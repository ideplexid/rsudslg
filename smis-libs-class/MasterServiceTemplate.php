<?php 
require_once 'smis-framework/smis/template/ModulTemplate.php';

/**
 * @since 19 Dec 2014
 * @license LGPL v3
 * @copyright Nurul Huda <goblooge@gmail.com>
 * @author goblooge
 *
 */
class MasterServiceTemplate extends MasterTemplate{
	protected $service;
	public function __construct($db,$service,$page,$action){
		$this->service=$service;
		parent::__construct($db,"",$page,$action);
	}
	
	public function setEntity($entity){
		$this->getServiceResponder()->setEntity($entity);
	}
	
	public function getServiceResponder(){
		return $this->getDBResponder();
	}
	
	public function getService(){
		return $this->service;
	}
	
	public function setDBresponder($dbresponder=null){
		if($dbresponder!=null){
			$this->dbresponder=$dbresponder;
		}else if($this->dbresponder==null){
			$this->dbresponder=new ServiceResponder($this->db, $this->getUItable(), $this->getAdapter(),$this->getService());
		}
		return $this;
	}
}

?>