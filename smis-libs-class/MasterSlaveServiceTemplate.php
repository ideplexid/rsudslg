<?php 
require_once 'smis-libs-class/MasterTemplate.php';

/**
 * this class used for creating UI that contains master and slave 
 * so this system will always create a child parent data
 * @since 27 Dec 2015
 * @license LGPL v3
 * @copyright Nurul Huda <goblooge@gmail.com>
 * @author goblooge
 *
 */
class MasterSlaveServiceTemplate extends MasterServiceTemplate{
	
	protected $form;
	protected $noclear;
	protected $view_data;
	protected $flag;
	protected $flag_title;
	protected $flag_warning;
	
	
	public function __construct($db,$service,$page,$action){
		parent::__construct($db, $service, $page, $action);
		$this->form=NULL;
		$this->autoreload=false;
		$this->noclear=array();
		$this->flag=array();
		$this->flag_title=array();
		$this->flag_warning=array();
	}
	
	/**
	 * get the form of master template of the system
	 * @param string $renew
	 * @return NULL or FORM;
	 */
	public function getForm($renew=false,$title=""){
		if($renew || $this->form==NULL){
			$this->form=$this->uitable->getModal()->getForm()->setTitle($title);
			$this->uitable->clearContent();
		}
		return $this->form;
	}
	
	public function setFlag($flag,$title,$content){
		$this->addFlag($flag, $title, $content);
		return $this;
	}
	
	public function addFlag($flag,$title,$content){
		$this->flag[$flag]=$flag;
		$this->flag_title[$flag]=$title;
		$this->flag_warning[$flag]=$content;	
		return $this;	
	}
	
	public function addNoClear($noclear){
		$this->noclear[]=$noclear;
		return $this;
	}
	
	public function addViewData($key,$value,$type="jquery"){
		$content="";
		if($type=="jquery") {
			$content="view_data['".$key."']=$('#".$this->action."_".$value."').val();";
		}else if($type=="jquery-free"){
			$content="view_data['".$key."']=$('#".$value."').val();";
		}else if($type=="free"){
			$content="view_data['".$key."']='".$value."';";
		}else if($type=="js"){
			$content=$value;
		}
		$this->view_data[]=$content;
		return $this;
	}
	
	
	/**
	 * (non-PHPdoc)
	 * @see MasterTemplate::phpPreLoad()
	 */
	public function phpPreLoad(){
		echo $this->form->getHtml();
		parent::phpPreLoad();
	} 
	/**
	 * (non-PHPdoc)
	 * @see MasterTemplate::onReady()
	 */
	public function onReady(){
		foreach($this->noclear as $noclear){
			echo $this->action.".addNoClear('".$noclear."');";
		}
		
		echo $this->action.".addViewData=function(view_data){";
		foreach($this->view_data as $content){
			echo $content; 
		}
		echo "return view_data;";
		echo "};";
		
		
		if(count($this->flag)>0){
			echo $this->action.".show_add_form=function(){";
			foreach($this->flag as $flag){
				echo "if($(\"#".$this->action."_".$flag."\").val()==\"\"){";
				echo "showWarning(\"".$this->flag_title[$flag]."\",\"".$this->flag_warning[$flag]."\");";
				echo "return;";
				echo "}";
			}
			echo "this.clear();";
			echo "this.show_form();";
			echo "};";
		}
	}
	
	
}

?>