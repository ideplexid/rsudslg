<?php 

/**
 * 
 * this class is used to avoid Concurent Process 
 * handling by use one single variable that store into database (settings)
 * a Process must get the lock token to do something 
 * otherwise it would return that another Process stil hold The Token
 * yet another Process cannot take it until the current process 
 * that hold the token release it
 * 
 * @license LGPL v 2
 * @since 19 Feb 2015
 * @author goblooge
 * @copyright Nurul Huda <goblooge@gmail.com>
 */


abstract class Locker{
	private $user;
	private $db;
	private $token;
	private $current_token;
	
	private $username;
	private $ip;
	private $time;
	private $created_token;
	protected $variable;
	
	public function __construct(Database $db, User $user, $token_name, $variable=""){
		$this->user		= $user;
		$this->db		= $db;
		$this->token	= $token_name;
		$this->variable	= $variable;
	}
	
	/**
	 * user must proceed this function 
	 */
	public abstract function proceed($variable);
	
	public function setVariable($variable){
		$this->variable	= $variable;
	}
	
	/**create a token hash, it' should be uniq each user */
	public function createToken(){
		$username			 = $this->user->getUsername();
		$time				 = $this->microtime_float();
		$ip					 = $this->user->get_client_ip();
		$random				 = rand(0, 1000);
		$this->created_token = md5($username.$time.$ip.$random);
		return $this->created_token;
	}

	/**change microtime to float */
	public function microtime_float(){
		list($usec, $sec) 	 = explode(" ", microtime());
		return ((float)$usec + (float)$sec);
	}
	
	/** executing the data using a token
	 *  if the token already used then gone.
	 */
	public function execute(){
		if($this->isTokenUsed()) 
			return $this->alreadyUsed();
		if($this->setLock()) 
			return $this->alreadyUsed();
		$result		= null;
		try{
			$result	= $this->succeed();
		}catch(Exception $e){
			$result	= $this->proceedError($e->getMessage());
		}
		$this->releaseLock();
		return $result;
	}
	
	/**check wethear the token or locker is already locked */
	public function isTokenUsed(){
		$this->current_token	= getSettings($this->db, "smis-locker-".$this->token, "",false);
		return  !$this->current_token=="";
	}
	
	/** try to lock a token in database */
	protected function setLock(){
		$new_token				= $this->createToken();
		return !setLockSettings($this->db, "smis-locker-".$this->token, $new_token);
	}
	
	public function alreadyUsed(){
		$result				= array();
		$result['success']	= 0;
		$result['message']	= "Failed to get Token";
		return $result;
	}
	
	public function proceedError($error){
		$result				= array();
		$result['success']	= 0;
		$result['message']	= $error;
		return $result;
	}
	
	public function succeed(){
		$result				= array();
		$result['success']	= 1;
		$result['data']		= $this->proceed($this->variable);
		$result['message']	= "Succeed";
		return $result;
	}
	
	/** release the token that already used by this session */
	public function releaseLock(){
		setSettings($this->db,  "smis-locker-".$this->token, "");
	}
}

?>