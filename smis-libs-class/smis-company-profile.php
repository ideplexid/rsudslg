<?php 

class CompanyProfile{
	private static $instance;
	private $db;
	private $logo;
	private $title;
	private $abbrevation;
	private $address;
	private $town;
	private $header;
	
	private function __construct($db){
		$this->db			= $db;
		$this->logo			= getLogo();
		$this->title		= getSettings($db, "smis_autonomous_title","");
		$this->town			= getSettings($db, "smis_autonomous_town","");
		$this->address		= getSettings($db, "smis_autonomous_address","");
		$this->abbrevation	= getSettings($db, "smis_autonomous_abbrevation", "SMIS");
	}
	
	public static function getInstance(){
		if(CompanyProfile::$instance==null){
			global $db;
			CompanyProfile::$instance=new CompanyProfile($db);
		}
		return CompanyProfile::$instance;
	}
	
	public function getLogo(){
		return $this->logo;
	}
	
	public function getTitle(){
		return $this->title;
	}

	public function getTown(){
		return $this->town;
	}
	
	public function getAddress(){
		return $this->address;
	}
	
	public function getAbbrevation(){
		return $this->abbrevation;
	}

	public function getLogoHtml(){
		return "<img src='".$this->logo."' />";
	}
	
	public function getHeader(){
		$tp=new TablePrint("pheader");
		$tp->setTableClass("pheader");
		$tp->setMaxWidth(false);
		
		$tp	->addColumn("<img src='".$this->logo."' />", 1, 2,null,"header_logo")
			->addColumn($this->title, 5, 1,null,"autonomous_title")
			->commit("title");
		$tp->addColumn($this->address, 5, 1,null,"autonomous_address")
			->commit("title");
		
		$css="<style type=\"text/css\">
					#header_container{ padding-bottom : 15px;}
					#header_logo{ text-align:right;}
					#header_logo img{height:70px; width:auto; margin:0;}
					#autonomous_title { font-size:40px; font-weight:800; color:green; text-align:center; line-height:40px;}
					#autonomous_address { font-size:20px; color:black; text-align:center; }
					#header_info > div > div > div:nth-child(2){ font-weight:800; text-transform:uppercase; }
					#header_info > div > div > div{ min-height:10px; }
					#header_info > div {margin:0px; padding:0px;}
					.pheader{width:100%; margin-bottom:5px;}
					.pheader tr td, #pheader tr th{border:none; margin:0; padding:0;}
					.phttl{font-weight:800;}	
				</style>
		";
		
		return $tp->getHtml().$css;
	}
	
	
	
}


?>