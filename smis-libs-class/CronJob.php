<?php 

class CronJob{
	
	private $db;
	public function __construct($db){
		$this->db	= $db;
	}
	
	public function run(){
        if($this->isCronJobTime())
			$this->runCronJob();		
	}
	
	private function isCronJobTime(){
        if(is_serverbus_request() || isset($_POST['smis_path_url']) || ( isset($_POST['page']) && $_POST['page']=="smis-administrator") ){
            return false;
        }
        
		$limit				= getSettings($this->db,"smis-cronjob-time","24")*1;
        if($limit==-1) {
            return false; /*CRONJOB DISABLED*/
        }
        $curtime=date("Y-m-d H:i:s");
        if($limit==0) {
            setSettings($this->db,"smis-last-cronjob-time",$curtime);
            return true; /*CRONJOB ALWAYS RUN*/
        }
		$lastcronjob		= getSettings($this->db,"smis-last-cronjob-time",null);
		if($lastcronjob==null){
			setSettings($this->db,"smis-last-cronjob-time",$curtime);
			return true;
		}else{
            loadLibrary("smis-libs-function-time");
			$time_elapse	= hour_different($lastcronjob,$curtime);
			if($time_elapse>=$limit){
				setSettings($this->db,"smis-last-cronjob-time",$curtime);
				return true;
			}
		}
		return false;
	}
	
	public function start(){
		$this->run();
	}
	
	public function runCronJob(){
        $cronjob	= array();
		$list		= getListActivePlugins(false);
		foreach($list as $name=>$link){
			if(file_exists($link."/cronjob.php")){
                if(getSettings($this->db,"smis-system-cronjob-".$name,"1")=="1"){
                    require_once $link."/cronjob.php";    
                }
				$cronjob[]	= $link."/cronjob.php";
			}
		}
        $this->saveCronjobLog($cronjob);
	}
    
    private function saveCronjobLog($cronjob_list){
        global $querylog;
        global $user;
        $waktu		= date("Y-m-d H:i:s");
        $username	= $user->getUsername();
        $cronjob	= json_encode($cronjob_list);
        $querylog->saveCronjob($waktu,$username,$cronjob);
    }
}
?>