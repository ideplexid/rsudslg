<?php 

/**
 * 
 * this class was implementing in the system to police the system
 * on user, including policy system of Prototype and etc.
 * 
 * @author goblooge
 * @since 4 Nov 2014
 * @version 1.0.0
 * @license LGPL v3
 * @copyright Nurul Huda <goblooge@gmail.com>
 *
 */

class Policy{
	private $page;
	private $user;
	private $policy;
	private $cookie;
	private $path;
	private $alias_page;
	private $array;
	private $default_policy_set;
	private $default_cookie_set;
	private $is_prototype;
	private $the_prototype;
	private $implement;	
	
	public static $DEFAULT_POLICY_ALLOW		= true;
	public static $DEFAULT_POLICY_RESTRICT	= false;
	
	public static $DEFAULT_COOKIE_CHANGE	= true;
	public static $DEFAULT_COOKIE_KEEP		= false;
	
	public function __construct($page,User $user){
		$this->page					= $page;
		$this->user					= $user;
		$this->default_cookie_set	= self::$DEFAULT_COOKIE_CHANGE;
		$this->default_policy_set	= self::$DEFAULT_POLICY_RESTRICT;
		$this->policy				= array();
		$this->cookie				= array();
		$this->is_prototype			= false;
		$this->the_prototype		= null;
		$this->implement			= "";
		$this->path					= array();
		$this->alias_page			= array();
	}
	
	/** adding alias page
	 *  @param $action is the actin name
	 *  @param $alias is the alias name for this action
	 */
	public function addAlias($action,$alias){
		if(!isset($this->alias_page[$action])){
			$this->alias_page[$action]	= array();
		}
		$this->alias_page[$action][]	= $alias;
        return $this;
	}
	
	/** get all policy that store in this object
	 *  @return Array of policy
	 */
	public function getAllPolicy(){
		$array					  = array();
		$array['policy']		  = $this->policy;
		$array['cookie']		  = $this->cookie;
		$array['path']			  = $this->path;
		return $array;
	}
	
	/** set default policy rule of this system
	 *  @param $default is the default value ther would be 2, 
	 *    	   DEFAULT_POLICY_ALLOW and DEFAULT_POLICY_RESTRICT  			
	 */
	public function setDefaultPolicy($default){
		$this->default_policy_set = $default;
		return $this;
	}
	
	/** default cookie change rule of this system
	 *  so when it's reload should it back to this page or not. 
	 *  there 2 different default cookie, DEFAULT_COOKIE_CHANGE and DEFAULT_COOKIE_KEEP
	*/
	public function setDefaultCookie($default){
		$this->default_cookie_set = $default;
		return $this;
	}
	
	
	/**
	 * combining more policy into one
	 * this one is for Library Purpose
	 * added @since 14 Nov 2014
	 * @param Policy $p
	 */
	public function combinePolicy(Policy $p){
		$array		= $p->getAllPolicy();
		$policy		= $array['policy'];
		$cookie		= $array['cookie'];
		$path		= $array['path'];
		foreach($policy as $action=>$policyname){
			$cook	= $cookie[$action];
			$pt		= $path[$action];
			$this->addPolicy($action, $policyname, $cook,$pt);
		}
		return $this;
	}
	
	/** adding the policy rules
	 *  @param $action is the action name 
	 *  @param $policyname is the policy name
	 *  @param $cookie is wether cookie change or not
	 *  @param $path is the string where the path should be land (default is inside the modul folder)
	 */
	public function addPolicy($action,$policyname=null,$cookie=true,$path=null){
		if($policyname==null) {
			$policyname			= $action;
		}
		$this->policy[$action]	= $policyname;
		$this->cookie[$action]	= $cookie;
		$this->path[$action]	= $path;
		return $this;
	}
	
	public function initialize(){
		$action			= $_POST['action'];
		if(isset($this->policy[$action])){
			$policyname = $this->policy[$action];
			if($this->user->isAuthorize($this->page, $policyname)){
				setChangeCookie(false);
				if($this->cookie[$action])
					changeCookie();
				$this->allow($this->page, $action);
			}else if(isset($this->alias_page[$action])){
				$all_alias		= $this->alias_page[$action];
				$allow_by_alias	= false;
				foreach ($all_alias as $alias_page){
					$allow_by_alias=$this->user->isAuthorize($alias_page, $policyname);
					if($allow_by_alias){
						setChangeCookie(false);
						if($this->cookie[$action])
							changeCookie();
						$this->allow($this->page, $action);
						return $this;
					}
				}
				$this->denied();
			}else{
				$this->denied();
			}
		}else{
			if($this->default_policy_set==self::$DEFAULT_POLICY_ALLOW){
				$this->allow($this->page, $action);
				setChangeCookie(false);
				if($this->default_cookie_set)
					changeCookie();
			}else{
				$this->denied();
			}
		}
		return $this;
	}
	
	/** if this modul is prorotype the this prototype should be set
	 *  @param $is_prototype is a boolean value 
	 *  @param $implement is the impement or th modul name
	 */
	public function setPrototype($is_prototype,$implement){
		$this->is_prototype	= $is_prototype;
		$this->implement	= $implement;
		return $this;
	}
	
	/** get the landing path of an action  */
	public function getPath($action){
		if(!isset($this->path[$action]) || $this->path[$action]==null){
			return $action;
		}
		return $this->path[$action];
	}
	
	/** allow the user to go to landing path of the current page and action
	 *  @param $page is the page (modul or prototype) name
	 *  @param $action is the action name
	 */
	public function allow($page,$action){
		$path	= $this->getPath($action);
		if($this->is_prototype){
				if(isset($_POST['prototype_implement']) 
					&& $this->implement==$_POST['prototype_implement']
					&& isset($_POST['prototype_slug']) 
					&& $_POST['prototype_slug']!=""
					&& isset($_POST['prototype_name']) 
					&& $_POST['prototype_name']!="" 
					&& file_exists($_POST['prototype_implement']."/".$path.".php") ){
						require_once $_POST['prototype_implement']."/".$path.".php";
				}
		}else{			
			global $db;
			require_once $this->page."/".$path.".php";
		}
		return $this;
	}
	
	/** denied the user of current request */
	public function denied(){
		setChangeCookie(false);
		$res = new ResponsePackage();
		$res ->setStatus(ResponsePackage::$STATUS_UNAHTORIZED);
		echo json_encode($res->getPackage());
		return $this;
	}
}
?>