<?php 
require_once 'smis-framework/smis/template/AdvanceModulTemplate.php';

/**
 * this class use to handle
 * a modul tempate that 
 * usually provide by
 * an prototype
 * 
 * @since       : 19 Dec 2014
 * @license     : LGPL v3
 * @copyright   : Nurul Huda <goblooge@gmail.com>
 * @author      : goblooge
 * @version     : 1.0.3
 */
class MasterTemplate extends AdvanceModulTemplate{
	/* @var DBTable */
	protected $dbtable;
    /* @var Table */
	protected $uitable;
    /* @var DBResponder */
	protected $dbresponder;
    /* @var ArrayAdapter */
	protected $adapter;
    /* @var String */
	protected $table_name;
    /* @var Modal */
	protected $uimodal;
    /* @var Array */
	protected $js_column;
	/* @var Array */
	protected $js_object;
    
	/* @var Array */
	protected $add_chooserdata;
	/* @var Array */
	protected $command_viewdata;	
	/* @var Array */
	protected $add_regulerdata;
    /* @var Array */
	protected $add_savedata;
    /* @var Array */
	protected $duplicate_name_view;
    
	
	/* @var Boolean */
	protected $is_use_datetime;
	/* @var Boolean */
	protected $is_use_date;
	/* @var Boolean */
	protected $is_use_time;
	/* @var Boolean */
	protected $is_use_select2;
    /* @var Boolean */
	protected $is_use_loader;
    
	/* @var String */
	protected $modal_title;
	/* @var Boolean */
	protected $autoreload;
	/* @var Boolean */
	protected $is_truncate;
	/* @var Boolean */
	protected $is_modal_save_button_enable;
	/* @var Array */
	protected $resource_before;
	/* @var Array */
	protected $resource_after;
	/* @var Boolean */
	protected $is_use_default_js;
	/* @var Boolean */
	protected $always_visible_form;
	/* @var Boolean */
	protected $is_autofocus;
	/* @var Boolean */
	protected $is_next_enter;
	/* @var Boolean */
	protected $is_autofocus_on_multiple;
	/* @var Boolean */
	protected $is_multiple;
	/* @var Boolean */
	protected $is_clear_on_edit_for_no_clear;
    /* @var Array */
	protected $noclear;
	/* @var String */
	protected $modal_component_size;
	/* @var String */
	protected $modal_size;
	
    /* @var String */
	protected $protoname;
    /* @var String */
	protected $protoslug;
    /* @var String */
	protected $protoimple;
    /* @var Boolean */
	protected $is_prototype;
    
	
	
	public function __construct($db,$tbl,$page,$action){
		parent::__construct($db,$page,$action);
		$this->table_name=$tbl;
		$this->js_column=array();
		$this->autoreload=true;
		$this->is_truncate=false;
		$this->is_modal_save_button_enable=true;
		$this->resource=array();
		$this->is_use_default_js=true;
		$this->always_visible_form=false;
		$this->command_viewdata=array();
		$this->js_object="TableAction";
		$this->add_regulerdata=array();
		$this->add_chooserdata=array();
		$this->is_autofocus=false;
        $this->is_clear_on_edit_for_no_clear=false;
		$this->is_next_enter=false;
		$this->is_multiple=false;
		$this->is_autofocus_on_multiple=null;
		$this->noclear=array();
		$this->modal_component_size=Modal::$SMALL;
		$this->modal_size=Modal::$NORMAL_MODEL;
        $this->is_prototype=false;
        $this->protoimple="";
        $this->protoname="";
        $this->protoslug="";
        $this->add_savedata=array();
        $this->duplicate_name_view=null;
        $this->is_use_loader=false;
	}
    
    /**
     * @brief change the default class of table action if needed
     * @param stirng $name 
     * @return  this
     */
    public function setTableActionClass($name){
        $this->js_object=$name;
        return $this;
    }
    
    /**
     * @brief while add add no clear on multiple
     *          if set true the no clear in multiple would be clear
     *          on edit
     * @param boolean $enabled 
     * @return  this
     */
    public function setClearOnEditForNoClear($enabled){
        $this->is_clear_on_edit_for_no_clear=$enabled;
        return $this;
    }
    
    /**
	 * enabled or disabled Truncate table in System
	 * @param unknown $enable
	 */
	public function setTruncateEnable($enable){
		$this->is_truncate=$enable;
		return $this;
	}
    
    
    
    /**
     * @brief add noclear array
     * @param string $noclear of the component name 
     * @return  MasterTemplate
     */
    public function addNoClear($noclear){
		$this->noclear[]=$noclear;
		return $this;
	}
    
   
    public function setPrototipe($is_prototype,$protoname,$protoslug,$protoimplement){
        $this->is_prototype=$is_prototype;
        $this->protoimple=$protoimplement;
        $this->protoname=$protoname;
        $this->protoslug=$protoslug;
        return $this;
    }
    
    /**
     * @brief : set the duplicate name view
     * @param : String $name of the table in database
     * return this;
     * */
    public function setDuplicateNameView($name){
        $this->duplicate_name_view=$name;
        return $this;
    }
	
	public function setModalComponentSize($size){
		$this->modal_component_size=$size;
        return $this;
	}
	
	public function setModalSize($size){
		$this->modal_size=$size;
        return $this;
	}
	
	/**
	 * whether the modal multiple input or not.
	 * @param boolean $is_multiple
	 */
	public function setMultipleInput($is_multiple){
		$this->is_multiple=$is_multiple;
	}
	
	/**
	 * whether the modal autofocus or not.
	 * @param boolean $auto
	 */
	public function setAutofocus($auto){
		$this->is_autofocus=$auto;
	}
	
	/**
	 * whether the modal autofocus on $element when used multiple input.
	 * @param boolean $auto
	 */
	public function setAutofocusOnMultiple($element){
		$this->is_autofocus_on_multiple=$element;
	}
	
	/**
	 * whether the modal can accept next enter or not.
	 * @param boolean $nextenter
	 */
	public function setNextEnter($nextenter){
		$this->is_next_enter=$nextenter;
	}
	
	/**
	 * whether the form always visible or not.
	 * @param boolean $visible
	 */
	public function setFormVisibility($visible){
		$this->always_visible_form=$visible;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see ModulTemplate::initialize()
	 */
	public function initialize(){
		if(isset($_POST['super_command']) && $_POST['super_command']!=""){
			$this->superCommand($_POST['super_command']);
		}else if(isset($_POST['command'])){
			$this->command($_POST['command']);
		}else{
			$this->getResourceBeforePreload();
			$this->phpPreLoad();
			if($this->is_use_default_js){
				$this->jsLoader();
				$this->cssLoader();
				$this->jsPreLoad();
				$this->cssPreLoad();
			}
			$this->htmlPreLoad();
			$this->getResourceAfterPreload();
		}
	}
	
	/**
	 * for enable or disabled Save Button in Modal
	 * @param unknown $enable
	 */
	public function setModalSaveButtonEnable($enable){
		$this->is_modal_save_button_enable=$enable;
		return $this;
	}
	
	/**
	 * adding data on view action comming;
	 * @param string $key
	 * @param string $value
	 */
	public function addViewData($key,$value,$type='value'){
		if($type=='value'){
			$this->command_viewdata[]="view_data['".$key."']='".$value."';";
		}else if($type=='id-value'){
			$this->command_viewdata[]="view_data['".$key."']=\$('#'+this.prefix+'_".$value."').val();";
		}else if($type=='free-id-value'){
			$this->command_viewdata[]="view_data['".$key."']=\$('#".$value."').val();";
		}else if($type=='id-html'){
			$this->command_viewdata[]="view_data['".$key."']=\$('#'+this.prefix+'_".$value."').html();";
		}else if($type=='free-id-html'){
			$this->command_viewdata[]="view_data['".$key."']=\$('#".$value."').html();";
		}else{
			$this->command_viewdata[]="view_data['".$key."']=".$value.";";
		}
        return $this;
	}
    
    /**
	 * adding data on save action comming;
	 * @param string $key
	 * @param string $value
	 */
	public function addSaveData($key,$value,$type='value'){
		if($type=='value'){
			$this->add_savedata[]="save_data['".$key."']='".$value."';";
		}else if($type=='id-value'){
			$this->add_savedata[]="save_data['".$key."']=\$('#'+this.prefix+'_".$value."').val();";
		}else if($type=='free-id-value'){
			$this->add_savedata[]="save_data['".$key."']=\$('#".$value."').val();";
		}else if($type=='id-html'){
			$this->add_savedata[]="save_data['".$key."']=\$('#'+this.prefix+'_".$value."').html();";
		}else if($type=='free-id-html'){
			$this->add_savedata[]="save_data['".$key."']=\$('#".$value."').html();";
		}else{
			$this->add_savedata[]="save_data['".$key."']=".$value.";";
		}
		return $this;
	}
	
	/**
	 * adding data on view action comming;
	 * @param string $key
	 * @param string $value
	 */
	public function addRegulerData($key,$value,$type='value'){
		if($type=='value'){
			$this->add_regulerdata[]="reg_data['".$key."']='".$value."';";
		}else if($type=='id-value'){
			$this->add_regulerdata[]="reg_data['".$key."']=\$('#'+this.prefix+'_".$value."').val();";
		}else if($type=='free-id-value'){
			$this->add_regulerdata[]="reg_data['".$key."']=\$('#".$value."').val();";
		}else if($type=='id-html'){
			$this->add_regulerdata[]="reg_data['".$key."']=\$('#'+this.prefix+'_".$value."').html();";
		}else if($type=='free-id-html'){
			$this->add_regulerdata[]="reg_data['".$key."']=\$('#".$value."').html();";
		}else{
			$this->add_regulerdata[]="reg_data['".$key."']=".$value.";";
		}
		return $this;
	}
	
	public function addChooserData($key,$value,$type='value'){
		if($type=='value'){
			$this->add_chooserdata[]="c_data['".$key."']='".$value."';";
		}else if($type=='id-value'){
			$this->add_chooserdata[]="c_data['".$key."']=\$('#'+this.prefix+'_".$value."').val();";
		}else if($type=='free-id-value'){
			$this->add_chooserdata[]="c_data['".$key."']=\$('#".$value."').val();";
		}else if($type=='id-html'){
			$this->add_chooserdata[]="c_data['".$key."']=\$('#'+this.prefix+'_".$value."').html();";
		}else if($type=='free-id-html'){
			$this->add_chooserdata[]="c_data['".$key."']=\$('#".$value."').html();";
		}else{
			$this->add_chooserdata[]="c_data['".$key."']=".$value.";";
		}
	}
	
	
	
	
	
	/**
	 * set whether using default js or not
	 * if yes then the default TableAction will print
	 * otherwise then will be no Javaascript of TableAction
	 * @param unknown_type $use
	 */
	public function setUseDefaultJS($use){
		$this->is_use_default_js=$use;
		return $thi;
	}
	
	
	
	
	/**
	 * adding superrcommand variable
	 * @param $var the suppercommand name
	 * @param $content content could be string for selected or array and combine it with function addSuperCommandArray($var,$key,$value,$free=false)
	 */
	public function addSuperCommand($var,$content){
		$this->supercommand_js_variable[$var]=$content;
		return $this;
	}
	
	public function addJSColumn($name,$reset=false){
		if($reset) $this->js_column=array();
		if($name!=null && $name!="") $this->js_column[]=$name;
		return $this;
	}
    
    public function setLoaderEnable($used){
		$this->is_use_loader=$used;
		return $this;
	}
	
	public function setDateEnable($used){
		$this->is_use_date=$used;
		return $this;
	}
	
	public function setDateTimeEnable($used){
		$this->is_use_datetime=$used;
		return $this;
	}
	
	public function setTimeEnable($used){
		$this->is_use_time=$used;
		return $this;
	}
	
	public function setSelect2Enable($used){
		$this->is_use_select2=$used;
		return $this;
	}
	
	public function setModalTitle($title){
		$this->modal_title=$title;
		return $this;
	}
	
	public function setAutoReload($autoreload){
		$this->autoreload=$autoreload;
		return $this;
	}
	
	
	
	public function getSuperCommand(){
		if($this->supercommand==null) 
			$this->setSuperCommand();
		return $this->supercommand;
	}
	
	public function setSuperCommand($supercommand=null){
		if($supercommand!=null){
			$this->supercommand=$supercommand;
		}else if($this->supercommand==null){
			$this->supercommand=new SuperCommand();
		}
		return $this;
	}
	
	public function getJSColumn($to_array=false){
		if(count($this->js_column)==0){
			$this->js_column=$this->getDBtable()->get_column();
		}
		
		if($to_array) return $this->js_column;
		$result="";
		foreach($this->js_column as $name){
			if($result==""){
				$result="'".$name."'";
			}else{
				$result.=",'".$name."'";
			}
		}
		return $result;
	}
	
    /**
     * @brief SuperCommand message
     * @return  MasterTamplate
     */
	
	public function superCommand($super_command){
		$init = $this->getSuperCommand()->initialize ();
		if ($init != null) {
			echo $init;
			return $this;
		}
        return $this;
	}
    
    /**
     * @brief adding super command responder in system
     * @param string $slug 
     * @param Responder $responder 
     * @return  
     */
    public function addSuperCommandResponder($slug,Responder $responder){
        $this->getSuperCommand()->addResponder($slug,$responder);
        return $this;
    }
    
	public function command($command){		  
		if($this->is_truncate){
			if ($_POST ['command'] == "tuncate_db") {
				$this->db->query ( "truncate ".$this->table_name.";" );
				return "";
			}
		}
		$dbres=$this->getDBResponder();
		$data = $dbres->command ($command);
		if($data!=NULL)
			echo json_encode ($data);
		return;
	}
	
	
	public function phpPreLoad(){
		if($this->is_truncate){
			$truncate= new Button ( "", "", "" );
			$truncate->setAction ( $this->action.".tuncate_db()" );
			$truncate->setIsButton ( Button::$ICONIC );
			$truncate->setIcon("fa fa-trash");
			$truncate->setClass("btn-primary");
			$this->getUItable()->addFooterButton($truncate);
		}

		if($this->always_visible_form){
			$form=$this->getModal()->joinFooterAndForm();
			echo $form->getHtml();
			echo $this->getUItable()->getHtml ();
		}else{
			echo $this->getUItable()->getHtml ();
			echo $this->getModal()->getHtml();
		}
        
        if($this->is_use_loader){
			echo addJS ( "base-js/smis-base-loading.js");
		}		
		echo addJS ( "framework/smis/js/table_action.js" );
		
		if($this->is_use_date){
			echo addJS ( "framework/bootstrap/js/bootstrap-datepicker.js" );
			echo addCSS ( "framework/bootstrap/css/datepicker.css" );
            echo '<script type="text/javascript">';
                echo '$(".mydate").datepicker();';
            echo '</script>';
		}
		
		if($this->is_use_datetime){
			echo addJS ( "framework/bootstrap/js/bootstrap-datetimepicker.js" );
			echo addCSS ( "framework/bootstrap/css/bootstrap-datetimepicker.css" );
			echo '<script type="text/javascript">';
                echo '$(".mydatetime").datetimepicker({ minuteStep: 1});';
            echo '</script>';
		}
		
		if($this->is_use_time){
			if(!$this->is_use_datetime){
				echo addJS ( "framework/bootstrap/js/bootstrap-datetimepicker.js" );
				echo addCSS ( "framework/bootstrap/css/bootstrap-datetimepicker.css" );
			}
            echo '<script type="text/javascript">';
                echo "$('.mytime').datetimepicker({ minuteStep: 1,startView:'day',maxView:'day'});";
            echo '</script>';
		}
		
		if($this->is_use_select2){
			echo addJS ( "framework/bootstrap/js/bootstrap-select.js" );
			echo addCSS ( "framework/bootstrap/css/bootstrap-select.css" );
            echo '<script type="text/javascript">';
                echo '$(".select2").select2();';
            echo '</script>';
		}
	}
	
	public function jsPreload(){
        echo "<script type='text/javascript'>";
        echo "var ".$this->action.";";
        echo '$(document).ready(function(){';
            echo "$('[data-toggle=\"popover\"]').popover({trigger: 'hover','placement': 'top'});";
            echo "var column=new Array(".$this->getJSColumn().");";
            echo $this->action."=new ".$this->js_object."('".$this->action."','".$this->page."','".$this->action."',column);";
            if($this->is_prototype){
                echo $this->action.".setPrototipe('".$this->protoname."','".$this->protoslug."','".$this->protoimple."');";
            }
            $this->onReady();
            if($this->is_truncate){
                echo $this->action.".tuncate_db=function(){";
                    echo "var data=this.getRegulerData();";
                    echo "data['command']='tuncate_db';";
                    echo '$.post("",data,function(res){';
                        echo $this->action.".view();";
                    echo "});";
                echo "};";
            }       
            
            if($this->is_multiple){
                echo $this->action.".setMultipleInput(true);";
            }            
            if($this->is_autofocus_on_multiple!=null){
                echo $this->action.".setFocusOnMultiple('".$this->is_autofocus_on_multiple."');";
            }            
            if($this->is_autofocus){
                echo $this->action.".setEnableAutofocus();";
            }            
            if($this->is_next_enter){
                echo $this->action.".setNextEnter();";
            }    
        
            if(count($this->command_viewdata)>0){
                echo $this->action.".addViewData=function(view_data){";
                    foreach($this->command_viewdata as $view){
                        echo $view;
                    }
                    echo "return view_data;";
                echo "};";
            }          
 
            if(count($this->add_savedata)>0){
                echo $this->action.".addSaveData=function(save_data){";
                    foreach($this->add_savedata as $save){
                        echo $save;
                    }
                    echo "return save_data;";
                echo "};";
            }
            if($this->duplicate_name_view!=null && $this->duplicate_name_view!=""){
                echo $this->action.".setDuplicateNameView('".$this->duplicate_name_view."');";
            }   
         
            if(count($this->add_regulerdata)>0){
                echo $this->action.".addRegulerData=function(reg_data){";
                    foreach($this->add_regulerdata as $reg){
                        echo $reg;
                    }
                    echo "return reg_data;";
                echo "};";
            }            
            if(count($this->add_chooserdata)>0){
                echo $this->action.".addChooserData=function(c_data){";
                    foreach($this->add_chooserdata as $c){
                        echo $c;
                    }
                    echo "return c_data;";
                echo "}"; 
            }    
        
            if($this->autoreload){
                echo $this->action.".view();";
            }
        echo "});";
        echo "</script>";
        $this->getSuperCommandJavascript();			
	}
	
	/**
	 * this function will call after system already full load in browser
	 */
	public function onReady(){
		foreach($this->noclear as $noclear){
			echo $this->action.".addNoClear('".$noclear."');";
		}
        
        if($this->is_clear_on_edit_for_no_clear){
            echo $this->action.".setEditClearForNoClear(true);";
        }
	}
    
	/**
     * @brief get Table Object in this 
     *          Class
     * @return  MasterTamplate
     */
	
	public function setAdapter($adapter=null){
		if($adapter==null && $this->adapter==null)
			$this->adapter=new SimpleAdapter();
		else $this->adapter=$adapter;
		$this->getDBResponder()->setAdapter($this->adapter);
		return $this;
	}
	
    /**
     * @brief seet Table Object in this Class
     * @return  MasterTamplate
     */
	
	public function setUITable($uitable=null){
		if($uitable!=null){
			$this->uitable=$uitable;
		}else if($this->uitable==null){
			$this->uitable=new Table(array());
			$this->uitable->setName($this->action);
		}
		$this->uitable->setModalSaveButtonEnable($this->is_modal_save_button_enable);
		if($this->dbresponder!=null){
            $this->dbresponder->setUITable($this->uitable);
        }
        return $this;
	}
	
	/**
     * @brief get DBTable Object in this Class
     * @return  MasterTamplate
     */
	public function setDBtable($dbtable=null){
		if($dbtable!=null){
			$this->dbtable=$dbtable;
		}else if($this->dbtable==null){
			$this->dbtable=new DBTable($this->db, $this->table_name);
		}
		return $this;
	}
    
	 /**
     * @brief get DBResponder Object in this Class
     * @return  MasterTamplate
     */
	public function setDBresponder($dbresponder=null){
		if($dbresponder!=null){
			$this->dbresponder=$dbresponder;
		}else if($this->dbresponder==null){
			$this->dbresponder=new DBResponder($this->getDBtable(), $this->getUItable(), $this->getAdapter());
		}
		return $this;
	}
	
	 /**
     * @brief get ArrayAdapter Object in this Class
     * @return  ArrayAdapter
     */
	public function getAdapter(){
		if($this->adapter==null) $this->setAdapter();
		return $this->adapter;
	}
    
	 /**
     * @brief get DBResponder Object in this Class
     * @return  DBResponder
     */
	public function getDBResponder(){
		if($this->dbresponder==null) $this->setDBresponder();
		return $this->dbresponder;
	}
	
    /**
     * @brief get Table Object in this Class
     * @return  Table
     */
	public function getUItable(){
		if($this->uitable==null) $this->setUITable();
		return $this->uitable;
	}
    
	 /**
     * @brief get DBTable Object in this Class
     * @return  DBTable
     */
	public function getDBtable(){
		if($this->dbtable==null) $this->setDBtable();
		return $this->dbtable;
	}
    
	 /**
     * @brief get Modal Object in this Class
     * @return  Modal
     */
	public function getModal($renew=false){
		if($this->uimodal==null || $renew || !is_object($this->uimodal)){
			$uitable=$this->getUItable();
			$uitable->setModalSaveButtonEnable($this->is_modal_save_button_enable);
			$modal=$uitable->getModal();
			$modal->setTitle($this->modal_title);
			$this->uimodal=$modal;
			$this->uimodal->setComponentSize($this->modal_component_size);
			$this->uimodal->setModalSize($this->modal_size);
		}
		return $this->uimodal;
	}
	
}

?>