<?php 

/**
 * this class used to read a data from 
 * Excel File then read it and
 * automatically store in database
 * 
 * @author      : Nurul Huda
 * @copyright   : goblooge@gmail.com
 * @since       : 17 Mei 2017
 * @license     : LGPLv3
 * @version     : 1.0.0
 * */

class ExcelImporter{
    /** @var Array*/
    protected $setup;
    /** @var Database*/
    protected $db;
    /** @var String*/
    protected $file_path;
    /** @var DBTable */
    protected $dbtable;
    /** @var Array */
    protected $id;
    /** @var Array */
    protected $fix_value;
   
    
    public function __construct(Database $db, DBTable $dbtable, $file_path){
        $this->setup        = array();
        $this->db           = $db;
        $this->file_path    = $file_path;
        $this->dbtable      = $dbtable;
        $this->db_content   = array();
        $this->id           = array();
        $this->fix_value    = array();
    }
    
    /**
     * @brief set the file path of excel importer
     * @param String $file_path 
     * @return  $this;
     */
    public function setFilePath($file_path){
        $this->file_path    = $file_path;
        return $this;
    }
    
    /**
     * @brief adding key to determine insert or update
     * @param String $id 
     * @return  $this
     */
    public function addKey($id){
        $this->id[]             = $id;
        return $this;
    }
    
    /**
     * @brief adding a fix value in database
     * @param String $id is the key
     * @param String $id is the value     * 
     * @return  $this
     */
    public function addFixValue($id,$value){
        $this->fix_value[$id]   = $value;
        return $this;
    }
    
    /**
     * @brief clear the table
     * @return  $this
     */
    public function truncate(){
        $this->dbtable->truncate();
        return $this;
    }
    
    /**
     * @brief adding setup of data each sheet that will be proceed
     * @param int $sheet_index 
     * @param int $start_row 
     * @param Array $column_map 
     * @param String $sheet_map 
     * @return  this
     */
    public function addSetup($sheet_index,$start_row,$column_map,$sheet_map){
        $this->setup[$sheet_index]              = array();
        $this->setup[$sheet_index]['start']     = $start_row;
        $this->setup[$sheet_index]['column']    = $column_map;
        $this->setup[$sheet_index]['sheet']     = $sheet_map;
        return $this;
    }
    
    /**
     * @brief start storing all database in file
     *          this methode will loop and start storing
     *          all data in excel.
     * @return  int $total_data imported;
     */
    public function execute(){
        $url_file                   = $this->getFile();
        $total_data                 = 0;
        if($url_file!=null){             
            $fileType               = 'Excel5';      
            foreach($this->setup as $setup_index=>$setup){
                $objReader          = PHPExcel_IOFactory::createReader($fileType);
                $objReader->setLoadSheetsOnly($setup['sheet_name']);
                $objPHPExcel        = $objReader->load($url_file);            
                $actived=$objPHPExcel->setActiveSheetIndex($setup_index);
                
                $setup_column       = $setup['column'];
                $setup_sheet_map    = $setup['sheet'];
                $rowindex           = 1;               
                foreach($actived->getRowIterator() as $row){
                    if($rowindex>=$setup['start']){                       
                        $onerow     = array();
                        $idx        = array();
                        $this->oneSetup($onerow,$idx,$setup_column,$rowindex,$actived);
                        if($setup_sheet_map!=null && $setup_sheet_map!=""){
                            $onerow[$setup_sheet_map]   = $actived->getTitle();
                        }
                        /* adding fix value */
                        $onerow     = array_merge($onerow,$this->fix_value);
                        if(count($this->id)>0){
                            $this->dbtable->insertOrUpdate($onerow,$idx);
                        }else{
                            $this->dbtable->insert($onerow);
                        }
                        $total_data++;
                    }
                    $rowindex++;
                }
            }
        }
        return $total_data;
    }
    
    /**
     * @brief processing one data in excel row
     * @param array $onerow is the result that will stored 
     * @param array $idx is the id that will stored
     * @param array $setup_column is the setup column
     * @param int $rowindex is the index of the row
     * @return  null
     */
    public function oneSetup(array &$onerow,array &$idx,array &$setup_column,int $rowindex,PHPExcel_Worksheet $sheet){
        foreach($setup_column as $index=>$name){
            $cell           = $sheet->getCellByColumnAndRow($index,$rowindex);
            $value          = $cell->getValue();
            if(PHPExcel_Shared_Date::isDateTime($cell)) {
                $value      = date("Y-m-d", PHPExcel_Shared_Date::ExcelToPHP($value)); 
            }
            $onerow[$name]  = $value;
            if(in_array($name,$this->id)){
                $idx[$name] = $value;
            }
        }
    }
    
    /**
     * @brief get the uploaded file
     *      in variable $file_path
     * @return  null if not exist
     */
    public function getFile(){
        $this->loadLibrary();
        if(is_file_exist($this->file_path)){
           return get_fileurl($this->file_path);
        }
        return null;
    }
    
    /**
     * @brief get all sheet name in worksheet
     * @return  Array all worksheet name 
     */
    public function getListSheet(){
        $url_file       = $this->getFile();
        $inputFileType  = 'Excel5';
        $inputFileName  = $url_file;
        $objReader      = PHPExcel_IOFactory::createReader($inputFileType);
        $worksheetNames = $objReader->listWorksheetNames($inputFileName);
        return $worksheetNames;
    }
    
    /**
     * @brief count of worksheet
     * @return  int count of the worksheet
     */
    public function countSheet(){
        $sheetname      = $this->getListSheet();
        return count($sheetname);
    }
    
    /**
     * @brief load the basic library
     * @return  this;
     */
    private function loadLibrary(){
        require_once "smis-libs-out/php-excel/PHPExcel.php"; 
        loadLibrary("smis-libs-function-excel");
        loadLibrary("smis-libs-function-file");
        loadLibrary("smis-libs-function-string");
        return $this;
    }
}
?>