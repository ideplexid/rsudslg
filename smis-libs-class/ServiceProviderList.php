<?php 
/** this class used to provide service to list the name of the prototype
 *  so the prototype name will automatically named as the slug
 *  @author Nurul Huda
 *  @since 15 aug 2015
 *  @version 1.2.3
 *  @copyright goblooge@gmail.com
 * 	@license LGPLv2
 * 
 */
class ServiceProviderList extends ServiceConsumer{
	private $default;
	public function __construct($db,$service,$default=""){
		parent::__construct($db, "get_entity",$service);
		$this->default				= $default;
	}
	public function proceedResult(){
		$content	= array();
		$result		= json_decode($this->result,true);
		foreach($result as $autonomous){
			foreach($autonomous as $entity){
				$option				= array();
				$option['value']	= $entity;
				$option['name']		= ArrayAdapter::format("unslug",$entity);
				$option['default']	= $this->default==$entity?"1":"0";
				$content[]			= $option;
			}
		}
		return $content;
	}
}
?>