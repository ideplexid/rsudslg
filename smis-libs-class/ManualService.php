<?php 

class ManualService {
	
	private $description;
	private $data_provide;
	private $data_need;
	private $name;
	private $author;
	private $respond_type;
	public static $RESPOND_DEFAULT=1;
	public static $RESPOND_SERVICE=2;
	public static $RESPOND_COVER=3;
	
	
	
	public function __construct($name,$respond=1){
		$this->name=$name;
		$this->respond_type=$respond;
		if($this->respond_type==self::$RESPOND_SERVICE){
			$this->data_need=array();
			$this->data_provide=array();
		}
	}
	
	public function setDescription($v){
		$this->description=$v;
		return $this;
	}
	
	public function addDataProvided($command,$value){
		if($this->respond_type==self::$RESPOND_DEFAULT)
			$this->data_provide[$command]=json_encode($value,JSON_PRETTY_PRINT);
		return $this;
	}
	
	public function addDataNeeded($command,$value){
		if($this->respond_type==self::$RESPOND_SERVICE)
			$this->data_need[$command]=json_encode($value,JSON_PRETTY_PRINT);
		return $this;
	}
	
	public function setDataProvided($v){
		if($this->respond_type==self::$RESPOND_DEFAULT)
			$this->data_provide=json_encode($v,JSON_PRETTY_PRINT);
		return $this;
	}
	
	public function setDataNeeded($v){
		if($this->respond_type==self::$RESPOND_DEFAULT)
			$this->data_need=json_encode($v,JSON_PRETTY_PRINT);
		return $this;
	}
	
	public function setAuthor($v){
		$this->author=$v;
		return $this;
	}
	
	public function getHtmlDefault(){
		$res="<manual>";
			$res.="<h3>".$this->name."</h3>";
			$res.=$this->getDescription();
			$res.="<div class='line clear'>";
				$res.="<h5 class='label label-success'>Data Provided</h5>";
				$res.="<pre class='spjson'>".$this->data_provide."</pre>";
			$res.="</div>";
			$res.="<div class='line clear'>";
				$res.="<h5 class='label label-info'>Data Needed</h5>";
				$res.="<pre class='spjson'>".$this->data_need."</pre>";
			$res.="</div>";
			$res.=$this->getAuthor();
		$res.="</manual>";
		return $res;
	}
	
	public function getDescription(){
		$res="<div class='line clear'>";
			$res.="<h3 class='label label-inverse'>DESCRIPTION</h3>";
			$res.="<pre>".$this->description."</pre>";
		$res.="</div>";
		return $res;
	}
	
	public function getAuthor(){
		$res="<div class='line clear'>";
			$res.="<h3 class='label '>AUTHOR</h3>";
			$res.="<pre class='alert alert-inverse'>".$this->author."</pre>";
		$res.="</div>";
		return $res;
	}
	
	public function getHtmlService(){
		$res="<manual>";
			$res.="<h3>".$this->name."</h3>";
			
			$res.="<div class='line clear'>";
				$res.="<h3 class='label label-important'>ServiceResponder Required</h3>";
				$res.="<p class='alert alert-danger' style='text-align:justify' id='service_responder'>
						this service using Class <strong>ServiceReponder</strong> as it's Based, 
						so The Implementator must use Class <strong>ServiceProvider</strong> As it's Based. 
						service like theese usually provide as chooser in System. 
						The Implementator Could Simply use ServiceProvider Class
						<strong>(if there no Overriden Data Needed for each Action) </strong>
						but if there <strong>exist</strong> Overriden 'Data Needed', then the implementator
						should override 'Class ServiceResponder ['action'] ' that need to be Override.
						</p>
						";
			$res.="</div>";
			
			$res.=$this->getDescription();		
			
			if(count($this->data_provide)>0){
				$res.="<h4>Data Provided That Overriden</h4>";
				foreach($this->data_provide as $action=>$value){
					$res.="<div class='line clear'>";
						$res.="<h5 class='label label-success'>".strtoupper($action)."</h5>";
						$res.="<pre class='spjson'>".$value."</pre>";
					$res.="</div>";
				}
			}
			
			if(count($this->data_need)>0){
				$res.="<h4>Action Needed Override</h4>";
				foreach($this->data_need as $action=>$value){
					$res.="<div class='line clear'>";
						$res.="<h5 class='label label-info'>".strtoupper($action)."</h5>";
						$res.="<pre class='spjson'>".$value."</pre>";
					$res.="</div>";
				}			
			}
			$res.=$this->getAuthor();
		$res.="</manual>";
		return $res;
	}
	
	public function getHtml(){
		$res="";
		if($this->respond_type==self::$RESPOND_DEFAULT){
			$res=$this->getHtmlDefault();
		}else if($this->respond_type==self::$RESPOND_SERVICE){
			$res=$this->getHtmlService();
		}
		return $res.$this->js();
	}
	
	public function js(){
		echo addJS("base-js/smis-base-fetreefy-json.js");
		?>
		<script type="text/javascript">
			$(document).ready(function(){
				$(".spjson").fetreefy();
			});
		</script>	
		<?php 
	}
	

}


?>