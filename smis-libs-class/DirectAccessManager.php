<?php 
/**
 * this class used to manage directory access
 * for direct acces in smis framework
 * 
 * 
 * @author      : Nurul Huda
 * @since       : 25 Mei 2017
 * @license     : Apache2
 * @database    : smis_adm_directory
 * @copyright   : goblooge@gmail.com
 * @version     : 1.0.0
 * */
class DirectAccessManager{
    /*@var DBTable*/
    protected $dbtable;
    /*@var DirectAccessManager*/
    private static $dma;
    protected $current;
    
    
    private function __construct(DBTable $dbtable){
        $this->dbtable  = $dbtable;
        $this->current  = array();
    }
    
    /**
     * @brief singleton model access
     * @param \Database $db 
     * @param \DBTable $dbtable 
     * @return  DirectAccessManager
     */
    public static function getIntance(Database $db, DBTable $dbtable=null){
        if(self::$dma==null){
            if($dbtable==null){
                $dbtable    = new DBTable($db,"smis_adm_directory");
            }
            self::$dma      = new DirectAccessManager($dbtable);
        }
        return self::$dma;
    }
    
    /**
     * @brief load the directory base
     *          on it's name
     * @param string $service 
     * @return  array
     */
    public function getDirectAccessSettings($service){
        $kriteria["service"]         = $service;
        $row                         = $this->dbtable->select($kriteria);
        $this->current               = array();
        $this->current['service']    = $row->service;
        $this->current['password']   = $row->password;
        $this->current['autonomous'] = $row->autonomous;
        return $this->current;
    }
    
    /**
     * @brief get current loaded service 
     * @return  string
     */
    public function getCurrentService(){
        return $this->current['service'];
    }
    
    /**
     * @brief get current loaded password 
     * @return  string
     */
    public function getCurrentPassword(){
        return $this->current['password'];
    }
    
    /**
     * @brief get current loaded autonomous 
     * @return  string
     */
    public function getCurrentAutonomous(){
        return $this->current['autonomous'];
    }
}

?>