<?php 
/**
 * 
 * this class provide a notification interface
 * so the programmer only need to create
 * a notification message using this class
 * 
 * @license LGPL v 2
 * @since 19 Feb 2015
 * @author goblooge
 * @copyright Nurul Huda <goblooge@gmail.com>
 */

class Notification{
	
	private $listnotif;
	private $db;
	public function __construct(Database $db){
		$this->listnotif	= array();
		$this->db			= $db;
	}
	
	/** add the notification  to system
	 *  @param $slug is string of the group of notification
	 *  @param $key is a md5 hash key that should be uniq to prevent double notif
	 *  @param $page is the user acces page that should get this notification
	 *  @param $action is the action that user shuould get this notifcation
	 *  @param $user is the spesific notification for single user
	*/
	public function addNotification($slug,$key,$message,$page="",$action="",$user=""){
		$a					= array();
		$a['slug']			= $slug;
		$a['passkey']		= $key;
		$a['message']		= $message;
		$a['page']			= $page;
		$a['action']		= $action;
		if($user=="" && $page!="" && $action!=""){
			$user			= getAllUserWithAccess($this->db,$page,$action);
		}
		$a['user']		   	= $user;
		$this->listnotif[] 	= $a;
	}
	
	public function commit(){
		$dbtable			= new DBTable($this->db, "smis_base_notification");
		foreach($this->listnotif as $key=>$data){
			$dbtable->insert($data);
		}
		$this->listnotif	= array();
	}
}
?>