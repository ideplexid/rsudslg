<?php 
/**
 * this class used for creating UI that contains parent and child 
 * so this system will always create a child parent data
 * @since 27 Dec 2015
 * @license LGPL v3
 * @copyright Nurul Huda <goblooge@gmail.com>
 * @author goblooge
 *
 */
require_once ("smis-framework/smis/database/DBParentChildResponder.php");
require_once "smis-libs-class/MasterTemplate.php";

class ChildParentTemplate extends MasterTemplate{
	
	protected $dbchild;
	protected $dbparent;
	protected $uitable_parent;
	protected $uitable_child;
	protected $adapter_parent;
	protected $adapter_child;
	protected $responder_parent;
	protected $responder_child;
	protected $allresponder;
	protected $modal_parent;
	protected $modal_child;
	protected $title_parent;
	protected $title_child;
	protected $link;
	
	public function __construct($db,$tblparent,$tblchild,$link,$page,$action){
		$this->dbchild=new DBTable($db, $tblchild);
		$this->dbparent=new DBTable($db, $tblparent);
		$this->uitable_child=new Table(array());
		$this->uitable_child->setName ( $action."_child" );
		$this->uitable_child->setActionName ( $action.".child" );
		$this->uitable_parent=new Table(array());
		$this->uitable_parent->setName ( $action."_parent" );
		$this->uitable_parent->setActionName ( $action.".parent" );
		$this->adapter_parent=new SimpleAdapter();
		$this->adapter_child=new SimpleAdapter();
		$this->link=$link;
		$this->modal_child=null;
		$this->modal_parent=null;
	}
	
	public function setModalTitle($title,$parent=true){
		if($parent) $this->title_parent=$title;
		else $this->title_child=$title;
		return $this;
	}
	
	public function phpPreLoad(){
		
	}
	
	public function setAdapter($adapter,$parent=true){
		if($parent) $this->adapter_parent=$adapter;
		else $this->adapter_child=$adapter;
		return $this;
	}
	
	public function getAdapter($parent=true){
		if($parent) return $this->adapter_parent;
		else return $this->adapter_child;
	}
	
	public function setUITable($uitable,$parent=true){
		if($parent) {
			$this->uitable_parent=$uitable;
			$this->uitable_parent->setName ( $action."_child" );
			$this->uitable_parent->setActionName ( $action.".child" );
		}else {
			$this->uitable_child=$uitable;
			$this->uitable_child->setName ( $action."_child" );
			$this->uitable_child->setActionName ( $action.".child" );
		}
		return $this;
	}
	
	public function getUITable($parent=true){
		if($parent) return $this->uitable_parent;
		else return $this->uitable_child;
	}
	
	public function getDBTable($parent=true){
		if($parent) return $this->dbparent;
		else return $this->dbchild;
	}
	
	public function getDBResponder($parent=true){
		if($parent) return $this->responder_parent;
		else return $this->responder_child;
	}
	
	public function setDBResponder($responder,$parent=true){
		if($parent) $this->responder_parent=$responder;
		else $this->responder_child=$responder;
		return $this;
	}
	
	public function getModal($renew=false,$parent=true){
		if($parent){
			if($this->modal_parent==null || $renew || !is_object($this->modal_parent)){
				$uitable=$this->getUItable($parent);
				$uitable->setModalSaveButtonEnable($this->is_modal_save_button_enable);
				$modal=$uitable->getModal();
				$this->modal_parent=$modal;
				$this->modal_parent->setTitle($this->title_parent);
			}
			return $this->modal_parent;
		}else{
			if($this->modal_child==null || $renew || !is_object($this->modal_child)){
				$uitable=$this->getUItable($parent);
				$uitable->setModalSaveButtonEnable($this->is_modal_save_button_enable);
				$modal=$uitable->getModal();
				$this->modal_child=$modal;
				$this->modal_child->setTitle($this->title_child);
			}
			return $this->modal_child;
		}
	}
	
	
	
}

?>