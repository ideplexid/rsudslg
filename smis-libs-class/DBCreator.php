<?php

/**
 * this class used for generating database sintax mode
 * so user can be easily manage the database
 * this class posibly adding new feature like repairing damage database
 * so the system the update will not fail just because the data is not exist
 * 
 * @version     : 2.1.2
 * @since       : 14 May 2017
 * @copyright   : goblooge@gmail.com
 * @author      : Nurul Huda
 * @license     : LGPLv3
 * */

class DBCreator{
    /*@var string*/
	private $dbname;
	/*@var Array*/
	private $column;
	/*@var Array*/
    private $unique;
    /*@var Array*/
    private $index;
    /*@var index non uniq*/
    private $index_nuniq;
    /*@var Array*/
	private $primary;
	/*@var string*/
	private $engine;
    /*@var DBController*/
	private $dbcontroller;
    /*@var Array*/
	private $query;
    /*@var bool*/
	private $is_duplicate;
    /*@var Array*/
	private $error_log;
    /*@var index*/
    private $db_index;
    /*@var index*/
    private $db_index_nuniq;
    /*@var boolean*/
    private $is_primary_non_id; //is as flag that the primary key is not column id
	/*@var boolean*/
    private $is_not_int_id;
    	
	public function __construct(DBController $dbcontroller,$dbname,$engine="MyISAM"){
		$this->dbcontroller         = $dbcontroller;
		$this->dbname               = $dbname;
		$this->column               = array();
        $this->unique               = array();
        $this->index                = array();
		$this->engine               = $engine;
        $this->primary              = array();
        $this->query                = array();
        $this->is_duplicate         = true;
        $this->defaultColumn();
        $this->db_index             = null;
        $this->db_index_nuniq       = array();
        $this->index_nuniq          = array();
        $this->is_primary_non_id    = false;
        $this->is_not_int_id        = false;
	}
    
    public function setIDNonInteger($enabled){
        $this->is_not_int_id        = $enabled;
    }

    public function setDuplicate($is_duplicate){
        $this->is_duplicate         = $is_duplicate;
        return $this;
    }
    
    public function toString(){
        $result      = "";
        $result     .= "<div>";
        $result     .= "<h5>".$this->dbname."</h5>";
        $result     .= "<pre>";
        foreach($this->query as $q){            
            $result .= $q;
            $result .= "\n\n";
        }
        $result     .= "</pre>";
        $result     .= "</div>";
        return $result;
    }
    
    /**
     * @brief this is default column and should not be override
     * @return  this
     */
    protected function defaultColumn(){
        if(!$this->is_not_int_id){
            $this   ->addColumn("id","int(11)",self::$SIGN_NONE,false,self::$DEFAULT_NULL,true,self::$ON_UPDATE_NONE,"")
                    ->addColumn("prop","varchar(10)",self::$SIGN_NONE,false,self::$DEFAULT_EMPTY,false,self::$ON_UPDATE_NONE,"")
                    ->addPrimaryKey("id");
        }else{
            $this   ->addColumn("prop","varchar(10)",self::$SIGN_NONE,false,self::$DEFAULT_EMPTY,false,self::$ON_UPDATE_NONE,"");
        }
        return $this;
    }
    
    private function addingDuplicateColumn(){
        if($this->is_duplicate){
            $this   ->addColumn("autonomous","text",self::$SIGN_NONE,false,self::$DEFAULT_NONE,false,self::$ON_UPDATE_NONE,"")
                    ->addColumn("duplicate","tinyint(1)",self::$SIGN_NONE,false,self::$DEFAULT_NONE,false,self::$ON_UPDATE_NONE,"")
                    ->addColumn("origin","varchar(64)",self::$SIGN_NONE,false,self::$DEFAULT_NONE,false,self::$ON_UPDATE_NONE,"")
                    ->addColumn("origin_id","int(11)",self::$SIGN_NONE,false,self::$DEFAULT_NONE,false,self::$ON_UPDATE_NONE,"")
                    ->addColumn("time_updated","datetime",self::$SIGN_NONE,false,self::$DEFAULT_NONE,false,self::$ON_UPDATE_NONE,"")
                    ->addColumn("origin_updated","varchar(64)",self::$SIGN_NONE,false,self::$DEFAULT_NONE,false,self::$ON_UPDATE_NONE,"");
        }
        return $this;
    }
    
    /**
     * @brief get all column info of a table
     *          complete including the type, key and etc.
     * @return  Array of column info
     */
	public function getDescription(){
		return $this->dbcontroller->get_full_col_info($this->dbname);
	}
    
    /** 
     *  @brief run the query
     *  @param string $q is the query system
     */
    public function query($q){
        $this->query[]  = $q;
        $this->dbcontroller->query($q);
        $error          = $this->dbcontroller->getLastError();
        if($error!=""){
            $this->putError($error,$q);
        }
    }
    
    /** 
     *  @brief add error log
     *  @param string $error is the query
     *  @param string $query is the query 
     */
    private function putError($error,$query){
        if($this->error_log==null){
            $this->error_log = array();
        }
        $this->error_log[]   = array("error"=>$error,"query"=>$query);
    }
    
    /**
     * @brief get all error log
     * @return  Array of Error Log
     */
    public function getError(){
        return $this->error_log;
    }
    
    /**
     * @brief get the error log
     * @return  
     */
    public function printErrorLog(){
        $result     = "";
        if($this->error_log!=null){
          $result   = "<h4>".$this->dbname."</h4>";
          $result  .= "<pre>\n";
          foreach($this->error_log as $x){
              $result   .= "############################## ERROR ###########################\n";
              $result   .= $x['error']."\n";
          }
          $result       .= "</pre>";   
        }
        return $result;
    }
    
	/**
	 * @brief get the table index information
	 * @return  Array of Index Info
	 */
    public function getIndex(){
        return $this->dbcontroller->get_index_info($this->dbname);
    }
    
    /**
     * @brief this system use to build indexing system based 
     *        on current existing table in database 
     */
    public function getBuildDBIndexInformation(){
        if($this->db_index==null){
            $index  = $this->getIndex();
            $result = array();
            foreach($index as $i){
                if($i['key']=="PRIMARY" && $i['column']!="id"){
                    $this->is_primary_non_id    = true;
                }
                if(!isset($result[$i['key']])){
                    $result[$i['key']]          = array();
                }
                $result[$i['key']][]            = $i['column'];
            }
            $this->db_index=$result;                
        }
        return $this->db_index;
    }
    
    /** 
     * @brief this used to build Index that in php file 
     */
    public function getBuildSystemIndexInformation(){
        $result                     = array();
        /**primary key */
        foreach($this->primary as $key=>$value){
            if(!isset($result['PRIMARY'])){
                $result['PRIMARY']  = array();
            }
            $result['PRIMARY'][]    = $value;
        }
        /**indexing and full text and uniq*/
        foreach($this->unique as $key=>$value){
            $result[$key]           = $value;
        }
        /**indexing and full text and non - uniq*/
        foreach($this->index as $key=>$value){
            $result[$key]           = $value;
        }
        return $result;
    }
    
    public function isIndexMatch($key,$system_index,$db_index){
        if(!isset($db_index[$key])){
            /**not exist */
            if($key=="PRIMARY"){
                return -2;
            }
            return -1;
        }
        $system_index_list  = $system_index[$key];
        $db_index_list      = $db_index[$key];
        
        if($key=="PRIMARY"){
           $primary_system  = $system_index_list[0];
           $primary_db      = $system_index_list[0];
           if($primary_system!=$primary_db){
               return -3;
           }
        }else{
            if(count($system_index_list)!=count($db_index_list)){
                /** if the total is not same, then return 0 */
                return 0;
            }            
            foreach($system_index_list as $x){
                /** cek apakah semua x sudah ada di bagian list */
                if(!in_array($x,$db_index_list)){
                    return 0;
                }
            }
            if( isset($this->db_index_nuniq[$key]) && !isset($this->index_nuniq[$key]) || !isset($this->db_index_nuniq[$key]) && isset($this->index_nuniq[$key]) ){
                /**tidak match satu uniq satu tidak unique */
                return 0;
            }
        }        
        return 1;
    }
    
    /**
     * @brief use to drop all index that no longer used in the system
     * @param Array $system_index all system index 
     * @param Array $db_index all db index
     * @return  null
     */
    private function indexDropper($system_index,$db_index){
        //index dropper
        $system = array_keys($system_index);
        $db     = array_keys($db_index);
        foreach($db as $key){
            if(!in_array($key,$system)){
                $query = " ALTER TABLE  `".$this->dbname."` DROP INDEX `".$key."`;";
               $this->query($query);
            }
        }
    }
    
    public function indexMatcher(){
        $system_index   = $this->getBuildSystemIndexInformation();
        $db_index       = $this->getBuildDBIndexInformation();        
        $this->indexDropper($system_index,$db_index);
        foreach($system_index as $key=>$set){
            $match      = $this->isIndexMatch($key,$system_index,$db_index);
            if($match==1){
                continue;
            }
                
            if($match==-1){
                //add index
                if(isset($this->index_nuniq[$key])){
                    $query   = " ALTER TABLE `".$this->dbname."` ADD INDEX ".$key." (`".(implode("`,`",$set))."`)"; 
                }else{
                    $query   = " ALTER TABLE `".$this->dbname."` ADD UNIQUE ".$key." (`".(implode("`,`",$set))."`)"; 
                }
                $this->query($query);
            }else if($match==-3){
                //add primary key
                $query  = " ALTER TABLE `".$this->dbname."` ADD PRIMARY KEY(`".$set[0]."`);";
                $this->query($query);
            }else if($match==-2 && $this->is_primary_non_id){     
                //drop primary key
                $query  = " ALTER TABLE `".$this->dbname."` DROP PRIMARY KEY;";
                $this->query($query);          
                //add primary key
                $query  = " ALTER TABLE `".$this->dbname."` ADD PRIMARY KEY(`".$set[0]."`);";
                $this->query($query);
            }else if($match==0){
                //drop the exist
                $query  = " ALTER TABLE `".$this->dbname."` DROP INDEX `".$key."`;";
                $this->query($query);

                //an recreate index
                if(isset($this->index_nuniq[$key])){
                    $query   = " ALTER TABLE `".$this->dbname."` ADD INDEX ".$key." (`".(implode("`,`",$set))."`)"; 
                }else{
                    $query   = " ALTER TABLE `".$this->dbname."` ADD UNIQUE ".$key." (`".(implode("`,`",$set))."`)"; 
                }
                $this->query($query);
            }
        }
    }
    
    /**
     * @brief set the database name
     * @param String $dbname 
     * @return  this;
     */
	public function setName($dbname){
		$this->dbname   = $dbname;
        return $this;
	}
	/**
	 * @brief set the engine that user wanted to
	 * @param String $engine name 
	 * @return  this;
	 */
	public function setEngine($engine){
		$this->engine   = $engine;
        return $this;
	}
    
    private function getColumnCreateSyntax($name, Array $settings){        
        $set[]   = $settings['type'];
        $set[]   = $settings['sign'];
        $set[]   = ($settings['nullable']) ? "":" NOT NULL ";        
        if($settings['default']==self::$DEFAULT_NULL){
           $set[]   = ($settings['default']==""?"":" DEFAULT NULL ");
        }else if(is_string($settings['default']) && $settings['default']!="" && !in_array($settings['default'],array(self::$DEFAULT_ZERO,self::$DEFAULT_EMPTY,self::$DEFAULT_NONE,self::$DEFAULT_CURRENT_TIMESTAMP))){
           $set[]   = ($settings['default']==""?"":" DEFAULT  '".$settings['default']."'");
        }else{
           $set[]   = ($settings['default']==""?"":" DEFAULT  ".$settings['default']); 
        }        
        $set[]   = ($settings['autoincrement']!==false?" AUTO_INCREMENT ":"");
        $set[]   = ($settings['onupdate']);
        $set[]   = ($settings['comment']==""?"":" COMMENT  '".$settings['comment']."'");        
        $result  = " `".$name."` ";
        $result .= " ".implode(" ",$set);
        return $result;
    }
    
    
    /**
     * @brief initialize database so it will match the system
     *        including adding column , change column and etc
     *        if table not exist then create, if not match then change 
     * @return  this;
     */
    public function initialize(){ 
        $this->addingDuplicateColumn();
        if(!$this->isExist()){
            $query  = $this->getCreator();
           $this->query($query);
            return $this;
        }        
        $db_set     = $this->getDescription();
        $system_set = $this->column;        
        /*drop column that not id but set as primary*/
        $this->getBuildDBIndexInformation();
        if($this->is_primary_non_id){
             $query2    = "ALTER TABLE `".$this->dbname."` DROP PRIMARY KEY;";
             $this->query($query2);
        }        
        $this->columnMatcher($system_set,$db_set);        
        $this->indexMatcher();
        echo $this->printErrorLog();
        return $this;
    }
    
    private function columnMatcher($system_set,$db_set){
        /*comparing in smis system table*/
        foreach($system_set as $column=>$settings){
            $comparation = $this->getColumnCompare($column,$settings);
            $match       = $this->is_match($comparation,$db_set[$column]);
            
            if($match==1){
                continue;
            }
            
            if($match==0){
                /**the column is wrong */
                $query  = $this->columnAlteration($column,$settings);
            }else if($match==-1){
                /**the column waas not present */
                $query  = $this->columnCreator($column,$settings);
            }

            if( ($column=="id" && !$this->is_not_int_id) || ($column=="id" && $match==-1) ){
                /** adding the primary key */
                $query .= ", ADD PRIMARY KEY (`".$column."`)";
            }
            
            if($db_set[$column]['key']=="PRI" && $column=="id" && $match==0 && !$this->is_not_int_id){
                /* drop index primary of id which it's not match 
                 * column that not id but set as primary*/  
                $query1  = "ALTER TABLE `".$this->dbname."` DROP PRIMARY KEY;";
                $this->query($query1);
            }
            $this->query($query);
        }
    }
    
    
    /**
     * @brief check is the data of column match or not
     * @param Array $set_system the column setting by system 
     * @param Array $set_db the column setting that exist in database
     * @return  1 if match, 0 if there something not match that need to alter, -1 if not exist that need to create
     */
    private function is_match($set_system,$set_db){
        if( $set_db==null ){
            return -1;
        }
        if( $set_db['field']   != $set_system['field'])        return 0;
        if( $set_db['type']    != $set_system['type'])         return 0;
        if( $set_db['null']    != $set_system['null'])         return 0;
        if( $set_db['default'] != $set_system['default'])      return 0;
        if( $set_db['extra']   != $set_system['extra'])        return 0;
        return 1;
    }
    
    /**
     * @brief get String query that used to create
     * @param String $name is the name column  
     * @param Array $settings is the Array of Settings
     * @return String $query; 
     */
    private function columnCreator($name, Array $settings){
        $column = $this->getColumnCreateSyntax($name,$settings);
        $query  = "ALTER TABLE `".$this->dbname."` ADD ".$column." FIRST ";
        return $query;
    }
    
    /**
     * @brief get String query that used to alter the un match column
     * @param String $name is the name column  
     * @param Array $settings is the Array of Settings
     * @return String $query; 
     */
    private function columnAlteration($name, Array $settings){
        $column = $this->getColumnCreateSyntax($name,$settings);
        $query  = "ALTER TABLE  `".$this->dbname."` CHANGE `".$name."`  ".$column;
        return $query;
    }
    
    /**
     * @brief create the column comparation array
     * @param String $name of the column 
     * @param Array $settings 
     * @return  Array $comparator;
     */
    private function getColumnCompare($name,$settings){        
        $comparator              = array();
        $comparator['field']     = $name;
        $comparator['type']      = $settings['type'];
        if($settings['sign']!=self::$SIGN_NONE){
            $comparator['type'] .= " ".$settings['sign'];
        }
        
        $comparator['null']      = $settings['nullable']!==false?"YES":"NO";
        $comparator['default']   = $settings['default']=="''"?"":$settings['default'];
        $comparator['extra']     = $settings['autoincrement']!==false?"auto_increment":"";
        if($comparator['extra']=="")
            $comparator['extra'] = $settings['onupdate']==self::$ON_UPDATE_CURRENT_TIMESTAMP?"on update CURRENT_TIMESTAMP":"";
        return $comparator;
    }
    
    /** get the whole query to create a table */
	public function getCreator(){
		$query          = "CREATE TABLE IF NOT EXISTS `".$this->dbname."` (";
		foreach($this->column as $name=>$settings){
            $return     = $this->getColumnCreateSyntax($name,$settings);
            if($name!="id"){
                $query .= ",";
            }
            $query.=$return;
		}        
        /**primary key */
        foreach($this->primary as $primary){
            $query      .= ",PRIMARY KEY (`".$primary."`)";
        }		
		/** unique key */
		foreach($this->unique as $name=>$set){
			$nameset     = "`".implode("`,`",$set)."`";
			$query      .= ",UNIQUE KEY $name (".$nameset.")";
        }   

        /** non-unique index */
		foreach($this->index as $name=>$set){
			$nameset     = "`".implode("`,`",$set)."`";
			$query      .= ",INDEX $name (".$nameset.")";
		}        
        /** engine*/
		$query          .= ") ENGINE=".$this->engine.";";
		return $query;
	}
    
    public function isExist(){
        return $this->dbcontroller->is_exist($this->dbname);
    }
    
	public function create(){
		$query = $this->getCreator();
		$this->wpdb->query($query);
	}
	
	/**
	 * @brief this push a column name from a table
	 * @param String $name the table name 
	 * @param String $type the data type of the table 
	 * @param String $sign the sign flag of the table 
	 * @param boolean $nullable is null or not 
	 * @param String $default the default value 
	 * @param boolean $autoincrement is autoincrement or not 
	 * @param String $onupdate adding on update syntax or not 
	 * @param String $comment just a comment 
	 * @return  $this;
	 */
	public function addColumn($name, $type, $sign, $nullable, $default, $autoincrement,$onupdate, $comment){
		$settings                   = array();
        $settings['type']           = $type;
        $settings['sign']           = $sign;
        $settings['nullable']       = $nullable;
        $settings['default']        = $default;
        $settings['autoincrement']  = $autoincrement;
        $settings['onupdate']       = $onupdate;
        $settings['comment']        = $comment;
        $this->column[$name]        = $settings;
        return $this;  
	}
    
    /**
     * @brief reorder column position 
     *        so the system will have good 
     *        position
     * @return  null
     */
    public function reorder(){
        $order              = array_keys($this->column);
        $previous           = null;
        foreach($order as $key){
            $order_syntax   = $this->getColumnCreateSyntax($key,$this->column[$key]);
            $query          = " ALTER TABLE `".$this->dbname."` MODIFY ".$order_syntax;
            if($previous==null){
              $query       .= " FIRST";  
            }else{
                $query     .= " AFTER ".$previous;                
            }
            $previous       = $key;
            $this->query($query);
        }
    }
	
	public function addUniqueKey($name,Array $set){
		$this->unique[$name] = $set;
        return $this;
    }
    
    public function addIndexKey($name,Array $set){
        $this->index[$name]       = $set;
        $this->index_nuniq[$name] = 1;
        return $this;
	}
    
    public function addPrimaryKey($column){
        $this->primary[]     = $column;
        return $this;
    }
       
    /**
     * @brief to reverse the current table into database
     *          so the programmer doesn't need to create himself
     * @param string $newline default  &#10; where it's mean new line in html
     * @return  string syntax of DBCreator 
     */
    public function reverseEngineer($newline="&#10;"){
        $result = 'require_once "smis-libs-class/DBCreator.php";'.$newline;
        /*preparing*/
        $engine = "";
        switch($this->engine){
            case self::$ENGINE_MYISAM       : $engine='DBCreator::$ENGINE_MYISAM'; break;
            case self::$ENGINE_INNODB       : $engine='DBCreator::$ENGINE_INNODB'; break;
            case self::$ENGINE_BLACKHOLE    : $engine='DBCreator::$ENGINE_BLACKHOLE'; break;
            case self::$ENGINE_CSV          : $engine='DBCreator::$ENGINE_CSV'; break;
            case self::$ENGINE_ARCHIVE      : $engine='DBCreator::$ENGINE_ARCHIVE'; break;
            case self::$ENGINE_MEMORY       : $engine='DBCreator::$ENGINE_MEMORY'; break;
            case self::$ENGINE_MRG_MYISAM   : $engine='DBCreator::$ENGINE_MRG_MYISAM'; break;
        }        
        $result .= '$dbcreator=new DBCreator($wpdb,"'.$this->dbname.'",'.$engine.');'.$newline;
        /*generating column*/
        $db_set      = $this->getDescription();
        $code_column = array("id","prop","autonomous","duplicate","origin","origin_id","time_updated","origin_updated");
        $x           = 0;
        foreach($db_set as $name=>$settings){
            if(in_array($name,$code_column)){
                $x++;
            }else{
                $result.=$this->getColumnReverse($name,$settings).$newline;
            }
        }
        
        /*generating index*/
        $index_grup = $this->dbcontroller->get_index_group($this->dbname);
        $index      = $index_grup['index'];
        $nuniq      = $index_grup['nuniq'];
        foreach($index as $x=>$y){
            if($x=="PRIMARY"){
                continue;
            }
            if(isset($nuniq[$x])){
                $result .= "\$dbcreator->addIndexKey('".$x."',array('".implode("','",$y)."'));".$newline;
            }else{
                $result .= "\$dbcreator->addUniqueKey('".$x."',array('".implode("','",$y)."'));".$newline;
            }
        }
        /*finishing*/
        if($x<8){
            $result .= '$dbcreator->setDuplicate(false);'.$newline;
        }
        $result     .= '$dbcreator->initialize();';
        return $result;
    }
    
    /** @brief  reverse engineering system 
     *          to make the php source code 
     *  @param string $name is the name of database
     *  @param Array $settings is the settings 
     * */
    private function getColumnReverse($name,$settings){        
        $type           = $settings['type'];
        $pos            = strpos($type,"unsigned");
        if($pos!==false){
            $sign       = 'DBCreator::$SIGN_SIGN';
            $type       = substr($type,0,$pos);
        }else{ 
            $sign       = 'DBCreator::$SIGN_NONE';
        } 
        $nullable       = $settings['null']=="NO"?'false':'true';
        $default        = $settings['default'];
        if($default==self::$DEFAULT_CURRENT_TIMESTAMP){
            $default    = 'DBCreator::$DEFAULT_CURRENT_TIMESTAMP';
        }else if($default==self::$DEFAULT_ZERO){
            $default    = 'DBCreator::$DEFAULT_ZERO';
        }else{
            $default    = 'DBCreator::$DEFAULT_NONE';
        }
        $extra          = $settings['extra'];
        $autoincrement  = $extra=="auto_increment"?'true':'false';
        $onupdate       = strtoupper($extra)==self::$ON_UPDATE_CURRENT_TIMESTAMP?'DBCreator::$ON_UPDATE_CURRENT_TIMESTAMP':'DBCreator::$ON_UPDATE_NONE';
        $result        .= '$dbcreator->addColumn("'.$name.'", "'.$type.'", '.$sign.', '.$nullable.', '.$default.', '.$autoincrement.','.$onupdate.', "");';
        return $result;
    }
    
    /**
     * @brief this function to repair the duplicating process
     *          on smis table that not yet have duplicate system
     * @return  string error
     */
    public function repairTableSynch(){
        $this->setDuplicate(true);
        $this->addingDuplicateColumn();
        if($this->isExist()){
            $db_set      = $this->getDescription();
            $system_set  = $this->column;        
            /*drop column that not id but set as primary*/
            $this->getBuildDBIndexInformation();
            if($this->is_primary_non_id){
                 $query2 = "ALTER TABLE `".$this->dbname."` DROP PRIMARY KEY;";
                 $this->query($query2);
            }        
            $this->columnMatcher($system_set,$db_set); 
        }else{
            $this->putError("Table <strong>".$this->dbname."</strong> Not Exist !!!","");
        }
        return $this->printErrorLog();
    }
    
    public static $ENGINE_MEMORY                = "MEMORY";
    public static $ENGINE_MRG_MYISAM            = "MRG_MYISAM";
    public static $ENGINE_INNODB                = "InnoDB";

    public static $ENGINE_BLACKHOLE             = "BLACKHOLE";
    public static $ENGINE_CSV                   = "CSV";
    public static $ENGINE_MYISAM                = "MyISAM";
    public static $ENGINE_ARCHIVE               = "ARCHIVE";

    public static $DEFAULT_ZERO                 = "0";
    public static $DEFAULT_NULL                 = NULL;
    public static $DEFAULT_NONE                 = "";
    public static $DEFAULT_EMPTY                = "''";
    public static $DEFAULT_CURRENT_TIMESTAMP    ="CURRENT_TIMESTAMP";
    
    public static $ON_UPDATE_CURRENT_TIMESTAMP  = " ON UPDATE CURRENT_TIMESTAMP";
    public static $ON_UPDATE_NONE               = "";
    
	public static $SIGN_NONE                    = " ";
    public static $SIGN_SIGN                    = " sign ";
    public static $SIGN_ZEROFILL                = " sign ZEROFILL ";
}