<?php
/**
 * all db responder response should be a JSON
 * @author goblooge
 *
 */

class DBParentResponder extends DBResponder{}
class DBChildResponder extends DBResponder{
	protected $parent_key;
	public function __construct($dbtable,$table,$adapter,$id_parent){
		parent::__construct($dbtable,$table,$adapter);
		$this->id_parent=$id_parent;
	}
	
	public function view(){
		$parent_key=$_POST['id_parent'];
		$this->dbtable->addCustomKriteria($this->id_parent."='",$parent_key."'");
		return parent::view();
	}
}


class DBParentChildResponder{
	protected $parent_responder;
	protected $child_responder;
	
	function __construct($parent,$child){
		$this->parent_responder=$parent;
		$this->child_responder=$child;	
	}
	
	function setResponderName($name){
		$this->parent_responder->getUITable()->setName($name.".parent");
		$this->children_responder->getUITable()->setName($name.".children");
	}
	
	
	public function setDebuggable($debug){
		$this->parent_responder->setDebuggable($debug);
		$this->child_responder->setDebuggable($debug);
	}
	
	public function setUseAdapterForSelect($use){
		$this->parent_responder->setUseAdapterForSelect($use);
		$this->child_responder->setUseAdapterForSelect($use);
	}
	
	protected function getDBTable($dbtable="parent"){
		if($dbtable=="parent"){
			return $this->parent_responder->getDBTable();
		}
		return $this->child_responder->getDBTable();
	}
	
	public function isParentProcess($super_command){
		return $super_command!="smis_children";
	}
	
	public function isChildProcess($super_command){
		return $super_command=="smis_children";
	}
	
	public function command($super_command,$command){
		if($super_command=="smis_children"){
			return $this->child_responder->command($command);
		}else{
			return $this->parent_responder->command($command);
		}
	}
}

?>