<?php
	class DBConnector{
		private $db_server;
		private $db_name;
		private $db_password;
		private $db_user;
		private static $MODE;
		private static $MYSQLI_LINK;
		private $error;
		
		private static $MYSQL_MODE="mysql";
		private static $MYSQLI_MODE="mysqli";
		private static $PDO_MODE="pdo";
		
		public function __construct($server,$name,$user,$password){
			$this->db_server	= $server;
			$this->db_name		= $name;
			$this->db_password	= $password;
			$this->db_user		= $user;
			self::$MODE			= $this->getMode();
		}
        
        /**
         * @brief preset the set mode of the mysql
         * @return  null
         */
        public function presetSQLMode(){
            if(self::$MODE==self::$MYSQL_MODE){
                mysql_query("SET SESSION sql_mode = ''");
            }else if(self::$MODE==self::$MYSQL_MODE){
                mysqli_query(self::$MYSQLI_LINK,"SET SESSION sql_mode = ''");
            }
        }
		
		public function getError(){
			return $this->error;
		}
		
		public static function getMysqliLink(){
			return self::$MYSQLI_LINK;
		}
		
		public function getMode(){
			if(defined("SMIS_PHP_MODE"))
				return SMIS_PHP_MODE;
			else return self::$MYSQLI_MODE;
		}
        
        public function isSetMode(){
            if(defined("SMIS_SQL_MODE"))
				return SMIS_SQL_MODE==1;
			return false;
        }
		
		public function connect(){
			try{
				$link			= false;
				if(self::$MODE==self::$MYSQL_MODE) {
					$link		= mysql_connect($this->db_server,$this->db_user,$this->db_password);
					if(!$link){ 
						return null; 
					}
					$res		= mysql_select_db($this->db_name);
					return $res;
				}else if(self::$MODE==self::$MYSQLI_MODE) {
					$link		= mysqli_connect($this->db_server,$this->db_user,$this->db_password,$this->db_name);
					if(!$link || mysqli_connect_errno($link)){
						$error 	= "MySQL error ".mysqli_errno($link).": ".mysqli_error($link)."\n<br>When executing:<br><font class='diff'>$query</font><br>";
						return null;
					}
					self::$MYSQLI_LINK = $link;
					return true;
				}
                if($this->isSetMode()){
                    $this->presetSQLMode();
                }
			}catch(Exception $e){
				return null;
			}
		}
		
		public static function close(){
			try{
				if(self::$MODE==self::$MYSQL_MODE){
					mysql_close();
				}
				if(self::$MODE==self::$MYSQLI_MODE){
					mysqli_close(self::$MYSQLI_LINK);
				}
			}catch(Exception $e){
				return null;
			}
		}
		
	}
	
	
	
	
?>