<?php

/**
 * this class represent Model in MVC of Safethree.
 * this class handling communication beetween 
 * DBResponder and Database. it also provide several
 * function that DBResponder Need to.
 * 
 * @author Nurul Huda
 * @version 5.12
 * @since 1 Januari 2014
 * @license LGPL v2
 * @copyright Nurul Huda <goblooge@gmail.com>
 */
class DBTable extends MySQLTable{
	/*@var Database*/
	protected $db;
	public static $OBJECT_FETCH = "object";
	public static $ARRAY_FETCH  = "array";
	public static $ASSOC_FETCH  = "assoc";
	protected $fetch;
    
	/**
	 * @param Database $db
	 * @param Table name $name
	 * @param array of column $column
	 */
	public function __construct(Database $db,$name,$column=NULL){
        $this->db           = $db;
        parent::__construct($name,$column);        
		$this->inserted_id  = -1;
        $this->fetch        = self::$OBJECT_FETCH;
	}
    
     public function setFetchMethode($fetch){
		$this->fetch=$fetch;
		return $this;
	}
    
    /**
	 * get the next ID of the table
	 * @return unknown
	 */
	public function getNextID(){
		$query="SHOW TABLE STATUS LIKE '".$this->name."'";
		$row=$this->db->get_row($query,false);
		$nextId = $row['Auto_increment'];
		return $nextId;
	}
	
	public function get_db(){
		return $this->db;
	}
	
	public function get_default_column($name=null){
        if($name==null){
            $name=$this->name;
        }
        $column = $this->db->get_col_info($name);
		return $column;
	}
		
	public function escaped_string($string){
		return $this->db->escaped_string($string);
	}
	
	public function delete($id){	
		$before=json_encode($this->select($id, false));
		$after='';
		$action='purge';
		$this->db->log($this->name,$action, $before, $after,$id);
		
		$query="DELETE FROM ".$this->name." WHERE id='".$id."'";
		$this->db->query($query);
	}
	
	/**
	 * get new inserted autoincrement id of Table in Database
	 * @return int number;
	 */
	public function get_inserted_id(){
		return $this->inserted_id;
	}
	
	/**
	 * Facade Function for inserting data to Database
	 * - insert the data into database
	 * - get the new inserted id 
	 * - save to the log action that perform
	 * - return the inserted proses 
	 * @param array $data that want to be inserted
     * @param $user_prop is boolean if the data need have the prop
     * @param $warp if need e data set that mysql function
	 * @return status of inserting process
	 */
	public function insert($data,$use_prop=true,$warp=null){
		$before="";
		$result= $this->db->insert($this->name,$data,$use_prop,$warp);
        $this->getLastQueryError();
		$this->inserted_id=$this->db->get_inserted_id();
		$after=json_encode($this->select($this->inserted_id, false,$use_prop));
		$action='insert';
		$this->db->log($this->name,$action, $before, $after,$this->inserted_id);
		return $result;
	}
	
	/**
	 * Facade function that update a record in database
	 * if id is array then it will be a several kriteria.
	 * if di is string then it will represent the id in database table
	 * what this function do is :
	 * - make sure that view for select is set to false (backing up this first)
	 * - save the current state of data in $before variable
	 * - update the data in Database
	 * - save the new update data state in $after variable
	 * - check is the peform action is update or delete 
	 *   (delete in this system is update the content of prop field into 'del' )
	 * - save the state ($before and $after) into log Action Table
	 * - return the result.
	 * @param array $data that want to update
	 * @param array or string $id that as the kriteria of data want to update
     * @param array $warp is the format of data need to extracted, as string or as code
	 * @return boolean $result that tell the update succes or failure
	 */
	public function update($data,$id,$warp=null){
		$b_fetch=$this->fetch;
		$this->fetch=self::$OBJECT_FETCH;
		$is_view_for_select=$this->is_view_for_select;
		$this->is_view_for_select=false;
		
		
		$before=$this->selectEventDel($id, false);
		$result=$this->db->update($this->name,$data,$id,$warp);
        $this->getLastQueryError();
		$after=$this->selectEventDel($id, false);
		$this->is_view_for_select=$is_view_for_select;
		$action='update';
		
		if($before->prop=='' && $after->prop=='del')
			$action='delete';
		$DBID="";
		if(is_array($id) && isset($id['id']) && $id['id']!="" ){
			$DBID=$id['id'];
			$this->db->log($this->name,$action, json_encode($before), json_encode($after),$DBID);
		}else{
			/*TODO STUFF - something shit happen here, because many data that need to be log*/
			$DBID="DBID-".json_encode($id);
		}
		$this->fetch=$b_fetch;
		return $result;
	}
	
	
    
    /**
     * @brief this function using to update the system based on the custom kriteria that given by user
     * @param Array $update data 
     * @param String $kriteria that given by user 
     * @param Array $warp how to update data 
     * @return  
     */
    public function updateByCustomKriteria($update,$kriteria,$warp=null){
       return $this->db->updateByCustomKriteria($this->name,$update,$kriteria,$warp);
    }
	
	
	/**
	 * this action is actually update or insert but 
	 * this function calling only in restoring data based 
	 * on smis_adm_log table where every action and state of other table was store
	 * @param array $data
	 * @param array $id
	 * @return boolean status of restoring.
	 */
	public function restore($data,$id){
		$before="";
		$after="";
		$DBID="";
		if($this->is_exist($id['id'],true)){
			if($data==null){	
				$data=array("prop"=>"del");
			}
			$before=json_encode($this->selectEventDel($id['id'], false));
			$result=$this->db->update($this->name, $data,$id);
            $this->getLastQueryError();
			$after=json_encode($this->selectEventDel($id['id'], false));
			$DBID=$id['id'];
		}else if($data!=null){
			$result=$this->db->insert($this->name, $data);
            $this->getLastQueryError();
			$this->inserted_id=$this->db->get_inserted_id();
			$after=json_encode($this->selectEventDel($this->inserted_id, false));
			$DBID=$this->inserted_id;
		}		
		$action='restore';
		$this->db->log($this->name,$action, $before, $after,$DBID);
		return $result;
	}
	

	/**
	 * check if the current data exist in database
	 * if the id is array then it could be several kriteria of the field in database
	 * if the id is a string then this it represent as id in database
	 * if include_del true, then seaching will including deleted (prop) row
	 * @param string or array $id
	 * @param boolean $include_del
	 * @return boolean true if exist in database, otherwise false
	 */
	public function is_exist($id,$include_del=false){
		if(is_array($id)){
			$where=" WHERE 1 ";
			foreach($id as $n=>$v){
				$where.=" AND ".$n."=\"".$v."\"";
			}
			if(!$include_del)
				$where.=" AND prop!='del' ";
			$query="SELECT count(*) FROM ".$this->name.$where;
			return $this->get_var($query)!="0";
		}else{
			$query="SELECT count(*) as total FROM ".$this->name." WHERE id='".$id."'";
			if(!$include_del)
				$query.=" AND prop!='del'";
			return $this->get_var($query)!="0";
		}
	}
    
    public function view($term,$page){
		$dpage=$page;
		$total=$this->count($term);
		$max_page=ceil($total/$this->maximum);
		if($page>=$max_page && $page!=0) $page=$max_page-1;			
		$start=$page*$this->maximum;
		$q=$this->getQueryView($term, $start);
		$mode=$q['mode'];
		$query=$q['query'];		
		$data=$this->get_result($query);
		if($this->debug) {
			$pack['query']=$query;
			$pack['mode']=$mode;
			$pack['showall']=$this->showall;
			$pack['page_post']=$dpage;				
			$pack['limit']=$start.",".$this->maximum;
			$pack['asal_maxpost']=$total." / ".$this->maximum;
			$pack['custom_kriteria']=$this->getCustomKriteria();
		}
		$pack['page']=$page;
		$pack['data']=$data;
		$pack['max_page']=$max_page;		
		return $pack;
	}
	
	public function query($query){
		return $this->db->query($query);
	}
	
	public function get_result($query){	
		$is_object=true;
		if($this->fetch==self::$ARRAY_FETCH){
			$is_object=false;
		}
		return $this->db->get_result($query,$is_object);
	}
	
	public function get_row($query){
		$is_object=true;
		if($this->fetch==self::$ARRAY_FETCH){
			$is_object=false;
		}
		return $this->db->get_row($query,$is_object);
	}
	
	public function get_var($query){
		return $this->db->get_var($query);
	}
    
    public function getLastQueryError(){
        $this->last_error=$this->db->getLastError();
        $this->last_query=$this->db->getLastQuery();
        return $this;
    }
}
?>