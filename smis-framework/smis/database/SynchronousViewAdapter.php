<?php 
/**
 * handling synchronizing data
 * the only different is it will take the sync
 * data as primary data
 * 
 * @author : Nurul Huda
 * @license : LGPLv3
 * @copyright : goblooge@gmail.com
 * @since : 09-Marc-2017
 * @version : 1.0.0
 * 
 * */
class SynchronousViewAdapter extends SimpleAdapter{
    
    public function __construct($use_number=false,$number_name=""){
        parent::__construct($use_number,$number_name);
        $this->add("synch","synch");
        $this->add("duplicate","duplicate");
    }
    
}

?>