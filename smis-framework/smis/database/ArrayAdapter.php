<?php

/**
 * this class used to adapt data from database 
 * that wanted to show to user 
 * 
 * @author : Nurul Huda
 * @since : 12 Januari 2014
 * @version : 1.0.0
 * @license : Apache2
 * @copyright : goblooge@gmail.com
 * */
abstract class ArrayAdapter{
	
	/* @var int*/
	protected $number;
	/* @var boolean*/
	protected $is_use_remove_zero;
	/* @var Array*/
	protected $array_remove_zero;
	
	public function __construct(){
		$this->number=0;
		$this->is_use_remove_zero=false;
		$this->array_remove_zero=null;
	}
	
    /**
     * @brief when enabled the zero value would remove and change to empty string
     * @param boolean $enabled 
     * @param array $array, the column that wanted to affect, null will determine as all column
     * @return  this
     */
	public function setRemoveZeroEnable($enabled,$array=null){
		$this->is_use_remove_zero=$enabled;
		$this->array_remove_zero=$array;
		return $this;
	}
	
    /**
     * @brief set the begining number for numbering the data
     * @param int $number is the start number 
     * @return  this
     */
	public function setNumber($number){
		$this->number=$number;
		return $this;
	}
	
    /**
     * @brief process to remove zero value
     * @param array $gdata 
     * @return  array that zero value was remove in certain column
     */
	public function removeZero($gdata){
		$hasil=array();
		foreach($gdata as $onedata){
			$one_hasil=array();
			if($this->array_remove_zero==null){
				foreach($onedata as $name=>$value){
					if($value=="0" || (is_int($value) && $value==0))
						$one_hasil[$name]="";
					else
						$one_hasil[$name]=$value;
				}
			}else{
				foreach($this->array_remove_zero as $name){
					if($value=="0" || (is_int($value) && $value==0)) 
						$one_hasil[$name]="";
					else 
						$one_hasil[$name]=$value;
				}
			}
			$hasil[]=$one_hasil;
		}
		return $hasil;
	}
	
	/**
	 * @brief the adaptation process
	 * @param array/object $onerow 
	 * @return  array
	 */
	abstract public function adapt($onerow);
	
    public static function moneyFormat($tag,$value){
        if(strpos($tag,"only-money")!==false){
			$currency=str_replace("only-money","",$tag);
			return $currency." ".number_format($value,2,',','.');
		}else if(strpos($tag,"non-leading-money")!==false){
			$currency=str_replace("non-leading-money","",$tag);
			return $currency." ".number_format($value,0,',','.');
		}else if(strpos($tag,"nonzero-money")!==false){
			if($value==NULL || $value=="") return "";
			$currency=str_replace("nonzero-money","",$tag);
			if($value*1!=0) return "<div class='left money-currency'>".$currency."</div><div class='right money-value'>".number_format($value,2,',','.')."</div>";
			else return "";
		}else if(strpos($tag,"zero-money")!==false){
			$currency=str_replace("zero-money","",$tag);
			return "<div class='left money-currency'>".$currency."</div><div class='right money-value'>".number_format($value,2,',','.')."</div>";
		}else if(strpos($tag,"money")!==false){
			if($value==NULL || $value=="") return "";
			$currency=str_replace("money","",$tag);
			return "<div class='left money-currency'>".$currency."</div><div class='right money-value'>".number_format($value,2,',','.')."</div>";
		}
    }
    
    public static function dateFormat($tag,$value){
        if(strpos($tag,"mini-date")!==false && $value!="0000-00-00" && $value!="0000-00-00 00:00:00" && $value!="00:00:00" && $value!=""  ){
			loadLibrary("smis-libs-function-time");
            $phpdate = strtotime( $value );
			$form=str_replace("mini-date ","",$tag);
			$mysqldate = date($form,$phpdate);
			return id_date_format($mysqldate,true);
		}else if(strpos($tag,"date")!==false && $value!="0000-00-00" && $value!="0000-00-00 00:00:00" && $value!="00:00:00" && $value!="" ){
			loadLibrary("smis-libs-function-time");
            $phpdate = strtotime( $value );
			$form=str_replace("date ","",$tag);
			$mysqldate = date($form,$phpdate);
			return id_date_format($mysqldate);
		}else if(strpos($tag,"romdate")!==false && $value!="0000-00-00" && $value!="0000-00-00 00:00:00" && $value!="00:00:00" && $value!=""){
			loadLibrary("smis-libs-function-time");
            $phpdate = strtotime( $value );
			$form=str_replace("romdate ","",$tag);
			$mysqldate = date($form,$phpdate);
			return rom_date_format($mysqldate);
		}
    }
    
    public static function arrayFormat($tag,$value){
        if(strpos($tag,"array-money")!==false){
			$currency=str_replace("array-money","",$tag);
			$number=0;
			$result="";
			foreach($value as $k=>$v){
				$result.="<div class='strip ".($number==0?'strip-odd':'strip-even')." '>".$k." : ".$currency." ".number_format($v,2,',','.')."</div>";
				$number++;
				$number=$number%2;
			}
			return $result;
		}else if($tag=="array"){
			$val=$value;
			if($val==null || $val=="" || !is_array($val)) return "";
			$result="";
			$number=0;
			foreach($val as $k=>$v){
				if(is_array($v) ){
					$v=json_encode($v);
				}
				$result.="<div class='strip ".($number==0?'strip-odd':'strip-even')."'>".$k." : ".$v."</div>";
				$number++;
				$number=$number%2;
			}
			return $result;
		}else if($tag=="array-bold"){
			$val=$value;
			if($val==null || $val=="" || !is_array($val)) return "";
			$result="";
			$number=0;
			foreach($val as $k=>$v){
				if(is_array($v) ){
					$v=json_encode($v);
				}
				$result.="<div class='strip ".($number==0?'strip-odd':'strip-even')." '><strong>".$k."</strong> : ".$v."</div>";
				$number++;
				$number=$number%2;
			}
			return $result;
		}else if($tag=="array-bold-trim"){
			$val=$value;
			if($val==null || $val=="" || !is_array($val)) return "";
			$result="";
			$number=0;
			foreach($val as $k=>$v){
				if(is_array($v) ){
					$v=json_encode($v);
				}
				$result.="<div class='strip ".($number==0?'strip-odd':'strip-even')." '><strong class='array-bold-trim'>".$k."</strong> : ".$v."</div>";
				$number++;
				$number=$number%2;
			}
			return $result;
		}else if($tag=="array-val"){
			$val=$value;
			if($val==null || $val=="" || !is_array($val)) return "";
			$result="";
			$number=0;
			foreach($val as $k=>$v){
				if(is_array($v) ){
					$v=json_encode($v);
				}
				$result.="<div class='strip ".($number==0?'strip-odd':'strip-even')." '>".$v."</div>";
				$number++;
				$number=$number%2;
			}
			return $result;
		}
    }
    
    public static function slugFormat($tag,$value){
        if($tag=='unslug'){
			return strtoupper(str_replace("_", " ", $value));
		}else if($tag=='slug'){
			$str=str_replace("-", "_", $value);
			$str=str_replace(" ", "_", $value);
			$str=strtolower($str);
			return $str;
		}else if($tag=='unslug-ucword'){
			return ucwords(str_replace("_", " ", $value));
		}else if($tag=='clean-slug'){
			$str=str_replace("-", "_", $value);
			$str=str_replace(" ", "_", $str);
			$str=strtolower($str);
			return $str;
		}else if($tag=='clean-unslug'){
			$str=str_replace("-", " ", $value);
			$str=str_replace("_", " ", $str);
			$str=ucwords($str);
			return $str;
		}
    }
    
    public static function textFormat($tag,$value){
        if($tag=='text-left'){
			return "<div class='left'>".$value."</div>";
		}else if($tag=='text-center'){
			return "<div class='center'>".$value."</div>";
		}else if($tag=='text-right'){
			return "<div class='right'>".$value."</div>";
		}
    }
    
    public static function jsonFormat($tag,$value){
        if($tag=="json"){
			$val=json_decode($value,true);
			if($val==null || $val=="" || !is_array($val)) return "";
			$result="";
			$number=0;
			foreach($val as $k=>$v){
				if(is_array($v) ){
					$v=json_encode($v);
				}
				$result.="<div class='strip ".($number==0?'strip-odd':'strip-even')." '>".$k." : ".$v."</div>";
				$number++;
				$number=$number%2;
			}
			return $result;
		}else if($tag=="json-val"){
			$val=json_decode($value,true);
			if($val==null || $val=="" || !is_array($val)) return "";
			$result="";
			$number=0;
			foreach($val as $k=>$v){
				if(is_array($v) ){
					$v=json_encode($v);
				}
				$result.="<div class='strip ".($number==0?'strip-odd':'strip-even')." '>".$v."</div>";
				$number++;
				$number=$number%2;
			}
			return $result;
		}
    }
    
    public static function digitFormat($tag,$value){
        if(is_numeric($value)){
            if(strpos($tag,"only-digit")!==false){
                $num=str_replace("only-digit","",$tag);
                $val=sprintf("%0".$num."d",$value);
                return $val;
            }else if(strpos($tag,"digit")!==false){
                $num=str_replace("digit","",$tag);
                $val=sprintf("%0".$num."d",$value);
                $val="<div class='left'>".$val."</div>";
                return $val;
            }
        }
        return $val;
    }
    
    public static function imageFormat($tag,$value){
        if(strpos($tag,"image-single-style")!==false ){
			if($value=="") return;
			try{
				$style=str_replace("image-single-style-", "", $tag);
				$src=get_fileurl($value);
				$image="<img style='".$style."' class='smis_img_style' src='".$src."' onclick=\"showImage('".$src."')\">";
				return $image;
			}catch(Exception $e){
				return "";
			}
		}else if(strpos($tag,"image-single-class")!==false ){
			if($value=="") return;
			try{
				$class=str_replace("image-single-style", "", $tag);
				$src=get_fileurl($value);
				$image="<img class='".$class."' src='".$src."' onclick=\"showImage('".$src."')\">";
				return $image;
			}catch(Exception $e){
				return "";
			}
		}
    }
    
    public static function fileFormat($tag,$value){
        if(strpos($tag,"file-image-show")!==false ){
			if($value=="") return;
			try{
                $src=get_fileurl($value);
				$image.="<kbd><a href='#' onclick=\"showImage('".$src."')\" >Image #1"."</a></kbd><br/>";
				return $image;
			}catch(Exception $e){
				return "";
			}
		}else if(strpos($tag,"files-image-show")!==false ){
			if($value=="") return;
			try{
				$image="";
				$json=json_decode($value);
				$num=1;
				$total=count($json);
				foreach($json as $one){
					$src=get_fileurl($one);
					if($num!=$total) $image.="<kbd><a href='#' onclick=\"showImage('".$src."')\" >File #".$num."</a></kbd><br/>";
					else $image.="<kbd><a href='#' onclick=\"showImage('".$src."')\" >File #".$num."</a></kbd>";
					$num++;
				}
				return $image;
			}catch(Exception $e){
				return "";
			}
		}
    }
    
    /**
     * @brief formatting process of raw data like adding 
     * Rp. adding money formatting, change the color
     * @param string $tag is the format model that wanted to 
     * @param mixed $value is the real data value 
     * @return  string formatted value
     */
	public static function format($tag,$value){
        if($tag==null) return $value;
		
		if(strpos($tag,"text")!==false){			
			return self::textFormat($tag,$value);
		}else if(strpos($tag,"slug")!==false){			
			return self::slugFormat($tag,$value);
		}else if(strpos($tag,"array")!==false){			
			return self::arrayFormat($tag,$value);
		}else if(strpos($tag,"money")!==false){			
			return self::moneyFormat($tag,$value);
		}else if(strpos($tag,"date")!==false){
        	return self::dateFormat($tag,$value);
        }else if(strpos($tag,"json")!==false){			
			return self::jsonFormat($tag,$value);
		}else if(strpos($tag,"file")!==false){			
			return self::fileFormat($tag,$value);
		}else if(strpos($tag,"digit")!==false){			
			return self::digitFormat($tag,$value);
		}else if(strpos($tag,"image")!==false){			
			return self::imageFormat($tag,$value);
		}else if(startsWith($tag, "front")){
			$b=str_replace("front","", $tag);
			return $b.$value;
		}else if(startsWith($tag, "back")){
			$b=str_replace("back","", $tag);
			return $value.$b;
		}else if($tag=='unixtimestamp'){
			return strtotime($value);
		}else if($tag=='microsecond'){
			return strtotime($value)."000";
		}else if($tag=='fix'){
			return $value;
		}else if($tag=='strtolower'){
			return strtolower($value);
		}else if($tag=='strtoupper'){
			return strtoupper($value);
		}else if($tag=='ucfirst'){
			return ucfirst($value);
		}else if($tag=='ucword'){
			return ucwords($value);
		}else  if($tag=='number'){
			return "<div class='right'>".number_format($value,0,',','.')."</div>";
		}else if(strpos($tag,"unit")!==false){
			$unit=str_replace("unit","",$tag);
			return "<div class='right'>".number_format($value,0,',','.')." ".$unit."</div>";
		}else if(strpos($tag,"trivial")!==false){
			$tg=str_replace("trivial_","",$tag);
			$th_tag=explode("_", $tg);
            $total_data = count($th_tag);
            for($i=0;$i<$total_data-2;$i=$i+2){
                if($value==$th_tag[$i]){
                    return $th_tag[$i+1];
                }
            }
            return $th_tag[$total_data-1];
		}else if(strpos($tag,"exception")!==false){
			$tg=str_replace("exception_","",$tag);
			$th_tag=explode("_", $tg);
			$compare=$th_tag[0];
			$true=$th_tag[1];
			$return=$value==$compare?$true:$value;
			return $return;
		}else if(strpos($tag,"empty")!==false){
			$tg=str_replace("empty","",$tag);
			$return=$value==""?$tg:$value;			
			return $return;
		}else if(strpos($tag,"substr")!==false){
            $num=str_replace("substr","",$tag);
			$sp=explode("-",$num);
			$from=$sp[0]*1;
			$length=$sp[1]*1;
			$val=substr($value, $from,$length);
            return $val;
		}else if(strpos($tag,"case")!==false){
			$pipe=explode("|", $tag);
			$total=count($pipe);
			for($i=1;$i<$total;){
				if($i==$total-1){
					return $pipe[$i];
				}
				$ekspresi=explode("-",$pipe[$i]);
				$kondisi=$ekspresi[0];
				$hasil=$ekspresi[1];
				if($value==$kondisi) 
					return $hasil;
				$i++;
			}
		}else if(strpos($tag,"function")!==false){
			$fungsi=str_replace("function-", "", $tag);
			return call_user_func($fungsi, $value);
		}else if(strpos($tag,"numbertell")!==false){
			$satuan=str_replace("numbertell-", "", $tag);
			loadLibrary("smis-libs-function-math");
			return numbertell($value)." ".$satuan;
		}else if($tag=="month-id"){
            loadLibrary("smis-libs-function-time");
			return enid_month($value);
		}else if($tag=="download"){
			return "<a href='javascript:;' class='btn btn-primary' onclick=\"download_file('".$value."');\"><i class='fa fa-download'></i></a>";
		}	
		return "";
	}
	
    /**
     * @brief this function used to compare a two set of
     *          data in array then show the result comparation
     *          into formatted and colored array
     * @param array $original 
     * @param array $eddited 
     * @param boolean $showall 
     * @return  array that show the formatted /colored different data
     */
	public function compare($original,$eddited,$showall=true){
		$ori=json_decode($original,true);
		$edt=json_decode($eddited,true);
			
		if($edt==null || $edt=="" || !is_array($edt)) 
            return "";
		
		$rsl=array();
		foreach($edt as $k=>$edt_v){
			$ori_v=$ori[$k];			
			if($edt_v!=$ori_v){
				$rsl[$k]=array("original"=>$ori_v,"update"=>"<font class='diff'>".$edt_v."</font>"); 
			}else if($showall){ 
				$rsl[$k]=array("original"=>$ori_v,"update"=>$edt_v); 
			}
		}
		return $rsl;
	}
	
	/**
	 * @brief a process to convert data raw from the database
     *          to the data that ready for viewing in user
	 * @param array $data is the 2 dimesional array of raw data from database
	 * @return  array $result is the 2 dimensional array of formatted data
	 */
	public function getContent($data){
		$content=array();
		foreach($data as $d){
			$adapt=$this->adapt($d);
			$content[]=$adapt;			
		}
		if($this->is_use_remove_zero){
			$content=$this->removeZero($content);
		}
		return $content;
	}
}

?>