<?php
/**
 * the controller is the wpdb wrapper
 * before then, smis is wordpress plugins
 * until i decide to remove the wordpress
 * and make smis a new whole framework
 * 
 * @author      : Nurul Huda
 * @since       : 14 Mei 2014
 * @license     : LGPLv3
 * @version     : 5.0.2
 * @copyright   : goblooge@gmail.com
 *
 */
 
class DBController{
	private $last_query;
	private $last_error;
	private $mode;
	
	private static $MYSQL_MODE="mysql";
	private static $MYSQLI_MODE="mysqli";
	private static $PDO_MODE="pdo";
    
    public static $NO_STRIP_WRAP=1;
	public static $NO_STRIP_ESCAPE_WRAP=2;
	
	public function __construct(){
		$this->last_query="";
		$this->last_error="";
		$this->mode=$this->getMode();
	}
	
    /**
     * @brief get the default PHP MySQL connector mode
     * @return  String mode;
     */
	public function getMode(){
		if(defined("SMIS_PHP_MODE"))
			return SMIS_PHP_MODE;
		else return self::$MYSQLI_MODE;
	}
	
    /**
     * @brief protect from sql injection
     *          this used to prevent sql injection in user input.
     * @param String $string 
     * @return  String escaped file, otherwise false;
     */
	public function escapeString($string){
		if($this->mode==self::$MYSQL_MODE){
			return mysql_real_escape_string($string);
		}else if($this->mode==self::$MYSQLI_MODE){
			$link=DBConnector::getMysqliLink();
			return mysqli_real_escape_string($link,$string);
		}
		return false;
	}
	
    
    /**
     * @brief check is the system already installed or not
     *          by detecting is the default database is already 
     *          exist or not.
     * @return  true if installed false if not.
     */
	public function is_installed(){
		if($this->mode==self::$MYSQL_MODE){
			if(mysql_num_rows(mysql_query("SHOW TABLES LIKE 'smis_adm%'"))>3)
				return true;
		}else if($this->mode==self::$MYSQLI_MODE){
			$link=DBConnector::getMysqliLink();
			if(mysqli_num_rows(mysqli_query($link,"SHOW TABLES LIKE 'smis_adm%'"))>3)
				return true;
		}
		return false;
	}
	
    /**
     * @brief is function query to take send some SQL 
     *          to the database
     * @param String $query 
     * @return  Fetched Object/Array
     */
	public function query($query){
		
		if($this->mode==self::$MYSQL_MODE){
			$this->last_query=$query;
			$this->last_error="";
			$res=mysql_query($query);
			if (mysql_errno()) {
				$error = "MySQL error ".mysql_errno().": ".mysql_error()."\n<br> When executing: <br><font class='diff'>$query</font><br>";
				$this->last_error=$error;
			}		
			return $res;
		}else if($this->mode==self::$MYSQLI_MODE){
			$link=DBConnector::getMysqliLink();
			$this->last_query=$query;
			$this->last_error="";
			$res=mysqli_query($link,$query);
			if (mysqli_errno($link)) {
				$error = "MySQL error ".mysqli_errno($link).": ".mysqli_error($link)."\n<br> When executing: <br><font class='diff'>$query</font><br>";
				$this->last_error=$error;
			}
			return $res;			
		}
	}
	
	public function get_col_info($table){
		$query="SHOW FIELDS FROM ".$table;
		$res=$this->query($query);
		$column=array();
		if($this->mode==self::$MYSQL_MODE){
			while($row=mysql_fetch_array($res)){
				$column[]=$row["Field"];
			}
		}else if($this->mode==self::$MYSQLI_MODE){
			while($row=mysqli_fetch_array($res)){
				$column[]=$row["Field"];
			}
		}
		return $column;
	}
    
   
    
    public function get_full_col_info($table){
		$query="SHOW FIELDS FROM ".$table;
		$res=$this->query($query);
		$column=array();
		if($this->mode==self::$MYSQL_MODE){
			while($row=mysql_fetch_array($res)){
				$one=array();
                $one['field']=$row['Field'];
                $one['type']=$row['Type'];
                $one['null']=$row['Null'];
                $one['key']=$row['Key'];
                $one['default']=$row['Default'];
                $one['extra']=$row['Extra'];
                $column[ $one['field']]=$one;
			}
		}else if($this->mode==self::$MYSQLI_MODE){
			while($row=mysqli_fetch_array($res)){
				$one=array();
                $one['field']=$row['Field'];
                $one['type']=$row['Type'];
                $one['null']=$row['Null'];
                $one['key']=$row['Key'];
                $one['default']=$row['Default'];
                $one['extra']=$row['Extra'];
                $column[$one['field']]=$one;
			}
		}
		return $column;
	}
    
    /**
     * @brief get the index but pack 
     *          as one of the group
     *          for each key name
     * @return  Array of inde
     */
    public function get_index_group($table){
        $query="SHOW INDEX FROM ".$table;
		$res=$this->query($query);
		$index=array();
		if($this->mode==self::$MYSQL_MODE){
			while($row=mysql_fetch_array($res)){
				$key=$row['Key_name'];
                $val=$row['Column_name'];
                if(!isset($index[$key])){
                    $index[$key]=array();
                }
                $index[$key][]=$val;
			}
		}else if($this->mode==self::$MYSQLI_MODE){
			while($row=mysqli_fetch_array($res)){
				$key=$row['Key_name'];
                $val=$row['Column_name'];
                if(!isset($index[$key])){
                    $index[$key]=array();
                }
                $index[$key][]=$val;
			}
		}
		return $index;
    }
    
    
    public function get_index_info($table){
		$query="SHOW INDEX FROM ".$table;
		$res=$this->query($query);
		$index=array();
		if($this->mode==self::$MYSQL_MODE){
			while($row=mysql_fetch_array($res)){
				$one=array();
                $one['key']=$row['Key_name'];
                $one['column']=$row['Column_name'];
                $index[]=$one;
			}
		}else if($this->mode==self::$MYSQLI_MODE){
			while($row=mysqli_fetch_array($res)){
				$one=array();
                $one['key']=$row['Key_name'];
                $one['column']=$row['Column_name'];
                $index[]=$one;
			}
		}
		return $index;
	}
    
    public function is_exist($table){
        $query="SHOW TABLES LIKE '".$table."';";
        $row=$this->get_row($query);
        if($row!=null){
            return true;
        }
        return false;
    }
	
	
	
	public function getLastQuery(){
		return $this->last_query;
	}
	
	public function getLastError(){
		return $this->last_error;
	}
	
	public function get_results($query,$is_object=true){
		$res=$this->query($query);
		if(!$res) return null;
		$result=array();
		
		if($this->mode==self::$MYSQL_MODE){
			if($is_object){
				while($row=mysql_fetch_object($res)){
					$result[]=$row;
				}
			}else{
				while($row=mysql_fetch_array($res)){
					$result[]=$row;
				}
			}
		}else if($this->mode==self::$MYSQLI_MODE){
			if($is_object){
				while($row=mysqli_fetch_object($res)){
					$result[]=$row;
				}
			}else{
				while($row=mysqli_fetch_array($res)){
					$result[]=$row;
				}
			}	
		}
		return $result;
	}
	
	public function get_row($query,$is_object=true){
		$res=$this->query($query);		
		if(!$res) return null;
		
		if($this->mode==self::$MYSQL_MODE){
			if($is_object){
				while($row=mysql_fetch_object($res)){
					return $row;
				}
			}else{
				while($row=mysql_fetch_array($res)){
					return $row;
				}
			}
		}else if($this->mode==self::$MYSQLI_MODE){
			if($is_object){
				while($row=mysqli_fetch_object($res)){
					return $row;
				}
			}else{
				while($row=mysqli_fetch_array($res)){
					return $row;
				}
			}
		}
		return null;
	}
	
	public function get_var($query){
		$row=$this->get_row($query,false);
		if($row!=null){
			return $row[0];
		}
		return null;
	}
	
	public function insert($name,$data,$use_prop=true,$warp=null){
		$column=" (";
		$values=" (";
		$number=0;		
		if( !isset($data['prop']) && $use_prop){
			$data['prop']="";
		}		
		foreach($data as $c=>$v){
			if($c=="id" && $v=="")
				continue;
			
			if($number==0){
                $column.="`".$c."`";
				if($warp!=null && isset($warp[$c]) && $warp[$c]==self::$NO_STRIP_WRAP ){
                    $values.=$this->escapeString($v);
                }else if($warp!=null && isset($warp[$c]) && $warp[$c]==self::$NO_STRIP_ESCAPE_WRAP ){
                    $values.=$v;
                }else{
                    $values.="'".$this->escapeString($v)."'";
                }
			}else{
				$column.=",`".$c."`";
                if($warp!=null && isset($warp[$c]) && $warp[$c]==self::$NO_STRIP_WRAP ){
                    $values.=",".$this->escapeString($v);
                }else if($warp!=null && isset($warp[$c]) && $warp[$c]==self::$NO_STRIP_ESCAPE_WRAP ){
                    $values.=",".$v;
                }else{
                    $values.=",'".$this->escapeString($v)."'";                    
                }
			}
			$number++;
		}
		$column.=") ";
		$values.=") ";
		$query="INSERT INTO `".$name."` ".$column." VALUES ".$values;
		return $this->query($query);
	}
	
	public function update($name,$data,$id,$warp=null){	
		$set=" ";
		$number=0;
		foreach($data as $c=>$v){
			if($number==0){
                if($warp!=null && isset($warp[$c]) && $warp[$c]==self::$NO_STRIP_WRAP ){
                    $set.=$c."=".$this->escapeString($v)."";
                }else if($warp!=null && isset($warp[$c]) && $warp[$c]==self::$NO_STRIP_ESCAPE_WRAP ){
                    $set.=$c."=".$v."";
                }else{
                    $set.=$c."='".$this->escapeString($v)."'";
                }                 
			}else{
                if($warp!=null && isset($warp[$c]) && $warp[$c]==self::$NO_STRIP_WRAP ){
                    $set.=",".$c."=".$this->escapeString($v);
                }else if($warp!=null && isset($warp[$c]) && $warp[$c]==self::$NO_STRIP_ESCAPE_WRAP ){
                    $set.=",".$c."=".$v;
                }else{
                    $set.=",".$c."='".$this->escapeString($v)."'";
                }
			}
			$number++;
		}
		
		$where=" WHERE 1 ";
		foreach($id as $n=>$v){
			$where.=" AND ".$n."='".$this->escapeString($v)."'";
		}
		
		$query="UPDATE ".$name." SET ".$set.$where;
		$this->query($query);
        if($this->last_error!=""){
            return false;
        }
		return $this->get_affected_rows();
	}
    
    public function updateByCustomKriteria($name,$data,$kriteria,$warp=null){
        $set=" ";
		$number=0;
		foreach($data as $c=>$v){
			if($number==0){
                if($warp!=null && isset($warp[$c]) && $warp[$c]==1 )
                    $set.=$c."=".$this->escapeString($v)."";
                else 
                    $set.=$c."='".$this->escapeString($v)."'";
			}else{
                if($warp!=null && isset($warp[$c]) && $warp[$c]==1 )
                    $set.=",".$c."=".$this->escapeString($v);
                else 
                    $set.=",".$c."='".$this->escapeString($v)."'";
			}
			$number++;
		}
        $where=" WHERE ".$kriteria;
        $query="UPDATE ".$name." SET ".$set.$where;
        $this->query($query);
		return $this->get_affected_rows();
    }
	
	public function delete($name,$id,$value=""){
		$query="DELETE FROM ".$name." WHERE 1 ";
		if(is_array($id)){
			foreach($id as $n=>$v){
				$query.=" AND ".$n."='".$this->escapeString($v)."'";
			}
		}else if($value=""){
			$query.=" AND id='".$id."' ";
		}else{
			$query.=" AND ".$id."='".$this->escapeString($value)."'";
		}
		return $this->query($query);
	}
	
	public function get_affected_rows(){
		if($this->mode==self::$MYSQL_MODE){
			return mysql_affected_rows();
		}else if($this->mode==self::$MYSQLI_MODE){
			$link=DBConnector::getMysqliLink();
			return mysqli_affected_rows($link);
		}
	}
	
	public function escaped_string($unescape_string){
		if($this->mode==self::$MYSQL_MODE){
			return mysql_real_escape_string($unescape_string);
		}else if($this->mode==self::$MYSQLI_MODE){
			$link=DBConnector::getMysqliLink();
			return  mysqli_real_escape_string($link,$unescape_string);
		}
	}
	
	public function set_autocommit($auto=true){
		$comm=$auto?"1":"0";
		if($this->mode==self::$MYSQL_MODE){
			return  $this->query("SET autocommit=".$comm.";");
		}else if($this->mode==self::$MYSQLI_MODE){
			$link=DBConnector::getMysqliLink();
			return mysqli_autocommit($link, $auto);
		}
	}
	
	public function begin_transaction(){
		if($this->mode==self::$MYSQL_MODE){
			return  $this->query("START TRANSACTION;");
		}else if($this->mode==self::$MYSQLI_MODE){
			return true;
		}
	}
	
	public function commit(){		
		if($this->mode==self::$MYSQL_MODE){
			return $this->query("COMMIT;");
		}else if($this->mode==self::$MYSQLI_MODE){
			$link=DBConnector::getMysqliLink();
			return mysqli_commit($link);
		}
	}
	
	public function rollback(){		
		if($this->mode==self::$MYSQL_MODE){
			return $this->query("ROLLBACK;");
		}else if($this->mode==self::$MYSQLI_MODE){
			$link=DBConnector::getMysqliLink();
			return mysqli_rollback($link);
		}
	}
	
	
	
	
	public function get_inserted_id(){
		if($this->mode==self::$MYSQL_MODE){
			return mysql_insert_id();
		}else if($this->mode==self::$MYSQLI_MODE){
			$link=DBConnector::getMysqliLink();
			return mysqli_insert_id($link);
		}
	}
	
	/*
	public function getUpdatedID(){
			//http://stackoverflow.com/questions/1388025/how-to-get-id-of-the-last-updated-row-in-mysql
			SET @uids := null;
			UPDATE footable
			   SET foo = 'bar'
			 WHERE fooid > 5
			   AND ( SELECT @uids := CONCAT_WS(',', fooid, @uids) );
			SELECT @uids;
	  
			//new methode
			SET @update_id := 0;
			UPDATE some_table SET column_name = 'value', id = (SELECT @update_id := id)
			WHERE some_other_column = 'blah' LIMIT 1; 
			SELECT @update_id;
	}*/
	
}
?>