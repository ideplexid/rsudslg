<?php 

class SummaryAdapter extends SimpleAdapter{
	
	protected $sum_array;
	protected $sum_value;
	protected $sum_format;
	
	
	public function __construct($use_number=false,$number_name=""){
		parent::__construct($use_number,$number_name);
		$this->sum_array=array();
		$this->sum_value=array();
		$this->sum_format=array();
	}
	
	public function addSummary($name,$dbname,$format="text"){
		$this->sum_array[$name]=$dbname;
		$this->sum_format[$name]=$format;
		return $this;
	}
	
	public function addFixValue($name,$value){
		$this->sum_value[$name]=$value;
		return $this;
	}
	
	public function adapt($d){
		$ar=array();
		if(is_array($d)){
			$ar=$d;
		}else{
			$ar=get_object_vars($d);
		}
		$array=parent::adapt($ar);
		foreach($this->sum_array as $name=>$dbname){
            if(!isset($this->sum_value[$name])){
                $this->sum_value[$name]=0;
            }
			$this->sum_value[$name]+=$ar[$dbname];
		}
		return $array;
	}
	
	public function getContent($data){
		$content=array();
		foreach($data as $d){
			$adapt=$this->adapt($d);
			$content[]=$adapt;			
		}
		foreach($this->sum_format as $name=>$format){
			if($format=="text") continue;
			$value=$this->sum_value[$name];
			$this->sum_value[$name]=self::format($format, $value);
		}
		$content[]=$this->sum_value;
		if($this->is_use_remove_zero){
			$content=$this->removeZero($content);
		}
		return $content;
	}
	
	
}


?>