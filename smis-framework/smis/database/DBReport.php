<?php 
/**
 * responding only the report
 * @author goblooge
 *
 */
class DBReport extends DBResponder{
	protected $time_control;
	protected $tipe;
	protected $diagram_query;
	protected $diagram_group;
	protected $diagram_enabled;	
	protected $diagram_adapter;	
	protected $timegroup;
    protected $use_from_date_in_control;
    protected $use_to_date_in_control;
    
	
	public static $TIME		= 1;
	public static $DATE		= 2;
	public static $MONTH	= 3;
	public static $YEAR		= 4;
	
	public function __construct($dbtable, $uitable, $adapter,$time=null,$tipe=1){
		parent::__construct($dbtable, $uitable, $adapter);
		$this->time_control=$time;
		$this->tipe=$tipe;
		$this->mode=self::$DATE;
		$this->timegroup=true;
        $this->use_from_date_in_control=true;
        $this->use_to_date_in_control=true;
	}
    
    public function setUseFromControl($used){
        $this->use_from_date_in_control=$used;
        return $this;
    }
    
    public function setUseToControl($used){
        $this->use_to_date_in_control=$used;
        return $this;
    }
	
	public function setTimeGroup($timegroup){
		$this->timegroup=$timegroup;
	}
	
	//based on time or date : waktu is that time or date
	public function setTipe($tipe){
		$this->tipe=$tipe;
	}
	
	public function setMode($mode){
		$this->mode=$mode;
	}
	
	public function setDiagram($diagram,$query,$group,$adapter){
		$this->diagram_group=$group;
		$this->diagram_query=$query;
		$this->diagram_enabled=$diagram;
		$this->diagram_adapter=$adapter;
		$this->uitable->setDiagram($diagram);
	}
	
	public function getDiagramGroup(){
		if($this->timegroup){
			if($this->diagram_group==null ||  $this->diagram_group==""){
				return "the_time";
			}else{
				"the_time , ".$this->diagram_group;
			}
		}else{
			if($this->diagram_group==null ||  $this->diagram_group==""){
				return "";
			}else{
				return $this->diagram_group;
			}
		}
	}
	
	public function getOrder(){
		if($this->timegroup){
			return " waktu ASC ";
		}else{
			return "";
		}
	}
	
	public function getView(){
		if($this->mode==self::$DATE){
			return $this->time_control." as waktu, DATE_FORMAT(".$this->time_control.",'%d-%M-%Y') as the_time, ";
		}else if($this->mode==self::$MONTH){
			return $this->time_control." as waktu, DATE_FORMAT(".$this->time_control.",'%M-%Y') as the_time, ";
		}else if($this->mode==self::$YEAR){
			return $this->time_control." as waktu, DATE_FORMAT(".$this->time_control.",'_%Y') as the_time, ";
		}
	}
	
	
	
	public function view(){
		$kriteria=$_POST['kriteria'];
		$number=$_POST['number']!=""?$_POST['number']:0;
		$max=$_POST['max'];
		$from_date=$_POST['from_date'];
		$to_date=$_POST['to_date'];
		$mode=isset($_POST['mode'])?$_POST['mode']:self::$DATE;		
		$this->setMode($mode);
		
		$this->dbtable->setMaximum($max);
		$custom_kriteria=$this->getAdvancedKriteria();
		if($this->time_control!=null){
			if($this->tipe==self::$TIME){
                if($this->use_from_date_in_control)
                    $custom_kriteria[$this->time_control.">="]="'".$from_date."'";
				if($this->use_to_date_in_control)
                    $custom_kriteria[$this->time_control."<="]="'".$to_date."'";
			}else{
				$control="DATE(".$this->time_control.")";
                if($this->use_from_date_in_control)
                    $custom_kriteria[$control.">="]="'".$from_date."'";
                if($this->use_to_date_in_control)
                    $custom_kriteria[$control."<="]="'".$to_date."'";
			}
		}
		$this->dbtable->addCustomKriteria("",$custom_kriteria);		
		$d=$this->dbtable->view($kriteria,$number);		
		$page=$d['page'];
		$data=$d['data'];
		$max_page=$d['max_page'];
		$this->adapter->setNumber($page*$max);
		$uidata=$this->adapter->getContent($data);
		$this->uitable->setContent($uidata);		
		$list=$this->uitable->getBodyContent();
		$pagination=$this->uitable->getPagination($page,3,$max_page);
		
		
		$json['list']=$list;
		if($this->debug) $json['dbtable']=$d;
		$json['pagination']=$pagination->getHtml();
		$json['number']=$page;
		$json['number_p']=$_POST['number'];
		
		//if using diagram
		if($this->diagram_enabled){
			$diagram=$this->fetchDiagram();
			$json['diagram_data']=$diagram['data'];
			if($this->debug) $json['ddiagram']=$diagram['debug'];
		}
		return $json;
	}
	
	public function fetchDiagram(){
		$kriteria=$_POST['kriteria'];
		$this->dbtable->setPreferredQuery(false,"","");
		$this->dbtable->setView($this->getView().$this->diagram_query);		
		$this->dbtable->setGroupBy(true,$this->getDiagramGrOup());
		$this->dbtable->setShowAll(true);
		if($this->timegroup)
		$this->dbtable->setOrder($this->getOrder());
		$ddiagram=$this->dbtable->view($kriteria, 0);
		$data=$ddiagram['data'];
		$diagram=$this->diagram_adapter->getContent($data);
		return array("data"=>$diagram,"debug"=>$ddiagram);
	}
	
	public function getAdvancedKriteria(){
		$result=array();
		foreach ($this->column as $c){
			if(isset($_POST[$c]))
			$result[$c]=" LIKE '".$_POST[$c]."'";
		}
		return $result;
	}
	
	
}

?>