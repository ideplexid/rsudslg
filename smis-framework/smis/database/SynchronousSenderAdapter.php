<?php 
/**
 * handling synchronizing data
 * the different is it will prepare if the data for deleting
 * 
 * @author : Nurul Huda
 * @license : LGPLv3
 * @copyright : goblooge@gmail.com
 * @since : 09-Marc-2017
 * @version : 1.0.0
 * 
 * */
abstract class SynchronousSenderAdapter extends ArrayAdapter{
    
    private $prop;
    public function __construct(){
        parent::__construct();
        $this->prop="";
    }
    
    public function getProp(){
        return $this->prop;
    }
    
    public function setProp($prop){
        $this->prop=$prop;
        return $this;
    }
    
}

?>