<?php 
/**
 * this class used to save
 * to handler duplicating data
 * from one autonomuse to another autonomous
 * there is 2 model that used in this
 * system
 * 
 * the data that only sync and not collide
 * and the data that the id could be collide
 * 
 * the data that not collide is used for table
 * that only one entity that have in full control and the other only read
 * this call master slave model.
 * 
 * but in other hand, the data that a data that all entity equally
 * can create or delete data. then sharing id creation model need
 * the id would be separately given for each database
 * so each autonomus can growth up the database without id collision
 * 
 * @author : Nurul Huda
 * @license : LGPLv3
 * @since : 21 Apr 2017
 * @copyright : goblooge@gmail.com
 * @version : 1.0.1
 * 
 * */
 
class DuplicateResponder extends DBResponder implements LockerInterface{
    
    private $autonomous;
    private $service_duplicate;
    private $is_autoduplicate;
    private $entity="all";
    private $locker;
    private $locker_id;
    private $increment_id_rule;
    private $user;
    private $notify_data;
    
    function __construct(MySQLTable $dbtable,Table $uitable=NULL,ArrayAdapter $adapter=NULL){
		global $db;
        parent::__construct($dbtable,$uitable,$adapter);
        $this->autonomous=getSettings($db,"smis_autonomous_id","");
	}
    
    public function command($command){
        if($command=="duplicate"){
            $pack=new ResponsePackage();
            $content=NULL;
            $status='not-authorized';	//not-authorized, not-command, fail, success
            $alert=array();
            $content=$this->duplicate();
            $pack->setAlertVisible(true);
            if($content!==false){
                $pack->setAlertContent("Data Duplicated","The data successfully Duplicated");
            }else{
                $pack->setAlertContent("Data Not Duplicated","The data not successfully Duplicated",ResponsePackage::$TIPE_WARNING);
            }
            $pack->setContent($content);
            $pack->setStatus(ResponsePackage::$STATUS_OK);
            return $pack->getPackage();
        }else if($command=="duplicateloop"){
            $pack=new ResponsePackage();
            $pack->setAlertVisible(false);
            $content=$this->duplicateloop();
            $pack->setContent($content);
            $pack->setStatus(ResponsePackage::$STATUS_OK);
            return $pack->getPackage();
        }else{
            return parent::command($command);
        }
    }
    
    /**
     * @brief : adding data that need to be send to Service
     * @param $key : the data key of array 
     * @param $value : the data value of array
     * return this 
     * */
    public  function addNotifyData($key,$value){
        if($this->notify_data==null){
            $this->notify_data=array();
        }
        $this->notify_data[$key]=$value;
        return $this;
    }
    
    public function getAutonomous(){
        return $this->autonomous;
    } 
    
    /**
     * @brief anything that 
     * call from here the synchronize should be not succeed
     * @return  
     */
    public function duplicate(){
        $id=$_POST['id'];
        return $this->notify($id);
    }
    
    /**
     * @brief this used to describe that wether system used 
     *          locker to determine the id or just simply
     *          follow the id that already provide by mysql sistem
     * @param boolean $locker true if the data need locker id 
     * @param String $locker_id the locker id model 
     * @param String $increment_id id rule 
     * @return  DuplicateResponder
     */
    public function setLocker($locker,$locker_id,$increment_id,$user){
        $this->locker=$locker;
        $this->locker_id=$locker_id;
        $this->increment_id_rule=$increment_id;
        $this->user=$user;
        return $this;
    }
    
    /**
     * @brief set the autonomouse name
     *          of the system so the database
     *          can be used as synchronize data
     * @param String $autonomous 
     * @return  DuplicateResponder (this)
     */
    public function setAutonomous($autonomous){
        $this->autonomous=$autonomous;
        return $this;
    }
    
    public function postToArray(){
        $data=parent::postToArray();
        if( !isset($data['id']) || $data['id']=="" || $data['id']==0  ){
            $data['origin']=$this->autonomous;
        }
        $data['autonomous']="[".$this->autonomous."]";
        $data['duplicate']=0;
        $data['time_updated']=date("Y-m-d H:i:s");
        $data['origin_updated']=$this->autonomous;
        return $data;
    }
    
    public function setEntity($entity){
        $this->entity=$entity;
        return $this;
    }
    
    public function setDuplicate($duplicate,$service){
        $this->is_autoduplicate=$duplicate;
        $this->service_duplicate=$service;
        return $this;
    }
    
    public function isAutoSync(){
        return $this->is_autoduplicate;
    }
    
    public function save(){
        if($this->locker && $this->getPost("id", 0)==0){
            /** locker used only when saved new id **/
            return $this->saveLocker();
        }else{
            return $this->saveNonLocker();
        }
    }
    
    public function duplicateloop(){
        if(isset($_POST['name_view']) && $_POST['name_view']!=""){
            $this->getDBTable()->setColumn(array("id",$_POST['name_view']));
        }
        $this->getDBTable()->setDelete(true);
        $this->getDBTable()->setShowAll(true);
        $this->getDBTable()->addCustomKriteria(" duplicate "," ='0' ");
        return $this->view();
    }
    
    /**
     * @brief save using locker called using duplicate system
     *          the whole system will automatically call all
     *          data that need to duplicate.
     * @return  Array
     */
    public function saveLocker(){
       $loker=new DuplicateLocker($this->getDBTable()->get_db(),$this->user,$this->locker_id,$this);
       return $loker->execute();
    }
    
    /**
     * @brief   the increment id not based on 
     *          mysql increment id, but based on
     *          setting incremental id
     * @return  int a next id
     */
    public function getIncrementRule(){
        $number=getSettings( $this->getDBTable()->get_db(),$this->increment_id_rule,1 );
        $number=$number*1;
        $number++;
        return $number;
    }
    
    /**
     * @brief   when get the locker 
     *          then proceed thi saving process
     *          locker only use for saving the insert process
     *          locker used only to get the unique id
     * @return  
     */
    public function proceed(){
        $db=$this->dbtable->get_db();
        $db->begin_transaction();
        $db->set_autocommit(false);        
        $data=$this->postToArray();
		$data['id']=$this->getIncrementRule();
        
		$result1 = $this->dbtable->insert($data,$this->is_use_prop,$this->upsave_condition);
		$result2 = setSettings($db,$this->increment_id_rule,$data['id']);
        
        $success['type']='insert';
		$success['id']=$data['id'];
		$success['success']=1;
		if($result1===false || $result2===false) {
            $success['success']=0;
            $db->rollback();
        }else{
            $db->commit();
        }
        $db->set_autocommit(true);
        if($result1!==false || $result2!==false){
            $this->notify($data['id'],true);
        }
        return $success;
    }
    
    /**
     * @brief save the data without locker
     * @return  
     */
    public function saveNonLocker(){
        $success=parent::save();
        $this->updateSynchOrigin($success);
        return $success;
    }
    
    /**
     * @brief update the origin id of the data
     *          if there is type is insert
     * @param Array $success 
     * @return  
     */
    protected function updateSynchOrigin($success){
        if($success['success']==1){
             $id=$success['id'];
            if($this->isAutoSync()){               
                $this->notify($id,$success['type']=="insert");
            }else if($success['type']=="insert"){
                $update['origin_id']=$id;
                $this->dbtable->update($update,array("id"=>$id));
            }
        }
    }
    
    
    /**
     * @brief this methode used to notify
     *          that the data have been created
     *          and all autonomous should listen
     *          and save this data also.
     * @param int $id of the inserted data
     * @param boolean $is_insert wether is insert process or not 
     * @return boolean true if success;
     */
    public function notify($id,$is_insert=false){
        $this->dbtable->setFetchMethode(DBTable::$ARRAY_FETCH);
        $row=$this->dbtable->selectEventDel($id);
        $update=array();
        if($is_insert) {
            $row['origin_id']=$id;
            $update['origin_id']=$id;
        }
        $row['duplicate']=1;
        $serv=new ServiceConsumer($this->getDBTable()->get_db(),$this->service_duplicate,$row,$this->entity);
        $serv->addData("command","save");
        foreach($this->notify_data as $key=>$value){
             $serv->addData($key,$value);
        }
        $serv->setMode(ServiceConsumer::$CLEAN_BOTH_JOIN_SINGLE_ARRAY);
        $serv->execute();
        $content=$serv->getContent();
        $success=$this->is_success($content);
        if($success){
            $update['duplicate']=1;
        }
        $this->dbtable->update($update,array("id"=>$id));
        return $success;
    }
    
    /**
     * @brief this function used to analyse 
     *          whether the save process is succes or not
     * @param Array $content is the result that need to analyse 
     * @return  boolean
     */
    public function is_success($content){
        foreach($content as $x=>$v){
            if(!isset($v['success']) || $v['success']!="1"){
                return false;
            }
        }
        return true;
    }
    
    public function delete(){
		$id['id']=$_POST['id'];
		if($this->dbtable->isRealDelete()){
			$result=$this->dbtable->delete(null,$id);
		}else {
            $data['duplicate']=0;
            $data['time_updated']=date("Y-m-d H:i:s");
            $data['origin_updated']=$this->autonomous;
            $data['prop']="del";
            
            $result=$this->dbtable->update($data,$id);
            if($result!==false && $this->isAutoSync()){
                $this->notify($id,false);
            }    
        }
		$success['success']=1;
		if($result==='false') $success['success']=0;
		return $success;
	}
    
}

?>