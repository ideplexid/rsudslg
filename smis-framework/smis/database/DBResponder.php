<?php
/**
 * 
 * this class represent of Controller in MVC of Safethree
 * most of this class using Facade and Bridge Design Pattern that combining 
 * several methode from DBTable, Adapter and Table.
 * this Class used with javascript TableAction in javascript
 * for handling Insert, View, Update, Delete from TableAction in javascript
 * all db responder response is a Package from ResponsePackage Class.
 * 
 * @see ResponsePackage
 * @see Table
 * @see Adapter
 * @see DBTable
 * @author goblooge
 * @since 12 Januari 2014
 * @copyright Nurul Huda <goblooge@gmail.com>
 * @version 3.4
 */

class DBResponder extends Responder{
    /*@var DBTable*/
	protected $dbtable;
	/*@var Table*/
	protected $uitable;
	/*@var ArrayAdapter*/
	protected $adapter;
	/*@var bool*/
	protected $debug;
	/*@var Array*/
	protected $column;
	/*@var Array*/
	protected $column_json;
	/*@var Array*/
	protected $column_fix_value;	
	/*@var bool*/
	protected $is_adapter_for_select;
	/*@var bool*/
	protected $include_adapter_data;
	/*@var bool*/
	protected $include_raw_data;
	/*@var bool*/
	protected $is_exist_include_del;
	/*@var ResponsePackage*/
	protected $response_pack;
	/*@var Array*/
	protected $upsave_condition;
	/*@var bool*/
	protected $is_add_enable;
	/*@var bool*/
	protected $is_edit_enable;
	/*@var bool*/
	protected $is_select_enabled;
	/*@var bool*/
	protected $is_del_enable;
	/*@var bool*/
	protected $is_print_enable;
    /*@var bool*/
	protected $is_use_prop;
	
	
	function __construct(MySQLTable $dbtable,Table $uitable=NULL,ArrayAdapter $adapter=NULL){
		$this->dbtable					= $dbtable;
		$this->uitable					= $uitable;
		$this->adapter					= $adapter;
		$this->debug					= false;
		$this->column					= $this->dbtable->getcolumn();
		$this->column_fix_value			= array();
		$this->is_adapter_for_select	= false;
		$this->column_json				= array();
		$this->include_adapter_data		= false;
		$this->include_raw_data			= false;
		global $DEBUGGABLE;
		$this->setDebuggable($DEBUGGABLE=="1");
		$this->is_add_enable			= true;
		$this->is_edit_enable			= true;
		$this->is_del_enable			= true;
		$this->is_print_enable			= true;
		$this->is_select_enabled		= true;
		$this->is_exist_include_del		= false;
        $this->upsave_condition			= null;
		$this->is_use_prop				= true;
	}
	    
    public function setProUsability($use){
        $this->is_use_prop				= $use;
        return $this;
    }
    
    public function addUpSaveWarp($name,$value){
        if($this->upsave_condition==null){
			$this->upsave_condition		= array();
		}
        $this->upsave_condition[$name]	= $value;
        return $this;
    }
    
	public function setExistIncludeDel($include){
		$this->is_exist_include_del		= $include;
		return $this;
	}
	
	public function setAdapter($adapter){
		$this->adapter					= $adapter;
		return $this;
	}
	
	public function setAddEnable($enabled){
		$this->is_add_enable			= $enabled;
		return $this;
	}
	
	public function setDelEnable($enabled){
		$this->is_del_enable			= $enabled;
		return $this;
	}
	
	public function setEditEnable($enabled){
		$this->is_edit_enable			= $enabled;
		return $this;
	}
	
	public function setPrintEnable($enabled){
		$this->is_print_enable			= $enabled;
		return $this;
	}
	
	public function isDelEnable(){
		return $this->is_del_enable;
	}
	
	public function isEditEnable(){
		return $this->is_edit_enable;
	}
	
	public function isPrintEnable(){
		return $this->is_print_enable;
	}
	
	/**
	 * set your own value
	 * @param unknown $name
	 * @param unknown $value
	 */
	public function addColumnFixValue($name,$value){
		$this->column_fix_value[$name]	= $value;
		return $this;
	}

	public function setIncludeAdapterData($use){
		$this->include_adapter_data		= $use;
		return $this;
	}
	
	public function setIncludeRawData($use){
		$this->include_raw_data			= $use;
		return $this;
	}
	
	/**
	 * if there option that is json
	 * @param array $array
	 */
	public function setJsonColumn(Array $array){
		$this->column_json				= $array;
		return $this;
	}
	
	public function setUseAdapterForSelect($use){
		$this->is_adapter_for_select	= $use;
		return $this;
	}
	
	public function getUITable(){
		return $this->uitable;
	}
    
    /**
     * @brief set the ui table in responder
     * @param Table $uitable 
     * @return  this
     */
    public function setUITable($uitable){
		$this->uitable					= $uitable;
        return $this;
	}
	
	/**
	 * get DBTable Object
	 * @return DBTable;
	 */
	protected function getDBTable(){
		return $this->dbtable;
	}

	/**
	 * get the Database Object
	 * @return Database;
	 */
	protected function getDB(){
		return $this->dbtable->get_db();
	}
	
	/**
	 * this function is used for selecting the correct 
	 * response based on the command that it got.
	 * this methode is use for selecting the correct command.
	 * each command will have different respose.
	 * and this function will pack it as Package using ResponsePackage Class
	 * 
	 * @see ResponsePackage
	 * @param string $command
	 * @return ResponsePackage pakage
	 */
	public function command($command){		
		$this->response_pack = new ResponsePackage();	
		if($command=="list"){
			$content		 = $this->view();
			$this->response_pack->setContent($content);
			$this->response_pack->setStatus(ResponsePackage::$STATUS_OK);
		}else if($command=='save' && $this->getAddCapability()){
			$content		 = $this->save();
			$this->response_pack->setContent($content);
			$this->response_pack->setStatus(ResponsePackage::$STATUS_OK);
			if($content['success']==0){
                $this->response_pack->setAlertVisible(true);
                $this->response_pack->setAlertContent("Saving Fail", "Data Saving Process Fail.. See Log to Trace Error", ResponsePackage::$TIPE_WARNING);
            }else{
                $this->response_pack->setAlertVisible(true);
                $this->response_pack->setAlertContent("Data Saved", "Your Data Had Been Saved", ResponsePackage::$TIPE_INFO);                
            }
		}else if($command=="del" && $this->getDeleteCapability()){
			$content		 = $this->delete();
            if($content['success']==1){
                $this->response_pack->setAlertContent("Data Removed", "Your Data Had Been Removed", ResponsePackage::$TIPE_INFO);
            }else{
                $this->response_pack->setAlertContent("Data Not Removed", "Your Data Not Removed", ResponsePackage::$TIPE_INFO);    
            }
			$this->response_pack->setContent($content);
			$this->response_pack->setStatus(ResponsePackage::$STATUS_OK);
			$this->response_pack->setAlertVisible(true);			
		}else if($command=="edit" && $this->getEditCapability()){
			$content		 = $this->edit();
			$this->response_pack->setContent($content);
			$this->response_pack->setStatus(ResponsePackage::$STATUS_OK);
		}else if($command=="select" && $this->getSelectCapability()){
			$content		 = $this->select();
			$this->response_pack->setContent($content);
			$this->response_pack->setStatus(ResponsePackage::$STATUS_OK);
		}else if($command=="reload" ){
			$content		 = $this->reload();
			$this->response_pack->setContent($content);
			$this->response_pack->setStatus(ResponsePackage::$STATUS_OK);
		}else if($command=="print-element"){
			$content		 = $this->printing("print-element");
			$this->response_pack->setContent($content);
			$this->response_pack->setStatus(ResponsePackage::$STATUS_OK);
		}else if($command=="printing"){
			$content		 = $this->printing();
			$this->response_pack->setContent($content);
			$this->response_pack->setStatus(ResponsePackage::$STATUS_OK);
		}else if($command=="excel"){
			$this->excel();
			return NULL;
		}else if($command=="excel-element"){
			$this->excel_element();
			return NULL;
		}else if($command=="pdf"){
			$content		 = $this->pdf();
			$this->response_pack->setContent($content);
			$this->response_pack->setStatus(ResponsePackage::$STATUS_OK);
		}else if($command=="pdf-element"){
			$content		 = $this->pdf_element();
			$this->response_pack->setContent($content);
			$this->response_pack->setStatus(ResponsePackage::$STATUS_OK);
		}else if($command=="multipart_view"){
			$content		 = $this->multipart_view();
			$this->response_pack->setContent($content);
			$this->response_pack->setStatus(ResponsePackage::$STATUS_OK);
		}else if($command=="detail"){
			$content		 = $this->detail();
			$this->response_pack->setContent($content);
			$this->response_pack->setStatus(ResponsePackage::$STATUS_OK);
		}else{
			$this->response_pack->setStatus(ResponsePackage::$STATUS_OK);
			$this->response_pack->setWarning(true, "Operation not Permitted !!","You have no permission to do this operation !!! ");
			if($command=="save") $this->response_pack->setContent("");
			else $this->response_pack->setContent(NULL);
		}
		return $this->response_pack->getPackage();
	}
	
    /* get the data detail */
    public function detail(){
       $this->getDBTable()->setFetchMethode(DBTable::$ARRAY_FETCH);
       $x		= $this->select();
       $id		= $_POST['id'];
       $table	= new TablePrint("");
       $table	->setDefaultBootrapClass(true);
       $table	->addColumn("DATA DETAIL OF ID : [ <strong>".$id."</strong> ]",2,1,"title");
       $table	->setMaxWidth(false);
       foreach($x as $n=>$v){
           if(is_int($n)){
			 continue;
		   }  
		   $table ->addColumn(strtoupper($n),1,1)
				  ->addColumn(strtoupper($v),1,1)
				  ->commit("body");
       }
       return $table->getHtml();
    }
	
	private function multipart_view(){
		$query = $this->dbtable->getQueryCount("");
		$total = $this->dbtable->get_db()->get_var($query);
		return $total;
	}
	
	protected function getPost($name,$default){
		if(isset($_POST[$name]))
			return $_POST[$name];
		return $default;
	}
	
	/**
	 * get Capability to add item
	 */
	public function getAddCapability(){
		 if( $this->is_add_enable) {
			 return true;
		 }
		return $this->getEditCapability();
	}
	
	/**
	 * get Capability to Select Item
	 */
	public function getSelectCapability(){
		 return ( $this->is_select_enabled );
	}
	
	/**
	 * get Capability to Delete Item
	 */
	public function getDeleteCapability(){
		return ($this->is_del_enable);
		
	}
	
	/**
	 * get Capability to Edit
	 */
	public function getEditCapability(){
		return($this->is_edit_enable);
	}
	
	/**
	 * handling excel creation when header menu excel button
	 * clicked. it's used simple excel library that only work for several and simple 
	 * excel code creation
	 */
	public function excel(){
		global $user;
		loadLibrary("smis-libs-function-export");
		$kriteria		= isset($_POST['kriteria'])?$_POST['kriteria']:""; 
		$kriteria		= $this->dbtable->escaped_string($kriteria);
		$number			= (isset($_POST['number']) && $_POST['number']!="")?$_POST['number']:0;
		$max			= isset($_POST['max'])?$_POST['max']:"10";		
		$this->dbtable->setMaximum($max);		
		$d				= $this->dbtable->view($kriteria,$number);
		$start_number	= $page*$max;
		$this->adapter->setNumber($start_number);
		$data			= $d['data'];
		$uidata			= $this->adapter->getContent($data);
		$filename		= $this->dbtable->getName();
		$filename	   .= "-".$kriteria;
		$filename	   .= " [ ".($start_number)." - ".($start_number+count($uidata))." ]";
		$filename	   .= " ( ".ArrayAdapter::format("date d M Y H:i:s", date("Y-m-d H:i:s")).") ";
		$filename	   .= " - ".$user->getNameOnly();
		createExcel($filename, $uidata);
		return;
	}

	public function pdf(){
		echo "Override PDF function in DBResponder";
		return;
	}
	
	public function reload(){
		return $this->uitable->getHtml();
	}
	
	public function excel_element(){
		echo "Override Excel_Element Function in DBResponder";
	}

	public function pdf_element(){
		echo "Override PDF_Element Function in DBResponder";
	}
	
	/**
	 * this is Facade Design Patern
	 * print single or several data from current Table in Database.
	 * what this function do is.
	 * 1. get the id of data that need to be print from post
	 * 2. search single row of the raw data in database using DBTable Class.
	 * 3. Adapt the raw data using Adapter for suitable purpose.
	 * 4. class Function getPrintedElement or getPrinted that will return string of how this each single data of this databse will be print
	 *   a. getPrintedElement will return single data for print purpose.
	 *   b. getPrined will return several data for print purpose (usually for table)
	 * @return string of HTML that viewing data for print purpose.
	 */
	public function printing(){
		$id 			= $_POST['id'];
		$this->dbtable->addCustomKriteria($this->dbtable->getViewTableName().".id","='".$id."'");
		$d				= $this->dbtable->view("","0");
		$data			= $d['data'];
		$uidata			= $this->adapter->getContent($data);
		$raw			= $this->select($id);
		$row			= $uidata[0];
		$slug			= $_POST['slug'];
		$print_data		= "";		
		if($slug=="print-element"){
			$print_data = $this->uitable->getPrintedElement($raw,$row); 
		}else{
			$print_data = $this->uitable->getPrinted($raw,$row,$slug);
		}
		return $print_data;
	}
	
	/**
	 * this is Facade Design Patern
	 * print single data from current Table in Database.
	 * what this function do is.
	 * 1. get the id of data that need to be print from post
	 * 2. search single row of the raw data in database using DBTable Class.
	 * 3. Adapt the raw data using Adapter for suitable purpose.
	 * 4. call Function getPrintedElement of Table that will return string of how this each single data of this databse will be print
	 * @return string of HTML that viewing single data for print purpose.
	 */
	public function printElement(){
		$id			= $_POST['id'];
		$this->dbtable->addCustomKriteria($this->dbtable->getName().".id","='".$id."'");
		$d			= $this->dbtable->view("","0");
		$data		= $d['data'];
		$uidata		= $this->adapter->getContent($data);
		$raw		= $this->select();
		$row		= $uidata[0];
		return $this->uitable->getPrintedElement($raw,$row);
	}
	
	/**
	 * get all data from post that needed for this DBResponder.
	 * @return Array String of needed data
	 */
	public function postToArray(){
		$result					= array();
		foreach ($this->column as $c){
			if(isset($_POST[$c])){
				$value			= $_POST[$c];
				if(is_array($value) || in_array($c,$this->column_json)){
					$result[$c]	= json_encode($value);
				}else{
					$result[$c]	= $value;
				}				
			}
		}
		foreach($this->column_fix_value as $c=>$v){
			$result[$c]			= $v;
		}
		return $result;
	}
	
	/**
	 * get all data from get that needed for this DBResponder.
	 * @return Array String of needed data
	 */
	public function getToArray(){
		$result			= array();
		foreach ($this->column as $c){
			if(isset($_GET[$c]))
			$result[$c] = $_GET[$c];
		}
		return $result;
	}
	
	public function setDebuggable($debug){
		$this->dbtable->setDebuggable($debug);
		$this->debug = $debug;		
	}
	
	/**
	 * Facade Pattern That Combining from several Class DBTable, Adapter, Table
	 * for viewing array from database. Adapter and Table is Brige Design Patter with DBResponder
	 * which each one of them can be vary indepedently
	 * what this function do are :
	 * - step 1 : look into database the requested data, based on kriteria, start , and limit of maximum data using DBTable class
	 * - step 2 : convert  the data from step 2 using Adapter Class so it's will be suitable for the Table Class
	 * - step 3 : implement the suitable data from step 2 to Table Class.
	 * 
	 * @return Array of package
	 */
	public function view(){
		$kriteria			 = isset($_POST['kriteria'])?$_POST['kriteria']:""; 
		$kriteria			 = $this->dbtable->escaped_string($kriteria);
		$number				 = (isset($_POST['number']) && $_POST['number']!="")?$_POST['number']:0;
		$max				 = isset($_POST['max'])?$_POST['max']:"10";		
		$this->dbtable->setMaximum($max);		
		$d					 = $this->dbtable->view($kriteria,$number);
		$page				 = $d['page'];
		$data				 = $d['data'];
		$max_page			 = $d['max_page'];
		$this->adapter->setNumber($page*$max);
		$uidata				 = $this->adapter->getContent($data);
		$this->uitable->setContent($uidata);
		$list				 = $this->uitable->getBodyContent();
		$pagination			 = $this->uitable->getPagination($page,3,$max_page);		
		$json['list']		 = $list;
		if(!$this->debug) {
			unset($d['custom_kriteria']);
			unset($d['query']);
		}
		$json['debug']		 = $this->debug;
		$json['dbtable']	 = $d;
		if($this->include_adapter_data){
			$json['adapter'] = $uidata;
		}
		if($this->include_raw_data){
			$json['raw']	 = $data;
		}
		$json['pagination']	 = $pagination->getHtml();
		$json['number']		 = $page;
		$json['number_p']	 = $number;
		return $json;
	}
	
	/**
	 * Facade Design Pattern for saving in Database, what this function do is
	 * 1. get All need data From post using posToArray Function
	 * 2. lookup if the ID Exist or not
	 * -- a. if Exist update current data in Database using DBTable Class.
	 * -- b. if Not Exist insert new data into Database using DBTable Class.
	 * 3. inform that success, by returning id (whether is new inserted id or updated id)
	 * @return Array of string
	 */
	public function save(){
		$data					= $this->postToArray();
		$id['id']				= $this->getPost("id", 0);
		if($id['id']==0 || $id['id']=="" || !$this->dbtable->is_exist($id,$this->is_exist_include_del)){
			$result				= $this->dbtable->insert($data,$this->is_use_prop,$this->upsave_condition);
			$id['id']			= $this->dbtable->get_inserted_id();
			$success['type']	= 'insert';
		}else {
			$result				= $this->dbtable->update($data,$id,$this->upsave_condition);
			$success['type']	= 'update';
		}
		$success['id']			= $id['id'];
		$success['success']		= 1;
		if($result===false){
			$success['success'] = 0;
		}
		return $success;
	}
	
	/**
	 * facade design pattern that used for deleting data in Database.
	 * what this function do is :
	 * 1. Get the id of data that want to be delete.
	 * 2. check wether real delete or just hide it (mean change the prop only)
	 * 3. inform the consumet whether success or fail
	 * @return number
	 */
	public function delete(){
		$id['id']				= $_POST['id'];
		if($this->dbtable->isRealDelete()){
			$result				= $this->dbtable->delete($id['id']);
		}else{
			$data['prop']		= "del";
			$result				= $this->dbtable->update($data,$id);
		}
		$success['success']		= 1;
		if($result==='false') {
			$success['success']	= 0;
		}
		return $success;
	}
	
	/**
	 * get raw data of current id in the database based on id
	 * 1. get the id of the data that need to be found
	 * 2. find the data in database using DBTable Class.
	 * 3. return to the consumer as is (raw data from the Database)
	 * @return raw data of current row.
	 */
	public function edit(){
		$id		= $_POST['id'];
		$row	= $this->dbtable->select($id,false);
		return $row;
	}
	
	/**
	 * get adapted data of current id in the database based on id
	 * 1. get the id of the data that need to be found
	 * 2. find the data in database using DBTable Class.
	 * 3. adapt the data that found in step 2 using Adapter Class.
	 * 4. return the adapted data to the consumer
	 * if the is_adapter_for_select is false and dbtable adapter is false then this class as same as edit
	 * @return raw data of current row.
	 */	
	public function select(){
		$row		= $this->edit();
		if($this->is_adapter_for_select || $this->dbtable->isUseAdapterForSelect()){
			$row	= $this->adapter->adapt($row);
		}
		return $row;
	}
	
}

?>
