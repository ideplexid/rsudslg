<?php 

class SimpleAdapter extends ArrayAdapter{
	
	protected $lname;
	protected $lvalue;
	protected $lformat;
	protected $is_use_number;
	protected $number_name;
	protected $is_use_id;
	protected $number_format;
	
	
	public function __construct($use_number=false,$number_name=""){
		parent::__construct();
		$this->lname=array();
		$this->lvalue=array();
		$this->lformat=array();		
		$this->is_use_number=$use_number;
		$this->number_name=$number_name;
		$this->is_use_id=true;
		$this->number_format=NULL;
	}
	
	public function setUseNumber($use,$name,$format=NULL){
		$this->is_use_number=$use;
		$this->number_name=$name;
		$this->number_format=$format;
		return $this;
	}
	
	public function setUseID($enable){
		$this->is_use_id=$enable;
		return $this;
	}
	
	
	
	public function add($name,$dbname,$format=null){
		$this->lname[]=$name;
		$this->lvalue[$name]=$dbname;
		$this->lformat[$name]=$format;
		return $this;
	}
	
	public function adapt($d){
		$ar=array();
		if(is_array($d)){
			$ar=$d;
		}else{
			$ar=get_object_vars($d);
		}
		
		$array=array();
		if($this->is_use_id && isset($ar['id']) )
			$array["id"]=$ar['id'];
		foreach ($this->lname as $n){
			$format=$this->lformat[$n];
			$value="";
			if($format=="fix"){
				$value=$this->lvalue[$n];
			}else if(isset($this->lvalue[$n]) && isset($ar[$this->lvalue[$n]])){
				$value=$ar[$this->lvalue[$n]];
				if($format!=null){
					$value=self::format($format, $value);
				}
			}
			$array[$n]=$value;
		}
		
		if($this->is_use_number){
			$this->number++;
			$array[$this->number_name]=self::format($this->number_format, $this->number);
		}
		
		return $array;
	}
	

	
}


?>