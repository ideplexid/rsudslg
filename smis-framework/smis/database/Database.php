<?php
/**
 * this is database application that depend on the database engine
 * i used wordpress to do so it will depend on wordpress but, 
 * when we change the engine we could do better.
 * 
 * @since 14 mar 2014
 * @version 1.5
 * @used	- smis-base/smis-init-variable.php
 * 			- smis-base/smis-service-variable
 * @author goblooge
 */
class Database{
	/* @var DBController */
	private $wpdb;
	/* @var User*/
	private $user;
	/* @var QueryLog*/
	private $querylog;
	/* @var string*/
	private $last_error;
	/* @var string*/
	private $last_query;
	
	private static $not_log=array("smis_adm_log","smis_adm_error","smis_sb_log","smis_adm_service_requests","smis_adm_service_responds");
	
	function __construct(DBController $wpdb,$user,$querylog=null){
		$this->wpdb= $wpdb;
		$this->user=$user;
		$this->querylog=$querylog;
	}
	
	public function log($database,$action,$before,$after,$dbid){
		if(in_array($database, self::$not_log)) 
			return;
		$data['user']=$this->user->getName();
		$data['service']=$this->user->getService();
		$data['code']=$this->user->getCode();
		$data['db']=$database;
		$data['ip']=$this->user->getIp()." ".$this->user->getMac();
		$data['post']=$this->user->getPost();
		$data['action']=$action;
		$data['original']=$before;
		$data['edited']=$after;
		$data['dbid']=$dbid;
		$this->querylog->saveDatabaseLog($database,$data,$dbid);
		return $this;
	}
	
    public function getLastError(){
        return $this->last_error;
    }
    
    public function getLastQuery(){
        return $this->last_query;
    }
	
	
	public function errorLog(){
		$this->last_query=$this->wpdb->getLastQuery();
		$this->last_error=$this->wpdb->getLastError();
		if($this->querylog!=null)
		$this->querylog->addQuery($this->last_query);
		$this->querylog->addErrorQuery($this->last_error);
		return $this;
	}
	
	public function escaped_string($string){
		return $this->wpdb->escaped_string($string);
	}
	
	
	/**
	 * use it as insert update or delete 
	 * without using any return except success or not succes
	 * @param string $query
	 */
	public function query($query){
		$res=$this->wpdb->query($query);
		$this->errorLog();
		return $res;
	}
	
	/**
	 * Proxy Pattern for WPDB
	 * @param unknown $query
	 * @param string $is_object
	 * @return multitype:object multitype:
	 */
	public function get_result($query,$is_object=true){
		$res=$this->wpdb->get_results($query,$is_object);
		$this->errorLog();
		return $res;
	}
	
	/**
	 * Proxy Pattern for WPDB
	 * @param unknown $query
	 * @param string $is_object
	 * @return multitype:object multitype:
	 */
	public function get_row($query,$is_object=true){
		$res=$this->wpdb->get_row($query,$is_object);
		$this->errorLog();
		return $res;
	}
	
	/**
	 * Proxy Pattern for WPDB
	 * @param unknown $query
	 * @param string $is_object
	 * @return multitype:object multitype:
	 */
	public function get_var($query){
		$res=$this->wpdb->get_var($query);
		$this->errorLog();
		return $res;
	}
	
	public function insert($name,$data,$use_prop=true,$warp=null){
		try{
			$result=$this->wpdb->insert($name,$data,$use_prop,$warp);
			$this->errorLog();
			return $result;
		}catch(Exception $e){
			
		}
		return false;
	}
	
	public function update($name,$data,$id,$warp=null){
		$res=$this->wpdb->update($name,$data,$id,$warp);
		$this->errorLog();
		return $res;
	}
    
    public function updatebyCustomKriteria($name,$data,$kriteria,$warp=null){
		$res=$this->wpdb->updatebyCustomKriteria($name,$data,$kriteria,$warp);
		$this->errorLog();
		return $res;
	}
	
	public function delete($name,$id){
		return true;
	}
	
	public function set_autocommit($auto){
		$this->wpdb->set_autocommit($auto);
	}
	
	public function begin_transaction(){
		$this->wpdb->begin_transaction();
	}
	
	public function commit(){
			$this->wpdb->commit();
	}
	
	public function rollback(){
		$this->wpdb->rollback();
	}
	
	public function get_col_info($info){
		$res=$this->wpdb->get_col_info($info);
		$this->errorLog();
		return $res;
	}
	
	public function get_inserted_id(){
		return $this->wpdb->get_inserted_id();
	}
	
	public function close(){
		$this->wpdb->close();
	}
	
	
}
	
?>