<?php 

class SelectAdapter extends ArrayAdapter{
	
	protected $name;
	protected $value;
	protected $cross;
	protected $convertion;
	
	public static $CROSS		= true;
	public static $SELECT		= false;
	public static $LISTVALUE	= "1";
	public $default;
	
	public function __construct($name,$value,$cross=false,$default=""){
		parent::__construct();
		$this->name				= $name;
		$this->value			= $value;
		$this->cross			= $cross;
		$this->convertion		= array();
		$this->default			= $default;
	}	
	
	public function setCross($cross,$name=null,$value=null){
		if($name!=null) 	$this->name		= $name;
		if($value!=null) 	$this->value	= $value;
		$this->cross = $cross;
	}
	
	public function adapt($d){
		$ar	= array();
		if(is_array($d)) $ar	= $d;
		else $ar	= get_object_vars($d);
		
		$array	= array();
		if(is_string($this->cross) &&  $this->cross==self::$LISTVALUE){
			return $ar[$this->value];
		}else if($this->cross){
			$name			= $ar[$this->name];
			$value			= $ar[$this->value];
			$array[$name]	= $value;
		}else{
			$array['name']		= $ar[$this->name];
			$array['value']		= $ar[$this->value];
			$array['default']	= $ar[$this->value]==$this->default?"1":"0";
		}
		
		return $array;
	}
	
	public function getContent($data){
		$content	= array();
		$number		= 0;
		foreach($data as $d){
			$adapt				= $this->adapt($d);
			$content[$number]	= $adapt;
			$number++;
		}
		if($this->cross==self::$LISTVALUE)
			return json_encode($content);
		return $content;
	}
	
	public function convert($d){
		$ar	= array();
		if(is_array($d)) $ar = $d;
		else $ar			 = get_object_vars($d);
		
		$name	= $ar[$this->name];
		$value	= $ar[$this->value];
		$this->convertion[$name] = $value;	
	}
	
	public function getConvert($data){
		foreach($data as $d){
			$this->convert($d);
		}
		return $this->convertion;
	}	
}
?>