<?php 

/**
 * this class used to synchronize 
 * from one database to antoher database
 * very usefull on redundant data for speed especially
 * like cache the sum, synchronize data and etc. 
 * 
 * @author : Nurul Huda
 * @license : LGPLv3
 * @copyright : goblooge@gmail.com
 * @since : 09-Marc-2017
 * @version : 1.0.0
 * @abstract : - synchronousData($id)
 * 
 * */
 
 abstract class DBSynchronousResponder extends DBResponder{
    
    protected $adapter_synchronous;
     
    function __construct(DBTable $dbtable, TableSynchronous $uitable=NULL,SynchronousViewAdapter $adapter=NULL,SynchronousSenderAdapter $synchronouse=NULL){
		parent::__construct($dbtable,$uitable,$adapter);
        $this->adapter_synchronous=$synchronouse;
	}
    
    public function command($command){      
        if($command=="synch"){
            $pack=new ResponsePackage();
            $content=NULL;
            $status='not-authorized';	//not-authorized, not-command, fail, success
            $alert=array();
            $content=$this->synchronous();
            $pack->setAlertVisible(true);
            if($content['success']=="1"){
                $pack->setAlertContent("Data Synched","The data successfully synched");
            }else{
                $pack->setAlertContent("Data Not Synched","The data not successfully synched",ResponsePackage::$TIPE_WARNING);
            }
            $pack->setContent($content);
            $pack->setStatus(ResponsePackage::$STATUS_OK);
            return $pack->getPackage();
        }else if($command=="synchloop"){
            $pack=new ResponsePackage();
            $pack->setAlertVisible(false);
            $content=$this->synchloop();
            $pack->setContent($content);
            $pack->setStatus(ResponsePackage::$STATUS_OK);
            return $pack->getPackage();
        }else{
            return parent::command($command);
        }
    }
    
    public function synchloop($command){
        if(isset($_POST['name_view']) && $_POST['name_view']!=""){
            $this->getDBTable()->setColumn(array("id",$_POST['name_view']));
        }
        $this->getDBTable()->setDelete(true);
        $this->getDBTable()->setShowAll(true);
        $this->getDBTable()->addCustomKriteria(" synch "," ='0' ");
        return $this->view();
    }
    
    /**
     * @brief notify to system that synhronize has been succeded
     * @param String $id is the id that need to be notify
     * @param int $sync is whether succed or failed
     * @param String $prop, if del, then should be delete 
     * @return  this
     */
    private function notifySynchronize($id,$sync,$prop=""){
        $up=array();
        $up['synch']=$sync;
        $up['prop']=$prop;
        $idx=array();
        $idx['id']=$id;
        $this->dbtable->update($up,$idx);
        return $this;
    }
    
    /**
     * @brief check is the synchronize process
     * @return  boolean is_synchronize
     */
    public function isSynchronize(){
		return $this->is("sync");
	}
    
    
    /**
     * @brief anything that call from here the synchronize should be not succeed
     * @return  
     */
    public function synchronous(){
        $id=$_POST['id'];
        $row=$this->getDBTable()->select($id);
        $data=$this->getSyncData($row);
        $result=$this->synchronousData($row,$data,$id);
        if($result==1){
            $this->notifySynchronize($id,1);
        }
        $success['success']=$result;
		return $success;
    }
    
    /**
     * 
     * @brief how system handling the data that as need to be synhronous
     *          this will handling when synhronize methode called
     * @param Array/Object $original is the original data 
     * @param Array $adapted that need to be synchronize 
     * @param String $id the id that need to taken 
     * @param String $prop, if for delete then should be delete 
     * @return  boolean succes (1) or fail (0) 
     */
    public abstract function synchronousData($original,$adapted,$id,$prop="");
    
    /**
     * @brief used to adapt the data before send to the synchronize system.
     *          this used to determine how to syncrhonize one data.
     * @param Object or Array $row is the data that need to be adapt (adaptee) 
     * @return  Array of adapted data
     */
    public function getSyncData($row,$prop=""){
        if($this->adapter_synchronous==null){
            return $row;
        }else{
            $this->adapter_synchronous->setProp($prop);
            $adaptee=$this->adapter_synchronous->adapt($row);
            return $adaptee;
        }
    }
    
    /**
     * @brief this used to override 
     *          and give notif that
     *          data not yet synchronize
     * @return  
     */
    public function postToArray(){
        $data=parent::postToArray();
        $data['synch']=0;
        return $data;
    }
    
    /**
     * @brief this one used to handling a synchronize data
     *          if synhrone succed then the data sinkron flag
     *          should be activated
     * @return  Array of string
     */
    public function save(){
		$success=parent::save();
        if($success['id']!="0" || $success['id']!=""){
            $row=$this->getDBTable()->select($success['id']);
            $data=$this->getSyncData($row);
            $result=$this->synchronousData($row,$data,$success['id']);
            if($result==1){
                /* data should be updated from the top */
                $this->notifySynchronize($success['id'],1);
            }
        }
        return $success;
	}
    
    /**
	 * facade design pattern that used for deleting data in Database.
	 * what this function do is :
     * 1. set that unsynchronize
     * 2. Send to Synchronize that this data need to be delete
	 * 3. Get the id of data that want to be delete.
	 * 4. check weather real delete or just hide it (mean change the prop only)
     * 5. if synchronize notify that successfully synchronize
	 * 6. inform the consumer whether success or fail
	 * @return number
	 */
	public function delete(){
		$id=$_POST['id'];
        $this->notifySynchronize($id,0);            //set that not yet synchronize
        $row=$this->getDBTable()->select($id);
        $data=$this->getSyncData($row,"del");
        $result=$this->synchronousData($row,$data,$id);        
        if($result==1){
            if($this->dbtable->isRealDelete()){
                $result=$this->dbtable->delete(null,$id);
            }else{
                /* notify that data sync with the server */
                $this->notifySynchronize($id,1,"del");  //if success set that synchronize
            }
        }
        $success['success']=$result;
		return $success;
	}
     
 }
 

?>