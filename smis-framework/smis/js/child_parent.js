/*parent action*/
function ParentAction(name,page,action,column,child){
	TableAction.call( this, name+"_parent",page,action,column);
	this.children=child;
}
ParentAction.prototype.constructor = ParentAction;
ParentAction.prototype = Object.create( TableAction.prototype );
ParentAction.prototype.setChild=function(c){
	this.children=c;
};


ParentAction.prototype.show_add_form=function(){
	this.clear();
	var self=this;
	var a=this.getSaveData();
	showLoading();
	$.ajax({url:"",data:a,type:'post',success:function(res){
		var json=getContent(res);
		if(json==null) return;
		var id_key=json.id;
		self.edit(id_key);
		dismissLoading();
	}});
};


ParentAction.prototype.edit=function (id){
	var self=this;
	var child=this.children;
	showLoading();	
	var edit_data=this.getEditData(id);
	$.post('',edit_data,function(res){		
		var json=getContent(res);
		if(json==null) return;
		for(var i=0;i<self.column.length;i++){
			if($.inArray(self.column[i],self.noclear)!=-1){
				continue;
			}
				
			var name=self.column[i];
			var the_id="#"+self.prefix+"_"+name;
			smis_edit(the_id,json[""+name]);
		}
		
		$("#"+self.prefix+"_add_form").smodal('show');
		child.view();
		dismissLoading();
	});
};


/*child action*/
function ChildAction(name,page,action,column,parent_key){
	TableAction.call( this, name+"_child",page,action,column);
	this.base_name=name;
	this.parent_key=parent_key;
}
ChildAction.prototype.constructor = ChildAction;
ChildAction.prototype = Object.create( TableAction.prototype );
ChildAction.prototype.getRegulerData=function(){
	var reg_data=TableAction.prototype.getRegulerData.call(this);
	reg_data['id_parent'] = $("#"+this.base_name+"_parent_id").val();	
	$("#"+this.base_name+"_child_"+this.parent_key).val(reg_data['id_parent']);	
	return reg_data;
};



/*binder action*/
function ParentChildAction(name,page,action,parent_column,child_column,parent_key){
	this.child=new ChildAction(name,page,action,child_column,parent_key);
	this.child.setSuperCommand("smis_children");
	this.parent=new ParentAction(name,page,action,parent_column,this.child);
}

ParentChildAction.prototype.setParent=function(parent_data){
	this.parent=parent_data;
	this.parent.setChild(this.child);
};

ParentChildAction.prototype.getParent=function(){
	return this.parent;
};

ParentChildAction.prototype.setChild=function(child_data){
	this.child=child_data;
	this.child.setSuperCommand("smis_children");
	this.parent.setChild(this.child);
	
};

ParentChildAction.prototype.getChild=function(){
	return this.child;
};