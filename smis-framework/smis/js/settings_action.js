function SettingsAction(page,action,column,prefix){
	TableAction.call( this, "",page,action,column);
	smis_format(column,prefix);
	this.is_single=false;
	this.single_variable_name="";
	this.prefix=prefix;
    this.all_slug=new Array();
    this.partial_itemid=new Array();
}

SettingsAction.prototype.constructor = SettingsAction;
SettingsAction.prototype = Object.create( TableAction.prototype );

SettingsAction.prototype.setSingleVariable=function(enable,name){
	this.is_single=enable;
	this.single_variable_name=name;
};

SettingsAction.prototype.getSaveData=function(){
	var save_data=this.getRegulerData();
	save_data['command']="save";
	if(this.is_single){
		var single_var={};
		for(var i=0;i<this.column.length;i++){
			var name=this.column[i];
			var the_id="#"+this.prefix+name;
			single_var[name]=smis_get_data(the_id);			
		}
		save_data[this.single_variable_name]=JSON.stringify(single_var);
	}else{
		for(var i=0;i<this.column.length;i++){
			var name=this.column[i];
			var the_id="#"+this.prefix+name;
			save_data[name]=smis_get_data(the_id);
		}
	}
	return save_data;
};

SettingsAction.prototype.getSaveDataPartial=function(slug){
	var save_data=this.getRegulerData();
	save_data['command']="save";
    var cur_column=this.partial_itemid[slug];
	if(this.is_single){
		var single_var={};
		for(var i=0;i<cur_column.length;i++){
			var name=cur_column[i];
			var the_id="#"+this.prefix+name;
			single_var[name]=smis_get_data(the_id);			
		}
		save_data[this.single_variable_name]=JSON.stringify(single_var);
	}else{
		for(var i=0;i<cur_column.length;i++){
			var name=cur_column[i];
			var the_id="#"+this.prefix+name;
			save_data[name]=smis_get_data(the_id);
		}
	}
	return save_data;
};

SettingsAction.prototype.save=function (){
	var self=this;
	var a=this.getSaveData();
	showLoading();
	$.ajax({url:"",data:a,type:'post',success:function(res){
		var json=getContent(res);
		dismissLoading();
	}});
};

SettingsAction.prototype.savePartial=function (slug){
	var self=this;
    var a=this.getSaveDataPartial(slug);
    a['slug']=slug;
    showLoading();
    $.ajax({url:"",data:a,type:'post',success:function(res){
        var json=getContent(res);
        dismissLoading();
    }});  
};

SettingsAction.prototype.slackPartial=function (slug){
	var self = this;
	var a    = this.getSaveDataPartial(slug);
	var data = $("#"+slug+" tbody tr");
	
	var id   = self.partial_itemid[slug];
	var list_id = new Array();
	$.each(id, function( index, value ) {
		list_id[index] = value;
	});
	var settings = {};
	var section  = 0;
	$.each(data, function( index, value ) {
		if( $(this).children().length<2){
			section++;
		}else{
			var no  			   = $(this).children().eq(0).html();
			var name  			   = $(this).children().eq(1).html();
			var idx	  			   = list_id[index-section];
			var dtx	  			   = smis_get_data_ux("#"+idx);
			settings[no+" "+name]  = dtx;	
		}
	});
	var a 			= this.getRegulerData();
	a['settings']   = settings;
	a['menu']   	= this.page+" -> "+this.action+" -> "+slug;
	a['page']   	= "smis-tools";
	a['action'] 	= "slack_settings_synch";
	showLoading();
	$.ajax({url:"",data:a,type:'post',success:function(res){
        var json=getContent(res);
        dismissLoading();
    }});
};

SettingsAction.prototype.loadPartial=function (slug){
    if($.inArray(slug,this.all_slug)==-1){
        var self=this;
        var a=this.getSaveData();
        a['slug']=slug;
        a['command']="loadPartial";
        showLoading();
        $.ajax({url:"",data:a,type:'post',success:function(res){
            var json=getContent(res);
            $("#"+slug).html(json.table);
            self.partial_itemid[slug]=json.id;
            smis_format(json.id,self.prefix);
            self.all_slug.push(slug);
            self.preLoad();
            dismissLoading();
        }});  
    }
};

SettingsAction.prototype.preLoad=function(){};