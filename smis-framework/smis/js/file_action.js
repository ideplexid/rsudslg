/**
 * 
 * this class used for handling with The
 * FileResponder. the file Responder like
 * DBResponder but instead using MySQL Database
 * it used File as it's Base. the comunication 
 * beetween them using ResponsePackage
 * 
 * @author goblooge
 * @since 09 Des 2016
 * @copyright Nurul Huda <goblooge@gmail.com>
 * @version 1.0.0
 * */

function FileAction(name,page,action){
	var column=new Array("oldname","fileowner","filename","filetype","filemode","isold");
	TableAction.call( this, name,page,action,column);
	this.setDisabledOnEdit(new Array("filetype"));
}
FileAction.prototype.constructor = FileAction;
FileAction.prototype = Object.create( TableAction.prototype );

FileAction.prototype.getBaseFolder=function(){
	return $("#"+this.prefix+"_folder").val();
};

FileAction.prototype.addRegulerData=function(d){
		d['folder']=this.getBaseFolder();
		return d;
};

FileAction.prototype.directory=function(dir){
	var base=this.getBaseFolder();
	base=base+dir+"/"
	$("#"+this.prefix+"_folder").val(base);
	this.view();
};





FileAction.prototype.levelup=function(){
	var curdir=$("#"+this.prefix+"_folder").val();
	if(curdir=="./"){
		return;
	}else if(curdir.indexOf("../")!=-1){
		$("#"+this.prefix+"_folder").val("./")
	}else{
		var trim=curdir.slice(-1);
		if(trim=="/"){
			curdir=curdir.substring(0, curdir.length - 1);
		}
		var last=curdir.lastIndexOf("/");
		var newdir=curdir.substring(0,last+1);
		$("#"+this.prefix+"_folder").val(newdir);
	}
	this.view();
};

FileAction.prototype.view=function(){
	var d=this.getRegulerData();
	d['command']="list";
	showLoading();
	var self=this;
	$.post("",d,function(res){
		var json=getContent(res);
		$("#"+self.prefix+"_list").html(json.list);
		$("#"+self.prefix+"_folder").val(json.basefolder);
		dismissLoading();
	});
};

FileAction.prototype.openfile=function(file){
	var a="</br> <strong>Example</strong></br>";
	a+=this.prefix+".openfile()=function(file){</br>";
	a+="&nbsp;&nbsp;&nbsp;&nbsp;alert('Hello i am File Manager');</br>";
	a+="};";
	var x="Please override <strong>"+this.prefix+".openfile(file)</strong> function "+a;
	
	var base=this.getRegulerData();
	base['filename']=file;
	base['command']="load";
	showLoading();
	$.post("",base,function(res){
		var json=getContent(res);
		dismissLoading();
		showWarning("Not Implemented",x+" <pre>"+json+"</pre>");
	});
};

FileAction.prototype.save=function(){
	if(!this.cekSave()){
		return;
	}
	var self=this;
	var d=this.getSaveData();
	d['command']="save";
	$.post("",d,function(res){
		self.hide_add_form();
		var json=getContent(res);
		self.view();			
		dismissLoading();			
	});
};

FileAction.prototype.editfile=function(nama,type,mode,owner){
	this.show_add_form();
	$("#"+this.prefix+"_oldname").val(nama);
	$("#"+this.prefix+"_filename").val(nama);
	$("#"+this.prefix+"_filetype").val(type);
	$("#"+this.prefix+"_filemode").val(mode);
	$("#"+this.prefix+"_fileowner").val(owner);
	$("#"+this.prefix+"_isold").val("1");
	$("#"+this.prefix+"_filetype").prop("disabled",true);
};

FileAction.prototype.showimage=function(nama){
	var curdir=$("#"+this.prefix+"_folder").val();
	var divtext="<div class='salman_img_name'>"+nama+"</div>";
	var clearing="<div class='clear'></div>";
	var img="<div class='salman_img'><img src='"+curdir+nama+"' ></div>";
	showWarning("Image","<div class='salman_img_container'>"+divtext+clearing+img+"</div>");
};

FileAction.prototype.showaudio=function(nama){
	var curdir=$("#"+this.prefix+"_folder").val();
	var control="<audio controls><source src=\""+curdir+nama+"\">";
	control+="Your browser does not support the audio element.";
	control+="</audio>";
	
	var divtext="<div class='salman_img_name'>"+nama+"</div>";
	var clearing="<div class='clear'></div>";
	var img="<div class='salman_img'>"+control+"</div>";
	showWarning("Audio","<div class='salman_img_container'>"+divtext+clearing+img+"</div>");
};

FileAction.prototype.download=function(name){
	var data=this.getRegulerData();
	data['command']="download";
	data['filename']=name;
	download(data);
	
};

FileAction.prototype.removefile=function(name){
	var curdir=$("#"+this.prefix+"_folder").val();
	var self=this;
	bootbox.confirm("Warning Remove File/Folder Cannot Recovered, Are you sure to remove <strong>"+name+"</strong> ?", function(result) {
		  if(result){
			var d=self.getSaveData();
			d['command']="remove";
			d['filename']=name;		
			$.post("",d,function(res){
				var json=getContent(res);
				self.view();
				dismissLoading();
			});
		  }
	}); 
};
