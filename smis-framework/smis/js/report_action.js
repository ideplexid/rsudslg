function ReportAction(name,page,action,column){
	var total=column.length;
	column[++total]='from_date';
	column[++total]='to_date';
	TableAction.call( this, name,page,action,column);
	this.diagram_holder=null;
	this.diagram_object=null;
	this.last_source=new Array();
	this.title="";
	this.source=null;
	this.ymin=0;
	this.ymax=10;
	this.xkey="";
	this.ykey=[];
	this.labels=[];
	
}
ReportAction.prototype.constructor = ReportAction;
ReportAction.prototype = Object.create( TableAction.prototype );


ReportAction.prototype.getViewData=function(){
	var view_data=this.getRegulerData();
	view_data['command']="list";
	view_data['kriteria']=$("#"+this.prefix+"_kriteria").val();
	view_data['number']=$("#"+this.prefix+"_number").val();
	view_data['max']=$("#"+this.prefix+"_max").val();
	view_data['mode']=$("#"+this.prefix+"_mode_model").val();
	for(var i=0;i<this.column.length;i++){
		var name=this.column[i];
		view_data[name]=$("#"+this.prefix+"_"+name).val();
	}
	view_data = this.addViewData(view_data);
	return view_data;
};

ReportAction.prototype.setXKey=function(xkey){
	this.xkey=xkey;
};

ReportAction.prototype.setYKey=function(ykey){
	this.ykey=ykey;
};

ReportAction.prototype.setLabels=function(labels){
	this.labels=labels;
};


ReportAction.prototype.setMax=function(element){
	var max=Number($(element).val());
	this.ymax=max;
	this.setModel();
};

ReportAction.prototype.setMin=function(element){
	var min=Number($(element).val());
	this.ymin=min;
	this.setModel();
};



ReportAction.prototype.view=function(){
	if(!this.cekSave()){
		return;
	}
	showLoading();
	var self=this;
	var view_data=this.getViewData();
	$.post('',view_data,function(res){
		var json=getContent(res);
		if(json==null) {
			return;
		}
		
		$("#"+self.prefix+"_add_form").smodal('hide');
		$("#"+self.prefix+"_list").html(json.list);
		$("#"+self.prefix+"_pagination").html(json.pagination);		
		
		if(self.diagram_holder!=null){
			self.last_source=json.diagram_data;
			self.setModel($("#"+self.prefix+"_tipe_model").val());
		}
		
		dismissLoading();
	});
};

ReportAction.prototype.setDiagramHolder=function(holder){
	this.diagram_holder=holder;
};

ReportAction.prototype.getSource=function(){
	var tipe= $("#"+this.prefix+"_tipe_model").val();
	if(tipe=="donut"){		
		var adata={
			element: this.diagram_holder,
			data:this.sourcePieAdapter(this.last_source),
		}
		return adata;
	}else{
		var adata={
			element: this.diagram_holder,
			data:this.last_source,
		    xkey: this.xkey,
		    ykeys: this.ykey,
		    labels: this.labels,
			ymin:this.ymin,
			ymax:this.ymax,
			resize:false
		}
		return adata;
	}	
};

ReportAction.prototype.initDiagram=function(tipe_model){
	$("#"+this.diagram_holder).html("");
	var sources=this.getSource();
	if(tipe_model=="bar"){
		new Morris.Bar(sources);
	}else if(tipe_model=="line"){
		new Morris.Line(sources);
	}else if(tipe_model=="area"){
		new Morris.Area(sources);
	}else if(tipe_model=="donut"){
		new Morris.Donut(sources);
	}
};

ReportAction.prototype.setTitle=function(title){
	this.title=title;
};

ReportAction.prototype.setModel=function(){
	var tipe_model=$("#"+this.prefix+"_tipe_model").val();
	this.initDiagram(tipe_model);
};

ReportAction.prototype.setSource=function(source){
	this.last_source=source;
	var tipe= $("#"+this.prefix+"_tipe_model").val();
	this.initDiagram(tipe);
};

ReportAction.prototype.sourcePieAdapter=function(json){
	alert("Override Source Pie Adapter Methode");
};


