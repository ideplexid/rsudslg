/*parent action
 * 
 * */
function MapsAction(name,page,action,column,lat_center,lon_center,start_zoom){
	TableAction.call( this, name,page,action,column);
	this.tipe=null;
	this.map=null;
	this.list_marker=new Array();
	this.area=null;
	this.current=new Array();
	loadScript("google_maps_"+page,lat_center,lon_center,start_zoom,this);	
	this.current_infowindow=null;
}

MapsAction.prototype.constructor = TableAction;
MapsAction.prototype = Object.create( TableAction.prototype );

MapsAction.prototype.MapsClick=function(id){	
	showLoading();	
	var data=this.getRegulerData();
	data['command']='maps_click';
	data['id']=id;
	
	$.post('',data,function(res){		
		var json=getContent(res);
		if(json==null) return;
		dismissLoading();
	});
};

MapsAction.prototype.clearCurrent=function(){	
	for (var i = 0; i < this.current.length; i++) {
		   var marker=this.current[i];
		   marker.setMap(null);
	}
	this.current=new Array();
};

MapsAction.prototype.view=function(){
	showLoading();
	var self=this;	
	var view_data=this.getViewData();
	$.post('',view_data,function(res){
		var json=getContent(res);
		if(json==null) {
			return;
		}
		$("#"+self.prefix+"_add_form").smodal('hide');
		$("#"+self.prefix+"_list").html(json.list);
		$("#"+self.prefix+"_pagination").html(json.pagination);		
		var m=json.maps;
		self.clearCurrent();
		$.each(m, function(k,v) {
			self.addObject(v);
		});
		dismissLoading();		
	});
};

MapsAction.prototype.addObject=function(v){
	var alamat=v.alamat;
	var color=v.color;
	var icon=v.icon;
	var nama=v.nama;
	var tipe=v.tipe;
	var gps=v.gps;
	var ket=v.ket;
	var code=v.id;
	
	if(tipe=="point"){
		var latlon=null;
		$.each(gps, function(k,gp) {
			var lat=gp.lat;
			var lng=gp.lng;
			latlon=new google.maps.LatLng(lat,lng);
		});		
		var marker = new google.maps.Marker({
		    position: latlon,
		    icon:icon
		  });
		marker.setMap(this.map);
		this.setInfoWindow(marker,nama,alamat,ket,code);		
		this.current.push(marker);
	}else if(tipe=="area"){
		var bounch=new Array();
		$.each(gps, function(k,gp) {
			var lat=gp.lat;
			var lng=gp.lng;
			var latlon=new google.maps.LatLng(lat,lng);
			bounch.push(latlon);
		});
		var area= new google.maps.Polygon({
		    paths: bounch,
		    strokeColor: color,
		    strokeOpacity: 0.8,
		    strokeWeight: 3,
		    fillColor: color,
		    fillOpacity: 0.35
		  });
		area.setMap(this.map);	
		this.setTooltip(area, nama,alamat,ket,code);		
		this.current.push(area);
	}else{
		var bounch=new Array();
		$.each(gps, function(k,gp) {
			var lat=gp.lat;
			var lng=gp.lng;
			var latlon=new google.maps.LatLng(lat,lng);
			bounch.push(latlon);
		});
		var area= new google.maps.Polyline({
		    path: bounch,
		    geodesic: true,
		    strokeColor: color,
		    strokeOpacity: 1.0,
		    strokeWeight: 5
		  });
		area.setMap(this.map);	
		this.setTooltip(area, nama,alamat,ket,code);		
		this.current.push(area);
	}
	
};



MapsAction.prototype.pushPoint=function(latlon){
	var self=this;
	if(this.tipe=="point"){		
		var latlng = {lat: latlon.lat(),lng: latlon.lng()};
		showLoading();
		geocoder.geocode({'location': latlng}, function(results, status) {
		dismissLoading();
          if (status === 'OK') {
            if (results[1]) {
              geo_address=results[1].formatted_address;
            } else {
              geo_address="";
			  smis_alert("Nothing Found", "Google Maps Doesn't Found Anything", "alert-info");
            }
          } else {
            showWarning("Failed",'Geocoder failed due to: ' + status);
          }
		  
        });
		if(this.list_marker.length>=1)
			return;
	}
		
		
	var marker = new google.maps.Marker({
	    position: latlon,
	    map: this.map
	  });
	marker.setDraggable(true);
	google.maps.event.addListener(marker, 'dragend', function(event) {
		if(self.tipe=="area")	self.drawArea();
		else self.drawLine();
	 });
	this.list_marker.push(marker);
	if(this.tipe=="area")	this.drawArea();
	else this.drawLine();
};

MapsAction.prototype.ok=function(latlon){
	if(this.list_marker.length<=0){
		showWarning("Warning","Nothing to Save");
		this.cancel();
	}
	this.choose(this.tipe);
	this.show_add_form();
	
};

MapsAction.prototype.choose=function(tipe){
	if(tipe=='point'){
		$("."+this.prefix+"_color").hide();
		$("."+this.prefix+"_icon").show();		
	}else{
		$("."+this.prefix+"_icon").hide();
		$("."+this.prefix+"_color").show();
	}
};


MapsAction.prototype.edit=function (id){
	var self=this;
	showLoading();	
	var edit_data=this.getEditData(id);
	$.post('',edit_data,function(res){		
		var json=getContent(res);
		if(json==null) return;
		for(var i=0;i<self.column.length;i++){
			var name=self.column[i];
			$("#"+self.prefix+"_"+name).val(json[""+name]);
			self.choose(json["tipe"]);
		}
		$("#"+self.prefix+"_icon").select2("val", json["icon"]);
		dismissLoading();
		$("#"+self.prefix+"_add_form").smodal('show');
	});
};



MapsAction.prototype.show_add_form=function(){
	this.clear();
	if(this.list_marker.length>0){
		var gps="";
		for (var i = 0; i < this.list_marker.length; i++) {
			   var marker=this.list_marker[i];
			   var lat=marker.position.lat();
			   var lng=marker.position.lng();
			   gps+=lat+" # "+lng+" | ";
			}
		
		$("#"+this.prefix+"_tipe").val(this.tipe);
		$("#"+this.prefix+"_gps").val(gps);
		$("#"+this.prefix+"_alamat").val(geo_address);
		$("#"+this.prefix+"_add_form").smodal('show');
		this.cancel();
	}
};


MapsAction.prototype.setMap=function(the_map){
	var self=this;
	this.map=the_map;
	 google.maps.event.addListener(map, 'click', function(event) {
		 if(self.tipe==null)
			 return;
		 self.pushPoint(event.latLng);
	 });
	 this.view();
};

MapsAction.prototype.add=function(type){
	this.tipe=type;
	var msg="Now Your are in Area Mode, Click in the maps more than 3 times to take the point of area, then click Ok Button , or cancel button to abort.";
	if(type=="point"){
		msg="Now Your are in Point Mode, Click in the one time to take the point , then click Ok Button , or cancel button to abort.";
	}else if(type=="line"){
		msg="Now Your are in Line Mode, Click in the two times or more to take the point or line, then click Ok Button , or cancel button to abort.";
	}
	
	$("."+this.prefix+"_ok_cancel").show();
	$("."+this.prefix+"_point_area").hide();	
	showWarning("Warning",msg);
};

MapsAction.prototype.drawArea=function(){
	var latlng=new Array();
	for (var i = 0; i < this.list_marker.length; i++) {
		   var marker=this.list_marker[i];
		   latlng.push(marker.position);
		}
	if(this.area!=null){
		this.area.setMap(null);
	}
	this.area= new google.maps.Polygon({
	    paths: latlng,
	    strokeColor: '#FF0000',
	    strokeOpacity: 0.8,
	    strokeWeight: 3,
	    fillColor: '#FF0000',
	    fillOpacity: 0.35
	  });
	this.area.setMap(this.map);	
};

MapsAction.prototype.drawLine=function(){
	var latlng=new Array();
	for (var i = 0; i < this.list_marker.length; i++) {
		   var marker=this.list_marker[i];
		   latlng.push(marker.position);
		}
	if(this.area!=null){
		this.area.setMap(null);
	}
	
	this.area = new google.maps.Polyline({
		    path: latlng,
		    geodesic: true,
		    strokeColor: '#FF0000',
		    strokeOpacity: 1.0,
		    strokeWeight: 2
		  });

	this.area.setMap(this.map);	
};

MapsAction.prototype.cancel=function(){	
	$("."+this.prefix+"_ok_cancel").hide();
	$("."+this.prefix+"_point_area").show();
	for (var i = 0; i < this.list_marker.length; i++) {
	   var marker=this.list_marker[i];
	   marker.setMap(null);
	}
	if(this.area!=null)
		this.area.setMap(null);
	
	this.list_marker=new Array();
	this.tipe=null;
	this.area=null;
	geo_address=null;
};


MapsAction.prototype.setInfoWindow=function (marker,nama,alamat,ket,code){
	var self=this;
	var content="<div>Nama : "+nama+"</div>";
	content+="<div> Alamat : "+alamat+"</div>";
	content+="<div>"+ket+"</div>";
	content+="<input type='button' class='btn btn-inverse' onclick=\""+this.action+".detail('"+code+"')\" value='Detail' >";
	var infoWindow = new google.maps.InfoWindow({
        content: content
    });

    google.maps.event.addListener(marker, 'click', function () {
    	if(self.current_infowindow!=null)
    		self.current_infowindow.close();
    	
        infoWindow.open(map, marker);
    	self.current_infowindow=infoWindow;
        
    });
};

MapsAction.prototype.detail=function (id){
	showLoading();	
	var edit_data=this.getEditData(id);
	edit_data['command']='maps_click';
	$.post('',edit_data,function(res){		
		getContent(res);
		dismissLoading();
		
	});
};

MapsAction.prototype.setTooltip=function (marker, nama,alamat,ket,code) {
	
	var content="<div>Nama : "+nama+"</div>";
	content+="<div> Alamat : "+alamat+"</div>";
	content+="<div>"+ket+"</div>";
	
	var self=this;
    var tooltip = new MarkerWithLabel({
     position: new google.maps.LatLng(0,0),
     draggable: false,
     raiseOnDrag: false,
     map: map,
     labelContent: content,
     labelAnchor: new google.maps.Point(30, 20),
     labelClass: "smis-poly-tooltip", // the CSS class for the label
     labelStyle: {opacity: 1.0},
     icon: "http://a.com/x.jpg",//plce icon here
     visible: false
    });

     google.maps.event.addListener(marker, "mousemove", function(event) {
       tooltip.setPosition(event.latLng);
       tooltip.setVisible(true);
     });
     google.maps.event.addListener(marker, "mouseout", function(event) {
       tooltip.setVisible(false);
     });
     
     google.maps.event.addListener(tooltip, 'click', function () {
     	 self.detail(code);
     });
     
   };







/*not part of object*/
function initialize() {	
	  var mapOptions = {
	    zoom: start_zoom,
	    center: new google.maps.LatLng(lat_center,lon_center)
	  };
		
	  map = new google.maps.Map(document.getElementById(google_load_page),mapOptions);
	  $.getScript( "smis-framework/smis/js/markerwithlabel_packed.js", function( data, textStatus, jqxhr ) {
		  object_map.setMap(map);
	  });
	  
	  geocoder= new google.maps.Geocoder;
	  
	  var markers = [];
	  // Create the search box and link it to the UI element.
	  var input = /** @type {HTMLInputElement} */(
	      document.getElementById('pac-input'));
	  map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

	  var searchBox = new google.maps.places.SearchBox(
	    /** @type {HTMLInputElement} */(input));

	  // [START region_getplaces]
	  // Listen for the event fired when the user selects an item from the
	  // pick list. Retrieve the matching places for that item.
	  google.maps.event.addListener(searchBox, 'places_changed', function() {
	    var places = searchBox.getPlaces();

	    if (places.length == 0) {
	      return;
	    }
	    for (var i = 0, marker; marker = markers[i]; i++) {
	      marker.setMap(null);
	    }

	    // For each place, get the icon, place name, and location.
	    markers = [];
	    var bounds = new google.maps.LatLngBounds();
	    for (var i = 0, place; place = places[i]; i++) {
	      var image = {
	        url: place.icon,
	        size: new google.maps.Size(71, 71),
	        origin: new google.maps.Point(0, 0),
	        anchor: new google.maps.Point(17, 34),
	        scaledSize: new google.maps.Size(25, 25)
	      };

	      // Create a marker for each place.
	      var marker = new google.maps.Marker({
	        map: map,
	        icon: image,
	        title: place.name,
	        position: place.geometry.location
	      });

	      markers.push(marker);

	      bounds.extend(place.geometry.location);
	    }

	    map.fitBounds(bounds);
	  });
	  // [END region_getplaces]

	  // Bias the SearchBox results towards places that are within the bounds of the
	  // current map's viewport.
	  google.maps.event.addListener(map, 'bounds_changed', function() {
	    var bounds = map.getBounds();
	    searchBox.setBounds(bounds);
	  });
	  
	  
}

function setMap(){
	object_map.setMap(map);
}


var google_load_page="";
var lat_center="";
var lon_center="";
var map=null;
var geocoder=null;
var start_zoom=10;
var object_map=null;
var geo_address=null;
function loadScript(page,lat,lon,zoom,obj) {
	google_load_page=page;
	lat_center=lat;
	lon_center=lon;
	start_zoom=zoom;
	object_map=obj;
	    
	var script = document.createElement('script');
	script.type = 'text/javascript';
	script.src = 'https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=places&' +
	      'callback=initialize&&key=AIzaSyDVUl5iuUbxug0Wgq1KGt5cdhFtL5ufOC0';
	document.body.appendChild(script);
}

