function TableAction(prefix,page,action,column){
	this.initialize(prefix, page, action, column);
};

TableAction.prototype.initialize		= function(prefix,page,action,column){
	this.page							= page;
	this.action							= action;
	this.column							= column;
	this.column_disabled_on_edit		= new Array();
	this.prefix							= prefix;
	this.super_command					= "";
	this.prototype_name					= "";
	this.prototype_slug					= "";
	this.prototype_implement			= "";
	this.print_area						= "print_table_"+prefix;
	this.parent_modal					= null;
	this.show_parent_modal_in_chooser	= true;
	this.current_chooser				= null;
	this.view_data_null_message			= "View Data is NULL";
	this.add_data_null_message			= "Add Data is NULL";	
	this.multiple_input					= false;
	this.edit_clear_for_no_clear		= false;
	this.noclear						= new Array();
    this.own_chooser_data				= false;
    this.duplicate_loop_runner			= false;
    this.duplicate_loop_name_view		= null;
    this.duplicate_loop_timeout			= 300;
    this.json_column					= new Array();
    this.is_post_action_del				= false;
	smis_format(column,prefix+"_"); 
	var fixhead							= $("#table_"+prefix).data("fix-header");
	if(fixhead=="y"){ 
		$("#table_"+prefix).sfixhead(); 
	} 
	this.focus_on_multi					= "";
    this.popover						= false;
};

TableAction.prototype.addJSONColumn		= function(column_name,json_grup){
	this.json_column[column_name]		= json_grup;
};

TableAction.prototype.setPopover		= function(enabled){
	this.popover						= enabled;
};

TableAction.prototype.setDuplicateNameView = function(set){
	this.duplicate_loop_name_view		   = set;
};

TableAction.prototype.setDuplicateTimeOut = function(time){
	this.duplicate_loop_timeout			  = time;
};

TableAction.prototype.setOwnChooserData	  = function(set){
	this.own_chooser_data				  = set;
};

TableAction.prototype.getChooserData	  = function(set){
	if(this.own_chooser_data){
        return this.getRegulerData();
    }else{
        return null;
    }
};

TableAction.prototype.setEditClearForNoClear = function(set){
	this.edit_clear_for_no_clear			 = set;
};


TableAction.prototype.setEnableAutofocus	 = function(){
	$('#'+this.action+'_add_form').on('shown.bs.modal', function() {
		  $(this).find('[autofocus]').focus();
	});
};

TableAction.prototype.setFocusOnMultiple	 = function(focuson){
	this.focus_on_multi						 = focuson;
};

TableAction.prototype.setMultipleInput		 = function(i){
	this.multiple_input						 = i;
};
TableAction.prototype.setNextEnter			 = function(){	
	var savefocus							 = "#"+this.prefix+"_save";
	for(var i=0;i<this.column.length;i++){
		var name		= this.column[i];
		var the_id		= "#"+this.prefix+"_"+name;
		var nxt			= $(the_id).data('next-enter');
		var type		= $(the_id).attr('type');	
		if(nxt!=null && nxt!=""){
			nxtid		= "#"+this.prefix+"_"+nxt;
			if($(the_id).is(':checkbox')){
				next_enter(the_id,nxtid,savefocus,false,this.prefix);
			}else if($(the_id).attr('typical')=="money"){
				next_enter(the_id,nxtid,savefocus,false,this.prefix);
			}else if($(the_id).hasClass("typehead")){
				/**type head do nothing */
			}else if(type=="text"){
				next_enter(the_id,nxtid,savefocus,false,this.prefix);
			}else if(type=="select"){
				next_enter(the_id,nxtid,savefocus,"select",this.prefix);
			}else if($(the_id).is("textarea")){
				next_enter(the_id,nxtid,savefocus,"textarea",this.prefix);
			}
		}
	}
};



TableAction.prototype.setShowParentModalInChooser=function(enabled){
	this.show_parent_modal_in_chooser		= enabled;	
};

TableAction.prototype.getShowParentModalInChooser=function(){
	return this.show_parent_modal_in_chooser;
};


/*REGULER COMMAND*/
TableAction.prototype.setSuperCommand=function(command){
	this.super_command						= command;
	return this;
};

TableAction.prototype.addNoClear=function(val){
	this.noclear.push(val);
	return this;
};

TableAction.prototype.setNoClear=function(ary){
	this.noclear				= ary;
	return this;
};

TableAction.prototype.setPrototipe=function(name,slug,implement){
	this.prototype_name			= name;
	this.prototype_slug			= slug;
	this.prototype_implement	= implement;
	return this;
};

TableAction.prototype.getSuperCommand=function(){
	return this.super_command;
};

TableAction.prototype.clear=function(show_first_form=false){	
	for(var i=0;i<this.column.length;i++){
		if($.inArray(this.column[i],this.noclear)!=-1 && !show_first_form)
			continue;
		var the_id		= "#"+this.prefix+"_"+this.column[i];
		smis_clear(the_id);
	}
	this.enabledOnNotEdit(this.column_disabled_on_edit);
	var id_alt			= "#modal_alert_"+this.prefix+"_add_form";		
	$(id_alt).html("");
	return this;
};

TableAction.prototype.setParentModal=function(modal){	
	this.parent_modal	= modal;
};

TableAction.prototype.hideParentModal=function(){	
	if(this.parent_modal!=null){
		$("#"+this.parent_modal).smodal('hide');
	}
	return this;
};

TableAction.prototype.showParentModal=function(){	
	if(this.parent_modal!=null){
		$("#"+this.parent_modal).smodal('show');
	}
	return this;
};

TableAction.prototype.getModalNama=function(modal){	
	return this.prefix+"_add_form";
};

TableAction.prototype.show_form=function(){
	$("#"+this.prefix+"_add_form").smodal('show');
	this.after_form_shown();
	return this;
};

TableAction.prototype.after_form_shown=function(){
	/**override this function */
};

TableAction.prototype.show_add_form=function(){
	this.clear(true);
	this.show_form();
	return this;
};

TableAction.prototype.clipboard=function(){
	copyToClipboard(document.getElementById("table_"+this.prefix),"outer");
	showWarning("Copied","Table sudah Terkopi di Clipboard. Silakan Buka Microsoft Excel Kemudian pilih tekan <strong>Ctrl + V</strong> atau tekan icon <strong><i class='fa fa-clipboard'></i> Paste</strong>  atau Klik Kanan pada cell kemudian <strong><i class='fa fa-clipboard'></i> Paste </strong> ");
};

TableAction.prototype.hide_add_form=function(){
	$("#"+this.prefix+"_add_form").smodal('hide');
	this.showParentModal();
	return this;
};

TableAction.prototype.paginate=function(e){
	var data=$(e).attr('val');
	$("#"+this.prefix+"_number").val(data);
	this.view();
	return this;
};

TableAction.prototype.paginateNext=function(e){
	var data=Number($("#"+this.prefix+"_number").val())+1;
	$("#"+this.prefix+"_number").val(data);
	this.view();
	return this;
};

TableAction.prototype.paginatePrev=function(e){
	var data=Number($("#"+this.prefix+"_number").val())+1;
	$("#"+this.prefix+"_number").val(data);
	this.view();
	return this;
};



TableAction.prototype.selected=function(json){
	/*Override this*/
	showWarning("Override Function "+this.prefix+".selected()", "<font class='label label-primary'>JSON Object</font></br>"+JSON.stringify(json));
};

TableAction.prototype.more_option=function(slug){
	/*Override this*/
	showWarning("Override Function "+this.prefix+".more_option()", "Override Function "+this.prefix+".more_option() with SLUG "+slug);
};

TableAction.prototype.select=function(id){
	hide_chooser();
	showLoading();
	var self=this;
	var edit_data=this.getEditData(id);
	$.post('',edit_data,function(res){
		var json=getContent(res);
		if(json==null) {
			return;
		}
		self.selected(json);
        dismissLoading();
        self.afterSelect(json);
	});
	return this;
};

TableAction.prototype.afterSelect=function(json){
    /*do something here*/
};


TableAction.prototype.setDisabledOnEdit=function(kolom){
	this.column_disabled_on_edit=kolom;
	return this;
};
TableAction.prototype.disabledOnEdit=function(kolom){
	for(var i=0;i<kolom.length;i++){
		var name	= kolom[i];
		var idx		= "#"+this.prefix+"_"+name;
		if($(idx).prop("tagName")=="A"){
			/**for button with a */
			var onclick = $(idx).attr("onclick");
			$(idx).attr("onclick","");
			$(idx).data("onclick",onclick);
		}else{
			$(idx).prop('disabled', true);
		}
	}
	return this;
};

TableAction.prototype.enabledOnNotEdit=function(kolom){
	for(var i=0;i<kolom.length;i++){
		var name=kolom[i];
		var idx		= "#"+this.prefix+"_"+name;
		if($(idx).prop("tagName")=="A"){
			/**for button with a */
			var onclick = $(idx).data("onclick");
			if(onclick!=""){
				$(idx).attr("onclick",onclick);
				$(idx).data("onclick","");	
			}
		}else{
			$(idx).prop('disabled', false);
		}			
	}
	return this;
};

/*DATA METHODE*/
TableAction.prototype.addRegulerData=function(reg_data){
	return reg_data;
};

TableAction.prototype.getRegulerData=function(){
	var reg_data={	
			page:this.page,
			action:this.action,
			super_command:this.super_command,
			prototype_name:this.prototype_name,
			prototype_slug:this.prototype_slug,
			prototype_implement:this.prototype_implement
			};
	reg_data=this.addRegulerData(reg_data);
	return reg_data;
};

TableAction.prototype.chooser=function(modal,elm_id,param,action,modal_title){
    var the_data=action.getChooserData();
    if(the_data==null){
       var data=this.getRegulerData();
       the_data=this.addChooserData(data);
    }
    var self=this;
	the_data['super_command']=param;
	$.post('',the_data,function(res){
		show_chooser(self,param,res,action.getShowParentModalInChooser(),modal_title);
		action.view();
		action.focusSearch();
		this.current_chooser=action;
		CURRENT_SMIS_CHOOSER=action;
	});
	return this;
};

TableAction.prototype.addChooserData=function(c_data){
	return c_data;
};

TableAction.prototype.addViewData=function(view_data){
	return view_data;
};

TableAction.prototype.addSaveData=function(view_data){
	return view_data;
};

TableAction.prototype.addExcelData=function(excel_data){
	return excel_data;
};


TableAction.prototype.getViewData=function(){
	var view_data=this.getRegulerData();
	view_data['command']="list";
	if($( "#"+this.prefix+"_kriteria").length)
		view_data['kriteria']=$("#"+this.prefix+"_kriteria").val();
	if($( "#"+this.prefix+"_number").length)
		view_data['number']=$("#"+this.prefix+"_number").val();
	if($( "#"+this.prefix+"_max").length)
		view_data['max']=$("#"+this.prefix+"_max").val();
	view_data=this.addViewData(view_data);
	return view_data;
};


TableAction.prototype.addDelData=function(del_data){
	return del_data;
};

TableAction.prototype.addDuplicateData=function(duplicate_data){
	return duplicate_data;
};


TableAction.prototype.addReloadData=function(reload_data){
	return reload_data;
};

TableAction.prototype.getDelData=function(id){
	var del_data=this.getRegulerData();
	del_data['command']="del";
	del_data['id']=id;
	del_data=this.addDelData(del_data);
	return del_data;
};

TableAction.prototype.getDuplicateData=function(id){
	var duplicate_data=this.getRegulerData();
	duplicate_data['command']="duplicate";
	duplicate_data['id']=id;
	duplicate_data=this.addDuplicateData(duplicate_data);
	return duplicate_data;
};

TableAction.prototype.addEditData=function(edit_data){
	return edit_data;
};

TableAction.prototype.getEditData=function(id){
	var edit_data=this.getRegulerData();
	edit_data['command']="edit";
	edit_data['id']=id;
	edit_data=this.addEditData(edit_data);
	return edit_data;
};

TableAction.prototype.addSelectData=function(select_data){
	return select_data;
};

TableAction.prototype.getSelectData=function(id){
	var edit_data=this.getRegulerData();
	edit_data['command']="select";
	edit_data['id']=id;
	edit_data=this.addSelectData(edit_data);
	return edit_data;
};

TableAction.prototype.getSaveData=function(){
	var save_data=this.getRegulerData();
	save_data['command']="save";
	for(var i=0;i<this.column.length;i++){		
		var name=this.column[i];
		var the_id="#"+this.prefix+"_"+name;
        var data_value=smis_get_data(the_id);
        if( name in this.json_column){
            /*handling part json column*/
            var json_grup_name=this.json_column[name];
            if(!(json_grup_name in save_data)){
                save_data[json_grup_name]=new Object();
            }
            save_data[json_grup_name][name]=data_value;
        }else{
            /**handling reguler data*/
            save_data[name]=data_value
        }
	}
    save_data=this.addSaveData(save_data);
	return save_data;
};


TableAction.prototype.print=function(){
	var area=$("#"+this.print_area).html();
	smis_print(area);
	return this;
};


TableAction.prototype.print_preview=function(id){
	var data=this.getRegulerData();
	data['command']='print-element';
	data['slug']='print-element';
	data['id']=id;
	$.post("",data,function(res){
		var json=getContent(res);
		if(json==null) return;
		showPrintPreview(json);
	});
	return this;
};

TableAction.prototype.detail=function(id){
	var data=this.getRegulerData();
	data['command']='detail';
	data['id']=id;
    showLoading();
	$.post("",data,function(res){
		var json=getContent(res);
        dismissLoading();
		showWarning("Detail of "+id,json);
	});
	return this;
};

TableAction.prototype.excel=function(){
	var data=this.getViewData();
	data['command']="excel";
	download(data);
};

TableAction.prototype.excel_element=function(id){
	var data=this.getEditData(id);
	data['command']="excel-element";
	download(data);
	return this;
};

TableAction.prototype.pdf=function(){
	var data=this.getViewData();
	data['command']="pdf";
    showLoading();
    $.post("",data,function(res){
		dismissLoading();
		var json = getContent(res);
		console.log(json);
		var getUrl = window.location['pathname']+json;
		window.open(getUrl, 'pdf');
	});
	return this;
};


TableAction.prototype.pdf_element=function(id){
	var data=this.getEditData(id);
	data['command']="pdf-element";
    showLoading();
    $.post("",data,function(res){
		dismissLoading();
		var json = getContent(res);
		console.log(json);
		var getUrl = window.location['pathname']+json;
		window.open(getUrl, 'pdf');
	});
	return this;
};






/*ACTION METHODE*/
//run after save or delete
TableAction.prototype.postAction=function(id){
	
};

TableAction.prototype.afterview=function (json){};

TableAction.prototype.initPopover=function(){
    if(this.popover){
        $('#'+this.prefix+'_list [data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});      
	}
	return this;
};

TableAction.prototype.view=function(){	
	var self		= this;
	var view_data	= this.getViewData();
	if(view_data==null){
		showWarning("Error",this.view_data_null_message);
		return;
	}
	showLoading();
	$.post('',view_data,function(res){
		var json=getContent(res);
		if(json==null) {
			
		}else{
			$("#"+self.prefix+"_list").html(json.list);
			$("#"+self.prefix+"_pagination").html(json.pagination);	
            self.initPopover();
		}
		self.afterview(json);
		dismissLoading();
	});
	return this;
};


TableAction.prototype.search=function(e){
	var charCode = (typeof e.which === "number") ? e.which : e.keyCode;
	if(charCode==13){
		this.view();
	}
	return this;
};


TableAction.prototype.duplicate=function(id){
	var self		= this;
	var del_data	= this.getDuplicateData(id);	
	showLoading();
    $.post('',del_data,function(res){
        var json=getContent(res);
        if(json==null) return;
        self.view();
        dismissLoading();
        self.postAction(id);
	});
	return this;
};

TableAction.prototype.del=function(id){
	var self		= this;
	var del_data	= this.getDelData(id);	
	bootbox.confirm("Anda yakin Ingin Menghapus ?", function(result) {
	   if(result){
		   showLoading();
		    $.post('',del_data,function(res){
				var json=getContent(res);
				if(json==null) return;
				self.view();
				dismissLoading();
                self.is_post_action_del=true;
				self.postAction(id);
			});
	   }
	}); 
	return this;
};


TableAction.prototype.edit=function (id){
	var self		= this;
    var json_obj	= new Array();
	showLoading();	
	var edit_data	= this.getEditData(id);
	$.post('',edit_data,function(res){		
		var json	= getContent(res);
		if(json==null) return;
		for(var i=0;i<self.column.length;i++){
			if($.inArray(self.column[i],self.noclear)!=-1 && !self.edit_clear_for_no_clear){
				continue;
			}
            var name	= self.column[i];
			var the_id	= "#"+self.prefix+"_"+name;
            if( name in self.json_column && self.json_column.length>0){
				/** this function handling if there is a column that part of another column */
                var json_grup_name=self.json_column[name];
                if(json[json_grup_name]==""){
                    continue;
                }
                if(!(json_grup_name in json_obj)){
				    json_obj[json_grup_name]=$.parseJSON(json[json_grup_name]);
                }
                smis_edit(the_id,json_obj[json_grup_name][""+name]);    
            }else{
                /** this function handling reguler column **/
                smis_edit(the_id,json[""+name]);    
            }
		}
		dismissLoading();
		self.disabledOnEdit(self.column_disabled_on_edit);
		self.show_form();
	});
	return this;
};

TableAction.prototype.set	= function(name,value){
	var id = "#"+this.prefix+"_"+name;
	smis_edit(id,value);
	return this;
}

TableAction.prototype.get	= function(name){
	var id = "#"+this.prefix+"_"+name;
	return smis_get_data(id);
}

TableAction.prototype.cekSave=function(){
	var result		= new Object();
    result.good		= true;
	result.msg		= "";
	for(var i=0;i<this.column.length;i++){
		var name	= this.column[i];
		var the_id	= "#"+this.prefix+"_"+name;
		var type	= $(the_id).attr('type');
		$(the_id).removeClass("error_field");
		
		if(type=='text' || type=='textarea' || type=="select" ){
			var empty	= $(the_id).attr('empty');
			var typical	= $(the_id).attr('typical');
			var val		= $(the_id).val();
			var name	= $(the_id).attr('name');
			if(empty=='n' && (val==null || val==''  )    ){
				result.msg	= result.msg+" </br> <strong>"+name+"</strong> Tidak Boleh Kosong";
				result.good	= false;
				$(the_id).addClass("error_field");
			}

			if(empty=='n' && $(the_id).hasClass('mydate') && val=='0000-00-00'  ){
				result.msg	= result.msg+" </br> <strong>"+name+"</strong> Tidak Boleh 0000-00-00";
				result.good	= false;
				$(the_id).addClass("error_field");
			}

			if(empty=='n' && $(the_id).hasClass('idate') && val=='00-00-0000'  ){
				result.msg	= result.msg+" </br> <strong>"+name+"</strong> Tidak Boleh 00-00-0000";
				result.good	= false;
				$(the_id).addClass("error_field");
			}

			if(empty=='n' && typical=="money" && getMoney(the_id)==0  ){
				result.msg=result.msg+" </br> <strong>"+name+"</strong> Tidak Boleh Nol";
				result.good=false;
				$(the_id).addClass("error_field");
			}

			if(empty=='n' && $(the_id).hasClass('mydatetime') && val=='0000-00-00 00:00:00'  ){
				result.msg	= result.msg+" </br> <strong>"+name+"</strong> Tidak Boleh 0000-00-00 00:00:00";
				result.good	= false;
				$(the_id).addClass("error_field");
			}	

			if(empty=='n' && $(the_id).hasClass('idatetime') && val=='00-00-0000 00:00:00'  ){
				result.msg	= result.msg+" </br> <strong>"+name+"</strong> Tidak Boleh 00-00-0000 00:00:00";
				result.good	= false;
				$(the_id).addClass("error_field");
			}

			if(typical=='alphanumeric'  && !is_alphanumeric(val)){
				result.msg	= result.msg+" </br> <strong>"+name+"</strong> Seharusnya Alphanumberic (A-Z,a-z,0-9)";
				result.good	= false;
				$(the_id).addClass("error_field");
			}else if(typical=='alpha' && !is_alpha(val)){
				result.msg	= result.msg+" </br> <strong>"+name+"</strong> Seharusnya Alpha (A-Z,a-z)";
				result.good	= false;
				$(the_id).addClass("error_field");
			}else if(typical=='numeric' && !is_numeric(val)){
				result.msg	= result.msg+" </br> <strong>"+name+"</strong> Seharusnya Numeric (0-9)";
				result.good = false;
				$(the_id).addClass("error_field");
			}else if(typical=='date' && !is_date(val)){
				result.msg	= result.msg+" </br> <strong>"+name+"</strong> Seharusnya Beformat YYYY-MM-DD";
				result.good	= false;
				$(the_id).addClass("error_field");
			}
		}
	}
    
    this.addCekSave(result);
	if(!result.good){
		var id_alt	= "#modal_alert_"+this.prefix+"_add_form";		
		var warn	= '<div class="alert alert-block alert-info "><h4>Pemberitahuan</h4>'+result.msg+'</div>';
		$(id_alt).html(warn);
		$(id_alt).focus();
	}
	return result.good;
};

TableAction.prototype.aftersave		= function (){};
TableAction.prototype.beforesave	= function (){};
TableAction.prototype.addCekSave	= function (result){
    return result;
};

TableAction.prototype.save=function (){
	if(!this.cekSave()){
		return;
	}
	this.beforesave();
	var self	= this;
	var a		= this.getSaveData();
	showLoading();
	$.ajax({url:"",data:a,type:'post',success:function(res){
		var json = getContent(res);
		if(json==null) {
			$("#"+self.prefix+"_add_form").smodal('hide');
			return;		
		}else if(json.success=="1"){
			if(self.multiple_input) {
				if(self.focus_on_multi!=""){
					$('#'+self.action+'_'+self.focus_on_multi).focus();
				}else{
					$('#'+self.action+'_add_form').find('[autofocus]').focus();					
				}
			}else {
				$("#"+self.prefix+"_add_form").smodal('hide');
			}
			self.view();
			self.clear();
		}
		dismissLoading();
        self.is_post_action_del=false;
        if(a['id']!=""){
            self.postAction(a['id']);    
        }else{
            self.postAction(json['id']);
        }
	}});
	this.aftersave();
	return this;
};

TableAction.prototype.load=function (a){
	$.post('',a,function(res){
		$("#smis_container").html(res);
	});
	return this;
};

/*PRINTING*/
TableAction.prototype.printelement=function(id){
	var data		= this.getRegulerData();
	data['command']	= 'print-element';
	data['slug']	= 'print-element';
	data['id']		= id;
	$.post("",data,function(res){
		var json	= getContent(res);
		if(json==null) return;
		smis_print(json);
	});
	return this;
};

TableAction.prototype.printing=function(id,slug){
	var data		= this.getRegulerData();
	data['command']	= 'printing';
	data['slug']	= slug;
	data['id']		= id;
	$.post("",data,function(res){
		var json	= getContent(res);
		if(json==null) return;
		smis_print(json);
	});
	return this;
};

TableAction.prototype.focusSearch=function(){
	var self=this;
	$('#smis-chooser-modal').on('shown.bs.modal', function () {
		$("#"+self.prefix+"_kriteria").focus();
	});
	return this;
};


TableAction.prototype.reload=function(){
	var self=this;
	var data=this.getRegulerData();
	data=this.addReloadData(data);
	data['command']='reload';
	$.post("",data,function(res){
		var json=getContent(res);
		if(json!=null) {
			$("#print_table_"+self.prefix).replaceWith(json);
		}
		self.view();
		dismissLoading();
	});
	return this;
};


TableAction.prototype.duplicateloop=function(){
    if(this.duplicate_loop_runner) {
        return;
    }
    $("#smis-loader-bar").sload("true","Fetching total data",0);
    $("#smis-loader-modal").modal("show");
    this.duplicate_loop_runner=true;
    var d=this.getRegulerData();
    d['name_view']=this.duplicate_loop_name_view;
    var self=this;
    d['command']="duplicateloop";
    $.post("",d,function(res){
        var content=getContent(res);
        var all=content.dbtable.data;
        if(all!=null) {   
            var total=all.length;
            self.duplicateunitloop(0,all,total);
        } else {
            $("#smis-loader-modal").modal("hide");
            self.duplicate_loop_runner=false;
        }
	});
	return this;
};

TableAction.prototype.duplicateunitloop=function(current,all,total){
    if(current>=total || !this.duplicate_loop_runner) {
        this.duplicate_loop_runner=false;
        $("#smis-loader-modal").modal("hide");
        this.view();
        return;
    }
    var self=this;
    var one_unit=all[current];
    var id=one_unit['id'];
    var name_view="";
    if(this.duplicate_loop_name_view!=null){
        name_view=one_unit[this.duplicate_loop_name_view];
    }
    $("#smis-loader-bar").sload("true",id+". "+name_view+". . . [ "+(current+1)+" / "+total+" ] ",(current*100/total));
    var self=this;
	var duplicate_data=this.getDuplicateData(id);	
    $.post('',duplicate_data,function(res){
        var ct=getContent(res);        
        setTimeout(function(){self.duplicateunitloop(++current,all,total)},this.duplicate_loop_timeout);
	}); 
	return this;
};