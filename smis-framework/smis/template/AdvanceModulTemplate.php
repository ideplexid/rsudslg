<?php 
require_once 'smis-framework/smis/template/ModulTemplate.php';

class AdvanceModulTemplate extends ModulTemplate{
    /*@var Database*/
	protected $db;
    /*@var string*/
	protected $action;
	/*@var string*/
	protected $page;
    /*@var Array*/
    protected $supercommand;
	/*@var Array*/
    protected $supercommand_js_variable;
	/*@var Array*/
    protected $supercommand_viewdata;
	/*@var Array*/
    protected $supercommand_regdata;
	/*@var Array*/
    protected $supercommand_action;
    /*@var Array*/
    protected $resource_before;
	/*@var Array*/
    protected $resource_after;
    
    public function __construct(Database $db,$page,$action){
        parent::__construct();
        $this->db=$db;
        $this->page=$page;
        $this->action=$action;
        $this->supercommand_viewdata=array();
        $this->supercommand_regdata=array();
        $this->supercommand_js_variable=array();
        $this->resource_before=array();
		$this->resource_after=array();
    }
    
    /**
     * @brief get the supercommand action in system
     * @param string $supercommand 
     * @return string $action  
     */
    protected function getSuperCommandAction($supercommand){
        if($this->supercommand_action==null){
            return $this->action;
        }
        
        if(!isset($this->supercommand_action[$supercommand])){
            return $this->action;
        }
        
        return $this->supercommand_action[$supercommand];
    }
    
    
    /**
     * @brief adding the supercommand spesific action on the file
     *          so it could be redirect to another file, not only in same
     *          file just like the first data.
     * @param string $supercommand name 
     * @param string $action redirect file
     * @return  MasterTemplate
     */
    public function addSuperCommandAction($supercommand,$action){
        if($this->supercommand_action==null){
            $this->supercommand_action=array();
        }
        $this->supercommand_action[$supercommand]=$action;
        return $this;
    }
    
    /**
	 * adding the var in supercommand
	 * <script>
	 * 	$var.addViewData=function(view_data){
	 * 		view_data['$key']='$value' -->  if $free false
	 * 		view_data['$key']=$value(); -->  if $free is smis_action
	 * 		view_data['$key']=$('#'.$key).val(); -->  if $free false
	 * 		view_data['$key']=$('#action_'.$key).val(); -->  if $free action
	 * 		return view_data;
	 * 	}
	 * </script>
	 *
	 * @param unknown $var name in javascript
	 * @param unknown $key key in selected
	 * @param unknown $value value from json while selected - json[value]
	 * @param boolean $free if true, then id would free as user input, otherwise will combine with action
	 */
	
	public function addSuperCommandViewData($supercommand,$key,$value,$free=false){
		if($free=="smis_action_js"){
			$this->supercommand_viewdata[$supercommand][]="view_data['".$key."']=$value;";
		}else if($free=="action"){
			$this->supercommand_viewdata[$supercommand][]="view_data['".$key."']=\$('#".$value."').val();";
		}else if($free=="acript"){
			$this->supercommand_viewdata[$supercommand][]=$value;
		}else if($free){
			$this->supercommand_viewdata[$supercommand][]="view_data['".$key."']='".$value."';";
		}else {
			$nvalue=$this->action."_".$value;
			$this->supercommand_viewdata[$supercommand][]="view_data['".$key."']=\$('#".$nvalue."').val();";
		}
		return $this;
	}
	
	/**
	 * adding the var in supercommand regulerdata
	 * <script>
	 * 	$var.addRegulerData=function(reg_data){
	 * 		reg_data['$key']='$value' -->  if $free false
	 * 		reg_data['$key']=$value(); -->  if $free is smis_action
	 * 		reg_data['$key']=$('#'.$key).val(); -->  if $free false
	 * 		reg_data['$key']=$('#action_'.$key).val(); -->  if $free action
	 * 		return reg_data;
	 * 	}
	 * </script>
	 *
	 * @param unknown $var name in javascript
	 * @param unknown $key key in selected
	 * @param unknown $value value from json while selected - json[value]
	 * @param boolean $free if true, then id would free as user input, otherwise will combine with action
	 */
	
	public function addSuperCommandRegulerData($supercommand,$key,$value,$free=false){
		if($free=="smis_action_js"){
			$this->supercommand_regdata[$supercommand][]="reg_data['".$key."']=$value;";
		}else if($free=="action"){
			$this->supercommand_regdata[$supercommand][]="reg_data['".$key."']=\$('#".$value."').val();";
		}else if($free=="script"){
			$this->supercommand_regdata[$supercommand][]=$value;
		}else if($free=="true"){
			$this->supercommand_regdata[$supercommand][]="reg_data['".$key."']='".$value."';";
		}else {
			$nvalue=$this->action."_".$value;
			$this->supercommand_regdata[$supercommand][]="reg_data['".$key."']=\$('#".$nvalue."').val();";
		}
		return $this;
	}
    
    /**
	 * adding the var in supercommand
	 * <script>
	 * 		var $var;
	 * 		$var=new TableAction(...);
	 * 		$var.setSuperCommand('$var');
	 * 		$var.selected=function(json){
	 * 			$('#action_'.$key).val(json[$value]); -->  if $free false
	 * 			$('#'.$key).val(json[$value]); -->  if $free true
	 * 		}
	 * </script>
	 * 
	 * @param unknown $var name in javascript
	 * @param unknown $key key in selected
	 * @param unknown $value value from json while selected - json[value]
	 * @param boolean $free if true, then id would free as user input, otherwise will combine with action
	 */
	public function addSuperCommandArray($var,$key,$value,$free=false){
		if(is_string($free) && $free=="smis_action_js"){
			$this->supercommand_js_variable[$var]["smis_action_js"]=$value;
		}else if($free){
			$this->supercommand_js_variable[$var][$key]=$value;
		}else {
			$nkey=$this->action."_".$key;
			$this->supercommand_js_variable[$var][$nkey]=$value;
		}
		return $this;
	}
    
    /**
	 * for every var name in 
	 * $this->suppercommand_js_variable would generate
	 * text that look like this
	 * <script>
	 * 		var $var;
	 * 		$var=new TableAction(...);
	 * 		$var.setSuperCommand('$var');
	 * 		$var.selected=function(json){
	 * 			$('#action_'.$key).val(json[$value]); -->  if $free false
	 * 			$('#'.$key).val(json[$value]); -->  if $free true
	 * 		}
	 * </script>
	 * <script>
	 * 	$var.addVIewData=function(view_data){
	 * 		view_data['$key']='$value' -->  if $free false
	 * 		view_data['$key']=$value(); -->  if $free is smis_action
	 * 		view_data['$key']=$('#'.$key).val(); -->  if $free false
	 * 		view_data['$key']=$('#action_'.$key).val(); -->  if $free action
	 * 		return view_data;
	 * 	}
	 * </script>
	 */
	public function getSuperCommandJavascript(){
		foreach($this->supercommand_js_variable as $var=>$content){
            $super_action=$this->getSuperCommandAction($var);
			echo "<script type='text/javascript'>";
			echo "var $var ;";
			echo "$var = new TableAction('$var','$this->page','".$super_action."',new Array());";
			if($this->is_prototype)
                echo $var.".setPrototipe(\"".$this->protoname."\",\"".$this->protoslug."\",\"".$this->protoimple."\");";		

            echo "$var.setSuperCommand('$var');";
			echo "$var.selected=function(json){";
					if(is_array($content)){
						foreach($content as $key=>$value){
							if($key=="smis_action_js") {
                                echo $value;
                            }else{
                                echo "smis_edit('#$key',json['$value']);";
                            }
						}
					}else{
						echo $content;
					}
			echo "};";
            
            if($super_action!=$this->action){
                echo "$var.setOwnChooserData(true);";
                echo "$var.addChooserData=function(data){";
                            echo "data['action']='".$super_action."';";
                            echo "return data;";
                echo "};";
            }
            
			echo 	"</script>";
		}
		
		foreach($this->supercommand_viewdata as $var=>$content){
			echo  "<script type='text/javascript'>";
			echo "$var.addViewData=function(view_data){";
			foreach($content as $key=>$value){
				echo $value;
			}
			echo "return view_data;";
			echo "};";
			echo "</script>";
		}
      
		foreach($this->supercommand_regdata as $var=>$content){
			echo  "<script type='text/javascript'>";
			echo "$var.addRegulerData=function(reg_data){";
			foreach($content as $key=>$value){
			echo $value;
			}
			echo "return reg_data;";
			echo "};";
			echo "</script>";
		}
		
	}
    
    /**
	 * adding external resource such as Javascript File (.js)
	 * or CSS File (.css), used if user want to make a customize whole system
	 * @param string $cssjs
	 * @param string $url
	 * @param string $position
	 * @param boolean $smis
	 */
	public function addResouce($cssjs,$url,$before="after",$smis=false){
		$res="";
		if($cssjs=="css")$res=addCSS($url,$smis); 
		else $res=addJS($url,$smis);
		
		if($before=="after")$this->resource_after[]=$res;
		else $this->resource_before[]=$res;
		
		return $this;
	}
    
    public function getResourceBeforePreload(){
		foreach($this->resource_before as $resource){
			echo $resource;
		}
	}
	
	public function getResourceAfterPreload(){
		foreach($this->resource_after as $resource){
			echo $resource;
		}
	}
}

?>