<?php 

abstract class NavigatorTemplate{
	protected $navigation;
	protected $top_menu;
	protected $page;//folder of this application\
	protected $name;//folder of this application\
	protected $menus;
	
	public function __construct($navigation,$tooltip,$name,$page,$menu="fa fa-sitemap"){
			$this->navigation=$navigation;
			$this->top_menu=$navigation->getMenu($page);
			if($this->top_menu==null){
				$this->top_menu=new Menu($menu);
				$this->top_menu->addProperty('title', $tooltip);
				$this->top_menu->addProperty('name',$name);
			}
			$navigation->addMenu($this->top_menu,$page);
			$this->page=$page;
			$this->name=$name;
			$this->menus="all";
	}
	
	public function setMenus($menus){
		$this->menus=$menus;
	}
	
	public function init(){
		$this->adminMenu();
		$this->topMenu();
		return $this->top_menu;
	}
	
	public function adminMenu(){}
	public function topMenu(){}
	public function extendMenu(Menu $nav, $pname="",$pslug="",$pimplement=""){
		return $nav;
	}
	
	public function addAdminMenu($menu){
			$menu_admin=$this->navigation->getMenu('smis-administrator');
			$menu_admin->addSub($menu);
	}
	
	public function addAdminSeparator(){
			$menu_admin=$this->navigation->getMenu('smis-administrator');
			$menu_admin->addSeparator();
	}
	
	public function addAdminLink($tooltip,$name,$href){
			$m=new Menu();
			$prop=array();
			$prop['title']=$tooltip;
			$prop['name']=$name;
			$prop['href']=$href;
			$m->setProperty($prop);
			$this->addAdminMenu($m);
	}
	
	public function pushMenu($menu){
		$this->top_menu->addSub($menu);
	}
	
	public function addMenu($tooltip,$name,$action,$page=null,$class='smis_action',$icon='fa fa-home'){
		if($page==null) $page=$this->page;
		$manage=new Menu();
		$manage->addProperty('title',$tooltip);
		$manage->addProperty('name',$name);
		$manage->addProperty('href','#');
		$manage->addProperty('page',$page);
		$manage->addProperty('action',$action);
		$manage->addProperty('class',$class);
		$manage->addProperty('icon',$icon);
		$this->pushMenu($manage);
	}
	
	public function addPrototype($protoype_name="",$protoype_slug="",$protoype_implement="",$tooltip,$name,$action,$icon="fa fa-sitemap",$page=null,$class='smis_action'){
		if($page==null) $page=$this->page;
		$manage=new Menu();
		$manage->addProperty('title',$tooltip);
		$manage->addProperty('name',$name);
		$manage->addProperty('href','#');
		$manage->addProperty('page',$page);
		$manage->addProperty('action',$action);
		$manage->addProperty('class',$class);
		$manage->addProperty('prototype_name',$protoype_name);
		$manage->addProperty('prototype_slug',$protoype_slug);
		$manage->addProperty('prototype_implement',$protoype_implement);
		$manage->addProperty("icon", $icon);
		$this->pushMenu($manage);
	}
	
	public function addSeparator(){
		$this->top_menu->addSeparator();
	}
}


?>