<?php 

abstract class InstallatorTemplate{
	
	protected $slug;
	protected $code;
	protected $db;
	protected $modul;
	protected $entity;
	
	public function __construct($db,$slug,$code){
		$this->slug=$slug;
		$this->code=$code;
		$this->db=$db;
		$this->modul=array();
		$this->entity=$slug!=""?"_".$slug:"";
	}
	
	public function init($params){}
	
	public function addModul($query){
		$number=count($this->modul);
		$this->modul[$number]=$query;
	}
	
	public function extendInstall($dbslug){
		
	} 
	
	public function install(){
		foreach($this->modul as $mdl){
			$this->db->query($mdl);
		}
	}
	
	
	
}

?>