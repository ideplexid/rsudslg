<?php 
	
/**
 * this class is Template Pattern of each modul
 * that should be implemented for plugins developer
 * 
 * @author goblooge
 * @copyright Nurul Huda <goblooge@gmail.com>
 * @license LGPL v3
 * @since 10 Jan 2014
 * @version 1.0
 */
 class ModulTemplate{
	
	public function __construct(){
		
	}
	
	/**
	 * imported file that needed
	 * @param string path of the file $file
	 */
	protected function import($file){
		require_once($file);
	}
	
	/**
	 * this function use for initialize how a Modul Should work
	 * this function do are :
	 * 1. if there super_command then call supercommand function
	 * 2. if there no supper_command but a command exist then call command
	 * 3. if there no super_command and no command, 
	 *    then execute phpPreload, jsLoader, css Loader, jsPreload, cssPreload and HtmlPreload
	 */
	public function initialize(){
		if(isset($_POST['super_command']) && $_POST['super_command']!=""){
			$this->superCommand($_POST['super_command']);
		}else if(isset($_POST['command'])){
			$this->command($_POST['command']);
		}else{			
			$this->phpPreLoad();
			$this->jsLoader();
			$this->cssLoader();
			$this->jsPreLoad();
			$this->cssPreLoad();
			$this->htmlPreLoad();
		}
	}
	
	public function importLoader($path){}
	public function superCommand($super_command){}
	public function command($command){}
	
	
	//load css and javascript before loading
	public function jsLoader(){}
	public function cssLoader(){}
	
	
	
	/*when it's star build*/
	public function phpPreLoad(){}
 	public function htmlPreLoad(){}
 	public function jsPreLoad(){}
	public function cssPreLoad(){}	
	
	
}

?>