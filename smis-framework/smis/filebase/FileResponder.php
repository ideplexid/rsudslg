<?php 
/**
 * 
 * this class represent of Controller in MVC of Safethree
 * most of this class using Facade and Bridge Design Pattern that combining 
 * several methode from Directori, Adapter and Table.
 * this Class used with javascript FileAction in javascript
 * for handling File Management.
 * 
 * @see ResponsePackage
 * @see Table
 * @see Adapter
 * @author goblooge
 * @since 09 December 2016
 * @copyright Nurul Huda <goblooge@gmail.com>
 * @version 1.1
 */
 
class FileResponder{
	protected $base;
	protected $adapter;
	protected $uitable;
	protected $action; 
	protected $path;
	protected $response_pack;
	protected $is_file_only;
	
	public function __construct($path,$action,$uitable,$adapter){
		$this->response_pack	= new ResponsePackage();
		$this->path				= $path;
		$this->base				= $this->correctBase();
		$this->action			= $action;
		$this->uitable			= $uitable;
		$this->adapter			= $adapter;
		$this->is_file_only		= false;
	}
	
	public function setIsFileOnly($file_only){
		$this->is_file_only		= $file_only;
	}
	
	/**
	 * @brief this function used for correcting the
	 * 		  basic of file system.
	 * 		  this used for correcting the whole file system so
	 * 		  user cannot going to unauthorize folder like root folder.
	 * @return  the folder base
	 */
	protected function correctBase(){
		$BASE			= isset($_POST['folder'])?$_POST['folder']:"./";
		if($BASE=="") {
			$BASE=$this->path;
		}else if(!startsWith($BASE,$this->path)) {
			$BASE=$this->path;
		}else if(strpos($BASE, "../")!==false) {
			$BASE=$this->path;
		}
		if(!endsWith($BASE,"/")) {
			$BASE=$BASE."/";
		}
		$this->base		= $BASE;
		return $this->base;
	}
	
	public function command($command){
		$content	= NULL;
		$status		= 'not-authorized';	//not-authorized, not-command, fail, success
		$alert		= array();
		if($command=="download"){
			$this->download();
		}else if($command=="list"){
			$this->response_pack->setStatus(ResponsePackage::$STATUS_OK);
			$hasil	= $this->view();
			$this->response_pack->setContent($hasil);
		}else if($command=="save"){
			$this->response_pack->setStatus(ResponsePackage::$STATUS_OK);
			$this->save($this->response_pack);
		}else if($command=="remove"){
			$this->response_pack->setStatus(ResponsePackage::$STATUS_OK);
			$this->remove($this->response_pack);
		}else if($command=="load"){
			$this->response_pack->setStatus(ResponsePackage::$STATUS_OK);
			$this->loadFile($this->response_pack);
		}
		return $this->response_pack->getPackage();
	}
	
	/**
	 * @brief open the content of the file
	 * 		  based on it's path.
	 * @return  Binary of the opened file.
	 */
	protected function loadFile(ResponsePackage $res){
		$filename	= $this->base."/".$_POST['filename'];
		if(file_exists($filename)){
			$file	= file_get_contents($filename);
			$res->setContent($file);
		}else{
			$res->setWarning(true,"Not Found",$filename." not found, perhaps it's deeleted at a moment !! try  reload ");
			$res->setContent("");
		}
	}
	
	/**
	 * @brief this one automatically detect wether 
	 * 		  save new file or update old file 
	 * @param ResponsePackage $pack 
	 * @return  
	 */
	protected function save(ResponsePackage  $pack){
		if($_POST['isold']=="1")
			$this->saveOld($pack);
		else 
			$this->saveNew($pack);
	}
	
	/**
	 * @brief this system used for removing file
	 * @param ResponsePackage $res 
	 * @return null 
	 */
	protected function remove(ResponsePackage  $res){
		$filename	= $_POST['filename'];		
		if($filename=="./"){
			$res->setWarning("y", "Error", "You Cannot Remove The Base Folder <strong>".getcwd()."</strong>");
		}else if(!file_exists($this->base.$filename)){
			$res->setWarning("y", "Error", "File/Folder <strong>".$this->base.$filename."</strong> is Not Exist Nothing to Remove");
		}else if(is_dir($this->base.$filename)){
			rrmdir($this->base.$filename);
			$res->setAlertVisible(true);
			$res->setAlertContent("Success", "Folder <strong>".$this->base.$filename."</strong> Removed Successfully");
		}else{
			$res->setAlertVisible(true);
			unlink($this->base.$filename);
			$res->setAlertContent("Success", "File <strong>".$this->base.$filename."</strong> Removed Successfully");
		}
	}
	
	/**
	 * @brief this system used for saving new file
	 * @param ResponsePackage $res 
	 * @return null 
	 */
	protected function saveNew(ResponsePackage $res){
		$filename	= $_POST['filename'];
		$owner		= $_POST['fileowner'];
		$chmod		= $_POST['filemode'];
		$chmod		= base_convert($_POST['filemode'], 8, 10);
		$type		= $_POST['filetype'];
		if($type=="File"){
			if(file_exists($this->base.$filename)){
				$res->setWarning("y", "Error", "The File/Folder <strong>".$this->base.$filename."</strong> Already Exist");
			}else{
				file_put_contents($this->base.$filename,"\xEF\xBB\xBF");
				chmod($this->base.$filename, $chmod);
				chown($this->base.$filename, $owner);
				$res->setAlertVisible(true);
				$res->setAlertContent("Success", "File <strong>".$this->base.$filename."</strong> Created");
			}
		}else{
			if(file_exists($this->base.$filename)){
				$res->setWarning("y", "Error", "The File/Folder <strong>".$this->base.$filename."</strong> Already Exist");
			}else{
				mkdir($this->base.$filename);
				chmod($this->base.$filename, $chmod);
				chown($this->base.$filename, $owner);
				$res->setAlertVisible(true);
				$res->setAlertContent("Success", "Folder <strong>".$this->base.$filename."</strong> Created");
			}
		}
	}
	
	/**
	 * @brief this used for save the eddited file 
	 * @param ResponsePackage $res 
	 * @return  null
	 */
	protected function saveOld(ResponsePackage  $res){
		$oldname	= $_POST['oldname'];
		$filename	= $_POST['filename'];
		$owner		= $_POST['fileowner'];
		$chmod		= $_POST['filemode'];
		$chmod		= base_convert($_POST['filemode'], 8, 10);
		$type		= $_POST['filetype'];
		if($filename==$oldname){
			if($type=="File" && is_dir($this->base.$filename) ){
				$res->setWarning("y", "Error", "Cannot Change File <strong>".$this->base.$filename."</strong> To Folder !!!");
			}else if($type=="Directory" && is_file($this->base.$filename) ) {
				$res->setWarning("y", "Error", "Cannot Change Folder <strong>".$this->base.$filename."</strong> To File !!!");
			}else{
				chmod($this->base.$filename, $chmod);
				chown($this->base.$filename, $owner);
				$res->setAlertVisible(true);
				$res->setAlertContent("Success", "File/Folder <strong>".$this->base.$oldname."</strong> Updated");
			}
		}else if(file_exists($this->base.$filename)){
			$res->setWarning("y", "Error", "The File/Folder <strong>".$this->base.$filename."</strong> Already Exist, The File/Folder ".$this->base.$oldname." Cannot Updated!!!");
		}else if($type=="File"){
			if(!file_exists($this->base.$oldname)){
				$res->setWarning("y", "Error", "The File <strong>".$this->base.$oldname."</strong> Not Exist !!!");
			}else if(is_dir($this->base.$oldname) ){
				$res->setWarning("y", "Error", "<strong>".$this->base.$oldname."</strong> is Folder Not A File !!!");
			}else{
				rename($this->base.$oldname,$this->base.$filename);
				chmod($this->base.$filename, $chmod);
				chown($this->base.$filename, $owner);
				$res->setAlertVisible(true);
				$res->setAlertContent("Success", "File <strong>".$this->base.$oldname."</strong> Updated and Renamed To <strong>".$this->base.$filename."</strong>");
			}
		}else{
			if(!file_exists($this->base.$oldname)){
				$res->setWarning("y", "Error", "The Folder <strong>".$this->base.$oldname."</strong> Not Exist !!!");
			}else if(is_file($this->base.$oldname) ){
				$res->setWarning("y", "Error", "<strong>".$this->base.$oldname."</strong> is File Not A Folder !!!");
			}else{
				rename($this->base.$oldname,$this->base.$filename);
				chmod($this->base.$filename, $chmod);
				chown($this->base.$filename, $owner);
				$res->setAlertVisible(true);
				$res->setAlertContent("Success", "Folder <strong>".$this->base.$oldname."</strong> Updated and Renamed To <strong>".$this->base.$filename."</strong>");
			}
		}
	}
	
	/**
	 * @brief this function used to view
	 * 		  the file inside the folder
	 * @return  String html
	 */
	protected function view(){
		$hasil				 = array();
		$hasil['basefolder'] = $this->base;
		$dt					 = null;
		try{
			$dt				 = getAllFileInDir($this->base,false,$this->is_file_only,true);
			$salman			 = $this->adapter;
			$content		 = $salman->getContent($dt);
			$this->uitable->setContent($content);
			$lcontent		 = $this->uitable->getBodyContent();
			$hasil['list']	 = $lcontent;
		}catch(Exception $e){
			$res->setWarning("y", "Error Opening File",$e->getMessage());
			$this->base		 	 = $this->path;
			$hasil['basefolder'] = $this->base;
			$dt=getAllFileInDir($this->base,false,$this->is_file_only,true);
			$salman				  = $this->adapter; 
			$content			  = $salman->getContent($dt);
			$this->uitable->setContent($content);
			$lcontent			  = $this->uitable->getBodyContent();
			$hasil['list']		  = $lcontent;
		}
		return $hasil;
	}
	
	/**
	 * @brief this function used to download the file
	 * @return  the downloaded file
	 */
	private function download(){
		$actual_file_name = $this->base.$_POST['filename'];
		$mime = 'application/zip';		
		header('Pragma: public');
		header('Expires: 0');
		header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
		header('Cache-Control: private', false);
		header('Content-Type: ' . $mime);
		header('Content-Disposition: attachment; filename="'. $_POST['filename'] .'"'); // Set it here!
		header('Content-Transfer-Encoding: binary');
		header('Content-Length: ' . filesize($actual_file_name));
		header('Connection: close');
		readfile($actual_file_name);
		exit();
	}
}
?>