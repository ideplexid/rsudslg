<?php 
class FileAdapter extends ArrayAdapter{
	protected $action;
	public function __construct($actioname){
		parent::__construct();
		$this->action = $actioname;
	}
	
	public function adapt($file){
		$hasil					= array();
		$lf 					= $this->getLink($file['name'],$file['fd'],$file['hidden'],$file['istext']);
		$hasil['&nbsp;']		= $lf['icon'];
		$hasil['Name']			= $lf['name'];
		$ukuran					= $this->getFileSize($file['size']);
		$hasil['Size']			= self::format("text-right", $ukuran);
		$hasil['Permission']	= $file['mode'];
		$hasil['Owner']			= $file['owner'];
		$hasil['User']			= $file['user'];
		$hasil['Type']			= strtoupper($file['fd']);
		$hasil['Visibility']	= $file['hidden']?"Hidden":"Visible";
		$hasil['Filetype']		= $file['istext']?"Text":"Binary";
		$hasil['Action']		= $this->getAction($file['name'], $file['fd']=="directory", $file['istext'], $file['hidden'],$file['mode'],$file['user']);		
		return $hasil;
	}
	
	public function getFileSize($size){
		if($size<1024){
			return number_format($size,0,",",".")." B ";
		}
		if($size<1048576){
			return number_format(ceil($size/1024),0,",",".")." KB ";
		}
		if($size<1073741824){
			return number_format(ceil($size/1048576),0,",",".")." MB ";
		}
		return number_format(ceil($size/1073741824),0,",",".")." GB ";
	}
	
	public function getLink($filename,$folder,$hidden,$istext){
		$icon	= $this->getIcon($filename, $folder, $hidden, $istext);
		$hasil	= array();
		if($folder=="directory"){
			$hasil['icon']	= "<a href='javascript:;' onclick=\"".$this->action.".directory('".$filename."')\" >".$icon."</a>";
			$hasil['name']	= "<a href='javascript:;' onclick=\"".$this->action.".directory('".$filename."')\" >".$filename."</a>";
		}else if($icon=="<i class='fa fa-file-image-o '></i>"){
			$hasil['icon']  = "<a href='javascript:;' onclick=\"".$this->action.".showimage('".$filename."')\" >".$icon."</a>";
			$hasil['name']  = "<a href='javascript:;' onclick=\"".$this->action.".showimage('".$filename."')\" >".$filename."</a>";
		}else if($icon=="<i class='fa fa-file-sound-o '></i>"){
			$hasil['icon']	= "<a href='javascript:;' onclick=\"".$this->action.".showaudio('".$filename."')\" >".$icon."</a>";
			$hasil['name']	= "<a href='javascript:;' onclick=\"".$this->action.".showaudio('".$filename."')\" >".$filename."</a>";
		}else{
			$hasil['icon']	= "<a href='javascript:;' onclick=\"".$this->action.".openfile('".$filename."')\" >".$icon."</a>";
			$hasil['name']	= "<a href='javascript:;' onclick=\"".$this->action.".openfile('".$filename."')\" >".$filename."</a>";
		}
		return $hasil;
	}
	
	public function getAction($name,$is_folder,$is_binary,$is_hidden,$permi,$owner){
		$btg	  = new ButtonGroup("");
		$ren	  = new Button("", "", "");
		$ren	  ->setIsButton(Button::$ICONIC)
				  ->setIcon("fa fa-pencil")
				  ->setClass("btn-primary btn-small")
				  ->setAction($this->action.".editfile('".$name."','".($is_folder?"Directory":"File")."','".$permi."','".$owner."')");
		$btg	  ->addButton($ren);
		
		$download = new Button("", "", "");
		$download ->setIsButton(Button::$ICONIC)
				  ->setIcon("fa fa-download")
				  ->setClass("btn-primary btn-small")
				  ->setAction($this->action.".download('".$name."')");
		$btg	  ->addButton($download);
		
		$del	  = new Button("", "", "");
		$del	  ->setIsButton(Button::$ICONIC)
				  ->setIcon("fa fa-trash")
				  ->setClass("btn-primary btn-small")
				  ->setAction($this->action.".removefile('".$name."')");
		$btg	  ->addButton($del);
		return $btg->getHtml();
	}
	
	public function getIcon($filename,$folder,$hidden,$binary){
		if($folder=="directory" && !$hidden) {
			return "<i class='fa fa-folder '></i>";
		}
		if($folder=="directory" && $hidden) {
			return "<i class='fa fa-folder-o '></i>";
		}
		$ext = pathinfo($filename, PATHINFO_EXTENSION);
		
		if(in_array($ext, array('png', 'jpg', 'gif','bmp','tiff','jpeg'))){
			return "<i class='fa fa-file-image-o '></i>";
		}else if(in_array($ext,array('avi','3gp','mkv','mp4','fla','wmv') )){
			return "<i class='fa fa-file-video-o '></i>";
		}else if(in_array($ext,array('mp3','ogg','wav','mid','amr','wma','m4a','m4p') )){
			return "<i class='fa fa-file-sound-o '></i>";
		}else if(in_array($ext, array('zip','rar','7z','tar','gz','bzip'))){
			return "<i class='fa fa-file-archive-o '></i>";
		}else if(in_array($ext,array('rtf','doc','docx') )){
			return "<i class='fa fa-file-word-o '></i>";
		}else if(in_array($ext,array('ppt','pptx') )){
			return "<i class='fa fa-file-powerpoint-o '></i>";
		}else if(in_array($ext,array('xls','xlsx') )){
			return "<i class='fa fa-file-excel-o '></i>";
		}else if(in_array($ext,array('txt') )){
			return "<i class='fa fa-file-text-o '></i>";
		}else if(in_array($ext,array('pdf') )){
			return "<i class='fa fa-file-pdf-o '></i>";
		}else if(in_array($ext, array('css'))){
			return "<i class='fa fa-css3 '></i>";
		}else if(in_array($ext, array('js'))){
			return "<i class='fa fa-jsfiddle '></i>";
		}else if(in_array($ext, array('php'))){
			return "<i class='fa fa-codepen'></i>";
		}else if(in_array($ext, array('html'))){
			return "<i class='fa fa-html5'></i>";
		}else if(in_array($ext, array('sh','py','rb'))){
			return "<i class='fa fa-terminal'></i>";
		}else if(in_array($ext, array('java','c','cpp','py'))){
			return "<i class='fa fa-file-code-o '></i>";
		}else if(in_array($ext, array('json'))){
			return "<i class='fa fa-code'></i>";
		}else if($hidden){
			return "<i class='fa fa-file-o'></i>";
		}
		return "<i class='fa fa-file'></i>";
	}
	
	public function getContent($data){
		$content		= array();
		$content_file	= array();
		$content_folder	= array();
		foreach($data as $d){
			$adapt		= $this->adapt($d);
			if($d['fd']=="directory")
				$content_folder[] = $adapt;
			else $content_file[]  = $adapt;
		}
		foreach($content_folder as $folder) {
			$content[] = $folder;
		}
		foreach($content_file as $file){
			$content[] = $file;
		}
		return $content;
	}
}
?>