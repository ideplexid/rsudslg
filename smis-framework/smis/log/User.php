<?php

/**
 * this class used for detecting user
 * that used this system
 * so everything that user used in this system
 * like edit, delete, login, remove, adding and etc 
 * would log in the system
 * 
 * @author      : Nurul Huda
 * @version     : 1.1.0
 * @license     : Apache 2
 * @copyright   : goblooge@gmail.com
 * @since       : 14 Januari 2014
 * @database    : smis_adm_user
 * */

class User{
	/*@var string*/
	private $name;
	/*@var string*/
	private $username;	
	/*@var string*/
	private $session;
	/*@var string*/
	private $menu;
	/*@var string*/
	private $id;
	/*@var string*/
	private $ip;
	/*@var string*/
	private $mac;
	/*@var array*/
	private $post;
	/*@var string*/
	private $capability;
	/*@var string*/
	private $last_ps;
	/*@var string*/
	private $last_fetch;	
	/*@var string*/
	private $password;
	/*@var string*/
	private $timediff;
	/*@var string*/
	private $foto;
	/*@var string*/
	private $time_out;
	/*@var string*/
	private $st_mmode;
	/*@var string*/
	private $notification;
	/*@var string*/
	private $color_slide;
	/*@var string*/
	private $color_invert;
	/*@var string*/
	private $color_grayscale;
	/*@var string*/
	private $slider;
	/*@var string*/
	private $service;
	/*@var string*/
	private $code;
    /*@var string*/
	private $css;
	/*@var string*/
    private $autonomous;
    
	public function __construct(){
		$this->ip			= $this->get_client_ip();
		$this->mac			= $this->get_mac_address($this->ip);
		$this->post			= json_encode($this->getPostRaw());
		$this->notification	= null;
		$this->service		= "";
		$this->code			= "";
        $this->css			= null;
	}
    
    public function setAutonomous($autonomous){
        $this->autonomous	= $autonomous;
        return $this;
    }
	
	public function setUser($user_data){
		if($user_data==null){
			return;
		}
		$row=(array)$user_data;
		$this->name				= $row['realname'];
		$this->id				= $row['id'];
		$this->username			= $row['username'];
		$this->menu				= json_decode($row['menu'],true);
		$this->email			= $row['email'];	
		$this->capability		= $row['authority'];
		$this->last_ps			= $row['last_change'];
		$this->password			= $row['password'];
		$this->timediff			= $row['timediff'];
		$this->foto				= $row['foto'];
		$this->session			= $row['session'];
		$this->last_fetch		= $row['last_fetch'];
		$this->time_out			= $row['timeout'];
		$this->st_mmode			= $row['st_mmode'];
		$this->color_slide		= $row['color_slide'];
		$this->color_invert		= $row['color_invert'];
		$this->color_grayscale	= $row['color_grayscale'];
		$this->css				= $row['css'];
		$this->slider			= isset($row['slider'])?$row['slider']:0;
	}

	public function getEmail(){
		return $this->email;
	}

	public function getSliderModel(){
		return $this->slider;
	} 
    
    public function getCSS(){
        return $this->css;
    }
	
	public function setService($service){
		$this->service=$service;
	}
	
	public function setCode($code){
		$this->code=$code;
	}
	
	public function getCode(){
		return $this->code;
	}
	
	public function getService(){
		return $this->service;
	}
	
	public function getSettingsNavigatorMode(){
		return $this->st_mmode;
	}
	
	public function loadUser($id){
		global $wpdb;
		$query		= "	SELECT *,DATE_FORMAT(last_change,'%d %b %Y %H:%i') as last_ps ,
						FLOOR(HOUR(TIMEDIFF(last_change, CURRENT_TIMESTAMP())) / 24) as timediff
						FROM smis_adm_user WHERE id='".(string)$id."' AND prop!='del'";
		$user_row	= $wpdb->get_row($query);
		$this->setUser($user_row);
	}
	
	public function getNotification(){
		if($this->notification==null){
			global $wpdb;
			$query	="	SELECT count(*) as total 
                    	FROM smis_base_notification 
                    	WHERE (
                    	        user LIKE '%".$this->username." %'
                    	        OR user LIKE '%".$this->username."' 
                    	    ) 
                    	AND ( 
                    	        readby NOT LIKE '%".$this->username." %'
                    	        AND readby NOT LIKE '%".$this->username."' 
                    	    )";
			$total	= $wpdb->get_var($query);
			$this->notification=$total*1;	
		}
		return $this->notification;
	}
	
	public function checkSession($is_smis_realtime=false){
		return getSession('session')==$this->session && $this->isNotReload($is_smis_realtime);
	}
	
	public function isNotReload($is_smis_realtime=false){
		global $wpdb;
		$query		= "select if(UNIX_TIMESTAMP('".$this->last_fetch."') > UNIX_TIMESTAMP(NOW()-INTERVAL ".$this->time_out." MINUTE), 1, 0) as last_fetch";
		$var		= $wpdb->get_var($query);
		if(!$is_smis_realtime){
			$query 	= "UPDATE smis_adm_user SET last_fetch=CURRENT_TIMESTAMP() WHERE id='".$this->id."'";
			$wpdb->query($query);
		}
		return $var=="1";
	}
	
	/**
	 * this used for setting user when user using a service
	 * the data will be set hardway because somtime the user not registered 
	 * in this autonomous
	 * @param $name string username
	 * @param $ip is the user computer ip address
	 */
	public function setRawUser($name,$ip){
		$this->name	= $name;
		$this->ip	= $ip;
		return $this;
	}
	
	public function getFoto(){
		if($this->foto!=""){
			if(file_exists("smis-upload/".$this->foto)){
				return $this->foto;
			}
		}
		return "";
	}
	
	public function getTimeOut(){
		return $this->time_out;
	}

	public function saveSliderMode($slider){
		$this->slider			= $slider;
		$this->save("slider",$slider);
		return $this;
	}
	
	public function saveTimeOut($number){
		$this->time_out			= $number;
		$this->save("timeout",$number);
		return $this;
	}
	
	public function saveColorInvert($color_invert){
		$this->color_invert		= $color_invert;
		$this->save("color_invert",$color_invert);
		return $this;
	}
	
	public function saveColorGrayscale($color_grayscale){
		$this->color_grayscale	= $color_grayscale;
		$this->save("color_grayscale",$color_grayscale);
		return $this;
	}
	
	public function saveColorSlide($color_slide){
		$this->color_slide		= $color_slide;
		$this->save("color_slide",$color_slide);
		return $this;
	}
	
	public function saveNavigatorMode($mode){
		$this->st_mmode			= $mode;
		$this->save("st_mmode",$mode);
		return $this;
	}
    
    public function saveCSS($css){
		$this->css				= $css;
		$this->save("css",$css);
		return $this;
	}
	
	public function save($name,$value){
		global 	$wpdb;
        $datetime	= date("Y-m-d H:i:s");
		$query		= "UPDATE smis_adm_user set ".$name."='".$value."',  origin_updated='".$this->autonomous."', time_updated='".$datetime."' WHERE id='".$this->id."' ";
		$wpdb->query($query);
		return $this;
	}
	
	public function getColorSlide(){
		return $this->color_slide;
	}
	
	public function getColorInvert(){
		return $this->color_invert;
	}
	
	public function getColorGrayscale(){
		return $this->color_grayscale;
	}
	
	public function login(){	
		global 	$wpdb;
		$ip			= $this->ip;
		$id			= $this->id;
		$time		= date("dmyhisu");
		$rand		= rand(0,10000);
		$session	= md5($ip.$id.$time.$rand);		
		setSession('userid',$this->id);
		setSession('session',$session);		
		$query		= "UPDATE smis_adm_user set last_login_ip='".$this->ip."' , last_login_time=CURRENT_TIMESTAMP(), last_fetch=CURRENT_TIMESTAMP(), session='".$session."',  is_active='active' WHERE id='".$this->id."'";
		$wpdb->query($query);
		return $this;
	}
    
    
    public function loginByDirect($username,$password){
        global 	$wpdb;
        $password	= md5($password);
        $query		= "SELECT * FROM smis_adm_user WHERE username='$username' AND prop!='del' AND password='$password' ";
        $data_user	= $wpdb->get_row($query);
        if($data_user!=null){
            $this->setUser($data_user);
            return true;
        }
        return false;
    }
	
	public function logout(){
		global 	$wpdb;
		$id		= getSession('userid');
		$query	= "UPDATE smis_adm_user set is_active='' WHERE id='".$id."'";
		$wpdb->query($query);
		return $this;
	}
	
	public function getLastChange(){
		if($this->timediff*1>30){
			return "<div style='color:#992222'><strong>".ArrayAdapter::format("date d M Y H:i", $this->last_ps)." (Password Anda Telah Berumur ".$this->timediff." Hari atau Lebih, Silakan Diganti)</strong></div>";
		}
		return ArrayAdapter::format("date d M Y H:i", $this->last_ps);
	}
	
	public function rangeChange(){
		return $this->timediff*1;
	}
	
	public function updatePassword($old,$new){
		global $wpdb;
		if($old==$this->password){
			$query	= "UPDATE smis_adm_user SET password='".$new."', last_change=CURRENT_TIMESTAMP() WHERE id='".$this->id."'";
			$wpdb->query($query);
			$this->loadUser($this->id);
			return true;
		}
		return false;
	}
	
	public function isAuthorize($page,$action){
		if($this->menu!=null && isset($this->menu[$page]) && isset($this->menu[$page][$action]) && $this->menu[$page][$action]=="1" )
			return true;
		return false;
	}
	

	public function getMenu(){
		return $this->menu;
	}
	
	public function isAdministrator(){
		return $this->capability=="administrator";
	}
	
	public function getCapability(){
		return $this->capability;
	}
	
	public function getUsername(){
		return $this->username;
	}
	
	public function getName(){
		return $this->username." (".$this->name.")"; 
	}
	
	public function getNameOnly(){
		return $this->name;
	}
	
	public function getIp(){
		return $this->ip;
	}
	
	public function getPost(){
		return $this->post;
	}
	
	public function getMac(){
		return $this->mac;
	}
	
	public function getID(){
		return $this->id;
	}
	
	function get_client_ip() {
		require_once 'smis-libs-function/smis-libs-function-essential.php';
		return getClientIP();
	}
	
	function get_mac_address($ipAddres){
		$macAddr=false;
		#run the external command, break output into lines
		$arp="arp -a ".$ipAddres;
		$lines=explode("\n", $arp);
		
		#look for the output line describing our IP address
		foreach($lines as $line){
			$cols=preg_split('/\s+/', trim($line));
			if ($cols[0]==$ipAddres)
			{
				$macAddr=$cols[1];
			}
		}
		return $macAddr;
	}
	
	function getAllPost(){
		$result = "";
		foreach ($_POST as $key => $value) {
			$result .= "<div>".$key." : ".$value."</div></br>";
		}
		return $result;
	}
	
	function getPostRaw(){
		return $_POST;
	}
}
?>