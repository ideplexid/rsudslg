<?php 

/**
 * this class used for handling user
 * logging. since the user all data need to be logging
 * for debugging and administration purpose
 * 
 * 
 * @used 	 - smis-farmework/smis/service/ServiceConsumer.php
 * 		 	 - smis-service/sevice.php
 * 		 	 - smis-base/smis-service.php
 * 		 	 - smis-base/smis-service-caller.php
 * 		 	 - smis-base/smis-service-variable.php
 * 		 	 - smis-base/smis-init-variable.php
 * 		 	 - smis-framework/smis/database/Database.php 
 * 		 	 - smis-libs-class/Cronjob.php 
 * 
 * @database - smis_adm_error
 * 			 - smis_adm_service_requests
 * 			 - smis_sb_log
 * 			 - smis_adm_service_responds
 * 			 - smis_adm_entity_response
 * 			 - smis_adm_log
 *           - smis_adm_cronjob
 * 
 * 
 * @author 		: Nurul Huda
 * @copyright 	: goblooge@gmail.com
 * @version 	: 12.0.2
 * @license 	: LGPLv3
 * @since       : 15 May 2014
 * */

class QueryLog {
	
	/*@var array*/
	private $lquery;
	/*@var array*/
	private $error_php;
	/*@var array*/
	private $error_query;
	/*@var array*/
	private $service;
	/*@var User*/
	private $user;
	/*@var Database*/
	private $db;
	/*@var boolean*/
	private $state;
	/*@var array*/
	private $msg_log;
	/*@var int*/
	private static $LOG_LEVEL_SYSTEM=1;
	/*@var int*/
	private static $LOG_LEVEL_REQUEST=2;
	/*@var int*/
	private static $LOG_LEVEL_SERVERBUS=4;
	/*@var int*/
	private static $LOG_LEVEL_RESPONSE=8;
	/*@var int*/
	private static $LOG_LEVEL_SERVICE=16;
	/*@var int*/
	private static $LOG_LEVEL_USER=32;
	/*@var int*/
	private static $LOG_LEVEL_CRONJOB=64;
    /*@var int*/
	private static $LOG_LEVEL_SYNCH_CONSUMER=128;
    /*@var int*/
	private static $LOG_LEVEL_SYNCH_PROVIDER=256;
    
	/**
	 * @brief the constructor
	 * @param DBController $wpdb 
	 * @param User $user 
	 * @param string $state 
	 * @return  null
	 */
	public function __construct($wpdb,$user,$state="false"){
		$this->user			= $user;
		$this->query		= array();
		$this->error_query	= array();
		$this->error_php	= array();
		$this->service		= array();
		$this->state		= $state;
		$this->msg_log		= array();
		$this->wpdb			= $wpdb;
	}
	
	/* if state enabled then it will be
     * save the log
	 */
	public function setEnabled($enable){
		$this->state		= $enable;
        return $this;
	}
	
    /**
     * @brief adding the query that proces in this methode
     * @param string $query 
     * @return  this
     */
	public function addQuery($query){
        if(is_log_disabled())
            return $this;
        
		if($this->state!="false" )
            $this->query[]	= $query;
        return $this;
	}
    
    /**
     * @brief adding error message for php
     * @param string $error 
     * @return  this
     */
	public function addError($error){
        if(is_log_disabled())
            return $this;
        
		if($this->state!="false")
			$this->error_php[]	= $error;
        return $this;
	}
	
    /**
     * @brief adding error query for mysql
     * @param string $error 
     * @return  this
     */
	public function addErrorQuery($error){
        if(is_log_disabled())
            return $this;
        
		if($this->state!="false" && !$error=="")
			$this->error_query[]	= $error;
        return $this;
	}
	
    /**
     * @brief adding service process
     * @param string $name is the service name 
     * @param string $code is the service code for tracking 
     * @return  this
     */
	public function addService($name,$code){
        if(is_log_disabled())
            return $this;
        
		if($this->state!="false" && !$name=="")
			$this->service[]	= $name." : ".$code;
        return $this;
	}
	
	/**
	 * @brief put a log defined by programmer
	 * 		  used for debugging process
	 * @param String $msg that what programmer want to show 
	 * @return  this
	 */
	public function addMessage($msg){
		$this->msg_log[]	= $msg;
		return $this;
	}
	/**
	 * @brief get the message log
	 * @return  array of the message
	 */
	public function getMessage(){
		return $this->msg_log;
	}
	
	/**
	 * @brief save the user request based on system
	 * 		 it would save on database or saved on 
	 * 		 file based on user setting in smis-config.php
	 * 		 in LOG_ONFILE variable, if 1 then save on file
	 * 		 if 0 then save on database.
	 * @return  null
	 */
	public function save(){
        if(is_log_disabled(self::$LOG_LEVEL_SYSTEM))
            return;
        
		if((	$this->state=="true" || ( $this->state=="selective" && count($this->error_php)+count($this->error_query)>0 ) )
				&& ($this->query!="") 
				&& !(isset($_POST['page']) 
				&& $_POST['page']=="smis-administrator") 
				&& isset($_POST['page']) 
				&& isset($_POST['action']) ){
					global $TIME_START;
                    global $MEMORY_START;
					$data['user']			= $this->user->getName();
					$data['ip']				= $this->user->getIp();
					$data['query']			= json_encode($this->query);
					$data['error_query']	= json_encode($this->error_query);
					$data['error_php']		= json_encode($this->error_php);
					$data['error_log']		= json_encode($this->msg_log);
					$data['service']		= json_encode($this->service);
					$data['post']			= json_encode($_POST);                    
					$data['time_usage']		= microtime(true)-$TIME_START;
                    $data['memory_usage']	= memory_get_peak_usage()-$MEMORY_START;
                    
					if(is_log_save_on_file() ){
						$filename			= date("Y-m-d H:i:s");
						$fullpath			= createLogDirectory($filename,$_POST['page'],$_POST['action']);
						createFolder($fullpath);
						$filename		   .= "_";
						if( isset($_POST['command']) && $_POST['command']!="" ) 	$filename.=($_POST['command']); 
						if($data['error_php']!="[]") 								$filename.="_P_";
						if($data['error_query']!="[]") 								$filename.="_Q_";
						if(count($this->service)>0) 								$filename.="_S_";
						if(count($this->msg_log)>0) 								$filename.="_M_";
						$filename		   .= ".json";
						return file_put_contents($fullpath.$filename,json_encode($data));
					}else{
						$this->wpdb->insert("smis_adm_error",$data);
					}
		}
	}
	
	/**
	 * @brief save the service request by a user
	 * 		  used in smis-farmework/smis/service/ServiceConsumer.php
	 * @param String $post the $_POST that user request 
	 * @param String $msg the request that put by user
	 * @param Strign $res the response from server bus
	 * @param String $code code fot debugging
	 * @return  null;
	 */
	public function addConsumer($post,$msg,$res,$code,$service,$autonomous){
        if(is_log_disabled(self::$LOG_LEVEL_REQUEST))
            return;
        
		$data['post']		= $post;
		$data['code']		= $code;
		$data['response']	= $res;
		$data['request']	= $msg;
		if(is_log_save_on_file()){
			$filename		= date("Y-m-d H:i:s")."_service_request_".$autonomous.".json";
			$fullpath		= createLogServiceDirectory($code,$service);
			$content		= json_encode($data);
			file_put_contents($fullpath.$filename,$content);
		}else{
			$this->wpdb->insert("smis_adm_service_requests",$data);
		}
	}
	
	/**
	 * @brief this function used for saving server bus log
	 * 		  wether in database or in a file
	 * 		  used id smis-service/sevice.php
	 * @param String $code or service 
	 * @param String $data that need to saved 
	 * @param String $autonomous autonomous that call 
	 * @return  
	 */
	public function saveServerBusLog($service,$code,$data,$autonomous){
        if(is_log_disabled(self::$LOG_LEVEL_SERVERBUS))
            return;
        
		if(is_log_save_on_file()){
			$fullpath	= createLogServiceDirectory($code,$service);
			$filename	= date("Y-m-d H:i:s")."_serverbus_log_".$autonomous.".json";
			$content	= json_encode($data);
			file_put_contents($fullpath.$filename,$content);
		}else{
			$this->wpdb->insert("smis_sb_log",$data);
		}
	}
	
	/**
	 * @brief this is used for saving service that provide by single autonomout
	 * 		  used in smis-base/smis-service.php
	 * @param String $code the service code
	 * @param array $data that need to be save
	 * @param String $autonomous autonomous that call 
	 * @return  false
	 */
	public function saveServiceAutonomous($service,$code,$data,$autonomous){
        if(is_log_disabled(self::$LOG_LEVEL_RESPONSE))
            return;
        
		if(is_log_save_on_file()){
			$erponse		= "";
			if($data['erponse']!="[]"){
				$erponse	= "_erponse_";
			}
			$fullpath		= createLogServiceDirectory($code,$service);
			$filename		= date("Y-m-d H:i:s")."_autonomous_log_".$autonomous.$erponse.".json";
			$content		= json_encode($data);
			file_put_contents($fullpath.$filename,$content);
		}else{
			$this->wpdb->insert("smis_adm_service_responds",$data);
		}
	}
	
	
	/**
	 * @brief this is used for saving service that provide by single autonomout
	 * 		  used in smis-base/smis-service-caller.php
	 * @param String $code the service code
	 * @param array $data that need to be save
	 * @param String $autonomous autonomous that call 
	 * @return  false
	 */
	public function saveServiceEntityLog($service,$code,$data,$autonomous,$entity){
        if(is_log_disabled(self::$LOG_LEVEL_SERVICE))
            return;
        
		if(is_log_save_on_file()){			
			$data['query']			= json_encode($this->query);
			$data['error_query']	= json_encode($this->error_query);
			$data['error_php']		= json_encode($this->error_php);
			$data['error_log']		= json_encode($this->msg_log);
			$data['service']		= json_encode($this->service);			
			$postfile				= "";
			if($data['error_php']!="[]") 	$postfile .= "_P_";
			if($data['error_query']!="[]") 	$postfile .= "_Q_";
			if(count($this->service)>0) 	$postfile .= "_S_";
			if(count($this->msg_log)>0) 	$postfile .= "_M_";
			
			$fullpath	= createLogServiceDirectory($code,$service);
			$filename	= date("Y-m-d H:i:s")."_entity_log_".$autonomous."_entity_".$entity."_".$postfile.".json";
			$content	= json_encode($data);
			file_put_contents($fullpath.$filename,$content);
		}else{
			$this->wpdb->insert("smis_adm_entity_response",$data);
		}
	}
	
	/**
	 * @brief save the database history in system.
	 * 		  every single row in database have a history
	 * 		  and here each history is saved.
	 * @used	- smis-framework/smis/database/Database.php 
	 * @param String $table the table name that need to save the history 
	 * @param Array $data the data that need to be save 
	 * @param String $dbid the id of the data that saved 
	 * @return  null
	 */
	public function saveDatabaseLog($table,$data,$dbid){
        if(is_log_disabled(self::$LOG_LEVEL_USER))
            return;
        
		$date	= date("Y-m-d H:i:s");
		if(is_log_save_on_file()){
			$username 	= $data['user'];
			$service	= $data['service'];			
			$command	= isset($data['action']) && $data['action']!=""? $data['action']:"";
			$code		= isset($data['code']) && $data['code']!=""? $data['code']:"";
			$fullpath	= createDatabaseLogDirectory($table,$dbid,$date);
			$filename	= $date."_dblog_".$table."_".$command."_usr_".$username."_svc_".$service."_code_".$code.".json";
			$content	= json_encode($data);
			file_put_contents($fullpath.$filename,$content);
		}else{
			$this->wpdb->insert("smis_adm_log",$data);
		}
	}
    
    
    /**
     * @brief save the timing of cronjob when it's running
     *          to determine whethear the cronjob running or not
     * @param datetime $waktu in format YYYY-MM-DD HH:ii:ss 
     * @param string $user the user that running this cronjob 
     * @param String $cronjob is A JSON that containts which cronjob was running 
     * @return  null
     */
    public function saveCronjob($waktu,$username,$cronjob){
        if(is_log_disabled(self::$LOG_LEVEL_CRONJOB))
            return;
        
		$date=date("Y-m-d H:i:s");
		if(is_log_save_on_file()){
			/* NOT YET IMPLEMENTED */
		}else{
            $data	= array("user"=>$username,"waktu"=>$waktu,"cronjob"=>$cronjob);
			$this->wpdb->insert("smis_adm_cronjob",$data);
		}
    }
    
    /**
     * @brief this used to save if there is an error synch provider
     * @param string $code_name the code name of synch provider
     * @param string $log the error log need to be save
     * @param string $service_code is md5hash code of service 
     * @return  null
     */
    public function saveSynchProvider($code_name,$log,$service_code){
        if(is_log_disabled(self::$LOG_LEVEL_SYNCH_PROVIDER) || $log=="")
            return;
            
        if(is_log_save_on_file()){
			/* NOT YET IMPLEMENTED */
		}else{
            $data					= array();
            $data['code_name']		= $code_name;
            $data['log']			= $log;
            $data['waktu']			= date("Y-m-d H:i:s");
			$data['service_code']	= $service_code;
			$this->wpdb->insert("smis_adm_log_synch_provider",$data);
		}        
    }
    
    /**
     * @brief this used to save if there is an error in synch consumer
     * @param string $code_name is the code name of synch consumer 
     * @param string $log is the error log 
     * @return  null
     */
    public function saveSynchConsumer($code_name,$log){
        if(is_log_disabled(self::$LOG_LEVEL_SYNCH_CONSUMER) || $log=="")
            return;
        
        
        if(is_log_save_on_file()){
			/* NOT YET IMPLEMENTED */
		}else{
            $data				= array();
            $data['code_name']	= $code_name;
            $data['log']		= $log;
            $data['waktu']		= date("Y-m-d H:i:s");
			$this->wpdb->insert("smis_adm_log_synch_consumer",$data);
		}
    }
}
?>