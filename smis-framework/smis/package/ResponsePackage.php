<?php

class ResponsePackage{
	private $alert;
	private $content;
	private $status;
	private $warning;
	private $splash;
	
	public static $TIPE_DANGER			= "alert-danger";
	public static $TIPE_SUCCESS			= "alert-success";
	public static $TIPE_WARNING			= "alert-warning";
	public static $TIPE_INFO			= "alert-info";
	
	public static $STATUS_UNAHTORIZED	= "unauthorized";
	public static $STATUS_LOGOUT		= "logout";
	public static $STATUS_NOT_INSTALLED	= "no-install";
	public static $STATUS_NO_COMMAND	= "no-command";
	public static $STATUS_FAIL			= "fail";
	public static $STATUS_OK			= "ok";
	
	public function __construct(){
		$this->alert	= array("status"=>"n");
		$this->content	= "";
		$this->status	= "n";
		$this->warning	= array("show"=>"n");
		$this->splash	= array("show"=>"n");
	}
	
	public function setWarning($show,$title,$content){
		if($show) {
			$show = "y";
		}else{
			$show = "n";
		}		
		$this->warning["show"]		= $show;
		$this->warning['title']		= $title;
		$this->warning['content']	= $content;
		return $this;
	}
	
	public function setSplash($show,$title,$content){
		if($show) {
			$show = "y";
		}else{
			$show = "n";
		}
		$this->splash["show"]		= $show;
		$this->splash['title']		= $title;
		$this->splash['content']	= $content;
		return $this;
	}
	
	public function setAlertContent($title,$content,$tipe="alert-info"){
		$this->alert['title']	= $title;
		$this->alert['content']	= $content;
		$this->alert['color']	= $tipe;
		return $this;
	}
	
	public function setAlertVisible($visible){
		if($visible){
			$this->alert['status'] = "y";
		}else{
			$this->alert['status'] = "n";
		}
		return $this;
	}
	
	public function setStatus($status){
		$this->status = $status;
		return $this;
	}
	
	public function setContent($content){
		$this->content = $content;
		return $this;
	}
	
	public function getContent(){
		return $this->content;
	}
	
	public function getPackage(){
		$data			 = array();
		$data['content'] = $this->content;
		$data['status']	 = $this->status;
		$data['alert']	 = $this->alert;
		$data['warning'] = $this->warning;
		return $data;
	}	
}
?>