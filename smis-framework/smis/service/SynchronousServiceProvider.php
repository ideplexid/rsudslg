<?php
/**
 * this class handling a synchronous and duplicate data
 * from one autonomous to antoher autonomous
 * each autonomous will have their own
 * data wether it's copied or it's collaborate
 * SINGLE DUPLICATE:mean only one master of autonomous 
 *                  and the other are slave. this case is very
 *                  good for handling master data. the weak of this methode
 *                  is that only one autonomous can only took the control of the data
 * MULTI DUPLICATE :mean there is no master, each autonomous collaborate 
 *                  to build one single big data. the advantage of this methode
 *                  every autonomous have advantages to share their data, but the data would be 
 *                  different since it asynchronous
 * 
 * @author      : Nurul Huda
 * @since       : 21 April 2017
 * @license     : Apache v 2.0
 * @version     : 1.0.0
 * @copyright   : goblooge@gmail.com
 * */

class SynchronousServiceProvider extends ServiceProvider{
	protected $duplicate_mode;
    public static $SINGLE_DUPLICATE=0;
    public static $MULTIPLE_DUPLICATE=1;
    private $skip_origin;
    private $autonomous;
    
	public function __construct(DBTable $dbtable,$skip=false, $autonomous=""){
		parent::__construct($dbtable, NULL, NULL);
        $this->duplicate_mode=self::$SINGLE_DUPLICATE;
        $this->skip_origin=$skip;
        $this->autonomous=$autonomous;
	}
	
	public function command($command){	
		if($command=="list"){
			$content=$this->view();
		}else if($command=='save'){
			$content=$this->save();
		}else if($command=="del"){
			$content=$this->delete();
		}else if($command=="edit"){
			$content=$this->edit();
		}else if($command=="select"){
			$content=$this->select();
		}else if($command=="print-element"){
			$content=$this->printing("print-element");
		}else if($command=="printing"){
			$content=$this->printing();
		}
		return $content;
	}
    
    public function isSkipAutonomous(){
        if(isset($_POST['acaller']) && $this->autonomous==$_POST['acaller'])
            return $this->skip_origin;
        return false;
    }
   
   /**
    * @brief if this set true then when the autonomous caller have same id
    *        the request will not process, since it's actually refer the same
    *        autonomous.
    * @param boolean $skip 
    * @return  SynchronousServiceProvider
    * */
    public function setSkipOrigin($skip){
        $this->skip_origin=$skip;
        return $this;
    }
    
    /**
     * @brief this used to determine how synch mode work
     *          there 2 mode. SINGLE and MULTIPLE
     *          -   multiple used to be for transaction process where each autonomous can insert
     *              since there are many inserted data, 
     *              multiple mode might have different id in different autonomous
     *          -   single used to be master data, where each autonomous have same id for each data.
     *              single mode usually for master data, where the id should same in all autonomous
     * @param int $mode 
     * @return  
     */
    public function setSynchMode($mode){
        $this->duplicate_mode=$mode;
        return $this;
    }
    
    public function save(){
        $data=$this->postToArray();
        if($this->duplicate_mode==self::$MULTIPLE_DUPLICATE){
           unset($data['id']);
        }
        
        if(!$this->isSkipAutonomous()){
            $kriteria['origin_id']=$this->getPost("origin_id", 0);
            $kriteria['origin']=$this->getPost("origin", "");
            
            if($kriteria['origin_id']==0 || $kriteria['origin']==""){
                /* if the origin id and the origin is not set, 
                 * then simply used the id */
                unset($kriteria['origin_id']);
                unset($kriteria['origin']);
                $kriteria['id']=$this->getPost("id", 0);
            }
            
            $the_id=0;
            if(!$this->dbtable->is_exist($kriteria,$this->is_exist_include_del)){
                $result=$this->dbtable->insert($data,$this->is_use_prop,$this->upsave_condition);
                $the_id=$this->dbtable->get_inserted_id();
                $success['type']='insert';
            }else {
                $current=$this->select($kriteria);
                $the_id=$current->id;
                if($old->time_updated<$data['time_updated']){
                   $result=$this->dbtable->update($data,$kriteria,$this->upsave_condition); 
                }else{
                    $result=true;
                }
                $success['type']='update';
            }
        }else{
            $the_id=$this->getPost("id", 0);
            $success['type']='skip';
        }
        
		$success['id']=$the_id;
		$success['success']=1;
		if($result===false) $success['success']=0;
		return $success;
	}
    
    
    
	
	public function view(){
		$kriteria=isset($_POST['kriteria'])?$_POST['kriteria']:"";
		$number=(isset($_POST['number']) && $_POST['number']!="")?$_POST['number']:0;
		$max=isset($_POST['max'])?$_POST['max']:"10";
		$this->dbtable->setMaximum($max);
		$d=$this->dbtable->view($kriteria,$number);
		return $d;
	}
	
	public function edit(){
		$id=$_POST['id'];
		$row=$this->dbtable->select($id,false);
		return $row;
	}
	
	public function select(){
		$row=$this->edit();
		return $row;
	}
	
}

?>
