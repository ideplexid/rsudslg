<?php

/**
 * this class used to make a responder for uitable, and adapter
 * instead using a dbtable class like DBResponder, this class
 * used service as the data transformation
 * 
 * @author      : Nurul HUda
 * @since       : 12 May 2015
 * @copyright   : goblooge@gmail.com
 * @version     : 2.0.2
 * */

class ServiceResponder extends Responder {
	private $db;
    /*@var Table*/
	private $uitable;
	/*@var ArrayAdapter*/
	private $adapter;
    /*@var ServiceConsumer*/
	private $service;	
    /*@var Array*/
	private $addition_data;
    /*@var Array*/
    private $substract_data;
    /*@var string*/
	private $mode;
    /*@var string*/
	private $sorted;
    
	public function __construct($db,$uitable,$adapter,$service,$entity="all",$autonomous="all",$tuser=null){
		$this->service=new ServiceConsumer($db, $service,null,$entity,$autonomous,$tuser);
		$this->adapter=$adapter;
		$this->uitable=$uitable;
		$this->addition_data=array();
        $this->substract_data=array();
        $this->db=$db;
		$this->mode=ServiceConsumer::$SINGLE_MODE;
        $this->sorted=null;
	}
    
    /**
     * @brief adding function sorting
     * @param a function to sort $sort 
     * @return  
     */
    public function setSortingMethode($sort){
        $this->sorted=$sort;
    }
    
    /**
     * @brief set the adapter for the uitable
     * @param <unknown> $adapter 
     * @return  
     */
	public function setAdapter($adapter=null){
		if($adapter==null && $this->adapter==null)
			$this->adapter=new SimpleAdapter();
		else $this->adapter=$adapter;
		return $this;
	}
	
    /**
     * @brief set the entity for
     * @param string $entity 
     * @return  
     */
	public function setEntity($entity){
		$this->service->setEntity($entity);
	}
	
    /**
     * @brief set the mode of service
     * @param string $mode 
     * @return  
     */
	public function setMode($mode){
		$this->mode=$mode;
		if($mode!=ServiceConsumer::$SINGLE_MODE){
			$this->service->setMode(ServiceConsumer::$MULTIPLE_MODE);
		}else{
			$this->service->setMode($mode);
		}
		return $this;
	}
    
    /**
     * @brief add the service data
     * @param string $name 
     * @param string $value 
     * @return  this
     */
	public function addData($name,$value){
		$this->addition_data[$name]=$value;
		return $this;
	}
    
    /**
     * @brief remove the data
     * @param string name $name 
     * @return  this
     */
    public function removeData($name){
		$this->substract_data[]=$name;
		return $this;
	}
	
    /**
     * @brief using adapter whene select a data called
     * @param boolean $use 
     * @return  this
     */
	public function setUseAdapterForSelect($use){
		$this->is_adapter_for_select=$use;
		return $this;
	}
	
	/**
	 * @brief get the ui Table
	 * @return  Table
	 */
	public function getUITable(){
		return $this->uitable;
	}
    
    /**
     * @brief set the ui table in responder
     * @param Table $uitable 
     * @return  this
     */
    public function setUITable($uitable){
		$this->uitable=$uitable;
        return $this;
	}
	
    /**
     * @brief get the Database
     * @return  Database
     */
	protected function getDB(){
		return $this->db;
	}
	
    /**
     * @brief get the service name
     * @return  string
     */
	public function getService(){
		return $this->service;
	}
	
    /**
     * @brief change the service name that want to be
     *          changed
     * @param string $name 
     * @return  $this
     */
    public function setServiceName($name){
        $this->service->setService($name);
        return $this;
    }
    
    /**
     * @brief set or change the service consumer that want to be custom
     * @param \ServiceConsumer $service 
     * @return  this
     */
	public function setService(ServiceConsumer $service){
		$this->service=$service;
		return $this;
	}
	
	public function command($command){
		$pack=new ResponsePackage();
		$content=NULL;
		$status='not-command';	//not-authorized, not-command, fail, success
		$alert=array();
		
		if($command=="list"){
			if($this->mode==ServiceConsumer::$SINGLE_MODE)
				$content=$this->view();
			else $content=$this->multiview();
			$pack->setContent($content);
			$pack->setStatus(ResponsePackage::$STATUS_OK);
		}else if($command=='save'){
			$content=$this->save();
			$pack->setContent($content);
			$pack->setStatus(ResponsePackage::$STATUS_OK);
			$pack->setAlertVisible(true);
			$pack->setAlertContent("Data Saved", "Your Data Had Been Saved", ResponsePackage::$TIPE_INFO);
		}else if($command=="del"){
			$content=$this->delete();
			$pack->setContent($content);
			$pack->setStatus(ResponsePackage::$STATUS_OK);
			$pack->setAlertVisible(true);
			$pack->setAlertContent("Data Removed", "Your Data Had Been Removed", ResponsePackage::$TIPE_INFO);
		}else if($command=="edit"){
			$content=$this->edit();
			$pack->setContent($content);
			$pack->setStatus(ResponsePackage::$STATUS_OK);
		}else if($command=="select"){
			$content=$this->select();
			$pack->setContent($content);
			$pack->setStatus(ResponsePackage::$STATUS_OK);
		}else if($command=="print-element"){
			$content=$this->printing("print-element");
			$pack->setContent($content);
			$pack->setStatus(ResponsePackage::$STATUS_OK);
		}else if($command=="printing"){
			$content=$this->printing();
			$pack->setContent($content);
			$pack->setStatus(ResponsePackage::$STATUS_OK);
		}else if($command=="excel"){
			$this->excel();
			return NULL;
		}
		return $pack->getPackage();
	}
	
	public function getData(){	
		$post=$_POST;
		foreach($this->addition_data as $name=>$value){
			$post[$name]=$value;
		}
        
        foreach($this->substract_data as $name=>$value){
			unset($post[$value]);
		}
		return $post;
	}
	
	protected function joinEntity($d){	
        $result=array();
		foreach($d as $autonomous=>$autonomous_content){
			foreach ($autonomous_content as $entity=>$entity_content){
				$data=$entity_content['data'];
				foreach($data as $index=>$data_content){
					$data_content['smis_entity']=ArrayAdapter::format("unslug", $entity);
					$result[]=$data_content;
				}
			}
		}
		return $result;
	}
	
	protected function joinAutonomous($d){
		$result=array();
		foreach($d as $autonomous=>$autonomous_content){
			foreach ($autonomous_content as $entity=>$entity_content){
				$data=$entity_content['data'];
				foreach($data as $index=>$data_content){
					$data_content['smis_autonomous']=ArrayAdapter::format("unslug", $autonomous);
					$result[]=$data_content;
				}
			}
		}
		return $result;
	}
	
	protected function joinBoth($d){
		$result=array();
		foreach($d as $autonomous=>$autonomous_content){
			foreach ($autonomous_content as $entity=>$entity_content){
				$data=$entity_content['data'];
				foreach($data as $index=>$data_content){
					$data_content['smis_entity']=ArrayAdapter::format("unslug", $entity);
					$data_content['smis_autonomous']=ArrayAdapter::format("unslug", $autonomous);
					$result[]=$data_content;
				}
			}
		}
		return $result;
	}
	
	protected function cleanAutonomous($d){
		$result=array();
		foreach($d as $autonomous=>$autonomous_content){
			foreach ($autonomous_content as $entity=>$entity_content){
				$data=$entity_content['data'];
				if(!isset($result[$entity])) 
					$result[$entity]=array();
				foreach($data as $index=>$data_content){
					$result[$entity][]=$data_content;
				}
			}
		}
		return $result;
	}
	
	protected function cleanEntity($d){
		$result=array();
		foreach($d as $autonomous=>$autonomous_content){
			foreach ($autonomous_content as $entity=>$entity_content){
				$data=$entity_content['data'];
				if(!isset($result[$autonomous])) 
					$result[$autonomous]=array();
				foreach($data as $index=>$data_content){
					$result[$autonomous][]=$data_content;
				}
			}
		}
		return $result;
	}
	
	protected function cleanBoth($d){
		$result=array();
		foreach($d as $autonomous=>$autonomous_content){
			foreach ($autonomous_content as $entity=>$entity_content){
				$data=$entity_content['data'];
				foreach($data as $index=>$data_content){
					$result[]=$data_content;
				}
			}
		}
		return $result;
	}
	
    
    
	public function getDataMode($d){
		$hasil=null;
		switch ($this->mode){
			case ServiceConsumer::$CLEAN_AUTONOMOUS : $hasil=$this->cleanAutonomous($d); break;
			case ServiceConsumer::$CLEAN_BOTH: $hasil=$this->cleanBoth($d); break;
			case ServiceConsumer::$CLEAN_ENTITY: $hasil=$this->cleanEntity($d); break;
			case ServiceConsumer::$JOIN_AUTONOMOUS: $hasil=$this->joinAutonomous($d); break;
			case ServiceConsumer::$JOIN_ENTITY : $hasil=$this->joinEntity($d); break;
			case ServiceConsumer::$JOIN_BOTH: $hasil=$this->joinBoth($d); break;
			default  : $hasil=$d; break;
		}
		return $hasil;
	}
	
	public function multiview(){
        //TODO ganti dengan source code untuk mengambil dan menggabungkan dari beberapa list
		$this->service->setData($this->getData());
		$this->service->execute();
		$d=$this->service->getContent();
        $data_ready=$this->getDataMode($d);		
        if($this->sorted!=null){
            usort($data_ready,$this->sorted);
        }
		$page=0;
		$max_page=10;
		$uidata=$this->adapter->getContent($data_ready);
		$this->uitable->setContent($uidata);
		$list=$this->uitable->getBodyContent();
		$pagination=$this->uitable->getPagination($page,5,$max_page);
	
		$json['list']=$list;
		$json['pagination']=$pagination->getHtml();
		$json['number']=$page;
		$json['d']=$d;
		$json['number_p']=isset($_POST['number'])?$_POST['number']:"0";
		return $json;
	}
	
	public function view(){
        
        $this->service->setData($this->getData());
		$this->service->execute();
		$d=$this->service->getContent();
        
		$page=$d['page'];
		$data=$d['data'];
		$max_page=$d['max_page'];
				
		$uidata=$this->adapter->getContent($data);
		$this->uitable->setContent($uidata);
		$list=$this->uitable->getBodyContent();
		$pagination=$this->uitable->getPagination($page,5,$max_page);
	
		$json['list']=$list;
		$json['pagination']=$pagination->getHtml();
		$json['number']=$page;
		$json['d']=$d;
		$json['number_p']=isset($_POST['number'])?$_POST['number']:"0";
		return $json;
	}
	
	public function save(){
		$this->service->setData($this->getData());
		$this->service->execute();
		$d=$this->service->getContent();
		return $d;
	}
	
	public function delete(){
		$this->service->setData($this->getData());
		$this->service->execute();
		$d=$this->service->getContent();
		return $d;
	}
	
	public function edit(){
		$this->service->setData($this->getData());
		$this->service->execute();
		$d=$this->service->getContent();
		return $d;
	}
	
	public function select(){
		$row=$this->edit();
		if($this->is_adapter_for_select){
			$row=$this->adapter->adapt($row);
		}
		return $row;
	}
	
	public function printing(){
		return "override_this";
	}
	
	
	public function printElement(){
		return "override_this";
	}
    
    public function getAdapter(){
        return $this->adapter;
    }
	
	public function excel(){
		global $user;
		loadLibrary("smis-libs-function-export");
        $this->addData("command","list");
        $this->service->setData($this->getData());
		$this->service->execute();
		$d=$this->service->getContent();
        
        $start_number=$page*$max;
		$this->adapter->setNumber($start_number);
		$data=$d['data'];
        
		$uidata=$this->adapter->getContent($data);
		$filename=$this->service;
		$filename.="-".$kriteria;
		$filename.=" [ ".($start_number)." - ".($start_number+count($uidata))." ]";
		$filename.=" ( ".ArrayAdapter::format("date d M Y H:i:s", date("Y-m-d H:i:s")).") ";
		$filename.=" - ".$user->getNameOnly();
		createExcel($filename, $uidata);
		return;
	}
	
	
}

?>
