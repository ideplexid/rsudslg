<?php 

/**
 * this class use to handling send request from 
 * one autonomous to antoher server bus
 * or server bus to another autonomous.
 * adding a feature that if defined then system will
 * not execute the service if the service defined 
 * not to be, instead using the cached file that
 * already save in the temporary folder.
 * 
 * @since    : 19 Mei 2014
 * @version  : 1.2.3
 * @license  : LGPLv3
 * @author   : Nurul Huda
 * @copyright: goblooge@gmail.com
 * */
class SendRequest{
    /*@var array*/
	private $package;
	/*@var string*/
	private $destination;
	/*@var Communication*/
    private $comunication;
	/*@var string*/
    private $message;
    /*@var boolean*/
    private $is_cached;
    /*@var string*/
	private $cached_name;
    
    
    /**
     * @brief the constructor
     * @param string $dest autonomous 
     * @return  null
     */
	public function __construct($dest){
		$this->package          = null;
		$this->destination      = $dest;
		$this->comunication     = new Communication();
        $this->is_cached        = false;
        $this->cached_name      = "";
	}
    
    /**
     * @brief set the cached function so system do not need to take 
     *          service if the cache is available
     * @param boolean $enabled 
     * @param string $cached_name 
     * @return  this
     */
    public function setCached($enabled,$cached_name){
        $this->is_cached        = $enabled;
        $this->cached_name      = $cached_name;
        return $this;
    }
	
    /**
     * @brief the the key, a sending key and the receive key
     * @param string $send_key is the key that use to encrypt when sending a message 
     * @param string $receive_key is the key that use to encrypt when receive a message
     * @return  this;
     */
	public function setKeys($send_key,$receive_key){
		$this->comunication->setKey($send_key, $receive_key);
        return $this;
	}
	
    /**
     * @brief set the data pack that would be send to serverbus/other autonomous
     * @param array $pack 
     * @return  this
     */
	public function setPackage($pack){
		$this->package          = $pack;
        return $this;
	}
	
    /**
     * @brief set the destination autonomouse
     * @param string $destination 
     * @return  this
     */
	public function setDestination($destination){
		$this->destination      = $destination;
        return $this;
	}
    
    /**
     * @brief get the message that catch from the other autonomous
     * @return  string
     */
    public function getSendedMessage(){
		return $this->message;
	}
	
    /**
     * @brief execute the request 
     *          if cache enabled the and available used the cache
     *          if cache enabled and not available then use the service then save to the cache
     *          otherwise use the service
     * @return string result
     */
	public function execute(){
        if(is_cached() && $this->is_cached){
            if($this->isCacheAvailable()){
                return file_get_contents("smis-temp/".$this->cached_name.".smc");
            }else{
                $result=$this->executeService(); 
                file_put_contents("smis-temp/".$this->cached_name.".smc",$result);
                return $result;
            }
        }else{
           return $this->executeService(); 
        }
	}
    
    /**
     * @brief check is the cache file available
     * @return  boolean
     */
    private function isCacheAvailable(){
        return (file_exists("smis-temp/".$this->cached_name.".smc"));
    }
    
    /**
     * @brief save the result to the cached file
     * @param string $content 
     * @return  this
     */
    private function saveCache($content){
        file_put_contents($this->cached_name,$content);
        return $this;
    }
    
    /**
     * @brief execute the request , so the data would sent to other autonomous
     * @return  the message that catch from the other autonomous
     */
    private function executeService(){
        $this->message  = $this->package->getMessage();
		$msg            = $this->comunication->send_command(json_encode($this->message));
		$params         = array('msg' => $msg,"smis-agent"=>"smis-service");
		$query          = http_build_query ($params);
		$contextData    = array (
				'method' => 'POST',
				'header' => "Connection: close\r\n".
				"Content-Type: application/x-www-form-urlencoded\r\n".
				"Content-Length: ".strlen($query)."\r\n",
				'content'=> $query );
		$context        = stream_context_create (array ( 'http' => $contextData ));
		$result         = file_get_contents($this->destination, false, $context);
		$result         = $this->comunication->receive_command($result);
		return $result;
    }
}

?>