<?php
/**
 * this class use to manage encription of communication beetween one
 * autonmous to another autonomous, in this case is autonomous and serverbus
 * 
 * @author : Nurul Huda
 * @since : 13 Juni 2014
 * @license : Apache 2
 * @copyright : goblooge@gmail.com
 * @version : 1.0.0
 * */

require_once(dirname(__FILE__)."/Encryption.php");
class Communication{
    /* @var Encryption*/
	private $send;
    /* @var Encryption*/
	private $receive;

	public function __construct(){
		$this->send		= new Encryption();
		$this->receive	= new Encryption();
	}
    
    /**
     * @brief the data key of the communication
     * @param string $send_key, is the key when send a data 
     * @param string $receive_key, is the key when encrypt the data 
     * @return  null
     */
	public function setKey($send_key,$receive_key){
		$this->send->setKey($send_key);
		$this->receive->setKey($receive_key);
	}
    
    /**
     * @brief sending message command to serverbus
     * @param string $text 
     * @return  encrypted message
     */
	public function send_command($text){
		$this->send->setText($text);
		$this->send->encrypt();
		return $this->send->getCypher();
	}

	/**
     * @brief receive message command from serverbus
     * @param string $text 
     * @return  encrypted message
     */
	public function receive_command($cypher){
		$this->receive->setCypher($cypher);
		$this->receive->decrypt();
		return $this->receive->getText();
	}
}

?>