<?php 
/**
 * this class used for handling data 
 * packaging that would sent from
 * one autonomous to antoher autonomous
 * in this case usually from one autonomous to server bus
 * 
 * @author : Nurul Huda
 * @copyright : goblooge@gmail.com
 * @since : 13 Mei 2014
 * @version : 1.0.0
 * @license : LGPLv3
 * */
 
class PackageComunication{
	/*@var string*/
	private $entity;
	/*@var string*/
	private $autonomous;
	/*@var string*/
	private $service;
    /*@var string*/
	private $acaller;
    /*@var mixed*/
	private $data;
	/*@var string*/
	private $code;
	/*@var string*/
	private $user;
	/*@var string*/
	private $ip;
	/*@var string*/
	private $return_key;
	/*@var string*/
	private $autonomous_id;
	
    /**
     * @brief the constructor
     * @param string $autonomous destination 
     * @param string $entity entity destination 
     * @param string $service the service destination
     * @param string $data that wnat to sent 
     * @return  null
     */
	public function __construct($autonomous,$entity,$service,$data=""){
		$this->autonomous	 = $autonomous;
		$this->entity		 = $entity;
		$this->service		 = $service;
		$this->data			 = $data;
		$this->return_key	 = "";
		$this->user			 = "noname";
		$this->ip			 = "127.0.0.1";
		global $db;
		$this->acaller		 = getSettings($db, "smis_autonomous_name", "Nothing",true);
		$this->autonomous_id = getSettings($db, "smis_autonomous_id", "Nothing",true);
	}
	
    /**
     * @brief set the data parameter 
     * @param string $return_key, is the key that would use as key descruyptor 
     * @param string $user is the user that access or using this service 
     * @param string $ip is the ipaddress of the user that use this service 
     * @param string $code is the code for tracking purpose 
     * @return  this
     */
	public function setParams($return_key,$user,$ip,$code=null){
		$this->return_key	= $return_key;
		$this->user			= $user;
		$this->ip			= $ip;
		if($code==null) {
			$this->code		= serviceCodeCreator($user,$ip,$this->acaller);
		} else { 
			$this->code		= $code;
		}
        return $this;
	}
	
    /**
     * @brief is set the code that would use as service encryption for tracking purpose
     * @param string $code 
     * @return  this
     */
	public function setCode($code){
		$this->code			= $code;
        return $this;
	}
	
    /**
     * @brief get the message after service 
     *        finished
     * @return  array
     */
	public function getMessage(){
		$pack					= array();
		$pack['autonomous']		= $this->autonomous;
		$pack['entity']			= $this->entity;
		$pack['service']		= $this->service;
		$pack['data']			= $this->data;
		$pack['user']			= $this->user;
		$pack['return_key']		= $this->return_key;
		$pack['autonomous_id']	= $this->autonomous_id;
		$pack['ip']				= $this->ip;
		$pack['code']			= $this->code;
		$pack['acaller']		= $this->acaller;
		return $pack;
	}
	
    /**
     * @brief set the destination entity
     * @param string $entity 
     * @return  this
     */
	public function setEntity($entity){
		$this->entity			= $entity;
        return $this;
	}
	
	/**
     * @brief set the destination autonomous
     * @param string $aut 
     * @return  this
     */
	public function setAutonomous($aut){
		$this->autonomous		= $aut;
        return $this;
	}
	
    /**
     * @brief set the destination service
     * @param string $service 
     * @return  this
     */
	public function setService($ser){
		$this->service			= $ser;
        return $this;
	}
	
    /**
     * @brief get the string code for tracking purpose
     * @return  string
     */
	public function getCode(){
		return $this->code;
	}
	
    /**
     * @brief set the data that want to send
     * @param string $data 
     * @return  this
     */
	public function setData($data){
		$this->data				= $data;
        return $this;
	}
	
}

?>