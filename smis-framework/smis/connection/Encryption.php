<?php

/**
 * this class provide and encryotion methode
 * base on md5, MCRYPT_RIJNDAEL_256 and MCRYPT_MODE_CBC
 * 
 * @author Nurul Huda
 * @license LPGLv3
 * @since 13 Mei 2014
 * @version 1.2.1
 * @copyright goblooge@gmail.com
 */
 
class Encryption{
    /*@var string*/
	private $key;
	/*@var string*/
	private $text;
	/*@var string*/
	private $gen_text;
	/*@var string*/
	private $cypher;
	
    public function __construct(){
		
	}
	
	public function isEncryptEnable(){
		if(defined('SMIS_ENCRYPT')){
			return SMIS_ENCRYPT==1;
		}else{
			return true;
		}
	}
	
	/**
	 * set the key parameter
	 * @param string $key
	 */
	public function setKey($key){
		$this->key		= md5($key);
		$this->gen_text	= md5($this->key);
	}
	
	/**
	 * get the key parameter
	 * @return string
	 */
	public function getKey(){
		return $this->key;
	}
	
    /**
     * @brief set the text that want to be encrypted
     * @param string $text 
     * @return  this
     */
	public function setText($text){
		$this->text	= $text;
        return $this;
	} 
    
    /**
     * @brief get the text that not encrypted
     * @return  string non excrypted text
     */
	public function getText(){
		return $this->text;
	}
    
    /**
     * @brief set the cypher text that want to decrypt
     * @param string $cypher 
     * @return  this
     */
	public function setCypher($cypher){
		$this->cypher	= $cypher;
        return $this;
	} 
    
    /**
     * @brief get the data cypher of the text
     * @return  string
     */
	public function getCypher(){
		return $this->cypher;
	}
    
    /**
     * @brief the cnryption process of the text with the key and the value
     * @return  this
     */
	public function encrypt(){
		$this->cypher	= $this->text;
        return $this;

	}
    
    /**
     * @brief the decripting process of cypher text with the key that already stored
     * @return  
     */
	public function decrypt(){
		$this->text=$this->cypher;
        return $this;
	}
}
?>
