<?php 

/**
 * this class used to handling a table
 * that synchronize the data by showing 
 * the button a synchronize or not based on it's data
 * 
 * @author : Nurul Huda
 * @license : LGPLv3
 * @copyright : goblooge@gmail.com
 * @since : 09-Marc-2017
 * @version : 1.0.0
 * 
 * */
 
class TableSynchronous extends Table{
    
    private $is_synchronize_button;
    private $is_duplicate_button;
    private $is_loop_duplicate_button;
    
    
    public function __construct($header,$title="",$content=NULL,$action=true){
        parent::__construct($header,$title,$content,$action);
        $this->is_synchronize_button=true;
        $this->is_duplicate_button=true;
        $this->is_loop_duplicate_button=false;
    }
    
    /**
     * @brief used as all sync button used
     * @param boolean $anabled
     * @return  this;
     */
    public function setLoopDuplicateButtonEnable($enabled){
        $this->is_loop_duplicate_button=$enabled;
        return $this;
    }
    
    /**
     * @brief used as duplicate button used
     * @param boolean $duplicate
     * @return  this;
     */
    public function setDuplicateButton($duplicate){
        $this->is_duplicate_button=$duplicate;
        return $this;
    }
    
    
    
    /**
     * @brief used as sync button used
     * @param boolean $sync 
     * @return  this;
     */
    public function setSynchronizeButton($sync){
        $this->is_synchronize_button=$sync;
        return $this;
    }
    
    /**
     * @brief check is duplicate button used or not
     * @return  boolean is_duplicate;
     */
    public function isDuplicate(){
        return $this->is_duplicate_button;
    }
    
     /**
     * @brief check is synchronize button used or not
     * @return  boolean is_synchronize;
     */
    public function isSynchronize(){
        return $this->is_synchronize_button;
    }
    
    /**
     * @brief this same as Table->getContentButton($id);
     *          the only different is adding Synchronize Button by Default
     * @param int $id 
     * @return  ButtonGroup
     */
    public function getContentButton($id){        
        $btn_group=parent::getContentButton($id);
        if($this->isSynchronize() && $this->current_data['synch']!="1" && $this->model!=Table::$SELECT){
            $btn=new Button($this->name."_synch", "","Synch");
            $btn->setAction($this->action.".synch('".$id."')");
            $btn->setClass("btn-success");
            $btn->setAtribute("data-content='Re Synch' data-toggle='popover'");
            $btn->setIcon("fa fa-recycle");
            $btn->setIsButton(Button::$ICONIC);
            $btn_group->addElement($btn);
        }
        
        if($this->isDuplicate() && $this->current_data['duplicate']!="1" && $this->model!=Table::$SELECT){
            $btn=new Button($this->name."_duplicate", "","Duplicate");
            $btn->setAction($this->action.".duplicate('".$id."')");
            $btn->setClass("btn-success");
            $btn->setAtribute("data-content='Re Duplicate' data-toggle='popover'");
            $btn->setIcon("fa fa-cloud-upload");
            $btn->setIsButton(Button::$ICONIC);
            $btn_group->addElement($btn);
        }
        
        return $btn_group;
    }
    
    public function getHeaderGroupButton(){
        $grup=parent::getHeaderGroupButton();
        if($this->isDuplicate() && $this->is_loop_duplicate_button && $this->model!=Table::$SELECT){
            $btn=new Button($this->name."_duplicateloop", "","Duplicateloop");
            $btn->setAction($this->action.".duplicateloop()");
            $btn->setClass("btn-primary");
            $btn->setAtribute("data-content='Duplicate Loop' data-toggle='popover'");
            $btn->setIcon("fa fa-cloud");
            $btn->setIsButton(Button::$ICONIC);
            $grup->addElement($btn);
        }
        return $grup;
    }
    
}

 ?>