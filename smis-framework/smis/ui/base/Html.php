<?php

class HTML{
	
	protected $id;
	protected $class;
	protected $name;
	protected $content;
	protected $value;
	protected $atribute;
	protected $action;
	protected $element;
	protected $disable;
	
	
	
	public function __construct($id,$name,$value){
		$this->name=$name;
		$this->id=$id;
		$this->setValue($value);
		$this->atribute="";
		$this->action="";
		$this->element="";
		$this->disable=false;
	}
	
	public function getID(){
		return $this->id;
	}
	
	public function getAction(){
		return $this->action;
	}
	
	public function setAction($action){
		$this->action=$action;
		return $this;
	}
	
	public function addAtribute($attribute,$value=NULL){
		if($value!=NULL) $this->atribute.=$attribute.'="'.$value.'"';
		else $this->atribute.=" ".$attribute." ";
		return $this;
	}
	
	public function setAtribute($atribute){
		$this->atribute=$atribute;
		return $this;
	}
	
	public function getAtribute(){
		return $this->atribute;
	}
	
	public function getValue(){
		return $this->value;
	}
	public function setValue($value){
		$this->value=$value;
		return $this;
	}
	
	public function setName($name){
		$this->name=$name;
		return $this;
	} 
	
	public function setId($id){
		$this->id=$id;
		return $this;
	}
	
	public function addClass($class){
		$this->class.=" ".$class;
		return $this;
	}
	
	public function setClass($class){
		$this->class=$class;
		return $this;
	}
	
	public function setDisabled($disabled){
		$this->disable=$disabled;
		return $this;
	}
	
	public function getDisabled(){
		if($this->disable){
			return "disabled='disabled'";
		}
		return "";
	}
	
	public function isDisabled(){
		return $this->disable;
	}
	
	public function setElement($element){
		$this->element=$element;
		return $this;
	}
	
	public function getHtml(){
		return $this->value;
	}
	
}


?>