<?php

class Text extends HTML{	
	protected $empty;
	protected $typical;
	protected $other_content;
	protected $model;
	
	public static $TEXT					= "text";
	public static $MONEY				= "money";
	public static $UNPRECISION			= "unprecision";
	public static $INDONESIAN_DATE		= "idate";
	public static $INDONESIAN_DATETIME	= "idatetime";
	public static $INDONESIAN_TEXTDATE	= "itextdate";
	public static $DATE					= "date";
	public static $DATETIME				= "datetime";
	public static $TIME					= "time";
	public static $COLOR				= "color";
	public static $TYPEHEAD_STRICT		= "typehead-strict";
	public static $TYPEHEAD_FREE		= "typehead-free";
	public static $ARRAY_MODEL			= array("text","unprecision","money","date","time","datetime","idate","itextdate","idatetime","color","typehead-strict","typehead-free");
	
	public function __construct($id, $name, $value,$model="text"){
		parent::__construct($id, $name, $value);
		$this->empty			= 'y';
		$this->typical			= 'free';
		$this->other_content	= '';
		$this->model			= $model;
	}
	
	public function getHtml(){
		$this->convertModel();
		$content="<input dv=\"".htmlspecialchars($this->value, ENT_QUOTES)."\" ".$this->getDisabled()." empty='".$this->empty."' typical='".$this->typical."' type='text' onkeyup=\"".$this->action."\" ".$this->getAtribute()." class='".$this->class."' id=\"".$this->id."\" name='".$this->name."' value=\"".htmlspecialchars($this->value, ENT_QUOTES)."\" />";
		return $content.$this->other_content;		
	}
	
	public function setOtherContent($other){
		$other="<p id='".$this->id."_others' class='hide'>".$other."</a>";
		$this->other_content=$other;
	}
	
	public function setEmpty($empty){
		$this->empty=$empty;
	}
	
	public function setTypical($typical){
		$this->typical=$typical;
	}
	
	public function setModel($model){
		$this->model=$model;
	}
	
	public function setSlider($min=0,$max=360,$step=1){
		$this->addAtribute(" data-slider-min", $min);
		$this->addAtribute(" data-slider-max", $max);
		$this->addAtribute(" data-slider-step", $step);
		$this->addAtribute(" data-slider-value", $this->value);
	}
	
	public function convertModel(){
		$old_attribute=$this->atribute;
		if($this->model==self::$UNPRECISION){
			$this->setTypical("money");
			$this->setAtribute(" data-thousands=\".\" data-decimal=\",\" data-prefix=\"Rp. \"  data-precision=\"0\"  " );
		}else if($this->model==self::$MONEY){
			$this->setTypical("money");
			$this->setAtribute(" data-thousands=\".\" data-decimal=\",\" data-prefix=\"Rp. \"  data-precision=\"2\"  " );
		}else if($this->model==self::$TYPEHEAD_FREE){
			$this->addClass("typehead");
			$this->setAtribute("freeinput='y'");
			$this->setOtherContent($value);
		}else if($this->model==self::$TYPEHEAD_STRICT){
			$this->addClass("typehead");
			$this->setAtribute("freeinput='n'");
			$this->setOtherContent($value);
		}else if($this->model==self::$INDONESIAN_DATE){
			$this->setClass("mydate");
			$this->setAtribute("data-date-format='dd-mm-yyyy'");
		}else if($this->model==self::$DATE){
			$this->setClass("mydate");
			$this->setAtribute("data-date-format='yyyy-mm-dd'");
		}else if($this->model==self::$INDONESIAN_DATETIME){
			$this->setClass("mydatetime");
			$this->setAtribute("data-date-format='dd-mm-yyyy hh:ii'");
		}else if($this->model==self::$DATETIME){
			$this->setClass("mydatetime");
			$this->setAtribute("data-date-format='yyyy-mm-dd hh:ii'");
		}else if($this->model==self::$INDONESIAN_TEXTDATE){
			$this->setClass("itextdate");
			$this->setAtribute("data-date-format='dd-mm-yyyy'");
		}else if($this->model==self::$TIME){
			$this->setClass("mytime");
			$this->setAtribute("data-date-format='hh:ii'");
		}else if($this->model==self::$COLOR){
			$this->setClass("mycolor");
		}
		$this->addAtribute($old_attribute);
	}
	
}

?>