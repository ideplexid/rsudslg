<?php

class File extends HTML{
	
	protected $empty;
	protected $typical;
	
	public function __construct($id, $name, $value){
		parent::__construct($id, $name, $value);
		$this->empty='y';
		$this->typical='free';
	}
	
	public function getHtml(){
		$content="<input ".$this->getDisabled()." empty='".$this->empty."' typical='".$this->typical."' type='file' onkeyup=\"".$this->action."\" ".$this->getAtribute()." class='".$this->class."' id=\"".$this->id."\" name='".$this->name."' value=\"".$this->value."\"/>";
		return $content;		
	}
	
	public function setEmpty($empty){
		$this->empty=$empty;
	}
	
	public function setTypical($typical){
		$this->typical=$typical;
	}
	
}

?>