<?php
	
	class Hidden extends HTML{
		protected $empty;
		protected $typical;
		protected $model;
		public function setEmpty($empty){
			$this->empty=$empty;
		}
		
		public function setTypical($typical){
			$this->typical=$typical;
		}
		
		public function setModel($model){
			$this->model=$model;
		}
		public function __construct($id, $name, $value){
			parent::__construct($id, $name, $value);
		}
		
		/*public function getHtml(){
			$content="<input  ".$this->getDisabled()." type='hidden' id='".$this->id."' name='".$this->name."' value='".$this->value."' dv='".$this->value."' />";
			return $content;
		}*/
		
		public function getHtml(){
			//$this->convertModel();
			$content="<input dv=\"".htmlspecialchars($this->value, ENT_QUOTES)."\" ".$this->getDisabled()." empty='".$this->empty."' typical='".$this->typical."' type='hidden' onkeyup=\"".$this->action."\" ".$this->getAtribute()." class='".$this->class."' id=\"".$this->id."\" name='".$this->name."' value=\"".htmlspecialchars($this->value, ENT_QUOTES)."\" />";
			return $content;
		}
		
	}

?>