<?php

/**
 * this class was used as based 
 * class of button, it's common used in
 * smis framework to create button from a <button>
 * or an anchor tag <a>
 * 
 * @since       : 14 Januari 2014
 * @version     : 5.0.1
 * @license     : Apache V3
 * @copyright   : goblooge@gmail.com
 * @author      : Nurul Huda
 * 
 * */

class Button extends HTML{
	private $is_button;
	private $icon;
    private $href;
    private $popover;
	public static $BUTTON=0;
	public static $DROPDOWN=1;
	public static $ICONIC=2;
	public static $ICONIC_TEXT=3;
	
	public function __construct($id="", $name="", $value=""){
		parent::__construct($id, $name, $value);
		$this->is_button=self::$BUTTON;
        $this->href="#";
        $this->popover=null;
		return $this;
	}
    
    /**
     * @brief set the popover string
     * @param string $string 
     * @return  this
     */
    public function setPopover($string){
        $this->popover=$string;
        return $this;
    }
    
    /**
     * @brief set the link of the anchor
     * @param string $link 
     * @return  this
     */
    public function setHRef($link){
        $this->href=$link;
        return $this;
    }
	
    /**
     * @brief set what kind of this button
     *        it's only contains 4 flag
     *        BUTTON = 0, DROPDOWN = 1, ICONIC = 1, ICONIC_TEXT = 2
     * @param int $is_button 
     * @return  this
     */
	public function setIsButton($is_button){
		$this->is_button=$is_button;
		return $this;
	}
	
    /**
     * @brief set the icon of the button
     *          used font awesome and bootstrap default icon
     * @param string $icon 
     * @return  this
     */
	public function setIcon($icon){
		$this->icon=$icon;
		return $this;
	}
	
    /**
     * @brief get the string html of this
     *          button
     * @return  this
     */
	public function getHtml(){
		$content="";
        $popover="";
        if($this->popover!=null){
            $popover="data-content='".$this->popover."' data-toggle='popover' ";
        }
		if($this->is_button==self::$BUTTON){
			$content="<input ".$this->getDisabled()." type='button' $popover onclick=\" ".$this->action." \" ".$this->atribute." class=' btn ".$this->class." ' id='".$this->id."' name='".$this->name."' value='".$this->value."'/>";
		}else if($this->is_button==self::$DROPDOWN) {
			$this->icon=str_replace("icon-white", "", $this->icon);
			$content="<a ".$this->getDisabled()."  href='".$this->href."' $popover tabindex='-1' onclick=\" ".$this->action." \" ".$this->atribute." id='".$this->id."'  >  <i class='icon-black ".$this->icon."'></i> ".$this->value."</a>";
		}else if($this->is_button==self::$ICONIC){
			$content="<a ".$this->getDisabled()."  href='".$this->href."' $popover onclick=\" ".$this->action." \" ".$this->atribute." id='".$this->id."'  class='input btn ".$this->class." ' > <i class='".$this->icon."'></i>  </a>";
		}else{
			$content="<a ".$this->getDisabled()."  href='".$this->href."' $popover onclick=\" ".$this->action." \" ".$this->atribute." id='".$this->id."'  class='input btn ".$this->class." ' > <i class='".$this->icon."'> </i> ".$this->value."   </a>";
		}
		return $content;
	}
	
	public static $icon_glass="icon-glass";
	public static $icon_music="icon-music";
	public static $icon_search="icon-search";
	public static $icon_envelope="icon-envelope";
	public static $icon_heart="icon-heart";
	public static $icon_star="icon-star";
	public static $icon_star_empty="icon-star-empty";
	public static $icon_user="icon-user";
	public static $icon_film="icon-film";
	public static $icon_th_large="icon-th-large";
	public static $icon_th="icon-th";
	public static $icon_th_list="icon-th-list";
	public static $icon_ok="icon-ok";
	public static $icon_remove="icon-remove";
	public static $icon_zoom_in="icon-zoom-in";
	public static $icon_zoom_out="icon-zoom-out";
	public static $icon_off="icon-off";
	public static $icon_signal="icon-signal";
	public static $icon_cog="icon-cog";
	public static $icon_trash="icon-trash";
	public static $icon_home="icon-home";
	public static $icon_file="icon-file";
	public static $icon_time="icon-time";
	public static $icon_road="icon-road";
	public static $icon_download_alt="icon-download-alt";
	public static $icon_download="icon-download";
	public static $icon_upload="icon-upload";
	public static $icon_inbox="icon-inbox";
	public static $icon_play_circle="icon-play-circle";
	public static $icon_repeat="icon-repeat";
	public static $icon_refresh="icon-refresh";
	public static $icon_list_alt="icon-list-alt";
	public static $icon_lock="icon-lock";
	public static $icon_flag="icon-flag";
	public static $icon_headphones="icon-headphones";
	public static $icon_volume_off="icon-volume-off";
	public static $icon_volume_down="icon-volume-down";
	public static $icon_volume_up="icon-volume-up";
	public static $icon_qrcode="icon-qrcode";
	public static $icon_barcode="icon-barcode";
	public static $icon_tag="icon-tag";
	public static $icon_tags="icon-tags";
	public static $icon_book="icon-book";
	public static $icon_bookmark="icon-bookmark";
	public static $icon_print="icon-print";
	public static $icon_camera="icon-camera";
	public static $icon_font="icon-font";
	public static $icon_bold="icon-bold";
	public static $icon_italic="icon-italic";
	public static $icon_text_height="icon-text-height";
	public static $icon_text_width="icon-text-width";
	public static $icon_align_left="icon-align-left";
	public static $icon_align_center="icon-align-center";
	public static $icon_align_right="icon-align-right";
	public static $icon_align_justify="icon-align-justify";
	public static $icon_list="icon-list";
	public static $icon_indent_left="icon-indent-left";
	public static $icon_indent_right="icon-indent-right";
	public static $icon_facetime_video="icon-facetime-video";
	public static $icon_picture="icon-picture";
	public static $icon_pencil="icon-pencil";
	public static $icon_map_marker="icon-map-marker";
	public static $icon_adjust="icon-adjust";
	public static $icon_tint="icon-tint";
	public static $icon_edit="icon-edit";
	public static $icon_share="icon-share";
	public static $icon_check="icon-check";
	public static $icon_move="icon-move";
	public static $icon_step_backward="icon-step-backward";
	public static $icon_fast_backward="icon-fast-backward";
	public static $icon_backward="icon-backward";
	public static $icon_play="icon-play";
	public static $icon_pause="icon-pause";
	public static $icon_stop="icon-stop";
	public static $icon_forward="icon-forward";
	public static $icon_fast_forward="icon-fast-forward";
	public static $icon_step_forward="icon-step-forward";
	public static $icon_eject="icon-eject";
	public static $icon_chevron_left="icon-chevron-left";
	public static $icon_chevron_right="icon-chevron-right";
	public static $icon_plus_sign="icon-plus-sign";
	public static $icon_minus_sign="icon-minus-sign";
	public static $icon_remove_sign="icon-remove-sign";
	public static $icon_ok_sign="icon-ok-sign";
	public static $icon_question_sign="icon-question-sign";
	public static $icon_info_sign="icon-info-sign";
	public static $icon_screenshot="icon-screenshot";
	public static $icon_remove_circle="icon-remove-circle";
	public static $icon_ok_circle="icon-ok-circle";
	public static $icon_ban_circle="icon-ban-circle";
	public static $icon_arrow_left="icon-arrow-left";
	public static $icon_arrow_right="icon-arrow-right";
	public static $icon_arrow_up="icon-arrow-up";
	public static $icon_arrow_down="icon-arrow-down";
	public static $icon_share_alt="icon-share-alt";
	public static $icon_resize_full="icon-resize-full";
	public static $icon_resize_small="icon-resize-small";
	public static $icon_plus="icon-plus";
	public static $icon_minus="icon-minus";
	public static $icon_asterisk="icon-asterisk";
	public static $icon_exclamation_sign="icon-exclamation-sign";
	public static $icon_gift="icon-gift";
	public static $icon_leaf="icon-leaf";
	public static $icon_fire="icon-fire";
	public static $icon_eye_open="icon-eye-open";
	public static $icon_eye_close="icon-eye-close";
	public static $icon_warning_sign="icon-warning-sign";
	public static $icon_plane="icon-plane";
	public static $icon_calendar="icon-calendar";
	public static $icon_random="icon-random";
	public static $icon_comment="icon-comment";
	public static $icon_magnet="icon-magnet";
	public static $icon_chevron_up="icon-chevron-up";
	public static $icon_chevron_down="icon-chevron-down";
	public static $icon_retweet="icon-retweet";
	public static $icon_shopping_cart="icon-shopping-cart";
	public static $icon_folder_close="icon-folder-close";
	public static $icon_folder_open="icon-folder-open";
	public static $icon_resize_vertical="icon-resize-vertical";
	public static $icon_resize_horizontal="icon-resize-horizontal";
	public static $icon_hdd="icon-hdd";
	public static $icon_bullhorn="icon-bullhorn";
	public static $icon_bell="icon-bell";
	public static $icon_certificate="icon-certificate";
	public static $icon_thumbs_up="icon-thumbs-up";
	public static $icon_thumbs_down="icon-thumbs-down";
	public static $icon_hand_right="icon-hand-right";
	public static $icon_hand_left="icon-hand-left";
	public static $icon_hand_up="icon-hand-up";
	public static $icon_hand_down="icon-hand-down";
	public static $icon_circle_arrow_right="icon-circle-arrow-right";
	public static $icon_circle_arrow_left="icon-circle-arrow-left";
	public static $icon_circle_arrow_up="icon-circle-arrow-up";
	public static $icon_circle_arrow_down="icon-circle-arrow-down";
	public static $icon_globe="icon-globe";
	public static $icon_wrench="icon-wrench";
	public static $icon_tasks="icon-tasks";
	public static $icon_filter="icon-filter";
	public static $icon_briefcase="icon-briefcase";
	public static $icon_fullscreen="icon-fullscreen";
}

?>