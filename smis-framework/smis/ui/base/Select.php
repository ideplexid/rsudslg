<?php

class Select extends HTML{
	
	private $selected;
	private $selected_type;
	private $is_multiple;
	protected $empty;
	
	private $dv;
	
	public function __construct($id, $name, $value){
		$this->selected=0;
		$this->is_multiple=false;
		$this->selected_type="value";
		parent::__construct($id, $name, $value);
	}
	
	public function setEmpty($empty){
		$this->empty=$empty;
	}
	
	public function setMultiple($multiple){
		$this->is_multiple=$multiple;
	}
	
	public function getValue(){
		return $this->value;
	}
	
	public function setSelected($selected,$selected_type="value"){
		$this->selected=$selected;
		$this->selected_type=$selected_type;
	}
	
	public function getSelected($selected){
		return $this->selected;
	}
	
	public function getHtml(){
		$multi=$this->is_multiple?" multiple ":"";
		$content="<select ".$this->getDisabled()." ".$this->getAtribute()." ".$multi." empty='".$this->empty."' type='select'  dv=\"".$this->dv."\" onchange=\"".$this->action."\" ".$this->element." class='".$this->class."' id='".$this->id."' name='".$this->name."'>".$this->value."</select>";
		return $content;
	}
	
	public function is_selected($selected,$number,$value){
		if($this->selected_type=="value") return $selected==$value;
		else return $selected==$number;		
	}
	
	/**
	 * the var should be array 2D that contains name and value
	 * (non-PHPdoc)
	 * @see HTML::setValue()
	 */
	public function setValue($var){
		if(!is_array($var)) return;
		$val="";
		$number=0;
		$curgrup=NULL;
		foreach($var as $v){
			$selected="";
			if(isset($v['default']) && $v['default']=="1" && $this->selected==""){
				$this->dv=$v['value'];
				$this->selected=$v['value'];
			}
			if($this->is_selected($this->selected, $number, $v['value'])) 
				$selected="selected='selected'";
			
			if(isset($v['grup']) && $v['grup']!=NULL && $v['grup']!=$curgrup){
				if($curgrup!=NULL){
					$val.="</optgroup>";
				}
				$val.="<optgroup label='".$v['grup']."'>";
				$curgrup=$v['grup'];
			}
			$val.="<option data-type='".$this->selected_type."' data-if=\"".($this->is_selected($this->selected, $number, $v['value'])?1:0)."\" data-selected='$this->selected' data-number='$number' ".$selected." value='".$v['value']."' > ".$v['name']."</option>";
			$number++;
		}
		if($curgrup!=NULL){
			$val.="</optgroup>";
		}
		$this->value=$val;
	}
	
	
	
}

?>