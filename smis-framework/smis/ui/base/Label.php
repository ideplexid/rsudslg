<?php

class Label extends HTML{
	public function __construct($id, $name, $value){
		parent::__construct($id, $name, $value);
	}
	
	public function getHtml($is_button=true){
		$content="<label class=\"".$this->class."\" id=\"".$this->id."\">".$this->value."</label>";
		return $content;
	}
}

?>