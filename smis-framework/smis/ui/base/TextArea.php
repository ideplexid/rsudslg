<?php

class TextArea extends Text{
	private $line;
	private $mode;
	public static $TEXTAREA="textarea";
	public static $DIV="divarea";
	
	public function __construct($id, $name, $value){
		parent::__construct($id, $name, $value);
		$this->line=8;
		$this->mode=self::$TEXTAREA;
	}
	
	public function setMode($mode){
		$this->mode=$mode;
	}
	
	public function setLine($line){
		$this->line=$line;
	}
	
	public function getHtml(){
		$content="";
		if($this->mode==self::$TEXTAREA)
			$content="<textarea dv=\"".htmlspecialchars($this->value, ENT_QUOTES)."\" ".$this->getDisabled()."  empty='".$this->empty."' typical='".$this->typical."' onchange=\" ".$this->action." \" ".$this->atribute." class=' ".$this->class." ' rows='".$this->line."' id='".$this->id."' name='".$this->name."' >".$this->value."</textarea>";
		else 
			$content="<div dv=\"".htmlspecialchars($this->value, ENT_QUOTES)."\" contenteditable='true' ".$this->getDisabled()."  empty='".$this->empty."' typical='".$this->typical."' onchange=\" ".$this->action." \" ".$this->atribute." class=' ".$this->class." ' rows='".$this->line."' id='".$this->id."' name='".$this->name."' >".$this->value."</div>";
		return $content;
	}
	
}

?>