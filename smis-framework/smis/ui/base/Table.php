<?php

/**
 * this class was used as based 
 * table in smis system 
 * the table will be generated 
 * and it's contains will be create 
 * as based of smis table
 * 
 * @since       : 14 Januari 2014
 * @version     : 5.0.1
 * @license     : Apache V3
 * @copyright   : goblooge@gmail.com
 * @author      : Nurul Huda
 * 
 * */
 
class Table{	
	protected $header;
	protected $header_before;
	protected $header_after;
	protected $footer_before;
	protected $footer_after;
	protected $body_before;
	protected $body_after;
	protected $add_button_enable;
	protected $print_button_enable;
	protected $edit_button_enable;
    protected $detail_button_enable;
	protected $print_element_button_enable;
	protected $print_element_preview_button_enable;
	protected $reload_button_enabled;	
	protected $excel_element_button_enable;
	protected $excel_button_enable;	
	protected $pdf_element_button_enable;
	protected $pdf_button_enable;
	protected $clipboard_button_enable;	
	protected $help_button_enable;
	protected $help_button_page;
	protected $help_button_action;	
	protected $help_button_slug;
	protected $help_button_name;
	protected $help_button_implement;
	protected $delete_button_enable;
	protected $enabled_header_button_on_select;
	protected $header_button;
	protected $footer_button;
	protected $enabled_all_content_button;	
	protected $caption;
	protected $content;
	protected $is_action;
	protected $action;
	protected $name;
	protected $class;	
	protected $modal_column;
	protected $modal_type;
	protected $modal_name;
	protected $modal_value;
	protected $modal_empty;
	protected $modal_typical;
	protected $modal_disable;
	protected $modal_option;
	protected $modal_autofocus;
	protected $modal_next_enter;
	protected $model;
	protected $control_position;
	protected $is_activate_responsive;
	protected $width_activate_responsive;
	protected $current_data;	
	protected $content_button_max;
	protected $content_button_default;	
	protected $is_modal_save_button_enable;
	protected $is_fix_header;	
	protected $footer_visibility;
	protected $footer_control_visibility;
	protected $header_visibility;	
	protected $content_button;
	public static $EDIT				= 'edit';
	public static $SELECT			= 'select';
	public static $BOTH				= 'both';
	public static $NONE				= 'none';	
	public static $CONTROL_TOP		= true;
	public static $CONTROL_BOTTOM	= false;
	
	
	public function __construct($header,$title="",$content=NULL,$action=true){
		$this->setHeader($header);
		$this->header_before						= array();
		$this->header_after							= array();
		$this->footer_before						= array();
		$this->footer_after							= array();
		$this->body_before							= array();
		$this->body_after							= array();
		$this->caption								= $title;
		$this->content								= $content;
		$this->is_action							= $action;
		$this->model								= self::$EDIT;
		$this->footer_visibility					= true;
		$this->header_visibility					= true;
		$this->class								= "";
		$this->add_button_enable					= true;
		$this->print_button_enable					= true;
		$this->delete_button_enable					= true;
		$this->edit_button_enable					= true;
		$this->reload_button_enabled				= true;
		$this->control_position						= self::$CONTROL_BOTTOM;
		$this->enabled_header_button_on_select		= false;
		$this->header_button						= array();
		$this->footer_button						= array();
		$this->enabled_all_content_button			= false;
		$this->print_element_button_enable			= false;
		$this->print_element_preview_button_enable	= false;
		$this->content_button						= array();
		$this->content_button_max					= 4;
		$this->content_button_default				= "Action";
		$this->modal_autofocus						= array();
		$this->modal_next_enter						= array();
		$this->is_modal_save_button_enable			= true;
		$this->is_activate_responsive				= false;
		$this->width_activate_responsive			= 768;
		$this->is_fix_header						= false;
		$this->footer_control_visibility			= true;	
		$this->help_button_enable					= false;	
		$this->help_button_page						= "";
		$this->help_button_action					= "";
		$this->help_button_slug						= "";
		$this->help_button_name						= "";
		$this->help_button_implement				= "";
		$this->excel_button_enable					= false;
		$this->excel_element_button_enable			= false;
		$this->pdf_button_enable					= false;
		$this->pdf_element_button_enable			= false;
		$this->clipboard_button_enable				= false;
        $this->detail_button_enable					= false;
	}
    
    /**
	 * enabling detail button header of content
	 * @param bool $enabled
	 */
    public function setDetailButtonEnable(bool $enabled){
        $this->detail_button_enable	= $enabled;
		return $this;
    }
	
	/**
	 * enabling button excell in header of content
	 * @param bool $enabled
	 */
	public function setExcelElementButtonEnable(bool $enabled){
		$this->excel_element_button_enable = $enabled;
		return $this;
	}
	
	/**
	 * enabled disabled excell in each element
	 * @param bool $enabled
	 */
	public function setExcelButtonEnable(bool $enabled){
		$this->excel_button_enable		= $enabled;
		return $this;
	}

	/**
	 * enabling button pdf in header of content
	 * @param bool $enabled
	 */
	public function setPDFElementButtonEnable(bool $enabled){
		$this->pdf_element_button_enable = $enabled;
		return $this;
	}
	
	/**
	 * enabled disabled pdf in each element
	 * @param bool $enabled
	 */
	public function setPDFButtonEnable(bool $enabled){
		$this->pdf_button_enable	= $enabled;
		return $this;
	}
	
	/**
	 * activated responsive table
	 * @param boolean $activate, true if activate
	 * @param int $width, base width that activated the table
	 */
	public function setActivatedResponsiveTable($activate,$width){
		$this->is_activate_responsive		= $activate;
		$this->width_activate_responsive	= $width;
		return $this;
	}
	
	/**
	 * set the header fix when it's scroll
	 * @param boolean $fix;
	 */
	public function setFixHeaderOnScroll($fix){
		$this->is_fix_header = $fix;
	}
	
	
	/**
	 * for enable or disabled Save Button in Modal
	 * @param boolean $enable
	 */
	public function setModalSaveButtonEnable($enable){
		$this->is_modal_save_button_enable = $enable;
		return $this;
	}
	
	public function setTitle($title){
		$this->caption		= $title;
		return $this;
	}
	
	public function addContentButton($action,$button){
		$this->content_button[$action] = $button;
		return $this;
	}
	
	public function setClass($class){
		$this->class				= $class;
		return $this;
	}

	public function addClass($class){
		$this->class			   .= " ". $class;
		return $this;
	}
	
	public function setControlPosition($position){
		$this->control_position		= $position;
		return $this;
	}
	
	public function addHeader($place,$header){
		if($place=="before"){
			$this->header_before[]	= $header;
		}else{
			$this->header_after[]	= $header;
		}
		return $this;
	}
	
	public function addBody($place,$body){
		if($place=="before"){
			$this->body_before[]	= $body;
		}else{
			$this->body_after[]		= $body;
		}
		return $this;
	}
	
	public function addFooter($place,$footer){
		if($place=="before"){
			$this->footer_before[] = $footer;
		}else{
			$this->footer_after[]  = $footer;
		}
		return $this;
	}
	
	public function setHeader($header){
		$this->header		= $header;
		return $this;
	}
	
	public function addHeaderElement($name){
		if($this->header==null){
			$this->header	= array();
		}
		$this->header[]		= $name;
		return $this;
	}
	
	public function setModel($model){
		$this->model		= $model;
		return $this;
	}
	
	public function setName($name){
		$this->name			= $name;
		$this->action		= $name;
		return $this;
	}
	
	public function setActionName($action){
		$this->action	= $action;
		return $this;
	}
	
	
	public function setModal($column=null,$type=null,$name=null,$value=null,$empty=null,$typical=null,$disable=null,$option=null,$autofocus=null,$next_enter=NULL){
		$this->modal_column		= $column;
		$this->modal_type		= $type;
		$this->modal_value		= $value;
		$this->modal_name		= $name;
		$this->modal_empty		= $empty;
		$this->modal_typical	= $typical;
		$this->modal_disable	= $disable;
		$this->modal_option		= $option;
		$this->modal_autofocus	= $autofocus;
		$this->modal_next_enter	= $next_enter;
		return $this;
	}
	/**
	 * this function to reset the 
	 * content of modal that already save
	 * @return Table
	 */
	public function clearContent(){
		$this->modal_column		= array();
		$this->modal_type		= array();
		$this->modal_value		= array();
		$this->modal_name		= array();
		$this->modal_empty		= array();
		$this->modal_typical	= array();
		$this->modal_disable	= array();
		$this->modal_option		= array();
		$this->modal_autofocus	= array();
		$this->modal_next_enter	= array();
		return $this;
	}
	
	public function addModal($column,$type,$name,$default_value,$empty='y',$typical=null,$disable=false,$option=null,$autofocus=false,$next_enter=NULL){
		$this->modal_column[]		= $column;
		$this->modal_type[]			= $type;
		$this->modal_value[]		= $default_value;
		$this->modal_name[]			= $name;
		$this->modal_empty[]		= $empty;
		$this->modal_typical[]		= $typical;
		$this->modal_disable[]		= $disable;
		$this->modal_option[]		= $option;
		$this->modal_autofocus[]	= $autofocus;	
		$this->modal_next_enter[]	= $next_enter;
		return $this;
	}
	
	
	public function getModal(){
		$modal = new Modal($this->name."_add_form", "smis_form_container",$this->name);
		$this->setModalComponent($modal);
		if($this->is_modal_save_button_enable){
			$button = new Button($this->name."_save", "", "");
			$button ->setClass("btn-primary")
					->setIsButton(Button::$ICONIC)
					->setIcon("fa fa-save")
					->setAction($this->action.".save()");
			$modal	->addFooter($button);
		}
		return $modal;
	}
	
	protected function setModalComponent(Modal $modal){
		$number		 		= 0;
		if($this->modal_column==null){
			return;
		}
		foreach ($this->modal_column as $column){
			$type	 		= $this->modal_type[$number];
			$value	 		= $this->modal_value[$number];
			$name	 		= $this->modal_name[$number];
			$tipical 		= null;
			if($this->modal_typical!=null){
				$tipical	= $this->modal_typical[$number];
			}
			$empty			= null;
			if($this->modal_empty!=null){
				$empty		= $this->modal_empty[$number];
			}
			$disable		= false;
			if($this->modal_disable!=null){
				$disable	= $this->modal_disable[$number];
			}
			$option			= false;
			if($this->modal_option!=null){
				$option		= $this->modal_option[$number];
			}			
			$autofocus		= false;
			if($this->modal_autofocus!=null){
				$autofocus	= $this->modal_autofocus[$number];
			}
			$next_enter		= NULL;
			if($this->modal_next_enter!=null){
				$next_enter	= $this->modal_next_enter[$number];
			}
			$modal->addModalElement($type, $value, $name, $column, $tipical, $empty,$disable,$option,$autofocus,$next_enter);
			$number++;
		}
		return $this;
	}
	
	/**
	 * $pure mean the raw data exactly come from database
	 * $formatted is data that already got from adapter
	 * @param unknown $pure
	 * @param unknown $formatted
	 * @return string
	 */
	public function getPrintedElement($pure,$formatted){
		return "Override getPrintedElement(pure,formatted) Methode";
	}
	
	/**
	 * $pure mean the raw data exactly come from database
	 * $formatted is data that already got from adapter
	 * @param unknown $pure
	 * @param unknown $formatted
	 * @return string
	 */
	public function getPrinted($pure,$formatted,$slug){
		return "Override getPrinted(pure,formatted,slug) Methode";
	}
	
	/**
	 * true or false should print the action
	 * @param boolean $action
	 */
	public function setAction($action){
		$this->is_action				= $action;
		return $this;
	}
	
	public function setActionEnable($action){
		$this->is_action				= $action;
		return $this;
	}
	
	public function setContent($content){
		$this->content					= $content;
		return $this;
	}
	
	public function getHeader(){
		$content="<thead>";
			foreach($this->header_before as $h){
				$content.=$h;
			}
			if($this->header_visibility){
				$content.="<tr  class=' inverse ".$this->name."_header_normal'>";
					foreach($this->header as $h){
						$content.="<th>".$h."</th>";
					}
					if($this->is_action){
						$content.="<th class='noprint'>".$this->getHeaderButton()."</th>";		
					}	
				$content.="</tr>";
			}
			foreach($this->header_after as $h){
				$content.=$h;
			}
		$content.="</thead>";
		return $content;
	}
	
	
	public function getHtml(){
		$table_content  = "";
		$table_content .= "<div class='".$this->class."' id='print_table_".$this->name."'>";
		$table_content .= "<div id='fix_table_".$this->name."' class='fixedheader noprint fixed'></div>";
		$table_content .= "<table data-fix-header='".($this->is_fix_header?"y":"n")."' class='table table-bordered table-hover table-striped table-condensed ' id='table_".$this->name."' >";
			$table_content .= "<caption>".$this->caption."</caption>";			
			$header			= $this->getHeader();
			$body			= $this->getBody();
			$footer			= "";
			if($this->footer_visibility){
				$footer		= $this->getFooter();
			}
			if(!$this->control_position) {
				$table_content .= $header.$body.$footer;
			}else{
				$table_content .= $footer.$header.$body;
			}
		$table_content .= "</table>";		
		$table_content .= "</div>";
		$content		= $table_content;
		
		if($this->model==self::$EDIT){
				$content = "<div class='smis-table-container'>";
					$content .= $table_content;
				$content .= "</div>";
		}
		if($this->is_activate_responsive) 
			$content   .= $this->getCSSActivated();
		return $content;
	}
	
	public function getBody(){
		$content	= "<tbody id='".$this->name."_list'>";
			$content	.= $this->getBodyContent();
		$content	.= "</tbody>";
		return $content;
	}
	
	public function getBodyContent(){
		$content		= "";
		foreach($this->body_before as $h){
			$content   .= $h;
		}
		if($this->content!=NULL){
			foreach($this->content as $d){
				$content.="<tr>";
					foreach($this->header as $h){
						$content.="<td>".(isset($d[$h])?$d[$h]:"")."</td>";
					}
					if($this->is_action){
                        $ctx="";
                        if(isset($d['id'])){
                            $this->current_data=$d;
                            $ctx=$this->getContentButton($d['id'])->getHtml();
                        }
						$content.="<td class='noprint'>".$ctx."</td>";
					}
				$content.="</tr>";
			}
		}
		foreach($this->body_after as $h){
			$content.=$h;
		}
		return $content;
	}
	
	/**this used to set how many content button 
	 * would become select if the button was too much
	 * @param int $max is maximum integer  
	 * @param $default is the text that would appear as button
	 * return $this;
	 */
	public function setMaxContentButton($max,$default){
		$this->content_button_max		= $max;
		$this->content_button_default	= $default;
		return $this;
	}

	public function getContentButton($id){
		$bgroup	= new ButtonGroup('noprint');
		if($id=="" || $id=="0") {
			return $bgroup;
		}
		$bgroup->setMax($this->content_button_max, $this->content_button_default);
		if($this->model==self::$EDIT || $this->model==self::$BOTH || $this->enabled_all_content_button){
			if($this->edit_button_enable){
				$edit   = new Button($this->name."_edit", "","Edit");
				$edit   ->setAction($this->action.".edit('".$id."')")
					    ->setClass("btn-warning")
					    ->setAtribute("data-content='Edit' data-toggle='popover'")
					    ->setIcon("fa fa-pencil")
					    ->setIsButton(Button::$ICONIC);
				$bgroup ->addElement($edit);
			}
			
			if($this->delete_button_enable){
				$delete	 = new Button($this->name."_del", "","Delete");
				$delete	 ->setAction($this->action.".del('".$id."')")
						 ->setClass("btn-danger")
						 ->setAtribute("data-content='Delete' data-toggle='popover'")
						 ->setIcon("fa fa-trash")
						 ->setIsButton(Button::$ICONIC);
				$bgroup ->addElement($delete);
			}			
			if($this->print_element_button_enable) {
				$print	 = new Button($this->name."_printelement", "","Print");
				$print	 ->setAction($this->action.".printelement('".$id."')")
						 ->setClass("btn-inverse")
						 ->setAtribute("data-content='Print' data-toggle='popover'")
						 ->setIcon("fa fa-print")
						 ->setIsButton(Button::$ICONIC);
				$bgroup	 ->addElement($print);
			}			
			if($this->print_element_preview_button_enable) {
				$preview  = new Button($this->name."_printpreviewelement", "","Print");
				$preview  ->setAction($this->action.".print_preview('".$id."')")
						  ->setClass("btn-info")
						  ->setAtribute("data-content='Print Preview' data-toggle='popover'")
						  ->setIcon("fa fa-print")
						  ->setIsButton(Button::$ICONIC);
				$bgroup	  ->addElement($preview);
			}			
			if($this->excel_element_button_enable) {
				$excel	  = new Button($this->name."_excel_element", "","Print");
				$excel	  ->setAction($this->action.".excel_element('".$id."')")
						  ->setClass("btn-succes")
						  ->setAtribute("data-content='Excel' data-toggle='popover'")
						  ->setIcon("fa fa-file-excel-o")
						  ->setIsButton(Button::$ICONIC);
				$bgroup	  ->addElement($excel);
			}
			if($this->pdf_element_button_enable) {
				$pdf	  = new Button($this->name."pdf_element", "","Print");
				$pdf	  ->setAction($this->action.".pdf_element('".$id."')")
						  ->setClass("btn")
						  ->setAtribute("data-content='Excel' data-toggle='popover'")
						  ->setIcon("fa fa-file-pdf-o")
						  ->setIsButton(Button::$ICONIC);
				$bgroup	  ->addElement($pdf);
			}            
            if($this->detail_button_enable){
				$detail	  = new Button($this->name."_detail", "","Detail");
				$detail	  ->setAction($this->action.".detail('".$id."')")
						  ->setClass("btn-info")
						  ->setAtribute("data-content='Detail' data-toggle='popover'")
						  ->setIcon("fa fa-eye")
						  ->setIsButton(Button::$ICONIC);
				$bgroup	  ->addElement($detail);
			}			
			foreach($this->content_button as  $slug=>$btn){
				$btn	  ->setAction($this->action.".".$slug."('".$id."')")
						  ->setId($this->name."_".$slug);
				$bgroup   ->addElement($btn);
			}
		}
		
		if($this->model==self::$SELECT || $this->model==self::$BOTH || $this->enabled_all_content_button){
			$btn	= new Button($this->name."_select", "","Select");
			$btn	->setAction($this->action.".select('".$id."')")
				   	->setClass("btn-info")
				   	->setAtribute("data-content='Pilih' data-toggle='popover'")
				   	->setIcon("fa fa-check")
					->setIsButton(Button::$ICONIC);
			$bgroup	->addElement($btn);
		}
		return $bgroup;
	}
	
	public function enabledAllContentButton($enable){
		$this->enabled_all_content_button	= $enable;
		return $this;
	}
	
	public function setAddButtonEnable($enable){
		$this->add_button_enable			= $enable;
		return $this;
	}
	
	public function setEditButtonEnable($enable){
		$this->edit_button_enable			= $enable;
		return $this;
	}
	
	public function setReloadButtonEnable($enable){
		$this->reload_button_enabled		= $enable;
		return $this;
	}
	
	public function setDelButtonEnable($enable){
		$this->delete_button_enable			= $enable;
		return $this;
	}
	
	public function setClipboardButtonEnable($enable){
		$this->clipboard_button_enable		= $enable;
		return $this;
	}
	
	public function setPrintButtonEnable($enable){
		$this->print_button_enable			= $enable;
		return $this;
	}
	
	public function setPrintElementButtonEnable($enable){
		$this->print_element_button_enable	= $enable;
		return $this;
	}
	
	public function setPrintPreviewElementButtonEnable($enable){
		$this->print_element_preview_button_enable	= $enable;
		return $this;
	}
	
	public function setEnableHeaderButtonOnSelect($enable){
		$this->enabled_header_button_on_select		= $enable;
		return $this;
	}
	
	public function setHelpButtonEnabled($enabled,$page,$action=null,$slug="",$name="",$implement=""){
		$this->help_button_slug			= $slug;
		$this->help_button_name			= $name;
		$this->help_button_implement	= $implement;
		$this->help_button_enable		= $enabled;
		$this->help_button_page			= $page;
		$this->help_button_action		= $action==null?$this->action:$action;
		return $this;
	}
	
	public function addHeaderButton($btn){
		$this->header_button[]			= $btn;
		return $this;
	}
	
	public function addFooterButton($btn){
		$this->footer_button[]			= $btn;
		return $this;
	}
    
    protected function getHeaderGroupButton(){
        if($this->model==self::$EDIT || $this->model==self::$BOTH || $this->enabled_header_button_on_select){
			$bgroup 	= new ButtonGroup('noprint');
			if($this->add_button_enable) {
				$add 	= new Button($this->name."_add", "","Add");
				$add 	->setAction($this->action.".show_add_form()")
					 	->setClass("btn-primary")
					 	->setAtribute("data-content='Add New Item' data-toggle='popover'")
					 	->setIcon("fa fa-plus")
					 	->setIsButton(Button::$ICONIC);
				$bgroup ->addElement($add);
			}			
			if($this->print_button_enable) {
				$print	= new Button($this->name."_print", "","Print");
				$print	->setAction($this->action.".print()")
						->setClass("btn-primary")
						->setAtribute("data-content='Print' data-toggle='popover'")
						->setIcon("fa fa-print")
						->setIsButton(Button::$ICONIC);
				$bgroup ->addElement($print);
			}			
			if($this->reload_button_enabled) {
				$reload	= new Button($this->name."_reload", "","Reload");
				$reload	->setAction($this->action.".view()")
						->setClass("btn-primary")
						->setAtribute("data-content='Reload' data-toggle='popover'")
						->setIcon("fa fa-refresh")
						->setIsButton(Button::$ICONIC);
				$bgroup ->addElement($reload);
			}			
			if($this->help_button_enable) {
				$help	= new Button($this->name."_help", "","Help");
				$help	->setAction("helptable('".$this->help_button_page."','".$this->help_button_action."','".$this->help_button_slug."','".$this->help_button_name."','".$this->help_button_implement."')")
						->setClass("btn-primary")
						->setAtribute("data-content='Help' data-toggle='popover'")
						->setIcon("fa fa-question-circle")
						->setIsButton(Button::$ICONIC);
				$bgroup	->addElement($help);
			}			
			if($this->excel_button_enable) {
				$excel	= new Button($this->name."_excel", "","Excel");
				$excel	->setAction($this->action.".excel()")
						->setClass("btn-primary")
						->setAtribute("data-content='Excel' data-toggle='popover'")
						->setIcon("fa fa-file-excel-o")
						->setIsButton(Button::$ICONIC);
				$bgroup	->addElement($excel);
			}
			if($this->pdf_button_enable) {
				$pdf	= new Button($this->name."_pdf", "","PDF");
				$pdf	->setAction($this->action.".pdf()")
						->setClass("btn")
						->setAtribute("data-content='PDF' data-toggle='popover'")
						->setIcon("fa fa-file-pdf-o")
						->setIsButton(Button::$ICONIC);
				$bgroup	->addElement($pdf);
			}			
			if($this->clipboard_button_enable) {
				$clip	= new Button($this->name."_clipboard", "","Clipboard");
				$clip	->setAction($this->action.".clipboard()")
						->setClass("btn-inverse")
						->setAtribute("data-content='Clipboard' data-toggle='popover'")
						->setIcon("fa fa-clipboard")
						->setIsButton(Button::$ICONIC);
				$bgroup	->addElement($clip);
			}			
			foreach ($this->header_button as $btn){
				$bgroup	->addElement($btn);
			}
			return $bgroup;
		}
		return null;
    }
	
	public function getHeaderButton(){
		$btngrup=$this->getHeaderGroupButton();
        if($btngrup!=null){
           return $btngrup->getHtml();
        }
		return "";		
	}
	
	public function setFooterVisible($visible){
		$this->footer_visibility=$visible;
		return $this;
	}
	
	public function setFooterControlVisible($visible){
		$this->footer_control_visibility=$visible;
		return $this;
	}
	
	public function setHeaderVisible($visible){
		$this->header_visibility=$visible;
		return $this;
	}
	
	public function getFooter(){
		$foot="";
		foreach($this->footer_before as $h){
			$foot.=$h;
		}
		if($this->footer_control_visibility){
			if($this->model==self::$EDIT || $this->model==self::$BOTH){
				$foot.=$this->getFooterReguler();
			}else if($this->model==self::$SELECT){
				$foot.=$this->getFooterMini();
			}
		}
		foreach($this->footer_after as $h){
			$foot.=$h;
		}
		return $foot;
	}
	
	/**
	 * this is used for mini footer that needed for select function
	 * if we used input for selection chooser
	 * there will be 1 single column but in three row
	 * first row contain search box and button,
	 * second row contain pagination
	 * and the third contain selector for number 'row per page' 
	 */
	private function getFooterMini(){
		$total_column	= count($this->header);
		$foot			= $this->control_position?"thead":"tfoot";		
		if($this->is_action){
			$total_column++;
		}
		$content="<$foot class='noprint'>";
			$content.="<tr class='inverse'>";
				$content.="<td><label><strong>Pencarian</strong></label></td>";
				$content.="<td colspan='".$total_column."' >";
					$content.="<span class='input-append'>";
						$content.=$this->getSearch();
					$content.="</span>";					
				$content.="</td>";
			$content.="</tr>";
			$content.="<tr class='inverse'>";
				$content.="<td><label><strong>Halaman </strong></label></td>";
				$content.="<td colspan='".$total_column."' class='pagination-left' id='".$this->name."_pagination'>";
					$content.=$this->getPagination(0, 3, 10)->getHtml();
				$content.="</td>";
			$content.="</tr>";
			$content.="<tr class='inverse'>";
				$content.="<td><label><strong>Nomor</strong></label></td>";
				$content.="<td colspan='".$total_column."' >";
					$content.=$this->getNumber();
				$content.="</td>";
			$content.="</tr>";
		$content.="</$foot>";
		return $content;
	}
	
	/**
	 * this is reguler footer for widescreen
	 * or default table in system. it's will have 3 cell in 1 row
	 * first cell contain search box and button,
	 * second cell contain pagination
	 * and the third contain selector for number 'row per page' 
	 */
	private function getFooterReguler(){
		$foot			= $this->control_position?"thead":"tfoot";
		$total_column	= count($this->header);
		if($this->is_action){
			$total_column++;
		}
		$colspan1	= floor($total_column/3);
		$colspan2	= $total_column-2*$colspan1;
		$content 	= "<$foot class='noprint'>";
			$content 	.= "<tr class='inverse'>";
				$content 	.= "<td colspan='".$colspan1."'>";
					$content 	.= "<span class='input-append'>".$this->getSearch()."</span>";
				$content 	.= "</td>";
				$content 	.= "<td colspan='".$colspan2."'  class='pagination-center' id='".$this->name."_pagination'>";
					$content 	.= $this->getPagination(0, 3, 10)->getHtml();
				$content 	.= "</td>";
				$content 	.= "<td colspan='".$colspan1."'>";
					$content 	.= $this->getNumber();
				$content 	.= "</td>";
			$content 	.= "</tr>";
		$content 	.= "</$foot>";
		return $content;
	}
	
	public function getPagination($active,$total,$max){
		$pagination = new Pagination('','', $active, 3, $this->action.".paginate(this)", $max);
		return $pagination;
	}
	
	public function getNumber(){
		$value  = array(
					array("name"=>"     5","value"=>5),
					array("name"=>"    10","value"=>10),
					array("name"=>"    50","value"=>50),
					array("name"=>"   100","value"=>100),
					array("name"=>"   500","value"=>500),
					array("name"=>" 1.000","value"=>1000),
					array("name"=>" 5.000","value"=>5000),
					array("name"=>"10.000","value"=>10000)
				);
		$select = new Select($this->name."_max", $this->name."_max", $value);
		$select ->setAction($this->action.".view()")
				->setClass("number_select");
		return $select->getHtml();
	}
	
	public function getCSSActivated(){
		$result 	 = "<style type='text/css'>";
		$result 	.= "@media only screen and (max-width: ".$this->width_activate_responsive."px)";
		$result 	.= "{";
			$result .="table#table_".$this->name.",table#table_".$this->name." thead ".",table#table_".$this->name." tbody ".",table#table_".$this->name." tfoot ".",table#table_".$this->name." tr ".",table#table_".$this->name." td ".",table#table_".$this->name." th ";
			$result .="{ display:block;}";
		$result 	.= "table#table_".$this->name." thead tr { position: absolute; top: -9999px; left: -9999px; } ";
		$result 	.= "table#table_".$this->name." tr { border: 1px solid #ccc; } ";
		$result 	.= "table#table_".$this->name." td, table#table_".$this->name." th { border: none; border-bottom: 1px solid #eee; position: relative; padding-left: 50%; } ";
		$result 	.= "table#table_".$this->name." td:before, table#table_".$this->name." th:before  { position: absolute; top: 6px; left: 6px; width: 45%; padding-right: 10px; white-space: nowrap; } ";
		$num		 = 0;
		foreach($this->header as $name){
			$num++;
			$result .= "table#table_".$this->name." td:nth-of-type(".$num."):before, table#table_".$this->name." th:nth-of-type(".$num."):before  { content: \"".$name."\"; } ";
		}
		$result 	.= "}";
		$result 	.= "</style>";
		return $result;
	}
	
	public function getSearch(){
		$hidden		 = new Hidden($this->name."_number", $this->name."_number",'0');
		$search		 = new Text($this->name."_kriteria", "", "");
		$search		 ->setAction("".$this->action.".search(event)")
					 ->addClass("search_text");
		$btn		 = new Button($this->name."_search_button", "", "");
		$btn		 ->setClass("btn-primary")
					 ->setIcon("icon-white icon-search")
					 ->setIsButton(Button::$ICONIC)
					 ->setAction("".$this->action.".view()");
		$button_grup = new ButtonGroup("");
		$button_grup ->addElement($hidden)
					 ->addElement($search)
					 ->addElement($btn)
					 ->setMax(10000, "");
		foreach($this->footer_button as $b){
			$button_grup->addElement($b);
		}
		return $button_grup->getHtml();
	}
}
?>
