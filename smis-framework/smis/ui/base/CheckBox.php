<?php

class CheckBox extends HTML{
	private $on;
	private $off;
	
	public function __construct($id, $name, $value){
		parent::__construct($id, $name, $value);
		$this->on="on";
		$this->off="off";
		$this->text="&nbsp;";
	}
	
	public function getHtml(){
		$checked="";
		if(is_bool($this->value) && $this->value || $this->value=="1"){
			$checked="checked='checked'";
		}
		$content="<div  class='div-check ".$this->class."' ><input ".$checked." dv='".$this->value."' ".$this->getDisabled()." id='".$this->id."' data-text-label='".$this->text."'  data-on-label='".$this->on."' data-off-label='".$this->off."' data-on='primary' data-off='' type='checkbox' data-size='normal' onclick=\" ".$this->action." \" ".$this->atribute."  class=' smis_switch switch-medium ".$this->class." ' name='".$this->name."'  /> <font class='checkbox_text'>".$this->name."</font></div>";
		return $content;
	}
	
	public function setString($on,$off,$text="&nbsp;"){
		$this->on=$on;
		$this->off=$off;
		$this->text=$text==""?"&nbsp;":$text;
	}
}

?>