<?php 

class Tree{
	
	private $parent;
	private $name;
	private $id;
	private $class;
	private $value;
	private $icon;
	private $child;
	private $expand;
	
	public function __construct($id,$name,$value,$parent=null){
		$this->id=$id;
		$this->name=$name;
		$this->parent=$parent;
		$this->class="";
		$this->value=$value;
		$this->icon="";
		$this->child=array();
		$this->expand=false;
	}
	
	public function addChild(Tree $child){
		$this->child[]=$child;
		$child->setParent($this);
		return $this;
	}
	
	public function setExpand($expand){
		$this->expand=$expand;
		return $this;
	}
	
	public function countChild(){
		return count($this->child);
	}
	
	public function setClass($class){
		$this->class=$class;
		return $this;
	}
	
	public function setParent(Tree $parent){
		$this->parent=$parent;
		return $this;
	}
	
	public function setValue($value){
		$this->value=$value;
		return $this;
	}
	
	public function setIcon($icon){
		$this->icon=$icon;
		return $this;
	}
	
	private function getChildContent(){
		if($this->countChild()==0)
			return "";
		
		$result="<ul>";
		foreach($this->child as $child){
			$result.=$child->getHtml();
		}
		$result.="</ul>";
		return $result;
		
	}
	
	public function getParent(){
		return $this->parent;
	}
	
	public function isExpand(){
		if($this->parent==null)
			return true;
		if($this->parent->getParent()==null)
			return true;
		return $this->expand;
	}
	
	private function getContent(){
		$result="";
		if($this->parent==null){
			$result=$this->getChildContent();
		}else{
			$expand=$this->isExpand()?"list-item":"none";
			$result='<li style="display:'.$expand.';">';
				$result.='<span id="'.$this->id.'_span" class="'.$this->class.'">';
					$result.='<i id="'.$this->id.'_icon" class="'.$this->icon.'"></i>';
					$result.='<font id="'.$this->id.'_name"> '.$this->name.' </font>';
					$result.='<font id="'.$this->id.'_value"> '.$this->value.' </font>';
				$result.='</span>';
				$result.=$this->getChildContent();
			$result.='</li>';
		}
		return $result;
	}
	
	public function getHtml(){
		$result="";
		if($this->parent==null){
			$result="<div class='tree well' id='".$this->id."'>";
				$result.=$this->getContent();
			$result.="</div>";
			$result.="<script type='text/javascript'>$('#".$this->id."').stree();</script>";
		}else{
			$result=$this->getContent();
		}
		return $result;
	}
	
	
}


?>