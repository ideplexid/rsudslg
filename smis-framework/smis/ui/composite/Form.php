<?php

class Form{
	
	private $id;
	private $class;
	private $title;
	private $element;
	private $is_multipart;
	private $component_class;
	
	
	
	public function __construct($id,$class,$title){
		$this->id=$id;
		$this->class=$class;
		$this->title=$title;
		$this->element=array();
		$this->is_multipart=false;
		$this->component_class="";
	}

	public function setComponentClass($class){
		$this->component_class=$class;
		return $this;
	}
	
	public function setMultipart($multi){
		$this->is_multipart=$multi;
		return $this;
	}
	
	public function setTitle($title){
		$this->title=$title;
		return $this;
	}
	
	public function setElement($element){
		$this->element=$element;
		return $this;
	}
	
	public function getSize(){
		return count($this->element);
	}
	
	
	
	public function addElement($name,$html,$class=""){
		$element=array();
		$element['name']=$name;
		$element['html']=$html;
		$element['class']=$class;
		if($this->element==null){
			$this->element=array();
		}
		$this->element[]=$element;
		return $this;
	}
	
	public function getElement(){
		return $this->element;
	}

	
	public function getHtml(){
			$multi="";
			if($this->is_multipart){
				$multi="enctype='multipart/form-data'";
			}
			$content="<div class='form-container' ".$multi."><form role='form' id='".$this->id."' class='smis_form'>";
			if($this->title!="") $content.="<legend>".$this->title."</legend>";
			
				foreach($this->element as $e){		
					$elm="";
					$class="";
					if(is_object($e['html'])) {
						$elm=$e['html']->getHtml();
						$class=$e['html']->getID();
					}else $elm=$e['html'];
					
					
					if($e['name']==null) $content.="<div class=' ".$this->component_class." ".$this->class." ".$class."'>".$elm." </div>";
					else $content.="<div class=' ".$this->component_class." ".$this->class." ".$class." ".$e['class']."'> <label>".$e['name']."</label> ".$elm." </div>";
				}
			$content.="</form></div>";
		return $content;
			
	}
}
?>