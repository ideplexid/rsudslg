<?php
/**
 * this class was used as based
 * component to join several 
 * button into one single 
 * look alike element
 * 
 * @since       : 14 Januari 2014
 * @version     : 4.0.1
 * @license     : Apache V3
 * @copyright   : goblooge@gmail.com
 * @author      : Nurul Huda
 * 
 * */

class ButtonGroup{
	
	/*@var Button*/
	private $bt_group;
    /*@var int*/
	private $total;
	/*@var String*/
	private $class;
	/*@var int*/
	private $max;
    /*@var string*/
	private $default;
	/*@var string*/
	private $btn_class;
	
	public function __construct($class=""){
		$this->bt_group=array();
		$this->total=0;
		$this->class=$class;
		$this->max=3;
		$this->default="Action ";
		$this->btn_class=" btn-primary ";
	}
	
	/**
	 * set max value and defautl control
	 * @param unknown $max
	 * @param unknown $default (default name of control)
	 */
	public function setMax($max,$default){
		$this->max=$max;
		$this->default=$default;
		return $this;
	}
	
    /**
     * @brief set the button class
     * @param string $class 
     * @return  this
     */
	public function setButtonClass($class){
		$this->btn_class=$class;
		return $this;
	}
	
    /**
     * @brief add the button element
     * @param Button $button 
     * @return  
     */
	public function addElement($button){
		$this->bt_group[$this->total]=$button;
		$this->total++;
		return $this;
	}
	
    /**
     * @brief just an alias, same like addElement
     * @param Button $button 
     * @return  this
     */
	public function addButton($button){
		$this->addElement($button);
		return $this;
	}
	
	
    /**
     * @brief set change the action id and slug
     * @param string $name 
     * @param string $action 
     * @param string $slug 
     * @param string $id 
     * @return  this
     */
    public function setChangeComponent($name,$action,$slug,$id){
        foreach($this->bt_group as $btn){
            $btn->setAction($action.".".$slug."('".$id."')");
            $btn->setId($name."_".$slug);
        }
        return $this;
    }
    
    /**
     * @brief this function used only to adding
     *          new element but directlu create new button
     * @param string $id 
     * @param string $name 
     * @param string $value 
     * @param string $action 
     * @param string $class 
     * @param string $atribute 
     * @param string $icon 
     * @return  button;
     */
    function add($id,$name,$value,$action,$class,$atribute,$icon='fa fa-cog'){
		$button=new Button($id, $name, $value);
		$button->setAction($action);
		$button->setIcon($icon);
		$button->setIsButton(Button::$ICONIC_TEXT);
		$button->setClass($class);
		$button->setAtribute($atribute);
		
		$this->addElement($button);
		return $button;
	}
	
    /**
     * @brief get the html generated string
     * @return  string
     */
	public function getHtml(){
		$content="<div class='btn-group ".$this->class."'>";
		if($this->max!=-1 && $this->max<$this->total){
			$content.=" <button class='btn ".$this->btn_class."'>".$this->default."  </button>";
			$content.="<a class='btn ".$this->btn_class." dropdown-toggle ' data-toggle='dropdown' href='#'><span class='caret'></span></a>";
			$content.="<ul class='dropdown-menu pull-right'>";
			foreach($this->bt_group as $b){
				if(is_a($b, "Button")){
					$b->setIsButton(Button::$DROPDOWN);
				}
				$content.="<li>".$b->getHtml()."</li>";
			}
  			$content.="</ul>";
		}else{
			foreach($this->bt_group as $b){
				$content.=$b->getHtml();
			}
		}
		$content.="</div>";
		return $content;
	}
	
	public function getID(){
		return "";
	}
	
	public function setDisabled(){
		/*only to override, never disabled actually*/
	}
	
}

?>