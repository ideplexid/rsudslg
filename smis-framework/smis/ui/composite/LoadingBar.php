<?php

class LoadingBar extends HTML{
	
	private $is_hide;
	
	public function __construct($id,$name,$value=0){
		parent::__construct($id, $name, $value);
		$this->is_hide=true;
	}
	
	public function setHidden($hide){
		$this->is_hide=$hide;
	}
	
	
	
	public function getHtml(){
		$html=" <div id='".$this->id."' class=' ".($this->is_hide?"hide":"")." '>
					<div class='loading_title'>".$this->name."</div>
					<div id='loading_bar' class='progress progress-striped ".$this->class."'>
							<div class='bar' style='width: ".$this->getValue()."%;'></div>
				    </div>
			    </div>
				";
		return $html;
	}
	
}

?>