<?php 
/**
 * this is a table that spesifically design for 
 * Reporting , and graphical data view
 * so the reporti would view a graphic data that could be 
 * see ad pie, scatter , area or bar data model
 * 
 * @author 		: Nurul Huda
 * @since 		: 17 Aug 2014
 * @copyright 	: goblooge@gmai.com
 * @version 	: 5.0.1
 * 
 */

class Report extends Table{
	/**@var boolean */
	private $place_advance_modal_to_footer;
	/**@var boolean */
	private $is_diagram_enabled;
	/**@var int */
	private $height;
	/**@var string */
	private $model_diagram;
	/**@var boolean */
	private $is_model_show;
	/**@var boolean */
	private $is_series_show;
	/**@var boolean */
	private $is_range_show;
	/**@var boolean */
	private $is_used_bar;
	/**@var boolean */
	private $is_used_area;
	/**@var boolean */
	private $is_used_donut;
	/**@var boolean */
	private $is_used_line;
    /**@var string */
	private $mode;
    /**@var boolean */
	protected $allow_date_empty;
	
    public static $DATETIME		 = 1;
	public static $DATE			 = 2;
    public static $NO_DATE_TIME	 = 3;    
	public static $DIAGRAM_BAR	 = "bar";
	public static $DIAGRAM_LINE	 = "line";
	public static $DIAGRAM_AREA	 = "area";
	public static $DIAGRAM_DONUT = "donut";
	
	
	public function __construct($header, $title, $content){
		parent::__construct($header, $title, $content, false);
		$this->setAction(false);
		$this->place_advance_modal_to_footer = false;
		$this->is_diagram_enabled			 = false;
		$this->height						 = 200;
		$this->model_diagram				 = null;
		$this->is_model_show				 = true;
		$this->is_series_show				 = true;
		$this->is_range_show				 = true;
		$this->is_used_area					 = true;
		$this->is_used_line					 = true;
		$this->is_used_bar					 = true;
		$this->is_used_donut				 = true;
        $this->mode							 = self::$DATE;
        $this->allow_date_empty				 = false;
	}
	
	public function setAllowDateEmptyEnable($enabled){
        $this->allow_date_empty = $enabled;
        return $this;
    }
	
	public function setModelBarUsability($used){
		$this->is_used_bar = $used;
		return $this;
	}
	
	public function setModelLineUsability($used){
		$this->is_used_line = $used;
		return $this;
	}
	
	public function setModelDonutUsability($used){
		$this->is_used_donut=$used;
		return $this;
	}
	
	public function setModelAreaUsability($used){
		$this->is_used_area = $used;
		return $this;
	}
	
	public function setModelEnabled($enabled){
		$this->is_model_show = $enabled;
		return $this;
	}
	
	public function setSeriesEnabled($enabled){
		$this->is_series_show = $enabled;
		return $this;
	}
	
	public function setRangeEnabled($enabled){
		$this->is_range_show = $enabled;
		return $this;
	}
	
	public function setModelDiagram($model){
		$this->model_diagram = $model;
		return $this;
	}
	
	public function setHeightDiagram($height){
		$this->height = $height;
		return $this;
	}
	
	public function setAdvanceModalToFooter($place){
		$this->place_advance_modal_to_footer = $place;
		return $this;
	}
	
	public function setDiagram($enabled){
		$this->is_diagram_enabled = $enabled;
		return $this;
	}
	
	public function getDiagram($id){
		$result="<div  id='".$id."' style='height:".$this->height."px'></div>";
		return $result;
	}
    
    public function setMode($mode){
        $this->mode = $mode;
        return $this;
    }
	
	public function getFooter(){
		$total_column 	= count($this->header);
		if($this->is_action) $total_column++;
		$colspan1		= floor($total_column/3);
		$colspan2		= $total_column-2*$colspan1;		
		$content="<tfoot class='noprint'>";			
			$content.="<tr>";
				$content.="<tr class='inverse'>";
				$content.="<td colspan='".$colspan1."'>";
					$content.="<div class='input-append'>";
						$content.=$this->getSearch();
					$content.="</div>";
				$content.="</td>";
				$content.="<td colspan='".$colspan2."'  class='pagination-center' id='".$this->name."_pagination'>";
					$content.=$this->getPagination(0, 3, 10)->getHtml();
				$content.="</td>";
				$content.="<td colspan='".$colspan1."'>";
					$content.=$this->getNumber();
				$content.="</td>";
			$content.="</tr>";
			$content.="</tr>";			
			if($this->place_advance_modal_to_footer)
			$content.=$this->getAdvanceFooterElement($total_column);			
		$content.="</tfoot>";		
		return $content;
	}
	
	public function getAdvanceFooterElement($total_tr){
		$content="<tr colspan='".$total_tr."'>";
			$this->getAdvanceModal()->getHtml();
		$content.="</tr>";		
		return $content;
	}
	
	public function getAdvanceModal(){
		$modal = new Modal($this->name."_add_form", "smis_form_container full",$this->name);
		$modal ->setAlwaysShow(true);
		$this  ->setModalComponent($modal);
		$yn	   = $this->allow_date_empty?"y":"n";
        if($this->mode==self::$DATE){
			$modal ->addModalElement('date', '', 'Dari', 'from_date', '', $yn,false)
				   ->addModalElement('date', '', 'Sampai', 'to_date', '', $yn,false);
        }else if($this->mode==self::$DATETIME){
			$modal ->addModalElement('datetime', '', 'Dari', 'from_date', '', $yn,false)
				   ->addModalElement('datetime', '', 'Sampai', 'to_date', '', $yn,false);
        }		
		if($this->is_diagram_enabled)
			$this->addDiagramFunctionality($modal);
		
		/** adding from and to in modal */
		$button = new Button("", "", "");
		$button ->setIsButton(Button::$ICONIC)
				->setIcon("icon-white icon-search")
				->setClass("btn-primary")
				->setAction($this->name.".view()");
		$modal  ->addFooter($button);
		return $modal;
	}
	
	private function addDiagramFunctionality($modal){
		$option = array(
				array("value"=>"10","name"=>"10"),
				array("value"=>"50","name"=>"50"),
				array("value"=>"100","name"=>"100"),
				array("value"=>"1000","name"=>"1.000"),
				array("value"=>"10000","name"=>"10.000"),
				array("value"=>"100000","name"=>"100.000"),
				array("value"=>"1000000","name"=>"1.000.000"),
				array("value"=>"10000000","name"=>"10.000.000"),
				array("value"=>"100000000","name"=>"100.000.000"),
				array("value"=>"1000000000","name"=>"1.000.000.000")
		);
		$select = new Select($this->name."_max_num", "max_num", $option);
		$select ->setAction($this->name.".setMax(this)");		
		$option = array();
		if($this->is_used_bar) 		$option[] = array("value"=>"bar","name"=>"Bar");
		if($this->is_used_line) 	$option[] = array("value"=>"line","name"=>"Line");
		if($this->is_used_area) 	$option[] = array("value"=>"area","name"=>"Area");
		if($this->is_used_donut) 	$option[] = array("value"=>"donut","name"=>"Donut");
		if($this->model_diagram==null){
			$select2 = new Select($this->name."_tipe_model", "tipe_model", $option);
			$select2 ->setAction($this->name.".setModel(this.value)");
		}else {
			$select2 = new Hidden($this->name."_tipe_model", "tipe_model",  $this->model_diagram);
		}
		
		$option = array(
				array("value"=>DBReport::$DATE,"name"=>"Daily"),
				array("value"=>DBReport::$MONTH,"name"=>"Monthly"),
				array("value"=>DBReport::$YEAR,"name"=>"Yearly"),
		);
		$select3 = new Select($this->name."_mode_model", "mode_model", $option);
		$select3 ->setAction($this->name.".view()");
		
		if($this->is_range_show) 
			$modal->addElement("Max Range", $select);
		
		if($this->is_model_show){
			if($this->model_diagram==null) $modal->addElement("Model", $select2);
			else $modal->addElement("", $select2);
		}
		if($this->is_series_show)
			$modal->addElement("Series", $select3);
	}
}
?>