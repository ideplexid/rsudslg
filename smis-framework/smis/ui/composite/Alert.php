<?php 

class Alert extends HTML{
	
	public static $INFO="alert-info";
	public static $INVERSE="alert-inverse";
	public static $DANGER="alert-error";
	public static $SUCCESS="alert-success";
	public static $WARNING="alert-warning";
	protected $listval;
	protected $model;
	protected $remove;
	protected $alert;
	
	
	public function __construct($id,$name,$value){
		parent::__construct($id, $name, $value);
		$this->model=self::$INFO;
		$this->listval=array();
		$this->remove=false;
		$this->alert=true;
	}
	
	public function setAlertMode($modealert){
		$this->model=$modealert;
		return $this;
	}
	
	public function setRemovable($removable){
		$this->remove=$removable;
		return $this;
	}
	
	public function addList($title,$content){
		if($this->listval[$title]==NULL){
			$this->listval[$title]=array();
		}
		$this->listval[$title][]=$content;
		return $this;
	}
	
	public function addString($content){
		$this->listval[]=$content;
		return $this;
	}
	
	public function setClass($class){
		$this->class=$class;
		return $this;
	}
	
	public function setTitle(){
		$this->name=$name;
		return $this;
	}
	
	public function getValue(){
		$result="<div>".$this->value."</div>";
		$curtitle="";
		foreach($this->listval as $key=>$content){			
			if(is_array($content)){
				$result.="<strong>".$key."</strong>";
				$result.="<ul>";
				foreach($content as $a=>$c){
					$result.="<li>".$c."</li>";
				}
				$result.="</ul>";
			}else{
				$result.="<p>".$content."</p>";
			}
		}
		return "<div style='text-align:justify;'>".$result."</div>";
	}
	
	public function setModel($model){
		$this->model=$model;
		return $this;
	}
	
	public function getHTML(){
		if($this->alert) $result='<div id="'.$this->id.'" class="alert '.$this->class.'  '.$this->model.'">';
		else $result='<div id="'.$this->id.'" class=" '.$this->class.' ">';
		
		if($this->remove)
			$result.='<button type="button" class="close" data-dismiss="alert"><i class="fa fa-remove"></i></button>';
		$result.='<h4>'.$this->name.'</h4>';
		$result.=$this->getValue();
		$result.='</div>';
		return $result;
	}
	
}

?>