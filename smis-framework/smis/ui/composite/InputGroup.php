<?php

class InputGroup{
	
	private $bt_group;
	private $total;
	private $class;
	
	function __construct($class){
		$this->bt_group=array();
		$this->total=0;
		$this->class=$class;
	}
	
	function addComponent($component){
		$this->bt_group[$this->total]=$component;
		$this->total++;
        return $this;
	}
	
	public function getElement($index){
		return $this->bt_group[$index];
	}
	
	public function setDisabled($disabled){
		return $this;
	}
	
	function getHtml(){
		$content="<div class='input-append ".$this->class."'>";
			foreach($this->bt_group as $b){
				$content.=$b->getHtml();
			}
		$content.="</div>";
		return $content;
	}
	
	function getID(){
		return "";
	}	
}

?>