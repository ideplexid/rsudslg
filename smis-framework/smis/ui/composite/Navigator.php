<?php
	
class Navigator{
	
	private $listMenu;
	private $total;
	private $property;
	private $settings;
	private $capabilities;
	private $mode;
	private $notification;
	private $user;
	private $invert_color;
	
	public static $MENU_MODE_MULTIPLE="multiple";
	public static $MENU_MODE_SINGLE="single";
	public static $MENU_MODE_SINGLE_5="single_5";
	public static $MENU_MODE_SINGLE_6="single_6";
	public static $MENU_MODE_SINGLE_7="single_7";
	public static $MENU_MODE_SINGLE_8="single_8";
	public static $MENU_MODE_SIDEBAR="sidebar";
	
	
	public function  __construct(){
		$this->listMenu=array();
		$this->total=0;
		$this->property=array();
		$this->mode=self::$MENU_MODE_MULTIPLE;
		$this->notification=0;
		$this->invert_color=true;
	}
	
	public function setInvertColor($invert){
		$this->invert_color=$invert;
		return $this;
	}
	
	public function getNavigatorMode(){
		return $this->mode;
	}
	
	public function setNavigatorMode($mode){
		$this->mode=$mode;
		return $this;
	}
	
	public function countCapability(){
		$total=0;
		foreach($this->listMenu as $slug=>$menu){
			if( ($slug=="smis-administrator" && $this->capabilities=="smis-administrator") ||  (isset($this->settings[$slug]) && is_array($this->settings[$slug]) && $menu->is_capable($this->settings[$slug])) ){
				$total++;
			}
		}
		return $total;
	}
	
	public function getName(){
		if($this->user!=null){
			$this->user->getUsername();
		}
		global $user;
		return $user->getUsername();
	}
	
	public function setUser($capabilities,$settings,User $user){
		$this->settings=$settings;
		$this->capabilities=$capabilities;
		$this->user=$user;
		return $this;
	}
	
	public function addProperty($slug,$value){
		$this->property[$slug]=$value;
		return $this;
	}
	
	
	public function setNotification($notif){
		$this->notification=$notif;
		return $this;
	}
	
	/**
	 * menu name that want to added
	 * @param Menu $menu
	 * @param String $slug
	 */
	
	public function addMenu(Menu $menu,$slug){
		$this->listMenu[$slug]=$menu;	
		return $this;
	}
	
	public function getMenus(){
		return $this->listMenu;
	}
	
	
	/**
	 * get Menu based on slug
	 * @param String $slug
	 */
	public function getMenu($slug){
		if(isset($this->listMenu[$slug]))
			return $this->listMenu[$slug];
		return null;
	}
	
	public function getProperty($slug){
		if(isset($this->property[$slug]))
			return $this->property[$slug];
		else return "";
	}
	
	private function getRequiresMenuHTML(){
		$notif="<font id='smis_notification' class='label '>".$this->notification."</font>";
		$notify=new Menu("");
		$notify->addProperty("name", $notif." Home <i class='fa fa-home'></i> ")
			   ->addProperty("title", "Notification Message")
			   ->addProperty("href", "#")
			   ->setAction("reload_page()")
			   ->setCapabilityEnabled(false);
		
				
		$logout=new Menu("fa fa-power-off");
		$logout	->addProperty("name", "Logout")
				->addProperty("href", "#")
				->addProperty("title", "Log out from this System")
				->setAction("logout()")
				->setCapabilityEnabled(false);
		
		$feedback=new Menu("fa fa-question");
		$feedback->addProperty("name", "Feedback")
				 ->addProperty("href", "javascript:;")
				 ->addProperty("title", "Feedback")
				 ->addProperty('class','smis_action')
				 ->addProperty("page","smis-tools")
				 ->addProperty("action", "feedback")
				 ->addProperty("icon", "fa fa-question")
				 ->addProperty('prototype_name',"")
				 ->addProperty('prototype_slug',"")
				 ->addProperty('prototype_implement',"")
				 ->setCapabilityEnabled(false);
		
		$reload=new Menu("fa fa-refresh");
		$reload	->addProperty("name", "Profile")
				->addProperty("href", "#")
				->addProperty("title", "Log out from this System")
				->setAction("reload_page()")
				->setCapabilityEnabled(false);
		
		$menu=new Menu("fa fa-user");
		$menu->setCapabilityEnabled(false)
			 ->addProperty('title','User Information')
			 ->addProperty('name',$this->getName())
			 ->addSub($feedback)
			 ->addSub($reload)
			 ->addSub($logout);
			 
		return $notify->getHtml().$menu->getHtml();
		
	}
	
	protected function getMultipleMenu(){
		$invert=$this->invert_color?"navbar-inverse":"";
		$content="<div class='navbar ".$invert." ' id=".$this->getProperty('id').">";
			$content.="<div class='navbar-inner'>";
				$content.="<div class='container-fluid'>";
				$content.='<a href="#" class="btn btn-navbar btn-inverse" data-toggle="collapse" data-target=".nav-collapse">';
					$content.='<i class="icon-white icon-th"></i>';
				$content.='</a>';
				$content.="<a class='brand' id='smis_brand' href='#' onclick='sidebar()' > <i class='fa fa-bars '></i> ".getSettings(null, 'smis_autonomous_abbrevation', "Safethree MIS")."</a>";
				$content.="<div class='nav-collapse collapse'>";
					$content.="<ul class='nav'>";
						$content.=$this->getMenusMultipeHtml();						
					$content.="</ul>";
					$content.="<ul class='nav navbar-nav navbar-right'>";
						$content.=$this->getRequiresMenuHTML();
					$content.="</ul>";
				$content.="</div>";
				
				$content.="</div>";
			$content.="</div>";
		$content.="</div>"; //NAVIGATION
		return $content;
		
	}
	
	protected function getSingleMenu(){
		$invert=$this->invert_color?"navbar-inverse":"";
		$content="<div class='navbar ".$invert." '  id=".$this->getProperty('id').">";
			$content.="<div class='navbar-inner'>";
				$content.="<div class='container-fluid'>";
				$content.='<a href="#" class="btn btn-navbar btn-inverse" data-toggle="collapse" data-target=".nav-collapse">';
					$content.='<i class="icon-white icon-th"></i>';
				$content.='</a>';
				$content.="<a class='brand' href='#' onclick='sidebar()' > <i class='fa fa-bars '></i> ".getSettings(null, 'smis_autonomous_abbrevation', "Safethree MIS")."</a>";
				$content.="<div class='nav-collapse collapse'>";
					$content.="<ul class='nav'>";
						$content.=$this->getMenusSingleHtml();
					$content.="</ul>";
				$content.="</div>";
				
				$content.="<ul class='nav navbar-nav navbar-right'>";
				$content.=$this->getRequiresMenuHTML();
				$content.="</ul>";
				
				$content.="</div>";
			$content.="</div>";
		$content.="</div>"; //NAVIGATION
		return $content;
	}
	
	

	
	public function getModeSidebase(){
		$invert=$this->invert_color?"navbar-inverse":"";
		$content="<div class='navbar ".$invert." '  id=".$this->getProperty('id').">";
			$content.="<div class='navbar-inner'>";
				$content.="<div class='container-fluid'>";
				$content.="<a class='brand' href='#' onclick='sidebar()' > <i class='fa fa-bars '></i> ".getSettings(null, 'smis_autonomous_abbrevation', "Safethree MIS")."</a>";
				$content.="<ul class='nav navbar-nav navbar-right'>";
				$content.=$this->getRequiresMenuHTML();
				$content.="</ul>";
				$content.="</div>";
			$content.="</div>";
		$content.="</div>"; //NAVIGATION
		return $content;
	}
	
	public function getHtml(){
	
		$count=$this->countCapability();
		if($this->mode==self::$MENU_MODE_SINGLE) return $this->getSingleMenu();
		else if($this->mode==self::$MENU_MODE_SINGLE_5 && $count>5 ) return $this->getSingleMenu();
		else if($this->mode==self::$MENU_MODE_SINGLE_6 && $count>6 ) return $this->getSingleMenu();
		else if($this->mode==self::$MENU_MODE_SINGLE_7 && $count>7 ) return $this->getSingleMenu();
		else if($this->mode==self::$MENU_MODE_SINGLE_8 && $count>8 ) return $this->getSingleMenu();
		else if($this->mode==self::$MENU_MODE_SIDEBAR ) return $this->getModeSidebase();
		else return $this->getMultipleMenu();
	
	}
	
	
	public function getMenusSingleHtml(){	
		$content="";
		ksort($this->listMenu);
		foreach($this->listMenu as $slug=>$menu){
			$menu->setParent($this);
		}
		$content=$this->getMenusMultipeHtml();
		
		$logo=getLogo();
		$class='';
		if(strlen($logo)>1000) $class="invert";
		$lg="<img src='".$logo."' class='menu_logo $class'/>";
		
		$result='<li class="menu-item  dropdown">	
					<a title="Start" href="#" data-toggle="dropdown" class="dropdown-toggle ">
						'.$lg.' Start
						<span class="caret"></span>
					</a>
					<ul class="dropdown-menu">
					'.$content.'
					</ul>	
				</li>';	
		return $result;
	}
	
	
	
	public function getMenusMultipeHtml(){
		$content="";
		$menulist=array();
		foreach($this->listMenu as $slug=>$menu){
			$menulist[$menu->getName()]=array("menu"=>$menu,"slug"=>$slug);
		}
		ksort($menulist);	
		foreach($menulist as $slug=>$cmenu){
			$slug=$cmenu['slug'];
			$menu=$cmenu['menu'];
			if($slug=="smis-administrator"){
				if($this->capabilities=="administrator"){
					$menu->setCapabilityFunction(false);
					$content.=$menu->getHtml();
				}	
			}else if($this->settings!=null && isset($this->settings[$slug]) && is_array($this->settings[$slug])){
				$settings=$this->settings[$slug];
				$menu->setCapability($settings);
				$content.=$menu->getHtml();
			}
		}
		return $content;
	}
	
	
	public function getMenuSideBaseHtml(){
		$content="<ul class='nav navbar-nav side-nav'>";
		$menulist=array();
		foreach($this->listMenu as $slug=>$menu){
			$menulist[$menu->getName()]=array("menu"=>$menu,"slug"=>$slug);
		}
		ksort($menulist);	
		foreach($menulist as $slug=>$cmenu){
			$slug=$cmenu['slug'];
			$menu=$cmenu['menu'];
			$menu->setMenuMode(Menu::$MODE_SIDEBAR);
			if($slug=="smis-administrator"){
				if($this->capabilities=="administrator"){
					$menu->setCapabilityFunction(false);
					$content.=$menu->getHtml();
				}	
			}else if($this->settings!=null && isset($this->settings[$slug]) && is_array($this->settings[$slug])){
				$settings=$this->settings[$slug];
				$menu->setCapability($settings);
				$content.=$menu->getHtml();
			}
		}
		$content.="</ul>";
		return $content;
	}
	
}
	
?>