<?php

class Pagination{
	
	private $id;
	private $class;
	private $active;
	private $total;
	private $action;
	private $max;
	
	public	function __construct($id,$class,$active,$total,$action,$max){
		$this->id=$id;
		$this->class=$class;
		$this->active=$active;
		$this->total=$total; //maximum kotak
		$this->action=$action;
		$this->max=$max;//las page
	}
	
	public function getHtml(){
		
		$content="<div class='pagination ".$this->class."' id='".$this->id."'>";
			$content.="<ul>";
				$start=0;
				if($this->active<$this->max){
					$start=$this->active-floor($this->total/2);
					if($start<0) $start=0;
				}
				
				if($start!=0) {
					$content.="<li>".$this->newLink($this->action,0,"<i class='fa fa-angle-double-left'></i>")."</li>";
				}
				
				for($i=0;$i<$this->total;$i++){
					$no=$start+$i;
					if($no>$this->max-1) break;
					
					if($no==$this->active) 
						$content.="<li class='active'>".$this->newLink($this->action,$no,($no+1))."</li>";
					else 
						$content.="<li>".$this->newLink($this->action,$no,($no+1))."</li>";	
				}
				
				if($start+$this->total<=$this->max){
					$content.="<li>".$this->newLink($this->action,$this->max,"<i class='fa fa-angle-double-right'></i>")."</li>";
				}
				
			$content.="</ul>";
		$content.="</div>";
		return $content;
	} 
	
	private function newLink($action,$value,$name){
		return "<a onclick='".$action."' val='".$value."' > ".$name." </a>";
	}
	
}

?>