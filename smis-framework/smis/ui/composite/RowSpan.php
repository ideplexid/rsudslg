<?php 

class RowSpan{
	
	private $id;
	private $is_fluid;
	private $class;
	private $span;
	private $div;
	private $noparent;
	private $nochild;
	
	public static $TYPE_HTML=1;
	public static $TYPE_COMPONENT=2;
	public static $TYPE_INCLUDE=3;
	
	public function __construct($id="",$class="",$is_fluid=true){
		$this->span=array();
		$this->id=$id;
		$this->class=$class;
		$this->is_fluid=$is_fluid;		
		$this->div=false;
		$this->noparent=false;
		$this->nochild=false;
	}
	
	public function setDivEnable($enabled,$noparent=false,$nochild=false){
		$this->div=$enabled;
		$this->noparent=$noparent;
		$this->nochild=$nochild;
		return $this;
	}
	
	public static function getJoinRowSpan(RowSpan $s1, RowSpan $s2, 
			RowSpan $s3=null, RowSpan $s4=null,RowSpan $s5=null,RowSpan $s6=null,
			RowSpan $s7=null,RowSpan $s8=null,RowSpan $s9=null,RowSpan $s10=null,
			RowSpan $s11=null,RowSpan $s12=null){
		$result=$s1->getHtml();
		$result.=$s2->getHtml();
		if($s3!=null) $result.=$s3->getHtml();
		if($s4!=null) $result.=$s4->getHtml();
		if($s5!=null) $result.=$s5->getHtml();
		if($s6!=null) $result.=$s6->getHtml();
		if($s7!=null) $result.=$s7->getHtml();
		if($s8!=null) $result.=$s8->getHtml();
		if($s9!=null) $result.=$s9->getHtml();
		if($s10!=null) $result.=$s10->getHtml();
		if($s11!=null) $result.=$s11->getHtml();
		if($s12!=null) $result.=$s12->getHtml();
		return $result;
	}
	
	private static function getComponent($type,$content){
		if($type==self::$TYPE_HTML){
			return $content;
		}else if($type==self::$TYPE_INCLUDE){
			ob_start();
			require_once($content);
			$ct = ob_get_clean();
			return $ct;
		}else if($type==self::$TYPE_COMPONENT){
			$ct=$content->getHtml();
			return $ct;
		}
	}
	
	public function addSpan($content, $span=1,$type=1, $id="",$class=""){
		$this->span[]=array("id"=>$id,"span"=>$span,"class"=>$class,"content"=>$content,"type"=>$type);
		return $this;
	}
	
	public function getHtml(){
		$result="";
		if($this->div){
			if(!$this->noparent) $result.="<div class=' ".$this->class." id='".$this->id."' >";
			foreach($this->span as $span){
				if(!$this->nochild) $result.="<div id='".$span['id']."' span".$span['span']." ".($span['span']>0?"span".$span['span']:"")." >";
				$result.=self::getComponent($span['type'], $span['content']);
				if(!$this->nochild) $result.="</div>";
			}
			if(!$this->noparent) $result.="</div>";
		}else{
			$result.="<div class=' ".$this->class." row".($this->is_fluid?"-fluid":"")."' id='".$this->id."' >";
			foreach($this->span as $span){
				$result.="<div id='".$span['id']."' class='".$span['class']." ".($span['span']>0?"span".$span['span']:"")." ' >";
				$result.=self::getComponent($span['type'], $span['content']);
				$result.="</div>";
			}
			$result.="</div>";
		}
		return $result;
	}
	
}

?>