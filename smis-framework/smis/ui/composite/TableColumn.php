<?php

class TableColumn{
	private $value;
	private $class;
	private $rowspan;
	private $colspan;
	private $model;
	private $id;
	public function __construct($value,$colspan,$rowspan,$class="",$id=""){
		$this->class    = $class;
		$this->value    = $value;
		$this->rowspan  = $rowspan;
		$this->colspan  = $colspan;
		$this->model    = "td";
		$this->id       = $id;
	}
	
	public function setModel($model){
		$this->model    = $model;
		return $this;
	}

	public function setValue($value){
		$this->value    = $value;
		return $this;
	}

	public function setClass($class){
		$this->class    = $class;
		return $this;
	}

	public function setSpan($colspan=-1,$rowspan=-1){
		if($colspan!=-1)
			$this->colspan  = $colspan;
		if($rowspan!=-1)
			$this->rowspan  = $rowspan;
		return $this;
	}

	public function getHtml(){
		return "<".$this->model." rowspan='".$this->rowspan."' colspan='".$this->colspan."' class='smis-column ".$this->class."' id='".$this->id."'>".$this->value."</".$this->model.">";
	}
}
?>