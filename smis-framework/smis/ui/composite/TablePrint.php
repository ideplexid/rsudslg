<?php 
require_once "smis-framework/smis/ui/composite/TableRow.php";
class TablePrint {	
	private $max_column;
	private $rows;
	private $header;
	private $body;
	private $footer;
	private $title;
	private $name;
	private $class;
	private $table_class;
	private $temporary_row;
	private $span12;
	private $is_responsive;
	private $style;
	private $use_footer;
	private $use_header;	
	
	public function __construct($name="",$smis_print_table=true){
		$this->max_column	 = 0;
		$this->name			 = $name;
		$this->class		 = $name."-smis-print-table ";
		if($smis_print_table){
			$this->class	.= " smis-print-table ";	
		}
		$this->use_footer	 = true;
		$this->use_header	 = true;
		$this->header		 = array();
		$this->footer		 = array();
		$this->title		 = array();
		$this->body			 = array();
		$this->createNewRow();
		$this->table_class	 = "";
		$this->is_responsive = false;
		$this->span12		 = true;
		$this->style		 = "";
	}
	
	public function setName($name){
		$this->name			= $name;
		return $this;
	}
	
	public function setDefaultBootrapClass($bool){
		if($bool){
			$this->addTableClass(" table table-bordered table-hover table-condensed ");
		}else{
			$cl 		 = $this->class;
			$cl 		 = str_replace("table table-bordered table-hover table-condensed", "", $cl);
			$this->class = $cl;
		}
		return $this;
	}
	
	public function setUseFooter($use){
		$this->use_footer = $use;
	}
	
	public function setUseHeader($use){
		$this->use_header = $use;
	}
	
	public function setStyle($style){
		$this->style 	  = $style;
	}
	
	public function addStyle($name,$value){
		$this->style 	  = " $name : $value ; ";
	}	
	
	public function setMaxWidth($se){
		$this->span12	  = $se;
		return $this;
	}
	
	public function setResponsive($responsive){
		$this->is_responsive = $responsive;
		return $this;
	}
	
	public function setTableClass($class){
		$this->table_class  .= $class;
		return $this;
	}
	
	
	public function addTableClass($class){
		$this->table_class  .= " ".$class;
		return $this;
	}
	
	public function addBody($tr){
		$this->body[]		 = $tr;
		return $this;
	}
	
	public function addFooter($tr){
		$this->footer[]		 = $tr;
	}
	
	public function addHeader($tr){
		$this->header[]		 = $tr;
		return $this;
	}
	
	public function addTitle($tr){
		$this->title[]		 = $tr;
		return $this;
	}
	
	public function commit($name,$class=""){
		$this->temporary_row->addClass($class);		
		switch ($name) {
			case "title"	: $this->addTitle($this->temporary_row); break;
			case "body"		: $this->addBody($this->temporary_row);  break;
			case "header"	: $this->addHeader($this->temporary_row);  break;
			case "footer"	: $this->addFooter($this->temporary_row);  break;
		}
		$total_element	= $this->temporary_row->getTotalElement();
		if($this->max_column<$total_element){
			$this->max_column = $total_element;
		}
		$row=$this->temporary_row;
		$this->createNewRow();
		return $row;
	}
	
	public function createNewRow(){
		$this->temporary_row	= new TableRows($this->class);
		return $this;
	}
	
	public function addColumn($val,$cspan=1,$rspan=1,$commit=null,$key=null,$class=""){
		$this->temporary_row->addElement($val,$cspan,$rspan,$key,$class);
		if($commit!=null){
			$this->commit($commit);
		}
		return $this;
	}
	
	public function addSpace($cspan,$rspan,$commit=null,$key=null){
		$this->addColumn("&nbsp;",$cspan,$rspan,$commit,$key);
		return $this;		
	}
	
	public function getTitle(){
		$content 		 = "";
		foreach($this->title as $t){
			$t->setModel("th");
			$content	.= $t->getHtml();
		}
		return $content;
	}
	
	public function getFooter(){
		$content		 = "";
		foreach($this->footer as $t){
			$t->setModel("th");
			$content	.= $t->getHtml();
		}
		return $content;
	}
	
	public function getHeader(){
		$content		 = "";
		foreach($this->header as $t){
			$content	.= $t->getHtml();
		}
		return $content;
	}
	
	public function getBody(){
		$content		= "";
		foreach($this->body as $t){
			$content   .= $t->getHtml();
		}
		return $content;
	}
	
	public function getHtml(){
		$span12	 = $this->span12?"span12":"";
		$content = "<table id='".$this->name."' style='".$this->style."' class=' ".$span12." ".$this->class." ".$this->table_class."' >";
			if($this->use_header){
				$content .= "<thead>";
					$content .= $this->getTitle();
					$content .= $this->getHeader();
				$content .= "</thead>";
			}
			$content .= "<tbody>";
				$content .= $this->getBody();
			$content .= "</tbody>";		
			if($this->use_footer){
				$content .= "<tfoot>";
					$content .= $this->getFooter();
				$content .= "</tfoot>";
			}
		$content .= "</table>";
		if($this->is_responsive){
			$ctn  	 = '<div class="smis-table-container table-responsive"><div class="" id="print_table_registration_patient">';
			$ctn 	.= $content;
			$ctn 	.= '</div></div>';
			$content = $ctn;
		}		
		return $content;
	}	
}
?>