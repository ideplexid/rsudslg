<?php

class Panel extends HTML{
    public function getHTML(){
        $result = " <div id='".$this->id."' class='panel ".$this->class."'>
                        <div class='panel-heading'>
                            <h3 class='panel-title'>".$this->name."</h3>
                        </div>
                        <div class='panel-body'>
                            ".$this->getValue()."
                        </div>
                    </div>";
        return $result;
        
    }
}

?>