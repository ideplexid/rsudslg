<?php 
require_once "smis-framework/smis/ui/composite/TableColumn.php";
class TableRows{

	private $column;
	private $column_to_span;
	private $class;
	private $model;
	
	public function __construct($class=""){
		$this->column           = array();
		$this->class            = $class;
		$this->model            = "td";
		$this->column_to_span   = 0;
	}
	
	public function setClass($class){
		$this->class            = $class;
		return $this;
	}
	
	public function addClass($class){
		$this->class           .= " ".$class;
		return $this;
	}
	
	public function setModel($model){
		$this->model            = $model;
		return $this;
	}

	public function setColumnSpan($column){
		$this->column_to_span   = $column;
		return $this;
	}

	public function addElement($element,$cspan,$rspan,$key=null,$class=""){
		$td                     = new TableColumn($element,$cspan,$rspan,$this->class." ".$class,$key);
		if($key==null){
			$this->column[]     = $td;
		}else{
			$this->column[$key] = $td;
		}
		return $this;
	}

	public function removeElement($key){
		if(isset($this->column[$key]))
			unset($this->column[$key]);
		return $this;
	}

	public function getTotalElement(){
		return count($this->column);
	}

	public function getHtml(){
		$content        = "<tr class='smis-row ".$this->class."'>";
		foreach($this->column as $col){
			$col->setModel($this->model);
			$content   .= $col->getHtml();
		}
		$content       .= "</tr>";
		return $content;
	}
}
?>