<?php


class Menu
{


	private $sub;
	private $total;
	private $property;
	private $divider;
	private $capability;
	private $capability_function;
	private $parent_menu;
	private $action;
	private $enable_capabilitiy;
	private $menu_mode;


	public static $TIPE_SEPARATOR = 0;
	public static $TIPE_SUBMENU = 1;
	public static $TIPE_MENU = 2;

	public static $MODE_SIDEBAR = false;
	public static $MODE_TOPBAR = true;


	function __construct($icon = "fa fa-home")
	{
		$this->sub				= array();
		$this->total				= 0;
		$this->divider				= NULL;
		$this->property				= array();
		$this->capability				= null;
		$this->capability_function  		= true;
		$this->enable_capabilitiy   		= true;
		$this->property['class']			= "";
		$this->property['title']			= "";
		$this->property['href']			= "javascript:;";
		$this->property['name']			= "";
		$this->property['page']			= "";
		$this->property['action']			= "";
		$this->property['prototype_name']		= "";
		$this->property['prototype_slug']		= "";
		$this->property['prototype_implement']	= "";
		$this->property['id']			= "";
		$this->property['icon']			= $icon;
		$this->parent_menu			= null;
		$this->action				= null;
		$this->menu_mode				= self::$MODE_TOPBAR;
	}

	public function setMenuMode($mode)
	{
		$this->menu_mode	= $mode;
		return $this;
	}

	public function setCapability($capability)
	{
		$this->capability	= $capability;
		return $this;
	}

	public function setParent($parent)
	{
		$this->parent_menu	= $parent;
		return $this;
	}

	public function setCapabilityFunction($capability_function)
	{
		$this->capability_function	= $capability_function;
		return $this;
	}

	public function setCapabilityEnabled($c)
	{
		$this->enable_capabilitiy	= $c;
		return $this;
	}

	public function setAction($action)
	{
		$this->action	= $action;
		return $this;
	}

	public function addLink($title, $href, $tooltip)
	{
		//sub menu 
		$wordpress		= new Menu();
		$wordpressprop		= array();
		$wordpressprop['title']	= $tooltip;
		$wordpressprop['name']	= $title;
		$wordpressprop['href']	= $href;
		$wordpress->setProperty($wordpressprop);
		$this->addSub($wordpress);
		return $this;
	}


	public function addLaravel($title, $page, $action, $tooltip, $icon)
	{
		//sub menu 
		$wordpress		= new Menu();
		$wordpress->addProperty("title", $tooltip);
		$wordpress->addProperty("name", $title);
		$wordpress->addProperty("href", get_laravel() . "/auth_from_simrs?session=" . getSession('session') . "&&page=" . $page . "&&action=" . $action);
		$wordpress->addProperty("icon", $icon);

		$wordpress->addProperty("class", "laravel");
		$wordpress->addProperty("prototype_name", "");
		$wordpress->addProperty("prototype_slug", "");
		$wordpress->addProperty("prototype_implement", "");
		$wordpress->addProperty("page", $page);
		$wordpress->addProperty("action", $action);
		$this->addSub($wordpress);
		return $this;
	}

	public function addSubMenu($title, $page, $action, $tooltip, $icon = "fa fa-home", $protoype_name = "", $protoype_slug = "", $protoype_implement = "", $href = "javascript:;")
	{
		//Sub Menu Settings
		$submenu = new Menu();
		$submenu->addProperty('title', $tooltip);
		$submenu->addProperty('name', $title);
		$submenu->addProperty('href', $href);
		$submenu->addProperty('page', $page);
		$submenu->addProperty('action', $action);
		$submenu->addProperty('class', 'smis_action');
		$submenu->addProperty("icon", $icon);
		$submenu->addProperty('prototype_name', $protoype_name);
		$submenu->addProperty('prototype_slug', $protoype_slug);
		$submenu->addProperty('prototype_implement', $protoype_implement);
		$this->addSub($submenu);
		return $this;
	}



	public function getName()
	{
		if (isset($this->property['name']))
			return $this->property['name'];
		return null;
	}

	public function getPrototypeImplement()
	{
		if (isset($this->property['prototype_implement']))
			return $this->property['prototype_implement'];
		return "";
	}

	public function getHref()
	{
		if (isset($this->property['href']))
			return $this->property['href'];
		return "";
	}

	public function getPage()
	{
		if (isset($this->property['page']))
			return $this->property['page'];
		return null;
	}

	public function getDescription()
	{
		if (isset($this->property['title']))
			return $this->property['title'];
		return null;
	}

	public function getAction()
	{
		if (isset($this->property['action']))
			return $this->property['action'];
		return null;
	}

	/**
	 * this is the menu that want to be include
	 * @param Menu $menu
	 */
	function addSub(Menu $menu)
	{
		if (!isset($this->sub[$menu->getAction()])) {
			$this->total = $this->total + 1;
		}
		$this->sub[$menu->getAction()] = $menu;
		$menu->setParent($this);
		return $this;
	}

	public function getSubMenu()
	{
		return $this->sub;
	}

	/**
	 * this is set the properti including
	 * id, href, class, page, action, title, name
	 * @param array that contains $properti
	 */
	function setProperty($property)
	{
		$this->property = $property;
		return $this;
	}

	public function addProperty($slug, $value)
	{
		$this->property[$slug] = $value;
		return $this;
	}

	public function getProperty($slug)
	{
		if (isset($this->property[$slug]))
			return $this->property[$slug];
		else return "";
	}

	public function getComponent()
	{
		$menus = array();
		foreach ($this->sub as $menu) {
			$name = $menu->getName();
			$number = count($menus);
			if ($name != null || $name != "")
				$menus[$number] = $menu;
		}
		return $menus;
	}

	public function getType()
	{
		if ($this->property == NULL)
			return self::$TIPE_SEPARATOR;
		if ($this->total == 0)
			return self::$TIPE_SUBMENU;
		return self::$TIPE_MENU;
	}

	public function is_capable($settings)
	{
		if (!$this->capability_function)
			return true;

		if ($this->property == NULL) //divide
			return false;

		if ($this->total == 0 && $settings != null && is_array($settings) && isset($settings[$this->getAction()])) { //sub menu
			$var = $settings[$this->getAction()];
			if ($var == 1 || $var == "1")
				return true;
		} else { //menu			
			foreach ($this->sub as $menu) {
				$result = $menu->is_capable($settings);
				if ($result) return true;
			}
		}
		return false;
	}

	public function getClass()
	{
		if (isset($this->property['class']))
			return $this->property['class'];
		return null;
	}

	public function getHtml()
	{
		if ($this->menu_mode == self::$MODE_TOPBAR) {
			return $this->getTopBarMode();
		} else if ($this->menu_mode == self::$MODE_SIDEBAR) {
			return $this->getSidebarMode();
		}
	}

	public function getTopBarMode()
	{

		$content = "";
		$capable = false;
		if ($this->property == NULL) {
			$content .= "<li class='divider' ></li>";
			return $content;
		} else if ($this->enable_capabilitiy && !$this->is_capable($this->capability)) {
			return "";
		}

		if ($this->total == 0 || $this->getProperty('href') != "javascript:;") {
			/*this is a single Menu */
			$content .= "<li class='menu-item' >";
			$content .= "	<a 	title='" . $this->property['title'] . "'
								href='" . $this->getProperty('href') . "'
								class='" . $this->getProperty('class') . "'
								page='" . $this->getProperty('page') . "'
								prototype_name='" . $this->getProperty('prototype_name') . "'
								prototype_slug='" . $this->getProperty('prototype_slug') . "'
								prototype_implement='" . $this->getProperty('prototype_implement') . "'
								action='" . $this->getProperty('action') . "'
								" . ($this->action == null ? "" : "onclick=\"" . $this->action . "\"") . "
										>
								<i class='" . $this->getProperty('icon') . "'></i> " . $this->getProperty('name') . "
							</a>";
			$content .= "</li>";
		} else {
			/*this is dropdown menu*/
			$drsubmenu = $this->parent_menu != null ? "dropdown-submenu" : "";
			$content .= "<li id='" . $this->getProperty('id') . "' class='menu-item  dropdown  $drsubmenu  " . $this->getProperty('class') . "' >";
			$content .= "	<a title='" . $this->getProperty('title') . "' href='#' data-toggle='dropdown' class='dropdown-toggle " . $this->getProperty('class') . "'>
								<i class='" . $this->getProperty('icon') . "'></i> " . $this->getProperty('name') . ($this->parent_menu == null ? "  <span class='caret'></span>" : "") .
				"</a>";
			$content .= "<ul role='menu' class='dropdown-menu'>";
			$last_type_print = -1;
			foreach ($this->sub as $menu) {
				if ($last_type_print == self::$TIPE_SEPARATOR && $menu->getType() == self::$TIPE_SEPARATOR)
					continue;
				$menu->setCapability($this->capability);
				$menu->setCapabilityFunction($this->capability_function);
				$last_content = $menu->getHtml();
				if ($last_content != "") {
					$last_type_print = $menu->getType();
				}
				$content .= $last_content;
			}
			$content .= "</ul>";
			$content .= "</li>";
		}
		return $content;
	}

	public function getSidebarMode()
	{
		$content = "";
		$capable = false;
		if ($this->property == NULL) {
			return "";
		} else if ($this->enable_capabilitiy && !$this->is_capable($this->capability)) {
			return "";
		}
		if ($this->total == 0 || $this->getProperty('href') != "javascript:;") {
			/*this is a single Menu */

			$id = $this->getProperty('page') . "__" . $this->getProperty('action');

			$content .= "<li >";
			$content .= "	<a 	title='" . $this->property['title'] . "'
								id='" . $id . "'
								href='" . $this->getProperty('href') . "'
								class='" . $this->getProperty('class') . "'
								page='" . $this->getProperty('page') . "'
								prototype_name='" . $this->getProperty('prototype_name') . "'
								prototype_slug='" . $this->getProperty('prototype_slug') . "'
								prototype_implement='" . $this->getProperty('prototype_implement') . "'
								action='" . $this->getProperty('action') . "'
								" . ($this->action == null ? "" : "onclick=\"" . $this->action . "\"") . "
										>
								<i class='" . $this->getProperty('icon') . "'></i> " . $this->getProperty('name') . "
							</a>";
			$content .= "</li>";
		} else {
			/*this is dropdown menu*/
			$drsubmenu = $this->parent_menu != null ? "dropdown-submenu" : "";
			$content .= "<li  id='" . $this->getProperty('id') . "' 
						class=' " . $this->getProperty('class') . "' >";

			$menu_slug = md5($this->getProperty("name"));

			$content .= "	<a 	title='" . $this->getProperty('title') . "' 
							href='" . $this->getProperty('href') . "' 
							data-toggle='collapse' 
							class=' collapsed " . $this->getProperty('class') . "' 
							data-target='#" . $menu_slug . "_child'		
							>
								<i class='" . $this->getProperty('icon') . " '></i> 
								" . $this->getProperty('name') .
				"</a>";
			$content .= "<ul role='menu' id='" . $menu_slug . "_child' class='collapse'>";
			$last_type_print = -1;
			foreach ($this->sub as $menu) {
				if ($last_type_print == self::$TIPE_SEPARATOR && $menu->getType() == self::$TIPE_SEPARATOR)
					continue;
				$menu->setMenuMode($this->menu_mode);
				$menu->setCapability($this->capability);
				$menu->setCapabilityFunction($this->capability_function);
				$last_content = $menu->getHtml();
				if ($last_content != "") {
					$last_type_print = $menu->getType();
				}
				$content .= $last_content;
			}
			$content .= "</ul>";
			$content .= "</li>";
		}
		return $content;
	}


	function addSeparator()
	{
		if ($this->divider == NULL) {
			$this->divider = new Menu();
			$this->divider->setProperty(NULL);
		}
		$this->addSub($this->divider);
		return $this;
	}
}
