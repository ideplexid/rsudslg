<?php
/**
 * This Class is used for creating Modal in the System there will be 3 Main Component 
 * <ol>
 * <li> Modal Header : Contain Title and Close Button Only </li>
 * <li> Modal Body	: Contain 5 Child </li>
 * 		<ul>
 * 		<li>  Modal Body Alert -> this is the alert will be show when user put wrong input</li>
 * 		<li>  Modal Body Title -> This is the Title that will be used for showing some little information</li>
 * 		<li>  Modal Body Component -> this is used to view the Component of the Modal, it could be Textarea, Text, Select , Checkbox including the Label (Responsive)</li>
 * 		<li>  Modal Body Element -> this is for showing SMIS Other Element like Another Modal, Table or Another Composite UI </li>
 * 		</ul>
 * <li> Modal Footer  : Contain the footer that usually contain a button for action purpose </li>
 * </ol>
 *  
 * @version 1.0.0
 * @since 15 Jan 2014
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 * @author Nurul Huda <goblooge@gmail.com>
 * @copyright Copyright 2014, Nurul Huda 
 * @link http://www.goblooge.com 
 * 
 */
class Modal{
	
	protected $id;
	protected $class;
	protected $title;
	protected $footer;
	protected $form;
	protected $body;
	protected $always_show;
	protected $modal_close;
	protected $component_size;
	protected $modal_size;
	protected $html_after_form;
	protected $html_before_form;
	
	
	
	public static $FULL_MODEL="full_model";
	public static $NORMAL_MODEL="normal_model";
	public static $HALF_MODEL="half_model";
	
	public static $MODAL_CLOSE_JS=true;
	public static $MODAL_CLOSE_BOOTSTRAP=false;
	
	public static $BIG="big_component";
	public static $MEDIUM="medium_component";
	public static $SMALL="small_component";
	
	
	
	/**
	 * Here the Example :
	 * <pre>
	 * 	$m=new Modal("user","user","User Experience");
	 * 	echo $m->getHtml();
	 * </pre>
	 * @param string $id the id of this Modal
	 * @param string $class class tag of this Modal
	 * @param string $title the title of this Modal
	 */	
	public function __construct($id,$class,$title){
		$this->id=$id;
		$this->class=$class;
		$this->title=$title;
		$this->form=new Form($id."_form", "","");
		$this->always_show=false;
		$this->component_size=self::$SMALL;
		$this->modal_size=self::$NORMAL_MODEL;
		$this->html_after_form=array();
		$this->html_before_form=array();
		$this->modal_close=self::$MODAL_CLOSE_JS;
	}
	
	
	public function setModalSize($size){
		$this->addClass($size);
		$this->modal_size=$size;
		return $this;
	}
	
	public function setModalClose($close){
		$this->modal_close=$close;
		return $this;
	}
	
	public function setComponentSize($size){
		$this->component_size=$size;
		return $this;
	}
	
	
	public function setClass($class){
		$this->setModalSize($class);
		$this->class=$class;
		return $this;
	}
	
	public function addClass($class){
		$this->class.=" ".$class;
		return $this;
	}
	
	
	public function setTitle($title){
		$this->title=$title;
		return $this;
	}
	
	public function setElement($element){
		$this->form->setElement($element);
		return $this;
	}
	
	public function getSize(){
		return $this->form->getSize();
	}
	
	public function addHTML($html,$position="after"){
		if($position=="after") $this->html_after_form[]=$html;
		else $this->html_before_form[]=$html;
		return $this;
	}
	
	public function addElement($name,$html,$class=""){
		$this->form->addElement($name, $html,$class);
		return $this;
	}
	
	/** 
	 * this function is used for adding element to Modal
	 * @param string $type the HTML element tag of this element : text, textarea, select,checkbox, label , file, chooser, summernote etc
	 * @param string $value the default value of this element
	 * @param string $name name of this element (aka Label) that will show o user
	 * @param string $column the column name in the database that use to save this element
	 * @param string $tipical for type text only the tipical of this value. it could be free, numeric, alphanumeric, money and date . it will trigger warning or error user not insert according to this tipical 
	 * @param string $empty could it be emty ? if not, warning would trigger if user leave it blank
	 * @param string $disabled should it disabled when it printed
	 * @param string $option if not null, there will be an button that appear in side of the element that riggering more_option.
	 */
	
	public function addModalElement($type,$value,$name,$column,$tipical,$empty,$disabled=false,$option=null,$autofocus=false,$next_enter=NULL){
		$c=$this->title."_".$column;
		$element=ComponentFactory::createComponent($c,$this->id,$column,$this->title,$type,$value,$name,$tipical,$empty,$disabled,$option,$autofocus,$next_enter);
		$this->addElement($name, $element,"fcontainer_".$c);
		return $this;
	}
	
	
	
	/**
	 * usually contain a button
	 * @param unknown_type $footer
	 */
	public function addFooter($footer){
		if($this->footer==null || count($this->footer)==0){
			$this->footer=array();
		}
		$this->footer[]=$footer;
		return $this;
	}
	
	public function clearFooter(){
		$this->footer=array();	
		return $this;
	}
	
	public function setAlwaysShow($show){
		$this->always_show=$show;
		return $this;
	}
	
	public function addBody($name,$element){
		if($this->body==null || $this->body=="" || !is_array($this->body))
			$this->body=array();
		
		$number=count($this->body);
		$this->body[$number]=$element;
		return $this;
	}
	
	public function getBodyElements(){
		if($this->body==null || $this->body=="" || !is_array($this->body))
			return "";
		$content="";
		foreach($this->body as $name=>$element){
			$content.="<div class=' ".$this->id."_".$name."  ' >".$element->getHtml()."</div>";
		}
		return $content;
	}
	
	public function getHtmlComponent(){
		return implode("", $this->html_after_form);
	}
	
	public function getHtmlBeforeComponent(){
		return implode("", $this->html_before_form);
	}
	
	public function getFormHtml(){
		return $this->form->getHtml();
	}
	
	public function getForm(){
		return $this->form;
	}
	
	public function getFooter(){
		return $this->footer;
	}
	
	public function joinFooterAndForm(){
		$nform=new Form($this->id."_form", "","");
		foreach($this->form->getElement() as $element){
			$nform->addElement($element['name'], $element['html'],$element['class']);
		}
		
		foreach($this->footer as $f){
			$nform->addElement("", $f);
		}
		return $nform;		
	}
	
	public function setFooter($footer){
		$this->footer=$footer;
		return $this;
	}
	
	public function getFooterHtml(){
		$content="";
		if($this->footer!=null){
			foreach($this->footer as $f){
				$content.=$f->getHtml();
			}
		}
		return $content;
	}
	
	public function getModalHeader(){
		$content="";
		if($this->always_show){
			$content.="<div tabindex='-1' data-backdrop='static' data-keyboard='true' class=' modal modal_always_show ".$this->class." ".$this->modal_size." '  id='".$this->id."' >";
			$content.="<div class='modal-header'> <h3>".$this->title."</h3> </div>";
		}else if($this->modal_close==self::$MODAL_CLOSE_JS){
			$content.="<div tabindex='-1' data-backdrop='static' data-keyboard='true' class='modal fade hide ".$this->class."' id='".$this->id."' >";
			$content.="<div class='modal-header'> <a class='close' onclick=\"$($(this).data('target')).smodal('hide')\" data-target='#".$this->id."'><i class='fa fa-remove fa-lg'></i></a>  <h3>".$this->title."</h3> </div>";
		}else{
			$content.="<div tabindex='-1' data-backdrop='static' data-keyboard='true' class='modal fade hide ".$this->class."' id='".$this->id."' >";
			$content.="<div class='modal-header'> <a class='close' data-toggle='modal' data-target='#".$this->id."'><i class='fa fa-remove fa-lg'></i></a> <h3>".$this->title."</h3> </div>";
		}
		return $content;
	}
	
	public function getHtml(){
		$content=$this->getModalHeader();
		$class=$this->modal_size."_".$this->component_size;
		$this->form->setComponentClass($class);
		
			$content.="<div class='modal-body'>";
				$content.="<div id='modal_alert_".$this->id."' class='modal-body-alert'></div>";
				$content.="<div class='modal-body-title'>".$this->getHtmlBeforeComponent()."</div>";
				$content.="<div class='modal-body-form ".$this->component_size." '>".$this->getFormHtml()."</div>";
				$content.="<div class='modal-body-element'>".$this->getBodyElements()."</div>";
				$content.="<div class='modal-body-html'>".$this->getHtmlComponent()."</div>";
			$content.="</div>";
			$content.="<div class='modal-footer'>";
				$content.=$this->getFooterHtml();
			$content.="</div>";
			
		$content.="</div>";
		return $content;			
	}
	
	
	public function getModalSkeleton($include_footer=true){
		if($include_footer){
			$nform=$this->joinFooterAndForm();
			$this->form=$nform;
		}
		$content="<div>";
			$content.="<div id='modal_alert_".$this->id."' class='modal-body-alert'></div>";
			$content.="<div class='modal-body-title'>".$this->getHtmlBeforeComponent()."</div>";
			$content.="<div class='modal-body-form ".$this->component_size." '>".$this->getFormHtml()."</div>";
			$content.="<div class='modal-body-element'>".$this->getBodyElements()."</div>";
			$content.="<div class='modal-body-html'>".$this->getHtmlComponent()."</div>";
		$content.="</div>";
		return $content;		
	}
}
?>
