<?php 
class Tabulator{
	protected $id;
	protected $title;
	protected $content;
	protected $type;
	protected $id_tabs;
	protected $orientation;
	protected $class_tabs;
	protected $icon;
	protected $page;
	protected $action;
	protected $onclick;
	
	private $flip_width;
	private $flip_height;
	
	
	public static $TYPE_HTML="html";
	public static $TYPE_INCLUDE="include";
	public static $TYPE_TEMPLATE="template";
	public static $TYPE_COMPONENT="component";
	
	public static $POTRAIT="tabs-potrait";
	public static $LANDSCAPE="tabs-right";
	public static $LANDSCAPE_LEFT="tabs-left";
	public static $LANDSCAPE_RIGHT="tabs-right";
	public static $ACCORDION="accordion";
	public static $BOOK_BASIC="book-basic";
	public $partial_load_enabled;
	public $reload_once;
	public $first_page;
	public $page_name;
	
	
	
	public function __construct($id,$class,$orientation="tabs-potrait"){
		$this->id=array();
		$this->title=array();
		$this->content=array();
		$this->type=array();
		$this->onclick=array();
		$this->id_tabs=$id;
		$this->class_tabs=$class;
		$this->orientation=$orientation;
		$this->icon=array();
		$this->flip_height=500;
		$this->flip_width=1000;
		$this->partial_load_enabled=false;
		$this->page="";
		$this->first_page="";
		$this->action="";
		$this->reload_once=false;
		$this->page_name="";
	}
	
	public function setBookDimension($w,$h){
		$this->flip_width=$w;
		$this->flip_height=$h;
		return $this;
	}
	
	public function getID(){
		return "";
	}
	
	public function getTotalElement(){
		return count($this->id);
	}
	
	public function setOrientation($mode){
		$this->orientation=$mode;
		return $this;
	}
	
	public function add($id,$title,$content,$type="html",$icon="",$onclik=""){
		$this->id[]=$id;
		$this->title[]=$title;
		$this->content[]=$content;
		$this->type[]=$type;
		$this->icon[]=$icon;
		$this->onclick[]=$onclik;
		return $this;
	}
	
	public function getBookBasic(){
		$result="<div class='basicbook-viewport'>";
			$result.="<div class='basicbook-container'>";
				$result.='<div class="basicbook" id="'.$this->id_tabs.'">';
					$number=0;
					foreach ($this->id as $id){
						$type=$this->type[$number];
						$ct=$this->content[$number];
						$content=self::getComponent($type, $ct);
						$result.="<div class='page' id='".$id."'>".$content."</div>";
						$number++;
					}
				$result.='</div>';
			$result.="</div>";
		$result.="</div>";
		return $result.$this->getBookBasicJS();
	}
	
	public function getBookBasicJS(){
		$js="<script type=\"text/javascript\">
			$(document).ready(function(){
				$('#".$this->id_tabs."').turn({
					width:".$this->flip_width.",
					height:".$this->flip_height.",
					elevation: 10,
					gradients: true,		
					autoCenter: true
				});
			});
			</script>";
		return $js;
	}
	
	public function getAccordion(){
		$result="<div id='".$this->id_tabs."' class='accordion ".$this->class_tabs." smis-tabs ".$this->orientation." ' >";
			$number=0;		
			foreach ($this->id as $id){
				$type=$this->type[$number];
				$ct=$this->content[$number];
				$content=self::getComponent($type, $ct);
				$title=$this->title[$number];
				$number++;
				$accordion_group="<div class='accordion-group'>";
					//HEADER
					$accordion_group.="<div class='accordion-heading'>";
						$accordion_group.='<a id="'.$id.'_anchor" class="accordion-toggle" data-toggle="collapse" data-parent="#'.$this->id_tabs.'" href="#'.$this->id_tabs.'_'.$id.'">'.$this->getIcon($number)." ".$title.'</a>';
					$accordion_group.="</div>";
					//CONTENT					
					$accordion_group.='<div id="'.$this->id_tabs.'_'.$id.'" class="accordion-body collapse">';
						$accordion_group.='<div class="accordion-inner">';
							$accordion_group.=$content;
						$accordion_group.='</div>';
					$accordion_group.='</div>';					
				$accordion_group.="</div>";
				$result.=$accordion_group;
			}
		$result.="</div>";
		return $result;
	}
	
	private function getIcon($number){
		if(isset($this->icon[$number]) && $this->icon[$number]!="" ){
			return "<i class='".$this->icon[$number]."'></i>";
		}
		return "";
	}
	
	public function getTabulator(){
		$result="<div id='".$this->id_tabs."_header' class='tabbable ".$this->class_tabs." smis-tabs ".$this->orientation." ' >";
			$result.="<ul class='nav nav-tabs ".$this->class_tabs." smis-tabs-header'>";
			$number=0;
			foreach ($this->id as $id){
				$result.="<li ".($number==0?"class='active'":"")." ><a id=\"".$id."_anchor\" href='#".$id."' data-toggle='tab' onclick=\"".$this->onclick[$number]."\">".$this->getIcon($number)." ".$this->title[$number]."</a></li>";
				$number++;
			}
			$result.="</ul>";
		
			$result.="<div class='tab-content ".$this->class_tabs." smis-tabs-content'>";
			$number=0;
			foreach ($this->id as $id){
				$type=$this->type[$number];
				$ct=$this->content[$number];
				$content=self::getComponent($type, $ct);
				$result.="<div class='tab-pane ".($number==0?"active":"")."' id='".$id."'>".$content."</div>";
				$number++;
			}
			$result.="</div>";
		$result.="</div>";
		return $result;
	}
	
	private static function getComponent($type,$content){
		if($type==self::$TYPE_HTML){
			return $content;
		}else if($type==self::$TYPE_INCLUDE){
			ob_start();
			require_once($content);
			$ct = ob_get_clean();
			return $ct;
		}else if($type==self::$TYPE_TEMPLATE){
			ob_start();
			$content->initialize();
			$ct = ob_get_clean();
			return $ct;
		}else if($type==self::$TYPE_COMPONENT){
			$ct=$content->getHtml();
			return $ct; 
		}
	}
	
	public function setPartialLoad($enabled,$page,$action,$first_page,$reload_once=false){
		$this->partial_load_enabled=$enabled;
		$this->page=$page;
		$this->first_page=$first_page;
		$this->action=$action;
		$this->reload_once=$reload_once;
		$this->page_name=$page;
	}
	
	public function setPageName($page){
		$this->page_name=$page;
	}
	
	public function getPartialLoadJavascript(){
		?>
		<script type="text/javascript">	
			var RELOAD_ONCE_<?php echo $this->page_name."_".$this->action; ?>=<?php echo (($this->reload_once)?"true":"false");  ?>;
			var RELOAD_ONCE_ARRAY_<?php echo $this->page_name."_".$this->action; ?>=new Array();
			var GET_LOAD_DATA_<?php echo $this->page_name."_".$this->action; ?>;
			GET_LOAD_DATA_<?php echo $this->page_name."_".$this->action; ?>=function(file){
				var data={
							page:"<?php echo $this->page; ?>",
							action:file,
							prototype_implement:"",
							prototype_slug:"",
							prototype_name:""
						};
				return data;
			};
			
			function loadFirstPagePV_<?php echo $this->page_name."_".$this->action; ?>(name){
				$("#"+name+"_header > ul > li.active a ").trigger( "click" );
			}	
			loadFirstPagePV_<?php echo $this->page_name."_".$this->action; ?>("<?php echo $this->first_page;?>");				
			function loadPagePV_<?php echo $this->page_name."_".$this->action; ?>(file,ref){

				if(RELOAD_ONCE_<?php echo $this->page_name."_".$this->action; ?>){
					if($.inArray(file, RELOAD_ONCE_ARRAY_<?php echo $this->page_name."_".$this->action; ?>)!=-1){
						return;
					}else{
						RELOAD_ONCE_ARRAY_<?php echo $this->page_name."_".$this->action; ?>.push(file);
					}
				}
				
				var data=GET_LOAD_DATA_<?php echo $this->page_name."_".$this->action; ?>(file);
				loadPartialTab_<?php echo $this->page_name."_".$this->action; ?>(data,ref);
			}

			
            
            function loadPartialTab_<?php echo $this->page_name."_".$this->action; ?>(data,ref){
                showLoading();
				$.post('',data,function(res){
					try {
					    var dx = JSON.parse(res);
					    getContent(dx);
					    return;
					} catch(e) {
						try{
							$("#"+ref).html(res);
							var idx = data["action"]+"_anchor";
							if($("#"+idx).parent().parent().parent().hasClass("tabs-right")){
								//window.location.hash = idx;
								scrolToAnimate($('#smis_container'), 100);
							}
							dismissLoading();
						}catch (e) {
							dismissLoading();							
							var rres=replaceAll("<","&lt;",res);
							rres=replaceAll(">","&gt;",rres);							
							var problem="";
							problem+=" <label class='label label-warning'> Javascript  </label> ";
							problem+="<pre>"+e.stack+"</pre>";
							problem+="</br>";
							problem+="</br>";
							problem+=" <label class='label label-info'> Server  </label> ";
							problem+="<pre>"+rres+"</pre>";
							
							showFullWarning("Error Javascript Loading Page",problem,"full_model");
						}finally{
							if($("#smis_window_wrapper").is(":hidden")){
								$("#smis_window_wrapper").show("fast");
							}				
						}
					}		
				});
            }
		</script>		
		<?php 
	}
	
	public function getHtml(){
		$result="";
		if($this->partial_load_enabled){
			$result.=$this->getPartialLoadJavascript();
            $number=0;
			foreach($this->id as $id){
                if( !isset($this->onclick[$number])|| $this->onclick[$number]==""){
                    $this->onclick[$number]="loadPagePV_".$this->page_name."_".$this->action."('".$id."','".$id."');";
                }
                $number++;
			}
		}
		if($this->orientation==self::$ACCORDION) {
			$result.=$this->getAccordion();			
		}else if($this->orientation==self::$BOOK_BASIC){
			$result.=$this->getBookBasic();
		}else {
			$result.=$this->getTabulator();
		}
		return $result;
	}
	
}
?>