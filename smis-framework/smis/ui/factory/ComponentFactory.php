<?php 

class ComponentFactory{
	/**
	 * this function is used for adding element to Modal
	 * @param string $type the HTML element tag of this element : text, textarea, select,checkbox, label , file, chooser, summernote etc
	 * @param string $value the default value of this element
	 * @param string $name name of this element (aka Label) that will show o user
	 * @param string $column the column name in the database that use to save this element
	 * @param string $tipical for type text only the tipical of this value. it could be free, numeric, alphanumeric, money and date . it will trigger warning or error user not insert according to this tipical
	 * @param string $empty could it be emty ? if not, warning would trigger if user leave it blank
	 * @param string $disabled should it disabled when it printed
	 * @param string $option if not null, there will be an button that appear in side of the element that riggering more_option.
	 */
	
	public static function createComponent($id,$parent_id,$column,$title,$type,$value,$name,$tipical="",$empty="y",$disabled=false,$option=null,$autofocus=false,$next_enter=NULL){
		$c=$id;
		$element=null;
		if($type=='html'){
			$element=new HTML($c, $name, $value);
		}else if($type=='hidden'){
			$element=new Hidden($c, $name, $value);
		}else if($type=='password'){
			$element=new Password($c, $name, $value);
		}else if(in_array($type, Text::$ARRAY_MODEL)){
			$element=new Text($c, $name, $value);
			$element->setModel($type);
		}else if($type=='multiple-select'){
			$element=new Select($c, $name, $value);
			$element->setMultiple(true);
		}else if($type=='select'){
			$element=new Select($c, $name, $value);
		}else if($type=='textarea'){
			$element=new TextArea($c, $name, $value);
		}else if($type=='checkbox'){
			$element=new CheckBox($c, $name, $value);
		}else if(strpos($type,'checkbox') !== false){
			$cek_name=str_replace("checkbox-", "", $type);
			$element=new CheckBox($c, $cek_name, $value);
		}else if($type=='label'){
			$element=new Label($c, $name, $value);
		}else if($type=='summernote'){
			$btn=new Button($title."_edit_".$column,"", "Write");
			$btn->setAction("smis_summernote_write('".$title."','".$c."','".$parent_id."')");
			$btn->setIsButton(Button::$ICONIC);
			$btn->setIcon("icon-black ".Button::$icon_pencil);
				
			$btns=new Button($title."_show_".$column,"", "Show");
			$btns->setClass("btn-inverse");
			$btns->setAction("smis_summernote_show('".$title."','".$c."','".$parent_id."')");
			$btns->setIsButton(Button::$ICONIC);
			$btns->setIcon("icon-white ".Button::$icon_eye_open);		
				
			$inp=new TextArea($c, $name, $value);
			$inp->setClass("hide");
			$element=new ButtonGroup("summernote-buton-group noprint");
			$element->addButton($btn);
			$element->addElement($inp);
			$element->addButton($btns);
			
		}else if($type=='summernote-print'){
			$btn=new Button($title."_edit_".$column,"", "Write");
			$btn->setAction("smis_summernote_write('".$title."','".$c."','".$parent_id."')");
			$btn->setIsButton(Button::$ICONIC);
			$btn->setClass("btn-warning");
			$btn->setIcon("icon-black ".Button::$icon_pencil);
				
			$btns=new Button($title."_show_".$column,"", "Show");
			$btns->setClass("btn-inverse");
			$btns->setAction("smis_summernote_show('".$title."','".$c."','".$parent_id."')");
			$btns->setIsButton(Button::$ICONIC);
			$btns->setIcon("icon-white ".Button::$icon_eye_open);
		
			$print=new Button($title."_print_".$column,"", "Print");
			$print->setClass("btn-info");
			$print->setAction("smis_summernote_print('".$title."','".$c."','".$parent_id."')");
			$print->setIsButton(Button::$ICONIC);
			$print->setIcon("icon-white ".Button::$icon_print);		
				
			$inp=new TextArea($c, $name, $value);
			$inp->setClass("hide");
			$element=new ButtonGroup("summernote-buton-group noprint");
			$element->addButton($btn);
			$element->addElement($inp);
			$element->addButton($btns);
			$element->addButton($print);
			$element->setMax(1000, "");
		}else if(startsWith($type,'file') !== false){
			$model='multiple';
			$filetype="all";
			$split=explode("-",$type);
			if(isset($split[1])){
				$model=$split[1];
			}
			if(isset($split[1])){
				$filetype=$split[2];
			}
			$btn=new Button("","", "");
			$btn->setAction("uploadfile('".$title."','".$c."','".$model."','".$filetype."')");
				
			if($model=='multiple') $btn->setClass("btn-primary");
			else $btn->setClass("btn-info");
			$btn->setIsButton(Button::$ICONIC);
				
			$icon="fa ";
			switch ($filetype){
				case "image" : $icon.="fa-file-image-o"; break;
				case "video" : $icon.="fa-file-video-o"; break;
				case "audio" : $icon.="fa-file-audio-o"; break;
				case "archive" : $icon.="fa-file-archive-o"; break;
				case "code" : $icon.="fa-file-code-o"; break;
				case "document" : $icon.="fa-file-text-o"; break;
				default : $icon.="fa-file-o"; break;
			}
				
			$content="Upload ".$model.($model=="single"?" File":" Files")." of ".($filetype=="all"?"any ":ucfirst($filetype))." filetype";
			$btn->addAtribute("data-content", $content);
			$btn->addAtribute("data-toggle", "popover");
			$btn->addAtribute("data-placement", "top");
			$btn->setIcon($icon);
            $inp=new Text($c, $name, $value);
			$inp->setDisabled($disabled);
			$inp->setClass("smis-one-option-input");
				
			$element=new InputGroup("");
			$element->addComponent($inp);
			$element->addComponent($btn);
		}else if(startsWith($type,'draw') !== false){
			$split=explode("-",$type);
			if($split[1]=="component"){
                $idx         = substr($type,15);
                $action_name = "_sdraw.showDrawingID('#".$idx."','#".$c."');";
            }else{
                $filename    = substr($type,9);
                $action_name = "_sdraw.showDrawing('".$filename."','#".$c."');";
            }
            
            $btn = new Button("","",$name);
            $btn->setIsButton(Button::$ICONIC_TEXT);
            $btn->setAction($action_name);
            $btn->addAtribute("data-content", "Image Editor");
			$btn->addAtribute("data-toggle", "popover");
			$btn->addAtribute("data-placement", "top");
			$btn->setIcon("fa fa-paint-brush");
            $btn->setClass("btn-primary");
			$inp = new Hidden($c, $name, $value);
			$inp->setDisabled($disabled);
            $element = new InputGroup("");
			$element ->addComponent($inp);
			$element ->addComponent($btn);
		}else if(startsWith($type,'gps') !== false){
			$split 		 = explode("-",$type);
			$action      = $split[1];
			$lat   		 = $split[2];
			$lon   		 = $split[3];
			$action_name = "_sgps.show(".$action.",'".$lat."','".$lon."');";
			
            $btn = new Button("","",$name);
            $btn->setIsButton(Button::$ICONIC_TEXT);
            $btn->setAction($action_name);
            $btn->addAtribute("data-content", "GPS Picker");
			$btn->addAtribute("data-toggle", "popover");
			$btn->addAtribute("data-placement", "top");
			$btn->setIcon("fa fa-paint-brush");
            $btn->setClass("btn-primary");
            $element = $btn;
		}else if(strpos($type,'chooser') !== false){
			$split	= explode("-",$type);
			$action	= "";
			$param	= "";
			$judul	= "";
			if(isset($split[1])){
				$action=$split[1];
			}
			if(isset($split[2])){
				$param = $split[2];
				$judul = $param;
			}
			
			if(isset($split[3])){
				$judul=$split[3];
			}
			
			$btn=new Button($title."_chooser_".$column,"", "");
			$btn->setClass("btn-primary");
			$btn->setIsButton(Button::$ICONIC);
			$btn->setIcon("icon-list-alt icon-white");
			$btn->setAction($action.".chooser('".$title."','".$c."','".$param."',".$param.",'".$judul."')");
			$inp=new Text($c, $name, $value);
			$inp->setDisabled($disabled);
			$inp->setClass("smis-one-option-input");
			if($next_enter!=NULL ) {
				$inp->addAtribute("data-next-enter",$next_enter);
			}
			if($autofocus ) {
				$inp->addAtribute("autofocus");
			}
			$element=new InputGroup("");
			$element->addComponent($inp);
			$element->addComponent($btn);			
		}
		if(($type=='text' || $type=='textarea' || $type=='date' || $type=='hidden' ) && $tipical!=null){
			$element->setTypical($tipical);
		}
		if($empty!=null && ($type=='text' || $type=='textarea' || $type=='date' || $type=='select' || $type=='money')){
			$element->setEmpty($empty);
		}
		
		if($option!=null) {
			$btn_more = new Button("", "", "");
			$btn_more->setIsButton(Button::$ICONIC);
			$btn_more->setIcon("icon-share");
			$btn_more->setAction($option.".more_option('".$c."')");
				
			if(is_a($element, "InputGroup")){
				$elmt=$element->getElement(0);
				$elmt->setClass("smis-two-option-input");
				$element->addComponent($btn_more);
			}else{
				$telement=$element;
				$telement->addClass("smis-two-option-input");
				$element=new InputGroup("");
				$element->addComponent($telement);
				$element->addComponent($btn_more);
			}
		}
		$element->setDisabled($disabled);
		if($autofocus && is_a($element, 'HTML')) {
			$element->addAtribute("autofocus");
		}
		if($next_enter!=NULL && is_a($element, 'HTML')) {
			$element->addAtribute("data-next-enter",$next_enter);
		}	
		return $element;
	}
}

?>