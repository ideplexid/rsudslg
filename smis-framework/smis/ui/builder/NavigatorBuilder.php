<?php 

abstract class NavigatorBuilder{
	
	private $navigation;
	public function __construct($navigation){
		$this->navigation=$navigation;
		$this->navigation=new Navigator();
	}
	
	public function addAdminMenu($menu){
		$menu_admin=$this->navigation->getMenu('smis-administrator');
		$menu_admin->addSub($menu);
		return $this;
	}
	
	public function addAdminSeparator(){
		$menu_admin=$this->navigation->getMenu('smis-administrator');
		$menu_admin->addSeparator();
		return $this;
	}
	
	public function addAdminLink($tooltip,$name,$href){
		$m=new Menu();
		$prop=array();
		$prop['title']=$tooltip;
		$prop['name']=$name;
		$prop['href']=$href;
		$m->setProperty($prop);
		$this->addAdminMenu($m);
		return $this;
	}
	
	
	
	
}

?>