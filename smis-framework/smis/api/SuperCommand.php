<?php 

/**
 * this class used to 
 * manage how several supercommand should be handle.
 * this is command design pattern that make user easily to 
 * adding several command at once.
 * 
 * @author : Nurul Huda
 * @since : 17 Agustus 2015
 * @version : 1.0.1
 * @license : Apache v2
 * @copyright : goblooge@gmail.com
 * */

class SuperCommand{
    /* @var string */
	private $command;
	/* @var Modal */
	private $modal;
    /* @var string */
	private $slug;
	
	public function __construct(){
		$this->command=array();
		$this->slug=array();
		$this->modal=array();
	}
	
    /**
     * @brief   adding new command and automatically create 
     *          new DBResponder as default responder
     * @param   string $slug 
     * @param   Table $uitable 
     * @param   DBTable $dbtable 
     * @param   ArrayAdapter $adapter 
     * @param   Modal $modal 
     * @return  SuperCommand
     */
	public function addCommand($slug,$uitable,$dbtable,$adapter,$modal=null){
		$uitable->setName($slug);
		$uitable->setModel(Table::$SELECT);
		$this->slug[]=$slug;
		$this->command[$slug]=new DBResponder($dbtable, $uitable, $adapter);
		$this->modal[$slug]=$modal;
		return $this;
	}
	
    /**
     * @brief adding new responder in the supercommand
     * @param String $slug 
     * @param Responder $responder 
     * @param Modal $modal 
     * @return  SuperCommand
     */
	public function addResponder($slug,$responder,$modal=null){
		$this->slug[]=$slug;
		$this->command[$slug]=$responder;
		$this->modal[$slug]=$modal;
		return $this;
	}
	
    /**
     * @brief check is the suppercomamnd 
     *          calling
     * @return  boolean
     */
	public function is_supercommand(){
		return isset($_POST['super_command']);
	}
	
    /**
     * @brief initialize system
     * @return  string
     */
	public function initialize(){
		if(isset($_POST['super_command'])){
			$slug=$_POST['super_command'];
			if(in_array($slug, $this->slug)){
				$dbres=$this->command[$slug];
				if(isset($_POST['command'])){
					$data=$dbres->command($_POST['command']);
					return json_encode($data);
				}else{
					$uitable=$dbres->getUITable();
					$result=$uitable->getHtml();
					if($this->modal[$slug]!=null){
						$modal=$this->modal[$slug];
						$result.=$modal->getHtml();
					}						
					return $result;
				}
				
			}
		}
		return null;
	}
	
}

?>