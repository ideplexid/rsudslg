<?php 
/**
 * this class is the 
 * variable inside the
 * SettingBuilder
 * where the user can define what type
 * of item that need to create inside the SettingBuilder
 * 
 * @author      : Nurul Huda
 * @license     : LGPLv3
 * @copyright   : goblooge@gmail.com
 * @since       : 21 Mei 2014
 * @version     : 1.1.0
 * 
 * */

class SettingsItem{

	protected $name;
	protected $id;
	protected $value;
	protected $type;
	protected $db;
	protected $element;
	protected $desc;
	protected $single_slug;
	protected $is_single;
	protected $help_text;
	protected $disabled;
	protected $prefix;
    
    /**
     * @brief constructor of settings item
     * @param Database $db 
     * @param string $id 
     * @param string $name 
     * @param mixed $value 
     * @param string $type is the type of element, like text, select, checbox 
     * @param string $desc 
     * @param boolean $is_single dataset, mean this dataq would save as one data in smis_adm_settings
     * @param string $single_name 
     * @param string $help 
	 * @param boolean $disabled
     * @return  
     */
	public function __construct($db, $id,$name,$value="",$type="text",$desc="",$is_single=false,$single_name="",$help="",$disabled=false){
		$this->id		 	= $id;
		$this->name		 	= $name;
		$this->value	 	= $value;
		$this->type		 	= $type;
		$this->db		 	= $db;
		$this->desc		 	= $desc;
		$this->element	 	= $this->getElement();
		$this->is_single 	= $is_single;
		$this->single_slug 	= $single_name;
		$this->prefix		= "";
		$this->help_text	= $help;
		$this->disabled		= $disabled;
	}
	/**
	 * @brief adding Help Text
	 * @param string $help 
	 * @return  this
	 */
	public function setHelpText($help){
		$this->help_text	= $help;
        return $this;
	}
	
	/**
	 * @brief adding prefix
	 * @param string $prefix 
	 * @return  this
	 */
	public function setPrefix($prefix){
		$this->prefix	= $prefix;
        return $this;
	}
    
    /**
     * @brief set selected value for 
     *          select item
     * @param mixed $val 
     * @return  this;
     */
	public function setSelected($val){
		if(is_a($this->element, "Select")){
			$this->element->setSelected($val);
		}
        return $this;
	}
    
    /**
     * @brief get the data description
     * @return  string
     */
	public function getDescription(){
		return "<small>".$this->desc."</small>";
	}
    
    /**
     * @brief get the data name
     * @return  string
     */
	public function getName(){
		return $this->name;
	}
    
    /**
     * @brief return id
     * @return  id
     */
	public function getId(){
		return $this->id;
	}
	
    /**
     * @brief get the help text
     * @return  string
     */
	public function getHelp(){
		return $this->help_text;
	}
	
    /**
     * @brief get thse settings value
     * @return  mixed settings value
     */
	private function getSettings(){
		if($this->is_single){
			$json=getSettings($this->db, $this->single_slug, null,true);
			if($json==null) return $this->value;
				else $vjson=json_decode($json,true);
			if($this->type=="select"){
				if(isset($vjson[$this->id])){
					$st=$vjson[$this->id];
					$newval=array();
					foreach($this->value as $one){
						if($one['value']==$st) $one['default']="1";
						else $one['default']="0";
						$newval[]=$one;
					}
					return $newval;
				}else{
					return $this->value;
				}
			}else{
				return isset($vjson[$this->id])?$vjson[$this->id]:$this->value;
			}
		}else{
			if($this->type=="select"){
				$st=getSettings($this->db, $this->id, NULL);
				if($st==NULL){
					return $this->value;
				}else{
					$newval=array();
					foreach($this->value as $one){
						if($one['value']==$st) $one['default']="1";
						else $one['default']="0";
						$newval[]=$one;
					}
					return $newval;
				}				
			}else{
				$val=getSettings($this->db, $this->id, $this->value);
			}
			return $val;
		}
	}
    
    /**
     * @brief get the setting HTML element
     * @return  string HTML
     */
	public function getElement(){
		$val=null;
		$val=$this->getSettings();
		$element=ComponentFactory::createComponent($this->prefix.$this->id,"","",$this->name,$this->type,$val,$this->name,"","y",$this->disabled);
		return $element;
	}

}


?>