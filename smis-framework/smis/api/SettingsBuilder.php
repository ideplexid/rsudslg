<?php 
require_once 'smis-framework/smis/api/SettingsItem.php';
require_once 'smis-framework/smis/template/AdvanceModulTemplate.php';
/**
 * this class used to create a setting 
 * actually work as MasterTemplate or can be called as 
 * SettingTemplate. that handle how a setting could be
 * create easily
 * 
 * @author      : Nurul Huda
 * @since       : 16 Juni 2015
 * @version     : 1.9.1
 * @license     : LGPLv3
 * @database    : smis_adm_settings
 * @copyright   : goblooge@gmail.com
 * */
 
class SettingsBuilder extends AdvanceModulTemplate{
    
    /*@var int*/
	protected $id;
	/*@var string*/
	protected $title;
	/*@var string*/
	protected $class;
	/*@var array*/
	protected $tabs_component;
	/*@var array*/
	protected $tabs_name;
	/*@var array*/
	protected $tabs_icon;
	/*@var boolean*/
	protected $is_show_description;
	/*@var boolean*/
	protected $is_show_help_button;
	/*@var string*/
	protected $orientation;
	
	
	/*@var boolean*/
	protected $is_use_date;
	/*@var boolean*/
	protected $is_use_datetime;
	/*@var boolean*/
	protected $is_checkbox;
	/*@var boolean*/
	protected $is_single;
	/*@var boolean*/
	protected $is_one_variable;
	
	/*@var string*/
	protected $single_slug;
	/*@var string*/
	protected $one_variable;
	/*@var string*/
	protected $tabulator_mode;
	/*@var array*/
	protected $js;
	/*@var array*/
	protected $css;
	/*@var string*/
	protected $prototype_name;
	/*@var string*/
	protected $prototype_slug;
	/*@var string*/
	protected $prototype_implement;
	/*@var SuperCommand*/
	protected $super_command;
    /*@var string*/
	protected $prefix;
    /*@var boolean*/
    protected $is_partial_load;
    /*@var boolean*/
    protected $cur_partial_slug;
    
	/**
	 * @brief the constructor of the settings
	 * @param Database $db 
	 * @param string $id, is the id pf this settings 
	 * @param string $page , is the page , usually the modul name
	 * @param string $action , is the action name of the settings, usually 
	 * @param string $title , the settings title
	 * @return  null
	 */
	public function __construct($db,$id,$page,$action,$title="Settings"){
		parent::__construct($db,$page,$action);
		$this->id                   = $id;
		$this->title                = $title;
		$this->class                = "";
		$this->tabs_name            = array();
		$this->tabs_icon            = array();
		$this->tabs_component       = array();
		$this->is_use_date          = false;
		$this->is_checkbox          = false;
		$this->is_use_datetime      = false;
		$this->is_show_description  = false;
		$this->is_show_help_button  = false;
		$this->is_single            = false;
		$this->single_slug          = "";
		$this->tabulator_mode       = Tabulator::$LANDSCAPE;
		$this->super_command        = new SuperCommand();
		$this->js                   = array();
		$this->css                  = array();
		$this->prefix               = "";
        $this->is_partial_load      = false;
	}
    
    /**
     * add and ask slug at the same time
     * @params $slug the slug of the group
     * @params $name the name of the group
     * @params $icon the icon for the group
     * @return boolean if this slug is current loading
     */
    public function isGroupAdded($code,$name,$icon){
        $this->cur_partial_slug = $code;
        $this->addTabs($code,$name,$icon);
        return $this->isGroup($code);
    }
    
    /** check weather this slug used in current loading
     *  @params $slug
     *  @return boolean if this slug is current loading
     */
    public function isGroup($slug){
        if(isset($_POST['slug']) && $_POST['slug']==$slug){
            return true;
        }
        return false;
    }
    
    /**
     * @brief activated partial load or not
     * @param boolean $enabled 
     * @return  this
     */
    public function setPartialLoad($enabled){
        $this->is_partial_load=$enabled;
        return $this;
    }
	
    /**
     * @brief set the prefix data
     * @param string $prefix 
     * @return  
     */
	public function setPrefix($prefix){
		$this->prefix=$prefix;
	}
	
    /**
     * @brief adding new js resource
     * @param string $js  position
     * @return  SettingBuilder
     */
	public function addJS($js){
		$this->js[]=$js;
        return $this;
	}
	
    /**
     * @brief adding new css resource
     * @param string $css 
     * @return  SettingsBuilder
     */
	public function addCSS($css){
		$this->css[]=$css;
        return $this;
	}
	
    /**
     * @brief set this SettingBuilder prototype
     * @param string $name is the prototype_name 
     * @param string $slug is the prototype_slug 
     * @param string $implement is the prototype_implement 
     * @return  SettingsBuilder
     */
	public function setPrototype($name,$slug,$implement){
		$this->prototype_name       = $name;
		$this->prototype_slug       = $slug;
		$this->prototype_implement  = $implement;
		return $this;
	}
	
    /**
     * @brief set tablulator mode 
     * @param string $mode 
     * @return SettingBuilder
     */
	public function setTabulatorMode($mode){
		$this->tabulator_mode=$mode;
        return $this;
	}
	
    /**
     * @brief set single variable that store in smis_adm_settings
     * @param boolean $enabled trus if yes, false if not 
     * @param string $name single name setting name 
     * @return  SettingBuilder
     */
	public function setSingle($enabled,$name){
		$this->is_single    = $enabled;
		$this->single_slug  = $name;
		return $this;
	}
	
    /**
     * @brief set one varible that stored in database
     * @param boolean $enabled trus if yes, false if not 
     * @param string $name 
     * @return  SettingBuilder
     */
	public function setOneVariable($enabled,$name){
		$this->is_one_variable  = $enabled;
		$this->one_variable     = $name;
		return $this;
	}
	
    /**
     * @brief set date function enabled
     * @param boolean $enabled 
     * @return  SettingBuilder
     */
	public function setDateEnabled($enabled){
		$this->is_use_date  = $enabled;
		return $this;
	}

	/**
     * @brief set datetime function enabled
     * @param boolean $enabled 
     * @return  SettingBuilder
     */
	public function setDateTimeEnabled($enabled){
		$this->is_use_datetime = $enabled;
		return $this;
	}
	
    /**
     * @brief set enabled a checkbox
     * @param boolean $enabled 
     * @return  SettingBuilder
     */
	public function setCheckBoxEnabled($enabled){
		$this->is_checkbox = $enabled;
		return $this;
	}
	
    /**
     * @brief a super command, used to responds chooser in the settings
     * @return  SettingBuilder
     */
	public function superCommand($command){
		$init=$this->super_command->initialize();
		if($init!=null){
			echo $init;
		}
		return $this;
	}
	
    /**
     * @brief adding new Responder for supercommand
     * @param string $slug for the supercommand 
     * @param Responder $responder 
     * @return  SettingBuilder
     */
	public function addSuperCommandResponder($slug,$responder){
		$this->super_command->addResponder($slug, $responder);
		return $this;
	}
	
    /**
     * @brief the command model 
     *          of the Setting Builder
     * @return  Array of ResponsePackage
     */
	public function command($command){
		if($command=="save"){
			return $this->save();
		}else if($command){
            return $this->loadPartial();
        }
		return null;
	}
    
    /**
     * @brief load partially settings
     * @return  Array of ResponsePackage
     */
    public function loadPartial(){
        $slug       = $_POST['slug'];
        $one_tabs   = $this->tabs_component[$slug];
        $one_name   = $this->tabs_name[$slug];
        $one_icon   = $this->tabs_icon[$slug];
        $table      = new TablePrint($one_name);			
        $this->createOneTabSettings($table,$one_tabs);
        $col=2;
        if($this->isDescritionShow()){
            $col++;
        }			
        if($this->isHelpButtonShow()){
            $col++;
        }
        $btg   = new ButtonGroup("","","");
        $save  = new Button("", "", " Save ".$one_name);
        $save  ->setAction($this->action.".savePartial('".$slug."')")
               ->setClass("btn-primary")
               ->setIcon($one_icon)
               ->setIsButton(Button::$ICONIC_TEXT);
        $btg   ->addButton($save);
        if(getSettings($this->db,"slack_enable","0")=="1"){
            $slack = new Button("", "", " Slack ".$one_name);
            $slack ->setAction($this->action.".slackPartial('".$slug."')")
                   ->setClass("btn-success")
                   ->setIcon("fa fa-slack")
                   ->setIsButton(Button::$ICONIC_TEXT);
            $btg   ->addButton($slack);
        }
        $table  ->addColumn($btg->getHtml(), $col, 1,"footer");
        $itemid = $this->getPartialItemsId($slug);
        
        ob_start();
        $this->getSuperCommandJavascript();
        $js = ob_get_clean();
        
        $content['table'] = $table->getHtml().$js;
        $content['id']    = $itemid;
        
        $package = new ResponsePackage();
        $package ->setStatus(ResponsePackage::$STATUS_OK);
        $package ->setContent($content);
        return $package->getPackage();
    }
    
    /**
     * @brief save the settings
     * @return Array of Response Package
     */
    public function save(){
        if($this->is_one_variable){
            $tmp=$_POST[$this->one_variable];
            $DATA=json_decode($tmp,true);
        }else{
            $DATA=$_POST;
        }				
        $id=$this->getAllItemsId();
        if($this->is_single){
            $json=getSettings($db,$this->single_slug,null);
            if($json!=null){
              $val=json_decode($json,true);
              if($val==null){
                  $val=array();
              }
            }
            foreach($id as $i){
                if(isset($DATA[$i])){
                    $val[$i]=$DATA[$i];
                }
            }
            setSettings($this->db, $this->single_slug, json_encode($val));
        }else{
            foreach($id as $i){
                if(isset($DATA[$i])){
                    $val=$DATA[$i];
                    setSettings($this->db, $i, $val);
                }
            }
        }
        $res=new ResponsePackage();
        $res->setStatus(ResponsePackage::$STATUS_OK);
        $res->setAlertVisible(true);
        $res->setAlertContent("Success", "Data Saved", ResponsePackage::$TIPE_INFO);
        return $res->getPackage();
    }
	
    /**
     * @brief initialize this system
     * @return  SettingsBuilder
     */
	public function init(){
		if(isset($_POST['super_command']) && $_POST['super_command']!=""){
			$this->superCommand();
		}else if(isset($_POST['command'])){
			$res=$this->command($_POST['command']);
			echo json_encode($res);
		}else{
            $this->getResourceBeforePreload();
			echo $this->getHtml();
            $this->getResourceAfterPreload();
		}
		return $this;
	}
    
    /**
     * @brief get current group that called in partial load
     * @return  string group
     */
    public function getGroup(){
        return $_POST['slug'];
    }
    
    /**
     * @brief is this a preload function
     * @return  true if this is prelaod
     */
    public function isPreload(){
        return !isset($_POST['super_command']) && !isset($_POST['command']);
    }
	
    /**
     * @brief get all id of the item
     * @return  Array of the id
     */
	public function getAllItemsId(){
		$id=array();
		foreach($this->tabs_component as $tabs){
			foreach($tabs as $element){
                if(!is_string($element)){
                    $id[]=$element->getId();
                }
			}
		}
		return $id;
	}
    
    /**
     * @brief get partial id of the item
     * @return  Array of the id
     */
	public function getPartialItemsId($slug){
		$id=array();
        $tabs=$this->tabs_component[$slug];
        foreach($tabs as $element){
            if(!is_string($element)){
                $id[]=$element->getId();
            }
        }
        return $id;
    }
    
    /**
     * @brief set the tab orientation
     * @param string $orientation 
     * @return  SettingsBuilder
     */
	public function setOrientation($orientation){
		$this->orientation=$orientation;
		return $this;
	}
	
    /**
     * @brief check is the description show or not
     * @return  boolean
     */
	public function isDescritionShow(){
		return $this->is_show_description;
	}
	
    /**
     * @brief check is the help button show or not
     * @return  boolean
     */
	public function isHelpButtonShow(){
		return $this->is_show_help_button;
    }
    
	/**
     * @brief set this setting show description or not
     * @param boolean $enabled 
     * @return  SettingsBuilder
     */
	public function setShowDescription($enabled){
		$this->is_show_description=$enabled;
		return $this;
	}
	
    /**
     * @brief show button help or not
     * @param boolean $enabled 
     * @return  SettingsBuilder
     */
	public function setShowHelpButton($enabled){
		$this->is_show_help_button=$enabled;
		return $this;
	}
	
    /**
     * @brief set this class
     * @param string $class 
     * @return  SettingsBuilder
     */
	public function setClass($class){
		$this->class=$class;
		return $this;
	}
	
    /**
     * @brief adding new tab in the setting
     * @param string $slug 
     * @param string $name 
     * @param string $icon 
     * @return SettingBuilder  
     */
	public function addTabs($slug,$name,$icon=""){
		$this->tabs_component[$slug]=array();
		$this->tabs_name[$slug]=$name;
		$this->tabs_icon[$slug]=$icon;
		return $this;
	}
	
    /**
     * @brief adding the item on this Settings
     * @param string $tab_slug 
     * @param SettingsItem $items 
     * @return  SettingsBuilder
     */
	public function addItem($tab_slug,SettingsItem $items){
		$items->setPrefix($this->prefix);
		if(isset($this->tabs_component[$tab_slug])){
			$this->tabs_component[$tab_slug][]=$items;
		}
		return $this;
    }

    /**
     * @brief adding the item on this Settings
     *        of current active partial load slug
     * @return  SettingsBuilder
     */
    public function addCurrent($setname,$name,$value="",$type="text",$desc="",$is_single=false,$single_name="",$help=""){
        $this->addItem($this->cur_partial_slug,new SettingsItem($this->db,$setname,$name,$value,$type,$desc,$is_single,$single_name,$help));
        return $this;    
    }

    /** @brief add section into current load 
     *  @param string $section 
     *  @return $this;
     * */
    public function addSectionCurrent($section){
        $this->addSection($this->cur_partial_slug,$section);
        return $this;
    }
    
    /**
     * @brief adding section title to the table, it just string that need to be added
     * @param string $tab_slug 
     * @param string $section 
     * @return  SettingsBuilder
     */
    public function addSection($tab_slug,$section){
        if(isset($this->tabs_component[$tab_slug])){
			$this->tabs_component[$tab_slug][]=$section;
        }
        return $this;
    }
	

	/**
	 * @brief adding new class for this setting
	 * @param string $class 
	 * @return  SettingsBuilder
	 */
	public function addClass($class){
		$this->class.=" ".$class;
		return $this;
	}
	
    /**
     * @brief get the javascript that autogenerate 
     * @return  string
     */
	public function getJS(){
		ob_start();
		echo addJS("framework/smis/js/table_action.js");
		echo addJS("framework/smis/js/settings_action.js");
		
		if($this->is_use_date){
			echo addJS("framework/bootstrap/js/bootstrap-datepicker.js");
			echo addCSS("framework/bootstrap/css/datepicker.css");
		}		
		if($this->is_use_datetime){
			echo addJS("framework/bootstrap/js/bootstrap-datetimepicker.js");
			echo addCSS("framework/bootstrap/css/bootstrap-datetimepicker.css");
		}		
		if($this->is_checkbox){
			echo addCSS("framework/bootstrap/css/bootstrap-switch.css");
			echo addJS("framework/bootstrap/js/bootstrap-switch.js");
		}		
		foreach($this->js as $one){
			echo addJS($one,false);
		}		
		foreach($this->css as $one){
			echo addCSS($one,false);
		}
		
		echo "<script type=\"text/javascript\">";
		echo "var ".$this->action.";";
			echo '$(document).ready(function(){'; 
					loadLibrary("smis-libs-function-javascript");
					echo arrayToJSVar("var column", $this->getAllItemsId(),false);
				    echo $this->action.'=new SettingsAction("'.$this->page.'","'.$this->action.'",column,"'.$this->prefix.'");'; 
				    echo $this->action.'.setPrototipe("'.$this->prototype_name.'","'.$this->prototype_slug.'","'.$this->prototype_implement.'");';
                    if($this->is_one_variable){
                        echo $this->action.".setSingleVariable(true,'".$this->one_variable."');";
                    }
                    echo $this->action.'.preLoad=function(){';
                        if($this->is_checkbox)  echo "$('.smis_switch').bootstrapSwitch();";
                        if($this->is_use_date)      echo "$('.mydate').datepicker();"; 
                        if($this->is_checkbox)  echo "$('.mydatetime').datetimepicker({ minuteStep: 1});";
                    echo '};';
                    echo $this->action.'.preLoad();';                     
            echo "});";            
            if($this->is_partial_load){
                echo "\$('#".$this->id."_header > ul > li.active a').trigger('click');";
            }            
        echo "</script>";
        if(!$this->is_partial_load){
            $this->getSuperCommandJavascript();
        }
		$result=ob_get_clean();
		return $result;
	}    
    
    /**
     * @brief this function used to create a complete settings table
     * @param Table $table 
     * @param Array $one_tabs 
     * @return  Table
     */
    private function createOneTabSettings(TablePrint &$table,Array &$one_tabs){
        $table->setMaxWidth(false);
        $table->addTableClass("table table-bordered table-hover table-condensed ");
        $table->addColumn("No.", 1, 1);
        $table->addColumn("Settings", 1, 1);
        $table->addColumn("Value", 1, 1);
        if($this->isDescritionShow()) {
            if($this->isHelpButtonShow()){
                $table->addColumn("Description", 2, 1);
            }else{
                $table->addColumn("Description", 1, 1);
            }
        }				
        $table->commit("title");
        $no = 1;			
        foreach($one_tabs as $settings){
            if(is_string($settings)){
                $table->addColumn("<h4>".$settings."</h4>", 6, 1,"body",null,"center");
                continue;
            }
            $element=$settings->getElement();				
            $name=$settings->getName();
            $desc=$settings->getDescription();
            $help=$settings->getHelp();
            $table->addColumn(($no++).".", 1, 1);
            $table->addColumn($name, 1, 1);
            
            $columm_element=1;
            $columm_desc=1;
            if($this->isDescritionShow() && $desc==""){
                $columm_element++;
            }
            if($this->isHelpButtonShow() && $help==""){
                if($this->isDescritionShow() && $desc=="" || !$this->isDescritionShow()){
                    $columm_element++;
                }
                $columm_desc++;
            }
            $table->addColumn($element->getHtml(), $columm_element, 1);				
            if($this->isDescritionShow() && $desc!="") 
                $table->addColumn($desc, $columm_desc, 1);
            if($this->isHelpButtonShow() && $help!=""){
                $button=new Button("","","");
                $button->setIsButton(Button::$ICONIC);
                $button->setIcon("fa fa-question-circle");
                $button->setClass(" btn btn-primary");
                $button->setAction("help('".$this->page."','".$help."')");
                $table->addColumn($button->getHtml(), 1, 1);
            }				
            $table->commit("body");
        }
        return $table;
    }
	
    /**
     * @brief this used to get the HTML
     *          data of this system setting, including the javascript
     * @return  string
     */
	public function getHtml(){
		if($this->is_partial_load ){
            return $this->getHTMLPartialLoad();
        }else{
            return $this->getHTMLFull();
        }
	}
    
    /**
     * @brief get full html data
     * @return  string HTML
     */
    private function getHTMLFull(){
        $tabulator=new Tabulator($this->id, $this->class,$this->tabulator_mode);
		foreach ($this->tabs_component as $slug=>$one_tabs){
			$tabs_name=$this->tabs_name[$slug];
			$tabs_icon=$this->tabs_icon[$slug];
			$table=new TablePrint($tabs_name);			
            $this->createOneTabSettings($table,$one_tabs);
			$col=2;
			if($this->isDescritionShow()){
				$col++;
			}			
			if($this->isHelpButtonShow()){
				$col++;
			}			
			$btn=new Button("", "", " Save ");
			$btn->setAction($this->action.".save()");
			$btn->setClass("btn-primary");
			$btn->setIcon(" fa fa-save");
			$btn->setIsButton(Button::$ICONIC_TEXT);
			$table->addColumn($btn->getHtml(), $col, 1,"footer");
			$tabulator->add($slug, $tabs_name, $table->getHtml(), Tabulator::$TYPE_HTML,$tabs_icon);
		}
		$header="<h2>".$this->title."</h2>";
		return $header.$tabulator->getHtml().$this->getJS();
    }
    
    /**
     * @brief get only partially HTML data
     * @return  string HTML
     */
    private function getHTMLPartialLoad(){
        $tabulator=new Tabulator($this->id, $this->class,$this->tabulator_mode);
		foreach ($this->tabs_component as $slug=>$one_tabs){
			$tabs_name=$this->tabs_name[$slug];
			$tabs_icon=$this->tabs_icon[$slug];
			$tabulator->add($slug, $tabs_name, "", Tabulator::$TYPE_HTML,$tabs_icon,$this->action.".loadPartial('".$slug."')");
		}
		$header="<h2>".$this->title."</h2>";
		return $header.$tabulator->getHtml().$this->getJS();
    }    
}

?>