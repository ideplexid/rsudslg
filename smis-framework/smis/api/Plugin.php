<?php
/**
 * this class used to manage the plugin in smis
 * describe the plugin name, the data, the service that need
 * and how the plugin manage the database
 * 
 * @author : Nurul Huda
 * @since : 14 Februari 2014
 * @license : LPGLv3
 * @version : 1.1.0
 * @copyright : goblooge@gmail.com
 * 
 * */
class Plugin{
    /*@var string*/
	private $name;
    /*@var DBController*/
	private $wpdb;
	/*@var string*/
	private $path;
	/*@var string*/
	private $description;
	/*@var string*/
	private $require;
	/*@var string*/
	private $service;
	/*@var string*/
	private $number;
	/*@var string*/
	private $version;
	/*@var boolean*/
	private $is_prototype;
	/*@var array*/
    private static $system_setup;
    /*@var string*/
    private $process_message;
    
    /**
     * @brief the constructor
     * @param Array $init 
     * @return  
     */
	public function __construct($init){
		$this->name             = $init['name'];
		$this->description      = $init['description'];
		$this->path             = $init['path'];
		$this->require          = $init['require'];
		$this->service          = $init['service'];
		$this->number           = $init['number'];
		$this->version          = $init['version'];
		$this->process_message  = "";
		if(isset($init['type'])) {
			$this->is_prototype = ($init['type']=="prototype");
		}else {
			$this->is_prototype = false;	
		}
		$init['type']   = "";
	}
    
    /**
     * @brief to get data system setup 
     * @return  array
     */
    public static function getSystemSetup(){
        if(self::$system_setup==null){
            $json=getSettings(null,"smis_plugins",null);
            self::$system_setup = json_decode($json,true);
        }
        return self::$system_setup;
    }
	
    /**
     * @brief check is this plugin is a prototype
     *          and need to implement using prototype
     * @return  boolean trus if yes, and false if no
     */
	public function is_prototype(){
		return $this->is_prototype;
	}
	
    /**
     * @brief check is the plugin is the basic smis-administrator
     * @return  boolean trus is yes, and false is not
     */
	public function is_administrator(){
		return $this->name=="smis-administrator";
	}
	
    /**
     * @brief check is this plugin is smis-serverbus
     * @return   boolean trus is yes, and false is not
     */
	public function is_serverbus(){
		return $this->name=="smis-serverbus";
	}
	
    /**
     * @brief this function used to uninstall 
     *          all database system (usually not implemented)
     * @return  
     */
	public function uninstall(){
		if(!$this->is_installed()) return;
		include_once ($this->path."uninstall.php");
		$data                           = self::getSystemSetup();
        $data[$this->name]['installed'] = "false";
		$json                           = json_encode($data);
		setSettings(null, 'smis_plugins', $json);
	}
	
    /**
     * @brief this function used to update the database system
     *          of the plugins
     * @return  boolean true if success,and false if not
     */
	public function update(){
		if(!$this->is_installed() || !$this->is_actived() || !$this->is_update() )
			return false;
		global $NUMBER;
		$NUMBER = $this->getCurrentNumber();
        
        ob_start();
		require_once($this->path."update.php");
        $this->process_message=trim(ob_get_clean());
        
        if($this->process_message==""){
            $data                           = self::getSystemSetup();
            $data[$this->name]['number']    = $this->number;
            $data[$this->name]['version']   = $this->version;
            $json=json_encode($data);
            setSettings(null, 'smis_plugins', $json);
            return true;
        }else{
            return false;
        }
		
	}
    
    /**
     * @brief check is this plugin need to be updated
     * @return  trus if yes, and false if not
     */
	public function is_update(){
		if($this->getNumber()>$this->getCurrentNumber())
			return true;
		return false;
	}
	
    /**
     * @brief check is the pluign installed or not
     * @return  true if yes, false if not
     */
	public function is_installed(){
		$data   = self::getSystemSetup();		
		if(isset($data[$this->name])){
			$pg=$data[$this->name];
			if($pg==NULL || $pg=="")
				return false;
			if($pg['installed']=="true")
				return true;
		}
		return false;
	}
	
    /**
     * @brief check is the plugins activated or not
     * @return  if yes, false if not
     */
	public function is_actived(){
		$data   = self::getSystemSetup();
		if(isset($data[$this->name])){
			$pg = $data[$this->name];
			if($pg==NULL || $pg=="")
				return false;
			
			if($pg['actived']=="true")
				return true;
		}
		return false;
	}
	
    /**
     * @brief process install the plugin
     * @return  string confirmation
     */
	public function install(){		
		$json   = getSettings(null,"smis_plugins",null);		
		$data   = json_decode($json,true);
		$pg     = NULL;
		if(isset($data[$this->name])) 
			$pg = $data[$this->name];
		
		if($pg==NULL || $pg==""){
			$pg                 = array();
			$pg['installed']    = "false";
			$pg['actived']      = "false";
			$pg['version']      = $this->version;
			$pg['number']       = $this->number;
			$pg['type']         = $this->is_prototype;
		}
		
		if($pg['installed']=="true" && $this->name!='smis-administrator'){
			return "Already Installed";
		}else{
            ob_start();
			require_once ($this->path."install.php");
            $this->process_message  = trim(ob_get_clean());
            
            if($this->process_message==""){
                $pg['installed']    = "true";
                $data[$this->name]  = $pg;
                $json               = json_encode($data);
                setSettings(null, 'smis_plugins', $json);
                return "Installation Success";
            }else{
                return "Installation Failed";
            }
		}
	}
    
    /**
     * @brief process repairing the plugin
     * @return  string confirmation
     */
	public function repair(){	
        ob_start();
		require_once ($this->path."update.php");
        $this->process_message  = trim(ob_get_clean());
        if($this->process_message==""){    
            return "Repair Success";
        }else{
            return "Repair Failed";
        }
	}
    
    public function getProcessMessage(){
        return $this->process_message;
    }

    /**
     * @brief set the pluign active or not.
     * @param int $active 1 mean actived, 0 mean deactivate 
     * @return  this;
     */
	public function setActivation($active){
		$data                           = self::getSystemSetup();
		$data[$this->name]['actived']   = $active;
		$json                           = json_encode($data);
		setSettings(null, 'smis_plugins', $json);
        return $this;
	}

	
	/**
	 * @brief this function is the facade to call the navigator.php
     *          file in plugins
	 * @return  this;
	 */
	public function init_navigator(){
		require_once($this->path."navigation.php");
        return $this;
	}
	
    /**
     * @brief to print the plugin detail
     * @return  string plugin detail
     */
	public function toString(){
		$content  = "=============================</br>";
		$content .= "Name 	    : ".$this->name."</br>";
		$content .= "Active 	: ".$this->is_actived()."</br>";
		$content .= "Install 	: ".$this->is_installed()."</br>";
		$content .= "Path 	    : ".$this->path."</br>";
		$content .= "Desc 	    : ".$this->description."</br>";
		$content .= "Require 	: ".$this->require."</br>";
		$content .= "Service 	: ".$this->service."</br>";
		$content .= "=============================</br>";
		return $content;
	}
	
    /**
     * @brief get the plugin name
     * @return  string
     */
	public function getName(){
		return $this->name;
	}
	
    /**
     * @brief get the plugin path
     * @return  string
     */
	public function getPath(){
		return $this->path;
	}
	
    /**
     * @brief get the plugin description
     * @return  string
     */
	public function getDescription(){
		return $this->description;
	}
	
    /**
     * @brief get the plugin requirement
     * @return  this
     */
	public function getRequire(){
		return $this->require;
	}
	
    /**
     * @brief get the plugin data service
     * @return  string
     */
	public function getService(){
		if($this->service==""){
			$list           = scandir($this->path."/service/");
			$list           = array_delete($list,".");
			$list           = array_delete($list,"..");				
			$this->service  = implode(", ",$list);
			$this->service  = str_replace(".php", "", $this->service);
		}
		return $this->service;
	}
	
    /**
     * @brief return the plugin version
     * @return  string
     */
	public function getVersion(){
		return $this->version;
	}
	
    /**
     * @brief return the plugin number
     * @return  string
     */
	public function getNumber(){
		return $this->number;
	}
	
    /**
     * @brief get the current version that installed in the smis system
     * @return  string version
     */
	public function getCurrentVersion(){
		$data   = self::getSystemSetup();
		$pg     = NULL;
		if(isset($data[$this->name]))
			$pg = $data[$this->name];

		if($pg==NULL || $pg=="")
            return "0.0.0";
            
		return $pg['version'];
	}
	
    /**
     * @brief get return the current version number that installed in the syste
     * @return  the current number
     */
	public function getCurrentNumber(){
		$data   = self::getSystemSetup();
		$pg     = NULL;
		if(isset($data[$this->name]))
			$pg = $data[$this->name];
		
		if($pg==NULL || $pg=="")
			return "0.0.0";
		return $pg['number'];
	}
    
	/**
	 * @brief get the status of the plugin 
     *          to display to the user
	 * @return  string html
	 */
	function getStatus(){
		$status = "";
		$btgrup = new ButtonGroup();
		if($this->getName()!="smis-administrator"){
			if(!$this->is_installed()){
				$status     = "<span class='label'>Not Installed</span>";
                $install    = new Button("",$this->getName(),"Install");
                $install    ->setIsButton(Button::$ICONIC_TEXT)
                            ->setClass("btn-success pluginaction")
                            ->setIcon("fa fa-sign-in")
                            ->addAtribute("action","install")
                            ->addAtribute("name",$this->getName());
                
                $remove     = new Button("",$this->getName(),"Remove");
                $remove     ->setIsButton(Button::$ICONIC_TEXT)
                            ->setClass(" pluginaction")
                            ->setIcon("fa fa-trash")
                            ->addAtribute("action","remove")
                            ->addAtribute("name",$this->getName());
                
                $btgrup     ->addButton($install)
                            ->addButton($remove);
			}else if(!$this->is_actived()){                
                $activate   = new Button("",$this->getName(),"Activate");
                $activate   ->setIsButton(Button::$ICONIC_TEXT)
                            ->setClass(" pluginaction")
                            ->setIcon("fa fa-toggle-on")
                            ->setClass("btn-warning pluginaction")
                            ->addAtribute("action","activate")
                            ->addAtribute("name",$this->getName());
                
                $uninstall  = new Button("",$this->getName(),"Uninstall");
                $uninstall  ->setIsButton(Button::$ICONIC_TEXT)
                            ->setClass("btn-danger pluginaction")
                            ->setIcon("fa fa-sign-out")
                            ->addAtribute("action","uninstall")
                            ->addAtribute("name",$this->getName());
                
				$status     = "<span class='label label-warning'>Not Actived</span>";
                $btgrup     ->addButton($activate)
                            ->addButton($uninstall);
			}else if(!$this->is_update()){
                
                $deactivate = new Button("",$this->getName(),"Deactivate");
                $deactivate ->setIsButton(Button::$ICONIC_TEXT)
                            ->setClass(" pluginaction")
                            ->setIcon("fa fa-toggle-off")
                            ->setClass("btn-inverse pluginaction")
                            ->addAtribute("action","deactivate")
                            ->addAtribute("name",$this->getName());
                
                $repair     = new Button("",$this->getName(),"Repair");
                $repair     ->setIsButton(Button::$ICONIC_TEXT)
                            ->setClass("btn-primary pluginaction")
                            ->setIcon("fa fa-repeat")
                            ->addAtribute("action","repair")
                            ->addAtribute("name",$this->getName());

                $btgrup     ->addButton($deactivate)
                            ->addButton($repair);
				$status     = "<span class='label label-info'>Installed</span>";
			}else{
                
                $deactivate = new Button("",$this->getName(),"Deactivate");
                $deactivate ->setIsButton(Button::$ICONIC_TEXT)
                            ->setClass(" pluginaction")
                            ->setIcon("fa fa-toggle-off")
                            ->setClass("btn-inverse pluginaction")
                            ->addAtribute("action","deactivate")
                            ->addAtribute("name",$this->getName());
                
                $update     = new Button("",$this->getName(),"Update");
                $update     ->setIsButton(Button::$ICONIC_TEXT)
                            ->setClass("btn-info pluginaction")
                            ->setIcon("fa fa-chevron-circle-up")
                            ->addAtribute("action","update")
                            ->addAtribute("name",$this->getName());
                
                $btgrup     ->addButton($deactivate)
                            ->addButton($update);
				$status     = "<span class='label label-success'>Update ".$this->getVersion()."</span>";
			}
		}else if($this->is_update()){
			$status         = "<span class='label label-success'>Update ".$this->getVersion()."</span>";
            $update         = new Button("",$this->getName(),"Update");
            $update         ->setIsButton(Button::$ICONIC_TEXT)
                            ->setClass("btn-info pluginaction")
                            ->setIcon("fa fa-chevron-circle-up")
                            ->addAtribute("action","update")
                            ->addAtribute("name",$this->getName());
            $btgrup         ->addButton($update);
		}else{
			$status         = "<span class='label label-info'>Installed</span>";
            $repair         = new Button("",$this->getName(),"Repair");
            $repair         ->setIsButton(Button::$ICONIC_TEXT)
                            ->setClass("btn-primary pluginaction")
                            ->setIcon("fa fa-repeat")
                            ->addAtribute("action","repair")
                            ->addAtribute("name",$this->getName());
            $btgrup         ->addButton($repair);
		}
		$result['button']   = $btgrup->getHtml();
		$result['status']   = $status;
		return $result;
	}
	
	
	
	

}

?>