<?php 

/*
 * this class used to create select option 
 * and manage how the select data would be viewed
 * usign OOP system this class would made easy 
 * in data select builder
 * 
 * @author : Nurul Huda
 * @since : 14 Maret 2014
 * @version : 1.3.4
 * @license : LGPLv3
 * @copyright : goblooge@gmail.com
 * 
 * */

class OptionBuilder{
	/*@var array*/	
	private $array;
	
    /**
     * @brief the constructor
     * @return  null
     */
	public function __construct(){
		$this->array=array();
	} 
	
    /**
     * @brief this function used to adding one data 
     *          in option.
     * @param String $name 
     * @param String $value 
     * @param int $default 0 mean not default, 1 mean is default 
     * @param String $grup option grup 
     * @return  
     */
	public function add($name,$value=NULL,$default="0",$grup=NULL){
		if(!isset($value)) {
			$value=$name;
		}
		$this->array[]=array("name"=>$name,"value"=>$value,"default"=>$default,"grup"=>$grup);
		return $this;
	}
	
    /**
     * @brief just a shorcut to make even faster
     *          adding data that name anda value are same
     * @param string $name and the value 
     * @param string $default 0 is not default, 1 mean is a default 
     * @param string $grup mean this is default 
     * @return  this
     */
	public function addSingle($name,$default="0",$grup=NULL){
		$this->array[]=array("name"=>$name,"value"=>$name,"default"=>$default,"grup"=>$grup);
		return $this;
	}
	
    /**
     * @brief get the data array
     * @return  Array
     */
	public function getContent(){
		return $this->array;
	}
	
    /**
     * @brief get html string
     * @return  string
     */
	public function getHtml(){
		$content="";
		$curgrup=NULL;
		foreach($this->array as $a){
			if(isset($a['grup']) && $a['grup']!=NULL && $a['grup']!=$curgrup){
				if($curgrup!=NULL){
					$content.="</optgrup>";
				}
				$content.="<optgrup label='".$a['grup']."'>";
				$curgrup=$a['grup'];
			}
			$content.="<option value='".$a['value']."'>".$a['name']."</option>";
		}
		if($curgrup!=NULL){
			$content.="</optgroup>";
		}
		return $content;
	}
	
}

?>