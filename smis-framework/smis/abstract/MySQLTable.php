<?php 

abstract class MySQLTable {    
    protected $last_error;
    protected $last_query;
    protected $name; 
	protected $column;
    protected $use_adapter_for_select;
    protected $real_delete;
	protected $is_view_for_select;
    protected $is_count_enable;
    protected $showall;
    protected $query_view;
	protected $query_count;	
    protected $view;
	protected $kriteria;
	protected $order;
	protected $is_order;
	protected $group_by;
	protected $group_by_column;
	protected $is_groupby_for_count;
	protected $maximum;
    protected $delete;	
	protected $prefered_view;
	protected $name_view;
	protected $column_view;	
	protected $having;	
	protected $prefered_query;
	
	protected $custom_kriteria;
	protected $debug;	
	protected $use_where_for_view;
	protected $inserted_id;
	protected $conjuntion_custom_kriteria;
	protected $conjuntion_each_custom_kriteria;	
	protected $is_masking_count;
	protected $masking_count;
    
    public function __construct($name,$column=NULL){        
        $this->name                             = $name;
        $this->column                           = $column==NULL?$this->get_default_column($name):$column;
        $this->is_count_enable                  = true;
        $this->showall                          = false;
		$this->delete                           = false;
		$this->real_delete                      = false;
        $this->query_view                       = "";
		$this->query_count                      = "";
        $this->prefered_query                   = false;
		$this->custom_kriteria                  = null;
		$this->group_by                         = false;
		$this->having                           = null;
		$this->use_where_for_view               = false;
		$this->use_adapter_for_select           = false;
		$this->is_order                         = false;
		$this->is_groupby_for_count             = true;
		$this->is_view_for_select               = false;
		$this->conjuntion_custom_kriteria       = "AND";
		$this->conjuntion_each_custom_kriteria  = "AND";
		$this->view                             = " * ";
		$this->kriteria                         = $column;
		$this->order                            = $this->arrayToString($column);
		$this->maximum=10;		
		$this->name_view=null;
    }
    
    public function getMaximum(){
        return $this->maximum;
    }
    
    public function getCustomKriteria(){
		if($this->custom_kriteria==null) 
            return "";
		if(!is_array($this->custom_kriteria)) 
            return " ".$this->conjuntion_custom_kriteria." ".$this->custom_kriteria;
		
		$result=" ".$this->conjuntion_custom_kriteria." (";
		$first=true;
		foreach ($this->custom_kriteria as $key=>$value){
			if($value=="") continue;
			if(is_int($key)){
				if($first) $result.=$value;
				else $result.=" ".$this->conjuntion_each_custom_kriteria." ".$value;
			}else{
				if($first) $result.=$key.$value;
				else $result.=" ".$this->conjuntion_each_custom_kriteria." ".$key.$value;
			}
			$first=false;
		}
		$result.=")";
		return $result;
	}
    
    public function getDataCustomKriteria(){
        return $this->custom_kriteria;
    }
    
    /**
     * @brief set the count disabled
     * @param boolean $enable 
     * @return  this
     */
    public function setCountEnable($enable){
        $this->is_count_enable=$enable;
        return $this;
    }
		
	public function setGroupByForCount($group){
		$this->is_groupby_for_count=$group;
		return $this;
	}
    
    public function get_column(){
		return $this->column;
	}
    
    public function getLastQuery(){
        return $this->last_query;
    }
    
    public function getLastError(){
        return $this->last_error;
    }
    
    public function getColumn(){
		return $this->column;
	}
	
	public function setColumn($column){
		 $this->column=$column;
		 return $this;
	}
	
	public function addColumn($column){
		$this->column[]=$column;
		return $this;
	}	
	
	public  function setName($name){
		$this->name=$name;
		return $this;
	}
	
	public function getName(){
		return $this->name;
	}
    
    public function isUseAdapterForSelect(){
		return $this->use_adapter_for_select;
	}
    
    public function setViewForSelect($is_view,$use_adapter=false){
		$this->is_view_for_select=$is_view;
		$this->use_adapter_for_select=$use_adapter;
		return $this;
	}
    
    public function setRealDelete($delete){
		$this->real_delete=$delete;
		return $this;
	}
    
      /**
     * @brief only flag that make sure this table use prop and sinkron flag 
     *          as the data, so we can synchronize this table
     *          with another table. if not synchronize then need to be added sinkron column
     *          make sure the table have filed name 'synch'
     * @return  
     */
    public function activateTableSynchronous(){
        if(!in_array("prop",$this->column)){
            $this->column[] = "prop";
        }
        if(!in_array("synch",$this->column)){
            $this->column[] = "synch";
        }
        return $this;
    }
    
    protected function arrayToString($array){
        if($array==null){
            return "";
        }
		$result	= "";
		$first	= true;
		foreach($array as $a){
			if($first){
				$result .= $a;
				$first	 = false;
			}else{
				$result	.= " , ".$a;
			}
		}
		return $result;
	}
    
    public function setShowAll($showall){
		$this->showall 	 = $showall;
		return $this;
	}
	
	public function isShowAll(){
		return $this->showall;
	}
	
	public function setDelete($delete){
		$this->delete	 = $delete;
	}
	
	public function isDelete(){
		return $this->delete;
	}
	
	public function isRealDelete(){
		return $this->real_delete;
	}
    
    public function getCustomHaving(){
		if($this->having==null)	 		return "";
		if(!is_array($this->having))	return "HAVING ".$this->having;
		$result=" HAVING (";
		foreach ($this->having as $key=>$value){
			if($value=="") continue;
			if($result==" HAVING (") $result.=$key.$value;
			else $result.=" AND ".$key.$value;
		}
		$result.=")";
		return $result;
	}
    
    /**
	 * Filter Pattern that use for searching data in Database
	 * every Kriteria will be Filter in Database.
	 * @param string $column name in database
	 * @param string $value value name in database
	 */
	public function addCustomKriteria($column,$value){
		if($this->custom_kriteria==null || !is_array($this->custom_kriteria)) {
			$this->custom_kriteria = array();
		}
		if(is_array($value)){
			foreach($value as $k=>$v){
				$this->custom_kriteria[$k]	= $v;
			}
		}else if($column=="" || $column==null){
			$this->custom_kriteria[]		= $value;
		}else{
			$this->custom_kriteria[$column]	= $value;
		}
		return $this;
	}
    
    public function getRawCustomKriteria(){
		return $this->custom_kriteria;
	}
    
    public function setCustomKriteria($kriteria){
		$this->custom_kriteria=$kriteria;
		return $this;
	}
	
	public function ConjuctionCustomKriteria($con){
		$this->conjuntion_custom_kriteria=$con;
		return $this;
	}
	
	public function ConjuctionEachCustomKriteria($con){
		$this->conjuntion_each_custom_kriteria=$con;
		return $this;
	}
    
    public function setPreferredQuery($preferred,$qv, $qc){
		$this->query_count		= $qc;
		$this->query_view		= $qv;
		$this->prefered_query	= $preferred;	
		return $this;
	}
    
    public function getViewTableName(){
		if($this->prefered_view){
			return $this->name_view;
		}
		return $this->name;
	}
	
	public function setForceOrderForQuery($isforce,$order){
		$this->is_order=$isforce;
		$this->setOrder($order);
		return $this;
	}
	
	public function setCustomHaving($having){
		$this->having=$having;
		return $this;
	}
	
	public function addCustomHaving($column,$value){
		if($this->having==null || !is_array($this->having)) {
			$this->having=array();
		}
		if(is_array($value)){
			foreach($value as $k=>$v){
				$this->having[$k]=$v;
			}
		}else{
			$this->having[$column]=$value;
		}	
		return $this;
	}
    
    public function setView($view){
		if(is_array($view)) $this->view=$this->arrayToString($view);
		else $this->view=$view;		
		return $this;
	}
	
	public function setOrder($order,$is_order=true){
		if(is_array($order)) $this->order=$this->arrayToString($order);
		else $this->order=$order;			
		$this->is_order=$is_order;
		return $this;
	}
	
	public function setGroupBy($enable,$group){
		$this->group_by=$enable;
		if(is_array($group)) $this->group_by_column=$this->arrayToString($group);
		else $this->group_by_column=$group;
		return $this;
	}
	
	public function setMaximum($max){
		if(!$this->isShowAll()) $this->maximum=$max;
		return $this;
	}
	
	public function setMaskingCount($is_masking,$masking){
		$this->is_masking_count=$is_masking;
		$this->masking_count=$masking;		
	}
    
    public function count($term=""){
        if(!$this->is_count_enable){
            return 0;
        }else{
            $query=$this->getQueryCount($term);		
            if($this->is_masking_count){
                $query=str_replace("[query]",$query,$this->masking_count);
            }
            return $this->get_var($query);
        }
	}
    
    public function getQueryCount($term){
		if($this->prefered_query){
			$query=$this->query_count;
			if($this->use_where_for_view){
				$first=true;
				foreach($this->column as $k){
					if($first){
						$first=false;
						$query.=" WHERE ".$this->name.".prop NOT LIKE '%del%'  AND ( ( ".$this->name.".".$k." LIKE '%".$term."%' ";
					}else $query.=" OR ".$this->name.".".$k." LIKE '%".$term."%' ";
				}
				$query.=")".$this->getCustomKriteria()." ) ";
				if($this->group_by && $this->is_groupby_for_count)
					$query.=" GROUP BY ".$this->group_by_column;
				$query.=" ".$this->getCustomHaving();
			}				
			return $query;
		}else if($this->prefered_view){
			$query="SELECT count(*) FROM ".$this->name_view;
			$first=true;
			foreach($this->column_view as $k){
				if($first){
					$first=false;
					$query.=" WHERE (".$k." LIKE '%".$term."%' ";
				}else $query.=" OR ".$k." LIKE '%".$term."%' ";
			}
			$query.=")".$this->getCustomKriteria();
			if(!$this->isDelete()) {
				$query.=" AND prop NOT LIKE '%del%' ";
			}
			$query.=$this->getCustomHaving();
			return $query;
		}else{
			$query="SELECT count(*) FROM ".$this->name;
			$first=true;
			foreach($this->column as $k){
				if($first){
					$first=false;
					$query.=" WHERE (".$k." LIKE '%".$term."%' ";
				}else $query.=" OR ".$k." LIKE '%".$term."%' ";
			}
			$query.=")".$this->getCustomKriteria();
			if(!$this->isDelete()){
				$query.=" AND prop NOT LIKE '%del%' ";
			}
			$query.=$this->getCustomHaving();
			return $query;
		}
	}
	
	public function setDebuggable($debug){
		$this->debug=$debug;
		return $this;
	}
	
	public function setUseWhereforView($used){
		$this->use_where_for_view=$used;
		return $this;
	}
    
    public function setPreferredView($preferred,$name, $column=NULL){
		$this->prefered_view=$preferred;
		$this->name_view=$name;
		$this->column_view=$column;
		if($this->column_view==NULL){
			$this->column_view=$this->get_default_column($name);
		}
		return $this;
	}
    
    public function truncate(){
		$query="truncate ".$this->name.";";
		$this->query($query);
		return $this;
	}
    
    public function getQueryView($term,$start){
		$query="";
		$mode="";
		if($this->prefered_query){//using your own query
			$query=$this->query_view;				
			if($this->use_where_for_view){
				$first=true;
				foreach($this->column as $k){
					if($first){
						$first=false;
						$query.=" WHERE ".$this->name.".prop NOT LIKE '%del%'  AND ( ( ".$this->name.".".$k." LIKE '%".$term."%' ";
					}else $query.=" OR ".$this->name.".".$k." LIKE '%".$term."%' ";
				}
				$query.=")".$this->getCustomKriteria()." )";
			}				
			if($this->group_by)		$query.=" GROUP BY ".$this->group_by_column;
									$query.=$this->getCustomHaving();
			if($this->is_order)		$query.=" ORDER BY ".$this->order;
			if(!$this->showall)		$query.=" LIMIT ".$start.",".$this->maximum;
			$mode="query";
		}else if($this->prefered_view){//using view
			$kolom=$this->column_view;
			if(is_array($this->column_view)){
				$kolom="*";
			}
			$query="SELECT ".$kolom." FROM ".$this->name_view;
			$first=true;
			foreach($this->column_view as $k){
				if($first){
					$first=false;
					$query.=" WHERE (".$k." LIKE '%".$term."%' ";
				}else $query.=" OR ".$k." LIKE '%".$term."%' ";
			}
			$query.=")".$this->getCustomKriteria();		
			if(!$this->isDelete()) 	$query.=" AND prop NOT LIKE '%del%' ";
			if($this->group_by) 	$query.=" GROUP BY ".$this->group_by_column;
									$query.=$this->getCustomHaving();
			if($this->is_order) 	$query.=" ORDER BY ".$this->order;			
			if(!$this->showall)		$query.=" LIMIT ".$start.",".$this->maximum;
			$mode="view";
		}else{
			$query="SELECT ".$this->view." FROM ".$this->name;
			$first=true;
			foreach($this->column as $k){
				if($first){
					$first=false;
					$query.=" WHERE (".$k." LIKE '%".$term."%' ";
				}else $query.=" OR ".$k." LIKE '%".$term."%' ";
			}
			$query.=")".$this->getCustomKriteria();				
			if(!$this->isDelete()) 	$query.=" AND prop NOT LIKE '%del%' ";				
			if($this->group_by)		$query.=" GROUP BY ".$this->group_by_column;
									$query.=$this->getCustomHaving();				
			if($this->is_order)		$query.=" ORDER BY ".$this->order;
			if(!$this->showall)		$query.=" LIMIT ".$start.",".$this->maximum;
			$mode="default";
		}		
		return array("query"=>$query,"mode"=>$mode);
	}
    
    /**
	 * select the row in database 
	 * include the del row
	 * this function for backup purpose
	 * @param unknown $id
	 * @param string $is_view
	 * @return Ambigous <multitype:object, NULL, object, multitype:>
	 */
	public function selectEventDel($id,$is_view=false){
		if(is_array($id)){
			$where=" WHERE 1 ";
			foreach($id as $n=>$v){
				$where.=" AND ".$n."='".$v."'";
			}
			if($is_view)
				$query=$query="SELECT ".$this->view." FROM ".$this->name.$where;
			else
				$query="SELECT * FROM ".$this->name.$where;
			$row=$this->get_row($query);
			return $this->get_row($query);
		}else{
			if($is_view)
				$query=$query="SELECT ".$this->view." FROM ".$this->name." WHERE id='".$id."' ";
			else
				$query="SELECT * FROM ".$this->name." WHERE id='".$id."' ";				
			return $this->get_row($query);
		}
	}
    
    /**
	 * get only one row
	 * @param $id is an id of data, can be array or just an id value.
	 * @param $is_view if true, the result will using the view, otherwise will user raw data
	 * @param $use_prop if true then prop will be considered as one of the property
	 */
	public function select($id,$is_view=false,$use_prop=true){	
		if($this->is_view_for_select){		
			$this->setUseWhereforView(true);
			if(is_array($id)){
				foreach($id as $n=>$v){
					$this->addCustomKriteria($n,$v);
				}
			}else{
				if($this->name_view==null) $this->addCustomKriteria($this->name.".id","='".$id."'");
				else $this->addCustomKriteria($this->name_view.".id","='".$id."'");
			}			
			$this->setShowAll(false);
			$res=$this->view("", 0);
			$data=$res['data'];
			$row=$data[0];
			return $row;
		}
		
		if(is_array($id)){
			$where=" WHERE  ";
			if($use_prop){
				$where.=" prop NOT LIKE 'del'";
			}else{
				$where.=" 1 ";
			}
			foreach($id as $n=>$v){
				$where.=" AND ".$n."='".$v."'";
			}
			if($is_view) 
				$query=$query="SELECT ".$this->view." FROM ".$this->name.$where;
			else 
				$query="SELECT * FROM ".$this->name.$where;
			$row=$this->get_row($query);
			return $row;
		}else if( (is_int($id) && $id!=0) || (is_string($id) && $id!="0") ){
			if($is_view) 
				$query=$query="SELECT ".$this->view." FROM ".$this->name." WHERE id='".$id."' ";
			else 
				$query="SELECT * FROM ".$this->name." WHERE id='".$id."' ";
			return $this->get_row($query);
		}
		return NULL;
	}
    
    public function convertToSQL($rows){
		$query="";
		if(is_array($rows)){
			$kolom=$this->get_column();
			$the_column=array();
			foreach($kolom as $k){
				$the_column[]="`".$k."`";
			}
			$column="(".implode(" , ", $the_column).")";
			
			foreach($rows as $row){
				$nilai=array();
				foreach ($kolom as $name_kolom){
					$nilai[$name_kolom]="'".$this->escaped_string($row[$name_kolom])."'";
				}
				$query.="INSERT INTO ".$this->name." ".$column." VALUES ( ".implode(" , ", $nilai)." );\n";
			}
		}
		return $query;
	}
    
    /**
	 * just look like update.
	 * it will check fisrt is the database exist based on id, 
	 * if exist then update, if not exist then insert
	 * if id is array then it will be a several kriteria for update purpose.
	 * if di is string then it will represent the id in database table for update purpose.
	 * @param array $data
	 * @param array of string $id
     * @param array $warp is format dataset that need to be inserted
	 */
	public function insertOrUpdate($data,$id,$warp=null){
		$result=null;
		if($this->is_exist($id,$this->isRealDelete())){
			$result=$this->update($data,$id,$warp);
		}else{
			$result=$this->insert($data,$warp);
		}
		return $result;
	}
    
    public abstract function get_default_column($name=null);
    public abstract function query($query);
    public abstract function get_result($query);
    public abstract function get_row($query);
    public abstract function get_var($query);
	public abstract function getLastQueryError();
    public abstract function getNextID();
    public abstract function setFetchMethode($fetch);
    public abstract function get_db();
    public abstract function escaped_string($string);
    public abstract function delete($id);
    public abstract function get_inserted_id();
    public abstract function insert($data,$use_prop=true,$warp=null);
    public abstract function update($data,$id,$warp=null);
    public abstract function updateByCustomKriteria($update,$kriteria,$warp=null);
    public abstract function restore($data,$id);
    public abstract function is_exist($id,$include_del=false);
    public abstract function view($term,$page);
}

?>