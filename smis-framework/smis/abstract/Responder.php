<?php 

/**
 * 
 * Responder is an abstract for user that make the 
 * class could work with Table Action and it's Inheritance
 * 
 * @author 		: Nurul Huda
 * @since 		: 09 December 2016
 * @version 	: 2.0.1
 * @copyright	: goblooge@gmail.com
 * @license		: LGPLv3
 * @used		: - smis-framework/smis/database/DBResponder.php
 * 				  - smis-framework/smis/database/FileResponder.php
 * 
 * */

abstract class Responder{
	public abstract function command($command);
	public abstract function view();
    
    public function is($name){
		if(isset($_POST['command']) && $_POST['command']==$name){
			return true;
		}
		return false;
	}
    
    public function isDetail(){
        return $this->is("detail");
    }
    
	public function isSave(){
		return $this->is("save");
	}	
	public function isDel(){
		return $this->is("del");
	}
	public function isEdit(){
		return $this->is("edit");
	}
	public function isReload(){
		return $this->is("reload");
	}
	public function isExcel(){
		return $this->is("excel");
	}
	public function isSelect(){
		return $this->is("select");
	}
	public function isPrintElement(){
		return $this->is("print-element");
	}
	public function isPrinting(){
		return $this->is("printing");
	}
	public function isView(){
		return $this->is("list");
	}
	
	public function isPreload(){
		return !isset($_POST['command']);
	}
	
}

?>