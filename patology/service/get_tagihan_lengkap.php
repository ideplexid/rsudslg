<?php
global $db;
require_once "patology/resource/PatologyResource.php";
$resource = new PatologyResource();
$names=$resource->list_name;


if (isset ( $_POST ['noreg_pasien'] )) {
	$noreg = $_POST ['noreg_pasien'];
	$dbtable = new DBTable ( $db, "smis_pat_pesanan" );
	$dbtable->addCustomKriteria ( "noreg_pasien", "='" . $noreg . "'" );
	$dbtable->setShowAll ( true );	
	$data = $dbtable->view ( "", "0" );
	$rows = $data['data'];
	$result = array ();
	foreach ( $rows as $d ) {
		// biaya layanan lain:
		$layanan_lain_row = $dbtable->get_row("
			SELECT SUM(jumlah * harga_layanan) AS 'biaya_lain'
			FROM smis_pat_dpesanan_lain
			WHERE id_pesanan = '" . $d->id . "' AND prop NOT LIKE 'del'
			GROUP BY id_pesanan
		");
		$biaya_lain = $layanan_lain_row->biaya_lain != null ? $layanan_lain_row->biaya_lain : 0;
		
		$periksa = json_decode ( $d->periksa, true );
		$harga = json_decode ( $d->harga, true );
		$kls = str_replace ( " ", "_", $d->kelas );		
		
		if($biaya_lain*1>0){
			$result[] = array(
					'id' => $d->id."_biaya_lain",
					'nama' => (empty($d->no_pat) ? "#$d->id" : "$d->no_pat")." Biaya Lain ",
					'waktu' => ArrayAdapter::format("date d M Y", $d->tanggal),
					'ruangan' => $d->ruangan,
					'start' =>$d->tanggal,
					'end' => $d->tanggal,
					'biaya' => $biaya_lain,
					'jumlah' => 1,
					'keterangan' => "",
			);
		}
		
		foreach ( $periksa as $p => $val ) {
			if ($val == "1") {
				//$ket['periksa'][$names[$p]] = $harga[$kls.'_'.$p];
                $dk=$resource->getDebitKredit($slug);			
				$result[] = array(
						'id' => $d->id."_".$p,
						'nama' => (empty($d->no_pat) ? "#$d->id" : "$d->no_pat")." ".$names[$p],
						'waktu' => ArrayAdapter::format("date d M Y", $d->tanggal),
						'ruangan' => $d->ruangan,
						'start' =>$d->tanggal,
						'end' => $d->tanggal,
						'biaya' => $harga[$kls.'_'.$p],
						'jumlah' => 1,
						'keterangan' => "",
                        'debet' => $dk['d'],
						'kredit' => $dk['k'],
				);
			}
		}
		
		
	}
	$query = "SELECT count(*) as total FROM smis_pat_pesanan WHERE noreg_pasien = '".$noreg."' AND `status` !='Selesai' AND prop!='del' ";
	$ct = $db->get_var($query);
	$selesai = $ct>0?"0":"1";
	
	echo json_encode(array(			
		'selesai' => $selesai,
		'exist' => '1',
		'reverse' => '0',
		'cara_keluar' => "Selesai",
		'data' => array(
			'patology' => array(
				"result" => $result,
				"jasa_pelayanan" => "1"
			)
		)
	));
}

?>
