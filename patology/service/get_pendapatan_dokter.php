<?php 
global $db;
$carabayar=array();
$carabayar['umum']=array("dari"=>$_POST['dari'],"sampai"=>$_POST['sampai'],"kriteria"=>"carabayar!='bpjs'");
$carabayar['bpjs']=array("dari"=>$_POST['dari_bpjs'],"sampai"=>$_POST['sampai_bpjs'],"kriteria"=>"carabayar='bpjs'");

$result=array();
foreach($carabayar as $c=>$cv){
    $query="SELECT id_dokter,sum(biaya) as total 
            FROM smis_pat_pesanan
            WHERE prop!='del' 
            AND waktu_daftar >= '".$cv['dari']."' 
            AND waktu_daftar < '".$cv['sampai']."' 
            AND ".$cv['kriteria']."
            GROUP BY id_dokter";
    $list=$db->get_result($query);
    foreach($list as $x){
        if(!isset($result[$x->id_dokter])){
            $result[$x->id_dokter]=array();
        }
        $result[$x->id_dokter][$c.'_patology']=$x->total;
    }
}
echo json_encode($result);
?>