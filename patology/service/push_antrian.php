<?php
require_once 'smis-base/smis-include-service-consumer.php';
require_once 'patology/class/responder/PatologyResponder.php';
global $db;
$nama_konsultan = getSettings ( $db, "patology-konsultan-nama", "" );
$id_konsultan = getSettings ( $db, "patology-konsultan-id", "" );
$pasien ['tanggal'] = $_POST ['waktu'];
$pasien ['nama_pasien'] = $_POST ['nama_pasien'];
$pasien ['nrm_pasien'] = $_POST ['nrm_pasien'];
$pasien ['noreg_pasien'] = $_POST ['no_register'];
$detail = json_decode($_POST['detail'], true);
$pasien ['alamat'] = $detail['alamat'];
$pasien ['ibu'] = $detail['ibu'];
$pasien ['umur'] = $_POST['umur'];
$pasien ['carabayar'] = $_POST ['carabayar'];
$pasien ['jk'] = $_POST ['jk'];

$default_kelas=getSettings($db,"patology-ui-pemeriksaan-default-kelas","rawat_jalan");
$kelas = getSettings($db, "smis-rs-kelas-" . ArrayAdapter::format("slug", $_POST['asal']));
if ($kelas == null)
	$kelas = $default_kelas;
$pasien ['kelas'] = $kelas;

//mendapatkan uri pasien saat ini:
$params = array();
$params['noreg_pasien'] = $_POST['no_register'];
$service = new ServiceConsumer($db, "get_uri", $params, "registration");
$service->setMode(ServiceConsumer::$CLEAN_BOTH);
$content = $service->execute()->getContent();
$pasien ['uri'] = $content[0];
$pasien ['ruangan'] = $_POST ['asal'];
$pasien ['no_pat'] = "";
$pasien ['id_konsultan'] = $id_konsultan;
$pasien ['nama_konsultan'] = $nama_konsultan;
$success = array ();

$utest = strpos ( $data ['umur'], " Tahun " );
if ($utest === false) {
	$pasien ['limapuluh'] = 0;
} else {
	$umrth = substr ( $data ['umur'], 0, $utest ) * 1;
	if ($umrth > 50)
		$pasien ['limapuluh'] = 1;
	else
		$pasien ['limapuluh'] = 0;
}

$pasien['waktu_daftar']=date("Y-m-d H:i:s");
$pasien['waktu_ditangani']=PatologyResponder::getPlafon($pasien['waktu_daftar']);

$dbtable = new DBTable ( $db, "smis_pat_pesanan" );
$dbtable->insert ( $pasien );

/*MEMASUKAN KE DALAM LIS*/
if(getSettings($db,"patology-lis-connect", "0")=="1"){
	require_once 'patology/driver/LISOrder.php';
	$lis=new LISOrder($db);
	$nama=$_POST['nama_pasien'];
	$nrm=$_POST['nrm_pasien'];
	$noreg=$_POST['no_register'];
	$alamat=$pasien['alamat'];
	$tgl_lahir=$detail['tgl_lahir'];
	$usia=$pasien['umur'];
	$jns_rawat=$pasien['uri']=="1"?"RI":"RJ";
	$kode_ruang="pendaftaran";
	$nama_ruang=ArrayAdapter::format("unslug", $_POST['pendaftaran']);
	$kode_cara_bayar=$pasien['carabayar'];
	$jk=$pasien['jk'];
	$cara_bayar=ArrayAdapter::format("unslug", $pasien['carabayar']);
	$lis->setPasien($nama, $nrm, $noreg, $alamat, $tgl_lahir, $usia, $jns_rawat, $kode_ruang, $nama_ruang, $kode_cara_bayar, $cara_bayar,$jk);
	$lis->setDataLab($dbtable->get_inserted_id (),$waktu_daftar, $waktu_selesai);
	$lis->setDokterPembaca("");
	$lis->setTest("");
	$lis->setDiagnosa("");
	$lis->setDokterPengirim("", "");
	$lis->saveData();
}
/*AKHIR PEMASUKAN DALAM LIS*/



$nomor = $dbtable->count ( "" );
$success ['id'] = $dbtable->get_inserted_id ();
$success ['success'] = 1;
$success ['type'] = "insert";
$success ['nomor'] = $nomor;
$content = $success;
$response = new ResponsePackage ();
$response->setContent ( $content );
$response->setStatus ( ResponsePackage::$STATUS_OK );
$response->setAlertVisible ( true );
$response->setAlertContent ( "Data Saved", "Your Data Had Been Saved", ResponsePackage::$TIPE_INFO );
echo json_encode ( $response->getPackage () );
?>