<?php 
global $db;
$data['nama_pasien']    = $_POST['nama_pasien'];
$data['nrm_pasien']     = $_POST['nrm_pasien'];
$data['noreg_pasien']   = $_POST['noreg_pasien'];
$data['jk']             = $_POST['jk'];
$data['kelas']          = $_POST['kelas'];
$data['umur']           = $_POST['umur'];
$data['alamat']         = $_POST['alamat'];
$data['ibu']            = $_POST['ibu'];
$data['carabayar']      = $_POST['carabayar'];
$data['id_dokter']      = $_POST['id_pengirim'];
$data['nama_dokter']    = $_POST['pengirim'];
$data['id_pengirim']    = $_POST['id_pengirim'];
$data['pengirim']       = $_POST['pengirim'];
$data['ruangan']        = $_POST['ruangan'];
$data['no_pat']         = $_POST['no_pat'];
$data['periksa']        = $_POST['patology'];
$data['harga']          = $_POST['tarif_patology'];
$data['biaya']          = $_POST['total_patology'];
$data['waktu_daftar']   = $_POST['tanggal'];
$data['id_paket']       = $_POST['id'];
$data['prop']           = "";
$data['tanggal']        = substr($_POST['tanggal'],0,10);

$id['id_paket']=$_POST['id'];
$dbtable=new DBTable($db,"smis_pat_pesanan");
$dbtable->setRealDelete(true);
$dbtable->insertOrUpdate($data,$id);

?>