<?php 
require_once "patology/resource/PatologyResource.php";
global $db;
$id=$_POST['data'];

$dbtable= new DBTable($db,"smis_pat_pesanan");
$x          = $dbtable->selectEventDel($id);
$resource   = new PatologyResource();

/*transaksi untuk pasien Patology*/
    
$list   = array();
$periksa=json_decode($x->periksa,true);
$harga=json_decode($x->harga,true);
foreach($periksa as $slug=>$v){
    if($v=="0")
        continue;
    
    $biaya=$harga[$x->kelas."_".$slug];
    $nama=$resource->getListName($slug,$x->carabayar);
    $debet=array();
    $dk=$resource->getDebitKredit($slug);
    $debet['akun']    = $dk['d'];
    $debet['debet']   = $biaya;
    $debet['kredit']  = 0;
    $debet['ket']     = "Piutang Patology - ".$nama." - ".$x->no_pat." pasien ".$x->nama_pasien." dengan No.Reg ".$x->id;
    $debet['code']    = "debet-patology-".$x->id;
    $list[] = $debet;
     
    $kredit=array();
    $kredit['akun']    = $dk['k'];
    $kredit['debet']   = 0;
    $kredit['kredit']  = $biaya;
    $kredit['ket']     = "Pendapatan Patology - ".$nama." - ".$x->no_pat." pasien ".$x->nama_pasien." dengan No.Reg ".$x->id;
    $kredit['code']    = "kredit-patology-".$x->id;
    $list[] = $kredit;
}

//content untuk header
$header=array();
$header['tanggal']      = $x->waktu_daftar;
$header['keterangan']   = "Transaksi Patology ".$x->no_pat." Pasien ".$x->nama_pasien." dengan No.Reg ".$x->id;
$header['code']         = "Patology-".$x->id;
$header['nomor']        = "LAB-".$x->id;
$header['debet']        = $x->biaya;
$header['kredit']       = $x->biaya;
$header['io']           = "1";

$transaction_Patology=array();
$transaction_Patology['header']=$header;
$transaction_Patology['content']=$list;


/*transaksi keseluruhan*/
$transaction=array();
$transaction[]=$transaction_Patology;
echo json_encode($transaction);

/*confirm that already synch*/
$update['akunting']=1;
$id['id']=$x->id;
$dbtable->setName("smis_pat_pesanan");
$dbtable->update($update,$id);

?>