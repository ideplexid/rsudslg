<?php
	global $wpdb;
	
	require_once 'smis-libs-inventory/install.php';
	$install = new InventoryInstallator($wpdb, "", "");
	$install->extendInstall("pat");
	$install->install();
	
	require_once "patology/resource/install/table/order_pat.php";
	require_once "patology/resource/install/table/hasil_pat.php";
	require_once "patology/resource/install/table/smis_pat_dpesanan_lain.php";
	require_once "patology/resource/install/table/smis_pat_hasil.php";
	require_once "patology/resource/install/table/smis_pat_layanan.php";
	require_once "patology/resource/install/table/smis_pat_pertindakan.php";
	require_once "patology/resource/install/table/smis_pat_pesanan.php";
	require_once "patology/resource/install/table/smis_pat_schedule.php";
	require_once "patology/resource/install/table/smis_pat_rl52.php";
	
?>