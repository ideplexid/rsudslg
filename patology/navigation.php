<?php
	global $NAVIGATOR;
	$mr = new Menu ("fa fa-flask");
	$mr->addProperty ( 'title', 'Patology' );
	$mr->addProperty ( 'name', 'Patology' );
	$mr->addSubMenu ( "Daftar Pasien", "patology", "pemeriksaan", "Daftar Pasien Patology" ,"fa fa-binoculars");
	$mr->addSubMenu ( "Arsip", "patology", "arsip", "Arsip Patology" ,"fa fa-archive");
	$mr->addSubMenu ( "Jadwal", "patology", "jadwal", "Jadwal" ,"fa fa-calendar");
	$mr->addSubMenu ( "Arsip Admin", "patology", "arsip_admin", "Arsip Admin" ,"fa fa-archive");
	$mr->addSubMenu ( "Ruang Pasien Inap", "patology", "ruang_pasien_inap", "Laporan Pasien Rawat Inap" ,"fa fa-map");
	$mr->addSeparator ();
	$mr->addSubMenu ( "Layanan", "patology", "layanan", "Layanan Hasil" ,"fa fa-list");
	$mr->addSubMenu ( "List Hasil", "patology", "list_hasil", "Setting List Hasil" ,"fa fa-list-alt");
	$mr->addSubMenu ( "Laporan Hasil dan Layanan", "patology", "laporan", "Laporan","fa fa-line-chart" );
	$mr->addSubMenu ( "Settings", "patology", "settings", "Settings Patology" ,"fa fa-gear");
	$mr->addSeparator ();
	require_once 'smis-libs-inventory/navigation.php';
	$inventory_navigator = new InventoryNavigator($NAVIGATOR, "", "Patology", "patology");
	$mr = $inventory_navigator->extendMenu($mr);
	$NAVIGATOR->addMenu ( $mr, 'patology' );
?>