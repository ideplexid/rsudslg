<?php
require_once 'patology/class/adapter/PatologySalaryAdapter.php';

class PatologySalaryCollector {
	private $id;
	private $from;
	private $to;
	private $db;
	private $adapter;
	private $mode;
	
	public static $MODE_EMPLOYEE="individual";
	public static $MODE_ROOM="communal";	

	public function __construct($db, $id, $from, $to, $mode="individual") {
		$this->id = $id;
		$this->from = $from;
		$this->to = $to;
		$this->db = $db;
		$this->mode=$mode;
	}
	
	public function getSalary() {
		$hasil = array ();
		$dbtable = new DBTable ( $this->db, "smis_pat_pesanan");
		$dbtable->setFetchMethode ( DBTable::$ARRAY_FETCH );
		$ckc = self::createWhere ( array("id_dokter","id_konsultan","id_petugas"), $this->id );
		$dbtable->addCustomKriteria ( null, $ckc );
		$dbtable->addCustomKriteria ( null, " tanggal >= '" . $this->from . "'" );
		$dbtable->addCustomKriteria ( null, " tanggal < '" . $this->to . "'" );
		$dbtable->setShowAll(true);
		$dbtable->setDebuggable(true);
		$data = $dbtable->view ( "", "0" );
		$adapter=new PatologySalaryAdapter ( $this->id, $this->mode);
		$list=$adapter->getContent($data['data']);
		$hasil = array_merge ( $hasil, $list );		
		return $hasil;
	}
	public static function createWhere($check, $id) {
		$d = " ( ";
		$d .= implode ( " ='" . $id . "' OR ", $TABNAME );
		$d .= " ='" . $id . "' ) ";
	}
}

?>