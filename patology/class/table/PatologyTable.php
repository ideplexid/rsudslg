<?php 
class PatologyTable extends Table {
	private $code;
	public function setCode($code) {
		$this->code = $code;
	}
    
    public function getDetailPrintedElement($p, $f){
		$tbl=new TablePrint("");
		$tbl->setMaxWidth(false);
		$tbl->addStyle("width", "100%");
		$tbl->setTableClass("pat_kwitansi");
		global $db;
		
		$SHOW_JENIS_PASIEN=getSettings($db,"patology-kwitansi-tampil-jenis-pasien","1")=="1";
		$TOTAL_RESULT=getSettings($db,"patology-kwitansi-jumlah","1")*1;
		$SET_TAMPIL_TINDAKAN=getSettings($db,"patology-kwitansi-tampil-tindakan","1")=="1";
		$nama=getSettings($db,"smis_autonomous_title", "");
		$alamat=getSettings($db,"smis_autonomous_address", "");
		$tbl->addColumn("<strong> LABORATORY ".$nama."</strong>", 4, 1);
		$tbl->commit("title");
		$tbl->addColumn("<small>".$alamat."</small>", 4, 1);
		$tbl->commit("title");
		
        
		$tbl->addColumn("<small>&nbsp;</small>", 2, 1);
		$tbl->commit("title");
		
		$nrg=ArrayAdapter::format("only-digit10", $p->id)." - ".ArrayAdapter::format("only-digit6", $p->nrm_pasien);
		$tbl->addColumn("Nama", 1, 1,NULL,"","r_left");
		$tbl->addColumn(": ".$p->nama_pasien, 1, 1,NULL,"","r_left");
		$tbl->addColumn("NRM/NOREG", 1, 1,NULL,"","r_left");
		$tbl->addColumn(": ".$nrg, 1, 1,NULL,"","r_left");
		$tbl->commit("title");
		
        
        $nrg=ArrayAdapter::format("only-digit10", $p->id)." - ".ArrayAdapter::format("only-digit6", $p->nrm_pasien);
		$tbl->addColumn("Tanggal", 1, 1,NULL,"","r_left");
		$tbl->addColumn(": ".ArrayAdapter::format("date d M Y",$p->tanggal), 1, 1,NULL,"","r_left");
		$tbl->addColumn("Kelas", 1, 1,NULL,"","r_left");
		$tbl->addColumn(": ".ArrayAdapter::format("unslug",$p->kelas), 1, 1,NULL,"","r_left");
		$tbl->commit("title");
        
        $nrg=ArrayAdapter::format("only-digit10", $p->id)." - ".ArrayAdapter::format("only-digit6", $p->nrm_pasien);
		$tbl->addColumn("No. Pemeriksaan", 1, 1,NULL,"","r_left");
		$tbl->addColumn(": ".$p->no_pat, 1, 1,NULL,"","r_left");
		$tbl->addColumn("Jenis Pasien", 1, 1,NULL,"","r_left");
		$tbl->addColumn(": ".ArrayAdapter::format("unslug",$p->carabayar), 1, 1,NULL,"","r_left");
		$tbl->commit("title");
		
		
		$tbl->addColumn("<small>&nbsp;</small>", 2, 1);
		$tbl->commit("title");
		
		$tbl->addColumn("NAMA TEST", 3, 1,NULL,"","r_left b_bottom b_top bold");
		$tbl->addColumn("SUB TOTAL", 1, 1,NULL,"","r_left b_bottom b_top bold");
		$tbl->commit("title");
		if($SET_TAMPIL_TINDAKAN){
			$periksa=json_decode($p->periksa,true);
			$harga=json_decode($p->harga,true);
			foreach($periksa as $code=>$value){
				if($value=="1" || $value==1){
					$tbl->addColumn(ArrayAdapter::format("unslug",$code), 3, 1,NULL,"","r_left");
					$tbl->addColumn(ArrayAdapter::format("money Rp.", $harga[$p->kelas."_".$code] ), 1, 1,NULL,"","r_left");
					$tbl->commit("title");
				}
			}
		}
		
		loadLibrary("smis-libs-function-math");
		$tbl->addColumn("TOTAL", 3, 1,NULL,"","r_left b_top bold");
		$tbl->addColumn(ArrayAdapter::format("money Rp.", $p->total_biaya), 1, 1,NULL,"","r_left b_top bold");
		$tbl->commit("title","");
		$tbl->addColumn("TERBILANG", 1, 1,NULL,"","r_left  bold");
		$tbl->addColumn(numbertell($p->total_biaya)." Rupiah", 3, 1,NULL,"","r_left  ");
		$tbl->commit("title");
		
		$tbl->addColumn("<small>&nbsp;</small>", 4, 1);
		$tbl->commit("footer");
		
		$tbl->addColumn("", 2, 1);
		$tbl->addColumn("Operator", 2, 1);
		$tbl->commit("footer");
		
		$tbl->addColumn("", 2, 1);
		$tbl->addColumn("</br></br>", 2, 1);
		$tbl->commit("footer");
		
		global $user;
		$tbl->addColumn("", 2, 1);
		$tbl->addColumn($user->getNameOnly(), 2, 1);
		$tbl->commit("footer");
        $tbl->addColumn("", 2, 1);
		$tbl->addColumn(ArrayAdapter::format("date d M Y",date("Y-m-d")), 2, 1);
		$tbl->commit("footer");
		
		$hasil=$tbl->getHtml();
		$final_result="";
		for($tot=0;$tot<$TOTAL_RESULT;$tot++){
			$final_result=$final_result.$hasil." <div class='cutline'></div> ";
		}
		return $final_result;
	}
	
	public function getMiniPrintedElement($p, $f){
		$tbl=new TablePrint("");
		$tbl->setMaxWidth(false);
		$tbl->addStyle("width", "100%");
		$tbl->setTableClass("pat_kwitansi");
		global $db;
		
		$SHOW_JENIS_PASIEN=getSettings($db,"patology-kwitansi-tampil-jenis-pasien","1")=="1";
		$TOTAL_RESULT=getSettings($db,"patology-kwitansi-jumlah","1")*1;
		$SET_TAMPIL_TINDAKAN=getSettings($db,"patology-kwitansi-tampil-tindakan","1")=="1";
		$nama=getSettings($db,"smis_autonomous_title", "");
		$alamat=getSettings($db,"smis_autonomous_address", "");
		$tbl->addColumn("<strong> Kwitansi Patology ".$nama."</strong>", 2, 1);
		$tbl->commit("title");
		$tbl->addColumn("<small>".$alamat."</small>", 2, 1);
		$tbl->commit("title");
		
		$tbl->addColumn("<small>&nbsp;</small>", 2, 1);
		$tbl->commit("title");
		
		$tbl->addColumn("Nama", 1, 1,NULL,"","r_left");
		$tbl->addColumn($p->nama_pasien, 1, 1,NULL,"","r_left");
		$tbl->commit("title");
		$tbl->addColumn("Alamat", 1, 1,NULL,"","r_left");
		$tbl->addColumn($p->alamat, 1, 1,NULL,"","r_left");
		$tbl->commit("title");
		
		$tbl->addColumn("No. Pat", 1, 1,NULL,"","r_left");
		$tbl->addColumn($p->no_pat, 1, 1,NULL,"","r_left");
		$tbl->commit("title");
		
		
		$tbl->addColumn("ID-NRM-JP", 1, 1,NULL,"","r_left");
		$nrg=ArrayAdapter::format("only-digit10", $p->id)." - ".ArrayAdapter::format("only-digit6", $p->nrm_pasien)." - ".$p->carabayar;
		$tbl->addColumn($nrg, 1, 1,NULL,"","r_left");
		$tbl->commit("title");	
		
		$tbl->addColumn("RUANG - KLS", 1, 1,NULL,"","r_left");
		$tbl->addColumn(ArrayAdapter::format("unslug", $p->ruangan)." - ".ArrayAdapter::format("unslug", $p->kelas), 1, 1,NULL,"","r_left");
		$tbl->commit("title");
		
		$tbl->addColumn("Tanggal", 1, 1,NULL,"","r_left");
		$tbl->addColumn(ArrayAdapter::format("date d M Y", $p->tanggal), 1, 1,NULL,"","r_left");
		$tbl->commit("title");
		
		$tbl->addColumn("<small>&nbsp;</small>", 2, 1);
		$tbl->commit("title");
		
		$tbl->addColumn("Nama Test", 1, 1,NULL,"","r_left b_bottom b_top");
		$tbl->addColumn("Total Rp.", 1, 1,NULL,"","r_left b_bottom b_top");
		$tbl->commit("title");
		if($SET_TAMPIL_TINDAKAN){
			$periksa=json_decode($p->periksa,true);
			$harga=json_decode($p->harga,true);
			foreach($periksa as $code=>$value){
				if($value=="1" || $value==1){
					$tbl->addColumn(ArrayAdapter::format("unslug",$code), 1, 1,NULL,"","r_left");
					$tbl->addColumn(ArrayAdapter::format("money Rp.", $harga[$p->kelas."_".$code] ), 1, 1,NULL,"","r_left");
					$tbl->commit("title");
				}
			}
		}
		
		loadLibrary("smis-libs-function-math");
		$tbl->addColumn("TOTAL", 1, 1,NULL,"","r_left b_top");
		$tbl->addColumn(ArrayAdapter::format("money Rp.", $p->total_biaya), 1, 1,NULL,"","r_left b_top");
		$tbl->commit("title","");
		$tbl->addColumn(numbertell($p->total_biaya)." Rupiah", 2, 1,NULL,"","r_left b_bottom ");
		$tbl->commit("title");
		
		$tbl->addColumn("<small>&nbsp;</small>", 2, 1);
		$tbl->commit("footer");
		
		$tbl->addColumn("", 1, 1);
		$tbl->addColumn("TTD", 1, 1);
		$tbl->commit("footer");
		
		$tbl->addColumn("", 1, 1);
		$tbl->addColumn("</br></br>", 1, 1);
		$tbl->commit("footer");
		
		global $user;
		$tbl->addColumn("", 1, 1);
		$tbl->addColumn($user->getNameOnly(), 1, 1);
		$tbl->commit("footer");
		
		$hasil=$tbl->getHtml();
		$final_result="";
		for($tot=0;$tot<$TOTAL_RESULT;$tot++){
			$final_result=$final_result.$hasil." <div class='cutline'></div> ";
		}
		return $final_result;
	}
	
	public function getRegulerPrintedElement($p, $f){
		$tbl=new TablePrint("");
		$tbl->setMaxWidth(false);
		$tbl->addStyle("width", "100%");
		$tbl->setTableClass("pat_kwitansi");
		global $db;
		
		$SHOW_JENIS_PASIEN=getSettings($db,"patology-kwitansi-tampil-jenis-pasien","1")=="1";
		$TOTAL_RESULT=getSettings($db,"patology-kwitansi-jumlah","1")*1;
		$SET_TAMPIL_TINDAKAN=getSettings($db,"patology-kwitansi-tampil-tindakan","1")=="1";
		$nama=getSettings($db,"smis_autonomous_title", "");
		$alamat=getSettings($db,"smis_autonomous_address", "");
		$tbl->addColumn("<h5> Kwitansi Patology ".$nama."</h5>", 4, 1);
		$tbl->commit("title");
		$tbl->addColumn("No. ".ArrayAdapter::format("only-digit10", $p->id), 1, 1);
		$tbl->addColumn("<small>".$alamat."</small>", 3, 1);
		$tbl->commit("title");
		
		$tbl->addColumn("Nama", 1, 1);
		$tbl->addColumn($p->nama_pasien, 1, 1);
		$tbl->addColumn("Alamat", 1, 1);
		$tbl->addColumn($p->alamat, 1, 1);
		$tbl->commit("header");
		
		$tbl->addColumn("No. Pat", 1, 1);
		$tbl->addColumn($p->no_pat, 1, 1);
		$tbl->addColumn("NRM", 1, 1);
		$tbl->addColumn(ArrayAdapter::format("only-digit6", $p->nrm_pasien), 1, 1);
		$tbl->commit("header");
		
		if($SHOW_JENIS_PASIEN){
			$tbl->addColumn("Kelas", 1, 1);
			$tbl->addColumn(ArrayAdapter::format("unslug", $p->kelas), 1, 1);
			$tbl->addColumn("Jenis Pasien", 1, 1);
			$tbl->addColumn($p->carabayar, 1, 1);
			$tbl->commit("header");
		}else{
			$tbl->addColumn("Kelas", 2, 1);
			$tbl->addColumn(ArrayAdapter::format("unslug", $p->kelas), 2, 1);
			$tbl->commit("header");
		}
		
		$tbl->addColumn("Pengirim", 1, 1);
		$tbl->addColumn($p->nama_dokter, 1, 1);
		$tbl->addColumn("Tanggal", 1, 1);
		$tbl->addColumn(ArrayAdapter::format("date d M Y", $p->tanggal), 1, 1);
		$tbl->commit("header");
		
		loadLibrary("smis-libs-function-math");
		$tbl->addColumn("Nominal", 1, 1);
		$tbl->addColumn(ArrayAdapter::format("money Rp.", $p->total_biaya), 3, 1);
		$tbl->commit("body");
		$tbl->addColumn(numbertell($p->total_biaya)." Rupiah", 4, 1);
		$tbl->commit("body");
		
		if($SET_TAMPIL_TINDAKAN){
			$pemeriksaaan="";
			$periksa=json_decode($p->periksa,true);
			foreach($periksa as $code=>$value){
				if($value=="1" || $value==1){
					$pemeriksaaan.=ArrayAdapter::format("unslug",$code)." , ";
				}
			}
			$tbl->addColumn($pemeriksaaan, 4, 1);
			$tbl->commit("body");
		}
		
		$tbl->addColumn("TTD Kasir", 2, 1);
		$tbl->addColumn("TTD", 2, 1);
		$tbl->commit("footer");
		
		$tbl->addColumn("", 2, 1);
		$tbl->addColumn("</br></br>", 2, 1);
		$tbl->commit("footer");
		
		global $user;
		$tbl->addColumn("", 2, 1);
		$tbl->addColumn($user->getNameOnly(), 2, 1);
		$tbl->commit("footer");
		
		$hasil=$tbl->getHtml();
		$final_result="";
		for($tot=0;$tot<$TOTAL_RESULT;$tot++){
			$final_result=$final_result.$hasil." <div class='cutline'></div> ";
		}
		return $final_result;
		
	}
	
	public function getPrintedElement($p, $f){
		global $db;
		$model=getSettings($db, "patology-kwitansi-model", "Reguler");
		$css="<style type='text/css'>".getSettings($db, "patology-kwitansi-css", "")."</style>";
		if($model=="Reguler"){
			return $this->getRegulerPrintedElement($p, $f).$css;
		}else if($model=="Mini"){
			return $this->getMiniPrintedElement($p, $f).$css;
		}else{
			return $this->getDetailPrintedElement($p, $f).$css;
		}
	}
	
	public function getContentButton($id) {
        global $db;
		$btn_group=null;
		if ($this->code == LaboratyTemplate::$MODE_DAFTAR && 
				( $this->current_data ['selesai'] == "1" || $this->current_data ['selesai'] == "-2" || $this->current_data ['selesai'] == "-1" ) ) {
                if(getSettings($db, "patology-ui-pemeriksaan-edit-hasil", "0") == "1"){
                    $btn_group=new ButtonGroup("");
                    $btn=new Button($this->name."_edit", "","Edit");
                    $btn->setAction($this->action.".edit('".$id."')");
                    if($this->current_data ['selesai'] == "1") 	$btn->setClass("btn-success");
                    else if($this->current_data ['selesai'] == "-1") 	$btn->setClass("btn-inverse");
                    else if($this->current_data ['selesai'] == "-2") 	$btn->setClass("btn-danger");
                    $btn->setAtribute("data-content='Edit' data-toggle='popover'");
                    $btn->setIcon("fa fa-pencil");
                    $btn->setIsButton(Button::$ICONIC);
                    $btn_group->addElement($btn);
                }
			
		}else if($this->code == LaboratyTemplate::$MODE_PERIKSA && $this->current_data ['selesai'] == "-2" ) {
			
			$btn=new Button($this->name."_tidak_batal", "","Tidak Batal");
			$btn->setAction($this->action.".kembalikan('".$id."')");
			$btn->setAtribute("data-content='Tidak Batal' data-toggle='popover'");
			$btn->setIcon("fa fa-refresh");
			$btn->setIsButton(Button::$ICONIC);
			
			$btn_group=parent::getContentButton($id);
			$btn_group->addElement($btn);
			$btn_group->setMax(4, "Batal");
			$btn_group->setButtonClass("btn-danger");
			
		}else {
			$btn_group= parent::getContentButton($id);
		}
            $btn_view=new Button($this->name."_lihathasil", "","View Hasil");
			$btn_view->setAction("lihathasil('".$id."')");
			//$btn_view->setAction("do_print_pat_gabungan()");
			$btn_view->setClass("btn-success");
			$btn_view->setAtribute("data-content='Print' data-toggle='popover'");
			$btn_view->setIcon("fa fa-eye");
			$btn_view->setIsButton(Button::$ICONIC);
			$btn_group->addElement($btn_view);
			$btn=new Button($this->name."_printelement", "","Print");
			$btn->setAction("cetak_kwitansi('".$id."')");
			$btn->setClass("btn-inverse");
			$btn->setAtribute("data-content='Print' data-toggle='popover'");
			$btn->setIcon("fa fa-ticket");
			$btn->setIsButton(Button::$ICONIC);
			$btn_group->addElement($btn);
			
		
		return $btn_group;
	}
}

?>