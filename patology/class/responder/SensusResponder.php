<?php 

class SensusResponder extends DBResponder{

    private $jenis_pasien;
    public function setJenisPasien($j){
        $this->jenis_pasien = $j;
    } 

    public function excel(){
        date_default_timezone_set ( "Asia/Jakarta" );
        
        $d = $this->dbtable->view("","0");
        $data  = $d['data'];
        $list = $this->adapter->getContent($data);

        require_once ("smis-libs-out/php-excel/PHPExcel.php");
        $file  = new PHPExcel ();
        $sheet = $file->getActiveSheet();
        
        $count = count($this->jenis_pasien);
        $count += 8;
        $last = PHPExcel_Cell::stringFromColumnIndex($count - 1);
        $last_jn = PHPExcel_Cell::stringFromColumnIndex($count - 2);

        $tgl = ArrayAdapter::format("date d M Y",$_POST['dari'])." s/d ".ArrayAdapter::format("date d M Y",$_POST['sampai']);

        $sheet ->mergeCells("A1:".$last."1")->setCellValue("A1","LAPORAN SENSUS HARIAN");
        $sheet ->mergeCells("A2:".$last."2")->setCellValue("A2","RUANGAN PATOLOGY");
        $sheet ->mergeCells("A3:".$last."3")->setCellValue("A3",$tgl);


        $sheet ->mergeCells("A5:A6")->setCellValue("A5","No.");
        $sheet ->mergeCells("B5:B6")->setCellValue("B5","Tanggal");
        $sheet ->mergeCells("C5:D5")->setCellValue("C5","Jenis Kunjungan");
        $sheet ->setCellValue("C6","Baru");
        $sheet ->setCellValue("D6","Lama");
        
        $sheet ->mergeCells("E5:G5")->setCellValue("E5","Asal Pasien");
        $sheet ->setCellValue("E6","Rawat Inap");
        $sheet ->setCellValue("F6","Poli");
        $sheet ->setCellValue("G6","Rujukan");
 
        $sheet ->mergeCells("H5:".$last_jn."5")->setCellValue("H5","Jenis Pasien");
        $cur_col = array();
        $num = 0;
        $jenis = array();
        foreach($this->jenis_pasien as $j){
            $c = PHPExcel_Cell::stringFromColumnIndex(7+$num);
            $num++;
            $sheet ->setCellValue($c."6",$j['name']);
            $j['column'] = $c;
            $jenis[] = $j;
        }
        $sheet ->mergeCells($last."5:".$last."6")->setCellValue($last."5","Total");

        $sheet ->getStyle("A1:".$last."6")->getFont()->setBold(true);
        $sheet->getStyle("A1:".$last."6")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);


        $no         = 6;
        $iter       = 0;
        foreach($list as $x){
            $no++;
            $iter++;

            $sheet->setCellValue("A".$no,$x['No.']);
            $sheet->setCellValue("B".$no,$x['Tanggal']);
            $sheet->setCellValue("C".$no,$x['baru']);
            $sheet->setCellValue("D".$no,$x['lama']);
            $sheet->setCellValue("E".$no,$x['rawat_inap']);
            $sheet->setCellValue("F".$no,$x['poli']);
            $sheet->setCellValue("G".$no,$x['rujukan']);

            foreach($jenis as $j){
                $sheet->setCellValue($j['column'].$no,$x[$j['value']]);
            }
            $sheet->setCellValue($last.$no,$x['Total']);
        }

        $thin = array ();
        $thin['borders']=array();
        $thin['borders']['allborders']=array();
        $thin['borders']['allborders']['style']=PHPExcel_Style_Border::BORDER_THIN ;
        $sheet->getStyle ( 'A5:'.$last.$no )->applyFromArray ($thin);

        //$this->setAutoSize($sheet,"A",$last);

        header  ('Content-Type: application/vnd.ms-excel');
        header  ('Content-Disposition: attachment;filename="Sensus Harian Patology.xls"' );
        header  ('Cache-Control: max-age=0');
        $writer = PHPExcel_IOFactory::createWriter( $file, 'Excel5' );
        $writer ->save ( 'php://output' );
        return;
    }

    
    private function setAutoSize($sheet, $start,$end){
        foreach(range($start,$end) as $columnID) {
            $sheet  ->getColumnDimension($columnID)
                    ->setAutoSize(true);
        }
    }


}