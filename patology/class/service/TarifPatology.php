<?php
require_once ("smis-base/smis-include-service-consumer.php");
class TarifPatology extends ServiceConsumer {
	public function __construct($db, $kelas) {
		$data = array (
				"kelas" => $kelas 
		);
		parent::__construct ( $db, "get_patology", $data, "manajemen" );
		$this->setMode ( ServiceConsumer::$SINGLE_MODE );
	}
}

?>