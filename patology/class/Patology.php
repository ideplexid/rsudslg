<?php
require_once 'smis-framework/smis/template/ModulTemplate.php';
require_once ("smis-base/smis-include-service-consumer.php");
require_once ("smis-libs-hrd/DKResponder.php");
require_once 'patology/class/responder/PatologyResponder.php';
require_once 'patology/class/adapter/PatologyAdapter.php';
require_once 'patology/class/service/RuanganService.php';
require_once 'smis-libs-manajemen/ProvitSharingService.php';
require_once 'patology/class/service/TarifPatology.php';
require_once 'patology/resource/PatologyResource.php';
require_once 'patology/class/table/PatologyTable.php';
require_once 'smis-libs-hrd/EmployeeResponder.php';


class LaboratyTemplate extends ModulTemplate {
	protected $db;
	protected $mode;
	protected $polislug;
	protected $page;
	protected $protoslug;
	protected $protoname;
	protected $protoimplement;
	protected $action;
	protected $dbtable;
	protected $uitable;
	protected $noreg_pasien;
	protected $nama_pasien;
	protected $nrm_pasien;
	protected $jk;
	protected $list_pesan;
	protected $list_hasil;
	protected $kelas;
	protected $patres;
	protected $umur;
	protected $alamat;
    protected $kelurahan;
	protected $kecamatan;
	protected $kota;
	protected $provinsi;
	protected $ibukandung;
	protected $limapuluh;
	protected $carabayar;
	protected $uri;
	protected $tgl_lahir;
	protected $is_stand_alone;
	protected $is_stand_alone_lis_button;
	
	public static $MODE_DAFTAR = "pendaftaran";
	public static $MODE_PERIKSA = "pemeriksaan";
	public static $MODE_ARCHIVE = "arsip";
	public function __construct($db, $mode, $polislug = "all", $noreg = "", $nrm = "", $nama = "", $jk = "", $page = "patology", $action = "pemeriksaan", $protoslug = "", $protoname = "", $protoimplement = "", $kelas = "Kelas I") {
		$this->db = $db;
		$this->mode = $mode;
		$this->noreg_pasien = $noreg;
		$this->nama_pasien = $nama;
		$this->nrm_pasien = $nrm;
		$this->jk = $jk;
		$this->polislug = $polislug;
        $col = array("id","id_paket","tanggal","nama_pasien","nrm_pasien","noreg_pasien","jk","kelas","setting_kelas","umur","alamat","kelurahan","kecamatan","kabupaten","provinsi","ibu","limapuluh","carabayar","id_marketing","marketing","ruangan","no_pat","id_dokter","nama_dokter","id_konsultan","nama_konsultan","id_petugas","nama_petugas","periksa","hasil","hapusan_darah","harga","pembagian","selesai","waktu_daftar","waktu_ditangani","waktu_selesai","waktu_ditangani_hapusan","waktu_selesai_hapusan","uri","cetak_hasil_ke","cetak_1","cetak_2","cetak_3","cetak_gabung","response_time","response_time_hapusan","diagnosa","file","akunting","status","operator","biaya","biaya_lain","total_biaya","kesimpulan");
        
        require_once "patology/class/dbtable/PatologyDBTable.php";
        $this->dbtable = new PatologyDBTable ( $db, "smis_pat_pesanan",$col );
        $this->dbtable ->setAutoSynch(getSettings($db,"cashier-real-time-tagihan","0")!="0");

        //$this->dbtable = new DBTable ( $db, "smis_pat_pesanan",$col );
        
        $this->dbtable->setOrder("id ASC", true);
		$this->is_stand_alone = getSettings($db, "patology-sistem-model", "Stand Alone")=="Stand Alone";
		$this->is_stand_alone_lis_button=getSettings($db, "patology-lis-stand-alone-button", "0")=="1";
		
		$this->page = $page;
		$this->protoslug = $protoslug;
		$this->protoimplement = $protoimplement;
		$this->protoname = $protoname;
		$this->action = $action;
		$this->kelas = $kelas;
		$this->umur = "";
		$this->ibukandung = "";
		$this->carabayar = "Number Register Not Active";
		$this->limapuluh = "";
		$this->alamat = "";
		$this->tgl_lahir=NULL;
		if ($noreg != "") {
			$data_post = array (
					"command" => "edit",
					"id" => $noreg 
			);
			$service = new ServiceConsumer ( $this->db, "get_registered", $data_post );
			$service->execute ();
			$data = $service->getContent ();
			if($data!=null){
				$this->umur = $data ['umur'];
				$this->jk = $data ['kelamin'];
				$this->alamat = $data ['alamat_pasien'];
                $this->kelurahan = $data ['nama_kelurahan'];
				$this->kecamatan = $data ['nama_kecamatan'];
				$this->kabupaten = $data ['nama_kabupaten'];
				$this->provinsi = $data ['nama_provinsi'];
				$this->ibukandung = $data ['ibu'];
				$this->carabayar = $data ['carabayar'];
				$utest = strpos ( $data ['umur'], " Tahun " );
				$this->uri=$data["uri"];
				$this->tgl_lahir=$data['tgl_lahir'];
				if ($utest === false) {
					$this->limapuluh = false;
				} else {
					$umrth = substr ( $data ['umur'], 0, $utest ) * 1;
					if ($umrth > 50)
						$this->limapuluh = true;
					else
						$this->limapuluh = false;
				}
			}
		}
		
		if ($polislug != "all") {
			$this->dbtable->addCustomKriteria ( "ruangan", "='" . $polislug . "'" );
		}
		
		if ($noreg != "") {
			$this->dbtable->addCustomKriteria ( "noreg_pasien", "='" . $noreg . "'" );
		}
		
		$thehead=array ("No.","Nomor",'Tanggal','Pasien',"Jenis Pasien","NRM",	"No. Reg",'Kelas',"Biaya","Biaya Lain","Total Biaya","Ruangan","No. Pat", "Lampiran", "Status");
		$this->uitable = new PatologyTable ( $thehead, ucfirst ( $this->mode ) . " Patology " . ($this->polislug == "all" ? "" : ucfirst ( $this->protoname )), NULL, true );
		$this->uitable->setName ( $action );
		$this->uitable->setCode($this->mode);
		$this->uitable->setPrintElementButtonEnable(true);
		if ($this->mode == self::$MODE_DAFTAR) {
			$this->uitable->setDelButtonEnable ( false );
			$btn = new Button("", "", "Tidak Jadi");
			$btn->setClass("btn-inverse");
			$btn->setIcon("fa fa-stop");
			$btn->setIsButton(Button::$ICONIC);
			$this->uitable->addContentButton("batal", $btn);
            if(getSettings($db, "patology-ui-pemeriksaan-edit-hasil", "0") == "1") {
                $this->uitable->setEditButtonEnable(true);
            } else {
                $this->uitable->setEditButtonEnable(false);
            }
			//$this->dbtable->addCustomKriteria ( "selesai", " !='1' " );
		} else if ($this->mode == self::$MODE_PERIKSA) {
            //$this->uitable->setAddButtonEnable ( $this->is_stand_alone );
			$this->uitable->setPrintElementButtonEnable ( false );
			$btn = new Button ( "", "", "Arsipkan" );
			$btn->setIcon ( "fa fa-archive" );
			$btn->setIsButton ( Button::$ICONIC );
			$this->uitable->addContentButton ( "selesai", $btn );
			$this->dbtable->addCustomKriteria ( null, "selesai !='1'" );
			$this->dbtable->addCustomKriteria ( null, "selesai !='-1'" );
			
			if($this->is_stand_alone_lis_button){
				$btn = new Button ( "", "", "LIS" );
				$btn->setIcon ( "fa fa-send" );
				$btn->setIsButton ( Button::$ICONIC );
				$this->uitable->addContentButton ( "lis", $btn );
            }
            
            $btn1 = new Button("", "", "Rehabilitasi Medik");
			$btn1 ->setClass("btn-inverse");
			$btn1 ->setIcon("fa fa-file");
            $btn1 ->setIsButton(Button::$ICONIC);
            $this->uitable->addContentButton("rehab_medik", $btn1);
            
            $btn2 = new Button("", "", "Pelayanan Khusus");
			$btn2 ->setClass("btn-inverse");
			$btn2 ->setIcon("fa fa-file");
            $btn2 ->setIsButton(Button::$ICONIC);
            $this->uitable->addContentButton("pelayanan_khusus", $btn2);

            $btn3 = new Button("", "", "Jenis Pelayanan");
			$btn3 ->setClass("btn-inverse");
			$btn3 ->setIcon("fa fa-file");
            $btn3 ->setIsButton(Button::$ICONIC);
            $this->uitable->addContentButton("lap_rl52", $btn3);


			$fnab = new Button ( "cetak_pdf_button", "", "Cetak FNAB" );
			$fnab->setIsButton ( Button::$ICONIC_TEXT );
			$fnab->setIcon ( "fa fa-file-pdf-o");
			$fnab->setClass ( "btn-danger" );
			$this->uitable->addContentButton("hasil_fnab", $fnab);


			$sitologi = new Button ( "cetak_pdf_button", "", "Cetak Sitologi" );
			$sitologi->setIsButton ( Button::$ICONIC_TEXT );
			$sitologi->setIcon ( "fa fa-file-pdf-o");
			$sitologi->setClass ( "btn-danger" );
			$this->uitable->addContentButton("hasil_sitologi", $sitologi);

			$histologi = new Button ( "cetak_pdf_button", "", "Cetak Histologi" );
			$histologi->setIsButton ( Button::$ICONIC_TEXT );
			$histologi->setIcon ( "fa fa-file-pdf-o");
			$histologi->setClass ( "btn-danger" );
			$this->uitable->addContentButton("hasil_histologi", $histologi);


			
		} else if ($this->mode == self::$MODE_ARCHIVE) {
			$this->uitable->setAddButtonEnable ( false );
			$this->uitable->setDelButtonEnable ( false );
			$this->uitable->setPrintButtonEnable ( false );
			$this->uitable->setPrintElementButtonEnable ( true );
			$this->dbtable->addCustomKriteria ( "selesai", "='1'" );
		}
		$this->labres = new PatologyResource ();
		$this->list_pesan = $this->labres->list_pesanan;
		$this->list_hasil = $this->labres->list_hasil;
	}
	public function command($command) {
		$adapter = new PatologyAdapter ();
		$adapter->add ( "Tanggal", "tanggal", "date d M Y" );
		$adapter->add ( "Pasien", "nama_pasien" );
		$adapter->add ( "NRM", "nrm_pasien", "digit8" );
		$adapter->add ( "No. Reg", "noreg_pasien", "digit8" );
		$adapter->add ( "No. Pat", "no_pat" );
        $adapter->add ( "Biaya", "biaya", "money Rp." );
		$adapter->add ( "Biaya Lain", "biaya_lain", "money Rp." );
		$adapter->add ( "Total Biaya", "total_biaya", "money Rp." );
		$adapter->add ( "Lampiran", "file", "files-image-show" );
		$adapter->add ( "Kelas", "kelas", "unslug" );
		$adapter->add ( "Jenis Pasien", "carabayar" ,"unslug");
		$adapter->add ( "selesai", "selesai");
		$adapter->add ( "Status", "status");
		$adapter->setUseNumber(true,"No.","back.");
		if ($_POST['command'] == "list") {
			//custom view to accomodate regular checkup fee + additional checkup fee:
			$filter = "1";
			if (isset($_POST['kriteria'])) {
				$filter .= " AND (tanggal LIKE '" . $_POST['kriteria'] . "' OR nama_pasien LIKE '%" . $_POST['kriteria'] . "%' OR nrm_pasien LIKE '%" . $_POST['kriteria'] . "%' OR noreg_pasien LIKE '%" . $_POST['kriteria'] . "%' OR no_pat LIKE '%" . $_POST['kriteria'] . "%')";
			}
			
			//Kalau kurang yang ini, dibuka di ruang lain bisa tampil semua
			if ($this->polislug != "all") {
				$filter .=" AND ruangan='".$this->polislug ."' ";
			}
			if ($this->noreg_pasien!= "") {
				$filter .=" AND noreg_pasien='".$this->noreg_pasien ."' ";				
			}
			
			
			$mode_filter = "";
			if ($this->mode == LaboratyTemplate::$MODE_DAFTAR || $this->mode == LaboratyTemplate::$MODE_PERIKSA) {
				$mode_filter = "AND ( selesai = '0' OR selesai='-2' )";
			} else if ($this->mode == LaboratyTemplate::$MODE_ARCHIVE) {
				$mode_filter = "AND selesai = '1' ";
			}
			
			
			
			$query_value = "
				SELECT *, (biaya + biaya_lain) AS 'total_biaya'
				FROM (
					SELECT a.*, SUM(CASE WHEN b.prop = 'del' OR b.jumlah IS NULL THEN 0 ELSE b.jumlah END * CASE WHEN b.prop = 'del' OR b.harga_layanan IS NULL THEN 0 ELSE b.harga_layanan END) AS 'biaya_lain'
					FROM smis_pat_pesanan a LEFT JOIN smis_pat_dpesanan_lain b ON a.id = b.id_pesanan
					WHERE a.prop NOT LIKE 'del' " . $mode_filter . "
					GROUP BY a.id
				) v
				WHERE " . $filter . "
			";
			$query_count = "
				SELECT COUNT(*)
				FROM (
					SELECT a.*, SUM(CASE WHEN b.prop = 'del' OR b.jumlah IS NULL THEN 0 ELSE b.jumlah END * CASE WHEN b.prop = 'del' OR b.harga_layanan IS NULL THEN 0 ELSE b.harga_layanan END) AS 'biaya_lain'
					FROM smis_pat_pesanan a LEFT JOIN smis_pat_dpesanan_lain b ON a.id = b.id_pesanan
					WHERE a.prop NOT LIKE 'del' " . $mode_filter . "
					GROUP BY a.id
				) v
				WHERE " . $filter . "
			";
			//$this->dbtable->setPreferredQuery(true, $query_value, $query_count);
            $this->dbtable->setDebuggable(true);
		}
		$dbres = new PatologyResponder ( $this->dbtable, $this->uitable, $adapter,$this->labres );
        $dbres->setDebuggable(true);
		$dbres->setJenisRawat($this->uri);
		$dbres->setTglLahir($this->tgl_lahir);
		if ($dbres->is ( "save" ) && isset($_POST ['kelas'] )) {
			$provit = new ProvitSharingService ( $this->db, $this->polislug, "smis-pv-patology",$this->carabayar );
			$provit->execute ();
			$ps = $provit->getContent ();
			$dbres->addColumnFixValue ( "pembagian", $ps );
			$harga = new TarifPatology ( $this->db, $_POST ['kelas'] );
			$harga->execute ();
			$harga_patology = $harga->getContent ();
			$dbres->addColumnFixValue ( "harga", $harga_patology );
		}
		$query = "SELECT count(*) as total FROM smis_pat_pesanan WHERE tanggal LIKE '" . date ( "Y" ) . "%' ";
		$nomor = $this->db->get_var ( $query );
		$nomor = ($nomor * 1) + 1;
		$nomor = ArrayAdapter::format ( "only-digit4", $nomor );
		$dbres->setJsonColumn ( array (	"hasil","periksa" ) );
		global $db;
		if ($command == "save" && getSettings($db,"patology-nomor","Manual")=="Otomatis" && isset($_POST ['no_pat']) && ($_POST ['no_pat'] == '' || $_POST ['no_pat'] == '0')) {
			loadLibrary("smis-libs-function-time");
			$patology = $_POST['periksa'];
			$slugname = "";
			foreach($patology as $slug=>$val){
				if($val=="1"){
					$slugname = $slug;
					break;
				}
			}

			if($slugname!=""){
				$query = "SELECT grup FROM smis_pat_layanan  WHERE slug = '$slugname' ;";
				$grup = $db->get_var($query);
				$start = 0;

				$pref = "";
				if($grup=="HISTOPATOLOGI"){
					$pref="ST.";
					$start = getSettings($db,"patology-nomor-histologi","0");
				}else if($grup=="SITOLOGI"){
					$pref="SS.";
					$start = getSettings($db,"patology-nomor-sitologi","0");
				}else if($grup=="FNAB"){
					$pref="SF.";
					$start = getSettings($db,"patology-nomor-fnab","0");
				}
				$like = $pref."%.".date("y");
				$query = "SELECT COUNT(*) as total FROM smis_pat_pesanan WHERE no_pat LIKE '$like' ";
				$total = intval($db->get_var($query));
				if($total==0){
					$total = 1+$start;
				}else{
					$total = $total+$start+1;
				}
				if($total<10){
					$total = "0".$total;
				}
				$number = $pref.$total.".".date("y");
				$dbres->addColumnFixValue("no_pat",$number);

			}


            //$dbres->addColumnFixValue ( "no_pat", "L" . $nomor . "/" . to_romawi ( date ( "m" ) ) . "-" . date ( "y" ) . "/LSRB" );
		}
		$data = $dbres->command ( $_POST ['command'] );
		echo json_encode ( $data );
		return;
	}
	
	private function setCetakHapusan(Modal &$modal){
		$cetak_hasil = new Button ( "", "", "Cetak Hapusan Darah" );
		$cetak_hasil->setIsButton ( Button::$ICONIC_TEXT );
		$cetak_hasil->setIcon ( "icon-white " . Button::$icon_print );
		$cetak_hasil->setClass ( "btn-danger" );
		$cetak_hasil->setAction ( "cetak_hapusan_darah()" );
		$modal->addFooter ( $cetak_hasil );
	}
	
	private function setFooter(Modal &$modal){
        if(getSettings($this->db, "patology-show-print-for-iks") == 1) {
            $asuransi=new CheckBox("asuransi_cek", "IKS", true);
        } else {
            $asuransi=new CheckBox("asuransi_cek", "IKS", false);
        }
        if(getSettings($this->db, "patology-show-print-for-pasien") == 1) {
            $pasien=new CheckBox("pasien_cek", "Pasien", true);
        } else {
            $pasien=new CheckBox("pasien_cek", "Pasien", false);
        }
        if(getSettings($this->db, "patology-show-print-for-rekam-medis") == 1) {
            $rekamedis=new CheckBox("rekamedis_cek", "Rekam Medis", true);
        } else {
            $rekamedis=new CheckBox("rekamedis_cek", "Rekam Medis", false);
        }
		if(getSettings($this->db, "patology-show-print-for-patology") == 1) {
            $pat=new CheckBox("pat_cek", "Patology", true);
        } else {
            $pat=new CheckBox("pat_cek", "Patology", false);
        }
		
		$asuransi->addClass("cek_samping");
		$pasien->addClass("cek_samping");
		$rekamedis->addClass("cek_samping");
		$pat->addClass("cek_samping");
		
		/*$cetak_hasil = new Button ( "cetak_gabungan_button", "", "Cetak" );
		$cetak_hasil->setIsButton ( Button::$ICONIC_TEXT );
		$cetak_hasil->setIcon ( "fa fa-print");
		$cetak_hasil->setClass ( "btn-success" );
		$cetak_hasil->setAction ( "pat_print_gabungan()" );*/
        
        $hasil_pdf = new Button ( "cetak_pdf_button", "", "Cetak PDF" );
		$hasil_pdf->setIsButton ( Button::$ICONIC_TEXT );
		$hasil_pdf->setIcon ( "fa fa-file-pdf-o");
		$hasil_pdf->setClass ( "btn-danger" );
		$hasil_pdf->setAction ( "hasil_pdf()" );
		
		$modal->addFooter ( $asuransi);
		$modal->addFooter ( $pasien);
		$modal->addFooter ( $rekamedis);
		$modal->addFooter ( $pat);
		//$modal->addFooter ( $cetak_hasil );
		//$modal->addFooter ( $hasil_pdf );
	}
	
	public function setPemeriksaanPreload(){
		if($this->mode==self::$MODE_PERIKSA){
			if(getSettings($this->db,"patology-ui-pemeriksaan-add-tindakan","1")=="0"){
				$this->uitable->setAddButtonEnable(false);
			}
		}
	}
	
	public function phpPreLoad() {
		$this->setPemeriksaanPreload();
		$service = new RuanganService ( $this->db ,$this->polislug);
		$service->execute ();
		$ruangan = $service->getContent ();
		$ruangan [] = array ("name" => "PENDAFTARAN ","value" => "pendaftaran" );
		require_once 'smis-base/smis-include-service-consumer.php';
		$service = new ServiceConsumer ( $this->db, "get_kelas" );
		$service->execute ();
        $kelas = $service->getContent ();
		$option_kelas = new OptionBuilder ();
		foreach ( $kelas as $k ) {
			$nama = $k ['nama'];
			$slug = $k ['slug'];
			$option_kelas->add ( $nama, $slug, $slug == $this->kelas ? "1" : "0" );
		}		
		$dokter_konsultan=getSettings($this->db, "patology-konsultan-nama", "");
		$id_dokter_konsultan=getSettings($this->db, "patology-konsultan-id", "");
		
        if($this->mode == self::$MODE_PERIKSA) {
            $this->uitable->addModal ( "id", "hidden", "", "" );
            $this->uitable->addModal ( "waktu_daftar", "hidden", "", "" );
            $this->uitable->addModal ( "waktu_ditangani", "hidden", "", "" );
            $this->uitable->addModal ( "waktu_selesai", "hidden", "", "" );
            $this->uitable->addModal ( "response_time", "hidden", "", "" );
            $this->uitable->addModal ( "setting_kelas", "hidden", "", getSettings($db, "patology-ui-pemeriksaan-setting-kelas","") );
            $this->uitable->addModal ( "tanggal", "date", "Tanggal", date ( "Y-m-d" ) ,"y", null, false);
            $this->uitable->addModal ( "no_pat", "text", "No. Pat", "", "y", null, getSettings($db,"patology-nomor","Manual")=="Otomatis" );
            $this->uitable->addModal ( "noreg_pasien", "chooser-" . $this->action . "-pat_pasien-Pilih Pasien", "No Reg", $this->noreg_pasien, "n", null, true );
            $this->uitable->addModal ( "nama_pasien", "text", "Pasien", $this->nama_pasien, "n", null, true);
            $this->uitable->addModal ( "nrm_pasien", "text", "NRM", $this->nrm_pasien, "n", null, true);
            if(getSettings($db, "patology-ui-pemeriksaan-setting-kelas","") == "Dapat Memilih") {
                $this->uitable->addModal ( "kelas", "select", "Kelas", $option_kelas->getContent (), "n", null, false);
            } if(getSettings($db, "patology-ui-pemeriksaan-setting-kelas","") == "Sesuai Kelas Tertentu") {
                $kls = new OptionBuilder();
                $kls->add(ArrayAdapter::slugFormat("unslug", getSettings($db, "patology-ui-pemeriksaan-default-kelas")), getSettings($db, "patology-ui-pemeriksaan-default-kelas"), 1);
                $this->uitable->addModal ( "kelas", "select", "Kelas", $kls->getContent(), "n", null, true);
            } if(getSettings($db, "patology-ui-pemeriksaan-setting-kelas","") == "Sesuai Kelas Pasien") {
                $this->uitable->addModal ( "kelas", "select", "Kelas", $option_kelas->getContent (), "n", null, true);
            }
            
            
            /*show and hide marketing*/
            if(getSettings($this->db,"patology-show-marketing","0")=="1"){
                $this->uitable->addModal ( "marketing", "chooser-".$this->action."-pat_marketing-Marketing", "Marketing", "", "n", null, true);
            }else{
                $this->uitable->addModal ( "marketing", "hidden", "", "", "y", null, true);
            }
            $this->uitable->addModal ( "id_marketing","hidden","","");
            
            //$this->uitable->addModal ( "pengirim", "chooser-".$this->action."-pat_pengirim-Pengirim", "Pengirim", "", "n", null, true);
            //$this->uitable->addModal ( "id_pengirim","hidden","","");
            
            $this->uitable->addModal ( "nama_dokter", "chooser-" . $this->action . "-pat_dokter", "Dokter", "", "n", null, true);
            $this->uitable->addModal ( "id_dokter", "hidden", "", "", "n", null, false);
            $this->uitable->addModal ( "nama_konsultan", "chooser-" . $this->action . "-pat_konsultan", "Konsultan", $dokter_konsultan, "y", null, true);
            $this->uitable->addModal ( "id_konsultan", "hidden", "", $id_dokter_konsultan, "y", null, false);	
            $this->uitable->addModal ( "nama_petugas", "chooser-" . $this->action . "-pat_petugas", "Petugas", "", "y", null, true );
            $this->uitable->addModal ( "id_petugas", "hidden", "", "", "y", null, false );
            
            $MODEL_JENIS_PASIEN=getSettings($this->db,"patology-model-jenis-pasien","1")=="1";		
            if($MODEL_JENIS_PASIEN){
                $service = new ServiceConsumer ( $this->db, "get_jenis_patient",NULL,"registration" );
                $service->setCached(true,"get_jenis_patient");
                $service->execute ();
                $jenis_pasien = $service->getContent ();
                $cbayar=new OptionBuilder();
                foreach($jenis_pasien as $jp){
                    $cbayar->add($jp['name'],$jp['value'],$jp['value']==$this->carabayar?"1":"0");
                }
                $this->uitable->addModal ( "carabayar", "select", "Jenis Pasien", $cbayar->getContent(), "y", null, true,null,false,"shift");
            }else{
                $this->uitable->addModal ( "carabayar", "text", "Jenis Pasien", $this->carabayar, "y", null, true );		
            }
            
            $this->uitable->addModal ( "file", "files-image-upload", "Lampiran", "", "y", null, true );
            
            $this->uitable->addModal ( "umur", "text", "Umur", $this->umur, "y", null, true );
            $this->uitable->addModal ( "alamat", "text", "Alamat", $this->alamat, "y", null, true );
            $this->uitable->addModal ( "kelurahan", "hidden", "", $this->kelurahan );
            $this->uitable->addModal ( "kecamatan", "hidden", "", $this->kecamatan );
            $this->uitable->addModal ( "kabupaten", "hidden", "", $this->Kabupaten );
            $this->uitable->addModal ( "provinsi", "hidden", "", $this->provinsi );
            $this->uitable->addModal ( "ibu", "text", "Ibu Kandung", $this->ibukandung, "y", null, true );		
            $jkselect=new OptionBuilder();
            $jkselect->add("L","0",$this->jk=="0")->add("P","1",$this->jk=="1");
            $this->uitable->addModal ( "jk", "select", "L/P", $jkselect->getContent(),"n",NULL, true);
            $this->uitable->addModal ( "diagnosa", "text", "Diagnosa", "", "y", null, true);
            $this->uitable->addModal ( "limapuluh", "hidden", "", $this->limapuluh );		
            $edit_ruang=getSettings($this->db, "patology-allow-edit-ruangan", "1");
            if($edit_ruang == "1") {
                $this->uitable->addModal ( "ruangan", "select", "Ruangan", $ruangan, "n", null, false);
            } else {
                $this->uitable->addModal ( "ruangan", "select", "Ruangan", $ruangan, "n", null, true);
            }
            
            $this->uitable->addModal("waktu_ditangani_hapusan", "datetime", "Darah Masuk", "", "y", null, false);
            $this->uitable->addModal ( "uri", "hidden", "",$this->uri);
            $this->uitable->addModal ( "cetak_hasil_ke", "hidden", "", "0");
        } else {
            $this->uitable->addModal ( "id", "hidden", "", "" );
            $this->uitable->addModal ( "waktu_daftar", "hidden", "", "" );
            $this->uitable->addModal ( "waktu_ditangani", "hidden", "", "" );
            $this->uitable->addModal ( "waktu_selesai", "hidden", "", "" );
            $this->uitable->addModal ( "response_time", "hidden", "", "" );
            $this->uitable->addModal ( "setting_kelas", "hidden", "", getSettings($db, "patology-ui-pemeriksaan-setting-kelas","") );
            $this->uitable->addModal ( "tanggal", "date", "Tanggal", date ( "Y-m-d" ) ,"y", null, false);
            $this->uitable->addModal ( "no_pat", "text", "No. Patology", "", "y", null, false );
            $this->uitable->addModal ( "noreg_pasien", "chooser-" . $this->action . "-pat_pasien-Pilih Pasien", "No Reg", $this->noreg_pasien, "n", null, true );
            $this->uitable->addModal ( "nama_pasien", "text", "Pasien", $this->nama_pasien, "n", null, true);
            $this->uitable->addModal ( "nrm_pasien", "text", "NRM", $this->nrm_pasien, "n", null, true);
            if(getSettings($db, "patology-ui-pemeriksaan-setting-kelas","") == "Dapat Memilih") {
                $this->uitable->addModal ( "kelas", "select", "Kelas", $option_kelas->getContent (), "n", null, false);
            } if(getSettings($db, "patology-ui-pemeriksaan-setting-kelas","") == "Sesuai Kelas Tertentu") {
                $kls = new OptionBuilder();
                $kls->add(ArrayAdapter::slugFormat("unslug", getSettings($db, "patology-ui-pemeriksaan-default-kelas")), getSettings($db, "patology-ui-pemeriksaan-default-kelas"), 1);
                $this->uitable->addModal ( "kelas", "select", "Kelas", $kls->getContent(), "n", null, true);
            } if(getSettings($db, "patology-ui-pemeriksaan-setting-kelas","") == "Sesuai Kelas Pasien") {
                $this->uitable->addModal ( "kelas", "select", "Kelas", $option_kelas->getContent (), "n", null, false);
            }
            
            
            /*show and hide marketing*/
            if(getSettings($this->db,"patology-show-marketing","0")=="1"){
                $this->uitable->addModal ( "marketing", "chooser-".$this->action."-pat_marketing-Marketing", "Marketing", "", "n", null, true);
            }else{
                $this->uitable->addModal ( "marketing", "hidden", "", "", "y", null, true);
            }
            $this->uitable->addModal ( "id_marketing","hidden","","");
            
            //$this->uitable->addModal ( "pengirim", "chooser-".$this->action."-pat_pengirim-Pengirim", "Pengirim", "", "n", null, true);
            //$this->uitable->addModal ( "id_pengirim","hidden","","");
            
            $this->uitable->addModal ( "nama_dokter", "chooser-" . $this->action . "-pat_dokter", "Dokter", "", "n", null, false);
            $this->uitable->addModal ( "id_dokter", "hidden", "", "", "n", null, false);
            $this->uitable->addModal ( "nama_konsultan", "chooser-" . $this->action . "-pat_konsultan", "Konsultan", $dokter_konsultan, "y", null, false);
            $this->uitable->addModal ( "id_konsultan", "hidden", "", $id_dokter_konsultan, "y", null, false);	
            $this->uitable->addModal ( "nama_petugas", "chooser-" . $this->action . "-pat_petugas", "Petugas", "", "y", null, false );
            $this->uitable->addModal ( "id_petugas", "hidden", "", "", "y", null, false );
            
            $MODEL_JENIS_PASIEN=getSettings($this->db,"patology-model-jenis-pasien","1")=="1";		
            if($MODEL_JENIS_PASIEN){
                $service = new ServiceConsumer ( $this->db, "get_jenis_patient",NULL,"registration" );
                $service->setCached(true,"get_jenis_patient");
                $service->execute ();
                $jenis_pasien = $service->getContent ();
                $cbayar=new OptionBuilder();
                foreach($jenis_pasien as $jp){
                    $cbayar->add($jp['name'],$jp['value'],$jp['value']==$this->carabayar?"1":"0");
                }
                $this->uitable->addModal ( "carabayar", "select", "Jenis Pasien", $cbayar->getContent(), "y", null, false,null,false,"shift");
            }else{
                $this->uitable->addModal ( "carabayar", "text", "Jenis Pasien", $this->carabayar, "y", null, true );		
            }
            
            $this->uitable->addModal ( "file", "files-image-upload", "Lampiran", "", "y", null, true );
            
            $this->uitable->addModal ( "umur", "text", "Umur", $this->umur );
            $this->uitable->addModal ( "alamat", "text", "Alamat", $this->alamat );
            $this->uitable->addModal ( "kelurahan", "hidden", "", $this->kelurahan );
            $this->uitable->addModal ( "kecamatan", "hidden", "", $this->kecamatan );
            $this->uitable->addModal ( "kabupaten", "hidden", "", $this->Kabupaten );
            $this->uitable->addModal ( "provinsi", "hidden", "", $this->provinsi );
            $this->uitable->addModal ( "ibu", "text", "Ibu Kandung", $this->ibukandung );		
            $jkselect=new OptionBuilder();
            $jkselect->add("L","0",$this->jk=="0")->add("P","1",$this->jk=="1");
            $this->uitable->addModal ( "jk", "select", "L/P", $jkselect->getContent(),"n",NULL,!$this->is_stand_alone);
            $this->uitable->addModal ( "diagnosa", "text", "Diagnosa", "");
            $this->uitable->addModal ( "limapuluh", "hidden", "", $this->limapuluh );		
            $edit_ruang=getSettings($this->db, "patology-allow-edit-ruangan", "1");
            if($edit_ruang == "1") {
                $this->uitable->addModal ( "ruangan", "select", "Ruangan", $ruangan, "n", null, false);
            } else {
                $this->uitable->addModal ( "ruangan", "select", "Ruangan", $ruangan, "n", null, true);
            }
            $this->uitable->addModal("waktu_ditangani_hapusan", "datetime", "Darah Masuk", "", "y", null, false);
            $this->uitable->addModal ( "uri", "hidden", "",$this->uri);
            $this->uitable->addModal ( "cetak_hasil_ke", "hidden", "", "0");
			
        }
       
        $status = new OptionBuilder();
        $status->addSingle("", "1");
        $status->addSingle("Menunggu Hasil", "0");
        $status->addSingle("Sudah Dicek", "0");
        $status->addSingle("Selesai", "0");
        $this->uitable->addModal ( "status", "select", "Status", $status->getContent(), "y", null, false, null, false, null);
		$this->uitable->addModal ( "kesimpulan", "textarea", "Kesimpulan", "");

		$modal = $this->uitable->getModal ();
		if ($this->mode == self::$MODE_ARCHIVE)
			$modal->setFooter ( array () );
		
		if(getSettings($this->db, "patology-show-hapusan-darah", "0")=="1"){
			$this->setCetakHapusan($modal);
		}
		
		if(getSettings($this->db, "patology-show-print-backup", "0")=="1"){
			$this->setFooter($modal);
		}
		$modal->setTitle ( "Pesanan" );
		
		$tabulator = new Tabulator ( "pesan", "pesanan", Tabulator::$POTRAIT );
		
		require_once 'patology/class/PatologyPesanan.php';
		$pesanan = new PatologyPesanan ( $this->labres->list_layanan,$this->labres->grup_name );
		$tabulator->add ( "pat_periksa", "Pemeriksaan", $pesanan, Tabulator::$TYPE_COMPONENT," fa fa-list" );
		
		if(getSettings($this->db,"patology-show-pemeriksaan-lain","0")=="1"){
			require_once 'patology/class/PatologyLain.php';
			$lain = new PatologyLain("","","");		
			$tabulator->add( "pat_lain", "Lain-Lain",  $lain, Tabulator::$TYPE_COMPONENT,"fa fa-ticket");
		}
		
		if(getSettings($this->db, "patology-show-standard-print", "0")=="1"){
			$tabulator->add( "pat_hasil_lis", "Hasil Standard",  "patology/resource/hasil_lis.php", Tabulator::$TYPE_INCLUDE," fa fa-book" );	
		}
		
		if(getSettings($this->db, "patology-show-lis-print", "0")=="1"){
			$tabulator->add( "pat_hasil_lis_crawl", "Hasil LIS",  "patology/resource/hasil_lis_crawler.php", Tabulator::$TYPE_INCLUDE ," fa fa-cloud");	
		}
		
		if(getSettings($this->db, "patology-show-hapusan-darah", "0")=="1"){
			$tabulator->add ( "pat_hasil_hapusan", "Hapusan Darah", "patology/resource/modal_hasil_hapusan_darah.php", Tabulator::$TYPE_INCLUDE ," fa fa-tint");
		}
        
         if(getSettings($this->db,"patology-show-diagnosa","0")=="1" && $this->mode!=self::$MODE_DAFTAR){
            $tabulator->add("pat_diagnosa", "Diagnosa", "<div id='page_diagnosa_patology'></div>", Tabulator::$TYPE_HTML,"fa fa-user-md","diagnosa_patology()");
        }
		
		$modal->addHTML ( $tabulator->getHtml () );
		$modal->setClass ( Modal::$FULL_MODEL );
		
		require_once 'patology/resource/print_header.php';
		require_once 'patology/resource/print_footer.php';
		require_once 'patology/resource/print_footer_hasil.php';
		
		//modal layanan lain:
		$layanan_lain_modal = new Modal("layanan_lain_add_form", "smis-form-container", "layanan_lain");
		$layanan_lain_modal->setTitle("Layanan Lain");
		$id_hidden = new Hidden("layanan_lain_id", "layanan_lain_id", "");
		$layanan_lain_modal->addElement("", $id_hidden);
		$nama_layanan_text = new Text("layanan_lain_nama", "layanan_lain_nama", "");
		$nama_layanan_text->addAtribute("autofocus");
		$layanan_lain_modal->addElement("Nama", $nama_layanan_text);
		$harga_layanan_text = new Text("layanan_lain_harga", "layanan_lain_harga", "");
		$harga_layanan_text->setTypical("money");
		$harga_layanan_text->setAtribute(" data-thousands=\".\" data-decimal=\",\" data-prefix=\"Rp. \"  data-precision=\"2\" " );
		$layanan_lain_modal->addElement("Harga Satuan", $harga_layanan_text);
		$jumlah_text = new Text("layanan_lain_jumlah", "layanan_lain_jumlah", "1");
		$layanan_lain_modal->addElement("Jumlah", $jumlah_text);
		$subtotal_text = new Text("layanan_lain_subtotal", "layanan_lain_subtotal", "");
		$subtotal_text->setTypical("money");
		$subtotal_text->setAtribute("data-thousands=\".\" data-decimal=\",\" data-prefix=\"Rp. \"  data-precision=\"2\" disabled='disabled' ");
		$layanan_lain_modal->addElement("Subtotal", $subtotal_text);
		$button = new Button("", "", "Simpan");
		$button->setClass("btn-success");
		$button->setIcon("fa fa-floppy-o");
		$button->setIsButton(Button::$ICONIC);
		$button->setAtribute("id='layanan_lain_save_btn'");
		$layanan_lain_modal->addFooter($button);
		
		echo $this->uitable->getHtml ();
		echo $modal->getHtml ();
		echo $layanan_lain_modal->getHtml();
		
		/*JS SETUP*/
		echo $this->jsSetup("PAT_MODE","",$this->mode);
		echo $this->jsSetup("PAT_NOREG","",$this->noreg_pasien);
		echo $this->jsSetup("PAT_NAMA","",$this->nama_pasien);
		echo $this->jsSetup("PAT_NRM","",$this->nrm_pasien);
		echo $this->jsSetup("PAT_POLISLUG","",$this->polislug);
		echo $this->jsSetup("PAT_PAGE","",$this->page);
		echo $this->jsSetup("PAT_PREFIX","",$this->action);
		echo $this->jsSetup("PAT_PROTOSLUG","",$this->protoslug);
		echo $this->jsSetup("PAT_PROTONAME","",$this->protoname);
		echo $this->jsSetup("PAT_PROTOIMPLEMENT","",$this->protoimplement);
		echo $this->jsSetup("PAT_JK","",$this->jk);
		echo $this->jsSetup("PAT_LIST_PESAN","",json_encode($this->list_pesan));
		echo $this->jsSetup("PAT_LIST_HASIL","",json_encode($this->list_hasil));
		echo $this->jsSetup("PAT_EDIT_HASIL","",getSettings($this->db,"patology-ui-pemeriksaan-edit-tindakan","0"));
		echo $this->jsSetup("PAT_EDIT_LAYANAN","",getSettings($this->db,"patology-ui-pemeriksaan-edit-hasil","0"));
        //echo $this->jsSetup("PAT_CETAK_RSUK","",getSettings($this->db,"patology-print-hasil-rsukaliwates","0"));
        echo $this->jsSetup("PAT_AUTO_SAVE_AFTER_PRINT","",getSettings($this->db,"patology-auto-save-after-print","0"));
		/*END OF JS SETUP*/
		
		echo addJS ( "framework/bootstrap/js/bootstrap-datepicker.js" );
		echo addJS ( "framework/smis/js/table_action.js" );
		echo addJS ( "framework/bootstrap/js/bootstrap-datetimepicker.js" );
		echo addJS ( "smis-base-js/smis-base-shortcut.js",false);
        echo addJS ( "patology/resource/js/layanan_lain.js",false);
        echo addJS ( "patology/resource/js/patology_action.js",false);
        echo addJS ( "patology/resource/js/patology.js",false);
		echo addJS ( "patology/resource/js/patology_function.js",false);
		echo '<script type="text/javascript">var '.$this->action.'=PAT_ACTION; </script>';
		echo addJS ( "patology/resource/js/patology_typeahead.js",false);
		echo addJS ( "patology/resource/js/patology_supercommand.js",false);		
		echo addJS ( "patology/resource/js/patology_diagnosa.js",false); 
		echo addCSS ( "framework/bootstrap/css/datepicker.css" );
		echo addCSS ( "framework/bootstrap/css/bootstrap-datetimepicker.css" );
		echo addCSS ( "patology/resource/css/patology.css",false);
	}
	
	private function jsSetup($id,$name,$value){
		$hidden=new Hidden($id,$name,$value);
		return $hidden->getHtml();
	}
	
	public function superCommand($super_command) {
		$super = new SuperCommand ();
        
        if($super_command=="pat_dokter")
        {
            $header=array (	'Nama','Jabatan',"NIP");
            $dktable = new Table($header, "", NULL, true);
            $dktable->setName("pat_dokter");
            $dktable->setModel(Table::$SELECT);
            $dkadapter = new SimpleAdapter ();
            $dkadapter->add("Jabatan", "nama_jabatan");
            $dkadapter->add("Nama", "nama");
            $dkadapter->add("NIP", "nip");
            $dkresponder = new EmployeeResponder($this->db, $dktable, $dkadapter, "dokter");
            $super->addResponder("pat_dokter", $dkresponder);
        }
        else if($super_command=="pat_konsultan")
        {
            $slug_konsultan = getSettings($this->db, 'patology-slug-dokter-konsultan-pat');
            $header=array (	'Nama','Jabatan',"NIP");
            $kktable = new Table ($header, "", NULL, true);
            $kktable->setName("pat_konsultan");
            $kktable->setModel(Table::$SELECT);
            $dkadapter = new SimpleAdapter ();
            $dkadapter->add("Jabatan", "nama_jabatan");
            $dkadapter->add("Nama", "nama");
            $dkadapter->add("NIP", "nip");
            if( $slug_konsultan != null || $slug_konsultan != '') {
                $kkresponder = new EmployeeResponder($this->db, $kktable, $dkadapter, $slug_konsultan);
            } else {
                $kkresponder = new DKResponder($this->db, $kktable, $dkadapter, "employee");
            }
            $super->addResponder("pat_konsultan", $kkresponder);
        }
        else if($super_command=="pat_petugas")
        {
            $slug_petugas = getSettings($this->db, 'patology-slug-petugas-pat');
            $header=array (	'Nama','Jabatan',"NIP");
            $pettable = new Table ($header, "", NULL, true);
            $pettable->setName("pat_petugas");
            $pettable->setModel(Table::$SELECT);
            $dkadapter = new SimpleAdapter ();
            $dkadapter->add("Jabatan", "nama_jabatan");
            $dkadapter->add("Nama", "nama");
            $dkadapter->add("NIP", "nip");
            if( $slug_petugas != null || $slug_petugas != '') {
                $petresponder = new EmployeeResponder($this->db, $pettable, $dkadapter, $slug_petugas);
            } else {
                $petresponder = new EmployeeResponder($this->db, $pettable, $dkadapter, "");
            }
            $super->addResponder("pat_petugas", $petresponder);
        }
        else if($super_command=="pat_pasien")
        {
            $phead=array ('Nama','NRM',"No Reg");
            $ptable = new Table($phead, "", NULL, true);
            $ptable->setName("pat_pasien");
            $ptable->setModel(Table::$SELECT);
            $padapter = new SimpleAdapter ();
            $padapter->add("Nama", "nama_pasien");
            $padapter->add("NRM", "nrm", "digit8");
            $padapter->add("No Reg", "id");
            $presponder = new ServiceResponder($this->db, $ptable, $padapter, "get_registered");
            $super->addResponder("pat_pasien", $presponder);
        }
        else if($super_command=="pat_marketing")
        {
            $marketing_table = new Table (array('Nama', 'Jabatan', 'NIP'), "", NULL, true);
            $marketing_table->setName("pat_marketing");
            $marketing_table->setModel(Table::$SELECT);
            $marketing_adapter = new SimpleAdapter();
            $marketing_adapter->add("Nama", "nama");
            $marketing_adapter->add("Jabatan", "nama_jabatan");
            $marketing_adapter->add("NIP", "nip");
            $marketing_dbresponder = new EmployeeResponder($this->db, $marketing_table, $marketing_adapter, "marketing");
            $super->addResponder ("pat_marketing", $marketing_dbresponder);
        }
        
		$init = $super->initialize ();
		if ($init != null) {
			echo $init;
			return;
		}
	}
}

?>
