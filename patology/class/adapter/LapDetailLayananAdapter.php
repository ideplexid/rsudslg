<?php 

/**
 * this class used to meka a detail 
 * reporting of each row in lab layanan
 * so the price that bind in json
 * would be take as one single data.
 * 
 * @author      : Nurul Huda
 * @license     : LGPLv3
 * @copyright   : goblooge@gmail.com
 * @database    : smis_pat_pesanan
 * @since       : 15 Jul 2017
 * @used        : patology/resource/php/laporan/lap_detail_layanan.php
 * @version     : 1.0.0
 * */

class LapDetailLayananAdapter extends ArrayAdapter{
    
    private $total;
    private $tindakan="";
    
    /**
     * @brief mengirimkan data spesifik nama tindakan yang ingin 
     *          ditampilkan penghasilanya
     * @param string $tindakan 
     * @return  this
     */
    public function setSlugTindakan($tindakan){
        $this->tindakan=$tindakan;
        return $this;
    }
    
    public function adapt($x){
        $data=array();
        $this->number++;
        $data["No."]=$this->number.".";
        $data["Tanggal"]=ArrayAdapter::dateFormat("date d M Y",$x->tanggal);
        $data["Nama"]= $x->nama_pasien;
        $data["NRM"]= ArrayAdapter::digitFormat("only-digit8",$x->nrm_pasien);
        $data["No. Reg"]=ArrayAdapter::digitFormat("only-digit8",$x->noreg_pasien);
        $data["Nomor"]=$x->no_pat;
        $data["Kelas"]=ArrayAdapter::slugFormat("unslug",$x->kelas);
        $data["Ruangan"]= ArrayAdapter::slugFormat("unslug",$x->ruangan);
        $data["Jenis Pasien"]= ArrayAdapter::slugFormat("unslug",$x->carabayar);
        $data["Response Time"] = $x->response_time <= 0 ? "N/A" : $x->response_time . " Menit";
        $uang=$x->biaya;
        if($this->tindakan!=""){
            $slug_harga=$x->kelas."_".$this->tindakan;
            $list=json_decode($x->harga,true);
            $uang=$list[$slug_harga];            
        }
        $data["Biaya"]= ArrayAdapter::moneyFormat("money Rp.",$uang);
        $this->total+=$uang;
        return $data;
        
    }
    
    public function getContent($data){
        $data=parent::getContent($data);
        $one=array();
        $one['Tanggal']="<strong>Total</strong>";
        $one['Biaya']= ArrayAdapter::moneyFormat("money Rp.",$this->total);
        $data[]=$one;
        return $data;
    }
    
}

?>