<?php

require_once("smis-libs-out/fpdf/fpdf.php");

class HasilPDF extends FPDF {
    //Page Header
    public function Header() {
        $fpdf->SetFont("Times", "B", 18);
        $fpdf->Cell(50, 0, $fpdf->Image($logo, $fpdf->GetX(), $fpdf->GetY(), 22, 22), 0, 0, "R");
        $fpdf->Ln(5);
        $fpdf->Cell(0, 0, getSettings($db,"smis_autonomous_title"), 0, 0, "C");
        $fpdf->Ln(7);
        $fpdf->SetFont("Times", "", 12);
        $fpdf->Cell(0, 0, getSettings($db,"smis_autonomous_address"), 0, 0, "C");
        $fpdf->Ln(5);
        $fpdf->SetFont("Times", "", 12);
        $fpdf->Cell(0, 0, getSettings($db,"smis_autonomous_contact"), 0, 0, "C");
        
        $fpdf->Ln(15);
        $line_break = 5;
        $fpdf->SetFont("Times", "B", 10);
        $fpdf->Cell(35, 0, "Nama", 0, 0, "L");
        $fpdf->Cell(2, 0, ":", 0, 0, "L");
        $fpdf->Cell(60, 0, $_POST['nama_pasien'], 0, 0, "L");
        $fpdf->Cell(5, 0, "", 0, 0, "L");
        $fpdf->Cell(35, 0, "Ibu Kandung", 0, 0, "L");
        $fpdf->Cell(2, 0, ":", 0, 0, "L");
        $fpdf->Cell(60, 0, $_POST['ibu'], 0, 0, "L");
        $fpdf->Ln($line_break);
        $fpdf->Cell(35, 0, "NRM", 0, 0, "L");
        $fpdf->Cell(2, 0, ":", 0, 0, "L");
        $fpdf->Cell(60, 0, $_POST['nrm'], 0, 0, "L");
        $fpdf->Cell(5, 0, "", 0, 0, "L");
        $fpdf->Cell(35, 0, "Noreg", 0, 0, "L");
        $fpdf->Cell(2, 0, ":", 0, 0, "L");
        $fpdf->Cell(60, 0, $_POST['noreg'], 0, 0, "L");
        $fpdf->Ln($line_break);
        $fpdf->Cell(35, 0, "Alamat", 0, 0, "L");
        $fpdf->Cell(2, 0, ":", 0, 0, "L");
        $fpdf->Cell(60, 0, $_POST['alamat'], 0, 0, "L");
        $fpdf->Cell(5, 0, "", 0, 0, "L");
        $fpdf->Cell(35, 0, "Umur", 0, 0, "L");
        $fpdf->Cell(2, 0, ":", 0, 0, "L");
        $fpdf->Cell(60, 0, $_POST['umur'], 0, 0, "L");
        $fpdf->Ln($line_break);
        $fpdf->Cell(35, 0, "Pengirim", 0, 0, "L");
        $fpdf->Cell(2, 0, ":", 0, 0, "L");
        $fpdf->Cell(60, 0, $_POST['pengirim'], 0, 0, "L");
        $fpdf->Cell(5, 0, "", 0, 0, "L");
        $fpdf->Cell(35, 0, "Konsultan", 0, 0, "L");
        $fpdf->Cell(2, 0, ":", 0, 0, "L");
        $fpdf->Cell(60, 0, $_POST['konsultan'], 0, 0, "L");
        $fpdf->Ln($line_break);
        $fpdf->Cell(35, 0, "L/P", 0, 0, "L");
        $fpdf->Cell(2, 0, ":", 0, 0, "L");
        $fpdf->Cell(60, 0, $_POST['jk'], 0, 0, "L");
        $fpdf->Cell(5, 0, "", 0, 0, "L");
        $fpdf->Cell(35, 0, "Ruang-Kelas", 0, 0, "L");
        $fpdf->Cell(2, 0, ":", 0, 0, "L");
        $ruang_kelas = ArrayAdapter::slugFormat("unslug", $_POST['ruangan'])." - ".ArrayAdapter::slugFormat("unslug", $_POST['kelas']);
        $fpdf->Cell(60, 0, $ruang_kelas, 0, 0, "L");
        $fpdf->Ln($line_break);
        $fpdf->Cell(35, 0, "No. Pat", 0, 0, "L");
        $fpdf->Cell(2, 0, ":", 0, 0, "L");
        $fpdf->Cell(60, 0, $_POST['no_pat'], 0, 0, "L");
        $fpdf->Cell(5, 0, "", 0, 0, "L");
        $fpdf->Cell(35, 0, "Cetakan Untuk", 0, 0, "L");
        $fpdf->Cell(2, 0, ":", 0, 0, "L");
        $fpdf->Cell(60, 0, "IKS", 0, 0, "L");
    }
}

?>