<?php
class PatologyPesanan extends HTML {
	private $list_pesanan;
	private $group_name;
	public function __construct($list_pesanan,$grup) {
		$this->list_pesanan = $list_pesanan;
		$this->group_name=$grup;
	}
	public function getHtml() {
		$all = $this->list_pesanan;
		$ret = "<div class='row' style='margin-top: -20px;margin-left: 30px;'>";
		$slug = "patology_";
		$row=$this->group_name;
		$set_result = "";
		foreach ( $all as $name => $value ) {
			$set_result .= "<div class='head-title'><strong>" . strtoupper ( $name ) . "</strong></div>";
			foreach ( $value as $key => $val ) {
				$check = new CheckBox ( $slug . $key, $val ['name'], false );
				if ($val ['indent'])
					$check->addClass ( "indent" );
                $check->setAction("cekList()");
				$set_result .= $check->getHtml ();
			}
			if (in_array ( $name, $row ) || $name == "faeces") {
				$ret .= "<div class='span3'>";
				$ret .= $set_result;
				$ret .= "</div>";
				$set_result = "";
			}
		}
		$ret .= "</div>";
		return $ret;
	}
}
?>