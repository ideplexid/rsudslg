<?php
class PatologyResource {
	public $list_layanan;
	public $list_hasil;
	public $list_pesanan;
	public $nama_hasil_uji;
	public $list_table;
	public $list_harga;
	public $list_group;
	public $list_reporting;
	public $list_lis;
	public $grup_name;
    public $list_name;
    public $list_debit_kredit;
    
	public function __construct() {
        $this->list_name=array();
        $this->list_layanan = array ();
		$this->list_harga = array ();
		$this->list_group = array ();
		$this->list_reporting=array();
		$this->list_lis=array();
		$this->grup_name=array();
        $this->list_debit_kredit=array();
		$this->initListPesanan ();
		$this->initHasilUji ();
		$this->initName ();
		$this->initLIS ();
		$this->initListHasil ();
		$this->initListTable ();
		$this->initGroup();
		$this->initHarga ();
		$this->initListReporting();
		$this->initGroupName();
	}
    
    /**
     * @brief for connecting to LIS
     * @return  
     */
	public function initLIS(){
		foreach ( $this->list_layanan as $name => $content ) {
			foreach ( $content as $cid => $cname ) {
				$this->list_lis [$cid] = $cname['lis'];
			}
		}
	}
    
    public function getDebitKredit($slug,$carabayar=""){
        $x = $this->list_debit_kredit[$slug];
        if($carabayar!=""){
            $carabayar = "_".$carabayar;
        }
        $result['d']=$x['debit'.$carabayar];
        $result['k']=$x['kredit'.$carabayar];
        return $result;
    }
    
    public function getListName($slug){
        return $this->list_name[$slug];
    }
	
    /**
     * @brief only list the group data
     * @return  
     */
	public function initGroupName(){
		foreach($this->list_layanan as $grup=>$list){
			$this->grup_name[]=$grup;
		}
	}
	
    /**
     * @brief only for reporting
     * @return  
     */
	public function initListReporting(){
		foreach ( $this->list_layanan as $name => $content ) {
			foreach ( $content as $cid => $cname ) {
				$this->list_reporting [$cid] = $cname;
			}
		}
	}
	
	public function initHarga() {
		foreach ( $this->list_layanan as $name => $content ) {
			foreach ( $content as $cid => $cname ) {
				$this->list_harga [$cid] = isset ( $cname ['tabname'] ) ? $cname ['tabname'] : $cname ['name'];
			}
		}
	}
    
    
	public function initListTable() {
		$this->list_table = array ();
		foreach ( $this->nama_hasil_uji as $key => $array ) {
			if ($array ["lt"]) {
				$this->list_table [$key] = $array ['grup'];
			}
		}
	}
    
    /**
     * @brief get only the slug
     */
	public function initName() {
		foreach ( $this->list_layanan as $name => $content ) {
			foreach ( $content as $cid => $cname ) {
				$this->list_pesanan [] = $cid;
			}
		}
	}
	
	public function initGroup(){
		foreach ( $this->list_layanan as $name => $content ) {
			foreach ( $content as $cid => $creplace ) {
				$this->list_group [$cid] = isset ( $creplace ['replace'] ) ? $creplace ['replace'] : "";
			}
		}
	}
	
	public function initListPesanan() {
		global $db;
		$dbtable=new DBTable($db,"smis_pat_layanan");
		$dbtable->setShowAll(true);
        $dbtable->setFetchMethode(DBTable::$ARRAY_FETCH);
		$d=$dbtable->view("","0");
		$data=$d['data'];
		
		$all=array();
		foreach($data as $x){
			if(!isset($all[$x['grup']])){
				$all[$x['grup']]=array();
			}
			$all[$x['grup']][$x['slug']]=array();
			$all[$x['grup']][$x['slug']]['name']=$x['nama'];
			$all[$x['grup']][$x['slug']]['indent']=($x['indent']=="1");
			$all[$x['grup']][$x['slug']]['replace']=$x['ganti'];
			$all[$x['grup']][$x['slug']]['lis']=$x['lis'];
            
            $this->list_name[$x['slug']]=$x['nama'];
            $this->list_debit_kredit[$x['slug']] = $x;
		}
		$this->list_layanan = $all;
	}
	
	
	public function getModalHasilTableAll() {
		return $this->nama_hasil_uji;
	}
	
	public function initHasilUji() {
		global $db;
		$dtable=new DBTable($db,"smis_pat_hasil");
        $dtable->setOrder(" nomor, grup ASC ",true);
		$dtable->setShowAll(true);
		$list=$dtable->view("", "0");
		$data=$list['data'];
		
		$ahj = array ();
		foreach($data as $x){
			$one=array();
			$one['nn']=$x->nn;
			$one['nt']=$x->nt;
			$one['metode']=$x->metode;
			$one['lt']=$x->lt=="1";
			$one['grup']=$x->grup;
			$one['name']=$x->name;
			$one['valmin']=$x->valmin;
			$one['valmax']=$x->valmax;	
			$one['pmin']=$x->pmin;
			$one['pmax']=$x->pmax;
			$one['lmin']=$x->lmin;
			$one['lmax']=$x->lmax;
			$one['sameval']=$x->lmin;
			$one['lessthan']=$x->lessthan;
			$one['morethan']=$x->morethan;
			$ahj [$x->slug]=$one;
            $this->name_map[$x->slug] = $one['name'];
		}		
		$this->nama_hasil_uji = &$ahj;
	}
	
	public function initListHasil() {
		$this->list_hasil = array ();
		foreach ( $this->nama_hasil_uji as $key => $array ) {
			if ($array ["name"] != "") {
				$this->list_hasil [] = $key;
			}
		}
	}
}

?>