<?php


/**
 * 
 * this function used to create system table in Labolatory
 * Creating based on Data Type and input user as well
 * 
 * @see PatologyResource for more detail
 * @since 14 May 2014
 * @author goblooge
 * @copyright Nurul Huda <goblooge@gmail.com>
 * @license LGPLv2
 */

/**
 * this function for negatif selector in Modal Table in Lab
 * contain Negatif, Positif 1/20, Positif 1/40, Positif 1/80, Positif 1/160, Positif 1/320, 1/640
 * @param string $id
 * @param string $class
 * @param string $grup
 * @return string of OptionBuilder
 */
function get_negatif($id, $class,$grup) {
	$option = new OptionBuilder ();
	$option->add ( "", "", "1" );
	$option->add ( "Negatif", "Negatif" );
	$option->add ( "Positif 1/20", "Positif 1/20" );
	$option->add ( "Positif 1/40", "Positif 1/40" );
	$option->add ( "Positif 1/80", "Positif 1/80" );
	$option->add ( "Positif 1/160", "Positif 1/160" );
	$option->add ( "Positif 1/320", "Positif 1/320" );
	$option->add ( "Positif 1/640", "Positif 1/640" );
	$option->add ( "Positif 1/1280", "Positif 1/1280" );
	$option->add ( "Positif 1/2560", "Positif 1/2560" );
	
	$select = new Select ( $id, "", $option->getContent () );
	$select->addClass ( $class );
	$select->addClass ( " negatif " );
	$select->addAtribute("grup",$grup);
	return $select->getHtml ();
}

/**
 * this function for reaktif - non reaktif selector in Modal Table in Lab
 * contain Reaktif, Non Reaktif
 * @param string $id
 * @param string $class
 * @param string $grup
 * @return string of OptionBuilder
 */
function get_reaktif_non_reaktif($id, $class,$grup) {
	$option = new OptionBuilder ();
	$option->add ( "", "", "1" );
	$option->add ( "Reaktif", "Reaktif" );
	$option->add ( "Non Reaktif", "Non Reaktif" );
	
	$select = new Select ( $id, "", $option->getContent () );
	$select->addClass ( $class );
	$select->addClass ( " reaktif " );
	$select->addAtribute("grup",$grup);
	return $select->getHtml ();
}

/**
 * this function for negatif selector in Modal Table in Lab
 * contain Negatif, Positif
 * @param string $id
 * @param string $class
 * @param string $grup
 * @return string of OptionBuilder
 */
function get_negatif_positif_only($id, $class,$grup) {
	$option = new OptionBuilder ();
	$option->add ( "", "", "1" );
	$option->add ( "Negatif", "Negatif" );
	$option->add ( "Positif", "Positif" );
	
	$select = new Select ( $id, "", $option->getContent () );
	$select->addClass ( $class );
	$select->addClass ( " negatif " );
	$select->addAtribute("grup",$grup);
	return $select->getHtml ();
}
 
/**
 * this function for negatif selector in Modal Table in Lab
 * contain Negatif, +1, +2, +3, +4
 * @param string $id
 * @param string $class
 * @param string $grup
 * @return string of OptionBuilder
 */
function get_negatif_2($id, $class,$grup) {
	$option = new OptionBuilder ();
	$option->add ( "", "", "1" );
	$option->add ( "Negatif", "Negatif" );
	$option->add ( "+1", "+1" );
	$option->add ( "+2", "+2" );
	$option->add ( "+3", "+3" );
	$option->add ( "+4", "+4" );
	
	$select = new Select ( $id, "", $option->getContent () );
	$select->addClass ( $class );
	$select->addClass ( " negatif " );
	$select->addAtribute("grup",$grup);
	return $select->getHtml ();
}

/**
 * this function for normal selector in Modal Table in Lab
 * contain Normal, +1, +2, +3, +4
 * @param string $id
 * @param string $class
 * @param string $grup
 * @return string of OptionBuilder
 */
function get_normal($id, $class,$grup) {
	$option = new OptionBuilder ();
	$option->add ( "", "", "1" );
	$option->add ( "Normal", "Normal" );
	$option->add ( "+1", "+1" );
	$option->add ( "+2", "+2" );
	$option->add ( "+3", "+3" );
	$option->add ( "+4", "+4" );
	
	$select = new Select ( $id, "", $option->getContent () );
	$select->addClass ( $class );
	$select->addClass ( " normal " );
	$select->addAtribute("grup",$grup);
	return $select->getHtml ();
}

/**
 * this function for normal - diabetes selector in Modal Table in Lab
 * contain Normal, Prediabetes, Diabetes
 * @param string $id
 * @param string $class
 * @param string $grup
 * @return string of OptionBuilder
 */
function get_normal_diabetes($id, $class,$grup) {
	$option = new OptionBuilder ();
	$option->add ( "", "", "1" );
	$option->add ( "Normal: < 6.0 %", "Normal" );
	$option->add ( "Prediabetes: 6.0 - 6.4 %", "Prediabetes: 6.0 - 6.4 %" );
	$option->add ( "Diabetes: > 6.5 %", "Diabetes: > 6.5 %" );
	
	$select = new Select ( $id, "", $option->getContent () );
	$select->addClass ( $class );
	$select->addClass ( " normal " );
	$select->addAtribute("grup",$grup);
	return $select->getHtml ();
}

/**
 * Creating a table print for meda table
 * @param TablePrint $table
 * @param string $id
 * @param string $setup
 * @param string $class
 */
function createLabTable(TablePrint $table, $id, $setup, $class) {
	$nn = $setup ['nn'];
	if ($nn == "textarea") {
		$input = new TextArea ( $id, "", "" );
        $input->addAtribute("grup", $setup ['grup']);
        $table->addColumn ( $setup ['name'], 2, 1,null, null, "entry entry_".$id  );
		$font = "<font id='copy_" . $id . "' class='copy_" . $class . "' ></font>";
		$table->addColumn ( "Catatan : \n" . $input->getHtml () . $font , 2, 1, null, null, "catatan_entry entry_".$id  );
	} else if ($nn == "negatif_positif") {
		$table->addColumn ( $setup ['name'], 2, 1,null, null, "entry entry_".$id  );
		$font = "<font id='copy_" . $slug . $id . "' class='copy_hasil2' ></font>";
		$table->addColumn ( get_negatif_positif_only ( $slug . $id, "asli_hasil2 modal_hasil2_negatif",$setup['grup'] ) . $font, 2, 1 , null, null, "nilai_hasil" );
	}else if ($nn == "negatif") {
		$table->addColumn ( $setup ['name'], 2, 1, null, null, "entry entry_".$id  );
		$font = "<font id='copy_" . $id . "' class='copy_" . $class . "' ></font>";
		$table->addColumn ( get_negatif ( $id, "asli_" . $class . " modal_hasil_negatif" , $setup['grup']) . $font, 2, 1, null, null, "nilai_hasil" );
	}else if ($nn == "negatif_2") {
		$table->addColumn ( $setup ['name'], 2, 1, null, null, "entry entry_".$id  );
		$font = "<font id='copy_" . $id . "' class='copy_" . $class . "' ></font>";
		$table->addColumn ( get_negatif_2 ( $id, "asli_" . $class . " modal_hasil_negatif" , $setup['grup']) . $font, 2, 1, null, null, "nilai_hasil" );
	}else if ($nn == "normal") {
		$table->addColumn ( $setup ['name'], 2, 1, null, null, "entry entry_".$id  );
		$font = "<font id='copy_" . $id . "' class='copy_" . $class . "' ></font>";
		$table->addColumn ( get_normal ( $id, "asli_" . $class . " modal_hasil_normal" , $setup['grup']) . $font, 2, 1, null, null, "nilai_hasil" );
	}else if ($nn == "normal_diabetes") {
		$table->addColumn ( $setup ['name'], 2, 1, null, null, "entry entry_".$id  );
		$font = "<font id='copy_" . $id . "' class='copy_" . $class . "' ></font>";
		$table->addColumn ( get_normal_diabetes ( $id, "asli_" . $class . " modal_hasil_normal_diabetes" , $setup['grup']) . $font, 2, 1, null, null, "nilai_hasil" );
	}else if ($nn == "reaktif_nonreaktif") {
        $table->addColumn ( $setup ['name'], 2, 1, null, null, "entry entry_".$id  );
		$font = "<font id='copy_" . $id . "' class='copy_" . $class . "' ></font>";
		$table->addColumn ( get_reaktif_non_reaktif ( $id, "asli_" . $class . " asli_hasil2 modal_hasil2_reaktif" , $setup['grup']) . $font, 2, 1, null, null, "nilai_hasil" );
    }else {
		$font = "<font id='copy_" . $id . "' class='copy_" . $class . "' ></font>";
		$input = new Text ( $id, "", "" );
		foreach ( $setup as $s_name => $s_value ) {
			$input->addAtribute ( $s_name, $s_value );
		}
		$input->addClass ( $nn );
		$input->addClass ( "asli_" . $class . "" );
		$table->addColumn ( $setup ['name'], 2, 1, null, null, "entry entry_".$id );
		$table->addColumn ( $input->getHtml () . $font, 2, 1, null, null, "nilai_hasil" );
	}

	$table->addColumn ( $setup ["nt"], 4, 1, null, null, "nilai_normal nn_".$id );
	$table->addColumn ( $setup ["metode"], 2, 1, null, null, "metode metode_".$id );
	$table->commit ( "body" );
}

/**
 * creating table in modal Labolatory
 * @param string $hasil1
 * @param string $slug
 * @param string $class
 * @return TablePrint
 */
function createNewTable($hasil1, $slug, $class) {
	$table = new TablePrint ( "pat_hasil", false );
	$table->addTableClass ( $class );
	$table->setMaxWidth ( false );
	$table->addColumn ( "Periksa", 2, 1, null, null, "table_pemeriksaan" );
	$table->addColumn ( "Hasil", 2, 1, null, null, "table_hasil" );
	$table->addColumn ( "Normal", 4, 1, null, null, "table_normal" );
	$table->addColumn ( "Metode", 2, 1, null, null, "table_metode" );
	$table->commit ( "header");
	$title = "";
	foreach ( $hasil1 as $id => $setup ) {
		$grup = $setup ['grup'];
		if ($grup != $title && $grup!="catatan") {
			$table->addColumn ( strtoupper ( "PEMERIKSAAN " . $grup ), 10, 1, "body", null, "t-center" );
			$title = $grup;
		}
		createLabTable ( $table, $slug . $id, $setup, "pat_hasil" );
	}
	return $table;
}

?>