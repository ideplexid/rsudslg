<?php
require_once 'patology/resource/modal_libs.php';
require_once 'patology/resource/PatologyResource.php';

$res = new PatologyResource ();
$hasil2 = $res->getModalHasil2 ();
$slug = "patology_";

$table = new TablePrint ( "hasil2_table", false );
$table->setMaxWidth ( false );
$table->addColumn ( "Periksa", 1, 1 );
$table->addColumn ( "Hasil", 1, 1 );
$table->addColumn ( "Nilai Normal", 1, 1 );
$table->addColumn ( "Metode", 1, 1 );
$table->commit ( "header", "header_hasil_2" );
$title = "";

foreach ( $hasil2 as $id => $setup ) {
	$grup = $setup ['grup'];
	if ($grup != $title) {
		$table->addColumn ( strtoupper ( "PEMERIKSAAN " . $grup ), 4, 1, "body", null, "t-center" );
		$title = $grup;
	}
	
	$nn = $setup ['nn'];
	if ($nn == "negatif") {
		$table->addColumn ( $setup ['name'], 1, 1,null, null, "entry entry_patology_".$id  );
		$font = "<font id='copy_" . $slug . $id . "' class='copy_hasil2' ></font>";
		$table->addColumn ( get_negatif ( $slug . $id, "asli_hasil2 modal_hasil2_negatif",$setup['grup'] ) . $font, 1, 1 , null, null, "nilai_hasil" );
		$table->addColumn ( $setup ["nt"], 1, 1 , null, null, "nilai_normal nn_patology_".$id );
		$table->addColumn ( $setup ["metode"], 1, 1 , null, null, "metode metode_patology_".$id );
		$table->commit ( "body" );
	} else if ($nn == "negatif_positif") {
		$table->addColumn ( $setup ['name'], 1, 1,null, null, "entry entry_patology_".$id  );
		$font = "<font id='copy_" . $slug . $id . "' class='copy_hasil2' ></font>";
		$table->addColumn ( get_negatif_positif_only ( $slug . $id, "asli_hasil2 modal_hasil2_negatif",$setup['grup'] ) . $font, 1, 1 , null, null, "nilai_hasil" );
		$table->addColumn ( $setup ["nt"], 1, 1 , null, null, "nilai_normal nn_patology_".$id );
		$table->addColumn ( $setup ["metode"], 1, 1 , null, null, "metode metode_patology_".$id );
		$table->commit ( "body" );
	} else if ($nn == "normal") {
		$table->addColumn ( $setup ['name'], 1, 1,null, null, "entry entry_patology_".$id  );
		$font = "<font id='copy_" . $slug . $id . "' class='copy_hasil2' ></font>";
		$table->addColumn ( get_normal ( $slug . $id, "asli_hasil2 modal_hasil2_normal",$setup['grup'] ) . $font, 1, 1 , null, null, "nilai_hasil" );
		$table->addColumn ( $setup ["nt"], 1, 1 , null, null, "nilai_normal nn_patology_".$id );
		$table->addColumn ( $setup ["metode"], 1, 1 , null, null, "metode metode_patology_".$id );
		$table->commit ( "body" );
	} else if ($nn == "reaktif_nonreaktif") {
		$table->addColumn ( $setup ['name'], 1, 1,null, null, "entry entry_patology_".$id  );
		$font = "<font id='copy_" . $slug . $id . "' class='copy_hasil2' ></font>";
		$table->addColumn ( get_reaktif_non_reaktif ( $slug . $id, "asli_hasil2 modal_hasil2_reaktif",$setup['grup'] ) . $font, 1, 1 , null, null, "nilai_hasil" );
		$table->addColumn ( $setup ["nt"], 1, 1 , null, null, "nilai_normal nn_patology_".$id );
		$table->addColumn ( $setup ["metode"], 1, 1 , null, null, "metode metode_patology_".$id );
		$table->commit ( "body" );
	} else if ($nn == "less-than") {
		$input = new Text ( $slug . $id, "", "" );
		$input->addClass ( "less-than" );
		$input->addClass ( "asli_hasil2" );
		$input->addAtribute ( "lessthan", $setup ['lessthan'] );
		$input->addAtribute ( "grup", $setup ['grup'] );
		$font = "<font id='copy_" . $slug . $id . "' class='copy_hasil2' ></font>";
		$table->addColumn ( $setup ['name'], 1, 1 , null, null, "entry entry_patology_".$id );
		$table->addColumn ( $input->getHtml () . $font, 1, 1, null, null, "nilai_hasil" );
		$table->addColumn ( $setup ["nt"], 1, 1, null, null, "nilai_normal nn_patology_".$id  );
		$table->addColumn ( $setup ["metode"], 1, 1 , null, null, "metode metode_patology_".$id );		
		$table->commit ( "body" );
	} else if ($nn == "negatif_2") {
		$table->addColumn ( $setup ['name'], 1, 1,null, null, "entry entry_patology_".$id  );
		$font = "<font id='copy_" . $slug . $id . "' class='copy_hasil2' ></font>";
		$table->addColumn ( get_negatif_2 ( $slug . $id, "asli_hasil2 modal_hasil2_negatif",$setup['grup'] ) . $font, 1, 1 , null, null, "nilai_hasil" );
		$table->addColumn ( $setup ["nt"], 1, 1 , null, null, "nilai_normal nn_patology_".$id );
		$table->addColumn ( $setup ["metode"], 1, 1 , null, null, "metode metode_patology_".$id );
		$table->commit ( "body" );
	} else {
		$font = "<font id='copy_" . $id . "' class='copy_hasil2' ></font>";
		$input = new Text ( $slug.$id, "", "" );
		foreach ( $setup as $s_name => $s_value ) {
			$input->addAtribute ( $s_name, $s_value );
		}
		$input->addClass ( $nn );
		$input->addClass ( "asli_hasil2" );
		$table->addColumn ( $setup ['name'], 1, 1, null, null, "entry entry_". $slug.$id );
		$table->addColumn ( $input->getHtml () . $font, 1, 1, null, null, "nilai_hasil" );
		$table->addColumn ( $setup ["nt"], 1, 1, null, null, "nilai_normal nn_". $slug.$id );
		$table->addColumn ( $setup ["metode"], 1, 1 , null, null, "metode metode_patology_".$id );
		$table->commit ( "body" );
	}
}
echo "<div id='hasil_2'>";
echo $table->getHtml ();
echo "</div>";

?>

<style type="text/css">
.modal_hasil2_negatif option[value="Positif"],.modal_hasil2_negatif option[value="Positif 1/20"],.negatif option[value="Positif 1/40"],.negatif option[value="Positif 1/80"],.negatif option[value="Positif 1/160"],.negatif option[value="Positif 1/320"],.negatif option[value="Positif 1/640"]
	{
	color: red;
	font-size: 14px;
}

.modal_hasil2_negatif option[value="Negatif"] {
	color: black;
	font-size: 14px;
}

.modal_hasil2_negatif {
	width: 200px;
}

.modal_hasil2_negatif option[value="Reaktif"] {
	color: red;
	font-size: 14px;
}

.modal_hasil2_negatif option[value="Non Reaktif"] {
	color: black;
	font-size: 14px;
}

tr.header_hasil_2 td {
	font-weight: 800;
	font-size: 20px;
	text-align:center; 
}

#hasil2_table {
	width: 85%;
	padding: 20px;
	margin: auto;
	border:solid 1px #000;
}

#hasil2_table tr {
	height: 43px;
}

.copy_hasil2 {
	display: none;
}

#hasil2_table .t-center {
	background-color: #bbb; 
	font-size:15px; 
	font-weight:800; 
	text-align:center; 
	border:solid 1px #000;
}

@media print {
	.asli_hasil2 {
		display: none !important;
	}
	.copy_hasil2 {
		display: block;
	}
	#patology_afp {
		font-size: 100px;
	}
	#hasil_2_pemeriksaan {
		min-width: 250px;
	}
	#hasil_2_normal {
		min-width: 206px;
	}
	.modal_hasil2_negatif option[value="Positif"],.modal_hasil2_negatif option[value="Positif 1/20"],.negatif option[value="Positif 1/40"],.negatif option[value="Positif 1/80"],.negatif option[value="Positif 1/160"],.negatif option[value="Positif 1/320"],.negatif option[value="Positif 1/640"]
		{
		color: red;
		font-size: 10px;
	}
	.modal_hasil2_negatif option[value="Negatif"] {
		color: black;
		font-size: 10px;
	}
    .modal_hasil2_negatif option[value="Reaktif"] {
		color: red;
		font-size: 10px;
	}
    .modal_hasil2_negatif option[value="Non Reaktif"] {
		color: black;
		font-size: 10px;
	}
	.t-center {
		-webkit-print-color-adjust: exact;
		text-align: center;
		border: 1px solid black;
		background-color: #bbb !important;
	}
	.kategori {
		-webkit-print-color-adjust: exact;
		padding: 3px 0px;
		border: 1px solid black;
		background-color: #ccc !important;
		font-weight: bold;
	}
	table#hasil2_table tr>td:first-child {
		padding-left: 10px;
		border-left: solid 1px #000;
		border-top: solid 1px #000;
		border-bottom: solid 1px #000;
	}
	table#hasil2_table tr>td:nth-child(2) {
		border-top: solid 1px #000;
		border-bottom: solid 1px #000;
	}
	table#hasil2_table tr>td:nth-child(3) {
		padding-left: 10px;
		border: solid 1px #000;
	}
	.header_hasil_2 td {
		border: solid 1px #000;
		text-align: center;
	}
}
</style>

<script type="text/javascript">
   function pat_print_hasil2(){

	   var pemeriksa=$("#"+PAT_PREFIX+"_nama_petugas").val();
		$("#pat_nama_pemeriksa").html(pemeriksa);
	
	   
	   for (var i = 0; i < pat_list_hasil.length; i++) {
			var name_list="patology_"+pat_list_hasil[i];
			var val_list=$("#"+name_list).val();
			$("#copy_"+name_list).html(": "+val_list);
			
			if($("#"+name_list).hasClass("red_text")){
				$("#copy_"+name_list).addClass("red_text").removeClass("black_text");
			}else{
				$("#copy_"+name_list).addClass("black_text").removeClass("red_text");
			}
		}


	   var header="";
	   var content=$("#hasil_2").html();
	   var footer=$("#print_footer").html();
	   var cara=$("#"+pat_the_action+"_carabayar").val();
	   var p_content="";
		
	   if(cara=="Asuransi"){
		    $("#utk_rm").hide();
			$("#utk_dokter").hide();
			$("#utk_asuransi").show();
			header=$("#print_header").html();
			p_content=header+content+footer;
			smis_print(p_content);
			
		}
	    $("#utk_rm").show();
		$("#utk_dokter").hide();
		$("#utk_asuransi").hide();
	    header=$("#print_header").html();
		p_content=header+content+footer;
		smis_print(p_content);
		
		$("#utk_rm").hide();
		$("#utk_dokter").show();
		$("#utk_asuransi").hide();
	    header=$("#print_header").html();
		p_content=header+content+footer;
		smis_print(p_content);

		$("#utk_rm").show();
		$("#utk_dokter").show();
		$("#utk_asuransi").show();

		var id=$("#"+pat_the_action+"_id").val();
		var currentHtml = $('<div>').append(content);
		currentHtml.find("input, select, textarea").remove();
		currentHtml.find(".copy_hasil2").removeClass("copy_hasil1");
		var t=currentHtml.html();
		
		var id=$("#"+pat_the_action+"_id").val();
		var data={
				page:pat_the_page,
				action:pat_the_action,
				command:"save",
				cetak_2:t,
				prototype_name:pat_the_protoname,
				prototype_slug:pat_the_protoslug,
				prototype_implement:pat_the_protoimplement,
				id:id
			};
		
		$.post("",data,function(res){});		
	   	
	}

 	
   </script>