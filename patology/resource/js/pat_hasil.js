
function dl_diffcount() {
	var dif=$("#patology_dl_diff_count").val();
	return diffcount(dif);
}
function h_diffcount() {
	var dif=$("#patology_h_diff_count").val();
	return diffcount(dif);
}

function diffcount(dif){
	var sp=dif.split("/");
	var totalvar=dif.length;
	var hasil="";
	for(var i=0;i<sp.length;i++){
		var vl=sp[i];
		if(vl=="") vl="-";
		if(i==0) {
			if(vl=="-" || (Number(vl)>=2 && Number(vl)<=4) ) hasil+=vl+"/";
			else  hasil+="<font class='red_text'>"+vl+"</font>/";
		}else if(i==1){
			if(vl=="-" || (Number(vl)>=0 && Number(vl)<=1) ) hasil+=vl+"/";
			else  hasil+="<font class='red_text'>"+vl+"</font>/";
		}else if(i==2) {
			if(vl=="-" || (Number(vl)>=3 && Number(vl)<=5) ) hasil+=vl+"/";
			else  hasil+="<font class='red_text'>"+vl+"</font>/";
		}else if(i==3){
			if(vl=="-" || (Number(vl)>=54 && Number(vl)<=62) ) hasil+=vl+"/";
			else  hasil+="<font class='red_text'>"+vl+"</font>/";
		}else if(i==4){
			if(vl=="-" || (Number(vl)>=25 && Number(vl)<=33) ) hasil+=vl+"/";
			else  hasil+="<font class='red_text'>"+vl+"</font>/";
		}else if(i==5){
			if(vl=="-" || (Number(vl)>=3 && Number(vl)<=7) ) hasil+=vl+"";
			else  hasil+="<font class='red_text'>"+vl+"</font>";
		}
	}
	if(totalvar<=12) return "<font id='df_count11'>"+hasil+"</font>";
	else return "<font id='df_count'>"+hasil+"</font>";
}

function maxlength(lt,name_list){
	var rt=$("#"+name_list).hasClass("red_text")?"red_text":"";
	if(lt=="Positif 1/20" || lt=="Positif 1/40" || lt=="Positif 1/80" || lt=="Positif 1/160" || lt=="Positif 1/320" || lt=="Positif 1/640") return lt;
	if(lt.length>12){
		return "<font id='df_count' class='"+rt+"'>"+lt+"</font>"
	}
	else return lt;
}

/*
function pat_print_hasil1(){
		var pemeriksa=$("#"+PAT_PREFIX+"_nama_petugas").val();
		$("#pat_nama_pemeriksa").html(pemeriksa);
	
	   for (var i = 0; i < pat_list_hasil.length; i++) {
			var name_list="patology_"+pat_list_hasil[i];
			var val_list=$("#"+name_list).val();

			if(name_list=="patology_Diff_Count") $("#copy_"+name_list).html(": "+diffcount());
			else if(name_list!="patology_catatan")	$("#copy_"+name_list).html(": "+maxlength(val_list,name_list));
			else $("#copy_"+name_list).html(val_list);
			
			if($("#"+name_list).hasClass("red_text")){
				$("#copy_"+name_list).addClass("red_text").removeClass("black_text");
			}else{
				$("#copy_"+name_list).addClass("black_text").removeClass("red_text");
			}
		}

	   var header="";
	   var content=$("#hasil_1_wrap").html();
	   var footer=$("#print_footer").html();
	   var cara=$("#"+pat_the_action+"_carabayar").val();
	   var p_content="";
		
	   if(cara=="Asuransi"){
		    $("#utk_rm").hide();
			$("#utk_dokter").hide();
			$("#utk_asuransi").show();
			header=$("#print_header").html();
			p_content=header+content+footer;
			smis_print(p_content);			
		}
	    $("#utk_rm").show();
		$("#utk_dokter").hide();
		$("#utk_asuransi").hide();
	    header=$("#print_header").html();
		p_content=header+content+footer;
		smis_print(p_content);
		
		$("#utk_rm").hide();
		$("#utk_dokter").show();
		$("#utk_asuransi").hide();
	    header=$("#print_header").html();
		p_content=header+content+footer;
		smis_print(p_content);

		$("#utk_rm").show();
		$("#utk_dokter").show();
		$("#utk_asuransi").show();
	
		var id=$("#"+pat_the_action+"_id").val();
		var currentHtml = $('<div>').append(content);
		currentHtml.find("input, select, textarea").remove();
		currentHtml.find(".copy_hasil1").removeClass("copy_hasil1");
		var t=currentHtml.html();
		var data={
				page:pat_the_page,
				action:pat_the_action,
				command:"save",
				cetak_1:t,
				prototype_name:pat_the_protoname,
				prototype_slug:pat_the_protoslug,
				prototype_implement:pat_the_protoimplement,
				id:id
			};
		$.post("",data,function(res){});		
}*/

var states = ['Negatif', 'Positif', 'Positif 1/20', 'Positif 1/40', 'Positif 1/80', 'Positif 1/160', 'Positif 1/320', 'Positif 1/640'];
$(document).ready(function(){	
	
	$(".negatif").on("change",function(){
		if(this.value=="Negatif")  $(this).addClass("black_text").removeClass("red_text");
		else  $(this).addClass("red_text").removeClass("black_text");
		var this_value=this.value;
		var child=$(this).children();
		$(child).each(function(e,val){
			if(this_value==$(val).val()) $(val).attr("selected","selected");
			else $(val).removeAttr("selected");
		});
	});
    
    $(".reaktif").on("change",function(){
		if(this.value=="Non Reaktif")  $(this).addClass("black_text").removeClass("red_text");
		else  $(this).addClass("red_text").removeClass("black_text");
		var this_value=this.value;
		var child=$(this).children();
		$(child).each(function(e,val){
			if(this_value==$(val).val()) $(val).attr("selected","selected");
			else $(val).removeAttr("selected");
		});
	});
    
    $(".normal").on("change",function(){
		if(this.value=="Normal")  $(this).addClass("black_text").removeClass("red_text");
		else  $(this).addClass("red_text").removeClass("black_text");
		var this_value=this.value;
		var child=$(this).children();
		$(child).each(function(e,val){
			if(this_value==$(val).val()) $(val).attr("selected","selected");
			else $(val).removeAttr("selected");
		});
	});
    
    $(".normal_diabetes").on("change",function(){
		if(this.value=="Normal")  $(this).addClass("black_text").removeClass("red_text");
		else  $(this).addClass("red_text").removeClass("black_text");
		var this_value=this.value;
		var child=$(this).children();
		$(child).each(function(e,val){
			if(this_value==$(val).val()) $(val).attr("selected","selected");
			else $(val).removeAttr("selected");
		});
	});
	
	$(".laper").on("change",function(){
		var jk = $("#pemeriksaan_jk").val();
		var nilai = parseFloat($(this).val());
		var nilaimin = 0;
		var nilaimax = 0;
		if(jk=="0")
		{
			nilaimin = parseFloat($(this).attr("lmin"));
			nilaimax = parseFloat($(this).attr("lmax"));
		}
		else
		{
			nilaimin = parseFloat($(this).attr("pmin"));
			nilaimax = parseFloat($(this).attr("pmax"));
		}
		if(nilai>=nilaimin && nilai<=nilaimax)
		$(this).addClass("black_text").removeClass("red_text");
		else $(this).addClass("red_text").removeClass("black_text");
		
	});
	
	$(".less-than").on("change",function(){
		var nilaimin = parseFloat($(this).attr("lessthan"));
		var nilai = parseFloat($(this).val());
		if(nilai>=nilaimin)	$(this).addClass("red_text").removeClass("black_text");
		else  $(this).addClass("black_text").removeClass("red_text");
	});
	
	$(".more-than").on("change",function(){
		var nilaimax = parseFloat($(this).attr("morethan"));
		var nilai = parseFloat($(this).val());
		if(nilai<=nilaimax) $(this).addClass("red_text").removeClass("black_text");
		else $(this).addClass("black_text").removeClass("red_text");
	});
	
	$(".between").on("change",function(){
		var nilaimin = parseFloat($(this).attr("valmin"));
		var nilaimax = parseFloat($(this).attr("valmax"));
		var nilai = parseFloat($(this).val());
		if(nilai>=nilaimin && nilai<=nilaimax)	$(this).addClass("black_text").removeClass("red_text");
		else $(this).addClass("red_text").removeClass("black_text");
	});

	$(".same").on("change",function(){
		var nilai = $(this).attr("sameval")+"";
		var cnilai = $(this).val()+"";
		if(nilai.toUpperCase() ==cnilai.toUpperCase() )	$(this).addClass("black_text").removeClass("red_text");
		else $(this).addClass("red_text").removeClass("black_text");
	});
});