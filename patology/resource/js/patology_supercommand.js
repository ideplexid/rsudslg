var pat_dokter;
var pat_konsultan;
var pat_petugas;
var pat_pasien;
var pat_marketing;
var pat_pengirim;
				
pat_pasien=new TableAction("pat_pasien",PAT_PAGE,PAT_PREFIX,new Array());
pat_pasien.setSuperCommand("pat_pasien");
pat_pasien.setPrototipe(pat_the_protoname,pat_the_protoslug,pat_the_protoimplement);
pat_pasien.selected=function(json){
	$("#"+PAT_PREFIX+"_nama_pasien").val(json.nama_pasien);
	$("#"+PAT_PREFIX+"_nrm_pasien").val(json.nrm);
	$("#"+PAT_PREFIX+"_noreg_pasien").val(json.id);
	$("#"+PAT_PREFIX+"_jk").val(json.kelamin);
	$("#"+PAT_PREFIX+"_umur").val(json.umur);
	$("#"+PAT_PREFIX+"_ibu").val(json.ibu);
	$("#"+PAT_PREFIX+"_alamat").val(json.alamat_pasien);
    $("#"+PAT_PREFIX+"_kelurahan").val(json.nama_kelurahan);
	$("#"+PAT_PREFIX+"_kecamatan").val(json.nama_kecamatan);
	$("#"+PAT_PREFIX+"_kabupaten").val(json.nama_kabupaten);
	$("#"+PAT_PREFIX+"_provinsi").val(json.nama_provinsi);
	$("#"+PAT_PREFIX+"_carabayar").val(json.carabayar);
    pat_pasien.get_last_ruangan(json.id,json.jenislayanan,json.kamar_inap);
};

pat_pasien.get_last_ruangan = function(noreg,rajal,ranap){
    var cur_ruang = (ranap==null || ranap=="") ? rajal:ranap;
    var data = pat_pasien.getRegulerData();
    data['super_command'] = "";
    data['command'] = "last_position";
    data['noreg_pasien'] = noreg;
    showLoading();
    $.post("",data,function(res){
        var json = getContent(res);
        if(json!=""){
            cur_ruang = json;
        }
        $("#"+PAT_PREFIX+"_ruangan").val(cur_ruang);
        if($("#"+PAT_PREFIX+"_setting_kelas").val() == "Sesuai Kelas Pasien") {
            var data=pat_pasien.getRegulerData();
            data['action']="get_kelas_ruangan";
            data['polislug']=cur_ruang;
            $.post("",data,function(res){
                var hasil=getContent(res);
                console.log(hasil.slug_kelas+" "+cur_ruang);
                $("#"+PAT_PREFIX+"_kelas").val(hasil.slug_kelas);
            });
        }
        dismissLoading();
    });
};

pat_dokter=new TableAction("pat_dokter",PAT_PAGE,PAT_PREFIX,new Array());
pat_dokter.setSuperCommand("pat_dokter");
pat_dokter.setPrototipe(pat_the_protoname,pat_the_protoslug,pat_the_protoimplement);
pat_dokter.selected=function(json){
	$("#"+PAT_PREFIX+"_nama_dokter").val(json.nama);
	$("#"+PAT_PREFIX+"_id_dokter").val(json.id);
};

pat_petugas=new TableAction("pat_petugas",PAT_PAGE,PAT_PREFIX,new Array());
pat_petugas.setSuperCommand("pat_petugas");
pat_petugas.setPrototipe(pat_the_protoname,pat_the_protoslug,pat_the_protoimplement);
pat_petugas.selected=function(json){
	$("#"+PAT_PREFIX+"_nama_petugas").val(json.nama);
	$("#"+PAT_PREFIX+"_id_petugas").val(json.id);
};

pat_konsultan=new TableAction("pat_konsultan",PAT_PAGE,PAT_PREFIX,new Array());
pat_konsultan.setSuperCommand("pat_konsultan");
pat_konsultan.setPrototipe(pat_the_protoname,pat_the_protoslug,pat_the_protoimplement);
pat_konsultan.selected=function(json){
	$("#"+PAT_PREFIX+"_nama_konsultan").val(json.nama);
	$("#"+PAT_PREFIX+"_id_konsultan").val(json.id);
};

pat_marketing=new TableAction("pat_marketing",PAT_PAGE,PAT_PREFIX,new Array());
pat_marketing.setSuperCommand("pat_marketing");
pat_marketing.setPrototipe(pat_the_protoname,pat_the_protoslug,pat_the_protoimplement);
pat_marketing.selected=function(json){
	$("#"+PAT_PREFIX+"_marketing").val(json.nama);
	$("#"+PAT_PREFIX+"_id_marketing").val(json.id);
};

pat_pengirim=new TableAction("pat_pengirim",PAT_PAGE,PAT_PREFIX,new Array());
pat_pengirim.setSuperCommand("pat_pengirim");
pat_pengirim.setPrototipe(pat_the_protoname,pat_the_protoslug,pat_the_protoimplement);
pat_pengirim.selected=function(json){
	$("#"+PAT_PREFIX+"_pengirim").val(json.nama);
	$("#"+PAT_PREFIX+"_id_pengirim").val(json.id);
};