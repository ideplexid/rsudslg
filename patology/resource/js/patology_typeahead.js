$('#'+PAT_PREFIX+'_nama_dokter').typeahead({
	minLength:3,
	source: function (query, process) {
	 var data_dokter=pat_dokter.getViewData();
	 data_dokter["kriteria"]=$('#'+PAT_PREFIX+'_nama_dokter').val();
	 var $items = new Array;
	   $items = [""];				                
	  $.ajax({
		url: '',
		type: 'POST',
		data: data_dokter,
		success: function(res) {
		  var json=getContent(res);
		  var the_data_proses=json.d.data;
		   $items = [""];	
		  $.map(the_data_proses, function(data){
			  var group;
			  group = {
				  id: data.id,
				  name: data.nama,                            
				  toString: function () {
					  return JSON.stringify(this);
				  },
				  toLowerCase: function () {
					  return this.name.toLowerCase();
				  },
				  indexOf: function (string) {
					  return String.prototype.indexOf.apply(this.name, arguments);
				  },
				  replace: function (string) {
					  var value = '';
					  value +=  this.name;
					  if(typeof(this.level) != 'undefined') {
						  value += ' <span class="pull-right muted">';
						  value += this.level;
						  value += '</span>';
					  }
					  return String.prototype.replace.apply('<div class="typeaheadiv">' + value + '</div>', arguments);
				  }
			  };
			  $items.push(group);
		  });
		  
		  process($items);
		}
	  });
	},
	updater: function (item) {
		var item = JSON.parse(item);
		$("#"+PAT_PREFIX+"_id_dokter").val(item.id);
		$("#"+PAT_PREFIX+"_ruangan").focus();
		return item.name;
	}
  });

$('#'+PAT_PREFIX+'_nama_petugas').typeahead({
	minLength:3,
	source: function (query, process) {
	 var data_dokter=pat_dokter.getViewData();
	 data_dokter["kriteria"]=$('#'+PAT_PREFIX+'_nama_petugas').val();
	 var $items = new Array;
	   $items = [""];				                
	  $.ajax({
		url: '',
		type: 'POST',
		data: data_dokter,
		success: function(res) {
		  var json=getContent(res);
		  var the_data_proses=json.d.data;
		   $items = [""];	
		  $.map(the_data_proses, function(data){
			  var group;
			  group = {
				  id: data.id,
				  name: data.nama,                            
				  toString: function () {
					  return JSON.stringify(this);
				  },
				  toLowerCase: function () {
					  return this.name.toLowerCase();
				  },
				  indexOf: function (string) {
					  return String.prototype.indexOf.apply(this.name, arguments);
				  },
				  replace: function (string) {
					  var value = '';
					  value +=  this.name;
					  if(typeof(this.level) != 'undefined') {
						  value += ' <span class="pull-right muted">';
						  value += this.level;
						  value += '</span>';
					  }
					  return String.prototype.replace.apply('<div class="typeaheadiv">' + value + '</div>', arguments);
				  }
			  };
			  $items.push(group);
		  });
		  
		  process($items);
		}
	  });
	},
	updater: function (item) {
		var item = JSON.parse(item);
		$("#"+PAT_PREFIX+"_id_petugas").val(item.id);
		$("#"+PAT_PREFIX+"_umur").focus();
		return item.name;
	}
  });



$("#patology_pdb").on("change",function(){
	var ck=$("#patology_pdb").is(":checked");
	$("#patology_pdb_dengueblot").prop("checked", ck);
	$("#patology_pdb_pcv").prop("checked", ck);
	$("#patology_pdb_pcv").prop("checked", ck);
	$("#patology_pdb_hemoglobin").prop("checked", ck);
	$("#patology_pdb_trombosit").prop("checked", ck);						
});