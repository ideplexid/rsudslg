var pat_list_hasil          = $.parseJSON($("#PAT_LIST_HASIL").val());
var pat_list_pesan          = $.parseJSON($("#PAT_LIST_PESAN").val());
var PAT_MODE                = $("#PAT_MODE").val();
var pat_noreg               = $("#PAT_NOREG").val();
var pat_nama_pasien         = $("#PAT_NAMA").val();
var pat_nrm_pasien          = $("#PAT_NRM").val();
var pat_polislug            = $("#PAT_POLISLUG").val();
var PAT_PAGE                = $("#PAT_PAGE").val();
var PAT_PREFIX              = $("#PAT_PREFIX").val();
var pat_the_protoslug       = $("#PAT_PROTOSLUG").val();
var pat_the_protoname       = $("#PAT_PROTONAME").val();
var pat_the_protoimplement  = $("#PAT_PROTOIMPLEMENT").val();			
var layanan_lain;
var layanan_lain_num;
var PAT_JK                  = $("#PAT_JK").val();	
var PAT_EDIT_HASIL          = $("#PAT_EDIT_HASIL").val();	
var PAT_EDIT_LAYANAN        = $("#PAT_EDIT_LAYANAN").val();	
var pat_the_action          = PAT_PREFIX;
var pat_the_page            = PAT_PAGE;
var PAT_ACTION;

$(document).ready(function() {
	$('.modal').on('shown.bs.modal', function() {
		$(this).find('[autofocus]').focus();
	});
	
	/*melakukan disabled pada pelayanan , tidak bisa di edit oleh petugas lab, degan syarat settingan di aktifkan*/
	$("div#pesan_header div.div-check input[type='checkbox']").prop("disabled",(PAT_EDIT_HASIL==0));
	layanan_lain = new LayananLainAction("layanan_lain",PAT_PAGE,PAT_PREFIX,new Array("harga", "subtotal"));
	
	$("#layanan_lain_harga, #layanan_lain_jumlah").on("keyup", function(e) {
		if (e.which == 13)
			return;
		$(".btn").removeAttr("disabled");
		$(".btn").attr("disabled", "disabled");
		var harga = $("#layanan_lain_harga").val().replace(/[^0-9-,]/g, '').replace(",", ".");
		var jumlah = $("#layanan_lain_jumlah").val();
		if (!is_numeric(jumlah))
			jumlah = 0;
		var subtotal = parseFloat(jumlah) * parseFloat(harga);
		subtotal = "Rp. " + (parseFloat(subtotal)).formatMoney("2", ".", ",");
		$("#layanan_lain_subtotal").val(subtotal);
		$(".btn").removeAttr("disabled");
	});
	
	$("#layanan_lain_nama").on("keyup", function(e) {
		if (e.which == 13) {
			$("#layanan_lain_harga").focus();
		}
	});
	
	$("#layanan_lain_harga").on("keyup", function(e) {
		if (e.which == 13) {
			$("#layanan_lain_jumlah").focus();
		}
	});
	
	$("#layanan_lain_jumlah").on("keyup", function(e) {
		if (e.which == 13) {
			$('#layanan_lain_save_btn').trigger('click');
		}
	});
	$(".mydate").datepicker();
	$('.mydatetime').datetimepicker({ minuteStep: 1});
	
	
	var column=new Array(
			"id","tanggal","ruangan","kelas",
			"nama_pasien","noreg_pasien","nrm_pasien",
            "id_marketing", "marketing", "id_pengirim", "pengirim",
			"no_pat","id_dokter","nama_dokter",
			"id_konsultan","nama_konsultan",
			"id_petugas","nama_petugas",
			"jk","hapusan_darah","umur",
			"ibu","limapuluh","alamat", "kelurahan", "kecamatan", "kabupaten", "provinsi","carabayar",
			"waktu_daftar","waktu_selesai",
			"waktu_ditangani_hapusan",
			"waktu_ditangani","uri","diagnosa", "file", "status",
			"kesimpulan"
		);
	PAT_ACTION=new PatologyAction(PAT_PREFIX,PAT_PAGE,PAT_PREFIX,column);
	PAT_ACTION.setPrototipe(pat_the_protoname,pat_the_protoslug,pat_the_protoimplement);
	PAT_ACTION.view();
    
    for(var i = 0; i < pat_list_hasil.length; i++) {
        var list_hasil = "#patology_"+pat_list_hasil[i];
        $(list_hasil).closest("tr").css({"display": "none"});
        $(list_hasil).val("");
    }
    
});