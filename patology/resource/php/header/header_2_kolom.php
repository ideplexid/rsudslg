<?php
$nama_konsultan = getSettings ( $db, "patology-konsultan-nama", "" );
$id_konsultan   = getSettings ( $db, "patology-konsultan-id", "" );
$title          = getSettings ( $db, "smis_autonomous_title", "" );
$logo           = getLogo ();
$adress         = getSettings ( $db, "smis_autonomous_address", "" );
$contact        = getSettings ( $db, "smis_autonomous_contact", "" );

/*
$utk  = ": <t id='untuk' class='untuk'><font  class='label label-important utk_rm' id='utk_rm'>Rekam Medis</font>";
$utk .= " <font class='label label-success utk_dokter' id='utk_dokter'>Pasien</font>";
$utk .= " <font class='label  utk_asuransi' id='utk_asuransi'>IKS</font><t>";
$utk .= " <font class='label label-important utk_pat' id='utk_pat'>Patology</font><t>";

$tp = new TablePrint ( "pheader" );
$tp	->setTableClass ( "pheader" )
    ->setMaxWidth ( false )
    ->addColumn ( "<img src='" . $logo . "' />", 2, 3, null, "header_logo" )
    ->addColumn ( $title, 8, 1, null, "autonomous_title" )
    ->commit ( "title" );
$tp	->addColumn ( $adress, 10, 1, null, "autonomous_address" )
    ->commit ( "title" );
$tp	->addColumn ( $contact, 10, 1, null, "autonomous_contact" )
    ->commit ( "title" );

$tp	->addColumn ( "Nama", 1, 1, null, null, "phttl" )
    ->addColumn ( "", 3, 1, null, "ph_nama" )
    ->addColumn ( "Ibu Kandung", 1, 1, null, null, "phttl" )
    ->addColumn ( "", 2, 1, null, "ph_ibu" )
    ->addColumn ( "NRM", 1, 1, null, null, "phttl" )
    ->addColumn ( "", 2, 1, null, "ph_nrm" )
    ->commit ( "header" );

$tp	->addColumn ( "Alamat", 1, 1, null, null, "phttl" )
    ->addColumn ( "", 3, 1, null, "ph_alamat" )
    ->addColumn ( "Umur", 1, 1, null, null, "phttl" )
    ->addColumn ( "", 2, 1, null, "ph_umur" )
    ->addColumn ( "Konsultan", 1, 1, null, null, "phttl" )
    ->addColumn ( "", 2, 1, null, "ph_konsultan" )
    ->commit ( "header" );

$tp	->addColumn ( "Pengirim", 1, 1, null, null, "phttl" )
    ->addColumn ( "", 3, 1, null, "ph_pengirim" )
    ->addColumn ( "L/P", 1, 1, null, null, "phttl" )
    ->addColumn ( "", 2, 1, null, "ph_jk" )
    ->addColumn ( "Ruang-Kelas", 1, 1, null, null, "phttl" )
    ->addColumn ( "", 2, 1, null, "ph_rkls" )
    ->commit ( "header" );

$tp	->addColumn ( "No Pat", 1, 1, null, null, "phttl" )
    ->addColumn ( "", 3, 1, null, "ph_nolab" )
    ->addColumn ( "Noreg", 1, 1, null, null, "phttl" )
    ->addColumn ( "", 2, 1, null, "ph_noreg" )
    ->addColumn ( "Cetakan Untuk", 1, 1, null, null, "phttl" )
    ->addColumn ( $utk, 2, 1, null, "ph_untuk" )
    ->commit ( "header" );

$tp	->addColumn ( "Diagnosa / Ket. Klinis", 1, 1, null, null, "phttl" )
    ->addColumn ( "", 10, 1, null, "ph_diagnosa" )
    ->commit ( "header" );*/

$utk = ": <t id='untuk' class='untuk'><font  class='label label-important utk_rm' id='g_utk_rm'>Rekam Medis</font>";
$utk .= " <font class='label label-success utk_dokter' id='g_utk_dokter'>Pasien</font>";
$utk .= " <font class='label  utk_asuransi' id='g_utk_asuransi'>IKS</font><t>";
$utk .= " <font class='label label-important utk_pat' id='g_utk_pat'>Patology</font><t>";

$tp = new TablePrint ( "pheader" );
$tp	->setTableClass ( "pheader" )
    ->setMaxWidth ( false )
    ->addColumn ( "<img src='" . $logo . "' />", 2, 3, null, "header_logo" )
    ->addColumn ( $title, 8, 1, null, "autonomous_title" )
    ->commit ( "title" );
$tp	->addColumn ( $adress, 10, 1, null, "autonomous_address" )
    ->commit ( "title" );
$tp	->addColumn ( $contact, 10, 1, null, "autonomous_contact" )
    ->commit ( "title" );

$tp	->addColumn ( "Nama", 1, 1, null, null, "phttl pbig" )
    ->addColumn ( "", 4, 1, null, "phg_nama","pbig" )
    ->addColumn ( "Ibu Kandung", 1, 1, null, null, "phttl pbig" )
    ->addColumn ( "", 4, 1, null, "phg_ibu","pbig" )
    ->commit("header");

$tp	->addColumn ( "NRM", 1, 1, null, null, "phttl pbig" )
    ->addColumn ( "", 4, 1, null, "phg_nrm","pbig" )
    ->addColumn ( "Noreg", 1, 1, null, null, "phttl pbig" )
    ->addColumn ( "", 4, 1, null, "phg_noreg","pbig" )
    ->commit ( "header" );

$tp	->addColumn ( "Alamat", 1, 1, null, null, "phttl pbig" )
    ->addColumn ( "", 4, 1, null, "phg_alamat","pbig" )
    ->addColumn ( "Umur", 1, 1, null, null, "phttl pbig" )
    ->addColumn ( "", 4, 1, null, "phg_umur","pbig" )
    ->commit("header");
    
$tp	->addColumn ( "Pengirim", 1, 1, null, null, "phttl pbig" )
    ->addColumn ( "", 4, 1, null, "phg_pengirim","pbig" )
    ->addColumn ( "Konsultan", 1, 1, null, null, "phttl pbig" )
    ->addColumn ( "", 4, 1, null, "phg_konsultan","pbig" )
    ->commit ( "header" );

$tp	->addColumn ( "L/P", 1, 1, null, null, "phttl pbig" )
    ->addColumn ( "", 4, 1, null, "phg_jk","pbig" )
    ->addColumn ( "Ruang-Kelas", 1, 1, null, null, "phttl pbig" )
    ->addColumn ( "", 4, 1, null, "phg_rkls","pbig" )
    ->commit ( "header" );

$tp	->addColumn ( "No Pat", 1, 1, null, null, "phttl pbig" )
    ->addColumn ( "", 4, 1, null, "phg_nolab","pbig" )
    ->addColumn ( "Cetakan Untuk", 1, 1, null, null, "phttl pbig" )
    ->addColumn ( $utk, 4, 1, null, "phg_untuk","pbig" )
    ->commit ( "header" );

$tp	->addColumn ( "Diagnosa / Ket. Klinis", 1, 1, null, null, "phttl" )
    ->addColumn ( "", 10, 1, null, "phg_diagnosa" )
    ->commit ( "header" );
echo "<div id='print_header_gabungan' class='hide'>" . $tp->getHtml () . "</div>";
?>