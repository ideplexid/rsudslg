<?php
setChangeCookie ( false );
$header = array ();
$header [] = "Grup";
$header [] = "Nama";
$header [] = "Pria";
$header [] = "Wanita";
$header [] = "Total";
$header [] = "Max Pria";
$header [] = "NMax Pria";
$header [] = "Max Wanita";
$header [] = "NMax Wanita";
$header [] = "Min Pria";
$header [] = "NMin Pria";
$header [] = "Min Wanita";
$header [] = "NMin Wanita";

$uitable = new Table ( $header, "", NULL, false );
$uitable->setName ( "lap_resume_hasil" );
$top = "<tr>
			<th rowspan='3'>Grup</th>
			<th rowspan='3'>Periksa</th>
			<th colspan='3'>Jumlah</th>
			<th colspan='4'>Maximum</th>
			<th colspan='4'>Minimum</th>
		</tr>
		";
$top2 = "<tr>
			<th rowspan='2'>L</th>
			<th rowspan='2'>P</th>
			<th rowspan='2'>Total</th>
			<th colspan='2'>L</th>
			<th colspan='2'>P</th>
			<th colspan='2'>L</th>
			<th colspan='2'>P</th>
		</tr>
		";

$top3 = "<tr>
			<th>Nilai</th>
			<th>Nama</th>
			<th>Nilai</th>
			<th>Nama</th>
			<th>Nilai</th>
			<th>Nama</th>
			<th>Nilai</th>
			<th>Nama</th>
		</tr>";

$uitable->addHeader ( "before", $top );
$uitable->addHeader ( "before", $top2 );
$uitable->addHeader ( "before", $top3 );
$uitable->setHeaderVisible ( false );
$uitable->setFooterVisible ( false );
class LapRangkumanPatology extends ArrayAdapter {
	private $jumlah_detail;
	public function __construct($resource) {
		$this->jumlah_detail = array ();
		$this->initDetail ( $resource );
	}
	public function initDetail($resource) {
		foreach ( $resource as $name => $grup ) {
			$nname = ArrayAdapter::format ( "unslug", $name );
			$this->jumlah_detail [$name] = array (
					"Nama" => $nname,
					"Grup" => strtoupper ( $grup ),
					"Pria" => 0,
					"Wanita" => 0,
					"Total" => 0,
					"Max Pria" => 0,
					"NMax Pria" => 0,
					"Max Wanita" => 0,
					"NMax Wanita" => 0,
					"Min Pria" => 0,
					"NMin Pria" => 0,
					"Min Wanita" => 0,
					"NMin Wanita" => 0 
			);
		}
	}
	public function adapt($d) {
		$jk = $d->jk == "0" ? "Pria" : "Wanita";
		$hasil = json_decode ( $d->hasil, true );
		if ($hasil == null)
			return;
			// var_dump($hasil);
		foreach ( $this->jumlah_detail as $name => $isi ) {
			$value = $hasil [$name];
			if ($value == "" || $value == 0 || $value == "0")
				continue;
			$curdet = &$this->jumlah_detail [$name];
			// check biasa
			$curdet [$jk] ++;
			$curdet ["Total"] ++;
			// check maximum
			if ($curdet ["Max " . $jk] == 0 || $curdet ["Max " . $jk] < ($value) * 1) {
				$curdet ["Max " . $jk] = ($value) * 1;
				$curdet ["NMax " . $jk] = $d->nama_pasien." ( ".$d->nrm_pasien." )";
			}
			// check minimum
			if ($curdet ["Min " . $jk] == 0 || $curdet ["Min " . $jk] > ($value) * 1) {
				$curdet ["Min " . $jk] = ($value) * 1;
				$curdet ["NMin " . $jk] = $d->nama_pasien." ( ".$d->nrm_pasien." )";
			}
		}
	}
	public function getContent($data) {
		parent::getContent ( $data );
		$result = array ();
		foreach ( $this->jumlah_detail as $name => $content ) {
			$result [] = $content;
		}
		$final = $this->removeZero ( $result );
		return $final;
	}
}

if (isset ( $_POST ['command'] )) {
	require_once 'patology/resource/PatologyResource.php';
	$resource = new PatologyResource ();
	$adapter = new LapRangkumanPatology ( $resource->list_table );
	$dbtable = new DBTable ( $db, "smis_pat_pesanan" );
	if (isset ( $_POST ['dari'] ) && isset ( $_POST ['sampai'] ) && $_POST ['dari'] != "" && $_POST ['sampai'] != "") {
		$dari = $_POST ['dari'];
		$sampai = $_POST ['sampai'];
		$dbtable->addCustomKriteria ( "'" . $dari . "'", "<=tanggal" );
		$dbtable->addCustomKriteria ( "'" . $sampai . "'", ">=tanggal" );
		$dbtable->setShowAll ( true );
	}
	$dbtable->setOrder ( " tanggal ASC " );
	$dbres = new DBResponder ( $dbtable, $uitable, $adapter );
	$data = $dbres->command ( $_POST ['command'] );
	echo json_encode ( $data );
	return;
}

$uitable->addModal ( "dari", "date", "Dari", "" );
$uitable->addModal ( "sampai", "date", "Sampai", "" );
$uitable->addModal ( "ruangan", "hidden", "", "igd" );
$modal = $uitable->getModal ();

$form = $modal->getForm ();
$button = new Button ( "", "", "" );
$button->setIsButton ( Button::$ICONIC );
$button->setIcon ( "fa fa-refresh");
$button->setAction ( "lap_resume_hasil.view()" );
$button->setClass("btn btn-primary");
$form->addElement ( "", $button );
$button = new Button ( "", "", "" );
$button->setIsButton ( Button::$ICONIC );
$button->setIcon ( "fa fa-print");
$button->setAction ( "smis_print($('#print_table_lap_resume_hasil').html())" );
$button->setClass("btn btn-primary");
$form->addElement ( "", $button );

echo $form->getHtml ();
echo $uitable->getHtml ();
echo addJS ( "framework/smis/js/table_action.js" );
echo addJS ( "framework/bootstrap/js/bootstrap-datepicker.js" );
echo addCSS ( "framework/bootstrap/css/datepicker.css" );
?>
<script type="text/javascript">
var lap_resume_hasil;
//var employee;
$(document).ready(function(){
	$('.mydate').datepicker();
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
	var column=new Array('id','nama','keterangan','slug');
	lap_resume_hasil=new TableAction("lap_resume_hasil","patology","lap_resume_hasil",column);
	lap_resume_hasil.getRegulerData=function(){
		var reg_data={	
				page:this.page,
				action:this.action,
				super_command:this.super_command,
				prototype_name:this.prototype_name,
				prototype_slug:this.prototype_slug,
				prototype_implement:this.prototype_implement,
				dari:$("#"+this.prefix+"_dari").val(),
				sampai:$("#"+this.prefix+"_sampai").val()
				};
		return reg_data;
	};
	lap_resume_hasil.view();
	
});
</script>

<style type="text/css">
#table_lap_resume_hasil {
	font-size: 12px;
}
</style>