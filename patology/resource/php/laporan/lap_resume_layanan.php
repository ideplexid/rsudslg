<?php
setChangeCookie ( false );
$header = array ();
$header [] = "Nama";
$header [] = "Pria";
$header [] = "Wanita";
$header [] = "Total";

$uitable = new Table ( $header, "", NULL, false );
$uitable->setName ( "lap_resume_layanan" );
$uitable->setFooterVisible ( false );
class LapPatology extends ArrayAdapter {
	private $jumlah_pasien_daftar;
	private $jumlah_pasien_selesai;
	private $jumlah_pasien_proses;
	private $jumlah_detail;
	public function __construct($resource) {
		$this->jumlah_pasien_daftar = array (
				"Nama" => "Jumlah Pasien Terdaftar",
				"Total" => 0,
				"Pria" => 0,
				"Wanita" => 0 
		);
		$this->jumlah_pasien_selesai = array (
				"Nama" => "Jumlah Yang Selesai",
				"Total" => 0,
				"Pria" => 0,
				"Wanita" => 0 
		);
		$this->jumlah_pasien_proses = array (
				"Nama" => "Jumlah Yang Sedang Proses",
				"Total" => 0,
				"Pria" => 0,
				"Wanita" => 0 
		);
		$this->jumlah_detail = array ();
		$this->initDetail ( $resource );
	}
	public function initDetail($resource) {
		foreach ( $resource as $name => $content ) {
			foreach ( $content as $cid => $cname ) {
				$the_name = isset ( $cname ['tabname'] ) ? $cname ['tabname'] : $cname ['name'];
				$this->jumlah_detail [$cid] = array (
						"Nama" => $the_name,
						"Total" => 0,
						"Pria" => 0,
						"Wanita" => 0 
				);
			}
		}
	}
	public function adapt($d) {
		$jk = $d->jk == "0" ? "Pria" : "Wanita";
		$this->jumlah_pasien_daftar [$jk] ++;
		$this->jumlah_pasien_daftar ['Total'] ++;
		if ($d->selesai == "0") {
			$this->jumlah_pasien_proses [$jk] ++;
			$this->jumlah_pasien_proses ['Total'] ++;
		}
		if ($d->selesai == "1") {
			$this->jumlah_pasien_selesai [$jk] ++;
			$this->jumlah_pasien_selesai ['Total'] ++;
		}
		$detail = json_decode ( $d->periksa, true );
		if ($detail == null)
			return;
		
		foreach ( $detail as $key => $value ) {
			if ($value == "1") {
				$this->jumlah_detail [$key] [$jk] ++;
				$this->jumlah_detail [$key] ['Total'] ++;
			}
		}
	}
	public function getContent($data) {
		parent::getContent ( $data );
		$result = array ();
		$result [] = $this->jumlah_pasien_daftar;
		$result [] = $this->jumlah_pasien_proses;
		$result [] = $this->jumlah_pasien_selesai;
		$result [] = array (
				"Nama" => "<strong>DETAIL PER PELAYANAN</strong>",
				"Total" => "<strong>TOTAL</strong>",
				"Pria" => "<strong>PRIA</strong>",
				"Wanita" => "<strong>WANITA</strong>" 
		);
		foreach ( $this->jumlah_detail as $name => $content ) {
			$result [] = $content;
		}
		$final = $this->removeZero ( $result );
		return $final;
	}
}

if (isset ( $_POST ['command'] )) {
	require_once 'patology/resource/PatologyResource.php';
	$resource = new PatologyResource ();
	$adapter = new LapPatology ( $resource->list_layanan );
	$dbtable = new DBTable ( $db, "smis_pat_pesanan" );
	if (isset ( $_POST ['dari'] ) && isset ( $_POST ['sampai'] ) && $_POST ['dari'] != "" && $_POST ['sampai'] != "") {
		$dari = $_POST ['dari'];
		$sampai = $_POST ['sampai'];
		$dbtable->addCustomKriteria ( "'" . $dari . "'", "<=tanggal" );
		$dbtable->addCustomKriteria ( "'" . $sampai . "'", ">=tanggal" );
		$dbtable->setShowAll ( true );
	}
	$dbtable->setOrder ( " tanggal ASC " );
	$dbres = new DBResponder ( $dbtable, $uitable, $adapter );
	$data = $dbres->command ( $_POST ['command'] );
	echo json_encode ( $data );
	return;
}

$uitable->addModal ( "dari", "date", "Dari", "" );
$uitable->addModal ( "sampai", "date", "Sampai", "" );
$uitable->addModal ( "ruangan", "hidden", "", "igd" );
$modal = $uitable->getModal ();

$form = $modal->getForm ();
$button = new Button ( "", "", "" );
$button->setIsButton ( Button::$ICONIC );
$button->setIcon ( "fa fa-refresh");
$button->setAction ( "lap_resume_layanan.view()" );
$button->setClass("btn btn-primary");
$form->addElement ( "", $button );
$button = new Button ( "", "", "" );
$button->setIsButton ( Button::$ICONIC );
$button->setIcon ( "fa fa-print");
$button->setClass("btn btn-primary");
$button->setAction ( "smis_print($('#print_table_lap_resume_layanan').html())" );
$form->addElement ( "", $button );

echo $form->getHtml ();
echo $uitable->getHtml ();
echo addJS ( "framework/smis/js/table_action.js" );
echo addJS ( "framework/bootstrap/js/bootstrap-datepicker.js" );
echo addCSS ( "framework/bootstrap/css/datepicker.css" );
?>
<script type="text/javascript">
var lap_resume_layanan;
//var employee;
$(document).ready(function(){
	$('.mydate').datepicker();
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
	var column=new Array('id','nama','keterangan','slug');
	lap_resume_layanan=new TableAction("lap_resume_layanan","patology","lap_resume_layanan",column);
	lap_resume_layanan.getRegulerData=function(){
		var reg_data={	
				page:this.page,
				action:this.action,
				super_command:this.super_command,
				prototype_name:this.prototype_name,
				prototype_slug:this.prototype_slug,
				prototype_implement:this.prototype_implement,
				dari:$("#"+this.prefix+"_dari").val(),
				sampai:$("#"+this.prefix+"_sampai").val()
				};
		return reg_data;
	};
	lap_resume_layanan.view();
	
});
</script>

<style type="text/css">
#table_lap_resume_layanan {
	font-size: 12px;
}
</style>