<?php
$header = array ();
$header [] = "No.";
$header [] = "Tanggal";
$header [] = "Nama";
$header [] = "Jenis Pasien";
$header [] = "NRM";
$header [] = "No. Reg";
$header [] = "Nomor";
$header [] = "Kelas";
$header [] = "Ruangan";
$header [] = "Biaya";
$header [] = "Response Time";

$uitable = new Table ( $header, "", NULL, false );
$uitable->setName ( "lap_detail_layanan" );

if (isset ( $_POST ['command'] )) {
	require_once "patology/class/adapter/LapDetailLayananAdapter.php";
	$adapter = new LapDetailLayananAdapter ();
	$dbtable = new DBTable ( $db, "smis_pat_pesanan" );
	if (isset ( $_POST ['dari'] ) && isset ( $_POST ['sampai'] ) && $_POST ['dari'] != "" && $_POST ['sampai'] != "") {
		$dari = $_POST ['dari'];
		$sampai = $_POST ['sampai'];
		$layanan = $_POST ['layanan'];
		$dbtable->addCustomKriteria ( "'" . $dari . "'", "<=tanggal" );
		$dbtable->addCustomKriteria ( "'" . $sampai . "'", ">=tanggal" );
		if ($layanan != "") {
            $adapter->setSlugTindakan($layanan);
			$dbtable->addCustomKriteria ( "periksa", " LIKE '%\"" . $layanan . "\":\"1\"%' " );
		}
	}
	$dbtable->setOrder ( " tanggal ASC " );
	$dbres = new DBResponder ( $dbtable, $uitable, $adapter );
	$data = $dbres->command ( $_POST ['command'] );
	echo json_encode ( $data );
	return;
}

require_once 'patology/resource/PatologyResource.php';
$resource = new PatologyResource ();
$layanan = new OptionBuilder ();
$layanan->add ( "", "" );
foreach ( $resource->list_layanan as $key => $content ) {
	foreach ( $content as $key => $value ) {
		$layanan->add ( $value ['name'], $key );
	}
}

$uitable->addModal ( "dari", "date", "Dari", "" );
$uitable->addModal ( "sampai", "date", "Sampai", "" );
$uitable->addModal ( "ruangan", "hidden", "", "igd" );
$uitable->addModal ( "layanan", "select", "Layanan", $layanan->getContent () );

$modal = $uitable->getModal ();
$form = $modal->getForm ();

$button = new Button ( "", "", "" );
$button->setIsButton ( Button::$ICONIC );
$button->setIcon ( "fa fa-refresh");
$button->setAction ( "lap_detail_layanan.view()" );
$button->setClass("btn btn-primary");
$form->addElement ( "", $button );
$button = new Button ( "", "", "" );
$button->setIsButton ( Button::$ICONIC );
$button->setIcon ( "fa fa-print");
$button->setClass("btn btn-primary");
$button->setAction ( "smis_print($('#print_table_lap_detail_layanan').html())" );
$form->addElement ( "", $button );
echo $form->getHtml ();

echo $uitable->getHtml ();
echo addJS ( "framework/smis/js/table_action.js" );
echo addJS ( "framework/bootstrap/js/bootstrap-datepicker.js" );
echo addCSS ( "framework/bootstrap/css/datepicker.css" );

?>
<script type="text/javascript">
var lap_detail_layanan;
//var employee;
$(document).ready(function(){
	$('.mydate').datepicker();
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
	var column=new Array('id','nama','keterangan','slug');
	lap_detail_layanan=new TableAction("lap_detail_layanan","patology","lap_detail_layanan",column);
	lap_detail_layanan.getRegulerData=function(){
		var reg_data={	
				page:this.page,
				action:this.action,
				super_command:this.super_command,
				prototype_name:this.prototype_name,
				prototype_slug:this.prototype_slug,
				prototype_implement:this.prototype_implement,
				dari:$("#"+this.prefix+"_dari").val(),
				sampai:$("#"+this.prefix+"_sampai").val(),
				layanan:$("#"+this.prefix+"_layanan").val()
				};
		return reg_data;
	};
	lap_detail_layanan.view();
	
});
</script>
