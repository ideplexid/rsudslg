<?php

require_once "smis-base/smis-include-service-consumer.php";

global $db;
$data = $_POST;

$dbtavle = new DBTable ( $db, "smis_pat_pesanan");
$row = $dbtavle->select ( $_POST ['id_antrian'] ); // id adalah id antrian

$data ['id_antrian'] = $_POST ['id_antrian'];
$data ['polislug'] = "patology";
$data ['noreg_pasien'] = $row->noreg_pasien;
$data ['nama_pasien'] = $row->nama_pasien;
$data ['nrm_pasien'] = $row->nrm_pasien;
$data ['action'] = "rehab_medik";
$data ['page'] = "patology";

$data ['jk'] = $row->jk;
$data ['umur'] = $row->umur;
$data ['asal_ruang'] = $row->ruangan;
$data ['page'] = "patology";
$data ['alamat'] = $row->alamat;


if( (!isset($_POST['super_command']) || $_POST['super_command']=="") && (!isset($_POST['command']) || $_POST['command']=="") ){
    $uitable = new Table(array());
    $uitable ->addModal("_nama","text","Nama Pasien",$row->nama_pasien,"",null,true);
    $uitable ->addModal("_noreg","text","Noreg Pasien",$row->noreg_pasien,"",null,true);
    $uitable ->addModal("_nrm","text","NRM Pasien",$row->nrm_pasien,"",null,true);

    $back = new Button("","","Back");
    $back ->setIsButton(BUtton::$ICONIC_TEXT);
    $back ->setClass("btn btn-primary");
    $back ->setIcon("fa fa-backward");
    $back ->setAction("back_to_pemeriksaan()");

    $form = $uitable ->getModal()->getForm();
    $form ->setTitle("Rehab Medik Patology");
    $form ->addElement("",$back);

    echo $form->getHtml();
    echo addJS("patology/resource/js/rehab_medik.js",false);
    echo addCSS("patology/resource/css/rehab_medik.css",false);
}

$service = new ServiceConsumer ( $db, "get_lap_rehabilitasi_medik", $data, "medical_record" );
$service->execute();
echo $service->getContent();
