<?php
require_once 'smis-libs-hrd/EmployeeResponder.php';
require_once 'smis-framework/smis/api/SettingsBuilder.php';
global $db;
class PatologySettingBulder extends SettingsBuilder {
	public function getJS() {
		$result = parent::getJS ();
		ob_start ();
		?>
		<script type="text/javascript">	
			var dokter_settings_patology;
			$(document).ready(function(){
				dokter_settings_patology=new TableAction("dokter_settings_patology","patology","settings",new Array());
				dokter_settings_patology.setSuperCommand("dokter_settings_patology");
				dokter_settings_patology.selected=function(json){
					$("#patology-konsultan-nama").val(json.nama);
					$("#patology-konsultan-id").val(json.id);
				};
				
			});
		</script>
		<?php
		$r2 = ob_get_clean ();
		$result .= $r2;
		return $result;
	}
}

$slug_konsultan = getSettings($db, 'patology-slug-dokter-konsultan-lab');
$dkadapter = new SimpleAdapter ();
$dkadapter->add ( "Jabatan", "nama_jabatan" );
$dkadapter->add ( "Nama", "nama" );
$dkadapter->add ( "NIP", "nip" );
$head=array ('Nama','Jabatan',"NIP" );
$dktable = new Table ( $head, "", NULL, true );
$dktable->setName ( "dokter_settings_patology" );
$dktable->setModel ( Table::$SELECT );
if( $slug_konsultan != null || $slug_konsultan != '') {
    $dokter = new EmployeeResponder ( $db, $dktable, $dkadapter, $slug_konsultan );
} else {
    $dokter = new EmployeeResponder ( $db, $dktable, $dkadapter, "dokter" );
}


$smis = new PatologySettingBulder ( $db, "pat_settings", "patology", "settings" );
$smis->setShowDescription ( true );
$smis->addSuperCommandResponder ( "dokter_settings_patology", $dokter );

$smis->setTabulatorMode ( Tabulator::$POTRAIT );
$smis->addTabs ( "patology", "Patology" );
$mr = new OptionBuilder();
$mr->addSingle("Otomatis");
$mr->addSingle("Manual");

$url = new SettingsItem ( $db, "patology-nomor", "Penomoran", $mr->getContent(), "select", "" );
$smis->addItem ( "patology", $url );
$url = new SettingsItem ( $db, "patology-nomor-fnab", "Awal Nomor FNAB", "0", "text", "" );
$smis->addItem ( "patology", $url );
$url = new SettingsItem ( $db, "patology-nomor-sitologi", "Awal Nomor SITOLOGI", "0", "text", "" );
$smis->addItem ( "patology", $url );
$url = new SettingsItem ( $db, "patology-nomor-histologi", "Awal Nomor HISTOLOGI", "0", "text", "" );
$smis->addItem ( "patology", $url );


$url = new SettingsItem ( $db, "patology-slug-dokter-konsultan-lab", "Slug Dokter Konsultan Lab", "", "text", "Untuk memfilter dokter yang muncul pada field Konsultan. Keterangan slug dpat dilihat pada Menu Personalia > Data Induk > Bagian" );
$smis->addItem ( "patology", $url );
$url = new SettingsItem ( $db, "patology-slug-petugas-lab", "Slug Petugas Lab", "", "text", "Untuk memfilter pegawai yang muncul pada field Petugas. Keterangan slug dpat dilihat pada Menu Personalia > Data Induk > Bagian" );
$smis->addItem ( "patology", $url );
$url = new SettingsItem ( $db, "patology-konsultan-nama", "Nama Konsultan patology", "", "chooser-settings-dokter_settings_patology", "Pilih Konsultan (Jangan Edit Manual) " );
$smis->addItem ( "patology", $url );
$url = new SettingsItem ( $db, "patology-konsultan-id", "ID Konsultan patology", "", "text", "Akan Otomatis Terisi Sendiri (Jangan Edit Manual)" );
$smis->addItem ( "patology", $url );
//$url = new SettingsItem ( $db, "patology-konsultan-ttd", "File Tanda Tangan Dokter Konsultan Patologi", "", "file-single-image", "Upload File Tanda Tangan Dokter Konsultan Patologi (file format .png Not Interlaced Image)" );
//$smis->addItem ( "patology", $url );
$url = new SettingsItem ( $db, "patology-pj-ttd-file", "File Tanda Tangan Dokter Penanggung Jawab Patologi", "", "file-single-image", "Upload File Tanda Tangan Dokter Penanggung Jawab Patologi (file format .png Not Interlaced Image)" );
$smis->addItem ( "patology", $url );

$option=new OptionBuilder();
$option->addSingle("Stand Alone");
$option->addSingle("Integrated");
$url = new SettingsItem ( $db, "patology-sistem-model", "Model System", $option->getContent(), "select", "System Model Patology untuk persiapan keseluruhan, ketika Stand Alone berarti sendiri - sendiri, ketika Integrated berarti terintegrasi" );
$smis->addItem ( "patology", $url );
$url = new SettingsItem ( $db, "patology-allow-edit-ruangan", "Perbolehkan Mengubah Ruangan", "0", "checkbox", "perbolehkan mengubah ruangan meskipun integrasi" );
$smis->addItem ( "patology", $url );
$url = new SettingsItem ( $db, "patology-model-jenis-pasien", "Perbaikan Model Jenis Pasien", "0", "checkbox", "Model Jenis Pasien Select (v) atau Text ( )" );
$smis->addItem ( "patology", $url );
$url = new SettingsItem ( $db, "patology-show-hapusan-darah", "Tampilkan Hapusan Darah", "0", "checkbox", "Menampilkan Hasil Untuk Hapusan Darah" );
$smis->addItem ( "patology", $url );
$default_template_hapusan = "<p><br></p>
<table class='table table-bordered'>
	<tbody>
		<tr>
			<td><span style='font-weight: bold;'>Jenis Sel</span></td>
			<td><span style='font-weight: bold;'>Hasil Pemeriksaan</span></td>
		</tr>
		<tr>
			<td>Eritrosit</td>
			<td>-</td>
		</tr>
		<tr>
			<td>Leukosit</td>
			<td>-</td>
		</tr>
		<tr>
			<td>Trombosit</td>
			<td>-</td>
		</tr>
		<tr>
			<td><span style='font-weight: bold;'>Kesimpulan</span></td>
			<td>-</td>
		</tr>
	</tbody>
</table>
<p><br></p>";
$url = new SettingsItem ( $db, "patology-template-hapusan-darah", "Kode HTML untuk Template Hapusan Darah", $default_template_hapusan, "textarea", "Kode HTML untuk Template Hapusan Darah" );
$smis->addItem ( "patology", $url );
$url = new SettingsItem ( $db, "patology-show-pemeriksaan-lain", "Tampilkan Pemeriksaan Lain", "0", "checkbox", "Menampilkan Pemeriksaan Lain" );
$smis->addItem ( "patology", $url );
$url = new SettingsItem ( $db, "patology-show-print-backup", "Tampilkan Cetakan Backup", "0", "checkbox", "Menampilkan Cetakan Backup" );
$smis->addItem ( "patology", $url );
$url = new SettingsItem ( $db, "patology-auto-save-after-print", "Auto Save Setelah Cetak Tombol Print", "0", "checkbox", "Auto Save Setelah Cetak Tombol Print" );
$smis->addItem ( "patology", $url );


$smis->addTabs ( "pat_printout", "Print Out" );
$url = new SettingsItem ( $db, "patology-show-standard-print", "Tampilkan Print Standard", "", "checkbox", "menampilkan Print Standard" );
$smis->addItem ( "pat_printout", $url );
$url = new SettingsItem ( $db, "patology-show-print-for-iks", "Cetak Hasil Lab untuk IKS", "", "checkbox", "Secara Default Sistem akan Mencetak Hasil Lab untuk IKS (Ikatan Kerja Sama)" );
$smis->addItem ( "pat_printout", $url );
$url = new SettingsItem ( $db, "patology-show-print-for-pasien", "Cetak Hasil Lab untuk Pasien", "", "checkbox", "Secara Default Sistem akan Mencetak Hasil Lab untuk Pasien" );
$smis->addItem ( "pat_printout", $url );
$url = new SettingsItem ( $db, "patology-show-print-for-rekam-medis", "Cetak Hasil Lab untuk Rekam Medis", "", "checkbox", "Secara Default Sistem akan Mencetak Hasil Lab untuk Rekam Medsi" );
$smis->addItem ( "pat_printout", $url );
$url = new SettingsItem ( $db, "patology-show-print-for-patology", "Cetak Hasil Lab untuk Patology", "", "checkbox", "Secara Default Sistem akan Mencetak Hasil Lab untuk Patology" );
$smis->addItem ( "pat_printout", $url );
$url = new SettingsItem ( $db, "patology-header-size", "Size of Header", "30", "text", "Font Size of Header Print Out" );
$smis->addItem ( "pat_printout", $url );
$url = new SettingsItem ( $db, "patology-sheader-size", "Size of Secondary Header", "20", "text", "Font Size of Secondary Header Print Out" );
$smis->addItem ( "pat_printout", $url );
$url = new SettingsItem ( $db, "patology-centergap", "Center Gap Size", "40%", "text", "Center Gap Size in Percent(%) or Pixel (px)" );
$smis->addItem ( "pat_printout", $url );
$url = new SettingsItem ( $db, "patology-ttd_pj", "Tampilkan TTD Penanggung Jawab", "", "checkbox", "Menampilkan Tanda Tangan Penanggung Jawab Patologi pada Form Cetak Hasil Pemeriksaan" );
$smis->addItem ( "pat_printout", $url );
$url = new SettingsItem ( $db, "patology-ukuran-font-title-cetak-pdf", "Ukuran Font Title Cetak Hasil PDF", "18", "text", "Ukuran Font Title Nama Rumah Sakit Cetak Hasil PDF. By default 18." );
$smis->addItem ( "pat_printout", $url );
$url = new SettingsItem ( $db, "patology-ukuran-font-sub-title-cetak-pdf", "Ukuran Font Sub Title Cetak Hasil PDF", "12", "text", "Ukuran Font Sub Title Cetak Hasil PDF. By default 12." );
$smis->addItem ( "pat_printout", $url );
$url = new SettingsItem ( $db, "patology-ukuran-font-header-cetak-pdf", "Ukuran Font Header (Identitas Pasien) Cetak Hasil PDF", "10", "text", "Ukuran Font Header (Identitas Pasien) Cetak Hasil PDF. By default 10." );
$smis->addItem ( "pat_printout", $url );
$url = new SettingsItem ( $db, "patology-ukuran-font-content-cetak-pdf", "Ukuran Font Content Cetak Hasil PDF", "8", "text", "Ukuran Font Content Cetak Hasil PDF. By default 8." );
$smis->addItem ( "pat_printout", $url );
//$url = new SettingsItem ( $db, "patology-print-hasil-rsukaliwates", "Menampilkan Format Cetak Lab kaliwates", "", "checkbox", "Menampilkan Format Cetak Lab kaliwates" );
//$smis->addItem ( "pat_printout", $url );


$smis->addTabs ( "kwitansi", "Kwitansi" );
$kwitansi=new OptionBuilder();
$kwitansi->addSingle("Reguler");
$kwitansi->addSingle("Mini");
$kwitansi->addSingle("Detail");
$url = new SettingsItem ( $db, "patology-kwitansi-model", "Jenis Model Kwitansi", $kwitansi->getContent(), "select", "Model Kwitansi" );
$smis->addItem ( "kwitansi", $url );
$url = new SettingsItem ( $db, "patology-kwitansi-jumlah", "Jumlah Kwitansi", "3", "text", "Jumlah Cetak Kwitansi" );
$smis->addItem ( "kwitansi", $url );
$url = new SettingsItem ( $db, "patology-kwitansi-tampil-tindakan", "Tampilkan Tindakanya", "0", "checkbox", "Menampilkan Tindakan" );
$smis->addItem ( "kwitansi", $url );
$url = new SettingsItem ( $db, "patology-kwitansi-tampil-jenis-pasien", "Tampilkan Jenis Pasien", "1", "checkbox", "Menampilkan Jenis Pasien" );
$smis->addItem ( "kwitansi", $url );
$url = new SettingsItem ( $db, "patology-kwitansi-css", "CSS Kwitansi", "", "textarea", "CSS Kwitansi" );
$smis->addItem ( "kwitansi", $url );

$smis->addTabs ( "lis", "LIS" );
$url = new SettingsItem ( $db, "patology-lis-stand-alone-button", "Buat Tombol Stand Alone Button Sendiri", "", "checkbox", "Untuk COnnect ke LIS harus Klik Dahulu" );
$smis->addItem ( "lis", $url );
$url = new SettingsItem ( $db, "patology-lis-connect", "Connect Patology LIS", "", "checkbox", "Connect LIS Patology" );
$smis->addItem ( "lis", $url );
$url = new SettingsItem ( $db, "patology-show-lis-print", "Tampilkan Print LIS", "", "checkbox", "Tampilkan Print LIS" );
$smis->addItem ( "lis", $url );

$smis->addTabs ( "ui", "Tampilan" );
$smis->addItem ( "ui", new SettingsItem ( $db, "patology-mode-get-tagihan", "Model Tagihan untuk Kasir", "0", "checkbox", "(v) Sederhana, (x) Komplek" ));
$smis->addItem ( "ui", new SettingsItem ( $db, "patology-show-diagnosa", "Tampilkan Diagnosa Pasien", "0", "checkbox", "Menampilkan Dagnosa Pasien" ));
$smis->addItem ( "ui", new SettingsItem ( $db, "patology-show-marketing", "Tampilkan Marketing", "0", "checkbox", "Menampilkan Marketing" ));



$smis->addTabs ( "ui_pemeriksaan","UI Pemeriksaan" );
require_once "smis-base/smis-include-service-consumer.php";
$serv=new ServiceConsumer($db,"get_kelas",null,"manajemen");
$serv->execute();
$opt_kelas=new OptionBuilder();
$opt_kelas->add("","",1);
$jns=$serv->getContent();
foreach($jns as $x){
    $opt_kelas->add($x['nama'],$x['slug'],0);
}

$setting_kelas = new OptionBuilder();
$setting_kelas->add("Sesuai Kelas Pasien", "Sesuai Kelas Pasien",1);
$setting_kelas->add("Sesuai Kelas Tertentu", "Sesuai Kelas Tertentu",0);
$setting_kelas->add("Dapat Memilih", "Dapat Memilih",0);

$smis->addItem ( "ui_pemeriksaan", new SettingsItem ( $db, "patology-ui-pemeriksaan-edit-tindakan", "Edit Tindakan", "0", "checkbox", "Jika di centang maka Petugas Lab Bisa Melakukan Edit Tindakan" ) );
$smis->addItem ( "ui_pemeriksaan", new SettingsItem ( $db, "patology-ui-pemeriksaan-add-tindakan", "Menambah Tindakan", "1", "checkbox", "Jika di Centang Petugas Lab bisa menambah data pasien, jadi bisa ngisi sendiri" ) );
$smis->addItem ( "ui_pemeriksaan", new SettingsItem ( $db, "patology-ui-pemeriksaan-auto-archive", "Auto Archive", "0", "text", "Sistem Secara Otomatis Meng-arsipkan data Radiology jika Sudah Lebih dari x Hari (0 berarti tidak aktif)" ) );
$smis->addItem ( "ui_pemeriksaan", new SettingsItem ( $db, "patology-ui-pemeriksaan-setting-kelas", "Setting Kelas Pada Lab", $setting_kelas->getContent(), "select", "Setting Kelas Pada Lab (Jika dipilih [Sesuai Kelas Pasien], maka ketika memilih pasien otomatis field Ruangan dan Kelas akan mengikuti Data Pasien pada Pendaftaran; [Sesuai Kelas Tertentu], maka field Kelas akan terisi Berdasarkan Setting Default Kelas; [Dapat Memilih], maka petugas dapat mengisi field Kelas secara Bebas Berdasarkan Kelas yang ada)." ) );
$smis->addItem ( "ui_pemeriksaan", new SettingsItem ( $db, "patology-ui-pemeriksaan-default-kelas", "Default Kelas Pasien", $opt_kelas->getContent(), "select", "Default Kelas Pasien pada Patology (Wajib Diisi Jika Setting Kelas Pada Lab dipilih -Sesuai Kelas Tertentu-)." ) );
$smis->addItem ( "ui_pemeriksaan", new SettingsItem ( $db, "patology-aktifkan-tutup-tagihan", "Aktifkan Tutup Tagihan", "0", "checkbox", "mengaktifkan tutup tagihan" ) );

$smis->addTabs ( "ui_pendaftaran","UI Ruangan" );
$smis->addItem ( "ui_pendaftaran", new SettingsItem ( $db, "patology-ui-pemeriksaan-edit-hasil", "Edit Hasil", "0", "checkbox", "Jika di centang maka Petugas Ruangan Bisa Melakukan Edit Hasil" ) );


$smis->addTabs ( "accounting","Accounting" );
$smis->addItem ( "accounting", new SettingsItem ( $db, "patology-accounting-auto-notif", "Aktifkan Setting Auto Notif ke Accounting", "0", "checkbox", "Jika Dicentang Maka Sistem Akan Menotifikasi ke Accounting Secara Otomatis" ) );
$smis->addItem ( "accounting", new SettingsItem ( $db, "patology-accounting-debit-global", "Kode Accounting Debit untuk Global (Kwitansi Simple)", "", "chooser-settings-debet_global-Debet", "Kode Accounting Debit untuk Global (Kwitansi Simple)" ) ); 
$smis->addItem ( "accounting", new SettingsItem ( $db, "patology-accounting-kredit-global", "Kode Accounting Kredit untuk Global (Kwitansi Simple)", "", "chooser-settings-kredit_global-Kredit", "Kode Accounting Kredit untuk Global (Kwitansi Simple)" ) );
$smis->addSuperCommandAction("debet_global","kode_akun");
$smis->addSuperCommandAction("kredit_global","kode_akun");
$smis->addSuperCommandArray("debet_global","patology-accounting-debit-global","nomor",true);
$smis->addSuperCommandArray("kredit_global","patology-accounting-kredit-global","nomor",true);

$serv = new ServiceConsumer($db,"get_jenis_patient",NULL,"registration");
$serv ->execute();
$cons = $serv->getContent();
foreach($cons as $x){
    $smis->addItem ( "accounting", new SettingsItem ( $db, "patology-accounting-debit-global-".$x['value'], "Kode Accounting Debit ".$x['name']." untuk Global (Kwitansi Simple)", "", "chooser-settings-debet_global_".$x['value']."-Debet ".$x['name'], "Kode Accounting Debit ".$x['name']." untuk Global (Kwitansi Simple)" ) ); 
    $smis->addItem ( "accounting", new SettingsItem ( $db, "patology-accounting-kredit-global-".$x['value'], "Kode Accounting Kredit ".$x['name']." untuk Global (Kwitansi Simple)", "", "chooser-settings-kredit_global_".$x['value']."-Kredit ".$x['name'], "Kode Accounting Kredit ".$x['name']." untuk Global (Kwitansi Simple)" ) );
    $smis->addSuperCommandAction("debet_global_".$x['value'],"kode_akun");
    $smis->addSuperCommandAction("kredit_global_".$x['value'],"kode_akun");
    $smis->addSuperCommandArray("debet_global_".$x['value'],"patology-accounting-debit-global-".$x['value'],"nomor",true);
    $smis->addSuperCommandArray("kredit_global_".$x['value'],"patology-accounting-kredit-global-".$x['value'],"nomor",true);
}

$response = $smis->init ();
?>