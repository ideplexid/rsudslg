<?php 

require_once 'smis-libs-class/MasterTemplate.php';

$layanan_dbtable = new DBTable ( $db, "smis_pat_layanan" );
$layanan_uitable = new Table(array('Pemeriksaan','Slug','Group'),"");
$layanan_uitable->setName("layanan");
$layanan_uitable->setModel (Table::$SELECT);
$layanan_adapter = new SimpleAdapter();
$layanan_adapter->add("Pemeriksaan", "nama");
$layanan_adapter->add("Slug", "slug");
$layanan_adapter->add("Group", "grup");
$layanan_responder = new DBResponder($layanan_dbtable, $layanan_uitable, $layanan_adapter);

$m=new MasterTemplate($db, "smis_pat_hasil", "patology", "list_hasil");
$m->getUItable()->addHeaderElement("Nomor");
$m->getUItable()->addHeaderElement("Layanan");
$m->getUItable()->addHeaderElement("Nama");
$m->getUItable()->addHeaderElement("Group");
$m->getUItable()->addHeaderElement("Slug");
$m->getUItable()->addHeaderElement("Nilai Normal");
$m->getUItable()->addHeaderElement("Nilai Text");
$m->getUItable()->addHeaderElement("Metode");
$m->getUItable()->addHeaderElement("Lk. Min");
$m->getUItable()->addHeaderElement("Lk. Max");
$m->getUItable()->addHeaderElement("Pr. Min");
$m->getUItable()->addHeaderElement("Pr. Max");
$m->getUItable()->addHeaderElement("Nilai Min");
$m->getUItable()->addHeaderElement("Nilai Max");
$m->getUItable()->addHeaderElement("Nilai");
$m->getUItable()->addHeaderElement("Kurang Dari");
$m->getUItable()->addHeaderElement("Lebih Dari");

$m->getAdapter()->add("Nomor", "nomor");
$m->getAdapter()->add("Nilai Normal", "nn");
$m->getAdapter()->add("Nilai Text", "nt");
$m->getAdapter()->add("Metode", "metode");
$m->getAdapter()->add("Lk. Min", "lmin");
$m->getAdapter()->add("Lk. Max", "lmax");
$m->getAdapter()->add("Pr. Min", "pmin");
$m->getAdapter()->add("Pr. Max", "pmax");
$m->getAdapter()->add("Nilai Min", "valmin");
$m->getAdapter()->add("Nilai Max", "valmax");
$m->getAdapter()->add("Nilai", "sameval");
$m->getAdapter()->add("Kurang Dari", "lessthan");
$m->getAdapter()->add("Lebih Dari", "morethan");
$m->getAdapter()->add("Group", "grup");
$m->getAdapter()->add("Nama", "name");
$m->getAdapter()->add("Slug", "slug");
$m->getAdapter()->add("Layanan", "layanan");

$option=new OptionBuilder();
$option->add("","nothing","1");
$option->add("Kurang Dari","less-than");
$option->add("Lebih Dari","more-than");
$option->add("Diantara","between");
$option->add("Sama Dengan","same");
$option->add("Reaktif - Non Reaktif","reaktif_nonreaktif");
$option->add("Negatif","negatif");
$option->add("Negatif 2","negatif_2");
$option->add("Normal","normal");
$option->add("Normal - Diabetes","normal_diabetes");
$option->add("Negatif - Positif","negatif_positif");
$option->add("Laki - Laki / Perempuan","laper");
$option->add("Teks","textarea");

$m->getUItable()->addModal("id", "hidden", "", "");
$m->getUItable()->addModal("layanan", "chooser-list_hasil-layanan-Layanan", "Layanan", "");
$m->getUItable()->addModal("slug", "text", "Slug", "");
$m->getUItable()->addModal("grup", "text", "Group", "");
$m->getUItable()->addModal("name", "text", "Nama", "");
$m->getUItable()->addModal("nomor", "text", "Nomor", "");
$m->getUItable()->addModal("nn", "select", "Nilai Normal", $option->getContent());
$m->getUItable()->addModal("nt", "text", "Nilai Text", "");
$m->getUItable()->addModal("metode", "text", "Metode", "");
$m->getUItable()->addModal("lmin", "text", "Lk. Min", "");
$m->getUItable()->addModal("lmax", "text", "Lk. Max", "");
$m->getUItable()->addModal("pmin", "text", "Pr. Min", "");
$m->getUItable()->addModal("pmax", "text", "Pr. Max", "");
$m->getUItable()->addModal("valmin", "text", "Nilai Min", "");
$m->getUItable()->addModal("valmax", "text", "Nilai Max", "");
$m->getUItable()->addModal("sameval", "text", "Nilai", "");
$m->getUItable()->addModal("lessthan", "text", "Kurang Dari", "");
$m->getUItable()->addModal("morethan", "text", "Lebih Dari", "");
$m->getUItable()->addModal("lt", "checkbox", "Left Gap", "0");
$m->getModal()->setTitle("Hasil Lab");

$m  ->getSuperCommand()
    ->addResponder("layanan", $layanan_responder);

$m->addResouce("js", "patology/resource/js/list_hasil.js", "after", false);
$m->initialize();

?>