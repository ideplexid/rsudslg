<?php 
require_once 'smis-libs-class/MasterTemplate.php';
global $db;
$arsip_admin=new MasterTemplate($db, "smis_pat_pesanan", "patology", "arsip_admin");
$uitable=$arsip_admin->getUItable();
$uitable->setReloadButtonEnable(false);
$uitable->setPrintButtonEnable(false);
$uitable->setEditButtonEnable(false);
$uitable->setAddButtonEnable(false);
$uitable->setDelButtonEnable(false);
$arsip_admin->getDBtable()->addCustomKriteria("selesai", "='1'");

$btn=new Button("", "", "");
$btn->setClass("btn btn-primary");
$btn->setIcon("fa fa-forward");
$btn->setIsButton(Button::$ICONIC);
$uitable->addContentButton("restore_arsip", $btn);
$header=array("No.","Tanggal","No. Pat","Pasien","NRM","No. Reg","Nilai");
$uitable->setHeader($header);

$adapter=$arsip_admin->getAdapter();
$adapter->setUseNumber(true, "No.","back.")
		->add("Tanggal", "tanggal","date d M Y")
		->add("No. Pat", "no_pat")
		->add("Pasien","nama_pasien")
		->add("NRM","nrm_pasien","only-digit8")
		->add("No. Reg","noreg_pasien","only-digit8")
		->add("Nilai","biaya","money Rp.");
$arsip_admin->addResouce("js","patology/resource/js/arsip_admin.js");
$arsip_admin->initialize();

?>