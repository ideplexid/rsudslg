<?php
global $PLUGINS;

$init ['name'] = 'patology';
$init ['path'] = SMIS_DIR . "patology/";
$init ['description'] = "Patology";
$init ['require'] = "administrator";
$init ['service'] = "";
$init ['version'] = "6.3.6";
$init ['number'] = "36";
$init ['type'] = "";
$myplugins = new Plugin ( $init );
$PLUGINS [$init ['name']] = $myplugins;
?>
