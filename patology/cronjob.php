<?php 
	require_once 'smis-libs-inventory/cronjob/StockBarangED.php';
	require_once 'smis-libs-inventory/cronjob/StockObatED.php';
	global $db;
	global $notification;
	$barang=new CronjobStockBarangED($db, $notification, "smis_pat_stok_barang", "patology");
	$barang->initialize();
	$obat=new CronjobStockObatED($db, $notification, "smis_pat_stok_obat", "patology");
	$obat->initialize();
    
    /*auto archive pasien*/
    $auto=getSettings($db,"patology-ui-pemeriksaan-auto-archive","0")*1;
    if($auto>0){
        loadLibrary("smis-libs-function-time");
        $max_date=sub_date(date("Y-m-d"),2);
        $query="update smis_pat_pesanan selesai=1 WHERE tanggal<'".$max_date."' AND selesai=0 ";
    }
?>