<?php

global $db;
global $user;

$nama_konsultan = getSettings ( $db, "patology-konsultan-nama", "" );
$id_konsultan = getSettings ( $db, "patology-konsultan-id", "" );
$ttd_pj = getSettings ( $db, "patology-ttd_pj", "0" );
$town = getSettings($db,"smis_autonomous_town","");
$town = strtolower($town);
$town = ucfirst($town);
$dd = ArrayAdapter::format ( "date d M Y", date ( "Y-m-d" ) );
$logo = getLogoNonInterlaced ();
$username = $user->getNameOnly();

require_once("smis-libs-out/fpdf/fpdf.php");

class HasilRSUKPDF extends FPDF {
    private $town;
    private $date;
    private $logo;
    
    public function setTown($town) {
        $this->town = $town;
    }
    
    public function setDate($date) {
        $this->date = $date;
    }
    
    public function setLogo($logo) {
        $this->logo = $logo;
    }
    
    //Page Header
    function Header() {
        $this->SetY(30);
        $line_break = 5;
        $this->Ln(15);
        $this->SetFont("Times", "B", 10);
        $this->Cell(195, 0, " ", "B", 0, "L");
        $this->Ln($line_break);
        $this->SetFont("Times", "B", 10);
        $this->Cell(35, 0, "Nama", 0, 0, "L");
        $this->Cell(2, 0, ":", 0, 0, "L");
        $this->Cell(60, 0, $_POST['nama_pasien'], 0, 0, "L");
        $this->Cell(5, 0, "", 0, 0, "L");
        $this->Cell(55, 0, "NRM - No.Lab", 0, 0, "L");
        $this->Cell(2, 0, ":", 0, 0, "L");
        $this->Cell(40, 0, $_POST['nrm']." - ".$_POST['no_pat'], 0, 0, "L");
        $this->Ln($line_break);
        
        $this->Cell(35, 0, "Pengirim", 0, 0, "L", false);
        $this->Cell(2, 0, ":", 0, 0, "L", false);
        $this->Cell(60, 0, $_POST['pengirim'], 0, 0, "L", false);
        $this->Cell(5, 0, "", 0, 0, "L", false);
        $this->Cell(55, 0, "Ruang", 0, 0, "L", false);
        $this->Cell(2, 0, ":", 0, 0, "L", false);
        $ruangan = ArrayAdapter::slugFormat("unslug", $_POST['ruangan']);
        $this->Cell(40, 0, $ruangan, 0, 0, "L", false);
        $this->Ln($line_break);
        
        $this->Cell(35, 0, "L/P - Usia", 0, 0, "L", false);
        $this->Cell(2, 0, ":", 0, 0, "L", false);
        $this->Cell(60, 0, $_POST['jk']." - ".$_POST['umur'], 0, 0, "L", false);
        $this->Cell(5, 0, "", 0, 0, "L", false);
        $this->Cell(55, 0, "Tanggal Periksa", 0, 0, "L", false);
        $this->Cell(2, 0, ":", 0, 0, "L", false);
        $this->Cell(40, 0, $_POST['tanggal'], 0, 0, "L", false);
        $this->Ln($line_break);
        
        $this->Cell(35, 0, "Alamat", 0, 0, "L", false);
        $this->Cell(2, 0, ":", 0, 0, "L", false);
        $this->Cell(60, 0, $_POST['alamat'], 0, 0, "L", false);
        $this->Cell(5, 0, "", 0, 0, "L", false);
        $this->Cell(55, 0, "Jam Terima Sample - Jam Selesai", 0, 0, "L", false);
        $this->Cell(2, 0, ":", 0, 0, "L", false);
        $this->Cell(40, 0, $_POST['sample_selesai'], 0, 0, "L", false);
        $this->Ln($line_break);
        
        $this->Cell(35, 6, " ", "B", 0, "L", false);
        $this->Cell(2, 6, " ", "B", 0, "L", false);
        $this->Cell(158, 6, $_POST['alamat_lengkap'], "B", 0, "L", false);
        $this->Ln(15);
    }
    
    //Page Footer
    function Footer() {
        $this->SetY(-40);
        $line_break = 5;
        $tempat_waktu = $this->town.", ".$this->date;
        $this->Cell(65, 6, "", 0, 0, "C");
        $this->Cell(65, 6, "", 0, 0, "C");
        $this->Cell(65, 6, $tempat_waktu, 0, 0, "C");
        $this->Ln($line_break);
        $this->Cell(65, 6, "", 0, 0, "C");
        $this->Cell(65, 6, "", 0, 0, "C");
        $this->Cell(65, 6, "Dokter Ahli Patologi Klinik,", 0, 0, "C");
        $this->Ln(20);
        $this->Cell(65, 6, "", 0, 0, "C");
        $this->Cell(65, 6, "", 0, 0, "C");
        $this->Cell(65, 6, $_POST['konsultan'], 0, 0, "C");
    }
}

$fpdf = new HasilRSUKPDF();
$fpdf->setTown($town);
$fpdf->setDate($dd);
$fpdf->setLogo($logo);

if($_POST['iks_cek'] == '1') {
    $fpdf->AddPage("P");
    $fpdf->SetAutoPageBreak(true , 50);
    $fpdf->SetFont("Times", "B", 8);
    $fpdf->Cell(65, 6, "JENIS PEMERIKSAAN", "TB", 0, "L");
    $fpdf->Cell(30, 6, "HASIL", "TB", 0, "L");
    $fpdf->Cell(60, 6, "NILAI RUJUKAN", "TB", 0, "L");
    $fpdf->Cell(40, 6, "METODE", "TB", 0, "L");
    $fpdf->Ln($line_break);
    $current_grup = '';
    for($i = 0; $i < sizeof($_POST['pemeriksaan']); $i++) {
        $pemeriksaan = explode(" $ ", $_POST['pemeriksaan'][$i]);
        $grup = $pemeriksaan[0];
        $jenis = $pemeriksaan[1];
        $hasil = $pemeriksaan[2];
        $normal = $pemeriksaan[3];
        $normal = htmlspecialchars_decode($normal);
        $metode = $pemeriksaan[4];
        $color = $pemeriksaan[5];
        if($current_grup == $grup) {
            if($i == sizeof($_POST['pemeriksaan']) - 1) {
                $fpdf->SetFont("Times", "", 8);
                $fpdf->Cell(2, 6, "", "B", 0, "L");
                $fpdf->Cell(63, 6, $jenis, "B", 0, "L");
                $fpdf->SetTextColor(0, 0, 0);
                $fpdf->Cell(30, 6, $hasil, "B", 0, "L");
                $fpdf->SetTextColor(0, 0, 0);
                $fpdf->Cell(60, 6, $normal, "B", 0, "L");
                $fpdf->Cell(40, 6, $metode, "B", 0, "L");
                $fpdf->Ln($line_break);
            } else {
                $fpdf->SetFont("Times", "", 8);
                $fpdf->Cell(3, 6, "", 0, 0, "L");
                $fpdf->Cell(62, 6, $jenis, 0, 0, "L");
                $fpdf->SetTextColor(0, 0, 0);
                $fpdf->Cell(30, 6, $hasil, 0, 0, "L");
                $fpdf->SetTextColor(0, 0, 0);
                $fpdf->Cell(60, 6, $normal, 0, 0, "L");
                $fpdf->Cell(40, 6, $metode, 0, 0, "L");
                $fpdf->Ln($line_break);
            }
        } else {
            if($i == sizeof($_POST['pemeriksaan']) - 1) {
                $fpdf->SetFont("Times", "B", 8);
                $fpdf->Cell(65, 6, $grup, 0, 0, "L");
                $fpdf->Cell(30, 6, "", 0, 0, "L");
                $fpdf->Cell(60, 6, "", 0, 0, "L");
                $fpdf->Cell(40, 6, "", 0, 0, "L");
                $fpdf->Ln($line_break);
                $current_grup = $grup;
                $fpdf->SetFont("Times", "", 8);
                $fpdf->Cell(3, 6, "", "B", 0, "L");
                $fpdf->Cell(62, 6, $jenis, "B", 0, "L");
                $fpdf->SetTextColor(0, 0, 0);
                $fpdf->Cell(30, 6, $hasil, "B", 0, "L");
                $fpdf->SetTextColor(0, 0, 0);
                $fpdf->Cell(60, 6, $normal, "B", 0, "L");
                $fpdf->Cell(40, 6, $metode, "B", 0, "L");
                $fpdf->Ln($line_break);
            } else {
                $fpdf->SetFont("Times", "B", 8);
                $fpdf->Cell(65, 6, $grup, 0, 0, "L");
                $fpdf->Cell(30, 6, "", 0, 0, "L");
                $fpdf->Cell(60, 6, "", 0, 0, "L");
                $fpdf->Cell(40, 6, "", 0, 0, "L");
                $fpdf->Ln($line_break);
                $current_grup = $grup;
                $fpdf->SetFont("Times", "", 8);
                $fpdf->Cell(2, 6, "", 0, 0, "L");
                $fpdf->Cell(63, 6, $jenis, 0, 0, "L");
                $fpdf->SetTextColor(0, 0, 0);
                $fpdf->Cell(30, 6, $hasil, 0, 0, "L");
                $fpdf->SetTextColor(0, 0, 0);
                $fpdf->Cell(60, 6, $normal, 0, 0, "L");
                $fpdf->Cell(40, 6, $metode, 0, 0, "L");
                $fpdf->Ln($line_break);
            }
        }
    }
    
    $fpdf->Ln(5);
    $fpdf->SetFont("Times", "B", 8);
    $fpdf->Cell(20, 6, "CATATAN:", 0, 0, "L");
    $fpdf->Ln(20);
    
    
    //Layanan Lain-Lain
    if($_POST['layanan_lain_lain'] != '' && $_POST['layanan_lain_lain'] != NULL) {
        $fpdf->Ln($line_break);
        $fpdf->SetFont("Times", "B", 8);
        $fpdf->Cell(20, 6, "LAYANAN LAIN:", 0, 0, "L");
        $fpdf->Ln($line_break);
        $fpdf->Cell(20, 6, "NOMOR", "TB", 0, "C");
        $fpdf->Cell(65, 6, "LAYANAN", "TB", 0, "C");
        $fpdf->Cell(40, 6, "HARGA", "TB", 0, "R");
        $fpdf->Cell(30, 6, "JUMLAH", "TB", 0, "C");
        $fpdf->Cell(40, 6, "SUBTOTAL", "TB", 0, "R");
        $fpdf->Ln($line_break);
        for($i = 0; $i < sizeof($_POST['layanan_lain_lain']); $i++) {
            $layanan_lain = explode(" $ ", $_POST['layanan_lain_lain'][$i]);
            $nomor = $layanan_lain[0];
            $layanan = $layanan_lain[1];
            $harga = $layanan_lain[2];
            $jumlah = $layanan_lain[3];
            $subtotal = $layanan_lain[4];
            $fpdf->SetFont("Times", "", 8);
            if($i+1 == sizeof($_POST['layanan_lain_lain'])) {
                $fpdf->Cell(20, 6, $nomor, "B", 0, "C");
                $fpdf->Cell(65, 6, $layanan, "B", 0, "C");
                $fpdf->Cell(40, 6, $harga, "B", 0, "R");
                $fpdf->Cell(30, 6, $jumlah, "B", 0, "C");
                $fpdf->Cell(40, 6, $subtotal, "B", 0, "R");
            } else {
                $fpdf->Cell(20, 6, $nomor, 0, 0, "C");
                $fpdf->Cell(65, 6, $layanan, 0, 0, "C");
                $fpdf->Cell(40, 6, $harga, 0, 0, "R");
                $fpdf->Cell(30, 6, $jumlah, 0, 0, "C");
                $fpdf->Cell(40, 6, $subtotal, 0, 0, "R");
            }
            $fpdf->Ln($line_break);
        }
        $fpdf->Ln($line_break);
    }
    $fpdf->Ln($line_break);
}

$md5 = md5($username);
$filename = "Hasil Lab RSUK.pdf";
$pathfile = "smis-temp/".$md5.$filename;

$fpdf->Output($pathfile, "F");

echo json_encode($pathfile);
return;

?>