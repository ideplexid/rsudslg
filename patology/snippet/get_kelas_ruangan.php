<?php

global $db;
require_once 'smis-base/smis-include-service-consumer.php';

$serv 			 = new ServiceConsumer($db,"get_kelas_sisa_kamar",NULL,$_POST ['polislug']);
$serv 			 ->execute();
$ctx 			 = $serv ->getContent();
if($ctx!=null){
    $result['slug_kelas'] = $ctx['slug_kelas'];	
}

$pack = new ResponsePackage();
$pack ->setStatus(ResponsePackage::$STATUS_OK);
$pack ->setContent($result);
echo json_encode($pack->getPackage());

?>