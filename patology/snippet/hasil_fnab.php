<?php 
global $user;
require_once "patology/class/MapRuangan.php";
$id = $_POST['id'];
$dbta = new DBTable($db,"smis_pat_pesanan");
$p = $dbta->select($id);
$priksa = json_decode($p->periksa,true);

$pr = new DBTable($db,"smis_pat_layanan");
$pr ->setShowAll(true);
$pr ->addCustomKriteria(" grup "," ='FNAB' ");
$list = $pr ->view("","0");
$data = $list['data'];

$slug_pick = "";
foreach($data as $d){
    if(isset($priksa[$d->slug]) && $priksa[$d->slug]=="1"){
        $slug_pick = $d->slug;
        break;
    }
}
$hasil = json_decode($p->hasil,true);

$has = array(
    "has1"=>"",
    "has2"=>"",
    "has3"=>"",
    "has4"=>"",
    "has5"=>"",
    "has6"=>"",
    "has7"=>"",
    "has8"=>"",
    "has9"=>"",
);
if($slug_pick!=""){
    $has = array(
        "has1"=>$hasil[$slug_pick."_hasil_has1"],
        "has2"=>$hasil[$slug_pick."_hasil_has2"],
        "has3"=>$hasil[$slug_pick."_hasil_has3"],
        "has4"=>$hasil[$slug_pick."_hasil_has4"],
        "has5"=>$hasil[$slug_pick."_hasil_has5"],
        "has6"=>$hasil[$slug_pick."_hasil_has6"],
        "has7"=>$hasil[$slug_pick."_hasil_has7"],
        "has8"=>$hasil[$slug_pick."_hasil_has8"],
        "has9"=>$hasil[$slug_pick."_hasil_has9"],
    );
}


$table = new TablePrint("");
$table->addColumn("<img src='".getLogo()."' style='max-width:100px;' >",1,3);
$table->addColumn("<h4>RSUD SIMPANG LIMA GUMUL</h4>",4,1);
$table->commit("header");
$table->addColumn("Jl. Galuh Candrakirana, Tugurejo, Ngasem, Kediri, East Java 64182",4,1);
$table->commit("header");
$table->addColumn("0354-2891400",5,1);
$table->commit("header");


$table->addColumn("&nbsp;",1,1);
$table->addColumn("&nbsp;",1,1);
$table->addColumn("&nbsp;",1,1);
$table->addColumn("&nbsp;",1,1);
$table->addColumn("&nbsp;",1,1);
$table->addColumn("&nbsp;",1,1);
$table->commit("header");

$table->addColumn("INSTALASI PATOLOGI ANATOMI",6,1,null,null,"b bf c p5");
$table->commit("header");

$table->addColumn("&nbsp;",1,1);
$table->addColumn("&nbsp;",1,1);
$table->addColumn("&nbsp;",1,1);
$table->addColumn("&nbsp;",1,1);
$table->addColumn("&nbsp;",1,1);
$table->addColumn("&nbsp;",1,1);
$table->commit("header");




$table->addColumn("NAMA",1,1);
$table->addColumn(":",1,1);
$table->addColumn($p->nama_pasien,1,1);
$table->addColumn("No. RM",1,1);
$table->addColumn(":",1,1);
$table->addColumn($p->nrm_pasien,1,1);
$table->commit("body");

$table->addColumn("UMUR",1,1);
$table->addColumn(":",1,1);
$table->addColumn($p->umur,1,1);
$table->addColumn("No. PA",1,1);
$table->addColumn(":",1,1);
$table->addColumn($p->no_pat,1,1);
$table->commit("body");

$table->addColumn("JENIS KELAMIN",1,1);
$table->addColumn(":",1,1);
$table->addColumn($p->jk=="0"?"Laki-Laki":"Perempuan",1,1);
$table->addColumn("TANGGAL",1,1);
$table->addColumn(":",1,1);
$table->addColumn(ArrayAdapter::format("date d M Y",$p->tanggal),1,1);
$table->commit("body");



$table->addColumn("ALAMAT",1,1);
$table->addColumn(":",1,1);
$table->addColumn($p->alamat,1,1);
$table->addColumn("DOKTER",1,1);
$table->addColumn(":",1,1);
$table->addColumn($p->nama_dokter,1,1);
$table->commit("body");

$table->addColumn("",1,1);
$table->addColumn("",1,1);
$table->addColumn("",1,1);
$table->addColumn("RUANG",1,1);
$table->addColumn(":",1,1);
$table->addColumn(MapRuangan::getRealName($p->ruangan),1,1);
$table->commit("body");


$table->addColumn("HASIL PEMERIKSAAN FNA",6,1,null,null,"c bb");
$table->commit("body");

$table->addColumn("LOKASI",1,1);
$table->addColumn(":",1,1);
$table->addColumn($has['has1'],4,1);
$table->commit("body");
$table->addColumn("Diagnosa Klinik",1,1);
$table->addColumn("",1,1);
$table->addColumn($has['has2'],4,1);
$table->commit("body");

$table->addColumn("&nbsp;",1,1,null,null,"bb");
$table->addColumn("&nbsp;",1,1,null,null,"bb");
$table->addColumn("&nbsp;",4,1,null,null,"bb");
$table->commit("body");


$table->addColumn("LAPORAN PEMERIKSAAN",1,1,null,null,"bb");
$table->addColumn(":",1,1);
$table->addColumn($has['has3'],4,1);
$table->commit("body");

$table->addColumn("MIKROSKOPI",1,1);
$table->addColumn(":",1,1);
$table->addColumn($has['has4'],4,1);
$table->commit("body");




//$table->addColumn("<h5>JENIS PEMERIKSAAN</h5>",6,1,null,null,"c");
$table->commit("body");

foreach($data as $d){
    if(isset($priksa[$d->slug]) && $priksa[$d->slug]=="1"){
        $table->addColumn(" - ".$d->nama,5,1,"body");
    }
}

//todo jenis pemeriksaan

$table->addColumn("&nbsp;",1,1);
$table->addColumn("&nbsp;",1,1);
$table->addColumn("&nbsp;",1,1);
$table->addColumn("&nbsp;",1,1);
$table->addColumn("&nbsp;",1,1);
$table->addColumn("&nbsp;",1,1);
$table->commit("body");

$table->addColumn("KESIMPULAN",1,1,null,null,"bl bt");
$table->addColumn(":",1,1,null,null,"bt");
$table->addColumn($p->kesimpulan,1,1,null,null,"bt");
$table->addColumn(":",1,1,null,null,"bt");
$table->addColumn("",1,1,null,null,"bt");
$table->addColumn("",1,1,null,null,"bt br");
$table->commit("body");

$table->addColumn("<br><br><br><br><br><br>",1,1,null,null,"bl bb");
$table->addColumn("<br><br><br><br><br><br>",1,1,null,null,"bb");
$table->addColumn("<br><br><br><br><br><br>",1,1,null,null,"bb");
$table->addColumn("<br><br><br><br><br><br>",1,1,null,null,"bb");
$table->addColumn("<br><br><br><br><br><br>",1,1,null,null,"bb");
$table->addColumn("<br><br><br><br><br><br>",1,1,null,null,"bb br");
$table->commit("body");


$table->addColumn("&nbsp;",1,1);
$table->addColumn("&nbsp;",1,1);
$table->addColumn("&nbsp;",1,1);
$table->addColumn("&nbsp;",1,1);
$table->addColumn("PEMERIKSA",2,1,null,null,"c");
$table->commit("body");



$table->addColumn("&nbsp;",1,1);
$table->addColumn("&nbsp;",1,1);
$table->addColumn("&nbsp;",1,1);
$table->addColumn("&nbsp;",1,1);
$table->addColumn("<br><br><br><br>",2,1);
$table->commit("body");


$table->addColumn("&nbsp;",1,1);
$table->addColumn("&nbsp;",1,1);
$table->addColumn("&nbsp;",1,1);
$table->addColumn("&nbsp;",1,1);
$table->addColumn("<u>NAMA PETUGAS<u>",2,1,null,null,"c");
$table->commit("body");

$table->addColumn("&nbsp;",1,1);
$table->addColumn("&nbsp;",1,1);
$table->addColumn("&nbsp;",1,1);
$table->addColumn("&nbsp;",1,1);
$table->addColumn($user->getNameOnly(),2,1,null,null,"c");
$table->commit("body");

$table->getHtml();

require_once 'smis-libs-out/mpdf-7.1.0/vendor/autoload.php';
include 'smis-libs-out/mpdf-7.1.0/src/Mpdf.php';

$pathfile = "smis-temp/FNAB_".$_POST['id'].".pdf";
$mpdfConfig = array(
   'mode' => 'utf-8', 
   'format' => [210,297]  
);

//['mode' => 'utf-8', 'format' => 'A4-P']
$mpdf = new \Mpdf\Mpdf($mpdfConfig);
$print_data = "<html><body>".$table->getHtml()."</body></html>";
$mpdf->AddPageByArray([
   'margin-left' => 2,
   'margin-right' => 2,
   'margin-top' => 10,
   'margin-bottom' => 2,
]);
$css = file_get_contents("patology/resource/css/pat_pdf.css");
$mpdf->WriteHTML("<style>".$css."</style>",0);
$mpdf->WriteHTML($print_data,0);
$mpdf->SetDisplayMode('fullpage');
$mpdf->Output($pathfile, "F");

$res = new ResponsePackage();
$res ->setStatus(ResponsePackage::$STATUS_OK);
$res ->setContent($pathfile);
echo json_encode($res->getPackage());