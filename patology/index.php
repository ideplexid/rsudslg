<?php
	global $db;
	global $user;
	require_once 'smis-libs-class/Policy.php';
	require_once 'smis-libs-inventory/policy.php';
	$inventory_policy = new InventoryPolicy("patology", $user, "modul/");
	
	$policy = new Policy("patology", $user);
	$policy->setDefaultPolicy(Policy::$DEFAULT_POLICY_ALLOW);
	
	$policy->addPolicy("arsip","arsip",Policy::$DEFAULT_COOKIE_CHANGE,"modul/arsip");
	$policy->addPolicy("arsip_admin","arsip_admin",Policy::$DEFAULT_COOKIE_CHANGE,"modul/arsip_admin");
	$policy->addPolicy("jadwal","jadwal",Policy::$DEFAULT_COOKIE_CHANGE,"modul/jadwal");
	
    $policy->addPolicy("laporan","laporan",Policy::$DEFAULT_COOKIE_CHANGE,"modul/laporan");
	$policy->addPolicy("lap_detail_hasil","laporan",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/laporan/lap_detail_hasil");
	$policy->addPolicy("lap_detail_layanan","laporan",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/laporan/lap_detail_layanan");
	$policy->addPolicy("lap_resume_hasil","laporan",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/laporan/lap_resume_hasil");
	$policy->addPolicy("lap_resume_layanan","laporan",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/laporan/lap_resume_layanan");
	$policy->addPolicy("laporan_pend_perlayanan","laporan",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/laporan/laporan_pend_perlayanan");
	$policy->addPolicy("laporan_sensus_harian","laporan",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/laporan/laporan_sensus_harian");
	
    $policy->addPolicy("ruang_pasien_inap","ruang_pasien_inap",Policy::$DEFAULT_COOKIE_CHANGE,"modul/ruang_pasien_inap");
	$policy->addPolicy("layanan","layanan",Policy::$DEFAULT_COOKIE_CHANGE,"modul/layanan");
	$policy->addPolicy("list_hasil","list_hasil",Policy::$DEFAULT_COOKIE_CHANGE,"modul/list_hasil");
	$policy->addPolicy("modal_pesanan","modal_pesanan",Policy::$DEFAULT_COOKIE_CHANGE,"modul/modal_pesanan");
    
    $policy->addPolicy("pemeriksaan","pemeriksaan",Policy::$DEFAULT_COOKIE_CHANGE,"modul/pemeriksaan");
    $policy->addPolicy("rehab_medik","pemeriksaan",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/pemeriksaan/rehab_medik");
    $policy->addPolicy("pelayanan_khusus","pemeriksaan",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/pemeriksaan/pelayanan_khusus");
    $policy->addPolicy("lap_rl52","pemeriksaan",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/pemeriksaan/lap_rl52");

	$policy->addPolicy("show_diagnosa","pemeriksaan",Policy::$DEFAULT_COOKIE_KEEP,"snippet/show_diagnosa");
	$policy->addPolicy("settings","settings",Policy::$DEFAULT_COOKIE_CHANGE,"modul/settings");
    $policy->addPolicy("kode_akun", "settings", Policy::$DEFAULT_COOKIE_KEEP,"snippet/kode_akun");
    
    $policy->addPolicy("get_kelas_ruangan", "pemeriksaan", Policy::$DEFAULT_COOKIE_KEEP,"snippet/get_kelas_ruangan");
    $policy->addPolicy("hasil_pdf", "pemeriksaan", Policy::$DEFAULT_COOKIE_KEEP,"snippet/hasil_pdf");
    $policy->addPolicy("rsuk_hasil_pdf", "pemeriksaan", Policy::$DEFAULT_COOKIE_KEEP,"snippet/rsuk_hasil_pdf");

	$policy->addPolicy("hasil_fnab", "pemeriksaan", Policy::$DEFAULT_COOKIE_KEEP,"snippet/hasil_fnab");
	$policy->addPolicy("hasil_sitologi", "pemeriksaan", Policy::$DEFAULT_COOKIE_KEEP,"snippet/hasil_sitologi");
	$policy->addPolicy("hasil_histologi", "pemeriksaan", Policy::$DEFAULT_COOKIE_KEEP,"snippet/hasil_histologi");

	
	$policy->combinePolicy($inventory_policy);
	$policy->initialize();