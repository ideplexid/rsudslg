<?php
	require_once("smis-base/smis-include-service-consumer.php");
	require_once("depo_farmasi_akhp/library/InventoryLibrary.php");
	require_once("depo_farmasi_akhp/responder/ObatCitoMasukDBResponder.php");
	global $db;

	$id = isset($_POST['id']) ? $_POST['id'] : "";
	$f_id = isset($_POST['f_id']) ? $_POST['f_id'] : "";
	$no_kwitansi = isset($_POST['no_kwitansi']) ? $_POST['no_kwitansi'] : "";
	$tanggal = isset($_POST['tanggal']) ? $_POST['tanggal'] : date("Y-m-d");
	$id_vendor = isset($_POST['id_vendor']) ? $_POST['id_vendor'] : "";
	$nama_vendor = isset($_POST['nama_vendor']) ? $_POST['nama_vendor'] : "";
	$keterangan = isset($_POST['keterangan']) ? $_POST['keterangan'] : "";
	$total = isset($_POST['total']) ? $_POST['total'] : "Rp 0,00";
	$editable_header = isset($_POST['editable_header']) ? $_POST['editable_header'] : "false";
	$editable_detail = isset($_POST['editable_detail']) ? $_POST['editable_detail'] : "false";
	$limited = isset($_POST['limited']) ? $_POST['limited'] : "false";

	$header_form = new Form("", "", "Depo Depo Farmasi AKHP : Penerimaan Obat Cito/Stat");
	$id_hidden = new Hidden("penerimaan_obat_cito_id", "penerimaan_obat_cito_id", $id);
	$header_form->addElement("", $id_hidden);
	$limited_hidden = new Hidden("penerimaan_obat_cito_limited", "penerimaan_obat_cito_limited", $limited);
	$header_form->addElement("", $limited_hidden);
	$f_id_hidden = new Hidden("penerimaan_obat_cito_f_id", "penerimaan_obat_cito_f_id", $f_id);
	$header_form->addElement("", $f_id_hidden);
	$no_kwitansi_text = new Text("penerimaan_obat_cito_no_kwitansi", "penerimaan_obat_cito_no_kwitansi", $no_kwitansi);
	if ($editable_header != "true") {
		$no_kwitansi_text->setAtribute("disabled='disabled'");
	}
	$header_form->addElement("No. Kwitansi", $no_kwitansi_text);
	$id_vendor_hidden = new Hidden("penerimaan_obat_cito_id_vendor", "penerimaan_obat_cito_id_vendor", $id_vendor);
	$header_form->addElement("", $id_vendor_hidden);
	$nama_vendor_text = new Text("penerimaan_obat_cito_nama_vendor", "penerimaan_obat_cito_nama_vendor", $nama_vendor);
	$nama_vendor_text->setAtribute("disabled='disabled'");
	$nama_vendor_text->setClass("smis-one-option-input");
	$vendor_browse_button = new Button("", "", "Pilih");
	$vendor_browse_button->setClass("btn-info");
	$vendor_browse_button->setIsButton(Button::$ICONIC);
	$vendor_browse_button->setIcon("fa fa-list");
	$vendor_browse_button->setAction("vendor.chooser('vendor', 'vendor', 'vendor', vendor)");
	$vendor_browse_button->setAtribute("id='vendor_browse'");
	$vendor_input_group = new InputGroup("");
	$vendor_input_group->addComponent($nama_vendor_text);
	$vendor_input_group->addComponent($vendor_browse_button);
	$header_form->addElement("Rekanan", $vendor_input_group);
	$tanggal_text = new Text("penerimaan_obat_cito_tanggal", "penerimaan_obat_cito_tanggal", $tanggal);
	if ($editable_header == "true") {
		$tanggal_text->setAtribute("data-date-format='yyyy-mm-dd'");
		$tanggal_text->setClass("mydate");
	} else
		$tanggal_text->setAtribute("disabled='disabled'");
	$header_form->addElement("Tanggal", $tanggal_text);
	$keterangan_hidden = new Hidden("penerimaan_obat_cito_keterangan", "penerimaan_obat_cito_keterangan", $keterangan);
	$header_form->addElement("", $keterangan_hidden);

	$detail_form = new Form("", "", "");
	$row_num_hidden = new Hidden("dpenerimaan_obat_cito_row_num", "dpenerimaan_obat_cito_row_num", "");
	$detail_form->addElement("", $row_num_hidden);
	$id_obat_hidden = new Hidden("dpenerimaan_obat_cito_id_obat", "dpenerimaan_obat_cito_id_obat", "");
	$detail_form->addElement("", $id_obat_hidden);
	$name_obat_hidden = new Hidden("dpenerimaan_obat_cito_name_obat", "dpenerimaan_obat_cito_name_obat", "");
	$detail_form->addElement("", $name_obat_hidden);
	$nama_obat_text = new Text("dpenerimaan_obat_cito_nama_obat", "dpenerimaan_obat_cito_nama_obat", "");
	if ($limited == "true") {
		$nama_obat_text->setAtribute("disabled='disabled'");
		$detail_form->addElement("Nama Obat", $nama_obat_text);
	} else {
		$nama_obat_text->setClass("smis-one-option-input");
		$obat_browse_button = new Button("", "", "Pilih");
		$obat_browse_button->setClass("btn-info");
		$obat_browse_button->setIsButton(Button::$ICONIC);
		$obat_browse_button->setIcon("fa fa-list");
		$obat_browse_button->setAction("obat.chooser('obat', 'obat', 'obat', obat)");
		$obat_browse_button->setAtribute("id='obat_browse'");
		$obat_input_group = new InputGroup("");
		$obat_input_group->addComponent($nama_obat_text);
		$obat_input_group->addComponent($obat_browse_button);
		$detail_form->addElement("Nama Obat", $obat_input_group);
	}
	$kode_obat_text = new Text("dpenerimaan_obat_cito_kode_obat", "dpenerimaan_obat_cito_kode_obat", "");
	$kode_obat_text->setAtribute("disabled='disabled'");
	$detail_form->addElement("Kode Obat", $kode_obat_text);
	$nama_jenis_obat_text = new Text("dpenerimaan_obat_cito_nama_jenis_obat", "dpenerimaan_obat_cito_nama_jenis_obat", "");
	$nama_jenis_obat_text->setAtribute("disabled='disabled'");
	$detail_form->addElement("Jenis Obat", $nama_jenis_obat_text);
	$jumlah_text = new Text("dpenerimaan_obat_cito_jumlah", "dpenerimaan_obat_cito_jumlah", "");
	if ($limited == "true")
		$jumlah_text->setAtribute("disabled='disabled'");
	$detail_form->addElement("Jumlah", $jumlah_text);
	$satuan_select = new Select("dpenerimaan_obat_cito_satuan", "dpenerimaan_obat_cito_satuan", "");
	$detail_form->addElement("Satuan", $satuan_select);
	$konversi_text = new Text("dpenerimaan_obat_cito_konversi", "dpenerimaan_obat_cito_konversi", "");
	$detail_form->addElement("Konversi", $konversi_text);
	$satuan_konversi_text = new Text("dpenerimaan_obat_cito_satuan_konversi", "dpenerimaan_obat_cito_satuan_konversi", "");
	$satuan_konversi_text->setAtribute("disabled='disabled'");
	$detail_form->addElement("Sat. Konversi", $satuan_konversi_text);
	$hpp_text = new Text("dpenerimaan_obat_cito_hpp", "dpenerimaan_obat_cito_hpp", "");
	$hpp_text->setTypical("money");
	if ($limited == "true")
		$hpp_text->setAtribute("data-thousands='.' data-decimal=',' data-prefix='Rp. '  data-precision='2' disabled='disabled'");
	else
		$hpp_text->setAtribute("data-thousands='.' data-decimal=',' data-prefix='Rp. '  data-precision='2'");
	$detail_form->addElement("HNA", $hpp_text);
	$hna_text = new Text("dpenerimaan_obat_cito_hna", "dpenerimaan_obat_cito_hna", "");
	$hna_text->setTypical("money");
	if ($limited == "true")
		$hna_text->setAtribute("data-thousands='.' data-decimal=',' data-prefix='Rp. '  data-precision='2' disabled='disabled'");
	else
		$hna_text->setAtribute("data-thousands='.' data-decimal=',' data-prefix='Rp. '  data-precision='2'");
	$detail_form->addElement("HPP", $hna_text);
	$produsen_text = new Text("dpenerimaan_obat_cito_produsen", "dpenerimaan_obat_cito_produsen", "");
	if ($limited == "true")
		$produsen_text->setAtribute("disabled='disabled'");
	$detail_form->addElement("Produsen", $produsen_text);
	$tanggal_exp_text = new Text("dpenerimaan_obat_cito_tanggal_exp", "dpenerimaan_obat_cito_tanggal_exp", "");
	if ($limited == "true")
		$tanggal_exp_text->setAtribute("data-date-format='yyyy-mm-dd' disabled='disabled'");
	else
		$tanggal_exp_text->setAtribute("data-date-format='yyyy-mm-dd'");
	$tanggal_exp_text->setClass("mydate");
	$detail_form->addElement("Tgl. Exp.", $tanggal_exp_text);
	$no_batch_text = new Text("dpenerimaan_obat_cito_no_batch", "dpenerimaan_obat_cito_no_batch", "");
	if ($limited == "true")
		$no_batch_text->setAtribute("disabled='disabled'");
	$detail_form->addElement("No. Batch", $no_batch_text);

	$save_button = new Button("", "", "Tambahkan");
	$save_button->setClass("btn-info");
	$save_button->setIsButton(Button::$ICONIC_TEXT);
	$save_button->setIcon("fa fa-chevron-right");
	$save_button->setAction("dpenerimaan_obat_cito.save()");
	$save_button->setAtribute("id='penerimaan_obat_cito_form_save'");

	$update_button = new Button("", "", "Perbarui");
	$update_button->setClass("btn-info");
	$update_button->setIsButton(Button::$ICONIC_TEXT);
	$update_button->setIcon("fa fa-chevron-right");
	$update_button->setAction("dpenerimaan_obat_cito.save()");
	$update_button->setAtribute("id='penerimaan_obat_cito_form_update'");

	$cancel_button = new Button("", "", "Batal");
	$cancel_button->setClass("btn-danger");
	$cancel_button->setIsButton(Button::$ICONIC_TEXT);
	$cancel_button->setIcon("icon-white icon-remove");
	$cancel_button->setAction("dpenerimaan_obat_cito.cancel()");
	$cancel_button->setAtribute("id='penerimaan_obat_cito_form_cancel'");	

	$button_group = new ButtonGroup("");
	$button_group->addButton($update_button);
	$button_group->addButton($cancel_button);

	if ($limited == "false")
		$detail_form->addElement("", $save_button);
	$detail_form->addElement("", $button_group);

	$table = new Table(
		array("No.", "Kode", "Obat", "Jenis", "Jumlah", "Satuan", "HNA", "Subtotal", "Ubah", "Hapus"),
		"",
		null,
		true
	);
	$table->setName("dpenerimaan_obat_cito");
	$table->setFooterVisible(false);
	$table->setAction(false);
	
	$button_group = new ButtonGroup("");
	$back_button = new Button("", "", "Kembali");
	$back_button->setClass("btn-inverse");
	$back_button->setIsButton(Button::$ICONIC_TEXT);
	$back_button->setIcon("fa fa-chevron-left");
	$back_button->setAction("penerimaan_obat_cito.back()");
	$back_button->setAtribute("id='penerimaan_obat_cito_back'");
	$button_group->addButton($back_button);
	if ($editable_header == "true") {
		if ($limited == "false") {
			$save_button = new Button("", "", "Simpan Draft");
			$save_button->setClass("btn-success");
			$save_button->setIsButton(Button::$ICONIC_TEXT);
			$save_button->setIcon("fa fa-floppy-o");
			$save_button->setAction("penerimaan_obat_cito.save()");
			$save_button->setAtribute("id='penerimaan_obat_cito_save'");
			$button_group->addButton($save_button);
		} else {
			$save_button = new Button("", "", "Update Info. Pembelian Cito");
			$save_button->setClass("btn-success");
			$save_button->setIsButton(Button::$ICONIC_TEXT);
			$save_button->setIcon("fa fa-floppy-o");
			$save_button->setAction("penerimaan_obat_cito.update_info_bbm()");
			$save_button->setAtribute("id='penerimaan_obat_cito_update'");
			$button_group->addButton($save_button);
		}
	}

	//get_daftar_vendor_consumer:
	$vendor_table = new Table(
		array("Nama", "NPWP", "Alamat", "No. Telp."),
		"",
		null,
		true
	);
	$vendor_table->setName("vendor");
	$vendor_table->setModel(Table::$SELECT);
	$vendor_adapter = new SimpleAdapter();
	$vendor_adapter->add("Nama", "nama");
	$vendor_adapter->add("NPWP", "npwp");
	$vendor_adapter->add("Alamat", "alamat");
	$vendor_adapter->add("No. Telp.", "telpon");
	$vendor_service_responder = new ServiceResponder(
		$db,
		$vendor_table,
		$vendor_adapter,
		"get_daftar_vendor"
	);
	
	//get_daftar_barang_reguler_consumer:
	$obat_table = new Table(
		array("ID", "Obat", "Jenis", "Satuan", "Konversi", "Sat. Konversi"),
		"",
		null,
		true
	);
	$obat_table->setName("obat");
	$obat_table->setModel(Table::$SELECT);
	$obat_adapter = new SimpleAdapter();
	$obat_adapter->add("ID", "id");
	$obat_adapter->add("Obat", "nama");
	$obat_adapter->add("Jenis", "nama_jenis_barang");
	$obat_adapter->add("Satuan", "satuan");
	$obat_adapter->add("Konversi", "konversi", "number");
	$obat_adapter->add("Sat. Konversi", "satuan_konversi");
	$obat_service_responder = new ServiceResponder(
		$db,
		$obat_table,
		$obat_adapter,
		"get_daftar_barang_m"
	);
	
	$super_command = new SuperCommand();
	$super_command->addResponder("obat", $obat_service_responder);
	$super_command->addResponder("vendor", $vendor_service_responder);
	$init = $super_command->initialize();
	if ($init != null) {
		echo $init;
		return;
	}

	if (isset($_POST['command'])) {
		if ($_POST['command'] == "show_detail") {
			$id = $_POST['id'];
			$limited = isset($_POST['limited']) ? $_POST['limited'] : "false";
			$dbtable = new DBTable($db, InventoryLibrary::$_TBL_PEMBELIAN_UP);
			$header_row = $dbtable->select($id);
			$detail_rows = $dbtable->get_result("
				SELECT *
				FROM " . InventoryLibrary::$_TBL_DPEMBELIAN_UP . "
				WHERE id_pembelian_up = '" . $id . "' AND prop NOT LIKE 'del'
			");
			$locked = isset($_POST['locked']) ? $_POST['locked'] : false;
			$html = "";
			$num = 0;
			if ($detail_rows != null) {
				foreach ($detail_rows as $row) {
					$html_edit_action = "";
					if ($limited == "true")
						$html_edit_action = "<a href='#' onclick='dpenerimaan_obat_cito.edit(" . $num . ")' class='input btn btn-warning'><i class='fa fa-pencil'></i></a>";
					$html_del_action = "";
					$subtotal = $row->hna / 1.1 * $row->jumlah;
					$hpp = $row->hna / 1.1;
					$f_subtotal = ArrayAdapter::format("money", $subtotal);
					$html .= "
						<tr id='data_" . $num . "'>
							<td id='nomor'></td>
							<td id='id' style='display: none;'>" . $row->id . "</td>
							<td id='id_obat' style='display: none;'>" . $row->id_obat . "</td>
							<td id='kode_obat'><small>" . $row->kode_obat . "</small></td>
							<td id='nama_obat'><small>" . $row->nama_obat . "</small></td>
							<td id='nama_jenis_obat'><small>" . $row->nama_jenis_obat . "</small></td>
							<td id='jumlah' style='display: none;'>" . $row->jumlah . "</td>
							<td id='f_jumlah'><small>" . ArrayAdapter::format("number", $row->jumlah) . "</small></td>
							<td id='satuan'><small>" . $row->satuan . "</small></td>
							<td id='hna' style='display: none;'>" . $row->hna . "</td>
							<td id='hpp' style='display: none;'>" . $hpp . "</td>
							<td id='f_hna'><small>" . ArrayAdapter::format("money", $hpp) . "</small></td>
							<td id='subtotal' style='display: none;'>" . $subtotal . "</td>
							<td id='f_subtotal'><small>" . $f_subtotal . "</small></td>
							<td id='produsen' style='display: none;'>" . $row->produsen . "</td>
							<td id='no_batch' style='display: none;'>" . $row->no_batch . "</td>
							<td id='tanggal_exp' style='display: none;'>" . $row->tanggal_exp . "</td>
							<td>" . $html_edit_action . "</td>
							<td>" . $html_del_action . "</td>
						</tr>";
					$num++;
				}
			}
			$data = array();
			$data['html'] = $html;
			$data['row_num'] = $num;
			echo json_encode($data);
		} else if ($_POST['command'] == "show_footer") {
			$html = "<tfoot>
						<tr>
							<td colspan='7'><div align='right'><small><strong>Total</strong></small></div></td>
							<td id='total_2'></td>
							<td colspan='2'>&nbsp;</td>
						</tr>
						<tr>
							<td colspan='7'><div align='right'><small><strong>PPn</strong></small></div></td>
							<td id='ppn'></td>
							<td colspan='2'>&nbsp;</td>
						</tr>
						<tr>
							<td colspan='7'><div align='right'><small><strong>Jumlah Tagihan</strong></small></div></td>
							<td id='tagihan'></td>
							<td colspan='2'>&nbsp;</td>
						</tr>
					</tfoot>
			";
			$data = array();
			$data['html'] = $html;
			echo json_encode($data);
		} else {
			$adapter = new SimpleAdapter();
			$dbtable = new DBTable($db, InventoryLibrary::$_TBL_PEMBELIAN_UP);
			$dbresponder = new ObatCitoMasukDBResponder(
				$dbtable,
				$table,
				$adapter
			);
			$data = $dbresponder->command($_POST['command']);
			echo json_encode($data);
		}
		return;
	}

	echo $header_form->getHtml();
	echo "<div id='table_content'>" .
			 "<div class='row-fluid'>";
	if ($editable_detail == "true") {
		echo	 "<div class='span4'>" .
					 $detail_form->getHtml() .
				 "</div>" .
				 "<div class='span8'>" .
					 $table->getHtml() .
					 "<div align='right'>" . $button_group->getHtml() . "</div>" .
				 "</div>";
	} else {
		echo	 "<div class='span12'>" .
					 $table->getHtml() .
					 "<div align='right'>" . $button_group->getHtml() . "</div>" .
				 "</div>";
	}
	echo 	 "</div>" .
		 "</div>";
	echo addJS("framework/bootstrap/js/bootstrap-datepicker.js");
	echo addCSS("framework/bootstrap/css/datepicker.css");
	echo addJS("depo_farmasi_akhp/js/penerimaan_obat_cito_vendor_action.js", false);
	echo addJS("depo_farmasi_akhp/js/penerimaan_obat_cito_obat_action.js", false);
	echo addJS("depo_farmasi_akhp/js/dpenerimaan_obat_cito_action.js", false);
	echo addJS("depo_farmasi_akhp/js/penerimaan_obat_cito_action.js", false);
	echo addJS("depo_farmasi_akhp/js/penerimaan_obat_cito_form.js", false);
?>