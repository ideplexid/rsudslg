<?php
	global $PLUGINS;
	
	$init['name'] = "depo_farmasi_akhp";
	$init['path'] = SMIS_DIR . $init['name'] . "/";
	$init['description'] = "Depo Farmasi AKHP";
	$init['require'] = "administrator";
	$init['service'] = "";
	$init['version'] = "2.4.3";
	$init['number'] = "18";
	$init['type'] = "";
	
	$plugin = new Plugin($init);
	$PLUGINS[$init['name']] = $plugin;
?>