<?php
class PenjualanBebasAdapter extends ArrayAdapter {
	public function adapt($row) {
		$array = array();
		$array['id'] = $row->id;
		$array['dibatalkan'] = $row->dibatalkan;
		$array['tanggal'] = $row->tanggal;
		$array['No. Penjualan'] = self::format("digit8", $row->id);
		$array['No. Resep'] = $row->nomor_resep;
		$array['Nama'] = $row->nama_pasien;
		$array['Tanggal/Jam'] = self::format("date d-m-Y H:i", $row->tanggal);
		return $array;
	}
}
?>