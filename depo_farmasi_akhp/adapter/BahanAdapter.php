<?php
class BahanAdapter extends ArrayAdapter {
	public function adapt($row) {
		$array = array();
		$array['id'] = $row->id;
		if (substr($row->kode_obat, 0, 4) == "JKN.")
			$array['Kode'] = "320." . substr($row->kode_obat, 4);
		else if (substr($row->kode_obat, 0, 4) == "REG.")
			$array['Kode'] = "310." . substr($row->kode_obat, 4);
		else
			$array['Kode'] = $row->kode_obat;
		$array['Bahan'] = $row->nama_obat;
		$array['Jenis'] = $row->nama_jenis_obat;
		$array['Stok'] = $row->stok;
		return $array;
	}
}
?>