<?php
	class ReturPenjualanResepAdapter extends ArrayAdapter {
		public function adapt($row) {
			$array = array();
			$array['id'] = $row->id;
			$array['dibatalkan'] = $row->dibatalkan;
			$array['tercetak'] = $row->tercetak;
			$array['tanggal'] = $row->tanggal;
			$array['Nomor'] = self::format("digit8", $row->id);
			$array['Tanggal/Jam'] = self::format("date d-m-Y H:i", $row->tanggal);
			$array['No. Resep'] = $row->nomor_resep;
			if ($row->nrm_pasien != 0)
				$array['NRM'] = self::format("digit8", $row->nrm_pasien);
			else
				$array['NRM'] = "-";
			if ($row->noreg_pasien != 0) {
				$flag_batal_berobat = "";
				if ($row->batal_berobat == 1)
					$flag_batal_berobat = "<span class='label label-important'>Batal Berobat</span>";
				$array['No. Reg.'] = self::format("digit8", $row->noreg_pasien) . $flag_batal_berobat;
			} else
				$array['No. Reg.'] = "-";
			$array['Pasien'] = $row->nama_pasien;
			$array['Dokter'] = $row->nama_dokter;
			if ($row->dibatalkan)
				$array['Status'] = "Dibatalkan";
			else
				$array['Status'] = "-";
			return $array;
		}
	}
?>