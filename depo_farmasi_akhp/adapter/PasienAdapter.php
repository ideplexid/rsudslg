<?php
class PasienAdapter extends ArrayAdapter {
	public function adapt($row) {
		$array = array();
		$array['id'] = $row['id'];
		$array['Tgl. Daftar'] = ArrayAdapter::format("date d-m-Y", $row['tanggal']);
		$array['No. Reg.'] = $row['id'];
		$array['NRM'] = self::format("digit6", $row['nrm']);
		$array['Nama'] = $row['sebutan'] . " " . $row['nama_pasien'];
		$array['Alamat'] = $row['alamat_pasien'];
		$array['Ruangan Terakhir'] = $row['last_nama_ruangan'];
		$array['Jns. Pasien'] = self::format("unslug", $row['carabayar']);
		if ($row['n_perusahaan'] != null && $row['n_perusahaan'] != "")
			$array['Perusahaan'] = $row['n_perusahaan'];
		else
			$array['Perusahaan'] = "-";
		if ($row['nama_asuransi'] != null && $row['nama_asuransi'] != "")
			$array['Asuransi'] = $row['nama_asuransi'];
		else
			$array['Asuransi'] = "-";
		return $array;
	}
}
?>