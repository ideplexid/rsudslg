<?php
class PenjualanResepLuarAdapter extends ArrayAdapter {
	public function adapt($row) {
		$array = array();
		$array['id'] = $row->id;
		$array['edukasi'] = $row->edukasi;
		$array['dibatalkan'] = $row->dibatalkan;
		$array['tipe'] = $row->tipe;
		$array['uri'] = $row->uri;
		$array['tanggal'] = $row->tanggal;
		$array['No. Penjualan'] = self::format("digit8", $row->id);
		$array['No. Resep'] = $row->nomor_resep;
		$array['Tanggal/Jam'] = self::format("date d-m-Y H:i", $row->tanggal);
		$array['Dokter'] = $row->nama_dokter;
		$array['Pasien'] = $row->nama_pasien;
		$array['Alamat'] = $row->alamat_pasien;
		$array['Kat. Pasien'] = ArrayAdapter::format("trivial_0_Rawat Jalan_Rawat Inap", $row->uri);
		$array['Jns. Pasien'] = ArrayAdapter::format("unslug", $row->jenis);
		if ($row->edukasi)
			$array['Diberikan/Edukasi'] = "&#10003;";
		else
			$array['Diberikan/Edukasi'] = "&#10005;";
		return $array;
	}
}
?>