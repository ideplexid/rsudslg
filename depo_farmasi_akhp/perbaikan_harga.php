<?php
	require_once("depo_farmasi_akhp/library/InventoryLibrary.php");
	global $db;
	
	$table = new Table(
		array("No.", "Kode", "Nama", "Jenis", "HPP"),
		"Depo Depo Farmasi AKHP : Koreksi Harga",
		null,
		true
	);
	$table->setName("perbaikan_harga");
	$table->setAddButtonEnable(false);
	$table->setDelButtonEnable(false);
	
	if (isset($_POST['command'])) {
		$adapter = new SimpleAdapter(true, "No.");
		$adapter->add("Kode", "kode_obat");
		$adapter->add("Nama", "nama_obat");
		$adapter->add("Jenis", "nama_jenis_obat");
		$adapter->add("HPP", "hna", "money Rp. ");
		$dbtable = new DBTable($db, InventoryLibrary::$_TBL_STOK_OBAT);
		$dbtable->setOrder(" kode_obat ASC ");
		$dbresponder = new DBResponder(
			$dbtable,
			$table,
			$adapter
		);
		$data = $dbresponder->command($_POST['command']);
		echo json_encode($data);
		return;
	}
	
	$modal = new Modal("perbaikan_harga_add_form", "smis_form_container", "perbaikan_harga");
	$id_hidden = new Hidden("perbaikan_harga_id", "perbaikan_harga_id", "");
	$modal->addElement("", $id_hidden);
	$kode_text = new Text("perbaikan_harga_kode", "perbaikan_harga_kode", "");
	$kode_text->setAtribute("disabled='disabled'");
	$modal->addElement("Kode", $kode_text);
	$nama_text = new Text("perbaikan_harga_nama", "perbaikan_harga_nama", "");
	$nama_text->setAtribute("disabled='disabled'");
	$modal->addElement("Nama", $nama_text);
	$jenis_text = new Text("perbaikan_harga_jenis", "perbaikan_harga_jenis", "");
	$jenis_text->setAtribute("disabled='disabled'");
	$modal->addElement("Jenis", $jenis_text);
	$hpp_lama_text = new Text("perbaikan_harga_hpp_lama", "perbaikan_harga_hpp_lama", "");
	$hpp_lama_text->setTypical("money");
	$hpp_lama_text->setAtribute(" data-thousands=\".\" data-decimal=\",\" data-prefix=\"Rp. \"  data-precision=\"2\"  disabled='disabled'" );
	$modal->addElement("HPP Sekarang", $hpp_lama_text);
	$hpp_baru_text = new Text("perbaikan_harga_hpp_baru", "perbaikan_harga_hpp_baru", "");
	$hpp_baru_text->setTypical("money");
	$hpp_baru_text->setAtribute(" data-thousands=\".\" data-decimal=\",\" data-prefix=\"Rp. \"  data-precision=\"2\" " );
	$modal->addElement("HPP Baru", $hpp_baru_text);
	$save_button = new Button("", "", "Simpan");
	$save_button->setClass("btn-success");
	$save_button->setAction("perbaikan_harga.save()");
	$save_button->setIcon("fa fa-floppy-o");
	$save_button->setIsButton(Button::$ICONIC);
	$modal->addFooter($save_button);
	
	echo $modal->getHtml();
	echo $table->getHtml();
	echo addJS("framework/smis/js/table_action.js");
?>
<script type="text/javascript">
	function PerbaikanHargaAction(name, page, action, column) {
		this.initialize(name, page, action, column);
	}
	PerbaikanHargaAction.prototype.constructor = PerbaikanHargaAction;
	PerbaikanHargaAction.prototype = new TableAction();
	PerbaikanHargaAction.prototype.edit = function(id) {
		var data = this.getEditData(id);
		$.post(
			"",
			data,
			function(response) {
				var json = getContent(response);
				if (json == null) return;
				$("#perbaikan_harga_id").val(json.id);
				$("#perbaikan_harga_kode").val(json.kode_obat);
				$("#perbaikan_harga_nama").val(json.nama_obat);
				$("#perbaikan_harga_jenis").val(json.nama_jenis_obat);
				$("#perbaikan_harga_hpp_lama").val("Rp. " + parseFloat(json.hna).formatMoney("2", ".", ","));
				$("#perbaikan_harga_hpp_baru").val("Rp. " + parseFloat(json.hna).formatMoney("2", ".", ","));
				$("#perbaikan_harga_add_form").smodal("show");
			}
		);
	};
	PerbaikanHargaAction.prototype.save = function() {
		var self = this;
		var data = this.getRegulerData();
		data['command'] = "save";
		data['id'] = $("#perbaikan_harga_id").val();
		data['hna'] = $("#perbaikan_harga_hpp_baru").val().replace(/[^0-9-,]/g, '').replace(",", ".");
		$.post(
			"",
			data,
			function() {
				self.view();
				$("#perbaikan_harga_add_form").smodal("hide");
			}
		);
	};
	
	var perbaikan_harga;
	$(document).ready(function() {
		var perbaikan_harga_columns = new Array("id", "kode_obat", "nama_obat", "nama_jenis_obat", "hpp_lama", "hpp_baru");
		perbaikan_harga = new PerbaikanHargaAction(
			"perbaikan_harga",
			"depo_farmasi_akhp",
			"perbaikan_harga",
			perbaikan_harga_columns
		);
		perbaikan_harga.view();
	});
</script>