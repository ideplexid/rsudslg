<?php
class PasienTable extends Table {
	public function getHtml() {
		$html = parent::getHtml();
		$html .= addJS("depo_farmasi_akhp/js/pasien_search.js", false);
		return $html;
	}
}
?>