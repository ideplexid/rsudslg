<?php
class ReturObatPerPasienTable extends Table {
	public function getBodyContent() {
		$content = "";
		if ($this->content!=NULL) {
			foreach ($this->content as $d) {
				$content .= "<tr>";
				foreach ($this->header as $h) {
					$content .= "<td>" . $d[$h] . "</td>";
				}
				if ($this->is_action) {
					$content .= "<td>" . $this->getFilteredContentButton($d['id'], $d['dibatalkan'])->getHtml() . "</td>";
				}
				$content .= "</tr>";
			}
		}
		return $content;
	}
	public function getFilteredContentButton($id, $dibatalkan) {
		$btn_group = new ButtonGroup("noprint");
		$btn = new Button("", "", "Detail");
		$btn->setAction($this->action . ".detail('" . $id . "')");
		$btn->setClass("btn-danger");
		$btn->setAtribute("data-content='Lihat' data-toggle='popover'");
		$btn->setIcon("icon-eye-open icon-white");
		$btn->setIsButton(Button::$ICONIC);
		$btn_group->addElement($btn);
		if (!$dibatalkan) {
			$btn = new Button("", "", "Cetak Resep");
			$btn->setAction($this->action . ".print('" . $id . "')");
			$btn->setClass("btn-inverse");
			$btn->setAtribute("data-content='Cetak' data-toggle='popover'");
			$btn->setIcon("icon-print icon-white");
			$btn->setIsButton(Button::$ICONIC);
			$btn_group->addElement($btn);
			$btn = new Button("", "", "Batal");
			$btn->setAction($this->action . ".cancel('" . $id . "')");
			$btn->setClass("btn-danger");
			$btn->setAtribute("data-content='Batal' data-toggle='popover'");
			$btn->setIcon("icon-remove icon-white");
			$btn->setIsButton(Button::$ICONIC);
			$btn_group->addElement($btn);
		}
		return $btn_group;
	}
}
?>