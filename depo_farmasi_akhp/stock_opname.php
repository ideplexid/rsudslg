<?php
	require_once("depo_farmasi_akhp/library/InventoryLibrary.php");
	global $db;

	$laporan_form = new Form("", "", "Depo Farmasi AKHP : Stock Opname");
	$tanggal_from_text = new Text("so_tanggal_from", "so_tanggal_from", date("Y-m-") . "01");
	$tanggal_from_text->setClass("mydate");
	$tanggal_from_text->setAtribute("data-date-format='yyyy-mm-dd'");
	$laporan_form->addElement("Waktu Awal", $tanggal_from_text);
	$tanggal_to_text = new Text("so_tanggal_to", "so_tanggal_to", date("Y-m-d"));
	$tanggal_to_text->setClass("mydate");
	$tanggal_to_text->setAtribute("data-date-format='yyyy-mm-dd'");
	$laporan_form->addElement("Waktu Akhir", $tanggal_to_text);
	$jenis_filter_option = new OptionBuilder();
	$jenis_filter_option->add("SEMUA", "semua", "1");
	$jenis_filter_option->add("PER OBAT", "per_obat");
	$jenis_filter_option->add("PER JENIS", "per_jenis");
	$jenis_filter_select = new Select("so_jenis_filter", "so_jenis_filter", $jenis_filter_option->getContent());
	$laporan_form->addElement("Jenis Filter", $jenis_filter_select);
	$kode_jenis_obat_hidden = new Hidden("so_kode_jenis_obat", "so_kode_jenis_obat", "");
	$laporan_form->addElement("", $kode_jenis_obat_hidden);
	$id_obat_hidden = new Hidden("so_id_obat", "so_id_obat", "");
	$laporan_form->addElement("", $id_obat_hidden);
	$nama_obat_text = new Text("so_nama_obat", "so_nama_obat", "");
	$nama_obat_text->setAtribute("disabled='disabled'");
	$nama_obat_text->setClass("smis-one-option-input");
	$browse_button = new Button("", "", "Pilih");
	$browse_button->setClass("btn-info");
	$browse_button->setIcon("fa fa-list");
	$browse_button->setIsButton(Button::$ICONIC);
	$browse_button->setAction("so_obat.chooser('so_obat', 'so_obat_button', 'so_obat', so_obat)");
	$input_group = new InputGroup("");
	$input_group->addComponent($nama_obat_text);
	$input_group->addComponent($browse_button);
	$laporan_form->addElement("Obat", $input_group);
	$nama_jenis_obat_text = new Text("so_nama_jenis_obat", "so_nama_jenis_obat", "");
	$nama_jenis_obat_text->setAtribute("disabled='disabled'");
	$nama_jenis_obat_text->setClass("smis-one-option-input");
	$browse_button = new Button("", "", "Pilih");
	$browse_button->setClass("btn-info");
	$browse_button->setIcon("fa fa-list");
	$browse_button->setIsButton(Button::$ICONIC);
	$browse_button->setAction("so_jenis_obat.chooser('so_jenis_obat', 'so_jenis_obat_button', 'so_jenis_obat', so_jenis_obat)");
	$input_group = new InputGroup("");
	$input_group->addComponent($nama_jenis_obat_text);
	$input_group->addComponent($browse_button);
	$laporan_form->addElement("Jenis Obat", $input_group);
	$urutan_option = new OptionBuilder();
	$urutan_option->addSingle("KODE OBAT", "1");
	$urutan_option->addSingle("NAMA OBAT");
	$urutan_select = new Select("so_urutan", "so_urutan", $urutan_option->getContent());
	$laporan_form->addElement("Urutan", $urutan_select);
	$tanggal_from_hidden = new Hidden("so_proceed_tanggal_from", "so_proceed_tanggal_from", "");
	$laporan_form->addElement("", $tanggal_from_hidden);
	$tanggal_to_hidden = new Hidden("so_proceed_tanggal_to", "so_proceed_tanggal_to", "");
	$laporan_form->addElement("", $tanggal_to_hidden);
	$show_button = new Button("", "", "Tampilkan");
	$show_button->setClass("btn-primary");
	$show_button->setIcon("icon-white icon-repeat");
	$show_button->setIsButton(Button::$ICONIC);
	$show_button->setAction("so.view()");
	$laporan_form->addElement("", $show_button);

	$so_table = new Table(
		array("No.", "Kode Obat", "Nama Obat", "Jenis Obat", "Stok", ""),
		"",
		null,
		true
	);
	$so_table->setName("so");
	$so_table->setAction(false);
	$so_table->setFooterVisible(false);

	//chooser nama obat:
	$obat_table = new Table(
		array("ID", "Kode", "Nama Obat", "Jenis Obat"),
		"",
		null,
		true
	);
	$obat_table->setName("so_obat");
	$obat_table->setModel(Table::$SELECT);
	$obat_adapter = new SimpleAdapter();
	$obat_adapter->add("ID", "id");
	$obat_adapter->add("Kode", "kode");
	$obat_adapter->add("Nama Obat", "nama");
	$obat_adapter->add("Jenis Obat", "nama_jenis_barang");
	$obat_dbtable = new DBTable($db, "smis_pr_barang");
	$obat_dbtable->addCustomKriteria(" medis ", " = 1 ");
	$obat_dbresponder = new DBResponder(
		$obat_dbtable,
		$obat_table,
		$obat_adapter
	);

	//chooser jenis obat:
	$jenis_obat_table = new Table(
		array("No.", "Kode", "Jenis Obat"),
		"",
		null,
		true
	);
	$jenis_obat_table->setName("so_jenis_obat");
	$jenis_obat_table->setModel(Table::$SELECT);
	$jenis_obat_adapter = new SimpleAdapter(true, "No.");
	$jenis_obat_adapter->add("Kode", "kode");
	$jenis_obat_adapter->add("Jenis Obat", "nama");
	$jenis_obat_dbtable = new DBTable($db, "smis_pr_jenis_barang");
	$jenis_obat_dbtable->addCustomKriteria(" medis ", " = 1 ");
	$jenis_obat_dbresponder = new DBResponder(
		$jenis_obat_dbtable,
		$jenis_obat_table,
		$jenis_obat_adapter
	);
	
	$super_command = new SuperCommand();
	$super_command->addResponder("so_obat", $obat_dbresponder);
	$super_command->addResponder("so_jenis_obat", $jenis_obat_dbresponder);
	$init = $super_command->initialize();
	if ($init != null) {
		echo $init;
		return;
	}

	if (isset($_POST['command'])) {
		if ($_POST['command'] == "get_jumlah_obat") {
			$tanggal_from = $_POST['tanggal_from'];
			$tanggal_to = $_POST['tanggal_to'];
			$jenis_filter = $_POST['jenis_filter'];
			$filter_id_obat = "%%";
			if ($jenis_filter == "per_obat")
				$filter_id_obat = $_POST['id_obat'];
			$filter_jenis_obat = "%%";
			if ($jenis_filter == "per_jenis")
				$filter_jenis_obat = $_POST['nama_jenis_obat'];
			$order_by = $_POST['urutan'];
			if ($order_by == "KODE OBAT")
				$order_by = " a.kode_obat ASC ";
			else
				$order_by = " a.nama_obat ASC ";

			$row = $db->get_row("
				SELECT COUNT(*) AS 'jumlah'
				FROM (
					SELECT DISTINCT a.id_obat
					FROM " . InventoryLibrary::$_TBL_STOK_OBAT . " a INNER JOIN " . InventoryLibrary::$_TBL_OBAT_MASUK . " b ON a.id_obat_masuk = b.id
					WHERE b.prop = '' AND b.status = 'sudah' AND a.prop = '' AND  a.id_obat LIKE '" . $filter_id_obat . "' AND a.nama_jenis_obat LIKE '" . $filter_jenis_obat . "'
				) v
			");

			$jumlah = 0;
			if ($row != null)
				$jumlah = $row->jumlah;
			$data = array();
			$data['jumlah'] = $jumlah;
			echo json_encode($data);
		} else if ($_POST['command'] == "get_info_obat") {
			$tanggal_from = $_POST['tanggal_from'];
			$tanggal_to = $_POST['tanggal_to'];
			$jenis_filter = $_POST['jenis_filter'];
			$filter_id_obat = "%%";
			if ($jenis_filter == "per_obat")
				$filter_id_obat = $_POST['id_obat'];
			$filter_jenis_obat = "%%";
			if ($jenis_filter == "per_jenis")
				$filter_jenis_obat = $_POST['nama_jenis_obat'];
			$order_by = $_POST['urutan'];
			if ($order_by == "KODE OBAT")
				$order_by = " a.kode_obat ASC ";
			else
				$order_by = " a.nama_obat ASC ";
			$num = $_POST['num'];

			$row = $db->get_row("
				SELECT *
				FROM (
					SELECT DISTINCT a.id_obat
					FROM " . InventoryLibrary::$_TBL_STOK_OBAT . " a INNER JOIN " . InventoryLibrary::$_TBL_OBAT_MASUK . " b ON a.id_obat_masuk = b.id
					WHERE b.prop = '' AND b.status = 'sudah' AND a.prop = '' AND  a.id_obat LIKE '" . $filter_id_obat . "' AND a.nama_jenis_obat LIKE '" . $filter_jenis_obat . "'
					ORDER BY " . $order_by . "
				) v
				LIMIT " . $num . ", 1
			");

			$profil_obat_row = $db->get_row("
				SELECT a.*
				FROM " . InventoryLibrary::$_TBL_STOK_OBAT . " a INNER JOIN " . InventoryLibrary::$_TBL_OBAT_MASUK . " b ON a.id_obat_masuk = b.id
				WHERE b.prop = '' AND b.status = 'sudah' AND a.prop = '' AND  a.id_obat = '" . $row->id_obat . "'
				LIMIT 0, 1
			");

			$id_obat = $row->id_obat;
			$kode_obat = $profil_obat_row->kode_obat;
			$nama_obat = $profil_obat_row->nama_obat;
			$nama_jenis_obat = $profil_obat_row->nama_jenis_obat;
			$satuan = "";
			$satuan_konversi = "";

			// Mendapatkan Saldo Akhir:
			$saldo_sekarang = InventoryLibrary::getCurrentStock($db, $id_obat, $satuan, 1, $satuan_konversi);
			$jumlah_masuk = InventoryLibrary::getStockIn($db, $id_obat, $satuan, 1, $satuan_konversi, date("Y-m-d", strtotime($tanggal_to . " +1 day")), date("Y-m-d")) + InventoryLibrary::getPenyesuaianStokPositif($db, $id_obat, $satuan, 1, $satuan_konversi, date("Y-m-d", strtotime($tanggal_to . "+1 day")), date("Y-m-d"));
			$jumlah_keluar = InventoryLibrary::getStockOut($db, $id_obat, $satuan, 1, $satuan_konversi,date("Y-m-d", strtotime($tanggal_to . " +1 day")), date("Y-m-d")) + InventoryLibrary::getPenyesuaianStokNegatif($db, $id_obat, $satuan, 1, $satuan_konversi, date("Y-m-d", strtotime($tanggal_to . "+1 day")), date("Y-m-d"));
			$saldo_akhir = $saldo_sekarang - $jumlah_masuk + $jumlah_keluar;
			// Mendapatkan Penyesuaian Stok:
			$penyesuaian_stok = InventoryLibrary::getPenyesuaianStokPositif($db, $id_obat, $satuan, 1, $satuan_konversi, $tanggal_from, $tanggal_to) - InventoryLibrary::getPenyesuaianStokNegatif($db, $id_obat, $satuan, 1, $satuan_konversi, $tanggal_from, $tanggal_to);
			// Mendapatkan Jumlah Masuk:
			$jumlah_masuk = InventoryLibrary::getStockIn($db, $id_obat, $satuan, 1, $satuan_konversi, $tanggal_from, $tanggal_to);
			// Mendapatkan Jumlah Keluar:
			$jumlah_keluar = InventoryLibrary::getStockOut($db, $id_obat, $satuan, 1, $satuan_konversi, $tanggal_from, $tanggal_to);
			// Mendapatkan Saldo Awal:
			$saldo_awal = $saldo_akhir - $jumlah_masuk + $jumlah_keluar - $penyesuaian_stok;
			
			$html = "
				<tr id='data_" . $num . "'>
					<td id='nomor'></td>
					<td id='id_obat' style='display: none;'>" .  $id_obat . "</td>
					<td id='kode_obat'><small>" .  $kode_obat . "</small></td>
					<td id='nama_obat'><small>" .  $nama_obat . "</small></td>
					<td id='nama_jenis_obat'><small>" .  $nama_jenis_obat . "</small></td>
					<td id='stok' style='display: none;'>" .  $saldo_akhir . "</td>
					<td id='f_stok'><small>" .  ArrayAdapter::format("number", $saldo_akhir) . "</small></td>
					<td>" .
						"<div class='btn-group noprint data_action_button' style='display: none;'>" .
							"<a onclick='so.edit(" . $num . ") 'data-content=\"Ubah Stok\" data-toggle=\"popover\" id='' class='input btn btn-warning'>" .
								"<i class='fa fa-pencil'></i>" .
							"</a>" .
						"</div>" .
					"</td>" .
				"</tr>
			";

			$data = array();
			$data['id_obat'] = ArrayAdapter::format("only-digit6", $id_obat);
			$data['kode_obat'] = $kode_obat;
			$data['nama_obat'] = $nama_obat;
			$data['nama_jenis_obat'] = $nama_jenis_obat;
			$data['html'] = $html;
			echo json_encode($data);
		} else if ($_POST['command'] == "save") {
			$id_obat = $_POST['id_obat'];
			$kode_obat = $_POST['kode_obat'];
			$nama_obat = $_POST['nama_obat'];
			$nama_jenis_obat = $_POST['nama_jenis_obat'];
			$tanggal_so = $_POST['tanggal_so'];
			$stok_sebelum = $_POST['stok'];
			$stok_sesudah = $_POST['koreksi_stok'];
			$selisih = $stok_sesudah - $stok_sebelum;

			$stock_opname_dbtable = new DBTable($db, InventoryLibrary::$_TBL_STOCK_OPNAME);
			global $user;
			$stock_opname_data = array(
				"id_obat"			=> $id_obat,
				"kode_obat"			=> $kode_obat,
				"nama_obat"			=> $nama_obat,
				"nama_jenis_obat"	=> $nama_jenis_obat,
				"stok_sebelum"		=> $stok_sebelum,
				"stok_sesudah"		=> $stok_sesudah,
				"operator"			=> $user->getName(),
				"tanggal_so"		=> $tanggal_so,
				"waktu"				=> date("Y-m-d, H:i:s")
			);
			$stock_opname_dbtable->insert($stock_opname_data);

			if ($selisih > 0) {
				$stok_row = $db->get_row("
					SELECT a.*
					FROM " . InventoryLibrary::$_TBL_STOK_OBAT . " a LEFT JOIN " . InventoryLibrary::$_TBL_OBAT_MASUK . " b ON a.id_obat_masuk = b.id
					WHERE a.id_obat = '" . $id_obat . "' AND a.prop = '' AND b.prop = '' AND b.status = 'sudah'
					ORDER BY a.id DESC
					LIMIT 0, 1
				");
				$jumlah_lama = $stok_row->sisa;
				$jumlah_baru = $stok_row->sisa + $selisih;
				$stok_data = array(
					"sisa"	=> $stok_row->sisa + $selisih
				);
				$stok_id = array(
					"id"	=> $stok_row->id
				);
				$stok_obat_dbtable = new DBTable($db, InventoryLibrary::$_TBL_STOK_OBAT);
				$stok_obat_dbtable->update($stok_data, $stok_id);

				$penyesuaian_dbtable = new DBTable($db, InventoryLibrary::$_TBL_PENYESUAIAN_STOK_OBAT);
				$penyesuaian_data = array(
					"id_stok_obat"  => $stok_row->id,
					"tanggal"		=> $tanggal_so,
					"jumlah_lama"	=> $jumlah_lama,
					"jumlah_baru"	=> $jumlah_baru,
					"keterangan"	=> "Stock Opname",
					"nama_user"		=> $user->getName()
				);
				$penyesuaian_dbtable->insert($penyesuaian_data);

				$riwayat_stok_dbtable = new DBTable($db, InventoryLibrary::$_TBL_RIWAYAT_STOK_OBAT);
				$riwayat_data = array(
					"tanggal"		=> $tanggal_so,
					"id_stok_obat"	=> $stok_row->id,
					"jumlah_masuk"	=> $selisih,
					"jumlah_keluar"	=> 0,
					"sisa"			=> ($stok_row->sisa + $selisih),
					"keterangan"	=> "Stock Opname",
					"nama_user"		=> $user->getName()
				);
				$riwayat_stok_dbtable->insert($riwayat_data);

				$sisa_row = $db->get_row("
					SELECT SUM(a.sisa) AS 'sisa'
					FROM " . InventoryLibrary::$_TBL_STOK_OBAT . " a LEFT JOIN " . InventoryLibrary::$_TBL_OBAT_MASUK . " b ON a.id_obat_masuk = b.id
					WHERE a.prop = '' AND a.id_obat = '" . $stok_row->id_obat . "' AND b.prop = '' AND b.status = 'sudah'
				");
				$kartu_stok_dbtable = new DBTable($db, InventoryLibrary::$_TBL_KARTU_STOK_OBAT);
				$kartu_stok_data = array(
					"f_id"				=> 0,
					"no_bon"			=> "-",
					"unit"				=> "Stock Opname",
					"id_obat"			=> $stok_row->id_obat,
					"kode_obat"			=> $stok_row->kode_obat,
					"nama_obat"			=> $stok_row->nama_obat,
					"nama_jenis_obat"	=> $stok_row->nama_jenis_obat,
					"tanggal"			=> $tanggal_so,
					"masuk"				=> $selisih,
					"keluar"			=> 0,
					"sisa"				=> $sisa_row->sisa
				);
				$kartu_stok_dbtable->insert($kartu_stok_data);
			} else if ($selisih < 0) {
				$stok_rows = $db->get_result("
					SELECT a.*
					FROM " . InventoryLibrary::$_TBL_STOK_OBAT . " a LEFT JOIN " . InventoryLibrary::$_TBL_OBAT_MASUK . " b ON a.id_obat_masuk = b.id
					WHERE a.id_obat = '" . $id_obat . "' AND a.prop = '' AND b.prop = '' AND b.status = 'sudah' AND a.sisa > 0
				");
				$info_selisih = $selisih;
				foreach ($stok_rows as $stok_row) {
					$sisa_stok = $stok_row->sisa;
					if ($sisa_stok + $selisih >= 0) {
						$jumlah_lama = $sisa_stok;
						$sisa_stok = $sisa_stok + $selisih;
						$jumlah_baru = $sisa_stok;
						$stok_data = array(
							"sisa"	=> $sisa_stok
						);
						$stok_id = array(
							"id"	=> $stok_row->id
						);
						$stok_obat_dbtable = new DBTable($db, InventoryLibrary::$_TBL_STOK_OBAT);
						$stok_obat_dbtable->update($stok_data, $stok_id);

						$penyesuaian_dbtable = new DBTable($db, InventoryLibrary::$_TBL_PENYESUAIAN_STOK_OBAT);
						$penyesuaian_data = array(
							"id_stok_obat"  => $stok_row->id,
							"tanggal"		=> $tanggal_so,
							"jumlah_lama"	=> $jumlah_lama,
							"jumlah_baru"	=> $jumlah_baru,
							"keterangan"	=> "Stock Opname",
							"nama_user"		=> $user->getName()
						);
						$penyesuaian_dbtable->insert($penyesuaian_data);

						$riwayat_stok_dbtable = new DBTable($db, InventoryLibrary::$_TBL_RIWAYAT_STOK_OBAT);
						$riwayat_data = array(
							"tanggal"		=> $tanggal_so,
							"id_stok_obat"	=> $stok_row->id,
							"jumlah_masuk"	=> 0,
							"jumlah_keluar"	=> abs($selisih),
							"sisa"			=> $sisa_stok,
							"keterangan"	=> "Stock Opname",
							"nama_user"		=> $user->getName()
						);
						$riwayat_stok_dbtable->insert($riwayat_data);
						$selisih = 0;
					} else {
						$jumlah_lama = $sisa_stok;
						$jumlah_baru = 0;
						$jumlah_keluar = $sisa_stok;
						$selisih = $selisih + $sisa_stok;
						$sisa_stok = 0;
						$stok_data = array(
							"sisa"	=> $sisa_stok
						);
						$stok_id = array(
							"id"	=> $stok_row->id
						);
						$stok_obat_dbtable = new DBTable($db, InventoryLibrary::$_TBL_STOK_OBAT);
						$stok_obat_dbtable->update($stok_data, $stok_id);

						$penyesuaian_dbtable = new DBTable($db, InventoryLibrary::$_TBL_PENYESUAIAN_STOK_OBAT);
						$penyesuaian_data = array(
							"id_stok_obat"  => $stok_row->id,
							"tanggal"		=> $tanggal_so,
							"jumlah_lama"	=> $jumlah_lama,
							"jumlah_baru"	=> $jumlah_baru,
							"keterangan"	=> "Stock Opname",
							"nama_user"		=> $user->getName()
						);
						$penyesuaian_dbtable->insert($penyesuaian_data);
						
						$riwayat_stok_dbtable = new DBTable($db, InventoryLibrary::$_TBL_RIWAYAT_STOK_OBAT);
						$riwayat_data = array(
							"tanggal"		=> $tanggal_so,
							"id_stok_obat"	=> $stok_row->id,
							"jumlah_masuk"	=> 0,
							"jumlah_keluar"	=> $jumlah_keluar,
							"sisa"			=> $sisa_stok,
							"keterangan"	=> "Stock Opname",
							"nama_user"		=> $user->getName()
						);
						$riwayat_stok_dbtable->insert($riwayat_data);
					}
					if ($selisih == 0)
						break;
				}
				$sisa_row = $db->get_row("
					SELECT SUM(a.sisa) AS 'sisa'
					FROM " . InventoryLibrary::$_TBL_STOK_OBAT . " a LEFT JOIN " . InventoryLibrary::$_TBL_OBAT_MASUK . " b ON a.id_obat_masuk = b.id
					WHERE a.prop NOT LIKE 'del' AND a.id_obat = '" . $stok_rows[0]->id_obat . "' AND b.prop = '' AND b.status = 'sudah'
				");
				$kartu_stok_dbtable = new DBTable($db, InventoryLibrary::$_TBL_KARTU_STOK_OBAT);
				$kartu_stok_data = array(
					"f_id"				=> 0,
					"no_bon"			=> "-",
					"unit"				=> "Stock Opname",
					"id_obat"			=> $stok_rows[0]->id_obat,
					"kode_obat"			=> $stok_rows[0]->kode_obat,
					"nama_obat"			=> $stok_rows[0]->nama_obat,
					"nama_jenis_obat"	=> $stok_rows[0]->nama_jenis_obat,
					"tanggal"			=> $tanggal_so,
					"masuk"				=> $info_selisih >= 0 ? $info_selisih : 0,
					"keluar"			=> $info_selisih < 0 ? (-1) * $info_selisih : 0,
					"sisa"				=> $sisa_row->sisa
				);
				$kartu_stok_dbtable->insert($kartu_stok_data);
			}

			$data = array(
				'success' => 1,
				'sisa'	  => $stok_sesudah
			);	
			echo json_encode($data);
		}
		return;
	}
	$loading_bar = new LoadingBar("so_loading_bar", "");
	$button = new Button("", "", "Batal");
	$button->addClass("btn-primary");
	$button->setIsButton(Button::$ICONIC_TEXT);
	$button->setIcon("fa fa-close");
	$button->setAction("so.cancel()");
	$loading_modal = new Modal("so_loading_modal", "", "Proses..");
	$loading_modal->addHtml($loading_bar->getHtml(), "after");
	$loading_modal->addFooter($button);

	$edit_stok_modal = new Modal("so_m_add_form", "smis_form_container", "so");
	$edit_stok_modal->setTitle("Stock Opname");
	$id_obat_hidden = new Hidden("so_m_id_obat", "so_m_id_obat", "");
	$edit_stok_modal->addElement("", $id_obat_hidden);
	$kode_obat_text = new Text("so_m_kode_obat", "so_m_kode_obat", "");
	$kode_obat_text->setAtribute("disabled='disabled'");
	$edit_stok_modal->addElement("Kode Obat", $kode_obat_text);
	$nama_obat_text = new Text("so_m_nama_obat", "so_m_nama_obat", "");
	$nama_obat_text->setAtribute("disabled='disabled'");
	$edit_stok_modal->addElement("Nama Obat", $nama_obat_text);
	$nama_jenis_obat_text = new Text("so_m_nama_jenis_obat", "so_m_nama_jenis_obat", "");
	$nama_jenis_obat_text->setAtribute("disabled='disabled'");
	$edit_stok_modal->addElement("Jenis Obat", $nama_jenis_obat_text);
	$tanggal_dari_pilihan_text = new Text("so_m_tanggal_dari", "so_m_tanggal_dari", "");
	$tanggal_dari_pilihan_text->setAtribute("disabled='disabled'");
	$edit_stok_modal->addElement("Dari", $tanggal_dari_pilihan_text);
	$tanggal_sampai_pilihan_text = new Text("so_m_tanggal_sampai", "so_m_tanggal_sampai", "");
	$tanggal_sampai_pilihan_text->setAtribute("disabled='disabled'");
	$edit_stok_modal->addElement("Sampai", $tanggal_sampai_pilihan_text);
	$stok_text = new Text("so_m_stok", "so_m_stok", "");
	$stok_text->setAtribute("disabled='disabled'");
	$edit_stok_modal->addElement("Stok", $stok_text);
	$koreksi_stok_text = new Text("so_m_koreksi_stok", "so_m_koreksi_stok", "");
	$edit_stok_modal->addElement("Koreksi Stok", $koreksi_stok_text);
	$tanggal_so_text = new Text("so_m_tanggal_so", "so_m_tanggal_so", "");
	$tanggal_so_text->setClass("mydate");
	$tanggal_so_text->setAtribute("data-date-format='yyyy-mm-dd'");
	$edit_stok_modal->addElement("Tgl. SO", $tanggal_so_text);
	$save_button = new Button("", "", "Simpan");
	$save_button->setClass("btn-success");
	$save_button->setIcon("fa fa-floppy-o");
	$save_button->setIsButton(Button::$ICONIC);
	$save_button->setAtribute("id='btn_save'");
	$edit_stok_modal->addFooter($save_button);
	
	echo $edit_stok_modal->getHtml();
	echo $loading_modal->getHtml();
	echo $laporan_form->getHtml();
	echo "<div id='table_content'>";
	echo $so_table->getHtml();
	echo "</div>";
	echo "<div id='so_info'></div>";
	echo addJS("framework/smis/js/table_action.js");
	echo addJS("framework/bootstrap/js/bootstrap-datepicker.js");
	echo addJS("base-js/smis-base-loading.js");
	echo addCSS("framework/bootstrap/css/datepicker.css");
	echo addJS("depo_farmasi_akhp/js/stock_opname_action.js", false);
	echo addJS("depo_farmasi_akhp/js/stock_opname_jenis_obat_action.js", false);
	echo addJS("depo_farmasi_akhp/js/stock_opname_obat_action.js", false);
	echo addJS("depo_farmasi_akhp/js/stock_opname.js", false);
?>