<?php 
	global $db;
	require_once("depo_farmasi_akhp/library/InventoryLibrary.php");
	require_once("smis-base/smis-include-service-consumer.php");
	require_once("depo_farmasi_akhp/table/PenjualanResepTable.php");
	require_once("depo_farmasi_akhp/table/PasienTable.php");
	require_once("depo_farmasi_akhp/table/DResepTable.php");
	require_once("depo_farmasi_akhp/table/DObatRacikanTable.php");
	require_once("depo_farmasi_akhp/table/BahanRacikanTable.php");
	require_once("depo_farmasi_akhp/adapter/PenjualanResepAdapter.php");
	require_once("depo_farmasi_akhp/adapter/ObatAdapter.php");
	require_once("depo_farmasi_akhp/adapter/BahanAdapter.php");
	require_once("depo_farmasi_akhp/adapter/PasienAdapter.php");
	require_once("depo_farmasi_akhp/responder/PenjualanResepDBResponder.php");
	require_once("depo_farmasi_akhp/responder/DokterServiceResponder.php");
	require_once("depo_farmasi_akhp/responder/ApotekerServiceResponder.php");
	require_once("depo_farmasi_akhp/responder/ObatDBResponder.php");
	require_once("depo_farmasi_akhp/responder/BahanDBResponder.php");
	require_once("depo_farmasi_akhp/responder/SisaDBResponder.php");
	
	$resep_table = new PenjualanResepTable(
		array("No. Penjualan", "No. Resep", "Kategori", "Tanggal/Jam", "Dokter", "No. Registrasi", "NRM", "Pasien", "Alamat", "Kat. Pasien", "Jns. Pasien", "Perusahaan", "Asuransi", "Diberikan/Edukasi"),
		"Depo Farmasi AKHP : Penjualan Obat - Resep (KIUP)"
	);
	$resep_table->setName("resep");
	
	if (isset($_POST['super_command']) && $_POST['super_command'] == "resep") {
		if (isset($_POST['command'])) {
			if ($_POST['command'] == "get_start_time") {
				$data = array(
					'start_time' 		=> date("Y-m-d H:i:s"),
					'ppn'				=> getSettings($db, "depo_farmasi5-penjualan_resep-ppn_jual", 11)
				);
				echo json_encode($data);
				return;
			}
			if ($_POST['command'] == "get_margin_penjualan") {
				/// mode margin :
				/// 0	=> paten
				/// 1 	=> jenis pasien
				/// 2 	=> obat
				/// 3 	=> obat dan jenis pasien
				$mode_margin = $_POST['mode_margin'];
				$jenis_pasien = $_POST['jenis_pasien'];
				$margin_penjualan = 0;
				if ($mode_margin == 0) {
					$margin_penjualan = getSettings($db, "depo_farmasi5-penjualan_resep-margin_penjualan", 0) / 100;
				} else if ($mode_margin == 1) {
					$dbtable = new DBTable($db, InventoryLibrary::$_TBL_MARGIN_JUAL_JENIS_PASIEN);
					$row = $dbtable->get_row("
						SELECT *
						FROM " . InventoryLibrary::$_TBL_MARGIN_JUAL_JENIS_PASIEN . "
						WHERE prop NOT LIKE 'del' AND slug = '" . $jenis_pasien . "'
					");
					if ($row != null)
						$margin_penjualan = $row->margin_jual / 100;
				}
				$data = array(
					"margin_penjualan" => $margin_penjualan
				);
				echo json_encode($data);
				return;
			} else if ($_POST['command'] == "get_batal_berobat_info") {
				$rows = $db->get_result("
					SELECT noreg_pasien, nrm_pasien, nama_pasien, GROUP_CONCAT(nomor_resep) csv_nomor_resep
					FROM " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . "
					WHERE prop = '' AND dibatalkan = 0 AND batal_berobat = 1 AND tipe = 'resep'
					GROUP BY noreg_pasien
				");
				$html = "";
				$show = 0;
				if ($rows != null) {
					$show = 1;
					$html = "<h4>Pasien Batal Berobat</h4>" .
							"<ul>";
					foreach ($rows as $r) {
						$html .= "<li>";
						$html .= "Pasien <b>" . $r->nama_pasien . " (NRM. " . ArrayAdapter::format("only-digit6", $r->nrm_pasien) . " / No. Reg. " . ArrayAdapter::format("only-digit8", $r->noreg_pasien) . ")</b> telah batal berobat, silakan membatalkan resep nomor : ";
						$nomor_resep_arr = explode(",", $r->csv_nomor_resep);
						if (count($nomor_resep_arr) > 0)
							foreach ($nomor_resep_arr as $nomor_resep)
								$html .= "<span class='badge badge-important'>" . $nomor_resep . "</span> ";	
						$html .= "</li>";
					}
					$html .= "</ul>";
				}
				$data = array(
					"html" 	=> $html,
					"show"	=> $show
				);
				echo json_encode($data);
				return;
			}
			
			$resep_adapter = new PenjualanResepAdapter();
			$resep_dbtable = new DBTable(
				$db,
				InventoryLibrary::$_TBL_PENJUALAN_RESEP
			);
			$resep_dbtable->addCustomKriteria(" tipe ", " ='resep' ");
			$resep_dbtable->setOrder(" id DESC ");			
			$resep_dbresponder = new PenjualanResepDBResponder(
				$resep_dbtable,
				$resep_table,
				$resep_adapter
			);
			if ($resep_dbresponder->isSave()) {
				global $user;
				$resep_dbresponder->addColumnFixValue("operator", $user->getNameOnly());
			}	
			$data = $resep_dbresponder->command($_POST['command']);

			/// Push Data Tagihan ke Kasir Ketika Penambahan Data Baru atau Pembatalan Data Lama :
			if (isset($data['content']['success']) && $data['content']['success'] == 1 && getSettings($db, "cashier-real-time-tagihan", "0") != 0) {
				$mode_data_tagihan = getSettings($db, "depo_farmasi5-service-get_tagihan", "get_simple_tagihan.php") == "get_simple_tagihan.php" ? "simple" : "detail";
				if ($data['content']['type'] == "insert") {
					$id_penjualan_resep = $data['content']['id'];
					$data_tagihan = array(
						'grup_name'		=> "penjualan_resep",
						'entity'		=> "depo_farmasi_akhp"
					);
					$list = array();

					$resep_row = $db->get_row("
						SELECT *
						FROM " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . "
						WHERE id = '" . $id_penjualan_resep . "'
					");
					if ($resep_row != null) {
						$data_tagihan['nama_pasien'] = $resep_row->nama_pasien;
						$data_tagihan['nrm_pasien'] = $resep_row->nrm_pasien;
						$data_tagihan['noreg_pasien'] = $resep_row->noreg_pasien;

						$date_part = explode("-", explode(" ", $resep_row->tanggal)[0]);
						$waktu = $date_part[2] . " " . ArrayAdapter::format("month-id", $date_part[1]) . " " . $date_part[0];

						$total_tagihan_tercatat = $resep_row->total;
						$diskon_tercatat = $resep_row->diskon;
						if ($resep_row->t_diskon == "persen")
							$diskon_tercatat = ($total_tagihan_tercatat / (100 - $diskon_tercatat) * 100) * $diskon_tercatat / 100;

						if ($mode_data_tagihan == "simple") {
							$keterangan = InventoryLibrary::getJSONDetailPenjualanResepKIUP($db, $resep_row->id, $resep_row->nama_dokter);

							$list[] = array(
								'id'				=> $id_penjualan_resep,
								'waktu'				=> $waktu,
								'nama'				=> "Resep " . $resep_row->nomor_resep,
								'start' 			=> $resep_row->tanggal,
								'end' 				=> $resep_row->tanggal,
								'jumlah' 			=> 1,
								'biaya' 			=> $resep_row->total,
								'dokter'			=> $resep_row->nama_dokter,
								'prop'				=> "",
								'grup_name'			=> "penjualan_resep",
								'keterangan'		=> $keterangan,
								'nama_dokter' 		=> $resep_row->nama_dokter,
								'id_dokter'			=> $resep_row->id_dokter,
								'jaspel_dokter'		=> 0,
								'urjigd'			=> "URI",
								'tanggal_tagihan'	=> ArrayAdapter::format("date Y-m-d", $resep_row->tanggal)
							);
						} else if ($mode_data_tagihan == "detail") {
							$detail_tagihan_arr = InventoryLibrary::getDetailPenjualanResepKIUP($db, $waktu, $resep_row->tanggal, $resep_row->id, $total_tagihan_tercatat, $diskon_tercatat, $resep_row->nama_dokter);
							if (count($detail_tagihan_arr) > 0) {
								foreach ($detail_tagihan_arr as $detail_tagihan)
									$list[] = $detail_tagihan;
							}
						}
					}
					$data_tagihan['list'] = $list;

					$consumer_service = new ServiceConsumer(
						$db,
						"proceed_receivable",
						$data_tagihan,
						"kasir"
					);
					$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
					$consumer_service->execute();
				} else if ($_POST['command'] == "save" && $_POST['dibatalkan'] == 1 && isset($_POST['id'])) {
					$id_penjualan_resep = $_POST['id'];
					$data_tagihan = array(
						'grup_name'		=> "penjualan_resep",
						'entity'		=> "depo_farmasi_akhp"
					);
					$list = array();

					$resep_row = $db->get_row("
						SELECT *
						FROM " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . "
						WHERE id = '" . $id_penjualan_resep . "'
					");
					if ($resep_row != null) {
						$data_tagihan['nama_pasien'] = $resep_row->nama_pasien;
						$data_tagihan['nrm_pasien'] = $resep_row->nrm_pasien;
						$data_tagihan['noreg_pasien'] = $resep_row->noreg_pasien;

						$date_part = explode("-", explode(" ", $resep_row->tanggal)[0]);
						$waktu = $date_part[2] . " " . ArrayAdapter::format("month-id", $date_part[1]) . " " . $date_part[0];

						$total_tagihan_tercatat = $resep_row->total;
						$diskon_tercatat = $resep_row->diskon;
						if ($resep_row->t_diskon == "persen")
							$diskon_tercatat = ($total_tagihan_tercatat / (100 - $diskon_tercatat) * 100) * $diskon_tercatat / 100;

						if ($mode_data_tagihan == "simple") {
							$keterangan = "";

							$list[] = array(
								'id'				=> $id_penjualan_resep,
								'waktu'				=> $waktu,
								'nama'				=> "Resep " . $resep_row->nomor_resep,
								'start' 			=> $resep_row->tanggal,
								'end' 				=> $resep_row->tanggal,
								'jumlah' 			=> 1,
								'biaya' 			=> $resep_row->total,
								'dokter'			=> $resep_row->nama_dokter,
								'prop'				=> "del",
								'grup_name'			=> "penjualan_resep",
								'keterangan'		=> $keterangan,
								'nama_dokter' 		=> $resep_row->nama_dokter,
								'id_dokter'			=> $resep_row->id_dokter,
								'jaspel_dokter'		=> 0,
								'urjigd'			=> "URI",
								'tanggal_tagihan'	=> ArrayAdapter::format("date Y-m-d", $resep_row->tanggal)
							);
						} else if ($mode_data_tagihan == "detail") {
							$detail_tagihan_arr = InventoryLibrary::getDetailPenjualanResepKIUP($db, $waktu, $resep_row->tanggal, $resep_row->id, $total_tagihan_tercatat, $diskon_tercatat, $resep_row->nama_dokter, "del");
							if (count($detail_tagihan_arr) > 0) {
								foreach ($detail_tagihan_arr as $detail_tagihan)
									$list[] = $detail_tagihan;
							}
						}
					}
					$data_tagihan['list'] = $list;

					$consumer_service = new ServiceConsumer(
						$db,
						"proceed_receivable",
						$data_tagihan,
						"kasir"
					);
					$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
					$consumer_service->execute();
				}
			}

			echo json_encode($data);
			return;
		}
		return;
	}
	
	//patient service consumer:
	$pasien_table = new PasienTable(
		array("Tgl. Daftar", "No. Reg.", "NRM", "Nama", "Alamat", "Ruangan Terakhir", "Jns. Pasien"),
		"",
		null,
		true
	);
	$pasien_table->setName("pasien");
	$pasien_table->setModel(Table::$SELECT);
	$tanggal_daftar_text = new Text("search_tanggal", "search_tanggal", "");
	$tanggal_daftar_text->setClass("search search-header-tiny search-text");
	$noreg_s_text = new Text("search_noreg", "search_noreg", "");
	$noreg_s_text->setClass("search search-header-tiny search-text");
	$nrm_s_text = new Text("search_nrm", "search_nrm", "");
	$nrm_s_text->addAtribute("autofocus");
	$nrm_s_text->setClass("search search-header-tiny search-text");
	$nama_s_text = new Text("search_nama", "search_nama", "");
	$nama_s_text->setClass("search search-header-med search-text ");
	$alamat_s_text = new Text("search_alamat", "search_alamat", "");
	$alamat_s_text->setClass("search search-header-big search-text");
	$last_ruangan_s_text = new Text("search_last_ruangan", "search_last_ruangan", "");
	$last_ruangan_s_text->setClass("search search-header-med search-text");
	$carabayar_s_text = new Text("search_carabayar", "search_carabayar", "");
	$carabayar_s_text->setClass("search search-header-tiny search-text");
	$header = "<tr class = 'header_pasien'>" .
					"<td>" . $tanggal_daftar_text->getHtml() . "</td>" .
					"<td>" . $noreg_s_text->getHtml() . "</td>" .
					"<td>" . $nrm_s_text->getHtml() . "</td>" .
					"<td>" . $nama_s_text->getHtml() . "</td>" .
					"<td>" . $alamat_s_text->getHtml() . "</td>" .
					"<td>" . $last_ruangan_s_text->getHtml() . "</td>" .
					"<td>" . $carabayar_s_text->getHtml() . "</td>" .
			  "</tr>";
	$pasien_table->addHeader("after", $header);
	$pasien_adapter = new PasienAdapter();
	$pasien_service_responder = new ServiceResponder(
		$db,
		$pasien_table,
		$pasien_adapter,
		"get_registered"
	);
	
	//dokter service consumer:
	$dokter_table = new Table(
		array("Nama", "Jabatan"),
		"",
		null,
		true
	);
	$dokter_table->setName("dokter");
	$dokter_table->setModel(Table::$SELECT);
	$dokter_adapter = new SimpleAdapter();
	$dokter_adapter->add("Nama", "nama");
	$dokter_adapter->add("Jabatan", "nama_jabatan");
	$dokter_service_responder = new DokterServiceResponder(
		$db, 
		$dokter_table, 
		$dokter_adapter,
		"employee"
	);
	
	//get obat chooser:
	$obat_table = new Table(array("Kode", "Obat", "Jenis", "Stok"));
	$obat_table->setName("obat");
	$obat_table->setModel(Table::$SELECT);
	$obat_adapter = new ObatAdapter();
	$obat_dbtable = new DBTable($db, InventoryLibrary::$_TBL_STOK_OBAT);
	$obat_dbtable->setViewForSelect(true);
	$filter = "";
	if (isset($_POST['kriteria'])) {
		$filter = " AND (nama_obat LIKE '%" . $_POST['kriteria'] . "%' OR nama_jenis_obat LIKE '%" . $_POST['kriteria'] . "%')";
	}
	$query_value = "
		SELECT v_stok.*, v_harga.hna, v_harga.markup
		FROM (
			SELECT id_obat AS 'id', kode_obat, nama_obat, nama_jenis_obat, GROUP_CONCAT(CONCAT(sisa, ' ', satuan, ' ', label) ORDER BY satuan, sisa, label ASC SEPARATOR ', ') AS 'stok', satuan, konversi, satuan_konversi
			FROM (
				SELECT id_obat, kode_obat, nama_obat, nama_jenis_obat, SUM(sisa) AS 'sisa', satuan, konversi, satuan_konversi, CASE label WHEN 'reguler' THEN '(R)' WHEN 'sito' THEN '(S)' ELSE '(K)' END AS 'label'
				FROM " . InventoryLibrary::$_TBL_STOK_OBAT . " a LEFT JOIN " . InventoryLibrary::$_TBL_OBAT_MASUK . " b ON a.id_obat_masuk = b.id
				WHERE a.prop NOT LIKE 'del' AND b.prop NOT LIKE 'del' AND b.status = 'sudah' AND a.sisa > 0 " . $filter . "
				GROUP BY id_obat, satuan, konversi, satuan_konversi, label
			) v_obat
			GROUP BY id_obat
		) v_stok LEFT JOIN (
			SELECT id_obat, nama_obat, nama_jenis_obat,  MAX(hna) AS 'hna', 0 AS 'markup', satuan, konversi, satuan_konversi
			FROM " . InventoryLibrary::$_TBL_STOK_OBAT . " a LEFT JOIN " . InventoryLibrary::$_TBL_OBAT_MASUK . " b ON a.id_obat_masuk = b.id
			WHERE a.prop NOT LIKE 'del' AND b.prop NOT LIKE 'del' AND b.status = 'sudah' " . $filter . "
			GROUP BY id_obat, nama_obat, nama_jenis_obat, satuan, konversi, satuan_konversi
		) v_harga ON v_stok.id = v_harga.id_obat AND v_stok.satuan = v_harga.satuan AND v_stok.konversi = v_harga.konversi AND v_stok.satuan_konversi = v_harga.satuan_konversi
	";
	$query_count = "
		SELECT COUNT(*)
		FROM (
			" . $query_value . "
		) v_obat
	";
	$obat_dbtable->setPreferredQuery(true, $query_value, $query_count);
	$obat_dbresponder = new ObatDBResponder(
		$obat_dbtable,
		$obat_table,
		$obat_adapter
	);
	
	//get sisa, hna, dan markup by id obat, satuan, konversi, satuan_konversi:
	$sisa_table = new Table(array("id_obat", "sisa", "satuan", "konversi", "satuan_konversi"));
	$sisa_table->setName("sisa");
	$sisa_adapter = new SimpleAdapter();
	$sisa_adapter->add("id_obat", "id_obat");
	$sisa_adapter->add("sisa", "sisa");
	$sisa_adapter->add("satuan", "satuan");
	$sisa_adapter->add("konversi", "konversi");
	$sisa_adapter->add("satuan_konversi", "satuan_konversi");
	$columns = array("id_obat", "sisa", "satuan", "konversi", "satuan_konversi");
	$sisa_dbtable = new DBTable(
		$db,
		InventoryLibrary::$_TBL_STOK_OBAT,
		$columns
	);
	$sisa_dbresponder = new SisaDBResponder(
		$sisa_dbtable,
		$sisa_table,
		$sisa_adapter
	);
	
	//apoteker service consumer:
	$apoteker_table = new Table(array("NIP", "Nama", "Jabatan"));
	$apoteker_table->setName("apoteker");
	$apoteker_table->setModel(Table::$SELECT);
	$apoteker_adapter = new SimpleAdapter();
	$apoteker_adapter->add("NIP", "nip");
	$apoteker_adapter->add("Nama", "nama");
	$apoteker_adapter->add("Jabatan", "nama_jabatan");
	$apoteker_service_responder = new ApotekerServiceResponder(
		$db,
		$apoteker_table,
		$apoteker_adapter,
		"employee"
	);
	
	//bahan chooser:
	$bahan_table = new Table(array("Kode", "Bahan", "Jenis", "Stok"));
	$bahan_table->setName("bahan");
	$bahan_table->setModel(Table::$SELECT);
	$bahan_adapter = new BahanAdapter();
	$bahan_dbtable = new DBTable($db, InventoryLibrary::$_TBL_STOK_OBAT);
	$bahan_dbtable->setViewForSelect(true);
	$filter = "";
	if (isset($_POST['kriteria'])) {
		$filter = " AND (nama_obat LIKE '%" . $_POST['kriteria'] . "%' OR nama_jenis_obat LIKE '%" . $_POST['kriteria'] . "%')";
	}
	$query_value = "
		SELECT v_stok.*, v_harga.hna, v_harga.markup
		FROM (
			SELECT id_obat AS 'id', kode_obat, nama_obat, nama_jenis_obat, GROUP_CONCAT(CONCAT(sisa, ' ', satuan, ' ', label) ORDER BY satuan, sisa, label ASC SEPARATOR ', ') AS 'stok', satuan, konversi, satuan_konversi
			FROM (
				SELECT id_obat, kode_obat, nama_obat, nama_jenis_obat, SUM(sisa) AS 'sisa', satuan, konversi, satuan_konversi, CASE label WHEN 'reguler' THEN '(R)' WHEN 'sito' THEN '(S)' ELSE '(K)' END AS 'label'
				FROM " . InventoryLibrary::$_TBL_STOK_OBAT . " a LEFT JOIN " . InventoryLibrary::$_TBL_OBAT_MASUK . " b ON a.id_obat_masuk = b.id
				WHERE a.prop NOT LIKE 'del' AND b.prop NOT LIKE 'del' AND b.status = 'sudah' AND a.sisa > 0 " . $filter . "
				GROUP BY id_obat, satuan, konversi, satuan_konversi, label
			) v_obat
			GROUP BY id_obat
		) v_stok LEFT JOIN (
			SELECT id_obat, nama_obat, nama_jenis_obat,  MAX(hna) AS 'hna', 0 AS 'markup', satuan, konversi, satuan_konversi
			FROM " . InventoryLibrary::$_TBL_STOK_OBAT . " a LEFT JOIN " . InventoryLibrary::$_TBL_OBAT_MASUK . " b ON a.id_obat_masuk = b.id
			WHERE a.prop NOT LIKE 'del' AND b.prop NOT LIKE 'del' AND b.status = 'sudah' " . $filter . "
			GROUP BY id_obat, nama_obat, nama_jenis_obat, satuan, konversi, satuan_konversi
		) v_harga ON v_stok.id = v_harga.id_obat AND v_stok.satuan = v_harga.satuan AND v_stok.konversi = v_harga.konversi AND v_stok.satuan_konversi = v_harga.satuan_konversi
	";
	$query_count = "
		SELECT COUNT(*)
		FROM (
			" . $query_value . "
		) v_obat
	";
	$bahan_dbtable->setPreferredQuery(true, $query_value, $query_count);
	$bahan_dbresponder = new BahanDBResponder(
		$bahan_dbtable,
		$bahan_table,
		$bahan_adapter
	);
	
	$super_command = new SuperCommand();
	$super_command->addResponder("pasien", $pasien_service_responder);
	$super_command->addResponder("dokter", $dokter_service_responder);
	$super_command->addResponder("obat", $obat_dbresponder);
	$super_command->addResponder("sisa", $sisa_dbresponder);
	$super_command->addResponder("apoteker", $apoteker_service_responder);
	$super_command->addResponder("bahan", $bahan_dbresponder);
	$init = $super_command->initialize();
	if ($init != null) {
		echo $init;
		return;
	}
	
	$resep_modal = new Modal("resep_add_form", "smis_form_container", "resep");
	$resep_modal->setTitle("Data Penjualan Resep");
	$resep_modal->setClass(Modal::$FULL_MODEL);
	$id_hidden = new Hidden("resep_id", "resep_id", "");
	$resep_modal->addElement("", $id_hidden);
	$start_time_hidden = new Hidden("resep_start_time", "resep_start_time", "");
	$resep_modal->addElement("", $start_time_hidden);
	$ppn_hidden = new Hidden("resep_ppn", "resep_ppn", getSettings($db, "depo_farmasi5-penjualan_resep-ppn_jual", 11));
	$resep_modal->addElement("", $ppn_hidden);
	$noresep_text = new Text("resep_nomor", "resep_nomor", "");
	if (getSettings($db, "depo_farmasi5-penjualan_resep-nomor_resep-otomatis", 0) == 0) {
		$noresep_text->addAtribute("autofocus");
	} else {
		$noresep_text->addAtribute("readonly='true'");
	}
	$resep_modal->addElement("Nomor", $noresep_text);
	$pasien_button = new Button("", "", "Pilih");
	$pasien_button->setClass("btn-info");
	$pasien_button->setIsButton(Button::$ICONIC);
	$pasien_button->setIcon("icon-white ".Button::$icon_list_alt);
	$pasien_button->setAction("pasien.chooser('pasien', 'pasien_button', 'pasien', pasien)");
	$pasien_button->setAtribute("id='pasien_browse'");
	$pasien_text = new Text("resep_nama_pasien", "resep_nama_pasien", "");
	$pasien_text->setAtribute("disabled='disabled'");
	$pasien_text->setClass("smis-one-option-input");
	$pasien_input_group = new InputGroup("");
	$pasien_input_group->addComponent($pasien_text);
	$pasien_input_group->addComponent($pasien_button);
	$resep_modal->addElement("Pasien", $pasien_input_group);
	$noreg_text = new Text("resep_noreg_pasien", "resep_noreg_pasien", "");
	$noreg_text->setAtribute("disabled='disabled'");
	$resep_modal->addElement("No. Registrasi", $noreg_text);
	$nrm_text = new Text("resep_nrm_pasien", "resep_nrm_pasien", "");
	$nrm_text->setAtribute("disabled='disabled'");
	$resep_modal->addElement("NRM Pasien", $nrm_text);
	$alamat_text = new Text("resep_alamat_pasien", "resep_alamat_pasien", "");
	$alamat_text->setAtribute("disabled='disabled'");
	$resep_modal->addElement("Alamat Pasien", $alamat_text);
	if (getSettings($db, "depo_farmasi5-resep-change_jenis_pasien", 0) == 0) {
		$jenis_text = new Text("resep_jenis", "resep_jenis", "");
		$jenis_text->setAtribute("disabled='disabled'");
		$resep_modal->addElement("Jns. Pasien", $jenis_text);
	} else {
		$jenis_pasien_service_consumer = new ServiceConsumer($db, "get_jenis_patient", null, "registration");
		$jenis_pasien_service_consumer->execute ();
		$jenis_pasien_option = $jenis_pasien_service_consumer->getContent();
		$jenis_pasien_select = new Select("resep_jenis", "resep_jenis", $jenis_pasien_option);
		$resep_modal->addElement("Jns. Pasien", $jenis_pasien_select);
	}
	$perusahaan_hidden = new Hidden("resep_perusahaan", "resep_perusahaan", "");
	$resep_modal->addElement("", $perusahaan_hidden);
	$asuransi_hidden = new Hidden("resep_asuransi", "resep_asuransi", "");
	$resep_modal->addElement("", $asuransi_hidden);
	if (getSettings($db, "depo_farmasi5-penjualan_resep-use_ruangan", 0) == 1) {
		$ruangan_service_consumer = new ServiceConsumer(
			$db, 
			"get_urjip",
			array()
		);
		$ruangan_service_consumer->setMode(ServiceConsumer::$MULTIPLE_MODE);
		$content = $ruangan_service_consumer->execute()->getContent();
		$ruangan_option = new OptionBuilder();
		$num = 0;
		foreach ($content as $autonomous=>$ruang){
			foreach ($ruang as $nama_ruang => $jip){
				if ($jip[$nama_ruang] == "URJ" || $jip[$nama_ruang] == "UP" || $jip[$nama_ruang] == "URJI"){
					if ($num == 0)
						$ruangan_option->add(ArrayAdapter::format("unslug", $nama_ruang), $nama_ruang, "1");
					else
						$ruangan_option->add(ArrayAdapter::format("unslug", $nama_ruang), $nama_ruang);
					$num++;
				}
			}
		
		}
		$ruangan_option->add("-", "");
		$ruangan_select = new Select("resep_ruangan", "resep_ruangan", $ruangan_option->getContent());
		$resep_modal->addElement("Ruangan", $ruangan_select);
	} 
	if (getSettings($db, "depo_farmasi5-resep-show_uri", 0) == 0) {
		$uri_hidden = new Hidden("resep_uri", "resep_uri", "");
		$resep_modal->addElement("", $uri_hidden);
	} else {
		$uri_option = new OptionBuilder();
		$uri_option->add("Resep Rawat Jalan", "0", "1");
		$uri_option->add("Resep Rawat Inap", "1");
		$uri_select = new Select("resep_uri", "resep_uri", $uri_option->getContent());
		$resep_modal->addElement("Jns. Resep", $uri_select);
	}
	$id_dokter_hidden = new Hidden("resep_id_dokter", "resep_id_dokter", "");
	$resep_modal->addElement("", $id_dokter_hidden);
	$nama_dokter_hidden = new Hidden("resep_name_dokter", "resep_name_dokter", "");
	$resep_modal->addElement("", $nama_dokter_hidden);
	$dokter_button = new Button("", "", "Pilih");
	$dokter_button->setClass("btn-info");
	$dokter_button->setIsButton(Button::$ICONIC);
	$dokter_button->setIcon("icon-white ".Button::$icon_list_alt);
	$dokter_button->setAction("dokter.chooser('dokter', 'dokter_button', 'dokter', dokter)");
	$dokter_button->setAtribute("id='dokter_browse'");
	$dokter_text = new Text("resep_nama_dokter", "resep_nama_dokter", "");
	$dokter_text->setClass("smis-one-option-input");
	$dokter_input_group = new InputGroup("");
	$dokter_input_group->addComponent($dokter_text);
	$dokter_input_group->addComponent($dokter_button);
	$resep_modal->addElement("Dokter", $dokter_input_group);
	$markup_hidden = new Hidden("resep_markup", "resep_markup", "0");
	$resep_modal->addElement("", $markup_hidden);
	$diskon_text = new Text("resep_diskon", "resep_diskon", "0,00");
	$diskon_text->setTypical("money");
	$diskon_text->setAtribute(" data-thousands=\".\" data-decimal=\",\"  data-precision=\"2\"  " );
	$resep_modal->addElement("Diskon", $diskon_text);
	$t_diskon_option = new OptionBuilder();
	$t_diskon_option->add("Persen (%)", "persen", "1");
	$t_diskon_option->add("Nominal (Rp)", "nominal");
	$t_diskon_option->add("Gratis (100 %)", "gratis");
	$t_diskon_select = new Select("resep_t_diskon", "resep_t_diskon", $t_diskon_option->getContent());
	$resep_modal->addElement("Tipe Diskon", $t_diskon_select);
	$kategori_option = new OptionBuilder();
	$kategori_option->add("", "", "1");
	$kategori_option->add("UMUM", "umum");
	$kategori_option->add("INA CBGS", "ina_cbgs");
	$kategori_option->add("INA CBGS NON TAGIHAN", "ina_cbgs_non_tagihan");
	$kategori_option->add("KRONIS", "kronis");
	$kategori_option->add("KEMOTERAPI", "kemoterapi");
	$kategori_select = new Select("resep_kategori", "resep_kategori", $kategori_option->getContent());
	$resep_modal->addElement("Kategori", $kategori_select);
	$total_text = new Text("resep_total", "resep_total", "");
	$total_text->setTypical("money");
	$total_text->setAtribute(" data-thousands=\".\" data-decimal=\",\" data-prefix=\"Rp. \"  data-precision=\"2\"  disabled='disabled'" );
	$resep_modal->addElement("Total", $total_text);
	$dresep_table = new DResepTable(array("No.", "Nama", "Jumlah", "Harga", "Embalase", "Tuslah", "Biaya Racik", "Subtotal", "Aturan Pakai", "Apoteker"));
	$dresep_table->setName("dresep");
	$dresep_table->setFooterVisible(false);
	$resep_modal->addBody("dresep_table", $dresep_table);
	$resep_button = new Button("", "", "Simpan");
	$resep_button->setClass("btn-success");
	$resep_button->setIcon("fa fa-floppy-o");
	$resep_button->setIsButton(Button::$ICONIC);
	$resep_button->setAtribute("id='resep_save'");
	$resep_modal->addFooter($resep_button);
	$resep_button = new Button("", "", "OK");
	$resep_button->setClass("btn-success");
	$resep_button->setAtribute("id='resep_ok'");
	$resep_button->setAction("$($(this).data('target')).smodal('hide')");
	$resep_modal->addFooter($resep_button);
	$tombol='<a href="#" class="input btn btn-info" ><i class="icon-white icon-list-alt"></i></a>';
	$resep_modal->addHTML("
		<div class='alert alert-block alert-inverse' id='help_resep'>
			<h4>Tips</h4>
			<ul>
				<li>Tombol <kbd>Tab</kbd> : Berpindah Cepat ke Isian Berikutnya (dari Kiri ke Kanan)</li>
				<li>Ketik Min. <strong>3 Karakter</strong> / Tombol " . $tombol . " pada Isian Dokter / Tombol <kbd>F8</kbd> : Menentukan Nama Dokter</li>
				<li>Tombol " . $tombol . " pada Isian Pasien / Tombol <kbd>F7</kbd> : Menentukan Nama Pasien</li>
				<li>Tombol <kbd>&uarr;</kbd> / <kbd>&darr;</kbd> : Memilih <strong>Persen (%) / Nominal (Rp) / Gratis (100 %)</strong> pada Isian <strong>Tipe Diskon</strong></li>
				<li>Tombol <kbd>F3</kbd> : Tombol Cepat Menambahkan Obat Jadi Baru</li>
				<li>Tombol <kbd>F4</kbd> : Tombol Cepat Menambahkan Obat Racikan Baru</li>
				<li>Tombol <kbd>F6</kbd> : Tombol Cepat Menyimpan Data Resep, Pastikan Semua Obat/Racikan dan Informasi Kepala Resep Sudah Lengkap.</li>
			<ul>
		</div>
	", "after");
				
	//obat jadi modal:
	$obat_jadi_modal = new Modal("obat_jadi_add_form", "smis_form_container", "obat_jadi");
	$obat_jadi_modal->setTitle("Data Obat Jadi");
	$id_hidden = new Hidden("obat_jadi_id", "obat_jadi_id", "");
	$obat_jadi_modal->addElement("", $id_hidden);
	$id_obat_hidden = new Hidden("obat_jadi_id_obat", "obat_jadi_id_obat", "");
	$obat_jadi_modal->addElement("", $id_obat_hidden);
	$obat_button = new Button("", "", "Pilih");
	$obat_button->setClass("btn-info");
	$obat_button->setAction("obat.chooser('obat', 'obat_button', 'obat', obat)");
	$obat_button->setIcon("icon-white icon-list-alt");
	$obat_button->setIsButton(Button::$ICONIC);
	$obat_button->setAtribute("id='obat_jadi_browse'");
	$nama_obat_text = new Text("obat_jadi_nama_obat", "obat_jadi_nama_obat", "");
	$nama_obat_text->setClass("smis-one-option-input");
	$nama_obat_text->addAtribute("autofocus");
	$nama_obat_input_group = new InputGroup("");
	$nama_obat_input_group->addComponent($nama_obat_text);
	$nama_obat_input_group->addComponent($obat_button);
	$obat_jadi_modal->addElement("Obat", $nama_obat_input_group);
	$kode_obat_hidden = new Hidden("obat_jadi_kode_obat", "obat_jadi_kode_obat", "");
	$obat_jadi_modal->addElement("", $kode_obat_hidden);
	$name_obat_hidden = new Hidden("obat_jadi_name_obat", "obat_jadi_name_obat", "");
	$obat_jadi_modal->addElement("", $name_obat_hidden);
	$nama_jenis_obat_hidden = new Hidden("obat_jadi_nama_jenis_obat", "obat_jadi_nama_jenis_obat", "");
	$obat_jadi_modal->addElement("", $nama_jenis_obat_hidden);
	$satuan_select = new Select("obat_jadi_satuan", "obat_jadi_satuan", "");
	$obat_jadi_modal->addElement("Satuan", $satuan_select);
	$konversi_hidden = new Hidden("obat_jadi_konversi", "obat_jadi_konversi", "");
	$konversi_hidden->setAtribute("disabled='disabled'");
	$obat_jadi_modal->addElement("", $konversi_hidden);
	$satuan_konversi_hidden = new Hidden("obat_jadi_satuan_konversi", "obat_jadi_satuan_konversi", "");
	$obat_jadi_modal->addElement("", $satuan_konversi_hidden);
	$stok_hidden = new Hidden("obat_jadi_stok", "obat_jadi_stok", "");
	$obat_jadi_modal->addElement("", $stok_hidden);
	$f_stok_text = new Text("obat_jadi_f_stok", "obat_jadi_f_stok", "");
	$f_stok_text->setAtribute("disabled='disabled'");
	$obat_jadi_modal->addElement("Stok", $f_stok_text);
	$jumlah_lama_hidden = new Hidden("obat_jadi_jumlah_lama", "obat_jadi_jumlah_lama", "");
	$obat_jadi_modal->addElement("", $jumlah_lama_hidden);
	$hna_text = new Text("obat_jadi_hna", "obat_jadi_hna", "");
	$hna_text->setTypical("money");
	$hna_text->setAtribute(" data-thousands=\".\" data-decimal=\",\" data-prefix=\"Rp. \"  data-precision=\"2\"  disabled='disabled'" );
	$obat_jadi_modal->addElement("HJA", $hna_text);
	if (getSettings($db, "depo_farmasi5-penjualan_resep-embalase_show-obat_jadi", 0) == 1) {
		$embalase_text = new Text("obat_jadi_embalase", "obat_jadi_embalase", "");
		$embalase_text->setTypical("money");
		$embalase_text->setAtribute(" data-thousands=\".\" data-decimal=\",\" data-prefix=\"Rp. \"  data-precision=\"2\"" );
		$obat_jadi_modal->addElement("Embalase", $embalase_text);
	} else {
		$embalase_hidden = new Hidden("obat_jadi_embalase", "obat_jadi_embalase", "");
		$obat_jadi_modal->addElement("", $embalase_hidden);
	}
	if (getSettings($db, "depo_farmasi5-penjualan_resep-tuslah_show-obat_jadi", 0) == 1) {
		$tuslah_text = new Text("obat_jadi_tuslah", "obat_jadi_tuslah", "");
		$tuslah_text->setTypical("money");
		$tuslah_text->setAtribute(" data-thousands=\".\" data-decimal=\",\" data-prefix=\"Rp. \"  data-precision=\"2\"" );
		$obat_jadi_modal->addElement("Tusla", $tuslah_text);
	} else {
		$tuslah_hidden = new Hidden("obat_jadi_tuslah", "obat_jadi_tuslah", "");
		$obat_jadi_modal->addElement("", $tuslah_hidden);
	}
	$markup_hidden = new Hidden("obat_jadi_markup", "obat_jadi_markup", "");
	$obat_jadi_modal->addElement("", $markup_hidden);
	$jumlah_text = new Text("obat_jadi_jumlah", "obat_jadi_jumlah", "");
	$obat_jadi_modal->addElement("Jumlah", $jumlah_text);
	$aturan_pakai_text = new Text("obat_jadi_aturan_pakai", "obat_jadi_aturan_pakai", "");
	$obat_jadi_modal->addElement("Aturan Pakai", $aturan_pakai_text);
	$obat_jadi_button = new Button("", "", "Simpan");
	$obat_jadi_button->setClass("btn-success");
	$obat_jadi_button->setAtribute("id='obat_jadi_save'");
	$obat_jadi_button->setIcon("fa fa-floppy-o");
	$obat_jadi_button->setIsButton(Button::$ICONIC);
	$obat_jadi_modal->addFooter($obat_jadi_button);
	$obat_jadi_modal->addHTML("
		<div class='alert alert-block alert-inverse'>
			<h4>Tips</h4>
			<ul>
				<li><small>Ketik Min. <strong>3 Karakter</strong> / Tombol " . $tombol . " / Tombol <kbd>F2</kbd> : Menentukan Nama Obat</small></li>
				<li>Tombol <kbd>Esc</kbd> : Tombol Cepat Kembali ke Formulir Resep</li>
				<li>Tombol <kbd>F6</kbd> : Tombol Menyimpan Obat Jadi</li>
			</ul>
		</div>
	", "before");
	
	//obat racikan modal:
	$obat_racikan_modal = new Modal("obat_racikan_add_form", "smis_form_container", "obat_racikan");
	$obat_racikan_modal->setTitle("Data Obat Racikan");
	$obat_racikan_modal->setClass(Modal::$FULL_MODEL);
	$id_hidden = new Hidden("obat_racikan_id", "obat_racikan_id", "");
	$obat_racikan_modal->addElement("", $id_hidden);
	$nama_text = new Text("obat_racikan_nama", "obat_racikan_nama", "");
	$nama_text->addAtribute("autofocus");
	$obat_racikan_modal->addElement("Nama Racikan", $nama_text);
	$id_apoteker_hidden = new Hidden("obat_racikan_id_apoteker", "obat_racikan_id_apoteker", "");
	$obat_racikan_modal->addElement("", $id_apoteker_hidden);
	$nama_apoteker_hidden = new Hidden("obat_racikan_name_apoteker", "obat_racikan_name_apoteker", "");
	$obat_racikan_modal->addElement("", $nama_apoteker_hidden);
	$apoteker_button = new Button("", "", "Pilih");
	$apoteker_button->setClass("btn-info");
	$apoteker_button->setIsButton(Button::$ICONIC);
	$apoteker_button->setIcon("icon-white ".Button::$icon_list_alt);
	$apoteker_button->setAction("apoteker.chooser('apoteker', 'apoteker_button', 'apoteker', apoteker)");
	$apoteker_button->setAtribute("id='apoteker_browse'");
	$apoteker_text = new Text("obat_racikan_nama_apoteker", "obat_racikan_nama_apoteker", "");
	$apoteker_text->setClass("smis-one-option-input");
	$apoteker_input_group = new InputGroup("");
	$apoteker_input_group->addComponent($apoteker_text);
	$apoteker_input_group->addComponent($apoteker_button);
	$obat_racikan_modal->addElement("Apoteker", $apoteker_input_group);
	$aturan_pakai_text = new Text("obat_racikan_aturan_pakai", "obat_racikan_aturan_pakai", "");
	$obat_racikan_modal->addElement("Aturan Pakai", $aturan_pakai_text);
	$biaya_racik_text = new Text("obat_racikan_biaya_racik", "obat_racikan_biaya_racik", "");
	$biaya_racik_text->setTypical("money");
	if (getSettings($db, "depo_farmasi5-penjualan_resep-jasa_racik_edit", 0) == 0)
		$biaya_racik_text->setAtribute(" data-thousands=\".\" data-decimal=\",\" data-prefix=\"Rp. \"  data-precision=\"2\" disabled='disabled'" );
	else
		$biaya_racik_text->setAtribute(" data-thousands=\".\" data-decimal=\",\" data-prefix=\"Rp. \"  data-precision=\"2\"" );
	$obat_racikan_modal->addElement("Biaya Racik", $biaya_racik_text);
	$dracikan_table = new DObatRacikanTable(
		array("Bahan", "Harga", "Jumlah", "Embalase", "Tuslah", "Subtotal")
	);
	$dracikan_table->setName("dracikan");
	$dracikan_table->setFooterVisible(false);
	$obat_racikan_modal->addBody("dracikan_table", $dracikan_table);
	$obat_racikan_button = new Button("", "", "Simpan");
	$obat_racikan_button->setClass("btn-success");
	$obat_racikan_button->setIcon("fa fa-floppy-o");
	$obat_racikan_button->setIsButton(Button::$ICONIC);
	$obat_racikan_button->setAtribute("id='racikan_save'");
	$obat_racikan_modal->addFooter($obat_racikan_button);
	$obat_racikan_button = new Button("", "", "OK");
	$obat_racikan_button->setClass("btn-success");
	$obat_racikan_button->setAtribute("id='racikan_ok'");
	$obat_racikan_button->setAction("$($(this).data('target')).smodal('hide')");
	$obat_racikan_modal->addFooter($obat_racikan_button);
	$obat_racikan_modal->addHTML("
		<div class='alert alert-block alert-inverse' id='help_racikan'>
			<h4>Tips</h4>
			<ul>
				<li>Ketik Min. <strong>3 Karakter</strong> / Tombol " . $tombol . " / Tombol <kbd>F7</kbd> : Menentukan Nama Apoteker</li>
				<li>Tombol <kbd>F2</kbd> : Tombol Cepat Menambahkan Bahan Baru dari Obat Racikan</li>
				<li>Tombol <kbd>F6</kbd> : Tombol Cepat Menyimpan Obat Racikan</li>
				<li>Tombol <kbd>Esc</kbd> : Tombol Cepat Kembali ke Formulir Resep</li>
			</ul>
		</div>
	", "after");
	
	//bahan modal:
	$bahan_modal = new Modal("bahan_add_form", "smis_form_container", "bahan");
	$bahan_modal->setTitle("Data Bahan");
	$id_hidden = new Hidden("bahan_id", "bahan_id", "");
	$bahan_modal->addElement("", $id_hidden);
	$id_bahan_hidden = new Hidden("bahan_id_bahan", "bahan_id_bahan", "");
	$bahan_modal->addElement("", $id_bahan_hidden);
	$bahan_button = new Button("", "", "Pilih");
	$bahan_button->setClass("btn-info");
	$bahan_button->setAction("bahan.chooser('bahan', 'bahan_button', 'bahan', bahan)");
	$bahan_button->setIcon("icon-white icon-list-alt");
	$bahan_button->setIsButton(Button::$ICONIC);
	$bahan_button->setAtribute("id='bahan_browse'");
	$nama_bahan_text = new Text("bahan_nama_bahan", "bahan_nama_bahan", "");
	$nama_bahan_text->addAtribute("autofocus");
	$nama_bahan_text->setClass("smis-one-option-input");
	$nama_bahan_input_group = new InputGroup("");
	$nama_bahan_input_group->addComponent($nama_bahan_text);
	$nama_bahan_input_group->addComponent($bahan_button);
	$bahan_modal->addElement("Bahan", $nama_bahan_input_group);
	$kode_bahan_hidden = new Hidden("bahan_kode_bahan", "bahan_kode_bahan", "");
	$bahan_modal->addElement("", $kode_bahan_hidden);
	$name_bahan_hidden = new Hidden("bahan_name_bahan", "bahan_name_bahan", "");
	$bahan_modal->addElement("", $name_bahan_hidden);
	$nama_jenis_bahan_hidden = new Hidden("bahan_nama_jenis_bahan", "bahan_nama_jenis_bahan", "");
	$bahan_modal->addElement("", $nama_jenis_bahan_hidden);
	$satuan_select = new Select("bahan_satuan", "bahan_satuan", "");
	$bahan_modal->addElement("Satuan", $satuan_select);
	$konversi_hidden = new Hidden("bahan_konversi", "bahan_konversi", "");
	$konversi_hidden->setAtribute("disabled='disabled'");
	$bahan_modal->addElement("", $konversi_hidden);
	$satuan_konversi_hidden = new Hidden("bahan_satuan_konversi", "bahan_satuan_konversi", "");
	$bahan_modal->addElement("", $satuan_konversi_hidden);
	$stok_hidden = new Hidden("bahan_stok", "bahan_stok", "");
	$bahan_modal->addElement("", $stok_hidden);
	$f_stok_text = new Text("bahan_f_stok", "bahan_f_stok", "");
	$f_stok_text->setAtribute("disabled='disabled'");
	$bahan_modal->addElement("Stok", $f_stok_text);
	$jumlah_lama_hidden = new Hidden("bahan_jumlah_lama", "bahan_jumlah_lama", "");
	$bahan_modal->addElement("", $jumlah_lama_hidden);
	$hna_text = new Text("bahan_hna", "bahan_hna", "");
	$hna_text->setTypical("money");
	$hna_text->setAtribute(" data-thousands=\".\" data-decimal=\",\" data-prefix=\"Rp. \"  data-precision=\"2\"  disabled='disabled'" );
	$bahan_modal->addElement("HJA", $hna_text);
	$markup_hidden = new Hidden("bahan_markup", "bahan_markup", "");
	$bahan_modal->addElement("", $markup_hidden);
	if (getSettings($db, "depo_farmasi5-penjualan_resep-embalase_show-bahan_racikan", 0) == 1) {
		$embalase_text = new Text("bahan_embalase", "bahan_embalase", "");
		$embalase_text->setTypical("money");
		$embalase_text->setAtribute(" data-thousands=\".\" data-decimal=\",\" data-prefix=\"Rp. \"  data-precision=\"2\"" );
		$bahan_modal->addElement("Embalase", $embalase_text);
	} else {
		$embalase_hidden = new Hidden("bahan_embalase", "bahan_embalase", "");
		$bahan_modal->addElement("", $embalase_hidden);
	}
	if (getSettings($db, "depo_farmasi5-penjualan_resep-tuslah_show-bahan_racikan", 0) == 1) {
		$tuslah_text = new Text("bahan_tuslah", "bahan_tuslah", "");
		$tuslah_text->setTypical("money");
		$tuslah_text->setAtribute(" data-thousands=\".\" data-decimal=\",\" data-prefix=\"Rp. \"  data-precision=\"2\"" );
		$bahan_modal->addElement("Tusla", $tuslah_text);
	} else {
		$tuslah_hidden = new Hidden("bahan_tuslah", "bahan_tuslah", "");
		$bahan_modal->addElement("", $tuslah_hidden);
	}
	$jumlah_text = new Text("bahan_jumlah", "bahan_jumlah", "");
	$bahan_modal->addElement("Jumlah", $jumlah_text);
	$bahan_button = new Button("", "", "Simpan");
	$bahan_button->setClass("btn-success");
	$bahan_button->setIcon("fa fa-floppy-o");
	$bahan_button->setIsButton(Button::$ICONIC);
	$bahan_button->setAtribute("id='bahan_save'");
	$bahan_modal->addFooter($bahan_button);
	$bahan_modal->addHTML("
		<div class='alert alert-block alert-inverse'>
			<h4>Tips</h4>
			<ul>
				<li><small>Ketik Min. <strong>3 Karakter</strong> / Tombol " . $tombol . " / Tombol <kbd>F2</kbd> : Menentukan Nama Bahan</small></li>
				<li>Tombol <kbd>Esc</kbd> : Tombol Cepat Kembali ke Formulir Obat Racikan</li>
				<li>Tombol <kbd>F6</kbd> : Tombol Menyimpan Bahan Racikan</li>
			</ul>
		</div>
	","before");
	
	//invi-table bahan racikan:
	$bahan_racikan_table = new BahanRacikanTable(array("ID", "IDO", "Nama", "Jumlah", "Satuan", "Konversi", "Satuan Konversi", "Harga", "Embalase", "Tuslah", "Label"));
	$bahan_racikan_table->setAction(false);
	$bahan_racikan_table->setName("bahan_racikan");
	
	echo "<div class='alert alert-block alert-inverse'>" .
			 "<h4>Tips</h4>" .
			 "Tombol <kbd>F2</kbd> : Menampilkan Formulir Penjualan Resep Baru" .
		 "</div>";
	echo "<div class='alert alert-block alert-danger' id='warning_batal_berobat' style='display: none'></div>";
	echo $bahan_modal->getHtml();
	echo $obat_racikan_modal->getHtml();
	echo $obat_jadi_modal->getHtml();
	echo $resep_modal->getHtml();
	echo $resep_table->getHtml();
	echo $bahan_racikan_table->getHtml();
	
	echo addCSS("depo_farmasi_akhp/css/penjualan_resep.css", false);
	echo addJS("smis-base-js/smis-base-shortcut.js", false);
	echo addJS("depo_farmasi_akhp/js/penjualan_resep_action.js", false);
	echo addJS("depo_farmasi_akhp/js/dpenjualan_resep_action.js", false);
	echo addJS("depo_farmasi_akhp/js/obat_action.js", false);
	echo addJS("depo_farmasi_akhp/js/apoteker_action.js", false);
	echo addJS("depo_farmasi_akhp/js/dokter_action.js", false);
	echo addJS("depo_farmasi_akhp/js/pasien_action.js", false);
	echo addJS("depo_farmasi_akhp/js/bahan_action.js", false);
	echo addJS("smis-libs-out/webprint/webprint.js", false);
	echo addJS("framework/smis/js/table_action.js");
?>
<script type="text/javascript">
	var embalase_obat_jadi = <?php echo getSettings($db, "depo_farmasi5-penjualan_resep-embalase-obat_jadi", 0); ?>;
	var tuslah_obat_jadi = <?php echo getSettings($db, "depo_farmasi5-penjualan_resep-tuslah-obat_jadi", 0); ?>;
	var embalase_bahan_racikan = <?php echo getSettings($db, "depo_farmasi5-penjualan_resep-embalase-bahan_racikan", 0); ?>;
	var tuslah_bahan_racikan = <?php echo getSettings($db, "depo_farmasi5-penjualan_resep-tuslah-bahan_racikan", 0); ?>;
	var jasa_racik = <?php echo getSettings($db, "depo_farmasi5-penjualan_resep-jasa_racik", 0); ?>;

	/// mode margin :
	/// 0	=> paten
	/// 1 	=> jenis pasien
	/// 2 	=> obat
	/// 3 	=> obat dan jenis pasien
	var mode_margin = <?php echo getSettings($db, "depo_farmasi5-penjualan_resep-mode_margin_penjualan", 0); ?>;
	var need_margin_penjualan_request = 1;

	var dresep_num;
	var racikan_num;
	var dracikan_num;
	var bahan_num;
	var resep;
	var dokter;
	var pasien;
	var dresep;
	var obat;
	var apoteker;
	var bahan;
	$(document).ready(function() {
		$('.modal').on('shown.bs.modal', function() {
			$(this).find('[autofocus]').focus();
		});
		$('[data-toggle="popover"]').popover({
			trigger: 'hover',
			'placement': 'top'
		});
		$("*").dblclick(function(e) {
			e.preventDefault();
		});
		$("#smis-chooser-modal").on("show", function() {
			if ($("#smis-chooser-modal .modal-header h3").text() == "DOKTER" || 
				$("#smis-chooser-modal .modal-header h3").text() == "PASIEN") {
				$("#smis-chooser-modal").removeClass("half_model");
				$("#smis-chooser-modal").removeClass("full_model");
				$("#smis-chooser-modal").addClass("full_model");
				if ($("#smis-chooser-modal .modal-header h3").text() == "PASIEN") {
					$("table#table_pasien tfoot tr").eq(0).hide();
				} else {
					$("table#table_pasien tfoot tr").eq(0).show();
				}
			} else if ($("#smis-chooser-modal .modal-header h3").text() == "OBAT" || 
					   $("#smis-chooser-modal .modal-header h3").text() == "BAHAN") {
				$("#smis-chooser-modal").removeClass("half_model");
				$("#smis-chooser-modal").removeClass("full_model");
				$("#smis-chooser-modal").addClass("half_model");
			} else {
				$("#smis-chooser-modal").removeClass("half_model");
				$("#smis-chooser-modal").removeClass("full_model");
			}
		});
		$("#smis-chooser-modal").on("hidden", function() {
			$("#smis-chooser-modal").removeClass("half_model");
		});
		$("#resep_add_form").on("show", function() {
			$("ul.typeahead").hide();
		});
		$("#obat_racikan_add_form").on("show", function() {
			$("ul.typeahead").hide();
		});
		$("#resep_t_diskon").on("change", function() {
			var diskon = $("#resep_diskon").val().replace(/[^0-9-,]/g, '').replace(",", ".");
			var t_diskon = $("#resep_t_diskon").val();
			if (diskon > 100 && t_diskon == "persen") {
				$("#modal_alert_resep_add_form").html(
					"<div class='alert alert-block alert-danger'>" +
						"<h4>Peringatan</h4>" +
						"<br /><b>Diskon</b> tidak boleh melebihi 100%" +
					"</div>"
				);
				return;
			}
			$("#modal_alert_resep_add_form").html("");
			if ($("#resep_t_diskon").val() == "gratis") {
				$("#resep_diskon").val("100,00");
				$("#resep_diskon").removeAttr("disabled");
				$("#resep_diskon").attr("disabled", "disabled");
			} else {
				$("#resep_diskon").val("0,00");
				$("#resep_diskon").removeAttr("disabled");
			}
			resep.refreshBiayaTotal();
		});
		$("#resep_diskon").on("keyup", function() {
			var diskon = $("#resep_diskon").val().replace(/[^0-9-,]/g, '').replace(",", ".");
			var t_diskon = $("#resep_t_diskon").val();
			if (diskon > 100 && t_diskon == "persen") {
				$("#modal_alert_resep_add_form").html(
					"<div class='alert alert-block alert-danger'>" +
						"<h4>Peringatan</h4>" +
						"<br /><b>Diskon</b> tidak boleh melebihi 100%" +
					"</div>"
				);
				return;
			} 
			$("#modal_alert_resep_add_form").html("");
			resep.refreshBiayaTotal();
		});
		$("#resep_diskon").on("change", function() {
			var diskon = $("#resep_diskon").val().replace(/[^0-9-,]/g, '').replace(",", ".");
			if (diskon == "") {
				$("#resep_diskon").val("0,00");
			}
		});
		dokter = new DokterAction(
			"dokter",
			"depo_farmasi_akhp",
			"penjualan_resep",
			new Array()
		);
		dokter.setSuperCommand("dokter");
		pasien = new PasienAction(
			"pasien",
			"depo_farmasi_akhp",
			"penjualan_resep",
			new Array()
		);
		pasien.setSuperCommand("pasien");
		obat = new ObatAction(
			"obat",
			"depo_farmasi_akhp",
			"penjualan_resep",
			new Array()
		);
		obat.setSuperCommand("obat");
		apoteker = new ApotekerAction(
			"apoteker",
			"depo_farmasi_akhp",
			"penjualan_resep",
			new Array()
		);
		apoteker.setSuperCommand("apoteker");
		bahan = new BahanAction(
			"bahan",
			"depo_farmasi_akhp",
			"penjualan_resep",
			new Array("embalase", "tuslah")
		);
		bahan.setSuperCommand("bahan");
		var dresep_columns = new Array("id", "id_penjualan_resep", "nama", "embalase", "tuslah", "hja", "subtotal");		
		dresep = new DResepAction(
			"dresep",
			"depo_farmasi_akhp",
			"penjualan_resep",
			dresep_columns
		);
		var resep_columns = new Array("id", "nomor_resep", "tanggal", "id_dokter", "nama_dokter", "noreg_pasien", "nrm_pasien", "nama_pasien", "alamat_pasien", "jenis", "perusahaan", "asuransi", "markup", "uri", "total", "edukasi", "dibatalkan", "embalase", "diskon", "t_diskon", "kategori", "batal_berobat");
		resep = new ResepAction(
			"resep",
			"depo_farmasi_akhp",
			"penjualan_resep",
			resep_columns
		);
		resep.setSuperCommand("resep");
		resep.view();
		var obat_jadi = new TableAction(
			"obat_jadi",
			"depo_farmasi_akhp",
			"penjualan_resep",
			new Array("tuslah", "embalase")
		);
		var obat_racikan = new TableAction(
			"obat_racikan",
			"depo_farmasi_akhp",
			"penjualan_resep",
			new Array("embalase", "tuslah", "biaya_racik")
		);
		var data_apoteker = apoteker.getViewData();
		$('#obat_racikan_nama_apoteker').typeahead({
			minLength:3,
			source: function (query, process) {
				data_apoteker["kriteria"]=$('#obat_racikan_nama_apoteker').val();
				var $items = new Array;
				$items = [""];	
				$.ajax({
					url: '',
					type: 'POST',
					data: data_apoteker,
					success: function(res) {
						var json = getContent(res);
						var the_data_proses = json.d.data;
						$items = [""];	
						$.map(the_data_proses, function(data) {
							var group;
							group = {
								id: data.id,
								name: data.nama,                            
								toString: function () {
									return JSON.stringify(this);
								},
								toLowerCase: function () {
									return this.name.toLowerCase();
								},
								indexOf: function (string) {
									return String.prototype.indexOf.apply(this.name, arguments);
								},
								replace: function (string) {
									var value = '';
									value +=  this.name;
									if(typeof(this.level) != 'undefined') {
										value += ' <span class="pull-right muted">';
										value += this.level;
										value += '</span>';
									}
									return String.prototype.replace.apply('<div class="typeaheadiv">' + value + '</div>', arguments);
								}
							};
							$items.push(group);
						});
						process($items);
					}
				});
			},
			updater: function (item) {
				var item = JSON.parse(item);
				$("#obat_racikan_id_apoteker").val(item.id);
				$("#obat_racikan_name_apoteker").val(item.name);
				return item.name;
			}
		});
		var data_dokter = dokter.getViewData();	
		$('#resep_nama_dokter').typeahead({
			minLength:3,
			source: function (query, process) {
			data_dokter['kriteria']=$('#resep_nama_dokter').val();
			var $items = new Array;
			$items = [""];                
			$.ajax({
				url: '',
				type: 'POST',
				data: data_dokter,
				success: function(res) {
					var json=getContent(res);
					var the_data_proses=json.d.data;
					$items = [""];      				
					$.map(the_data_proses, function(data) {
						var group;
						group = {
							id: data.id,
							name: data.nama,                            
							toString: function () {
								return JSON.stringify(this);
							},
							toLowerCase: function () {
								return this.name.toLowerCase();
							},
							indexOf: function (string) {
								return String.prototype.indexOf.apply(this.name, arguments);
							},
							replace: function (string) {
								var value = '';
								value +=  this.name;
								if(typeof(this.level) != 'undefined') {
									value += ' <span class="pull-right muted">';
									value += this.level;
									value += '</span>';
								}
								return String.prototype.replace.apply('<div class="typeaheadiv">' + value + '</div>', arguments);
							}
						};
						$items.push(group);
					});
					process($items);
				}
				});
			},
			updater: function (item) {
				var item = JSON.parse(item);
				$("#resep_nama_pasien").focus();
				$("#resep_id_dokter").val(item.id);
				$("#resep_name_dokter").val(item.name);
				return item.name;
			}
		});
		var bahan_nama_bahan = bahan.getViewData();
		$('#bahan_nama_bahan').typeahead({
			minLength:3,
			source: function (query, process) {
				var $items = new Array;
				$items = [""];                
				bahan_nama_bahan['kriteria']=$('#bahan_nama_bahan').val();
				$.ajax({
					url: '',
					type: 'POST',
					data: bahan_nama_bahan,
					success: function(res) {
						var json=getContent(res);
						var the_data_proses=json.dbtable.data;
						$items = [""];      				
						$.map(the_data_proses, function(data){
							var group;
							group = {
								id: data.id,
								name: data.nama_obat,
								kode: data.kode_obat,                       
								toString: function () {
									return JSON.stringify(this);
								},
								toLowerCase: function () {
									return this.name.toLowerCase();
								},
								indexOf: function (string) {
									return String.prototype.indexOf.apply(this.name, arguments);
								},
								replace: function (string) {
									var value = '';
									value +=  this.kode + " - " + this.name;
									if(typeof(this.level) != 'undefined') {
										value += ' <span class="pull-right muted">';
										value += this.level;
										value += '</span>';
									}
									return String.prototype.replace.apply('<div class="typeaheadiv">' + value + '</div>', arguments);
								}
							};
							$items.push(group);
						});
						process($items);
					}
				});
			},
			updater: function (item) {
				var item = JSON.parse(item);  
				bahan.select(item.id);
				$("#bahan_jumlah").focus();       
				return item.name;
			}
		});
		var obat_jadi_nama = obat.getViewData();
		$('#obat_jadi_nama_obat').typeahead({
			minLength:3,
			source: function (query, process) {
				var $items = new Array;
				$items = [""];                
				obat_jadi_nama['kriteria']=$('#obat_jadi_nama_obat').val();
				$.ajax({
					url: '',
					type: 'POST',
					data: obat_jadi_nama,
					success: function(res) {
						var json=getContent(res);
						var the_data_proses=json.dbtable.data;
						$items = [""];      				
						$.map(the_data_proses, function(data){
							var group;
							group = {
								id: data.id,
								name: data.nama_obat,
								kode: data.kode_obat,                   
								toString: function () {
									return JSON.stringify(this);
								},
								toLowerCase: function () {
									return this.name.toLowerCase();
								},
								indexOf: function (string) {
									return String.prototype.indexOf.apply(this.name, arguments);
								},
								replace: function (string) {
									var value = '';
									value +=  this.kode + " - " + this.name;
									if(typeof(this.level) != 'undefined') {
										value += ' <span class="pull-right muted">';
										value += this.level;
										value += '</span>';
									}
									return String.prototype.replace.apply('<div class="typeaheadiv">' + value + '</div>', arguments);
								}
							};
							$items.push(group);
						});
						process($items);
					}
				});
			},
			updater: function (item) {
				var item = JSON.parse(item);  
				obat.select(item.id);  
				$("#obat_jadi_jumlah").focus();       
				return item.name;
			}
		});
		$("#bahan_jumlah").keypress(function(e) {
			$("ul.typeahead").html("");
			$("ul.typeahead").hide();
			if(e.which == 13) {
				$('#bahan_save').trigger('click');
			}
		});
		$("#resep_nomor").keypress(function(e) {
			if(e.which == 13) {
				$('#pasien_browse').focus();
			}
		});
		$("#resep_nama_dokter").keypress(function(e) {
			$("ul.typeahead").html("");
			$("ul.typeahead").hide();
			if(e.which == 13) {
				$('#resep_diskon').focus();
			}
		});
		$("#resep_diskon").keypress(function(e) {
			if(e.which == 13) {
				$('#resep_t_diskon').focus();
			}
		});
		$("#obat_jadi_nama_obat").keypress(function(e) {
			if(e.which == 13) {
				$('#obat_jadi_satuan').focus();
			}
		});
		$("#obat_jadi_satuan").keypress(function(e) {
			e.preventDefault();
			if(e.which == 13) {
				$('#obat_jadi_jumlah').focus();
			}
		});
		$("#obat_racikan_nama").keypress(function(e) {
			if(e.which == 13) {
				$('#obat_racikan_nama_apoteker').focus();
			}
		});
		$("#obat_racikan_nama_apoteker").keypress(function(e) {
			$("ul.typeahead").html("");
			$("ul.typeahead").hide();
			if(e.which == 13) {
				$('#obat_racikan_aturan_pakai').focus();
			}
		});
		$("#bahan_nama_bahan").keypress(function(e) {
			if(e.which == 13) {
				$('#bahan_satuan').focus();
			}
		});
		$("#bahan_satuan").keypress(function(e) {
			e.preventDefault();
			if(e.which == 13) {
				$('#bahan_jumlah').focus();
			}
		});
		$("#obat_jadi_jumlah").keypress(function(e) {
			if(e.which == 13) {
				$('#obat_jadi_aturan_pakai').focus();
			}
		});
		$("#obat_jadi_aturan_pakai").keypress(function(e) {
			$("ul.typeahead").html("");
			$("ul.typeahead").hide();
			if(e.which == 13) {
				$('#obat_jadi_save').trigger('click');
			}
		});
		shortcut.add("F2", function() {
			if($('#obat_racikan_add_form').hasClass('in')) {
				 $("#bahan_add").trigger("click");
			} else if(!$('#resep_add_form').hasClass('in') && !$('#obat_jadi_add_form').hasClass('in') && !$('#bahan_add_form').hasClass('in') && !$("#smis-chooser-modal").hasClass('in')) {
				$("#resep_add").trigger("click");
			} else if ($('#obat_jadi_add_form').hasClass('in')) {
				$("#obat_jadi_browse").trigger('click');
			} else if ($('#bahan_add_form').hasClass('in')) {
				$("#bahan_browse").trigger('click');
			}
		});
		shortcut.add("F3", function() {
			if($('#resep_add_form').hasClass('in')) {
				$("#obat_jadi_add").trigger('click');
			}
		});
		shortcut.add("F4", function() {
			if($('#resep_add_form').hasClass('in')) {
				$("#obat_racikan_add").trigger("click");
			}
		});
		shortcut.add("F6", function() {
			if($('#resep_add_form').hasClass('in')) {
				$("#resep_save").trigger('click');
			} else if($('#obat_racikan_add_form').hasClass('in')){
				$('#racikan_save').trigger('click');
			} else if($("#obat_jadi_add_form").hasClass('in')) {
				$("#obat_jadi_save").trigger('click');
			} else if($("#bahan_add_form").hasClass('in')) {
				$("#bahan_save").trigger('click');
			}
		});
		shortcut.add("F7", function() {
			if($('#resep_add_form').hasClass('in')) {
				$("#pasien_browse").trigger("click");
			} else if($("#obat_racikan_add_form").hasClass('in')) {
				$("#apoteker_browse").trigger("click");
			}
		});
		shortcut.add("F8", function() {
			if($('#resep_add_form').hasClass('in')) {
				$("#dokter_browse").trigger("click");
			}
		});
		resep.refreshHargaAndSubtotal();
	});
</script>