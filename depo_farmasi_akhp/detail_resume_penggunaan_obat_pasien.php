<?php
	require_once("depo_farmasi_akhp/library/InventoryLibrary.php");
	global $db;

	$nrm_pasien = $_POST['nrm_pasien'];
	$noreg_pasien = $_POST['noreg_pasien'];
	$nama_pasien = $_POST['nama_pasien'];
	$alamat_pasien = $_POST['alamat_pasien'];
	$tanggal_daftar = $_POST['tanggal_daftar'];
	$uri = $_POST['uri'];
	$umur = $_POST['umur'];
	$unit = $_POST['unit'];
	$pasien_aktif = $_POST['pasien_aktif'];

	$back_button = new Button("", "", "Kembali");
	$back_button->setClass("btn-danger");
	$back_button->setIcon("fa fa-back");
	$back_button->setAction("rpop.back('" . $nrm_pasien . "', '" . $nama_pasien . "', '" . $alamat_pasien . "')");
	$refresh_button = new Button("", "", "Reload");
	$refresh_button->setClass("btn-info");
	$refresh_button->setIcon("fa fa-refresh");
	$refresh_button->setIsButton(Button::$ICONIC);
	$refresh_button->setAction("rpop.refresh('" . $nrm_pasien . "', '" . $nama_pasien . "', '" . $alamat_pasien . "', '" . $noreg_pasien . "', '" . $tanggal_daftar . "', '" . $uri . "', '" . $umur . "', '" . $unit . "', '" . $pasien_aktif . "')");
	$print_button = new Button("", "", "Unduh PDF");
	$print_button->setClass("btn-inverse");
	$print_button->setIcon("fa fa-file-pdf-o");
	$print_button->setIsButton(Button::$ICONIC);
	$print_button->setAction("rpop.print_detail()");
	$button_group = new ButtonGroup("noprint");
	$button_group->addButton($back_button);
	$button_group->addButton($print_button);
	$button_group->addButton($refresh_button);

	$rows = $db->get_result("
		SELECT *
		FROM " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . "
		WHERE noreg_pasien = '" . $noreg_pasien . "' AND dibatalkan = '0' AND prop NOT LIKE 'del'
	");

	$html = "
		<div id='resume'>
			<table border='0' width='100%'>
				<thead>
					<center><h3>RESUME PENGGUNAAN OBAT PASIEN</h3></center>
					<br/>
				</thead>
				<tbody>
					<tr>
						<td><b>No. RM</b></td>
						<td>:</td>
						<td>" . $nrm_pasien . "</td>
						<td>&Tab;</td>
						<td><b>No. Reg.</b></td>
						<td>:</td>
						<td>" . $noreg_pasien . "</td>
					</tr>
					<tr>
						<td><b>Nama Pasien</b></td>
						<td>:</td>
						<td>" . $nama_pasien . "</td>
						<td>&Tab;</td>
						<td><b>Alamat Pasien</b></td>
						<td>:</td>
						<td>" . $alamat_pasien . "</td>
					</tr>
					<tr>
						<td><b>Umur</b></td>
						<td>:</td>
						<td>" . $umur . "</td>
						<td>&Tab;</td>
						<td><b>Rawat Jalan/Inap</b></td>
						<td>:</td>
						<td>" . $uri . "</td>
					</tr>
					<tr>
						<td><b>Pasien Aktif</b></td>
						<td>:</td>
						<td>" . $pasien_aktif . "</td>
						<td>&Tab;</td>
						<td><b>Waktu Penarikan Data</b></td>
						<td>:</td>
						<td>" . date("d-m-Y, H:i:s") . "</td>
					</tr>
				</tbody>
			</table>
			<br/>
			<table data-fix-header='n' class='table table-bordered table-hover table-striped table-condensed' id='table_dpr'>
				<thead>
					<tr class='inverse dpr_header_normal'>
						<th>No.</th>
						<th>Jenis Transaksi</th>
						<th>Tanggal</th>
						<th>ID Transaksi</th>
						<th>Lokasi</th>
						<th>No. Resep</th>
						<th>Dokter</th>
						<th>Ruangan</th>
						<th>Nama Obat</th>
						<th>Harga</th>
						<th>Jumlah</th>
						<th>Total</th>
					</tr>
				</thead>
				<tbody id='dpr_list'>
	";
	$num = 1;
	foreach ($rows as $row) {
		$is_first_row = true;
		$total = 0;
		$embalase = 0;
		$tuslah = 0;
		$biaya_racik = 0;
		if ($row != null) {
			// obat jadi :
			$obat_jadi_rows = $db->get_result("
				SELECT *
				FROM " . InventoryLibrary::$_TBL_PENJUALAN_OBAT_JADI . "
				WHERE id_penjualan_resep = '" . $row->id . "' AND prop NOT LIKE 'del'
			");
			if ($obat_jadi_rows != null) {
				foreach ($obat_jadi_rows as $obat_jadi_row) {
					$embalase += $obat_jadi_row->embalase;
					$tuslah += $obat_jadi_row->tusla;
					$html .= "<tr>";
					if ($is_first_row) {
						$html .= "<td id='dpr_nomor'><small>" . $num++ . "</small></td>";
						$is_first_row = false;
					} else {
						$html .= "<td></td>";
					}
					$subtotal = $obat_jadi_row->harga * $obat_jadi_row->jumlah;
					$total += $subtotal;
					$html .= "
							<td id='dpr_label' style='display: none;'>data</td>
							<td id='dpr_jenis_transaksi'><small>PENJUALAN OBAT JADI</small></td>
							<td id='dpr_tanggal'><small>" . ArrayAdapter::format("date d-m-Y, H:i:s", $row->tanggal) . "</small></td>
							<td id='dpr_id'><small>" . ArrayAdapter::format("only-digit8", $row->id) . "</small></td>
							<td id='dpr_lokasi'><small>DEPO FARMASI</small></td>
							<td id='dpr_no_resep'><small>" . $row->nomor_resep . "</small></td>
							<td id='dpr_nama_dokter'><small>" . strtoupper($row->nama_dokter) . "</small></td>
							<td id='dpr_unit'><small>" . ArrayAdapter::format("unslug", $row->ruangan) . "</small></td>
							<td id='dpr_nama_obat'><small>" . $obat_jadi_row->nama_obat . "</small></td>
							<td id='dpr_harga' style='display: none;'>" . $obat_jadi_row->harga . "</td>
							<td id='dpr_f_harga'><small><div align='right'>" . ArrayAdapter::format("only-money", $obat_jadi_row->harga) . "</div></small></td>
							<td id='dpr_jumlah' style='display: none;'>" . $obat_jadi_row->jumlah . "</td>
							<td id='dpr_f_jumlah'><small>" . ArrayAdapter::format("number", $obat_jadi_row->jumlah) . "</small></td>
							<td id='dpr_subtotal' style='display: none;'>" . $subtotal . "</td>
							<td id='dpr_f_subtotal'><small><div align='right'>" . ArrayAdapter::format("only-money", $subtotal) . "</div></small></td>
						</tr>
					";
				}
			}
			// bahan racikan :
			$bahan_racikan_rows = $db->get_result("
				SELECT b.*, a.embalase AS 'embalase_racikan', a.tusla AS 'tusla_racikan', a.biaya_racik AS 'biaya_racik_racikan'
				FROM " . InventoryLibrary::$_TBL_PENJUALAN_OBAT_RACIKAN . " a LEFT JOIN " . InventoryLibrary::$_TBL_BAHAN_PAKAI_OBAT_RACIKAN . " b ON a.id = b.id_penjualan_obat_racikan
				WHERE a.id_penjualan_resep = '" . $row->id . "' AND a.prop NOT LIKE 'del' AND b.prop NOT LIKE 'del'
			");
			if ($bahan_racikan_rows != null) {
				foreach ($bahan_racikan_rows as $bahan_racikan_row) {
					$html .= "<tr>";
					if ($is_first_row) {
						$embalase += $bahan_racikan_row->embalase_racikan;
						$tuslah += $bahan_racikan_row->tusla_racikan;
						$biaya_racik += $bahan_racikan_row->biaya_racik_racikan;
						$html .= "<td id='dpr_nomor'><small>" . $num++ . "</small></td>";
						$is_first_row = false;
					} else {
						$html .= "<td></td>";
					}
					$subtotal = $bahan_racikan_row->harga * $bahan_racikan_row->jumlah;
					$total += $subtotal;
					$html .= "
							<td id='dpr_label' style='display: none;'>data</td>
							<td id='dpr_jenis_transaksi'><small>PENJUALAN OBAT RACIKAN</small></td>
							<td id='dpr_tanggal'><small>" . ArrayAdapter::format("date d-m-Y, H:i:s", $row->tanggal) . "</small></td>
							<td id='dpr_id'><small>" . ArrayAdapter::format("only-digit8", $row->id) . "</small></td>
							<td id='dpr_lokasi'><small>DEPO FARMASI</small></td>
							<td id='dpr_no_resep'><small>" . $row->nomor_resep . "</small></td>
							<td id='dpr_nama_dokter'><small>" . strtoupper($row->nama_dokter) . "</small></td>
							<td id='dpr_unit'><small>" . ArrayAdapter::format("unslug", $row->ruangan) . "</small></td>
							<td id='dpr_nama_obat'><small>" . $bahan_racikan_row->nama_obat . "</small></td>
							<td id='dpr_harga' style='display: none;'>" . $bahan_racikan_row->harga . "</td>
							<td id='dpr_f_harga'><small><div align='right'>" . ArrayAdapter::format("only-money", $bahan_racikan_row->harga) . "</div></small></td>
							<td id='dpr_jumlah' style='display: none;'>" . $bahan_racikan_row->jumlah . "</td>
							<td id='dpr_f_jumlah'><small>" . ArrayAdapter::format("number", $bahan_racikan_row->jumlah) . "</small></td>
							<td id='dpr_subtotal' style='display: none;'>" . $subtotal . "</td>
							<td id='dpr_f_subtotal'><small><div align='right'>" . ArrayAdapter::format("only-money", $subtotal) . "</div></small></td>
						</tr>
					";
				}
			}
			// retur obat :
			$detail_retur_rows = $db->get_result("
				SELECT b.id, b.tanggal, c.id_obat, c.kode_obat, c.nama_obat, c.nama_jenis_obat, a.jumlah, a.harga, b.persentase_retur
				FROM (" . InventoryLibrary::$_TBL_DRETUR_PENJUALAN_RESEP . " a LEFT JOIN " . InventoryLibrary::$_TBL_RETUR_PENJUALAN_RESEP . " b ON a.id_retur_penjualan_resep = b.id) LEFT JOIN " . InventoryLibrary::$_TBL_STOK_OBAT . " c ON a.id_stok_obat = c.id
				WHERE b.id_penjualan_resep = '" . $row->id . "' AND a.prop NOT LIKE 'del' AND b.prop NOT LIKE 'del'
			");
			if ($detail_retur_rows != null) {
				foreach ($detail_retur_rows as $detail_retur_row) {
					$html .= "<tr>";
					if ($is_first_row) {
						$html .= "<td id='dpr_nomor'><small>" . $num++ . "</small></td>";
						$is_first_row = false;
					} else {
						$html .= "<td></td>";
					}
					$harga = (-1) * ($detail_retur_row->harga - ($detail_retur_row->harga * (100 - $detail_retur_row->persentase_retur) / 100));
					$subtotal = $detail_retur_row->jumlah * $harga;
					$total += $subtotal;
					$html .= "
							<td id='dpr_label' style='display: none;'>data</td>
							<td id='dpr_jenis_transaksi'><small>RETUR PENJUALAN RESEP</small></td>
							<td id='dpr_tanggal'><small>" . ArrayAdapter::format("date d-m-Y, H:i:s", $detail_retur_row->tanggal) . "</small></td>
							<td id='dpr_id'><small>" . ArrayAdapter::format("only-digit8", $detail_retur_row->id) . "</small></td>
							<td id='dpr_lokasi'><small>DEPO FARMASI</small></td>
							<td id='dpr_no_resep'><small>" . $row->nomor_resep . "</small></td>
							<td id='dpr_nama_dokter'><small>" . strtoupper($row->nama_dokter) . "</small></td>
							<td id='dpr_unit'><small>" . ArrayAdapter::format("unslug", $row->ruangan) . "</small></td>
							<td id='dpr_nama_obat'><small>" . $detail_retur_row->nama_obat . "</small></td>
							<td id='dpr_harga' style='display: none;'>" . $harga . "</td>
							<td id='dpr_f_harga'><small><div align='right'>" . ArrayAdapter::format("only-money", $harga) . "</div></small></td>
							<td id='dpr_jumlah' style='display: none;'>" . $detail_retur_row->jumlah . "</td>
							<td id='dpr_f_jumlah'><small>" . ArrayAdapter::format("number", $detail_retur_row->jumlah) . "</small></td>
							<td id='dpr_subtotal' style='display: none;'>" . $subtotal . "</td>
							<td id='dpr_f_subtotal'><small><div align='right'>" . ArrayAdapter::format("only-money", $subtotal) . "</div></small></td>
						</tr>
					";
				}
			}
			$html .= "
				<tr>
					<td id='dpr_label' colspan='11'><div align='right'><small><strong>Subtotal</strong></small></div></td>
					<td id='dpr_subtotal' style='display: none;'>" . $total . "</td>
					<td id='dpr_f_subtotal'><strong><small><div align='right'>" . ArrayAdapter::format("only-money", $total) . "</div></small></strong></td>
				</tr>
				<tr>
					<td id='dpr_label' colspan='11'><div align='right'><small><strong>Embalase</strong></small></div></td>
					<td id='dpr_embalase' style='display: none;'>" . $embalase . "</td>
					<td id='dpr_f_embalase'><strong><small><div align='right'>" . ArrayAdapter::format("only-money", $embalase) . "</div></small></strong></td>
				</tr>
				<tr style='display: none;'>
					<td id='dpr_label' colspan='11'><div align='right'><small><strong>Tuslah</strong></small></div></td>
					<td id='dpr_tuslah' style='display: none;'>" . $tuslah . "</td>
					<td id='dpr_f_tuslah'><strong><small><div align='right'>" . ArrayAdapter::format("only-money", $tuslah) . "</div></small></strong></td>
				</tr>
				<tr>
					<td id='dpr_label' colspan='11'><div align='right'><small><strong>Biaya Racik</strong></small></div></td>
					<td id='dpr_biaya_racik' style='display: none;'>" . $biaya_racik . "</td>
					<td id='dpr_f_biaya_racik'><strong><small><div align='right'>" . ArrayAdapter::format("only-money", $biaya_racik) . "</div></small></strong></td>
				</tr>
				<tr>
					<td id='dpr_label' colspan='11'><div align='right'><small><strong>Total</strong></small></div></td>
					<td id='dpr_total' style='display: none;'>" . ($total + $embalase + $tuslah + $biaya_racik) . "</td>
					<td id='dpr_f_total'><strong><small><div align='right'>" . ArrayAdapter::format("only-money", ($total + $embalase + $tuslah + $biaya_racik)) . "</div></small></strong></td>
				</tr>
			";
		}
	}
	$html .= "
				</tbody>
			</table>
		</div>
		<div align='right'>
			" . $button_group->getHtml() . "
		</div>
	";
	echo $html;
	echo addJS("depo_farmasi_akhp/js/resume_penggunaan_obat_pasien_action.js", false);
?>
<script type="text/javascript">
	var rpop;
	$(document).ready(function() {
		rpop = new RPOPAction(
			"rpop",
			"depo_farmasi_akhp",
			"resume_penggunaan_obat_pasien",
			new Array()
		);
	});
</script>