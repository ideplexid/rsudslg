<?php
    require_once("depo_farmasi_akhp/library/InventoryLibrary.php");
    global $db;

    $id = array(
        "noreg_pasien"    => $_POST["id"]
    );
    $data                   = array();
    $data['alamat_pasien']  = $_POST['alamat_pasien'];
    $data['nama_pasien']    = $_POST['nama_pasien'];
    $data['jenis']          = $_POST['carabayar'];
    
    $dbtable = new DBTable($db, InventoryLibrary::$_TBL_PENJUALAN_RESEP);
    $dbtable->update($data, $id);
    
    return true;
?>