<?php
	global $db;

	if (isset($_POST['noreg_pasien'])) {
		$noreg_pasien = $_POST['noreg_pasien'];

		require_once("depo_farmasi_akhp/library/InventoryLibrary.php");

		$obat = 0;
		$obat_kemoterapi = 0;
		$obat_kronis = 0;

		$resep_rows = $db->get_result("
			SELECT *
			FROM " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . "
			WHERE prop = '' AND dibatalkan = '0' AND noreg_pasien = '" . $noreg_pasien . "'
		");

		if ($resep_rows != null) {
			foreach ($resep_rows as $rr) {
				
				if ($rr->kategori == "kemoterapi")
					$obat_kemoterapi += $rr->total;
				else if ($rr->kategori == "kronis")
					$obat_kronis += $rr->total;
				else
					$obat += $rr->total;
				
				$retur_resep_rows = $db->get_result("
					SELECT 
						a.*, b.persentase_retur, c.nama_obat
					FROM 
						" . InventoryLibrary::$_TBL_DRETUR_PENJUALAN_RESEP . " a
							LEFT JOIN " . InventoryLibrary::$_TBL_RETUR_PENJUALAN_RESEP . " b ON a.id_retur_penjualan_resep = b.id
							LEFT JOIN " . InventoryLibrary::$_TBL_STOK_OBAT . " c ON a.id_stok_obat = c.id
					WHERE 
						a.id_penjualan_resep = '" . $rr->id . "' 
							AND b.dibatalkan = 0
				");
				
				if ($retur_resep_rows != null) {
					foreach($retur_resep_rows as $rrr) {
						if ($rr->kategori == "kemoterapi")
							$obat_kemoterapi -= ($rrr->harga * $rrr->persentase_retur / 100 * $rrr->jumlah);
						else if ($rr->kategori == "kronis")
							$obat_kronis -= ($rrr->harga * $rrr->persentase_retur / 100 * $rrr->jumlah);
						else
							$obat -= ($rrr->harga * $rrr->persentase_retur / 100 * $rrr->jumlah);
					}
				}
			}
		}

		$data = array(
			"obat" 				=> $obat,
			"obat_kemoterapi"	=> $obat_kemoterapi,
			"obat_kronis"		=> $obat_kronis
		);
		echo json_encode($data);
	}
?>