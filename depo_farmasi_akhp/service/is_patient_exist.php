<?php
	require_once("depo_farmasi_akhp/library/InventoryLibrary.php");
	global $db; 
	
	$dbtable = new DBTable($db, InventoryLibrary::$_TBL_PENJUALAN_RESEP);
	$kriteria = array();
	$kriteria['noreg_pasien'] = $_POST['noreg_pasien'];
	$exist_resep = $dbtable->is_exist($kriteria);
	$dbtable = new DBTable($db, InventoryLibrary::$_TBL_ASUHAN_FARMASI);
	$exist_asuhan = $dbtable->is_exist($kriteria);
	if ($exist_resep || $exist_asuhan) {
	    echo json_encode(1);
	} else {
	    echo json_encode(0);
	}
?>