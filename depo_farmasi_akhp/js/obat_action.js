function ObatAction(name, page, action, column) {
	this.initialize(name, page, action, column);
}
ObatAction.prototype.constructor = ObatAction;
ObatAction.prototype = new TableAction();
ObatAction.prototype.chooser = function(modal, elm_id, param,action, modal_title) {
	if ($(".btn").attr("disabled") == "disabled")
		return;
	$(".btn").removeAttr("disabled");
	$(".btn").attr("disabled", "disabled");
	var data = this.getRegulerData();
	var dt = this.addChooserData(data);
	var self = this;
	data['super_command'] = param;
	$.post(
		'',
		dt,
		function(res) {
			show_chooser(self, param, res, action.getShowParentModalInChooser(), modal_title);
			action.view();
			action.focusSearch();
			this.current_chooser=action;
			CURRENT_SMIS_CHOOSER=action;
			$(".btn").removeAttr("disabled");
		}
	);
};
ObatAction.prototype.selected = function(json) {
	$("#obat_jadi_id_obat").val(json.header.id_obat);
	$("#obat_jadi_kode_obat").val(json.header.kode_obat);
	$("#obat_jadi_name_obat").val(json.header.nama_obat);
	$("#obat_jadi_nama_obat").val(json.header.nama_obat);
	$("#obat_jadi_nama_jenis_obat").val(json.header.nama_jenis_obat);
	$("#obat_jadi_satuan").html(json.satuan_option);
	this.setDetailInfo();
};
ObatAction.prototype.setDetailInfo = function() {
	var part = $("#obat_jadi_satuan").val().split("_");
	$("#obat_jadi_konversi").val(part[0]);
	$("#obat_jadi_satuan_konversi").val(part[1]);
	var data = this.getRegulerData();
	data['super_command'] = "sisa";
	data['command'] = "edit";
	data['id_obat'] = $("#obat_jadi_id_obat").val();
	data['satuan'] = $("#obat_jadi_satuan").find(":selected").text();
	data['konversi'] = $("#obat_jadi_konversi").val();
	data['satuan_konversi'] = $("#obat_jadi_satuan_konversi").val();
	if ($("#resep_jenis").length)
		data['jenis_pasien'] = $("#resep_jenis").val();
	if ($("#resep_asuransi").length)
		data['asuransi'] = $("#resep_asuransi").val();
	if ($("#resep_perusahaan").length)
		data['perusahaan'] = $("#resep_perusahaan").val();
	if ($("#resep_uri").length)
		data['uri'] = $("#resep_uri").val();
	data['ppn'] = $("#resep_ppn").val();
	$.post(
		"",
		data,
		function(response) {
			var json = getContent(response);
			if (json == null) return;
			$("#obat_jadi_stok").val(json.sisa);
			$("#obat_jadi_f_stok").val(json.sisa + " " + json.satuan);
			var hna = parseFloat(json.hna) * (1 + parseFloat(json.markup) / 100);
			hna = "Rp. " + (parseFloat(hna)).formatMoney("2", ".", ",");
			$("#obat_jadi_hna").val(hna);
			$("#obat_jadi_markup").val(json.markup);
			$("#obat_jadi_jumlah").focus();
		}
	);
};