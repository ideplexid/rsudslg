function LSOAction(name, page, action, column) {
	this.initialize(name, page, action, column);
}
LSOAction.prototype.constructor = LSOAction;
LSOAction.prototype = new TableAction();
LSOAction.prototype.view = function() {
	if ($("#lso_tanggal_from").val() == "" || $("#lso_tanggal_to").val() == "")
		return;
	var self = this;
	$("#lso_proceed_tanggal_from").val($("#lso_tanggal_from").val());
	$("#lso_proceed_tanggal_to").val($("#lso_tanggal_to").val());
	$("#lso_info").empty();
	$("#laporan_stock_opname_loading_bar").sload("true", "Harap ditunggu...", 0);
	$("#lso_loading_modal").smodal("show");
	FINISHED = false;
	var data = this.getRegulerData();
	data['command'] = "get_jumlah_obat";
	data['tanggal_from'] = $("#lso_tanggal_from").val();
	data['tanggal_to'] = $("#lso_tanggal_to").val();
	$.post(
		"",
		data,
		function(response) {
			var json = JSON.parse(response);
			if (json == null) return;
			$("#lso_list").empty();
			self.fillHtml(0, json.jumlah);
		}
	);
};
LSOAction.prototype.fillHtml = function(num, limit) {
	if (FINISHED || num == limit) {
		if (FINISHED == false && num == limit) {
			$(".data_action_button").show();
			this.finalize();
		} else {
			$("#lso_loading_modal").smodal("hide");
			$("#lso_info").html(
				"<div class='alert alert-block alert-inverse'>" +
					 "<center><strong>TIDAK SEMUA DATA PERSEDIAAN DITAMPILKAN</strong></center>" +
				 "</div>"
			);
			$("#lso_export_button").removeAttr("onclick");
		}
		return;
	}
	var self = this;
	var data = this.getRegulerData();
	data['command'] = "get_info_obat";
	data['num'] = num;
	data['tanggal_from'] = $("#lso_tanggal_from").val();
	data['tanggal_to'] = $("#lso_tanggal_to").val();
	$.post(
		"",
		data,
		function(response) {
			var json = JSON.parse(response);
			if (json == null) return;
			$("tbody#lso_list").append(
				json.html
			);
			$("#laporan_stock_opname_loading_bar").sload("true", json.id_obat + " - " + json.kode_obat + " - " + json.nama_obat + " - " + json.nama_jenis_obat + " (" + (num + 1) + " / " + limit + ")", (num + 1) * 100 / limit - 1);
			self.fillHtml(num + 1, limit);
		}
	);
};
LSOAction.prototype.finalize = function() {
	var num_rows = $("tbody#lso_list tr").length;
	for (var i = 0; i < num_rows; i++)
		$("tbody#lso_list tr:eq(" + i + ") td#nomor").html("<small>" + (i + 1) + "</small>");
	$("#lso_loading_modal").smodal("hide");
	$("#lso_info").html(
		"<div class='alert alert-block alert-info'>" +
			 "<center><strong>SEMUA DATA PERSEDIAAN BERHASIL DITAMPILKAN</strong></center>" +
		 "</div>"
	);
	$("#lso_export_button").removeAttr("onclick");
	$("#lso_export_button").attr("onclick", "lso.export_xls()");
};
LSOAction.prototype.cancel = function() {
	FINISHED = true;
};
LSOAction.prototype.export_xls = function() {
	showLoading();
	var num_rows = $("#lso_list").children("tr").length;
	var d_data = {};
	for (var i = 0; i < num_rows; i++) {
		var nomor = $("tbody#lso_list tr:eq(" + i + ") td#nomor").text();
		var id_obat = $("tbody#lso_list tr:eq(" + i + ") td#id_obat").text();
		var kode_obat = $("tbody#lso_list tr:eq(" + i + ") td#kode_obat").text();
		var nama_obat = $("tbody#lso_list tr:eq(" + i + ") td#nama_obat").text();
		var nama_jenis_obat = $("tbody#lso_list tr:eq(" + i + ") td#nama_jenis_obat").text();
		var stok_sebelum = $("tbody#lso_list tr:eq(" + i + ") td#stok_sebelum").text();
		var stok_sesudah = $("tbody#lso_list tr:eq(" + i + ") td#stok_sesudah").text();
		var selisih = $("tbody#lso_list tr:eq(" + i + ") td#selisih").text();
		var nilai = $("tbody#lso_list tr:eq(" + i + ") td#nilai").text();
		d_data[i] = {
			"nomor" 			: nomor,
			"id_obat" 			: id_obat,
			"kode_obat" 		: kode_obat,
			"nama_obat" 		: nama_obat,
			"nama_jenis_obat" 	: nama_jenis_obat,
			"stok_sebelum"		: stok_sebelum,
			"stok_sesudah"		: stok_sesudah,
			"selisih"			: selisih,
			"nilai"				: nilai
		};
	}
	var data = this.getRegulerData();
	data['command'] = "export_xls";
	data['tanggal_from'] = $("#lso_tanggal_from").val();
	data['tanggal_to'] = $("#lso_tanggal_to").val();
	data['d_data'] = JSON.stringify(d_data);
	data['num_rows'] = num_rows;
	postForm(data);
	dismissLoading();
};