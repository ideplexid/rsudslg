function DRPBAction(name, page, action, column) {
	this.initialize(name, page, action, column);
}
DRPBAction.prototype.constructor = DRPBAction;
DRPBAction.prototype = new TableAction();
DRPBAction.prototype.getRegulerData = function() {
	var data = TableAction.prototype.getRegulerData.call(this);
	data['tanggal_from'] = $("#drpb_tanggal_from").val();
	data['tanggal_to'] = $("#drpb_tanggal_to").val();
	return data;
};
DRPBAction.prototype.view = function() {
	if ($("#drpb_tanggal_from").val() == "" || $("#drpb_tanggal_to").val() == "")
		return;
	var self = this;
	FINISHED = false;
	$("#drpb_info").empty();
	$("#table_drpb tfoot").remove();
	$("#drpb_list").empty();
	$("#drpb_loading_bar").sload("true", "Harap ditunggu...", 0);
	$("#drpb_loading_modal").smodal("show");
	var data = this.getRegulerData();
	data['command'] = "get_jumlah";
	$.post(
		"",
		data,
		function(response) {
			var json = JSON.parse(response);
			if (json == null) return;
			self.fillHtml(0, json.jumlah);
		}
	);
};
DRPBAction.prototype.fillHtml = function(num, limit) {
	if (FINISHED || num == limit) {
		if (FINISHED == false && num == limit) {
			this.finalize();
		} else {
			$("#drpb_loading_modal").smodal("hide");
			$("#drpb_info").html(
				"<div class='alert alert-block alert-inverse'>" +
					 "<center><strong>PROSES DIBATALKAN</strong></center>" +
				 "</div>"
			);
			$("#drpb_export_button").removeAttr("onclick");
		}
		return;
	}
	var self = this;
	var data = this.getRegulerData();
	data['command'] = "get_info";
	data['num'] = num;
	$.post(
		"",
		data,
		function(response) {
			var json = JSON.parse(response);
			if (json == null) return;
			console.log(json);
			$("tbody#drpb_list").append(
				json.html
			);
			$("#drpb_loading_bar").sload("true", json.nomor_transaksi + " - " + json.nomor_resep + " - " + json.nama_pasien + " (" + (num + 1) + " / " + limit + ")", (num + 1) * 100 / limit - 1);
			self.fillHtml(num + 1, limit);
		}
	);
};
DRPBAction.prototype.finalize = function() {
	$("#drpb_loading_modal").smodal("hide");
	$("#drpb_info").html(
		"<div class='alert alert-block alert-info'>" +
			 "<center><strong>PROSES SELESAI</strong></center>" +
		 "</div>"
	);
	$("#drpb_export_button").removeAttr("onclick");
	$("#drpb_export_button").attr("onclick", "drpb.export_xls()");
};
DRPBAction.prototype.cancel = function() {
	FINISHED = true;
};
DRPBAction.prototype.export_xls = function() {
	showLoading();
	var num_rows = $("#drpb_list").children("tr").length;
	var d_data = {};
	for (var i = 0; i < num_rows; i++) {
		var label = $("tbody#drpb_list tr:eq(" + i + ") td#drpb_label").text();
		if (label == "data") {
			var nomor = $("tbody#drpb_list tr:eq(" + i + ") td#drpb_nomor").text();
			var tanggal = $("tbody#drpb_list tr:eq(" + i + ") td#drpb_tanggal").text();
			var id = $("tbody#drpb_list tr:eq(" + i + ") td#drpb_id").text();
			var lokasi = $("tbody#drpb_list tr:eq(" + i + ") td#drpb_lokasi").text();
			var nama_pasien = $("tbody#drpb_list tr:eq(" + i + ") td#drpb_nama_pasien").text();
			var no_resep = $("tbody#drpb_list tr:eq(" + i + ") td#drpb_no_resep").text();
			var nama_obat = $("tbody#drpb_list tr:eq(" + i + ") td#drpb_nama_obat").text();
			var harga = $("tbody#drpb_list tr:eq(" + i + ") td#drpb_harga").text();
			var jumlah = $("tbody#drpb_list tr:eq(" + i + ") td#drpb_jumlah").text();
			var subtotal = $("tbody#drpb_list tr:eq(" + i + ") td#drpb_subtotal").text();
			d_data[i] = {
				"label" 		: label,
				"nomor" 		: nomor,
				"tanggal" 		: tanggal,
				"id" 			: id,
				"lokasi" 		: lokasi,
				"nama_pasien"	: nama_pasien,
				"no_resep"		: no_resep,
				"nama_obat"		: nama_obat,
				"harga"			: harga,
				"jumlah"		: jumlah,
				"subtotal"		: subtotal
			};
		} else {
			var total = $("tbody#drpb_list tr:eq(" + i + ") td#drpb_total").text();
			d_data[i] = {
				"label"			: "total",
				"total"			: total
			};
		}
	}
	var data = this.getRegulerData();
	data['command'] = "export_xls";
	data['tanggal_from'] = $("#drpb_tanggal_from").val();
	data['tanggal_to'] = $("#drpb_tanggal_to").val();
	data['d_data'] = JSON.stringify(d_data);
	data['num_rows'] = num_rows;
	postForm(data);
	dismissLoading();
};