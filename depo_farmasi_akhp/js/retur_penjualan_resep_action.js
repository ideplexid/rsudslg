function ReturPenjualanResepAction(name, page, action, column) {
	this.initialize(name, page, action, column);
}
ReturPenjualanResepAction.prototype.constructor = ReturPenjualanResepAction;
ReturPenjualanResepAction.prototype = new TableAction();
ReturPenjualanResepAction.prototype.refreshInfoBatalBerobat = function () {
	var data = this.getRegulerData();
	data['command'] = "get_batal_berobat_info";
	data['super_command'] == "retur";
	$.post(
		"",
		data,
		function (response) {
			var json = JSON.parse(response);
			if (json == null) return;
			$("#warning_batal_berobat").html(json.html);
			if (json.show == 1)
				$("#warning_batal_berobat").show();
			else
				$("#warning_batal_berobat").hide();
		}
	);
};
ReturPenjualanResepAction.prototype.view = function () {
	TableAction.prototype.view.call(this);
	this.refreshInfoBatalBerobat();
};
ReturPenjualanResepAction.prototype.show_add_form = function() {
	if ($(".btn").attr("disabled") == "disabled")
		return;
	$(".btn").removeAttr("disabled");
	$(".btn").attr("disabled", "disabled");
	$("#retur_id").val("");
	$("#retur_id_resep").val("");
	$("#retur_dokter").val("");
	$("#retur_noreg").val("");
	$("#retur_nrm").val("");
	$("#retur_pasien").val("");
	$("#retur_alamat").val("");
	$("tbody#dretur_list").children("tr").remove();
	$("#modal_alert_retur_add_form").html("");
	$(".error_field").removeClass("error_field");
	$("#retur_add_form").smodal("show");
	$(".btn").removeAttr("disabled");
};
ReturPenjualanResepAction.prototype.validate = function() {
	var valid = true;
	var invalid_msg = "";
	var tanggal = $("#retur_tanggal").val();
	var id_resep = $("#retur_id_resep").val();
	var retur_exist = false;
	var nor = $("tbody#dretur_list").children("tr").length;
	for(var i = 0; i < nor; i++) {
		var prefix = $("tbody#dretur_list").children("tr").eq(i).prop("id");
		var jumlah_retur = parseFloat($("#" + prefix + "_jumlah_retur").text());
		if (jumlah_retur > 0) {
			retur_exist = true;
			break;
		}
	}
	$(".error_field").removeClass("error_field");
	if (tanggal == "") {
		valid = false;
		invalid_msg += "</br><strong>Tanggal</strong> tidak boleh kosong";
		$("#retur_tanggal").addClass("error_field");
	}
	if (id_resep == "") {
		valid = false;
		invalid_msg += "</br><strong>No. Resep</strong> tidak boleh kosong";
		$("#retur_id_resep").addClass("error_field");
	}
	if (!retur_exist) {
		valid = false;
		invalid_msg += "</br>Tidak ada obat yang diretur";
	}
	if (!valid) {
		$("#modal_alert_retur_add_form").html(
			"<div class='alert alert-block alert-danger'>" +
				"<h4>Peringatan</h4>" +
				invalid_msg +
			"</div>"
		);
	}
	return valid;
};
ReturPenjualanResepAction.prototype.save = function() {
	if ($(".btn").attr("disabled") == "disabled")
		return;
	$(".btn").removeAttr("disabled");
	$(".btn").attr("disabled", "disabled");
	if (!this.validate()) {
		$(".btn").removeAttr("disabled");
		return;
	}
	showLoading();
	$("#retur_add_form").smodal("hide");
	var self = this;
	var cb_data = this.getRegulerData();
	cb_data['super_command'] = "retur";
	cb_data['command'] = "check_closing_bill";
	cb_data['noreg_pasien'] = $("#retur_noreg").val();
	$.post(
		"",
		cb_data,
		function(response_cb) {
			var json_cb = getContent(response_cb);
			if (json_cb == null) {
				dismissLoading();
				$("#retur_add_form").smodal("show");
				$(".btn").removeAttr("disabled");
				return;
			}
			if (json_cb == "0") {
				var data = self.getRegulerData();
				data['super_command'] = "retur";
				data['command'] = "save";
				data['id'] = $("#retur_id").val();
				data['id_penjualan_resep'] = $("#retur_id_resep").val();
				var detail = {};
				var nor = $("tbody#dretur_list").children("tr").length;
				var j = 0;
				for(var i = 0; i < nor; i++) {
					var prefix = $("tbody#dretur_list").children("tr").eq(i).prop("id");
					var jumlah_retur = parseFloat($("#" + prefix + "_jumlah_retur").text());
					if (jumlah_retur > 0) {
						var id_stok_obat = $("#" + prefix + "_id_stok_obat").text();
						var harga = $("#" + prefix + "_harga").text();
						var d_data = {};
						d_data['id_stok_obat'] = id_stok_obat;
						d_data['jumlah'] = jumlah_retur;
						d_data['harga'] = harga;
						detail[j] = d_data;
						j++;
					}
				}
				data['detail'] = detail;
				$.post(
					"",
					data,
					function(response) {
						var json = getContent(response);
						if (json == null) {
							$("#retur_add_form").smodal("show");
						} else {
							self.view();
						}
						dismissLoading();
						$(".btn").removeAttr("disabled");
					}
				);
			} else {
				dismissLoading();
				$(".btn").removeAttr("disabled");
				$("#retur_add_form").smodal("show");
			}
		}
	);
};
ReturPenjualanResepAction.prototype.detail = function(id) {
	if ($(".btn").attr("disabled") == "disabled")
		return;
	$(".btn").removeAttr("disabled");
	$(".btn").attr("disabled", "disabled");
	var data = this.getRegulerData();
	data['super_command'] = "retur";
	data['command'] = "edit";
	data['id'] = id;
	$.post(
		"",
		data,
		function(response) {
			var json = getContent(response);
			if (json == null) return;
			$("#v_retur_id").val(json.header.id);
			$("#v_retur_id_resep").val(json.header.id_penjualan_resep);
			$("#v_retur_no_resep").val(json.header.nomor_resep);
			$("#v_retur_dokter").val(json.header.nama_dokter);
			$("#v_retur_noreg").val(json.header.noreg_pasien);
			$("#v_retur_nrm").val(json.header.nrm_pasien);
			$("#v_retur_pasien").val(json.header.nama_pasien);
			$("#v_retur_alamat").val(json.header.alamat_pasien);
			$("tbody#v_dretur_list").html(json.detail_list);
			$("#v_retur_add_form").smodal("show");
			$(".btn").removeAttr("disabled");
		}
	);
};
ReturPenjualanResepAction.prototype.print_retur = function(id) {
	if ($(".btn").attr("disabled") == "disabled")
		return;
	$(".btn").removeAttr("disabled");
	$(".btn").attr("disabled", "disabled");
	var self = this;
	var data = this.getRegulerData();
	data['super_command'] = "retur";
	data['command'] = "print_retur";
	data['id'] = id;
	$.post(
		"",
		data,
		function(response) {
			var json = getContent(response);
			if (json == null) return;
			self.view();
			smis_print(json);
			$(".btn").removeAttr("disabled");
		}
	);
};
ReturPenjualanResepAction.prototype.cancel = function(id) {
	var self = this;
	bootbox.confirm(
		"Yakin membatalkan Retur Penjualan Resep ini?",
		function(result) {
			if (result) {
				showLoading();
				var data_t = self.getRegulerData();
				data_t['super_command'] = "retur";
				data_t['command'] = "edit";
				data_t['id'] = id;
				$.post(
					"",
					data_t,
					function(response_t) {
						var json_t = getContent(response_t);
						if (json_t == null) return;
						var cb_data = self.getRegulerData();
						cb_data['super_command'] = "retur";
						cb_data['command'] = "check_closing_bill";
						cb_data['noreg_pasien'] = json_t.header.noreg_pasien;
						$.post(
							"",
							cb_data,
							function(response_cb) {
								var json_cb = getContent(response_cb);
								if (json_cb == null) {
									dismissLoading();
									return;
								}
								if (json_cb == "0") {
									var data = self.getRegulerData();
									data['super_command'] = "retur";
									data['command'] = "save";
									data['id'] = id;
									data['dibatalkan'] = "1";
									$.post(	
										"",
										data,
										function(response) {
											var json = getContent(response);
											if (json == null) return;
											self.view();
											dismissLoading();
										}
									);
								} else {
									bootbox.alert("Tagihan pasien tidak dapat diubah karena tagihan pasien sudah ditutup. Silakan menghubungi pihak Kasir.");
									dismissLoading();
								}
							}
						);
					}
				);
			}
		}
	);
};