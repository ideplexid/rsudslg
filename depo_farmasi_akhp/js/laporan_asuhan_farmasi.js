var laf;
$(document).ready(function() {
	laf = new TableAction(
		"laf",
		"depo_farmasi_akhp",
		"laporan_asuhan_farmasi",
		new Array()
	);
	laf.addRegulerData = function(data) {
		data['tanggal_from'] = $("#laf_tanggal_from").val();
		data['tanggal_to'] = $("#laf_tanggal_to").val();
		data['ruangan'] = $("#laf_ruangan").val();
		data['ruangan_label'] = $("#laf_ruangan option:selected").text();
		return data;
	};
	laf.export_xls = function() {
		var data = this.getRegulerData();
		data['command'] = "export_xls";
		postForm(data);
	};
	laf.view();
});