function ObatKSAction(name, page, action, column) {
	this.initialize(name, page, action, column);
}
ObatKSAction.prototype.constructor = ObatKSAction;
ObatKSAction.prototype = new TableAction();
ObatKSAction.prototype.selected = function(json) {
	console.log(json);
	$("#ks_id_obat").val(json.id);
	$("#ks_kode_obat").val(json.kode_obat);
	$("#ks_nama_obat").val(json.nama_obat);
	$("#ks_nama_jenis_obat").val(json.nama_jenis_obat);
};