<?php
	$laporan_tabulator = new Tabulator("laporan", "", Tabulator::$POTRAIT);
	$laporan_tabulator->add("laporan_rincian_penjualan_resep", "Lap. Rincian Penjualan Resep", "depo_farmasi_akhp/laporan_rincian_penjualan_resep.php", Tabulator::$TYPE_INCLUDE);
	$laporan_tabulator->add("laporan_rincian_penjualan_bebas", "Lap. Rincian Penjualan Bebas", "depo_farmasi_akhp/laporan_rincian_penjualan_bebas.php", Tabulator::$TYPE_INCLUDE);
	$laporan_tabulator->add("laporan_rincian_penjualan_up", "Lap. Rincian Penjualan UP", "depo_farmasi_akhp/laporan_rincian_penjualan_up.php", Tabulator::$TYPE_INCLUDE);
	echo $laporan_tabulator->getHtml();
?>