<?php
	require_once("depo_farmasi_akhp/library/InventoryLibrary.php");
	require_once("smis-base/smis-include-service-consumer.php");
	require_once("depo_farmasi_akhp/table/PenjualanResepTable.php");
	require_once("depo_farmasi_akhp/adapter/ObatAdapter.php");
	require_once("depo_farmasi_akhp/adapter/BahanAdapter.php");
	require_once("depo_farmasi_akhp/adapter/PenjualanResepAdapter.php");
	require_once("depo_farmasi_akhp/responder/ObatDBResponder.php");
	require_once("depo_farmasi_akhp/responder/ApotekerServiceResponder.php");
	require_once("depo_farmasi_akhp/responder/BahanDBResponder.php");
	require_once("depo_farmasi_akhp/responder/PenjualanResepDBResponder.php");
	global $db;

	$setting_suffix = "depo_farmasi_akhp";

	$obat_table = new Table(array("Kode", "Obat", "Jenis", "Stok"));
	$obat_table->setName("obat");
	$obat_table->setModel(Table::$SELECT);
	$obat_adapter = new ObatAdapter();
	$obat_dbtable = new DBTable($db, "smis_" . $depo_prefix . "_stok_obat");
	$obat_dbtable->setViewForSelect(true);
	$filter = "";
	if (isset($_POST['kriteria'])) {
		$filter = " AND (nama_obat LIKE '%" . $_POST['kriteria'] . "%' OR nama_jenis_obat LIKE '%" . $_POST['kriteria'] . "%')";
	}	
	$query_value = "
		SELECT v_stok.*, v_harga.hna
		FROM (
			SELECT id_obat AS 'id', kode_obat, nama_obat, nama_jenis_obat, GROUP_CONCAT(CONCAT(sisa, ' ', satuan, ' ', label) ORDER BY satuan, sisa, label ASC SEPARATOR ', ') AS 'stok', satuan, konversi, satuan_konversi
			FROM (
				SELECT a.id_obat, a.kode_obat, a.nama_obat, a.nama_jenis_obat, SUM(a.sisa) AS 'sisa', a.satuan, a.konversi, a.satuan_konversi, CASE a.label WHEN 'reguler' THEN '(R)' WHEN 'sito' THEN '(S)' ELSE '(K)' END AS 'label'
				FROM " . InventoryLibrary::$_TBL_STOK_OBAT . " a LEFT JOIN " .InventoryLibrary::$_TBL_OBAT_MASUK . " b ON a.id_obat_masuk = b.id
				WHERE a.prop NOT LIKE 'del' AND b.status = 'sudah' AND a.konversi = '1' AND a.satuan = a.satuan_konversi  " . $filter . "
				GROUP BY a.id_obat, a.satuan, a.konversi, a.satuan_konversi, a.label
			) v_obat
			GROUP BY id_obat
		) v_stok LEFT JOIN (
			SELECT a.id_obat, a.kode_obat, a.nama_obat, a.nama_jenis_obat,  MAX(a.hna) AS 'hna', a.satuan, a.konversi, a.satuan_konversi
			FROM " . InventoryLibrary::$_TBL_STOK_OBAT . " a LEFT JOIN " . InventoryLibrary::$_TBL_OBAT_MASUK . " b ON a.id_obat_masuk = b.id
			WHERE a.prop NOT LIKE 'del' AND b.status = 'sudah' AND a.konversi = '1' AND a.satuan = a.satuan_konversi " . $filter . "
			GROUP BY a.id_obat, a.nama_obat, a.nama_jenis_obat, a.satuan, a.konversi, a.satuan_konversi
		) v_harga ON v_stok.id = v_harga.id_obat AND v_stok.satuan = v_harga.satuan AND v_stok.konversi = v_harga.konversi AND v_stok.satuan_konversi = v_harga.satuan_konversi
	";
	$query_count = "
		SELECT COUNT(*)
		FROM (
			" . $query_value . "
		) v_obat
	";
	$obat_dbtable->setPreferredQuery(true, $query_value, $query_count);
	$obat_dbresponder = new ObatDBResponder(
		$obat_dbtable,
		$obat_table,
		$obat_adapter
	);

	$apoteker_table = new Table(array("NIP", "Nama", "Jabatan"));
	$apoteker_table->setName("apoteker");
	$apoteker_table->setModel(Table::$SELECT);
	$apoteker_adapter = new SimpleAdapter();
	$apoteker_adapter->add("NIP", "nip");
	$apoteker_adapter->add("Nama", "nama");
	$apoteker_adapter->add("Jabatan", "nama_jabatan");
	$apoteker_service_responder = new ApotekerServiceResponder(
		$db,
		$apoteker_table,
		$apoteker_adapter,
		"employee"
	);

	$bahan_table = new Table(array("Kode", "Bahan", "Jenis", "Stok"));
	$bahan_table->setName("bahan");
	$bahan_table->setModel(Table::$SELECT);
	$bahan_adapter = new BahanAdapter();
	$bahan_dbtable = new DBTable($db, "smis_" . $depo_prefix . "_stok_obat");
	$bahan_dbtable->setViewForSelect(true);
	$filter = "";
	if (isset($_POST['kriteria'])) {
		$filter = " AND (nama_obat LIKE '%" . $_POST['kriteria'] . "%' OR nama_jenis_obat LIKE '%" . $_POST['kriteria'] . "%')";
	}
	$query_value = "
		SELECT v_stok.*, v_harga.hna
		FROM (
			SELECT id_obat AS 'id', kode_obat, nama_obat, nama_jenis_obat, GROUP_CONCAT(CONCAT(sisa, ' ', satuan, ' ', label) ORDER BY satuan, sisa, label ASC SEPARATOR ', ') AS 'stok', satuan, konversi, satuan_konversi
			FROM (
				SELECT a.id_obat, a.kode_obat, a.nama_obat, a.nama_jenis_obat, SUM(a.sisa) AS 'sisa', a.satuan, a.konversi, a.satuan_konversi, CASE a.label WHEN 'reguler' THEN '(R)' WHEN 'sito' THEN '(S)' ELSE '(K)' END AS 'label'
				FROM " . InventoryLibrary::$_TBL_STOK_OBAT . " a LEFT JOIN " . InventoryLibrary::$_TBL_OBAT_MASUK . " b ON a.id_obat_masuk = b.id
				WHERE a.prop NOT LIKE 'del' AND b.status = 'sudah' AND a.konversi = '1' AND a.satuan = a.satuan_konversi  " . $filter . "
				GROUP BY a.id_obat, a.satuan, a.konversi, a.satuan_konversi, a.label
			) v_obat
			GROUP BY id_obat
		) v_stok LEFT JOIN (
			SELECT a.id_obat, a.kode_obat, a.nama_obat, a.nama_jenis_obat,  MAX(a.hna) AS 'hna', a.satuan, a.konversi, a.satuan_konversi
			FROM " . InventoryLibrary::$_TBL_STOK_OBAT . " a LEFT JOIN " . InventoryLibrary::$_TBL_OBAT_MASUK . " b ON a.id_obat_masuk = b.id
			WHERE a.prop NOT LIKE 'del' AND b.status = 'sudah' AND a.konversi = '1' AND a.satuan = a.satuan_konversi " . $filter . "
			GROUP BY a.id_obat, a.nama_obat, a.nama_jenis_obat, a.satuan, a.konversi, a.satuan_konversi
		) v_harga ON v_stok.id = v_harga.id_obat AND v_stok.satuan = v_harga.satuan AND v_stok.konversi = v_harga.konversi AND v_stok.satuan_konversi = v_harga.satuan_konversi
	";
	$query_count = "
		SELECT COUNT(*)
		FROM (
			" . $query_value . "
		) v
	";
	$bahan_dbtable->setPreferredQuery(true, $query_value, $query_count);
	$bahan_dbresponder = new BahanDBResponder(
		$bahan_dbtable,
		$bahan_table,
		$bahan_adapter
	);

	$super_command = new SuperCommand();
	$super_command->addResponder("obat", $obat_dbresponder);
	$super_command->addResponder("apoteker", $apoteker_service_responder);
	$super_command->addResponder("bahan", $bahan_dbresponder);
	$init = $super_command->initialize();
	if ($init != null) {
		echo $init;
		return;
	}

	if (isset($_POST['command'])) {
		if ($_POST['command'] == "get_current_stock") {
			$id_obat = $_POST['id_obat'];
			$satuan = $_POST['satuan'];
			$dbtable = new DBTable($db, "smis_" . $depo_prefix . "_stok_obat");
			$row = $dbtable->get_row("
				SELECT SUM(sisa) AS 'sisa', MAX(hna) AS 'hna'
				FROM " . InventoryLibrary::$_TBL_STOK_OBAT . " a LEFT JOIN " . InventoryLibrary::$_TBL_OBAT_MASUK . " b ON a.id_obat_masuk = b.id
				WHERE a.prop NOT LIKE 'del' AND b.prop NOT LIKE 'del' AND b.status = 'sudah' AND id_obat = '" . $id_obat . "' AND satuan = '" . $satuan . "' AND konversi = '1' AND satuan_konversi = '" . $satuan . "'
			");
			$data = array(
				"sisa" 	=> $row->sisa,
				"hja"	=> $row->hna * (1 + getSettings($db, "smis-" . $setting_suffix . "-markup", 0.2))
			);
			echo json_encode($data);
		} else if ($_POST['command'] == "save_as_penjualan_resep") {
			$resep_table = new PenjualanResepTable(
				array("No. Penjualan", "No. Resep", "Tanggal/Jam", "Dokter", "No. Registrasi", "NRM", "Pasien", "Alamat", "Kat. Pasien", "Jns. Pasien", "Perusahaan", "Asuransi", "Edukasi"),
				"Depo Depo Farmasi AKHP : Penjualan Obat - Resep (KIUP)"
			);
			$resep_adapter = new PenjualanResepAdapter();
			$columns = array("id", "nomor_resep", "tanggal", "id_dokter", "nama_dokter", "noreg_pasien", "nrm_pasien", "nama_pasien", "alamat_pasien", "asuransi", "perusahaan", "ruangan", "uri", "jenis", "markup", "embalase", "tusla", "biaya_racik", "total", "edukasi", "dibatalkan", "diskon", "t_diskon", "keterangan_batal");
			$resep_dbtable = new DBTable(
				$db,
				"smis_" . $depo_prefix . "_penjualan_resep",
				$columns
			);			
			$resep_dbresponder = new PenjualanResepDBResponder(
				$resep_dbtable,
				$resep_table,
				$resep_adapter
			);
			$data = $resep_dbresponder->command("save");
			echo json_encode($data);
		}
		return;
	}

	$editable = $_POST['editable'];
	$id_e_resep = $_POST['header']['id'];
	$tanggal_e_resep = $_POST['header']['tanggal'];
	$id_dokter = $_POST['header']['id_dokter'];
	$nama_dokter = $_POST['header']['nama_dokter'];
	$noreg_pasien = $_POST['header']['noreg_pasien'];
	$nrm_pasien = $_POST['header']['nrm_pasien'];
	$nama_pasien = $_POST['header']['nama_pasien'];
	$alamat_pasien = $_POST['header']['alamat_pasien'];
	$no_telpon_pasien = $_POST['header']['no_telpon'];
	$jenis_pasien = $_POST['header']['jenis'];
	$perusahaan_pasien = $_POST['header']['perusahaan'];
	$asuransi_pasien = $_POST['header']['asuransi'];
	$ruangan = $_POST['header']['ruangan'];

	$header_form = new Form("", "", "Depo Farmasi AKHP : Formulir Tindak Lanjut E-Resep");
	$id_hidden = new Hidden("resep_id", "resep_id", $id_e_resep);
	$header_form->addElement("", $id_hidden);
	$uri_hidden = new Hidden("resep_uri", "resep_uri", "0");
	$header_form->addElement("", $uri_hidden);
	$markup_hidden = new Hidden("resep_markup", "resep_markup", getSettings($db, "smis-" . $setting_suffix . "-markup", 0.2));
	$header_form->addElement("", $markup_hidden);
	$tanggal_text = new Text("resep_tanggal", "resep_tanggal", $tanggal_e_resep);
	$tanggal_text->setAtribute("disabled='disabled'");
	$header_form->addElement("Waktu", $tanggal_text);
	$id_dokter_hidden = new Hidden("resep_id_dokter", "resep_id_dokter", $id_dokter);
	$header_form->addElement("", $id_dokter_hidden);
	$nama_dokter_text = new Text("resep_nama_dokter", "resep_nama_dokter", $nama_dokter);
	$nama_dokter_text->setAtribute("disabled='disabled'");
	$header_form->addElement("Dokter", $nama_dokter_text);
	$noreg_pasien_text = new Text("resep_noreg_pasien", "resep_noreg_pasien", $noreg_pasien);
	$noreg_pasien_text->setAtribute("disabled='disabled'");
	$header_form->addElement("No. Reg.", $noreg_pasien_text);
	$nrm_pasien_text = new Text("resep_nrm_pasien", "resep_nrm_pasien", $nrm_pasien);
	$nrm_pasien_text->setAtribute("disabled='disabled'");
	$header_form->addElement("No. RM", $nrm_pasien_text);
	$nama_pasien_text = new Text("resep_nama_pasien", "resep_nama_pasien", $nama_pasien);
	$nama_pasien_text->setAtribute("disabled='disabled'");
	$header_form->addElement("Nama Pasien", $nama_pasien_text);
	$alamat_pasien_text = new Text("resep_alamat_pasien", "resep_alamat_pasien", $alamat_pasien);
	$alamat_pasien_text->setAtribute("disabled='disabled'");
	$header_form->addElement("Alamat Pasien", $alamat_pasien_text);
	$no_telpon_text = new Text("resep_no_telpon", "resep_no_telpon", $no_telpon_pasien);
	$no_telpon_text->setAtribute("disabled='disabled'");
	$header_form->addElement("Telp. Pasien", $no_telpon_text);
	$jenis_hidden = new Hidden("resep_jenis", "resep_jenis", $jenis_pasien);
	$header_form->addElement("", $jenis_hidden);
	$f_jenis_text = new Text("resep_f_jenis", "resep_f_jenis", ArrayAdapter::format("unslug", $jenis_pasien));
	$f_jenis_text->setAtribute("disabled='disabled'");
	$header_form->addElement("Jenis Pasien", $f_jenis_text);
	$perusahaan_text = new Text("resep_perusahaan", "resep_perusahaan", $perusahaan_pasien);
	$perusahaan_text->setAtribute("disabled='disabled'");
	$header_form->addElement("Perusahaan", $perusahaan_text);
	$asuransi_text = new Text("resep_asuransi", "resep_asuransi", $asuransi_pasien);
	$asuransi_text->setAtribute("disabled='disabled'");
	$header_form->addElement("Asuransi", $asuransi_text);
	$diskon_text = new Text("resep_diskon", "resep_diskon", "0,00");
	$diskon_text->setTypical("money");
	$diskon_text->setAtribute("data-thousands='.' data-decimal=','  data-precision='2'");
	$header_form->addElement("Diskon", $diskon_text);
	$t_diskon_option = new OptionBuilder();
	$t_diskon_option->add("Persen (%)", "persen", "1");
	$t_diskon_option->add("Nominal (Rp)", "nominal");
	$t_diskon_option->add("Paket Tindakan (100 %)", "gratis");
	$t_diskon_select = new Select("resep_t_diskon", "resep_t_diskon", $t_diskon_option->getContent());
	$header_form->addElement("Tipe Diskon", $t_diskon_select);
	$ruangan_hidden = new Hidden("resep_ruangan", "resep_ruangan", $ruangan);
	$header_form->addElement("", $ruangan_hidden);
	$f_ruangan_text = new Text("resep_f_ruangan", "resep_f_ruangan", ArrayAdapter::format("unslug", $ruangan));
	$f_ruangan_text->setAtribute("disabled='disabled'");
	$header_form->addElement("Ruangan", $f_ruangan_text);

	$embalase_hidden = new Hidden("resep_embalase", "resep_embalase", getSettings($db, "smis-" . $setting_suffix . "-embalase", 0));
	$header_form->addElement("", $embalase_hidden);
	$tuslah_hidden = new Hidden("resep_tuslah", "resep_tuslah", getSettings($db, "smis-" . $setting_suffix . "-tuslah", 0));
	$header_form->addElement("", $tuslah_hidden);
	$biaya_racik_hidden = new Hidden("resep_biaya_racik", "resep_biaya_racik", getSettings($db, "smis-" . $setting_suffix . "-biaya_racik", 0));
	$header_form->addElement("", $biaya_racik_hidden);

	$bahan_html = "
		<table style='display: none;'>
			<tbody id='r_bahan_list'>
	";
	$detail_html = "<table data-fix-header='n' class='table table-bordered table-hover table-striped table-condensed' id='table_dresep'>
						<thead>
							<tr class='inverse dresep_header_normal'>
								<th>No.</th>
								<th>Nama</th>
								<th>Jumlah</th>
								<th>Harga Satuan</th>
								<th>Embalase</th>
								<th>Tuslah</th>
								<th>Biaya Racik</th>
								<th>Subtotal</th>
								<th>Aturan Pakai</th>
								<th>Apoteker</th>
								<th>Ubah</th>
								<th>Hapus</th>
							</tr>
						</thead>
						<tbody id='dresep_list'>";
	$details = json_decode($_POST['details'], true);
	$nomor = 1;
	$row_num = 0;
	$total_biaya = 0;
	$dbtable = new DBTable($db, "smis_" . $depo_prefix . "_penjualan_resep");
	foreach ($details as $d) {
		if ($d['is_racikan'] == 1) {
			$harga_racikan = 0;
			$embalase_racikan = 0;
			$tuslah_racikan = 0;
			$biaya_racik_racikan = getSettings($db, "smis-" . $setting_suffix . "-biaya_racik", 0);

			$racikan_details = $d['details'];
			foreach ($racikan_details as $rd) {
				$harga_row = $dbtable->get_row("
					SELECT MAX(hna) AS 'harga'
					FROM smis_" . $depo_prefix . "_stok_obat a LEFT JOIN smis_" . $depo_prefix . "_obat_masuk b ON a.id_obat_masuk = b.id
					WHERE a.prop NOT LIKE 'del' AND b.prop NOT LIKE 'del' AND b.status = 'sudah' AND a.id_obat = '" . $rd['id_obat'] . "' AND a.satuan = '" . $rd['satuan'] . "' AND a.konversi = '1' AND a.satuan_konversi = '" . $rd['satuan'] . "'
				");
				
				$markup =  getSettings($db, "smis-" . $setting_suffix . "-markup", 0.2);
				$harga = $harga_row->harga * (1 + $markup);
				$embalase = getSettings($db, "smis-" . $setting_suffix . "-embalase", 0);
				$tuslah = getSettings($db, "smis-" . $setting_suffix . "-tuslah", 0);
				
				$harga_racikan += ($rd['jumlah'] * $harga);
				$embalase_racikan += $embalase;
				$tuslah_racikan += $tuslah;

				$bahan_html .= "
					<tr class='" . $row_num . "'>
						<td id='id_obat'>" . $rd['id_obat'] . "</td>
						<td id='kode_obat'>" . $rd['kode_obat'] . "</td>
						<td id='nama_obat'>" . $rd['nama_obat'] . "</td>
						<td id='nama_jenis_obat'>" . $rd['nama_jenis_obat'] . "</td>
						<td id='jumlah'>" . $rd['jumlah'] . "</td>
						<td id='satuan'>" . $rd['satuan'] . "</td>
						<td id='konversi'>" . 1 . "</td>
						<td id='satuan_konversi'>" . $rd['satuan'] . "</td>
						<td id='harga'>" . $harga . "</td>
						<td id='embalase'>" . $embalase . "</td>
						<td id='tuslah'>" . $tuslah . "</td>
					</tr>
				";
			}

			$subtotal = $harga_racikan + $embalase_racikan + $tuslah_racikan + $biaya_racik_racikan;
			$total_biaya += $subtotal;

			$edit_button = "<center><a href='#' onclick='dresep.edit_racikan(" . $row_num . ")' class='input btn btn-warning'><i class='fa fa-pencil'></i></a></center>";
			$delete_button = "<center><a href='#' onclick='dresep.delete_racikan(" . $row_num . ")' class='input btn btn-danger'><i class='fa fa-trash-o'></i></a></center>";
			$row_num++;
			$detail_html .= "
				<tr>
					<td id='nomor'><small>" . ($nomor++) . "</small></td>
					<td id='is_racikan' style='display: none;'>" . $d['is_racikan'] . "</td>
					<td id='nama_racikan'><small>RACIKAN #" . $d['label_racikan'] . "</small></td>
					<td id='jumlah' style='display: none;'>1</td>
					<td id='satuan' style='display: none;'>RACIKAN</td>
					<td id='f_jumlah'><small>1 RACIKAN</small></td>
					<td id='harga' style='display: none;'>" . $harga_racikan . "</td>
					<td id='f_harga'><small><div align='right'>" . ArrayAdapter::format("only-money", $harga_racikan) . "</div></small></td>
					<td id='embalase' style='display: none;'>" . $embalase_racikan . "</td>
					<td id='f_embalase'><small><div align='right'>" . ArrayAdapter::format("only-money", $embalase_racikan) . "</div></small></td>
					<td id='tuslah' style='display: none;'>" . $tuslah_racikan . "</td>
					<td id='f_tuslah'><small><div align='right'>" . ArrayAdapter::format("only-money", $tuslah_racikan) . "</div></small></td>
					<td id='biaya_racik' style='display: none;'>" . $biaya_racik_racikan . "</td>
					<td id='f_biaya_racik'><small><div align='right'>" . ArrayAdapter::format("only-money", $biaya_racik_racikan) . "</div></small></td>
					<td id='subtotal' style='display: none;'>" . $subtotal . "</td>
					<td id='f_subtotal'><small><div align='right'>" . ArrayAdapter::format("only-money", $subtotal) . "</div></small></td>
					<td id='aturan_pakai'><small>" . $d['aturan_pakai'] . "</small></td>
					<td id='id_apoteker' style='display: none;'></td>
					<td id='nama_apoteker'><small>-</small></td>
					<td class='edit_button'>" . $edit_button . "</td>
					<td class='del_button'>" . $delete_button . "</td>
				</tr>
			";
		} else {
			$harga_row = $dbtable->get_row("
				SELECT MAX(hna) AS 'harga'
				FROM smis_" . $depo_prefix . "_stok_obat a LEFT JOIN smis_" . $depo_prefix . "_obat_masuk b ON a.id_obat_masuk = b.id
				WHERE a.prop NOT LIKE 'del' AND b.prop NOT LIKE 'del' AND b.status = 'sudah' AND a.id_obat = '" . $d['id_obat'] . "' AND a.satuan = '" . $d['satuan'] . "' AND a.konversi = '1' AND a.satuan_konversi = '" . $d['satuan'] . "'
			");
			
			$markup =  getSettings($db, "smis-" . $setting_suffix . "-markup", 0.2);
			$harga_non_racikan = $harga_row->harga * (1 + $markup);
			$embalase_non_racikan = getSettings($db, "smis-" . $setting_suffix . "-embalase", 0);
			$tuslah_non_racikan = getSettings($db, "smis-" . $setting_suffix . "-tuslah", 0);
			$biaya_racik_non_racikan = 0;
			$subtotal = $d['jumlah'] * $harga_non_racikan + $embalase_non_racikan + $tuslah_non_racikan + $biaya_racik_non_racikan;
			$total_biaya += $subtotal;

			$edit_button = "<center><a href='#' onclick='dresep.edit_non_racikan(" . $row_num . ")' class='input btn btn-warning'><i class='fa fa-pencil'></i></a></center>";
			$delete_button = "<center><a href='#' onclick='dresep.delete_non_racikan(" . $row_num . ")' class='input btn btn-danger'><i class='fa fa-trash-o'></i></a></center>";
			$row_num++;
			$detail_html .= "
				<tr>
					<td id='nomor'><small>" . ($nomor++) . "</small></td>
					<td id='is_racikan' style='display: none;'>" . $d['is_racikan'] . "</td>
					<td id='id_obat' style='display: none;'>" . $d['id_obat'] . "</td>
					<td id='kode_obat' style='display: none;'><small>" . $d['kode_obat'] . "</small></td>
					<td id='nama_obat'><small>" . $d['nama_obat'] . "</small></td>
					<td id='nama_jenis_obat' style='display: none;'><small>" . $d['nama_jenis_obat'] . "</small></td>
					<td id='jumlah' style='display: none;'>" . $d['jumlah'] .  "</td>
					<td id='satuan' style='display: none;'>" . $d['satuan'] . "</td>
					<td id='konversi' style='display: none;'>" . 1 .  "</td>
					<td id='satuan_konversi' style='display: none;'>" . $d['satuan'] .  "</td>
					<td id='f_jumlah'><small>" . $d['jumlah'] . " " . $d['satuan'] . "</small></td>
					<td id='harga' style='display: none;'>" . $harga_non_racikan . "</td>
					<td id='f_harga'><small><div align='right'>" . ArrayAdapter::format("only-money", $harga_non_racikan) . "</div></small></td>
					<td id='embalase' style='display: none;'>" . $embalase_non_racikan . "</td>
					<td id='f_embalase'><small><div align='right'>" . ArrayAdapter::format("only-money", $embalase_non_racikan) . "</div></small></td>
					<td id='tuslah' style='display: none;'>" . $tuslah_non_racikan . "</td>
					<td id='f_tuslah'><small><div align='right'>" . ArrayAdapter::format("only-money", $tuslah_non_racikan) . "</div></small></td>
					<td id='biaya_racik' style='display: none;'>" . $biaya_racik_non_racikan . "</td>
					<td id='f_biaya_racik'><small><div align='right'>" . ArrayAdapter::format("only-money", $biaya_racik_non_racikan) . "</div></small></td>
					<td id='subtotal' style='display: none;'>" . $subtotal . "</td>
					<td id='f_subtotal'><small><div align='right'>" . ArrayAdapter::format("only-money", $subtotal) . "</div></small></td>
					<td id='aturan_pakai'><small>" . $d['aturan_pakai'] . "</small></td>
					<td id='id_apoteker' style='display: none;'></td>
					<td id='nama_apoteker'><small>-</small></td>
					<td class='edit_button'>" . $edit_button . "</td>
					<td class='del_button'>" . $delete_button . "</td>
				</tr>
			";
		}
	} 
	$detail_html .= "	</tbody>
						<tfoot>
							<tr>
								<td colspan='7'><strong><small><center>T O T A L</center></small></strong></td>
								<td id='total_resep'><strong><small><div align='right'>" . ArrayAdapter::format("only-money", $total_biaya) . "</div></small></strong></td>
								<td colspan='4'></td>
							</tr>
						</tfoot>
					</table>";
	$bahan_html .= "
			</tbody>
		</table>
	";

	$non_racikan_form = new Form("", "", "<small>Form. Obat Non-Racikan&Tab;<a href='#' onclick='racikan.show_form()' ><i class='fa fa-chevron-circle-right'></i></a></small>");
	$row_num_hidden = new Hidden("non_racikan_row_num", "non_racikan_row_num", "");
	$non_racikan_form->addElement("", $row_num_hidden);
	$id_obat_hidden = new Hidden("non_racikan_id_obat", "non_racikan_id_obat", "");
	$non_racikan_form->addElement("", $id_obat_hidden);
	$kode_obat_text = new Text("non_racikan_kode_obat", "non_racikan_kode_obat", "");
	$kode_obat_text->setAtribute("disabled='disabled'");
	$non_racikan_form->addElement("Kode Obat", $kode_obat_text);
	$name_obat_hidden = new Hidden("non_racikan_name_obat", "non_racikan_name_obat", "");
	$non_racikan_form->addElement("", $name_obat_hidden);
	$nama_obat_text = new Text("non_racikan_nama_obat", "non_racikan_nama_obat", "");
	$nama_obat_text->setClass("smis-one-option-input");
	$obat_browse_button = new Button("", "", "Pilih");
	$obat_browse_button->setClass("btn-info");
	$obat_browse_button->setIsButton(Button::$ICONIC);
	$obat_browse_button->setIcon("icon-white ".Button::$icon_list_alt);
	$obat_browse_button->setAction("obat.chooser('obat', 'obat_button', 'obat', obat)");
	$obat_browse_button->setAtribute("id='obat_browse'");
	$obat_input_group = new InputGroup("");
	$obat_input_group->addComponent($nama_obat_text);
	$obat_input_group->addComponent($obat_browse_button);
	$non_racikan_form->addElement("Nama Obat", $obat_input_group);
	$nama_jenis_obat_text = new Text("non_racikan_nama_jenis_obat", "non_racikan_nama_jenis_obat", "");
	$nama_jenis_obat_text->setAtribute("disabled='disabled'");
	$non_racikan_form->addElement("Jenis Obat", $nama_jenis_obat_text);
	$sisa_text = new Text("non_racikan_sisa", "non_racikan_sisa", "");
	$sisa_text->setAtribute("disabled='disabled'");
	$non_racikan_form->addElement("Sisa", $sisa_text);
	$jumlah_text = new Text("non_racikan_jumlah", "non_racikan_jumlah", "");
	$non_racikan_form->addElement("Jumlah", $jumlah_text);
	$satuan_select = new Select("non_racikan_satuan", "non_racikan_satuan", "");
	$satuan_select->setAtribute("onchange", "obat.get_current_stock()");
	$non_racikan_form->addElement("Satuan", $satuan_select);
	$harga_text = new Text("non_racikan_harga", "non_racikan_harga", "");
	$harga_text->setTypical("money");
	$harga_text->setAtribute("data-thousands='.' data-decimal=',' data-prefix='Rp. '  data-precision='2' disabled='disabled'");
	$non_racikan_form->addElement("Harga", $harga_text);
	$aturan_pakai_text = new Text("non_racikan_aturan_pakai", "non_racikan_aturan_pakai", "");
	$non_racikan_form->addElement("Aturan Pakai", $aturan_pakai_text);
	$save_button = new Button("", "", "Tambahkan");
	$save_button->setClass("btn-info");
	$save_button->setIsButton(Button::$ICONIC_TEXT);
	$save_button->setIcon("fa fa-chevron-right");
	$save_button->setAction("non_racikan.save()");
	$save_button->setAtribute("id='non_racikan_save'");
	$update_button = new Button("", "", "Perbarui");
	$update_button->setClass("btn-info");
	$update_button->setIsButton(Button::$ICONIC_TEXT);
	$update_button->setIcon("fa fa-chevron-right");
	$update_button->setAction("non_racikan.save()");
	$update_button->setAtribute("id='non_racikan_update'");
	$cancel_button = new Button("", "", "Batal");
	$cancel_button->setClass("btn-danger");
	$cancel_button->setIsButton(Button::$ICONIC_TEXT);
	$cancel_button->setIcon("icon-white icon-remove");
	$cancel_button->setAction("non_racikan.cancel()");
	$cancel_button->setAtribute("id='non_racikan_cancel'");
	$button_group = new ButtonGroup("");
	$button_group->addButton($update_button);
	$button_group->addButton($cancel_button);
	$non_racikan_form->addElement("", $save_button);
	$non_racikan_form->addElement("", $button_group);

	$racikan_form = new Form("", "", "Depo Farmasi " . $depo_label . " : Form. Obat Racikan");
	$row_num_hidden = new Hidden("racikan_row_num", "racikan_row_num", "");
	$racikan_form->addElement("", $row_num_hidden);
	$label_text = new Text("racikan_label", "racikan_label", "");
	$label_text->setAtribute("disabled='disabled'");
	$racikan_form->addElement("Label", $label_text);
	$aturan_pakai_text = new Text("racikan_aturan_pakai", "racikan_aturan_pakai", "");
	$racikan_form->addElement("Aturan Pakai", $aturan_pakai_text);
	$id_apoteker_hidden = new Hidden("racikan_id_apoteker", "racikan_id_apoteker", "");
	$racikan_form->addElement("", $id_apoteker_hidden);
	$name_apoteker_hidden = new Hidden("racikan_name_apoteker", "racikan_name_apoteker", "");
	$racikan_form->addElement("", $name_apoteker_hidden);
	$nama_apoteker_text = new Text("racikan_nama_apoteker", "racikan_nama_apoteker", "");
	$nama_apoteker_text->setClass("smis-one-option-input");
	$apoteker_browse_button = new Button("", "", "Pilih");
	$apoteker_browse_button->setClass("btn-info");
	$apoteker_browse_button->setIsButton(Button::$ICONIC);
	$apoteker_browse_button->setIcon("icon-white ".Button::$icon_list_alt);
	$apoteker_browse_button->setAction("apoteker.chooser('apoteker', 'apoteker_button', 'apoteker', apoteker)");
	$apoteker_browse_button->setAtribute("id='apoteker_browse'");
	$apoteker_input_group = new InputGroup("");
	$apoteker_input_group->addComponent($nama_apoteker_text);
	$apoteker_input_group->addComponent($apoteker_browse_button);
	$racikan_form->addElement("Apoteker", $apoteker_input_group);

	$dracikan_form = new Form("", "", "<small>Form. Bahan Racikan</small>");
	$row_num_hidden = new Hidden("dracikan_row_num", "dracikan_row_num", "");
	$dracikan_form->addElement("", $row_num_hidden);
	$id_bahan_hidden = new Hidden("dracikan_id_bahan", "dracikan_id_bahan", "");
	$dracikan_form->addElement("", $id_bahan_hidden);
	$name_bahan_hidden = new Hidden("dracikan_name_bahan", "dracikan_name_bahan", "");
	$dracikan_form->addElement("", $name_bahan_hidden);
	$kode_bahan_text = new Text("dracikan_kode_bahan", "dracikan_kode_bahan", "");
	$kode_bahan_text->setAtribute("disabled='disabled'");
	$dracikan_form->addElement("Kode Bahan", $kode_bahan_text);
	$nama_bahan_text = new Text("dracikan_nama_bahan", "dracikan_nama_bahan", "");
	$nama_bahan_text->setClass("smis-one-option-input");
	$bahan_browse_button = new Button("", "", "Pilih");
	$bahan_browse_button->setClass("btn-info");
	$bahan_browse_button->setIsButton(Button::$ICONIC);
	$bahan_browse_button->setIcon("icon-white ".Button::$icon_list_alt);
	$bahan_browse_button->setAction("bahan.chooser('bahan', 'bahan_button', 'bahan', bahan)");
	$bahan_browse_button->setAtribute("id='bahan_browse'");
	$bahan_input_group = new InputGroup("");
	$bahan_input_group->addComponent($nama_bahan_text);
	$bahan_input_group->addComponent($bahan_browse_button);
	$dracikan_form->addElement("Nama Bahan", $bahan_input_group);
	$nama_jenis_bahan_text = new Text("dracikan_nama_jenis_bahan", "dracikan_nama_jenis_bahan", "");
	$nama_jenis_bahan_text->setAtribute("disabled='disabled'");
	$dracikan_form->addElement("Jenis Bahan", $nama_jenis_bahan_text);
	$sisa_text = new Text("dracikan_sisa", "dracikan_sisa", "");
	$sisa_text->setAtribute("disabled='disabled'");
	$dracikan_form->addElement("Sisa", $sisa_text);
	$jumlah_text = new Text("dracikan_jumlah", "dracikan_jumlah", "");
	$dracikan_form->addElement("Jumlah", $jumlah_text);
	$satuan_select = new Select("dracikan_satuan", "dracikan_satuan", "");
	$satuan_select->setAtribute("onchange", "bahan.get_current_stock()");
	$dracikan_form->addElement("Satuan", $satuan_select);
	$harga_text = new Text("dracikan_harga", "dracikan_harga", "Rp. 0,00");
	$harga_text->setAtribute("disabled='disabled'");
	$dracikan_form->addElement("Harga", $harga_text);
	$save_button = new Button("", "", "Tambahkan");
	$save_button->setClass("btn-info");
	$save_button->setIsButton(Button::$ICONIC_TEXT);
	$save_button->setIcon("fa fa-chevron-right");
	$save_button->setAction("dracikan.save()");
	$save_button->setAtribute("id='dracikan_save'");
	$update_button = new Button("", "", "Perbarui");
	$update_button->setClass("btn-info");
	$update_button->setIsButton(Button::$ICONIC_TEXT);
	$update_button->setIcon("fa fa-chevron-right");
	$update_button->setAction("dracikan.save()");
	$update_button->setAtribute("id='dracikan_update'");
	$cancel_button = new Button("", "", "Batal");
	$cancel_button->setClass("btn-danger");
	$cancel_button->setIsButton(Button::$ICONIC_TEXT);
	$cancel_button->setIcon("icon-white icon-remove");
	$cancel_button->setAction("dracikan.cancel()");
	$cancel_button->setAtribute("id='dracikan_cancel'");
	$button_group = new ButtonGroup("");
	$button_group->addButton($update_button);
	$button_group->addButton($cancel_button);
	$dracikan_form->addElement("", $save_button);
	$dracikan_form->addElement("", $button_group);

	$dracikan_html = "
		<table data-fix-header='n' class='table table-bordered table-hover table-striped table-condensed' id='table_dracikan'>
			<thead>
				<tr class='inverse dracikan_header_normal'>
					<th>No.</th>
					<th>Nama</th>
					<th>Jumlah</th>
					<th>Harga Satuan</th>
					<th>Embalase</th>
					<th>Tuslah</th>
					<th>Subtotal</th>
					<th>Ubah</th>
					<th>Hapus</th>
				</tr>
			</thead>
			<tbody id='dracikan_list'></tbody>
			<tfoot>
				<tr>
					<td colspan='6'><strong><small><div align='right'>Total I</div></small></strong></td>
					<td id='subtotal_racikan'><small><div align='right'>0,00</div></small></td>
					<td colspan='2'></td>
				</tr>
				<tr>
					<td colspan='6'><strong><small><div align='right'>Biaya Racik</div></small></strong></td>
					<td id='biaya_racik_racikan'><small><div align='right'>0,00</div></small></td>
					<td colspan='2'></td>
				</tr>
				<tr>
					<td colspan='6'><strong><small><div align='right'>Total II</div></small></strong></td>
					<td id='total_racikan'><small><div align='right'>0,00</div></small></td>
					<td colspan='2'></td>
				</tr>
			</tfoot>
		</table>";

	$racikan_button_group = new ButtonGroup("");
	$cancel_button = new Button("", "", "Kembali");
	$cancel_button->setClass("btn-inverse");
	$cancel_button->setIsButton(Button::$ICONIC_TEXT);
	$cancel_button->setIcon("fa fa-close");
	$cancel_button->setAction("racikan.cancel()");
	$cancel_button->setAtribute("id='racikan_back'");
	$racikan_button_group->addButton($cancel_button);
	$save_button = new Button("", "", "Simpan");
	$save_button->setClass("btn-success");
	$save_button->setIsButton(Button::$ICONIC_TEXT);
	$save_button->setIcon("fa fa-floppy-o");
	$save_button->setAction("racikan.save()");
	$save_button->setAtribute("id='racikan_save'");
	$racikan_button_group->addButton($save_button);

	$resep_button_group = new ButtonGroup("");
	$cancel_button = new Button("", "", "Kembali");
	$cancel_button->setClass("btn-inverse");
	$cancel_button->setIsButton(Button::$ICONIC_TEXT);
	$cancel_button->setIcon("fa fa-close");
	$cancel_button->setAction("resep.cancel()");
	$cancel_button->setAtribute("id='resep_back'");
	$resep_button_group->addButton($cancel_button);
	$save_button = new Button("", "", "Simpan");
	$save_button->setClass("btn-success");
	$save_button->setIsButton(Button::$ICONIC_TEXT);
	$save_button->setIcon("fa fa-floppy-o");
	$save_button->setAction("resep.save()");
	$save_button->setAtribute("id='resep_save'");
	$resep_button_group->addButton($save_button);

	echo "<div id='e_resep_form'>";
	echo 	$header_form->getHtml();
	echo 	"<div id='table_content'>" .
				"<div class='row-fluid'>";
	echo 			"<div id='non_racikan_form_section' class='span3'>" .
						$non_racikan_form->getHtml() .
					"</div>";
	echo 			"<div id='dresep_table_section' class='span9'>" .
						$detail_html .
					"</div>";
	echo 			"<div align='right'>" .
						$resep_button_group->getHtml() .
					"</div>";
	echo 		"</div>";
	echo 	"</div>";
	echo "</div>";
	echo "<div id='racikan_form_section' style='display: none;'>";
	echo 	$racikan_form->getHtml();
	echo 	"<div id='table_content'>" .
				"<div class='row-fluid'>";
	echo 			"<div id='dracikan_form_section' class='span3'>" .
						$dracikan_form->getHtml() .
					"</div>";
	echo 			"<div id='dresep_table_section' class='span9'>" .
						$dracikan_html .
					"</div>";
	echo 			"<div align='right'>" .
						$racikan_button_group->getHtml() .
					"</div>";
	echo 		"</div>";
	echo 	"</div>";
	echo "</div>";
	echo $bahan_html;
	echo addJS("framework/smis/js/table_action.js");
?>
<script type="text/javascript">
	function ResepAction(name, page, action, column) {
		this.initialize(name, page, action, column);
	}
	ResepAction.prototype.constructor = ResepAction;
	ResepAction.prototype = new TableAction();
	ResepAction.prototype.cancel = function() {
		var data = this.getRegulerData();
		data['action'] = "e_resep";
		data['kriteria'] = "";
		data['number'] = 0;
		data['max'] = 5;
		LoadSmisPage(data);
	};
	ResepAction.prototype.show_penjualan_resep = function() {
		var data = this.getRegulerData();
		data['action'] = "penjualan_resep";
		data['kriteria'] = "";
		data['number'] = 0;
		data['max'] = 5;
		LoadSmisPage(data);	
	};
	ResepAction.prototype.validate = function() {
		var valid = true;
		var invalid_msg = "";
		$(".error_field").removeClass("error_field");
		var diskon = parseFloat($("#resep_diskon").val().replace(/[^0-9-,]/g, '').replace(",", "."));
		var t_diskon = $("#resep_t_diskon").val();
		if (diskon > 100 && t_diskon == "persen") {
			valid = false;
			$("#resep_diskon").addClass("error_field");
			$("#resep_t_diskon").addClass("error_field");
			$("#resep_diskon").focus();
			invalid_msg += "</br><strong>Diskon</strong> tidak boleh melebihi 100%.";
		}
		var dresep_num_rows = $("#dresep_list tr").length;
		if (dresep_num_rows == 0) {
			valid = false;
			invalid_msg += "</br><strong>Detail Resep</strong> tidak boleh kosong.";
		}
		if (!valid)
			bootbox.alert(invalid_msg);
		return valid;
	};
	ResepAction.prototype.getSaveData = function() {
		var data = this.getRegulerData();
		data['super_command'] == "";
		data['command'] = "save_as_penjualan_resep";
		data['id'] = "";
		data['nomor_resep'] = "ER-" + $("#resep_id").val();
		data['id_dokter'] = $("#resep_id_dokter").val();
		data['nama_dokter'] = $("#resep_nama_dokter").val();
		data['noreg_pasien'] = $("#resep_noreg_pasien").val();
		data['nrm_pasien'] = $("#resep_nrm_pasien").val();
		data['nama_pasien'] = $("#resep_nama_pasien").val();
		data['alamat_pasien'] = $("#resep_alamat_pasien").val();
		data['jenis'] = $("#resep_jenis").val();
		data['perusahaan'] = $("#resep_perusahaan").val();
		data['asuransi'] = $("#resep_asuransi").val();
		data['ruangan'] = $("#resep_ruangan").val();
		data['markup'] = $("#resep_markup").val();
		data['embalase'] = $("#resep_embalase").val();
		data['tusla'] = $("#resep_tuslah").val();
		data['biaya_racik'] = $("#resep_biaya_racik").val();
		data['uri'] = $("#resep_uri").val();
		var v_total = parseFloat($("#total_resep").text().replace(/[^0-9-,]/g, '').replace(",", "."));
		data['total'] = v_total;
		data['diskon'] = $("#resep_diskon").val();
		data['t_diskon'] = $("#resep_t_diskon").val();
		var detail_resep = {};
		var nor_dresep =  $("tbody#dresep_list").children("tr").length;
		var pos = 0;
		for(var i = 0; i < nor_dresep; i++) {
			var tipe = $("#dresep_list tr:eq(" + i + ") td#is_racikan").text() == "1" ? "obat_racikan" : "obat_jadi";
			if (tipe == "obat_jadi") {
				var id_obat_jadi = "";
				var id_obat = $("#dresep_list tr:eq(" + i + ") td#id_obat").text();
				var kode_obat = $("#dresep_list tr:eq(" + i + ") td#kode_obat").text();
				var nama_obat = $("#dresep_list tr:eq(" + i + ") td#nama_obat").text();
				var nama_jenis_obat = $("#dresep_list tr:eq(" + i + ") td#nama_jenis_obat").text();
				var jumlah = $("#dresep_list tr:eq(" + i + ") td#jumlah").text();
				var jumlah_lama = 0;
				var satuan = $("#dresep_list tr:eq(" + i + ") td#satuan").text();
				var konversi = 1;
				var satuan_konversi = $("#dresep_list tr:eq(" + i + ") td#satuan").text();
				var v_harga = parseFloat($("#dresep_list tr:eq(" + i + ") td#harga").text());
				var v_embalase = parseFloat($("#dresep_list tr:eq(" + i + ") td#embalase").text());
				var v_tusla = parseFloat($("#dresep_list tr:eq(" + i + ") td#tuslah").text());
				var v_biaya_racik = parseFloat($("#dresep_list tr:eq(" + i + ") td#biaya_racik").text());
				var v_subtotal = parseFloat($("#dresep_list tr:eq(" + i + ") td#subtotal").text());
				var aturan_pakai = $("#dresep_list tr:eq(" + i + ") td#aturan_pakai").text();
				var d_data = {};
				d_data['tipe'] = tipe;
				d_data['id_obat_jadi'] = id_obat_jadi;
				d_data['id_obat'] = id_obat;
				d_data['kode_obat'] = kode_obat;
				d_data['nama_obat'] = nama_obat;
				d_data['nama_jenis_obat'] = nama_jenis_obat;
				d_data['jumlah'] = jumlah;
				d_data['jumlah_lama'] = jumlah_lama;
				d_data['satuan'] = satuan;
				d_data['konversi'] = konversi;
				d_data['satuan_konversi'] = satuan_konversi;
				d_data['harga'] = v_harga;
				d_data['embalase'] = v_embalase;
				d_data['tusla'] = v_tusla;
				d_data['biaya_racik'] = v_biaya_racik;
				d_data['subtotal'] = v_subtotal;
				d_data['aturan_pakai'] = aturan_pakai;
				d_data['pos'] = pos++;
				detail_resep[i] = d_data;
			} else if (tipe == "obat_racikan") {
				var id_obat_racikan = "";
				var nama = $("#dresep_list tr:eq(" + i + ") td#nama_racikan").text();
				var id_apoteker = $("#dresep_list tr:eq(" + i + ") td#id_apoteker").text();
				var nama_apoteker = $("#dresep_list tr:eq(" + i + ") td#nama_apoteker").text();
				var jumlah_racikan = $("#dresep_list tr:eq(" + i + ") td#jumlah").text();
				var v_embalase = parseFloat($("#dresep_list tr:eq(" + i + ") td#embalase").text());
				var v_tusla = parseFloat($("#dresep_list tr:eq(" + i + ") td#tuslah").text());
				var v_biaya_racik = parseFloat($("#dresep_list tr:eq(" + i + ") td#biaya_racik").text());
				var v_harga = parseFloat($("#dresep_list tr:eq(" + i + ") td#harga").text());
				var v_subtotal = parseFloat($("#dresep_list tr:eq(" + i + ") td#subtotal").text());
				var aturan_pakai = $("#dresep_list tr:eq(" + i + ") td#aturan_pakai").text();
				var detail_racikan = {};
				var nor_bahan = $("#r_bahan_list tr." + i).length;
				for(var j = 0; j < nor_bahan; j++) {
					var id_bahan_pakai = "";
					var id_obat = $("#r_bahan_list tr:eq(" + j + ") td#id_obat").text();
					var kode_obat = $("#r_bahan_list tr:eq(" + j + ") td#kode_obat").text();
					var nama_obat = $("#r_bahan_list tr:eq(" + j + ") td#nama_obat").text();
					var nama_jenis_obat = $("#r_bahan_list tr:eq(" + j + ") td#nama_jenis_obat").text();
					var jumlah = $("#r_bahan_list tr:eq(" + j + ") td#jumlah").text();
					var satuan = $("#r_bahan_list tr:eq(" + j + ") td#satuan").text();
					var konversi = $("#r_bahan_list tr:eq(" + j + ") td#konversi").text();
					var satuan_konversi = $("#r_bahan_list tr:eq(" + j + ") td#satuan_konversi").text();
					var harga = parseFloat($("#r_bahan_list tr:eq(" + j + ") td#harga").text());
					var embalase = parseFloat($("#r_bahan_list tr:eq(" + j + ") td#embalase").text());
					var tusla = parseFloat($("#r_bahan_list tr:eq(" + j + ") td#tuslah").text());
					var markup = parseFloat($("#resep_markup").val());
					var b_data = {};
					b_data['id'] = id_bahan_pakai;
					b_data['id_obat'] = id_obat;
					b_data['kode_obat'] = kode_obat;
					b_data['nama_obat'] = nama_obat;
					b_data['nama_jenis_obat'] = nama_jenis_obat;
					b_data['jumlah'] = jumlah;
					b_data['satuan'] = satuan;
					b_data['konversi'] = konversi;
					b_data['satuan_konversi'] = satuan_konversi;
					b_data['harga'] = harga;
					b_data['embalase'] = embalase;
					b_data['tusla'] = tusla;
					detail_racikan[j] = b_data;
				}
				var d_data = {};
				d_data['tipe'] = tipe;
				d_data['nama'] = nama;
				d_data['id_apoteker'] = id_apoteker;
				d_data['nama_apoteker'] = nama_apoteker;
				d_data['jumlah'] = jumlah_racikan;
				d_data['harga'] = v_harga;
				d_data['total_embalase'] = v_embalase;
				d_data['total_tusla'] = v_tusla;
				d_data['biaya_racik'] = v_biaya_racik;
				d_data['subtotal'] = v_subtotal;
				d_data['aturan_pakai'] = aturan_pakai;
				d_data['pos'] = pos++;
				d_data['detail_racikan'] = detail_racikan;
				detail_resep[i] = d_data;
			}
		}
		data['detail_resep'] = detail_resep;
		return data;
	};
	ResepAction.prototype.save = function() {
		if (!this.validate())
			return;
		var self = this;
		var data = this.getSaveData();
		showLoading();
		$.post(
			"",
			data,
			function(response) {
				var json = getContent(response);				
				if (json == null || json.success == 0) {
					dismissLoading();
					bootbox.alert(json.message);
				} else {
					dismissLoading();
					self.show_penjualan_resep();
				}
			}
		);
	};

	function DResepAction(name, page, action, column) {
		this.initialize(name, page, action, column);
	}
	DResepAction.prototype.constructor = DResepAction;
	DResepAction.prototype = new TableAction();
	DResepAction.prototype.edit_racikan = function(r_num) {
		var self = this;
		var nama_racikan = $("#dresep_list tr:eq(" + r_num + ") td#nama_racikan").text();
		var aturan_pakai = $("#dresep_list tr:eq(" + r_num + ") td#aturan_pakai").text();
		var id_apoteker = $("#dresep_list tr:eq(" + r_num + ") td#id_apoteker").text();
		var nama_apoteker = $("#dresep_list tr:eq(" + r_num + ") td#nama_apoteker").text();

		$("#racikan_row_num").val(r_num);
		$("#racikan_label").val(nama_racikan);
		$("#racikan_aturan_pakai").val(aturan_pakai);
		$("#racikan_id_apoteker").val(id_apoteker);
		$("#racikan_name_apoteker").val(nama_apoteker);
		$("#racikan_nama_apoteker").val(nama_apoteker);
		$("#biaya_racik_racikan").html("<small><div align='right'>" + parseFloat($("#resep_biaya_racik").val()).formatMoney("2", ".", ",") + "</div></small>");

		$("#dracikan_list").html("");
		dracikan_row_num = $("#r_bahan_list tr." + r_num).length;
		var subtotal_racikan = 0;
		for (var i = 0; i < dracikan_row_num; i++) {
			var id_obat = $("#r_bahan_list tr." + r_num + ":eq(" + i + ") td#id_obat").text();
			var kode_obat = $("#r_bahan_list tr." + r_num + ":eq(" + i + ") td#kode_obat").text();
			var nama_obat = $("#r_bahan_list tr." + r_num + ":eq(" + i + ") td#nama_obat").text();
			var nama_jenis_obat = $("#r_bahan_list tr." + r_num + ":eq(" + i + ") td#nama_jenis_obat").text();
			var jumlah = $("#r_bahan_list tr." + r_num + ":eq(" + i + ") td#jumlah").text();
			var satuan = $("#r_bahan_list tr." + r_num + ":eq(" + i + ") td#satuan").text();
			var f_jumlah = jumlah + " " + satuan; 
			var harga = $("#r_bahan_list tr." + r_num + ":eq(" + i + ") td#harga").text();
			var f_harga = parseFloat(harga).formatMoney("2", ".", ",");
			var embalase = $("#r_bahan_list tr." + r_num + ":eq(" + i + ") td#embalase").text();
			var f_embalase = parseFloat(embalase).formatMoney("2", ".", ",");
			var tuslah = $("#r_bahan_list tr." + r_num + ":eq(" + i + ") td#tuslah").text();
			var f_tuslah = parseFloat(tuslah).formatMoney("2", ".", ",");
			var subtotal = parseFloat(jumlah) * parseFloat(harga) + parseFloat(embalase) + parseFloat(tuslah);
			subtotal_racikan += subtotal;
			var f_subtotal = parseFloat(subtotal).formatMoney("2", ".", ",");
			
			var edit_button = "<center><a href='#' onclick='dracikan.edit(" + i + ")' class='input btn btn-warning'><i class='fa fa-pencil'></i></a></center>";
			var delete_button = "<center><a href='#' onclick='dracikan.delete(" + i + ")' class='input btn btn-danger'><i class='fa fa-trash-o'></i></a></center>";

			$("#dracikan_list").append(
				"<tr id='" + i + "'>" +
					"<td id='nomor'><small>" + (i + 1) + "</small></td>" +
					"<td id='bahan_r_num' style='display: none;'>" + i + "</td>" +
					"<td id='id_obat' style='display: none;'>" + id_obat + "</td>" +
					"<td id='kode_obat' style='display: none;'>" + kode_obat + "</td>" +
					"<td id='nama_obat'><small>" + nama_obat + "</small></td>" +
					"<td id='nama_jenis_obat' style='display: none;'>" + nama_jenis_obat + "</td>" +
					"<td id='jumlah' style='display: none;'>" + jumlah + "</td>" +
					"<td id='satuan' style='display: none;'>" + satuan + "</td>" +
					"<td id='f_jumlah'><small>" + f_jumlah + "</small></td>" +
					"<td id='harga' style='display: none;'>" + harga + "</td>" +
					"<td id='f_harga'><small><div align='right'>" + f_harga + "</div></small></td>" +
					"<td id='embalase' style='display: none;'>" + embalase + "</td>" +
					"<td id='f_embalase'><small><div align='right'>" + f_embalase + "</div></small></td>" +
					"<td id='tuslah' style='display: none;'>" + tuslah + "</td>" +
					"<td id='f_tuslah'><small><div align='right'>" + f_tuslah + "</div></small></td>" +
					"<td id='subtotal' style='display: none;'>" + subtotal + "</td>" +
					"<td id='f_subtotal'><small><div align='right'>" + f_subtotal + "</div></small></td>" +
					"<td>" + edit_button + "</td>" +
					"<td>" + delete_button + "</td>" +
				"</tr>"
			);
		}
		var f_subtotal_racikan = parseFloat(subtotal_racikan).formatMoney("2", ".", ".");
		$("#subtotal_racikan").html("<small><div align='right'>" + f_subtotal_racikan + "</div></small>");
		var total_racikan = parseFloat(subtotal_racikan) + parseFloat($("#resep_biaya_racik").val());
		var f_total_racikan = parseFloat(total_racikan).formatMoney("2", ".", ",");
		$("#total_racikan").html("<small><div align='right'>" + f_total_racikan + "</div></small>");

		racikan.show_form();
	};
	DResepAction.prototype.edit_non_racikan = function(r_num) {
		var self = this;
		var id_obat = $("#dresep_list tr:eq(" + r_num + ") td#id_obat").text();
		var kode_obat = $("#dresep_list tr:eq(" + r_num + ") td#kode_obat").text();
		var nama_obat = $("#dresep_list tr:eq(" + r_num + ") td#nama_obat").text();
		var nama_jenis_obat = $("#dresep_list tr:eq(" + r_num + ") td#nama_jenis_obat").text();
		var jumlah = $("#dresep_list tr:eq(" + r_num + ") td#jumlah").text();
		var satuan = $("#dresep_list tr:eq(" + r_num + ") td#satuan").text();
		var harga = "Rp. " + parseFloat($("#dresep_list tr:eq(" + r_num + ") td#harga").text()).formatMoney("2", ".", ",");
		var aturan_pakai = $("#dresep_list tr:eq(" + r_num + ") td#aturan_pakai").text();
		var data = this.getRegulerData();
		data['super_command'] = "obat";
		data['command'] = "edit";
		data['id'] = id_obat;
		showLoading();
		$.post(
			"",
			data,
			function(response) {
				var json = getContent(response);
				if (json == null) {
					dismissLoading();
					return;
				}
				$("#non_racikan_row_num").val(r_num);
				$("#non_racikan_id_obat").val(json.header.id_obat);
				$("#non_racikan_kode_obat").val(json.header.kode_obat);
				$("#non_racikan_name_obat").val(json.header.nama_obat);
				$("#non_racikan_nama_obat").val(json.header.nama_obat);
				$("#non_racikan_nama_jenis_obat").val(json.header.nama_jenis_obat);
				$("#non_racikan_jumlah").val(jumlah);
				$("#non_racikan_satuan").html(json.satuan_option);
				$("#non_racikan_satuan").val("1_" + satuan);
				$("#non_racikan_harga").val(harga);
				$("#non_racikan_aturan_pakai").val(aturan_pakai);
				data = self.getRegulerData();
				data['super_command'] = "";
				data['command'] = "get_current_stock";
				data['id_obat'] = json.header.id_obat;
				data['satuan'] = satuan;
				$.post(
					"",
					data,
					function(response) {
						json = JSON.parse(response);
						if (json == null) {
							dismissLoading();
							return;
						}
						$("#non_racikan_sisa").val(json.sisa);
						$("#non_racikan_jumlah").focus();
						non_racikan.edit_mode();
						dismissLoading();
					}
				);
			}
		);
	};
	DResepAction.prototype.delete_non_racikan = function(r_num) {
		$("#dresep_list tr:eq(" + r_num + ")").remove();
		this.refreshTotal();
		if ($("#non_racikan_row_num").val() == r_num)
			non_racikan.clear();
		if ($("#racikan_row_num").val() == r_num)
			racikan.clear();
	};
	DResepAction.prototype.refreshTotal = function() {
		var num_rows = $("#dresep_list tr").length;
		var number = 1;
		var total = 0;
		for (var i = 0; i < num_rows; i++, number++) {
			$("#dresep_list tr:eq(" + i + ") td#nomor").html("<small>" + number + "</small>");
			var edit_button = "<center><a href='#' onclick='dresep.edit_non_racikan(" + i + ")' class='input btn btn-warning'><i class='fa fa-pencil'></i></a></center>";
			var delete_button = "<center><a href='#' onclick='dresep.delete_non_racikan(" + i + ")' class='input btn btn-danger'><i class='fa fa-trash-o'></i></a></center>";
			if ($("#dresep_list tr:eq(" + i + ") td#is_racikan").text() == "1") {
				edit_button = "<center><a href='#' onclick='dresep.edit_racikan(" + i + ")' class='input btn btn-warning'><i class='fa fa-pencil'></i></a></center>";
				delete_button = "<center><a href='#' onclick='dresep.delete_racikan(" + i + ")' class='input btn btn-danger'><i class='fa fa-trash-o'></i></a></center>";
			}
			$("#dresep_list tr:eq(" + i + ") td.edit_button").html(edit_button);
			$("#dresep_list tr:eq(" + i + ") td.del_button").html(delete_button);
			var subtotal = parseFloat($("#dresep_list tr:eq(" + i + ") td#subtotal").text());
			total += subtotal;
		}
		$(".no_num").attr("class", parseFloat(num_rows - 1));
		$("#total_resep").html("<strong><small><div align='right'>" + total.formatMoney("2",  ".",  ",") + "</div></small></strong>");
	};

	function NonRacikanAction(name, page, action, column) {
		this.initialize(name, page, action, column);
	}
	NonRacikanAction.prototype.constructor = NonRacikanAction;
	NonRacikanAction.prototype = new TableAction();
	NonRacikanAction.prototype.hide_form = function() {
		$("#non_racikan_form_section").hide();
		$("#dresep_table_section").removeClass("span9");
	};
	NonRacikanAction.prototype.show_form = function() {
		$("#non_racikan_form_section").show();
		$("#dresep_table_section").addClass("span9");
	};
	NonRacikanAction.prototype.clear = function() {
		$("#non_racikan_row_num").val("");
		$("#non_racikan_id_obat").val("");
		$("#non_racikan_kode_obat").val("");
		$("#non_racikan_name_obat").val("");
		$("#non_racikan_nama_obat").val("");
		$("#non_racikan_nama_jenis_obat").val("");
		$("#non_racikan_sisa").val("");
		$("#non_racikan_jumlah").val("");
		$("#non_racikan_satuan").html("");
		$("#non_racikan_harga").val("");
		$("#non_racikan_aturan_pakai").val("");
	};
	NonRacikanAction.prototype.add_mode = function() {
		$("#non_racikan_nama_obat").addClass("smis-one-option-input");
		$("#non_racikan_nama_obat").removeAttr("disabled");
		$("#obat_browse").show();
		$("#non_racikan_save").show();
		$("#non_racikan_update").hide();
		$("#non_racikan_cancel").hide();
	};
	NonRacikanAction.prototype.edit_mode = function() {
		$("#non_racikan_nama_obat").removeClass("smis-one-option-input");
		$("#non_racikan_nama_obat").attr("disabled", "disabled");
		$("#obat_browse").hide();
		$("#non_racikan_save").hide();
		$("#non_racikan_update").show();
		$("#non_racikan_cancel").show();
	};
	NonRacikanAction.prototype.cancel = function() {
		this.clear();
		this.add_mode();
	};
	NonRacikanAction.prototype.validate = function() {
		var valid = true;
		var invalid_msg = "";
		$(".error_field").removeClass("error_field");
		var id_obat = $("#non_racikan_id_obat").val();
		var jumlah = $("#non_racikan_jumlah").val();
		var sisa = $("#non_racikan_sisa").val();
		var satuan = $("#non_racikan_satuan option:selected").text();
		var aturan_pakai = $("#non_racikan_aturan_pakai").val();
		if (id_obat == "") {
			valid = false;
			invalid_msg += "</br><strong>Obat</strong> tidak boleh kosong.";
			$("#non_racikan_nama_obat").addClass("error_field");
		}
		if (jumlah == "") {
			valid = false;
			invalid_msg += "</br><strong>Jumlah</strong> tidak boleh kosong.";
			$("#non_racikan_jumlah").addClass("error_field");
		} else if (!is_numeric(jumlah)) {
			valid = false;
			invalid_msg += "</br><strong>Jumlah</strong> seharusnya numerik (0-9).";
			$("#non_racikan_jumlah").addClass("error_field");
		} else if (parseFloat(jumlah) > parseFloat(sisa)) {
			valid = false;
			invalid_msg += "</br><strong>Jumlah</strong> tidak boleh melebihi <strong>Sisa</strong>.";
			$("#non_racikan_jumlah").addClass("error_field");
		}
		if (aturan_pakai == "") {
			valid = false;
			invalid_msg += "</br><strong>Aturan Pakai</strong> tidak boleh kosong.";
			$("#non_racikan_aturan_pakai").addClass("error_field");
		}
		if (!valid)
			bootbox.alert(invalid_msg);
		return valid;
	};
	NonRacikanAction.prototype.save = function() {
		if (!this.validate())
			return;
		var r_num = $("#non_racikan_row_num").val();
		var is_racikan = 0;
		var id_obat = $("#non_racikan_id_obat").val();
		var kode_obat = $("#non_racikan_kode_obat").val();
		var nama_obat = $("#non_racikan_nama_obat").val();
		var nama_jenis_obat = $("#non_racikan_nama_jenis_obat").val();
		var jumlah = $("#non_racikan_jumlah").val();
		var satuan = $("#non_racikan_satuan option:selected").text();
		var konversi = 1;
		var satuan_konversi = satuan;
		var f_jumlah = jumlah + " " + satuan;
		var harga = $("#non_racikan_harga").val().replace(/[^0-9-,]/g, '').replace(",", ".");
		var f_harga = parseFloat(harga).formatMoney("2", ".", ",");
		var embalase = $("#resep_embalase").val();
		var f_embalase = parseFloat(embalase).formatMoney("2", ".", ",");
		var tuslah = $("#resep_tuslah").val();
		var f_tuslah = parseFloat(tuslah).formatMoney("2", ".", ",");
		var biaya_racik = 0;
		var f_biaya_racik = parseFloat(biaya_racik).formatMoney("2", ".", ",");
		var subtotal = parseFloat(jumlah) * parseFloat(harga) + parseFloat(embalase) + parseFloat(tuslah) + parseFloat(biaya_racik);
		var f_subtotal = parseFloat(subtotal).formatMoney("2", ".", ",");
		var aturan_pakai = $("#non_racikan_aturan_pakai").val();
		var id_apoteker = "";
		var nama_apoteker = "-";
		if (r_num == "") {
			$("#dresep_list").append(
				"<tr>" +
					"<td id='nomor'></td>" +
					"<td id='is_racikan' style='display: none;'>" + is_racikan + "</td>" +
					"<td id='id_obat' style='display: none;'>" + id_obat + "</td>" +
					"<td id='kode_obat' style='display: none;'><small>" + kode_obat + "</small></td>" +
					"<td id='nama_obat'><small>" + nama_obat + "</small></td>" +
					"<td id='nama_jenis_obat' style='display: none;'><small>" + nama_jenis_obat + "</small></td>" +
					"<td id='jumlah' style='display: none;'>" + jumlah + "</td>" +
					"<td id='satuan' style='display: none;'>" + satuan + "</td>" +
					"<td id='konversi' style='display: none;'>" + konversi + "</td>" +
					"<td id='satuan_konversi' style='display: none;'>" + satuan_konversi + "</td>" +
					"<td id='f_jumlah'><small>" + f_jumlah + "</small></td>" +
					"<td id='harga' style='display: none;'>" + harga + "</td>" +
					"<td id='f_harga'><small><div align='right'>" + f_harga + "</div></small></td>" +
					"<td id='embalase' style='display: none;'>" + embalase + "</td>" +
					"<td id='f_embalase'><small><div align='right'>" + f_embalase + "</div></small></td>" +
					"<td id='tuslah' style='display: none;'>" + tuslah + "</td>" +
					"<td id='f_tuslah'><small><div align='right'>" + f_tuslah + "</div></small></td>" +
					"<td id='biaya_racik' style='display: none;'>" + biaya_racik + "</td>" +
					"<td id='f_biaya_racik'><small><div align='right'>" + f_biaya_racik + "</div></small></td>" +
					"<td id='subtotal' style='display: none;'>" + subtotal + "</td>" +
					"<td id='f_subtotal'><small><div align='right'>" + f_subtotal + "</div></small></td>" +
					"<td id='aturan_pakai'><small>" + aturan_pakai + "</small></td>" +
					"<td id='id_apoteker' style='display: none;'>" + id_apoteker + "</td>" +
					"<td id='nama_apoteker'><small>" + nama_apoteker + "</small></td>" +
					"<td class='edit_button'></td>" +
					"<td class='del_button'></td>" +
				"</tr>"
			);
		} else {
			$("#dresep_list tr:eq(" + r_num + ") td#id_obat").html(id_obat);
			$("#dresep_list tr:eq(" + r_num + ") td#kode_obat").html(kode_obat);
			$("#dresep_list tr:eq(" + r_num + ") td#nama_obat").html("<small>" + nama_obat + "</small>");
			$("#dresep_list tr:eq(" + r_num + ") td#nama_jenis_obat").html("<small>" + nama_jenis_obat + "</small>");
			$("#dresep_list tr:eq(" + r_num + ") td#jumlah").html(jumlah);
			$("#dresep_list tr:eq(" + r_num + ") td#satuan").html(satuan);
			$("#dresep_list tr:eq(" + r_num + ") td#konversi").html(konversi);
			$("#dresep_list tr:eq(" + r_num + ") td#satuan_konversi").html(satuan_konversi);
			$("#dresep_list tr:eq(" + r_num + ") td#f_jumlah").html("<small>" + f_jumlah + "</small>");
			$("#dresep_list tr:eq(" + r_num + ") td#harga").html(harga);
			$("#dresep_list tr:eq(" + r_num + ") td#hf_arga").html("<small><div align='right'>" + f_harga + "</div></small>");
			$("#dresep_list tr:eq(" + r_num + ") td#embalase").html(embalase);
			$("#dresep_list tr:eq(" + r_num + ") td#f_embalase").html("<small><div align='right'>" + f_embalase + "</div></small>");
			$("#dresep_list tr:eq(" + r_num + ") td#tuslah").html(tuslah);
			$("#dresep_list tr:eq(" + r_num + ") td#f_tuslah").html("<small><div align='right'>" + f_tuslah + "</div></small>");
			$("#dresep_list tr:eq(" + r_num + ") td#biaya_racik").html(biaya_racik);
			$("#dresep_list tr:eq(" + r_num + ") td#f_biaya_racik").html("<small><div align='right'>" + f_biaya_racik + "</div></small>");
			$("#dresep_list tr:eq(" + r_num + ") td#subtotal").html(subtotal);
			$("#dresep_list tr:eq(" + r_num + ") td#f_subtotal").html("<small><div align='right'>" + f_subtotal + "</div></small>");
			$("#dresep_list tr:eq(" + r_num + ") td#aturan_pakai").html("<small>" + aturan_pakai + "</small>");
		}
		this.clear();
		this.add_mode();
		dresep.refreshTotal();
	};

	function RacikanAction(name, page, action, column) {
		this.initialize(name, page, action, column);
	}
	RacikanAction.prototype.constructor = RacikanAction;
	RacikanAction.prototype = new TableAction();
	RacikanAction.prototype.hide_form = function() {
		$("#racikan_form_section").hide();
		$("#e_resep_form").show();
	};
	RacikanAction.prototype.show_form = function() {
		$("#racikan_form_section").show();
		$("#e_resep_form").hide();
	};
	RacikanAction.prototype.clear = function() {
		$("#racikan_row_num").val("");
		$("#racikan_label").val("");
		$("#racikan_aturan_pakai").val("");
		$("#racikan_id_apoteker").val("");
		$("#racikan_name_apoteker").val("");
		$("#racikan_nama_apoteker").val("");
		$("#dracikan_list").html("");
		$("#subtotal_racikan").html("<small><div align='right'>0,00</div></small>");
		$("#biaya_racik_racikan").html("<small><div align='right'>0,00</div></small>");
		$("#total_racikan").html("<small><div align='right'>0,00</div></small>");
	};
	RacikanAction.prototype.cancel = function() {
		dracikan.clear();
		dracikan.add_mode();
		this.clear();
		this.hide_form();
	};
	RacikanAction.prototype.validate = function() {
		var valid = true;
		var invalid_msg = "";
		$(".error_field").removeClass("error_field");
		var aturan_pakai = $("#racikan_aturan_pakai").val();
		var jumlah_bahan = $("#dracikan_list tr").length;
		if (aturan_pakai == "") {
			valid = false;
			invalid_msg += "</br><strong>Aturan Pakai</strong> tidak boleh kosong.";
			$("#racikan_aturan_pakai").addClass("error_field");
		}
		if (jumlah_bahan == 0) {
			valid = false;
			invalid_msg += "</br><strong>Detail Bahan Racikan</strong> tidak boleh kosong.";
		} 
		if (!valid)
			bootbox.alert(invalid_msg);
		return valid;
	};
	RacikanAction.prototype.save = function() {
		if (!this.validate())
			return;
		var r_num = $("#racikan_row_num").val();
		var label = $("#racikan_label").val();
		var aturan_pakai = $("#racikan_aturan_pakai").val();
		var id_apoteker = $("#racikan_id_apoteker").val();
		var nama_apoteker = $("#racikan_name_apoteker").val();

		var embalase_racikan = 0;
		var tuslah_racikan = 0;
		var biaya_racik_racikan = $("#resep_biaya_racik").val();
		var harga_racikan = 0;

		if (r_num == "") {
			var last_racikan_num = 0;
			var num_rows = $("#dresep_list tr").length;
			for (var i = 0; i < num_rows; i++) {
				if ($("#dresep_list tr:eq(" + i + ") td#is_racikan").text() == "1") {
					var racikan_num = parseFloat($("#dresep_list tr:eq(" + i + ") td#nama_racikan").text().replace("RACIKAN #", ""));
					if (racikan_num > last_racikan_num)
						last_racikan_num = racikan_num;
				}
			}
			r_num = num_rows;
			label = "RACIKAN #" + parseFloat(last_racikan_num + 1);

			var num_rows = $("#dracikan_list tr").length;
			for (var i = 0; i < num_rows; i++) {
				var id_obat = $("#dracikan_list tr:eq(" + i + ") td#id_obat").text();
				var kode_obat = $("#dracikan_list tr:eq(" + i + ") td#kode_obat").text();
				var nama_obat = $("#dracikan_list tr:eq(" + i + ") td#nama_obat").text();
				var nama_jenis_obat = $("#dracikan_list tr:eq(" + i + ") td#nama_jenis_obat").text();
				var jumlah = $("#dracikan_list tr:eq(" + i + ") td#jumlah").text();
				var satuan = $("#dracikan_list tr:eq(" + i + ") td#satuan").text();
				var konversi = 1;
				var satuan_konversi = $("#dracikan_list tr:eq(" + i + ") td#satuan").text();
				var harga = $("#dracikan_list tr:eq(" + i + ") td#harga").text();
				var embalase = $("#dracikan_list tr:eq(" + i + ") td#embalase").text();
				var tuslah = $("#dracikan_list tr:eq(" + i + ") td#tuslah").text();

				$("#r_bahan_list").append(
					"<tr class='no_num'>" +
						"<td id='id_obat'>" + id_obat + "</td>" +
						"<td id='kode_obat'>" + kode_obat + "</td>" +
						"<td id='nama_obat'>" + nama_obat + "</td>" +
						"<td id='nama_jenis_obat'>" + nama_jenis_obat + "</td>" +
						"<td id='jumlah'>" + jumlah + "</td>" +
						"<td id='satuan'>" + satuan + "</td>" +
						"<td id='konversi'>" + konversi + "</td>" +
						"<td id='satuan_konversi'>" + satuan_konversi + "</td>" +
						"<td id='harga'>" + harga + "</td>" +
						"<td id='embalase'>" + embalase + "</td>" +
						"<td id='tuslah'>" + tuslah + "</td>" +
					"</tr>"
				);
				embalase_racikan = parseFloat(embalase_racikan) + parseFloat(embalase);
				tuslah_racikan = parseFloat(tuslah_racikan) + parseFloat(tuslah);
				harga_racikan = parseFloat(harga_racikan) + parseFloat(jumlah) * parseFloat(harga);
			}
			var subtotal_racikan = parseFloat(harga_racikan) + parseFloat(embalase_racikan) + parseFloat(tuslah_racikan) + parseFloat(biaya_racik_racikan);
			var f_subtotal_racikan = parseFloat(subtotal_racikan).formatMoney("2", ".", ",");
			var f_harga_racikan = parseFloat(harga_racikan).formatMoney("2", ".", ",");
			var f_embalase_racikan = parseFloat(embalase_racikan).formatMoney("2", ".", ",");
			var f_tuslah_racikan = parseFloat(tuslah_racikan).formatMoney("2", ".", ",");
			var f_biaya_racik_racikan = parseFloat(biaya_racik_racikan).formatMoney("2", ".", ",");
			$("#dresep_list").append(
				"<tr>" +
					"<td id='nomor'></td>" +
					"<td id='is_racikan' style='display: none;'>1</td>" +
					"<td id='nama_racikan'><small>" + label + "</small></td>" +
					"<td id='jumlah' style='display: none;'>1</td>" +
					"<td id='satuan' style='display: none;'>RACIKAN</td>" +
					"<td id='f_jumlah'><small>1 RACIKAN</small></td>" +
					"<td id='harga' style='display: none;'>" + harga_racikan + "</td>" +
					"<td id='f_harga'><small><div align='right'>" + f_harga_racikan + "</div></small></td>" +
					"<td id='embalase' style='display: none;'>" + embalase_racikan + "</td>" +
					"<td id='f_embalase'><small><div align='right'>" + f_embalase_racikan + "</div></small></td>" +
					"<td id='embalase' style='display: none;'>" + tuslah_racikan + "</td>" +
					"<td id='f_embalase'><small><div align='right'>" + f_tuslah_racikan + "</div></small></td>" +
					"<td id='biaya_racik' style='display: none;'>" + biaya_racik_racikan + "</td>" +
					"<td id='f_biaya_racik'><small><div align='right'>" + f_biaya_racik_racikan + "</div></small></td>" +
					"<td id='subtotal' style='display: none;'>" + subtotal_racikan + "</td>" +
					"<td id='f_subtotal'><small><div align='right'>" + f_subtotal_racikan + "</div></small></td>" +
					"<td id='aturan_pakai'><small>" + aturan_pakai + "</small></td>" +
					"<td id='id_apoteker' style='display: none;'>" + id_apoteker + "</td>" +
					"<td id='nama_apoteker'><small>" + nama_apoteker + "</small></td>" +
					"<td class='edit_button'></td>" +
					"<td class='del_button'></td>" +
				"</tr>"
			);
		} else {
			$("#r_bahan_list tr." + r_num).remove();
			var num_rows = $("#dracikan_list tr").length;
			for (var i = 0; i < num_rows; i++) {
				var id_obat = $("#dracikan_list tr:eq(" + i + ") td#id_obat").text();
				var kode_obat = $("#dracikan_list tr:eq(" + i + ") td#kode_obat").text();
				var nama_obat = $("#dracikan_list tr:eq(" + i + ") td#nama_obat").text();
				var nama_jenis_obat = $("#dracikan_list tr:eq(" + i + ") td#nama_jenis_obat").text();
				var jumlah = $("#dracikan_list tr:eq(" + i + ") td#jumlah").text();
				var satuan = $("#dracikan_list tr:eq(" + i + ") td#satuan").text();
				var konversi = 1;
				var satuan_konversi = $("#dracikan_list tr:eq(" + i + ") td#satuan").text();
				var harga = $("#dracikan_list tr:eq(" + i + ") td#harga").text();
				var embalase = $("#dracikan_list tr:eq(" + i + ") td#embalase").text();
				var tuslah = $("#dracikan_list tr:eq(" + i + ") td#tuslah").text();

				$("#r_bahan_list").append(
					"<tr class='" + r_num + "'>" +
						"<td id='id_obat'>" + id_obat + "</td>" +
						"<td id='kode_obat'>" + kode_obat + "</td>" +
						"<td id='nama_obat'>" + nama_obat + "</td>" +
						"<td id='nama_jenis_obat'>" + nama_jenis_obat + "</td>" +
						"<td id='jumlah'>" + jumlah + "</td>" +
						"<td id='satuan'>" + satuan + "</td>" +
						"<td id='konversi'>" + konversi + "</td>" +
						"<td id='satuan_konversi'>" + satuan_konversi + "</td>" +
						"<td id='harga'>" + harga + "</td>" +
						"<td id='embalase'>" + embalase + "</td>" +
						"<td id='tuslah'>" + tuslah + "</td>" +
					"</tr>"
				);
				embalase_racikan = parseFloat(embalase_racikan) + parseFloat(embalase);
				tuslah_racikan = parseFloat(tuslah_racikan) + parseFloat(tuslah);
				harga_racikan = parseFloat(harga_racikan) + parseFloat(jumlah) * parseFloat(harga);
			}
			var subtotal_racikan = parseFloat(harga_racikan) + parseFloat(embalase_racikan) + parseFloat(tuslah_racikan) + parseFloat(biaya_racik_racikan);
			var f_subtotal_racikan = parseFloat(subtotal_racikan).formatMoney("2", ".", ",");
			var f_harga_racikan = parseFloat(harga_racikan).formatMoney("2", ".", ",");
			var f_embalase_racikan = parseFloat(embalase_racikan).formatMoney("2", ".", ",");
			var f_tuslah_racikan = parseFloat(tuslah_racikan).formatMoney("2", ".", ",");
			var f_biaya_racik_racikan = parseFloat(biaya_racik_racikan).formatMoney("2", ".", ",");
			$("#dresep_list tr:eq(" + r_num + ") td#harga").html(harga_racikan);
			$("#dresep_list tr:eq(" + r_num + ") td#f_harga").html("<small>" + f_harga_racikan + "</small>");
			$("#dresep_list tr:eq(" + r_num + ") td#embalase").html(embalase_racikan);
			$("#dresep_list tr:eq(" + r_num + ") td#f_embalase").html("<small>" + f_embalase_racikan + "</small>");
			$("#dresep_list tr:eq(" + r_num + ") td#tuslah").html(tuslah_racikan);
			$("#dresep_list tr:eq(" + r_num + ") td#f_tuslah").html("<small>" + f_tuslah_racikan + "</small>");
			$("#dresep_list tr:eq(" + r_num + ") td#biaya_racik").html(biaya_racik_racikan);
			$("#dresep_list tr:eq(" + r_num + ") td#f_biaya_racik").html("<small>" + f_biaya_racik_racikan + "</small>");
			$("#dresep_list tr:eq(" + r_num + ") td#subtotal").html(subtotal_racikan);
			$("#dresep_list tr:eq(" + r_num + ") td#f_subtotal").html("<small>" + f_subtotal_racikan + "</small>");
			$("#dresep_list tr:eq(" + r_num + ") td#aturan_pakai").html("<small>" + aturan_pakai + "</small>");
			$("#dresep_list tr:eq(" + r_num + ") td#id_apoteker").html(id_apoteker);
			$("#dresep_list tr:eq(" + r_num + ") td#nama_apoteker").html("<small>" + nama_apoteker + "</small>");
		}
		this.clear();
		this.hide_form();
		dresep.refreshTotal();
	};

	function DRacikanAction(name, page, action, column) {
		this.initialize(name, page, action, column);
	}
	DRacikanAction.prototype.constructor = DRacikanAction;
	DRacikanAction.prototype = new TableAction();
	DRacikanAction.prototype.add_mode = function() {
		$("#dracikan_nama_bahan").addClass("smis-one-option-input");
		$("#dracikan_nama_bahan").removeAttr("disabled");
		$("#bahan_browse").show();
		$("#dracikan_save").show();
		$("#dracikan_update").hide();
		$("#dracikan_cancel").hide();
	};
	DRacikanAction.prototype.edit_mode = function() {
		$("#dracikan_nama_bahan").removeClass("smis-one-option-input");
		$("#dracikan_nama_bahan").attr("disabled", "disabled");
		$("#bahan_browse").hide();
		$("#dracikan_save").hide();
		$("#dracikan_update").show();
		$("#dracikan_cancel").show();
	};
	DRacikanAction.prototype.clear = function() {
		$("#dracikan_row_num").val("");
		$("#dracikan_id_bahan").val("");
		$("#dracikan_kode_bahan").val("");
		$("#dracikan_name_bahan").val("");
		$("#dracikan_nama_bahan").val("");
		$("#dracikan_nama_jenis_bahan").val("");
		$("#dracikan_sisa").val("");
		$("#dracikan_jumlah").val("");
		$("#dracikan_satuan").html("");
		$("#dracikan_harga").val("Rp. 0,00");
	};
	DRacikanAction.prototype.refreshTotal = function() {
		var num_rows = $("#dracikan_list tr").length;
		var number = 1;
		var subtotal_racikan = 0;
		for (var i = 0; i < num_rows; i++, number++) {
			$("#dracikan_list tr:eq(" + i + ") td#nomor").html("<small>" + number + "</small>");
			var edit_button = "<center><a href='#' onclick='dracikan.edit(" + i + ")' class='input btn btn-warning'><i class='fa fa-pencil'></i></a></center>";
			var delete_button = "<center><a href='#' onclick='dracikan.delete(" + i + ")' class='input btn btn-danger'><i class='fa fa-trash-o'></i></a></center>";
			$("#dracikan_list tr:eq(" + i + ") td.edit_button").html(edit_button);
			$("#dracikan_list tr:eq(" + i + ") td.del_button").html(delete_button);
			var subtotal = parseFloat($("#dracikan_list tr:eq(" + i + ") td#subtotal").text());
			subtotal_racikan += subtotal;
		}
		$("#subtotal_racikan").html("<strong><small><div align='right'>" + subtotal_racikan.formatMoney("2",  ".",  ",") + "</div></small></strong>");
		var total = parseFloat(subtotal_racikan) + parseFloat($("#biaya_racik_racikan").text().replace(/[^0-9-,]/g, '').replace(",", "."));
		$("#total_racikan").html("<strong><small><div align='right'>" + total.formatMoney("2",  ".",  ",") + "</div></small></strong>");

	};
	DRacikanAction.prototype.edit = function(r_num) {
		var self = this;
		var id_obat = $("#dracikan_list tr:eq(" + r_num + ") td#id_obat").text();
		var kode_obat = $("#dracikan_list tr:eq(" + r_num + ") td#kode_obat").text();
		var nama_obat = $("#dracikan_list tr:eq(" + r_num + ") td#nama_obat").text();
		var nama_jenis_obat = $("#dracikan_list tr:eq(" + r_num + ") td#nama_jenis_obat").text();
		var jumlah = $("#dracikan_list tr:eq(" + r_num + ") td#jumlah").text();
		var satuan = $("#dracikan_list tr:eq(" + r_num + ") td#satuan").text();
		var harga = "Rp. " + parseFloat($("#dracikan_list tr:eq(" + r_num + ") td#harga").text()).formatMoney("2", ".", ",");
		var data = this.getRegulerData();
		data['super_command'] = "bahan";
		data['command'] = "edit";
		data['id'] = id_obat;
		showLoading();
		$.post(
			"",
			data,
			function(response) {
				var json = getContent(response);
				if (json == null) {
					dismissLoading();
					return;
				}
				$("#dracikan_row_num").val(r_num);
				$("#dracikan_id_bahan").val(json.header.id_obat);
				$("#dracikan_kode_bahan").val(json.header.kode_obat);
				$("#dracikan_name_bahan").val(json.header.nama_obat);
				$("#dracikan_nama_bahan").val(json.header.nama_obat);
				$("#dracikan_nama_jenis_bahan").val(json.header.nama_jenis_obat);
				$("#dracikan_jumlah").val(jumlah);
				$("#dracikan_satuan").html(json.satuan_option);
				$("#dracikan_satuan").val("1_" + satuan);
				$("#dracikan_harga").val(harga);
				data = self.getRegulerData();
				data['super_command'] = "";
				data['command'] = "get_current_stock";
				data['id_obat'] = json.header.id_obat;
				data['satuan'] = satuan;
				$.post(
					"",
					data,
					function(response) {
						json = JSON.parse(response);
						if (json == null) {
							dismissLoading();
							return;
						}
						$("#dracikan_sisa").val(json.sisa);
						$("#dracikan_jumlah").focus();
						self.edit_mode();
						dismissLoading();
					}
				);
			}
		);
	};
	DRacikanAction.prototype.cancel = function() {
		this.clear();
		this.add_mode();
	};
	DRacikanAction.prototype.validate = function() {
		var valid = true;
		var invalid_msg = "";
		$(".error_field").removeClass("error_field");
		var id_obat = $("#dracikan_id_obat").val();
		var jumlah = $("#dracikan_jumlah").val();
		var sisa = $("#dracikan_sisa").val();
		var satuan = $("#dracikan_satuan option:selected").text();
		if (id_obat == "") {
			valid = false;
			invalid_msg += "</br><strong>Obat</strong> tidak boleh kosong.";
			$("#dracikan_nama_obat").addClass("error_field");
		}
		if (jumlah == "") {
			valid = false;
			invalid_msg += "</br><strong>Jumlah</strong> tidak boleh kosong.";
			$("#dracikan_jumlah").addClass("error_field");
		} else if (!is_numeric(jumlah)) {
			valid = false;
			invalid_msg += "</br><strong>Jumlah</strong> seharusnya numerik (0-9).";
			$("#dracikan_jumlah").addClass("error_field");
		} else if (parseFloat(jumlah) > parseFloat(sisa)) {
			valid = false;
			invalid_msg += "</br><strong>Jumlah</strong> tidak boleh melebihi <strong>Sisa</strong>.";
			$("#dracikan_jumlah").addClass("error_field");
		}
		if (!valid)
			bootbox.alert(invalid_msg);
		return valid;
	};
	DRacikanAction.prototype.save = function() {
		if (!this.validate())
			return;
		var r_num = $("#dracikan_row_num").val();
		var is_racikan = 1;
		var id_obat = $("#dracikan_id_bahan").val();
		var kode_obat = $("#dracikan_kode_bahan").val();
		var nama_obat = $("#dracikan_nama_bahan").val();
		var nama_jenis_obat = $("#dracikan_nama_jenis_bahan").val();
		var jumlah = $("#dracikan_jumlah").val();
		var satuan = $("#dracikan_satuan option:selected").text();
		var f_jumlah = jumlah + " " + satuan;
		var harga = $("#dracikan_harga").val().replace(/[^0-9-,]/g, '').replace(",", ".");
		var f_harga = parseFloat(harga).formatMoney("2", ".", ",");
		var embalase = $("#resep_embalase").val();
		var f_embalase = parseFloat(embalase).formatMoney("2", ".", ",");
		var tuslah = $("#resep_tuslah").val();
		var f_tuslah = parseFloat(tuslah).formatMoney("2", ".", ",");
		var subtotal = parseFloat(jumlah) * parseFloat(harga) + parseFloat(embalase) + parseFloat(tuslah);
		var f_subtotal = parseFloat(subtotal).formatMoney("2", ".", ",");
		if (r_num == "") {
			$("#dracikan_list").append(
				"<tr>" +
					"<td id='nomor'></td>" +
					"<td id='bahan_r_num' style='display: none;'></td>" +
					"<td id='id_obat' style='display: none;'>" + id_obat + "</td>" +
					"<td id='kode_obat' style='display: none;'><small>" + kode_obat + "</small></td>" +
					"<td id='nama_obat'><small>" + nama_obat + "</small></td>" +
					"<td id='nama_jenis_obat' style='display: none;'><small>" + nama_jenis_obat + "</small></td>" +
					"<td id='jumlah' style='display: none;'>" + jumlah + "</td>" +
					"<td id='satuan' style='display: none;'>" + satuan + "</td>" +
					"<td id='f_jumlah'><small>" + f_jumlah + "</small></td>" +
					"<td id='harga' style='display: none;'>" + harga + "</td>" +
					"<td id='f_harga'><small><div align='right'>" + f_harga + "</div></small></td>" +
					"<td id='embalase' style='display: none;'>" + embalase + "</td>" +
					"<td id='f_embalase'><small><div align='right'>" + f_embalase + "</div></small></td>" +
					"<td id='tuslah' style='display: none;'>" + tuslah + "</td>" +
					"<td id='f_tuslah'><small><div align='right'>" + f_tuslah + "</div></small></td>" +
					"<td id='subtotal' style='display: none;'>" + subtotal + "</td>" +
					"<td id='f_subtotal'><small><div align='right'>" + f_subtotal + "</div></small></td>" +
					"<td class='edit_button'></td>" +
					"<td class='del_button'></td>" +
				"</tr>"
			);
			bahan_r_num++;
		} else {
			$("#dracikan_list tr:eq(" + r_num + ") td#id_obat").html(id_obat);
			$("#dracikan_list tr:eq(" + r_num + ") td#kode_obat").html(kode_obat);
			$("#dracikan_list tr:eq(" + r_num + ") td#nama_obat").html("<small>" + nama_obat + "</small>");
			$("#dracikan_list tr:eq(" + r_num + ") td#nama_jenis_obat").html("<small>" + nama_jenis_obat + "</small>");
			$("#dracikan_list tr:eq(" + r_num + ") td#jumlah").html(jumlah);
			$("#dracikan_list tr:eq(" + r_num + ") td#satuan").html(satuan);
			$("#dracikan_list tr:eq(" + r_num + ") td#f_jumlah").html("<small>" + f_jumlah + "</small>");
			$("#dracikan_list tr:eq(" + r_num + ") td#harga").html(harga);
			$("#dracikan_list tr:eq(" + r_num + ") td#hf_arga").html("<small><div align='right'>" + f_harga + "</div></small>");
			$("#dracikan_list tr:eq(" + r_num + ") td#embalase").html(embalase);
			$("#dracikan_list tr:eq(" + r_num + ") td#f_embalase").html("<small><div align='right'>" + f_embalase + "</div></small>");
			$("#dracikan_list tr:eq(" + r_num + ") td#tuslah").html(tuslah);
			$("#dracikan_list tr:eq(" + r_num + ") td#f_tuslah").html("<small><div align='right'>" + f_tuslah + "</div></small>");
			$("#dracikan_list tr:eq(" + r_num + ") td#subtotal").html(subtotal);
			$("#dracikan_list tr:eq(" + r_num + ") td#f_subtotal").html("<small><div align='right'>" + f_subtotal + "</div></small>");
		}
		this.clear();
		this.add_mode();
		this.refreshTotal();
	};
	DRacikanAction.prototype.delete = function(r_num) {
		$("#dracikan_list tr:eq(" + r_num + ")").remove();
		this.refreshTotal();
		if ($("#dracikan_row_num").val() == r_num)
			this.clear();
	};

	function ObatAction(name, page, action, column) {
		this.initialize(name, page, action, column);
	}
	ObatAction.prototype.constructor = ObatAction;
	ObatAction.prototype = new TableAction();
	ObatAction.prototype.selected = function(json) {
		$("#non_racikan_id_obat").val(json.header.id_obat);
		$("#non_racikan_kode_obat").val(json.header.kode_obat);
		$("#non_racikan_name_obat").val(json.header.nama_obat);
		$("#non_racikan_nama_obat").val(json.header.nama_obat);
		$("#non_racikan_nama_jenis_obat").val(json.header.nama_jenis_obat);
		$("#non_racikan_satuan").html(json.satuan_option);
		$("#non_racikan_satuan").val($("#non_racikan_satuan option:eq(0)").val());
		var data = this.getRegulerData();
		data['super_command'] = "";
		data['command'] = "get_current_stock";
		data['id_obat'] = json.header.id_obat;
		data['satuan'] = $("#non_racikan_satuan option:selected").text();
		showLoading();
		$.post(
			"",
			data,
			function(response) {
				json = JSON.parse(response);
				if (json == null) {
					dismissLoading();
					return;
				}
				$("#non_racikan_sisa").val(json.sisa);
				$("#non_racikan_harga").val("Rp. " + parseFloat(json.hja).formatMoney("2", ".", ","));
				$("#non_racikan_jumlah").focus();
				dismissLoading();
			}
		);
	};
	ObatAction.prototype.get_current_stock = function() {
		if ($("#non_racikan_id_obat").val() == "" || $("#non_racikan_satuan option:selected").text() == "")
			return;
		var data = this.getRegulerData();
		data['super_command'] = "";
		data['command'] = "get_current_stock";
		data['id_obat'] = $("#non_racikan_id_obat").val();
		data['satuan'] = $("#non_racikan_satuan option:selected").text();
		showLoading();
		$.post(
			"",
			data,
			function(response) {
				json = JSON.parse(response);
				if (json == null) {
					dismissLoading();
					return;
				}
				$("#non_racikan_sisa").val(json.sisa);
				$("#non_racikan_harga").val("Rp. " + parseFloat(json.hja).formatMoney("2", ".", ","));
				$("#non_racikan_jumlah").focus();
				dismissLoading();
			}
		);
	};

	function BahanAction(name, page, action, column) {
		this.initialize(name, page, action, column);
	}
	BahanAction.prototype.constructor = BahanAction;
	BahanAction.prototype = new TableAction();
	BahanAction.prototype.selected = function(json) {
		$("#dracikan_id_bahan").val(json.header.id_obat);
		$("#dracikan_kode_bahan").val(json.header.kode_obat);
		$("#dracikan_name_bahan").val(json.header.nama_obat);
		$("#dracikan_nama_bahan").val(json.header.nama_obat);
		$("#dracikan_nama_jenis_bahan").val(json.header.nama_jenis_obat);
		$("#dracikan_satuan").html(json.satuan_option);
		$("#dracikan_satuan").val($("#dracikan_satuan option:eq(0)").val());
		var data = this.getRegulerData();
		data['super_command'] = "";
		data['command'] = "get_current_stock";
		data['id_obat'] = json.header.id_obat;
		data['satuan'] = $("#dracikan_satuan option:selected").text();
		showLoading();
		$.post(
			"",
			data,
			function(response) {
				json = JSON.parse(response);
				if (json == null) {
					dismissLoading();
					return;
				}
				$("#dracikan_sisa").val(json.sisa);
				$("#dracikan_harga").val("Rp. " + parseFloat(json.hja).formatMoney("2", ".", ","));
				$("#dracikan_jumlah").focus();
				dismissLoading();
			}
		);
	};
	BahanAction.prototype.get_current_stock = function() {
		if ($("#dracikan_id_obat").val() == "" || $("#dracikan_satuan option:selected").text() == "")
			return;
		var data = this.getRegulerData();
		data['super_command'] = "";
		data['command'] = "get_current_stock";
		data['id_obat'] = $("#dracikan_id_bahan").val();
		data['satuan'] = $("#dracikan_satuan option:selected").text();
		showLoading();
		$.post(
			"",
			data,
			function(response) {
				json = JSON.parse(response);
				if (json == null) {
					dismissLoading();
					return;
				}
				$("#dracikan_sisa").val(json.sisa);
				$("#dracikan_harga").val("Rp. " + parseFloat(json.hja).formatMoney("2", ".", ","));
				$("#dracikan_jumlah").focus();
				dismissLoading();
			}
		);
	};

	function ApotekerAction(name, page, action, column) {
		this.initialize(name, page, action, column);
	}
	ApotekerAction.prototype.constructor = ApotekerAction;
	ApotekerAction.prototype = new TableAction();
	ApotekerAction.prototype.selected = function(json) {
		$("#racikan_id_apoteker").val(json.id);
		$("#racikan_name_apoteker").val(json.nama);
		$("#racikan_nama_apoteker").val(json.nama);
	};

	var non_racikan;
	var racikan;
	var dracikan;
	var dracikan_row_num;
	var apoteker;
	var obat;
	var bahan;
	var bahan_row_num;
	var resep;
	var dresep;
	$(document).ready(function() {
		$("#resep_t_diskon").on("change", function() {
			var diskon = $("#resep_diskon").val().replace(/[^0-9-,]/g, '').replace(",", ".");
			var t_diskon = $("#resep_t_diskon").val();
			if ($("#resep_t_diskon").val() == "gratis") {
				$("#resep_diskon").val("100,00");
				$("#resep_diskon").removeAttr("disabled");
				$("#resep_diskon").attr("disabled", "disabled");
			} else {
				$("#resep_diskon").val("0,00");
				$("#resep_diskon").removeAttr("disabled");
			}
		});
		$("#resep_diskon").on("change", function() {
			var diskon = $("#resep_diskon").val().replace(/[^0-9-,]/g, '').replace(",", ".");
			if (diskon == "") {
				$("#resep_diskon").val("0,00");
			}
		});
		apoteker = new ApotekerAction(
			"apoteker",
			"depo_farmasi_akhp",
			"e_resep_form",
			new Array()
		);
		apoteker.setSuperCommand("apoteker");
		obat = new ObatAction(
			"obat",
			"depo_farmasi_akhp",
			"e_resep_form",
			new Array()
		);
		obat.setSuperCommand("obat");
		bahan = new BahanAction(
			"bahan",
			"depo_farmasi_akhp",
			"e_resep_form",
			new Array()
		);
		bahan.setSuperCommand("bahan");
		dracikan = new DRacikanAction(
			"dracikan",
			"depo_farmasi_akhp",
			"e_resep_form",
			new Array("harga")
		);
		racikan = new RacikanAction(
			"racikan",
			"depo_farmasi_akhp",
			"e_resep_form",
			new Array()
		);
		non_racikan = new NonRacikanAction(
			"non_racikan",
			"depo_farmasi_akhp",
			"e_resep_form",
			new Array("harga")
		);
		dresep = new DResepAction(
			"dresep",
			"depo_farmasi_akhp",
			"e_resep_form",
			new Array()
		);
		resep = new ResepAction(
			"resep",
			"depo_farmasi_akhp",
			"e_resep_form",
			new Array("diskon")
		);

		bahan_row_num = $("#r_bahan_list tr").length;
		non_racikan.add_mode();
		dracikan.add_mode();
		racikan.hide_form();
	});
</script>