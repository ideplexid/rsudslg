<?php 
	require_once("smis-base/smis-include-service-consumer.php");
	require_once("depo_farmasi_akhp/library/InventoryLibrary.php");
	global $db;
	
	$laporan_form = new Form("lpov_form", "", "Depo Depo Farmasi AKHP : Laporan Penjualan Obat Per Vendor");
	$vendor_button = new Button("", "", "Pilih");
	$vendor_button->setClass("btn-info");
	$vendor_button->setAction("vendor_lpov.chooser('vendor_lpov', 'vendor_lpov_button', 'vendor_lpov', vendor_lpov)");
	$vendor_button->setIcon("icon-white icon-list-alt");
	$vendor_button->setIsButton(Button::$ICONIC);
	$vendor_button->setAtribute("id='vendor_browse'");
	$vendor_text = new Text("lpov_vendor", "lpov_vendor", "");
	$vendor_text->setClass("smis-one-option-input");
	$vendor_text->setAtribute("disabled='disabled'");
	$vendor_input_group = new InputGroup("");
	$vendor_input_group->addComponent($vendor_text);
	$vendor_input_group->addComponent($vendor_button);
	$laporan_form->addElement("Vendor", $vendor_input_group);
	$tanggal_from_text = new Text("lpov_tanggal_from", "lpov_tanggal_from", date("Y-m-") . "01 00:00");
	$tanggal_from_text->setClass("mydatetime");
	$tanggal_from_text->setAtribute("data-date-format='yyyy-mm-dd hh:ii'");
	$laporan_form->addElement("Waktu Awal", $tanggal_from_text);
	$tanggal_to_text = new Text("lpov_tanggal_to", "lpov_tanggal_to", date("Y-m-d H:i"));
	$tanggal_to_text->setClass("mydatetime");
	$tanggal_to_text->setAtribute("data-date-format='yyyy-mm-dd hh:ii'");
	$laporan_form->addElement("Waktu Akhir", $tanggal_to_text);
	$show_button = new Button("", "", "Tampilkan");
	$show_button->setClass("btn-primary");
	$show_button->setIcon("icon-white icon-repeat");
	$show_button->setIsButton(Button::$ICONIC);
	$show_button->setAction("lpov.view()");
	$print_button = new Button("", "", "Cetak");
	$print_button->setClass("btn-inverse");
	$print_button->setIcon("fa fa-download");
	$print_button->setIsButton(Button::$ICONIC);
	$print_button->setAction("lpov.export_xls()");
	$btn_group = new ButtonGroup("noprint");
	$btn_group->addButton($show_button);
	$btn_group->addButton($print_button);
	$laporan_form->addElement("", $btn_group);
	
	$lpov_table = new Table(
		array("Obat", "Jenis Obat", "Vendor", "Jumlah", "Satuan", "Harga Satuan", "Disk. (%)", "Harga Total"),
		"",
		null,
		true
	);
	$lpov_table->setName("lpov");
	$lpov_table->setAction(false);
	
	//vendor chooser:
	$vendor_table = new Table(array("No.", "Vendor"));
	$vendor_table->setName("vendor_lpov");
	$vendor_table->setModel(Table::$SELECT);
	$vendor_adapter = new SimpleAdapter(true, "No.");
	$vendor_adapter->add("Vendor", "nama");
	$vendor_service_responder = new ServiceResponder(
		$db, 
		$vendor_table, 
		$vendor_adapter,
		"vendor"
	);
	
	$super_command = new SuperCommand();
	$super_command->addResponder("vendor_lpov", $vendor_service_responder);
	$init = $super_command->initialize();
	if ($init != null) {
		echo $init;
		return;
	}
	
	if (isset($_POST['command'])) {
		if ($_POST['command'] == "export_xls") {
			$tanggal_from = $_POST['from'];
			$tanggal_to = $_POST['to'];
			$nama_vendor = $_POST['nama_vendor'];
			require_once("smis-libs-out/php-excel/PHPExcel.php");
			$objPHPExcel = PHPExcel_IOFactory::load("depo_farmasi_akhp/templates/template_laporan_penjualan_per_vendor.xlsx");
			$objPHPExcel->setActiveSheetIndexByName("PENJUALAN - VENDOR");
			$objWorksheet = $objPHPExcel->getActiveSheet();
			$objWorksheet->setCellValue("B3", "PERIODE : " . ArrayAdapter::format("date d-m-Y, H:i", $tanggal_from) . " s/d " . ArrayAdapter::format("date d-m-Y, H:i", $tanggal_to) . " (" . $nama_vendor . ")");
			$filter = "tanggal >= '" . $tanggal_from . "' AND tanggal <= '" . $tanggal_to . "' AND nama_vendor = '" . $nama_vendor . "'";
			$query_value = "
				SELECT *
				FROM (
					SELECT nama_obat, nama_jenis_obat, nama_vendor, SUM(jumlah) AS 'jumlah', satuan, harga, diskon, t_diskon
					FROM (
						SELECT " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".id, " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".tanggal, " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".id_dokter, " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".nama_dokter, " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".tipe, " . InventoryLibrary::$_TBL_PENJUALAN_OBAT_JADI . ".id_obat, " . InventoryLibrary::$_TBL_PENJUALAN_OBAT_JADI . ".nama_obat, " . InventoryLibrary::$_TBL_PENJUALAN_OBAT_JADI . ".nama_jenis_obat, " . InventoryLibrary::$_TBL_STOK_PAKAI_OBAT_JADI . ".jumlah, " . InventoryLibrary::$_TBL_PENJUALAN_OBAT_JADI . ".satuan, " . InventoryLibrary::$_TBL_STOK_OBAT . ".nama_vendor, " . InventoryLibrary::$_TBL_PENJUALAN_OBAT_JADI . ".harga, 'OBAT JADI' AS 'keterangan', " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".diskon, " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".t_diskon, " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".total
						FROM ((" . InventoryLibrary::$_TBL_STOK_PAKAI_OBAT_JADI . " LEFT JOIN " . InventoryLibrary::$_TBL_STOK_OBAT . " ON " . InventoryLibrary::$_TBL_STOK_PAKAI_OBAT_JADI . ".id_stok_obat = " . InventoryLibrary::$_TBL_STOK_OBAT . ".id) LEFT JOIN " . InventoryLibrary::$_TBL_PENJUALAN_OBAT_JADI . " ON " . InventoryLibrary::$_TBL_STOK_PAKAI_OBAT_JADI . ".id_penjualan_obat_jadi = " . InventoryLibrary::$_TBL_PENJUALAN_OBAT_JADI . ".id) LEFT JOIN " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . " ON " . InventoryLibrary::$_TBL_PENJUALAN_OBAT_JADI . ".id_penjualan_resep = " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".id
						WHERE " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".prop NOT LIKE 'del' AND " . InventoryLibrary::$_TBL_PENJUALAN_OBAT_JADI . ".prop NOT LIKE 'del' AND " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".dibatalkan = 0 AND " . InventoryLibrary::$_TBL_STOK_PAKAI_OBAT_JADI . ".jumlah > 0
						UNION ALL
						SELECT " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".id, " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".tanggal, " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".id_dokter, " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".nama_dokter, " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".tipe, " . InventoryLibrary::$_TBL_BAHAN_PAKAI_OBAT_RACIKAN . ".id_obat, " . InventoryLibrary::$_TBL_BAHAN_PAKAI_OBAT_RACIKAN . ".nama_obat, " . InventoryLibrary::$_TBL_BAHAN_PAKAI_OBAT_RACIKAN . ".nama_jenis_obat, " . InventoryLibrary::$_TBL_STOK_PAKAI_BAHAN . ".jumlah, " . InventoryLibrary::$_TBL_BAHAN_PAKAI_OBAT_RACIKAN . ".satuan, " . InventoryLibrary::$_TBL_STOK_OBAT . ".nama_vendor, " . InventoryLibrary::$_TBL_BAHAN_PAKAI_OBAT_RACIKAN . ".harga, CONCAT('BAHAN RACIKAN ', " . InventoryLibrary::$_TBL_PENJUALAN_OBAT_RACIKAN . ".nama) AS 'keterangan', " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".diskon, " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".t_diskon, " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".total
						FROM (((" . InventoryLibrary::$_TBL_STOK_PAKAI_BAHAN . " LEFT JOIN " . InventoryLibrary::$_TBL_STOK_OBAT . " ON " . InventoryLibrary::$_TBL_STOK_PAKAI_BAHAN . ".id_stok_obat = " . InventoryLibrary::$_TBL_STOK_OBAT . ".id) LEFT JOIN " . InventoryLibrary::$_TBL_BAHAN_PAKAI_OBAT_RACIKAN . " ON " . InventoryLibrary::$_TBL_STOK_PAKAI_BAHAN . ".id_bahan = " . InventoryLibrary::$_TBL_BAHAN_PAKAI_OBAT_RACIKAN . ".id) LEFT JOIN " . InventoryLibrary::$_TBL_PENJUALAN_OBAT_RACIKAN . " ON " . InventoryLibrary::$_TBL_BAHAN_PAKAI_OBAT_RACIKAN . ".id_penjualan_obat_racikan = " . InventoryLibrary::$_TBL_PENJUALAN_OBAT_RACIKAN . ".id) LEFT JOIN " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . " ON " . InventoryLibrary::$_TBL_PENJUALAN_OBAT_RACIKAN . ".id_penjualan_resep = " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".id
						WHERE " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".prop NOT LIKE 'del' AND " . InventoryLibrary::$_TBL_PENJUALAN_OBAT_RACIKAN . ".prop NOT LIKE 'del' AND " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".dibatalkan = 0 AND " . InventoryLibrary::$_TBL_STOK_PAKAI_BAHAN . ".jumlah > 0
					) v_lpov
					WHERE " . $filter . "
					GROUP BY id_obat, satuan, harga, diskon, t_diskon
					ORDER BY nama_obat, nama_jenis_obat ASC
				) v_laporan
			";
			$dbtable = new DBTable($db, "" . InventoryLibrary::$_TBL_PENJUALAN_RESEP . "");
			$data = $dbtable->get_result($query_value);
			if (count($data) - 3 > 0)
				$objWorksheet->insertNewRowBefore(8, count($data) - 3);
			$start_row_num = 7;
			$end_row_num = 7;
			$row_num = $start_row_num;
			$nomor = 1;
			foreach ($data as $d) {
				$col_num = 1;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $nomor++);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->nama_obat);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->nama_jenis_obat);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->nama_vendor);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->jumlah);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->satuan);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->harga);
				$v_diskon = $d->diskon;
				if ($d->t_diskon == "nominal") {
					$v_diskon = ($d->diskon / $d->total) * 100;
					$v_diskon = round($v_diskon, 2);
				}
				$harga_total = $d->jumlah * $d->harga - (($v_diskon / 100) * $d->jumlah * $d->harga);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $v_diskon);
				$col_num++;				
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $harga_total);
				$objWorksheet->getStyle("E" . $row_num)->getNumberFormat()->setFormatCode("#,##0");
				$objWorksheet->getStyle("J" . $row_num)->getNumberFormat()->setFormatCode("#,##0");
				$row_num++;
				$end_row_num++;
			}
			header("Content-type: application/vnd.ms-excel");	
			header("Content-Disposition: attachment; filename=LAPORAN_PENJUALAN_OBAT_PER_VENDOR_" . $vendor . "_" . ArrayAdapter::format("date Ymd_Hi", $tanggal_from) . "_" . ArrayAdapter::format("date Ymd_Hi", $tanggal_to) . ".xlsx");
			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");
			$objWriter->save("php://output");
			return;
		}
		class LPODPAdapter extends ArrayAdapter {
			public function adapt($row) {
				$array = array();
				$array['Obat'] = $row->nama_obat;
				$array['Jenis Obat'] = $row->nama_jenis_obat;
				$array['Vendor'] = $row->nama_vendor;
				$array['Jumlah'] = $row->jumlah;
				$array['Satuan'] = $row->satuan;
				$array['Harga Satuan'] = self::format("money Rp.", $row->harga);
				$f_diskon = self::format("only-money", $row->diskon);
				$v_diskon = $row->diskon;
				if ($row->t_diskon == "nominal") {
					$v_diskon = ($row->diskon / $row->total) * 100;
					$v_diskon = round($v_diskon, 2);
					$f_diskon = self::format("only-money", $v_diskon) . "<sup>*</sup>";
				}
				$array['Disk. (%)'] = $f_diskon;
				$harga_total = $row->jumlah * $row->harga - (($v_diskon / 100) * $row->jumlah * $row->harga);
				$array['Harga Total'] = self::format("money Rp.", $harga_total);
				return $array;
			}
		}
		$lpov_adapter = new LPODPAdapter();		
		$lpov_dbtable = new DBTable($db, "" . InventoryLibrary::$_TBL_PENJUALAN_RESEP . "");
		if (isset($_POST['command']) == "list") {
			$filter = "tanggal >= '" . $_POST['tanggal_from'] . "' AND tanggal <= '" . $_POST['tanggal_to'] . "' AND nama_vendor = '" . $_POST['nama_vendor'] . "'";
			if (isset($_POST['kriteria'])) {
				$filter .= " AND (nama_obat LIKE '%" . $_POST['kriteria'] . "%' OR nama_jenis_obat LIKE '%" . $_POST['kriteria'] . "%' OR satuan LIKE '%" . $_POST['kriteria'] . "%') ";
			}
			$query_value = "
				SELECT *
				FROM (
					SELECT nama_obat, nama_jenis_obat, nama_vendor, SUM(jumlah) AS 'jumlah', satuan, harga, diskon, t_diskon
					FROM (
						SELECT " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".id, " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".tanggal, " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".id_dokter, " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".nama_dokter, " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".tipe, " . InventoryLibrary::$_TBL_PENJUALAN_OBAT_JADI . ".id_obat, " . InventoryLibrary::$_TBL_PENJUALAN_OBAT_JADI . ".nama_obat, " . InventoryLibrary::$_TBL_PENJUALAN_OBAT_JADI . ".nama_jenis_obat, " . InventoryLibrary::$_TBL_STOK_PAKAI_OBAT_JADI . ".jumlah, " . InventoryLibrary::$_TBL_PENJUALAN_OBAT_JADI . ".satuan, " . InventoryLibrary::$_TBL_STOK_OBAT . ".nama_vendor, " . InventoryLibrary::$_TBL_PENJUALAN_OBAT_JADI . ".harga, 'OBAT JADI' AS 'keterangan', " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".diskon, " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".t_diskon, " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".total
						FROM ((" . InventoryLibrary::$_TBL_STOK_PAKAI_OBAT_JADI . " LEFT JOIN " . InventoryLibrary::$_TBL_STOK_OBAT . " ON " . InventoryLibrary::$_TBL_STOK_PAKAI_OBAT_JADI . ".id_stok_obat = " . InventoryLibrary::$_TBL_STOK_OBAT . ".id) LEFT JOIN " . InventoryLibrary::$_TBL_PENJUALAN_OBAT_JADI . " ON " . InventoryLibrary::$_TBL_STOK_PAKAI_OBAT_JADI . ".id_penjualan_obat_jadi = " . InventoryLibrary::$_TBL_PENJUALAN_OBAT_JADI . ".id) LEFT JOIN " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . " ON " . InventoryLibrary::$_TBL_PENJUALAN_OBAT_JADI . ".id_penjualan_resep = " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".id
						WHERE " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".prop NOT LIKE 'del' AND " . InventoryLibrary::$_TBL_PENJUALAN_OBAT_JADI . ".prop NOT LIKE 'del' AND " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".dibatalkan = 0 AND " . InventoryLibrary::$_TBL_STOK_PAKAI_OBAT_JADI . ".jumlah > 0
						UNION ALL
						SELECT " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".id, " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".tanggal, " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".id_dokter, " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".nama_dokter, " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".tipe, " . InventoryLibrary::$_TBL_BAHAN_PAKAI_OBAT_RACIKAN . ".id_obat, " . InventoryLibrary::$_TBL_BAHAN_PAKAI_OBAT_RACIKAN . ".nama_obat, " . InventoryLibrary::$_TBL_BAHAN_PAKAI_OBAT_RACIKAN . ".nama_jenis_obat, " . InventoryLibrary::$_TBL_STOK_PAKAI_BAHAN . ".jumlah, " . InventoryLibrary::$_TBL_BAHAN_PAKAI_OBAT_RACIKAN . ".satuan, " . InventoryLibrary::$_TBL_STOK_OBAT . ".nama_vendor, " . InventoryLibrary::$_TBL_BAHAN_PAKAI_OBAT_RACIKAN . ".harga, CONCAT('BAHAN RACIKAN ', " . InventoryLibrary::$_TBL_PENJUALAN_OBAT_RACIKAN . ".nama) AS 'keterangan', " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".diskon, " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".t_diskon, " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".total
						FROM (((" . InventoryLibrary::$_TBL_STOK_PAKAI_BAHAN . " LEFT JOIN " . InventoryLibrary::$_TBL_STOK_OBAT . " ON " . InventoryLibrary::$_TBL_STOK_PAKAI_BAHAN . ".id_stok_obat = " . InventoryLibrary::$_TBL_STOK_OBAT . ".id) LEFT JOIN " . InventoryLibrary::$_TBL_BAHAN_PAKAI_OBAT_RACIKAN . " ON " . InventoryLibrary::$_TBL_STOK_PAKAI_BAHAN . ".id_bahan = " . InventoryLibrary::$_TBL_BAHAN_PAKAI_OBAT_RACIKAN . ".id) LEFT JOIN " . InventoryLibrary::$_TBL_PENJUALAN_OBAT_RACIKAN . " ON " . InventoryLibrary::$_TBL_BAHAN_PAKAI_OBAT_RACIKAN . ".id_penjualan_obat_racikan = " . InventoryLibrary::$_TBL_PENJUALAN_OBAT_RACIKAN . ".id) LEFT JOIN " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . " ON " . InventoryLibrary::$_TBL_PENJUALAN_OBAT_RACIKAN . ".id_penjualan_resep = " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".id
						WHERE " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".prop NOT LIKE 'del' AND " . InventoryLibrary::$_TBL_PENJUALAN_OBAT_RACIKAN . ".prop NOT LIKE 'del' AND " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".dibatalkan = 0 AND " . InventoryLibrary::$_TBL_STOK_PAKAI_BAHAN . ".jumlah > 0
					) v_lpov
					WHERE " . $filter . "
					GROUP BY id_obat, satuan, harga, diskon, t_diskon
					ORDER BY nama_obat, nama_jenis_obat ASC
				) v_laporan
			";
			$query_count = "
				SELECT COUNT(*)
				FROM (
					" . $query_value . "
				) v_laporan
			";
			$lpov_dbtable->setPreferredQuery(true, $query_value, $query_count);
		}
		$lpov_dbresponder = new DBResponder(
			$lpov_dbtable,
			$lpov_table,
			$lpov_adapter
		);
		$data = $lpov_dbresponder->command($_POST['command']);
		echo json_encode($data);
		return;
	}
	
	echo $laporan_form->getHtml();
	echo "<div id='table_content'>";
	echo $lpov_table->getHtml();
	echo "</div>";
	echo addJS("framework/smis/js/table_action.js");
	echo addJS("framework/bootstrap/js/bootstrap-datetimepicker.js");
	echo addCSS("framework/bootstrap/css/bootstrap-datetimepicker.css");
?>
<script type="text/javascript">
	function LPOVAction(name, page, action, column) {
		this.initialize(name, page, action, column);
	}
	LPOVAction.prototype.constructor = LPOVAction;
	LPOVAction.prototype = new TableAction();
	LPOVAction.prototype.view = function() {
		var self = this;
		var data = this.getViewData();
		data['tanggal_from'] = $("#lpov_tanggal_from").val();
		data['tanggal_to'] = $("#lpov_tanggal_to").val();
		data['nama_vendor'] = $("#lpov_vendor").val();
		showLoading();
		$.post(
			"",
			data,
			function(response) {
				var json = getContent(response);
				if (json == null) return;
				$("#" + self.prefix + "_list").html(json.list);
				$("#" + self.prefix + "_pagination").html(json.pagination);
				dismissLoading();
			}
		);
	};
	LPOVAction.prototype.export_xls = function() {
		if ($("#lpov_tanggal_from").val() == "" || $("#lpov_tanggal_to").val() == "")
			return;
		showLoading();
		var data = this.getRegulerData();
		data['command'] = "export_xls";
		data['from'] = $("#lpov_tanggal_from").val();
		data['to'] = $("#lpov_tanggal_to").val();
		data['nama_vendor'] = $("#lpov_vendor").val();
		postForm(data);
		dismissLoading();
	}
	
	function VendorLPOVAction(name, page, action, column) {
		this.initialize(name, page, action, column);
	}
	VendorLPOVAction.prototype.constructor = VendorLPOVAction;
	VendorLPOVAction.prototype = new TableAction();
	VendorLPOVAction.prototype.selected = function(json) {
		$("#lpov_vendor").val(json.nama);
	};
	
	var lpov;
	var vendor_lpov;
	$(document).ready(function() {
		$('[data-toggle="popover"]').popover({
			trigger: 'hover',
			'placement': 'top'
		});
		vendor_lpov = new VendorLPOVAction(
			"vendor_lpov",
			"depo_farmasi_akhp",
			"laporan_penjualan_obat_per_vendor",
			new Array()
		);
		vendor_lpov.setSuperCommand("vendor_lpov");
		lpov = new LPOVAction(
			"lpov",
			"depo_farmasi_akhp",
			"laporan_penjualan_obat_per_vendor",
			new Array()
		);
		lpov.view();
		$('.mydatetime').datetimepicker({ 
			minuteStep: 1
		});
	});
</script>