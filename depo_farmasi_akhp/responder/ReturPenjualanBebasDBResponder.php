<?php
	require_once("depo_farmasi_akhp/library/InventoryLibrary.php");
	
	class ReturPenjualanBebasDBResponder extends DBResponder {
		public function command($command) {
			if ($command != "print_retur") {
				return parent::command($command);
			}
			$pack = null;
			if ($command == "print_retur") {
				$id['id'] = $_POST['id'];
				$data['tercetak'] = 1;
				$this->dbtable->update($data, $id);
				$pack = new ResponsePackage();
				$content = $this->print_retur();
				$pack->setContent($content);
				$pack->setStatus(ResponsePackage::$STATUS_OK);
			}
			return $pack->getPackage();
		}
		public function print_retur() {
			$id = $_POST['id'];
			$data = $this->dbtable->get_row("SELECT " . InventoryLibrary::$_TBL_RETUR_PENJUALAN_RESEP . ".id, " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".id AS 'id_penjualan_resep', " . InventoryLibrary::$_TBL_RETUR_PENJUALAN_RESEP . ".tanggal, " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".total, " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".nama_dokter, " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".noreg_pasien, " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".nrm_pasien, " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".uri, " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".nama_pasien, " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".alamat_pasien, " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".diskon, " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".t_diskon, " . InventoryLibrary::$_TBL_RETUR_PENJUALAN_RESEP . ".persentase_retur
											 FROM " . InventoryLibrary::$_TBL_RETUR_PENJUALAN_RESEP . " LEFT JOIN " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . " ON " . InventoryLibrary::$_TBL_RETUR_PENJUALAN_RESEP . ".id_penjualan_resep = " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".id
											 WHERE " . InventoryLibrary::$_TBL_RETUR_PENJUALAN_RESEP . ".id = '" . $id . "'");
			$obat_jadi_sql = "
				SELECT " . InventoryLibrary::$_TBL_STOK_OBAT . ".nama_obat AS 'nama', " . InventoryLibrary::$_TBL_DRETUR_PENJUALAN_RESEP . ".jumlah, " . InventoryLibrary::$_TBL_STOK_OBAT . ".satuan, " . InventoryLibrary::$_TBL_DRETUR_PENJUALAN_RESEP . ".harga, " . InventoryLibrary::$_TBL_DRETUR_PENJUALAN_RESEP . ".subtotal
				FROM " . InventoryLibrary::$_TBL_DRETUR_PENJUALAN_RESEP . " LEFT JOIN " . InventoryLibrary::$_TBL_STOK_OBAT . " ON " . InventoryLibrary::$_TBL_DRETUR_PENJUALAN_RESEP . ".id_stok_obat = " . InventoryLibrary::$_TBL_STOK_OBAT . ".id
				WHERE " . InventoryLibrary::$_TBL_DRETUR_PENJUALAN_RESEP . ".prop NOT LIKE 'del' AND id_retur_penjualan_resep = '" . $id . "'
			";
			$obat_jadi_data = $this->dbtable->get_result($obat_jadi_sql);
			$content = "<table border='0' width='100%' cellpadding='10' class='etiket_header'>";
				$content .= "<tr>";
					$content .= "<td>";
						$content .= "<div align='center'><table border='0' class='etiket_content' >";
							$content .= "<tr>";
								$content .= "<td colspan='5' align='center' style='font-size: 13px !important;'><b><u>DEPO FARMASI " . ucwords(getSettings($this->dbtable->get_db(), "smis_autonomous_title", "")) . " - RETUR PENJUALAN BEBAS</u></b></td>";
							$content .= "</tr>";
							$content .= "<tr>";
								$content .= "<td colspan='5'></td>";
							$content .= "</tr>";
							$content .= "<tr>";
								$content .= "<td colspan='1'>No. Retur</td>";
								$content .= "<td colspan='1'>: " .  ArrayAdapter::format("only-digit8", $data->id) . "</td>";
								$content .= "<td colspan='1' class='tpadding'>No. Penjualan</td>";
								$content .= "<td colspan='2'>: " .  ArrayAdapter::format("only-digit8", $data->id_penjualan_resep) . "</td>";
							$content .= "</tr>";
							$content .= "<tr>";
								$content .= "<td colspan='1'>Tgl. Retur</td>"; 
								$content .= "<td colspan='4'>: " . ArrayAdapter::format("date d-m-Y H:i", $data->tanggal) . "</td>";
							$content .= "</tr>";
						$content .= "</table></div>";
					$content .= "</td>";
				$content .= "</tr>";
				$content .= "<tr>";
					$content .= "<td>";
						$content .= "<div align='center'><table border='1' class='etiket_content'>";
						$content .= "<tr align='center'>";
							$content .= "<td colspan='1'>No.</td>";
							$content .= "<td colspan='1'>Obat</td>";
							$content .= "<td colspan='1'>Jumlah</td>";
							$content .= "<td colspan='1'>Harga Satuan</td>";
							$content .= "<td colspan='1'>Subtotal</td>";
						$content .= "</tr>";
						$no_barang = 1;
						$total = 0;
						foreach($obat_jadi_data as $ojd) {
							$content .= "<tr>";
								$content .= "<td colspan='1'>" . $no_barang++ . "</td>";
								$content .= "<td colspan='1'>" . $ojd->nama . "</td>";
								$content .= "<td colspan='1'>" . $ojd->jumlah . " " . $ojd->satuan . "</td>";
								$content .= "<td colspan='1' align='right'>" . ArrayAdapter::format("money Rp.", $ojd->harga) . "</td>";
								$content .= "<td colspan='1' align='right'>" . ArrayAdapter::format("money Rp.", $ojd->subtotal) . "</td>";
							$content .= "</tr>";
							$total += $ojd->subtotal;
						}
						$content .= "<tr>";
							$content .= "<td colspan='4' align='right'>Total</td>";
							$content .= "<td colspan='1' align='right'>" . ArrayAdapter::format("money Rp.", $total) . "</td>";
						$content .= "</tr>";
						$content .= "<tr>";
							$content .= "<td colspan='4' align='right'>Diskon</td>";
							$v_diskon = 0;
							if ($data->t_diskon == "persen" || $data->t_diskon == "gratis") {
								$v_diskon = ($total * $data->diskon) / 100;
								$content .= "<td colspan='1' align='right'>" . ArrayAdapter::format("money Rp.", $v_diskon) . "</td>";
							} else {
								$v_diskon = ($total * $data->diskon) / $data->total;
								$content .= "<td colspan='1' align='right'>" . ArrayAdapter::format("money Rp.", $v_diskon) . "</td>";
							}
						$content .= "</tr>";
						$content .= "<tr>";
							$content .= "<td colspan='4' align='right'>Total Setelah Diskon</td>";
							$total = $total - $v_diskon;
							$content .= "<td colspan='1'>" . ArrayAdapter::format("money Rp.", $total) . "</td>";
						$content .= "</tr>";
						$content .= "<tr>";
							$potongan = 100 - $data->persentase_retur;
							$v_potongan = ($total * $potongan) / 100;
							$content .= "<td colspan='4' align='right'>Potongan Retur (" . $potongan . " %)</td>";
							$content .= "<td colspan='1'>" . ArrayAdapter::format("money Rp.", $v_potongan) . "</td>";
						$content .= "</tr>";
						$content .= "<tr>";
							$potongan = 100 - $data->persentase_retur;
							$content .= "<td colspan='4' align='right'>Uang Kembali</td>";
							$content .= "<td colspan='1'>" . ArrayAdapter::format("money Rp.", $total - $v_potongan) . "</td>";
						$content .= "</tr>";
						$content .= "</table></div>";
					$content .= "</td>";
				$content .= "</tr>";
				$content .= "<tr>";
					$content .= "<td colspan='5'>";
						$content .= "<table border='0' align='center' class='etiket_content'>";
							$content .= "<tr>";
								$content .= "<td align='center'>PENERIMA,</td>";
								$content .= "<td>&Tab;</td>";
								$content .= "<td align='center'>PETUGAS DEPO FARMASI,</td>";
							$content .= "</tr>";
							$content .= "<tr>";
								$content .= "<td></td>";
								$content .= "<td>&Tab;</td>";
								$content .= "<td></td>";
							$content .= "</tr>";
							$content .= "<tr>";
								$content .= "<td></td>";
								$content .= "<td>&Tab;</td>";
								$content .= "<td></td>";
							$content .= "</tr>";
							$content .= "<tr>";
								$content .= "<td></td>";
								$content .= "<td>&Tab;</td>";
								$content .= "<td></td>";
							$content .= "</tr>";
							$content .= "<tr>";
								$content .= "<td></td>";
								$content .= "<td>&Tab;</td>";
								$content .= "<td></td>";
							$content .= "</tr>";
							$content .= "<tr>";
								$content .= "<td align='center'>(_____________________)</td>";
								$content .= "<td>&Tab;</td>";
								global $user;
								$content .= "<td align='center'>" . $user->getNameOnly() . "</td>";
							$content .= "</tr>";
							$content .= "<tr>";
								$content .= "<td></td>";
							$content .= "</tr>";
						$content .= "</table>";
					$content .= "</td>";
				$content .= "</tr>";
			$content .= "</table>";
			return $content;
		}
		public function save() {
			$header_data = $this->postToArray();
			$id['id'] = $_POST['id'];
			if ($id['id'] == 0 || $id['id'] == "") {
				//do insert header here:
				$header_data['persentase_retur'] = getSettings($this->dbtable->get_db(), "depo_farmasi5-retur_penjualan-persentase_retur", 100);;
				$header_data['tanggal'] = date("Y-m-d H:i:s");
				$result = $this->dbtable->insert($header_data);
				if (isset($_POST['id_penjualan_resep'])) {
					$penjualan_penjualan_bebas_dbtable = new DBTable($this->dbtable->get_db(), InventoryLibrary::$_TBL_PENJUALAN_RESEP);
					$resep_data = array();
					$resep_data['diretur'] = "1";
					$resep_id['id'] = $_POST['id_penjualan_resep'];
					$penjualan_penjualan_bebas_dbtable->update($resep_data, $resep_id);
				}
				$id['id'] = $this->dbtable->get_inserted_id();
				$success['type'] = "insert";
				if (isset($_POST['detail'])) {
					//do insert detail here:
					$dretur_penjualan_bebas_dbtable = new DBTable($this->dbtable->get_db(), InventoryLibrary::$_TBL_DRETUR_PENJUALAN_RESEP);
					$stok_obat_dbtable = new DBTable($this->dbtable->get_db(), InventoryLibrary::$_TBL_STOK_OBAT);
					$detail = $_POST['detail'];
					foreach($detail as $d) {
						$dretur_data = array();
						$dretur_data['id_retur_penjualan_resep'] = $id['id'];
						$dretur_data['id_penjualan_resep'] = $_POST['id_penjualan_resep'];
						$dretur_data['id_stok_obat'] = $d['id_stok_obat'];
						$dretur_data['jumlah'] = $d['jumlah'];
						$dretur_data['harga'] = $d['harga'];
						$dretur_data['subtotal'] = $d['jumlah'] * $d['harga'];
						$dretur_penjualan_bebas_dbtable->insert($dretur_data);
						$stok_row = $stok_obat_dbtable->get_row("
							SELECT id, sisa
							FROM " . InventoryLibrary::$_TBL_STOK_OBAT . "
							WHERE id = '" . $d['id_stok_obat'] . "'
						");
						$stok_data = array();
						$stok_data['sisa'] = $stok_row->sisa + $d['jumlah'];
						$stok_id['id'] = $d['id_stok_obat'];
						$stok_obat_dbtable->update($stok_data, $stok_id);
						//logging riwayat stok:
						$riwayat_dbtable = new DBTable($this->dbtable->get_db(), InventoryLibrary::$_TBL_RIWAYAT_STOK_OBAT);
						$data_riwayat = array();
						$data_riwayat['tanggal'] = date("Y-m-d");
						$data_riwayat['id_stok_obat'] = $d['id_stok_obat'];
						$data_riwayat['jumlah_masuk'] = $d['jumlah'];
						$data_riwayat['sisa'] = $stok_row->sisa + $d['jumlah'];
						$data_riwayat['keterangan'] = "Stok Retur Penjualan Bebas (" . ArrayAdapter::format("only-digit8", $_POST['id_penjualan_resep']) . ")";
						global $user;
						$data_riwayat['nama_user'] = $user->getName();
						$riwayat_dbtable->insert($data_riwayat);
					}
				}
				$dretur_rows = $this->dbtable->get_result("
					SELECT b.id_obat, b.kode_obat, b.nama_obat, b.nama_jenis_obat, SUM(a.jumlah) AS 'jumlah', b.satuan
					FROM " . InventoryLibrary::$_TBL_DRETUR_PENJUALAN_RESEP . " a LEFT JOIN " . InventoryLibrary::$_TBL_STOK_OBAT . " b ON a.id_stok_obat = b.id
					WHERE a.prop NOT LIKE 'del' AND a.id_retur_penjualan_resep = '" . $id['id'] . "'
					GROUP BY b.id_obat, b.satuan
				");
				//logging kartu stok:
				$kartu_stok_dbtable = new DBTable($this->dbtable->get_db(), InventoryLibrary::$_TBL_KARTU_STOK_OBAT);
				$keterangan = "Resep (" . ArrayAdapter::format("only-digit8", $_POST['id_penjualan_resep']) . ")";
				foreach ($dretur_rows as $drr) {
					$sisa_row = $this->dbtable->get_row("
						SELECT SUM(a.sisa) AS 'sisa'
						FROM " . InventoryLibrary::$_TBL_STOK_OBAT . " a LEFT JOIN " . InventoryLibrary::$_TBL_OBAT_MASUK . " b ON a.id_obat_masuk = b.id
						WHERE a.prop NOT LIKE 'del' AND b.prop NOT LIKE 'del' AND  AND a.id_obat = '" . $drr->id_obat . "' AND a.satuan = '" . $drr->satuan . "' AND a.konversi = '1'
					");
					$kartu_stok_data = array(
						"f_id"				=> $id['id'],
						"no_bon"			=> $id['id'],
						"unit"				=> "Retur Penjualan Bebas : " . $keterangan,
						"id_obat"			=> $drr->id_obat,
						"kode_obat"			=> $drr->kode_obat,
						"nama_obat"			=> $drr->nama_obat,
						"nama_jenis_obat"	=> $drr->nama_jenis_obat,
						"tanggal"			=> date("Y-m-d"),
						"masuk"				=> $drr->jumlah,
						"keluar"			=> 0,
						"sisa"				=> $sisa_row->sisa
					);
					$result = $kartu_stok_dbtable->insert($kartu_stok_data);
				}
			} else {
				//do update header here:
				$result = $this->dbtable->update($header_data, $id);
				$success['type'] = "update";
				if (isset($_POST['dibatalkan']) && $_POST['dibatalkan'] == 1) {
					$header_row = $this->dbtable->get_row("
						SELECT *
						FROM " . InventoryLibrary::$_TBL_RETUR_PENJUALAN_RESEP . "
						WHERE id = '" . $id['id'] . "'
					");
					$penjualan_penjualan_bebas_dbtable = new DBTable($this->dbtable->get_db(), InventoryLibrary::$_TBL_PENJUALAN_RESEP);
					$resep_data = array();
					$resep_data['diretur'] = "0";
					$resep_id['id'] = $header_row->id_penjualan_resep;
					$penjualan_penjualan_bebas_dbtable->update($resep_data, $resep_id);
					$detail_rows = $this->dbtable->get_result("
						SELECT *
						FROM " . InventoryLibrary::$_TBL_DRETUR_PENJUALAN_RESEP . "
						WHERE id_retur_penjualan_resep = '" . $id['id'] . "' AND prop NOT LIKE 'del'
					");
					$stok_obat_dbtable = new DBTable($this->dbtable->get_db(), InventoryLibrary::$_TBL_STOK_OBAT);
					foreach($detail_rows as $dr) {
						$stok_row = $stok_obat_dbtable->get_row("
							SELECT id, sisa
							FROM " . InventoryLibrary::$_TBL_STOK_OBAT . "
							WHERE id = '" . $dr->id_stok_obat . "'
						");
						$stok_data = array();
						$stok_data['sisa'] = $stok_row->sisa - $dr->jumlah;
						$stok_id['id'] = $dr->id_stok_obat;
						$stok_obat_dbtable->update($stok_data, $stok_id);
						//logging riwayat stok:
						$riwayat_dbtable = new DBTable($this->dbtable->get_db(), InventoryLibrary::$_TBL_RIWAYAT_STOK_OBAT);
						$data_riwayat = array();
						$data_riwayat['tanggal'] = date("Y-m-d");
						$data_riwayat['id_stok_obat'] = $dr->id_stok_obat;
						$data_riwayat['jumlah_keluar'] = $dr->jumlah;
						$data_riwayat['sisa'] = $stok_row->sisa - $dr->jumlah;
						$data_riwayat['keterangan'] = "Pembatalan Retur Penjualan Bebas (" . ArrayAdapter::format("only-digit8", $id['id']) . ")";
						global $user;
						$data_riwayat['nama_user'] = $user->getName();
						$riwayat_dbtable->insert($data_riwayat);
					}
					$dretur_rows = $this->dbtable->get_result("
						SELECT b.id_obat, b.kode_obat, b.nama_obat, b.nama_jenis_obat, SUM(a.jumlah) AS 'jumlah', b.satuan
						FROM " . InventoryLibrary::$_TBL_DRETUR_PENJUALAN_RESEP . " a LEFT JOIN " . InventoryLibrary::$_TBL_STOK_OBAT . " b ON a.id_stok_obat = b.id
						WHERE a.prop NOT LIKE 'del' AND a.id_retur_penjualan_resep = '" . $id['id'] . "'
						GROUP BY b.id_obat, b.satuan
					");
					//logging kartu stok:
					$kartu_stok_dbtable = new DBTable($this->dbtable->get_db(), InventoryLibrary::$_TBL_KARTU_STOK_OBAT);
					foreach ($dretur_rows as $drr) {
						$sisa_row = $this->dbtable->get_row("
							SELECT SUM(a.sisa) AS 'sisa'
							FROM " . InventoryLibrary::$_TBL_STOK_OBAT . " a LEFT JOIN " . InventoryLibrary::$_TBL_OBAT_MASUK . " b ON a.id_obat_masuk = b.id
							WHERE a.prop NOT LIKE 'del' AND b.prop LIKE 'del' AND b.status = 'sudah' AND a.id_obat = '" . $drr->id_obat . "' AND a.satuan = '" . $drr->satuan . "' AND a.konversi = '1'
						");
						$kartu_stok_data = array(
							"f_id"				=> $id['id'],
							"no_bon"			=> $id['id'],
							"unit"				=> "Retur Penjualan Bebas : Pembatalan",
							"id_obat"			=> $drr->id_obat,
							"kode_obat"			=> $drr->kode_obat,
							"nama_obat"			=> $drr->nama_obat,
							"nama_jenis_obat"	=> $drr->nama_jenis_obat,
							"tanggal"			=> date("Y-m-d"),
							"masuk"				=> 0,
							"keluar"			=> $drr->jumlah,
							"sisa"				=> $sisa_row->sisa
						);
						$result = $kartu_stok_dbtable->insert($kartu_stok_data);
					}
				}
			}
			$success['id'] = $id['id'];
			$success['success'] = 1;
			if ($result === false) $success['success'] = 0;
			return $success;
		}
		public function edit() {
			$id = $_POST['id'];
			$data['header'] = $this->dbtable->get_row("
				SELECT " . InventoryLibrary::$_TBL_RETUR_PENJUALAN_RESEP . ".*, " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".nama_dokter, " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".nama_pasien, " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".noreg_pasien, " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".nrm_pasien, " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".alamat_pasien
				FROM " . InventoryLibrary::$_TBL_RETUR_PENJUALAN_RESEP . " LEFT JOIN " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . " ON " . InventoryLibrary::$_TBL_RETUR_PENJUALAN_RESEP . ".id_penjualan_resep = " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".id
				WHERE " . InventoryLibrary::$_TBL_RETUR_PENJUALAN_RESEP . ".id = '" . $id . "'
			");
			$detail_rows = $this->dbtable->get_result("
				SELECT " . InventoryLibrary::$_TBL_DRETUR_PENJUALAN_RESEP . ".*, " . InventoryLibrary::$_TBL_STOK_OBAT . ".id_obat, " . InventoryLibrary::$_TBL_STOK_OBAT . ".nama_jenis_obat, " . InventoryLibrary::$_TBL_STOK_OBAT . ".nama_obat, " . InventoryLibrary::$_TBL_STOK_OBAT . ".satuan, " . InventoryLibrary::$_TBL_STOK_OBAT . ".konversi, " . InventoryLibrary::$_TBL_STOK_OBAT . ".satuan_konversi
				FROM " . InventoryLibrary::$_TBL_DRETUR_PENJUALAN_RESEP . " LEFT JOIN " . InventoryLibrary::$_TBL_STOK_OBAT . " ON " . InventoryLibrary::$_TBL_DRETUR_PENJUALAN_RESEP . ".id_stok_obat = " . InventoryLibrary::$_TBL_STOK_OBAT . ".id
				WHERE " . InventoryLibrary::$_TBL_DRETUR_PENJUALAN_RESEP . ".id_retur_penjualan_resep = '" . $id . "' AND " . InventoryLibrary::$_TBL_DRETUR_PENJUALAN_RESEP . ".prop NOT LIKE 'del'
			");
			$detail_list = "";
			$row_id = 0;
			foreach($detail_rows as $dr) {
				if ($dr->jumlah > 0) {
					$detail_list .= "<tr id='detail_" . $row_id . "'>" .
										"<td id='detail_" . $row_id . "_id' style='display: none;'>" . $dr->id . "</td>" .
										"<td id='detail_" . $row_id . "_id_stok_obat' style='display: none;'>" . $dr->id_stok_obat . "</td>" .
										"<td id='detail_" . $row_id . "_id_obat' style='display: none;'>" . $dr->id_obat . "</td>" .
										"<td id='detail_" . $row_id . "_nama_jenis_obat' style='display: none;'>" . $dr->nama_jenis_obat . "</td>" .
										"<td id='detail_" . $row_id . "_harga' style='display: none;'>" . $dr->harga . "</td>" .
										"<td id='detail_" . $row_id . "_jumlah_retur' style='display: none;'>" . $dr->jumlah . "</td>" .
										"<td id='detail_" . $row_id . "_satuan' style='display: none;'>" . $dr->satuan . "</td>" .
										"<td id='detail_" . $row_id . "_konversi' style='display: none;'>" . $dr->konversi . "</td>" .
										"<td id='detail_" . $row_id . "_satuan_konversi' style='display: none;'>" . $dr->satuan_konversi . "</td>" .
										"<td id='detail_" . $row_id . "_nama_obat'>" . $dr->nama_obat . "</td>" .
										"<td id='detail_" . $row_id . "_f_harga'>" . "Rp. " . number_format($dr->harga, 2, ",", ".") . "</td>" .
										"<td id='detail_" . $row_id . "_f_jumlah'>" . $dr->jumlah . " " . $dr->satuan . "</td>" .
										"<td></td>" .
									"</tr>";
					$row_id++;
				}
			}
			$data['detail_list'] = $detail_list;
			return $data;
		}
	}
?>