<?php 
	global $db;

	$laporan_tabulator = new Tabulator("laporan", "", Tabulator::$LANDSCAPE);
	if (getSettings($db, "depo_farmasi5-laporan-show_lap_rekap_penjualan", 1) == 1)
		$laporan_tabulator->add("laporan_rekap_penjualan", "Lap. Rekap Penjualan", "depo_farmasi_akhp/laporan_rekap_penjualan.php", Tabulator::$TYPE_INCLUDE);
	if (getSettings($db, "depo_farmasi5-laporan-show_lap_detail_penjualan", 1) == 1)
		$laporan_tabulator->add("laporan_rincian_penjualan", "Lap. Rincian Penjualan", "depo_farmasi_akhp/laporan_rincian_penjualan.php", Tabulator::$TYPE_INCLUDE);
	if (getSettings($db, "depo_farmasi5-laporan-show_lap_rekap_retur", 1) == 1)
		$laporan_tabulator->add("laporan_rekap_retur_penjualan", "Lap. Rekap Retur Penjualan", "depo_farmasi_akhp/laporan_rekap_retur_penjualan.php", Tabulator::$TYPE_INCLUDE);
	if (getSettings($db, "depo_farmasi5-laporan-show_lap_detail_retur_penjualan", 1) == 1)
		$laporan_tabulator->add("laporan_rincian_retur_penjualan", "Lap. Rincian Retur Penjualan", "depo_farmasi_akhp/laporan_rincian_retur_penjualan.php", Tabulator::$TYPE_INCLUDE);
	if (getSettings($db, "depo_farmasi5-laporan-show_lap_so", 1) == 1)
		$laporan_tabulator->add("laporan_persediaan_obat", "Lap. Persediaan Obat", "depo_farmasi_akhp/laporan_persediaan_obat.php", Tabulator::$TYPE_INCLUDE);
	if (getSettings($db, "depo_farmasi5-laporan-show_lap_stock_opname", 1) == 1)
		$laporan_tabulator->add("laporan_stock_opname", "Lap. Stock Opname", "depo_farmasi_akhp/laporan_stock_opname.php", Tabulator::$TYPE_INCLUDE);
	if (getSettings($db, "depo_farmasi5-laporan-show_lap_penjualan_okt", 1) == 1)
		$laporan_tabulator->add("laporan_penjualan_okt", "Lap. Penjualan OKT", "depo_farmasi_akhp/laporan_penjualan_okt.php", Tabulator::$TYPE_INCLUDE);
	if (getSettings($db, "depo_farmasi5-laporan-show_lap_penjualan_narkotika", 1) == 1)
		$laporan_tabulator->add("laporan_penjualan_narkotika", "Lap. Penjualan Narkotika", "depo_farmasi_akhp/laporan_penjualan_narkotika.php", Tabulator::$TYPE_INCLUDE);
	if (getSettings($db, "depo_farmasi5-laporan-show_lap_penjualan_prekursor_farmasi", 1) == 1)
		$laporan_tabulator->add("laporan_penjualan_prekursor_farmasi", "Lap. Penjualan Prekursor Farmasi", "depo_farmasi_akhp/laporan_penjualan_prekursor_farmasi.php", Tabulator::$TYPE_INCLUDE);
	if (getSettings($db, "depo_farmasi5-laporan-show_lap_penjualan_per_produsen", 1) == 1)
		$laporan_tabulator->add("laporan_penjualan_obat_per_produsen", "Lap. Penjualan Obat Per Produsen", "depo_farmasi_akhp/laporan_penjualan_obat_per_produsen.php", Tabulator::$TYPE_INCLUDE);
	if (getSettings($db, "depo_farmasi5-laporan-show_lap_penjualan_per_vendor", 1) == 1)
		$laporan_tabulator->add("laporan_penjualan_obat_per_vendor", "Lap. Penjualan Obat Per Vendor", "depo_farmasi_akhp/laporan_penjualan_obat_per_vendor.php", Tabulator::$TYPE_INCLUDE);
	if (getSettings($db, "depo_farmasi5-laporan-show_lap_monitoring_pelayanan_resep_pasien", 1) == 1)
		$laporan_tabulator->add("laporan_monitoring_pelayanan_resep_pasien", "Lap. Monitoring Pelayanan Resep Pasien", "depo_farmasi_akhp/laporan_monitoring_pelayanan_resep_pasien.php", Tabulator::$TYPE_INCLUDE);
	if (getSettings($db, "depo_farmasi5-laporan-show_lap_asuhan_farmasi", 1) == 1)
		$laporan_tabulator->add("laporan_asuhan_farmasi", "Lap. Asuhan Farmasi", "depo_farmasi_akhp/laporan_asuhan_farmasi.php", Tabulator::$TYPE_INCLUDE);
	echo $laporan_tabulator->getHtml();
?>