<?php 
/**
 * this file handling about smis multisite system. 
 * in this area user would be force to choose which system they want to login.
 * 
 * @author 		: Nurul Huda
 * @copyright 	: goblooge@gmail.com
 * @since 		: 3 nov 2016
 * @version 	: 1.0.0
 * @license 	: LGPLv3
 * 
 * */
?>
<html>
	<head>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">		
	</head>
			<link rel='stylesheet' type='text/css' href='smis-base-css/multisite.css'>
	</head>
	<body>

<?php 

require_once "smis-libs-function/smis-libs-function-essential.php";
$s=getAllFileInDir("./");
$filtered=array();
$filtered[]=array(
						"Name"=>"PRIMARY SITE",
						"Link"=>"<a href='index.php' class='btn btn-primary'> <i class='fa fa-sign-in'></i> Login</a>"
					);
foreach($s as $file){
	if(strpos($file,"smis-index-")!==false){
		$name=str_replace("smis-index-","",substr($file,0,-4));
		$filtered[]=array(
						"Name"=>strtoupper($name),
						"Link"=>"<a href='".$file."' class='btn btn-inverse'> <i class='fa fa-sign-in'></i> Login</a>"
					);
	}
}
require_once "smis-base/smis-include-ui.php";
$uitable=new Table(array("Name","Link"),"Choose Which One");
$uitable->setContent($filtered);
$uitable->setAction(false);
$uitable->setFooterVisible(false);
$uitable->setName("multisite");
echo $uitable->getHtml();

?>

		</div>
	</body>
</html>