<?php 
	global $NAVIGATOR;
	
	$kamus_data_menu = new Menu("fa fa-archive");
	$kamus_data_menu->addProperty('title', "Kamus Data");
	$kamus_data_menu->addProperty('name', "Kamus Data");
	$kamus_data_menu->addSubMenu("Data Obat", "kamus_data", "data_obat", "Referensi Data Obat", "fa fa-plus-square");
	$kamus_data_menu->addSubMenu("Data Produsen", "kamus_data", "data_produsen", "Referensi Data Produsen", "fa fa-building");
	$NAVIGATOR->addMenu($kamus_data_menu, "kamus_data");
?>