<?php 
	global $wpdb;

	$query = "
		CREATE TABLE IF NOT EXISTS `smis_kd_data_obat` (
		  `id` INT(11) NOT NULL AUTO_INCREMENT ,
		  `name` TEXT ,
		  `brand` TEXT ,
		  `product_code` TEXT ,
		  `composition` TEXT ,
		  `indication` TEXT ,
		  `dosage` TEXT ,
		  `consuming` TEXT ,
		  `counter_indication` TEXT ,
		  `attention` TEXT ,
		  `side_effect` TEXT ,
		  `interaction` TEXT ,
		  `packaging` TEXT ,
		  `price` TEXT ,
		  `prop` VARCHAR(10) DEFAULT '' ,
		  PRIMARY KEY (`id`)
		) ENGINE=InnoDB ;
	";
	$wpdb->query($query);
	
	$query = "
		CREATE TABLE IF NOT EXISTS `smis_kd_data_produsen` (
		  `id` INT(11) NOT NULL AUTO_INCREMENT ,
		  `nama` TEXT ,
		  `kota` TEXT ,
		  `alamat` TEXT ,
		  `prop` VARCHAR(10) DEFAULT '' ,
		  PRIMARY KEY (`id`)
		) ENGINE=InnoDB ;
	";
	$wpdb->query($query);
	
	require_once("kamus_data/install_data_obat.php");
	require_once("kamus_data/install_data_produsen.php");
?>