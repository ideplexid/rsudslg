<?php 
	$data_produsen_table = new Table(
		array("Nomor", "Nama", "Kota", "Alamat Lengkap"),
		"Kamus Data : Data Produsen",
		null,
		true
	);
	$data_produsen_table->setName("data_produsen");
	$data_produsen_table->setAddButtonEnable(false);
	$data_produsen_table->setEditButtonEnable(false);
	$data_produsen_table->setDelButtonEnable(false);
	
	$nomor_text = new Text("search_nomor", "search_nomor", "");
	$nomor_text->setClass("search");
	$nama_text = new Text("search_nama", "search_nama", "");
	$nama_text->setClass("search");
	$kota_text = new Text("search_kota", "search_kota", "");
	$kota_text->setClass("search");
	$alamat_text = new Text("search_alamat", "search_alamat", "");
	$alamat_text->setClass("search");
	$search_button = new Button("", "", "Cari");
	$search_button->setClass("btn-inverse");
	$search_button->setAction("data_produsen.view()");
	$search_button->setIsButton(Button::$ICONIC);
	$search_button->setIcon("icon-white icon-search");
	$header = "<tr class = 'header_data_produsen'>" .
					"<td>" . $nomor_text->getHtml() . "</td>" .
					"<td>" . $nama_text->getHtml() . "</td>" .
					"<td>" . $kota_text->getHtml() . "</td>" .
					"<td>" . $alamat_text->getHtml() . "</td>" .
					"<td>" . $search_button->getHtml() . "</td>" .
			  "</tr>";
	$data_produsen_table->addHeader("after", $header);
	
	if (isset($_POST['command'])) {
		$data_produsen_adapter = new SimpleAdapter();
		$data_produsen_adapter->add("Nomor", "id", "digit8");
		$data_produsen_adapter->add("Nama", "nama");
		$data_produsen_adapter->add("Kota", "kota");
		$data_produsen_adapter->add("Alamat Lengkap", "alamat");
		$data_produsen_dbtable = null;
		$columns = array("id", "nama", "kota", "alamat");
		$data_produsen_dbtable = new DBTable(
			$db, 
			"smis_kd_data_produsen",
			$columns
		);		
		if ($_POST['command'] == "list") {
			$data_produsen_dbtable->addCustomKriteria(" id ", " LIKE '%" . $_POST['search_nomor'] . "%' ");
			$data_produsen_dbtable->addCustomKriteria(" nama ", " LIKE '%" . $_POST['search_nama'] . "%' ");
			$data_produsen_dbtable->addCustomKriteria(" kota ", " LIKE '%" . $_POST['search_kota'] . "%' ");
			$data_produsen_dbtable->addCustomKriteria(" alamat ", " LIKE '%" . $_POST['search_alamat'] . "%' ");
		}		
		$data_produsen_dbresponder = new DBResponder(
			$data_produsen_dbtable,
			$data_produsen_table,
			$data_produsen_adapter
		);
		
		$data = $data_produsen_dbresponder->command($_POST['command']);
		echo json_encode($data);
		return;
	}
	
	echo $data_produsen_table->getHtml();
	echo addJS("framework/smis/js/table_action.js");
?>
<script type="text/javascript">
	function DataProdusenAction(name, page, action, column) {
		this.initialize(name, page, action, column);
	}
	DataProdusenAction.prototype.constructor = DataProdusenAction;
	DataProdusenAction.prototype = new TableAction();
	DataProdusenAction.prototype.getViewData = function() {
		var data = TableAction.prototype.getViewData.call(this);
		data['search_nomor'] = $("#search_nomor").val();
		data['search_nama'] = $("#search_nama").val();
		data['search_kota'] = $("#search_kota").val();
		data['search_alamat'] = $("#search_alamat").val();
		return data;
	};
	
	var data_produsen;
	$(document).ready(function() {
		$('[data-toggle="popover"]').popover({
			trigger: 'hover',
			'placement': 'top'
		});
		data_produsen = new DataProdusenAction(
			"data_produsen",
			"kamus_data",
			"data_produsen",
			new Array()
		);
		data_produsen.view();
	});
</script>