<?php
	global $PLUGINS;
	
	$init['name'] = "kamus_data";
	$init['path'] = SMIS_DIR . $init['name'] . "/";
	$init['description'] = "Kamus Data";
	$init['require'] = "administrator";
	$init['service'] = "";
	$init['version'] = "1.0.0";
	$init['number'] = "1";
	$init['type'] = "";
	
	$plugin = new Plugin($init);
	$PLUGINS[$init['name']] = $plugin;
?>