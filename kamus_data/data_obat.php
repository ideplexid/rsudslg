<?php 
	class DataObatTable extends Table {
		public function getContentButton($id) {
			$btn_group = new ButtonGroup("noprint");
			$btn = new Button("", "", "Lihat");
			$btn->setAction($this->action . ".detail(" . $id . ")");
			$btn->setClass("btn-success");
			$btn->setAtribute("data-content='Lihat' data-toggle='popover'");
			$btn->setIcon("icon-white icon-th");
			$btn->setIsButton(Button::$ICONIC);
			$btn_group->addElement($btn);
			return $btn_group;
		}
	}
	$data_obat_table = new DataObatTable(
		array("Nomor", "Nama", "Produsen", "Kemasan", "Indikasi"),
		"Kamus Data : Data Obat",
		null,
		true
	);
	$data_obat_table->setName("data_obat");
	$data_obat_table->setAddButtonEnable(false);
	
	$nomor_text = new Text("search_nomor", "search_nomor", "");
	$nomor_text->setClass("search");
	$nama_text = new Text("search_nama", "search_nama", "");
	$nama_text->setClass("search");
	$produsen_text = new Text("search_produsen", "search_produsen", "");
	$produsen_text->setClass("search");
	$kemasan_text = new Text("search_kemasan", "search_kemasan", "");
	$kemasan_text->setClass("search");
	$indikasi_text = new Text("search_indikasi", "search_indikasi", "");
	$indikasi_text->setClass("search");
	$search_button = new Button("", "", "Cari");
	$search_button->setClass("btn-inverse");
	$search_button->setAction("data_obat.view()");
	$search_button->setIsButton(Button::$ICONIC);
	$search_button->setIcon("icon-white icon-search");
	$header = "<tr class = 'header_data_obat'>" .
					"<td>" . $nomor_text->getHtml() . "</td>" .
					"<td>" . $nama_text->getHtml() . "</td>" .
					"<td>" . $produsen_text->getHtml() . "</td>" .
					"<td>" . $kemasan_text->getHtml() . "</td>" .
					"<td>" . $indikasi_text->getHtml() . "</td>" .
					"<td>" . $search_button->getHtml() . "</td>" .
			  "</tr>";
	$data_obat_table->addHeader("after", $header);
	
	if (isset($_POST['command'])) {
		class DataObatAdapter extends ArrayAdapter {
			public function adapt ($row) {
				$array = array();
				$array['id'] = $row->id;
				$array['Nomor'] = self::format("digit8", $row->id);
				if ($row->name == "")
					$array['Nama'] = "-";
				else
					$array['Nama'] = $row->name;
				if ($row->brand == "")
					$array['Produsen'] = "-";
				else
					$array['Produsen'] = $row->brand;
				if ($row->packaging == "")
					$array['Kemasan'] = "-";
				else
					$array['Kemasan'] = $row->packaging;
				if ($row->indication == "")
					$array['Indikasi'] = "-";
				else
					$array['Indikasi'] = $row->indication;
				return $array;
			}
		}
		$data_obat_adapter = new DataObatAdapter();
		$data_obat_dbtable = null;
		$columns = array("id", "name", "brand", "packaging", "indication");
		$data_obat_dbtable = new DBTable(
			$db, 
			"smis_kd_data_obat",
			$columns
		);		
		if ($_POST['command'] == "list") {
			$data_obat_dbtable->addCustomKriteria(" id ", " LIKE '%" . $_POST['search_nomor'] . "%' ");
			$data_obat_dbtable->addCustomKriteria(" name ", " LIKE '%" . $_POST['search_nama'] . "%' ");
			$data_obat_dbtable->addCustomKriteria(" brand ", " LIKE '%" . $_POST['search_produsen'] . "%' ");
			$data_obat_dbtable->addCustomKriteria(" packaging ", " LIKE '%" . $_POST['search_kemasan'] . "%' ");
			if ($_POST['search_indikasi'] != "") {
				$indication_keywords = preg_split("/[\s,]+/", $_POST['search_indikasi']);
				$indication_keywords = implode("|", $indication_keywords);
				$data_obat_dbtable->addCustomKriteria(" indication ", " REGEXP '" . $indication_keywords . "' ");
			}
		}		
		$data_obat_dbresponder = new DBResponder(
			$data_obat_dbtable,
			$data_obat_table,
			$data_obat_adapter
		);
		
		$data = $data_obat_dbresponder->command($_POST['command']);
		echo json_encode($data);
		return;
	}
	
	$data_obat_table->addModal("id", "hidden", "", "");
	$data_obat_table->addModal("name", "text", "Nama", "", "y", null, true);
	$data_obat_table->addModal("brand", "text", "Produsen", "", "y", null, true);
	$data_obat_table->addModal("packaging", "text", "Kemasan", "", "y", null, true);
	$data_obat_table->addModal("indication", "textarea", "Indikasi", "", "y", null, true);
	$data_obat_table->addModal("dosage", "textarea", "Dosis", "", "y", null, true);
	$data_obat_table->addModal("consuming", "textarea", "Pemberian Obat", "", "y", null, true);
	$data_obat_table->addModal("counter_indication", "textarea", "Kontra Indikasi", "", "y", null, true);
	$data_obat_table->addModal("attention", "textarea", "Perhatian", "", "y", null, true);
	$data_obat_table->addModal("side_effect", "textarea", "Efek Samping", "", "y", null, true);
	$data_obat_table->addModal("interaction", "textarea", "Interaksi Obat", "", "y", null, true);
	$data_obat_table->addModal("price", "text", "Harga*", "", "y", null, true);
	$data_obat_modal = $data_obat_table->getModal();
	$data_obat_modal->setTitle("Data Obat");
	$data_obat_modal->clearFooter();
	
	echo $data_obat_modal->getHtml();
	echo $data_obat_table->getHtml();
	echo addJS("framework/smis/js/table_action.js");
?>
<script type="text/javascript">
	function DataObatAction(name, page, action, column) {
		this.initialize(name, page, action, column);
	}
	DataObatAction.prototype.constructor = DataObatAction;
	DataObatAction.prototype = new TableAction();
	DataObatAction.prototype.getViewData = function() {
		var data = TableAction.prototype.getViewData.call(this);
		data['search_nomor'] = $("#search_nomor").val();
		data['search_nama'] = $("#search_nama").val();
		data['search_produsen'] = $("#search_produsen").val();
		data['search_kemasan'] = $("#search_kemasan").val();
		data['search_indikasi'] = $("#search_indikasi").val();
		return data;
	};
	DataObatAction.prototype.detail = function(id) {
		var data = TableAction.prototype.getEditData.call(this, id);
		$.post(
			"",
			data,
			function(response) {
				var json = getContent(response);
				if (json == null) return;
				$("#data_obat_id").val(id);
				$("#data_obat_name").val(json.name);
				if (json.brand == "")
					$(".data_obat_brand").hide();
				else {
					$(".data_obat_brand").show();
					$("#data_obat_brand").val(json.brand);
				}
				if (json.packaging == "")
					$(".data_obat_packaging").hide();
				else {
					$(".data_obat_packaging").show();
					$("#data_obat_packaging").val(json.packaging);
				}
				if (json.indication == "")
					$(".data_obat_indication").hide();
				else {
					$(".data_obat_indication").show();
					$("#data_obat_indication").val(json.indication);
				}
				if (json.dosage == "")
					$(".data_obat_dosage").hide();
				else {
					$(".data_obat_dosage").show();
					$("#data_obat_dosage").val(json.dosage);
				}
				if (json.consuming == "")
					$(".data_obat_consuming").hide();
				else {
					$(".data_obat_consuming").show();
					$("#data_obat_consuming").val(json.consuming);
				}
				if (json.counter_indication == "")
					$(".data_obat_counter_indication").hide();
				else {
					$(".data_obat_counter_indication").show();
					$("#data_obat_counter_indication").val(json.counter_indication);
				}
				if (json.attention == "")
					$(".data_obat_attention").hide();
				else {
					$(".data_obat_attention").show();
					$("#data_obat_attention").val(json.attention);
				}
				if (json.side_effect == "")
					$(".data_obat_side_effect").hide();
				else {
					$(".data_obat_side_effect").show();
					$("#data_obat_side_effect").val(json.side_effect);
				}
				if (json.interaction == "")
					$(".data_obat_interaction").hide();
				else {
					$(".data_obat_interaction").show();
					$("#data_obat_interaction").val(json.interaction);
				}
				if (json.price == "")
					$(".data_obat_price").hide();
				else {
					$(".data_obat_price").show();
					$("#data_obat_price").val(json.price);
				}
				$("#data_obat_add_form").smodal("show");
			}
		);
	};
	
	var data_obat;
	$(document).ready(function() {
		$('[data-toggle="popover"]').popover({
			trigger: 'hover',
			'placement': 'top'
		});
		data_obat = new DataObatAction(
			"data_obat",
			"kamus_data",
			"data_obat",
			new Array()
		);
		data_obat.view();
	});
</script>