<?php
	global $db;
	global $user;
	require_once 'smis-libs-class/Policy.php';

	$policy = new Policy("kamus_data", $user);
	$policy->addPolicy("data_obat", "data_obat", Policy::$DEFAULT_COOKIE_CHANGE);
	$policy->addPolicy("data_produsen", "data_produsen", Policy::$DEFAULT_COOKIE_CHANGE);
	$policy->setDefaultPolicy(Policy::$DEFAULT_POLICY_RESTRICT);
	$policy->initialize();
?>