<?php
	global $db;
	global $user;
	require_once 'smis-libs-class/Policy.php';
	$policy=new Policy("smis-social-network", $user);
	$policy->setDefaultPolicy(Policy::$DEFAULT_POLICY_ALLOW);
	$policy->addPolicy("facebook","facebook",Policy::$DEFAULT_COOKIE_CHANGE,"modul/facebook");
	$policy->addPolicy("settings_facebook","facebook",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/facebook/settings_facebook");
    $policy->addPolicy("slack","slack",Policy::$DEFAULT_COOKIE_CHANGE,"modul/slack");
	$policy->addPolicy("settings_slack","slack",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/slack/settings_slack");
    $policy->initialize();
?>