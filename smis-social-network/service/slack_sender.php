<?php 
global $db;
require_once "smis-social-network/class/other/SlackMessage.php";
$message = $_POST['slack_message'];
$slack   = new SlackMessage($db);
$slack   ->setSlackMessage($message);
$result  = $slack->sendDirect();

if($result){
    echo json_encode("1");
}else{
    echo json_encode("0");
}

?>