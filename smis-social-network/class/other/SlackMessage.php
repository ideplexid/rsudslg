<?php 
/**
 * this class handling for make slack command
 * so the system can do and report any problem
 * throgh slack
 * 
 * @author      : Nurul Huda
 * @since       : 22 Des 2017
 * @copyright   : goblooge@gmail.com
 * @license     : LGPLv3
 * @version     : 1.0.0
 * */
 
class SlackMessage {
    private $slack_message;
    private $current_message;
    private $db;
    
    public function __construct(Database $db){
        $this->db                           = $db;
        $this->slack_message                = array();
        $this->slack_message['attachments'] = array();
        $this->current_message              = 0;
    }
    
    public function setSlackMessage($message){
        $this->slack_message=$message;
        return $this;
    }
    
    public function getSlackMessage(){
        return $this->slack_message;
    }
    
    public function createNewAttachMent($number=-1){
        if($number==-1){
            $this->current_message=count($this->slack_message['attachments']);
        }else{
            $this->current_message=number;
        }
        $this->slack_message['attachments'][$this->current_message]=array();
        return $this;
    }
    
    public function setCurrentMessage($number){
        $this->current_message=$number;
        return $this;
    }
    
    public function setFallBack($string){
        $this->slack_message['attachments'][$this->current_message]["fallback"]=$string;
        return $this;
    }
    
    public function setColor($string){
        $this->slack_message['attachments'][$this->current_message]["color"]=$string;
        return $this;
    }
    
    public function setPreText($string){
        $this->slack_message['attachments'][$this->current_message]["pretext"]=$string;
        return $this;
    }
    
    public function setAuthorName($string){
        $this->slack_message['attachments'][$this->current_message]["author_name"]=$string;
        return $this;
    }
    
    public function setAuthorLink($string){
        $this->slack_message['attachments'][$this->current_message]["author_link"]=$string;
        return $this;
    }
    
    public function setAuthorIcon($string){
        $this->slack_message['attachments'][$this->current_message]["author_icon"]=$string;
        return $this;
    }
    
    public function setTitle($string){
        $this->slack_message['attachments'][$this->current_message]["title"]=$string;
        return $this;
    }
    
    public function setTitleLink($string){
        $this->slack_message['attachments'][$this->current_message]["title_link"]=$string;
        return $this;
    }
    
    public function setText($string){
        $this->slack_message['attachments'][$this->current_message]["text"]=$string;
        return $this;
    }
    
    public function setImageUrl($string){
        $this->slack_message['attachments'][$this->current_message]["image_url"]=$string;
        return $this;
    }
    
    public function setThumbUrl($string){
        $this->slack_message['attachments'][$this->current_message]["thumb_url"]=$string;
        return $this;
    }
    
    public function setFooter($string){
        $this->slack_message['attachments'][$this->current_message]["footer"]=$string;
        return $this;
    }
    
    public function setFooterIcon($string){
        $this->slack_message['attachments'][$this->current_message]["footer_icon"]=$string;
        return $this;
    }
    
    public function setTimeSecond($ts=null){
        if($ts==null){
            $ts=time();
        }
        $this->slack_message['attachments'][$this->current_message]["ts"]=$ts;
    }
    
    public function addField($title,$value,$short=false){
        if(!isset($this->slack_message['attachments'][$this->current_message]["fields"])){
            $this->slack_message['attachments'][$this->current_message]["fields"]=array();
        }
        $onefield=array();
        $onefield['title']=$title;
        $onefield['value']=$value;
        $onefield['short']=$short;
        $this->slack_message['attachments'][$this->current_message]["fields"][]=$onefield;
        return $this;
    }
    
    public function getMessage(){
        return $this->slack_message;
    }
    
    public function sendDirect($url=null){
        if($url==null){
            $url                   = getSettings($this->db,"slack_access_url","");
        }
        if($url==""){
            return false;
        }        
        $options                   = array();
        $options['http']           = array();
        $options['http']['header'] = "Content-type: application/x-www-form-urlencoded\r\n";
        $options['http']['method'] = "POST";
        $options['http']['content']= json_encode($this->slack_message);        
        $context                   = stream_context_create($options);
        $result                    = file_get_contents($url, false, $context);
        if ($result === false) { 
            return false;
        }else{
            return true;
        }
    }
    
    public function send($url=null){
        if(getSettings($this->db,"slack_type","1")=="1"){
            $result=$this->sendDirect($url);
        }else{
            $result=$this->sendThroughService();
        }
        return $this;
    }
    
    public function sendThroughService(){
        require_once "smis-base/smis-include-service-consumer.php";
        $data['slack_message'] = $this->slack_message;
        $serv=new ServiceConsumer($this->db,"slack_sender",$data,"smis-social-network");
        $serv->execute();
        $result=$serv->getContent();
        return $result=="1";
    }
    
}

?>