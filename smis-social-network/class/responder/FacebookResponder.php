<?php 

class FacebookResponder extends Responder{
    private $id;
    private $ip;
    private $db;
    private $message;
    private $command;
    private $base_url;
    public function __construct($db,$id,$ip,$base_url){
        $this->id=$id;
        $this->ip=$ip;
        $this->db=$db;
        $this->message=array();
        $this->message['recipient']=array();
        $this->message['recipient']['id']=$id;
        $this->message['message']=array();
        $this->base_url=$base_url;
    }
    
    public function command($command){
        $_SESSION['command']=$command;
        $command=explode("#",$command);
    }
    
    private function commandNotFound(){
        
    }
    
    public function getMessage(){
        return $this->message;
    }
    
    public function setAttacmentMessage($fileposition){
        if(!isset($this->message['message']['attachment'])){
            $this->message['message']['attachment']=array();
            $this->message['message']['attachment']['type']="file";
        }
        $this->message['message']['attachment']['payload']=array("url"=>$this->base_url."/".$fileposition);
    }
    
    
    public function appendTextMessage($msg){
        if(!isset($this->message['message']['text'])){            
            $this->message['message']['text']="";
        }
        $this->message['message']['text'].=$msg;
    }
    
    public function setTextMessage($msg){
        if(!isset($this->message['message']['text'])){
            $this->message['message']['text']="";
        }
        $this->message['message']['text']=$msg;
    }
    
	public function view(){}
}

?>