<?php
require_once 'smis-framework/smis/api/SettingsBuilder.php';
global $db;
$settings = new SettingsBuilder ( $db, "smis_social_network", "smis-social-network", "settings_facebook","" );
$settings->setShowDescription ( true );
$settings->addTabs ( "oauth", "Authentification"," fa fa-key" );
$settings->addItem ( "oauth", new SettingsItem ( $db, "facebook_access_token", "Access Token", "", "text", "Token Acces From Facebook" ) );
$settings->addItem ( "oauth", new SettingsItem ( $db, "facebook_verify_token", "Verify Token", "", "text", "Verify Token Acces From Facebook" ) );
$settings->addItem ( "oauth", new SettingsItem ( $db, "facebook_tunnel_url", "Tunnel URL", "", "text", "Set Facebook Tunnel URL" ) );
$settings->init ();

?>