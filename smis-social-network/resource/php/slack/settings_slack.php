<?php
require_once 'smis-framework/smis/api/SettingsBuilder.php';
global $db;
$settings = new SettingsBuilder ( $db, "smis_social_network", "smis-social-network", "settings_slack","" );
$settings->setShowDescription ( true );

$integration_type=new OptionBuilder();
$integration_type->add("Service Integration","1","0");
$integration_type->add("Direct Integration","0","1");

$settings->addTabs ( "url", "URL"," fa fa-key" );
$settings->addItem ( "url", new SettingsItem ( $db, "slack_access_url", "Access URL", "", "text", "Slack Acces URL" ) );
$settings->addItem ( "url", new SettingsItem ( $db, "slack_enable", "Enable Slack Integration", "0", "checkbox", "Enable Slack Integration" ) );
$settings->addItem ( "url", new SettingsItem ( $db, "slack_type", "Slack Type Integration", $integration_type->getContent(), "select", "Slack Integration Type" ) );
$settings->addItem ( "url", new SettingsItem ( $db, "slack_name", "Slack Autonomous Name", "", "text", "Nama Server Name yang akan muncul dalam Notif Slack" ) );
$settings->init ();

?>