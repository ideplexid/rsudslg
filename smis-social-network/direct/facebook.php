<?php 

global $db;
global $user;

/*verify access token*/
$access_token = getSettings($db,"facebook_access_token","");
$verify_token = getSettings($db,"facebook_verify_token","");
$base_url     = getSettings($db,"facebook_tunnel_url","");
$hub_verify_token = null;
if(isset($_REQUEST['hub_challenge'])) {
    $challenge = $_REQUEST['hub_challenge'];
    $hub_verify_token = $_REQUEST['hub_verify_token'];
}
if ($hub_verify_token === $verify_token) {
    /* testing verify access token */
    echo $challenge;
    return;
}

/* end of verify access token */
$input = json_decode(file_get_contents("php://input"), true);
$sender = $input["entry"][0]["messaging"][0]["sender"]["id"];
$message = $input["entry"][0]["messaging"][0]["message"]["text"];
$ip=$user->getIp();
$user->setRawUser($sender,$ip);
$url = "https://graph.facebook.com/v2.6/me/messages?access_token=".$access_token;
$ch = curl_init($url);

require_once "smis-social-network/class/responder/FacebookResponder.php";
$responder=new FacebookResponder($db,$sender,$ip,$base_url);
$responder->command($message);
$result=$responder->getMessage();

/* encoding data to send to facebook */
$jsonDataEncoded = json_encode($result);
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonDataEncoded);
curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
if(!empty($input['entry'][0]['messaging'][0]['message'])){
    $result = curl_exec($ch);
}
?> 
