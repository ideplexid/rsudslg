<?php
	global $NAVIGATOR;
	$mr = new Menu ("fa fa-share-alt");
	$mr->addProperty ( 'title', 'Social Network' );
	$mr->addProperty ( 'name', 'Social Network' );
	$mr->addSubMenu ( "Facebook", "smis-social-network", "facebook", "Facebook API" ,"fa fa-facebook-official");	
	$mr->addSubMenu ( "Slack", "smis-social-network", "slack", "Slack API" ,"fa fa-slack");	
	$NAVIGATOR->addMenu ( $mr, 'smis-social-network' );
?>
