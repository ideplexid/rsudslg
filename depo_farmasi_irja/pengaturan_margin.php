<?php
	require_once("depo_farmasi_irja/library/InventoryLibrary.php");
	require_once("smis-base/smis-include-service-consumer.php");
	global $db;

	$table = new Table(
		array("No.", "ID Obat", "Kode Obat", "Nama Obat", "Jenis Obat", "Margin"),
		"Depo Farmasi IRJA : Pengaturan Margin Jual Per Obat",
		null,
		true
	);
	$table->setName("pengaturan_margin");

	$obat_table = new Table(
		array("No.", "ID Obat", "Kode Obat", "Nama Obat", "Jenis Obat"),
		"",
		null,
		true
	);
	$obat_table->setName("obat");
	$obat_table->setModel(Table::$SELECT);
	$obat_adapter = new SimpleAdapter(true, "No.");
	$obat_adapter->add("ID Obat", "id", "digit6");
	$obat_adapter->add("Kode Obat", "kode");
	$obat_adapter->add("Nama Obat", "nama");
	$obat_adapter->add("Jenis Obat", "nama_jenis_barang");
	$obat_service_responder = new ServiceResponder(
		$db,
		$obat_table,
		$obat_adapter,
		"get_daftar_barang_m"
	);
	$super_command = new SuperCommand();
	$super_command->addResponder("obat", $obat_service_responder);
	$init = $super_command->initialize();
	if ($init != null) {
		echo $init;
		return;
	}

	if (isset($_POST['command'])) {
		$adapter = new SimpleAdapter(true, "No.");
		$adapter->add("ID Obat", "id_obat", "digit6");
		$adapter->add("Kode Obat", "kode_obat");
		$adapter->add("Nama Obat", "nama_obat");
		$adapter->add("Jenis Obat", "nama_jenis_obat");
		$adapter->add("Margin", "margin_jual", "money");
		$dbtable = new DBTable($db, InventoryLibrary::$_TBL_MARGIN_JUAL_OBAT);
		if ($_POST['command'] == "get_max_hpp") {
			$id_obat = $_POST['id_obat'];
			$row = $dbtable->get_row("
				SELECT MAX(a.hna) AS 'hpp'
				FROM " . InventoryLibrary::$_TBL_STOK_OBAT . " a LEFT JOIN " . InventoryLibrary::$_TBL_OBAT_MASUK . " b ON a.id_obat_masuk = b.id
				WHERE a.prop NOT LIKE 'del' AND b.prop NOT LIKE 'del' AND b.status = 'sudah' AND a.id_obat = '" . $id_obat . "'
			");
			$hpp = "Rp. 0,00";
			if ($row != null)
				$hpp = $row->hpp == 0 ? "Rp. 0,00" : ArrayAdapter::format("only-money", $row->hpp);
			echo json_encode(array(
				"hpp" => $hpp
			));
			return;
		}
		$dbresponder = new DBResponder(
			$dbtable,
			$table,
			$adapter
		);
		$data = $dbresponder->command($_POST['command']);
		echo json_encode($data);
		return;
	}

	$modal = new Modal("pengaturan_margin_add_form", "smis_form_container", "pengaturan_margin");
	$modal->setTitle("Data Margin Jual Obat");
	$id_hidden = new Hidden("pengaturan_margin_id", "pengaturan_margin_id", "");
	$modal->addElement("", $id_hidden);
	$id_obat_text = new Text("pengaturan_margin_id_obat", "pengaturan_margin_id_obat", "");
	$id_obat_text->setAtribute("disabled='disabled'");
	$modal->addElement("ID Obat", $id_obat_text);
	$kode_obat_text = new Text("pengaturan_margin_kode_obat", "pengaturan_margin_kode_obat", "");
	$kode_obat_text->setAtribute("disabled='disabled'");
	$modal->addElement("Kode Obat", $kode_obat_text);
	$nama_obat_text = new Text("pengaturan_margin_nama_obat", "pengaturan_margin_nama_obat", "");
	$nama_obat_text->setAtribute("disabled='disabled'");
	$nama_obat_text->setClass("smis-one-option-input");
	$nama_obat_button = new Button("", "", "Pilih");
	$nama_obat_button->setAtribute("id='nama_obat_btn'");
	$nama_obat_button->setClass("btn-info");
	$nama_obat_button->setIsButton(Button::$ICONIC);
	$nama_obat_button->setIcon("icon-white ".Button::$icon_list_alt);
	$nama_obat_button->setAction("obat.chooser('obat', 'obat_button', 'obat', obat)");
	$nama_obat_input_group = new InputGroup("");
	$nama_obat_input_group->addComponent($nama_obat_text);
	$nama_obat_input_group->addComponent($nama_obat_button);
	$modal->addElement("Nama Obat", $nama_obat_input_group);
	$nama_jenis_obat_text = new Text("pengaturan_margin_nama_jenis_obat", "pengaturan_margin_nama_jenis_obat", "");
	$nama_jenis_obat_text->setAtribute("disabled='disabled'");
	$modal->addElement("Jenis Obat", $nama_jenis_obat_text);
	$hpp_max_text = new Text("pengaturan_margin_hpp_max", "pengaturan_margin_hpp_max", "");
	$hpp_max_text->setAtribute("disabled='disabled'");
	$modal->addElement("Maks. HPP *", $hpp_max_text);
	$hja_text = new Text("pengaturan_margin_hja", "pengaturan_margin_hja", "");
	$hja_text->setTypical("money");
	$hja_text->setAtribute(" data-thousands=\".\" data-decimal=\",\"  data-precision=\"2\" " );
	$modal->addElement("HJA", $hja_text);
	$margin_jual_text = new Text("pengaturan_margin_margin_jual", "pengaturan_margin_margin_jual", "");
	$modal->addElement("Margin Jual", $margin_jual_text);
	$save_button = new Button("", "", "Simpan");
	$save_button->setClass("btn-success");
	$save_button->setAction("pengaturan_margin.save()");
	$save_button->setIcon("fa fa-floppy-o");
	$save_button->setIsButton(Button::$ICONIC);
	$modal->addFooter($save_button);
	
	echo $modal->getHtml();
	echo $table->getHtml();
	echo addJS("framework/smis/js/table_action.js");
	echo addJS("depo_farmasi_irja/js/pengaturan_margin_obat_action.js", false);
	echo addJS("depo_farmasi_irja/js/pengaturan_margin_action.js", false);
	echo addJS("depo_farmasi_irja/js/pengaturan_margin.js", false);
?>