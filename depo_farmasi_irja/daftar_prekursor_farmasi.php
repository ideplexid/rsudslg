<?php
	require_once("smis-base/smis-include-service-consumer.php");
	global $db;

	$table = new Table(
		array("No.", "ID Obat", "Kode Obat", "Nama Obat", "Jenis Obat"),
		"Depo Depo Farmasi IRJA : Daftar Prekursor Farmasi",
		true,
		null
	);
	$table->setName("daftar_prekursor_farmasi");
	$table->setAction(false);
	$adapter = new SimpleAdapter(true, "No.");
	$adapter->add("ID Obat", "id");
	$adapter->add("Kode Obat", "kode");
	$adapter->add("Nama Obat", "nama");
	$adapter->add("Jenis Obat", "nama_jenis_barang");
	$service_responder = new ServiceResponder(
		$db,
		$table,
		$adapter,
		"daftar_prekursor_farmasi"
	);
	$super_command = new SuperCommand();
	$super_command->addResponder("daftar_prekursor_farmasi", $service_responder);
	$init = $super_command->initialize();
	if ($init != null) {
		echo $init;
		return;
	}

	echo $table->getHtml();
	echo addJS("framework/smis/js/table_action.js");	
?>
<script type="text/javascript">
	var daftar_prekursor_farmasi;
	$(document).ready(function() {
		daftar_prekursor_farmasi = new TableAction(
			"daftar_prekursor_farmasi",
			"depo_farmasi_irja",
			"daftar_prekursor_farmasi",
			new Array()
		);
		daftar_prekursor_farmasi.setSuperCommand("daftar_prekursor_farmasi");
		daftar_prekursor_farmasi.view();
	});
</script>