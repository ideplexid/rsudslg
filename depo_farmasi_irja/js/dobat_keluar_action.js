function DObatKeluarAction(name, page, action, column) {
	this.initialize(name, page, action, column);
}
DObatKeluarAction.prototype.constructor = DObatKeluarAction;
DObatKeluarAction.prototype = new TableAction();
DObatKeluarAction.prototype.show_add_form = function() {
	$("#dobat_keluar_id").val("");
	$("#dobat_keluar_id_obat").val("");
	$("#dobat_keluar_kode_obat").val("");
	$("#dobat_keluar_nama_jenis_obat").val("");
	$("#dobat_keluar_name_obat").val("");
	$("#dobat_keluar_nama_obat").val("");
	$("#obat_browse").removeAttr("onclick");
	$("#obat_browse").attr("onclick", "obat.chooser('obat', 'obat_button', 'obat', obat)");
	$("#obat_browse").removeClass("btn-info");
	$("#obat_browse").removeClass("btn-inverse");
	$("#obat_browse").addClass("btn-info");
	$("#dobat_keluar_satuan").html("");
	$("#dobat_keluar_satuan").removeAttr("onchange");
	$("#dobat_keluar_satuan").attr("onchange", "obat.setDetailInfo()");
	$("#dobat_keluar_satuan").removeAttr("disabled");
	$("#dobat_keluar_satuan").val("");
	$("#dobat_keluar_konversi").val("");
	$("#dobat_keluar_satuan_konversi").val("");
	$("#dobat_keluar_harga_ma").val("");
	$("#dobat_keluar_stok").val("");
	$("#dobat_keluar_f_stok").val("");
	$("#dobat_keluar_jumlah_diminta").val("");
	$("#dobat_keluar_jumlah_lama").val(0);
	$("#dobat_keluar_jumlah").val("");
	$("#modal_alert_dobat_keluar_add_form").html("");
	$(".error_field").removeClass("error_field");
	$("#dobat_keluar_save").removeAttr("onclick");
	$("#dobat_keluar_save").attr("onclick", "dobat_keluar.save()");
	$("#dobat_keluar_add_form").smodal("show");
};
DObatKeluarAction.prototype.validate = function() {
	var valid = true;
	var invalid_msg = "";
	var nama_obat = $("#dobat_keluar_name_obat").val();
	var satuan = $("#dobat_keluar_satuan").val();
	var stok = $("#dobat_keluar_stok").val();
	var jumlah = $("#dobat_keluar_jumlah").val();
	$(".error_field").removeClass("error_field");
	if (nama_obat == "") {
		valid = false;
		invalid_msg += "</br><strong>Nama Obat</strong> tidak boleh kosong";
		$("#dobat_keluar_nama_obat").addClass("error_field");
	}
	if (satuan == "") {
		valid = false;
		invalid_msg += "</br><strong>Satuan</strong> tidak boleh kosong";
		$("#dobat_keluar_satuan").addClass("error_field");
	}
	if (jumlah == "") {
		valid = false;
		invalid_msg += "</br><strong>Jumlah</strong> tidak boleh kosong";
		$("#dobat_keluar_jumlah").addClass("error_field");
	} else if (!is_numeric(jumlah)) {
		valid = false;
		invalid_msg += "</br><strong>Jumlah</strong> seharusnya numerik (0-9)";
		$("#dobat_keluar_jumlah").addClass("error_field");
	} else if (stok != "" && is_numeric(stok) && parseFloat(jumlah) > parseFloat(stok)) {
		valid = false;
		invalid_msg += "</br><strong>Jumlah</strong> tidak boleh melebihi stok";
		$("#dobat_keluar_jumlah").addClass("error_field");
	}
	if (!valid) {
		$("#modal_alert_dobat_keluar_add_form").html(
			"<div class='alert alert-block alert-danger'>" +
				"<h4>Peringatan</h4>" +
				invalid_msg +
			"</div>"
		);
	}
	return valid;
};
DObatKeluarAction.prototype.save = function() {
	if (!this.validate()) {
		return;
	}
	var dobat_keluar_id = $("#dobat_keluar_id").val();
	var dobat_keluar_id_obat = $("#dobat_keluar_id_obat").val();
	var dobat_keluar_kode_obat = $("#dobat_keluar_kode_obat").val();
	var dobat_keluar_nama_jenis_obat = $("#dobat_keluar_nama_jenis_obat").val();
	var dobat_keluar_nama_obat = $("#dobat_keluar_name_obat").val();
	var dobat_keluar_harga_ma = $("#dobat_keluar_harga_ma").val();
	var dobat_keluar_stok_entri = $("#dobat_keluar_stok").val();
	var dobat_keluar_jumlah_diminta = $("#dobat_keluar_jumlah_diminta").val();
	var dobat_keluar_jumlah_lama = $("#dobat_keluar_jumlah_lama").val();
	var dobat_keluar_jumlah = $("#dobat_keluar_jumlah").val();
	var dobat_keluar_satuan = $("#dobat_keluar_satuan").find(":selected").text();
	var dobat_keluar_konversi = $("#dobat_keluar_konversi").val();
	var dobat_keluar_satuan_konversi = $("#dobat_keluar_satuan_konversi").val();
	$("tbody#dobat_keluar_list").append(
		"<tr id='data_" + row_id + "'>" +
			"<td style='display: none;' id='data_" + row_id + "_id'>" + dobat_keluar_id + "</td>" +
			"<td style='display: none;' id='data_" + row_id + "_kode_obat'>" + dobat_keluar_kode_obat + "</td>" +
			"<td style='display: none;' id='data_" + row_id + "_id_obat'>" + dobat_keluar_id_obat + "</td>" +
			"<td style='display: none;' id='data_" + row_id + "_harga_ma'>" + dobat_keluar_harga_ma + "</td>" +
			"<td style='display: none;' id='data_" + row_id + "_stok_entri'>" + dobat_keluar_stok_entri + "</td>" +
			"<td style='display: none;' id='data_" + row_id + "_jumlah_diminta'>" + dobat_keluar_jumlah_diminta + "</td>" +
			"<td style='display: none;' id='data_" + row_id + "_jumlah_lama'>" + dobat_keluar_jumlah_lama + "</td>" +
			"<td style='display: none;' id='data_" + row_id + "_jumlah'>" + dobat_keluar_jumlah + "</td>" +
			"<td style='display: none;' id='data_" + row_id + "_satuan'>" + dobat_keluar_satuan + "</td>" +
			"<td style='display: none;' id='data_" + row_id + "_konversi'>" + dobat_keluar_konversi + "</td>" +
			"<td style='display: none;' id='data_" + row_id + "_satuan_konversi'>" + dobat_keluar_satuan_konversi + "</td>" +
			"<td id='data_" + row_id + "_nomor'></td>" +
			"<td id='data_" + row_id + "_nama_obat'>" + dobat_keluar_nama_obat + "</td>" +
			"<td id='data_" + row_id + "_nama_jenis_obat'>" + dobat_keluar_nama_jenis_obat + "</td>" +
			"<td id='data_" + row_id + "_f_jumlah_diminta'>" + dobat_keluar_jumlah_diminta + " " + dobat_keluar_satuan + "</td>" +
			"<td id='data_" + row_id + "_f_jumlah'>" + dobat_keluar_jumlah + " " + dobat_keluar_satuan + "</td>" +
			"<td id='data_" + row_id + "_keterangan'>1 " + dobat_keluar_satuan + " = " + dobat_keluar_konversi + " " + dobat_keluar_satuan_konversi + "</td>" +
			"<td>" + 
				"<div class='btn-group noprint'>" +
					"<a href='#' onclick='dobat_keluar.edit(" + row_id + ")' data-content='Ubah' data-toggle='popover' class='input btn btn-warning'>" + 
						"<i class='icon-edit icon-white'></i>" +
					"</a>" +
					"<a href='#' onclick='dobat_keluar.delete(" + row_id + ")' data-content='Hapus' data-toggle='popover' class='input btn btn-danger'>" + 
						"<i class='icon-remove icon-white'></i>" + 
					"</a>" +
				 "</div>" +
			"</td>" +
		"</tr>"
	);
	row_id++;
	obat_keluar.refreshNumber();
	$("#dobat_keluar_id").val("");
	$("#dobat_keluar_id_obat").val("");
	$("#dobat_keluar_kode_obat").val("");
	$("#dobat_keluar_nama_jenis_obat").val("");
	$("#dobat_keluar_name_obat").val("");
	$("#dobat_keluar_nama_obat").val("");
	$("#dobat_keluar_satuan").html("");
	$("#dobat_keluar_satuan").val("");
	$("#dobat_keluar_konversi").val("");
	$("#dobat_keluar_satuan_konversi").val("");
	$("#dobat_keluar_harga_ma").val("");
	$("#dobat_keluar_stok").val("");
	$("#dobat_keluar_f_stok").val("");
	$("#dobat_keluar_jumlah_diminta").val("");
	$("#dobat_keluar_jumlah_lama").val(0);
	$("#dobat_keluar_jumlah").val("");
	$("#modal_alert_dobat_keluar_add_form").html("");
	$(".error_field").removeClass("error_field");
	$("#dobat_keluar_nama_obat").focus();
};
DObatKeluarAction.prototype.edit = function(r_num) {
	var dobat_keluar_id = $("#data_" + r_num + "_id").text();
	var dobat_keluar_id_obat = $("#data_" + r_num + "_id_obat").text();
	var dobat_keluar_kode_obat = $("#data_" + r_num + "_kode_obat").text();
	var dobat_keluar_nama_jenis_obat = $("#data_" + r_num + "_nama_jenis_obat").text();
	var dobat_keluar_nama_obat = $("#data_" + r_num + "_nama_obat").text();
	var dobat_keluar_harga_ma = $("#data_" + r_num + "_harga_ma").text();
	var dobat_keluar_stok_entri = $("#data_" + r_num + "_stok_entri").text();
	var dobat_keluar_jumlah_diminta = $("#data_" + r_num + "_jumlah_diminta").text();
	var dobat_keluar_jumlah = $("#data_" + r_num + "_jumlah").text();
	var dobat_keluar_satuan = $("#data_" + r_num + "_konversi").text() + "_" + $("#data_" + r_num + "_satuan_konversi").text();
	$("#dobat_keluar_id").val(dobat_keluar_id);
	$("#dobat_keluar_harga_ma").val(dobat_keluar_harga_ma);
	$("#dobat_keluar_jumlah_diminta").val(dobat_keluar_jumlah_diminta);
	$("#dobat_keluar_jumlah").val(dobat_keluar_jumlah);
	$("#dobat_keluar_satuan").html("");
	var self = this;
	var data = this.getRegulerData();
	data['super_command'] = "obat";
	data['command'] = "edit";
	data['id'] = dobat_keluar_id_obat;
	$.post(
		"",
		data,
		function(response) {
			var json = getContent(response);
			if (json == null) return;
			$("#dobat_keluar_id_obat").val(json.header.id_obat);
			$("#dobat_keluar_kode_obat").val(json.header.kode_obat);
			$("#dobat_keluar_name_obat").val(json.header.nama_obat);
			$("#dobat_keluar_nama_obat").val(json.header.nama_obat);
			$("#dobat_keluar_nama_jenis_obat").val(json.header.nama_jenis_obat);				
			$("#obat_browse").removeAttr("onclick");
			$("#obat_browse").attr("onclick", "obat.chooser('obat', 'obat_button', 'obat', obat)");
			$("#obat_browse").removeClass("btn-info");
			$("#obat_browse").removeClass("btn-inverse");
			$("#obat_browse").addClass("btn-info");
			$("#dobat_keluar_satuan").html(json.satuan_option);
			$("#dobat_keluar_satuan").val(dobat_keluar_satuan);
			var part = $("#dobat_keluar_satuan").val().split("_");
			$("#dobat_keluar_konversi").val(part[0]);
			$("#dobat_keluar_satuan_konversi").val(part[1]);
			data = self.getRegulerData();
			data['super_command'] = "sisa";
			data['command'] = "edit";
			data['id_obat'] = $("#dobat_keluar_id_obat").val();
			data['satuan'] = $("#dobat_keluar_satuan").find(":selected").text();
			data['konversi'] = $("#dobat_keluar_konversi").val();
			data['satuan_konversi'] = $("#dobat_keluar_satuan_konversi").val();
			$.post(
				"",
				data,
				function(response) {
					var json = getContent(response);
					if (json == null) return;
					$("#dobat_keluar_stok").val(json.sisa);
					$("#dobat_keluar_f_stok").val(json.sisa + " " + json.satuan);
					$("#dobat_keluar_satuan").removeAttr("disabled");
					$("#modal_alert_dobat_keluar_add_form").html("");
					$(".error_field").removeClass("error_field");
					$("#dobat_keluar_save").removeAttr("onclick");
					$("#dobat_keluar_save").attr("onclick", "dobat_keluar.update(" + r_num + ")");
					$("#dobat_keluar_add_form").smodal("show");
				}
			);
		}
	);
};
DObatKeluarAction.prototype.update = function(r_num) {
	if (!this.validate()) {
		return;
	}
	var dobat_keluar_id_obat = $("#dobat_keluar_id_obat").val();
	var dobat_keluar_kode_obat = $("#dobat_keluar_kode_obat").val();
	var dobat_keluar_nama_jenis_obat = $("#dobat_keluar_nama_jenis_obat").val();
	var dobat_keluar_nama_obat = $("#dobat_keluar_name_obat").val();
	var dobat_keluar_harga_ma = $("#dobat_keluar_harga_ma").val();
	var dobat_keluar_stok_entri = $("#dobat_keluar_stok").val();
	var dobat_keluar_jumlah_diminta = $("#dobat_keluar_jumlah_diminta").val();
	var dobat_keluar_jumlah = $("#dobat_keluar_jumlah").val();
	var dobat_keluar_satuan = $("#dobat_keluar_satuan").find(":selected").text();
	var dobat_keluar_konversi = $("#dobat_keluar_konversi").val();
	var dobat_keluar_satuan_konversi = $("#dobat_keluar_satuan_konversi").val();
	$("#data_" + r_num + "_id_obat").text(dobat_keluar_id_obat);
	$("#data_" + r_num + "_kode_obat").text(dobat_keluar_kode_obat);
	$("#data_" + r_num + "_nama_obat").text(dobat_keluar_nama_obat);
	$("#data_" + r_num + "_nama_jenis_obat").text(dobat_keluar_nama_jenis_obat);
	$("#data_" + r_num + "_harga_ma").text(dobat_keluar_harga_ma);
	$("#data_" + r_num + "_stok_entri").text(dobat_keluar_stok_entri);
	$("#data_" + r_num + "_jumlah_diminta").text(dobat_keluar_jumlah_diminta);
	$("#data_" + r_num + "_jumlah").text(dobat_keluar_jumlah);
	$("#data_" + r_num + "_satuan").text(dobat_keluar_satuan);
	$("#data_" + r_num + "_konversi").text(dobat_keluar_konversi);
	$("#data_" + r_num + "_satuan_konversi").text(dobat_keluar_satuan_konversi);
	$("#data_" + r_num + "_f_jumlah").text(dobat_keluar_jumlah + " " + dobat_keluar_satuan);
	$("#data_" + r_num + "_keterangan").text("1 " + dobat_keluar_satuan + " = " + dobat_keluar_konversi + " " + dobat_keluar_satuan_konversi);
	$("#dobat_keluar_add_form").smodal("hide");
};
DObatKeluarAction.prototype.delete = function(r_num) {
	var id = $("#data_" + r_num + "_id").text();
	if (id.length == 0) {
		$("#data_" + r_num).remove();
	} else {
		$("#data_" + r_num).attr("style", "display: none;");
		$("#data_" + r_num).attr("class", "deleted");
	}
	obat_keluar.refreshNumber();
};