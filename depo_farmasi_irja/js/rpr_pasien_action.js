function PasienAction(name, page, action, column) {
	this.initialize(name, page, action, column);
}
PasienAction.prototype.constructor = PasienAction;
PasienAction.prototype = new TableAction();
PasienAction.prototype.selected = function(json) {
	$("#rpr_noreg_pasien").val(json.id);
	$("#rpr_nrm_pasien").val(json.nrm);
	$("#rpr_nama_pasien").val(json.sebutan + " " + json.nama_pasien);
};
PasienAction.prototype.addViewData = function(d) {
	d['noreg_pasien'] = $("#search_noreg").val();
	d['nrm_pasien'] = $("#search_nrm").val();
	d['nama_pasien'] = $("#search_nama").val();
	return  d;
};