function ObatAction(name, page, action, column) {
	this.initialize(name, page, action, column);
}
ObatAction.prototype.constructor = ObatAction;
ObatAction.prototype = new TableAction();
ObatAction.prototype.selected = function(json) {
	$("#dpenerimaan_obat_cito_id_obat").val(json.id);
	$("#dpenerimaan_obat_cito_kode_obat").val(json.kode);
	$("#dpenerimaan_obat_cito_nama_obat").val(json.nama);
	$("#dpenerimaan_obat_cito_name_obat").val(json.nama);
	$("#dpenerimaan_obat_cito_nama_jenis_obat").val(json.nama_jenis_barang);
	$("#dpenerimaan_obat_cito_jumlah").val("");
	var html_option = "<option value='" + json.konversi + "_" + json.satuan_konversi + "'>" + json.satuan + "</option>";
	if (json.konversi > 1)
		html_option += "<option value='1_" + json.satuan_konversi + "'>" + json.satuan_konversi + "</option>";
	$("#dpenerimaan_obat_cito_satuan").html(html_option);
	$("#dpenerimaan_obat_cito_satuan").val(json.konversi + "_" + json.satuan_konversi);
	$("#dpenerimaan_obat_cito_konversi").val(json.konversi);
	$("#dpenerimaan_obat_cito_satuan_konversi").val(json.satuan_konversi);
	$("#dpenerimaan_obat_cito_produsen").val(json.produsen);
};