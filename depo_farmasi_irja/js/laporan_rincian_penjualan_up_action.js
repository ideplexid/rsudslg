function DPUAction(name, page, action, column) {
	this.initialize(name, page, action, column);
}
DPUAction.prototype.constructor = DPUAction;
DPUAction.prototype = new TableAction();
DPUAction.prototype.getRegulerData = function() {
	var data = TableAction.prototype.getRegulerData.call(this);
	data['tanggal_from'] = $("#dpu_tanggal_from").val();
	data['tanggal_to'] = $("#dpu_tanggal_to").val();
	return data;
};
DPUAction.prototype.view = function() {
	if ($("#dpu_tanggal_from").val() == "" || $("#dpu_tanggal_to").val() == "")
		return;
	var self = this;
	FINISHED = false;
	$("#dpu_info").empty();
	$("#table_dpr tfoot").remove();
	$("#dpu_list").empty();
	$("#dpu_loading_bar").sload("true", "Harap ditunggu...", 0);
	$("#dpu_loading_modal").smodal("show");
	var data = this.getRegulerData();
	data['command'] = "get_jumlah";
	$.post(
		"",
		data,
		function(response) {
			var json = JSON.parse(response);
			if (json == null) return;
			self.fillHtml(0, json.jumlah);
		}
	);
};
DPUAction.prototype.fillHtml = function(num, limit) {
	if (FINISHED || num == limit) {
		if (FINISHED == false && num == limit) {
			this.finalize();
		} else {
			$("#dpu_loading_modal").smodal("hide");
			$("#dpu_info").html(
				"<div class='alert alert-block alert-inverse'>" +
					 "<center><strong>PROSES DIBATALKAN</strong></center>" +
				 "</div>"
			);
			$("#dpu_export_button").removeAttr("onclick");
		}
		return;
	}
	var self = this;
	var data = this.getRegulerData();
	data['command'] = "get_info";
	data['num'] = num;
	$.post(
		"",
		data,
		function(response) {
			var json = JSON.parse(response);
			if (json == null) return;
			console.log(json);
			$("tbody#dpu_list").append(
				json.html
			);
			$("#dpu_loading_bar").sload("true", json.nomor_transaksi + " - " + json.nomor_resep + " - " + json.nama_pasien + " (" + (num + 1) + " / " + limit + ")", (num + 1) * 100 / limit - 1);
			self.fillHtml(num + 1, limit);
		}
	);
};
DPUAction.prototype.finalize = function() {
	$("#dpu_loading_modal").smodal("hide");
	$("#dpu_info").html(
		"<div class='alert alert-block alert-info'>" +
			 "<center><strong>PROSES SELESAI</strong></center>" +
		 "</div>"
	);
	$("#dpu_export_button").removeAttr("onclick");
	$("#dpu_export_button").attr("onclick", "dpu.export_xls()");
};
DPUAction.prototype.cancel = function() {
	FINISHED = true;
};
DPUAction.prototype.export_xls = function() {
	showLoading();
	var num_rows = $("#dpu_list").children("tr").length;
	var d_data = {};
	for (var i = 0; i < num_rows; i++) {
		var label = $("tbody#dpu_list tr:eq(" + i + ") td#dpu_label").text();
		if (label == "data") {
			var nomor = $("tbody#dpu_list tr:eq(" + i + ") td#dpu_nomor").text();
			var tanggal = $("tbody#dpu_list tr:eq(" + i + ") td#dpu_tanggal").text();
			var id = $("tbody#dpu_list tr:eq(" + i + ") td#dpu_id").text();
			var lokasi = $("tbody#dpu_list tr:eq(" + i + ") td#dpu_lokasi").text();
			var nama_pasien = $("tbody#dpu_list tr:eq(" + i + ") td#dpu_nama_pasien").text();
			var no_resep = $("tbody#dpu_list tr:eq(" + i + ") td#dpu_no_resep").text();
			var nama_obat = $("tbody#dpu_list tr:eq(" + i + ") td#dpu_nama_obat").text();
			var harga = $("tbody#dpu_list tr:eq(" + i + ") td#dpu_harga").text();
			var jumlah = $("tbody#dpu_list tr:eq(" + i + ") td#dpu_jumlah").text();
			var subtotal = $("tbody#dpu_list tr:eq(" + i + ") td#dpu_subtotal").text();
			d_data[i] = {
				"label" 		: label,
				"nomor" 		: nomor,
				"tanggal" 		: tanggal,
				"id" 			: id,
				"lokasi" 		: lokasi,
				"nama_pasien"	: nama_pasien,
				"no_resep"		: no_resep,
				"nama_obat"		: nama_obat,
				"harga"			: harga,
				"jumlah"		: jumlah,
				"subtotal"		: subtotal
			};
		} else if (label == "diskon") {
			var diskon = $("tbody#dpu_list tr:eq(" + i + ") td#dpu_diskon").text();
			d_data[i] = {
				"label"			: "diskon",
				"diskon"		: diskon
			};
		} else if (label == "total") {
			var total = $("tbody#dpu_list tr:eq(" + i + ") td#dpu_total").text();
			d_data[i] = {
				"label"			: "total",
				"total"			: total
			};
		}
	}
	var data = this.getRegulerData();
	data['command'] = "export_xls";
	data['tanggal_from'] = $("#dpu_tanggal_from").val();
	data['tanggal_to'] = $("#dpu_tanggal_to").val();
	data['d_data'] = JSON.stringify(d_data);
	data['num_rows'] = num_rows;
	postForm(data);
	dismissLoading();
};