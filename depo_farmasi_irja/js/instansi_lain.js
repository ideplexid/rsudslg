var instansi_l;
$(document).ready(function() {
	$('[data-toggle="popover"]').popover({
		trigger: 'hover',
		'placement': 'top'
	});
	var instansi_l_columns = new Array("id", "nama", "alamat", "telpon", "margin_jual");
	instansi_l = new InstansiLainAction(
		"instansi_l",
		"depo_farmasi_irja",
		"instansi_lain",
		instansi_l_columns
	);
	instansi_l.view();
});