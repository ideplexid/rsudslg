function StokObatAction(name, page, action, column) {
	this.initialize(name, page, action, column);
}
StokObatAction.prototype.constructor = StokObatAction;
StokObatAction.prototype = new TableAction();
StokObatAction.prototype.selected = function(json) {
	$("#riwayat_stok_id_stok_obat").val(json.id);
	$("#riwayat_stok_nama_obat").val(json.nama_obat);
	$("#riwayat_stok_jenis_obat").val(json.nama_jenis_obat);
	var jenis_stok = json.label.replace("_", " ").toUpperCase();
	$("#riwayat_stok_jenis_stok").val(jenis_stok);
	$("#riwayat_stok_produsen").val(json.produsen);
	$("#riwayat_stok_vendor").val(json.nama_vendor);
	$("#riwayat_stok_satuan").val(json.satuan);
	if (json.tanggal_exp == "0000-00-00")
		$("#riwayat_stok_ed").val("-");
	else
		$("#riwayat_stok_ed").val(json.tanggal_exp);
	riwayat_stok.view();
};