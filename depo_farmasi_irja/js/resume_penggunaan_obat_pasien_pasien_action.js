function PasienAction(name, page, action, column) {
	this.initialize(name, page, action, column);
}
PasienAction.prototype.constructor = PasienAction;
PasienAction.prototype = new TableAction();
PasienAction.prototype.addViewData = function(d) {
	d['nrm_pasien'] = $("#search_nrm").val();
	d['nama_pasien'] = $("#search_nama").val();
	return  d;
};
PasienAction.prototype.selected = function(json) {
	$("#rpop_nama_pasien").val(json.nama);
	$("#rpop_nrm_pasien").val(json.id);
	$("#rpop_alamat_pasien").val(json.alamat);
	rpop.view();
};