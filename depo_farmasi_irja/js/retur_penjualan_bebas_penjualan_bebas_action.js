function PenjualanBebasAction(name, page, action, column) {
	this.initialize(name, page, action, column);
}
PenjualanBebasAction.prototype.constructor = PenjualanBebasAction;
PenjualanBebasAction.prototype = new TableAction();
PenjualanBebasAction.prototype.chooser = function(modal, elm_id, param,action, modal_title) {
	if ($(".btn").attr("disabled") == "disabled")
		return;
	$(".btn").removeAttr("disabled");
	$(".btn").attr("disabled", "disabled");
	var data = this.getRegulerData();
	var dt = this.addChooserData(data);
	var self = this;
	data['super_command'] = param;
	$.post(
		'',
		dt,
		function(res) {
			show_chooser(self, param, res, action.getShowParentModalInChooser(), modal_title);
			action.view();
			action.focusSearch();
			this.current_chooser=action;
			CURRENT_SMIS_CHOOSER=action;
			$(".btn").removeAttr("disabled");
		}
	);
};
PenjualanBebasAction.prototype.selected = function(json) {
	$("#retur_id_penjualan_bebas").val(json.header.id);
	$("#retur_dokter").val(json.header.nama_dokter);
	$("#retur_noreg").val(json.header.noreg_pasien);
	$("#retur_nrm").val(json.header.nrm_pasien);
	$("#retur_pasien").val(json.header.nama_pasien);
	$("#retur_alamat").val(json.header.alamat_pasien);
	$("tbody#dretur_list").html(json.detail_list);
};