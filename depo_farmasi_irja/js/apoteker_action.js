function ApotekerAction(name, page, action, column) {
	this.initialize(name, page, action, column);
}
ApotekerAction.prototype.constructor = ApotekerAction;
ApotekerAction.prototype = new TableAction();
ApotekerAction.prototype.chooser = function(modal, elm_id, param,action, modal_title) {
	if ($(".btn").attr("disabled") == "disabled")
		return;
	$(".btn").removeAttr("disabled");
	$(".btn").attr("disabled", "disabled");
	var data = this.getRegulerData();
	var dt = this.addChooserData(data);
	var self = this;
	data['super_command'] = param;
	$.post(
		'',
		dt,
		function(res) {
			show_chooser(self, param, res, action.getShowParentModalInChooser(), modal_title);
			action.view();
			action.focusSearch();
			this.current_chooser=action;
			CURRENT_SMIS_CHOOSER=action;
			$(".btn").removeAttr("disabled");
		}
	);
};
ApotekerAction.prototype.selected = function(json) {
	$("#obat_racikan_id_apoteker").val(json.id);
	$("#obat_racikan_nama_apoteker").val(json.nama);
	$("#obat_racikan_name_apoteker").val(json.nama);
	$("#obat_racikan_aturan_pakai").focus();
};