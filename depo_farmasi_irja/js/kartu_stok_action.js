function KartuStokAction(name, page, action, column) {
	this.initialize(name, page, action, column);
}
KartuStokAction.prototype.constructor = KartuStokAction;
KartuStokAction.prototype = new TableAction();
KartuStokAction.prototype.getRegulerData = function() {
	var data = TableAction.prototype.getRegulerData.call(this);
	data['id_obat'] = $("#ks_id_obat").val();
	data['kode_obat'] = $("#ks_kode_obat").val();
	data['nama_obat'] = $("#ks_nama_obat").val();
	data['nama_jenis_obat'] = $("#ks_nama_jenis_obat").val();
	data['tanggal_from'] = $("#ks_tanggal_from").val();
	data['tanggal_to'] = $("#ks_tanggal_to").val();
	return data;
};
KartuStokAction.prototype.view = function() {
	if ($("#ks_id_obat").val() == "")
		return;
	var self = this;
	$("#info").empty();
	$("#loading_bar").sload("true", "Harap ditunggu...", 0);
	$("#loading_modal").smodal("show");
	FINISHED = false;
	var data = this.getRegulerData();
	data['command'] = "get_jumlah_sisa";
	$.post(
		"",
		data,
		function(response) {
			var json = JSON.parse(response);
			if (json == null) return;
			$("#kartu_stok_list").empty();
			self.fill_html(0, json.jumlah, json.sisa);
		}
	);
};
KartuStokAction.prototype.fill_html = function(num, limit, sisa) {
	if (num == limit || FINISHED) {
		if (FINISHED == false && num == limit) {
			this.finalize();
		} else {
			$("#loading_modal").smodal("hide");
			$("#info").html(
				"<div class='alert alert-block alert-inverse'>" +
					 "<center><strong>PROSES DIBATALKAN</strong></center>" +
				 "</div>"
			);
			$("#download_button").removeAttr("onclick");
		}
		return;
	}
	var self = this;
	var data = this.getRegulerData();
	data['command'] = "get_info";
	data['num'] = num;
	data['sisa'] = sisa;
	$.post(
		"",
		data,
		function(response) {
			var json = JSON.parse(response);
			if (json == null) return;
			var html = $("#kartu_stok_list").html();
			$("#kartu_stok_list").html(json.html + html);
			$("#loading_bar").sload("true", json.id_obat + " - " + json.nama_obat + " - TANGGAL : " + json.tanggal + " - NO. BON : " + json.no_bon + " (" + (num + 1) + " / " + limit + ")", (num + 1) * 100 / limit - 1);
			self.fill_html(num + 1, limit, json.sisa);
		}
	);
};
KartuStokAction.prototype.finalize = function() {
	var num_rows = $("tbody#kartu_stok_list tr").length;
	for (var i = 0; i < num_rows; i++)
		$("tbody#kartu_stok_list tr:eq(" + i + ") td#nomor").html(i + 1);
	$("#loading_modal").smodal("hide");
	$("#info").html(
		"<div class='alert alert-block alert-info'>" +
			 "<center><strong>PROSES SELESAI</strong></center>" +
		 "</div>"
	);
	$("#download_button").removeAttr("onclick");
	$("#download_button").attr("onclick", "kartu_stok.download_au54()");
};
KartuStokAction.prototype.download_au54 = function() {
	showLoading();
	var num_rows = $("#kartu_stok_list").children("tr").length;
	var d_data = {};
	for (var i = 0; i < num_rows; i++) {
		var nomor = $("tbody#kartu_stok_list tr:eq(" + i + ") td#nomor").text();
		var tanggal = $("tbody#kartu_stok_list tr:eq(" + i + ") td#tanggal").text();
		var no_bon = $("tbody#kartu_stok_list tr:eq(" + i + ") td#no_bon").text();
		var unit = $("tbody#kartu_stok_list tr:eq(" + i + ") td#unit").text();
		var masuk = $("tbody#kartu_stok_list tr:eq(" + i + ") td#masuk").text();
		var keluar = $("tbody#kartu_stok_list tr:eq(" + i + ") td#keluar").text();
		var sisa = $("tbody#kartu_stok_list tr:eq(" + i + ") td#sisa").text();
		d_data[i] = {
			"nomor" 	: nomor,
			"tanggal" 	: tanggal,
			"no_bon" 	: no_bon,
			"unit" 		: unit,
			"masuk"		: masuk,
			"keluar"	: keluar,
			"sisa"		: sisa
		};
	}
	var data = this.getRegulerData();
	data['command'] = "export_excel";
	data['d_data'] = JSON.stringify(d_data);
	data['num_rows'] = num_rows;
	postForm(data);
	dismissLoading();
};