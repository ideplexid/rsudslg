<?php
    require_once("depo_farmasi_irja/library/InventoryLibrary.php");
    require_once("depo_farmasi_irja/responder/DataPaketObatDBResponder.php");
    global $db;

    $obat_table = new Table(array("No.", "ID Obat", "Kode Obat", "Nama Obat", "Jenis Obat", "Satuan"));
    $obat_table->setName("obat");
    $obat_table->setModel(Table::$SELECT);
    $obat_adapter = new SimpleAdapter(true, "No.");
    $obat_adapter->add("ID Obat", "id");
    $obat_adapter->add("Kode Obat", "kode_obat");
    $obat_adapter->add("Nama Obat", "nama_obat");
    $obat_adapter->add("Jenis Obat", "nama_jenis_obat");
    $obat_adapter->add("Satuan", "satuan");
    $obat_dbtable = new DBTable($db, InventoryLibrary::$_TBL_STOK_OBAT, array("nama_obat", "nama_jenis_obat"));
    $obat_dbtable->setViewForSelect(true);
    $filter = "";
    if (isset($_POST['kriteria'])) {
        $filter = " AND (a.nama LIKE '%" . $_POST['kriteria'] . "%' OR a.nama_jenis_barang LIKE '%" . $_POST['kriteria'] . "%')";
    }
    $query_value = "
        SELECT 
            *
        FROM 
            (
                SELECT DISTINCT 
                    a.id, 
                    a.kode kode_obat,
                    a.nama nama_obat, 
                    a.nama_jenis_barang nama_jenis_obat, 
                    a.satuan_konversi satuan, 
                    a.prop 
                FROM 
                    smis_pr_barang a 
                        INNER JOIN " . InventoryLibrary::$_TBL_STOK_OBAT . " b on a.id = b.id_obat 
                        INNER JOIN " . InventoryLibrary::$_TBL_OBAT_MASUK . " c on b.id_obat_masuk = c.id 
                WHERE 
                    a.prop = '' 
                        AND b.prop = '' 
                        AND c.prop = '' 
                        AND c.status = 'sudah' 
                        " . $filter . "
                ORDER BY 
                    a.nama asc
            ) " . InventoryLibrary::$_TBL_STOK_OBAT . "
    ";
    $query_count = "
        SELECT 
            COUNT(*)
        FROM 
            (
                SELECT DISTINCT 
                    a.id, 
                    a.kode kode_obat,
                    a.nama nama_obat, 
                    a.nama_jenis_barang nama_jenis_obat, 
                    a.satuan_konversi satuan, 
                    a.prop 
                FROM 
                    smis_pr_barang a 
                        INNER JOIN " . InventoryLibrary::$_TBL_STOK_OBAT . " b on a.id = b.id_obat 
                        INNER JOIN " . InventoryLibrary::$_TBL_OBAT_MASUK . " c on b.id_obat_masuk = c.id 
                WHERE 
                    a.prop = '' 
                        AND b.prop = '' 
                        AND c.prop = '' 
                        AND c.status = 'sudah' 
                        " . $filter . "
                ORDER BY 
                    a.nama asc
            ) " . InventoryLibrary::$_TBL_STOK_OBAT . "
    ";
    $obat_dbtable->setPreferredQuery(true, $query_value, $query_count);
    $obat_dbresponder = new DBResponder(
        $obat_dbtable,
        $obat_table,
        $obat_adapter
    );
    $super_command = new SuperCommand();
    $super_command->addResponder("obat", $obat_dbresponder);
    $init = $super_command->initialize();
    if ($init != null) {
        echo $init;
        return;
    }

    $table = new Table(
        array("No.", "Nama Paket", "Racikan/Non-Racikan", "Jumlah Obat"),
        "Depo Farmasi IRJA : Paket Obat",
        null,
        true
    );
    $table->setName("data_paket_obat");
    $table->setPrintButtonEnable(false);
    $table->setReloadButtonEnable(false);

    if (isset($_POST['command'])) {
        $dbtable = new DBTable($db, InventoryLibrary::$_TBL_PAKET_OBAT);
        $filter = "";
        if (isset($_POST['kriteria'])) {
            $filter = " AND (nama_paket LIKE '%" . $_POST['kriteria'] . "%') ";
        }
        $query_value = "
            SELECT
                CONCAT(nama_paket, '|', is_racikan) id, nama_paket, is_racikan, COUNT(id) jumlah_obat
            FROM
                " . InventoryLibrary::$_TBL_PAKET_OBAT . "
            WHERE
                prop = ''
                    " . $filter . "
            GROUP BY
                nama_paket, is_racikan
            ORDER BY
                nama_paket ASC
        ";
        $query_count = "
            SELECT 
                COUNT(*)
            FROM
                (" . $query_value . ") v
        ";
        $dbtable->setPreferredQuery(true, $query_value, $query_count);
        $adapter = new SimpleAdapter(true, "No.");
        $adapter->add("Nama Paket", "nama_paket");
        $adapter->add("Racikan/Non-Racikan", "is_racikan", "trivial_0_Non-Racikan_Racikan");
        $adapter->add("Jumlah Obat", "jumlah_obat", "number");
        $dbresponder = new DataPaketObatDBResponder(
            $dbtable,
            $table,
            $adapter
        );
        $data = $dbresponder->command($_POST['command']);
        echo json_encode($data);
        return;
    }

    $header_modal = new Modal("header_paket_obat_modal", "header_paket_obat_modal", "Data Paket Obat");
    $header_modal->setClass(Modal::$HALF_MODEL);
    $nama_paket_text = new Text("paket_obat_nama_paket", "paket_obat_nama_paket", "");
    $header_modal->addElement("Nama Paket", $nama_paket_text);
    $is_racikan_option = new OptionBuilder();
    $is_racikan_option->add("Ya", "1");
    $is_racikan_option->add("Tidak", "0", "1");
    $is_racikan_select = new Select("paket_obat_is_racikan", "paket_obat_is_racikan", $is_racikan_option->getContent());
    $header_modal->addElement("Racikan <small>(Ya/Tidak)</small>", $is_racikan_select);
    $detail_paket_obat_table = new Table(
        array("No.", "ID Obat", "Kode Obat", "Nama Obat", "Jenis Obat", "Jumlah", "Satuan")
    );
    $detail_paket_obat_table->setReloadButtonEnable(false);
    $detail_paket_obat_table->setPrintButtonEnable(false);
	$detail_paket_obat_table->setName("detail_paket_obat");
	$detail_paket_obat_table->setFooterVisible(false);
	$header_modal->addBody("detail_paket_obat_table", $detail_paket_obat_table);
    $save_button = new Button("", "", "Simpan");
    $save_button->setClass("btn-success");
    $save_button->setIcon("fa fa-floppy-o");
    $save_button->setIsButton(Button::$ICONIC);
    $save_button->setAction("data_paket_obat.save()");
    $header_modal->addFooter($save_button);

    $detail_modal = new Modal("detail_paket_obat_modal", "detail_paket_obat_modal", "Data Detail Paket Obat");
    $row_num_hidden = new Hidden("paket_obat_row_num", "paket_obat_row_num", "");
    $detail_modal->addElement("", $row_num_hidden);
    $id_obat_hidden = new Hidden("paket_obat_id_obat", "paket_obat_id_obat", "");
    $detail_modal->addElement("", $id_obat_hidden);
    $kode_obat_text = new Text("paket_obat_kode_obat", "paket_obat_kode_obat", "");
    $kode_obat_text->setAtribute("disabled='disabled'");
    $detail_modal->addElement("Kode Obat", $kode_obat_text);
    $nama_obat_text = new Text("paket_obat_nama_obat", "paket_obat_nama_obat", "");
    $nama_obat_text->setAtribute("disabled='disabled'");
    $nama_obat_text->setClass("smis-one-option-input");
    $obat_button = new Button("", "", "Pilih");
    $obat_button->setClass("btn-info");
    $obat_button->setAction("obat.chooser('obat', 'obat_button', 'obat', obat)");
    $obat_button->setIcon("icon-white icon-list-alt");
    $obat_button->setIsButton(Button::$ICONIC);
    $obat_button->setAtribute("id='obat_browse'");
    $obat_input_group = new InputGroup("");
    $obat_input_group->addComponent($nama_obat_text);
    $obat_input_group->addComponent($obat_button);
    $detail_modal->addElement("Nama Obat", $obat_input_group);
    $nama_jenis_obat_text = new Text("paket_obat_nama_jenis_obat", "paket_obat_nama_jenis_obat", "");
    $nama_jenis_obat_text->setAtribute("disabled='disabled'");
    $detail_modal->addElement("Jenis Obat", $nama_jenis_obat_text);
    $jumlah_text = new Text("paket_obat_jumlah", "paket_obat_jumlah", "0");
    $detail_modal->addElement("Jumlah", $jumlah_text);
    $satuan_text = new Text("paket_obat_satuan", "paket_obat_satuan", "");
    $satuan_text->setAtribute("disabled='disabled'");
    $detail_modal->addElement("Satuan", $satuan_text);
    $save_button = new Button("", "", "Simpan");
    $save_detail_button = new Button("", "", "Simpan");
    $save_detail_button->setClass("btn-success");
    $save_detail_button->setIcon("fa fa-floppy-o");
    $save_detail_button->setIsButton(Button::$ICONIC);
    $save_detail_button->setAction("detail_paket_obat.save()");
    $detail_modal->addFooter($save_detail_button);

    echo $detail_modal->getHtml();
    echo $header_modal->getHtml();
    echo $table->getHtml();
    echo addJS("framework/smis/js/table_action.js");
?>
<script type="text/javascript">
    var data_paket_obat;
    var detail_paket_obat;
    var obat;
    var i_row;
    $(document).ready(function() {
        obat = new TableAction(
            "obat",
            "depo_farmasi_irja",
            "data_paket_obat",
            new Array()
        );
        obat.selected = function(json) {
            $("#paket_obat_id_obat").val(json.id);
            $("#paket_obat_kode_obat").val(json.kode_obat);
            $("#paket_obat_nama_obat").val(json.nama_obat);
            $("#paket_obat_nama_jenis_obat").val(json.nama_jenis_obat);
            $("#paket_obat_jumlah").focus();
            $("#paket_obat_satuan").val(json.satuan);
        };
        obat.setSuperCommand("obat");

        detail_paket_obat = new TableAction(
            "detail_paket_obat",
            "depo_farmasi_irja",
            "data_paket_obat",
            new Array()
        );
        detail_paket_obat.clear = function() {
            $(".error_field").removeClass("error_field");
            $("#paket_obat_row_num").val("");
            $("#paket_obat_kode_obat").val("");
            $("#paket_obat_nama_obat").val("");
            $("#paket_obat_nama_jenis_obat").val("");
            $("#paket_obat_jumlah").val("0");
            $("#paket_obat_satuan").val("");
        };
        detail_paket_obat.show_add_form = function() {
            this.clear();
            i_row = 0;
            $("#detail_paket_obat_modal").smodal("show");
        };
        detail_paket_obat.refreshNumber = function() {
            var nor = $("#detail_paket_obat_list tr").length;
            for (var i = 0; i < nor; i++) {
                $("#detail_paket_obat_list tr:eq(" + i + ") td#nomor").text(i + 1);
            }
        };
        detail_paket_obat.validate = function() {
            var valid = true;
            var invalid_msg = "";
            var id_obat = $("#paket_obat_id_obat").val();
            var jumlah = $("#paket_obat_jumlah").val();
            $(".error_field").removeClass("error_field");
            if (id_obat == "") {
                valid = false;
                invalid_msg += "</br><strong>Obat</strong> tidak boleh kosong";
                $("#paket_obat_nama_obat").addClass("error_field");
            }
            if (jumlah == "") {
                valid = false;
                invalid_msg += "</br><strong>Jumlah</strong> tidak boleh kosong";
                $("#paket_obat_jumlah").addClass("error_field");
                $("#paket_obat_jumlah").focus();
            } else if (!is_numeric(jumlah)) {
                valid = false;
                invalid_msg += "</br><strong>Jumlah</strong> seharusnya numerik (0-9)";
                $("#paket_obat_jumlah").addClass("error_field");
                $("#paket_obat_jumlah").focus();
            }
            if (!valid) {
                $("#modal_alert_detail_paket_obat_modal").html(
                    "<div class='alert alert-block alert-danger'>" +
                        "<h4>Peringatan</h4>" +
                        invalid_msg +
                    "</div>"
                );
            }
            return valid;
        };
        detail_paket_obat.save = function() {
            if (!this.validate())
                return;
            var row_num = $("#paket_obat_row_num").val();
            var id_obat = $("#paket_obat_id_obat").val();
            var kode_obat = $("#paket_obat_kode_obat").val();
            var nama_obat = $("#paket_obat_nama_obat").val();
            var nama_jenis_obat = $("#paket_obat_nama_jenis_obat").val();
            var jumlah = $("#paket_obat_jumlah").val();
            var satuan = $("#paket_obat_satuan").val();
            var html_button =   "<div class='btn-group noprint'>" +
                                    "<a href='#' onclick='detail_paket_obat.edit(" + i_row + ")' data-content='Edit' id='data_paket_obat_edit' class='input btn btn-warning'>" +
                                        "<i class='fa fa-pencil'></i>" +
                                    "</a>" +
                                    "<a href='#' onclick='detail_paket_obat.del(" + i_row + ")' data-content='Delete' id='data_paket_obat_del' class='input btn btn-danger'>" +
                                        "<i class='fa fa-trash'></i>" +
                                    "</a>" +
                                "</div>";
            if (row_num == "") {
                $("#temp_row").remove();
                $("#detail_paket_obat_list").append(
                    "<tr>" +
                        "<td id='id' style='display: none;'></td>" +
                        "<td id='nomor'></td>" +
                        "<td id='id_obat'>" + id_obat + "</td>" +
                        "<td id='kode_obat'>" + kode_obat + "</td>" +
                        "<td id='nama_obat'>" + nama_obat + "</td>" +
                        "<td id='nama_jenis_obat'>" + nama_jenis_obat + "</td>" +
                        "<td id='jumlah'>" + jumlah + "</td>" +
                        "<td id='satuan'>" + satuan + "</td>" +
                        "<td class='noprint'>" + html_button + "</td>" +
                    "</tr>"
                );
                i_row++;
                this.refreshNumber();
            } else {
                $("#detail_paket_obat_list tr:eq(" + row_num + ") td#id_obat").text(id_obat);
                $("#detail_paket_obat_list tr:eq(" + row_num + ") td#kode_obat").text(kode_obat);
                $("#detail_paket_obat_list tr:eq(" + row_num + ") td#nama_obat").text(nama_obat);
                $("#detail_paket_obat_list tr:eq(" + row_num + ") td#nama_jenis_obat").text(nama_jenis_obat);
                $("#detail_paket_obat_list tr:eq(" + row_num + ") td#jumlah").text(jumlah);
                $("#detail_paket_obat_list tr:eq(" + row_num + ") td#satuan").text(satuan);
            }
            $("#detail_paket_obat_modal").smodal("hide");
        };
        detail_paket_obat.edit = function(row_num) {
            this.clear();
            var id_obat = $("#detail_paket_obat_list tr:eq(" + row_num + ") td#id_obat").text();
            var kode_obat = $("#detail_paket_obat_list tr:eq(" + row_num + ") td#kode_obat").text();
            var nama_obat = $("#detail_paket_obat_list tr:eq(" + row_num + ") td#nama_obat").text();
            var nama_jenis_obat = $("#detail_paket_obat_list tr:eq(" + row_num + ") td#nama_jenis_obat").text();
            var jumlah = $("#detail_paket_obat_list tr:eq(" + row_num + ") td#jumlah").text();
            var satuan = $("#detail_paket_obat_list tr:eq(" + row_num + ") td#satuan").text();

            $("#paket_obat_row_num").val(row_num);
            $("#paket_obat_id_obat").val(id_obat);
            $("#paket_obat_kode_obat").val(kode_obat);
            $("#paket_obat_nama_obat").val(nama_obat);
            $("#paket_obat_nama_jenis_obat").val(nama_jenis_obat);
            $("#paket_obat_jumlah").val(jumlah);
            $("#paket_obat_satuan").val(satuan);

            $("#detail_paket_obat_modal").smodal("show");
        };
        detail_paket_obat.del = function(row_num) {
            var id = $("#detail_paket_obat_list tr:eq(" + row_num + ") td#id").text();
            if (id.length == 0)
                $("#detail_paket_obat_list tr:eq(" + row_num + ")").remove();
            else {
                $("#detail_paket_obat_list tr:eq(" + row_num + ")").hide();
                $("#detail_paket_obat_list tr:eq(" + row_num + ")").addClass("deleted");
            }
        };

        data_paket_obat = new TableAction(
            "data_paket_obat",
            "depo_farmasi_irja",
            "data_paket_obat",
            new Array()
        );
        data_paket_obat.clear = function() {
            $(".error_field").removeClass("error_field");
            $("#paket_obat_nama_paket").val("");
            $("#paket_obat_is_racikan").val(0);
            $("#detail_paket_obat_list").html("");
        };
        data_paket_obat.show_add_form = function() {
            this.clear();
            $("#header_paket_obat_modal").smodal("show");
        };
        data_paket_obat.addSaveData = function(data) {
            data['nama_paket'] = $("#paket_obat_nama_paket").val();
            data['is_racikan'] = $("#paket_obat_is_racikan").val();
            var detail = {};
            var nor = $("#detail_paket_obat_list tr").length;
            for (var i = 0; i < nor; i++) {
                var id = $("#detail_paket_obat_list tr:eq(" + i + ") td#id").text();
                var id_obat = $("#detail_paket_obat_list tr:eq(" + i + ") td#id_obat").text();
                var kode_obat = $("#detail_paket_obat_list tr:eq(" + i + ") td#kode_obat").text();
                var nama_obat = $("#detail_paket_obat_list tr:eq(" + i + ") td#nama_obat").text();
                var nama_jenis_obat = $("#detail_paket_obat_list tr:eq(" + i + ") td#nama_jenis_obat").text();
                var jumlah = $("#detail_paket_obat_list tr:eq(" + i + ") td#jumlah").text();
                var satuan = $("#detail_paket_obat_list tr:eq(" + i + ") td#satuan").text();
                var was_deleted = 0;
                if ($("#detail_paket_obat_list tr:eq(" + i + ")").hasClass("deleted"))
                    was_deleted = 1;
                var d_data = {
                    'id'                : id,
                    'id_obat'           : id_obat,
                    'kode_obat'         : kode_obat,
                    'nama_obat'         : nama_obat,
                    'nama_jenis_obat'   : nama_jenis_obat,
                    'jumlah'            : jumlah,
                    'satuan'            : satuan,
                    'was_deleted'       : was_deleted
                }
                detail[i] = d_data;
            }
            data['detail'] = JSON.stringify(detail);
            return data;
        }
        data_paket_obat.validate = function() {
            var valid = true;
            var invalid_msg = "";
            var nama_paket = $("#paket_obat_nama_paket").val();
            var nor = $("#detail_paket_obat_list tr").length;
            var jumlah_detail = 0;
            for (var i = 0; i < nor; i++) {
                var was_deleted = $("#detail_paket_obat_list tr:eq(" + i + ")").hasClass("deleted");
                var id_obat = $("#detail_paket_obat_list tr:eq(" + i + ") td#id_obat").text();
                if (id_obat != null && !was_deleted)
                    jumlah_detail++;
            }
            $(".error_field").removeClass("error_field");
            if (nama_paket == "") {
                valid = false;
                invalid_msg += "</br><strong>Nama Paket</strong> tidak boleh kosong";
                $("#paket_obat_nama_paket").addClass("error_field");
            }
            if (jumlah_detail == 0) {
                valid = false;
                invalid_msg += "</br><strong>Detail Paket Obat</strong> tidak boleh kosong";
            }
            if (!valid) {
                $("#modal_alert_header_paket_obat_modal").html(
                    "<div class='alert alert-block alert-danger'>" +
                        "<h4>Peringatan</h4>" +
                        invalid_msg +
                    "</div>"
                );
            }
            return valid;
        };
        data_paket_obat.save = function() {
            if (!this.validate())
                return;
            TableAction.prototype.save.call(this);
            $("#header_paket_obat_modal").smodal("hide");
        };
        data_paket_obat.edit = function(id) {
            this.clear();
            var data = this.getRegulerData();
            data['id'] = id;
            data['command'] = "edit";
            $.post(
                "",
                data,
                function(response) {
                    var json = getContent(response);
                    if (json == null) return;
                    $("#paket_obat_nama_paket").val(json.nama_paket);
                    $("#paket_obat_is_racikan").val(json.is_racikan);
                    $("#detail_paket_obat_list").html(json.detail_html);
                    $("#header_paket_obat_modal").smodal("show");
                }
            );
        };
        data_paket_obat.view();
    });
</script>
<style type="text/css">
    label {
        width: 150 !important;
    }
</style>