<?php
	require_once("depo_farmasi_irja/library/InventoryLibrary.php");
	require_once("smis-base/smis-include-service-consumer.php");
	global $db;

	$form = new Form("", "", "Depo Farmasi IRJA : Laporan Rincian Penjualan UP");
	$tanggal_from_text = new Text("dpu_tanggal_from", "dpu_tanggal_from", date("Y-m-d 00:00"));
	$tanggal_from_text->setClass("mydatetime");
	$tanggal_from_text->setAtribute("data-date-format='yyyy-mm-dd hh:ii'");
	$form->addElement("Waktu Awal", $tanggal_from_text);
	$tanggal_to_text = new Text("dpu_tanggal_to", "dpu_tanggal_to", date("Y-m-d H:i"));
	$tanggal_to_text->setClass("mydatetime");
	$tanggal_to_text->setAtribute("data-date-format='yyyy-mm-dd hh:ii'");
	$form->addElement("Waktu Akhir", $tanggal_to_text);
	$show_button = new Button("", "", "Tampilkan");
	$show_button->setClass("btn-primary");
	$show_button->setIcon("icon-white icon-repeat");
	$show_button->setIsButton(Button::$ICONIC);
	$show_button->setAction("dpu.view()");
	$download_button = new Button("", "", "Eksport XLS");
	$download_button->setClass("btn-inverse");
	$download_button->setIcon("fa fa-download");
	$download_button->setIsButton(Button::$ICONIC);
	$download_button->setAtribute("id='dpu_export_button'");
	$btn_group = new ButtonGroup("noprint");
	$btn_group->addButton($show_button);
	$btn_group->addButton($download_button);
	$form->addElement("", $btn_group);

	$table = new Table(
		array("No.", "Tanggal", "ID Transaksi", "Lokasi", "Instansi", "No. Resep", "Nama Obat", "Harga", "Jumlah", "Total"),
		"",
		null,
		true
	);
	$table->setName("dpu");
	$table->setAction(false);
	$table->setFooterVisible(false);

	if (isset($_POST['command'])) {
		$dbtable = new DBTable($db, InventoryLibrary::$_TBL_PENJUALAN_RESEP);
		if ($_POST['command'] == "get_jumlah") {
			$tanggal_from = $_POST['tanggal_from'];
			$tanggal_to = $_POST['tanggal_to'];
			$row = $dbtable->get_row("
				SELECT COUNT(*) AS 'jumlah'
				FROM " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . "
				WHERE tanggal >= '" . $tanggal_from . "' AND tanggal <= '" . $tanggal_to . "' AND dibatalkan = '0' AND prop NOT LIKE 'del' AND dibatalkan = '0' AND tipe LIKE 'instansi_lain'
			");
			$data = array(
				"jumlah"	=> $row->jumlah
			);
			echo json_encode($data);
		} else if ($_POST['command'] == "get_info") {
			$tanggal_from = $_POST['tanggal_from'];
			$tanggal_to = $_POST['tanggal_to'];
			$num = $_POST['num'];
			$is_first_row = true;
			$row = $dbtable->get_row("
				SELECT *
				FROM " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . "
				WHERE tanggal >= '" . $tanggal_from . "' AND tanggal <= '" . $tanggal_to . "' AND dibatalkan = '0' AND prop NOT LIKE 'del' AND dibatalkan = '0' AND tipe LIKE 'instansi_lain'
				LIMIT " . $num . ", 1
			");
			$html = "";
			$total = 0;
			if ($row != null) {
				// obat jadi :
				$obat_jadi_rows = $dbtable->get_result("
					SELECT *
					FROM " . InventoryLibrary::$_TBL_PENJUALAN_OBAT_JADI . "
					WHERE id_penjualan_resep = '" . $row->id . "' AND prop NOT LIKE 'del'
				");
				if ($obat_jadi_rows != null) {
					foreach ($obat_jadi_rows as $obat_jadi_row) {
						$html .= "<tr>";
						if ($is_first_row) {
							$html .= "<td id='dpu_nomor'><small>" . ($num + 1) . "</small></td>";
							$is_first_row = false;
						} else {
							$html .= "<td></td>";
						}
						$subtotal = $obat_jadi_row->harga * $obat_jadi_row->jumlah;
						$total += $subtotal;
						$html .= "
								<td id='dpu_label' style='display: none;'>data</td>
								<td id='dpu_tanggal'><small>" . ArrayAdapter::format("date d-m-Y, H:i:s", $row->tanggal) . "</small></td>
								<td id='dpu_id'><small>" . ArrayAdapter::format("only-digit8", $row->id) . "</small></td>
								<td id='dpu_lokasi'><small>FARMASI</small></td>
								<td id='dpu_nama_pasien'><small>" . strtoupper($row->nama_pasien) . "</small></td>
								<td id='dpu_no_resep'><small>" . $row->nomor_resep . "</small></td>
								<td id='dpu_nama_obat'><small>" . $obat_jadi_row->nama_obat . "</small></td>
								<td id='dpu_harga' style='display: none;'>" . $obat_jadi_row->harga . "</td>
								<td id='dpu_f_harga'><small><div align='right'>" . ArrayAdapter::format("only-money", $obat_jadi_row->harga) . "</div></small></td>
								<td id='dpu_jumlah' style='display: none;'>" . $obat_jadi_row->jumlah . "</td>
								<td id='dpu_f_jumlah'><small>" . ArrayAdapter::format("number", $obat_jadi_row->jumlah) . "</small></td>
								<td id='dpu_subtotal' style='display: none;'>" . $subtotal . "</td>
								<td id='dpu_f_subtotal'><small><div align='right'>" . ArrayAdapter::format("only-money", $subtotal) . "</div></small></td>
							</tr>
						";
					}
				}
				// bahan racikan :
				$bahan_racikan_rows = $dbtable->get_result("
					SELECT b.*
					FROM " . InventoryLibrary::$_TBL_PENJUALAN_OBAT_RACIKAN . " a LEFT JOIN " . InventoryLibrary::$_TBL_BAHAN_PAKAI_OBAT_RACIKAN . " b ON a.id = b.id_penjualan_obat_racikan
					WHERE a.id_penjualan_resep = '" . $row->id . "' AND a.prop NOT LIKE 'del' AND b.prop NOT LIKE 'del'
				");
				if ($bahan_racikan_rows != null) {
					foreach ($bahan_racikan_rows as $bahan_racikan_row) {
						$html .= "<tr>";
						if ($is_first_row) {
							$html .= "<td id='dpu_nomor'><small>" . ($num + 1) . "</small></td>";
							$is_first_row = false;
						} else {
							$html .= "<td></td>";
						}
						$subtotal = $bahan_racikan_row->harga * $bahan_racikan_row->jumlah;
						$total += $subtotal;
						$html .= "
								<td id='dpu_label' style='display: none;'>data</td>
								<td id='dpu_tanggal'><small>" . ArrayAdapter::format("date d-m-Y, H:i:s", $row->tanggal) . "</small></td>
								<td id='dpu_id'><small>" . ArrayAdapter::format("only-digit8", $row->id) . "</small></td>
								<td id='dpu_lokasi'><small>FARMASI</small></td>
								<td id='dpu_nama_pasien'><small>" . strtoupper($row->nama_pasien) . "</small></td>
								<td id='dpu_nama_obat'><small>" . $bahan_racikan_row->nama_obat . "</small></td>
								<td id='dpu_harga' style='display: none;'>" . $bahan_racikan_row->harga . "</td>
								<td id='dpu_f_harga'><small><div align='right'>" . ArrayAdapter::format("only-money", $bahan_racikan_row->harga) . "</div></small></td>
								<td id='dpu_jumlah' style='display: none;'>" . $bahan_racikan_row->jumlah . "</td>
								<td id='dpu_f_jumlah'><small>" . ArrayAdapter::format("number", $bahan_racikan_row->jumlah) . "</small></td>
								<td id='dpu_subtotal' style='display: none;'>" . $subtotal . "</td>
								<td id='dpu_f_subtotal'><small><div align='right'>" . ArrayAdapter::format("only-money", $subtotal) . "</div></small></td>
							</tr>
						";
					}
				}
				$diskon = $row->diskon * $total / 100;
				if ($row->t_diskon == "nominal")
					$diskon = $row->diskon;
				$total -= $diskon;
				$html .= "
					<tr>
						<td id='dpu_label' style='display: none;'>diskon</td>
						<td colspan='9'><div align='right'><small><strong>DISKON</strong></small></div></td>
						<td id='dpu_diskon' style='display: none;'>" . $diskon . "</td>
						<td id='dpu_f_diskon'><strong><small><div align='right'>" . ArrayAdapter::format("only-money", $diskon) . "</div></small></strong></td>
					</tr>
					<tr>
						<td id='dpu_label' style='display: none;'>total</td>
						<td colspan='9'><div align='right'><small><strong>TOTAL</strong></small></div></td>
						<td id='dpu_total' style='display: none;'>" . $total . "</td>
						<td id='dpu_f_total'><strong><small><div align='right'>" . ArrayAdapter::format("only-money", $total) . "</div></small></strong></td>
					</tr>
				";
			}
			$data = array(
				"nomor_transaksi"	=> ArrayAdapter::format("only-digit8", $row->id),
				"nomor_resep"		=> $row->nomor_resep,
				"nama_pasien"		=> $row->nama_pasien,
				"html"				=> $html
			);
			echo json_encode($data);
		} else if ($_POST['command'] == "export_xls") {
			ini_set("memory_limit", "1024M");
			set_time_limit(0);
			$tanggal_from = $_POST['tanggal_from'];
			$tanggal_to = $_POST['tanggal_to'];
			require_once("smis-libs-out/php-excel/PHPExcel.php");
			$objPHPExcel = PHPExcel_IOFactory::load("depo_farmasi_irja/templates/template_rincian_penjualan_up.xlsx");
			$objPHPExcel->setActiveSheetIndexByName("RINCIAN PENJUALAN UP");
			$objWorksheet = $objPHPExcel->getActiveSheet();
			$nama_instansi = getSettings($db, "smis_autonomous_title", "SIMRS");
			if ($nama_instansi == "")
				$objWorksheet->setCellValue("B2", "FARMASI");
			else
				$objWorksheet->setCellValue("B2", "FARMASI - " . $nama_instansi);
			$objWorksheet->setCellValue("B3", "PERIODE : " . ArrayAdapter::format("date d-m-Y, H:i", $tanggal_from) . " s/d " . ArrayAdapter::format("date d-m-Y, H:i", $tanggal_to));
			$data = json_decode($_POST['d_data']);
			if ($_POST['num_rows'] - 2 > 0)
				$objWorksheet->insertNewRowBefore(8, $_POST['num_rows'] - 2);
			$start_row_num = 7;
			$end_row_num = 7;
			$row_num = $start_row_num;
			foreach ($data as $d) {
				if ($d->label == "data") {
					$col_num = 1;
					$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->nomor);
					$col_num++;
					$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->tanggal);
					$col_num++;
					$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->id);
					$col_num++;
					$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->lokasi);
					$col_num++;
					$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->nama_pasien);
					$col_num++;
					$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->no_resep);
					$col_num++;
					$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->nama_obat);
					$col_num++;
					$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->harga);
					$col_num++;
					$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->jumlah);
					$col_num++;
					$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->subtotal);
					$objWorksheet->getStyle("I" . $row_num)->getNumberFormat()->setFormatCode("#,##0.00");
					$objWorksheet->getStyle("J" . $row_num)->getNumberFormat()->setFormatCode("#,##0");
					$objWorksheet->getStyle("K" . $row_num)->getNumberFormat()->setFormatCode("#,##0.00");
				} else if ($d->label == "diskon") {
					$style = array(
						"font"		=> array(
							"bold"			=> true
						)
					);
					$objWorksheet->getStyle("J" . $row_num . ":K" . $row_num)->applyFromArray($style);
					$col_num = 9;
					$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, "DISKON");
					$col_num += 1;
					$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->diskon);
					$objWorksheet->getStyle("K" . $row_num)->getNumberFormat()->setFormatCode("#,##0.00");
				} else if ($d->label == "total") {
					$style = array(
						"font"		=> array(
							"bold"			=> true
						)
					);
					$objWorksheet->getStyle("J" . $row_num . ":K" . $row_num)->applyFromArray($style);
					$col_num = 9;
					$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, "TOTAL");
					$col_num += 1;
					$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->total);
					$objWorksheet->getStyle("K" . $row_num)->getNumberFormat()->setFormatCode("#,##0.00");
				}
				$row_num++;
				$end_row_num++;
				$no++;
			}
			header("Content-type: application/vnd.ms-excel");	
			header("Content-Disposition: attachment; filename=B3_RINCIAN_PENJUALAN_UP_" . ArrayAdapter::format("date Ymd_Hi", $tanggal_from) . "_" . ArrayAdapter::format("date Ymd_Hi", $tanggal_to) . ".xlsx");
			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");
			$objWriter->save("php://output");
		}
		return;
	}
	$loading_bar = new LoadingBar("dpu_loading_bar", "");
	$button = new Button("", "", "Batal");
	$button->addClass("btn-primary");
	$button->setIsButton(Button::$ICONIC_TEXT);
	$button->setIcon("fa fa-close");
	$button->setAction("dpb.cancel()");
	$loading_modal = new Modal("dpu_loading_modal", "", "Proses..");
	$loading_modal->addHtml($loading_bar->getHtml(), "after");
	$loading_modal->addFooter($button);

	echo $loading_modal->getHtml();
	echo $form->getHtml();
	echo "<div id='table_content'>";
	echo $table->getHtml();
	echo "</div>";
	echo "<div id='dpu_info'></div>";
	echo addJS("framework/smis/js/table_action.js");
	echo addJS("framework/bootstrap/js/bootstrap-datetimepicker.js");
	echo addCSS("framework/bootstrap/css/bootstrap-datetimepicker.css");
	echo addJS("depo_farmasi_irja/js/laporan_rincian_penjualan_up_action.js", false);
	echo addJS("depo_farmasi_irja/js/laporan_rincian_penjualan_up.js", false);
?>