<?php
	global $db;
	require_once("smis-base/smis-include-service-consumer.php");
	require_once("depo_farmasi_irja/library/InventoryLibrary.php");
	require_once("depo_farmasi_irja/table/PasienTable.php");
	require_once("depo_farmasi_irja/responder/PasienDBResponder.php");

	$form = new Form("", "", "Depo Depo Farmasi IRJA : Resume Penggunaan Obat Pasien");
	$nama_pasien_text = new Text("rpop_nama_pasien", "rpop_nama_pasien", isset($_POST['nama_pasien']) ? $_POST['nama_pasien'] : "");
	$nama_pasien_text->setAtribute("disabled='disabled'");
	$nama_pasien_text->setClass("smis-one-option-input");
	$browse_button = new Button("", "", "Pilih");
	$browse_button->setClass("btn-info");
	$browse_button->setIcon("fa fa-list");
	$browse_button->setIsButton(Button::$ICONIC);
	$browse_button->setAction("pasien.chooser('pasien', 'pasien_button', 'pasien', pasien)");
	$input_group = new InputGroup("");
	$input_group->addComponent($nama_pasien_text);
	$input_group->addComponent($browse_button);
	$form->addElement("Nama Pasien", $input_group);
	$nrm_pasien_text = new Text("rpop_nrm_pasien", "rpop_nrm_pasien", isset($_POST['nrm_pasien']) ? $_POST['nrm_pasien'] : "");
	$nrm_pasien_text->setAtribute("disabled='disabled'");
	$form->addElement("No. RM", $nrm_pasien_text);
	$alamat_pasien_text = new Text("rpop_alamat_pasien", "rpop_alamat_pasien", isset($_POST['alamat_pasien']) ? $_POST['alamat_pasien'] : "");
	$alamat_pasien_text->setAtribute("disabled='disabled'");
	$form->addElement("Alamat", $alamat_pasien_text);
	$show_button = new Button("", "", "Tampilkan");
	$show_button->setClass("btn-primary");
	$show_button->setIcon("icon-white icon-repeat");
	$show_button->setIsButton(Button::$ICONIC);
	$show_button->setAction("rpop.view()");
	$btn_group = new ButtonGroup("noprint");
	$btn_group->addButton($show_button);
	$form->addElement("", $btn_group);

	//patient service consumer:
	$pasien_table = new PasienTable(
		array("No.", "NRM", "Nama", "Alamat"),
		"",
		null,
		true
	);
	$pasien_table->setName("pasien");
	$pasien_table->setModel(Table::$SELECT);
	$nrm_s_text = new Text("search_nrm", "search_nrm", "");
	$nrm_s_text->addAtribute("autofocus");
	$nrm_s_text->setClass("search search-header-tiny search-text");
	$nama_s_text = new Text("search_nama", "search_nama", "");
	$nama_s_text->setClass("search search-header-med search-text ");
	$header = "<tr class = 'header_pasien'>" .
					"<td></td>" .
					"<td>" . $nrm_s_text->getHtml() . "</td>" .
					"<td>" . $nama_s_text->getHtml() . "</td>" .
					"<td></td>" .
					"<td></td>" .
			  "</tr>";
	$pasien_table->addHeader("after", $header);
	$pasien_adapter = new SimpleAdapter(true, "No.");
	$pasien_adapter->add("NRM", "id", "digit6");
	$pasien_adapter->add("Nama", "nama");
	$pasien_adapter->add("Alamat", "alamat");
	$pasien_service_responder = new ServiceResponder(
		$db,
		$pasien_table,
		$pasien_adapter,
		"get_patient"
	);

	$super_command = new SuperCommand();
	$super_command->addResponder("pasien", $pasien_service_responder);
	$init = $super_command->initialize();
	if ($init != null) {
		echo $init;
		return;
	}

	if (isset($_POST['command'])) {
		if ($_POST['command'] == "get_resume") {
			$nrm_pasien = $_POST['nrm_pasien'];
			$html = "";
			$params = array(
				"nrm_pasien" => $nrm_pasien
			);
			$total = 0;
			$service_consumer = new ServiceConsumer(
				$db,
				"get_noreg_by_nrm",
				$params,
				"registration"
			);
			$service_consumer->setMode(ServiceConsumer::$CLEAN_BOTH);
			$content = $service_consumer->execute()->getContent();
			if ($content != null) {
				$nomor = 1;
				$button = new Button("", "", "Detail");
				$button->setClass("btn-inverse");
				$button->setIcon("fa fa-search");
				$button->setIsButton(Button::$ICONIC);
				$button_group = new ButtonGroup("noprint");
				$button_group->addButton($button);
				foreach ($content as $c) {
					$resep_row = $db->get_row("
						SELECT COUNT(*) AS 'jumlah'
						FROM " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . "
						WHERE noreg_pasien = '" . $c['id'] . "' AND prop NOT LIKE 'del' AND dibatalkan = '0'
					");
					if ($resep_row->jumlah > 0) {
						$index = $nomor - 1;
						$button->setAction("rpop.detail(" . $index . ")");
						$html .= "
							<tr>
								<td>" . $nomor++ . "</td>
								<td style='display: none;'>" . $c['id'] . "</td>
								<td>" . ArrayAdapter::format("only-digit6", $c['id']) . "</td>
								<td>" . ArrayAdapter::format("date d-m-Y, H:i:s", $c['tanggal']) . "</td>
								<td>" . ArrayAdapter::format("trivial_1_Rawat Inap_Rawat Jalan", $c['uri']) . "</td>
								<td>" . $c['umur'] . "</td>
								<td>" . ArrayAdapter::format("unslug", $c['jenislayanan']) . "</td>
								<td>" . ArrayAdapter::format("trivial_0_Ya_Tidak", $c['selesai']) . "</td>
								<td>" . $button_group->getHtml() . "</td>
							</tr>
						";
					}
				}
			}
			$data = array(
				"html"		=> $html
			);
			echo json_encode($data);
		}
		return;
	}

	$table = new Table(
		array("No.", "No. Reg.", "Tanggal Daftar", "Rawat Jalan/Inap", "Usia", "Unit Layanan", "Pasien Aktif", ""),
		"",
		null,
		true
	);
	$table->setName("resume");
	$table->setAction(false);
	$table->setFooterVisible(false);

	echo $form->getHtml();
	echo $table->getHtml();
	echo addCSS("depo_farmasi_irja/css/resume_penggunaan_obat_pasien.css", false);
	echo addJS("framework/smis/js/table_action.js");
	echo addJS("depo_farmasi_irja/js/resume_penggunaan_obat_pasien_action.js", false);
	echo addJS("depo_farmasi_irja/js/resume_penggunaan_obat_pasien_pasien_action.js", false);
	echo addJS("depo_farmasi_irja/js/resume_penggunaan_obat_pasien.js", false);
?>