<?php
	class ReturObatTable extends Table {
		public function getBodyContent(){
			$content = "";
			if ($this->content!=NULL) {
				foreach ($this->content as $d) {
					$content .= "<tr>";
					foreach ($this->header as $h) {
						$content .= "<td>" . $d[$h] . "</td>";
					}
					if ($this->is_action) {
						$content .= "<td>" . $this->getFilteredContentButton($d['id'], $d['status'], $d['restok'])->getHtml() . "</td>";
					}
					$content .= "</tr>";
				}
			}
			return $content;
		}
		public function getFilteredContentButton($id, $status, $restok) {
			$btn_group = new ButtonGroup("noprint");
			if ($status == "sudah") {
				$btn = new Button("", "", "Lihat");
				$btn->setAction($this->action . ".detail('" . $id . "')");
				if (!$restok) {
					$btn->setClass("btn-success");
				} else {
					$btn->setClass("btn-inverse");
				}
				$btn->setAtribute("data-content='Lihat' data-toggle='popover'");
				$btn->setIcon("icon-eye-open icon-white");
				$btn->setIsButton(Button::$ICONIC);
				$btn_group->addElement($btn);
				if (!$restok) {
					$btn = new Button("", "", "Restok");
					$btn->setAction($this->action . ".restock('" . $id . "')");
					$btn->setAtribute("data-content='Restok' data-toggle='popover'");
					$btn->setIcon("fa fa-refresh");
					$btn->setIsButton(Button::$ICONIC);
					$btn_group->addElement($btn);
				}
			} else {
				$btn = new Button("", "", "Ubah");
				$btn->setAction($this->action . ".edit('" . $id . "')");
				$btn->setClass("btn-warning");
				$btn->setAtribute("data-content='Ubah' data-toggle='popover'");
				$btn->setIcon("icon-edit icon-white");
				$btn->setIsButton(Button::$ICONIC);
				$btn_group->addElement($btn);
			}
			return $btn_group;
		}
	}
?>