<?php
class PenjualanResepLocker extends Locker{
	public function proceed($variable){
		return $variable->save();
	}
	
	public function alreadyUsed(){
		$result=array();
		$result['success']=0;
		$result['message']="Terjadi Tabrakan Data Harap satu-satu saat memasukan Data, Silakan Ulangi Lagi";
		return $result;
	}
	
	public function succeed(){
		return $this->proceed($this->variable);
	}	
}
?>