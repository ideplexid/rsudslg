<?php
	require_once("depo_farmasi_irja/library/InventoryLibrary.php");
	require_once("smis-base/smis-include-service-consumer.php");
	global $db;

	$form = new Form("", "", "Depo Farmasi IRJA : Laporan Rincian Retur Penjualan Bebas");
	$tanggal_from_text = new Text("drpb_tanggal_from", "drpb_tanggal_from", date("Y-m-d 00:00"));
	$tanggal_from_text->setClass("mydatetime");
	$tanggal_from_text->setAtribute("data-date-format='yyyy-mm-dd hh:ii'");
	$form->addElement("Waktu Awal", $tanggal_from_text);
	$tanggal_to_text = new Text("drpb_tanggal_to", "drpb_tanggal_to", date("Y-m-d H:i"));
	$tanggal_to_text->setClass("mydatetime");
	$tanggal_to_text->setAtribute("data-date-format='yyyy-mm-dd hh:ii'");
	$form->addElement("Waktu Akhir", $tanggal_to_text);
	$show_button = new Button("", "", "Tampilkan");
	$show_button->setClass("btn-primary");
	$show_button->setIcon("icon-white icon-repeat");
	$show_button->setIsButton(Button::$ICONIC);
	$show_button->setAction("drpb.view()");
	$download_button = new Button("", "", "Eksport XLS");
	$download_button->setClass("btn-inverse");
	$download_button->setIcon("fa fa-download");
	$download_button->setIsButton(Button::$ICONIC);
	$download_button->setAtribute("id='drpb_export_button'");
	$btn_group = new ButtonGroup("noprint");
	$btn_group->addButton($show_button);
	$btn_group->addButton($download_button);
	$form->addElement("", $btn_group);

	$table = new Table(
		array("No.", "Tanggal", "ID Transaksi", "Lokasi", "Nama", "No. Resep", "Nama Obat", "Harga", "Jumlah", "Total"),
		"",
		null,
		true
	);
	$table->setName("drpb");
	$table->setAction(false);
	$table->setFooterVisible(false);

	if (isset($_POST['command'])) {
		$dbtable = new DBTable($db, InventoryLibrary::$_TBL_PENJUALAN_RESEP);
		if ($_POST['command'] == "get_jumlah") {
			$tanggal_from = $_POST['tanggal_from'];
			$tanggal_to = $_POST['tanggal_to'];
			$row = $dbtable->get_row("
				SELECT COUNT(a.id) AS 'jumlah'
				FROM " . InventoryLibrary::$_TBL_RETUR_PENJUALAN_RESEP . " a INNER JOIN " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . " b ON a.id_penjualan_resep = b.id
				WHERE a.tanggal >= '" . $tanggal_from . "' AND a.tanggal <= '" . $tanggal_to . "' AND a.prop NOT LIKE 'del' AND (b.tipe LIKE 'bebas')
			");
			$data = array(
				"jumlah"	=> $row->jumlah
			);
			echo json_encode($data);
		} else if ($_POST['command'] == "get_info") {
			$tanggal_from = $_POST['tanggal_from'];
			$tanggal_to = $_POST['tanggal_to'];
			$num = $_POST['num'];
			$is_first_row = true;
			$row = $dbtable->get_row("
				SELECT a.*, b.nama_pasien, b.nomor_resep
				FROM " . InventoryLibrary::$_TBL_RETUR_PENJUALAN_RESEP . " a INNER JOIN " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . " b ON a.id_penjualan_resep = b.id
				WHERE a.tanggal >= '" . $tanggal_from . "' AND a.tanggal <= '" . $tanggal_to . "' AND a.prop NOT LIKE 'del' AND (b.tipe LIKE 'bebas')
				LIMIT " . $num . ", 1
			");
			$html = "";
			$total = 0;
			if ($row != null) {
				// detail retur :
				$detail_retur_rows = $dbtable->get_result("
					SELECT a.*, b.nama_obat
					FROM " . InventoryLibrary::$_TBL_DRETUR_PENJUALAN_RESEP . " a INNER JOIN " . InventoryLibrary::$_TBL_STOK_OBAT . " b ON a.id_stok_obat = b.id
					WHERE a.id_retur_penjualan_resep = '" . $row->id . "' AND a.prop NOT LIKE 'del'
				");
				if ($detail_retur_rows != null) {
					foreach ($detail_retur_rows as $detail_retur_row) {
						$html .= "<tr>";
						if ($is_first_row) {
							$html .= "<td id='drpb_nomor'><small>" . ($num + 1) . "</small></td>";
							$is_first_row = false;
						} else {
							$html .= "<td></td>";
						}
						$subtotal = $detail_retur_row->harga * $detail_retur_row->jumlah * $row->persentase_retur / 100;
						$total += $subtotal;
						$html .= "
								<td id='drpb_label' style='display: none;'>data</td>
								<td id='drpb_tanggal'><small>" . ArrayAdapter::format("date d-m-Y, H:i:s", $row->tanggal) . "</small></td>
								<td id='drpb_id'><small>" . ArrayAdapter::format("only-digit8", $row->id) . "</small></td>
								<td id='drpb_lokasi'><small>FARMASI</small></td>
								<td id='drpb_nama_pasien'><small>" . strtoupper($row->nama_pasien) . "</small></td>
								<td id='drpb_no_resep'><small>" . $row->nomor_resep . "</small></td>
								<td id='drpb_nama_obat'><small>" . $detail_retur_row->nama_obat . "</small></td>
								<td id='drpb_harga' style='display: none;'>" . $detail_retur_row->harga . "</td>
								<td id='drpb_f_harga'><small><div align='right'>" . ArrayAdapter::format("only-money", $detail_retur_row->harga) . "</div></small></td>
								<td id='drpb_jumlah' style='display: none;'>" . $detail_retur_row->jumlah . "</td>
								<td id='drpb_f_jumlah'><small>" . ArrayAdapter::format("number", $detail_retur_row->jumlah) . "</small></td>
								<td id='drpb_subtotal' style='display: none;'>" . $subtotal . "</td>
								<td id='drpb_f_subtotal'><small><div align='right'>" . ArrayAdapter::format("only-money", $subtotal) . "</div></small></td>
							</tr>
						";
					}
				}
				$html .= "
					<tr>
						<td id='drpb_label' colspan='9'><center><small><strong>TOTAL</strong></small></center></td>
						<td id='drpb_total' style='display: none;'>" . $total . "</td>
						<td id='drpb_f_total'><strong><small><div align='right'>" . ArrayAdapter::format("only-money", $total) . "</div></small></strong></td>
					</tr>
				";
			}
			$data = array(
				"nomor_transaksi"	=> ArrayAdapter::format("only-digit8", $row->id),
				"nomor_resep"		=> $row->nomor_resep,
				"nama_pasien"		=> strtoupper($row->nama_pasien),
				"nama_dokter"		=> strtoupper($row->nama_dokter),
				"html"				=> $html
			);
			echo json_encode($data);
		} else if ($_POST['command'] == "export_xls") {
			ini_set("memory_limit", "1024M");
			set_time_limit(0);
			$tanggal_from = $_POST['tanggal_from'];
			$tanggal_to = $_POST['tanggal_to'];
			$jenis_pasien = $_POST['jenis_pasien'];
			$uri = $_POST['uri'];
			require_once("smis-libs-out/php-excel/PHPExcel.php");
			$objPHPExcel = PHPExcel_IOFactory::load("depo_farmasi_irja/templates/template_rincian_retur_penjualan_bebas.xlsx");
			$objPHPExcel->setActiveSheetIndexByName("RINCIAN RETUR PENJUALAN BEBAS");
			$objWorksheet = $objPHPExcel->getActiveSheet();
			$nama_instansi = getSettings($db, "smis_autonomous_title", "SIMRS");
			if ($nama_instansi == "")
				$objWorksheet->setCellValue("B2", "FARMASI");
			else
				$objWorksheet->setCellValue("B2", "FARMASI - " . $nama_instansi);
			$objWorksheet->setCellValue("B3", "PERIODE : " . ArrayAdapter::format("date d-m-Y, H:i", $tanggal_from) . " s/d " . ArrayAdapter::format("date d-m-Y, H:i", $tanggal_to));
			$data = json_decode($_POST['d_data']);
			if ($_POST['num_rows'] - 2 > 0)
				$objWorksheet->insertNewRowBefore(8, $_POST['num_rows'] - 2);
			$start_row_num = 7;
			$end_row_num = 7;
			$row_num = $start_row_num;
			foreach ($data as $d) {
				if ($d->label == "data") {
					$col_num = 1;
					$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->nomor);
					$col_num++;
					$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->tanggal);
					$col_num++;
					$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->id);
					$col_num++;
					$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->lokasi);
					$col_num++;
					$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->nama_pasien);
					$col_num++;
					$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->no_resep);
					$col_num++;
					$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->nama_obat);
					$col_num++;
					$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->harga);
					$col_num++;
					$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->jumlah);
					$col_num++;
					$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->subtotal);
					$objWorksheet->getStyle("I" . $row_num)->getNumberFormat()->setFormatCode("#,##0.00");
					$objWorksheet->getStyle("J" . $row_num)->getNumberFormat()->setFormatCode("#,##0");
					$objWorksheet->getStyle("K" . $row_num)->getNumberFormat()->setFormatCode("#,##0.00");
				} else {
					$style = array(
						"font"		=> array(
							"bold"			=> true
						)
					);
					$objWorksheet->getStyle("J" . $row_num . ":S" . $row_num)->applyFromArray($style);
					$col_num = 9;
					$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, "TOTAL");
					$col_num += 1;
					$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->total);
					$objWorksheet->getStyle("K" . $row_num)->getNumberFormat()->setFormatCode("#,##0.00");
				}
				$row_num++;
				$end_row_num++;
				$no++;
			}
			header("Content-type: application/vnd.ms-excel");	
			header("Content-Disposition: attachment; filename=S2_RINCIAN_RETUR_PENJUALAN_BEBAS_" . ArrayAdapter::format("date Ymd_Hi", $tanggal_from) . "_" . ArrayAdapter::format("date Ymd_Hi", $tanggal_to) . ".xlsx");
			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");
			$objWriter->save("php://output");
		}
		return;
	}
	$loading_bar = new LoadingBar("drpb_loading_bar", "");
	$button = new Button("", "", "Batal");
	$button->addClass("btn-primary");
	$button->setIsButton(Button::$ICONIC_TEXT);
	$button->setIcon("fa fa-close");
	$button->setAction("drpb.cancel()");
	$loading_modal = new Modal("drpb_loading_modal", "", "Proses..");
	$loading_modal->addHtml($loading_bar->getHtml(), "after");
	$loading_modal->addFooter($button);

	echo $loading_modal->getHtml();
	echo $form->getHtml();
	echo "<div id='table_content'>";
	echo $table->getHtml();
	echo "</div>";
	echo "<div id='drpb_info'></div>";
	echo addJS("framework/smis/js/table_action.js");
	echo addJS("framework/bootstrap/js/bootstrap-datetimepicker.js");
	echo addCSS("framework/bootstrap/css/bootstrap-datetimepicker.css");
	echo addJS("depo_farmasi_irja/js/laporan_rincian_retur_penjualan_bebas_action.js", false);
	echo addJS("depo_farmasi_irja/js/laporan_rincian_retur_penjualan_bebas.js", false);
?>