<?php
	require_once("depo_farmasi_irja/library/InventoryLibrary.php");
	
	class StokDBResponder extends DBResponder {
		public function edit() {
			$id_obat = $_POST['id_obat'];
			$satuan = $_POST['satuan'];
			$konversi = $_POST['konversi'];
			$satuan_konversi = $_POST['satuan_konversi'];
			$data = $this->dbtable->get_row("
				SELECT a.id_obat, a.nama_obat, a.nama_jenis_obat, SUM(a.sisa) AS 'sisa', a.satuan, a.konversi, a.satuan_konversi
				FROM " . InventoryLibrary::$_TBL_STOK_OBAT . " a LEFT JOIN " . InventoryLibrary::$_TBL_OBAT_MASUK . " b ON a.id_obat_masuk = b.id
				WHERE a.prop NOT LIKE 'del' AND b.prop NOT LIKE 'del' AND b.status = 'sudah' AND a.id_obat = '" . $id_obat . "' AND a.satuan = '" . $satuan . "' AND a.konversi = '" . $konversi . "' AND a.satuan_konversi = '" . $satuan_konversi . "'
				GROUP BY a.id_obat, satuan, a.konversi, a.satuan_konversi
			");
			return $data;
		}
	}
?>