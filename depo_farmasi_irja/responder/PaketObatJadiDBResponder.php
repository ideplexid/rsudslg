<?php
    require_once("depo_farmasi_irja/library/InventoryLibrary.php");
    require_once("gudang_farmasi/kelas/GudangFarmasiInventory.php");

    class PaketObatJadiDBResponder extends DBResponder {
        public function edit() {
            $nama_paket = $_POST['id'];
            $ppn = getSettings($this->dbtable->get_db(), "depo_farmasi6-penjualan_resep-ppn_jual", 11);
            $details = $this->dbtable->get_db()->get_result("
                SELECT
                    *
                FROM
                    " . InventoryLibrary::$_TBL_PAKET_OBAT . "
                WHERE
                    prop = '' AND is_racikan = 0 AND nama_paket = '" . $nama_paket . "'
            ");
            if ($details != null) {
                foreach ($details as $detail) {
                    $detail->stok = InventoryLibrary::getCurrentStock($this->dbtable->get_db(), $detail->id_obat, "", 1, "");
                    $hpp = GudangFarmasiInventory::getHPPPenjualan($this->dbtable->get_db(), $detail->id_obat, $ppn);
                    $margin_jual = 0;
                    // margin per rentang harga :
                    if (getSettings($this->dbtable->get_db(), "depo_farmasi6-" . $_POST['action'] . "-mode_margin_penjualan", 0) == 4) {
                        $margin_data = $this->dbtable->get_row("
                            SELECT 
                                IF(" . $hpp . " >= batas_bawah AND " . $hpp . " < batas_atas, margin_jual, 0) AS markup 
                            FROM 
                                " . InventoryLibrary::$_TBL_MARGIN_JUAL_RENTANG_HARGA . "
                            HAVING 
                                markup > 0
                            LIMIT 
                                0, 1
                        ");
                        if ($margin_data != null) 
                            $margin_jual = $margin_data->markup;
                    }
                    $detail->margin_jual = $margin_jual;
                    $hja = $hpp * (1 + $margin_jual / 100);
                    $detail->harga = $hja == 0 ? "Rp. 0,00" : ArrayAdapter::format("only-money Rp. ", $hja);
                    $satuan = "";
                    $satuan_row = $this->dbtable->get_db()->get_row("
                        SELECT
                            DISTINCT a.satuan
                        FROM
                            " . InventoryLibrary::$_TBL_STOK_OBAT ." a
                                INNER JOIN " . InventoryLibrary::$_TBL_OBAT_MASUK . " b ON a.id_obat_masuk = b.id
                        WHERE
                            b.status = 'sudah'
                                AND a.id_obat = '" . $detail->id_obat . "'
                                AND a.prop = ''
                                AND b.prop = ''
                                AND a.konversi = '1'
                        LIMIT 
                            0, 1
                    ");
                    $editable = 1;
                    if ($satuan_row != null)
                        $satuan = $satuan_row->satuan;
                    else {
                        $editable = 0;
                        $satuan_row = $this->dbtable->get_db()->get_row("
                            SELECT
                                satuan_konversi
                            FROM
                                smis_pr_barang
                            WHERE
                                id = '" . $detail->id_obat . "'
                        ");
                        if ($satuan_row != null)
                            $satuan = $satuan_row->satuan_konversi;
                    }
                    $detail->satuan = $satuan;
                    $detail->editable = $editable;
                    $embalase = getSettings($this->dbtable->get_db(), "depo_farmasi6-penjualan_resep-embalase-obat_jadi", 0);
                    $tuslah = getSettings($this->dbtable->get_db(), "depo_farmasi6-penjualan_resep-tuslah-obat_jadi", 0);
                    $detail->embalase = $embalase == 0 ? "Rp. 0,00" : ArrayAdapter::format("only-money Rp. ", $embalase);
                    $detail->tuslah = $tuslah == 0 ? "Rp. 0,00" : ArrayAdapter::format("only-money Rp. ", $tuslah);
                }
                return $details;
            }
            return null;
        }
    }
?>