<?php
	require_once("smis-base/smis-include-service-consumer.php");
	require_once("depo_farmasi_irja/library/InventoryLibrary.php");
	require_once("depo_farmasi_irja/table/ObatCitoMasukTable.php");
	global $db;

	$table = new ObatCitoMasukTable(
		array("No.", "No. Kwitansi", "Tanggal", "Rekanan"),
		"Depo Depo Farmasi IRJA : Pembelian Cito/Stat Obat",
		null,
		true
	);
	$table->setName("penerimaan_obat_cito");

	if (isset($_POST['command'])) {
		$adapter = new SimpleAdapter(true, "No.");
		$adapter->add("No. Kwitansi", "no_kwitansi");
		$adapter->add("Tanggal", "tanggal", "date d-m-Y");
		$adapter->add("Rekanan", "nama_vendor");
		$dbtable = new DBTable($db, InventoryLibrary::$_TBL_PEMBELIAN_UP);
		$dbtable->setOrder(" id DESC ");
		$dbresponder = new DBResponder(
			$dbtable,
			$table,
			$adapter
		);
		$data = $dbresponder->command($_POST['command']);
		echo json_encode($data);
		return;
	}

	echo $table->getHtml();
	echo addJS("framework/smis/js/table_action.js");
	echo addJS("depo_farmasi_irja/js/penerimaan_obat_cito_action.js", false);
	echo addJS("depo_farmasi_irja/js/penerimaan_obat_cito.js", false);
?>