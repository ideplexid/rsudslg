<?php 
	require_once ("smis-libs-inventory/navigation.php");
	global $NAVIGATOR;

	$menu = new Menu("fa fa-medkit");
	$menu->addProperty('title', "Depo Farmasi IRJA");
	$menu->addProperty('name', "Depo Farmasi IRJA");
	$menu->addSubMenu("Resume Tagihan Obat Pasien", "depo_farmasi_irja", "resume_tagihan_obat_pasien", "Resume Tagihan Obat Pasien", "fa fa-file");
	$menu->addSubMenu("Resume Penggunaan Obat Pasien", "depo_farmasi_irja", "resume_penggunaan_obat_pasien", "Resume Penggunaan Obat Pasien", "fa fa-file");
	$menu->addSubMenu("Validasi Pasien", "depo_farmasi_irja", "validasi_penjualan_obat_pasien", "Validasi Pasien", "fa fa-check");
	$menu->addSeparator();
	$menu->addSubMenu("Penerimaan Obat Cito", "depo_farmasi_irja", "penerimaan_obat_cito", "Data Penerimaan Obat Cito", "fa fa-plus");
	$menu->addSeparator();
	$menu->addSubMenu("Retur Penjualan Resep", "depo_farmasi_irja", "retur_penjualan_resep", "Data Retur Penjualan Resep", "fa fa-plus");
	$menu->addSubMenu("Retur Penjualan Bebas", "depo_farmasi_irja", "retur_penjualan_bebas", "Data Retur Penjualan Bebas", "fa fa-plus");
	$menu->addSubMenu("Retur Obat Per Pasien", "depo_farmasi_irja", "retur_obat_per_pasien", "Data Retur Penjualan Obat Per Pasien", "fa fa-plus");
	$menu->addSeparator();
	$menu->addSubMenu("E-Resep", "depo_farmasi_irja", "e_resep", "Data E-Resep", "fa fa-inbox");
	$menu->addSeparator();
	if (getSettings($db, "depo_farmasi6-asuhan_farmasi-activate", 0) == 1) {
		$menu->addSubMenu("Asuhan Farmasi", "depo_farmasi_irja", "asuhan_farmasi", "Data Asuhan Farmasi", "fa fa-inbox");
		$menu->addSeparator();
	}
	$menu->addSubMenu("Penjualan Resep (KIUP)", "depo_farmasi_irja", "penjualan_resep", "Data Penjualan Obat - KIUP", "fa fa-minus");
	$menu->addSubMenu("Penjualan Resep (Non-KIUP)", "depo_farmasi_irja", "penjualan_resep_luar", "Data Penjualan Obat - Non-KIUP", "fa fa-minus");
	$menu->addSubMenu("Penjualan Bebas", "depo_farmasi_irja", "penjualan_bebas", "Data Penjualan Bebas", "fa fa-minus");
	$menu->addSubMenu("Penjualan UP", "depo_farmasi_irja", "penjualan_instansi_lain", "Data Penjualan UP", "fa fa-minus");
	$menu->addSeparator();
	$menu->addSubMenu("Perbaikan Harga", "depo_farmasi_irja", "perbaikan_harga", "Perbaikan Harga" ,"fa fa-check");
	$menu->addSubMenu("Stock Opname", "depo_farmasi_irja", "stock_opname", "Stock Opname" ,"fa fa-check");
	// $menu->addSeparator();
	// $menu->addSubMenu("Mutasi Obat Antar-Unit - Masuk", "depo_farmasi_irja", "mutasi_masuk_unit", "Data Mutasi Obat Antar-Unit Masuk", "fa fa-reply");
	// $menu->addSubMenu("Mutasi Obat Antar-Unit - Keluar", "depo_farmasi_irja", "mutasi_keluar_unit", "Data Mutasi Obat Antar-Unit Keluar", "fa fa-mail-forward");
	$menu->addSeparator();
	$inventory_navigator = new InventoryNavigator($NAVIGATOR, "", "Depo Farmasi", "depo_farmasi_irja");
	$menu = $inventory_navigator->extendMenu($menu);
	$menu->addSeparator();
	$menu->addSubMenu("Daftar Obat Lanjutan", "depo_farmasi_irja", "daftar_obat_lanjutan", "Daftar OKT, Narkotika, dan Prekursor Farmasi" ,"fa fa-file");
	$menu->addSubMenu("Laporan", "depo_farmasi_irja", "laporan", "Laporan" ,"fa fa-file");
	$menu->addSeparator();
	$menu->addSubMenu("Margin Jual Per Obat", "depo_farmasi_irja", "pengaturan_margin", "Margin Jual Per Obat" ,"fa fa-gear");
	$menu->addSubMenu("Margin Jual Per Jenis Pasien", "depo_farmasi_irja", "pengaturan_margin_jenis_pasien", "Margin Jual Per Jenis Pasien" ,"fa fa-gear");
	$menu->addSubMenu("Margin Jual Per Obat - Jenis Pasien", "depo_farmasi_irja", "pengaturan_margin_obat_jenis_pasien", "Margin Jual Per Obat Per Jenis Pasien" ,"fa fa-gear");
	$menu->addSubMenu("Margin Jual Per Rentang Harga", "depo_farmasi_irja", "pengaturan_margin_obat_rentang_harga", "Margin Jual Per Rentang Harga" ,"fa fa-gear");
	$menu->addSubMenu("Data Paket Obat", "depo_farmasi_irja", "data_paket_obat", "Data Paket Obat", "fa fa-archive");
	$menu->addSubMenu("Pengaturan Umum", "depo_farmasi_irja", "settings", "Pengaturan Umum" ,"fa fa-gear");
	$NAVIGATOR->addMenu($menu, "depo_farmasi_irja");
?>