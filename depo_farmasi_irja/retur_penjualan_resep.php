<?php 
	require_once("depo_farmasi_irja/library/InventoryLibrary.php");
	require_once("depo_farmasi_irja/table/ReturPenjualanResepTable.php");
	require_once("depo_farmasi_irja/adapter/ReturPenjualanResepAdapter.php");
	require_once("depo_farmasi_irja/responder/ReturPenjualanResepDBResponder.php");
	require_once("depo_farmasi_irja/responder/ReturPenjualanResep_ResepDBResponder.php");
	
	$retur_penjualan_resep_table = new ReturPenjualanResepTable(
		array("Nomor", "Tanggal/Jam", "No. Resep", "NRM", "No. Reg.", "Pasien", "Dokter", "Status"),
		"Depo Farmasi IRJA : Retur Penjualan Resep",
		null,
		true
	);
	$retur_penjualan_resep_table->setName("retur");
	
	if (isset($_POST['super_command']) && $_POST['super_command'] == "retur") {
		if (isset($_POST['command'])) {
			if ($_POST['command'] == "get_batal_berobat_info") {
				$rows = $db->get_result("
					SELECT noreg_pasien, nrm_pasien, nama_pasien, GROUP_CONCAT(nomor_resep) csv_nomor_resep
					FROM " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . "
					WHERE prop = '' AND dibatalkan = 0 AND batal_berobat = 1 AND tipe = 'resep'
					GROUP BY noreg_pasien
				");
				$html = "";
				$show = 0;
				if ($rows != null) {
					$show = 1;
					$html = "<h4>Pasien Batal Berobat</h4>" .
							"<ul>";
					foreach ($rows as $r) {
						$html .= "<li>";
						$html .= "Pasien <b>" . $r->nama_pasien . " (NRM. " . ArrayAdapter::format("only-digit6", $r->nrm_pasien) . " / No. Reg. " . ArrayAdapter::format("only-digit8", $r->noreg_pasien) . ")</b> telah batal berobat, silakan membatalkan resep nomor : ";
						$nomor_resep_arr = explode(",", $r->csv_nomor_resep);
						if (count($nomor_resep_arr) > 0)
							foreach ($nomor_resep_arr as $nomor_resep)
								$html .= "<span class='badge badge-important'>" . $nomor_resep . "</span> ";	
						$html .= "</li>";
					}
					$html .= "</ul>";
				}
				$data = array(
					"html" 	=> $html,
					"show"	=> $show
				);
				echo json_encode($data);
				return;
			}
			$retur_penjualan_resep_adapter = new ReturPenjualanResepAdapter();
			$columns = array("id", "tanggal", "id_penjualan_resep", "persentase_retur", "dibatalkan", "tercetak");
			$retur_penjualan_resep_dbtable = new DBTable(
				$db,
				InventoryLibrary::$_TBL_RETUR_PENJUALAN_RESEP,
				$columns
			);
			$filter = "";
			if (isset($_POST['kriteria'])) {
				$filter = " AND (" . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".nama_dokter LIKE '%" . $_POST['kriteria'] . "%' OR " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".nama_pasien LIKE '%" . $_POST['kriteria'] . "%' OR " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".nrm_pasien LIKE '%" . $_POST['kriteria'] . "%' OR " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".noreg_pasien LIKE '%" . $_POST['kriteria'] . "%' OR " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".nomor_resep LIKE '%" . $_POST['kriteria'] . "%')";
			}
			$query_value = "
				SELECT *
				FROM (
					SELECT 
						" . InventoryLibrary::$_TBL_RETUR_PENJUALAN_RESEP . ".id, " . InventoryLibrary::$_TBL_RETUR_PENJUALAN_RESEP . ".id_penjualan_resep, " . InventoryLibrary::$_TBL_RETUR_PENJUALAN_RESEP . ".tanggal, " . InventoryLibrary::$_TBL_RETUR_PENJUALAN_RESEP . ".persentase_retur, " . InventoryLibrary::$_TBL_RETUR_PENJUALAN_RESEP . ".dibatalkan, " . InventoryLibrary::$_TBL_RETUR_PENJUALAN_RESEP . ".tercetak,
						" . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".nama_dokter, " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".nama_pasien, " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".nomor_resep, " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".nrm_pasien, " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".noreg_pasien, " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".batal_berobat
					FROM " . InventoryLibrary::$_TBL_RETUR_PENJUALAN_RESEP . " LEFT JOIN " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . " ON " . InventoryLibrary::$_TBL_RETUR_PENJUALAN_RESEP . ".id_penjualan_resep = " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".id
					WHERE " . InventoryLibrary::$_TBL_RETUR_PENJUALAN_RESEP . ".prop NOT LIKE 'del' AND (" . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".tipe = 'resep' OR " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".tipe = 'resep_luar') " . $filter . "
					ORDER BY " . InventoryLibrary::$_TBL_RETUR_PENJUALAN_RESEP . ".id DESC
				) v_retur
			";
			$query_count = "
				SELECT COUNT(*)
				FROM (
					SELECT 
						" . InventoryLibrary::$_TBL_RETUR_PENJUALAN_RESEP . ".id, " . InventoryLibrary::$_TBL_RETUR_PENJUALAN_RESEP . ".id_penjualan_resep, " . InventoryLibrary::$_TBL_RETUR_PENJUALAN_RESEP . ".tanggal, " . InventoryLibrary::$_TBL_RETUR_PENJUALAN_RESEP . ".persentase_retur, " . InventoryLibrary::$_TBL_RETUR_PENJUALAN_RESEP . ".dibatalkan, " . InventoryLibrary::$_TBL_RETUR_PENJUALAN_RESEP . ".tercetak,
						" . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".nama_dokter, " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".nama_pasien, " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".nomor_resep, " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".nrm_pasien, " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".noreg_pasien, " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".batal_berobat
					FROM " . InventoryLibrary::$_TBL_RETUR_PENJUALAN_RESEP . " LEFT JOIN " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . " ON " . InventoryLibrary::$_TBL_RETUR_PENJUALAN_RESEP . ".id_penjualan_resep = " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".id
					WHERE " . InventoryLibrary::$_TBL_RETUR_PENJUALAN_RESEP . ".prop NOT LIKE 'del'  AND (" . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".tipe = 'resep' OR " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".tipe = 'resep_luar') " . $filter . "
					ORDER BY " . InventoryLibrary::$_TBL_RETUR_PENJUALAN_RESEP . ".id DESC
				) v_retur
			";
			$retur_penjualan_resep_dbtable->setPreferredQuery(true, $query_value, $query_count);
			$retur_penjualan_resep_dbresponder = new ReturPenjualanResepDBResponder(
				$retur_penjualan_resep_dbtable,
				$retur_penjualan_resep_table,
				$retur_penjualan_resep_adapter
			);
			$data = $retur_penjualan_resep_dbresponder->command($_POST['command']);

			/// Push Data Tagihan ke Kasir Ketika Penambahan Data Baru atau Pembatalan Data Lama :
			if (isset($data['content']['success']) && $data['content']['success'] == 1 && getSettings($db, "cashier-real-time-tagihan", "0") != 0) {
				$mode_data_tagihan = getSettings($db, "depo_farmasi6-service-get_tagihan", "get_simple_tagihan.php") == "get_simple_tagihan.php" ? "simple" : "detail";
				if ($_POST['command'] == "save" && $data['content']['type'] == "insert") {
					$id_retur_penjualan_resep = $data['content']['id'];
					$retur_resep_row = $db->get_row("
						SELECT 
							a.id, a.tanggal, b.nama_dokter, b.tipe, b.nrm_pasien, b.noreg_pasien, b.nama_pasien, b.nomor_resep
						FROM 
							" . InventoryLibrary::$_TBL_RETUR_PENJUALAN_RESEP . " a LEFT JOIN " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . " b ON a.id_penjualan_resep = b.id
						WHERE 
							a.id = '" . $id_retur_penjualan_resep . "'
					");
					if ($retur_resep_row != null) {
						if ($retur_resep_row->tipe == "resep") {
							$date_part = explode("-", explode(" ", $retur_resep_row->tanggal)[0]);
							$tanggal = $date_part[2] . " " . ArrayAdapter::format("month-id", $date_part[1]) . " " . $date_part[0];
							$timestamp = $retur_resep_row->tanggal;
							$nama_dokter = $retur_resep_row->nama_dokter;
							$prop = "";

							$data_tagihan = array(
								'grup_name'		=> "return_resep",
								'entity'		=> "depo_farmasi_irja",
								'nama_pasien'	=> $retur_resep_row->nama_pasien,
								'nrm_pasien'	=> $retur_resep_row->nrm_pasien,
								'noreg_pasien'	=> $retur_resep_row->noreg_pasien
							);
							$list = array();
							$retur_resep_rows = $db->get_result("
								SELECT 
									a.id, b.id AS 'id_dretur', a.tanggal, a.id_penjualan_resep, c.id_obat, c.nama_obat, b.jumlah, c.satuan, a.persentase_retur, b.harga, d.nomor_resep, d.id_dokter, d.nama_dokter, d.uri
								FROM 
									(
										(
											" . InventoryLibrary::$_TBL_RETUR_PENJUALAN_RESEP ." a LEFT JOIN " . InventoryLibrary::$_TBL_DRETUR_PENJUALAN_RESEP ." b ON a.id = b.id_retur_penjualan_resep
										) LEFT JOIN " . InventoryLibrary::$_TBL_STOK_OBAT ." c ON b.id_stok_obat = c.id
									) LEFT JOIN " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . " d ON a.id_penjualan_resep = d.id
								WHERE 
									a.id = '" . $id_retur_penjualan_resep . "' AND b.prop = ''
							");
							if ($mode_data_tagihan == "simple") {
								if ($retur_resep_rows != null) {
									$nilai_pendapatan = 0;
									$keterangan = "Return Resep No. " . $retur_resep_row->nomor_resep . " : ";
									foreach ($retur_resep_rows as $rrr) {
										$nilai_pendapatan -= $rrr->jumlah * $rrr->harga * $rrr->persentase_retur / 100;
										$keterangan .= $rrr->nama_obat . " " . $rrr->satuan . ", ";
									}
									$keterangan = rtrim($keterangan, ", ");
									$info = array(
										'id'				=> "retur_resep_" . $id_retur_penjualan_resep,
										'waktu'				=> $tanggal,
										'nama'				=> "Return Resep No. " . $retur_resep_row->nomor_resep,
										'jumlah'			=> 1,
										'biaya'				=> $nilai_pendapatan,
										'keterangan'		=> $keterangan,
										'id'				=> $retur_resep_row->id,
										'start'				=> $timestamp,
										'end'				=> $timestamp,
										'dokter'			=> $nama_dokter,
										'prop'				=> $prop,
										'grup_name'			=> "return_resep",
										'nama_dokter' 		=> $rrr->nama_dokter,
										'id_dokter'			=> $rrr->id_dokter,
										'jaspel_dokter'		=> 0,
										'urjigd'			=> $rrr->uri == 1 ? "URI" : "URJ",
										'tanggal_tagihan'	=> ArrayAdapter::format("date Y-m-d", $rrr->tanggal)
									);					
									$list[] = $info;
								}
							} else if ($mode_data_tagihan == "detail") {
								if ($retur_resep_rows != null) {
									foreach ($retur_resep_rows as $rrr) {
										$nilai_pendapatan = -1 * $rrr->jumlah * $rrr->harga * $rrr->persentase_retur / 100;
										$info = array(
											'id'				=> "detail_retur_resep_" . $rrr->id_dretur,
											'waktu'				=> $tanggal,
											'nama'				=> "Return Resep No. " . $rrr->nomor_resep . " ID. " . $rrr->id_penjualan_resep . " - " . $rrr->nama_obat,
											'jumlah'			=> $rrr->jumlah,
											'biaya'				=> $nilai_pendapatan,
											'keterangan'		=> "Retur Obat " . $rrr->nama_obat . " : " . $rrr->jumlah . " x " . ArrayAdapter::format("only-money", $nilai_pendapatan / $rrr->jumlah),
											'id'				=> "R" . $rrr->id . "." . $rrr->id_dretur . "-PENDAPATAN",
											'start'				=> $timestamp,
											'end'				=> $timestamp,
											'dokter'			=> $nama_dokter,
											'prop'				=> $prop,
											'grup_name'			=> "return_resep",
											'nama_dokter' 		=> $rrr->nama_dokter,
											'id_dokter'			=> $rrr->id_dokter,
											'jaspel_dokter'		=> 0,
											'urjigd'			=> $rrr->uri == 1 ? "URI" : "URJ",
											'tanggal_tagihan'	=> ArrayAdapter::format("date Y-m-d", $rrr->tanggal)
										);					
										$list[] = $info;
									}
								}
							}
							$data_tagihan['list'] = $list;
							$consumer_service = new ServiceConsumer(
								$db,
								"proceed_receivable",
								$data_tagihan,
								"kasir"
							);
							$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
							$consumer_service->execute();
						}
					} 
				} else if ($_POST['command'] == "save" && $_POST['dibatalkan'] == 1 && isset($_POST['id'])) {
					$id_retur_penjualan_resep = $_POST['id'];
					$retur_resep_row = $db->get_row("
						SELECT 
							a.id, a.tanggal, b.nama_dokter, b.tipe, b.nrm_pasien, b.noreg_pasien, b.nama_pasien, b.nomor_resep
						FROM 
							" . InventoryLibrary::$_TBL_RETUR_PENJUALAN_RESEP ." a LEFT JOIN " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . " b ON a.id_penjualan_resep = b.id
						WHERE 
							a.id = '" . $id_retur_penjualan_resep . "'
					");
					if ($retur_resep_row != null) {
						if ($retur_resep_row->tipe == "resep") {
							$date_part = explode("-", explode(" ", $retur_resep_row->tanggal)[0]);
							$tanggal = $date_part[2] . " " . ArrayAdapter::format("month-id", $date_part[1]) . " " . $date_part[0];
							$timestamp = $retur_resep_row->tanggal;
							$nama_dokter = $retur_resep_row->nama_dokter;
							$prop = "del";

							$data_tagihan = array(
								'grup_name'		=> "return_resep",
								'entity'		=> "depo_farmasi_irja",
								'nama_pasien'	=> $retur_resep_row->nama_pasien,
								'nrm_pasien'	=> $retur_resep_row->nrm_pasien,
								'noreg_pasien'	=> $retur_resep_row->noreg_pasien
							);
							$list = array();
							$retur_resep_rows = $db->get_result("
								SELECT 
									a.id, b.id AS 'id_dretur', a.tanggal, a.id_penjualan_resep, c.id_obat, c.nama_obat, b.jumlah, c.satuan, a.persentase_retur, b.harga, d.nomor_resep, d.id_dokter, d.nama_dokter, d.uri
								FROM 
									(
										(
											" . InventoryLibrary::$_TBL_RETUR_PENJUALAN_RESEP ." a LEFT JOIN " . InventoryLibrary::$_TBL_DRETUR_PENJUALAN_RESEP ." b ON a.id = b.id_retur_penjualan_resep
										) LEFT JOIN " . InventoryLibrary::$_TBL_STOK_OBAT ." c ON b.id_stok_obat = c.id
									) LEFT JOIN " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . " d ON a.id_penjualan_resep = d.id
								WHERE 
									a.id = '" . $id_retur_penjualan_resep . "' AND b.prop = ''
							");
							if ($mode_data_tagihan == "simple") {
								if ($retur_resep_rows != null) {
									$nilai_pendapatan = 0;
									$keterangan = "Return Resep No. " . $retur_resep_row->nomor_resep . " : ";
									foreach ($retur_resep_rows as $rrr) {
										$nilai_pendapatan -= $rrr->jumlah * $rrr->harga * $rrr->persentase_retur / 100;
										$keterangan .= $rrr->nama_obat . " " . $rrr->satuan . ", ";
									}
									$keterangan = rtrim($keterangan, ", ");
									$info = array(
										'id'				=> "retur_resep_" . $id_retur_penjualan_resep,
										'waktu'				=> $tanggal,
										'nama'				=> "Return Resep No. " . $retur_resep_row->nomor_resep,
										'jumlah'			=> 1,
										'biaya'				=> $nilai_pendapatan,
										'keterangan'		=> $keterangan,
										'id'				=> $retur_resep_row->id,
										'start'				=> $timestamp,
										'end'				=> $timestamp,
										'dokter'			=> $nama_dokter,
										'prop'				=> $prop,
										'grup_name'			=> "return_resep",
										'nama_dokter' 		=> $rrr->nama_dokter,
										'id_dokter'			=> $rrr->id_dokter,
										'jaspel_dokter'		=> 0,
										'urjigd'			=> $rrr->uri == 1 ? "URI" : "URJ",
										'tanggal_tagihan'	=> ArrayAdapter::format("date Y-m-d", $rrr->tanggal)
									);					
									$list[] = $info;
								}
							} else if ($mode_data_tagihan == "detail") {
								if ($retur_resep_rows != null) {
									foreach ($retur_resep_rows as $rrr) {
										$nilai_pendapatan = -1 * $rrr->jumlah * $rrr->harga * $retur_resep_row->persentase_retur / 100;
										$info = array(
											'id'				=> "detail_retur_resep_" . $rrr->id_dretur,
											'waktu'				=> $tanggal,
											'nama'				=> "Return Resep No. " . $rrr->nomor_resep . " ID. " . $rrr->id_penjualan_resep . " - " . $rrr->nama_obat,
											'jumlah'			=> $rrr->jumlah,
											'biaya'				=> $nilai_pendapatan,
											'keterangan'		=> "Retur Obat " . $rrr->nama_obat . " : " . $rrr->jumlah . " x " . ArrayAdapter::format("only-money", $nilai_pendapatan / $rrr->jumlah),
											'id'				=> "R" . $rrr->id . "." . $rrr->id_dretur . "-PENDAPATAN",
											'start'				=> $timestamp,
											'end'				=> $timestamp,
											'dokter'			=> $nama_dokter,
											'prop'				=> $prop,
											'grup_name'			=> "return_resep",
											'nama_dokter' 		=> $rrr->nama_dokter,
											'id_dokter'			=> $rrr->id_dokter,
											'jaspel_dokter'		=> 0,
											'urjigd'			=> $rrr->uri == 1 ? "URI" : "URJ",
											'tanggal_tagihan'	=> ArrayAdapter::format("date Y-m-d", $rrr->tanggal)
										);					
										$list[] = $info;
									}
								}
							}
							$data_tagihan['list'] = $list;
							$consumer_service = new ServiceConsumer(
								$db,
								"proceed_receivable",
								$data_tagihan,
								"kasir"
							);
							$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
							$consumer_service->execute();
						}
					}
				}
			}

			echo json_encode($data);
			return;
		}
		return;
	}
	
	//resep chooser:
	$resep_table = new Table(
		array("No. Penjualan", "No. Resep", "Tanggal/Jam", "Dokter", "NRM", "No. Reg.", "Pasien", "Alamat"),
		"",
		null,
		true
	);
	$resep_table->setName("resep");
	$resep_table->setModel(Table::$SELECT);
	$resep_adapter = new SimpleAdapter();
	$resep_adapter->add("No. Penjualan", "id", "digit8");
	$resep_adapter->add("No. Resep", "nomor_resep");
	$resep_adapter->add("Tanggal/Jam", "tanggal", "date d-m-Y H:i");
	$resep_adapter->add("Dokter", "nama_dokter");
	$resep_adapter->add("NRM", "nrm_pasien");
	$resep_adapter->add("No. Reg.", "noreg_pasien");
	$resep_adapter->add("Pasien", "nama_pasien");
	$resep_adapter->add("Alamat", "alamat_pasien");
	$resep_dbtable = new DBTable($db, InventoryLibrary::$_TBL_PENJUALAN_RESEP);
	$resep_dbtable->addCustomKriteria(" dibatalkan ", " = 0 ");
	$resep_dbtable->addCustomKriteria(" diretur ", " = 0 ");
	$resep_dbtable->addCustomKriteria("", " tipe='resep' OR tipe='resep_luar' ");
	$resep_dbreseponder = new ResepDBResponder(
		$resep_dbtable,
		$resep_table,
		$resep_adapter
	);
	
	$super_command = new SuperCommand();
	$super_command->addResponder("resep", $resep_dbreseponder);
	$init = $super_command->initialize();
	if ($init != null) {
		echo $init;
		return;
	}
	
	$retur_modal = new Modal("retur_add_form", "smis_form_container", "retur");
	$retur_modal->setTitle("Data Retur Penjualan Resep");
	$retur_modal->setClass(Modal::$FULL_MODEL);
	$id_hidden = new Hidden("retur_id", "retur_id", "");
	$retur_modal->addElement("", $id_hidden);
	$resep_button = new Button("", "", "Pilih");
	$resep_button->setClass("btn-info");
	$resep_button->setIsButton(Button::$ICONIC);
	$resep_button->setIcon("icon-white ".Button::$icon_list_alt);
	$resep_button->setAction("resep.chooser('resep', 'resep_button', 'resep', resep)");
	$resep_button->setAtribute("id='resep_browse'");
	$resep_text = new Text("retur_id_resep", "retur_id_resep", "");
	$resep_text->setAtribute("disabled='disabled'");
	$resep_text->setClass("smis-one-option-input");
	$resep_input_group = new InputGroup("");
	$resep_input_group->addComponent($resep_text);
	$resep_input_group->addComponent($resep_button);
	$retur_modal->addElement("No. Penjualan", $resep_input_group);
	$no_resep_text = new Text("retur_no_resep", "retur_no_resep", "");
	$no_resep_text->setAtribute("disabled='disabled'");
	$retur_modal->addElement("No.Resep", $no_resep_text);
	$dokter_text = new Text("retur_dokter", "retur_dokter", "");
	$dokter_text->setAtribute("disabled='disabled'");
	$retur_modal->addElement("Dokter", $dokter_text);
	$noreg_text = new Text("retur_noreg", "retur_noreg", "");
	$noreg_text->setAtribute("disabled='disabled'");
	$retur_modal->addElement("No. Registrasi", $noreg_text);
	$nrm_text = new Text("retur_nrm", "retur_nrm", "");
	$nrm_text->setAtribute("disabled='disabled'");
	$retur_modal->addElement("NRM", $nrm_text);
	$pasien_text = new Text("retur_pasien", "retur_pasien", "");
	$pasien_text->setAtribute("disabled='disabled'");
	$retur_modal->addElement("Pasien", $pasien_text);
	$alamat_text = new Text("retur_alamat", "retur_alamat", "");
	$alamat_text->setAtribute("disabled='disabled'");
	$retur_modal->addElement("Alamat", $alamat_text);
	$dretur_table = new Table(
		array("Obat", "Tgl. Exp.", "Subtotal", "Jumlah Beli", "Jumlah Retur"),
		"",
		null,
		true
	);
	$dretur_table->setName("dretur");
	$dretur_table->setFooterVisible(false);
	$dretur_table->setAddButtonEnable(false);
	$dretur_table->setReloadButtonEnable(false);
	$dretur_table->setPrintButtonEnable(false);
	$retur_modal->addBody("dretur_table", $dretur_table);
	$retur_button = new Button("", "", "Simpan");
	$retur_button->setClass("btn-success");
	$retur_button->setIcon("fa fa-floppy-o");
	$retur_button->setIsButton(Button::$ICONIC);
	$retur_button->setAtribute("id='retur_save'");
	$retur_button->setAction("retur.save()");
	$retur_modal->addFooter($retur_button);
	
	$dretur_modal = new Modal("dretur_add_form", "smis_form_container", "dretur");
	$dretur_modal->setTitle("Data Detail Retur Penjualan Resep");
	$id_hidden = new Hidden("dretur_id", "dretur_id", "");
	$dretur_modal->addElement("", $id_hidden);
	$nama_obat_text = new Text("dretur_nama_obat", "dretur_nama_obat", "");
	$nama_obat_text->setAtribute("disabled='disabled'");
	$dretur_modal->addElement("Obat", $nama_obat_text);
	$jumlah_hidden = new Hidden("dretur_jumlah", "dretur_jumlah", "");
	$dretur_modal->addElement("", $jumlah_hidden);
	$f_jumlah_text = new Text("dretur_f_jumlah", "dretur_f_jumlah", "");
	$f_jumlah_text->setAtribute("disabled='disabled'");
	$dretur_modal->addElement("Jml. Awal", $f_jumlah_text);
	$jumlah_retur_text = new Text("dretur_jumlah_retur", "dretur_jumlah_retur", "");
	$dretur_modal->addElement("Jml. Retur", $jumlah_retur_text);
	$satuan_text = new Text("dretur_satuan", "dretur_satuan", "");
	$satuan_text->setAtribute("disabled='disabled'");
	$dretur_modal->addElement("Satuan", $satuan_text);
	$dretur_button = new Button("", "", "Simpan");
	$dretur_button->setClass("btn-success");
	$dretur_button->setIcon("fa fa-floppy-o");
	$dretur_button->setIsButton(Button::$ICONIC);
	$dretur_button->setAtribute("id='dretur_save'");
	$dretur_modal->addFooter($dretur_button);
	
	$v_retur_modal = new Modal("v_retur_add_form", "smis_form_container", "v_retur");
	$v_retur_modal->setTitle("Data Retur Penjualan Resep");
	$v_retur_modal->setClass(Modal::$FULL_MODEL);
	$id_hidden = new Hidden("v_retur_id", "v_retur_id", "");
	$v_retur_modal->addElement("", $id_hidden);
	$resep_text = new Text("v_retur_id_resep", "v_retur_id_resep", "");
	$resep_text->setAtribute("disabled='disabled'");
	$v_retur_modal->addElement("No. Penjualan", $resep_text);
	$no_resep_text = new Text("v_retur_no_resep", "v_retur_no_resep", "");
	$no_resep_text->setAtribute("disabled='disabled'");
	$v_retur_modal->addElement("No. Resep", $no_resep_text);
	$dokter_text = new Text("v_retur_dokter", "v_retur_dokter", "");
	$dokter_text->setAtribute("disabled='disabled'");
	$v_retur_modal->addElement("Dokter", $dokter_text);
	$noreg_text = new Text("v_retur_noreg", "v_retur_noreg", "");
	$noreg_text->setAtribute("disabled='disabled'");
	$v_retur_modal->addElement("No. Registrasi", $noreg_text);
	$nrm_text = new Text("v_retur_nrm", "v_retur_nrm", "");
	$nrm_text->setAtribute("disabled='disabled'");
	$v_retur_modal->addElement("NRM", $nrm_text);
	$pasien_text = new Text("v_retur_pasien", "v_retur_pasien", "");
	$pasien_text->setAtribute("disabled='disabled'");
	$v_retur_modal->addElement("Pasien", $pasien_text);
	$alamat_text = new Text("v_retur_alamat", "v_retur_alamat", "");
	$alamat_text->setAtribute("disabled='disabled'");
	$v_retur_modal->addElement("Alamat", $alamat_text);
	$v_dretur_table = new Table(
		array("Obat", "Harga Satuan", "Jumlah Retur"),
		"",
		null,
		true
	);
	$v_dretur_table->setName("v_dretur");
	$v_dretur_table->setFooterVisible(false);
	$v_dretur_table->setAddButtonEnable(false);
	$v_dretur_table->setReloadButtonEnable(false);
	$v_dretur_table->setPrintButtonEnable(false);
	$v_retur_modal->addBody("v_dretur_table", $v_dretur_table);
	$v_retur_button = new Button("", "", "OK");
	$v_retur_button->setClass("btn-success");
	$v_retur_button->setAction("$($(this).data('target')).smodal('hide')");
	$v_retur_modal->addFooter($v_retur_button);

	echo "<div class='alert alert-block alert-danger' id='warning_batal_berobat' style='display: none'></div>";
	echo $v_retur_modal->getHtml();
	echo $dretur_modal->getHtml();
	echo $retur_modal->getHtml();
	echo $retur_penjualan_resep_table->getHtml();
	echo addJS("framework/smis/js/table_action.js");
	echo addJS("depo_farmasi_irja/js/retur_penjualan_resep_action.js", false);
	echo addJS("depo_farmasi_irja/js/dretur_penjualan_resep_action.js", false);
	echo addJS("depo_farmasi_irja/js/retur_penjualan_resep_resep_action.js", false);
	echo addJS("depo_farmasi_irja/js/retur_penjualan_resep.js", false);
	echo addCSS("depo_farmasi_irja/css/retur_penjualan_resep.css", false);
?>