<?php 
	require_once("depo_farmasi_irja/library/InventoryLibrary.php");
	global $db;

	if (isset($_POST['noreg_pasien'])) {
		$noreg = $_POST['noreg_pasien'];
		$response['page'] = 0;
		$response['max_page'] = 0;
		$penjualan_resep_dbtable = new DBTable($db, InventoryLibrary::$_TBL_PENJUALAN_RESEP);
		$resep_rows = $penjualan_resep_dbtable->get_result("
			SELECT *
			FROM " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . "
			WHERE prop = '' AND noreg_pasien = '" . $noreg . "' AND tipe = 'resep' AND dibatalkan = '0'
		");
		$data = array();
		foreach($resep_rows as $rr) {
			$obat_jadi_rows = $penjualan_resep_dbtable->get_result("
				SELECT *
				FROM " . InventoryLibrary::$_TBL_PENJUALAN_OBAT_JADI . "
				WHERE id_penjualan_resep = '" . $rr->id . "'
			");
			foreach($obat_jadi_rows as $ojr) {
				$obat = array();
				$obat['Racikan'] = "-";
				$obat['Obat']= $ojr->nama_obat;
				$obat['Jumlah'] = $ojr->jumlah . " " . $ojr->satuan;
				$data[] = $obat;
			}
			$obat_racikan_rows = $penjualan_resep_dbtable->get_result("
				SELECT *
				FROM " . InventoryLibrary::$_TBL_PENJUALAN_OBAT_RACIKAN . "
				WHERE id_penjualan_resep = '" . $rr->id . "'
			");
			foreach($obat_racikan_rows as $orr) {
				$bahan_racikan_rows = $penjualan_resep_dbtable->get_result("
					SELECT *
					FROM " . InventoryLibrary::$_TBL_BAHAN_PAKAI_OBAT_RACIKAN . "
					WHERE id_penjualan_obat_racikan = '" . $orr->id . "'
				");
				foreach($bahan_racikan_rows as $brr) {
					$obat = array();
					$obat['Racikan'] = $orr->nama;
					$obat['Obat'] = $brr->nama_obat;
					$obat['Jumlah'] = $brr->jumlah . " " . $brr->satuan;
					$data[] = $obat;
				}
			}
		}
		$response['data'] = $data;
		echo json_encode($response);
	}
?>