<?php
	require_once("depo_farmasi_irja/library/InventoryLibrary.php");
	global $db;
	
	$tahun_rows = $db->get_result("
		SELECT DISTINCT YEAR(tanggal) tahun
		FROM " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . "
		WHERE prop = '' AND dibatalkan = 0 AND YEAR(tanggal) > 0
		ORDER BY YEAR(tanggal) ASC
	");
	$data = array();
	if ($tahun_rows != null) {
		foreach ($tahun_rows as $tr)
			$data[] = $tr->tahun;
	}
	echo json_encode($data);
?>