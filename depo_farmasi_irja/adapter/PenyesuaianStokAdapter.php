<?php
	class PenyesuaianStokAdapter extends ArrayAdapter {
		public function adapt($row) {
			$array = array();
			$array['id'] = $row->id;
			$array['Kode Obat'] = $row->kode_obat;
			$array['Nama Obat'] = $row->nama_obat;
			$array['Jenis Obat'] = $row->nama_jenis_obat;
			$array['Jenis Stok'] = self::format("unslug", $row->label);
			$array['Produsen'] = $row->produsen;
			if ($row->konversi == 1)
				$array['Stok'] = $row->sisa . " " . $row->satuan;
			else
				$array['Stok'] = $row->sisa . " " . $row->satuan . " (1 " . $row->satuan . " = " . $row->konversi . " " . $row->satuan_konversi . ")";
			if ($row->tanggal_exp == "0000-00-00")
				$array['Tgl. Exp.'] = "-";
			else
				$array['Tgl. Exp.'] = self::format("date d M Y", $row->tanggal_exp);
			$array['No. BBM'] = $row->no_bbm;
			if ($row->tanggal_datang == null || $row->tanggal_datang == "0000-00-00")
				$array['Tgl. Masuk'] = "N/A";	
			else
				$array['Tgl. Masuk'] = ArrayAdapter::format("date d-m-Y", $row->tanggal_datang);
			return $array;
		}
	}
?>