<?php
	class PenjualanInstansiLainAdapter extends ArrayAdapter {
		public function adapt($row) {
			$array = array();
			$array['id'] = $row->id;
			$array['dibatalkan'] = $row->dibatalkan;
			$array['tanggal'] = $row->tanggal;
			$array['No. Penjualan'] = self::format("digit8", $row->id);
			$array['No. Resep'] = $row->nomor_resep;
			$array['Tanggal/Jam'] = self::format("date d-m-Y H:i", $row->tanggal);
			$array['Nama Instansi'] = $row->nama_pasien;
			$array['Alamat Instansi'] = $row->alamat_pasien;
			$array['No. Telp. Instansi'] = $row->no_telpon;
			if ($row->dibatalkan)
				$array['Status'] = "Dibatalkan";
			else
				$array['Status'] = "-";
			return $array;
		}
	}
?>