<?php 
	require_once("smis-base/smis-include-service-consumer.php");
	require_once("depo_farmasi_irja/library/InventoryLibrary.php");
	global $db;
	
	$laporan_form = new Form("lpop_form", "", "Depo Depo Farmasi IRJA : Laporan Penjualan Obat Per Produsen");
	$produsen_button = new Button("", "", "Pilih");
	$produsen_button->setClass("btn-info");
	$produsen_button->setAction("produsen_lpop.chooser('produsen_lpop', 'produsen_lpop_button', 'produsen_lpop', produsen_lpop)");
	$produsen_button->setIcon("icon-white icon-list-alt");
	$produsen_button->setIsButton(Button::$ICONIC);
	$produsen_button->setAtribute("id='produsen_browse'");
	$produsen_text = new Text("lpop_produsen", "lpop_produsen", "");
	$produsen_text->setClass("smis-one-option-input");
	$produsen_text->setAtribute("disabled='disabled'");
	$produsen_input_group = new InputGroup("");
	$produsen_input_group->addComponent($produsen_text);
	$produsen_input_group->addComponent($produsen_button);
	$laporan_form->addElement("Produsen", $produsen_input_group);
	$tanggal_from_text = new Text("lpop_tanggal_from", "lpop_tanggal_from", date("Y-m-") . "01 00:00");
	$tanggal_from_text->setClass("mydatetime");
	$tanggal_from_text->setAtribute("data-date-format='yyyy-mm-dd hh:ii'");
	$laporan_form->addElement("Waktu Awal", $tanggal_from_text);
	$tanggal_to_text = new Text("lpop_tanggal_to", "lpop_tanggal_to", date("Y-m-d H:i"));
	$tanggal_to_text->setClass("mydatetime");
	$tanggal_to_text->setAtribute("data-date-format='yyyy-mm-dd hh:ii'");
	$laporan_form->addElement("Waktu Akhir", $tanggal_to_text);
	$show_button = new Button("", "", "Tampilkan");
	$show_button->setClass("btn-primary");
	$show_button->setIcon("icon-white icon-repeat");
	$show_button->setIsButton(Button::$ICONIC);
	$show_button->setAction("lpop.view()");
	$print_button = new Button("", "", "Cetak");
	$print_button->setClass("btn-inverse");
	$print_button->setIcon("fa fa-download");
	$print_button->setIsButton(Button::$ICONIC);
	$print_button->setAction("lpop.export_xls()");
	$btn_group = new ButtonGroup("noprint");
	$btn_group->addButton($show_button);
	$btn_group->addButton($print_button);
	$laporan_form->addElement("", $btn_group);
	
	$lpop_table = new Table(
		array("Obat", "Jenis Obat", "Produsen", "Jumlah", "Satuan", "Harga Satuan", "Disk. (%)", "Harga Total"),
		"",
		null,
		true
	);
	$lpop_table->setName("lpop");
	$lpop_table->setAction(false);
	
	//produsen chooser:
	$produsen_table = new Table(array("No.", "Produsen"));
	$produsen_table->setName("produsen_lpop");
	$produsen_table->setModel(Table::$SELECT);
	$produsen_adapter = new SimpleAdapter(true, "No.");
	$produsen_adapter->add("Produsen", "nama");
	$produsen_service_responder = new ServiceResponder(
		$db, 
		$produsen_table, 
		$produsen_adapter,
		"produsen"
	);
	
	$super_command = new SuperCommand();
	$super_command->addResponder("produsen_lpop", $produsen_service_responder);
	$init = $super_command->initialize();
	if ($init != null) {
		echo $init;
		return;
	}
	
	if (isset($_POST['command'])) {
		if ($_POST['command'] == "export_xls") {
			$tanggal_from = $_POST['from'];
			$tanggal_to = $_POST['to'];
			$produsen = $_POST['produsen'];
			require_once("smis-libs-out/php-excel/PHPExcel.php");
			$objPHPExcel = PHPExcel_IOFactory::load("depo_farmasi_irja/templates/template_laporan_penjualan_per_produsen.xlsx");
			$objPHPExcel->setActiveSheetIndexByName("PENJUALAN - PRODUSEN");
			$objWorksheet = $objPHPExcel->getActiveSheet();
			$objWorksheet->setCellValue("B3", "PERIODE : " . ArrayAdapter::format("date d-m-Y, H:i", $tanggal_from) . " s/d " . ArrayAdapter::format("date d-m-Y, H:i", $tanggal_to) . " (" . $produsen . ")");
			$filter = "tanggal >= '" . $tanggal_from . "' AND tanggal <= '" . $tanggal_to . "' AND produsen = '" . $produsen . "'";
			$query_value = "
				SELECT *
				FROM (
					SELECT nama_obat, nama_jenis_obat, produsen, SUM(jumlah) AS 'jumlah', satuan, harga, diskon, t_diskon
					FROM (
						SELECT " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".id, " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".tanggal, " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".id_dokter, " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".nama_dokter, " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".tipe, " . InventoryLibrary::$_TBL_PENJUALAN_OBAT_JADI . ".id_obat, " . InventoryLibrary::$_TBL_PENJUALAN_OBAT_JADI . ".nama_obat, " . InventoryLibrary::$_TBL_PENJUALAN_OBAT_JADI . ".nama_jenis_obat, " . InventoryLibrary::$_TBL_STOK_PAKAI_OBAT_JADI . ".jumlah, " . InventoryLibrary::$_TBL_PENJUALAN_OBAT_JADI . ".satuan, " . InventoryLibrary::$_TBL_STOK_OBAT . ".produsen, " . InventoryLibrary::$_TBL_PENJUALAN_OBAT_JADI . ".harga, 'OBAT JADI' AS 'keterangan', " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".diskon, " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".t_diskon, " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".total
						FROM ((" . InventoryLibrary::$_TBL_STOK_PAKAI_OBAT_JADI . " LEFT JOIN " . InventoryLibrary::$_TBL_STOK_OBAT . " ON " . InventoryLibrary::$_TBL_STOK_PAKAI_OBAT_JADI . ".id_stok_obat = " . InventoryLibrary::$_TBL_STOK_OBAT . ".id) LEFT JOIN " . InventoryLibrary::$_TBL_PENJUALAN_OBAT_JADI . " ON " . InventoryLibrary::$_TBL_STOK_PAKAI_OBAT_JADI . ".id_penjualan_obat_jadi = " . InventoryLibrary::$_TBL_PENJUALAN_OBAT_JADI . ".id) LEFT JOIN " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . " ON " . InventoryLibrary::$_TBL_PENJUALAN_OBAT_JADI . ".id_penjualan_resep = " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".id
						WHERE " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".prop NOT LIKE 'del' AND " . InventoryLibrary::$_TBL_PENJUALAN_OBAT_JADI . ".prop NOT LIKE 'del' AND " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".dibatalkan = 0 AND " . InventoryLibrary::$_TBL_STOK_PAKAI_OBAT_JADI . ".jumlah > 0
						UNION ALL
						SELECT " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".id, " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".tanggal, " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".id_dokter, " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".nama_dokter, " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".tipe, " . InventoryLibrary::$_TBL_BAHAN_PAKAI_OBAT_RACIKAN . ".id_obat, " . InventoryLibrary::$_TBL_BAHAN_PAKAI_OBAT_RACIKAN . ".nama_obat, " . InventoryLibrary::$_TBL_BAHAN_PAKAI_OBAT_RACIKAN . ".nama_jenis_obat, " . InventoryLibrary::$_TBL_STOK_PAKAI_BAHAN . ".jumlah, " . InventoryLibrary::$_TBL_BAHAN_PAKAI_OBAT_RACIKAN . ".satuan, " . InventoryLibrary::$_TBL_STOK_OBAT . ".produsen, " . InventoryLibrary::$_TBL_BAHAN_PAKAI_OBAT_RACIKAN . ".harga, CONCAT('BAHAN RACIKAN ', " . InventoryLibrary::$_TBL_PENJUALAN_OBAT_RACIKAN . ".nama) AS 'keterangan', " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".diskon, " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".t_diskon, " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".total
						FROM (((" . InventoryLibrary::$_TBL_STOK_PAKAI_BAHAN . " LEFT JOIN " . InventoryLibrary::$_TBL_STOK_OBAT . " ON " . InventoryLibrary::$_TBL_STOK_PAKAI_BAHAN . ".id_stok_obat = " . InventoryLibrary::$_TBL_STOK_OBAT . ".id) LEFT JOIN " . InventoryLibrary::$_TBL_BAHAN_PAKAI_OBAT_RACIKAN . " ON " . InventoryLibrary::$_TBL_STOK_PAKAI_BAHAN . ".id_bahan = " . InventoryLibrary::$_TBL_BAHAN_PAKAI_OBAT_RACIKAN . ".id) LEFT JOIN " . InventoryLibrary::$_TBL_PENJUALAN_OBAT_RACIKAN . " ON " . InventoryLibrary::$_TBL_BAHAN_PAKAI_OBAT_RACIKAN . ".id_penjualan_obat_racikan = " . InventoryLibrary::$_TBL_PENJUALAN_OBAT_RACIKAN . ".id) LEFT JOIN " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . " ON " . InventoryLibrary::$_TBL_PENJUALAN_OBAT_RACIKAN . ".id_penjualan_resep = " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".id
						WHERE " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".prop NOT LIKE 'del' AND " . InventoryLibrary::$_TBL_PENJUALAN_OBAT_RACIKAN . ".prop NOT LIKE 'del' AND " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".dibatalkan = 0 AND " . InventoryLibrary::$_TBL_STOK_PAKAI_BAHAN . ".jumlah > 0
					) v_lpop
					WHERE " . $filter . "
					GROUP BY id_obat, satuan, harga, diskon, t_diskon
					ORDER BY nama_obat, nama_jenis_obat ASC
				) v_laporan
			";
			$dbtable = new DBTable($db, "" . InventoryLibrary::$_TBL_PENJUALAN_RESEP . "");
			$data = $dbtable->get_result($query_value);
			if (count($data) - 3 > 0)
				$objWorksheet->insertNewRowBefore(8, count($data) - 3);
			$start_row_num = 7;
			$end_row_num = 7;
			$row_num = $start_row_num;
			$nomor = 1;
			foreach ($data as $d) {
				$col_num = 1;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $nomor++);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->nama_obat);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->nama_jenis_obat);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->produsen);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->jumlah);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->satuan);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->harga);
				$v_diskon = $d->diskon;
				if ($d->t_diskon == "nominal") {
					$v_diskon = ($d->diskon / $d->total) * 100;
					$v_diskon = round($v_diskon, 2);
				}
				$harga_total = $d->jumlah * $d->harga - (($v_diskon / 100) * $d->jumlah * $d->harga);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $v_diskon);
				$col_num++;				
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $harga_total);
				$objWorksheet->getStyle("E" . $row_num)->getNumberFormat()->setFormatCode("#,##0");
				$objWorksheet->getStyle("J" . $row_num)->getNumberFormat()->setFormatCode("#,##0");
				$row_num++;
				$end_row_num++;
			}
			header("Content-type: application/vnd.ms-excel");	
			header("Content-Disposition: attachment; filename=LAPORAN_PENJUALAN_OBAT_PER_PRODUSEN_" . $produsen . "_" . ArrayAdapter::format("date Ymd_Hi", $tanggal_from) . "_" . ArrayAdapter::format("date Ymd_Hi", $tanggal_to) . ".xlsx");
			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");
			$objWriter->save("php://output");
			return;
		}
		class LPODPAdapter extends ArrayAdapter {
			public function adapt($row) {
				$array = array();
				$array['Obat'] = $row->nama_obat;
				$array['Jenis Obat'] = $row->nama_jenis_obat;
				$array['Produsen'] = $row->produsen;
				$array['Jumlah'] = $row->jumlah;
				$array['Satuan'] = $row->satuan;
				$array['Harga Satuan'] = self::format("money Rp.", $row->harga);
				$f_diskon = self::format("only-money", $row->diskon);
				$v_diskon = $row->diskon;
				if ($row->t_diskon == "nominal") {
					$v_diskon = ($row->diskon / $row->total) * 100;
					$v_diskon = round($v_diskon, 2);
					$f_diskon = self::format("only-money", $v_diskon) . "<sup>*</sup>";
				}
				$array['Disk. (%)'] = $f_diskon;
				$harga_total = $row->jumlah * $row->harga - (($v_diskon / 100) * $row->jumlah * $row->harga);
				$array['Harga Total'] = self::format("money Rp.", $harga_total);
				return $array;
			}
		}
		$lpop_adapter = new LPODPAdapter();		
		$lpop_dbtable = new DBTable($db, "" . InventoryLibrary::$_TBL_PENJUALAN_RESEP . "");
		if (isset($_POST['command']) == "list") {
			$filter = "tanggal >= '" . $_POST['tanggal_from'] . "' AND tanggal <= '" . $_POST['tanggal_to'] . "' AND produsen = '" . $_POST['produsen'] . "'";
			if (isset($_POST['kriteria'])) {
				$filter .= " AND (nama_obat LIKE '%" . $_POST['kriteria'] . "%' OR nama_jenis_obat LIKE '%" . $_POST['kriteria'] . "%' OR satuan LIKE '%" . $_POST['kriteria'] . "%') ";
			}
			$query_value = "
				SELECT *
				FROM (
					SELECT nama_obat, nama_jenis_obat, produsen, SUM(jumlah) AS 'jumlah', satuan, harga, diskon, t_diskon
					FROM (
						SELECT " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".id, " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".tanggal, " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".id_dokter, " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".nama_dokter, " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".tipe, " . InventoryLibrary::$_TBL_PENJUALAN_OBAT_JADI . ".id_obat, " . InventoryLibrary::$_TBL_PENJUALAN_OBAT_JADI . ".nama_obat, " . InventoryLibrary::$_TBL_PENJUALAN_OBAT_JADI . ".nama_jenis_obat, " . InventoryLibrary::$_TBL_STOK_PAKAI_OBAT_JADI . ".jumlah, " . InventoryLibrary::$_TBL_PENJUALAN_OBAT_JADI . ".satuan, " . InventoryLibrary::$_TBL_STOK_OBAT . ".produsen, " . InventoryLibrary::$_TBL_PENJUALAN_OBAT_JADI . ".harga, 'OBAT JADI' AS 'keterangan', " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".diskon, " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".t_diskon, " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".total
						FROM ((" . InventoryLibrary::$_TBL_STOK_PAKAI_OBAT_JADI . " LEFT JOIN " . InventoryLibrary::$_TBL_STOK_OBAT . " ON " . InventoryLibrary::$_TBL_STOK_PAKAI_OBAT_JADI . ".id_stok_obat = " . InventoryLibrary::$_TBL_STOK_OBAT . ".id) LEFT JOIN " . InventoryLibrary::$_TBL_PENJUALAN_OBAT_JADI . " ON " . InventoryLibrary::$_TBL_STOK_PAKAI_OBAT_JADI . ".id_penjualan_obat_jadi = " . InventoryLibrary::$_TBL_PENJUALAN_OBAT_JADI . ".id) LEFT JOIN " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . " ON " . InventoryLibrary::$_TBL_PENJUALAN_OBAT_JADI . ".id_penjualan_resep = " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".id
						WHERE " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".prop NOT LIKE 'del' AND " . InventoryLibrary::$_TBL_PENJUALAN_OBAT_JADI . ".prop NOT LIKE 'del' AND " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".dibatalkan = 0 AND " . InventoryLibrary::$_TBL_STOK_PAKAI_OBAT_JADI . ".jumlah > 0
						UNION ALL
						SELECT " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".id, " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".tanggal, " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".id_dokter, " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".nama_dokter, " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".tipe, " . InventoryLibrary::$_TBL_BAHAN_PAKAI_OBAT_RACIKAN . ".id_obat, " . InventoryLibrary::$_TBL_BAHAN_PAKAI_OBAT_RACIKAN . ".nama_obat, " . InventoryLibrary::$_TBL_BAHAN_PAKAI_OBAT_RACIKAN . ".nama_jenis_obat, " . InventoryLibrary::$_TBL_STOK_PAKAI_BAHAN . ".jumlah, " . InventoryLibrary::$_TBL_BAHAN_PAKAI_OBAT_RACIKAN . ".satuan, " . InventoryLibrary::$_TBL_STOK_OBAT . ".produsen, " . InventoryLibrary::$_TBL_BAHAN_PAKAI_OBAT_RACIKAN . ".harga, CONCAT('BAHAN RACIKAN ', " . InventoryLibrary::$_TBL_PENJUALAN_OBAT_RACIKAN . ".nama) AS 'keterangan', " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".diskon, " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".t_diskon, " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".total
						FROM (((" . InventoryLibrary::$_TBL_STOK_PAKAI_BAHAN . " LEFT JOIN " . InventoryLibrary::$_TBL_STOK_OBAT . " ON " . InventoryLibrary::$_TBL_STOK_PAKAI_BAHAN . ".id_stok_obat = " . InventoryLibrary::$_TBL_STOK_OBAT . ".id) LEFT JOIN " . InventoryLibrary::$_TBL_BAHAN_PAKAI_OBAT_RACIKAN . " ON " . InventoryLibrary::$_TBL_STOK_PAKAI_BAHAN . ".id_bahan = " . InventoryLibrary::$_TBL_BAHAN_PAKAI_OBAT_RACIKAN . ".id) LEFT JOIN " . InventoryLibrary::$_TBL_PENJUALAN_OBAT_RACIKAN . " ON " . InventoryLibrary::$_TBL_BAHAN_PAKAI_OBAT_RACIKAN . ".id_penjualan_obat_racikan = " . InventoryLibrary::$_TBL_PENJUALAN_OBAT_RACIKAN . ".id) LEFT JOIN " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . " ON " . InventoryLibrary::$_TBL_PENJUALAN_OBAT_RACIKAN . ".id_penjualan_resep = " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".id
						WHERE " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".prop NOT LIKE 'del' AND " . InventoryLibrary::$_TBL_PENJUALAN_OBAT_RACIKAN . ".prop NOT LIKE 'del' AND " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".dibatalkan = 0 AND " . InventoryLibrary::$_TBL_STOK_PAKAI_BAHAN . ".jumlah > 0
					) v_lpop
					WHERE " . $filter . "
					GROUP BY id_obat, satuan, harga, diskon, t_diskon
					ORDER BY nama_obat, nama_jenis_obat ASC
				) v_laporan
			";
			$query_count = "
				SELECT COUNT(*)
				FROM (
					" . $query_value . "
				) v_laporan
			";
			$lpop_dbtable->setPreferredQuery(true, $query_value, $query_count);
		}
		$lpop_dbresponder = new DBResponder(
			$lpop_dbtable,
			$lpop_table,
			$lpop_adapter
		);
		$data = $lpop_dbresponder->command($_POST['command']);
		echo json_encode($data);
		return;
	}
	
	echo $laporan_form->getHtml();
	echo "<div id='table_content'>";
	echo $lpop_table->getHtml();
	echo "</div>";
	echo addJS("framework/smis/js/table_action.js");
	echo addJS("framework/bootstrap/js/bootstrap-datetimepicker.js");
	echo addCSS("framework/bootstrap/css/bootstrap-datetimepicker.css");
?>
<script type="text/javascript">
	function LPOPAction(name, page, action, column) {
		this.initialize(name, page, action, column);
	}
	LPOPAction.prototype.constructor = LPOPAction;
	LPOPAction.prototype = new TableAction();
	LPOPAction.prototype.view = function() {
		var self = this;
		var data = this.getViewData();
		data['tanggal_from'] = $("#lpop_tanggal_from").val();
		data['tanggal_to'] = $("#lpop_tanggal_to").val();
		data['produsen'] = $("#lpop_produsen").val();
		showLoading();
		$.post(
			"",
			data,
			function(response) {
				var json = getContent(response);
				if (json == null) return;
				$("#" + self.prefix + "_list").html(json.list);
				$("#" + self.prefix + "_pagination").html(json.pagination);
				dismissLoading();
			}
		);
	};
	LPOPAction.prototype.export_xls = function() {
		if ($("#lpop_tanggal_from").val() == "" || $("#lpop_tanggal_to").val() == "")
			return;
		showLoading();
		var data = this.getRegulerData();
		data['command'] = "export_xls";
		data['from'] = $("#lpop_tanggal_from").val();
		data['to'] = $("#lpop_tanggal_to").val();
		data['produsen'] = $("#lpop_produsen").val();
		postForm(data);
		dismissLoading();
	}
	
	function ProdusenLPOPAction(name, page, action, column) {
		this.initialize(name, page, action, column);
	}
	ProdusenLPOPAction.prototype.constructor = ProdusenLPOPAction;
	ProdusenLPOPAction.prototype = new TableAction();
	ProdusenLPOPAction.prototype.selected = function(json) {
		$("#lpop_produsen").val(json.nama);
	};
	
	var lpop;
	var produsen_lpop;
	$(document).ready(function() {
		$('[data-toggle="popover"]').popover({
			trigger: 'hover',
			'placement': 'top'
		});
		produsen_lpop = new ProdusenLPOPAction(
			"produsen_lpop",
			"depo_farmasi_irja",
			"laporan_penjualan_obat_per_produsen",
			new Array()
		);
		produsen_lpop.setSuperCommand("produsen_lpop");
		lpop = new LPOPAction(
			"lpop",
			"depo_farmasi_irja",
			"laporan_penjualan_obat_per_produsen",
			new Array()
		);
		lpop.view();
		$('.mydatetime').datetimepicker({ 
			minuteStep: 1
		});
	});
</script>