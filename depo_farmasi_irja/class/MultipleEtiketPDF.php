<?php
    require_once("depo_farmasi_irja/library/InventoryLibrary.php");

	class MultipleEtiketPDF {
        protected $_template_url;

		protected $_font_face;
		protected $_paper_size_w;
		protected $_paper_size_h;
		protected $_left_margin;
        protected $_top_margin;

		protected $_img_width;
		protected $_img_height;
		protected $_img_x;
        protected $_img_y;
        
        protected $_obat_jadi_rows;
        protected $_obat_racikan_rows;

        protected $_nama_user;

		public function __construct($db, $id_penjualan_resep, $nama_user) {
            $this->_template_url    = "depo_farmasi_irja/templates/etiket_template.png";

			$this->_font_face 	    = "Arial";
			$this->_paper_size_w    = 65;
			$this->_paper_size_h    = 58;
			$this->_left_margin     = 0;
            $this->_top_margin 	    = 0;

			$this->_img_width 	    = 65;
			$this->_img_height 	    = 40;
			$this->_img_x 		    = 0;
            $this->_img_y 		    = 0;
            
            $this->_nama_user = $nama_user;

			$this->_obat_jadi_rows = $db->get_result("
                SELECT
                    a.tanggal, a.nrm_pasien, a.nama_pasien, b.*
                FROM
                    " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . " a
                        INNER JOIN " . InventoryLibrary::$_TBL_PENJUALAN_OBAT_JADI . " b ON a.id = b.id_penjualan_resep
                WHERE
                    a.id = '" . $id_penjualan_resep . "' AND b.prop = ''
			");

			$this->_obat_racikan_rows = $db->get_result("
				SELECT
                    a.tanggal, a.nrm_pasien, a.nama_pasien, b.*
                FROM
                    " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . " a
                        INNER JOIN " . InventoryLibrary::$_TBL_PENJUALAN_OBAT_RACIKAN . " b ON a.id = b.id_penjualan_resep
                WHERE
                    a.id = '" . $id_penjualan_resep . "' AND b.prop = ''
			");
		}

		public function Draw() {
			require_once("depo_farmasi_irja/class/PDF.php");
			$fpdf = new PDF("L", "mm", array($this->_paper_size_w, $this->_paper_size_h));
			$fpdf->SetTopMargin($this->_top_margin);
			$fpdf->SetLeftMargin($this->_left_margin);
			
			if ($this->_obat_jadi_rows != null) {
				foreach ($this->_obat_jadi_rows as $obat_jadi_row) {
                    $tanggal = ArrayAdapter::format("date d - m - Y", $obat_jadi_row->tanggal);
                    $nrm_pasien = $obat_jadi_row->nrm_pasien;
					$nama_pasien = $obat_jadi_row->nama_pasien;
					$nama_obat = $obat_jadi_row->nama_obat;
                    $tanggal_exp = $obat_jadi_row->tanggal_exp == "0000-00-00" || $obat_jadi_row->tanggal_exp == "" ? "-" : ArrayAdapter::format("date d - m - Y", $obat_jadi_row->tanggal_exp);
                    $jumlah_pakai_sehari = "N/A";
                    if (strlen($obat_jadi_row->penggunaan) > 0) {
                        $jumlah_pakai_sehari = trim($obat_jadi_row->penggunaan, "x");
                    }
                    $satuan_pakai = $obat_jadi_row->satuan_pakai;
                    $pemakaian = $obat_jadi_row->pemakaian;
                    $pemakaian_obat_luar = $obat_jadi_row->pemakaian_obat_luar;
                    $obat_luar = $obat_jadi_row->obat_luar;
                    $takaran_pakai = $obat_jadi_row->takaran_pakai;

                    $line_1 = "";
                    $line_2 = "";
                    if ($obat_luar == 1) {
                        $line_1 =  $jumlah_pakai_sehari . " x sehari " . $pemakaian_obat_luar;
                        $line_2 = "OBAT LUAR";
                    } else {
                        $line_1 = $jumlah_pakai_sehari . " x sehari " . $takaran_pakai . " " . $satuan_pakai;
                        $line_2 = $pemakaian . " makan";
                    }

                    $line_3 = "";
                    if ($obat_jadi_row->penggunaan == "1x") {
                        $line_3 = "Pagi, Siang, Sore, Malam";
                        if ($obat_jadi_row->detail_penggunaan != "") {
                            $line_3 = $obat_jadi_row->detail_penggunaan;
                        }
                    } else if ($obat_jadi_row->penggunaan == "2x") {
                        $line_3 = "Pagi - Siang, Pagi - Sore";
                        if ($obat_jadi_row->detail_penggunaan != "") {
                            $line_3 = $obat_jadi_row->detail_penggunaan;
                        }
                    }
                    else if ($obat_jadi_row->penggunaan == "3x")
                        $line_3 = "Pagi - Siang - Malam";
                    else if ($obat_jadi_row->penggunaan == "4x")
                        $line_3 = "Pagi - Siang - Sore - Malam";

					$fpdf->AddPage();
                    $fpdf->Image($this->_template_url, $this->_img_x, $this->_img_y, $this->_img_width, $this->_img_height);
                    $fpdf->SetFont($this->_font_face, "", 8);
                    $fpdf->SetX(0);
                    $fpdf->SetY(0);
                    $fpdf->Cell($this->_paper_size_w, 10, "", 0, 1);
                    $fpdf->Cell(17, 3.75, "", 0, 0);
                    $fpdf->Cell(20, 3.75, $nrm_pasien, 0, 0);
                    $fpdf->Cell(5.5, 3.75, "", 0, 0);
                    $fpdf->Cell(22.5, 3.75, $tanggal, 0, 1);
                    $fpdf->Cell(17, 3.75, "", 0, 0);
                    $fpdf->Cell(43, 3.75, $tanggal_exp, 0, 1);
                    $fpdf->Cell(17, 3.75, "", 0, 0);
                    $fpdf->MultiCell(48, 3.75, $nama_obat, 0, 'L', false, 1);
                    $fpdf->SetX(0);
                    $fpdf->SetFont($this->_font_face, "B", 8);
                    $fpdf->MultiCell(65, 3.75, $nama_pasien, 0, 'C', false, 1);
                    $fpdf->SetFont($this->_font_face, "", 8);
                    $fpdf->SetX(0);
                    $fpdf->MultiCell(65, 3.75, $line_1, 0, 'C', false, 1);
                    $fpdf->SetX(0);
                    $fpdf->MultiCell(65, 3.75, $line_3, 0, 'C', false, 1);
                    if ($obat_luar == 1)
                        $fpdf->SetFont($this->_font_face, "B", 8);
                    $fpdf->MultiCell(65, 3.75, $line_2, 0, 'C', false, 1);
				}
			}

			if ($this->_obat_racikan_rows != null) {
				foreach ($this->_obat_racikan_rows as $obat_racikan_row) {
                    $tanggal = ArrayAdapter::format("date d - m - Y", $obat_racikan_row->tanggal);
                    $nrm_pasien = $obat_racikan_row->nrm_pasien;
					$nama_pasien = $obat_racikan_row->nama_pasien;
					$nama_obat = $obat_racikan_row->nama;
                    $tanggal_exp = $obat_racikan_row->tanggal_exp == "0000-00-00" || $obat_racikan_row->tanggal_exp == "" ? "-" : ArrayAdapter::format("date d - m - Y", $obat_racikan_row->tanggal_exp);
                    $jumlah_pakai_sehari = "N/A";
                    if (strlen($obat_racikan_row->penggunaan) > 0) {
                        $jumlah_pakai_sehari = trim($obat_racikan_row->penggunaan, "x");
                    }
                    $satuan_pakai = $obat_racikan_row->satuan_pakai;
                    $pemakaian = $obat_racikan_row->pemakaian;
                    $pemakaian_obat_luar = $obat_racikan_row->pemakaian_obat_luar;
                    $obat_luar = $obat_racikan_row->obat_luar;
                    $takaran_pakai = $obat_racikan_row->takaran_pakai;

                    $line_1 = "";
                    $line_2 = "";
                    if ($obat_luar == 1) {
                        $line_1 =  $jumlah_pakai_sehari . " x sehari " . $pemakaian_obat_luar;
                        $line_2 = "OBAT LUAR";
                    } else {
                        $line_1 = $jumlah_pakai_sehari . " x sehari " . $takaran_pakai . " " . $satuan_pakai;
                        $line_2 = $pemakaian . " makan";
                    }

                    $line_3 = "";
                    if ($obat_racikan_row->penggunaan == "1x") {
                        $line_3 = "Pagi, Siang, Sore, Malam";
                        if ($obat_racikan_row->detail_penggunaan != "") {
                            $line_3 = $obat_racikan_row->detail_penggunaan;
                        }
                    } else if ($obat_racikan_row->penggunaan == "2x") {
                        $line_3 = "Pagi - Siang, Pagi - Sore";
                        if ($obat_racikan_row->detail_penggunaan != "") {
                            $line_3 = $obat_racikan_row->detail_penggunaan;
                        }
                    } else if ($obat_racikan_row->penggunaan == "3x")
                        $line_3 = "Pagi - Siang - Malam";
                    else if ($obat_racikan_row->penggunaan == "4x")
                        $line_3 = "Pagi - Siang - Sore - Malam";

					$fpdf->AddPage();
                    $fpdf->Image($this->_template_url, $this->_img_x, $this->_img_y, $this->_img_width, $this->_img_height);
                    $fpdf->SetFont($this->_font_face, "", 8);
                    $fpdf->SetX(0);
                    $fpdf->SetY(0);
                    $fpdf->Cell($this->_paper_size_w, 10, "", 0, 1);
                    $fpdf->Cell(17, 3.75, "", 0, 0);
                    $fpdf->Cell(20, 3.75, $nrm_pasien, 0, 0);
                    $fpdf->Cell(5.5, 3.75, "", 0, 0);
                    $fpdf->Cell(22.5, 3.75, $tanggal, 0, 1);
                    $fpdf->Cell(17, 3.75, "", 0, 0);
                    $fpdf->Cell(43, 3.75, $tanggal_exp, 0, 1);
                    $fpdf->Cell(17, 3.75, "", 0, 0);
                    $fpdf->MultiCell(48, 3.75, $nama_obat, 0, 'L', false, 1);
                    $fpdf->SetX(0);
                    $fpdf->SetFont($this->_font_face, "B", 8);
                    $fpdf->MultiCell(65, 3.75, $nama_pasien, 0, 'C', false, 1);
                    $fpdf->SetFont($this->_font_face, "", 8);
                    $fpdf->SetX(0);
                    $fpdf->MultiCell(65, 3.75, $line_1, 0, 'C', false, 1);
                    $fpdf->SetX(0);
                    $fpdf->MultiCell(65, 3.75, $line_3, 0, 'C', false, 1);
                    if ($obat_luar == 1)
                        $fpdf->SetFont($this->_font_face, "B", 8);
                    $fpdf->MultiCell(65, 3.75, $line_2, 0, 'C', false, 1);
				}
			}

		    $md5 = md5(date("Ymd_His") . "_" . $this->_nama_user);
			$filename = "etiket.pdf";
			$pathfile = "smis-temp/" . $md5 . $filename;
			$fpdf->Output($pathfile, "F");
			return $pathfile;
		}
	}
?>