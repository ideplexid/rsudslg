<?php
	global $db;

	$laporan_tabulator = new Tabulator("laporan_rekap_retur_penjualan", "", Tabulator::$POTRAIT);
	$laporan_tabulator->add("laporan_rekap_retur_penjualan_resep", "Lap. Rekap Retur Penjualan Resep", "depo_farmasi_irja/laporan_rekap_retur_penjualan_resep.php", Tabulator::$TYPE_INCLUDE);
	$laporan_tabulator->add("laporan_rekap_retur_penjualan_bebas", "Lap. Rekap Retur Penjualan Bebas", "depo_farmasi_irja/laporan_rekap_retur_penjualan_bebas.php", Tabulator::$TYPE_INCLUDE);
	echo $laporan_tabulator->getHtml();
?>