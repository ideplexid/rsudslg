<?php
global $NAVIGATOR;
// Administrator Top Menu
$mr = new Menu ("fa fa-user-md");
$mr->addProperty ( 'title', 'Fisiotherapy' );
$mr->addProperty ( 'name', 'Fisiotherapy' );
$mr->addSubMenu ( "Tindakan Fisiotherapy", "fisiotherapy", "pemeriksaan", "Tindakan" ,"fa fa-stethoscope");
$mr->addSubMenu ( "Arsip Fisiotherapy", "fisiotherapy", "arsip", "Arsip" ,"fa fa-archive");
$mr->addSubMenu ( "Arsip Admin", "fisiotherapy", "arsip_admin", "Arsip Admin" ,"fa fa-archive");
$mr->addSubMenu ( "Layanan Fisiotherapy", "fisiotherapy", "layanan", "Layanan" ,"fa fa-heart");
$mr->addSubMenu ( "Settings", "fisiotherapy", "settings", "Settings" ,"fa fa-cog");
$mr->addSubMenu ( "Laporan", "fisiotherapy", "laporan", "Laporan" ,"fa fa-pie-chart");
$mr->addSubMenu ( "Laporan Detail", "fisiotherapy", "laporan_detail", "Laporan Detail Pasien" ,"fa fa-file-o");
$mr->addSubMenu ( "Koreksi Diagnosa Pasien", "fisiotherapy", "koreksi_diagnosa_pasien", "Koreksi Diagnosa Pasien" ,"fa fa-file-o");
$mr->addSubMenu ( "Laporan Pendapatan Per Layanan", "fisiotherapy", "laporan_pend_perlayanan", "Laporan Pendapatan Per Layanan" ,"fa fa-money");
$mr->addSubMenu ( "Laporan Asuhan Fisiotherapy", "fisiotherapy", "laporan_asuhan", "Laporan Asuhan Fisiotherapy" ,"fa fa-money");
$mr->addSubMenu ( "Laporan Dokter Rehab", "fisiotherapy", "laporan_dokter_rehab", "Laporan Dokter Rehab Fisiotherapy" ,"fa fa-money");
//implementasi dari library 

require_once 'smis-libs-inventory/navigation.php';
$inav=new InventoryNavigator($NAVIGATOR, "", "Fisiotherapy", "fisiotherapy");
$mr=$inav->extendMenu($mr);
$NAVIGATOR->addMenu ( $mr, 'fisiotherapy' );
?>
