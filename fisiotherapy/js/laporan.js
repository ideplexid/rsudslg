var laporan;
var laporan_karyawan;
var laporan_data;
var IS_laporan_RUNNING;
$(document).ready(function(){
    $('.mydate').datepicker();
    laporan=new TableAction("laporan","fisiotherapy","laporan",new Array());
    laporan.addRegulerData=function(data){
        data['dari']=$("#laporan_dari").val();
        data['sampai']=$("#laporan_sampai").val();
        $("#dari_table_laporan").html(getFormattedDate(data['dari']));
        $("#sampai_table_laporan").html(getFormattedDate(data['sampai']));		
        return data;
    };

    laporan.batal=function(){
        IS_laporan_RUNNING=false;
        $("#rekap_fis_modal").modal("hide");
    };
    
    laporan.afterview=function(json){
        if(json!=null){
            $("#kode_table_laporan").html(json.nomor);
            $("#waktu_table_laporan").html(json.waktu);
            laporan_data=json;
        }
    };

    laporan.rekaptotal=function(){
        if(IS_laporan_RUNNING) return;
        $("#rekap_rad_bar").sload("true","Fetching total data",0);
        $("#rekap_fis_modal").modal("show");
        IS_laporan_RUNNING=true;
        var d=this.getRegulerData();
        d['super_command']="total";
        $.post("",d,function(res){
            var total=Number(getContent(res));
            if(total>0) {
                laporan.rekaploop(0,total);
            } else {
                $("#rekap_fis_modal").modal("hide");
                IS_laporan_RUNNING=false;
            }
        });
    };

    laporan.rekaploop=function(current,total){
        $("#rekap_rad_bar").sload("true","Calculation... [ "+current+" / "+total+" ] ",(current*100/total));
        if(current>=total || !IS_laporan_RUNNING) {
            $("#rekap_fis_modal").modal("hide");
            laporan.view();
            IS_laporan_RUNNING=false;
            return;
        }
        var d=this.getRegulerData();
        d['super_command']="limit";
        d['limit_start']=current;			
        $.post("",d,function(res){
            setTimeout(function(){laporan.rekaploop(++current,total)},300);
        });
    };
            
});