<?php
global $db;
global $user;
require_once 'smis-libs-class/Policy.php';
require_once 'smis-libs-inventory/policy.php';
$ipolicy=new InventoryPolicy("fisiotherapy", $user,"modul/");

$policy=new Policy("fisiotherapy", $user);
$policy->addPolicy("pemeriksaan", "pemeriksaan", Policy::$DEFAULT_COOKIE_CHANGE,"modul/pemeriksaan");
$policy->addPolicy("rehab_medik","pemeriksaan",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/pemeriksaan/rehab_medik");
$policy->addPolicy("pelayanan_khusus","pemeriksaan",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/pemeriksaan/pelayanan_khusus");
$policy->addPolicy("lap_rl52","pemeriksaan",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/pemeriksaan/lap_rl52");

$policy->addPolicy("arsip", "arsip", Policy::$DEFAULT_COOKIE_CHANGE,"modul/arsip");
$policy->addPolicy("arsip_admin", "arsip_admin", Policy::$DEFAULT_COOKIE_CHANGE,"modul/arsip_admin");
$policy->addPolicy("layanan", "layanan", Policy::$DEFAULT_COOKIE_CHANGE,"modul/layanan");
$policy->addPolicy("settings", "settings", Policy::$DEFAULT_COOKIE_CHANGE,"modul/settings");
$policy->addPolicy("laporan", "laporan", Policy::$DEFAULT_COOKIE_CHANGE,"modul/laporan");
$policy->addPolicy("laporan_detail", "laporan_detail", Policy::$DEFAULT_COOKIE_CHANGE,"modul/laporan_detail");
$policy->addPolicy("koreksi_diagnosa_pasien", "koreksi_diagnosa_pasien", Policy::$DEFAULT_COOKIE_CHANGE,"modul/koreksi_diagnosa_pasien");
$policy->addPolicy("laporan_pend_perlayanan", "laporan_pend_perlayanan", Policy::$DEFAULT_COOKIE_CHANGE,"modul/laporan_pend_perlayanan");
$policy->addPolicy("laporan_asuhan", "laporan_asuhan", Policy::$DEFAULT_COOKIE_CHANGE,"modul/laporan_asuhan");
$policy->addPolicy("laporan_dokter_rehab", "laporan_dokter_rehab", Policy::$DEFAULT_COOKIE_CHANGE,"modul/laporan_dokter_rehab");
$policy->addPolicy("kode_akun", "settings", Policy::$DEFAULT_COOKIE_KEEP,"snippet/kode_akun");
$policy->setDefaultPolicy(Policy::$DEFAULT_POLICY_RESTRICT);
$policy->combinePolicy($ipolicy);
$policy->initialize();

?>
