<?php
show_error();
class FisiotherapyResource {
	public $list_layanan;
	public $list_pesan;
	public $list_reporting;
    public $list_name;
    public $list_debit_kredit;
	private $db;
	public function __construct($db) {
		$this->db=$db;
		$this->list_reporting=array();
		$this->initListLayanan();
		$this->initPesan();
		$this->initListReporting();
        $this->list_name=array();
        $this->list_debit_kredit=array();
	}
    
    public function getDebitKredit($slug,$carabayar=""){
        $x = $this->list_debit_kredit[$slug];
        if($carabayar!=""){
            $carabayar = "_".$carabayar;
        }
        $result['d']=$x['debit'.$carabayar];
        $result['k']=$x['kredit'.$carabayar];
        return $result;
    }
    
    public function getListName($slug){
        return $this->list_name[$slug];
    }
	
	public function initListReporting(){
		foreach($this->list_layanan as $id=>$name){
			$this->list_reporting[$id]=array("name"=>$name);
		}
	}
	
	public function initListLayanan() {
		$dbtable = new DBTable($this->db, "smis_fst_layanan");
		$dbtable ->setShowAll(true);
		$dbtable ->setOrder(" nama ASC ");
		$data    = $dbtable->view("", "0");
		$list    = $data['data'];
		$array   = array();
		foreach($list as $d){
			$slug                           = $d->slug;
			$name                           = $d->nama;
			$array[$slug]                   = $name;
            $this->list_name[$slug]         = $name;
            $this->list_debit_kredit[$slug] = $d;
		}
		$this->list_layanan                 = $array;
	}
	
	public function initPesan(){
		$this->list_pesan = array();
		foreach($this->list_layanan as $name=>$value){
			$this->list_pesan[] = $name;
		}
	}
	
	public function getNyeriType(){
		$nyeri=new OptionBuilder();
		$nyeri->addSingle("");
		$nyeri->addSingle("Numeric Rating Scale 0 : tidak nyeri");
		$nyeri->addSingle("Numeric Rating Scale 1 : nyeri ringan");
		$nyeri->addSingle("Numeric Rating Scale 2 : nyeri ringan");
		$nyeri->addSingle("Numeric Rating Scale 3 : nyeri ringan");
		$nyeri->addSingle("Numeric Rating Scale 4 : nyeri sedang");
		$nyeri->addSingle("Numeric Rating Scale 5 : nyeri sedang");
		$nyeri->addSingle("Numeric Rating Scale 6 : nyeri sedang");
		$nyeri->addSingle("Numeric Rating Scale 7 : nyeri berat");
		$nyeri->addSingle("Numeric Rating Scale 8 : nyeri berat");
		$nyeri->addSingle("Numeric Rating Scale 9 : nyeri berat");
		$nyeri->addSingle("Numeric Rating Scale 10: nyeri sangat berat");
		$nyeri->addSingle("Wong Baker Faces 0 : tidak merasakan sakit");
		$nyeri->addSingle("Wong Baker Faces 2 : sedikit rasa sakit");
		$nyeri->addSingle("Wong Baker Faces 4 : nyeri ringan");
		$nyeri->addSingle("Wong Baker Faces 6 : nyeri sedang");
		$nyeri->addSingle("Wong Baker Faces 8 : nyeri berat");
		$nyeri->addSingle("Wong Baker Faces 10 : nyeri sangat berat");
		$nyeri->addSingle("FLACCS 0 : tidak nyeri");
		$nyeri->addSingle("FLACCS 1 : nyeri ringan");
		$nyeri->addSingle("FLACCS 2 : nyeri ringan");
		$nyeri->addSingle("FLACCS 3 : nyeri ringan");
		$nyeri->addSingle("FLACCS 4 : nyeri sedang");
		$nyeri->addSingle("FLACCS 5 : nyeri sedang");
		$nyeri->addSingle("FLACCS 6 : nyeri sedang");
		$nyeri->addSingle("FLACCS 7 : nyeri hebat");
		$nyeri->addSingle("FLACCS 8 : nyeri hebat");
		$nyeri->addSingle("FLACCS 9 : nyeri hebat");
		$nyeri->addSingle("FLACCS 10: nyeri hebat");
		$nyeri->addSingle("NIPS 0 : Tidak ada nyeri");
		$nyeri->addSingle("NIPS 1 : Nyeri Ringan");
		$nyeri->addSingle("NIPS 2 : Nyeri Ringan");
		$nyeri->addSingle("NIPS 3 : Nyeri Sedang");
		$nyeri->addSingle("NIPS 4 : Nyeri Sedang");
		$nyeri->addSingle("NIPS 5 : Nyeri Berat");
		$nyeri->addSingle("NIPS 6 : Nyeri Berat");
		
		$nyeri_content=$nyeri->getContent();
		return $nyeri_content;
	}
	

}

?>