<?php
global $wpdb;

$query = "	
	CREATE TABLE IF NOT EXISTS `smis_fst_pesanan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tanggal` date NOT NULL,
  `nama_pasien` varchar(64) NOT NULL,
  `nrm_pasien` int(11) NOT NULL,
  `noreg_pasien` int(11) NOT NULL,
  `kelas` varchar(32) NOT NULL,
  `ruangan` varchar(32) NOT NULL,
  `no_lab` varchar(32) NOT NULL,
  `id_dokter` varchar(64) NOT NULL,
  `nama_dokter` varchar(32) NOT NULL,
  `id_petugas` varchar(32) NOT NULL,
  `nama_petugas` varchar(32) NOT NULL,
  `periksa` text NOT NULL,
  `nyeri_diam` varchar(128) NOT NULL,
  `nyeri_gerak` varchar(128) NOT NULL,
  `nyeri_tekan` varchar(128) NOT NULL,
  `hasil` text NOT NULL,
  `diskon` int(11) NOT NULL,
  `harga_asal` int(11) NOT NULL,
  `keterangan` varchar(32) NOT NULL,
  `harga` text NOT NULL,
  `biaya` int(11) NOT NULL,
  `pembagian` text NOT NULL,
  `selesai` int(11) NOT NULL,
  `uri` tinyint(1) NOT NULL,
  `jk` tinyint(1) NOT NULL,
  `umur` varchar(64) NOT NULL,
  `carabayar` varchar(32) NOT NULL,
  `id_marketing` int(11) NOT NULL,
  `marketing` varchar(64) NOT NULL,
  `diagnosa` varchar(128) NOT NULL,
  `barulama` tinyint(1) NOT NULL COMMENT 'baru adalah 0, lama 1',
  `shift` varchar(8) NOT NULL,
  `id_dokter_rhb` int(11) NOT NULL,
  `nama_dokter_rhb` varchar(32) NOT NULL,
  `biaya_dokter_rhb` int(11) NOT NULL,
  `asuhan_fisio` INT NOT NULL,
  `akunting` tinyint(1) NOT NULL,
  `prop` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB; ";
$wpdb->query ( $query );

$query="CREATE TABLE IF NOT EXISTS `smis_fst_layanan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(64) NOT NULL,
  `slug` varchar(32) NOT NULL,
  `keterangan` varchar(64) NOT NULL,
  `debet` varchar(64) NOT NULL,
  `kredit` varchar(64) NOT NULL,
  `prop` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB ";
$wpdb->query ( $query );

$query="
	CREATE TABLE IF NOT EXISTS `smis_fst_report` (
	  `id` int(11) NOT NULL AUTO_INCREMENT,
	  `urutan` int(11) NOT NULL,
	  `grup` varchar(32) NOT NULL,
	  `item` varchar(32) NOT NULL,
	  `jumlah` int(11) NOT NULL,
	  `prop` varchar(10) NOT NULL,
	  PRIMARY KEY (`id`),
	  UNIQUE KEY `item` (`item`)
	) ENGINE=InnoDB;";
$wpdb->query ( $query );

$query="CREATE TABLE `smis_fst_pertindakan` (
			  `id` int(11) NOT NULL AUTO_INCREMENT,
			  `nama` varchar(64) NOT NULL,
			  `kelas` varchar(32) NOT NULL,
			  `total` int(11) NOT NULL,
			  `harga_satuan` int(11) NOT NULL,
			  `harga_total` int(11) NOT NULL,
			  `prop` varchar(10) NOT NULL,
			  PRIMARY KEY (`id`),
			  UNIQUE KEY `nama` (`nama`,`kelas`,`harga_satuan`)
			) ENGINE=InnoDB;";
$wpdb->query($query);

require_once 'smis-libs-inventory/install.php';
$install=new InventoryInstallator($wpdb, "", "");
$install->extendInstall("fst");
$install->install();



?>
