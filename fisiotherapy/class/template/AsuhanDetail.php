<?php
require_once ("smis-framework/smis/template/ModulTemplate.php");
require_once ("smis-base/smis-include-service-consumer.php");
require_once 'smis-libs-hrd/EmployeeResponder.php';

 class AsuhanDetail extends ModulTemplate {

	private $uitable;
	private $dbres;
	private $dbtable;
	private $db;
	private $mode;
	
	public function __construct($db) {
		parent::__construct ();
		$this->db = $db;
		$header=array ("No.",'Waktu',"Nama Petugas","Nama Pasien","No. Reg",'NRM','Harga','Jumlah',"Total","Cara Bayar","Ruangan");
		$uitable = new Table ( $header, "", NULL, false );
		$uitable->setDelButtonEnable(false);
		$uitable->setEditButtonEnable(false);
		$uitable->setReloadButtonEnable(false);
		$uitable->setAddButtonEnable(false);
		$uitable->setName ( "laporan_asuhan" );
		$this->uitable=$uitable;
		
	}
	
	public function getCaraBayar(){
		$ser=new ServiceConsumer($this->db,"get_carabayar",NULL,"registration");
		$ser->execute();
		$content=$ser->getContent();
		$result=array();
		$result[]=array("name"=>"","value"=>"%","default"=>"1");
		foreach($content as $x){
			$result[]=array("name"=>$x['nama'],"value"=>$x['slug'],"default"=>"0");
		}
		return $result;
	}
	
	private function detail(Table &$uitable,ArrayAdapter &$adapter,DBTable &$dbtable){
		$adapter->addFixValue("Waktu","<total>Total</total>");
		$adapter->addSummary("Total","harga_total","money Rp.");
		$adapter->setUseNumber(true, "No.","back.");
		$adapter->add("Waktu", "waktu","date d M Y");
		$adapter->add("Nama Pasien", "nama_pasien");
		$adapter->add("No. Reg", "noreg_pasien");
		$adapter->add("NRM", "nrm_pasien");
		$adapter->add("Nama Petugas", "nama_petugas");
		$adapter->add("Harga", "harga","money Rp.");
		$adapter->add("Jumlah", "jumlah");
		$adapter->add("Total", "harga_total","money Rp.");
		$adapter->add("Cara Bayar", "carabayar","unslug");
        $adapter->add("Ruangan", "ruangan","unslug");
		
		if($_POST['command']=="excel"){
			$adapter->add("Waktu", "waktu","date d/m/Y");
			$adapter->add("Harga", "harga");
			$adapter->add("Total", "harga_total");
			$adapter->addFixValue("Waktu","Total");
			$adapter->addSummary("Total","harga");
			$adapter->addFixValue("Nama Pasien", "");
			$adapter->addFixValue("NRM", "");
			$adapter->addFixValue("Tindakan", "");
			$adapter->addFixValue("No. Reg", "");
			$adapter->addFixValue("NRM", "");
			$adapter->addFixValue("Jumlah", "");
			$adapter->addFixValue("No", "");
            $adapter->addFixValue("Ruangan", "");
		}
	}
	
	private function petugas_tgl_harga(Table &$uitable,ArrayAdapter &$adapter,DBTable &$dbtable){
		$qv="SELECT date(tanggal) as waktu,
					asuhan_fisio as harga,
					sum(1) as jumlah,
					sum(asuhan_fisio)  as harga_total,					
					'-' as noreg_pasien,
					'-' as nrm_pasien,
					nama_petugas as nama_petugas,
					'-' as nama_pasien,
					'-' as ruangan,
                    '-' as carabayar
					FROM smis_fst_pesanan
			";
			
		$qc="SELECT COUNT(*) as total FROM smis_fst_pesanan";
		$dbtable->setPreferredQuery(true,$qv,$qc);
		$dbtable->setMaskingCount(true," SELECT count(*) as total FROM ([query])X ");
		$dbtable->setGroupBy(true,"  nama_petugas,  date(tanggal), harga ");
		$dbtable->setUseWhereforView(true);
		$this->detail($uitable,$adapter,$dbtable);
	}
	
	private function petugas_tgl_harga_carabayar(Table &$uitable,ArrayAdapter &$adapter,DBTable &$dbtable){
		$qv="SELECT date(tanggal) as waktu,
					asuhan_fisio as harga,
					sum(1) as jumlah,
					sum(asuhan_fisio)  as harga_total,					
					'-' as noreg_pasien,
					'-' as nrm_pasien,
					'-' as ruangan,
                    '-' as nama_pasien,
					nama_petugas as nama_petugas,
					carabayar as carabayar
					FROM smis_fst_pesanan
			";
			
		$qc="SELECT COUNT(*) as total FROM smis_fst_pesanan";
		$dbtable->setPreferredQuery(true,$qv,$qc);
		$dbtable->setMaskingCount(true," SELECT count(*) as total FROM ([query])X ");
		$dbtable->setGroupBy(true,"  nama_petugas, date(tanggal), harga,  carabayar ");
		$dbtable->setUseWhereforView(true);
		$this->detail($uitable,$adapter,$dbtable);
	}
	
	private function petugas_harga_carabayar(Table &$uitable,ArrayAdapter &$adapter,DBTable &$dbtable){
		$qv="SELECT '-' as waktu,
					asuhan_fisio as harga,
					sum(1) as jumlah,
					sum(asuhan_fisio)  as harga_total,					
					'-' as noreg_pasien,
					'-' as nrm_pasien,
					'-' as ruangan,
                    '-' as nama_pasien,
					nama_petugas as nama_petugas,
					carabayar as carabayar
					FROM smis_fst_pesanan
			";
			
		$qc="SELECT COUNT(*) as total FROM smis_fst_pesanan";
		$dbtable->setPreferredQuery(true,$qv,$qc);
		$dbtable->setMaskingCount(true," SELECT count(*) as total FROM ([query])X ");
		$dbtable->setGroupBy(true," nama_petugas, harga, carabayar ");
		$dbtable->setUseWhereforView(true);
		$this->detail($uitable,$adapter,$dbtable);
		$adapter->add("Waktu", "waktu");

	}
	
	private function petugas_harga(Table &$uitable,ArrayAdapter &$adapter,DBTable &$dbtable){
		$qv="SELECT '-' as waktu,
					asuhan_fisio as harga,
					sum(1) as jumlah,
					sum(asuhan_fisio)  as harga_total,					
					'-' as noreg_pasien,
					'-' as nrm_pasien,
					'-' as nama_pasien,
					nama_petugas as nama_petugas,
					'-' as ruangan,
                    '-' as carabayar
					FROM smis_fst_pesanan
			";
			
		$qc="SELECT COUNT(*) as total FROM smis_fst_pesanan";
		$dbtable->setPreferredQuery(true,$qv,$qc);
		$dbtable->setMaskingCount(true," SELECT count(*) as total FROM ([query])X ");
		$dbtable->setGroupBy(true,"  nama_petugas, harga");
		$dbtable->setUseWhereforView(true);
		$this->detail($uitable,$adapter,$dbtable);
		$adapter->add("Waktu", "waktu");
	}
	
	private function petugas_carabayar(Table &$uitable,ArrayAdapter &$adapter,DBTable &$dbtable){
		$qv="SELECT '-' as waktu,
					'-' as harga,					
					sum(1) as jumlah,
					sum(asuhan_fisio)  as harga_total,						
					'-' as noreg_pasien,
					'-' as nrm_pasien,
					'-' as nama_pasien,
					nama_petugas as nama_petugas,
					'-' as ruangan,
                    carabayar as carabayar
					FROM smis_fst_pesanan
			";
			
		$qc="SELECT COUNT(*) as total FROM smis_fst_pesanan";
		$dbtable->setPreferredQuery(true,$qv,$qc);
		$dbtable->setMaskingCount(true," SELECT count(*) as total FROM ([query])X ");
		$dbtable->setGroupBy(true," nama_petugas, carabayar ");
		$dbtable->setUseWhereforView(true);
		$this->detail($uitable,$adapter,$dbtable);
		$adapter->add("Waktu", "waktu");
		

	}
	
	private function petugas(Table &$uitable,ArrayAdapter &$adapter,DBTable &$dbtable){
		$qv="SELECT '-' as waktu,
					'-' as harga,					
					sum(1) as jumlah,
					sum(asuhan_fisio)  as harga_total,						
					'-' as noreg_pasien,
					'-' as nrm_pasien,
					'-' as nama_pasien,
					nama_petugas as nama_petugas,
					'-' as ruangan,
                    '-' as carabayar
					FROM smis_fst_pesanan
			";
			
		$qc="SELECT COUNT(*) as total FROM smis_fst_pesanan";
		$dbtable->setPreferredQuery(true,$qv,$qc);
		$dbtable->setMaskingCount(true," SELECT count(*) as total FROM ([query])X ");
		$dbtable->setGroupBy(true," nama_petugas ");
		$dbtable->setUseWhereforView(true);
		$this->detail($uitable,$adapter,$dbtable);
		$adapter->add("Waktu", "waktu");
		

	}
	
	private function tgl_harga(Table &$uitable,ArrayAdapter &$adapter,DBTable &$dbtable){
		$qv="SELECT date(tanggal) as waktu,
					asuhan_fisio as harga,
					sum(1) as jumlah,
					sum(asuhan_fisio)  as harga_total,					
					'-' as noreg_pasien,
					'-' as nrm_pasien,
					'-' as nama_pasien,
					'-' as nama_petugas,
					'-' as ruangan,
                    '-' as carabayar
					FROM smis_fst_pesanan
			";
			
		$qc="SELECT COUNT(*) as total FROM smis_fst_pesanan";
		$dbtable->setPreferredQuery(true,$qv,$qc);
		$dbtable->setMaskingCount(true," SELECT count(*) as total FROM ([query])X ");
		$dbtable->setGroupBy(true," harga, date(tanggal) ");
		$dbtable->setUseWhereforView(true);
		$this->detail($uitable,$adapter,$dbtable);
		$adapter->add("Waktu", "waktu");
		

	}
	
	
	private function tgl_harga_carabayar(Table &$uitable,ArrayAdapter &$adapter,DBTable &$dbtable){
		$qv="SELECT date(tanggal) as waktu,
					asuhan_fisio as harga,
					sum(1) as jumlah,
					sum(asuhan_fisio)  as harga_total,						
					'-' as noreg_pasien,
					'-' as nrm_pasien,
					'-' as nama_pasien,
					nama_petugas as nama_petugas,
					'-' as ruangan,
                    carabayar as carabayar
					FROM smis_fst_pesanan
			";
			
		$qc="SELECT COUNT(*) as total FROM smis_fst_pesanan";
		$dbtable->setPreferredQuery(true,$qv,$qc);
		$dbtable->setMaskingCount(true," SELECT count(*) as total FROM ([query])X ");
		$dbtable->setGroupBy(true," harga, carabayar, date(tanggal) ");
		$dbtable->setUseWhereforView(true);
		$this->detail($uitable,$adapter,$dbtable);
		$adapter->add("Waktu", "waktu");
		

	}
	
	private function harga_carabayar(Table &$uitable,ArrayAdapter &$adapter,DBTable &$dbtable){
		$qv="SELECT '-' as waktu,
					asuhan_fisio as harga,
					sum(1) as jumlah,
					sum(asuhan_fisio)  as harga_total,					
					'-' as noreg_pasien,
					'-' as nrm_pasien,
					'-' as nama_pasien,
					nama_petugas as nama_petugas,
					'-' as ruangan,
                    carabayar as carabayar
					FROM smis_fst_pesanan
			";
			
		$qc="SELECT COUNT(*) as total FROM smis_fst_pesanan";
		$dbtable->setPreferredQuery(true,$qv,$qc);
		$dbtable->setMaskingCount(true," SELECT count(*) as total FROM ([query])X ");
		$dbtable->setGroupBy(true," harga, carabayar ");
		$dbtable->setUseWhereforView(true);
		$this->detail($uitable,$adapter,$dbtable);
		$adapter->add("Waktu", "waktu");
		

	}
	
	
	private function tgl(Table &$uitable,ArrayAdapter &$adapter,DBTable &$dbtable){
		$qv="SELECT date(tanggal) as waktu,
					asuhan_fisio as harga,
					sum(1) as jumlah,
					sum(asuhan_fisio)  as harga_total,					
					'-' as noreg_pasien,
					'-' as nrm_pasien,
					'-' as nama_pasien,
					'-' as nama_petugas,
					'-' as ruangan,
                    '-' as carabayar
					FROM smis_fst_pesanan
			";
			
		$qc="SELECT COUNT(*) as total FROM smis_fst_pesanan";
		$dbtable->setPreferredQuery(true,$qv,$qc);
		$dbtable->setMaskingCount(true," SELECT count(*) as total FROM ([query])X ");
		$dbtable->setGroupBy(true," date(tanggal) ");
		$dbtable->setUseWhereforView(true);
		$this->detail($uitable,$adapter,$dbtable);
		$adapter->add("Waktu", "waktu");
		

	}
	
	
	private function carabayar(Table &$uitable,ArrayAdapter &$adapter,DBTable &$dbtable){
		$qv="SELECT '-' as waktu,
					asuhan_fisio as harga,
					sum(1) as jumlah,
					sum(asuhan_fisio)  as harga_total,					
					'-' as noreg_pasien,
					'-' as nrm_pasien,
					'-' as nama_pasien,
					'-' as nama_petugas,
					'-' as ruangan,
                    carabayar as carabayar
					FROM smis_fst_pesanan
			";
			
		$qc="SELECT COUNT(*) as total FROM smis_fst_pesanan";
		$dbtable->setPreferredQuery(true,$qv,$qc);
		$dbtable->setMaskingCount(true," SELECT count(*) as total FROM ([query])X ");
		$dbtable->setGroupBy(true," carabayar ");
		$dbtable->setUseWhereforView(true);
		$this->detail($uitable,$adapter,$dbtable);
		$adapter->add("Waktu", "waktu");
	}
    
    private function ruangan(Table &$uitable,ArrayAdapter &$adapter,DBTable &$dbtable){
		$qv="SELECT '-' as waktu,
					asuhan_fisio as harga,
					sum(1) as jumlah,
					sum(asuhan_fisio)  as harga_total,					
					'-' as noreg_pasien,
					'-' as nrm_pasien,
					'-' as nama_pasien,
					'-' as nama_petugas,
					ruangan as ruangan,
                    '-' as carabayar
					FROM smis_fst_pesanan
			";
			
			
		$qc="SELECT COUNT(*) as total FROM smis_fst_pesanan";
		$dbtable->setPreferredQuery(true,$qv,$qc);
		$dbtable->setMaskingCount(true," SELECT count(*) as total FROM ([query])X ");
		$dbtable->setGroupBy(true," ruangan ");
		$dbtable->setUseWhereforView(true);
		$this->detail($uitable,$adapter,$dbtable);
		$adapter->add("Waktu", "waktu");
		

	}
    
    private function ruangan_carabayar(Table &$uitable,ArrayAdapter &$adapter,DBTable &$dbtable){
		$qv="SELECT '-' as waktu,
					asuhan_fisio as harga,
					sum(1) as jumlah,
					sum(asuhan_fisio)  as harga_total,					
					'-' as noreg_pasien,
					'-' as nrm_pasien,
					'-' as nama_pasien,
					'-' as nama_petugas,
					ruangan as ruangan,
                    carabayar as carabayar
					FROM smis_fst_pesanan
			";
			
			
		$qc="SELECT COUNT(*) as total FROM smis_fst_pesanan";
		$dbtable->setPreferredQuery(true,$qv,$qc);
		$dbtable->setMaskingCount(true," SELECT count(*) as total FROM ([query])X ");
		$dbtable->setGroupBy(true," ruangan, carabayar ");
		$dbtable->setUseWhereforView(true);
		$this->detail($uitable,$adapter,$dbtable);
		$adapter->add("Waktu", "waktu");
		

	}
	
	
	
	public function command($command) {		
		$this->dbtable = new DBTable ( $this->db, 'smis_fst_pesanan' );
		$this->dbtable->addCustomKriteria(NULL,"tanggal>='".$_POST['dari']."'");
		$this->dbtable->addCustomKriteria(NULL,"tanggal<'".$_POST['sampai']."'");
		$this->dbtable->addCustomKriteria("carabayar"," LIKE '".$_POST['carabayar']."'");
		$this->dbtable->addCustomKriteria(" asuhan_fisio "," > 0 ");
		if($_POST['nama_petugas']!=""){
			$this->dbtable->addCustomKriteria("nama_petugas"," = '".$_POST['nama_petugas']."'");
		}
		$this->dbtable->setOrder ( " tanggal ASC " );
		$adapter=new SummaryAdapter();
		
		if(isset($_POST['grup']) && $_POST['grup']=="petugas_tgl_harga_carabayar" ){
			$this->petugas_tgl_harga_carabayar($this->uitable,$adapter,$this->dbtable);
		}else if(isset($_POST['grup']) && $_POST['grup']=="petugas_harga_carabayar" ){
			$this->petugas_harga_carabayar($this->uitable,$adapter,$this->dbtable);
		}else if(isset($_POST['grup']) && $_POST['grup']=="petugas_harga" ){
			$this->petugas_harga($this->uitable,$adapter,$this->dbtable);
		}else if(isset($_POST['grup']) && $_POST['grup']=="petugas" ){
			$this->petugas($this->uitable,$adapter,$this->dbtable);
		}else if(isset($_POST['grup']) && $_POST['grup']=="tgl_harga" ){
			$this->tgl_harga($this->uitable,$adapter,$this->dbtable);
		}else if(isset($_POST['grup']) && $_POST['grup']=="tgl_harga_carabayar" ){
			$this->tgl_harga_carabayar($this->uitable,$adapter,$this->dbtable);
		}else if(isset($_POST['grup']) && $_POST['grup']=="harga_carabayar" ){
			$this->harga_carabayar($this->uitable,$adapter,$this->dbtable);
		}else if(isset($_POST['grup']) && $_POST['grup']=="carabayar" ){
			$this->carabayar($this->uitable,$adapter,$this->dbtable);
		}else if(isset($_POST['grup']) && $_POST['grup']=="tgl" ){
			$this->tgl($this->uitable,$adapter,$this->dbtable);
		}else if(isset($_POST['grup']) && $_POST['grup']=="ruangan" ){
			$this->ruangan($this->uitable,$adapter,$this->dbtable);
		}else if(isset($_POST['grup']) && $_POST['grup']=="ruangan_carabayar" ){
			$this->ruangan_carabayar($this->uitable,$adapter,$this->dbtable);
		}else{			
			$qv="SELECT date(tanggal) as waktu,
					asuhan_fisio as harga,
					1 as jumlah,
					asuhan_fisio as harga_total,
					noreg_pasien as noreg_pasien,
					nrm_pasien as nrm_pasien,
					nama_petugas as nama_petugas,
					nama_pasien as nama_pasien,
                    ruangan as ruangan,
					carabayar as carabayar
					FROM smis_fst_pesanan";			
			$qc="SELECT COUNT(*) as total FROM smis_fst_pesanan";
			$this->dbtable->setPreferredQuery(true,$qv,$qc);
			$this->dbtable->setUseWhereforView(true);			
			
			$this->detail($this->uitable,$adapter,$this->dbtable);
		}
		
		if(isset($_POST['orderby']) && $_POST['orderby']!="" ){
			$this->dbtable->setOrder($_POST['orderby'],true);
		}		
		$this->dbres = new DBResponder( $this->dbtable, $this->uitable, $adapter,$this->polislug );
		
		
		$data = $this->dbres->command ( $_POST ['command'] );
		if($data!=null){
			echo json_encode ( $data );
		}
	}
	
	
	
	public function superCommand($super_command) {
		$dkadapter = new SimpleAdapter ();
		$dkadapter->add ( "Jabatan", "nama_jabatan" );
		$dkadapter->add ( "Nama", "nama" );
		$dkadapter->add ( "NIP", "nip" );
		$header=array ('Nama','Jabatan',"NIP" );
		$dktable = new Table ( $header);
		$dktable->setName ( "petugas_laporan_asuhan" );
		$dktable->setModel ( Table::$SELECT );
		$petugas = new EmployeeResponder ( $this->db, $dktable, $dkadapter, "" );
		
		$super = new SuperCommand ();
		$super->addResponder ( "petugas_laporan_asuhan", $petugas );
		$init = $super->initialize ();
		if ($init != null) {
			echo $init;
			return;
		}
	}
	
	/* when it's star build */
	public function phpPreload() {
		
		$grup=new OptionBuilder();
		$grup->add("","","1");
		$grup->add("Grup By petugas/Tanggal/Harga/Carabayar","petugas_tgl_harga_carabayar");
		$grup->add("Grup By petugas/Harga/Carabayar","petugas_harga_carabayar");
		$grup->add("Grup By petugas/Harga","petugas_harga");
		$grup->add("Grup By petugas","petugas");		
		$grup->add("Grup By Tanggal","tgl");		
		$grup->add("Grup By Carabayar","carabayar");		
		$grup->add("Grup By Tanggal/Harga","tgl_harga");
		$grup->add("Grup By Tanggal/Harga/Carabayar","tgl_harga_carabayar");
		$grup->add("Grup By Harga/Carabayar","harga_carabayar");		
		$grup->add("Grup By Ruangan","ruangan");		
		$grup->add("Grup By Ruangan/Carabayar","ruangan_carabayar");		
		
		$order=new OptionBuilder();
		$order->add("","","1");
		$order->add("Order By Total - Jumlah"," harga DESC, jumlah DESC ");
		$order->add("Order By Jumlah - Total"," jumlah DESC, harga DESC ");
		$order->add("Order By petugas"," nama_petugas asC ");		
		$order->add("Order By Carabayar"," carabayar ASC ");
		$order->add("Order By Carabayar - Total - Jumlah"," carabayar ASC ,harga DESC, jumlah DESC");
		$order->add("Order By Carabayar - Total - Jumlah"," carabayar ASC ,jumlah DESC, harga DESC");
		$order->add("Order By petugas - Carabayar"," nama_petugas asC,  carabayar ASC ");
		$order->add("Order By petugas - Carabayar - Total - Jumlah"," nama_petugas asC, carabayar ASC ,harga DESC, jumlah DESC");
		$order->add("Order By petugas - Carabayar - Total - Jumlah"," nama_petugas asC, carabayar ASC ,jumlah DESC, harga DESC");		
		
		
		$carabayar=$this->getCaraBayar();
		$this->uitable->addModal("dari", "date", "Dari", "");
		$this->uitable->addModal("sampai", "date", "Sampai", "");
		$this->uitable->setFooterVisible(true);
		$this->uitable->addModal("carabayar", "select", "Cara Bayar",$carabayar);
		$this->uitable->addModal("nama_petugas", "chooser-laporan_asuhan-petugas_laporan_asuhan-Petugas", "Petugas","");
		$this->uitable->addModal("grup", "select", "Grup",$grup->getContent());
		$this->uitable->addModal("orderby", "select", "Order By",$order->getContent());
		$form=$this->uitable->getModal()->getForm();
		
		$btg=new ButtonGroup("");
		$btg->setMax(100,"");
		$btn=new Button("", "", "View");
		$btn->setAction("laporan_asuhan.view()");
		$btn->setClass("btn btn-primary");
		$btn->setIcon(" fa fa-refresh");
		$btn->setIsButton(Button::$ICONIC_TEXT);
		$btg->addButton($btn);
		
		$btn=new Button("", "", "Print");
		$btn->setAction("laporan_asuhan.print()");
		$btn->setClass("btn btn-primary");
		$btn->setIcon(" fa fa-print");
		$btn->setIsButton(Button::$ICONIC_TEXT);
		$btg->addButton($btn);
		
		$btn=new Button("", "", "Excel");
		$btn->setAction("laporan_asuhan.excel()");
		$btn->setClass("btn btn-primary");
		$btn->setIcon(" fa fa-file-excel-o");
		$btn->setIsButton(Button::$ICONIC_TEXT);
		$btg->addButton($btn);
		
				
		$form->addElement("", $btg);
		
		
		echo $form->getHtml();
		echo $this->uitable->getHtml ();
		echo addJS ( "framework/bootstrap/js/bootstrap-datepicker.js" );
		echo addJS ( "framework/smis/js/table_action.js" );
		echo addCSS ( "framework/bootstrap/css/datepicker.css" );
	}
	public function jsPreLoad() {
		?>
		<script type="text/javascript">
		var laporan_asuhan;		
		var petugas_laporan_asuhan; 		
		$(document).ready(function(){
			$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
			$('.mydate').datepicker();
			var column=new Array("");
			laporan_asuhan=new TableAction("laporan_asuhan","fisiotherapy","laporan_asuhan",column);
			laporan_asuhan.addViewData=function(save_data){
				save_data['command']="list";
				save_data['dari']=$("#laporan_asuhan_dari").val();
				save_data['sampai']=$("#laporan_asuhan_sampai").val();
				save_data['carabayar']=$("#laporan_asuhan_carabayar").val();
				save_data['nama_petugas']=$("#laporan_asuhan_nama_petugas").val();
				save_data['grup']=$("#laporan_asuhan_grup").val();
				save_data['orderby']=$("#laporan_asuhan_orderby").val();
				return save_data;
			};	
			
			
			petugas_laporan_asuhan=new TableAction("petugas_laporan_asuhan","fisiotherapy","laporan_asuhan",new Array());
			petugas_laporan_asuhan.setSuperCommand("petugas_laporan_asuhan");
			petugas_laporan_asuhan.selected=function(json){
				var nama=json.nama;
				$("#laporan_asuhan_nama_petugas").val(nama);
			};
			
		});
		</script>
<?php
	}
}?>
