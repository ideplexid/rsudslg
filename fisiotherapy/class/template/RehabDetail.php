<?php
require_once ("smis-framework/smis/template/ModulTemplate.php");
require_once ("smis-base/smis-include-service-consumer.php");
require_once 'smis-libs-hrd/EmployeeResponder.php';

 class RehabDetail extends ModulTemplate {

	private $uitable;
	private $dbres;
	private $dbtable;
	private $db;
	private $mode;
	
	public function __construct($db) {
		parent::__construct ();
		$this->db = $db;
		$header=array ("No.",'Waktu',"Dokter Rehab","Nama Pasien","No. Reg",'NRM','Harga','Jumlah',"Total","Cara Bayar");
		$uitable = new Table ( $header, "", NULL, false );
		$uitable->setDelButtonEnable(false);
		$uitable->setEditButtonEnable(false);
		$uitable->setReloadButtonEnable(false);
		$uitable->setAddButtonEnable(false);
		$uitable->setName ( "laporan_dokter_rehab" );
		$this->uitable=$uitable;
		
	}
	
	public function getCaraBayar(){
		$ser=new ServiceConsumer($this->db,"get_carabayar",NULL,"registration");
		$ser->execute();
		$content=$ser->getContent();
		$result=array();
		$result[]=array("name"=>"","value"=>"%","default"=>"1");
		foreach($content as $x){
			$result[]=array("name"=>$x['nama'],"value"=>$x['slug'],"default"=>"0");
		}
		return $result;
	}
	
	private function detail(Table &$uitable,ArrayAdapter &$adapter,DBTable &$dbtable){
		$adapter->addFixValue("Waktu","<total>Total</total>");
		$adapter->addSummary("Total","harga_total","money Rp.");
		$adapter->setUseNumber(true, "No.","back.");
		$adapter->add("Waktu", "waktu","date d M Y");
		$adapter->add("Nama Pasien", "nama_pasien");
		$adapter->add("No. Reg", "noreg_pasien");
		$adapter->add("NRM", "nrm_pasien");
		$adapter->add("Dokter Rehab", "nama_dokter");
		$adapter->add("Harga", "harga","money Rp.");
		$adapter->add("Jumlah", "jumlah");
		$adapter->add("Total", "harga_total","money Rp.");
		$adapter->add("Cara Bayar", "carabayar","unslug");
		
		if($_POST['command']=="excel"){
			$adapter->add("Waktu", "waktu","date d/m/Y");
			$adapter->add("Harga", "harga");
			$adapter->add("Total", "harga_total");
			$adapter->addFixValue("Waktu","Total");
			$adapter->addSummary("Total","harga");
			$adapter->addFixValue("Nama Pasien", "");
			$adapter->addFixValue("NRM", "");
			$adapter->addFixValue("Tindakan", "");
			$adapter->addFixValue("No. Reg", "");
			$adapter->addFixValue("NRM", "");
			$adapter->addFixValue("Jumlah", "");
			$adapter->addFixValue("No", "");
		}
	}
	
	private function dokter_tgl_harga(Table &$uitable,ArrayAdapter &$adapter,DBTable &$dbtable){
		$qv="SELECT date(tanggal) as waktu,
					biaya_dokter_rhb as harga,
					sum(1) as jumlah,
					sum(biaya_dokter_rhb)  as harga_total,					
					'-' as noreg_pasien,
					'-' as nrm_pasien,
					nama_dokter_rhb as nama_dokter,
					'-' as nama_pasien,
					'-' as carabayar
					FROM smis_fst_pesanan
			";
			
		$qc="SELECT COUNT(*) as total FROM smis_fst_pesanan";
		$dbtable->setPreferredQuery(true,$qv,$qc);
		$dbtable->setMaskingCount(true," SELECT count(*) as total FROM ([query])X ");
		$dbtable->setGroupBy(true,"  nama_dokter,  date(tanggal), harga ");
		$dbtable->setUseWhereforView(true);
		$this->detail($uitable,$adapter,$dbtable);
	}
	
	private function dokter_tgl_harga_carabayar(Table &$uitable,ArrayAdapter &$adapter,DBTable &$dbtable){
		$qv="SELECT date(tanggal) as waktu,
					biaya_dokter_rhb as harga,
					sum(1) as jumlah,
					sum(biaya_dokter_rhb)  as harga_total,					
					'-' as noreg_pasien,
					'-' as nrm_pasien,
					'-' as nama_pasien,
					nama_dokter_rhb as nama_dokter,
					carabayar as carabayar
					FROM smis_fst_pesanan
			";
			
		$qc="SELECT COUNT(*) as total FROM smis_fst_pesanan";
		$dbtable->setPreferredQuery(true,$qv,$qc);
		$dbtable->setMaskingCount(true," SELECT count(*) as total FROM ([query])X ");
		$dbtable->setGroupBy(true,"  nama_dokter, date(tanggal), harga,  carabayar ");
		$dbtable->setUseWhereforView(true);
		$this->detail($uitable,$adapter,$dbtable);
	}
	
	private function dokter_harga_carabayar(Table &$uitable,ArrayAdapter &$adapter,DBTable &$dbtable){
		$qv="SELECT '-' as waktu,
					biaya_dokter_rhb as harga,
					sum(1) as jumlah,
					sum(biaya_dokter_rhb)  as harga_total,					
					'-' as noreg_pasien,
					'-' as nrm_pasien,
					'-' as nama_pasien,
					nama_dokter_rhb as nama_dokter,
					carabayar as carabayar
					FROM smis_fst_pesanan
			";
			
		$qc="SELECT COUNT(*) as total FROM smis_fst_pesanan";
		$dbtable->setPreferredQuery(true,$qv,$qc);
		$dbtable->setMaskingCount(true," SELECT count(*) as total FROM ([query])X ");
		$dbtable->setGroupBy(true," nama_dokter, harga, carabayar ");
		$dbtable->setUseWhereforView(true);
		$this->detail($uitable,$adapter,$dbtable);
		$adapter->add("Waktu", "waktu");

	}
	
	private function dokter_harga(Table &$uitable,ArrayAdapter &$adapter,DBTable &$dbtable){
		$qv="SELECT '-' as waktu,
					biaya_dokter_rhb as harga,
					sum(1) as jumlah,
					sum(biaya_dokter_rhb)  as harga_total,					
					'-' as noreg_pasien,
					'-' as nrm_pasien,
					'-' as nama_pasien,
					nama_dokter_rhb as nama_dokter,
					'-' as carabayar
					FROM smis_fst_pesanan
			";
			
		$qc="SELECT COUNT(*) as total FROM smis_fst_pesanan";
		$dbtable->setPreferredQuery(true,$qv,$qc);
		$dbtable->setMaskingCount(true," SELECT count(*) as total FROM ([query])X ");
		$dbtable->setGroupBy(true,"  nama_dokter, harga");
		$dbtable->setUseWhereforView(true);
		$this->detail($uitable,$adapter,$dbtable);
		$adapter->add("Waktu", "waktu");
	}
	
	private function dokter_carabayar(Table &$uitable,ArrayAdapter &$adapter,DBTable &$dbtable){
		$qv="SELECT '-' as waktu,
					'-' as harga,					
					sum(1) as jumlah,
					sum(biaya_dokter_rhb)  as harga_total,						
					'-' as noreg_pasien,
					'-' as nrm_pasien,
					'-' as nama_pasien,
					nama_dokter_rhb as nama_dokter,
					carabayar as carabayar
					FROM smis_fst_pesanan
			";
			
		$qc="SELECT COUNT(*) as total FROM smis_fst_pesanan";
		$dbtable->setPreferredQuery(true,$qv,$qc);
		$dbtable->setMaskingCount(true," SELECT count(*) as total FROM ([query])X ");
		$dbtable->setGroupBy(true," nama_dokter, carabayar ");
		$dbtable->setUseWhereforView(true);
		$this->detail($uitable,$adapter,$dbtable);
		$adapter->add("Waktu", "waktu");
		

	}
	
	private function dokter(Table &$uitable,ArrayAdapter &$adapter,DBTable &$dbtable){
		$qv="SELECT '-' as waktu,
					'-' as harga,					
					sum(1) as jumlah,
					sum(biaya_dokter_rhb)  as harga_total,						
					'-' as noreg_pasien,
					'-' as nrm_pasien,
					'-' as nama_pasien,
					nama_dokter_rhb as nama_dokter,
					'-' as carabayar
					FROM smis_fst_pesanan
			";
			
		$qc="SELECT COUNT(*) as total FROM smis_fst_pesanan";
		$dbtable->setPreferredQuery(true,$qv,$qc);
		$dbtable->setMaskingCount(true," SELECT count(*) as total FROM ([query])X ");
		$dbtable->setGroupBy(true," nama_dokter ");
		$dbtable->setUseWhereforView(true);
		$this->detail($uitable,$adapter,$dbtable);
		$adapter->add("Waktu", "waktu");
		

	}
	
	private function tgl_harga(Table &$uitable,ArrayAdapter &$adapter,DBTable &$dbtable){
		$qv="SELECT date(tanggal) as waktu,
					biaya_dokter_rhb as harga,
					sum(1) as jumlah,
					sum(biaya_dokter_rhb)  as harga_total,					
					'-' as noreg_pasien,
					'-' as nrm_pasien,
					'-' as nama_pasien,
					'-' as nama_dokter,
					'-' as carabayar
					FROM smis_fst_pesanan
			";
			
		$qc="SELECT COUNT(*) as total FROM smis_fst_pesanan";
		$dbtable->setPreferredQuery(true,$qv,$qc);
		$dbtable->setMaskingCount(true," SELECT count(*) as total FROM ([query])X ");
		$dbtable->setGroupBy(true," harga, date(tanggal) ");
		$dbtable->setUseWhereforView(true);
		$this->detail($uitable,$adapter,$dbtable);
		$adapter->add("Waktu", "waktu");
		

	}
	
	
	private function tgl_harga_carabayar(Table &$uitable,ArrayAdapter &$adapter,DBTable &$dbtable){
		$qv="SELECT date(tanggal) as waktu,
					biaya_dokter_rhb as harga,
					sum(1) as jumlah,
					sum(biaya_dokter_rhb)  as harga_total,						
					'-' as noreg_pasien,
					'-' as nrm_pasien,
					'-' as nama_pasien,
					nama_dokter_rhb as nama_dokter,
					carabayar as carabayar
					FROM smis_fst_pesanan
			";
			
		$qc="SELECT COUNT(*) as total FROM smis_fst_pesanan";
		$dbtable->setPreferredQuery(true,$qv,$qc);
		$dbtable->setMaskingCount(true," SELECT count(*) as total FROM ([query])X ");
		$dbtable->setGroupBy(true," harga, carabayar, date(tanggal) ");
		$dbtable->setUseWhereforView(true);
		$this->detail($uitable,$adapter,$dbtable);
		$adapter->add("Waktu", "waktu");
		

	}
	
	private function harga_carabayar(Table &$uitable,ArrayAdapter &$adapter,DBTable &$dbtable){
		$qv="SELECT '-' as waktu,
					biaya_dokter_rhb as harga,
					sum(1) as jumlah,
					sum(biaya_dokter_rhb)  as harga_total,					
					'-' as noreg_pasien,
					'-' as nrm_pasien,
					'-' as nama_pasien,
					nama_dokter_rhb as nama_dokter,
					carabayar as carabayar
					FROM smis_fst_pesanan
			";
			
		$qc="SELECT COUNT(*) as total FROM smis_fst_pesanan";
		$dbtable->setPreferredQuery(true,$qv,$qc);
		$dbtable->setMaskingCount(true," SELECT count(*) as total FROM ([query])X ");
		$dbtable->setGroupBy(true," harga, carabayar ");
		$dbtable->setUseWhereforView(true);
		$this->detail($uitable,$adapter,$dbtable);
		$adapter->add("Waktu", "waktu");
		

	}
	
	
	private function tgl(Table &$uitable,ArrayAdapter &$adapter,DBTable &$dbtable){
		$qv="SELECT date(tanggal) as waktu,
					biaya_dokter_rhb as harga,
					sum(1) as jumlah,
					sum(biaya_dokter_rhb)  as harga_total,					
					'-' as noreg_pasien,
					'-' as nrm_pasien,
					'-' as nama_pasien,
					'-' as nama_dokter,
					'-' as carabayar
					FROM smis_fst_pesanan
			";
			
		$qc="SELECT COUNT(*) as total FROM smis_fst_pesanan";
		$dbtable->setPreferredQuery(true,$qv,$qc);
		$dbtable->setMaskingCount(true," SELECT count(*) as total FROM ([query])X ");
		$dbtable->setGroupBy(true," date(tanggal) ");
		$dbtable->setUseWhereforView(true);
		$this->detail($uitable,$adapter,$dbtable);
		$adapter->add("Waktu", "waktu");
		

	}
	
	
	private function carabayar(Table &$uitable,ArrayAdapter &$adapter,DBTable &$dbtable){
		$qv="SELECT '-' as waktu,
					biaya_dokter_rhb as harga,
					sum(1) as jumlah,
					sum(biaya_dokter_rhb)  as harga_total,					
					'-' as noreg_pasien,
					'-' as nrm_pasien,
					'-' as nama_pasien,
					'-' as nama_dokter,
					carabayar as carabayar
					FROM smis_fst_pesanan
			";
			
		$qc="SELECT COUNT(*) as total FROM smis_fst_pesanan";
		$dbtable->setPreferredQuery(true,$qv,$qc);
		$dbtable->setMaskingCount(true," SELECT count(*) as total FROM ([query])X ");
		$dbtable->setGroupBy(true," carabayar ");
		$dbtable->setUseWhereforView(true);
		$this->detail($uitable,$adapter,$dbtable);
		$adapter->add("Waktu", "waktu");
	}
	
	
	
	public function command($command) {		
		$this->dbtable = new DBTable ( $this->db, 'smis_fst_pesanan' );
		$this->dbtable->addCustomKriteria(NULL,"tanggal>='".$_POST['dari']."'");
		$this->dbtable->addCustomKriteria(NULL,"tanggal<'".$_POST['sampai']."'");
		$this->dbtable->addCustomKriteria("carabayar"," LIKE '".$_POST['carabayar']."'");
		$this->dbtable->addCustomKriteria(" biaya_dokter_rhb "," > 0 ");
		if($_POST['nama_dokter']!=""){
			$this->dbtable->addCustomKriteria("nama_dokter_rhb"," = '".$_POST['nama_dokter']."'");
		}
		$this->dbtable->setOrder ( " tanggal ASC " );
		$adapter=new SummaryAdapter();
		
		if(isset($_POST['grup']) && $_POST['grup']=="dokter_tgl_harga_carabayar" ){
			$this->dokter_tgl_harga_carabayar($this->uitable,$adapter,$this->dbtable);
		}else if(isset($_POST['grup']) && $_POST['grup']=="dokter_harga_carabayar" ){
			$this->dokter_harga_carabayar($this->uitable,$adapter,$this->dbtable);
		}else if(isset($_POST['grup']) && $_POST['grup']=="dokter_harga" ){
			$this->dokter_harga($this->uitable,$adapter,$this->dbtable);
		}else if(isset($_POST['grup']) && $_POST['grup']=="dokter" ){
			$this->dokter($this->uitable,$adapter,$this->dbtable);
		}else if(isset($_POST['grup']) && $_POST['grup']=="tgl_harga" ){
			$this->tgl_harga($this->uitable,$adapter,$this->dbtable);
		}else if(isset($_POST['grup']) && $_POST['grup']=="tgl_harga_carabayar" ){
			$this->tgl_harga_carabayar($this->uitable,$adapter,$this->dbtable);
		}else if(isset($_POST['grup']) && $_POST['grup']=="harga_carabayar" ){
			$this->harga_carabayar($this->uitable,$adapter,$this->dbtable);
		}else if(isset($_POST['grup']) && $_POST['grup']=="carabayar" ){
			$this->carabayar($this->uitable,$adapter,$this->dbtable);
		}else if(isset($_POST['grup']) && $_POST['grup']=="tgl" ){
			$this->tgl($this->uitable,$adapter,$this->dbtable);
		}else{			
			$qv="SELECT date(tanggal) as waktu,
					biaya_dokter_rhb as harga,
					1 as jumlah,
					biaya_dokter_rhb as harga_total,
					noreg_pasien as noreg_pasien,
					nrm_pasien as nrm_pasien,
					nama_dokter_rhb as nama_dokter,
					nama_pasien as nama_pasien,
					carabayar as carabayar
					FROM smis_fst_pesanan";			
			$qc="SELECT COUNT(*) as total FROM smis_fst_pesanan";
			$this->dbtable->setPreferredQuery(true,$qv,$qc);
			$this->dbtable->setUseWhereforView(true);			
			
			$this->detail($this->uitable,$adapter,$this->dbtable);
		}
		
		if(isset($_POST['orderby']) && $_POST['orderby']!="" ){
			$this->dbtable->setOrder($_POST['orderby'],true);
		}		
		$this->dbres = new DBResponder( $this->dbtable, $this->uitable, $adapter,$this->polislug );
		
		
		$data = $this->dbres->command ( $_POST ['command'] );
		if($data!=null){
			echo json_encode ( $data );
		}
	}
	
	
	
	public function superCommand($super_command) {
		$dkadapter = new SimpleAdapter ();
		$dkadapter->add ( "Jabatan", "nama_jabatan" );
		$dkadapter->add ( "Nama", "nama" );
		$dkadapter->add ( "NIP", "nip" );
		$header=array ('Nama','Jabatan',"NIP" );
		$dktable = new Table ( $header);
		$dktable->setName ( "dokter_laporan_dokter_rehab" );
		$dktable->setModel ( Table::$SELECT );
		$dokter = new EmployeeResponder ( $this->db, $dktable, $dkadapter, "dokter" );
		
		$super = new SuperCommand ();
		$super->addResponder ( "dokter_laporan_dokter_rehab", $dokter );
		$init = $super->initialize ();
		if ($init != null) {
			echo $init;
			return;
		}
	}
	
	/* when it's star build */
	public function phpPreload() {
		
		$grup=new OptionBuilder();
		$grup->add("","","1");
		$grup->add("Grup By Dokter/Tanggal/Harga/Carabayar","dokter_tgl_harga_carabayar");
		$grup->add("Grup By Dokter/Harga/Carabayar","dokter_harga_carabayar");
		$grup->add("Grup By Dokter/Harga","dokter_harga");
		$grup->add("Grup By Dokter","dokter");		
		$grup->add("Grup By Tanggal","tgl");		
		$grup->add("Grup By Carabayar","carabayar");		
		$grup->add("Grup By Tanggal/Harga","tgl_harga");
		$grup->add("Grup By Tanggal/Harga/Carabayar","tgl_harga_carabayar");
		$grup->add("Grup By Harga/Carabayar","harga_carabayar");		
		
		$order=new OptionBuilder();
		$order->add("","","1");
		$order->add("Order By Total - Jumlah"," harga DESC, jumlah DESC ");
		$order->add("Order By Jumlah - Total"," jumlah DESC, harga DESC ");
		$order->add("Order By Dokter"," nama_dokter_rhb asC ");		
		$order->add("Order By Carabayar"," carabayar ASC ");
		$order->add("Order By Carabayar - Total - Jumlah"," carabayar ASC ,harga DESC, jumlah DESC");
		$order->add("Order By Carabayar - Total - Jumlah"," carabayar ASC ,jumlah DESC, harga DESC");
		$order->add("Order By Dokter - Carabayar"," nama_dokter_rhb asC,  carabayar ASC ");
		$order->add("Order By Dokter - Carabayar - Total - Jumlah"," nama_dokter_rhb asC, carabayar ASC ,harga DESC, jumlah DESC");
		$order->add("Order By Dokter - Carabayar - Total - Jumlah"," nama_dokter_rhb asC, carabayar ASC ,jumlah DESC, harga DESC");		
		
		
		$carabayar=$this->getCaraBayar();
		$this->uitable->addModal("dari", "date", "Dari", "");
		$this->uitable->addModal("sampai", "date", "Sampai", "");
		$this->uitable->setFooterVisible(true);
		$this->uitable->addModal("carabayar", "select", "Cara Bayar",$carabayar);
		$this->uitable->addModal("nama_dokter", "chooser-laporan_dokter_rehab-dokter_laporan_dokter_rehab-Dokter", "Dokter","");
		$this->uitable->addModal("grup", "select", "Grup",$grup->getContent());
		$this->uitable->addModal("orderby", "select", "Order By",$order->getContent());
		$form=$this->uitable->getModal()->getForm();
		
		$btg=new ButtonGroup("");
		$btg->setMax(100,"");
		$btn=new Button("", "", "View");
		$btn->setAction("laporan_dokter_rehab.view()");
		$btn->setClass("btn btn-primary");
		$btn->setIcon(" fa fa-refresh");
		$btn->setIsButton(Button::$ICONIC_TEXT);
		$btg->addButton($btn);
		
		$btn=new Button("", "", "Print");
		$btn->setAction("laporan_dokter_rehab.print()");
		$btn->setClass("btn btn-primary");
		$btn->setIcon(" fa fa-print");
		$btn->setIsButton(Button::$ICONIC_TEXT);
		$btg->addButton($btn);
		
		$btn=new Button("", "", "Excel");
		$btn->setAction("laporan_dokter_rehab.excel()");
		$btn->setClass("btn btn-primary");
		$btn->setIcon(" fa fa-file-excel-o");
		$btn->setIsButton(Button::$ICONIC_TEXT);
		$btg->addButton($btn);
		
				
		$form->addElement("", $btg);
		
		
		echo $form->getHtml();
		echo $this->uitable->getHtml ();
		echo addJS ( "framework/bootstrap/js/bootstrap-datepicker.js" );
		echo addJS ( "framework/smis/js/table_action.js" );
		echo addCSS ( "framework/bootstrap/css/datepicker.css" );
	}
	public function jsPreLoad() {
		?>
		<script type="text/javascript">
		var laporan_dokter_rehab;		
		var dokter_laporan_dokter_rehab; 		
		$(document).ready(function(){
			$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
			$('.mydate').datepicker();
			var column=new Array("");
			laporan_dokter_rehab=new TableAction("laporan_dokter_rehab","fisiotherapy","laporan_dokter_rehab",column);
			laporan_dokter_rehab.addViewData=function(save_data){
				save_data['command']="list";
				save_data['dari']=$("#laporan_dokter_rehab_dari").val();
				save_data['sampai']=$("#laporan_dokter_rehab_sampai").val();
				save_data['carabayar']=$("#laporan_dokter_rehab_carabayar").val();
				save_data['nama_dokter']=$("#laporan_dokter_rehab_nama_dokter").val();
				save_data['grup']=$("#laporan_dokter_rehab_grup").val();
				save_data['orderby']=$("#laporan_dokter_rehab_orderby").val();
				return save_data;
			};	
			
			
			dokter_laporan_dokter_rehab=new TableAction("dokter_laporan_dokter_rehab","fisiotherapy","laporan_dokter_rehab",new Array());
			dokter_laporan_dokter_rehab.setSuperCommand("dokter_laporan_dokter_rehab");
			dokter_laporan_dokter_rehab.selected=function(json){
				var nama=json.nama;
				$("#laporan_dokter_rehab_nama_dokter").val(nama);
			};
			
		});
		</script>
<?php
	}
}?>
