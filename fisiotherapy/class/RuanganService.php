<?php
class RuanganService extends ServiceConsumer {
	public function __construct($db) {
		parent::__construct ( $db, "get_entity", "push_antrian" );
	}
	public function proceedResult() {
		$content = array ();
		$option = array ();
		$option ['value'] = "Pendaftaran";
		$option ['name'] = "PENDAFTARAN" ;
		$option ['default']="Pendaftaran"==$this->ruangan?"1":"0";
		$content [] = $option;
		$result = json_decode ( $this->result, true );
		foreach ( $result as $autonomous ) {
			foreach ( $autonomous as $entity ) {
				$option = array ();
				$option ['value'] = $entity;
				$option ['name'] = ArrayAdapter::format("unslug", $entity ) ;
				$number = count ( $content );
				$content [$number] = $option;
			}
		}
		return $content;
	}
}
?>