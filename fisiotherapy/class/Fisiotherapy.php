<?php
require_once 'smis-framework/smis/template/ModulTemplate.php';
require_once ("smis-base/smis-include-service-consumer.php");
require_once "smis-libs-hrd/EmployeeResponder.php";
require_once 'fisiotherapy/class/responder/FisiotherapyResponder.php';
require_once 'fisiotherapy/class/RuanganService.php';
require_once 'smis-libs-manajemen/ProvitSharingService.php';
require_once 'fisiotherapy/class/TarifFisiotherapy.php';
require_once 'fisiotherapy/class/table/FisiotherapyTable.php';

class FisiotherapyTemplate extends ModulTemplate {
	protected $db;
	protected $mode;
	protected $polislug;
	protected $page;
	protected $protoslug;
	protected $protoname;
	protected $protoimplement;
	protected $action;
	protected $dbtable;
	protected $uitable;
	protected $noreg_pasien;
	protected $nama_pasien;
	protected $nrm_pasien;
	protected $list_pesan;
	protected $list_hasil;
	protected $kelas;
	protected $uri;
	protected $jk;
	protected $umur;
	protected $carabayar;
	protected $nyeri_list;
	protected $is_stand_alone;
	protected $is_keep;
	public static $MODE_DAFTAR = "pendaftaran";
	public static $MODE_PERIKSA = "pemeriksaan";
	public static $MODE_ARCHIVE = "archive";
	public function __construct($db, $mode, $polislug = "all", $noreg = "", $nrm = "", $nama = "", $page = "fisiotherapy", $action = "pemeriksaan", $protoslug = "", $protoname = "", $protoimplement = "",$kelas="rawat_jalan",$jk="-1") {
		$this->db = $db;
		$this->mode = $mode;
		$this->noreg_pasien = $noreg;
		$this->nama_pasien = $nama;
		$this->nrm_pasien = $nrm;
		$this->polislug = $polislug;
		$this->dbtable = new DBTable ( $db, "smis_fst_pesanan" );
		$this->is_stand_alone=getSettings($db, "fisiotherapy-sistem-model", "Stand Alone")=="Stand Alone";
		$this->is_keep=getSettings($db, "fisiotherapy-add-button", "0")=="1";
		$this->page = $page;
		$this->protoslug = $protoslug;
		$this->protoimplement = $protoimplement;
		$this->protoname = $protoname;
		$this->action = $action;
		$this->kelas=$kelas;
		$this->jk=$jk;
		$this->carabayar="No Reg Not Active";
		
		if ($noreg != "") {
			$data_post = array ("command" => "edit","id" => $noreg );
			$service = new ServiceConsumer($this->db, "get_registered", $data_post);
			$service->execute ();
			$data = $service->getContent ();
			$this->carabayar = $data ['carabayar'];
			$this->uri=$data['uri'];
			$this->umur = $data ['umur'];
			if($this->jk=="-1")
				$this->jk=$data['kelamin'];
		}
		
		if ($polislug != "all") {
			$this->dbtable->addCustomKriteria ( "ruangan", "='" . $polislug . "'" );
		}
		
		if ($noreg != "") {
			$this->dbtable->addCustomKriteria ( "noreg_pasien", "='" . $noreg . "'" );
		}
		
		$header=array ('Tanggal','Pasien',"NRM","No. Reg",'Kelas',"Harga Asal","Diskon (%)","Biaya Rehab","Asuhan","Biaya Total","Ruangan");$name=ucfirst ( $this->mode ) . " Fisiotherapy " . ($this->polislug == "all" ? "" : ucfirst ( $this->protoname ));
		$this->uitable = new FisiotherapyTable( $header, $name, NULL, true );
		$this->uitable->setPrintButtonEnable(false);
		$this->uitable->setReloadButtonEnable(false);
		$this->uitable->setPrintElementButtonEnable(true);
		$this->uitable->setName ( $action );
		if ($this->mode == self::$MODE_DAFTAR) {
			$this->uitable->setDelButtonEnable ( false );
		} else if($this->mode == self::$MODE_PERIKSA){
			$this->uitable->setAddButtonEnable ( $this->is_stand_alone || $this->is_keep);
			$btn=new Button("", "", "Selesai");
			$btn->setIsButton(Button::$ICONIC);
			$btn->setClass("btn-primary");
			$btn->setIcon(" fa fa-check");
			$this->uitable->addContentButton("selesai", $btn);
			$this->dbtable->addCustomKriteria ( "selesai", "='0'" );
			
			$btn=new Button("", "", "Register");
			$btn->setIsButton(Button::$ICONIC_TEXT);
			$btn->setClass("btn-success");
			$btn->setIcon(" fa fa-forward");
			$this->uitable->addContentButton("show_register", $btn);
            

            $btn1 = new Button("", "", "Rehabilitasi Medik");
            $btn1 ->setClass("btn-inverse");
            $btn1 ->setIcon("fa fa-file");
            $btn1 ->setIsButton(Button::$ICONIC_TEXT);
            $this->uitable->addContentButton("rehab_medik", $btn1);
            
            $btn2 = new Button("", "", "Pelayanan Khusus");
            $btn2 ->setClass("btn-inverse");
            $btn2 ->setIcon("fa fa-file");
            $btn2 ->setIsButton(Button::$ICONIC_TEXT);
            $this->uitable->addContentButton("pelayanan_khusus", $btn2);

            $btn3 = new Button("", "", "Jenis Pelayanan");
            $btn3 ->setClass("btn-inverse");
            $btn3 ->setIcon("fa fa-file");
            $btn3 ->setIsButton(Button::$ICONIC_TEXT);
            $this->uitable->addContentButton("lap_rl52", $btn3);

		} else {
			$btn=new Button("", "", "");
			$btn->setIsButton(Button::$ICONIC);
			$btn->setClass("btn-primary");
			$btn->setIcon(" fa fa-eye");
			$this->uitable->addContentButton("edit", $btn);
			$this->uitable->setEditButtonEnable ( false);
			$this->uitable->setAddButtonEnable ( false);
			$this->uitable->setDelButtonEnable ( false);
			$this->dbtable->addCustomKriteria ( "selesai", "='1'" );
		}
		require_once 'fisiotherapy/resource/FisiotherapyResource.php';
		$fisres=new FisiotherapyResource($this->db);
		$this->list_pesan = $fisres->list_pesan;
		$this->nyeri_list=$fisres->getNyeriType();
	}
	public function command($command) {
		$adapter = new SimpleAdapter ();
		$adapter->add ( "Tanggal", "tanggal", "date d M Y" );
		$adapter->add ( "Pasien", "nama_pasien" );
		$adapter->add ( "NRM", "nrm_pasien", "digit8" );
		$adapter->add ( "No. Reg", "noreg_pasien", "digit8" );
		$adapter->add ( "Biaya Dokter", "biaya_dokter_rhb", "money Rp." );
		$adapter->add ( "Biaya Total", "biaya", "money Rp." );
		$adapter->add ( "Harga Asal", "harga_asal", "money Rp." );
		$adapter->add ( "Diskon (%)", "diskon", "back %");
		$adapter->add ( "Kelas", "kelas" ,"unslug");
		$adapter->add ( "Ruangan", "ruangan","unslug" );
		$adapter->add ( "Biaya Rehab", "biaya_dokter_rhb","money Rp." );
		$adapter->add ( "Asuhan", "asuhan_fisio","money Rp." );
		
		$dbres = new FisiotherapyResponder( $this->dbtable, $this->uitable, $adapter );
		if ($dbres->is ( "save" )) {
			//provit share
			$provit = new ProvitSharingService ( $this->db, $this->polislug, "smis-pv-fisiotherapy",$this->carabayar );
			$provit->execute ();
			$ps = $provit->getContent ();
			$dbres->addColumnFixValue ( "pembagian", $ps );
			
			//harga fisiotherapy
			$harga = new TarifFisiotherapy ( $this->db, $_POST ['kelas'] );
			$harga->execute ();
			$harga_fisiotherapy = $harga->getContent ();
			$dbres->addColumnFixValue ( "harga", $harga_fisiotherapy );
			
			//get_urjip
			$urji=new ServiceConsumer($this->db, "get_urjip",NULL,$_POST['ruangan']);
			$urji->setMode(ServiceConsumer::$SINGLE_MODE);
            $urji->setCached(true,"get_urjip");
			$urji->execute();
			$hasil=$urji->getContent();
			$urjip="-1";
			try{
				if($hasil!=NULL){
					if($hasil[$_POST['ruangan']]=="URI"){
						$urjip="1";
					}else if($hasil[$_POST['ruangan']]=="URJ"){
						$urjip="0";
					}else{
						$urjip="-1";
					}
				}
			}catch(Exception $e){
				$urjip="-1";
			}
			if($urjip!="-1")
				$dbres->addColumnFixValue ( "uri", $urjip);
		}
        
		loadLibrary("smis-libs-function-time");
		$dbres->setJsonColumn(array ("hasil","periksa"));
		if ($command == "save" && isset($_POST ['no_lab']) && ($_POST ['no_lab'] == '' || $_POST ['no_lab'] == '0')) {
			if ($_POST ['id'] == "" || $_POST ['id'] == "0") {
				$nomor = $this->dbtable->getNextID () * 1 - 1;
				$dbres->addColumnFixValue("no_lab", "FIS-" . $_POST ['noreg_pasien'] . "/" . to_romawi(date("m")) . "/" . date("y") . "/" . $nomor);
			} else {
				$dbres->addColumnFixValue("no_lab", "FIS-" . $_POST ['noreg_pasien'] . "/" . to_romawi(date("m")) . "/" . date("y") . "/" . $_POST ['id']);
			}
		}
		
		$dbres->setJsonColumn ( array ("hasil",	"periksa") );
		$data = $dbres->command ( $_POST ['command'] );
		echo json_encode ( $data );
		return;
	}
	public function phpPreLoad() {
		if(getSettings($this->db, "fisiotherapy-ui-model", "0")=="1"){
			$this->simpleUI();
		}else{
			$this->rigidUI();
		}
		
        /*show and hide marketing*/
        if(getSettings($this->db,"fisiotherapy-show-marketing","0")=="1"){
            $this->uitable->addModal("marketing", "chooser-".$this->action."-fst_marketing-Marketing", "Marketing", "", "n", null, true);
        }else{
            $this->uitable->addModal ( "marketing", "hidden", "", "", "y", null, true);
        }
        
		$shift=new OptionBuilder();
		$shift->add("Shift Pagi","pagi");
		$shift->add("Shift Sore","sore");
		$shift->add("Shift Malam","malam");
		
		$option=new OptionBuilder();
		$option->add("Bayar Penuh","0");
		$option->add("Gratis","100");
		$option->add("Keluarga Pegawai","15");
		$option->add("15%","15");
		$option->add("Paket 2","-100");
		$option->add("Paket 3","-200");
		$option->add("Paket 4","-300");
		$option->add("Paket 5","-400");
		$option->add("Paket 6","-500");
		$option->add("Paket 7","-600");
		$option->add("Paket 8","-700");
		$option->add("Paket 9","-800");
		$option->add("Paket 10","-9000");
		
		
		if(getSettings($this->db, "fisiotherapy-show-dokter-rehab", "0")=="1"){
			$this->uitable->addModal ( "nama_dokter_rhb", "chooser-" . $this->action . "-dokter_rhb-Dokter Rehab Medik", "Dokter Rehab Medis", "", "y", null, !$this->is_stand_alone,null,false,"id_dokter_rhb");
			$this->uitable->addModal ( "id_dokter_rhb", "hidden", "", "", "y", null, !$this->is_stand_alone,null,false,"biaya_dokter_rhb");
			$this->uitable->addModal ( "biaya_dokter_rhb", "money", "Biaya Dokter Rehab", "", "y", null, false,null,false,"diskon");
			
		}else{
			$this->uitable->addModal ( "nama_dokter_rhb", "hidden", "", "", "y", null, !$this->is_stand_alone,null,false,"id_dokter_rhb");
			$this->uitable->addModal ( "id_dokter_rhb", "hidden", "", "", "y", null, !$this->is_stand_alone,null,false,"biaya_dokter_rhb");
			$this->uitable->addModal ( "biaya_dokter_rhb", "hidden", "", "", "y", null, !$this->is_stand_alone,null,false,"diskon");
		}
		
		
		if(getSettings($this->db, "fisiotherapy-show-discount", "0")=="1") {
			$this->uitable->addModal ( "diskon", "select", "Diskon", $option->getContent(),"y",null,false,null,false,"diagnosa");
		}else{
			$this->uitable->addModal ( "diskon", "hidden", "", "0","y",null,false,null,false,"diagnosa");
		}
		
		if(getSettings($this->db, "fisiotherapy-show-diagnosa", "0")=="1") {
			$this->uitable->addModal ( "diagnosa", "text", "Diagnosa", "","y",null,false,null,false,"nyeri_diam");
		}else {
			$this->uitable->addModal ( "diagnosa", "hidden", "", "","y",null,false,null,false,"nyeri_diam");
		}
		
		if(getSettings($this->db, "fisiotherapy-show-diam", "0")=="1") {
			$this->uitable->addModal ( "nyeri_diam", "select", "Nyeri Diam", $this->nyeri_list,"y",null,false,null,false,"nyeri_gerak");
		}else{
			$this->uitable->addModal ( "nyeri_diam", "hidden", "", "","y",null,false,null,false,"nyeri_gerak");
		}
		
		if(getSettings($this->db, "fisiotherapy-show-gerak", "0")=="1") {
			$this->uitable->addModal ( "nyeri_gerak", "select", "Nyeri Gerak", $this->nyeri_list,"y",null,false,null,false,"nyeri_tekan");
		}else{
			$this->uitable->addModal ( "nyeri_gerak",  "hidden", "", "","y",null,false,null,false,"nyeri_tekan");
		}
		if(getSettings($this->db, "fisiotherapy-show-tekan", "0")=="1") {
			$this->uitable->addModal ( "nyeri_tekan", "select", "Nyeri Tekan", $this->nyeri_list,"y",null,false,null,false,"jk");
		}else{
			$this->uitable->addModal ( "nyeri_tekan",  "hidden", "", "","y",null,false,null,false,"jk");
		}
		
		$jkselect=new OptionBuilder();
		$jkselect->add("L","0",$this->jk=="0")->add("P","1",$this->jk=="1");
		if(getSettings($this->db, "fisiotherapy-show-gender", "0")=="1"){
			$this->uitable->addModal ( "jk", "select", "L/P", $jkselect->getContent(),"n",NULL,false,null,false,"umur");
		}else{
			$this->uitable->addModal ( "jk",  "hidden", "", $this->jk,"y",null,false,null,false,"umur");
		}
		if(getSettings($this->db, "fisiotherapy-show-aged", "0")=="1") {
			$this->uitable->addModal ( "umur", "text", "Umur", $this->umur,"y",null,false,null,false,"carabayar");
		}else{
			$this->uitable->addModal ( "umur",  "hidden", "", $this->umur,"y",null,false,null,false,"carabayar");
		}
		if($this->is_stand_alone){
			$service = new ServiceConsumer ( $this->db, "get_jenis_patient",NULL,"registration" );
			$service->execute ();
			$jenis_pasien = $service->getContent ();
			$cbayar=new OptionBuilder();
			foreach($jenis_pasien as $jp){
				$cbayar->add($jp['name'],$jp['value'],$jp['value']==$this->carabayar?"1":"0");
			}
			$this->uitable->addModal ( "carabayar", "select", "Jenis Pasien", $cbayar->getContent(), "y", null, false,null,false,"shift" );
		}else{
			$this->uitable->addModal ( "carabayar", "text", "Jenis Pasien", $this->carabayar, "y", null, false ,null,false,"shift" );
		}
		
		if(getSettings($this->db, "fisiotherapy-show-shift", "0")=="1") {
			$this->uitable->addModal ( "shift", "select", "Shift", $shift->getContent(),"y",null,false,null,false,"keterangan");
		}else{
			$this->uitable->addModal ( "shift",  "hidden", "", "","y",null,false,null,false,"keterangan");
		}
		
		$this->uitable->addModal ( "keterangan", "hidden", "", "","y",null,false,null,false,"save");
		
		if(getSettings($this->db, "fisiotherapy-show-asuhan-fisio", "0")=="1"){
			$this->uitable->addModal ( "asuhan_fisio", "money", "Asuhan Fisio", "", "y", null, false);			
		}else{
			$this->uitable->addModal ( "asuhan_fisio", "hidden", "", "", "y", null, false);
		}
		
		$modal = $this->uitable->getModal ();
		$modal->setTitle ( "Pesanan" );		
		$tabulator = new Tabulator ( "pesan", "pesanan", Tabulator::$POTRAIT );
		$tabulator->add ( "fst_perika", "Terapi", "fisiotherapy/resource/modal_pesanan.php", Tabulator::$TYPE_INCLUDE );		
		$modal->addHTML ( $tabulator->getHtml () );
		$modal->setClass ( Modal::$FULL_MODEL );	
		echo $this->uitable->getHtml ();
		echo $modal->getHtml ();
		
		require_once 'smis-libs-class/ServiceProviderList.php';
		$serv=new ServiceProviderList($this->db, "push_antrian");
		$serv->execute();
		$ruangan=$serv->getContent();		
		$select=new Select($this->action."_ruang_pindah", "Ruangan", $ruangan);		
		$hidden=new Hidden($this->action."_ruang_pindah_id", "", "");
		$btn=new Button("", "", "");
		$btn->setIsButton(Button::$ICONIC);
		$btn->setIcon("fa fa-save");
		$btn->setClass("btn-primary");
		$btn->setAction($this->action.".register()");
		$modal=new Modal("ruang_tujuan_modal", "", "Pilih Ruangan");
		$modal->addElement("Ruangan", $select);
		$modal->addElement("", $hidden);
		$modal->addFooter($btn);
		echo $modal->getHtml();
	}
	
	public function simpleUI(){
		$service = new RuanganService ( $this->db );
		$service->execute ();
		$ruangan = $service->getContent ();
		$ruangan [] = array ("name" => "PENDAFTARAN","value" => "Pendaftaran" );
		
		require_once 'smis-base/smis-include-service-consumer.php';
		$service = new ServiceConsumer ( $this->db, "get_kelas" );
        $service->setCached(true,"get_kelas");
		$service->execute ();
		$kelas = $service->getContent ();
		$option_kelas = new OptionBuilder ();
		foreach ( $kelas as $k ) {
			$nama = $k ['nama'];
			$slug = $k ['slug'];
			$option_kelas->add ( $nama, $slug, $slug == $this->kelas ? "1" : "0" );
		}
		
		$this->uitable->addModal ( "id", "hidden", "", "" );
		$this->uitable->addModal ( "uri", "hidden", "", $this->uri );
		if ($this->mode == self::$MODE_DAFTAR) {
			$this->uitable->addModal ( "tanggal", "date", "Tanggal", date ( "Y-m-d" ),"n",null,false,null,true,"no_lab" );
			$this->uitable->addModal ( "no_lab", "text", "Nomor", "", "y", null, true,null,false,"noreg_pasien" );
			$this->uitable->addModal ( "noreg_pasien", "text", "No Reg", $this->noreg_pasien, "n", null, true,null,false,"nama_pasien" );
			if ($this->noreg_pasien != "")
				$this->uitable->addModal ( "nama_pasien", "text", "Pasien", $this->nama_pasien, "n", null, true,null,false );
			else
				$this->uitable->addModal ( "nama_pasien", "chooser-" . $this->action . "-fst_pasien", "Pasien", $this->nama_pasien, "n", null, true ,null,false);
			$this->uitable->addModal ( "nrm_pasien", "text", "NRM", $this->nrm_pasien, "n", null, true ,null,false,"kelas");
			$this->uitable->addModal ( "kelas", "select", "Kelas", $option_kelas->getContent (), "n", null, false ,null,false,"id_dokter");
            
            $this->uitable->addModal ( "id_marketing","hidden","","");
            $this->uitable->addModal ( "nama_dokter", "chooser-" . $this->action . "-fst_dokter", "Dokter Pengirim", "", "n", null, false ,null,false,"id_dokter");
			$this->uitable->addModal ( "id_dokter", "hidden", "", "", "n",null,false,null,false,"nama_petugas");
			$this->uitable->addModal ( "nama_petugas", "text", "petugas", "", "y", null, true ,null,false,"id_pettugas");
			$this->uitable->addModal ( "id_petugas", "hidden", "", "","y",null,false,null,false,"diskon");			
		} else if($this->mode == self::$MODE_PERIKSA){
			$this->uitable->addModal ( "tanggal", "date", "Tanggal", date("Y-m-d"), "n", null, getSettings($this->db,"fisiotherapy-edit-tanggal","0")=="0",null,$this->is_stand_alone,"no_lab");
			$this->uitable->addModal ( "no_lab", "text", "Nomor", "" ,"y",null,false,null,!$this->is_stand_alone,"noreg_pasien");
			if($this->is_stand_alone || $this->is_keep){
				$this->uitable->addModal ( "noreg_pasien", "text", "No Reg", $this->noreg_pasien, "n", null, false,null,false,"nama_pasien");
				$this->uitable->addModal ( "nama_pasien", "chooser-" . $this->action . "-fst_pasien", "Pasien", $this->nama_pasien, "n", null, false,null,false);
				$this->uitable->addModal ( "nrm_pasien", "text", "NRM", $this->nrm_pasien, "n", null, false,null,false,"kelas");
			}else{
				$this->uitable->addModal ( "noreg_pasien", "text", "No Reg", $this->noreg_pasien, "n", null, !$this->is_stand_alone, null,false,"nama_pasien");
				$this->uitable->addModal ( "nama_pasien", "text", "Pasien", $this->nama_pasien, "n", null, !$this->is_stand_alone,null,false);
				$this->uitable->addModal ( "nrm_pasien", "text", "NRM", $this->nrm_pasien, "n", null, !$this->is_stand_alone,null,false,"kelas");
			}
			$this->uitable->addModal ( "kelas", "select", "Kelas", $option_kelas->getContent (), "n", null, false ,null,false,"nama_dokter");
           
            $this->uitable->addModal ( "id_marketing","hidden","","");
            $this->uitable->addModal ( "nama_dokter", "chooser-" . $this->action . "-fst_dokter", "Dokter Pengirim", "", "n", null, !$this->is_stand_alone ,null,false,"id_dokter");
			$this->uitable->addModal ( "id_dokter", "hidden", "", "", "n",null,false,null,false,"nama_petugas");
			$this->uitable->addModal ( "nama_petugas", "chooser-" . $this->action . "-fst_petugas", "Petugas", "", "y", null, !$this->is_stand_alone,null,false,"id_petugas");
			$this->uitable->addModal ( "id_petugas", "hidden", "", "","y",null,false,null,false,"diskon");
		}else{
			$this->uitable->addModal ( "tanggal", "date", "Tanggal", "", "y", null, getSettings($this->db,"fisiotherapy-edit-tanggal","0")=="0");
			$this->uitable->addModal ( "no_lab", "text", "Nomor", "y", null, true);
			$this->uitable->addModal ( "noreg_pasien", "text", "No Reg", $this->noreg_pasien, "n", null, !$this->is_stand_alone );
			$this->uitable->addModal ( "nama_pasien", "text", "Pasien", $this->nama_pasien, "n", null, !$this->is_stand_alone);
			$this->uitable->addModal ( "nrm_pasien", "text", "NRM", $this->nrm_pasien, "n", null, !$this->is_stand_alone);
			$this->uitable->addModal ( "kelas", "text", "Kelas", "", "y", null, true);
            
            $this->uitable->addModal ( "id_marketing","hidden","","");
            $this->uitable->addModal ( "pengirim", "chooser-".$this->action."-fst_pengirim-Pengirim", "Pengirim", "", "n", null, true);
            $this->uitable->addModal ( "id_pengirim","hidden","","");
			$this->uitable->addModal ( "nama_dokter", "text", "Dokter Pengirim", "", "y", null, true);
			$this->uitable->addModal ( "nama_petugas", "text", "Petugas", "", "y", null, true);
		}		
		$this->uitable->addModal ( "ruangan", "hidden", "",$this->polislug == "all"?$ruangan:$this->polislug );
	}
	
	public function rigidUI(){
		$service = new RuanganService ( $this->db );
		$service->execute ();
		$ruangan = $service->getContent ();
		$ruangan [] = array ("name" => "PENDAFTARAN","value" => "Pendaftaran" );
		
		require_once 'smis-base/smis-include-service-consumer.php';
		$service = new ServiceConsumer ( $this->db, "get_kelas" );
        $service->setCached(true,"get_kelas");
		$service->execute ();
		$kelas = $service->getContent ();
		$option_kelas = new OptionBuilder ();
		foreach ( $kelas as $k ) {
			$nama = $k ['nama'];
			$slug = $k ['slug'];
			$option_kelas->add ( $nama, $slug, $slug == $this->kelas ? "1" : "0" );
		}
		
		$this->uitable->addModal ( "id", "hidden", "", "" );
		$this->uitable->addModal ( "uri", "hidden", "", $this->uri );
		if ($this->mode == self::$MODE_DAFTAR) {
			$this->uitable->addModal ( "tanggal", "date", "Tanggal", date ( "Y-m-d" ),"n",null,false,null,false,"kelas" );
			$this->uitable->addModal ( "kelas", "select", "Kelas", $option_kelas->getContent (), "n", null, false,null,false,"no_lab" );
			$this->uitable->addModal ( "no_lab", "text", "Nomor", "", "y", null, true,null,false,null,"nama_pasien" );
			if ($this->noreg_pasien != "")
				$this->uitable->addModal ( "nama_pasien", "text", "Pasien", $this->nama_pasien, "n", null, true,null,false );
			else
				$this->uitable->addModal ( "nama_pasien", "chooser-" . $this->action . "-fst_pasien", "Pasien", $this->nama_pasien, "n", null, true,null,false );
			$this->uitable->addModal ( "noreg_pasien", "text", "No Reg", $this->noreg_pasien, "n", null, true,null,false,"nama_dokter" );
			$this->uitable->addModal ( "nrm_pasien", "text", "NRM", $this->nrm_pasien, "n", null, true ,null,false,"nama_dokter");
			$this->uitable->addModal ( "nama_dokter", "chooser-" . $this->action . "-fst_dokter", "Pengirim", "", "y", null, false,null,false,"id_doker" );
			$this->uitable->addModal ( "id_dokter", "text", "NIP Pengirim", "", "y", null, !$this->is_stand_alone,null,false,"ruangan");
			if ($this->polislug == "all")
				$this->uitable->addModal ( "ruangan", "select", "Asal Ruangan", $ruangan,"y",null,false,null,false,"nama_petugas" );
			else
				$this->uitable->addModal ( "ruangan", "text", "Asal Ruangan", $this->polislug, "n", null, true,null,false,"nama_petugas"  );
			$this->uitable->addModal ( "nama_petugas", "text", "petugas", "", "y", null, true,null,false,"id_petugas"  );
			$this->uitable->addModal ( "id_petugas", "text", "NIP petugas", "", "y", null, true,null,false,"diskon"  );
		} else if($this->mode == self::$MODE_PERIKSA){
			$this->uitable->addModal ( "tanggal", "date", "Tanggal", date("Y-m-d"), "n", null,getSettings($this->db,"fisiotherapy-edit-tanggal","0")=="0",null,false,"kelas");
			$this->uitable->addModal ( "kelas", "select", "Kelas", $option_kelas->getContent (), "n", null, false,null,!$this->is_stand_alone,"no_lab" );
			$this->uitable->addModal ( "no_lab", "text", "Nomor", "" ,"y",null,false,null,false,"nama_pasien");
			if($this->is_stand_alone || $this->is_keep){
				$this->uitable->addModal ( "nama_pasien", "chooser-" . $this->action . "-fst_pasien", "Pasien", $this->nama_pasien, "n", null, false,null,false);
				$this->uitable->addModal ( "noreg_pasien", "text", "No Reg", $this->noreg_pasien, "n", null, false,null,false,"nrm_pasien");
				$this->uitable->addModal ( "nrm_pasien", "text", "NRM", $this->nrm_pasien, "n", null, false,null,false,"nama_dokter");
			}else{
				$this->uitable->addModal ( "nama_pasien", "text", "Pasien", $this->nama_pasien, "n", null, !$this->is_stand_alone,null,false);
				$this->uitable->addModal ( "noreg_pasien", "text", "No Reg", $this->noreg_pasien, "n", null, !$this->is_stand_alone,null,false,"nrm_pasien" );
				$this->uitable->addModal ( "nrm_pasien", "text", "NRM", $this->nrm_pasien, "n", null, !$this->is_stand_alone,null,false,"nama_dokter");
			}
			$this->uitable->addModal ( "nama_dokter", "chooser-" . $this->action . "-fst_dokter", "Pengirim", "", "y", null, !$this->is_stand_alone ,null,false,"id_dokter");
			$this->uitable->addModal ( "id_dokter", "text", "NIP Pengirim", "", "y", null, !$this->is_stand_alone,null,false,"ruangan");
			$this->uitable->addModal ( "ruangan", "select", "Asal Ruangan", $ruangan ,"n", null, false,null,false,"nama_petugas");
			$this->uitable->addModal ( "nama_petugas", "chooser-" . $this->action . "-fst_petugas", "Petugas", "", "y", null, !$this->is_stand_alone,null,false,"id_petugas");
			$this->uitable->addModal ( "id_petugas", "text", "NIP petugas", "", "y", null, !$this->is_stand_alone,null,false,"diskon");
		}else{
			$this->uitable->addModal ( "tanggal", "date", "Tanggal", "", "y", null, getSettings($this->db,"fisiotherapy-edit-tanggal","0")=="0");
			$this->uitable->addModal ( "kelas", "text", "Kelas", "", "y", null, true);
			$this->uitable->addModal ( "no_lab", "text", "Nomor", "y", null, true);
			$this->uitable->addModal ( "nama_pasien", "text", "Pasien", $this->nama_pasien, "n", null, !$this->is_stand_alone);
			$this->uitable->addModal ( "noreg_pasien", "text", "No Reg", $this->noreg_pasien, "n", null, !$this->is_stand_alone );
			$this->uitable->addModal ( "nrm_pasien", "text", "NRM", $this->nrm_pasien, "n", null, !$this->is_stand_alone);
			$this->uitable->addModal ( "nama_dokter", "text", "Pengirim", "", "y", null, true);
			$this->uitable->addModal ( "ruangan", "text", "Asal Ruangan", "","y", null, true);
			$this->uitable->addModal ( "nama_petugas", "text", "Petugas", "", "y", null, true);
		}
		
		
	}
	
	/* when it's star build */
	public function jsPreLoad() {
		?>
		<script type="text/javascript">

		var <?php echo $this->action; ?>;
		var fst_dokter;
		var fst_petugas;
		var fst_pasien;
        var fst_marketing;
        var fst_pengirim;
		var dokter_rhb;
		var fst_list_pesan;
		var fst_mode="<?php echo $this->mode; ?>";
		var fst_noreg="<?php echo $this->noreg_pasien; ?>";
		var fst_nama_pasien="<?php echo $this->nama_pasien; ?>";
		var fst_nrm_pasien="<?php echo $this->nrm_pasien; ?>";
		var fst_polislug="<?php echo $this->polislug; ?>";
		var fst_the_page="<?php echo $this->page; ?>";
		var fst_the_protoslug="<?php echo $this->protoslug; ?>";
		var fst_the_protoname="<?php echo $this->protoname; ?>";
		var fst_the_protoimplement="<?php echo $this->protoimplement; ?>";
		var FST_PREFIX="<?php echo $this->action; ?>";

		
		
		$(document).ready(function() {
			$(".mydate").datepicker();
			$("#"+FST_PREFIX+"_diskon").on("change",function(){
				$("#"+FST_PREFIX+"_keterangan").val($("#"+FST_PREFIX+"_diskon option:selected").text());
			});
			
			<?php  echo arrayToJSVar("fst_list_pesan",$this->list_pesan,false); ?>
			
			fst_pasien=new TableAction("fst_pasien",fst_the_page,"<?php echo $this->action; ?>",new Array());
			fst_pasien.setSuperCommand("fst_pasien");
			fst_pasien.setPrototipe(fst_the_protoname,fst_the_protoslug,fst_the_protoimplement);
			fst_pasien.selected=function(json){
				$("#<?php echo $this->action; ?>_nama_pasien").val(json.nama_pasien);
				$("#<?php echo $this->action; ?>_nrm_pasien").val(json.nrm);
				$("#<?php echo $this->action; ?>_noreg_pasien").val(json.id);
				$("#<?php echo $this->action; ?>_carabayar").val(json.carabayar);
				$("#<?php echo $this->action; ?>_jk").val(json.kelamin);
				$("#<?php echo $this->action; ?>_umur").val(json.umur);
                fst_pasien.get_last_ruangan(json.id,json.jenislayanan,json.kamar_inap);
            };
            
            fst_pasien.get_last_ruangan = function(noreg,rajal,ranap){
                var cur_ruang = (ranap==null || ranap=="") ? rajal:ranap;
                var data = fst_pasien.getRegulerData();
                data['super_command'] = "";
                data['command'] = "last_position";
                data['noreg_pasien'] = noreg;
                showLoading();
                $.post("",data,function(res){
                    var json = getContent(res);
                    if(json!=""){
                        cur_ruang = json;
                    }
                    $("#<?php echo $this->action; ?>_ruangan").val(cur_ruang);
                    dismissLoading();
                });
            };

			dokter_rhb=new TableAction("dokter_rhb",fst_the_page,"<?php echo $this->action; ?>",new Array());
			dokter_rhb.setSuperCommand("dokter_rhb");
			dokter_rhb.setPrototipe(fst_the_protoname,fst_the_protoslug,fst_the_protoimplement);
			dokter_rhb.selected=function(json){
				$("#<?php echo $this->action; ?>_nama_dokter_rhb").val(json.nama);
				$("#<?php echo $this->action; ?>_id_dokter_rhb").val(json.id);
			};
			
			fst_dokter=new TableAction("fst_dokter",fst_the_page,"<?php echo $this->action; ?>",new Array());
			fst_dokter.setSuperCommand("fst_dokter");
			fst_dokter.setPrototipe(fst_the_protoname,fst_the_protoslug,fst_the_protoimplement);
			fst_dokter.selected=function(json){
				$("#<?php echo $this->action; ?>_nama_dokter").val(json.nama);
				$("#<?php echo $this->action; ?>_id_dokter").val(json.id);
			};

			fst_petugas=new TableAction("fst_petugas",fst_the_page,"<?php echo $this->action; ?>",new Array());
			fst_petugas.setSuperCommand("fst_petugas");
			fst_petugas.setPrototipe(fst_the_protoname,fst_the_protoslug,fst_the_protoimplement);
			fst_petugas.selected=function(json){
				$("#<?php echo $this->action; ?>_nama_petugas").val(json.nama);
				$("#<?php echo $this->action; ?>_id_petugas").val(json.id);
			};
            
            fst_marketing=new TableAction("fst_marketing",fst_the_page,"<?php echo $this->action; ?>",new Array());
			fst_marketing.setSuperCommand("fst_marketing");
			fst_marketing.setPrototipe(fst_the_protoname,fst_the_protoslug,fst_the_protoimplement);
			fst_marketing.selected=function(json){
				$("#<?php echo $this->action; ?>_marketing").val(json.nama);
				$("#<?php echo $this->action; ?>_id_marketing").val(json.id);
			};
            
            fst_pengirim=new TableAction("fst_pengirim",fst_the_page,"<?php echo $this->action; ?>",new Array());
			fst_pengirim.setSuperCommand("fst_pengirim");
			fst_pengirim.setPrototipe(fst_the_protoname,fst_the_protoslug,fst_the_protoimplement);
			fst_pengirim.selected=function(json){
				$("#<?php echo $this->action; ?>_pengirim").val(json.nama);
				$("#<?php echo $this->action; ?>_id_pengirim").val(json.id);
			};
			
			var column=new Array("id","tanggal","ruangan",
									"kelas","nama_pasien","noreg_pasien",
                                    "id_marketing", "marketing", "id_pengirim", "pengirim",
									"nrm_pasien","no_lab","id_dokter",
									"nama_dokter","id_petugas",
									"nama_petugas","nyeri_tekan",
									"nyeri_diam","nyeri_gerak",
									"uri","jk","umur","carabayar",
									"diskon","keterangan","diagnosa",
									"nama_dokter_rhb","id_dokter_rhb","biaya_dokter_rhb",
									"shift","asuhan_fisio");
			<?php echo $this->action; ?>=new TableAction("<?php echo $this->action; ?>",fst_the_page,"<?php echo $this->action; ?>",column);
			<?php echo $this->action; ?>.setPrototipe(fst_the_protoname,fst_the_protoslug,fst_the_protoimplement);
			<?php echo $this->action; ?>.setEnableAutofocus(true);
			<?php echo $this->action; ?>.setNextEnter();
			<?php echo $this->action; ?>.getRegulerData=function(){
				var reg_data={	
						page:this.page,
						action:this.action,
						super_command:this.super_command,
						prototype_name:this.prototype_name,
						prototype_slug:this.prototype_slug,
						prototype_implement:this.prototype_implement,
						polislug:fst_polislug,
						noreg_pasien:fst_noreg,
						nama_pasien:fst_nama_pasien,
						nrm_pasien:fst_nrm_pasien,
						mode:"<?php echo $this->mode; ?>"
						};
				return reg_data;
            };
            
            <?php echo $this->action; ?>.cekTutupTagihan = function(){
                var reg_data = {	
                    page                : this.page,
                    action              : this.action,
                    super_command       : this.super_command,
                    prototype_name      : this.prototype_name,
                    prototype_slug      : this.prototype_slug,
                    prototype_implement : this.prototype_implement,
                    polislug            : fst_polislug,
                };			
                var noreg                 = $("#"+this.prefix+"_noreg_pasien").val();
                if(noreg==""){
                    smis_alert("Error","Silakan Pilih Pasien Terlebih Dahulu","alert-danger");
                    return;
                }
                reg_data['command']        = 'cek_tutup_tagihan';
                reg_data['noreg_pasien']  = noreg;
                
                var res = $.ajax({
                    type: "POST",
                    url: "",
                    data:reg_data,
                    async: false
                }).responseText;

                var json = getContent(res);
                if(json=="1"){
                    return false;
                }else{
                    return true;
                }
            };

			<?php echo $this->action; ?>.selesai=function(id){
				var selesai_data=this.getRegulerData();
				var self=this;
				selesai_data['command']="selesai";
				selesai_data['id']=id;
				showLoading();
				$.post("",selesai_data,function(res){
					var json=getContent(res);
					self.view();
					dismissLoading();
				});
			};

			<?php echo $this->action; ?>.register=function(id){
				$("#ruang_tujuan_modal").smodal('hide');
				var self=this;
				var reg_data=self.getRegulerData();
				reg_data['command']="register";
				reg_data['tujuan']=$("#"+FST_PREFIX+"_ruang_pindah").val();
				reg_data['id']=$("#"+FST_PREFIX+"_ruang_pindah_id").val();
				showLoading();
				$.post("",reg_data,function(res){
					var json=getContent(res);
					self.view();
					dismissLoading();
				});

				
			};

			<?php echo $this->action; ?>.show_register=function(id){
				$("#"+FST_PREFIX+"_ruang_pindah_id").val(id);
				$("#ruang_tujuan_modal").smodal('show');
			};
			
			<?php echo $this->action; ?>.getSaveData=function(){
				var save_data=this.getRegulerData();
				save_data['command']="save";

				if(fst_mode=="<?php echo self::$MODE_ARCHIVE; ?>"){
					return save_data;	
				}
			
				
				for(var i=0;i<this.column.length;i++){
					var name=this.column[i];
					var typical=$("#"+this.prefix+"_"+name).attr('typical');
					var type=$("#"+this.prefix+"_"+name).attr('type');
					if(typical=="money"){
						save_data[name]=$("#"+this.prefix+"_"+name).maskMoney('unmasked')[0];
					}else if(type=="checkbox"){
						save_data[name]=$("#"+this.prefix+"_"+name).is(':checked')?1:0;
					}else{
						save_data[name]=$("#"+this.prefix+"_"+name).val();
					}
				}

				//if(fst_mode=="<?php echo self::$MODE_DAFTAR; ?>"){
					var data_pesan={};
					for (var i = 0; i < fst_list_pesan.length; i++) {
						var name_list=fst_list_pesan[i];
						data_pesan[name_list]=$("#fisiotherapy_"+name_list).is(':checked')?1:0;
					}
					save_data['periksa']=data_pesan;	
				//}

			
				return save_data;
            };
            
            <?php echo $this->action; ?>.save = function(){
                showLoading();
                var cek = this.cekTutupTagihan();
                if(cek){
                    TableAction.prototype.save.call(this);        
                }
                dismissLoading();
            };

			<?php echo $this->action; ?>.clear=function(){	
				for(var i=0;i<this.column.length;i++){
					var name=this.column[i];	
						if($("#"+this.prefix+"_"+name).is(':checkbox')){
							$("#"+this.prefix+"_"+name).attr('checked', false);
						}else if($("#"+this.prefix+"_"+name).attr('typical')=="money"){
							var val=$("#"+this.prefix+"_"+name).attr("dv");
							$("#"+this.prefix+"_"+name).maskMoney('mask',Number(val));
						}else{
							var val=$("#"+this.prefix+"_"+name).attr("dv");
							$("#"+this.prefix+"_"+name).val(val);
						}			
					
				}
				this.enabledOnNotEdit(this.column_disabled_on_edit);	
				
				for (var i = 0; i < fst_list_pesan.length; i++) {
					var name_list=fst_list_pesan[i];
					$("#fisiotherapy_"+name_list).prop('checked', false);
				}
            };
            
            <?php echo $this->action; ?>.rehab_medik=function(id){
                LoadSmisPage({
                    page:this.page,
                    action:"rehab_medik",
                    prototype_name:this.prototype_name,
                    prototype_slug:this.prototype_slug,
                    prototype_implement:this.prototype_implement,
                    id_antrian:id
                });
            };

            <?php echo $this->action; ?>.pelayanan_khusus=function(id){
                LoadSmisPage({
                    page:this.page,
                    action:"pelayanan_khusus",
                    prototype_name:this.prototype_name,
                    prototype_slug:this.prototype_slug,
                    prototype_implement:this.prototype_implement,
                    id_antrian:id
                });	
            };

            <?php echo $this->action; ?>.lap_rl52=function(id){
                LoadSmisPage({
                    page:this.page,
                    action:"lap_rl52",
                    prototype_name:this.prototype_name,
                    prototype_slug:this.prototype_slug,
                    prototype_implement:this.prototype_implement,
                    id_antrian:id
                });	
            };

			<?php echo $this->action; ?>.edit=function (id){
				var self=this;
				showLoading();	
				var edit_data=this.getEditData(id);
				$.post('',edit_data,function(res){		
					var json=getContent(res);
					if(json==null) return;
					for(var i=0;i<self.column.length;i++){
						var name=self.column[i];
						var typical=$("#"+self.prefix+"_"+name).attr('typical');
						var type=$("#"+self.prefix+"_"+name).attr('type');
						if(typical=="money"){
							$("#"+self.prefix+"_"+name).maskMoney('mask',Number(json[""+name]));
						}else if(type=="checkbox"){
							if(json[""+name]=="1") $("#"+self.prefix+"_"+name).prop('checked', true);
							else $("#"+self.prefix+"_"+name).prop('checked', false);
						}else{
							$("#"+self.prefix+"_"+name).val(json[""+name]);
						}
						
					}

					if(json['periksa']!=""){
						try{
							list_periksa_json=$.parseJSON(json['periksa']);
							for (var i = 0; i < fst_list_pesan.length; i++) {
								var name_list=fst_list_pesan[i];
								var value_list=list_periksa_json[name_list];
								if(value_list=="1") $("#fisiotherapy_"+name_list).prop('checked', true);
								else $("#fisiotherapy_"+name_list).prop('checked', false);
							}
						}catch(e){

						}
					}

					dismissLoading();
					self.disabledOnEdit(self.column_disabled_on_edit);
					self.show_form();
				});
			};
			
			<?php /*if($this->mode!=self::$MODE_DAFTAR)*/ echo $this->action.".view();"; ?>
			/*fst_next_enter("tanggal","kelas",false);
			fst_next_enter("kelas","no_lab",true);
			fst_next_enter("no_lab","nama_pasien",false);
			fst_next_enter("nama_pasien","noreg_pasien",false);
			fst_next_enter("noreg_pasien","nrm_pasien",false);
			fst_next_enter("nrm_pasien","nama_dokter",false);
			fst_next_enter("nama_dokter","ruangan",false);
			fst_next_enter("ruangan","nama_petugas",true);
			fst_next_enter("nama_petugas","nyeri_diam",false);
			fst_next_enter("nyeri_diam","nyeri_gerak",true);
			fst_next_enter("nyeri_gerak","nyeri_tekan",true);
			fst_next_enter("nyeri_tekan","jk",true);
			fst_next_enter("jk","umur",true);*/
			
			$('#'+FST_PREFIX+'_nama_dokter').typeahead({
				minLength:3,
		        source: function (query, process) {
			     var data_dokter=fst_dokter.getViewData();
			     data_dokter["kriteria"]=$('#'+FST_PREFIX+'_nama_dokter').val();
		         var $items = new Array;
		           $items = [""];				                
		          $.ajax({
		            url: '',
		            type: 'POST',
		            data: data_dokter,
		            success: function(res) {
		              var json=getContent(res);
		              var the_data_proses=json.d.data;
		               $items = [""];	
		              $.map(the_data_proses, function(data){
		                  var group;
		                  group = {
		                      id: data.id,
		                      name: data.nama,                            
		                      toString: function () {
		                    	  return JSON.stringify(this);
		                      },
		                      toLowerCase: function () {
		                          return this.name.toLowerCase();
		                      },
		                      indexOf: function (string) {
		                          return String.prototype.indexOf.apply(this.name, arguments);
		                      },
		                      replace: function (string) {
		                          var value = '';
		                          value +=  this.name;
		                          if(typeof(this.level) != 'undefined') {
		                              value += ' <span class="pull-right muted">';
		                              value += this.level;
		                              value += '</span>';
		                          }
		                          return String.prototype.replace.apply('<div class="typeaheadiv">' + value + '</div>', arguments);
		                      }
		                  };
		                  $items.push(group);
		              });
		              
		              process($items);
		            }
		          });
		        },
		        updater: function (item) {
		            var item = JSON.parse(item);
					$("#"+FST_PREFIX+"_id_dokter").val(item.id);
					$("#"+FST_PREFIX+"_ruangan").focus();
		            return item.name;
		        }
		      });


			$('#'+FST_PREFIX+'_nama_dokter_rhb').typeahead({
				minLength:3,
		        source: function (query, process) {
			     var data_dokter=fst_dokter.getViewData();
			     data_dokter["kriteria"]=$('#'+FST_PREFIX+'_nama_dokter_rhb').val();
		         var $items = new Array;
		           $items = [""];				                
		          $.ajax({
		            url: '',
		            type: 'POST',
		            data: data_dokter,
		            success: function(res) {
		              var json=getContent(res);
		              var the_data_proses=json.d.data;
		               $items = [""];	
		              $.map(the_data_proses, function(data){
		                  var group;
		                  group = {
		                      id: data.id,
		                      name: data.nama,                            
		                      toString: function () {
		                    	  return JSON.stringify(this);
		                      },
		                      toLowerCase: function () {
		                          return this.name.toLowerCase();
		                      },
		                      indexOf: function (string) {
		                          return String.prototype.indexOf.apply(this.name, arguments);
		                      },
		                      replace: function (string) {
		                          var value = '';
		                          value +=  this.name;
		                          if(typeof(this.level) != 'undefined') {
		                              value += ' <span class="pull-right muted">';
		                              value += this.level;
		                              value += '</span>';
		                          }
		                          return String.prototype.replace.apply('<div class="typeaheadiv">' + value + '</div>', arguments);
		                      }
		                  };
		                  $items.push(group);
		              });
		              
		              process($items);
		            }
		          });
		        },
		        updater: function (item) {
		            var item = JSON.parse(item);
					$("#"+FST_PREFIX+"_id_dokter_rhb").val(item.id);
					$("#"+FST_PREFIX+"_biaya_dokter_rhb").focus();
		            return item.name;
		        }
		      });


			$('#'+FST_PREFIX+'_nama_pasien').typeahead({
				minLength:3,
		        source: function (query, process) {
			     var data_dokter=fst_pasien.getViewData();
			     data_dokter["kriteria"]=$('#'+FST_PREFIX+'_nama_pasien').val();
		         var $items = new Array;
		           $items = [""];				                
		          $.ajax({
		            url: '',
		            type: 'POST',
		            data: data_dokter,
		            success: function(res) {
		              var json=getContent(res);
		              var the_data_proses=json.d.data;
		               $items = [""];	
		              $.map(the_data_proses, function(data){
		                  var group;
		                  group = {
		                      id: data.id,
		                      name: data.nama_pasien,                
		                      nrm: data.nrm,                            
		                      toString: function () {
		                    	  return JSON.stringify(this);
		                      },
		                      toLowerCase: function () {
		                          return this.name.toLowerCase();
		                      },
		                      indexOf: function (string) {
		                          return String.prototype.indexOf.apply(this.name, arguments);
		                      },
		                      replace: function (string) {
		                          var value = '';
		                          value +=  this.name;
		                          if(typeof(this.level) != 'undefined') {
		                              value += ' <span class="pull-right muted">';
		                              value += this.level;
		                              value += '</span>';
		                          }
		                          return String.prototype.replace.apply('<div class="typeaheadiv">' + value + '</div>', arguments);
		                      }
		                  };
		                  $items.push(group);
		              });
		              
		              process($items);
		            }
		          });
		        },
		        updater: function (item) {
		            var item = JSON.parse(item);
					$("#"+FST_PREFIX+"_noreg_pasien").val(item.id);
					$("#"+FST_PREFIX+"_nama_pasien").val(item.name);
					$("#"+FST_PREFIX+"_nrm_pasien").val(item.nrm);
					$("#"+FST_PREFIX+"_uri").val(item.uri);
					$("#"+FST_PREFIX+"_nrm_pasien").focus();
		            return item.name;
		        }
		      });


			$('#'+FST_PREFIX+'_nama_petugas').typeahead({
				minLength:3,
		        source: function (query, process) {
			     var data_dokter=fst_petugas.getViewData();
			     data_dokter["kriteria"]=$('#'+FST_PREFIX+'_nama_petugas').val();
		         var $items = new Array;
		           $items = [""];				                
		          $.ajax({
		            url: '',
		            type: 'POST',
		            data: data_dokter,
		            success: function(res) {
		              var json=getContent(res);
		              var the_data_proses=json.d.data;
		               $items = [""];	
		              $.map(the_data_proses, function(data){
		                  var group;
		                  group = {
		                      id: data.id,
		                      name: data.nama,                            
		                      toString: function () {
		                    	  return JSON.stringify(this);
		                      },
		                      toLowerCase: function () {
		                          return this.name.toLowerCase();
		                      },
		                      indexOf: function (string) {
		                          return String.prototype.indexOf.apply(this.name, arguments);
		                      },
		                      replace: function (string) {
		                          var value = '';
		                          value +=  this.name;
		                          if(typeof(this.level) != 'undefined') {
		                              value += ' <span class="pull-right muted">';
		                              value += this.level;
		                              value += '</span>';
		                          }
		                          return String.prototype.replace.apply('<div class="typeaheadiv">' + value + '</div>', arguments);
		                      }
		                  };
		                  $items.push(group);
		              });
		              
		              process($items);
		            }
		          });
		        },
		        updater: function (item) {
		            var item = JSON.parse(item);
					$("#"+FST_PREFIX+"_id_petugas").val(item.id);
					$("#"+FST_PREFIX+"_nyeri_diam").focus();
		            return item.name;
		        }
		      });
			
			
		});
		</script>
			<?php
	}
	
	public function cssPreLoad() {
		?>
			<style type="text/css">
				#laboratory_add_form_form label {width: 150px;}
				.smis_form div label {margin-top: 0px !important;}
				.head-title {margin-top: 20px;}
				#laboratory_add_form_form .span3 {margin-top: 20px;}
				.indent {padding-left: 40px !important;	}
				.div-check {height: 40px;}
				.div-check label {	width: 250px !important;}
				.div-check:hover {color: red;}			
				.div-check span {	width: 200px;}			
				.div-check input[type='checkbox'] {	min-height: 10px !important;	max-height: 20px !important;}
				.modal-body-html {	clear: both;}		
				#hasil_Fisiotherapy label {	min-width: 250px !important;}
				.fis_kwitansi{ page-break-inside:avoid !important; }
				.fis_kwitansi td { height:auto; margin:0px !important; }
			</style>
		<?php
	}
	
	public function superCommand($super_command) {
		$super = new SuperCommand ();
        
        if($super_command=="dokter_rhb")
        {
            $header=array (	'Nama','Jabatan',"NIP");
            $dktable = new Table($header, "", NULL, true);
            $dktable->setName("dokter_rhb");
            $dktable->setModel(Table::$SELECT);
            $dkadapter = new SimpleAdapter ();
            $dkadapter->add("Jabatan", "nama_jabatan");
            $dkadapter->add("Nama", "nama");
            $dkadapter->add("NIP", "nip");
            $dkresponder = new EmployeeResponder($this->db, $dktable, $dkadapter, "");
            $super->addResponder("dokter_rhb", $dkresponder);
        }
        else if($super_command=="fst_dokter")
        {
            $header=array (	'Nama','Jabatan',"NIP");
            $dktable = new Table($header, "", NULL, true);
            $dktable->setName("fst_dokter");
            $dktable->setModel(Table::$SELECT);
            $dkadapter = new SimpleAdapter ();
            $dkadapter->add("Jabatan", "nama_jabatan");
            $dkadapter->add("Nama", "nama");
            $dkadapter->add("NIP", "nip");
            $dkresponder = new EmployeeResponder($this->db, $dktable, $dkadapter, "");
            $super->addResponder("fst_dokter", $dkresponder);
        }
        else if($super_command=="fst_konsultan")
        {
            $header=array (	'Nama','Jabatan',"NIP");
            $kktable = new Table ($header, "", NULL, true);
            $kktable->setName("fst_konsultan");
            $kktable->setModel(Table::$SELECT);
            $dkadapter = new SimpleAdapter ();
            $dkadapter->add("Jabatan", "nama_jabatan");
            $dkadapter->add("Nama", "nama");
            $dkadapter->add("NIP", "nip");
            $kkresponder = new DKResponder($this->db, $kktable, $dkadapter, "employee");
            $super->addResponder("fst_konsultan", $kkresponder);
        }
        else if($super_command=="fst_petugas")
        {
            $header=array (	'Nama','Jabatan',"NIP");
            $pettable = new Table ($header, "", NULL, true);
            $pettable->setName("fst_petugas");
            $pettable->setModel(Table::$SELECT);
            $dkadapter = new SimpleAdapter ();
            $dkadapter->add("Jabatan", "nama_jabatan");
            $dkadapter->add("Nama", "nama");
            $dkadapter->add("NIP", "nip");
            $petresponder = new EmployeeResponder($this->db, $pettable, $dkadapter, "emd");
            $super->addResponder("fst_petugas", $petresponder);
        }
        else if($super_command=="fst_pasien")
        {
            $phead=array ('Nama','NRM',"No Reg");
            $ptable = new Table($phead, "", NULL, true);
            $ptable->setName("fst_pasien");
            $ptable->setModel(Table::$SELECT);
            $padapter = new SimpleAdapter ();
            $padapter->add("Nama", "nama_pasien");
            $padapter->add("NRM", "nrm", "digit8");
            $padapter->add("No Reg", "id");
            $presponder = new ServiceResponder($this->db, $ptable, $padapter, "get_registered");
            $super->addResponder("fst_pasien", $presponder);
        }
        else if($super_command=="fst_marketing")
        {
            $marketing_table = new Table (array('Nama', 'Jabatan', 'NIP'), "", NULL, true);
            $marketing_table->setName("fst_marketing");
            $marketing_table->setModel(Table::$SELECT);
            $marketing_adapter = new SimpleAdapter();
            $marketing_adapter->add("Nama", "nama");
            $marketing_adapter->add("Jabatan", "nama_jabatan");
            $marketing_adapter->add("NIP", "nip");
            $marketing_dbresponder = new EmployeeResponder($this->db, $marketing_table, $marketing_adapter, "marketing");
            $super->addResponder ("fst_marketing", $marketing_dbresponder);
        }
        
        
		$init = $super->initialize ();
		if ($init != null) {
			echo $init;
			return;
		}
	}
	
	public function jsLoader() {
		echo addJS ( "framework/bootstrap/js/bootstrap-datepicker.js" );
		echo addJS ( "framework/smis/js/table_action.js" );
		loadLibrary ( "smis-libs-function-javascript" );
	}
	public function cssLoader() {
		echo addCSS ( "framework/bootstrap/css/datepicker.css" );
	}
	
	
}


?>