<?php 

class  FisiotherapyLaporanAdapter extends SimpleAdapter{
	
	private $content=array();
	private $curgrup="";
	
	public function adapt($d){
		$a=parent::adapt($d);
		if($this->curgrup!=$d['grup']){
			$this->curgrup=$d['grup'];
			$b=array("No."=>"<strong>".$d['grup']."</strong>");
			$this->content[]=$b;
		}
		$this->content[]=$a;
	}
	
	public function getContent($data){
		parent::getContent($data);
		return $this->content;
	}
	
}


?>