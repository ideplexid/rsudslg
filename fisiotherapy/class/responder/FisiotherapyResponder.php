<?php
class FisiotherapyResponder extends DBResponder {
	
	public function command($command){
      
       if($command=="last_position"){
            $pack    = new ResponsePackage();
			$service = new ServiceConsumer ( $db, "get_last_position",$params = array("noreg_pasien"=> $_POST['noreg_pasien']),"medical_record" );
	        $service->execute();
            $content=$service->getContent();
    
            $pack->setContent($content);
			$pack->setStatus(ResponsePackage::$STATUS_OK);
			$pack->setAlertVisible(false);
			return $pack->getPackage();
        }else if($command=="register"){
			$pack=new ResponsePackage();
			$content=NULL;
			$status='not-command';	//not-authorized, not-command, fail, success
			$alert=array();
			$content=$this->register();
			if($content['success']==1){
				$pack->setAlertVisible(true);
				$pack->setAlertContent("Berhasil", "Pasien Telah Berhasil Di Daftarkan Ke Ruang Tujuan dengan nomor urut <i class='badge badge-info'>".$content['no_urut']."</i> ");
			}else{
				$pack->setAlertVisible(true);
				$pack->setAlertContent("Gagal ", "Pasien Telah Gagal Di Daftarkan Ke Ruang Tujuan");
			}			
			$pack->setStatus(ResponsePackage::$STATUS_OK);
			return $pack->getPackage();
		}else if($command=="cek_tutup_tagihan"){
			$pack=new ResponsePackage();
			$content=NULL;
			$status='not-command';	//not-authorized, not-command, fail, success
			$alert=array();
            $result = $this->cek_tutup_tagihan();
            
            if($result=="1"){
                $pack->setAlertVisible(true);
			    $pack->setAlertContent("Peringatan", "<strong>Pasien Telah Di Tutup Tagihanya Oleh Kasir untuk proses pembayaran</strong>, </br>silakan menghubungi kasir dahulu bila akan menambah, menubah, mengurangi dan menghapus tagihan pasien",Alert::$DANGER);
            }

			$pack->setContent($result);
			$pack->setStatus(ResponsePackage::$STATUS_OK);
			return $pack->getPackage();
		}else if($command!="selesai" && $command!="register"){
			return parent::command($command);
		}else{
			$pack=new ResponsePackage();
			$content=NULL;
			$status='not-command';	//not-authorized, not-command, fail, success
			$alert=array();
			$content=$this->selesai();
			$pack->setContent($content);
			$pack->setStatus(ResponsePackage::$STATUS_OK);
			return $pack->getPackage();
		}
	}
	
	public function register(){
		$data = $this->postToArray ();
		
		
		$id = $_POST ['id'];	
		$row = $this->dbtable->select($id);
		$ruangan = $_POST ['tujuan'];
		$array = array ();
		$array ['waktu'] = date("Y-m-d");
		$array ['nrm_pasien'] = $row->nrm_pasien;
		$array ['nama_pasien'] = $row->nama_pasien;
		$array ['id'] = "";
		$array ['command'] = 'save';
		$array ['carabayar'] = $row->carabayar;
		$array ['no_register'] = $row->noreg_pasien;
		$array ['umur'] = $row->umur;
		$array ['asal'] = "Fisiotherapy";
		$array ['kelas'] = "Rawat Jalan";
		$array ['jk'] = $row->jk;
		$array ['golongan_umur'] = "";
		
		$antri = new ServiceConsumer ( $this->dbtable->get_db (), "push_antrian", $array, $ruangan );
		$antri->execute ();
		$result = $antri->getContent ();
		$no_urut=-1;
		if ($result != null && $result != "" && isset ( $result ['content'] ['nomor'] )) {
			$no_urut= $result ['content'] ['nomor'];
		}

		if ($no_urut == - 1) { // gagal
			$success ['id'] = - 1;
			$success ['success'] = 0;
			return $success;
		}
		$success ['no_urut'] = $no_urut;
		$success ['success'] = 1;		
		return $success;
	}
	
	public function selesai(){
		$data['selesai']="1";
		$id['id']=$_POST['id'];
		$result = $this->dbtable->update ( $data, $id );
		$success ['type'] = 'update';
		$success ['id'] = $id ['id'];
		$success ['success'] = 1;
		if ($result === false)
			$success ['success'] = 0;
		return $success;
	}
	
	public function save() {
		$data = $this->postToArray ();
		$data['barulama']=$this->isBaru($_POST['nrm_pasien'], $_POST['id']);
		$id ['id'] = $_POST ['id'];
		$result = $this->dbtable->update ( $data, $id );
		$success ['type'] = 'update';
		
		if (isset ( $data ['periksa'] )) {
			$sklas = $data ['kelas'];
			//$sklas = strtolower ( $kelas );
			$sklas = str_replace ( " ", "_", $sklas );
			$harga = $data ['harga'];
			$data ['harga'] = $harga;
			$periksa = json_decode ( $data ['periksa'], true );
			$biaya = 0;
			if ($harga != null) {
				$dharga = json_decode ( $harga, true );
				foreach ( $periksa as $k => $v ) {
					if ($v == "1") {
						$the_key = $sklas . "_" . $k;
						$biaya += ($dharga [$the_key] * 1);
					}
				}
			}
			$data ['harga_asal'] = $biaya;
            $data ['biaya'] = $data ['harga_asal'] * (100-$data['diskon'])/100;
            $data ['biaya']=$data ['biaya']+$data ['biaya_dokter_rhb']+$data['asuhan_fisio'];
		}
		
		
		if ($_POST ['id'] == 0 || $_POST ['id'] == "") {
			$result = $this->dbtable->insert ( $data );
			$id ['id'] = $this->dbtable->get_inserted_id ();
			$success ['type'] = 'insert';
			$this->notifInsert();
		} else {
			$result = $this->dbtable->update ( $data, $id );
			$success ['type'] = 'update';
		}
		$success ['id'] = $id ['id'];
		$success ['success'] = 1;
		$this->sendMedicalRecord($success ['id']);
		if ($result === false) 
			$success ['success'] = 0;
        
        if(getSettings($db,"fisiotherapy-accounting-auto-notif","0")=="1") {
            $this-> synchronizeToAccounting($this->dbtable->get_db(),$success ['id'] ,"");
        }
		return $success;
	}

    public function delete(){
        $id['id']				= $_POST['id'];
        $one = $this->dbtable->select($id);
        $_POST['noreg_pasien'] = $one->noreg_pasien;
        $cek = $this->cek_tutup_tagihan();

        if($cek=="1"){
            $success['success']	= -1;
            return $success;
        }

		$result = parent::delete();
        if(getSettings($db,"fisiotherapy-accounting-auto-notif","0")=="1") {
            $this-> synchronizeToAccounting($this->dbtable->get_db(), $_POST['id'] ,"del");
        }
        return $result;
	}
	
	private function sendMedicalRecord($id){
		loadLibrary("smis-libs-function-medical");
		$this->dbtable->setFetchMethode(DBTable::$OBJECT_FETCH);
		$fst=$this->dbtable->select($id);
		$service=new ServiceConsumer($this->dbtable->get_db(), "push_diagnosa",NULL,"medical_record");
		$service->addData("tanggal", $fst->tanggal);
		$service->addData("noreg_pasien", $fst->noreg_pasien);
		$service->addData("nrm_pasien", $fst->nrm_pasien);
		$service->addData("nama_pasien", $fst->nama_pasien);
		$service->addData("ruangan", "fisiotherapy");
		$service->addData("urji", $fst->uri);
		$service->addData("umur", $fst->umur);
		$service->addData("id_asal_data", $fst->id);
		$service->addData("diagnosa", $fst->diagnosa);
		$service->addData("jk", $fst->jk);
		$service->addData("id_dokter", $fst->id_dokter_rhb);
		$service->addData("nama_dokter", $fst->nama_dokter_rhb);
		$service->addData("gol_umur", medical_umur_to_gol_umur($fst->umur));
		$service->addData("kunjungan", $fst->barulama==0?"Baru":"Lama");
		$service->addData("kelas", $fst->kelas);
		$service->addData("carabayar", $fst->carabayar);
		$service->execute();
	}
	
	private function isBaru($nrm,$id){
		$query="SELECT COUNT(*) as total FROM smis_fst_pesanan WHERE nrm_pasien='".$nrm."' AND id!='".$id."'";
		$db=$this->getDBTable()->get_db();
		$total=$db->get_var($query);
		if($total=="0" || $total<=0){
			return 0;
		}else{
			return 1;
		}
	}
	
	private function notifInsert(){
		global $notification;
		$slug="Masuk Fisiotherapy";
		$tgl=ArrayAdapter::format("date d M Y H:i:s", $_POST['tanggal']." ".date("H:i:s"));
		$ruang=ArrayAdapter::format("unslug", $_POST['ruangan']);
		$message="Pasien <strong>".$_POST['nama_pasien']."</strong> Masuk Fisiotherapy Pada <strong>".$tgl."</strong> Dari <strong>".$ruang."</strong>";
		$key=md5($message.$slug);
		$notification->addNotification($slug, $key, $message,"fisiotherapy","pemeriksaan");
	}
    
    public function synchronizeToAccounting($db,$id,$is_del=""){
        $x=$this->dbtable->selectEventDel($id);
        
        $data=array();
        $data['jenis_akun'] = "transaction";
        $data['jenis_data'] = "penjualan";
        $data['id_data']    = $id;
        $data['entity']     = "fisiotherapy";
        $data['service']    = "get_detail_accounting";
        $data['data']       = $id;
        $data['code']       = "fst-".$id;
        $data['operation']  = $is_del;
        $data['tanggal']    = $x->tanggal;
        $data['uraian']     = "Fisiotherapy Pasien ".$x->nama_pasien." Pada Noreg ".$x->id;;
        $data['nilai']      = $x->biaya;
        
        $serv=new ServiceConsumer($db,"push_notify_accounting",$data,"accounting");
        $serv->execute();
    }

    public function cek_tutup_tagihan(){
        global $db;
        if(getSettings($db,"fisiotherapy-aktifkan-tutup-tagihan","0")=="0"){
            return 0;
        }
        require_once "smis-base/smis-include-service-consumer.php";
        $serv = new ServiceConsumer($this->getDBTable()->get_db(),"cek_tutup_tagihan",NULL,"registration");
        $serv ->addData("noreg_pasien",$_POST['noreg_pasien']);
        $serv ->execute();
        $result = $serv ->getContent();
        return $result;
    }
	
}