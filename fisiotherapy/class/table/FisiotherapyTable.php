<?php

class FisiotherapyTable extends Table{
	
	public function getPrintedElement($p, $f){
		$tbl=new TablePrint("");
		$tbl->setMaxWidth(false);
		$tbl->addStyle("width", "100%");
		$tbl->setTableClass("fis_kwitansi");
		global $db;
		
		$periksa=json_decode($p->periksa,true);
		$name=array();
		if($periksa!=null){
			foreach($periksa as $k=>$v){
				if($v=="1") $name[]=ArrayAdapter::format("unslug", $k);
			}
		}
		$nama_fis=getSettings($db,"fisiotherapy-name", "Fisiotherapy");
		$seting_rhb=getSettings($db, "fisiotherapy-show-dokter-rehab", "0")=="1";
		$seting_asuhan=getSettings($db, "fisiotherapy-show-asuhan-fisio", "0")=="1";
		$nama=getSettings($db,"smis_autonomous_title", "");
		$alamat=getSettings($db,"smis_autonomous_address", "");
		$tbl->addColumn("<h5> Kwitansi ".$nama_fis." ".$nama."</h5>", 4, 1);
		$tbl->commit("title");
		$tbl->addColumn("No. ".ArrayAdapter::format("only-digit10", $p->id), 1, 1);
		$tbl->addColumn("<small>".$alamat."</small>", 3, 1);
		$tbl->commit("title");
		
		$tbl->addColumn("Nama", 1, 1);
		$tbl->addColumn($p->nama_pasien, 1, 1);
		$tbl->addColumn("Keterangan", 1, 1);
		$tbl->addColumn($p->keterangan, 1, 1);
		$tbl->commit("header");
		
		
		
		$tbl->addColumn("No. Fisiotherapy", 1, 1);
		$tbl->addColumn($p->no_lab, 1, 1);
		$tbl->addColumn("NRM", 1, 1);
		$tbl->addColumn(ArrayAdapter::format("only-digit8", $p->nrm_pasien), 1, 1);
		$tbl->commit("header");
		
		$tbl->addColumn("Kelas", 1, 1);
		$tbl->addColumn(ArrayAdapter::format("unslug", $p->kelas), 1, 1);
		$tbl->addColumn("Jenis Pasien", 1, 1);
		$tbl->addColumn($p->carabayar, 1, 1);
		$tbl->commit("header");
		
		
		$tbl->addColumn("Pengirim", 1, 1);
		$tbl->addColumn($p->nama_dokter, 1, 1);
		$tbl->addColumn("Tanggal", 1, 1);
		$tbl->addColumn(ArrayAdapter::format("date d M Y", $p->tanggal), 1, 1);
		$tbl->commit("header");
		
		
		loadLibrary("smis-libs-function-math");
		$tbl->addColumn("Terapi", 1, 1);
		$tbl->addColumn(implode(" , ", $name), 3, 1);
		$tbl->commit("body");
		
		$TOTAL_BIAYA=$p->biaya-$p->biaya_dokter_rhb-$p->asuhan_fisio;
		$tbl->addColumn("Nominal", 3, 1);
		$tbl->addColumn(ArrayAdapter::format("money Rp.", $TOTAL_BIAYA), 1, 1);
		$tbl->commit("body");
		if($seting_rhb && $p->biaya_dokter_rhb!=NULL && $p->biaya_dokter_rhb*1>0){
			$tbl->addColumn("Biaya Dokter Rehab Medik (".$p->nama_dokter_rhb.") ", 3, 1);
			$tbl->addColumn(ArrayAdapter::format("money Rp.", $p->biaya_dokter_rhb), 1, 1);
			$tbl->commit("body");
			$TOTAL_BIAYA+=$p->biaya_dokter_rhb;
			
			if(!$seting_asuhan || $p->asuhan_fisio==NULL || $p->asuhan_fisio*1<=0){
				$tbl->addColumn("Total Biaya", 3, 1);
				$tbl->addColumn(ArrayAdapter::format("money Rp.", $TOTAL_BIAYA), 1, 1);
				$tbl->commit("body");
			}
		}
		
		if($seting_asuhan && $p->asuhan_fisio!=NULL && $p->asuhan_fisio*1>0){
			
			$tbl->addColumn("Biaya Asuhan Fisiotherapy  ", 3, 1);
			$tbl->addColumn(ArrayAdapter::format("money Rp.", $p->asuhan_fisio), 1, 1);
			$tbl->commit("body");
			$TOTAL_BIAYA+=$p->asuhan_fisio;
			
			$tbl->addColumn("Total Biaya", 3, 1);
			$tbl->addColumn(ArrayAdapter::format("money Rp.", $TOTAL_BIAYA), 1, 1);
			$tbl->commit("body");
		}
		
		$tbl->addColumn(numbertell($TOTAL_BIAYA)." Rupiah", 4, 1);
		$tbl->commit("body");
		
		$tbl->addColumn("TTD Kasir", 2, 1);
		$tbl->addColumn("TTD", 2, 1);
		$tbl->commit("footer");
		
		$tbl->addColumn("", 2, 1);
		$tbl->addColumn("</br>", 2, 1);
		$tbl->commit("footer");
		
		global $user;
		$tbl->addColumn("", 2, 1);
		$tbl->addColumn($user->getNameOnly(), 2, 1);
		$tbl->commit("footer");
		
		$hasil=$tbl->getHtml();
		$result=$hasil."<div class='cutline'></div>".$hasil." <div class='cutline'></div>".$hasil;
		return $result;
	}
	
}

?>
