<?php 
global $db;
$tahun  = $_POST['tahun'];
$query  = "select '' AS id,
        year(tanggal) AS tahun,
        sum(if((month(tanggal) = 1),1,0)) AS jan,
        sum(if((month(tanggal) = 2),1,0)) AS feb,
        sum(if((month(tanggal) = 3),1,0)) AS mar,
        sum(if((month(tanggal) = 4),1,0)) AS apr,
        sum(if((month(tanggal) = 5),1,0)) AS mei,
        sum(if((month(tanggal) = 6),1,0)) AS jun,
        sum(if((month(tanggal) = 7),1,0)) AS jul,
        sum(if((month(tanggal) = 8),1,0)) AS ags,
        sum(if((month(tanggal) = 9),1,0)) AS sep,
        sum(if((month(tanggal) = 10),1,0)) AS okt,
        sum(if((month(tanggal) = 11),1,0)) AS nov,
        sum(if((month(tanggal) = 12),1,0)) AS des,
        sum(if((year(tanggal) = '".$tahun."'),1,0)) AS total,
        '' AS prop from smis_fst_pesanan 
        where (prop != 'del') 
        AND (year(tanggal) = '".$tahun."')
        group by year(tanggal)
        ";
$result = $db->get_result($query);

$array=array();
foreach($result as $key => $value) {
    foreach($value as $x => $y) {
        $array[$x] = $y;
    }
}

$array['ruangan']= 'fisiotherapy';
$array['urji'] = 'URJ';
echo json_encode($array);

?>