<?php 
require_once 'fisiotherapy/class/TarifFisiotherapy.php';
global $db;
$id=$_POST['id'];
$kelas=$_POST['kelas'];
$dbtable=new DBTable($db, "smis_fst_pesanan");
$one=$dbtable->select($id);
$periksa=json_decode($one->periksa,true);


$harga = new TarifFisiotherapy ( $db, $kelas );
$harga->execute ();
$harga_fisiotherapy = $harga->getContent ();

$biaya=0;
$dharga = json_decode ( $harga_fisiotherapy, true );
foreach ( $periksa as $k => $v ) {
	if ($v == "1") {
		$the_key = $kelas . "_" . $k;
		$biaya += ($dharga [$the_key] * 1);
	}
}

$hasil=array();
$hasil['harga']=$biaya;
$hasil['nama']="Fisiotherapy - ".ArrayAdapter::format("unslug", $kelas);
echo json_encode($hasil);


?>