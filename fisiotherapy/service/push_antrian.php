<?php
global $db;
require_once 'smis-base/smis-include-service-consumer.php';

$data ['tanggal'] = $_POST ['waktu'];
$data ['nama_pasien'] = $_POST ['nama_pasien'];
$data ['nrm_pasien'] = $_POST ['nrm_pasien'];
$data ['noreg_pasien'] = $_POST ['no_register'];

$default_kelas=getSettings($db,"fisiotherapy-ui-pemeriksaan-default-kelas","rawat_jalan");
$kelas = getSettings($db, "smis-rs-kelas-" . ArrayAdapter::format("slug", $_POST['asal']));
if ($kelas == null)
	$kelas = $default_kelas;
$data ['kelas'] = $kelas;

//mendapatkan uri pasien saat ini:
$params = array();
$params['noreg_pasien'] = $_POST['no_register'];
$service = new ServiceConsumer($db, "get_uri", $params, "registration");
$service->setMode(ServiceConsumer::$CLEAN_BOTH);
$content = $service->execute()->getContent();
$data ['uri'] = $content[0];
$data ['ruangan'] = $_POST ['asal'];
$data ['jk'] = $_POST ['jk'];
$detail=json_decode($_POST['detail'],true);
$data ['umur'] = $_POST ['umur'];
$dbtable = new DBTable ( $db, "smis_fst_pesanan" );
$dbtable->insert ( $data );

$nomor = $dbtable->count ( "" );
$success ['id'] = $dbtable->get_inserted_id ();
$success ['success'] = 1;
$success ['type'] = "insert";
$success ['nomor'] = $nomor;
$content = $success;
$response = new ResponsePackage ();
$response->setContent ( $content );
$response->setStatus ( ResponsePackage::$STATUS_OK );
$response->setAlertVisible ( true );
$response->setAlertContent ( "Data Saved", "Your Data Had Been Saved", ResponsePackage::$TIPE_INFO );
echo json_encode ( $response->getPackage () );
?>