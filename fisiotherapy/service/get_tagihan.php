<?php
global $db;
$array = array ();
$list_name = array();
$list_debit_kredit = array();
$dbtable=new DBTable($db, "smis_fst_layanan");
$dbtable->setShowAll(true);
$d=$dbtable->view("", "0");
$list=$d['data'];
foreach($list as $one){
    $slug                      = $one->slug;
    $name                      = $one->nama;
    $array [$slug]             = $name;
    $list_name[$slug]          = $name;
    $list_debit_kredit[$slug]  = array("d"=>$one->debet,"k"=>$one->kredit);
}
if (isset ( $_POST ['noreg_pasien'] )) {
	$noreg = $_POST ['noreg_pasien'];
	$response['selesai'] = "1";
	$response['exist'] = "1";
	$response['reverse'] = "0";
	$response['cara_keluar'] = "Selesai";
	$response['jasa_pelayanan'] = "1";
	$dbtable = new DBTable ( $db, "smis_fst_pesanan" );
	$dbtable->addCustomKriteria ( "noreg_pasien", "='" . $noreg . "'" );
	$dbtable->setShowAll ( true );
	$data    = $dbtable->view ( "", "0" );
	$rows    = $data ['data'];
	$result  = array ();
	foreach ( $rows as $d ) {
		$onedata['waktu']   = ArrayAdapter::format ( "date d M Y", $d->tanggal );
		$onedata['nama']    = "Fisiotherapy - " . $d->kelas;
		$onedata['id']      = $d->id;
		$onedata['biaya']   = $d->biaya-$d->biaya_dokter_rhb-$d->asuhan_fisio;
		$onedata['start']   = $d->tanggal;
		$onedata['end']     = $d->tanggal;
		$onedata['debet']   = getSettings($db, "fisiotherapy-accounting-debit-global-".$d->carabayar, "");
		$onedata['kredit']  = getSettings($db, "fisiotherapy-accounting-kredit-global-".$d->carabayar, "");
        
		$periksa            = json_decode ( $d->periksa, true );
		$harga              = json_decode ( $d->harga, true );
		$kls                = str_replace ( " ", "_", $d->kelas );
		$ket_res            = array ();
		$ket_res []         = "(No Fisiotherapy : ".$d->no_lab.")";
		$ket_res []         = "('Dokter Pengirim : ' ".$d->nama_dokter.")";
		foreach ( $periksa as $p => $val ) {
			if ($val == "1") {
				$nm         = $array [$p];
				$vlu        = $harga [$kls . "_" . $p];
				$dk         = $list_debit_kredit[$p];
				$ket_res [] = "( ".$nm." : " .ArrayAdapter::format ( "only-money Rp.", $vlu )." )";
			}
		}
		$onedata ['keterangan'] = implode(" , ", $ket_res );//;ArrayAdapter::format ( "array", $ket_res );
		$result [] = $onedata;
		
		/*menampilkaan dokter rehab medis secara terpisah*/
		if($d->biaya_dokter_rhb!=NULL &&  $d->biaya_dokter_rhb*1>0){
			$onedata                 = array();
			$onedata['waktu']        = ArrayAdapter::format ( "date d M Y", $d->tanggal );
			$onedata['nama']         = "Dokter Rehab Fisiotherapy - ".$d->nama_dokter_rhb;
			$onedata['id']           = $d->id."_rhb";
			$onedata['biaya']        = $d->biaya_dokter_rhb;
			$onedata['start']        = $d->tanggal;
			$onedata['end']          = $d->tanggal;		
			$onedata['keterangan']   = '(Dokter Rehab : '.$d->nama_dokter_rhb.")";
			$onedata['keterangan']  .= '(Biaya Dokter Rehab : '. ArrayAdapter::format ( "only-money Rp.", $d->biaya_dokter_rhb ).")";
			$result []               = $onedata;
		}
		
		/*menampilkaan asuhan fisiotherapy secara terpisah*/
		if($d->asuhan_fisio!=NULL &&  $d->asuhan_fisio*1>0){
			$onedata               = array();
			$onedata['waktu']      = ArrayAdapter::format ( "date d M Y", $d->tanggal );
			$onedata['nama']       = "Asuhan Fisiotherapy ";
			$onedata['id']         = $d->id."_asuhan";
			$onedata['biaya']      = $d->asuhan_fisio;
			$onedata['start']      = $d->tanggal;
			$onedata['end']        = $d->tanggal;		
			$onedata['keterangan'] = 'Asuhan Fisiotherapy';
			$result[]              = $onedata;
		}
		
	}
	$unit_data = array (
			"result" => $result,
			"jasa_pelayanan" => "1" 
	);
	$ldata ['fisiotherapy'] = $unit_data;
	$response ['data'] = $ldata;
	echo json_encode ( $response );
}

?>