<?php
        global $db;

        if (isset($_POST['noreg_pasien'])) {
                $noreg_pasien = $_POST['noreg_pasien'];

                $dbtable = new DBTable($db, "smis_fst_pesanan");
                $row = $dbtable->get_row("
                        SELECT SUM(biaya) AS 'pemeriksaan_reguler'
                        FROM smis_fst_pesanan
                        WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop NOT LIKE 'del'
                ");
                $pemeriksaan_reguler = 0;
                if ($row != null) {
                        $pemeriksaan_reguler += $row->pemeriksaan_reguler;
                }
                 
                $data = array();
                $data['data'] = array(
                        "ruangan"                               => "fisiotherapy",
                        "pemeriksaan_reguler"   => $pemeriksaan_reguler
                );
                echo json_encode($data);
        }
?>