<?php

require_once "fisiotherapy/resource/FisiotherapyResource.php";
global $db;
$id=$_POST['data'];
show_error();
$dbtable= new DBTable($db,"smis_fst_pesanan");
$x          = $dbtable->selectEventDel($id);
$resource   = new FisiotherapyResource($db);

//Mendapatkan nama dan kode akun tiap layanan
$list_name = array();
$list_debit_kredit = array();
$fst_dbtable=new DBTable($db, "smis_fst_layanan");
$fst_dbtable->setShowAll(true);
$fst_dbtable->setOrder(" nama ASC ");
$data=$fst_dbtable->view("", "0");
$list_fst=$data['data'];
$array=array();
foreach($list_fst as $d){
    $slug=$d->slug;
    $name=$d->nama;
    $array [$slug] = $name;
    
    $list_name[$d->slug]=$d->nama;
    $list_debit_kredit[$d->slug]=array("d"=>$d->debet,"k"=>$d->kredit);
}

/*transaksi untuk pasien Fisiotherapy*/
$list   = array();
$periksa=json_decode($x->periksa,true);
$harga=json_decode($x->harga,true);

foreach($periksa as $slug=>$v){
    if($v=="0")
        continue;
    
    $biaya=$harga[$x->kelas."_".$slug];
    //$nama=$resource->getListName($slug);
    $nama=$list_name[$slug];
    $debet=array();
    //$dk=$resource->getDebitKredit($slug);
    $dk = $list_debit_kredit[$slug];
    $debet['akun']    = $dk['d'];
    $debet['debet']   = $biaya;
    $debet['kredit']  = 0;
    $debet['ket']     = "Piutang Fisiotherapy - ".$nama." - ".$x->no_lab." pasien ".$x->nama_pasien." dengan No.Reg ".$x->id;
    $debet['code']    = "debet-fisiotherapy-".$x->id;
    $list[] = $debet;
     
    $kredit=array();
    $kredit['akun']    = $dk['k'];
    $kredit['debet']   = 0;
    $kredit['kredit']  = $biaya;
    $kredit['ket']     = "Pendapatan Fisiotherapy - ".$nama." - ".$x->no_lab." pasien ".$x->nama_pasien." dengan No.Reg ".$x->id;
    $kredit['code']    = "kredit-fisiotherapy-".$x->id;
    $list[] = $kredit;
}

//content untuk header
$header=array();
$header['tanggal']      = $x->tanggal;
$header['keterangan']   = "Transaksi Fisiotherapy ".$x->no_lab." Pasien ".$x->nama_pasien." dengan No.Reg ".$x->id;
$header['code']         = "Fisiotherapy-".$x->id;
$header['nomor']        = "FST-".$x->id;
$header['debet']        = $x->biaya;
$header['kredit']       = $x->biaya;
$header['io']           = "1";

$transaction_Fisiotherapy=array();
$transaction_Fisiotherapy['header']=$header;
$transaction_Fisiotherapy['content']=$list;

/*transaksi keseluruhan*/
$transaction=array();
$transaction[]=$transaction_Fisiotherapy;
echo json_encode($transaction);

/*confirm that already synch*/
$update['akunting']=1;
$id['id']=$x->id;
$dbtable->setName("smis_fst_pesanan");
$dbtable->update($update,$id);

?>