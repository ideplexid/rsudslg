<?php 
global $db;
global $user;
require_once 'smis-base/smis-include-service-consumer.php';
if(isset($_POST['super_command']) && $_POST['super_command']=="total") {
	$dbtable=new DBTable($db, "smis_fst_pesanan");
	$dbtable->addCustomKriteria(NULL, "tanggal>='".$_POST['dari']."'");
	$dbtable->addCustomKriteria(NULL, "tanggal<'".$_POST['sampai']."'");
	$query=$dbtable->getQueryCount("");
	$content=$db->get_var($query);
	$pack=new ResponsePackage();
	$pack->setContent($content);
	$pack->setStatus(ResponsePackage::$STATUS_OK);
	echo json_encode($pack->getPackage());
	return;
}

$header=array("ID","Pasien","NRM","Biaya","Dokter","Kelas","B/L","Diagnosa");
$uitable=new Table($header);
$uitable->setName("koreksi_diagnosa_pasien")
		->setActionEnable(false)
		->setFooterVisible(false);

if(isset($_POST['super_command']) && $_POST['super_command']=="limit") {
	$dbtable=new DBTable($db, "smis_fst_pesanan");
	$dbtable->addCustomKriteria(NULL, "tanggal>='".$_POST['dari']."'");
	$dbtable->addCustomKriteria(NULL, "tanggal<'".$_POST['sampai']."'");
	$dbtable->setMaximum(1);
	$limit_start=$_POST['limit_start'];
	$data=$dbtable->view("", $limit_start);
	$fst=$data['data'][0];
	
	loadLibrary("smis-libs-function-medical");	
	$service=new ServiceConsumer($db, "push_diagnosa",NULL,"medical_record");
	$service->addData("tanggal", $fst->tanggal);
	$service->addData("noreg_pasien", $fst->noreg_pasien);
	$service->addData("nrm_pasien", $fst->nrm_pasien);
	$service->addData("nama_pasien", $fst->nama_pasien);
	$service->addData("ruangan", "fisiotherapy");
	$service->addData("umur", $fst->umur);
	$service->addData("id_asal_data", $fst->id);
	$service->addData("diagnosa", $fst->diagnosa);
	$service->addData("jk", $fst->jk);
	$service->addData("id_dokter", $fst->id_dokter_rhb);
	$service->addData("nama_dokter", $fst->nama_dokter_rhb);
	$service->addData("gol_umur", medical_umur_to_gol_umur($fst->umur));
	$service->addData("kunjungan", $fst->barulama==0?"Baru":"Lama");
	$service->addData("kelas", $fst->kelas);
	$service->addData("carabayar", $fst->carabayar);
	$service->execute();
	
	$simple=new SimpleAdapter();
	$simple->add("ID","id","digit6");
	$simple->add("Pasien","nama_pasien");
	$simple->add("NRM","nrm_pasien","digit8");
	$simple->add("Biaya","biaya","money Rp.");
	$simple->add("Dokter","nama_dokter");
	$simple->add("Kelas","kelas");
	$simple->add("Diagnosa","diagnosa");
	$simple->add("B/L","barulama","trivial_0_B_L");
	$content=$simple->getContent($data['data']);
	$uitable->setContent($content);
	$bdcontent=$uitable->getBodyContent();	
	

	
	$pack=new ResponsePackage();
	$pack->setContent($bdcontent);
	$pack->setStatus(ResponsePackage::$STATUS_OK);
	echo json_encode($pack->getPackage());
	return;
	
}

$uitable->addModal("dari", "date", "Dari", "")
		->addModal("sampai", "date", "Sampai", "");
$action=new Button("","","View");
$action->setClass("btn-primary")
	   ->setIsButton(Button::$ICONIC)
	   ->setIcon("icon-white fa fa-circle-o-notch")
	   ->setAction("koreksi_diagnosa_pasien.koreksi()");
$btn_froup=new ButtonGroup("");
$btn_froup->addButton($action);
$form=$uitable
	  ->getModal()
	  ->setTitle("fisiotherapy")
	  ->getForm()
	  ->addElement("",$btn_froup);

$close=new Button("", "", "Batal");
$close	->addClass("btn-primary")
		->setIsButton(Button::$ICONIC_TEXT)
		->setIcon("fa fa-close")
		->setAction("koreksi_diagnosa_pasien.batal()");

$load=new LoadingBar("rekap_fst_bar", "");
$modal=new Modal("rekap_fst_modal", "", "Processing...");
$modal	->addHTML($load->getHtml(),"after")
		->addFooter($close);



echo $form->getHtml();
echo "<div class='clear'></div>";
echo "<div id='result_koreksi_diagnosa_pasien'>".$uitable->getHtml()."</div>";
echo $modal->getHtml();
echo addJS("framework/bootstrap/js/bootstrap-datepicker.js");
echo addJS("framework/smis/js/table_action.js");
echo addCSS("framework/bootstrap/css/datepicker.css");
echo addJS ( "base-js/smis-base-loading.js");
?>

<script type="text/javascript">
	var koreksi_diagnosa_pasien;
	var koreksi_diagnosa_pasien_karyawan;
	var koreksi_diagnosa_pasien_data;
	var IS_koreksi_diagnosa_pasien_RUNNING;
	$(document).ready(function(){
		$('.mydate').datepicker();
		koreksi_diagnosa_pasien=new TableAction("koreksi_diagnosa_pasien","fisiotherapy","koreksi_diagnosa_pasien",new Array());
		koreksi_diagnosa_pasien.addRegulerData=function(data){
			data['dari']=$("#koreksi_diagnosa_pasien_dari").val();
			data['sampai']=$("#koreksi_diagnosa_pasien_sampai").val();
			$("#dari_table_koreksi_diagnosa_pasien").html(getFormattedDate(data['dari']));
			$("#sampai_table_koreksi_diagnosa_pasien").html(getFormattedDate(data['sampai']));			
			return data;
		};

		koreksi_diagnosa_pasien.batal=function(){
			IS_koreksi_diagnosa_pasien_RUNNING=false;
			$("#rekap_fst_modal").modal("hide");
		};
		
		koreksi_diagnosa_pasien.afterview=function(json){
			if(json!=null){
				$("#kode_table_koreksi_diagnosa_pasien").html(json.nomor);
				$("#waktu_table_koreksi_diagnosa_pasien").html(json.waktu);
				koreksi_diagnosa_pasien_data=json;
			}
		};

		koreksi_diagnosa_pasien.koreksi=function(){
			if(IS_koreksi_diagnosa_pasien_RUNNING) return;
			$("#rekap_fst_bar").sload("true","Fetching total data",0);
			$("#rekap_fst_modal").modal("show");
			IS_koreksi_diagnosa_pasien_RUNNING=true;
			var d=this.getRegulerData();
			d['super_command']="total";
			$.post("",d,function(res){
				var total=Number(getContent(res));
				if(total>0) {
					$("#koreksi_diagnosa_pasien_list").html("");
					koreksi_diagnosa_pasien.rekaploop(0,total);
				} else {
					$("#rekap_fst_modal").modal("hide");
					IS_koreksi_diagnosa_pasien_RUNNING=false;
				}
			});
		};

		koreksi_diagnosa_pasien.rekaploop=function(current,total){
			$("#rekap_fst_bar").sload("true","Calculation... [ "+current+" / "+total+" ] ",(current*100/total));
			if(current>=total || !IS_koreksi_diagnosa_pasien_RUNNING) {
				$("#rekap_fst_modal").modal("hide");
				IS_koreksi_diagnosa_pasien_RUNNING=false;
				return;
			}
			var d=this.getRegulerData();
			d['super_command']="limit";
			d['limit_start']=current;			
			$.post("",d,function(res){
				var ct=getContent(res);
				$("#koreksi_diagnosa_pasien_list").append(ct);
				setTimeout(function(){koreksi_diagnosa_pasien.rekaploop(++current,total)},300);
			});
		};
				
	});
</script>