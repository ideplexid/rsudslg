<?php
global $db;
require_once 'smis-libs-class/MasterTemplate.php';
require_once ("smis-base/smis-include-service-consumer.php");
$layanan= new MasterTemplate($db, "smis_fst_layanan", "fisiotherapy", "layanan");
$header = array("Nama","Slug","Keterangan", "Kode Debet", "Kode Kredit");
$layanan->getDBtable()
        ->setOrder(" nama ASC ");
$layanan->getUItable()
        ->setHeader($header)
        ->addModal("id", "hidden", "", "")
        ->addModal("nama", "text", "Nama","")
        ->addModal("slug", "text","Slug", "")
        ->addModal("keterangan", "textarea", "Keterangan", "")
        ->addModal("debet", "chooser-layanan-debet_layanan-Kode Debet", "Kode Debet", "","y",NULL,true,NULL,false)
        ->addModal("kredit", "chooser-layanan-kredit_layanan-Kode Kredit", "Kode Kredit", "","y",NULL,true,NULL,false);
$layanan->getAdapter()
        ->add("Nama", "nama")
        ->add("Slug", "slug")
        ->add("Keterangan", "keterangan")
        ->add("Kode Debet", "debet")
        ->add("Kode Kredit", "kredit");
$layanan->getModal()
        ->setTitle("Layanan");
        
if($layanan->getDBResponder()->isPreload() || $_POST['super_command']!=""  ){
    if(strpos($_POST['super_command'],"debet")!==false || strpos($_POST['super_command'],"kredit")!==false ){
        $uitable   = new Table(array('Nomor','Nama'),"");
        $uitable   ->setName($_POST['super_command'])
                   ->setModel(Table::$SELECT);
        $adapter   = new SimpleAdapter();
        $adapter   ->add("Nomor","nomor")
                   ->add("Nama","nama");
        $responder = new ServiceResponder($db, $uitable, $adapter, "get_account", "accounting");
        $layanan   ->getSuperCommand()
                   ->addResponder($_POST['super_command'],$responder);
        $layanan   ->addSuperCommand($_POST['super_command'],array())
                   ->addSuperCommandArray($_POST['super_command'],substr($_POST['super_command'],0,5),"nomor");
    }
}

if($layanan->getDBResponder()->isPreload() && $_POST['super_command']==""  ){
    $serv = new ServiceConsumer($db,"get_jenis_patient",NULL,"registration");
    $serv ->execute();
    $cons = $serv->getContent();
    global $wpdb;
    foreach($cons as $x){
        require_once "smis-libs-class/DBCreator.php";
        $dbcreator = new DBCreator($wpdb,"smis_fst_layanan",DBCreator::$ENGINE_MYISAM);
        $dbcreator ->addColumn("debet_".$x['value'], "varchar(64)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
        $dbcreator ->addColumn("kredit_".$x['value'], "varchar(64)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
        $dbcreator ->initialize();
        $uitable    = new Table(array('Kode Debet','Kode Kredit'),"");
        $uitable   ->addModal("debet_".$x['value'], "chooser-layanan-debet_layanan_".$x['value']."-Kode Debet ".$x['name'], "Kode Debet ".$x['name'], "","y",NULL,true,NULL,false);
        $uitable   ->addModal("kredit_".$x['value'], "chooser-layanan-kredit_layanan_".$x['value']."-Kode Kredit".$x['name'], "Kode Kredit ".$x['name'], "","y",NULL,true,NULL,false);
        $layanan   ->addSuperCommand("debet_layanan_".$x['value'],array())
                   ->addSuperCommandArray("debet_layanan_".$x['value'],"debet_".$x['value'],"nomor");
        $layanan   ->addSuperCommand("kredit_layanan_".$x['value'],array())
                   ->addSuperCommandArray("kredit_layanan_".$x['value'],"kredit_".$x['value'],"nomor");
    }    
    $layanan  ->addSuperCommand("debet_layanan",array())
              ->addSuperCommandArray("debet_layanan","debet","nomor");
    $layanan  ->addSuperCommand("kredit_layanan",array())
              ->addSuperCommandArray("kredit_layanan","kredit","nomor");
}

$layanan->setModalComponentSize(Modal::$MEDIUM);
$layanan->initialize();
?>