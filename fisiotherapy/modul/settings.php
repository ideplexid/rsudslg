<?php
    require_once ("smis-base/smis-include-service-consumer.php");
	require_once 'smis-framework/smis/api/SettingsBuilder.php';
	global $db;	
	$smis   = new SettingsBuilder($db,"rad_settings","fisiotherapy","settings" );
	$smis   ->setShowDescription(true )
            ->setTabulatorMode(Tabulator::$POTRAIT )
            ->addTabs("fisiotherapy","Setting System" )
            ->addTabs("akunting","Akunting" );
	$option = new OptionBuilder();
	$option ->addSingle("Stand Alone")
            ->addSingle("Integrated");
	
	$smis->addItem("fisiotherapy",new SettingsItem($db,"fisiotherapy-sistem-model","Model System",$option->getContent(),"select","System Model Fisiotherapy untuk persiapan keseluruhan,ketika Stand Alone berarti sendiri - sendiri,ketika Integrated berarti terintegrasi" ) );
	$smis->addItem("fisiotherapy",new SettingsItem($db,"fisiotherapy-ui-model","Show UI Simple Model","0","checkbox","Tampilkan Model UI Simple" ) );
    $smis->addItem ( "fisiotherapy", new SettingsItem ( $db, "fisiotherapy-edit-tanggal", "Bisa Edit Tanggal", "0", "checkbox", "jika di centang maka bisa edit tanggal" ) );

    $smis->addItem("fisiotherapy",new SettingsItem($db,"fisiotherapy-add-button","Add Button Enabled","0","checkbox","Tetap Tampilkan Tombol Tambah Meskipun Telah Terintegrasi" ) );
	$smis->addItem("fisiotherapy",new SettingsItem($db,"fisiotherapy-show-gender","Tampilkan Gender","1","checkbox","Tampilkan Gender" ) );
	$smis->addItem("fisiotherapy",new SettingsItem($db,"fisiotherapy-show-discount","Tampilkan Discount","1","checkbox","Tampilkan Diskon" ) );
	$smis->addItem("fisiotherapy",new SettingsItem($db,"fisiotherapy-show-diagnosa","Tampilkan Diagnosa","1","checkbox","Tampilkan Diagnosa" ) );
    $smis->addItem("fisiotherapy",new SettingsItem($db,"fisiotherapy-show-marketing","Tampilkan Marketing","0","checkbox","Tampilkan Marketing" ) );
	$smis->addItem("fisiotherapy",new SettingsItem($db,"fisiotherapy-show-diam","Tampilkan Nyeri Diam","1","checkbox","Tampilkan Nyeri Diam" ) );
	$smis->addItem("fisiotherapy",new SettingsItem($db,"fisiotherapy-show-gerak","Tampilkan Nyeri Gerak","1","checkbox","Tampilkan Nyeri Gerak" ) );
	$smis->addItem("fisiotherapy",new SettingsItem($db,"fisiotherapy-show-tekan","Tampilkan Nyeri Tekan","1","checkbox","Tampilkan Nyeri Tekan" ) );
	$smis->addItem("fisiotherapy",new SettingsItem($db,"fisiotherapy-show-aged","Tampilkan Umur","1","checkbox","Tampilkan Umur" ) );	
	$smis->addItem("fisiotherapy",new SettingsItem($db,"fisiotherapy-show-shift","Tampilkan Shift","1","checkbox","Tampilkan Shift" ) );	
	$smis->addItem("fisiotherapy",new SettingsItem($db,"fisiotherapy-show-dokter-rehab","Tampilkan Dokter Rehab","1","checkbox","Mengaktifkan Dokter Rehab Medis" ) );
	$smis->addItem("fisiotherapy",new SettingsItem($db,"fisiotherapy-show-asuhan-fisio","Tampilkan Asuhan Fisio","0","checkbox","Mengaktifkan Asuhan Fisiotherapy" ) );
	$smis->addItem("fisiotherapy",new SettingsItem($db,"fisiotherapy-name","Nama Fisiotherapy","","text","Nama Fisitoherapy di Cetakan" ) );

    $smis->addItem("akunting",new SettingsItem($db,"fisiotherapy-accounting-auto-notif","Aktifkan Setting Auto Notif ke Accounting","0","checkbox","Jika Dicentang Maka Sistem Akan Menotifikasi ke Accounting Secara Otomatis" ) );
    $smis->addItem("akunting",new SettingsItem($db,"fisiotherapy-accounting-debit-global","Kode Accounting Debit untuk Global (Kwitansi Simple)","","chooser-settings-debet_global-Debet","Kode Accounting Debit untuk Global (Kwitansi Simple)" ) );
    $smis->addSuperCommandAction("debet_global","kode_akun");
    $smis->addSuperCommandArray("debet_global","fisiotherapy-accounting-debit-global","nomor",true);
    
    $smis->addItem("akunting",new SettingsItem($db,"fisiotherapy-accounting-kredit-global","Kode Accounting Kredit untuk Global(Kwitansi Simple)","","chooser-settings-kredit_global-Kredit","Kode Accounting Kredit untuk Global(Kwitansi Simple)" ));
    $smis->addSuperCommandAction("kredit_global","kode_akun");
    $smis->addSuperCommandArray("kredit_global","fisiotherapy-accounting-kredit-global","nomor",true);
    
    $serv = new ServiceConsumer($db,"get_jenis_patient",NULL,"registration");
    $serv ->execute();
    $cons = $serv->getContent();
    foreach($cons as $x){
        $smis->addItem ( "akunting", new SettingsItem ( $db, "fisiotherapy-accounting-debit-global-".$x['value'], "Kode Accounting Debit ".$x['name']." untuk Global (Kwitansi Simple)", "", "chooser-settings-debet_global_".$x['value']."-Debet ".$x['name'], "Kode Accounting Debit ".$x['name']." untuk Global (Kwitansi Simple)" ) ); 
        $smis->addItem ( "akunting", new SettingsItem ( $db, "fisiotherapy-accounting-kredit-global-".$x['value'], "Kode Accounting Kredit ".$x['name']." untuk Global (Kwitansi Simple)", "", "chooser-settings-kredit_global_".$x['value']."-Kredit ".$x['name'], "Kode Accounting Kredit ".$x['name']." untuk Global (Kwitansi Simple)" ) );
        $smis->addSuperCommandAction("debet_global_".$x['value'],"kode_akun");
        $smis->addSuperCommandAction("kredit_global_".$x['value'],"kode_akun");
        $smis->addSuperCommandArray("debet_global_".$x['value'],"fisiotherapy-accounting-debit-global-".$x['value'],"nomor",true);
        $smis->addSuperCommandArray("kredit_global_".$x['value'],"fisiotherapy-accounting-kredit-global-".$x['value'],"nomor",true);
    }

    
    require_once "smis-base/smis-include-service-consumer.php";
    $serv=new ServiceConsumer($db,"get_kelas",null,"manajemen");
    $serv->execute();
    $opt_kelas=new OptionBuilder();
    $opt_kelas->add("","",1);
    $jns=$serv->getContent();
    foreach($jns as $x){
        $opt_kelas->add($x['nama'],$x['slug'],0);
    }
    $smis->addItem("fisiotherapy",new SettingsItem($db,"fisiotherapy-ui-pemeriksaan-default-kelas","Default Kelas Pasien",$opt_kelas->getContent(),"select","Default Kelas Pasien pada Fisiotherapy" ) );
	$smis->addItem ( "fisiotherapy", new SettingsItem ( $db, "fisiotherapy-aktifkan-tutup-tagihan", "Aktifkan Tutup Tagihan", "0", "checkbox", "mengaktifkan tutup tagihan" ) );
    
    
    $response = $smis->init ();
?>