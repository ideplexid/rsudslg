<?php 
require_once 'smis-libs-class/MasterSlaveTemplate.php';
require_once 'smis-base/smis-include-service-consumer.php';
require_once 'fisiotherapy/class/RuanganService.php';
global $db;


$button=new Button("", "", "");
$button->setAction("laporan_detail.view()");
$button->setClass("btn-primary");
$button->setIsButton(Button::$ICONIC);
$button->setIcon("fa fa-refresh");

$cetak=new Button("", "", "");
$cetak->setAction("laporan_detail.print()");
$cetak->setClass("btn-primary");
$cetak->setIsButton(Button::$ICONIC);
$cetak->setIcon("fa fa-print");

$laporan_detail=new MasterSlaveTemplate($db, "smis_fst_pesanan", "fisiotherapy", "laporan_detail");
$laporan_detail->getDBtable()->setOrder(" tanggal ASC");
$uitable=$laporan_detail->getUItable();
$uitable->setAction(false);
if(isset($_POST['dari']) && $_POST['dari']!="" && isset($_POST['sampai']) && $_POST['sampai']!=""){
	$laporan_detail ->getDBtable()
					->setShowAll(true)
					->addCustomKriteria(NULL, "tanggal>='".$_POST['dari']."'")
					->addCustomKriteria(NULL, "tanggal<'".$_POST['sampai']."'");
			
	if($_POST['urji']!="") 		$laporan_detail->getDBtable()->addCustomKriteria("uri","='".$_POST['urji']."'");
	if($_POST['shift']!="") 	$laporan_detail->getDBtable()->addCustomKriteria("shift","='".$_POST['shift']."'");
	if($_POST['jk']!="") 		$laporan_detail->getDBtable()->addCustomKriteria("jk","='".$_POST['jk']."'");
	if($_POST['ruangan']!="") 	$laporan_detail->getDBtable()->addCustomKriteria("ruangan","='".$_POST['ruangan']."'");
	if($_POST['kelas']!="") 	$laporan_detail->getDBtable()->addCustomKriteria("kelas","='".$_POST['kelas']."'");
	if($_POST['carabayar']!="") $laporan_detail->getDBtable()->addCustomKriteria("carabayar","='".$_POST['carabayar']."'");
	if($_POST['diagnosa']!="")  {
		if($_POST['diagnosa']=="kosong") 
			$laporan_detail->getDBtable()->addCustomKriteria("diagnosa","=''");
		else $laporan_detail->getDBtable()->addCustomKriteria("diagnosa","!=''");
	}
	
}

$header=array("No.","Tanggal","Nama","NRM","No Reg","Kelas","URI/URJ","Ruangan","Biaya","Jenis Pasien","Kelamin","Diagnosa","Shift");
$uitable->setHeader($header);
$adapter=new SummaryAdapter();
$adapter->addFixValue("Ruangan", "<strong>Total</strong>");
$adapter->addSummary("Biaya", "biaya","money Rp.");
$adapter->setUseNumber(true, "No.","back.");
$adapter->add("Tanggal", "tanggal","date d M Y")
		->add("Nama", "nama_pasien")
		->add("NRM", "nrm_pasien","only-digit8")
		->add("No Reg", "noreg_pasien","only-digit8")
		->add("Kelas", "kelas","unslug")
		->add("Ruangan", "ruangan","unslug")
		->add("Jenis Pasien", "carabayar","unslug")
		->add("Kelamin", "jk","trivial_0_Laki-laki_Perempuan")
		->add("URI/URJ", "uri","trivial_0_URJ_URI")
		->add("Diagnosa", "diagnosa")
		->add("Shift", "shift")
		->add("Biaya", "biaya","money Rp.");
$laporan_detail->setAdapter($adapter);
$laporan_detail ->addViewData("dari", "dari")
				->addViewData("sampai", "sampai")
				->addViewData("shift","shift")
				->addViewData("jk","jk")
				->addViewData("urji","urji")
				->addViewData("kelas","kelas")
				->addViewData("diagnosa","diagnosa")
				->addViewData("ruangan","ruangan")
				->addViewData("carabayar","carabayar");

if($laporan_detail->getDBResponder()->isPreload()){

	$jk=new OptionBuilder();
	$jk->add("","","1");
	$jk->add("Laki-Laki","0","0");
	$jk->add("Perempuan","1","0");

	$urji=new OptionBuilder();
	$urji->add("","","1");
	$urji->add("Rawat Inap","1","0");
	$urji->add("Rawat Jalan","0","0");

	$shift=new OptionBuilder();
	$shift->add("","","1");
	$shift->add("Pagi","pagi","0");
	$shift->add("Sore","sore","0");
	$shift->add("Malam","malam","0");

	$diagnosa=new OptionBuilder();
	$diagnosa->add("","","1");
	$diagnosa->add("Kosong","kosong","0");
	$diagnosa->add("Terisi","terisi","0");
	
	
	$service = new ServiceConsumer ( $db, "get_kelas" );
    $service->setCached(true,"get_kelas");
	$service->execute ();
	$kelas = $service->getContent ();
	$option_kelas = new OptionBuilder ();
	$option_kelas->add("","","1");
	foreach ( $kelas as $k ) {
		$nama = $k ['nama'];
		$slug = $k ['slug'];
		$option_kelas->add ( $nama, $slug);
	}
	$service = new RuanganService ( $db );
	$service->execute ();
	$ruangan = $service->getContent ();
	$ruangan [] = array ("name" => "PENDAFTARAN","value" => "Pendaftaran" );
	$ruangan [] = array ("name" => "","value" => "","default"=>"1" );

	$service = new ServiceConsumer ( $db, "get_jenis_patient",NULL,"registration" );
	$service->execute ();
	$jenis_pasien = $service->getContent ();
	$cbayar=new OptionBuilder();
	$cbayar->add("","","1");
	foreach($jenis_pasien as $jp){
		$cbayar->add($jp['name'],$jp['value']);
	}

	$laporan_detail ->addFlag("dari", "Tanggal Awal", "Silakan Masukan Tanggal Mulai")
					->addFlag("sampai", "Tanggal Akhir", "Silakan Masukan Tanggal Akhir")
					->addNoClear("dari")
					->addNoClear("sampai")
					->addNoClear("shift")
					->addNoClear("jk")
					->addNoClear("urji")
					->addNoClear("kelas")
					->addNoClear("ruangan")
					->addNoClear("carabayar")
					->addNoClear("diagnosa")
					->setDateEnable(true)
					->getUItable()
					->setActionEnable(false)
					->setFooterVisible(false)
					->addModal("dari", "date", "Awal", "")
					->addModal("sampai", "date", "Akhir", "")
					->addModal("shift", "select", "Shift", $shift->getContent())
					->addModal("diagnosa", "select", "Diagnosa", $diagnosa->getContent())
					->addModal("jk", "select", "Jenis Kelamin", $jk->getContent())
					->addModal("urji", "select", "URI/URJ", $urji->getContent())
					->addModal("kelas", "select", "Kelas", $option_kelas->getContent())
					->addModal("ruangan", "select", "Ruangan", $ruangan)
					->addModal("carabayar", "select", "Jenis Pasien", $cbayar->getContent());
	$laporan_detail->getForm()->addElement("", $button)->addElement("", $cetak);		
}

$laporan_detail->initialize();
?>