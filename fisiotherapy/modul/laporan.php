<?php 
global $db;
global $user;

if(isset($_POST['super_command']) && $_POST['super_command']=="total") {
	$rtable=new DBTable($db, "smis_fst_pesanan");
	$rtable->addCustomKriteria(NULL, "tanggal>='".$_POST['dari']."'");
	$rtable->addCustomKriteria(NULL, "tanggal<'".$_POST['sampai']."'");
	$qry=$rtable->getQueryCount("");
	$total=$db->get_var($qry);
	
	$query="truncate smis_fst_report";
	$db->query($query);
	$query=$rtable->getQueryCount("");
	$content=$db->get_var($query);
	
	$query="INSERT INTO smis_fst_report (urutan,grup,item,jumlah) VALUES (0,'RESUME','JUMLAH PASIEN','".$content."')  ON DUPLICATE KEY UPDATE jumlah=jumlah+1;";
	$db->query($query);
	
	$pack=new ResponsePackage();
	$pack->setContent($content);
	$pack->setStatus(ResponsePackage::$STATUS_OK);
	echo json_encode($pack->getPackage());
	return;
}

$header=array("No.","Item","Jumlah");
$uitable=new Table($header);
if(isset($_POST['super_command']) && $_POST['super_command']=="limit") {
	require_once 'radiology/adapter/RadiologySalaryCounter.php';	
	$rtable=new DBTable($db, "smis_fst_pesanan");
	$rtable->setFetchMethode(DBTable::$ARRAY_FETCH);
	$view=array("id","periksa","nama_dokter","kelas","ruangan","carabayar","umur","uri","jk","barulama","shift");
	$rtable->setView($view);
	$rtable->addCustomKriteria(NULL, "tanggal>='".$_POST['dari']."'");
	$rtable->addCustomKriteria(NULL, "tanggal<'".$_POST['sampai']."'");
	$rtable->setMaximum(1);
	$one=$rtable->view("", $_POST['limit_start']);	
	$svx=$one['data'][0];
	
	$periksa=json_decode($svx['periksa']);
	foreach($periksa as $name=>$value){
		if($value==1 || $value=="1"){
			$name=ArrayAdapter::format("unslug", $name);
			$query="INSERT INTO smis_fst_report (urutan,grup,item,jumlah) VALUES (5,'MODALITAS','".$name."','1')  ON DUPLICATE KEY UPDATE jumlah=jumlah+1;";
			$db->query($query);
		}
	}
	
	$name=$svx['nama_dokter'];
	$query="INSERT INTO smis_fst_report (urutan,grup,item,jumlah) VALUES (6,'PENGIRIM','".$name."','1')  ON DUPLICATE KEY UPDATE jumlah=jumlah+1;";
	$db->query($query);
	
	$name=ArrayAdapter::format("unslug", $svx['kelas']);
	$query="INSERT INTO smis_fst_report (urutan,grup,item,jumlah) VALUES (7,'KELAS','".$name."','1')  ON DUPLICATE KEY UPDATE jumlah=jumlah+1;";
	$db->query($query);
	
	$name=ArrayAdapter::format("unslug", $svx['ruangan']);
	$query="INSERT INTO smis_fst_report (urutan,grup,item,jumlah) VALUES (8,'RUANG ASAL','".$name."','1')  ON DUPLICATE KEY UPDATE jumlah=jumlah+1;";
	$db->query($query);
	
	$name=ArrayAdapter::format("unslug", $svx['carabayar']);
	$query="INSERT INTO smis_fst_report (urutan,grup,item,jumlah) VALUES (9,'JENIS PASIEN','".$name."','1')  ON DUPLICATE KEY UPDATE jumlah=jumlah+1;";
	$db->query($query);
	
	
	$umur=$svx['umur'];
	
	$pos=strpos($umur, "Tahun");
	if($pos!==false && $umur!=""){
		$umur=substr($umur,0,$pos)*1;
	}
	
	$pos=strpos($umur, ".");
	if($pos!==false && $umur!=""){
		$umur=substr($umur,0,$pos)*1;
	}
	
	$pos=strpos($umur, ",");
	if($pos!==false && $umur!=""){
		$umur=substr($umur,0,$pos)*1;
	}
	
	$pos=strpos($umur, "th");
	if($pos!==false && $umur!=""){
		$umur=substr($umur,0,$pos)*1;
	}
	
	$pos=strpos($umur, "thn");
	if($pos!==false && $umur!=""){
		$umur=substr($umur,0,$pos)*1;
	}
	
	$umur=($umur*1);
	$umur_name=">40 Tahun";
	if($umur=="" || $umur==0 || $umur<=5) $umur_name="0-5 Tahun";
	if($umur>=6) $umur_name="6-19 Tahun";
	if($umur>=20) $umur_name="20-40 Tahun";
	if($umur>40) $umur_name=">40 Tahun";
	$svx['ujadi2']=$umur_name;
	$query="INSERT INTO smis_fst_report (urutan,grup,item,jumlah) VALUES (10,'UMUR PASIEN','".$umur_name."','1')  ON DUPLICATE KEY UPDATE jumlah=jumlah+1;";
	$db->query($query);
	
	$name=$svx['uri']=="0"?"URJ":"URI";
	$query="INSERT INTO smis_fst_report (urutan,grup,item,jumlah) VALUES (1,'RESUME','".$name."','1')  ON DUPLICATE KEY UPDATE jumlah=jumlah+1;";
	$db->query($query);
	
	$name=$svx['jk']=="0"?"LAKI-LAKI":"PEREMPUAN";
	$query="INSERT INTO smis_fst_report (urutan,grup,item,jumlah) VALUES (2,'RESUME','".$name."','1')  ON DUPLICATE KEY UPDATE jumlah=jumlah+1;";
	$db->query($query);
	
	$name=$svx['barulama']=="0"?"PASIEN BARU":"PASIEN LAMA";
	$query="INSERT INTO smis_fst_report (urutan,grup,item,jumlah) VALUES (3,'RESUME','".$name."','1')  ON DUPLICATE KEY UPDATE jumlah=jumlah+1;";
	$db->query($query);
	
	$name=ArrayAdapter::format("unslug", $svx['shift']);
	$query="INSERT INTO smis_fst_report (urutan,grup,item,jumlah) VALUES (11,'SHIFT','".$name."','1')  ON DUPLICATE KEY UPDATE jumlah=jumlah+1;";
	$db->query($query);
	
	
	
	$pack=new ResponsePackage();
	$pack->setContent($svx);
	$pack->setStatus(ResponsePackage::$STATUS_OK);
	echo json_encode($pack->getPackage());
	return;
}



$uitable->setName("laporan")
		->setActionEnable(false)
		->setFooterVisible(false);

if(isset($_POST['command']) && $_POST['command']== "list") {	
	loadLibrary("smis-libs-function-math");
	$dbtable=new DBTable($db, "smis_fst_report");
	$dbtable->setFetchMethode(DBTable::$ARRAY_FETCH);
	$dbtable->setShowAll(true);
	$dbtable->setOrder(" urutan ASC , item ASC ");
	$data=$dbtable->view("", "0");
	require_once 'fisiotherapy/class/adapter/FisiotherapyLaporanAdapter.php';
	$adapter=new FisiotherapyLaporanAdapter(true,"No.");
	$adapter->add("Item", "item");
	$adapter->add("Jumlah", "jumlah","back Pasien");
	$adapter->add("Grup", "grup");
	$uidata=$adapter->getContent($data["data"]);
	
	$list=$uitable->setContent($uidata)
				  ->getBodyContent();
	
	$json['list']=$list;
	$json['pagination']="";
	$json['number']="0";
	$json['number_p']="0";
	$json['data']=$uidata;
	$json['waktu']=ArrayAdapter::format("date d M Y H:i:s", date("Y-m-d H:i:s"));
	$json['nomor']="RFIS-".date("dmy-his")."-".substr(md5($user->getNameOnly()), 5,rand(2, 5));
	$ft=ArrayAdapter::format("date d M Y", $_POST['dari']);
	$to=ArrayAdapter::format("date d M Y", $_POST['sampai']);	
	$json['json']=json_encode($uidata);
	$json['status']=1;
	
	$pack=new ResponsePackage();
	$pack->setContent($json);
	$pack->setStatus(ResponsePackage::$STATUS_OK);
	echo json_encode($pack->getPackage());
	return;
}

$uitable->addModal("dari", "date", "Dari", "")
		->addModal("sampai", "date", "Sampai", "");
$action=new Button("","","View");
$action->setClass("btn-primary")
	   ->setIsButton(Button::$ICONIC)
	   ->setIcon("fa fa-circle-o-notch")
	   ->setAction("laporan.rekaptotal()");
$print=new Button("","","View");
$print->setClass("btn-primary")
		->setIsButton(Button::$ICONIC)
		->setIcon("fa fa-print")
		->setAction("laporan.print()");


$btn_froup=new ButtonGroup("");
$btn_froup->addButton($action);
$btn_froup->addButton($print);
$form=$uitable
	  ->getModal()
	  ->setTitle("fisiotherapy")
	  ->getForm()
	  ->addElement("",$btn_froup);

$close=new Button("", "", "Batal");
$close	->addClass("btn-primary")
		->setIsButton(Button::$ICONIC_TEXT)
		->setIcon("fa fa-close")
		->setAction("laporan.batal()");

$load=new LoadingBar("rekap_rad_bar", "");
$modal=new Modal("rekap_fis_modal", "", "Processing...");
$modal	->addHTML($load->getHtml(),"after")
		->addFooter($close);

echo $form->getHtml();
echo "<div class='clear'></div>";
echo "<div id='result_laporan'>".$uitable->getHtml()."</div>";
echo $modal->getHtml();
echo addJS("framework/bootstrap/js/bootstrap-datepicker.js");
echo addJS("framework/smis/js/table_action.js");
echo addCSS("framework/bootstrap/css/datepicker.css");
echo addJS ( "base-js/smis-base-loading.js");
echo addJS ( "fisiotherapy/js/laporan.js",false);
?>
