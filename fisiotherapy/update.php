<?php
global $wpdb;
global $NUMBER;
if($NUMBER<2){
	$wpdb->query ( "ALTER TABLE  `smis_fst_pesanan` ADD  `selesai` INT NOT NULL AFTER  `pembagian`" );
}

if($NUMBER<3){
	$query="ALTER TABLE  `smis_fst_pesanan` ADD  `nyeri_diam` VARCHAR( 128 ) NOT NULL AFTER  `periksa` ,
			ADD  `nyeri_gerak` VARCHAR( 128 ) NOT NULL AFTER  `nyeri_diam` ,
			ADD  `nyeri_tekan` VARCHAR( 128 ) NOT NULL AFTER  `nyeri_gerak`";
	$wpdb->query($query);
}

if($NUMBER<4){
	$query="ALTER TABLE  `smis_fst_pesanan` ADD  `uri` BOOLEAN NOT NULL AFTER  `selesai`";
	$wpdb->query($query);
	$query="ALTER TABLE  `smis_fst_pesanan` ADD  `jk` BOOLEAN NOT NULL AFTER  `uri` ,
			ADD  `umur` VARCHAR( 64 ) NOT NULL AFTER  `jk` ,
			ADD  `carabayar` VARCHAR( 32 ) NOT NULL AFTER  `umur`";
	$wpdb->query($query);
}

if($NUMBER<5){
	$query="CREATE TABLE IF NOT EXISTS `smis_fst_layanan` (
		  `id` int(11) NOT NULL AUTO_INCREMENT,
		  `nama` varchar(64) NOT NULL,
		  `slug` varchar(32) NOT NULL,
		  `keterangan` varchar(64) NOT NULL,
		  `prop` varchar(10) NOT NULL,
		  PRIMARY KEY (`id`)
		) ENGINE=InnoDB ";
	$wpdb->query ( $query );	
}

if($NUMBER<6){
	$query="
	ALTER TABLE  `smis_fst_pesanan` ADD  `diskon` INT NOT NULL AFTER  `hasil` ,
									ADD  `harga_asal` INT NOT NULL AFTER  `diskon` ,
									ADD  `keterangan` VARCHAR( 32 ) NOT NULL AFTER  `harga_asal` ;";
	$wpdb->query ( $query );
}

if($NUMBER<7){
	$query="ALTER TABLE  `smis_fst_pesanan` ADD  `diagnosa` VARCHAR( 128 ) NOT NULL AFTER  `carabayar` ;";
	$wpdb->query ( $query );
}

if($NUMBER<8){	
	$query="ALTER TABLE  `smis_fst_pesanan` ADD  `barulama` BOOLEAN NOT NULL COMMENT  'baru adalah 0, lama 1' AFTER  `diagnosa` ;";
	$wpdb->query ( $query );
	
	$query="
	CREATE TABLE IF NOT EXISTS `smis_fst_report` (
	  `id` int(11) NOT NULL AUTO_INCREMENT,
	  `urutan` int(11) NOT NULL,
	  `grup` varchar(32) NOT NULL,
	  `item` varchar(32) NOT NULL,
	  `jumlah` int(11) NOT NULL,
	  `prop` varchar(10) NOT NULL,
	  PRIMARY KEY (`id`),
	  UNIQUE KEY `item` (`item`)
	) ENGINE=InnoDB;";
	$wpdb->query ( $query );	
}

if($NUMBER<10){
	$query="ALTER TABLE  `smis_fst_pesanan` ADD  `shift` VARCHAR( 8 ) NOT NULL AFTER  `barulama` ;";
	$wpdb->query ( $query );
}

if($NUMBER<11){
	$query="ALTER TABLE  `smis_fst_pesanan` ADD  `id_dokter_rhb` INT NOT NULL AFTER  `shift` ,
	ADD  `nama_dokter_rhb` VARCHAR( 32 ) NOT NULL AFTER  `id_dokter_rhb` ,
	ADD  `biaya_dokter_rhb` INT NOT NULL AFTER  `nama_dokter_rhb` ;";
	$wpdb->query ( $query );
}

if($NUMBER<12){
	$query="CREATE TABLE `smis_fst_pertindakan` (
			  `id` int(11) NOT NULL AUTO_INCREMENT,
			  `nama` varchar(64) NOT NULL,
			  `kelas` varchar(32) NOT NULL,
			  `total` int(11) NOT NULL,
			  `harga_satuan` int(11) NOT NULL,
			  `harga_total` int(11) NOT NULL,
			  `prop` varchar(10) NOT NULL,
			  PRIMARY KEY (`id`),
			  UNIQUE KEY `nama` (`nama`,`kelas`,`harga_satuan`)
			) ENGINE=InnoDB;";
	$wpdb->query($query);
}

if($NUMBER<13){
	$query="ALTER TABLE `smis_fst_pesanan` ADD `asuhan_fisio` INT NOT NULL AFTER `biaya_dokter_rhb`;";
	$wpdb->query($query);
}

if($NUMBER<17){
	$query="ALTER TABLE  `smis_fst_layanan` ADD  `debet` varchar(64) NOT NULL AFTER  `keterangan` ,
	ADD  `kredit` varchar(64) NOT NULL AFTER  `keterangan` ;";
	$wpdb->query ( $query );
    
    $query="ALTER TABLE `smis_fst_pesanan` ADD `akunting` tinyint(1) NOT NULL AFTER `asuhan_fisio`;";
	$wpdb->query($query);
}


require_once "fisiotherapy/resource/install/table/smis_fst_rl52.php";
?>