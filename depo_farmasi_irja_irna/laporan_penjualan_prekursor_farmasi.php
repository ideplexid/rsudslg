<?php 
	require_once("smis-base/smis-include-service-consumer.php");
	require_once("depo_farmasi_irja_irna/library/InventoryLibrary.php");
	global $db;
	
	$laporan_form = new Form("lppf_form", "", "Depo Depo Farmasi IRJA - IRNA : Laporan Penjualan Perkursor Farmasi");
	$tanggal_from_text = new Text("lppf_tanggal_from", "lppf_tanggal_from", date("Y-m-") . "01 00:00");
	$tanggal_from_text->setClass("mydatetime");
	$tanggal_from_text->setAtribute("data-date-format='yyyy-mm-dd hh:ii'");
	$laporan_form->addElement("Waktu Awal", $tanggal_from_text);
	$tanggal_to_text = new Text("lppf_tanggal_to", "lppf_tanggal_to", date("Y-m-d H:i"));
	$tanggal_to_text->setClass("mydatetime");
	$tanggal_to_text->setAtribute("data-date-format='yyyy-mm-dd hh:ii'");
	$laporan_form->addElement("Waktu Akhir", $tanggal_to_text);
	$show_button = new Button("", "", "Tampilkan");
	$show_button->setClass("btn-primary");
	$show_button->setIcon("icon-white icon-repeat");
	$show_button->setIsButton(Button::$ICONIC);
	$show_button->setAction("lppf.view()");
	$print_button = new Button("", "", "Cetak");
	$print_button->setClass("btn-inverse");
	$print_button->setIcon("fa fa-download");
	$print_button->setIsButton(Button::$ICONIC);
	$print_button->setAction("lppf.export_xls()");
	$btn_group = new ButtonGroup("noprint");
	$btn_group->addButton($show_button);
	$btn_group->addButton($print_button);
	$laporan_form->addElement("", $btn_group);
	
	$lppf_table = new Table(
		array("No.", "Obat", "Jenis Obat", "Jumlah", "Satuan", "Tanggal", "No. RM", "Nama Pasien", "Alamat Pasien", "No. Resep", "Dokter"),
		"",
		null,
		true
	);
	$lppf_table->setName("lppf");
	$lppf_table->setAction(false);
	
	if (isset($_POST['command'])) {
		if ($_POST['command'] == "export_xls") {
			$tanggal_from = $_POST['from'];
			$tanggal_to = $_POST['to'];
			require_once("smis-libs-out/php-excel/PHPExcel.php");
			$objPHPExcel = PHPExcel_IOFactory::load("depo_farmasi_irja_irna/templates/template_laporan_prekursor_farmasi.xlsx");
			$objPHPExcel->setActiveSheetIndexByName("LAPORAN PREKURSOR FARMASI");
			$objWorksheet = $objPHPExcel->getActiveSheet();
			$objWorksheet->setCellValue("B3", "PERIODE : " . ArrayAdapter::format("date d-m-Y, H:i", $tanggal_from) . " s/d " . ArrayAdapter::format("date d-m-Y, H:i", $tanggal_to));
			$service_consumer = new ServiceConsumer(
				$db,
		        "get_okt_narkotika_prekursor_csv",
		         null,
		        "perencanaan"
			);
			$content = $service_consumer->execute()->getContent();
			$filter = "id_obat IN (
				" . $content[0]['prekursor_farmasi_csv'] . "
			) AND tanggal >= '" . $tanggal_from . "' AND tanggal <= '" . $tanggal_to . "'";
			$query_value = "
				SELECT *
				FROM (
					SELECT id_obat, nama_obat, nama_jenis_obat, SUM(jumlah) AS 'jumlah', satuan, tanggal, nrm_pasien, nama_pasien, nomor_resep, nama_dokter, alamat_pasien
					FROM (
						SELECT " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".id, " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".nomor_resep, " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".nrm_pasien, " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".nama_pasien, " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".alamat_pasien, " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".nama_dokter, " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".tanggal, " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".tipe, " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".total, " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".diskon, " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".t_diskon, " . InventoryLibrary::$_TBL_PENJUALAN_OBAT_JADI . ".id_obat, " . InventoryLibrary::$_TBL_PENJUALAN_OBAT_JADI . ".nama_obat, " . InventoryLibrary::$_TBL_PENJUALAN_OBAT_JADI . ".nama_jenis_obat, " . InventoryLibrary::$_TBL_PENJUALAN_OBAT_JADI . ".jumlah, " . InventoryLibrary::$_TBL_PENJUALAN_OBAT_JADI . ".satuan, " . InventoryLibrary::$_TBL_PENJUALAN_OBAT_JADI . ".harga, 'OBAT JADI' AS 'keterangan'
						FROM " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . " LEFT JOIN " . InventoryLibrary::$_TBL_PENJUALAN_OBAT_JADI . " ON " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".id = " . InventoryLibrary::$_TBL_PENJUALAN_OBAT_JADI . ".id_penjualan_resep
						WHERE " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".prop NOT LIKE 'del' AND " . InventoryLibrary::$_TBL_PENJUALAN_OBAT_JADI . ".prop NOT LIKE 'del' AND " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".dibatalkan = 0 AND " . InventoryLibrary::$_TBL_PENJUALAN_OBAT_JADI . ".jumlah > 0
						UNION ALL
						SELECT " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".id, " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".nomor_resep, " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".nrm_pasien, " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".nama_pasien, " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".alamat_pasien, " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".nama_dokter, " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".tanggal, " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".tipe, " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".total, " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".diskon, " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".t_diskon, " . InventoryLibrary::$_TBL_BAHAN_PAKAI_OBAT_RACIKAN . ".id_obat, " . InventoryLibrary::$_TBL_BAHAN_PAKAI_OBAT_RACIKAN . ".nama_obat, " . InventoryLibrary::$_TBL_BAHAN_PAKAI_OBAT_RACIKAN . ".nama_jenis_obat, " . InventoryLibrary::$_TBL_BAHAN_PAKAI_OBAT_RACIKAN . ".jumlah, " . InventoryLibrary::$_TBL_BAHAN_PAKAI_OBAT_RACIKAN . ".satuan, " . InventoryLibrary::$_TBL_BAHAN_PAKAI_OBAT_RACIKAN . ".harga, CONCAT('BAHAN RACIKAN ', " . InventoryLibrary::$_TBL_PENJUALAN_OBAT_RACIKAN . ".nama) AS 'keterangan'
						FROM (" . InventoryLibrary::$_TBL_PENJUALAN_RESEP . " LEFT JOIN " . InventoryLibrary::$_TBL_PENJUALAN_OBAT_RACIKAN . " ON " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".id = " . InventoryLibrary::$_TBL_PENJUALAN_OBAT_RACIKAN . ".id_penjualan_resep) LEFT JOIN " . InventoryLibrary::$_TBL_BAHAN_PAKAI_OBAT_RACIKAN . " ON " . InventoryLibrary::$_TBL_PENJUALAN_OBAT_RACIKAN . ".id = " . InventoryLibrary::$_TBL_BAHAN_PAKAI_OBAT_RACIKAN . ".id_penjualan_obat_racikan
						WHERE " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".prop NOT LIKE 'del' AND " . InventoryLibrary::$_TBL_PENJUALAN_OBAT_RACIKAN . ".prop NOT LIKE 'del' AND " . InventoryLibrary::$_TBL_BAHAN_PAKAI_OBAT_RACIKAN . ".prop NOT LIKE 'del' AND " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".dibatalkan = 0 AND " . InventoryLibrary::$_TBL_BAHAN_PAKAI_OBAT_RACIKAN . ".jumlah > 0
					) v_lpt
					WHERE " . $filter . "
					GROUP BY id_obat, satuan, nomor_resep
					ORDER BY CONVERT(SUBSTRING_INDEX(nomor_resep,'.',-1),UNSIGNED INTEGER), nama_obat, nama_jenis_obat ASC
				) v_laporan
			";
			$dbtable = new DBTable($db, "" . InventoryLibrary::$_TBL_PENJUALAN_RESEP . "");
			$data = $dbtable->get_result($query_value);
			if (count($data) - 3 > 0)
				$objWorksheet->insertNewRowBefore(8, count($data) - 3);
			$start_row_num = 7;
			$end_row_num = 7;
			$row_num = $start_row_num;
			$nomor = 1;
			foreach ($data as $d) {
				$col_num = 1;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $nomor++);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->nama_obat);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->nama_jenis_obat);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->jumlah);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->satuan);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->tanggal);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->nrm_pasien);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, strtoupper($d->nama_pasien));
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, strtoupper($d->alamat_pasien));
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->nomor_resep);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->nama_dokter);
				$objWorksheet->getStyle("E" . $row_num)->getNumberFormat()->setFormatCode("#,##0");
				$row_num++;
				$end_row_num++;
			}
			header("Content-type: application/vnd.ms-excel");	
			header("Content-Disposition: attachment; filename=LAPORAN_PREKURSOR_FARMASI_" . ArrayAdapter::format("date Ymd_Hi", $tanggal_from) . "_" . ArrayAdapter::format("date Ymd_Hi", $tanggal_to) . ".xlsx");
			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");
			$objWriter->save("php://output");
			return;
		}
		$lppf_adapter = new SimpleAdapter(true, "No.");
		$lppf_adapter->add("Obat", "nama_obat");
		$lppf_adapter->add("Jenis Obat", "nama_jenis_obat");
		$lppf_adapter->add("Jumlah", "jumlah", "number");
		$lppf_adapter->add("Satuan", "satuan");
		$lppf_adapter->add("Tanggal", "tanggal", "date d-m-Y");
		$lppf_adapter->add("No. RM", "nrm_pasien", "digit6");
		$lppf_adapter->add("Nama Pasien", "nama_pasien", "unslug");
		$lppf_adapter->add("Alamat Pasien", "alamat_pasien", "unslug");
		$lppf_adapter->add("No. Resep", "nomor_resep");
		$lppf_adapter->add("Dokter", "nama_dokter");
		$lppf_dbtable = new DBTable($db, "" . InventoryLibrary::$_TBL_PENJUALAN_RESEP . "");
		$service_consumer = new ServiceConsumer(
			$db,
	        "get_okt_narkotika_prekursor_csv",
	         null,
	        "perencanaan"
		);
		$content = $service_consumer->execute()->getContent();
		$filter = "id_obat IN (
			" . $content[0]['prekursor_farmasi_csv'] . "
		) AND tanggal >= '" . $_POST['tanggal_from'] . "' AND tanggal <= '" . $_POST['tanggal_to'] . "'";
		if (isset($_POST['kriteria'])) {
			$filter .= " AND (nama_obat LIKE '%" . $_POST['kriteria'] . "%' OR satuan LIKE '%" . $_POST['kriteria'] . "%') ";
		}
		$query_value = "
			SELECT *
			FROM (
				SELECT id_obat, nama_obat, nama_jenis_obat, SUM(jumlah) AS 'jumlah', satuan, tanggal, nrm_pasien, nama_pasien, nomor_resep, nama_dokter, alamat_pasien
				FROM (
					SELECT " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".id, " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".nomor_resep, " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".nrm_pasien, " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".nama_pasien, " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".alamat_pasien, " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".nama_dokter, " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".tanggal, " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".tipe, " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".total, " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".diskon, " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".t_diskon, " . InventoryLibrary::$_TBL_PENJUALAN_OBAT_JADI . ".id_obat, " . InventoryLibrary::$_TBL_PENJUALAN_OBAT_JADI . ".nama_obat, " . InventoryLibrary::$_TBL_PENJUALAN_OBAT_JADI . ".nama_jenis_obat, " . InventoryLibrary::$_TBL_PENJUALAN_OBAT_JADI . ".jumlah, " . InventoryLibrary::$_TBL_PENJUALAN_OBAT_JADI . ".satuan, " . InventoryLibrary::$_TBL_PENJUALAN_OBAT_JADI . ".harga, 'OBAT JADI' AS 'keterangan'
					FROM " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . " LEFT JOIN " . InventoryLibrary::$_TBL_PENJUALAN_OBAT_JADI . " ON " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".id = " . InventoryLibrary::$_TBL_PENJUALAN_OBAT_JADI . ".id_penjualan_resep
					WHERE " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".prop NOT LIKE 'del' AND " . InventoryLibrary::$_TBL_PENJUALAN_OBAT_JADI . ".prop NOT LIKE 'del' AND " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".dibatalkan = 0 AND " . InventoryLibrary::$_TBL_PENJUALAN_OBAT_JADI . ".jumlah > 0
					UNION ALL
					SELECT " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".id, " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".nomor_resep, " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".nrm_pasien, " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".nama_pasien, " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".alamat_pasien, " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".nama_dokter, " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".tanggal, " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".tipe, " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".total, " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".diskon, " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".t_diskon, " . InventoryLibrary::$_TBL_BAHAN_PAKAI_OBAT_RACIKAN . ".id_obat, " . InventoryLibrary::$_TBL_BAHAN_PAKAI_OBAT_RACIKAN . ".nama_obat, " . InventoryLibrary::$_TBL_BAHAN_PAKAI_OBAT_RACIKAN . ".nama_jenis_obat, " . InventoryLibrary::$_TBL_BAHAN_PAKAI_OBAT_RACIKAN . ".jumlah, " . InventoryLibrary::$_TBL_BAHAN_PAKAI_OBAT_RACIKAN . ".satuan, " . InventoryLibrary::$_TBL_BAHAN_PAKAI_OBAT_RACIKAN . ".harga, CONCAT('BAHAN RACIKAN ', " . InventoryLibrary::$_TBL_PENJUALAN_OBAT_RACIKAN . ".nama) AS 'keterangan'
					FROM (" . InventoryLibrary::$_TBL_PENJUALAN_RESEP . " LEFT JOIN " . InventoryLibrary::$_TBL_PENJUALAN_OBAT_RACIKAN . " ON " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".id = " . InventoryLibrary::$_TBL_PENJUALAN_OBAT_RACIKAN . ".id_penjualan_resep) LEFT JOIN " . InventoryLibrary::$_TBL_BAHAN_PAKAI_OBAT_RACIKAN . " ON " . InventoryLibrary::$_TBL_PENJUALAN_OBAT_RACIKAN . ".id = " . InventoryLibrary::$_TBL_BAHAN_PAKAI_OBAT_RACIKAN . ".id_penjualan_obat_racikan
					WHERE " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".prop NOT LIKE 'del' AND " . InventoryLibrary::$_TBL_PENJUALAN_OBAT_RACIKAN . ".prop NOT LIKE 'del' AND " . InventoryLibrary::$_TBL_BAHAN_PAKAI_OBAT_RACIKAN . ".prop NOT LIKE 'del' AND " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".dibatalkan = 0 AND " . InventoryLibrary::$_TBL_BAHAN_PAKAI_OBAT_RACIKAN . ".jumlah > 0
				) v_lpt
				WHERE " . $filter . "
				GROUP BY id_obat, satuan, nomor_resep
				ORDER BY CONVERT(SUBSTRING_INDEX(nomor_resep,'.',-1),UNSIGNED INTEGER), nama_obat, nama_jenis_obat ASC
			) v_laporan
		";
		$query_count = "
			SELECT COUNT(*)
			FROM (
				" . $query_value . "
			) v_laporan
		";
		$lppf_dbtable->setPreferredQuery(true, $query_value, $query_count);
		$lppf_dbresponder = new DBResponder(
			$lppf_dbtable,
			$lppf_table,
			$lppf_adapter
		);
		$data = $lppf_dbresponder->command($_POST['command']);
		echo json_encode($data);
		return;
	}
	
	echo $laporan_form->getHtml();
	echo "<div id='table_content'>";
	echo $lppf_table->getHtml();
	echo "</div>";
	echo addJS("framework/smis/js/table_action.js");
	echo addJS("framework/bootstrap/js/bootstrap-datetimepicker.js");
	echo addCSS("framework/bootstrap/css/bootstrap-datetimepicker.css");
?>
<script type="text/javascript">
	function LPPFAction(name, page, action, column) {
		this.initialize(name, page, action, column);
	}
	LPPFAction.prototype.constructor = LPPFAction;
	LPPFAction.prototype = new TableAction();
	LPPFAction.prototype.view = function() {
		var self = this;
		var data = this.getViewData();
		data['tanggal_from'] = $("#lppf_tanggal_from").val();
		data['tanggal_to'] = $("#lppf_tanggal_to").val();
		showLoading();
		$.post(
			"",
			data,
			function(response) {
				var json = getContent(response);
				if (json == null) return;
				$("#" + self.prefix + "_list").html(json.list);
				$("#" + self.prefix + "_pagination").html(json.pagination);
				dismissLoading();
			}
		);
	};
	LPPFAction.prototype.print = function() {
		if ($("#lppf_tanggal_from").val() == "" || $("#lppf_tanggal_to").val() == "")
			return;
		var data = this.getRegulerData();
		data['command'] = "print_laporan";
		data['from'] = $("#lppf_tanggal_from").val();
		data['to'] = $("#lppf_tanggal_to").val();
		$.post(
			"",
			data,
			function(response) {
				var json = getContent(response);
				if (json == null) return;
				smis_print(json);
			}
		);
	};
	LPPFAction.prototype.export_xls = function() {
		if ($("#lppf_tanggal_from").val() == "" || $("#lppf_tanggal_to").val() == "")
			return;
		showLoading();
		var data = this.getRegulerData();
		data['command'] = "export_xls";
		data['from'] = $("#lppf_tanggal_from").val();
		data['to'] = $("#lppf_tanggal_to").val();
		postForm(data);
		dismissLoading();
	};
	
	var lppf;
	$(document).ready(function() {
		$('[data-toggle="popover"]').popover({
			trigger: 'hover',
			'placement': 'top'
		});
		lppf = new LPPFAction(
			"lppf",
			"depo_farmasi_irja_irna",
			"laporan_penjualan_prekursor_farmasi",
			new Array()
		);
		lppf.view();
		$('.mydatetime').datetimepicker({ 
			minuteStep: 1
		});
	});
</script>