<?php
	class ReturPenjualanBebasAdapter extends ArrayAdapter {
		public function adapt($row) {
			$array = array();
			$array['id'] = $row->id;
			$array['dibatalkan'] = $row->dibatalkan;
			$array['tercetak'] = $row->tercetak;
			$array['tanggal'] = $row->tanggal;
			$array['Nomor'] = self::format("digit8", $row->id);
			$array['Tanggal/Jam'] = self::format("date d-m-Y H:i", $row->tanggal);
			$array['No. Penjualan'] = self::format("digit8", $row->id_penjualan_resep);
			if ($row->dibatalkan)
				$array['Status'] = "Dibatalkan";
			else
				$array['Status'] = "-";
			return $array;
		}
	}
?>