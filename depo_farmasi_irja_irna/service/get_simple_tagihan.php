<?php 
	require_once("smis-base/smis-include-service-consumer.php");
	require_once("depo_farmasi_irja_irna/library/InventoryLibrary.php");
	global $db;
	
	if (isset($_POST['noreg_pasien'])) {
		$noreg = $_POST['noreg_pasien'];
		$penjualan_resep_dbtable = new DBTable($db, InventoryLibrary::$_TBL_PENJUALAN_RESEP);
		$resep_rows = $penjualan_resep_dbtable->get_result("
			SELECT *
			FROM " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . "
			WHERE prop NOT LIKE 'del' AND noreg_pasien = '$noreg' AND tipe = 'resep' AND dibatalkan = '0'
		");
		
		$result = array();
		foreach($resep_rows as $rr) {
			$resep = array(
				'id' => $rr->id,
				'nama' 				=> "Resep No. $rr->nomor_resep",
				'waktu' 			=> ArrayAdapter::format("date d M Y", $rr->tanggal),
				'start' 			=> $rr->tanggal,
				'end' 				=> $rr->tanggal,
				'jumlah' 			=> 1,
				'biaya' 			=> $rr->total,
				'nama_dokter' 		=> $rr->nama_dokter,
				'id_dokter'			=> $rr->id_dokter,
				'jaspel_dokter'		=> 0,
				'urjigd'			=> $rr->uri == 1 ? "URI" : "URJ",
				'tanggal_tagihan'	=> ArrayAdapter::format("date Y-m-d", $rr->tanggal)
			);
			
			$ket = array(
				'dokter' => $rr->nama_dokter,
			);
			
			$diskon = -$rr->total;
			
			$obat_jadi_rows = $penjualan_resep_dbtable->get_result("
				SELECT *
				FROM " . InventoryLibrary::$_TBL_PENJUALAN_OBAT_JADI . "
				WHERE id_penjualan_resep = '$rr->id'");
			
			foreach($obat_jadi_rows as $o) {
				$jaspel = $o->embalase + $o->tusla;
				$total = $o->harga * $o->jumlah + $jaspel;
				$diskon += $total;
				$ket['obat'][] = array(
					'nama_obat' => $o->nama_obat,		
					'jumlah' => $o->jumlah,
					'harga' => $o->harga,
					'jaspel' => $jaspel,
					'total' => $total,
				);
			}
			
			$obat_racikan_rows = $penjualan_resep_dbtable->get_result("
				SELECT *
				FROM " . InventoryLibrary::$_TBL_PENJUALAN_OBAT_RACIKAN . "
				WHERE id_penjualan_resep = '$rr->id'");
			
			foreach($obat_racikan_rows as $o) {
				$jaspel = $o->embalase + $o->tusla + $o->biaya_racik;
				$total = $o->harga * $o->jumlah + $jaspel;
				$diskon += $total;
				$ket['obat'][] = array(
					'nama_obat' => $o->nama,
					'jumlah' => $o->jumlah,
					'harga' => $o->harga,
					'jaspel' => $jaspel,
					'total' => $total,
				);
			}
			
			$detail_retur_rows = $penjualan_resep_dbtable->get_result("
				SELECT a.*, b.nama_obat, c.persentase_retur, c.tanggal
				FROM " . InventoryLibrary::$_TBL_DRETUR_PENJUALAN_RESEP . " a 
					LEFT JOIN " . InventoryLibrary::$_TBL_STOK_OBAT . " b ON a.id_stok_obat = b.id
					LEFT JOIN " . InventoryLibrary::$_TBL_RETUR_PENJUALAN_RESEP . " c ON a.id_retur_penjualan_resep = c.id
				WHERE 
					a.id_penjualan_resep = '" . $rr->id . "' AND c.dibatalkan = 0 AND c.prop = ''
			");
			
			foreach($detail_retur_rows as $drr) {
				$ket['retur'][] = array(
					'nama_obat' => $drr->nama_obat,
					'jumlah' => $drr->jumlah,
					'harga' => $drr->harga,
					'persentase' => $drr->persentase_retur, 
				);					
				$resep['biaya'] -= $drr->harga * $drr->persentase_retur / 100 * $drr->jumlah;
				$resep['start'] = $drr->tanggal;
				$resep['end'] = $drr->tanggal;			
			}

			if ($diskon > 1)
				$ket['diskon'] = $diskon;
						
			$resep['keterangan'] = $ket;
			$result[] = $resep;
		}

		$asuhan_farmasi_rows = $db->get_result("
			SELECT a.*, b.uri
			FROM " . InventoryLibrary::$_TBL_ASUHAN_FARMASI . " a LEFT JOIN smis_rg_layananpasien b ON a.noreg_pasien = b.id
			WHERE a.noreg_pasien = '" . $noreg . "' AND a.prop = ''
		");
		$asuhan_farmasi_result = array();
		if ($asuhan_farmasi_rows != null) {
			foreach ($asuhan_farmasi_rows as $afr) {
				$nama = $afr->nama_tindakan == "" ? "Asuhan Farmasi" : $afr->nama_tindakan;
				$asuhan_farmasi = array(
					'id' 				=> $afr->id,
					'nama' 				=> $nama . " No. " . $afr->id,
					'waktu' 			=> ArrayAdapter::format("date d M Y", $afr->tanggal),
					'start' 			=> $afr->tanggal,
					'end' 				=> $afr->tanggal,
					'jumlah' 			=> 1,
					'biaya' 			=> $afr->biaya,
					'keterangan'		=> $nama . " di Ruangan " . ArrayAdapter::format("unslug", $afr->ruangan),
					'nama_dokter' 		=> "",
					'id_dokter'			=> 0,
					'jaspel_dokter'		=> 0,
					'urjigd'			=> $afr->uri == 1 ? "URI" : "URJ",
					'tanggal_tagihan'	=> ArrayAdapter::format("date Y-m-d", $afr->tanggal)
				);
				$asuhan_farmasi_result[] = $asuhan_farmasi;
			}
		}

		$cara_keluar = "Selesai";
		$selesai = "1";
		$params = array(
			'id' => $noreg
		);
		$service_consumer = new ServiceConsumer(
			$db,
			"get_one_register",
			$params,
			"registration"
		);
		$content = $service_consumer->execute()->getContent();
		if (count($content) == 1) {
			if ($content[0]['uri'] == "1") {
				$selesai = $db->get_var("
					SELECT 
						COUNT(*) jumlah 
					FROM 
						" . InventoryLibrary::$_TBL_PENJUALAN_RESEP . "
					WHERE
						prop = ''
							AND dibatalkan = 0
							AND selesai = 0
							AND noreg_pasien = '" . $_POST['noreg_pasien'] . "'
				") > 0 ? "0" : "1";
				if ($selesai == "0") {
					$cara_keluar = "Belum Selesai";
				}
			}
		}
		
		echo json_encode(array(
			'selesai' => $selesai,
			'exist' => "1",
			'reverse' => "0",
			'cara_keluar' => $cara_keluar,
			'jasa_pelayanan' => "0",
			'data' => array(
				'penjualan_resep' => array(
					'result'			=> $result,
					'jasa_pelayanan' 	=> "0"
				),
				'asuhan_farmasi' => array(
					'result'			=> $asuhan_farmasi_result,
					'jasa_pelayanan' 	=> "0"
				)
			),
		));
	}
?>
