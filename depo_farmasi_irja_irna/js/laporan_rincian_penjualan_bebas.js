var dpb;
var FINISHED;
$(document).ready(function() {
	$('.mydatetime').datetimepicker({ 
		minuteStep: 1
	});
	dpb = new DPBAction(
		"dpb",
		"depo_farmasi_irja_irna",
		"laporan_rincian_penjualan_bebas",
		new Array()
	);
	$("#dpb_list").append(
		"<tr id='temp'>" +
			"<td colspan='14'><strong><small><center>DATA PENJUALAN BEBAS BELUM DIPROSES</center></small></strong></td>" +
		"</tr>"
	);
	$(document).keyup(function(e) {
		if (e.which == 27) {
			FINISHED = true;
		}
	});
});