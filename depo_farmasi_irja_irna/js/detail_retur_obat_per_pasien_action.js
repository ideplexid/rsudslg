function DetailReturObatPerPasienAction(name, page, action, column) {
    this.initialize(name, page, action, column);
}
DetailReturObatPerPasienAction.prototype.constructor = DetailReturObatPerPasienAction;
DetailReturObatPerPasienAction.prototype = new TableAction();
DetailReturObatPerPasienAction.prototype.clear = function() {
    $("#detail_retur_obat_per_pasien_nama_obat").val("");
    $("#detail_retur_obat_per_pasien_jumlah_jual").val("");
    $("#detail_retur_obat_per_pasien_f_jumlah_jual").val("");
    $("#detail_retur_obat_per_pasien_jumlah_retur").val("");
    $("#detail_retur_obat_per_pasien_satuan").val("");
};
DetailReturObatPerPasienAction.prototype.edit = function(r_num) {
    var nama_obat = $("#detail_retur_obat_per_pasien_list tr:eq(" + r_num + ") td#nama_obat").text();
    var jumlah_retur = $("#detail_retur_obat_per_pasien_list tr:eq(" + r_num + ") td#jumlah_retur").text();
    var jumlah_jual = $("#detail_retur_obat_per_pasien_list tr:eq(" + r_num + ") td#jumlah_jual").text();
    var satuan = $("#detail_retur_obat_per_pasien_list tr:eq(" + r_num + ") td#satuan").text();
    var f_jumlah_jual = jumlah_jual + " " + satuan;

    this.clear();
    showLoading();
    $("#detail_retur_obat_per_pasien_nama_obat").val(nama_obat);
    $("#detail_retur_obat_per_pasien_jumlah_jual").val(jumlah_jual);
    $("#detail_retur_obat_per_pasien_f_jumlah_jual").val(f_jumlah_jual);
    $("#detail_retur_obat_per_pasien_jumlah_retur").val(jumlah_retur);
    $("#detail_retur_obat_per_pasien_satuan").val(satuan);
    $("#save_detail_button").attr("onclick", "detail_retur_obat_per_pasien.update(" + r_num + ")");
    $("#dretur_obat_per_pasien_modal").smodal("show");
    dismissLoading();
};
DetailReturObatPerPasienAction.prototype.validate = function() {
    var jumlah_retur = $("#detail_retur_obat_per_pasien_jumlah_retur").val();
    var jumlah_jual = $("#detail_retur_obat_per_pasien_jumlah_jual").val();

    var valid = true;
    $(".error_field").removeClass("error_field");
    var invalid_msg = "";

    if (jumlah_retur == "") {
        valid = false;
        invalid_msg += "</br><strong>Jml. Retur</strong> tidak boleh kosong";
        $("#detail_retur_obat_per_pasien_jumlah_retur").addClass("error_field");
        $("#detail_retur_obat_per_pasien_jumlah_retur").focus();
    } else if (!is_numeric(jumlah_retur)) {
        valid = false;
        invalid_msg += "</br><strong>Jml. Retur</strong> hanya diperkenankan angka (0-9)";
        $("#detail_retur_obat_per_pasien_jumlah_retur").addClass("error_field");
        $("#detail_retur_obat_per_pasien_jumlah_retur").focus();
    } else if (parseFloat(jumlah_retur) > parseFloat(jumlah_jual)) {
        valid = false;
        invalid_msg += "</br><strong>Jml. Retur</strong> tidak diperkenankan melebihi <strong>Jml. Beli</strong>";
        $("#detail_retur_obat_per_pasien_jumlah_retur").addClass("error_field");
        $("#detail_retur_obat_per_pasien_jumlah_retur").focus();
    }

    if (!valid) {
        $("#modal_alert_dretur_obat_per_pasien_modal").html(
            "<div class='alert alert-block alert-danger'>" +
            "<h4>Peringatan</h4>" +
            invalid_msg +
            "</div>"
        );
    }
    return valid;
};
DetailReturObatPerPasienAction.prototype.update = function(r_num) {
    if (!this.validate())
        return;

    showLoading();
    var jumlah_retur = $("#detail_retur_obat_per_pasien_jumlah_retur").val();
    var satuan = $("#detail_retur_obat_per_pasien_satuan").val();
    var f_jumlah_retur = jumlah_retur + " " + satuan;

    $("#detail_retur_obat_per_pasien_list tr:eq(" + r_num + ") td#jumlah_retur").html(jumlah_retur);
    $("#detail_retur_obat_per_pasien_list tr:eq(" + r_num + ") td#f_jumlah_retur").html(f_jumlah_retur);
    $("#dretur_obat_per_pasien_modal").smodal("hide");
    dismissLoading();
};