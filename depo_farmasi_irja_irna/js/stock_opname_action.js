function SOAction(name, page, action, column) {
	this.initialize(name, page, action, column);
}
SOAction.prototype.constructor = SOAction;
SOAction.prototype = new TableAction();
SOAction.prototype.view = function() {
	if ($("#so_tanggal_from").val() == "" || $("#so_tanggal_to").val() == "")
		return;
	var self = this;
	$("#so_proceed_tanggal_from").val($("#so_tanggal_from").val());
	$("#so_proceed_tanggal_to").val($("#so_tanggal_to").val());
	$("#so_info").empty();
	$("#so_loading_bar").sload("true", "Harap ditunggu...", 0);
	$("#so_loading_modal").smodal("show");
	FINISHED = false;
	var data = this.getRegulerData();
	data['command'] = "get_jumlah_obat";
	data['tanggal_from'] = $("#so_tanggal_from").val();
	data['tanggal_to'] = $("#so_tanggal_to").val();
	data['jenis_filter'] = $("#so_jenis_filter").val();
	data['id_obat'] = $("#so_id_obat").val();
	data['nama_obat'] = $("#so_nama_obat").val();
	data['nama_jenis_obat'] = $("#so_nama_jenis_obat").val();
	data['kode_jenis_obat'] = $("#so_kode_jenis_obat").val();
	data['urutan'] = $("#so_urutan").val();
	$.post(
		"",
		data,
		function(response) {
			var json = JSON.parse(response);
			if (json == null) return;
			$("#so_list").empty();
			self.fillHtml(0, json.jumlah);
		}
	);
};
SOAction.prototype.fillHtml = function(num, limit) {
	if (FINISHED || num == limit) {
		if (FINISHED == false && num == limit) {
			$(".data_action_button").show();
			this.finalize();
		} else {
			$("#so_loading_modal").smodal("hide");
			$("#so_info").html(
				"<div class='alert alert-block alert-inverse'>" +
					 "<center><strong>TIDAK SEMUA DATA PERSEDIAAN DITAMPILKAN</strong></center>" +
				 "</div>"
			);
			$("#so_export_button").removeAttr("onclick");
		}
		return;
	}
	var self = this;
	var data = this.getRegulerData();
	data['command'] = "get_info_obat";
	data['num'] = num;
	data['tanggal_from'] = $("#so_tanggal_from").val();
	data['tanggal_to'] = $("#so_tanggal_to").val();
	data['jenis_filter'] = $("#so_jenis_filter").val();
	data['id_obat'] = $("#so_id_obat").val();
	data['nama_obat'] = $("#so_nama_obat").val();
	data['nama_jenis_obat'] = $("#so_nama_jenis_obat").val();
	data['kode_jenis_obat'] = $("#so_kode_jenis_obat").val();
	data['urutan'] = $("#so_urutan").val();
	$.post(
		"",
		data,
		function(response) {
			var json = JSON.parse(response);
			if (json == null) return;
			$("tbody#so_list").append(
				json.html
			);
			$("#so_loading_bar").sload("true", json.id_obat + " - " + json.kode_obat + " - " + json.nama_obat + " - " + json.nama_jenis_obat + " (" + (num + 1) + " / " + limit + ")", (num + 1) * 100 / limit - 1);
			self.fillHtml(num + 1, limit);
		}
	);
};
SOAction.prototype.finalize = function() {
	var num_rows = $("tbody#so_list tr").length;
	for (var i = 0; i < num_rows; i++)
		$("tbody#so_list tr:eq(" + i + ") td#nomor").html("<small>" + (i + 1) + "</small>");
	$("#so_loading_modal").smodal("hide");
	$("#so_info").html(
		"<div class='alert alert-block alert-info'>" +
			 "<center><strong>SEMUA DATA PERSEDIAAN BERHASIL DITAMPILKAN</strong></center>" +
		 "</div>"
	);
	$("#so_export_button").removeAttr("onclick");
	$("#so_export_button").attr("onclick", "so.export_xls()");
};
SOAction.prototype.cancel = function() {
	FINISHED = true;
};
SOAction.prototype.edit = function(r_num) {
	showLoading();
	var id_obat = $("tr#data_" + r_num + " td#id_obat").text();
	var kode_obat = $("tr#data_" + r_num + " td#kode_obat").text();
	var nama_obat = $("tr#data_" + r_num + " td#nama_obat").text();
	var nama_jenis_obat = $("tr#data_" + r_num + " td#nama_jenis_obat").text();
	var tanggal_from = $("#so_proceed_tanggal_from").val();
	var tanggal_to = $("#so_proceed_tanggal_to").val();
	var stok = $("tr#data_" + r_num + " td#stok").text();
	var tanggal_so = tanggal_to;

	$("#so_m_id_obat").val(id_obat);
	$("#so_m_kode_obat").val(kode_obat);
	$("#so_m_nama_obat").val(nama_obat);
	$("#so_m_nama_jenis_obat").val(nama_jenis_obat);
	$("#so_m_tanggal_dari").val(tanggal_from);
	$("#so_m_tanggal_sampai").val(tanggal_to);
	$("#so_m_stok").val(stok);
	$("#so_m_tanggal_so").val(tanggal_so);
	$("#so_m_add_form").smodal("show");

	$("#btn_save").removeAttr("onclick");
	$("#btn_save").attr("onclick", "so.save(" + r_num + ")");
	dismissLoading();
};
SOAction.prototype.validate = function() {
	var valid = true;
	return valid;
};
SOAction.prototype.save = function(r_num) {
	if (!this.validate())
		return;
	showLoading();
	var id_obat = $("#so_m_id_obat").val();
	var kode_obat = $("#so_m_kode_obat").val();
	var nama_obat = $("#so_m_nama_obat").val();
	var nama_jenis_obat = $("#so_m_nama_jenis_obat").val();
	var tanggal_so = $("#so_m_tanggal_so").val();
	var stok = $("#so_m_stok").val();
	var koreksi_stok = $("#so_m_koreksi_stok").val();

	var data = this.getRegulerData();
	data['command'] = "save";
	data['id_obat'] = id_obat;
	data['kode_obat'] = kode_obat;
	data['nama_obat'] = nama_obat;
	data['nama_jenis_obat'] = nama_jenis_obat;
	data['stok'] = stok;
	data['koreksi_stok'] = koreksi_stok;
	data['tanggal_so'] = tanggal_so;

	$.post(
		"",
		data,
		function(response) {
			var json = JSON.parse(response);
			if (json == null) {
				dismissLoading();
				return;
			}
			$("#data_" + r_num + " td#stok").html(json.sisa);
			$("#data_" + r_num + " td#f_stok").html("<small><div align='right'>" + parseFloat(json.sisa).formatMoney("0", ".", ",") + "</div></small>");

			$("#so_m_id_obat").val("");
			$("#so_m_kode_obat").val("");
			$("#so_m_nama_obat").val("");
			$("#so_m_nama_jenis_obat").val("");
			$("#so_m_tanggal_dari").val("");
			$("#so_m_tanggal_sampai").val("");
			$("#so_m_stok").val("");
			$("#so_m_koreksi_stok").val("");
			$("#so_m_tanggal_so").val("");

			$("#so_m_add_form").smodal("hide");
			dismissLoading();
		}
	);
};