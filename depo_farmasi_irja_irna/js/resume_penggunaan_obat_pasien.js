var rpop;
var pasien;
$(document).ready(function() {
	$("#smis-chooser-modal").on("show", function() {
		$("table#table_pasien tfoot tr").eq(0).hide();
		$("#smis-chooser-modal").addClass("full_model");	
	});
	rpop = new RPOPAction(
		"rpop",
		"depo_farmasi_irja_irna",
		"resume_penggunaan_obat_pasien",
		new Array()
	);
	rpop.view();
	pasien = new PasienAction(
		"pasien",
		"depo_farmasi_irja_irna",
		"resume_penggunaan_obat_pasien",
		new Array()
	);
	pasien.setSuperCommand("pasien");
	$("#resume_list").append("<tr><td colspan='8'><center><strong>DATA BELUM DIPROSES</strong></center></td></tr>");
});