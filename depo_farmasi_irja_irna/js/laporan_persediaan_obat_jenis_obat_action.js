function LPOJenisObatAction(name, page, action, column) {
	this.initialize(name, page, action, column);
}
LPOJenisObatAction.prototype.constructor = LPOJenisObatAction;
LPOJenisObatAction.prototype = new TableAction();
LPOJenisObatAction.prototype.selected = function(json) {
	$("#lpo_kode_jenis_obat").val(json.kode);
	$("#lpo_nama_jenis_obat").val(json.nama);
};