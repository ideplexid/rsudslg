function RRPRAction(name, page, action, column) {
	this.initialize(name, page, action, column);
}
RRPRAction.prototype.constructor = RRPRAction;
RRPRAction.prototype = new TableAction();
RRPRAction.prototype.getRegulerData = function() {
	var data = TableAction.prototype.getRegulerData.call(this);
	data['tanggal_from'] = $("#rrpr_tanggal_from").val();
	data['tanggal_to'] = $("#rrpr_tanggal_to").val();
	return data;
};
RRPRAction.prototype.view = function() {
	if ($("#rrpr_tanggal_from").val() == "" || $("#rrpr_tanggal_to").val() == "")
		return;
	var self = this;
	FINISHED = false;
	$("#rrpr_info").empty();
	$("#table_rrpr tfoot").remove();
	$("#rrpr_list").empty();
	$("#rrpr_loading_bar").sload("true", "Harap ditunggu...", 0);
	$("#rrpr_loading_modal").smodal("show");
	var data = this.getRegulerData();
	data['command'] = "get_jumlah";
	$.post(
		"",
		data,
		function(response) {
			var json = JSON.parse(response);
			if (json == null) return;
			self.fillHtml(0, json.jumlah);
		}
	);
};
RRPRAction.prototype.fillHtml = function(num, limit) {
	if (FINISHED || num == limit) {
		if (FINISHED == false && num == limit) {
			this.finalize();
		} else {
			$("#rrpr_loading_modal").smodal("hide");
			$("#rrpr_info").html(
				"<div class='alert alert-block alert-inverse'>" +
					 "<center><strong>PROSES DIBATALKAN</strong></center>" +
				 "</div>"
			);
			$("#rrpr_export_button").removeAttr("onclick");
		}
		return;
	}
	var self = this;
	var data = this.getRegulerData();
	data['command'] = "get_info";
	data['num'] = num;
	$.post(
		"",
		data,
		function(response) {
			var json = JSON.parse(response);
			if (json == null) return;
			console.log(json);
			$("tbody#rrpr_list").append(
				json.html
			);
			$("#rrpr_loading_bar").sload("true", json.nomor_transaksi + " - " + json.nomor_resep + " - " + json.nama_dokter + " - " + json.nama_pasien + " (" + (num + 1) + " / " + limit + ")", (num + 1) * 100 / limit - 1);
			self.fillHtml(num + 1, limit);
		}
	);
};
RRPRAction.prototype.finalize = function() {
	var num_rows = $("tbody#rrpr_list tr").length;
	var t_total = 0;
	for (var i = 0; i < num_rows; i++) {
		$("tbody#rrpr_list tr:eq(" + i + ") td#rrpr_nomor").html("<small>" + (i + 1) + "</small>");
		var total = parseFloat($("tbody#rrpr_list tr:eq(" + i + ") td#rrpr_total").text());
		t_total += total;
	}
	$("#table_rrpr").append(
		"<tfoot>" +
			"<tr>" +
				"<td colspan='12'><small><strong><center>T O T A L</center></strong></small></td>" +
				"<td><small><div align='right'><strong>" + t_total.formatMoney("2", ".", ",") + "</strong></div></small></td>" +
			"</tr>" +
		"</tfoot>"
	);
	$("#rrpr_loading_modal").smodal("hide");
	$("#rrpr_info").html(
		"<div class='alert alert-block alert-info'>" +
			 "<center><strong>PROSES SELESAI</strong></center>" +
		 "</div>"
	);
	$("#rrpr_export_button").removeAttr("onclick");
	$("#rrpr_export_button").attr("onclick", "rrpr.export_xls()");
};
RRPRAction.prototype.cancel = function() {
	FINISHED = true;
};
RRPRAction.prototype.export_xls = function() {
	showLoading();
	var num_rows = $("#rrpr_list").children("tr").length;
	var d_data = {};
	for (var i = 0; i < num_rows; i++) {
		var nomor = $("tbody#rrpr_list tr:eq(" + i + ") td#rrpr_nomor").text();
		var nomor_transaksi = $("tbody#rrpr_list tr:eq(" + i + ") td#rrpr_id").text();
		var nomor_resep = $("tbody#rrpr_list tr:eq(" + i + ") td#rrpr_no_resep").text();
		var tanggal = $("tbody#rrpr_list tr:eq(" + i + ") td#rrpr_tanggal").text();
		var nama_dokter = $("tbody#rrpr_list tr:eq(" + i + ") td#rrpr_nama_dokter").text();
		var noreg_pasien = $("tbody#rrpr_list tr:eq(" + i + ") td#rrpr_noreg_pasien").text();
		var nrm_pasien = $("tbody#rrpr_list tr:eq(" + i + ") td#rrpr_nrm_pasien").text();
		var nama_pasien = $("tbody#rrpr_list tr:eq(" + i + ") td#rrpr_nama_pasien").text();
		var alamat_pasien = $("tbody#rrpr_list tr:eq(" + i + ") td#rrpr_alamat_pasien").text();
		var jenis = $("tbody#rrpr_list tr:eq(" + i + ") td#rrpr_jenis").text();
		var uri = $("tbody#rrpr_list tr:eq(" + i + ") td#rrpr_uri").text();
		var unit = $("tbody#rrpr_list tr:eq(" + i + ") td#rrpr_unit").text();
		var total = $("tbody#rrpr_list tr:eq(" + i + ") td#rrpr_total").text();
		d_data[i] = {
			"nomor" 			: nomor,
			"nomor_transaksi"	: nomor_transaksi,
			"nomor_resep"		: nomor_resep,
			"tanggal"			: tanggal,
			"nama_dokter"		: nama_dokter,
			"noreg_pasien"		: noreg_pasien,
			"nrm_pasien"		: nrm_pasien,
			"nama_pasien"		: nama_pasien,
			"alamat_pasien"		: alamat_pasien,
			"jenis"				: jenis,
			"uri"				: uri,
			"unit"				: unit,
			"total"				: total
		};
	}
	var data = this.getRegulerData();
	data['command'] = "export_xls";
	data['tanggal_from'] = $("#rrpr_tanggal_from").val();
	data['tanggal_to'] = $("#rrpr_tanggal_to").val();
	data['d_data'] = JSON.stringify(d_data);
	data['num_rows'] = num_rows;
	postForm(data);
	dismissLoading();
};