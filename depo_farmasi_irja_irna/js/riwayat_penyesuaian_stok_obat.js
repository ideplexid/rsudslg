var riwayat_penyesuaian_stok;
$(document).ready(function() {
	$('[data-toggle="popover"]').popover({
		trigger: 'hover',
		'placement': 'top'
	});
	riwayat_penyesuaian_stok = new RiwayatPenyesuaianStokAction(
		"riwayat_penyesuaian_stok", 
		"depo_farmasi_irja_irna", 
		"riwayat_penyesuaian_stok_obat",
		new Array()
	);
	riwayat_penyesuaian_stok.view();
});