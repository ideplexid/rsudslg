function RiwayatStokAction(name, page, action, column) {
	this.initialize(name, page, action, column);
}
RiwayatStokAction.prototype.constructor = RiwayatStokAction;
RiwayatStokAction.prototype = new TableAction();
RiwayatStokAction.prototype.getViewData = function() {
	var data = TableAction.prototype.getViewData.call(this);
	data['id_stok_obat'] = $("#riwayat_stok_id_stok_obat").val();
	return data;
};