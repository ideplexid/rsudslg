<?php
class ObatKeluarUnitLainTable extends Table {
	public function getContentButton($id) {
		$btn_group = new ButtonGroup("noprint");			
		$btn = new Button("", "", "Lihat");
		$btn->setAction($this->action . ".detail('" . $id . "')");
		$btn->setClass("btn-success");
		$btn->setAtribute("data-content='Lihat' data-toggle='popover'");
		$btn->setIcon("icon-eye-open icon-white");
		$btn->setIsButton(Button::$ICONIC);
		$btn_group->addElement($btn);
		$btn = new Button("", "", "Hapus");
		$btn->setAction($this->action . ".del('" . $id . "')");
		$btn->setClass("btn-danger");
		$btn->setAtribute("data-content='Hapus' data-toggle='popover'");
		$btn->setIcon("icon-remove icon-white");
		$btn->setIsButton(Button::$ICONIC);
		$btn_group->addElement($btn);
		$btn = new Button("", "", "AU-58");
		$btn->setAction($this->action . ".download_au58('" . $id . "')");
		$btn->setClass("btn-inverse");
		$btn->setAtribute("data-content='AU-58' data-toggle='popover'");
		$btn->setIcon("fa fa-download");
		$btn->setIsButton(Button::$ICONIC);
		$btn_group->addElement($btn);
		return $btn_group;
	}
}
?>