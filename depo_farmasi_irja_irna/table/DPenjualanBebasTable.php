<?php
class DPenjualanBebasTable extends Table {
	public function getHeaderButton() {
		$btn_group = new ButtonGroup("noprint");
		$btn_add_obat = new Button("", "", "Obat Jadi");
		$btn_add_obat->setAtribute("id='obat_jadi_add'");
		$btn_add_obat->setAction($this->action . ".show_add_obat_jadi_form()");
		$btn_add_obat->setClass("btn-primary");
		$btn_add_obat->setIcon("icon-plus icon-white");
		$btn_add_obat->setIsButton(Button::$ICONIC_TEXT);
		$btn_group->addElement($btn_add_obat);
		return $btn_group->getHtml();
	}
}
?>