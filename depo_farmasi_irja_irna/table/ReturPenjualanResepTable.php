<?php
class ReturPenjualanResepTable extends Table {
	public function getBodyContent() {
		$content = "";
		if ($this->content!=NULL) {
			foreach ($this->content as $d) {
				$content .= "<tr>";
				foreach ($this->header as $h) {
					$content .= "<td>" . $d[$h] . "</td>";
				}
				if ($this->is_action) {
					$content .= "<td>" . $this->getFilteredContentButton($d['id'], $d['dibatalkan'], $d['tercetak'], $d['tanggal'])->getHtml() . "</td>";
				}
				$content .= "</tr>";
			}
		}
		return $content;
	}
	public function getFilteredContentButton($id, $dibatalkan, $tercetak, $tanggal) {
		$btn_group = new ButtonGroup("noprint");
		if ($dibatalkan) {
			$btn = new Button("", "", "Lihat");
			$btn->setAction($this->action . ".detail('" . $id . "')");
			$btn->setClass("btn-danger");
			$btn->setAtribute("data-content='Lihat' data-toggle='popover'");
			$btn->setIcon("icon-eye-open icon-white");
			$btn->setIsButton(Button::$ICONIC);
			$btn_group->addElement($btn);
		} else {
			$btn = new Button("", "", "Lihat");
			$btn->setAction($this->action . ".detail('" . $id . "')");
			$btn->setClass("btn-success");
			$btn->setAtribute("data-content='Lihat' data-toggle='popover'");
			$btn->setIcon("icon-eye-open icon-white");
			$btn->setIsButton(Button::$ICONIC);
			$btn_group->addElement($btn);
			if (!$tercetak || !getSettings($db, "depo_farmasi-retur_penjualan-print_once", 1)) {
				$btn = new Button("", "", "Cetak Retur Resep");
				$btn->setAction($this->action . ".print_retur('" . $id . "')");
				$btn->setClass("btn-inverse");
				$btn->setAtribute("data-content='Cetak Retur Resep' data-toggle='popover'");
				$btn->setIcon("icon-print icon-white");
				$btn->setIsButton(Button::$ICONIC);
				$btn_group->addElement($btn);
			}
			global $db;
			$lock_transact_date = getSettings($db, "depo_farmasi-retur_penjualan-lock_transact_date", "") == "" ? date("Y-m-d H:i") : getSettings($db, "depo_farmasi-retur_penjualan-lock_transact_date", "") . " 23:59";
			if (strtotime($tanggal) > strtotime($lock_transact_date)) {
				$btn = new Button("", "", "Batal");
				$btn->setAction($this->action . ".cancel('" . $id . "')");
				$btn->setClass("btn-danger");
				$btn->setAtribute("data-content='Batal' data-toggle='popover'");
				$btn->setIcon("icon-remove icon-white");
				$btn->setIsButton(Button::$ICONIC);
				$btn_group->addElement($btn);
			}
		}
		return $btn_group;
	}
}
?>