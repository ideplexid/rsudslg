<?php
class DResepTable extends Table {
	public function getHeaderButton() {
		$btn_group = new ButtonGroup("noprint");
		$btn_add_obat = new Button("", "", "Obat Jadi");
		$btn_add_obat->setAtribute("id='obat_jadi_add'");
		$btn_add_obat->setAction($this->action . ".show_add_obat_jadi_form()");
		$btn_add_obat->setClass("btn-primary");
		$btn_add_obat->setIcon("icon-plus icon-white");
		$btn_add_obat->setIsButton(Button::$ICONIC_TEXT);
		$btn_group->addElement($btn_add_obat);
		$btn_add_racikan = new Button("", "", "Obat Racikan");
		$btn_add_racikan->setAction($this->action . ".show_add_obat_racikan_form()");
		$btn_add_racikan->setAtribute("id='obat_racikan_add'");
		$btn_add_racikan->setClass("btn-info");
		$btn_add_racikan->setIcon("icon-plus icon-white");
		$btn_add_racikan->setIsButton(Button::$ICONIC_TEXT);
		$btn_group->addElement($btn_add_racikan);
		global $db;
		if (getSettings($db, "depo_farmasi-paket_obat_non_racikan", 0) == 1) {
			$btn_add_paket_obat_jadi = new Button("", "", "Paket Obat");
			$btn_add_paket_obat_jadi->setAction("paket_obat_jadi.chooser('paket_obat_jadi', 'paket_obat_jadi_button', 'paket_obat_jadi', paket_obat_jadi, 'Paket Obat Non-Racikan')");
			$btn_add_paket_obat_jadi->setAtribute("id='paket_obat_non_racikan_add'");
			$btn_add_paket_obat_jadi->setClass("btn-inverse");
			$btn_add_paket_obat_jadi->setIcon("icon-plus icon-white");
			$btn_add_paket_obat_jadi->setIsButton(Button::$ICONIC_TEXT);
			$btn_group->addElement($btn_add_paket_obat_jadi);
		}
		return $btn_group->getHtml();
	}
}
?>