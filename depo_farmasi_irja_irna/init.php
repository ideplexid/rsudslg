<?php
	global $PLUGINS;
	
	$init['name'] = "depo_farmasi_irja_irna";
	$init['path'] = SMIS_DIR . $init['name'] . "/";
	$init['description'] = "Depo Farmasi IRJA - IRNA";
	$init['require'] = "administrator";
	$init['service'] = "";
	$init['version'] = "2.4.6";
	$init['number'] = "21";
	$init['type'] = "";
	
	$plugin = new Plugin($init);
	$PLUGINS[$init['name']] = $plugin;
?>