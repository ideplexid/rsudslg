<?php
    require_once("depo_farmasi_irja_irna/library/InventoryLibrary.php");
    global $db;

    $table = new Table(
        array("No.", "No. Reg.", "NRM", "Nama", "Alamat", "Jns. Pasien", "Asuransi", "Perusahaan"),
        "Depo Farmasi IRJA - IRNA : Daftar Pasien",
        null,
        true
    );
    $table->setName("validasi_penjualan_obat_pasien");
    $table->setAddButtonEnable(false);
    $table->setReloadButtonEnable(false);
    $table->setPrintButtonEnable(false);
    $table->setEditButtonEnable(false);
    $table->setDelButtonEnable(false);
    $validation_button = new Button("", "", "Validasi");
    $validation_button->setIsButton(Button::$ICONIC_TEXT);
    $validation_button->setClass("btn-success");
    $table->addContentButton("validate", $validation_button);

    if (isset($_POST['command'])) {
        if ($_POST['command'] == "validate") {
            $noreg_pasien = $_POST['noreg_pasien'];
            $db->query("
                UPDATE
                    " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . "
                SET
                    selesai = 1
                WHERE
                    noreg_pasien = '" . $noreg_pasien . "'
                        AND tipe = 'resep'
            ");
            return;
        }
        $adapter = new SimpleAdapter(true, "No.");
        $adapter->add("No. Reg.", "noreg_pasien", "digit6");
        $adapter->add("NRM", "nrm_pasien", "digit6");
        $adapter->add("Nama", "nama_pasien");
        $adapter->add("Alamat", "alamat_pasien");
        $adapter->add("Jns. Pasien", "jenis", "unslug");
        $adapter->add("Asuransi", "asuransi");
        $adapter->add("Perusahaan", "perusahaan");

        $dbtable = new DBTable($db, InventoryLibrary::$_TBL_PENJUALAN_RESEP);
        $filter = "";
        if (isset($_POST['kriteria'])) {
            $filter = " AND (a.noreg_pasien LIKE '%" . $_POST['kriteria'] . "%' OR a.nrm_pasien LIKE '%" . $_POST['kriteria'] . "%' OR a.nama_pasien LIKE '%" . $_POST['kriteria'] . "%') ";
        }
        $query_value = "
            SELECT
                DISTINCT a.noreg_pasien id, a.noreg_pasien, a.nrm_pasien, a.nama_pasien, a.alamat_pasien, b.carabayar jenis, c.nama asuransi, d.nama perusahaan
            FROM
                " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . " a
                    INNER JOIN smis_rg_layananpasien b ON a.noreg_pasien = b.id
                    LEFT JOIN smis_rg_asuransi c ON b.asuransi = c.id
                    LEFT JOIN smis_rg_perusahaan d ON b.nama_perusahaan = d.id
            WHERE
                a.tipe = 'resep'
                    AND a.prop = ''
                    AND a.dibatalkan = 0
                    AND a.selesai = 0
                    AND b.uri = 1
                    " . $filter . "
            ORDER BY
                a.noreg_pasien * 1 DESC
        ";
        $query_count = "
            SELECT
                COUNT(*)
            FROM
                (" . $query_value . ") v
        ";
        $dbtable->setPreferredQuery(true, $query_value, $query_count);
        $dbresponder = new DBResponder(
            $dbtable,
            $table,
            $adapter
        );
        $data = $dbresponder->command($_POST['command']);
        echo json_encode($data);
        return;
    }

    echo $table->getHtml();
    echo addJS("framework/smis/js/table_action.js");
	echo addJS("framework/bootstrap/js/bootstrap-datepicker.js");
	echo addCSS("framework/bootstrap/css/datepicker.css");
?>
<script type="text/javascript">
    var validasi_penjualan_obat_pasien;

    $(document).ready(function() {
        validasi_penjualan_obat_pasien = new TableAction(
            "validasi_penjualan_obat_pasien",
            "depo_farmasi_irja_irna",
            "validasi_penjualan_obat_pasien",
            new Array()
        );
        validasi_penjualan_obat_pasien.validate = function(noreg_pasien) {
            var self = this;
            var data = this.getRegulerData();
            data['command'] = "validate";
            data['noreg_pasien'] = noreg_pasien;
            bootbox.confirm({
                'title'     : "Konfirmasi",
                'message'   : "Yakin melakukan validasi transaksi penjualan pasien ini? (No. Reg. " + noreg_pasien + ")",
                'buttons'   : {
                    'confirm'   : {
                        'label'     : "Ya",
                        'className' : "btn-success"
                    },
                    'cancel'    : {
                        label       : "Tidak",
                        className   : "btn-danger"
                    }
                },
                'callback'  : function(result) {
                    if (result) {
                        showLoading();
                        $.post(
                            '',
                            data,
                            function(response) {
                                self.view();
                                dismissLoading();
                            }
                        );
                    }
                }
            });
        };
        validasi_penjualan_obat_pasien.view();
    });
</script>