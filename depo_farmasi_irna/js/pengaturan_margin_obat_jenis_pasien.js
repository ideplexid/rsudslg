var pengaturan_margin_obat_jenis_pasien;
var obat;
$(document).ready(function() {
	obat = new ObatAction(
		"obat",
		"depo_farmasi_irna",
		"pengaturan_margin_obat_jenis_pasien",
		new Array()
	);
	obat.setSuperCommand("obat");
	var columns = new Array("id", "id_obat", "kode_obat", "nama_obat", "nama_jenis_obat", "slug", "jenis_pasien", "asuransi", "perusahaan", "margin_jual", "uri");
	pengaturan_margin_obat_jenis_pasien = new TableAction(
		"pengaturan_margin_obat_jenis_pasien",
		"depo_farmasi_irna",
		"pengaturan_margin_obat_jenis_pasien",
		columns
	);
	pengaturan_margin_obat_jenis_pasien.view();
});