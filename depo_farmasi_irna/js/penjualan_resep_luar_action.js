function ResepAction(name, page, action, column) {
	this.initialize(name, page, action, column);
}
ResepAction.prototype.constructor = ResepAction;
ResepAction.prototype = new TableAction();
ResepAction.prototype.refresh_no_dresep = function() {
	var no = 1;
	var nor_dresep = $("tbody#dresep_list").children("tr").length;
	for(var i = 0; i < nor_dresep; i++) {
		var dr_prefix = $("tbody#dresep_list").children("tr").eq(i).prop("id");
		$("#" + dr_prefix + "_nomor").html(no++);
	}
};
ResepAction.prototype.show_add_form = function() {
	var self = this;
	var data = this.getRegulerData();
	data['super_command'] = "resep";
	data['command'] = "get_start_time";
	$.post(
		"",
		data,
		function(response) {
			var json = JSON.parse(response);
			if (json == null) return;
			if ($(".btn").attr("disabled") == "disabled")
				return;
			$(".btn").removeAttr("disabled");
			$(".btn").attr("disabled", "disabled");
			$("#help_resep").show();
			$("#help_racikan").show();
			$("#resep_id").val("");
			$("#resep_start_time").val(json.start_time);
			$("#resep_ppn").val(json.ppn);
			$("#resep_nomor").val("");
			$("#resep_nomor").removeAttr("disabled");
			$("#resep_nama_dokter").val("");
			$("#resep_nama_dokter").removeAttr("disabled");
			$("#resep_nama_pasien").val("");
			$("#resep_nama_pasien").removeAttr("disabled");
			$("#resep_alamat_pasien").val("");
			$("#resep_alamat_pasien").removeAttr("disabled");
			$("#resep_jenis").val("");
			$("#resep_jenis").removeAttr("disabled");
			$("#resep_uri").val("0");
			$("#resep_uri").removeAttr("disabled");
			$("#resep_asuransi").val("");
			$("#resep_asuransi").removeAttr("disabled");
			$("#resep_perusahaan").val("");
			$("#resep_perusahaan").removeAttr("disabled");
			$("#obat_jadi_add").show();
			$("#obat_racikan_add").show();
			$("tbody#dresep_list").children().remove();
			$("tbody#bahan_racikan_list").children().remove();
			dresep_num = 0;
			racikan_num = 0;
			bahan_num = 0;
			$("#modal_alert_resep_add_form").html("");
			$(".error_field").removeClass("error_field");
			$("#resep_diskon").val("0,00");
			$("#resep_diskon").removeAttr("disabled");
			$("#resep_t_diskon").val("persen");
			$("#resep_t_diskon").removeAttr("disabled");
			self.refreshBiayaTotal();
			$("#resep_save").removeAttr("onclick");
			$("#resep_save").attr("onclick", "resep.save()");
			$("#resep_save").show();
			$("#resep_save_cetak").removeAttr("onclick");
			$("#resep_save_cetak").attr("onclick", "resep.save_cetak()");
			$("#resep_save_cetak").show();
			$("#resep_ok").hide();		
			$("#obat_racikan_nama_apoteker").removeAttr("disabled");
			$("#resep_add_form").smodal("show");
			$(".btn").removeAttr("disabled");
		}
	);
};
ResepAction.prototype.validate = function() {
	var valid = true;
	var invalid_msg = "";
	var nomor_resep = $("#resep_nomor").val();
	var nama_dokter = $("#resep_nama_dokter").val();
	var nama_pasien = $("#resep_nama_pasien").val();
	var alamat_pasien = $("#resep_alamat_pasien").val();
	var jenis_pasien = $("#resep_jenis").val();
	var diskon = $("#resep_diskon").val().replace(/[^0-9-,]/g, '').replace(",", ".");
	var t_diskon = $("#resep_t_diskon").val();
	var nord = $("tbody#dresep_list").children("tr").length;
	$(".error_field").removeClass("error_field");
	if (nomor_resep == "") {
		valid = false;
		invalid_msg += "</br><strong>Nomor</strong> tidak boleh kosong";
		$("#resep_nomor").addClass("error_field");
	}
	if (nama_dokter == "") {
		valid = false;
		invalid_msg += "</br><strong>Dokter</strong> tidak boleh kosong";
		$("#resep_nama_dokter").addClass("error_field");
	}
	if (nama_pasien == "") {
		valid = false;
		invalid_msg += "</br><strong>Nama Pasien</strong> tidak boleh kosong";
		$("#resep_nama_pasien").addClass("error_field");
	}
	if (alamat_pasien == "") {
		valid = false;
		invalid_msg += "</br><strong>Alamat Pasien</strong> tidak boleh kosong";
		$("#resep_alamat_pasien").addClass("error_field");
	}
	if (jenis_pasien == "") {
		valid = false;
		invalid_msg += "</br><strong>Jns. Pasien</strong> tidak boleh kosong";
		$("#resep_jenis").addClass("error_field");
	}
	if (diskon == "") {
		valid = false;
		invalid_msg += "</br><strong>Diskon</strong> tidak boleh kosong";
		$("#resep_diskon").addClass("error_field");
	}
	if (t_diskon == "") {
		valid = false;
		invalid_msg += "</br><strong>Tipe Diskon</strong> tidak boleh kosong";
		$("#resep_t_diskon").addClass("error_field");
	} else if (t_diskon == "persen") {
		if (diskon > 100) {
			valid = false;
			invalid_msg += "</br><strong>Diskon</strong> tidak boleh lebih dari 100%";
			$("#resep_diskon").addClass("error_field");
		}
	}
	if (nord == 0) {
		valid = false;
		invalid_msg += "</br><strong>Detail Resep</strong> tidak boleh kosong";
	}
	if (!valid) {
		$("#modal_alert_resep_add_form").html(
			"<div class='alert alert-block alert-danger'>" +
				"<h4>Peringatan</h4>" +
				invalid_msg +
			"</div>"
		);
	}
	return valid;
};
ResepAction.prototype.getSaveData = function() {
	var data = this.getRegulerData();
	data['super_command'] == "resep";
	data['command'] = "save";
	data['id'] = $("#resep_id").val();
	data['start_time'] = $("#resep_start_time").val();
	data['nomor_resep'] = $("#resep_nomor").val();
	data['nama_dokter'] = $("#resep_nama_dokter").val();
	data['nama_pasien'] = $("#resep_nama_pasien").val();
	data['alamat_pasien'] = $("#resep_alamat_pasien").val();
	data['jenis'] = $("#resep_jenis").val();
	data['asuransi'] = $("#resep_asuransi").val();
	data['perusahaan'] = $("#resep_perusahaan").val();
	data['markup'] = $("#resep_markup").val();
	data['embalase'] = "0";
	data['tusla'] = "0";
	data['biaya_racik'] = "0";
	data['uri'] = $("#resep_uri").val();
	var v_total = parseFloat($("#resep_total").val().replace(/[^0-9-,]/g, '').replace(",", "."));
	data['total'] = v_total;
	data['diskon'] = parseFloat($("#resep_diskon").val().replace(/[^0-9-,]/g, '').replace(",", "."));
	data['t_diskon'] = $("#resep_t_diskon").val();
	data['ppn'] = $("#resep_ppn").val();
	var detail_resep = {};
	var nor_dresep =  $("tbody#dresep_list").children("tr").length;
	var pos = 0;
	for(var i = 0; i < nor_dresep; i++) {
		var dr_prefix = $("tbody#dresep_list").children("tr").eq(i).prop("id");
		var tipe = $("#" + dr_prefix + "_tipe").text();
		if (tipe == "obat_jadi") {
			var id_obat_jadi = $("#" + dr_prefix + "_id").text();
			var id_obat = $("#" + dr_prefix + "_id_obat").text();
			var kode_obat = $("#" + dr_prefix + "_kode_obat").text();
			var nama_obat = $("#" + dr_prefix + "_nama_obat").text();
			var nama_jenis_obat = $("#" + dr_prefix + "_nama_jenis_obat").text();
			var jumlah = $("#" + dr_prefix + "_jumlah").text();
			var jumlah_lama = $("#" + dr_prefix + "_jumlah_lama").text();
			var satuan = $("#" + dr_prefix + "_satuan").text();
			var konversi = $("#" + dr_prefix + "_konversi").text();
			var satuan_konversi = $("#" + dr_prefix + "_satuan_konversi").text();
			var markup = $("#" + dr_prefix + "_markup").text();
			var v_harga = parseFloat($("#" + dr_prefix + "_harga").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var v_embalase = parseFloat($("#" + dr_prefix + "_embalase").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var v_tusla = parseFloat($("#" + dr_prefix + "_tuslah").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var v_biaya_racik = parseFloat($("#" + dr_prefix + "_biaya_racik").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var v_subtotal = parseFloat($("#" + dr_prefix + "_subtotal").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var aturan_pakai = $("#" + dr_prefix + "_aturan_pakai").text();
			var tanggal_exp = $("#" + dr_prefix + "_tanggal_exp").text();
			var obat_luar = $("#" + dr_prefix + "_obat_luar").text();
			var jumlah_pakai_sehari = $("#" + dr_prefix + "_jumlah_pakai_sehari").text();
			var satuan_pakai = $("#" + dr_prefix + "_satuan_pakai").text();
			var takaran_pakai = $("#" + dr_prefix + "_takaran_pakai").text();
			var pemakaian = $("#" + dr_prefix + "_pemakaian").text();
			var pemakaian_obat_luar = $("#" + dr_prefix + "_pemakaian_obat_luar").text();
			var penggunaan = $("#" + dr_prefix + "_penggunaan").text();
			var detail_penggunaan = $("#" + dr_prefix + "_detail_penggunaan").text();
			var d_data = {};
			d_data['tipe'] = tipe;
			d_data['id_obat_jadi'] = id_obat_jadi;
			d_data['id_obat'] = id_obat;
			d_data['kode_obat'] = kode_obat;
			d_data['nama_obat'] = nama_obat;
			d_data['nama_jenis_obat'] = nama_jenis_obat;
			d_data['jumlah'] = jumlah;
			d_data['jumlah_lama'] = jumlah_lama;
			d_data['satuan'] = satuan;
			d_data['konversi'] = konversi;
			d_data['satuan_konversi'] = satuan_konversi;
			d_data['markup'] = markup;
			d_data['harga'] = v_harga;
			d_data['embalase'] = v_embalase;
			d_data['tusla'] = v_tusla;
			d_data['biaya_racik'] = v_biaya_racik;
			d_data['subtotal'] = v_subtotal;
			d_data['aturan_pakai'] = aturan_pakai;
			d_data['tanggal_exp'] = tanggal_exp;
			d_data['obat_luar'] = obat_luar;
			d_data['jumlah_pakai_sehari'] = jumlah_pakai_sehari;
			d_data['satuan_pakai'] = satuan_pakai;
			d_data['takaran_pakai'] = takaran_pakai;
			d_data['pemakaian'] = pemakaian;
			d_data['pemakaian_obat_luar'] = pemakaian_obat_luar;
			d_data['penggunaan'] = penggunaan;
			d_data['detail_penggunaan'] = detail_penggunaan;
			d_data['pos'] = pos++;
			detail_resep[i] = d_data;
		} else if (tipe == "obat_racikan") {
			var id_obat_racikan = $("#" + dr_prefix + "_id").text();
			var nama = $("#" + dr_prefix + "_nama_racikan").text();
			var id_apoteker = $("#" + dr_prefix + "_id_apoteker").text();
			var nama_apoteker = $("#" + dr_prefix + "_apoteker").text();
			var jumlah_racikan = $("#" + dr_prefix + "_jumlah").text();
			var v_embalase = parseFloat($("#" + dr_prefix + "_embalase").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var v_tusla = parseFloat($("#" + dr_prefix + "_tuslah").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var v_biaya_racik = parseFloat($("#" + dr_prefix + "_biaya_racik").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var v_harga = parseFloat($("#" + dr_prefix + "_harga").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var v_subtotal = parseFloat($("#" + dr_prefix + "_subtotal").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var aturan_pakai = $("#" + dr_prefix + "_aturan_pakai").text();
			var tanggal_exp = $("#" + dr_prefix + "_tanggal_exp").text();
			var obat_luar = $("#" + dr_prefix + "_obat_luar").text();
			var jumlah_pakai_sehari = $("#" + dr_prefix + "_jumlah_pakai_sehari").text();
			var satuan_pakai = $("#" + dr_prefix + "_satuan_pakai").text();
			var takaran_pakai = $("#" + dr_prefix + "_takaran_pakai").text();
			var pemakaian = $("#" + dr_prefix + "_pemakaian").text();
			var pemakaian_obat_luar = $("#" + dr_prefix + "_pemakaian_obat_luar").text();
			var penggunaan = $("#" + dr_prefix + "_penggunaan").text();
			var detail_penggunaan = $("#" + dr_prefix + "_detail_penggunaan").text();
			var label = $("#" + dr_prefix + "_label").text();
			var detail_racikan = {};
			var nor_bahan = $("tbody#bahan_racikan_list").children("tr").length;
			for(var j = 0; j < nor_bahan; j++) {
				var b_prefix = $("tbody#bahan_racikan_list").children("tr").eq(j).prop("id");
				var b_label = $("#" + b_prefix + "_label").text();
				if (b_label == label) {
					var id_bahan_pakai = $("#" + b_prefix + "_id").text();
					var id_obat = $("#" + b_prefix + "_id_bahan").text();
					var kode_obat = $("#" + b_prefix + "_kode").text();
					var nama_obat = $("#" + b_prefix + "_nama").text();
					var nama_jenis_obat = $("#" + b_prefix + "_nama_jenis_bahan").text();
					var jumlah = $("#" + b_prefix + "_jumlah").text();
					var satuan = $("#" + b_prefix + "_satuan").text();
					var konversi = $("#" + b_prefix + "_konversi").text();
					var satuan_konversi = $("#" + b_prefix + "_satuan_konversi").text();
					var markup_bahan = $("#" + b_prefix + "_markup").text();
					var harga = parseFloat($("#" + b_prefix + "_harga").text().replace(/[^0-9-,]/g, '').replace(",", "."));
					var embalase = parseFloat($("#" + b_prefix + "_embalase").text().replace(/[^0-9-,]/g, '').replace(",", "."));
					var tusla = parseFloat($("#" + b_prefix + "_tuslah").text().replace(/[^0-9-,]/g, '').replace(",", "."));
					var markup = parseFloat($("#resep_markup").val());
					harga = harga + (markup * harga);
					var b_data = {};
					b_data['id'] = id_bahan_pakai;
					b_data['id_obat'] = id_obat;
					b_data['kode_obat'] = kode_obat;
					b_data['nama_obat'] = nama_obat;
					b_data['nama_jenis_obat'] = nama_jenis_obat;
					b_data['jumlah'] = jumlah;
					b_data['satuan'] = satuan;
					b_data['konversi'] = konversi;
					b_data['satuan_konversi'] = satuan_konversi;
					b_data['markup'] = markup_bahan;
					b_data['harga'] = harga;
					b_data['embalase'] = embalase;
					b_data['tusla'] = tusla;
					detail_racikan[j] = b_data;
				}
			}
			var d_data = {};
			d_data['tipe'] = tipe;
			d_data['nama'] = nama;
			d_data['id_apoteker'] = id_apoteker;
			d_data['nama_apoteker'] = nama_apoteker;
			d_data['jumlah'] = jumlah_racikan;
			d_data['harga'] = v_harga;
			d_data['total_embalase'] = v_embalase;
			d_data['total_tusla'] = v_tusla;
			d_data['biaya_racik'] = v_biaya_racik;
			d_data['subtotal'] = v_subtotal;
			d_data['aturan_pakai'] = aturan_pakai;
			d_data['tanggal_exp'] = tanggal_exp;
			d_data['obat_luar'] = obat_luar;
			d_data['jumlah_pakai_sehari'] = jumlah_pakai_sehari;
			d_data['satuan_pakai'] = satuan_pakai;
			d_data['takaran_pakai'] = takaran_pakai;
			d_data['pemakaian'] = pemakaian;
			d_data['pemakaian_obat_luar'] = pemakaian_obat_luar;
			d_data['penggunaan'] = penggunaan;
			d_data['detail_penggunaan'] = detail_penggunaan;
			d_data['pos'] = pos++;
			d_data['detail_racikan'] = detail_racikan;
			detail_resep[i] = d_data;
		}
	}
	data['detail_resep'] = detail_resep;
	return data;
};
ResepAction.prototype.save = function() {
	if ($(".btn").attr("disabled") == "disabled")
		return;
	$(".btn").removeAttr("disabled");
	$(".btn").attr("disabled", "disabled");
	if (!this.validate()) {
		$(".btn").removeAttr("disabled");
		return;
	}
	showLoading();
	var self = this;
	var data = this.getSaveData();
	$.post(
		"",
		data,
		function(response) {
			var json = getContent(response);				
			if (json == null || json.success==0) {
				$("#modal_alert_resep_add_form").html(
					"<div class='alert alert-block alert-info'>" +
						"<h4>Peringatan</h4>" +
						json.message +
					"</div>"
				);
			} else {
				$("#resep_add_form").smodal("hide");
				self.print_prescription(json.id);
				self.view();
			}
			dismissLoading();
			$(".btn").removeAttr("disabled");
		}
	);
};
ResepAction.prototype.detail = function(id) {
	if ($(".btn").attr("disabled") == "disabled")
		return;
	$(".btn").removeAttr("disabled");
	$(".btn").attr("disabled", "disabled");
	var self = this;
	var data = this.getRegulerData();
	data['super_command'] = "resep";
	data['command'] = "edit";
	data['id'] = id;
	data['readonly'] = true;
	$.post(
		"",
		data,
		function(response) {
			var json = getContent(response);
			if (json == null) return;
			$("#help_resep").hide();
			$("#help_racikan").hide();
			$("#resep_ppn").val(json.header.ppn);
			$("#resep_nomor").val(json.header.nomor_resep);
			$("#resep_nomor").removeAttr("disabled");
			$("#resep_nomor").attr("disabled", "disabled");
			$("#resep_nama_dokter").val(json.header.nama_dokter);
			$("#resep_nama_dokter").removeAttr("disabled");
			$("#resep_nama_dokter").attr("disabled", "disabled");
			$("#resep_nama_pasien").val(json.header.nama_pasien);
			$("#resep_nama_pasien").removeAttr("disabled");
			$("#resep_nama_pasien").attr("disabled", "disabled");
			$("#resep_alamat_pasien").val(json.header.alamat_pasien);
			$("#resep_alamat_pasien").removeAttr("disabled");
			$("#resep_alamat_pasien").attr("disabled", "disabled");
			$("#resep_jenis").val(json.header.jenis);
			$("#resep_jenis").attr("disabled", "disabled");
			$("#resep_uri").val(json.header.uri);
			$("#resep_uri").attr("disabled", "disabled");
			$("#resep_asuransi").val(json.header.asuransi);
			$("#resep_asuransi").attr("disabled", "disabled");
			$("#resep_perusahaan").val(json.header.perusahaan);
			$("#resep_perusahaan").attr("disabled", "disabled");
			$("#resep_markup").val(json.header.markup);
			$("#resep_uri").val(json.header.uri);
			$("#resep_diskon").val((parseFloat(json.header.diskon)).formatMoney("2", ".", ","));
			$("#resep_diskon").removeAttr("disabled");
			$("#resep_diskon").attr("disabled", "disabled");
			$("#resep_t_diskon").val(json.header.t_diskon);
			$("#resep_t_diskon").removeAttr("disabled");
			$("#resep_t_diskon").attr("disabled", "disabled");
			$("#obat_racikan_nama_apoteker").removeAttr("disabled");
			$("#obat_racikan_nama_apoteker").attr("disabled", "disabled");
			$("#obat_jadi_add").hide();
			$("#obat_racikan_add").hide();
			$("tbody#dresep_list").html(json.dresep_list);
			$("tbody#bahan_racikan_list").html(json.bahan_racikan_list);
			dresep_num = json.dresep_num;
			racikan_num = json.racikan_num;
			dracikan_num = json.dracikan_num;
			bahan_num = json.bahan_num;
			$("#modal_alert_resep_add_form").html("");
			$(".error_field").removeClass("error_field");
			self.refreshBiayaTotal();
			$("#resep_save").removeAttr("onclick");
			$("#resep_save").hide();
			$("#resep_ok").show();
			$("#resep_add_form").smodal("show");
			$(".btn").removeAttr("disabled");
		}
	);
};
ResepAction.prototype.refreshBiayaTotal = function() {
	var nord = $("tbody#dresep_list").children("tr").length;
	var biaya_total = 0;
	for(var i = 0; i < nord; i++) {
		var prefix = $("tbody#dresep_list").children("tr").eq(i).prop("id");
		var v_subtotal = parseFloat($("#" + prefix + "_subtotal").text().replace(/[^0-9-,]/g, '').replace(",", "."));
		biaya_total += v_subtotal;
	}
	var diskon = $("#resep_diskon").val();
	diskon = parseFloat(diskon.replace(/[^0-9-,]/g, '').replace(",", "."));
	var t_diskon = $("#resep_t_diskon").val();
	if (t_diskon == "persen" || t_diskon == "gratis") {
		diskon = (diskon * biaya_total) / 100;
	}
	biaya_total = biaya_total - diskon;
	biaya_total = "Rp. " + parseFloat(biaya_total).formatMoney("2", ".", ",");
	$("#resep_total").val(biaya_total);
};
ResepAction.prototype.refreshHargaAndSubtotal = function() {
	var self = this;
	var jenis_pasien = $("#resep_jenis").val();
	var asuransi = $("#resep_asuransi").val();
	var perusahaan = $("#resep_perusahaan").val();
	if (need_margin_penjualan_request == 1) {
		need_margin_penjualan_request = 0;
		if (mode_margin == 0 || mode_margin == 1) {
			var data = this.getRegulerData();
			data['super_command'] = "resep";
			data['command'] = "get_margin_penjualan";
			data['mode_margin'] = mode_margin;
			data['jenis_pasien'] = jenis_pasien;
			data['asuransi'] = asuransi;
			data['perusahaan'] = perusahaan;
			$.post(
				"",
				data,
				function(response) {
					var json = JSON.parse(response);
					self.refreshDetailInfo(json.margin_penjualan);
				}
			);
		} else if (mode_margin == 3) {
			var dresep_counts = $("#dresep_list tr").length;
			showLoading();
			this.updateDetailInfo(0, dresep_counts);
		}
	} else {
		var markup = $("#resep_markup").val();
		this.refreshDetailInfo(markup);
	}
};
ResepAction.prototype.updateDetailInfo = function(num, limit) {
	if (num == limit) {
		dismissLoading();
		this.refreshBiayaTotal();
		return;
	}
	var self = this;
	var row_id = $("#dresep_list tr:eq(" + num + ")").prop("id");
	var tipe = $("#" + row_id + "_tipe").text();
	if (tipe == "obat_jadi") {
		var data = this.getRegulerData();
		data['super_command'] = "sisa";
		data['command'] = "edit";
		data['id_obat'] = $("#" + row_id + "_id_obat").text();
		data['satuan'] = $("#" + row_id + "_satuan").text();
		data['konversi'] = $("#" + row_id + "_konversi").text();
		data['satuan_konversi'] = $("#" + row_id + "_satuan_konversi").text();
		if ($("#resep_jenis").length)
			data['jenis_pasien'] = $("#resep_jenis").val();
		if ($("#resep_asuransi").length)
			data['asuransi'] = $("#resep_asuransi").val();
		if ($("#resep_perusahaan").length)
			data['perusahaan'] = $("#resep_perusahaan").val();
		data['ppn'] = $("#resep_ppn").val();
		$.post(
			"",
			data,
			function(response) {
				var json = getContent(response);
				if (json == null) return;
				var markup = json.markup;
				var harga_jual = parseFloat($("#" + row_id + "_hna").text().replace(/[^0-9-,]/g, '').replace(",", ".")) * (1 + parseFloat(json.markup) / 100);
				var f_harga_jual = "Rp. " + parseFloat(harga_jual).formatMoney("2", ".", ",");
				var embalase = parseFloat($("#" + row_id + "_embalase").text().replace(/[^0-9-,]/g, '').replace(",", "."));
				var tuslah = parseFloat($("#" + row_id + "_tuslah").text().replace(/[^0-9-,]/g, '').replace(",", "."));
				var biaya_racik = parseFloat($("#" + row_id + "_biaya_racik").text().replace(/[^0-9-,]/g, '').replace(",", "."));
				var jumlah = parseFloat($("#" + row_id + "_jumlah").text());
				var subtotal = jumlah * harga_jual + embalase + tuslah + biaya_racik;
				var f_subtotal = "Rp. " + parseFloat(subtotal).formatMoney("2", ".", ",");
				$("#" + row_id + "_markup").html(markup);
				$("#" + row_id + "_harga").html(f_harga_jual);
				$("#" + row_id + "_subtotal").html(f_subtotal);
				self.updateDetailInfo(num + 1, limit);
			}
		);
	} else {
		$("#" + row_id + "_hna").html("Rp. 0,00");
		var bahan_counts = $("#bahan_racikan_list tr").length;
		this.updateBahanInfo(row_id, 0, bahan_counts, num, limit);
	}
};
ResepAction.prototype.updateBahanInfo = function(d_resep_row_id, num, limit, d_resep_num, d_resep_limit) {
	if (num == limit) {
		this.updateDetailInfo(d_resep_num + 1, d_resep_limit);
		return;
	}
	var self = this;
	var label = $("#" + d_resep_row_id + "_label").text();
	var row_id = $("#bahan_racikan_list tr:eq(" + num + ")").prop("id");
	var bahan_label = $("#" + row_id + "_label").text();
	if (label == bahan_label) {
		var data = this.getRegulerData();
		data['super_command'] = "sisa";
		data['command'] = "edit";
		data['id_obat'] = $("#" + row_id + "_id_bahan").text();
		data['satuan'] = $("#" + row_id + "_satuan").text();
		data['konversi'] = $("#" + row_id + "_konversi").text();
		data['satuan_konversi'] = $("#" + row_id + "_satuan_konversi").text();
		if ($("#resep_jenis").length)
			data['jenis_pasien'] = $("#resep_jenis").val();
		if ($("#resep_asuransi").length)
			data['asuransi'] = $("#resep_asuransi").val();
		if ($("#resep_perusahaan").length)
			data['perusahaan'] = $("#resep_perusahaan").val();
		data['ppn'] = $("#resep_ppn").val();
		$.post(
			"",
			data,
			function(response) {
				var json = getContent(response);
				if (json == null) return;
				var markup = json.markup;
				var harga_jual = parseFloat($("#" + row_id + "_harga").text().replace(/[^0-9-,]/g, '').replace(",", "."));
				var hna = harga_jual / (1 + parseFloat($("#" + row_id + "_markup").text()) / 100);
				harga_jual = parseFloat(hna) * (1 + parseFloat(json.markup) / 100);
				var f_harga_jual = "Rp. " + parseFloat(harga_jual).formatMoney("2", ".", ",");
				$("#" + d_resep_row_id + "_h");
				self.updateBahanInfo(d_resep_row_id, num + 1, limit, d_resep_num, d_resep_limit);
			}
		);
	} else {
		this.updateBahanInfo(d_resep_row_id, num + 1, limit, d_resep_num, d_resep_limit);
	}
};
ResepAction.prototype.refreshDetailInfo = function(markup) {
	$("#resep_markup").val(markup);
	var nord = $("tbody#dresep_list").children("tr").length;
	for(var i = 0; i < nord; i++) {
		var prefix = $("tbody#dresep_list").children("tr").eq(i).prop("id");
		var hna = $("#" + prefix + "_hna").text();
		var embalase = $("#" + prefix + "_embalase").text();
		var tuslah = $("#" + prefix + "_tuslah").text();
		var biaya_racik = $("#" + prefix + "_biaya_racik").text();
		var v_hna = parseFloat(hna.replace(/[^0-9-,]/g, '').replace(",", "."));
		var v_embalase = parseFloat(embalase.replace(/[^0-9-,]/g, '').replace(",", "."));
		var v_tuslah = parseFloat(tuslah.replace(/[^0-9-,]/g, '').replace(",", "."));
		var v_biaya_racik = parseFloat(biaya_racik.replace(/[^0-9-,]/g, '').replace(",", "."));
		var v_hja = v_hna + (markup * v_hna);
		var hja = "Rp. " + (parseFloat(v_hja)).formatMoney("2", ".", ",");
		$("#" + prefix + "_harga").text(hja);
		var v_jumlah = $("#" + prefix + "_jumlah").text();
		var v_subtotal = parseFloat(v_jumlah) * parseFloat(v_hja) + parseFloat(v_embalase) + parseFloat(v_tuslah) + parseFloat(v_biaya_racik);
		var subtotal = "Rp. " + (parseFloat(v_subtotal)).formatMoney("2", ".", ",");
		$("#" + prefix + "_subtotal").text(subtotal);
	}
	resep.refreshBiayaTotal();
};
ResepAction.prototype.educate = function(id) {
	var self = this;
	bootbox.confirm(
		"Yakin merubah status edukasi resep ini?",
		function(result) {
			if (result) {
				if ($(".btn").attr("disabled") == "disabled")
					return;
				$(".btn").removeAttr("disabled");
				$(".btn").attr("disabled", "disabled");
				showLoading();
				var data = self.getRegulerData();
				data['super_command'] = "resep";
				data['command'] = "save";
				data['id'] = id;
				data['edukasi'] = "1";
				$.post(
					"",
					data,
					function(response) {
						var json = getContent(response);
						if (json != null) {
							self.view();
						}
						dismissLoading();
						$(".btn").removeAttr("disabled");
					}
				);
			}
		}
	);
};
ResepAction.prototype.undo_educate = function(id) {
	var self = this;
	bootbox.confirm(
		"Yakin merubah status edukasi resep ini?",
		function(result) {
			if (result) {
				if ($(".btn").attr("disabled") == "disabled")
					return;
				$(".btn").removeAttr("disabled");
				$(".btn").attr("disabled", "disabled");
				showLoading();
				var data = self.getRegulerData();
				data['super_command'] = "resep";
				data['command'] = "save";
				data['id'] = id;
				data['edukasi'] = "0";
				$.post(
					"",
					data,
					function(response) {
						var json = getContent(response);
						if (json != null) {
							self.view();
						}
						dismissLoading();
						$(".btn").removeAttr("disabled");
					}
				);
			}
		}
	);
};
ResepAction.prototype.cancel = function(id) {
	var self = this;
	bootbox.prompt(
		"Alasan Pembatalan Resep",
		function(result) {
			if (result !== null && result !== "") {
				if ($(".btn").attr("disabled") == "disabled")
					return;
				$(".btn").removeAttr("disabled");
				$(".btn").attr("disabled", "disabled");
				showLoading();
				var data = self.getRegulerData();
				data['super_command'] = "resep";
				data['command'] = "save";
				data['id'] = id;
				data['dibatalkan'] = 1;
				data['keterangan_batal'] = result;
				$.post(
					"",
					data,
					function(response) {
						var json = getContent(response);
						if (json == null) return;
						self.view();
						dismissLoading();
						$(".btn").removeAttr("disabled");
					}
				);
			} else if (result !== null && result == ""){
				self.cancel(id);
			}
		}
	);
};
ResepAction.prototype.view_cancel_info = function(id) {
	if ($(".btn").attr("disabled") == "disabled")
		return;
	$(".btn").removeAttr("disabled");
	$(".btn").attr("disabled", "disabled");
	var data = this.getRegulerData();
	data['id'] = id;
	data['super_command'] = "resep";
	data['command'] = "edit";
	showLoading();
	$.post(
		"",
		data,
		function(response) {
			var json = getContent(response);
			if (json == null) return;
			dismissLoading();
			if (json.header.keterangan_batal != "null")
				bootbox.alert("<b>Keterangan Pembatalan Resep</b><br/>" + json.header.keterangan_batal);
			else
				bootbox.alert("<b>Keterangan Pembatalan Resep</b><br/>&nbsp;");
			$(".btn").removeAttr("disabled");
		}
	);
};
ResepAction.prototype.print_prescription = function(id) {
	if ($(".btn").attr("disabled") == "disabled")
		return;
	$(".btn").removeAttr("disabled");
	$(".btn").attr("disabled", "disabled");
	var self = this;
	var data = this.getRegulerData();
	data['super_command'] = "resep";
	data['command'] = "print_prescription";
	data['id'] = id;
	$.post(
		"",
		data,
		function(response) {
			var json = getContent(response);
			if (json == null) return;
			smis_print(json);
			// webprint(json);
			$(".btn").removeAttr("disabled");
		}
	);
};
ResepAction.prototype.copy_header = function(id) {
	if ($(".btn").attr("disabled") == "disabled")
		return;
	$(".btn").removeAttr("disabled");
	$(".btn").attr("disabled", "disabled");
	var self = this;
	var data = this.getRegulerData();
	data['super_command'] = "resep";
	data['command'] = "edit";
	data['id'] = id;
	$.post(
		"",
		data,
		function(response) {
			var json = getContent(response);
			if (json == null) return;
			$("#help_resep").hide();
			$("#help_racikan").hide();
			$("#resep_id").val("");
			$("#resep_ppn").val(json.header.ppn);
			$("#resep_nomor").val(json.header.nomor_resep);
			$("#resep_nomor").removeAttr("disabled");
			$("#resep_nomor").attr("disabled", "disabled");
			$("#resep_id_dokter").val(json.header.id_dokter);
			$("#resep_name_dokter").val(json.header.nama_dokter);
			$("#resep_nama_dokter").val(json.header.nama_dokter);
			$("#resep_nama_dokter").removeAttr("disabled");
			$("#resep_nama_dokter").attr("disabled", "disabled");
			$("#dokter_browse").removeAttr("onclick");
			$("#dokter_browse").removeClass("btn-info");
			$("#dokter_browse").removeClass("btn-inverse");
			$("#dokter_browse").addClass("btn-inverse");
			$("#resep_nama_pasien").val(json.header.nama_pasien);
			$("#resep_nama_pasien").removeAttr("disabled");
			$("#resep_nama_pasien").attr("disabled", "disabled");
			$("#resep_alamat_pasien").val(json.header.alamat_pasien);
			$("#resep_alamat_pasien").removeAttr("disabled");
			$("#resep_alamat_pasien").attr("disabled", "disabled");
			$("#resep_jenis").val(json.header.jenis);
			$("#resep_jenis").removeAttr("disabled");
			$("#resep_jenis").attr("disabled", "disabled");
			$("#resep_uri").val(json.header.uri);
			$("#resep_uri").removeAttr("disabled");
			$("#resep_uri").attr("disabled", "disabled");
			$("#resep_perusahaan").val(json.header.perusahaan);
			$("#resep_perusahaan").removeAttr("disabled");
			$("#resep_perusahaan").attr("disabled", "disabled");
			$("#resep_asuransi").val(json.header.asuransi);
			$("#resep_asuransi").removeAttr("disabled");
			$("#resep_asuransi").attr("disabled", "disabled");
			$("#resep_markup").val(json.header.markup);
			$("#resep_uri").val(json.header.uri);
			$("#resep_diskon").val((parseFloat(json.header.diskon)).formatMoney("2", ".", ","));
			$("#resep_diskon").removeAttr("disabled");
			$("#resep_diskon").attr("disabled", "disabled");
			$("#resep_t_diskon").val(json.header.t_diskon);
			$("#resep_t_diskon").removeAttr("disabled");
			$("#resep_t_diskon").attr("disabled", "disabled");
			$("#obat_racikan_nama_apoteker").removeAttr("disabled");
			$("#obat_jadi_add").show();
			$("#obat_racikan_add").show();
			$("tbody#dresep_list").html("");
			$("tbody#bahan_racikan_list").html("");
			$("#modal_alert_resep_add_form").html("");
			$(".error_field").removeClass("error_field");
			self.refreshBiayaTotal();
			$("#resep_save").removeAttr("onclick");
			$("#resep_save").attr("onclick", "resep.save()");
			$("#resep_save").show();
			$("#resep_ok").hide();
			var d_data = self.getRegulerData();
			d_data['super_command'] = "resep";
			d_data['command'] = "get_start_time";
			$.post(
				"",
				d_data,
				function(rspns) {
					var jsn = JSON.parse(rspns);
					if (jsn == null) return;
					$("#resep_start_time").val(jsn);
					$("#resep_add_form").smodal("show");
					$(".btn").removeAttr("disabled");
				}
			);
		}
	);
};
ResepAction.prototype.print_all_prescription = function(id) {
	if ($(".btn").attr("disabled") == "disabled")
		return;
	$(".btn").removeAttr("disabled");
	$(".btn").attr("disabled", "disabled");
	var data = this.getRegulerData();
	data['super_command'] = "resep";
	data['command'] = "print_claim_prescription";
	data['id'] = id;
	$.post(
		"",
		data,
		function(response) {
			var json = getContent(response);
			if (json == null) return;
			smis_print(json);
			// webprint(json);
			$(".btn").removeAttr("disabled");
		}
	);
};
ResepAction.prototype.print_all_etiket = function (id) {
	if ($(".btn").attr("disabled") == "disabled")
		return;
	$(".btn").removeAttr("disabled");
	$(".btn").attr("disabled", "disabled");
	var data = this.getRegulerData();
	data['super_command'] = "resep";
	data['command'] = "print_all_etiket";
	data['id'] = id;
	showLoading();
	$.post(
		"",
		data,
		function (response) {
			var json = getContent(response);
			if (json == null) {
				dismissLoading();
				$(".btn").removeAttr("disabled");
				return;
			}
			var getUrl = window.location['pathname'] + json;
			window.open(getUrl, 'pdf');
			dismissLoading();
			$(".btn").removeAttr("disabled");
		}
	);
};
ResepAction.prototype.print_etiket_obat_jadi = function (id) {
	if ($(".btn").attr("disabled") == "disabled")
		return;
	$(".btn").removeAttr("disabled");
	$(".btn").attr("disabled", "disabled");
	var data = this.getRegulerData();
	data['super_command'] = "resep";
	data['command'] = "print_etiket_obat_jadi";
	data['id'] = id;
	showLoading();
	$.post(
		"",
		data,
		function (response) {
			var json = getContent(response);
			if (json == null) {
				dismissLoading();
				$(".btn").removeAttr("disabled");
				return;
			}
			var getUrl = window.location['pathname'] + json;
			window.open(getUrl, 'pdf');
			dismissLoading();
			$(".btn").removeAttr("disabled");
		}
	);
};
ResepAction.prototype.print_etiket_obat_racikan = function (id) {
	if ($(".btn").attr("disabled") == "disabled")
		return;
	$(".btn").removeAttr("disabled");
	$(".btn").attr("disabled", "disabled");
	var data = this.getRegulerData();
	data['super_command'] = "resep";
	data['command'] = "print_etiket_obat_racikan";
	data['id'] = id;
	showLoading();
	$.post(
		"",
		data,
		function (response) {
			var json = getContent(response);
			if (json == null) {
				dismissLoading();
				$(".btn").removeAttr("disabled");
				return;
			}
			var getUrl = window.location['pathname'] + json;
			window.open(getUrl, 'pdf');
			dismissLoading();
			$(".btn").removeAttr("disabled");
		}
	);
};