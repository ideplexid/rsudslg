function PengaturanMarginAction(name, page, action, column) {
	this.initialize(name, page, action, column);
}
PengaturanMarginAction.prototype.constructor = PengaturanMarginAction;
PengaturanMarginAction.prototype = new TableAction();
PengaturanMarginAction.prototype.edit = function(id) {
	var self = this;
	showLoading();	
	var edit_data = this.getEditData(id);
	$.post(
		"",
		edit_data,
		function(res) {		
		var json = getContent(res);
		if (json == null) return;
		for (var i = 0; i < self.column.length; i++) {
			if ($.inArray(self.column[i],self.noclear) !=-1 && !self.edit_clear_for_no_clear) {
				continue;
			}
			var name = self.column[i];
			var the_id = "#"+self.prefix+"_" + name;
			smis_edit( the_id, json["" + name]);
		}

		var data = self.getRegulerData();
		data['command'] = "get_max_hpp";
		data['super_command'] = "";
		data['id_obat'] = $("#pengaturan_margin_id_obat").val();
		$.post(
			"",
			data,
			function(response) {
				var jsn = JSON.parse(response);
				if (jsn == null) {
					dismissLoading();
					return;
				}
				$("#pengaturan_margin_hpp_max").val(jsn.hpp);

				var v_hpp = jsn.hpp.replace(/[^0-9-,]/g, '').replace(",", ".");
				var v_margin = $("#pengaturan_margin_margin_jual").val();
				var v_hja = parseFloat(v_hpp) + parseFloat(v_margin * v_hpp / 100);
				$("#pengaturan_margin_hja").val(parseFloat(v_hja).formatMoney("2", ".", ","));

				dismissLoading();
				self.disabledOnEdit(self.column_disabled_on_edit);
				self.show_form();
			}
		);
	});
}