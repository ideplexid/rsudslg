function DPenjualanBebasAction(name, page, action, column) {
	this.initialize(name, page, action, column);
}
DPenjualanBebasAction.prototype.constructor = DPenjualanBebasAction;
DPenjualanBebasAction.prototype = new TableAction();
DPenjualanBebasAction.prototype.show_add_obat_jadi_form = function() {
	if ($(".btn").attr("disabled") == "disabled")
		return;
	$(".btn").removeAttr("disabled");
	$(".btn").attr("disabled", "disabled");
	$("#obat_jadi_id").val("");
	$("#obat_jadi_id_obat").val("");
	$("#obat_jadi_kode_obat").val("");
	$("#obat_jadi_name_obat").val("");
	$("#obat_jadi_nama_obat").val("");
	$("#obat_jadi_nama_jenis_obat").val("");
	$("#obat_jadi_satuan").removeAttr("onchange");
	$("#obat_jadi_satuan").attr("onchange", "obat.setDetailInfo()");
	$("#obat_jadi_satuan").html("");
	$("#obat_jadi_stok").val("");
	$("#obat_jadi_f_stok").val("");
	$("#obat_jadi_jumlah_lama").val(0);
	$("#obat_jadi_hna").val("");
	$("#obat_jadi_embalase").val("Rp. " + parseFloat(embalase_obat_jadi).formatMoney("2", ".", ","));
	$("#obat_jadi_tuslah").val("Rp. " + parseFloat(tuslah_obat_jadi).formatMoney("2", ".", ","));
	$("#obat_jadi_markup").val(0);
	$("#obat_jadi_jumlah").val("");
	$("#obat_jadi_save").removeAttr("onclick");
	$("#obat_jadi_save").attr("onclick", "dpenjualan_bebas.save_obat_jadi()");
	$("#modal_alert_obat_jadi_add_form").html("");
	$(".error_field").removeClass("error_field");
	$("#obat_jadi_add_form").smodal("show");
	$(".btn").removeAttr("disabled");
};
DPenjualanBebasAction.prototype.validate_obat_jadi = function() {
	var valid = true;
	var invalid_msg = "";
	var nama_obat = $("#obat_jadi_name_obat").val();
	var satuan = $("#obat_jadi_satuan").val();
	var stok = $("#obat_jadi_stok").val();
	var jumlah_lama = $("#obat_jadi_jumlah_lama").val();
	var jumlah = $("#obat_jadi_jumlah").val();
	var harga = $("#obat_jadi_hna").val();
	$(".error_field").removeClass("error_field");
	if (nama_obat == "") {
		valid = false;
		invalid_msg += "</br><strong>Nama Obat</strong> tidak boleh kosong";
		$("#obat_jadi_nama_obat").addClass("error_field");
		$("#obat_jadi_nama_obat").focus();
	}
	if (satuan == "") {
		valid = false;
		invalid_msg += "</br><strong>Satuan</strong> tidak boleh kosong";
		$("#obat_jadi_satuan").addClass("error_field");
		$("#obat_jadi_satuan").focus();
	}
	if (jumlah == "") {
		valid = false;
		invalid_msg += "</br><strong>Jumlah</strong> tidak boleh kosong";
		$("#obat_jadi_jumlah").addClass("error_field");
		$("#obat_jadi_jumlah").focus();
	} else if (!is_numeric(jumlah)) {
		valid = false;
		invalid_msg += "</br><strong>Jumlah</strong> seharusnya numerik (0-9)";
		$("#obat_jadi_jumlah").addClass("error_field");
		$("#obat_jadi_jumlah").focus();
	} else if (stok != "" && is_numeric(stok) && (parseFloat(jumlah) - parseFloat(jumlah_lama)) > parseFloat(stok)) {
		valid = false;
		invalid_msg += "</br><strong>Jumlah</strong> tidak boleh melebihi stok";
		$("#obat_jadi_jumlah").addClass("error_field");
		$("#obat_jadi_jumlah").focus();
	}
	if (harga == 0 || harga == "") {
		valid = false;
		invalid_msg += "</br><strong>Harga Netto</strong> tidak boleh kosong";
		$("#obat_jadi_hna").addClass("error_field");
		$("#obat_jadi_jumlah").focus();
	}
	if (!valid) {
		$("#modal_alert_obat_jadi_add_form").html(
			"<div class='alert alert-block alert-danger'>" +
				"<h4>Peringatan</h4>" +
				invalid_msg +
			"</div>"
		);
	}
	return valid;
};
DPenjualanBebasAction.prototype.save_obat_jadi = function() {
	if ($(".btn").attr("disabled") == "disabled")
		return;
	$(".btn").removeAttr("disabled");
	$(".btn").attr("disabled", "disabled");
	if (!this.validate_obat_jadi()) {
		$(".btn").removeAttr("disabled");
		return;
	}
	var dpenjualan_bebas_id = $("#obat_jadi_id").val();
	var dpenjualan_bebas_id_obat = $("#obat_jadi_id_obat").val();
	var dpenjualan_bebas_kode_obat = $("#obat_jadi_kode_obat").val();
	var dpenjualan_bebas_nama_obat = $("#obat_jadi_name_obat").val();
	var dpenjualan_bebas_nama_jenis_obat = $("#obat_jadi_nama_jenis_obat").val();
	var dpenjualan_bebas_jumlah_lama = $("#obat_jadi_jumlah_lama").val();
	var dpenjualan_bebas_jumlah = $("#obat_jadi_jumlah").val();
	var dpenjualan_bebas_satuan = $("#obat_jadi_satuan").find(":selected").text();
	var dpenjualan_bebas_konversi = $("#obat_jadi_konversi").val();
	var dpenjualan_bebas_satuan_konversi = $("#obat_jadi_satuan_konversi").val();
	var dpenjualan_bebas_hna = $("#obat_jadi_hna").val();
	var dpenjualan_bebas_embalase = $("#obat_jadi_embalase").val();
	var dpenjualan_bebas_tuslah = $("#obat_jadi_tuslah").val();
	var dpenjualan_bebas_markup = $("#obat_jadi_markup").val();
	$("tbody#dpenjualan_bebas_list").append(
		"<tr id='dpenjualan_bebas_" + dpenjualan_bebas_num + "'>" +
			"<td id='dpenjualan_bebas_" + dpenjualan_bebas_num + "_id' style='display: none;'>" + dpenjualan_bebas_id + "</td>" +
			"<td id='dpenjualan_bebas_" + dpenjualan_bebas_num + "_tipe' style='display: none;'>obat_jadi</td>" +
			"<td id='dpenjualan_bebas_" + dpenjualan_bebas_num + "_id_obat' style='display: none;'>" + dpenjualan_bebas_id_obat + "</td>" +
			"<td id='dpenjualan_bebas_" + dpenjualan_bebas_num + "_kode_obat' style='display: none;'>" + dpenjualan_bebas_kode_obat + "</td>" +
			"<td id='dpenjualan_bebas_" + dpenjualan_bebas_num + "_nama_jenis_obat' style='display: none;'>" + dpenjualan_bebas_nama_jenis_obat + "</td>" +
			"<td id='dpenjualan_bebas_" + dpenjualan_bebas_num + "_jumlah' style='display: none;'>" + dpenjualan_bebas_jumlah + "</td>" +
			"<td id='dpenjualan_bebas_" + dpenjualan_bebas_num + "_jumlah_lama' style='display: none;'>" + dpenjualan_bebas_jumlah_lama + "</td>" +
			"<td id='dpenjualan_bebas_" + dpenjualan_bebas_num + "_satuan' style='display: none;'>" + dpenjualan_bebas_satuan + "</td>" +
			"<td id='dpenjualan_bebas_" + dpenjualan_bebas_num + "_konversi' style='display: none;'>" + dpenjualan_bebas_konversi + "</td>" +
			"<td id='dpenjualan_bebas_" + dpenjualan_bebas_num + "_satuan_konversi' style='display: none;'>" + dpenjualan_bebas_satuan_konversi + "</td>" +
			"<td id='dpenjualan_bebas_" + dpenjualan_bebas_num + "_markup' style='display: none;'>" + dpenjualan_bebas_markup + "</td>" +
			"<td id='dpenjualan_bebas_" + dpenjualan_bebas_num + "_hna' style='display: none;'>" + dpenjualan_bebas_hna + "</td>" +
			"<td id='dpenjualan_bebas_" + dpenjualan_bebas_num + "_nomor'></td>" +
			"<td id='dpenjualan_bebas_" + dpenjualan_bebas_num + "_nama_obat'>" + dpenjualan_bebas_nama_obat + "</td>" +
			"<td id='dpenjualan_bebas_" + dpenjualan_bebas_num + "_f_jumlah'>" + dpenjualan_bebas_jumlah + " " + dpenjualan_bebas_satuan + "</td>" +
			"<td id='dpenjualan_bebas_" + dpenjualan_bebas_num + "_harga'></td>" +
			"<td id='dpenjualan_bebas_" + dpenjualan_bebas_num + "_embalase'>"  + dpenjualan_bebas_embalase + "</td>" +
			"<td id='dpenjualan_bebas_" + dpenjualan_bebas_num + "_tuslah'>"  + dpenjualan_bebas_tuslah + "</td>" +
			"<td id='dpenjualan_bebas_" + dpenjualan_bebas_num + "_subtotal'></td>" +
			"<td id='dpenjualan_bebas_" + dpenjualan_bebas_num + "_apoteker'>-</td>" +
			"<td>" +
				"<div class='btn-group noprint'>" +
					"<a href='#' onclick='dpenjualan_bebas.edit_obat_jadi(" + dpenjualan_bebas_num + ")' data-content='Ubah' data-toggle='popover' class='input btn btn-warning'>" + 
						"<i class='icon-edit icon-white'></i>" +
					"</a>" +
					"<a href='#' onclick='dpenjualan_bebas.delete(" + dpenjualan_bebas_num + ")' data-content='Hapus' data-toggle='popover' class='input btn btn-danger'>" + 
						"<i class='icon-remove icon-white'></i>" + 
					"</a>" +
				"</div>" +
			"</td>" +
		"</tr>"
	);
	dpenjualan_bebas_num++;
	$("#obat_jadi_id").val("");
	$("#obat_jadi_id_obat").val("");
	$("#obat_jadi_kode_obat").val("");
	$("#obat_jadi_name_obat").val("");
	$("#obat_jadi_nama_obat").val("");
	$("#obat_jadi_nama_jenis_obat").val("");
	$("#obat_jadi_satuan").removeAttr("onchange");
	$("#obat_jadi_satuan").attr("onchange", "obat.setDetailInfo()");
	$("#obat_jadi_satuan").html("");
	$("#obat_jadi_stok").val("");
	$("#obat_jadi_f_stok").val("");
	$("#obat_jadi_jumlah_lama").val(0);
	$("#obat_jadi_hna").val("");
	$("#obat_jadi_markup").val(0);
	$("#obat_jadi_jumlah").val("");
	$("#obat_jadi_save").removeAttr("onclick");
	$("#obat_jadi_save").attr("onclick", "dpenjualan_bebas.save_obat_jadi()");
	$("#modal_alert_obat_jadi_add_form").html("");
	$(".error_field").removeClass("error_field");
	$("#obat_jadi_nama_obat").focus();
	penjualan_bebas.refreshHargaAndSubtotal();
	penjualan_bebas.refresh_no_dpenjualan_bebas();
	$(".btn").removeAttr("disabled");	
};
DPenjualanBebasAction.prototype.edit_obat_jadi = function(r_num) {
	if ($(".btn").attr("disabled") == "disabled")
		return;
	$(".btn").removeAttr("disabled");
	$(".btn").attr("disabled", "disabled");
	var dpenjualan_bebas_id = $("#dpenjualan_bebas_" + r_num + "_id").text();
	var dpenjualan_bebas_id_obat = $("#dpenjualan_bebas_" + r_num + "_id_obat").text();
	var dpenjualan_bebas_kode_obat = $("#dpenjualan_bebas_" + r_num + "_kode_obat").text();
	var dpenjualan_bebas_nama_obat = $("#dpenjualan_bebas_" + r_num + "_nama_obat").text();
	var dpenjualan_bebas_nama_jenis_obat = $("#dpenjualan_bebas_" + r_num + "_nama_jenis_obat").text();
	var dpenjualan_bebas_jumlah_lama = $("#dpenjualan_bebas_" + r_num + "_jumlah_lama").text();
	var dpenjualan_bebas_jumlah = $("#dpenjualan_bebas_" + r_num + "_jumlah").text();
	var dpenjualan_bebas_satuan = $("#dpenjualan_bebas_" + r_num + "_satuan").text();
	var dpenjualan_bebas_konversi = $("#dpenjualan_bebas_" + r_num + "_konversi").text();
	var dpenjualan_bebas_satuan_konversi = $("#dpenjualan_bebas_" + r_num + "_satuan_konversi").text();
	var dpenjualan_bebas_hna = $("#dpenjualan_bebas_" + r_num + "_hna").text();
	var dpenjualan_bebas_embalase = $("#dpenjualan_bebas_" + r_num + "_embalase").text();
	var dpenjualan_bebas_tuslah = $("#dpenjualan_bebas_" + r_num + "_tuslah").text();
	var dpenjualan_bebas_markup = $("#dpenjualan_bebas_" + r_num + "_markup").text();
	$("#obat_jadi_id").val(dpenjualan_bebas_id);
	$("#obat_jadi_jumlah_lama").val(dpenjualan_bebas_jumlah_lama);
	$("#obat_jadi_jumlah").val(dpenjualan_bebas_jumlah);
	var self = this;
	var data = this.getRegulerData();
	data['super_command'] = "obat";
	data['command'] = "edit";
	data['id'] = dpenjualan_bebas_id_obat;
	$.post(
		"",
		data,
		function(response) {
			var json = getContent(response);
			if (json == null) return;
			$("#obat_jadi_id_obat").val(json.header.id_obat);
			$("#obat_jadi_kode_obat").val(json.header.kode_obat);
			$("#obat_jadi_name_obat").val(json.header.nama_obat);
			$("#obat_jadi_nama_obat").val(json.header.nama_obat);
			$("#obat_jadi_nama_jenis_obat").val(json.header.nama_jenis_obat);
			$("#obat_jadi_satuan").html(json.satuan_option);
			$("#obat_jadi_satuan").val(dpenjualan_bebas_konversi + "_" + dpenjualan_bebas_satuan_konversi);
			$("#obat_jadi_embalase").val(dpenjualan_bebas_embalase);
			$("#obat_jadi_tuslah").val(dpenjualan_bebas_tuslah);
			var part = $("#obat_jadi_satuan").val().split("_");
			$("#obat_jadi_konversi").val(part[0]);
			$("#obat_jadi_satuan_konversi").val(part[1]);
			data = self.getRegulerData();
			data['super_command'] = "sisa";
			data['command'] = "edit";
			data['id_obat'] = $("#obat_jadi_id_obat").val();
			data['satuan'] = $("#obat_jadi_satuan").find(":selected").text();
			data['konversi'] = $("#obat_jadi_konversi").val();
			data['satuan_konversi'] = $("#obat_jadi_satuan_konversi").val();
			$.post(
				"",
				data,
				function(response) {
					var json = getContent(response);
					if (json == null) return;
					$("#obat_jadi_stok").val(json.sisa);
					$("#obat_jadi_f_stok").val(json.sisa + " " + json.satuan);						
					var hna = parseFloat(json.hna) * (1 + parseFloat(json.markup) / 100);
					hna = "Rp. " + (parseFloat(hna)).formatMoney("2", ".", ",");
					$("#obat_jadi_hna").val(hna);
					$("#obat_jadi_markup").val(json.markup);
					$("#obat_jadi_satuan").removeAttr("onchange");
					$("#obat_jadi_satuan").attr("onchange", "obat.setDetailInfo()");
					$("#modal_alert_obat_jadi_add_form").html("");
					$(".error_field").removeClass("error_field");
					$("#obat_jadi_save").removeAttr("onclick");
					$("#obat_jadi_save").attr("onclick", "dpenjualan_bebas.update_obat_jadi(" + r_num + ")");
					$("#obat_jadi_add_form").smodal("show");
					$(".btn").removeAttr("disabled");
				}
			);
		}
	);
};
DPenjualanBebasAction.prototype.update_obat_jadi = function(r_num) {
	if ($(".btn").attr("disabled") == "disabled")
		return;
	$(".btn").removeAttr("disabled");
	$(".btn").attr("disabled", "disabled");
	if (!this.validate_obat_jadi()) {
		$(".btn").removeAttr("disabled");
		return;
	}
	var dpenjualan_bebas_id_obat = $("#obat_jadi_id_obat").val();
	var dpenjualan_bebas_kode_obat = $("#obat_jadi_kode_obat").val();
	var dpenjualan_bebas_nama_obat = $("#obat_jadi_name_obat").val();
	var dpenjualan_bebas_nama_jenis_obat = $("#obat_jadi_nama_jenis_obat").val();
	var dpenjualan_bebas_jumlah = $("#obat_jadi_jumlah").val();
	var dpenjualan_bebas_satuan = $("#obat_jadi_satuan").find(":selected").text();
	var dpenjualan_bebas_konversi = $("#obat_jadi_konversi").val();
	var dpenjualan_bebas_satuan_konversi = $("#obat_jadi_satuan_konversi").val();
	var dpenjualan_bebas_hna = $("#obat_jadi_hna").val();
	var dpenjualan_bebas_embalase = $("#obat_jadi_embalase").val();
	var dpenjualan_bebas_tuslah = $("#obat_jadi_tuslah").val();
	var dpenjualan_bebas_markup = $("#obat_jadi_markup").val();
	$("#dpenjualan_bebas_" + r_num + "_id_obat").text(dpenjualan_bebas_id_obat);
	$("#dpenjualan_bebas_" + r_num + "_kode_obat").text(dpenjualan_bebas_kode_obat);
	$("#dpenjualan_bebas_" + r_num + "_nama_obat").text(dpenjualan_bebas_nama_obat);
	$("#dpenjualan_bebas_" + r_num + "_nama_jenis_obat").text(dpenjualan_bebas_nama_jenis_obat);
	$("#dpenjualan_bebas_" + r_num + "_jumlah").text(dpenjualan_bebas_jumlah);
	$("#dpenjualan_bebas_" + r_num + "_satuan").text(dpenjualan_bebas_satuan);
	$("#dpenjualan_bebas_" + r_num + "_konversi").text(dpenjualan_bebas_konversi);
	$("#dpenjualan_bebas_" + r_num + "_satuan_konversi").text(dpenjualan_bebas_satuan_konversi);
	$("#dpenjualan_bebas_" + r_num + "_hna").text(dpenjualan_bebas_hna);
	$("#dpenjualan_bebas_" + r_num + "_markup").text(dpenjualan_bebas_markup);
	$("#dpenjualan_bebas_" + r_num + "_embalase").text(dpenjualan_bebas_embalase);
	$("#dpenjualan_bebas_" + r_num + "_tuslah").text(dpenjualan_bebas_tuslah);
	$("#dpenjualan_bebas_" + r_num + "_f_jumlah").text(dpenjualan_bebas_jumlah + " " + dpenjualan_bebas_satuan);
	penjualan_bebas.refreshHargaAndSubtotal();
	$("#obat_jadi_add_form").smodal("hide");
	$(".btn").removeAttr("disabled");
};
DPenjualanBebasAction.prototype.delete = function(r_num) {
	var id = $("#dpenjualan_bebas_" + r_num + "_id").text();
	if (id.length == 0) {
		$("#dpenjualan_bebas_" + r_num).remove();
	} else {
		$("#dpenjualan_bebas_" + r_num).attr("style", "display: none;");
		$("#dpenjualan_bebas_" + r_num).attr("class", "deleted");
	}
	penjualan_bebas.refreshBiayaTotal();
	penjualan_bebas.refresh_no_dpenjualan_bebas();
};