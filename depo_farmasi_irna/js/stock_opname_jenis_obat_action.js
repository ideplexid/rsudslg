function SOJenisObatAction(name, page, action, column) {
	this.initialize(name, page, action, column);
}
SOJenisObatAction.prototype.constructor = SOJenisObatAction;
SOJenisObatAction.prototype = new TableAction();
SOJenisObatAction.prototype.selected = function(json) {
	$("#so_kode_jenis_obat").val(json.kode);
	$("#so_nama_jenis_obat").val(json.nama);
};