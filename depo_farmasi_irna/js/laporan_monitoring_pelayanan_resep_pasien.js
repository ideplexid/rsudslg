var lmprp;
var FINISHED;
$(document).ready(function() {
	$('.mydatetime').datetimepicker({ 
		minuteStep: 1
	});
	lmprp = new LMPRPAction(
		"lmprp",
		"depo_farmasi_irna",
		"laporan_monitoring_pelayanan_resep_pasien",
		new Array()
	);
	$("#lmprp_list").append(
		"<tr id='temp'>" +
			"<td colspan='15'><strong><small><center>DATA MONITORING PELAYANAN RESEP PASIEN BELUM DIPROSES</center></small></strong></td>" +
		"</tr>"
	);
	$(document).keyup(function(e) {
		if (e.which == 27) {
			FINISHED = true;
		}
	});
});