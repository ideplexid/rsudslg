var retur_obat_per_pasien;
var detail_retur_obat_per_pasien;
var pasien;
$(document).ready(function() {
    pasien = new TableAction(
        "pasien",
        "depo_farmasi_irna",
        "retur_obat_per_pasien",
        new Array()
    );
    pasien.setSuperCommand("pasien");
    pasien.selected = function(json) {
        $("#retur_obat_per_pasien_noreg_pasien").val(json.noreg_pasien);
        $("#retur_obat_per_pasien_nrm_pasien").val(json.nrm_pasien);
        $("#retur_obat_per_pasien_nama_pasien").val(json.nama_pasien);
        $("#retur_obat_per_pasien_alamat_pasien").val(json.alamat_pasien);
        $("#retur_obat_per_pasien_dokter").val(json.daftar_dokter);
        $("#detail_retur_obat_per_pasien_list").html(json.detail_html);
        $("#search_panel").show();
    };
    retur_obat_per_pasien = new ReturObatPerPasienAction(
        "retur_obat_per_pasien",
        "depo_farmasi_irna",
        "retur_obat_per_pasien",
        new Array()
    );
    detail_retur_obat_per_pasien = new DetailReturObatPerPasienAction(
        "detail_retur_obat_per_pasien",
        "depo_farmasi_irna",
        "detail_retur_obat_per_pasien",
        new Array()
    );
    $("#search_nama_obat").keypress(function(e) {
        if(e.which == 13) {
            retur_obat_per_pasien.search_obat();
        }
    });

    retur_obat_per_pasien.view();
    retur_obat_per_pasien.clear();
});