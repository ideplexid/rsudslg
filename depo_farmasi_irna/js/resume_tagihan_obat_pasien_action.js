function PasienAction(name, page, action, column) {
	this.initialize(name, page, action, column);
}
PasienAction.prototype.constructor = PasienAction;
PasienAction.prototype = new TableAction();
PasienAction.prototype.addViewData = function(d) {
	d['noreg_pasien'] = $("#search_noreg").val();
	d['nrm_pasien'] = $("#search_nrm").val();
	d['nama_pasien'] = $("#search_nama").val();
	d['jenis_pasien'] = $("#search_carabayar").val();
	d['nama_kecamatan'] = $("#search_kecamatan").val();
	d['tanggal'] = $("#search_tanggal").val();
	return  d;
};
PasienAction.prototype.selected = function(json) {
	$("#rtop_nama_pasien").val(json.nama_pasien);
	$("#rtop_nrm_pasien").val(json.nrm);
	$("#rtop_noreg_pasien").val(json.id);
	$("#rtop_tanggal_daftar").val(json.tanggal);
};