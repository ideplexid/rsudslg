<?php
	global $db;
	require_once("smis-base/smis-include-service-consumer.php");
	require_once("depo_farmasi_irna/table/PasienTable.php");

	$form = new Form("", "", "Depo Depo Farmasi IRNA : Resume Tagihan Obat Pasien");
	$nama_pasien_text = new Text("rtop_nama_pasien", "rtop_nama_pasien", "");
	$nama_pasien_text->setAtribute("disabled='disabled'");
	$nama_pasien_text->setClass("smis-one-option-input");
	$browse_button = new Button("", "", "Pilih");
	$browse_button->setClass("btn-info");
	$browse_button->setIcon("fa fa-list");
	$browse_button->setIsButton(Button::$ICONIC);
	$browse_button->setAction("pasien.chooser('pasien', 'pasien_button', 'pasien', pasien)");
	$input_group = new InputGroup("");
	$input_group->addComponent($nama_pasien_text);
	$input_group->addComponent($browse_button);
	$form->addElement("Nama Pasien", $input_group);
	$noreg_pasien_text = new Text("rtop_noreg_pasien", "rtop_noreg_pasien", "");
	$noreg_pasien_text->setAtribute("disabled='disabled'");
	$form->addElement("No. Reg.", $noreg_pasien_text);
	$nrm_pasien_text = new Text("rtop_nrm_pasien", "rtop_nrm_pasien", "");
	$nrm_pasien_text->setAtribute("disabled='disabled'");
	$form->addElement("No. RM", $nrm_pasien_text);
	$tanggal_daftar_text = new Text("rtop_tanggal_daftar", "rtop_tanggal_daftar", "");
	$tanggal_daftar_text->setAtribute("disabled='disabled'");
	$form->addElement("Tanggal Daftar", $tanggal_daftar_text);
	$show_button = new Button("", "", "Tampilkan");
	$show_button->setClass("btn-primary");
	$show_button->setIcon("icon-white icon-repeat");
	$show_button->setIsButton(Button::$ICONIC);
	$show_button->setAction("rtop.view()");
	$btn_group = new ButtonGroup("noprint");
	$btn_group->addButton($show_button);
	$form->addElement("", $btn_group);

	$pasien_table = new PasienTable(
		array("No. Reg.", "Tgl. Daftar", "NRM", "Nama", "Kecamatan", "Jns. Pasien"),
		"",
		null,
		true
	);
	$pasien_table->setName("pasien");
	$pasien_table->setModel(Table::$SELECT);
	$noreg_s_text = new Text("search_noreg", "search_noreg", "");
	$noreg_s_text->setClass("search search-header-tiny search-text");
	$tanggal_s_text = new Text("search_tanggal", "search_tanggal", "");
	$tanggal_s_text->setClass("search search-header-tiny search-text");
	$nrm_s_text = new Text("search_nrm", "search_nrm", "");
	$nrm_s_text->addAtribute("autofocus");
	$nrm_s_text->setClass("search search-header-tiny search-text");
	$nama_s_text = new Text("search_nama", "search_nama", "");
	$nama_s_text->setClass("search search-header-med search-text ");
	$kecamatan_s_text = new Text("search_kecamatan", "search_kecamatan", "");
	$kecamatan_s_text->setClass("search search-header-big search-text");
	$carabayar_s_text = new Text("search_carabayar", "search_carabayar", "");
	$carabayar_s_text->setClass("search search-header-tiny search-text");
	$header = "<tr class = 'header_pasien'>" .
					"<td>" . $noreg_s_text->getHtml() . "</td>" .
					"<td>" . $tanggal_s_text->getHtml() . "</td>" .
					"<td>" . $nrm_s_text->getHtml() . "</td>" .
					"<td>" . $nama_s_text->getHtml() . "</td>" .
					"<td>" . $kecamatan_s_text->getHtml() . "</td>" .
					"<td>" . $carabayar_s_text->getHtml() . "</td>" .
			  "</tr>";
	$pasien_table->addHeader("after", $header);
	$pasien_adapter = new SimpleAdapter();
	$pasien_adapter->add("No. Reg.", "id", "digit8");
	$pasien_adapter->add("Tgl. Daftar", "tanggal", "date d-m-Y, H:i:s");
	$pasien_adapter->add("NRM", "nrm", "digit6");
	$pasien_adapter->add("Nama", "nama_pasien");
	$pasien_adapter->add("Kecamatan", "nama_kecamatan");
	$pasien_adapter->add("Jns. Pasien", "carabayar", "unslug");
	$pasien_service_responder = new ServiceResponder(
		$db,
		$pasien_table,
		$pasien_adapter,
		"get_registered_all"
	);

	$super_command = new SuperCommand();
	$super_command->addResponder("pasien", $pasien_service_responder);
	$init = $super_command->initialize();
	if ($init != null) {
		echo $init;
		return;
	}

	if (isset($_POST['command'])) {
		if ($_POST['command'] == "get_resume") {
			$noreg_pasien = $_POST['noreg_pasien'];
			$body_html = "";
			$footer_html = "";
			$params = array(
				"noreg_pasien" => $noreg_pasien
			);
			$total = 0;
			$depo_farmasi = "depo_farmasi_irna";
			$service_consumer = new ServiceConsumer(
				$db,
				"get_tagihan",
				$params,
				$depo_farmasi
			);
			$service_consumer->setMode(ServiceConsumer::$CLEAN_BOTH);
			$content = $service_consumer->execute()->getContent();
			if ($content != null) {
				foreach ($content[5]['penjualan_resep']['result'] as $tagihan_resep) {
					$body_html .= "
						<tr>
							<td>" . ArrayAdapter::format("unslug", $depo_farmasi) . "</td>
							<td>" . $tagihan_resep['waktu'] . "</td>
							<td>" . $tagihan_resep['nama'] . "</td>
							<td>" . $tagihan_resep['keterangan']['dokter'] . "</td>
							<td>" . ArrayAdapter::format("money", ceil($tagihan_resep['biaya'])) . "</td>
						</tr>
					";
					$total += $tagihan_resep['biaya'];
				}
			}
			$footer_html .= "
				<tr>
					<td colspan='4'><center><strong>T O T A L</strong></center></td>
					<td><strong>" . ArrayAdapter::format("money", $total) . "</strong></td>
				</tr>
			";
			$data = array(
				"body_html"		=> $body_html,
				"footer_html"	=> $footer_html
			);
			echo json_encode($data);
		}
		return;
	}

	$table = new Table(
		array("Depo Farmasi", "Tanggal", "Nama Tagihan", "Dokter", "Biaya"),
		"",
		null,
		true
	);
	$table->setName("resume");
	$table->setAction(false);
	$table->setFooterVisible(false);

	echo $form->getHtml();
	echo $table->getHtml();
	echo addCSS("depo_farmasi_irna/css/resume_tagihan_obat.css", false);
	echo addJS("framework/smis/js/table_action.js");
	echo addJS("depo_farmasi_irna/js/resume_tagihan_obat_action.js", false);
	echo addJS("depo_farmasi_irna/js/resume_tagihan_obat_pasien_action.js", false);
	echo addJS("depo_farmasi_irna/js/resume_tagihan_obat.js", false);
?>