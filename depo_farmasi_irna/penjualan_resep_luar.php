<?php
	global $db;
	require_once("depo_farmasi_irna/library/InventoryLibrary.php");
	require_once("smis-base/smis-include-service-consumer.php");
	require_once("depo_farmasi_irna/table/PenjualanResepTable.php");
	require_once("depo_farmasi_irna/table/DResepTable.php");
	require_once("depo_farmasi_irna/table/DObatRacikanTable.php");
	require_once("depo_farmasi_irna/table/BahanRacikanTable.php");
	require_once("depo_farmasi_irna/adapter/PenjualanResepLuarAdapter.php");
	require_once("depo_farmasi_irna/adapter/ObatAdapter.php");
	require_once("depo_farmasi_irna/adapter/BahanAdapter.php");
	require_once("depo_farmasi_irna/responder/PenjualanResepLuarDBResponder.php");
	require_once("depo_farmasi_irna/responder/DokterServiceResponder.php");
	require_once("depo_farmasi_irna/responder/ObatDBResponder.php");
	require_once("depo_farmasi_irna/responder/ApotekerServiceResponder.php");
	require_once("depo_farmasi_irna/responder/BahanDBResponder.php");
	require_once("depo_farmasi_irna/responder/SisaDBResponder.php");

	$resep_table = new PenjualanResepTable(
		array("No. Penjualan", "No. Resep", "Tanggal/Jam", "Dokter", "Pasien", "Alamat", "Kat. Pasien", "Jns. Pasien", "Diberikan/Edukasi"),
		"Depo Farmasi IRNA : Penjualan Obat - Resep (Non-KIUP)",
		null,
		true
	);
	$resep_table->setName("resep");

	if (isset($_POST['super_command']) && $_POST['super_command'] == "resep") {
		if (isset($_POST['command'])) {
			if ($_POST['command'] == "get_start_time") {
				$data = array(
					'start_time' 		=> date("Y-m-d H:i:s"),
					'ppn'				=> getSettings($db, "depo_farmasi7-penjualan_resep_luar-ppn_jual", 11)
				);
				echo json_encode($data);
				return;
			}
			if ($_POST['command'] == "get_margin_penjualan") {
				/// mode margin :
				/// 0	=> paten
				/// 1 	=> jenis pasien
				/// 2 	=> per obat
				/// 3 	=> obat dan jenis pasien
				$mode_margin = $_POST['mode_margin'];
				$jenis_pasien = $_POST['jenis_pasien'];
				$margin_penjualan = 0;
				if ($mode_margin == 0)
					$margin_penjualan = getSettings($db, "depo_farmasi7-penjualan_resep_luar-margin_penjualan", 0) / 100;
				else if ($mode_margin == 1) {
					$dbtable = new DBTable($db, InventoryLibrary::$_TBL_MARGIN_JUAL_JENIS_PASIEN);
					$row = $dbtable->get_row("
							SELECT *
							FROM " . InventoryLibrary::$_TBL_MARGIN_JUAL_JENIS_PASIEN . "
							WHERE prop NOT LIKE 'del' AND slug = '" . $jenis_pasien . "'
						");
					if ($row != null)
						$margin_penjualan = $row->margin_jual / 100;
				}
				$data = array(
					"margin_penjualan" => $margin_penjualan
				);
				echo json_encode($data);
				return;
			}
			$resep_adapter = new PenjualanResepLuarAdapter();
			$resep_dbtable = new DBTable(
				$db,
				InventoryLibrary::$_TBL_PENJUALAN_RESEP
			);
			$resep_dbtable->addCustomKriteria(" tipe ", " ='resep_luar' ");
			$resep_dbtable->setOrder(" id DESC ");
			$resep_dbresponder = new PenjualanResepLuarDBResponder(
				$resep_dbtable,
				$resep_table,
				$resep_adapter
			);
			if ($resep_dbresponder->isSave()) {
				global $user;
				$resep_dbresponder->addColumnFixValue("operator", $user->getNameOnly());
			}
			$data = $resep_dbresponder->command($_POST['command']);
			echo json_encode($data);
			return;
		}
		return;
	}

	//dokter service consumer:
	$dokter_table = new Table(
		array("Nama", "Jabatan"),
		"",
		null,
		true
	);
	$dokter_table->setName("dokter");
	$dokter_table->setModel(Table::$SELECT);
	$dokter_adapter = new SimpleAdapter();
	$dokter_adapter->add("Nama", "nama");
	$dokter_adapter->add("Jabatan", "nama_jabatan");
	$dokter_service_responder = new DokterServiceResponder(
		$db,
		$dokter_table,
		$dokter_adapter,
		"employee"
	);

	//get obat chooser:
	$obat_table = new Table(array("Kode", "Obat", "Jenis", "Stok"));
	$obat_table->setName("obat");
	$obat_table->setModel(Table::$SELECT);
	$obat_adapter = new ObatAdapter();
	$obat_dbtable = new DBTable($db, InventoryLibrary::$_TBL_STOK_OBAT);
	$obat_dbtable->setViewForSelect(true);
	$filter = "";
	if (isset($_POST['kriteria'])) {
		$filter = " AND (nama_obat LIKE '%" . $_POST['kriteria'] . "%' OR nama_jenis_obat LIKE '%" . $_POST['kriteria'] . "%')";
	}
	$query_value = "
		SELECT v_stok.*, v_harga.hna, v_harga.markup
		FROM (
			SELECT id_obat AS 'id', kode_obat, nama_obat, nama_jenis_obat, GROUP_CONCAT(CONCAT(sisa, ' ', satuan, ' ', label) ORDER BY satuan, sisa, label ASC SEPARATOR ', ') AS 'stok', satuan, konversi, satuan_konversi
			FROM (
				SELECT id_obat, kode_obat, nama_obat, nama_jenis_obat, SUM(sisa) AS 'sisa', satuan, konversi, satuan_konversi, CASE label WHEN 'reguler' THEN '(R)' WHEN 'sito' THEN '(S)' ELSE '(K)' END AS 'label'
				FROM " . InventoryLibrary::$_TBL_STOK_OBAT . " a LEFT JOIN " . InventoryLibrary::$_TBL_OBAT_MASUK . " b ON a.id_obat_masuk = b.id
				WHERE a.prop NOT LIKE 'del' AND b.prop NOT LIKE 'del' AND b.status = 'sudah' AND a.sisa > 0 " . $filter . "
				GROUP BY id_obat, satuan, konversi, satuan_konversi, label
			) v_obat
			GROUP BY id_obat
		) v_stok LEFT JOIN (
			SELECT id_obat, nama_obat, nama_jenis_obat,  MAX(hna) AS 'hna', 0 AS 'markup', satuan, konversi, satuan_konversi
			FROM " . InventoryLibrary::$_TBL_STOK_OBAT . " a LEFT JOIN " . InventoryLibrary::$_TBL_OBAT_MASUK . " b ON a.id_obat_masuk = b.id
			WHERE a.prop NOT LIKE 'del' AND b.prop NOT LIKE 'del' AND b.status = 'sudah' " . $filter . "
			GROUP BY id_obat, nama_obat, nama_jenis_obat, satuan, konversi, satuan_konversi
		) v_harga ON v_stok.id = v_harga.id_obat AND v_stok.satuan = v_harga.satuan AND v_stok.konversi = v_harga.konversi AND v_stok.satuan_konversi = v_harga.satuan_konversi
	";
	$query_count = "
		SELECT COUNT(*)
		FROM (
			" . $query_value . "
		) v_obat
	";
	$obat_dbtable->setPreferredQuery(true, $query_value, $query_count);
	$obat_dbresponder = new ObatDBResponder(
		$obat_dbtable,
		$obat_table,
		$obat_adapter
	);

	//get sisa, hna, dan markup by id obat, satuan, konversi, satuan_konversi:
	$sisa_table = new Table(array("id_obat", "sisa", "satuan", "konversi", "satuan_konversi"));
	$sisa_table->setName("sisa");
	$sisa_adapter = new SimpleAdapter();
	$sisa_adapter->add("id_obat", "id_obat");
	$sisa_adapter->add("sisa", "sisa");
	$sisa_adapter->add("satuan", "satuan");
	$sisa_adapter->add("konversi", "konversi");
	$sisa_adapter->add("satuan_konversi", "satuan_konversi");
	$columns = array("id_obat", "sisa", "satuan", "konversi", "satuan_konversi");
	$sisa_dbtable = new DBTable(
		$db,
		InventoryLibrary::$_TBL_STOK_OBAT,
		$columns
	);
	$sisa_dbresponder = new SisaDBResponder(
		$sisa_dbtable,
		$sisa_table,
		$sisa_adapter
	);

	//apoteker service consumer:
	$apoteker_table = new Table(array("NIP", "Nama", "Jabatan"));
	$apoteker_table->setName("apoteker");
	$apoteker_table->setModel(Table::$SELECT);
	$apoteker_adapter = new SimpleAdapter();
	$apoteker_adapter->add("NIP", "nip");
	$apoteker_adapter->add("Nama", "nama");
	$apoteker_adapter->add("Jabatan", "nama_jabatan");
	$apoteker_service_responder = new ApotekerServiceResponder(
		$db,
		$apoteker_table,
		$apoteker_adapter,
		"employee"
	);

	//bahan chooser:
	$bahan_table = new Table(array("Kode", "Bahan", "Jenis", "Stok"));
	$bahan_table->setName("bahan");
	$bahan_table->setModel(Table::$SELECT);
	$bahan_adapter = new BahanAdapter();
	$bahan_dbtable = new DBTable($db, InventoryLibrary::$_TBL_STOK_OBAT);
	$bahan_dbtable->setViewForSelect(true);
	$filter = "";
	if (isset($_POST['kriteria'])) {
		$filter = " AND (nama_obat LIKE '%" . $_POST['kriteria'] . "%' OR nama_jenis_obat LIKE '%" . $_POST['kriteria'] . "%')";
	}
	$query_value = "
		SELECT v_stok.*, v_harga.hna, v_harga.markup
		FROM (
			SELECT id_obat AS 'id', kode_obat, nama_obat, nama_jenis_obat, GROUP_CONCAT(CONCAT(sisa, ' ', satuan, ' ', label) ORDER BY satuan, sisa, label ASC SEPARATOR ', ') AS 'stok', satuan, konversi, satuan_konversi
			FROM (
				SELECT id_obat, kode_obat, nama_obat, nama_jenis_obat, SUM(sisa) AS 'sisa', satuan, konversi, satuan_konversi, CASE label WHEN 'reguler' THEN '(R)' WHEN 'sito' THEN '(S)' ELSE '(K)' END AS 'label'
				FROM " . InventoryLibrary::$_TBL_STOK_OBAT . " a LEFT JOIN " . InventoryLibrary::$_TBL_OBAT_MASUK . " b ON a.id_obat_masuk = b.id
				WHERE a.prop NOT LIKE 'del' AND b.prop NOT LIKE 'del' AND b.status = 'sudah' AND a.sisa > 0 " . $filter . "
				GROUP BY id_obat, satuan, konversi, satuan_konversi, label
			) v_obat
			GROUP BY id_obat
		) v_stok LEFT JOIN (
			SELECT id_obat, nama_obat, nama_jenis_obat,  MAX(hna) AS 'hna', 0 AS 'markup', satuan, konversi, satuan_konversi
			FROM " . InventoryLibrary::$_TBL_STOK_OBAT . " a LEFT JOIN " . InventoryLibrary::$_TBL_OBAT_MASUK . " b ON a.id_obat_masuk = b.id
			WHERE a.prop NOT LIKE 'del' AND b.prop NOT LIKE 'del' AND b.status = 'sudah' " . $filter . "
			GROUP BY id_obat, nama_obat, nama_jenis_obat, satuan, konversi, satuan_konversi
		) v_harga ON v_stok.id = v_harga.id_obat AND v_stok.satuan = v_harga.satuan AND v_stok.konversi = v_harga.konversi AND v_stok.satuan_konversi = v_harga.satuan_konversi
	";
	$query_count = "
		SELECT COUNT(*)
		FROM (
			" . $query_value . "
		) v_obat
	";
	$bahan_dbtable->setPreferredQuery(true, $query_value, $query_count);
	$bahan_dbresponder = new BahanDBResponder(
		$bahan_dbtable,
		$bahan_table,
		$bahan_adapter
	);

	$super_command = new SuperCommand();
	$super_command->addResponder("dokter", $dokter_service_responder);
	$super_command->addResponder("obat", $obat_dbresponder);
	$super_command->addResponder("sisa", $sisa_dbresponder);
	$super_command->addResponder("apoteker", $apoteker_service_responder);
	$super_command->addResponder("bahan", $bahan_dbresponder);
	$init = $super_command->initialize();
	if ($init != null) {
		echo $init;
		return;
	}

	$resep_modal = new Modal("resep_add_form", "smis_form_container", "resep");
	$resep_modal->setTitle("Data Penjualan Resep Luar");
	$resep_modal->setClass(Modal::$FULL_MODEL);
	$id_hidden = new Hidden("resep_id", "resep_id", "");
	$resep_modal->addElement("", $id_hidden);
	$start_time_hidden = new Hidden("resep_start_time", "resep_start_time", "");
	$resep_modal->addElement("", $start_time_hidden);
	$ppn_hidden = new Hidden("resep_ppn", "resep_ppn", getSettings($db, "depo_farmasi7-penjualan_resep_luar-ppn_jual", 11));
	$resep_modal->addElement("", $ppn_hidden);
	$noresep_text = new Text("resep_nomor", "resep_nomor", "");
	$noresep_text->addAtribute("autofocus");
	$resep_modal->addElement("Nomor", $noresep_text);
	$pasien_text = new Text("resep_nama_pasien", "resep_nama_pasien", "");
	$resep_modal->addElement("Pasien", $pasien_text);
	$alamat_text = new Text("resep_alamat_pasien", "resep_alamat_pasien", "");
	$resep_modal->addElement("Alamat Pasien", $alamat_text);
	$uri_option = new OptionBuilder();
	$uri_option->add("Rawat Jalan (IRJA)", "0", "1");
	$uri_option->add("Rawat Inap (IRNA)", "1");
	$uri_select = new Select("resep_uri", "resep_uri", $uri_option->getContent());
	$resep_modal->addElement("IRJA/IRNA", $uri_select);
	if (getSettings($db, "depo_farmasi7-resep-jenis_pasien_registration", 0) == 1) {
		require_once("smis-base/smis-include-service-consumer.php");

		$jenis_service_consumer = new ServiceConsumer(
			$db,
			"get_jenis_patient",
			null,
			"registration"
		);
		$jenis_service_consumer->execute();
		$jenis_option = $jenis_service_consumer->getContent();
		$jenis_option[] = array(
			"name"		=> "",
			"value"	=> "",
			"default"	=> 1
		);

		$jenis_select = new Select("resep_jenis", "resep_jenis", $jenis_option);
		$resep_modal->addElement("Jns. Pasien", $jenis_select);
	} else {
		$jenis_text = new Text("resep_jenis", "resep_jenis", "");
		$resep_modal->addElement("Jns. Pasien", $jenis_text);
	}
	$asuransi_option = new OptionBuilder();
	$asuransi_option->add("", "", 1);
	$service = new ServiceConsumer(
		$db,
		"get_all_asuransi",
		null,
		"registration"
	);
	$content = $service->execute()->getContent();
	if ($content != null)
		foreach ($content as $c)
			$asuransi_option->add($c['nama'], $c['nama']);
	$asuransi_select = new Select("resep_asuransi", "resep_asuransi", $asuransi_option->getContent());
	$resep_modal->addElement("Asuransi", $asuransi_select);
	$perusahaan_option = new OptionBuilder();
	$perusahaan_option->add("", "", 1);
	$service = new ServiceConsumer(
		$db,
		"get_all_perusahaan",
		null,
		"registration"
	);
	$content = $service->execute()->getContent();
	if ($content != null)
		foreach ($content as $c)
			$perusahaan_option->add($c['nama'], $c['nama']);
	$perusahaan_select = new Select("resep_perusahaan", "resep_perusahaan", $perusahaan_option->getContent());
	$resep_modal->addElement("Perusahaan", $perusahaan_select);
	$markup_hidden = new Hidden("resep_markup", "resep_markup", 0);
	$resep_modal->addElement("", $markup_hidden);
	$id_dokter_hidden = new Hidden("resep_id_dokter", "resep_id_dokter", "");
	$resep_modal->addElement("", $id_dokter_hidden);
	$nama_dokter_hidden = new Hidden("resep_name_dokter", "resep_name_dokter", "");
	$resep_modal->addElement("", $nama_dokter_hidden);
	$dokter_button = new Button("", "", "Pilih");
	$dokter_button->setClass("btn-info");
	$dokter_button->setIsButton(Button::$ICONIC);
	$dokter_button->setIcon("icon-white " . Button::$icon_list_alt);
	$dokter_button->setAction("dokter.chooser('dokter', 'dokter_button', 'dokter', dokter)");
	$dokter_button->setAtribute("id='dokter_browse'");
	$dokter_text = new Text("resep_nama_dokter", "resep_nama_dokter", "");
	$dokter_text->setClass("smis-one-option-input");
	$dokter_input_group = new InputGroup("");
	$dokter_input_group->addComponent($dokter_text);
	$dokter_input_group->addComponent($dokter_button);
	$resep_modal->addElement("Dokter", $dokter_input_group);
	$diskon_text = new Text("resep_diskon", "resep_diskon", "0,00");
	$diskon_text->setTypical("money");
	$diskon_text->setAtribute(" data-thousands=\".\" data-decimal=\",\"  data-precision=\"2\"  ");
	$resep_modal->addElement("Diskon", $diskon_text);
	$t_diskon_option = new OptionBuilder();
	$t_diskon_option->add("Persen (%)", "persen", "1");
	$t_diskon_option->add("Nominal (Rp)", "nominal");
	$t_diskon_option->add("Gratis (100 %)", "gratis");
	$t_diskon_select = new Select("resep_t_diskon", "resep_t_diskon", $t_diskon_option->getContent());
	$resep_modal->addElement("Tipe Diskon", $t_diskon_select);
	$total_text = new Text("resep_total", "resep_total", "");
	$total_text->setTypical("money");
	$total_text->setAtribute(" data-thousands=\".\" data-decimal=\",\" data-prefix=\"Rp. \"  data-precision=\"2\"  disabled='disabled'");
	$resep_modal->addElement("Total", $total_text);
	$dresep_table = new DResepTable(array("No.", "Nama", "Jumlah", "Harga", "Embalase", "Tuslah", "Biaya Racik", "Subtotal", "Aturan Pakai", "Apoteker"));
	$dresep_table->setName("dresep");
	$dresep_table->setFooterVisible(false);
	$resep_modal->addBody("dresep_table", $dresep_table);
	$resep_button = new Button("", "", "Simpan");
	$resep_button->setClass("btn-success");
	$resep_button->setIcon("fa fa-floppy-o");
	$resep_button->setIsButton(Button::$ICONIC);
	$resep_button->setAtribute("id='resep_save'");
	$resep_modal->addFooter($resep_button);
	$resep_button = new Button("", "", "OK");
	$resep_button->setClass("btn-success");
	$resep_button->setAtribute("id='resep_ok'");
	$resep_button->setAction("$($(this).data('target')).smodal('hide')");
	$resep_modal->addFooter($resep_button);
	$tombol = '<a href="#" class="input btn btn-info" ><i class="icon-white icon-list-alt"></i></a>';
	$resep_modal->addHTML("
		<div class='alert alert-block alert-inverse' id='help_resep'>
			<h4>Tips</h4>
			<ul>
				<li>Tombol <kbd>Tab</kbd> : Berpindah Cepat ke Isian Berikutnya (dari Kiri ke Kanan)</li>
				<li>Ketik Min. <strong>3 Karakter</strong> / Tombol " . $tombol . " pada Isian Dokter / Tombol <kbd>F8</kbd> : Menentukan Nama Dokter</li>
				<li>Tombol <kbd>&uarr;</kbd> / <kbd>&darr;</kbd> : Memilih <strong>Persen (%) / Nominal (Rp) / Gratis (100 %)</strong> pada Isian <strong>Tipe Diskon</strong></li>
				<li>Tombol <kbd>F3</kbd> : Tombol Cepat Menambahkan Obat Jadi Baru</li>
				<li>Tombol <kbd>F4</kbd> : Tombol Cepat Menambahkan Obat Racikan Baru</li>
				<li>Tombol <kbd>F6</kbd> : Tombol Cepat Menyimpan Data Resep, Pastikan Semua Obat/Racikan dan Informasi Kepala Resep Sudah Lengkap.</li>
			<ul>
		</div>
	", "after");

	//obat jadi modal:
	$obat_jadi_modal = new Modal("obat_jadi_add_form", "smis_form_container", "obat_jadi");
	$obat_jadi_modal->setTitle("Data Obat Jadi");
	if (getSettings($db, "depo_farmasi7-penjualan_resep_luar-etiket_obat", 0) == 1)
		$obat_jadi_modal->setClass(Modal::$HALF_MODEL);
	$id_hidden = new Hidden("obat_jadi_id", "obat_jadi_id", "");
	$obat_jadi_modal->addElement("", $id_hidden);
	$id_obat_hidden = new Hidden("obat_jadi_id_obat", "obat_jadi_id_obat", "");
	$obat_jadi_modal->addElement("", $id_obat_hidden);
	$obat_button = new Button("", "", "Pilih");
	$obat_button->setClass("btn-info");
	$obat_button->setAction("obat.chooser('obat', 'obat_button', 'obat', obat)");
	$obat_button->setIcon("icon-white icon-list-alt");
	$obat_button->setIsButton(Button::$ICONIC);
	$obat_button->setAtribute("id='obat_jadi_browse'");
	$nama_obat_text = new Text("obat_jadi_nama_obat", "obat_jadi_nama_obat", "");
	$nama_obat_text->setClass("smis-one-option-input");
	$nama_obat_text->addAtribute("autofocus");
	$nama_obat_input_group = new InputGroup("");
	$nama_obat_input_group->addComponent($nama_obat_text);
	$nama_obat_input_group->addComponent($obat_button);
	$obat_jadi_modal->addElement("Obat", $nama_obat_input_group);
	$kode_obat_hidden = new Hidden("obat_jadi_kode_obat", "obat_jadi_kode_obat", "");
	$obat_jadi_modal->addElement("", $kode_obat_hidden);
	$name_obat_hidden = new Hidden("obat_jadi_name_obat", "obat_jadi_name_obat", "");
	$obat_jadi_modal->addElement("", $name_obat_hidden);
	$nama_jenis_obat_hidden = new Hidden("obat_jadi_nama_jenis_obat", "obat_jadi_nama_jenis_obat", "");
	$obat_jadi_modal->addElement("", $nama_jenis_obat_hidden);
	$satuan_select = new Select("obat_jadi_satuan", "obat_jadi_satuan", "");
	$obat_jadi_modal->addElement("Satuan", $satuan_select);
	$konversi_hidden = new Hidden("obat_jadi_konversi", "obat_jadi_konversi", "");
	$konversi_hidden->setAtribute("disabled='disabled'");
	$obat_jadi_modal->addElement("", $konversi_hidden);
	$satuan_konversi_hidden = new Hidden("obat_jadi_satuan_konversi", "obat_jadi_satuan_konversi", "");
	$obat_jadi_modal->addElement("", $satuan_konversi_hidden);
	$stok_hidden = new Hidden("obat_jadi_stok", "obat_jadi_stok", "");
	$obat_jadi_modal->addElement("", $stok_hidden);
	$f_stok_text = new Text("obat_jadi_f_stok", "obat_jadi_f_stok", "");
	$f_stok_text->setAtribute("disabled='disabled'");
	$obat_jadi_modal->addElement("Stok", $f_stok_text);
	$jumlah_lama_hidden = new Hidden("obat_jadi_jumlah_lama", "obat_jadi_jumlah_lama", "");
	$obat_jadi_modal->addElement("", $jumlah_lama_hidden);
	$hna_text = new Text("obat_jadi_hna", "obat_jadi_hna", "");
	$hna_text->setTypical("money");
	$hna_text->setAtribute(" data-thousands=\".\" data-decimal=\",\" data-prefix=\"Rp. \"  data-precision=\"2\"  disabled='disabled'");
	$obat_jadi_modal->addElement("HJA", $hna_text);
	if (getSettings($db, "depo_farmasi7-penjualan_resep_luar-embalase_show-obat_jadi", 0) == 1) {
		$embalase_text = new Text("obat_jadi_embalase", "obat_jadi_embalase", "");
		$embalase_text->setTypical("money");
		$embalase_text->setAtribute(" data-thousands=\".\" data-decimal=\",\" data-prefix=\"Rp. \"  data-precision=\"2\"");
		$obat_jadi_modal->addElement("Embalase", $embalase_text);
	} else {
		$embalase_hidden = new Hidden("obat_jadi_embalase", "obat_jadi_embalase", "");
		$obat_jadi_modal->addElement("", $embalase_hidden);
	}
	if (getSettings($db, "depo_farmasi7-penjualan_resep_luar-tuslah_show-obat_jadi", 0) == 1) {
		$tuslah_text = new Text("obat_jadi_tuslah", "obat_jadi_tuslah", "");
		$tuslah_text->setTypical("money");
		$tuslah_text->setAtribute(" data-thousands=\".\" data-decimal=\",\" data-prefix=\"Rp. \"  data-precision=\"2\"");
		$obat_jadi_modal->addElement("Tusla", $tuslah_text);
	} else {
		$tuslah_hidden = new Hidden("obat_jadi_tuslah", "obat_jadi_tuslah", "");
		$obat_jadi_modal->addElement("", $tuslah_hidden);
	}
	$markup_hidden = new Hidden("obat_jadi_markup", "obat_jadi_markup", "");
	$obat_jadi_modal->addElement("", $markup_hidden);
	$jumlah_text = new Text("obat_jadi_jumlah", "obat_jadi_jumlah", "");
	$obat_jadi_modal->addElement("Jumlah", $jumlah_text);

	if (getSettings($db, "depo_farmasi7-penjualan_resep_luar-etiket_obat", 0) == 1) {
		$aturan_pakai_hidden = new Hidden("obat_jadi_aturan_pakai", "obat_jadi_aturan_pakai", "-");
		$obat_jadi_modal->addElement("", $aturan_pakai_hidden);
		$tanggal_exp_text = new Text("obat_jadi_tanggal_exp", "obat_jadi_tanggal_exp", "");
		$tanggal_exp_text->setAtribute("data-date-format='dd-mm-yyyy'");
		$tanggal_exp_text->setClass("mydate");
		$obat_jadi_modal->addElement("Tgl. Exp.", $tanggal_exp_text);
		$obat_luar_option = new OptionBuilder();
		$obat_luar_option->add("Iya", "1");
		$obat_luar_option->add("Bukan", "0", "1");
		$obat_luar_select = new Select("obat_jadi_obat_luar", "obat_jadi_obat_luar", $obat_luar_option->getContent());
		$obat_jadi_modal->addElement("Obat Luar", $obat_luar_select);
		$jumlah_pakai_sehari_hidden = new Hidden("obat_jadi_jumlah_pakai_sehari", "obat_jadi_jumlah_pakai_sehari", "");
		$obat_jadi_modal->addElement("", $jumlah_pakai_sehari_hidden);
		$satuan_pakai_option = new OptionBuilder();
		$satuan_pakai_option->add("", "", 1);
		$satuan_pakai_option->addSingle("ml");
		$satuan_pakai_option->addSingle("tetes");
		$satuan_pakai_option->addSingle("sendok takar");
		$satuan_pakai_option->addSingle("sendok makan");
		$satuan_pakai_option->addSingle("Tablet");
		$satuan_pakai_option->addSingle("Kapsul");
		$satuan_pakai_option->addSingle("Suppositoria");
		$satuan_pakai_option->addSingle("Bungkus");
		$satuan_pakai_select = new Select("obat_jadi_satuan_pakai", "obat_jadi_satuan_pakai", $satuan_pakai_option->getContent());
		$obat_jadi_modal->addElement("Satuan Pakai", $satuan_pakai_select);
		$takaran_pakai_text = new Text("obat_jadi_takaran_pakai", "obat_jadi_takaran_pakai", "");
		$obat_jadi_modal->addElement("Takaran Pakai", $takaran_pakai_text);
		$pemakaian_option = new OptionBuilder();
		$pemakaian_option->add("", "", "1");
		$pemakaian_option->addSingle("sebelum");
		$pemakaian_option->addSingle("sesudah");
		$pemakaian_option->addSingle("bersama");
		$pemakaian_select = new Select("obat_jadi_pemakaian", "obat_jadi_pemakaian", $pemakaian_option->getContent());
		$obat_jadi_modal->addElement("Pemakaian", $pemakaian_select);
		$pemakaian_obat_luar_text = new Text("obat_jadi_pemakaian_obat_luar", "obat_jadi_pemakaian_obat_luar", "");
		$obat_jadi_modal->addElement("Pemakaian", $pemakaian_obat_luar_text);
		$penggunaan_option = new OptionBuilder();
		$penggunaan_option->add("", "", "1");
		$penggunaan_option->add("1x", "1x");
		$penggunaan_option->add("2x", "2x");
		$penggunaan_option->add("3x", "3x");
		$penggunaan_option->add("4x", "4x");
		$penggunaan_select = new Select("obat_jadi_penggunaan", "obat_jadi_penggunaan", $penggunaan_option->getContent());
		$obat_jadi_modal->addElement("Penggunaan", $penggunaan_select);
		$detail_penggunaan_option = new OptionBuilder();
		$detail_penggunaan_option->add("", "", "1");
		$detail_penggunaan_select = new Select("obat_jadi_detail_penggunaan", "obat_jadi_detail_penggunan", $detail_penggunaan_option->getContent());
		$obat_jadi_modal->addElement("Waktu", $detail_penggunaan_select);
	} else {
		$aturan_pakai_text = new Text("obat_jadi_aturan_pakai", "obat_jadi_aturan_pakai", "-");
		$obat_jadi_modal->addElement("Atuan Pakai", $aturan_pakai_text);
		$tanggal_exp_hidden = new Hidden("obat_jadi_tanggal_exp", "obat_jadi_tanggal_exp", "");
		$obat_jadi_modal->addElement("", $tanggal_exp_hidden);
		$obat_luar_hidden = new Hidden("obat_jadi_obat_luar", "obat_jadi_obat_luar", "1");
		$obat_jadi_modal->addElement("", $obat_luar_hidden);
		$jumlah_pakai_sehari_hidden = new Hidden("obat_jadi_jumlah_pakai_sehari", "obat_jadi_jumlah_pakai_sehari", "");
		$obat_jadi_modal->addElement("", $jumlah_pakai_sehari_hidden);
		$satuan_pakai_hidden = new Hidden("obat_jadi_satuan_pakai", "obat_jadi_satuan_pakai", "");
		$obat_jadi_modal->addElement("", $satuan_pakai_hidden);
		$takaran_pakai_hidden = new Hidden("obat_jadi_takaran_pakai", "obat_jadi_takaran_pakai", "");
		$obat_jadi_modal->addElement("", $takaran_pakai_hidden);
		$pemakaian_hidden = new Hidden("obat_jadi_pemakaian", "obat_jadi_pemakaian", "");
		$obat_jadi_modal->addElement("", $pemakaian_hidden);
		$pemakaian_obat_luar_hidden = new Hidden("obat_jadi_pemakaian_obat_luar", "obat_jadi_pemakaian_obat_luar", "");
		$obat_jadi_modal->addElement("", $pemakaian_obat_luar_hidden);
		$penggunaan_hidden = new Hidden("obat_jadi_penggunaan", "obat_jadi_penggunaan", "");
		$obat_jadi_modal->addElement("", $penggunaan_hidden);
		$detail_penggunaan_hidden = new Hidden("obat_jadi_detail_penggunaan", "obat_jadi_detail_penggunaan", "");
		$obat_jadi_modal->addElement("", $detail_penggunaan_hidden);
	}
	$obat_jadi_button = new Button("", "", "Simpan");
	$obat_jadi_button->setClass("btn-success");
	$obat_jadi_button->setAtribute("id='obat_jadi_save'");
	$obat_jadi_button->setIcon("fa fa-floppy-o");
	$obat_jadi_button->setIsButton(Button::$ICONIC);
	$obat_jadi_modal->addFooter($obat_jadi_button);
	$obat_jadi_modal->addHTML("
		<div class='alert alert-block alert-inverse'>
			<h4>Tips</h4>
			<ul>
				<li><small>Ketik Min. <strong>3 Karakter</strong> / Tombol " . $tombol . " / Tombol <kbd>F2</kbd> : Menentukan Nama Obat</small></li>
				<li>Tombol <kbd>Esc</kbd> : Tombol Cepat Kembali ke Formulir Resep</li>
				<li>Tombol <kbd>F6</kbd> : Tombol Menyimpan Bahan</li>
			</ul>
		</div>
	", "before");

	//obat racikan modal:
	$obat_racikan_modal = new Modal("obat_racikan_add_form", "smis_form_container", "obat_racikan");
	$obat_racikan_modal->setTitle("Data Obat Racikan");
	$obat_racikan_modal->setClass(Modal::$FULL_MODEL);
	$id_hidden = new Hidden("obat_racikan_id", "obat_racikan_id", "");
	$obat_racikan_modal->addElement("", $id_hidden);
	$nama_text = new Text("obat_racikan_nama", "obat_racikan_nama", "");
	$nama_text->addAtribute("autofocus");
	$obat_racikan_modal->addElement("Nama Racikan", $nama_text);
	$id_apoteker_hidden = new Hidden("obat_racikan_id_apoteker", "obat_racikan_id_apoteker", "");
	$obat_racikan_modal->addElement("", $id_apoteker_hidden);
	$nama_apoteker_hidden = new Hidden("obat_racikan_name_apoteker", "obat_racikan_name_apoteker", "");
	$obat_racikan_modal->addElement("", $nama_apoteker_hidden);
	$apoteker_button = new Button("", "", "Pilih");
	$apoteker_button->setClass("btn-info");
	$apoteker_button->setIsButton(Button::$ICONIC);
	$apoteker_button->setIcon("icon-white " . Button::$icon_list_alt);
	$apoteker_button->setAction("apoteker.chooser('apoteker', 'apoteker_button', 'apoteker', apoteker)");
	$apoteker_button->setAtribute("id='apoteker_browse'");
	$apoteker_text = new Text("obat_racikan_nama_apoteker", "obat_racikan_nama_apoteker", "");
	$apoteker_text->setClass("smis-one-option-input");
	$apoteker_input_group = new InputGroup("");
	$apoteker_input_group->addComponent($apoteker_text);
	$apoteker_input_group->addComponent($apoteker_button);
	$obat_racikan_modal->addElement("Apoteker", $apoteker_input_group);
	if (getSettings($db, "depo_farmasi7-penjualan_resep_luar-etiket_obat", 0) == 1) {
		$aturan_pakai_hidden = new Hidden("obat_racikan_aturan_pakai", "obat_racikan_aturan_pakai", "-");
		$obat_racikan_modal->addElement("", $aturan_pakai_hidden);
		$tanggal_exp_text = new Text("obat_racikan_tanggal_exp", "obat_racikan_tanggal_exp", "");
		$tanggal_exp_text->setAtribute("data-date-format='dd-mm-yyyy'");
		$tanggal_exp_text->setClass("mydate");
		$obat_racikan_modal->addElement("Tgl. Exp.", $tanggal_exp_text);
		$obat_luar_option = new OptionBuilder();
		$obat_luar_option->add("Iya", "1");
		$obat_luar_option->add("Bukan", "0", "1");
		$obat_luar_select = new Select("obat_racikan_obat_luar", "obat_racikan_obat_luar", $obat_luar_option->getContent());
		$obat_racikan_modal->addElement("Obat Luar", $obat_luar_select);
		$jumlah_pakai_sehari_hidden = new Hidden("obat_racikan_jumlah_pakai_sehari", "obat_racikan_jumlah_pakai_sehari", "");
		$obat_racikan_modal->addElement("", $jumlah_pakai_sehari_hidden);
		$satuan_pakai_option = new OptionBuilder();
		$satuan_pakai_option->add("", "", 1);
		$satuan_pakai_option->addSingle("ml");
		$satuan_pakai_option->addSingle("tetes");
		$satuan_pakai_option->addSingle("sendok takar");
		$satuan_pakai_option->addSingle("sendok makan");
		$satuan_pakai_option->addSingle("Tablet");
		$satuan_pakai_option->addSingle("Kapsul");
		$satuan_pakai_option->addSingle("Suppositoria");
		$satuan_pakai_option->addSingle("Bungkus");
		$satuan_pakai_select = new Select("obat_racikan_satuan_pakai", "obat_racikan_satuan_pakai", $satuan_pakai_option->getContent());
		$obat_racikan_modal->addElement("Satuan Pakai", $satuan_pakai_select);
		$takaran_pakai_text = new Text("obat_racikan_takaran_pakai", "obat_racikan_takaran_pakai", "");
		$obat_racikan_modal->addElement("Takaran Pakai", $takaran_pakai_text);
		$pemakaian_option = new OptionBuilder();
		$pemakaian_option->add("", "", "1");
		$pemakaian_option->addSingle("sebelum");
		$pemakaian_option->addSingle("sesudah");
		$pemakaian_option->addSingle("bersama");
		$pemakaian_select = new Select("obat_racikan_pemakaian", "obat_racikan_pemakaian", $pemakaian_option->getContent());
		$obat_racikan_modal->addElement("Pemakaian", $pemakaian_select);
		$pemakaian_obat_luar_text = new Text("obat_racikan_pemakaian_obat_luar", "obat_racikan_pemakaian_obat_luar", "");
		$obat_racikan_modal->addElement("Pemakaian", $pemakaian_obat_luar_text);
		$penggunaan_option = new OptionBuilder();
		$penggunaan_option->add("", "", "1");
		$penggunaan_option->add("1x", "1x");
		$penggunaan_option->add("2x", "2x");
		$penggunaan_option->add("3x", "3x");
		$penggunaan_option->add("4x", "4x");
		$penggunaan_select = new Select("obat_racikan_penggunaan", "obat_racikan_penggunaan", $penggunaan_option->getContent());
		$obat_racikan_modal->addElement("Penggunaan", $penggunaan_select);
		$detail_penggunaan_option = new OptionBuilder();
		$detail_penggunaan_option->add("", "", "1");
		$detail_penggunaan_select = new Select("obat_racikan_detail_penggunaan", "obat_racikan_detail_penggunan", $detail_penggunaan_option->getContent());
		$obat_racikan_modal->addElement("Waktu", $detail_penggunaan_select);
	} else {
		$aturan_pakai_text = new Text("obat_racikan_aturan_pakai", "obat_racikan_aturan_pakai", "-");
		$obat_racikan_modal->addElement("Aturan Pakai", $aturan_pakai_text);
		$tanggal_exp_hidden = new Hidden("obat_racikan_tanggal_exp", "obat_racikan_tanggal_exp", "");
		$obat_racikan_modal->addElement("", $tanggal_exp_hidden);
		$obat_luar_hidden = new Hidden("obat_racikan_obat_luar", "obat_racikan_obat_luar", "1");
		$obat_racikan_modal->addElement("", $obat_luar_hidden);
		$jumlah_pakai_sehari_hidden = new Hidden("obat_racikan_jumlah_pakai_sehari", "obat_racikan_jumlah_pakai_sehari", "");
		$obat_racikan_modal->addElement("", $jumlah_pakai_sehari_hidden);
		$satuan_pakai_hidden = new Hidden("obat_racikan_satuan_pakai", "obat_racikan_satuan_pakai", "");
		$obat_racikan_modal->addElement("", $satuan_pakai_hidden);
		$takaran_pakai_hidden = new Hidden("obat_racikan_takaran_pakai", "obat_racikan_takaran_pakai", "");
		$obat_racikan_modal->addElement("", $takaran_pakai_hidden);
		$pemakaian_hidden = new Hidden("obat_racikan_pemakaian", "obat_racikan_pemakaian", "");
		$obat_racikan_modal->addElement("", $pemakaian_hidden);
		$pemakaian_obat_luar_hidden = new Hidden("obat_racikan_pemakaian_obat_luar", "obat_racikan_pemakaian_obat_luar", "");
		$obat_racikan_modal->addElement("", $pemakaian_obat_luar_hidden);
		$penggunaan_hidden = new Hidden("obat_racikan_penggunaan", "obat_racikan_penggunaan", "");
		$obat_racikan_modal->addElement("", $penggunaan_hidden);
		$detail_penggunaan_hidden = new Hidden("obat_racikan_detail_penggunaan", "obat_racikan_detail_penggunaan", "");
		$obat_racikan_modal->addElement("", $detail_penggunaan_hidden);
	}
	$biaya_racik_text = new Text("obat_racikan_biaya_racik", "obat_racikan_biaya_racik", "");
	$biaya_racik_text->setTypical("money");
	if (getSettings($db, "depo_farmasi7-penjualan_resep_luar-jasa_racik_edit", 0) == 0)
		$biaya_racik_text->setAtribute(" data-thousands=\".\" data-decimal=\",\" data-prefix=\"Rp. \"  data-precision=\"2\" disabled='disabled'");
	else
		$biaya_racik_text->setAtribute(" data-thousands=\".\" data-decimal=\",\" data-prefix=\"Rp. \"  data-precision=\"2\"");
	$obat_racikan_modal->addElement("Biaya Racik", $biaya_racik_text);
	$dracikan_table = new DObatRacikanTable(
		array("Bahan", "Harga", "Jumlah", "Embalase", "Tuslah", "Subtotal")
	);
	$dracikan_table->setName("dracikan");
	$dracikan_table->setFooterVisible(false);
	$obat_racikan_modal->addBody("dracikan_table", $dracikan_table);
	$obat_racikan_button = new Button("", "", "Simpan");
	$obat_racikan_button->setClass("btn-success");
	$obat_racikan_button->setIcon("fa fa-floppy-o");
	$obat_racikan_button->setIsButton(Button::$ICONIC);
	$obat_racikan_button->setAtribute("id='racikan_save'");
	$obat_racikan_modal->addFooter($obat_racikan_button);
	$obat_racikan_button = new Button("", "", "OK");
	$obat_racikan_button->setClass("btn-success");
	$obat_racikan_button->setAtribute("id='racikan_ok'");
	$obat_racikan_button->setAction("$($(this).data('target')).smodal('hide')");
	$obat_racikan_modal->addFooter($obat_racikan_button);
	$obat_racikan_modal->addHTML("
		<div class='alert alert-block alert-inverse' id='help_racikan'>
			<h4>Tips</h4>
			<ul>
				<li>Ketik Min. <strong>3 Karakter</strong> / Tombol " . $tombol . " / Tombol <kbd>F7</kbd> : Menentukan Nama Apoteker</li>
				<li>Tombol <kbd>F2</kbd> : Tombol Cepat Menambahkan Bahan Baru dari Obat Racikan</li>
				<li>Tombol <kbd>F6</kbd> : Tombol Cepat Menyimpan Obat Racikan</li>
				<li>Tombol <kbd>Esc</kbd> : Tombol Cepat Kembali ke Formulir Resep</li>
			</ul>
		</div>
	", "after");

	//bahan modal:
	$bahan_modal = new Modal("bahan_add_form", "smis_form_container", "bahan");
	$bahan_modal->setTitle("Data Bahan");
	$id_hidden = new Hidden("bahan_id", "bahan_id", "");
	$bahan_modal->addElement("", $id_hidden);
	$id_bahan_hidden = new Hidden("bahan_id_bahan", "bahan_id_bahan", "");
	$bahan_modal->addElement("", $id_bahan_hidden);
	$bahan_button = new Button("", "", "Pilih");
	$bahan_button->setClass("btn-info");
	$bahan_button->setAction("bahan.chooser('bahan', 'bahan_button', 'bahan', bahan)");
	$bahan_button->setIcon("icon-white icon-list-alt");
	$bahan_button->setIsButton(Button::$ICONIC);
	$bahan_button->setAtribute("id='bahan_browse'");
	$nama_bahan_text = new Text("bahan_nama_bahan", "bahan_nama_bahan", "");
	$nama_bahan_text->addAtribute("autofocus");
	$nama_bahan_text->setClass("smis-one-option-input");
	$nama_bahan_input_group = new InputGroup("");
	$nama_bahan_input_group->addComponent($nama_bahan_text);
	$nama_bahan_input_group->addComponent($bahan_button);
	$bahan_modal->addElement("Bahan", $nama_bahan_input_group);
	$kode_bahan_hidden = new Hidden("bahan_kode_bahan", "bahan_kode_bahan", "");
	$bahan_modal->addElement("", $kode_bahan_hidden);
	$name_bahan_hidden = new Hidden("bahan_name_bahan", "bahan_name_bahan", "");
	$bahan_modal->addElement("", $name_bahan_hidden);
	$nama_jenis_bahan_hidden = new Hidden("bahan_nama_jenis_bahan", "bahan_nama_jenis_bahan", "");
	$bahan_modal->addElement("", $nama_jenis_bahan_hidden);
	$satuan_select = new Select("bahan_satuan", "bahan_satuan", "");
	$bahan_modal->addElement("Satuan", $satuan_select);
	$konversi_hidden = new Hidden("bahan_konversi", "bahan_konversi", "");
	$konversi_hidden->setAtribute("disabled='disabled'");
	$bahan_modal->addElement("", $konversi_hidden);
	$satuan_konversi_hidden = new Hidden("bahan_satuan_konversi", "bahan_satuan_konversi", "");
	$bahan_modal->addElement("", $satuan_konversi_hidden);
	$stok_hidden = new Hidden("bahan_stok", "bahan_stok", "");
	$bahan_modal->addElement("", $stok_hidden);
	$f_stok_text = new Text("bahan_f_stok", "bahan_f_stok", "");
	$f_stok_text->setAtribute("disabled='disabled'");
	$bahan_modal->addElement("Stok", $f_stok_text);
	$jumlah_lama_hidden = new Hidden("bahan_jumlah_lama", "bahan_jumlah_lama", "");
	$bahan_modal->addElement("", $jumlah_lama_hidden);
	$hna_text = new Text("bahan_hna", "bahan_hna", "");
	$hna_text->setTypical("money");
	$hna_text->setAtribute(" data-thousands=\".\" data-decimal=\",\" data-prefix=\"Rp. \"  data-precision=\"2\"  disabled='disabled'");
	$bahan_modal->addElement("HJA", $hna_text);
	$markup_hidden = new Hidden("bahan_markup", "bahan_markup", "");
	$bahan_modal->addElement("", $markup_hidden);
	if (getSettings($db, "depo_farmasi7-penjualan_resep_luar-embalase_show-bahan_racikan", 0) == 1) {
		$embalase_text = new Text("bahan_embalase", "bahan_embalase", "");
		$embalase_text->setTypical("money");
		$embalase_text->setAtribute(" data-thousands=\".\" data-decimal=\",\" data-prefix=\"Rp. \"  data-precision=\"2\"");
		$bahan_modal->addElement("Embalase", $embalase_text);
	} else {
		$embalase_hidden = new Hidden("bahan_embalase", "bahan_embalase", "");
		$bahan_modal->addElement("", $embalase_hidden);
	}
	if (getSettings($db, "depo_farmasi7-penjualan_resep_luar-tuslah_show-bahan_racikan", 0) == 1) {
		$tuslah_text = new Text("bahan_tuslah", "bahan_tuslah", "");
		$tuslah_text->setTypical("money");
		$tuslah_text->setAtribute(" data-thousands=\".\" data-decimal=\",\" data-prefix=\"Rp. \"  data-precision=\"2\"");
		$bahan_modal->addElement("Tusla", $tuslah_text);
	} else {
		$tuslah_hidden = new Hidden("bahan_tuslah", "bahan_tuslah", "");
		$bahan_modal->addElement("", $tuslah_hidden);
	}
	$jumlah_text = new Text("bahan_jumlah", "bahan_jumlah", "");
	$bahan_modal->addElement("Jumlah", $jumlah_text);
	$bahan_button = new Button("", "", "Simpan");
	$bahan_button->setClass("btn-success");
	$bahan_button->setIcon("fa fa-floppy-o");
	$bahan_button->setIsButton(Button::$ICONIC);
	$bahan_button->setAtribute("id='bahan_save'");
	$bahan_modal->addFooter($bahan_button);
	$bahan_modal->addHTML("
		<div class='alert alert-block alert-inverse'>
			<h4>Tips</h4>
			<ul>
				<li><small>Ketik Min. <strong>3 Karakter</strong> / Tombol " . $tombol . " / <kbd>F2</kbd> : Menentukan Nama Bahan</small></li>
				<li>Tombol <kbd>Esc</kbd> : Tombol Cepat Kembali ke Formulir Obat Racikan</li>
				<li>Tombol <kbd>F6</kbd> : Tombol Menyimpan Bahan</li>
			</ul>
		</div>
	", "before");

	//invi-table bahan racikan:
	$bahan_racikan_table = new BahanRacikanTable(array("ID", "IDO", "Nama", "Jumlah", "Satuan", "Konversi", "Satuan Konversi", "Harga", "Label"));
	$bahan_racikan_table->setAction(false);
	$bahan_racikan_table->setName("bahan_racikan");

	echo "<div class='alert alert-block alert-inverse'>" .
		"<h4>Tips</h4>" .
		"Tombol <kbd>F2</kbd> : Menampilkan Formulir Penjualan Resep Baru" .
		"</div>";
	echo $bahan_modal->getHtml();
	echo $obat_racikan_modal->getHtml();
	echo $obat_jadi_modal->getHtml();
	echo $resep_modal->getHtml();
	echo $resep_table->getHtml();
	echo $bahan_racikan_table->getHtml();

	echo addCSS("depo_farmasi_irna/css/penjualan_resep.css", false);
	echo addJS("smis-base-js/smis-base-shortcut.js", false);
	echo addJS("depo_farmasi_irna/js/penjualan_resep_luar_action.js", false);
	echo addJS("depo_farmasi_irna/js/dpenjualan_resep_action.js", false);
	echo addJS("depo_farmasi_irna/js/obat_action.js", false);
	echo addJS("depo_farmasi_irna/js/apoteker_action.js", false);
	echo addJS("depo_farmasi_irna/js/bahan_action.js", false);
	echo addJS("depo_farmasi_irna/js/dokter_action.js", false);
	echo addJS("smis-libs-out/webprint/webprint.js", false);
	echo addJS("framework/smis/js/table_action.js");
	echo addJS("framework/bootstrap/js/bootstrap-datepicker.js");
	echo addCSS("framework/bootstrap/css/datepicker.css");
?>
<script type="text/javascript">
	var embalase_obat_jadi = <?php echo getSettings($db, "depo_farmasi7-penjualan_resep_luar-embalase-obat_jadi", 0); ?>;
	var tuslah_obat_jadi = <?php echo getSettings($db, "depo_farmasi7-penjualan_resep_luar-tuslah-obat_jadi", 0); ?>;
	var embalase_bahan_racikan = <?php echo getSettings($db, "depo_farmasi7-penjualan_resep_luar-embalase-bahan_racikan", 0); ?>;
	var tuslah_bahan_racikan = <?php echo getSettings($db, "depo_farmasi7-penjualan_resep_luar-tuslah-bahan_racikan", 0); ?>;
	var jasa_racik = <?php echo getSettings($db, "depo_farmasi7-penjualan_resep_luar-jasa_racik", 0); ?>;

	/// mode margin :
	/// 0	=> paten
	/// 1 	=> jenis pasien
	/// 2 	=> per obat
	/// 3	=> per obat - jenis pasien
	var mode_margin = <?php echo getSettings($db, "depo_farmasi7-penjualan_resep_luar-mode_margin_penjualan", 0); ?>;
	var need_margin_penjualan_request = 1;

	var dresep_num;
	var racikan_num;
	var dracikan_num;
	var bahan_num;
	var resep;
	var dokter;
	var dresep;
	var obat;
	var apoteker;
	var bahan;
	$(document).ready(function() {
		$(".mydate").datepicker();
		$('.modal').on('shown.bs.modal', function() {
			$(this).find('[autofocus]').focus();
		});
		$('[data-toggle="popover"]').popover({
			trigger: 'hover',
			'placement': 'top'
		});
		$("*").dblclick(function(e) {
			e.preventDefault();
		});
		$("#smis-chooser-modal").on("show", function() {
			if ($("#smis-chooser-modal .modal-header h3").text() == "OBAT" ||
				$("#smis-chooser-modal .modal-header h3").text() == "BAHAN") {
				$("#smis-chooser-modal").removeClass("half_model");
				$("#smis-chooser-modal").addClass("half_model");
			} else {
				$("#smis-chooser-modal").removeClass("half_model");
			}
		});
		$("#smis-chooser-modal").on("hidden", function() {
			$("#smis-chooser-modal").removeClass("half_model");
		});
		$("#resep_add_form").on("show", function() {
			$("ul.typeahead").hide();
		});
		$("#obat_racikan_add_form").on("show", function() {
			$("ul.typeahead").hide();
		});
		$("#resep_t_diskon").on("change", function() {
			var diskon = $("#resep_diskon").val().replace(/[^0-9-,]/g, '').replace(",", ".");
			var t_diskon = $("#resep_t_diskon").val();
			if (diskon > 100 && t_diskon == "persen") {
				$("#modal_alert_resep_add_form").html(
					"<div class='alert alert-block alert-danger'>" +
					"<h4>Peringatan</h4>" +
					"<br /><b>Diskon</b> tidak boleh melebihi 100%" +
					"</div>"
				);
				return;
			}
			$("#modal_alert_resep_add_form").html("");
			if ($("#resep_t_diskon").val() == "gratis") {
				$("#resep_diskon").val("100,00");
				$("#resep_diskon").removeAttr("disabled");
				$("#resep_diskon").attr("disabled", "disabled");
			} else {
				$("#resep_diskon").val("0,00");
				$("#resep_diskon").removeAttr("disabled");
			}
			resep.refreshBiayaTotal();
		});
		$("#resep_diskon").on("keyup", function() {
			var diskon = $("#resep_diskon").val().replace(/[^0-9-,]/g, '').replace(",", ".");
			var t_diskon = $("#resep_t_diskon").val();
			if (diskon > 100 && t_diskon == "persen") {
				$("#modal_alert_resep_add_form").html(
					"<div class='alert alert-block alert-danger'>" +
					"<h4>Peringatan</h4>" +
					"<br /><b>Diskon</b> tidak boleh melebihi 100%" +
					"</div>"
				);
				return;
			}
			$("#modal_alert_resep_add_form").html("");
			resep.refreshBiayaTotal();
		});
		$("#resep_diskon").on("change", function() {
			var diskon = $("#resep_diskon").val().replace(/[^0-9-,]/g, '').replace(",", ".");
			if (diskon == "") {
				$("#resep_diskon").val("0,00");
			}
		});
		dokter = new DokterAction(
			"dokter",
			"depo_farmasi_irna",
			"penjualan_resep_luar",
			new Array()
		);
		dokter.setSuperCommand("dokter");
		obat = new ObatAction(
			"obat",
			"depo_farmasi_irna",
			"penjualan_resep_luar",
			new Array()
		);
		obat.setSuperCommand("obat");
		apoteker = new ApotekerAction(
			"apoteker",
			"depo_farmasi_irna",
			"penjualan_resep_luar",
			new Array()
		);
		apoteker.setSuperCommand("apoteker");
		bahan = new BahanAction(
			"bahan",
			"depo_farmasi_irna",
			"penjualan_resep_luar",
			new Array("embalase", "tuslah")
		);
		bahan.setSuperCommand("bahan");
		var dresep_columns = new Array("id", "id_penjualan_resep", "nama", "hja", "embalase", "tuslah", "subtotal", "tanggal_exp");
		dresep = new DResepAction(
			"dresep",
			"depo_farmasi_irna",
			"penjualan_resep_luar",
			dresep_columns
		);
		var resep_columns = new Array("id", "nomor_resep", "tanggal", "id_dokter", "nama_dokter", "noreg_pasien", "nrm_pasien", "nama_pasien", "alamat_pasien", "jenis", "perusahaan", "asuransi", "markup", "uri", "total", "edukasi", "dibatalkan", "embalase", "diskon", "t_diskon");
		resep = new ResepAction(
			"resep",
			"depo_farmasi_irna",
			"penjualan_resep_luar",
			resep_columns
		);
		resep.setSuperCommand("resep");
		resep.show_add_form = function() {
			ResepAction.prototype.show_add_form.call(this);
			<?php if (getSettings($db, "depo_farmasi7-paket_obat_non_racikan", 0) == 1) { ?>
				$("#paket_obat_non_racikan_add").show();
			<?php } ?>
		};
		resep.detail = function(id) {
			ResepAction.prototype.detail.call(this, id);
			<?php if (getSettings($db, "depo_farmasi7-paket_obat_non_racikan", 0) == 1) { ?>
				$("#paket_obat_non_racikan_add").hide();
			<?php } ?>
		};
		resep.view();
		var obat_jadi = new TableAction(
			"obat_jadi",
			"depo_farmasi_irna",
			"penjualan_resep_luar",
			new Array("tuslah", "embalase")
		);
		var obat_racikan = new TableAction(
			"obat_racikan",
			"depo_farmasi_irna",
			"penjualan_resep_luar",
			new Array("embalase", "tuslah", "biaya_racik")
		);
		var data_dokter = dokter.getViewData();
		$('#resep_nama_dokter').typeahead({
			minLength: 3,
			source: function(query, process) {
				data_dokter['kriteria'] = $('#resep_nama_dokter').val();
				var $items = new Array;
				$items = [""];
				$.ajax({
					url: '',
					type: 'POST',
					data: data_dokter,
					success: function(res) {
						var json = getContent(res);
						var the_data_proses = json.d.data;
						$items = [""];
						$.map(the_data_proses, function(data) {
							var group;
							group = {
								id: data.id,
								name: data.nama,
								toString: function() {
									return JSON.stringify(this);
								},
								toLowerCase: function() {
									return this.name.toLowerCase();
								},
								indexOf: function(string) {
									return String.prototype.indexOf.apply(this.name, arguments);
								},
								replace: function(string) {
									var value = '';
									value += this.name;
									if (typeof(this.level) != 'undefined') {
										value += ' <span class="pull-right muted">';
										value += this.level;
										value += '</span>';
									}
									return String.prototype.replace.apply('<div class="typeaheadiv">' + value + '</div>', arguments);
								}
							};
							$items.push(group);
						});
						process($items);
					}
				});
			},
			updater: function(item) {
				var item = JSON.parse(item);
				$("#resep_nama_pasien").focus();
				$("#resep_id_dokter").val(item.id);
				return item.name;
			}
		});
		var data_apoteker = apoteker.getViewData();
		$('#obat_racikan_nama_apoteker').typeahead({
			minLength: 3,
			source: function(query, process) {
				data_apoteker["kriteria"] = $('#obat_racikan_nama_apoteker').val();
				var $items = new Array;
				$items = [""];
				$.ajax({
					url: '',
					type: 'POST',
					data: data_apoteker,
					success: function(res) {
						var json = getContent(res);
						var the_data_proses = json.d.data;
						$items = [""];
						$.map(the_data_proses, function(data) {
							var group;
							group = {
								id: data.id,
								name: data.nama,
								toString: function() {
									return JSON.stringify(this);
								},
								toLowerCase: function() {
									return this.name.toLowerCase();
								},
								indexOf: function(string) {
									return String.prototype.indexOf.apply(this.name, arguments);
								},
								replace: function(string) {
									var value = '';
									value += this.name;
									if (typeof(this.level) != 'undefined') {
										value += ' <span class="pull-right muted">';
										value += this.level;
										value += '</span>';
									}
									return String.prototype.replace.apply('<div class="typeaheadiv">' + value + '</div>', arguments);
								}
							};
							$items.push(group);
						});
						process($items);
					}
				});
			},
			updater: function(item) {
				var item = JSON.parse(item);
				$("#obat_racikan_id_apoteker").val(item.id);
				$("#obat_racikan_name_apoteker").val(item.name);
				return item.name;
			}
		});
		var bahan_nama_bahan = bahan.getViewData();
		$('#bahan_nama_bahan').typeahead({
			minLength: 3,
			source: function(query, process) {
				var $items = new Array;
				$items = [""];
				bahan_nama_bahan['kriteria'] = $('#bahan_nama_bahan').val();
				$.ajax({
					url: '',
					type: 'POST',
					data: bahan_nama_bahan,
					success: function(res) {
						var json = getContent(res);
						var the_data_proses = json.dbtable.data;
						$items = [""];
						$.map(the_data_proses, function(data) {
							var group;
							group = {
								id: data.id,
								name: data.nama_obat,
								kode: data.kode_obat,
								toString: function() {
									return JSON.stringify(this);
								},
								toLowerCase: function() {
									return this.name.toLowerCase();
								},
								indexOf: function(string) {
									return String.prototype.indexOf.apply(this.name, arguments);
								},
								replace: function(string) {
									var value = '';
									value += this.kode + " - " + this.name;
									if (typeof(this.level) != 'undefined') {
										value += ' <span class="pull-right muted">';
										value += this.level;
										value += '</span>';
									}
									return String.prototype.replace.apply('<div class="typeaheadiv">' + value + '</div>', arguments);
								}
							};
							$items.push(group);
						});
						process($items);
					}
				});
			},
			updater: function(item) {
				var item = JSON.parse(item);
				bahan.select(item.id);
				$("#bahan_jumlah").focus();
				return item.name;
			}
		});
		var obat_jadi_nama = obat.getViewData();
		$('#obat_jadi_nama_obat').typeahead({
			minLength: 3,
			source: function(query, process) {
				var $items = new Array;
				$items = [""];
				obat_jadi_nama['kriteria'] = $('#obat_jadi_nama_obat').val();
				$.ajax({
					url: '',
					type: 'POST',
					data: obat_jadi_nama,
					success: function(res) {
						var json = getContent(res);
						var the_data_proses = json.dbtable.data;
						$items = [""];
						$.map(the_data_proses, function(data) {
							var group;
							group = {
								id: data.id,
								name: data.nama_obat,
								kode: data.kode_obat,
								toString: function() {
									return JSON.stringify(this);
								},
								toLowerCase: function() {
									return this.name.toLowerCase();
								},
								indexOf: function(string) {
									return String.prototype.indexOf.apply(this.name, arguments);
								},
								replace: function(string) {
									var value = '';
									value += this.kode + " - " + this.name;
									if (typeof(this.level) != 'undefined') {
										value += ' <span class="pull-right muted">';
										value += this.level;
										value += '</span>';
									}
									return String.prototype.replace.apply('<div class="typeaheadiv">' + value + '</div>', arguments);
								}
							};
							$items.push(group);
						});
						process($items);
					}
				});
			},
			updater: function(item) {
				var item = JSON.parse(item);
				obat.select(item.id);
				$("#obat_jadi_jumlah").focus();
				return item.name;
			}
		});
		$("#resep_nomor").keypress(function(e) {
			if (e.which == 13) {
				$('#resep_nama_pasien').focus();
			}
		});
		$("#resep_nama_pasien").keypress(function(e) {
			if (e.which == 13) {
				$("ul.typeahead").html("");
				$("ul.typeahead").hide();
				$('#resep_alamat_pasien').focus();
			}
		});
		$("#resep_alamat_pasien").keypress(function(e) {
			if (e.which == 13) {
				$("ul.typeahead").html("");
				$("ul.typeahead").hide();
				$('#resep_nama_dokter').focus();
			}
		});
		$("#resep_nama_dokter").keypress(function(e) {
			if (e.which == 13) {
				$("#resep_diskon").focus();
			}
		});
		$("#resep_jenis").on("change", function() {
			need_margin_penjualan_request = 1;
			resep.refreshHargaAndSubtotal();
			resep.refreshBiayaTotal();
		});
		$("#bahan_jumlah").keypress(function(e) {
			$("ul.typeahead").html("");
			$("ul.typeahead").hide();
			if (e.which == 13) {
				$('#bahan_save').trigger('click');
			}
		});
		$("#resep_diskon").keypress(function(e) {
			if (e.which == 13) {
				$('#resep_t_diskon').focus();
			}
		});
		$("#obat_jadi_nama_obat").keypress(function(e) {
			if (e.which == 13) {
				$('#obat_jadi_satuan').focus();
			}
		});
		$("#obat_jadi_satuan").keypress(function(e) {
			e.preventDefault();
			if (e.which == 13) {
				$('#obat_jadi_jumlah').focus();
			}
		});
		$("#obat_racikan_nama").keypress(function(e) {
			if (e.which == 13) {
				$('#obat_racikan_nama_apoteker').focus();
			}
		});
		$("#bahan_nama_bahan").keypress(function(e) {
			if (e.which == 13) {
				$('#bahan_satuan').focus();
			}
		});
		$("#bahan_satuan").keypress(function(e) {
			e.preventDefault();
			if (e.which == 13) {
				$('#bahan_jumlah').focus();
			}
		});
		$("#obat_jadi_jumlah").keypress(function(e) {
			$("ul.typeahead").html("");
			$("ul.typeahead").hide();
			if (e.which == 13) {
				$('#obat_jadi_save').trigger('click');
			}
		});
		<?php if (getSettings($db, "depo_farmasi7-penjualan_resep_luar-etiket_obat", 0) == 1) { ?>
			$("#obat_jadi_obat_luar").on("change", function() {
				var obat_luar = $(this).val();
				$(".obat_jadi_jumlah_pakai_sehari").show();
				if (obat_luar == 1) {
					$(".obat_jadi_satuan_pakai").hide();
					$(".obat_jadi_takaran_pakai").hide();
					$(".obat_jadi_pemakaian").hide();
					$(".obat_jadi_pemakaian_obat_luar").show();
				} else {
					$(".obat_jadi_satuan_pakai").show();
					$(".obat_jadi_takaran_pakai").show();
					$(".obat_jadi_pemakaian").show();
					$(".obat_jadi_pemakaian_obat_luar").hide();
				}
			});
			$("#obat_racikan_obat_luar").on("change", function() {
				var obat_luar = $(this).val();
				$(".obat_racikan_jumlah_pakai_sehari").show();
				if (obat_luar == 1) {
					$(".obat_racikan_satuan_pakai").hide();
					$(".obat_racikan_takaran_pakai").hide();
					$(".obat_racikan_pemakaian").hide();
					$(".obat_racikan_pemakaian_obat_luar").show();
				} else {
					$(".obat_racikan_satuan_pakai").show();
					$(".obat_racikan_takaran_pakai").show();
					$(".obat_racikan_pemakaian").show();
					$(".obat_racikan_pemakaian_obat_luar").hide();
				}
			});
			$("#obat_jadi_penggunaan").on("change", function() {
				var val = $(this).val();
				if (val == "1x" || val == "2x") {
					$(".obat_jadi_detail_penggunaan").show();
					if (val == "2x") {
						var html =	"<option value='' selected></option>" +
									"<option value='Pagi - Siang'>Pagi - Siang</option>" +
									"<option value='Pagi - Sore'>Pagi - Sore</option>";
						$("#obat_jadi_detail_penggunaan").html(html);
					} else {
						var html =	"<option value='' selected></option>" +
									"<option value='Pagi'>Pagi</option>" +
									"<option value='Siang'>Siang</option>" +
									"<option value='Sore'>Sore</option>" +
									"<option value='Malam'>Malam</option>";
						$("#obat_jadi_detail_penggunaan").html(html);
					}
				} else
					$(".obat_jadi_detail_penggunaan").hide();
			});
			$("#obat_racikan_penggunaan").on("change", function() {
				var val = $(this).val();
				if (val == "1x" || val == "2x") {
					$(".obat_racikan_detail_penggunaan").show();
					if (val == "2x") {
						var html =	"<option value='' selected></option>" +
									"<option value='Pagi - Siang'>Pagi - Siang</option>" +
									"<option value='Pagi - Sore'>Pagi - Sore</option>";
						$("#obat_racikan_detail_penggunaan").html(html);
					} else {
						var html =	"<option value='' selected></option>" +
									"<option value='Pagi'>Pagi</option>" +
									"<option value='Siang'>Siang</option>" +
									"<option value='Sore'>Sore</option>" +
									"<option value='Malam'>Malam</option>";
						$("#obat_racikan_detail_penggunaan").html(html);
					}
				} else
					$(".obat_racikan_detail_penggunaan").hide();
			});
		<?php } ?>
		shortcut.add("F2", function() {
			if ($('#obat_racikan_add_form').hasClass('in')) {
				$("#bahan_add").trigger("click");
			} else if (!$('#resep_add_form').hasClass('in') && !$('#obat_jadi_add_form').hasClass('in') && !$('#bahan_add_form').hasClass('in') && !$("#smis-chooser-modal").hasClass('in')) {
				$("#resep_add").trigger("click");
			} else if ($('#obat_jadi_add_form').hasClass('in')) {
				$("#obat_jadi_browse").trigger('click');
			} else if ($('#bahan_add_form').hasClass('in')) {
				$("#bahan_browse").trigger('click');
			}
		});
		shortcut.add("F3", function() {
			if ($('#resep_add_form').hasClass('in')) {
				$("#obat_jadi_add").trigger('click');
			}
		});
		shortcut.add("F4", function() {
			if ($('#resep_add_form').hasClass('in')) {
				$("#obat_racikan_add").trigger("click");
			}
		});
		shortcut.add("F6", function() {
			if ($('#resep_add_form').hasClass('in')) {
				$("#resep_save").trigger('click');
			} else if ($('#obat_racikan_add_form').hasClass('in')) {
				$('#racikan_save').trigger('click');
			} else if ($("#obat_jadi_add_form").hasClass('in')) {
				$("#obat_jadi_save").trigger('click');
			} else if ($("#bahan_add_form").hasClass('in')) {
				$("#bahan_save").trigger('click');
			}
		});
		shortcut.add("F7", function() {
			if ($("#obat_racikan_add_form").hasClass('in')) {
				$("#apoteker_browse").trigger("click");
			}
		});
		shortcut.add("F8", function() {
			if ($('#resep_add_form').hasClass('in')) {
				$("#dokter_browse").trigger("click");
			}
		});
		resep.refreshHargaAndSubtotal();
	});
</script>