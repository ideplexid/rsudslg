<?php
	class InstansiLainAdapter extends ArrayAdapter {
		public function adapt($row) {
			$array = array();
			$array['id'] = $row->id;
			$array['Nomor'] = self::format("digit8", $row->id);
			$array['Nama'] = $row->nama;
			$array['Alamat'] = $row->alamat;
			$array['No. Telp.'] = $row->telpon;
			$array['Margin Penjualan'] = $row->margin_jual . " %";
			return $array;
		}
	}
?>