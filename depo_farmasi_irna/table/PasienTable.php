<?php
class PasienTable extends Table {
	public function getHtml() {
		$html = parent::getHtml();
		$html .= addJS("depo_farmasi_irna/js/pasien_search.js", false);
		return $html;
	}
}
?>