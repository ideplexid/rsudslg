<?php
	require_once("depo_farmasi_irna/library/InventoryLibrary.php");
	require_once("smis-base/smis-include-service-consumer.php");
	global $db;

	$form = new Form("", "", "Depo Farmasi IRNA : Laporan Monitoring Pelayanan Resep Pasien");
	$tanggal_from_text = new Text("lmprp_tanggal_from", "lmprp_tanggal_from", date("Y-m-d 00:00"));
	$tanggal_from_text->setClass("mydatetime");
	$tanggal_from_text->setAtribute("data-date-format='yyyy-mm-dd hh:ii'");
	$form->addElement("Waktu Awal", $tanggal_from_text);
	$tanggal_to_text = new Text("lmprp_tanggal_to", "lmprp_tanggal_to", date("Y-m-d H:i"));
	$tanggal_to_text->setClass("mydatetime");
	$tanggal_to_text->setAtribute("data-date-format='yyyy-mm-dd hh:ii'");
	$form->addElement("Waktu Akhir", $tanggal_to_text);
	$consumer_service = new ServiceConsumer(
		$db, 
		"get_jenis_patient",
		null,
		"registration" 
	);
	$content = $consumer_service->execute()->getContent();
	$jenis_pasien_option = new OptionBuilder();
	$jenis_pasien_option->add("Semua", "%%", 1);
	foreach($content as $c){
		$jenis_pasien_option->add($c['name'], $c['value']);
	}
	$jenis_pasien_select = new Select("lmprp_jenis_pasien", "lmprp_jenis_pasien", $jenis_pasien_option->getContent());
	$form->addElement("Jenis Pasien", $jenis_pasien_select);
	$uri_option = new OptionBuilder();
	$uri_option->add("SEMUA", "%%", "1");
	$uri_option->add("RAWAT JALAN", "0");
	$uri_option->add("RAWAT INAP", "1");
	$uri_select = new Select("lmprp_uri", "lmprp_uri", $uri_option->getContent());
	$form->addElement("IRJA/IRNA", $uri_select);
	$show_button = new Button("", "", "Tampilkan");
	$show_button->setClass("btn-primary");
	$show_button->setIcon("icon-white icon-repeat");
	$show_button->setIsButton(Button::$ICONIC);
	$show_button->setAction("lmprp.view()");
	$download_button = new Button("", "", "Eksport XLS");
	$download_button->setClass("btn-inverse");
	$download_button->setIcon("fa fa-download");
	$download_button->setIsButton(Button::$ICONIC);
	$download_button->setAtribute("id='lmprp_export_button'");
	$btn_group = new ButtonGroup("noprint");
	$btn_group->addButton($show_button);
	$btn_group->addButton($download_button);
	$form->addElement("", $btn_group);

	$table = new Table(
		array("No.", "No. Trans.", "No. Resep", "Tanggal/Jam", "Dokter", "No. Reg.", "No. RM", "Pasien", "Alamat", "Jenis", "IRNA/IRJA", "Unit", "Mulai Entri", "Selesai Entri", "Durasi Entri (detik)", "Durasi Edukasi (detik)", "Durasi Layanan (detik)"),
		"",
		null,
		true
	);
	$table->setName("lmprp");
	$table->setAction(false);
	$table->setFooterVisible(false);

	if (isset($_POST['command'])) {
		$dbtable = new DBTable($db, InventoryLibrary::$_TBL_PENJUALAN_RESEP);
		if ($_POST['command'] == "get_jumlah") {
			$tanggal_from = $_POST['tanggal_from'];
			$tanggal_to = $_POST['tanggal_to'];
			$jenis_pasien = $_POST['jenis_pasien'];
			$uri = $_POST['uri'];
			$row = $dbtable->get_row("
				SELECT COUNT(*) AS 'jumlah'
				FROM " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . "
				WHERE tanggal >= '" . $tanggal_from . "' AND tanggal <= '" . $tanggal_to . "' AND dibatalkan = '0' AND prop NOT LIKE 'del' AND dibatalkan = '0' AND (tipe LIKE 'resep' OR tipe LIKE 'resep_luar') AND jenis LIKE '" . $jenis_pasien . "' AND uri LIKE '" . $uri . "'
			");
			$data = array(
				"jumlah"	=> $row->jumlah
			);
			echo json_encode($data);
		} else if ($_POST['command'] == "get_info") {
			$tanggal_from = $_POST['tanggal_from'];
			$tanggal_to = $_POST['tanggal_to'];
			$jenis_pasien = $_POST['jenis_pasien'];
			$uri = $_POST['uri'];
			$num = $_POST['num'];
			$row = $dbtable->get_row("
				SELECT *, CASE WHEN start_time != '0000-00-00 00:00:00' THEN TIMESTAMPDIFF(SECOND, start_time, finish_time) ELSE 0 END AS 'entry_duration_in_seconds', CASE WHEN educate_time != '0000-00-00 00:00:00' THEN TIMESTAMPDIFF(SECOND, finish_time, educate_time) ELSE 0 END AS 'educate_duration_in_seconds', CASE WHEN start_time != '0000-00-00 00:00:00' AND educate_time != '0000-00-00 00:00:00' THEN TIMESTAMPDIFF(SECOND, start_time, finish_time) + TIMESTAMPDIFF(SECOND, finish_time, educate_time) ELSE 0 END AS 'response_time_in_seconds'
				FROM " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . "
				WHERE tanggal >= '" . $tanggal_from . "' AND tanggal <= '" . $tanggal_to . "' AND dibatalkan = '0' AND prop NOT LIKE 'del' AND dibatalkan = '0' AND (tipe LIKE 'resep' OR tipe LIKE 'resep_luar') AND jenis LIKE '" . $jenis_pasien . "' AND uri LIKE '" . $uri . "'
				LIMIT " . $num . ", 1
			");
			$html = "";
			if ($row != null) {
				$uri = $row->uri == 1 ? "RAWAT INAP" : "RAWAT JALAN";
				$duration = 
				$html = "
					<tr>
						<td id='lmprp_nomor'></td>
						<td id='lmprp_id'><small>" . ArrayAdapter::format("only-digit8", $row->id) . "</small></td>
						<td id='lmprp_no_resep'><small>" . $row->nomor_resep . "</small></td>
						<td id='lmprp_tanggal'><small>" . ArrayAdapter::format("date d-m-Y, H:i:s", $row->tanggal) . "</small></td>
						<td id='lmprp_nama_dokter'><small>" . $row->nama_dokter . "</small></td>
						<td id='lmprp_noreg_pasien'><small>" . ArrayAdapter::format("only-digit8", $row->noreg_pasien) . "</small></td>
						<td id='lmprp_nrm_pasien'><small>" . ArrayAdapter::format("only-digit6", $row->nrm_pasien) . "</small></td>
						<td id='lmprp_nama_pasien'><small>" . $row->nama_pasien . "</small></td>
						<td id='lmprp_alamat_pasien'><small>" . $row->alamat_pasien . "</small></td>
						<td id='lmprp_jenis'><small>" . ArrayAdapter::format("unslug", $row->jenis) . "</small></td>
						<td id='lmprp_uri'><small>" . $uri . "</small></td>
						<td id='lmprp_unit'><small>" . ArrayAdapter::format("unslug", $row->ruangan) . "</small></td>
						<td id='lmprp_start_time'><small>" . ArrayAdapter::format("date d-m-Y, H:i:s", $row->start_time) . "</small></td>
						<td id='lmprp_finish_time'><small>" . ArrayAdapter::format("date d-m-Y, H:i:s", $row->finish_time) . "</small></td>
						<td id='lmprp_duration' style='display: none;'>" . $row->entry_duration_in_seconds . "</td>
						<td id='lmprp_f_duration'><small>" . ArrayAdapter::format("number", $row->entry_duration_in_seconds) . "</small></td>
						<td id='lmprp_edukasi' style='display: none;'>" . $row->educate_duration_in_seconds . "</td>
						<td id='lmprp_f_edukasi'><small>" . ArrayAdapter::format("number", $row->educate_duration_in_seconds) . "</small></td>
						<td id='lmprp_response_time' style='display: none;'>" . $row->response_time_in_seconds . "</td>
						<td id='lmprp_f_response_time'><small>" . ArrayAdapter::format("number", $row->response_time_in_seconds) . "</small></td>
					</tr>
				";
			}
			$data = array(
				"nomor_transaksi"	=> ArrayAdapter::format("only-digit8", $row->id),
				"nomor_resep"		=> $row->nomor_resep,
				"nama_pasien"		=> $row->nama_pasien,
				"nama_dokter"		=> $row->nama_dokter,
				"html"				=> $html
			);
			echo json_encode($data);
		} else if ($_POST['command'] == "export_xls") {
			ini_set("memory_limit", "256M");
			set_time_limit(0);
			$tanggal_from = $_POST['tanggal_from'];
			$tanggal_to = $_POST['tanggal_to'];
			$jenis_pasien = $_POST['jenis_pasien'];
			$uri = $_POST['uri'];
			require_once("smis-libs-out/php-excel/PHPExcel.php");
			$objPHPExcel = PHPExcel_IOFactory::load("depo_farmasi_irna/templates/template_monitoring_pelayanan_resep_pasien.xlsx");
			$objPHPExcel->setActiveSheetIndexByName("MONITORING PELAYANAN");
			$objWorksheet = $objPHPExcel->getActiveSheet();
			$objWorksheet->setCellValue("B3", "PERIODE : " . ArrayAdapter::format("date d-m-Y, H:i", $tanggal_from) . " s/d " . ArrayAdapter::format("date d-m-Y, H:i", $tanggal_to));
			$data = json_decode($_POST['d_data']);
			$objWorksheet->getStyle("P9")->getNumberFormat()->setFormatCode("#,##0");
			if ($_POST['num_rows'] - 2 > 0)
				$objWorksheet->insertNewRowBefore(8, $_POST['num_rows'] - 2);
			$start_row_num = 7;
			$end_row_num = 7;
			$row_num = $start_row_num;
			foreach ($data as $d) {
				$col_num = 1;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->nomor);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->nomor_transaksi);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->nomor_resep);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->tanggal);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->nama_dokter);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->noreg_pasien);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->nrm_pasien);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->nama_pasien);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->alamat_pasien);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->jenis);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->uri);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->unit);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->start_time);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->finish_time);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->duration);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->edukasi);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->response_time);
				$objWorksheet->getStyle("P" . $row_num)->getNumberFormat()->setFormatCode("#,##0");
				$objWorksheet->getStyle("Q" . $row_num)->getNumberFormat()->setFormatCode("#,##0");
				$objWorksheet->getStyle("R" . $row_num)->getNumberFormat()->setFormatCode("#,##0");
				$row_num++;
				$end_row_num++;
				$no++;
			}
			header("Content-type: application/vnd.ms-excel");	
			header("Content-Disposition: attachment; filename=LAPORAN_MONITORING_PELAYANAN_RESEP_PASIEN_" . ArrayAdapter::format("date Ymd_Hi", $tanggal_from) . "_" . ArrayAdapter::format("date Ymd_Hi", $tanggal_to) . ".xlsx");
			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");
			$objWriter->save("php://output");
		}
		return;
	}
	$loading_bar = new LoadingBar("lmprp_loading_bar", "");
	$button = new Button("", "", "Batal");
	$button->addClass("btn-primary");
	$button->setIsButton(Button::$ICONIC_TEXT);
	$button->setIcon("fa fa-close");
	$button->setAction("lmprp.cancel()");
	$loading_modal = new Modal("lmprp_loading_modal", "", "Proses..");
	$loading_modal->addHtml($loading_bar->getHtml(), "after");
	$loading_modal->addFooter($button);

	echo $loading_modal->getHtml();
	echo $form->getHtml();
	echo "<div id='table_content'>";
	echo $table->getHtml();
	echo "</div>";
	echo "<div id='lmprp_info'></div>";
	echo addJS("framework/smis/js/table_action.js");
	echo addJS("framework/bootstrap/js/bootstrap-datetimepicker.js");
	echo addCSS("framework/bootstrap/css/bootstrap-datetimepicker.css");
	echo addJS("depo_farmasi_irna/js/laporan_monitoring_pelayanan_resep_pasien_action.js", false);
	echo addJS("depo_farmasi_irna/js/laporan_monitoring_pelayanan_resep_pasien.js", false);
?>