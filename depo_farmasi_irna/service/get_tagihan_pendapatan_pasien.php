<?php
	global $db;
	require_once("depo_farmasi_irna/library/InventoryLibrary.php");

	if (isset($_POST['noreg_pasien'])) {
		$noreg_pasien = $_POST['noreg_pasien'];
		$rows = $db->get_result("
			SELECT ruangan, SUM(total) AS total 
			FROM " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . "
			WHERE noreg_pasien = '" . $noreg_pasien . "' AND dibatalkan = '0' AND prop NOT LIKE 'del'
			GROUP BY ruangan
		");

		$data = array();
		if ($rows != null) {
			foreach ($rows as $row) {
				$ruangan = $row->ruangan;
				if ($ruangan == "")
					$ruangan = "kosong";
				$data[$ruangan]['obat'] = $row->total;
			}
		}

		$rows = $db->get_result("
			SELECT ruangan, SUM(biaya) AS total
			FROM " . InventoryLibrary::$_TBL_ASUHAN_FARMASI . "
			WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop = ''
			GROUP BY ruangan
		");
		if ($rows != null) {
			foreach ($rows as $row) {
				$ruangan = $row->ruangan;
				if ($ruangan == "")
					$ruangan = "kosong";
				$data[$ruangan]['asuhan_farmasi'] = $row->total;
			}
		}
		
		echo json_encode($data);
	}
?>