<?php
	require_once("depo_farmasi_irna/library/InventoryLibrary.php");
	global $db;

	$bulan = $_POST['bulan'];
	$tahun = $_POST['tahun'];
	$id_obat_csv = $_POST['id_obat_csv'];
	$uri = $_POST['uri'];

	$jumlah = 0;

	$row = $db->get_row("
		SELECT COUNT(*) jumlah
		FROM (
			SELECT a.id
			FROM " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . " a INNER JOIN " . InventoryLibrary::$_TBL_PENJUALAN_OBAT_JADI . " b ON a.id = b.id_penjualan_resep
			WHERE a.prop = '' AND a.dibatalkan = 0 AND b.prop = '' AND YEAR(a.tanggal) = '" . $tahun . "' AND MONTH(a.tanggal) LIKE '" . $bulan . "' AND b.id_obat IN (" . $id_obat_csv . ") AND a.uri LIKE '" . $uri . "'
			UNION
			SELECT a.id
			FROM (" . InventoryLibrary::$_TBL_PENJUALAN_RESEP . " a INNER JOIN " . InventoryLibrary::$_TBL_PENJUALAN_OBAT_RACIKAN . " b ON a.id = b.id_penjualan_resep) INNER JOIN " . InventoryLibrary::$_TBL_BAHAN_PAKAI_OBAT_RACIKAN . " c ON b.id = c.id_penjualan_obat_racikan
			WHERE a.prop = '' AND a.dibatalkan = 0 AND b.prop = '' AND c.prop = '' AND YEAR(a.tanggal) = '" . $tahun . "' AND MONTH(a.tanggal) LIKE '" . $bulan . "' AND c.id_obat IN (" . $id_obat_csv . ") AND a.uri LIKE '" . $uri . "'
		) v
	");
	if ($row != null)
		$jumlah = $row->jumlah;
	
	$data = array(
		'jumlah' => $jumlah
	);
	echo json_encode($data);
?>