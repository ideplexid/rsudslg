<?php
	class InventoryLibrary {
		public static $_TBL_OBAT_MASUK = "smis_dfm7_obat_masuk";
		public static $_TBL_STOK_OBAT = "smis_dfm7_stok_obat";
		public static $_TBL_RIWAYAT_STOK_OBAT = "smis_dfm7_riwayat_stok_obat";
		public static $_TBL_KARTU_STOK_OBAT = "smis_dfm7_kartu_stok_obat";
		public static $_TBL_PENJUALAN_RESEP = "smis_dfm7_penjualan_resep";
		public static $_TBL_PENJUALAN_OBAT_JADI = "smis_dfm7_penjualan_obat_jadi";
		public static $_TBL_STOK_PAKAI_OBAT_JADI = "smis_dfm7_stok_pakai_obat_jadi";
		public static $_TBL_PENJUALAN_OBAT_RACIKAN = "smis_dfm7_penjualan_obat_racikan";
		public static $_TBL_BAHAN_PAKAI_OBAT_RACIKAN = "smis_dfm7_bahan_pakai_obat_racikan";
		public static $_TBL_STOK_PAKAI_BAHAN = "smis_dfm7_stok_pakai_bahan";
		public static $_TBL_RETUR_PENJUALAN_RESEP = "smis_dfm7_retur_penjualan_resep";
		public static $_TBL_DRETUR_PENJUALAN_RESEP = "smis_dfm7_dretur_penjualan_resep";
		public static $_TBL_RETUR_OBAT = "smis_dfm7_retur_obat";
		public static $_TBL_PENYESUAIAN_STOK_OBAT = "smis_dfm7_penyesuaian_stok_obat";
		public static $_TBL_PENGGUNAAN_OBAT = "smis_dfm7_penggunaan_obat";
		public static $_TBL_INSTANSI_LAIN = "smis_dfm7_instansi_lain";
		public static $_TBL_MARGIN_JUAL_OBAT = "smis_dfm7_margin_jual_obat";
		public static $_TBL_MARGIN_JUAL_JENIS_PASIEN = "smis_dfm7_margin_jual_jenis_pasien";
		public static $_TBL_MARGIN_JUAL_OBAT_JENIS_PASIEN = "smis_dfm7_margin_jual_obat_jenis_pasien";
		public static $_TBL_MARGIN_JUAL_RENTANG_HARGA = "smis_dfm7_margin_jual_rentang_harga";
		public static $_TBL_PEMBELIAN_UP = "smis_dfm7_pembelian_up";
		public static $_TBL_DPEMBELIAN_UP = "smis_dfm7_dpembelian_up";
		public static $_TBL_MUTASI_DEPO_KELUAR = "smis_dfm7_mutasi_keluar";
		public static $_TBL_STOK_MUTASI_DEPO_KELUAR = "smis_dfm7_stok_mutasi_keluar";
		public static $_TBL_NOMOR_RESEP = "smis_dfm7_nomor_resep";
		public static $_TBL_ASUHAN_FARMASI = "smis_dfm7_asuhan_farmasi";
		public static $_TBL_STOCK_OPNAME = "smis_dfm7_stock_opname";
		public static $_TBL_PAKET_OBAT = "smis_dfm7_paket_obat";

		public static function getNomorResep($prefix = "", $suffix = "", $postfix = "", $kronis = 0) {
			$prefix = date("dmy");
			$suffix = strtoupper(substr($suffix, 0, 1));
			if ($kronis == 1) {
				$suffix = "K";
			}

			global $db;
			$current_row = $db->get_row("
				SELECT *
				FROM " . InventoryLibrary::$_TBL_NOMOR_RESEP . "
				WHERE prefix = '" . $prefix . "' AND suffix = '" . $suffix . "'
			");
			if ($current_row == null) {
				$postfix = "0001";
				$db->query("
					INSERT INTO " . InventoryLibrary::$_TBL_NOMOR_RESEP . " (prefix, suffix, postfix)
					VALUES ('" . $prefix . "', '" . $suffix . "', '" . $postfix . "')
				");
				return $prefix . $suffix . $postfix;
			}

			$postfix = ArrayAdapter::format("only-digit4", $current_row->postfix + 1);
			$db->query("
				UPDATE " . InventoryLibrary::$_TBL_NOMOR_RESEP . " SET postfix = '" . $postfix . "'
				WHERE prefix = '" . $prefix . "' AND suffix = '" . $suffix . "'
			");

			return $prefix . $suffix . $postfix;
		}

		public static function getCurrentStock($db, $id_obat, $satuan, $konversi, $satuan_konversi) {
			$dbtable = new DBTable($db, InventoryLibrary::$_TBL_STOK_OBAT);
			$jumlah = 0;
			$row = $dbtable->get_row("
				SELECT SUM(a.sisa) AS 'jumlah'
				FROM " . InventoryLibrary::$_TBL_STOK_OBAT . " a LEFT JOIN " . InventoryLibrary::$_TBL_OBAT_MASUK . " b ON a.id_obat_masuk = b.id
				WHERE a.prop NOT LIKE 'del' AND b.prop NOT LIKE 'del' AND b.status = 'sudah' AND a.id_obat = '" . $id_obat . "'
			");
			if ($row != null)
				$jumlah += $row->jumlah;
			return $jumlah;
		}

		public static function getLastHPP($db, $id_obat, $satuan, $konversi, $satuan_konversi, $tanggal_to) {
			$params = array(
				"id_obat" 		=> $id_obat,
				"tanggal_to"	=> $tanggal_to
			);
			$consumer_service = new ServiceConsumer(
				$db,
				"get_last_hpp_until",
				$params,
				"gudang_farmasi"
			);
			$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
			$content = $consumer_service->execute()->getContent();
			$hpp_terakhir = 0;
			if ($content != null)
				$hpp_terakhir = $content[0];
			return $hpp_terakhir;
		}

		public static function getStockIn($db, $id_obat, $satuan, $konversi, $satuan_konversi, $tanggal_from, $tanggal_to) {
			$dbtable = new DBTable($db, InventoryLibrary::$_TBL_OBAT_MASUK);
			$jumlah = 0;
			// retur penjualan resep:
			$row = $dbtable->get_row("
				SELECT SUM(b.jumlah) AS 'jumlah'
				FROM (" . InventoryLibrary::$_TBL_RETUR_PENJUALAN_RESEP . " a LEFT JOIN " . InventoryLibrary::$_TBL_DRETUR_PENJUALAN_RESEP . " b ON a.id = b.id_retur_penjualan_resep) LEFT JOIN " . InventoryLibrary::$_TBL_STOK_OBAT . " c ON b.id_stok_obat = c.id
				WHERE a.prop NOT LIKE 'del' AND a.dibatalkan = '0' AND c.id_obat = '" . $id_obat . "' AND a.tanggal >= '" . $tanggal_from . " 00:00' AND a.tanggal <= '" . $tanggal_to . " 23:59'
			");
			if ($row != null)
				$jumlah += $row->jumlah;
			return $jumlah;
		}

		public static function getStockOut($db, $id_obat, $satuan, $konversi, $satuan_konversi, $tanggal_from, $tanggal_to) {
			$dbtable = new DBTable($db, InventoryLibrary::$_TBL_PENJUALAN_RESEP);
			$jumlah = 0;
			// penjualan obat jadi:
			$row = $dbtable->get_row("
				SELECT SUM(a.jumlah) AS 'jumlah'
				FROM " . InventoryLibrary::$_TBL_PENJUALAN_OBAT_JADI . " a LEFT JOIN " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . " b ON a.id_penjualan_resep = b.id
				WHERE a.prop NOT LIKE 'del' AND b.prop NOT LIKE 'del' AND b.dibatalkan = '0' AND a.id_obat = '" . $id_obat . "' AND b.tanggal >= '" . $tanggal_from . " 00:00' AND b.tanggal <= '" . $tanggal_to . " 23:59'
			");
			if ($row != null)
				$jumlah += $row->jumlah;
			// penjualan racikan:
			$row = $dbtable->get_row("
				SELECT SUM(a.jumlah) AS 'jumlah'
				FROM (" . InventoryLibrary::$_TBL_BAHAN_PAKAI_OBAT_RACIKAN . " a LEFT JOIN " . InventoryLibrary::$_TBL_PENJUALAN_OBAT_RACIKAN . " b ON a.id_penjualan_obat_racikan = b.id) LEFT JOIN " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . " c ON b.id_penjualan_resep = c.id
				WHERE a.prop NOT LIKE 'del' AND b.prop NOT LIKE 'del' AND c.prop NOT LIKE 'del' AND c.dibatalkan = '0' AND a.id_obat = '" . $id_obat . "' AND c.tanggal >= '" . $tanggal_from . " 00:00' AND c.tanggal <= '" . $tanggal_to . " 23:59'
			");
			if ($row != null)
				$jumlah += $row->jumlah;
			// retur gudang:
			$row = $dbtable->get_row("
				SELECT SUM(a.jumlah) AS 'jumlah'
				FROM " . InventoryLibrary::$_TBL_RETUR_OBAT . " a LEFT JOIN " . InventoryLibrary::$_TBL_STOK_OBAT . " b ON a.id_stok_obat = b.id
				WHERE a.prop NOT LIKE 'del' AND b.id_obat = '" . $id_obat . "' AND a.tanggal >= '" . $tanggal_from . "' AND a.tanggal <= '" . $tanggal_to . "'
			");
			if ($row != null)
				$jumlah += $row->jumlah;
			return $jumlah;
		}

		public static function getPenyesuaianStokPositif($db, $id_obat, $satuan, $konversi, $satuan_konversi, $tanggal_from, $tanggal_to) {
			$dbtable = new DBTable($db, InventoryLibrary::$_TBL_PENYESUAIAN_STOK_OBAT);
			$row = $dbtable->get_row("
				SELECT SUM(a.jumlah_baru - a.jumlah_lama) AS 'jumlah'
				FROM " . InventoryLibrary::$_TBL_PENYESUAIAN_STOK_OBAT . " a LEFT JOIN " . InventoryLibrary::$_TBL_STOK_OBAT . " b ON a.id_stok_obat = b.id
				WHERE a.prop NOT LIKE 'del' AND b.id_obat = '" . $id_obat . "' AND a.tanggal >= '" . $tanggal_from . "' AND a.tanggal <= '" . $tanggal_to . "' AND a.jumlah_baru >= a.jumlah_lama
			");
			$jumlah = 0;
			if ($row != null)
				$jumlah = $row->jumlah;
			return $jumlah;
		}

		public static function getPenyesuaianStokNegatif($db, $id_obat, $satuan, $konversi, $satuan_konversi, $tanggal_from, $tanggal_to) {
			$dbtable = new DBTable($db, InventoryLibrary::$_TBL_PENYESUAIAN_STOK_OBAT);
			$row = $dbtable->get_row("
				SELECT SUM(a.jumlah_lama - a.jumlah_baru) AS 'jumlah'
				FROM " . InventoryLibrary::$_TBL_PENYESUAIAN_STOK_OBAT . " a LEFT JOIN " . InventoryLibrary::$_TBL_STOK_OBAT . " b ON a.id_stok_obat = b.id
				WHERE a.prop NOT LIKE 'del' AND b.id_obat = '" . $id_obat . "' AND a.tanggal >= '" . $tanggal_from . "' AND a.tanggal <= '" . $tanggal_to . "' AND a.jumlah_baru < a.jumlah_lama
			");
			$jumlah = 0;
			if ($row != null)
				$jumlah = $row->jumlah;
			return $jumlah;
		}

		public static function getDetailPenjualanResepKIUP($db, $tanggal, $timestamp, $id_penjualan_resep, $total_tagihan_tercatat, $diskon_tercatat, $nama_dokter, $prop = "") {
			$result = array();
			$obat_jadi_rows = $db->get_result("
				SELECT *
				FROM " . InventoryLibrary::$_TBL_PENJUALAN_OBAT_JADI . "
				WHERE prop NOT LIKE 'del' AND id_penjualan_resep = '" . $id_penjualan_resep . "'
			");
			if ($obat_jadi_rows != null) {
				foreach ($obat_jadi_rows as $ojr) {
					$diskon_penjualan = 0;
					if ($total_tagihan_tercatat > 0 && $diskon_tercatat > 0)
						$diskon_penjualan = $ojr->subtotal / $total_tagihan_tercatat * $diskon_tercatat;
					$nilai_pendapatan = $ojr->subtotal - $diskon_penjualan;
					$ppn_keluar = $nilai_pendapatan * 0.1;
					$nilai_pendapatan = $nilai_pendapatan - $ppn_keluar;

					$info = array(
						'waktu'				=> $tanggal,
						'nama'				=> "Penjualan " . $ojr->nama_obat,
						'jumlah'			=> $ojr->jumlah,
						'biaya'				=> $nilai_pendapatan + $ppn_keluar - $diskon_penjualan,
						'keterangan'		=> "Penjualan " . $ojr->nama_obat . " : " . $ojr->jumlah . " x " . ArrayAdapter::format("only-money", ($nilai_pendapatan + $ppn_keluar - $diskon_penjualan) / $ojr->jumlah),
						'id'				=> "J" . $id_penjualan_resep . "-OJ" . $ojr->id . "-PENDAPATAN",
						'start'				=> $timestamp,
						'end'				=> $timestamp,
						'dokter'			=> $nama_dokter,
						'prop'				=> $prop,
						'grup_name'			=> "penjualan_resep"
					);
					$result[] = $info;
				}
			}

			$obat_racikan_rows = $db->get_result("
				SELECT *
				FROM " . InventoryLibrary::$_TBL_PENJUALAN_OBAT_RACIKAN . "
				WHERE prop NOT LIKE 'del' AND id_penjualan_resep = '" . $id_penjualan_resep . "'
			");
			if ($obat_racikan_rows != null) {
				foreach ($obat_racikan_rows as $orr) {
					$bahan_racikan_rows = $db->get_result("
						SELECT *
						FROM " . InventoryLibrary::$_TBL_BAHAN_PAKAI_OBAT_RACIKAN . "
						WHERE id_penjualan_obat_racikan = '" . $orr->id . "'
					");
					if ($bahan_racikan_rows != null) {
						$jasa_racik = 0;
						if ($bahan_racikan_rows != null)
							$jasa_racik = $orr->biaya_racik / count($bahan_racikan_rows);
						foreach ($bahan_racikan_rows as $brr) {
							$embalase = $brr->embalase;
							$tusla = $brr->tusla;
							$subtotal = $brr->jumlah * $brr->harga + $embalase + $tusla + $jasa_racik;

							$diskon_penjualan = 0;
							if ($total_tagihan_tercatat > 0 && $diskon_tercatat > 0)
								$diskon_penjualan = $subtotal / $total_tagihan_tercatat * $diskon_tercatat;
							$nilai_pendapatan = $subtotal - $diskon_penjualan;
							$ppn_keluar = $nilai_pendapatan * 0.1;
							$nilai_pendapatan = $nilai_pendapatan - $ppn_keluar;

							$info = array(
								'waktu'				=> $tanggal,
								'nama'				=> "Penjualan Racikan " . $orr->nama . " Bahan " . $brr->nama_obat,
								'jumlah'			=> $brr->jumlah,
								'biaya'				=> $nilai_pendapatan + $ppn_keluar - $diskon_penjualan,
								'keterangan'		=> "Penjualan Racikan " . $orr->nama . " Bahan " . $brr->nama_obat . " : " . $brr->jumlah . " x " . ArrayAdapter::format("only-money", ($nilai_pendapatan + $ppn_keluar - $diskon_penjualan) / $brr->jumlah),
								'id'				=> "J" . $id_penjualan_resep . "-OR" . $orr->id . "." . $brr->id . "-PENDAPATAN",
								'start'				=> $timestamp,
								'end'				=> $timestamp,
								'dokter'			=> $nama_dokter,
								'prop'				=> $prop,
								'grup_name'			=> "penjualan_resep"
							);							
							$result[] = $info;
						}
					}
				}
			}
			return $result;
		}

		public static function getJSONDetailPenjualanResepKIUP($db, $id_penjualan_resep, $nama_dokter) {
			$result = array(
				'nama_dokter' => $nama_dokter
			);
			$obat_jadi_rows = $db->get_result("
				SELECT *
				FROM " . InventoryLibrary::$_TBL_PENJUALAN_OBAT_JADI . "
				WHERE prop NOT LIKE 'del' AND id_penjualan_resep = '" . $id_penjualan_resep . "'
			");
			if ($obat_jadi_rows != null) {
				foreach ($obat_jadi_rows as $ojr) {
					$jaspel = $ojr->embalase + $ojr->tusla;
					$total = $ojr->harga * $ojr->jumlah + $jaspel;
					$info = array(
						'nama_obat' => $ojr->nama_obat,
						'jumlah'	=> $ojr->jumlah,
						'harga' 	=> $ojr->harga,
						'jaspel' 	=> $jaspel,
						'total' 	=> $total,
					);
					$result['obat'][] = $info;
				}
			}

			$obat_racikan_rows = $db->get_result("
				SELECT *
				FROM " . InventoryLibrary::$_TBL_PENJUALAN_OBAT_RACIKAN . "
				WHERE id_penjualan_resep = ' . $id_penjualan_resep . '
			");

			if ($obat_racikan_rows != null) {
				foreach ($obat_racikan_rows as $orr) {
					$jaspel = $orr->embalase + $orr->tusla + $orr->biaya_racik;
					$total = $orr->harga * $orr->jumlah + $jaspel;
					$info = array(
						'nama_obat' => $orr->nama,
						'jumlah' 	=> $orr->jumlah,
						'harga' 	=> $orr->harga,
						'jaspel' 	=> $jaspel,
						'total' 	=> $total,
					);
					$result['obat'][] = $info;
				}
			}
			return json_encode($result);
		}
	}
?>