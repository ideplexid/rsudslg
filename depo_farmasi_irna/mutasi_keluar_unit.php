<?php
	require_once("depo_farmasi_irna/library/InventoryLibrary.php");
	require_once("smis-libs-inventory/mutasi_keluar_unit.php");
	global $db;
	$mutasi_keluar_unit = new MutasiKeluarUnit($db, "Depo Farmasi IRNA/IRNA", InventoryLibrary::$_TBL_OBAT_MASUK, InventoryLibrary::$_TBL_STOK_OBAT, InventoryLibrary::$_TBL_MUTASI_DEPO_KELUAR, InventoryLibrary::$_TBL_STOK_MUTASI_DEPO_KELUAR, InventoryLibrary::$_TBL_KARTU_STOK_OBAT, InventoryLibrary::$_TBL_RIWAYAT_STOK_OBAT, "depo_farmasi_irna");
	$mutasi_keluar_unit->initialize();
?>