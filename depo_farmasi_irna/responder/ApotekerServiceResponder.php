<?php
class ApotekerServiceResponder extends ServiceResponder {
	public function getData() {
		$data = $_POST;
		if (isset($_POST['kriteria']) && $_POST['kriteria'] != "") {
			$kriteria = array(
					"multiple_kriteria"	=>	array(
						"nip"		=> $_POST['kriteria'],
						"nama"		=> $_POST['kriteria'],
						"jabatan"	=> "apoteker"
					)
			);
			$data['kriteria'] = json_encode($kriteria);
		} else {
			$data['kriteria'] = "apoteker";
		}
		return $data;
	}
}
?>