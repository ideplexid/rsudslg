<?php
	require_once("depo_farmasi_irna/library/InventoryLibrary.php");
	require_once("smis-libs-class/Locker.php");
	require_once("depo_farmasi_irna/locker/PenjualanResepLocker.php");
	
	class PenjualanResepLuarDBResponder extends DBResponder {
		protected $message_failure;
		
		public function command($command) {
			if ($command != "print_prescription"  && $command != "print_claim_prescription" && $command!="save" && $command != "print_all_etiket" && $command != "print_etiket_obat_jadi" && $command != "print_etiket_obat_racikan") {
				return parent::command($command);
			}	
			$pack = null;
			if($command=="save"){
				global $user;
				$locker=new PenjualanResepLocker($this->dbtable->get_db(), $user, "penjualan-resep-depo_farmasi");
				$locker->setVariable($this);
				$pack = new ResponsePackage();
				$content=$locker->execute();
				$pack->setContent($content);
				$pack->setStatus(ResponsePackage::$STATUS_OK);
				
				if($content['success']==1){
					$pack->setAlertVisible(true);
					$pack->setAlertContent("Data Saved", "Your Data Had Been Saved", ResponsePackage::$TIPE_INFO);
				}else{
					$pack->setAlertVisible(false);
				}
			}else if ($command == "print_prescription") {
				$pack = new ResponsePackage();
				$content = $this->print_prescription();
				// $content = $this->print_prescription_escp();
				$pack->setContent($content);
				$pack->setStatus(ResponsePackage::$STATUS_OK);
			} else if ($command == "print_claim_prescription") {
				$pack = new ResponsePackage();
				$content = $this->print_claim_prescription();
				// $content = $this->print_claim_prescription_escp();
				$pack->setContent($content);
				$pack->setStatus(ResponsePackage::$STATUS_OK);
			} else if ($command == "print_all_etiket") {
				$pack = new ResponsePackage();
				$content = $this->printAllEtiket();
				$pack->setContent($content);
				$pack->setStatus(ResponsePackage::$STATUS_OK);
			} else if ($command == "print_etiket_obat_jadi") {
				$pack = new ResponsePackage();
				$content = $this->printSingleEtiket("obat_jadi");
				$pack->setContent($content);
				$pack->setStatus(ResponsePackage::$STATUS_OK);
			} else if ($command == "print_etiket_obat_racikan") {
				$pack = new ResponsePackage();
				$content = $this->printSingleEtiket("obat_racikan");
				$pack->setContent($content);
				$pack->setStatus(ResponsePackage::$STATUS_OK);
			}
			return $pack->getPackage();
		}

		public function printAllEtiket() {
			require_once("depo_farmasi_irna/class/MultipleEtiketPDF.php");
			$id = $_POST['id'];
			global $user;
			$etiket = new MultipleEtiketPDF($this->dbtable->get_db(), $id, $user->getNameOnly());
			return $etiket->Draw();
		}

		public function printSingleEtiket($tipe) {
			require_once("depo_farmasi_irna/class/SingleEtiketPDF.php");
			$id = $_POST['id'];
			global $user;
			$etiket = new SingleEtiketPDF($this->dbtable->get_db(), $id, $user->getNameOnly(), $tipe);
			return $etiket->Draw();
		}

		public function addMessage($msg,$hidden_tbl_name=""){
			$this->message_failure.="<li data-stable='".$hidden_tbl_name."' >".$msg."</li>";
		}
		public function getMessage(){
			return "<ul>".$this->message_failure."</ul>";
		}
		protected function formatMoneyForPrint($money){
			return sprintf(' Rp.%14s', ArrayAdapter::format("only-money", $money));
		}
		public function print_prescription_escp() {
			$id = $_POST['id'];
			$data = $this->dbtable->get_row("
				SELECT *
			    FROM " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . "
				WHERE id = '" . $id . "'
			");
			$obat_jadi_data = $this->dbtable->get_result("
				SELECT nama_obat AS 'nama', jumlah, satuan, harga, subtotal, embalase, tusla, 0 AS 'biaya_racik', 'obat_jadi' AS 'label'
				FROM " . InventoryLibrary::$_TBL_PENJUALAN_OBAT_JADI . "
				WHERE prop NOT LIKE 'del' AND id_penjualan_resep = '" . $id . "'
			");
			$obat_racik_data = $this->dbtable->get_result("
				SELECT id, nama AS 'nama', jumlah, 'Racikan' AS 'satuan', harga, embalase, tusla, biaya_racik, subtotal, 'obat_racikan' AS 'label'
				FROM " . InventoryLibrary::$_TBL_PENJUALAN_OBAT_RACIKAN . "
				WHERE prop NOT LIKE 'del' AND id_penjualan_resep = '" . $id . "'
			");
			
			$no_barang = 1;
			$total = 0;
			foreach($obat_jadi_data as $ojd) {
				$items[] = array(
					"no" => $no_barang++,
					"name" => $ojd->nama,
					"qty" => $ojd->jumlah . " " . $ojd->satuan,
					"prc" => $this->formatMoneyForPrint($ojd->harga),
					"jaspel" => $this->formatMoneyForPrint($ojd->embalase + $ojd->tusla),
					"sbt" => $this->formatMoneyForPrint($ojd->subtotal)
				);
				$total += $ojd->subtotal;
			}
			foreach($obat_racik_data as $ord) {
				$items[] = array(
					"no" => $no_barang++,
					"name" => ArrayAdapter::format("unslug", $ord->nama),
					"qty" => $ord->jumlah . " " . $ord->satuan,
					"prc" => $this->formatMoneyForPrint($ord->harga),
					"jaspel" => $this->formatMoneyForPrint($ord->embalase + $ord->tusla + $ord->biaya_racik),
					"sbt" => $this->formatMoneyForPrint($ord->subtotal)
				);			
				$total += $ord->subtotal;
				
				//detail racikan:
				$bahan_racik_data = $this->dbtable->get_result("
					SELECT *
					FROM " . InventoryLibrary::$_TBL_BAHAN_PAKAI_OBAT_RACIKAN . "
					WHERE id_penjualan_obat_racikan = '" . $ord->id . "'
				");
				
				$bahan_no = 'a';
				foreach($bahan_racik_data as $brd) {
					$items[] = array(
						"no" => '',
						"name" => $bahan_no++ . ". " . $brd->nama_obat,
						"qty" => $brd->jumlah . " " . $brd->satuan,
						"prc" => '',
						"jaspel" => '',
						"sbt" => ''
					);
				}
			}
			
			$diskonLabel = 'DISKON';
			$diskonValue = $data->diskon;		
			if ($data->diskon > 0 && ($data->t_diskon == "persen" || $data->t_diskon == "gratis")) {
				$diskonLabel = "DISKON (" . $data->diskon . " %)";
				$diskonValue =  $total * $data->diskon / 100;
			}
			
			global $user;
			loadLibrary("smis-libs-function-math");
							
			$template = file_get_contents("depo_farmasi_irna/webprint/penjualan_obat.json");
				
			return array(
				'template' => json_decode($template, true),
				'data' => array(
					'kode_resep' => $data->uri == 1 ? "RI" : "RJ",
					'no_penjualan' => ArrayAdapter::format('only-digit8', $data->id),
					'no_resep' => $data->nomor_resep,
					'tgl_resep' => ArrayAdapter::format('date d-m-Y H:i', $data->tanggal),
					'nama_dokter' => substr(strtoupper($data->nama_dokter), 0, 30),
					'nama_pasien' => substr(strtoupper($data->nama_pasien), 0, 30),
					'no_registrasi' => "-",
					'nrm_pasien' => "-",
					'alamat_pasien' => substr(strtoupper($data->alamat_pasien), 0, 30),
					'jenis_pasien' => ArrayAdapter::format('unslug', $data->jenis) . ' - RAWAT ' . ($data->uri == 1 ? 'INAP' : 'JALAN'),
					'total' => $this->formatMoneyForPrint($total),
					'diskon_label' => $diskonLabel,
					'diskon' => $this->formatMoneyForPrint($diskonValue),
					'tagihan' => $this->formatMoneyForPrint($data->total),
					'terbilang' => numbertell($data->total),
					'ppn_info' => $data->uri == 0 ? '' : '* Sudah termasuk PPn (' . $data->ppn . ' %)',
					'petugas' => $data->operator,
					'items' => $items
				)
			);
		}
		public function print_claim_prescription_escp() {
			$id = $_POST['id'];
			$header_data = $this->dbtable->get_row("
				SELECT *
				FROM " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . "
				WHERE id = '" . $id . "'
			");
			$nomor_resep = $header_data->nomor_resep;
			$resep_result = $this->dbtable->get_result("
				SELECT *
				FROM " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . "
				WHERE nomor_resep = '" . $nomor_resep . "' AND tipe = 'resep_luar' AND prop NOT LIKE 'del' AND dibatalkan = 0
				ORDER BY id ASC
			");
			
			$filter_id = "id_penjualan_resep = '" . $id . "'";
			if ($resep_result != null) {
				$filter_id = "(";
				$cur = 1;
				$count = count($resep_result);
				foreach($resep_result as $rr) {
					$filter_id .= "id_penjualan_resep = '" . $rr->id . "'";
					if ($cur < $count) {
						$filter_id .= " OR ";
						$cur++;
					}
				}
				$filter_id .= ")";
			}
			
			$obat_jadi_data = $this->dbtable->get_result("
				SELECT nama_obat AS 'nama', jumlah, satuan, harga, subtotal, embalase, tusla, 0 AS 'biaya_racik', 'obat_jadi' AS 'label'
				FROM " . InventoryLibrary::$_TBL_PENJUALAN_OBAT_JADI . "
				WHERE prop NOT LIKE 'del' AND " . $filter_id . "
			");
			$obat_racik_data = $this->dbtable->get_result("
				SELECT id, nama AS 'nama', jumlah, 'Racikan' AS 'satuan', harga, embalase, tusla, biaya_racik, subtotal, 'obat_racikan' AS 'label'
				FROM " . InventoryLibrary::$_TBL_PENJUALAN_OBAT_RACIKAN . "
				WHERE prop NOT LIKE 'del' AND " . $filter_id . "
			");
			
			$no_barang = 1;
			$total = 0;
			foreach($obat_jadi_data as $ojd) {
				$items[] = array(
					"no" => $no_barang++,
					"name" => $ojd->nama,
					"qty" => $ojd->jumlah . " " . $ojd->satuan,
					"prc" => $this->formatMoneyForPrint($ojd->harga),
					"jaspel" => $this->formatMoneyForPrint($ojd->embalase + $ojd->tusla),
					"sbt" => $this->formatMoneyForPrint($ojd->subtotal)
				);
				$total += $ojd->subtotal;
			}
			foreach($obat_racik_data as $ord) {
				$items[] = array(
					"no" => $no_barang++,
					"name" => ArrayAdapter::format("unslug", $ord->nama),
					"qty" => $ord->jumlah . " " . $ord->satuan,
					"prc" => $this->formatMoneyForPrint($ord->harga),
					"jaspel" => $this->formatMoneyForPrint($ord->embalase + $ord->tusla + $ord->biaya_racik),
					"sbt" => $this->formatMoneyForPrint($ord->subtotal)
				);			
				$total += $ord->subtotal;
				
				//detail racikan:
				$bahan_racik_data = $this->dbtable->get_result("
					SELECT *
					FROM " . InventoryLibrary::$_TBL_BAHAN_PAKAI_OBAT_RACIKAN . "
					WHERE id_penjualan_obat_racikan = '" . $ord->id . "'
				");
				
				$bahan_no = 'a';
				foreach($bahan_racik_data as $brd) {
					$items[] = array(
						"no" => '',
						"name" => $bahan_no++ . ". " . $brd->nama_obat,
						"qty" => $brd->jumlah . " " . $brd->satuan,
						"prc" => '',
						"jaspel" => '',
						"sbt" => ''
					);
				}
			}
			
			$diskonLabel = 'DISKON';
			$diskonValue = $resep_result[0]->diskon;		
			if ($resep_result[0]->diskon > 0 && ($resep_result[0]->t_diskon == "persen" || $resep_result[0]->t_diskon == "gratis")) {
				$diskonLabel = "DISKON (" . $resep_result[0]->diskon . " %)";
				$diskonValue =  $total * $resep_result[0]->diskon / 100;
			}
			
			global $user;
			loadLibrary("smis-libs-function-math");
							
			$template = file_get_contents("depo_farmasi_irna/webprint/penjualan_obat.json");
				
			return array(
				'template' => json_decode($template, true),
				'data' => array(
					'kode_resep' => $data->uri == 1 ? "RI" : "RJ",
					'no_penjualan' => ArrayAdapter::format('only-digit8', $resep_result[0]->id),
					'no_resep' => $resep_result[0]->nomor_resep,
					'tgl_resep' => ArrayAdapter::format('date d-m-Y H:i', $resep_result[0]->tanggal),
					'nama_dokter' => substr(strtoupper($resep_result[0]->nama_dokter), 0, 30),
					'nama_pasien' => substr(strtoupper($resep_result[0]->nama_pasien), 0, 30),
					'no_registrasi' => "-",
					'nrm_pasien' => "-",
					'alamat_pasien' => substr(strtoupper($resep_result[0]->alamat_pasien), 0, 30),
					'jenis_pasien' => ArrayAdapter::format('unslug', $resep_result[0]->jenis) . ' - RAWAT ' . ($data->uri == 1 ? 'INAP' : 'JALAN'),
					'total' => $this->formatMoneyForPrint($total),
					'diskon_label' => $diskonLabel,
					'diskon' => $this->formatMoneyForPrint($diskonValue),
					'tagihan' => $this->formatMoneyForPrint($total),
					'terbilang' => numbertell($resep_result[0]->total),
					'ppn_info' => $data->uri == 0 ? '' : '* Sudah termasuk PPn (' . $resep_result[0]->ppn . ' %)',
					'petugas' => "",
					'items' => $items
				)
			);
		}
		public function print_prescription() {
			$id = $_POST['id'];
			$data = $this->dbtable->get_row("
				SELECT *
			    FROM " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . "
				WHERE id = '" . $id . "'
			");
			$obat_jadi_sql = "
				SELECT nama_obat AS 'nama', jumlah, satuan, harga, subtotal, embalase, tusla, 0 AS 'biaya_racik', 'obat_jadi' AS 'label'
				FROM " . InventoryLibrary::$_TBL_PENJUALAN_OBAT_JADI . "
				WHERE prop NOT LIKE 'del' AND id_penjualan_resep = '" . $id . "'
			";
			$obat_jadi_data = $this->dbtable->get_result($obat_jadi_sql);
			$obat_racik_sql = "
				SELECT id, nama AS 'nama', jumlah, 'Racikan' AS 'satuan', harga, embalase, tusla, biaya_racik, subtotal, 'obat_racikan' AS 'label'
				FROM " . InventoryLibrary::$_TBL_PENJUALAN_OBAT_RACIKAN . "
				WHERE prop NOT LIKE 'del' AND id_penjualan_resep = '" . $id . "'
			";
			$obat_racik_data = $this->dbtable->get_result($obat_racik_sql);
			$content = "<table border='0' width='100%' cellpadding='3' class='etiket_header'>";
				$content .= "<tr>";
					$content .= "<td>";
						$content .= "<div align='center'><table border='0' class='etiket_content' >";
							$content .= "<tr>";
								$content .= "<td colspan='5' align='center' style='font-size: 14px !important;'><b>DEPO FARMASI " . ucwords(getSettings($this->dbtable->get_db(), "smis_autonomous_title", "")) . " - RESEP (NON-KIUP)</b></td>";
							$content .= "</tr>";
						$content .= "</div></table>";
						$content .= "<div align='center'><table border='0' class='etiket_content' >";
							$content .= "<tr>";
								$content .= "<td colspan='1'>No. Penjualan</td>";
								$content .= "<td colspan='1'>: " .  ArrayAdapter::format("only-digit8", $data->id) . "</td>";
								$content .= "<td colspan='1' class='tpadding'>No. Resep</td>";
								$content .= "<td colspan='2'>: " .  $data->nomor_resep . "</td>";
							$content .= "</tr>";
							$content .= "<tr>";
								$content .= "<td colspan='1'>Tgl. Resep</td>"; 
								$content .= "<td colspan='1'>: " . ArrayAdapter::format("date d-m-Y H:i", $data->tanggal) . "</td>"; 
								$content .= "<td colspan='1' class='tpadding'>Nama Dokter</td>";
								$content .= "<td colspan='2'>: " . substr(strtoupper($data->nama_dokter), 0, 30) . "</td>";
							$content .= "</tr>";
							$content .= "<tr>";
								$content .= "<td colspan='1'>Nama Pasien</td>";
								$content .= "<td colspan='1'>: " . substr(strtoupper($data->nama_pasien), 0, 30) . "</td>";
								$content .= "<td colspan='1' class='tpadding'>Alamat Pasien</td>";
								$content .= "<td colspan='2'>: " . substr(strtoupper($data->alamat_pasien), 0, 30) . "</td>";
							$content .= "</tr>";
							$content .= "<tr>";
								$content .= "<td colspan='1'>Jns. Pasien</td>";
								$jenis_pasien = ArrayAdapter::format("unslug", $data->jenis) . " - ";
								if ($data->uri == 1) {
									$jenis_pasien .= "RAWAT INAP";
								} else {
									$jenis_pasien .= "RAWAT JALAN";
								}
								$content .= "<td colspan='4'>: " . $jenis_pasien . "</td>";
							$content .= "</tr>";
						$content .= "</table></div>";
					$content .= "</td>";
				$content .= "</tr>";
				$content .= "<tr>";
					$content .= "<td>";
						$content .= "<div align='center'><table border='1' class='etiket_content'>";
						$content .= "<tr align='center'>";
							$content .= "<td colspan='1'>No.</td>";
							$content .= "<td colspan='1'>Obat</td>";
							$content .= "<td colspan='1'>Jumlah</td>";
							// $content .= "<td colspan='1'>Harga Satuan</td>";
							// $content .= "<td colspan='1'>Jasa Pelyanan</td>";
							$content .= "<td colspan='1'>Subtotal</td>";
						$content .= "</tr>";
						$no_barang = 1;
						$total = 0;
						foreach($obat_jadi_data as $ojd) {
							$content .= "<tr>";
								$content .= "<td colspan='1'>" . $no_barang++ . "</td>";
								$content .= "<td colspan='1'>" . $ojd->nama . "</td>";
								$content .= "<td colspan='1'>" . $ojd->jumlah . " " . $ojd->satuan . "</td>";
								$content .= "<td colspan='1' align='right' style='display: none;'>" . ArrayAdapter::format("money Rp.", $ojd->harga) . "</td>";
								$content .= "<td colspan='1' align='right' style='display: none;'>" . ArrayAdapter::format("money Rp.", $ojd->embalase + $ojd->tusla) . "</td>";
								$content .= "<td colspan='1' align='right'>" . ArrayAdapter::format("money Rp.", $ojd->subtotal) . "</td>";
							$content .= "</tr>";
							$total += $ojd->subtotal;
						}
						foreach($obat_racik_data as $ord) {
							$content .= "<tr>";
								$content .= "<td colspan='1'>" . $no_barang++ . "</td>";
								$content .= "<td colspan='1'>" . ArrayAdapter::format("unslug", $ord->nama) . "</td>";
								$content .= "<td colspan='1'>" . $ord->jumlah . " " . $ord->satuan . "</td>";
								$content .= "<td colspan='1' align='right' style='display: none;'>" . ArrayAdapter::format("money Rp.", $ord->harga) . "</td>";
								$content .= "<td colspan='1' align='right' style='display: none;'>" . ArrayAdapter::format("money Rp.", $ord->embalase + $ord->tusla + $ord->biaya_racik) . "</td>";
								$content .= "<td colspan='1' align='right'>" . ArrayAdapter::format("money Rp.", $ord->subtotal) . "</td>";
							$content .= "</tr>";
							$total += $ord->subtotal;
							//detail racikan:
							$bahan_racik_data = $this->dbtable->get_result("
								SELECT *
								FROM " . InventoryLibrary::$_TBL_BAHAN_PAKAI_OBAT_RACIKAN . "
								WHERE id_penjualan_obat_racikan = '" . $ord->id . "'
							");
							$bahan_no = 'a';
							foreach($bahan_racik_data as $brd) {
								$content .= "<tr style='font-size: 12px !important;'>";
									$content .= "<td colspan='1'></td>";
									$content .= "<td colspan='1'>" . $bahan_no++ . ". " . $brd->nama_obat . "</td>";
									$content .= "<td colspan='1'>" . $brd->jumlah . " " . $brd->satuan . "</td>";
									$content .= "<td colspan='3' align='right'></td>";
								$content .= "</tr>";
							}
						}
						$content .= "<tr>";
							$content .= "<td colspan='3' align='right'>Total</td>";
							$content .= "<td colspan='1' align='right'>" . ArrayAdapter::format("money Rp.", $total) . "</td>";
						$content .= "</tr>";
						$content .= "<tr>";
						if ($data->diskon > 0) {
							if ($data->t_diskon == "persen" || $data->t_diskon == "gratis") {
								$v_diskon = ($total * $data->diskon) / 100;
								$content .= "<td colspan='3' align='right'>Diskon (" . $data->diskon . " %)</td>";
								$content .= "<td colspan='1' align='right'>" . ArrayAdapter::format("money Rp.", $v_diskon) . "</td>";
							} else {
								$content .= "<td colspan='3' align='right'>Diskon</td>";
								$content .= "<td colspan='1' align='right'>" . ArrayAdapter::format("money Rp.", $data->diskon) . "</td>";
							}
						}
						$content .= "</tr>";
						$content .= "<tr>";
							$content .= "<td colspan='3' align='right'>Tagihan</td>";
							$content .= "<td colspan='1'>" . ArrayAdapter::format("money Rp.", $data->total) . "</td>";
						$content .= "</tr>";
						loadLibrary("smis-libs-function-math");
						$content .= "<tr>";
							$content .= "<td colspan='1' align='right'>Terbilang</td>";
							$content .= "<td colspan='9'>" . numbertell($data->total) . " Rupiah </td>";
						$content .= "</tr>";
						$content .= "</table></div>";
					$content .= "</td>";
				$content .= "</tr>";
			if ($data->uri == 1) {
				$content .= "</tr>";
					$content .= "<td colspan='4'><i>* Sudah termasuk PPn (" . $data->ppn . " %)</i></td>";
				$content .= "<tr>";
			}
				$content .= "<tr>";
					$content .= "<td colspan='4'>";
						$content .= "<table border='0' align='center' class='etiket_content'>";
							$content .= "<tr>";
								$content .= "<td align='center'>PENERIMA,</td>";
								$content .= "<td>&Tab;</td>";
								$content .= "<td align='center'>PETUGAS DEPO FARMASI,</td>";
							$content .= "</tr>";
							$content .= "<tr>";
								$content .= "<td></td>";
								$content .= "<td>&Tab;</td>";
								$content .= "<td></td>";
							$content .= "</tr>";
							$content .= "<tr>";
								$content .= "<td></td>";
								$content .= "<td>&Tab;</td>";
								$content .= "<td></td>";
							$content .= "</tr>";
							$content .= "<tr>";
								$content .= "<td align='center'>(_____________________)</td>";
								$content .= "<td>&Tab;</td>";
								if ($data->operator != null)
									$content .= "<td align='center'>" . $data->operator . "</td>";
								else
									$content .= "<td align='center'>(_____________________)</td>";
							$content .= "</tr>";
							$content .= "<tr>";
								$content .= "<td></td>";
							$content .= "</tr>";
						$content .= "</table>";
					$content .= "</td>";
				$content .= "</tr>";
			$content .= "</table>";
			return $content;
		}
		public function print_claim_prescription() {
			$id = $_POST['id'];
			$header_data = $this->dbtable->get_row("
				SELECT *
				FROM " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . "
				WHERE id = '" . $id . "'
			");
			$nomor_resep = $header_data->nomor_resep;
			$resep_result = $this->dbtable->get_result("
				SELECT *
				FROM " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . "
				WHERE nomor_resep = '" . $nomor_resep . "' AND tipe = 'resep_luar' AND prop NOT LIKE 'del' AND dibatalkan = 0
				ORDER BY id ASC
			");
			$content = "<table border='0' width='100%' cellpadding='3' class='etiket_header'>";
				$content .= "<tr>";
					$content .= "<td>";
						$content .= "<div align='center'><table border='0' class='etiket_content' >";
							$content .= "<tr>";
								$content .= "<td colspan='5' align='center' style='font-size: 14px !important;'><b>DEPO FARMASI " . ucwords(getSettings($this->dbtable->get_db(), "smis_autonomous_title", "")) . " - RESEP (NON-KIUP)</b></td>";
							$content .= "</tr>";
						$content .= "</div></table>";
						$content .= "<div align='center'><table border='0' class='etiket_content' >";
							$content .= "<tr>";
								$content .= "<td colspan='1'>No. Penjualan</td>";
								$content .= "<td colspan='1'>: " .  ArrayAdapter::format("only-digit8", $resep_result[0]->id) . "</td>";
								$content .= "<td colspan='1' class='tpadding'>No. Resep</td>";
								$content .= "<td colspan='2'>: " .  $resep_result[0]->nomor_resep . "</td>";
							$content .= "</tr>";
							$content .= "<tr>";
								$content .= "<td colspan='1'>Tgl. Resep</td>"; 
								$content .= "<td colspan='1'>: " . ArrayAdapter::format("date d-m-Y H:i", $resep_result[0]->tanggal) . "</td>"; 
								$content .= "<td colspan='1' class='tpadding'>Nama Dokter</td>";
								$content .= "<td colspan='2'>: " . substr(strtoupper($resep_result[0]->nama_dokter), 0, 30) . "</td>";
							$content .= "</tr>";
							$content .= "<tr>";
								$content .= "<td colspan='1'>Nama Pasien</td>";
								$content .= "<td colspan='1'>: " . substr(strtoupper($resep_result[0]->nama_pasien), 0, 30) . "</td>";
								$content .= "<td colspan='1' class='tpadding'>Alamat Pasien</td>";
								$content .= "<td colspan='2'>: " . substr(strtoupper($resep_result[0]->alamat_pasien), 0, 30) . "</td>";
							$content .= "</tr>";
							$content .= "<tr>";
								$content .= "<td colspan='1'>Jns. Pasien</td>";
								$jenis_pasien = ArrayAdapter::format("unslug", $resep_result[0]->jenis) . " - ";
								if ($resep_result[0]->uri == 1) {
									$jenis_pasien .= "RAWAT INAP";
								} else {
									$jenis_pasien .= "RAWAT JALAN";
								}
								$content .= "<td colspan='4'>: " . $jenis_pasien . "</td>";
							$content .= "</tr>";
						$content .= "</table></div>";
					$content .= "</td>";
				$content .= "</tr>";
				$content .= "<tr>";
					$content .= "<td>";
						$content .= "<div align='center'><table border='1' class='etiket_content'>";
						$content .= "<tr align='center'>";
							$content .= "<td colspan='1'>No.</td>";
							$content .= "<td colspan='1'>Obat</td>";
							$content .= "<td colspan='1'>Jumlah</td>";
							// $content .= "<td colspan='1'>Harga Satuan</td>";
							// $content .= "<td colspan='1'>Jasa Pelyanan</td>";
							$content .= "<td colspan='1'>Subtotal</td>";
						$content .= "</tr>";
						$no_barang = 1;
						$total = 0;
						foreach($resep_result as $row_resep_result) {
							$obat_jadi_sql = "
								SELECT nama_obat AS 'nama', jumlah, satuan, harga, subtotal, embalase, tusla, 0 AS 'biaya_racik', 'obat_jadi' AS 'label'
								FROM " . InventoryLibrary::$_TBL_PENJUALAN_OBAT_JADI . "
								WHERE prop NOT LIKE 'del' AND id_penjualan_resep = '" . $row_resep_result->id . "'
							";
							$obat_jadi_data = $this->dbtable->get_result($obat_jadi_sql);
							foreach($obat_jadi_data as $ojd) {
								$content .= "<tr>";
									$content .= "<td colspan='1'>" . $no_barang++ . "</td>";
									$content .= "<td colspan='1'>" . $ojd->nama . "</td>";
									$content .= "<td colspan='1'>" . $ojd->jumlah . " " . $ojd->satuan . "</td>";
									$content .= "<td colspan='1' align='right' style='display: none;'>" . ArrayAdapter::format("money Rp.", $ojd->harga) . "</td>";
									$content .= "<td colspan='1' align='right' style='display: none;'>" . ArrayAdapter::format("money Rp.", $ojd->embalase + $ojd->tusla) . "</td>";
									$content .= "<td colspan='1' align='right'>" . ArrayAdapter::format("money Rp.", $ojd->subtotal) . "</td>";
								$content .= "</tr>";
								$total += $ojd->subtotal;
							}
							$obat_racik_sql = "
								SELECT id, nama AS 'nama', jumlah, 'Racikan' AS 'satuan', harga, embalase, tusla, biaya_racik, subtotal, 'obat_racikan' AS 'label'
								FROM " . InventoryLibrary::$_TBL_PENJUALAN_OBAT_RACIKAN . "
								WHERE prop NOT LIKE 'del' AND id_penjualan_resep = '" . $row_resep_result->id . "'
							";
							$obat_racik_data = $this->dbtable->get_result($obat_racik_sql);
							foreach($obat_racik_data as $ord) {
								$content .= "<tr>";
									$content .= "<td colspan='1'>" . $no_barang++ . "</td>";
									$content .= "<td colspan='1'>" . ArrayAdapter::format("unslug", $ord->nama) . "</td>";
									$content .= "<td colspan='1'>" . $ord->jumlah . " " . $ord->satuan . "</td>";
									$content .= "<td colspan='1' align='right' style='display: none;'>" . ArrayAdapter::format("money Rp.", $ord->harga) . "</td>";
									$content .= "<td colspan='1' align='right' style='display: none;'>" . ArrayAdapter::format("money Rp.", $ord->embalase + $ord->tusla + $ord->biaya_racik) . "</td>";
									$content .= "<td colspan='1' align='right'>" . ArrayAdapter::format("money Rp.", $ord->subtotal) . "</td>";
								$content .= "</tr>";
								$total += $ord->subtotal;
								//detail racikan:
								$bahan_racik_data = $this->dbtable->get_result("
									SELECT *
									FROM " . InventoryLibrary::$_TBL_BAHAN_PAKAI_OBAT_RACIKAN . "
									WHERE id_penjualan_obat_racikan = '" . $ord->id . "'
								");
								$bahan_no = 'a';
								foreach($bahan_racik_data as $brd) {
									$content .= "<tr style='font-size: 12px !important;'>";
										$content .= "<td colspan='1'></td>";
										$content .= "<td colspan='1'>" . $bahan_no++ . ". " . $brd->nama_obat . "</td>";
										$content .= "<td colspan='1'>" . $brd->jumlah . " " . $brd->satuan . "</td>";
										$content .= "<td colspan='3' align='right'></td>";
									$content .= "</tr>";
								}
							}
						}
						$content .= "<tr>";
							$content .= "<td colspan='3' align='right'>Total</td>";
							$content .= "<td colspan='1' align='right'>" . ArrayAdapter::format("money Rp.", $total) . "</td>";
						$content .= "</tr>";
						$content .= "<tr>";
						$v_diskon = 0;
						if ($resep_result[0]->diskon > 0) {
							if ($resep_result[0]->t_diskon == "persen" || $resep_result[0]->t_diskon == "gratis") {
								$v_diskon = ($total * $resep_result[0]->diskon) / 100;
								$content .= "<td colspan='3' align='right'>Diskon (" . $resep_result[0]->diskon . " %)</td>";
								$content .= "<td colspan='1' align='right'>" . ArrayAdapter::format("money Rp.", $v_diskon) . "</td>";
							} else {
								$v_diskon = $resep_result[0]->diskon;
								$content .= "<td colspan='3' align='right'>Diskon</td>";
								$content .= "<td colspan='1' align='right'>" . ArrayAdapter::format("money Rp.", $v_diskon) . "</td>";
							}
						}
						$total_min_diskon = $total - $v_diskon;
						$content .= "</tr>";
						$content .= "<tr>";
							$content .= "<td colspan='3' align='right'>Tagihan</td>";
							$content .= "<td colspan='1'>" . ArrayAdapter::format("money Rp.", $total) . "</td>";
						$content .= "</tr>";
						loadLibrary("smis-libs-function-math");
						$content .= "<tr>";
							$content .= "<td colspan='1' align='right'>Terbilang</td>";
							$content .= "<td colspan='9'>" . numbertell($total) . " Rupiah </td>";
						$content .= "</tr>";
						$content .= "</table></div>";
					$content .= "</td>";
				$content .= "<tr>";
					$content .= "<td colspan='4'>";
						$content .= "<table border='0' align='center' class='etiket_content'>";
							$content .= "<tr>";
								$content .= "<td align='center'>PENERIMA,</td>";
								$content .= "<td>&Tab;</td>";
								$content .= "<td align='center'>PETUGAS DEPO FARMASI,</td>";
							$content .= "</tr>";
							$content .= "<tr>";
								$content .= "<td></td>";
								$content .= "<td>&Tab;</td>";
								$content .= "<td></td>";
							$content .= "</tr>";
							$content .= "<tr>";
								$content .= "<td></td>";
								$content .= "<td>&Tab;</td>";
								$content .= "<td></td>";
							$content .= "</tr>";
							$content .= "<tr>";
								$content .= "<td align='center'>(_____________________)</td>";
								$content .= "<td>&Tab;</td>";
								$content .= "<td align='center'>(_____________________)</td>";
							$content .= "</tr>";
							$content .= "<tr>";
								$content .= "<td></td>";
							$content .= "</tr>";
						$content .= "</table>";
					$content .= "</td>";
				$content .= "</tr>";
			$content .= "</table>";
			return $content;
		}
		public function batalDetailResep($id){
			$header = $this->dbtable->select($id);
			$stok_obat_dbtable = new DBTable($this->dbtable->get_db(), InventoryLibrary::$_TBL_STOK_OBAT);
			$stok_pakai_obat_jadi_rows = $this->dbtable->get_result("
				SELECT " . InventoryLibrary::$_TBL_STOK_PAKAI_OBAT_JADI . ".*
				FROM " . InventoryLibrary::$_TBL_STOK_PAKAI_OBAT_JADI . " LEFT JOIN " . InventoryLibrary::$_TBL_PENJUALAN_OBAT_JADI . " ON " . InventoryLibrary::$_TBL_STOK_PAKAI_OBAT_JADI . ".id_penjualan_obat_jadi = " . InventoryLibrary::$_TBL_PENJUALAN_OBAT_JADI . ".id
				WHERE " . InventoryLibrary::$_TBL_STOK_PAKAI_OBAT_JADI . ".prop NOT LIKE 'del' AND " . InventoryLibrary::$_TBL_PENJUALAN_OBAT_JADI . ".id_penjualan_resep = '" . $id['id'] . "'
			");
			$keterangan = "Nomor: " . ArrayAdapter::format("only-digit8", $id['id']) . "; Jenis: Obat Jadi; Pasien: " . $header->nama_pasien . "; Alamat: " . $header->alamat_pasien . "; No. Resep : " . $header->nomor_resep;
			foreach($stok_pakai_obat_jadi_rows as $spojr) {
				$stok_obat_row = $stok_obat_dbtable->get_row("
					SELECT *
					FROM " . InventoryLibrary::$_TBL_STOK_OBAT . "
					WHERE id = '" . $spojr->id_stok_obat . "'
				");
				$stok_obat_data = array();
				$stok_obat_data['sisa'] = $stok_obat_row->sisa + $spojr->jumlah;
				$stok_obat_id['id'] = $stok_obat_row->id;
				$result=$stok_obat_dbtable->update($stok_obat_data, $stok_obat_id);
				if($result<1){
					$this->addMessage(" Obat <strong>".$stok_obat_row->nama_obat." </strong> Gagal Dibatalkan");
					return false;
				}
				
				//logging riwayat stok:
				$riwayat_dbtable = new DBTable($this->dbtable->get_db(), InventoryLibrary::$_TBL_RIWAYAT_STOK_OBAT);
				$data_riwayat = array();
				$data_riwayat['tanggal'] = date("Y-m-d");
				$data_riwayat['id_stok_obat'] = $stok_obat_row->id;
				$data_riwayat['jumlah_masuk'] = $spojr->jumlah;
				$data_riwayat['sisa'] = $stok_obat_row->sisa + $spojr->jumlah;
				$data_riwayat['keterangan'] = "Stok Batal Digunakan Penjualan Resep: " . $keterangan;
				global $user;
				$data_riwayat['nama_user'] = $user->getName();
				$result=$riwayat_dbtable->insert($data_riwayat);
				if($result===false){
					$this->addMessage(" Obat <strong>".$stok_obat_row->nama_obat." </strong> Gagal Dicatat di Riwayat");
					return false;
				}
			}

			$obat_jadi_rows = $this->dbtable->get_result("
				SELECT id_obat, kode_obat, nama_obat, nama_jenis_obat, SUM(jumlah) AS 'jumlah', satuan
				FROM " . InventoryLibrary::$_TBL_PENJUALAN_OBAT_JADI . "
				WHERE prop NOT LIKE 'del' AND id_penjualan_resep = '" . $id['id'] . "'
				GROUP BY id_obat, satuan
			");
			//logging kartu stok:
			$kartu_stok_dbtable = new DBTable($this->dbtable->get_db(), InventoryLibrary::$_TBL_KARTU_STOK_OBAT);
			foreach ($obat_jadi_rows as $ojr) {
				$sisa_row = $this->dbtable->get_row("
					SELECT SUM(a.sisa) AS 'sisa'
					FROM " . InventoryLibrary::$_TBL_STOK_OBAT . " a LEFT JOIN " . InventoryLibrary::$_TBL_OBAT_MASUK . " b ON a.id_obat_masuk = b.id
					WHERE a.prop NOT LIKE 'del' AND b.prop NOT LIKE 'del' AND  AND a.id_obat = '" . $ojr->id_obat . "' AND a.satuan = '" . $ojr->satuan . "' AND a.konversi = '1'
				");
				$kartu_stok_data = array(
					"f_id"				=> $id['id'],
					"no_bon"			=> $id['id'],
					"unit"				=> "Penjualan Resep Luar : Pembatalan, " . $keterangan,
					"id_obat"			=> $ojr->id_obat,
					"kode_obat"			=> $ojr->kode_obat,
					"nama_obat"			=> $ojr->nama_obat,
					"nama_jenis_obat"	=> $ojr->nama_jenis_obat,
					"tanggal"			=> date("Y-m-d"),
					"masuk"				=> $ojr->jumlah,
					"keluar"			=> 0,
					"sisa"				=> $sisa_row->sisa
				);
				$result = $kartu_stok_dbtable->insert($kartu_stok_data);
				if($result===false){
					$this->addMessage(" Obat <strong>".$ojr->nama_obat." </strong> Gagal Dicatat di Kartu Stok");
					return false;
				}
			}
			
			$stok_pakai_bahan_rows = $this->dbtable->get_result("
				SELECT " . InventoryLibrary::$_TBL_STOK_PAKAI_BAHAN . ".*
				FROM (" . InventoryLibrary::$_TBL_STOK_PAKAI_BAHAN . " LEFT JOIN " . InventoryLibrary::$_TBL_BAHAN_PAKAI_OBAT_RACIKAN . " ON " . InventoryLibrary::$_TBL_STOK_PAKAI_BAHAN . ".id_bahan = " . InventoryLibrary::$_TBL_BAHAN_PAKAI_OBAT_RACIKAN . ".id) LEFT JOIN " . InventoryLibrary::$_TBL_PENJUALAN_OBAT_RACIKAN . " ON " . InventoryLibrary::$_TBL_BAHAN_PAKAI_OBAT_RACIKAN . ".id_penjualan_obat_racikan = " . InventoryLibrary::$_TBL_PENJUALAN_OBAT_RACIKAN . ".id
				WHERE " . InventoryLibrary::$_TBL_STOK_PAKAI_BAHAN . ".prop NOT LIKE 'del' AND " . InventoryLibrary::$_TBL_PENJUALAN_OBAT_RACIKAN . ".id_penjualan_resep = '" . $id['id'] . "'
			");
			$keterangan = "Nomor: " . ArrayAdapter::format("only-digit8", $id['id']) . "; Jenis: Racikan; Pasien: " . $header->nama_pasien . "; Alamat: " . $header->alamat_pasien . ";";
			foreach($stok_pakai_bahan_rows as $spbr) {
				$stok_obat_row = $stok_obat_dbtable->get_row("
					SELECT *
					FROM " . InventoryLibrary::$_TBL_STOK_OBAT . "
					WHERE id = '" . $spbr->id_stok_obat . "'
				");
				$stok_obat_data = array();
				$stok_obat_data['sisa'] = $stok_obat_row->sisa + $spbr->jumlah;
				$stok_obat_id['id'] = $stok_obat_row->id;
				$result=$stok_obat_dbtable->update($stok_obat_data, $stok_obat_id);
				if($result<1){
					$this->addMessage("Bahan <strong>".$stok_obat_row->nama_obat." </strong> Gagal Dibatalkan");
					return false;
				}
				//logging riwayat stok:
				$riwayat_dbtable = new DBTable($this->dbtable->get_db(), InventoryLibrary::$_TBL_RIWAYAT_STOK_OBAT);
				$data_riwayat = array();
				$data_riwayat['tanggal'] = date("Y-m-d");
				$data_riwayat['id_stok_obat'] = $stok_obat_row->id;
				$data_riwayat['jumlah_masuk'] = $spbr->jumlah;
				$data_riwayat['sisa'] = $stok_obat_row->sisa + $spbr->jumlah;
				$data_riwayat['keterangan'] = "Stok Batal Digunakan Penjualan Resep: " . $keterangan;
				global $user;
				$data_riwayat['nama_user'] = $user->getName();
				$result=$riwayat_dbtable->insert($data_riwayat);
				if($result===false){
					$this->addMessage("Bahan <strong>".$stok_obat_row->nama_obat." </strong> Gagal Gagal Dicatat di Riwayat");
					return false;
				}
			}

			$bahan_rows = $this->dbtable->get_result("
				SELECT a.id_obat, a.kode_obat, a.nama_obat, a.nama_jenis_obat, SUM(a.jumlah) AS 'jumlah', a.satuan
				FROM " . InventoryLibrary::$_TBL_BAHAN_PAKAI_OBAT_RACIKAN . " a LEFT JOIN " . InventoryLibrary::$_TBL_PENJUALAN_OBAT_RACIKAN . " b ON a.id_penjualan_obat_racikan = b.id
				WHERE a.prop NOT LIKE 'del' AND b.prop NOT LIKE 'del' AND b.id_penjualan_resep = '" . $id['id'] . "'
				GROUP BY a.id_obat, a.satuan
			");
			//logging kartu stok:
			$kartu_stok_dbtable = new DBTable($this->dbtable->get_db(), InventoryLibrary::$_TBL_KARTU_STOK_OBAT);
			foreach ($bahan_rows as $br) {
				$sisa_row = $this->dbtable->get_row("
					SELECT SUM(a.sisa) AS 'sisa'
					FROM " . InventoryLibrary::$_TBL_STOK_OBAT . " a LEFT JOIN " . InventoryLibrary::$_TBL_OBAT_MASUK . " b ON a.id_obat_masuk = b.id
					WHERE a.prop NOT LIKE 'del' AND b.prop NOT LIKE 'del' AND b.status = 'sudah' AND a.id_obat = '" . $br->id_obat . "' AND a.satuan = '" . $br->satuan . "' AND a.konversi = '1'
				");
				$kartu_stok_data = array(
					"f_id"				=> $id['id'],
					"no_bon"			=> $id['id'],
					"unit"				=> "Penjualan Resep Luar : Pembatalan, " . $keterangan,
					"id_obat"			=> $br->id_obat,
					"kode_obat"			=> $br->kode_obat,
					"nama_obat"			=> $br->nama_obat,
					"nama_jenis_obat"	=> $br->nama_jenis_obat,
					"tanggal"			=> date("Y-m-d"),
					"masuk"				=> $br->jumlah,
					"keluar"			=> 0,
					"sisa"				=> $sisa_row->sisa
				);
				$result = $kartu_stok_dbtable->insert($kartu_stok_data);
				if($result===false){
					$this->addMessage(" Obat <strong>".$br->nama_obat." </strong> Gagal Dicatat di Kartu Stok");
					return false;
				}
			}
			return true;
		}
		public function saveDetailResep($id){
			$success=true;
			$header = $this->dbtable->select($id);
			//do insert detail resep here:
			$penjualan_obat_jadi_dbtable = new DBTable($this->dbtable->get_db(), InventoryLibrary::$_TBL_PENJUALAN_OBAT_JADI);
			$penjualan_obat_racikan_dbtable = new DBTable($this->dbtable->get_db(), InventoryLibrary::$_TBL_PENJUALAN_OBAT_RACIKAN);
			$bahan_pakai_obat_racikan_dbtable = new DBTable($this->dbtable->get_db(), InventoryLibrary::$_TBL_BAHAN_PAKAI_OBAT_RACIKAN);
			$stok_obat_dbtable = new DBTable($this->dbtable->get_db(), InventoryLibrary::$_TBL_STOK_OBAT);
			$stok_pakai_obat_jadi_dbtable = new DBTable($this->dbtable->get_db(), InventoryLibrary::$_TBL_STOK_PAKAI_OBAT_JADI);
			$stok_pakai_bahan_dbtable = new DBTable($this->dbtable->get_db(), InventoryLibrary::$_TBL_STOK_PAKAI_BAHAN);
			$detail_resep = $_POST['detail_resep'];
			foreach($detail_resep as $dresep) {
				if ($dresep['tipe'] == "obat_jadi") {
					//do insert obat jadi here:
					$obat_jadi_data = array();
					$obat_jadi_data['id_penjualan_resep'] = $id['id'];
					$obat_jadi_data['id_obat'] = $dresep['id_obat'];
					$obat_jadi_data['kode_obat'] = $dresep['kode_obat'];
					$obat_jadi_data['nama_obat'] = $dresep['nama_obat'];
					$obat_jadi_data['nama_jenis_obat'] = $dresep['nama_jenis_obat'];
					$obat_jadi_data['jumlah'] = $dresep['jumlah'];
					$obat_jadi_data['satuan'] = $dresep['satuan'];
					$obat_jadi_data['konversi'] = $dresep['konversi'];
					$obat_jadi_data['satuan_konversi'] = $dresep['satuan_konversi'];
					$obat_jadi_data['harga'] = $dresep['harga'];
					$obat_jadi_data['embalase'] = $dresep['embalase'];
					$obat_jadi_data['tusla'] = $dresep['tusla'];
					$obat_jadi_data['subtotal'] = $dresep['subtotal'];
					$obat_jadi_data['aturan_pakai'] = $dresep['aturan_pakai'];
					if ($dresep['tanggal_exp'] != "") {
						$obat_jadi_data['tanggal_exp'] = date_format(date_create($dresep['tanggal_exp']), 'Y-m-d');
					}
					$obat_jadi_data['obat_luar'] = $dresep['obat_luar'];
					$obat_jadi_data['jumlah_pakai_sehari'] = $dresep['jumlah_pakai_sehari'];
					$obat_jadi_data['satuan_pakai'] = $dresep['satuan_pakai'];
					$obat_jadi_data['takaran_pakai'] = $dresep['takaran_pakai'];
					$obat_jadi_data['pemakaian'] = $dresep['pemakaian'];
					$obat_jadi_data['pemakaian_obat_luar'] = $dresep['pemakaian_obat_luar'];
					$obat_jadi_data['penggunaan'] = $dresep['penggunaan'];
					$obat_jadi_data['detail_penggunaan'] = $dresep['detail_penggunaan'];
					$obat_jadi_data['pos'] = $dresep['pos'];
					$obat_jadi_data['markup'] = $dresep['markup'];
					$result=$penjualan_obat_jadi_dbtable->insert($obat_jadi_data);
					
					if($result===false){
						$success=false;
						$this->addMessage(" Obat <strong>".$dresep['nama_obat']." </strong> Gagal Masuk Resep");
						continue;
					}
					
					$id_penjualan_obat_jadi = $penjualan_obat_jadi_dbtable->get_inserted_id();
					//do insert stok pakai obat jadi and update stok obat here:
					$stok_obat_query = "
						SELECT a.id, a.sisa
						FROM " . InventoryLibrary::$_TBL_STOK_OBAT . " a LEFT JOIN " . InventoryLibrary::$_TBL_OBAT_MASUK . " b ON a.id_obat_masuk = b.id
						WHERE a.id_obat = '" . $dresep['id_obat'] . "' 
						  AND a.satuan = '" . $dresep['satuan'] . "' 
						  AND a.konversi = '" . $dresep['konversi'] . "' 
						  AND a.satuan_konversi = '" . $dresep['satuan_konversi'] . "'
						  AND a.sisa > 0
						  AND a.prop NOT LIKE 'del'
						  AND b.prop NOT LIKE 'del'
						  AND b.status = 'sudah'
						ORDER BY a.tanggal_exp ASC
					";
					$stok_obat_rows = $stok_obat_dbtable->get_result($stok_obat_query);
					$jumlah = $dresep['jumlah'] - $dresep['jumlah_lama'];//kalau-kalau update
					foreach($stok_obat_rows as $sor) {
						$sisa_stok = 0;
						$jumlah_stok_pakai = 0;
						if ($sor->sisa >= $jumlah) {
							$sisa_stok = $sor->sisa - $jumlah;
							$jumlah_stok_pakai = $jumlah;
							$jumlah = 0;
						} else {
							$sisa_stok = 0;
							$jumlah_stok_pakai = $sor->sisa;
							$jumlah = $jumlah - $sor->sisa;
						}
						//do update stok obat here:
						$stok_obat_data = array();
						$stok_obat_data['sisa'] = $sisa_stok;
						$stok_obat_id['id'] = $sor->id;
						$keberhasilan_mengambil_stok=$stok_obat_dbtable->update($stok_obat_data, $stok_obat_id);
						if($keberhasilan_mengambil_stok==0){
							$success=false;
							$this->addMessage(" Stok <strong>".$dresep['nama_obat']." </strong> Gagal diambil ");
							continue;
						}
						
						//logging riwayat stok:
						$riwayat_dbtable = new DBTable($this->dbtable->get_db(), InventoryLibrary::$_TBL_RIWAYAT_STOK_OBAT);
						$data_riwayat = array();
						$data_riwayat['tanggal'] = date("Y-m-d");
						$data_riwayat['id_stok_obat'] = $sor->id;
						$data_riwayat['jumlah_keluar'] = $jumlah_stok_pakai;
						$data_riwayat['sisa'] = $sisa_stok;
						$keterangan = "Nomor: " . ArrayAdapter::format("only-digit8", $id['id']) . "; Jenis: Obat Jadi; Pasien: " . $_POST['nama_pasien'] . "; Alamat: " . $_POST['alamat_pasien'] . "; No. Resep : " . $header->nomor_resep;
						$data_riwayat['keterangan'] = "Stok Digunakan Penjualan Resep Luar: " . $keterangan;
						global $user;
						$data_riwayat['nama_user'] = $user->getName();
						$result=$riwayat_dbtable->insert($data_riwayat);
						if($result===false){
							$success=false;
							$this->addMessage(" Riwayat <strong>".$dresep['nama_obat']." </strong> Gagal dicatat");
							continue;
						}
						
						//do insert stok pakai obat jadi here:
						$stok_pakai_obat_jadi_data = array();
						$stok_pakai_obat_jadi_data['id_penjualan_obat_jadi'] = $id_penjualan_obat_jadi;
						$stok_pakai_obat_jadi_data['id_stok_obat'] = $sor->id;
						$stok_pakai_obat_jadi_data['jumlah'] = $jumlah_stok_pakai;
						$result=$stok_pakai_obat_jadi_dbtable->insert($stok_pakai_obat_jadi_data);
						if($result===false){
							$success=false;
							$this->addMessage(" Obat <strong>".$dresep['nama_obat']." </strong> Gagal Disimpan di Riwayat Pemakaian Obat Jadi","Pakai Obat Jadi");
							continue;
						}
						if ($jumlah == 0) break;
					}
					if($jumlah>0) {
						$success=false;
						$this->addMessage(" Stok untuk Obat Jadi pada <strong>".$dresep['nama_obat']." </strong> Kurang ".$jumlah." ".$dresep['satuan']);
					}
				} else if ($dresep['tipe'] == "obat_racikan") {
					//do insert obat racikan header here:
					$obat_racikan_data = array();
					$obat_racikan_data['id_penjualan_resep'] = $id['id'];
					$obat_racikan_data['nama'] = $dresep['nama'];
					$obat_racikan_data['jumlah'] = $dresep['jumlah'];
					$obat_racikan_data['harga'] = $dresep['harga'];
					$obat_racikan_data['embalase'] = $dresep['total_embalase'];
					$obat_racikan_data['tusla'] = $dresep['total_tusla'];
					$obat_racikan_data['biaya_racik'] = $dresep['biaya_racik'];
					$obat_racikan_data['subtotal'] = $dresep['subtotal'];
					$obat_racikan_data['aturan_pakai'] = $dresep['aturan_pakai'];
					if ($dresep['tanggal_exp'] != "") {
						$obat_racikan_data['tanggal_exp'] = date_format(date_create($dresep['tanggal_exp']), 'Y-m-d');
					}
					$obat_racikan_data['obat_luar'] = $dresep['obat_luar'];
					$obat_racikan_data['jumlah_pakai_sehari'] = $dresep['jumlah_pakai_sehari'];
					$obat_racikan_data['satuan_pakai'] = $dresep['satuan_pakai'];
					$obat_racikan_data['takaran_pakai'] = $dresep['takaran_pakai'];
					$obat_racikan_data['pemakaian'] = $dresep['pemakaian'];
					$obat_racikan_data['pemakaian_obat_luar'] = $dresep['pemakaian_obat_luar'];
					$obat_racikan_data['penggunaan'] = $dresep['penggunaan'];
					$obat_racikan_data['detail_penggunaan'] = $dresep['detail_penggunaan'];
					$obat_racikan_data['id_apoteker'] = $dresep['id_apoteker'];
					$obat_racikan_data['nama_apoteker'] = $dresep['nama_apoteker'];
					$obat_racikan_data['pos'] = $dresep['pos'];
					$result=$penjualan_obat_racikan_dbtable->insert($obat_racikan_data);
					if($result===false){
						$success=false;
						$this->addMessage(" Racikan  <strong>". $dresep['nama']." </strong> Gagal Masuk Resep");
						continue;
					}
					$id_penjualan_obat_racikan = $penjualan_obat_racikan_dbtable->get_inserted_id();
					if (isset($dresep['detail_racikan'])) {
						//do insert detail racikan here:
						$detail_racikan = $dresep['detail_racikan'];
						foreach($detail_racikan as $dracikan) {
							$bahan_data = array();
							$bahan_data['id_penjualan_obat_racikan'] = $id_penjualan_obat_racikan;
							$bahan_data['id_obat'] = $dracikan['id_obat'];
							$bahan_data['kode_obat'] = $dracikan['kode_obat'];
							$bahan_data['nama_obat'] = $dracikan['nama_obat'];
							$bahan_data['nama_jenis_obat'] = $dracikan['nama_jenis_obat'];
							$bahan_data['jumlah'] = $dracikan['jumlah'];
							$bahan_data['satuan'] = $dracikan['satuan'];
							$bahan_data['konversi'] = $dracikan['konversi'];
							$bahan_data['satuan_konversi'] = $dracikan['satuan_konversi'];
							$bahan_data['harga'] = $dracikan['harga'];
							$bahan_data['embalase'] = $dracikan['embalase'];
							$bahan_data['tusla'] = $dracikan['tusla'];
							$bahan_data['markup'] = $dracikan['markup'];
							$result=$bahan_pakai_obat_racikan_dbtable->insert($bahan_data);
							if($result===false){
								$success=false;
								$this->addMessage(" Racikan ".$dresep['nama']." pada  <strong>". $dracikan['nama_obat']." </strong> Gagal Masuk Resep");
								continue;
							}
							$id_bahan = $bahan_pakai_obat_racikan_dbtable->get_inserted_id();
							//do insert stok pakai bahan and update stok obat here:
							$stok_obat_query = "
								SELECT a.id, a.sisa
								FROM " . InventoryLibrary::$_TBL_STOK_OBAT . " a LEFT JOIN " . InventoryLibrary::$_TBL_OBAT_MASUK . " b ON a.id_obat_masuk = b.id
								WHERE a.id_obat = '" . $dracikan['id_obat'] . "' 
								  AND a.satuan = '" . $dracikan['satuan'] . "' 
								  AND a.konversi = '" . $dracikan['konversi'] . "' 
								  AND a.satuan_konversi = '" . $dracikan['satuan_konversi'] . "'
								  AND a.sisa > 0
								  AND a.prop NOT LIKE 'del'
								  AND b.prop NOT LIKE 'del'
								  AND b.status = 'sudah'
								ORDER BY a.tanggal_exp ASC
							";
							$stok_obat_rows = $stok_obat_dbtable->get_result($stok_obat_query);
							$jumlah = $dracikan['jumlah'];
							foreach($stok_obat_rows as $sor) {
								$sisa_stok = 0;
								$jumlah_stok_pakai = 0;
								if ($sor->sisa >= $jumlah) {
									$sisa_stok = $sor->sisa - $jumlah;
									$jumlah_stok_pakai = $jumlah;
									$jumlah = 0;
								} else {
									$sisa_stok = 0;
									$jumlah_stok_pakai = $sor->sisa;
									$jumlah = $jumlah - $sor->sisa;
								}
								//do update stok obat here:
								$stok_obat_data = array();
								$stok_obat_data['sisa'] = $sisa_stok;
								$stok_obat_id['id'] = $sor->id;
								$keberhasilan_mengambil_stok=$stok_obat_dbtable->update($stok_obat_data, $stok_obat_id);
								if($keberhasilan_mengambil_stok==0){
									$success=false;
									$this->addMessage(" Stok Racikan ".$dresep['nama']." pada <strong>".$dracikan['nama_obat']." </strong> Gagal diambil ");
									continue;
								}
								//logging riwayat stok:
								$riwayat_dbtable = new DBTable($this->dbtable->get_db(), InventoryLibrary::$_TBL_RIWAYAT_STOK_OBAT);
								$data_riwayat = array();
								$data_riwayat['tanggal'] = date("Y-m-d");
								$data_riwayat['id_stok_obat'] = $sor->id;
								$data_riwayat['jumlah_keluar'] = $jumlah_stok_pakai;
								$data_riwayat['sisa'] = $sisa_stok;
								$keterangan = "Nomor: " . ArrayAdapter::format("only-digit8", $id['id']) . "; Jenis: Obat Racikan; Pasien: " . $_POST['nama_pasien'] . "; Alamat: " . $_POST['alamat_pasien'] . "; No. Resep : " . $header->nomor_resep;
								$data_riwayat['keterangan'] = "Stok Digunakan Penjualan Resep Luar: " . $keterangan;
								global $user;
								$data_riwayat['nama_user'] = $user->getName();
								$result=$riwayat_dbtable->insert($data_riwayat);
								if($result===false){
									$success=false;
									$this->addMessage(" Riwayat Racikan ".$dresep['nama']." <strong>".$dracikan['nama_obat']." </strong> Gagal dicatat ");
									continue;
								}	
								//do insert stok pakai bahan here:
								$stok_pakai_bahan_data = array();
								$stok_pakai_bahan_data['id_bahan'] = $id_bahan;
								$stok_pakai_bahan_data['id_stok_obat'] = $sor->id;
								$stok_pakai_bahan_data['jumlah'] = $jumlah_stok_pakai;
								$stok_pakai_bahan_dbtable->insert($stok_pakai_bahan_data);
								if($result===false){
									$success=false;
									$this->addMessage(" Racikan ".$dresep['nama']." pada Bahan <strong>".$dracikan['nama_obat']." </strong> Gagal Disimpan di Riwayat Pemakaian Bahan","Pakai Bahan");
									continue;
								}
								if ($jumlah == 0) break;
							}
							if($jumlah > 0) {
								$success=false;
								$this->addMessage(" Stok untuk Racikan  ".$dresep['nama']."  pada <strong>".$dracikan['nama_obat']." </strong> Tidak Cukup Kurang ".$jumlah." ".$dracikan['satuan']);
							}
						}
					}
				}
			}

			$obat_jadi_rows = $this->dbtable->get_result("
				SELECT id_obat, kode_obat, nama_obat, nama_jenis_obat, SUM(jumlah) AS 'jumlah', satuan
				FROM " . InventoryLibrary::$_TBL_PENJUALAN_OBAT_JADI . "
				WHERE prop NOT LIKE 'del' AND id_penjualan_resep = '" . $id['id'] . "'
				GROUP BY id_obat, satuan
			");
			//logging kartu stok:
			$kartu_stok_dbtable = new DBTable($this->dbtable->get_db(), InventoryLibrary::$_TBL_KARTU_STOK_OBAT);
			$keterangan = "Nomor: " . ArrayAdapter::format("only-digit8", $id['id']) . "; Jenis: Obat Jadi; Pasien: " . $_POST['nama_pasien'] . "; Alamat: " . $_POST['alamat_pasien'] . "; No. Resep : " . $header->nomor_resep;
			foreach ($obat_jadi_rows as $ojr) {
				$sisa_row = $this->dbtable->get_row("
					SELECT SUM(a.sisa) AS 'sisa'
					FROM " . InventoryLibrary::$_TBL_STOK_OBAT . " a LEFT JOIN " . InventoryLibrary::$_TBL_OBAT_MASUK . " b ON a.id_obat_masuk = b.id
					WHERE a.prop NOT LIKE 'del' AND a.id_obat = '" . $ojr->id_obat . "' AND a.satuan = '" . $ojr->satuan . "' AND a.konversi = '1'
				");
				$kartu_stok_data = array(
					"f_id"				=> $id['id'],
					"no_bon"			=> $id['id'],
					"unit"				=> "Penjualan Resep Luar : " . $keterangan,
					"id_obat"			=> $ojr->id_obat,
					"kode_obat"			=> $ojr->kode_obat,
					"nama_obat"			=> $ojr->nama_obat,
					"nama_jenis_obat"	=> $ojr->nama_jenis_obat,
					"tanggal"			=> date("Y-m-d"),
					"masuk"				=> 0,
					"keluar"			=> $ojr->jumlah,
					"sisa"				=> $sisa_row->sisa
				);
				$result = $kartu_stok_dbtable->insert($kartu_stok_data);
				if($result===false){
					$this->addMessage(" Obat <strong>".$ojr->nama_obat." </strong> Gagal Dicatat di Kartu Stok");
					return false;
				}
			}

			$bahan_rows = $this->dbtable->get_result("
				SELECT a.id_obat, a.kode_obat, a.nama_obat, a.nama_jenis_obat, SUM(a.jumlah) AS 'jumlah', a.satuan
				FROM " . InventoryLibrary::$_TBL_BAHAN_PAKAI_OBAT_RACIKAN . " a LEFT JOIN " . InventoryLibrary::$_TBL_PENJUALAN_OBAT_RACIKAN . " b ON a.id_penjualan_obat_racikan = b.id
				WHERE a.prop NOT LIKE 'del' AND b.prop NOT LIKE 'del' AND b.id_penjualan_resep = '" . $id['id'] . "'
				GROUP BY a.id_obat, a.satuan
			");
			//logging kartu stok:
			$kartu_stok_dbtable = new DBTable($this->dbtable->get_db(), InventoryLibrary::$_TBL_KARTU_STOK_OBAT);
			$keterangan = "Nomor: " . ArrayAdapter::format("only-digit8", $id['id']) . "; Jenis: Obat Racikan; Pasien: " . $_POST['nama_pasien'] . "; Alamat: " . $_POST['alamat_pasien'] . "; No. Resep : " . $header->nomor_resep;
			foreach ($bahan_rows as $br) {
				$sisa_row = $this->dbtable->get_row("
					SELECT SUM(a.sisa) AS 'sisa'
					FROM " . InventoryLibrary::$_TBL_STOK_OBAT . " a LEFT JOIN " . InventoryLibrary::$_TBL_OBAT_MASUK . " b ON a.id_obat_masuk = b.id
					WHERE a.prop NOT LIKE 'del' AND a.id_obat = '" . $br->id_obat . "' AND a.satuan = '" . $br->satuan . "' AND a.konversi = '1' 
				");
				$kartu_stok_data = array(
					"f_id"				=> $id['id'],
					"no_bon"			=> $id['id'],
					"unit"				=> "Penjualan Resep Luar : " . $keterangan,
					"id_obat"			=> $br->id_obat,
					"kode_obat"			=> $br->kode_obat,
					"nama_obat"			=> $br->nama_obat,
					"nama_jenis_obat"	=> $br->nama_jenis_obat,
					"tanggal"			=> date("Y-m-d"),
					"masuk"				=> 0,
					"keluar"			=> $br->jumlah,
					"sisa"				=> $sisa_row->sisa
				);
				$result = $kartu_stok_dbtable->insert($kartu_stok_data);
				if($result===false){
					$this->addMessage(" Obat <strong>".$br->nama_obat." </strong> Gagal Dicatat di Kartu Stok");
					return false;
				}
			}

			return $success;
		}
		public function save() {
			$header_data = $this->postToArray();
			$id['id'] = $_POST['id'];
			$berhasil=false;
			$db=$this->dbtable->get_db();
			$db->set_autocommit(false);
			$db->begin_transaction();
			if ($id['id'] == 0 || $id['id'] == "") {
				//do insert header here:
				$header_data['tipe'] = "resep_luar";
				$header_data['finish_time'] = date("Y-m-d H:i:s");
				$tahun = date("Y");
				if (!isset($_POST['nomor_resep'])) {
					$nomor = getSettings($this->dbtable->get_db(), "R-" . $tahun, "0") + 1;
					setSettings($this->dbtable->get_db(), "R-" . $tahun, $nomor);
					$header_data['nomor_resep'] = "R-" . $tahun . "." . $nomor;
				}
				$header_data['tanggal'] = date("Y-m-d H:i:s");

				$tanggal_resep_sebelum = date("Y-m-d H:i:s", strtotime($header_data['tanggal']) - getSettings($this->dbtable->get_db(), "depo_farmasi7-penjualan-interval_transaksi", 15));
				$tanggal_resep_sesudah = date("Y-m-d H:i:s", strtotime($header_data['tanggal']) + getSettings($this->dbtable->get_db(), "depo_farmasi7-penjualan-interval_transaksi", 15));
				$row = $this->dbtable->get_row("
					SELECT COUNT(*) AS 'jumlah'
					FROM " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . "
					WHERE tanggal >= '" . $tanggal_resep_sebelum . "' AND tanggal <= '" . $tanggal_resep_sesudah . "' AND dibatalkan = '0' AND tipe = 'resep_luar'
				");
				$result = true;
				$berhasil = true;
				if ($row != null) {
					if ($row->jumlah > 0) {
						$result = false;
						$berhasil = false;
						$this->addMessage(" Penjualan Resep Gagal Disimpan : Tidak dapat Menyimpan Transaksi dalam Sela Waktu " . getSettings($this->dbtable->get_db(), "depo_farmasi7-penjualan-interval_transaksi", 15) . " detik");
					}
				}
				if ($result && $berhasil) {
					$result = $this->dbtable->insert($header_data);	
					if ($result===false) {
						$berhasil = false;
						$this->addMessage(" Penjualan Resep Gagal Disimpan");
					} else {
						$id['id'] = $this->dbtable->get_inserted_id();
						$success['type'] = "insert";
						if (isset($_POST['detail_resep'])) {
							$berhasil=$this->saveDetailResep($id);
						}
					}
				}
			} else {
				//do update header here:
				if (isset($_POST['edukasi'])) {
					if ($_POST['edukasi'] == 1)
						$header_data['educate_time'] = date("Y-m-d H:i:s");
					else
						$header_data['educate_time'] = "0000-00-00 00:00:00";
				}
				$result = $this->dbtable->update($header_data, $id);
				if ($result===false){
					$berhasil=false;
					$this->addMessage(" Pembatalan Resep Gagal");
				} else {
					$success['type'] = "update";
					$berhasil=true;
				}
				if (isset($_POST['dibatalkan']) && $_POST['dibatalkan'] == 1) {
					$berhasil=$this->batalDetailResep($id);
				}
			}
			if ($berhasil) $db->commit();
			else $db->rollback();
			$db->set_autocommit(true);
			$success['id'] = $id['id'];
			$success['success'] = 1;
			$success['message'] = $this->getMessage();
			if ($berhasil === false) $success['success'] = 0;
			return $success;
		}
		public function edit() {
			$id = $_POST['id'];
			$data['header'] = $this->dbtable->get_row("
				SELECT *
				FROM " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . "
				WHERE id = '" . $id . "'
			");
			$dresep_num = 0;
			$racikan_num = 0;
			$dracikan_num = 0;
			$bahan_num = 0;
			$dresep_list = "";
			$bahan_racikan_list = "";
			$obat_jadi_rows = $this->dbtable->get_result("
				SELECT *
				FROM " . InventoryLibrary::$_TBL_PENJUALAN_OBAT_JADI . "
				WHERE id_penjualan_resep = '" .  $id . "' AND prop NOT LIKE 'del'
			");
			$obat_racikan_rows = $this->dbtable->get_result("
				SELECT *
				FROM " . InventoryLibrary::$_TBL_PENJUALAN_OBAT_RACIKAN . "
				WHERE id_penjualan_resep = '" . $id . "' AND prop NOT LIKE 'del'
			");
			$no = 1;
			foreach($obat_jadi_rows as $obat_jadi) {
				$dresep_action = "";
				if (!$_POST['readonly']) {
					$dresep_action = "<div class='btn-group noprint'>" .
										"<a href='#' onclick='dresep.edit_obat_jadi(" . $dresep_num . ")' data-content='Ubah' data-toggle='popover' class='input btn btn-warning'>" .
											"<i class='icon-edit icon-white'></i>" .
										"</a>" .
										"<a href='#' onclick='dresep.delete(" . $dresep_num . ")' data-content='Hapus' data-toggle='popover' class='input btn btn-danger'>" .
											"<i class='icon-remove icon-white'></i>" .
										"</a>" .
									 "</div>";
				} else {
					if (getSettings($this->dbtable->get_db(), "depo_farmasi7-penjualan_resep_luar-etiket_obat", 0) == 1) {
						$dresep_action = "<div class='btn-group noprint'>" .
											"<a href='#' onclick='resep.print_etiket_obat_jadi(" . $obat_jadi->id . ")' data-content='Cetak Etiket' data-toggle='popover' class='input btn btn-inverse'>" .
												"<i class='fa fa-ticket'></i>" .
											"</a>" .
										"</div>";
					}
				}
				$dresep_list .= "<tr id='dresep_" . $dresep_num . "'>" .
									"<td id='dresep_" . $dresep_num . "_id' style='display: none;'>" . $obat_jadi->id . "</td>" .
									"<td id='dresep_" . $dresep_num . "_tipe' style='display: none;'>obat_jadi</td>" .
									"<td id='dresep_" . $dresep_num . "_id_obat' style='display: none;'>" . $obat_jadi->id_obat . "</td>" .
									"<td id='dresep_" . $dresep_num . "_kode_obat' style='display: none;'>" . $obat_jadi->kode_obat . "</td>" .
									"<td id='dresep_" . $dresep_num . "_nama_jenis_obat' style='display: none;'>" . $obat_jadi->nama_jenis_obat . "</td>" .
									"<td id='dresep_" . $dresep_num . "_jumlah' style='display: none;'>" . $obat_jadi->jumlah . "</td>" .
									"<td id='dresep_" . $dresep_num . "_jumlah_lama' style='display: none;'>" . $obat_jadi->jumlah . "</td>" .
									"<td id='dresep_" . $dresep_num . "_satuan' style='display: none;'>" . $obat_jadi->satuan . "</td>" .
									"<td id='dresep_" . $dresep_num . "_konversi' style='display: none;'>" . $obat_jadi->konversi . "</td>" .
									"<td id='dresep_" . $dresep_num . "_satuan_konversi' style='display: none;'>" . $obat_jadi->satuan_konversi . "</td>" .
									"<td id='dresep_" . $dresep_num . "_tanggal_exp' style='display: none;'>" . ArrayAdapter::format("date d-m-Y", $obat_jadi->tanggal_exp) . "</td>" .
									"<td id='dresep_" . $dresep_num . "_obat_luar' style='display: none;'>" . $obat_jadi->obat_luar . "</td>" .
									"<td id='dresep_" . $dresep_num . "_jumlah_pakai_sehari' style='display: none;'>" . $obat_jadi->jumlah_pakai_sehari . "</td>" .
									"<td id='dresep_" . $dresep_num . "_satuan_pakai' style='display: none;'>" . $obat_jadi->satuan_pakai . "</td>" .
									"<td id='dresep_" . $dresep_num . "_takaran_pakai' style='display: none;'>" . $obat_jadi->takaran_pakai . "</td>" .
									"<td id='dresep_" . $dresep_num . "_pemakaian' style='display: none;'>" . $obat_jadi->pemakaian . "</td>" .
									"<td id='dresep_" . $dresep_num . "_pemakaian_obat_luar' style='display: none;'>" . $obat_jadi->pemakaian_obat_luar . "</td>" .
									"<td id='dresep_" . $dresep_num . "_penggunaan' style='display: none;'>" . $obat_jadi->penggunaan . "</td>" .
									"<td id='dresep_" . $dresep_num . "_detail_penggunaan' style='display: none;'>" . $obat_jadi->detail_penggunaan . "</td>" .
									"<td id='dresep_" . $dresep_num . "_nomor'>" . $no++ . "</td>" .
									"<td id='dresep_" . $dresep_num . "_nama_obat'>" . $obat_jadi->nama_obat . "</td>" .
									"<td id='dresep_" . $dresep_num . "_f_jumlah'>" . $obat_jadi->jumlah . " " . $obat_jadi->satuan . "</td>" .
									"<td id='dresep_" . $dresep_num . "_harga'>" . "Rp. " . number_format($obat_jadi->harga, 2, ",", ".") . "</td>" .
									"<td id='dresep_" . $dresep_num . "_embalase'>" . "Rp. " . number_format($obat_jadi->embalase, 2, ",", ".") . "</td>" .
									"<td id='dresep_" . $dresep_num . "_tuslah'>" . "Rp. " . number_format($obat_jadi->tusla, 2, ",", ".") . "</td>" .
									"<td id='dresep_" . $dresep_num . "_biaya_racik'>" . "Rp. 0,00</td>" .
									"<td id='dresep_" . $dresep_num . "_subtotal'>" . "Rp. " . number_format($obat_jadi->subtotal, 2, ",", ".") . "</td>" .
									"<td id='dresep_" . $dresep_num . "_aturan_pakai'>" . $obat_jadi->aturan_pakai . "</td>" .
									"<td id='dresep_" . $dresep_num . "_apoteker'>-</td>" .
									"<td>" .
										$dresep_action .
									"</td>" .
								"</tr>";
				$dresep_num++;
			}
			foreach($obat_racikan_rows as $obat_racikan) {			
				$bahan_rows = $this->dbtable->get_result("
								SELECT *
								FROM " . InventoryLibrary::$_TBL_BAHAN_PAKAI_OBAT_RACIKAN . "
								WHERE id_penjualan_obat_racikan = '" . $obat_racikan->id . "'
							");
				foreach($bahan_rows as $bahan) {
					$bahan_racikan_list .= "<tr id=bahan_" . $bahan_num . ">" .
												"<td style='display: none;' id='bahan_" . $bahan_num . "_id'>" . $bahan->id . "</td>" .
												"<td style='display: none;' id='bahan_" . $bahan_num . "_id_bahan'>" . $bahan->id_obat . "</td>" .
												"<td style='display: none;' id='bahan_" . $bahan_num . "_kode'>" . $bahan->kode_obat . "</td>" .
												"<td style='display: none;' id='bahan_" . $bahan_num . "_nama'>" . $bahan->nama_obat . "</td>" .
												"<td style='display: none;' id='bahan_" . $bahan_num . "_nama_jenis_bahan'>" . $bahan->nama_jenis_obat . "</td>" .
												"<td style='display: none;' id='bahan_" . $bahan_num . "_jumlah'>" . $bahan->jumlah . "</td>" .
												"<td style='display: none;' id='bahan_" . $bahan_num . "_jumlah_lama'>" . $bahan->jumlah . "</td>" .
												"<td style='display: none;' id='bahan_" . $bahan_num . "_satuan'>" . $bahan->satuan . "</td>" .
												"<td style='display: none;' id='bahan_" . $bahan_num . "_konversi'>" . $bahan->konversi . "</td>" .
												"<td style='display: none;' id='bahan_" . $bahan_num . "_satuan_konversi'>" . $bahan->satuan_konversi . "</td>" .
												"<td style='display: none;' id='bahan_" . $bahan_num . "_harga'>" . "Rp. " . number_format($bahan->harga, 2, ",", ".") . "</td>" .
												"<td style='display: none;' id='bahan_" . $bahan_num . "_embalase'>" . "Rp. " . number_format($bahan->embalase, 2, ",", ".") . "</td>" .
												"<td style='display: none;' id='bahan_" . $bahan_num . "_tuslah'>" . "Rp. " . number_format($bahan->tusla, 2, ",", ".") . "</td>" .
												"<td style='display: none;' id='bahan_" . $bahan_num . "_label'>racikan_" . $racikan_num . "</td>" .
										   "</tr>";
					$bahan_num++;
				}
				$dresep_action = "";
				if (!$_POST['readonly']) {
					$dresep_action = "<div class='btn-group noprint'>" .
										  "<a href='#' onclick='dresep.edit_obat_racikan(" . $dresep_num . "," . $racikan_num . ")' data-content='Ubah' data-toggle='popover' class='input btn btn-warning'>" .
											  "<i class='icon-edit icon-white'></i>" .
										  "</a>" .
										  "<a href='#' onclick='dresep.delete(" . $dresep_num . ")' data-content='Hapus' data-toggle='popover' class='input btn btn-danger'>" .
											  "<i class='icon-remove icon-white'></i>" .
										  "</a>" .
									  "</div>";
				} else {
					$dresep_action = "<div class='btn-group noprint'>" .
										 "<a href='#' onclick='dresep.view_obat_racikan(" . $dresep_num . "," . $racikan_num . ")' data-content='Ubah' data-toggle='popover' class='input btn btn-success'>" .
											"<i class='icon-eye-open icon-white'></i>" .
										 "</a>";
					if (getSettings($this->dbtable->get_db(), "depo_farmasi7-penjualan_resep_luar-etiket_obat", 0) == 1) {
						$dresep_action .=	"<a href='#' onclick='resep.print_etiket_obat_racikan(" . $obat_racikan->id . ")' data-content='Cetak Etiket' data-toggle='popover' class='input btn btn-inverse'>" .
												"<i class='fa fa-ticket'></i>" .
											"</a>";
					}
					$dresep_action .= "</div>";
				}
				$dresep_list .= "<tr id='dresep_" . $dresep_num . "'>" .
									"<td id='dresep_" . $dresep_num . "_id' style='display: none;'>" . $obat_racikan->id . "</td>" .
									"<td id='dresep_" . $dresep_num . "_tipe' style='display: none;'>obat_racikan</td>" .
									"<td id='dresep_" . $dresep_num . "_label' style='display: none;'>racikan_" . $racikan_num . "</td>" .
									"<td id='dresep_" . $dresep_num . "_id_apoteker' style='display: none;'>" . $obat_racikan->id_apoteker . "</td>" .
									"<td id='dresep_" . $dresep_num . "_jumlah' style='display: none;'>" . $obat_racikan->jumlah . "</td>" .
									"<td id='dresep_" . $dresep_num . "_tanggal_exp' style='display: none;'>" . ArrayAdapter::format("date d-m-Y", $obat_racikan->tanggal_exp) . "</td>" .
									"<td id='dresep_" . $dresep_num . "_obat_luar' style='display: none;'>" . $obat_racikan->obat_luar . "</td>" .
									"<td id='dresep_" . $dresep_num . "_jumlah_pakai_sehari' style='display: none;'>" . $obat_racikan->jumlah_pakai_sehari . "</td>" .
									"<td id='dresep_" . $dresep_num . "_satuan_pakai' style='display: none;'>" . $obat_racikan->satuan_pakai . "</td>" .
									"<td id='dresep_" . $dresep_num . "_takaran_pakai' style='display: none;'>" . $obat_racikan->takaran_pakai . "</td>" .
									"<td id='dresep_" . $dresep_num . "_pemakaian' style='display: none;'>" . $obat_racikan->pemakaian . "</td>" .
									"<td id='dresep_" . $dresep_num . "_pemakaian_obat_luar' style='display: none;'>" . $obat_racikan->pemakaian_obat_luar . "</td>" .
									"<td id='dresep_" . $dresep_num . "_penggunaan' style='display: none;'>" . $obat_racikan->penggunaan . "</td>" .
									"<td id='dresep_" . $dresep_num . "_detail_penggunaan' style='display: none;'>" . $obat_racikan->detail_penggunaan . "</td>" .
									"<td id='dresep_" . $dresep_num . "_nomor'>" . $no++ . "</td>" .
									"<td id='dresep_" . $dresep_num . "_nama_racikan'>" . $obat_racikan->nama . "</td>" .
									"<td id='dresep_" . $dresep_num . "_f_jumlah'>" . $obat_racikan->jumlah . " Racikan</td>" .
									"<td id='dresep_" . $dresep_num . "_f_harga'>" . "Rp. " . number_format($obat_racikan->harga, 2, ",", ".") . "</td>" .
									"<td id='dresep_" . $dresep_num . "_embalase'>" . "Rp. " . number_format($obat_racikan->embalase, 2, ",", ".") . "</td>" .
									"<td id='dresep_" . $dresep_num . "_tuslah'>" . "Rp. " . number_format($obat_racikan->tusla, 2, ",", ".") . "</td>" .
									"<td id='dresep_" . $dresep_num . "_biaya_racik'>" . "Rp. " . number_format($obat_racikan->biaya_racik, 2, ",", ".") . "</td>" .
									"<td id='dresep_" . $dresep_num . "_subtotal'>" . "Rp. " . number_format($obat_racikan->subtotal, 2, ",", ".") . "</td>" .
									"<td id='dresep_" . $dresep_num . "_aturan_pakai'>" . $obat_racikan->aturan_pakai . "</td>" . 
									"<td id='dresep_" . $dresep_num . "_apoteker'>" . $obat_racikan->nama_apoteker . "</td>" .
									"<td>" .
										$dresep_action .
									"</td>" .
								"</tr>";
				$dresep_num++;
				$racikan_num++;
			}
			$data['dresep_num'] = $dresep_num;
			$data['racikan_num'] = $racikan_num;
			$data['dracikan_num'] = $dracikan_num;
			$data['bahan_num'] = $bahan_num;
			$data['dresep_list'] = $dresep_list;
			$data['bahan_racikan_list'] = $bahan_racikan_list;
			return $data;
		}
	}
?>