<?php
require_once("depo_farmasi_irna/library/InventoryLibrary.php");
require_once("smis-base/smis-include-service-consumer.php");
require_once("smis-base/smis-include-duplicate.php");

class ObatCitoMasukDBResponder extends DuplicateResponder {
	public function save() {
		$header_data = $this->postToArray();
		$id['id'] = $_POST['id'];
		$result = false;
		if ($id['id'] == 0 || $id['id'] == "") {
			//do insert header here:
			$result = $this->dbtable->insert($header_data);
			$id['id'] = $this->dbtable->get_inserted_id();
			$success['type'] = "insert";
			$subtotal = 0;
			if (isset($_POST['detail'])) {
				//do insert detail here:
				$detail_dbtable = new DBTable($this->dbtable->get_db(), InventoryLibrary::$_TBL_DPEMBELIAN_UP);
				$detail = $_POST['detail'];				
				foreach($detail as $d) {
					//detail:
					$detail_data = array();
					$detail_data['id_pembelian_up'] = $id['id'];
					$detail_data['id_obat'] = $d['id_obat'];
					$detail_data['kode_obat'] = $d['kode_obat'];
					$detail_data['nama_obat'] = $d['nama_obat'];
					$detail_data['nama_jenis_obat'] = $d['nama_jenis_obat'];
					$detail_data['jumlah'] = $d['jumlah'];
					$detail_data['satuan'] = $d['satuan'];
					$detail_data['konversi'] = $d['konversi'];
					$detail_data['satuan_konversi'] = $d['satuan_konversi'];
					$detail_data['hna'] = $d['hna'];
					$detail_data['produsen'] = $d['produsen'];
					$detail_data['tanggal_exp'] = $d['tanggal_exp'];
					$detail_data['no_batch'] = $d['no_batch'];
					$detail_data['autonomous'] = "[".$this->getAutonomous()."]";
			        $detail_data['duplicate'] = 0;
			        $detail_data['time_updated'] = date("Y-m-d H:i:s");
			        $detail_data['origin_updated'] = $this->getAutonomous();
					$detail_dbtable->insert($detail_data);
				}
			}
			//do push service:
			$data = array();
			$data['unit'] = "depo_farmasi_irna";
			$data['command'] = "insert";
			global $user;
			$data['user'] =  $user->getName();
			$data['header'] = $this->dbtable->select($id['id']);
			$data['detail'] = $this->dbtable->get_result("
				SELECT *
				FROM " . InventoryLibrary::$_TBL_DPEMBELIAN_UP . "
				WHERE id_pembelian_up = '" . $id['id'] . "'
			");
			$service_consumer = new ServiceConsumer(
				$this->dbtable->get_db(),
				"push_pembelian_up",
				$data,
				"gudang_farmasi"
			);
			$service_consumer->setMode(ServiceConsumer::$CLEAN_BOTH);
			$content = $service_consumer->execute()->getContent();
			if ($content != null && $content[0] > 0) {
				$f_id = $content[0];
				$header_data = array();
				$header_data['f_id'] = $f_id;
				$this->dbtable->update($header_data, $id);
			}
		} else {
			$result = $this->dbtable->update($header_data, $id);
			$success['type'] = "update";
			//do update header service:
			$data = array();
			$data['unit'] = "depo_farmasi_irna";
			$data['command'] = "update";
			$data['user'] =  $user->getName();
			$data['header'] = $this->dbtable->select($id['id']);
			$data['detail'] = $this->dbtable->get_result("
				SELECT *
				FROM " . InventoryLibrary::$_TBL_DPEMBELIAN_UP . "
				WHERE id_pembelian_up = '" . $id['id'] . "'
			");
			$service_consumer = new ServiceConsumer(
				$this->dbtable->get_db(),
				"push_pembelian_up",
				$data,
				"gudang_farmasi"
			);
			$service_consumer->execute();
		}
		$success['id'] = $id['id'];
		$success['success'] = 1;
		if ($result === false) $success['success'] = 0;
		return $success;
	}
	public function edit() {
		$id = $_POST['id'];
		$header_row = $this->dbtable->get_row("
			SELECT *
			FROM " . InventoryLibrary::$_TBL_PEMBELIAN_UP . "
			WHERE id = '" . $id . "'
		");
		$data['header'] = $header_row;
		$detail_dbtable = new DBTable($this->dbtable->get_db(), InventoryLibrary::$_TBL_DPEMBELIAN_UP);
		$data['detail'] = $detail_dbtable->get_result("
			SELECT *
			FROM " . InventoryLibrary::$_TBL_DPEMBELIAN_UP . "
			WHERE id_pembelian_up = '" . $id . "' AND prop NOT LIKE 'del'
		");
		return $data;
	}
	public function delete() {
		$id['id'] = $_POST['id'];
		if ($this->dbtable->isRealDelete()) {
			$result = $this->dbtable->delete(null,$id);
		} else {
			$data['prop'] = "del";
			$data['autonomous'] = "[".$this->getAutonomous()."]";
	        $data['duplicate'] = 0;
	        $data['time_updated'] = date("Y-m-d H:i:s");
			$result = $this->dbtable->update($data, $id);
		}
		$success['success'] = 1;
		$success['id'] = $_POST['id'];
		if ($result === 'false') $success['success'] = 0;
		else {
			//do delete service:
			$data = array();
			$data['user'] =  $user->getName();
			$data['unit'] = "depo_farmasi_irna";
			$data['command'] = "delete";
			$data['header'] = $this->dbtable->select($id['id']);
			$data['detail'] = $this->dbtable->get_result("
				SELECT *
				FROM " . InventoryLibrary::$_TBL_DPEMBELIAN_UP . "
				WHERE id_pembelian_up = '" . $id['id'] . "'
			");
			$service_consumer = new ServiceConsumer(
				$this->dbtable->get_db(),
				"push_pembelian_up",
				$data,
				"gudang_farmasi"
			);
			$service_consumer->execute();
		}
		return $success;
	}
}
?>