<?php
	require_once("depo_farmasi_irna/library/InventoryLibrary.php");
	require_once("gudang_farmasi/kelas/GudangFarmasiInventory.php");
	
	class SisaDBResponder extends DBResponder {
		public function edit() {
			$id_obat = $_POST['id_obat'];
			$satuan = $_POST['satuan'];
			$konversi = $_POST['konversi'];
			$satuan_konversi = $_POST['satuan_konversi'];
			$ppn = $_POST['ppn'];
			$default_margin = 0;
			$value_margin = 0;

			$hna = GudangFarmasiInventory::getHPPPenjualan($this->dbtable->get_db(), $id_obat, $ppn);

			// margin per rentang harga :
			if (getSettings($this->dbtable->get_db(), "depo_farmasi7-" . $_POST['action'] . "-mode_margin_penjualan", 0) == 4) {
				$stok_data = $this->dbtable->get_row("
					SELECT id_obat, " . $hna . " AS 'hna', SUM(sisa) AS 'sisa', satuan, konversi, satuan_konversi, MIN(tanggal_exp) tanggal_exp
					FROM (
						SELECT a.id_obat, a.sisa, a.satuan, a.konversi, a.satuan_konversi, a.tanggal_exp
						FROM " . InventoryLibrary::$_TBL_STOK_OBAT . " a LEFT JOIN " . InventoryLibrary::$_TBL_OBAT_MASUK . " b ON a.id_obat_masuk = b.id
						WHERE a.prop NOT LIKE 'del' AND b.prop NOT LIKE 'del' AND b.status = 'sudah' AND a.id_obat = '" . $id_obat . "' AND a.satuan = '" . $satuan . "' AND a.konversi = '" . $konversi . "' AND a.satuan_konversi = '" . $satuan_konversi . "'
					) v_stok_obat
					GROUP BY id_obat, satuan, konversi, satuan_konversi
				");
				$margin_data = $this->dbtable->get_row("
					SELECT IF(" . $hna . " >= batas_bawah AND " . $hna . " < batas_atas, margin_jual, 0) AS markup 
					FROM " . InventoryLibrary::$_TBL_MARGIN_JUAL_RENTANG_HARGA . "
					HAVING markup > 0
					LIMIT 0, 1
				");
				$margin_jual = 0;
				if ($margin_data != null) 
					$margin_jual = $margin_data->markup;
				return (object)array(
					"id_obat" 			=> $stok_data->id_obat,
					"hna"				=> $stok_data->hna,
					"sisa"				=> $stok_data->sisa,
					"satuan" 			=> $stok_data->satuan,
					"konversi" 			=> $stok_data->konversi,
					"satuan_konversi"	=> $stok_data->satuan_konversi,
					"markup"			=> $margin_jual,
					"tanggal_exp"		=> ArrayAdapter::format("date d-m-Y", $stok_data->tanggal_exp)
				);
			}
			// margin per obat - jenis pasien :
			if (getSettings($this->dbtable->get_db(), "depo_farmasi7-" . $_POST['action'] . "-mode_margin_penjualan", 0) == 3) {
				$jenis_pasien = "";
				$asuransi = "";
				$perusahaan = "";
				$uri = "";
				if (isset($_POST['jenis_pasien']))
					$jenis_pasien = $_POST['jenis_pasien'];
				if (isset($_POST['asuransi']))
					$asuransi = $_POST['asuransi'];
				if (isset($_POST['perusahaan']))
					$perusahaan = $_POST['perusahaan'];
				if (isset($_POST['uri']))
					$uri = $_POST['uri'];
				$value_margin = "b.margin_jual";
				$data = $this->dbtable->get_row("
					SELECT a.*, CASE WHEN b.margin_jual IS NULL THEN " . $default_margin . " ELSE " . $value_margin . " END AS 'markup'
					FROM
					(
						SELECT id_obat, " . $hna . " AS 'hna', SUM(sisa) AS 'sisa', satuan, konversi, satuan_konversi, MIN(tanggal_exp) tanggal_exp
						FROM (
							SELECT a.id_obat, a.sisa, a.satuan, a.konversi, a.satuan_konversi, a.tanggal_exp
							FROM " . InventoryLibrary::$_TBL_STOK_OBAT . " a LEFT JOIN " . InventoryLibrary::$_TBL_OBAT_MASUK . " b ON a.id_obat_masuk = b.id
							WHERE a.prop NOT LIKE 'del' AND b.prop NOT LIKE 'del' AND b.status = 'sudah' AND a.id_obat = '" . $id_obat . "' AND a.satuan = '" . $satuan . "' AND a.konversi = '" . $konversi . "' AND a.satuan_konversi = '" . $satuan_konversi . "'
						) v_stok_obat
						GROUP BY id_obat, satuan, konversi, satuan_konversi
					) a LEFT JOIN (
						SELECT id_obat, margin_jual
						FROM " . InventoryLibrary::$_TBL_MARGIN_JUAL_OBAT_JENIS_PASIEN . "
						WHERE id_obat = " . $id_obat . " AND prop NOT LIKE 'del' AND slug = '" . $jenis_pasien . "' AND asuransi = '" . $asuransi . "' AND perusahaan = '" . $perusahaan . "' AND uri = '" . $uri . "'
						ORDER BY id DESC
						LIMIT 0, 1
					) b ON a.id_obat = b.id_obat
				");
				if ($data != null)
					$data->tanggal_exp = ArrayAdapter::format("date d-m-Y", $data->tanggal_exp);
				return $data;
			}
			// margin per obat :
			if (getSettings($this->dbtable->get_db(), "depo_farmasi7-" . $_POST['action'] . "-mode_margin_penjualan", 0) == 2) {
				$value_margin = "b.margin_jual";
				$data = $this->dbtable->get_row("
					SELECT a.*, CASE WHEN b.margin_jual IS NULL THEN " . $default_margin . " ELSE " . $value_margin . " END AS 'markup'
					FROM
					(
						SELECT id_obat, " . $hna . " AS 'hna', SUM(sisa) AS 'sisa', satuan, konversi, satuan_konversi, MIN(tanggal_exp) tanggal_exp
						FROM (
							SELECT a.id_obat, a.sisa, a.satuan, a.konversi, a.satuan_konversi, a.tanggal_exp
							FROM " . InventoryLibrary::$_TBL_STOK_OBAT . " a LEFT JOIN " . InventoryLibrary::$_TBL_OBAT_MASUK . " b ON a.id_obat_masuk = b.id
							WHERE a.prop NOT LIKE 'del' AND b.prop NOT LIKE 'del' AND b.status = 'sudah' AND a.id_obat = '" . $id_obat . "' AND a.satuan = '" . $satuan . "' AND a.konversi = '" . $konversi . "' AND a.satuan_konversi = '" . $satuan_konversi . "'
						) v_stok_obat
						GROUP BY id_obat, satuan, konversi, satuan_konversi
					) a LEFT JOIN (
						SELECT id_obat, margin_jual
						FROM " . InventoryLibrary::$_TBL_MARGIN_JUAL_OBAT . "
						WHERE id_obat = " . $id_obat . " AND prop NOT LIKE 'del'
						ORDER BY id DESC
						LIMIT 0, 1
					) b ON a.id_obat = b.id_obat
				");
				if ($data != null)
					$data->tanggal_exp = ArrayAdapter::format("date d-m-Y", $data->tanggal_exp);
				return $data;
			}
			// margin per jenis pasien :
			if (getSettings($this->dbtable->get_db(), "depo_farmasi7-" . $_POST['action'] . "-mode_margin_penjualan", 0) == 1) {
				$jenis_pasien = $_POST['jenis_pasien'];
				$data = $this->dbtable->get_row("
					SELECT *
					FROM " . InventoryLibrary::$_TBL_MARGIN_JUAL_JENIS_PASIEN . "
					WHERE prop NOT LIKE 'del' AND slug = '" . $jenis_pasien . "'
					LIMIT 0, 1
				");
				if ($data != null)
					$value_margin = $data->margin_jual;
				$data = $this->dbtable->get_row("
					SELECT a.id_obat, " . $hna . " AS 'hna', SUM(a.sisa) AS 'sisa', a.satuan, a.konversi, a.satuan_konversi, " . $value_margin . " AS markup, MIN(a.tanggal_exp) tanggal_exp
					FROM " . InventoryLibrary::$_TBL_STOK_OBAT . " a LEFT JOIN " . InventoryLibrary::$_TBL_OBAT_MASUK . " b ON a.id_obat_masuk = b.id
					WHERE a.prop NOT LIKE 'del' AND b.prop NOT LIKE 'del' AND b.status = 'sudah' AND a.id_obat = '" . $id_obat . "' AND a.satuan = '" . $satuan . "' AND a.konversi = '" . $konversi . "' AND a.satuan_konversi = '" . $satuan_konversi . "'
					GROUP BY a.id_obat, a.satuan, a.konversi, a.satuan_konversi
				");
				if ($data != null)
					$data->tanggal_exp = ArrayAdapter::format("date d-m-Y", $data->tanggal_exp);
				return $data;
			}
			// margin per jenis transaksi:
			if (getSettings($this->dbtable->get_db(), "depo_farmasi7-" . $_POST['action'] . "-mode_margin_penjualan", 0) == 0) {
				$value_margin = getSettings($this->dbtable->get_db(), "depo_farmasi7-" . $_POST['action'] . "-margin_penjualan", 0);
				$data = $this->dbtable->get_row("
					SELECT a.id_obat, " . $hna . " AS 'hna', SUM(a.sisa) AS 'sisa', a.satuan, a.konversi, a.satuan_konversi, " . $value_margin . " AS markup, MIN(a.tanggal_exp) tanggal_exp
					FROM " . InventoryLibrary::$_TBL_STOK_OBAT . " a LEFT JOIN " . InventoryLibrary::$_TBL_OBAT_MASUK . " b ON a.id_obat_masuk = b.id
					WHERE a.prop NOT LIKE 'del' AND b.prop NOT LIKE 'del' AND b.status = 'sudah' AND a.id_obat = '" . $id_obat . "' AND a.satuan = '" . $satuan . "' AND konversi = '" . $konversi . "' AND satuan_konversi = '" . $satuan_konversi . "'
					GROUP BY a.id_obat, a.satuan, a.konversi, a.satuan_konversi
				");
				if ($data != null)
					$data->tanggal_exp = ArrayAdapter::format("date d-m-Y", $data->tanggal_exp);
				return $data;
			}
			return null;
		}
	}
?>