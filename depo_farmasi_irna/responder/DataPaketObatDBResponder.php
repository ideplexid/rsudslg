<?php
    require_once("depo_farmasi_irna/library/InventoryLibrary.php");

    class DataPaketObatDBResponder extends DBResponder {
        public function edit() {
            $part = explode("|", $_POST['id']);
            $nama_paket = $part[0];
            $is_racikan = $part[1];

            $data['nama_paket'] = $nama_paket;
            $data['is_racikan'] = $is_racikan;

            $html = "";

            $detail_rows = $this->dbtable->get_db()->get_result("
                SELECT
                    *
                FROM
                    " . InventoryLibrary::$_TBL_PAKET_OBAT . "
                WHERE
                    prop = ''
                        AND nama_paket LIKE '" . $nama_paket . "'
                        AND is_racikan = '" . $is_racikan . "'
            ");
            if ($detail_rows != null) {
                $row_num = 0;
                foreach ($detail_rows as $dr) {
                    $html_button =  "<div class='btn-group noprint'>" .
                                        "<a href='#' onclick='detail_paket_obat.edit(" . $row_num . ")' data-content='Edit' id='data_paket_obat_edit' class='input btn btn-warning'>" .
                                            "<i class='fa fa-pencil'></i>" .
                                        "</a>" .
                                        "<a href='#' onclick='detail_paket_obat.del(" . $row_num . ")' data-content='Delete' id='data_paket_obat_del' class='input btn btn-danger'>" .
                                            "<i class='fa fa-trash'></i>" .
                                        "</a>" .
                                    "</div>";
                    $html .=    "<tr>" .
                                    "<td id='id' style='display: none;'>" . $dr->id . "</td>" .
                                    "<td id='nomor'>" . ($row_num + 1) . "</td>" .
                                    "<td id='id_obat'>" . $dr->id_obat . "</td>" .
                                    "<td id='kode_obat'>" . $dr->kode_obat . "</td>" .
                                    "<td id='nama_obat'>" . $dr->nama_obat . "</td>" .
                                    "<td id='nama_jenis_obat'>" . $dr->nama_jenis_obat . "</td>" .
                                    "<td id='jumlah'>" . $dr->jumlah . "</td>" .
                                    "<td id='satuan'>" . $dr->satuan . "</td>" .
                                    "<td class='noprint'>" . $html_button . "</td>" .
                                "</tr>";
                    $row_num++;
                }
            }

            $data['detail_html'] = $html;

            return $data;
        }

        public function save() {
            $nama_paket = $_POST['nama_paket'];
            $is_racikan = $_POST['is_racikan'];
            $detail = json_decode($_POST['detail'], true);
            $result = array(
                "type"      => "-",
                "id"        => $nama_paket . "|" . $is_racikan,
                "success"   => 0
            );

            if (count($detail) > 0) {
                foreach ($detail as $d) {
                    $data = array(
                        'nama_paket'        => $nama_paket,
                        'is_racikan'        => $is_racikan,
                        'id_obat'           => $d['id_obat'],
                        'kode_obat'         => $d['kode_obat'],
                        'nama_obat'         => $d['nama_obat'],
                        'nama_jenis_obat'   => $d['nama_jenis_obat'],
                        'jumlah'            => $d['jumlah'],
                        'satuan'            => $d['satuan']
                    );
                    if ($d['id'] == "") {
                        $this->dbtable->insert($data);
                        $result['type'] = "insert";
                    } else {
                        if ($d['was_deleted'] == 1) {
                            $data = array('prop' => "del");
                            $id['id'] = $d['id'];
                            $this->dbtable->update($data, $id);
                            $result['type'] = "delete";
                        } else {
                            $id['id'] = $d['id'];
                            $this->dbtable->update($data, $id);
                            $result['type'] = "update";
                        }
                    }
                }
                $result['success'] = 1;
            }
            return $result;
        }

        public function delete(){
            $part = explode("|", $_POST['id']);
            $nama_paket = $part[0];
            $is_racikan = $part[1];
            
            $id['nama_paket'] = $nama_paket;
            $id['is_racikan'] = $is_racikan;
            
            $data['prop'] = "del";
            
            $this->dbtable->update($data, $id);

            return true;
        }
    }
?>