<?php
    class ROPPPasienDBResponder extends DBResponder {
        public function edit() {
            $id = $_POST['id'];

            $noreg_pasien = $id;
            $nrm_pasien = "";
            $nama_pasien = "";
            $alamat_pasien = "";
            $daftar_dokter = "";
            $detail_html = "<tr><td colspan='7'><center><small>Tidak Memiliki Transaksi Penjualan</small></center></td></tr>";

            $header_row = $this->dbtable->get_row("
                SELECT
                    noreg_pasien, nrm_pasien, nama_pasien, alamat_pasien, GROUP_CONCAT(DISTINCT nama_dokter SEPARATOR '\r\n') daftar_dokter
                FROM
                    " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . "
                WHERE
                    noreg_pasien = '" . $id . "' AND dibatalkan = '0'
                GROUP BY
                    noreg_pasien, nrm_pasien, nama_pasien, alamat_pasien
                LIMIT
                    0, 1
            ");
            if ($header_row != null) {
                $nrm_pasien = $header_row->nrm_pasien;
                $nama_pasien = $header_row->nama_pasien;
                $alamat_pasien = $header_row->alamat_pasien;
                $daftar_dokter = $header_row->daftar_dokter;

                $detail_rows = $this->dbtable->get_result("
                    SELECT
                        a.id id_penjualan_resep, 
                        a.nomor_resep, 
                        c.id_stok_obat,
                        c.id id_stok_pakai_obat_jadi, 
                        b.nama_obat, 
                        d.tanggal_exp, 
                        c.jumlah, 
                        b.harga, 
                        b.satuan
                    FROM
                        " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . " a
                            INNER JOIN " . InventoryLibrary::$_TBL_PENJUALAN_OBAT_JADI . " b ON a.id = b.id_penjualan_resep
                            INNER JOIN " . InventoryLibrary::$_TBL_STOK_PAKAI_OBAT_JADI . " c ON b.id = c.id_penjualan_obat_jadi
                            INNER JOIN " . InventoryLibrary::$_TBL_STOK_OBAT . " d ON c.id_stok_obat = d.id
                    WHERE
                        a.prop = ''
                            AND a.dibatalkan = '0'
                            AND a.diretur = '0'
                            AND b.prop = ''
                            AND c.prop = ''
                            AND c.jumlah > 0
                            AND a.noreg_pasien = '" . $noreg_pasien . "'
                    ORDER BY
                        b.nama_obat ASC
                ");
                if ($detail_rows != null) {
                    $detail_html = "";
                    $row_num = 0;
                    foreach ($detail_rows as $detail) {
                        $subtotal = $detail->jumlah * $detail->harga;
                        $f_subtotal = $detail->jumlah . " x " . ArrayAdapter::format("only-money", $detail->harga) . " = " . ArrayAdapter::format("only-money", $subtotal);
                        $f_jumlah_beli = $detail->jumlah . " " . $detail->satuan;
                        $tanggal_exp = $detail->tanggal_exp == "0000-00-00" ? "-" : ArrayAdapter::format("date d-m-Y", $detail->tanggal_exp);
                        $btn_html = "<a href='#' onclick='detail_retur_obat_per_pasien.edit(" . $row_num++ . ")' data-content='Edit' class='input btn btn-warning'><i class='fa fa-pencil'></i></a>";
                        $detail_html .= "<tr>";
                            $detail_html .= "<td id='id_penjualan_resep'>" . $detail->id_penjualan_resep . "</td>";
                            $detail_html .= "<td id='nomor_resep'>" . $detail->nomor_resep . "</td>";
                            $detail_html .= "<td id='id_stok_obat' style='display: none;'>" . $detail->id_stok_obat . "</td>";
                            $detail_html .= "<td id='id_stok_pakai_obat_jadi' style='display: none;'>" . $detail->id_stok_pakai_obat_jadi . "</td>";
                            $detail_html .= "<td id='nama_obat'>" . $detail->nama_obat . "</td>";
                            $detail_html .= "<td id='tanggal_exp'>" . $tanggal_exp . "</td>";
                            $detail_html .= "<td id='f_subtotal'>" . $f_subtotal . "</td>";
                            $detail_html .= "<td id='jumlah_jual' style='display: none;'>" . $detail->jumlah . "</td>";
                            $detail_html .= "<td id='satuan' style='display: none;'>" . $detail->satuan . "</td>";
                            $detail_html .= "<td id='harga_jual' style='display: none;'>" . $detail->harga . "</td>";
                            $detail_html .= "<td id='f_jumlah_jual'>" . $f_jumlah_beli . "</td>";
                            $detail_html .= "<td id='jumlah_retur' style='display: none;'>0</td>";
                            $detail_html .= "<td id='f_jumlah_retur'>0 " . $detail->satuan . "</td>";
                            $detail_html .= "<td>" . $btn_html . "</td>";
                        $detail_html .= "</tr>";
                    }
                }
            }

            $data = array(
                'noreg_pasien'  => $noreg_pasien,
                'nrm_pasien'    => $nrm_pasien,
                'nama_pasien'   => $nama_pasien,
                'alamat_pasien' => $alamat_pasien,
                'daftar_dokter' => $daftar_dokter,
                'detail_html'   => $detail_html
            );
            return $data;
        }
    }
?>