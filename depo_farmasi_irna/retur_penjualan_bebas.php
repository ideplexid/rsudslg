<?php 
	require_once("depo_farmasi_irna/library/InventoryLibrary.php");
	require_once("depo_farmasi_irna/table/ReturPenjualanBebasTable.php");
	require_once("depo_farmasi_irna/adapter/ReturPenjualanBebasAdapter.php");
	require_once("depo_farmasi_irna/responder/ReturPenjualanBebasDBResponder.php");
	require_once("depo_farmasi_irna/responder/ReturPenjualanBebas_PenjualanBebasDBResponder.php");
	global $db;
	
	$retur_penjualan_bebas_table = new ReturPenjualanBebasTable(
		array("Nomor", "Tanggal/Jam", "No. Penjualan", "Status"),
		"Depo Farmasi IRNA : Retur Penjualan Bebas",
		null,
		true
	);
	$retur_penjualan_bebas_table->setName("retur");
	
	if (isset($_POST['super_command']) && $_POST['super_command'] == "retur") {
		if (isset($_POST['command'])) {
			$retur_penjualan_bebas_adapter = new ReturPenjualanBebasAdapter();
			$columns = array("id", "tanggal", "id_penjualan_resep", "persentase_retur", "dibatalkan", "tercetak");
			$retur_penjualan_bebas_dbtable = new DBTable(
				$db,
				InventoryLibrary::$_TBL_RETUR_PENJUALAN_RESEP,
				$columns
			);
			$filter = "";
			if (isset($_POST['kriteria'])) {
				$filter = " AND (" . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".id LIKE '%" . $_POST['kriteria'] . "%') ";
			}
			$query_value = "
				SELECT *
				FROM (
					SELECT " . InventoryLibrary::$_TBL_RETUR_PENJUALAN_RESEP . ".*, " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".nama_dokter, " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".nama_pasien, " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".nomor_resep, " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".nrm_pasien, " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".noreg_pasien
					FROM " . InventoryLibrary::$_TBL_RETUR_PENJUALAN_RESEP . " LEFT JOIN " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . " ON " . InventoryLibrary::$_TBL_RETUR_PENJUALAN_RESEP . ".id_penjualan_resep = " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".id
					WHERE " . InventoryLibrary::$_TBL_RETUR_PENJUALAN_RESEP . ".prop NOT LIKE 'del' AND " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".tipe = 'bebas' " . $filter . "
					ORDER BY " . InventoryLibrary::$_TBL_RETUR_PENJUALAN_RESEP . ".id DESC
				) v_retur
			";
			$query_count = "
				SELECT COUNT(*)
				FROM (
					SELECT " . InventoryLibrary::$_TBL_RETUR_PENJUALAN_RESEP . ".*, " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".nama_dokter, " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".nama_pasien, " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".nomor_resep, " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".nrm_pasien, " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".noreg_pasien
					FROM " . InventoryLibrary::$_TBL_RETUR_PENJUALAN_RESEP . " LEFT JOIN " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . " ON " . InventoryLibrary::$_TBL_RETUR_PENJUALAN_RESEP . ".id_penjualan_resep = " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".id
					WHERE " . InventoryLibrary::$_TBL_RETUR_PENJUALAN_RESEP . ".prop NOT LIKE 'del' AND " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . ".tipe = 'bebas' " . $filter . "
					ORDER BY " . InventoryLibrary::$_TBL_RETUR_PENJUALAN_RESEP . ".id DESC
				) v_retur
			";
			$retur_penjualan_bebas_dbtable->setPreferredQuery(true, $query_value, $query_count);
			$retur_penjualan_bebas_dbresponder = new ReturPenjualanBebasDBResponder(
				$retur_penjualan_bebas_dbtable,
				$retur_penjualan_bebas_table,
				$retur_penjualan_bebas_adapter
			);
			$data = $retur_penjualan_bebas_dbresponder->command($_POST['command']);
			echo json_encode($data);
			return;
		}
		return;
	}
	
	//penjualan bebas chooser:
	$penjualan_bebas_table = new Table(
		array("No. Penjualan", "Tanggal/Jam"),
		"",
		null,
		true
	);
	$penjualan_bebas_table->setName("penjualan_bebas");
	$penjualan_bebas_table->setModel(Table::$SELECT);
	$penjualan_bebas_adapter = new SimpleAdapter();
	$penjualan_bebas_adapter->add("No. Penjualan", "id", "digit8");
	$penjualan_bebas_adapter->add("Tanggal/Jam", "tanggal", "date d-m-Y H:i");
	$penjualan_bebas_dbtable = new DBTable($db, InventoryLibrary::$_TBL_PENJUALAN_RESEP);
	$penjualan_bebas_dbtable->addCustomKriteria(" dibatalkan ", " = 0 ");
	$penjualan_bebas_dbtable->addCustomKriteria(" diretur ", " = 0 ");
	$penjualan_bebas_dbtable->addCustomKriteria(" tipe ", " = 'bebas' ");
	$penjualan_bebas_dbresponder = new PenjualanBebasDBResponder(
		$penjualan_bebas_dbtable,
		$penjualan_bebas_table,
		$penjualan_bebas_adapter
	);
	
	$super_command = new SuperCommand();
	$super_command->addResponder("penjualan_bebas", $penjualan_bebas_dbresponder);
	$init = $super_command->initialize();
	if ($init != null) {
		echo $init;
		return;
	}
	
	$retur_modal = new Modal("retur_add_form", "smis_form_container", "retur");
	$retur_modal->setTitle("Data Retur Penjualan Bebas");
	$retur_modal->setClass(Modal::$FULL_MODEL);
	$id_hidden = new Hidden("retur_id", "retur_id", "");
	$retur_modal->addElement("", $id_hidden);
	$penjualan_bebas_button = new Button("", "", "Pilih");
	$penjualan_bebas_button->setClass("btn-info");
	$penjualan_bebas_button->setIsButton(Button::$ICONIC);
	$penjualan_bebas_button->setIcon("icon-white ".Button::$icon_list_alt);
	$penjualan_bebas_button->setAction("penjualan_bebas.chooser('penjualan_bebas', 'penjualan_bebas_button', 'penjualan_bebas', penjualan_bebas)");
	$penjualan_bebas_button->setAtribute("id='penjualan_bebas_browse'");
	$penjualan_bebas_text = new Text("retur_id_penjualan_bebas", "retur_id_penjualan_bebas", "");
	$penjualan_bebas_text->setAtribute("disabled='disabled'");
	$penjualan_bebas_text->setClass("smis-one-option-input");
	$penjualan_bebas_input_group = new InputGroup("");
	$penjualan_bebas_input_group->addComponent($penjualan_bebas_text);
	$penjualan_bebas_input_group->addComponent($penjualan_bebas_button);
	$retur_modal->addElement("No. Penjualan", $penjualan_bebas_input_group);
	$dretur_table = new Table(
		array("Obat", "Tgl. Exp.", "Subtotal", "Jumlah Beli", "Jumlah Retur"),
		"",
		null,
		true
	);
	$dretur_table->setName("dretur");
	$dretur_table->setFooterVisible(false);
	$dretur_table->setAddButtonEnable(false);
	$dretur_table->setReloadButtonEnable(false);
	$dretur_table->setPrintButtonEnable(false);
	$retur_modal->addBody("dretur_table", $dretur_table);
	$retur_button = new Button("", "", "Simpan");
	$retur_button->setClass("btn-success");
	$retur_button->setIcon("fa fa-floppy-o");
	$retur_button->setIsButton(Button::$ICONIC);
	$retur_button->setAtribute("id='retur_save'");
	$retur_button->setAction("retur.save()");
	$retur_modal->addFooter($retur_button);
	
	$dretur_modal = new Modal("dretur_add_form", "smis_form_container", "dretur");
	$dretur_modal->setTitle("Data Detail Retur Penjualan Bebas");
	$id_hidden = new Hidden("dretur_id", "dretur_id", "");
	$dretur_modal->addElement("", $id_hidden);
	$nama_obat_text = new Text("dretur_nama_obat", "dretur_nama_obat", "");
	$nama_obat_text->setAtribute("disabled='disabled'");
	$dretur_modal->addElement("Obat", $nama_obat_text);
	$tanggal_exp_text = new Text("dretur_tanggal_exp", "dretur_tanggal_exp", "");
	$tanggal_exp_text->setAtribute("disabled='disabled'");
	$dretur_modal->addElement("Tgl. Exp.", $tanggal_exp_text);
	$jumlah_hidden = new Hidden("dretur_jumlah", "dretur_jumlah", "");
	$dretur_modal->addElement("", $jumlah_hidden);
	$f_jumlah_text = new Text("dretur_f_jumlah", "dretur_f_jumlah", "");
	$f_jumlah_text->setAtribute("disabled='disabled'");
	$dretur_modal->addElement("Jml. Awal", $f_jumlah_text);
	$jumlah_retur_text = new Text("dretur_jumlah_retur", "dretur_jumlah_retur", "");
	$dretur_modal->addElement("Jml. Retur", $jumlah_retur_text);
	$satuan_text = new Text("dretur_satuan", "dretur_satuan", "");
	$satuan_text->setAtribute("disabled='disabled'");
	$dretur_modal->addElement("Satuan", $satuan_text);
	$dretur_button = new Button("", "", "Simpan");
	$dretur_button->setClass("btn-success");
	$dretur_button->setIcon("fa fa-floppy-o");
	$dretur_button->setIsButton(Button::$ICONIC);
	$dretur_button->setAtribute("id='dretur_save'");
	$dretur_modal->addFooter($dretur_button);
	
	$v_retur_modal = new Modal("v_retur_add_form", "smis_form_container", "v_retur");
	$v_retur_modal->setTitle("Data Retur Penjualan Bebas");
	$v_retur_modal->setClass(Modal::$FULL_MODEL);
	$id_hidden = new Hidden("v_retur_id", "v_retur_id", "");
	$v_retur_modal->addElement("", $id_hidden);
	$penjualan_bebas_text = new Text("v_retur_id_penjualan_bebas", "v_retur_id_penjualan_bebas", "");
	$penjualan_bebas_text->setAtribute("disabled='disabled'");
	$v_retur_modal->addElement("No. Penjualan", $penjualan_bebas_text);
	$v_dretur_table = new Table(
		array("Obat", "Harga Satuan", "Jumlah Retur"),
		"",
		null,
		true
	);
	$v_dretur_table->setName("v_dretur");
	$v_dretur_table->setFooterVisible(false);
	$v_dretur_table->setAddButtonEnable(false);
	$v_dretur_table->setReloadButtonEnable(false);
	$v_dretur_table->setPrintButtonEnable(false);
	$v_retur_modal->addBody("v_dretur_table", $v_dretur_table);
	$v_retur_button = new Button("", "", "OK");
	$v_retur_button->setClass("btn-success");
	$v_retur_button->setAction("$($(this).data('target')).smodal('hide')");
	$v_retur_modal->addFooter($v_retur_button);
	
	echo $v_retur_modal->getHtml();
	echo $dretur_modal->getHtml();
	echo $retur_modal->getHtml();
	echo $retur_penjualan_bebas_table->getHtml();
	echo addJS("framework/smis/js/table_action.js");
	echo addJS("depo_farmasi_irna/js/retur_penjualan_bebas_action.js", false);
	echo addJS("depo_farmasi_irna/js/dretur_penjualan_bebas_action.js", false);
	echo addJS("depo_farmasi_irna/js/retur_penjualan_bebas_penjualan_bebas_action.js", false);
	echo addJS("depo_farmasi_irna/js/retur_penjualan_bebas.js", false);
	echo addCSS("depo_farmasi_irna/css/retur_penjualan_bebas.css", false);
?>