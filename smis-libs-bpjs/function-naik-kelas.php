<?php

function get_topup_class_array(){
    $list        = array();
    $list["32"]  = "Kelas 3 ke Kelas 2";
    $list["31"]  = "Kelas 3 ke Kelas 1";
    $list["21"]  = "Kelas 2 ke Kelas 1";
    $list["32t"] = "Kelas 3 ke Kelas 2 - Tampilkan";
    $list["31t"] = "Kelas 3 ke Kelas 1 - Tampilkan";
    $list["21t"] = "Kelas 2 ke Kelas 1 - Tampilkan";
    $list["v1"]  = "Kelas 1 ke Kelas VIP";
    $list["v2"]  = "Kelas 2 ke Kelas VIP";
    $list["v3"]  = "Kelas 3 ke Kelas VIP";
    return $list;
}

function get_topup_class_select(){
    $naik_kelas=new OptionBuilder();
    $naik_kelas->add("","","1");
    $list = get_topup_class_array();
    foreach($list as $code=>$value){
        $naik_kelas->add($value,$code,"0");
    }
    return $naik_kelas->getContent();
}

function select_topup_class($code){
    $list = get_topup_class_array();
    return $list[$code];
}

?>


