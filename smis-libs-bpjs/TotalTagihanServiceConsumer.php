<?php

class TotalTagihanServiceConsumer extends ServiceConsumer{
	
	public function __construct($db,$noreg){
		parent::__construct($db,"get_sum_tagihan_pasien");
		$this->addData("noreg_pasien", $noreg);
	}
	
	public function getContent(){
		$content=0;
		$result=json_decode($this->result,true);
		
		foreach($result as $autonomous_name=>$autonomous){
			foreach($autonomous as $entity_name=>$entity){
				$num=$entity==NULL?0:$entity*1;
				$content+=$num;
			}
		}
		return $content;
	}
} 

?>