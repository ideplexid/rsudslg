<?php

/**
 * masih belum jadi nunggu buatkan service */

global $db;
require_once("smis-base/smis-include-service-consumer.php");
require_once 'smis-libs-class/MasterTemplate.php';
require_once 'smis-libs-class/MasterServiceTemplate.php';
require_once 'smis-libs-class/MasterSlaveServiceTemplate.php';

$button=new Button("","","");
$button->addClass("btn-primary");
$button->setIsButton(Button::$ICONIC);
$button->setIcon("fa fa-print");

$init=new Button("","","");
$init->addClass("btn-primary");
$init->setIsButton(Button::$ICONIC);
$init->setIcon("fa fa-refresh");

$koreksi_carabayar=new Button("","","");
$koreksi_carabayar->addClass("btn-primary");
$koreksi_carabayar->setIsButton(Button::$ICONIC);
$koreksi_carabayar->setIcon("fa fa-upload");

$header=array ("No.","No Reg","NRM",'Nama',"Tanggal","Jenis","Status","Layanan","Kamar","Cara Bayar","Asuransi",'No. BPJS',"Plafon","Tagihan");
$plafon=new MasterSlaveServiceTemplate($db, "get_registered_all", "bpjs", "rekap_tagihan_bpjs");
$plafon->setEntity("registration");
$plafon->setDateEnable(true);
if( isset($_POST['command']) && $_POST['command']=="list" ){
	$plafon->getServiceResponder()->addData("noreg_pasien",$_POST['noreg_pasien']);	
	$plafon->getServiceResponder()->addData("nrm_pasien",$_POST['nrm_pasien']);	
	$plafon->getServiceResponder()->addData("jenis_pasien",$_POST['jenis_pasien']);		
}
$plafon->addResouce("js","bpjs/resource/js/rekap_tagihan_bpjs.js");
$plafon->addResouce("js","base-js/smis-base-loading.js","before",true);

$adapt=new SummaryAdapter();
$adapt	->setUseNumber(true, "No.","back.")
		->add("No Reg", "id","only-digit9")
		->add("NRM", "nrm","only-digit8")
		->add("Nama", "nama_pasien")
		->add("No. BPJS","nobpjs")
		->add("Cara Bayar","carabayar","unslug")
		->add("Layanan","jenislayanan","unslug")
		->add("Plafon","plafon_bpjs","money Rp.")
		->add("Tanggal", "tanggal","date d M Y H:i")
		->add("Kamar", "kamar_inap","unslug")
		->add("Jenis", "uri","trivial_0_URJ_URI")
		->add("Asuransi", "nama_asuransi")
		->add("Status", "selesai","trivial_0_Berlangsung_Selesai")
		->add("Tagihan", "total_tagihan","money Rp.");
$adapt->addSummary("Tagihan","total_tagihan","money Rp.");
$adapt->addFixValue("Tanggal","<strong>Total</strong>");
$plafon->setAdapter($adapt);
$plafon ->getUItable()
		->addContentButton("print_tagihan_pasien",$button)
		->addContentButton("init_tagihan",$init)
		//->addContentButton("init_koreksi_carabayar",$koreksi_carabayar)
		->setReloadButtonEnable(false)
		->setDelButtonEnable(false)
		->setAddButtonEnable(false)
		->setEditButtonEnable(false)
		->setHeader($header);



if(!isset($_POST['command'])){
	
	$service=new ServiceConsumer($db,"get_format_kasir",NULL,"kasir");
	$service->execute();
	$mode_kwitansi=$service->getContent();

	$service=new ServiceConsumer($db,"get_jenis_patient",NULL,"registration");
	$service->execute();
	$jenis_patient=$service->getContent();
	$jenis_patient[]=array("name"=>"","value"=>"","default"=>"1");
	
	$plafon ->getUItable()
			->addModal("nrm_pasien", "text", "NRM", "","y")
			->addModal("noreg_pasien", "text", "No. Reg", "","y")
			->addModal("jenis_pasien", "select", "Cara Bayar", $jenis_patient,"y")
			->addModal("kwitansi", "select", "Kwitansi", $mode_kwitansi,"y");
	$btn=new Button("","","View");
	$btn->addClass("btn-primary");
	$btn->setIcon("fa fa-search");
	$btn->setAction("rekap_tagihan_bpjs.view()");
	$btn->setIsButton(Button::$ICONIC_TEXT);

	$all_init=new Button("","","Process All");
	$all_init->addClass("btn-primary");
	$all_init->setIsButton(Button::$ICONIC_TEXT);
	$all_init->setIcon("fa fa-forward");
	$all_init->setAction("rekap_tagihan_bpjs.init_all_tagihan_bpjs()");

	$all_print=new Button("","","Print All");
	$all_print->addClass("btn-primary");
	$all_print->setIsButton(Button::$ICONIC_TEXT);
	$all_print->setIcon("fa fa-print");
	$all_print->setAction("rekap_tagihan_bpjs.init_all_print()");
	
	$all_koreksi=new Button("","","");
	$all_koreksi->addClass("btn-primary");
	$all_koreksi->setIsButton(Button::$ICONIC);
	$all_koreksi->setIcon("fa fa-upload");
	$all_koreksi->setAction("rekap_tagihan_bpjs.init_all_koreksi_carabayar()");
	
	$service=new ServiceConsumer($db,"get_jenis_patient",NULL,"registration");
	$service->execute();
	$jenis_patient=$service->getContent();
	$jenis_patient[]=array("name"=>"","value"=>"","default"=>"1");
	

	$plafon	->getForm()
			->addElement("",$btn)
			->addElement("",$all_init)
			->addElement("",$all_print);
			//->addElement("",$all_koreksi);

	$plafon->getUItable()->clearContent();
	$plafon->addJSColumn("nama_pasien");
	$plafon->addJSColumn("nama_dokter");
	$plafon->addJSColumn("id");
	$plafon->addJSColumn("nrm");
	$plafon->addJSColumn("nobpjs");
	$plafon->addJSColumn("plafon_bpjs");
	$plafon->addJSColumn("diagnosa_bpjs");
	$plafon->addJSColumn("tindakan_bpjs");
	$plafon->addJSColumn("keterangan_bpjs");
	$plafon->addJSColumn("kunci_bpjs");
	$plafon->getModal()->setTitle("Plafon");
	$plafon->addViewData("noreg_pasien","noreg_pasien");
	$plafon->addViewData("nrm_pasien","nrm_pasien");
	$plafon->addViewData("jenis_pasien","jenis_pasien");
	
	
	$close=new Button("", "", "Batal");
	$close	->addClass("btn-primary")
			->setIsButton(Button::$ICONIC_TEXT)
			->setIcon("fa fa-close")
			->setAction("location.reload()");
	$person=new LoadingBar("rtb_person_bar", "");
	$modal=new Modal("rtb_load_modal", "", "Process in Progress");
	$modal	->addHTML($person->getHtml(),"after")
			->addFooter($close);
	echo "<div id='tagihan_place_init'></div>";
	echo $modal->getHtml();
}

$plafon->initialize();
?>

