<?php

global $db;

$header = array("No.", "Tgl. Masuk", "Tgl. Pulang", "No.RM", "Nama Pasien", "No.SEP", "INACBG", "Total Tarif", "Tarif RS", "Jenis");

$uitable = new Table($header);
$uitable->setAction(false);
$uitable->setName("excess_tarif");

if(isset($_POST['command'])) {
    require_once 'bpjs/class/adapter/ExcessTarifAdapter.php';
    $adapter = new ExcessTarifAdapter();
    
    $dbtable = new DBTable($db, "smis_rgv_layananpasien");
    $dbtable->setOrder("tanggal_pulang ASC");
    $dbtable->addCustomKriteria(NULL, "prop != 'del'");
    
    require_once 'bpjs/class/responder/ExcessTarifResponder.php';
    $dbres=new ExcessTarifResponder($dbtable, $uitable, $adapter);
    
	if(isset($_POST['dari']) && isset($_POST['sampai'])){
		$dbtable->addCustomKriteria(NULL, "tanggal_pulang >='".$_POST['dari']."'");
		$dbtable->addCustomKriteria(NULL, "tanggal_pulang <='".$_POST['sampai']."'");
	}
    
	$data=$dbres->command($_POST['command']);
	echo json_encode($data);
	return;
}

$load=new Button("", "", "");
$load->setClass("btn-primary");
$load->setIsButton(Button::$ICONIC);
$load->setIcon("fa fa-refresh");
$load->setAction("excess_tarif.view()");

$print=new Button("", "", "");
$print->setClass("btn-primary");
$print->setIsButton(Button::$ICONIC);
$print->setIcon("fa fa-print");
$print->setAction("excess_tarif.print()");

$excel=new Button("", "", "");
$excel->setClass("btn-primary");
$excel->setIsButton(Button::$ICONIC);
$excel->setIcon("fa fa-file-excel-o");
$excel->setAction("excess_tarif.excel()");

$btgrup=new ButtonGroup("");
$btgrup->addButton($load)
	   ->addButton($print)
	   ->addButton($excel);

$uitable->addModal("dari", "date", "Dari", "");
$uitable->addModal("sampai", "date", "Sampai", "");
$form=$uitable->getModal()->getForm();
$form->addElement("", $btgrup);

echo $form->getHtml();
echo $uitable->getHtml();

echo addJS ( "framework/bootstrap/js/bootstrap-datepicker.js" );
echo addJS ( "framework/smis/js/table_action.js" );
echo addCSS ( "framework/bootstrap/css/datepicker.css" );
echo addJS ( "bpjs/js/excess_tarif.js" ,false);

?>