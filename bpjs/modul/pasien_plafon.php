<?php
    $tabs = new Tabulator("pasien_plafon", "",Tabulator::$POTRAIT);
    $tabs->add("pasien_bpjs", "Pemberian Plafon", "",Tabulator::$TYPE_HTML,"fa fa-users");
    $tabs->add("tinjauan_plafon", "Permintaan Peninjauan Plafon", "",Tabulator::$TYPE_HTML,"fa fa-comments");
    $tabs->setPartialLoad(true,"bpjs","pasien_plafon","pasien_plafon",true);
    echo $tabs->getHtml();
?>