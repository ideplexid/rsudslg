<?php
	global $NAVIGATOR;

	// Administrator Top Menu
	$mr = new Menu ("fa fa-university");
	$mr->addProperty ( 'title', 'Plafon' );
	$mr->addProperty ( 'name', 'Plafon' );

	$mr->addSubMenu ( "Pasien", "bpjs", "pasien_plafon", "Pasien & Plafon" ,"fa fa-users");
	$mr->addSubMenu ( "Rencana", "bpjs", "rencana_bpjs", "Rencana" ,"fa fa-user-md");
	$mr->addSubMenu ( "Rekap Tagihan", "bpjs", "rekap_tagihan_bpjs", "Rekap Tagihan" ,"fa fa-user-md");
    $mr->addSubMenu ( "Excess Tarif", "bpjs", "excess_tarif", "Excess Tarif" ,"fa fa-usd");
	$NAVIGATOR->addMenu ( $mr, 'bpjs' );
?>
