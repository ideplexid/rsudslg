<?php
	global $db;
	global $user;
	require_once 'smis-libs-class/Policy.php';
	require_once 'smis-libs-inventory/policy.php';
	$inventory_policy = new InventoryPolicy("gizi", $user);
	
	$policy=new Policy("bpjs", $user);
	$policy->setDefaultPolicy(Policy::$DEFAULT_POLICY_RESTRICT);
	$policy->addPolicy("pasien_plafon","pasien_plafon",Policy::$DEFAULT_COOKIE_CHANGE,"modul/pasien_plafon");
	$policy->addPolicy("pasien_bpjs","pasien_plafon",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/pasien/pasien_bpjs");
	$policy->addPolicy("tinjauan_plafon","pasien_plafon",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/pasien/tinjauan_plafon");
	
	$policy->addPolicy("rencana_bpjs","rencana_bpjs",Policy::$DEFAULT_COOKIE_CHANGE,"modul/rencana_bpjs");
	$policy->addPolicy("rekap_tagihan_bpjs","rekap_tagihan_bpjs",Policy::$DEFAULT_COOKIE_CHANGE,"modul/rekap_tagihan_bpjs");
	$policy->addPolicy("total_tagihan_kasir","rekap_tagihan_bpjs",Policy::$DEFAULT_COOKIE_KEEP,"modul/total_tagihan_kasir");
	$policy->addPolicy("excess_tarif","excess_tarif",Policy::$DEFAULT_COOKIE_KEEP,"modul/excess_tarif");
	$policy->initialize();
?>