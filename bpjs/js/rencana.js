$(document).ready(function(){
	
	pasien_bpjs.load_rencana=function(id){
		var edit=this.getEditData();
		edit["id"]=id;
		showLoading();
		$.post("",edit,function(res){
			var json=getContent(res);
			var data={
					page:"bpjs",
					action:"rencana_bpjs",
					prototype_implement:"",
					prototype_slug:"",
					prototype_name:"",	
					nrm_pasien:json.nrm,
					nama_pasien:json.nama_pasien,
					noreg_pasien:json.id,			
			}
			LoadSmisPage(data);
			dismissLoading();				
		});
	};
	
	$("#pasien_bpjs_naik_kelas_bpjs").on("change",function(res){
		var nilai=$("#pasien_bpjs_naik_kelas_bpjs").val();
		if(nilai=="v1" || nilai=="v2" || nilai=="v3"){
			$("#pasien_bpjs_plafon_naik_kelas").prop( "disabled", true );				
		}else{
			$("#pasien_bpjs_plafon_naik_kelas").prop( "disabled", false );
		}
	});
	
	
	
});
