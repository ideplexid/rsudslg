var REKAP_CUR_NUMBER=0;
var REKAP_TOT_NUMBER=0;
var dump_reinit=function(){
	rekap_tagihan_bpjs.view();
};
var _full_print="";	
var loop_init_all_tagihan_bpjs=function(){rekap_tagihan_bpjs.loop_init_all_tagihan_bpjs(); };
$(document).ready(function(){
	rekap_tagihan_bpjs.print_tagihan_bpjs=function(id){
		var data=this.getRegulerData();
		data['action']="total_tagihan_kasir";
		data['noreg_pasien']=id;
		data['command']="show_kwitansi";
		data['file_mode']=$("#rekap_tagihan_bpjs_kwitansi").val();
		showLoading();
		$.post("",data,function(res){
			dismissLoading();
			smis_print(res);	
		});
	};
	
	rekap_tagihan_bpjs.init_tagihan=function(id){
		var data=this.getRegulerData();
		data['action']="total_tagihan_kasir";
		data['noreg_pasien']=id;
		data['invoke']="dump_reinit";
		showLoading();
		$.post("",data,function(res){
			$("#tagihan_place_init").html(res);
			dismissLoading();
		});
	};
	
	rekap_tagihan_bpjs.loop_print=function(total,number){
		if(number>=total){
			$("#rtb_load_modal").modal("hide");
			smis_print(_full_print);
			return ;
		}
		var id=$("#rekap_tagihan_bpjs_list").children().eq(number).children().eq(1).html();
		var nama=$("#rekap_tagihan_bpjs_list").children().eq(number).children().eq(3).html();
		var data=this.getRegulerData();
		data['command']="show_kwitansi";
		data['action']="total_tagihan_kasir";
		data['file_mode']=$("#rekap_tagihan_bpjs_kwitansi").val();
		data['noreg_pasien']=id;
		$.post("",data,function(res){
			$("#rtb_person_bar").sload("true","Loading..."+nama+" [ "+(number+1)+" / "+total+" ]",(number)*100/total);
			_full_print+=("<div class='pagebreak'></div>"+res);
			rekap_tagihan_bpjs.loop_print(total,number+1)
		});
	};
	
	rekap_tagihan_bpjs.init_all_print=function(id){
		_full_print="";
		var total=$("#rekap_tagihan_bpjs_list").children().length-1;		
		$("#rtb_load_modal").modal("show");
		rekap_tagihan_bpjs.loop_print(total,0);
	};
	
	rekap_tagihan_bpjs.init_all_tagihan_bpjs=function(){
		REKAP_TOT_NUMBER=$("#rekap_tagihan_bpjs_list").children().length-1;
		REKAP_CUR_NUMBER=0;
		this.loop_init_all_tagihan_bpjs();
	}
	
	rekap_tagihan_bpjs.loop_init_all_tagihan_bpjs=function(){
		if(REKAP_CUR_NUMBER>=REKAP_TOT_NUMBER){
			rekap_tagihan_bpjs.view();
			return;	
		}
		var id=$("#rekap_tagihan_bpjs_list").children().eq(REKAP_CUR_NUMBER).children().eq(1).html();
		var nama=$("#rekap_tagihan_bpjs_list").children().eq(REKAP_CUR_NUMBER).children().eq(3).html();
		var title="<small>Processing..."+nama+" [ "+(REKAP_CUR_NUMBER+1)+" / "+REKAP_TOT_NUMBER+" ]</small>";
		REKAP_CUR_NUMBER++;
		var data=this.getRegulerData();
		data['action']="total_tagihan_kasir";
		data['noreg_pasien']=Number(id);
		data['invoke']="loop_init_all_tagihan_bpjs";
		data['title']=title;
		showLoading();
		$.post("",data,function(res){
			$("#tagihan_place_init").html(res);
			dismissLoading();
		});
	}
	
});