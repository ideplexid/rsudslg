$(document).ready(function(){
    tinjauan_plafon.komentar = function(id){
        var data = this.getEditData(id);
        showLoading();
        $.post("",data,function(res){
            var json = getContent(res);
            if(json!=null){
                var noreg    = json.noreg_pasien;
                var datasave = tinjauan_plafon.getRegulerData();
                datasave['id']      = id;
                datasave['command'] = "save";
                datasave['status']  = "1";
                showLoading();
                $.post("",datasave,function(res){
                     var result = getContent(res);
                     $("#pasien_bpjs_anchor").trigger("click");
                     pasien_bpjs.edit(noreg);
                     dismissLoading(); 
                });
            }
            dismissLoading();
        });
    };
});
