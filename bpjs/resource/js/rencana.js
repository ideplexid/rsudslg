$(document).ready(function(){
	
	pasien_bpjs.load_rencana=function(id){
		var edit=this.getEditData();
		edit["id"]=id;
		showLoading();
		$.post("",edit,function(res){
			var json=getContent(res);
			var data={
					page:"bpjs",
					action:"rencana_bpjs",
					prototype_implement:"",
					prototype_slug:"",
					prototype_name:"",	
					nrm_pasien:json.nrm,
					nama_pasien:json.nama_pasien,
					noreg_pasien:json.id,			
			}
			LoadSmisPage(data);
			dismissLoading();				
		});
	};
    
    
    pasien_bpjs.load_diagnosa=function(id){
		var edit=this.getRegulerData();
		edit["id"]=id;
        edit["command"]="load_diagnosa";
        
		showLoading();
		$.post("",edit,function(res){
			var json=getContent(res);
			dismissLoading();				
		});
	};
    
    
    pasien_bpjs.load_dpjp=function(id){
		var edit=this.getRegulerData();
		edit["id"]=id;
        edit["command"]="edit";        
		showLoading();
		$.post("",edit,function(res){
			var json=getContent(res);
            try{
                var history=$.parseJSON(json.history_dokter);
                var table="";
                table+="<table class='table'>";
                    table+="<tr>";
                        table+="<th>No.</th>";
                        table+="<th>Nama</th>";
                        table+="<th>Waktu</th>";
                    table+="</tr>";
                var number=0;
                $.each(history,function(key,value){
                    if( value['nama_dokter']==null )
                        return true;
                    number++;
                    table+="<tr>";
                        table+="<td>"+number+".</td>";
                        table+="<td>"+value['nama_dokter']+"</td>";
                        table+="<td>"+getFormattedTime(value['waktu'])+"</td>";
                    table+="</tr>";
                });
                table+="</table>";
                
                
                showWarning("History Dokter",table);
            }catch(e){
                showWarning("History Dokter","History Dokter Tidak Ditemukan");
            }
            
			dismissLoading();				
		});
	};
	
	$("#pasien_bpjs_naik_kelas_bpjs").on("change",function(res){
		var nilai=$("#pasien_bpjs_naik_kelas_bpjs").val();
		if(nilai=="v1" || nilai=="v2" || nilai=="v3"){
			$("#pasien_bpjs_plafon_naik_kelas").prop( "disabled", true );				
		}else{
			$("#pasien_bpjs_plafon_naik_kelas").prop( "disabled", false );
		}
	});
	
	
	
});
