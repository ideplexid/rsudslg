<?php
    global $wpdb;
    require_once "smis-libs-class/DBCreator.php";
    $dbcreator = new DBCreator($wpdb,"smis_plf_request");
    $dbcreator->addColumn("nama_pasien", "varchar(128)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
    $dbcreator->addColumn("nrm_pasien", "varchar(32)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
    $dbcreator->addColumn("noreg_pasien", "varchar(32)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
    $dbcreator->addColumn("keterangan", "varchar(64)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
    $dbcreator->addColumn("ruangan", "varchar(64)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
    $dbcreator->addColumn("total_tagihan", "int(11)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
    $dbcreator->addColumn("plafon", "int(11)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
    $dbcreator->addColumn("waktu", "datetime", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_CURRENT_TIMESTAMP , false,DBCreator::$ON_UPDATE_NONE, "");
    $dbcreator->addColumn("petugas", "varchar(64)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE , false,DBCreator::$ON_UPDATE_NONE, "");
    $dbcreator->addColumn("status", "tinyint(1)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE , false,DBCreator::$ON_UPDATE_NONE, "");
    $dbcreator->setDuplicate(false);
    $dbcreator->initialize();
?>