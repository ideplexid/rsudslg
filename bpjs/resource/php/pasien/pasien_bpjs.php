<?php
global $db;
require_once 'smis-libs-hrd/EmployeeResponder.php';
require_once "smis-base/smis-include-service-consumer.php";
require_once 'smis-libs-class/MasterTemplate.php';
require_once 'smis-libs-class/MasterServiceTemplate.php';
require_once 'smis-libs-class/MasterSlaveServiceTemplate.php';

$dokter_adapter = new SimpleAdapter();
$dokter_adapter->add("Jabatan", "nama_jabatan");
$dokter_adapter->add("Nama", "nama");
$dokter_adapter->add("NIP", "nip");
$header = array("Nama", "Jabatan", "NIP");
$dokter_table = new Table($header);
$dokter_table->setName("dokter");
$dokter_table->setModel(Table::$SELECT);
$dokter_responder = new EmployeeResponder($db, $dokter_table, $dokter_adapter, "dokter");
$button=new Button("","","");
$button->addClass("btn-success");
$button->setIsButton(Button::$ICONIC);
$button->setIcon("fa fa-money");
$diagnosa=new Button("","","");
$diagnosa->addClass("btn-danger");
$diagnosa->setIsButton(Button::$ICONIC);
$diagnosa->setIcon("fa fa-bed");
$dpjp=new Button("","","");
$dpjp->addClass("btn-primary");
$dpjp->setIsButton(Button::$ICONIC);
$dpjp->setIcon("fa fa-user-md");

$header=array ("No.","No Reg","NRM",'Nama',"Cara Bayar","Diagnosa","DPJP",'BPJS',"Plafon","Kelas BPJS","Total","Tanggal","Jenis","Layanan","Kamar","Proses","Status");
$plafon=new MasterSlaveServiceTemplate($db, "get_registration_all", "bpjs", "pasien_bpjs");
$plafon->getUItable()->setHeader($header);
$plafon->setEntity("registration");
$plafon->addResouce("js","bpjs/resource/js/rencana.js");
$plafon->getAdapter()->setUseNumber(true, "No.","back.")
					 ->add("No Reg", "id","only-digit9")
					 ->add("NRM", "nrm","only-digit8")
					 ->add("Nama", "nama_pasien")
					 ->add("BPJS","nobpjs")
                     ->add("DPJP","nama_dokter")
                     ->add("Diagnosa","diagnosa_bpjs")
                     ->add("Kamar", "kamar_inap","unslug")
                     ->add("Jenis", "uri","trivial_0_URJ_URI")
                     ->add("Layanan","jenislayanan","unslug")
                     ->add("Cara Bayar","carabayar","unslug")
					 ->add("Plafon","plafon_bpjs","money Rp.")
                     ->add("Total","total_tagihan","money Rp.")
					 ->add("Status","kunci_bpjs","trivial_0_<i class='fa fa-unlock fa-2x' style=\"color: rgb(218, 79, 73);\"  ></i>_<i class='fa fa-lock fa-2x' style=\"color:rgb(71, 99, 158);\"></i>")
					 ->add("Tanggal", "tanggal","date d M Y H:i")
                     ->add("Proses", "selesai","trivial_0_Berlangsung_Selesai")
                     ->add("Kamar", "kamar_inap","unslug")
                     ->add("Kelas BPJS", "kelas_bpjs");
					 

if(isset($_POST['command']) && $_POST['command']=="load_diagnosa"){
    require_once "bpjs/class/responder/RencanaResponder.php";
    $responder=new RencanaResponder($plafon->getDBtable(),$plafon->getUItable(),$plafon->getAdapter());
    $plafon->setDBresponder($responder);
}
$plafon ->setDateEnable(true);	

if(!isset($_POST['command'])){  
    require_once "smis-libs-bpjs/function-naik-kelas.php";  
    $uri=new OptionBuilder();
	$uri->add("","","1");
	$uri->add("URI","1","0");
	$uri->add("URJ","0","0");
	$urjip=new ServiceConsumer($db, "get_urjip",array());
	$urjip->setMode(ServiceConsumer::$MULTIPLE_MODE);
    $urjip->setCached(true,"get_urjip");
	$urjip->execute();
	$content=$urjip->getContent();
	$ruangan=array();
	$ruangan_inap=array();
	foreach ($content as $autonomous=>$ruang){
		foreach($ruang as $nama_ruang=>$jip){
			$option=array();
			$option['value']=$nama_ruang;
			$option['name']=ArrayAdapter::format("unslug", $nama_ruang);
			
			if($jip[$nama_ruang]=="URI"){
				$ruangan_inap[]=$option;
			}else{
				$ruangan[]=$option;
			}
		}
	}	
	$ruangan[]=array("nama"=>"","value"=>"","default"=>"1");
	$ruangan_inap[]=array("nama"=>"","value"=>"","default"=>"1");
	$selesai=new OptionBuilder();
	$selesai->add("","","1");
	$selesai->add("Selesai","1","0");
	$selesai->add("Belangsung","0","0");
	$service=new ServiceConsumer($db,"get_format_kasir",NULL,"kasir");
	$service->execute();
	$mode_kwitansi=$service->getContent();
	$service=new ServiceConsumer($db,"get_jenis_patient",NULL,"registration");
	$service->execute();
	$jenis_patient=$service->getContent();
	$jenis_patient[]=array("name"=>"","value"=>"","default"=>"1");
    $plafon ->getUItable()
            ->addContentButton("load_rencana",$button)
            ->addContentButton("load_diagnosa",$diagnosa)
            ->addContentButton("load_dpjp",$dpjp)
            ->addModal("dari_search", "date", "Dari", "")
            ->addModal("sampai_search", "date", "Sampai", "")
            ->addModal("nama_search", "text", "Nama", "")
            ->addModal("nrm_search", "text", "NRM", "")
            ->addModal("noreg_search", "text", "No. Reg", "")
            ->addModal("selesai", "select", "Proses", $selesai->getContent(),"y")
            ->addModal("uri", "select", "Jenis", $uri->getContent(),"y")
            ->addModal("jenislayanan", "select", "Layanan", $ruangan,"y")
            ->addModal("kamar_inap", "select", "Kamar Inap", $ruangan_inap,"y")
            ->addModal("jenis_pasien", "select", "Cara Bayar", $jenis_patient,"y");
    $form=$plafon ->getForm();
    $button=new Button("","","");
    $button->setClass(" btn btn-primary ");
    $button->setIsButton(Button::$ICONIC);
    $button->setIcon(" fa fa-search");
    $button->setAction("pasien_bpjs.view()");
    $form->addElement("",$button);    
    $plafon ->getUItable()
            ->setDelButtonEnable(false)
            ->setAddButtonEnable(false)
            ->addModal("id", "hidden", "", "")
            ->addModal("nama_pasien", "text", "Pasien", "","n",NULL,true)
            ->addModal("nama_dokter", "chooser-pasien_bpjs-dokter", "Dokter", "","y",NULL,true)
            ->addModal("nrm", "text", "NRM", "","n",NULL,true)
            ->addModal("nobpjs", "text", "No BPJS", "","y",NULL,false)
            ->addModal("diagnosa_bpjs", "text", "Diagnosa", "","y",NULL,false,NULL,true)
            ->addModal("tindakan_bpjs", "text", "Tindakan", "")
            ->addModal("kunci_bpjs", "checkbox", "Kunci Rencana", "0")
            ->addModal("plafon_bpjs", "money", "Plafon", "0")
            ->addModal("total_tagihan", "money", "Total", "0")
            ->addModal("keterangan_bpjs", "textarea", "Keterangan", "")		
            ->addModal("inacbg_bpjs", "text", "INA-CBGs", "")
            ->addModal("deskripsi_bpjs", "textarea", "Deskripsi INA-CBGs", "")
            ->addModal("kelas_bpjs", "text", "Kelas BPJS", "","y",null,true)
            ->addModal("naik_kelas_bpjs", "select", "Naik Kelas", get_topup_class_select())
            ->addModal("plafon_naik_kelas", "money", "Plafon Naik Kelas", "");
    $plafon->addJSColumn("nama_pasien");
    $plafon->addJSColumn("nama_dokter");
    $plafon->addJSColumn("id");
    $plafon->addJSColumn("nrm");
    $plafon->addJSColumn("nobpjs");
    $plafon->addJSColumn("plafon_bpjs");
    $plafon->addJSColumn("diagnosa_bpjs");
    $plafon->addJSColumn("tindakan_bpjs");
    $plafon->addJSColumn("keterangan_bpjs");
    $plafon->addJSColumn("kelas_bpjs");
    $plafon->addJSColumn("kunci_bpjs");
    $plafon->addJSColumn("inacbg_bpjs");
    $plafon->addJSColumn("deskripsi_bpjs");
    $plafon->addJSColumn("naik_kelas_bpjs");
    $plafon->addJSColumn("plafon_naik_kelas");
    $plafon->addSuperCommand("dokter", array());
    $plafon->addSuperCommandArray("dokter", "nama_dokter", "nama");
    $plafon->getModal()
            ->setComponentSize(Modal::$MEDIUM)
            ->setTitle("Plafon");
    $plafon->addViewData("nama","nama_search");
    $plafon->addViewData("nrm","nrm_search");
    $plafon->addViewData("noreg","noreg_search");
    $plafon->addViewData("dari","dari_search");
    $plafon->addViewData("sampai","sampai_search");
    $plafon->addViewData("uri","uri");
	$plafon->addViewData("selesai","selesai");
    $plafon->addViewData("carabayar","jenis_pasien");
    $plafon->addViewData("kamar_inap","kamar_inap");
}
$plafon->getSuperCommand()->addResponder("dokter", $dokter_responder);
$plafon->initialize();
		
?>