<?php
require_once "smis-libs-class/MasterTemplate.php";

global $db;
$comments = new Button("","","Tanggapi");
$comments ->setClass("btn btn-success")
          ->setIcon(" fa fa-comments")
          ->setIsButton(Button::$ICONIC_TEXT);
$plafon   = new MasterTemplate($db,"smis_plf_request","bpjs","tinjauan_plafon");
$plafon   ->getUItable()
          ->setHeader(array("Tanggal","Nama","NRM","No. Reg","Keterangan","Ruangan","Total","Plafon","Petugas","Status"))
          ->setAddButtonEnable(false)
          ->setDelButtonEnable(true)
          ->setEditButtonEnable(false)
          ->setPrintButtonEnable(false)
          ->setReloadButtonEnable(false)
          ->addContentButton("komentar",$comments);
        
$adapter  = $plafon ->getAdapter();
$adapter  ->setUseNumber(true,"No.","back.")
          ->add("Tanggal","waktu","date d M Y H:i")
          ->add("Nama","nama_pasien")
          ->add("NRM","nrm_pasien")
          ->add("No. Reg","noreg_pasien")
          ->add("Keterangan","keterangan")
          ->add("Ruangan","ruangan")
          ->add("Total","total_tagihan","money Rp.")
          ->add("Plafon","plafon","money Rp.")
          ->add("Petugas","petugas")
          ->add("Status","status","trivial_1_<i class='fa fa-check'></i>_");
$plafon->addResouce("js","bpjs/resource/js/tinjauan_plafon.js"); 
$plafon->initialize();
?>