<?php 

require_once "smis-framework/smis/template/ModulTemplate.php";
require_once "smis-base/smis-include-service-consumer.php";
class RencanaBPJS extends ModulTemplate {
	private $service;
	private $db;
	private $nama;
	private $nrm;
	private $noreg;
	public function __construct($db,$nama,$noreg,$nrm) {
		parent::__construct ();
		$this->db 		= $db;
		$this->nama		= $nama;
		$this->noreg	= $noreg;
		$this->nrm		= $nrm;
	}
	public function initialize() {
		$data 							= $_POST;
		$data['prototype_slug'] 		= "";
		$data['prototype_name'] 		= "";
		$data['prototype_implement'] 	= "";
		$data['noreg_pasien'] 			= $this->noreg;
		$data['nama_pasien'] 			= $this->nama;
		$data['nrm_pasien'] 			= $this->nrm;
		$data['action'] 				= "rencana_bpjs";
		$data['page'] 					= "bpjs";
		$service 						= new ServiceConsumer ( $this->db, "get_rencana_bpjs", $data, "medical_record" );
		$service->execute();
		echo $service->getContent();
	}
}
?>