<?php

class ExcessTarifResponder extends DBResponder {
    public function excel() {
        $this->dbtable->setShowAll(true);
        $data = $this->dbtable->view("",0);
        $fix = $this->adapter->getContent($data['data']);
        
        require_once ("smis-libs-out/php-excel/PHPExcel.php");
        $file = new PHPExcel ();
        $file->getProperties ()->setCreator ( "SIMRS Inovasi Ide Utama" );
        $file->getProperties ()->setLastModifiedBy ( "SIMRS Bagian Administrator" );
        $file->getProperties ()->setTitle ( "Excess Tarif" );
        $file->getProperties ()->setSubject ( "Excess Tarif" );
        $file->getProperties ()->setDescription ( "Excess Tarif Generated From SIMRS" );
        $file->getProperties ()->setKeywords ( "Excess Tarif" );
        $file->getProperties ()->setCategory ( "Excess Tarif" );
        $sheet = $file->getActiveSheet ();
        
        $row = 1;
        $sheet->setCellValue("A".$row, "LAPORAN REKAP KLAIM");
        $sheet->mergeCells("A".$row.":J".$row);
        $sheet->getStyle('A'.$row.':'.'J'.$row)->getFont()->setBold(true);
        $sheet->getStyle('A'.$row.':'.'J'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $row++;
        
        $sheet->setCellValue("A".$row, strtoupper(date("d F Y",strtotime($_POST['dari'])))." - ".strtoupper(date("d F Y",strtotime($_POST['sampai']))));
        $sheet->mergeCells("A".$row.":J".$row);
        $sheet->getStyle('A'.$row.':'.'J'.$row)->getFont()->setBold(true);
        $sheet->getStyle('A'.$row.':'.'J'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $row += 2;
        
        $sheet->setCellValue("A".$row, "No.");
        $sheet->setCellValue("B".$row, "Tgl. Masuk");
        $sheet->setCellValue("C".$row, "Tgl. Pulang");
        $sheet->setCellValue("D".$row, "No.RM");
        $sheet->setCellValue("E".$row, "Nama Pasien");
        $sheet->setCellValue("F".$row, "NO.SEP");
        $sheet->setCellValue("G".$row, "INACBG");
        $sheet->setCellValue("H".$row, "Total Tarif");
        $sheet->setCellValue("I".$row, "Tarif RS");
        $sheet->setCellValue("J".$row, "Jenis");
        $sheet->getStyle('A'.$row.':'.'J'.$row)->getFont()->setBold(true);
        $sheet->getStyle('A'.$row.':'.'J'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $row++;
        
        $no = 1;
        for($i = 0; $i < sizeof($fix); $i++) {
            $sheet->setCellValue("A".$row, $no);
            $sheet->setCellValue("B".$row, $fix[$i]["Tgl. Masuk"]);
            $sheet->setCellValue("C".$row, $fix[$i]["Tgl. Pulang"]);
            $sheet->setCellValue("D".$row, $fix[$i]["No.RM"]);
            $sheet->setCellValue("E".$row, $fix[$i]["Nama Pasien"]);
            $sheet->setCellValue("F".$row, $fix[$i]["NO.SEP"]);
            $sheet->setCellValue("G".$row, $fix[$i]["INACBG"]);
            $sheet->setCellValue("H".$row, $fix[$i]["plafon_bpjs"]);
            $sheet->setCellValue("I".$row, $fix[$i]["total_tagihan"]);
            $sheet->setCellValue("J".$row, $fix[$i]["Jenis"]);
            $sheet->getStyle('H'.$row.':'.'I'.$row)->getNumberFormat()->setFormatCode('#,##0.00');
            $sheet->getStyle('A'.$row.':'.'C'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $sheet->getStyle('D'.$row.':'.'E'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            $sheet->getStyle('F'.$row.':'.'G'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $sheet->getStyle('H'.$row.':'.'I'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
            $sheet->getStyle('J'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $row++;
            $no++;
        }
        
        foreach(range('A','J') as $columnID) {
            $sheet->getColumnDimension($columnID)->setAutoSize(true);
        }
        
        header ( 'Content-Type: application/vnd.ms-excel' );
        header ( 'Content-Disposition: attachment;filename="LAPORAN REKAP KLAIM.xls"' );
        header ( 'Cache-Control: max-age=0' );
        $writer = PHPExcel_IOFactory::createWriter ( $file, 'Excel5' );
        $writer->save ( 'php://output' );
		return;
    }
}

?>