<?php 
class RencanaResponder extends DBResponder{    
    public function command($command){
        if($command=="load_diagnosa"){
            $pack    = new ResponsePackage();
            $pack    ->setStatus(ResponsePackage::$STATUS_OK);
            $content = $this->getDiagnosaList();
            $pack    ->setWarning(true,"Diagnosa",$content);
            return $pack->getPackage();
        }else{
            parent::command($command);
        }
    }    
    
    public function getDiagnosaList($noreg){
        global $db;
        require_once "smis-base/smis-include-service-consumer.php";
        $serv = new ServiceConsumer($db,"get_diagnosa_by_noreg",null,"medical_record");
        $serv ->addData("noreg_pasien",$_POST['id']);
        $serv ->execute();
        $data = $serv->getContent();
        
        $table  = new TablePrint();
        $table  ->setDefaultBootrapClass(true);
        $table  ->setMaxWidth(false);
        $table  ->addColumn("Diagnosa","1","1")
                ->addColumn("Kode ICD","1","1")
                ->addColumn("Nama ICD","1","1")
                ->addColumn("Ruangan","1","1")
                ->commit("title");

        foreach($data as $x){
            $table  ->addColumn($x['diagnosa'],"1","1")
                    ->addColumn($x['kode_icd'],"1","1")
                    ->addColumn($x['nama_icd'],"1","1")
                    ->addColumn(ArrayAdapter::format("unslug",$x['ruangan']),"1","1")
                    ->commit("body");
        }

        return $table->getHtml();
    }    
}
?>