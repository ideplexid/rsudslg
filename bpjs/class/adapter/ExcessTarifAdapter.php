<?php

class ExcessTarifAdapter extends ArrayAdapter {
    private $no_sep;
    private $jenis;
    
    public function adapt($d) {
        if($d->uri == 0) {
            $this->jenis = "RJ";
            if($d->no_sep_rj == NULL || $d->no_sep_rj == "") {
                $this->no_sep = $d->no_sep_ri;
            }
            $this->no_sep = $d->no_sep_rj;
        } else {
            $this->jenis = "RI";
            if($d->no_sep_ri == NULL || $d->no_sep_ri == "") {
                $this->no_sep = $d->no_sep_rj;
            }
            $this->no_sep = $d->no_sep_ri;
        }
        
        $this->number++;
        $a = array();
        $a['No.']           = $this->number.".";
        $a['Tgl. Masuk']    = self::format ( "date d M Y ", $d->tanggal );
        $a['Tgl. Pulang']   = self::format ( "date d M Y ", $d->tanggal_pulang );
        $a['No.RM']         = $d->nrm;
        $a['Nama Pasien']   = $d->nama_pasien;
        $a['No.SEP']        = $this->no_sep;
        $a['INACBG']        = $d->inacbg_bpjs;
        $a['Total Tarif']   = self::format ( "money ", $d->plafon_bpjs );
        $a['Tarif RS']      = self::format ( "money ", $d->total_tagihan );
        $a['plafon_bpjs']   = $d->plafon_bpjs;
        $a['total_tagihan'] = $d->total_tagihan ;
        $a['Jenis']         = $this->jenis;
        
		return $a;
    }
}

?>