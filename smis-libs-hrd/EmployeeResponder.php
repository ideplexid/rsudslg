<?php
require_once ("smis-base/smis-include-service-consumer.php");
class EmployeeResponder extends ServiceResponder {
	private $employee;
	private $strict="0";
	private $ruangan="%";
    
    public static $PEGAWAI_ORGANIK="employee_organik";
    public static $PEGAWAI_NON_ORGANIK="employee_non_organik";
    public static $PEGAWAI_BOTH_ORGANIK_NON_ORGANIK="employee";
    
	public function __construct($db, $uitable, $adapter, $employee) {
		parent::__construct ( $db, $uitable, $adapter, "employee","hrd" );
		$this->employee = $employee;
	}
    
    public function setJenisPegawai($jenis){
        $this->setServiceName($jenis);
        return $this;
    }
	
	public function setStrict($strict=false,$ruangan="%"){
		$this->strict=$strict?"1":"0";
		$this->ruangan=$ruangan;
	}
	
	public function getData() {
		$data = $_POST;
		if (isset($_POST ['kriteria']) && $_POST ['kriteria']  != "") {
			$kriteria = array (
					"multiple_kriteria" => array (
							"nama" => $_POST ['kriteria'],
							"jabatan" => $this->employee,
							"nip" => $_POST ['kriteria'] 
					) 
			);
			$data ['kriteria'] = json_encode ( $kriteria );
		} else {
			$data ['kriteria'] = $this->employee;
		}
		$data['strict']=$this->strict;
		$data['ruangan']=$this->ruangan;
		return $data;
	}
}

?>