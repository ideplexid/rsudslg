<?php
class DKResponder extends ServiceResponder {
	private $strict="0";
	private $ruangan="%";
	
	public function setStrict($strict=false,$ruangan="%"){
		$this->strict=$strict?"1":"0";
		$this->ruangan=$ruangan;
	}
	
	public function getData() {
		$data = $_POST;
		if ($_POST ['kriteria'] != "") {
			$kriteria = array (
					"multiple_kriteria" => array (
							"nama" => $_POST ['kriteria'],
							"jabatan" => "",
							"nip" => $_POST ['kriteria'] 
					) 
			);
			$data ['kriteria'] = json_encode ( $kriteria );
		} else {
			$data ['kriteria'] = "";
		}
		$data['strict']=$this->strict;
		$data['ruangan']=$this->ruangan;
		return $data;
	}
}
?>