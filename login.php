<?php
if(file_exists("smis-base/smis-config.php")){
	require_once ("smis-base/smis-config.php");
}
require_once 'smis-base/smis-include-ui.php';
require_once 'smis-base/smis-include-system.php';
require_once 'smis-libs-function/smis-libs-function-essential.php';

$installed 						= is_system_installed (); 						                                        // check is this system was installed
require_once 'smis-libs-function/smis-libs-function-base.php';
$url = getSettings($db, "smis_autonomous_title", "Safethree MIS");
    
session_name(getSessionPrefix());	
session_start ();

$session = $_GET['session'];

global $wpdb;
$query = "SELECT * FROM smis_adm_user WHERE `session`='$session' ";
$usr = $wpdb->get_row($query);

setSession('userid',$usr->id);
setSession('session',$session);	

$page = isset($_GET['page'])?$_GET['page']:"";
$action = isset($_GET['action'])?$_GET['action']:"";
$pname =  isset($_GET['pname'])?$_GET['pname']:"";
$pslug =  isset($_GET['pslug'])?$_GET['pslug']:"";
$pimpl =  isset($_GET['pimpl'])?$_GET['pimpl']:"";

$result = array(
    "page"=>$page,
    "action"=>$action,
    "prototype_name"=>$pname,
    "prototype_slug"=>$pslug,
    "prototype_implement"=>$pimpl
);

setrawcookie("smis-index",rawurlencode(json_encode($result)));
//$_COOKIE['smis-index']=json_encode($result);

if (!function_exists('base_url')) {
    function base_url($atRoot=FALSE, $atCore=FALSE, $parse=FALSE){
        if (isset($_SERVER['HTTP_HOST'])) {
            $http = isset($_SERVER['HTTPS']) && strtolower($_SERVER['HTTPS']) !== 'off' ? 'https' : 'http';
            $hostname = $_SERVER['HTTP_HOST'];
            $dir =  str_replace(basename($_SERVER['SCRIPT_NAME']), '', $_SERVER['SCRIPT_NAME']);

            $core = preg_split('@/@', str_replace($_SERVER['DOCUMENT_ROOT'], '', realpath(dirname(__FILE__))), NULL, PREG_SPLIT_NO_EMPTY);
            $core = $core[0];

            $tmplt = $atRoot ? ($atCore ? "%s://%s/%s/" : "%s://%s/") : ($atCore ? "%s://%s/%s/" : "%s://%s%s");
            $end = $atRoot ? ($atCore ? $core : $hostname) : ($atCore ? $core : $dir);
            $base_url = sprintf( $tmplt, $http, $hostname, $end );
        }
        else $base_url = 'http://localhost/';

        if ($parse) {
            $base_url = parse_url($base_url);
            if (isset($base_url['path'])) if ($base_url['path'] == '/') $base_url['path'] = '';
        }

        return $base_url;
    }
}

$base = base_url();
header("location:".$base);
require_once 'smis-base/smis-end-process.php';
