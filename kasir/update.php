<?php
    global $wpdb;
    require_once 'smis-libs-inventory/install.php';
    $install = new InventoryInstallator($wpdb, "", "");
    $install ->setUsing(true, true, true);
    $install ->extendInstall("ksr");
    $install ->install();
    require_once "kasir/resource/install/table/smis_ksr_bank.php";
    require_once "kasir/resource/install/table/smis_ksr_bayangan.php";
    require_once "kasir/resource/install/table/smis_ksr_bayar.php";
    require_once "kasir/resource/install/table/smis_ksr_kolektif.php";
    require_once "kasir/resource/install/table/smis_ksr_kwitansi.php";
    require_once "kasir/resource/install/table/smis_ksr_nonpasien.php";
    require_once "kasir/resource/install/table/smis_ksr_np.php";
    require_once "kasir/resource/install/table/smis_ksr_npd.php";
    require_once "kasir/resource/install/table/smis_ksr_piutang.php";
    require_once "kasir/resource/install/table/smis_ksr_resume.php";
    require_once "kasir/resource/install/table/smis_ksr_rumus.php";
    require_once "kasir/resource/install/table/smis_ksr_tagihan.php";
    require_once "kasir/resource/install/table/smis_ksr_tagihan_rumus.php";
    require_once "kasir/resource/install/table/smis_ksr_tagihan_tambahan.php";
    require_once "kasir/resource/install/table/smis_ksr_sks.php";
    require_once "kasir/resource/install/table/smist_ksr_rekap_total.php";
    require_once "kasir/resource/install/table/smis_ksr_diskon.php";
    require_once "kasir/resource/install/view/smis_vpiutang.php";
    require_once "kasir/resource/install/table/smis_ksr_obat_resume_pasien.php";
