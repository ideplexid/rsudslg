<?php
	global $NAVIGATOR;
	// Administrator Top Menu
	$mr = new Menu ("fa fa-cc-mastercard");
	$mr->addProperty ( 'title', 'Cashier' );
	$mr->addProperty ( 'name', 'Kasir' );
    
	$mr->addSubMenu ( "Tagihan Non Pasien", "kasir", "tagihan_non_pasien", "Tagihan Non Pasien" ,"fa fa-money");
	$mr->addSubMenu ( "Pembayaran Resep", "kasir", "pembayaran_resep", "Pembayaran Resep" ,"fa fa-file-text-o");
	$mr->addSubMenu ( "Pembayaran Pasien", "kasir", "pembayaran_patient", "Pembayaran Pasien" ,"fa fa-fax");
	$mr->addSubMenu ( "Klaim Pasien", "kasir", "klaim_pasien", "Klaim Pasien" ,"fa fa-book");
	$mr->addSubMenu ( "Tambahan Biaya Pasien", "kasir", "tambahan_biaya_pasien", "Tambahan Biaya Pasien" ,"fa fa-money");
	$mr->addSubMenu ( "Data Induk", "kasir", "data_induk", "Data Induk" ,"fa fa-database");
	$mr->addSubMenu ( "Laporan", "kasir", "laporan", "Laporan Uang" ,"fa fa-ticket");
    $mr->addSubMenu ( "Jurnal 2015", "kasir", "jurnal", "Jurnal Format 2015" ,"fa fa-file-text");
	$mr->addSubMenu ( "Jurnal 2016", "kasir", "jurnal_2016", "Jurnal Format 2016", "fa fa-file-text" );
    $mr->addSubMenu ( "Settings", "kasir", "settings", "Settings" ,"fa fa-cog");
	$mr->addSubMenu ( "Mapping Akunting", "kasir", "mapping_akunting", "Mapping Akunting" ,"fa fa-list-alt");
	
   require_once 'smis-libs-inventory/navigation.php';
	$inventory_navigator = new InventoryNavigator($NAVIGATOR, "", "Kasir", "kasir");
	$mr = $inventory_navigator->extendMenu($mr);
	$NAVIGATOR->addMenu ( $mr, 'kasir' );
    
    $persetujuan = new Menu("fa fa-user");
	$persetujuan->addProperty('title', "Persetujuan Direktur");
	$persetujuan->addProperty('name', "Persetujuan Direktur");
	$persetujuan->addSubMenu("Persetujuan Diskon Pasien", "persetujuan_diskon_pasien", "persetujuan_diskon", "Data Persetujuan Diskon Pasien", "fa fa-bed", "", "", "kasir");
	$NAVIGATOR->addMenu($persetujuan, "persetujuan_diskon_pasien");
?>
