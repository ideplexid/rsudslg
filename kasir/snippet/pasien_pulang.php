<?php 
/**
 * memulangkan pasien sehingga, keluar dari sistem.
 * - dengan ini kasir bisa memulangkan pasien
 */
global $user;
global $db;
require_once ("smis-base/smis-include-service-consumer.php");
require_once "kasir/function/get_total_tagihan.php";
$result  = get_total_tagihan($db,$_POST['noreg_pasien']);
$ruangan = "registration";
if(getSettings($db,"cashier-activate-out-force-mode","0")=="1"){
    $ruangan = "all";
}

$service = new ServiceConsumer ( $db, "set_unregistered", NULL, $ruangan );
$service ->addData("total_tagihan",$result['total']);
$service ->addData("asuransi",$result['asuransi']);
$service ->addData("bank",$result['bank']);
$service ->addData("cash",$result['cash']);
$service ->addData("diskon",$result['diskon']);
$service ->addData("diskon_keterangan",$result['diskon_keterangan']);
$service ->addData("noreg_pasien",$_POST['noreg_pasien']);
$service ->addData('id',$_POST ['noreg_pasien']);
$service ->addData('carapulang',$_POST ['carapulang']);
$service ->addData('waktu_pulang',$_POST ['waktu_pulang']);
$service ->addData('gratis',$_POST ['gratis']);
$service ->addData('alasan_gratis',$_POST ['alasan_gratis']);
$service ->addData('opsi_gratis',$_POST ['opsi_gratis']);

$service ->addData('selesai',1);
$service ->addData('tanggal_pulang',date ( "Y-m-d H:i:s" ));
$service ->addData('username',$user->getUsername());
$service ->execute ();
$content = $service->getContent ();
echo json_encode ( $content );


/**update ksr kolektif untuk mengirimkan tanggal pulangnya */
$dbtable = new DBTable($db,"smis_ksr_kolektif");
$dbtable ->update(
    array("tanggal_pulang"=>substr($_POST['waktu_pulang'],0,10) ),
    array("noreg_pasien"=>$_POST['noreg_pasien'])
);

?>