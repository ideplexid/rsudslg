<?php 
    global $db;
    require_once "smis-base/smis-include-service-consumer.php";
    require_once "kasir/function/get_total_tagihan.php";
    $result = total_tagihan_non_administrasi_inap($db,$_POST['noreg_pasien']);      
    
    
    $ser    = new ServiceConsumer($db,"update_total_tagihan",NULL,"registration");
    $ser->addData("total_tagihan",$result['total']);
    $ser->addData("administrasi",$result['administrasi']);
    $ser->addData("asuransi",$result['asuransi']);
    $ser->addData("bank",$result['bank']);
    $ser->addData("cash",$result['cash']);
    $ser->addData("diskon",$result['diskon']);
    $ser->addData("diskon_keterangan",$result['diskon_keterangan']);
    $ser->addData("noreg_pasien",$_POST['noreg_pasien']);
    $ser->execute();

    $result = $ser->getContent();

    $dbtable = new DBTable($db,"smis_ksr_kolektif");
    $up['noreg_pasien'] = $_POST['noreg_pasien'];
    $data['tanggal_masuk'] = substr($result['tanggal'],0,10);
    $data['tanggal_pulang'] = substr($result['tanggal_pulang'],0,10);
    $data['carabayar'] = substr($result['carabayar'],0,10);
    
    if($result['administrasi_inap']*1>0){
        $query = "UPDATE smis_ksr_kolektif SET nilai=".$result['administrasi_inap'].", 
                    total = '".$result['administrasi_inap']."', quantity=1 , prop='' 
                    WHERE noreg_pasien = '".$_POST['noreg_pasien']."' AND jenis_tagihan='administrasi' ";
        $db->query($query);

        file_put_contents("smis-temp/adminp.text",$query);
    }else{
        $query = "UPDATE smis_ksr_kolektif SET prop='del' 
                    WHERE noreg_pasien = '".$_POST['noreg_pasien']."' AND jenis_tagihan='administrasi' ";
        $db->query($query);
    }    
    $dbtable ->update($data,$up);