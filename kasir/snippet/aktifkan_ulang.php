<?php 
/**
 * mengaktifkan ulang data pasien yang terjadi masalah.
 * - dengan ini kasir bisa mengeblaikan pasien tertentu ke ruang tertentu
 */
 
require_once ("smis-base/smis-include-service-consumer.php");
class Reactived extends ServiceConsumer {
	public function __construct($db, $noreg, $polislug) {
		$array=array();
		$array['noreg_pasien']=$noreg;
		parent::__construct ( $db, "reactived_antrian", $array, $polislug );
	}
	public function proceedResult() {
		return $this->result;
	}
}
$polislug = $_POST ['polislug'];
$noreg_pasien = $_POST ['noreg_pasien'];
$ser = new Reactived ( $db, $noreg_pasien, $polislug );
$ser->execute ();
echo $ser->getContent ();
return;

 ?>