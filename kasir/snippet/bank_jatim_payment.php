<?php 
$entityBody = file_get_contents('php://input');
$json       = json_decode($entityBody,true);
$array      = array();
if($json==null){
    $array["DateTime"]  = "";
    $array["NoBayar"]   = "";    
    $array["KodeVoucher"]               = "";
    $array["Status"]                    = array();
    $array["Status"]["IsError"]         = "True";
    $array["Status"]["ResponseCode"]    = "00";
    $array["Status"]["ErrorDesc"]       = "Format JSON Salah";
}else{
    $idbiling   = $json["NoBayar"];
    $dbtable    = new DBTable($db,"smis_ksr_bayar");
    $bayar      = $dbtable ->select(array("idbilling"=>$idbiling));
    if($bayar==null){
        /**id biling salah */
        $array["DateTime"]                  = "";
        $array["NoBayar"]                   = "";    
        $array["KodeVoucher"]               = "";
        $array["Status"]                    = array();
        $array["Status"]["IsError"]         = "True";
        $array["Status"]["ResponseCode"]    = "01";
        $array["Status"]["ErrorDesc"]       = "No Bayar Salah";
    }else if(trim($bayar->no_bukti)!=""){
        /**sudah dibayar */
        $array["DateTime"]                  = "";
        $array["NoBayar"]                   = "";  
        $array["KodeVoucher"]               = "";  
        $array["Status"]                    = array();
        $array["Status"]["IsError"]         = "True";
        $array["Status"]["ResponseCode"]    = "06";
        $array["Status"]["ErrorDesc"]       = "Tagihan Lunas";
    }else{
        $update['waktu_masuk']  = $array["DateTime"];
        $update['no_bukti']     = $json['Reff'];
        $result = $dbtable->update($update,array("id"=>$bayar->id));
        if($result!==false){
            //berhasil update
            $array["DateTime"]                  = $json["DateTime"];
            $array["NoBayar"]                   = $bayar->idbilling; 
            $array["KodeVoucher"]               = "";   
            $array["Status"]                    = array();
            $array["Status"]["IsError"]         = "False";
            $array["Status"]["ResponseCode"]    = "00";
            $array["Status"]["ErrorDesc"]       = "Success";  
  
        }else{
              //gagal update
            $array["DateTime"]                  = "";
            $array["NoBayar"]                   = "";
            $array["KodeVoucher"]               = "";    
            $array["Status"]                    = array();
            $array["Status"]["IsError"]         = "False";
            $array["Status"]["ResponseCode"]    = "02";
            $array["Status"]["ErrorDesc"]       = "Pembayaran Tidak Dapat di Proses";    
        }
    }
}
echo json_encode($array);