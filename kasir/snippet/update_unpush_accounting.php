<?php 
/**
 * dipakai untuk melakukan notify bahwa 
 * untuk pengakuan hutang pasien telah diambil ditanggal sekarang
 * 
 */
global $user;
global $db;
require_once ("smis-base/smis-include-service-consumer.php");
require_once "kasir/function/get_total_tagihan.php";
$service = new ServiceConsumer ( $db, "update_unpost_notify", NULL, "registration" );
$service->addData('id',$_POST ['noreg_pasien']);
$service->execute ();
$content = $service->getContent ();

$response = new ResponsePackage();
$response->setStatus(ResponsePackage::$STATUS_OK);
$response->setAlertVisible(true);
$response->setAlertContent("Information",$content);

echo json_encode ( $response->getPackage() );

?>