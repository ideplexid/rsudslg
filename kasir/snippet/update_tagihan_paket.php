<?php 
/**
 * this file used to update the price of kwitansi paket
 * this will update the kode inacbg, desc inacbg, and paket price
 * and after that it will automatically.
 * show the update of kwitansi_paket after the price updated.
 * 
 * @author 		: Nurul Huda
 * @since 		: 20 Des 2016
 * @version 	: 1.0.0
 * @copyright 	: goblooge@gmail.com
 * @license 	: Apache v2
 * @service 	: registration/service/update_total_kwitansi_paket.php
 * @used 		: kasir/resource/php/pembayaran_patient/kwitansi_paket.php
 * */
 
global $db;
require_once "smis-base/smis-include-service-consumer.php";
$data['noreg_pasien']=$_POST['noreg_pasien'];
$data['total_paket']=$_POST['harga'];
$data['kode_inacbg']=$_POST['kode'];
$data['desc_inacbg']=$_POST['desc'];
$service=new ServiceConsumer($db,"update_total_paket",$data,"registration");
$service->execute();
require_once "kasir/resource/php/pembayaran_patient/kwitansi_paket.php";

?>