<?php 
/**
 * mencari semua entitas yang menmiliki data tagihan
 * - pertama hapus dahulu semua tagihan pasien dengan noreg tertentu
 * - kemudian cari semua entitas yang memiliki service tagian
 * - kembalikan ke javascript dan akan dilakukan penelusuran dengan javascript
 * dipakai di kasr/modul/list_registered.php dan dipakai di kasir/class/responder/TotalResponder.php
 */

require_once 'smis-base/smis-include-service-consumer.php';
$noreg=$_POST ['noreg'];

/*menghapus seluruh data tagihan pasien*/
$query="UPDATE smis_ksr_kolektif SET prop='del' 
        WHERE noreg_pasien='".$noreg."'";
$db->query($query);	

$content=NULL;
if(getSettings($db,"cashier-selective-area","0")=="1"){
    /* mencari hanya ruangan yang dimasuki oleh pasien - selectif model */
    require_once "kasir/class/service/RuanganAktifManager.php";
    $arm=new RuanganAktifManager($db,"is_patient_exist");
    $arm->setNoregPatient($noreg);
    $arm->execute();
    $ctx=$arm->getContent();
}else{ 
    /*mencari seluruh ruangan yang memungkinkan dimasuki pasien*/
    require_once "smis-libs-class/ServiceProviderList.php";
    $lis_get_tagihan=new ServiceProviderList($db, "get_tagihan");
    $lis_get_tagihan->execute();
    $ctx=$lis_get_tagihan->getContent();   
}


/*ketika actived area setting aktif, maka hanya area yang dipilih yang akan di crawler*/
if(getSettings($db, "cashier-actived-area", "0")=="1"){
	$content=array();
	foreach($ctx as $x){
		/*by default , sebuah ruangan dianggap aktif jika belum di settings*/
		if(getSettings($db, "cashier-actived-area-".$x['value'], "1")=="1"){
			$content[]=$x;
		}
	}
}else{
	/*ketika non aktif maka semua dipilih*/
	$content=$ctx;
}
$pack=new ResponsePackage();
$pack->setContent($content);
$pack->setStatus(ResponsePackage::$STATUS_OK);
echo json_encode($pack->getPackage());


/**Jika tutup tagihan 0 maka masih bisa narik, jika 1 maka nggak bisa narik*/
if($_POST['tutup_tagihan']=="0"){
    $serv = new ServiceConsumer($db,"get_resume_obat_pasien",array("noreg_pasien"=>$noreg));
    $serv ->setMode(ServiceConsumer::$CLEAN_BOTH_JOIN_SINGLE_ARRAY);
    $serv ->execute();
    $result = $serv ->getContent();
    
    $x = array(
        "noreg_pasien"=>$noreg
    );
    $uptd = array(
        "noreg_pasien"=>$noreg,
        "detail_obat"=>json_encode($result)
    );
    $dbtable = new DBTable($db,"smis_ksr_obat_resume_pasien");
    $dbtable ->insertOrUpdate($uptd,$x);    
}
