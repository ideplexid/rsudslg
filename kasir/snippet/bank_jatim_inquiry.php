<?php 

$idbiling   = $_GET['no'];
$dbtable    = new DBTable($db,"smis_ksr_bayar");
$bayar      = $dbtable ->select(array("idbilling"=>$idbiling));
$array      = array();
if($bayar==null){
    /**id biling salah */
    $array["DetailTagihan"] = array();
    $array["JumlahTagihan"]     = 0;
    $array["NoBayar"]           = ""; 
    $array["Nama"]              = ""; 
    $array["institutionCode"]   = $_GET['institutionCode'];    
    $array["Keterangan"]        = "";
    $array["Status"]                    = array();
    $array["Status"]["IsError"]         = "True";
    $array["Status"]["ResponseCode"]    = "01";
    $array["Status"]["ErrorDesc"]       = "Failed";
}else if($bayar->urji=="Rawat Inap"){
    $array["DetailTagihan"] = array();

    $one_detail                 = array();
    $one_detail['KetBayar']     = "Rawat Inap";
    $one_detail['kodeBayar']    = getSettings($db,"cashier-bridging-bank-rawat-inap","000");
    $one_detail['Amount']       = (int)$bayar->nilai;
    $array["DetailTagihan"][]   = $one_detail;

    $array["JumlahTagihan"]     = "01";
    $array["NoBayar"]           = $bayar->idbilling; 
    $array["Nama"]              = $bayar->nama_pasien; 
    $array["Keterangan"]        = "Rawat Inap No. Reg ".$bayar->noreg_pasien; 
    $array["institutionCode"]   = $_GET['institutionCode'];

    $array["Status"]                    = array();
    $array["Status"]["IsError"]         = "False";
    $array["Status"]["ResponseCode"]    = "00";
    $array["Status"]["ErrorDesc"]       = "Success";

}else{
    $array["DetailTagihan"] = array();
    $array["JumlahTagihan"]     = 0;
    $total = 0;

    $_noreg = $bayar->noreg_pasien;

    $pendaftaran = $db->get_var("SELECT SUM(smis_ksr_kolektif.total) FROM smis_ksr_kolektif WHERE noreg_pasien='".$_noreg."' AND prop!='del' AND ruangan='registration' ");    
    $periksa     = $db->get_var("SELECT SUM(smis_ksr_kolektif.total) FROM smis_ksr_kolektif WHERE noreg_pasien='".$_noreg."' AND prop!='del' AND  ( jenis_tagihan LIKE 'konsultasi_dokter')");
    $konsul      = $db->get_var("SELECT SUM(smis_ksr_kolektif.total) FROM smis_ksr_kolektif WHERE noreg_pasien='".$_noreg."' AND prop!='del' AND  ( jenis_tagihan LIKE '%konsul')");
    $tindakan    = $db->get_var("SELECT SUM(smis_ksr_kolektif.total) FROM smis_ksr_kolektif WHERE noreg_pasien='".$_noreg."' AND prop!='del' AND  ( jenis_tagihan LIKE '%tindakan%')");    
    $laboratory  = $db->get_var("SELECT SUM(smis_ksr_kolektif.total) FROM smis_ksr_kolektif WHERE noreg_pasien='".$_noreg."' AND prop!='del' AND  ( jenis_tagihan LIKE 'laboratory')");
    $radiology   = $db->get_var("SELECT SUM(smis_ksr_kolektif.total) FROM smis_ksr_kolektif WHERE noreg_pasien='".$_noreg."' AND prop!='del' AND  ( jenis_tagihan LIKE 'radiology')");
    $farmasi     = $db->get_var("SELECT SUM(smis_ksr_kolektif.total) FROM smis_ksr_kolektif WHERE noreg_pasien='".$_noreg."' AND prop!='del' AND  ( jenis_tagihan='penjualan_resep' OR jenis_tagihan='return_resep')"); 
    $lain        = $db->get_var("SELECT SUM(smis_ksr_kolektif.total) FROM smis_ksr_kolektif WHERE noreg_pasien='".$_noreg."' AND prop!='del' AND  ( jenis_tagihan LIKE 'umum')");
    $total       = $db->get_var("SELECT SUM(smis_ksr_kolektif.total) FROM smis_ksr_kolektif WHERE noreg_pasien='".$_noreg."' AND prop!='del' ");
    $penunjang   = $total - $pendaftaran -  $farmasi - $tindakan - $konsul - $periksa - $laboratory - $radiology - $lain;


    /**pendaftaran */
    if($pendaftaran>0){
        $one_detail                 = array();
        $one_detail['KetBayar']     = "Pendaftaran";
        $one_detail['kodeBayar']    = getSettings($db,"cashier-bridging-bank-pendaftaran","000");
        $one_detail['Amount']       = (int)$pendaftaran;
        $array["DetailTagihan"][]   = $one_detail;    
        $total+=$pendaftaran;
    }

    /**periksa */
    if($periksa>0){
        $one_detail                 = array();
        $one_detail['KetBayar']     = "Pemeriksaan Poli";
        $one_detail['kodeBayar']    = getSettings($db,"cashier-bridging-bank-periksa-poli","000");
        $one_detail['Amount']       = (int)$periksa;
        $array["DetailTagihan"][]   = $one_detail;    
        $total+=$periksa;
    }

    /**konsultasi poli */
    if($konsul>0){
        $one_detail                 = array();
        $one_detail['KetBayar']     = "Konsultasi Poli";
        $one_detail['kodeBayar']    = getSettings($db,"cashier-bridging-bank-konsultasi-poli","000");
        $one_detail['Amount']       = (int)$konsul;
        $array["DetailTagihan"][]   = $one_detail;    
        $total+=$konsul;
    }

    /**tindakan */
    if($tindakan>0){
        $one_detail                 = array();
        $one_detail['KetBayar']     = "Tindakan";
        $one_detail['kodeBayar']    = getSettings($db,"cashier-bridging-bank-tindakan","000");
        $one_detail['Amount']       = (int)$tindakan;
        $array["DetailTagihan"][]   = $one_detail;    
        $total+=$tindakan;
    }

    /**tindakan */
    if($laboratory>0){
        $one_detail                 = array();
        $one_detail['KetBayar']     = "Laboratorium";
        $one_detail['kodeBayar']    = getSettings($db,"cashier-bridging-bank-laboratorium","000");
        $one_detail['Amount']       = (int)$laboratory;
        $array["DetailTagihan"][]   = $one_detail;    
        $total+=$laboratory;
    }

    /**tindakan */
    if($radiology>0){
        $one_detail                 = array();
        $one_detail['KetBayar']     = "Radiology";
        $one_detail['kodeBayar']    = getSettings($db,"cashier-bridging-bank-radiology","000");
        $one_detail['Amount']       = (int)$radiology;
        $array["DetailTagihan"][]   = $one_detail;    
        $total+=$radiology;
    }

    /**penunjang */
    if($penunjang>0){
        $one_detail                 = array();
        $one_detail['KetBayar']     = "Penunjang Lain";
        $one_detail['kodeBayar']    = getSettings($db,"cashier-bridging-bank-penunjang","000");
        $one_detail['Amount']       = (int)$penunjang;
        $array["DetailTagihan"][]   = $one_detail;    
        $total+=$penunjang;
    }

    /**farmasi */
    if($farmasi>0){
        $one_detail                 = array();
        $one_detail['KetBayar']     = "Farmasi";
        $one_detail['kodeBayar']    = getSettings($db,"cashier-bridging-bank-farmasi","000");
        $one_detail['Amount']       = (int)$farmasi;
        $array["DetailTagihan"][]   = $one_detail;  
        $total+=$farmasi; 
    }

     /**lain-lain */
     if($lain>0){
        $one_detail                 = array();
        $one_detail['KetBayar']     = "Lain - Lain";
        $one_detail['kodeBayar']    = getSettings($db,"cashier-bridging-bank-lain","000");
        $one_detail['Amount']       = (int)$lain;
        $array["DetailTagihan"][]   = $one_detail;  
        $total+=$lain; 
    }

    $count = count($array["DetailTagihan"]);

    $array["JumlahTagihan"]     = $count>9?"".$count:"0".$count;

    
    $array["NoBayar"]           = $bayar->idbilling; 
    $array["Nama"]              = $bayar->nama_pasien; 
    $array["Keterangan"]        = "Rawat Jalan No. Reg ".$bayar->noreg_pasien; 
    $array["institutionCode"]   = $_GET['institutionCode'];
    
    $array["Status"]                    = array();
    $array["Status"]["IsError"]         = "False";
    $array["Status"]["ResponseCode"]    = "00";
    $array["Status"]["ErrorDesc"]       = "Success";
}

echo json_encode($array);