<?php 

$id=$_POST['id'];
$dbtable=new DBTable($db,"smis_ksr_tagihan_rumus");
$x=$dbtable->select($id);

$table=new TablePrint("");
$table->setDefaultBootrapClass(true);
$table->setMaxWidth(false);

$table->addColumn("<h4>ID ".ArrayAdapter::format("only-digit12",$id),2,1,"title")."</h4>";

$table->addColumn("<strong class='label label-important'>Pasien<strong>",2,1,"body",null,"center");
$table->addColumn("Nama",1,1)->addColumn($x->nama_pasien,1,1,"body");
$table->addColumn("Noreg",1,1)->addColumn($x->noreg_pasien,1,1,"body");
$table->addColumn("NRM",1,1)->addColumn($x->nrm_pasien,1,1,"body");

$table->addColumn("Kelas Plafon",1,1)->addColumn($x->kelas_plafon,1,1,"body");
$table->addColumn("Kode INACBG",1,1)->addColumn($x->kode_inacbg,1,1,"body");
$table->addColumn("Deskripsi INACBG",1,1)->addColumn($x->deskripsi_inacbg,1,1,"body");
$table->addColumn("Ruang Pelayanan",1,1)->addColumn(ArrayAdapter::format("unslug",$x->ruang_pelayanan),1,1,"body");

$table->addColumn("Plafon INACBG",1,1)->addColumn(ArrayAdapter::format("money Rp.",$x->tarif_plafon_inacbg),1,1,"body");
$table->addColumn("Plafon Kelas I",1,1)->addColumn(ArrayAdapter::format("money Rp.",$x->tarif_plafon_kelas1),1,1,"body");
$table->addColumn("Tagihan (T)",1,1)->addColumn(ArrayAdapter::format("money Rp.",$x->total_tagihan),1,1,"body");
$table->addColumn("Potongan (P)",1,1)->addColumn(ArrayAdapter::format("money Rp.",$x->total_potongan),1,1,"body");
$table->addColumn("Biaya (B=T-P)",1,1)->addColumn(ArrayAdapter::format("money Rp.",$x->total_biaya),1,1,"body");


$table->addColumn("<strong class='label label-success'>Rumus</strong>",2,1,"body",null,"center");
$table->addColumn("ID Rumus PV",1,1)->addColumn($x->id_rumus_pv,1,1,"body");
$table->addColumn("Keterangan PV",1,1)->addColumn($x->keterangan_pv,1,1,"body");
$table->addColumn("ID Rumus Tagihan",1,1)->addColumn($x->id_rumus,1,1,"body");
$table->addColumn("Nama Rumus Tagihan",1,1)->addColumn($x->nama_rumus,1,1,"body");
$table->addColumn("Rumus Biaya",1,1)->addColumn($x->rumus_biaya,1,1,"body");
$table->addColumn("Rumus Tagihan",1,1)->addColumn($x->rumus_tagihan,1,1,"body");
$table->addColumn("Rumus Potongan",1,1)->addColumn($x->rumus_potongan,1,1,"body");



$table->addColumn("<strong class='label label-warning'>Dokter</strong>",2,1,"body",null,"center");
if($x->nama_dpjp_satu!="") {
    $table->addColumn("Dr. DPJP I",1,1)->addColumn($x->nama_dpjp_satu,1,1,"body");
    $table->addColumn("Biaya Dr. DPJP I",1,1)->addColumn(ArrayAdapter::format("money Rp.",$x->biaya_dpjp_satu),1,1,"body");
    $table->addColumn("Mulai Dr. DPJP I",1,1)->addColumn(ArrayAdapter::format("date d M Y H:i",$x->mulai_dpjp_satu),1,1,"body");
    $table->addColumn("Selesai Dr. DPJP I",1,1)->addColumn(ArrayAdapter::format("date d M Y H:i",$x->selesai_dpjp_satu),1,1,"body");
    $table->addColumn("Durasi Dr. DPJP I",1,1)->addColumn($x->durasi_dpjp_satu." Jam",1,1,"body");
}
if($x->nama_dpjp_dua!="") {
    $table->addColumn("Dr. DPJP II",1,1)->addColumn($x->nama_dpjp_dua,1,1,"body");
    $table->addColumn("Biaya Dr. DPJP II",1,1)->addColumn(ArrayAdapter::format("money Rp.",$x->biaya_dpjp_dua),1,1,"body");
    $table->addColumn("Mulai Dr. DPJP II",1,1)->addColumn(ArrayAdapter::format("date d M Y H:i",$x->mulai_dpjp_dua),1,1,"body");
    $table->addColumn("Selesai Dr. DPJP II",1,1)->addColumn(ArrayAdapter::format("date d M Y H:i",$x->selesai_dpjp_dua),1,1,"body");
    $table->addColumn("Durasi Dr. DPJP II",1,1)->addColumn($x->durasi_dpjp_dua." Jam",1,1,"body");
}
if($x->nama_dpjp_tiga!="") {
    $table->addColumn("Dr. DPJP III",1,1)->addColumn($x->nama_dpjp_tiga,1,1,"body");
    $table->addColumn("Biaya Dr. DPJP III",1,1)->addColumn(ArrayAdapter::format("money Rp.",$x->biaya_dpjp_tiga),1,1,"body");
    $table->addColumn("Mulai Dr. DPJP III",1,1)->addColumn(ArrayAdapter::format("date d M Y H:i",$x->mulai_dpjp_tiga),1,1,"body");
    $table->addColumn("Selesai Dr. DPJP III",1,1)->addColumn(ArrayAdapter::format("date d M Y H:i",$x->selesai_dpjp_tiga),1,1,"body");
    $table->addColumn("Durasi Dr. DPJP III",1,1)->addColumn($x->durasi_dpjp_tiga." Jam",1,1,"body");
}

if($x->nama_konsul_satu!="") $table->addColumn("Dr. Konsul I",1,1)->addColumn($x->nama_konsul_satu,1,1,"body");
if($x->nama_konsul_dua!="") $table->addColumn("Dr. Konsul II",1,1)->addColumn($x->nama_konsul_dua,1,1,"body");
if($x->nama_anastesi!="") $table->addColumn("Dr. Anastesi",1,1)->addColumn($x->nama_anastesi,1,1,"body");
if($x->nama_spesialis!="") $table->addColumn("Dr. Spesialis",1,1)->addColumn($x->nama_spesialis,1,1,"body");
if($x->nama_umum!="") $table->addColumn("Dr. Umum",1,1)->addColumn($x->nama_umum,1,1,"body");
if($x->nama_igd!="") $table->addColumn("Dr. IGD",1,1)->addColumn($x->nama_igd,1,1,"body");
if($x->nama_anak!="") $table->addColumn("Dr. Anak",1,1)->addColumn($x->nama_anak,1,1,"body");

$table->addColumn("<strong class='label label-inverse'>Durasi IRNA</strong>",2,1,"body",null,"center");
if($x->irna_a!=0) $table->addColumn("IRNA A",1,1)->addColumn($x->irna_a,1,1,"body");
if($x->irna_b!=0) $table->addColumn("IRNA B",1,1)->addColumn($x->irna_b,1,1,"body");
if($x->irna_b1!=0) $table->addColumn("IRNA B1",1,1)->addColumn($x->irna_b1,1,1,"body");
if($x->irna_c!=0) $table->addColumn("IRNA C",1,1)->addColumn($x->irna_c,1,1,"body");
if($x->irna_e!=0) $table->addColumn("IRNA E",1,1)->addColumn($x->irna_e,1,1,"body");
if($x->irna_f!=0) $table->addColumn("IRNA F",1,1)->addColumn($x->irna_f,1,1,"body");
if($x->irna_g!=0) $table->addColumn("IRNA G",1,1)->addColumn($x->irna_g,1,1,"body");
if($x->paviliun!=0) $table->addColumn("PAVILIUN",1,1)->addColumn($x->paviliun,1,1,"body");

$table->addColumn("<strong class='label label-inverse'>Durasi Kelas</strong>",2,1,"body",null,"center");
if($x->durasi_non_kelas!=0) $table->addColumn("Non Kelas",1,1)->addColumn($x->durasi_non_kelas,1,1,"body");
if($x->durasi_kelas_i!=0) $table->addColumn("Kelas I",1,1)->addColumn($x->durasi_kelas_i,1,1,"body");
if($x->durasi_kelas_ii!=0) $table->addColumn("Kelas II",1,1)->addColumn($x->durasi_kelas_ii,1,1,"body");
if($x->durasi_kelas_iii!=0) $table->addColumn("Kelas III",1,1)->addColumn($x->durasi_kelas_iii,1,1,"body");
if($x->durasi_kelas_vip!=0) $table->addColumn("Kelas VIP",1,1)->addColumn($x->durasi_kelas_vip,1,1,"body");
if($x->durasi_kelas_vvip!=0) $table->addColumn("Kelas VVIP",1,1)->addColumn($x->durasi_kelas_vvip,1,1,"body");
if($x->durasi_kelas_deluxe!=0) $table->addColumn("Kelas Deluxe",1,1)->addColumn($x->durasi_kelas_deluxe,1,1,"body");
if($x->durasi_kelas_nicu!=0) $table->addColumn("Kelas NICU",1,1)->addColumn($x->durasi_kelas_nicu,1,1,"body");
if($x->durasi_kelas_hcu!=0) $table->addColumn("Kelas HCU",1,1)->addColumn($x->durasi_kelas_hcu,1,1,"body");


$history = json_decode($x->history_ruang,true);
$history = "<pre id='pre_kwitansi_rumus'>".json_encode($history,JSON_PRETTY_PRINT)."</pre>";
$table->addColumn("<strong class='label'>History Ruangan</strong>",2,1,"body",null,"center");
$table->addColumn($history,2,1,"body");

$pack=new ResponsePackage();
$pack->setStatus(ResponsePackage::$STATUS_OK);
$pack->setContent($table->getHtml());
echo json_encode($pack->getPackage());
?>