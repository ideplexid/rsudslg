<?php 
/**
 * dipakai untuk melakukan notify bahwa 
 * untuk pengakuan hutang pasien telah diambil ditanggal sekarang
 * 
 */
global $user;
global $db;
require_once ("smis-base/smis-include-service-consumer.php");

$id                 = $_POST['id'];
$dbtable            = new DBTable($db,"smis_ksr_bayar");
$dbtable->setFetchMethode(DBTable::$ARRAY_FETCH);
$x                  = $dbtable->selectEventDel($id);
$data               = array();
$data['jenis_akun'] = "transaction";
$data['jenis_data'] = "pelunasan_asuransi";
$data['id_data']    = $id;
$data['entity']     = "kasir";
$data['service']    = "get_detail_accounting_pelunasan_asuransi";
$data['data']       = $id;
$data['code']       = "kasir-pelunasan_asuransi-p-".$id;
$data['operation']  = $x['prop'];
$data['tanggal']    = $x['tgl_klaim'];
$data['uraian']     = "Pelunasan Piutang Asuransi ".$x['carabayar']." Pasien ".$x['nama_pasien']." NRM ".$x['nrm_pasien'].", No Reg. ".$x['noreg_pasien']." dengan ID Bayar [".$id."]";
$data['nilai']      = $x['nilai'];

$serv               = new ServiceConsumer($db,"push_notify_accounting",$data,"accounting");
$serv->execute();

$pack = new ResponsePackage();
$pack ->setStatus(ResponsePackage::$STATUS_OK);
echo json_encode($pack->getPackage());
?>