<?php 
    /**
     * menelusuri semua tagihan pada satu entitas untuk satu orang pasien 
     * dengan noreg tertentu
     * - data yang dikirimkan adalah noreg-nya.
     */
    require_once 'kasir/class/service/TagihanService.php';
    $noreg=$_POST['noreg'];	
    $nama=$_POST ['nama_pasien'];
    $nrm= $_POST ['nrm_pasien'];
    $inap=$_POST['inap'];
    $entitas=$_POST ['slug'];
    $lis_get_tagihan=new TagihanService($db, $noreg,$nama,$nrm,$inap,$entitas);
    $lis_get_tagihan->execute();
    $content=$lis_get_tagihan->getContent();
    $pack=new ResponsePackage();
    $pack->setContent($content);
    $pack->setStatus(ResponsePackage::$STATUS_OK);
    echo json_encode($pack->getPackage());
