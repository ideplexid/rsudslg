<?php 
/**
 * this file used for get all patient 
 * that go out from the hospital in
 * given date and export it as excel
 * 
 * @author 		: Nurul Huda
 * @license 	: Apache v3
 * @copyright 	: goblooge@gmail.com
 * @used 		: /kasir/resource/php/laporan_pasien/rekap_tagihan_pasien_total.php
 * @since		: 01 Feb 2017
 * @version		: 1.0.0
 * @database	: smist_ksr_rekap_total
 * */

require_once "smis-libs-out/php-excel/PHPExcel.php"; 
require_once "smis-base/smis-include-service-consumer.php";
loadLibrary("smis-libs-function-excel");
global $user;
global $db;

$dbres=new ServiceResponder($db, new Table(), new SimpleAdapter(), "get_registered_all");
$dbres->addData("setshowall","1");
$dbres->addData("command","list");
$dbres->removeData("download_token");
$data=$dbres->command("list");
$list=$data['content']['d']['data'];

show_error();
/*start - BLOK RESOURCE*/
	$_thin = array ();
	$_thin['borders']=array();
	$_thin['borders']['allborders']=array();
	$_thin['borders']['allborders']['style']=PHPExcel_Style_Border::BORDER_THIN ;

	$_center = array();
	$_center ['alignment']=array();
	$_center ['alignment']['horizontal']=PHPExcel_Style_Alignment::HORIZONTAL_CENTER;
/*end - BLOCK RESOURCE*/

/*start - BLOCK PROPERTIES FILE EXCEL*/
	$file = new PHPExcel ();
	$file->getProperties ()->setCreator ( $user->getUsername() );
	$file->getProperties ()->setLastModifiedBy ( $user->getNameOnly() );
	$file->getProperties ()->setTitle ( "Laporan Rekap Tagihan Total" );
	$file->getProperties ()->setSubject ( "Laporan Rekap Tagihan Total" );
	$file->getProperties ()->setDescription ( "Data Laporan Rekap Tagihan Total " );
	$file->getProperties ()->setKeywords ( "pasien, tagihan, total" );
	$file->getProperties ()->setCategory ( "Kasir" );
/*end - BLOCK PROPERTIES FILE EXCEL*/

/*start - BLOCK PASIEN MASUK*/
	$file->setActiveSheetIndex ( 0 );
	$sheet = $file->getActiveSheet ( 0 );
	$sheet  ->setTitle ( "PASIEN" );	
    $sheet	->mergeCells('A1:O1');
	$sheet	->setCellValue ( "A1", "LAPORAN REKAP TAGIHAN PASIEN" );
	$sheet	->mergeCells ( "A2:A3") ->setCellValue ( "A2", "NO." )
			->mergeCells ( "B2:B3") ->setCellValue ( "B2", "NAMA" )
			->mergeCells ( "C2:C3")->setCellValue ( "C2", "NO.REG" )
			->mergeCells ( "D2:D3")->setCellValue ( "D2", "NRM" )
            ->mergeCells ( "E2:E3")->setCellValue ( "E2", "CARA BAYAR" )
			->mergeCells ( "F2:F3")->setCellValue ( "F2", "MASUK" )
            ->mergeCells ( "G2:G3")->setCellValue ( "G2", "PULANG" )
            ->mergeCells ( "H2:H3")->setCellValue ( "H2", "RUANGAN" )
            ->mergeCells ( "I2:I3")->setCellValue ( "I2", "TAGIHAN" )
            ->mergeCells ( "J2:J3")->setCellValue ( "J2", "ASURANSI" )
            ->mergeCells ( "K2:K3")->setCellValue ( "K2", "BANK" )
            ->mergeCells ( "L2:L3")->setCellValue ( "L2", "CASH" )
            ->mergeCells ( "M2:M3")->setCellValue ( "M2", "DISKON" )
            ->mergeCells ( "N2:O2")->setCellValue ( "N2", "SELISIH" )
            ->setCellValue ( "N3", "KURANG" )
            ->setCellValue ( "O3", "LEBIH" );
	$sheet  ->getStyle ( 'A1:O1' )
            ->applyFromArray ($_center);
		
	$sheet ->getColumnDimension ( "A" )->setAutoSize ( true );
	$sheet ->getColumnDimension ( "B" )->setAutoSize ( true );
	$sheet ->getColumnDimension ( "C" )->setAutoSize ( true );
	$sheet ->getColumnDimension ( "D" )->setAutoSize ( true );
	$sheet ->getColumnDimension ( "E" )->setAutoSize ( true );
	$sheet ->getColumnDimension ( "F" )->setAutoSize ( true );
	$sheet ->getColumnDimension ( "G" )->setAutoSize ( true );
	$sheet ->getColumnDimension ( "H" )->setAutoSize ( true );
	$sheet ->getColumnDimension ( "I" )->setAutoSize ( true );
	$sheet ->getColumnDimension ( "J" )->setAutoSize ( true );
	$sheet ->getColumnDimension ( "K" )->setAutoSize ( true );
    $sheet ->getColumnDimension ( "L" )->setAutoSize ( true );
    $sheet ->getColumnDimension ( "M" )->setAutoSize ( true );
    $sheet ->getColumnDimension ( "N" )->setAutoSize ( true );
	$sheet ->getColumnDimension ( "O" )->setAutoSize ( true );
	$sheet ->getStyle("A1:O2")->getFont()->setBold(true);
	$no=3;
	foreach($list as $x){
		$no++;
        datetime_excel_format($sheet,"E".$no,$x['tanggal']);
        datetime_excel_format($sheet,"F".$no,$x['tanggal_pulang']);
        
        $ruangan="-";
        if($x->uri=="1"){
            $ruangan=ArrayAdapter::format("unslug",$x['kamar_inap']);
        }else{
            $ruangan=ArrayAdapter::format("unslug",$x['jenislayanan']);
        }
        if($ruangan=="") $ruangan="-";
        $selisih=$x['tb_cash']+$x['tb_asuransi']+$x['tb_bank']+$x['tb_diskon']-$x['total_tagihan'];
        if($ruangan=="") $ruangan="-";
        $sheet	->setCellValue ( "A".$no, ($no-3)."." )
                ->setCellValue ( "B".$no, $x['nama_pasien'] )
                ->setCellValue ( "C".$no, "'".ArrayAdapter::format("only-digit6",$x['id']))
                ->setCellValue ( "D".$no, "'".ArrayAdapter::format("only-digit6",$x['nrm']) )
                ->setCellValue ( "G".$no, ArrayAdapter::format("unslug",$x['carabayar']) )
                ->setCellValue ( "H".$no, $ruangan )
                ->setCellValue ( "I".$no, $x['total_tagihan'] )
                ->setCellValue ( "J".$no, $x['tb_asuransi'] )
                ->setCellValue ( "K".$no, $x['tb_bank'] )
                ->setCellValue ( "L".$no, $x['tb_cash'] )
                ->setCellValue ( "M".$no, $x['tb_diskon'] )
                ->setCellValue ( "N".$no, $selisih<0?$selisih:0 )
                ->setCellValue ( "O".$no, $selisih>0?($selisih*1):0 );
	}
    $no++;
    
    $sheet->setCellValue ( "I".$no, "=sum(I4:I".($no-1).")" );
    $sheet->setCellValue ( "J".$no, "=sum(J4:J".($no-1).")" );
    $sheet->setCellValue ( "K".$no, "=sum(K4:K".($no-1).")" );
    $sheet->setCellValue ( "L".$no, "=sum(L4:L".($no-1).")" );
    $sheet->setCellValue ( "M".$no, "=sum(M4:M".($no-1).")" );
    $sheet->setCellValue ( "N".$no, "=sum(N4:N".($no-1).")" );
    $sheet->setCellValue ( "O".$no, "=sum(O4:O".($no-1).")" );
    
    $sheet->mergeCells ( "A".$no.":H".$no)->setCellValue ( "A".$no, "TOTAL" );
	$sheet->getStyle ( 'A2:O'.$no )->applyFromArray ($_thin);
    $sheet->getStyle("A".$no,":O".$no)->getFont()->setBold(true);
    $sheet->getStyle("A1:O3")->getFont()->setBold(true);
/*end - BLOCK PASIEN MASUK*/

$filename= "LAPORAN TOTAL TAGIHAN PASIEN - ".
           " - ".ArrayAdapter::format("date d M Y",$_POST['dari']).
           " - ".ArrayAdapter::format("date d M Y",$_POST['sampai']).".xls";

header ( 'Content-Type: application/vnd.ms-excel' );
header ( 'Content-Disposition: attachment;filename="'.$filename.'"' );
header ( 'Cache-Control: max-age=0' );
$writer = PHPExcel_IOFactory::createWriter ( $file, 'Excel5' );
$writer->save ( 'php://output' );

?>