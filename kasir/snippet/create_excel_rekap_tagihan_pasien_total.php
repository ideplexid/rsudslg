<?php 
/**
 * this file used for get all patient 
 * that go out from the hospital in
 * given date and export it as excel
 * 
 * @author 		: Nurul Huda
 * @license 	: Apache v3
 * @copyright 	: goblooge@gmail.com
 * @used 		: /kasir/resource/php/laporan_pasien/rekap_tagihan_pasien_total.php
 * @since		: 01 Feb 2017
 * @version		: 1.0.0
 * @database	: smist_ksr_rekap_total
 * */

require_once "smis-libs-out/php-excel/PHPExcel.php"; 
require_once "smis-base/smis-include-service-consumer.php";
loadLibrary("smis-libs-function-excel");

global $db;
$dbtable=new DBTable($db, "smist_ksr_rekap_total");
$dbtable->setOrder(" masuk ASC");
$dbtable->setShowAll(true);
$data=$dbtable->view("","0");
$list=$data['data'];

show_error();
/*start - BLOK RESOURCE*/
	$_thin = array ();
	$_thin['borders']=array();
	$_thin['borders']['allborders']=array();
	$_thin['borders']['allborders']['style']=PHPExcel_Style_Border::BORDER_THIN ;

	$_center = array();
	$_center ['alignment']=array();
	$_center ['alignment']['horizontal']=PHPExcel_Style_Alignment::HORIZONTAL_CENTER;
/*end - BLOCK RESOURCE*/

/*start - BLOCK PROPERTIES FILE EXCEL*/
	$file = new PHPExcel ();
	$file->getProperties ()->setCreator ( $user->getUsername() );
	$file->getProperties ()->setLastModifiedBy ( $user->getNameOnly() );
	$file->getProperties ()->setTitle ( "Laporan Rekap Tagihan Total" );
	$file->getProperties ()->setSubject ( "Laporan Rekap Tagihan Total" );
	$file->getProperties ()->setDescription ( "Data Laporan Rekap Tagihan Total " );
	$file->getProperties ()->setKeywords ( "pasien, tagihan, total" );
	$file->getProperties ()->setCategory ( "Kasir" );
/*end - BLOCK PROPERTIES FILE EXCEL*/

/*start - BLOCK PASIEN MASUK*/
	$file->setActiveSheetIndex ( 0 );
	$sheet = $file->getActiveSheet ( 0 );
	$sheet  ->setTitle ( "PASIEN" );	
    $sheet	->mergeCells('A1:N1');
	$sheet	->setCellValue ( "A1", "LAPORAN REKAP TAGIHAN PASIEN" );
	$sheet	->mergeCells ( "A2:A3") ->setCellValue ( "A2", "NO." )
			->mergeCells ( "B2:B3") ->setCellValue ( "B2", "NAMA" )
			->mergeCells ( "C2:C3")->setCellValue ( "C2", "NO.REG" )
			->mergeCells ( "D2:D3")->setCellValue ( "D2", "NRM" )
			->mergeCells ( "E2:E3")->setCellValue ( "E2", "MASUK" )
            ->mergeCells ( "F2:F3")->setCellValue ( "F2", "PULANG" )
            ->mergeCells ( "G2:G3")->setCellValue ( "G2", "RUANGAN" )
            ->mergeCells ( "H2:H3")->setCellValue ( "H2", "TAGIHAN" )
            ->mergeCells ( "I2:I3")->setCellValue ( "I2", "ASURANSI" )
            ->mergeCells ( "J2:J3")->setCellValue ( "J2", "BANK" )
            ->mergeCells ( "K2:K3")->setCellValue ( "K2", "CASH" )
            ->mergeCells ( "L2:L3")->setCellValue ( "L2", "DISKON" )
            ->mergeCells ( "M2:N2")->setCellValue ( "M2", "SELISIH" )
            ->setCellValue ( "M3", "KURANG" )
            ->setCellValue ( "N3", "LEBIH" );
	$sheet  ->getStyle ( 'A1:N1' )
            ->applyFromArray ($_center);
		
	$sheet ->getColumnDimension ( "A" )->setAutoSize ( true );
	$sheet ->getColumnDimension ( "B" )->setAutoSize ( true );
	$sheet ->getColumnDimension ( "C" )->setAutoSize ( true );
	$sheet ->getColumnDimension ( "D" )->setAutoSize ( true );
	$sheet ->getColumnDimension ( "E" )->setAutoSize ( true );
	$sheet ->getColumnDimension ( "F" )->setAutoSize ( true );
	$sheet ->getColumnDimension ( "G" )->setAutoSize ( true );
	$sheet ->getColumnDimension ( "H" )->setAutoSize ( true );
	$sheet ->getColumnDimension ( "I" )->setAutoSize ( true );
	$sheet ->getColumnDimension ( "J" )->setAutoSize ( true );
	$sheet ->getColumnDimension ( "K" )->setAutoSize ( true );
    $sheet ->getColumnDimension ( "L" )->setAutoSize ( true );
    $sheet ->getColumnDimension ( "M" )->setAutoSize ( true );
    $sheet ->getColumnDimension ( "N" )->setAutoSize ( true );
	$sheet ->getStyle("A1:N2")->getFont()->setBold(true);
	$_ruang=array();
	$no=3;
	foreach($list as $x){
		$no++;
        datetime_excel_format($sheet,"E".$no,$x->masuk);
        datetime_excel_format($sheet,"F".$no,$x->pulang);
       
        $ruangan=ArrayAdapter::format("unslug",$x->ruangan);
        if($ruangan=="") $ruangan="-";
		$sheet	->setCellValue ( "A".$no, ($no-3)."." )
				->setCellValue ( "B".$no, $x->nama_pasien )
                ->setCellValue ( "C".$no, "'".ArrayAdapter::format("only-digit6",$x->noreg_pasien))
				->setCellValue ( "D".$no, "'".ArrayAdapter::format("only-digit6",$x->nrm_pasien) )
				->setCellValue ( "G".$no, $ruangan )
                ->setCellValue ( "H".$no, $x->tagihan )
                ->setCellValue ( "I".$no, $x->asuransi )
                ->setCellValue ( "J".$no, $x->bank )
                ->setCellValue ( "K".$no, $x->cash )
                ->setCellValue ( "L".$no, $x->diskon )
                ->setCellValue ( "M".$no, $x->selisih<0?$x->selisih:0 )
                ->setCellValue ( "N".$no, $x->selisih>0?($x->selisih*1):0 );
	}
    $no++;
    
    $sheet->setCellValue ( "G".$no, "=sum(G4:G".($no-1).")" );
    $sheet->setCellValue ( "H".$no, "=sum(H4:H".($no-1).")" );
    $sheet->setCellValue ( "I".$no, "=sum(I4:I".($no-1).")" );
    $sheet->setCellValue ( "J".$no, "=sum(J4:J".($no-1).")" );
    $sheet->setCellValue ( "K".$no, "=sum(K4:K".($no-1).")" );
    $sheet->setCellValue ( "L".$no, "=sum(L4:L".($no-1).")" );
    $sheet->setCellValue ( "M".$no, "=sum(M4:M".($no-1).")" );
    $sheet->setCellValue ( "N".$no, "=sum(N4:N".($no-1).")" );
    
    $sheet->mergeCells ( "A".$no.":G".$no)->setCellValue ( "A".$no, "TOTAL" );
	$sheet->getStyle ( 'A2:N'.$no )->applyFromArray ($_thin);
    $sheet->getStyle("A".$no,":N".$no)->getFont()->setBold(true);
    $sheet->getStyle("A1:N3")->getFont()->setBold(true);
/*end - BLOCK PASIEN MASUK*/

$filename= "LAPORAN TOTAL TAGIHAN PASIEN - ".
           " - ".ArrayAdapter::format("date d M Y",$_POST['dari']).
           " - ".ArrayAdapter::format("date d M Y",$_POST['sampai']).".xls";

header ( 'Content-Type: application/vnd.ms-excel' );
header ( 'Content-Disposition: attachment;filename="'.$filename.'"' );
header ( 'Cache-Control: max-age=0' );
$writer = PHPExcel_IOFactory::createWriter ( $file, 'Excel5' );
$writer->save ( 'php://output' );

?>