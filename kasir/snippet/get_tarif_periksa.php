<?php 
require_once 'smis-base/smis-include-service-consumer.php';
global $db;
$ruang=$_POST['ruangan'];
$konsultasi_harga_service = new ServiceConsumer ( $this->db, "get_konsultasi", array (
        "ruangan" => $ruang 
), "manajemen" );
$konsultasi_harga_service->execute ();
$harga = $konsultasi_harga_service->getContent ();

$response=new ResponsePackage();
$response->setStatus(ResponsePackage::$STATUS_OK);
$response->setContent($harga);
echo json_encode($response->getPackage());
?>