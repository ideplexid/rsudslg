<?php 
global $db;
require_once "smis-base/smis-include-service-consumer.php";

$serv = new ServiceConsumer($db,"set_tutup_tagihan");
$serv ->addData("noreg_pasien",$_POST['noreg_pasien']);
$serv ->addData("tutup_tagihan",$_POST['tutup_tagihan']);
$serv ->execute();

$dbtale = new DBTable($db,"smis_ksr_kolektif");
$dbtale ->update(
    array("tutup_tagihan"=>$_POST['tutup_tagihan']),
    array("noreg_pasien"=>$_POST['noreg_pasien'])
);

$response = new ResponsePackage();
$response ->setStatus(ResponsePackage::$STATUS_OK);
$pack = $response ->getPackage();
echo json_encode($pack);
