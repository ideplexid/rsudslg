<?php 
global $db;
require_once "smis-base/smis-include-duplicate.php";
require_once 'kasir/class/responder/PembayaranResponder.php';
$id    = $_POST['id'];
$dbtab = new DBTable ( $db, "smis_ksr_bayar" );
$dbres = new PembayaranResponder ( $dbtab, new Table(), $adapter );
$dbres->setMetodePembayaran("cash_resep");
$dbres->synchronizeToAccounting($db,$id);

$response = new ResponsePackage();
$response ->setStatus(ResponsePackage::$STATUS_OK);
$response ->setAlertVisible(true);
$response ->setAlertContent("Posting Success","Posting to Accounting Success");
echo json_encode($response->getPackage());
?>