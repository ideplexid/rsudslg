<?php

/**
 * versi lama akan dihapus pada versi berikutnya. 
 * karena memakan resource dan waktu yang cukup lama
 */
 
require_once 'kasir/class/service/ProceedTagihan.php';
$ser = new ProceedTagihanService ( $db, $_POST ['noreg'],$_POST ['nama_pasien'], $_POST ['nrm_pasien'],$_POST['inap'],$_POST['carabayar'],$_POST['namapenanggungjawab'],$_POST['asuransi'],$_POST['perusahaan'],$_POST['alamat'] );$ser->execute ();
$tagihan = $ser->getContent ();
$total = $ser->getTotal ();
$jaspel = $ser->getJaspel ();
require_once 'kasir/class/resource/TotalResume.php';
$resume = new TotalResume ( $total, $jaspel, $_POST ['noreg'], $_POST ['nama_pasien'], $_POST ['nrm_pasien'], $_POST['inap'], $ser->getSelesai (), $ser->getBillPerbagian () );
if ($_POST ['super_command'] == "get_tagihan") {		
	$tabulation = new Tabulator ( "", "" );
	if(getSettings($db, "cashier-tagihan-backup", "0")=="1"){
		$tabulation->add ( "tagihan_backup_inp", "Tambahan Biaya", "", Tabulator::$TYPE_HTML,"fa fa-book", "list_registered.tagihan_backup_input()" );
	}
	if(getSettings($db, "cashier-activate-input-periksa", "0")=="1"){
		$tabulation->add ( "periksa_dokter", "Periksa", "kasir/resource/php/tambahan_biaya_pasien/periksa_dokter.php", Tabulator::$TYPE_INCLUDE );
	}
	$tabulation->add ( "tagihan", "Tagihan", "<div id='list_tagihan'>" . $tagihan . "</div>", Tabulator::$TYPE_HTML );
	$tabulation->add ( "cash", "Cash", "kasir/resource/php/pembayaran_patient/cash.php", Tabulator::$TYPE_INCLUDE );
	$tabulation->add ( "bank", "Bank", "kasir/resource/php/pembayaran_patient/bank.php", Tabulator::$TYPE_INCLUDE );
	$tabulation->add ( "asurant", "Asuransi", "kasir/resource/php/pembayaran_patient/asuransi.php", Tabulator::$TYPE_INCLUDE );
	$tabulation->add ( "discount", "Diskon", "kasir/resource/php/pembayaran_patient/diskon.php", Tabulator::$TYPE_INCLUDE );
	$tabulation->add ( "bayangan", "Bayangan", "kasir/resource/php/pembayaran_patient/bayangan.php", Tabulator::$TYPE_INCLUDE );
	$div=new RowSpan();
	$div->addSpan($tabulation,"7",RowSpan::$TYPE_COMPONENT)
		->addSpan("<div id='resume_html' class='divpanel'>" . $resume->getHtml () . "</div>","5",RowSpan::$TYPE_HTML);
	echo $div->getHtml ();
	return;
} else {
	echo $resume->getHtml ();
	return;
}

?>