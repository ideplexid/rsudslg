<?php 
/**
 * dipakai untuk melakukan notify bahwa 
 * untuk pengakuan hutang pasien telah diambil ditanggal sekarang
 * 
 */
global $user;
global $db;
require_once ("smis-base/smis-include-service-consumer.php");
require_once "kasir/function/get_total_tagihan.php";
$result  = get_total_tagihan($db,$_POST['noreg_pasien']);
$service = new ServiceConsumer ( $db, "update_post_notify", NULL, "registration" );
$service->addData("total_tagihan",$result['total']);
$service->addData("asuransi",$result['asuransi']);
$service->addData("bank",$result['bank']);
$service->addData("cash",$result['cash']);
$service->addData("diskon",$result['diskon']);
$service->addData("noreg_pasien",$_POST['noreg_pasien']);
$service->addData('id',$_POST ['noreg_pasien']);
$service->execute ();
$content = $service->getContent ();
echo json_encode ( $content );

?>