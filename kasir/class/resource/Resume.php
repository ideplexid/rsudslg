<?php

/**
 * this class using for counting the resume for new Mode
 * this one is New Mode
 * @author goblooge
 *
 */

class Resume {
	private $form;
	private $end_form;
	private $table;
	private $selesai;
	private $noreg_pasien;
	private $nrm_pasien;
	private $nama_pasien;
	private $simple_report;
	private $inap;
	public function __construct($total, $kena_jaspel, $noreg, $nama, $nrm, $inap,$persen,$last_out,$last_carapulang,$alasan_gratis,$opsi_gratis) {
		global $db;
		$this->noreg_pasien = $noreg;
		$this->nrm_pasien   = $nrm;
		$this->nama_pasien  = $nama;
		$this->inap         = $inap;
		$jaspel             = $persen * $kena_jaspel / 100; // perhitungan jaspel
		if($inap=="0"){
            $jaspel         = 0;
        }
		$this->selesai      = 1;
		$query              = "SELECT SUM(nilai) as total from smis_ksr_bayar WHERE noreg_pasien='" . $noreg . "' AND prop!='del'";
		$total_dibayar      = $db->get_var($query);
		$sisa_bayar         = $total + $jaspel - $total_dibayar;		
		$tagihan            = new Text('total_tagihan', 'total_tagihan', ArrayAdapter::format("only-money", $total));
		$tjaspel            = new Text('total_jaspel', 'total_jaspel', ArrayAdapter::format("only-money", $jaspel));
		$kjaspel            = new Text('kena_jaspel', 'kena_jaspel', ArrayAdapter::format("only-money", $kena_jaspel));
		$tagjas             = new Text('total_tagjas', 'total_tagjas', ArrayAdapter::format("only-money", $total + $jaspel));
		$dibayar            = new Text('total_dibayar', 'total_dibayar', ArrayAdapter::format("only-money", $total_dibayar));
		$sisa               = new Text('sisa_dibayar', 'sisa_dibayar', ArrayAdapter::format("only-money", $sisa_bayar));
		$waktu_pulang       = new Text('waktu_pulang', 'waktu_pulang', $last_out);
		$waktu_pulang 		->setAtribute("disabled='disabled'");
		$waktu_pulang		->setModel(Text::$DATETIME);

		$tagihan    		->setDisabled(true);
		$dibayar    		->setDisabled(true);
		$sisa       		->setDisabled(true);
		$tagjas     		->setDisabled(true);
		$tjaspel    		->setDisabled(true);
		$kjaspel    		->setDisabled(true);

		$form  		 = new Form("form_resume", "", "");
		$form  		 ->addElement("Tagihan", $tagihan)
               		 ->addElement("Kena Jaspel ", $kjaspel)
               		 ->addElement("Jaspel (".$persen."%)", $tjaspel)
               		 ->addElement("Total", $tagjas)
               		 ->addElement("Dibayar", $dibayar)
               		 ->addElement("Sisa", $sisa);
 
		$hitung 	 = new Button('select', 'Hitung', 'Hitung');
		$hitung 	 ->setClass("btn-primary")
			    	 ->setIcon("fa fa-calculator")
			    	 ->setAction("hitung_ulang()")
			    	 ->setIsButton(Button::$ICONIC);
		 
 		$kwitansi 	 = new Button('cetak_kwitansi', '', 'Kwitansi');
 		$kwitansi 	 ->setClass("btn-primary")
 				  	 ->setIsButton(Button::$ICONIC_TEXT)
 				  	 ->setIcon("fa fa-print")
 				  	 ->setAction("list_registered.tagihan_kwitansi()");
							
 		$tgh_lengkap = new Button('cetak_lengkap', '', 'Tagihan Lengkap');
 		$tgh_lengkap ->setClass("btn-primary")
 					 ->setIsButton(Button::$ICONIC_TEXT)
 					 ->setIcon("fa fa-book")
 					 ->setAction("list_registered.active_kasir('tagihan','tagihan')");
		   		
		$tagihan 	 = new Button('webprint_kwitansi2', '', 'Tagihan Web Print');
		$tagihan 	 ->setClass("btn-primary")
			     	 ->setIsButton(Button::$ICONIC_TEXT)
			     	 ->setIcon("fa fa-globe")
			     	 ->setAction("webp	 rint_kwitansi()");
		 
	 	$rincian 	 = new Button('rincian_kwitansi', '', 'Rincian Excel');
		$rincian 	 ->setClass("btn-primary")
			     	 ->setIsButton(Button::$ICONIC_TEXT)
			     	 ->setIcon("fa fa-file-excel-o")
			     	 ->	 setAction("rincian_kwitansi()");
			   	 
		$btgroup 	 = new ButtonGroup("");
		$btgroup 	 ->addButton($hitung)
				 	 ->addButton($tagihan)
				 	 ->addButton($rincian)
 				 	 ->addButton($kwitansi)
 				 	 ->addButton($tgh_lengkap);
				
		$ksr_check_manual             = getSettings($db, "cashier-out-check-manual", "0");
		$var_checkbox_manual          = "";
		$var_checkbox_manual_name     = "";
		if($ksr_check_manual=="0"){
			$var_checkbox_manual      = new Hidden("cashier_out_checkbox_manual", "", "0");
		}else{
			$var_checkbox_manual_name = " Cek Keuangan ";
			$option                   = new OptionBuilder();
			$option                   ->add("Pulang dengan Status Kabur","0",trim($opsi_gratis)=="Pulang dengan Status Kabur"?1:0)
									  ->add("Gratis","1",trim($opsi_gratis)=="Gratis"?1:0)
									  ->add("Pulang Atas Ijin Kasir","2",trim($opsi_gratis)=="Pulang Atas Ijin Kasir" || trim($opsi_gratis)==""?1:0);
            $var_checkbox_manual      = new Select("cashier_out_checkbox_manual", "Gratis", $option->getContent());
            $var_checkbox_manual      ->addAtribute(" onClick='cek_alasan_gratis()' ");
		}
		
		$form->addElement("", $btgroup);
        $this->form = $form;
        
        $keluar = new OptionBuilder();
		$keluar	->add("Tidak Datang","Tidak Datang",$last_carapulang=="Tidak Datang"?"1":"0")
				->add("Dipulangkan Hidup","Dipulangkan Hidup",$last_carapulang=="Dipulangkan Hidup"||$last_carapulang==""?"1":"0")
				->add("Dipulangkan Mati <=24 Jam","Dipulangkan Mati <=24 Jam",$last_carapulang=="Dipulangkan Mati <=24 Jam"?"1":"0")
				->add("Dipulangkan Mati <=48 Jam","Dipulangkan Mati <=48 Jam",$last_carapulang=="Dipulangkan Mati <=48 Jam"?"1":"0")
				->add("Dipulangkan Mati >48 Jam","Dipulangkan Mati >48 Jam",$last_carapulang=="Dipulangkan Mati >48 Jam"?"1":"0")
				->add("Pulang Paksa","Pulang Paksa",$last_carapulang=="Pulang Paksa"?"1":"0")
				->add("Kabur","Kabur",$last_carapulang=="Kabur"?"1":"0")
				->add("Masuk IGD","Masuk IGD",$last_carapulang=="Masuk IGD"?"1":"0")
				->add("Pindah Kamar","Pindah Kamar",$last_carapulang=="Pindah Kamar"?"1":"0")
				->add("Dirujuk Ke Poli Lain","Rujuk Poli Lain",$last_carapulang=="Rujuk Poli Lain"?"1":"0")
                ->add("Pindah ke RS Lain","Pindah ke RS Lain",$last_carapulang=="Pindah ke RS Lain"?"1":"0")
                ->add("Dirujuk","Dirujuk",$last_carapulang=="Dirujuk"?"1":"0");
        $result_keluar = $keluar->getContent();
        
		$tombol_name	= getSettings($db,"cashier-df-out-button","Pulang");
		$carapulang     = new Select("carapulang", "carapulang", $result_keluar);
		$carapulang 	->setAtribute("disabled='disabled'");
		$dibayar        = new Text('total_dibayar', 'total_dibayar', ArrayAdapter::format("only-money", $total_dibayar));
		$pulang         = new Button('select', 'Pulang', $tombol_name);
		$pulang         ->setClass("btn-primary")
                        ->setIcon(" fa fa-sign-out")
                        ->setAction("pasien_pulang('" . $sisa_bayar . "',SELESAI_SEMUA,'" . $noreg . "')")
                        ->setIsButton(Button::$ICONIC_TEXT);

        $alasan = new OptionBuilder();
        $alasan ->add("","",$alasan_gratis==""?1:0);
        $alasan ->add("Kartu Sehat","Kartu Sehat",$alasan_gratis=="Kartu Sehat"?1:0);
        $alasan ->add("Keterangan Tidak Mampu","Keterangan Tidak Mampu",$alasan_gratis=="Keterangan Tidak Mampu"?1:0);
        $alasan ->add("Lain-Lain","Lain-Lain",$alasan_gratis=="Lain-Lain"?1:0);

        $select         = new Select("alasan_gratis","",$alasan->getContent());
		$end_form       = new Form("form_pasien", "", "");
		$end_form       ->addElement("Waktu Pulang", $waktu_pulang)
                        ->addElement("Cara Pulang", $carapulang)
                        ->addElement($var_checkbox_manual_name, $var_checkbox_manual)
                        ->addElement("Alasan",$select)
                        ->addElement("", $pulang);

        
        
		$this->end_form = $end_form;
	}
	
	public function getHtml(){
		require_once "kasir/snippet/update_total_tagihan.php";
		return $this->form->getHtml()."<div class='clear line'></div>".$this->end_form->getHtml();
	}
}