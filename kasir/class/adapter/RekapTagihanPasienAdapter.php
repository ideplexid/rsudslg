<?php
    class RekapTagihanPasienAdapter extends SummaryAdapter {
        public function adapt($d){
            $ar=array();
            if(is_array($d)){
                $ar=$d;
            }else{
                $ar=get_object_vars($d);
            }
            $array=$this->customAdapt($ar);
            $this->sum_value["Tagihan"] += $ar['total_tagihan'] + $ar['administrasi_inap'];
            return $array;
        }

        private function customAdapt($d) {
            $data = array();
            $data['id'] = $d["id"];
            $data['No.'] = ++$this->number;
            $data['No Reg'] = self::format("only-digit9", $d["id"]);
            $data['NRM'] = self::format("only-digit8", $d["nrm"]);
            $data['Nama'] = $d["nama_pasien"];
            $data['No. BPJS'] = $d["nobpjs"];
            $data['Cara Bayar'] = self::format("unslug", $d["carabayar"]);
            $data['Layanan'] = self::format("unslug", $d["jenislayanan"]);
            $data['Plafon'] = self::format("money Rp.", $d["plafon_bpjs"]);
            $data['Tanggal'] = self::format("date d M Y H:i", $d["tanggal"]);
            $data['Kamar'] = self::format("unslug", $d["kamar_inap"]);
            $data['Jenis'] = self::format("trivial_0_URJ_URI", $d["uri"]);
            $data['Nama Asuransi'] = $d["nama_asuransi"];
            $data['Status'] = self::format("trivial_0_Berlangsung_Selesai", $d["selesai"]);
            $data['Tagihan'] = self::format("money Rp.", $d["total_tagihan"] + $d["administrasi_inap"]);
            $data['Asuransi'] = self::format("money Rp.", $d["tb_asuransi"]);
            $data['Bank'] = self::format("money Rp.", $d["tb_bank"]);
            $data['Cash'] = self::format("money Rp.", $d["tb_cash"]);
            $data['Diskon'] = self::format("money Rp.", $d["tb_diskon"]);
            $data['Keterangan'] = $d["diskon_keterangan"];
            return $data;
        }
    }
?>