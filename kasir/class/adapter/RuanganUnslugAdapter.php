<?php 

class RuanganUnslugAdapter extends SimpleAdapter{

    public function adapt($x){
        $result = parent::adapt($x);
        $result ['Ruangan'] = MapRuangan::getRealName($x->ruangan);
        return $result;
    }

}