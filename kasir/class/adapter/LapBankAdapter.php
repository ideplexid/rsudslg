<?php 
require_once "kasir/class/MapRuangan.php";

class LapBankAdapter extends SimpleAdapter{
	
	private $nilai=0;
	private $nilai_cash=0;
	private $nilai_bank=0;
	
	
	
	public function adapt($d){
		$data=parent::adapt($d);
		$this->nilai=$this->nilai+$d['nilai'];
		if($d['di_bank']=="1") {
			$this->nilai_bank=$this->nilai_bank+$d['nilai_tambahan'];
			$data['Tambahan Bank']=self::format("money Rp.", $d['nilai_tambahan']);
			$data['Tambahan Cash']="";				
		}else {
			$data['Tambahan Cash']=self::format("money Rp.", $d['nilai_tambahan']);
			$data['Tambahan Bank']="";
			$this->nilai_cash=$this->nilai_cash+$d['nilai_tambahan'];
		}
		$b=explode("-", $d['nama_bank']);
        $data['Bank']=$b[0];
        $data['Ruangan'] = MapRuangan::getRealName($d['ruangan']);
		return $data;
	}
	
	public function getContent($data){
		$parent=parent::getContent($data);
		$bottom=array();
		$bottom['Tanggal']="<strong>Total</strong>";
		$bottom['Nilai']="<strong>".self::format("money Rp.", $this->nilai)."</strong>";
		$bottom['Tambahan Bank']="<strong>".self::format("money Rp.", $this->nilai_bank)."</strong>";
		$bottom['Tambahan Cash']="<strong>".self::format("money Rp.", $this->nilai_cash)."</strong>";
		$bottom['Keterangan']="";
		$bottom['Noreg']="";
		$bottom['NRM']="";
		$bottom['Nama']="";
		$parent[]=$bottom;
		return $parent;
	}
	
	
	
}


?>