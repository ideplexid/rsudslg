<?php 
require_once "kasir/class/MapRuangan.php";

class LapCashAdapter extends SimpleAdapter{
	
	private $nilai=0;
	
	public function adapt($d){
		$this->nilai=$this->nilai+$d['nilai'];
		$data=parent::adapt($d);
		$data['Ruangan'] = MapRuangan::getRealName($d['ruangan']);
		return $data;
	}
	
	public function getContent($data){
		$parent=parent::getContent($data);
		$bottom=array();
		$bottom['Tanggal']="<strong>Total</strong>";
		$bottom['Nilai']="<strong>".self::format("money Rp.", $this->nilai)."</strong>";
		$bottom['Keterangan']="";
		$bottom['Noreg']="";
		$bottom['NRM']="";
		$bottom['Nama']="";
		$parent[]=$bottom;
		return $parent;
	}
	
	
	
}


?>