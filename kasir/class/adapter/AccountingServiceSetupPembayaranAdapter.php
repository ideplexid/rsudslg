<?php 

class AccountingServiceSetupPembayaranAdapter extends ArrayAdapter{
	private $result;
	private $total;
	public function __construct(){
		$this->total=0;
		$this->result=array();
		parent::__construct();
	}
	
	public function getTotal(){
		return $this->total;
	}
	
	public function adapt($x){		
		$code = $this->getCode($x);		
		if(isset($this->result[$code])){
			$this->result[$code]['nilai']+=abs($x->nilai);
		}else{
			$this->result[$code]=array(
					"code"=>$code,
					"nilai"=>abs($x->nilai)
			);
		}
		$this->total+=$x->nilai;
	}
	
	public function getCode($x){
		switch($x->metode){
			default 			: return "Pembayaran Cash - ".$keterangan;
			case 'asuransi'		: return "Pembayaran Asuransi - ".$x->nama_asuransi." - ".($x->terklaim=='1'?"Lunas":"Hutang");
			case 'cash_resep'	: return "Pembayaran Resep ".$x->no_resep;
			case 'bank'			: return "Pembayaran Bank ".$x->nama_bank;
			case 'diskon'		: return "Potongan - ".$x->keterangan;
		}
	}
	
	public function getContent($data){
		parent::getContent($data);
		return $this->result;
	}
	
	
}

?>