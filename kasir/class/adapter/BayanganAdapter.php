<?php

class BayanganAdapter extends SimpleAdapter{
	private $total_harga;
	private $total_jaspel;
	
	public function adapt($d){
		$a=parent::adapt($d);
		$this->total_harga+=$d['harga'];
		$this->total_jaspel+=$d['jaspel'];
		return $a;
	}
	
	public function getContent($data){
		$hasil=parent::getContent($data);
		$array=array();
		$array['Ruangan']="";
		$array['Layanan']="";
		$array['Tindakan']="<strong>Sub Total</strong>";
		$array['Harga']="<strong>".self::format("money Rp.", $this->total_harga)."</strong>";
		$array['Jaspel']="<strong>".self::format("money Rp.", $this->total_jaspel)."</strong>";
		
		$end=array();
		$end['Ruangan']="";
		$end['Layanan']="";
		$end['Tindakan']="<strong>Total</strong>";
		$end['Harga']="<strong>".self::format("money Rp.", $this->total_harga+$this->total_jaspel)."</strong>";
		$end['Jaspel']="";
		$hasil[]=$array;
		$hasil[]=$end;
		return $hasil;
	}
	
	
}

?>