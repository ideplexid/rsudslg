<?php 

class AccountingServiceSetupTagihanAdapter extends ArrayAdapter{
	private $mode;
	private $result;
	private $total;
	public function __construct($mode){
		$this->mode=$mode;
		$this->result=array();
		$this->total=0;
		parent::__construct();
	}
	
	public function getTotal(){
		return $this->total;
	}
	
	public function adapt($x){
		$code=$this->getCode($x);		
		if(isset($this->result[$code])){
			$this->result[$code]['nilai']+=abs($x->nilai);
		}else{
			$this->result[$code]=array(
					"code"=>$code,
					"nilai"=>abs($x->nilai)
			);
		}		
		$this->total+=$x->nilai;
	}
	
	public function getCode($x){
		$mode=$this->mode[$x->jenis_tagihan];
		switch($mode){
			case 'ng' 	: return $x->nama_grup;
			case 'r'	: return $x->ruangan;
			case 'nt'	: return $x->nama_tagihan;
			case 'ng_r'	: return $x->nama_grup."+".$x->ruangan;
			case 'ng_nt': return $x->nama_grup."+".$x->nama_tagihan;
			case 'r_nt'	: return $x->ruangan."+".$x->nama_tagihan;
			default		: return $x->nama_grup."+".$x->ruangan."+".$x->nama_tagihan;
		}
	}
	
	public function getContent($data){
		parent::getContent($data);
		return $this->result;
	}
	
	
}

?>