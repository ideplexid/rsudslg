<?php

class OksigenManualAdapter extends ArrayAdapter {
	public function adapt($d) {
		$l = array ();
		$l ['id'] = $d['id'];
		$l ['Waktu'] = self::format ( "date d-M-Y", $d['waktu'] );
		$l ['Harga (H)'] = self::format ( "money Rp.", $d['harga_liter'] );
		$l ['Biaya Lain (B)'] = self::format ( "money Rp.", $d['biaya_lain'] );
		$l ['Liter (L)'] = $d['jumlah_liter'];
		$l ['Total (H*L+B)'] = self::format ( "money Rp.", $d['harga'] );
		return $l;
	}
}
?>