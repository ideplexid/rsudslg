<?php 

class RuanganAktifManager extends ServiceConsumer{
    public function __construct($db,$service,$data=NULL,$entity="all",$autonomous="all",$tuser=null){
        parent::__construct($db,$service,$data,$entity,$autonomous,$tuser);
        $this->setMode(ServiceConsumer::$KEY_ENTITY);
    }
    
    public function setNoregPatient($noreg){
        $this->addData("noreg_pasien",$noreg);
    }
    
    public function getContent(){
        $result=parent::getContent();
        return $result;
    }
    
    protected function keyEntity(){
		$content=array();
		$result=json_decode($this->result,true);
        $content=array();
		foreach($result as $autonomous_name=>$autonomous){
			foreach($autonomous as $entity_name=>$entity){
                if($entity==1){
                    $one=array();
                    $one['name']=ArrayAdapter::format("unslug",$entity_name);
                    $one['value']=$entity_name;
                    $content[]=$one;
                }
			}
		}
		return $content;
	}
    
}

?>