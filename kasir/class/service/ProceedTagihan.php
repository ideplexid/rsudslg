<?php
require_once ("smis-base/smis-include-service-consumer.php");

/**
 * 
 * this class running o crawl all Bill of patient
 * so tha all system will be shown all
 * bill from patient. it used by kasir/list_registered.php
 * 
 * 
 * @copyright Nurul Huda <goblooge@gmail.com>
 * @license LGPLv3
 * @version 2.0.0
 * @since 17 Aug 2014
 * @author goblooge
 *
 */
class ProceedTagihanService extends ServiceConsumer {
	private $total;
	private $total_jaspel;
	private $status_selesai;
	private $bill_per_bagian;
	private $code;
	private $bed;
	private $is_inap;
	private $noreg;
	private $nama;
	private $nrml;
	private $is_use_jaspel=true;
	private $jenis;
	private $pj;
	private $asuransi;
	private $perusahaan;
	private $alamat;
	
	public function __construct($db, $noreg, $nama,$nrm, $inap, $jenis, $pj, $asuransi, $perusahaan, $alamat) {
		$data = array ();
		$data ['noreg_pasien'] = $noreg;
		$data ['entity_caller'] = "kasir";
		
		$this->total = 0;
		$this->total_jaspel = 0;
		$this->code = 0;
		$this->status_selesai = array ();
		$this->bill_per_bagian = array ();
		$this->is_inap=($inap=="1");
		$this->nama=$nama;
		$this->noreg=$noreg;
		$this->nrm=$nrm;
		$this->jenis=$jenis;
		$this->pj=$pj;
		$this->asuransi=$asuransi;
		$this->perusahaan=$perusahaan;
		$this->alamat=$alamat;
		$this->is_use_jaspel=getSettings($db, "cashier-activate-jaspel", "1")=="1" && $inap=="1";
		parent::__construct ( $db, "get_tagihan", $data );
	}
	public function isInap(){
		return $this->is_inap;
	}
	public function getTotal() {
		return $this->total;
	}
	public function getJaspel() {
		if($this->is_use_jaspel) return $this->total_jaspel;
		else 0;
	}
	public function getSelesai() {
		return $this->status_selesai;
	}
	public function getBillPerbagian() {
		return $this->bill_per_bagian;
	}
	public function proceedBill() {
		$bill = new Tabulator ( "Bill", "" );
		//$bill->add ( "list", "List", $this->proceedTableAccordion (), Tabulator::$TYPE_HTML );
		$bill->add ( "table", "Table", $this->proceedTableResult (), Tabulator::$TYPE_HTML );
		$bill->add ( "bed", "Bed", $this->getBed(), Tabulator::$TYPE_HTML );
		return $bill->getHtml ();
	}
	public function addBedDescription($waktu,$nama,$biaya,$keterangan){
		$a=array("Durasi"=>$waktu,"Bed"=>$nama,"Biaya"=>ArrayAdapter::format("money Rp.", $biaya),"Keterangan"=>$keterangan);		
		$this->bed[]=$a;
	}
	
	public function getBed(){
		$header=array("Bed","Durasi","Biaya","Keterangan");
		$uitable=new Table($header, "",$this->bed,false);
		$uitable->setFooterVisible(false);
		return $uitable->getHtml();
	}
	
	/**
	 * show the bill into accordion
	 * @return string
	 */
	public function proceedTableAccordion() {
		$whole = new Tabulator ( "entity", "", Tabulator::$ACCORDION );
		$number = 0;
		$result = json_decode ( $this->result, true );
		foreach ( $result as $autonomous ) {
			$number ++;
			foreach ( $autonomous as $name => $ct ) { // memecah satu autonomous
				if ($ct ['exist'] == "1") {
					$stat = array ();
					$stat ['selesai'] = $ct ['selesai'];
					$stat ['cara_keluar'] = $ct ['cara_keluar'];
					$stat ['reverse'] = $ct ['reverse'];
					$this->status_selesai [$name] = $stat;
					$data = $ct ['data'];
					$btn = new Button ( "", "", "" );
					$btn->setIcon ( "icon-white icon-list-alt" );
					$btn->setIsButton ( Button::$ICONIC );
					$btn->setClass ( "btn-info" );
					$number ++;
					$subtotal_name = 0;
					
					$one_entity = new Tabulator ( $name, "", Tabulator::$ACCORDION );
					foreach ( $data as $entity => $econtent ) { // memecah dari tiap entity, polijantung, ambulan, rawat
						$jaspel = $econtent ['jasa_pelayanan'] == "1" ? "<font class='badge badge-success'> + </font>" : "";
						$eresult = $econtent ['result'];
						$subtotal = 0;
						$one_price = new Tabulator ( "price-" . $name . $entity, "", Tabulator::$ACCORDION );
						$number ++;
						foreach ( $eresult as $list ) { // memecah tiap bagian entity (masing-masing tindakan)
						                            	// membuat modal untuk keterangan
							$number ++;
							$header = "<div class='layanan-header row '>
									<font class='layanan-title span4 '> <i class='icon-th-list icon-black'></i> " . strtoupper ( $list ['nama'] ) . "  " . $jaspel . "</font>
									<font class='layanan-price span4'>" . ArrayAdapter::format ( "only-money Rp.", $list ['biaya'] ) . "</font>
								</div>";
							$one_price->add ( "price_" . $number, $header, $list ['waktu'] . "</br>" . $list ['keterangan'], Tabulator::$TYPE_HTML );
							$subtotal += $list ['biaya'];
							
							if($entity=="bed"){
								$this->addBedDescription($list ['waktu'], $name, $list ['biaya'], $list ['keterangan']);
							}
						}
						
						$subtotal_name += $subtotal;
						if ($econtent ['jasa_pelayanan'] == "1") {
							$this->total_jaspel += $subtotal;
						}
						
						if ($subtotal != 0 || $name == "registration") {
							if ($entity == "alok")
								$entity = "Alat dan Obat";
							if ($entity == "ok")
								$entity = "Operatie Kamer";
							if ($entity == "vk")
								$entity = "Verlos Kamer";
							if ($entity == "rr")
								$entity = "Recovery Room";
							
							$header = "<div class='entity-header row '>
									<font class='entity-title span4 '> <i class='icon-th icon-black'></i> " . ArrayAdapter::format ( "unslug", $entity ) . "</font>
									<font class='entity-price span4'>" . ArrayAdapter::format ( "only-money Rp.", $subtotal ) . "</font>
								</div>";
							$one_entity->add ( "entity_" . $number, $header, $one_price->getHtml (), Tabulator::$TYPE_HTML );
						}
					}
					$this->bill_per_bagian [$name] = $subtotal_name;
					if ($subtotal_name != 0 || $name == "registration") {
						$header = "<div class='room-header row '>
									<font class='room-title span4 '> <i class='icon-th-large icon-black'></i> " . ArrayAdapter::format ( "unslug", $name ) . "</font>
									<font class='room-price span4'>" . ArrayAdapter::format ( "only-money Rp.", $subtotal_name ) . "</font>
								</div>";
						$whole->add ( "room_" . $number, $header, $one_entity->getHtml (), Tabulator::$TYPE_HTML );
						$this->total += $subtotal_name;
					}
				}
			}
		}
		return $whole->getHtml ();
	}
	
	/**
	 * show the data in table Result
	 * @return string
	 */
	public function proceedTableResult() {
		$content = new TablePrint ( "print-detail-tagihan" );
		$content->addTableClass ( "table table-bordered table-hover table-condensed " );
		$content->setResponsive ( true );
		$content->setMaxWidth ( false );
		
		$title=ArrayAdapter::format("unslug", getSettings($this->db, "smis_autonomous_title", ""));
		$subtitle=ArrayAdapter::format("unslug", getSettings($this->db, "smis_autonomous_sub_title", ""));
		$address=ArrayAdapter::format("unslug", getSettings($this->db, "smis_autonomous_address", ""));
		
		$content->addColumn ( $subtitle . "<br/>" . $title . "<br/>" . $address, 10, 1,"title","","center" );
		
		$content->addColumn ( "NAMA / PJ", 1, 1 );
		$content->addColumn ( ArrayAdapter::format("unslug", $this->nama) . " / " . ArrayAdapter::format("unslug", $this->pj), 5, 1 );
		$content->commit ( "title" );
		$content->addColumn ( "ALAMAT", 1, 1 );
		$content->addColumn ( ArrayAdapter::format("unslug", $this->alamat), 5, 1 );
		$content->commit ( "title" );
		$content->addColumn ( "NRM / NO REG", 1, 1 );
		$content->addColumn ( ArrayAdapter::format("only-digit6", $this->nrm) . " / " . ArrayAdapter::format("only-digit8", $this->noreg), 5, 1 );
		$content->commit ( "title" );
		$content->addColumn ( "JENIS PASIEN", 1, 1 );
		$content->addColumn ( ArrayAdapter::format("unslug", $this->jenis), 5, 1 );
		$content->commit ( "title" );
		if (trim($this->perusahaan) != "") {
			$content->addColumn ( "PERUSAHAAN", 1, 1 );
			$content->addColumn ( ArrayAdapter::format("unslug", $this->perusahaan), 5, 1 );
			$content->commit ( "title" );
		}
		if (trim($this->asuransi) != "") {
			$content->addColumn ( "ASURANSI", 1, 1 );
			$content->addColumn ( ArrayAdapter::format("unslug", $this->asuransi), 5, 1 );
			$content->commit ( "title" );
		}
		
		$content->addColumn ( "RUANGAN", 1, 1 );
		$content->addColumn ( "LAYANAN", 1, 1 );
		$content->addColumn ( "TINDAKAN", 1, 1 );
		$content->addColumn ( "HRG. SAT.", 1, 1 );
		$content->addColumn ( "JML", 1, 1 );
		$content->addColumn ( "SUBTOTAL", 1, 1 );
		if($this->is_use_jaspel) $content->addColumn ( "JASPEL", 1, 1 );
		$content->addColumn ( "Keterangan", 1, 1,null,null,"tagihan_keterangan" );
		$content->commit ( "title" );		
		
		$result = json_decode ( $this->result, true );
		foreach ( $result as $autonomous ) {
			foreach ( $autonomous as $name => $ct ) { // memecah satu autonomous
				if ($ct ['exist'] == "1") {
					$stat = array ();
					$stat ['selesai'] = $ct ['selesai'];
					$stat ['cara_keluar'] = $ct ['cara_keluar'];
					$stat ['reverse'] = $ct ['reverse'];
					$this->status_selesai [$name] = $stat;
					$data = $ct ['data'];
					$btn = new Button ( "", "", "" );
					$btn->setIcon ( "fa fa-arrows-alt" );
					$btn->setIsButton ( Button::$ICONIC );
					$btn->setClass ( "btn-primary" );
					foreach ( $data as $entity => $econtent ) { // memecah dari tiap entity, polijantung, ambulan, rawat
						$eresult = $econtent ['result'];
						$subtotal = 0;
						foreach ( $eresult as $list ) { // memecah tiap bagian entity (masing-masing tindakan)
							$jaspel=$econtent ['jasa_pelayanan'] == "1" ? $list ['biaya']*0.1:0;
							if($this->is_use_jaspel) $this->total_jaspel+=$jaspel;
							$content->addColumn ( ArrayAdapter::format("unslug", $name), 1, 1 );
							$content->addColumn ( ArrayAdapter::format("unslug",  $entity ), 1, 1 );
							$content->addColumn ( ArrayAdapter::format("unslug",  $list ['nama']) , 1, 1 );
							if($name!="kasir" && ( $entity=="tindakan_perawat" || $entity=="bed") ) {
								$qty = $list['jumlah'] != null ? $list['jumlah'] : 0;
								$content->addColumn ( ArrayAdapter::format ( "money Rp.", $list ['biaya']/$qty  ), 1, 1 );
								$content->addColumn ( ArrayAdapter::format("number", $qty) , 1, 1 );
								$content->addColumn ( ArrayAdapter::format ( "money Rp.", $list ['biaya']), 1, 1 );
								$subtotal += $list ['biaya'];
							}else{
								$qty = $list['jumlah'] != null ? $list['jumlah'] : 0;
								$content->addColumn ( ArrayAdapter::format ( "money Rp.", $list ['biaya'] ), 1, 1 );
								$content->addColumn ( ArrayAdapter::format("number", $qty) , 1, 1 );
								$content->addColumn ( ArrayAdapter::format ( "money Rp.", $qty * $list ['biaya'] ), 1, 1 );
								$subtotal += $list ['biaya']*$qty;
							}
							if($this->is_use_jaspel) $content->addColumn (  ArrayAdapter::format ( "money Rp.", $jaspel ), 1, 1 );
							$keterangan= "<div ><a href='#'>" . $list ['waktu'] . "</a></br>" . $list ['keterangan'] . "</div>";
							$content->addColumn ( $keterangan, 1, 1 ,null,null,"tagihan_keterangan");
							$content->commit ( "body" );
							
							
							if($entity=="bed"){
								$this->addBedDescription($list ['waktu'], $name, $list ['biaya'], $list ['keterangan']);
							}
						}
						$this->total += $subtotal;
					}
				}
			}
			
		}
		
		$print_table=new Button("", "", "Cetak");
		$print_table->setAction("cetak_lengkap(1)");
		$print_table->setIcon(" fa fa-print");
		$print_table->setIsButton(Button::$ICONIC_TEXT);
		$print_table->setClass(" btn-primary noprint");
		
		$print_table_nk=new Button("", "", "Tanpa Keterangan");
		$print_table_nk->setAction("cetak_lengkap(0)");
		$print_table_nk->setIcon(" fa fa-print");
		$print_table_nk->setIsButton(Button::$ICONIC_TEXT);
		$print_table_nk->setClass(" btn-primary noprint");
		
		$btngrup=new ButtonGroup("");
		$btngrup->addButton($print_table);
		$btngrup->addButton($print_table_nk);
		
		loadLibrary("smis-libs-function-math");
		if($this->is_use_jaspel){
			$content->addColumn ( "SUBTOTAL", 5, 1 );
			$content->addColumn ( ArrayAdapter::format ( "money Rp.", $this->total ), 1, 1 );
			$content->addColumn ( ArrayAdapter::format ( "money Rp.", $this->total_jaspel ), 1, 1 );
			$content->addColumn ( $btngrup->getHtml(), 1, 2 ,null,null,"tagihan_keterangan");
			$content->commit ( "footer" );
			
			$content->addColumn ( "TOTAL", 5, 1 );
			$content->addColumn ( ArrayAdapter::format ( "only-money Rp.", $this->total + $this->total_jaspel ), 2, 1 );
			$content->commit ( "footer" );
			
			$content->addColumn ( "TERBILANG : " . ArrayAdapter::format("unslug", numbertell($this->total)) . " Rupiah", 6, 1 );
			$content->commit ( "footer" );
		}else{
			$content->addColumn ( "TOTAL", 5, 1 );
			$content->addColumn ( ArrayAdapter::format ( "money Rp.", $this->total ), 1, 1 );
			$content->addColumn ( $btngrup->getHtml(), 1, 2 ,null,null,"tagihan_keterangan");
			$content->commit ( "footer" );
			
			$content->addColumn ( "TERBILANG : " . ArrayAdapter::format("unslug", numbertell($this->total)) . " RUPIAH", 6, 1 );
			$content->commit ( "footer" );
		}
		
		global $user;
		$lokasi=ArrayAdapter::format("unslug", getSettings($this->db, "smis-rs-footer", ""));
		$content->addSpace(3, 1);
		$content->addColumn ( $lokasi.", ".ArrayAdapter::format("date d-m-Y", date("y-m-d")), 10, 1,"footer","","center" );
		$content->addSpace(3, 1);
		$content->addColumn ( "</br></br>", 10, 1,"footer","","center" );
		$content->addSpace(3, 1);
		$content->addColumn ( ArrayAdapter::format("unslug", $user->getNameOnly()), 10, 1,"footer","","center" );
		
		return "<div id='table_lengkap'>".$content->getHtml ().$this->cssjsTable()."</div>";
	}
	
	public function cssjsTable(){
		$st=ob_start();
		?>
		<script type="text/javascript">
			function cetak_lengkap(kode){
				if(kode=="0"){
					$(".tagihan_keterangan").addClass("noprint");
				}
				var data=$("#table_lengkap").clone();
				smis_print(data);
				$(".tagihan_keterangan").removeClass("noprint");
			}
		</script>
		<style type="text/css">
			@media print{
				table.print-detail-tagihan-smis-print-table td, table.print-detail-tagihan-smis-print-table th  { padding: 1px !important; font-size:12px !important; border:1px solid #000 !important; }
				table.print-detail-tagihan-smis-print-table {border-radius:0px !important; }
			}
		</style>
		<?php 
		
		return ob_get_clean();
	}
	
	public function proceedResult() {
		return $this->proceedBill ();
	}
}
?>