<?php

class GetResponseTime extends ServiceConsumer{
	
	public function __construct($db,$noreg,$entity="all",$autonomous="all",$tuser=null){
		$data=array("noreg_pasien"=>$noreg);
		parent::__construct($db,"get_out_time",$data,$entity,$autonomous,$tuser);
		$this->setMode(ServiceConsumer::$CLEAN_BOTH);
	}
	
	public function getResponseTime(){
		
		$content=array();
		$result=json_decode($this->result,true);
		foreach($result as $autonomous_name=>$autonomous){
			foreach($autonomous as $entity_name=>$entity){
				$content[]=$entity;
			}
		}
		
		$curtime=date("Y-m-d H:i:s ");
		$outtime=NULL;
		$lis="";
		foreach($content as $one){
			$lis.="[$one]";
			if($one=="")
				continue;
			else if($one=="0000-00-00" || $one=="0000-00-00 00:00:00"){
				$outtime=NULL;
				break;
			}else if($one==NULL || strcmp ( $one,$outtime )>0){
				$outtime=$one;
			}
		}
		if($outtime==NULL) {
            return "";
        }else {
            loadLibrary("smis-libs-function-time");
            return "( ".minute_different(date("Y-m-d H:i:s"), $outtime)." Menit )";
        }
	}
	
}

?>
