<?php 

require_once ("smis-base/smis-include-service-consumer.php");

class TagihanService extends ServiceConsumer {
	private $code;
	private $bed;
	private $is_inap;
	private $noreg;
	private $nama;
	private $nrml;
	private $selesai;
	private $jaspel_persen;
	private $dbtable;
	private $is_use_jaspel;
	private $db;
	
	public function __construct($db, $noreg, $nama,$nrm, $inap, $entity) {		
		$this->total 			= 0;
		$this->total_jaspel 	= 0;
		$this->code 			= 0;
		$this->status_selesai 	= array ();
		$this->is_inap 			= ($inap=="1");
		$this->nama 			= $nama;
		$this->noreg 			= $noreg;
		$this->nrm 				= $nrm;
		$this->is_use_jaspel	= getSettings($db, "cashier-activate-jaspel", "1")=="1" && $inap=="1";
		$this->dbtable			= new DBTable($db, "smis_ksr_kolektif");
		$this->jaspel_persen	= getSettings($db, "cashier-jaspel-persen", "10");
		$data 					= array ();
		$data ['noreg_pasien'] 	= $noreg;
		$data ['entity_caller'] = "kasir";
		$this->selesai			= 0;
		$this->db				= $db;
		parent::__construct ( $db, "get_tagihan", $data,$entity );
	}
	
	public function getSelesai() {
		return $this->status_selesai;
	}
	
	public function proceed(){	
		loadLibrary("smis-libs-function-medical");
		$tipe						= medical_service_slug();	
		$replace					= array();
		foreach($tipe as $tp){
			$replace[$tp] 			= getSettings($this->db, "cashier-replace-tipe-".$tp, $tp);
			$replace[$tp."-urutan"] = getSettings($this->db, "cashier-replace-order-".$tp, "1");
		}
		$result = json_decode($this->result, true);
		foreach ( $result as $autonomous ) {
			foreach ( $autonomous as $name => $ct ) {
				// memecah satu autonomous
				if ($ct ['exist'] == "1") {

					$this->status_selesai[$name] = array(
                        'selesai' => $ct['selesai'],
                        'real_name' =>isset($ct['real_name'])?$ct['real_name']:ArrayAdapter::format("unslug", $name ),
						'cara_keluar' => $ct['cara_keluar'],
						'reverse' => $ct['reverse'],
					);
					
					$data 	= $ct['data'];
					$btn 	= new Button ( "", "", "" );
					$btn	->setIcon ( "fa fa-arrows-alt" )
							->setIsButton ( Button::$ICONIC )
							->setClass ( "btn-primary" );
					
					$jaspel_ruang 	= getSettings($this->db, "cashier-jaspel-ruang-".$name, "0") == "1";
					$ruangan_map 	= getSettings($this->db, "cashier-map-area-".$name, $name);
					
					foreach ( $data as $entity => $econtent ) {
						// memecah dari tiap entity, polijantung, ambulan, rawat
						$eresult = $econtent ['result'];
						$jaspel_jenis = getSettings($this->db, "cashier-jaspel-".$entity, "0") == "1";
						foreach ( $eresult as $list ) {
                            $save['nama_grup']          = isset($replace[$entity]) ? $replace[$entity] : $entity; 
							$save['nama_pasien']        = $this->nama; 
							$save['id_unit']            = $list['id']; 
							$save['noreg_pasien']       = $this->noreg; 
							$save['nrm_pasien']         = $this->nrm; 
							$save['jenis_tagihan']      = $entity ; 
							$save['nama_tagihan']       = $list ['nama']; 
							$save['nama_by']            = $list ['nama']; 
							$save['ruang_by']           = $name; 
							$save['nilai_by']           = $list ['biaya']; 
							$save['dari']               = $list['start']; 
							$save['sampai']             = $list ['end']; 
							$save['ruangan_map']        = $ruangan_map; 
							$save['ruangan']            = $name; 
							$save['urutan']             = $replace[$entity."-urutan"]; 
							$save['ruangan_kasir']      = isset($list['ruangan_kasir'])?$list['ruangan_kasir']:$name; 
                            $save['keterangan']         = $list['keterangan']; 
                            $save['tanggal']            = $list['waktu']; 
                            $save['nilai']              = $list['biaya']; 
                            $save['quantity']           = isset($list['jumlah']) ? $list['jumlah'] : 1; 
                            
                            $save['nama_dokter']        = isset($list['dokter'])?$list['dokter']:(isset($list['nama_dokter'])?$list['nama_dokter']:""); 
                            $save['id_dokter']          = isset($list['id_dokter'])?$list['id_dokter']:""; 
                            $save['jaspel_persen']      = isset($list['jaspel_persen'])?$list['jaspel_persen']:"0"; 
							$save['jaspel_dokter']      = isset($list['jaspel_dokter'])?$list['jaspel_dokter']:"0"; 
							
							$save['jaspel_persen_perawat']      = isset($list['jaspel_persen_perawat'])?$list['jaspel_persen_perawat']:"0"; 
                            $save['jaspel_perawat']      		= isset($list['jaspel_perawat'])?$list['jaspel_perawat']:"0"; 
							
							

                            $save['urjigd']             = isset($list['urjigd'])?$list['urjigd']:"urj"; 
                            $save['prop']               = "";
                            $save['debet']              = $list['debet'];
                            $save['kredit']             = $list['kredit'];
                            
							/* jika dari atau sampainya yang terbaru malah kosong
							 * mendhing sebaiknya dibiarkan kemungkinan petugas lupa 
							 * di set up ulang bed */
							if($save['dari']    == "") unset($save['dari']);
                            if($save['sampai']  == "") unset($save['sampai']);
    	
							if(is_array($save['keterangan'])) {
								$save['keterangan'] = json_encode($save['keterangan']);
							}
							
							/* khusus jika ada jasa pelayanan di ruangan yang mana diambil 10% 
							 * dari tagihan yang di jaspelkan, tetapi 10% bisa di set di masing-masing
							 * jenis tagihan melalui menu kasir */
							$save['nilai']              = $list ['biaya'];
							$save['jaspel']             = $list['jaspel'] == "1" ? "1" : "0";//$econtent ['jasa_pelayanan'];
							$save['total']              = $list['biaya'] * (100 + $save['jaspel'] * $this->jaspel_persen) / 100;
							$save['akunting_nama']      = isset($list['akunting_nama'])?$list['akunting_nama']:$save['nama_tagihan'];
                            $save['akunting_only']      = isset($list['akunting_only'])?$list['akunting_only']:"0";
                            $save['akunting_nilai']     = isset($list['akunting_nilai'])?$list['akunting_nilai']:$save['total'];
                            
							/* dipakai untuk memastikan uniq tidaknya sebuah data.
							 * id_unit adalah id row dari setiap table ruangan masing-masing
							 * noreg_pasien adalah noreg dari seorang pasien
							 * jenis_tagihan adalah jenis tagihanya pakaha tindakan perawat, tindakan dokter
							 * ruangan adalah nama ruanganya */
							$up = array(
								'id_unit' => $list['id'],
								'noreg_pasien' => $this->noreg,
								'jenis_tagihan' => $entity,	
 								'ruangan' => $name,					//ini tolong jangan dibuang, karena id_unit bisa saja sama, ruangan beda.
							);
							
							
							if($this->dbtable->is_exist($up,true)){
								/* jika id_unit sama, noreg_pasien , jenis tagihan ada, ruanganya juga ada
								 * berarti udah exist tinggal di aktifkan kembali 
                                 * khusus nilai_by,nama_by,ruang_by tidak diset karena akan dihilangkan*/
                                 unset($save['nilai_by']);
                                 unset($save['ruang_by']);
                                 unset($save['nama_by']);                                 
								 $this->dbtable->update($save, $up);
							}else{
								/* ini berarti melakukan saving data baru */
 								$save['nilai_by'] = $list['biaya'];
 								$save['jaspel_by'] = $jaspel_ruang && $jaspel_jenis? "1" : "0";//$econtent ['jasa_pelayanan'];
								$this->dbtable->insert($save);
							}
						}
					}
				}
			}		
		}
	}
	
	public function createStatusKeluar() {
		$noreg			= $this->noreg;
		$layanan		= $this->status_selesai;		
		$table 			= new TablePrint ( "Selesai" );
		$row			= "";
		$this->selesai 	= 1;
		foreach ( $layanan as $autnomous => $status ) {
			$btn  = new Button ( "", "", "" );
			$btn  ->setClass ( "btn-primary" )
				  ->setIcon("fa fa-sign-in")
				  ->setIsButton(Button::$ICONIC)
				  ->setAction ( "buka_kembali(this,'" . $autnomous . "','" . $noreg . "')" );
			$stat = "<a href='#'>Belum Selesai</a>";
			$aksi = "";
			if ($status ['selesai'] == "1") {
				$stat = $status ['cara_keluar'];
				if ($status ['reverse'] != "0")
					$aksi = $btn->getHtml ();
				$this->selesai = 1;
			} else {
				$this->selesai = 0;
			}
			$table->addColumn ( $status['real_name'], 1, 1 );
			$table->addColumn ( $stat, 1, 1 );
			$table->addColumn ( $aksi, 1, 1 );
			$row   = $table->commit ( "body" )->getHtml();
		}
		return $row;
	}
	
	public function proceedResult() {
		$this->proceed();
		$row 		  = $this->createStatusKeluar();
		$a			  = array();
		$a["selesai"] = $this->selesai;
		$a["row"]	  = $row;	
		return $a;
	}
}
?>
