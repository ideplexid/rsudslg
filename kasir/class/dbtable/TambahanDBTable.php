<?php 

/**
 * 
 * this class used for determining
 * and auto synchronize data automatically  
 * to cashier based on it's current needed
 * 
 * @author      : Nurul Huda
 * @license     : LGPLv3
 * @database    : smis_amb_ambulan
 * @version     : 1.0.1
 * @since       : 09-Mar-2017
 * @copyright   : goblooge@gmail.com
 * 
 * */

class TambahanDBTable extends DBTable{
    private $autosynch;
    public function __construct($db,$name,$column=NULL){
        parent::__construct($db,$name,$column);
        $this->autosynch=false;
    }
    /**
     * @brief set whether should auto synchronize or not
     * @param boolean $auto 
     * @return  
     */
    public function setAutoSynch($auto){
        $this->autosynch=$auto;
        return $this;
    }
    
    /**
     * @brief override DBTable->insert();
     */
    public function insert($data,$use_prop=true,$warp=null){
        $result=parent::insert($data,$use_prop,$warp);
        if($this->autosynch){
            $row=$this->select($this->get_inserted_id());
            $this->synchToCashier($row);
        }
		return $result;
	}
    
    /**
     * @brief override DBTable->update();
     */
    public function update($data,$id,$warp=null){
		$result=parent::update($data,$id,$warp);
        if($this->autosynch){
            if(isset($data['prop']) && $data['prop']=="del"){
                $row=$this->select($id,false,false);
                $this->synchToCashier($row,"del");
            }else{
                $row=$this->select($id);
                $this->synchToCashier($row);
            }
            
        }
		return $result;
	}
    
    /**
     * @brief synchronize data through service to cashier
     * @param Array/Object $raw 
     * @param String $prop, if delete then prop should be filled with string 'del' 
     * @return  null
     */
    private function synchToCashier($raw,$prop=""){
        require_once "smis-base/smis-include-service-consumer.php";

        
        $data=array();
        $data['grup_name']      = $raw->jenis_tagihan;
        $data['nama_pasien']    = $raw->nama_pasien;
        $data['noreg_pasien']   = $raw->noreg_pasien;
        $data['nrm_pasien']     = $raw->nrm_pasien;
        $data['entity']         = "kasir";
        $data['list']           = $this->adapt($raw,$prop);
        $serv = new ServiceConsumer($this->get_db(),"proceed_receivable",$data,"kasir");
        $serv->execute();
        $serv->getContent();
    }
	
    /**
     * @brief this function used to adapt one record in this entity
     *          should become how much record in cashier
     * @param Array/Object $d 
     * @param String $prop , if del, then prop should be filled with string 'del' 
     * @return  Array $result that contains data of one record becoming.
     */
    public function adapt($one,$prop=""){
        global $db;
        $oneresult                      = array();
	    $oneresult['waktu']             = ArrayAdapter::format("date d M Y", $one->tanggal);
	    $oneresult['start']             = $one->tanggal;
        $oneresult['end']               = $one->tanggal;
        $crawler_mode                   = getSettings($db, "cashier-activate-crawler-cashier", "0");
        if($crawler_mode=="1"){
            /* Crawler per Item 
            * Biaya adalah Nilai dan Jumlah adalah Jumlah
            * artinya jumlah dan biaya ditampilkan sendiri*/
            $oneresult['nama']          = $one->nama_tagihan . " (" . ArrayAdapter::format("unslug", $one->ruangan) . ")";
            $oneresult['biaya']         = $one->nilai ;
            $oneresult['jumlah']        = $one->jumlah;
            $oneresult['keterangan']    = "Tagihan dari ".ArrayAdapter::format("unslug", $one->ruangan)." , ".$one->keterangan.", ( ".($one->jaspel=="1"?"J":"NJ")." ) ".$one->jumlah." x ".ArrayAdapter::format("only-money Rp.", $one->nilai);
        }else if($crawler_mode=="2"){
            /* Crawler per Item Per Summary 
            * biaya adalah Nilai * Jumlah,
            * sedangkan Jumlah adalah Jumlah
            * sehingga pemanggil service untuk mendapatkan nilai 
            * satuanya harus dibagi dulu dengan jumlah data*/
            $oneresult['nama']          = $one->nama_tagihan . " (" . ArrayAdapter::format("unslug", $one->ruangan) . ")";
            $oneresult['biaya']         = $one->nilai * $one->jumlah;
            $oneresult['jumlah']        = $one->jumlah;
            $oneresult['keterangan']    = "Tagihan dari ".ArrayAdapter::format("unslug", $one->ruangan)." , ".$one->keterangan.", ( ".($one->jaspel=="1"?"J":"NJ")." ) ".$one->jumlah." x ".ArrayAdapter::format("only-money Rp.", $one->nilai);
        }else{
            /* Crawler Summary Item
            * biaya adalah Nilai * Jumlah , sedangkan jumlah dianggap 1.
            * sehingga untuk mendapatkan nilai per itemnya tidak bisa */
            $oneresult['nama']          = $one->nama_tagihan . " (" . ArrayAdapter::format("unslug", $one->ruangan) . ") x ".$one->jumlah;
            $oneresult['biaya']         = $one->nilai * $one->jumlah;
            $oneresult['jumlah']        = 1;
            $oneresult['keterangan']    = "Tagihan dari ".ArrayAdapter::format("unslug", $one->ruangan)." , ".$one->keterangan.", ( ".($one->jaspel=="1"?"J":"NJ")." ) ".$one->jumlah." x ".ArrayAdapter::format("only-money Rp.", $one->nilai);
        }
        $oneresult['id']                = $one->id; 
        $oneresult['ruangan_kasir']     = $one->ruangan;
        $oneresult['jaspel']            = $one->jaspel;
        $oneresult['debet']             = $one->debet;
        $oneresult['kredit']            = $one->kredit;
        $oneresult['urjigd']        = $one->uri=="1"?"URI":($one->ruangan=="igd"?"IGD":"URJ");
	    $oneresult['tanggal_tagihan'] = substr($one->tanggal,0,10);
        $oneresult['prop']              = $prop;
        $result[]                       = $oneresult;
        return $result;
    }
    
}
