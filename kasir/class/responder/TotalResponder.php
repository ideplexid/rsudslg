<?php

/**
 * kelas ini dipakai untuk melakukan invoke 
 * initialisasi tagihan pasien di kasir
 * sehingga tagihan-tagihan pasien yang ada 
 * di semua ruangan akan dikumpulkan di kasir 
 * melalui menu ini untuk di proses lebih lanjut
 * 
 * @author 		: Nurul Huda
 * @copyright 	: Nurul Huda <goblooge@gmail.com>
 * @license 	: GPLv3
 * @since 		: 4 august 2016
 * @version 	: 1.2
 * @update 		: penambahan invoke untuk javascript 
 * */

require_once 'smis-framework/smis/template/ModulTemplate.php';
require_once ("smis-base/smis-include-service-consumer.php");
class TotalResponder extends ModulTemplate {
	protected $db;
	protected $polislug;
	protected $page;
	protected $protoslug;
	protected $protoname;
	protected $protoimplement;
	protected $action;
	protected $dbtable;
	protected $uitable;
	protected $noreg_pasien;
	protected $nama_pasien;
	protected $nrm_pasien;
	protected $invoke;
	protected $head_title;
	protected $timeout;
	protected $autoskipper;
	public function __construct($db, $polislug = "all", $noreg = "", $nrm = "", $nama = "", $page = "kasir", $action = "total_tagihan", $protoslug = "", $protoname = "", $protoimplement = "",$invoke="NULL",$head_title="Processing...",$timeout=100) {
		$this->db = $db;
		$this->invoke=$invoke;
		$this->noreg_pasien = $noreg;
		$this->nama_pasien = $nama;
		$this->nrm_pasien = $nrm;
		$this->polislug = $polislug;
		$this->page = $page;
		$this->protoslug = $protoslug;
		$this->protoimplement = $protoimplement;
		$this->protoname = $protoname;
		$this->action = $action;
		$this->head_title=$head_title;
		$this->timeout=$timeout;
		$this->autoskipper=100;
	}
	
	public function setTimeout($timeout,$skipper){
		$this->timeout=$timeout;
		$this->autoskipper=$skipper;
		return $this;
	}
	
	public function command($command) {
		global $db;
		require_once "kasir/snippet/".$_POST ['command'].".php";
	}
	
	public function phpPreLoad() {
		$data_post = array ("command" => "edit","id" => $this->noreg_pasien );
		$service = new ServiceConsumer($this->db, "get_registered", $data_post);
		$service->execute ();
		$data = $service->getContent ();
		$need=array();
		if($data!=NULL){
			$need['inap']=$data ['uri'];
			$need['nama_asuransi']=$data ['nama_asuransi'];
			$need['id_asuransi']=$data ['asuransi'];
			$need['nrm_pasien']=$this->nrm_pasien;
			$need['nama_pasien']=$this->nama_pasien;
			$need['noreg_pasien']=$this->noreg_pasien;
		}
		$data_needed="<div id='total_responder_data' class='hide'>".json_encode($need)."</div>";
		$show_kwitansi="<div id='kwitansi_total_responder' ></div>";
		
		$skip=new Button("","","Skip");
		$skip->setIcon(" fa fa-forward");
		$skip->setClass(" btn-primary");
		$skip->setIsButton(Button::$ICONIC_TEXT);
		$skip->setAction("doSkipper()");
		
		$cancel=new Button("","","Cancel");
		$cancel->setIcon(" fa fa-times");
		$cancel->setClass(" btn-danger");
		$cancel->setIsButton(Button::$ICONIC_TEXT);
		$cancel->setAction("doCancel()");
		
		$load=new LoadingBar("rekap_ksr_bar", "");
		$modal=new Modal("rekap_ksr_modal", "", $this->head_title);
		$modal->addHTML("<div class='countdown_boundary'>
						<div id='countdown_crawler' class='countdown'></div>
						<div id='error_msg_crawler'></div>
						</div>","after");
		$modal->addHTML($load->getHtml(),"after");
		$modal->addHTML("<ul id='skipper'></ul>","after");
		$modal->addFooter($skip);
		$modal->addFooter($cancel);
		$modal->setModalSize(Modal::$HALF_MODEL);
		
		
		echo $modal->getHtml();
		echo $data_needed;
		echo $show_kwitansi;
		
		$lks_action				= new Hidden("lks_action","",$this->action);
		$lks_noreg				= new Hidden("lks_noreg","",$this->noreg_pasien);
		$lks_nama_pasien		= new Hidden("lks_nama_pasien","",$this->nama_pasien);
		$lks_nrm_pasien			= new Hidden("lks_nrm_pasien","",$this->nrm_pasien);
		$lks_polislug			= new Hidden("lks_polislug","",$this->polislug);
		$lks_the_page			= new Hidden("lks_the_page","",$this->page);
		$lks_the_protoslug		= new Hidden("lks_the_protoslug","",$this->protoslug);
		$lks_the_protoname		= new Hidden("lks_the_protoname","",$this->protoname);
		$lks_the_protoimplement	= new Hidden("lks_the_protoimplement","",$this->protoimplement);
		$lks_prefix				= new Hidden("lks_prefix","",$this->action);
		$lks_invoke				= new Hidden("lks_invoke","",$this->invoke);
		$lks_timeout			= new Hidden("lks_timeout","",$this->timeout);
		$lks_autoskipper		= new Hidden("lks_autoskipper","",$this->autoskipper);
		
		echo $lks_action->getHtml();
		echo $lks_noreg->getHtml();
		echo $lks_nama_pasien->getHtml();
		echo $lks_nrm_pasien->getHtml();
		echo $lks_polislug->getHtml();
		echo $lks_the_page->getHtml();
		echo $lks_the_protoslug->getHtml();
		echo $lks_the_protoname->getHtml();
		echo $lks_the_protoimplement->getHtml();
		echo $lks_prefix->getHtml();
		echo $lks_invoke->getHtml();
		echo $lks_timeout->getHtml();
		echo $lks_autoskipper->getHtml();
	}
	public function jsLoader() {
		echo addJS ( "framework/bootstrap/js/bootstrap-datepicker.js" );
		echo addJS ( "framework/smis/js/table_action.js" );	
		echo addJS ( "base-js/smis-base-loading.js");	
		echo addJS ( "kasir/resource/js/total_responder.js",false);	
		loadLibrary ( "smis-libs-function-javascript" );
	}
	public function cssLoader() {
		echo addCSS ( "framework/bootstrap/css/datepicker.css" );
		echo addCss ("kasir/resource/css/total_responder.css",false);
	}
	
}

?>