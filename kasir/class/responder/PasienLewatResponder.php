<?php 

class PasienLewatResponder extends ServiceResponder{
	public function command($command){
		$pack=new ResponsePackage();
		$content=NULL;
		$status='not-command';	//not-authorized, not-command, fail, success
		$alert=array();
		if($command=="list"){
			if($this->mode==ServiceConsumer::$SINGLE_MODE)
				$content=$this->view();
			else $content=$this->multiview();
			$pack->setContent($content);
			$pack->setStatus(ResponsePackage::$STATUS_OK);
		}else if($command=='save'){
			$content=$this->save();
			$pack->setContent($content);
			$pack->setStatus(ResponsePackage::$STATUS_OK);
			$pack->setAlertVisible(true);
			$pack->setAlertContent("Data Saved", "Your Data Had Been Saved", ResponsePackage::$TIPE_INFO);
		}else if($command=="del"){
			$content=$this->delete();
			$pack->setContent($content);
			$pack->setStatus(ResponsePackage::$STATUS_OK);
			$pack->setAlertVisible(true);
			$pack->setAlertContent("Data Removed", "Your Data Had Been Removed", ResponsePackage::$TIPE_INFO);
		}else if($command=="edit"){
			$content=$this->edit();
			$pack->setContent($content);
			$pack->setStatus(ResponsePackage::$STATUS_OK);
		}else if($command=="select"){
			$content=$this->select();
			$pack->setContent($content);
			$pack->setStatus(ResponsePackage::$STATUS_OK);
		}else if($command=="print-element"){
			$content=$this->printing("print-element");
			$pack->setContent($content);
			$pack->setStatus(ResponsePackage::$STATUS_OK);
		}else if($command=="printing"){
			$content=$this->printing();
			$pack->setContent($content);
			$pack->setStatus(ResponsePackage::$STATUS_OK);
		}
		return $pack->getPackage();
	}
}

?>