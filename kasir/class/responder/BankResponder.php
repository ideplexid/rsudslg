<?php 

class BankResponder extends PembayaranResponder{
	
	public function excel_element(){
		require_once "/var/www/html/teambaru/smis-libs-out/php-excel/PHPExcel.php";
		global $user;
		$id=$_POST['id'];
		loadLibrary("smis-libs-function-math");
		$_bayar=$this->dbtable->select($id);
		$fileType = 'Excel5';
		$fileName = 'kasir/webprint/bank_tunai.xls';
		$objReader = PHPExcel_IOFactory::createReader($fileType);
		$objPHPExcel = $objReader->load($fileName);
		
		$_kode=$_POST['uri']=="Rawat Inap"?"RI":"RJ";
		$_timestamp=date("Ymd_his");
		$_date=date("d-m-Y");
		$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue('F7', strtoupper($_bayar['nama_pasien']." / NO. RM. ".$_bayar['nrm_pasien']))
					->setCellValue('F13', "PEMBAYARAN PERAWATAN PASIEN / ".strtoupper($_POST['uri'])." / ".ArrayAdapter::format("unslug",$_POST['jenis_pasien']))
					->setCellValue('F10', strtoupper(numbertell($_bayar['nilai'])))
					->setCellValue('M4', ":".$_kode.".03-[MO.MANUAL]")
					->setCellValue('O16', ",".$_date)
					->setCellValue('N20',$user->getNameOnly())
					->setCellValue('B16', ArrayAdapter::format("only-money Rp.",$_bayar['nilai']));
		header ( 'Content-Type: application/vnd.ms-excel' );
		header ( 'Content-Disposition: attachment;filename="JURNAL_03_'.$_bayar['noreg_pasien'].'_'.$_bayar['nrm_pasien'].'_'.$user->getNameOnly().'_'.$_timestamp.'xls"' );
		header ( 'Cache-Control: max-age=0' );
		$writer = PHPExcel_IOFactory::createWriter ( $objPHPExcel, 'Excel5' );
		$writer->save ( 'php://output' );
	}


	public function postToArray(){
		if($this->isSave() && ($_POST['id']=="0" || $_POST['id']=="") ){
			$date 		= date("Ymdhi");
			$settings	= "last-cashier-id-billing-".$date;
			$number		= getSettings($this->dbtable->get_db(),$settings,"0");
        	$number		= $number*1+1;
			setSettings($this->dbtable->get_db(),$settings,$number);
			$idbilling = $date."".ArrayAdapter::format("only-digit3",$number);
			$this->addColumnFixValue("idbilling",$idbilling);
		}
		return parent::postToArray();
	}
	
}

?>