<?php

class NonPasienParentResponder extends DBParentResponder{
        
    
    public function save(){
        $success=parent::save();
        if($success['type']=="update"){
            $this->synchronizeToAccounting($this->dbtable->get_db(),$success['id'],"");
        }
        return $success;
    }
    
    public function delete(){
        $result=parent::delete();
        $this->synchronizeToAccounting($this->dbtable->get_db(),$_POST['id'],"del");
        $query = "UPDATE smis_ksr_bayar SET prop='del' WHERE id_np = '".$_POST['id']."' ";
        global $db;
        $db->query($query);
        return $result;
    }
    
    public function synchronizeToAccounting($db,$id,$is_del=""){
        $x=$this->dbtable->selectEventDel($id);
        $data=array();
        $data['jenis_akun'] = "transaction";
        $data['jenis_data'] = "penjualan";//"tagihan_non_pasien";
        $data['id_data']    = $id;
        $data['entity']     = "kasir";
        $data['service']    = "get_detail_accounting_non_pasien";
        $data['data']       = $id;
        $data['code']       = "kasir-non-p-".$id;
        $data['operation']  = $is_del;
        $data['tanggal']    = $x->tanggal;        
        $data['uraian']     = ucwords($x->keterangan)." Atas Nama ".$x->nama;
        $data['nilai']      = $x->nilai;
        require_once 'smis-base/smis-include-service-consumer.php';
        $serv=new ServiceConsumer($db,"push_notify_accounting",$data,"accounting");
        $serv->execute();
    }
}

?>