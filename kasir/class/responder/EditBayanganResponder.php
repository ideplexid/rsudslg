<?php

class EditBayanganResponder extends DBResponder{
	private $pack;
	public static $NON_EDIT=array("penjualan_resep","tindakan_dokter","ambulan","konsultasi_dokter","return_resep","oksigen_central","oksigen_manual","darah","tindakan_perawat_igd","tindakan_igd","ok","vk","rr");
	public static $NON_RUANG=array("administrasi","registration","gizi","fisiotherapy","laboratory","radiology","gizi","darah","return_resep","penjualan_resep");
	
	
	public function save(){
		$data=$this->postToArray();
		$id['id']=$_POST['id'];
		if($_POST['id']==0 || $_POST['id']=="" || !$this->dbtable->is_exist($id,$this->is_exist_include_del)){
			$result=$this->dbtable->insert($data);
			$id['id']=$this->dbtable->get_inserted_id();
			$success['type']='insert';
		}else {
			if(in_array($_POST['jenis_tagihan'], self::$NON_RUANG)){
				unset($data['ruang_by']);
			}
			
			$result=$this->dbtable->update($data,$id);
			$success['type']='update';
		}
		$success['id']=$id['id'];
		$success['success']=1;
		if($result===false) $success['success']=0;
		return $success;
	}
	
	
	
	public function command($command){
		$this->pack=new ResponsePackage();
		$content=NULL;
		$status='not-command';	//not-authorized, not-command, fail, success
		$alert=array();
		if($command=="jaspel"){
			$content=$this->jaspel();
			$this->pack->setContent($content);
			$this->pack->setStatus(ResponsePackage::$STATUS_OK);
		}else if($command=="hidden"){
			$content=$this->hidden();
			$this->pack->setContent($content);
			$this->pack->setStatus(ResponsePackage::$STATUS_OK);
		}else if($command=="back"){
			$content=$this->back();
			$this->pack->setContent($content);
			$this->pack->setStatus(ResponsePackage::$STATUS_OK);
		}else if($command=="edit_bayangan_nilai"){
			$content=$this->edit_bayangan();
			$this->pack->setContent($content);
			$this->pack->setStatus(ResponsePackage::$STATUS_OK);
		}else if($command=="load_all"){
			$content=$this->load_all();
			$this->pack->setContent($content);
			$this->pack->setStatus(ResponsePackage::$STATUS_OK);
		}else if($command=="balance_scale"){
			$content=$this->balance_scale();
			$this->pack->setContent($content);
			$this->pack->setStatus(ResponsePackage::$STATUS_OK);
		}else if($command=="load_one"){
			$content=$this->load_one();
			$this->pack->setContent($content);
			$this->pack->setStatus(ResponsePackage::$STATUS_OK);
		}else{
			return parent::command($command);
		}
		
		return $this->pack->getPackage();
	}
	
	public function load_one(){
		$id=$_POST['id'];
		$hidden=$_POST['hidden_all'];
		$jaspel=$_POST['jaspel_by_all'];
		$nilai=$_POST['nilai_by_all'];
		$kelas_by=$_POST['kelas_by_all'];
		$ruang_by=$_POST['ruang_by_all'];
		$nama=$_POST['nama_by_all'];
		$namaruang=$_POST['namaruang_by_all'];
		
		
		if($id=="" || ($id*1)<0 || $id=="0" || ( $hidden=="Biarkan" &&  $jaspel=="biarkan" && $nilai=="Biarkan" ))
			return false;
		
		$up=array();
		$one=$this->dbtable->select($id);
		
		if($hidden!="Biarkan"){
			if($hidden=="Ganti"){
				$up['hidden']=$one->hidden=="0"?"1":"0";
			}else if($hidden=="Hilangkan"){
				$up['hidden']="1";
			}else if($hidden=="Tampilkan"){
				$up['hidden']="0";
			}
		}
		
		if($jaspel!="biarkan"){
			if($jaspel=="Ganti"){
				$up['jaspel_by']=$one->hidden=="0"?"1":"0";
			}else if($jaspel=="Adakan"){
				$up['jaspel_by']="1";
			}else if($jaspel=="Hilangkan"){
				$up['jaspel_by']="0";
			}else if($jaspel=="Kembalikan"){
				$up['jaspel_by']=$one->jaspel;
			}
		}
		
		if($nilai!="Biarkan" || $nama!="Biarkan"){
			$hasil=null;			
			$hasil=$this->interchange($one->id_unit, $one->jenis_tagihan, $one->ruangan, $ruang_by, $kelas_by);			
			if($nilai=="Kembalikan"){
				$up['nilai_by']=$one->nilai;
			}else if($nilai=="Ubah"){
				if($hasil!=null && $hasil['harga']!=-1 && $hasil['harga']!=0 && $hasil['harga']!="0") {
					$up['nilai_by']=$hasil['harga'];
				}else{
					$up['status']=1;
				}
			}

			if($nama=="Ubah"){
				if($hasil!=null && $hasil['nama']!="") {
					$up['nama_by']=$hasil['nama'];
				}
			}else if($nama=="Kembalikan"){
				$up['nama_by']=$one->nama;
			}
		}
		
		if($namaruang=="Ubah" && !in_array($one->jenis_tagihan, self::$NON_RUANG)){
			$up['ruang_by']=$ruang_by;
		}else if($namaruang=="Kembalikan"){
			$up['ruang_by']=$one->ruangan;
		}
		
		$this->dbtable->update($up, array("id"=>$id));
		return true;
	}
	
	public function load_all(){
		$jenis=$_POST['jenis_tagihan_all'];
		$noreg=$_POST['noreg_pasien'];
		$nama=$_POST['nama_pasien'];
		$nrm=$_POST['nrm_pasien'];
		$ruang_asal_by=$_POST['ruang_asal_by_all'];
		$namabyall=$_POST['nama_by_all'];
		$namaruang=$_POST['namaruang_by_all'];
		
		$hidden=$_POST['hidden_all'];
		$jaspel=$_POST['jaspel_by_all'];
		$nilai=$_POST['nilai_by_all'];
		$idx=array();
		if($hidden=="Tampilkan" && $jaspel=="Kembalikan" && $nilai="Kembalikan" && $namabyall="Kembalikan" && $namaruang=="Kembalikan"){
			$query="UPDATE smis_ksr_kolektif  set nilai_by=nilai, nama_by=nama_tagihan,ruang_by=ruangan, jaspel_by=jaspel, hidden=0, status=0  WHERE nama_pasien ='".$nama."' AND noreg_pasien='".$noreg."' AND jenis_tagihan LIKE '".$jenis."' AND nrm_pasien='".$nrm."' AND ruangan LIKE '".$ruang_asal_by."' AND prop!='del'";
			$this->dbtable->get_db()->get_result($query);
		}else{
			$query="SELECT id FROM smis_ksr_kolektif where nama_pasien ='".$nama."' AND noreg_pasien='".$noreg."' AND jenis_tagihan LIKE '".$jenis."' AND nrm_pasien='".$nrm."' AND ruangan LIKE '".$ruang_asal_by."' AND prop!='del'";
			$ids=$this->dbtable->get_db()->get_result($query);
			foreach($ids as $one){
				$idx[]=$one->id;
			}
		}
		return $idx;
	}
    
    public function balance_scale(){
        $maksimal=$_POST['balance_scale'];
        $total_query="SELECT sum(nilai) as total FROM smis_ksr_kolektif 
                        WHERE noreg_pasien='".$_POST['noreg_pasien']."' 
                        AND akunting_only=0 
                        AND prop!='del' 
                        AND hidden=0";
        $total=$this->getDBTable()->get_db()->get_var($total_query);
        $update['nilai_by']="nilai*".$maksimal."/".$total;
        $filter['noreg_pasien']=$_POST['noreg_pasien'];
        $filter['hidden']="0";
        $filter['prop']="";
        $wrap['nilai_by']=DBController::$NO_STRIP_WRAP;
        $this->getDBTable()->update($update,$filter,$wrap);
		return 1;
	}
	
	public function interchange($id_unit,$jenis_tagihan,$ruangan,$ruang_by,$kelas_by){
		//TODO masukan kode untuk meminta perubahan harga
		//echo $_POST['ruang_by'];
		if(in_array($jenis_tagihan, self::$NON_EDIT)){
			$this->pack->setWarning(true, "Peringatan", ArrayAdapter::format("unslug", $jenis_tagihan).", tidak dapat diubah berdasarkan kelas");
			return null;
		}else if($jenis_tagihan=="administrasi" && $ruang_by!=""){
			return $this->administrasi($id_unit,$ruang_by);
		}else if($jenis_tagihan=="registration" && $ruang_by!=""){
			return $this->registrasi($id_unit,$ruang_by);
		}else if($jenis_tagihan=="fisiotherapy"){
			return $this->fisiotherapy($id_unit,$kelas_by);
		}else if($jenis_tagihan=="laboratory"){
			return $this->laboratory($id_unit,$kelas_by,$ruang_by);
		}else if($jenis_tagihan=="radiology"){
			return $this->radiology($id_unit,$kelas_by,$ruang_by);
		}else if($jenis_tagihan=="visite_dokter"){
			return $this->visite($id_unit,$kelas_by,$ruangan);
		}else if($jenis_tagihan=="konsul_dokter"){
			return $this->konsul($id_unit,$kelas_by,$ruangan);
		}else if($jenis_tagihan=="tindakan_perawat"){
			return $this->tindakan_perawat($id_unit,$kelas_by,$ruangan);
		}else if($jenis_tagihan=="audiometry"){
			return $this->audiometry($id_unit,$kelas_by);
		}else if($jenis_tagihan=="spirometry"){
			return $this->spirometry($id_unit,$kelas_by);
		}else if($jenis_tagihan=="bronchoscopy"){
			return $this->bronchoscopy($id_unit,$kelas_by);
		}else if($jenis_tagihan=="endoscopy"){
			return $this->endoscopy($id_unit,$kelas_by);
		}else if($jenis_tagihan=="faal_paru"){
			return $this->faal_paru($id_unit,$kelas_by);
		}else if($jenis_tagihan=="ekg"){
			return $this->ekg($id_unit,$kelas_by);
		}else if($jenis_tagihan=="bed"){
			return $this->bed($id_unit,$ruangan,$ruang_by);
		}else if($jenis_tagihan=="alok"){
			return $this->alok($id_unit,$ruangan,$ruang_by);
		}
		return null;
	}
	
	public function edit_bayangan(){
		return $this->interchange($_POST['id_unit'], $_POST['jenis_tagihan'],$_POST['ruangan'], $_POST['ruang_by'], $_POST['kelas_by']);
	}
	
	public function registrasi($id,$ruang_by){
		$array=array();
		$array['ruang']=$ruang_by;
		$array['id']=$id;
		$sc=new ServiceConsumer($this->dbtable->get_db(), "change_price_registration",$array,"registration");
		$sc->execute();
		$hasil=$sc->getContent();
		return $hasil;
	}
	
	public function administrasi($id,$ruang_by){
		$array=array();
		$array['ruang']=$ruang_by;
		$array['id']=$id;
		$sc=new ServiceConsumer($this->dbtable->get_db(), "change_price_administration",$array,"registration");
		$sc->execute();
		$hasil=$sc->getContent();
		return $hasil;
	}
	
	public function visite($id,$kelas,$ruang){
		$array=array();
		$array['kelas']=$kelas;
		$array['id']=$id;
		$sc=new ServiceConsumer($this->dbtable->get_db(), "change_price_visite",$array,$ruang);
		$sc->execute();
		$harga=$sc->getContent();
		return $harga;
	}
	
	public function audiometry($id,$kelas,$ruang){
		$array=array();
		$array['kelas']=$kelas;
		$sc=new ServiceConsumer($this->dbtable->get_db(), "change_price_audiometry",$array,"manajemen");
		$sc->execute();
		$harga=$sc->getContent();
		return $harga;
	}
	
	public function bed($id,$ruang_asal,$ruang_tujuan){
		$array=array();
		$array['id']=$id;
		$array['ruangan']=$ruang_tujuan;
		$sc=new ServiceConsumer($this->dbtable->get_db(), "change_price_bed",$array,$ruang_asal);
		$sc->execute();
		$harga=$sc->getContent();
		return $harga;
	}
	
	public function alok($id,$ruang_asal,$ruang_tujuan){
		$array=array();
		$array['id']=$id;
		$array['ruangan']=$ruang_tujuan;
		$sc=new ServiceConsumer($this->dbtable->get_db(), "change_price_alok",$array,$ruang_asal);
		$sc->execute();
		$harga=$sc->getContent();
		return $harga;
	}
	
	public function ekg($id,$kelas,$ruang){
		$array=array();
		$array['kelas']=$kelas;
		$sc=new ServiceConsumer($this->dbtable->get_db(), "change_price_ekg",$array,"manajemen");
		$sc->execute();
		$harga=$sc->getContent();
		return $harga;
	}
	
	public function bronchoscopy($id,$kelas,$ruang){
		$array=array();
		$array['kelas']=$kelas;
		$sc=new ServiceConsumer($this->dbtable->get_db(), "change_price_bronchoscopy",$array,"manajemen");
		$sc->execute();
		$harga=$sc->getContent();
		return $harga;
	}
	
	public function faal_paru($id,$kelas,$ruang){
		$array=array();
		$array['kelas']=$kelas;
		$sc=new ServiceConsumer($this->dbtable->get_db(), "change_price_faal_paru",$array,"manajemen");
		$sc->execute();
		$harga=$sc->getContent();
		return $harga;
	}
	
	public function endoscopy($id,$kelas,$ruang){
		$array=array();
		$array['kelas']=$kelas;
		$sc=new ServiceConsumer($this->dbtable->get_db(), "change_price_endoscopy",$array,"manajemen");
		$sc->execute();
		$harga=$sc->getContent();
		return $harga;
	}
	
	public function spirometry($id,$kelas,$ruang){
		$array=array();
		$array['kelas']=$kelas;
		$sc=new ServiceConsumer($this->dbtable->get_db(), "change_price_spirometry",$array,"manajemen");
		$sc->execute();
		$harga=$sc->getContent();
		return $harga;
	}
	
	public function tindakan_perawat($id,$kelas,$ruang){
		$array=array();
		$array['kelas']=$kelas;
		$array['id']=$id;
		$sc=new ServiceConsumer($this->dbtable->get_db(), "change_price_tindakan_perawat",$array,$ruang);
		$sc->execute();
		$harga=$sc->getContent();
		return $harga;
	}
	
	public function konsul($id,$kelas,$ruang){
		$array=array();
		$array['kelas']=$kelas;
		$array['id']=$id;
		$sc=new ServiceConsumer($this->dbtable->get_db(), "change_price_konsul",$array,$ruang);
		$sc->execute();
		$harga=$sc->getContent();
		return $harga;
	}
	
	public function fisiotherapy($id,$kelas){
		$array=array();
		$array['kelas']=$kelas;
		$array['id']=$id;
		$sc=new ServiceConsumer($this->dbtable->get_db(), "change_price",$array,"fisiotherapy");
		$sc->execute();
		$hasil=$sc->getContent();
		return $hasil;
	}
	
	public function laboratory($id,$kelas,$ruang_by){
		$array=array();
		$array['kelas']=$kelas;
		$array['id']=$id;
		$array['ruang']=$ruang_by;
		$sc=new ServiceConsumer($this->dbtable->get_db(), "change_price",$array,"laboratory");
		$sc->execute();
		$harga=$sc->getContent();
		return $harga;
	}
	
	public function radiology($id,$kelas,$ruang_by){
		$array=array();
		$array['kelas']=$kelas;
		$array['id']=$id;
		$array['ruang']=$ruang_by;
		$sc=new ServiceConsumer($this->dbtable->get_db(), "change_price",$array,"radiology");
		$sc->execute();
		$harga=$sc->getContent();
		return $harga;
	}
	
	public function jaspel(){		
		$id['id']=$_POST['id'];
		$row=$this->dbtable->select($id);
		$data['jaspel_by']="0";
		if($row->jaspel_by=="0") 
			$data['jaspel_by']="1";
		$result=$this->dbtable->update($data,$id);
		$success['type']='update';
		$success['id']=$id['id'];
		$success['success']=1;
		if($result===false) $success['success']=0;
		return $success;
	}
	
	public function hidden(){
		$id['id']=$_POST['id'];
		$row=$this->dbtable->select($id);
		$data['hidden']="0";
		if($row->hidden=="0")
			$data['hidden']="1";
		$result=$this->dbtable->update($data,$id);
		$success['type']='update';
		$success['id']=$id['id'];
		$success['success']=1;
		if($result===false) $success['success']=0;
		return $success;
	}
	
	public function back(){
		$id['id']=$_POST['id'];
		$row=$this->dbtable->select($id);
		$data['hidden']="0";
		$data['jaspel_by']=$row->jaspel;
		$data['nama_by']=$row->nama_tagihan;
		$data['nilai_by']=$row->nilai;
		$data['ruang_by']=$row->ruangan;
		
		$result=$this->dbtable->update($data,$id);
		$success['type']='update';
		$success['id']=$id['id'];
		$success['success']=1;
		if($result===false) $success['success']=0;
		return $success;
	}
}

?>