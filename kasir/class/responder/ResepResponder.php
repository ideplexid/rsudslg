<?php
class ResepResponder extends ServiceResponder {
	
	private $nokwitansi;
	public function edit() {
		$d = parent::edit ();
		$this->nokwitansi = strtoupper ( "RSP-" . date ( 'dmy-his' ) . "-" . substr ( md5 ( $d ['header'] ['id'] . date ( 'dmyhis' ) ), 1, 4 ) );
		return $this->getTabulator ( $d );
	}
	
	public function getTabulator($d) {
		$d = $this->getDetailResep ( $d );
		$tab = new Tabulator ( "tab_detail_resep", "" );
		$tab->add ( "detil_resep", "Detail Resep", $d ['table_resep'], Tabulator::$TYPE_HTML," fa fa-list" );
		
		$cash_resep=$this->getCashResep($d);
		$bank_resep=$this->getBankResep($d);
		$asuransi_resep=$this->getAsuransiResep($d);
		$tab->add ( "cash_resep", "Cash Resep", $cash_resep, Tabulator::$TYPE_HTML," fa fa-money" );
		$tab->add ( "bank_resep", "Bank Resep", $bank_resep, Tabulator::$TYPE_HTML," fa fa-credit-card" );
		$tab->add ( "asuransi_resep", "Asuransi Resep", $asuransi_resep, Tabulator::$TYPE_HTML," fa fa-university" );
		
		$d ['tabulator'] = $tab->getHtml ();
		$d ['print_kwitansi'] = $d ['table_kwitansi'];
		return $d;
	}
	
	public function getCashResep($d){
		ob_start ();
		require_once 'kasir/class/template/CashResep.php';
		$cash = new CashResep ( $this->getDB (), $d ['header'] ['id'], $d ['header'] ['nama_pasien'], $d ['header'] ['nrm_pasien'], $d ['header'] ['noreg_pasien'], $d ['header'] ['carabayar'] );
		$cash->initialize ();
		$hasil = ob_get_clean ();
		return $hasil;
	}
	public function getBankResep($d){
		ob_start ();
		require_once 'kasir/class/template/BankResep.php';
		$cash = new BankResep ( $this->getDB (), $d ['header'] ['id'], $d ['header'] ['nama_pasien'], $d ['header'] ['nrm_pasien'], $d ['header'] ['noreg_pasien'], $d ['header'] ['carabayar'] );
		$cash->initialize ();
		$hasil = ob_get_clean ();
		return $hasil;
	}
	
	public function getAsuransiResep($d){
		ob_start ();
		require_once 'kasir/class/template/AsuransiResep.php';
		$cash = new AsuransiResep ( $this->getDB (), $d ['header'] ['id'], $d ['header'] ['nama_pasien'], $d ['header'] ['nrm_pasien'], $d ['header'] ['noreg_pasien'], $d ['header'] ['carabayar'] );
		$cash->initialize ();
		$hasil = ob_get_clean ();
		return $hasil;
	}

	public function getFormBayar($d) {
		/* berhubungan dengan pembayaran_resep.php */
		
		$input = new Text ( "resep_dibayar", "Pembayaran", "0" );
		$input->setTypical ( "money" );
		$input->setAtribute ( " data-thousands=\".\" data-decimal=\",\" data-prefix=\"Rp. \"  data-precision=\"2\"  " );
		$lunas = new Hidden ( "dibayar", "", $d ['kurang_bayar'] );
		$waktu = new Hidden ( "waktu_resep", "", date ( 'Y-m-d H:i:s' ) );
		$bayar = new Button ( "", "", "Simpan" );
		$bayar->setIsButton ( Button::$ICONIC_TEXT );
		$bayar->setIcon ( " fa fa-money icon-black " );
		$bayar->setAction ( "bayar_resep()" );
		$bayar->setClass( " btn btn-primary" );
		$blunas = new Button ( "", "", " Lunas " );
		$blunas->setIsButton ( Button::$ICONIC_TEXT );
		$blunas->setIcon ( " fa fa-thumbs-up" );
		$blunas->setClass( " btn btn-primary" );
		$blunas->setAction ( "lunas_resep()" );
		$cetak_kwitansi = new Button ( "", "", "Cetak Kwitansi" );
		$cetak_kwitansi->setAction ( "print_kwitansi_resep('" . $this->nokwitansi . "')" );
		$cetak_kwitansi->setIsButton ( Button::$ICONIC_TEXT );
		$cetak_kwitansi->setIcon ( " fa fa-print");
		$cetak_kwitansi->setClass( " btn btn-inverse" );
		$bgrup = new InputGroup ( "" );
		$bgrup->addComponent ( $input );
		$bgrup->addComponent ( $lunas );
		$bgrup->addComponent ( $blunas );
		$bgrup->addComponent ( $bayar );
		$bgrup->addComponent ( $cetak_kwitansi );
		$form = new Form ( "Pembayaran", "", "" );
		$form->addElement ( "Dibayar", $bgrup );
		$form->addElement ( "", $waktu );
		return "<div id='form_pembayaran'>" . $form->getHtml () . "</div>";
	}
	
	public function getTotalPembayaranResep($id_resep,$ruangan) {
		global $wpdb;
		$query = "SELECT SUM(nilai) FROM smis_ksr_bayar WHERE id_resep='" . $id_resep . "' AND ruangan_resep = '".$ruangan."' AND prop!='del'";
		$total = $wpdb->get_var ( $query );	
		if($total==NULL || $total==""){
			$total = 0;
		}
		return $total*1;
	}
    
    private function notifyPelunasan($id,$lunas,$entity){
        if(getSettings($this->db,"cashier-resep-activate-notify-lunas","0")=="1"){
            $data=array("id"=>$id,"lunas"=>$lunas);
            $serv=new ServiceConsumer($this->db,"set_lunas_penjualan_resep",$data,$entity);
            $serv->execute();            
        }
    }
	
	public function getDetailResep($d) {
		$header = $d ['header'];
		$total = 0;
		$id_resep = $header ['id'];		
		$header ['id'] = ArrayAdapter::format ( "only-digit8", $header ['id'] );
		$header ['nrm_pasien'] = ArrayAdapter::format ( "only-digit8", $header ['nrm_pasien'] );
		$header ['noreg_pasien'] = ArrayAdapter::format ( "only-digit8", $header ['noreg_pasien'] );
		$d ['header'] = $header;
		$jasa_resep=$header["tuslah"]+$header["embalase"]+$header["biaya_racik"];
		$total_bersih=$header["total"];
		$potongan_diskon=$header["diskon"];
		$tdiskon=$header["t_diskon"];
        
        
		$obat_jadi = $d ['detail_jadi'];
		$obat_racikan = $d ['detail_racikan'];
		$obat_return = $d ['detail_retur'];
		$table = new TablePrint ( "resep_obat" );
		$table->setMaxWidth ( false );
		$table->addTableClass ( "table table-condensed table-stripped" );
		$table->addColumn ( "<strong>Obat</strong>", 1, 1 );
		$table->addColumn ( "<strong>Jumlah</strong>", 1, 1 );
		$table->addColumn ( "<strong>Satuan</strong>", 1, 1 );
		$table->addColumn ( "<strong>Total</strong>", 1, 1 );
		$table->commit ( "body" );
		
		foreach ( $obat_jadi as $obat ) {
			$table->addColumn ( $obat ['nama'], 1, 1 );
			$table->addColumn ( abs ( $obat ['jumlah'] ) . " " . $obat ['satuan'], 1, 1 );
			$table->addColumn ( ArrayAdapter::format ( "money Rp.", $obat ['harga_satuan'] ), 1, 1 );
			$table->addColumn ( ArrayAdapter::format ( "money Rp.", $obat ['harga_total']), 1, 1 );
			$table->commit ( "body" );
			$total += abs ( $obat ['harga_total'] );
		}
		
		foreach ( $obat_racikan as $racikan ) {
			$table->addColumn ( $racikan ['nama'], 1, 1 );
			$table->addColumn ( $racikan ['jumlah'] . " Racikan ", 1, 1 );
			$table->addColumn ( ArrayAdapter::format ( "money Rp.", $racikan ['harga_satuan'] ), 1, 1 );
			$table->addColumn ( ArrayAdapter::format ( "money Rp.", $racikan ['harga_total']), 1, 1 );
			$table->commit ( "body" );
			$total += abs ( $racikan ['harga_total'] );
		}
		
		if (count ( $obat_return ) != 0) {
			$table->addColumn ( "<strong>Sub Total Sebelum Return</strong>", 3, 1 );
			$table->addColumn ( "<strong>" . ArrayAdapter::format ( "money Rp.", $total ) . "</strong>", 1, 1 );
			$table->commit ( "body", "warning" );
		}
		
		foreach ( $obat_return as $return ) {
			$table->addColumn ( "Return : " . ArrayAdapter::format ( "date d M Y", $return ['tanggal'] ), 3, 1 );
			$table->addColumn ( ArrayAdapter::format ( "money Rp.", $return ['nominal_retur'] ), 1, 1 );
			$table->commit ( "body" );
			$total -= abs ( $return ['nominal_retur'] );
		}
		
		if (count ( $obat_return ) == 0) {
			$table->addColumn ( "<strong>Sub Total</strong>", 3, 1 );
			$table->addColumn ( ArrayAdapter::format ( "money Rp.", $total ), 1, 1 );
			$table->commit ( "body", "success" );
		} else {
			$table->addColumn ( "<strong>Sub Total Setelah Return</strong>", 3, 1 );
			$table->addColumn ( ArrayAdapter::format ( "money Rp.", $total ), 1, 1 );
			$table->commit ( "body", "success" );
		}
		$table->addColumn("Jasa Resep",3,1);
		$table->addColumn(ArrayAdapter::format("money Rp.",$jasa_resep),1,1);
		$table->commit("body");

		$table->addColumn("Total",3,1);
		$table->addColumn(ArrayAdapter::format("money Rp.",$total+$jasa_resep),1,1);
		$table->commit("body");

		//$total_bersih = $total+$jasa_resep;
		if($tdiskon=="nominal"){
			$table->addColumn("Diskon",3,1);
			$total=$total-($potongan_diskon*1);
			$table->addColumn(ArrayAdapter::format("money Rp.",$potongan_diskon),1,1);
			$table->commit("body");
		}else{
			$table->addColumn("Diskon",3,1);
			$potongan=($total*$potongan_diskon/100);
			$table->addColumn(ArrayAdapter::format("money Rp.",$potongan),1,1);
			$total=$total-$potongan;
			$table->commit("body");
		}
		$total = $total+$jasa_resep;
		$total=$total-$potongan;
		$total_bayar = $this->getTotalPembayaranResep ( $id_resep,$_POST['ruang'] );
		
		$table->addColumn ( "<i>Sudah Dibayar</i>", 3, 1 );
		$table->addColumn ( ArrayAdapter::format ( "money Rp.", $total_bayar ), 1, 1 );
		$table->commit ( "body", "info" );
		
		if ($total - $total_bayar > 0) {
			$table->addColumn ( "<i>Kekurangan</i>", 3, 1 );
			$table->addColumn ( ArrayAdapter::format ( "money Rp.", $total - $total_bayar ), 1, 1 );
			$table->commit ( "body", "error" );
		} else {
			$table->addColumn ( "<i>Lunas</i>", 4, 1 );
			$table->commit ( "body", "error" );
		}
		
		$d ['total_bayar'] = $total_bayar;
		$d ['kurang_bayar'] = $total  - $total_bayar;
		$d ['table_resep'] = $table->getHtml () . $this->getFormBayar ( $d );
		
		global $user;
		$table->addColumn ( "<i>Petugas</i>", 3, 1 );
		$table->addColumn ( $user->getNameOnly (), 1, 1 );
		$table->commit ( "body", "error" );
		
		global $db;
		
		$_SHOW_ID=getSettings($db,"cashier-resep-show-last-id","0")=="1";
		$_ID=0;
		if($_SHOW_ID){
			$query="SELECT MAX(id) as id FROM smis_ksr_bayar WHERE id_resep='".$id_resep."' AND prop!='del' ";
			$_ID=$db->get_var($query);
		}		
		
		$name=strtoupper(getSettings($db, "smis_autonomous_title", "Safethree MIS"));
		$table->addColumn ( "<div style='text-align:center;'>".$name."</div>", 4, 1, "title" );
		if($_SHOW_ID && $_ID!=0){
			$table->addColumn ( "No Kwitansi", 1, 1 );
			$table->addColumn ( ArrayAdapter::format("only-digit10",$_ID), 3, 1 );
			$table->commit ( "title" );
		}
		
		$table->addColumn ( "ID Resep", 1, 1 );
		$table->addColumn ( $d ['header'] ['id'], 1, 1 );
		$table->addColumn ( "Dokter", 1, 1 );
		$table->addColumn ( $d ['header'] ['nama_dokter'], 1, 1 );
		$table->commit ( "title" );
		$table->addColumn ( "Pasien", 1, 1 );
		$table->addColumn ( $d ['header'] ['nama_pasien'], 1, 1 );
		$table->addColumn ( "No Registrasi", 1, 1 );
		$table->addColumn ( $d ['header'] ['noreg_pasien'], 1, 1 );
		$table->commit ( "title" );
		
		$table->addColumn ( "Tanggal", 1, 1 );
		$table->addColumn ( ArrayAdapter::format ( "date d M Y", $d ['header'] ['tanggal'] ), 1, 1 );
		$table->addColumn ( "No Referensi", 1, 1 );
		$table->addColumn ( $this->nokwitansi, 1, 1 );
		$table->commit ( "title" );
		$table->setTableClass ( "" );
		$table->setName ( "table_print_kwitansi" );
		$d ['table_kwitansi'] = $table->getHtml ();
        
        $id_resep=$d ['header'] ['id']*1;
        $lunas=$d ['kurang_bayar']>0?0:1;
        $entity=$_POST['ruang'];
        $this->notifyPelunasan($id_resep,$lunas,$entity);
		return $d;
	}
}

?>
