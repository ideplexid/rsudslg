<?php 
/**
 * 
 * handling excel creation of LaporanCash
 * 
 * @author goblooge
 * @since 29 March 2016
 * @copyright LGPL v2
 * @version 1.0.0
 */

ini_set("max_memory_limit","512M");
class LaporanCashPerNomorResponder extends DBResponder{
	
	/**
	 * (non-PHPdoc)
	 * @see DBResponder::view()
	 */
	public function excel(){
		global $user;
		include 'smis-libs-out/php-excel/PHPExcel.php';
		$date_range=$_POST['dari']." - ". $_POST['sampai'];
		
        
		//loadLibrary("smis-libs-function-export");
		$kriteria = isset($_POST['kriteria'])?$_POST['kriteria']:""; 
		$kriteria = $this->dbtable->escaped_string($kriteria);
		$number	  = (isset($_POST['number']) && $_POST['number']!="")?$_POST['number']:0;
		$max	  = isset($_POST['max'])?$_POST['max']:"10";		
		$this->dbtable->setMaximum($max);		
		$this->dbtable->setShowAll(true);
		$this->dbtable->setFetchMethode(DBTable::$OBJECT_FETCH);

        
		$d		      = $this->dbtable->view($kriteria,$number);
		$start_number = $page*$max;
		$this->adapter->setNumber($start_number);
		$data=$d['data'];
		$objPHPExcel  = new PHPExcel();
		$properties   = $objPHPExcel->getProperties();
		$properties->setCreator($user->getNameOnly());
		$properties->setLastModifiedBy($user->getNameOnly());
		$properties->setTitle("Laporan Cash");
		$properties->setSubject("Laporan Cash Kasir Syamrabu");
		$properties->setDescription("Laporan Cash Kasir Syamrabu ".$date_range);
		
		$sheet = $objPHPExcel->setActiveSheetIndex(0);
		$sheet->setTitle ( "Laporan Cash");
		$sheet->mergeCells('A1:K1')->setCellValue("A1","Laporan Pembayaran Tunai Pasien ".$_POST['jenis'])->getStyle ( "A1")->getFont ()->setBold ( true );
		$sheet->mergeCells('A2:K2')->setCellValue("A2",$date_range)->getStyle ( "A2")->getFont ()->setBold ( true );
		$sheet->mergeCells('A3:K3')->setCellValue("A3","");
		$sheet->getStyle('A1:A2')->getAlignment()->applyFromArray(
				array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,)
		);
		
		$sheet->SetCellValue('A4', 'ID')->getStyle ( "A4")->getFont ()->setBold ( true );
		$sheet->SetCellValue('B4', 'No Kwitansi')->getStyle ( "B4")->getFont ()->setBold ( true );
		$sheet->SetCellValue('C4', 'Tanggal')->getStyle ( "C4")->getFont ()->setBold ( true );
		$sheet->SetCellValue('D4', 'Ruangan')->getStyle ( "D4")->getFont ()->setBold ( true );
		$sheet->SetCellValue('E4', 'Carabayar')->getStyle ( "E4")->getFont ()->setBold ( true );
		$sheet->SetCellValue('F4', 'Nama')->getStyle ( "F4")->getFont ()->setBold ( true );
		$sheet->SetCellValue('G4', 'No Reg')->getStyle ( "G4")->getFont ()->setBold ( true );
		$sheet->SetCellValue('H4', 'NRM')->getStyle ( "H4")->getFont ()->setBold ( true );
		$sheet->SetCellValue('I4', 'Nilai')->getStyle ( "I4")->getFont ()->setBold ( true );
		$sheet->SetCellValue('J4', 'Keterangan')->getStyle ( "J4")->getFont ()->setBold ( true );
		$sheet->SetCellValue('K4', 'Operator')->getStyle ( "K4")->getFont ()->setBold ( true );
		$ROWS_NUMBER=5;
		$TOTAL_NILAI=0;
		foreach($data as $one){
			$sheet->SetCellValue('A'.$ROWS_NUMBER, $one->id);
			$sheet->SetCellValue('B'.$ROWS_NUMBER, $one->no_kwitansi);
			$sheet->SetCellValue('C'.$ROWS_NUMBER, ArrayAdapter::format("date d M Y H:i:s", $one->waktu));
			$sheet->SetCellValueExplicit('D'.$ROWS_NUMBER, ArrayAdapter::format("unslug", $one->ruangan),PHPExcel_Cell_DataType::TYPE_STRING);
			$sheet->SetCellValueExplicit('E'.$ROWS_NUMBER, ArrayAdapter::format("unslug", $one->carabayar),PHPExcel_Cell_DataType::TYPE_STRING);
			$sheet->SetCellValue('F'.$ROWS_NUMBER, $one->nama_pasien);
			$sheet->SetCellValueExplicit('G'.$ROWS_NUMBER, ArrayAdapter::format("only-digit8", $one->noreg_pasien),PHPExcel_Cell_DataType::TYPE_STRING);
			$sheet->SetCellValueExplicit('H'.$ROWS_NUMBER, ArrayAdapter::format("only-digit8", $one->nrm_pasien),PHPExcel_Cell_DataType::TYPE_STRING);
			$sheet->SetCellValue('I'.$ROWS_NUMBER, $one->nilai);
			$sheet->SetCellValue('J'.$ROWS_NUMBER, $one->keterangan);
			$sheet->SetCellValue('K'.$ROWS_NUMBER, $one->operator);
			$ROWS_NUMBER++;
			$TOTAL_NILAI+=$one->nilai;
		}
		$sheet->SetCellValue('A'.$ROWS_NUMBER, "TOTAL")->getStyle ( "A" . $ROWS_NUMBER)->getFont ()->setBold ( true );
		$sheet->SetCellValue('B'.$ROWS_NUMBER, "");
		$sheet->SetCellValue('C'.$ROWS_NUMBER, "");
		$sheet->SetCellValue('D'.$ROWS_NUMBER, "");
		$sheet->SetCellValue('E'.$ROWS_NUMBER, "");
		$sheet->SetCellValue('F'.$ROWS_NUMBER, "");
		$sheet->SetCellValue('G'.$ROWS_NUMBER, "");
		$sheet->SetCellValue('H'.$ROWS_NUMBER, "");
		$sheet->SetCellValue('I'.$ROWS_NUMBER, $TOTAL_NILAI)->getStyle ( "H" . $ROWS_NUMBER)->getFont ()->setBold ( true );
		$sheet->SetCellValue('J'.$ROWS_NUMBER, "");
		$sheet->SetCellValue('K'.$ROWS_NUMBER, "");
		$sheet->getStyle("I5:I".$ROWS_NUMBER)->getNumberFormat()->setFormatCode('#,##0,00');
		$sheet->getStyle('A4:K'.$ROWS_NUMBER)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
		
		$sheet->getColumnDimension("A")->setAutoSize(true);
		$sheet->getColumnDimension("B")->setAutoSize(true);
		$sheet->getColumnDimension("C")->setAutoSize(true);
		$sheet->getColumnDimension("D")->setAutoSize(true);
		$sheet->getColumnDimension("E")->setAutoSize(true);
		$sheet->getColumnDimension("F")->setAutoSize(true);
		$sheet->getColumnDimension("G")->setAutoSize(true);
		$sheet->getColumnDimension("H")->setAutoSize(true);
		$sheet->getColumnDimension("I")->setAutoSize(true);
		$sheet->getColumnDimension("J")->setAutoSize(true);
		$sheet->getColumnDimension("K")->setAutoSize(true);
		
		$filename=$this->dbtable->getName();
		$filename.="-".$kriteria;
		$filename.=" [ ".($start_number)." - ".($start_number+count($uidata))." ]";
		$filename.=" ( ".$date_range.") ";
		$filename.=" - ".$user->getNameOnly();
		
		header ( 'Content-Type: application/vnd.ms-excel' );
		header ( 'Content-Disposition: attachment;filename="'.$filename.'.xls"' );
		header ( 'Cache-Control: max-age=0' );
		$writer = PHPExcel_IOFactory::createWriter ( $objPHPExcel, 'Excel5' );
		$writer->save ( 'php://output' );		
		return NULL;
	}
	
	public function pdf_element(){
        global $db;
        global $user;
        require_once("kasir/function/kwitansi_bodong_pdf.php");
        require_once "kasir/function/increment_bayar.php";
		$user_ke    = $user->getNameOnly();
        $tanggal_ke = date("Y-m-d H:i:s");
		$this->getDBTable()->setFetchMethode(DBTable::$ARRAY_FETCH);
		$p 		    = $this->getDBTable()->select($_POST['id']);
		$path 	    = kwitansi_bodong_pdf($this->getDB(),$p['no_kwitansi']!=""?$p['no_kwitansi']:$p['id'], $p['nama_pasien'], $p['nilai'], $p['keterangan'],$p['operator'],$p['waktu'],"Kasir",$p['cetakan_ke']+1,$user_ke,$tanggal_ke);
		increment_bayar($db,$p['id']);
        return $path;
	}
}
?>