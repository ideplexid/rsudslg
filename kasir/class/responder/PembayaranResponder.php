<?php 

class PembayaranResponder extends DuplicateResponder{
	private $prefix_kwitansi="";
    private $jenis_pembayaran="";
	
    public function command($command){
        
        if($command=="save"){
            $pack=new ResponsePackage();
            $content=NULL;
            $status='not-command';	//not-authorized, not-command, fail, success
            $alert=array();            
            $content=$this->save();
            if($content['success']==0){
                $pack->setAlertContent("Saving Failed", "Saving data failed perhaps caused by data collition, try save again.. !!", ResponsePackage::$TIPE_INFO);
            }else{
                $pack->setAlertContent("Data Saved", "Your Data Had Been Saved", ResponsePackage::$TIPE_INFO);
            }
            $pack->setContent($content);
			$pack->setStatus(ResponsePackage::$STATUS_OK);
			$pack->setAlertVisible(true);
			return $pack->getPackage();
        }else{
            return parent::command($command);
        }
    }
    
    public function pdf_element(){
        global $db;
        global $user;
        require_once("kasir/function/kwitansi_bodong_pdf.php");
        require_once "kasir/function/increment_bayar.php";       
        $this->getDBTable()->setFetchMethode(DBTable::$ARRAY_FETCH);
		$user_ke    = $user->getNameOnly();
        $tanggal_ke = date("Y-m-d H:i:s");
        $p          = $this->getDBTable()->select($_POST['id']);
		$path       = kwitansi_bodong_pdf($this->getDB(),$p['no_kwitansi']!=""?$p['no_kwitansi']:$p['id'], $p['nama_pasien'], $p['nilai'], $p['keterangan'],$p['operator'],$p['waktu'],"Kasir",$p['cetakan_ke']+1,$user_ke,$tanggal_ke);
		increment_bayar($db,$p['id']);
        return $path;
	}
    
    public function save(){
        if(getSettings($db,"cashier-simple-kwitansi-use-own-number-separated","0")=="1"){
            global $user;
            global $db;
            require_once "kasir/class/locker/PembayaranLocker.php";
            $locker=new PembayaranLocker($db,$user,"kasir-pembayaran",$this);
            return $locker->execute();
        }else{
            return $this->saveProceed();
        }
	}
    
    public function setMetodePembayaran($jenis){
        $this->jenis_pembayaran=$jenis;
        return $this;
    }
    
    public function saveProceed(){
        $success= parent::save();
        $this->synchronizeToAccounting($this->dbtable->get_db(),$success['id'],"");
        return $success;
    }
    
    public function delete(){
        $result=parent::delete();
        $this->synchronizeToAccounting($this->dbtable->get_db(),$_POST['id'],"del");
        return $result;
    }
    
    public function is_inap(){
        if(isset($_POST['urji']) && $_POST['urji']=="Rawat Inap"){
            return true;
        }
        return false;
    }
    
    public function setPrefix($prefix){
        $this->prefix_kwitansi=$prefix;
        return $this;
    } 
    
	public function postToArray(){
		if($this->isSave()){
			/*berfungsi membuat nomor kwitansi baru jika kebetulan ID belum ada
			 * dan setting cashier-simple-kwitansi-use-own-number diaktifkan */
			 if( (!isset($_POST['id']) || $_POST['id']=="0" || $_POST['id']=="" || $_POST['id']==null) ){
				/*model reset setiap tahun*/
                $model = getSettings($this->dbtable->get_db(),"cashier-simple-kwitansi-use-own-number","");
                switch($model){
                    case "yearly"       : $this->yearly_model();    break;
                    case "monthly"      : $this->monthly_model();   break;
                    case "palimirma"    : $this->palimirma_model(); break;
                }
			}
		}
		return parent::postToArray();
	}
    
    
    public function monthly_model(){
        $waktu=$_POST['waktu'];
        $tahun=substr($waktu,0,4);
        $thn=substr($tahun,2,2);
        $bulan=substr($waktu,5,2);
        $settings="last-cashier-kwitansi-monthly-".$tahun."-".$bulan;
        if($this->prefix_kwitansi!=""){
            $settings.="-".$this->prefix_kwitansi;
        }
        $number=getSettings($this->dbtable->get_db(),$settings,"0");
        $number=$number*1+1;
        setSettings($this->dbtable->get_db(),$settings,$number);
        $no_kwitansi=$thn.$bulan.ArrayAdapter::format("only-digit4",$number);
        if($this->prefix_kwitansi!=""){
            $no_kwitansi=$this->prefix_kwitansi."-".$no_kwitansi;
        }
        $this->addColumnFixValue("no_kwitansi",$no_kwitansi);
    }
    
    
    /**
     * @brief   this function used to create 
     *          cashier number that will reset every year
     * @return  null
     */
    public function yearly_model(){
        $waktu=$_POST['waktu'];
        $tahun=substr($waktu,0,4);
        $thn=substr($tahun,2,2);
        $settings="last-cashier-kwitansi-yearly-".$tahun;
        if($this->prefix_kwitansi!=""){
            $settings.="-".$this->prefix_kwitansi;
        }
        $number=getSettings($this->dbtable->get_db(),$settings,"0");
        $number=$number*1+1;
        setSettings($this->dbtable->get_db(),$settings,$number);
        $no_kwitansi=$thn.ArrayAdapter::format("only-digit8",$number);
        if($this->prefix_kwitansi!=""){
            $no_kwitansi=$this->prefix_kwitansi."-".$no_kwitansi;
        }
        $this->addColumnFixValue("no_kwitansi",$no_kwitansi);
    }
    
    public function palimirma_model(){
        loadLibrary("smis-libs-function-time");
        $urji="RJ";
        if($this->is_inap()){
            $urji="RI";
        }
        $waktu=$_POST['waktu'];
        $tahun_bulan=substr($waktu,0,7);
        $tahun=substr($tahun_bulan,0,4);
        $bulan=substr($tahun_bulan,5,2);
        
        $number=getSettings($this->dbtable->get_db(),"last-cashier-kwitansi-palimirma-".$urji."-".$tahun_bulan,"0");
        $number=$number*1+1;
        setSettings($this->dbtable->get_db(),"last-cashier-kwitansi-palimirma-".$urji."-".$tahun_bulan,$number);
        $nrm=ArrayAdapter::format("only-digit8",$_POST['nrm_pasien']);
        $bulan=to_romawi($bulan);
        $no_kwitansi=$urji." - ".$number."/".$nrm."/".$bulan."/".$tahun;
        $this->addColumnFixValue("no_kwitansi",$no_kwitansi);
    }
    
    
    public function synchronizeToAccounting($db,$id,$is_del=""){
        $metode=str_replace("_","-",$this->jenis_pembayaran);
        if(getSettings($this->getDBTable()->get_db(),"cashier-acc-enabled-".$metode,"1")=="1"){
            $this->dbtable->setFetchMethode(DBTable::$ARRAY_FETCH);
            $x=$this->dbtable->selectEventDel($id);
            $data=array();
            $data['jenis_akun'] = "transaction";
            $data['jenis_data'] = "penjualan";
            $data['id_data']    = $id;
            $data['entity']     = "kasir";
            $data['service']    = "get_detail_accounting_".$this->jenis_pembayaran;
            $data['data']       = $id;
            $data['code']       = "kasir-".$this->jenis_pembayaran."-p-".$id;
            $data['operation']  = $is_del;
            $data['tanggal']    = $x['waktu'];
            $data['uraian']     = ucwords(strtolower(ArrayAdapter::slugFormat("unslug",$this->jenis_pembayaran)))." ".$x['nama_pasien']." dengan No. ".($x['no_kwitansi']!=""?$x['no_kwitansi']:$x['id']);
            $data['nilai']      = $x['nilai'];
            
            $serv=new ServiceConsumer($db,"push_notify_accounting",$data,"accounting");
            $serv->execute();
        }
    }
	
}

?>