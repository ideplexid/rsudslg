<?php 
/**
 * 
 * handling excel creation of LaporanCashNonPasien
 * 
 * @author goblooge
 * @since 29 March 2016
 * @copyright LGPL v2
 * @version 1.0.0
 */


class LaporanCashNonPasienResponder extends DBResponder{
	
	/**
	 * (non-PHPdoc)
	 * @see DBResponder::view()
	 */
	public function excel(){
		global $user;
		include 'smis-libs-out/php-excel/PHPExcel.php';
		$date_range=ArrayAdapter::format("date d M Y H:i", $_POST['dari'])." - ".ArrayAdapter::format("date d M Y H:i", $_POST['sampai']);
		
		$kriteria=isset($_POST['kriteria'])?$_POST['kriteria']:""; 
		$kriteria=$this->dbtable->escaped_string($kriteria);
        $this->dbtable->setOrder(" id ASC ");
		$number=(isset($_POST['number']) && $_POST['number']!="")?$_POST['number']:0;
		$max=isset($_POST['max'])?$_POST['max']:"10";	
		$this->dbtable->setMaximum($max);		
		$this->dbtable->setShowAll(true);
		$d=$this->dbtable->view($kriteria,$number);
		$start_number=$page*$max;
		$this->adapter->setNumber($start_number);
		$data=$d['data'];
		$objPHPExcel = new PHPExcel();
		$objPHPExcel->getProperties()->setCreator($user->getNameOnly());
		$objPHPExcel->getProperties()->setLastModifiedBy($user->getNameOnly());
		$objPHPExcel->getProperties()->setTitle("Laporan Cash");
		$objPHPExcel->getProperties()->setSubject("Laporan Cash Kasir Syamrabu");
		$objPHPExcel->getProperties()->setDescription("Laporan Cash Kasir Syamrabu ".$date_range);
		
		$objPHPExcel->setActiveSheetIndex(0);
		$objPHPExcel->getActiveSheet()->setTitle ( "Laporan Cash");
		$objPHPExcel->getActiveSheet()->mergeCells('A1:F1')->setCellValue("A1","Laporan Pembayaran Tunai Non Pasien")->getStyle ( "A1")->getFont ()->setBold ( true );
		$objPHPExcel->getActiveSheet()->mergeCells('A2:F2')->setCellValue("A2",$date_range)->getStyle ( "A2")->getFont ()->setBold ( true );
		$objPHPExcel->getActiveSheet()->mergeCells('A3:F3')->setCellValue("A3","");
		$objPHPExcel->getActiveSheet()->getStyle('A1:A2')->getAlignment()->applyFromArray(
				array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,)
		);
		
		$objPHPExcel->getActiveSheet()->SetCellValue('A4', 'No Kwitansi')->getStyle ( "A4")->getFont ()->setBold ( true );
		$objPHPExcel->getActiveSheet()->SetCellValue('B4', 'Tanggal')->getStyle ( "B4")->getFont ()->setBold ( true );
		$objPHPExcel->getActiveSheet()->SetCellValue('C4', 'Nama')->getStyle ( "C4")->getFont ()->setBold ( true );
		$objPHPExcel->getActiveSheet()->SetCellValue('D4', 'Nilai')->getStyle ( "D4")->getFont ()->setBold ( true );
		$objPHPExcel->getActiveSheet()->SetCellValue('E4', 'Keterangan')->getStyle ( "E4")->getFont ()->setBold ( true );
		$objPHPExcel->getActiveSheet()->SetCellValue('F4', 'Operator')->getStyle ( "F4")->getFont ()->setBold ( true );
		$ROWS_NUMBER=5;
		$TOTAL_NILAI=0;
		foreach($data as $one){
			$objPHPExcel->getActiveSheet()->SetCellValue('A'.$ROWS_NUMBER, ArrayAdapter::format("only-digit8", $one->id));
			$objPHPExcel->getActiveSheet()->SetCellValue('B'.$ROWS_NUMBER, ArrayAdapter::format("date d M Y", $one->tanggal));
			$objPHPExcel->getActiveSheet()->SetCellValue('C'.$ROWS_NUMBER, $one->nama);
			$objPHPExcel->getActiveSheet()->SetCellValue('D'.$ROWS_NUMBER, $one->nilai);
			$objPHPExcel->getActiveSheet()->SetCellValue('E'.$ROWS_NUMBER, $one->keterangan);
			$objPHPExcel->getActiveSheet()->SetCellValue('F'.$ROWS_NUMBER, $one->operator);
			$ROWS_NUMBER++;
			$TOTAL_NILAI+=$one->nilai;
		}
		$objPHPExcel->getActiveSheet()->SetCellValue('A'.$ROWS_NUMBER, "TOTAL")->getStyle ( "A" . $ROWS_NUMBER)->getFont ()->setBold ( true );
		$objPHPExcel->getActiveSheet()->SetCellValue('B'.$ROWS_NUMBER, "");
		$objPHPExcel->getActiveSheet()->SetCellValue('C'.$ROWS_NUMBER, "");
		$objPHPExcel->getActiveSheet()->SetCellValue('D'.$ROWS_NUMBER, $TOTAL_NILAI)->getStyle ( "D" . $ROWS_NUMBER)->getFont ()->setBold ( true );
		$objPHPExcel->getActiveSheet()->SetCellValue('E'.$ROWS_NUMBER, "");
		$objPHPExcel->getActiveSheet()->SetCellValue('F'.$ROWS_NUMBER, "");

		$objPHPExcel->getActiveSheet()->getStyle("D5:D".$ROWS_NUMBER)->getNumberFormat()->setFormatCode('#,##0,00');
		$objPHPExcel->getActiveSheet()->getStyle('A4:F'.$ROWS_NUMBER)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
		
		$objPHPExcel->getActiveSheet()->getColumnDimension("A")->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension("B")->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension("C")->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension("D")->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension("E")->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension("F")->setAutoSize(true);
		
		$filename=$this->dbtable->getName();
		$filename.="-".$kriteria;
		$filename.=" [ ".($start_number)." - ".($start_number+count($uidata))." ]";
		$filename.=" ( ".$date_range.") ";
		$filename.=" - ".$user->getNameOnly();
		
		header ( 'Content-Type: application/vnd.ms-excel' );
		header ( 'Content-Disposition: attachment;filename="'.$filename.'.xls"' );
		header ( 'Cache-Control: max-age=0' );
		$writer = PHPExcel_IOFactory::createWriter ( $objPHPExcel, 'Excel5' );
		$writer->save ( 'php://output' );		
		return NULL;
	}
	
	
	
}


?>