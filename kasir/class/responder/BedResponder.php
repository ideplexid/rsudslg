<?php 

class BedResponder extends DBResponder{
	public function postToArray(){
		$data=parent::postToArray();
		$id_unit=$data['id_unit'];
		$ruangan=$data['ruangan'];
		$mulai=$data['dari'];
		$selesai=$data['sampai'];
		require_once 'smis-base/smis-include-service-consumer.php';
		$sev=new ServiceConsumer($db, "change_bed_time",NULL,$ruangan);
		$sev->setMode(ServiceConsumer::$SINGLE_MODE);
		$sev->addData("mulai", $mulai);
		$sev->addData("selesai", $selesai);
		$sev->addData("id", $id_unit);
		$sev->execute();
		$bed=$sev->getContent();
		$data['nama_tagihan']=$bed['nama_bed']." , ".$bed['hari']."x@".ArrayAdapter::format ( "only-money Rp.", $bed['harga'] );
		$data['quantity']=$bed['hari'];
		$data['tanggal']=ArrayAdapter::format ( "mini-date d M Y H:i", $bed['waktu_masuk'] ) . " - " . ArrayAdapter::format ( "mini-date d M Y H:i", $bed['waktu_keluar'] );
		$data['keterangan']="Durasi (" . $bed['hari'] . " Hari ), Per Hari " . ArrayAdapter::format ( "only-money Rp.", $d['harga'] );
		$data['total']=$bed['harga']*$bed['hari'];
		$data['nama_by']=$data['nama_by'];
		$data['nilai_by']=$data['total'];
		$data['nilai']=$bed['harga'];
		return $data;		
	}
	
}

?>