<?php 
/**
 * 
 * handling excel creation of LaporanCash
 * 
 * @author goblooge
 * @since 29 March 2016
 * @copyright LGPL v2
 * @version 1.0.0
 */


class LaporanBankResponder extends DBResponder{
	
	/**
	 * (non-PHPdoc)
	 * @see DBResponder::view()
	 */
	public function excel(){
		global $user;
		include 'smis-libs-out/php-excel/PHPExcel.php';
		$date_range=ArrayAdapter::format("date d M Y H:i", $_POST['dari'])." - ".ArrayAdapter::format("date d M Y H:i", $_POST['sampai']);
		
		//loadLibrary("smis-libs-function-export");
		$kriteria=isset($_POST['kriteria'])?$_POST['kriteria']:""; 
		$kriteria=$this->dbtable->escaped_string($kriteria);
		$number=(isset($_POST['number']) && $_POST['number']!="")?$_POST['number']:0;
		$max=isset($_POST['max'])?$_POST['max']:"10";
		$this->dbtable->setShowAll(true);
		$this->dbtable->setMaximum($max);		
		$this->dbtable->setFetchMethode(DBTable::$OBJECT_FETCH);
		$d=$this->dbtable->view($kriteria,$number);
		$start_number=$page*$max;
		$this->adapter->setNumber($start_number);
		$data=$d['data'];
		$objPHPExcel = new PHPExcel();
		$objPHPExcel->getProperties()->setCreator($user->getNameOnly());
		$objPHPExcel->getProperties()->setLastModifiedBy($user->getNameOnly());
		$objPHPExcel->getProperties()->setTitle("Laporan Cash");
		$objPHPExcel->getProperties()->setSubject("Laporan Cash Kasir Syamrabu");
		$objPHPExcel->getProperties()->setDescription("Laporan Cash Kasir Syamrabu ".$date_range);
		
		$objPHPExcel->setActiveSheetIndex(0);
		$objPHPExcel->getActiveSheet()->setTitle ( "Laporan Cash");
		$objPHPExcel->getActiveSheet()->mergeCells('A1:M1')->setCellValue("A1","Laporan Pembayaran Tunai Pasien ".$_POST['jenis'])->getStyle ( "A1")->getFont ()->setBold ( true );
		$objPHPExcel->getActiveSheet()->mergeCells('A2:M2')->setCellValue("A2",$date_range)->getStyle ( "A2")->getFont ()->setBold ( true );
		$objPHPExcel->getActiveSheet()->mergeCells('A3:M3')->setCellValue("A3","");
		$objPHPExcel->getActiveSheet()->getStyle('A1:A2')->getAlignment()->applyFromArray(
				array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,)
		);
		
		$objPHPExcel->getActiveSheet()->setCellValue('A4', 'No Kwitansi')->getStyle ( "A4")->getFont ()->setBold ( true );
		$objPHPExcel->getActiveSheet()->setCellValue('B4', 'Tanggal')->getStyle ( "B4")->getFont ()->setBold ( true );
		$objPHPExcel->getActiveSheet()->setCellValue('C4', 'Ruangan')->getStyle ( "C4")->getFont ()->setBold ( true );
		$objPHPExcel->getActiveSheet()->setCellValue('D4', 'Carabayar')->getStyle ( "D4")->getFont ()->setBold ( true );
		$objPHPExcel->getActiveSheet()->setCellValue('E4', 'Nama')->getStyle ( "E4")->getFont ()->setBold ( true );
		$objPHPExcel->getActiveSheet()->setCellValue('F4', 'No Reg')->getStyle ( "F4")->getFont ()->setBold ( true );
		$objPHPExcel->getActiveSheet()->setCellValue('G4', 'NRM')->getStyle ( "G4")->getFont ()->setBold ( true );
		$objPHPExcel->getActiveSheet()->setCellValue('H4', 'Nilai')->getStyle ( "H4")->getFont ()->setBold ( true );
		$objPHPExcel->getActiveSheet()->setCellValue('I4', 'Bank')->getStyle ( "I4")->getFont ()->setBold ( true );
		$objPHPExcel->getActiveSheet()->setCellValue('J4', 'Tambahan Bank')->getStyle ( "J4")->getFont ()->setBold ( true );
		$objPHPExcel->getActiveSheet()->setCellValue('K4', 'Tambahan Cash')->getStyle ( "K4")->getFont ()->setBold ( true );
		$objPHPExcel->getActiveSheet()->setCellValue('L4', 'Keterangan')->getStyle ( "L4")->getFont ()->setBold ( true );
		$objPHPExcel->getActiveSheet()->setCellValue('M4', 'Operator')->getStyle ( "M4")->getFont ()->setBold ( true );
		$ROWS_NUMBER=5;
		$TOTAL_NILAI=0;
		$TOTAL_NILAI_BANK=0;
		$TOTAL_NILAI_CASH=0;
		foreach($data as $one){
			$objPHPExcel->getActiveSheet()->setCellValue('A'.$ROWS_NUMBER, ArrayAdapter::format("only-digit8", $one->id));
			$objPHPExcel->getActiveSheet()->setCellValue('B'.$ROWS_NUMBER, ArrayAdapter::format("date d M Y H:i:s", $one->waktu));
			$objPHPExcel->getActiveSheet()->setCellValueExplicit('C'.$ROWS_NUMBER, ArrayAdapter::format("unslug", $one->ruangan),PHPExcel_Cell_DataType::TYPE_STRING);
			$objPHPExcel->getActiveSheet()->setCellValueExplicit('D'.$ROWS_NUMBER, ArrayAdapter::format("unslug", $one->carabayar),PHPExcel_Cell_DataType::TYPE_STRING);
			$objPHPExcel->getActiveSheet()->setCellValue('E'.$ROWS_NUMBER, $one->nama_pasien);
			$objPHPExcel->getActiveSheet()->setCellValueExplicit('F'.$ROWS_NUMBER, ArrayAdapter::format("only-digit8", $one->noreg_pasien),PHPExcel_Cell_DataType::TYPE_STRING);
			$objPHPExcel->getActiveSheet()->setCellValueExplicit('G'.$ROWS_NUMBER, ArrayAdapter::format("only-digit8", $one->nrm_pasien),PHPExcel_Cell_DataType::TYPE_STRING);
			$objPHPExcel->getActiveSheet()->setCellValue('H'.$ROWS_NUMBER, $one->nilai);
			$bank=explode("-", $one->bank);
			$objPHPExcel->getActiveSheet()->setCellValue('I'.$ROWS_NUMBER, $bank[0]);
			if($one->di_bank=="1"){
				$objPHPExcel->getActiveSheet()->setCellValue('J'.$ROWS_NUMBER, $one->nilai_tambahan);
				$objPHPExcel->getActiveSheet()->setCellValue('K'.$ROWS_NUMBER, 0);
				$TOTAL_NILAI_BANK+=$one->nilai_tambahan;
			}else{
				$objPHPExcel->getActiveSheet()->setCellValue('J'.$ROWS_NUMBER, 0);
				$objPHPExcel->getActiveSheet()->setCellValue('K'.$ROWS_NUMBER, $one->nilai_tambahan);
				$TOTAL_NILAI_CASH+=$one->nilai_tambahan;
			}
			$objPHPExcel->getActiveSheet()->setCellValue('L'.$ROWS_NUMBER, $one->keterangan);
			$objPHPExcel->getActiveSheet()->setCellValue('M'.$ROWS_NUMBER, $one->operator);
			$ROWS_NUMBER++;
			$TOTAL_NILAI+=$one->nilai;
		}
		$objPHPExcel->getActiveSheet()->setCellValue('A'.$ROWS_NUMBER, "TOTAL")->getStyle ( "A" . $ROWS_NUMBER)->getFont ()->setBold ( true );
		$objPHPExcel->getActiveSheet()->setCellValue('B'.$ROWS_NUMBER, "");
		$objPHPExcel->getActiveSheet()->setCellValue('C'.$ROWS_NUMBER, "");
		$objPHPExcel->getActiveSheet()->setCellValue('D'.$ROWS_NUMBER, "");
		$objPHPExcel->getActiveSheet()->setCellValue('E'.$ROWS_NUMBER, "");
		$objPHPExcel->getActiveSheet()->setCellValue('F'.$ROWS_NUMBER, "");
		$objPHPExcel->getActiveSheet()->setCellValue('G'.$ROWS_NUMBER, "");
		$objPHPExcel->getActiveSheet()->setCellValue('H'.$ROWS_NUMBER, $TOTAL_NILAI)->getStyle ( "H" . $ROWS_NUMBER)->getFont ()->setBold ( true );
		$objPHPExcel->getActiveSheet()->setCellValue('I'.$ROWS_NUMBER, "");		
		$objPHPExcel->getActiveSheet()->setCellValue('J'.$ROWS_NUMBER, $TOTAL_NILAI_BANK)->getStyle ( "H" . $ROWS_NUMBER)->getFont ()->setBold ( true );
		$objPHPExcel->getActiveSheet()->setCellValue('K'.$ROWS_NUMBER, $TOTAL_NILAI_CASH)->getStyle ( "H" . $ROWS_NUMBER)->getFont ()->setBold ( true );
		$objPHPExcel->getActiveSheet()->setCellValue('L'.$ROWS_NUMBER, "");
		$objPHPExcel->getActiveSheet()->setCellValue('M'.$ROWS_NUMBER, "");
		
		$objPHPExcel->getActiveSheet()->getStyle("H5:H".$ROWS_NUMBER)->getNumberFormat()->setFormatCode('#,##0,00');
		$objPHPExcel->getActiveSheet()->getStyle("J5:J".$ROWS_NUMBER)->getNumberFormat()->setFormatCode('#,##0,00');
		$objPHPExcel->getActiveSheet()->getStyle("K5:K".$ROWS_NUMBER)->getNumberFormat()->setFormatCode('#,##0,00');
		$objPHPExcel->getActiveSheet()->getStyle('A4:M'.$ROWS_NUMBER)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
		
		$objPHPExcel->getActiveSheet()->getColumnDimension("A")->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension("B")->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension("C")->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension("D")->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension("E")->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension("F")->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension("G")->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension("H")->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension("I")->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension("J")->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension("K")->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension("L")->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension("M")->setAutoSize(true);
		$filename=$this->dbtable->getName();
		$filename.="-".$kriteria;
		$filename.=" [ ".($start_number)." - ".($start_number+count($uidata))." ]";
		$filename.=" ( ".$date_range.") ";
		$filename.=" - ".$user->getNameOnly();
		
		header ( 'Content-Type: application/vnd.ms-excel' );
		header ( 'Content-Disposition: attachment;filename="'.$filename.'.xls"' );
		header ( 'Cache-Control: max-age=0' );
		$writer = PHPExcel_IOFactory::createWriter ( $objPHPExcel, 'Excel5' );
		$writer->save ( 'php://output' );		
		return NULL;
	}
	
	
	
}


?>