<?php 

class TagihanBackupResponder extends DBResponder{
    public function save(){
        $success=parent::save();
        $this->synchronizeToAccounting($this->dbtable->get_db(),$success['id'],"");
        return $success;
    }
    
    public function delete(){
        $result=parent::delete();
        $this->synchronizeToAccounting($this->dbtable->get_db(),$_POST['id'],"del");
        return $result;
    }
    
    public function synchronizeToAccounting($db,$id,$is_del=""){
        $x=$this->dbtable->selectEventDel($id);
        $data=array();
        $data['jenis_akun'] = "transaction";
        $data['jenis_data'] = "penjualan";//"tagihan_tambahan_pasien";
        $data['id_data']    = $id;
        $data['entity']     = "kasir";
        $data['service']    = "get_detail_accounting_tagihan_backup";
        $data['data']       = $id;
        $data['code']       = "kasir-backup-p-".$id;
        $data['operation']  = $is_del;
        $data['tanggal']    = $x->tanggal;
        $data['uraian']     = $x->keterangan." Atas Pasien ".$x->nama_pasien." No. Reg ".$x->noreg_pasien;
        $data['nilai']      = $x->nilai;
        
        $serv=new ServiceConsumer($db,"push_notify_accounting",$data,"accounting");
        $serv->execute();
    }
}

?>