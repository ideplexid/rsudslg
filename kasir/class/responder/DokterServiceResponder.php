<?php
class DokterServiceResponder extends ServiceResponder {
	public function getData() {
		$data = $_POST;
		if (isset($_POST['kriteria']) && $_POST['kriteria'] != "") {
			$kriteria = $_POST['kriteria'];
			$data['kriteria'] = json_encode(array(
				"multiple_kriteria"	=> 	array(
					"nip" => $kriteria,
					"nama" => $kriteria,
					"jabatan" => "dokter"
				)
			));
		} else {
			$data['kriteria'] = "dokter";
		}
		return $data;
	}
}
