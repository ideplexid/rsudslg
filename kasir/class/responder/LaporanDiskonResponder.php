<?php 
class LaporanDiskonResponder extends ServiceResponder{
    public function excel() {
        ob_clean();
        show_error();
        $this->addData("command","list");
        $this->addData("max","1000000000");
        $this->getService()->setData($this->getData());       
		$this->getService()->execute();
		$content = $this->getService()->getContent();
        $fix = $content['data'];
        
        require_once ("smis-libs-out/php-excel/PHPExcel.php");
        $file = new PHPExcel ();
        $sheet = $file->getActiveSheet ();

        $i = 1;
        
        $objDrawing = new PHPExcel_Worksheet_Drawing();
        $objDrawing->setName('logo_img');
        $objDrawing->setDescription('logo_img');
        $objDrawing->setPath(getLogoNonInterlaced());
        $objDrawing->setCoordinates('A'.$i);
        $objDrawing->setOffsetX(20); 
        $objDrawing->setOffsetY(5);
        $objDrawing->setWidth(100); 
        $objDrawing->setHeight(100);
        $objDrawing->setWorksheet($sheet);
        $i++;
        
        $sheet->setCellValue("C".$i, getSettings($db,"smis_autonomous_title"));
        $sheet->getStyle("C".$i)->getFont()->setBold(true)->setSize(14);
        $i++;
        $sheet->setCellValue("C".$i, getSettings($db,"smis_autonomous_address"));
        $sheet->getStyle("C".$i)->getFont()->setBold(true)->setSize(14);
        $i++;
        $sheet->setCellValue("C".$i, getSettings($db,"smis_autonomous_contact"));
        $sheet->getStyle("C".$i)->getFont()->setBold(true)->setSize(14);
        $i += 3;
        $sheet->mergeCells("A".$i.":P".$i)->setCellValue("A".$i,"LAPORAN DISKON");
        $sheet->getStyle("A".$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle("A".$i)->getFont()->setBold(true)->setSize(14);
        $i++;
        $sheet->mergeCells("A".$i.":P".$i)->setCellValue("A".$i, strtoupper(ArrayAdapter::dateFormat("date d M Y", $_POST['dari']))." - ".strtoupper(ArrayAdapter::dateFormat("date d M Y", $_POST['sampai'])));
        $sheet->getStyle("A".$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle("A".$i)->getFont()->setBold(true);
        $i += 3;
        
        $border_start = $i;
        $sheet->setCellValue("A".$i, "No.");
        $sheet->setCellValue("B".$i, "Noreg Pasien");
        $sheet->setCellValue("C".$i, "NRM Pasien");
        $sheet->setCellValue("D".$i, "Nama Pasien");
        $sheet->setCellValue("E".$i, "Tanggal");
        $sheet->setCellValue("F".$i, "Jenis");
        $sheet->setCellValue("G".$i, "Status");
        $sheet->setCellValue("H".$i, "Layanan");
        $sheet->setCellValue("I".$i, "Kamar");
        $sheet->setCellValue("J".$i, "Cara Bayar");
        $sheet->setCellValue("K".$i, "Tagihan");
        $sheet->setCellValue("L".$i, "Asuransi");
        $sheet->setCellValue("M".$i, "Bank");
        $sheet->setCellValue("N".$i, "Cash");
        $sheet->setCellValue("O".$i, "Diskon");
        $sheet->setCellValue("P".$i, "Keterangan Diskon");
        
        $sheet->getStyle("A".$i.":P".$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle("A".$i.":P".$i)->getFont()->setBold(true);
        $i++;
        $no = 0;

        foreach($fix as $a) {
            $no++;
            $sheet->setCellValue("A".$i, " ".$no.".");
            $sheet->setCellValue("B".$i, $a['id']);
            $sheet->setCellValue("C".$i, $a['nrm']);
            $sheet->setCellValue("D".$i, $a['nama_pasien']);
            $sheet->setCellValue("E".$i, $a['tanggal']);
            $sheet->setCellValue("F".$i, $a['uri']=="1"?"URI":"URJ");
            $sheet->setCellValue("G".$i, $a['selesai']=="0"?"Berlangsung":"Selesai");
            $sheet->setCellValue("H".$i, ArrayAdapter::format("unslug",$a['jenislayanan']));
            $sheet->setCellValue("I".$i, ArrayAdapter::format("unslug",$a['kamar_inap']));
            $sheet->setCellValue("J".$i, ArrayAdapter::format("unslug",$a['carabayar']));
            $sheet->setCellValue("K".$i, $a['total_tagihan']);
            $sheet->setCellValue("L".$i, $a['tb_asuransi']);
            $sheet->setCellValue("M".$i, $a['tb_bank']);
            $sheet->setCellValue("N".$i, $a['tb_cash']);
            $sheet->setCellValue("O".$i, $a['tb_diskon']);
            $sheet->setCellValue("P".$i, $a['diskon_keterangan']);

            $sheet->getStyle("A".$i.":J".$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $sheet->getStyle("K".$i.":O".$i)->getNumberFormat()->setFormatCode('#,##0.00');
            $i++;
        }

        $sheet->setCellValue("J".$i, "TOTAL");
        $sheet->setCellValue("K".$i, "=sum(K12:K".($i-1).")");
        $sheet->setCellValue("L".$i, "=sum(L12:L".($i-1).")");
        $sheet->setCellValue("M".$i, "=sum(M12:M".($i-1).")");
        $sheet->setCellValue("N".$i, "=sum(N12:N".($i-1).")");
        $sheet->setCellValue("O".$i, "=sum(O12:O".($i-1).")");
        $sheet->getStyle("K".$i.":O".$i)->getNumberFormat()->setFormatCode('#,##0.00');
        $i++;

        $border_end = $i - 1;
        //$end = $i-2;
        //$jumlah_data = ($end - $start) + 1;
        $sheet->getStyle("A".$border_end.":J".$border_end)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle("A".$border_end.":J".$border_end)->getFont()->setBold(true);
        
        $thin = array ();
        $thin['borders']=array();
        $thin['borders']['allborders']=array();
        $thin['borders']['allborders']['style']=PHPExcel_Style_Border::BORDER_THIN ;
        $sheet  ->getStyle ( "A".$border_start.":P".$border_end )->applyFromArray ($thin);
        
        $filename = "Laporan Diskon ".strtoupper(ArrayAdapter::dateFormat("date d M Y", $_POST['dari']))." - ".strtoupper(ArrayAdapter::dateFormat("date d M Y", $_POST['sampai'])).".xls";
        
        header ( 'Content-Type: application/vnd.ms-excel' );
        header ( 'Content-Disposition: attachment;filename="'.$filename.'"' );
        header ( 'Cache-Control: max-age=0' );
        $writer = PHPExcel_IOFactory::createWriter ( $file, 'Excel5' );
        ob_clean();
        $writer ->save ( 'php://output' );
		return;
    }
}