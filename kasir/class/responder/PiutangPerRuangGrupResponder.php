<?php 

class PiutangPerRuangGrupResponder extends DBResponder{
    private $header;
    
    public function setHeader($header){
        $this->header=$header;
        return $this;
    }
    
    public function excel(){
        global $user;
        require_once 'smis-libs-out/php-excel/PHPExcel.php';
        $date_range=ArrayAdapter::format("date d M Y H:i", $_POST['dari'])." - ".ArrayAdapter::format("date d M Y H:i", $_POST['sampai']);
		
        
		$this->dbtable->setShowAll(true);
		$this->dbtable->setMaximum($max);		
		$this->dbtable->setFetchMethode(DBTable::$ARRAY_FETCH);
		$d=$this->dbtable->view($kriteria,$number);
		$total=count($this->header)+5;
        $col=PHPExcel_Cell::stringFromColumnIndex($total);
        
        $data=$d['data'];
		
        $file = new PHPExcel();
		$file->getProperties()->setCreator($user->getNameOnly());
		$file->getProperties()->setLastModifiedBy($user->getNameOnly());
		$file->getProperties()->setTitle("Laporan Cash");
		$file->getProperties()->setSubject("Laporan Cash Kasir");
		$file->getProperties()->setDescription("Laporan Cash Kasir ".$date_range);
		
        $sheet=$file->getActiveSheet();
		$sheet->setTitle ( "Laporan Per Group");
		$sheet->mergeCells('A1:'.$col.'1')->setCellValue("A1","Laporan Pembayaran Tunai Pasien")->getStyle ( "A1")->getFont ()->setBold ( true );
		$sheet->mergeCells('A2:'.$col.'2')->setCellValue("A2",$date_range)->getStyle ( "A2")->getFont ()->setBold ( true );
		$sheet->mergeCells('A3:'.$col.'3')->setCellValue("A3","");
		$sheet->getStyle('A1:A2')->getAlignment()->applyFromArray(
				array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,)
		);
		
		$sheet->setCellValue('A4', 'No.')->getStyle ( "A4")->getFont ()->setBold ( true );
		$sheet->setCellValue('B4', 'Nama')->getStyle ( "B4")->getFont ()->setBold ( true );
		$sheet->setCellValue('C4', 'NRM')->getStyle ( "C4")->getFont ()->setBold ( true );
		$sheet->setCellValue('D4', 'No Reg')->getStyle ( "D4")->getFont ()->setBold ( true );
		$sheet->setCellValue('E4', 'Tanggal')->getStyle ( "E4")->getFont ()->setBold ( true );
		$pos=6;
        foreach($this->header as $h){
            $colx=PHPExcel_Cell::stringFromColumnIndex($pos-1);
            $pos++;
            $sheet->setCellValue($colx.'4', ArrayAdapter::format("unslug",$h->ruangan))->getStyle ( $colx.'4')->getFont ()->setBold ( true );
        }
        $sheet->setCellValue($col.'4', 'Total')->getStyle ( $col.'4')->getFont ()->setBold ( true );
		
        $line=5;
        $number=1;
        $colx="";
        foreach($data as $x){
            $sheet->setCellValue('A'.$line, $number.".")->getStyle ( "A4")->getFont ()->setBold ( true );
            $sheet->setCellValue('B'.$line, $x['nama_pasien'])->getStyle ( "B4")->getFont ()->setBold ( true );
            $sheet->setCellValue('C'.$line, "'".$x['nrm_pasien'])->getStyle ( "C4")->getFont ()->setBold ( true );
            $sheet->setCellValue('D'.$line, "'".$x['noreg_pasien'])->getStyle ( "D4")->getFont ()->setBold ( true );
            $sheet->setCellValue('E'.$line, ArrayAdapter::format("date d M Y H:i",$x['akunting_posted']))->getStyle ( "E4")->getFont ()->setBold ( true );
            $pos=6;
            foreach($this->header as $h){
                $colx=PHPExcel_Cell::stringFromColumnIndex($pos-1);
                $pos++;
                $sheet->setCellValue($colx.$line, $x[$h->ruangan]);
            }
            $sheet->setCellValue($col.$line, '=SUM(F'.$line.':'.$colx.$line.')');
            $line++;
            $number++;
        }
        $sheet->setCellValue("E".$line, 'Total')->getStyle ( "E".$line.":".$col.$line)->getFont ()->setBold ( true );
		$pos=6;
        foreach($this->header as $h){
            $colx=PHPExcel_Cell::stringFromColumnIndex($pos-1);
            $pos++;
            $sheet->setCellValue($colx.$line, "=SUM(".$colx."5:".$colx.($line-1).")");
        }    
        
        $sheet->getStyle("F5:".$col.($line) )->getNumberFormat()->setFormatCode('#,##0.00');
        
        
        header ( 'Content-Type: application/vnd.ms-excel' );
		header ( 'Content-Disposition: attachment;filename="Laporan Pendapatan per Grup.xls"' );
		header ( 'Cache-Control: max-age=0' );
		$writer = PHPExcel_IOFactory::createWriter ( $file, 'Excel5' );
		$writer->save ( 'php://output' );
        
    }
    
}

?>