<?php

class TagihanNonPasienTable extends Table{
	
	public function getPrintedElement($p, $f){
		global $db;

		$query = "SELECT * FROM smis_ksr_bayar WHERE id_np = '$p->id' AND prop='' ORDER BY id DESC ";
		$pxx = $db->get_row($query);


		if(strpos($pxx->metode,"_")!==false){
            $metode = explode("_",$pxx->metode);
            $mtod = ucwords($metode[0]);    
        }else{
            $mtod = ucwords($pxx->metode);   
		}
		// /$mtod = json_encode($p);
		$keterangan=$p->keterangan."; Melalui $mtod \n";
		$dbtable=new DBTable($db, "smis_ksr_npd");
		$dbtable->setShowAll(true);
		$dbtable->addCustomKriteria("id_np", "='".$p->id."'");
		$list=$dbtable->view("", "0");
		$data=$list['data'];
		foreach($data as $x){
			$keterangan.=$x->nama."( Rp. ".ArrayAdapter::format("non-leading-money", $x->harga_satuan)." x ".$x->jumlah." )"."; \n";
		}
		
		$tp=new TablePrint("tagihan_non_pasien");
		$tp->setDefaultBootrapClass(false);
		$tp->setResponsive(false);
		$tp->setMaxWidth(false);
		$tp->setTableClass("");
		
		$logo=getLogo();
		$logo="<img src='".$logo."'/>";
		$auth_name=getSettings($db, "smis_autonomous_title", "SMIS");
		$auth_address=getSettings($db, "smis_autonomous_address", "...");
		
		$tp->addSpace(13, 1,null,null,"plain");
		$tp->addColumn("KWITANSI", 2,1,null,null,"bold lbottom");
		$tp->addSpace(1, 1,null,null,"plain");
		$tp->commit("body");
		
		$tp->addColumn($logo, 5, 4,null,null,"logo plain");
		$tp->addColumn($auth_name, 6, 2,null,null,"plain f20 bold center");
		$tp->addSpace(2, 1,null,null,"plain");
		$tp->addColumn("RECEIPT", 2,1,null,null,"plain italic");
		$tp->addSpace(1, 1,null,null,"plain");
		$tp->commit("body");
		
		$tp->addSpace(1, 1,null,null,"plain");
		$tp->addSpace(5, 1,null,null,"plain");
		$tp->commit("body");
		
		$tp->addColumn($auth_address, 6, 2,null,null,"plain center f15");
		$tp->addSpace(2, 1,null,null,"plain");
		$tp->addColumn("No. ", 1,1,null,null,"lbottom");
		$tp->addColumn(": ".ArrayAdapter::format("only-digit5", $p->id), 1,1,null,null,"plain");
		$tp->addSpace(1, 1,null,null,"plain");
		$tp->commit("body");
		
		$tp->addSpace(2, 1,null,null,"plain");
		$tp->addColumn("Number ", 1,1,null,null,"plain");
		$tp->addSpace(2, 1,null,null,"plain");
		$tp->commit("body");
		
		$tp->addSpace(16, 1,null,null,"plain");
		$tp->commit("body");
		
		$tp->addColumn("Sudah Terima Dari", 3,1,null,null,"lbottom ltop lleft");
		$tp->addColumn(":", 1,1,null,null,"ltop");
		$tp->addColumn($p->nama, 12,3,null,null,"ltop lright bold vtop");
		$tp->commit("body");
		$tp->addColumn("Received From", 3,1,null,null,"lleft italic");
		$tp->addSpace(1, 1,null,null,"plain");
		$tp->commit("body");
		$tp->addColumn("", 4, 1,null,null,"lleft");		
		$tp->commit("body");
		loadLibrary("smis-libs-function-math");
		
		$tp->addColumn("Banyaknya Uang", 3,1,null,null,"lleft lbottom");
		$tp->addColumn(":", 1,1,null,null,"plain");
		$tp->addColumn(strtoupper(numbertell($p->nilai))." RUPIAH", 12,3,null,null,"lright vtop");
		$tp->commit("body");
		$tp->addColumn("Amount Received", 3,1,null,null,"lleft italic");
		$tp->addSpace(1, 1,null,null,"plain");
		$tp->commit("body");
		$tp->addColumn("", 4, 1,null,null,"lleft");		
		$tp->commit("body");
		
		$tp->addColumn("Untuk Pembayaran", 3,1,null,null,"lleft lbottom");
		$tp->addColumn(":", 1,1,null,null,"plain");
		$tp->addColumn($keterangan, 12,3,null,null,"lright  lbottom vtop");
		$tp->commit("body");
		$tp->addColumn("In Payment Of", 3,1,null,null,"lleft italic");
		$tp->addSpace(1, 1,null,null,"plain");
		$tp->commit("body");
		//$tp->addSpace(4, 1,null,null,"lleft lbottom");
		$tp->addColumn("", 4, 1,null,null,"lleft lbottom");
		$tp->commit("body");
		
		$tp->addSpace(16, 1,null,null,"plain");
		$tp->commit("body");
		
		$tp->addColumn("Rp. ".ArrayAdapter::format("non-leading-money", $p->nilai), 3,1,null,null,"lbottomb ltopb f20 grey");
		$tp->addSpace(10, 1,null,null,"plain");
		$tp->addColumn(getSettings($db, "smis-rs-footer", "")." , ".ArrayAdapter::format("date d M Y", $p->tanggal), 3, 1,null,null,"plain center");
		$tp->commit("body");
		
		$tp->addSpace(13, 1,null,null,"plain");
		$tp->addColumn("Kasir,", 3, 1,null,null,"plain center");
		$tp->commit("body",null,null,"plain");
		
		$tp->addSpace(13, 1,null,null,"plain");
		$tp->addColumn($p->operator, 3, 1,null,null,"plain center");
		$tp->commit("body");
		
		
		
		return $tp->getHtml();
		
	}
	
}

?>