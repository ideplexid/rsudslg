<?php

class CashTable extends Table{

    public function getPrintedElement($p,$f){
        global $db;
        global $user;
        require_once("kasir/function/kwitansi_bodong.php");
        require_once "kasir/function/increment_bayar.php";       
        increment_bayar($db,$p->id);
        if(strpos($p['metode'],"_")!==false){
            $metode = explode("_",$p['metode']);
            $mtod = ucwords($metode[0]);    
        }else{
            $mtod = ucwords($p['metode']);
        }
        //$mtod = json_encode($p);

        $user_ke    = $user->getNameOnly();
        $tanggal_ke = date("Y-m-d H:i:s");
        $html       = kwitansi_bodong($db,$p['no_kwitansi']!=""?$p['no_kwitansi']:$p['id'], $p['nama_pasien'], $p['nilai'], $p['keterangan']."; Melalui $mtod ",$p['operator'],$p['waktu'],"Kasir",$p['cetakan_ke']+1,$user_ke,$tanggal_ke);
        return $html; 
    }

}

?>