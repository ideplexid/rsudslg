<?php

class AsuransiTable extends Table{
    public function getContentButton($id){
        
        $bgroup=new ButtonGroup('noprint');
		if($id=="" || $id=="0") {
            return $bgroup;
        }
        if($this->edit_button_enable){
            $edit	 = new Button($this->name."_edit", "","Edit");
            $edit	 ->setAction($this->action.".edit('".$id."')")
                        ->setClass("btn-warning")
                        ->setAtribute("data-content='Edit' data-toggle='popover'")
                        ->setIcon("fa fa-pencil")
                        ->setIsButton(Button::$ICONIC_TEXT);
            $bgroup  ->addElement($edit);
        }
        $bgroup->setMax($this->content_button_max, $this->content_button_default);
        foreach($this->content_button as  $slug=>$btn){
            
            if($slug=="cair" && $this->current_data['Cair']=="Belum" ){
                $btn->setAction($this->action.".".$slug."('".$id."')");
                $btn->setId($this->name."_".$slug);
                $btn->setAtribute(" data-id-cair = '".$id."' ");
                $btn->setClass("btn btn-success cair_button");
                $bgroup->addElement($btn);
               
            }else if($slug=="push_to_lunas" && $this->current_data['Cair']=="Ya"){
                $btn->setAction($this->action.".".$slug."('".$id."')");
                $btn->setId($this->name."_".$slug);
                $btn->setAtribute(" data-id-lunas = '".$id."' ");
                $btn->setClass("btn btn-info push_to_lunas_button");
                $bgroup->addElement($btn);
            }else if($slug!="push_to_lunas" && $slug!="cair"){
                $btn->setAction($this->action.".".$slug."('".$id."')");
                $btn->setId($this->name."_".$slug);
                $bgroup->addElement($btn);
            }
        }        
        return $bgroup;
        
    }
}

?>