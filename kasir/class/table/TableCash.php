<?php 

class TableCash extends Table{
	
	public function getPrintedElement($p, $f){
		global $db;
        global $user;
		require_once "kasir/function/kwitansi_bodong.php";       
        require_once "kasir/function/increment_bayar.php"; 
		
		if(strpos($p['metode'],"_")!==false){
            $metode = explode("_",$p['metode']);
            $mtod = ucwords($metode[0]);    
        }else{
            $mtod = ucwords($p['metode']);   
		}
		//$mtod = json_encode($p);

        $user_ke    = $user->getNameOnly();
        $tanggal_ke = date("Y-m-d H:i:s");
        $table 		=  kwitansi_bodong($db,$p['no_kwitansi']!=""?$p['no_kwitansi']:$p['id'], $p['nama_pasien'], $p['nilai'], $p['keterangan']."; Melalui $mtod",$p['operator'],$p['waktu'],"Kasir",$p['cetakan_ke']+1,$user_ke,$tanggal_ke);
		increment_bayar($db,$p['id']);
        
        $print		= "";
		global $db;
		$times		= getSettings($db, "cashier-print-out-times", "1");
		if($times*1<1) {
			$times	= 1;
		}
		for($t=1;$t<=($times*1);$t++){
			$print .= $table."<div class='clear line'></div>";
		}		
		$return					= array();
		$return['html_print']	= $print;
		$return['html']			= $table;
		$return['nama_pasien']	= $pure['nama_pasien'];
		$return['noreg_pasien']	= $pure['noreg_pasien'];
		$return['nrm_pasien']	= $pure['nrm_pasien'];
		$return['noref']		= $nomor;
		$return['jenis']		= "Pembayaran Kasir - Cash";
		if(getSettings($db,"cashier-webprint-cash","0")=="1"){
            $return['webprint'] = array(
								'template' 	=> json_decode(file_get_contents("kasir/webprint/kwitansi.json"), true),
								'data' 		=> array(
											'nomor' 		=> $nomor,
											'nrm_pasien' 	=> ArrayAdapter::format("only-digit6", $pure['nrm_pasien']),
											'nama_pasien' 	=> ArrayAdapter::format("clean-unslug", $pure['nama_pasien']),
											'layanan' 		=> $layanan,
											'jenis_pasien' 	=> ArrayAdapter::format("clean-unslug", $_POST['jenis_pasien']),
											'bayar' 		=> ArrayAdapter::format ( "only-money Rp.", $pure['nilai'] ),
											'terbilang' 	=> numbertell($pure['nilai']) . " Rupiah",
											'tanggal' 		=> ArrayAdapter::format("date d-m-Y", date("y-m-d")),
											'kasir' 		=> ArrayAdapter::format("clean-unslug", $user->getNameOnly())
								)
            );
        }
		return $return;
	}	
}

?>