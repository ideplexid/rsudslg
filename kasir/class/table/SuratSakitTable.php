<?php 

class SuratSakitTable extends Table{
    
    public function getPrintedElement($p,$f){
        $name    = getSettings($db,"smis_autonomous_title","");
        $address = getSettings($db,"smis_autonomous_address","");
        $phone   = getSettings($db,"smis_autonomous_contact","");
        $town    = getSettings($db,"smis_autonomous_town","");
        $logo    = "<img src='".getLogo()."' />";
        $table   = new TablePrint("table_surat_sakit");
        $table   ->setDefaultBootrapClass(false);
        $table   ->setMaxWidth(false);
        $table   ->addColumn($logo,1,4)
                 ->addColumn("Klinik",4,1,"body",null,"ssb ssg ssl");
        $table   ->addColumn($name,4,1,"body",null,"ssb ssg ssl");
        $table   ->addColumn($address,4,1,"body",null,"ssl");
        $table   ->addColumn($phone.".",4,1,"body",null,"ssl");

        $table   ->addColumn("No. ".$p->nomor,5,1,"body",null,"ssb ssm ssc");
        $table   ->addColumn("SURAT KETERANGAN",5,1,"body",null,"ssb ssg ssc ssu");
        $table   ->addColumn("MEDICAL CERTIFICATE",5,1,"body",null,"ssb ssg ssc");
        $table   ->addSpace(5,1,"body");
        $table   ->addColumn("Menerangakan dengan sesungguhnya bahwa",5,1,"body");
        $table   ->addColumn("This is to certify that ",5,1,"body",null,"ssi");
        $table   ->addSpace(5,1,"body");
        $table   ->addColumn("Nama / <i>Name</i>",3,1)
                 ->addColumn(":",1,1)
                 ->addColumn($p->nama_pasien,1,1,"body");
        $table   ->addColumn("Umur / <i>Age</i>",3,1)
                 ->addColumn(":",1,1)
                 ->addColumn($p->umur,1,1,"body");
        $table   ->addColumn("Jenis Kelamin / <i>Gender</i>",3,1)
                 ->addColumn(":",1,1)
                 ->addColumn($p->jk=="0"?"L":"P",1,1,"body");
        $table   ->addColumn("Perusahaan / <i>Company</i>",3,1)
                 ->addColumn(":",1,1)
                 ->addColumn($p->perusahaan,1,1,"body");
        $table   ->addColumn("Nomor Pesereta / <i>Member ID</i>",3,1)
                 ->addColumn(":",1,1)
                 ->addColumn($p->nrm_pasien,1,1,"body");
        $tanggal = ArrayAdapter::dateFormat("date d M Y",$p->tanggal);
        $table   ->addColumn("Pada tanggal ".$tanggal." dilakukan pemeriksaan kesehatan dari orang",5,1,"body");
        $table   ->addColumn("On the ".$tanggal." Has done medical examination to the above ",5,1,"body",null,"ssi");
        $table   ->addColumn("tersebut diatas dengan hasilnya",3,1)
                 ->addColumn(":",2,1,"body");
        $table   ->addColumn("mentioned person with result as follows",3,1,null,"ssi")
                 ->addColumn(":",2,1,"body");
        if($p->kondisi=="0"){
            $table   ->addColumn("Baik",5,1,"body",null,"sspl");
            $table   ->addColumn("Good",5,1,"body",null,"ssi sspl");
        }else{
            loadLibrary("smis-libs-function-math");
            $table   ->addColumn("Perlu istirahat selama ".$p->selama." Hari ( ".numbertell($p->selama)." Hari)",5,1,"body",null,"sspl");
            $table   ->addColumn("Need Medical Leave",5,1,"body",null,"ssi sspl");
            $dari    = ArrayAdapter::dateFormat("date d M Y",$p->dari);
            $sampai  = ArrayAdapter::dateFormat("date d M Y",$p->sampai);
            $table   ->addColumn("Hari sejak tanggal ".$dari." s/d ".$sampai,5,1,"body",null,"sspl");
            $table   ->addColumn("Day(s) since",5,1,"body",null,"ssi sspl");                
        }        
                 
        $table   ->addColumn("Demikianlah untuk diketahui adanya.",3,1)
                 ->addColumn($town.", ".$tanggal,2,1,"body",null,"ssc");
        $table   ->addColumn("Please be advised accordingly",3,1,null,"ssi")
                 ->addColumn("Dokter / <i>Doctor</i>",2,1,"body",null,"ssc");
        
        $table   ->addColumn("<div class='ctrl_1'>&nbsp;</div>",1,1);
        $table   ->addColumn("<div class='ctrl_2'>&nbsp;</div>",1,1);
        $table   ->addColumn("<div class='ctrl_3'>&nbsp;</div>",1,1);
        $table   ->addColumn("<div class='ctrl_4'>&nbsp;</div>",1,1);
        $table   ->addColumn("<div class='ctrl_5'>&nbsp;</div>",1,1,"body");
        $table   ->addSpace(5,1,"body");
        $table   ->addSpace(3,1)
                 ->addColumn("( ".$p->nama_dokter." )",2,1,"body",null," ssc");
        $table   ->addSpace(3,1)
                 ->addColumn(" Nama / <i>Name</i> ",2,1,"body",null," ssc");
        
        return   $table->getHtml();
    }
    
}

?>