<?php

class TableEditBayangan extends Table{
	
	public function getBodyContent(){
		$content="";
	
		foreach($this->body_before as $h){
			$content.=$h;
		}
	
		if($this->content!=NULL){
			foreach($this->content as $d){
				$content.="<tr>";
				foreach($this->header as $h){
					
					$class='';
					if($h=="Nilai" && $d['Nilai Asli']!=$d['Nilai'] || $h=='Nama' && $d['Nama Asli']!=$d['Nama'] || $h=="Jaspel" && $d['Jaspel Asli']!=$d['Jaspel'] || $h=="Tampil" && $d['Tampil']==0 || $h=="Ruang" && $d['Ruang']!=$d['Ruangan']){
						$class="red_text";	
						if($h=="Jaspel" && $d['Jaspel Asli']!=$d['Jaspel'] && $d['Jaspel']==""){
							$d['Jaspel']="<i class='fa fa-remove'></i>";
						}
					}
					
					
					
					
					$content.="<td class='".$class."'>".(isset($d[$h])?$d[$h]:"")."</td>";
				}
				if($this->is_action){
					$this->current_data=$d;
					$content.="<td class='noprint'>".$this->getContentButton($d['id'])->getHtml()."</td>";
				}
				$content.="</tr>";
			}
		}
	
		foreach($this->body_after as $h){
			$content.=$h;
		}
	
	
		return $content;
	}
	
	
}

?>