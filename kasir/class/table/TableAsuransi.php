<?php 

class TableAsuransi extends Table{
	
	public function getPrintedElement($pure, $formatted){
		$table=new TablePrint("cash_print");
		global $db;
		global $user;
		
		loadLibrary("smis-libs-function-math");
		$rs=getSettings($db, "smis_autonomous_title", "");
		$penerima=$user->getNameOnly();
		$address=getSettings($db, "smis_autonomous_address", "");
		$table->addColumn("<h4>".$rs."</h4>", 4, 1,"title");
		$table->addColumn("<h4><u>".$address."</u></h4>", 4, 1,"title");
		$table->setMaxWidth(false);
		
// 		$id=ArrayAdapter::format("only-digit9", $pure['id']);
// 		$nomor= "No. : ".strtoupper ( $id."-" . date ( 'dmy-his' ) . "-" . substr ( md5 ( $this->nama_pasien . $this->noreg_pasien . date ( 'dmyhis' ).$id ), 1, 4 ) );
		
		$layanan = $_POST['layanan'];
		$nomor = ($layanan === 'Rawat Inap' ? 'RI.01-' : 'RJ.01') . ArrayAdapter::format("only-digit9", $pure['id']);
		
		$klaim=$pure['terklaim']=="1"?"Telah Diterima Pada ".ArrayAdapter::format("date d M Y H:i", $pure['waktu']):"Belum Diterima Pada ".ArrayAdapter::format("date d M Y H:i", $pure['waktu']);
		$kata=$klaim.", Pembayaran Oleh Asuransi <strong>".$pure['nama_asuransi']."</strong>";
		
		$kata.=$pure['terklaim']=="1"?"Dengan Nomor Bukti <strong>".$pure['no_bukti']."</strong>":"";
		$numbertell=numbertell($pure['nilai'])." Rupiah ";
		
		$table->addColumn("<div class='left'>".$nomor."</div>", 4, 1,"title");
		$table->addColumn("<div class='left'>".$kata."</div>", 4, 1,"body");
		$table->addColumn("Senilai ", 1, 2);
		$table->addColumn(ArrayAdapter::format("money Rp.", $pure['nilai']), 3, 1,"body");
		$table->addColumn($numbertell, 3, 1,"body");
		
		$kata="Untuk <u>".($pure['keterangan']==""?"Pembayaran Perawatan Rumah Sakit":$pure['keterangan'])."</u>, Dari";
		$table->addColumn($kata, 4, 1,"body");
		
		$pasien=strtoupper($pure['nama_pasien']);
		$nrm=ArrayAdapter::format("digit8", $pure['nrm_pasien']);
		$noreg=ArrayAdapter::format("digit8", $pure['noreg_pasien']);
		
		$table->addColumn("Nama Pasien ", 1, 1);
		$table->addColumn($pasien, 3, 1,"body");
		$table->addColumn("NRM Pasien ", 1, 1);
		$table->addColumn($nrm, 3, 1,"body");
		$table->addColumn("No Registrasi", 1, 1);
		$table->addColumn($noreg, 3, 1,"body");
		
		$table->addColumn("</br>Pembayar</br></br></br><u>".$pasien."</u>", 2, 1);
		$table->addColumn("</br>Penerima</br></br></br><u>".$penerima."</u>", 2, 1,"footer");
		
		$print="";
		global $db;
		$times=getSettings($db, "cashier-print-out-times", "1");
		if($times*1<1) $times=1;
		for($t=1;$t<=($times*1);$t++){
			$print.=$table->getHtml()."<div class='clear line'></div>";
		}
		
		$return=array();
		$return['html_print']=$print;
		$return['html']=$table->getHtml();
		$return['nama_pasien']=$pure['nama_pasien'];
		$return['noreg_pasien']=$pure['noreg_pasien'];
		$return['nrm_pasien']=$pure['nrm_pasien'];
		$return['noref']=$nomor;
		$return['jenis']="Pembayaran Kasir - Asuransi";
		if(getSettings($db,"cashier-webprint-asuransi","0")=="1"){
            $return['webprint'] = array(
				'template' => json_decode(file_get_contents("kasir/webprint/kwitansi.json"), true),
				'data' => array(
						'nomor' => $nomor,
						'nrm_pasien' => ArrayAdapter::format("only-digit6", $pure['nrm_pasien']),
						'nama_pasien' => ArrayAdapter::format("clean-unslug", $pure['nama_pasien']),
						'layanan' => $layanan,
						'jenis_pasien' => ArrayAdapter::format("clean-unslug", $_POST['jenis_pasien']),
						'bayar' => ArrayAdapter::format ( "only-money Rp.", $pure['nilai'] ),
						'terbilang' => numbertell($pure['nilai']) . " Rupiah",
						'tanggal' => ArrayAdapter::format("date d-m-Y", date("y-m-d")),
						'kasir' => ArrayAdapter::format("clean-unslug", $user->getNameOnly())
				)
            );
        }
		
		return $return;
		
		
	}
	
}

?>