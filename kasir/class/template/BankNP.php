<?php
require_once 'smis-framework/smis/template/ModulTemplate.php';
require_once "smis-base/smis-include-duplicate.php";
require_once 'kasir/class/responder/PembayaranResponder.php';
class BankNP extends ModulTemplate {
	private $db;
	private $id_np;
	private $nama_pasien;
	private $nrm_pasien;
	private $noreg_pasien;
	private $carabayar;
	private $uitable;
	public function __construct($db, $id_np, $nama_pasien, $nrm_pasien, $noreg_pasien,$carabayar) {
		parent::__construct ();
		$this->db = $db;
		$this->id_np = $id_np;
		$this->nama_pasien = $nama_pasien;
		$this->nrm_pasien = $nrm_pasien;
		$this->noreg_pasien = $noreg_pasien;
        $this->carabayar=$carabayar;
		$header=array (	"ID","No. Kwitansi",'Tanggal','Nilai',"Bank",'No Bukti',"Biaya","Dibayar");
		$this->uitable = new Table ( $header, "Pembayaran Bank Non Pasien", NULL, true );
		$this->uitable->setName ( "bank_np" );
	}
	public function initialize() {
		if (isset ( $_POST ['action'] ) && $_POST ['action'] != "pembayaran_np" && isset ( $_POST ['super_command'] ) && $_POST ['super_command'] != "") {
			$this->superCommand ( $_POST ['super_command'] );
		} else if (isset ( $_POST ['action'] ) && $_POST ['action'] != "pembayaran_np" && isset ( $_POST ['command'] )) {
			$this->command ( $_POST ['command'] );
		} else {
			$this->phpPreLoad ();
			$this->jsLoader ();
			$this->cssLoader ();
			$this->jsPreLoad ();
			$this->cssPreLoad ();
			$this->htmlPreLoad ();
		}
	}
	
	
	public function command($command) {
		$adapter = new SummaryAdapter();
		$adapter->addSummary("Nilai", "nilai","money Rp.");
		$adapter->addSummary("Biaya", "nilai_tambahan","money Rp.");
		$adapter->addFixValue("Tanggal", "<strong>Total</strong>");
		$adapter->add ( "Tanggal", "waktu", "date d M Y H:i" );
		$adapter->add ( "Nilai", "nilai", "money Rp." );
		$adapter->add ( "Bank", "nama_bank" );
		$adapter->add ( "No Bukti", "no_bukti" );
		$adapter->add ( "Biaya", "nilai_tambahan","money Rp." );
		$adapter->add ( "Dibayar", "di_bank","trivial_0_Tunai_Bank" );
		$adapter->add ( "ID", "id", "only-digit8" );
		$adapter->add ( "No. Kwitansi", "no_kwitansi" );
		$dbtable = new DBTable ( $this->db, "smis_ksr_bayar" );
		$dbtable->setFetchMethode ( DBTable::$ARRAY_FETCH );
		$dbtable->addCustomKriteria ( "id_np", "='" . $this->id_np . "'" );
		$dbtable->addCustomKriteria ( "metode", "='bank_np'" );
		$dbtable->setShowAll(true);

		$dbres = new PembayaranResponder ( $dbtable, $this->uitable, $adapter );
		$dbres->setDuplicate(false,"");
        $dbres->setMetodePembayaran("bank_np");
        $dbres->setAutonomous(getSettings($this->db,"smis_autonomous_id",""));
        if ($dbres->isSave()) {
			global $user;
			$dbres->addColumnFixValue("operator", $user->getNameOnly());
		}
		$data = $dbres->command ( $_POST ['command'] );
		echo json_encode ( $data );
		return;
	}
	public function phpPreLoad() {
		
		$this->uitable->setFooterVisible(false);
		echo $this->uitable->getHtml ();
		
	}
	public function jsPreLoad() {
		echo addJS ( "framework/bootstrap/js/bootstrap-datetimepicker.js" );
		echo addCSS ( "framework/bootstrap/css/bootstrap-datetimepicker.css" );
		echo addJS ( "kasir/resource/js/bank_np.js",false );
	}
}

?>