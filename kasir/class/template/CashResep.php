<?php
require_once 'smis-framework/smis/template/ModulTemplate.php';
require_once "smis-base/smis-include-duplicate.php";
require_once 'kasir/class/responder/PembayaranResponder.php';
class CashResep extends ModulTemplate {
	private $db;
	private $id_resep;
	private $nama_pasien;
	private $nrm_pasien;
	private $noreg_pasien;
	private $carabayar;
	private $uitable;
	public function __construct($db, $id_resep, $nama_pasien, $nrm_pasien, $noreg_pasien,$carabayar) {
		parent::__construct ();
		$this->db = $db;
		$this->id_resep = $id_resep;
		$this->nama_pasien = $nama_pasien;
		$this->nrm_pasien = $nrm_pasien;
		$this->noreg_pasien = $noreg_pasien;
        $this->carabayar=$carabayar;
		$header=array (	"ID","No. Kwitansi",'Tanggal','Nilai','Keterangan' );
		$this->uitable = new Table ( $header, "Pembayaran Cash Resep", NULL, true );
		$this->uitable->setName ( "cash_resep" );
	}
	public function initialize() {
		if (isset ( $_POST ['action'] ) && $_POST ['action'] != "pembayaran_resep" && isset ( $_POST ['super_command'] ) && $_POST ['super_command'] != "") {
			$this->superCommand ( $_POST ['super_command'] );
		} else if (isset ( $_POST ['action'] ) && $_POST ['action'] != "pembayaran_resep" && isset ( $_POST ['command'] )) {
			$this->command ( $_POST ['command'] );
		} else {
			$this->phpPreLoad ();
			$this->jsLoader ();
			$this->cssLoader ();
			$this->jsPreLoad ();
			$this->cssPreLoad ();
			$this->htmlPreLoad ();
		}
	}
	
	
	public function command($command) {
		$adapter = new SimpleAdapter ();
		$adapter->add ( "Tanggal", "waktu", "date d M Y H:i" );
		$adapter->add ( "Nilai", "nilai", "money Rp." );
		$adapter->add ( "Keterangan", "keterangan" );
		$adapter->add ( "ID", "id", "only-digit8" );
		$adapter->add ( "No. Kwitansi", "no_kwitansi" );
		
		$dbtable = new DBTable ( $this->db, "smis_ksr_bayar" );
		$dbtable->setFetchMethode ( DBTable::$ARRAY_FETCH );
		$dbtable->addCustomKriteria ( "id_resep", "='" . $this->id_resep . "'" );
		$dbtable->addCustomKriteria ( "metode", "='cash_resep'" );
		$dbtable->addCustomKriteria ( "ruangan_resep", "='".$_POST['ruangan']."'" );
		$dbres = new PembayaranResponder ( $dbtable, $this->uitable, $adapter );
		$dbres->setMetodePembayaran("cash_resep");
		$dbres->setDuplicate(false,"");
		$dbres->setAutonomous(getSettings($this->db,"smis_autonomous_id",""));
		if($dbres->isSave()){
			$dbres->addColumnFixValue("ruangan_resep",$_POST['ruangan']);
		}
		$data = $dbres->command ( $_POST ['command'] );
		echo json_encode ( $data );
		return;
	}
	public function phpPreLoad() {
		$this->uitable->addModal ( "id", "hidden", "", "" );
		$this->uitable->addModal ( "nama_pasien", "hidden", "", $this->nama_pasien );
		$this->uitable->addModal ( "noreg_pasien", "hidden", "", $this->noreg_pasien );
		$this->uitable->addModal ( "nrm_pasien", "hidden", "", $this->nrm_pasien );
		$this->uitable->addModal ( "id_resep", "hidden", "", $this->id_resep );
		$this->uitable->addModal ( "no_resep", "hidden", "", $this->id_resep );
        $this->uitable->addModal ( "carabayar", "hidden", "", $this->carabayar );
		$this->uitable->addModal ( "waktu", "datetime", "Tanggal", date ( "Y-m-d H:i" ) );
		$this->uitable->addModal ( "nilai", "money", "Nilai", "" );
		$this->uitable->addModal ( "keterangan", "textarea", "Keterangan", "" );
		$modal = $this->uitable->getModal ();
		$modal->setTitle ( "Pembayaran Resep" );
		
		echo $this->uitable->getHtml ();
		echo $modal->getHtml ();
	}
	public function jsPreLoad() {
		echo addJS ( "framework/bootstrap/js/bootstrap-datetimepicker.js" );
		echo addCSS ( "framework/bootstrap/css/bootstrap-datetimepicker.css" );
        echo addJS ( "kasir/resource/js/cash_resep.js",false );
	}
}

?>