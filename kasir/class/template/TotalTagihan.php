<?php 

require_once ("smis-framework/smis/template/ModulTemplate.php");
require_once ("smis-base/smis-include-service-consumer.php");

class TotalTagihan extends ModulTemplate {
	private $service;
	private $db;
	private $nama;
	private $nrm;
	private $noreg;
	public function __construct($db,$nama,$noreg,$nrm) {
		parent::__construct ();
		$this->db = $db;
		$this->nama=$nama;
		$this->noreg=$noreg;
		$this->nrm=$nrm;
	}
	
	public function initialize() {
		$data = $_POST;
		$data ['prototype_slug'] = "";
		$data ['prototype_name'] = "";
		$data ['prototype_implement'] = "";
		$data ['noreg_pasien'] = $this->noreg;
		$data ['nama_pasien'] = $this->nama;
		$data ['nrm_pasien'] = $this->nrm;
		$data ['action'] = "total_tagihan_kasir";
		$data ['page'] = "kasir";
		$service = new ServiceConsumer ( $this->db, "get_total_tagihan_kasir", $data, "kasir" );
		$service->execute ();
		echo $service->getContent ();
	}
	
}
?>