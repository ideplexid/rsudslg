<?php
require_once 'smis-framework/smis/template/ModulTemplate.php';
require_once "smis-base/smis-include-service-consumer.php";
require_once 'smis-libs-hrd/EmployeeResponder.php';
require_once "kasir/class/table/SuratSakitTable.php";
		
class SuratSakit extends ModulTemplate {
    protected $db;
	protected $polislug;
	protected $page;
	protected $protoslug;
	protected $protoname;
	protected $protoimplement;
	protected $action;
	protected $dbtable;
	protected $uitable;
	protected $noreg_pasien;
	protected $nama_pasien;
	protected $nrm_pasien;
	protected $alamat;
	protected $umur;
    protected $tgl_lahir;
    protected $jk;
	protected $ruang_asal;
	protected $id_antrian;
    protected $carabayar;
    
    public function __construct($db, $polislug = "all", $noreg = "", $nrm = "", $nama = "", $alamat = "", $umur = "", $jk = "0", $tgl_lahir = "",  $id_antrian = "0", $page = "kasir", $action = "surat_sakit",$carabayar, $protoslug = "", $protoname = "", $protoimplement = "") {
        $this->db               = $db;
		$this->noreg_pasien     = $noreg;
		$this->id_antrian       = $id_antrian;
		$this->nama_pasien      = $nama;
		$this->nrm_pasien       = $nrm;
		$this->alamat           = $alamat;
		$this->umur             = $umur;
        $this->jk               = $jk;
        $this->tgl_lahir        = $tgl_lahir;
		$this->ruang_asal       = $ruang_asal;
		$this->polislug         = $polislug;
		$this->dbtable          = new DBTable($this->db, "smis_ksr_sks");
		$this->page             = $page;
		$this->protoslug        = $protoslug;
		$this->protoimplement   = $protoimplement;
		$this->protoname        = $protoname;
		$this->action           = $action;
        $this->carabayar        = $carabayar;
        
        if ($noreg != ""){
            $this->dbtable->addCustomKriteria("noreg_pasien", "='" . $noreg . "'");
        }
        $head=array ('NRM',"Pasien","No Register");
        $this->uitable = new SuratSakitTable ($head , " Surat Sakit - " . ($this->polislug == "all" ? "" : ucfirst($this->protoname)), NULL, true);
		$this->uitable->setName($action);
    }
    
    public function command($command) {
		$adapter = new SimpleAdapter ();
		$adapter->add("Pasien", "nama_pasien");
		$adapter->add("NRM", "nrm_pasien", "digit8");
		$adapter->add("No Register", "noreg_pasien", "digit8");
		$dbres = new DBResponder($this->dbtable, $this->uitable, $adapter);
        if($dbres->isSave()){
            if($_POST['id']=="" || $_POST['id']=="0"){
                $code      = str_replace("-","",substr($_POST['tanggal'],0,7));
                $urutan    = getSettings($db,"cashier-surat-sakit-nomor-".$code,0);
                $urutan++;
                setSettings($db,"cashier-surat-sakit-nomor-".$code,$urutan);
                $prefix    = getSettings($db,"cashier-surat-sakit-prefix","");
                $nomor     = $prefix.$code."-".ArrayAdapter::digitFormat("only-digit4",$urutan);
                $dbres     ->addColumnFixValue("nomor",$nomor);
            }
            loadLibrary("smis-libs-function-time");
            $dbres->addColumnFixValue("tarif",getSettings($this->db,"cashier-surat-sakit-tarif","0"));
            $dbres->addColumnFixValue("selama",day_diff($_POST['sampai'],$_POST['dari']));   
        }
		$data = $dbres->command($_POST ['command']);
		echo json_encode($data);
		return;
	}
    
    public function phpPreLoad() {
        $this->uitable->addModal("nama_pasien", "hidden", "", $this->nama_pasien, "n", null, true);
        $this->uitable->addModal("nrm_pasien", "hidden", "", $this->nrm_pasien, "n", null, true);
        $this->uitable->addModal("noreg_pasien", "hidden", "", $this->noreg_pasien, "n", null, true);
        $this->uitable->addModal("jk", "hidden", "", $this->jk, "n", null, true);
        $this->uitable->addModal("umur", "hidden", "", $this->umur, "n", null, true);
        $this->uitable->addModal("ruangan", "hidden", "", $this->polislug);
        $this->uitable->addModal("carabayar", "hidden", "", $this->carabayar ,"n",null,true,null,false);
        
        $the_row                = array ();
		$the_row["id"] 		    = "";
		$the_row["nama_dokter"] = "";
		$the_row["id_dokter"] 	= "";
		$the_row['waktu'] 		= date("Y-m-d H:i:s");
        $exist['noreg_pasien']  = $this->noreg_pasien;
		$exist['nama_pasien']   = $this->nama_pasien;
		$exist['nrm_pasien']    = $this->nrm_pasien;
        if ($this->dbtable->is_exist($exist)){
            $row = $this->dbtable->select($exist);
            $the_row["id"] 		     = $row->id;
            $the_row["nama_dokter"]  = $row->nama_dokter;
            $the_row["id_dokter"] 	 = $row->id_dokter;
            $the_row["tanggal"] 	 = $row->tanggal;
            $the_row["perusahaan"]   = $row->perusahaan;
            $the_row["kondisi"] 	 = $row->kondisi;
            $the_row["dari"] 		 = $row->dari;
            $the_row["sampai"] 	     = $row->sampai;
            $the_row["tarif"] 	     = $row->tarif;
        }else{
            $the_row["nama_dokter"]  = "";
            $the_row["id_dokter"] 	 = "";
            $the_row["tanggal"] 	 = date("Y-m-d H:i:s");
            $the_row["perusahaan"]   = "";
            $the_row["kondisi"] 	 = "1";
            $the_row["dari"] 		 = date("Y-m-d");
            $the_row["sampai"] 	     = "";
            $the_row["tarif"] 	     = $row->tarif;
        }
        $option = new OptionBuilder();
        $option ->add("Perlu Istirahat","1",$the_row ['kondisi']=="1"?"1":"0")
                ->add("Sehat","0",$the_row ['kondisi']=="0"?"1":"0");        
        
        $this->uitable->addModal("id", "hidden", "", $the_row ['id']);
		$this->uitable->addModal("tanggal", "datetime", "Tanggal", $the_row ['tanggal'] ,"y",null,false,null,false);
		$this->uitable->addModal("nama_dokter", "chooser-sks_pasien-dokter_asa-Dokter Pemeriksa", "Dokter", $the_row ['nama_dokter'],"n",null,true,null,true);
		$this->uitable->addModal("id_dokter", "hidden", "", $the_row ['id_dokter'],"y",null,false,null,false);
		$this->uitable->addModal("perusahaan", "text", "Perusahaan", $the_row ['perusahaan'] ,"y",null,false,null,false);
        $this->uitable->addModal("kondisi", "select", "Kondisi",$option->getContent(),"y",null,false,null,false);
        $this->uitable->addModal("dari", "date", "Dari", $the_row ['dari'] ,"y",null,false,null,false);
        $this->uitable->addModal("sampai", "date", "Sampai", $the_row ['sampai'] ,"y",null,false,null,false);
        
        $modal = $this->uitable->getModal ();
		$modal->setTitle("Surat Sakit");
		if ($this->page == "kasir") {
			echo $this->uitable->getHtml ();
		} else {
			echo "<div class='hide'>" . $this->uitable->getHtml () . "</div>";
			$modal->setAlwaysShow(true);
		}
        $button = new Button("","","");
        $button ->setIcon("fa fa-print")
                ->setAction("surat_sakit.cetak()")
                ->setClass("btn-primary")
                ->setIsButton(Button::$ICONIC);
        
		$form = $modal->joinFooterAndForm ();
        $form ->addElement("",$button);
        $form ->setTitle("Surat Keterangan Sakit");
        echo $form ->getHtml();
        
        $hidden_noreg               = new Hidden("sks_noreg_pasien","",$this->noreg_pasien);
        $hidden_nama                = new Hidden("sks_nama_pasien","",$this->nama_pasien);
        $hidden_nrm                 = new Hidden("sks_nrm_pasien","",$this->nrm_pasien);
        $hidden_polislug            = new Hidden("sks_polislug","",$this->polislug);
        $hidden_the_page            = new Hidden("sks_the_page","",$this->page);
        $hidden_the_protoslug       = new Hidden("sks_the_protoslug","",$this->protoslug);
        $hidden_the_protoname       = new Hidden("sks_the_protoname","",$this->protoname);
        $hidden_the_protoimpl       = new Hidden("sks_the_protoimplement","",$this->protoimplement);
        $hidden_antrian             = new Hidden("sks_id_antrian","",$this->id_antrian);
        $hidden_action              = new Hidden("sks_action","",$this->action);
        
        echo $hidden_noreg->getHtml();
        echo $hidden_nama->getHtml();
        echo $hidden_nrm->getHtml();
        echo $hidden_polislug->getHtml();
        echo $hidden_the_page->getHtml();
        echo $hidden_the_protoslug->getHtml();
        echo $hidden_the_protoname->getHtml();
        echo $hidden_the_protoimpl->getHtml();
        echo $hidden_antrian->getHtml();
        echo $hidden_action->getHtml();
        
        echo addJS  ("kasir/resource/js/surat_sakit.js",false);
        echo addCSS ("framework/bootstrap/css/datepicker.css");
        echo addJS  ("framework/bootstrap/js/bootstrap-datetimepicker.js");
        echo addJS  ("framework/bootstrap/js/bootstrap-datepicker.js");
        echo addCSS ("framework/bootstrap/css/bootstrap-datetimepicker.css");
        echo addCSS("kasir/resource/css/print_surat_sakit.css",false);
    }
    
    
    public function superCommand($super_command){
		$super   = new SuperCommand ();
		$eadapt  = new SimpleAdapter ();
		$eadapt  ->add("Jabatan", "nama_jabatan")
                 ->add("Nama", "nama")
                 ->add("NIP", "nip");
		$head_karyawan = array ('Nama','Jabatan',"NIP");
		$dktable  = new Table($head_karyawan, "", NULL, true);
		$dktable  ->setName("dokter_asa")
                  ->setModel(Table::$SELECT);
		$employee = new EmployeeResponder($this->db, $dktable, $eadapt, "dokter");
		$super    ->addResponder("dokter_asa", $employee);
        $init     = $super->initialize ();
		if ($init != null) {
			echo $init;
			return;
		}
    }
}

?>