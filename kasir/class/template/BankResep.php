<?php
require_once 'smis-framework/smis/template/ModulTemplate.php';
require_once "smis-base/smis-include-duplicate.php";
require_once 'kasir/class/responder/PembayaranResponder.php';
class BankResep extends ModulTemplate {
	private $db;
	private $id_resep;
	private $nama_pasien;
	private $nrm_pasien;
	private $noreg_pasien;
	private $carabayar;
	private $uitable;
	public function __construct($db, $id_resep, $nama_pasien, $nrm_pasien, $noreg_pasien,$carabayar) {
		parent::__construct ();
		$this->db = $db;
		$this->id_resep = $id_resep;
		$this->nama_pasien = $nama_pasien;
		$this->nrm_pasien = $nrm_pasien;
		$this->noreg_pasien = $noreg_pasien;
        $this->carabayar=$carabayar;
		$header=array (	"ID","No. Kwitansi",'Tanggal','Nilai',"Bank",'No Bukti',"Biaya","Dibayar");
		$this->uitable = new Table ( $header, "Pembayaran Bank Resep", NULL, true );
		$this->uitable->setName ( "bank_resep" );
	}
	public function initialize() {
		if (isset ( $_POST ['action'] ) && $_POST ['action'] != "pembayaran_resep" && isset ( $_POST ['super_command'] ) && $_POST ['super_command'] != "") {
			$this->superCommand ( $_POST ['super_command'] );
		} else if (isset ( $_POST ['action'] ) && $_POST ['action'] != "pembayaran_resep" && isset ( $_POST ['command'] )) {
			$this->command ( $_POST ['command'] );
		} else {
			$this->phpPreLoad ();
			$this->jsLoader ();
			$this->cssLoader ();
			$this->jsPreLoad ();
			$this->cssPreLoad ();
			$this->htmlPreLoad ();
		}
	}
	
	
	public function command($command) {
		$adapter = new SummaryAdapter();
		$adapter->addSummary("Nilai", "nilai","money Rp.");
		$adapter->addSummary("Biaya", "nilai_tambahan","money Rp.");
		$adapter->addFixValue("Tanggal", "<strong>Total</strong>");
		$adapter->add ( "Tanggal", "waktu", "date d M Y H:i" );
		$adapter->add ( "Nilai", "nilai", "money Rp." );
		$adapter->add ( "Bank", "nama_bank" );
		$adapter->add ( "No Bukti", "no_bukti" );
		$adapter->add ( "Biaya", "nilai_tambahan","money Rp." );
		$adapter->add ( "Dibayar", "di_bank","trivial_0_Tunai_Bank" );
		$adapter->add ( "ID", "id", "only-digit8" );
		$adapter->add ( "No. Kwitansi", "no_kwitansi" );
		$dbtable = new DBTable ( $this->db, "smis_ksr_bayar" );
		$dbtable->setFetchMethode ( DBTable::$ARRAY_FETCH );
		$dbtable->addCustomKriteria ( "id_resep", "='" . $this->id_resep . "'" );
		$dbtable->addCustomKriteria ( "metode", "='bank_resep'" );
		$dbres = new PembayaranResponder ( $dbtable, $this->uitable, $adapter );
		$dbres->setDuplicate(false,"");
        $dbres->setMetodePembayaran("bank_resep");
        $dbres->setAutonomous(getSettings($this->db,"smis_autonomous_id",""));
        if ($dbres->isSave()) {
			global $user;
			$dbres->addColumnFixValue("operator", $user->getNameOnly());
			$dbres->addColumnFixValue("ruangan_resep",$_POST['ruangan']);
		}
		$data = $dbres->command ( $_POST ['command'] );
		echo json_encode ( $data );
		return;
	}
	public function phpPreLoad() {
		require_once 'kasir/class/adapter/BankAdapter.php';
		$vtable = new DBTable ( $this->db, "smis_ksr_bank" );
		$vtable->setOrder ( "bank ASC, rekening ASC, nama ASC " );
		$vtable->setShowAll ( true );
		$v = $vtable->view ( "", 0 );
		$tvadapter = new BankAdapater ();
		$bank = $tvadapter->getContent ( $v ['data'] );
		
		$this->uitable->addModal ( "id", "hidden", "", "" );
		$this->uitable->addModal ( "nama_pasien", "hidden", "", $this->nama_pasien );
		$this->uitable->addModal ( "noreg_pasien", "hidden", "", $this->noreg_pasien );
		$this->uitable->addModal ( "nrm_pasien", "hidden", "", $this->nrm_pasien );
		$this->uitable->addModal ( "id_resep", "hidden", "", $this->id_resep );
		$this->uitable->addModal ( "no_resep", "hidden", "", $this->id_resep );
		$this->uitable->addModal ( "waktu", "datetime", "Tanggal", date ( "Y-m-d H:i" ) );
		$this->uitable->addModal ( "id_bank", "select", "Bank", $bank );
		$this->uitable->addModal ( "no_bukti", "text", "No. Bukti", "" );
		$this->uitable->addModal ( "nilai", "money", "Nilai", "" );
		$this->uitable->addModal ( "persen", "text", "Persentase Biaya", "2.5" );
		$this->uitable->addModal ( "nilai_tambahan", "money", "Nilai Biaya", "" );
		$this->uitable->addModal ( "di_bank", "checkbox", "Lewat Bank", "1" );
		$this->uitable->addModal ( "keterangan", "textarea", "Keterangan", "" );
		$this->uitable->addModal ( "carabayar", "hidden", "", $this->carabayar );
		
		$modal = $this->uitable->getModal ();
		$modal->setTitle ( "Pembayaran Resep" );
		
		echo $this->uitable->getHtml ();
		echo $modal->getHtml ();
	}
	public function jsPreLoad() {
		echo addJS ( "framework/bootstrap/js/bootstrap-datetimepicker.js" );
		echo addCSS ( "framework/bootstrap/css/bootstrap-datetimepicker.css" );
		echo addJS ( "kasir/resource/js/bank_resep.js",false );
	}
}

?>