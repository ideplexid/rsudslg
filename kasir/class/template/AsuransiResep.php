<?php
require_once 'smis-framework/smis/template/ModulTemplate.php';
require_once 'smis-base/smis-include-service-consumer.php';
require_once "smis-base/smis-include-duplicate.php";
require_once 'kasir/class/responder/PembayaranResponder.php';
class AsuransiResep extends ModulTemplate {
	private $db;
	private $id_resep;
	private $nama_pasien;
	private $nrm_pasien;
	private $noreg_pasien;
	private $carabayar;
	private $uitable;
	public function __construct($db, $id_resep, $nama_pasien, $nrm_pasien, $noreg_pasien,$carabayar) {
		parent::__construct ();
		$this->db = $db;
		$this->id_resep = $id_resep;
		$this->nama_pasien = $nama_pasien;
		$this->nrm_pasien = $nrm_pasien;
		$this->noreg_pasien = $noreg_pasien;
		$this->carabayar=$carabayar;
		$header=array (	"ID","No. Kwitansi",'Tanggal','Nilai',"Asuransi","Perusahaan",'No Bukti' ,"Terbayar");
		$this->uitable = new Table ( $header, "Pembayaran Asuransi Resep", NULL, true );
		$this->uitable->setName ( "asuransi_resep" );
	}
    
	public function initialize() {
		if (isset ( $_POST ['action'] ) && $_POST ['action'] != "pembayaran_resep" && isset ( $_POST ['super_command'] ) && $_POST ['super_command'] != "") {
			$this->superCommand ( $_POST ['super_command'] );
		} else if (isset ( $_POST ['action'] ) && $_POST ['action'] != "pembayaran_resep" && isset ( $_POST ['command'] )) {
			$this->command ( $_POST ['command'] );
		} else {
			$this->phpPreLoad ();
			$this->jsLoader ();
			$this->cssLoader ();
			$this->jsPreLoad ();
			$this->cssPreLoad ();
			$this->htmlPreLoad ();
		}
	}
	
	
	public function command($command) {
		$adapter = new SummaryAdapter();
		$adapter->addSummary("Nilai", "nilai","money Rp.");
		$adapter->addFixValue("Tanggal", "<strong>Total</strong>");
		$adapter->add ( "Tanggal", "waktu", "date d M Y H:i" );
		$adapter->add ( "Nilai", "nilai", "money Rp." );
		$adapter->add ( "Asuransi", "nama_asuransi" );
		$adapter->add ( "Perusahaan", "nama_perusahaan" );
		$adapter->add ( "No Bukti", "no_bukti" );
		$adapter->add ( "Keterangan", "keterangan" );
		$adapter->add ( "Terbayar", "terklaim","trivial_1_Ya_Belum" );
		$adapter->add ( "ID", "id", "only-digit8" );
		$adapter->add ( "No. Kwitansi", "no_kwitansi" );
		$dbtable = new DBTable ( $this->db, "smis_ksr_bayar" );
		$dbtable->setFetchMethode ( DBTable::$ARRAY_FETCH );
		$dbtable->addCustomKriteria ( "id_resep", "='" . $this->id_resep . "'" );
		$dbtable->addCustomKriteria ( "metode", "='asuransi_resep'" );
		$dbres = new PembayaranResponder ( $dbtable, $this->uitable, $adapter );
		$dbres->setDuplicate(false,"");
        $dbres->setMetodePembayaran("asuransi_resep");
        $dbres->setAutonomous(getSettings($this->db,"smis_autonomous_id",""));
        if ($dbres->isSave()) {
			global $user;
			$dbres->addColumnFixValue("operator", $user->getNameOnly());
			$dbres->addColumnFixValue("ruangan_resep",$_POST['ruangan']);
		}
		$data = $dbres->command ( $_POST ['command'] );
		echo json_encode ( $data );
		return;
	}
	public function phpPreLoad() {
		$this->uitable->addModal ( "id", "hidden", "", "" );
		$this->uitable->addModal ( "nama_pasien", "hidden", "", $this->nama_pasien );
		$this->uitable->addModal ( "noreg_pasien", "hidden", "", $this->noreg_pasien );
		$this->uitable->addModal ( "nrm_pasien", "hidden", "", $this->nrm_pasien );
		$this->uitable->addModal ( "id_resep", "hidden", "", $this->id_resep );
		$this->uitable->addModal ( "no_resep", "hidden", "", $this->id_resep );
		$this->uitable->addModal ( "id_asuransi", "hidden", "", "" );
		$this->uitable->addModal ( "id_perusahaan", "hidden", "", "" );
		$this->uitable->addModal ( "waktu", "datetime", "Tanggal", date ( "Y-m-d H:i" ) );
		$this->uitable->addModal ( "nama_asuransi", "chooser-asuransi_resep-asuransi_resep_asuransi-Asuransi", "Asuransi", "" );
		$this->uitable->addModal ( "nama_perusahaan", "chooser-asuransi_resep-asuransi_resep_perusahaan-Perusahaan", "Perusahaan", "" );
		$this->uitable->addModal ( "no_bukti", "text", "No. Bukti", "" );
		$this->uitable->addModal ( "nilai", "money", "Nilai", "" );
		$this->uitable->addModal ( "terklaim", "checkbox", "Terbayar", "0" );
		$this->uitable->addModal ( "keterangan", "textarea", "Keterangan", "" );
        $this->uitable->addModal ( "carabayar", "hidden", "", $this->carabayar );
		$modal = $this->uitable->getModal ();
		$modal->setTitle ( "Pembayaran Resep" );
		
		echo $this->uitable->getHtml ();
		echo $modal->getHtml ();
	}
	
	public function superCommand($super_command){
		
		$header_perusahaan=array("Nama","Alamat","Telpon");
		$uitable_perusahaan=new Table($header_perusahaan);
		$uitable_perusahaan->setModel(Table::$SELECT);
		$uitable_perusahaan->setName("asuransi_resep_perusahaan");
		$adapter_perusahaan=new SimpleAdapter();
		$adapter_perusahaan->add("Nama", "nama");
		$adapter_perusahaan->add("Alamat", "alamat");
		$adapter_perusahaan->add("Telpon", "telpon");
		$service_perusahaan=new ServiceResponder($db, $uitable_perusahaan, $adapter_perusahaan, "get_perusahaan");
		
		
		$header_asuransi=array("Nama","Alamat","Telpon");
		$uitable_asuransi=new Table($header_asuransi);
		$uitable_asuransi->setModel(Table::$SELECT);
		$uitable_asuransi->setName("asuransi_resep_asuransi");
		$adapter_asuransi=new SimpleAdapter();
		$adapter_asuransi->add("Nama", "nama");
		$adapter_asuransi->add("Alamat", "alamat");
		$adapter_asuransi->add("Telpon", "telpon");
		$service_asuransi=new ServiceResponder($db, $uitable_asuransi, $adapter_asuransi, "get_asuransi");
		
		$super=new SuperCommand();
		$super->addResponder("asuransi_resep_asuransi", $service_asuransi);
		$super->addResponder("asuransi_resep_perusahaan", $service_perusahaan);
		$data=$super->initialize();
		if($data!=null){
			echo $data;
			return;
		}
	}
	
	public function jsPreLoad() {
		echo addJS ( "framework/bootstrap/js/bootstrap-datetimepicker.js" );
		echo addCSS ( "framework/bootstrap/css/bootstrap-datetimepicker.css" );
        echo addJS ( "kasir/resource/js/asuransi_resep.js",false );
	}
}

?>