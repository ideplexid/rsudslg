<?php
require_once 'smis-framework/smis/template/ModulTemplate.php';
require_once "smis-base/smis-include-duplicate.php";
require_once 'kasir/class/responder/PembayaranResponder.php';
class CashNP extends ModulTemplate {
	private $db;
	private $id_np;
	private $nama_pasien;
	private $nrm_pasien;
	private $noreg_pasien;
	private $carabayar;
	private $uitable;
	public function __construct($db, $id_np, $nama_pasien, $nrm_pasien, $noreg_pasien,$carabayar) {
		parent::__construct ();
		$this->db = $db;
		$this->id_np = $id_np;
		$this->nama_pasien = $nama_pasien;
		$this->nrm_pasien = $nrm_pasien;
		$this->noreg_pasien = $noreg_pasien;
        $this->carabayar=$carabayar;
		$header=array (	"ID","No. Kwitansi",'Tanggal','Nilai','Keterangan' );
		$this->uitable = new Table ( $header, "Pembayaran Cash Non Pasien", NULL, true );
		$this->uitable->setName ( "cash_np" );
	}

	public function initialize() {
		if (isset ( $_POST ['action'] ) && $_POST ['action'] != "pembayaran_np" && isset ( $_POST ['super_command'] ) && $_POST ['super_command'] != "") {
			$this->superCommand ( $_POST ['super_command'] );
		} else if (isset ( $_POST ['action'] ) && $_POST ['action'] != "pembayaran_np" && isset ( $_POST ['command'] )) {
			$this->command ( $_POST ['command'] );
		} else {
			$this->phpPreLoad ();
			$this->jsLoader ();
			$this->cssLoader ();
			$this->jsPreLoad ();
			$this->cssPreLoad ();
			$this->htmlPreLoad ();
		}
	}
	
	
	public function command($command) {
		$adapter = new SimpleAdapter ();
		$adapter->add ( "Tanggal", "waktu", "date d M Y H:i" );
		$adapter->add ( "Nilai", "nilai", "money Rp." );
		$adapter->add ( "Keterangan", "keterangan" );
		$adapter->add ( "ID", "id", "only-digit8" );
		$adapter->add ( "No. Kwitansi", "no_kwitansi" );
		
		$dbtable = new DBTable ( $this->db, "smis_ksr_bayar" );
		$dbtable->setFetchMethode ( DBTable::$ARRAY_FETCH );
		$dbtable->addCustomKriteria ( "id_np", "='" . $this->id_np . "'" );
		$dbtable->addCustomKriteria ( "metode", "='cash_np'" );
        $dbtable->setShowAll(true);

		$dbres = new PembayaranResponder ( $dbtable, $this->uitable, $adapter );
		$dbres->setMetodePembayaran("cash_np");
		$dbres->setDuplicate(false,"");
		$dbres->setAutonomous(getSettings($this->db,"smis_autonomous_id",""));
		
		$data = $dbres->command ( $_POST ['command'] );
		echo json_encode ( $data );
		return;
	}
	public function phpPreLoad() {
		$this->uitable->setFooterVisible(false);
		echo $this->uitable->getHtml ();
		
	}
	public function jsPreLoad() {
		echo addJS ( "framework/bootstrap/js/bootstrap-datetimepicker.js" );
		echo addCSS ( "framework/bootstrap/css/bootstrap-datetimepicker.css" );
        echo addJS ( "kasir/resource/js/cash_np.js",false );
	}
}

?>