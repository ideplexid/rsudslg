<?php
    global $PLUGINS;
    $init ['name'] = 'kasir';
    $init ['path'] = SMIS_DIR . $init ['name'] . "/";
    $init ['description'] = "Kasir";
    $init ['require'] = "administrator, registration, rawat, radiology, laboratory, gizi";
    $init ['service'] = "nothing";
    $init ['type'] = "";
    $init ['version'] = "5.5.4";
    $init ['number'] = "54";

    $myplugins = new Plugin ( $init );
    $PLUGINS [$init ['name']] = $myplugins;
?>