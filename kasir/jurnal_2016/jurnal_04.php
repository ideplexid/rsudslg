<?php
	require_once("smis-base/smis-include-service-consumer.php");
	require_once("kasir/jurnal_2016/classes/OperatorDBResponder.php");
	global $db;
	
	$form = new Form("", "", "Jurnal 04 : Rekap Pendapatan Per Pasien");
	$operator_text = new Text("jurnal_04_operator", "jurnal_04_operator", "");
	$operator_text->setClass("smis-one-option-input");
	$operator_text->setAtribute("disabled='disabled'");
	$browse_button = new Button("", "", "Pilih");
	$browse_button->setClass("btn-info");
	$browse_button->setIsButton(Button::$ICONIC);
	$browse_button->setIcon("icon-white ".Button::$icon_list_alt);
	$browse_button->setAction("jurnal_04_operator.chooser('jurnal_04_operator', 'jurnal_04_operator_button', 'jurnal_04_operator', jurnal_04_operator)");
	$browse_button->setAtribute("id='jurnal_04_operator_browse'");
	$input_group = new InputGroup("");
	$input_group->addComponent($operator_text);
	$input_group->addComponent($browse_button);
	$form->addELement("Operator", $input_group);
	$from_date_text = new Text("jurnal_04_from", "jurnal_04_from", date("Y-m-d") . " 00:00");
	$from_date_text->setClass("mydatetime");
	$from_date_text->setAtribute("data-date-format='yyyy-mm-dd hh:ii'");
	$form->addELement("Waktu Awal", $from_date_text);
	$to_date_text = new Text("jurnal_04_to", "jurnal_04_to", date("Y-m-d H:i"));
	$to_date_text->setClass("mydatetime");
	$to_date_text->setAtribute("data-date-format='yyyy-mm-dd hh:ii'");
	$form->addELement("Waktu Akhir", $to_date_text);
	$consumer_service = new ServiceConsumer(
		$db, 
		"get_jenis_patient",
		null,
		"registration" 
	);
	$content = $consumer_service->execute()->getContent();
	$jenis_pasien_option = new OptionBuilder();
	$jenis_pasien_option->add("SEMUA", "%%", "1");
	foreach($content as $c){
		$jenis_pasien_option->add($c['name'], $c['value']);
	}
	$jenis_pasien_select = new Select("jurnal_04_jenis_pasien", "jurnal_04_jenis_pasien", $jenis_pasien_option->getContent());
	$form->addELement("Jenis Pasien", $jenis_pasien_select);
	$uri_option = new OptionBuilder();
	$uri_option->add("SEMUA", "%%", 1);
	$uri_option->add("IRJA", "0");
	$uri_option->add("IRNA", "1");
	$uri_select = new Select("jurnal_04_uri", "jurnal_04_uri", $uri_option->getContent());
	$form->addELement("IRJA / IRNA", $uri_select);
	$view_button = new Button("", "", "Lihat");
	$view_button->setIsButton(Button::$ICONIC);
	$view_button->setClass("btn-info");
	$view_button->setIcon("icon-white icon-repeat");
	$view_button->setAction("jurnal_04.view()");
	$export_button = new Button("", "", "Ekspor Berkas Excel");
	$export_button->setIsButton(Button::$ICONIC);
	$export_button->setClass("btn-inverse");
	$export_button->setIcon("fa fa-download");
	$export_button->setAtribute("id='jurnal_04_export_button'");
	$button_group = new ButtonGroup("");
	$button_group->addButton($view_button);
	$button_group->addButton($export_button);
	$form->addELement("", $button_group);
	
	$table = new Table(
		array(
			"No.", "No. Reg.", "No. RM", "Nama Pasien", "Jenis Pasien", "Perusahaan", "Asuransi",
			"Tunai", "Tunai Bank Mandiri", "EDC Bank Mandiri", "Tunai BRI", "EDC BRI", "Rekanan Non-PTPN XII", "PTPN XII", "Total"			
		),
		"",
		null,
		true
	);
	$table->setName("jurnal_04");
	$table->setAction(false);
	$table->setFooterVisible(false);
	$table->setHeaderVisible(false);
	$table->addHeader("after", "
		<tr class='inverse'>
			<th rowspan='3' style='vertical-align: middle !important;'>
				<center>No.</center>
			</th>
			<th rowspan='3' style='vertical-align: middle !important;'>
				<center>No. Reg.</center>
			</th>
			<th rowspan='3' style='vertical-align: middle !important;'>
				<center>No. RM.</center>
			</th>
			<th rowspan='3' style='vertical-align: middle !important;'>
				<center>Nama Pasien</center>
			</th>
			<th rowspan='3' style='vertical-align: middle !important;'>
				<center>Jenis Pasien</center>
			</th>
			<th rowspan='3' style='vertical-align: middle !important;'>
				<center>Perusahaan</center>
			</th>
			<th rowspan='3' style='vertical-align: middle !important;'>
				<center>Asuransi</center>
			</th>
			<th rowspan='2' style='vertical-align: middle !important;'>
				<center>K A S</center>
			</th>
			<th colspan='2' style='vertical-align: middle !important;'>
				<center>Bank Mandiri</center>
			</th>
			<th colspan='2' style='vertical-align: middle !important;'>
				<center>BRI</center>
			</th>
			<th colspan='2' style='vertical-align: middle !important;'>
				<center>BCA</center>
			</th>
			<th colspan='2' style='vertical-align: middle !important;'>
				<center>Rekanan</center>
			</th>
			<th rowspan='3' style='vertical-align: middle !important;'>
				<center>Total</center>
			</th>
		</tr>
		<tr class='inverse'>
			<th style='vertical-align: middle !important;'>
				<center>Tunai</center>
			</th>
			<th style='vertical-align: middle !important;'>
				<center>EDC</center>
			</th>
			<th style='vertical-align: middle !important;'>
				<center>Tunai</center>
			</th>
			<th style='vertical-align: middle !important;'>
				<center>EDC</center>
			</th>
			<th style='vertical-align: middle !important;'>
				<center>Tunai</center>
			</th>
			<th style='vertical-align: middle !important;'>
				<center>EDC</center>
			</th>
			<th style='vertical-align: middle !important;'>
				<center>Non-PTPN XII</center>
			</th>
			<th style='vertical-align: middle !important;'>
				<center>PTPN XII</center>
			</th>
		</tr>
		<tr class='inverse'>
			<th style='vertical-align: middle !important;'>
				<center>100.00.</center>
			</th>
			<th style='vertical-align: middle !important;'>
				<center>110.00.00</center>
			</th>
			<th style='vertical-align: middle !important;'>
				<center>110.00.01</center>
			</th>
			<th style='vertical-align: middle !important;'>
				<center>110.01.00</center>
			</th>
			<th style='vertical-align: middle !important;'>
				<center>110.01.01</center>
			</th>
			<th style='vertical-align: middle !important;'>
				<center>110.02.00</center>
			</th>
			<th style='vertical-align: middle !important;'>
				<center>110.02.01</center>
			</th>
			<th style='vertical-align: middle !important;'>
				<center>134.00.xx</center>
			</th>
			<th style='vertical-align: middle !important;'>
				<center>136.00.xx</center>
			</th>
		</tr>
	");
	
	//Operator Chooser:
	$operator_table = new Table(
		array("No.", "Nama Operator"),
		"",
		null,
		true
	);
	$operator_table->setName("jurnal_04_operator");
	$operator_table->setModel(Table::$SELECT);
	$operator_adapter = new SimpleAdapter(true, "No.");
	$operator_adapter->add("Nama Operator", "operator");
	$operator_dbtable = new DBTable($db, "smis_ksr_bayar");
	$filter = "";
	if (isset($_POST['kriteria'])) {
		$filter = " AND operator LIKE '%" . $_POST['kriteria'] . "%' ";
	}
	$query_value = "
		SELECT operator AS 'id', operator
		FROM (
			SELECT DISTINCT operator
			FROM smis_ksr_bayar
			WHERE prop NOT LIKE 'del' AND operator IS NOT NULL AND operator <> '' " . $filter . "
		) v_operator
	";
	$query_count = "
		SELECT COUNT(*)
		FROM (
			" . $query_value . "
		) v_operator
	";
	$operator_dbtable->setPreferredQuery(true, $query_value, $query_count);
	$operator_dbresponder = new OperatorDBResponder(
		$operator_dbtable,
		$operator_table,
		$operator_adapter
	);
	$super_command = new SuperCommand();
	$super_command->addResponder("jurnal_04_operator", $operator_dbresponder);
	$init = $super_command->initialize();
	if ($init != null) {
		echo $init;
		return;
	}
	
	if (isset($_POST['command'])) {
		$command = $_POST['command'];
		require_once("kasir/jurnal_2016/commands/jurnal_04_" . $command . ".php");
		return;
	}
	
	$loading_bar = new LoadingBar("jurnal_04_loading_bar", "");
	$button = new Button("", "", "Batal");
	$button->addClass("btn-primary");
	$button->setIsButton(Button::$ICONIC_TEXT);
	$button->setIcon("fa fa-close");
	$button->setAction("jurnal_04.cancel()");
	$loading_modal = new Modal("jurnal_04_modal", "", "Proses..");
	$loading_modal->addHtml($loading_bar->getHtml(), "after");
	$loading_modal->addFooter($button);
	
	echo $loading_modal->getHtml();
	echo $form->getHtml();
	echo $table->getHtml();
	echo "<div id='jurnal_04_info'></div>";
	echo addJS("framework/smis/js/table_action.js");
	echo addJS("base-js/smis-base-loading.js");
	echo addJS("framework/bootstrap/js/bootstrap-datetimepicker.js");
	echo addCSS("framework/bootstrap/css/bootstrap-datetimepicker.css");
	echo addJS("kasir/jurnal_2016/scripts/jurnal_04_operator_action.js", false);
	echo addJS("kasir/jurnal_2016/scripts/jurnal_04_action.js", false);
	echo addJS("kasir/jurnal_2016/scripts/jurnal_04_main.js", false);
?>