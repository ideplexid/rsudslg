<?php
	require_once 'smis-base/smis-include-service-consumer.php';
	global $db;
	
	$from = $_POST['from'];
	$to = $_POST['to'];
	$persentase_jaspel = $_POST['persentase_jaspel'];
	$num = $_POST['num'];
	$uri = "%%";
	$jenis_pasien = $_POST['jenis_pasien'];
	
	/// Mendapatkan informasi Pasien (terutama No. Registrasi) berdasarkan asal jurnal pasien dengan urutan: 
	/// 1) Pasien Rawat Inap
	/// 2) Pasien Poli Spesialis Center
	/// 3) Pasien Poli Terpadu
	/// Service Provider Unit: Pendaftaran (registration)
	$params = array();
	$params['from'] = $from;
	$params['to'] = $to;
	$params['jenis_pasien'] = "%%";
	$params['num'] = $num;
	$params['uri'] = $uri;
	$params['grup_transaksi'] = $grup_transaksi;
	$consumer_service = new ServiceConsumer(
		$db,
		"get_info_pasien",
		$params,
		"registration"
	);
	$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
	$content = $consumer_service->execute()->getContent();
	$noreg_pasien = $content[0];
	$nrm_pasien = $content[1];
	$nama_pasien = $content[2];
	$tanggal_daftar_pasien = ArrayAdapter::format("date d/m/Y", $content[3]);
	$jenis_pasien = $content[4];
	$nama_perusahaan = $content[5];
	$nama_asuransi = $content[6];
	$id_perusahaan = $content[7];
	$id_asuransi = $content[8];
	
	$arr_ruang_akun = array(
		"rpa_argopuro"				=> "159.01.01",
		"rpa_bromo"					=> "159.01.01",
		"rpa_bromobaru"				=> "159.01.01",
		"rpa_kelud"					=> "159.01.01",
		"rpb_argopuro"				=> "159.01.02",
		"rpb_ijen"					=> "159.01.02",
		"rpb_raung"					=> "159.01.02",
		"rollaas_diamond"			=> "159.01.__",
		"rollaas_emerald"			=> "159.01.__",
		"rollaas_ruby"				=> "159.01.__",
		"rollaas_saphire"			=> "159.01.__",
		"icu"						=> "159.01.03",
		"instalasi_gawat_darurat"	=> "159.01.07",
		"poliumum"					=> "159.01.08",
		"poligigi"					=> "159.01.09",
		"fisiotherapy"				=> "159.01.11",
		"gizi"						=> "159.01.21",
		"hemodialisa"				=> "159.01.23",
		"kamar_operasi"				=> "159.01.24",
		"rpkk_argopuro"				=> "159.01.25",
		"rpkk_kelud"				=> "159.01.25",
		"rpkk_raung"				=> "159.01.25",
		"rpkk_semeru"				=> "159.01.25",
		"rpkk_wilis"				=> "159.01.25",
		"ruang_bersalin"			=> "159.01.25",
		"ruang_pathologis"			=> "159.01.25",
		"laboratory"				=> "159.01.26",
		"depo_farmasi_irja"			=> "159.01.27",
		"depo_farmasi_irna"			=> "159.01.27",
		"radiology"					=> "159.01.28"
	);
	$html = "";
	$params = array();
	$params['noreg_pasien'] = $noreg_pasien;
	$total_jaspel = 0;
	foreach ($arr_ruang_akun as $ruangan => $akun) {
		$consumer_service = new ServiceConsumer(
			$db,
			"get_detil_jasa_pelayanan_jurnal",
			$params,
			$ruangan
		);
		$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
		$content = $consumer_service->execute()->getContent();
		foreach($content as $c) {
			$unit = ArrayAdapter::format("unslug", $ruangan);
			foreach ($c['tindakan_jaspel'] as $dtj) {
				$nama_tindakan = strtoupper($dtj['nama_tindakan']);
				$harga_tindakan = $dtj['harga_tindakan'] == null || $dtj['harga_tindakan'] == 0 ? ArrayAdapter::format("money", "0") : ArrayAdapter::format("money", $dtj['harga_tindakan']);
				$jaspel_tindakan = ArrayAdapter::format("money", "0");
				if ($dtj['jaspel_tindakan'] != null && $dtj['jaspel_tindakan'] != 0) {
					$total_jaspel += round($persentase_jaspel * $dtj['jaspel_tindakan'] / 100);
					$jaspel_tindakan = ArrayAdapter::format("money", round($persentase_jaspel * $dtj['jaspel_tindakan'] / 100));
				}
				$html .= "
					<tr>
						<td></td>
						<td><small>" . $akun . "</small></td>
						<td><small>" . $unit . "</small></td>
						<td><small>" . ArrayAdapter::format("only-digit6", $noreg_pasien) . "</small></td>
						<td><small>" . ArrayAdapter::format("only-digit6", $nrm_pasien) . "</small></td>
						<td><small>" . strtoupper($nama_pasien) . "</small></td>
						<td><small>" . ArrayAdapter::format("unslug", $jenis_pasien) . "</small></td>
						<td><small>" . $nama_tindakan . "</small></td>
						<td><small>" . $harga_tindakan . "</small></td>
						<td><small>" . $jaspel_tindakan . "</small></td>
					</tr>
				";
			}
		}
	}
	if ($total_jaspel > 0) {
		$html .= "
			<tr>
				<td colspan='9'><center><small><strong>TOTAL JASA PELAYANAN PASIEN " . strtoupper($nama_pasien) . " - NRM. " . ArrayAdapter::format("only-digit6", $nrm_pasien) . " - NO. REG. " . ArrayAdapter::format("only-digit6", $noreg_pasien) . "</strong></small></center></td>
				<td><small><strong>" . ArrayAdapter::format("money", $total_jaspel) . "</strong></small></td>
			</tr>
		";
	}
	
	$data = array();
	$data['noreg_pasien'] = ArrayAdapter::format("only-digit6", $noreg_pasien);
	$data['nrm_pasien'] = ArrayAdapter::format("only-digit6", $nrm_pasien);
	$data['nama_pasien'] = ArrayAdapter::format("unslug", $nama_pasien);
	$data['tanggal_daftar_pasien'] = $tanggal_daftar_pasien;
	$data['jenis_pasien'] = ArrayAdapter::format("unslug", $jenis_pasien);
	$data['nama_perusahaan'] = $nama_perusahaan;
	$data['nama_asuransi'] = $nama_asuransi;
	$data['html'] = $html;
	
	echo json_encode($data);
?>