<?php
	require_once 'smis-base/smis-include-service-consumer.php';
	global $db;
	
	$from = $_POST['from'];
	$to = $_POST['to'];
	$persentase_jaspel = $_POST['persentase_jaspel'];
	$num = $_POST['num'];
	$uri = "%%";
	$jenis_pasien = "%%";
	
	/// Mendapatkan informasi Pasien
	/// Service Provider Unit: Pendaftaran (registration)
	$params = array();
	$params['from'] = $from;
	$params['to'] = $to;
	$params['jenis_pasien'] = $jenis_pasien;
	$params['num'] = $num;
	$params['uri'] = $uri;
	$consumer_service = new ServiceConsumer(
		$db,
		"get_info_pasien",
		$params,
		"registration"
	);
	$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
	$content = $consumer_service->execute()->getContent();
	$noreg_pasien = $content[0];
	$nrm_pasien = $content[1];
	$nama_pasien = $content[2];
	$tanggal_daftar_pasien = ArrayAdapter::format("date d/m/Y", $content[3]);
	$jenis_pasien = $content[4];
	$nama_perusahaan = $content[5];
	$nama_asuransi = $content[6];
	$id_perusahaan = $content[7];
	$id_asuransi = $content[8];
	
	$html = "";
	// semua prototype rawat; kecuali poli spesialis
	$arr_rawat_akun = array(
		"rpa_argopuro"				=> "159.01.01",
		"rpa_bromo"					=> "159.01.01",
		"rpa_bromobaru"				=> "159.01.01",
		"rpa_kelud"					=> "159.01.01",
		"rpb_argopuro"				=> "159.01.02",
		"rpb_ijen"					=> "159.01.02",
		"rpb_raung"					=> "159.01.02",
		"rollaas_diamond"			=> "159.01.__",
		"rollaas_emerald"			=> "159.01.__",
		"rollaas_ruby"				=> "159.01.__",
		"rollaas_saphire"			=> "159.01.__",
		"icu"						=> "159.01.03",
		"instalasi_gawat_darurat"	=> "159.01.07",
		"poliumum"					=> "159.01.08",
		"poligigi"					=> "159.01.09",
		"hemodialisa"				=> "159.01.23",
		"rpkk_argopuro"				=> "159.01.25",
		"rpkk_kelud"				=> "159.01.25",
		"rpkk_raung"				=> "159.01.25",
		"rpkk_semeru"				=> "159.01.25",
		"rpkk_wilis"				=> "159.01.25",
		"kamar_operasi"				=> "159.01.__",
		"ruang_bersalin"			=> "159.01.25",
		"ruang_pathologis"			=> "159.01.25",
		"laboratory"				=> "159.01.26",
		"depo_farmasi_irja"			=> "159.01.27",
		"depo_farmasi_irna"			=> "159.01.27",
		"radiology"					=> "159.01.28"
	);
	$params = array();
	$params['noreg_pasien'] = $noreg_pasien;
	foreach ($arr_rawat_akun as $unit => $kode_akun) {
		$consumer_service = new ServiceConsumer(
			$db,
			"get_jasa_keperawatan_jurnal",
			$params,
			$unit
		);
		$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
		$content = $consumer_service->execute()->getContent();
		$jasa_keperawatan = 0;
		if ($content != null)
			$jasa_keperawatan = $content[0]['jasa_keperawatan'];
		if ($jasa_keperawatan > 0) {
			$consumer_service = new ServiceConsumer(
				$db,
				"get_jaspel_keperawatan_jurnal",
				$params,
				$unit
			);
			$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
			$content = $consumer_service->execute()->getContent();
			$jasa_pelayanan = ArrayAdapter::format("money", "0");
			if ($content != null) {
				$jasa_pelayanan_netto = round($content[0]['jaspel'] * $persentase_jaspel / 100);
				$jasa_pelayanan = ArrayAdapter::format("money", $jasa_pelayanan_netto);
				if ($jasa_pelayanan_netto == 0)
					$jasa_pelayanan = ArrayAdapter::format("money", "0");
			}
			$html .= "
				<tr>
					<td></td>
					<td><small>" . $kode_akun . "</small></td>
					<td><small>" . ArrayAdapter::format("unslug", $unit) . "</small></td>
					<td><small>" . ArrayAdapter::format("only-digit6", $noreg_pasien) . "</small></td>
					<td><small>" . ArrayAdapter::format("only-digit6", $nrm_pasien) . "</small></td>
					<td><small>" . strtoupper($nama_pasien) . "</small></td>
					<td><small>" . ArrayAdapter::format("unslug", $jenis_pasien) . "</small></td>
					<td><small>" . ArrayAdapter::format("money", $jasa_keperawatan) . "</small></td>
					<td><small>" . $jasa_pelayanan . "</small></td>
				</tr>
			";
		}
	}
	// fisioterapi - 159.01.11:
	$consumer_service = new ServiceConsumer(
		$db,
		"get_fisioterapi_jurnal",
		$params,
		"fisiotherapy"
	);
	$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
	$content = $consumer_service->execute()->getContent();
	$jasa_keperawatan = 0;
	if ($content != null)
		$jasa_keperawatan = $content[0]['pemeriksaan_reguler'];
	if ($jasa_keperawatan > 0) {
		$consumer_service = new ServiceConsumer(
			$db,
			"get_jaspel_fisioterapi_jurnal",
			$params,
			"fisiotherapy"
		);
		$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
		$content = $consumer_service->execute()->getContent();
		$jasa_pelayanan = ArrayAdapter::format("money", "0");
		if ($content != null) {
			$jasa_pelayanan_netto = round($content[0]['jaspel'] * $persentase_jaspel / 100);
			$jasa_pelayanan = ArrayAdapter::format("money", $jasa_pelayanan_netto);
			if ($jasa_pelayanan_netto == 0)
				$jasa_pelayanan = ArrayAdapter::format("money", "0");
		}
		$html .= "
			<tr>
				<td></td>
				<td><small>159.01.11</small></td>
				<td><small>" . ArrayAdapter::format("unslug", "fisiotherapy") . "</small></td>
				<td><small>" . ArrayAdapter::format("only-digit6", $noreg_pasien) . "</small></td>
				<td><small>" . ArrayAdapter::format("only-digit6", $nrm_pasien) . "</small></td>
				<td><small>" . strtoupper($nama_pasien) . "</small></td>
				<td><small>" . ArrayAdapter::format("unslug", $jenis_pasien) . "</small></td>
				<td><small>" . ArrayAdapter::format("money", $jasa_keperawatan) . "</small></td>
				<td><small>" . $jasa_pelayanan . "</small></td>
			</tr>
		";
	}
	// gizi - 159.01.21:
	$consumer_service = new ServiceConsumer(
		$db,
		"get_gizi_jurnal",
		$params,
		"gizi"
	);
	$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
	$content = $consumer_service->execute()->getContent();
	$jasa_keperawatan = 0;
	if ($content != null)
		$jasa_keperawatan = $content[0]['gizi'];
	if ($jasa_keperawatan > 0) {
		$consumer_service = new ServiceConsumer(
			$db,
			"get_jaspel_keperawatan_jurnal",
			$params,
			"gizi"
		);
		$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
		$content = $consumer_service->execute()->getContent();
		$jasa_pelayanan = ArrayAdapter::format("money", "0");
		if ($content != null) {
			$jasa_pelayanan_netto = round($content[0]['jaspel'] * $persentase_jaspel / 100);
			$jasa_pelayanan = ArrayAdapter::format("money", $jasa_pelayanan_netto);
			if ($jasa_pelayanan_netto == 0)
				$jasa_pelayanan = ArrayAdapter::format("money", "0");
		}
		$html .= "
			<tr>
				<td></td>
				<td><small>159.01.21</small></td>
				<td><small>" . ArrayAdapter::format("unslug", "gizi") . "</small></td>
				<td><small>" . ArrayAdapter::format("only-digit6", $noreg_pasien) . "</small></td>
				<td><small>" . ArrayAdapter::format("only-digit6", $nrm_pasien) . "</small></td>
				<td><small>" . strtoupper($nama_pasien) . "</small></td>
				<td><small>" . ArrayAdapter::format("unslug", $jenis_pasien) . "</small></td>
				<td><small>" . ArrayAdapter::format("money", $jasa_keperawatan) . "</small></td>
				<td><small>" . $jasa_pelayanan . "</small></td>
			</tr>
		";
	}
	// kamar operasi - 159.01.24:
	$jasa_pelayanan = 0;
	$consumer_service = new ServiceConsumer(
		$db,
		"get_jasa_keperawatan_jurnal",
		$params,
		"kamar_operasi"
	);
	$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
	$content = $consumer_service->execute()->getContent();
	$jasa_keperawatan = 0;
	if ($content != null)
		$jasa_keperawatan = $content[0]['jasa_keperawatan'];
	if ($jasa_keperawatan > 0) {
		$consumer_service = new ServiceConsumer(
			$db,
			"get_jaspel_keperawatan_jurnal",
			$params,
			"kamar_operasi"
		);
		$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
		$content = $consumer_service->execute()->getContent();
		$jasa_pelayanan = ArrayAdapter::format("money", "0");
		if ($content != null)
			$jasa_pelayanan += $content[0]['jaspel'];
	}
	$consumer_service = new ServiceConsumer(
		$db,
		"get_ok_jurnal",
		$params,
		"kamar_operasi"
	);
	$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
	$content = $consumer_service->execute()->getContent();
	$jasa_operasi = 0;
	if ($content != null) {
		$jasa_ast_operator_2 = $content[0]['jasa_ast_operator_2'];
		$jasa_ast_anastesi_1 = $content[0]['jasa_ast_anastesi_1'];
		$jasa_ast_anastesi_2 = $content[0]['jasa_ast_anastesi_2'];
		$jasa_oomloop_1 = $content[0]['jasa_oomloop_1'];
		$jasa_oomloop_2 = $content[0]['jasa_oomloop_2'];
		$jasa_instrument_1 = $content[0]['jasa_instrument_1'];
		$jasa_instrument_2 = $content[0]['jasa_instrument_2'];
		$jasa_operasi = $jasa_ast_operator_2 + $jasa_ast_anastesi_1 + $jasa_ast_anastesi_2 + $jasa_oomloop_1 + $jasa_oomloop_2 + $jasa_instrument_1 + $jasa_instrument_2;
		$jasa_pelayanan += $jasa_operasi;
	}
	$jasa_kamar_operasi = $jasa_keperawatan + $jasa_operasi;
	if ($jasa_kamar_operasi > 0)
		$jasa_kamar_operasi = ArrayAdapter::format("money", $jasa_kamar_operasi);
	else
		$jasa_kamar_operasi = ArrayAdapter::format("money", "0");
	$jasa_pelayanan_netto = round($jasa_pelayanan * $persentase_jaspel / 100);
	$jasa_pelayanan = ArrayAdapter::format("money", $jasa_pelayanan_netto);
	if ($jasa_pelayanan_netto == 0)
		$jasa_pelayanan = ArrayAdapter::format("money", "0");
	if ($jasa_kamar_operasi > 0) {
		$html .= "
			<tr>
				<td></td>
				<td><small>159.01.24</small></td>
				<td><small>" . ArrayAdapter::format("unslug", "kamar_operasi") . "</small></td>
				<td><small>" . ArrayAdapter::format("only-digit6", $noreg_pasien) . "</small></td>
				<td><small>" . ArrayAdapter::format("only-digit6", $nrm_pasien) . "</small></td>
				<td><small>" . strtoupper($nama_pasien) . "</small></td>
				<td><small>" . ArrayAdapter::format("unslug", $jenis_pasien) . "</small></td>
				<td><small>" . $jasa_kamar_operasi . "</small></td>
				<td><small>" . $jasa_pelayanan . "</small></td>
			</tr>
		";
	}
	// laboratorium - 159.01.26:
	$consumer_service = new ServiceConsumer(
		$db,
		"get_laboratorium_jurnal",
		$params,
		"laboratory"
	);
	$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
	$jasa_keperawatan = 0;
	$content = $consumer_service->execute()->getContent();
	if ($content != null) {
		$jasa_keperawatan += $content[0]['pemeriksaan_reguler'];
		$jasa_keperawatan += $content[0]['pemeriksaan_cito'];
	}
	if ($jasa_keperawatan > 0) {
		$consumer_service = new ServiceConsumer(
			$db,
			"get_jaspel_laboratorium_jurnal",
			$params,
			"laboratory"
		);
		$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
		$content = $consumer_service->execute()->getContent();
		$jasa_pelayanan = ArrayAdapter::format("money", "0");
		if ($content != null) {
			$jasa_pelayanan_netto = round($content[0]['jaspel'] * $persentase_jaspel / 100);
			$jasa_pelayanan = ArrayAdapter::format("money", $jasa_pelayanan_netto);
			if ($jasa_pelayanan_netto == 0)
				$jasa_pelayanan = ArrayAdapter::format("money", "0");
			
		}
		$html .= "
			<tr>
				<td></td>
				<td><small>159.01.26</small></td>
				<td><small>" . ArrayAdapter::format("unslug", "laboratory") . "</small></td>
				<td><small>" . ArrayAdapter::format("only-digit6", $noreg_pasien) . "</small></td>
				<td><small>" . ArrayAdapter::format("only-digit6", $nrm_pasien) . "</small></td>
				<td><small>" . strtoupper($nama_pasien) . "</small></td>
				<td><small>" . ArrayAdapter::format("unslug", $jenis_pasien) . "</small></td>
				<td><small>" . ArrayAdapter::format("money", $jasa_keperawatan) . "</small></td>
				<td><small>" . $jasa_pelayanan . "</small></td>
			</tr>
		";
	}
	// farmasi - 159.01.27:
	$consumer_service = new ServiceConsumer(
		$db,
		"get_farmasi_jurnal",
		$params,
		"depo_farmasi"
	);
	$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
	$content = $consumer_service->execute()->getContent();
	$jasa_farmasi = 0;
	if ($content != null) {
		$jasa_farmasi += $content[0]['penjualan_rawat_jalan_tanpa_jasa'];
		$jasa_farmasi += $content[0]['penjualan_rawat_inap_tanpa_jasa'];
		$jasa_farmasi += $content[0]['jasa_resep'];
	}
	if ($jasa_farmasi > 0) {
		$html .= "
			<tr>
				<td></td>
				<td><small>159.01.27</small></td>
				<td><small>" . ArrayAdapter::format("unslug", "depo_farmasi") . "</small></td>
				<td><small>" . ArrayAdapter::format("only-digit6", $noreg_pasien) . "</small></td>
				<td><small>" . ArrayAdapter::format("only-digit6", $nrm_pasien) . "</small></td>
				<td><small>" . strtoupper($nama_pasien) . "</small></td>
				<td><small>" . ArrayAdapter::format("unslug", $jenis_pasien) . "</small></td>
				<td><small>" . ArrayAdapter::format("money", $jasa_farmasi) . "</small></td>
				<td><small>" . ArrayAdapter::format("money", "0") . "</small></td>
			</tr>
		";
	}
	// radiologi - 159.01.28:
	$consumer_service = new ServiceConsumer(
		$db,
		"get_radiologi_jurnal",
		$params,
		"radiology"
	);
	$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
	$content = $consumer_service->execute()->getContent();
	$jasa_radiologi = 0;
	if ($content != null) {
		$jasa_radiologi += $content[0]['pemeriksaan_x_ray'] + $content[0]['pemeriksaan_cito'];
		$jasa_radiologi += $content[0]['pemeriksaan_ct_scan'];
		$jasa_radiologi += $content[0]['pemeriksaan_usg_echo'];
	}
	if ($jasa_radiologi > 0) {
		$consumer_service = new ServiceConsumer(
			$db,
			"get_jaspel_radiologi_jurnal",
			$params,
			"radiology"
		);
		$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
		$content = $consumer_service->execute()->getContent();
		$jasa_pelayanan = ArrayAdapter::format("money", "0");
		if ($content != null) {
			$jasa_pelayanan_netto = round($content[0]['jaspel'] * $persentase_jaspel / 100);
			$jasa_pelayanan = ArrayAdapter::format("money", $jasa_pelayanan_netto);
			if ($jasa_pelayanan_netto == 0)
				$jasa_pelayanan = ArrayAdapter::format("money", "0");
		}
		$html .= "
			<tr>
				<td></td>
				<td><small>159.01.28</small></td>
				<td><small>" . ArrayAdapter::format("unslug", "radiology") . "</small></td>
				<td><small>" . ArrayAdapter::format("only-digit6", $noreg_pasien) . "</small></td>
				<td><small>" . ArrayAdapter::format("only-digit6", $nrm_pasien) . "</small></td>
				<td><small>" . strtoupper($nama_pasien) . "</small></td>
				<td><small>" . ArrayAdapter::format("unslug", $jenis_pasien) . "</small></td>
				<td><small>" . ArrayAdapter::format("money", $jasa_radiologi) . "</small></td>
				<td><small>" . $jasa_pelayanan . "</small></td>
			</tr>
		";
	}
	
	$data = array();
	$data['noreg_pasien'] = ArrayAdapter::format("only-digit6", $noreg_pasien);
	$data['nrm_pasien'] = ArrayAdapter::format("only-digit6", $nrm_pasien);
	$data['nama_pasien'] = ArrayAdapter::format("unslug", $nama_pasien);
	$data['tanggal_daftar_pasien'] = $tanggal_daftar_pasien;
	$data['jenis_pasien'] = ArrayAdapter::format("unslug", $jenis_pasien);
	$data['nama_perusahaan'] = $nama_perusahaan;
	$data['nama_asuransi'] = $nama_asuransi;
	$data['html'] = $html;
	
	echo json_encode($data);
?>