<?php
	$jumlah_inap = 0;
	$jumlah_terpadu = 0;
	$jumlah_spesialis = 0;
	
	// inap:
	$params = array();
	$params['from'] = $_POST['from'];
	$params['to'] = $_POST['to'];
	$params['jenis_pasien'] = "%%";
	$params['uri'] = "1";
	$params['grup_transaksi'] = "rawat_inap";
	$service = new ServiceConsumer(
		$db,
		"get_jumlah_pasien",
		$params,
		"registration"
	);
	$service->setMode(ServiceConsumer::$CLEAN_BOTH);
	$content = $service->execute()->getContent();
	$jumlah_inap += $content[0];
	
	// terpadu:
	$params = array();
	$params['from'] = $_POST['from'];
	$params['to'] = $_POST['to'];
	$params['jenis_pasien'] = "%%";
	$params['uri'] = "0";
	$params['grup_transaksi'] = "rajal_poli_terpadu";
	$service = new ServiceConsumer(
		$db,
		"get_jumlah_pasien",
		$params,
		"registration"
	);
	$service->setMode(ServiceConsumer::$CLEAN_BOTH);
	$content = $service->execute()->getContent();
	$jumlah_terpadu += $content[0];
	
	// spesialis:
	$params = array();
	$params['from'] = $_POST['from'];
	$params['to'] = $_POST['to'];
	$params['jenis_pasien'] = "%%";
	$params['uri'] = "0";
	$params['grup_transaksi'] = "rajal_poli_spesialis";
	$service = new ServiceConsumer(
		$db,
		"get_jumlah_pasien",
		$params,
		"registration"
	);
	$service->setMode(ServiceConsumer::$CLEAN_BOTH);
	$content = $service->execute()->getContent();
	$jumlah_spesialis += $content[0];
	
	$data = array();
	$data['jumlah_inap'] = $jumlah_inap;
	$data['jumlah_terpadu'] = $jumlah_terpadu;
	$data['jumlah_spesialis'] = $jumlah_spesialis;
	$data['timestamp'] = date("d-m-Y H:i:s");
	echo json_encode($data);
?>