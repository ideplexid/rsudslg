<?php
	require_once 'smis-base/smis-include-service-consumer.php';
	global $db;
	
	$from = $_POST['from'];
	$to = $_POST['to'];
	$num = $_POST['num'];
	$uri = $_POST['uri'];
	$jenis_pasien = $_POST['jenis_pasien'];
	$persentase_jaspel  = $_POST['persentase_jaspel'];
	
	/// Mendapatkan informasi Pasien
	$params = array();
	$params['from'] = $from;
	$params['to'] = $to;
	$params['jenis_pasien'] = $jenis_pasien;
	$params['num'] = $num;
	$params['uri'] = $uri;
	$consumer_service = new ServiceConsumer(
		$db,
		"get_info_pasien",
		$params,
		"registration"
	);
	$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
	$content = $consumer_service->execute()->getContent();
	$noreg_pasien = $content[0] * 1;
	$nrm_pasien = $content[1];
	$nama_pasien = $content[2];
	$tanggal_daftar_pasien = ArrayAdapter::format("date d/m/Y", $content[3]);
	$jenis_pasien = $content[4];
	$nama_perusahaan = $content[5];
	$nama_asuransi = $content[6];
	$id_perusahaan = $content[7];
	$id_asuransi = $content[8];
	
	/// Mendapatkan informasi jurnal di RPA :
	/// 1) RPA - Argopuro (rpa_argopuro)
	/// 2) RPA - Bromo (rpa_bromo)
	/// 3) RPA - Bromo Baru (rpa_bromobaru)
	/// 4) RPA - Kelud (rpa_kelud)
	$prototype_slugs = array(
		"rpa_argopuro",
		"rpa_bromo",
		"rpa_bromobaru",
		"rpa_kelud"
	);
	/// Jasa / Tindakan Keperawatan RPA (html element: rpa-1-debet, rpa-1-kredit) :
	$rpa_1_debet = 0;	// debit total pendapatan tindakan keperawatan
	$rpa_1_kredit = 0; 	// kredit total pendapatan tindakan keperawatan
	foreach ($prototype_slugs as $prototype_slug) {
		$params = array();
		$params['noreg_pasien'] = $noreg_pasien;
		$consumer_service = new ServiceConsumer(
			$db,
			"get_jasa_keperawatan_jurnal",
			$params,
			$prototype_slug
		);
		$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
		$content = $consumer_service->execute()->getContent();
		if ($content != null)
			$rpa_1_debet += $content[0]['jasa_keperawatan'];
	}
	$rpa_2_debet = 0;	// debit pendapatan tindakan keperawatan rs
	$rpa_2_kredit = 0;	// kredit pendapatan tindakan keperawatan rs
	$rpa_3_debet = 0;	// debit jaspel keperawatan
	$rpa_3_kredit = 0;	// kredit jaspel keperawatan
	if ($persentase_jaspel > 0) {
		$rpa_3_kredit = $persentase_jaspel * $rpa_1_debet / 100;
		$rpa_2_kredit = $rpa_1_debet - $rpa_3_kredit;
	}
	
	/// Mendapatkan informasi jurnal di RPB :
	/// 1) RPB - Argopuro (rpb_argopuro)
	/// 2) RPB - Ijen (rpb_ijen)
	/// 3) RPB - Raung (rpb_raung)
	/// 4) RPB - Kelud (rpb_kelud)
	$prototype_slugs = array(
		"rpb_argopuro",
		"rpb_ijen",
		"rpb_raung",
		"rpb_kelud"
	);
	$rpb_1_debet = 0;	// debit total pendapatan tindakan keperawatan
	$rpb_1_kredit = 0; 	// kredit total pendapatan tindakan keperawatan
	foreach ($prototype_slugs as $prototype_slug) {
		$params = array();
		$params['noreg_pasien'] = $noreg_pasien;
		$consumer_service = new ServiceConsumer(
			$db,
			"get_jasa_keperawatan_jurnal",
			$params,
			$prototype_slug
		);
		$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
		$content = $consumer_service->execute()->getContent();
		if ($content != null)
			$rpb_1_debet += $content[0]['jasa_keperawatan'];
	}
	$rpb_2_debet = 0;	// debit pendapatan tindakan keperawatan rs
	$rpb_2_kredit = 0;	// kredit pendapatan tindakan keperawatan rs
	$rpb_3_debet = 0;	// debit jaspel keperawatan
	$rpb_3_kredit = 0;	// kredit jaspel keperawatan
	if ($persentase_jaspel > 0) {
		$rpb_3_kredit = $persentase_jaspel * $rpb_1_debet / 100;
		$rpb_2_kredit = $rpb_1_debet - $rpb_3_kredit;
	}
	
	/// Mendapatkan informasi jurnal di RPC :
	/// 1) Rollaas Diamond (rollaas_diamond)
	/// 2) Rollaas Emerald (rollaas_emerald)
	$prototype_slugs = array(
		"rollaas_diamond",
		"rollaas_emerald"
	);
	$rpc_1_debet = 0;	// debit total pendapatan tindakan keperawatan
	$rpc_1_kredit = 0; 	// kredit total pendapatan tindakan keperawatan
	foreach ($prototype_slugs as $prototype_slug) {
		$params = array();
		$params['noreg_pasien'] = $noreg_pasien;
		$consumer_service = new ServiceConsumer(
			$db,
			"get_jasa_keperawatan_jurnal",
			$params,
			$prototype_slug
		);
		$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
		$content = $consumer_service->execute()->getContent();
		if ($content != null)
			$rpc_1_debet += $content[0]['jasa_keperawatan'];
	}
	$rpc_2_debet = 0;	// debit pendapatan tindakan keperawatan rs
	$rpc_2_kredit = 0;	// kredit pendapatan tindakan keperawatan rs
	$rpc_3_debet = 0;	// debit jaspel keperawatan
	$rpc_3_kredit = 0;	// kredit jaspel keperawatan
	if ($persentase_jaspel > 0) {
		$rpc_3_kredit = $persentase_jaspel * $rpc_1_debet / 100;
		$rpc_2_kredit = $rpc_1_debet - $rpc_3_kredit;
	}
	
	/// Mendapatkan informasi jurnal di RPD :
	/// 1) Rollaas Ruby (rollaas_ruby)
	$prototype_slugs = array(
		"rollaas_ruby"
	);
	$rpd_1_debet = 0;	// debit total pendapatan tindakan keperawatan
	$rpd_1_kredit = 0; 	// kredit total pendapatan tindakan keperawatan
	foreach ($prototype_slugs as $prototype_slug) {
		$params = array();
		$params['noreg_pasien'] = $noreg_pasien;
		$consumer_service = new ServiceConsumer(
			$db,
			"get_jasa_keperawatan_jurnal",
			$params,
			$prototype_slug
		);
		$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
		$content = $consumer_service->execute()->getContent();
		if ($content != null)
			$rpd_1_debet += $content[0]['jasa_keperawatan'];
	}
	$rpd_2_debet = 0;	// debit pendapatan tindakan keperawatan rs
	$rpd_2_kredit = 0;	// kredit pendapatan tindakan keperawatan rs
	$rpd_3_debet = 0;	// debit jaspel keperawatan
	$rpd_3_kredit = 0;	// kredit jaspel keperawatan
	if ($persentase_jaspel > 0) {
		$rpd_3_kredit = $persentase_jaspel * $rpd_1_debet / 100;
		$rpd_2_kredit = $rpd_1_debet - $rpd_3_kredit;
	}
	
	/// Mendapatkan informasi jurnal di RPE :
	/// 1) Rollaas Saphire (rollaas_saphire)
	$prototype_slugs = array(
		"rollaas_saphire"
	);
	$rpe_1_debet = 0;	// debit total pendapatan tindakan keperawatan
	$rpe_1_kredit = 0; 	// kredit total pendapatan tindakan keperawatan
	foreach ($prototype_slugs as $prototype_slug) {
		$params = array();
		$params['noreg_pasien'] = $noreg_pasien;
		$consumer_service = new ServiceConsumer(
			$db,
			"get_jasa_keperawatan_jurnal",
			$params,
			$prototype_slug
		);
		$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
		$content = $consumer_service->execute()->getContent();
		if ($content != null)
			$rpe_1_debet += $content[0]['jasa_keperawatan'];
	}
	$rpe_2_debet = 0;	// debit pendapatan tindakan keperawatan rs
	$rpe_2_kredit = 0;	// kredit pendapatan tindakan keperawatan rs
	$rpe_3_debet = 0;	// debit jaspel keperawatan
	$rpe_3_kredit = 0;	// kredit jaspel keperawatan
	if ($persentase_jaspel > 0) {
		$rpe_3_kredit = $persentase_jaspel * $rpe_1_debet / 100;
		$rpe_2_kredit = $rpe_1_debet - $rpe_3_kredit;
	}
	
	/// Mendapatkan informasi jurnal di ICU / RPO :
	$prototype_slugs = array(
		"icu"
	);
	$icu_1_debet = 0;	// debit total pendapatan tindakan keperawatan
	$icu_1_kredit = 0; 	// kredit total pendapatan tindakan keperawatan
	foreach ($prototype_slugs as $prototype_slug) {
		$params = array();
		$params['noreg_pasien'] = $noreg_pasien;
		$consumer_service = new ServiceConsumer(
			$db,
			"get_jasa_keperawatan_jurnal",
			$params,
			$prototype_slug
		);
		$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
		$content = $consumer_service->execute()->getContent();
		if ($content != null)
			$icu_1_debet += $content[0]['jasa_keperawatan'];
	}
	$icu_2_debet = 0;	// debit pendapatan tindakan keperawatan rs
	$icu_2_kredit = 0;	// kredit pendapatan tindakan keperawatan rs
	$icu_3_debet = 0;	// debit jaspel keperawatan
	$icu_3_kredit = 0;	// kredit jaspel keperawatan
	if ($persentase_jaspel > 0) {
		$icu_3_kredit = $persentase_jaspel * $icu_1_debet / 100;
		$icu_2_kredit = $icu_1_debet - $icu_3_kredit;
	}
	
	/// Mendapatkan informasi jurnal di UGD :
	$prototype_slugs = array(
		"instalasi_gawat_darurat"
	);
	$igd_1_debet = 0;	// debit total pendapatan tindakan keperawatan
	$igd_1_kredit = 0; 	// kredit total pendapatan tindakan keperawatan
	foreach ($prototype_slugs as $prototype_slug) {
		$params = array();
		$params['noreg_pasien'] = $noreg_pasien;
		$consumer_service = new ServiceConsumer(
			$db,
			"get_jasa_keperawatan_jurnal",
			$params,
			$prototype_slug
		);
		$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
		$content = $consumer_service->execute()->getContent();
		if ($content != null)
			$igd_1_debet += $content[0]['jasa_keperawatan'];
	}
	$igd_2_debet = 0;	// debit pendapatan tindakan keperawatan rs
	$igd_2_kredit = 0;	// kredit pendapatan tindakan keperawatan rs
	$igd_3_debet = 0;	// debit jaspel keperawatan
	$igd_3_kredit = 0;	// kredit jaspel keperawatan
	if ($persentase_jaspel > 0) {
		$igd_3_kredit = $persentase_jaspel * $igd_1_debet / 100;
		$igd_2_kredit = $igd_1_debet - $igd_3_kredit;
	}
	
	/// Mendapatkan informasi jurnal di Poli Umum :
	$prototype_slugs = array(
		"poliumum"
	);
	$poliumum_1_debet = 0;		// debit total pendapatan tindakan keperawatan
	$poliumum_1_kredit = 0; 	// kredit total pendapatan tindakan keperawatan
	foreach ($prototype_slugs as $prototype_slug) {
		$params = array();
		$params['noreg_pasien'] = $noreg_pasien;
		$consumer_service = new ServiceConsumer(
			$db,
			"get_jasa_keperawatan_jurnal",
			$params,
			$prototype_slug
		);
		$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
		$content = $consumer_service->execute()->getContent();
		if ($content != null)
			$poliumum_1_debet += $content[0]['jasa_keperawatan'];
	}
	$poliumum_2_debet = 0;	// debit pendapatan tindakan keperawatan rs
	$poliumum_2_kredit = 0;	// kredit pendapatan tindakan keperawatan rs
	$poliumum_3_debet = 0;	// debit jaspel keperawatan
	$poliumum_3_kredit = 0;	// kredit jaspel keperawatan
	if ($persentase_jaspel > 0) {
		$poliumum_3_kredit = $persentase_jaspel * $poliumum_1_debet / 100;
		$poliumum_2_kredit = $poliumum_1_debet - $poliumum_3_kredit;
	}

	/// Mendapatkan informasi jurnal di Fisioterapi :
	$fisiotherapy_1_debet = 0;	// debit total pendapatan tindakan keperawatan
	$fisiotherapy_1_kredit = 0; 	// kredit total pendapatan tindakan keperawatan
	$params = array();
	$params['noreg_pasien'] = $noreg_pasien;
	$consumer_service = new ServiceConsumer(
		$db,
		"get_jaspel_fisioterapi_jurnal",
		$params,
		"fisiotherapy"
	);
	$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
	$content = $consumer_service->execute()->getContent();
	if ($content != null)
		$fisiotherapy_1_debet += $content[0]['jaspel'];
	$fisiotherapy_2_debet = 0;	// debit pendapatan tindakan keperawatan rs
	$fisiotherapy_2_kredit = 0;	// kredit pendapatan tindakan keperawatan rs
	$fisiotherapy_3_debet = 0;	// debit jaspel keperawatan
	$fisiotherapy_3_kredit = 0;	// kredit jaspel keperawatan
	if ($persentase_jaspel > 0) {
		$fisiotherapy_3_kredit = $persentase_jaspel * $fisiotherapy_1_debet / 100;
		$fisiotherapy_2_kredit = $fisiotherapy_1_debet - $fisiotherapy_3_kredit;
	}

	/// Mendapatkan informasi jurnal di Poli Obgyn :
	$poli_obgyn_1_debet = 0;
	$poli_obgyn_1_kredit = 0;
	$poli_obgyn_2_debet = 0;
	$poli_obgyn_2_kredit = 0;
	$poli_obgyn_3_debet = 0;
	$poli_obgyn_3_kredit = 0;

	/// Mendapatkan informasi jurnal di Poli Syaraf :
	$poli_syaraf_1_debet = 0;
	$poli_syaraf_1_kredit = 0;
	$poli_syaraf_2_debet = 0;
	$poli_syaraf_2_kredit = 0;
	$poli_syaraf_3_debet = 0;
	$poli_syaraf_3_kredit = 0;
	
	/// Mendapatkan informasi jurnal di Hemodialisa (html element: hemodialisa) :
	/// Jasa / Tindakan Keperawatan Hemodialisa (html element: hemodialisa-3-debet, hemodialisa-3-kredit) :
	$hemodialisa_1_debet = 0;
	$hemodialisa_1_kredit = 0;
	$params = array();
	$params['noreg_pasien'] = $noreg_pasien;
	$consumer_service = new ServiceConsumer(
		$db,
		"get_jasa_keperawatan_jurnal",
		$params,
		"hemodialisa"
	);
	$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
	$content = $consumer_service->execute()->getContent();
	if ($content != null)
		$hemodialisa_1_debet += $content[0]['jasa_keperawatan'];
	$hemodialisa_2_debet = 0;	// debit pendapatan tindakan keperawatan rs
	$hemodialisa_2_kredit = 0;	// kredit pendapatan tindakan keperawatan rs
	$hemodialisa_3_debet = 0;	// debit jaspel keperawatan
	$hemodialisa_3_kredit = 0;	// kredit jaspel keperawatan
	if ($persentase_jaspel > 0) {
		$hemodialisa_3_kredit = $persentase_jaspel * $hemodialisa_1_debet / 100;
		$hemodialisa_2_kredit = $hemodialisa_1_debet - $hemodialisa_3_kredit;
	}
	
	/// Mendapatkan informasi jurnal di Kamar Operasi (html element: kamar_operasi)
	/// Sewa Kamar Kamar Operasi (html element: kamar_operasi-1-debet, kamar_operasi-1-kredit) :
	$kamar_operasi_1_debet = 0;
	$kamar_operasi_1_kredit = 0;
	$params = array();
	$params['noreg_pasien'] = $noreg_pasien;
	$consumer_service = new ServiceConsumer(
		$db,
		"get_jasa_keperawatan_jurnal",
		$params,
		"kamar_operasi"
	);
	$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
	$content = $consumer_service->execute()->getContent();
	if ($content != null)
		$kamar_operasi_1_debet += $content[0]['jasa_keperawatan'];
	$kamar_operasi_2_debet = 0;		// debit pendapatan tindakan keperawatan rs
	$kamar_operasi_2_kredit = 0;	// kredit pendapatan tindakan keperawatan rs
	$kamar_operasi_3_debet = 0;		// debit jaspel keperawatan
	$kamar_operasi_3_kredit = 0;	// kredit jaspel keperawatan
	if ($persentase_jaspel > 0) {
		$kamar_operasi_3_kredit = $persentase_jaspel * $kamar_operasi_1_debet / 100;
		$kamar_operasi_2_kredit = $kamar_operasi_1_debet - $kamar_operasi_3_kredit;
	}
	
	/// Mendapatkan informasi jurnal di RPKK :
	/// 1) RPKK - Argopuro (rpkk_argopuro)
	/// 2) RPKK - Kelud (rpkk_kelud)
	/// 3) RPKK - Raung (rpkk_raung)
	/// 4) RPKK - Semeru (rpkk_semeru)
	/// 5) RPKK - Wilis (rpkk_wilis)
	$prototype_slugs = array(
		"rpkk_argopuro",
		"rpkk_kelud",
		"rpkk_raung",
		"rpkk_semeru",
		"rpkk_wilis",
		"ruang_bersalin",
		"ruang_pathologis"
	);
	$rpkk_1_debet = 0;		// debit total pendapatan tindakan keperawatan
	$rpkk_1_kredit = 0; 	// kredit total pendapatan tindakan keperawatan
	foreach ($prototype_slugs as $prototype_slug) {
		$params = array();
		$params['noreg_pasien'] = $noreg_pasien;
		$consumer_service = new ServiceConsumer(
			$db,
			"get_jasa_keperawatan_jurnal",
			$params,
			$prototype_slug
		);
		$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
		$content = $consumer_service->execute()->getContent();
		if ($content != null)
			$rpkk_1_debet += $content[0]['jasa_keperawatan'];
	}
	$rpkk_2_debet = 0;	// debit pendapatan tindakan keperawatan rs
	$rpkk_2_kredit = 0;	// kredit pendapatan tindakan keperawatan rs
	$rpkk_3_debet = 0;	// debit jaspel keperawatan
	$rpkk_3_kredit = 0;	// kredit jaspel keperawatan
	if ($persentase_jaspel > 0) {
		$rpkk_3_kredit = $persentase_jaspel * $rpkk_1_debet / 100;
		$rpkk_2_kredit = $rpkk_1_debet - $rpkk_3_kredit;
	}
			
	$data = array();
	$data['noreg_pasien'] = ArrayAdapter::format("only-digit6", $noreg_pasien);
	$data['nrm_pasien'] = ArrayAdapter::format("only-digit6", $nrm_pasien);
	$data['nama_pasien'] = ArrayAdapter::format("unslug", $nama_pasien);
	$data['tanggal_daftar_pasien'] = $tanggal_daftar_pasien;
	$data['jenis_pasien'] = ArrayAdapter::format("unslug", $jenis_pasien);
	$data['nama_perusahaan'] = $nama_perusahaan;
	$data['nama_asuransi'] = $nama_asuransi;
	$data['rpa_1_debet'] = $rpa_1_debet;
	$data['rpa_1_kredit'] = $rpa_1_kredit;
	$data['rpa_2_debet'] = $rpa_2_debet;
	$data['rpa_2_kredit'] = $rpa_2_kredit;
	$data['rpa_3_debet'] = $rpa_3_debet;
	$data['rpa_3_kredit'] = $rpa_3_kredit;
	$data['rpb_1_debet'] = $rpb_1_debet;
	$data['rpb_1_kredit'] = $rpb_1_kredit;
	$data['rpb_2_debet'] = $rpb_2_debet;
	$data['rpb_2_kredit'] = $rpb_2_kredit;
	$data['rpb_3_debet'] = $rpb_3_debet;
	$data['rpb_3_kredit'] = $rpb_3_kredit;
	$data['icu_1_debet'] = $icu_1_debet;
	$data['icu_1_kredit'] = $icu_1_kredit;
	$data['icu_2_debet'] = $icu_2_debet;
	$data['icu_2_kredit'] = $icu_2_kredit;
	$data['icu_3_debet'] = $icu_3_debet;
	$data['icu_3_kredit'] = $icu_3_kredit;
	$data['rpc_1_debet'] = $rpc_1_debet;
	$data['rpc_1_kredit'] = $rpc_1_kredit;
	$data['rpc_2_debet'] = $rpc_2_debet;
	$data['rpc_2_kredit'] = $rpc_2_kredit;
	$data['rpc_3_debet'] = $rpc_3_debet;
	$data['rpc_3_kredit'] = $rpc_3_kredit;
	$data['rpd_1_debet'] = $rpd_1_debet;
	$data['rpd_1_kredit'] = $rpd_1_kredit;
	$data['rpd_2_debet'] = $rpd_2_debet;
	$data['rpd_2_kredit'] = $rpd_2_kredit;
	$data['rpd_3_debet'] = $rpd_3_debet;
	$data['rpd_3_kredit'] = $rpd_3_kredit;
	$data['rpe_1_debet'] = $rpe_1_debet;
	$data['rpe_1_kredit'] = $rpe_1_kredit;
	$data['rpe_2_debet'] = $rpe_2_debet;
	$data['rpe_2_kredit'] = $rpe_2_kredit;
	$data['rpe_3_debet'] = $rpe_3_debet;
	$data['rpe_3_kredit'] = $rpe_3_kredit;
	$data['rpkk_1_debet'] = $rpkk_1_debet;
	$data['rpkk_1_kredit'] = $rpkk_1_kredit;
	$data['rpkk_2_debet'] = $rpkk_2_debet;
	$data['rpkk_2_kredit'] = $rpkk_2_kredit;
	$data['rpkk_3_debet'] = $rpkk_3_debet;
	$data['rpkk_3_kredit'] = $rpkk_3_kredit;
	$data['igd_1_debet'] = $igd_1_debet;
	$data['igd_1_kredit'] = $igd_1_kredit;
	$data['igd_2_debet'] = $igd_2_debet;
	$data['igd_2_kredit'] = $igd_2_kredit;
	$data['igd_3_debet'] = $igd_3_debet;
	$data['igd_3_kredit'] = $igd_3_kredit;
	$data['poliumum_1_debet'] = $poliumum_1_debet;
	$data['poliumum_1_kredit'] = $poliumum_1_kredit;
	$data['poliumum_2_debet'] = $poliumum_2_debet;
	$data['poliumum_2_kredit'] = $poliumum_2_kredit;
	$data['poliumum_3_debet'] = $poliumum_3_debet;
	$data['poliumum_3_kredit'] = $poliumum_3_kredit;
	$data['fisiotherapy_1_debet'] = $fisiotherapy_1_debet;
	$data['fisiotherapy_1_kredit'] = $fisiotherapy_1_kredit;
	$data['fisiotherapy_2_debet'] = $fisiotherapy_2_debet;
	$data['fisiotherapy_2_kredit'] = $fisiotherapy_2_kredit;
	$data['fisiotherapy_3_debet'] = $fisiotherapy_3_debet;
	$data['fisiotherapy_3_kredit'] = $fisiotherapy_3_kredit;
	$data['poli_obgyn_1_debet'] = $poli_obgyn_1_debet;
	$data['poli_obgyn_1_kredit'] = $poli_obgyn_1_kredit;
	$data['poli_obgyn_2_debet'] = $poli_obgyn_2_debet;
	$data['poli_obgyn_2_kredit'] = $poli_obgyn_2_kredit;
	$data['poli_obgyn_3_debet'] = $poli_obgyn_3_debet;
	$data['poli_obgyn_3_kredit'] = $poli_obgyn_3_kredit;
	$data['poli_syaraf_1_debet'] = $poli_syaraf_1_debet;
	$data['poli_syaraf_1_kredit'] = $poli_syaraf_1_kredit;
	$data['poli_syaraf_2_debet'] = $poli_syaraf_2_debet;
	$data['poli_syaraf_2_kredit'] = $poli_syaraf_2_kredit;
	$data['poli_syaraf_3_debet'] = $poli_syaraf_3_debet;
	$data['poli_syaraf_3_kredit'] = $poli_syaraf_3_kredit;
	$data['hemodialisa_1_debet'] = $hemodialisa_1_debet;
	$data['hemodialisa_1_kredit'] = $hemodialisa_1_kredit;
	$data['hemodialisa_2_debet'] = $hemodialisa_2_debet;
	$data['hemodialisa_2_kredit'] = $hemodialisa_2_kredit;
	$data['hemodialisa_3_debet'] = $hemodialisa_3_debet;
	$data['hemodialisa_3_kredit'] = $hemodialisa_3_kredit;
	$data['kamar_operasi_1_debet'] = $kamar_operasi_1_debet;
	$data['kamar_operasi_1_kredit'] = $kamar_operasi_1_kredit;
	$data['kamar_operasi_2_debet'] = $kamar_operasi_2_debet;
	$data['kamar_operasi_2_kredit'] = $kamar_operasi_2_kredit;
	$data['kamar_operasi_3_debet'] = $kamar_operasi_3_debet;
	$data['kamar_operasi_3_kredit'] = $kamar_operasi_3_kredit;	

	echo json_encode($data);
?>