<?php
	require_once("smis-base/smis-include-service-consumer.php");
	global $db;
	
	$non_dokter_items = array(
		array(
			"kode_akun"	=> "551.01.01",
			"nama_akun"	=> "JASA DOKTER RPA",
			"slug"		=> "rpa_argopuro;rpa_bromo;rpa_bromobaru;rpa_kelud",
			"label"		=> "rpa",
			"filter" 	=> ""
		),
		array(
			"kode_akun"	=> "551.02.01",
			"nama_akun"	=> "JASA DOKTER RPB",
			"slug"		=> "rpb_argopuro;rpb_kelud;rpb_ijen;rpb_raung",
			"label"		=> "rpb",
			"filter"	=> ""
		),
		array(
			"kode_akun"	=> "551.03.01",
			"nama_akun"	=> "JASA DOKTER ICU/RPO",
			"slug"		=> "icu",
			"label"		=> "icu",
			"filter"	=> ""
		),
		array(
			"kode_akun"	=> "551.04.01",
			"nama_akun"	=> "JASA DOKTER RPC (Lt-1)",
			"slug"		=> "rollaas_diamond;rollaas_emerald",
			"label"		=> "rpc_lt1",
			"filter"	=> ""
		),
		array(
			"kode_akun"	=> "551.05.01",
			"nama_akun"	=> "JASA DOKTER RPD (Lt-2)",
			"slug"		=> "rollaas_ruby",
			"label"		=> "rpd_lt2",
			"filter"	=> ""
		),
		array(
			"kode_akun"	=> "551.06.01",
			"nama_akun"	=> "JASA DOKTER RPD (Lt-3)",
			"slug"		=> "rollaas_saphire",
			"label"		=> "rpd_lt3",
			"filter"	=> ""
		),
		array(
			"kode_akun"	=> "552.01.01",
			"nama_akun"	=> "JASA DOKTER IGD",
			"slug"		=> "instalasi_gawat_darurat",
			"label"		=> "instalasi_gawat_darurat",
			"filter"	=> ""
		),
		array(
			"kode_akun"	=> "552.02.01",
			"nama_akun"	=> "JASA DOKTER POLI UMUM",
			"slug"		=> "poliumum",
			"label"		=> "poliumum",
			"filter"	=> ""
		),
		array(
			"kode_akun"	=> "552.03.01",
			"nama_akun"	=> "JASA DOKTER POLI GIGI",
			"slug"		=> "poligigi",
			"label"		=> "poligigi",
			"filter"	=> ""
		),
		array(
			"kode_akun"	=> "552.04.01",
			"nama_akun"	=> "JASA DOKTER POLI ANAK",
			"slug"		=> "poli_spesialis_center",
			"label"		=> "poli_anak",
			"filter"	=> "poli_anak"
		),
		array(
			"kode_akun"	=> "552.05.01",
			"nama_akun"	=> "JASA DOKTER POLI FISIOTERAPI",
			"slug"		=> "fisiotherapy",
			"label"		=> "fisiotherapy",
			"filter"	=> "fisiotherapy"
		),
		array(
			"kode_akun"	=> "552.06.01",
			"nama_akun"	=> "JASA DOKTER POLI OBGYN",
			"slug"		=> "poli_spesialis_center",
			"label"		=> "poli_obgyn",
			"filter"	=> "poli_obgyn"
		),
		array(
			"kode_akun"	=> "552.07.01",
			"nama_akun"	=> "JASA DOKTER POLI BEDAH",
			"slug"		=> "poli_spesialis_center",
			"label"		=> "poli_bedah",
			"filter"	=> "poli_bedah"
		),
		array(
			"kode_akun"	=> "552.08.01",
			"nama_akun"	=> "JASA DOKTER POLI MATA",
			"slug"		=> "poli_spesialis_center",
			"label"		=> "poli_mata",
			"filter"	=> "poli_mata"
		),
		array(
			"kode_akun"	=> "552.09.01",
			"nama_akun"	=> "JASA DOKTER POLI THT",
			"slug"		=> "poli_spesialis_center",
			"label"		=> "poli_tht",
			"filter"	=> "poli_tht"
		),
		array(
			"kode_akun"	=> "552.10.01",
			"nama_akun"	=> "JASA DOKTER POLI KULIT DAN KELAMIN",
			"slug"		=> "poli_spesialis_center",
			"label"		=> "poli_kulit_dan_kelamin",
			"filter"	=> "poli_kulit_dan_kelamin"
		),
		array(
			"kode_akun"	=> "552.11.01",
			"nama_akun"	=> "JASA DOKTER POLI SYARAF",
			"slug"		=> "poli_spesialis_center",
			"label"		=> "poli_syaraf",
			"filter"	=> "poli_syaraf"
		),
		array(
			"kode_akun"	=> "552.12.01",
			"nama_akun"	=> "JASA DOKTER POLI ORTOPEDI",
			"slug"		=> "poli_spesialis_center",
			"label"		=> "poli_ortopedi",
			"filter"	=> "poli_ortopedi"
		),
		array(
			"kode_akun"	=> "552.13.01",
			"nama_akun"	=> "JASA DOKTER POLI JANTUNG",
			"slug"		=> "poli_spesialis_center",
			"label"		=> "poli_jantung",
			"filter"	=> "poli_jantung"
		),
		array(
			"kode_akun"	=> "552.14.01",
			"nama_akun"	=> "JASA DOKTER POLI PARU",
			"slug"		=> "poli_spesialis_center",
			"label"		=> "poli_paru",
			"filter"	=> "poli_paru"
		),
		array(
			"kode_akun"	=> "552.15.01",
			"nama_akun"	=> "JASA DOKTER POLI GIZI",
			"slug"		=> "gizi",
			"label"		=> "gizi",
			"filter"	=> "gizi"
		),
		array(
			"kode_akun"	=> "502.16.01",
			"nama_akun"	=> "JASA DOKTER POLI PENYAKIT DALAM",
			"slug"		=> "poli_spesialis_center",
			"label"		=> "poli_penyakit_dalam",
			"filter"	=> "poli_penyakit_dalam"
		),
		array(
			"kode_akun"	=> "552.17.01",
			"nama_akun"	=> "JASA DOKTER POLI UROLOGI",
			"slug"		=> "poli_spesialis_center",
			"label"		=> "poli_urologi",
			"filter"	=> "poli_urologi"
		),
		array(
			"kode_akun"	=> "552.18.01",
			"nama_akun"	=> "JASA DOKTER HEMODIALISA",
			"slug"		=> "hemodialisa",
			"label"		=> "hemodialisa",
			"filter"	=> "hemodialisa"
		),
		array(
			"kode_akun"	=> "553.01.",
			"nama_akun"	=> "JASA DOKTER / OPERATOR KAMAR OPERASI",
			"slug"		=> "kamar_operasi",
			"label"		=> "kamar_operasi",
			"filter"	=> "kamar_operasi"
		),
		array(
			"kode_akun"	=> "554.01.",
			"nama_akun"	=> "JASA DOKTER RPKK",
			"slug"		=> "rpkk_argopuro;rpkk_kelud;rpkk_raung;rpkk_semeru;rpkk_wilis;ruang_bersalin;ruang_pathologis",
			"label"		=> "rpkk",
			"filter"	=> "rpkk"
		),
		array(
			"kode_akun"	=> "",
			"nama_akun"	=> "JASA DOKTER RADIOLOGI",
			"slug"		=> "radiology",
			"label"		=> "radiology",
			"filter"	=> "radiology"
		)
	);
	$html = "";
	$num = 1;
	foreach ($non_dokter_items as $ndi) {
		$html .= "
			<tr id='data_" . $ndi['label'] . "' class='data_unit'>
				<td id='data_" . $ndi['label'] . "_nomor'><small>" . $num . "<small></td>
				<td id='data_" . $ndi['label'] . "_kode_akun'><small>" . $ndi['kode_akun'] . "</small></td>
				<td id='data_" . $ndi['label'] . "_nama_akun'><small>" . $ndi['nama_akun'] . "</small></td>
				<td id='data_" . $ndi['label'] . "_debet'><small>" . ArrayAdapter::format("money", "0") . "</small></td>
				<td id='data_" . $ndi['label'] . "_kredit'><small>" . ArrayAdapter::format("money", "0") . "</small></td>
				<td id='data_" . $ndi['label'] . "_slug' style='display:none;'>" . $ndi['slug'] . "</small></td>
				<td id='data_" . $ndi['label'] . "_filter' style='display:none;'>" . $ndi['filter'] . "</td>
			</tr>
		";
		$num++;
	}
	$html .= "
		<tr id='data_total_sewa_alat' class='data_total_sewa_alat'>
			<td id='data_total_sewa_alat_nomor'><small>" . $num . "<small></td>
			<td id='data_total_sewa_alat_kode_akun'><small></small></td>
			<td id='data_total_sewa_alat_nama_akun'><small>SEWA ALAT DOKTER</small></td>
			<td id='data_total_sewa_alat_debet'><small>" . ArrayAdapter::format("money", "0") . "</small></td>
			<td id='data_total_sewa_alat_kredit'><small>" . ArrayAdapter::format("money", "0") . "</small></td>
			<td id='data_total_sewa_alat_filter' style='display:none;'>SEWA ALAT DR%</td>
		</tr>
	";
	$num++;
	$consumer_service = new ServiceConsumer(
		$db,
		"get_dokter_jurnal",
		null,
		"manajemen"
	);
	$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
	$content = $consumer_service->execute()->getContent();
	foreach ($content as $c) {
		$html .= "
			<tr id='data_" . $c['id_dokter'] . "' class='data_dokter'>
				<td id='data_" . $c['id_dokter'] . "_nomor'><small>" . $num . "<small></td>
				<td id='data_" . $c['id_dokter'] . "_kode_akun'><small>" . $c['kode_dokter'] . "</small></td>
				<td id='data_" . $c['id_dokter'] . "_nama_akun'><small>TITIPAN JASA MEDIS " . strtoupper($c['nama_dokter']) . "</small></td>
				<td id='data_" . $c['id_dokter'] . "_debet'><small>" . ArrayAdapter::format("money", "0") . "</small></td>
				<td id='data_" . $c['id_dokter'] . "_kredit'><small>" . ArrayAdapter::format("money", "0") . "</small></td>
				<td id='data_" . $c['id_dokter'] . "_id_dokter' style='display:none;'>" . $c['id_dokter'] . "</td>
			</tr>
		";
		$num++;
	}
	$sewa_alat_dokter_rows = $db->get_result("
		SELECT id, nama 
		FROM smis_ksr_tagihan_tambahan
		WHERE prop NOT LIKE 'del' AND nama LIKE 'SEWA ALAT DR%'
	");
	if ($sewa_alat_dokter_rows != null) {
		foreach ($sewa_alat_dokter_rows as $sadr) {
			$html .= "
				<tr id='data_sewa_alat_" . $sadr->id . "' class='data_sewa_alat'>
					<td id='data_sewa_alat_" . $sadr->id . "_nomor'><small>" . $num . "<small></td>
					<td id='data_sewa_alat_" . $sadr->id . "_kode_akun'></td>
					<td id='data_sewa_alat_" . $sadr->id . "_nama_akun'><small>" . strtoupper($sadr->nama) . "</small></td>
					<td id='data_sewa_alat_" . $sadr->id . "_debet'><small>" . ArrayAdapter::format("money", "0") . "</small></td>
					<td id='data_sewa_alat_" . $sadr->id . "_kredit'><small>" . ArrayAdapter::format("money", "0") . "</small></td>
					<td id='data_sewa_alat_" . $sadr->id . "_filter' style='display:none;'>" . $sadr->nama . "</small></td>
				</tr>
			";
			$num++;
		}
	}
	$data = array();
	$data['html'] = $html;
	echo json_encode($data);
?>