<?php
	require_once("smis-base/smis-include-service-consumer.php");
	
	$from = $_POST['from'];
	$to = $_POST['to'];
	$num = $_POST['num'];
	$uri = "%%";
	$jenis_pasien = $_POST['jenis_pasien'];
	
	/// Mendapatkan informasi Pasien (terutama No. Registrasi) berdasarkan asal jurnal pasien dengan urutan: 
	/// 1) Pasien Rawat Inap
	/// 2) Pasien Poli Spesialis Center
	/// 3) Pasien Poli Terpadu
	/// Service Provider Unit: Pendaftaran (registration)
	$params = array();
	$params['from'] = $from;
	$params['to'] = $to;
	$params['jenis_pasien'] = $jenis_pasien;
	$params['num'] = $num;
	$params['uri'] = $uri;
	$consumer_service = new ServiceConsumer(
		$db,
		"get_info_pasien",
		$params,
		"registration"
	);
	$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
	$content = $consumer_service->execute()->getContent();
	$noreg_pasien = $content[0];
	$nrm_pasien = $content[1];
	$nama_pasien = $content[2];
	$tanggal_daftar_pasien = ArrayAdapter::format("date d/m/Y", $content[3]);
	$jenis_pasien = $content[4];
	$nama_perusahaan = $content[5];
	$nama_asuransi = $content[6];
	$id_perusahaan = $content[7];
	$id_asuransi = $content[8];
	
	//get payment info:
	$params = array();
	$params['noreg_pasien'] = $noreg_pasien;
	$params['jenis_pasien'] = $_POST['jenis_pasien'];
	$params['from'] = $_POST['from'];
	$params['to'] = $_POST['to'];
	$params['operator'] = $_POST['operator'];
	$service = new ServiceConsumer(
		$db,
		"get_pembayaran_pasien",
		$params,
		"kasir"
	);
	$service->setMode(ServiceConsumer::$CLEAN_BOTH);
	$content = $service->execute()->getContent();
	
	$tagihan = $content[7] + $content[8];	
	if ($tagihan == 0)
		$tagihan = ArrayAdapter::format("only-money", "0");
	else
		$tagihan = ArrayAdapter::format("only-money", $tagihan);
	
	$html = "";
	if ($tagihan > 0) {
		$html = "
			<tr id='data_" . $num . "'>
				<td id='data_" . $num . "_num'></td>
				<td id='data_" . $num . "_noreg'><small>" . ArrayAdapter::format("only-digit6", $noreg_pasien) . "</small></td>
				<td id='data_" . $num . "_nrm'><small>" . ArrayAdapter::format("only-digit6", $nrm_pasien) . "</small></td>
				<td id='data_" . $num . "_nama'><small>" . ArrayAdapter::format("unslug", $nama_pasien) . "</small></td>
				<td id='data_" . $num . "_jenis'><small>" . ArrayAdapter::format("unslug", $jenis_pasien) . "</small></td>
				<td id='data_" . $num . "_perusahaan'><small>" . $nama_perusahaan . "</small></td>
				<td id='data_" . $num . "_asuransi'><small>" . $nama_asuransi . "</small></td>
				<td id='data_" . $num . "_piutang'><small><div class='right money-value'>" . $tagihan . "</div></small></td>
			</tr>
		";
	}
	$data = array();
	$data['html'] = $html;
	$data['noreg_pasien'] = ArrayAdapter::format("only-digit6", $noreg_pasien);
	$data['nrm_pasien'] = ArrayAdapter::format("only-digit6", $nrm_pasien);
	$data['nama_pasien'] = ArrayAdapter::format("unslug", $nama_pasien);
	$data['content'] = $content;
	echo json_encode($data);
?>