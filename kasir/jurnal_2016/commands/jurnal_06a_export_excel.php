<?php
	require_once("smis-libs-out/php-excel/PHPExcel.php");
	
	$objPHPExcel = PHPExcel_IOFactory::load("kasir/jurnal_2016/templates/template_jurnal_06a.xlsx");
	
	$objPHPExcel->setActiveSheetIndexByName("Jurnal 06a");
	$objWorksheet = $objPHPExcel->getActiveSheet();
	if ($_POST['num_rows'] - 1 > 0)
		$objWorksheet->insertNewRowBefore(9, $_POST['num_rows'] - 1);
	
	$objWorksheet->setCellValue("L1", "JURNAL 06.a");
	$objWorksheet->setCellValue("L2", "TGL. " . ArrayAdapter::format("date d-m-Y", $_POST['from']) . " s/d " . ArrayAdapter::format("date d-m-Y", $_POST['to']));
	$objWorksheet->setCellValue("L5", $_POST['kode_dokter'] . " / " . strtoupper($_POST['nama_dokter']));
	
	$data = json_decode($_POST['d_data']);
	$start_row_num = 8;
	$end_row_num = 8;
	$row_num = $start_row_num;
	foreach ($data as $d) {
		$col_num = 0;
		$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->no);
		$col_num++;
		$objWorksheet->getCellByColumnAndRow($col_num, $row_num)->setValueExplicit($d->noreg_pasien, PHPExcel_Cell_DataType::TYPE_STRING);
		$col_num++;
		$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->nama_pasien);
		$col_num++;
		$objWorksheet->getCellByColumnAndRow($col_num, $row_num)->setValueExplicit($d->nrm_pasien, PHPExcel_Cell_DataType::TYPE_STRING);
		$col_num++;
		$objWorksheet->getCellByColumnAndRow($col_num, $row_num)->setValueExplicit($d->tanggal_layanan, PHPExcel_Cell_DataType::TYPE_STRING);
		$col_num++;
		$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->bruto);
		$col_num++;
		$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->jasa_rs);
		$col_num++;
		$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->jasa_dokter_bruto);
		$col_num++;
		$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->dpp);
		$col_num++;
		$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->pph_21);
		$col_num++;
		$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->jasa_dokter_netto);
		$col_num++;
		$objWorksheet->getCellByColumnAndRow($col_num, $row_num)->setValueExplicit($d->no_jurnal, PHPExcel_Cell_DataType::TYPE_STRING);
		$row_num++;
		$end_row_num++;
	}
	
	$objDrawing = new PHPExcel_Worksheet_Drawing();
	$objDrawing->setPath("kasir/jurnal_2016/resources/logo_1_5.png");
	$objDrawing->setCoordinates("A1");
	$objDrawing->setOffsetX(0);
	$objDrawing->setWorksheet($objWorksheet);
	
	header("Content-type: application/vnd.ms-excel");	
	header("Content-Disposition: attachment; filename=" . ArrayAdapter::format("slug", "JURNAL_06a") . "_" . $_POST['from'] . "_" . $_POST['to'] . "_" . date("Ymd_His") . ".xlsx");
	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");
	$objWriter->save("php://output");
?>