<?php
	require_once 'smis-base/smis-include-service-consumer.php';
	global $db;
	
	$from = $_POST['from'];
	$to = $_POST['to'];
	$num = $_POST['num'];
	$jenis_pasien = "%%";
	$uri = "%%";
	
	/// Mendapatkan informasi Pasien
	/// Service Provider Unit: Pendaftaran (registration)
	$params = array();
	$params['from'] = $from;
	$params['to'] = $to;
	$params['jenis_pasien'] = $jenis_pasien;
	$params['num'] = $num;
	$params['uri'] = $uri;
	$params['grup_transaksi'] = $grup_transaksi;
	$consumer_service = new ServiceConsumer(
		$db,
		"get_info_pasien",
		$params,
		"registration"
	);
	$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
	$content = $consumer_service->execute()->getContent();
	$noreg_pasien = $content[0];
	$nrm_pasien = $content[1];
	$nama_pasien = $content[2];
	$tanggal_daftar_pasien = ArrayAdapter::format("date d/m/Y", $content[3]);
	$jenis_pasien = $content[4];
	$nama_perusahaan = $content[5];
	$nama_asuransi = $content[6];
	$id_perusahaan = $content[7];
	$id_asuransi = $content[8];
	
	$update_data = array();
	/// Jasa Medis Per Unit:
	$unit_data = json_decode($_POST['unit_data']);
	foreach ($unit_data as $ud) {
		$id_dokter_csv = "";
		if ($ud->filter != "") {
			$params = array();
			$params['filter_kp'] = array(
				"poli_spesialis" => $ud->filter
			);
			$consumer_service = new ServiceConsumer(
				$db,
				"get_dokter_jurnal",
				$params,
				"manajemen"
			);
			$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
			$content = $consumer_service->execute()->getContent();
			$id_dokter_csv = "";
			if ($content != null) {
				foreach ($content as $c)
					$id_dokter_csv .= $c['id_dokter'] . ",";
			}
			$id_dokter_csv = rtrim($id_dokter_csv, ",");
		}
		$params = array();
		$params['noreg_pasien'] = $noreg_pasien;
		if ($id_dokter_csv != "")
			$params['id_dokter_csv'] = $id_dokter_csv;
		$unit_slugs = explode(";", $ud->slug);
		$jasa_medis = 0;
		foreach ($unit_slugs as $us) {
			$consumer_service = new ServiceConsumer(
				$db,
				"get_jasa_dokter_jurnal",
				$params,
				$us
			);
			$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
			$content = $consumer_service->execute()->getContent();
			if ($content != null)
				$jasa_medis += $content[0]['jasa_dokter'];
		}
		$d_update_data = array(
			"element_id"	=> $ud->element_id,
			"debet"			=> $jasa_medis,
			"kredit"		=> 0
		);
		$update_data[] = $d_update_data;
	}
	/// Jasa Medis Per Dokter:
	$dokter_data = json_decode($_POST['dokter_data']);
	foreach ($dokter_data as $dd) {
		$params = array();
		$params['noreg_pasien'] = $noreg_pasien;
		$params['id_dokter_csv'] = $dd->id_dokter;
		$consumer_service = new ServiceConsumer(
			$db,
			"get_jasa_dokter_jurnal",
			$params
		);
		$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
		$content = $consumer_service->execute()->getContent();
		$jasa_medis = 0;
		if ($content != null) {
			foreach ($content as $c)
				$jasa_medis += $c['jasa_dokter'];
		}
		$d_update_data = array(
			"element_id"	=> $dd->element_id,
			"debet"			=> 0,
			"kredit"		=> $jasa_medis
		);
		$update_data[] = $d_update_data;
	}

	// Sewa Alat Dokter
	$sewa_alat_dokter_data = json_decode($_POST['sewa_alat_dokter_data']);
	foreach ($sewa_alat_dokter_data as $sadd) {
		$params = array();
		$params['noreg_pasien'] = $noreg_pasien;
		$params['criteria'] = array(
			"nama_tagihan"	=> $sadd->filter
		);
		$params['criteria_conjunction'] = "AND";
		$consumer_service = new ServiceConsumer(
			$db,
			"get_tagihan_tambahan_jurnal",
			$params,
			"kasir"
		);
		$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
		$content = $consumer_service->execute()->getContent();
		$sewa_alat_dokter = 0;
		if ($content != null) {
			$sewa_alat_dokter += $content[0]['nominal'];
		}
		$debet = 0;
		$kredit = 0;
		if ($sadd->debet_kredit == "debet")
			$debet = $sewa_alat_dokter;
		else
			$kredit = $sewa_alat_dokter;
		$d_update_data = array(
			"element_id"	=> $sadd->element_id,
			"debet"			=> $debet,
			"kredit"		=> $kredit
		);
		$update_data[] = $d_update_data;
	}
	
	$data = array();
	$data['noreg_pasien'] = ArrayAdapter::format("only-digit6", $noreg_pasien);
	$data['nrm_pasien'] = ArrayAdapter::format("only-digit6", $nrm_pasien);
	$data['nama_pasien'] = ArrayAdapter::format("unslug", $nama_pasien);
	$data['tanggal_daftar_pasien'] = $tanggal_daftar_pasien;
	$data['jenis_pasien'] = ArrayAdapter::format("unslug", $jenis_pasien);
	$data['nama_perusahaan'] = $nama_perusahaan;
	$data['nama_asuransi'] = $nama_asuransi;
	$data['update_data'] = $update_data;
	
	echo json_encode($data);
?>