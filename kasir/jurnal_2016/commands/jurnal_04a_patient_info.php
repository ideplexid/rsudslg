<?php
	require_once("smis-base/smis-include-service-consumer.php");
	
	$from = $_POST['from'];
	$to = $_POST['to'];
	$limit = $_POST['limit'];
	$num = $_POST['num'];
	$jenis_pasien = $_POST['jenis_pasien'];
	$uri = 1;
	
	/// Mendapatkan informasi Pasien
	$params = array();
	$params['from'] = $from;
	$params['to'] = $to;
	$params['jenis_pasien'] = $jenis_pasien;
	$params['num'] = $num;
	$params['uri'] = $uri;
	$consumer_service = new ServiceConsumer(
		$db,
		"get_info_pasien",
		$params,
		"registration"
	);
	$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
	$content = $consumer_service->execute()->getContent();
	$noreg_pasien = $content[0];
	$nrm_pasien = $content[1];
	$nama_pasien = $content[2];
	$tanggal_daftar_pasien = ArrayAdapter::format("date d/m/Y", $content[3]);
	$jenis_pasien = $content[4];
	$nama_perusahaan = $content[5];
	$nama_asuransi = $content[6];
	$id_perusahaan = $content[7];
	$id_asuransi = $content[8];
	
	//get payment info:
	$params = array();
	$params['noreg_pasien'] = $noreg_pasien;
	$params['jenis_pasien'] = $jenis_pasien;
	$params['from'] = $_POST['from'];
	$params['to'] = $_POST['to'];
	$params['operator'] = $_POST['operator'];
	$service = new ServiceConsumer(
		$db,
		"get_pembayaran_pasien",
		$params,
		"kasir"
	);
	$service->setMode(ServiceConsumer::$CLEAN_BOTH);
	$content = $service->execute()->getContent();
	$no_bukti_bank = "";
	if ($content[10] != "")
		$no_bukti_bank = $content[10];
	for ($i = 11; $i <= 15; $i++)
		if ($content[$i] != "") {
			if ($no_bukti_bank != "")
				$no_bukti_bank .= ", ";
			$no_bukti_bank .= $content[$i];
		}

	//get prescription bill:
	$params = array();
	$params['noreg_pasien'] = $noreg_pasien;
	$consumer_service = new ServiceConsumer(
		$db,
		"get_tagihan",
		$params,
		"depo_farmasi_irja"
	);
	$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
	$content = $consumer_service->execute()->getContent();
	$tagihan_obat = 0;
	if ($content != null) {
		foreach ($content[5]['penjualan_resep']['result'] as $tagihan_resep)
			$tagihan_obat += $tagihan_resep['biaya'];
	}
	$consumer_service = new ServiceConsumer(
		$db,
		"get_tagihan",
		$params,
		"depo_farmasi_irna"
	);
	$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
	$content = $consumer_service->execute()->getContent();
	$tagihan_obat = 0;
	if ($content != null) {
		foreach ($content[5]['penjualan_resep']['result'] as $tagihan_resep)
			$tagihan_obat += $tagihan_resep['biaya'];
	}
	$f_tagihan_obat = ArrayAdapter::format("only-money", $tagihan_obat);
	if ($tagihan_obat == 0)
		$f_tagihan_obat = "0,00";
	$html = "";
	if ($no_bukti_bank != "") {
		$html = "
			<tr id='data_" . $num . "'>
				<td id='data_" . $num . "_num'></td>
				<td id='data_" . $num . "_noreg'><small>" . ArrayAdapter::format("only-digit6", $noreg_pasien) . "</small></td>
				<td id='data_" . $num . "_nrm'><small>" . ArrayAdapter::format("only-digit6", $nrm_pasien) . "</small></td>
				<td id='data_" . $num . "_nama'><small>" . ArrayAdapter::format("unslug", $nama_pasien) . "</small></td>
				<td id='data_" . $num . "_tanggal_setoran'><small>" . date("d-m-Y") . "</small></td>
				<td id='data_" . $num . "_nomor_bukti_setoran'><small>" . $no_bukti_bank . "</small></td>
				<td id='data_" . $num . "_status'><small>PASIEN " . ArrayAdapter::format("unslug", $jenis_pasien) . "</small></td>
				<td id='data_" . $num . "_tagihan_obat'><small><div class='right money-value'>" . $f_tagihan_obat . "</div></small></td>
			</tr>
		";
	}
	$data = array();
	$data['html'] = $html;
	$data['noreg_pasien'] = ArrayAdapter::format("only-digit6", $noreg_pasien);
	$data['nrm_pasien'] = ArrayAdapter::format("only-digit6", $nrm_pasien);
	$data['nama_pasien'] = ArrayAdapter::format("unslug", $nama_pasien);
	echo json_encode($data);
?>