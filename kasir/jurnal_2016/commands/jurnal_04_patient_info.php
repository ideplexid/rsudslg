<?php
	require_once("smis-base/smis-include-service-consumer.php");
	
	$from = $_POST['from'];
	$to = $_POST['to'];
	$num = $_POST['num'];
	$uri = $_POST['uri'];
	$jenis_pasien = $_POST['jenis_pasien'];
	
	/// Mendapatkan informasi Pasien
	$params = array();
	$params['from'] = $from;
	$params['to'] = $to;
	$params['jenis_pasien'] = $jenis_pasien;
	$params['num'] = $num;
	$params['uri'] = $uri;
	$consumer_service = new ServiceConsumer(
		$db,
		"get_info_pasien",
		$params,
		"registration"
	);
	$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
	$content = $consumer_service->execute()->getContent();
	$noreg_pasien = $content[0];
	$nrm_pasien = $content[1];
	$nama_pasien = $content[2];
	$tanggal_daftar_pasien = ArrayAdapter::format("date d/m/Y", $content[3]);
	$jenis_pasien = $content[4];
	$nama_perusahaan = $content[5];
	$nama_asuransi = $content[6];
	$id_perusahaan = $content[7];
	$id_asuransi = $content[8];
	
	//get payment info:
	$params = array();
	$params['noreg_pasien'] = $noreg_pasien;
	$params['jenis_pasien'] = $jenis_pasien;
	$params['from'] = $_POST['from'];
	$params['to'] = $_POST['to'];
	$params['operator'] = $_POST['operator'];
	$service = new ServiceConsumer(
		$db,
		"get_pembayaran_pasien",
		$params,
		"kasir"
	);
	$service->setMode(ServiceConsumer::$CLEAN_BOTH);
	$content = $service->execute()->getContent();

	$total = 0;	
	$tunai = $content[0];
	$total = $total + $tunai;
	$mandiri_tunai = $content[1];
	$total = $total + $mandiri_tunai;
	$mandiri_edc = $content[2];
	$total = $total + $mandiri_edc;
	$bri_tunai = $content[3];
	$total = $total + $bri_tunai;
	$bri_edc = $content[4];
	$total = $total + $bri_edc;
	$bca_tunai = $content[5];
	$total = $total + $bca_tunai;
	$bca_edc = $content[6];
	$total = $total + $bca_edc;
	$tagihan_non_ptpn_12 = $content[7];
	$total = $total + $tagihan_non_ptpn_12;
	$tagihan_ptpn_12 = $content[8];
	$total = $total + $tagihan_ptpn_12;
	$diskon = $content[9];
	$total = $total + $diskon;

	if ($diskon == 0)
		$diskon = ArrayAdapter::format("only-money", "0");
	else
		$diskon = ArrayAdapter::format("only-money", $diskon);
	if ($tunai == 0)
		$tunai = ArrayAdapter::format("only-money", "0");
	else
		$tunai = ArrayAdapter::format("only-money", $tunai);
	if ($bri_tunai == 0)
		$bri_tunai = ArrayAdapter::format("only-money", "0");
	else
		$bri_tunai = ArrayAdapter::format("only-money", $bri_tunai);
	if ($bri_edc == 0)
		$bri_edc = ArrayAdapter::format("only-money", "0");
	else
		$bri_edc = ArrayAdapter::format("only-money", $bri_edc);
	if ($mandiri_tunai == 0)
		$mandiri_tunai = ArrayAdapter::format("only-money", "0");
	else
		$mandiri_tunai = ArrayAdapter::format("only-money", $mandiri_tunai);
	if ($mandiri_edc == 0)
		$mandiri_edc = ArrayAdapter::format("only-money", "0");
	else
		$mandiri_edc = ArrayAdapter::format("only-money", $mandiri_edc);

	if ($bca_tunai == 0)
		$bca_tunai = ArrayAdapter::format("only-money", "0");
	else
		$bca_tunai = ArrayAdapter::format("only-money", $bca_tunai);
	if ($bca_edc == 0)
		$bca_edc = ArrayAdapter::format("only-money", "0");
	else
		$bca_edc = ArrayAdapter::format("only-money", $bca_edc);
	
	if ($tagihan_non_ptpn_12 == 0)
		$tagihan_non_ptpn_12 = ArrayAdapter::format("only-money", "0");
	else
		$tagihan_non_ptpn_12 = ArrayAdapter::format("only-money", $tagihan_non_ptpn_12);
	if ($tagihan_ptpn_12 == 0)
		$tagihan_ptpn_12 = ArrayAdapter::format("only-money", "0");
	else
		$tagihan_ptpn_12 = ArrayAdapter::format("only-money", $tagihan_ptpn_12);
	if ($total == 0)
		$total = ArrayAdapter::format("only-money", "0");
	else
		$total = ArrayAdapter::format("only-money", $total);
	
	$html = "";
	if ($total > 0) {
		$html = "
			<tr id='data_" . $num . "'>
				<td id='data_" . $num . "_num'></td>
				<td id='data_" . $num . "_noreg'><small>" . ArrayAdapter::format("only-digit6", $noreg_pasien) . "</small></td>
				<td id='data_" . $num . "_nrm'><small>" . ArrayAdapter::format("only-digit6", $nrm_pasien) . "</small></td>
				<td id='data_" . $num . "_nama'><small>" . ArrayAdapter::format("unslug", $nama_pasien) . "</small></td>
				<td id='data_" . $num . "_jenis'><small>" . ArrayAdapter::format("unslug", $jenis_pasien) . "</small></td>
				<td id='data_" . $num . "_perusahaan'><small>" . $nama_perusahaan . "</small></td>
				<td id='data_" . $num . "_asuransi'><small>" . $nama_asuransi . "</small></td>
				<td id='data_" . $num . "_tunai'><small><div class='right money-value'>" . $tunai . "</div></small></td>
				<td id='data_" . $num . "_mandiri_tunai'><small><div class='right money-value'>" . $mandiri_tunai . "</div></small></td>
				<td id='data_" . $num . "_mandiri_edc'><small><div class='right money-value'>" . $mandiri_edc . "</div></small></td>
				<td id='data_" . $num . "_bri_tunai'><small><div class='right money-value'>" . $bri_tunai . "</div></small></td>
				<td id='data_" . $num . "_bri_edc'><small><div class='right money-value'>" . $bri_edc . "</div></small></td>
				<td id='data_" . $num . "_bca_tunai'><small><div class='right money-value'>" . $bca_tunai . "</div></small></td>
				<td id='data_" . $num . "_bca_edc'><small><div class='right money-value'>" . $bca_edc . "</div></small></td>
				<td id='data_" . $num . "_tagihan_non_ptpn_12'><small><div class='right money-value'>" . $tagihan_non_ptpn_12 . "</div></small></td>
				<td id='data_" . $num . "_tagihan_ptpn_12'><small><div class='right money-value'>" . $tagihan_ptpn_12 . "</div></small></td>
				<td id='data_" . $num . "_diskon' style='display: none;'><small>" . $diskon . "</small></td>
				<td id='data_" . $num . "_total'><small><div class='right money-value'>" . $total . "</div></small></td>
			</tr>
		";
	}
	$data = array();
	$data['html'] = $html;
	$data['noreg_pasien'] = ArrayAdapter::format("only-digit6", $noreg_pasien);
	$data['nrm_pasien'] = ArrayAdapter::format("only-digit6", $nrm_pasien);
	$data['nama_pasien'] = ArrayAdapter::format("unslug", $nama_pasien);
	echo json_encode($data);
?>