<?php
	$params = array();
	$params['from'] = $_POST['from'];
	$params['to'] = $_POST['to'];
	$params['jenis_pasien'] = "%%";
	$params['uri'] = "%%";
	$service = new ServiceConsumer(
		$db,
		"get_jumlah_pasien",
		$params,
		"registration"
	);
	$service->setMode(ServiceConsumer::$CLEAN_BOTH);
	$content = $service->execute()->getContent();
	$jumlah = $content[0];
	
	$data = array();
	$data['jumlah'] = $jumlah;
	$data['timestamp'] = date("d-m-Y H:i:s");
	echo json_encode($data);
?>