<?php
	require_once("smis-libs-out/php-excel/PHPExcel.php");
	
	$objPHPExcel = PHPExcel_IOFactory::load("kasir/jurnal_2016/templates/template_jurnal_04.xlsx");
	
	$objPHPExcel->setActiveSheetIndexByName("Jurnal 04");
	$objWorksheet = $objPHPExcel->getActiveSheet();
	if ($_POST['num_rows'] - 1 > 0)
		$objWorksheet->insertNewRowBefore(10, $_POST['num_rows'] - 1);
	
	$kode_dokumen = "JURNAL 04";
	$objWorksheet->setCellValue("Q1", $kode_dokumen);
	setlocale(LC_ALL, 'IND');
	$objWorksheet->setCellValue("Q4", "TGL. " . ArrayAdapter::format("date d-m-Y H:i", $_POST['from']) . " s/d " . ArrayAdapter::format("date d-m-Y H:i", $_POST['to']));
	
	$data = json_decode($_POST['d_data']);
	$no = 1;
	$start_row_num = 9;
	$end_row_num = 11;
	$row_num = $start_row_num;
	foreach ($data as $d) {
		$col_num = 0;
		$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $no);
		$col_num++;
		$objWorksheet->getCellByColumnAndRow($col_num, $row_num)->setValueExplicit(ArrayAdapter::format("only-digit6", $d->noreg_pasien), PHPExcel_Cell_DataType::TYPE_STRING);
		$col_num++;
		$objWorksheet->getCellByColumnAndRow($col_num, $row_num)->setValueExplicit(ArrayAdapter::format("only-digit6", $d->nrm_pasien), PHPExcel_Cell_DataType::TYPE_STRING);
		$col_num++;
		$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->nama_pasien);
		$col_num++;
		$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->jenis_pasien);
		$col_num++;
		$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->perusahaan);
		$col_num++;
		$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->asuransi);
		$col_num++;
		$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->tunai);
		$col_num++;
		$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->mandiri_tunai);
		$col_num++;
		$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->mandiri_edc);
		$col_num++;
		$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->bri_tunai);
		$col_num++;
		$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->bri_edc);
		$col_num++;
		$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->bca_tunai);
		$col_num++;
		$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->bca_edc);
		$col_num++;
		$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->tagihan_non_ptpn_12);
		$col_num++;
		$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->tagihan_ptpn_12);
		$col_num++;
		$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, "=SUM(H" . $row_num . ":P" . $row_num . ")");
		$no++;
		$row_num++;
		$end_row_num++;
	}
	
	$objDrawing = new PHPExcel_Worksheet_Drawing();
	$objDrawing->setPath("kasir/jurnal_2016/resources/logo_1_5.png");
	$objDrawing->setCoordinates("A1");
	$objDrawing->setOffsetX(0);
	$objDrawing->setWorksheet($objWorksheet);
	
	$row_num = $end_row_num + 5;
	global $user;
	$objWorksheet->setCellValue("O" . $row_num, ArrayAdapter::format("unslug", $user->getNameOnly()));
		
	header("Content-type: application/vnd.ms-excel");	
	header("Content-Disposition: attachment; filename=" . ArrayAdapter::format("slug", $kode_dokumen) . "_" . ArrayAdapter::format("slug", $_POST['operator']) . "_" . ArrayAdapter::format("date Ymd_Hi", $_POST['from']) . "_" . ArrayAdapter::format("date Ymd_Hi", $_POST['to']) . "_" . date("Ymd_His") . ".xlsx");
	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");
	$objWriter->save("php://output");
?>