<?php
	$jurnal_items = array(
		"rpa"	=> 	array(
			array(
				"no"		=> "A",
				"kode"		=> "551.01.",
				"nama"		=> "Ruang Perawatan A",
				"debet"		=> "",
				"kredit"	=> "",
				"cmd"		=> "",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>1</div>",
				"kode"		=> "",
				"nama"		=> "Tindakan Perawat Ruangan",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "total_pendapatan_keperawatan",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>2</div>",
				"kode"		=> "551.01.02",
				"nama"		=> "Pendapatan Tind. Perawat",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "pendapatan_keperawatan_rs",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>3</div>",
				"kode"		=> "159.11.600",
				"nama"		=> "Jasa Keperawatan",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "jaspel_keperawatan",
				"filter"	=> ""
			)
		),
		"rpb"	=> 	array(
			array(
				"no"		=> "B",
				"kode"		=> "551.02.",
				"nama"		=> "Ruang Perawatan B",
				"debet"		=> "",
				"kredit"	=> "",
				"cmd"		=> "",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>1</div>",
				"kode"		=> "",
				"nama"		=> "Tindakan Perawat Ruangan",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "total_pendapatan_keperawatan",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>2</div>",
				"kode"		=> "551.02.02",
				"nama"		=> "Pendapatan Tind. Perawat",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "pendapatan_keperawatan_rs",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>3</div>",
				"kode"		=> "159.11.601",
				"nama"		=> "Jasa Keperawatan",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "jaspel_keperawatan",
				"filter"	=> ""
			)
		),
		"icu"	=> 	array(
			array(
				"no"		=> "C",
				"kode"		=> "551.03.",
				"nama"		=> "Ruang ICU/RPO",
				"debet"		=> "",
				"kredit"	=> "",
				"cmd"		=> "",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>1</div>",
				"kode"		=> "",
				"nama"		=> "Tindakan Perawat Ruangan",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "total_pendapatan_keperawatan",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>2</div>",
				"kode"		=> "551.03.02",
				"nama"		=> "Pendapatan Tind. Perawat",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "pendapatan_keperawatan_rs",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>3</div>",
				"kode"		=> "159.11.602",
				"nama"		=> "Jasa Keperawatan",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "jaspel_keperawatan",
				"filter"	=> ""
			)
		),
		"rpc"	=> 	array(
			array(
				"no"		=> "D",
				"kode"		=> "551.04.",
				"nama"		=> "Ruang Perawatan C (Lt-1)",
				"debet"		=> "",
				"kredit"	=> "",
				"cmd"		=> "",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>1</div>",
				"kode"		=> "551.04.02",
				"nama"		=> "Tindakan Perawat Ruangan",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "total_pendapatan_keperawatan",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>2</div>",
				"kode"		=> "159.11.603",
				"nama"		=> "Pendapatan Tind. Perawat",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "pendapatan_keperawatan_rs",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>3</div>",
				"kode"		=> "",
				"nama"		=> "Jasa Keperawatan",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "jaspel_keperawatan",
				"filter"	=> ""
			)
		),
		"rpd"	=> 	array(
			array(
				"no"		=> "E",
				"kode"		=> "551.05.",
				"nama"		=> "Ruang Perawatan D (Lt-2)",
				"debet"		=> "",
				"kredit"	=> "",
				"cmd"		=> "",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>1</div>",
				"kode"		=> "",
				"nama"		=> "Tindakan Perawat Ruangan",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "total_pendapatan_keperawatan",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>2</div>",
				"kode"		=> "551.05.02",
				"nama"		=> "Pendapatan Tind. Perawat",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "pendapatan_keperawatan_rs",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>3</div>",
				"kode"		=> "159.11.604",
				"nama"		=> "Jasa Keperawatan",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "jaspel_keperawatan",
				"filter"	=> ""
			)
		),
		"rpe"	=> 	array(
			array(
				"no"		=> "F",
				"kode"		=> "551.06.",
				"nama"		=> "Ruang Perawatan D (Lt-3)",
				"debet"		=> "",
				"kredit"	=> "",
				"cmd"		=> "",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>1</div>",
				"kode"		=> "",
				"nama"		=> "Tindakan Perawat Ruangan",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "total_pendapatan_keperawatan",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>2</div>",
				"kode"		=> "551.06.02",
				"nama"		=> "Pendapatan Tind. Perawat",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "pendapatan_keperawatan_rs",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>3</div>",
				"kode"		=> "159.11.605",
				"nama"		=> "Jasa Keperawatan",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "jaspel_keperawatan",
				"filter"	=> ""
			)
		),
		"instalasi_gawat_darurat"	=> array(
			array(
				"no"		=> "G",
				"kode"		=> "552.01.",
				"nama"		=> "Ruang UGD",
				"debet"		=> "",
				"kredit"	=> "",
				"cmd"		=> "",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>1</div>",
				"kode"		=> "",
				"nama"		=> "Tindakan Perawat Ruangan",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "total_pendapatan_keperawatan",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>2</div>",
				"kode"		=> "552.01.02",
				"nama"		=> "Pendapatan Tind. Perawat",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "pendapatan_keperawatan_rs",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>3</div>",
				"kode"		=> "159.11.606",
				"nama"		=> "Jasa Keperawatan",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "jaspel_keperawatan",
				"filter"	=> ""
			)
		),
		"poliumum"	=> array(
			array(
				"no"		=> "H",
				"kode"		=> "552.02.",
				"nama"		=> "Poli Umum",
				"debet"		=> "",
				"kredit"	=> "",
				"cmd"		=> "",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>1</div>",
				"kode"		=> "",
				"nama"		=> "Tindakan Perawat Ruangan",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "total_pendapatan_keperawatan",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>2</div>",
				"kode"		=> "552.02.02",
				"nama"		=> "Pendapatan Tind. Perawat",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "pendapatan_keperawatan_rs",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>3</div>",
				"kode"		=> "159.11.607",
				"nama"		=> "Jasa Keperawatan",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "jaspel_keperawatan",
				"filter"	=> ""
			)
		),
		"fisiotherapy"	=> array(
			array(
				"no"		=> "I",
				"kode"		=> "552.05",
				"nama"		=> "Poli Fisioterapi",
				"debet"		=> "",
				"kredit"	=> "",
				"cmd"		=> "",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>1</div>",
				"kode"		=> "",
				"nama"		=> "Tindakan Perawat Ruangan",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "total_pendapatan_keperawatan",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>2</div>",
				"kode"		=> "552.05.02",
				"nama"		=> "Pendapatan Tind. Perawat",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "pendapatan_keperawatan_rs",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>3</div>",
				"kode"		=> "159.11.608",
				"nama"		=> "Jasa Keperawatan",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "jaspel_keperawatan",
				"filter"	=> ""
			)
		),
		"poli_obgyn"	=> array(
			array(
				"no"		=> "J",
				"kode"		=> "552.06.",
				"nama"		=> "Poli Obgyn",
				"debet"		=> "",
				"kredit"	=> "",
				"cmd"		=> "",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>1</div>",
				"kode"		=> "",
				"nama"		=> "Tindakan Perawat Ruangan",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "total_pendapatan_keperawatan",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>2</div>",
				"kode"		=> "552.06.02",
				"nama"		=> "Pendapatan Tind. Perawat",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "pendapatan_keperawatan_rs",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>3</div>",
				"kode"		=> "159.11.609",
				"nama"		=> "Jasa Keperawatan",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "jaspel_keperawatan",
				"filter"	=> ""
			)
		),
		"poli_syaraf"	=> array(
			array(
				"no"		=> "K",
				"kode"		=> "552.11",
				"nama"		=> "Poli Syaraf",
				"debet"		=> "",
				"kredit"	=> "",
				"cmd"		=> "",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>1</div>",
				"kode"		=> "",
				"nama"		=> "Tindakan Perawat Ruangan",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "total_pendapatan_keperawatan",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>2</div>",
				"kode"		=> "552.11.02",
				"nama"		=> "Pendapatan Tind. Perawat",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "pendapatan_keperawatan_rs",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>3</div>",
				"kode"		=> "159.11.610",
				"nama"		=> "Jasa Keperawatan",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "jaspel_keperawatan",
				"filter"	=> ""
			)
		),
		"hemodialisa"	=> array(
			array(
				"no"		=> "L",
				"kode"		=> "552.18",
				"nama"		=> "Hemodialisa",
				"debet"		=> "",
				"kredit"	=> "",
				"cmd"		=> "",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>1</div>",
				"kode"		=> "",
				"nama"		=> "Tindakan Perawat Ruangan",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "total_pendapatan_keperawatan",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>2</div>",
				"kode"		=> "552.18.02",
				"nama"		=> "Pendapatan Tind. Perawat",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "pendapatan_keperawatan_rs",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>3</div>",
				"kode"		=> "159.11.611",
				"nama"		=> "Jasa Keperawatan",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "jaspel_keperawatan",
				"filter"	=> ""
			)
		),
		"kamar_operasi"	=> array(
			array(
				"no"		=> "M",
				"kode"		=> "553.",
				"nama"		=> "Kamar Operasi",
				"debet"		=> "",
				"kredit"	=> "",
				"cmd"		=> "",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>1</div>",
				"kode"		=> "",
				"nama"		=> "Tindakan Perawat Ruangan",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "total_pendapatan_keperawatan",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>2</div>",
				"kode"		=> "553.02.",
				"nama"		=> "Pendapatan Tind. Perawat",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "pendapatan_keperawatan_rs",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>3</div>",
				"kode"		=> "159.11.612",
				"nama"		=> "Jasa Keperawatan",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "jaspel_keperawatan",
				"filter"	=> ""
			)
		),
		"rpkk"	=> array(
			array(
				"no"		=> "N",
				"kode"		=> "554.",
				"nama"		=> "RPKK",
				"debet"		=> "",
				"kredit"	=> "",
				"cmd"		=> "",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>1</div>",
				"kode"		=> "",
				"nama"		=> "Tindakan Perawat Ruangan",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "total_pendapatan_keperawatan",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>2</div>",
				"kode"		=> "554.02.",
				"nama"		=> "Pendapatan Tind. Perawat",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "pendapatan_keperawatan_rs",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>3</div>",
				"kode"		=> "159.11.613",
				"nama"		=> "Jasa Keperawatan",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "jaspel_keperawatan",
				"filter"	=> ""
			)
		)
	);
	$html = "";
	foreach ($jurnal_items as $ji_key => $ji_value) {
		foreach ($ji_value as $ji_k => $ji_v) {
			$html .= "<tr id='" . $ji_key . "'>";
			foreach ($ji_v as $k => $v) {
				if ($k == "cmd" || $k == "filter")
					$html .= "<td style='display: none;' id='07_2_" . $ji_key . "-" . $ji_k . "-" . $k . "'><small>" . $v . "</small></td>";
				else
					$html .= "<td id='07_2_" . $ji_key . "-" . $ji_k . "-" . $k . "'><small>" . $v . "</small></td>";
			}
			$html .= "</tr>";
		}
	}
	$data = array();
	$data['html'] = $html;
	echo json_encode($data);
?>