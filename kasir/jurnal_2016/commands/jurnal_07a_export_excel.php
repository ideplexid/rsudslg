<?php
	require_once("smis-libs-out/php-excel/PHPExcel.php");
	
	$objPHPExcel = PHPExcel_IOFactory::load("kasir/jurnal_2016/templates/template_jurnal_07a.xlsx");
	
	$objPHPExcel->setActiveSheetIndexByName("Jurnal 07a");
	$objWorksheet = $objPHPExcel->getActiveSheet();
	if ($_POST['num_rows'] - 1 > 0)
		$objWorksheet->insertNewRowBefore(8, $_POST['num_rows'] - 1);
	
	$objWorksheet->setCellValue("J1", "JURNAL 07a");
	$objWorksheet->setCellValue("J2", "TGL. " . ArrayAdapter::format("date d-m-Y", $_POST['from']) . " s/d " . ArrayAdapter::format("date d-m-Y", $_POST['to']));
	
	$data = json_decode($_POST['d_data']);
	$start_row_num = 7;
	$end_row_num = 7;
	$row_num = $start_row_num;
	foreach ($data as $d) {
		$col_num = 0;
		$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->no);
		$col_num++;
		$objWorksheet->getCellByColumnAndRow($col_num, $row_num)->setValueExplicit($d->kode_akun, PHPExcel_Cell_DataType::TYPE_STRING);
		$col_num++;
		$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->ruangan);
		$col_num++;
		$objWorksheet->getCellByColumnAndRow($col_num, $row_num)->setValueExplicit($d->noreg_pasien, PHPExcel_Cell_DataType::TYPE_STRING);
		$col_num++;
		$objWorksheet->getCellByColumnAndRow($col_num, $row_num)->setValueExplicit($d->nrm_pasien, PHPExcel_Cell_DataType::TYPE_STRING);
		$col_num++;
		$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->nama_pasien);
		$col_num++;
		$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->jenis_pasien);
		$col_num++;
		$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->layanan);
		$col_num++;
		$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->rincian);
		$col_num++;
		$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->total);
		if ($d->merge) {
			$objWorksheet->mergeCells("A" . $row_num . ":I" . $row_num);
			$style = array(
				"alignment"	=> array(
					"horizontal"	=> PHPExcel_Style_Alignment::HORIZONTAL_CENTER
				),
				"font"		=> array(
					"bold"			=> true
				)
			);
			$objWorksheet->getStyle("A" . $row_num . ":I" . $row_num)->applyFromArray($style);
			unset($style['alignment']);
			$objWorksheet->getStyle("J" . $row_num)->applyFromArray($style);
		}
		$row_num++;
		$end_row_num++;
	}
	
	$objDrawing = new PHPExcel_Worksheet_Drawing();
	$objDrawing->setPath("kasir/jurnal_2016/resources/logo_1_5.png");
	$objDrawing->setCoordinates("A1");
	$objDrawing->setOffsetX(0);
	$objDrawing->setWorksheet($objWorksheet);
	
	header("Content-type: application/vnd.ms-excel");	
	header("Content-Disposition: attachment; filename=" . ArrayAdapter::format("slug", "JURNAL_07a") . "_" . $_POST['persentase_jaspel'] . "_" . $_POST['from'] . "_" . $_POST['to'] . "_" . date("Ymd_His") . ".xlsx");
	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");
	$objWriter->save("php://output");
?>