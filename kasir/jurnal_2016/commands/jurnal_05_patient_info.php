<?php
	require_once 'smis-base/smis-include-service-consumer.php';
	global $db;
	
	$from = $_POST['from'];
	$to = $_POST['to'];
	$num = $_POST['num'];
	$uri = $_POST['uri'];
	$jenis_pasien = $_POST['jenis_pasien'];
	
	/// Mendapatkan informasi Pasien
	$params = array();
	$params['from'] = $from;
	$params['to'] = $to;
	$params['jenis_pasien'] = $jenis_pasien;
	$params['num'] = $num;
	$params['uri'] = $uri;
	$consumer_service = new ServiceConsumer(
		$db,
		"get_info_pasien",
		$params,
		"registration"
	);
	$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
	$content = $consumer_service->execute()->getContent();
	$noreg_pasien = $content[0] * 1;
	$nrm_pasien = $content[1];
	$nama_pasien = $content[2];
	$tanggal_daftar_pasien = ArrayAdapter::format("date d/m/Y", $content[3]);
	$jenis_pasien = $content[4];
	$nama_perusahaan = $content[5];
	$nama_asuransi = $content[6];
	$id_perusahaan = $content[7];
	$id_asuransi = $content[8];
	
	/// Mendapatkan informasi jurnal di RPA (sewa_kamar, konsul_dokter, konsultasi_dokter, visite_dokter, tindakan_keperawatan, alok), meliputi prototype rawat :
	/// 1) RPA - Argopuro (rpa_argopuro)
	/// 2) RPA - Bromo (rpa_bromo)
	/// 3) RPA - Bromo Baru (rpa_bromobaru)
	/// 4) RPA - Kelud (rpa_kelud)
	$prototype_slugs = array(
		"rpa_argopuro",
		"rpa_bromo",
		"rpa_bromobaru",
		"rpa_kelud"
	);
	/// Sewa Kamar RPA (html element: rpa-1-debet, rpa-1-kredit) :
	$rpa_1_debet = 0;
	$rpa_1_kredit = 0;
	foreach ($prototype_slugs as $prototype_slug) {
		$params = array();
		$params['noreg_pasien'] = $noreg_pasien;
		$consumer_service = new ServiceConsumer(
			$db,
			"get_sewa_kamar_jurnal",
			$params,
			$prototype_slug
		);
		$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
		$content = $consumer_service->execute()->getContent();
		if ($content != null)
			$rpa_1_kredit += $content[0]['sewa_kamar'];
	}
	/// Visite / Jasa Dokter RPA (html element: rpa-2-debet, rpa-2-kredit) :
	$rpa_2_debet = 0;
	$rpa_2_kredit = 0;
	foreach ($prototype_slugs as $prototype_slug) {
		$params = array();
		$params['noreg_pasien'] = $noreg_pasien;
		$consumer_service = new ServiceConsumer(
			$db,
			"get_jasa_dokter_jurnal",
			$params,
			$prototype_slug
		);
		$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
		$content = $consumer_service->execute()->getContent();
		if ($content != null)
			$rpa_2_kredit += $content[0]['jasa_dokter'];
	}
	/// Jasa / Tindakan Keperawatan RPA (html element: rpa-3-debet, rpa-3-kredit) :
	$rpa_3_debet = 0;
	$rpa_3_kredit = 0;
	foreach ($prototype_slugs as $prototype_slug) {
		$params = array();
		$params['noreg_pasien'] = $noreg_pasien;
		$consumer_service = new ServiceConsumer(
			$db,
			"get_jasa_keperawatan_jurnal",
			$params,
			$prototype_slug
		);
		$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
		$content = $consumer_service->execute()->getContent();
		if ($content != null)
			$rpa_3_kredit += $content[0]['jasa_keperawatan'];
	}
	/// Alat Kesehatan RPA (html element: rpa-4-debet, rpa-4-kredit) :
	$rpa_4_debet = 0;
	$rpa_4_kredit = 0;
	foreach ($prototype_slugs as $prototype_slug) {
		$params = array();
		$params['noreg_pasien'] = $noreg_pasien;
		$consumer_service = new ServiceConsumer(
			$db,
			"get_alok_jurnal",
			$params,
			$prototype_slug
		);
		$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
		$content = $consumer_service->execute()->getContent();
		if ($content != null)
			$rpa_4_kredit += $content[0]['alok'];
	}
	/// Lain-Lain RPA (html element: rpa-5-debet, rpa-5-kredit) :
	$rpa_5_debet = 0;
	$rpa_5_kredit = 0;
	foreach ($prototype_slugs as $prototype_slug) {
		$params = array();
		$params['noreg_pasien'] = $noreg_pasien;
		$consumer_service = new ServiceConsumer(
			$db,
			"get_pendapatan_lain_jurnal",
			$params,
			$prototype_slug
		);
		$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
		$content = $consumer_service->execute()->getContent();
		if ($content != null)
			$rpa_5_kredit += $content[0]['pendapatan_lain'];
	}
	/// Total Debet Kredit RPA (html element: rpa-0-debet, rpa-0-kredit) :
	$rpa_0_debet = $rpa_1_debet + $rpa_2_debet + $rpa_3_debet + $rpa_4_debet + $rpa_5_debet;
	$rpa_0_kredit = $rpa_1_kredit + $rpa_2_kredit + $rpa_3_kredit + $rpa_4_kredit + $rpa_5_kredit;
	
	/// Mendapatkan informasi jurnal di RPB (sewa_kamar, konsul_dokter, konsultasi_dokter, visite_dokter, tindakan_keperawatan, alok), meliputi prototype rawat :
	/// 1) RPB - Argopuro (rpb_argopuro)
	/// 2) RPB - Ijen (rpb_ijen)
	/// 3) RPB - Raung (rpb_raung)
	/// 4) RPB - Kelud (rpb_kelud)
	$prototype_slugs = array(
		"rpb_argopuro",
		"rpb_ijen",
		"rpb_raung",
		"rpb_kelud"
	);
	/// Sewa Kamar RPB (html element: rpb-1-debet, rpb-1-kredit) :
	$rpb_1_debet = 0;
	$rpb_1_kredit = 0;
	$temp = "";
	foreach ($prototype_slugs as $prototype_slug) {
		$params = array();
		$params['noreg_pasien'] = $noreg_pasien;
		$consumer_service = new ServiceConsumer(
			$db,
			"get_sewa_kamar_jurnal",
			$params,
			$prototype_slug
		);
		$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
		$content = $consumer_service->execute()->getContent();
		if ($content != null)
			$rpb_1_kredit += $content[0]['sewa_kamar'];
	}
	/// Visite / Jasa Dokter RPB (html element: rpb-2-debet, rpb-2-kredit) :
	$rpb_2_debet = 0;
	$rpb_2_kredit = 0;
	foreach ($prototype_slugs as $prototype_slug) {
		$params = array();
		$params['noreg_pasien'] = $noreg_pasien;
		$consumer_service = new ServiceConsumer(
			$db,
			"get_jasa_dokter_jurnal",
			$params,
			$prototype_slug
		);
		$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
		$content = $consumer_service->execute()->getContent();
		if ($content != null)
			$rpb_2_kredit += $content[0]['jasa_dokter'];
	}
	/// Jasa / Tindakan Keperawatan RPB (html element: rpb-3-debet, rpb-3-kredit) :
	$rpb_3_debet = 0;
	$rpb_3_kredit = 0;
	foreach ($prototype_slugs as $prototype_slug) {
		$params = array();
		$params['noreg_pasien'] = $noreg_pasien;
		$consumer_service = new ServiceConsumer(
			$db,
			"get_jasa_keperawatan_jurnal",
			$params,
			$prototype_slug
		);
		$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
		$content = $consumer_service->execute()->getContent();
		if ($content != null)
			$rpb_3_kredit += $content[0]['jasa_keperawatan'];
	}
	/// Alat Kesehatan RPB (html element: rpb-4-debet, rpb-4-kredit) :
	$rpb_4_debet = 0;
	$rpb_4_kredit = 0;
	foreach ($prototype_slugs as $prototype_slug) {
		$params = array();
		$params['noreg_pasien'] = $noreg_pasien;
		$consumer_service = new ServiceConsumer(
			$db,
			"get_alok_jurnal",
			$params,
			$prototype_slug
		);
		$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
		$content = $consumer_service->execute()->getContent();
		if ($content != null)
			$rpb_4_kredit += $content[0]['alok'];
	}
	/// Lain-Lain RPB (html element: rpb-5-debet, rpb-5-kredit) :
	$rpb_5_debet = 0;
	$rpb_5_kredit = 0;
	foreach ($prototype_slugs as $prototype_slug) {
		$params = array();
		$params['noreg_pasien'] = $noreg_pasien;
		$consumer_service = new ServiceConsumer(
			$db,
			"get_pendapatan_lain_jurnal",
			$params,
			$prototype_slug
		);
		$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
		$content = $consumer_service->execute()->getContent();
		if ($content != null)
			$rpb_5_kredit += $content[0]['pendapatan_lain'];
	}
	/// Total Debet Kredit RPB (html element: rpb-0-debet, rpb-0-kredit) :
	$rpb_0_debet = $rpb_1_debet + $rpb_2_debet + $rpb_3_debet + $rpb_4_debet + $rpb_5_debet;
	$rpb_0_kredit = $rpb_1_kredit + $rpb_2_kredit + $rpb_3_kredit + $rpb_4_kredit + $rpb_5_kredit;
	
	/// Mendapatkan informasi jurnal di RPC (sewa_kamar, konsul_dokter, konsultasi_dokter, visite_dokter, tindakan_keperawatan, alok), meliputi prototype rawat :
	/// 1) Rollaas Diamond (rollaas_diamond)
	/// 2) Rollaas Emerald (rollaas_emerald)
	$prototype_slugs = array(
		"rollaas_diamond",
		"rollaas_emerald"
	);
	/// Sewa Kamar RPC (html element: rpc-1-debet, rpc-1-kredit) :
	$rpc_1_debet = 0;
	$rpc_1_kredit = 0;
	foreach ($prototype_slugs as $prototype_slug) {
		$params = array();
		$params['noreg_pasien'] = $noreg_pasien;
		$consumer_service = new ServiceConsumer(
			$db,
			"get_sewa_kamar_jurnal",
			$params,
			$prototype_slug
		);
		$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
		$content = $consumer_service->execute()->getContent();
		if ($content != null)
			$rpc_1_kredit += $content[0]['sewa_kamar'];
	}
	/// Visite / Jasa Dokter RPC (html element: rpc-2-debet, rpc-2-kredit) :
	$rpc_2_debet = 0;
	$rpc_2_kredit = 0;
	foreach ($prototype_slugs as $prototype_slug) {
		$params = array();
		$params['noreg_pasien'] = $noreg_pasien;
		$consumer_service = new ServiceConsumer(
			$db,
			"get_jasa_dokter_jurnal",
			$params,
			$prototype_slug
		);
		$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
		$content = $consumer_service->execute()->getContent();
		if ($content != null)
			$rpc_2_kredit += $content[0]['jasa_dokter'];
	}
	/// Jasa / Tindakan Keperawatan RPC (html element: rpc-3-debet, rpc-3-kredit) :
	$rpc_3_debet = 0;
	$rpc_3_kredit = 0;
	foreach ($prototype_slugs as $prototype_slug) {
		$params = array();
		$params['noreg_pasien'] = $noreg_pasien;
		$consumer_service = new ServiceConsumer(
			$db,
			"get_jasa_keperawatan_jurnal",
			$params,
			$prototype_slug
		);
		$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
		$content = $consumer_service->execute()->getContent();
		if ($content != null)
			$rpc_3_kredit += $content[0]['jasa_keperawatan'];
	}
	/// Alat Kesehatan RPC (html element: rpc-4-debet, rpc-4-kredit) :
	$rpc_4_debet = 0;
	$rpc_4_kredit = 0;
	foreach ($prototype_slugs as $prototype_slug) {
		$params = array();
		$params['noreg_pasien'] = $noreg_pasien;
		$consumer_service = new ServiceConsumer(
			$db,
			"get_alok_jurnal",
			$params,
			$prototype_slug
		);
		$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
		$content = $consumer_service->execute()->getContent();
		if ($content != null)
			$rpc_4_kredit += $content[0]['alok'];
	}
	/// Lain-Lain RPC (html element: rpc-5-debet, rpc-5-kredit) :
	$rpc_5_debet = 0;
	$rpc_5_kredit = 0;
	foreach ($prototype_slugs as $prototype_slug) {
		$params = array();
		$params['noreg_pasien'] = $noreg_pasien;
		$consumer_service = new ServiceConsumer(
			$db,
			"get_pendapatan_lain_jurnal",
			$params,
			$prototype_slug
		);
		$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
		$content = $consumer_service->execute()->getContent();
		if ($content != null)
			$rpc_5_kredit += $content[0]['pendapatan_lain'];
	}
	/// Total Debet Kredit RPC (html element: rpc-0-debet, rpc-0-kredit) :
	$rpc_0_debet = $rpc_1_debet + $rpc_2_debet + $rpc_3_debet + $rpc_4_debet + $rpc_5_debet;
	$rpc_0_kredit = $rpc_1_kredit + $rpc_2_kredit + $rpc_3_kredit + $rpc_4_kredit + $rpc_5_kredit;
	
	/// Mendapatkan informasi jurnal di RPD (sewa_kamar, konsul_dokter, konsultasi_dokter, visite_dokter, tindakan_keperawatan, alok), meliputi prototype rawat :
	/// 1) Rollaas Ruby (rollaas_ruby)
	$prototype_slugs = array(
		"rollaas_ruby"
	);
	/// Sewa Kamar RPD (html element: rpd-1-debet, rpd-1-kredit) :
	$rpd_1_debet = 0;
	$rpd_1_kredit = 0;
	foreach ($prototype_slugs as $prototype_slug) {
		$params = array();
		$params['noreg_pasien'] = $noreg_pasien;
		$consumer_service = new ServiceConsumer(
			$db,
			"get_sewa_kamar_jurnal",
			$params,
			$prototype_slug
		);
		$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
		$content = $consumer_service->execute()->getContent();
		if ($content != null)
			$rpd_1_kredit += $content[0]['sewa_kamar'];
	}
	/// Visite / Jasa Dokter RPD (html element: rpd-2-debet, rpd-2-kredit) :
	$rpd_2_debet = 0;
	$rpd_2_kredit = 0;
	foreach ($prototype_slugs as $prototype_slug) {
		$params = array();
		$params['noreg_pasien'] = $noreg_pasien;
		$consumer_service = new ServiceConsumer(
			$db,
			"get_jasa_dokter_jurnal",
			$params,
			$prototype_slug
		);
		$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
		$content = $consumer_service->execute()->getContent();
		if ($content != null)
			$rpd_2_kredit += $content[0]['jasa_dokter'];
	}
	/// Jasa / Tindakan Keperawatan RPD (html element: rpd-3-debet, rpd-3-kredit) :
	$rpd_3_debet = 0;
	$rpd_3_kredit = 0;
	foreach ($prototype_slugs as $prototype_slug) {
		$params = array();
		$params['noreg_pasien'] = $noreg_pasien;
		$consumer_service = new ServiceConsumer(
			$db,
			"get_jasa_keperawatan_jurnal",
			$params,
			$prototype_slug
		);
		$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
		$content = $consumer_service->execute()->getContent();
		if ($content != null)
			$rpd_3_kredit += $content[0]['jasa_keperawatan'];
	}
	/// Alat Kesehatan RPD (html element: rpd-4-debet, rpd-4-kredit) :
	$rpd_4_debet = 0;
	$rpd_4_kredit = 0;
	foreach ($prototype_slugs as $prototype_slug) {
		$params = array();
		$params['noreg_pasien'] = $noreg_pasien;
		$consumer_service = new ServiceConsumer(
			$db,
			"get_alok_jurnal",
			$params,
			$prototype_slug
		);
		$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
		$content = $consumer_service->execute()->getContent();
		if ($content != null)
			$rpd_4_kredit += $content[0]['alok'];
	}
	/// Lain-Lain RPD (html element: rpd-5-debet, rpd-5-kredit) :
	$rpd_5_debet = 0;
	$rpd_5_kredit = 0;
	foreach ($prototype_slugs as $prototype_slug) {
		$params = array();
		$params['noreg_pasien'] = $noreg_pasien;
		$consumer_service = new ServiceConsumer(
			$db,
			"get_pendapatan_lain_jurnal",
			$params,
			$prototype_slug
		);
		$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
		$content = $consumer_service->execute()->getContent();
		if ($content != null)
			$rpd_5_kredit += $content[0]['pendapatan_lain'];
	}
	/// Total Debet Kredit RPD (html element: rpd-0-debet, rpd-0-kredit) :
	$rpd_0_debet = $rpd_1_debet + $rpd_2_debet + $rpd_3_debet + $rpd_4_debet + $rpd_5_debet;
	$rpd_0_kredit = $rpd_1_kredit + $rpd_2_kredit + $rpd_3_kredit + $rpd_4_kredit + $rpd_5_kredit;
	
	/// Mendapatkan informasi jurnal di RPE (sewa_kamar, konsul_dokter, konsultasi_dokter, visite_dokter, tindakan_keperawatan, alok), meliputi prototype rawat :
	/// 1) Rollaas Saphire (rollaas_saphire)
	$prototype_slugs = array(
		"rollaas_saphire"
	);
	/// Sewa Kamar RPE (html element: rpe-1-debet, rpe-1-kredit) :
	$rpe_1_debet = 0;
	$rpe_1_kredit = 0;
	foreach ($prototype_slugs as $prototype_slug) {
		$params = array();
		$params['noreg_pasien'] = $noreg_pasien;
		$consumer_service = new ServiceConsumer(
			$db,
			"get_sewa_kamar_jurnal",
			$params,
			$prototype_slug
		);
		$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
		$content = $consumer_service->execute()->getContent();
		if ($content != null)
			$rpe_1_kredit += $content[0]['sewa_kamar'];
	}
	/// Visite / Jasa Dokter RPE (html element: rpe-2-debet, rpe-2-kredit) :
	$rpe_2_debet = 0;
	$rpe_2_kredit = 0;
	foreach ($prototype_slugs as $prototype_slug) {
		$params = array();
		$params['noreg_pasien'] = $noreg_pasien;
		$consumer_service = new ServiceConsumer(
			$db,
			"get_jasa_dokter_jurnal",
			$params,
			$prototype_slug
		);
		$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
		$content = $consumer_service->execute()->getContent();
		if ($content != null)
			$rpe_2_kredit += $content[0]['jasa_dokter'];
	}
	/// Jasa / Tindakan Keperawatan RPE (html element: rpe-3-debet, rpe-3-kredit) :
	$rpe_3_debet = 0;
	$rpe_3_kredit = 0;
	foreach ($prototype_slugs as $prototype_slug) {
		$params = array();
		$params['noreg_pasien'] = $noreg_pasien;
		$consumer_service = new ServiceConsumer(
			$db,
			"get_jasa_keperawatan_jurnal",
			$params,
			$prototype_slug
		);
		$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
		$content = $consumer_service->execute()->getContent();
		if ($content != null)
			$rpe_3_kredit += $content[0]['jasa_keperawatan'];
	}
	/// Alat Kesehatan RPE (html element: rpe-4-debet, rpe-4-kredit) :
	$rpe_4_debet = 0;
	$rpe_4_kredit = 0;
	foreach ($prototype_slugs as $prototype_slug) {
		$params = array();
		$params['noreg_pasien'] = $noreg_pasien;
		$consumer_service = new ServiceConsumer(
			$db,
			"get_alok_jurnal",
			$params,
			$prototype_slug
		);
		$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
		$content = $consumer_service->execute()->getContent();
		if ($content != null)
			$rpe_4_kredit += $content[0]['alok'];
	}
	/// Lain-Lain RPE (html element: rpe-5-debet, rpe-5-kredit) :
	$rpe_5_debet = 0;
	$rpe_5_kredit = 0;
	foreach ($prototype_slugs as $prototype_slug) {
		$params = array();
		$params['noreg_pasien'] = $noreg_pasien;
		$consumer_service = new ServiceConsumer(
			$db,
			"get_pendapatan_lain_jurnal",
			$params,
			$prototype_slug
		);
		$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
		$content = $consumer_service->execute()->getContent();
		if ($content != null)
			$rpe_5_kredit += $content[0]['pendapatan_lain'];
	}
	/// Total Debet Kredit RPE (html element: rpe-0-debet, rpe-0-kredit) :
	$rpe_0_debet = $rpe_1_debet + $rpe_2_debet + $rpe_3_debet + $rpe_4_debet + $rpe_5_debet;
	$rpe_0_kredit = $rpe_1_kredit + $rpe_2_kredit + $rpe_3_kredit + $rpe_4_kredit + $rpe_5_kredit;
	
	/// Mendapatkan informasi jurnal di ICU / RPO (sewa_kamar, konsul_dokter, konsultasi_dokter, visite_dokter, tindakan_keperawatan, alok), meliputi prototype rawat icu
	$prototype_slugs = array(
		"icu"
	);
	/// Sewa Kamar ICU / RPO (html element: icu-1-debet, icu-1-kredit) :
	$icu_1_debet = 0;
	$icu_1_kredit = 0;
	foreach ($prototype_slugs as $prototype_slug) {
		$params = array();
		$params['noreg_pasien'] = $noreg_pasien;
		$consumer_service = new ServiceConsumer(
			$db,
			"get_sewa_kamar_jurnal",
			$params,
			$prototype_slug
		);
		$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
		$content = $consumer_service->execute()->getContent();
		if ($content != null)
			$icu_1_kredit += $content[0]['sewa_kamar'];
	}
	/// Visite / Jasa Dokter ICU / RPO (html element: icu-2-debet, icu-2-kredit) :
	$icu_2_debet = 0;
	$icu_2_kredit = 0;
	foreach ($prototype_slugs as $prototype_slug) {
		$params = array();
		$params['noreg_pasien'] = $noreg_pasien;
		$consumer_service = new ServiceConsumer(
			$db,
			"get_jasa_dokter_jurnal",
			$params,
			$prototype_slug
		);
		$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
		$content = $consumer_service->execute()->getContent();
		if ($content != null)
			$icu_2_kredit += $content[0]['jasa_dokter'];
	}
	/// Jasa / Tindakan Keperawatan ICU / RPO (html element: icu-3-debet, icu-3-kredit) :
	$icu_3_debet = 0;
	$icu_3_kredit = 0;
	foreach ($prototype_slugs as $prototype_slug) {
		$params = array();
		$params['noreg_pasien'] = $noreg_pasien;
		$consumer_service = new ServiceConsumer(
			$db,
			"get_jasa_keperawatan_jurnal",
			$params,
			$prototype_slug
		);
		$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
		$content = $consumer_service->execute()->getContent();
		if ($content != null)
			$icu_3_kredit += $content[0]['jasa_keperawatan'];
	}
	/// Alat Kesehatan ICU / RPO (html element: icu-4-debet, icu-4-kredit) :
	$icu_4_debet = 0;
	$icu_4_kredit = 0;
	foreach ($prototype_slugs as $prototype_slug) {
		$params = array();
		$params['noreg_pasien'] = $noreg_pasien;
		$consumer_service = new ServiceConsumer(
			$db,
			"get_alok_jurnal",
			$params,
			$prototype_slug
		);
		$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
		$content = $consumer_service->execute()->getContent();
		if ($content != null)
			$icu_4_kredit += $content[0]['alok'];
	}
	/// Lain-Lain ICU / RPO (html element: icu-5-debet, icu-5-kredit) :
	$icu_5_debet = 0;
	$icu_5_kredit = 0;
	foreach ($prototype_slugs as $prototype_slug) {
		$params = array();
		$params['noreg_pasien'] = $noreg_pasien;
		$consumer_service = new ServiceConsumer(
			$db,
			"get_pendapatan_lain_jurnal",
			$params,
			$prototype_slug
		);
		$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
		$content = $consumer_service->execute()->getContent();
		if ($content != null)
			$icu_5_kredit += $content[0]['pendapatan_lain'];
	}
	/// Total Debet Kredit ICU / RPO (html element: icu-0-debet, icu-0-kredit) :
	$icu_0_debet = $icu_1_debet + $icu_2_debet + $icu_3_debet + $icu_4_debet + $icu_5_debet;
	$icu_0_kredit = $icu_1_kredit + $icu_2_kredit + $icu_3_kredit + $icu_4_kredit + $icu_5_kredit;
	
	/// Mendapatkan informasi jurnal di UGD (sewa_kamar, konsul_dokter, konsultasi_dokter, visite_dokter, tindakan_keperawatan, alok), meliputi prototype rawat instalasi_gawat_darurat
	$prototype_slugs = array(
		"instalasi_gawat_darurat"
	);
	/// Sewa Kamar UGD (html element: instalasi_gawat_darurat-1-debet, instalasi_gawat_darurat-1-kredit) :
	$instalasi_gawat_darurat_1_debet = 0;
	$instalasi_gawat_darurat_1_kredit = 0;
	foreach ($prototype_slugs as $prototype_slug) {
		$params = array();
		$params['noreg_pasien'] = $noreg_pasien;
		$consumer_service = new ServiceConsumer(
			$db,
			"get_sewa_kamar_jurnal",
			$params,
			$prototype_slug
		);
		$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
		$content = $consumer_service->execute()->getContent();
		if ($content != null)
			$instalasi_gawat_darurat_1_kredit += $content[0]['sewa_kamar'];
	}
	/// Visite / Jasa Dokter UGD (html element: instalasi_gawat_darurat-2-debet, instalasi_gawat_darurat-2-kredit) :
	$instalasi_gawat_darurat_2_debet = 0;
	$instalasi_gawat_darurat_2_kredit = 0;
	foreach ($prototype_slugs as $prototype_slug) {
		$params = array();
		$params['noreg_pasien'] = $noreg_pasien;
		$consumer_service = new ServiceConsumer(
			$db,
			"get_jasa_dokter_jurnal",
			$params,
			$prototype_slug
		);
		$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
		$content = $consumer_service->execute()->getContent();
		if ($content != null)
			$instalasi_gawat_darurat_2_kredit += $content[0]['jasa_dokter'];
	}
	/// Jasa / Tindakan Keperawatan UGD (html element: instalasi_gawat_darurat-3-debet, instalasi_gawat_darurat-3-kredit) :
	$instalasi_gawat_darurat_3_debet = 0;
	$instalasi_gawat_darurat_3_kredit = 0;
	foreach ($prototype_slugs as $prototype_slug) {
		$params = array();
		$params['noreg_pasien'] = $noreg_pasien;
		$consumer_service = new ServiceConsumer(
			$db,
			"get_jasa_keperawatan_jurnal",
			$params,
			$prototype_slug
		);
		$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
		$content = $consumer_service->execute()->getContent();
		if ($content != null)
			$instalasi_gawat_darurat_3_kredit += $content[0]['jasa_keperawatan'];
	}
	/// Alat Kesehatan UGD (html element: instalasi_gawat_darurat-4-debet, instalasi_gawat_darurat-4-kredit) :
	$instalasi_gawat_darurat_4_debet = 0;
	$instalasi_gawat_darurat_4_kredit = 0;
	foreach ($prototype_slugs as $prototype_slug) {
		$params = array();
		$params['noreg_pasien'] = $noreg_pasien;
		$consumer_service = new ServiceConsumer(
			$db,
			"get_alok_jurnal",
			$params,
			$prototype_slug
		);
		$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
		$content = $consumer_service->execute()->getContent();
		if ($content != null)
			$instalasi_gawat_darurat_4_kredit += $content[0]['alok'];
	}
	/// Lain-Lain UGD (html element: instalasi_gawat_darurat-5-debet, instalasi_gawat_darurat-5-kredit) :
	$instalasi_gawat_darurat_5_debet = 0;
	$instalasi_gawat_darurat_5_kredit = 0;
	foreach ($prototype_slugs as $prototype_slug) {
		$params = array();
		$params['noreg_pasien'] = $noreg_pasien;
		$consumer_service = new ServiceConsumer(
			$db,
			"get_pendapatan_lain_jurnal",
			$params,
			$prototype_slug
		);
		$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
		$content = $consumer_service->execute()->getContent();
		if ($content != null)
			$instalasi_gawat_darurat_5_kredit += $content[0]['pendapatan_lain'];
	}
	/// Total Debet Kredit UGD (html element: instalasi_gawat_darurat-0-debet, instalasi_gawat_darurat-0-kredit) :
	$instalasi_gawat_darurat_0_debet = $instalasi_gawat_darurat_1_debet + $instalasi_gawat_darurat_2_debet + $instalasi_gawat_darurat_3_debet + $instalasi_gawat_darurat_4_debet + $instalasi_gawat_darurat_5_debet;
	$instalasi_gawat_darurat_0_kredit = $instalasi_gawat_darurat_1_kredit + $instalasi_gawat_darurat_2_kredit + $instalasi_gawat_darurat_3_kredit + $instalasi_gawat_darurat_4_kredit + $instalasi_gawat_darurat_5_kredit;
	
	/// Mendapatkan informasi jurnal di Poli Umum (sewa_kamar, konsul_dokter, konsultasi_dokter, visite_dokter, tindakan_keperawatan, alok), meliputi prototype rawat poliumum
	$prototype_slugs = array(
		"poliumum"
	);
	/// Sewa Kamar Poli Umum (html element: poliumum-1-debet, poliumum-1-kredit) :
	$poliumum_1_debet = 0;
	$poliumum_1_kredit = 0;
	foreach ($prototype_slugs as $prototype_slug) {
		$params = array();
		$params['noreg_pasien'] = $noreg_pasien;
		$consumer_service = new ServiceConsumer(
			$db,
			"get_sewa_kamar_jurnal",
			$params,
			$prototype_slug
		);
		$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
		$content = $consumer_service->execute()->getContent();
		if ($content != null)
			$poliumum_1_kredit += $content[0]['sewa_kamar'];
	}
	/// Visite / Jasa Dokter Poli Umum (html element: poliumum-2-debet, poliumum-2-kredit) :
	$poliumum_2_debet = 0;
	$poliumum_2_kredit = 0;
	foreach ($prototype_slugs as $prototype_slug) {
		$params = array();
		$params['noreg_pasien'] = $noreg_pasien;
		$consumer_service = new ServiceConsumer(
			$db,
			"get_jasa_dokter_jurnal",
			$params,
			$prototype_slug
		);
		$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
		$content = $consumer_service->execute()->getContent();
		if ($content != null)
			$poliumum_2_kredit += $content[0]['jasa_dokter'];
	}
	/// Jasa / Tindakan Keperawatan Poli Umum (html element: poliumum-3-debet, poliumum-3-kredit) :
	$poliumum_3_debet = 0;
	$poliumum_3_kredit = 0;
	foreach ($prototype_slugs as $prototype_slug) {
		$params = array();
		$params['noreg_pasien'] = $noreg_pasien;
		$consumer_service = new ServiceConsumer(
			$db,
			"get_jasa_keperawatan_jurnal",
			$params,
			$prototype_slug
		);
		$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
		$content = $consumer_service->execute()->getContent();
		if ($content != null)
			$poliumum_3_kredit += $content[0]['jasa_keperawatan'];
	}
	/// Alat Kesehatan Poli Umum (html element: poliumum-4-debet, poliumum-4-kredit) :
	$poliumum_4_debet = 0;
	$poliumum_4_kredit = 0;
	foreach ($prototype_slugs as $prototype_slug) {
		$params = array();
		$params['noreg_pasien'] = $noreg_pasien;
		$consumer_service = new ServiceConsumer(
			$db,
			"get_alok_jurnal",
			$params,
			$prototype_slug
		);
		$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
		$content = $consumer_service->execute()->getContent();
		if ($content != null)
		$poliumum_4_kredit += $content[0]['alok'];
	}
	/// Lain-Lain Poli Umum (html element: poliumum-5-debet, poliumum-5-kredit) :
	$poliumum_5_debet = 0;
	$poliumum_5_kredit = 0;
	foreach ($prototype_slugs as $prototype_slug) {
		$params = array();
		$params['noreg_pasien'] = $noreg_pasien;
		$consumer_service = new ServiceConsumer(
			$db,
			"get_pendapatan_lain_jurnal",
			$params,
			$prototype_slug
		);
		$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
		$content = $consumer_service->execute()->getContent();
		if ($content != null)
			$poliumum_5_kredit += $content[0]['pendapatan_lain'];
	}
	/// Total Debet Kredit Poli Umum (html element: poliumum-0-debet, poliumum-0-kredit) :
	$poliumum_0_debet = $poliumum_1_debet + $poliumum_2_debet + $poliumum_3_debet + $poliumum_4_debet + $poliumum_5_debet;
	$poliumum_0_kredit = $poliumum_1_kredit + $poliumum_2_kredit + $poliumum_3_kredit + $poliumum_4_kredit + $poliumum_5_kredit;
	
	/// Mendapatkan informasi jurnal di Poli Gigi (sewa_kamar, konsul_dokter, konsultasi_dokter, visite_dokter, tindakan_keperawatan, alok), meliputi prototype rawat poligigi
	$prototype_slugs = array(
		"poligigi"
	);
	/// Sewa Kamar Poli Gigi (html element: poligigi-1-debet, poligigi-1-kredit) :
	$poligigi_1_debet = 0;
	$poligigi_1_kredit = 0;
	foreach ($prototype_slugs as $prototype_slug) {
		$params = array();
		$params['noreg_pasien'] = $noreg_pasien;
		$consumer_service = new ServiceConsumer(
			$db,
			"get_sewa_kamar_jurnal",
			$params,
			$prototype_slug
		);
		$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
		$content = $consumer_service->execute()->getContent();
		if ($content != null)
			$poligigi_1_kredit += $content[0]['sewa_kamar'];
	}
	/// Visite / Jasa Dokter Poli Gigi (html element: poligigi-2-debet, poligigi-2-kredit) :
	$poligigi_2_debet = 0;
	$poligigi_2_kredit = 0;
	foreach ($prototype_slugs as $prototype_slug) {
		$params = array();
		$params['noreg_pasien'] = $noreg_pasien;
		$consumer_service = new ServiceConsumer(
			$db,
			"get_jasa_dokter_jurnal",
			$params,
			$prototype_slug
		);
		$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
		$content = $consumer_service->execute()->getContent();
		if ($content != null)
			$poligigi_2_kredit += $content[0]['jasa_dokter'];
	}
	/// Jasa / Tindakan Keperawatan Poli Gigi (html element: poligigi-3-debet, poligigi-3-kredit) :
	$poligigi_3_debet = 0;
	$poligigi_3_kredit = 0;
	foreach ($prototype_slugs as $prototype_slug) {
		$params = array();
		$params['noreg_pasien'] = $noreg_pasien;
		$consumer_service = new ServiceConsumer(
			$db,
			"get_jasa_keperawatan_jurnal",
			$params,
			$prototype_slug
		);
		$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
		$content = $consumer_service->execute()->getContent();
		if ($content != null)
			$poligigi_3_kredit += $content[0]['jasa_keperawatan'];
	}
	/// Alat Kesehatan Poli Gigi (html element: poligigi-4-debet, poligigi-4-kredit) :
	$poligigi_4_debet = 0;
	$poligigi_4_kredit = 0;
	foreach ($prototype_slugs as $prototype_slug) {
		$params = array();
		$params['noreg_pasien'] = $noreg_pasien;
		$consumer_service = new ServiceConsumer(
			$db,
			"get_alok_jurnal",
			$params,
			$prototype_slug
		);
		$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
		$content = $consumer_service->execute()->getContent();
		if ($content != null)
			$poligigi_4_kredit += $content[0]['alok'];
	}
	/// Lain-Lain Poli Gigi (html element: poligigi-5-debet, poligigi-5-kredit) :
	$poligigi_5_debet = 0;
	$poligigi_5_kredit = 0;
	foreach ($prototype_slugs as $prototype_slug) {
		$params = array();
		$params['noreg_pasien'] = $noreg_pasien;
		$consumer_service = new ServiceConsumer(
			$db,
			"get_pendapatan_lain_jurnal",
			$params,
			$prototype_slug
		);
		$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
		$content = $consumer_service->execute()->getContent();
		if ($content != null)
			$poligigi_5_kredit += $content[0]['pendapatan_lain'];
	}
	/// Total Debet Kredit Poli Gigi (html element: poligigi-0-debet, poligigi-0-kredit) :
	$poligigi_0_debet = $poligigi_1_debet + $poligigi_2_debet + $poligigi_3_debet + $poligigi_4_debet + $poligigi_5_debet;
	$poligigi_0_kredit = $poligigi_1_kredit + $poligigi_2_kredit + $poligigi_3_kredit + $poligigi_4_kredit + $poligigi_5_kredit;
	
	/// Poli Spesialis Center (poli_spesialis_center) meliputi:
	/// 1) Poli Anak 				(html element: poli_anak)
	/// 2) Poli Obgyn 				(html element: poli_obgyn)
	/// 3) Poli Bedah				(html element: poli_bedah)
	/// 4) Poli Mata				(html element: poli_mata)
	/// 5) Poli THT					(html element: poli_tht)
	/// 6) Poli Kulit dan Kelamin	(html element: poli_kulit_dan_kelamin)
	/// 7) Poli Syaraf				(html element: poli_syaraf)
	/// 8) Poli Ortopedi			(html element: poli_ortopedi)
	/// 9) Poli Jantung				(html element: poli_jantung)
	/// 10) Poli Paru				(html element: poli_paru)
	/// 11) Poli Penyakit Dalam		(html element: poli_penyakit_dalam)
	
	/// Mendapatkan informasi jurnal di Poli Anak (sewa_kamar, konsul_dokter, konsultasi_dokter, visite_dokter, tindakan_keperawatan, alok)
	$params = array();
	$params['filter_kp'] = array(
		"poli_spesialis" => "poli_anak"
	);
	$consumer_service = new ServiceConsumer(
		$db,
		"get_dokter_jurnal",
		$params,
		"manajemen"
	);
	$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
	$content = $consumer_service->execute()->getContent();
	$id_dokter_poli_anak_csv = "";
	if ($content != null) {
		foreach ($content as $c)
			$id_dokter_poli_anak_csv .= $c['id_dokter'] . ",";
	}
	$id_dokter_poli_anak_csv = rtrim($id_dokter_poli_anak_csv, ",");
	/// Sewa Kamar Poli Anak (html element: poli_anak-1-debet, poli_anak-1-kredit) :
	$poli_anak_1_debet = 0;
	$poli_anak_1_kredit = 0;
	/// Visite / Jasa Dokter Poli Anak (html element: poli_anak-2-debet, poli_anak-2-kredit) :
	$poli_anak_2_debet = 0;
	$poli_anak_2_kredit = 0;
	$params = array();
	$params['noreg_pasien'] = $noreg_pasien;
	$params['id_dokter_csv'] = $id_dokter_poli_anak_csv;
	$consumer_service = new ServiceConsumer(
		$db,
		"get_jasa_dokter_jurnal",
		$params,
		"poli_spesialis_center"
	);
	$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
	$content = $consumer_service->execute()->getContent();
	if ($content != null)
		$poli_anak_2_kredit += $content[0]['jasa_dokter'];
	/// Jasa / Tindakan Keperawatan Poli Anak (html element: poli_anak-3-debet, poli_anak-3-kredit) :
	$poli_anak_3_debet = 0;
	$poli_anak_3_kredit = 0;
	/// Alat Kesehatan Poli Anak (html element: poli_anak-4-debet, poli_anak-4-kredit) :
	$poli_anak_4_debet = 0;
	$poli_anak_4_kredit = 0;
	/// Lain-Lain Poli Anak (html element: poli_anak-5-debet, poli_anak-5-kredit) :
	$poli_anak_5_debet = 0;
	$poli_anak_5_kredit = 0;
	/// Total Debet Kredit Poli Anak (html element: poli_anak-0-debet, poli_anak-0-kredit) :
	$poli_anak_0_debet = $poli_anak_1_debet + $poli_anak_2_debet + $poli_anak_3_debet + $poli_anak_4_debet + $poli_anak_5_debet;
	$poli_anak_0_kredit = $poli_anak_1_kredit + $poli_anak_2_kredit + $poli_anak_3_kredit + $poli_anak_4_kredit + $poli_anak_5_kredit;
	
	/// Mendapatkan informasi jurnal di Poli Obgyn (sewa_kamar, konsul_dokter, konsultasi_dokter, visite_dokter, tindakan_keperawatan, alok)
	$params = array();
	$params['filter_kp'] = array(
		"poli_spesialis" => "poli_obgyn"
	);
	$consumer_service = new ServiceConsumer(
		$db,
		"get_dokter_jurnal",
		$params,
		"manajemen"
	);
	$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
	$content = $consumer_service->execute()->getContent();
	$id_dokter_poli_obgyn_csv = "";
	if ($content != null) {
		foreach ($content as $c)
			$id_dokter_poli_obgyn_csv .= $c['id_dokter'] . ",";
	}
	$id_dokter_poli_obgyn_csv = rtrim($id_dokter_poli_obgyn_csv, ",");
	/// Sewa Kamar Poli Obgyn (html element: poli_obgyn-1-debet, poli_obgyn-1-kredit) :
	$poli_obgyn_1_debet = 0;
	$poli_obgyn_1_kredit = 0;
	/// Visite / Jasa Dokter Poli Obgyn (html element: poli_obgyn-2-debet, poli_obgyn-2-kredit) :
	$poli_obgyn_2_debet = 0;
	$poli_obgyn_2_kredit = 0;
	$params = array();
	$params['noreg_pasien'] = $noreg_pasien;
	$params['id_dokter_csv'] = $id_dokter_poli_obgyn_csv;
	$consumer_service = new ServiceConsumer(
		$db,
		"get_jasa_dokter_jurnal",
		$params,
		"poli_spesialis_center"
	);
	$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
	$content = $consumer_service->execute()->getContent();
	if ($content != null)
		$poli_obgyn_2_kredit += $content[0]['jasa_dokter'];
	/// Jasa / Tindakan Keperawatan Poli Obgyn (html element: poli_obgyn-3-debet, poli_obgyn-3-kredit) :
	$poli_obgyn_3_debet = 0;
	$poli_obgyn_3_kredit = 0;
	/// Alat Kesehatan Poli Obgyn (html element: poli_obgyn-4-debet, poli_obgyn-4-kredit) :
	$poli_obgyn_4_debet = 0;
	$poli_obgyn_4_kredit = 0;
	/// Lain-Lain Poli Obgyn (html element: poli_obgyn-5-debet, poli_obgyn-5-kredit) :
	$poli_obgyn_5_debet = 0;
	$poli_obgyn_5_kredit = 0;
	/// Total Debet Kredit Poli Obgyn (html element: poli_obgyn-0-debet, poli_obgyn-0-kredit) :
	$poli_obgyn_0_debet = $poli_obgyn_1_debet + $poli_obgyn_2_debet + $poli_obgyn_3_debet + $poli_obgyn_4_debet + $poli_obgyn_5_debet;
	$poli_obgyn_0_kredit = $poli_obgyn_1_kredit + $poli_obgyn_2_kredit + $poli_obgyn_3_kredit + $poli_obgyn_4_kredit + $poli_obgyn_5_kredit;
	
	/// Mendapatkan informasi jurnal di Poli Bedah (sewa_kamar, konsul_dokter, konsultasi_dokter, visite_dokter, tindakan_keperawatan, alok)
	$params = array();
	$params['filter_kp'] = array(
		"poli_spesialis" => "poli_bedah"
	);
	$consumer_service = new ServiceConsumer(
		$db,
		"get_dokter_jurnal",
		$params,
		"manajemen"
	);
	$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
	$content = $consumer_service->execute()->getContent();
	$id_dokter_poli_bedah_csv = "";
	if ($content != null) {
		foreach ($content as $c)
			$id_dokter_poli_bedah_csv .= $c['id_dokter'] . ",";
	}
	$id_dokter_poli_bedah_csv = rtrim($id_dokter_poli_bedah_csv, ",");
	/// Sewa Kamar Poli Bedah (html element: poli_bedah-1-debet, poli_bedah-1-kredit) :
	$poli_bedah_1_debet = 0;
	$poli_bedah_1_kredit = 0;
	/// Visite / Jasa Dokter Poli Bedah (html element: poli_bedah-2-debet, poli_bedah-2-kredit) :
	$poli_bedah_2_debet = 0;
	$poli_bedah_2_kredit = 0;
	$params = array();
	$params['noreg_pasien'] = $noreg_pasien;
	$params['id_dokter_csv'] = $id_dokter_poli_bedah_csv;
	$consumer_service = new ServiceConsumer(
		$db,
		"get_jasa_dokter_jurnal",
		$params,
		"poli_spesialis_center"
	);
	$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
	$content = $consumer_service->execute()->getContent();
	if ($content != null)
		$poli_bedah_2_kredit += $content[0]['jasa_dokter'];
	/// Jasa / Tindakan Keperawatan Poli Bedah (html element: poli_bedah-3-debet, poli_bedah-3-kredit) :
	$poli_bedah_3_debet = 0;
	$poli_bedah_3_kredit = 0;
	/// Alat Kesehatan Poli Bedah (html element: poli_bedah-4-debet, poli_bedah-4-kredit) :
	$poli_bedah_4_debet = 0;
	$poli_bedah_4_kredit = 0;
	/// Lain-Lain Poli Bedah (html element: poli_bedah-5-debet, poli_bedah-5-kredit) :
	$poli_bedah_5_debet = 0;
	$poli_bedah_5_kredit = 0;
	/// Total Debet Kredit Poli Bedah (html element: poli_bedah-0-debet, poli_bedah-0-kredit) :
	$poli_bedah_0_debet = $poli_bedah_1_debet + $poli_bedah_2_debet + $poli_bedah_3_debet + $poli_bedah_4_debet + $poli_bedah_5_debet;
	$poli_bedah_0_kredit = $poli_bedah_1_kredit + $poli_bedah_2_kredit + $poli_bedah_3_kredit + $poli_bedah_4_kredit + $poli_bedah_5_kredit;
	
	/// Mendapatkan informasi jurnal di Poli Mata (sewa_kamar, konsul_dokter, konsultasi_dokter, visite_dokter, tindakan_keperawatan, alok)
	$params = array();
	$params['filter_kp'] = array(
		"poli_spesialis" => "poli_mata"
	);
	$consumer_service = new ServiceConsumer(
		$db,
		"get_dokter_jurnal",
		$params,
		"manajemen"
	);
	$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
	$content = $consumer_service->execute()->getContent();
	$id_dokter_poli_mata_csv = "";
	if ($content != null) {
		foreach ($content as $c)
			$id_dokter_poli_mata_csv .= $c['id_dokter'] . ",";
	}
	$id_dokter_poli_mata_csv = rtrim($id_dokter_poli_mata_csv, ",");
	/// Sewa Kamar Poli Mata (html element: poli_mata-1-debet, poli_mata-1-kredit) :
	$poli_mata_1_debet = 0;
	$poli_mata_1_kredit = 0;
	/// Visite / Jasa Dokter Poli Mata (html element: poli_mata-2-debet, poli_mata-2-kredit) :
	$poli_mata_2_debet = 0;
	$poli_mata_2_kredit = 0;
	$params = array();
	$params['noreg_pasien'] = $noreg_pasien;
	$params['id_dokter_csv'] = $id_dokter_poli_mata_csv;
	$consumer_service = new ServiceConsumer(
		$db,
		"get_jasa_dokter_jurnal",
		$params,
		"poli_spesialis_center"
	);
	$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
	$content = $consumer_service->execute()->getContent();
	if ($content != null)
		$poli_mata_2_kredit += $content[0]['jasa_dokter'];
	/// Jasa / Tindakan Keperawatan Poli Mata (html element: poli_mata-3-debet, poli_mata-3-kredit) :
	$poli_mata_3_debet = 0;
	$poli_mata_3_kredit = 0;
	/// Alat Kesehatan Poli Mata (html element: poli_mata-4-debet, poli_mata-4-kredit) :
	$poli_mata_4_debet = 0;
	$poli_mata_4_kredit = 0;
	/// Lain-Lain Poli Mata (html element: poli_mata-5-debet, poli_mata-5-kredit) :
	$poli_mata_5_debet = 0;
	$poli_mata_5_kredit = 0;
	/// Total Debet Kredit Poli Mata (html element: poli_mata-0-debet, poli_mata-0-kredit) :
	$poli_mata_0_debet = $poli_mata_1_debet + $poli_mata_2_debet + $poli_mata_3_debet + $poli_mata_4_debet + $poli_mata_5_debet;
	$poli_mata_0_kredit = $poli_mata_1_kredit + $poli_mata_2_kredit + $poli_mata_3_kredit + $poli_mata_4_kredit + $poli_mata_5_kredit;
	
	/// Mendapatkan informasi jurnal di Poli THT (sewa_kamar, konsul_dokter, konsultasi_dokter, visite_dokter, tindakan_keperawatan, alok)
	$params = array();
	$params['filter_kp'] = array(
		"poli_spesialis" => "poli_tht"
	);
	$consumer_service = new ServiceConsumer(
		$db,
		"get_dokter_jurnal",
		$params,
		"manajemen"
	);
	$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
	$content = $consumer_service->execute()->getContent();
	$id_dokter_poli_tht_csv = "";
	if ($content != null) {
		foreach ($content as $c)
			$id_dokter_poli_tht_csv .= $c['id_dokter'] . ",";
	}
	$id_dokter_poli_tht_csv = rtrim($id_dokter_poli_tht_csv, ",");
	/// Sewa Kamar Poli THT (html element: poli_tht-1-debet, poli_tht-1-kredit) :
	$poli_tht_1_debet = 0;
	$poli_tht_1_kredit = 0;
	/// Visite / Jasa Dokter Poli THT (html element: poli_tht-2-debet, poli_tht-2-kredit) :
	$poli_tht_2_debet = 0;
	$poli_tht_2_kredit = 0;
	$params = array();
	$params['noreg_pasien'] = $noreg_pasien;
	$params['id_dokter_csv'] = $id_dokter_poli_tht_csv;
	$consumer_service = new ServiceConsumer(
		$db,
		"get_jasa_dokter_jurnal",
		$params,
		"poli_spesialis_center"
	);
	$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
	$content = $consumer_service->execute()->getContent();
	if ($content != null)
		$poli_tht_2_kredit += $content[0]['jasa_dokter'];
	/// Jasa / Tindakan Keperawatan Poli THT (html element: poli_tht-3-debet, poli_tht-3-kredit) :
	$poli_tht_3_debet = 0;
	$poli_tht_3_kredit = 0;
	/// Alat Kesehatan Poli THT (html element: poli_tht-4-debet, poli_tht-4-kredit) :
	$poli_tht_4_debet = 0;
	$poli_tht_4_kredit = 0;
	/// Lain-Lain Poli THT (html element: poli_tht-5-debet, poli_tht-5-kredit) :
	$poli_tht_5_debet = 0;
	$poli_tht_5_kredit = 0;
	/// Total Debet Kredit Poli THT (html element: poli_tht-0-debet, poli_tht-0-kredit) :
	$poli_tht_0_debet = $poli_tht_1_debet + $poli_tht_2_debet + $poli_tht_3_debet + $poli_tht_4_debet + $poli_tht_5_debet;
	$poli_tht_0_kredit = $poli_tht_1_kredit + $poli_tht_2_kredit + $poli_tht_3_kredit + $poli_tht_4_kredit + $poli_tht_5_kredit;
	
	/// Mendapatkan informasi jurnal di Poli Kulit dan Kelamin (sewa_kamar, konsul_dokter, konsultasi_dokter, visite_dokter, tindakan_keperawatan, alok)
	$params = array();
	$params['filter_kp'] = array(
		"poli_spesialis" => "poli_kulit_dan_kelamin"
	);
	$consumer_service = new ServiceConsumer(
		$db,
		"get_dokter_jurnal",
		$params,
		"manajemen"
	);
	$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
	$content = $consumer_service->execute()->getContent();
	$id_dokter_poli_kulit_dan_kelamin_csv = "";
	if ($content != null) {
		foreach ($content as $c)
			$id_dokter_poli_kulit_dan_kelamin_csv .= $c['id_dokter'] . ",";
	}
	$id_dokter_poli_kulit_dan_kelamin_csv = rtrim($id_dokter_poli_kulit_dan_kelamin_csv, ",");
	/// Sewa Kamar Poli Kulit dan Kelamin (html element: poli_kulit_dan_kelamin-1-debet, poli_kulit_dan_kelamin-1-kredit) :
	$poli_kulit_dan_kelamin_1_debet = 0;
	$poli_kulit_dan_kelamin_1_kredit = 0;
	/// Visite / Jasa Dokter Poli Kulit dan Kelamin (html element: poli_kulit_dan_kelamin-2-debet, poli_kulit_dan_kelamin-2-kredit) :
	$poli_kulit_dan_kelamin_2_debet = 0;
	$poli_kulit_dan_kelamin_2_kredit = 0;
	$params = array();
	$params['noreg_pasien'] = $noreg_pasien;
	$params['id_dokter_csv'] = $id_dokter_poli_kulit_dan_kelamin_csv;
	$consumer_service = new ServiceConsumer(
		$db,
		"get_jasa_dokter_jurnal",
		$params,
		"poli_spesialis_center"
	);
	$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
	$content = $consumer_service->execute()->getContent();
	if ($content != null)
		$poli_kulit_dan_kelamin_2_kredit += $content[0]['jasa_dokter'];
	/// Jasa / Tindakan Keperawatan Poli Kulit dan Kelamin (html element: poli_kulit_dan_kelamin-3-debet, poli_kulit_dan_kelamin-3-kredit) :
	$poli_kulit_dan_kelamin_3_debet = 0;
	$poli_kulit_dan_kelamin_3_kredit = 0;
	/// Alat Kesehatan Poli Kulit dan Kelamin (html element: poli_kulit_dan_kelamin-4-debet, poli_kulit_dan_kelamin-4-kredit) :
	$poli_kulit_dan_kelamin_4_debet = 0;
	$poli_kulit_dan_kelamin_4_kredit = 0;
	/// Lain-Lain Poli Kulit dan Kelamin (html element: poli_kulit_dan_kelamin-5-debet, poli_kulit_dan_kelamin-5-kredit) :
	$poli_kulit_dan_kelamin_5_debet = 0;
	$poli_kulit_dan_kelamin_5_kredit = 0;
	/// Total Debet Kredit Poli Kulit dan Kelamin (html element: poli_kulit_dan_kelamin-0-debet, poli_kulit_dan_kelamin-0-kredit) :
	$poli_kulit_dan_kelamin_0_debet = $poli_kulit_dan_kelamin_1_debet + $poli_kulit_dan_kelamin_2_debet + $poli_kulit_dan_kelamin_3_debet + $poli_kulit_dan_kelamin_4_debet + $poli_kulit_dan_kelamin_5_debet;
	$poli_kulit_dan_kelamin_0_kredit = $poli_kulit_dan_kelamin_1_kredit + $poli_kulit_dan_kelamin_2_kredit + $poli_kulit_dan_kelamin_3_kredit + $poli_kulit_dan_kelamin_4_kredit + $poli_kulit_dan_kelamin_5_kredit;
	
	/// Mendapatkan informasi jurnal di Poli Syaraf (sewa_kamar, konsul_dokter, konsultasi_dokter, visite_dokter, tindakan_keperawatan, alok)
	$params = array();
	$params['filter_kp'] = array(
		"poli_spesialis" => "poli_syaraf"
	);
	$consumer_service = new ServiceConsumer(
		$db,
		"get_dokter_jurnal",
		$params,
		"manajemen"
	);
	$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
	$content = $consumer_service->execute()->getContent();
	$id_dokter_poli_syaraf_csv = "";
	if ($content != null) {
		foreach ($content as $c)
			$id_dokter_poli_syaraf_csv .= $c['id_dokter'] . ",";
	}
	$id_dokter_poli_syaraf_csv = rtrim($id_dokter_poli_syaraf_csv, ",");
	/// Sewa Kamar Poli Syaraf (html element: poli_syaraf-1-debet, poli_syaraf-1-kredit) :
	$poli_syaraf_1_debet = 0;
	$poli_syaraf_1_kredit = 0;
	/// Visite / Jasa Dokter Poli Syaraf (html element: poli_syaraf-2-debet, poli_syaraf-2-kredit) :
	$poli_syaraf_2_debet = 0;
	$poli_syaraf_2_kredit = 0;
	$params = array();
	$params['noreg_pasien'] = $noreg_pasien;
	$params['id_dokter_csv'] = $id_dokter_poli_syaraf_csv;
	$consumer_service = new ServiceConsumer(
		$db,
		"get_jasa_dokter_jurnal",
		$params,
		"poli_spesialis_center"
	);
	$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
	$content = $consumer_service->execute()->getContent();
	if ($content != null)
		$poli_syaraf_2_kredit += $content[0]['jasa_dokter'];
	/// Jasa / Tindakan Keperawatan Poli Syaraf (html element: poli_syaraf-3-debet, poli_syaraf-3-kredit) :
	$poli_syaraf_3_debet = 0;
	$poli_syaraf_3_kredit = 0;
	/// Alat Kesehatan Poli Syaraf (html element: poli_syaraf-4-debet, poli_syaraf-4-kredit) :
	$poli_syaraf_4_debet = 0;
	$poli_syaraf_4_kredit = 0;
	/// Lain-Lain Poli Syaraf (html element: poli_syaraf-5-debet, poli_syaraf-5-kredit) :
	$poli_syaraf_5_debet = 0;
	$poli_syaraf_5_kredit = 0;
	/// Total Debet Kredit Poli Syaraf (html element: poli_syaraf-0-debet, poli_syaraf-0-kredit) :
	$poli_syaraf_0_debet = $poli_syaraf_1_debet + $poli_syaraf_2_debet + $poli_syaraf_3_debet + $poli_syaraf_4_debet + $poli_syaraf_5_debet;
	$poli_syaraf_0_kredit = $poli_syaraf_1_kredit + $poli_syaraf_2_kredit + $poli_syaraf_3_kredit + $poli_syaraf_4_kredit + $poli_syaraf_5_kredit;
	
	/// Mendapatkan informasi jurnal di Poli Ortopedi (sewa_kamar, konsul_dokter, konsultasi_dokter, visite_dokter, tindakan_keperawatan, alok)
	$params = array();
	$params['filter_kp'] = array(
		"poli_spesialis" => "poli_ortopedi"
	);
	$consumer_service = new ServiceConsumer(
		$db,
		"get_dokter_jurnal",
		$params,
		"manajemen"
	);
	$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
	$content = $consumer_service->execute()->getContent();
	$id_dokter_poli_ortopedi_csv = "";
	if ($content != null) {
		foreach ($content as $c)
			$id_dokter_poli_ortopedi_csv .= $c['id_dokter'] . ",";
	}
	$id_dokter_poli_ortopedi_csv = rtrim($id_dokter_poli_ortopedi_csv, ",");
	/// Sewa Kamar Poli Ortopedi (html element: poli_ortopedi-1-debet, poli_ortopedi-1-kredit) :
	$poli_ortopedi_1_debet = 0;
	$poli_ortopedi_1_kredit = 0;
	/// Visite / Jasa Dokter Poli Ortopedi (html element: poli_ortopedi-2-debet, poli_ortopedi-2-kredit) :
	$poli_ortopedi_2_debet = 0;
	$poli_ortopedi_2_kredit = 0;
	$params = array();
	$params['noreg_pasien'] = $noreg_pasien;
	$params['id_dokter_csv'] = $id_dokter_poli_ortopedi_csv;
	$consumer_service = new ServiceConsumer(
		$db,
		"get_jasa_dokter_jurnal",
		$params,
		"poli_spesialis_center"
	);
	$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
	$content = $consumer_service->execute()->getContent();
	if ($content != null)
		$poli_ortopedi_2_kredit += $content[0]['jasa_dokter'];
	/// Jasa / Tindakan Keperawatan Poli Ortopedi (html element: poli_ortopedi-3-debet, poli_ortopedi-3-kredit) :
	$poli_ortopedi_3_debet = 0;
	$poli_ortopedi_3_kredit = 0;
	/// Alat Kesehatan Poli Ortopedi (html element: poli_ortopedi-4-debet, poli_ortopedi-4-kredit) :
	$poli_ortopedi_4_debet = 0;
	$poli_ortopedi_4_kredit = 0;
	/// Lain-Lain Poli Ortopedi (html element: poli_ortopedi-5-debet, poli_ortopedi-5-kredit) :
	$poli_ortopedi_5_debet = 0;
	$poli_ortopedi_5_kredit = 0;
	/// Total Debet Kredit Poli Ortopedi (html element: poli_ortopedi-0-debet, poli_ortopedi-0-kredit) :
	$poli_ortopedi_0_debet = $poli_ortopedi_1_debet + $poli_ortopedi_2_debet + $poli_ortopedi_3_debet + $poli_ortopedi_4_debet + $poli_ortopedi_5_debet;
	$poli_ortopedi_0_kredit = $poli_ortopedi_1_kredit + $poli_ortopedi_2_kredit + $poli_ortopedi_3_kredit + $poli_ortopedi_4_kredit + $poli_ortopedi_5_kredit;
	
	/// Mendapatkan informasi jurnal di Poli Jantung (sewa_kamar, konsul_dokter, konsultasi_dokter, visite_dokter, tindakan_keperawatan, alok)
	$params = array();
	$params['filter_kp'] = array(
		"poli_spesialis" => "poli_jantung"
	);
	$consumer_service = new ServiceConsumer(
		$db,
		"get_dokter_jurnal",
		$params,
		"manajemen"
	);
	$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
	$content = $consumer_service->execute()->getContent();
	$id_dokter_poli_jantung_csv = "";
	if ($content != null) {
		foreach ($content as $c)
			$id_dokter_poli_jantung_csv .= $c['id_dokter'] . ",";
	}
	$id_dokter_poli_jantung_csv = rtrim($id_dokter_poli_jantung_csv, ",");
	/// Sewa Kamar Poli Jantung (html element: poli_jantung-1-debet, poli_jantung-1-kredit) :
	$poli_jantung_1_debet = 0;
	$poli_jantung_1_kredit = 0;
	/// Visite / Jasa Dokter Poli Jantung (html element: poli_jantung-2-debet, poli_jantung-2-kredit) :
	$poli_jantung_2_debet = 0;
	$poli_jantung_2_kredit = 0;
	$params = array();
	$params['noreg_pasien'] = $noreg_pasien;
	$params['id_dokter_csv'] = $id_dokter_poli_jantung_csv;
	$consumer_service = new ServiceConsumer(
		$db,
		"get_jasa_dokter_jurnal",
		$params,
		"poli_spesialis_center"
	);
	$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
	$content = $consumer_service->execute()->getContent();
	if ($content != null)
		$poli_jantung_2_kredit += $content[0]['jasa_dokter'];
	/// Jasa / Tindakan Keperawatan Poli Jantung (html element: poli_jantung-3-debet, poli_jantung-3-kredit) :
	$poli_jantung_3_debet = 0;
	$poli_jantung_3_kredit = 0;
	/// Alat Kesehatan Poli Jantung (html element: poli_jantung-4-debet, poli_jantung-4-kredit) :
	$poli_jantung_4_debet = 0;
	$poli_jantung_4_kredit = 0;
	/// Lain-Lain Poli Jantung (html element: poli_jantung-5-debet, poli_jantung-5-kredit) :
	$poli_jantung_5_debet = 0;
	$poli_jantung_5_kredit = 0;
	/// Total Debet Kredit Poli Jantung (html element: poli_jantung-0-debet, poli_jantung-0-kredit) :
	$poli_jantung_0_debet = $poli_jantung_1_debet + $poli_jantung_2_debet + $poli_jantung_3_debet + $poli_jantung_4_debet + $poli_jantung_5_debet;
	$poli_jantung_0_kredit = $poli_jantung_1_kredit + $poli_jantung_2_kredit + $poli_jantung_3_kredit + $poli_jantung_4_kredit + $poli_jantung_5_kredit;
	
	/// Mendapatkan informasi jurnal di Poli Paru (sewa_kamar, konsul_dokter, konsultasi_dokter, visite_dokter, tindakan_keperawatan, alok)
	$params = array();
	$params['filter_kp'] = array(
		"poli_spesialis" => "poli_paru"
	);
	$consumer_service = new ServiceConsumer(
		$db,
		"get_dokter_jurnal",
		$params,
		"manajemen"
	);
	$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
	$content = $consumer_service->execute()->getContent();
	$id_dokter_poli_paru_csv = "";
	if ($content != null) {
		foreach ($content as $c)
			$id_dokter_poli_paru_csv .= $c['id_dokter'] . ",";
	}
	$id_dokter_poli_paru_csv = rtrim($id_dokter_poli_paru_csv, ",");
	/// Sewa Kamar Poli Paru (html element: poli_paru-1-debet, poli_paru-1-kredit) :
	$poli_paru_1_debet = 0;
	$poli_paru_1_kredit = 0;
	/// Visite / Jasa Dokter Poli Paru (html element: poli_paru-2-debet, poli_paru-2-kredit) :
	$poli_paru_2_debet = 0;
	$poli_paru_2_kredit = 0;
	$params = array();
	$params['noreg_pasien'] = $noreg_pasien;
	$params['id_dokter_csv'] = $id_dokter_poli_paru_csv;
	$consumer_service = new ServiceConsumer(
		$db,
		"get_jasa_dokter_jurnal",
		$params,
		"poli_spesialis_center"
	);
	$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
	$content = $consumer_service->execute()->getContent();
	if ($content != null)
		$poli_paru_2_kredit += $content[0]['jasa_dokter'];
	/// Jasa / Tindakan Keperawatan Poli Paru (html element: poli_paru-3-debet, poli_paru-3-kredit) :
	$poli_paru_3_debet = 0;
	$poli_paru_3_kredit = 0;
	/// Alat Kesehatan Poli Paru (html element: poli_paru-4-debet, poli_paru-4-kredit) :
	$poli_paru_4_debet = 0;
	$poli_paru_4_kredit = 0;
	/// Lain-Lain Poli Paru (html element: poli_paru-5-debet, poli_paru-5-kredit) :
	$poli_paru_5_debet = 0;
	$poli_paru_5_kredit = 0;
	/// Total Debet Kredit Poli Paru (html element: poli_paru-0-debet, poli_paru-0-kredit) :
	$poli_paru_0_debet = $poli_paru_1_debet + $poli_paru_2_debet + $poli_paru_3_debet + $poli_paru_4_debet + $poli_paru_5_debet;
	$poli_paru_0_kredit = $poli_paru_1_kredit + $poli_paru_2_kredit + $poli_paru_3_kredit + $poli_paru_4_kredit + $poli_paru_5_kredit;
	
	/// Mendapatkan informasi jurnal di Poli Penyakit Dalam (sewa_kamar, konsul_dokter, konsultasi_dokter, visite_dokter, tindakan_keperawatan, alok)
	$params = array();
	$params['filter_kp'] = array(
		"poli_spesialis" => "poli_penyakit_dalam"
	);
	$consumer_service = new ServiceConsumer(
		$db,
		"get_dokter_jurnal",
		$params,
		"manajemen"
	);
	$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
	$content = $consumer_service->execute()->getContent();
	$id_dokter_poli_penyakit_dalam_csv = "";
	if ($content != null) {
		foreach ($content as $c)
			$id_dokter_poli_penyakit_dalam_csv .= $c['id_dokter'] . ",";
	}
	$id_dokter_poli_penyakit_dalam_csv = rtrim($id_dokter_poli_penyakit_dalam_csv, ",");
	/// Sewa Kamar Poli Penyakit Dalam (html element: poli_penyakit_dalam-1-debet, poli_penyakit_dalam-1-kredit) :
	$poli_penyakit_dalam_1_debet = 0;
	$poli_penyakit_dalam_1_kredit = 0;
	/// Visite / Jasa Dokter Poli Penyakit Dalam (html element: poli_penyakit_dalam-2-debet, poli_penyakit_dalam-2-kredit) :
	$poli_penyakit_dalam_2_debet = 0;
	$poli_penyakit_dalam_2_kredit = 0;
	$params = array();
	$params['noreg_pasien'] = $noreg_pasien;
	$params['id_dokter_csv'] = $id_dokter_poli_penyakit_dalam_csv;
	$consumer_service = new ServiceConsumer(
		$db,
		"get_jasa_dokter_jurnal",
		$params,
		"poli_spesialis_center"
	);
	$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
	$content = $consumer_service->execute()->getContent();
	if ($content != null)
		$poli_penyakit_dalam_2_kredit += $content[0]['jasa_dokter'];
	/// Jasa / Tindakan Keperawatan Poli Penyakit Dalam (html element: poli_penyakit_dalam-3-debet, poli_penyakit_dalam-3-kredit) :
	$poli_penyakit_dalam_3_debet = 0;
	$poli_penyakit_dalam_3_kredit = 0;
	/// Alat Kesehatan Poli Penyakit Dalam (html element: poli_penyakit_dalam-4-debet, poli_penyakit_dalam-4-kredit) :
	$poli_penyakit_dalam_4_debet = 0;
	$poli_penyakit_dalam_4_kredit = 0;
	/// Lain-Lain Poli Penyakit Dalam (html element: poli_penyakit_dalam-5-debet, poli_penyakit_dalam-5-kredit) :
	$poli_penyakit_dalam_5_debet = 0;
	$poli_penyakit_dalam_5_kredit = 0;
	/// Total Debet Kredit Poli Penyakit Dalam (html element: poli_penyakit_dalam-0-debet, poli_penyakit_dalam-0-kredit) :
	$poli_penyakit_dalam_0_debet = $poli_penyakit_dalam_1_debet + $poli_penyakit_dalam_2_debet + $poli_penyakit_dalam_3_debet + $poli_penyakit_dalam_4_debet + $poli_penyakit_dalam_5_debet;
	$poli_penyakit_dalam_0_kredit = $poli_penyakit_dalam_1_kredit + $poli_penyakit_dalam_2_kredit + $poli_penyakit_dalam_3_kredit + $poli_penyakit_dalam_4_kredit + $poli_penyakit_dalam_5_kredit;
	
	/// Mendapatkan informasi jurnal di Gizi (html element: gizi) :
	/// Sewa Kamar Gizi (html element: gizi-1-debet, gizi-1-kredit) :
	$gizi_1_debet = 0;
	$gizi_1_kredit = 0;
	/// Visite / Jasa Dokter Gizi (html element: gizi-2-debet, gizi-2-kredit) :
	$gizi_2_debet = 0;
	$gizi_2_kredit = 0;
	/// Jasa Gizi (html element: gizi-3-debet, gizi-3-kredit) :
	$gizi_3_debet = 0;
	$gizi_3_kredit = 0;
	$params = array();
	$params['noreg_pasien'] = $noreg_pasien;
	$consumer_service = new ServiceConsumer(
		$db,
		"get_gizi_jurnal",
		$params,
		"gizi"
	);
	$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
	$content = $consumer_service->execute()->getContent();
	if ($content != null)
		$gizi_3_kredit += $content[0]['gizi'];
	/// Alat Kesehatan Gizi (html element: gizi-4-debet, gizi-4-kredit) :
	$gizi_4_debet = 0;
	$gizi_4_kredit = 0;
	/// Lain-Lain Gizi (html element: gizi-5-debet, gizi-5-kredit) :
	$gizi_5_debet = 0;
	$gizi_5_kredit = 0;
	/// Total Debet Kredit Gizi (html element: gizi-0-debet, gizi-0-kredit) :
	$gizi_0_debet = $gizi_1_debet + $gizi_2_debet + $gizi_3_debet + $gizi_4_debet + $gizi_5_debet;
	$gizi_0_kredit = $gizi_1_kredit + $gizi_2_kredit + $gizi_3_kredit + $gizi_4_kredit + $gizi_5_kredit;
	
	/// Mendapatkan informasi jurnal di Hemodialisa (html element: hemodialisa)
	/// Sewa Kamar Hemodialisa (html element: hemodialisa-1-debet, hemodialisa-1-kredit) :
	$hemodialisa_1_debet = 0;
	$hemodialisa_1_kredit = 0;
	$params = array();
	$params['noreg_pasien'] = $noreg_pasien;
	$consumer_service = new ServiceConsumer(
		$db,
		"get_sewa_kamar_jurnal",
		$params,
		"hemodialisa"
	);
	$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
	$content = $consumer_service->execute()->getContent();
	if ($content != null)
		$hemodialisa_1_kredit += $content[0]['sewa_kamar'];
	/// Visite / Jasa Dokter Hemodialisa (html element: hemodialisa-2-debet, hemodialisa-2-kredit) :
	$hemodialisa_2_debet = 0;
	$hemodialisa_2_kredit = 0;
	$params = array();
	$params['noreg_pasien'] = $noreg_pasien;
	$consumer_service = new ServiceConsumer(
		$db,
		"get_jasa_dokter_jurnal",
		$params,
		"hemodialisa"
	);
	$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
	$content = $consumer_service->execute()->getContent();
	if ($content != null)
		$hemodialisa_2_kredit += $content[0]['jasa_dokter'];
	/// Jasa / Tindakan Keperawatan Hemodialisa (html element: hemodialisa-3-debet, hemodialisa-3-kredit) :
	$hemodialisa_3_debet = 0;
	$hemodialisa_3_kredit = 0;
	$params = array();
	$params['noreg_pasien'] = $noreg_pasien;
	$consumer_service = new ServiceConsumer(
		$db,
		"get_jasa_keperawatan_jurnal",
		$params,
		"hemodialisa"
	);
	$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
	$content = $consumer_service->execute()->getContent();
	if ($content != null)
		$hemodialisa_3_kredit += $content[0]['jasa_keperawatan'];
	/// Alat Kesehatan Hemodialisa (html element: hemodialisa-4-debet, hemodialisa-4-kredit) :
	$hemodialisa_4_debet = 0;
	$hemodialisa_4_kredit = 0;
	$params = array();
	$params['noreg_pasien'] = $noreg_pasien;
	$consumer_service = new ServiceConsumer(
		$db,
		"get_alok_jurnal",
		$params,
		"hemodialisa"
	);
	$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
	$content = $consumer_service->execute()->getContent();
	if ($content != null)
		$hemodialisa_4_kredit += $content[0]['alok'];
	/// Lain-Lain Hemodialisa (html element: hemodialisa-5-debet, hemodialisa-5-kredit) :
	$hemodialisa_5_debet = 0;
	$hemodialisa_5_kredit = 0;
	$params = array();
	$params['noreg_pasien'] = $noreg_pasien;
	$consumer_service = new ServiceConsumer(
		$db,
		"get_pendapatan_lain_jurnal",
		$params,
		"hemodialisa"
	);
	$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
	$content = $consumer_service->execute()->getContent();
	if ($content != null)
		$hemodialisa_5_kredit += $content[0]['pendapatan_lain'];
	/// Total Debet Kredit Hemodialisa (html element: hemodialisa-0-debet, hemodialisa-0-kredit) :
	$hemodialisa_0_debet = $hemodialisa_1_debet + $hemodialisa_2_debet + $hemodialisa_3_debet + $hemodialisa_4_debet + $hemodialisa_5_debet;
	$hemodialisa_0_kredit = $hemodialisa_1_kredit + $hemodialisa_2_kredit + $hemodialisa_3_kredit + $hemodialisa_4_kredit + $hemodialisa_5_kredit;
	
	/// Mendapatkan informasi jurnal di Fisioterapi (html element: fisiotherapy)
	/// Sewa Kamar Fisioterapi (html element: fisiotherapy-1-debet, fisiotherapy-1-kredit) :
	$fisiotherapy_1_debet = 0;
	$fisiotherapy_1_kredit = 0;
	/// Visite / Jasa Dokter Fisioterapi (html element: fisiotherapy-2-debet, fisiotherapy-2-kredit) :
	$fisiotherapy_2_debet = 0;
	$fisiotherapy_2_kredit = 0;
	/// Jasa / Tindakan Keperawatan Fisioterapi (html element: fisiotherapy-3-debet, fisiotherapy-3-kredit) :
	$fisiotherapy_3_debet = 0;
	$fisiotherapy_3_kredit = 0;
	$params = array();
	$params['noreg_pasien'] = $noreg_pasien;
	$consumer_service = new ServiceConsumer(
		$db,
		"get_fisioterapi_jurnal",
		$params,
		"fisiotherapy"
	);
	$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
	$content = $consumer_service->execute()->getContent();
	if ($content != null) {
		$fisiotherapy_2_kredit += $content[0]['jasa_dokter'];
		$fisiotherapy_3_kredit += $content[0]['pemeriksaan_reguler'];
	}
	/// Alat Kesehatan Fisioterapi (html element: fisiotherapy-4-debet, fisiotherapy-4-kredit) :
	$fisiotherapy_4_debet = 0;
	$fisiotherapy_4_kredit = 0;
	/// Lain-Lain Fisioterapi (html element: fisiotherapy-5-debet, fisiotherapy-5-kredit) :
	$fisiotherapy_5_debet = 0;
	$fisiotherapy_5_kredit = 0;
	/// Total Debet Kredit Fisioterapi (html element: fisiotherapy-0-debet, fisiotherapy-0-kredit) :
	$fisiotherapy_0_debet = $fisiotherapy_1_debet + $fisiotherapy_2_debet + $fisiotherapy_3_debet + $fisiotherapy_4_debet + $fisiotherapy_5_debet;
	$fisiotherapy_0_kredit = $fisiotherapy_1_kredit + $fisiotherapy_2_kredit + $fisiotherapy_3_kredit + $fisiotherapy_4_kredit + $fisiotherapy_5_kredit;
	
	/// Mendapatkan informasi jurnal di Kamar Operasi (html element: kamar_operasi)
	/// Sewa Kamar Kamar Operasi (html element: kamar_operasi-1-debet, kamar_operasi-1-kredit) :
	$kamar_operasi_1_debet = 0;
	$kamar_operasi_1_kredit = 0;
	$params = array();
	$params['noreg_pasien'] = $noreg_pasien;
	$consumer_service = new ServiceConsumer(
		$db,
		"get_sewa_kamar_jurnal",
		$params,
		"kamar_operasi"
	);
	$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
	$content = $consumer_service->execute()->getContent();
	if ($content != null)
		$kamar_operasi_1_kredit += $content[0]['sewa_kamar'];
	/// Jasa Operasi dan Anasthesi Kamar Operasi (html element: kamar_operasi-2-debet, kamar_operasi-2-kredit; kamar_operasi-5-debet, kamar_operasi-5-kredit) :
	$kamar_operasi_2_debet = 0;
	$kamar_operasi_2_kredit = 0;
	$kamar_operasi_5_debet = 0;
	$kamar_operasi_5_kredit = 0;
	$params = array();
	$params['noreg_pasien'] = $noreg_pasien;
	$consumer_service = new ServiceConsumer(
		$db,
		"get_ok_jurnal",
		$params,
		"kamar_operasi"
	);
	$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
	$content = $consumer_service->execute()->getContent();
	if ($content != null) {
		$jasa_operator_1 = $content[0]['jasa_operator_1'];
		$jasa_operator_2 = $content[0]['jasa_operator_2'];
		$jasa_ast_operator_1 = $content[0]['jasa_ast_operator_1'];
		$jasa_ast_operator_2 = $content[0]['jasa_ast_operator_2'];
		$jasa_anastesi = $content[0]['jasa_anastesi'];
		$jasa_ast_anastesi_1 = $content[0]['jasa_ast_anastesi_1'];
		$jasa_ast_anastesi_2 = $content[0]['jasa_ast_anastesi_2'];
		$jasa_team_ok = $content[0]['jasa_team_ok'];
		$jasa_bidan_1 = $content[0]['jasa_bidan_1'];
		$jasa_bidan_2 = $content[0]['jasa_bidan_2'];
		$jasa_perawat = $content[0]['jasa_perawat'];
		$harga_operasi = $content[0]['harga_operasi'];
		$jasa_oomloop_1 = $content[0]['jasa_oomploop_1'];
		$jasa_oomloop_2 = $content[0]['jasa_oomploop_2'];
		$jasa_instrument_1 = $content[0]['jasa_instrument_1'];
		$jasa_instrument_2 = $content[0]['jasa_instrument_2'];
		$rr = $content[0]['rr'];
		$kamar_operasi_2_kredit = $jasa_operator_1 + $jasa_operator_2 + $jasa_ast_operator_1 + $jasa_ast_operator_2 + $jasa_team_ok + $jasa_bidan_1 + $jasa_bidan_2 + $jasa_perawat + $harga_operasi + $jasa_oomloop_1 + $jasa_oomloop_2 + $jasa_instrument_1 + $jasa_instrument_2 + $rr;
		$kamar_operasi_5_kredit = $jasa_anastesi + $jasa_ast_anastesi_1 + $jasa_ast_anastesi_2;
	}
	/// Jasa / Tindakan Keperawatan Kamar Operasi (html element: kamar_operasi-3-debet, kamar_operasi-3-kredit) :
	$kamar_operasi_3_debet = 0;
	$kamar_operasi_3_kredit = 0;
	$params = array();
	$params['noreg_pasien'] = $noreg_pasien;
	$consumer_service = new ServiceConsumer(
		$db,
		"get_jasa_keperawatan_jurnal",
		$params,
		"kamar_operasi"
	);
	$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
	$content = $consumer_service->execute()->getContent();
	if ($content != null)
		$kamar_operasi_3_kredit += $content[0]['jasa_keperawatan'];
	/// Alat Kesehatan Kamar Operasi (html element: kamar_operasi-4-debet, kamar_operasi-4-kredit) :
	$kamar_operasi_4_debet = 0;
	$kamar_operasi_4_kredit = 0;
	$params = array();
	$params['noreg_pasien'] = $noreg_pasien;
	$consumer_service = new ServiceConsumer(
		$db,
		"get_alok_jurnal",
		$params,
		"kamar_operasi"
	);
	$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
	$content = $consumer_service->execute()->getContent();
	if ($content != null)
		$kamar_operasi_4_kredit += $content[0]['alok'];
	/// Lain-Lain Kamar Operasi (html element: kamar_operasi-6-debet, kamar_operasi-6-kredit) :
	$kamar_operasi_6_debet = 0;
	$kamar_operasi_6_kredit = 0;
	$params = array();
	$params['noreg_pasien'] = $noreg_pasien;
	$consumer_service = new ServiceConsumer(
		$db,
		"get_pendapatan_lain_jurnal",
		$params,
		"kamar_operasi"
	);
	$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
	$content = $consumer_service->execute()->getContent();
	if ($content != null)
		$kamar_operasi_6_kredit += $content[0]['pendapatan_lain'];
	/// Total Debet Kredit Kamar Operasi (html element: kamar_operasi-0-debet, kamar_operasi-0-kredit) :
	$kamar_operasi_0_debet = $kamar_operasi_1_debet + $kamar_operasi_2_debet + $kamar_operasi_3_debet + $kamar_operasi_4_debet + $kamar_operasi_5_debet + $kamar_operasi_6_debet;
	$kamar_operasi_0_kredit = $kamar_operasi_1_kredit + $kamar_operasi_2_kredit + $kamar_operasi_3_kredit + $kamar_operasi_4_kredit + $kamar_operasi_5_kredit + $kamar_operasi_6_kredit;
	
	/// Mendapatkan informasi jurnal di RPKK (sewa_kamar, konsul_dokter, konsultasi_dokter, visite_dokter, tindakan_keperawatan, alok), meliputi prototype rawat :
	/// 1) RPKK - Argopuro (rpkk_argopuro)
	/// 2) RPKK - Kelud (rpkk_kelud)
	/// 3) RPKK - Raung (rpkk_raung)
	/// 4) RPKK - Semeru (rpkk_semeru)
	/// 5) RPKK - Wilis (rpkk_wilis)
	$prototype_slugs = array(
		"rpkk_argopuro",
		"rpkk_kelud",
		"rpkk_raung",
		"rpkk_semeru",
		"rpkk_wilis",
		"ruang_bersalin",
		"ruang_pathologis"
	);
	/// Sewa Kamar RPKK (html element: rpkk-1-debet, rpkk-1-kredit) :
	$rpkk_1_debet = 0;
	$rpkk_1_kredit = 0;
	foreach ($prototype_slugs as $prototype_slug) {
		$params = array();
		$params['noreg_pasien'] = $noreg_pasien;
		$consumer_service = new ServiceConsumer(
			$db,
			"get_sewa_kamar_jurnal",
			$params,
			$prototype_slug
		);
		$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
		$content = $consumer_service->execute()->getContent();
		if ($content != null)
			$rpkk_1_kredit += $content[0]['sewa_kamar'];
	}
	/// Visite / Jasa Dokter RPKK (html element: rpkk-2-debet, rpkk-2-kredit) :
	$rpkk_2_debet = 0;
	$rpkk_2_kredit = 0;
	foreach ($prototype_slugs as $prototype_slug) {
		$params = array();
		$params['noreg_pasien'] = $noreg_pasien;
		$consumer_service = new ServiceConsumer(
			$db,
			"get_jasa_dokter_jurnal",
			$params,
			$prototype_slug
		);
		$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
		$content = $consumer_service->execute()->getContent();
		if ($content != null)
			$rpkk_2_kredit += $content[0]['jasa_dokter'];
	}
	/// Jasa / Tindakan Kebidanan RPKK (html element: rpkk-3-debet, rpkk-3-kredit) :
	$rpkk_3_debet = 0;
	$rpkk_3_kredit = 0;
	foreach ($prototype_slugs as $prototype_slug) {
		$params = array();
		$params['noreg_pasien'] = $noreg_pasien;
		$params['not_in_elements'] = array(
			" nama_tindakan NOT LIKE '%Persalinan Bidan%' "
		);
		$consumer_service = new ServiceConsumer(
			$db,
			"get_jasa_keperawatan_jurnal",
			$params,
			$prototype_slug
		);
		$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
		$content = $consumer_service->execute()->getContent();
		if ($content != null)
			$rpkk_3_kredit += $content[0]['jasa_keperawatan'];
	}
	/// Alat Kesehatan RPKK (html element: rpkk-4-debet, rpkk-4-kredit) :
	$rpkk_4_debet = 0;
	$rpkk_4_kredit = 0;
	foreach ($prototype_slugs as $prototype_slug) {
		$params = array();
		$params['noreg_pasien'] = $noreg_pasien;
		$consumer_service = new ServiceConsumer(
			$db,
			"get_alok_jurnal",
			$params,
			$prototype_slug
		);
		$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
		$content = $consumer_service->execute()->getContent();
		if ($content != null)
			$rpkk_4_kredit += $content[0]['alok'];
	}
	/// Jasa Persalinan RPKK (html element: rpkk-5-debet, rpkk-5-kredit) :
	$rpkk_5_debet = 0;
	$rpkk_5_kredit = 0;
	foreach ($prototype_slugs as $prototype_slug) {
		$params = array();
		$params['noreg_pasien'] = $noreg_pasien;
		$params['not_in_elements'] = array(
			" nama_tindakan LIKE '%Persalinan Bidan%' "
		);
		$consumer_service = new ServiceConsumer(
			$db,
			"get_jasa_keperawatan_jurnal",
			$params,
			$prototype_slug
		);
		$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
		$content = $consumer_service->execute()->getContent();
		if ($content != null)
			$rpkk_5_kredit += $content[0]['jasa_keperawatan'];
	}
	/// Lain-Lain RPKK (html element: rpkk-5-debet, rpkk-5-kredit) :
	$rpkk_6_debet = 0;
	$rpkk_6_kredit = 0;
	foreach ($prototype_slugs as $prototype_slug) {
		$params = array();
		$params['noreg_pasien'] = $noreg_pasien;
		$consumer_service = new ServiceConsumer(
			$db,
			"get_pendapatan_lain_jurnal",
			$params,
			$prototype_slug
		);
		$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
		$content = $consumer_service->execute()->getContent();
		if ($content != null)
			$rpkk_6_kredit += $content[0]['pendapatan_lain'];
	}
	/// Total Debet Kredit RPKK (html element: rpkk-0-debet, rpkk-0-kredit) :
	$rpkk_0_debet = $rpkk_1_debet + $rpkk_2_debet + $rpkk_3_debet + $rpkk_4_debet + $rpkk_5_debet + $rpkk_6_debet;
	$rpkk_0_kredit = $rpkk_1_kredit + $rpkk_2_kredit + $rpkk_3_kredit + $rpkk_4_kredit + $rpkk_5_kredit + $rpkk_6_kredit;
	
	/// Mendapatkan informasi jurnal di Laboratorium (pemeriksaan rutin, pemeriksaan khusus, blood tapping dan blood bag PMI) :
	$laboratory_1_kredit = 0;
	$laboratory_1_debet = 0;
	$laboratory_2_kredit = 0;
	$laboratory_2_debet = 0;
	$laboratory_3_kredit = 0;
	$laboratory_3_debet = 0;
	$params = array();
	$params['noreg_pasien'] = $noreg_pasien;
	$consumer_service = new ServiceConsumer(
		$db,
		"get_laboratorium_jurnal",
		$params,
		"laboratory"
	);
	$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
	$content = $consumer_service->execute()->getContent();
	if ($content != null) {
		$laboratory_1_kredit += $content[0]['pemeriksaan_reguler'];
		$laboratory_2_kredit += $content[0]['pemeriksaan_cito'];
	}
	$params = array();
	$params['noreg_pasien'] = $noreg_pasien;
	$params['criteria'] = array(
		"nama_tagihan"	=> "Darah PMI"
	);
	$params['criteria_conjunction'] = "AND";
	$consumer_service = new ServiceConsumer(
		$db,
		"get_tagihan_tambahan_jurnal",
		$params,
		"kasir"
	);
	$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
	$content = $consumer_service->execute()->getContent();
	if ($content != null) {
		$laboratory_3_kredit += $content[0]['nominal'];
	}
	/// Total Debet Kredit Laboratorium (html element: laboratory-0-debet, laboratory-0-kredit):
	$laboratory_0_debet = $laboratory_1_debet + $laboratory_2_debet + $laboratory_3_debet;
	$laboratory_0_kredit = $laboratory_1_kredit + $laboratory_2_kredit + $laboratory_3_kredit;
	
	/// Mendapatkan informasi jurnal di Depo Farmasi (penjualan rawat inap, penjualan rawat jalan umum, penjualan rawat jalan manfaat, penjualan rawat jalan kronis, penjualan rawat jalan inacbgs, penjualan rawat jalan tidak ditanggung, embalase)
	$depo_farmasi_1_kredit = 0;
	$depo_farmasi_1_debet = 0;
	$depo_farmasi_2_kredit = 0;
	$depo_farmasi_2_debet = 0;
	$depo_farmasi_3_kredit = 0;
	$depo_farmasi_3_debet = 0;
	$depo_farmasi_4_kredit = 0;
	$depo_farmasi_4_debet = 0;
	$depo_farmasi_5_kredit = 0;
	$depo_farmasi_5_debet = 0;
	$depo_farmasi_6_kredit = 0;
	$depo_farmasi_6_debet = 0;
	$depo_farmasi_7_kredit = 0;
	$depo_farmasi_7_debet = 0;
	$depo_farmasi_8_kredit = 0;
	$depo_farmasi_8_debet = 0;
	$params = array();
	$params['noreg_pasien'] = $noreg_pasien;
	$consumer_service = new ServiceConsumer(
		$db,
		"get_farmasi_jurnal",
		$params,
		"depo_farmasi_irja"
	);
	$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
	$content = $consumer_service->execute()->getContent();
	if ($content != null) {
		$depo_farmasi_1_kredit += ($content[0]['penjualan_rawat_inap_tanpa_jasa'] - $content[0]['retur_penjualan_rawat_inap']);
		$depo_farmasi_2_kredit += ($content[0]['penjualan_rawat_jalan_umum_tanpa_jasa'] - $content[0]['retur_penjualan_rawat_jalan_umum']);
		$depo_farmasi_3_kredit += ($content[0]['penjualan_rawat_jalan_manfaat_tanpa_jasa'] - $content[0]['retur_penjualan_rawat_jalan_manfaat']);
		$depo_farmasi_4_kredit += ($content[0]['penjualan_rawat_jalan_kronis_tanpa_jasa'] - $content[0]['retur_penjualan_rawat_jalan_kronis']);
		$depo_farmasi_5_kredit += ($content[0]['penjualan_rawat_jalan_inacbgs_tanpa_jasa'] - $content[0]['retur_penjualan_rawat_jalan_inacbgs']);
		$depo_farmasi_6_kredit += ($content[0]['penjualan_rawat_jalan_tidak_ditanggung_tanpa_jasa'] - $content[0]['retur_penjualan_rawat_jalan_tidak_ditanggung']);
		$depo_farmasi_7_kredit += ($content[0]['jasa_resep']);
	}
	$consumer_service = new ServiceConsumer(
		$db,
		"get_farmasi_jurnal",
		$params,
		"depo_farmasi_irna"
	);
	$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
	$content = $consumer_service->execute()->getContent();
	if ($content != null) {
		$depo_farmasi_1_kredit += ($content[0]['penjualan_rawat_inap_tanpa_jasa'] - $content[0]['retur_penjualan_rawat_inap']);
		$depo_farmasi_2_kredit += ($content[0]['penjualan_rawat_jalan_umum_tanpa_jasa'] - $content[0]['retur_penjualan_rawat_jalan_umum']);
		$depo_farmasi_3_kredit += ($content[0]['penjualan_rawat_jalan_manfaat_tanpa_jasa'] - $content[0]['retur_penjualan_rawat_jalan_manfaat']);
		$depo_farmasi_4_kredit += ($content[0]['penjualan_rawat_jalan_kronis_tanpa_jasa'] - $content[0]['retur_penjualan_rawat_jalan_kronis']);
		$depo_farmasi_5_kredit += ($content[0]['penjualan_rawat_jalan_inacbgs_tanpa_jasa'] - $content[0]['retur_penjualan_rawat_jalan_inacbgs']);
		$depo_farmasi_6_kredit += ($content[0]['penjualan_rawat_jalan_tidak_ditanggung_tanpa_jasa'] - $content[0]['retur_penjualan_rawat_jalan_tidak_ditanggung']);
		$depo_farmasi_7_kredit += ($content[0]['jasa_resep']);
	}
	$params = array();
	$params['noreg_pasien'] = $noreg_pasien;
	$params['criteria'] = array(
		"nama_tagihan"	=> "Penjualan Resep CITO"
	);
	$params['criteria_conjunction'] = "AND";
	$consumer_service = new ServiceConsumer(
		$db,
		"get_tagihan_tambahan_jurnal",
		$params,
		"kasir"
	);
	$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
	$content = $consumer_service->execute()->getContent();
	if ($content != null) {
		$depo_farmasi_8_kredit += $content[0]['nominal'];
	}
	/// Total Debet Kredit Depo Farmasi (html element: depo_farmasi-0-debet, depo_farmasi-0-kredit):
	$depo_farmasi_0_debet = $depo_farmasi_1_debet + $depo_farmasi_2_debet + $depo_farmasi_3_debet + $depo_farmasi_4_debet + $depo_farmasi_5_debet + $depo_farmasi_6_debet + $depo_farmasi_7_debet + $depo_farmasi_8_debet;
	$depo_farmasi_0_kredit = $depo_farmasi_1_kredit + $depo_farmasi_2_kredit + $depo_farmasi_3_kredit + $depo_farmasi_4_kredit + $depo_farmasi_5_kredit + $depo_farmasi_6_kredit + $depo_farmasi_7_kredit + $depo_farmasi_8_kredit;
	
	/// Mendapatkan informasi jurnal di Radiologi (x-ray, ct-scan, usg):
	$radiology_1_kredit = 0;
	$radiology_1_debet = 0;
	$radiology_2_kredit = 0;
	$radiology_2_debet = 0;
	$radiology_3_kredit = 0;
	$radiology_3_debet = 0;
	$radiology_4_kredit = 0;
	$radiology_4_debet = 0;
	$radiology_5_kredit = 0;
	$radiology_5_debet = 0;
	$params = array();
	$params['noreg_pasien'] = $noreg_pasien;
	$consumer_service = new ServiceConsumer(
		$db,
		"get_radiologi_jurnal",
		$params,
		"radiology"
	);
	$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
	$content = $consumer_service->execute()->getContent();
	if ($content != null) {
		$radiology_1_kredit += $content[0]['pemeriksaan_x_ray'];
		$radiology_2_kredit += $content[0]['pemeriksaan_ct_scan'];
		$radiology_3_kredit += $content[0]['pemeriksaan_usg_echo'];
		$radiology_4_kredit += $content[0]['konsul_radiologi'];
		$radiology_5_kredit += $content[0]['pemeriksaan_cito'];
	}
	$params = array();
	$params['noreg_pasien'] = $noreg_pasien;
	$params['criteria'] = array(
		"nama_tagihan"	=> "Pemeriksaan Radiologi CT Scan"
	);
	$params['criteria_conjunction'] = "AND";
	$consumer_service = new ServiceConsumer(
		$db,
		"get_tagihan_tambahan_jurnal",
		$params,
		"kasir"
	);
	$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
	$content = $consumer_service->execute()->getContent();
	if ($content != null) {
		$radiology_2_kredit += $content[0]['nominal'];
	}
	$params = array();
	$params['noreg_pasien'] = $noreg_pasien;
	$params['criteria'] = array(
		"nama_tagihan"	=> "Pemeriksaan Radiologi MRI"
	);
	$params['criteria_conjunction'] = "AND";
	$consumer_service = new ServiceConsumer(
		$db,
		"get_tagihan_tambahan_jurnal",
		$params,
		"kasir"
	);
	$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
	$content = $consumer_service->execute()->getContent();
	if ($content != null) {
		$radiology_5_kredit += $content[0]['nominal'];
	}
	/// Total Debet Kredit Radiologi (html element: radiology-0-debet, radiology-0-kredit):
	$radiology_0_debet = $radiology_1_debet + $radiology_2_debet + $radiology_3_debet + $radiology_4_debet + $radiology_5_debet;
	$radiology_0_kredit = $radiology_1_kredit + $radiology_2_kredit + $radiology_3_kredit + $radiology_4_kredit + $radiology_5_kredit;
	
	/// Mendapatkan informasi jurnal Lain-Lain (ambulan, administrasi, billing, lain-lain edc)
	$lain_lain_1_kredit = 0;
	$lain_lain_1_debet = 0;
	$lain_lain_2_kredit = 0;
	$lain_lain_2_debet = 0;
	$lain_lain_3_kredit = 0;
	$lain_lain_3_debet = 0;
	$lain_lain_4_kredit = 0;
	$lain_lain_4_debet = 0;
	$lain_lain_5_kredit = 0;
	$lain_lain_5_debet = 0;
	$lain_lain_6_kredit = 0;
	$lain_lain_6_debet = 0;
	$params = array();
	$params['noreg_pasien'] = $noreg_pasien;
	$consumer_service = new ServiceConsumer(
		$db,
		"get_ambulan_jurnal",
		$params,
		"ambulan"
	);
	$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
	$content = $consumer_service->execute()->getContent();
	if ($content != null) {
		$lain_lain_1_kredit += $content[0]['ambulan'];
	}
	$params = array();
	$params['noreg_pasien'] = $noreg_pasien;
	$params['criteria'] = array(
		"nama_tagihan"	=> "Ambulance"
	);
	$params['criteria_conjunction'] = "AND";
	$consumer_service = new ServiceConsumer(
		$db,
		"get_tagihan_tambahan_jurnal",
		$params,
		"kasir"
	);
	$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
	$content = $consumer_service->execute()->getContent();
	if ($content != null) {
		$lain_lain_1_kredit += $content[0]['nominal'];
	}
	$consumer_service = new ServiceConsumer(
		$db,
		"get_karcis_jurnal",
		$params,
		"registration"
	);
	$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
	$content = $consumer_service->execute()->getContent();
	if ($content != null) {
		$lain_lain_2_kredit += $content[0]['administrasi'];
		$lain_lain_3_kredit += $content[0]['billing_rawat_jalan'];
		$lain_lain_4_kredit += $content[0]['billing_rawat_inap'];
	}
	$params = array();
	$params['noreg_pasien'] = $noreg_pasien;
	$params['criteria'] = array(
		"nama_tagihan"	=> "ADMINISTRASI"
	);
	$params['criteria_conjunction'] = "AND";
	$consumer_service = new ServiceConsumer(
		$db,
		"get_tagihan_tambahan_jurnal",
		$params,
		"kasir"
	);
	$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
	$content = $consumer_service->execute()->getContent();
	if ($content != null) {
		$lain_lain_2_kredit += $content[0]['nominal'];
	}
	$params = array();
	$params['noreg_pasien'] = $noreg_pasien;
	$params['criteria'] = array(
		"nama_tagihan"	=> "ADMINISTRASI 1"
	);
	$params['criteria_conjunction'] = "AND";
	$consumer_service = new ServiceConsumer(
		$db,
		"get_tagihan_tambahan_jurnal",
		$params,
		"kasir"
	);
	$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
	$content = $consumer_service->execute()->getContent();
	if ($content != null) {
		$lain_lain_2_kredit += $content[0]['nominal'];
	}
	$params = array();
	$params['noreg_pasien'] = $noreg_pasien;
	$params['criteria'] = array(
		"nama_tagihan"	=> "ADMINISTRASI 2"
	);
	$params['criteria_conjunction'] = "AND";
	$consumer_service = new ServiceConsumer(
		$db,
		"get_tagihan_tambahan_jurnal",
		$params,
		"kasir"
	);
	$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
	$content = $consumer_service->execute()->getContent();
	if ($content != null) {
		$lain_lain_2_kredit += $content[0]['nominal'];
	}
	$params = array();
	$params['noreg_pasien'] = $noreg_pasien;
	$params['criteria'] = array(
		"nama_tagihan"	=> "ADMINISTRASI 3"
	);
	$params['criteria_conjunction'] = "AND";
	$consumer_service = new ServiceConsumer(
		$db,
		"get_tagihan_tambahan_jurnal",
		$params,
		"kasir"
	);
	$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
	$content = $consumer_service->execute()->getContent();
	if ($content != null) {
		$lain_lain_2_kredit += $content[0]['nominal'];
	}
	$params = array();
	$params['noreg_pasien'] = $noreg_pasien;
	$params['criteria'] = array(
		"nama_tagihan"	=> "ASKEP"
	);
	$params['criteria_conjunction'] = "AND";
	$consumer_service = new ServiceConsumer(
		$db,
		"get_tagihan_tambahan_jurnal",
		$params,
		"kasir"
	);
	$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
	$content = $consumer_service->execute()->getContent();
	if ($content != null) {
		$lain_lain_5_kredit += $content[0]['nominal'];
	}
	$params = array();
	$params['noreg_pasien'] = $noreg_pasien;
	$params['criteria'] = array(
		"nama_tagihan"	=> "Biaya Materai"
	);
	$params['criteria_conjunction'] = "AND";
	$consumer_service = new ServiceConsumer(
		$db,
		"get_tagihan_tambahan_jurnal",
		$params,
		"kasir"
	);
	$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
	$content = $consumer_service->execute()->getContent();
	if ($content != null) {
		$lain_lain_5_kredit += $content[0]['nominal'];
	}
	$params = array();
	$params['noreg_pasien'] = $noreg_pasien;
	$params['criteria'] = array(
		"nama_tagihan"	=> "SEWA ALAT DR%"
	);
	$params['criteria_conjunction'] = "AND";
	$consumer_service = new ServiceConsumer(
		$db,
		"get_tagihan_tambahan_jurnal",
		$params,
		"kasir"
	);
	$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
	$content = $consumer_service->execute()->getContent();
	if ($content != null) {
		$lain_lain_6_kredit += $content[0]['nominal'];
	}
	/// Total Debet Kredit Lain-Lain (html element: lain_lain-0-debet, lain_lain-0-kredit):
	$lain_lain_0_debet = $lain_lain_1_debet + $lain_lain_2_debet + $lain_lain_3_debet + $lain_lain_4_debet + $lain_lain_5_debet + $lain_lain_6_debet;
	$lain_lain_0_kredit = $lain_lain_1_kredit + $lain_lain_2_kredit + $lain_lain_3_kredit + $lain_lain_4_kredit + $lain_lain_5_kredit + $lain_lain_6_kredit;
		
	/// Mendapatkan informasi Pembayaran Pasien:
	$kasir_0_debet = 0;		// cash
	$kasir_0_kredit = 0;	// cash
	$kasir_1_debet = 0;		// total cash bank
	$kasir_1_kredit = 0;	// total cash bank
	$kasir_2_debet = 0;		// mandiri tunai
	$kasir_2_kredit = 0;	// mandiri tunai
	$kasir_3_debet = 0;		// mandiri edc
	$kasir_3_kredit = 0;	// mandiri edc
	$kasir_4_debet = 0;		// bri tunai
	$kasir_4_kredit = 0;	// bri tunai
	$kasir_5_debet = 0;		// bri edc
	$kasir_5_kredit = 0;	// bri edc
	$kasir_6_debet = 0;		// bca tunai
	$kasir_6_kredit = 0;	// bca tunai
	$kasir_7_debet = 0;		// bca edc
	$kasir_7_kredit = 0;	// bca edc
	$kasir_8_debet = 0;    	// tagihan rekanan lain non ptpn xii
	$kasir_8_kredit = 0;	// tagihan rekanan lain non ptpn xii
	$kasir_9_debet = 0;    	// tagihan rekanan lain ptpn xii
	$kasir_9_kredit = 0;	// tagihan rekanan lain ptpn xii
	$kasir_10_debet = 0;  	// tambahan biaya lain di luar kesepakatan mapping
	$kasir_10_kredit = 0;  	// tambahan biaya lain di luar kesepakatan mapping

	$perusahaan_ptpn_arr = array();
	$perusahaan_rows = $db->get_result("
		SELECT *
		FROM smis_rg_perusahaan
		WHERE prop NOT LIKE 'del' AND kode LIKE '134.%'
	");
	if ($perusahaan_rows != null)
		foreach ($perusahaan_rows as $pr) {
			$perusahaan_ptpn_arr[] = array(
				"id"		=> $pr->id,
				"kode"		=> $pr->kode,
				"debet"		=> 0,
				"kredit"	=> 0
			);
		}

	$asuransi_ptpn_arr = array();
	$asuransi_rows = $db->get_result("
		SELECT *
		FROM smis_rg_asuransi
		WHERE prop NOT LIKE 'del' AND kode LIKE '134.%'
	");
	if ($asuransi_rows != null)
		foreach ($asuransi_rows as $ar) {
			$asuransi_ptpn_arr[] = array(
				"id"		=> $ar->id,
				"kode"		=> $ar->kode,
				"debet"		=> 0,
				"kredit"	=> 0
			);
		}

	$perusahaan_non_ptpn_arr = array();
	$perusahaan_rows = $db->get_result("
		SELECT *
		FROM smis_rg_perusahaan
		WHERE prop NOT LIKE 'del' AND kode NOT LIKE '134.%'
	");
	if ($perusahaan_rows != null)
		foreach ($perusahaan_rows as $pr) {
			$perusahaan_non_ptpn_arr[] = array(
				"id"		=> $pr->id,
				"kode"		=> $pr->kode,
				"debet"		=> 0,
				"kredit"	=> 0
			);
		}

	$asuransi_non_ptpn_arr = array();
	$asuransi_rows = $db->get_result("
		SELECT *
		FROM smis_rg_asuransi
		WHERE prop NOT LIKE 'del' AND kode NOT LIKE '134.%'
	");
	if ($asuransi_rows != null)
		foreach ($asuransi_rows as $ar) {
			$asuransi_non_ptpn_arr[] = array(
				"id"		=> $ar->id,
				"kode"		=> $ar->kode,
				"debet"		=> 0,
				"kredit"	=> 0
			);
		}

	$params = array();
	$params['noreg_pasien'] = $noreg_pasien;
	$consumer_service = new ServiceConsumer(
		$db,
		"get_pembayaran_jurnal",
		$params,
		"kasir"
	);
	$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
	$content = $consumer_service->execute()->getContent();
	if ($content != null) {
		$kasir_1_debet += $content[0]['tunai'];
		$kasir_2_debet += $content[0]['mandiri_tunai'];
		$kasir_3_debet += $content[0]['mandiri_edc'];
		$kasir_4_debet += $content[0]['bri_tunai'];
		$kasir_5_debet += $content[0]['bri_edc'];
		$kasir_6_debet += $content[0]['bca_tunai'];
		$kasir_7_debet += $content[0]['bca_edc'];
		
		$tagihan_is_done = false;
		$nominal_tagihan = $content[0]['tagihan'];
		if ($nominal_tagihan > 0) {
			for ($i = 0; $i < count($perusahaan_ptpn_arr); $i++) {
				if (!$tagihan_is_done && $id_perusahaan == $perusahaan_ptpn_arr[$i]['id']) {
					$perusahaan_ptpn_arr[$i]['debet'] = $nominal_tagihan;
					$tagihan_is_done = true;
					break;
				}
			}
			if (!$tagihan_is_done) {
				for ($i = 0; $i < count($asuransi_ptpn_arr); $i++) {
					if (!$tagihan_is_done && $id_asuransi == $asuransi_ptpn_arr[$i]['id']) {
						$asuransi_ptpn_arr[$i]['debet'] = $nominal_tagihan;
						$tagihan_is_done = true;
						break;
					}
				}
				if (!$tagihan_is_done) {
					for ($i = 0; $i < count($perusahaan_non_ptpn_arr); $i++) {
						if (!$tagihan_is_done && $id_perusahaan == $perusahaan_non_ptpn_arr[$i]['id']) {
							$perusahaan_non_ptpn_arr[$i]['debet'] = $nominal_tagihan;
							$tagihan_is_done = true;
							break;
						}
					}
					if (!$tagihan_is_done) {
						for ($i = 0; $i < count($asuransi_non_ptpn_arr); $i++) {
							if (!$tagihan_is_done && $id_asuransi == $asuransi_non_ptpn_arr[$i]['id']) {
								$asuransi_non_ptpn_arr[$i]['debet'] = $nominal_tagihan;
								$tagihan_is_done = true;
								break;
							}
						}
						if ($jenis_pasien != "umum" && $jenis_pasien != "rekanan" && $jenis_pasien != "bank") {
							if ($jenis_pasien == "bpjs_ptpn_12" || $jenis_pasien == "pensiunan_ptpn_12") {
								$kasir_9_debet += $nominal_tagihan;
								$tagihan_is_done = true;		
							} else {
								$kasir_8_debet += $nominal_tagihan;
								$tagihan_is_done = true;		
							}					
						} else {
							$kasir_8_debet += $nominal_tagihan;
							$tagihan_is_done = true;
						}
					}
				}
			}
		}
	}

	/// debit-kredit tagihan tambahan non-kesepakatan mapping jurnal 05:
	$params = array();
	$params['noreg_pasien'] = $noreg_pasien;
	$params['criteria'] = array(
		" 1 AND nama_tagihan NOT "	=> "ASKEP",
		" AND 2 AND nama_tagihan NOT "	=> "ADMINISTRASI 1",
		" AND 3 AND nama_tagihan NOT "	=> "ADMINISTRASI 2",
		" AND 4 AND nama_tagihan NOT "	=> "ADMINISTRASI 3",
		" AND 5 AND nama_tagihan NOT "	=> "Biaya Materai",
		" AND 6 AND nama_tagihan NOT "	=> "Penjualan Resep CITO",
		" AND 7 AND nama_tagihan NOT "	=> "Darah PMI",
		" AND 8 AND nama_tagihan NOT "  => "SEWA ALAT DR%"
	);
	$params['criteria_conjunction'] = "AND";
	$consumer_service = new ServiceConsumer(
		$db,
		"get_tagihan_tambahan_jurnal",
		$params,
		"kasir"
	);
	$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
	$content = $consumer_service->execute()->getContent();
	if ($content != null) {
		$kasir_10_kredit += $content[0]['nominal'];
	}
	
	/* 	
	/// Mendapatkan informasi jurnal Jasa Pelayanan:
	$jaspel_kredit_0_debet = 0;		// jaspel total kredit
	$jaspel_kredit_0_kredit = 0;	// jaspel total kredit
	$jaspel_debet_0_debet = 0;		// jaspel total debet
	$jaspel_debet_0_kredit = 0;		// jaspel total debet
	$jaspel_debet_1_debet = 0;		// jaspel rpa debet
	$jaspel_debet_1_kredit = 0;		// jaspel rpa debet
	$jaspel_debet_2_debet = 0;		// jaspel rpb debet
	$jaspel_debet_2_kredit = 0;		// jaspel rpb debet
	$jaspel_debet_3_debet = 0;		// jaspel icu/rpo debet
	$jaspel_debet_3_kredit = 0;		// jaspel icu/rpo debet
	$jaspel_debet_4_debet = 0;		// jaspel rpc debet
	$jaspel_debet_4_kredit = 0;		// jaspel rpc debet
	$jaspel_debet_5_debet = 0;		// jaspel rpd debet
	$jaspel_debet_5_kredit = 0;		// jaspel rpd debet
	$jaspel_debet_6_debet = 0;		// jaspel rpe debet
	$jaspel_debet_6_kredit = 0;		// jaspel rpe debet
	$jaspel_debet_7_debet = 0;		// jaspel instalasi_gawat_darurat debet
	$jaspel_debet_7_kredit = 0;		// jaspel instalasi_gawat_darurat debet
	$jaspel_debet_8_debet = 0;		// jaspel poliumum debet
	$jaspel_debet_8_kredit = 0;		// jaspel poliumum debet
	$jaspel_debet_9_debet = 0;		// jaspel poligigi debet
	$jaspel_debet_9_kredit = 0;		// jaspel poligigi debet
	$jaspel_debet_10_debet = 0;		// jaspel poli_spesialis_center / poli_anak debet
	$jaspel_debet_10_kredit = 0;	// jaspel poli_spesialis_center / poli_anak debet
	$jaspel_debet_11_debet = 0;		// jaspel fisiotherapy debet
	$jaspel_debet_11_kredit = 0;	// jaspel fisiotherapy debet
	$jaspel_debet_12_debet = 0;		// jaspel poli_spesialis_center / poli_obgyn debet
	$jaspel_debet_12_kredit = 0;	// jaspel poli_spesialis_center / poli_obgyn debet
	$jaspel_debet_13_debet = 0;		// jaspel poli_spesialis_center / poli_bedah debet
	$jaspel_debet_13_kredit = 0;	// jaspel poli_spesialis_center / poli_bedah debet
	$jaspel_debet_14_debet = 0;		// jaspel poli_spesialis_center / poli_mata debet
	$jaspel_debet_14_kredit = 0;	// jaspel poli_spesialis_center / poli_mata debet
	$jaspel_debet_15_debet = 0;		// jaspel poli_spesialis_center / poli_tht debet
	$jaspel_debet_15_kredit = 0;	// jaspel poli_spesialis_center / poli_tht debet
	$jaspel_debet_16_debet = 0;		// jaspel poli_spesialis_center / poli_kulit_dan_kelamin debet
	$jaspel_debet_16_kredit = 0;	// jaspel poli_spesialis_center / poli_kulit_dan_kelamin debet
	$jaspel_debet_17_debet = 0;		// jaspel poli_spesialis_center / poli_syaraf debet
	$jaspel_debet_17_kredit = 0;	// jaspel poli_spesialis_center / poli_syaraf debet
	$jaspel_debet_18_debet = 0;		// jaspel poli_spesialis_center / poli_ortopedi debet
	$jaspel_debet_18_kredit = 0;	// jaspel poli_spesialis_center / poli_ortopedi debet
	$jaspel_debet_19_debet = 0;		// jaspel poli_spesialis_center / poli jantung debet
	$jaspel_debet_19_kredit = 0;	// jaspel poli_spesialis_center / poli jantung debet
	$jaspel_debet_20_debet = 0;		// jaspel poli_spesialis_center / poli paru debet
	$jaspel_debet_20_kredit = 0;	// jaspel poli_spesialis_center / poli paru debet
	$jaspel_debet_21_debet = 0;		// jaspel gizi debet
	$jaspel_debet_21_kredit = 0;	// jaspel gizi debet
	$jaspel_debet_22_debet = 0;		// jaspel poli_spesialis_center / poli penyakit dalam debet
	$jaspel_debet_22_kredit = 0;	// jaspel poli_spesialis_center / poli penyakit dalam debet
	$jaspel_debet_23_debet = 0;		// jaspel hemodialisa debet
	$jaspel_debet_23_kredit = 0;	// jaspel hemodialisa debet
	$jaspel_debet_24_debet = 0;		// jaspel kamar operasi debet
	$jaspel_debet_24_kredit = 0;	// jaspel kamar operasi debet
	$jaspel_debet_25_debet = 0;		// jaspel rpkk debet
	$jaspel_debet_25_kredit = 0;	// jaspel rpkk debet
	$jaspel_debet_26_debet = 0;		// jaspel laboratory debet
	$jaspel_debet_26_kredit = 0;	// jaspel laboratory debet
	$jaspel_debet_27_debet = 0;		// jaspel depo_farmasi debet
	$jaspel_debet_27_kredit = 0;	// jaspel depo_farmasi debet
	$jaspel_debet_28_debet = 0;		// jaspel radiology debet
	$jaspel_debet_28_kredit = 0;	// jaspel radiology debet
	
	/// Jasa Pelayanan RPA:
	/// 1) RPA - Argopuro (rpa_argopuro)
	/// 2) RPA - Bromo (rpa_bromo)
	/// 3) RPA - Bromo Baru (rpa_bromobaru)
	/// 4) RPA - Kelud (rpa_kelud)
	$prototype_slugs = array(
		"rpa_argopuro",
		"rpa_bromo",
		"rpa_bromobaru",
		"rpa_kelud"
	);
	$params = array();
	$params['noreg_pasien'] = $noreg_pasien;
	foreach ($prototype_slugs as $prototype_slug) {
		$consumer_service = new ServiceConsumer(
			$db,
			"get_jaspel_keperawatan_jurnal",
			$params,
			$prototype_slug
		);
		$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
		$content = $consumer_service->execute()->getContent();
		if ($content != null)
			$jaspel_debet_1_debet += $content[0]['jaspel'];
	}
	
	/// Jasa Pelayanan RPB:
	/// 1) RPB - Argopuro (rpb_argopuro)
	/// 2) RPB - Ijen (rpb_ijen)
	/// 3) RPB - Raung (rpb_raung)
	$prototype_slugs = array(
		"rpb_argopuro",
		"rpb_ijen",
		"rpb_raung"
	);
	$params = array();
	$params['noreg_pasien'] = $noreg_pasien;
	foreach ($prototype_slugs as $prototype_slug) {
		$consumer_service = new ServiceConsumer(
			$db,
			"get_jaspel_keperawatan_jurnal",
			$params,
			$prototype_slug
		);
		$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
		$content = $consumer_service->execute()->getContent();
		if ($content != null)
			$jaspel_debet_2_debet += $content[0]['jaspel'];
	}
	/// Jasa Pelayanan ICU:
	$prototype_slugs = array(
		"icu"
	);
	$params = array();
	$params['noreg_pasien'] = $noreg_pasien;
	foreach ($prototype_slugs as $prototype_slug) {
		$consumer_service = new ServiceConsumer(
			$db,
			"get_jaspel_keperawatan_jurnal",
			$params,
			$prototype_slug
		);
		$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
		$content = $consumer_service->execute()->getContent();
		if ($content != null)
			$jaspel_debet_3_debet += $content[0]['jaspel'];
	}
	/// Jasa Pelayanan RPC:
	$prototype_slugs = array(
	);
	$params = array();
	$params['noreg_pasien'] = $noreg_pasien;
	foreach ($prototype_slugs as $prototype_slug) {
		$consumer_service = new ServiceConsumer(
			$db,
			"get_jaspel_keperawatan_jurnal",
			$params,
			$prototype_slug
		);
		$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
		$content = $consumer_service->execute()->getContent();
		if ($content != null)
			$jaspel_debet_4_debet += $content[0]['jaspel'];
	}
	/// Jasa Pelayanan RPD:
	$prototype_slugs = array(
	);
	$params = array();
	$params['noreg_pasien'] = $noreg_pasien;
	foreach ($prototype_slugs as $prototype_slug) {
		$consumer_service = new ServiceConsumer(
			$db,
			"get_jaspel_keperawatan_jurnal",
			$params,
			$prototype_slug
		);
		$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
		$content = $consumer_service->execute()->getContent();
		if ($content != null)
			$jaspel_debet_5_debet += $content[0]['jaspel'];
	}
	/// Jasa Pelayanan RPE:
	$prototype_slugs = array(
	);
	$params = array();
	$params['noreg_pasien'] = $noreg_pasien;
	foreach ($prototype_slugs as $prototype_slug) {
		$consumer_service = new ServiceConsumer(
			$db,
			"get_jaspel_keperawatan_jurnal",
			$params,
			$prototype_slug
		);
		$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
		$content = $consumer_service->execute()->getContent();
		if ($content != null)
			$jaspel_debet_6_debet += $content[0]['jaspel'];
	}
	/// IGD
	$prototype_slugs = array(
		"instalasi_gawat_darurat"
	);
	$params = array();
	$params['noreg_pasien'] = $noreg_pasien;
	foreach ($prototype_slugs as $prototype_slug) {
		$consumer_service = new ServiceConsumer(
			$db,
			"get_jaspel_keperawatan_jurnal",
			$params,
			$prototype_slug
		);
		$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
		$content = $consumer_service->execute()->getContent();
		if ($content != null)
			$jaspel_debet_7_debet += $content[0]['jaspel'];
	}
	/// Poli Umum
	$prototype_slugs = array(
		"poliumum"
	);
	$params = array();
	$params['noreg_pasien'] = $noreg_pasien;
	foreach ($prototype_slugs as $prototype_slug) {
		$consumer_service = new ServiceConsumer(
			$db,
			"get_jaspel_keperawatan_jurnal",
			$params,
			$prototype_slug
		);
		$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
		$content = $consumer_service->execute()->getContent();
		if ($content != null)
			$jaspel_debet_8_debet += $content[0]['jaspel'];
	}
	/// Poli Gigi
	$prototype_slugs = array(
		"poligigi"
	);
	$params = array();
	$params['noreg_pasien'] = $noreg_pasien;
	foreach ($prototype_slugs as $prototype_slug) {
		$consumer_service = new ServiceConsumer(
			$db,
			"get_jaspel_keperawatan_jurnal",
			$params,
			$prototype_slug
		);
		$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
		$content = $consumer_service->execute()->getContent();
		if ($content != null)
			$jaspel_debet_9_debet += $content[0]['jaspel'];
	}
	/// Poli Anak:
	$jaspel_debet_10_debet = 0;
	// Fisioterapi:
	$params = array();
	$params['noreg_pasien'] = $noreg_pasien;
	$consumer_service = new ServiceConsumer(
		$db,
		"get_jaspel_fisioterapi_jurnal",
		$params,
		"fisiotherapy"
	);
	$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
	$content = $consumer_service->execute()->getContent();
	if ($content != null)
		$jaspel_debet_11_debet += $content[0]['jaspel'];
	/// Poli Obgyn:
	$jaspel_debet_12_debet = 0;
	/// Poli Bedah:
	$jaspel_debet_13_debet = 0;
	/// Poli Mata:
	$jaspel_debet_14_debet = 0;
	/// Poli THT:
	$jaspel_debet_15_debet = 0;
	/// Poli Kulit dan Kelamin:
	$jaspel_debet_16_debet = 0;
	/// Poli Syaraf:
	$jaspel_debet_17_debet = 0;
	/// Poli Ortopedi:
	$jaspel_debet_18_debet = 0;
	/// Poli Jantung:
	$jaspel_debet_19_debet = 0;
	/// Poli Paru:
	$jaspel_debet_20_debet = 0;
	// Gizi:
	$params = array();
	$params['noreg_pasien'] = $noreg_pasien;
	$consumer_service = new ServiceConsumer(
		$db,
		"get_jaspel_keperawatan_jurnal",
		$params,
		"gizi"
	);
	$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
	$content = $consumer_service->execute()->getContent();
	if ($content != null)
		$jaspel_debet_21_debet += $content[0]['jaspel'];
	/// Poli Penyakit Dalam:
	$jaspel_debet_22_debet = 0;
	/// Hemodialisa:
	$prototype_slugs = array(
		"hemodialisa"
	);
	$params = array();
	$params['noreg_pasien'] = $noreg_pasien;
	foreach ($prototype_slugs as $prototype_slug) {
		$consumer_service = new ServiceConsumer(
			$db,
			"get_jaspel_keperawatan_jurnal",
			$params,
			$prototype_slug
		);
		$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
		$content = $consumer_service->execute()->getContent();
		if ($content != null)
			$jaspel_debet_23_debet += $content[0]['jaspel'];
	}
	/// Kamar Operasi:
	$params = array();
	$params['noreg_pasien'] = $noreg_pasien;
	$consumer_service = new ServiceConsumer(
		$db,
		"get_ok_jurnal",
		$params,
		"kamar_operasi"
	);
	$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
	$content = $consumer_service->execute()->getContent();
	if ($content != null) {
		$jasa_operator_1 = 0; 		// $content[0]['jasa_operator_1']; // Jasa Anestesi dan Operator di Kamar Operasi
		$jasa_operator_2 = 0;		// $content[0]['jasa_operator_2']; // Jasa Anestesi dan Operator di Kamar Operasi
		$jasa_ast_operator_1 = 0; 	// $content[0]['jasa_ast_operator_1']; // Jasa Anestesi dan Operator di Kamar Operasi
		$jasa_ast_operator_2 = $content[0]['jasa_ast_operator_2'];
		$jasa_anastesi = 0; 		// $content[0]['jasa_anastesi'];  // Jasa Anestesi dan Operator di Kamar Operasi
		$jasa_ast_anastesi_1 = $content[0]['jasa_ast_anastesi_1'];
		$jasa_ast_anastesi_2 = $content[0]['jasa_ast_anastesi_2'];
		$jasa_team_ok = 0; // $content[0]['jasa_team_ok']; // Jasa Anestesi dan Operator di Kamar Operasi
		$jasa_bidan_1 = 0; // $content[0]['jasa_bidan_1']; // Jasa Anestesi dan Operator di Kamar Operasi
		$jasa_bidan_2 = 0; // $content[0]['jasa_bidan_2']; // Jasa Anestesi dan Operator di Kamar Operasi
		$jasa_perawat = 0; // $content[0]['jasa_perawat']; // Jasa Anestesi dan Operator di Kamar Operasi
		$harga_operasi = 0; // $content[0]['harga_operasi']; // Jasa Anestesi dan Operator di Kamar Operasi
		$jasa_oomloop_1 = $content[0]['jasa_oomloop_1'];
		$jasa_oomloop_2 = $content[0]['jasa_oomloop_2'];
		$jasa_instrument_1 = $content[0]['jasa_instrument_1'];
		$jasa_instrument_2 = $content[0]['jasa_instrument_2'];
		$rr = 0;// $content[0]['rr']; // Jasa Anestesi dan Operator di Kamar Operasi
		$jaspel_debet_24_debet += $jasa_operator_1 + $jasa_operator_2 + $jasa_ast_operator_1 + $jasa_ast_operator_2 + $jasa_team_ok + $jasa_bidan_1 + $jasa_bidan_2 + $jasa_perawat + $harga_operasi + $jasa_oomloop_1 + $jasa_oomloop_2 + $jasa_instrument_1 + $jasa_instrument_2 + $rr;
		$jaspel_debet_24_debet += $jasa_anastesi + $jasa_ast_anastesi_1 + $jasa_ast_anastesi_2;
	}
	$params = array();
	$params['noreg_pasien'] = $noreg_pasien;
	$consumer_service = new ServiceConsumer(
		$db,
		"get_jaspel_keperawatan_jurnal",
		$params,
		"kamar_operasi"
	);
	$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
	$content = $consumer_service->execute()->getContent();
	if ($content != null)
		$jaspel_debet_24_debet += $content[0]['jaspel'];
	/// RPKK:
	$prototype_slugs = array(
		"rpkk_argopuro",
		"rpkk_kelud",
		"rpkk_raung",
		"rpkk_semeru",
		"rpkk_wilis",
		"ruang_bersalin",
		"ruang_pathologis"
	);
	$params = array();
	$params['noreg_pasien'] = $noreg_pasien;
	foreach ($prototype_slugs as $prototype_slug) {
		$consumer_service = new ServiceConsumer(
			$db,
			"get_jaspel_keperawatan_jurnal",
			$params,
			$prototype_slug
		);
		$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
		$content = $consumer_service->execute()->getContent();
		if ($content != null)
			$jaspel_debet_25_debet += $content[0]['jaspel'];
	}
	/// Laboratorium:
	$params = array();
	$params['noreg_pasien'] = $noreg_pasien;
	$consumer_service = new ServiceConsumer(
		$db,
		"get_jaspel_laboratorium_jurnal",
		$params,
		"laboratory"
	);
	$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
	$content = $consumer_service->execute()->getContent();
	if ($content != null)
		$jaspel_debet_26_debet += $content[0]['jaspel'];
	/// Depo Farmasi:
	$jaspel_debet_27_debet = 0;
	/// Radiologi:
	$params = array();
	$params['noreg_pasien'] = $noreg_pasien;
	$consumer_service = new ServiceConsumer(
		$db,
		"get_jaspel_radiologi_jurnal",
		$params,
		"radiology"
	);
	$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
	$content = $consumer_service->execute()->getContent();
	if ($content != null)
		$jaspel_debet_28_debet += $content[0]['jaspel'];
	/// Jasa Pelayanan Total:
	$jaspel_debet_0_debet = $jaspel_debet_1_debet + $jaspel_debet_2_debet + $jaspel_debet_3_debet + $jaspel_debet_4_debet + $jaspel_debet_5_debet + $jaspel_debet_6_debet + $jaspel_debet_7_debet + $jaspel_debet_8_debet + $jaspel_debet_9_debet + $jaspel_debet_10_debet + $jaspel_debet_11_debet + $jaspel_debet_12_debet + $jaspel_debet_13_debet + $jaspel_debet_14_debet + $jaspel_debet_15_debet + $jaspel_debet_16_debet + $jaspel_debet_17_debet + $jaspel_debet_18_debet + $jaspel_debet_19_debet + $jaspel_debet_20_debet + $jaspel_debet_21_debet + $jaspel_debet_22_debet + $jaspel_debet_23_debet + $jaspel_debet_24_debet + $jaspel_debet_25_debet + $jaspel_debet_26_debet + $jaspel_debet_27_debet + $jaspel_debet_28_debet;
	$jaspel_kredit_0_kredit = $jaspel_debet_0_debet;
	*/
			
	$data = array();
	$data['noreg_pasien'] = ArrayAdapter::format("only-digit6", $noreg_pasien);
	$data['nrm_pasien'] = ArrayAdapter::format("only-digit6", $nrm_pasien);
	$data['nama_pasien'] = ArrayAdapter::format("unslug", $nama_pasien);
	$data['tanggal_daftar_pasien'] = $tanggal_daftar_pasien;
	$data['jenis_pasien'] = ArrayAdapter::format("unslug", $jenis_pasien);
	$data['nama_perusahaan'] = $nama_perusahaan;
	$data['nama_asuransi'] = $nama_asuransi;
	$data['rpa_0_debet'] = $rpa_0_debet;
	$data['rpa_0_kredit'] = $rpa_0_kredit;
	$data['rpa_1_debet'] = $rpa_1_debet;
	$data['rpa_1_kredit'] = $rpa_1_kredit;
	$data['rpa_2_debet'] = $rpa_2_debet;
	$data['rpa_2_kredit'] = $rpa_2_kredit;
	$data['rpa_3_debet'] = $rpa_3_debet;
	$data['rpa_3_kredit'] = $rpa_3_kredit;
	$data['rpa_4_debet'] = $rpa_4_debet;
	$data['rpa_4_kredit'] = $rpa_4_kredit;
	$data['rpa_5_debet'] = $rpa_5_debet;
	$data['rpa_5_kredit'] = $rpa_5_kredit;
	$data['rpb_0_debet'] = $rpb_0_debet;
	$data['rpb_0_kredit'] = $rpb_0_kredit;
	$data['rpb_1_debet'] = $rpb_1_debet;
	$data['rpb_1_kredit'] = $rpb_1_kredit;
	$data['rpb_2_debet'] = $rpb_2_debet;
	$data['rpb_2_kredit'] = $rpb_2_kredit;
	$data['rpb_3_debet'] = $rpb_3_debet;
	$data['rpb_3_kredit'] = $rpb_3_kredit;
	$data['rpb_4_debet'] = $rpb_4_debet;
	$data['rpb_4_kredit'] = $rpb_4_kredit;
	$data['rpb_5_debet'] = $rpb_5_debet;
	$data['rpb_5_kredit'] = $rpb_5_kredit;
	$data['icu_0_debet'] = $icu_0_debet;
	$data['icu_0_kredit'] = $icu_0_kredit;
	$data['icu_1_debet'] = $icu_1_debet;
	$data['icu_1_kredit'] = $icu_1_kredit;
	$data['icu_2_debet'] = $icu_2_debet;
	$data['icu_2_kredit'] = $icu_2_kredit;
	$data['icu_3_debet'] = $icu_3_debet;
	$data['icu_3_kredit'] = $icu_3_kredit;
	$data['icu_4_debet'] = $icu_4_debet;
	$data['icu_4_kredit'] = $icu_4_kredit;
	$data['icu_5_debet'] = $icu_5_debet;
	$data['icu_5_kredit'] = $icu_5_kredit;
	$data['rpc_0_debet'] = $rpc_0_debet;
	$data['rpc_0_kredit'] = $rpc_0_kredit;
	$data['rpc_1_debet'] = $rpc_1_debet;
	$data['rpc_1_kredit'] = $rpc_1_kredit;
	$data['rpc_2_debet'] = $rpc_2_debet;
	$data['rpc_2_kredit'] = $rpc_2_kredit;
	$data['rpc_3_debet'] = $rpc_3_debet;
	$data['rpc_3_kredit'] = $rpc_3_kredit;
	$data['rpc_4_debet'] = $rpc_4_debet;
	$data['rpc_4_kredit'] = $rpc_4_kredit;
	$data['rpc_5_debet'] = $rpc_5_debet;
	$data['rpc_5_kredit'] = $rpc_5_kredit;
	$data['rpd_0_debet'] = $rpd_0_debet;
	$data['rpd_0_kredit'] = $rpd_0_kredit;
	$data['rpd_1_debet'] = $rpd_1_debet;
	$data['rpd_1_kredit'] = $rpd_1_kredit;
	$data['rpd_2_debet'] = $rpd_2_debet;
	$data['rpd_2_kredit'] = $rpd_2_kredit;
	$data['rpd_3_debet'] = $rpd_3_debet;
	$data['rpd_3_kredit'] = $rpd_3_kredit;
	$data['rpd_4_debet'] = $rpd_4_debet;
	$data['rpd_4_kredit'] = $rpd_4_kredit;
	$data['rpd_5_debet'] = $rpd_5_debet;
	$data['rpd_5_kredit'] = $rpd_5_kredit;
	$data['rpe_0_debet'] = $rpe_0_debet;
	$data['rpe_0_kredit'] = $rpe_0_kredit;
	$data['rpe_1_debet'] = $rpe_1_debet;
	$data['rpe_1_kredit'] = $rpe_1_kredit;
	$data['rpe_2_debet'] = $rpe_2_debet;
	$data['rpe_2_kredit'] = $rpe_2_kredit;
	$data['rpe_3_debet'] = $rpe_3_debet;
	$data['rpe_3_kredit'] = $rpe_3_kredit;
	$data['rpe_4_debet'] = $rpe_4_debet;
	$data['rpe_4_kredit'] = $rpe_4_kredit;
	$data['rpe_5_debet'] = $rpe_5_debet;
	$data['rpe_5_kredit'] = $rpe_5_kredit;
	$data['rpkk_0_debet'] = $rpkk_0_debet;
	$data['rpkk_0_kredit'] = $rpkk_0_kredit;
	$data['rpkk_1_debet'] = $rpkk_1_debet;
	$data['rpkk_1_kredit'] = $rpkk_1_kredit;
	$data['rpkk_2_debet'] = $rpkk_2_debet;
	$data['rpkk_2_kredit'] = $rpkk_2_kredit;
	$data['rpkk_3_debet'] = $rpkk_3_debet;
	$data['rpkk_3_kredit'] = $rpkk_3_kredit;
	$data['rpkk_4_debet'] = $rpkk_4_debet;
	$data['rpkk_4_kredit'] = $rpkk_4_kredit;
	$data['rpkk_5_debet'] = $rpkk_5_debet;
	$data['rpkk_5_kredit'] = $rpkk_5_kredit;
	$data['instalasi_gawat_darurat_0_debet'] = $instalasi_gawat_darurat_0_debet;
	$data['instalasi_gawat_darurat_0_kredit'] = $instalasi_gawat_darurat_0_kredit;
	$data['instalasi_gawat_darurat_1_debet'] = $instalasi_gawat_darurat_1_debet;
	$data['instalasi_gawat_darurat_1_kredit'] = $instalasi_gawat_darurat_1_kredit;
	$data['instalasi_gawat_darurat_2_debet'] = $instalasi_gawat_darurat_2_debet;
	$data['instalasi_gawat_darurat_2_kredit'] = $instalasi_gawat_darurat_2_kredit;
	$data['instalasi_gawat_darurat_3_debet'] = $instalasi_gawat_darurat_3_debet;
	$data['instalasi_gawat_darurat_3_kredit'] = $instalasi_gawat_darurat_3_kredit;
	$data['instalasi_gawat_darurat_4_debet'] = $instalasi_gawat_darurat_4_debet;
	$data['instalasi_gawat_darurat_4_kredit'] = $instalasi_gawat_darurat_4_kredit;
	$data['instalasi_gawat_darurat_5_debet'] = $instalasi_gawat_darurat_5_debet;
	$data['instalasi_gawat_darurat_5_kredit'] = $instalasi_gawat_darurat_5_kredit;
	$data['poliumum_0_debet'] = $poliumum_0_debet;
	$data['poliumum_0_kredit'] = $poliumum_0_kredit;
	$data['poliumum_1_debet'] = $poliumum_1_debet;
	$data['poliumum_1_kredit'] = $poliumum_1_kredit;
	$data['poliumum_2_debet'] = $poliumum_2_debet;
	$data['poliumum_2_kredit'] = $poliumum_2_kredit;
	$data['poliumum_3_debet'] = $poliumum_3_debet;
	$data['poliumum_3_kredit'] = $poliumum_3_kredit;
	$data['poliumum_4_debet'] = $poliumum_4_debet;
	$data['poliumum_4_kredit'] = $poliumum_4_kredit;
	$data['poliumum_5_debet'] = $poliumum_5_debet;
	$data['poliumum_5_kredit'] = $poliumum_5_kredit;
	$data['poligigi_0_debet'] = $poligigi_0_debet;
	$data['poligigi_0_kredit'] = $poligigi_0_kredit;
	$data['poligigi_1_debet'] = $poligigi_1_debet;
	$data['poligigi_1_kredit'] = $poligigi_1_kredit;
	$data['poligigi_2_debet'] = $poligigi_2_debet;
	$data['poligigi_2_kredit'] = $poligigi_2_kredit;
	$data['poligigi_3_debet'] = $poligigi_3_debet;
	$data['poligigi_3_kredit'] = $poligigi_3_kredit;
	$data['poligigi_4_debet'] = $poligigi_4_debet;
	$data['poligigi_4_kredit'] = $poligigi_4_kredit;
	$data['poligigi_5_debet'] = $poligigi_5_debet;
	$data['poligigi_5_kredit'] = $poligigi_5_kredit;
	$data['poli_anak_0_debet'] = $poli_anak_0_debet;
	$data['poli_anak_0_kredit'] = $poli_anak_0_kredit;
	$data['poli_anak_1_debet'] = $poli_anak_1_debet;
	$data['poli_anak_1_kredit'] = $poli_anak_1_kredit;
	$data['poli_anak_2_debet'] = $poli_anak_2_debet;
	$data['poli_anak_2_kredit'] = $poli_anak_2_kredit;
	$data['poli_anak_3_debet'] = $poli_anak_3_debet;
	$data['poli_anak_3_kredit'] = $poli_anak_3_kredit;
	$data['poli_anak_4_debet'] = $poli_anak_4_debet;
	$data['poli_anak_4_kredit'] = $poli_anak_4_kredit;
	$data['poli_anak_5_debet'] = $poli_anak_5_debet;
	$data['poli_anak_5_kredit'] = $poli_anak_5_kredit;
	$data['poli_obgyn_0_debet'] = $poli_obgyn_0_debet;
	$data['poli_obgyn_0_kredit'] = $poli_obgyn_0_kredit;
	$data['poli_obgyn_1_debet'] = $poli_obgyn_1_debet;
	$data['poli_obgyn_1_kredit'] = $poli_obgyn_1_kredit;
	$data['poli_obgyn_2_debet'] = $poli_obgyn_2_debet;
	$data['poli_obgyn_2_kredit'] = $poli_obgyn_2_kredit;
	$data['poli_obgyn_3_debet'] = $poli_obgyn_3_debet;
	$data['poli_obgyn_3_kredit'] = $poli_obgyn_3_kredit;
	$data['poli_obgyn_4_debet'] = $poli_obgyn_4_debet;
	$data['poli_obgyn_4_kredit'] = $poli_obgyn_4_kredit;
	$data['poli_obgyn_5_debet'] = $poli_obgyn_5_debet;
	$data['poli_obgyn_5_kredit'] = $poli_obgyn_5_kredit;
	$data['poli_bedah_0_debet'] = $poli_bedah_0_debet;
	$data['poli_bedah_0_kredit'] = $poli_bedah_0_kredit;
	$data['poli_bedah_1_debet'] = $poli_bedah_1_debet;
	$data['poli_bedah_1_kredit'] = $poli_bedah_1_kredit;
	$data['poli_bedah_2_debet'] = $poli_bedah_2_debet;
	$data['poli_bedah_2_kredit'] = $poli_bedah_2_kredit;
	$data['poli_bedah_3_debet'] = $poli_bedah_3_debet;
	$data['poli_bedah_3_kredit'] = $poli_bedah_3_kredit;
	$data['poli_bedah_4_debet'] = $poli_bedah_4_debet;
	$data['poli_bedah_4_kredit'] = $poli_bedah_4_kredit;
	$data['poli_bedah_5_debet'] = $poli_bedah_5_debet;
	$data['poli_bedah_5_kredit'] = $poli_bedah_5_kredit;
	$data['poli_mata_0_debet'] = $poli_mata_0_debet;
	$data['poli_mata_0_kredit'] = $poli_mata_0_kredit;
	$data['poli_mata_1_debet'] = $poli_mata_1_debet;
	$data['poli_mata_1_kredit'] = $poli_mata_1_kredit;
	$data['poli_mata_2_debet'] = $poli_mata_2_debet;
	$data['poli_mata_2_kredit'] = $poli_mata_2_kredit;
	$data['poli_mata_3_debet'] = $poli_mata_3_debet;
	$data['poli_mata_3_kredit'] = $poli_mata_3_kredit;
	$data['poli_mata_4_debet'] = $poli_mata_4_debet;
	$data['poli_mata_4_kredit'] = $poli_mata_4_kredit;
	$data['poli_mata_5_debet'] = $poli_mata_5_debet;
	$data['poli_mata_5_kredit'] = $poli_mata_5_kredit;
	$data['poli_tht_0_debet'] = $poli_tht_0_debet;
	$data['poli_tht_0_kredit'] = $poli_tht_0_kredit;
	$data['poli_tht_1_debet'] = $poli_tht_1_debet;
	$data['poli_tht_1_kredit'] = $poli_tht_1_kredit;
	$data['poli_tht_2_debet'] = $poli_tht_2_debet;
	$data['poli_tht_2_kredit'] = $poli_tht_2_kredit;
	$data['poli_tht_3_debet'] = $poli_tht_3_debet;
	$data['poli_tht_3_kredit'] = $poli_tht_3_kredit;
	$data['poli_tht_4_debet'] = $poli_tht_4_debet;
	$data['poli_tht_4_kredit'] = $poli_tht_4_kredit;
	$data['poli_tht_5_debet'] = $poli_tht_5_debet;
	$data['poli_tht_5_kredit'] = $poli_tht_5_kredit;
	$data['poli_kulit_dan_kelamin_0_debet'] = $poli_kulit_dan_kelamin_0_debet;
	$data['poli_kulit_dan_kelamin_0_kredit'] = $poli_kulit_dan_kelamin_0_kredit;
	$data['poli_kulit_dan_kelamin_1_debet'] = $poli_kulit_dan_kelamin_1_debet;
	$data['poli_kulit_dan_kelamin_1_kredit'] = $poli_kulit_dan_kelamin_1_kredit;
	$data['poli_kulit_dan_kelamin_2_debet'] = $poli_kulit_dan_kelamin_2_debet;
	$data['poli_kulit_dan_kelamin_2_kredit'] = $poli_kulit_dan_kelamin_2_kredit;
	$data['poli_kulit_dan_kelamin_3_debet'] = $poli_kulit_dan_kelamin_3_debet;
	$data['poli_kulit_dan_kelamin_3_kredit'] = $poli_kulit_dan_kelamin_3_kredit;
	$data['poli_kulit_dan_kelamin_4_debet'] = $poli_kulit_dan_kelamin_4_debet;
	$data['poli_kulit_dan_kelamin_4_kredit'] = $poli_kulit_dan_kelamin_4_kredit;
	$data['poli_kulit_dan_kelamin_5_debet'] = $poli_kulit_dan_kelamin_5_debet;
	$data['poli_kulit_dan_kelamin_5_kredit'] = $poli_kulit_dan_kelamin_5_kredit;
	$data['poli_syaraf_0_debet'] = $poli_syaraf_0_debet;
	$data['poli_syaraf_0_kredit'] = $poli_syaraf_0_kredit;
	$data['poli_syaraf_1_debet'] = $poli_syaraf_1_debet;
	$data['poli_syaraf_1_kredit'] = $poli_syaraf_1_kredit;
	$data['poli_syaraf_2_debet'] = $poli_syaraf_2_debet;
	$data['poli_syaraf_2_kredit'] = $poli_syaraf_2_kredit;
	$data['poli_syaraf_3_debet'] = $poli_syaraf_3_debet;
	$data['poli_syaraf_3_kredit'] = $poli_syaraf_3_kredit;
	$data['poli_syaraf_4_debet'] = $poli_syaraf_4_debet;
	$data['poli_syaraf_4_kredit'] = $poli_syaraf_4_kredit;
	$data['poli_syaraf_5_debet'] = $poli_syaraf_5_debet;
	$data['poli_syaraf_5_kredit'] = $poli_syaraf_5_kredit;
	$data['poli_ortopedi_0_debet'] = $poli_ortopedi_0_debet;
	$data['poli_ortopedi_0_kredit'] = $poli_ortopedi_0_kredit;
	$data['poli_ortopedi_1_debet'] = $poli_ortopedi_1_debet;
	$data['poli_ortopedi_1_kredit'] = $poli_ortopedi_1_kredit;
	$data['poli_ortopedi_2_debet'] = $poli_ortopedi_2_debet;
	$data['poli_ortopedi_2_kredit'] = $poli_ortopedi_2_kredit;
	$data['poli_ortopedi_3_debet'] = $poli_ortopedi_3_debet;
	$data['poli_ortopedi_3_kredit'] = $poli_ortopedi_3_kredit;
	$data['poli_ortopedi_4_debet'] = $poli_ortopedi_4_debet;
	$data['poli_ortopedi_4_kredit'] = $poli_ortopedi_4_kredit;
	$data['poli_ortopedi_5_debet'] = $poli_ortopedi_5_debet;
	$data['poli_ortopedi_5_kredit'] = $poli_ortopedi_5_kredit;
	$data['poli_jantung_0_debet'] = $poli_jantung_0_debet;
	$data['poli_jantung_0_kredit'] = $poli_jantung_0_kredit;
	$data['poli_jantung_1_debet'] = $poli_jantung_1_debet;
	$data['poli_jantung_1_kredit'] = $poli_jantung_1_kredit;
	$data['poli_jantung_2_debet'] = $poli_jantung_2_debet;
	$data['poli_jantung_2_kredit'] = $poli_jantung_2_kredit;
	$data['poli_jantung_3_debet'] = $poli_jantung_3_debet;
	$data['poli_jantung_3_kredit'] = $poli_jantung_3_kredit;
	$data['poli_jantung_4_debet'] = $poli_jantung_4_debet;
	$data['poli_jantung_4_kredit'] = $poli_jantung_4_kredit;
	$data['poli_jantung_5_debet'] = $poli_jantung_5_debet;
	$data['poli_jantung_5_kredit'] = $poli_jantung_5_kredit;
	$data['poli_paru_0_debet'] = $poli_paru_0_debet;
	$data['poli_paru_0_kredit'] = $poli_paru_0_kredit;
	$data['poli_paru_1_debet'] = $poli_paru_1_debet;
	$data['poli_paru_1_kredit'] = $poli_paru_1_kredit;
	$data['poli_paru_2_debet'] = $poli_paru_2_debet;
	$data['poli_paru_2_kredit'] = $poli_paru_2_kredit;
	$data['poli_paru_3_debet'] = $poli_paru_3_debet;
	$data['poli_paru_3_kredit'] = $poli_paru_3_kredit;
	$data['poli_paru_4_debet'] = $poli_paru_4_debet;
	$data['poli_paru_4_kredit'] = $poli_paru_4_kredit;
	$data['poli_paru_5_debet'] = $poli_paru_5_debet;
	$data['poli_paru_5_kredit'] = $poli_paru_5_kredit;
	$data['poli_penyakit_dalam_0_debet'] = $poli_penyakit_dalam_0_debet;
	$data['poli_penyakit_dalam_0_kredit'] = $poli_penyakit_dalam_0_kredit;
	$data['poli_penyakit_dalam_1_debet'] = $poli_penyakit_dalam_1_debet;
	$data['poli_penyakit_dalam_1_kredit'] = $poli_penyakit_dalam_1_kredit;
	$data['poli_penyakit_dalam_2_debet'] = $poli_penyakit_dalam_2_debet;
	$data['poli_penyakit_dalam_2_kredit'] = $poli_penyakit_dalam_2_kredit;
	$data['poli_penyakit_dalam_3_debet'] = $poli_penyakit_dalam_3_debet;
	$data['poli_penyakit_dalam_3_kredit'] = $poli_penyakit_dalam_3_kredit;
	$data['poli_penyakit_dalam_4_debet'] = $poli_penyakit_dalam_4_debet;
	$data['poli_penyakit_dalam_4_kredit'] = $poli_penyakit_dalam_4_kredit;
	$data['poli_penyakit_dalam_5_debet'] = $poli_penyakit_dalam_5_debet;
	$data['poli_penyakit_dalam_5_kredit'] = $poli_penyakit_dalam_5_kredit;
	$data['gizi_0_debet'] = $gizi_0_debet;
	$data['gizi_0_kredit'] = $gizi_0_kredit;
	$data['gizi_1_debet'] = $gizi_1_debet;
	$data['gizi_1_kredit'] = $gizi_1_kredit;
	$data['gizi_2_debet'] = $gizi_2_debet;
	$data['gizi_2_kredit'] = $gizi_2_kredit;
	$data['gizi_3_debet'] = $gizi_3_debet;
	$data['gizi_3_kredit'] = $gizi_3_kredit;
	$data['gizi_4_debet'] = $gizi_4_debet;
	$data['gizi_4_kredit'] = $gizi_4_kredit;
	$data['gizi_5_debet'] = $gizi_5_debet;
	$data['gizi_5_kredit'] = $gizi_5_kredit;
	$data['hemodialisa_0_debet'] = $hemodialisa_0_debet;
	$data['hemodialisa_0_kredit'] = $hemodialisa_0_kredit;
	$data['hemodialisa_1_debet'] = $hemodialisa_1_debet;
	$data['hemodialisa_1_kredit'] = $hemodialisa_1_kredit;
	$data['hemodialisa_2_debet'] = $hemodialisa_2_debet;
	$data['hemodialisa_2_kredit'] = $hemodialisa_2_kredit;
	$data['hemodialisa_3_debet'] = $hemodialisa_3_debet;
	$data['hemodialisa_3_kredit'] = $hemodialisa_3_kredit;
	$data['hemodialisa_4_debet'] = $hemodialisa_4_debet;
	$data['hemodialisa_4_kredit'] = $hemodialisa_4_kredit;
	$data['hemodialisa_5_debet'] = $hemodialisa_5_debet;
	$data['hemodialisa_5_kredit'] = $hemodialisa_5_kredit;
	$data['fisiotherapy_0_debet'] = $fisiotherapy_0_debet;
	$data['fisiotherapy_0_kredit'] = $fisiotherapy_0_kredit;
	$data['fisiotherapy_1_debet'] = $fisiotherapy_1_debet;
	$data['fisiotherapy_1_kredit'] = $fisiotherapy_1_kredit;
	$data['fisiotherapy_2_debet'] = $fisiotherapy_2_debet;
	$data['fisiotherapy_2_kredit'] = $fisiotherapy_2_kredit;
	$data['fisiotherapy_3_debet'] = $fisiotherapy_3_debet;
	$data['fisiotherapy_3_kredit'] = $fisiotherapy_3_kredit;
	$data['fisiotherapy_4_debet'] = $fisiotherapy_4_debet;
	$data['fisiotherapy_4_kredit'] = $fisiotherapy_4_kredit;
	$data['fisiotherapy_5_debet'] = $fisiotherapy_5_debet;
	$data['fisiotherapy_5_kredit'] = $fisiotherapy_5_kredit;
	$data['kamar_operasi_0_debet'] = $kamar_operasi_0_debet;
	$data['kamar_operasi_0_kredit'] = $kamar_operasi_0_kredit;
	$data['kamar_operasi_1_debet'] = $kamar_operasi_1_debet;
	$data['kamar_operasi_1_kredit'] = $kamar_operasi_1_kredit;
	$data['kamar_operasi_2_debet'] = $kamar_operasi_2_debet;
	$data['kamar_operasi_2_kredit'] = $kamar_operasi_2_kredit;
	$data['kamar_operasi_3_debet'] = $kamar_operasi_3_debet;
	$data['kamar_operasi_3_kredit'] = $kamar_operasi_3_kredit;
	$data['kamar_operasi_4_debet'] = $kamar_operasi_4_debet;
	$data['kamar_operasi_4_kredit'] = $kamar_operasi_4_kredit;
	$data['kamar_operasi_5_debet'] = $kamar_operasi_5_debet;
	$data['kamar_operasi_5_kredit'] = $kamar_operasi_5_kredit;
	$data['kamar_operasi_6_debet'] = $kamar_operasi_6_debet;
	$data['kamar_operasi_6_kredit'] = $kamar_operasi_6_kredit;
	$data['laboratory_0_kredit'] = $laboratory_0_kredit;
	$data['laboratory_0_debet'] = $laboratory_0_debet;
	$data['laboratory_1_kredit'] = $laboratory_1_kredit;
	$data['laboratory_1_debet'] = $laboratory_1_debet;
	$data['laboratory_2_kredit'] = $laboratory_2_kredit;
	$data['laboratory_2_debet'] = $laboratory_2_debet;
	$data['laboratory_3_kredit'] = $laboratory_3_kredit;
	$data['laboratory_3_debet'] = $laboratory_3_debet;
	$data['depo_farmasi_0_kredit'] = $depo_farmasi_0_kredit;
	$data['depo_farmasi_0_debet'] = $depo_farmasi_0_debet;
	$data['depo_farmasi_1_kredit'] = $depo_farmasi_1_kredit;
	$data['depo_farmasi_1_debet'] = $depo_farmasi_1_debet;
	$data['depo_farmasi_2_kredit'] = $depo_farmasi_2_kredit;
	$data['depo_farmasi_2_debet'] = $depo_farmasi_2_debet;
	$data['depo_farmasi_3_kredit'] = $depo_farmasi_3_kredit;
	$data['depo_farmasi_3_debet'] = $depo_farmasi_3_debet;
	$data['depo_farmasi_4_kredit'] = $depo_farmasi_4_kredit;
	$data['depo_farmasi_4_debet'] = $depo_farmasi_4_debet;
	$data['depo_farmasi_5_kredit'] = $depo_farmasi_5_kredit;
	$data['depo_farmasi_5_debet'] = $depo_farmasi_5_debet;
	$data['depo_farmasi_6_kredit'] = $depo_farmasi_6_kredit;
	$data['depo_farmasi_6_debet'] = $depo_farmasi_6_debet;
	$data['depo_farmasi_7_kredit'] = $depo_farmasi_7_kredit;
	$data['depo_farmasi_7_debet'] = $depo_farmasi_7_debet;
	$data['depo_farmasi_8_kredit'] = $depo_farmasi_8_kredit;
	$data['depo_farmasi_8_debet'] = $depo_farmasi_8_debet;
	$data['radiology_0_kredit'] = $radiology_0_kredit;
	$data['radiology_0_debet'] = $radiology_0_debet;
	$data['radiology_1_kredit'] = $radiology_1_kredit;
	$data['radiology_1_debet'] = $radiology_1_debet;
	$data['radiology_2_kredit'] = $radiology_2_kredit;
	$data['radiology_2_debet'] = $radiology_2_debet;
	$data['radiology_3_kredit'] = $radiology_3_kredit;
	$data['radiology_3_debet'] = $radiology_3_debet;
	$data['radiology_4_kredit'] = $radiology_4_kredit;
	$data['radiology_4_debet'] = $radiology_4_debet;
	$data['radiology_5_kredit'] = $radiology_5_kredit;
	$data['radiology_5_debet'] = $radiology_5_debet;
	$data['lain_lain_0_kredit'] = $lain_lain_0_kredit;
	$data['lain_lain_0_debet'] = $lain_lain_0_debet;
	$data['lain_lain_1_kredit'] = $lain_lain_1_kredit;
	$data['lain_lain_1_debet'] = $lain_lain_1_debet;
	$data['lain_lain_2_kredit'] = $lain_lain_2_kredit;
	$data['lain_lain_2_debet'] = $lain_lain_2_debet;
	$data['lain_lain_3_kredit'] = $lain_lain_3_kredit;
	$data['lain_lain_3_debet'] = $lain_lain_3_debet;
	$data['lain_lain_4_kredit'] = $lain_lain_4_kredit;
	$data['lain_lain_4_debet'] = $lain_lain_4_debet;
	$data['lain_lain_5_kredit'] = $lain_lain_5_kredit;
	$data['lain_lain_5_debet'] = $lain_lain_5_debet;
	$data['lain_lain_6_kredit'] = $lain_lain_6_kredit;
	$data['lain_lain_6_debet'] = $lain_lain_6_debet;

	$data['kasir_0_kredit'] = $kasir_0_kredit;
	$data['kasir_0_debet'] = $kasir_0_debet;
	$index = 2;
	foreach ($perusahaan_ptpn_arr as $parr) {
		$data['kasir_' . $index . '_kredit'] = $parr['kredit'];
		$data['kasir_' . $index . '_debet'] = $parr['debet'];
		$index++;
	}
	foreach ($asuransi_ptpn_arr as $aarr) {
		$data['kasir_' . $index . '_kredit'] = $aarr['kredit'];
		$data['kasir_' . $index . '_debet'] = $aarr['debet'];
		$index++;
	}
	$index += 1;
	foreach ($perusahaan_non_ptpn_arr as $parr) {
		$data['kasir_' . $index . '_kredit'] = $parr['kredit'];
		$data['kasir_' . $index . '_debet'] = $parr['debet'];
		$index++;
	}
	foreach ($asuransi_non_ptpn_arr as $aarr) {
		$data['kasir_' . $index . '_kredit'] = $aarr['kredit'];
		$data['kasir_' . $index . '_debet'] = $aarr['debet'];
		$index++;
	}
	$data['kasir_' .  $index . '_kredit'] = $kasir_1_kredit;
	$data['kasir_' . $index++ . '_debet'] = $kasir_1_debet;
	$data['kasir_' .  $index . '_kredit'] = $kasir_2_kredit;
	$data['kasir_' . $index++ . '_debet'] = $kasir_2_debet;
	$data['kasir_' .  $index . '_kredit'] = $kasir_3_kredit;
	$data['kasir_' . $index++ . '_debet'] = $kasir_3_debet;
	$data['kasir_' .  $index . '_kredit'] = $kasir_4_kredit;
	$data['kasir_' . $index++ . '_debet'] = $kasir_4_debet;
	$data['kasir_' .  $index . '_kredit'] = $kasir_5_kredit;
	$data['kasir_' . $index++ . '_debet'] = $kasir_5_debet;
	$data['kasir_' .  $index . '_kredit'] = $kasir_6_kredit;
	$data['kasir_' . $index++ . '_debet'] = $kasir_6_debet;
	$data['kasir_' .  $index . '_kredit'] = $kasir_7_kredit;
	$data['kasir_' . $index++ . '_debet'] = $kasir_7_debet;
	$data['kasir_' .  $index . '_kredit'] = $kasir_8_kredit;
	$data['kasir_' . $index++ . '_debet'] = $kasir_8_debet;
	$data['kasir_' .  $index . '_kredit'] = $kasir_9_kredit;
	$data['kasir_' . $index++ . '_debet'] = $kasir_9_debet;
	$data['kasir_' .  $index . '_kredit'] = $kasir_10_kredit;
	$data['kasir_' . $index . '_debet'] = $kasir_10_debet;
	$data['kasir_index_num'] = $index;
	/*
	$data['jaspel_kredit_0_kredit'] = $jaspel_kredit_0_kredit;
	$data['jaspel_kredit_0_debet'] = $jaspel_kredit_0_debet;
	$data['jaspel_debet_0_kredit'] = $jaspel_debet_0_kredit;
	$data['jaspel_debet_0_debet'] = $jaspel_debet_0_debet;
	$data['jaspel_debet_1_kredit'] = $jaspel_debet_1_kredit;
	$data['jaspel_debet_1_debet'] = $jaspel_debet_1_debet;
	$data['jaspel_debet_2_kredit'] = $jaspel_debet_2_kredit;
	$data['jaspel_debet_2_debet'] = $jaspel_debet_2_debet;
	$data['jaspel_debet_3_kredit'] = $jaspel_debet_3_kredit;
	$data['jaspel_debet_3_debet'] = $jaspel_debet_3_debet;
	$data['jaspel_debet_4_kredit'] = $jaspel_debet_4_kredit;
	$data['jaspel_debet_4_debet'] = $jaspel_debet_4_debet;
	$data['jaspel_debet_5_kredit'] = $jaspel_debet_5_kredit;
	$data['jaspel_debet_5_debet'] = $jaspel_debet_5_debet;
	$data['jaspel_debet_6_kredit'] = $jaspel_debet_6_kredit;
	$data['jaspel_debet_6_debet'] = $jaspel_debet_6_debet;
	$data['jaspel_debet_7_kredit'] = $jaspel_debet_7_kredit;
	$data['jaspel_debet_7_debet'] = $jaspel_debet_7_debet;
	$data['jaspel_debet_8_kredit'] = $jaspel_debet_8_kredit;
	$data['jaspel_debet_8_debet'] = $jaspel_debet_8_debet;
	$data['jaspel_debet_9_kredit'] = $jaspel_debet_9_kredit;
	$data['jaspel_debet_9_debet'] = $jaspel_debet_9_debet;
	$data['jaspel_debet_10_kredit'] = $jaspel_debet_10_kredit;
	$data['jaspel_debet_10_debet'] = $jaspel_debet_10_debet;
	$data['jaspel_debet_11_kredit'] = $jaspel_debet_11_kredit;
	$data['jaspel_debet_11_debet'] = $jaspel_debet_11_debet;
	$data['jaspel_debet_12_kredit'] = $jaspel_debet_12_kredit;
	$data['jaspel_debet_12_debet'] = $jaspel_debet_12_debet;
	$data['jaspel_debet_13_kredit'] = $jaspel_debet_13_kredit;
	$data['jaspel_debet_13_debet'] = $jaspel_debet_13_debet;
	$data['jaspel_debet_14_kredit'] = $jaspel_debet_14_kredit;
	$data['jaspel_debet_14_debet'] = $jaspel_debet_14_debet;
	$data['jaspel_debet_15_kredit'] = $jaspel_debet_15_kredit;
	$data['jaspel_debet_15_debet'] = $jaspel_debet_15_debet;
	$data['jaspel_debet_16_kredit'] = $jaspel_debet_16_kredit;
	$data['jaspel_debet_16_debet'] = $jaspel_debet_16_debet;
	$data['jaspel_debet_17_kredit'] = $jaspel_debet_17_kredit;
	$data['jaspel_debet_17_debet'] = $jaspel_debet_17_debet;
	$data['jaspel_debet_18_kredit'] = $jaspel_debet_18_kredit;
	$data['jaspel_debet_18_debet'] = $jaspel_debet_18_debet;
	$data['jaspel_debet_19_kredit'] = $jaspel_debet_19_kredit;
	$data['jaspel_debet_19_debet'] = $jaspel_debet_19_debet;
	$data['jaspel_debet_20_kredit'] = $jaspel_debet_20_kredit;
	$data['jaspel_debet_20_debet'] = $jaspel_debet_20_debet;
	$data['jaspel_debet_21_kredit'] = $jaspel_debet_21_kredit;
	$data['jaspel_debet_21_debet'] = $jaspel_debet_21_debet;
	$data['jaspel_debet_22_kredit'] = $jaspel_debet_22_kredit;
	$data['jaspel_debet_22_debet'] = $jaspel_debet_22_debet;
	$data['jaspel_debet_23_kredit'] = $jaspel_debet_23_kredit;
	$data['jaspel_debet_23_debet'] = $jaspel_debet_23_debet;
	$data['jaspel_debet_24_kredit'] = $jaspel_debet_24_kredit;
	$data['jaspel_debet_24_debet'] = $jaspel_debet_24_debet;
	$data['jaspel_debet_25_kredit'] = $jaspel_debet_25_kredit;
	$data['jaspel_debet_25_debet'] = $jaspel_debet_5_debet;
	$data['jaspel_debet_26_kredit'] = $jaspel_debet_26_kredit;
	$data['jaspel_debet_26_debet'] = $jaspel_debet_26_debet;
	$data['jaspel_debet_27_kredit'] = $jaspel_debet_27_kredit;
	$data['jaspel_debet_27_debet'] = $jaspel_debet_27_debet;
	$data['jaspel_debet_28_kredit'] = $jaspel_debet_28_kredit;
	$data['jaspel_debet_28_debet'] = $jaspel_debet_28_debet;
	*/

	echo json_encode($data);
?>