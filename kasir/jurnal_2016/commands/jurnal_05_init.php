<?php
	$jurnal_items = array(
		"rpa"	=> 	array(
			array(
				"no"		=> "A",
				"kode"		=> "551.01.",
				"nama"		=> "Ruang Perawatan A",
				"debet"		=> "",
				"kredit"	=> "",
				"cmd"		=> "",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>1</div>",
				"kode"		=> "551.01.00",
				"nama"		=> "Sewa Kamar",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "sewa_kamar",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>2</div>",
				"kode"		=> "551.01.01",
				"nama"		=> "Visite / Jasa Dokter",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "jasa_dokter",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>3</div>",
				"kode"		=> "",
				"nama"		=> "Tindakan Perawat Ruangan",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "jasa_keperawatan",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>4</div>",
				"kode"		=> "551.01.03",
				"nama"		=> "Alat Kesehatan",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "alok",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>5</div>",
				"kode"		=> "551.01.09",
				"nama"		=> "Pendapatan Lain-Lain",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "pendapatan_lain",
				"filter"	=> ""
			)
		),
		"rpb"	=> 	array(
			array(
				"no"		=> "B",
				"kode"		=> "551.02.",
				"nama"		=> "Ruang Perawatan B",
				"debet"		=> "",
				"kredit"	=> "",
				"cmd"		=> "",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>1</div>",
				"kode"		=> "551.02.00",
				"nama"		=> "Sewa Kamar",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "sewa_kamar",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>2</div>",
				"kode"		=> "551.02.01",
				"nama"		=> "Visite / Jasa Dokter",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "jasa_dokter",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>3</div>",
				"kode"		=> "",
				"nama"		=> "Tindakan Perawat Ruangan",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "jasa_keperawatan",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>4</div>",
				"kode"		=> "551.02.03",
				"nama"		=> "Alat Kesehatan",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "alok",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>5</div>",
				"kode"		=> "551.02.09",
				"nama"		=> "Pendapatan Lain-Lain",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "pendapatan_lain",
				"filter"	=> ""
			)
		),
		"icu"	=> 	array(
			array(
				"no"		=> "C",
				"kode"		=> "551.03.",
				"nama"		=> "Ruang ICU/RPO",
				"debet"		=> "",
				"kredit"	=> "",
				"cmd"		=> "",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>1</div>",
				"kode"		=> "551.03.00",
				"nama"		=> "Sewa Kamar",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "sewa_kamar",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>2</div>",
				"kode"		=> "551.03.01",
				"nama"		=> "Visite / Jasa Dokter",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "jasa_dokter",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>3</div>",
				"kode"		=> "",
				"nama"		=> "Tindakan Perawat Ruangan",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "jasa_keperawatan",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>4</div>",
				"kode"		=> "551.03.03",
				"nama"		=> "Alat Kesehatan",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "alok",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>5</div>",
				"kode"		=> "551.03.09",
				"nama"		=> "Pendapatan Lain-Lain",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "pendapatan_lain",
				"filter"	=> ""
			)
		),
		"rpc"	=> 	array(
			array(
				"no"		=> "D",
				"kode"		=> "551.04.",
				"nama"		=> "Ruang Perawatan C (Lt-1)",
				"debet"		=> "",
				"kredit"	=> "",
				"cmd"		=> "",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>1</div>",
				"kode"		=> "551.04.00",
				"nama"		=> "Sewa Kamar",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "sewa_kamar",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>2</div>",
				"kode"		=> "551.04.01",
				"nama"		=> "Visite / Jasa Dokter",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "jasa_dokter",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>3</div>",
				"kode"		=> "",
				"nama"		=> "Tindakan Perawat Ruangan",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "jasa_keperawatan",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>4</div>",
				"kode"		=> "551.04.03",
				"nama"		=> "Alat Kesehatan",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "alok",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>5</div>",
				"kode"		=> "551.04.09",
				"nama"		=> "Pendapatan Lain-Lain",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "pendapatan_lain",
				"filter"	=> ""
			)
		),
		"rpd"	=> 	array(
			array(
				"no"		=> "E",
				"kode"		=> "551.05.",
				"nama"		=> "Ruang Perawatan D (Lt-2)",
				"debet"		=> "",
				"kredit"	=> "",
				"cmd"		=> "",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>1</div>",
				"kode"		=> "551.05.00",
				"nama"		=> "Sewa Kamar",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "sewa_kamar",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>2</div>",
				"kode"		=> "551.05.01",
				"nama"		=> "Visite / Jasa Dokter",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "jasa_dokter",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>3</div>",
				"kode"		=> "",
				"nama"		=> "Tindakan Perawat Ruangan",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "jasa_keperawatan",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>4</div>",
				"kode"		=> "551.05.03",
				"nama"		=> "Alat Kesehatan",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "alok",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>5</div>",
				"kode"		=> "551.05.09",
				"nama"		=> "Pendapatan Lain-Lain",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "pendapatan_lain",
				"filter"	=> ""
			)
		),
		"rpe"	=> 	array(
			array(
				"no"		=> "F",
				"kode"		=> "551.06.",
				"nama"		=> "Ruang Perawatan D (Lt-3)",
				"debet"		=> "",
				"kredit"	=> "",
				"cmd"		=> "",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>1</div>",
				"kode"		=> "551.06.00",
				"nama"		=> "Sewa Kamar",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "sewa_kamar",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>2</div>",
				"kode"		=> "551.06.01",
				"nama"		=> "Visite / Jasa Dokter",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "jasa_dokter",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>3</div>",
				"kode"		=> "",
				"nama"		=> "Tindakan Perawat Ruangan",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "jasa_keperawatan",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>4</div>",
				"kode"		=> "551.06.03",
				"nama"		=> "Alat Kesehatan",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "alok",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>5</div>",
				"kode"		=> "551.06.09",
				"nama"		=> "Pendapatan Lain-Lain",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "pendapatan_lain",
				"filter"	=> ""
			)
		),
		"instalasi_gawat_darurat"	=> array(
			array(
				"no"		=> "G",
				"kode"		=> "552.01.",
				"nama"		=> "Ruang UGD",
				"debet"		=> "",
				"kredit"	=> "",
				"cmd"		=> "",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>1</div>",
				"kode"		=> "552.01.00",
				"nama"		=> "Sewa Kamar",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "sewa_kamar",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>2</div>",
				"kode"		=> "552.01.01",
				"nama"		=> "Visite / Jasa Dokter",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "jasa_dokter",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>3</div>",
				"kode"		=> "",
				"nama"		=> "Tindakan Perawat Ruangan",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "jasa_keperawatan",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>4</div>",
				"kode"		=> "552.01.03",
				"nama"		=> "Alat Kesehatan",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "alok",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>5</div>",
				"kode"		=> "552.01.09",
				"nama"		=> "Pendapatan Lain-Lain",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "pendapatan_lain",
				"filter"	=> ""
			)
		),
		"poliumum"	=> array(
			array(
				"no"		=> "H",
				"kode"		=> "552.02.",
				"nama"		=> "Poli Umum",
				"debet"		=> "",
				"kredit"	=> "",
				"cmd"		=> "",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>1</div>",
				"kode"		=> "552.02.00",
				"nama"		=> "Sewa Kamar",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "sewa_kamar",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>2</div>",
				"kode"		=> "552.02.01",
				"nama"		=> "Visite / Jasa Dokter",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "jasa_dokter",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>3</div>",
				"kode"		=> "",
				"nama"		=> "Tindakan Perawat Ruangan",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "jasa_keperawatan",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>4</div>",
				"kode"		=> "552.02.03",
				"nama"		=> "Alat Kesehatan",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "alok",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>5</div>",
				"kode"		=> "552.02.09",
				"nama"		=> "Pendapatan Lain-Lain",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "pendapatan_lain",
				"filter"	=> ""
			)
		),
		"poligigi"	=> array(
			array(
				"no"		=> "I",
				"kode"		=> "552.03.",
				"nama"		=> "Poli Gigi",
				"debet"		=> "",
				"kredit"	=> "",
				"cmd"		=> "",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>1</div>",
				"kode"		=> "552.03.00",
				"nama"		=> "Sewa Kamar",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "sewa_kamar",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>2</div>",
				"kode"		=> "552.03.01",
				"nama"		=> "Visite / Jasa Dokter",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "jasa_dokter",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>3</div>",
				"kode"		=> "",
				"nama"		=> "Tindakan Perawat Ruangan",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "jasa_keperawatan",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>4</div>",
				"kode"		=> "552.03.03",
				"nama"		=> "Alat Kesehatan",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "alok",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>5</div>",
				"kode"		=> "552.03.09",
				"nama"		=> "Pendapatan Lain-Lain",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "pendapatan_lain",
				"filter"	=> ""
			)
		),
		"poli_anak"	=> array(
			array(
				"no"		=> "J",
				"kode"		=> "552.04.",
				"nama"		=> "Poli Anak",
				"debet"		=> "",
				"kredit"	=> "",
				"cmd"		=> "",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>1</div>",
				"kode"		=> "552.04.00",
				"nama"		=> "Sewa Kamar",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "sewa_kamar",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>2</div>",
				"kode"		=> "552.04.01",
				"nama"		=> "Visite / Jasa Dokter",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "jasa_dokter",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>3</div>",
				"kode"		=> "",
				"nama"		=> "Tindakan Perawat Ruangan",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "jasa_keperawatan",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>4</div>",
				"kode"		=> "552.04.03",
				"nama"		=> "Alat Kesehatan",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "alok",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>5</div>",
				"kode"		=> "552.04.09",
				"nama"		=> "Pendapatan Lain-Lain",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "pendapatan_lain",
				"filter"	=> ""
			)
		),
		"fisiotherapy"	=> array(
			array(
				"no"		=> "K",
				"kode"		=> "552.05.",
				"nama"		=> "Poli Fisioterapi",
				"debet"		=> "",
				"kredit"	=> "",
				"cmd"		=> "",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>1</div>",
				"kode"		=> "552.05.00",
				"nama"		=> "Sewa Kamar",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "sewa_kamar",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>2</div>",
				"kode"		=> "552.05.01",
				"nama"		=> "Visite / Jasa Dokter",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "jasa_dokter",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>3</div>",
				"kode"		=> "",
				"nama"		=> "Tindakan Perawat Ruangan",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "jasa_keperawatan",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>4</div>",
				"kode"		=> "552.05.03",
				"nama"		=> "Alat Kesehatan",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "alok",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>5</div>",
				"kode"		=> "552.05.09",
				"nama"		=> "Pendapatan Lain-Lain",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "pendapatan_lain",
				"filter"	=> ""
			)
		),
		"poli_obgyn"	=> array(
			array(
				"no"		=> "L",
				"kode"		=> "552.06.",
				"nama"		=> "Poli Obgyn",
				"debet"		=> "",
				"kredit"	=> "",
				"cmd"		=> "",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>1</div>",
				"kode"		=> "552.06.00",
				"nama"		=> "Sewa Kamar",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "sewa_kamar",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>2</div>",
				"kode"		=> "552.06.01",
				"nama"		=> "Visite / Jasa Dokter",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "jasa_dokter",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>3</div>",
				"kode"		=> "",
				"nama"		=> "Tindakan Perawat Ruangan",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "jasa_keperawatan",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>4</div>",
				"kode"		=> "552.06.03",
				"nama"		=> "Alat Kesehatan",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "alok",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>5</div>",
				"kode"		=> "552.06.09",
				"nama"		=> "Pendapatan Lain-Lain",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "pendapatan_lain",
				"filter"	=> ""
			)
		),
		"poli_bedah"	=> array(
			array(
				"no"		=> "M",
				"kode"		=> "552.07.",
				"nama"		=> "Poli Bedah",
				"debet"		=> "",
				"kredit"	=> "",
				"cmd"		=> "",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>1</div>",
				"kode"		=> "552.07.00",
				"nama"		=> "Sewa Kamar",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "sewa_kamar",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>2</div>",
				"kode"		=> "552.07.01",
				"nama"		=> "Visite / Jasa Dokter",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "jasa_dokter",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>3</div>",
				"kode"		=> "",
				"nama"		=> "Tindakan Perawat Ruangan",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "jasa_keperawatan",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>4</div>",
				"kode"		=> "552.07.03",
				"nama"		=> "Alat Kesehatan",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "alok",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>5</div>",
				"kode"		=> "552.07.09",
				"nama"		=> "Pendapatan Lain-Lain",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "pendapatan_lain",
				"filter"	=> ""
			)
		),
		"poli_mata"	=> array(
			array(
				"no"		=> "N",
				"kode"		=> "552.08.",
				"nama"		=> "Poli Mata",
				"debet"		=> "",
				"kredit"	=> "",
				"cmd"		=> "",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>1</div>",
				"kode"		=> "552.08.00",
				"nama"		=> "Sewa Kamar",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "sewa_kamar",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>2</div>",
				"kode"		=> "552.08.01",
				"nama"		=> "Visite / Jasa Dokter",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "jasa_dokter"
			),
			array(
				"no"		=> "<div align='right'>3</div>",
				"kode"		=> "",
				"nama"		=> "Tindakan Perawat Ruangan",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "jasa_keperawatan",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>4</div>",
				"kode"		=> "552.08.03",
				"nama"		=> "Alat Kesehatan",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "alok",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>5</div>",
				"kode"		=> "552.08.09",
				"nama"		=> "Pendapatan Lain-Lain",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "pendapatan_lain",
				"filter"	=> ""
			)
		),
		"poli_tht"	=> array(
			array(
				"no"		=> "O",
				"kode"		=> "552.09.",
				"nama"		=> "Poli THT",
				"debet"		=> "",
				"kredit"	=> "",
				"cmd"		=> "",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>1</div>",
				"kode"		=> "552.09.00",
				"nama"		=> "Sewa Kamar",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "sewa_kamar",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>2</div>",
				"kode"		=> "552.09.01",
				"nama"		=> "Visite / Jasa Dokter",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "jasa_dokter",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>3</div>",
				"kode"		=> "",
				"nama"		=> "Tindakan Perawat Ruangan",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "jasa_keperawatan",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>4</div>",
				"kode"		=> "552.09.03",
				"nama"		=> "Alat Kesehatan",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "alok",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>5</div>",
				"kode"		=> "552.09.09",
				"nama"		=> "Pendapatan Lain-Lain",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "pendapatan_lain",
				"filter"	=> ""
			)
		),
		"poli_kulit_dan_kelamin"	=> array(
			array(
				"no"		=> "P",
				"kode"		=> "552.10.",
				"nama"		=> "Poli Kulit dan Kelamin",
				"debet"		=> "",
				"kredit"	=> "",
				"cmd"		=> "",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>1</div>",
				"kode"		=> "552.10.00",
				"nama"		=> "Sewa Kamar",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "sewa_kamar",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>2</div>",
				"kode"		=> "552.10.01",
				"nama"		=> "Visite / Jasa Dokter",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "jasa_dokter",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>3</div>",
				"kode"		=> "",
				"nama"		=> "Tindakan Perawat Ruangan",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "jasa_keperawatan",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>4</div>",
				"kode"		=> "552.10.03",
				"nama"		=> "Alat Kesehatan",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "alok",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>5</div>",
				"kode"		=> "552.10.09",
				"nama"		=> "Pendapatan Lain-Lain",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "pendapatan_lain",
				"filter"	=> ""
			)
		),
		"poli_syaraf"	=> array(
			array(
				"no"		=> "Q",
				"kode"		=> "552.11.",
				"nama"		=> "Poli Syaraf",
				"debet"		=> "",
				"kredit"	=> "",
				"cmd"		=> ""
			),
			array(
				"no"		=> "<div align='right'>1</div>",
				"kode"		=> "552.11.00",
				"nama"		=> "Sewa Kamar",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "sewa_kamar",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>2</div>",
				"kode"		=> "552.11.01",
				"nama"		=> "Visite / Jasa Dokter",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "jasa_dokter",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>3</div>",
				"kode"		=> "",
				"nama"		=> "Tindakan Perawat Ruangan",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "jasa_keperawatan",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>4</div>",
				"kode"		=> "552.11.03",
				"nama"		=> "Alat Kesehatan",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "alok",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>5</div>",
				"kode"		=> "552.11.09",
				"nama"		=> "Pendapatan Lain-Lain",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "pendapatan_lain",
				"filter"	=> ""
			)
		),
		"poli_ortopedi"	=> array(
			array(
				"no"		=> "R",
				"kode"		=> "552.12.",
				"nama"		=> "Poli Ortopedi",
				"debet"		=> "",
				"kredit"	=> "",
				"cmd"		=> "",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>1</div>",
				"kode"		=> "552.12.00",
				"nama"		=> "Sewa Kamar",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "sewa_kamar",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>2</div>",
				"kode"		=> "552.12.01",
				"nama"		=> "Visite / Jasa Dokter",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "jasa_dokter",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>3</div>",
				"kode"		=> "",
				"nama"		=> "Tindakan Perawat Ruangan",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "jasa_keperawatan",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>4</div>",
				"kode"		=> "552.12.03",
				"nama"		=> "Alat Kesehatan",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "alok",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>5</div>",
				"kode"		=> "552.12.09",
				"nama"		=> "Pendapatan Lain-Lain",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "pendapatan_lain",
				"filter"	=> ""
			)
		),
		"poli_jantung"	=> array(
			array(
				"no"		=> "S",
				"kode"		=> "552.13.",
				"nama"		=> "Poli Jantung",
				"debet"		=> "",
				"kredit"	=> "",
				"cmd"		=> "",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>1</div>",
				"kode"		=> "552.13.00",
				"nama"		=> "Sewa Kamar",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "sewa_kamar",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>2</div>",
				"kode"		=> "552.13.01",
				"nama"		=> "Visite / Jasa Dokter",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "jasa_dokter",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>3</div>",
				"kode"		=> "",
				"nama"		=> "Tindakan Perawat Ruangan",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "jasa_keperawatan",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>4</div>",
				"kode"		=> "552.13.03",
				"nama"		=> "Alat Kesehatan",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "alok",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>5</div>",
				"kode"		=> "552.13.09",
				"nama"		=> "Pendapatan Lain-Lain",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "pendapatan_lain",
				"filter"	=> ""
			)
		),
		"poli_paru"	=> array(
			array(
				"no"		=> "T",
				"kode"		=> "552.14.",
				"nama"		=> "Poli Paru",
				"debet"		=> "",
				"kredit"	=> "",
				"cmd"		=> "",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>1</div>",
				"kode"		=> "552.14.00",
				"nama"		=> "Sewa Kamar",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "sewa_kamar",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>2</div>",
				"kode"		=> "552.14.01",
				"nama"		=> "Visite / Jasa Dokter",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "jasa_dokter",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>3</div>",
				"kode"		=> "",
				"nama"		=> "Tindakan Perawat Ruangan",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "jasa_keperawatan",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>4</div>",
				"kode"		=> "552.14.03",
				"nama"		=> "Alat Kesehatan",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "alok",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>5</div>",
				"kode"		=> "552.14.09",
				"nama"		=> "Pendapatan Lain-Lain",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "pendapatan_lain",
				"filter"	=> ""
			)
		),
		"gizi"	=> array(
			array(
				"no"		=> "T",
				"kode"		=> "552.15.",
				"nama"		=> "Poli Gizi",
				"debet"		=> "",
				"kredit"	=> "",
				"cmd"		=> "",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>1</div>",
				"kode"		=> "552.15.00",
				"nama"		=> "Sewa Kamar",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "sewa_kamar",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>2</div>",
				"kode"		=> "552.15.01",
				"nama"		=> "Visite / Jasa Dokter",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "jasa_dokter",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>3</div>",
				"kode"		=> "552.15.02",
				"nama"		=> "Konsultasi Gizi",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "jasa_gizi",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>4</div>",
				"kode"		=> "552.15.03",
				"nama"		=> "Alat Kesehatan",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "alok",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>5</div>",
				"kode"		=> "552.15.09",
				"nama"		=> "Pendapatan Lain-Lain",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "pendapatan_lain",
				"filter"	=> ""
			)
		),
		"poli_penyakit_dalam"	=> array(
			array(
				"no"		=> "U",
				"kode"		=> "552.16.",
				"nama"		=> "Poli Penyakit Dalam",
				"debet"		=> "",
				"kredit"	=> "",
				"cmd"		=> "",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>1</div>",
				"kode"		=> "552.16.00",
				"nama"		=> "Sewa Kamar",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "sewa_kamar",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>2</div>",
				"kode"		=> "552.16.01",
				"nama"		=> "Visite / Jasa Dokter",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "jasa_dokter",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>3</div>",
				"kode"		=> "",
				"nama"		=> "Tindakan Perawat Ruangan",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "jasa_keperawatan",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>4</div>",
				"kode"		=> "552.16.03",
				"nama"		=> "Alat Kesehatan",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "alok",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>5</div>",
				"kode"		=> "552.16.09",
				"nama"		=> "Pendapatan Lain-Lain",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "pendapatan_lain",
				"filter"	=> ""
			)
		),
		"poli_urologi"	=> array(
			array(
				"no"		=> "V",
				"kode"		=> "552.17.",
				"nama"		=> "Poli Urologi",
				"debet"		=> "",
				"kredit"	=> "",
				"cmd"		=> "",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>1</div>",
				"kode"		=> "552.17.00",
				"nama"		=> "Sewa Kamar",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "sewa_kamar",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>2</div>",
				"kode"		=> "552.17.01",
				"nama"		=> "Visite / Jasa Dokter",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "jasa_dokter",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>3</div>",
				"kode"		=> "",
				"nama"		=> "Tindakan Perawat Ruangan",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "jasa_keperawatan",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>4</div>",
				"kode"		=> "552.17.03",
				"nama"		=> "Alat Kesehatan",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "alok",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>5</div>",
				"kode"		=> "552.17.09",
				"nama"		=> "Pendapatan Lain-Lain",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "pendapatan_lain",
				"filter"	=> ""
			)
		),
		"hemodialisa"	=> array(
			array(
				"no"		=> "W",
				"kode"		=> "552.18.",
				"nama"		=> "Hemodialisa",
				"debet"		=> "",
				"kredit"	=> "",
				"cmd"		=> "",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>1</div>",
				"kode"		=> "552.18.00",
				"nama"		=> "Sewa Kamar",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "sewa_kamar",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>2</div>",
				"kode"		=> "552.18.01",
				"nama"		=> "Visite / Jasa Dokter",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "jasa_dokter",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>3</div>",
				"kode"		=> "",
				"nama"		=> "Tindakan Perawat Ruangan",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "jasa_keperawatan",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>4</div>",
				"kode"		=> "552.18.03",
				"nama"		=> "Alat Kesehatan",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "alok",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>5</div>",
				"kode"		=> "552.18.09",
				"nama"		=> "Pendapatan Lain-Lain",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "pendapatan_lain",
				"filter"	=> ""
			)
		),
		"kamar_operasi"	=> array(
			array(
				"no"		=> "X",
				"kode"		=> "553.",
				"nama"		=> "Kamar Operasi",
				"debet"		=> "",
				"kredit"	=> "",
				"cmd"		=> "",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>1</div>",
				"kode"		=> "553.00.",
				"nama"		=> "Sewa Kamar",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "sewa_kamar",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>2</div>",
				"kode"		=> "553.01.",
				"nama"		=> "Operasi",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "ok",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>3</div>",
				"kode"		=> "",
				"nama"		=> "Tindakan Perawat Ruangan",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "anasthesi",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>4</div>",
				"kode"		=> "553.03.",
				"nama"		=> "Materi / Alat Kesehatan",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "alok",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>5</div>",
				"kode"		=> "553.04.",
				"nama"		=> "Anasthesi",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "anasthesi",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>6</div>",
				"kode"		=> "553.09.",
				"nama"		=> "Pendapatan Lain-Lain",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "pendapatan_lain",
				"filter"	=> ""
			)
		),
		"rpkk"	=> array(
			array(
				"no"		=> "Y",
				"kode"		=> "554.",
				"nama"		=> "RPKK",
				"debet"		=> "",
				"kredit"	=> "",
				"cmd"		=> "",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>1</div>",
				"kode"		=> "554.00.",
				"nama"		=> "Sewa Kamar",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "sewa_kamar",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>2</div>",
				"kode"		=> "554.01.",
				"nama"		=> "Visite / Jasa Dokter",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "jasa_dokter",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>3</div>",
				"kode"		=> "554.02.",
				"nama"		=> "Jasa / Tindakan Kebidanan",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "jasa_keperawatan",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>4</div>",
				"kode"		=> "554.03",
				"nama"		=> "Alat Kesehatan",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "alok",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>5</div>",
				"kode"		=> "554.04",
				"nama"		=> "Persalinan Normal",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "persalinan",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>6</div>",
				"kode"		=> "554.09.",
				"nama"		=> "Pendapatan Lain-Lain",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "pendapatan_lain",
				"filter"	=> ""
			)
		),
		"laboratory"	=> array(
			array(
				"no"		=> "Z",
				"kode"		=> "555.",
				"nama"		=> "Laboratorium",
				"debet"		=> "",
				"kredit"	=> "",
				"cmd"		=> "",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>1</div>",
				"kode"		=> "555.01.",
				"nama"		=> "Pemeriksaan Rutin",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "pemeriksaan_rutin",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>2</div>",
				"kode"		=> "555.02.",
				"nama"		=> "Pemeriksaan Khusus",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "pemeriksaan_khusus",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>3</div>",
				"kode"		=> "555.03.",
				"nama"		=> "Blood Taping dan Blood Bag (PMI)",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "darah_pmi",
				"filter"	=> ""
			)
		),
		"depo_farmasi"	=> array(
			array(
				"no"		=> "AA",
				"kode"		=> "556.",
				"nama"		=> "Farmasi",
				"debet"		=> "",
				"kredit"	=> "",
				"cmd"		=> "",

				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>1</div>",
				"kode"		=> "556.01.00",
				"nama"		=> "Penjualan Obat-Obatan Rawat Inap",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "penjualan_rawat_inap",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>2</div>",
				"kode"		=> "556.01.01",
				"nama"		=> "Penjualan Obat-Obatan Rawat Jalan Umum",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "penjualan_rawat_jalan",
				"filter"	=> "umum"
			),
			array(
				"no"		=> "<div align='right'>3</div>",
				"kode"		=> "",
				"nama"		=> "Penjualan Obat-Obatan Rawat Jalan Manfaat",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "penjualan_rawat_jalan",
				"filter"	=> "manfaat"
			),
			array(
				"no"		=> "<div align='right'>4</div>",
				"kode"		=> "",
				"nama"		=> "Penjualan Obat-Obatan Rawat Jalan Kronis",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "penjualan_rawat_jalan",
				"filter"	=> "kronis"
			),
			array(
				"no"		=> "<div align='right'>5</div>",
				"kode"		=> "",
				"nama"		=> "Penjualan Obat-Obatan Rawat Jalan INA-CBG's",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "penjualan_rawat_jalan",
				"filter"	=> "inacbgs"
			),
			array(
				"no"		=> "<div align='right'>6</div>",
				"kode"		=> "",
				"nama"		=> "Penjualan Obat-Obatan Rawat Jalan Tidak Ditanggung",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "penjualan_rawat_jalan",
				"filter"	=> "tidak_ditanggung"
			),
			array(
				"no"		=> "<div align='right'>7</div>",
				"kode"		=> "556.02.",
				"nama"		=> "Embalase (Racik Obat)",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "embalase",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>8</div>",
				"kode"		=> "",
				"nama"		=> "Penjualan Obat CITO",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "penjualan_cito",
				"filter"	=> ""
			)
		),
		"radiology"	=> array(
			array(
				"no"		=> "AB",
				"kode"		=> "557.",
				"nama"		=> "Radiologi",
				"debet"		=> "",
				"kredit"	=> "",
				"cmd"		=> "",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>1</div>",
				"kode"		=> "557.01.",
				"nama"		=> "X-Ray Film",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "x_ray_film",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>2</div>",
				"kode"		=> "557.02.",
				"nama"		=> "CT-Scan",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "ct_scan",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>3</div>",
				"kode"		=> "557.03.",
				"nama"		=> "USG / Echo Cardiography",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "echo",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>4</div>",
				"kode"		=> "",
				"nama"		=> "Visite / Jasa Dokter",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "jasa_dokter",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>5</div>",
				"kode"		=> "",
				"nama"		=> "Lain-Lain",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "lain_lain",
				"filter"	=> ""
			)
		),
		"lain_lain"	=> array(
			array(
				"no"		=> "AC",
				"kode"		=> "560.",
				"nama"		=> "Pendapatan Lain-Lain",
				"debet"		=> "",
				"kredit"	=> "",
				"cmd"		=> "",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>1</div>",
				"kode"		=> "560.01.",
				"nama"		=> "Kendaraan / Ambulance",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "ambulan",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>2</div>",
				"kode"		=> "560.02.",
				"nama"		=> "Administrasi",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "administrasi",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>3</div>",
				"kode"		=> "159.11.700",
				"nama"		=> "Billing Rawat Jalan",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "billing_rawat_jalan",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>4</div>",
				"kode"		=> "159.11.701",
				"nama"		=> "Billing Rawat Inap",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "billing_rawat_inap",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>5</div>",
				"kode"		=> "560.04.",
				"nama"		=> "Lain-Lain / EDC",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "pendapatan_lain",
				"filter"	=> ""
			),
			array(
				"no"		=> "<div align='right'>6</div>",
				"kode"		=> "",
				"nama"		=> "Sewa Alat Dokter",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "sewa_alat_dokter",
				"filter"	=> ""
			)
		),
		"kasir"	=> array(
			array(
				"no"		=> "AD",
				"kode"		=> "134.00.00",
				"nama"		=> "Pasien Umum",
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "tunai",
				"filter"	=> "umum"
			)
		)
	);
	$nomor = "AE";
	$jurnal_items['kasir'][] = array(
		"no"		=> $nomor++,
		"kode"		=> "134.",
		"nama"		=> "Piutang Rekanan",
		"debet"		=> "",
		"kredit"	=> "",
		"cmd"		=> "label",
		"filter"	=> "label"
	);
	$numerik = 1;
	global $db;
	$perusahaan_rows = $db->get_result("
		SELECT *
		FROM smis_rg_perusahaan
		WHERE prop NOT LIKE 'del' AND kode LIKE '134.%'
	");
	if ($perusahaan_rows != null) {
		foreach ($perusahaan_rows as $pr)
			$jurnal_items['kasir'][] = array(
				"no"		=> "<div align='right'>" . $numerik++ . "</div>",
				"kode"		=> $pr->kode,
				"nama"		=> $pr->nama,
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "rekanan",
				"filter"	=> "perusahaan_" . $pr->id
			);
	}
	$asuransi_rows = $db->get_result("
		SELECT *
		FROM smis_rg_asuransi
		WHERE prop NOT LIKE 'del' AND kode LIKE '134.%'
	");
	if ($asuransi_rows != null) {
		foreach ($asuransi_rows as $ar)
			$jurnal_items['kasir'][] = array(
				"no"		=> "<div align='right'>" . $numerik++ . "</div>",
				"kode"		=> $ar->kode,
				"nama"		=> $ar->nama,
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "rekanan",
				"filter"	=> "asuransi_" . $ar->id
			);
	}
	$jurnal_items['kasir'][] = array(
		"no"		=> $nomor++,
		"kode"		=> "136.01",
		"nama"		=> "Piutang PTPN",
		"debet"		=> "",
		"kredit"	=> "",
		"cmd"		=> "label",
		"filter"	=> "label"
	);
	$numerik = 1;
	$perusahaan_rows = $db->get_result("
		SELECT *
		FROM smis_rg_perusahaan
		WHERE prop NOT LIKE 'del' AND kode NOT LIKE '134.%'
	");
	if ($perusahaan_rows != null) {
		foreach ($perusahaan_rows as $pr)
			$jurnal_items['kasir'][] = array(
				"no"		=> "<div align='right'>" . $numerik++ . "</div>",
				"kode"		=> $pr->kode,
				"nama"		=> $pr->nama,
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "rekanan",
				"filter"	=> "perusahaan_" . $pr->id
			);
	}
	$asuransi_rows = $db->get_result("
		SELECT *
		FROM smis_rg_asuransi
		WHERE prop NOT LIKE 'del' AND kode NOT LIKE '134.%'
	");
	if ($asuransi_rows != null) {
		foreach ($asuransi_rows as $ar)
			$jurnal_items['kasir'][] = array(
				"no"		=> "<div align='right'>" . $numerik++ . "</div>",
				"kode"		=> $ar->kode,
				"nama"		=> $ar->nama,
				"debet"		=> "<div align='right'>0,00</div>",
				"kredit"	=> "<div align='right'>0,00</div>",
				"cmd"		=> "rekanan",
				"filter"	=> "asuransi_" . $ar->id
			);
	}
	$jurnal_items['kasir'][] = array(
		"no"		=> $nomor++,
		"kode"		=> "100.01",
		"nama"		=> "Kas",
		"debet"		=> "<div align='right'>0,00</div>",
		"kredit"	=> "<div align='right'>0,00</div>",
		"cmd"		=> "",
		"filter"	=> ""
	);
	$jurnal_items['kasir'][] = array(
		"no"		=> $nomor++,
		"kode"		=> "110.01.001",
		"nama"		=> "Bank Mandiri",
		"debet"		=> "<div align='right'>0,00</div>",
		"kredit"	=> "<div align='right'>0,00</div>",
		"cmd"		=> "bank",
		"filter"	=> "bank_mandiri"
	);
	$jurnal_items['kasir'][] = array(
		"no"		=> $nomor++,
		"kode"		=> "110.01.001",
		"nama"		=> "Bank Mandiri - EDC",
		"debet"		=> "<div align='right'>0,00</div>",
		"kredit"	=> "<div align='right'>0,00</div>",
		"cmd"		=> "bank",
		"filter"	=> "bank_mandiri_edc"
	);
	$jurnal_items['kasir'][] = array(
		"no"		=> $nomor++,
		"kode"		=> "110.01.100",
		"nama"		=> "BRI",
		"debet"		=> "<div align='right'>0,00</div>",
		"kredit"	=> "<div align='right'>0,00</div>",
		"cmd"		=> "bank",
		"filter"	=> "bri"
	);
	$jurnal_items['kasir'][] = array(
		"no"		=> $nomor++,
		"kode"		=> "110.01.100",
		"nama"		=> "BRI - EDC",
		"debet"		=> "<div align='right'>0,00</div>",
		"kredit"	=> "<div align='right'>0,00</div>",
		"cmd"		=> "bank",
		"filter"	=> "bri_edc"
	);
	$jurnal_items['kasir'][] = array(
		"no"		=> $nomor++,
		"kode"		=> "110.01.202",
		"nama"		=> "BCA",
		"debet"		=> "<div align='right'>0,00</div>",
		"kredit"	=> "<div align='right'>0,00</div>",
		"cmd"		=> "bank",
		"filter"	=> "bca"
	);
	$jurnal_items['kasir'][] = array(
		"no"		=> $nomor++,
		"kode"		=> "110.01.202",
		"nama"		=> "BCA - EDC",
		"debet"		=> "<div align='right'>0,00</div>",
		"kredit"	=> "<div align='right'>0,00</div>",
		"cmd"		=> "bank",
		"cmd"		=> "bca_edc"
	);
	$jurnal_items['kasir'][] = array(
		"no"		=> $nomor++,
		"kode"		=> "",
		"nama"		=> "REKANAN LAIN : NON-PTPN XII",
		"debet"		=> "<div align='right'>0,00</div>",
		"kredit"	=> "<div align='right'>0,00</div>",
		"cmd"		=> "rekanan",
		"filter"	=> "rekanan_lain_non_ptpn_xii"
	);
	$jurnal_items['kasir'][] = array(
		"no"		=> $nomor++,
		"kode"		=> "",
		"nama"		=> "REKANAN LAIN : PTPN XII",
		"debet"		=> "<div align='right'>0,00</div>",
		"kredit"	=> "<div align='right'>0,00</div>",
		"cmd"		=> "rekanan",
		"filter"	=> "rekanan_lain_ptpn_xii"
	);
	$jurnal_items['kasir'][] = array(
		"no"		=> $nomor++,
		"kode"		=> "",
		"nama"		=> "TAMBAHAN BIAYA LAIN",
		"debet"		=> "<div align='right'>0,00</div>",
		"kredit"	=> "<div align='right'>0,00</div>",
		"cmd"		=> "error",
		"filter"	=> ""
	);	
	$html = "";
	foreach ($jurnal_items as $ji_key => $ji_value) {
		foreach ($ji_value as $ji_k => $ji_v) {
			$html .= "<tr id='" . $ji_key . "'>";
			foreach ($ji_v as $k => $v) {
				if ($k == "cmd" || $k == "filter")
					$html .= "<td style='display: none;' id='" . $ji_key . "-" . $ji_k . "-" . $k . "'><small>" . $v . "</small></td>";
				else
					$html .= "<td id='" . $ji_key . "-" . $ji_k . "-" . $k . "'><small>" . $v . "</small></td>";
			}
			$html .= "</tr>";
		}
	}
	$data = array();
	$data['html'] = $html;
	echo json_encode($data);
?>