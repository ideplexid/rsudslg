<?php
	require_once("smis-libs-out/php-excel/PHPExcel.php");
	
	$objPHPExcel = PHPExcel_IOFactory::load("kasir/jurnal_2016/templates/template_jurnal_05.xlsx");
	
	$objPHPExcel->setActiveSheetIndexByName("Jurnal 05");
	$objWorksheet = $objPHPExcel->getActiveSheet();
	$objWorksheet->setTitle("Jurnal 07");
	if ($_POST['num_rows'] - 1 > 0)
		$objWorksheet->insertNewRowBefore(8, $_POST['num_rows'] - 1);
	
	$objWorksheet->setCellValue("D1", "JURNAL 07");
	$objWorksheet->setCellValue("D3", "JURNAL REKAP JASA PELAYANAN");
	$objWorksheet->setCellValue("D4", "TGL. " . ArrayAdapter::format("date d-m-Y", $_POST['from']) . " s/d " . ArrayAdapter::format("date d-m-Y", $_POST['to']));
	
	$data = json_decode($_POST['d_data']);
	$start_row_num = 7;
	$end_row_num = 7;
	$row_num = $start_row_num;
	foreach ($data as $d) {
		$col_num = 0;
		$objWorksheet->getCellByColumnAndRow($col_num, $row_num)->setValueExplicit($d->kode_akun, PHPExcel_Cell_DataType::TYPE_STRING);
		if (is_numeric($d->nomor))
			$objWorksheet->getStyle(PHPExcel_Cell::stringFromColumnIndex($col_num) . $row_num)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
		$col_num++;
		$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->nama_akun);
		$col_num++;
		$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->debet);
		$col_num++;
		$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->kredit);
		$row_num++;
		$end_row_num++;
	}
	$row_num++;
	$end_row_num++;
	$objWorksheet->setCellValueByColumnAndRow(2, $row_num, $_POST['total_debet']);
	$objWorksheet->setCellValueByColumnAndRow(3, $row_num, $_POST['total_kredit']);
	
	$objDrawing = new PHPExcel_Worksheet_Drawing();
	$objDrawing->setPath("kasir/jurnal_2016/resources/logo_1_5.png");
	$objDrawing->setCoordinates("A1");
	$objDrawing->setOffsetX(0);
	$objDrawing->setWorksheet($objWorksheet);
	
	header("Content-type: application/vnd.ms-excel");	
	header("Content-Disposition: attachment; filename=" . ArrayAdapter::format("slug", "JURNAL_07_2") . "_" . $_POST['from'] . "_" . $_POST['to'] . "_" . date("Ymd_His") . ".xlsx");
	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");
	$objWriter->save("php://output");
?>