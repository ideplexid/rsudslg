<?php
	require_once 'smis-base/smis-include-service-consumer.php';
	global $db;
	
	$from = $_POST['from'];
	$to = $_POST['to'];
	$id_dokter = $_POST['id_dokter'];
	$hak_dokter = $_POST['hak_dokter'];
	$no_jurnal = $_POST['no_jurnal'];
	$num = $_POST['num'];
	$uri = "%%";
	$jenis_pasien = "%%";
	
	/// Mendapatkan informasi Pasien
	/// Service Provider Unit: Pendaftaran (registration)
	$params = array();
	$params['from'] = $from;
	$params['to'] = $to;
	$params['jenis_pasien'] = $jenis_pasien;
	$params['num'] = $num;
	$params['uri'] = $uri;
	$params['grup_transaksi'] = $grup_transaksi;
	$consumer_service = new ServiceConsumer(
		$db,
		"get_info_pasien",
		$params,
		"registration"
	);
	$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
	$content = $consumer_service->execute()->getContent();
	$noreg_pasien = $content[0];
	$nrm_pasien = $content[1];
	$nama_pasien = $content[2];
	$tanggal_daftar_pasien = ArrayAdapter::format("date d-m-Y", $content[3]);
	$jenis_pasien = $content[4];
	$nama_perusahaan = $content[5];
	$nama_asuransi = $content[6];
	$id_perusahaan = $content[7];
	$id_asuransi = $content[8];
	
	$jasa_medis = 0;
	$params = array();
	$params['noreg_pasien'] = $noreg_pasien;
	$params['id_dokter_csv'] = $id_dokter;
	$consumer_service = new ServiceConsumer(
		$db,
		"get_jasa_dokter_jurnal",
		$params
	);
	$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
	$content = $consumer_service->execute()->getContent();
	if ($content != null) {
		foreach ($content as $c)
			$jasa_medis += $c['jasa_dokter'];
	}
	$html = "";
	if ($jasa_medis > 0) {
		$bruto = $jasa_medis;
		$jasa_medis_bruto = $hak_dokter * $bruto / 100;
		$jasa_rs = $bruto - $jasa_medis_bruto;
		$dpp = 0.5 * $jasa_medis_bruto;
		$pph_21 = 0.3;
		if ($bruto < 50000000)
			$pph_21 = 0.05;
		else if ($bruto < 250000000)
			$pph_21 = 0.15;
		else if ($bruto < 500000000)
			$pph_21 = 0.25;
		$pph_21 = $pph_21 * $dpp;
		$jasa_medis_netto = $jasa_medis_bruto - $pph_21;
		$html = "
			<tr id='data_" . $noreg_pasien . "'>
				<td id='data_" . $noreg_pasien . "_nomor'></td>
				<td id='data_" . $noreg_pasien . "_noreg_pasien'><small>" . ArrayAdapter::format("only-digit6", $noreg_pasien) . "</td>
				<td id='data_" . $noreg_pasien . "_nama_pasien'><small>" . strtoupper($nama_pasien) . "</td>
				<td id='data_" . $noreg_pasien . "_nrm_pasien'><small>" . ArrayAdapter::format("only-digit6", $nrm_pasien) . "</td>
				<td id='data_" . $noreg_pasien . "_tanggal_layanan'><small>" . $tanggal_daftar_pasien . "</td>
				<td id='data_" . $noreg_pasien . "_bruto'><small><div align='right'>" . ArrayAdapter::format("only-money", $bruto) . "</div></td>
				<td id='data_" . $noreg_pasien . "_jasa_rs'><small><div align='right'>" . ArrayAdapter::format("only-money", $jasa_rs) . "</div></td>
				<td id='data_" . $noreg_pasien . "_jasa_medis_bruto'><small><div align='right'>" . ArrayAdapter::format("only-money", $jasa_medis_bruto) . "</div></td>
				<td id='data_" . $noreg_pasien . "_dpp'><small><div align='right'>" . ArrayAdapter::format("only-money", $dpp) . "</div></td>
				<td id='data_" . $noreg_pasien . "_pph_21'><small><div align='right'>" . ArrayAdapter::format("only-money", $pph_21) . "</div></td>				
				<td id='data_" . $noreg_pasien . "_jasa_medis_netto'><small><div align='right'>" . ArrayAdapter::format("only-money", $jasa_medis_netto) . "</div></td>
				<td id='data_" . $noreg_pasien . "_no_jurnal'><small>" . $no_jurnal . "</td>
			</tr>
		";
	}
	
	$data = array();
	$data['noreg_pasien'] = ArrayAdapter::format("only-digit6", $noreg_pasien);
	$data['nrm_pasien'] = ArrayAdapter::format("only-digit6", $nrm_pasien);
	$data['nama_pasien'] = ArrayAdapter::format("unslug", $nama_pasien);
	$data['tanggal_daftar_pasien'] = $tanggal_daftar_pasien;
	$data['jenis_pasien'] = ArrayAdapter::format("unslug", $jenis_pasien);
	$data['nama_perusahaan'] = $nama_perusahaan;
	$data['nama_asuransi'] = $nama_asuransi;
	$data['html'] = $html;
	
	echo json_encode($data);
?>