var jurnal_03a;
var jurnal_03a_operator;
var FINISHED;
$(document).ready(function() {
	$('.mydatetime').datetimepicker({ 
		minuteStep: 1
	});
	$("#jurnal_03a_modal").on("show", function() {
		$("a.close").hide();
	});
	$("tbody#jurnal_03a_list").append(
		"<tr>" +
			"<td colspan='8'><center><small><strong>JURNAL BELUM DIPROSES</strong></small></center></td>" +
		"</tr>"
	);
	jurnal_03a_operator = new Jurnal03AOperatorAction(
		"jurnal_03a_operator",
		"kasir",
		"jurnal_2016/jurnal_03a",
		new Array()
	);
	jurnal_03a_operator.setSuperCommand("jurnal_03a_operator");
	jurnal_03a = new Jurnal03AAction(
		"jurnal_03a",
		"kasir",
		"jurnal_2016/jurnal_03a",
		new Array()
	);
});
$(document).keyup(function(e) {
	if (e.which == 27)
		FINISHED = true;
});