var jurnal_07;
var FINISHED;
$(document).ready(function() {
	$(".mydatetime").datetimepicker({ 
		minuteStep: 1
	});
	$("#jurnal_07_modal").on("show", function() {
		$("a.close").hide();
	});
	$("tbody#jurnal_07_list").append(
		"<tr>" +
			"<td colspan='9'><center><small><strong>JURNAL BELUM DIPROSES</strong></small></center></td>" +
		"</tr>"
	);
	jurnal_07 = new Jurnal07Action(
		"jurnal_07",
		"kasir",
		"jurnal_2016/jurnal_07",
		new Array()
	);
});
$(document).keyup(function(e) {
	if (e.which == 27)
		FINISHED = true;
});