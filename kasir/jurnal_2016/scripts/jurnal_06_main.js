var jurnal_06;
var FINISHED;
$(document).ready(function() {
	$(".mydatetime").datetimepicker({ 
		minuteStep: 1
	});
	$("#jurnal_06_modal").on("show", function() {
		$("a.close").hide();
	});
	$("tbody#jurnal_06_list").append(
		"<tr>" +
			"<td colspan='5'><center><small><strong>JURNAL BELUM DIPROSES</strong></small></center></td>" +
		"</tr>"
	);
	jurnal_06 = new Jurnal06Action(
		"jurnal_06",
		"kasir",
		"jurnal_2016/jurnal_06",
		new Array()
	);
});
$(document).keyup(function(e) {
	if (e.which == 27)
		FINISHED = true;
});