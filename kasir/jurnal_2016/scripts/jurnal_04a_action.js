function Jurnal04AAction(name, page, action, column) {
	this.initialize(name, page, action, column);
}
Jurnal04AAction.prototype.constructor = Jurnal04AAction;
Jurnal04AAction.prototype = new TableAction();
Jurnal04AAction.prototype.getRegulerData = function() {
	var data = TableAction.prototype.getRegulerData.call(this);
	data['from'] = $("#jurnal_04a_from").val();
	data['to'] = $("#jurnal_04a_to").val();
	data['jenis_pasien'] = $("#jurnal_04a_jenis_pasien").val();
	data['operator'] = $("#jurnal_04a_operator").val();
	if (data['operator'] == "")
		data['operator'] = "%%";
	return data;
};
Jurnal04AAction.prototype.view = function() {
	var self = this;
	$("#jurnal_04a_info").html("");
	$("#jurnal_04a_loading_bar").sload("true", "Harap ditunggu...", 0);
	$("#jurnal_04a_modal").smodal("show");
	FINISHED = false;
	var data = this.getRegulerData();
	data['command'] = "patient_number";
	$.post(
		"",
		data,
		function(response) {
			var json = JSON.parse(response);
			$("#jurnal_04a_list").empty();
			self.fillHtml(0, json.jumlah);
		}
	);
}
Jurnal04AAction.prototype.fillHtml = function(num, limit) {
	if (FINISHED || num == limit) {
		$("#jurnal_04_loading_bar").sload("true", "Harap ditunggu...", 0);
		if (FINISHED == false && num == limit) {
			this.finalize();
		} else {
			$("#jurnal_04a_modal").smodal("hide");
			$("#jurnal_04a_info").html(
				"<div class='alert alert-block alert-inverse'>" +
					"<center><strong>PROSES DIBATALKAN</strong></center>" +
				"</div>"
			);
			$("#jurnal_04_export_button").removeAttr("onclick");
		}
		return;
	}
	var self = this;
	var data = this.getRegulerData();
	data['command'] = "patient_info";
	data['num'] = num;
	data['limit'] = limit;
	$.post(
		"",
		data,
		function(response) {
			var json = JSON.parse(response);
			if (parseFloat(json.html)  != "") {
				$("tbody#jurnal_04a_list").append(json.html);
				$("#jurnal_04a_loading_bar").sload("true", json.noreg_pasien + " - " + json.nrm_pasien + " - " + json.nama_pasien + " (" + (num + 1) + " / " + limit + ")", (num + 1) * 100 / limit - 1);
			}
			self.fillHtml(num + 1, limit);
		}
	);
};
Jurnal04AAction.prototype.finalize = function() {
	$("#jurnal_04_loading_bar").sload("true", "Finalisasi...", 100);
	var num_rows = $("tbody#jurnal_04a_list").children("tr").length;
	var total = 0;
	for(var i = 0; i < num_rows; i++) {
		$("tbody#jurnal_04a_list tr:eq(" + i + ") td:eq(0)").html("<small>" + (i + 1) + "</small>");
		var prefix = $("tbody#jurnal_04a_list tr:eq(" + i + ")").prop("id");		
		var tagihan_obat = parseFloat($("#" + prefix + "_tagihan_obat").text().replace(/[^0-9-,]/g, '').replace(",", "."));
		total += tagihan_obat;
	}
	$("#jurnal_04a_list").append(
		"<tr>" +
			"<td colspan='7'><center><small><strong>T O T A L</strong></small></center></td>" +
			"<td><small><strong><div class='right money-value'>" + parseFloat(total).formatMoney("2", ".", ",") + "</div></strong></small></td>" +
		"</tr>"
	);
	$("#jurnal_04a_modal").smodal("hide");
	$("#jurnal_04a_info").html(
		"<div class='alert alert-block alert-info'>" +
			"<center><strong>PROSES SELESAI</strong></center>" + 
		"</div>"
	);
	$("#jurnal_04a_export_button").removeAttr("onclick");
	$("#jurnal_04a_export_button").attr("onclick", "jurnal_04a.export_excel()");
};
Jurnal04AAction.prototype.export_excel = function() {
	var num_rows = $("#jurnal_04a_list").children("tr").length - 1;
	var d_data = {};
	for (var i = 0; i < num_rows; i++) {
		var prefix = $("tbody#jurnal_04a_list").children("tr").eq(i).prop("id");
		var noreg_pasien = $("#" + prefix + "_noreg").text();
		var nrm_pasien = $("#" + prefix + "_nrm").text();
		var nama_pasien = $("#" + prefix + "_nama").text();
		var tanggal_setoran = $("#" + prefix + "_tanggal_setoran").text();
		var nomor_bukti_setoran = $("#" + prefix + "_nomor_bukti_setoran").text();
		var status = $("#" + prefix + "_status").text();
		var tagihan_obat = $("#" + prefix + "_tagihan_obat").text().replace(/[^0-9-,]/g, '').replace(",", ".");
		var dd_data = {};
		dd_data['noreg_pasien'] = noreg_pasien;
		dd_data['nrm_pasien'] = nrm_pasien;
		dd_data['nama_pasien'] = nama_pasien;
		dd_data['tanggal_setoran'] = tanggal_setoran;
		dd_data['nomor_bukti_setoran'] = nomor_bukti_setoran;
		dd_data['status'] = status;
		dd_data['tagihan_obat'] = tagihan_obat;
		
		d_data[i] = dd_data;
	}
	var data = this.getRegulerData();
	data['command'] = "export_excel";
	data['num_rows'] = num_rows;
	data['d_data'] = JSON.stringify(d_data);
	postForm(data);
};
Jurnal04AAction.prototype.cancel = function() {
	FINISHED = true;
};