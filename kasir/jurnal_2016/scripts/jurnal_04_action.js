function Jurnal04Action(name, page, action, column) {
	this.initialize(name, page, action, column);
}
Jurnal04Action.prototype.constructor = Jurnal04Action;
Jurnal04Action.prototype = new TableAction();
Jurnal04Action.prototype.getRegulerData = function() {
	var data = TableAction.prototype.getRegulerData.call(this);
	data['from'] = $("#jurnal_04_from").val();
	data['to'] = $("#jurnal_04_to").val();
	data['jenis_pasien'] = $("#jurnal_04_jenis_pasien").val();
	data['uri'] = $("#jurnal_04_uri").val();
	data['operator'] = $("#jurnal_04_operator").val();
	if (data['operator'] == "")
		data['operator'] = "%%";
	return data;
};
Jurnal04Action.prototype.view = function() {
	var self = this;
	$("#jurnal_04_info").html("");
	$("#jurnal_04_loading_bar").sload("true", "Harap ditunggu...", 0);
	$("#jurnal_04_modal").smodal("show");
	FINISHED = false;
	var data = this.getRegulerData();
	data['command'] = "patient_number";
	$.post(
		"",
		data,
		function(response) {
			var json = JSON.parse(response);
			$("#jurnal_04_list").empty();
			self.fillHtml(0, json.jumlah);
		}
	);
}
Jurnal04Action.prototype.fillHtml = function(num, limit) {
	if (FINISHED || num == limit) {
		$("#jurnal_04_loading_bar").sload("true", "Harap ditunggu...", 0);
		if (FINISHED == false && num == limit) {
			this.finalize();
		} else {
			$("#jurnal_04_modal").smodal("hide");
			$("#jurnal_04_info").html(
				"<div class='alert alert-block alert-inverse'>" +
					"<center><strong>PROSES DIBATALKAN</strong></center>" +
				"</div>"
			);
			$("#jurnal_04_export_button").removeAttr("onclick");
		}
		return;
	}
	var self = this;
	var data = this.getRegulerData();
	data['command'] = "patient_info";
	data['num'] = num;
	$.post(
		"",
		data,
		function(response) {
			var json = JSON.parse(response);
			if (parseFloat(json.noreg_pasien)  != 0) {
				$("tbody#jurnal_04_list").append(
					json.html
				);
				$("#jurnal_04_loading_bar").sload("true", json.noreg_pasien + " - " + json.nrm_pasien + " - " + json.nama_pasien + " (" + (num + 1) + " / " + limit + ")", (num + 1) * 100 / limit - 1);
			}
			self.fillHtml(num + 1, limit);
		}
	);
};
Jurnal04Action.prototype.finalize = function() {
	$("#jurnal_04_loading_bar").sload("true", "Finalisasi...", 100);
	var num_rows = $("tbody#jurnal_04_list").children("tr").length;
	var total_tagihan_non_ptpn_12 = 0;
	var total_tagihan_ptpn_12 = 0;
	var total_mandiri_edc = 0;
	var total_mandiri_tunai = 0;
	var total_bri_edc = 0;
	var total_bri_tunai = 0;
	var total_bca_edc = 0;
	var total_bca_tunai = 0;
	var total_tunai = 0;
	var total_total = 0;
	for(var i = 0; i < num_rows; i++) {
		$("tbody#jurnal_04_list tr:eq(" + i + ") td:eq(0)").html("<small>" + (i + 1) + "</small>");
		var prefix = $("tbody#jurnal_04_list tr:eq(" + i + ")").prop("id");		
		var tunai = parseFloat($("#" + prefix + "_tunai").text().replace(/[^0-9-,]/g, '').replace(",", "."));
		total_tunai += tunai;
		var mandiri_tunai = parseFloat($("#" + prefix + "_mandiri_tunai").text().replace(/[^0-9-,]/g, '').replace(",", "."));
		total_mandiri_tunai += mandiri_tunai;
		var mandiri_edc = parseFloat($("#" + prefix + "_mandiri_edc").text().replace(/[^0-9-,]/g, '').replace(",", "."));
		total_mandiri_edc += mandiri_edc;
		var bri_tunai = parseFloat($("#" + prefix + "_bri_tunai").text().replace(/[^0-9-,]/g, '').replace(",", "."));
		total_bri_tunai += bri_tunai;
		var bri_edc = parseFloat($("#" + prefix + "_bri_edc").text().replace(/[^0-9-,]/g, '').replace(",", "."));
		total_bri_edc += bri_edc;
		var bca_tunai = parseFloat($("#" + prefix + "_bca_tunai").text().replace(/[^0-9-,]/g, '').replace(",", "."));
		total_bca_tunai += bca_tunai;
		var bca_edc = parseFloat($("#" + prefix + "_bca_edc").text().replace(/[^0-9-,]/g, '').replace(",", "."));
		total_bca_edc += bca_edc;
		var tagihan_non_ptpn_12 = parseFloat($("#" + prefix + "_tagihan_non_ptpn_12").text().replace(/[^0-9-,]/g, '').replace(",", "."));
		total_tagihan_non_ptpn_12 += tagihan_non_ptpn_12;
		var tagihan_ptpn_12 = parseFloat($("#" + prefix + "_tagihan_ptpn_12").text().replace(/[^0-9-,]/g, '').replace(",", "."));
		total_tagihan_ptpn_12 += tagihan_ptpn_12;
		var total = parseFloat($("#" + prefix + "_total").text().replace(/[^0-9-,]/g, '').replace(",", "."));
		total_total += total;
	}
	$("#jurnal_04_list").append(
		"<tr>" +
			"<td colspan='7'><center><small><strong>T O T A L</strong></small></center></td>" +
			"<td><small><strong><div class='right money-value'>" + parseFloat(total_tunai).formatMoney("2", ".", ",") + "</div></strong></small></td>" +
			"<td><small><strong><div class='right money-value'>" + parseFloat(total_mandiri_tunai).formatMoney("2", ".", ",") + "</div></strong></small></td>" +
			"<td><small><strong><div class='right money-value'>" + parseFloat(total_mandiri_edc).formatMoney("2", ".", ",") + "</div></strong></small></td>" +
			"<td><small><strong><div class='right money-value'>" + parseFloat(total_bri_tunai).formatMoney("2", ".", ",") + "</div></strong></small></td>" +
			"<td><small><strong><div class='right money-value'>" + parseFloat(total_bri_edc).formatMoney("2", ".", ",") + "</div></strong></small></td>" +
			"<td><small><strong><div class='right money-value'>" + parseFloat(total_bca_tunai).formatMoney("2", ".", ",") + "</div></strong></small></td>" +
			"<td><small><strong><div class='right money-value'>" + parseFloat(total_bca_edc).formatMoney("2", ".", ",") + "</div></strong></small></td>" +
			"<td><small><strong><div class='right money-value'>" + parseFloat(total_tagihan_non_ptpn_12).formatMoney("2", ".", ",") + "</div></strong></small></td>" +
			"<td><small><strong><div class='right money-value'>" + parseFloat(total_tagihan_ptpn_12).formatMoney("2", ".", ",") + "</div></strong></small></td>" +
			"<td><small><strong><div class='right money-value'>" + parseFloat(total_total).formatMoney("2", ".", ",") + "</div></strong></small></td>" +
		"</tr>"
	);
	$("#jurnal_04_modal").smodal("hide");
	$("#jurnal_04_info").html(
		"<div class='alert alert-block alert-info'>" +
			"<center><strong>PROSES SELESAI</strong></center>" + 
		"</div>"
	);
	$("#jurnal_04_export_button").removeAttr("onclick");
	$("#jurnal_04_export_button").attr("onclick", "jurnal_04.export_excel()");
};
Jurnal04Action.prototype.export_excel = function() {
	var num_rows = $("#jurnal_04_list").children("tr").length - 1;
	var d_data = {};
	for (var i = 0; i < num_rows; i++) {
		var prefix = $("tbody#jurnal_04_list").children("tr").eq(i).prop("id");
		var noreg_pasien = $("#" + prefix + "_noreg").text();
		var nrm_pasien = $("#" + prefix + "_nrm").text();
		var nama_pasien = $("#" + prefix + "_nama").text();
		var jenis_pasien = $("#" + prefix + "_jenis").text();
		var perusahaan = $("#" + prefix + "_perusahaan").text();
		var asuransi = $("#" + prefix + "_asuransi").text();
		var tunai = $("#" + prefix + "_tunai").text().replace(/[^0-9-,]/g, '').replace(",", ".");
		var mandiri_tunai = $("#" + prefix + "_mandiri_tunai").text().replace(/[^0-9-,]/g, '').replace(",", ".");
		var mandiri_edc = $("#" + prefix + "_mandiri_edc").text().replace(/[^0-9-,]/g, '').replace(",", ".");
		var bri_tunai = $("#" + prefix + "_bri_tunai").text().replace(/[^0-9-,]/g, '').replace(",", ".");
		var bri_edc = $("#" + prefix + "_bri_edc").text().replace(/[^0-9-,]/g, '').replace(",", ".");
		var bca_tunai = $("#" + prefix + "_bca_tunai").text().replace(/[^0-9-,]/g, '').replace(",", ".");
		var bca_edc = $("#" + prefix + "_bca_edc").text().replace(/[^0-9-,]/g, '').replace(",", ".");
		var tagihan_non_ptpn_12 = $("#" + prefix + "_tagihan_non_ptpn_12").text().replace(/[^0-9-,]/g, '').replace(",", ".");
		var tagihan_ptpn_12 = $("#" + prefix + "_tagihan_ptpn_12").text().replace(/[^0-9-,]/g, '').replace(",", ".");
		var dd_data = {};
		dd_data['noreg_pasien'] = noreg_pasien;
		dd_data['nrm_pasien'] = nrm_pasien;
		dd_data['nama_pasien'] = nama_pasien;
		dd_data['jenis_pasien'] = jenis_pasien;
		dd_data['perusahaan'] = perusahaan;
		dd_data['asuransi'] = asuransi;
		dd_data['tunai'] = tunai;
		dd_data['mandiri_tunai'] = mandiri_tunai;
		dd_data['mandiri_edc'] = mandiri_edc;
		dd_data['bri_tunai'] = bri_tunai;
		dd_data['bri_edc'] = bri_edc;
		dd_data['bca_tunai'] = bca_tunai;
		dd_data['bca_edc'] = bca_edc;
		dd_data['tagihan_non_ptpn_12'] = tagihan_non_ptpn_12;
		dd_data['tagihan_ptpn_12'] = tagihan_ptpn_12;
		
		d_data[i] = dd_data;
	}
	var data = this.getRegulerData();
	data['command'] = "export_excel";
	data['num_rows'] = num_rows;
	data['d_data'] = JSON.stringify(d_data);
	postForm(data);
};
Jurnal04Action.prototype.cancel = function() {
	FINISHED = true;
};