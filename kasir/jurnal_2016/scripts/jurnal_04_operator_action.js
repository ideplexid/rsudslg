function Jurnal04OperatorAction(name, page, action, column) {
	this.initialize(name, page, action, column);
}
Jurnal04OperatorAction.prototype.constructor = Jurnal04OperatorAction;
Jurnal04OperatorAction.prototype = new TableAction();
Jurnal04OperatorAction.prototype.selected = function(json) {
	$("#jurnal_04_operator").val(json.operator);
};