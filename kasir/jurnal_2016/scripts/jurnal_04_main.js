var jurnal_04;
var jurnal_04_operator;
var FINISHED;
$(document).ready(function() {
	$('.mydatetime').datetimepicker({ 
		minuteStep: 1
	});
	$("#jurnal_04_modal").on("show", function() {
		$("a.close").hide();
	});
	$("tbody#jurnal_04_list").append(
		"<tr>" +
			"<td colspan='17'><center><small><strong>JURNAL BELUM DIPROSES</strong></small></center></td>" +
		"</tr>"
	);
	jurnal_04_operator = new Jurnal04OperatorAction(
		"jurnal_04_operator",
		"kasir",
		"jurnal_2016/jurnal_04",
		new Array()
	);
	jurnal_04_operator.setSuperCommand("jurnal_04_operator");
	jurnal_04 = new Jurnal04Action(
		"jurnal_04",
		"kasir",
		"jurnal_2016/jurnal_04",
		new Array()
	);
});
$(document).keyup(function(e) {
	if (e.which == 27)
		FINISHED = true;
});