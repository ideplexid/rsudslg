function Jurnal03AAction(name, page, action, column) {
	this.initialize(name, page, action, column);
}
Jurnal03AAction.prototype.constructor = Jurnal03AAction;
Jurnal03AAction.prototype = new TableAction();
Jurnal03AAction.prototype.getRegulerData = function() {
	var data = TableAction.prototype.getRegulerData.call(this);
	data['from'] = $("#jurnal_03a_from").val();
	data['to'] = $("#jurnal_03a_to").val();
	data['jenis_pasien'] = $("#jurnal_03a_jenis_pasien").val();
	data['operator'] = $("#jurnal_03a_operator").val();
	if (data['operator'] == "")
		data['operator'] = "%%";
	return data;
};
Jurnal03AAction.prototype.view = function() {
	var self = this;
	$("#jurnal_03a_info").html("");
	$("#jurnal_03a_loading_bar").sload("true", "Harap ditunggu...", 0);
	$("#jurnal_03a_modal").smodal("show");
	FINISHED = false;
	var data = this.getRegulerData();
	data['command'] = "patient_number";
	$.post(
		"",
		data,
		function(response) {
			var json = JSON.parse(response);
			$("#jurnal_03a_list").empty();
			self.fillHtml(0, json.jumlah);
		}
	);
}
Jurnal03AAction.prototype.fillHtml = function(num, limit) {
	if (FINISHED || num == limit) {
		$("#jurnal_03a_loading_bar").sload("true", "Harap ditunggu...", 0);
		if (FINISHED == false && num == limit) {
			this.finalize();
		} else {
			$("#jurnal_03a_modal").smodal("hide");
			$("#jurnal_03a_info").html(
				"<div class='alert alert-block alert-inverse'>" +
					"<center><strong>PROSES DIBATALKAN</strong></center>" +
				"</div>"
			);
			$("#jurnal_03a_export_button").removeAttr("onclick");
		}
		return;
	}
	var self = this;
	var data = this.getRegulerData();
	data['command'] = "patient_info";
	data['num'] = num;
	$.post(
		"",
		data,
		function(response) {
			var json = JSON.parse(response);
			if (parseFloat(json.noreg_pasien)  != 0) {
				$("tbody#jurnal_03a_list").append(
					json.html
				);
				$("#jurnal_03a_loading_bar").sload("true", json.noreg_pasien + " - " + json.nrm_pasien + " - " + json.nama_pasien + " (" + (num + 1) + " / " + limit + ")", (num + 1) * 100 / limit - 1);
			}
			self.fillHtml(num + 1, limit);
		}
	);
};
Jurnal03AAction.prototype.finalize = function() {
	$("#jurnal_03a_loading_bar").sload("true", "Finalisasi...", 100);
	var num_rows = $("tbody#jurnal_03a_list").children("tr").length;
	var total_piutang = 0;
	var total_edc = 0;
	var total_tunai = 0;
	var total_total = 0;
	for(var i = 0; i < num_rows; i++) {
		$("tbody#jurnal_03a_list tr:eq(" + i + ") td:eq(0)").html("<small>" + (i + 1) + "</small>");
		var prefix = $("tbody#jurnal_03a_list tr:eq(" + i + ")").prop("id");
		var piutang = parseFloat($("#" + prefix + "_piutang").text().replace(/[^0-9-,]/g, '').replace(",", "."));
		total_piutang += piutang;
		var edc = parseFloat($("#" + prefix + "_edc").text().replace(/[^0-9-,]/g, '').replace(",", "."));
		total_edc += edc;
		var tunai = parseFloat($("#" + prefix + "_tunai").text().replace(/[^0-9-,]/g, '').replace(",", "."));
		total_tunai += tunai;
		var total = parseFloat($("#" + prefix + "_total").text().replace(/[^0-9-,]/g, '').replace(",", "."));
		total_total += total;
		console.log("i " + i + ", piutang : " + piutang + ", edc : " + edc + ", tunai : " + tunai);
	}
	$("#jurnal_03a_list").append(
		"<tr>" +
			"<td colspan='7'><center><small><strong>T O T A L</strong></small></center></td>" +
			"<td><small><strong><div class='right money-value'>" + parseFloat(total_piutang).formatMoney("2", ".", ",") + "</div></strong></small></td>" +
		"</tr>"
	);
	$("#jurnal_03a_modal").smodal("hide");
	$("#jurnal_03a_info").html(
		"<div class='alert alert-block alert-info'>" +
			"<center><strong>PROSES SELESAI</strong></center>" + 
		"</div>"
	);
	$("#jurnal_03a_export_button").removeAttr("onclick");
	$("#jurnal_03a_export_button").attr("onclick", "jurnal_03a.export_excel()");
};
Jurnal03AAction.prototype.export_excel = function() {
	var num_rows = $("#jurnal_03a_list").children("tr").length - 1;
	var d_data = {};
	for (var i = 0; i < num_rows; i++) {
		var prefix = $("tbody#jurnal_03a_list").children("tr").eq(i).prop("id");
		var noreg_pasien = $("#" + prefix + "_noreg").text();
		var nrm_pasien = $("#" + prefix + "_nrm").text();
		var nama_pasien = $("#" + prefix + "_nama").text();
		var jenis_pasien = $("#" + prefix + "_jenis").text();
		var perusahaan = $("#" + prefix + "_perusahaan").text();
		var asuransi = $("#" + prefix + "_asuransi").text();
		var piutang = $("#" + prefix + "_piutang").text().replace(/[^0-9-,]/g, '').replace(",", ".");
		var dd_data = {};
		dd_data['noreg_pasien'] = noreg_pasien;
		dd_data['nrm_pasien'] = nrm_pasien;
		dd_data['nama_pasien'] = nama_pasien;
		dd_data['jenis_pasien'] = jenis_pasien;
		dd_data['perusahaan'] = perusahaan;
		dd_data['asuransi'] = asuransi;
		dd_data['piutang'] = piutang;		
		d_data[i] = dd_data;
	}
	var data = this.getRegulerData();
	data['command'] = "export_excel";
	data['num_rows'] = num_rows;
	data['d_data'] = JSON.stringify(d_data);
	postForm(data);
};
Jurnal03AAction.prototype.cancel = function() {
	FINISHED = true;
};