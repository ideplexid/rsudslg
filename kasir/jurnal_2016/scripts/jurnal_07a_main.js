var jurnal_07a;
var FINISHED;
$(document).ready(function() {
	$(".mydatetime").datetimepicker({ 
		minuteStep: 1
	});
	$("#jurnal_07a_modal").on("show", function() {
		$("a.close").hide();
	});
	$("tbody#jurnal_07a_list").append(
		"<tr>" +
			"<td colspan='10'><center><small><strong>JURNAL BELUM DIPROSES</strong></small></center></td>" +
		"</tr>"
	);
	jurnal_07a = new Jurnal07aAction(
		"jurnal_07a",
		"kasir",
		"jurnal_2016/jurnal_07a",
		new Array()
	);
});
$(document).keyup(function(e) {
	if (e.which == 27)
		FINISHED = true;
});