var jurnal_07_2;
var FINISHED;
$(document).ready(function() {
	$(".mydatetime").datetimepicker({ 
		minuteStep: 1
	});
	$("#jurnal_07_2_modal").on("show", function() {
		$("a.close").hide();
	});
	$("tbody#jurnal_07_2_list").append(
		"<tr>" +
			"<td colspan='8'><center><small><strong>JURNAL BELUM DIPROSES</strong></small></center></td>" +
		"</tr>"
	);
	jurnal_07_2 = new Jurnal07_2Action(
		"jurnal_07_2",
		"kasir",
		"jurnal_2016/jurnal_07_2",
		new Array()
	);
});
$(document).keyup(function(e) {
	if (e.which == 27)
		FINISHED = true;
});