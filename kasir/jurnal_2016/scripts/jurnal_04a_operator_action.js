function Jurnal04AOperatorAction(name, page, action, column) {
	this.initialize(name, page, action, column);
}
Jurnal04AOperatorAction.prototype.constructor = Jurnal04AOperatorAction;
Jurnal04AOperatorAction.prototype = new TableAction();
Jurnal04AOperatorAction.prototype.selected = function(json) {
	$("#jurnal_04a_operator").val(json.operator);
};