function Jurnal03AOperatorAction(name, page, action, column) {
	this.initialize(name, page, action, column);
}
Jurnal03AOperatorAction.prototype.constructor = Jurnal03AOperatorAction;
Jurnal03AOperatorAction.prototype = new TableAction();
Jurnal03AOperatorAction.prototype.selected = function(json) {
	$("#jurnal_03a_operator").val(json.operator);
};