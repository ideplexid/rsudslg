var jurnal_06a;
var jurnal_06a_dokter;
var FINISHED;
$(document).ready(function() {
	$(".mydatetime").datetimepicker({ 
		minuteStep: 1
	});
	$("#jurnal_06a_modal").on("show", function() {
		$("a.close").hide();
	});
	$("tbody#jurnal_06a_list").append(
		"<tr>" +
			"<td colspan='12'><center><small><strong>JURNAL BELUM DIPROSES</strong></small></center></td>" +
		"</tr>"
	);
	jurnal_06a_dokter = new Jurnal06aDokterAction(
		"jurnal_06a_dokter",
		"kasir",
		"jurnal_2016/jurnal_06a",
		new Array()
	);
	jurnal_06a_dokter.setSuperCommand("jurnal_06a_dokter");
	jurnal_06a = new Jurnal06aAction(
		"jurnal_06a",
		"kasir",
		"jurnal_2016/jurnal_06a",
		new Array()
	);
});
$(document).keyup(function(e) {
	if (e.which == 27)
		FINISHED = true;
});