function Jurnal07aAction(name, page, action, column) {
	this.initialize(name, page, action, column);
}
Jurnal07aAction.prototype.constructor = Jurnal07aAction;
Jurnal07aAction.prototype = new TableAction();
Jurnal07aAction.prototype.getRegulerData = function() {
	var data = TableAction.prototype.getRegulerData.call(this);
	data['from'] = $("#jurnal_07a_from").val();
	data['to'] = $("#jurnal_07a_to").val();
	data['persentase_jaspel'] = $("#jurnal_07a_persentase_jaspel").val();
	return data;
};
Jurnal07aAction.prototype.view = function() {
	var self = this;
	$("#jurnal_07a_info").html("");
	$("#jurnal_07a_list").html("");
	$("#jurnal_07a_loading_bar").sload("true", "Harap ditunggu...", 0);
	$("#jurnal_07a_modal").smodal("show");
	FINISHED = false;
	var data = this.getRegulerData();
	data['command'] = "patient_number";
	$.post(
		"",
		data,
		function(response) {
			var json = JSON.parse(response);
			self.fillHtml(0, json.jumlah);
		}
	);
};
Jurnal07aAction.prototype.fillHtml = function(num, limit) {
	if (FINISHED || num == limit) {
		$("#jurnal_07a_loading_bar").sload("true", "Harap ditunggu...", 0);
		if (FINISHED == false && num == limit) {
			this.finalize();
		} else {
			$("#jurnal_07a_modal").smodal("hide");
			$("#jurnal_07a_info").html(
				"<div class='alert alert-block alert-inverse'>" +
					"<center><strong>PROSES DIBATALKAN</strong></center>" +
				"</div>"
			);
			$("#jurnal_07a_export_button").removeAttr("onclick");
		}
		return;
	}
	var self = this;
	var data = this.getRegulerData();
	data['command'] = "patient_info";
	data['num'] = num;
	$.post(
		"",
		data,
		function(response) {
			var json = JSON.parse(response);
			if (json == null) FINISHED = true;
			$("#jurnal_07a_list").append(json.html);
			$("#jurnal_07a_loading_bar").sload("true", json.noreg_pasien + " - " + json.nrm_pasien + " - " + json.nama_pasien + " - " + json.jenis_pasien + " (" + (num + 1) + " / " + limit + ")", (num + 1) * 100 / limit - 1);
			self.fillHtml(num + 1, limit);
		}
	);
};
Jurnal07aAction.prototype.finalize = function() {
	$("#jurnal_07a_loading_bar").sload("true", "Finalisasi...", 100);
	var num_rows = $("#jurnal_07a_list").children("tr").length;
	var nomor = 1;
	for (var i = 0; i < num_rows; i++, nomor++) {
		if ($("#jurnal_07a_list").children("tr").eq(i).children("td").eq(0).text() == "") {
			$("#jurnal_07a_list").children("tr").eq(i).children("td").eq(0).html(
				"<small>" + nomor + "</small>"
			);
		} else
			nomor = 0;
	}
	$("#jurnal_07a_modal").smodal("hide");
	$("#jurnal_07a_info").html(
		"<div class='alert alert-block alert-info'>" +
			"<center><strong>PROSES SELESAI</strong></center>" + 
		"</div>"
	);
	$("#jurnal_07a_export_button").removeAttr("onclick");
	$("#jurnal_07a_export_button").attr("onclick", "jurnal_07a.export_excel()");
};
Jurnal07aAction.prototype.export_excel = function() {
	var data = this.getRegulerData();
	data['command'] = "export_excel";
	var num_rows = $("tbody#jurnal_07a_list").children("tr").length;
	var d_data = {};
	for (var i = 0; i < num_rows; i++) {
		var dd_data = {};
		var num_fields = $("tbody#jurnal_07a_list").children("tr").eq(i).children("td").length;
		if (num_fields == 10) {
			dd_data['no'] = $("tbody#jurnal_07a_list").children("tr").eq(i).children("td").eq(0).text();
			dd_data['kode_akun'] = $("tbody#jurnal_07a_list").children("tr").eq(i).children("td").eq(1).text();
			dd_data['ruangan'] = $("tbody#jurnal_07a_list").children("tr").eq(i).children("td").eq(2).text();
			dd_data['noreg_pasien'] = $("tbody#jurnal_07a_list").children("tr").eq(i).children("td").eq(3).text();
			dd_data['nrm_pasien'] = $("tbody#jurnal_07a_list").children("tr").eq(i).children("td").eq(4).text();
			dd_data['nama_pasien'] = $("tbody#jurnal_07a_list").children("tr").eq(i).children("td").eq(5).text();
			dd_data['jenis_pasien'] = $("tbody#jurnal_07a_list").children("tr").eq(i).children("td").eq(6).text();
			dd_data['layanan'] = $("tbody#jurnal_07a_list").children("tr").eq(i).children("td").eq(7).text();
			dd_data['rincian'] = $("tbody#jurnal_07a_list").children("tr").eq(i).children("td").eq(8).text().replace(/[^0-9-,]/g, '').replace(",", ".");
			dd_data['total'] = $("tbody#jurnal_07a_list").children("tr").eq(i).children("td").eq(9).text().replace(/[^0-9-,]/g, '').replace(",", ".");
			dd_data['merge'] = false;
		} else {
			dd_data['no'] = $("tbody#jurnal_07a_list").children("tr").eq(i).children("td").eq(0).text();
			dd_data['kode_akun'] = "";
			dd_data['ruangan'] = "";
			dd_data['noreg_pasien'] = "";
			dd_data['nrm_pasien'] = "";
			dd_data['nama_pasien'] = "";
			dd_data['jenis_pasien'] = "";
			dd_data['layanan'] = "";
			dd_data['rincian'] = "";
			dd_data['total'] = $("tbody#jurnal_07a_list").children("tr").eq(i).children("td").eq(1).text().replace(/[^0-9-,]/g, '').replace(",", ".");
			dd_data['merge'] = true;
		}
		d_data[i] = dd_data;
	}
	data['d_data'] = JSON.stringify(d_data);
	data['num_rows'] = num_rows;
	postForm(data);
};
Jurnal07aAction.prototype.cancel = function() {
	FINISHED = true;
};