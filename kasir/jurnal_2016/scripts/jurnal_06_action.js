function Jurnal06Action(name, page, action, column) {
	this.initialize(name, page, action, column);
}
Jurnal06Action.prototype.constructor = Jurnal06Action;
Jurnal06Action.prototype = new TableAction();
Jurnal06Action.prototype.getRegulerData = function() {
	var data = TableAction.prototype.getRegulerData.call(this);
	data['from'] = $("#jurnal_06_from").val();
	data['to'] = $("#jurnal_06_to").val();
	return data;
};
Jurnal06Action.prototype.view = function() {
	var self = this;
	$("#jurnal_06_info").html("");
	$("#jurnal_06_loading_bar").sload("true", "Harap ditunggu...", 0);
	$("#jurnal_06_modal").smodal("show");
	FINISHED = false;
	var data = this.getRegulerData();
	data['command'] = "init";
	$.post(
		"",
		data,
		function(response) {
			var json = JSON.parse(response);
			$("#jurnal_06_list").html(json.html);
			var pi_data = self.getRegulerData();
			pi_data['command'] = "patient_number";
			$.post(
				"",
				pi_data,
				function(pi_response) {
					var pi_json = JSON.parse(pi_response);
					self.fillHtml(0, pi_json.jumlah);
				}
			);
		}
	);
}
Jurnal06Action.prototype.fillHtml = function(num, limit) {
	if (FINISHED || num == limit) {
		$("#jurnal_06_loading_bar").sload("true", "Harap ditunggu...", 0);
		if (FINISHED == false && num == limit) {
			this.finalize();
		} else {
			$("#jurnal_06_modal").smodal("hide");
			$("#jurnal_06_info").html(
				"<div class='alert alert-block alert-inverse'>" +
					"<center><strong>PROSES DIBATALKAN</strong></center>" +
				"</div>"
			);
			$("#jurnal_06_export_button").removeAttr("onclick");
		}
		return;
	}
	var self = this;
	var data = this.getRegulerData();
	data['command'] = "patient_info";
	data['num'] = num;
	var unit_data = {};
	var num_non_dokter_rows = $("tr.data_unit").length;
	for (var i = 0; i < num_non_dokter_rows; i++) {
		var slug = $("tr.data_unit").eq(i).children("td").eq(5).text();
		var filter = $("tr.data_unit").eq(i).children("td").eq(6).text();
		var element_id = $("tr.data_unit").eq(i).prop("id");
		var d_unit_data = {
			"slug"			: slug,
			"filter"		: filter,
			"element_id"	: element_id
		};
		unit_data[i] = d_unit_data;
	}
	data['unit_data'] = JSON.stringify(unit_data);
	var dokter_data = {};
	var num_dokter_rows = $("tr.data_dokter").length;
	for (var i = 0; i < num_dokter_rows; i++) {
		var id_dokter = $("tr.data_dokter").eq(i).children("td").eq(5).text();
		var element_id = $("tr.data_dokter").eq(i).prop("id");
		var d_dokter_data = {
			"id_dokter"		: id_dokter,
			"element_id"	: element_id
		};
		dokter_data[i] = d_dokter_data;
	}
	data['dokter_data'] = JSON.stringify(dokter_data);
	var sewa_alat_dokter_data = {};
	sewa_alat_dokter_data[0] = {
		"filter"		: $("#data_total_sewa_alat_filter").text(),
		"element_id"	: "data_total_sewa_alat",
		"debet_kredit"	: "debet"
	};
	var num_data_sewa_alat_rows = $("tr.data_sewa_alat").length;
	for (var i = 0; i < num_data_sewa_alat_rows; i++) {
		var filter = $("tr.data_sewa_alat").eq(i).children("td").eq(5).text();
		var element_id = $("tr.data_sewa_alat").eq(i).prop("id");
		var d_sewa_alat_data = {
			"filter"		: filter,
			"element_id"	: element_id,
			"debet_kredit"	: "kredit"
		};
		sewa_alat_dokter_data[i + 1] = d_sewa_alat_data;
	}
	data['sewa_alat_dokter_data'] = JSON.stringify(sewa_alat_dokter_data);
	$.post(
		"",
		data,
		function(response) {
			var json = JSON.parse(response);
			if (json == null) FINISHED = true;
			
			for (var i = 0; i < json.update_data.length; i++) {
				var prefix = json.update_data[i].element_id;
				var debet = parseFloat($("#" + prefix + "_debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
				var kredit = parseFloat($("#" + prefix + "_kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
				debet += parseFloat(json.update_data[i].debet);
				debet = parseFloat(debet).formatMoney("2", ".", ",");
				kredit += parseFloat(json.update_data[i].kredit);
				kredit = parseFloat(kredit).formatMoney("2", ".", ",");
				$("td#" + prefix + "_debet div.money-value").html(debet);
				$("td#" + prefix + "_kredit div.money-value").html(kredit);
			}
			
			$("#jurnal_06_loading_bar").sload("true", json.noreg_pasien + " - " + json.nrm_pasien + " - " + json.nama_pasien + " - " + json.jenis_pasien + " (" + (num + 1) + " / " + limit + ")", (num + 1) * 100 / limit - 1);
			self.fillHtml(num + 1, limit);
		}
	);
};
Jurnal06Action.prototype.finalize = function() {
	$("#jurnal_06_loading_bar").sload("true", "Finalisasi...", 100);
	$("#jurnal_06_modal").smodal("hide");
	$("#jurnal_06_info").html(
		"<div class='alert alert-block alert-info'>" +
			"<center><strong>PROSES SELESAI</strong></center>" + 
		"</div>"
	);
	$("#jurnal_06_export_button").removeAttr("onclick");
	$("#jurnal_06_export_button").attr("onclick", "jurnal_06.export_excel()");
};
Jurnal06Action.prototype.export_excel = function() {
	var data = this.getRegulerData();
	data['command'] = "export_excel";
	var num_rows = $("tbody#jurnal_06_list").children("tr").length;
	var d_data = {};
	for (var i = 0; i < num_rows; i++) {
		var dd_data = {};
		dd_data['kode_akun'] = $("tbody#jurnal_06_list").children("tr").eq(i).children("td").eq(1).text();
		dd_data['nama_akun'] = $("tbody#jurnal_06_list").children("tr").eq(i).children("td").eq(2).text();
		dd_data['debet'] = $("tbody#jurnal_06_list").children("tr").eq(i).children("td").eq(3).text().replace(/[^0-9-,]/g, '').replace(",", ".");
		dd_data['kredit'] = $("tbody#jurnal_06_list").children("tr").eq(i).children("td").eq(4).text().replace(/[^0-9-,]/g, '').replace(",", ".");
		d_data[i] = dd_data;
	}
	data['d_data'] = JSON.stringify(d_data);
	data['num_rows'] = num_rows;
	postForm(data);
};
Jurnal06Action.prototype.cancel = function() {
	FINISHED = true;
};