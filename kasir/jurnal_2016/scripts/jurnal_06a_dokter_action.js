function Jurnal06aDokterAction(name, page, action, column) {
	this.initialize(name, page, action, column);
}
Jurnal06aDokterAction.prototype.constructor = Jurnal06aDokterAction;
Jurnal06aDokterAction.prototype = new TableAction();
Jurnal06aDokterAction.prototype.selected = function(json) {
	$("#jurnal_06a_id_dokter").val(json.id_dokter);
	$("#jurnal_06a_kode_dokter").val(json.kode_dokter);
	$("#jurnal_06a_nama_dokter").val(json.nama_dokter);
	$("#jurnal_06a_hak_dokter").val(json.hak_dokter);
};