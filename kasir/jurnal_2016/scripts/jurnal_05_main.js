var jurnal_05;
var FINISHED;
$(document).ready(function() {
	$(".mydatetime").datetimepicker({ 
		minuteStep: 1
	});
	$("#jurnal_05_modal").on("show", function() {
		$("a.close").hide();
	});
	$("tbody#jurnal_05_list").append(
		"<tr>" +
			"<td colspan='8'><center><small><strong>JURNAL BELUM DIPROSES</strong></small></center></td>" +
		"</tr>"
	);
	jurnal_05 = new Jurnal05Action(
		"jurnal_05",
		"kasir",
		"jurnal_2016/jurnal_05",
		new Array()
	);
});
$(document).keyup(function(e) {
	if (e.which == 27)
		FINISHED = true;
});