function Jurnal07Action(name, page, action, column) {
	this.initialize(name, page, action, column);
}
Jurnal07Action.prototype.constructor = Jurnal07Action;
Jurnal07Action.prototype = new TableAction();
Jurnal07Action.prototype.getRegulerData = function() {
	var data = TableAction.prototype.getRegulerData.call(this);
	data['from'] = $("#jurnal_07_from").val();
	data['to'] = $("#jurnal_07_to").val();
	data['persentase_jaspel'] = $("#jurnal_07_persentase_jaspel").val();
	return data;
};
Jurnal07Action.prototype.view = function() {
	var self = this;
	$("#jurnal_07_info").html("");
	$("#jurnal_07_list").html("");
	$("#jurnal_07_loading_bar").sload("true", "Harap ditunggu...", 0);
	$("#jurnal_07_modal").smodal("show");
	FINISHED = false;
	var data = this.getRegulerData();
	data['command'] = "patient_number";
	$.post(
		"",
		data,
		function(response) {
			var json = JSON.parse(response);
			self.fillHtml(0, json.jumlah);
		}
	);
};
Jurnal07Action.prototype.fillHtml = function(num, limit) {
	if (FINISHED || num == limit) {
		$("#jurnal_07_loading_bar").sload("true", "Harap ditunggu...", 0);
		if (FINISHED == false && num == limit) {
			this.finalize();
		} else {
			$("#jurnal_07_modal").smodal("hide");
			$("#jurnal_07_info").html(
				"<div class='alert alert-block alert-inverse'>" +
					"<center><strong>PROSES DIBATALKAN</strong></center>" +
				"</div>"
			);
			$("#jurnal_07_export_button").removeAttr("onclick");
		}
		return;
	}
	var self = this;
	var data = this.getRegulerData();
	data['command'] = "patient_info";
	data['num'] = num;
	data['limit'] = limit;
	$.post(
		"",
		data,
		function(response) {
			var json = JSON.parse(response);
			if (json == null) FINISHED = true;
			$("#jurnal_07_list").append(json.html);
			$("#jurnal_07_loading_bar").sload("true", json.noreg_pasien + " - " + json.nrm_pasien + " - " + json.nama_pasien + " - " + json.jenis_pasien + " (" + (num + 1) + " / " + limit + ")", (num + 1) * 100 / limit - 1);
			self.fillHtml(num + 1, limit);
		}
	);
};
Jurnal07Action.prototype.finalize = function() {
	$("#jurnal_07_loading_bar").sload("true", "Finalisasi...", 100);
	var num_rows = $("#jurnal_07_list").children("tr").length;
	var nomor = 1;
	for (var i = 0; i < num_rows; i++, nomor++) {
		$("#jurnal_07_list").children("tr").eq(i).children("td").eq(0).html(
			"<small>" + nomor + "</small>"
		);
	}
	$("#jurnal_07_modal").smodal("hide");
	$("#jurnal_07_info").html(
		"<div class='alert alert-block alert-info'>" +
			"<center><strong>PROSES SELESAI</strong></center>" + 
		"</div>"
	);
	$("#jurnal_07_export_button").removeAttr("onclick");
	$("#jurnal_07_export_button").attr("onclick", "jurnal_07.export_excel()");
};
Jurnal07Action.prototype.export_excel = function() {
	var data = this.getRegulerData();
	data['command'] = "export_excel";
	var num_rows = $("tbody#jurnal_07_list").children("tr").length;
	var d_data = {};
	for (var i = 0; i < num_rows; i++) {
		var dd_data = {};
		dd_data['no'] = $("tbody#jurnal_07_list").children("tr").eq(i).children("td").eq(0).text();
		dd_data['kode_akun'] = $("tbody#jurnal_07_list").children("tr").eq(i).children("td").eq(1).text();
		dd_data['ruangan'] = $("tbody#jurnal_07_list").children("tr").eq(i).children("td").eq(2).text();
		dd_data['noreg_pasien'] = $("tbody#jurnal_07_list").children("tr").eq(i).children("td").eq(3).text();
		dd_data['nrm_pasien'] = $("tbody#jurnal_07_list").children("tr").eq(i).children("td").eq(4).text();
		dd_data['nama_pasien'] = $("tbody#jurnal_07_list").children("tr").eq(i).children("td").eq(5).text();
		dd_data['jenis_pasien'] = $("tbody#jurnal_07_list").children("tr").eq(i).children("td").eq(6).text();
		dd_data['rincian'] = $("tbody#jurnal_07_list").children("tr").eq(i).children("td").eq(7).text().replace(/[^0-9-,]/g, '').replace(",", ".");
		dd_data['total'] = $("tbody#jurnal_07_list").children("tr").eq(i).children("td").eq(8).text().replace(/[^0-9-,]/g, '').replace(",", ".");
		d_data[i] = dd_data;
	}
	data['d_data'] = JSON.stringify(d_data);
	data['num_rows'] = num_rows;
	postForm(data);
};
Jurnal07Action.prototype.cancel = function() {
	FINISHED = true;
};