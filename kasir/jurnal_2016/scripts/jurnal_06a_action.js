function Jurnal06aAction(name, page, action, column) {
	this.initialize(name, page, action, column);
}
Jurnal06aAction.prototype.constructor = Jurnal06aAction;
Jurnal06aAction.prototype = new TableAction();
Jurnal06aAction.prototype.getRegulerData = function() {
	var data = TableAction.prototype.getRegulerData.call(this);
	data['from'] = $("#jurnal_06a_from").val();
	data['to'] = $("#jurnal_06a_to").val();
	data['id_dokter'] = $("#jurnal_06a_id_dokter").val();
	data['kode_dokter'] = $("#jurnal_06a_kode_dokter").val();
	data['nama_dokter'] = $("#jurnal_06a_nama_dokter").val();
	data['hak_dokter'] = $("#jurnal_06a_hak_dokter").val();
	data['no_jurnal'] = $("#jurnal_06a_no_jurnal").val();
	return data;
};
Jurnal06aAction.prototype.view = function() {
	if ($("#jurnal_06a_no_jurnal").val() == "" || $("#jurnal_06a_id_dokter").val() == "")
		return;
	var self = this;
	$("#jurnal_06a_info").html("");
	$("#jurnal_06a_list").html("");
	$("#jurnal_06a_loading_bar").sload("true", "Harap ditunggu...", 0);
	$("#jurnal_06a_modal").smodal("show");
	FINISHED = false;
	
	var pi_data = self.getRegulerData();
	pi_data['command'] = "patient_number";
	$.post(
		"",
		pi_data,
		function(pi_response) {
			var pi_json = JSON.parse(pi_response);
			self.fillHtml(0, pi_json.jumlah);
		}
	);
}
Jurnal06aAction.prototype.fillHtml = function(num, limit) {
	if (FINISHED || num == limit) {
		$("#jurnal_06a_loading_bar").sload("true", "Harap ditunggu...", 0);
		if (FINISHED == false && num == limit) {
			this.finalize();
		} else {
			$("#jurnal_06a_modal").smodal("hide");
			$("#jurnal_06a_info").html(
				"<div class='alert alert-block alert-inverse'>" +
					"<center><strong>PROSES DIBATALKAN</strong></center>" +
				"</div>"
			);
			$("#jurnal_06a_export_button").removeAttr("onclick");
		}
		return;
	}
	var self = this;
	var data = this.getRegulerData();
	data['command'] = "patient_info";
	data['num'] = num;
	$.post(
		"",
		data,
		function(response) {
			var json = JSON.parse(response);
			if (json == null) FINISHED = true;
			
			$("#jurnal_06a_list").append(json.html);
									
			$("#jurnal_06a_loading_bar").sload("true", json.noreg_pasien + " - " + json.nrm_pasien + " - " + json.nama_pasien + " - " + json.jenis_pasien + " (" + (num + 1) + " / " + limit + ")", (num + 1) * 100 / limit - 1);
			self.fillHtml(num + 1, limit);
		}
	);
};
Jurnal06aAction.prototype.finalize = function() {
	$("#jurnal_06a_loading_bar").sload("true", "Finalisasi...", 100);
	var num_rows = $("#jurnal_06a_list").children("tr").length;
	var nomor = 1;
	for (var i = 0; i < num_rows; i++, nomor++) {
		$("#jurnal_06a_list").children("tr").eq(i).children("td").eq(0).html(
			"<small>" + nomor + "</small>"
		);
	}
	$("#jurnal_06a_modal").smodal("hide");
	$("#jurnal_06a_info").html(
		"<div class='alert alert-block alert-info'>" +
			"<center><strong>PROSES SELESAI</strong></center>" + 
		"</div>"
	);
	$("#jurnal_06a_export_button").removeAttr("onclick");
	$("#jurnal_06a_export_button").attr("onclick", "jurnal_06a.export_excel()");
};
Jurnal06aAction.prototype.export_excel = function() {
	var data = this.getRegulerData();
	data['command'] = "export_excel";
	var num_rows = $("tbody#jurnal_06a_list").children("tr").length;
	var d_data = {};
	for (var i = 0; i < num_rows; i++) {
		var dd_data = {};
		dd_data['no'] = $("tbody#jurnal_06a_list").children("tr").eq(i).children("td").eq(0).text();
		dd_data['noreg_pasien'] = $("tbody#jurnal_06a_list").children("tr").eq(i).children("td").eq(1).text();
		dd_data['nama_pasien'] = $("tbody#jurnal_06a_list").children("tr").eq(i).children("td").eq(2).text();
		dd_data['nrm_pasien'] = $("tbody#jurnal_06a_list").children("tr").eq(i).children("td").eq(3).text();
		dd_data['tanggal_layanan'] = $("tbody#jurnal_06a_list").children("tr").eq(i).children("td").eq(4).text();
		dd_data['bruto'] = $("tbody#jurnal_06a_list").children("tr").eq(i).children("td").eq(5).text().replace(/[^0-9-,]/g, '').replace(",", ".");
		dd_data['jasa_rs'] = $("tbody#jurnal_06a_list").children("tr").eq(i).children("td").eq(6).text().replace(/[^0-9-,]/g, '').replace(",", ".");
		dd_data['jasa_dokter_bruto'] = $("tbody#jurnal_06a_list").children("tr").eq(i).children("td").eq(7).text().replace(/[^0-9-,]/g, '').replace(",", ".");
		dd_data['dpp'] = $("tbody#jurnal_06a_list").children("tr").eq(i).children("td").eq(8).text().replace(/[^0-9-,]/g, '').replace(",", ".");
		dd_data['pph_21'] = $("tbody#jurnal_06a_list").children("tr").eq(i).children("td").eq(9).text().replace(/[^0-9-,]/g, '').replace(",", ".");
		dd_data['jasa_dokter_netto'] = $("tbody#jurnal_06a_list").children("tr").eq(i).children("td").eq(10).text().replace(/[^0-9-,]/g, '').replace(",", ".");
		dd_data['no_jurnal'] = $("tbody#jurnal_06a_list").children("tr").eq(i).children("td").eq(11).text();
		d_data[i] = dd_data;
	}
	data['d_data'] = JSON.stringify(d_data);
	data['num_rows'] = num_rows;
	postForm(data);
};
Jurnal06aAction.prototype.cancel = function() {
	FINISHED = true;
};