function Jurnal05Action(name, page, action, column) {
	this.initialize(name, page, action, column);
}
Jurnal05Action.prototype.constructor = Jurnal05Action;
Jurnal05Action.prototype = new TableAction();
Jurnal05Action.prototype.getRegulerData = function() {
	var data = TableAction.prototype.getRegulerData.call(this);
	data['from'] = $("#jurnal_05_from").val();
	data['to'] = $("#jurnal_05_to").val();
	data['jenis_pasien'] = $("#jurnal_05_jenis_pasien").val();
	data['uri'] = $("#jurnal_05_uri").val();
	return data;
};
Jurnal05Action.prototype.view = function() {
	var self = this;
	$("#jurnal_05_info").html("");
	$("#jurnal_05_loading_bar").sload("true", "Harap ditunggu...", 0);
	$("#jurnal_05_modal").smodal("show");
	FINISHED = false;
	var data = this.getRegulerData();
	data['command'] = "init";
	$.post(
		"",
		data,
		function(response) {
			var json = JSON.parse(response);
			$("#jurnal_05_list").html(json.html);
			var pi_data = self.getRegulerData();
			pi_data['command'] = "patient_number";
			$.post(
				"",
				pi_data,
				function(pi_response) {
					var pi_json = JSON.parse(pi_response);
					self.fillHtml(0, pi_json.jumlah);
				}
			);
		}
	);
};
Jurnal05Action.prototype.fillHtml = function(num, limit) {
	if (FINISHED || num == limit) {
		$("#jurnal_05_loading_bar").sload("true", "Harap ditunggu...", 0);
		if (FINISHED == false && num == limit) {
			this.finalize();
		} else {
			$("#jurnal_05_modal").smodal("hide");
			$("#jurnal_05_info").html(
				"<div class='alert alert-block alert-inverse'>" +
					"<center><strong>PROSES DIBATALKAN</strong></center>" +
				"</div>"
			);
			$("#jurnal_05_export_button").removeAttr("onclick");
		}
		return;
	}
	var self = this;
	var data = this.getRegulerData();
	data['command'] = "patient_info";
	data['num'] = num;
	$.post(
		"",
		data,
		function(response) {
			var json = JSON.parse(response);
			if (json == null) { 
				FINISHED = true;
				return;
			}
			// Update Subtotal RPA:
			// var rpa_0_debet = parseFloat($("#rpa-0-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			// var rpa_0_kredit = parseFloat($("#rpa-0-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			// rpa_0_debet += json.rpa_0_debet;
			// rpa_0_debet = parseFloat(rpa_0_debet).formatMoney("2", ".", ",");
			// rpa_0_kredit += json.rpa_0_kredit;
			// rpa_0_kredit = parseFloat(rpa_0_kredit).formatMoney("2", ".", ",");
			// $("#rpa-0-debet div").html(rpa_0_debet);
			// $("#rpa-0-kredit div").html(rpa_0_kredit);
			
			// Update Display Sewa Kamar RPA:
			var rpa_1_debet = parseFloat($("#rpa-1-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var rpa_1_kredit = parseFloat($("#rpa-1-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			rpa_1_debet += json.rpa_1_debet;
			rpa_1_debet = parseFloat(rpa_1_debet).formatMoney("2", ".", ",");
			rpa_1_kredit += json.rpa_1_kredit;
			rpa_1_kredit = parseFloat(rpa_1_kredit).formatMoney("2", ".", ",");
			$("#rpa-1-debet div").html(rpa_1_debet);
			$("#rpa-1-kredit div").html(rpa_1_kredit);
			
			// Update Display Tindakan / Jasa Dokter RPA:
			var rpa_2_debet = parseFloat($("#rpa-2-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var rpa_2_kredit = parseFloat($("#rpa-2-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			rpa_2_debet += json.rpa_2_debet;
			rpa_2_debet = parseFloat(rpa_2_debet).formatMoney("2", ".", ",");
			rpa_2_kredit += json.rpa_2_kredit;
			rpa_2_kredit = parseFloat(rpa_2_kredit).formatMoney("2", ".", ",");
			$("#rpa-2-debet div").html(rpa_2_debet);
			$("#rpa-2-kredit div").html(rpa_2_kredit);
			
			// Update Display Jasa / Tindakan Keperawatan RPA:
			var rpa_3_debet = parseFloat($("#rpa-3-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var rpa_3_kredit = parseFloat($("#rpa-3-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			rpa_3_debet += json.rpa_3_debet;
			rpa_3_debet = parseFloat(rpa_3_debet).formatMoney("2", ".", ",");
			rpa_3_kredit += json.rpa_3_kredit;
			rpa_3_kredit = parseFloat(rpa_3_kredit).formatMoney("2", ".", ",");
			$("#rpa-3-debet div").html(rpa_3_debet);
			$("#rpa-3-kredit div").html(rpa_3_kredit);
			
			// Update Display Alat Kesehatan RPA:
			var rpa_4_debet = parseFloat($("#rpa-4-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var rpa_4_kredit = parseFloat($("#rpa-4-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			rpa_4_debet += json.rpa_4_debet;
			rpa_4_debet = parseFloat(rpa_4_debet).formatMoney("2", ".", ",");
			rpa_4_kredit += json.rpa_4_kredit;
			rpa_4_kredit = parseFloat(rpa_4_kredit).formatMoney("2", ".", ",");
			$("#rpa-4-debet div").html(rpa_4_debet);
			$("#rpa-4-kredit div").html(rpa_4_kredit);
			
			// Update Display Lain-Lain RPA:
			var rpa_5_debet = parseFloat($("#rpa-5-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var rpa_5_kredit = parseFloat($("#rpa-5-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			rpa_5_debet += json.rpa_5_debet;
			rpa_5_debet = parseFloat(rpa_5_debet).formatMoney("2", ".", ",");
			rpa_5_kredit += json.rpa_5_kredit;
			rpa_5_kredit = parseFloat(rpa_5_kredit).formatMoney("2", ".", ",");
			$("#rpa-5-debet div").html(rpa_5_debet);
			$("#rpa-5-kredit div").html(rpa_5_kredit);
			
			// Update Subtotal RPB:
			// var rpb_0_debet = parseFloat($("#rpb-0-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			// var rpb_0_kredit = parseFloat($("#rpb-0-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			// rpb_0_debet += json.rpb_0_debet;
			// rpb_0_debet = parseFloat(rpb_0_debet).formatMoney("2", ".", ",");
			// rpb_0_kredit += json.rpb_0_kredit;
			// rpb_0_kredit = parseFloat(rpb_0_kredit).formatMoney("2", ".", ",");
			// $("#rpb-0-debet div").html(rpb_0_debet);
			// $("#rpb-0-kredit div").html(rpb_0_kredit);
			
			// Update Display Sewa Kamar RPB:
			var rpb_1_debet = parseFloat($("#rpb-1-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var rpb_1_kredit = parseFloat($("#rpb-1-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			rpb_1_debet += json.rpb_1_debet;
			rpb_1_debet = parseFloat(rpb_1_debet).formatMoney("2", ".", ",");
			rpb_1_kredit += json.rpb_1_kredit;
			rpb_1_kredit = parseFloat(rpb_1_kredit).formatMoney("2", ".", ",");
			$("#rpb-1-debet div").html(rpb_1_debet);
			$("#rpb-1-kredit div").html(rpb_1_kredit);
			
			// Update Display Tindakan / Jasa Dokter RPB:
			var rpb_2_debet = parseFloat($("#rpb-2-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var rpb_2_kredit = parseFloat($("#rpb-2-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			rpb_2_debet += json.rpb_2_debet;
			rpb_2_debet = parseFloat(rpb_2_debet).formatMoney("2", ".", ",");
			rpb_2_kredit += json.rpb_2_kredit;
			rpb_2_kredit = parseFloat(rpb_2_kredit).formatMoney("2", ".", ",");
			$("#rpb-2-debet div").html(rpb_2_debet);
			$("#rpb-2-kredit div").html(rpb_2_kredit);
			
			// Update Display Jasa / Tindakan Keperawatan RPB:
			var rpb_3_debet = parseFloat($("#rpb-3-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var rpb_3_kredit = parseFloat($("#rpb-3-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			rpb_3_debet += json.rpb_3_debet;
			rpb_3_debet = parseFloat(rpb_3_debet).formatMoney("2", ".", ",");
			rpb_3_kredit += json.rpb_3_kredit;
			rpb_3_kredit = parseFloat(rpb_3_kredit).formatMoney("2", ".", ",");
			$("#rpb-3-debet div").html(rpb_3_debet);
			$("#rpb-3-kredit div").html(rpb_3_kredit);
			
			// Update Display Alat Kesehatan RPB:
			var rpb_4_debet = parseFloat($("#rpb-4-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var rpb_4_kredit = parseFloat($("#rpb-4-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			rpb_4_debet += json.rpb_4_debet;
			rpb_4_debet = parseFloat(rpb_4_debet).formatMoney("2", ".", ",");
			rpb_4_kredit += json.rpb_4_kredit;
			rpb_4_kredit = parseFloat(rpb_4_kredit).formatMoney("2", ".", ",");
			$("#rpb-4-debet div").html(rpb_4_debet);
			$("#rpb-4-kredit div").html(rpb_4_kredit);
			
			// Update Display Lain-Lain RPB:
			var rpb_5_debet = parseFloat($("#rpb-5-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var rpb_5_kredit = parseFloat($("#rpb-5-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			rpb_5_debet += json.rpb_5_debet;
			rpb_5_debet = parseFloat(rpb_5_debet).formatMoney("2", ".", ",");
			rpb_5_kredit += json.rpb_5_kredit;
			rpb_5_kredit = parseFloat(rpb_5_kredit).formatMoney("2", ".", ",");
			$("#rpb-5-debet div").html(rpb_5_debet);
			$("#rpb-5-kredit div").html(rpb_5_kredit);
			
			// Update Subtotal RPC:
			// var rpc_0_debet = parseFloat($("#rpc-0-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			// var rpc_0_kredit = parseFloat($("#rpc-0-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			// rpc_0_debet += json.rpc_0_debet;
			// rpc_0_debet = parseFloat(rpc_0_debet).formatMoney("2", ".", ",");
			// rpc_0_kredit += json.rpc_0_kredit;
			// rpc_0_kredit = parseFloat(rpc_0_kredit).formatMoney("2", ".", ",");
			// $("#rpc-0-debet div").html(rpc_0_debet);
			// $("#rpc-0-kredit div").html(rpc_0_kredit);
			
			// Update Display Sewa Kamar RPC:
			var rpc_1_debet = parseFloat($("#rpc-1-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var rpc_1_kredit = parseFloat($("#rpc-1-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			rpc_1_debet += json.rpc_1_debet;
			rpc_1_debet = parseFloat(rpc_1_debet).formatMoney("2", ".", ",");
			rpc_1_kredit += json.rpc_1_kredit;
			rpc_1_kredit = parseFloat(rpc_1_kredit).formatMoney("2", ".", ",");
			$("#rpc-1-debet div").html(rpc_1_debet);
			$("#rpc-1-kredit div").html(rpc_1_kredit);
			
			// Update Display Tindakan / Jasa Dokter RPC:
			var rpc_2_debet = parseFloat($("#rpc-2-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var rpc_2_kredit = parseFloat($("#rpc-2-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			rpc_2_debet += json.rpc_2_debet;
			rpc_2_debet = parseFloat(rpc_2_debet).formatMoney("2", ".", ",");
			rpc_2_kredit += json.rpc_2_kredit;
			rpc_2_kredit = parseFloat(rpc_2_kredit).formatMoney("2", ".", ",");
			$("#rpc-2-debet div").html(rpc_2_debet);
			$("#rpc-2-kredit div").html(rpc_2_kredit);
			
			// Update Display Jasa / Tindakan Keperawatan RPC:
			var rpc_3_debet = parseFloat($("#rpc-3-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var rpc_3_kredit = parseFloat($("#rpc-3-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			rpc_3_debet += json.rpc_3_debet;
			rpc_3_debet = parseFloat(rpc_3_debet).formatMoney("2", ".", ",");
			rpc_3_kredit += json.rpc_3_kredit;
			rpc_3_kredit = parseFloat(rpc_3_kredit).formatMoney("2", ".", ",");
			$("#rpc-3-debet div").html(rpc_3_debet);
			$("#rpc-3-kredit div").html(rpc_3_kredit);
			
			// Update Display Alat Kesehatan RPC:
			var rpc_4_debet = parseFloat($("#rpc-4-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var rpc_4_kredit = parseFloat($("#rpc-4-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			rpc_4_debet += json.rpc_4_debet;
			rpc_4_debet = parseFloat(rpc_4_debet).formatMoney("2", ".", ",");
			rpc_4_kredit += json.rpc_4_kredit;
			rpc_4_kredit = parseFloat(rpc_4_kredit).formatMoney("2", ".", ",");
			$("#rpc-4-debet div").html(rpc_4_debet);
			$("#rpc-4-kredit div").html(rpc_4_kredit);
			
			// Update Display Lain-Lain RPC:
			var rpc_5_debet = parseFloat($("#rpc-5-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var rpc_5_kredit = parseFloat($("#rpc-5-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			rpc_5_debet += json.rpc_5_debet;
			rpc_5_debet = parseFloat(rpc_5_debet).formatMoney("2", ".", ",");
			rpc_5_kredit += json.rpc_5_kredit;
			rpc_5_kredit = parseFloat(rpc_5_kredit).formatMoney("2", ".", ",");
			$("#rpc-5-debet div").html(rpc_5_debet);
			$("#rpc-5-kredit div").html(rpc_5_kredit);
			
			// Update Subtotal RPD:
			// var rpd_0_debet = parseFloat($("#rpd-0-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			// var rpd_0_kredit = parseFloat($("#rpd-0-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			// rpd_0_debet += json.rpd_0_debet;
			// rpd_0_debet = parseFloat(rpd_0_debet).formatMoney("2", ".", ",");
			// rpd_0_kredit += json.rpd_0_kredit;
			// rpd_0_kredit = parseFloat(rpd_0_kredit).formatMoney("2", ".", ",");
			// $("#rpd-0-debet div").html(rpd_0_debet);
			// $("#rpd-0-kredit div").html(rpd_0_kredit);
			
			// Update Display Sewa Kamar RPD:
			var rpd_1_debet = parseFloat($("#rpd-1-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var rpd_1_kredit = parseFloat($("#rpd-1-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			rpd_1_debet += json.rpd_1_debet;
			rpd_1_debet = parseFloat(rpd_1_debet).formatMoney("2", ".", ",");
			rpd_1_kredit += json.rpd_1_kredit;
			rpd_1_kredit = parseFloat(rpd_1_kredit).formatMoney("2", ".", ",");
			$("#rpd-1-debet div").html(rpd_1_debet);
			$("#rpd-1-kredit div").html(rpd_1_kredit);
			
			// Update Display Tindakan / Jasa Dokter RPD:
			var rpd_2_debet = parseFloat($("#rpd-2-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var rpd_2_kredit = parseFloat($("#rpd-2-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			rpd_2_debet += json.rpd_2_debet;
			rpd_2_debet = parseFloat(rpd_2_debet).formatMoney("2", ".", ",");
			rpd_2_kredit += json.rpd_2_kredit;
			rpd_2_kredit = parseFloat(rpd_2_kredit).formatMoney("2", ".", ",");
			$("#rpd-2-debet div").html(rpd_2_debet);
			$("#rpd-2-kredit div").html(rpd_2_kredit);
			
			// Update Display Jasa / Tindakan Keperawatan RPD:
			var rpd_3_debet = parseFloat($("#rpd-3-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var rpd_3_kredit = parseFloat($("#rpd-3-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			rpd_3_debet += json.rpd_3_debet;
			rpd_3_debet = parseFloat(rpd_3_debet).formatMoney("2", ".", ",");
			rpd_3_kredit += json.rpd_3_kredit;
			rpd_3_kredit = parseFloat(rpd_3_kredit).formatMoney("2", ".", ",");
			$("#rpd-3-debet div").html(rpd_3_debet);
			$("#rpd-3-kredit div").html(rpd_3_kredit);
			
			// Update Display Alat Kesehatan RPD:
			var rpd_4_debet = parseFloat($("#rpd-4-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var rpd_4_kredit = parseFloat($("#rpd-4-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			rpd_4_debet += json.rpd_4_debet;
			rpd_4_debet = parseFloat(rpd_4_debet).formatMoney("2", ".", ",");
			rpd_4_kredit += json.rpd_4_kredit;
			rpd_4_kredit = parseFloat(rpd_4_kredit).formatMoney("2", ".", ",");
			$("#rpd-4-debet div").html(rpd_4_debet);
			$("#rpd-4-kredit div").html(rpd_4_kredit);
			
			// Update Display Lain-Lain RPD:
			var rpd_5_debet = parseFloat($("#rpd-5-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var rpd_5_kredit = parseFloat($("#rpd-5-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			rpd_5_debet += json.rpd_5_debet;
			rpd_5_debet = parseFloat(rpd_5_debet).formatMoney("2", ".", ",");
			rpd_5_kredit += json.rpd_5_kredit;
			rpd_5_kredit = parseFloat(rpd_5_kredit).formatMoney("2", ".", ",");
			$("#rpd-5-debet div").html(rpd_5_debet);
			$("#rpd-5-kredit div").html(rpd_5_kredit);
			
			// Update Subtotal RPE:
			// var rpe_0_debet = parseFloat($("#rpe-0-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			// var rpe_0_kredit = parseFloat($("#rpe-0-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			// rpe_0_debet += json.rpe_0_debet;
			// rpe_0_debet = parseFloat(rpe_0_debet).formatMoney("2", ".", ",");
			// rpe_0_kredit += json.rpe_0_kredit;
			// rpe_0_kredit = parseFloat(rpe_0_kredit).formatMoney("2", ".", ",");
			// $("#rpe-0-debet div").html(rpe_0_debet);
			// $("#rpe-0-kredit div").html(rpe_0_kredit);
			
			// Update Display Sewa Kamar RPE:
			var rpe_1_debet = parseFloat($("#rpe-1-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var rpe_1_kredit = parseFloat($("#rpe-1-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			rpe_1_debet += json.rpe_1_debet;
			rpe_1_debet = parseFloat(rpe_1_debet).formatMoney("2", ".", ",");
			rpe_1_kredit += json.rpe_1_kredit;
			rpe_1_kredit = parseFloat(rpe_1_kredit).formatMoney("2", ".", ",");
			$("#rpe-1-debet div").html(rpe_1_debet);
			$("#rpe-1-kredit div").html(rpe_1_kredit);
			
			// Update Display Tindakan / Jasa Dokter RPE:
			var rpe_2_debet = parseFloat($("#rpe-2-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var rpe_2_kredit = parseFloat($("#rpe-2-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			rpe_2_debet += json.rpe_2_debet;
			rpe_2_debet = parseFloat(rpe_2_debet).formatMoney("2", ".", ",");
			rpe_2_kredit += json.rpe_2_kredit;
			rpe_2_kredit = parseFloat(rpe_2_kredit).formatMoney("2", ".", ",");
			$("#rpe-2-debet div").html(rpe_2_debet);
			$("#rpe-2-kredit div").html(rpe_2_kredit);
			
			// Update Display Jasa / Tindakan Keperawatan RPE:
			var rpe_3_debet = parseFloat($("#rpe-3-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var rpe_3_kredit = parseFloat($("#rpe-3-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			rpe_3_debet += json.rpe_3_debet;
			rpe_3_debet = parseFloat(rpe_3_debet).formatMoney("2", ".", ",");
			rpe_3_kredit += json.rpe_3_kredit;
			rpe_3_kredit = parseFloat(rpe_3_kredit).formatMoney("2", ".", ",");
			$("#rpe-3-debet div").html(rpe_3_debet);
			$("#rpe-3-kredit div").html(rpe_3_kredit);
			
			// Update Display Alat Kesehatan RPE:
			var rpe_4_debet = parseFloat($("#rpe-4-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var rpe_4_kredit = parseFloat($("#rpe-4-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			rpe_4_debet += json.rpe_4_debet;
			rpe_4_debet = parseFloat(rpe_4_debet).formatMoney("2", ".", ",");
			rpe_4_kredit += json.rpe_4_kredit;
			rpe_4_kredit = parseFloat(rpe_4_kredit).formatMoney("2", ".", ",");
			$("#rpe-4-debet div").html(rpe_4_debet);
			$("#rpe-4-kredit div").html(rpe_4_kredit);
			
			// Update Display Lain-Lain RPE:
			var rpe_5_debet = parseFloat($("#rpe-5-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var rpe_5_kredit = parseFloat($("#rpe-5-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			rpe_5_debet += json.rpe_5_debet;
			rpe_5_debet = parseFloat(rpe_5_debet).formatMoney("2", ".", ",");
			rpe_5_kredit += json.rpe_5_kredit;
			rpe_5_kredit = parseFloat(rpe_5_kredit).formatMoney("2", ".", ",");
			$("#rpe-5-debet div").html(rpe_5_debet);
			$("#rpe-5-kredit div").html(rpe_5_kredit);
			
			// Update Subtotal RPKK:
			// var rpkk_0_debet = parseFloat($("#rpkk-0-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			// var rpkk_0_kredit = parseFloat($("#rpkk-0-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			// rpkk_0_debet += json.rpkk_0_debet;
			// rpkk_0_debet = parseFloat(rpkk_0_debet).formatMoney("2", ".", ",");
			// rpkk_0_kredit += json.rpkk_0_kredit;
			// rpkk_0_kredit = parseFloat(rpkk_0_kredit).formatMoney("2", ".", ",");
			// $("#rpkk-0-debet div").html(rpkk_0_debet);
			// $("#rpkk-0-kredit div").html(rpkk_0_kredit);
			
			// Update Display Sewa Kamar RPKK:
			var rpkk_1_debet = parseFloat($("#rpkk-1-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var rpkk_1_kredit = parseFloat($("#rpkk-1-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			rpkk_1_debet += json.rpkk_1_debet;
			rpkk_1_debet = parseFloat(rpkk_1_debet).formatMoney("2", ".", ",");
			rpkk_1_kredit += json.rpkk_1_kredit;
			rpkk_1_kredit = parseFloat(rpkk_1_kredit).formatMoney("2", ".", ",");
			$("#rpkk-1-debet div").html(rpkk_1_debet);
			$("#rpkk-1-kredit div").html(rpkk_1_kredit);
			
			// Update Display Tindakan / Jasa Dokter RPKK:
			var rpkk_2_debet = parseFloat($("#rpkk-2-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var rpkk_2_kredit = parseFloat($("#rpkk-2-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			rpkk_2_debet += json.rpkk_2_debet;
			rpkk_2_debet = parseFloat(rpkk_2_debet).formatMoney("2", ".", ",");
			rpkk_2_kredit += json.rpkk_2_kredit;
			rpkk_2_kredit = parseFloat(rpkk_2_kredit).formatMoney("2", ".", ",");
			$("#rpkk-2-debet div").html(rpkk_2_debet);
			$("#rpkk-2-kredit div").html(rpkk_2_kredit);
			
			// Update Display Jasa / Tindakan Kebidanan RPKK:
			var rpkk_3_debet = parseFloat($("#rpkk-3-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var rpkk_3_kredit = parseFloat($("#rpkk-3-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			rpkk_3_debet += json.rpkk_3_debet;
			rpkk_3_debet = parseFloat(rpkk_3_debet).formatMoney("2", ".", ",");
			rpkk_3_kredit += json.rpkk_3_kredit;
			rpkk_3_kredit = parseFloat(rpkk_3_kredit).formatMoney("2", ".", ",");
			$("#rpkk-3-debet div").html(rpkk_3_debet);
			$("#rpkk-3-kredit div").html(rpkk_3_kredit);
			
			// Update Display Alat Kesehatan RPKK:
			var rpkk_4_debet = parseFloat($("#rpkk-4-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var rpkk_4_kredit = parseFloat($("#rpkk-4-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			rpkk_4_debet += json.rpkk_4_debet;
			rpkk_4_debet = parseFloat(rpkk_4_debet).formatMoney("2", ".", ",");
			rpkk_4_kredit += json.rpkk_4_kredit;
			rpkk_4_kredit = parseFloat(rpkk_4_kredit).formatMoney("2", ".", ",");
			$("#rpkk-4-debet div").html(rpkk_4_debet);
			$("#rpkk-4-kredit div").html(rpkk_4_kredit);
			
			// Update Display Jasa / Tindakan Kebidanan RPKK:
			var rpkk_5_debet = parseFloat($("#rpkk-5-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var rpkk_5_kredit = parseFloat($("#rpkk-5-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			rpkk_5_debet += json.rpkk_5_debet;
			rpkk_5_debet = parseFloat(rpkk_5_debet).formatMoney("2", ".", ",");
			rpkk_5_kredit += json.rpkk_5_kredit;
			rpkk_5_kredit = parseFloat(rpkk_5_kredit).formatMoney("2", ".", ",");
			$("#rpkk-5-debet div").html(rpkk_5_debet);
			$("#rpkk-5-kredit div").html(rpkk_5_kredit);
			
			// Update Display Lain-Lain RPKK:
			var rpkk_6_debet = parseFloat($("#rpkk-6-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var rpkk_6_kredit = parseFloat($("#rpkk-6-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			rpkk_6_debet += json.rpkk_6_debet;
			rpkk_6_debet = parseFloat(rpkk_6_debet).formatMoney("2", ".", ",");
			rpkk_6_kredit += json.rpkk_6_kredit;
			rpkk_6_kredit = parseFloat(rpkk_6_kredit).formatMoney("2", ".", ",");
			$("#rpkk-6-debet div").html(rpkk_6_debet);
			$("#rpkk-6-kredit div").html(rpkk_6_kredit);
			
			// Update Subtotal ICU / RPO:
			// var icu_0_debet = parseFloat($("#icu-0-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			// var icu_0_kredit = parseFloat($("#icu-0-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			// icu_0_debet += json.icu_0_debet;
			// icu_0_debet = parseFloat(icu_0_debet).formatMoney("2", ".", ",");
			// icu_0_kredit += json.icu_0_kredit;
			// icu_0_kredit = parseFloat(icu_0_kredit).formatMoney("2", ".", ",");
			// $("#icu-0-debet div").html(icu_0_debet);
			// $("#icu-0-kredit div").html(icu_0_kredit);
			
			// Update Display Sewa Kamar ICU / RPO:
			var icu_1_debet = parseFloat($("#icu-1-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var icu_1_kredit = parseFloat($("#icu-1-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			icu_1_debet += json.icu_1_debet;
			icu_1_debet = parseFloat(icu_1_debet).formatMoney("2", ".", ",");
			icu_1_kredit += json.icu_1_kredit;
			icu_1_kredit = parseFloat(icu_1_kredit).formatMoney("2", ".", ",");
			$("#icu-1-debet div").html(icu_1_debet);
			$("#icu-1-kredit div").html(icu_1_kredit);
			
			// Update Display Tindakan / Jasa Dokter ICU / RPO:
			var icu_2_debet = parseFloat($("#icu-2-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var icu_2_kredit = parseFloat($("#icu-2-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			icu_2_debet += json.icu_2_debet;
			icu_2_debet = parseFloat(icu_2_debet).formatMoney("2", ".", ",");
			icu_2_kredit += json.icu_2_kredit;
			icu_2_kredit = parseFloat(icu_2_kredit).formatMoney("2", ".", ",");
			$("#icu-2-debet div").html(icu_2_debet);
			$("#icu-2-kredit div").html(icu_2_kredit);
			
			// Update Display Jasa / Tindakan Keperawatan ICU / RPO:
			var icu_3_debet = parseFloat($("#icu-3-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var icu_3_kredit = parseFloat($("#icu-3-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			icu_3_debet += json.icu_3_debet;
			icu_3_debet = parseFloat(icu_3_debet).formatMoney("2", ".", ",");
			icu_3_kredit += json.icu_3_kredit;
			icu_3_kredit = parseFloat(icu_3_kredit).formatMoney("2", ".", ",");
			$("#icu-3-debet div").html(icu_3_debet);
			$("#icu-3-kredit div").html(icu_3_kredit);
			
			// Update Display Alat Kesehatan ICU / RPO:
			var icu_4_debet = parseFloat($("#icu-4-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var icu_4_kredit = parseFloat($("#icu-4-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			icu_4_debet += json.icu_4_debet;
			icu_4_debet = parseFloat(icu_4_debet).formatMoney("2", ".", ",");
			icu_4_kredit += json.icu_4_kredit;
			icu_4_kredit = parseFloat(icu_4_kredit).formatMoney("2", ".", ",");
			$("#icu-4-debet div").html(icu_4_debet);
			$("#icu-4-kredit div").html(icu_4_kredit);
			
			// Update Display Lain-Lain ICU / RPO:
			var icu_5_debet = parseFloat($("#icu-5-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var icu_5_kredit = parseFloat($("#icu-5-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			icu_5_debet += json.icu_5_debet;
			icu_5_debet = parseFloat(icu_5_debet).formatMoney("2", ".", ",");
			icu_5_kredit += json.icu_5_kredit;
			icu_5_kredit = parseFloat(icu_5_kredit).formatMoney("2", ".", ",");
			$("#icu-5-debet div").html(icu_5_debet);
			$("#icu-5-kredit div").html(icu_5_kredit);
			
			// Update Subtotal UGD:
			// var instalasi_gawat_darurat_0_debet = parseFloat($("#instalasi_gawat_darurat-0-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			// var instalasi_gawat_darurat_0_kredit = parseFloat($("#instalasi_gawat_darurat-0-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			// instalasi_gawat_darurat_0_debet += json.instalasi_gawat_darurat_0_debet;
			// instalasi_gawat_darurat_0_debet = parseFloat(instalasi_gawat_darurat_0_debet).formatMoney("2", ".", ",");
			// instalasi_gawat_darurat_0_kredit += json.instalasi_gawat_darurat_0_kredit;
			// instalasi_gawat_darurat_0_kredit = parseFloat(instalasi_gawat_darurat_0_kredit).formatMoney("2", ".", ",");
			// $("#instalasi_gawat_darurat-0-debet div").html(instalasi_gawat_darurat_0_debet);
			// $("#instalasi_gawat_darurat-0-kredit div").html(instalasi_gawat_darurat_0_kredit);
			
			// Update Display Sewa Kamar UGD:
			var instalasi_gawat_darurat_1_debet = parseFloat($("#instalasi_gawat_darurat-1-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var instalasi_gawat_darurat_1_kredit = parseFloat($("#instalasi_gawat_darurat-1-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			instalasi_gawat_darurat_1_debet += json.instalasi_gawat_darurat_1_debet;
			instalasi_gawat_darurat_1_debet = parseFloat(instalasi_gawat_darurat_1_debet).formatMoney("2", ".", ",");
			instalasi_gawat_darurat_1_kredit += json.instalasi_gawat_darurat_1_kredit;
			instalasi_gawat_darurat_1_kredit = parseFloat(instalasi_gawat_darurat_1_kredit).formatMoney("2", ".", ",");
			$("#instalasi_gawat_darurat-1-debet div").html(instalasi_gawat_darurat_1_debet);
			$("#instalasi_gawat_darurat-1-kredit div").html(instalasi_gawat_darurat_1_kredit);
			
			// Update Display Tindakan / Jasa Dokter UGD:
			var instalasi_gawat_darurat_2_debet = parseFloat($("#instalasi_gawat_darurat-2-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var instalasi_gawat_darurat_2_kredit = parseFloat($("#instalasi_gawat_darurat-2-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			instalasi_gawat_darurat_2_debet += json.instalasi_gawat_darurat_2_debet;
			instalasi_gawat_darurat_2_debet = parseFloat(instalasi_gawat_darurat_2_debet).formatMoney("2", ".", ",");
			instalasi_gawat_darurat_2_kredit += json.instalasi_gawat_darurat_2_kredit;
			instalasi_gawat_darurat_2_kredit = parseFloat(instalasi_gawat_darurat_2_kredit).formatMoney("2", ".", ",");
			$("#instalasi_gawat_darurat-2-debet div").html(instalasi_gawat_darurat_2_debet);
			$("#instalasi_gawat_darurat-2-kredit div").html(instalasi_gawat_darurat_2_kredit);
			
			// Update Display Jasa / Tindakan Keperawatan UGD:
			var instalasi_gawat_darurat_3_debet = parseFloat($("#instalasi_gawat_darurat-3-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var instalasi_gawat_darurat_3_kredit = parseFloat($("#instalasi_gawat_darurat-3-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			instalasi_gawat_darurat_3_debet += json.instalasi_gawat_darurat_3_debet;
			instalasi_gawat_darurat_3_debet = parseFloat(instalasi_gawat_darurat_3_debet).formatMoney("2", ".", ",");
			instalasi_gawat_darurat_3_kredit += json.instalasi_gawat_darurat_3_kredit;
			instalasi_gawat_darurat_3_kredit = parseFloat(instalasi_gawat_darurat_3_kredit).formatMoney("2", ".", ",");
			$("#instalasi_gawat_darurat-3-debet div").html(instalasi_gawat_darurat_3_debet);
			$("#instalasi_gawat_darurat-3-kredit div").html(instalasi_gawat_darurat_3_kredit);
			
			// Update Display Alat Kesehatan UGD:
			var instalasi_gawat_darurat_4_debet = parseFloat($("#instalasi_gawat_darurat-4-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var instalasi_gawat_darurat_4_kredit = parseFloat($("#instalasi_gawat_darurat-4-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			instalasi_gawat_darurat_4_debet += json.instalasi_gawat_darurat_4_debet;
			instalasi_gawat_darurat_4_debet = parseFloat(instalasi_gawat_darurat_4_debet).formatMoney("2", ".", ",");
			instalasi_gawat_darurat_4_kredit += json.instalasi_gawat_darurat_4_kredit;
			instalasi_gawat_darurat_4_kredit = parseFloat(instalasi_gawat_darurat_4_kredit).formatMoney("2", ".", ",");
			$("#instalasi_gawat_darurat-4-debet div").html(instalasi_gawat_darurat_4_debet);
			$("#instalasi_gawat_darurat-4-kredit div").html(instalasi_gawat_darurat_4_kredit);
			
			// Update Display Lain-Lain UGD:
			var instalasi_gawat_darurat_5_debet = parseFloat($("#instalasi_gawat_darurat-5-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var instalasi_gawat_darurat_5_kredit = parseFloat($("#instalasi_gawat_darurat-5-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			instalasi_gawat_darurat_5_debet += json.instalasi_gawat_darurat_5_debet;
			instalasi_gawat_darurat_5_debet = parseFloat(instalasi_gawat_darurat_5_debet).formatMoney("2", ".", ",");
			instalasi_gawat_darurat_5_kredit += json.instalasi_gawat_darurat_5_kredit;
			instalasi_gawat_darurat_5_kredit = parseFloat(instalasi_gawat_darurat_5_kredit).formatMoney("2", ".", ",");
			$("#instalasi_gawat_darurat-5-debet div").html(instalasi_gawat_darurat_5_debet);
			$("#instalasi_gawat_darurat-5-kredit div").html(instalasi_gawat_darurat_5_kredit);
			
			// Update Subtotal Poli Umum:
			// var poliumum_0_debet = parseFloat($("#poliumum-0-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			// var poliumum_0_kredit = parseFloat($("#poliumum-0-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			// poliumum_0_debet += json.poliumum_0_debet;
			// poliumum_0_debet = parseFloat(poliumum_0_debet).formatMoney("2", ".", ",");
			// poliumum_0_kredit += json.poliumum_0_kredit;
			// poliumum_0_kredit = parseFloat(poliumum_0_kredit).formatMoney("2", ".", ",");
			// $("#poliumum-0-debet div").html(poliumum_0_debet);
			// $("#poliumum-0-kredit div").html(poliumum_0_kredit);
			
			// Update Display Sewa Kamar Poli Umum:
			var poliumum_1_debet = parseFloat($("#poliumum-1-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var poliumum_1_kredit = parseFloat($("#poliumum-1-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			poliumum_1_debet += json.poliumum_1_debet;
			poliumum_1_debet = parseFloat(poliumum_1_debet).formatMoney("2", ".", ",");
			poliumum_1_kredit += json.poliumum_1_kredit;
			poliumum_1_kredit = parseFloat(poliumum_1_kredit).formatMoney("2", ".", ",");
			$("#poliumum-1-debet div").html(poliumum_1_debet);
			$("#poliumum-1-kredit div").html(poliumum_1_kredit);
			
			// Update Display Tindakan / Jasa Dokter Poli Umum:
			var poliumum_2_debet = parseFloat($("#poliumum-2-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var poliumum_2_kredit = parseFloat($("#poliumum-2-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			poliumum_2_debet += json.poliumum_2_debet;
			poliumum_2_debet = parseFloat(poliumum_2_debet).formatMoney("2", ".", ",");
			poliumum_2_kredit += json.poliumum_2_kredit;
			poliumum_2_kredit = parseFloat(poliumum_2_kredit).formatMoney("2", ".", ",");
			$("#poliumum-2-debet div").html(poliumum_2_debet);
			$("#poliumum-2-kredit div").html(poliumum_2_kredit);
			
			// Update Display Jasa / Tindakan Keperawatan Poli Umum:
			var poliumum_3_debet = parseFloat($("#poliumum-3-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var poliumum_3_kredit = parseFloat($("#poliumum-3-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			poliumum_3_debet += json.poliumum_3_debet;
			poliumum_3_debet = parseFloat(poliumum_3_debet).formatMoney("2", ".", ",");
			poliumum_3_kredit += json.poliumum_3_kredit;
			poliumum_3_kredit = parseFloat(poliumum_3_kredit).formatMoney("2", ".", ",");
			$("#poliumum-3-debet div").html(poliumum_3_debet);
			$("#poliumum-3-kredit div").html(poliumum_3_kredit);
			
			// Update Display Alat Kesehatan Poli Umum:
			var poliumum_4_debet = parseFloat($("#poliumum-4-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var poliumum_4_kredit = parseFloat($("#poliumum-4-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			poliumum_4_debet += json.poliumum_4_debet;
			poliumum_4_debet = parseFloat(poliumum_4_debet).formatMoney("2", ".", ",");
			poliumum_4_kredit += json.poliumum_4_kredit;
			poliumum_4_kredit = parseFloat(poliumum_4_kredit).formatMoney("2", ".", ",");
			$("#poliumum-4-debet div").html(poliumum_4_debet);
			$("#poliumum-4-kredit div").html(poliumum_4_kredit);
			
			// Update Display Lain-Lain Poli Umum:
			var poliumum_5_debet = parseFloat($("#poliumum-5-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var poliumum_5_kredit = parseFloat($("#poliumum-5-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			poliumum_5_debet += json.poliumum_5_debet;
			poliumum_5_debet = parseFloat(poliumum_5_debet).formatMoney("2", ".", ",");
			poliumum_5_kredit += json.poliumum_5_kredit;
			poliumum_5_kredit = parseFloat(poliumum_5_kredit).formatMoney("2", ".", ",");
			$("#poliumum-5-debet div").html(poliumum_5_debet);
			$("#poliumum-5-kredit div").html(poliumum_5_kredit);
			
			// Update Subtotal Poli Gigi:
			// var poligigi_0_debet = parseFloat($("#poligigi-0-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			// var poligigi_0_kredit = parseFloat($("#poligigi-0-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			// poligigi_0_debet += json.poligigi_0_debet;
			// poligigi_0_debet = parseFloat(poligigi_0_debet).formatMoney("2", ".", ",");
			// poligigi_0_kredit += json.poligigi_0_kredit;
			// poligigi_0_kredit = parseFloat(poligigi_0_kredit).formatMoney("2", ".", ",");
			// $("#poligigi-0-debet div").html(poligigi_0_debet);
			// $("#poligigi-0-kredit div").html(poligigi_0_kredit);
			
			// Update Display Sewa Kamar Poli Gigi:
			var poligigi_1_debet = parseFloat($("#poligigi-1-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var poligigi_1_kredit = parseFloat($("#poligigi-1-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			poligigi_1_debet += json.poligigi_1_debet;
			poligigi_1_debet = parseFloat(poligigi_1_debet).formatMoney("2", ".", ",");
			poligigi_1_kredit += json.poligigi_1_kredit;
			poligigi_1_kredit = parseFloat(poligigi_1_kredit).formatMoney("2", ".", ",");
			$("#poligigi-1-debet div").html(poligigi_1_debet);
			$("#poligigi-1-kredit div").html(poligigi_1_kredit);
			
			// Update Display Tindakan / Jasa Dokter Poli Gigi:
			var poligigi_2_debet = parseFloat($("#poligigi-2-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var poligigi_2_kredit = parseFloat($("#poligigi-2-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			poligigi_2_debet += json.poligigi_2_debet;
			poligigi_2_debet = parseFloat(poligigi_2_debet).formatMoney("2", ".", ",");
			poligigi_2_kredit += json.poligigi_2_kredit;
			poligigi_2_kredit = parseFloat(poligigi_2_kredit).formatMoney("2", ".", ",");
			$("#poligigi-2-debet div").html(poligigi_2_debet);
			$("#poligigi-2-kredit div").html(poligigi_2_kredit);
			
			// Update Display Jasa / Tindakan Keperawatan Poli Gigi:
			var poligigi_3_debet = parseFloat($("#poligigi-3-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var poligigi_3_kredit = parseFloat($("#poligigi-3-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			poligigi_3_debet += json.poligigi_3_debet;
			poligigi_3_debet = parseFloat(poligigi_3_debet).formatMoney("2", ".", ",");
			poligigi_3_kredit += json.poligigi_3_kredit;
			poligigi_3_kredit = parseFloat(poligigi_3_kredit).formatMoney("2", ".", ",");
			$("#poligigi-3-debet div").html(poligigi_3_debet);
			$("#poligigi-3-kredit div").html(poligigi_3_kredit);
			
			// Update Display Alat Kesehatan Poli Gigi:
			var poligigi_4_debet = parseFloat($("#poligigi-4-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var poligigi_4_kredit = parseFloat($("#poligigi-4-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			poligigi_4_debet += json.poligigi_4_debet;
			poligigi_4_debet = parseFloat(poligigi_4_debet).formatMoney("2", ".", ",");
			poligigi_4_kredit += json.poligigi_4_kredit;
			poligigi_4_kredit = parseFloat(poligigi_4_kredit).formatMoney("2", ".", ",");
			$("#poligigi-4-debet div").html(poligigi_4_debet);
			$("#poligigi-4-kredit div").html(poligigi_4_kredit);
			
			// Update Display Lain-Lain Poli Gigi:
			var poligigi_5_debet = parseFloat($("#poligigi-5-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var poligigi_5_kredit = parseFloat($("#poligigi-5-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			poligigi_5_debet += json.poligigi_5_debet;
			poligigi_5_debet = parseFloat(poligigi_5_debet).formatMoney("2", ".", ",");
			poligigi_5_kredit += json.poligigi_5_kredit;
			poligigi_5_kredit = parseFloat(poligigi_5_kredit).formatMoney("2", ".", ",");
			$("#poligigi-5-debet div").html(poligigi_5_debet);
			$("#poligigi-5-kredit div").html(poligigi_5_kredit);
			
			// Update Subtotal Poli Anak:
			// var poli_anak_0_debet = parseFloat($("#poli_anak-0-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			// var poli_anak_0_kredit = parseFloat($("#poli_anak-0-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			// poli_anak_0_debet += json.poli_anak_0_debet;
			// poli_anak_0_debet = parseFloat(poli_anak_0_debet).formatMoney("2", ".", ",");
			// poli_anak_0_kredit += json.poli_anak_0_kredit;
			// poli_anak_0_kredit = parseFloat(poli_anak_0_kredit).formatMoney("2", ".", ",");
			// $("#poli_anak-0-debet div").html(poli_anak_0_debet);
			// $("#poli_anak-0-kredit div").html(poli_anak_0_kredit);
			
			// Update Display Sewa Kamar Poli Anak:
			var poli_anak_1_debet = parseFloat($("#poli_anak-1-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var poli_anak_1_kredit = parseFloat($("#poli_anak-1-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			poli_anak_1_debet += json.poli_anak_1_debet;
			poli_anak_1_debet = parseFloat(poli_anak_1_debet).formatMoney("2", ".", ",");
			poli_anak_1_kredit += json.poli_anak_1_kredit;
			poli_anak_1_kredit = parseFloat(poli_anak_1_kredit).formatMoney("2", ".", ",");
			$("#poli_anak-1-debet div").html(poli_anak_1_debet);
			$("#poli_anak-1-kredit div").html(poli_anak_1_kredit);
			
			// Update Display Tindakan / Jasa Dokter Poli Anak:
			var poli_anak_2_debet = parseFloat($("#poli_anak-2-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var poli_anak_2_kredit = parseFloat($("#poli_anak-2-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			poli_anak_2_debet += json.poli_anak_2_debet;
			poli_anak_2_debet = parseFloat(poli_anak_2_debet).formatMoney("2", ".", ",");
			poli_anak_2_kredit += json.poli_anak_2_kredit;
			poli_anak_2_kredit = parseFloat(poli_anak_2_kredit).formatMoney("2", ".", ",");
			$("#poli_anak-2-debet div").html(poli_anak_2_debet);
			$("#poli_anak-2-kredit div").html(poli_anak_2_kredit);
			
			// Update Display Jasa / Tindakan Keperawatan Poli Anak:
			var poli_anak_3_debet = parseFloat($("#poli_anak-3-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var poli_anak_3_kredit = parseFloat($("#poli_anak-3-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			poli_anak_3_debet += json.poli_anak_3_debet;
			poli_anak_3_debet = parseFloat(poli_anak_3_debet).formatMoney("2", ".", ",");
			poli_anak_3_kredit += json.poli_anak_3_kredit;
			poli_anak_3_kredit = parseFloat(poli_anak_3_kredit).formatMoney("2", ".", ",");
			$("#poli_anak-3-debet div").html(poli_anak_3_debet);
			$("#poli_anak-3-kredit div").html(poli_anak_3_kredit);
			
			// Update Display Alat Kesehatan Poli Anak:
			var poli_anak_4_debet = parseFloat($("#poli_anak-4-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var poli_anak_4_kredit = parseFloat($("#poli_anak-4-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			poli_anak_4_debet += json.poli_anak_4_debet;
			poli_anak_4_debet = parseFloat(poli_anak_4_debet).formatMoney("2", ".", ",");
			poli_anak_4_kredit += json.poli_anak_4_kredit;
			poli_anak_4_kredit = parseFloat(poli_anak_4_kredit).formatMoney("2", ".", ",");
			$("#poli_anak-4-debet div").html(poli_anak_4_debet);
			$("#poli_anak-4-kredit div").html(poli_anak_4_kredit);
			
			// Update Display Lain-Lain Poli Anak:
			var poli_anak_5_debet = parseFloat($("#poli_anak-5-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var poli_anak_5_kredit = parseFloat($("#poli_anak-5-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			poli_anak_5_debet += json.poli_anak_5_debet;
			poli_anak_5_debet = parseFloat(poli_anak_5_debet).formatMoney("2", ".", ",");
			poli_anak_5_kredit += json.poli_anak_5_kredit;
			poli_anak_5_kredit = parseFloat(poli_anak_5_kredit).formatMoney("2", ".", ",");
			$("#poli_anak-5-debet div").html(poli_anak_5_debet);
			$("#poli_anak-5-kredit div").html(poli_anak_5_kredit);
			
			// Update Subtotal Poli Obgyn:
			// var poli_obgyn_0_debet = parseFloat($("#poli_obgyn-0-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			// var poli_obgyn_0_kredit = parseFloat($("#poli_obgyn-0-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			// poli_obgyn_0_debet += json.poli_obgyn_0_debet;
			// poli_obgyn_0_debet = parseFloat(poli_obgyn_0_debet).formatMoney("2", ".", ",");
			// poli_obgyn_0_kredit += json.poli_obgyn_0_kredit;
			// poli_obgyn_0_kredit = parseFloat(poli_obgyn_0_kredit).formatMoney("2", ".", ",");
			// $("#poli_obgyn-0-debet div").html(poli_obgyn_0_debet);
			// $("#poli_obgyn-0-kredit div").html(poli_obgyn_0_kredit);
			
			// Update Display Sewa Kamar Poli Obgyn:
			var poli_obgyn_1_debet = parseFloat($("#poli_obgyn-1-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var poli_obgyn_1_kredit = parseFloat($("#poli_obgyn-1-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			poli_obgyn_1_debet += json.poli_obgyn_1_debet;
			poli_obgyn_1_debet = parseFloat(poli_obgyn_1_debet).formatMoney("2", ".", ",");
			poli_obgyn_1_kredit += json.poli_obgyn_1_kredit;
			poli_obgyn_1_kredit = parseFloat(poli_obgyn_1_kredit).formatMoney("2", ".", ",");
			$("#poli_obgyn-1-debet div").html(poli_obgyn_1_debet);
			$("#poli_obgyn-1-kredit div").html(poli_obgyn_1_kredit);
			
			// Update Display Tindakan / Jasa Dokter Poli Obgyn:
			var poli_obgyn_2_debet = parseFloat($("#poli_obgyn-2-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var poli_obgyn_2_kredit = parseFloat($("#poli_obgyn-2-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			poli_obgyn_2_debet += json.poli_obgyn_2_debet;
			poli_obgyn_2_debet = parseFloat(poli_obgyn_2_debet).formatMoney("2", ".", ",");
			poli_obgyn_2_kredit += json.poli_obgyn_2_kredit;
			poli_obgyn_2_kredit = parseFloat(poli_obgyn_2_kredit).formatMoney("2", ".", ",");
			$("#poli_obgyn-2-debet div").html(poli_obgyn_2_debet);
			$("#poli_obgyn-2-kredit div").html(poli_obgyn_2_kredit);
			
			// Update Display Jasa / Tindakan Keperawatan Poli Obgyn:
			var poli_obgyn_3_debet = parseFloat($("#poli_obgyn-3-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var poli_obgyn_3_kredit = parseFloat($("#poli_obgyn-3-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			poli_obgyn_3_debet += json.poli_obgyn_3_debet;
			poli_obgyn_3_debet = parseFloat(poli_obgyn_3_debet).formatMoney("2", ".", ",");
			poli_obgyn_3_kredit += json.poli_obgyn_3_kredit;
			poli_obgyn_3_kredit = parseFloat(poli_obgyn_3_kredit).formatMoney("2", ".", ",");
			$("#poli_obgyn-3-debet div").html(poli_obgyn_3_debet);
			$("#poli_obgyn-3-kredit div").html(poli_obgyn_3_kredit);
			
			// Update Display Alat Kesehatan Poli Obgyn:
			var poli_obgyn_4_debet = parseFloat($("#poli_obgyn-4-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var poli_obgyn_4_kredit = parseFloat($("#poli_obgyn-4-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			poli_obgyn_4_debet += json.poli_obgyn_4_debet;
			poli_obgyn_4_debet = parseFloat(poli_obgyn_4_debet).formatMoney("2", ".", ",");
			poli_obgyn_4_kredit += json.poli_obgyn_4_kredit;
			poli_obgyn_4_kredit = parseFloat(poli_obgyn_4_kredit).formatMoney("2", ".", ",");
			$("#poli_obgyn-4-debet div").html(poli_obgyn_4_debet);
			$("#poli_obgyn-4-kredit div").html(poli_obgyn_4_kredit);
			
			// Update Display Lain-Lain Poli Obgyn:
			var poli_obgyn_5_debet = parseFloat($("#poli_obgyn-5-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var poli_obgyn_5_kredit = parseFloat($("#poli_obgyn-5-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			poli_obgyn_5_debet += json.poli_obgyn_5_debet;
			poli_obgyn_5_debet = parseFloat(poli_obgyn_5_debet).formatMoney("2", ".", ",");
			poli_obgyn_5_kredit += json.poli_obgyn_5_kredit;
			poli_obgyn_5_kredit = parseFloat(poli_obgyn_5_kredit).formatMoney("2", ".", ",");
			$("#poli_obgyn-5-debet div").html(poli_obgyn_5_debet);
			$("#poli_obgyn-5-kredit div").html(poli_obgyn_5_kredit);
			
			// Update Subtotal Poli Bedah:
			// var poli_bedah_0_debet = parseFloat($("#poli_bedah-0-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			// var poli_bedah_0_kredit = parseFloat($("#poli_bedah-0-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			// poli_bedah_0_debet += json.poli_bedah_0_debet;
			// poli_bedah_0_debet = parseFloat(poli_bedah_0_debet).formatMoney("2", ".", ",");
			// poli_bedah_0_kredit += json.poli_bedah_0_kredit;
			// poli_bedah_0_kredit = parseFloat(poli_bedah_0_kredit).formatMoney("2", ".", ",");
			// $("#poli_bedah-0-debet div").html(poli_bedah_0_debet);
			// $("#poli_bedah-0-kredit div").html(poli_bedah_0_kredit);
			
			// Update Display Sewa Kamar Poli Bedah:
			var poli_bedah_1_debet = parseFloat($("#poli_bedah-1-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var poli_bedah_1_kredit = parseFloat($("#poli_bedah-1-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			poli_bedah_1_debet += json.poli_bedah_1_debet;
			poli_bedah_1_debet = parseFloat(poli_bedah_1_debet).formatMoney("2", ".", ",");
			poli_bedah_1_kredit += json.poli_bedah_1_kredit;
			poli_bedah_1_kredit = parseFloat(poli_bedah_1_kredit).formatMoney("2", ".", ",");
			$("#poli_bedah-1-debet div").html(poli_bedah_1_debet);
			$("#poli_bedah-1-kredit div").html(poli_bedah_1_kredit);
			
			// Update Display Tindakan / Jasa Dokter Poli Bedah:
			var poli_bedah_2_debet = parseFloat($("#poli_bedah-2-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var poli_bedah_2_kredit = parseFloat($("#poli_bedah-2-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			poli_bedah_2_debet += json.poli_bedah_2_debet;
			poli_bedah_2_debet = parseFloat(poli_bedah_2_debet).formatMoney("2", ".", ",");
			poli_bedah_2_kredit += json.poli_bedah_2_kredit;
			poli_bedah_2_kredit = parseFloat(poli_bedah_2_kredit).formatMoney("2", ".", ",");
			$("#poli_bedah-2-debet div").html(poli_bedah_2_debet);
			$("#poli_bedah-2-kredit div").html(poli_bedah_2_kredit);
			
			// Update Display Jasa / Tindakan Keperawatan Poli Bedah:
			var poli_bedah_3_debet = parseFloat($("#poli_bedah-3-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var poli_bedah_3_kredit = parseFloat($("#poli_bedah-3-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			poli_bedah_3_debet += json.poli_bedah_3_debet;
			poli_bedah_3_debet = parseFloat(poli_bedah_3_debet).formatMoney("2", ".", ",");
			poli_bedah_3_kredit += json.poli_bedah_3_kredit;
			poli_bedah_3_kredit = parseFloat(poli_bedah_3_kredit).formatMoney("2", ".", ",");
			$("#poli_bedah-3-debet div").html(poli_bedah_3_debet);
			$("#poli_bedah-3-kredit div").html(poli_bedah_3_kredit);
			
			// Update Display Alat Kesehatan Poli Bedah:
			var poli_bedah_4_debet = parseFloat($("#poli_bedah-4-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var poli_bedah_4_kredit = parseFloat($("#poli_bedah-4-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			poli_bedah_4_debet += json.poli_bedah_4_debet;
			poli_bedah_4_debet = parseFloat(poli_bedah_4_debet).formatMoney("2", ".", ",");
			poli_bedah_4_kredit += json.poli_bedah_4_kredit;
			poli_bedah_4_kredit = parseFloat(poli_bedah_4_kredit).formatMoney("2", ".", ",");
			$("#poli_bedah-4-debet div").html(poli_bedah_4_debet);
			$("#poli_bedah-4-kredit div").html(poli_bedah_4_kredit);
			
			// Update Display Lain-Lain Poli Bedah:
			var poli_bedah_5_debet = parseFloat($("#poli_bedah-5-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var poli_bedah_5_kredit = parseFloat($("#poli_bedah-5-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			poli_bedah_5_debet += json.poli_bedah_5_debet;
			poli_bedah_5_debet = parseFloat(poli_bedah_5_debet).formatMoney("2", ".", ",");
			poli_bedah_5_kredit += json.poli_bedah_5_kredit;
			poli_bedah_5_kredit = parseFloat(poli_bedah_5_kredit).formatMoney("2", ".", ",");
			$("#poli_bedah-5-debet div").html(poli_bedah_5_debet);
			$("#poli_bedah-5-kredit div").html(poli_bedah_5_kredit);
			
			// Update Subtotal Poli Mata:
			// var poli_mata_0_debet = parseFloat($("#poli_mata-0-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			// var poli_mata_0_kredit = parseFloat($("#poli_mata-0-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			// poli_mata_0_debet += json.poli_mata_0_debet;
			// poli_mata_0_debet = parseFloat(poli_mata_0_debet).formatMoney("2", ".", ",");
			// poli_mata_0_kredit += json.poli_mata_0_kredit;
			// poli_mata_0_kredit = parseFloat(poli_mata_0_kredit).formatMoney("2", ".", ",");
			// $("#poli_mata-0-debet div").html(poli_mata_0_debet);
			// $("#poli_mata-0-kredit div").html(poli_mata_0_kredit);
			
			// Update Display Sewa Kamar Poli Mata:
			var poli_mata_1_debet = parseFloat($("#poli_mata-1-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var poli_mata_1_kredit = parseFloat($("#poli_mata-1-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			poli_mata_1_debet += json.poli_mata_1_debet;
			poli_mata_1_debet = parseFloat(poli_mata_1_debet).formatMoney("2", ".", ",");
			poli_mata_1_kredit += json.poli_mata_1_kredit;
			poli_mata_1_kredit = parseFloat(poli_mata_1_kredit).formatMoney("2", ".", ",");
			$("#poli_mata-1-debet div").html(poli_mata_1_debet);
			$("#poli_mata-1-kredit div").html(poli_mata_1_kredit);
			
			// Update Display Tindakan / Jasa Dokter Poli Mata:
			var poli_mata_2_debet = parseFloat($("#poli_mata-2-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var poli_mata_2_kredit = parseFloat($("#poli_mata-2-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			poli_mata_2_debet += json.poli_mata_2_debet;
			poli_mata_2_debet = parseFloat(poli_mata_2_debet).formatMoney("2", ".", ",");
			poli_mata_2_kredit += json.poli_mata_2_kredit;
			poli_mata_2_kredit = parseFloat(poli_mata_2_kredit).formatMoney("2", ".", ",");
			$("#poli_mata-2-debet div").html(poli_mata_2_debet);
			$("#poli_mata-2-kredit div").html(poli_mata_2_kredit);
			
			// Update Display Jasa / Tindakan Keperawatan Poli Mata:
			var poli_mata_3_debet = parseFloat($("#poli_mata-3-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var poli_mata_3_kredit = parseFloat($("#poli_mata-3-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			poli_mata_3_debet += json.poli_mata_3_debet;
			poli_mata_3_debet = parseFloat(poli_mata_3_debet).formatMoney("2", ".", ",");
			poli_mata_3_kredit += json.poli_mata_3_kredit;
			poli_mata_3_kredit = parseFloat(poli_mata_3_kredit).formatMoney("2", ".", ",");
			$("#poli_mata-3-debet div").html(poli_mata_3_debet);
			$("#poli_mata-3-kredit div").html(poli_mata_3_kredit);
			
			// Update Display Alat Kesehatan Poli Mata:
			var poli_mata_4_debet = parseFloat($("#poli_mata-4-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var poli_mata_4_kredit = parseFloat($("#poli_mata-4-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			poli_mata_4_debet += json.poli_mata_4_debet;
			poli_mata_4_debet = parseFloat(poli_mata_4_debet).formatMoney("2", ".", ",");
			poli_mata_4_kredit += json.poli_mata_4_kredit;
			poli_mata_4_kredit = parseFloat(poli_mata_4_kredit).formatMoney("2", ".", ",");
			$("#poli_mata-4-debet div").html(poli_mata_4_debet);
			$("#poli_mata-4-kredit div").html(poli_mata_4_kredit);
			
			// Update Display Lain-Lain Poli Mata:
			var poli_mata_5_debet = parseFloat($("#poli_mata-5-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var poli_mata_5_kredit = parseFloat($("#poli_mata-5-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			poli_mata_5_debet += json.poli_mata_5_debet;
			poli_mata_5_debet = parseFloat(poli_mata_5_debet).formatMoney("2", ".", ",");
			poli_mata_5_kredit += json.poli_mata_5_kredit;
			poli_mata_5_kredit = parseFloat(poli_mata_5_kredit).formatMoney("2", ".", ",");
			$("#poli_mata-5-debet div").html(poli_mata_5_debet);
			$("#poli_mata-5-kredit div").html(poli_mata_5_kredit);
			
			// Update Subtotal Poli THT:
			// var poli_tht_0_debet = parseFloat($("#poli_tht-0-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			// var poli_tht_0_kredit = parseFloat($("#poli_tht-0-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			// poli_tht_0_debet += json.poli_tht_0_debet;
			// poli_tht_0_debet = parseFloat(poli_tht_0_debet).formatMoney("2", ".", ",");
			// poli_tht_0_kredit += json.poli_tht_0_kredit;
			// poli_tht_0_kredit = parseFloat(poli_tht_0_kredit).formatMoney("2", ".", ",");
			// $("#poli_tht-0-debet div").html(poli_tht_0_debet);
			// $("#poli_tht-0-kredit div").html(poli_tht_0_kredit);
			
			// Update Display Sewa Kamar Poli THT:
			var poli_tht_1_debet = parseFloat($("#poli_tht-1-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var poli_tht_1_kredit = parseFloat($("#poli_tht-1-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			poli_tht_1_debet += json.poli_tht_1_debet;
			poli_tht_1_debet = parseFloat(poli_tht_1_debet).formatMoney("2", ".", ",");
			poli_tht_1_kredit += json.poli_tht_1_kredit;
			poli_tht_1_kredit = parseFloat(poli_tht_1_kredit).formatMoney("2", ".", ",");
			$("#poli_tht-1-debet div").html(poli_tht_1_debet);
			$("#poli_tht-1-kredit div").html(poli_tht_1_kredit);
			
			// Update Display Tindakan / Jasa Dokter Poli THT:
			var poli_tht_2_debet = parseFloat($("#poli_tht-2-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var poli_tht_2_kredit = parseFloat($("#poli_tht-2-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			poli_tht_2_debet += json.poli_tht_2_debet;
			poli_tht_2_debet = parseFloat(poli_tht_2_debet).formatMoney("2", ".", ",");
			poli_tht_2_kredit += json.poli_tht_2_kredit;
			poli_tht_2_kredit = parseFloat(poli_tht_2_kredit).formatMoney("2", ".", ",");
			$("#poli_tht-2-debet div").html(poli_tht_2_debet);
			$("#poli_tht-2-kredit div").html(poli_tht_2_kredit);
			
			// Update Display Jasa / Tindakan Keperawatan Poli THT:
			var poli_tht_3_debet = parseFloat($("#poli_tht-3-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var poli_tht_3_kredit = parseFloat($("#poli_tht-3-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			poli_tht_3_debet += json.poli_tht_3_debet;
			poli_tht_3_debet = parseFloat(poli_tht_3_debet).formatMoney("2", ".", ",");
			poli_tht_3_kredit += json.poli_tht_3_kredit;
			poli_tht_3_kredit = parseFloat(poli_tht_3_kredit).formatMoney("2", ".", ",");
			$("#poli_tht-3-debet div").html(poli_tht_3_debet);
			$("#poli_tht-3-kredit div").html(poli_tht_3_kredit);
			
			// Update Display Alat Kesehatan Poli THT:
			var poli_tht_4_debet = parseFloat($("#poli_tht-4-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var poli_tht_4_kredit = parseFloat($("#poli_tht-4-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			poli_tht_4_debet += json.poli_tht_4_debet;
			poli_tht_4_debet = parseFloat(poli_tht_4_debet).formatMoney("2", ".", ",");
			poli_tht_4_kredit += json.poli_tht_4_kredit;
			poli_tht_4_kredit = parseFloat(poli_tht_4_kredit).formatMoney("2", ".", ",");
			$("#poli_tht-4-debet div").html(poli_tht_4_debet);
			$("#poli_tht-4-kredit div").html(poli_tht_4_kredit);
			
			// Update Display Lain-Lain Poli THT:
			var poli_tht_5_debet = parseFloat($("#poli_tht-5-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var poli_tht_5_kredit = parseFloat($("#poli_tht-5-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			poli_tht_5_debet += json.poli_tht_5_debet;
			poli_tht_5_debet = parseFloat(poli_tht_5_debet).formatMoney("2", ".", ",");
			poli_tht_5_kredit += json.poli_tht_5_kredit;
			poli_tht_5_kredit = parseFloat(poli_tht_5_kredit).formatMoney("2", ".", ",");
			$("#poli_tht-5-debet div").html(poli_tht_5_debet);
			$("#poli_tht-5-kredit div").html(poli_tht_5_kredit);
			
			// Update Subtotal Poli Kulit dan Kelamin:
			// var poli_kulit_dan_kelamin_0_debet = parseFloat($("#poli_kulit_dan_kelamin-0-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			// var poli_kulit_dan_kelamin_0_kredit = parseFloat($("#poli_kulit_dan_kelamin-0-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			// poli_kulit_dan_kelamin_0_debet += json.poli_kulit_dan_kelamin_0_debet;
			// poli_kulit_dan_kelamin_0_debet = parseFloat(poli_kulit_dan_kelamin_0_debet).formatMoney("2", ".", ",");
			// poli_kulit_dan_kelamin_0_kredit += json.poli_kulit_dan_kelamin_0_kredit;
			// poli_kulit_dan_kelamin_0_kredit = parseFloat(poli_kulit_dan_kelamin_0_kredit).formatMoney("2", ".", ",");
			// $("#poli_kulit_dan_kelamin-0-debet div").html(poli_kulit_dan_kelamin_0_debet);
			// $("#poli_kulit_dan_kelamin-0-kredit div").html(poli_kulit_dan_kelamin_0_kredit);
			
			// Update Display Sewa Kamar Poli Kulit dan Kelamin:
			var poli_kulit_dan_kelamin_1_debet = parseFloat($("#poli_kulit_dan_kelamin-1-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var poli_kulit_dan_kelamin_1_kredit = parseFloat($("#poli_kulit_dan_kelamin-1-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			poli_kulit_dan_kelamin_1_debet += json.poli_kulit_dan_kelamin_1_debet;
			poli_kulit_dan_kelamin_1_debet = parseFloat(poli_kulit_dan_kelamin_1_debet).formatMoney("2", ".", ",");
			poli_kulit_dan_kelamin_1_kredit += json.poli_kulit_dan_kelamin_1_kredit;
			poli_kulit_dan_kelamin_1_kredit = parseFloat(poli_kulit_dan_kelamin_1_kredit).formatMoney("2", ".", ",");
			$("#poli_kulit_dan_kelamin-1-debet div").html(poli_kulit_dan_kelamin_1_debet);
			$("#poli_kulit_dan_kelamin-1-kredit div").html(poli_kulit_dan_kelamin_1_kredit);
			
			// Update Display Tindakan / Jasa Dokter Poli Kulit dan Kelamin:
			var poli_kulit_dan_kelamin_2_debet = parseFloat($("#poli_kulit_dan_kelamin-2-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var poli_kulit_dan_kelamin_2_kredit = parseFloat($("#poli_kulit_dan_kelamin-2-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			poli_kulit_dan_kelamin_2_debet += json.poli_kulit_dan_kelamin_2_debet;
			poli_kulit_dan_kelamin_2_debet = parseFloat(poli_kulit_dan_kelamin_2_debet).formatMoney("2", ".", ",");
			poli_kulit_dan_kelamin_2_kredit += json.poli_kulit_dan_kelamin_2_kredit;
			poli_kulit_dan_kelamin_2_kredit = parseFloat(poli_kulit_dan_kelamin_2_kredit).formatMoney("2", ".", ",");
			$("#poli_kulit_dan_kelamin-2-debet div").html(poli_kulit_dan_kelamin_2_debet);
			$("#poli_kulit_dan_kelamin-2-kredit div").html(poli_kulit_dan_kelamin_2_kredit);
			
			// Update Display Jasa / Tindakan Keperawatan Poli Kulit dan Kelamin:
			var poli_kulit_dan_kelamin_3_debet = parseFloat($("#poli_kulit_dan_kelamin-3-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var poli_kulit_dan_kelamin_3_kredit = parseFloat($("#poli_kulit_dan_kelamin-3-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			poli_kulit_dan_kelamin_3_debet += json.poli_kulit_dan_kelamin_3_debet;
			poli_kulit_dan_kelamin_3_debet = parseFloat(poli_kulit_dan_kelamin_3_debet).formatMoney("2", ".", ",");
			poli_kulit_dan_kelamin_3_kredit += json.poli_kulit_dan_kelamin_3_kredit;
			poli_kulit_dan_kelamin_3_kredit = parseFloat(poli_kulit_dan_kelamin_3_kredit).formatMoney("2", ".", ",");
			$("#poli_kulit_dan_kelamin-3-debet div").html(poli_kulit_dan_kelamin_3_debet);
			$("#poli_kulit_dan_kelamin-3-kredit div").html(poli_kulit_dan_kelamin_3_kredit);
			
			// Update Display Alat Kesehatan Poli Kulit dan Kelamin:
			var poli_kulit_dan_kelamin_4_debet = parseFloat($("#poli_kulit_dan_kelamin-4-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var poli_kulit_dan_kelamin_4_kredit = parseFloat($("#poli_kulit_dan_kelamin-4-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			poli_kulit_dan_kelamin_4_debet += json.poli_kulit_dan_kelamin_4_debet;
			poli_kulit_dan_kelamin_4_debet = parseFloat(poli_kulit_dan_kelamin_4_debet).formatMoney("2", ".", ",");
			poli_kulit_dan_kelamin_4_kredit += json.poli_kulit_dan_kelamin_4_kredit;
			poli_kulit_dan_kelamin_4_kredit = parseFloat(poli_kulit_dan_kelamin_4_kredit).formatMoney("2", ".", ",");
			$("#poli_kulit_dan_kelamin-4-debet div").html(poli_kulit_dan_kelamin_4_debet);
			$("#poli_kulit_dan_kelamin-4-kredit div").html(poli_kulit_dan_kelamin_4_kredit);
			
			// Update Display Lain-Lain Poli Kulit dan Kelamin:
			var poli_kulit_dan_kelamin_5_debet = parseFloat($("#poli_kulit_dan_kelamin-5-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var poli_kulit_dan_kelamin_5_kredit = parseFloat($("#poli_kulit_dan_kelamin-5-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			poli_kulit_dan_kelamin_5_debet += json.poli_kulit_dan_kelamin_5_debet;
			poli_kulit_dan_kelamin_5_debet = parseFloat(poli_kulit_dan_kelamin_5_debet).formatMoney("2", ".", ",");
			poli_kulit_dan_kelamin_5_kredit += json.poli_kulit_dan_kelamin_5_kredit;
			poli_kulit_dan_kelamin_5_kredit = parseFloat(poli_kulit_dan_kelamin_5_kredit).formatMoney("2", ".", ",");
			$("#poli_kulit_dan_kelamin-5-debet div").html(poli_kulit_dan_kelamin_5_debet);
			$("#poli_kulit_dan_kelamin-5-kredit div").html(poli_kulit_dan_kelamin_5_kredit);
			
			// Update Subtotal Poli Syaraf:
			// var poli_syaraf_0_debet = parseFloat($("#poli_syaraf-0-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			// var poli_syaraf_0_kredit = parseFloat($("#poli_syaraf-0-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			// poli_syaraf_0_debet += json.poli_syaraf_0_debet;
			// poli_syaraf_0_debet = parseFloat(poli_syaraf_0_debet).formatMoney("2", ".", ",");
			// poli_syaraf_0_kredit += json.poli_syaraf_0_kredit;
			// poli_syaraf_0_kredit = parseFloat(poli_syaraf_0_kredit).formatMoney("2", ".", ",");
			// $("#poli_syaraf-0-debet div").html(poli_syaraf_0_debet);
			// $("#poli_syaraf-0-kredit div").html(poli_syaraf_0_kredit);
			
			// Update Display Sewa Kamar Poli Syaraf:
			var poli_syaraf_1_debet = parseFloat($("#poli_syaraf-1-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var poli_syaraf_1_kredit = parseFloat($("#poli_syaraf-1-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			poli_syaraf_1_debet += json.poli_syaraf_1_debet;
			poli_syaraf_1_debet = parseFloat(poli_syaraf_1_debet).formatMoney("2", ".", ",");
			poli_syaraf_1_kredit += json.poli_syaraf_1_kredit;
			poli_syaraf_1_kredit = parseFloat(poli_syaraf_1_kredit).formatMoney("2", ".", ",");
			$("#poli_syaraf-1-debet div").html(poli_syaraf_1_debet);
			$("#poli_syaraf-1-kredit div").html(poli_syaraf_1_kredit);
			
			// Update Display Tindakan / Jasa Dokter Poli Syaraf:
			var poli_syaraf_2_debet = parseFloat($("#poli_syaraf-2-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var poli_syaraf_2_kredit = parseFloat($("#poli_syaraf-2-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			poli_syaraf_2_debet += json.poli_syaraf_2_debet;
			poli_syaraf_2_debet = parseFloat(poli_syaraf_2_debet).formatMoney("2", ".", ",");
			poli_syaraf_2_kredit += json.poli_syaraf_2_kredit;
			poli_syaraf_2_kredit = parseFloat(poli_syaraf_2_kredit).formatMoney("2", ".", ",");
			$("#poli_syaraf-2-debet div").html(poli_syaraf_2_debet);
			$("#poli_syaraf-2-kredit div").html(poli_syaraf_2_kredit);
			
			// Update Display Jasa / Tindakan Keperawatan Poli Syaraf:
			var poli_syaraf_3_debet = parseFloat($("#poli_syaraf-3-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var poli_syaraf_3_kredit = parseFloat($("#poli_syaraf-3-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			poli_syaraf_3_debet += json.poli_syaraf_3_debet;
			poli_syaraf_3_debet = parseFloat(poli_syaraf_3_debet).formatMoney("2", ".", ",");
			poli_syaraf_3_kredit += json.poli_syaraf_3_kredit;
			poli_syaraf_3_kredit = parseFloat(poli_syaraf_3_kredit).formatMoney("2", ".", ",");
			$("#poli_syaraf-3-debet div").html(poli_syaraf_3_debet);
			$("#poli_syaraf-3-kredit div").html(poli_syaraf_3_kredit);
			
			// Update Display Alat Kesehatan Poli Syaraf:
			var poli_syaraf_4_debet = parseFloat($("#poli_syaraf-4-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var poli_syaraf_4_kredit = parseFloat($("#poli_syaraf-4-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			poli_syaraf_4_debet += json.poli_syaraf_4_debet;
			poli_syaraf_4_debet = parseFloat(poli_syaraf_4_debet).formatMoney("2", ".", ",");
			poli_syaraf_4_kredit += json.poli_syaraf_4_kredit;
			poli_syaraf_4_kredit = parseFloat(poli_syaraf_4_kredit).formatMoney("2", ".", ",");
			$("#poli_syaraf-4-debet div").html(poli_syaraf_4_debet);
			$("#poli_syaraf-4-kredit div").html(poli_syaraf_4_kredit);
			
			// Update Display Lain-Lain Poli Syaraf:
			var poli_syaraf_5_debet = parseFloat($("#poli_syaraf-5-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var poli_syaraf_5_kredit = parseFloat($("#poli_syaraf-5-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			poli_syaraf_5_debet += json.poli_syaraf_5_debet;
			poli_syaraf_5_debet = parseFloat(poli_syaraf_5_debet).formatMoney("2", ".", ",");
			poli_syaraf_5_kredit += json.poli_syaraf_5_kredit;
			poli_syaraf_5_kredit = parseFloat(poli_syaraf_5_kredit).formatMoney("2", ".", ",");
			$("#poli_syaraf-5-debet div").html(poli_syaraf_5_debet);
			$("#poli_syaraf-5-kredit div").html(poli_syaraf_5_kredit);
			
			// Update Subtotal Poli Ortopedi:
			// var poli_ortopedi_0_debet = parseFloat($("#poli_ortopedi-0-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			// var poli_ortopedi_0_kredit = parseFloat($("#poli_ortopedi-0-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			// poli_ortopedi_0_debet += json.poli_ortopedi_0_debet;
			// poli_ortopedi_0_debet = parseFloat(poli_ortopedi_0_debet).formatMoney("2", ".", ",");
			// poli_ortopedi_0_kredit += json.poli_ortopedi_0_kredit;
			// poli_ortopedi_0_kredit = parseFloat(poli_ortopedi_0_kredit).formatMoney("2", ".", ",");
			// $("#poli_ortopedi-0-debet div").html(poli_ortopedi_0_debet);
			// $("#poli_ortopedi-0-kredit div").html(poli_ortopedi_0_kredit);
			
			// Update Display Sewa Kamar Poli Ortopedi:
			var poli_ortopedi_1_debet = parseFloat($("#poli_ortopedi-1-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var poli_ortopedi_1_kredit = parseFloat($("#poli_ortopedi-1-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			poli_ortopedi_1_debet += json.poli_ortopedi_1_debet;
			poli_ortopedi_1_debet = parseFloat(poli_ortopedi_1_debet).formatMoney("2", ".", ",");
			poli_ortopedi_1_kredit += json.poli_ortopedi_1_kredit;
			poli_ortopedi_1_kredit = parseFloat(poli_ortopedi_1_kredit).formatMoney("2", ".", ",");
			$("#poli_ortopedi-1-debet div").html(poli_ortopedi_1_debet);
			$("#poli_ortopedi-1-kredit div").html(poli_ortopedi_1_kredit);
			
			// Update Display Tindakan / Jasa Dokter Poli Ortopedi:
			var poli_ortopedi_2_debet = parseFloat($("#poli_ortopedi-2-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var poli_ortopedi_2_kredit = parseFloat($("#poli_ortopedi-2-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			poli_ortopedi_2_debet += json.poli_ortopedi_2_debet;
			poli_ortopedi_2_debet = parseFloat(poli_ortopedi_2_debet).formatMoney("2", ".", ",");
			poli_ortopedi_2_kredit += json.poli_ortopedi_2_kredit;
			poli_ortopedi_2_kredit = parseFloat(poli_ortopedi_2_kredit).formatMoney("2", ".", ",");
			$("#poli_ortopedi-2-debet div").html(poli_ortopedi_2_debet);
			$("#poli_ortopedi-2-kredit div").html(poli_ortopedi_2_kredit);
			
			// Update Display Jasa / Tindakan Keperawatan Poli Ortopedi:
			var poli_ortopedi_3_debet = parseFloat($("#poli_ortopedi-3-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var poli_ortopedi_3_kredit = parseFloat($("#poli_ortopedi-3-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			poli_ortopedi_3_debet += json.poli_ortopedi_3_debet;
			poli_ortopedi_3_debet = parseFloat(poli_ortopedi_3_debet).formatMoney("2", ".", ",");
			poli_ortopedi_3_kredit += json.poli_ortopedi_3_kredit;
			poli_ortopedi_3_kredit = parseFloat(poli_ortopedi_3_kredit).formatMoney("2", ".", ",");
			$("#poli_ortopedi-3-debet div").html(poli_ortopedi_3_debet);
			$("#poli_ortopedi-3-kredit div").html(poli_ortopedi_3_kredit);
			
			// Update Display Alat Kesehatan Poli Ortopedi:
			var poli_ortopedi_4_debet = parseFloat($("#poli_ortopedi-4-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var poli_ortopedi_4_kredit = parseFloat($("#poli_ortopedi-4-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			poli_ortopedi_4_debet += json.poli_ortopedi_4_debet;
			poli_ortopedi_4_debet = parseFloat(poli_ortopedi_4_debet).formatMoney("2", ".", ",");
			poli_ortopedi_4_kredit += json.poli_ortopedi_4_kredit;
			poli_ortopedi_4_kredit = parseFloat(poli_ortopedi_4_kredit).formatMoney("2", ".", ",");
			$("#poli_ortopedi-4-debet div").html(poli_ortopedi_4_debet);
			$("#poli_ortopedi-4-kredit div").html(poli_ortopedi_4_kredit);
			
			// Update Display Lain-Lain Poli Ortopedi:
			var poli_ortopedi_5_debet = parseFloat($("#poli_ortopedi-5-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var poli_ortopedi_5_kredit = parseFloat($("#poli_ortopedi-5-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			poli_ortopedi_5_debet += json.poli_ortopedi_5_debet;
			poli_ortopedi_5_debet = parseFloat(poli_ortopedi_5_debet).formatMoney("2", ".", ",");
			poli_ortopedi_5_kredit += json.poli_ortopedi_5_kredit;
			poli_ortopedi_5_kredit = parseFloat(poli_ortopedi_5_kredit).formatMoney("2", ".", ",");
			$("#poli_ortopedi-5-debet div").html(poli_ortopedi_5_debet);
			$("#poli_ortopedi-5-kredit div").html(poli_ortopedi_5_kredit);
			
			// Update Subtotal Poli Jantung:
			// var poli_jantung_0_debet = parseFloat($("#poli_jantung-0-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			// var poli_jantung_0_kredit = parseFloat($("#poli_jantung-0-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			// poli_jantung_0_debet += json.poli_jantung_0_debet;
			// poli_jantung_0_debet = parseFloat(poli_jantung_0_debet).formatMoney("2", ".", ",");
			// poli_jantung_0_kredit += json.poli_jantung_0_kredit;
			// poli_jantung_0_kredit = parseFloat(poli_jantung_0_kredit).formatMoney("2", ".", ",");
			// $("#poli_jantung-0-debet div").html(poli_jantung_0_debet);
			// $("#poli_jantung-0-kredit div").html(poli_jantung_0_kredit);
			
			// Update Display Sewa Kamar Poli Jantung:
			var poli_jantung_1_debet = parseFloat($("#poli_jantung-1-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var poli_jantung_1_kredit = parseFloat($("#poli_jantung-1-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			poli_jantung_1_debet += json.poli_jantung_1_debet;
			poli_jantung_1_debet = parseFloat(poli_jantung_1_debet).formatMoney("2", ".", ",");
			poli_jantung_1_kredit += json.poli_jantung_1_kredit;
			poli_jantung_1_kredit = parseFloat(poli_jantung_1_kredit).formatMoney("2", ".", ",");
			$("#poli_jantung-1-debet div").html(poli_jantung_1_debet);
			$("#poli_jantung-1-kredit div").html(poli_jantung_1_kredit);
			
			// Update Display Tindakan / Jasa Dokter Poli Jantung:
			var poli_jantung_2_debet = parseFloat($("#poli_jantung-2-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var poli_jantung_2_kredit = parseFloat($("#poli_jantung-2-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			poli_jantung_2_debet += json.poli_jantung_2_debet;
			poli_jantung_2_debet = parseFloat(poli_jantung_2_debet).formatMoney("2", ".", ",");
			poli_jantung_2_kredit += json.poli_jantung_2_kredit;
			poli_jantung_2_kredit = parseFloat(poli_jantung_2_kredit).formatMoney("2", ".", ",");
			$("#poli_jantung-2-debet div").html(poli_jantung_2_debet);
			$("#poli_jantung-2-kredit div").html(poli_jantung_2_kredit);
			
			// Update Display Jasa / Tindakan Keperawatan Poli Jantung:
			var poli_jantung_3_debet = parseFloat($("#poli_jantung-3-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var poli_jantung_3_kredit = parseFloat($("#poli_jantung-3-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			poli_jantung_3_debet += json.poli_jantung_3_debet;
			poli_jantung_3_debet = parseFloat(poli_jantung_3_debet).formatMoney("2", ".", ",");
			poli_jantung_3_kredit += json.poli_jantung_3_kredit;
			poli_jantung_3_kredit = parseFloat(poli_jantung_3_kredit).formatMoney("2", ".", ",");
			$("#poli_jantung-3-debet div").html(poli_jantung_3_debet);
			$("#poli_jantung-3-kredit div").html(poli_jantung_3_kredit);
			
			// Update Display Alat Kesehatan Poli Jantung:
			var poli_jantung_4_debet = parseFloat($("#poli_jantung-4-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var poli_jantung_4_kredit = parseFloat($("#poli_jantung-4-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			poli_jantung_4_debet += json.poli_jantung_4_debet;
			poli_jantung_4_debet = parseFloat(poli_jantung_4_debet).formatMoney("2", ".", ",");
			poli_jantung_4_kredit += json.poli_jantung_4_kredit;
			poli_jantung_4_kredit = parseFloat(poli_jantung_4_kredit).formatMoney("2", ".", ",");
			$("#poli_jantung-4-debet div").html(poli_jantung_4_debet);
			$("#poli_jantung-4-kredit div").html(poli_jantung_4_kredit);
			
			// Update Display Lain-Lain Poli Jantung:
			// var poli_jantung_5_debet = parseFloat($("#poli_jantung-5-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			// var poli_jantung_5_kredit = parseFloat($("#poli_jantung-5-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			// poli_jantung_5_debet += json.poli_jantung_5_debet;
			// poli_jantung_5_debet = parseFloat(poli_jantung_5_debet).formatMoney("2", ".", ",");
			// poli_jantung_5_kredit += json.poli_jantung_5_kredit;
			// poli_jantung_5_kredit = parseFloat(poli_jantung_5_kredit).formatMoney("2", ".", ",");
			// $("#poli_jantung-5-debet div").html(poli_jantung_5_debet);
			// $("#poli_jantung-5-kredit div").html(poli_jantung_5_kredit);
			
			// Update Subtotal Poli Paru:
			var poli_paru_0_debet = parseFloat($("#poli_paru-0-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var poli_paru_0_kredit = parseFloat($("#poli_paru-0-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			poli_paru_0_debet += json.poli_paru_0_debet;
			poli_paru_0_debet = parseFloat(poli_paru_0_debet).formatMoney("2", ".", ",");
			poli_paru_0_kredit += json.poli_paru_0_kredit;
			poli_paru_0_kredit = parseFloat(poli_paru_0_kredit).formatMoney("2", ".", ",");
			$("#poli_paru-0-debet div").html(poli_paru_0_debet);
			$("#poli_paru-0-kredit div").html(poli_paru_0_kredit);
			
			// Update Display Sewa Kamar Poli Paru:
			var poli_paru_1_debet = parseFloat($("#poli_paru-1-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var poli_paru_1_kredit = parseFloat($("#poli_paru-1-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			poli_paru_1_debet += json.poli_paru_1_debet;
			poli_paru_1_debet = parseFloat(poli_paru_1_debet).formatMoney("2", ".", ",");
			poli_paru_1_kredit += json.poli_paru_1_kredit;
			poli_paru_1_kredit = parseFloat(poli_paru_1_kredit).formatMoney("2", ".", ",");
			$("#poli_paru-1-debet div").html(poli_paru_1_debet);
			$("#poli_paru-1-kredit div").html(poli_paru_1_kredit);
			
			// Update Display Tindakan / Jasa Dokter Poli Paru:
			var poli_paru_2_debet = parseFloat($("#poli_paru-2-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var poli_paru_2_kredit = parseFloat($("#poli_paru-2-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			poli_paru_2_debet += json.poli_paru_2_debet;
			poli_paru_2_debet = parseFloat(poli_paru_2_debet).formatMoney("2", ".", ",");
			poli_paru_2_kredit += json.poli_paru_2_kredit;
			poli_paru_2_kredit = parseFloat(poli_paru_2_kredit).formatMoney("2", ".", ",");
			$("#poli_paru-2-debet div").html(poli_paru_2_debet);
			$("#poli_paru-2-kredit div").html(poli_paru_2_kredit);
			
			// Update Display Jasa / Tindakan Keperawatan Poli Paru:
			var poli_paru_3_debet = parseFloat($("#poli_paru-3-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var poli_paru_3_kredit = parseFloat($("#poli_paru-3-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			poli_paru_3_debet += json.poli_paru_3_debet;
			poli_paru_3_debet = parseFloat(poli_paru_3_debet).formatMoney("2", ".", ",");
			poli_paru_3_kredit += json.poli_paru_3_kredit;
			poli_paru_3_kredit = parseFloat(poli_paru_3_kredit).formatMoney("2", ".", ",");
			$("#poli_paru-3-debet div").html(poli_paru_3_debet);
			$("#poli_paru-3-kredit div").html(poli_paru_3_kredit);
			
			// Update Display Alat Kesehatan Poli Paru:
			var poli_paru_4_debet = parseFloat($("#poli_paru-4-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var poli_paru_4_kredit = parseFloat($("#poli_paru-4-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			poli_paru_4_debet += json.poli_paru_4_debet;
			poli_paru_4_debet = parseFloat(poli_paru_4_debet).formatMoney("2", ".", ",");
			poli_paru_4_kredit += json.poli_paru_4_kredit;
			poli_paru_4_kredit = parseFloat(poli_paru_4_kredit).formatMoney("2", ".", ",");
			$("#poli_paru-4-debet div").html(poli_paru_4_debet);
			$("#poli_paru-4-kredit div").html(poli_paru_4_kredit);
			
			// Update Display Lain-Lain Poli Paru:
			var poli_paru_5_debet = parseFloat($("#poli_paru-5-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var poli_paru_5_kredit = parseFloat($("#poli_paru-5-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			poli_paru_5_debet += json.poli_paru_5_debet;
			poli_paru_5_debet = parseFloat(poli_paru_5_debet).formatMoney("2", ".", ",");
			poli_paru_5_kredit += json.poli_paru_5_kredit;
			poli_paru_5_kredit = parseFloat(poli_paru_5_kredit).formatMoney("2", ".", ",");
			$("#poli_paru-5-debet div").html(poli_paru_5_debet);
			$("#poli_paru-5-kredit div").html(poli_paru_5_kredit);
			
			// Update Subtotal Poli Penyakit Dalam:
			// var poli_penyakit_dalam_0_debet = parseFloat($("#poli_penyakit_dalam-0-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			// var poli_penyakit_dalam_0_kredit = parseFloat($("#poli_penyakit_dalam-0-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			// poli_penyakit_dalam_0_debet += json.poli_penyakit_dalam_0_debet;
			// poli_penyakit_dalam_0_debet = parseFloat(poli_penyakit_dalam_0_debet).formatMoney("2", ".", ",");
			// poli_penyakit_dalam_0_kredit += json.poli_penyakit_dalam_0_kredit;
			// poli_penyakit_dalam_0_kredit = parseFloat(poli_penyakit_dalam_0_kredit).formatMoney("2", ".", ",");
			// $("#poli_penyakit_dalam-0-debet div").html(poli_penyakit_dalam_0_debet);
			// $("#poli_penyakit_dalam-0-kredit div").html(poli_penyakit_dalam_0_kredit);
			
			// Update Display Sewa Kamar Poli Penyakit Dalam:
			var poli_penyakit_dalam_1_debet = parseFloat($("#poli_penyakit_dalam-1-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var poli_penyakit_dalam_1_kredit = parseFloat($("#poli_penyakit_dalam-1-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			poli_penyakit_dalam_1_debet += json.poli_penyakit_dalam_1_debet;
			poli_penyakit_dalam_1_debet = parseFloat(poli_penyakit_dalam_1_debet).formatMoney("2", ".", ",");
			poli_penyakit_dalam_1_kredit += json.poli_penyakit_dalam_1_kredit;
			poli_penyakit_dalam_1_kredit = parseFloat(poli_penyakit_dalam_1_kredit).formatMoney("2", ".", ",");
			$("#poli_penyakit_dalam-1-debet div").html(poli_penyakit_dalam_1_debet);
			$("#poli_penyakit_dalam-1-kredit div").html(poli_penyakit_dalam_1_kredit);
			
			// Update Display Tindakan / Jasa Dokter Poli Penyakit Dalam:
			var poli_penyakit_dalam_2_debet = parseFloat($("#poli_penyakit_dalam-2-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var poli_penyakit_dalam_2_kredit = parseFloat($("#poli_penyakit_dalam-2-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			poli_penyakit_dalam_2_debet += json.poli_penyakit_dalam_2_debet;
			poli_penyakit_dalam_2_debet = parseFloat(poli_penyakit_dalam_2_debet).formatMoney("2", ".", ",");
			poli_penyakit_dalam_2_kredit += json.poli_penyakit_dalam_2_kredit;
			poli_penyakit_dalam_2_kredit = parseFloat(poli_penyakit_dalam_2_kredit).formatMoney("2", ".", ",");
			$("#poli_penyakit_dalam-2-debet div").html(poli_penyakit_dalam_2_debet);
			$("#poli_penyakit_dalam-2-kredit div").html(poli_penyakit_dalam_2_kredit);
			
			// Update Display Jasa / Tindakan Keperawatan Poli Penyakit Dalam:
			var poli_penyakit_dalam_3_debet = parseFloat($("#poli_penyakit_dalam-3-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var poli_penyakit_dalam_3_kredit = parseFloat($("#poli_penyakit_dalam-3-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			poli_penyakit_dalam_3_debet += json.poli_penyakit_dalam_3_debet;
			poli_penyakit_dalam_3_debet = parseFloat(poli_penyakit_dalam_3_debet).formatMoney("2", ".", ",");
			poli_penyakit_dalam_3_kredit += json.poli_penyakit_dalam_3_kredit;
			poli_penyakit_dalam_3_kredit = parseFloat(poli_penyakit_dalam_3_kredit).formatMoney("2", ".", ",");
			$("#poli_penyakit_dalam-3-debet div").html(poli_penyakit_dalam_3_debet);
			$("#poli_penyakit_dalam-3-kredit div").html(poli_penyakit_dalam_3_kredit);
			
			// Update Display Alat Kesehatan Poli Penyakit Dalam:
			var poli_penyakit_dalam_4_debet = parseFloat($("#poli_penyakit_dalam-4-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var poli_penyakit_dalam_4_kredit = parseFloat($("#poli_penyakit_dalam-4-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			poli_penyakit_dalam_4_debet += json.poli_penyakit_dalam_4_debet;
			poli_penyakit_dalam_4_debet = parseFloat(poli_penyakit_dalam_4_debet).formatMoney("2", ".", ",");
			poli_penyakit_dalam_4_kredit += json.poli_penyakit_dalam_4_kredit;
			poli_penyakit_dalam_4_kredit = parseFloat(poli_penyakit_dalam_4_kredit).formatMoney("2", ".", ",");
			$("#poli_penyakit_dalam-4-debet div").html(poli_penyakit_dalam_4_debet);
			$("#poli_penyakit_dalam-4-kredit div").html(poli_penyakit_dalam_4_kredit);
			
			// Update Display Lain-Lain Poli Penyakit Dalam:
			var poli_penyakit_dalam_5_debet = parseFloat($("#poli_penyakit_dalam-5-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var poli_penyakit_dalam_5_kredit = parseFloat($("#poli_penyakit_dalam-5-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			poli_penyakit_dalam_5_debet += json.poli_penyakit_dalam_5_debet;
			poli_penyakit_dalam_5_debet = parseFloat(poli_penyakit_dalam_5_debet).formatMoney("2", ".", ",");
			poli_penyakit_dalam_5_kredit += json.poli_penyakit_dalam_5_kredit;
			poli_penyakit_dalam_5_kredit = parseFloat(poli_penyakit_dalam_5_kredit).formatMoney("2", ".", ",");
			$("#poli_penyakit_dalam-5-debet div").html(poli_penyakit_dalam_5_debet);
			$("#poli_penyakit_dalam-5-kredit div").html(poli_penyakit_dalam_5_kredit);
			
			// Update Subtotal Gizi:
			// var gizi_0_debet = parseFloat($("#gizi-0-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			// var gizi_0_kredit = parseFloat($("#gizi-0-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			// gizi_0_debet += json.gizi_0_debet;
			// gizi_0_debet = parseFloat(gizi_0_debet).formatMoney("2", ".", ",");
			// gizi_0_kredit += json.gizi_0_kredit;
			// gizi_0_kredit = parseFloat(gizi_0_kredit).formatMoney("2", ".", ",");
			// $("#gizi-0-debet div").html(gizi_0_debet);
			// $("#gizi-0-kredit div").html(gizi_0_kredit);
			
			// Update Display Sewa Kamar Gizi:
			var gizi_1_debet = parseFloat($("#gizi-1-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var gizi_1_kredit = parseFloat($("#gizi-1-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			gizi_1_debet += json.gizi_1_debet;
			gizi_1_debet = parseFloat(gizi_1_debet).formatMoney("2", ".", ",");
			gizi_1_kredit += json.gizi_1_kredit;
			gizi_1_kredit = parseFloat(gizi_1_kredit).formatMoney("2", ".", ",");
			$("#gizi-1-debet div").html(gizi_1_debet);
			$("#gizi-1-kredit div").html(gizi_1_kredit);
			
			// Update Display Tindakan / Jasa Dokter Gizi:
			var gizi_2_debet = parseFloat($("#gizi-2-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var gizi_2_kredit = parseFloat($("#gizi-2-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			gizi_2_debet += json.gizi_2_debet;
			gizi_2_debet = parseFloat(gizi_2_debet).formatMoney("2", ".", ",");
			gizi_2_kredit += json.gizi_2_kredit;
			gizi_2_kredit = parseFloat(gizi_2_kredit).formatMoney("2", ".", ",");
			$("#gizi-2-debet div").html(gizi_2_debet);
			$("#gizi-2-kredit div").html(gizi_2_kredit);
			
			// Update Display Jasa / Tindakan Keperawatan Gizi:
			var gizi_3_debet = parseFloat($("#gizi-3-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var gizi_3_kredit = parseFloat($("#gizi-3-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			gizi_3_debet += json.gizi_3_debet;
			gizi_3_debet = parseFloat(gizi_3_debet).formatMoney("2", ".", ",");
			gizi_3_kredit += json.gizi_3_kredit;
			gizi_3_kredit = parseFloat(gizi_3_kredit).formatMoney("2", ".", ",");
			$("#gizi-3-debet div").html(gizi_3_debet);
			$("#gizi-3-kredit div").html(gizi_3_kredit);
			
			// Update Display Alat Kesehatan Gizi:
			var gizi_4_debet = parseFloat($("#gizi-4-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var gizi_4_kredit = parseFloat($("#gizi-4-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			gizi_4_debet += json.gizi_4_debet;
			gizi_4_debet = parseFloat(gizi_4_debet).formatMoney("2", ".", ",");
			gizi_4_kredit += json.gizi_4_kredit;
			gizi_4_kredit = parseFloat(gizi_4_kredit).formatMoney("2", ".", ",");
			$("#gizi-4-debet div").html(gizi_4_debet);
			$("#gizi-4-kredit div").html(gizi_4_kredit);
			
			// Update Display Lain-Lain Gizi:
			var gizi_5_debet = parseFloat($("#gizi-5-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var gizi_5_kredit = parseFloat($("#gizi-5-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			gizi_5_debet += json.gizi_5_debet;
			gizi_5_debet = parseFloat(gizi_5_debet).formatMoney("2", ".", ",");
			gizi_5_kredit += json.gizi_5_kredit;
			gizi_5_kredit = parseFloat(gizi_5_kredit).formatMoney("2", ".", ",");
			$("#gizi-5-debet div").html(gizi_5_debet);
			$("#gizi-5-kredit div").html(gizi_5_kredit);
			
			// Update Subtotal Hemodialisa:
			// var hemodialisa_0_debet = parseFloat($("#hemodialisa-0-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			// var hemodialisa_0_kredit = parseFloat($("#hemodialisa-0-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			// hemodialisa_0_debet += json.hemodialisa_0_debet;
			// hemodialisa_0_debet = parseFloat(hemodialisa_0_debet).formatMoney("2", ".", ",");
			// hemodialisa_0_kredit += json.hemodialisa_0_kredit;
			// hemodialisa_0_kredit = parseFloat(hemodialisa_0_kredit).formatMoney("2", ".", ",");
			// $("#hemodialisa-0-debet div").html(hemodialisa_0_debet);
			// $("#hemodialisa-0-kredit div").html(hemodialisa_0_kredit);
			
			// Update Display Sewa Kamar Hemodialisa:
			var hemodialisa_1_debet = parseFloat($("#hemodialisa-1-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var hemodialisa_1_kredit = parseFloat($("#hemodialisa-1-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			hemodialisa_1_debet += json.hemodialisa_1_debet;
			hemodialisa_1_debet = parseFloat(hemodialisa_1_debet).formatMoney("2", ".", ",");
			hemodialisa_1_kredit += json.hemodialisa_1_kredit;
			hemodialisa_1_kredit = parseFloat(hemodialisa_1_kredit).formatMoney("2", ".", ",");
			$("#hemodialisa-1-debet div").html(hemodialisa_1_debet);
			$("#hemodialisa-1-kredit div").html(hemodialisa_1_kredit);
			
			// Update Display Tindakan / Jasa Dokter Hemodialisa:
			var hemodialisa_2_debet = parseFloat($("#hemodialisa-2-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var hemodialisa_2_kredit = parseFloat($("#hemodialisa-2-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			hemodialisa_2_debet += json.hemodialisa_2_debet;
			hemodialisa_2_debet = parseFloat(hemodialisa_2_debet).formatMoney("2", ".", ",");
			hemodialisa_2_kredit += json.hemodialisa_2_kredit;
			hemodialisa_2_kredit = parseFloat(hemodialisa_2_kredit).formatMoney("2", ".", ",");
			$("#hemodialisa-2-debet div").html(hemodialisa_2_debet);
			$("#hemodialisa-2-kredit div").html(hemodialisa_2_kredit);
			
			// Update Display Jasa / Tindakan Keperawatan Hemodialisa:
			var hemodialisa_3_debet = parseFloat($("#hemodialisa-3-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var hemodialisa_3_kredit = parseFloat($("#hemodialisa-3-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			hemodialisa_3_debet += json.hemodialisa_3_debet;
			hemodialisa_3_debet = parseFloat(hemodialisa_3_debet).formatMoney("2", ".", ",");
			hemodialisa_3_kredit += json.hemodialisa_3_kredit;
			hemodialisa_3_kredit = parseFloat(hemodialisa_3_kredit).formatMoney("2", ".", ",");
			$("#hemodialisa-3-debet div").html(hemodialisa_3_debet);
			$("#hemodialisa-3-kredit div").html(hemodialisa_3_kredit);
			
			// Update Display Alat Kesehatan Hemodialisa:
			var hemodialisa_4_debet = parseFloat($("#hemodialisa-4-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var hemodialisa_4_kredit = parseFloat($("#hemodialisa-4-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			hemodialisa_4_debet += json.hemodialisa_4_debet;
			hemodialisa_4_debet = parseFloat(hemodialisa_4_debet).formatMoney("2", ".", ",");
			hemodialisa_4_kredit += json.hemodialisa_4_kredit;
			hemodialisa_4_kredit = parseFloat(hemodialisa_4_kredit).formatMoney("2", ".", ",");
			$("#hemodialisa-4-debet div").html(hemodialisa_4_debet);
			$("#hemodialisa-4-kredit div").html(hemodialisa_4_kredit);
			
			// Update Display Lain-Lain Hemodialisa:
			var hemodialisa_5_debet = parseFloat($("#hemodialisa-5-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var hemodialisa_5_kredit = parseFloat($("#hemodialisa-5-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			hemodialisa_5_debet += json.hemodialisa_5_debet;
			hemodialisa_5_debet = parseFloat(hemodialisa_5_debet).formatMoney("2", ".", ",");
			hemodialisa_5_kredit += json.hemodialisa_5_kredit;
			hemodialisa_5_kredit = parseFloat(hemodialisa_5_kredit).formatMoney("2", ".", ",");
			$("#hemodialisa-5-debet div").html(hemodialisa_5_debet);
			$("#hemodialisa-5-kredit div").html(hemodialisa_5_kredit);
			
			// Update Subtotal Fisioterapi:
			// var fisiotherapy_0_debet = parseFloat($("#fisiotherapy-0-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			// var fisiotherapy_0_kredit = parseFloat($("#fisiotherapy-0-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			// fisiotherapy_0_debet += json.fisiotherapy_0_debet;
			// fisiotherapy_0_debet = parseFloat(fisiotherapy_0_debet).formatMoney("2", ".", ",");
			// fisiotherapy_0_kredit += json.fisiotherapy_0_kredit;
			// fisiotherapy_0_kredit = parseFloat(fisiotherapy_0_kredit).formatMoney("2", ".", ",");
			// $("#fisiotherapy-0-debet div").html(fisiotherapy_0_debet);
			// $("#fisiotherapy-0-kredit div").html(fisiotherapy_0_kredit);
			
			// Update Display Sewa Kamar Fisioterapi:
			var fisiotherapy_1_debet = parseFloat($("#fisiotherapy-1-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var fisiotherapy_1_kredit = parseFloat($("#fisiotherapy-1-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			fisiotherapy_1_debet += json.fisiotherapy_1_debet;
			fisiotherapy_1_debet = parseFloat(fisiotherapy_1_debet).formatMoney("2", ".", ",");
			fisiotherapy_1_kredit += json.fisiotherapy_1_kredit;
			fisiotherapy_1_kredit = parseFloat(fisiotherapy_1_kredit).formatMoney("2", ".", ",");
			$("#fisiotherapy-1-debet div").html(fisiotherapy_1_debet);
			$("#fisiotherapy-1-kredit div").html(fisiotherapy_1_kredit);
			
			// Update Display Tindakan / Jasa Dokter Fisioterapi:
			var fisiotherapy_2_debet = parseFloat($("#fisiotherapy-2-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var fisiotherapy_2_kredit = parseFloat($("#fisiotherapy-2-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			fisiotherapy_2_debet += json.fisiotherapy_2_debet;
			fisiotherapy_2_debet = parseFloat(fisiotherapy_2_debet).formatMoney("2", ".", ",");
			fisiotherapy_2_kredit += json.fisiotherapy_2_kredit;
			fisiotherapy_2_kredit = parseFloat(fisiotherapy_2_kredit).formatMoney("2", ".", ",");
			$("#fisiotherapy-2-debet div").html(fisiotherapy_2_debet);
			$("#fisiotherapy-2-kredit div").html(fisiotherapy_2_kredit);
			
			// Update Display Jasa / Tindakan Keperawatan Fisioterapi:
			var fisiotherapy_3_debet = parseFloat($("#fisiotherapy-3-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var fisiotherapy_3_kredit = parseFloat($("#fisiotherapy-3-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			fisiotherapy_3_debet += json.fisiotherapy_3_debet;
			fisiotherapy_3_debet = parseFloat(fisiotherapy_3_debet).formatMoney("2", ".", ",");
			fisiotherapy_3_kredit += json.fisiotherapy_3_kredit;
			fisiotherapy_3_kredit = parseFloat(fisiotherapy_3_kredit).formatMoney("2", ".", ",");
			$("#fisiotherapy-3-debet div").html(fisiotherapy_3_debet);
			$("#fisiotherapy-3-kredit div").html(fisiotherapy_3_kredit);
			
			// Update Display Alat Kesehatan Fisioterapi:
			var fisiotherapy_4_debet = parseFloat($("#fisiotherapy-4-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var fisiotherapy_4_kredit = parseFloat($("#fisiotherapy-4-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			fisiotherapy_4_debet += json.fisiotherapy_4_debet;
			fisiotherapy_4_debet = parseFloat(fisiotherapy_4_debet).formatMoney("2", ".", ",");
			fisiotherapy_4_kredit += json.fisiotherapy_4_kredit;
			fisiotherapy_4_kredit = parseFloat(fisiotherapy_4_kredit).formatMoney("2", ".", ",");
			$("#fisiotherapy-4-debet div").html(fisiotherapy_4_debet);
			$("#fisiotherapy-4-kredit div").html(fisiotherapy_4_kredit);
			
			// Update Display Lain-Lain Fisioterapi:
			var fisiotherapy_5_debet = parseFloat($("#fisiotherapy-5-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var fisiotherapy_5_kredit = parseFloat($("#fisiotherapy-5-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			fisiotherapy_5_debet += json.fisiotherapy_5_debet;
			fisiotherapy_5_debet = parseFloat(fisiotherapy_5_debet).formatMoney("2", ".", ",");
			fisiotherapy_5_kredit += json.fisiotherapy_5_kredit;
			fisiotherapy_5_kredit = parseFloat(fisiotherapy_5_kredit).formatMoney("2", ".", ",");
			$("#fisiotherapy-5-debet div").html(fisiotherapy_5_debet);
			$("#fisiotherapy-5-kredit div").html(fisiotherapy_5_kredit);
			
			// Update Subtotal Kamar Operasi:
			// var kamar_operasi_0_debet = parseFloat($("#kamar_operasi-0-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			// var kamar_operasi_0_kredit = parseFloat($("#kamar_operasi-0-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			// kamar_operasi_0_debet += json.kamar_operasi_0_debet;
			// kamar_operasi_0_debet = parseFloat(kamar_operasi_0_debet).formatMoney("2", ".", ",");
			// kamar_operasi_0_kredit += json.kamar_operasi_0_kredit;
			// kamar_operasi_0_kredit = parseFloat(kamar_operasi_0_kredit).formatMoney("2", ".", ",");
			// $("#kamar_operasi-0-debet div").html(kamar_operasi_0_debet);
			// $("#kamar_operasi-0-kredit div").html(kamar_operasi_0_kredit);
			
			// Update Display Sewa Kamar Kamar Operasi:
			var kamar_operasi_1_debet = parseFloat($("#kamar_operasi-1-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var kamar_operasi_1_kredit = parseFloat($("#kamar_operasi-1-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			kamar_operasi_1_debet += json.kamar_operasi_1_debet;
			kamar_operasi_1_debet = parseFloat(kamar_operasi_1_debet).formatMoney("2", ".", ",");
			kamar_operasi_1_kredit += json.kamar_operasi_1_kredit;
			kamar_operasi_1_kredit = parseFloat(kamar_operasi_1_kredit).formatMoney("2", ".", ",");
			$("#kamar_operasi-1-debet div").html(kamar_operasi_1_debet);
			$("#kamar_operasi-1-kredit div").html(kamar_operasi_1_kredit);
			
			// Update Display Jasa Operasi Kamar Operasi:
			var kamar_operasi_2_debet = parseFloat($("#kamar_operasi-2-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var kamar_operasi_2_kredit = parseFloat($("#kamar_operasi-2-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			kamar_operasi_2_debet += json.kamar_operasi_2_debet;
			kamar_operasi_2_debet = parseFloat(kamar_operasi_2_debet).formatMoney("2", ".", ",");
			kamar_operasi_2_kredit += json.kamar_operasi_2_kredit;
			kamar_operasi_2_kredit = parseFloat(kamar_operasi_2_kredit).formatMoney("2", ".", ",");
			$("#kamar_operasi-2-debet div").html(kamar_operasi_2_debet);
			$("#kamar_operasi-2-kredit div").html(kamar_operasi_2_kredit);
			
			// Update Display Jasa / Tindakan Keperawatan Kamar Operasi:
			var kamar_operasi_3_debet = parseFloat($("#kamar_operasi-3-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var kamar_operasi_3_kredit = parseFloat($("#kamar_operasi-3-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			kamar_operasi_3_debet += json.kamar_operasi_3_debet;
			kamar_operasi_3_debet = parseFloat(kamar_operasi_3_debet).formatMoney("2", ".", ",");
			kamar_operasi_3_kredit += json.kamar_operasi_3_kredit;
			kamar_operasi_3_kredit = parseFloat(kamar_operasi_3_kredit).formatMoney("2", ".", ",");
			$("#kamar_operasi-3-debet div").html(kamar_operasi_3_debet);
			$("#kamar_operasi-3-kredit div").html(kamar_operasi_3_kredit);
			
			// Update Display Alat Kesehatan Kamar Operasi:
			var kamar_operasi_4_debet = parseFloat($("#kamar_operasi-4-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var kamar_operasi_4_kredit = parseFloat($("#kamar_operasi-4-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			kamar_operasi_4_debet += json.kamar_operasi_4_debet;
			kamar_operasi_4_debet = parseFloat(kamar_operasi_4_debet).formatMoney("2", ".", ",");
			kamar_operasi_4_kredit += json.kamar_operasi_4_kredit;
			kamar_operasi_4_kredit = parseFloat(kamar_operasi_4_kredit).formatMoney("2", ".", ",");
			$("#kamar_operasi-4-debet div").html(kamar_operasi_4_debet);
			$("#kamar_operasi-4-kredit div").html(kamar_operasi_4_kredit);
			
			// Update Display Anasthesi Kamar Operasi:
			var kamar_operasi_5_debet = parseFloat($("#kamar_operasi-5-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var kamar_operasi_5_kredit = parseFloat($("#kamar_operasi-5-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			kamar_operasi_5_debet += json.kamar_operasi_5_debet;
			kamar_operasi_5_debet = parseFloat(kamar_operasi_5_debet).formatMoney("2", ".", ",");
			kamar_operasi_5_kredit += json.kamar_operasi_5_kredit;
			kamar_operasi_5_kredit = parseFloat(kamar_operasi_5_kredit).formatMoney("2", ".", ",");
			$("#kamar_operasi-5-debet div").html(kamar_operasi_5_debet);
			$("#kamar_operasi-5-kredit div").html(kamar_operasi_5_kredit);
			
			// Update Display Lain-Lain Kamar Operasi:
			var kamar_operasi_6_debet = parseFloat($("#kamar_operasi-6-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var kamar_operasi_6_kredit = parseFloat($("#kamar_operasi-6-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			kamar_operasi_6_debet += json.kamar_operasi_6_debet;
			kamar_operasi_6_debet = parseFloat(kamar_operasi_6_debet).formatMoney("2", ".", ",");
			kamar_operasi_6_kredit += json.kamar_operasi_6_kredit;
			kamar_operasi_6_kredit = parseFloat(kamar_operasi_6_kredit).formatMoney("2", ".", ",");
			$("#kamar_operasi-6-debet div").html(kamar_operasi_6_debet);
			$("#kamar_operasi-6-kredit div").html(kamar_operasi_6_kredit);
			
			// Update Subtotal Laboratorium:
			// var laboratory_0_debet = parseFloat($("#laboratory-0-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			// var laboratory_0_kredit = parseFloat($("#laboratory-0-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			// laboratory_0_debet += json.laboratory_0_debet;
			// laboratory_0_debet = parseFloat(laboratory_0_debet).formatMoney("2", ".", ",");
			// laboratory_0_kredit += json.laboratory_0_kredit;
			// laboratory_0_kredit = parseFloat(laboratory_0_kredit).formatMoney("2", ".", ",");
			// $("#laboratory-0-debet div").html(laboratory_0_debet);
			// $("#laboratory-0-kredit div").html(laboratory_0_kredit);
			
			// Update Display Pemeriksaan Rutin Laboratorium:
			var laboratory_1_debet = parseFloat($("#laboratory-1-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var laboratory_1_kredit = parseFloat($("#laboratory-1-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			laboratory_1_debet += json.laboratory_1_debet;
			laboratory_1_debet = parseFloat(laboratory_1_debet).formatMoney("2", ".", ",");
			laboratory_1_kredit += json.laboratory_1_kredit;
			laboratory_1_kredit = parseFloat(laboratory_1_kredit).formatMoney("2", ".", ",");
			$("#laboratory-1-debet div").html(laboratory_1_debet);
			$("#laboratory-1-kredit div").html(laboratory_1_kredit);
			
			// Update Display Pemeriksaan Khusus Laboratorium:
			var laboratory_2_debet = parseFloat($("#laboratory-2-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var laboratory_2_kredit = parseFloat($("#laboratory-2-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			laboratory_2_debet += json.laboratory_2_debet;
			laboratory_2_debet = parseFloat(laboratory_2_debet).formatMoney("2", ".", ",");
			laboratory_2_kredit += json.laboratory_2_kredit;
			laboratory_2_kredit = parseFloat(laboratory_2_kredit).formatMoney("2", ".", ",");
			$("#laboratory-2-debet div").html(laboratory_2_debet);
			$("#laboratory-2-kredit div").html(laboratory_2_kredit);
			
			// Update Display Blood Tapping + Bag (PMI) Laboratorium:
			var laboratory_3_debet = parseFloat($("#laboratory-3-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var laboratory_3_kredit = parseFloat($("#laboratory-3-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			laboratory_3_debet += json.laboratory_3_debet;
			laboratory_3_debet = parseFloat(laboratory_3_debet).formatMoney("2", ".", ",");
			laboratory_3_kredit += json.laboratory_3_kredit;
			laboratory_3_kredit = parseFloat(laboratory_3_kredit).formatMoney("2", ".", ",");
			$("#laboratory-3-debet div").html(laboratory_3_debet);
			$("#laboratory-3-kredit div").html(laboratory_3_kredit);
			
			// Update Subtotal Depo Farmasi:
			// var depo_farmasi_0_debet = parseFloat($("#depo_farmasi-0-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			// var depo_farmasi_0_kredit = parseFloat($("#depo_farmasi-0-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			// depo_farmasi_0_debet += json.depo_farmasi_0_debet;
			// depo_farmasi_0_debet = parseFloat(depo_farmasi_0_debet).formatMoney("2", ".", ",");
			// depo_farmasi_0_kredit += json.depo_farmasi_0_kredit;
			// depo_farmasi_0_kredit = parseFloat(depo_farmasi_0_kredit).formatMoney("2", ".", ",");
			// $("#depo_farmasi-0-debet div").html(depo_farmasi_0_debet);
			// $("#depo_farmasi-0-kredit div").html(depo_farmasi_0_kredit);
			
			// Update Display Penjualan Resep Rawat Inap Depo Farmasi:
			var depo_farmasi_1_debet = parseFloat($("#depo_farmasi-1-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var depo_farmasi_1_kredit = parseFloat($("#depo_farmasi-1-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			depo_farmasi_1_debet += json.depo_farmasi_1_debet;
			depo_farmasi_1_debet = parseFloat(depo_farmasi_1_debet).formatMoney("2", ".", ",");
			depo_farmasi_1_kredit += json.depo_farmasi_1_kredit;
			depo_farmasi_1_kredit = parseFloat(depo_farmasi_1_kredit).formatMoney("2", ".", ",");
			$("#depo_farmasi-1-debet div").html(depo_farmasi_1_debet);
			$("#depo_farmasi-1-kredit div").html(depo_farmasi_1_kredit);
			
			// Update Display Penjualan Resep Rawat Jalan Umum Depo Farmasi:
			var depo_farmasi_2_debet = parseFloat($("#depo_farmasi-2-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var depo_farmasi_2_kredit = parseFloat($("#depo_farmasi-2-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			depo_farmasi_2_debet += json.depo_farmasi_2_debet;
			depo_farmasi_2_debet = parseFloat(depo_farmasi_2_debet).formatMoney("2", ".", ",");
			depo_farmasi_2_kredit += json.depo_farmasi_2_kredit;
			depo_farmasi_2_kredit = parseFloat(depo_farmasi_2_kredit).formatMoney("2", ".", ",");
			$("#depo_farmasi-2-debet div").html(depo_farmasi_2_debet);
			$("#depo_farmasi-2-kredit div").html(depo_farmasi_2_kredit);

			// Update Display Penjualan Resep Rawat Jalan Manfaat Depo Manfaat:
			var depo_farmasi_3_debet = parseFloat($("#depo_farmasi-3-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var depo_farmasi_3_kredit = parseFloat($("#depo_farmasi-3-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			depo_farmasi_3_debet += json.depo_farmasi_3_debet;
			depo_farmasi_3_debet = parseFloat(depo_farmasi_3_debet).formatMoney("2", ".", ",");
			depo_farmasi_3_kredit += json.depo_farmasi_3_kredit;
			depo_farmasi_3_kredit = parseFloat(depo_farmasi_3_kredit).formatMoney("2", ".", ",");
			$("#depo_farmasi-3-debet div").html(depo_farmasi_3_debet);
			$("#depo_farmasi-3-kredit div").html(depo_farmasi_3_kredit);

			// Update Display Penjualan Resep Rawat Jalan Kronis Depo Farmasi:
			var depo_farmasi_4_debet = parseFloat($("#depo_farmasi-4-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var depo_farmasi_4_kredit = parseFloat($("#depo_farmasi-4-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			depo_farmasi_4_debet += json.depo_farmasi_4_debet;
			depo_farmasi_4_debet = parseFloat(depo_farmasi_4_debet).formatMoney("2", ".", ",");
			depo_farmasi_4_kredit += json.depo_farmasi_4_kredit;
			depo_farmasi_4_kredit = parseFloat(depo_farmasi_4_kredit).formatMoney("2", ".", ",");
			$("#depo_farmasi-4-debet div").html(depo_farmasi_4_debet);
			$("#depo_farmasi-4-kredit div").html(depo_farmasi_4_kredit);

			// Update Display Penjualan Resep Rawat Jalan INA-CBG's Depo Farmasi:
			var depo_farmasi_5_debet = parseFloat($("#depo_farmasi-5-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var depo_farmasi_5_kredit = parseFloat($("#depo_farmasi-5-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			depo_farmasi_5_debet += json.depo_farmasi_5_debet;
			depo_farmasi_5_debet = parseFloat(depo_farmasi_5_debet).formatMoney("2", ".", ",");
			depo_farmasi_5_kredit += json.depo_farmasi_5_kredit;
			depo_farmasi_5_kredit = parseFloat(depo_farmasi_5_kredit).formatMoney("2", ".", ",");
			$("#depo_farmasi-5-debet div").html(depo_farmasi_5_debet);
			$("#depo_farmasi-5-kredit div").html(depo_farmasi_5_kredit);

			// Update Display Penjualan Resep Rawat Jalan Tidak Ditanggung Depo Farmasi:
			var depo_farmasi_6_debet = parseFloat($("#depo_farmasi-6-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var depo_farmasi_6_kredit = parseFloat($("#depo_farmasi-6-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			depo_farmasi_6_debet += json.depo_farmasi_6_debet;
			depo_farmasi_6_debet = parseFloat(depo_farmasi_6_debet).formatMoney("2", ".", ",");
			depo_farmasi_6_kredit += json.depo_farmasi_6_kredit;
			depo_farmasi_6_kredit = parseFloat(depo_farmasi_6_kredit).formatMoney("2", ".", ",");
			$("#depo_farmasi-6-debet div").html(depo_farmasi_6_debet);
			$("#depo_farmasi-6-kredit div").html(depo_farmasi_6_kredit);

			// Update Display Jasa Resep Depo Farmasi:
			var depo_farmasi_7_debet = parseFloat($("#depo_farmasi-7-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var depo_farmasi_7_kredit = parseFloat($("#depo_farmasi-7-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			depo_farmasi_7_debet += json.depo_farmasi_7_debet;
			depo_farmasi_7_debet = parseFloat(depo_farmasi_7_debet).formatMoney("2", ".", ",");
			depo_farmasi_7_kredit += json.depo_farmasi_7_kredit;
			depo_farmasi_7_kredit = parseFloat(depo_farmasi_7_kredit).formatMoney("2", ".", ",");
			$("#depo_farmasi-7-debet div").html(depo_farmasi_7_debet);
			$("#depo_farmasi-7-kredit div").html(depo_farmasi_7_kredit);

			// Update Display Penjualan CITO Depo Farmasi:
			var depo_farmasi_8_debet = parseFloat($("#depo_farmasi-8-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var depo_farmasi_8_kredit = parseFloat($("#depo_farmasi-8-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			depo_farmasi_8_debet += json.depo_farmasi_8_debet;
			depo_farmasi_8_debet = parseFloat(depo_farmasi_8_debet).formatMoney("2", ".", ",");
			depo_farmasi_8_kredit += json.depo_farmasi_8_kredit;
			depo_farmasi_8_kredit = parseFloat(depo_farmasi_8_kredit).formatMoney("2", ".", ",");
			$("#depo_farmasi-8-debet div").html(depo_farmasi_8_debet);
			$("#depo_farmasi-8-kredit div").html(depo_farmasi_8_kredit);
			
			// Update Subtotal Radiologi:
			// var radiology_0_debet = parseFloat($("#radiology-0-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			// var radiology_0_kredit = parseFloat($("#radiology-0-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			// radiology_0_debet += json.radiology_0_debet;
			// radiology_0_debet = parseFloat(radiology_0_debet).formatMoney("2", ".", ",");
			// radiology_0_kredit += json.radiology_0_kredit;
			// radiology_0_kredit = parseFloat(radiology_0_kredit).formatMoney("2", ".", ",");
			// $("#radiology-0-debet div").html(radiology_0_debet);
			// $("#radiology-0-kredit div").html(radiology_0_kredit);
			
			// Update Display X-Ray Radiologi:
			var radiology_1_debet = parseFloat($("#radiology-1-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var radiology_1_kredit = parseFloat($("#radiology-1-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			radiology_1_debet += json.radiology_1_debet;
			radiology_1_debet = parseFloat(radiology_1_debet).formatMoney("2", ".", ",");
			radiology_1_kredit += json.radiology_1_kredit;
			radiology_1_kredit = parseFloat(radiology_1_kredit).formatMoney("2", ".", ",");
			$("#radiology-1-debet div").html(radiology_1_debet);
			$("#radiology-1-kredit div").html(radiology_1_kredit);
			
			// Update Display CT-Scan Radiologi:
			var radiology_2_debet = parseFloat($("#radiology-2-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var radiology_2_kredit = parseFloat($("#radiology-2-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			radiology_2_debet += json.radiology_2_debet;
			radiology_2_debet = parseFloat(radiology_2_debet).formatMoney("2", ".", ",");
			radiology_2_kredit += json.radiology_2_kredit;
			radiology_2_kredit = parseFloat(radiology_2_kredit).formatMoney("2", ".", ",");
			$("#radiology-2-debet div").html(radiology_2_debet);
			$("#radiology-2-kredit div").html(radiology_2_kredit);
			
			// Update Display USG Radiologi:
			var radiology_3_debet = parseFloat($("#radiology-3-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var radiology_3_kredit = parseFloat($("#radiology-3-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			radiology_3_debet += json.radiology_3_debet;
			radiology_3_debet = parseFloat(radiology_3_debet).formatMoney("2", ".", ",");
			radiology_3_kredit += json.radiology_3_kredit;
			radiology_3_kredit = parseFloat(radiology_3_kredit).formatMoney("2", ".", ",");
			$("#radiology-3-debet div").html(radiology_3_debet);
			$("#radiology-3-kredit div").html(radiology_3_kredit);

			// Update Display Konsul Radiologi :
			var radiology_4_debet = parseFloat($("#radiology-4-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var radiology_4_kredit = parseFloat($("#radiology-4-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			radiology_4_debet += json.radiology_4_debet;
			radiology_4_debet = parseFloat(radiology_4_debet).formatMoney("2", ".", ",");
			radiology_4_kredit += json.radiology_4_kredit;
			radiology_4_kredit = parseFloat(radiology_4_kredit).formatMoney("2", ".", ",");
			$("#radiology-4-debet div").html(radiology_4_debet);
			$("#radiology-4-kredit div").html(radiology_4_kredit);

			// Update Display Lain-Lain Radiologi :
			var radiology_5_debet = parseFloat($("#radiology-5-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var radiology_5_kredit = parseFloat($("#radiology-5-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			radiology_5_debet += json.radiology_5_debet;
			radiology_5_debet = parseFloat(radiology_5_debet).formatMoney("2", ".", ",");
			radiology_5_kredit += json.radiology_5_kredit;
			radiology_5_kredit = parseFloat(radiology_5_kredit).formatMoney("2", ".", ",");
			$("#radiology-5-debet div").html(radiology_5_debet);
			$("#radiology-5-kredit div").html(radiology_5_kredit);
			
			// Update Subtotal Lain-Lain:
			// var lain_lain_0_debet = parseFloat($("#lain_lain-0-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			// var lain_lain_0_kredit = parseFloat($("#lain_lain-0-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			// lain_lain_0_debet += json.lain_lain_0_debet;
			// lain_lain_0_debet = parseFloat(lain_lain_0_debet).formatMoney("2", ".", ",");
			// lain_lain_0_kredit += json.lain_lain_0_kredit;
			// lain_lain_0_kredit = parseFloat(lain_lain_0_kredit).formatMoney("2", ".", ",");
			// $("#lain_lain-0-debet div").html(lain_lain_0_debet);
			// $("#lain_lain-0-kredit div").html(lain_lain_0_kredit);
			
			// Update Display Ambulan di Lain-Lain:
			var lain_lain_1_debet = parseFloat($("#lain_lain-1-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var lain_lain_1_kredit = parseFloat($("#lain_lain-1-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			lain_lain_1_debet += json.lain_lain_1_debet;
			lain_lain_1_debet = parseFloat(lain_lain_1_debet).formatMoney("2", ".", ",");
			lain_lain_1_kredit += json.lain_lain_1_kredit;
			lain_lain_1_kredit = parseFloat(lain_lain_1_kredit).formatMoney("2", ".", ",");
			$("#lain_lain-1-debet div").html(lain_lain_1_debet);
			$("#lain_lain-1-kredit div").html(lain_lain_1_kredit);
			
			// Update Display Administrasi di lain-Lain:
			var lain_lain_2_debet = parseFloat($("#lain_lain-2-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var lain_lain_2_kredit = parseFloat($("#lain_lain-2-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			lain_lain_2_debet += json.lain_lain_2_debet;
			lain_lain_2_debet = parseFloat(lain_lain_2_debet).formatMoney("2", ".", ",");
			lain_lain_2_kredit += json.lain_lain_2_kredit;
			lain_lain_2_kredit = parseFloat(lain_lain_2_kredit).formatMoney("2", ".", ",");
			$("#lain_lain-2-debet div").html(lain_lain_2_debet);
			$("#lain_lain-2-kredit div").html(lain_lain_2_kredit);
			
			// Update Display Billing Rawat Jalan di Lain-Lain:
			var lain_lain_3_debet = parseFloat($("#lain_lain-3-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var lain_lain_3_kredit = parseFloat($("#lain_lain-3-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			lain_lain_3_debet += json.lain_lain_3_debet;
			lain_lain_3_debet = parseFloat(lain_lain_3_debet).formatMoney("2", ".", ",");
			lain_lain_3_kredit += json.lain_lain_3_kredit;
			lain_lain_3_kredit = parseFloat(lain_lain_3_kredit).formatMoney("2", ".", ",");
			$("#lain_lain-3-debet div").html(lain_lain_3_debet);
			$("#lain_lain-3-kredit div").html(lain_lain_3_kredit);

			// Update Display Billing Rawat Inap di Lain-Lain:
			var lain_lain_4_debet = parseFloat($("#lain_lain-4-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var lain_lain_4_kredit = parseFloat($("#lain_lain-4-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			lain_lain_4_debet += json.lain_lain_4_debet;
			lain_lain_4_debet = parseFloat(lain_lain_4_debet).formatMoney("2", ".", ",");
			lain_lain_4_kredit += json.lain_lain_4_kredit;
			lain_lain_4_kredit = parseFloat(lain_lain_4_kredit).formatMoney("2", ".", ",");
			$("#lain_lain-4-debet div").html(lain_lain_4_debet);
			$("#lain_lain-4-kredit div").html(lain_lain_4_kredit);
			
			// Update Display Lain-Lain / EDC di Lain-Lain:
			var lain_lain_5_debet = parseFloat($("#lain_lain-5-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var lain_lain_5_kredit = parseFloat($("#lain_lain-5-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			lain_lain_5_debet += json.lain_lain_5_debet;
			lain_lain_5_debet = parseFloat(lain_lain_5_debet).formatMoney("2", ".", ",");
			lain_lain_5_kredit += json.lain_lain_5_kredit;
			lain_lain_5_kredit = parseFloat(lain_lain_5_kredit).formatMoney("2", ".", ",");
			$("#lain_lain-5-debet div").html(lain_lain_5_debet);
			$("#lain_lain-5-kredit div").html(lain_lain_5_kredit);

			// Update Display Sewa Alat Dokter:
			var lain_lain_6_debet = parseFloat($("#lain_lain-6-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var lain_lain_6_kredit = parseFloat($("#lain_lain-6-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			lain_lain_6_debet += json.lain_lain_6_debet;
			lain_lain_6_debet = parseFloat(lain_lain_6_debet).formatMoney("2", ".", ",");
			lain_lain_6_kredit += json.lain_lain_6_kredit;
			lain_lain_6_kredit = parseFloat(lain_lain_6_kredit).formatMoney("2", ".", ",");
			$("#lain_lain-6-debet div").html(lain_lain_6_debet);
			$("#lain_lain-6-kredit div").html(lain_lain_6_kredit);
			
			// Update Display Terkait Pembayaran (html element: kasir-0-debet, kasir-0-kredit s/d kasir-x-debet, kasir-x-kredit):
			for (var i = 0; i <= json.kasir_index_num; i++) {
				var debet = parseFloat($("#kasir-" + i + "-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
				var kredit = parseFloat($("#kasir-" + i + "-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
				debet += json['kasir_' + i + '_debet'];
				debet = parseFloat(debet).formatMoney("2", ".", ",");
				kredit += json['kasir_' + i + '_kredit'];
				kredit = parseFloat(kredit).formatMoney("2", ".", ",");
				$("#kasir-" + i + "-debet div").html(debet);
				$("#kasir-" + i + "-kredit div").html(kredit);
			}
			
			/// Update Display Terkait Jasa Pelayanan:
			// var jaspel_kredit_0_debet = parseFloat($("#jaspel_kredit-0-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			// var jaspel_kredit_0_kredit = parseFloat($("#jaspel_kredit-0-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			// jaspel_kredit_0_debet += json.jaspel_kredit_0_debet;
			// jaspel_kredit_0_debet = parseFloat(jaspel_kredit_0_debet).formatMoney("2", ".", ",");
			// jaspel_kredit_0_kredit += json.jaspel_kredit_0_kredit;
			// jaspel_kredit_0_kredit = parseFloat(jaspel_kredit_0_kredit).formatMoney("2", ".", ",");
			// $("#jaspel_kredit-0-debet div").html(jaspel_kredit_0_debet);
			// $("#jaspel_kredit-0-kredit div").html(jaspel_kredit_0_kredit);
			
			// var jaspel_debet_0_debet = parseFloat($("#jaspel_debet-0-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			// var jaspel_debet_0_kredit = parseFloat($("#jaspel_debet-0-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			// jaspel_debet_0_debet += json.jaspel_debet_0_debet;
			// jaspel_debet_0_debet = parseFloat(jaspel_debet_0_debet).formatMoney("2", ".", ",");
			// jaspel_debet_0_kredit += json.jaspel_debet_0_kredit;
			// jaspel_debet_0_kredit = parseFloat(jaspel_debet_0_kredit).formatMoney("2", ".", ",");
			// $("#jaspel_debet-0-debet div").html(jaspel_debet_0_debet);
			// $("#jaspel_debet-0-kredit div").html(jaspel_debet_0_kredit);
			
			// var jaspel_debet_1_debet = parseFloat($("#jaspel_debet-1-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			// var jaspel_debet_1_kredit = parseFloat($("#jaspel_debet-1-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			// jaspel_debet_1_debet += json.jaspel_debet_1_debet;
			// jaspel_debet_1_debet = parseFloat(jaspel_debet_1_debet).formatMoney("2", ".", ",");
			// jaspel_debet_1_kredit += json.jaspel_debet_1_kredit;
			// jaspel_debet_1_kredit = parseFloat(jaspel_debet_1_kredit).formatMoney("2", ".", ",");
			// $("#jaspel_debet-1-debet div").html(jaspel_debet_1_debet);
			// $("#jaspel_debet-1-kredit div").html(jaspel_debet_1_kredit);
			
			// var jaspel_debet_2_debet = parseFloat($("#jaspel_debet-2-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			// var jaspel_debet_2_kredit = parseFloat($("#jaspel_debet-2-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			// jaspel_debet_2_debet += json.jaspel_debet_2_debet;
			// jaspel_debet_2_debet = parseFloat(jaspel_debet_2_debet).formatMoney("2", ".", ",");
			// jaspel_debet_2_kredit += json.jaspel_debet_2_kredit;
			// jaspel_debet_2_kredit = parseFloat(jaspel_debet_2_kredit).formatMoney("2", ".", ",");
			// $("#jaspel_debet-2-debet div").html(jaspel_debet_2_debet);
			// $("#jaspel_debet-2-kredit div").html(jaspel_debet_2_kredit);
			
			// var jaspel_debet_3_debet = parseFloat($("#jaspel_debet-3-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			// var jaspel_debet_3_kredit = parseFloat($("#jaspel_debet-3-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			// jaspel_debet_3_debet += json.jaspel_debet_3_debet;
			// jaspel_debet_3_debet = parseFloat(jaspel_debet_3_debet).formatMoney("2", ".", ",");
			// jaspel_debet_3_kredit += json.jaspel_debet_3_kredit;
			// jaspel_debet_3_kredit = parseFloat(jaspel_debet_3_kredit).formatMoney("2", ".", ",");
			// $("#jaspel_debet-3-debet div").html(jaspel_debet_3_debet);
			// $("#jaspel_debet-3-kredit div").html(jaspel_debet_3_kredit);
			
			// var jaspel_debet_4_debet = parseFloat($("#jaspel_debet-4-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			// var jaspel_debet_4_kredit = parseFloat($("#jaspel_debet-4-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			// jaspel_debet_4_debet += json.jaspel_debet_4_debet;
			// jaspel_debet_4_debet = parseFloat(jaspel_debet_4_debet).formatMoney("2", ".", ",");
			// jaspel_debet_4_kredit += json.jaspel_debet_4_kredit;
			// jaspel_debet_4_kredit = parseFloat(jaspel_debet_4_kredit).formatMoney("2", ".", ",");
			// $("#jaspel_debet-4-debet div").html(jaspel_debet_4_debet);
			// $("#jaspel_debet-4-kredit div").html(jaspel_debet_4_kredit);
			
			// var jaspel_debet_5_debet = parseFloat($("#jaspel_debet-5-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			// var jaspel_debet_5_kredit = parseFloat($("#jaspel_debet-5-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			// jaspel_debet_5_debet += json.jaspel_debet_5_debet;
			// jaspel_debet_5_debet = parseFloat(jaspel_debet_5_debet).formatMoney("2", ".", ",");
			// jaspel_debet_5_kredit += json.jaspel_debet_5_kredit;
			// jaspel_debet_5_kredit = parseFloat(jaspel_debet_5_kredit).formatMoney("2", ".", ",");
			// $("#jaspel_debet-5-debet div").html(jaspel_debet_5_debet);
			// $("#jaspel_debet-5-kredit div").html(jaspel_debet_5_kredit);
			
			// var jaspel_debet_6_debet = parseFloat($("#jaspel_debet-6-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			// var jaspel_debet_6_kredit = parseFloat($("#jaspel_debet-6-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			// jaspel_debet_6_debet += json.jaspel_debet_6_debet;
			// jaspel_debet_6_debet = parseFloat(jaspel_debet_6_debet).formatMoney("2", ".", ",");
			// jaspel_debet_6_kredit += json.jaspel_debet_6_kredit;
			// jaspel_debet_6_kredit = parseFloat(jaspel_debet_6_kredit).formatMoney("2", ".", ",");
			// $("#jaspel_debet-6-debet div").html(jaspel_debet_6_debet);
			// $("#jaspel_debet-6-kredit div").html(jaspel_debet_6_kredit);
			
			// var jaspel_debet_7_debet = parseFloat($("#jaspel_debet-7-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			// var jaspel_debet_7_kredit = parseFloat($("#jaspel_debet-7-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			// jaspel_debet_7_debet += json.jaspel_debet_7_debet;
			// jaspel_debet_7_debet = parseFloat(jaspel_debet_7_debet).formatMoney("2", ".", ",");
			// jaspel_debet_7_kredit += json.jaspel_debet_7_kredit;
			// jaspel_debet_7_kredit = parseFloat(jaspel_debet_7_kredit).formatMoney("2", ".", ",");
			// $("#jaspel_debet-7-debet div").html(jaspel_debet_7_debet);
			// $("#jaspel_debet-7-kredit div").html(jaspel_debet_7_kredit);
			
			// var jaspel_debet_8_debet = parseFloat($("#jaspel_debet-8-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			// var jaspel_debet_8_kredit = parseFloat($("#jaspel_debet-8-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			// jaspel_debet_8_debet += json.jaspel_debet_8_debet;
			// jaspel_debet_8_debet = parseFloat(jaspel_debet_8_debet).formatMoney("2", ".", ",");
			// jaspel_debet_8_kredit += json.jaspel_debet_8_kredit;
			// jaspel_debet_8_kredit = parseFloat(jaspel_debet_8_kredit).formatMoney("2", ".", ",");
			// $("#jaspel_debet-8-debet div").html(jaspel_debet_8_debet);
			// $("#jaspel_debet-8-kredit div").html(jaspel_debet_8_kredit);
			
			// var jaspel_debet_9_debet = parseFloat($("#jaspel_debet-9-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			// var jaspel_debet_9_kredit = parseFloat($("#jaspel_debet-9-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			// jaspel_debet_9_debet += json.jaspel_debet_9_debet;
			// jaspel_debet_9_debet = parseFloat(jaspel_debet_9_debet).formatMoney("2", ".", ",");
			// jaspel_debet_9_kredit += json.jaspel_debet_9_kredit;
			// jaspel_debet_9_kredit = parseFloat(jaspel_debet_9_kredit).formatMoney("2", ".", ",");
			// $("#jaspel_debet-9-debet div").html(jaspel_debet_9_debet);
			// $("#jaspel_debet-9-kredit div").html(jaspel_debet_9_kredit);
			
			// var jaspel_debet_10_debet = parseFloat($("#jaspel_debet-10-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			// var jaspel_debet_10_kredit = parseFloat($("#jaspel_debet-10-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			// jaspel_debet_10_debet += json.jaspel_debet_10_debet;
			// jaspel_debet_10_debet = parseFloat(jaspel_debet_10_debet).formatMoney("2", ".", ",");
			// jaspel_debet_10_kredit += json.jaspel_debet_10_kredit;
			// jaspel_debet_10_kredit = parseFloat(jaspel_debet_10_kredit).formatMoney("2", ".", ",");
			// $("#jaspel_debet-10-debet div").html(jaspel_debet_10_debet);
			// $("#jaspel_debet-10-kredit div").html(jaspel_debet_10_kredit);
			
			// var jaspel_debet_11_debet = parseFloat($("#jaspel_debet-11-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			// var jaspel_debet_11_kredit = parseFloat($("#jaspel_debet-11-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			// jaspel_debet_11_debet += json.jaspel_debet_11_debet;
			// jaspel_debet_11_debet = parseFloat(jaspel_debet_11_debet).formatMoney("2", ".", ",");
			// jaspel_debet_11_kredit += json.jaspel_debet_11_kredit;
			// jaspel_debet_11_kredit = parseFloat(jaspel_debet_11_kredit).formatMoney("2", ".", ",");
			// $("#jaspel_debet-11-debet div").html(jaspel_debet_11_debet);
			// $("#jaspel_debet-11-kredit div").html(jaspel_debet_11_kredit);
			
			// var jaspel_debet_12_debet = parseFloat($("#jaspel_debet-12-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			// var jaspel_debet_12_kredit = parseFloat($("#jaspel_debet-12-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			// jaspel_debet_12_debet += json.jaspel_debet_12_debet;
			// jaspel_debet_12_debet = parseFloat(jaspel_debet_12_debet).formatMoney("2", ".", ",");
			// jaspel_debet_12_kredit += json.jaspel_debet_12_kredit;
			// jaspel_debet_12_kredit = parseFloat(jaspel_debet_12_kredit).formatMoney("2", ".", ",");
			// $("#jaspel_debet-12-debet div").html(jaspel_debet_12_debet);
			// $("#jaspel_debet-12-kredit div").html(jaspel_debet_12_kredit);
			
			// var jaspel_debet_13_debet = parseFloat($("#jaspel_debet-13-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			// var jaspel_debet_13_kredit = parseFloat($("#jaspel_debet-13-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			// jaspel_debet_13_debet += json.jaspel_debet_13_debet;
			// jaspel_debet_13_debet = parseFloat(jaspel_debet_13_debet).formatMoney("2", ".", ",");
			// jaspel_debet_13_kredit += json.jaspel_debet_13_kredit;
			// jaspel_debet_13_kredit = parseFloat(jaspel_debet_13_kredit).formatMoney("2", ".", ",");
			// $("#jaspel_debet-13-debet div").html(jaspel_debet_13_debet);
			// $("#jaspel_debet-13-kredit div").html(jaspel_debet_13_kredit);
			
			// var jaspel_debet_14_debet = parseFloat($("#jaspel_debet-14-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			// var jaspel_debet_14_kredit = parseFloat($("#jaspel_debet-14-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			// jaspel_debet_14_debet += json.jaspel_debet_14_debet;
			// jaspel_debet_14_debet = parseFloat(jaspel_debet_14_debet).formatMoney("2", ".", ",");
			// jaspel_debet_14_kredit += json.jaspel_debet_14_kredit;
			// jaspel_debet_14_kredit = parseFloat(jaspel_debet_14_kredit).formatMoney("2", ".", ",");
			// $("#jaspel_debet-14-debet div").html(jaspel_debet_14_debet);
			// $("#jaspel_debet-14-kredit div").html(jaspel_debet_14_kredit);
			
			// var jaspel_debet_15_debet = parseFloat($("#jaspel_debet-15-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			// var jaspel_debet_15_kredit = parseFloat($("#jaspel_debet-15-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			// jaspel_debet_15_debet += json.jaspel_debet_15_debet;
			// jaspel_debet_15_debet = parseFloat(jaspel_debet_15_debet).formatMoney("2", ".", ",");
			// jaspel_debet_15_kredit += json.jaspel_debet_15_kredit;
			// jaspel_debet_15_kredit = parseFloat(jaspel_debet_15_kredit).formatMoney("2", ".", ",");
			// $("#jaspel_debet-15-debet div").html(jaspel_debet_15_debet);
			// $("#jaspel_debet-15-kredit div").html(jaspel_debet_15_kredit);
			
			// var jaspel_debet_16_debet = parseFloat($("#jaspel_debet-16-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			// var jaspel_debet_16_kredit = parseFloat($("#jaspel_debet-16-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			// jaspel_debet_16_debet += json.jaspel_debet_16_debet;
			// jaspel_debet_16_debet = parseFloat(jaspel_debet_16_debet).formatMoney("2", ".", ",");
			// jaspel_debet_16_kredit += json.jaspel_debet_16_kredit;
			// jaspel_debet_16_kredit = parseFloat(jaspel_debet_16_kredit).formatMoney("2", ".", ",");
			// $("#jaspel_debet-16-debet div").html(jaspel_debet_16_debet);
			// $("#jaspel_debet-16-kredit div").html(jaspel_debet_16_kredit);
			
			// var jaspel_debet_17_debet = parseFloat($("#jaspel_debet-17-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			// var jaspel_debet_17_kredit = parseFloat($("#jaspel_debet-17-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			// jaspel_debet_17_debet += json.jaspel_debet_17_debet;
			// jaspel_debet_17_debet = parseFloat(jaspel_debet_17_debet).formatMoney("2", ".", ",");
			// jaspel_debet_17_kredit += json.jaspel_debet_17_kredit;
			// jaspel_debet_17_kredit = parseFloat(jaspel_debet_17_kredit).formatMoney("2", ".", ",");
			// $("#jaspel_debet-17-debet div").html(jaspel_debet_17_debet);
			// $("#jaspel_debet-17-kredit div").html(jaspel_debet_17_kredit);
			
			// var jaspel_debet_18_debet = parseFloat($("#jaspel_debet-18-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			// var jaspel_debet_18_kredit = parseFloat($("#jaspel_debet-18-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			// jaspel_debet_18_debet += json.jaspel_debet_18_debet;
			// jaspel_debet_18_debet = parseFloat(jaspel_debet_18_debet).formatMoney("2", ".", ",");
			// jaspel_debet_18_kredit += json.jaspel_debet_18_kredit;
			// jaspel_debet_18_kredit = parseFloat(jaspel_debet_18_kredit).formatMoney("2", ".", ",");
			// $("#jaspel_debet-18-debet div").html(jaspel_debet_18_debet);
			// $("#jaspel_debet-18-kredit div").html(jaspel_debet_18_kredit);
			
			// var jaspel_debet_19_debet = parseFloat($("#jaspel_debet-19-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			// var jaspel_debet_19_kredit = parseFloat($("#jaspel_debet-19-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			// jaspel_debet_19_debet += json.jaspel_debet_19_debet;
			// jaspel_debet_19_debet = parseFloat(jaspel_debet_19_debet).formatMoney("2", ".", ",");
			// jaspel_debet_19_kredit += json.jaspel_debet_19_kredit;
			// jaspel_debet_19_kredit = parseFloat(jaspel_debet_19_kredit).formatMoney("2", ".", ",");
			// $("#jaspel_debet-19-debet div").html(jaspel_debet_19_debet);
			// $("#jaspel_debet-19-kredit div").html(jaspel_debet_19_kredit);
			
			// var jaspel_debet_20_debet = parseFloat($("#jaspel_debet-20-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			// var jaspel_debet_20_kredit = parseFloat($("#jaspel_debet-20-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			// jaspel_debet_20_debet += json.jaspel_debet_20_debet;
			// jaspel_debet_20_debet = parseFloat(jaspel_debet_20_debet).formatMoney("2", ".", ",");
			// jaspel_debet_20_kredit += json.jaspel_debet_20_kredit;
			// jaspel_debet_20_kredit = parseFloat(jaspel_debet_20_kredit).formatMoney("2", ".", ",");
			// $("#jaspel_debet-20-debet div").html(jaspel_debet_20_debet);
			// $("#jaspel_debet-20-kredit div").html(jaspel_debet_20_kredit);
			
			// var jaspel_debet_21_debet = parseFloat($("#jaspel_debet-21-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			// var jaspel_debet_21_kredit = parseFloat($("#jaspel_debet-21-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			// jaspel_debet_21_debet += json.jaspel_debet_21_debet;
			// jaspel_debet_21_debet = parseFloat(jaspel_debet_21_debet).formatMoney("2", ".", ",");
			// jaspel_debet_21_kredit += json.jaspel_debet_21_kredit;
			// jaspel_debet_21_kredit = parseFloat(jaspel_debet_21_kredit).formatMoney("2", ".", ",");
			// $("#jaspel_debet-21-debet div").html(jaspel_debet_21_debet);
			// $("#jaspel_debet-21-kredit div").html(jaspel_debet_21_kredit);
			
			// var jaspel_debet_22_debet = parseFloat($("#jaspel_debet-22-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			// var jaspel_debet_22_kredit = parseFloat($("#jaspel_debet-22-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			// jaspel_debet_22_debet += json.jaspel_debet_22_debet;
			// jaspel_debet_22_debet = parseFloat(jaspel_debet_22_debet).formatMoney("2", ".", ",");
			// jaspel_debet_22_kredit += json.jaspel_debet_22_kredit;
			// jaspel_debet_22_kredit = parseFloat(jaspel_debet_22_kredit).formatMoney("2", ".", ",");
			// $("#jaspel_debet-22-debet div").html(jaspel_debet_22_debet);
			// $("#jaspel_debet-22-kredit div").html(jaspel_debet_22_kredit);
			
			// var jaspel_debet_23_debet = parseFloat($("#jaspel_debet-23-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			// var jaspel_debet_23_kredit = parseFloat($("#jaspel_debet-23-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			// jaspel_debet_23_debet += json.jaspel_debet_23_debet;
			// jaspel_debet_23_debet = parseFloat(jaspel_debet_23_debet).formatMoney("2", ".", ",");
			// jaspel_debet_23_kredit += json.jaspel_debet_23_kredit;
			// jaspel_debet_23_kredit = parseFloat(jaspel_debet_23_kredit).formatMoney("2", ".", ",");
			// $("#jaspel_debet-23-debet div").html(jaspel_debet_23_debet);
			// $("#jaspel_debet-23-kredit div").html(jaspel_debet_23_kredit);
			
			// var jaspel_debet_24_debet = parseFloat($("#jaspel_debet-24-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			// var jaspel_debet_24_kredit = parseFloat($("#jaspel_debet-24-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			// jaspel_debet_24_debet += json.jaspel_debet_24_debet;
			// jaspel_debet_24_debet = parseFloat(jaspel_debet_24_debet).formatMoney("2", ".", ",");
			// jaspel_debet_24_kredit += json.jaspel_debet_24_kredit;
			// jaspel_debet_24_kredit = parseFloat(jaspel_debet_24_kredit).formatMoney("2", ".", ",");
			// $("#jaspel_debet-24-debet div").html(jaspel_debet_24_debet);
			// $("#jaspel_debet-24-kredit div").html(jaspel_debet_24_kredit);
			
			// var jaspel_debet_25_debet = parseFloat($("#jaspel_debet-25-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			// var jaspel_debet_25_kredit = parseFloat($("#jaspel_debet-25-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			// jaspel_debet_25_debet += json.jaspel_debet_25_debet;
			// jaspel_debet_25_debet = parseFloat(jaspel_debet_25_debet).formatMoney("2", ".", ",");
			// jaspel_debet_25_kredit += json.jaspel_debet_25_kredit;
			// jaspel_debet_25_kredit = parseFloat(jaspel_debet_25_kredit).formatMoney("2", ".", ",");
			// $("#jaspel_debet-25-debet div").html(jaspel_debet_25_debet);
			// $("#jaspel_debet-25-kredit div").html(jaspel_debet_25_kredit);
			
			// var jaspel_debet_26_debet = parseFloat($("#jaspel_debet-26-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			// var jaspel_debet_26_kredit = parseFloat($("#jaspel_debet-26-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			// jaspel_debet_26_debet += json.jaspel_debet_26_debet;
			// jaspel_debet_26_debet = parseFloat(jaspel_debet_26_debet).formatMoney("2", ".", ",");
			// jaspel_debet_26_kredit += json.jaspel_debet_26_kredit;
			// jaspel_debet_26_kredit = parseFloat(jaspel_debet_26_kredit).formatMoney("2", ".", ",");
			// $("#jaspel_debet-26-debet div").html(jaspel_debet_26_debet);
			// $("#jaspel_debet-26-kredit div").html(jaspel_debet_26_kredit);
			
			// var jaspel_debet_27_debet = parseFloat($("#jaspel_debet-27-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			// var jaspel_debet_27_kredit = parseFloat($("#jaspel_debet-27-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			// jaspel_debet_27_debet += json.jaspel_debet_27_debet;
			// jaspel_debet_27_debet = parseFloat(jaspel_debet_27_debet).formatMoney("2", ".", ",");
			// jaspel_debet_27_kredit += json.jaspel_debet_27_kredit;
			// jaspel_debet_27_kredit = parseFloat(jaspel_debet_27_kredit).formatMoney("2", ".", ",");
			// $("#jaspel_debet-27-debet div").html(jaspel_debet_27_debet);
			// $("#jaspel_debet-27-kredit div").html(jaspel_debet_27_kredit);
			
			// var jaspel_debet_28_debet = parseFloat($("#jaspel_debet-28-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			// var jaspel_debet_28_kredit = parseFloat($("#jaspel_debet-28-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			// jaspel_debet_28_debet += json.jaspel_debet_28_debet;
			// jaspel_debet_28_debet = parseFloat(jaspel_debet_28_debet).formatMoney("2", ".", ",");
			// jaspel_debet_28_kredit += json.jaspel_debet_28_kredit;
			// jaspel_debet_28_kredit = parseFloat(jaspel_debet_28_kredit).formatMoney("2", ".", ",");
			// $("#jaspel_debet-28-debet div").html(jaspel_debet_28_debet);
			// $("#jaspel_debet-28-kredit div").html(jaspel_debet_28_kredit);
			
			$("#jurnal_05_loading_bar").sload("true", json.noreg_pasien + " - " + json.nrm_pasien + " - " + json.nama_pasien + " - " + json.jenis_pasien + " (" + (num + 1) + " / " + limit + ")", (num + 1) * 100 / limit - 1);
			self.fillHtml(num + 1, limit);
		}
	);
};
Jurnal05Action.prototype.finalize = function() {
	$("#jurnal_05_loading_bar").sload("true", "Finalisasi...", 100);
	var num_rows = $("#jurnal_05_list").children("tr").length;
	var total_debet = 0;
	var total_kredit = 0;
	for (var i = 1; i < num_rows + 1; i++) {
		var no = $("#table_jurnal_05 tr:eq(" + i + ") td:eq(0)").text();
		var debet = 0;
		if ($("#table_jurnal_05 tr:eq(" + i + ") td:eq(3)").text() != null && $("#table_jurnal_05 tr:eq(" + i + ") td:eq(3)").text() != "")
			debet = parseFloat($("#table_jurnal_05 tr:eq(" + i + ") td:eq(3)").text().replace(/[^0-9-,]/g, '').replace(",", "."));
		var kredit = 0;
		if ($("#table_jurnal_05 tr:eq(" + i + ") td:eq(4)").text() != null && $("#table_jurnal_05 tr:eq(" + i + ") td:eq(4)").text() != "")
			kredit = parseFloat($("#table_jurnal_05 tr:eq(" + i + ") td:eq(4)").text().replace(/[^0-9-,]/g, '').replace(",", "."));
		total_debet += debet;
		total_kredit += kredit;
	}
	$("#jurnal_05_list").append(
		"<tr>" +
			"<td colspan='3'><small><strong><center>T O T A L</center></strong></small></td>" +
			"<td id='total_jurnal_debet'><small><div align='right'>" + total_debet.formatMoney("2", ".", ",") + "</div></td>" +
			"<td id='total_jurnal_kredit'><small><div align='right'>" + total_kredit.formatMoney("2", ".", ",") + "</div></td>" +
		"</tr>"
	);
	$("#jurnal_05_modal").smodal("hide");
	$("#jurnal_05_info").html(
		"<div class='alert alert-block alert-info'>" +
			"<center><strong>PROSES SELESAI</strong></center>" + 
		"</div>"
	);
	$("#jurnal_05_export_button").removeAttr("onclick");
	$("#jurnal_05_export_button").attr("onclick", "jurnal_05.export_excel()");
};
Jurnal05Action.prototype.export_excel = function() {
	var data = this.getRegulerData();
	data['command'] = "export_excel";
	var num_rows = $("tbody#jurnal_05_list").children("tr").length - 1;
	var d_data = {};
	for (var i = 0; i < num_rows; i++) {
		var dd_data = {};
		dd_data['nomor'] = $("tbody#jurnal_05_list").children("tr").eq(i).children("td").eq(0).text();
		dd_data['kode_akun'] = $("tbody#jurnal_05_list").children("tr").eq(i).children("td").eq(1).text();
		dd_data['nama_akun'] = $("tbody#jurnal_05_list").children("tr").eq(i).children("td").eq(2).text();
		dd_data['debet'] = $("tbody#jurnal_05_list").children("tr").eq(i).children("td").eq(3).text().replace(/[^0-9-,]/g, '').replace(",", ".");
		dd_data['kredit'] = $("tbody#jurnal_05_list").children("tr").eq(i).children("td").eq(4).text().replace(/[^0-9-,]/g, '').replace(",", ".");
		d_data[i] = dd_data;
	}
	data['d_data'] = JSON.stringify(d_data);
	data['total_debet'] = parseFloat($("#total_jurnal_debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
	data['total_kredit'] = parseFloat($("#total_jurnal_kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
	data['num_rows'] = num_rows;
	postForm(data);
};
Jurnal05Action.prototype.cancel = function() {
	FINISHED = true;
};