var jurnal_04a;
var jurnal_04a_operator;
var FINISHED;
$(document).ready(function() {
	$('.mydatetime').datetimepicker({ 
		minuteStep: 1
	});
	$("#jurnal_04a_modal").on("show", function() {
		$("a.close").hide();
	});
	$("tbody#jurnal_04a_list").append(
		"<tr>" +
			"<td colspan='8'><center><small><strong>JURNAL BELUM DIPROSES</strong></small></center></td>" +
		"</tr>"
	);
	jurnal_04a_operator = new Jurnal04AOperatorAction(
		"jurnal_04a_operator",
		"kasir",
		"jurnal_2016/jurnal_04a",
		new Array()
	);
	jurnal_04a_operator.setSuperCommand("jurnal_04a_operator");
	jurnal_04a = new Jurnal04AAction(
		"jurnal_04a",
		"kasir",
		"jurnal_2016/jurnal_04a",
		new Array()
	);
});
$(document).keyup(function(e) {
	if (e.which == 27)
		FINISHED = true;
});