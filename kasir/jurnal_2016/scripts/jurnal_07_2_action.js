function Jurnal07_2Action(name, page, action, column) {
	this.initialize(name, page, action, column);
}
Jurnal07_2Action.prototype.constructor = Jurnal07_2Action;
Jurnal07_2Action.prototype = new TableAction();
Jurnal07_2Action.prototype.getRegulerData = function() {
	var data = TableAction.prototype.getRegulerData.call(this);
	data['from'] = $("#jurnal_07_2_from").val();
	data['to'] = $("#jurnal_07_2_to").val();
	data['jenis_pasien'] = $("#jurnal_07_2_jenis_pasien").val();
	data['uri'] = $("#jurnal_07_2_uri").val();
	data['persentase_jaspel'] = $("#jurnal_07_2_persentase_jaspel").val();
	return data;
};
Jurnal07_2Action.prototype.view = function() {
	var self = this;
	$("#jurnal_07_2_info").html("");
	$("#jurnal_07_2_loading_bar").sload("true", "Harap ditunggu...", 0);
	$("#jurnal_07_2_modal").smodal("show");
	FINISHED = false;
	var data = this.getRegulerData();
	data['command'] = "init";
	$.post(
		"",
		data,
		function(response) {
			var json = JSON.parse(response);
			$("#jurnal_07_2_list").html(json.html);
			var pi_data = self.getRegulerData();
			pi_data['command'] = "patient_number";
			$.post(
				"",
				pi_data,
				function(pi_response) {
					var pi_json = JSON.parse(pi_response);
					self.fillHtml(0, pi_json.jumlah);
				}
			);
		}
	);
};
Jurnal07_2Action.prototype.fillHtml = function(num, limit) {
	if (FINISHED || num == limit) {
		$("#jurnal_07_2_loading_bar").sload("true", "Harap ditunggu...", 0);
		if (FINISHED == false && num == limit) {
			this.finalize();
		} else {
			$("#jurnal_07_2_modal").smodal("hide");
			$("#jurnal_07_2_info").html(
				"<div class='alert alert-block alert-inverse'>" +
					"<center><strong>PROSES DIBATALKAN</strong></center>" +
				"</div>"
			);
			$("#jurnal_07_2_export_button").removeAttr("onclick");
		}
		return;
	}
	var self = this;
	var data = this.getRegulerData();
	data['command'] = "patient_info";
	data['num'] = num;
	$.post(
		"",
		data,
		function(response) {
			var json = JSON.parse(response);
			if (json == null) { 
				FINISHED = true;
				return;
			}
			
			/// RPA :			
			var rpa_1_debet = parseFloat($("#07_2_rpa-1-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var rpa_1_kredit = parseFloat($("#07_2_rpa-1-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			rpa_1_debet += json.rpa_1_debet;
			rpa_1_debet = parseFloat(rpa_1_debet).formatMoney("2", ".", ",");
			rpa_1_kredit += json.rpa_1_kredit;
			rpa_1_kredit = parseFloat(rpa_1_kredit).formatMoney("2", ".", ",");
			$("#07_2_rpa-1-debet div").html(rpa_1_debet);
			$("#07_2_rpa-1-kredit div").html(rpa_1_kredit);

			var rpa_2_debet = parseFloat($("#07_2_rpa-2-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var rpa_2_kredit = parseFloat($("#07_2_rpa-2-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			rpa_2_debet += json.rpa_2_debet;
			rpa_2_debet = parseFloat(rpa_2_debet).formatMoney("2", ".", ",");
			rpa_2_kredit += json.rpa_2_kredit;
			rpa_2_kredit = parseFloat(rpa_2_kredit).formatMoney("2", ".", ",");
			$("#07_2_rpa-2-debet div").html(rpa_2_debet);
			$("#07_2_rpa-2-kredit div").html(rpa_2_kredit);

			var rpa_3_debet = parseFloat($("#07_2_rpa-3-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var rpa_3_kredit = parseFloat($("#07_2_rpa-3-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			rpa_3_debet += json.rpa_3_debet;
			rpa_3_debet = parseFloat(rpa_3_debet).formatMoney("2", ".", ",");
			rpa_3_kredit += json.rpa_3_kredit;
			rpa_3_kredit = parseFloat(rpa_3_kredit).formatMoney("2", ".", ",");
			$("#07_2_rpa-3-debet div").html(rpa_3_debet);
			$("#07_2_rpa-3-kredit div").html(rpa_3_kredit);

			/// RPB :			
			var rpb_1_debet = parseFloat($("#07_2_rpb-1-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var rpb_1_kredit = parseFloat($("#07_2_rpb-1-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			rpb_1_debet += json.rpb_1_debet;
			rpb_1_debet = parseFloat(rpb_1_debet).formatMoney("2", ".", ",");
			rpb_1_kredit += json.rpb_1_kredit;
			rpb_1_kredit = parseFloat(rpb_1_kredit).formatMoney("2", ".", ",");
			$("#07_2_rpb-1-debet div").html(rpb_1_debet);
			$("#07_2_rpb-1-kredit div").html(rpb_1_kredit);

			var rpb_2_debet = parseFloat($("#07_2_rpb-2-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var rpb_2_kredit = parseFloat($("#07_2_rpb-2-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			rpb_2_debet += json.rpb_2_debet;
			rpb_2_debet = parseFloat(rpb_2_debet).formatMoney("2", ".", ",");
			rpb_2_kredit += json.rpb_2_kredit;
			rpb_2_kredit = parseFloat(rpb_2_kredit).formatMoney("2", ".", ",");
			$("#07_2_rpb-2-debet div").html(rpb_2_debet);
			$("#07_2_rpb-2-kredit div").html(rpb_2_kredit);

			var rpb_3_debet = parseFloat($("#07_2_rpb-3-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var rpb_3_kredit = parseFloat($("#07_2_rpb-3-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			rpb_3_debet += json.rpb_3_debet;
			rpb_3_debet = parseFloat(rpb_3_debet).formatMoney("2", ".", ",");
			rpb_3_kredit += json.rpb_3_kredit;
			rpb_3_kredit = parseFloat(rpb_3_kredit).formatMoney("2", ".", ",");
			$("#07_2_rpb-3-debet div").html(rpb_3_debet);
			$("#07_2_rpb-3-kredit div").html(rpb_3_kredit);

			/// RPC :			
			var rpc_1_debet = parseFloat($("#07_2_rpc-1-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var rpc_1_kredit = parseFloat($("#07_2_rpc-1-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			rpc_1_debet += json.rpc_1_debet;
			rpc_1_debet = parseFloat(rpc_1_debet).formatMoney("2", ".", ",");
			rpc_1_kredit += json.rpc_1_kredit;
			rpc_1_kredit = parseFloat(rpc_1_kredit).formatMoney("2", ".", ",");
			$("#07_2_rpc-1-debet div").html(rpc_1_debet);
			$("#07_2_rpc-1-kredit div").html(rpc_1_kredit);

			var rpc_2_debet = parseFloat($("#07_2_rpc-2-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var rpc_2_kredit = parseFloat($("#07_2_rpc-2-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			rpc_2_debet += json.rpc_2_debet;
			rpc_2_debet = parseFloat(rpc_2_debet).formatMoney("2", ".", ",");
			rpc_2_kredit += json.rpc_2_kredit;
			rpc_2_kredit = parseFloat(rpc_2_kredit).formatMoney("2", ".", ",");
			$("#07_2_rpc-2-debet div").html(rpc_2_debet);
			$("#07_2_rpc-2-kredit div").html(rpc_2_kredit);

			var rpc_3_debet = parseFloat($("#07_2_rpc-3-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var rpc_3_kredit = parseFloat($("#07_2_rpc-3-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			rpc_3_debet += json.rpc_3_debet;
			rpc_3_debet = parseFloat(rpc_3_debet).formatMoney("2", ".", ",");
			rpc_3_kredit += json.rpc_3_kredit;
			rpc_3_kredit = parseFloat(rpc_3_kredit).formatMoney("2", ".", ",");
			$("#07_2_rpc-3-debet div").html(rpc_3_debet);
			$("#07_2_rpc-3-kredit div").html(rpc_3_kredit);

			/// RPD :			
			var rpd_1_debet = parseFloat($("#07_2_rpd-1-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var rpd_1_kredit = parseFloat($("#07_2_rpd-1-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			rpd_1_debet += json.rpd_1_debet;
			rpd_1_debet = parseFloat(rpd_1_debet).formatMoney("2", ".", ",");
			rpd_1_kredit += json.rpd_1_kredit;
			rpd_1_kredit = parseFloat(rpd_1_kredit).formatMoney("2", ".", ",");
			$("#07_2_rpd-1-debet div").html(rpd_1_debet);
			$("#07_2_rpd-1-kredit div").html(rpd_1_kredit);

			var rpd_2_debet = parseFloat($("#07_2_rpd-2-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var rpd_2_kredit = parseFloat($("#07_2_rpd-2-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			rpd_2_debet += json.rpd_2_debet;
			rpd_2_debet = parseFloat(rpd_2_debet).formatMoney("2", ".", ",");
			rpd_2_kredit += json.rpd_2_kredit;
			rpd_2_kredit = parseFloat(rpd_2_kredit).formatMoney("2", ".", ",");
			$("#07_2_rpd-2-debet div").html(rpd_2_debet);
			$("#07_2_rpd-2-kredit div").html(rpd_2_kredit);

			var rpd_3_debet = parseFloat($("#07_2_rpd-3-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var rpd_3_kredit = parseFloat($("#07_2_rpd-3-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			rpd_3_debet += json.rpd_3_debet;
			rpd_3_debet = parseFloat(rpd_3_debet).formatMoney("2", ".", ",");
			rpd_3_kredit += json.rpd_3_kredit;
			rpd_3_kredit = parseFloat(rpd_3_kredit).formatMoney("2", ".", ",");
			$("#07_2_rpd-3-debet div").html(rpd_3_debet);
			$("#07_2_rpd-3-kredit div").html(rpd_3_kredit);

			/// RPE :			
			var rpe_1_debet = parseFloat($("#07_2_rpe-1-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var rpe_1_kredit = parseFloat($("#07_2_rpe-1-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			rpe_1_debet += json.rpe_1_debet;
			rpe_1_debet = parseFloat(rpe_1_debet).formatMoney("2", ".", ",");
			rpe_1_kredit += json.rpe_1_kredit;
			rpe_1_kredit = parseFloat(rpe_1_kredit).formatMoney("2", ".", ",");
			$("#07_2_rpe-1-debet div").html(rpe_1_debet);
			$("#07_2_rpe-1-kredit div").html(rpe_1_kredit);

			var rpe_2_debet = parseFloat($("#07_2_rpe-2-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var rpe_2_kredit = parseFloat($("#07_2_rpe-2-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			rpe_2_debet += json.rpe_2_debet;
			rpe_2_debet = parseFloat(rpe_2_debet).formatMoney("2", ".", ",");
			rpe_2_kredit += json.rpe_2_kredit;
			rpe_2_kredit = parseFloat(rpe_2_kredit).formatMoney("2", ".", ",");
			$("#07_2_rpe-2-debet div").html(rpe_2_debet);
			$("#07_2_rpe-2-kredit div").html(rpe_2_kredit);

			var rpe_3_debet = parseFloat($("#07_2_rpe-3-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var rpe_3_kredit = parseFloat($("#07_2_rpe-3-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			rpe_3_debet += json.rpe_3_debet;
			rpe_3_debet = parseFloat(rpe_3_debet).formatMoney("2", ".", ",");
			rpe_3_kredit += json.rpe_3_kredit;
			rpe_3_kredit = parseFloat(rpe_3_kredit).formatMoney("2", ".", ",");
			$("#07_2_rpe-3-debet div").html(rpe_3_debet);
			$("#07_2_rpe-3-kredit div").html(rpe_3_kredit);

			/// ICU :			
			var icu_1_debet = parseFloat($("#07_2_icu-1-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var icu_1_kredit = parseFloat($("#07_2_icu-1-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			icu_1_debet += json.icu_1_debet;
			icu_1_debet = parseFloat(icu_1_debet).formatMoney("2", ".", ",");
			icu_1_kredit += json.icu_1_kredit;
			icu_1_kredit = parseFloat(icu_1_kredit).formatMoney("2", ".", ",");
			$("#07_2_icu-1-debet div").html(icu_1_debet);
			$("#07_2_icu-1-kredit div").html(icu_1_kredit);

			var icu_2_debet = parseFloat($("#07_2_icu-2-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var icu_2_kredit = parseFloat($("#07_2_icu-2-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			icu_2_debet += json.icu_2_debet;
			icu_2_debet = parseFloat(icu_2_debet).formatMoney("2", ".", ",");
			icu_2_kredit += json.icu_2_kredit;
			icu_2_kredit = parseFloat(icu_2_kredit).formatMoney("2", ".", ",");
			$("#07_2_icu-2-debet div").html(icu_2_debet);
			$("#07_2_icu-2-kredit div").html(icu_2_kredit);

			var icu_3_debet = parseFloat($("#07_2_icu-3-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var icu_3_kredit = parseFloat($("#07_2_icu-3-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			icu_3_debet += json.icu_3_debet;
			icu_3_debet = parseFloat(icu_3_debet).formatMoney("2", ".", ",");
			icu_3_kredit += json.icu_3_kredit;
			icu_3_kredit = parseFloat(icu_3_kredit).formatMoney("2", ".", ",");
			$("#07_2_icu-3-debet div").html(icu_3_debet);
			$("#07_2_icu-3-kredit div").html(icu_3_kredit);

			/// IGD :			
			var igd_1_debet = parseFloat($("#07_2_instalasi_gawat_darurat-1-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var igd_1_kredit = parseFloat($("#07_2_instalasi_gawat_darurat-1-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			igd_1_debet += json.igd_1_debet;
			igd_1_debet = parseFloat(igd_1_debet).formatMoney("2", ".", ",");
			igd_1_kredit += json.igd_1_kredit;
			igd_1_kredit = parseFloat(igd_1_kredit).formatMoney("2", ".", ",");
			$("#07_2_instalasi_gawat_darurat-1-debet div").html(igd_1_debet);
			$("#07_2_instalasi_gawat_darurat-1-kredit div").html(igd_1_kredit);

			var igd_2_debet = parseFloat($("#07_2_instalasi_gawat_darurat-2-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var igd_2_kredit = parseFloat($("#07_2_instalasi_gawat_darurat-2-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			igd_2_debet += json.igd_2_debet;
			igd_2_debet = parseFloat(igd_2_debet).formatMoney("2", ".", ",");
			igd_2_kredit += json.igd_2_kredit;
			igd_2_kredit = parseFloat(igd_2_kredit).formatMoney("2", ".", ",");
			$("#07_2_instalasi_gawat_darurat-2-debet div").html(igd_2_debet);
			$("#07_2_instalasi_gawat_darurat-2-kredit div").html(igd_2_kredit);

			var igd_3_debet = parseFloat($("#07_2_instalasi_gawat_darurat-3-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var igd_3_kredit = parseFloat($("#07_2_instalasi_gawat_darurat-3-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			igd_3_debet += json.igd_3_debet;
			igd_3_debet = parseFloat(igd_3_debet).formatMoney("2", ".", ",");
			igd_3_kredit += json.igd_3_kredit;
			igd_3_kredit = parseFloat(igd_3_kredit).formatMoney("2", ".", ",");
			$("#07_2_instalasi_gawat_darurat-3-debet div").html(igd_3_debet);
			$("#07_2_instalasi_gawat_darurat-3-kredit div").html(igd_3_kredit);

			/// Poli Umum :			
			var poliumum_1_debet = parseFloat($("#07_2_poliumum-1-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var poliumum_1_kredit = parseFloat($("#07_2_poliumum-1-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			poliumum_1_debet += json.poliumum_1_debet;
			poliumum_1_debet = parseFloat(poliumum_1_debet).formatMoney("2", ".", ",");
			poliumum_1_kredit += json.poliumum_1_kredit;
			poliumum_1_kredit = parseFloat(poliumum_1_kredit).formatMoney("2", ".", ",");
			$("#07_2_poliumum-1-debet div").html(poliumum_1_debet);
			$("#07_2_poliumum-1-kredit div").html(poliumum_1_kredit);

			var poliumum_2_debet = parseFloat($("#07_2_poliumum-2-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var poliumum_2_kredit = parseFloat($("#07_2_poliumum-2-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			poliumum_2_debet += json.poliumum_2_debet;
			poliumum_2_debet = parseFloat(poliumum_2_debet).formatMoney("2", ".", ",");
			poliumum_2_kredit += json.poliumum_2_kredit;
			poliumum_2_kredit = parseFloat(poliumum_2_kredit).formatMoney("2", ".", ",");
			$("#07_2_poliumum-2-debet div").html(poliumum_2_debet);
			$("#07_2_poliumum-2-kredit div").html(poliumum_2_kredit);

			var poliumum_3_debet = parseFloat($("#07_2_poliumum-3-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var poliumum_3_kredit = parseFloat($("#07_2_poliumum-3-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			poliumum_3_debet += json.poliumum_3_debet;
			poliumum_3_debet = parseFloat(poliumum_3_debet).formatMoney("2", ".", ",");
			poliumum_3_kredit += json.poliumum_3_kredit;
			poliumum_3_kredit = parseFloat(poliumum_3_kredit).formatMoney("2", ".", ",");
			$("#07_2_poliumum-3-debet div").html(poliumum_3_debet);
			$("#07_2_poliumum-3-kredit div").html(poliumum_3_kredit);

			/// Fisioterapi :			
			var fisiotherapy_1_debet = parseFloat($("#07_2_fisiotherapy-1-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var fisiotherapy_1_kredit = parseFloat($("#07_2_fisiotherapy-1-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			fisiotherapy_1_debet += json.fisiotherapy_1_debet;
			fisiotherapy_1_debet = parseFloat(fisiotherapy_1_debet).formatMoney("2", ".", ",");
			fisiotherapy_1_kredit += json.fisiotherapy_1_kredit;
			fisiotherapy_1_kredit = parseFloat(fisiotherapy_1_kredit).formatMoney("2", ".", ",");
			$("#07_2_fisiotherapy-1-debet div").html(fisiotherapy_1_debet);
			$("#07_2_fisiotherapy-1-kredit div").html(fisiotherapy_1_kredit);

			var fisiotherapy_2_debet = parseFloat($("#07_2_fisiotherapy-2-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var fisiotherapy_2_kredit = parseFloat($("#07_2_fisiotherapy-2-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			fisiotherapy_2_debet += json.fisiotherapy_2_debet;
			fisiotherapy_2_debet = parseFloat(fisiotherapy_2_debet).formatMoney("2", ".", ",");
			fisiotherapy_2_kredit += json.fisiotherapy_2_kredit;
			fisiotherapy_2_kredit = parseFloat(fisiotherapy_2_kredit).formatMoney("2", ".", ",");
			$("#07_2_fisiotherapy-2-debet div").html(fisiotherapy_2_debet);
			$("#07_2_fisiotherapy-2-kredit div").html(fisiotherapy_2_kredit);

			var fisiotherapy_3_debet = parseFloat($("#07_2_fisiotherapy-3-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var fisiotherapy_3_kredit = parseFloat($("#07_2_fisiotherapy-3-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			fisiotherapy_3_debet += json.fisiotherapy_3_debet;
			fisiotherapy_3_debet = parseFloat(fisiotherapy_3_debet).formatMoney("2", ".", ",");
			fisiotherapy_3_kredit += json.fisiotherapy_3_kredit;
			fisiotherapy_3_kredit = parseFloat(fisiotherapy_3_kredit).formatMoney("2", ".", ",");
			$("#07_2_fisiotherapy-3-debet div").html(fisiotherapy_3_debet);
			$("#07_2_fisiotherapy-3-kredit div").html(fisiotherapy_3_kredit);

			/// Poli Obgyn :			
			var poli_obgyn_1_debet = parseFloat($("#07_2_poli_obgyn-1-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var poli_obgyn_1_kredit = parseFloat($("#07_2_poli_obgyn-1-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			poli_obgyn_1_debet += json.poli_obgyn_1_debet;
			poli_obgyn_1_debet = parseFloat(poli_obgyn_1_debet).formatMoney("2", ".", ",");
			poli_obgyn_1_kredit += json.poli_obgyn_1_kredit;
			poli_obgyn_1_kredit = parseFloat(poli_obgyn_1_kredit).formatMoney("2", ".", ",");
			$("#07_2_poli_obgyn-1-debet div").html(poli_obgyn_1_debet);
			$("#07_2_poli_obgyn-1-kredit div").html(poli_obgyn_1_kredit);

			var poli_obgyn_2_debet = parseFloat($("#07_2_poli_obgyn-2-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var poli_obgyn_2_kredit = parseFloat($("#07_2_poli_obgyn-2-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			poli_obgyn_2_debet += json.poli_obgyn_2_debet;
			poli_obgyn_2_debet = parseFloat(poli_obgyn_2_debet).formatMoney("2", ".", ",");
			poli_obgyn_2_kredit += json.poli_obgyn_2_kredit;
			poli_obgyn_2_kredit = parseFloat(poli_obgyn_2_kredit).formatMoney("2", ".", ",");
			$("#07_2_poli_obgyn-2-debet div").html(poli_obgyn_2_debet);
			$("#07_2_poli_obgyn-2-kredit div").html(poli_obgyn_2_kredit);

			var poli_obgyn_3_debet = parseFloat($("#07_2_poli_obgyn-3-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var poli_obgyn_3_kredit = parseFloat($("#07_2_poli_obgyn-3-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			poli_obgyn_3_debet += json.poli_obgyn_3_debet;
			poli_obgyn_3_debet = parseFloat(poli_obgyn_3_debet).formatMoney("2", ".", ",");
			poli_obgyn_3_kredit += json.poli_obgyn_3_kredit;
			poli_obgyn_3_kredit = parseFloat(poli_obgyn_3_kredit).formatMoney("2", ".", ",");
			$("#07_2_poli_obgyn-3-debet div").html(poli_obgyn_3_debet);
			$("#07_2_poli_obgyn-3-kredit div").html(poli_obgyn_3_kredit);

			/// Poli Syaraf :			
			var poli_syaraf_1_debet = parseFloat($("#07_2_poli_syaraf-1-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var poli_syaraf_1_kredit = parseFloat($("#07_2_poli_syaraf-1-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			poli_syaraf_1_debet += json.poli_syaraf_1_debet;
			poli_syaraf_1_debet = parseFloat(poli_syaraf_1_debet).formatMoney("2", ".", ",");
			poli_syaraf_1_kredit += json.poli_syaraf_1_kredit;
			poli_syaraf_1_kredit = parseFloat(poli_syaraf_1_kredit).formatMoney("2", ".", ",");
			$("#07_2_poli_syaraf-1-debet div").html(poli_syaraf_1_debet);
			$("#07_2_poli_syaraf-1-kredit div").html(poli_syaraf_1_kredit);

			var poli_syaraf_2_debet = parseFloat($("#07_2_poli_syaraf-2-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var poli_syaraf_2_kredit = parseFloat($("#07_2_poli_syaraf-2-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			poli_syaraf_2_debet += json.poli_syaraf_2_debet;
			poli_syaraf_2_debet = parseFloat(poli_syaraf_2_debet).formatMoney("2", ".", ",");
			poli_syaraf_2_kredit += json.poli_syaraf_2_kredit;
			poli_syaraf_2_kredit = parseFloat(poli_syaraf_2_kredit).formatMoney("2", ".", ",");
			$("#07_2_poli_syaraf-2-debet div").html(poli_syaraf_2_debet);
			$("#07_2_poli_syaraf-2-kredit div").html(poli_syaraf_2_kredit);

			var poli_syaraf_3_debet = parseFloat($("#07_2_poli_syaraf-3-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var poli_syaraf_3_kredit = parseFloat($("#07_2_poli_syaraf-3-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			poli_syaraf_3_debet += json.poli_syaraf_3_debet;
			poli_syaraf_3_debet = parseFloat(poli_syaraf_3_debet).formatMoney("2", ".", ",");
			poli_syaraf_3_kredit += json.poli_syaraf_3_kredit;
			poli_syaraf_3_kredit = parseFloat(poli_syaraf_3_kredit).formatMoney("2", ".", ",");
			$("#07_2_poli_syaraf-3-debet div").html(poli_syaraf_3_debet);
			$("#07_2_poli_syaraf-3-kredit div").html(poli_syaraf_3_kredit);

			/// Hemodilisa :			
			var hemodialisa_1_debet = parseFloat($("#07_2_hemodialisa-1-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var hemodialisa_1_kredit = parseFloat($("#07_2_hemodialisa-1-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			hemodialisa_1_debet += json.hemodialisa_1_debet;
			hemodialisa_1_debet = parseFloat(hemodialisa_1_debet).formatMoney("2", ".", ",");
			hemodialisa_1_kredit += json.hemodialisa_1_kredit;
			hemodialisa_1_kredit = parseFloat(hemodialisa_1_kredit).formatMoney("2", ".", ",");
			$("#07_2_hemodialisa-1-debet div").html(hemodialisa_1_debet);
			$("#07_2_hemodialisa-1-kredit div").html(hemodialisa_1_kredit);

			var hemodialisa_2_debet = parseFloat($("#07_2_hemodialisa-2-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var hemodialisa_2_kredit = parseFloat($("#07_2_hemodialisa-2-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			hemodialisa_2_debet += json.hemodialisa_2_debet;
			hemodialisa_2_debet = parseFloat(hemodialisa_2_debet).formatMoney("2", ".", ",");
			hemodialisa_2_kredit += json.hemodialisa_2_kredit;
			hemodialisa_2_kredit = parseFloat(hemodialisa_2_kredit).formatMoney("2", ".", ",");
			$("#07_2_hemodialisa-2-debet div").html(hemodialisa_2_debet);
			$("#07_2_hemodialisa-2-kredit div").html(hemodialisa_2_kredit);

			var hemodialisa_3_debet = parseFloat($("#07_2_hemodialisa-3-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var hemodialisa_3_kredit = parseFloat($("#07_2_hemodialisa-3-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			hemodialisa_3_debet += json.hemodialisa_3_debet;
			hemodialisa_3_debet = parseFloat(hemodialisa_3_debet).formatMoney("2", ".", ",");
			hemodialisa_3_kredit += json.hemodialisa_3_kredit;
			hemodialisa_3_kredit = parseFloat(hemodialisa_3_kredit).formatMoney("2", ".", ",");
			$("#07_2_hemodialisa-3-debet div").html(hemodialisa_3_debet);
			$("#07_2_hemodialisa-3-kredit div").html(hemodialisa_3_kredit);

			/// Kamar Operasi :			
			var kamar_operasi_1_debet = parseFloat($("#07_2_kamar_operasi-1-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var kamar_operasi_1_kredit = parseFloat($("#07_2_kamar_operasi-1-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			kamar_operasi_1_debet += json.kamar_operasi_1_debet;
			kamar_operasi_1_debet = parseFloat(kamar_operasi_1_debet).formatMoney("2", ".", ",");
			kamar_operasi_1_kredit += json.kamar_operasi_1_kredit;
			kamar_operasi_1_kredit = parseFloat(kamar_operasi_1_kredit).formatMoney("2", ".", ",");
			$("#07_2_kamar_operasi-1-debet div").html(kamar_operasi_1_debet);
			$("#07_2_kamar_operasi-1-kredit div").html(kamar_operasi_1_kredit);

			var kamar_operasi_2_debet = parseFloat($("#07_2_kamar_operasi-2-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var kamar_operasi_2_kredit = parseFloat($("#07_2_kamar_operasi-2-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			kamar_operasi_2_debet += json.kamar_operasi_2_debet;
			kamar_operasi_2_debet = parseFloat(kamar_operasi_2_debet).formatMoney("2", ".", ",");
			kamar_operasi_2_kredit += json.kamar_operasi_2_kredit;
			kamar_operasi_2_kredit = parseFloat(kamar_operasi_2_kredit).formatMoney("2", ".", ",");
			$("#07_2_kamar_operasi-2-debet div").html(kamar_operasi_2_debet);
			$("#07_2_kamar_operasi-2-kredit div").html(kamar_operasi_2_kredit);

			var kamar_operasi_3_debet = parseFloat($("#07_2_kamar_operasi-3-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var kamar_operasi_3_kredit = parseFloat($("#07_2_kamar_operasi-3-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			kamar_operasi_3_debet += json.kamar_operasi_3_debet;
			kamar_operasi_3_debet = parseFloat(kamar_operasi_3_debet).formatMoney("2", ".", ",");
			kamar_operasi_3_kredit += json.kamar_operasi_3_kredit;
			kamar_operasi_3_kredit = parseFloat(kamar_operasi_3_kredit).formatMoney("2", ".", ",");
			$("#07_2_kamar_operasi-3-debet div").html(kamar_operasi_3_debet);
			$("#07_2_kamar_operasi-3-kredit div").html(kamar_operasi_3_kredit);

			/// RPKK :			
			var rpkk_1_debet = parseFloat($("#07_2_rpkk-1-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var rpkk_1_kredit = parseFloat($("#07_2_rpkk-1-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			rpkk_1_debet += json.rpkk_1_debet;
			rpkk_1_debet = parseFloat(rpkk_1_debet).formatMoney("2", ".", ",");
			rpkk_1_kredit += json.rpkk_1_kredit;
			rpkk_1_kredit = parseFloat(rpkk_1_kredit).formatMoney("2", ".", ",");
			$("#07_2_rpkk-1-debet div").html(rpkk_1_debet);
			$("#07_2_rpkk-1-kredit div").html(rpkk_1_kredit);

			var rpkk_2_debet = parseFloat($("#07_2_rpkk-2-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var rpkk_2_kredit = parseFloat($("#07_2_rpkk-2-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			rpkk_2_debet += json.rpkk_2_debet;
			rpkk_2_debet = parseFloat(rpkk_2_debet).formatMoney("2", ".", ",");
			rpkk_2_kredit += json.rpkk_2_kredit;
			rpkk_2_kredit = parseFloat(rpkk_2_kredit).formatMoney("2", ".", ",");
			$("#07_2_rpkk-2-debet div").html(rpkk_2_debet);
			$("#07_2_rpkk-2-kredit div").html(rpkk_2_kredit);

			var rpkk_3_debet = parseFloat($("#07_2_rpkk-3-debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var rpkk_3_kredit = parseFloat($("#07_2_rpkk-3-kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			rpkk_3_debet += json.rpkk_3_debet;
			rpkk_3_debet = parseFloat(rpkk_3_debet).formatMoney("2", ".", ",");
			rpkk_3_kredit += json.rpkk_3_kredit;
			rpkk_3_kredit = parseFloat(rpkk_3_kredit).formatMoney("2", ".", ",");
			$("#07_2_rpkk-3-debet div").html(rpkk_3_debet);
			$("#07_2_rpkk-3-kredit div").html(rpkk_3_kredit);

			$("#jurnal_07_2_loading_bar").sload("true", json.noreg_pasien + " - " + json.nrm_pasien + " - " + json.nama_pasien + " - " + json.jenis_pasien + " (" + (num + 1) + " / " + limit + ")", (num + 1) * 100 / limit - 1);
			self.fillHtml(num + 1, limit);
		}
	);
};
Jurnal07_2Action.prototype.finalize = function() {
	$("#jurnal_07_2_loading_bar").sload("true", "Finalisasi...", 100);
	var num_rows = $("#jurnal_07_2_list").children("tr").length;
	var total_debet = 0;
	var total_kredit = 0;
	for (var i = 1; i < num_rows + 1; i++) {
		var no = $("#table_jurnal_07_2 tr:eq(" + i + ") td:eq(0)").text();
		var debet = 0;
		if ($("#table_jurnal_07_2 tr:eq(" + i + ") td:eq(3)").text() != null && $("#table_jurnal_07_2 tr:eq(" + i + ") td:eq(3)").text() != "")
			debet = parseFloat($("#table_jurnal_07_2 tr:eq(" + i + ") td:eq(3)").text().replace(/[^0-9-,]/g, '').replace(",", "."));
		var kredit = 0;
		if ($("#table_jurnal_07_2 tr:eq(" + i + ") td:eq(4)").text() != null && $("#table_jurnal_07_2 tr:eq(" + i + ") td:eq(4)").text() != "")
			kredit = parseFloat($("#table_jurnal_07_2 tr:eq(" + i + ") td:eq(4)").text().replace(/[^0-9-,]/g, '').replace(",", "."));
		total_debet += debet;
		total_kredit += kredit;
	}
	$("#jurnal_07_2_list").append(
		"<tr>" +
			"<td colspan='3'><small><strong><center>T O T A L</center></strong></small></td>" +
			"<td id='total_jurnal_debet'><small><div align='right'>" + total_debet.formatMoney("2", ".", ",") + "</div></td>" +
			"<td id='total_jurnal_kredit'><small><div align='right'>" + total_kredit.formatMoney("2", ".", ",") + "</div></td>" +
		"</tr>"
	);
	$("#jurnal_07_2_modal").smodal("hide");
	$("#jurnal_07_2_info").html(
		"<div class='alert alert-block alert-info'>" +
			"<center><strong>PROSES SELESAI</strong></center>" + 
		"</div>"
	);
	$("#jurnal_07_2_export_button").removeAttr("onclick");
	$("#jurnal_07_2_export_button").attr("onclick", "jurnal_07_2.export_excel()");
};
Jurnal07_2Action.prototype.export_excel = function() {
	var data = this.getRegulerData();
	data['command'] = "export_excel";
	var num_rows = $("tbody#jurnal_07_2_list").children("tr").length - 1;
	var d_data = {};
	for (var i = 0; i < num_rows; i++) {
		var dd_data = {};
		dd_data['nomor'] = $("tbody#jurnal_07_2_list").children("tr").eq(i).children("td").eq(0).text();
		dd_data['kode_akun'] = $("tbody#jurnal_07_2_list").children("tr").eq(i).children("td").eq(1).text();
		dd_data['nama_akun'] = $("tbody#jurnal_07_2_list").children("tr").eq(i).children("td").eq(2).text();
		dd_data['debet'] = $("tbody#jurnal_07_2_list").children("tr").eq(i).children("td").eq(3).text().replace(/[^0-9-,]/g, '').replace(",", ".");
		dd_data['kredit'] = $("tbody#jurnal_07_2_list").children("tr").eq(i).children("td").eq(4).text().replace(/[^0-9-,]/g, '').replace(",", ".");
		d_data[i] = dd_data;
	}
	data['d_data'] = JSON.stringify(d_data);
	data['total_debet'] = parseFloat($("#total_jurnal_debet").text().replace(/[^0-9-,]/g, '').replace(",", "."));
	data['total_kredit'] = parseFloat($("#total_jurnal_kredit").text().replace(/[^0-9-,]/g, '').replace(",", "."));
	data['num_rows'] = num_rows;
	postForm(data);
};
Jurnal07_2Action.prototype.cancel = function() {
	FINISHED = true;
};