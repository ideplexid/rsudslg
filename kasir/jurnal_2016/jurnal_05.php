<?php
	require_once("smis-base/smis-include-service-consumer.php");
	global $db;
	
	$form = new Form("", "", "Jurnal 05 : Jurnal Pendapatan");
	$from_date_text = new Text("jurnal_05_from", "jurnal_05_from", date("Y-m-d") . " 00:00");
	$from_date_text->setClass("mydatetime");
	$from_date_text->setAtribute("data-date-format='yyyy-mm-dd hh:ii'");
	$form->addELement("Tanggal Awal", $from_date_text);
	$to_date_text = new Text("jurnal_05_to", "jurnal_05_to", date("Y-m-d H:i"));
	$to_date_text->setClass("mydatetime");
	$to_date_text->setAtribute("data-date-format='yyyy-mm-dd hh:ii'");
	$form->addELement("Tanggal Akhir", $to_date_text);
		$consumer_service = new ServiceConsumer(
		$db, 
		"get_jenis_patient",
		null,
		"registration" 
	);
	$content = $consumer_service->execute()->getContent();
	$jenis_pasien_option = new OptionBuilder();
	$jenis_pasien_option->add("SEMUA", "%%", "1");
	foreach($content as $c){
		$jenis_pasien_option->add($c['name'], $c['value']);
	}
	$jenis_pasien_select = new Select("jurnal_05_jenis_pasien", "jurnal_05_jenis_pasien", $jenis_pasien_option->getContent());
	$form->addELement("Jenis Pasien", $jenis_pasien_select);
	$uri_option = new OptionBuilder();
	$uri_option->add("SEMUA", "%%", 1);
	$uri_option->add("IRJA", "0");
	$uri_option->add("IRNA", "1");
	$uri_select = new Select("jurnal_05_uri", "jurnal_05_uri", $uri_option->getContent());
	$form->addELement("IRJA / IRNA", $uri_select);
	$view_button = new Button("", "", "Lihat");
	$view_button->setIsButton(Button::$ICONIC);
	$view_button->setClass("btn-info");
	$view_button->setIcon("icon-white icon-repeat");
	$view_button->setAction("jurnal_05.view()");
	$export_button = new Button("", "", "Ekspor Berkas Excel");
	$export_button->setIsButton(Button::$ICONIC);
	$export_button->setClass("btn-inverse");
	$export_button->setIcon("fa fa-download");
	$export_button->setAtribute("id='jurnal_05_export_button'");
	$button_group = new ButtonGroup("");
	$button_group->addButton($view_button);
	$button_group->addButton($export_button);
	$form->addELement("", $button_group);
	
	$table = new Table(
		array(
			"No.", "Kode Akun", "Nama Akun", "Debet", "Kredit"
		),
		"",
		null,
		true
	);
	$table->setName("jurnal_05");
	$table->setAction(false);
	$table->setFooterVisible(false);
	
	if (isset($_POST['command'])) {
		$command = $_POST['command'];
		require_once("kasir/jurnal_2016/commands/jurnal_05_" . $command . ".php");
		return;
	}
	
	$loading_bar = new LoadingBar("jurnal_05_loading_bar", "");
	$button = new Button("", "", "Batal");
	$button->addClass("btn-primary");
	$button->setIsButton(Button::$ICONIC_TEXT);
	$button->setIcon("fa fa-close");
	$button->setAction("jurnal_05.cancel()");
	$loading_modal = new Modal("jurnal_05_modal", "", "Proses..");
	$loading_modal->addHtml($loading_bar->getHtml(), "after");
	$loading_modal->addFooter($button);
	
	echo $loading_modal->getHtml();
	echo $form->getHtml();
	echo $table->getHtml();
	echo "<div id='jurnal_05_info'></div>";
	echo addJS("framework/smis/js/table_action.js");
	echo addJS("base-js/smis-base-loading.js");
	echo addJS("framework/bootstrap/js/bootstrap-datetimepicker.js");
	echo addCSS("framework/bootstrap/css/bootstrap-datetimepicker.css");
	echo addJS("kasir/jurnal_2016/scripts/jurnal_05_action.js", false);
	echo addJS("kasir/jurnal_2016/scripts/jurnal_05_main.js", false);
?>