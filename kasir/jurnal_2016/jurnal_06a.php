<?php
	require_once("smis-base/smis-include-service-consumer.php");
	global $db;
	
	$form = new Form("", "", "Jurnal 06.a : Titipan Jasa Medis");
	$id_dokter_hidden = new Hidden("jurnal_06a_id_dokter", "jurnal_06a_id_dokter", "");
	$form->addELement("", $id_dokter_hidden);
	$nama_dokter_text = new Text("jurnal_06a_nama_dokter", "jurnal_06a_nama_dokter", "");
	$nama_dokter_text->setAtribute("disabled='disabled'");
	$nama_dokter_text->setClass("smis-one-option-input");
	$browse_button = new Button("", "", "Pilih");
	$browse_button->setClass("btn-info");
	$browse_button->setIsButton(Button::$ICONIC);
	$browse_button->setIcon("icon-white ".Button::$icon_list_alt);
	$browse_button->setAction("jurnal_06a_dokter.chooser('jurnal_06a_dokter', 'jurnal_06a_dokter_button', 'jurnal_06a_dokter', jurnal_06a_dokter)");
	$browse_button->setAtribute("id='jurnal_06a_dokter_browse'");
	$input_group = new InputGroup("");
	$input_group->addComponent($nama_dokter_text);
	$input_group->addComponent($browse_button);
	$form->addELement("Nama Dokter", $input_group);
	$kode_dokter_text = new Text("jurnal_06a_kode_dokter", "jurnal_06a_kode_dokter", "");
	$kode_dokter_text->setAtribute("disabled='disabled'");
	$form->addELement("Kode Dokter", $kode_dokter_text);
	$hak_dokter_hidden = new Hidden("jurnal_06a_hak_dokter", "jurnal_06a_hak_dokter", "");
	$form->addELement("", $hak_dokter_hidden);
	$no_jurnal_text = new Text("jurnal_06a_no_jurnal", "jurnal_06a_no_jurnal", "");
	$form->addELement("<small>No. Buku / Jurnal</small>", $no_jurnal_text);
	$from_date_text = new Text("jurnal_06a_from", "jurnal_06a_from", date("Y-m-d") . " 00:00");
	$from_date_text->setClass("mydatetime");
	$from_date_text->setAtribute("data-date-format='yyyy-mm-dd hh:ii'");
	$form->addELement("Tanggal Awal", $from_date_text);
	$to_date_text = new Text("jurnal_06a_to", "jurnal_06a_to", date("Y-m-d H:i"));
	$to_date_text->setClass("mydatetime");
	$to_date_text->setAtribute("data-date-format='yyyy-mm-dd hh:ii'");
	$form->addELement("Tanggal Akhir", $to_date_text);
	$view_button = new Button("", "", "Lihat");
	$view_button->setIsButton(Button::$ICONIC);
	$view_button->setClass("btn-info");
	$view_button->setIcon("icon-white icon-repeat");
	$view_button->setAction("jurnal_06a.view()");
	$export_button = new Button("", "", "Ekspor Berkas Excel");
	$export_button->setIsButton(Button::$ICONIC);
	$export_button->setClass("btn-inverse");
	$export_button->setIcon("fa fa-download");
	$export_button->setAtribute("id='jurnal_06a_export_button'");
	$button_group = new ButtonGroup("");
	$button_group->addButton($view_button);
	$button_group->addButton($export_button);
	$form->addELement("", $button_group);
	
	$table = new Table(
		array(
			"No.", "No. Reg.", "Nama Pasien", "No. RM", "Tgl. Layanan", "Bruto", "Jasa RSUK", "Jasa Sebelum Pajak", "DPP", "PPH 21", "Netto", "No. Buku / Jurnal"
		),
		"",
		null,
		true
	);
	$table->setName("jurnal_06a");
	$table->setAction(false);
	$table->setFooterVisible(false);
	
	/// Chooser Dokter:
	$dokter_table = new Table(
		array("Kode Dokter", "Nama Dokter"),
		"",
		null,
		true
	);
	$dokter_table->setName("jurnal_06a_dokter");
	$dokter_table->setModel(Table::$SELECT);
	$dokter_adapter = new SimpleAdapter();
	$dokter_adapter->add("Kode Dokter", "kode_dokter");
	$dokter_adapter->add("Nama Dokter", "nama_dokter");
	$dokter_service_responder = new ServiceResponder(
		$db, 
		$dokter_table, 
		$dokter_adapter,
		"get_list_dokter_jurnal"
	);
	$super_command = new SuperCommand();
	$super_command->addResponder("jurnal_06a_dokter", $dokter_service_responder);
	$init = $super_command->initialize();
	if ($init != null) {
		echo $init;
		return;
	}
	
	if (isset($_POST['command'])) {
		$command = $_POST['command'];
		require_once("kasir/jurnal_2016/commands/jurnal_06a_" . $command . ".php");
		return;
	}
	
	$loading_bar = new LoadingBar("jurnal_06a_loading_bar", "");
	$button = new Button("", "", "Batal");
	$button->addClass("btn-primary");
	$button->setIsButton(Button::$ICONIC_TEXT);
	$button->setIcon("fa fa-close");
	$button->setAction("jurnal_06a.cancel()");
	$loading_modal = new Modal("jurnal_06a_modal", "", "Proses..");
	$loading_modal->addHtml($loading_bar->getHtml(), "after");
	$loading_modal->addFooter($button);
	
	echo $loading_modal->getHtml();
	echo $form->getHtml();
	echo $table->getHtml();
	echo "<div id='jurnal_06a_info'></div>";
	echo addJS("framework/smis/js/table_action.js");
	echo addJS("base-js/smis-base-loading.js");
	echo addJS("framework/bootstrap/js/bootstrap-datetimepicker.js");
	echo addCSS("framework/bootstrap/css/bootstrap-datetimepicker.css");
	echo addJS("kasir/jurnal_2016/scripts/jurnal_06a_dokter_action.js", false);
	echo addJS("kasir/jurnal_2016/scripts/jurnal_06a_action.js", false);
	echo addJS("kasir/jurnal_2016/scripts/jurnal_06a_main.js", false);
?>