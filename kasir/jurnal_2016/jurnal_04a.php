<?php
	require_once("smis-base/smis-include-service-consumer.php");
	require_once("kasir/jurnal_2016/classes/OperatorDBResponder.php");
	global $db;
	
	$form = new Form("", "", "Jurnal 04A : Rekap Penjualan Obat Pasien Umum");
	$operator_text = new Text("jurnal_04a_operator", "jurnal_04a_operator", "");
	$operator_text->setClass("smis-one-option-input");
	$operator_text->setAtribute("disabled='disabled'");
	$browse_button = new Button("", "", "Pilih");
	$browse_button->setClass("btn-info");
	$browse_button->setIsButton(Button::$ICONIC);
	$browse_button->setIcon("icon-white ".Button::$icon_list_alt);
	$browse_button->setAction("jurnal_04a_operator.chooser('jurnal_04a_operator', 'jurnal_04a_operator_button', 'jurnal_04a_operator', jurnal_04a_operator)");
	$browse_button->setAtribute("id='jurnal_04a_operator_browse'");
	$input_group = new InputGroup("");
	$input_group->addComponent($operator_text);
	$input_group->addComponent($browse_button);
	$form->addELement("Operator", $input_group);
	$from_date_text = new Text("jurnal_04a_from", "jurnal_04a_from", date("Y-m-d") . " 00:00");
	$from_date_text->setClass("mydatetime");
	$from_date_text->setAtribute("data-date-format='yyyy-mm-dd hh:ii'");
	$form->addELement("Waktu Awal", $from_date_text);
	$to_date_text = new Text("jurnal_04a_to", "jurnal_04a_to", date("Y-m-d H:i"));
	$to_date_text->setClass("mydatetime");
	$to_date_text->setAtribute("data-date-format='yyyy-mm-dd hh:ii'");
	$form->addELement("Waktu Akhir", $to_date_text);
	$uri_hidden = new Hidden("jurnal_04a_uri", "jurnal_04a_uri", "1");
	$form->addELement("", $uri_hidden);
	$jenis_pasien_hidden = new Hidden("jurnal_04a_jenis_pasien", "jurnal_04a_jenis_pasien", "umum");
	$form->addELement("", $jenis_pasien_hidden);
	$view_button = new Button("", "", "Lihat");
	$view_button->setIsButton(Button::$ICONIC);
	$view_button->setClass("btn-info");
	$view_button->setIcon("icon-white icon-repeat");
	$view_button->setAction("jurnal_04a.view()");
	$export_button = new Button("", "", "Ekspor Berkas Excel");
	$export_button->setIsButton(Button::$ICONIC);
	$export_button->setClass("btn-inverse");
	$export_button->setIcon("fa fa-download");
	$export_button->setAtribute("id='jurnal_04a_export_button'");
	$button_group = new ButtonGroup("");
	$button_group->addButton($view_button);
	$button_group->addButton($export_button);
	$form->addELement("", $button_group);
	
	$table = new Table(
		array(
			"No.", "No. Reg.", "No. RM", "Nama Pasien", "Tgl. Setoran", "No. BB / M", "Status", "Obat Ranap"
		),
		"",
		null,
		true
	);
	$table->setName("jurnal_04a");
	$table->setAction(false);
	$table->setFooterVisible(false);
	
	//Operator Chooser:
	$operator_table = new Table(
		array("No.", "Nama Operator"),
		"",
		null,
		true
	);
	$operator_table->setName("jurnal_04a_operator");
	$operator_table->setModel(Table::$SELECT);
	$operator_adapter = new SimpleAdapter(true, "No.");
	$operator_adapter->add("Nama Operator", "operator");
	$operator_dbtable = new DBTable($db, "smis_ksr_bayar");
	$filter = "";
	if (isset($_POST['kriteria'])) {
		$filter = " AND operator LIKE '%" . $_POST['kriteria'] . "%' ";
	}
	$query_value = "
		SELECT operator AS 'id', operator
		FROM (
			SELECT DISTINCT operator
			FROM smis_ksr_bayar
			WHERE prop NOT LIKE 'del' AND operator IS NOT NULL AND operator <> '' " . $filter . "
		) v_operator
	";
	$query_count = "
		SELECT COUNT(*)
		FROM (
			" . $query_value . "
		) v_operator
	";
	$operator_dbtable->setPreferredQuery(true, $query_value, $query_count);
	$operator_dbresponder = new OperatorDBResponder(
		$operator_dbtable,
		$operator_table,
		$operator_adapter
	);
	$super_command = new SuperCommand();
	$super_command->addResponder("jurnal_04a_operator", $operator_dbresponder);
	$init = $super_command->initialize();
	if ($init != null) {
		echo $init;
		return;
	}
	
	if (isset($_POST['command'])) {
		$command = $_POST['command'];
		require_once("kasir/jurnal_2016/commands/jurnal_04a_" . $command . ".php");
		return;
	}
	
	$loading_bar = new LoadingBar("jurnal_04a_loading_bar", "");
	$button = new Button("", "", "Batal");
	$button->addClass("btn-primary");
	$button->setIsButton(Button::$ICONIC_TEXT);
	$button->setIcon("fa fa-close");
	$button->setAction("jurnal_04a.cancel()");
	$loading_modal = new Modal("jurnal_04a_modal", "", "Proses..");
	$loading_modal->addHtml($loading_bar->getHtml(), "after");
	$loading_modal->addFooter($button);
	
	echo $loading_modal->getHtml();
	echo $form->getHtml();
	echo $table->getHtml();
	echo "<div id='jurnal_04a_info'></div>";
	echo addJS("framework/smis/js/table_action.js");
	echo addJS("base-js/smis-base-loading.js");
	echo addJS("framework/bootstrap/js/bootstrap-datetimepicker.js");
	echo addCSS("framework/bootstrap/css/bootstrap-datetimepicker.css");
	echo addJS("framework/bootstrap/js/bootstrap-datepicker.js");
	echo addCSS("framework/bootstrap/css/datepicker.css");
	echo addJS("kasir/jurnal_2016/scripts/jurnal_04a_operator_action.js", false);
	echo addJS("kasir/jurnal_2016/scripts/jurnal_04a_action.js", false);
	echo addJS("kasir/jurnal_2016/scripts/jurnal_04a_main.js", false);
?>