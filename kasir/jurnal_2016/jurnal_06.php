<?php
	require_once("smis-base/smis-include-service-consumer.php");
	global $db;
	
	$form = new Form("", "", "Jurnal 06 : Titipan Jasa Medis");
	$from_date_text = new Text("jurnal_06_from", "jurnal_06_from", date("Y-m-d") . " 00:00");
	$from_date_text->setClass("mydatetime");
	$from_date_text->setAtribute("data-date-format='yyyy-mm-dd hh:ii'");
	$form->addELement("Tanggal Awal", $from_date_text);
	$to_date_text = new Text("jurnal_06_to", "jurnal_06_to", date("Y-m-d H:i"));
	$to_date_text->setClass("mydatetime");
	$to_date_text->setAtribute("data-date-format='yyyy-mm-dd hh:ii'");
	$form->addELement("Tanggal Akhir", $to_date_text);
	$view_button = new Button("", "", "Lihat");
	$view_button->setIsButton(Button::$ICONIC);
	$view_button->setClass("btn-info");
	$view_button->setIcon("icon-white icon-repeat");
	$view_button->setAction("jurnal_06.view()");
	$export_button = new Button("", "", "Ekspor Berkas Excel");
	$export_button->setIsButton(Button::$ICONIC);
	$export_button->setClass("btn-inverse");
	$export_button->setIcon("fa fa-download");
	$export_button->setAtribute("id='jurnal_06_export_button'");
	$button_group = new ButtonGroup("");
	$button_group->addButton($view_button);
	$button_group->addButton($export_button);
	$form->addELement("", $button_group);
	
	$table = new Table(
		array(
			"No.", "Kode Akun", "Nama Akun", "Debet", "Kredit"
		),
		"",
		null,
		true
	);
	$table->setName("jurnal_06");
	$table->setAction(false);
	$table->setFooterVisible(false);
	
	if (isset($_POST['command'])) {
		$command = $_POST['command'];
		require_once("kasir/jurnal_2016/commands/jurnal_06_" . $command . ".php");
		return;
	}
	
	$loading_bar = new LoadingBar("jurnal_06_loading_bar", "");
	$button = new Button("", "", "Batal");
	$button->addClass("btn-primary");
	$button->setIsButton(Button::$ICONIC_TEXT);
	$button->setIcon("fa fa-close");
	$button->setAction("jurnal_06.cancel()");
	$loading_modal = new Modal("jurnal_06_modal", "", "Proses..");
	$loading_modal->addHtml($loading_bar->getHtml(), "after");
	$loading_modal->addFooter($button);
	
	echo $loading_modal->getHtml();
	echo $form->getHtml();
	echo $table->getHtml();
	echo "<div id='jurnal_06_info'></div>";
	echo addJS("framework/smis/js/table_action.js");
	echo addJS("base-js/smis-base-loading.js");
	echo addJS("framework/bootstrap/js/bootstrap-datetimepicker.js");
	echo addCSS("framework/bootstrap/css/bootstrap-datetimepicker.css");
	echo addJS("kasir/jurnal_2016/scripts/jurnal_06_action.js", false);
	echo addJS("kasir/jurnal_2016/scripts/jurnal_06_main.js", false);
?>