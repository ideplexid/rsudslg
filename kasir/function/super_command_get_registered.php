<?php 

/**
 * @brief this function used for getting super command of get_registered 
 *          using this function system would have fleksible 
 *          funcitonality.
 * @author : Nurul Huda
 * @copyright : goblooge@gmail.com
 * @since : 05 Apr 2017
 * @version : 1.0.0
 * @license : LGPLv3
 * @param String $command the super_command that send bay the requester 
 * @return  ServiceResponder
 */
function super_command_get_registered($command){
    global $db;
    require_once "smis-base/smis-include-service-consumer.php";
    $pheader=array("Nama","NRM","Alamat");
    $pasientable=new Table($pheader);
    $pasientable->setName($command);
    $pasientable->setModel(Table::$SELECT);
    $adapter=new SimpleAdapter();
    $adapter->add("Nama", "nama_pasien");
    $adapter->add("NRM", "nrm","digit8");
    $adapter->add("Alamat", "alamat_pasien");
    $pasien=new ServiceResponder($db, $pasientable, $adapter, "get_registered");
    return $pasien;
}


?>