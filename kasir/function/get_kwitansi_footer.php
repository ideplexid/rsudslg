<?php 

function get_kwitansi_footer(TablePrint $tfoot,$_setup){
    global $db;
    $total       = getSettings($db,"cashier-simple-kwitansi-space-footer","3");
    $total       = $total>3?$total:3;
    $is_penerima = getSettings($db,"cashier-simple-kwitansi-show-pembayar","0")=="1";
    $br = "";
    for($x=0;$x<$total;$x++){
        $br .= "</br>";
    }
    if($is_penerima){
        $tfoot ->addColumn(" Dibayar Oleh : ", 3, 1,NULL,NULL,"center")
               ->addColumn(" <div class='ngedit' contenteditable='true'>".$_setup['kota'].", ".ArrayAdapter::format("date d M Y", date("Y-M-d"))." </div>", 3, 1,NULL,NULL,"center")
               ->commit("body");
        $tfoot ->addSpace(3, 1)
               ->addColumn($br, 3, 1,NULL,NULL,"center")
               ->commit("body");
        $tfoot ->addColumn("______________", 3, 1,NULL,NULL,"center")
               ->addColumn($_setup['pencetak'], 3, 1,NULL,NULL,"center")
               ->commit("body");
    }else{
        $tfoot ->addSpace(3, 1)
               ->addColumn(" <div class='ngedit' contenteditable='true'>".$_setup['kota'].", ".ArrayAdapter::format("date d M Y", date("Y-M-d"))." </div>", 3, 1,NULL,NULL,"center")
               ->commit("body");
        $tfoot ->addSpace(3, 1)
               ->addColumn($br, 3, 1,NULL,NULL,"center")
               ->commit("body");
        $tfoot ->addSpace(3, 1)
               ->addColumn($_setup['pencetak'], 3, 1,NULL,NULL,"center")
               ->commit("body");
    }    
}

?>