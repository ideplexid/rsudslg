<?php 

function get_query_kwitansi($noreg,$urutan,$metode){
    $query="SELECT id,waktu,nilai, no_kwitansi 
            FROM smis_ksr_bayar 
            WHERE noreg_pasien='".$noreg."' 
            AND prop!='del'
            ".($metode!=""?"AND metode='".$metode."' ":" AND metode!='diskon' ")."
            ORDER BY ".$urutan;
    return $query; 
}

function get_system_tgl_pulang($noreg){
    require_once "kasir/function/get_last_validate_keluar.php";
    $keluar  = get_last_validate_keluar($noreg);
    return $keluar['waktu_keluar'];
}

function get_system_setup($db,$user,$noreg,$px,$metode=""){
    $history_dokter                = json_decode($px['history_dokter'],true);
    
    $result                        = array();
    $result['dpjp1']               = $px['nama_dokter'];
    $result['dpjp2']               = $history_dokter[0]['nama_dokter'];
    $result['tampilan_ruangan']    = getSettings($db, "cashier-simple-kwitansi-place", "0")=="1"?"ruangan_kasir":"ruangan";
    $result['kota']                = getSettings($db, "cashier-simple-kwitansi-town", "");
    $result['pencetak']            = $user->getNameOnly();
    $result['autonomous']          = getSettings($db, "smis_autonomous_name", "SMIS");
    $result['nama_rs']             = getSettings($db, "cashier-simple-kwitansi-rs", $result['autonomous']);
    $result['autonomous_address']  = getSettings($db, "smis_autonomous_address", "LOCALHOST");
    $result['alamat_rs']           = getSettings($db, "cashier-simple-kwitansi-address", $result['autonomous_address']);
    $result['tgl_pulang']          = ($px['tanggal_pulang']=="0000-00-00 00:00:00" || $px['tanggal_pulang']=="0000-00-00") ?get_system_tgl_pulang($noreg):$px['tanggal_pulang'];
    $result['tgl_masuk']           = $px['tanggal'];
    $result['kelamin']             = $px['kelamin']=="1"?"Perempuan":"Laki-Laki";
    $result['urutan']              = getSettings($db, "cashier-simple-kwitansi-arrange-bayar-last-show", "id DESC");
    $result['urutan_bayar']        = getSettings($db, "cashier-simple-kwitansi-arrange-bayar", "id DESC");
    $result['last_row']            = $db->get_row(get_query_kwitansi($noreg,$result['urutan'],$metode));
    $result['no_kwitansi']         = ArrayAdapter::format("only-digit8",$result['last_row']->id);
    $result['waktu_kwitansi']      = ArrayAdapter::format("date d M Y H:i",$result['last_row']->waktu);
    $result['nilai_kwitansi']      = $result['last_row']->nilai;
    $result['own_number']          = getSettings($db,"cashier-simple-kwitansi-use-own-number","id");
    $result['set-sum-bayar']       = getSettings($db, "cashier-simple-kwitansi-sum-bayar", "1");
    if($result['own_number']!="id"){
        $result['no_kwitansi']     = $result['last_row']->no_kwitansi;
    }
    return $result;
}



?>