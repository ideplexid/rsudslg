<?php 

function get_total_tagihan(Database $db,$noreg_pasien){
    $tt="SELECT SUM(total) as total 
         FROM smis_ksr_kolektif 
         WHERE noreg_pasien='".$noreg_pasien."' 
            AND akunting_only=0 
            AND prop!='del'";
    $result['total']    = $db->get_var($tt);

    $query="SELECT 
    SUM(IF(metode LIKE 'cash%',nilai,0)) as cash, 
    SUM(IF(metode LIKE 'bank',nilai,0)) as bank, 
    SUM(IF(metode LIKE 'asuransi',nilai,0)) as asuransi, 
    SUM(IF(metode LIKE 'diskon',nilai,0)) as diskon,
    GROUP_CONCAT(IF(metode LIKE 'diskon',keterangan,NULL)) as diskon_keterangan    
    FROM smis_ksr_bayar 
    WHERE noreg_pasien='".$noreg_pasien."' 
    AND prop!='del'";
    $bayar=$db->get_row($query);
    $result['asuransi'] = $bayar->asuransi;
    $result['bank']     = $bayar->bank;
    $result['cash']     = $bayar->cash;
    $result['diskon']   = $bayar->diskon;
    $result['diskon_keterangan']   = $bayar->diskon_keterangan;
    return $result;
}

function total_tagihan_non_administrasi_inap(Database $db,$noreg_pasien){
    $tt="SELECT SUM(total) as total 
         FROM smis_ksr_kolektif 
         WHERE noreg_pasien='".$noreg_pasien."' 
            AND akunting_only=0 
            AND jenis_tagihan!='administrasi'
            AND prop!='del'";
    $result['total']    = $db->get_var($tt);

    $administrasi = ($result['total']*1)/100;
    if($administrasi>150000){
        $administrasi = 150000;
    }


    $query="SELECT 
    SUM(IF(metode LIKE 'cash%',nilai,0)) as cash, 
    SUM(IF(metode LIKE 'bank',nilai,0)) as bank, 
    SUM(IF(metode LIKE 'asuransi',nilai,0)) as asuransi, 
    SUM(IF(metode LIKE 'diskon',nilai,0)) as diskon,
    GROUP_CONCAT(IF(metode LIKE 'diskon',keterangan,NULL)) as diskon_keterangan    
    FROM smis_ksr_bayar 
    WHERE noreg_pasien='".$noreg_pasien."' 
    AND prop!='del'";
    $bayar=$db->get_row($query);
    $result['asuransi']             = $bayar->asuransi;
    $result['bank']                 = $bayar->bank;
    $result['cash']                 = $bayar->cash;
    $result['diskon']               = $bayar->diskon;
    $result['diskon_keterangan']    = $bayar->diskon_keterangan;
    $result['administrasi']         = $administrasi;
    return $result;
}

?>