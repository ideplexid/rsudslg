<?php 

function get_patient_operasi($db,$noreg){
    require_once ("smis-base/smis-include-service-consumer.php");
    $result                   = array();
    $data['noreg_pasien']     = $noreg;
    $operasi                  = new ServiceConsumer ( $db, "get_operasi_by_noreg",$data,"medical_record" );
    $operasi->execute();
    $ctx                      = $operasi->getContent();
    $result['operasi_1']      = "-";
    $result['operasi_2']      = "-";
    $total                    = count($ctx);
    if($total>1){
        $result['operasi_1']  = $ctx[0]['icd_tindakan']." - ".$ctx[0]['ket_tindakan'];
    }
    if($total>2){
        $result['operasi_2']  = $ctx[1]['icd_tindakan']." - ".$ctx[1]['ket_tindakan'];
    }
    return $result;
}

?>