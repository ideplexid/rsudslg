<?php 

function get_transaksi_bayar(Database $db,$_noreg,$_setup){
    $dbtable    = new DBTable($db,"smis_ksr_bayar");
    $dbtable    ->addCustomKriteria("noreg_pasien","='".$_noreg."'")
                ->addCustomKriteria("metode","!='diskon'")
                ->setShowAll(true)
                ->setFetchMethode(DBTable::$ARRAY_FETCH)
                ->setOrder($_setup['urutan_bayar']);
    $dt         = $dbtable->view("","0");
    $list_bayar = $dt['data'];
    return $list_bayar;
}

function get_transaksi_bayar_metode(Database $db,$_noreg,$_setup,$metode){
    $list_bayar = array();
    $list       = get_transaksi_bayar($db,$_noreg,$_setup);
    foreach($list as $x){
        if($x['metode']==$metode){
            $list_bayar[] = $x;
        }
    }
    return $list_bayar;
}

function get_list_transaksi_bayar(TablePrint &$tbayar,$list_bayar,&$total_bayar,&$bayar_akhir,$_setup){
    global $db;
    $shownumber                     = getSettings($db,"cashier-simple-kwitansi-show-all-number-kwitansi","1")=="1";
    $total                          = count($list_bayar);
    $no                             = 1;
    foreach($list_bayar as $x){
        $total_bayar               += $x['nilai'];
        $_setup['no_kwitansi']      = ArrayAdapter::format("only-digit8",$x['id']);
        if($_setup['own_number']!="id"){
            $_setup['no_kwitansi']  = $x['no_kwitansi'];
        }
        if($no==$total){
            $nokwitansi             = $shownumber?" <strong>**</strong> &#09; [ ".$_setup['no_kwitansi']." ] - ":"";
            $bayar_akhir            = $x['nilai'];
            $tbayar                 ->addColumn($nokwitansi.$x['metode']." - ".$x['keterangan'],5,1,NULL,NULL,"")
                                    ->addColumn(ArrayAdapter::format("money Rp.",$x['nilai']),1,1,NULL,NULL,"bold");
        }else{
            $nokwitansi             = $shownumber?"  &#09; [ ".$_setup['no_kwitansi']." ] - ":"";
            $tbayar                 ->addColumn($nokwitansi.$x['metode']." - ".$x['keterangan'],5,1)
                                    ->addColumn(ArrayAdapter::format("money Rp.",$x['nilai']),1,1);
        }	
        $tbayar->commit("body");
        $no++;
    }
}

?>