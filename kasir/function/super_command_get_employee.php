<?php 

/**
 * @brief this function used for getting super command HRD system
 *          suign this methode system would flesible 
 * @author : Nurul Huda
 * @copyright : goblooge@gmail.com
 * @since : 05 Apr 2017
 * @version : 1.0.0
 * @license : LGPLv3
 * @param String $command the super_command that send bay the requester 
 * @param String $filter the employee filter
 * @return  ServiceResponder
 */
function super_command_get_employee($command,$filter){
    require_once "smis-base/smis-include-service-consumer.php";
    require_once 'smis-libs-hrd/EmployeeResponder.php';
    $pheader=array("Nama","Jabatan","NIP");
    $dktable=new Table($pheader);
    $dktable->setName($command);
    $dktable->setModel(Table::$SELECT);
    $adapter=new SimpleAdapter();
    $adapter->add("Nama", "nama");
    $adapter->add("Jabatan", "nama_jabatan");
    $adapter->add("NIP", "nip");
    $dokter=new EmployeeResponder($db, $dktable, $adapter, $filter);
    return $dokter;
}


?>