<?php 

function end_css_kwitansi($db){
    $font_size          = getSettings($db, "cashier-simple-kwitansi-font-size", "12px");
    $font_size_f20      = getSettings($db, "cashier-simple-kwitansi-font-size-f20", "12px");
    $font_letter_space  = getSettings($db, "cashier-simple-kwitansi-letter-space", "normal");
    $max_width          = getSettings($db,"cashier-simple-kwitansi-max-width","100%");

    $result  = "<style type='text/css'>";
    $result .= "table.kwitansi_print tr td.f20{font-size:".$font_size_f20." !important;}";
    $result .= "table.kwitansi_print tr td, table.kwitansi_print tr th{font-size:".$font_size." !important;}";
    $result .= "table.kwitansi_print tr td, table.kwitansi_print tr th {letter-spacing:".$font_letter_space." !important;}";
    $result .= "table.kwitansi_print tr td, table.kwitansi_print tr th {letter-spacing:".$font_letter_space." !important;}";
    $result .= "div#printing_area > table.kwitansi_print { max-width:".$max_width." !important;};";
    $result .= "</style>";    
    return $result;
}

?>