<?php 

function get_kwitansi_terbilang(TablePrint &$tfoot,$total_bayar,$class="bold"){
    loadLibrary("smis-libs-function-math");
    $total_say  = numbertell($total_bayar);
    $tfoot ->addColumn("TERBILANG", 1, 1,NULL,NULL,"left  lbottom")
           ->addColumn(": ".$total_say." Rupiah", 5, 1,NULL,NULL,"left lbottom ".$class)
           ->commit("body");
}

?>