<?php 

function getRuanganDaftar(){
	global $db;
	require_once 'smis-base/smis-include-service-consumer.php';
	$urjip=new ServiceConsumer($db, "get_urjip",array());
	$urjip->setMode(ServiceConsumer::$MULTIPLE_MODE);
    $urjip->setCached(true,"get_urjip");
	$urjip->execute();
	$content=$urjip->getContent();
	$ruangan=array();
	foreach ($content as $autonomous=>$ruang){
		foreach($ruang as $nama_ruang=>$jip){
				$option=array();
				$option['value']=$nama_ruang;
                $ruangan[$nama_ruang]=isset($jip['name'])?$jip['name']:ArrayAdapter::format("unslug",$nama_ruang);
				$ruangan[]=$option;
		}

	}
	return $ruangan;
}

?>