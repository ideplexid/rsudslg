<?php 

function get_transaksi_kwitansi(Database $db, $_noreg){
    require_once "kasir/class/MapRuangan.php";

    $dbtable = new DBTable($db,"smis_ksr_kolektif");
    $dbtable ->setShowAll(true)
             ->setFetchMethode(DBTable::$ARRAY_FETCH)
             ->setOrder(" urutan ASC, nama_grup ASC , ruangan ASC, dari ASC, sampai ASC ")
             ->addCustomKriteria("noreg_pasien","='".$_noreg."'")
             ->addCustomKriteria("akunting_only","='0'");
    if(getSettings($db, "cashier-simple-kwitansi-per-kwitansi", "0")=="1"){
        $dbtable->addCustomKriteria("id_kwitansi","='0'");	//jika kwitansi sendiri - sendiri
    }

    $dt             = $dbtable->view("","0");
    $list_tagihan   = $dt['data'];
    $result         = array();
    foreach($list_tagihan as $x){
        $x['ruangan'] = MapRuangan::getRealName($x['ruangan']);
        $x['ruangan_map'] = MapRuangan::getRealName($x['ruangan_map']);
        $x['ruangan_kasir'] = MapRuangan::getRealName($x['ruangan_kasir']);
        $result[] = $x;
    }
    return $result;
}

function get_transaksi_kwitansi_grup(Database $db, $_noreg){
    $qv=" SELECT id,urutan,nama_grup,
          jenis_tagihan, tanggal,
          if(jenis_tagihan LIKE 'tindakan%' AND nama_tagihan LIKE '% - %',substring_index(nama_tagihan,' - ',-1), nama_tagihan)  as vnama_tagihan,
          ruangan, ruangan_kasir, dari,sampai,
          sum(quantity) as quantity, 
          sum(total) as total FROM smis_ksr_kolektif 
          WHERE noreg_pasien='".$_noreg."' 
          AND prop!='del'
          AND akunting_only=0
          GROUP BY ruangan, vnama_tagihan
          ORDER BY ruangan, jenis_tagihan ";
    $list_tagihan=$db->get_result($qv,false);
    return $list_tagihan;
}

function get_transaksi_kwitansi_grup_bayangan(Database $db, $noreg){
    $qv="SELECT id,urutan,nama_grup,
         jenis_tagihan, tanggal,
         ruangan_map, dari,sampai,
         sum(quantity) as quantity, 
         sum(nilai_by) as total FROM smis_ksr_kolektif 
         WHERE noreg_pasien='".$noreg."' 
         AND akunting_only=0
         AND prop!='del'
         GROUP BY ruangan_map,nama_grup
         ORDER BY ruangan_map,nama_grup ";
    $list_tagihan=$db->get_result($qv,false);
    return $list_tagihan;
}

?>