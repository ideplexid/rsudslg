<?php 

function get_patient_diagnosa($db,$noreg,$px){
    require_once ("smis-base/smis-include-service-consumer.php");
    $result                             = array();
    $data['noreg_pasien']               = $noreg;
    $diagnosa                           = new ServiceConsumer ( $db, "get_diagnosa_by_noreg",$data,"medical_record" );
    $diagnosa->execute();
    $ctx                                = $diagnosa->getContent();
    
    $total=count($ctx);//kode_icd,nama_icd
    if($px['inacbg_bpjs']!=""){
        $result['diagnosa_akhir']       = $px['inacbg_bpjs']." - ".$px['deskripsi_bpjs'];
    }else{
        $result['diagnosa_akhir']       = $ctx[0]['kode_icd']." - ".$ctx[0]['nama_icd'];
    }
    $result['diagnosa_penyerta_1']      = "-";
    $result['diagnosa_penyerta_2']      = "-";
    $result['diagnosa_awal']            = $ctx[$total-1]['kode_icd']." - ".$ctx[$total-1]['nama_icd'];
    if($total>2){
        $result['diagnosa_penyerta_1']  = $ctx[1]['kode_icd']." - ".$ctx[1]['nama_icd'];
    }
    if($total>3){
        $result['diagnosa_penyerta_2']  = $ctx[2]['kode_icd']." - ".$ctx[2]['nama_icd'];
    }
    return $result;
}

?>