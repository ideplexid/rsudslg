<?php

function medical_carapulang_valuex($cara){
    $carapulang[""]											= -1;
    $carapulang["Tidak Datang"]								= 0;
    $carapulang["Dipulangkan Hidup"]						= 2;
    $carapulang["Dipulangkan Mati < 1 Jam Post Operasi"]	= 3;
    $carapulang["Dipulangkan Mati <=24 Jam"]				= 4;
    $carapulang["Dipulangkan Mati <=48 Jam"]				= 5;
    $carapulang["Dipulangkan Mati >48 Jam"]					= 6;
    $carapulang["Pulang Paksa"]								= 7;
    $carapulang["Kabur"]									= 6;
    $carapulang["Rawat Inap"]								= 1;
    $carapulang["Masuk IGD"]								= 1;
    $carapulang["Pindah Kamar"]								= 1;
    $carapulang["Rujuk Poli Lain"]							= 1;
    $carapulang["Pindah RS Lain"]							= 4;
    $carapulang["Pindah ke RS Lain"]					    = 4;
    $carapulang["Dikembalikan ke Perujuk"]					= 4;
    $carapulang["Dirujuk"]					                = 4;
    return $carapulang[$cara];
}

function get_last_validate_keluar($noreg){
    global $db;
    /** mengambil data pulang dan waktu pulang terakhir */
    require_once ("smis-base/smis-include-service-consumer.php");
    //loadLibrary("smis-libs-function-medical");
    $serv    = new ServiceConsumer($db,"get_status_pulang");
    $serv    ->setMode(ServiceConsumer::$CLEAN_BOTH_JOIN_SINGLE_ARRAY);
    $serv    ->addData("noreg_pasien",$noreg);
    $serv    ->execute();
    $content = $serv->getContent();
    $plng    = "";
    $lstwktu = "";
    foreach($content as $x){
        $bobot_pulang = medical_carapulang_valuex($plng);
        $bobot_baru   = medical_carapulang_valuex($x['cara_keluar']);
        if($x['cara_keluar']=="" || $x['waktu_keluar']==""){
            continue;
        }
        if( $x['cara_keluar']!="" &&  ( $plng=="" || $bobot_pulang<$bobot_baru || ( $bobot_pulang=$bobot_baru && $lstwktu<$x['waktu_keluar']) ) ){
            $plng     = $x['cara_keluar'];    
            //echo "DIambil Cara Pulang >> ".$plng."</br>";   
        }
        if( $x['waktu_keluar']!="" &&  ($lstwktu=="" || $lstwktu<$x['waktu_keluar']) ){
            $lstwktu  = $x['waktu_keluar'];       
            //echo "DIambil Wakty Pulang >> ".$lstwktu."</br>";   
        }
    }
    if($lstwktu==""){
        $lstwktu = date("Y-m-d H:i:s");
    }
    $result['waktu_keluar'] = $lstwktu;
    $result['cara_keluar']  = $plng;    
    return $result;
}