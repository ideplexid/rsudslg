<?php 
function kwitansi_bodong_pdf(Database $db, $nomor, $customer, $money, $keterangan,$username,$tgl,$jabatan,$cetakan_ke,$user_ke,$tanggal_ke){
    require('smis-libs-out/fpdf/fpdf.php');
    $width      = 210;
    $font       = "Times";
    $pdf        = new FPDF('L','mm','A5');
    $pdf        ->AddPage();
    $pdf        ->SetMargins(0);
    $town    	= getSettings($db,"smis_autonomous_town","");
    $nama    	= getSettings($db,"smis_autonomous_title","");
    $alamat  	= getSettings($db,"smis_autonomous_address","");
    $phone   	= getSettings($db,"smis_autonomous_contact","");

    loadLibrary("smis-libs-function-math");
    $nilai      = trim(numbertell($money)." Rupiah");
    loadLibrary("smis-libs-function-string");
    $rnilai     = explodeLessThan($nilai,50);
    $ket        = explodeLessThan($keterangan,50);
    $tanggal    = ArrayAdapter::format("date d M Y",$tgl);
    $liner      = "   _____________________________________________________________________________________________  ";
    /** logo perusahaan atau model perusahaan untuk framework */
    try{
        $pdf ->Image(getLogoNonInterlaced(),10,10,20,0,'');    
    }catch(Exception $e){

    }
    
    $pdf ->Ln(0);
    $pdf ->SetFont($font,'',12);
    $pdf ->Cell(0.2*$width,5,"",0);
    $pdf ->Cell(0.5*$width,5,$nama,0,0,'L');
    $pdf ->SetFont($font,'BU',15);
    $pdf ->Cell(0.3*$width,5,"KWITANSI",0,0,"L");

    $pdf ->Ln(5);
    $pdf ->SetFont($font,'',12);
    $pdf ->Cell(0.2*$width,5,"",0);
    $pdf ->Cell(0.5*$width,5,$alamat,0,0,'L');
    $pdf ->SetFont($font,'BI',15);
    $pdf ->Cell(0.3*$width,5,"RECEIPT",0,0,"L");

    $pdf ->Ln(5);
    $pdf ->SetFont($font,'',12);
    $pdf ->Cell(0.2*$width,5,"",0);
    $pdf ->Cell(0.5*$width,5,$phone,0,0,'L');

    $pdf ->Ln(5);
    $pdf ->SetFont($font,'BU',12);
    $pdf ->Cell(0.7*$width,5,"",0);
    $pdf ->Cell(0.08*$width,5,"No.         ",0,0,'L');
    $pdf ->SetFont($font,'B',12);
    $pdf ->Cell(0.02*$width,5," : ",0,0,'L');
    $pdf ->Cell(0.12*$width,5,$nomor,0,0,'L');

    $pdf ->Ln(5);
    $pdf ->SetFont($font,'BI',12);
    $pdf ->Cell(0.7*$width,5,"",0);
    $pdf ->Cell(0.15*$width,5,"Number",0,0,'L');

    $pdf ->Ln(5);
    $pdf ->SetFont($font,'',12);
    $pdf ->Cell($width,5,$liner,0);

    /**customer penerima */
    $pdf ->Ln(5);
    $pdf ->SetFont($font,'U',12);
    $pdf ->Cell(0.02*$width,5,"",0);
    $pdf ->Cell(0.16*$width,5,"Sudah Terima Dari",0);
    $pdf ->SetFont($font,'',12);
    $pdf ->Cell(0.02*$width,5," : ",0);
    $pdf ->Cell(0.8*$width,5,$customer,0,0,'L');
    $pdf ->Ln(5);
    $pdf ->SetFont($font,'I',12);
    $pdf ->Cell(0.02*$width,5,"",0);
    $pdf ->Cell(0.17*$width,5,"Received From",0);

    /**nilai uang yang dikeluarkan */
    $pdf ->Ln(10);
    $pdf ->SetFont($font,'U',12);
    $pdf ->Cell(0.02*$width,5,"",0);
    $pdf ->Cell(0.16*$width,5,"Banyaknya Uang",0);
    $pdf ->SetFont($font,'',12);
    $pdf ->Cell(0.02*$width,5," : ",0);
    $pdf ->Ln(5);
    $pdf ->SetFont($font,'I',12);
    $pdf ->Cell(0.02*$width,5,"",0);
    $pdf ->Cell(0.17*$width,5,"Amount Received",0);
    $pdf ->Ln(-5);
    foreach($rnilai as $x){
        $pdf ->SetFont($font,'',12);
        $pdf ->Cell(0.2*$width,5,"",0);
        $pdf ->Cell(0.8*$width,5,$x,0,0,'L');
        $pdf ->Ln(5);
    }

    /**keterangan untuk pembayaran */
    $pdf ->Ln(2);
    $pdf ->SetFont($font,'',12);
    $pdf ->Cell($width,5,$liner,0);
    $pdf ->Ln(5);
    $pdf ->SetFont($font,'U',12);
    $pdf ->Cell(0.02*$width,5,"",0);
    $pdf ->Cell(0.16*$width,5,"Untuk Pembayaran",0);
    $pdf ->SetFont($font,'',12);
    $pdf ->Cell(0.02*$width,5," : ",0);
    $pdf ->Ln(5);
    $pdf ->SetFont($font,'I',12);
    $pdf ->Cell(0.02*$width,5,"",0);
    $pdf ->Cell(0.17*$width,5,"In Payment Of",0);
    $pdf ->Ln(-5);
    foreach($ket as $x){
        $pdf ->SetFont($font,'',12);
        $pdf ->Cell(0.2*$width,5,"",0);
        $pdf ->Cell(0.8*$width,5,$x,0,0,'L');
        $pdf ->Ln(5);
    }

    $pdf ->Ln(5);
    $pdf ->SetFont($font,'',12);
    $pdf ->Cell($width,5,$liner,0);
    
    /** drawing money */
    $line = "_";
    $gape = "";
    $moneyformat = "Rp. ".ArrayAdapter::format("only-money",$money)."";
    $len = 23;
    for($x=0;$x<$len;$x++){
        $gape.=" ";
        $line.="_";
    }
    $pdf ->Ln(10);
    $pdf ->SetFont($font,'BU',15);
    $pdf ->Cell(0.065*$width,5,"",0);
    $pdf ->Cell(0.305*$width,5,$line,0);
    $pdf ->Ln(5);
    $pdf ->SetFillColor(191,191,191);
    $pdf ->SetFont($font,'B',15);
    $pdf ->Cell(0.07*$width,5,"",0);
    $pdf ->Cell(0.3*$width,5,$gape,0,"","",true);
    $pdf ->Ln(5);
    $pdf ->SetFillColor(191,191,191);
    $pdf ->SetFont($font,'B',15);
    $pdf ->Cell(0.07*$width,5,"",0);
    $pdf ->Cell(0.3*$width,5,$moneyformat,0,0,"C",true);
    $pdf ->Ln(5);
    $pdf ->SetFillColor(191,191,191);
    $pdf ->SetFont($font,'B',15);
    $pdf ->Cell(0.07*$width,5,"",0);
    $pdf ->Cell(0.3*$width,5,$gape,0,"","",true);
    $pdf ->Ln(0.3);
    $pdf ->SetFont($font,'BU',15);
    $pdf ->Cell(0.065*$width,5,"",0);
    $pdf ->Cell(0.305*$width,5,$line,0);


    $pdf ->Ln(-18);
    $pdf ->SetFont($font,'B',12);
    $pdf ->Cell(0.5*$width,5,"",0);
    $pdf ->Cell(0.5*$width,5,$town.", ".$tanggal,0,0,"C");
    $pdf ->Ln(5);
    $pdf ->SetFont($font,'B',12);
    $pdf ->Cell(0.5*$width,5,"",0);
    $pdf ->Cell(0.5*$width,5,$jabatan,0,0,"C");
    $pdf ->Ln(16);
    $pdf ->SetFont($font,'B',12);
    $pdf ->Cell(0.5*$width,5,"",0);
    $pdf ->Cell(0.5*$width,5,$username,0,0,"C");
    
    $ftr = "      Cetakan Ke-".$cetakan_ke." oleh ".$user_ke." pada ".ArrayAdapter::format("date d-M-Y H:i:s",$tanggal_ke);
    $pdf ->Ln(5);
    $pdf ->SetFont($font,'',8);
    $pdf ->Cell($width,5,$ftr,0,0,"L");
    
    $pathfile = "smis-temp/".md5(date("ymdhisu"))."_kwitansi_".$nomor.".pdf";
    $pdf  ->Output($pathfile, "F");
    return $pathfile;
}

?>