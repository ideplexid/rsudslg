<?php 

function kwitansi_print_button($db,$name,$px){
    $btn = new Button("print_button_".$name, "", " Cetak ".(ucwords(strtolower(ArrayAdapter::slugFormat("unslug",$name)))));
    $btn ->addClass("btn btn-primary")
         ->setIcon(" fa fa-print ")
         ->setIsButton(BUtton::$ICONIC_TEXT)
         ->setAction("kwitansi_print('".$name."')");
    if( $px['uri']=="0" && getSettings($db,"cashier-simple-kwitansi-cek-print-rj","0")=="1" || $px['uri']=="1" && getSettings($db,"cashier-simple-kwitansi-cek-print-ri","0")=="1"){
        $btn->setAction("cek_kwitansi_print('".$name."')");
    }
    return $btn->getHtml();
}


?>