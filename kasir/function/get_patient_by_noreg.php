<?php 

function get_patient_by_noreg($db,$noreg_pasien){
    require_once ("smis-base/smis-include-service-consumer.php");
    $header     = array ();
    $adapter    = new SimpleAdapter ();
    $uitable    = new Table ( $header );
    $responder  = new ServiceResponder ( $db, $uitable, $adapter, "get_registered" );
    $responder  ->addData("id", $noreg_pasien);
    $responder  ->addData("command", "edit");
    $data       = $responder->command ( "edit" );
    $px         = $data['content'];
    return $px;
}

?>