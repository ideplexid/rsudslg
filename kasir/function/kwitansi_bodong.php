<?php

function kwitansi_bodong(Database $db, $nomor, $dari, $nilai, $keterangan,$user,$tanggal,$jabatan,$cetakan_ke,$user_ke,$tanggal_ke ){
    loadLibrary("smis-libs-function-math");
    $uang    = numbertell($nilai)." Rupiah"; 
    $town    = getSettings($db,"smis_autonomous_town","");
    $name    = getSettings($db,"smis_autonomous_title","");
    $address = getSettings($db,"smis_autonomous_address","");
    $phone   = getSettings($db,"smis_autonomous_contact","");
    $logo    = "<img src='".getLogo()."' class='logo' />";


    $table   = new TablePrint("kwitansi_bodong");
    $table   ->setDefaultBootrapClass(false);
    $table   ->addSpace(1,1)
             ->addColumn($logo,1,5)
             ->addColumn($name,8,1,null,null,"title c gap")
             ->addSpace(1,1)
             ->addColumn("<div>KWITANSI</div>",4,1,null,null,"utext jumbo")
             ->commit("body");
    $table   ->addSpace(1,1)
             ->addColumn($address,8,1,null,null,"title c")
             ->addSpace(1,1)
             ->addColumn("RECEIPT",4,1,null,null,"i jumbo")
             ->commit("body");
    $table   ->addSpace(1,1)
             ->addColumn($phone,8,1,null,null,"title c")
             ->addSpace(5,1)
             ->commit("body");
    $table   ->addSpace(1,1)
             ->addSpace(9,2)
             ->addColumn("<div>No.</div>",1,1,null,null,"u title")
             ->addColumn(" : ".$nomor,3,1,null,null,"title")
             ->commit("body");
    $table   ->addSpace(1,1)
             ->addColumn("Number",1,1,null,null,"i")
             ->addSpace(3,1)
             ->commit("body");
 
    /**space page - penerimaan dari*/
    $table   ->addSpace(15,1,"body","top");  
    $table   ->addSpace(1,1)
             ->addColumn("<div>Sudah Terima Dari</div>",3,1,null,null,"u")
             ->addColumn(":",1,1)
             ->addColumn($dari,10,2,null,null,"vtop")
             ->commit("body");
    $table   ->addSpace(1,1)
             ->addColumn("Received From",4,1,null,null,"i")
             ->commit("body");
    
    /**space page nilai rupiah*/
    for($x=1; $x<=15; $x++){
        $table  ->addSpace(1,1);
    }
    $table   ->commit("body");
    $table   ->addSpace(1,1)
             ->addColumn("<div>Banyaknya Uang</div>",3,1,null,null,"u")
             ->addColumn(":",1,1)
             ->addColumn($uang,10,2,null,null,"vtop")
             ->commit("body");
    $table   ->addSpace(1,1)
             ->addColumn("Amount Received",4,1,null,null,"i")
             ->commit("body");

    /**space page kegunaan*/
    $table   ->addSpace(15,1,"body","top");
    $table   ->commit("body");
    $table   ->addSpace(1,1)
             ->addColumn("<div>Untuk Pembayaran</div>",3,1,null,null,"u")
             ->addColumn(":",1,1)
             ->addColumn($keterangan,10,2,null,null,"vtop")
             ->commit("body");
    $table   ->addSpace(1,1)
             ->addColumn("In Payment Of",4,1,null,null,"i")
             ->commit("body");
    
    $table   ->addSpace(15,1,"body","bottom");  
   
    $table   ->addSpace(1,1)
             ->addColumn("<div>".ArrayAdapter::format("money Rp.",$nilai)."</div>",6,4,null,null,"money")
             ->addSpace(6,1)
             ->addColumn(ArrayAdapter::format("date d M Y",$tanggal).", ".$town,2,1,null,null,"c")
             ->commit("body");
    $table   ->addSpace(1,1)
             ->addSpace(6,1)
             ->addColumn($jabatan,2,1,null,null,"c")
             ->commit("body");
    $table   ->addSpace(1,1)
             ->addSpace(8,1)
             ->commit("body");
    $table   ->addSpace(1,1)
             ->addSpace(8,1)
             ->commit("body");
    $table   ->addSpace(1,1)
             ->addSpace(12,1)
             ->addColumn($user,2,1,null,null,"c")
             ->commit("body");
    $table   ->addSpace(1,1)
             ->addSpace(8,1)
             ->commit("body");
    $table   ->addSpace(1,1)
             ->addSpace(8,1)
             ->commit("body");
    $table   ->addSpace(1,1)
             ->addSpace(8,1)
             ->commit("body");
    $table   ->addColumn("Cetakan Ke-".$cetakan_ke." oleh ".$user_ke." pada ".ArrayAdapter::format("date d-M-Y H:i:s",$tanggal_ke),15,1,null,null,"lft")
             ->commit("body");
    return $table->getHtml();
}

?>