<?php 

    /**semua tagihan dijadikan satu, jumlah selalu dianggap satu */

    function sum_all_tagihan($tagihan){
        if($tagihan==null){
            return 0;
        }
        $total = 0; 
        foreach($tagihan as $x){
            $total += $x['total'];
        }
        
        return $total;
    }

    function sum_all_as_one($tagihan,$name){
        $total              = sum_all_tagihan($tagihan);
        $result             = array();
        $result['name']     = $name;
        $result['satuan']   = $total;
        $result['quantity'] = 1;
        $result['jumlah']   = $total;
        $all_res            = array();
        $all_res[]          = $result;
        return $$all_res;
    }

    /**semua tagihan dijadikan satu, jumlah selalu dianggap satu, tetapi dipecah di masing-masing ruang */
    function get_per_ruang_as_one($tagihan){
        $result = array();
        foreach($tagihan as $x){
             if(!$result[$x['ruangan_map']]){
                $result[$x['ruangan_map']]   = 0;
             }
             $result[$x['ruangan_map']]     += $x['total'];
        }

        $all_res                             = array();
        foreach($result as $ruangan=>$nilai){
            $all_res[] = array(
                'name'      => $ruangan,
                'satuan'    => $nilai,
                'jumlah'    => $nilai,
                'quantity'  => 1
            );
       }
       return $all_res;
    }

    /**semua tagihan dijumlah jadi satu per ruang, tetapi jumlah tidak dianggap selalu 1, melainkan mengikuti berapa quantity*/
    function get_all_per_ruang_as_total($tagihan){
        $result                                 = array();
        if($tagihan!=null){
            foreach($tagihan as $x){
                if(!$result[$x['ruangan_map']]){
                   $result[$x['ruangan_map']]   = array(
                       'name'      => $x['ruangan_map'],
                       'satuan'    => 0,
                       'jumlah'    => 0,
                       'quantity'  => 0
                   );
                }
                $result[$x['ruangan_map']]['jumlah']  += $x['quantity'];
                $result[$x['ruangan_map']]['total']   += $x['total'];
                $result[$x['ruangan_map']]['satuan']  += $result[$x['ruangan_map']]['total']/$result[$x['ruangan_map']]['jumlah'];
           }
        }
       return $result;
    }

    /**dikelokmpokan berdasarkan per nama, tetapi jumlah tidak dianggap selalu satu melainkan mengikuti quantity  */
    function get_all_per_nama_as_total($tagihan){
        $result                             = array();
        foreach($tagihan as $x){
             if(!$result[$x['nama_tagihan']]){
                $result[$x['nama_tagihan']]  = array(
                    'name'      => $x['nama_tagihan'],
                    'satuan'    => 0,
                    'jumlah'    => 0,
                    'quantity'  => 0
                );
             }
             $result[$x['nama_tagihan']]['jumlah']     += $x['total'];
             $result[$x['nama_tagihan']]['quantity']   += $x['quantity'];
             $result[$x['nama_tagihan']]['satuan']     += $result[$x['nama_tagihan']]['total']/$result[$x['nama_tagihan']]['jumlah'];
        }
       return $result;
    }

    /**berfungsi untuk menggabungkan antara dua detail tagihan */
    function join_tagihan($tagihan1,$tagihan2){
        $result                             = array();
        foreach($tagihan1 as $x){
             if(!$result[$x['nama']]){
                $result[$x['name']]  = array(
                    'name'      => $x['name'],
                    'satuan'    => 0,
                    'jumlah'    => 0,
                    'quantity'  => 1
                );
             }
             $result[$x['name']]['jumlah']  += $x['jumlah'];
             $result[$x['name']]['satuan']  += $x['jumlah'];
        }
        
        foreach($tagihan2 as $x){
            if(!$result[$x['name']]){
               $result[$x['name']]  = array(
                   'name'      => $x['name'],
                   'satuan'    => 0,
                   'jumlah'    => 0,
                   'quantity'  => 1
               );
            }
            $result[$x['name']]['jumlah']  += $x['jumlah'];
            $result[$x['name']]['satuan']  += $x['jumlah'];
       }
       return $result;
    }
?>