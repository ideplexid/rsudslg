<?php 

function get_header_kwitansi(TablePrint &$thead,$_setup,$_px,$header){
    global $db;
    $file       = getSettings($db,"cashier-simple-kwitansi-header-file","");
    $nokwit     = getSettings($db,"cashier-simple-kwitansi-show-no-kwitansi","1")=="1";
    $custitle   = getSettings($db,"cashier-simple-kwitansi-custom-title","");
    $is_alamat  = getSettings($db,"cashier-simple-kwitansi-header-alamat","0")=="1";
    
    if($file   != "" && file_exists(get_fileurl($file))){
        $width  = getSettings($db,"cashier-simple-kwitansi-header-file-width","100%");
        $height = getSettings($db,"cashier-simple-kwitansi-header-file-height","auto");
        $header = "<img src='".get_fileurl($file)."' style='width:".$width.";height:".$height.";'>";
    }
    $thead  ->addColumn($header, 6, 1,NULL,NULL,"center bold")
            ->commit("body");
    if($custitle!=""){
        $thead  ->addColumn($custitle, 6, 1,NULL,NULL,"center f20 bold subtitle")
                ->commit("body");
    }

    $thead  ->addColumn("NAMA / PJ", 1, 1,NULL,NULL,"left ltop")
            ->addColumn(" : ".$_px['nama_pasien']." , ".$_px['sebutan']." / ".$_px['namapenanggungjawab']." / ".$_setup['kelamin'], 2, 1,NULL,NULL,"left ltop ");
    if($nokwit){
        $thead  ->addColumn("No. Kwitansi", 2, 1,NULL,NULL,"left ltop f20 underlined")
                ->addColumn( " : ".$_setup['no_kwitansi'], 1, 1,NULL,NULL,"left ltop f20 bold")
                ->commit("body");
    }else{
        $thead  ->addColumn("", 2, 1,NULL,NULL,"left ltop f20 underlined")
                ->addColumn( "", 1, 1,NULL,NULL,"left ltop f20 bold")
                ->commit("body");
    }
    
            
    if($is_alamat){
        $alamat = $_px['alamat_pasien']." ".trim($_px['nama_kedusunan'])." ".($_px['nama_kelurahan'])."</br>  ".($_px['nama_kecamatan'])." ".($_px['nama_kabupaten']);
        $thead  ->addColumn("ALAMAT", 1, 1,NULL,NULL,"left")
                ->addColumn(" : ".trim($alamat), 2, 1,NULL,NULL,"left ");
    }else{
        $thead  ->addColumn("ALAMAT", 1, 1,NULL,NULL,"left")
                ->addColumn(" : ".$_px['alamat_pasien'], 2, 1,NULL,NULL,"left ");       
    }
    require_once("kasir/class/MapRuangan.php");
    $ruanganx = ($_px['uri']*1==0?$_px['jenislayanan']:$_px['kamar_inap']);
    $thead      ->addColumn("RUANGAN", 2, 1,NULL,NULL,"left")
                ->addColumn(" : ".MapRuangan::getRealName($ruanganx), 2, 1,NULL,NULL,"left ")
                ->commit("body");

    $thead  ->addColumn("NRM / NO. REG", 1, 1,NULL,NULL,"left")
            ->addColumn(" : ".ArrayAdapter::format("only-digit8", $_px['nrm'])." / ".ArrayAdapter::format("only-digit8", $_px['id']), 2, 1,NULL,NULL,"left")
            ->addColumn("TANGGAL MASUK", 2, 1,NULL,NULL,"")
            ->addColumn( " <div class='ngedit' contenteditable='true'> ".ArrayAdapter::format("date d M Y", $_setup['tgl_masuk'])."</div>", 1, 1,NULL,NULL,"left")
            ->commit("body");
    $thead  ->addColumn("JENIS PASIEN", 1, 1,NULL,NULL,"left")
            ->addColumn(" : ".ArrayAdapter::format("unslug", $_px['carabayar'] . ($_px['nama_asuransi']!=""?" / ".$_px['nama_asuransi']:"")  ), 2, 1,NULL,NULL,"left")
            ->addColumn("TANGGAL KELUAR", 2, 1,NULL,NULL,"left")
            ->addColumn( "<div class='ngedit' contenteditable='true'>".ArrayAdapter::format("date d M Y", $_setup['tgl_pulang'])."</div>", 1, 1,NULL,NULL,"left")
            ->commit("body");
    $thead  ->addColumn("DPJP", 1, 1,NULL,NULL,"left lbottom")
            ->addColumn(" : ".strtoupper($_px['nama_dokter']), 5, 1,NULL,NULL,"left lbottom")
            ->commit("body");
    
    $thead  ->addSpace(6, 1)
            ->commit("body");
}


?>