<?php 

function invoke_pengajuan_diskon($noreg,$total){
    global $db;
    if(getSettings($db,"cashier-enabled-diskon-invoker","0")=="1"){
        /*clear the current data*/
        $update['prop']                 = "del";
        $bykrit['metode']               = "diskon";
        $bykrit['noreg_pasien']         = $noreg;
        $bykrit['id_pengajuan_diskon']  = ">0";
        $warp['id_pengajuan_diskon']    = DBController::$NO_STRIP_ESCAPE_WRAP;        
        $bytable                        = new DBTable($db,"smis_ksr_bayar");
        $bytable                        ->update($update,$bykrit,$warp);
        
        
        /*get approved data*/
        $dbtable = new DBTable($db,"smis_ksr_diskon");
        $dbtable ->addCustomKriteria("noreg_pasien","='".$noreg."'");
        $dbtable ->addCustomKriteria("setuju","='1'");
        $dbtable ->setShowAll(true);
        $data    = $dbtable ->view("",0);
        $list    = $data['data'];
        foreach($list as $y){
            $persen = $y->persen*1;
            $nilai  = $y->nilai*1;
            if($nilai<0 && $persen<0){
                continue;
            }
            $ndata['noreg_pasien']          = $noreg;
            $ndata['nama_pasien']           = $y->nama_pasien;
            $ndata['nrm_pasien']            = $y->nrm_pasien;
            $ndata['waktu']                 = date("Y-m-d H:i:s");
            $ndata['metode']                = "diskon";
            $ndata['prop']                  = "";
            $ndata['id_pengajuan_diskon']   = $y->id;
            $ndata['keterangan']            = "Diskon diajukan oleh ".$y->operator." & disetujui oleh ".$y->pengecek." Pada Kode Pengajuan ".ArrayAdapter::format("only-digit8",$y->id);
            $ndata['operator']              = $y->operator;            
            $nid['id_pengajuan_diskon']     = $y->id;
            if($persen>0){
                $ndata['nilai'] = $persen*$total/100;
            }else{
                $ndata['nilai'] = $nilai;
            }
            $bytable->insertOrUpdate($ndata,$nid);
        }
    }
}

?>