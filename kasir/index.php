<?php
	global $db;
	global $user;
	require_once 'smis-libs-class/Policy.php';
	require_once 'smis-libs-inventory/policy.php';
	$inventory_policy = new InventoryPolicy("kasir", $user,"modul/");
	
	$policy=new Policy("kasir", $user);	
    $policy->addPolicy("mapping_akunting","mapping_akunting", Policy::$DEFAULT_COOKIE_CHANGE,"modul/mapping_akunting");
	$policy->addPolicy("kode_akun", "mapping_akunting", Policy::$DEFAULT_COOKIE_KEEP,"snippet/kode_akun");
	
    $policy->addPolicy("tagihan_non_pasien","tagihan_non_pasien", Policy::$DEFAULT_COOKIE_CHANGE,"modul/tagihan_non_pasien");
	$policy->addPolicy("cash_np","tagihan_non_pasien", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/tagihan_non_pasien/cash_np");
	$policy->addPolicy("bank_np","tagihan_non_pasien", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/tagihan_non_pasien/bank_np");
	$policy->addPolicy("asuransi_np","tagihan_non_pasien", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/tagihan_non_pasien/asuransi_np");
	
	$policy->addPolicy("settings", "settings", Policy::$DEFAULT_COOKIE_CHANGE,"modul/settings");
	$policy->addPolicy("help/user/crawler_mode", "settings", Policy::$DEFAULT_COOKIE_KEEP);
	
    $policy->addPolicy("pembayaran_patient", "pembayaran_patient", Policy::$DEFAULT_COOKIE_CHANGE,"modul/pembayaran_patient");
	$policy->addPolicy("list_registered", "pembayaran_patient", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/pembayaran_patient/list_registered");
	$policy->addPolicy("cash", "pembayaran_patient", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/pembayaran_patient/cash");
	$policy->addPolicy("bank", "pembayaran_patient", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/pembayaran_patient/bank");
	$policy->addPolicy("get_sisa_bayar", "pembayaran_patient", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/pembayaran_patient/get_sisa_bayar");
	$policy->addPolicy("bayangan", "pembayaran_patient", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/pembayaran_patient/bayangan");
	$policy->addPolicy("asuransi", "pembayaran_patient", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/pembayaran_patient/asuransi");
	$policy->addPolicy("diskon", "pembayaran_patient", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/pembayaran_patient/diskon");
	$policy->addPolicy("tagihan_backup_input", "pembayaran_patient", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/pembayaran_patient/tagihan_backup_input");
	$policy->addPolicy("edit_ok", "pembayaran_patient", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/pembayaran_patient/edit_ok");
	$policy->addPolicy("kwitansi_rumus", "pembayaran_patient", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/pembayaran_patient/kwitansi_rumus");
	$policy->addPolicy("kwitansi_rumus_detail", "pembayaran_patient", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/pembayaran_patient/kwitansi_rumus_detail");
	$policy->addPolicy("simple_kwitansi", "pembayaran_patient", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/pembayaran_patient/simple_kwitansi");
	$policy->addPolicy("simple_kwitansi_grup_alok", "pembayaran_patient", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/pembayaran_patient/simple_kwitansi_grup_alok");
	$policy->addPolicy("simple_kwitansi_total", "pembayaran_patient", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/pembayaran_patient/simple_kwitansi_total");
	$policy->addPolicy("simple_kwitansi_bayangan", "pembayaran_patient", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/pembayaran_patient/simple_kwitansi_bayangan");
	$policy->addPolicy("simple_kwitansi_bayangan_group", "pembayaran_patient", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/pembayaran_patient/simple_kwitansi_bayangan_group");
	$policy->addPolicy("simple_kwitansi_escp", "pembayaran_patient", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/pembayaran_patient/simple_kwitansi_escp");
	$policy->addPolicy("kwitansi_bank_jatim", "pembayaran_patient", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/pembayaran_patient/kwitansi_bank_jatim");
	$policy->addPolicy("kwitansi_rincian", "pembayaran_patient", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/pembayaran_patient/kwitansi_rincian");
	$policy->addPolicy("kwitansi_pasien", "pembayaran_patient", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/data_induk/kwitansi");
	$policy->addPolicy("get_registered_patient", "pembayaran_patient", Policy::$DEFAULT_COOKIE_KEEP,"snippet/get_registered_patient");
	$policy->addPolicy("get_employee", "pembayaran_patient", Policy::$DEFAULT_COOKIE_KEEP,"snippet/get_employee");
	$policy->addPolicy("edit_bed", "pembayaran_patient", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/pembayaran_patient/edit_bed");
	$policy->addPolicy("edit_tindakan_perawat", "pembayaran_patient", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/pembayaran_patient/edit_tindakan_perawat");
	$policy->addPolicy("edit_tindakan_igd", "pembayaran_patient", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/pembayaran_patient/edit_tindakan_igd");
	$policy->addPolicy("edit_tindakan_dokter", "pembayaran_patient", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/pembayaran_patient/edit_tindakan_dokter");
	$policy->addPolicy("edit_visit", "pembayaran_patient", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/pembayaran_patient/edit_visit");
	$policy->addPolicy("edit_konsul", "pembayaran_patient", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/pembayaran_patient/edit_konsul");
	$policy->addPolicy("edit_alok", "pembayaran_patient", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/pembayaran_patient/edit_alok");
	$policy->addPolicy("edit_alok_connected", "pembayaran_patient", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/pembayaran_patient/edit_alok_connected");
	$policy->addPolicy("edit_oksigen_central", "pembayaran_patient", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/pembayaran_patient/edit_oksigen_central");
	$policy->addPolicy("edit_oksigen_manual", "pembayaran_patient", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/pembayaran_patient/edit_oksigen_manual");
	$policy->addPolicy("edit_radiology", "pembayaran_patient", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/pembayaran_patient/edit_radiology");
	$policy->addPolicy("edit_laboratory", "pembayaran_patient", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/pembayaran_patient/edit_laboratory");
    $policy->addPolicy("edit_elektromedis", "pembayaran_patient", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/pembayaran_patient/edit_elektromedis");
    $policy->addPolicy("edit_mcu", "pembayaran_patient", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/pembayaran_patient/edit_mcu");
    $policy->addPolicy("set_tutup_tagihan", "pembayaran_patient", Policy::$DEFAULT_COOKIE_KEEP,"snippet/set_tutup_tagihan");
	
    $policy->addPolicy("klaim_pasien","klaim_pasien", Policy::$DEFAULT_COOKIE_CHANGE,"modul/klaim_pasien");
	$policy->addPolicy("update_push_accounting","klaim_pasien", Policy::$DEFAULT_COOKIE_CHANGE,"snippet/update_push_accounting");
	$policy->addPolicy("update_unpush_accounting","klaim_pasien", Policy::$DEFAULT_COOKIE_CHANGE,"snippet/update_unpush_accounting");
	    
	/* PEMBAYARAN RESEP */
	$policy->addPolicy("pembayaran_resep", "pembayaran_resep", Policy::$DEFAULT_COOKIE_CHANGE,"modul/pembayaran_resep");
	$policy->addPolicy("cash_resep", "pembayaran_resep", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/pembayaran_resep/cash_resep");
	$policy->addPolicy("bank_resep", "pembayaran_resep", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/pembayaran_resep/bank_resep");
	$policy->addPolicy("asuransi_resep", "pembayaran_resep", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/pembayaran_resep/asuransi_resep");
	$policy->addPolicy("kwitansi_resep", "pembayaran_resep", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/data_induk/kwitansi");
	
    /* GRUP MENU LAPORAN */
    $policy->addPolicy("laporan", "laporan", Policy::$DEFAULT_COOKIE_CHANGE,"modul/laporan");
    $policy->addPolicy("lap_cash", "laporan", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/laporan/lap_cash");
	$policy->addPolicy("lap_cash_resep_non_pasien", "laporan", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/laporan/lap_cash_resep_non_pasien");
	$policy->addPolicy("lap_cash_per_nomor", "laporan", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/laporan/lap_cash_per_nomor");
	$policy->addPolicy("lap_bank", "laporan", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/laporan/lap_bank");
	$policy->addPolicy("lap_asuransi", "laporan", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/laporan/lap_asuransi");
	$policy->addPolicy("lap_bpjs", "laporan", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/laporan/lap_bpjs");
	$policy->addPolicy("lap_perusahaan", "laporan", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/laporan/lap_perusahaan");
	$policy->addPolicy("laporan_karcis", "laporan", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/laporan/laporan_karcis");
    $policy->addPolicy("lap_non_pasien", "laporan", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/laporan/lap_non_pasien");
	$policy->addPolicy("lap_non_pasien_per_nomor", "laporan", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/laporan/lap_non_pasien_per_nomor");
	$policy->addPolicy("laporan_tahunan_lain_lain","laporan", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/laporan/laporan_tahunan_lain_lain");
	$policy->addPolicy("laporan_tahunan_karcis","laporan", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/laporan/laporan_tahunan_karcis");
	$policy->addPolicy("laporan_tahunan_ambulance","laporan", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/laporan/laporan_tahunan_ambulance");
	$policy->addPolicy("laporan_tahunan_kamar_mayat","laporan", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/laporan/laporan_tahunan_kamar_mayat");
	$policy->addPolicy("laporan_tahunan_obat","laporan", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/laporan/laporan_tahunan_obat");
	$policy->addPolicy("laporan_tahunan_ruangan","laporan", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/laporan/laporan_tahunan_ruangan");
	$policy->addPolicy("laporan_piutang_per_jenis","laporan", Policy::$DEFAULT_COOKIE_CHANGE,"resource/php/laporan/laporan_piutang_per_jenis");
	$policy->addPolicy("piutang_per_ruang","laporan", Policy::$DEFAULT_COOKIE_CHANGE,"resource/php/laporan/piutang_per_ruang");
	$policy->addPolicy("piutang_per_grup_ruang","laporan", Policy::$DEFAULT_COOKIE_CHANGE,"resource/php/laporan/piutang_per_grup_ruang");
	$policy->addPolicy("rekap_tagihan_pasien", "laporan", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/laporan/rekap_tagihan_pasien");
    $policy->addPolicy("rekap_tagihan_pasien_total", "laporan", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/laporan/rekap_tagihan_pasien_total");
    $policy->addPolicy("rekap_diskon_pasien", "laporan", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/laporan/rekap_diskon_pasien");
	$policy->addPolicy("laporan_uang_masuk","laporan", Policy::$DEFAULT_COOKIE_CHANGE,"resource/php/laporan/laporan_uang_masuk");
	$policy->addPolicy("total_tagihan_kasir","laporan",Policy::$DEFAULT_COOKIE_KEEP,"snippet/total_tagihan_kasir");
	$policy->addPolicy("koreksi_carabayar","laporan",Policy::$DEFAULT_COOKIE_KEEP,"snippet/koreksi_carabayar");
	$policy->addPolicy("lap_kartu", "laporan", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/laporan/lap_kartu");
	$policy->addPolicy("push_to_accounting", "laporan", Policy::$DEFAULT_COOKIE_KEEP,"snippet/push_to_accounting");
	$policy->addPolicy("push_to_lunas", "laporan", Policy::$DEFAULT_COOKIE_KEEP,"snippet/push_to_lunas");
	$policy->addPolicy("push_cash_resep_np_to_accounting", "laporan", Policy::$DEFAULT_COOKIE_KEEP,"snippet/push_cash_resep_np_to_accounting");
	
    /*GRUP MENU JURNAL*/
    $policy->addPolicy("jurnal", "jurnal", Policy::$DEFAULT_COOKIE_CHANGE,"modul/jurnal");
	$policy->addPolicy("laporan_pendapatan_dokter_spesialis", "jurnal", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/jurnal/laporan_pendapatan_dokter_spesialis");
	$policy->addPolicy("laporan_rekap_pendapatan_per_pasien_rawat_jalan", "jurnal", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/jurnal/laporan_rekap_pendapatan_per_pasien_rawat_jalan");
	$policy->addPolicy("laporan_rekap_pendapatan_per_pasien_spesialis", "jurnal", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/jurnal/laporan_rekap_pendapatan_per_pasien_spesialis");
	$policy->addPolicy("laporan_rekap_pendapatan_per_pasien_rawat_inap", "jurnal", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/jurnal/laporan_rekap_pendapatan_per_pasien_rawat_inap");
	$policy->addPolicy("laporan_pendapatan_spesialis", "jurnal", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/jurnal/laporan_pendapatan_spesialis");
	
    /*GRUP DATA INDUK*/
    $policy->addPolicy("data_induk", "data_induk", Policy::$DEFAULT_COOKIE_CHANGE,"modul/data_induk");
	$policy->addPolicy("mbank", "data_induk", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/data_induk/mbank");
	$policy->addPolicy("master_non_pasien", "data_induk", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/data_induk/master_non_pasien");
	$policy->addPolicy("pengaturan_nomor_kwitansi", "data_induk", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/data_induk/pengaturan_nomor_kwitansi");
	$policy->addPolicy("pasien_lewat_tanggal", "data_induk", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/data_induk/pasien_lewat_tanggal");
	$policy->addPolicy("kwitansi", "data_induk", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/data_induk/kwitansi");
	$policy->addPolicy("rumus", "data_induk", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/data_induk/rumus");
	$policy->addPolicy("master_kwitansi_rumus", "data_induk", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/data_induk/master_kwitansi_rumus");
	$policy->addPolicy("detail_master_kwitansi_rumus", "data_induk", Policy::$DEFAULT_COOKIE_KEEP,"snippet/detail_master_kwitansi_rumus");
	
    /*GRUP TAMBAHAN BIAYA*/
    $policy->addPolicy("tambahan_biaya_pasien", "tambahan_biaya_pasien", Policy::$DEFAULT_COOKIE_CHANGE,"modul/tambahan_biaya_pasien");
	$policy->addPolicy("tagihan_backup", "tambahan_biaya_pasien", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/tambahan_biaya_pasien/tagihan_backup");
	$policy->addPolicy("periksa_dokter", "tambahan_biaya_pasien", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/tambahan_biaya_pasien/periksa_dokter");
	$policy->addPolicy("get_tarif_periksa", "tambahan_biaya_pasien", Policy::$DEFAULT_COOKIE_KEEP,"snippet/get_tarif_periksa");
	$policy->addPolicy("rekap_tambahan_biaya", "tambahan_biaya_pasien", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/tambahan_biaya_pasien/rekap_tambahan_biaya");
	$policy->addPolicy("edit_tagihan_bayangan", "tambahan_biaya_pasien", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/tambahan_biaya_pasien/edit_tagihan_bayangan");
	$policy->addPolicy("kwitansi_tagihan_bayangan", "tambahan_biaya_pasien", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/tambahan_biaya_pasien/kwitansi_tagihan_bayangan");
	$policy->addPolicy("urutan_kwitansi", "tambahan_biaya_pasien", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/tambahan_biaya_pasien/urutan_kwitansi");
    $policy->addPolicy("master_tagihan_tambahan", "tambahan_biaya_pasien", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/tambahan_biaya_pasien/master_tagihan_tambahan");
    $policy->addPolicy("persetujuan_diskon", "tambahan_biaya_pasien", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/tambahan_biaya_pasien/persetujuan_diskon");
    $policy->addPolicy("surat_sakit", "tambahan_biaya_pasien", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/tambahan_biaya_pasien/surat_sakit");
    
    /*GRUP JURNAL 2016*/
    $policy->addPolicy("jurnal_2016", "jurnal_2016", Policy::$DEFAULT_COOKIE_CHANGE,"modul/jurnal_2016");
	$policy->addPolicy("jurnal_2016/jurnal_03a", "jurnal_2016", Policy::$DEFAULT_COOKIE_KEEP);
	$policy->addPolicy("jurnal_2016/jurnal_04", "jurnal_2016", Policy::$DEFAULT_COOKIE_KEEP);
	$policy->addPolicy("jurnal_2016/jurnal_04a", "jurnal_2016", Policy::$DEFAULT_COOKIE_KEEP);
	$policy->addPolicy("jurnal_2016/jurnal_05", "jurnal_2016", Policy::$DEFAULT_COOKIE_KEEP);
	$policy->addPolicy("jurnal_2016/jurnal_06", "jurnal_2016", Policy::$DEFAULT_COOKIE_KEEP);
	$policy->addPolicy("jurnal_2016/jurnal_06a", "jurnal_2016", Policy::$DEFAULT_COOKIE_KEEP);
	$policy->addPolicy("jurnal_2016/jurnal_07", "jurnal_2016", Policy::$DEFAULT_COOKIE_KEEP);
	$policy->addPolicy("jurnal_2016/jurnal_07_2", "jurnal_2016", Policy::$DEFAULT_COOKIE_KEEP);
	$policy->addPolicy("jurnal_2016/jurnal_07a", "jurnal_2016", Policy::$DEFAULT_COOKIE_KEEP);
	$policy->addPolicy("jurnal_2016/jurnal_dbf", "jurnal_2016", Policy::$DEFAULT_COOKIE_KEEP);
	
    $policy->addAlias("persetujuan_diskon", "acc_diskon");
	$policy->combinePolicy($inventory_policy);
	$policy->initialize();
?>
