<?php 
global $wpdb;
$query="create or replace view smis_vpiutang as 
    SELECT ruangan, 
    sum(if(layanan='radiology',nilai,0)) as radiology ,
    sum(if(layanan='laboratory',nilai,0)) as laboratory, 
    sum(if(layanan='fisiotherapy',nilai,0)) as fisiotherapy, 
    sum(if(layanan='darah',nilai,0)) as darah ,
    sum(if(layanan='ambulan',nilai,0)) as ambulan, 
    sum(if(layanan='gizi',nilai,0)) as gizi ,
    sum(if(layanan='tindakan_perawat',nilai,0)) as tindakan_perawat ,
    sum(if(layanan='tindakan_dokter',nilai,0)) as tindakan_dokter ,
    sum(if(layanan='konsul',nilai,0)) as konsul ,
    sum(if(layanan='visite',nilai,0)) as visite ,
    sum(if(layanan='periksa',nilai,0)) as periksa ,
    sum(if(layanan='bank_darah',nilai,0)) as bank_darah ,
    sum(if(layanan='kamar_mayat',nilai,0)) as kamar_mayat ,
    sum(if(layanan='bed',nilai,0)) as bed ,
    sum(if(layanan='penjualan_resep',nilai,0)) as penjualan_resep ,
    sum(if(layanan='return_resep',nilai,0)) as return_resep ,
    sum(if(layanan='vk',nilai,0)) as vk ,
    sum(if(layanan='operasi',nilai,0)) as operasi ,
    sum(if(layanan='recovery_room',nilai,0)) as recovery_room ,
    sum(if(layanan='alat_obat',nilai,0)) as alat_obat ,
    sum(if(layanan='bronchoscopy',nilai,0)) as bronchoscopy ,
    sum(if(layanan='spirometry' ,nilai,0)) as spirometry ,
    sum(if(layanan='audiometry',nilai,0)) as audiometry ,
    sum(if(layanan='oksigen_central',nilai,0)) as oksigen_central ,
    sum(if(layanan='oksigen_manual',nilai,0)) as oksigen_manual ,
    sum(if(layanan='endoscopy',nilai,0)) as endoscopy ,
    sum(if(layanan='ekg',nilai,0)) as ekg ,
    sum(if(layanan='administrasi',nilai,0)) as administrasi ,
    sum(if(layanan='karcis',nilai,0)) as karcis,
    sum(nilai) as total,
    '' as prop
    FROM smis_ksr_piutang where prop!='del' group by ruangan";
$wpdb->query ( $query );

?>