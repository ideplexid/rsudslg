<?php 
    global $wpdb;
    require_once "smis-libs-class/DBCreator.php";
    $dbcreator=new DBCreator($wpdb,"smist_ksr_rekap_total");
    $dbcreator->addColumn("nama_pasien","varchar(64)",DBCreator::$SIGN_NONE,false,DBCreator::$DEFAULT_NONE,false,DBCreator::$ON_UPDATE_NONE,"pembayaran pasien, bayar resep pasien, bayar lain-lain");
    $dbcreator->addColumn("noreg_pasien","int(11)",DBCreator::$SIGN_NONE,false,DBCreator::$DEFAULT_NONE,false,DBCreator::$ON_UPDATE_NONE,"pembayaran pasien, bayar resep pasien, bayar lain-lain");
    $dbcreator->addColumn("nrm_pasien","varchar(16)",DBCreator::$SIGN_NONE,false,DBCreator::$DEFAULT_NONE,false,DBCreator::$ON_UPDATE_NONE,"pembayaran pasien, bayar resep pasien, bayar lain-lain");
    $dbcreator->addColumn("ruangan","varchar(64)",DBCreator::$SIGN_NONE,false,DBCreator::$DEFAULT_NONE,false,DBCreator::$ON_UPDATE_NONE,"pembayaran pasien, bayar resep pasien, bayar lain-lain");
    $dbcreator->addColumn("tagihan","int(11)",DBCreator::$SIGN_NONE,false,DBCreator::$DEFAULT_NONE,false,DBCreator::$ON_UPDATE_NONE,"pembayaran pasien, bayar resep pasien, bayar lain-lain");
    $dbcreator->addColumn("asuransi","int(11)",DBCreator::$SIGN_NONE,false,DBCreator::$DEFAULT_NONE,false,DBCreator::$ON_UPDATE_NONE,"pembayaran pasien, bayar resep pasien, bayar lain-lain");
    $dbcreator->addColumn("bank","int(11)",DBCreator::$SIGN_NONE,false,DBCreator::$DEFAULT_NONE,false,DBCreator::$ON_UPDATE_NONE,"pembayaran pasien, bayar resep pasien, bayar lain-lain");
    $dbcreator->addColumn("cash","int(11)",DBCreator::$SIGN_NONE,false,DBCreator::$DEFAULT_NONE,false,DBCreator::$ON_UPDATE_NONE,"pembayaran pasien, bayar resep pasien, bayar lain-lain");
    $dbcreator->addColumn("diskon","int(11)",DBCreator::$SIGN_NONE,false,DBCreator::$DEFAULT_NONE,false,DBCreator::$ON_UPDATE_NONE,"pembayaran pasien, bayar resep pasien, bayar lain-lain");
    $dbcreator->addColumn("selisih","int(11)",DBCreator::$SIGN_NONE,false,DBCreator::$DEFAULT_NONE,false,DBCreator::$ON_UPDATE_NONE,"pembayaran pasien, bayar resep pasien, bayar lain-lain");
    $dbcreator->addUniqueKey("noreg_pasien",array("noreg_pasien"));
    $dbcreator->setDuplicate(false);
    $dbcreator->initialize();
?>