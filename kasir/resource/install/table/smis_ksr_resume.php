<?php 
    global $wpdb;
    require_once "smis-libs-class/DBCreator.php";
    $dbcreator=new DBCreator($wpdb,"smis_ksr_resume");
    $dbcreator->addColumn("nrm_pasien","varchar(16)",DBCreator::$SIGN_NONE,false,DBCreator::$DEFAULT_NONE,false,DBCreator::$ON_UPDATE_NONE,"pembayaran pasien, bayar resep pasien, bayar lain-lain");
    $dbcreator->addColumn("nama_pasien","int(11)",DBCreator::$SIGN_NONE,false,DBCreator::$DEFAULT_NONE,false,DBCreator::$ON_UPDATE_NONE,"pembayaran pasien, bayar resep pasien, bayar lain-lain");
    $dbcreator->addColumn("resume_pasien","int(11)",DBCreator::$SIGN_NONE,false,DBCreator::$DEFAULT_NONE,false,DBCreator::$ON_UPDATE_NONE,"pembayaran pasien, bayar resep pasien, bayar lain-lain");
    $dbcreator->addColumn("detail_tagihan","int(11)",DBCreator::$SIGN_NONE,false,DBCreator::$DEFAULT_NONE,false,DBCreator::$ON_UPDATE_NONE,"pembayaran pasien, bayar resep pasien, bayar lain-lain");
    $dbcreator->addColumn("total_tagihan","int(11)",DBCreator::$SIGN_NONE,false,DBCreator::$DEFAULT_NONE,false,DBCreator::$ON_UPDATE_NONE,"pembayaran pasien, bayar resep pasien, bayar lain-lain");
    $dbcreator->addColumn("jaspel","int(11)",DBCreator::$SIGN_NONE,false,DBCreator::$DEFAULT_NONE,false,DBCreator::$ON_UPDATE_NONE,"pembayaran pasien, bayar resep pasien, bayar lain-lain");
    $dbcreator->addColumn("total_pembayaran","int(11)",DBCreator::$SIGN_NONE,false,DBCreator::$DEFAULT_NONE,false,DBCreator::$ON_UPDATE_NONE,"pembayaran pasien, bayar resep pasien, bayar lain-lain");
    $dbcreator->setDuplicate(false);
    $dbcreator->initialize();
?>