<?php 
    global $wpdb;
    require_once "smis-libs-class/DBCreator.php";
    $dbcreator=new DBCreator($wpdb,"smis_ksr_nonpasien");
    $dbcreator->addColumn("nama","varchar(64)",DBCreator::$SIGN_NONE,false,DBCreator::$DEFAULT_NONE,false,DBCreator::$ON_UPDATE_NONE,"");
    $dbcreator->addColumn("harga","int(11)",DBCreator::$SIGN_NONE,false,DBCreator::$DEFAULT_NONE,false,DBCreator::$ON_UPDATE_NONE,"");
    $dbcreator->addColumn("harga_lama","int(11)",DBCreator::$SIGN_NONE,false,DBCreator::$DEFAULT_NONE,false,DBCreator::$ON_UPDATE_NONE,"");
    $dbcreator->addColumn("rencana_harga","int(11)",DBCreator::$SIGN_NONE,false,DBCreator::$DEFAULT_NONE,false,DBCreator::$ON_UPDATE_NONE,"");
    $dbcreator->addColumn("keterangan","varchar(128)",DBCreator::$SIGN_NONE,false,DBCreator::$DEFAULT_NONE,false,DBCreator::$ON_UPDATE_NONE,"");
    $dbcreator->addColumn("kredit","varchar(32)",DBCreator::$SIGN_NONE,false,DBCreator::$DEFAULT_NONE,false,DBCreator::$ON_UPDATE_NONE,"");
    $dbcreator->setDuplicate(false);
    $dbcreator->initialize();
?>