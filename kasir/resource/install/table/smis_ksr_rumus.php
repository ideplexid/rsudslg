<?php 
    global $wpdb;
    require_once "smis-libs-class/DBCreator.php";
    $dbcreator=new DBCreator($wpdb,"smis_ksr_rumus");
    $dbcreator->addColumn("nama","varchar(64)",DBCreator::$SIGN_NONE,false,DBCreator::$DEFAULT_NONE,false,DBCreator::$ON_UPDATE_NONE,"pembayaran pasien, bayar resep pasien, bayar lain-lain");
    $dbcreator->addColumn("rumus_biaya","varchar(32)",DBCreator::$SIGN_NONE,false,DBCreator::$DEFAULT_NONE,false,DBCreator::$ON_UPDATE_NONE,"pembayaran pasien, bayar resep pasien, bayar lain-lain");
    $dbcreator->addColumn("rumus_potongan","varchar(32)",DBCreator::$SIGN_NONE,false,DBCreator::$DEFAULT_NONE,false,DBCreator::$ON_UPDATE_NONE,"pembayaran pasien, bayar resep pasien, bayar lain-lain");
    $dbcreator->addColumn("rumus_tagihan","varchar(32)",DBCreator::$SIGN_NONE,false,DBCreator::$DEFAULT_NONE,false,DBCreator::$ON_UPDATE_NONE,"pembayaran pasien, bayar resep pasien, bayar lain-lain");
    $dbcreator->addColumn("keterangan","text",DBCreator::$SIGN_NONE,false,DBCreator::$DEFAULT_NONE,false,DBCreator::$ON_UPDATE_NONE,"pembayaran pasien, bayar resep pasien, bayar lain-lain");
    $dbcreator->setDuplicate(false);
    $dbcreator->initialize();
?>