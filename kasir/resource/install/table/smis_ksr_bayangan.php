<?php 
    global $wpdb;
    require_once "smis-libs-class/DBCreator.php";
    $dbcreator=new DBCreator($wpdb,"smis_ksr_bayangan");
    $dbcreator->addColumn("nama","varchar(64)",DBCreator::$SIGN_NONE,false,DBCreator::$DEFAULT_NONE,false,DBCreator::$ON_UPDATE_NONE,"");
    $dbcreator->addColumn("nrm","varchar(16)",DBCreator::$SIGN_NONE,false,DBCreator::$DEFAULT_NONE,false,DBCreator::$ON_UPDATE_NONE,"");
    $dbcreator->addColumn("noreg","int(11)",DBCreator::$SIGN_NONE,false,DBCreator::$DEFAULT_NONE,false,DBCreator::$ON_UPDATE_NONE,"");
    $dbcreator->addColumn("ruangan","varchar(64)",DBCreator::$SIGN_NONE,false,DBCreator::$DEFAULT_NONE,false,DBCreator::$ON_UPDATE_NONE,"");
    $dbcreator->addColumn("layanan","varchar(32)",DBCreator::$SIGN_NONE,false,DBCreator::$DEFAULT_NONE,false,DBCreator::$ON_UPDATE_NONE,"");
    $dbcreator->addColumn("tindakan","varchar(64)",DBCreator::$SIGN_NONE,false,DBCreator::$DEFAULT_NONE,false,DBCreator::$ON_UPDATE_NONE,"");
    $dbcreator->addColumn("harga","int(11)",DBCreator::$SIGN_NONE,false,DBCreator::$DEFAULT_NONE,false,DBCreator::$ON_UPDATE_NONE,"");
    $dbcreator->addColumn("jaspel","int(11)",DBCreator::$SIGN_NONE,false,DBCreator::$DEFAULT_NONE,false,DBCreator::$ON_UPDATE_NONE,"");
    $dbcreator->addColumn("keterangan","text",DBCreator::$SIGN_NONE,false,DBCreator::$DEFAULT_NONE,false,DBCreator::$ON_UPDATE_NONE,"");
    $dbcreator->initialize();
?>