<?php 
    global $wpdb;
    require_once "smis-libs-class/DBCreator.php";
    $dbcreator=new DBCreator($wpdb,"smis_ksr_np");
    $dbcreator->addColumn("tanggal","datetime",DBCreator::$SIGN_NONE,false,DBCreator::$DEFAULT_NONE,false,DBCreator::$ON_UPDATE_NONE,"pembayaran pasien, bayar resep pasien, bayar lain-lain");
    $dbcreator->addColumn("nama","varchar(32)",DBCreator::$SIGN_NONE,false,DBCreator::$DEFAULT_NONE,false,DBCreator::$ON_UPDATE_NONE,"pembayaran pasien, bayar resep pasien, bayar lain-lain");
    $dbcreator->addColumn("keterangan","varchar(64)",DBCreator::$SIGN_NONE,false,DBCreator::$DEFAULT_NONE,false,DBCreator::$ON_UPDATE_NONE,"pembayaran pasien, bayar resep pasien, bayar lain-lain");
    $dbcreator->addColumn("operator","varchar(32)",DBCreator::$SIGN_NONE,false,DBCreator::$DEFAULT_NONE,false,DBCreator::$ON_UPDATE_NONE,"pembayaran pasien, bayar resep pasien, bayar lain-lain");
    $dbcreator->addColumn("nilai","bigint(32)",DBCreator::$SIGN_NONE,false,DBCreator::$DEFAULT_NONE,false,DBCreator::$ON_UPDATE_NONE,"pembayaran pasien, bayar resep pasien, bayar lain-lain");
    $dbcreator->addColumn("id_dompet", "int(11)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
    $dbcreator->addColumn("no_akun", "varchar(16)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
    $dbcreator->addColumn("nama_dompet", "varchar(32)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
    $dbcreator->addColumn("akunting","tinyint(1)",DBCreator::$SIGN_NONE,false,DBCreator::$DEFAULT_NONE,false,DBCreator::$ON_UPDATE_NONE,"operator yang input");
    $dbcreator->initialize();
?>