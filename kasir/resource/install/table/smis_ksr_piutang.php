<?php 
    global $wpdb;
    require_once "smis-libs-class/DBCreator.php";
    $dbcreator=new DBCreator($wpdb,"smis_ksr_piutang");
    $dbcreator->addColumn("layanan","varchar(32)",DBCreator::$SIGN_NONE,false,DBCreator::$DEFAULT_NONE,false,DBCreator::$ON_UPDATE_NONE,"pembayaran pasien, bayar resep pasien, bayar lain-lain");
    $dbcreator->addColumn("nilai","int(11)",DBCreator::$SIGN_NONE,false,DBCreator::$DEFAULT_NONE,false,DBCreator::$ON_UPDATE_NONE,"pembayaran pasien, bayar resep pasien, bayar lain-lain");
    $dbcreator->addColumn("ruangan","varchar(32)",DBCreator::$SIGN_NONE,false,DBCreator::$DEFAULT_NONE,false,DBCreator::$ON_UPDATE_NONE,"pembayaran pasien, bayar resep pasien, bayar lain-lain");
    $dbcreator->addColumn("urjip","varchar(3)",DBCreator::$SIGN_NONE,false,DBCreator::$DEFAULT_NONE,false,DBCreator::$ON_UPDATE_NONE,"pembayaran pasien, bayar resep pasien, bayar lain-lain");
    $dbcreator->setDuplicate(false);
    $dbcreator->initialize();
?>