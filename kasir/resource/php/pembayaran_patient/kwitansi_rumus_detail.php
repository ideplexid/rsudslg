<?php
/**
 * kwitansi ini dibuat agar tagihan pasien sesuai dengan rumus yang sudah di tetapkan.
 * dengan adanya kwitansi rumus ini , tagihan pasien akan lebih mudah mengikuti rumus yang
 * sudah ada.
 * 
 * perbedaanya pada kwitansi ini akan ditampilkan detail dari masing-masing tagihan 
 * yang ada. seperti dokter, konsul dan setiap ruangan.
 * 
 * @author      : Nurul Huda
 * @license     : goblooge@gmail.com
 * @since       : 23 Feb 2017
 * @version     : 1.0
 * @database    :   - smis_ksr_rumus
 *                  - smis_ksr_kwitansi_rumus_detail
 * @service     :   - get_last_position
 *                  - get_registered
 * 
 * */

require_once ("smis-base/smis-include-service-consumer.php");
global $db;
global $user;

if(isset($_POST['super_command']) && $_POST['super_command']=="rumus_kwitansi_rumus_detail"){
    $uitable=new Table(array("No.","Nama","Biaya","Potongan","Tagihan","Keterangan"));
    $uitable->setName("rumus_kwitansi_rumus_detail");
    $uitable->setModel(Table::$SELECT);
    $rmstable=new DBTable($db,"smis_ksr_rumus");
    $adapter=new SimpleAdapter();
    $adapter->setUseNumber(true,"No.","back.");
    $adapter->add("Nama","nama");
    $adapter->add("Biaya","rumus_biaya");
    $adapter->add("Potongan","rumus_potongan");
    $adapter->add("Tagihan","rumus_tagihan");
    $adapter->add("Keterangan","keterangan");
    $dbresponder=new DBResponder($rmstable,$uitable,$adapter);
    $super=new SuperCommand();
    $super->addResponder("rumus_kwitansi_rumus_detail",$dbresponder);
    $supr=$super->initialize();
    if($supr!=null){
        echo $supr;
    }
    return;
}

$header=array ();
$adapter = new SimpleAdapter ();
$uitable = new Table ( $header );
$uitable->setName("kwitansi_rumus_detail");
$rumustbl=new DBTable($db,"smis_ksr_tagihan_rumus");

if(isset($_POST['command'])){
    $dbres=new DBResponder($rumustbl,$uitable,$adapter);
    if($dbres->isSave()){
        /* adalah biaya total pelayanan terhadap pasien oleh pasien */
        $X=$_POST['tarif_plafon_inacbg'];
        $Y=$_POST['tarif_plafon_kelas1'];
        $rumusT=$_POST['rumus_biaya'];
        $rumusT=str_replace("X",$X,$rumusT);
        $rumusT=str_replace("Y",$Y,$rumusT);
        eval( '$nilaiT= (' . $rumusT. ');' );
        $T=$nilaiT;
        $dbres->addColumnFixValue("total_biaya",$T);
        
        /* adalah potongan pengurangan biaya dari pelayanan pasien */
        $rumusU=$_POST['rumus_potongan'];
        $rumusU=str_replace("X",$X,$rumusU);
        $rumusU=str_replace("Y",$Y,$rumusU);
        $rumusU=str_replace("T",$T,$rumusU);       
        eval( '$nilaiU= (' . $rumusU. ');' );
        $U=$nilaiU;
        $dbres->addColumnFixValue("total_potongan",$U);
        
        /* adalah biaya yang harus dibayar oleh pasien */
        $rumusZ=$_POST['rumus_tagihan'];
        $rumusZ=str_replace("X",$X,$rumusZ);
        $rumusZ=str_replace("Y",$Y,$rumusZ);
        $rumusZ=str_replace("T",$T,$rumusZ);
        $rumusZ=str_replace("U",$U,$rumusZ);
        eval( '$nilaiZ= (' . $rumusZ. ');' );
        $Z=$nilaiZ;
        $dbres->addColumnFixValue("total_tagihan",$Z);
        
    }
    $data=$dbres->command($_POST['command']);
    echo json_encode($data);
    return;
}

/*MENGAMBIL DATA PASIEN di REGISTRASI*/
require_once "kasir/function/get_patient_by_noreg.php";
require_once "kasir/function/get_system_setup.php";
$noreg           = $_POST ['noreg_pasien'];
$px              = get_patient_by_noreg($db,$noreg);
$_setup          = get_system_setup($db,$user,$noreg,$px,"");

/** TODO SYNCH TGL PULANG */
$_NAMA_PASIEN	 =	$px['nama_pasien']." / ".$px['namapenanggungjawab'];
$_ALAMAT_PASIEN	 =	$px['alamat_pasien'];
$_NRMNORG_PASIEN =	ArrayAdapter::format("only-digit8",$px['nrm'])." / ".ArrayAdapter::format("only-digit8",$px['id']);
$_TGL_MASUK		 =	ArrayAdapter::format("date d M Y H:i",$_setup['tgl_masuk']);
$_TGL_KELUAR	 =	ArrayAdapter::format("date d M Y H:i",$_setup['tgl_pulang']);
$_NOMOR_KWITANSI =  $_setup['no_kwitansi'];
$_TGL_KWITANSI	 = 	$_setup['waktu_kwitansi'];
$_NILAI_BAYAR	 =	$_setup['nilai_kwitansi'];
    
$_ID_RUMUS_PV   = 0;
$_ASURANSI      =   $px['nama_asuransi']; 
$_CARABAYAR     =   ArrayAdapter::format("unslug",$px['carabayar']); 
/* END OF MENGAMBIL DATA PASIEN DI REGISTRASI DI SIMPAN DI $PX */


$rumustbl->setFetchMethode(DBTable::$ARRAY_FETCH);
$rumus=$rumustbl->select(array("noreg_pasien"=>$noreg));
if($rumus==null){
    $responder = new ServiceConsumer ( $db, "get_last_position",$params = array("noreg_pasien"	=> $_POST['noreg_pasien']),"medical_record" );
    $responder->execute();
    $_LAST_POSITION=ArrayAdapter::format("unslug",$responder->getContent());
    
    $rumus['nama_pasien']       = $px['nama_pasien'];
    $rumus['nrm_pasien']        = $px['nrm'];
    $rumus['noreg_pasien']      = $noreg;
    $rumus['ruang_pelayanan']   = $_LAST_POSITION;
    $rumus['id_rumus_pv']       = $_ID_RUMUS_PV;
    $rumustbl->insert($rumus);
    $rumus['id']=$rumustbl->get_inserted_id();
}
$_ID_RUMUS_PV       = $rumus['id_rumus_pv'];

/*service mengambil Rumus PV*/
$params=array();
$params['command']="edit";
$params['id']=$_ID_RUMUS_PV;

$responder = new ServiceConsumer ( $db, "get_formula_pv",$params,"manajemen" );
$responder->execute();
$content=$responder->getContent();
$_FORMULA=$content;
/*END - service mengambil Rumus PV*/



/*MEMBUAT TOMBOL CETAK*/
$cetak=new Button("print_button_simple_kwitansi", "", " Cetak ");
$cetak->addClass("btn btn-primary");
$cetak->setIcon(" fa fa-print ");
$cetak->setIsButton(BUtton::$ICONIC_TEXT);
$cetak->setAction("kwitansi_print('kwitansi_rumus_detail')");
if( $px['uri']=="0" && getSettings($db,"cashier-simple-kwitansi-cek-print-rj","0")=="1" || $px['uri']=="1" && getSettings($db,"cashier-simple-kwitansi-cek-print-ri","0")=="1"){
    $cetak->setAction("cek_kwitansi_print('kwitansi_rumus_detail')");
}

$simpan=new Button("print_button_simple_kwitansi", "", " Simpan ");
$simpan->addClass("btn btn-primary");
$simpan->setIcon(" fa fa-save ");
$simpan->setIsButton(Button::$ICONIC_TEXT);
$simpan->setAction("kwitansi_rumus_detail.save()");
/*AKHIR DARI MEMBUAT TOMBOL CETAK*/

/*FORM*/
$kelas_pasien=new OptionBuilder();
$kelas_pasien->addSingle("",($rumus['kelas_plafon']==""?"1":"0"));
$kelas_pasien->addSingle("Kelas I",($rumus['kelas_plafon']=="Kelas I"?"1":"0"));
$kelas_pasien->addSingle("Kelas II",($rumus['kelas_plafon']=="Kelas II"?"1":"0"));
$kelas_pasien->addSingle("Kelas III",($rumus['kelas_plafon']=="Kelas III"?"1":"0"));
        
$uitable->addModal("id","hidden","",$rumus['id']);
$uitable->addModal("nama_pasien","hidden","",$rumus['nama_pasien']);
$uitable->addModal("nrm_pasien","hidden","",$rumus['nrm_pasien']);
$uitable->addModal("noreg_pasien","hidden","",$rumus['noreg_pasien']);
$uitable->addModal("kelas_plafon","select","Kelas",$kelas_pasien->getContent());
$uitable->addModal("kode_inacbg","text","Kode INACBG",$rumus['kode_inacbg']);
$uitable->addModal("deskripsi_inacbg","text","Deskripsi",$rumus['deskripsi_inacbg']);
$uitable->addModal("ruang_pelayanan","text","R. Pelayanan",$rumus['ruang_pelayanan']);
$uitable->addModal("id_rumus","hidden","",$rumus['id_rumus']);
$uitable->addModal("nama_rumus","chooser-kwitansi_rumus_detail-rumus_kwitansi_rumus_detail-Pilih Rumus","Rumus",$rumus['nama_rumus']);
$uitable->addModal("rumus_tagihan","hidden","",$rumus['rumus_tagihan']);
$uitable->addModal("rumus_potongan","hidden","",$rumus['rumus_potongan']);
$uitable->addModal("rumus_biaya","hidden","",$rumus['rumus_biaya']);
$uitable->addModal("tarif_plafon_inacbg","money","Plafon INACBG",$rumus['tarif_plafon_inacbg']);
$uitable->addModal("tarif_plafon_kelas1","money","Plafon Kelas I",$rumus['tarif_plafon_kelas1']);

$form=$uitable->getModal()->getForm();
$form->addElement("",$cetak);
$form->addElement("",$simpan);
/*END - FOM*/

/*MENGAMBIL DATA RUMAH SAKIT*/
$autonomous=getSettings($db, "smis_autonomous_name", "SMIS");
$nama_rs=getSettings($db, "cashier-simple-kwitansi-rs", $autonomous);
$autonomous_address=getSettings($db, "smis_autonomous_address", "LOCALHOST");
$alamat_rs=getSettings($db, "cashier-simple-kwitansi-address", $autonomous_address);
/*END - MENGAMBIL DATA RUMAH SAKIT*/

/*MENGAMBIL DATA TOTAL TAGIHAN*/
$query="SELECT sum(total) FROM smis_ksr_kolektif WHERE prop!='del' AND akunting_only='0'  AND noreg_pasien='$noreg' ";
$_TOTAL_TAGIHAN=$db->get_var($query);
/*END - MENGAMBIL DATA TOTAL TAGIHAN*/

$table=new TablePrint("kwitansi_rumus_detail");
$table->setMaxWidth(true);
$table->setTableClass("kwitansi_print");
$table  ->addColumn($nama_rs."</br>".$alamat_rs, 6, 1,NULL,NULL,"center bold ")
        ->commit("body");
$table  ->addColumn("NAMA / PJ",1,1)
        ->addColumn($_NAMA_PASIEN,2,1)  
        ->addColumn("No. Kwitansi : ".$_NOMOR_KWITANSI)
        ->commit("body");
$table  ->addColumn("ALAMAT",1,1)
        ->addColumn($_ALAMAT_PASIEN,2,1)
        ->addColumn("TANGGAL MASUK",1,1,null,null,"bold")
        ->commit("body");
$table  ->addColumn("NRM / NO. REG",1,1)
        ->addColumn($_NRMNORG_PASIEN,2,1)
        ->addColumn($_TGL_MASUK,1,1)
        ->commit("body");
$table  ->addColumn("Cara Bayar Pasien",1,1)
        ->addColumn($_CARABAYAR,2,1)
        ->addColumn("TANGGAL KELUAR",1,1,null,null,"bold")
        ->commit("body");
$table  ->addColumn("Kode Rincian",1,1)
        ->addColumn($_FORMULA['kode'],1,1)
        ->addSpace(1,1)
        ->addColumn($_TGL_KELUAR)
        ->commit("body");
$table  ->addSpace(4,1)
        ->commit("body");
$table  ->addColumn("INA-CBG ",1,1)
        ->addColumn(" : ".$rumus['kode_inacbg'])
        ->addColumn("Kelas Pasien ",1,1)
        ->addColumn(" : ".$rumus['kelas_plafon'])
        ->commit("body");
$table  ->addColumn("Deskripsi ",1,1)
        ->addColumn(" : ".$rumus['deskripsi_inacbg'])
        ->addColumn("Ruang Pelayanan ",1,1)
        ->addColumn(" : ".$rumus['ruang_pelayanan'])
        ->commit("body");
$table  ->addColumn("RINCIAN",1,1,null,null,"bold upperscore center")
        ->addColumn("KETERANGAN",1,1,null,null,"bold upperscore center")
        ->addColumn("TOTAL",2,1,null,null,"bold upperscore center")
        ->commit("body");

/*START - MEMASUKAN DETAIL RUMUS*/
$_basis_persen=$rumus['total_biaya'];
$_total_biaya_yg_harus_ditanggung=0;

$table->addSpace(4,1,"body");
$table->addColumn("KAMAR DAN MAKAN",4,1,null,null,"bold");
$table->commit("body");
$nilai_uang=($_FORMULA['jasa_sarana']+$_FORMULA['jasa_umum']+$_FORMULA['cito'])*$_basis_persen;
$nilai_max=$_FORMULA['bed_kelas_nicu']*$rumus['durasi_kelas_nicu'];
$nilai_max+=$_FORMULA['bed_kelas_hcu']*$rumus['durasi_kelas_hcu'];
$nilai_max+=$_FORMULA['bed_kelas_i']*$rumus['durasi_kelas_i'];
$nilai_max+=$_FORMULA['bed_kelas_ii']*$rumus['durasi_kelas_ii'];
$nilai_max+=$_FORMULA['bed_kelas_iii']*$rumus['durasi_kelas_iii'];
$nilai_max+=$_FORMULA['bed_kelas_vip']*$rumus['durasi_kelas_vip'];
$nilai_max+=$_FORMULA['bed_kelas_vvip']*$rumus['durasi_kelas_vvip'];
$nilai_max+=$_FORMULA['bed_kelas_deluxe']*$rumus['durasi_kelas_deluxe'];
$nilai_max+=$_FORMULA['bed_kelas_non_kelas']*$rumus['durasi_kelas_non_kelas'];

$harga_basic=0;
$sisa_harga_kamar=0;
$nilai_persen=array();
$total_durasi=array();
if($nilai_uang>$nilai_max){
    /* nilai tetap, karena maximal uangnya lebih besar dariapda detailnya */
    $harga_basic=$nilai_max;
    $sisa_harga_kamar=$nilai_uang-$nilai_max;
    
    $nilai_persen['kelas_nicu']=($_FORMULA['bed_kelas_nicu']*$rumus['durasi_kelas_nicu']);
    $nilai_persen['kelas_hcu']=($_FORMULA['bed_kelas_hcu']*$rumus['durasi_kelas_hcu']);
    $nilai_persen['kelas_i']=($_FORMULA['bed_kelas_i']*$rumus['durasi_kelas_i']);
    $nilai_persen['kelas_ii']=($_FORMULA['bed_kelas_ii']*$rumus['durasi_kelas_ii']);
    $nilai_persen['kelas_iii']=($_FORMULA['bed_kelas_iii']*$rumus['durasi_kelas_iii']);
    $nilai_persen['kelas_vip']=($_FORMULA['bed_kelas_vip']*$rumus['durasi_kelas_vip']);
    $nilai_persen['kelas_vvip']=($_FORMULA['bed_kelas_vvip']*$rumus['durasi_kelas_vvip']);
    $nilai_persen['kelas_deluxe']=($_FORMULA['bed_kelas_deluxe']*$rumus['durasi_kelas_deluxe']);
    $nilai_persen['non_kelas']=($_FORMULA['bed_kelas_non_kelas']*$rumus['durasi_kelas_non_kelas']);
}else{
    /* nilai strect, karena maximal uangnya lebih kecil dariapda detailnya */
    $harga_basic=$nilai_uang;
    $nilai_persen['kelas_nicu']=($_FORMULA['bed_kelas_nicu']*$rumus['durasi_kelas_nicu'])*$harga_basic/$nilai_max;
    $nilai_persen['kelas_hcu']=($_FORMULA['bed_kelas_hcu']*$rumus['durasi_kelas_hcu'])*$harga_basic/$nilai_max;
    $nilai_persen['kelas_i']=($_FORMULA['bed_kelas_i']*$rumus['durasi_kelas_i'])*$harga_basic/$nilai_max;
    $nilai_persen['kelas_ii']=($_FORMULA['bed_kelas_ii']*$rumus['durasi_kelas_ii'])*$harga_basic/$nilai_max;
    $nilai_persen['kelas_iii']=($_FORMULA['bed_kelas_iii']*$rumus['durasi_kelas_iii'])*$harga_basic/$nilai_max;
    $nilai_persen['kelas_vip']=($_FORMULA['bed_kelas_vip']*$rumus['durasi_kelas_vip'])*$harga_basic/$nilai_max;
    $nilai_persen['kelas_vvip']=($_FORMULA['bed_kelas_vvip']*$rumus['durasi_kelas_vvip'])*$harga_basic/$nilai_max;
    $nilai_persen['kelas_deluxe']=($_FORMULA['bed_kelas_deluxe']*$rumus['durasi_kelas_deluxe'])*$harga_basic/$nilai_max;
    $nilai_persen['non_kelas']=($_FORMULA['bed_non_kelas']*$rumus['durasi_non_kelas'])*$harga_basic/$nilai_max;
    $sisa_harga_kamar=0;    
}

$total_durasi['kelas_nicu']=($rumus['durasi_kelas_nicu']);
$total_durasi['kelas_hcu']=($rumus['durasi_kelas_hcu']);
$total_durasi['kelas_i']=($rumus['durasi_kelas_i']);
$total_durasi['kelas_ii']=($rumus['durasi_kelas_ii']);
$total_durasi['kelas_iii']=($rumus['durasi_kelas_iii']);
$total_durasi['kelas_vip']=($rumus['durasi_kelas_vip']);
$total_durasi['kelas_vvip']=($rumus['durasi_kelas_vvip']);
$total_durasi['kelas_deluxe']=($rumus['durasi_kelas_deluxe']);
$total_durasi['non_kelas']=($rumus['durasi_kelas_non_kelas']);
    

//kamar
$history_ruang=json_decode($rumus['history_ruang'],true);
foreach($history_ruang as $n=>$x){
    if($x['durasi']==0)
        continue;
    $kelas=$x['kelas'];
    $hari=$x['durasi'];
    $uang_kelas=$nilai_persen[$kelas];
    $jumlah_max_kelas=$total_durasi[$kelas];
    $uang_ruang=$hari*$uang_kelas/$jumlah_max_kelas;
    
    $table->addColumn(ArrayAdapter::format("unslug",$x['nama_ruang']),1,1);
    $table->addColumn($hari." Hari ",1,1,null,null,"left");
    $table->addColumn(ArrayAdapter::format("money Rp. ",$uang_kelas),2,1);
    $table->commit("body");
    $_total_biaya_yg_harus_ditanggung+=$uang_ruang;
}
//makan dll
/*$opmk=($_FORMULA['jasa_sarana']+$_FORMULA['jasa_umum']+$_FORMULA['cito'])*$_basis_persen;
 * 
$_total_biaya_yg_harus_ditanggung+=$opmk;
$table->addColumn("Operasional, Kamar, dan Makan",1,1);
$table->addColumn("-",1,1,null,null,"center");
$table->addColumn(ArrayAdapter::format("money Rp. ",$opmk),2,1);
$table->commit("body");*/

//bmhp
$table->addSpace(4,1,"body");
$bmhp=$_FORMULA['bmhp']*$_basis_persen+$sisa_harga_kamar;
$_total_biaya_yg_harus_ditanggung+=$bmhp;
$table->addColumn("Obat dan Alat",1,1);
$table->addColumn("-",1,1,null,null,"center");
$table->addColumn(ArrayAdapter::format("money Rp. ",$bmhp),2,1);
$table->commit("body");


//dokter
$table->addSpace(4,1,"body");
$table->addColumn("TINDAKAN DOKTER",4,1,null,null,"bold");
$table->commit("body");
    $list_dokter_pakem=array();
    $list_dokter_pakem[] =   array("formula"=>$_FORMULA['dokter_igd'],"ket"=>"Dokter IGD","name"=>$rumus['nama_igd']);
    $list_dokter_pakem[] =   array("formula"=>$_FORMULA['dokter_anastesi'],"ket"=>"Dokter Anastesi","name"=>$rumus['nama_anastesi']);
    $list_dokter_pakem[] =   array("formula"=>$_FORMULA['dokter_anak'],"ket"=>"Dokter Anak","name"=>$rumus['nama_anak']);
    $list_dokter_pakem[] =   array("formula"=>$_FORMULA['dokter_spesialis'],"ket"=>"Dokter Spesialis","name"=>$rumus['nama_spesialis']);
    $list_dokter_pakem[] =   array("formula"=>$_FORMULA['dokter_umum'],"ket"=>"Dokter Umum","name"=>$rumus['nama_umum']);
    
    foreach($list_dokter_pakem as $k=>$v){
        if($v['formula']!=0){
            $uang=($v['formula'])*$_basis_persen;
            $_total_biaya_yg_harus_ditanggung+=$uang;
            $table->addColumn($v['name'],1,1);
            $table->addColumn($v['ket'],1,1,null,null,"left");
            $table->addColumn(ArrayAdapter::format("money Rp. ",$uang),2,1);
            $table->commit("body");
        }
    }
    
    //dokter konsul
    $uang=$_FORMULA['dokter_konsul']*$_basis_persen;
    if($uang>0){
        $_total_biaya_yg_harus_ditanggung+=$uang;
        if($rumus['nama_konsul_satu']!="" && $rumus['nama_konsul_dua']!=""){
            $table->addColumn($rumus['nama_konsul_satu'],1,1);
            $table->addColumn("Dokter Konsul I",1,1,null,null,"left");
            $table->addColumn(ArrayAdapter::format("money Rp. ",$uang/2),2,1);
            $table->commit("body");
            
            $table->addColumn($rumus['nama_konsul_dua'],1,1);
            $table->addColumn("Dokter Konsul II",1,1,null,null,"left");
            $table->addColumn(ArrayAdapter::format("money Rp. ",$uang/2),2,1);
            $table->commit("body");
        }else if($rumus['nama_konsul_satu']!=""){
            $table->addColumn($rumus['nama_konsul_satu'],1,1);
            $table->addColumn("Dokter Konsul I",1,1,null,null,"left");
            $table->addColumn(ArrayAdapter::format("money Rp. ",$uang),2,1);
            $table->commit("body");
        }else if($rumus['nama_konsul_dua']!=""){
            $table->addColumn($rumus['nama_konsul_dua'],1,1);
            $table->addColumn("Dokter Konsul II",1,1,null,null,"left");
            $table->addColumn(ArrayAdapter::format("money Rp. ",$uang),2,1);
            $table->commit("body");
        }else{
            $table->addColumn("-",1,1);
            $table->addColumn("Dokter Konsul",1,1,null,null,"left");
            $table->addColumn(ArrayAdapter::format("money Rp. ",$uang),2,1);
            $table->commit("body");
        }
    }
    
    //dokter dpjp
    $uang=$_FORMULA['dokter_dpjp']*$_basis_persen;
    $durasi_satu=$rumus['durasi_dpjp_satu'];
    $durasi_dua=$rumus['durasi_dpjp_dua'];
    $durasi_tiga=$rumus['durasi_dpjp_tiga'];
    $total_durasi=$durasi_satu+$durasi_dua+$durasi_tiga;
    if($total_durasi==0){
        $durasi_satu=1;
        $durasi_dua=1;
        $durasi_tiga=1;
    }
    $jumlah_dpjp=$rumus['nama_dpjp_satu']!=""?$durasi_satu:0;
    $jumlah_dpjp+=$rumus['nama_dpjp_dua']!=""?$durasi_dua:0;
    $jumlah_dpjp+=$rumus['nama_dpjp_tiga']!=""?$durasi_tiga:0;
    if($jumlah_dpjp>0){
        $uang=$uang/$jumlah_dpjp;
    }
    
    if($rumus['nama_dpjp_satu']!=""){
        $biaya=$rumus['biaya_dpjp_satu']!=0?$rumus['biaya_dpjp_satu']:($uang*$durasi_satu);
        $_total_biaya_yg_harus_ditanggung+=$biaya;
        $table->addColumn($rumus['nama_dpjp_satu'],1,1);
        $table->addColumn("Dokter DPJP I",1,1,null,null,"left");
        $table->addColumn(ArrayAdapter::format("money Rp. ",$biaya),2,1);
        $table->commit("body");
    }
    
    if($rumus['nama_dpjp_dua']!=""){
        $biaya=$rumus['biaya_dpjp_dua']!=0?$rumus['biaya_dpjp_dua']:($uang*$durasi_dua);
        $_total_biaya_yg_harus_ditanggung+=$biaya;
        $table->addColumn($rumus['nama_dpjp_dua'],1,1);
        $table->addColumn("Dokter DPJP II",1,1,null,null,"left");
        $table->addColumn(ArrayAdapter::format("money Rp. ",$biaya),2,1);
        $table->commit("body");
    }
    
    if($rumus['nama_dpjp_tiga']!=""){
        $biaya=$rumus['biaya_dpjp_tiga']!=0?$rumus['biaya_dpjp_tiga']:($uang*$durasi_tiga);
        $_total_biaya_yg_harus_ditanggung+=$biaya;
        $table->addColumn($rumus['nama_dpjp_tiga'],1,1);
        $table->addColumn("Dokter DPJP III",1,1,null,null,"left");
        $table->addColumn(ArrayAdapter::format("money Rp. ",$biaya),2,1);
        $table->commit("body");
    }
    /* kode ini untuk mengatasi dokter DPJP yang kosong */
    if($jumlah_dpjp==0){
        $biaya=$uang;
        $_total_biaya_yg_harus_ditanggung+=$biaya;
        $table->addColumn("",1,1);
        $table->addColumn("Dokter DPJP",1,1,null,null,"left");
        $table->addColumn(ArrayAdapter::format("money Rp. ",$biaya),2,1);
        $table->commit("body");
    }
    

//perawatan
$table->addSpace(4,1,"body");
$table->addColumn("TINDAKAN KEPERAWATAN",4,1,null,null,"bold");
$table->commit("body");
    //icu
    $list_ruang_pakem=array();
    $list_ruang_pakem['IGD']                        = $_FORMULA['perawat_igd'];
    $list_ruang_pakem['ICU']                        = $_FORMULA['perawat_icu'];
    $list_ruang_pakem['OK']                         = $_FORMULA['perawat_ok'];
    $list_ruang_pakem['IRNA G']                     = $_FORMULA['perawat_irna_g'];
    $list_ruang_pakem['Recovery Room dan Anastesi'] = $_FORMULA['perawat_anastesi']+$_FORMULA['perawat_recovery_room'];
    
    foreach($list_ruang_pakem as $k=>$v){
        if($v!=0){
            $uang=($v)*$_basis_persen;
            $_total_biaya_yg_harus_ditanggung+=$uang;
            $table->addColumn($k,1,1);
            $table->addColumn("-",1,1,null,null,"left");
            $table->addColumn(ArrayAdapter::format("money Rp. ",$uang),2,1);
            $table->commit("body");
        }
    }
    
    //ruangan tidak pakem, berdasarkan lama rawat masing-masing ruangan
    $pembagi=abs($rumus['irna_a']+$rumus['irna_b']+$rumus['irna_b1']+$rumus['irna_c']+$rumus['irna_e']+$rumus['irna_f']+$rumus['paviliun']);
    $persen=$_FORMULA['perawat_ruangan']/$pembagi;
    $list_ruangan=array();
    $list_ruangan['IRNA A']=abs($rumus['irna_a']);
    $list_ruangan['IRNA B']=abs($rumus['irna_b']);
    $list_ruangan['IRNA B1']=abs($rumus['irna_b1']);
    $list_ruangan['IRNA C']=abs($rumus['irna_c']);
    $list_ruangan['IRNA E']=abs($rumus['irna_e']);
    $list_ruangan['IRNA F']=abs($rumus['irna_f']);
    $list_ruangan['Paviliun']=abs($rumus['paviliun']);
    foreach($list_ruangan as $k=>$v){
        if($v!=0){
            $uang=ceil($persen*$v*$_basis_persen);
            $_total_biaya_yg_harus_ditanggung+=$uang;
            $table->addColumn($k,1,1);
            $table->addColumn("-",1,1,null,null,"left");
            $table->addColumn(ArrayAdapter::format("money Rp. ",$uang),2,1);
            $table->commit("body");
        }
    }
    

/* PENUNJANG */
$table  ->addSpace(4,1,"body")
        ->addColumn("PENUNJANG",4,1,null,null,"bold")
        ->commit("body");
    //laboratory
    $lab=$_FORMULA['laboratory']*$_basis_persen;
    $_total_biaya_yg_harus_ditanggung+=$lab;
    $table  ->addColumn("Laboratory",1,1)
            ->addColumn("-",1,1,null,null,"left")
            ->addColumn(ArrayAdapter::format("money Rp. ",$lab),2,1)
            ->commit("body");
    //radiology
    $rad=$_FORMULA['radiology']*$_basis_persen;
    $_total_biaya_yg_harus_ditanggung+=$rad;
    $table  ->addColumn("Radiology",1,1)
            ->addColumn("-",1,1,null,null,"left")
            ->addColumn(ArrayAdapter::format("money Rp. ",$rad),2,1)
            ->commit("body");

/*END - MEMASUKAN DETAIL RUMUS*/

$table  ->addSpace(4,1,"body")
        ->addColumn("Total Tagihan",1,1)
        ->addColumn("-",1,1,null,null,"left")
        ->addColumn(ArrayAdapter::format("money Rp. ",$_total_biaya_yg_harus_ditanggung),2,1)
        ->commit("body");

$table  ->addColumn("Asuransi",1,1)
        ->addColumn($_ASURANSI,1,1)
        ->addColumn(ArrayAdapter::format("money Rp. ",$rumus['total_potongan']),2,1)
        ->commit("body");

$_DITANGGUNG=$_total_biaya_yg_harus_ditanggung-$rumus['total_potongan'];

if($_CARABAYAR=="BPJS"){
    $table  ->addColumn("Sharing BPJS",1,1)
            ->addColumn("-",1,1)
            ->addColumn(ArrayAdapter::format("money Rp. ",$_DITANGGUNG),2,1,null,null,"upperscore bold")
            ->commit("body");    
}

$table  ->addColumn("Tagihan Pasien",1,1)
        ->addColumn("-",1,1,null,null,"left")
        ->addColumn(ArrayAdapter::format("money Rp. ",$_DITANGGUNG),2,1,null,null,"upperscore bold")
        ->commit("body");
$table  ->addSpace("4","1")
        ->commit("body");
$table  ->addColumn("PEMBAYARAN",2,1)
        ->addColumn(ArrayAdapter::format("money Rp. ", $_NILAI_BAYAR),2,1)
        ->commit("body");
loadLibrary("smis-libs-function-math");

$table  ->addColumn("TERBILANG : ",1,1,null,null,"upperscore underscore bold")
        ->addColumn(strtoupper(numbertell($_NILAI_BAYAR)." rupiah"),3,1,null,null,"upperscore  underscore bold")
        ->commit("body");

$_TANGGUNGAN=$_total_biaya_yg_harus_ditanggung;
/*KODE TANDA TANGAN*/

global $user;
$kota=getSettings($db, "cashier-simple-kwitansi-town", "");
$table  ->addSpace(3,1)
        ->addColumn( $kota.", ".ArrayAdapter::format("date d M Y", date("Y-M-d")),1,1,NULL,NULL,"center")
        ->commit("body");
$table  ->addColumn("</br></br>", 4, 1,NULL,NULL,"center")
        ->commit("body");
$table  ->addSpace(3, 1)
        ->addColumn($user->getNameOnly(), 1, 1,NULL,NULL,"center")
        ->commit("body");

echo "<div>".$form->getHtml()."</div>";
echo "<div class='clear line'></div>";
echo "<div id='cetak_kwitansi_rumus_detail_area'>".$table->getHtml()."</div>";
echo addCSS("kasir/resource/css/kwitansi_print.css",false);
echo addJS("kasir/resource/js/kwitansi_print.js",false);
echo addJS("kasir/resource/js/kwitansi_rumus_detail.js",false);
?>