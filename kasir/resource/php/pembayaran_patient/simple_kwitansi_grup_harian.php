<?php
/**
 * this create a kwitansi using the normal data
 * but separate based on it's daily base 
 * and group base on name base
 * 
 * @author : Nurul Huda
 * @license : LGPLv3
 * @copyright : goblooge@gmail.com
 * @since : 21 Des 2017
 * @version : 1.0.0
 * @database :  - smis_ksr_bayar
 *              - smis_ksr_kolektif
 * */

global $db;
global $user;
require_once "kasir/function/get_patient_by_noreg.php";
require_once "kasir/function/get_system_setup.php";
require_once "kasir/function/get_patient_diagnosa.php";
require_once "kasir/function/get_patient_operasi.php";
require_once ("smis-base/smis-include-service-consumer.php");
$_noreg      = $_POST ['noreg_pasien'];
$_px         = get_patient_by_noreg($db,$_noreg);
$_setup      = get_system_setup($db,$user,$_noreg,$_px);
$_diagnosa   = get_patient_diagnosa($db,$_noreg,$_px);
$_operasi    = get_patient_operasi($db,$_noreg);

$header = "<div  style='font-size:18px;line-height:24px;'>PEMERINTAH KABUPATEN KEDIRI</br>DINAS KESEHATAN</br>UOBK RSUD SLG</div>
<div style='font-size:13px; line-height:18px;'>Jalan Galuh Candra Kirana Ds. Tugurejo Kec. Ngasem 
    </br> Telp. (0354) 2891400 Fax. (0354) 2891414 email: <u>rsud_slg@kedirikab.go.id</u>
    </br> K E D I R I
</div>";
$logo="<img src='kasir/resource/image/logokediri.png'  style='width:100px;height:auto;'> ";
	
$tp      = new TablePrint("cetak_kwitansi_grup_bpjs");
$tp_head = $tp;
$tp_head ->setDefaultBootrapClass(false)
         ->setMaxWidth(false)
         ->setTableClass("kwitansi_print")
         ->addColumn ( $logo, 1, 1,NULL,"","" )
         ->addColumn($header , 5, 1,NULL,NULL,"center bold")
         ->commit("body");
$tp_head ->addColumn("Nama / PJ / Tlp.", 1, 1,NULL,NULL,"left ltop")
         ->addColumn(" : ".$_px['nama_pasien']." / ".$_px['namapenanggungjawab']." / ".$_px['telponpenanggungjawab'], 2, 1,NULL,NULL,"left ltop ")
         ->addColumn("NRM", 1, 1,NULL,NULL,"left ltop")
         ->addColumn(" : ".ArrayAdapter::format("only-digit8", $_px['nrm']), 2, 1,NULL,NULL,"left  ltop")
         ->commit("body");
$tp_head ->addColumn("Kelamin", 1, 1,NULL,NULL,"left")
         ->addColumn(" : ".$_setup['kelamin'], 2, 1,NULL,NULL,"left")
         ->addColumn("No. Reg", 1, 1,NULL,NULL,"left")
         ->addColumn(" : ".ArrayAdapter::format("only-digit8", $_px['id']), 2, 1,NULL,NULL,"left")
         ->commit("body");
$tp_head ->addColumn("Alamat", 1, 1,NULL,NULL,"left")
         ->addColumn(" : ".$_px['alamat_pasien'], 2, 1,NULL,NULL,"left ")
         ->addColumn("Jenis Perawatan", 1, 1,NULL,NULL,"left")
         ->addColumn(" : ".($_px['uri']*1==0?"Rawat Jalan":"Rawat Inap"), 2, 1,NULL,NULL,"left ")
         ->commit("body");
$tp_head ->addColumn("Tanggal Lahir", 1, 1,NULL,NULL,"left")
         ->addColumn(" : ".ArrayAdapter::format("date d M Y",$_px['tgl_lahir']), 2, 1,NULL,NULL,"left ")
         ->addColumn("Ruang", 1, 1,NULL,NULL,"left")
         ->addColumn(" : ".ArrayAdapter::format("unslug",($_px['uri']*1==0?$_px['jenislayanan']:$_px['kamar_inap'])), 2, 1,NULL,NULL,"left ")
         ->commit("body");
$tp_head ->addColumn("Umur", 1, 1,NULL,NULL,"left")
         ->addColumn(" : ".$_px['umur'], 2, 1,NULL,NULL,"left ")
         ->addColumn("Tanggal Jam Masuk", 1, 1,NULL,NULL,"left")
         ->addColumn(" <div class='ngedit' contenteditable='true'> ".ArrayAdapter::format("date d M Y H:i", $_setup['tgl_masuk'])."</div>", 2, 1,NULL,NULL,"left ")
         ->commit("body");
$tp_head ->addColumn("Jenis Pasien", 1, 1,NULL,NULL,"left")
         ->addColumn(" : ".ArrayAdapter::format("unslug", $_px['carabayar'])." / ".$_px['nama_asuransi'],2, 1,NULL,NULL,"left ")
         ->addColumn("Tanggal Jam Keluar", 1, 1,NULL,NULL,"left")
         ->addColumn(" <div class='ngedit' contenteditable='true'> ".ArrayAdapter::format("date d M Y H:i", $_setup['tgl_pulang'])."</div>", 2, 1,NULL,NULL,"left ")
         ->commit("body");
$tp_head ->addColumn("No Peserta", 1, 1,NULL,NULL,"left")
         ->addColumn(" : ".$_px['nobpjs'],2, 1,NULL,NULL,"left ")
         ->addColumn("Lama Rawat", 1, 1,NULL,NULL,"left")
         ->addColumn(" : ".day_diff($_setup['tgl_masuk'],$_setup['tgl_pulang'])." Hari ", 2, 1,NULL,NULL,"left ")
         ->commit("body");
$tp_head ->addColumn("Diagnosa Awal", 1, 1,NULL,NULL,"left")
         ->addColumn(" : ".$_diagnosa['diagnosa_awal'],5, 1,NULL,NULL,"left ");
$tp_head ->addSpace(6, 1)
         ->commit("body");

$tp_cont        = $tp;
$qv             = "SELECT id,urutan,nama_grup,
                   jenis_tagihan, tanggal, DATE(dari) as xdari,
                   if(jenis_tagihan LIKE 'tindakan%' AND nama_tagihan LIKE '% - %',substring_index(nama_tagihan,' - ',-1), nama_tagihan)  as vnama_tagihan,
                   ruangan, dari,sampai,
                   sum(quantity) as quantity, 
                   sum(total) as total FROM smis_ksr_kolektif 
                   WHERE 
                   noreg_pasien='".$_noreg."' 
                   AND prop!='del'
                   AND akunting_only=0
                   GROUP BY ruangan, vnama_tagihan, DATE(dari)
                   ORDER BY DATE(dari), ruangan, jenis_tagihan";
$list           = $db->get_result($qv,false);
$current_tgl    = "";
$TOTAL_TAGIHAN  = 0;
$day            = 0;
foreach($list as $x){
    if($current_tgl!=$x['xdari']){
        $day++;
        $current_tgl = $x['xdari'];
        $tp_cont ->addSpace(6, 1)
                 ->commit("body");
        $tp_cont ->addColumn("Hari Rawat Ke-".$day." - ".ArrayAdapter::format("date d M Y",$current_tgl), 6, 1,NULL,NULL,"left lbottom")
                 ->commit("body");
        $tp_cont ->addColumn("Ruangan", 1, 1,NULL,NULL,"left bold")
                 ->addColumn("Tanggal", 1, 1,NULL,NULL,"left  bold")
                 ->addColumn("Tindakan / Obat", 1, 1,NULL,NULL,"left  bold")
                 ->addColumn("Harga", 1, 1,NULL,NULL,"left bold")
                 ->addColumn("Jumlah", 1, 1,NULL,NULL,"left bold")
                 ->addColumn("Sub Total", 1, 1,NULL,NULL,"left bold")
                 ->commit("body");
    }    
	$tp_cont ->addColumn("&#09;".ArrayAdapter::format("unslug", $x['ruangan']), 1, 1)
             ->addColumn( $x['tanggal'], 1, 1)
             ->addColumn(ArrayAdapter::format("unslug", $x['vnama_tagihan']), 1, 1)
             ->addColumn(ArrayAdapter::format("money Rp.", $x['total']/$x['quantity']), 1, 1)
             ->addColumn(" x ". $x['quantity'], 1, 1)
             ->addColumn(ArrayAdapter::format("money Rp.", $x['total']), 1, 1)
             ->commit("body");
	$TOTAL_TAGIHAN+=$x['total'];
}

loadLibrary("smis-libs-function-math");
$round = getSettings($db,"smis-simple-rounding-uang","-1");
$model = getSettings($db,"smis-simple-rounding-model","round");
$TOTAL_TAGIHAN=smis_money_round($TOTAL_TAGIHAN,$round,$model);

$tp_cont ->addSpace(6, 1)
         ->commit("body");        
$tp_cont ->addColumn("Total Tagihan ", 5, 1,NULL,NULL,"bold right ltop lbottom")
         ->addColumn(ArrayAdapter::moneyFormat("money Rp.",$TOTAL_TAGIHAN), 1, 1,NULL,NULL,"bold ltop lbottom")
         ->commit("body");

$tp_foot = $tp;
$tp_foot ->addSpace(6, 1)
         ->commit("body");
$tp_foot ->addColumn("Diagnosa Akhir Utama", 1, 1,NULL,NULL,"left")
         ->addColumn(" : ".$_diagnosa['diagnosa_akhir'],2, 1,NULL,NULL,"left ")
         ->addColumn("Dokter Penanggung Jawab Pasien", 1, 1,NULL,NULL,"left")
         ->addColumn(" : ".$_setup['dpjp1'], 2, 1,NULL,NULL,"left ")
         ->commit("body");
$tp_foot ->addColumn("Diagnosa Akhir Penyerta", 1, 1,NULL,NULL,"left")
         ->addColumn(" : ".$_diagnosa['diagnosa_penyerta_1'],2, 1,NULL,NULL,"left ")
         ->addSpace(1, 1)
         ->addColumn(" : ".$_setup['dpjp2'], 2, 1,NULL,NULL,"left ")
         ->commit("body");
$tp_foot ->addSpace(1, 1)
         ->addColumn(" : ".$_diagnosa['diagnosa_penyerta_2'],2, 1,NULL,NULL,"left ")
         ->addColumn("Perawat Ruangan", 1, 1,NULL,NULL,"left")
         ->addColumn(" : ".(isset($_POST['user'])?$_POST['user']:$_setup['pencetak']), 2, 1,NULL,NULL,"left ")
         ->commit("body");
$tp_foot ->addColumn("Jenis Tindakan ",1, 1,NULL,NULL,"left ")
         ->addColumn(" : ".$_operasi['operasi_1'],2, 1,NULL,NULL,"left ")
         ->addColumn("Pelaksana Verivikasi ", 1, 1,NULL,NULL,"left")
         ->addColumn(" : ".getSettings($db,"cashier-pelaksana-verivicator",""), 2, 1,NULL,NULL,"left ")
         ->commit("body");
$tp_foot ->addSpace(1, 1,NULL,NULL,"left ")
         ->addColumn(" : ".$_operasi['operasi_2'],2, 1,NULL,NULL,"left ")
         ->addColumn("Keterangan Keluar ", 1, 1,NULL,NULL,"left")
         ->addColumn(" : ".$_px['carapulang'], 2, 1,NULL,NULL,"left ")
         ->commit("body");
$tp_foot ->addSpace(6, 1)
         ->commit("body");
$tp_foot ->addSpace(4, 1,NULL,NULL,"left ")
         ->addColumn($_setup['kota']." ".ArrayAdapter::dateFormat("date d M Y",$_setup['tgl_pulang']),2, 1,NULL,NULL,"left ")
         ->commit("body");
$tp_foot ->addSpace(4, 1,NULL,NULL,"left ")
         ->addColumn("Penanggung Jawab",2, 1,NULL,NULL,"left ")
         ->commit("body");
$tp_foot ->addSpace(4, 1,NULL,NULL,"left ")
         ->addColumn("Dokter yang Merawat",2, 1,NULL,NULL,"left ")
         ->commit("body");
$tp_foot ->addSpace(6, 1,NULL,NULL,"left ")
         ->commit("body");
$tp_foot ->addSpace(6, 1,NULL,NULL,"left ")
         ->commit("body");
$tp_foot ->addSpace(4, 1,NULL,NULL,"left ")
         ->addColumn("",2, 1,NULL,NULL,"left lbottom")
         ->commit("body");

require_once "kasir/function/kwitansi_print_button.php";
echo kwitansi_print_button($db,"simple_kwitansi_grup_bpjs",$_px);
echo "<div id='cetak_simple_kwitansi_grup_bpjs_area'>".$tp->getHtml()."</div>";
echo addCSS("kasir/resource/css/kwitansi_print.css",false);
echo addJS("kasir/resource/js/kwitansi_print.js",false);
require_once "kasir/function/end_css_kwitansi.php";
echo end_css_kwitansi($db,"simple_kwitansi_bpjs");

?>
