<?php
global $db;
require_once 'smis-base/smis-include-service-consumer.php';
require_once 'smis-libs-class/ServiceProviderList.php';


/* GRUP SUPER COMMAND */
if(isset($_POST['super_command'])){
    $super=new SuperCommand();
    if($_POST['super_command']=="tarif_edit_tindakan_dokter"){
        $dktable = new Table ( array ("Nama","Kelas","Pasien","Tarif"), "", NULL, true );
		$dktable->setName ( "tarif_edit_tindakan_dokter" )
				->setModel ( Table::$SELECT );
		$dkadapter = new SimpleAdapter ();
		$dkadapter->add ( "Nama", "nama" )
				  ->add ( "Kelas", "kelas", "unslug" )
				  ->add ( "Pasien", "jenis_pasien", "unslug" )
				  ->add ( "Tarif", "tarif", "money Rp." );
		
		$tarif = new ServiceResponder ( $this->db, $dktable, $dkadapter, "get_tindakan_dokter" );
		$super->addResponder ( "tarif_edit_tindakan_dokter", $tarif );
    }
    
    $data=$super->initialize();
    if($data!=null){
        echo $data;
        return;
    }
}
/* END - GRUP SUPER COMMAND */




$uitable = new Table ( array ('Tanggal','Tindakan','Dokter',"Biaya Dokter","Perawat","Biaya perawat" ), "", NULL, true );
$uitable->setName ( "edit_tindakan_dokter" );

if(isset($_POST['header'])){
    $header=json_decode($_POST['header'],true);
    $uitable->setHeader($header);
}



if (isset ( $_POST ['command'] )) {
	$adapter = new SimpleAdapter ();
	$adapter->add ( "Tindakan", "nama_tindakan" )
            ->add ( "Dokter", "nama_dokter" )
            ->add ( "Biaya Dokter", "harga", "money Rp." )
            ->add ( "Perawat", "nama_perawat" )
            ->add ( "Biaya perawat", "harga_perawat","money Rp." )
            ->add ( "Tanggal", "waktu", "date d M Y H:i" );
	$dbres = new ServiceResponder($db, $uitable, $adapter, "edit_tindakan_dokter",$_POST["ruang"]);
	$data = $dbres->command ( $_POST ['command'] );
	echo json_encode ( $data );
	return;
}
$noreg_pasien="";
if( isset($_POST['noreg']) && $_POST['noreg']!="" || isset($_POST['noreg_pasien']) && $_POST['noreg_pasien']!="" ){
    $noreg_pasien=isset($_POST['noreg'])?$_POST['noreg']:$_POST['noreg_pasien'];
}

$df_ruangan="";
$jumlah_perawat=1;
if(isset($_POST['ruangan']) && $_POST['ruangan']!=""){
    $df_ruangan=$_POST['ruangan'];
    $serv=new ServiceConsumer($db,"setup_tindakan_dokter",NULL,$df_ruangan);
    $serv->execute();
    $content=$serv->getContent();
    $jumlah_perawat=$content['jumlah_perawat'];
}

$service=new ServiceProviderList($db,"edit_tindakan_dokter",$df_ruangan);
$service->execute();
$ruangan=$service->getContent();
	
$uitable->addModal("nama_pasien",  "hidden","", "","",NULL,true);
$uitable->addModal("nrm_pasien", "hidden", "", "","",NULL,true);
$uitable->addModal("noreg_pasien", "hidden","",  $noreg_pasien,"",NULL,true);
$uitable->addModal("carabayar", "hidden","",  "","",NULL,true);
$uitable->addModal("ruang", "select", "Ruangan", $ruangan);
$form=$uitable->getModal()->setTitle("Pasien")->getForm();
$uitable->clearContent();

$uitable->addModal ( "id", "hidden", "",  ""  )
        ->addModal ( "waktu", "datetime", "Waktu", date ( "Y-m-d H:i" ) )
        ->addModal ( "nama_tindakan", "chooser-edit_tindakan_dokter-tarif_edit_tindakan_dokter-Tarif Tindakan Dokter", "Tindakan", "" ,"n",null,false,null,true)
        ->addModal ( "nama_dokter", "chooser-edit_tindakan_dokter-dokter_edit_tindakan_dokter-Pilih Dokter", "Dokter", $this->last_setup['dokter_nama'],"n",null,false,null,true   )
        ->addModal ( "id_dokter", "hidden", "","" )
        ->addModal ( "jenis_dokter", "hidden", "", "" )
        ->addModal ( "harga", "money", "Biaya", "","n",null,false,null,false,"nama_perawat")
        ->addModal ( "harga_perawat", "money", "Biaya Perawat", "","y",null,false,null,false,"save" );
        
$number=array("I"=>"",
            "II"=>"_dua",
            "III"=>"_tiga",
            "IV"=>"_empat",
            "V"=>"_lima",
            "VI"=>"_enam",
            "VII"=>"_tujuh",
            "VIII"=>"_delapan",
            "IX"=>"_sembilan",
            "X"=>"_sepuluh"
            );
$numero=1;
foreach($number as $rum=>$num){
    if($numero<=$jumlah_perawat){
        $uitable->addModal ( "nama_perawat".$num, "chooser-edit_tindakan_dokter-perawat_edit_tindakan_dokter".$num."-Pilih Perawat ".$rum, "Perawat ".$rum, $this->last_setup['nama_perawat'.$num] );	
    }else{
        $uitable->addModal ( "nama_perawat".$num, "hidden", "", "","n",null,false,null,false);
    }
    $uitable->addModal ( "id_perawat".$num, "hidden", "", "" );
    $numero++;
}
        
echo $uitable->getModal()->setTitle("Tindakan Dokter")->getHtml();
echo $form ->getHtml();
echo $uitable->getHtml ();
echo addJS ( "framework/smis/js/table_action.js" );
echo addJS ( "framework/bootstrap/js/bootstrap-datetimepicker.js" );
echo addCSS ( "framework/bootstrap/css/bootstrap-datetimepicker.css" );
echo addJS ( "kasir/resource/js/edit_tindakan_dokter.js",false );

?>

