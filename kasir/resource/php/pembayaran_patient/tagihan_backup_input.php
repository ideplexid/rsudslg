<?php 
require_once 'smis-libs-class/MasterTemplate.php';
require_once "kasir/class/dbtable/TambahanDBTable.php";
global $db;
global $user;
if(isset($_POST['super_command']) && $_POST['super_command']=="tagihan_backup_input"){
	unset($_POST['super_command']);
}

$jenis=new OptionBuilder();
$tagihan_backup_input=new MasterTemplate($db, "smis_ksr_tagihan", "kasir", "tagihan_backup_input");
$dbtable = new TambahanDBTable($db,"smis_ksr_tagihan");
$dbtable ->setAutoSynch(getSettings($db,"cashier-real-time-tagihan","0")!="0");
$tagihan_backup_input ->setDBtable($dbtable);
$tagihan_backup_input->addJSColumn("id",true);
$tagihan_backup_input->addJSColumn("jenis_tagihan");
$tagihan_backup_input->addJSColumn("nama_tagihan");
$tagihan_backup_input->addJSColumn("ruangan");
$tagihan_backup_input->addJSColumn("keterangan");
$tagihan_backup_input->addJSColumn("tanggal");
$tagihan_backup_input->addJSColumn("nilai");
$tagihan_backup_input->addJSColumn("jaspel");
$tagihan_backup_input->addJSColumn("total");
$tagihan_backup_input->addJSColumn("subtotal");
$tagihan_backup_input->addJSColumn("jumlah");

$tagihan_backup_input->setDateEnable(true)
					 ->getDBtable()
					 ->setOrder(" ruangan ASC, tanggal ASC ");

if(isset($_POST['noreg_pasien'])){
	$tagihan_backup_input->getDBtable()
						 ->addCustomKriteria("noreg_pasien", "='".$_POST['noreg_pasien']."'");	
}

$noreg="";
if(isset($_POST['noreg_pasien'])){
	$noreg=$_POST['noreg_pasien'];
}
$button=new Button("", "", "Calculate");
$button->setIcon("fa fa-calculator");
$button->setIsButton(Button::$ICONIC_TEXT);
$button->setAction(" list_registered.autoload('".$noreg."') ");
$button->setClass("btn-primary");

$uitable=$tagihan_backup_input->getUItable();
$uitable->setReloadButtonEnable(false);
$uitable->setPrintButtonEnable(false);
$uitable->addFooterButton($button);
$header=array("Jenis","Ruangan","Nama","Keterangan","Tanggal","Nilai","Jumlah","Total","Jaspel","Operator");
$uitable->setHeader($header);

loadLibrary("smis-libs-function-medical");
$replace=medical_service();
$op=new OptionBuilder();
foreach($replace as $id=>$name){
	$op->add($name,$id);
}

$enable_tagihan = getSettings($db,"cashier-tambahan-biaya-enabled-tagihan","1")=="1";
$enable_jenis   = getSettings($db,"cashier-tambahan-biaya-enabled-jenis","1")=="1";
require_once "kasir/class/MapRuangan.php";
if($tagihan_backup_input->getDBResponder()->isPreload()){
    require_once "kasir/function/get_ruang_daftar.php";
    
	$uitable->addModal("kredit", "hidden", "", "")
            ->addModal("debet", "hidden", "", "")
            ->addModal("id", "hidden", "", "")
			->addModal("tanggal", "date", "Tanggal", "","n",NULL,false,NULL,false,"ruangan")
			->addModal("nama_tagihan","chooser-tagihan_backup_input-tagihan_backup_master-Tagihan", "Tagihan", "","n",NULL,!$enable_tagihan,NULL,false,"nilai")
			->addModal("ruangan", "select", "Ruangan", MapRuangan::getRuangan(),"n",NULL,false,NULL,true,"jenis_tagihan")
			->addModal("jenis_tagihan", "select", "Jenis Tagihan", $op->getContent(),"n",NULL,!$enable_jenis,NULL,false,"nama_tagihan")
			->addModal("nilai", "money", "Biaya", "","n",NULL,false,NULL,false,"jumlah")
			->addModal("jumlah", "text", "Jumlah", "1","y",NULL,false,NULL,false,"subtotal")
			->addModal("subtotal", "money", "Total", "0","y",NULL,true,NULL,false,"jaspel")
			->addModal("jaspel", "checkbox", "Jaspel", "0","y",NULL,false,NULL,false,"keterangan")
			->addModal("keterangan","text", "Keterangan", "","y",NULL,false,NULL,false,"save");
	
	$tagihan_backup_input->setModalTitle("Tambah Tagihan");
	$modal=$tagihan_backup_input->getModal();
	$modal->setComponentSize(Modal::$MEDIUM);
}
require_once "kasir/class/adapter/RuanganUnslugAdapter.php";
$adapter = new RuanganUnslugAdapter();
$adapter->add("Jenis", "jenis_tagihan","unslug")
		//->add("Ruangan", "ruangan","unslug")
		->add("Nama","nama_tagihan")
		->add("Tanggal","tanggal")
		->add("Keterangan","keterangan")
        ->add("Operator","operator")
		->add("Jaspel","jaspel","trivial_1_V_-")
		->add("Keterangan","keterangan")
		->add("Nilai", "nilai","money Rp.")
		->add("Jumlah", "jumlah")
		->add("Total", "total","money Rp.");
$tagihan_backup_input->setAdapter($adapter);

if($tagihan_backup_input->getDBResponder()->isSave()){
	$total=$_POST['nilai']*$_POST['jumlah'];
	$tagihan_backup_input->getDBResponder()->addColumnFixValue("total", $total);
    $tagihan_backup_input->getDBResponder()->addColumnFixValue("operator", $user->getNameOnly());
}
$tagihan_backup_input->addResouce("js", "kasir/resource/js/tagihan_backup_input.js");
$tagihan_backup_input->setMultipleInput(true);
$tagihan_backup_input->setNextEnter(true);
$tagihan_backup_input->setAutofocus(true);
$tagihan_backup_input->setAutofocusOnMultiple("nama_tagihan");
$tagihan_backup_input->addNoClear("ruangan");
$tagihan_backup_input->addNoClear("jenis_tagihan");


$header=array ('Nama','Ruangan',"Jenis","Harga" ,"Jaspel");
$dbtable=new DBTable($db,"smis_ksr_tagihan_tambahan");
$uitable = new Table ( $header );
$uitable->setName ( "tagihan_backup_master" );
$uitable->setModel ( Table::$SELECT );
$adapter = new SimpleAdapter ();
$adapter->add ( "Nama", "nama" );
$adapter->add ( "Jenis", "jenis" );
$adapter->add ( "Ruangan", "ruangan", "unslug" );
$adapter->add ( "Harga", "harga","money Rp." );
$adapter->add ( "Jaspel", "jaspel", "trivial_1_x_" );
$responder = new DBResponder ( $dbtable, $uitable, $adapter );
$tagihan_backup_input->getSuperCommand()->addResponder("tagihan_backup_master", $responder);
$tagihan_backup_input->addSuperCommand("tagihan_backup_master", array());
$tagihan_backup_input->addSuperCommandArray("tagihan_backup_master", "id_tagihan", "id");
$tagihan_backup_input->addSuperCommandArray("tagihan_backup_master", "jenis_tagihan", "jenis");
$tagihan_backup_input->addSuperCommandArray("tagihan_backup_master", "nama_tagihan", "nama");
$tagihan_backup_input->addSuperCommandArray("tagihan_backup_master", "keterangan", "keterangan");
$tagihan_backup_input->addSuperCommandArray("tagihan_backup_master", "nilai", "biaya");
$tagihan_backup_input->addSuperCommandArray("tagihan_backup_master", "jaspel", "jaspel");
$tagihan_backup_input->addSuperCommandArray("tagihan_backup_master", "ruangan", "ruangan");
$tagihan_backup_input->addSuperCommandArray("tagihan_backup_master", "debet", "debet");
$tagihan_backup_input->addSuperCommandArray("tagihan_backup_master", "kredit", "kredit");
$tagihan_backup_input->initialize();
?>