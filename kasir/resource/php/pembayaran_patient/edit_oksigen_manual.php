<?php
global $db;
require_once 'smis-base/smis-include-service-consumer.php';
require_once 'smis-libs-class/ServiceProviderList.php';

$header=array ('Waktu',"Harga (H)","Liter (L)",'Biaya Lain (B)','Total (H*L+B)');
$uitable = new Table ( $header, "", NULL, true );
$uitable->setName ( "edit_oksigen_manual" );

if (isset ( $_POST ['command'] )) {
    require_once "kasir/class/adapter/OksigenManualAdapter.php";
	$adapter = new OksigenManualAdapter ();
	$dbres = new ServiceResponder($db, $uitable, $adapter, "edit_oksigen_manual",$_POST["ruang"]);
	$data = $dbres->command ( $_POST ['command'] );
	echo json_encode ( $data );
	return;
}

$noreg_pasien="";
if( isset($_POST['noreg']) && $_POST['noreg']!="" || isset($_POST['noreg_pasien']) && $_POST['noreg_pasien']!="" ){
    $noreg_pasien=isset($_POST['noreg'])?$_POST['noreg']:$_POST['noreg_pasien'];
}


$service=new ServiceProviderList($db,"edit_oksigen_manual");
$service->execute();
$ruangan=$service->getContent();

$uitable->addModal("nama_pasien",  "hidden","", "","",NULL,true);
$uitable->addModal("alamat_pasien",  "hidden","", "","",NULL,true);
$uitable->addModal("nrm_pasien", "hidden", "", "","",NULL,true);
$uitable->addModal("noreg_pasien", "hidden","",  $noreg_pasien,"",NULL,true);
$uitable->addModal("carabayar", "hidden","",  "","",NULL,true);
$uitable->addModal("ruang", "select", "Ruangan", $ruangan);
$form=$uitable->getModal()->setTitle("Pasien")->getForm();
$uitable->clearContent();
$uitable->addModal("jumlah_liter", "text", "Jumlah (Liter)", "","n",null,false,null,true,"biaya_lain");
$uitable->addModal("biaya_lain", "money", "Biaya Manometer", "","n",null,false,null,false,"save");
$uitable->addModal("id", "hidden", "", "");

echo $uitable->getModal()->setTitle("O2 manual")->getHtml();
echo $form ->getHtml();
echo $uitable->getHtml ();
echo addJS ( "framework/bootstrap/js/bootstrap-datetimepicker.js" );
echo addCSS ( "framework/bootstrap/css/bootstrap-datetimepicker.css" );
echo addJS ( "framework/smis/js/table_action.js" );
echo addJS ( "kasir/resource/js/edit_oksigen_manual.js",false );

?>
