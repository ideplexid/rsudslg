<?php
global $db;
require_once 'smis-base/smis-include-service-consumer.php';
require_once 'smis-libs-class/ServiceProviderList.php';


/* GRUP SUPER COMMAND */
if(isset($_POST['super_command'])){
    $super=new SuperCommand();
    if($_POST['super_command']=="tarif_konsul"){
        $head=array ('Nama','Jabatan',"Kelas","Tarif" );
		$dkadapter = new SimpleAdapter ();
		if($_POST['separated']=="1") 
			$dkadapter->add ( "Nama", "nama_konsul" );
		else 
			$dkadapter->add ( "Nama", "nama_dokter" );
		$dkadapter->add ( "Jabatan", "jabatan", "unslug" );
		$dkadapter->add ( "Kelas", "kelas", "unslug" );
		$dkadapter->add ( "Tarif", "tarif", "money Rp." );		
		$dktable = new Table ( $head);
		$dktable->setName ( "tarif_konsul" );
		$dktable->setModel ( Table::$SELECT );		
		$tarif = new ServiceResponder ( $db, $dktable, $dkadapter, "get_konsul" );
		$super->addResponder ( "tarif_konsul", $tarif);
    }
    
    $data=$super->initialize();
    if($data!=null){
        echo $data;
        return;
    }
}
/* END - GRUP SUPER COMMAND */




$uitable = new Table ( NULL, "", NULL, true );
$uitable->setName ( "edit_konsul" );

if(isset($_POST['header'])){
    $header=json_decode($_POST['header'],true);
    $uitable->setHeader($header);
}



if (isset ( $_POST ['command'] )) {
	$adapter = new SimpleAdapter ();
	$adapter->add ( "Tanggal", "waktu", "date d M Y H:i" );
    $adapter->add ( "Dokter", "nama_dokter" );
    $adapter->add ( "Kelas", "kelas", "unslug" );
    $adapter->add ( "Harga", "harga", "money Rp." );
    $adapter->add ( "Konsul", "nama_konsul" );
	$dbres = new ServiceResponder($db, $uitable, $adapter, "edit_konsul",$_POST["ruang"]);
	$data = $dbres->command ( $_POST ['command'] );
	echo json_encode ( $data );
	return;
}
$noreg_pasien="";
if( isset($_POST['noreg']) && $_POST['noreg']!="" || isset($_POST['noreg_pasien']) && $_POST['noreg_pasien']!="" ){
    $noreg_pasien=isset($_POST['noreg'])?$_POST['noreg']:$_POST['noreg_pasien'];
}

$df_ruangan="";
$is_separated=0;
$jumlah_dokter=1;
$header_element=null;
if(isset($_POST['ruangan']) && $_POST['ruangan']!=""){
    $df_ruangan=$_POST['ruangan'];
    $serv=new ServiceConsumer($db,"setup_konsul",NULL,$df_ruangan);
    $serv->execute();
    $content=$serv->getContent();
    $is_separated=$content['separated'];
    $header_element=$content['header'];
    $uitable->setHeader($header_element);
}

$service=new ServiceProviderList($db,"edit_konsul",$df_ruangan);
$service->execute();
$ruangan=$service->getContent();

$uitable->addModal("nama_pasien",  "hidden","", "","",NULL,true);
$uitable->addModal("nrm_pasien", "hidden", "", "","",NULL,true);
$uitable->addModal("noreg_pasien", "hidden","",  $noreg_pasien,"",NULL,true);
$uitable->addModal("carabayar", "hidden","",  "","",NULL,true);
$uitable->addModal("ruang", "select", "Ruangan", $ruangan);
$form=$uitable->getModal()->setTitle("Pasien")->getForm();
$uitable->clearContent();

$uitable->addModal ( "id", "hidden", "",  ""  );	
$uitable->addModal ( "waktu", "datetime", "Tanggal",  date("Y-m-d H:i")  );
if($is_separated=="1"){
    $uitable->addModal ( "nama_dokter", "chooser-edit_konsul-dokter_konsul-Pilih Dokter", "Dokter", "","n",null,false,null,true );
    $uitable->addModal ( "nama_konsul", "chooser-edit_konsul-tarif_konsul-Pilih konsul", "Konsul", "" ,"n",null,false,null,false);
}else{
    $uitable->addModal ( "nama_konsul", "hidden", "", "" ,"y",null,false,null,true);
    $uitable->addModal ( "nama_dokter", "chooser-edit_konsul-tarif_konsul-Pilih konsul", "Dokter", "","n",null,false,null,false );
}

$uitable->addModal ( "id_dokter", "hidden", "", "0" );
$uitable->addModal ( "id_konsul", "hidden", "", "0" );
$uitable->addModal ( "kelas", "text", "Kelas", "", 'n', null, true );
$uitable->addModal ( "harga", "money", "Harga", "", 'y', null, true );

$separated=new Hidden("is_konsul_separated", "", $is_separated);
$headelm=new Hidden("edit_konsul_header_element", "", json_encode($header_element));
echo $uitable->getModal()->setTitle("Konsul")->getHtml();
echo $form ->getHtml();
echo $uitable->getHtml ();
echo $separated->getHtml();
echo $headelm->getHtml();
echo addJS ( "framework/bootstrap/js/bootstrap-datetimepicker.js" );
echo addCSS ( "framework/bootstrap/css/bootstrap-datetimepicker.css" );
echo addJS ( "framework/smis/js/table_action.js" );
echo addJS ( "kasir/resource/js/edit_konsul.js",false );

?>
