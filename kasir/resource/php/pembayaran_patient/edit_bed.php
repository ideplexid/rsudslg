<?php
global $db;
require_once 'smis-base/smis-include-service-consumer.php';
require_once 'smis-libs-class/ServiceProviderList.php';

/* GRUP SUPER COMMAND */
if(isset($_POST['super_command'])){
    $super=new SuperCommand();
    if($_POST['super_command']=="nomor_bed"){
        $adapter = new SimpleAdapter ();
		$adapter->add ( "Nama", "nama" );
		$adapter->add ( "Status", "terpakai", "trivial_0_Kosong_Terpakai" );
		$adapter->add ( "Pasien", "nama_pasien" );
		$adapter->add ( "NRM", "nrm_pasien", "digit6" );		
		$header=array ("Nama","Status","Pasien","NRM");
		$bedtable = new Table ( $header, "Bed", NULL, true );
		$bedtable->setName ( "nomor_bed" );
		$bedtable->setModel ( Table::$SELECT );	
		$tarif = new ServiceResponder ( $db, $bedtable, $adapter, "get_bed_provider",$_POST['ruangan'] );
		$super->addResponder ( "nomor_bed", $tarif);
    }
    
    $data=$super->initialize();
    if($data!=null){
        echo $data;
        return;
    }
}
/* END - GRUP SUPER COMMAND */


$header=array ('Nama','Harga',"Masuk","Keluar","Hari","Total");
$uitable = new Table ( $header, "", NULL, true );
$uitable->setName ( "edit_bed" );

if (isset ( $_POST ['command'] )) {
	$adapter = new SimpleAdapter ();
    $adapter->add ( "Nama", "nama_bed" );
    $adapter->add ( "Tarif", "nama_tarif" );
    $adapter->add ( "Hari", "hari" );
    $adapter->add ( "Masuk", "waktu_masuk", "date d M Y H:i" );
    $adapter->add ( "Keluar", "waktu_keluar", "date d M Y H:i" );
    $adapter->add ( "Total", "biaya", "money Rp." );
    $adapter->add ( "Harga", "harga", "money Rp." );
	$dbres = new ServiceResponder($db, $uitable, $adapter, "edit_bed",$_POST["ruangan"]);
	$data = $dbres->command ( $_POST ['command'] );
	echo json_encode ( $data );
	return;
}

$noreg_pasien="";
if( isset($_POST['noreg']) && $_POST['noreg']!="" || isset($_POST['noreg_pasien']) && $_POST['noreg_pasien']!="" ){
    $noreg_pasien=isset($_POST['noreg'])?$_POST['noreg']:$_POST['noreg_pasien'];
}

$df_ruangan="";
$harga=0;
if(isset($_POST['ruangan']) && $_POST['ruangan']!=""){
    $df_ruangan=$_POST['ruangan'];
    /*service untuk mendapatkan harga bed*/
    $bed_harga_service = new ServiceConsumer ( $db, "get_bed", array ("ruangan" => $df_ruangan), "manajemen" );
    $bed_harga_service->execute ();
    $harga = $bed_harga_service->getContent ();
}

$service=new ServiceProviderList($db,"edit_bed",$df_ruangan);
$service->execute();
$ruangan=$service->getContent();

$uitable->addModal("nama_pasien",  "hidden","", "","",NULL,true);
$uitable->addModal("nrm_pasien", "hidden", "", "","",NULL,true);
$uitable->addModal("noreg_pasien", "hidden","",  $noreg_pasien,"",NULL,true);
$uitable->addModal("carabayar", "hidden","",  "","",NULL,true);
$uitable->addModal("ruang", "select", "Ruangan", $ruangan);
$form=$uitable->getModal()->setTitle("Pasien")->getForm();
$uitable->clearContent();

$uitable->addModal("id", "hidden", "", "","n");
$uitable->addModal("id_bed", "hidden", "", "","n");
$uitable->addModal("nama_bed", "chooser-edit_bed-nomor_bed-Nomor Bed", "Bed", "","y",null,true);
$uitable->addModal("waktu_masuk", "datetime", "Masuk", "","n");
$uitable->addModal("waktu_keluar", "datetime", "Keluar", "","n");
$uitable->addModal("harga", "money", "Biaya", $harga,"n");
				

echo $uitable->getModal()->setTitle("Bed")->getHtml();
echo $form ->getHtml();
echo $uitable->getHtml ();
echo addJS ( "framework/bootstrap/js/bootstrap-datetimepicker.js" );
echo addCSS ( "framework/bootstrap/css/bootstrap-datetimepicker.css" );
echo addJS ( "framework/smis/js/table_action.js" );
echo addJS ( "kasir/resource/js/edit_bed.js",false );

?>
