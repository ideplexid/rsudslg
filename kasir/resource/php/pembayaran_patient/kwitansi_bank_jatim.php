<?php 
show_error();
global $user;
require_once ("smis-base/smis-include-service-consumer.php");
require_once "kasir/function/get_patient_by_noreg.php";
require_once "kasir/function/get_system_setup.php";

$_noreg      = $_POST ['noreg_pasien'];
$_px         = get_patient_by_noreg($db,$_noreg);
$_setup      = get_system_setup($db,$user,$_noreg,$_px,"asuransi");


$dbtable    = new DBTable($db,"smis_ksr_bayar");
$bayar      = $dbtable ->select(array("noreg_pasien"=>$_noreg));
$id_billing = $bayar->idbilling;

$table  = new TablePrint("");
$table  ->setDefaultBootrapClass(false);
$table  ->setMaxWidth(true);

$table  ->addColumn("1.","1","1")
        ->addColumn("ID Billing","1","1")
        ->addColumn(":","1","1")
        ->addColumn($id_billing,"1","1")
        ->commit("header");
$table  ->addColumn("2.","1","1")
        ->addColumn("No RM","1","1")
        ->addColumn(":","1","1")
        ->addColumn(ArrayAdapter::format("only-digit8",$_px['nrm']),"1","1")
        ->commit("header");
$table  ->addColumn("3.","1","1")
        ->addColumn("Nama","1","1")
        ->addColumn(":","1","1")
        ->addColumn($_px['nama_pasien'],"1","1")
        ->commit("header");
$table  ->addColumn("4.","1","1")
        ->addColumn("Alamat","1","1")
        ->addColumn(":","1","1")
        ->addColumn($_px['alamat_pasien']." ".trim($_px['nama_kedusunan'])." ".($_px['nama_kelurahan'])."  ".($_px['nama_kecamatan'])." ".($_px['nama_kabupaten']),"1","1")
        ->commit("header");

if($_px['uri']=="1"){
    $table  ->addColumn("5.","1","1")
            ->addColumn("Total Pembayaran","1","1")
            ->addColumn(": Rp.","1","1")
            ->addColumn(ArrayAdapter::format("only-money",$bayar->nilai),"1","1")
            ->commit("body");
}else{

    $query       = "SELECT SUM(smis_ksr_kolektif.total) FROM smis_ksr_kolektif WHERE noreg_pasien='".$_noreg."' AND prop!='del' AND ruangan='registration' ";
    $pendaftaran = $db->get_var($query);
    
    $query       = "SELECT SUM(smis_ksr_kolektif.total) FROM smis_ksr_kolektif WHERE noreg_pasien='".$_noreg."' AND prop!='del' AND  ( jenis_tagihan='penjualan_resep' OR jenis_tagihan='return_resep')";
    $farmasi     = $db->get_var($query);    
    
    $query       = "SELECT SUM(smis_ksr_kolektif.total) FROM smis_ksr_kolektif WHERE noreg_pasien='".$_noreg."' AND prop!='del' AND  ( jenis_tagihan LIKE '%tindakan%')";
    $tindakan    = $db->get_var($query);    
    
    $query       = "SELECT SUM(smis_ksr_kolektif.total) FROM smis_ksr_kolektif WHERE noreg_pasien='".$_noreg."' AND prop!='del' AND  ( jenis_tagihan LIKE '%konsul')";
    $konsul      = $db->get_var($query);    
    
    $query       = "SELECT SUM(smis_ksr_kolektif.total) FROM smis_ksr_kolektif WHERE noreg_pasien='".$_noreg."' AND prop!='del' AND  ( jenis_tagihan LIKE 'konsultasi_dokter')";
    $periksa     = $db->get_var($query);    
    
    $query       = "SELECT SUM(smis_ksr_kolektif.total) FROM smis_ksr_kolektif WHERE noreg_pasien='".$_noreg."' AND prop!='del' AND  ( jenis_tagihan LIKE 'laboratory')";
    $laboratory  = $db->get_var($query);
    
    $query       = "SELECT SUM(smis_ksr_kolektif.total) FROM smis_ksr_kolektif WHERE noreg_pasien='".$_noreg."' AND prop!='del' AND  ( jenis_tagihan LIKE 'radiology')";
    $radiology   = $db->get_var($query);
    
    $query       = "SELECT SUM(smis_ksr_kolektif.total) FROM smis_ksr_kolektif WHERE noreg_pasien='".$_noreg."' AND prop!='del' AND  ( jenis_tagihan LIKE 'umum')";
    $lain        = $db->get_var($query);

    $query       = "SELECT SUM(smis_ksr_kolektif.total) FROM smis_ksr_kolektif WHERE noreg_pasien='".$_noreg."' AND prop!='del' ";
    $total       = $db->get_var($query);


    $penunjang   = $total - $pendaftaran -  $farmasi - $tindakan - $konsul - $periksa - $laboratory - $radiology - $lain;

    $table  ->addColumn("5.","1","1")
            ->addColumn("Pendaftararan","1","1")
            ->addColumn(": Rp.","1","1")
            ->addColumn(ArrayAdapter::format("only-money",$pendaftaran),"1","1")
            ->commit("body");
    $table  ->addColumn("6.","1","1")
            ->addColumn("Pemeriksaan Poli","1","1")
            ->addColumn(": Rp.","1","1")
            ->addColumn(ArrayAdapter::format("only-money",$periksa),"1","1")
            ->commit("body");
    $table  ->addColumn("7.","1","1")
            ->addColumn("Konsultasi Poli","1","1")
            ->addColumn(": Rp.","1","1")
            ->addColumn(ArrayAdapter::format("only-money",$konsul),"1","1")
            ->commit("body");
    $table  ->addColumn("8.","1","1")
            ->addColumn("Tindakan","1","1")
            ->addColumn(": Rp.","1","1")
            ->addColumn(ArrayAdapter::format("only-money",$tindakan),"1","1")
            ->commit("body");
    $table  ->addColumn("9.","1","1")
            ->addColumn("Laboratorium","1","1")
            ->addColumn(": Rp.","1","1")
            ->addColumn(ArrayAdapter::format("only-money",$laboratory),"1","1")
            ->commit("body");
    $table  ->addColumn("10.","1","1")
            ->addColumn("Radiology","1","1")
            ->addColumn(": Rp.","1","1")
            ->addColumn(ArrayAdapter::format("only-money",$radiology),"1","1")
            ->commit("body");
    $table  ->addColumn("11.","1","1")
            ->addColumn("Penunjang Lain","1","1")
            ->addColumn(": Rp.","1","1")
            ->addColumn(ArrayAdapter::format("only-money",$penunjang),"1","1")
            ->commit("body");
    $table  ->addColumn("12.","1","1")
            ->addColumn("Farmasi","1","1")
            ->addColumn(": Rp.","1","1")
            ->addColumn(ArrayAdapter::format("only-money",$farmasi),"1","1")
            ->commit("body");
    $table  ->addColumn("13.","1","1")
            ->addColumn("Lain-Lain","1","1")
            ->addColumn(": Rp.","1","1")
            ->addColumn(ArrayAdapter::format("only-money",$lain),"1","1")
            ->commit("body");
}
$table  ->addColumn("</br>","1","1")
        ->addColumn("","1","1")
        ->addColumn("","1","1")
        ->addColumn("","1","1")
        ->commit("body");
$table  ->addColumn("","1","1")
        ->addColumn("","1","1")
        ->addColumn("","1","1")
        ->addColumn(getSettings($db,"smis_autonomous_town").", ".ArrayAdapter::format("date d M Y",$bayar->waktu),"1","1")
        ->commit("body");
$table  ->addColumn("</br>","1","1")
        ->addColumn("","1","1")
        ->addColumn("","1","1")
        ->addColumn("","1","1")
        ->commit("body");

$table  ->addColumn("","1","1")
        ->addColumn("Bank Jatim","1","1")
        ->addColumn("Penyetor","1","1")
        ->addColumn("Kasir","1","1")
        ->commit("body");

                
$table  ->addColumn("</br>","1","1")
        ->addColumn("","1","1")
        ->addColumn("","1","1")
        ->addColumn("","1","1")
        ->commit("body");
$table  ->addColumn("</br>","1","1")
        ->addColumn("","1","1")
        ->addColumn("","1","1")
        ->addColumn("","1","1")
        ->commit("body");
$table  ->addColumn("","1","1")
        ->addColumn("(-----------)","1","1")
        ->addColumn("(-----------)","1","1")
        ->addColumn("(-----------)","1","1")
        ->commit("body");



require_once "kasir/function/kwitansi_print_button.php";
require_once "kasir/function/end_css_kwitansi.php";
echo kwitansi_print_button($db,"simple_kwitansi_bank_jatim",$_px);
echo "</br>";
echo "</br>";
echo "<div id='cetak_simple_kwitansi_bank_jatim_area'>".$table->getHtml()."</div>";
echo addCSS ("kasir/resource/css/kwitansi_print.css",false);
echo addJS  ("kasir/resource/js/kwitansi_print.js",false);
echo end_css_kwitansi($db);
