<?php 
global $db;
require_once ("smis-base/smis-include-service-consumer.php");
$noreg_pasien="";
if( isset($_POST['noreg']) && $_POST['noreg']!="" || isset($_POST['noreg_pasien']) && $_POST['noreg_pasien']!="" ){
    $noreg_pasien=isset($_POST['noreg'])?$_POST['noreg']:$_POST['noreg_pasien'];
}
$data                   = $_POST;
$data['noreg_pasien']   = $_POST ['noreg_pasien'];
$data['nama_pasien']    = $_POST ['nama_pasien'];
$data['nrm_pasien']     = $_POST ['nrm_pasien'];
$data['jk']             = $_POST ['jk'];
$data['page']           = $_POST ['page'];
$data['action']         = "edit_elektromedis";
$data['polislug']       = "all";
$data['pslug']          = "";
$data['pname']          = "";
$data['pimplement']     = "";
$data['kelas']          = isset($_POST['kelas'])?$_POST['kelas']:"all";

if(isset($data['super_command']) && $data['super_command']=="edit_elektromedis" ){
    $data['super_command']="";
}
$service = new ServiceConsumer ( $db, "emd_register", $data, "elektromedis" );
$service->execute ();
echo $service->getContent ();
?>