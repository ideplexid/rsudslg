<?php
require_once ("smis-base/smis-include-service-consumer.php");
global $db;
global $user;
/** TODO SYNCH TGL PULANG */
/*MENGAMBIL DATA PASIEN di REGISTRASI*/
	$header=array ();
	$adapter = new SimpleAdapter ();
	$uitable = new Table ( $header );
	require_once "kasir/function/get_patient_by_noreg.php";
	require_once "kasir/function/get_system_setup.php";
	$noreg           = $_POST ['noreg_pasien'];
	$px              = get_patient_by_noreg($db,$noreg);
	$_setup          = get_system_setup($db,$user,$noreg,$px,"");

	$_NAMA_PASIEN	 =	$px['nama_pasien']." / ".$px['namapenanggungjawab'];
	$_ALAMAT_PASIEN	 =	$px['alamat_pasien'];
	$_NRMNORG_PASIEN =	ArrayAdapter::format("only-digit8",$px['nrm'])." / ".ArrayAdapter::format("only-digit8",$px['id']);
	$_TGL_MASUK		 =	ArrayAdapter::format("date d M Y H:i",$_setup['tgl_masuk']);
	$_TGL_KELUAR	 =	ArrayAdapter::format("date d M Y H:i",$_setup['tgl_pulang']);
	$_NOMOR_KWITANSI =  $_setup['no_kwitansi'];
	$_TGL_KWITANSI	 = 	$_setup['waktu_kwitansi'];
	$_NILAI_BAYAR	 =	$_setup['nilai_kwitansi'];
	$_INACBGs		=	$px['inacbg_bpjs'];
	$_DESC_INACBGs	=	$px['deskripsi_bpjs'];
	$_PLAFON_BPJS	=	$px['plafon_bpjs'];
	$_PLAFON_NAIK	=	$px['plafon_naik_kelas'];
	$_NAIK_KELAS	=	$px['naik_kelas_bpjs'];
	$_TOTAL_q 		=	"SELECT sum(nilai) as total FROM smis_ksr_bayar WHERE noreg_pasien='".$noreg."' AND prop!='del'";
	$_TOTAL_BAYAR	= 	$db->get_var($_TOTAL_q);
/*END OF MENGAMBIL DATA PASIEN DI REGISTRASI DI SIMPAN DI $PX*/


/*LAST POSITION PASIEN*/
	$responder = new ServiceConsumer ( $db, "get_last_position",$params = array("noreg_pasien"	=> $_POST['noreg_pasien']),"medical_record" );
	$responder->execute();
	$_LAST_POSITION=ArrayAdapter::format("unslug",$responder->getContent());
/*END - LAST POSITION PASIEN*/

/*MEMBUAT TOMBOL CETAK*/
	$btn=new Button("print_button_simple_kwitansi", "", " Cetak ");
	$btn->addClass("btn btn-primary");
	$btn->setIcon(" fa fa-print ");
	$btn->setIsButton(BUtton::$ICONIC_TEXT);
	$btn->setAction("kwitansi_print('simple_kwitansi_bpjs')");
	if( $px['uri']=="0" && getSettings($db,"cashier-simple-kwitansi-cek-print-rj","0")=="1" || $px['uri']=="1" && getSettings($db,"cashier-simple-kwitansi-cek-print-ri","0")=="1"){
		$btn->setAction("cek_kwitansi_print('simple_kwitansi_bpjs')");
	}
/*AKHIR DARI MEMBUAT TOMBOL CETAK*/


/*MENGAMBIL DATA RUMAH SAKIT*/
	$autonomous=getSettings($db, "smis_autonomous_name", "SMIS");
	$nama_rs=getSettings($db, "cashier-simple-kwitansi-rs", $autonomous);
	$autonomous_address=getSettings($db, "smis_autonomous_address", "LOCALHOST");
	$alamat_rs=getSettings($db, "cashier-simple-kwitansi-address", $autonomous_address);
/*END - MENGAMBIL DATA RUMAH SAKIT*/

/*MENGAMBIL DATA TOTAL TAGIHAN*/
	$query="SELECT sum(total) FROM smis_ksr_kolektif WHERE prop!='del' AND akunting_only=0 AND noreg_pasien='$noreg' ";
	$_TOTAL_TAGIHAN=$db->get_var($query);
/*END - MENGAMBIL DATA TOTAL TAGIHAN*/

$table=new TablePrint("simple_kwitansi_bpjs");
$table->setTableClass("kwitansi_print");
$table->setMaxWidth(true);

$table->addColumn($nama_rs."</br>".$alamat_rs, 6, 1,NULL,NULL,"center bold");
$table->commit("body");

$table->addSpace(3,1);
$table->addColumn("No. Kwitansi : ".$_NOMOR_KWITANSI,1,1);
$table->commit("body");

$table->addColumn("NAMA / PJ",1,1);
$table->addColumn($_NAMA_PASIEN,2,1);
$table->addColumn("TANGGAL MASUK",1,1,null,null,"bold");
$table->commit("body");

$table->addColumn("ALAMAT",1,1);
$table->addColumn($_ALAMAT_PASIEN,2,1);
$table->addColumn($_TGL_MASUK,1,1);
$table->commit("body");

$table->addColumn("NRM / NO. REG",1,1);
$table->addColumn($_NRMNORG_PASIEN,2,1);
$table->addColumn("TANGGAL KELUAR",1,1,null,null,"bold");
$table->commit("body");

$table->addSpace(3,1);
$table->addColumn($_TGL_KELUAR,1,1);
$table->commit("body");

$table->addSpace(4,1);
$table->commit("body");

$table->addColumn("Tanggal",1,1);
$table->addColumn(" : ".$_TGL_KWITANSI,1,1);
$table->commit("body");

$table->addColumn("INA-CBG ",1,1);
$table->addColumn(" : ".$_INACBGs,1,1);
$table->commit("body");

$table->addColumn("Deskripsi ",1,1);
$table->addColumn(" : ".$_DESC_INACBGs,1,1);
$table->commit("body");

$table->addColumn("KETERANGAN",1,1,null,null,"bold upperscore");
$table->addColumn("RUANGAN",1,1,null,null,"bold upperscore");
$table->addColumn("KELAS",1,1,null,null,"bold upperscore");
$table->addColumn("TOTAL",1,1,null,null,"bold upperscore");
$table->commit("body");

$_TANGGUNGAN=0;
if($_NAIK_KELAS==""){
	/*	pasien tidak naik kelas
	 * jika terjadi selisih maka hanya diminta untuk bayar selisihnya saja*/
	if($_TOTAL_TAGIHAN <= $_PLAFON_BPJS){
		echo "Plafon Lebih Besar Tidak Perlu Mencetak Kwitansi";
		return;
	}	
	$table->addColumn("Plafon Paket BPJS",1,1);
	$table->addColumn("-",1,1);
	$table->addColumn("-",1,1);
	$table->addColumn(ArrayAdapter::format("money Rp. ",$_PLAFON_BPJS),1,1);
	$table->commit("body");
	
	$table->addColumn("Tindakan Perawat Ruangan",1,1);
	$table->addColumn($_LAST_POSITION,1,1);
	$table->addColumn("-",1,1);
	$table->addColumn(ArrayAdapter::format("money Rp. ",$_TOTAL_TAGIHAN),1,1);
	$table->commit("body");

	$table->addColumn("Plafon Paket BPJS Naik Kelas",1,1);
	$table->addColumn("-",1,1);
	$table->addColumn("-",1,1);
	$table->addColumn("-",1,1);
	$table->commit("body");
	
	$table->addColumn("TOTAL TAGIHAN (BPJS SHARING)",1,1,null,null,"bold");
	$table->addColumn("-",1,1);
	$table->addColumn("-",1,1);
	$table->addColumn(ArrayAdapter::format("money Rp. ",$_TOTAL_TAGIHAN - $_PLAFON_BPJS),1,1,null,null,"upperscore bold");
	$table->commit("body");
	$_TANGGUNGAN=$_TOTAL_TAGIHAN - $_PLAFON_BPJS;
}else if(substr($_NAIK_KELAS,0,1)=="v"){
	/**
	 * pasien naik ke kelas VIP
	 * jika terdapat selisih maka
	 * diminta untuk bayar selisihnya saja
	 * */
	if($_TOTAL_TAGIHAN <= $_PLAFON_BPJS ){
		echo "Plafon Lebih Besar Tidak Perlu Mencetak Kwitansi";
		return;
	}
	$kelas=substr($_NAIK_KELAS,1,1);
	$table->addColumn("Plafon Paket BPJS",1,1);
	$table->addColumn("-",1,1);
	$table->addColumn($kelas,1,1);
	$table->addColumn(ArrayAdapter::format("money Rp. ",$_PLAFON_BPJS),1,1);
	$table->commit("body");
	
	$table->addColumn("Tindakan Perawat Ruangan",1,1);
	$table->addColumn($_LAST_POSITION,1,1);
	$table->addColumn("VIP",1,1);
	$table->addColumn(ArrayAdapter::format("money Rp. ",$_TOTAL_TAGIHAN),1,1);
	$table->commit("body");

	$table->addColumn("Plafon Paket BPJS Naik Kelas",1,1);
	$table->addColumn("-",1,1);
	$table->addColumn("-",1,1);
	$table->addColumn("-",1,1);
	$table->commit("body");
	
	$table->addColumn("TOTAL TAGIHAN (BPJS SHARING)",1,1,null,null,"bold");
	$table->addColumn("-",1,1);
	$table->addColumn("-",1,1);
	$table->addColumn(ArrayAdapter::format("money Rp. ", $_TOTAL_TAGIHAN - $_PLAFON_BPJS),1,1,null,null,"upperscore bold");
	$table->commit("body");
	$_TANGGUNGAN=$_PLAFON_NAIK-$_PLAFON_BPJS;
	
}else{
	/**
	 * pasien naik kelas non vip
	 * diminta bayar selisih antara 
	 * kelas asal ke kelas tujuan.
	 * */
	$kelas_asal=substr($_NAIK_KELAS,0,1);
	$kelas_naik=substr($_NAIK_KELAS,1,1);
	$tampilkan=substr($_NAIK_KELAS,2,1);
	$table->addColumn("Plafon Paket BPJS",1,1);
	$table->addColumn("-",1,1);
	$table->addColumn($kelas_asal,1,1);
	$table->addColumn(ArrayAdapter::format("money Rp. ",$_PLAFON_BPJS),1,1);
	$table->commit("body");
	
	$table->addColumn("Tindakan Perawat Ruangan",1,1);
	if($tampilkan=="t"){
		$table->addColumn($_LAST_POSITION,1,1);
		$table->addColumn("-",1,1);
		$table->addColumn(ArrayAdapter::format("money Rp. ",$_TOTAL_TAGIHAN),1,1);
	}else{
		$table->addColumn("-",1,1);
		$table->addColumn("-",1,1);
		$table->addColumn("-",1,1);
	}
	$table->commit("body");	
	
	$table->addColumn("Plafon Paket BPJS Naik Kelas",1,1,null,null,"bold");
	$table->addColumn($_LAST_POSITION,1,1);
	$table->addColumn($kelas_naik,1,1);
	$table->addColumn(ArrayAdapter::format("money Rp. ",$_PLAFON_NAIK),1,1);
	$table->commit("body");
	
	$table->addColumn("TOTAL TAGIHAN (BPJS SHARING)",1,1);
	$table->addColumn("-",1,1);
	$table->addColumn("-",1,1);
	$table->addColumn(ArrayAdapter::format("money Rp. ", $_PLAFON_NAIK-$_PLAFON_BPJS),1,1,null,null,"upperscore bold");
	$table->commit("body");
	$_TANGGUNGAN=$_PLAFON_NAIK-$_PLAFON_BPJS;
	
}


$table->addSpace("4","1");
$table->commit("body");

$table->addColumn("PEMBAYARAN",3,1);
$table->addColumn(ArrayAdapter::format("money Rp. ", $_NILAI_BAYAR),1,1);
$table->commit("body");
loadLibrary("smis-libs-function-math");

$table->addColumn("TERBILANG : ",1,1,null,null,"upperscore underscore bold");
$table->addColumn(strtoupper(numbertell($_NILAI_BAYAR)." rupiah"),3,1,null,null,"upperscore  underscore bold");
$table->commit("body");

if($_TOTAL_BAYAR*1<$_TANGGUNGAN){
	$HUTANG=$_TANGGUNGAN - $_TOTAL_BAYAR;
	$table->addColumn("SISA PEMBAYARAN : ",3,1);
	$table->addColumn(ArrayAdapter::format("money Rp. ",$HUTANG),1,1);
	$table->commit("body");
}
$table->addSpace(4, 1);
$table->commit("body");
/*KODE TANDA TANGAN*/
global $user;
$kota=getSettings($db, "cashier-simple-kwitansi-town", "");
$table->addSpace(3,1);
$table->addColumn( $kota.", ".ArrayAdapter::format("date d M Y", date("Y-M-d")),1,1,NULL,NULL,"center");
$table->commit("body");
$table->addColumn("</br></br>", 4, 1,NULL,NULL,"center");
$table->commit("body");

$table->addSpace(3, 1);
$table->addColumn($user->getNameOnly(), 1, 1,NULL,NULL,"center");
$table->commit("body");

echo $btn->getHtml();
echo "<div id='cetak_simple_kwitansi_bpjs_area'>".$table->getHtml()."</div>";
echo addCSS("kasir/resource/css/kwitansi_print.css",false);
echo addJS("kasir/resource/js/kwitansi_print.js",false);
?>
