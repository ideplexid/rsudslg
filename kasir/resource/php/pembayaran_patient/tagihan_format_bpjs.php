<?php
global $db;
global $user;
require_once ("smis-base/smis-include-service-consumer.php");

/*PERHITUNGAN RESPONSE TIME*/
$RESPONSE_TIME="";
if(getSettings($db, "cashier-printout-show-response-time", "0")=="1"){
	require_once ("kasir/class/service/GetResponseTime.php");
	$response=new GetResponseTime($db,  $_POST ['noreg_pasien']);
	$response->execute();
	$RESPONSE_TIME=$response->getResponseTime();
}
/*AKHIR PERTHITUNGAN RESPONSE TIME*/
$print=new Button("print", "", "Print");
$print->setIcon("fa fa-print");
$print->setIsButton(Button::$ICONIC_TEXT);
$print->setClass("btn-primary");
$print->setAction("detail_tagihan_print();");

$show=new Button("print", "", "Tampilkan");
$show->setIcon("fa fa-eye");
$show->setIsButton(Button::$ICONIC_TEXT);
$show->setClass("btn-success");
$show->setAction("detail_tagihan_show();");

$hide=new Button("print", "", "Sembunyikan");
$hide->setIcon("fa fa-stop");
$hide->setIsButton(Button::$ICONIC_TEXT);
$hide->setClass("btn-success");
$hide->setAction("detail_tagihan_hide();");

$simple=new Button("print", "", "Simple");
$simple->setIcon("fa fa-terminal");
$simple->setIsButton(Button::$ICONIC_TEXT);
$simple->setClass("btn-danger");
$simple->setAction("detail_tagihan_simple();");

$fetree=new Button("print", "", "Fetree");
$fetree->setIcon("fa fa-heart");
$fetree->setIsButton(Button::$ICONIC_TEXT);
$fetree->setClass("btn-danger");
$fetree->setAction("detail_tagihan_fetree();");

$border=new Button("print", "", "Border");
$border->setIcon("fa fa-arrows-h");
$border->setIsButton(Button::$ICONIC_TEXT);
$border->setClass("btn-inverse");
$border->setAction("detail_tagihan_border();");

$noborder=new Button("print", "", "No Border");
$noborder->setIcon("fa fa-arrows-v");
$noborder->setIsButton(Button::$ICONIC_TEXT);
$noborder->setClass("btn-inverse");
$noborder->setAction("detail_tagihan_noborder();");


$grup=new ButtonGroup("");
$grup->setMax(15,"");
$grup->addButton($print);
$grup->addButton($show);
$grup->addButton($hide);
$grup->addButton($simple);
$grup->addButton($fetree);
$grup->addButton($border);
$grup->addButton($noborder);
echo $grup->getHtml();

$header=array ();
$adapter = new SimpleAdapter ();
$uitable = new Table ( $header );
require_once "kasir/function/get_patient_by_noreg.php";
require_once "kasir/function/get_system_setup.php";
$noreg           = $_POST ['noreg_pasien'];
$px              = get_patient_by_noreg($db,$noreg);
$_setup          = get_system_setup($db,$user,$noreg,$px,"");
$hide=getSettings($db, "cashier-printout-table-keterangan", "Hide Mode")=="Hide Mode"?"hide noprint":"";
$table=new TablePrint("table_detail_tagihan",false);
$table->setMaxWidth(false);
$table->setTableClass("noborder_table_kasir");
$table->setDefaultBootrapClass(getSettings($db, "cashier-printout-table-model", "Simple Mode")=="Fetree Mode");
$header=getSettings($db, "cashier-printout-header", "1")=="1";

if($header){
	///------------ BARIS KODE PEMBUATAN HEADER
	$title=getSettings($db, "smis_autonomous_title", "");
	$subtitle=getSettings($db, "smis_autonomous_sub_title", "");
	$address=getSettings($db, "smis_autonomous_address", "");
	$logo="<img src='".getLogo()."'  style='width:100px;height:auto;'> ";
	
	$table->addColumn ( $logo, 1, 1,NULL,"","" );
	$table->addColumn ( "<h4>".$subtitle . "</h4><h2>" . $title . "</h2><h5>" . $address."<h5>", 8, 1,NULL,"","center bold big   uppercase " );
	$table->commit("body","bottom_dash");
	$table->addColumn("Nama", "1", "1",NULL,NULL," bold");
	$table->addColumn(strtoupper($px['nama_pasien']),"1", "1");
	$table->addColumn("Alamat", "1", "1",NULL,NULL," bold");
	$table->addColumn($px['alamat_pasien'],"4", "1");
	$table->commit("body");
	$table->addColumn("NRM", "1", "1",NULL,NULL," bold");
	$table->addColumn(ArrayAdapter::format("only-digit8", $px['nrm']),"1", "1");
	$table->addColumn("No. Reg", "1", "1",NULL,NULL," bold");
	$table->addColumn(ArrayAdapter::format("only-digit8", $px['id']),"4", "1");
	$table->commit("body");
	$table->addColumn("Jenis Pasien", "1", "1",NULL,NULL,"bold ");
	$table->addColumn($px['carabayar'],"1", "1");
	$table->addColumn("Perusahaan", "1", "1",NULL,NULL," bold");
	$table->addColumn($px['n_perusahaan'],"4", "1");
	$table->commit("body");
	$table->addColumn("P. Jawab", "1", "1",NULL,NULL," bold");
	$table->addColumn($px['namapenanggungjawab'],"1", "1");
	$table->addColumn("Telp.", "1", "1",NULL,NULL," bold");
	$table->addColumn(strtoupper($px['telponpenanggungjawab']),"4", "1");
	$table->commit("body");
	$table->addColumn("Tanggal Masuk", "1", "1",NULL,NULL," bold");
	$table->addColumn(ArrayAdapter::format("date d M Y H:i", $_setup['tgl_masuk']),"1", "1");
	$table->addColumn("Tanggal Keluar", "1", "1",NULL,NULL," bold");
	$table->addColumn(ArrayAdapter::format("date d M Y H:i", $_setup['tgl_pulang'])." ".$RESPONSE_TIME,"4", "1");
	$table->commit("body","bottom_dash");
	
	/// ------- END OF BARIS KODE PEMBUATAN HEADER
}

$table->addColumn("</br></br>", "10", "1");
$table->commit("body");

$jaspel=getSettings($db, "cashier-activate-jaspel", "1");
$jaspel_position=getSettings($db, "cashier-printout-jaspel-position", "Bottom")=="Side";
$s_grup=getSettings($db, "cashier-printout-title", "1")=="1";
$s_stack=getSettings($db, "cashier-printout-stack", "1")=="1";
$s_substotal=getSettings($db, "cashier-printout-subtotal", "1")=="1";
$s_jsubstotal=getSettings($db, "cashier-printout-subtotal-jaspel", "1")=="1";
$s_space=getSettings($db, "cashier-printout-space", "1")=="1";
$s_side=getSettings($db, "cashier-printout-side", "1")=="1";
$jaspel_persen=getSettings($db, "cashier-jaspel-persen", "10");
$l_side=3;
if($s_side) $l_side++;

//----- BARIS KODE UNTUK TAGIHAN
$inap=$px['uri']=="1";
$hide_pj_perawat=getSettings($db, "cashier-printout-hide-perawat-pj-on-jalan", "0")==1 && !$inap;
if($s_side) $table->addColumn("Layanan", "1", "1",NULL,NULL,"  center uppercase big ");
$table->addColumn("Ruang", "1", "1",NULL,NULL," center uppercase big ");
$table->addColumn("Nama", "1", "1",NULL,NULL," center  uppercase big ");
$table->addColumn("Tanggal", "1", "1",NULL,NULL," center  uppercase big ");
if($jaspel && $inap && $jaspel_position){
	$table->addColumn("Sub Total", "1", "1",NULL,NULL," center  uppercase big ");
	$table->addColumn("Jaspel", "1", "1",NULL,NULL," center  uppercase big ");
}else{
	$table->addColumn("Total", "2", "1",NULL,NULL," center  uppercase big ");
} 
$table->addColumn("Keterangan", "1", "1",NULL,NULL," center uppercase big tagihan_keterangan ".$hide." ");
$table->commit("body","top_dash bottom_dash");

$join_adminstrasi=getSettings($db, "cashier-printout-join-administrasi", "0")=="1" && !$jaspel_position && $jaspel && $inap;
$dbtable=new DBTable($db,"smis_ksr_kolektif");
$dbtable->setShowAll(true);
$dbtable->setFetchMethode(DBTable::$ARRAY_FETCH);
$dbtable->setOrder(" urutan ASC, nama_grup ASC , ruangan ASC, dari ASC, sampai ASC ");
$dbtable->addCustomKriteria("noreg_pasien","='".$noreg."'");
$dbtable->addCustomKriteria("akunting_only","=0");
if($join_adminstrasi) {
	$dbtable->addCustomKriteria("jenis_tagihan","!='administrasi'");
}	

$dt=$dbtable->view("","0");
$list=$dt['data'];
$total_biaya=0;
$total_jaspel=0;
$sub_ruang=getSettings($db, "cashier-printout-sub-ruang", "0")=="1";
$subtotal_jaspel=0;
$subtotal_biaya=0;
$curname="";
$cur_ruang_name="";
$last_jenis="";


function createSubTotal($table,$jaspel,$inap,$curname,$subtotal_biaya,$subtotal_jaspel,$s_space,$s_substotal,$s_jsubstotal,$l_side,$jaspel_position,$hide){
	if($jaspel && $inap &&  $jaspel_position){
		if($s_jsubstotal ){
			$table->addColumn ( "Sub Total ".ArrayAdapter::format("unslug-ucword", $curname),$l_side, 1,NULL,NULL,"" );
			$table->addColumn ( ArrayAdapter::format ( "money Rp.", $subtotal_biaya), 1, 1 );
			$table->addColumn ( ArrayAdapter::format ( "money Rp.", $subtotal_jaspel), 1, 1 );
			$table->addColumn ( "", 1, 1 ,null,null," tagihan_keterangan ".$hide."");
			$table->commit ( "body","bottom_dash" );
		}
		if($s_substotal){
			$table->addColumn ( "Total ".ArrayAdapter::format("unslug-ucword", $curname), $l_side+1, 1,NULL,NULL,"left");
			$table->addColumn ( ArrayAdapter::format ( "money Rp.", $subtotal_biaya+$subtotal_jaspel), $jaspel_position?2:1, 1,NULL,NULL," " );
			$table->addColumn ( " ", 6, 1,NULL,NULL," tagihan_keterangan ".$hide.""  );
			$table->commit ( "body","bottom_dash" );
		}
	}else if($s_substotal){
		$table->addColumn ( "Total ".ArrayAdapter::format("unslug-ucword", $curname), $l_side+1, 1 ,NULL,NULL,"left");
		$table->addColumn ( ArrayAdapter::format ( "money Rp.", $subtotal_biaya), 1, 1,NULL,NULL,"right" );
		$table->addColumn ("", 6, 1 ,NULL,NULL," tagihan_keterangan ".$hide."");
		$table->commit ( "body","bottom_dash " );
	}
	if($s_space) $table->addColumn("&nbsp;", "10", "1","body",NULL," ");
}
$length_list=count($list);
foreach( $list as $x){
	
	if($sub_ruang && ($x['jenis_tagihan']=="tindakan_perawat" || $x['jenis_tagihan']=="alok") && $x['jenis_tagihan']!=$last_jenis && $subtotal_biaya!=0){
		createSubTotal($table,$jaspel,$inap, $curname, $subtotal_biaya, $subtotal_jaspel,$s_space,$s_substotal,$s_jsubstotal,$l_side,$jaspel_position,$hide);
		$cur_ruang_name=$x['ruangan'];
		$subtotal_jaspel=0;
		$subtotal_biaya=0;
	}
	
	if($sub_ruang && ( $x['jenis_tagihan']=="alok" || $x['jenis_tagihan']=="tindakan_perawat" || $last_jenis=="alok" || $last_jenis=="tindakan_perawat") ){
		if($cur_ruang_name!=""  
				&& 	($cur_ruang_name!=$x['ruangan'] || $x['jenis_tagihan']!=$last_jenis) 
				&& ($last_jenis=="alok" || $last_jenis=="tindakan_perawat") && $subtotal_biaya!=0
				){
			createSubTotal($table,$jaspel,$inap, $curname." - ".ArrayAdapter::format("unslug", $cur_ruang_name), $subtotal_biaya, $subtotal_jaspel,$s_space,$s_substotal,$s_jsubstotal,$l_side,$jaspel_position,$hide);
			$cur_ruang_name=$x['ruangan'];
			$subtotal_jaspel=0;
			$subtotal_biaya=0;
		}else{
			$cur_ruang_name=$x['ruangan'];
		}
		
		if($s_grup && $x['nama_grup']!=$curname) {
			$table->addColumn("<u>".ArrayAdapter::format("unslug-ucword", $x['nama_grup'])."</u>", "10", "1",NULL,NULL,"center big uppercase");
			$table->commit("body");
			$curname=$x['nama_grup'];
		}
	}
	
	if($x['nama_grup']!=$curname && (!$sub_ruang || $sub_ruang && !($last_jenis=="alok" || $last_jenis=="tindakan_perawat") ) ){		
		if($curname!="" && $subtotal_biaya!=0){			
			createSubTotal($table,$jaspel,$inap, $curname, $subtotal_biaya, $subtotal_jaspel,$s_space,$s_substotal,$s_jsubstotal,$l_side,$jaspel_position,$hide);
		}		
		$curname=$x['nama_grup'];
		$subtotal_jaspel=0;
		$subtotal_biaya=0;
		if($s_grup) {
			$table->addColumn("<u>".ArrayAdapter::format("unslug-ucword", $x['nama_grup'])."</u>", "10", "1",NULL,NULL,"center big uppercase");
			$table->commit("body");
		}
	}
	if($s_side) $table->addColumn(ArrayAdapter::format("unslug", $x['nama_grup']), "1", "1");
	$table->addColumn(ArrayAdapter::format("unslug", $x['ruangan']), "1", "1");
	$table->addColumn(ArrayAdapter::format("unslug", $x['nama_tagihan']), "1", "1");
	$table->addColumn( $x['tanggal'], "1", "1");
	if($jaspel && $inap){
		$table->addColumn(ArrayAdapter::format("money Rp.", $x['nilai']), "1", "1");
		$jsp=$x['nilai']*$jaspel_persen*$x['jaspel']/100;
		$total_jaspel+=$jsp;
		$subtotal_jaspel+=$jsp;
		if($jaspel_position) $table->addColumn(ArrayAdapter::format("money Rp.", $jsp), "1", "1");
	}else{
		$table->addColumn(ArrayAdapter::format("money Rp.", $x['nilai']), "1", "1");
	}	
	$table->addColumn("", "1", "1",NULL,NULL,"");	//gap kosong disini	
	$total_biaya+=$x['nilai'];	
	$subtotal_biaya+=$x['nilai'];	
	$table->addColumn($x['keterangan'], "1", "1",NULL,NULL,"tagihan_keterangan ".$hide."");
	$table->commit("body");
	$last_jenis=$x['jenis_tagihan'];
}
if(($last_jenis=="alok" || $last_jenis=="tindakan_perawat") && $sub_ruang && $subtotal_biaya!=0){
	createSubTotal($table,$jaspel,$inap,$curname." - ".ArrayAdapter::format("unslug", $cur_ruang_name),$subtotal_biaya,$subtotal_jaspel,$s_space,$s_substotal,$s_jsubstotal,$l_side,$jaspel_position,$hide);
}else if($subtotal_biaya!=0){
	createSubTotal($table,$jaspel,$inap, $curname, $subtotal_biaya, $subtotal_jaspel,$s_space,$s_substotal,$s_jsubstotal,$l_side,$jaspel_position,$hide);
}
loadLibrary("smis-libs-function-math");
if($jaspel && $inap){
	
	if($jaspel_position){
		$table->addColumn ( "Sub Total Tagihan", $l_side+1, 1,NULL,NULL," " );
		$table->addColumn ( ArrayAdapter::format ( "money Rp.", $total_biaya), 1, 1 ,NULL,NULL," ");
		$table->addColumn ( ArrayAdapter::format ( "money Rp.", $total_jaspel), 1, 1 ,NULL,NULL," ");
		$table->addColumn ( "", 1, 1 ,null,null,"tagihan_keterangan ".$hide."");
		$table->commit ( "body" );
		
		$table->addColumn ( "Total Tagihan", $l_side+1, 1,NULL,NULL,"left  ");
		$table->addColumn ( ArrayAdapter::format ( "money Rp.", $total_biaya+$total_jaspel), 2, 1 ,NULL,NULL," ");
		$table->addColumn ( numbertell($total_biaya+$total_jaspel) . " Rupiah", 6, 1 ,NULL,NULL,"tagihan_keterangan ".$hide."");
		$table->commit ( "body" );
	}else{
		$table->addColumn ( "Sub Total Tagihan", $l_side+1, 1,NULL,NULL," " );
		$table->addColumn ( ArrayAdapter::format ( "money Rp.", $total_biaya), 1, 1 ,NULL,NULL," ");
		$table->addColumn ( "", 1, 1 ,null,null,"tagihan_keterangan ".$hide."");
		$table->commit ( "body" );
		if($join_adminstrasi){
			$query="SELECT sum(total) FROM smis_ksr_kolektif WHERE prop!='del' AND akunting_only=0 AND jenis_tagihan='administrasi' AND noreg_pasien='".$noreg."'";
			$administrasi=$db->get_var($query);
			$total_jaspel+=$administrasi;
			$table->addColumn ( "Jasa Pelayanan ( + )", $l_side+1, 1,NULL,NULL," " );
			$table->addColumn ( ArrayAdapter::format ( "money Rp.", $total_jaspel), 1, 1 ,NULL,NULL," ");
			$table->addColumn ( "", 1, 1 ,null,null,"tagihan_keterangan ".$hide."");
			$table->commit ( "body" );
		}else{
			$table->addColumn ( "Jasa Pelayanan", $l_side+1, 1,NULL,NULL," " );
			$table->addColumn ( ArrayAdapter::format ( "money Rp.", $total_jaspel), 1, 1 ,NULL,NULL," ");
			$table->addColumn ( "", 1, 1 ,null,null,"tagihan_keterangan ".$hide."");
			$table->commit ( "body" );
		}		
		$table->addColumn ( "Total Tagihan", $l_side+1, 1,NULL,NULL,"left  ");
		$table->addColumn ( ArrayAdapter::format ( "money Rp.", $total_biaya+$total_jaspel), 1, 1 ,NULL,NULL," ");
		$table->addColumn ( numbertell($total_biaya+$total_jaspel) . " Rupiah", 6, 1 ,NULL,NULL,"tagihan_keterangan ".$hide."");
		$table->commit ( "body","bottom_dash" );
	}
}else{
	$table->addColumn ( "Total Tagihan", $l_side+1, 1 ,NULL,NULL," ");
	$table->addColumn ( ArrayAdapter::format ( "money Rp.", $total_biaya+$total_jaspel), 1, 1 ,NULL,NULL," ");
	$table->addColumn ( numbertell($total_biaya+$total_jaspel) . " Rupiah", 6, 1 ,NULL,NULL,"tagihan_keterangan ".$hide."");
	$table->commit ( "body","top_dash bottom_dash" );
}
//----- END OF BARIS KODE UNTUK TAGIHAN


//---- MENAMPILLAN DISKON SENDIRI
$total_diskon=0;
$diskon_alone=getSettings($db, "cashier-printout-discount-alone", "1")=="1";
if($diskon_alone){
	$query="SELECT nilai as nilai, keterangan as keterangan FROM smis_ksr_bayar WHERE metode='diskon' AND noreg_pasien='".$noreg."' AND prop!='del'";
	$ldiskon=$db->get_result($query);
	$total_diskon=0;
	foreach($ldiskon as $diskon){
		$table->addColumn("Potongan ".$diskon->keterangan, $l_side, "1",NULL,NULL," ");
		$table->addColumn(ArrayAdapter::format ( "money Rp.", $diskon->nilai), ($jaspel && $inap && $jaspel_position)?2:1, "1",NULL,NULL,"  ");
		$table->addColumn ( "", 1, 1 ,NULL,null,"");
		$table->addColumn ( "", 1, 1 ,NULL,null,"tagihan_keterangan ".$hide."");
		$table->commit("body","");
		$total_diskon+=$diskon->nilai;
	}
	
	if($total_diskon>0){
		$table->addColumn(" Total Potongan ", $l_side+1, "1",NULL,NULL," ");
		$table->addColumn(ArrayAdapter::format ( "money Rp.", $total_diskon), ($jaspel && $inap && $jaspel_position)?2:1, "1",NULL,NULL,"  ");
		$table->addColumn ( "", 1, 1 ,NULL,null,"tagihan_keterangan ".$hide."");
		$table->commit("body","bottom_dash");
	}
	
	if($total_diskon>0){
		$table->addColumn ( "Total Tagihan - Potongan", $l_side+1, 1 ,NULL,NULL," ");
		$table->addColumn ( ArrayAdapter::format ( "money Rp.", $total_biaya+$total_jaspel-$total_diskon), 1, 1 ,NULL,NULL," ");
		$table->addColumn ( numbertell($total_biaya+$total_jaspel) . " Rupiah", 6, 1 ,NULL,NULL,"tagihan_keterangan ".$hide."");
		$table->commit ( "body","top_dash bottom_dash" );
	}
	
}
//---- AKHIR PENAMPILAN DISKON

//----- BARIS TAGIHAN BPJS
$plafon_bpjs=$px['plafon_bpjs'];
$table->addColumn("Tagihan BPJS", $l_side+1, "1",NULL,NULL,"bold ");
$table->addColumn(ArrayAdapter::format ( "money Rp.", $plafon_bpjs), ($jaspel && $inap && $jaspel_position)?2:1, "1",NULL,NULL," bold");
$table->addColumn ( numbertell($plafon_bpjs) . " Rupiah", 6, 1,NULL,NULL,"  tagihan_keterangan ".$hide." bold" );
$table->commit("body","bottom_dash");
$table->addColumn("&nbsp;", "10", "1","body",NULL,"  ");
//---- BARIS TAGIHAN BPJS


//---- CODE TTD
$blank_pj=getSettings($db, "cashier-printout-blank-pj", "0")=="1";
$perawat_ttd=getSettings($db, "cashier-printout-perawat", "0")=="1";
global $user;
$lokasi=getSettings($db, "smis-rs-footer", "");

if($perawat_ttd){
	$first=floor($l_side/2);
	$second=$l_side-$first;
	if($hide_pj_perawat){
		$table->addColumn ( "", $first, 1,null,"","center" );
		$table->addColumn ( "", $second, 1,null,"","center" );
	}else{
		$table->addColumn ( "Penanggung Jawab </br></br></br> ".($blank_pj?"(_ _ _ _ _ _ _)":$px['namapenanggungjawab']), $first, 1,null,"","center" );
		$table->addColumn ( "Perawat Ruangan </br></br></br> (_ _ _ _ _ _ _)", $second, 1,null,"","center" );
	}
	$table->addColumn ( $lokasi.", ".ArrayAdapter::format("date d M Y", $tgl_pulang)."</br></br></br>".$user->getNameOnly(), 10, 1,"body","","center" );
}else{
	if($hide_pj_perawat){
		$table->addColumn ( "", $l_side, 1,null,"","center" );
	}else{
		$table->addColumn ( "Penanggung Jawab </br></br></br> ".($blank_pj?"(_ _ _ _ _ _ _)":$px['namapenanggungjawab']), $l_side, 1,null,"","center" );
	}
	$table->addColumn ( "Penanggung Jawab </br></br></br> ".($blank_pj?"(_ _ _ _ _ _ _)":$px['namapenanggungjawab']), $l_side, 1,null,"","center" );
	$table->addColumn ( $lokasi.", ".ArrayAdapter::format("date d M Y", $tgl_pulang)."</br></br></br>".$user->getNameOnly(), 10, 1,"body","","center" );
}
//---- END TTD

echo "<div id='print_area_tagihan'>".$table->getHtml()."</div>";
echo addJS("kasir/resource/js/detail_tagihan.js",false);

?>