<?php

$tipe=array();
$tipe[]=array("value"=>"%","name"=>" - - SEMUA - - ","default"=>"1");
$tipe[]=array("value"=>"registration","name"=>"Pendaftaran");
$tipe[]=array("value"=>"administrasi","name"=>"Administrasi");
$tipe[]=array("value"=>"radiology","name"=>"Radiology");
$tipe[]=array("value"=>"fisiotherapy","name"=>"Fisiotherapy");
$tipe[]=array("value"=>"laboratory","name"=>"Laboratory");
$tipe[]=array("value"=>"ambulan","name"=>"Ambulan");
$tipe[]=array("value"=>"gizi","name"=>"Gizi");
$tipe[]=array("value"=>"tindakan_perawat","name"=>"Tindakan Perawat");
$tipe[]=array("value"=>"tindakan_perawat_igd","name"=>"Tindakan Perawat IGD");
$tipe[]=array("value"=>"tindakan_igd","name"=>"Tindakan Dokter IGD");
$tipe[]=array("value"=>"tindakan_dokter","name"=>"Tindakan Dokter");
$tipe[]=array("value"=>"konsul","name"=>"Konsul");
$tipe[]=array("value"=>"visite_dokter","name"=>"Visite");
$tipe[]=array("value"=>"konsultasi_dokter","name"=>"Periksa");
$tipe[]=array("value"=>"bed","name"=>"Bed");
$tipe[]=array("value"=>"penjualan_resep","name"=>"Resep");
$tipe[]=array("value"=>"return_resep","name"=>"Return Resep");
$tipe[]=array("value"=>"vk","name"=>"VK");
$tipe[]=array("value"=>"ok","name"=>"OK");
$tipe[]=array("value"=>"rr","name"=>"Recovery Room");
$tipe[]=array("value"=>"alok","name"=>"Alat Obat Kesehatan");
$tipe[]=array("value"=>"oksigen_central","name"=>"Oksigen Central");
$tipe[]=array("value"=>"oksigen_manual","name"=>"Oksigen Manual");
$tipe[]=array("value"=>"audiometry","name"=>"Audiometry");
$tipe[]=array("value"=>"bronchoscopy","name"=>"Bronchoscopy");
$tipe[]=array("value"=>"faal_paru","name"=>"Faal Paru");
$tipe[]=array("value"=>"spirometry","name"=>"Spirometry");
$tipe[]=array("value"=>"endoscopy","name"=>"Endoscopy");
$tipe[]=array("value"=>"ekg","name"=>"EKG");
$tipe[]=array("value"=>"darah","name"=>"Darah PMI");

global $db;
require_once 'kasir/class/table/TableEditBayangan.php';
require_once 'kasir/class/responder/EditBayanganResponder.php';
require_once 'smis-base/smis-include-service-consumer.php';
$header0="<tr>  <th rowspan='2'>No</th> <th rowspan='2'>Group</th> <th rowspan='2'>Ruangan</th> <th rowspan='2'>Tanggal</th> <th colspan='3'>Asli</th> <th colspan='3'> Bayangan</th>  </tr>";
$header1="<tr>  <th>Nama</th> <th>Nilai</th> <th>Jaspel</th> <th>Nama</th> <th>Nilai</th> <th>Jaspel</th> <th>Tampil</th> <th>Ruang</th> <th></th> </tr>";
$header=array ('No.','Group','Ruangan',"Tanggal","Nama Asli","Nilai Asli","Jaspel Asli",'Nama','Nilai' ,"Jaspel","Tampil","Ruang");
$uitable = new TableEditBayangan( $header, "", NULL, true );
$uitable->addHeader("before", $header0);
$uitable->addHeader("before", $header1);
$uitable->setName ( "edit_bayangan" );
$uitable->setDelButtonEnable(false);
$uitable->setAddButtonEnable(false);
$uitable->setPrintButtonEnable(false);
$uitable->setReloadButtonEnable(false);
$uitable->setHeaderVisible(false);



$hide=new Button("", "", "");
$hide->setIsButton(Button::$ICONIC);
$hide->setClass(" btn-inverse");
$hide->setIcon(" fa fa-eye-slash");

$jaspel=new Button("", "", "");
$jaspel->setIsButton(Button::$ICONIC);
$jaspel->setClass(" btn-inverse");
$jaspel->setIcon(" fa fa-check");

$back=new Button("", "", "");
$back->setIsButton(Button::$ICONIC);
$back->setClass(" btn-inverse");
$back->setIcon(" fa fa-backward");

$uitable->addContentButton("hidding", $hide);
$uitable->addContentButton("jaspel", $jaspel);
$uitable->addContentButton("back", $back);


if (isset ( $_POST ['command'] )) {
	$dbtable=new DBTable($db, "smis_ksr_kolektif");
	$dbtable->addCustomKriteria("noreg_pasien", "='".$_POST['noreg_pasien']."'");
	//$dbtable->setShowAll(true);
	$adapter=new SummaryAdapter();
	$adapter->setUseNumber(true, "No.","back.");
	$adapter->add("Group", "nama_grup","unslug");
	$adapter->add("Jenis", "jenis_tagihan","unslug");
	$adapter->add("Nama", "nama_by");
	$adapter->add("Nama Asli", "nama_tagihan");
	$adapter->add("Ruangan", "ruangan","unslug");
	$adapter->add("Ruang", "ruang_by","unslug");
	$adapter->add("Tanggal", "tanggal");
	$adapter->add("Nilai", "nilai_by","money Rp.");
	$adapter->add("Nilai Asli", "nilai","money Rp.");
	$adapter->add("Jaspel Asli", "jaspel","trivial_1_<i class='fa fa-check'></i>_");
	$adapter->add("Jaspel", "jaspel_by","trivial_1_<i class='fa fa-check'></i>_");
	$adapter->add("Tampil", "hidden","trivial_1_<i class='fa fa-eye-slash'></i>_");
	$adapter->addFixValue("Group","<strong>TOTAL</strong>");
    $adapter->addSummary("Nilai Asli", "nilai","money Rp.");
    $adapter->addSummary("Nilai", "nilai_by","money Rp.");
    
	$bres=new EditBayanganResponder($dbtable, $uitable, $adapter);
	$p=$bres->command($_POST['command']);
	echo json_encode($p);
	return;
}

require_once 'smis-base/smis-include-service-consumer.php';
$service = new ServiceConsumer ( $db, "get_kelas" );
$service->setCached(true,"get_kelas");
$service->execute ();
$kelas = $service->getContent ();
$option_kelas = new OptionBuilder ();
$option_kelas->add("","","1");
foreach ( $kelas as $k ) {
	$nama = $k ['nama'];
	$slug = $k ['slug'];
	$option_kelas->add ( $nama, $slug, "0" );
}

$urjip=new ServiceConsumer($db, "get_urjip",array());
$urjip->setMode(ServiceConsumer::$MULTIPLE_MODE);
$urjip->setCached(true,"get_urjip");
$urjip->execute();
$content=$urjip->getContent();
$ruangan_all=array();
$ruangan_all[]=array("name"=>"","value"=>"%","default"=>"1");

$ruangan=array();
$ruangan[]=array("name"=>"","value"=>"","default"=>"1");
foreach ($content as $autonomous=>$ruang){
	foreach($ruang as $nama_ruang=>$jip){
		$option=array();
		$option['value']=$nama_ruang;
		$option['name']=ArrayAdapter::format("unslug", $nama_ruang);
		$ruangan_all[]=$option;
		if($jip[$nama_ruang]=="URI" || $jip[$nama_ruang]=="URJI"){	
			$ruangan[]=$option;
		}
	}
}

$jsp=new OptionBuilder();
$jsp->addSingle("Adakan");
$jsp->addSingle("Hilangkan");
$jsp->addSingle("Ganti");
$jsp->addSingle("Kembalikan");
$jsp->addSingle("Biarkan");

$tampil=new OptionBuilder();
$tampil->addSingle("Hilangkan");
$tampil->addSingle("Tampilkan");
$tampil->addSingle("Ganti");
$tampil->addSingle("Biarkan");

$tarif=new OptionBuilder();
$tarif->addSingle("Ubah");
$tarif->addSingle("Kembalikan");
$tarif->addSingle("Biarkan");

$nm=new OptionBuilder();
$nm->addSingle("Ubah");
$nm->addSingle("Kembalikan");
$nm->addSingle("Biarkan");

$proportionalize=new Button("","","Proporsional");
$proportionalize->setIcon("fa fa-balance-scale");
$proportionalize->setClass(" btn btn-inverse");
$proportionalize->setAction("edit_bayangan.balance_scale()");
$proportionalize->setIsButton(Button::$ICONIC_TEXT);

$loadall=new Button("", "", "");
$loadall->setClass("btn btn-primary");
$loadall->setAction("edit_bayangan.loadAll()");
$loadall->setIsButton(Button::$ICONIC);
$loadall->setIcon(" fa fa-circle-o");

$uitable->addModal ( "nama_pasien", "hidden", "", $_POST['nama_pasien'] );
$uitable->addModal ( "noreg_pasien", "hidden", "", $_POST['noreg_pasien'] );
$uitable->addModal ( "nrm_pasien", "hidden", "", $_POST['nrm_pasien'] );
$uitable->addModal ( "hidden_all", "select", "Tampil", $tampil->getContent());
$uitable->addModal ( "jaspel_by_all", "select", "Jaspel", $jsp->getContent());
$uitable->addModal ( "nilai_by_all", "select", "Tarif", $tarif->getContent());
$uitable->addModal ( "nama_by_all", "select", "Ubah Nama", $nm->getContent());
$uitable->addModal ( "namaruang_by_all", "select", "Ubah Ruangan", $nm->getContent());
$uitable->addModal ( "ruang_asal_by_all", "select", "Ruangan Asal", $ruangan_all,"y",null,false);
$uitable->addModal ( "jenis_tagihan_all", "select", "Tagihan", $tipe,"y",null,true);
$uitable->addModal ( "kelas_by_all", "select", "Kelas", $option_kelas->getContent(),"y",null,true );
$uitable->addModal ( "ruang_by_all", "select", "Ruangan", $ruangan,"y",null,true);
$uitable->addModal ( "balance_scale", "money", "Proporsi", "0","y",null,false);

$form=$uitable->getModal()->getForm();
$form->addElement("", $loadall->getHtml());
$form->addElement("", $proportionalize->getHtml());

$uitable->clearContent();
$uitable->addModal ( "id", "hidden", "", "" );
$uitable->addModal ( "id_unit", "hidden", "", "","n",NULL,true );
$uitable->addModal ( "nama_tagihan", "text", "Nama", "","n",NULL,true );
$uitable->addModal ( "nama_by", "text", "Nama Baru", "","n",NULL,false);
$uitable->addModal ( "hidden", "checkbox", "Sembunyikan", "0" );
$uitable->addModal ( "jaspel_by", "checkbox", "Jaspel", "0" );
$uitable->addModal ( "kelas_by", "select", "Kelas", $option_kelas->getContent() );
$uitable->addModal ( "ruang_by", "select", "Ruangan", $ruangan_all);
$uitable->addModal ( "nilai", "money", "Nilai Asli", "0","n",NULL,true  );
$uitable->addModal ( "nilai_by", "money", "Nilai", "0" );
$uitable->addModal ( "keterangan", "textarea", "Keterangan", "","n",NULL,true );
$uitable->addModal ( "tanggal", "text", "Tanggal", "","n",NULL,true );
$uitable->addModal ( "ruangan", "text", "Ruangan", "","n",NULL,true );
$uitable->addModal ( "nama_grup", "text", "Grup", "","n",NULL,true );
$uitable->addModal ( "jenis_tagihan", "text", "Jenis", "","n",NULL,true );
$modal = $uitable->getModal ();
$modal->setTitle ( "Ubah Tagihan" );

$load=new LoadingBar("rekap_edit_bayangan_bar", "");
$mload=new Modal("rekap_edit_bayangan_modal", "", "Processing...");
$mload	->addHTML($load->getHtml(),"after");

echo $form->getHtml ();
echo $uitable->getHtml ();
echo $modal->getHtml ();
echo $mload->getHtml();
echo addJS ( "base-js/smis-base-loading.js");
echo addJS("kasir/resource/js/edit_bayangan.js",false);
?>
<style type="text/css">
#edit_bayangan_keterangan{ width:488px; height: 189px; }
.red_text{ color:red; }
</style>
