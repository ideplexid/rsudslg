<?php
require_once ("smis-base/smis-include-service-consumer.php");
global $db;
global $user;

/*MENAGMBIL DATA ICD DIAGNOSA*/
	$ser=new ServiceConsumer($db,"get_icd_diagnosa",NULL,"medical_record");
	$ser->addData("noreg_pasien",$_POST ['noreg_pasien']);
	$ser->execute();
	$diagnosa=$ser->getContent();
	$total_diagnosa=count($diagnosa);
	$diagnosa_awal=isset($diagnosa[0])?$diagnosa[0]:array("nama_icd"=>"","kode_icd"=>"");
	$diagnosa_akhir=isset($diagnosa[$total_diagnosa-1])?$diagnosa[$total_diagnosa-1]:array("nama_icd"=>"","kode_icd"=>"");
/*END - MENAGMBIL DATA ICD DIAGNOSA*/

/*MENAGMBIL DATA ICD TINDAKAN*/
	$ser=new ServiceConsumer($db,"get_icd_tindakan",NULL);
	$ser->addData("noreg_pasien",$_POST ['noreg_pasien']);
	$ser->setMode(ServiceConsumer::$CLEAN_BOTH_JOIN_SINGLE_ARRAY);
	$ser->execute();
	$dtprosedure=$ser->getContent();
	$prosedur=array();
	foreach($dtprosedure as $one_entity){
		foreach($one_entity as $one_procedure_kode=>$konten){
			$prosedur[$one_procedure_kode]=$konten;
		}
	}
/*END - MENAGMBIL DATA ICD TINDAKAN*/

/*GABUNGAN ICD TINDAKAN DAN ICD DIAGNOSA*/
	unset($diagnosa[0]);
	unset($diagnosa[$total_diagnosa-1]);
	array_unshift ($diagnosa,$diagnosa_akhir);
	
	$nomor=1;
	$_diag_tin=array();
	foreach($diagnosa as $d){
		$x=array();
		if($nomor==1) $x['title']="Utama";
		else if($nomor==2) $x['title']="Penyerta";
		else $x['title']="";
		$x['nama_diag']=$d['nama_icd'];
		$x['kode_diag']=$d['kode_icd'];
		$x['nama_tin']="";
		$x['kode_tin']="";
		$_diag_tin[$nomor]=$x;
		$nomor++;
	}
	
	$nomor=1;
	foreach($prosedur as $p){
		if(!isset($_diag_tin[$nomor])){
			$title="";
			if($nomor==1) $title="Utama";
			else if($nomor==2) $title="Penyerta";
			$_diag_tin[$nomor]=array("title"=>$title,"nama_diag"=>"","kode_diag"=>"");
		}
		$_diag_tin[$nomor]['nama_tin']=$p['nama_icd'];
		$_diag_tin[$nomor]['kode_tin']=$p['kode_icd'];
		$nomor++;
	}
/*END - GABUNGAN ICD TINDAKAN DAN ICD DIAGNOSA*/

/*MENGAMBIL DATA PASIEN di REGISTRASI*/
	$header=array ();
	$adapter = new SimpleAdapter ();
	$uitable = new Table ( $header );
	$noreg=$_POST ['noreg_pasien'];
	$responder = new ServiceResponder ( $db, $uitable, $adapter, "get_registered" );
	$responder->addData("id", $noreg);
	$responder->addData("command", "edit");
	$data = $responder->command ( "edit" );
	$px=$data['content'];
	$kelamin=$px['kelamin']=="1"?"Perempuan":"Laki-Laki";
/*END OF MENGAMBIL DATA PASIEN DI REGISTRASI DI SIMPAN DI $PX*/


/*MENGAMBIL DATA KOTA, CETAK TANGGAL, PENCETAK ,NAMA RS DAN ALAMAT RS*/
	$kota=getSettings($db, "cashier-simple-kwitansi-town", "");
	$pencetak=$user->getNameOnly();
	$autonomous=getSettings($db, "smis_autonomous_name", "SMIS");
	$nama_rs=getSettings($db, "cashier-simple-kwitansi-rs", $autonomous);
	$autonomous_address=getSettings($db, "smis_autonomous_address", "LOCALHOST");
	$alamat_rs=getSettings($db, "cashier-simple-kwitansi-address", $autonomous_address);
	$tgl_pulang=($px['tanggal_pulang']=="0000-00-00 00:00:00" || $px['tanggal_pulang']=="0000-00-00") ?date("Y-m-d H:i"):$px['tanggal_pulang'];
	$tgl_masuk= $px['tanggal'];
	$tgl_lahir= ArrayAdapter::format("date d M Y",$px['tgl_lahir']);
/*END MENGAMBIL DATA KOTA, CETAK TANGGAL, PENCETAK ,NAMA RS DAN ALAMAT RS*/

/*start - header*/
	$t=new TablePrint();
	$t->setDefaultBootrapClass(true);
	$t->setMaxWidth(false);
	$t->setTableClass("lap_bpjs_irja");
	$t->addColumn($autonomous,8,1)
	  ->commit("body");
	$t->addColumn("Rawat Jalan",8,1)
	  ->commit("body");

	$t->addColumn("Nama Pasien",3,1)
	  ->addColumn("Umur")
	  ->addColumn("Jenis Kelamin")
	  ->addColumn("Tanggal Lahir")
	  ->addColumn("No. RM")
	  ->addColumn("No. Reg")
	  ->commit("body");

	$t->addColumn($px['nama_pasien'],3,1)
	  ->addColumn($px['umur'])
	  ->addColumn($kelamin)
	  ->addColumn($tgl_lahir)
	  ->addColumn(ArrayAdapter::format("only-digit8",$px['nrm']))
	  ->addColumn(ArrayAdapter::format("only-digit8",$noreg))
	  ->commit("body");
	  
	$t->addColumn("Nomor SEP",1,1)
	  ->addColumn("",3,1)
	  ->addColumn("Nomor BPJS",1,1)
	  ->addColumn($px['nobpjs'],3,1)
	  ->commit("body");
/*end - header*/

/*start diagnosa*/

	$t->addColumn("Diagnosis Awal",1,1)
	  ->addColumn($diagnosa_awal['nama_icd'],3,1)
	  ->addColumn("Kode ICD",1,1)
	  ->addColumn($diagnosa_awal['kode_icd'],3,1)
	  ->commit("body");

	$t->addColumn("Jenis Perawatan",1,1)
	  ->addColumn("Rawat Jalan / IRJA",3,1)
	  ->addColumn("Ruangan",1,1)
	  ->addColumn("",3,1)
	  ->commit("body");

	$t->addColumn("Asessmen Klinis / Dokter P. Jawab :",2,1)
	  ->addColumn($px['nama_dokter'],2,1)
	  ->addColumn("Tgl Masuk :")
	  ->addColumn(ArrayAdapter::format("date d M Y",$tgl_masuk))
	  ->addColumn("Tgl Keluar")
	  ->addColumn(ArrayAdapter::format("date d M Y",$tgl_pulang))
	  ->commit("body");

	$t->addColumn("Diagnosa Akhir",3,1)
	  ->addColumn("ICD",1,1)
	  ->addColumn("Jenis Tindakan",3,1)
	  ->addColumn("ICD",1,1)
	  ->commit("body");
	
	foreach($_diag_tin as $row){
		//kode untuk diagnosa akhir penyerta		
		$t->addColumn($row['title'],1,1)
		  ->addColumn($row['nama_diag'],2,1)
		  ->addColumn($row['kode_diag'],1,1)
		  ->addColumn($row['nama_tin'],3,1)
		  ->addColumn($row['kode_tin'],1,1)
		  ->commit("body");
	};
	
/*end diagnosa*/

/*start - tindakan ruang dan obat*/
	$t->addColumn("Tindakan Ruangan",2,1)
	  ->addColumn("Jumlah",1,1)
	  ->addColumn("Biaya",1,1)
	  ->addColumn("Obat",2,1)
	  ->addColumn("Jumlah",1,1)
	  ->addColumn("Biaya",1,1)
	  ->commit("body");
	  
	$tin_obat=array();
	$tin="SELECT * FROM smis_ksr_kolektif 
        WHERE noreg_pasien='$noreg' 
        AND prop!='del' 
        AND jenis_tagihan!='laboratory' 
        AND jenis_tagihan!='radiology' 
        AND jenis_tagihan!='resep' 
        AND jenis_tagihan!='return_resep'
        AND akunting_only=0";
	$tindakan=$db->get_result($tin);
	$number=0;
	foreach($tindakan as $x){
		if(!isset($tin_obat[$number])){
			$tin_obat[$number]=array();
		}
		$tin_obat[$number]['ntin']=$x->nama_tagihan;
		$tin_obat[$number]['jtin']=1;
		$tin_obat[$number]['htin']=$x->total;
		$number++;
	}

	$obt="SELECT * FROM smis_ksr_kolektif 
            WHERE noreg_pasien='$noreg' 
            AND prop!='del' 
            AND akunting_only='0' 
            AND ( jenis_tagihan='resep' OR jenis_tagihan='return_resep' OR jenis_tagihan='alok') ";
	$obat=$db->get_result($obt);
	$number=0;
	foreach($obat as $x){
		if(!isset($tin_obat[$number])){
			$tin_obat[$number]=array();
		}
		$tin_obat[$number]['nobat']=$x->nama_tagihan;
		$tin_obat[$number]['jobat']=$x->quantity;
		$tin_obat[$number]['hobat']=$x->total/$x->quantity;
		$number++;
	}
	
	foreach($tin_obat as $x){
		$t->addColumn(isset($x['ntin'])?$x['ntin']:"",2,1)
		  ->addColumn(isset($x['jtin'])?$x['jtin']:"",1,1)
		  ->addColumn(isset($x['htin'])?$x['htin']:"",1,1)
		  ->addColumn(isset($x['nobat'])?$x['nobat']:"",2,1)
		  ->addColumn(isset($x['jobat'])?$x['jobat']:"",1,1)
		  ->addColumn(isset($x['hobat'])?$x['hobat']:"",1,1)
		  ->commit("body");
	}
/*end - tindakan ruang dan obat*/

/*start - lab dan radiology*/
	$t->addColumn("Radiology",2,1)
	  ->addColumn("Jumlah",1,1)
	  ->addColumn("Biaya",1,1)
	  ->addColumn("Laboratory",2,1)
	  ->addColumn("Jumlah",1,1)
	  ->addColumn("Biaya",1,1)
	  ->commit("body");


	$lab_rad=array();
	$lab="SELECT * FROM 
            smis_ksr_kolektif             
            WHERE noreg_pasien='$noreg' 
            AND prop!='del' 
            AND akunting_only='0' 
            AND jenis_tagihan='laboratory'";
	$laboratory=$db->get_result($lab);
	$number=0;
	foreach($laboratory as $x){
		if(!isset($lab_rad[$number])){
			$lab_rad[$number]=array();
		}
		$lab_rad[$number]['nlab']=substr($x->nama_tagihan,13);
		$lab_rad[$number]['jlab']=1;
		$lab_rad[$number]['hlab']=$x->total;
		$number++;
	}

	$rad="SELECT * FROM smis_ksr_kolektif 
            WHERE noreg_pasien='$noreg' 
            AND prop!='del' 
            AND jenis_tagihan='radiology'
            AND akunting_only='0' ";
	$radiologi=$db->get_result($rad);
	$number=0;
	foreach($radiologi as $x){
		if(!isset($lab_rad[$number])){
			$lab_rad[$number]=array();
		}
		$lab_rad[$number]['nrad']=$x->nama_tagihan;
		$lab_rad[$number]['jrad']=1;
		$lab_rad[$number]['hrad']=$x->total;
		$number++;
	}
	
	foreach($lab_rad as $x){
		$t->addColumn(isset($x['nrad'])?$x['nrad']:"",2,1)
		  ->addColumn(isset($x['jrad'])?$x['jrad']:"",1,1)
		  ->addColumn(isset($x['hrad'])?$x['hrad']:"",1,1)
		  ->addColumn(isset($x['nlab'])?$x['nlab']:"",2,1)
		  ->addColumn(isset($x['jlab'])?$x['jlab']:"",1,1)
		  ->addColumn(isset($x['hlab'])?$x['hlab']:"",1,1)
		  ->commit("body");	}
/*end - lab dan radiology*/

/*start - footer*/
	$query="SELECT SUM(total) 
            FROM smis_ksr_kolektif
            WHERE noreg_pasien='$noreg' 
            AND akunting_only='0' 
            AND prop!='del'";
	$total=$db->get_var($query);
	$t->addColumn("Total Biaya",4,1)
	  ->addColumn($total,4,1)
	  ->commit("body");
/*end - footer*/

echo $t->getHtml();

?>

