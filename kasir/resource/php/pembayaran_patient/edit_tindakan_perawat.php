<?php
global $db;
require_once 'smis-base/smis-include-service-consumer.php';
require_once 'smis-libs-class/ServiceProviderList.php';


/* GRUP SUPER COMMAND */
if(isset($_POST['super_command'])){
    $super=new SuperCommand();
    if($_POST['super_command']=="tarif_keperawatan_perawat"){
        $hader=array ("Nama","Kelas","Tarif" );
		$dktable = new Table ( $hader, "", NULL, true );
		$dktable->setName ( "tarif_keperawatan_perawat" )
				->setModel ( Table::$SELECT );
		$dkadapter = new SimpleAdapter ();
		$dkadapter	->add ( "Nama", "nama" )
					->add ( "Kelas", "kelas", "unslug" )
					->add ( "Tarif", "tarif", "money Rp." );
		$tarif = new ServiceResponder ( $this->db, $dktable, $dkadapter, "get_keperawatan" );
		$super->addResponder ( "tarif_keperawatan_perawat", $tarif );
    }
    
    $data=$super->initialize();
    if($data!=null){
        echo $data;
        return;
    }
}
/* END - GRUP SUPER COMMAND */



$uitable = new Table ( array ('Tanggal','Nama','Perawat',"Satuan","Jumlah","Biaya" ), "", NULL, true );
$uitable->setName ( "edit_tindakan_perawat" );

if(isset($_POST['header'])){
    $header=json_decode($_POST['header'],true);
    $uitable->setHeader($header);
}

if (isset ( $_POST ['command'] )) {
	$adapter = new SimpleAdapter ();
	$adapter->add ( "Nama", "nama_tindakan" )
            ->add ( "Perawat", "nama_perawat" )
            ->add ( "Kelas", "kelas", "unslug" )
            ->add ( "Satuan", "satuan", "money Rp." )
            ->add ( "Jumlah", "jumlah" )
            ->add ( "Biaya", "harga_tindakan", "money Rp." )
            ->add ( "Tanggal", "waktu", "date d M Y H:i" );
	$dbres = new ServiceResponder($db, $uitable, $adapter, "edit_tindakan_perawat",$_POST["ruang"]);
	$data = $dbres->command ( $_POST ['command'] );
	echo json_encode ( $data );
	return;
}

$noreg_pasien="";
if( isset($_POST['noreg']) && $_POST['noreg']!="" || isset($_POST['noreg_pasien']) && $_POST['noreg_pasien']!="" ){
    $noreg_pasien=isset($_POST['noreg'])?$_POST['noreg']:$_POST['noreg_pasien'];
}

$df_ruangan="";
$jumlah_perawat=1;
if(isset($_POST['ruangan']) && $_POST['ruangan']!=""){
    $df_ruangan=$_POST['ruangan'];
    $serv=new ServiceConsumer($db,"setup_tindakan_perawat",NULL,$df_ruangan);
    $serv->execute();
    $content=$serv->getContent();
    $jumlah_perawat=$content['jumlah_perawat'];
}

$service=new ServiceProviderList($db,"edit_tindakan_perawat",$df_ruangan);
$service->execute();
$ruangan=$service->getContent();
	
$uitable->addModal("nama_pasien",  "hidden","", "","",NULL,true);
$uitable->addModal("nrm_pasien", "hidden", "", "","",NULL,true);
$uitable->addModal("noreg_pasien", "hidden","",  $noreg_pasien,"",NULL,true);
$uitable->addModal("carabayar", "hidden","",  "","",NULL,true);
$uitable->addModal("ruang", "select", "Ruangan", $ruangan);
$form=$uitable->getModal()->setTitle("Pasien")->getForm();
$uitable->clearContent();

$uitable->addModal ( "id", "hidden", "",  ""  )
		->addModal ( "waktu", "datetime", "Waktu", date ( "Y-m-d H:i" ) )
        ->addModal ( "nama_tindakan", "chooser-edit_tindakan_perawat-tarif_keperawatan_perawat-Pilih Tindakan", "Tindakan", "", 'n', null, false )
        ->addModal ( "id_tindakan", "hidden", "", "0" )
        ->addModal ( "kelas", "text", "Kelas", "", 'n', null, true )
        ->addModal ( "satuan", "money", "Harga", "", 'n', null, true )
        ->addModal ( "jumlah", "text", "Jumlah", "1", 'n', "numeric", false,null,false,"save")
        ->addModal ( "harga_tindakan", "money", "Total", "", 'n', null, true )
        ->addModal ("jaspel", "hidden","",  "","",NULL,true)
        ->addModal ( "jaspel_lain_lain", "hidden", "","" )
        ->addModal ( "jaspel_penunjang", "hidden", "","" );

$number=array("I"=>"",
            "II"=>"_dua",
            "III"=>"_tiga",
            "IV"=>"_empat",
            "V"=>"_lima",
            "VI"=>"_enam",
            "VII"=>"_tujuh",
            "VIII"=>"_delapan",
            "IX"=>"_sembilan",
            "X"=>"_sepuluh"
            );
$numero=1;
foreach($number as $rum=>$num){
    if($numero<=$jumlah_perawat){
        $uitable	->addModal ( "nama_perawat".$num, "chooser-edit_tindakan_perawat-perawat_edit_tindakan_perawat".$num."-Pilih Perawat ".$rum, "Perawat ".$rum, "" );	
    }else{
        $uitable->addModal ( "nama_perawat".$num, "hidden", "", "","n",null,false,null,false);
    }
    $uitable->addModal ( "id_perawat".$num, "hidden", "", "" );
    $numero++;
}
echo $uitable->getModal()->setTitle("Tindakan Perawat")->getHtml();
echo $form ->getHtml();
echo $uitable->getHtml ();
echo addJS ( "framework/smis/js/table_action.js" );
echo addJS ( "framework/bootstrap/js/bootstrap-datetimepicker.js" );
echo addCSS ( "framework/bootstrap/css/bootstrap-datetimepicker.css" );
echo addJS ( "kasir/resource/js/edit_tindakan_perawat.js",false );

?>

