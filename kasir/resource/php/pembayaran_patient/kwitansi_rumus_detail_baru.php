<?php
/**
 * kwitansi ini dibuat agar tagihan pasien sesuai dengan rumus yang sudah di tetapkan.
 * dengan adanya kwitansi rumus ini , tagihan pasien akan lebih mudah mengikuti rumus yang
 * sudah ada.
 * 
 * perbedaanya pada kwitansi ini akan ditampilkan detail dari masing-masing tagihan 
 * yang ada. seperti dokter, konsul dan setiap ruangan.
 * 
 * @author      : Nurul Huda
 * @license     : goblooge@gmail.com
 * @since       : 23 Feb 2017
 * @version     : 1.0
 * @database    :   - smis_ksr_rumus
 *                  - smis_ksr_kwitansi_rumus_detail
 * @service     :   - get_last_position
 *                  - get_registered
 * 
 * */

require_once ("smis-base/smis-include-service-consumer.php");
global $db;
global $user;

if(isset($_POST['super_command']) && $_POST['super_command']=="rumus_kwitansi_rumus_detail"){
    $uitable=new Table(array("No.","Nama","Biaya","Potongan","Tagihan","Keterangan"));
    $uitable->setName("rumus_kwitansi_rumus_detail");
    $uitable->setModel(Table::$SELECT);
    $rmstable=new DBTable($db,"smis_ksr_rumus");
    $adapter=new SimpleAdapter();
    $adapter->setUseNumber(true,"No.","back.");
    $adapter->add("Nama","nama");
    $adapter->add("Biaya","rumus_biaya");
    $adapter->add("Potongan","rumus_potongan");
    $adapter->add("Tagihan","rumus_tagihan");
    $adapter->add("Keterangan","keterangan");
    $dbresponder=new DBResponder($rmstable,$uitable,$adapter);
    $super=new SuperCommand();
    $super->addResponder("rumus_kwitansi_rumus_detail",$dbresponder);
    $supr=$super->initialize();
    if($supr!=null){
        echo $supr;
    }
    return;
}

$header=array ();
$adapter = new SimpleAdapter ();
$uitable = new Table ( $header );
$uitable->setName("kwitansi_rumus_detail");
$rumustbl=new DBTable($db,"smis_ksr_tagihan_rumus");

if(isset($_POST['command'])){
    $dbres=new DBResponder($rumustbl,$uitable,$adapter);
    if($dbres->isSave()){
        /* adalah biaya total pelayanan terhadap pasien oleh pasien */
        $X=$_POST['tarif_plafon_inacbg'];
        $Y=$_POST['tarif_plafon_kelas1'];
        $rumusT=$_POST['rumus_biaya'];
        $rumusT=str_replace("X",$X,$rumusT);
        $rumusT=str_replace("Y",$Y,$rumusT);
        eval( '$nilaiT= (' . $rumusT. ');' );
        $T=$nilaiT;
        $dbres->addColumnFixValue("total_biaya",$T);
        
        /* adalah potongan pengurangan biaya dari pelayanan pasien */
        $rumusU=$_POST['rumus_potongan'];
        $rumusU=str_replace("X",$X,$rumusU);
        $rumusU=str_replace("Y",$Y,$rumusU);
        $rumusU=str_replace("T",$T,$rumusU);       
        eval( '$nilaiU= (' . $rumusU. ');' );
        $U=$nilaiU;
        $dbres->addColumnFixValue("total_potongan",$U);
        
        /* adalah biaya yang harus dibayar oleh pasien */
        $rumusZ=$_POST['rumus_tagihan'];
        $rumusZ=str_replace("X",$X,$rumusZ);
        $rumusZ=str_replace("Y",$Y,$rumusZ);
        $rumusZ=str_replace("T",$T,$rumusZ);
        $rumusZ=str_replace("U",$U,$rumusZ);
        eval( '$nilaiZ= (' . $rumusZ. ');' );
        $Z=$nilaiZ;
        $dbres->addColumnFixValue("total_tagihan",$Z);
        
    }
    $data=$dbres->command($_POST['command']);
    echo json_encode($data);
    return;
}

/*MENGAMBIL DATA PASIEN di REGISTRASI*/
require_once "kasir/function/get_patient_by_noreg.php";
require_once "kasir/function/get_system_setup.php";
$noreg           = $_POST ['noreg_pasien'];
$px              = get_patient_by_noreg($db,$noreg);
$_setup          = get_system_setup($db,$user,$noreg,$px,"");

/** TODO SYNCH TGL PULANG */
$_NAMA_PASIEN	 =	$px['nama_pasien']." / ".$px['namapenanggungjawab'];
$_ALAMAT_PASIEN	 =	$px['alamat_pasien'];
$_NRMNORG_PASIEN =	ArrayAdapter::format("only-digit8",$px['nrm'])." / ".ArrayAdapter::format("only-digit8",$px['id']);
$_TGL_MASUK		 =	ArrayAdapter::format("date d M Y H:i",$px['tgl_masuk']);
$_TGL_KELUAR	 =	ArrayAdapter::format("date d M Y H:i",$_setup['tgl_pulang']);
$_NOMOR_KWITANSI =  $_setup['no_kwitansi'];
$_TGL_KWITANSI	 = 	$_setup['waktu_kwitansi'];
$_NILAI_BAYAR	 =	$_setup['nilai_kwitansi'];

$_ID_RUMUS_PV   = 0;
$_ASURANSI      =   $px['nama_asuransi']; 
$_CARABAYAR     =   ArrayAdapter::format("unslug",$px['carabayar']); 
/* END OF MENGAMBIL DATA PASIEN DI REGISTRASI DI SIMPAN DI $PX */


$rumustbl->setFetchMethode(DBTable::$ARRAY_FETCH);
$rumus=$rumustbl->select(array("noreg_pasien"=>$noreg));
if($rumus==null){
    $responder = new ServiceConsumer ( $db, "get_last_position",$params = array("noreg_pasien"	=> $_POST['noreg_pasien']),"medical_record" );
    $responder->execute();
    $_LAST_POSITION=ArrayAdapter::format("unslug",$responder->getContent());
    
    $rumus['nama_pasien']       = $px['nama_pasien'];
    $rumus['nrm_pasien']        = $px['nrm'];
    $rumus['noreg_pasien']      = $noreg;
    $rumus['ruang_pelayanan']   = $_LAST_POSITION;
    $rumus['id_rumus_pv']       = $_ID_RUMUS_PV;
    $rumustbl->insert($rumus);
    $rumus['id']=$rumustbl->get_inserted_id();
}
$_ID_RUMUS_PV       = $rumus['id_rumus_pv'];

/*service mengambil Rumus PV*/
$params=array();
$params['command']="edit";
$params['id']=$_ID_RUMUS_PV;

$responder = new ServiceConsumer ( $db, "get_formula_pv",$params,"manajemen" );
$responder->execute();
$content=$responder->getContent();
$_FORMULA=$content;
/*END - service mengambil Rumus PV*/



/*MEMBUAT TOMBOL CETAK*/
$cetak=new Button("print_button_simple_kwitansi", "", " Cetak ");
$cetak->addClass("btn btn-primary");
$cetak->setIcon(" fa fa-print ");
$cetak->setIsButton(BUtton::$ICONIC_TEXT);
$cetak->setAction("kwitansi_rumus_detail_print()");
if( $px['uri']=="0" && getSettings($db,"cashier-simple-kwitansi-cek-print-rj","0")=="1" || $px['uri']=="1" && getSettings($db,"cashier-simple-kwitansi-cek-print-ri","0")=="1"){
    $cetak->setAction("cek_kwitansi_rumus_detail_print()");
}

$simpan=new Button("print_button_simple_kwitansi", "", " Simpan ");
$simpan->addClass("btn btn-primary");
$simpan->setIcon(" fa fa-save ");
$simpan->setIsButton(Button::$ICONIC_TEXT);
$simpan->setAction("kwitansi_rumus_detail.save()");
/*AKHIR DARI MEMBUAT TOMBOL CETAK*/

/*FORM*/
$kelas_pasien=new OptionBuilder();
$kelas_pasien->addSingle("",($rumus['kelas_plafon']==""?"1":"0"));
$kelas_pasien->addSingle("Kelas I",($rumus['kelas_plafon']=="Kelas I"?"1":"0"));
$kelas_pasien->addSingle("Kelas II",($rumus['kelas_plafon']=="Kelas II"?"1":"0"));
$kelas_pasien->addSingle("Kelas III",($rumus['kelas_plafon']=="Kelas III"?"1":"0"));
        
$uitable->addModal("id","hidden","",$rumus['id']);
$uitable->addModal("nama_pasien","hidden","",$rumus['nama_pasien']);
$uitable->addModal("nrm_pasien","hidden","",$rumus['nrm_pasien']);
$uitable->addModal("noreg_pasien","hidden","",$rumus['noreg_pasien']);
$uitable->addModal("kelas_plafon","select","Kelas",$kelas_pasien->getContent());
$uitable->addModal("kode_inacbg","text","Kode INACBG",$rumus['kode_inacbg']);
$uitable->addModal("deskripsi_inacbg","text","Deskripsi",$rumus['deskripsi_inacbg']);
$uitable->addModal("ruang_pelayanan","text","R. Pelayanan",$rumus['ruang_pelayanan']);
$uitable->addModal("id_rumus","hidden","",$rumus['id_rumus']);
$uitable->addModal("nama_rumus","chooser-kwitansi_rumus_detail-rumus_kwitansi_rumus_detail-Pilih Rumus","Rumus",$rumus['nama_rumus']);
$uitable->addModal("rumus_tagihan","hidden","",$rumus['rumus_tagihan']);
$uitable->addModal("rumus_potongan","hidden","",$rumus['rumus_potongan']);
$uitable->addModal("rumus_biaya","hidden","",$rumus['rumus_biaya']);
$uitable->addModal("tarif_plafon_inacbg","money","Plafon INACBG",$rumus['tarif_plafon_inacbg']);
$uitable->addModal("tarif_plafon_kelas1","money","Plafon Kelas I",$rumus['tarif_plafon_kelas1']);

$form=$uitable->getModal()->getForm();
$form->addElement("",$cetak);
$form->addElement("",$simpan);
/*END - FOM*/

/*MENGAMBIL DATA RUMAH SAKIT*/
$autonomous=getSettings($db, "smis_autonomous_name", "SMIS");
$nama_rs=getSettings($db, "cashier-simple-kwitansi-rs", $autonomous);
$autonomous_address=getSettings($db, "smis_autonomous_address", "LOCALHOST");
$alamat_rs=getSettings($db, "cashier-simple-kwitansi-address", $autonomous_address);
/*END - MENGAMBIL DATA RUMAH SAKIT*/

/*MENGAMBIL DATA TOTAL TAGIHAN*/
$query="SELECT sum(total) FROM smis_ksr_kolektif WHERE prop!='del' AND akunting_only='0'  AND noreg_pasien='$noreg' ";
$_TOTAL_TAGIHAN=$db->get_var($query);
/*END - MENGAMBIL DATA TOTAL TAGIHAN*/

$table=new TablePrint("kwitansi_rumus_detail");
$table->setTableClass("kwitansi_print");
$table->setMaxWidth(true);

$table->addColumn($nama_rs."</br>".$alamat_rs, 6, 1,NULL,NULL,"center bold ");
$table->commit("body");

$table->addColumn("NAMA / PJ",1,1);
$table->addColumn($_NAMA_PASIEN,2,1);
$table->addColumn("No. Kwitansi : ".$_NOMOR_KWITANSI);
$table->commit("body");

$table->addColumn("ALAMAT",1,1);
$table->addColumn($_ALAMAT_PASIEN,2,1);
$table->addColumn("TANGGAL MASUK",1,1,null,null,"bold");
$table->commit("body");

$table->addColumn("NRM / NO. REG",1,1);
$table->addColumn($_NRMNORG_PASIEN,2,1);
$table->addColumn($_TGL_MASUK,1,1);
$table->commit("body");

$table->addColumn("Cara Bayar Pasien",1,1);
$table->addColumn($_CARABAYAR,2,1);
$table->addColumn("TANGGAL KELUAR",1,1,null,null,"bold");
$table->commit("body");

$table->addColumn("Kode Rincian",1,1);
$table->addColumn($_FORMULA['kode'],1,1);
$table->addSpace(1,1);
$table->addColumn($_TGL_KELUAR);
$table->commit("body");

$table->addSpace(4,1);
$table->commit("body");

$table->addColumn("INA-CBG ",1,1);
$table->addColumn(" : ".$rumus['kode_inacbg']);
$table->addColumn("Kelas Pasien ",1,1);
$table->addColumn(" : ".$rumus['kelas_plafon']);
$table->commit("body");

$table->addColumn("Deskripsi ",1,1);
$table->addColumn(" : ".$rumus['deskripsi_inacbg']);
$table->addColumn("Ruang Pelayanan ",1,1);
$table->addColumn(" : ".$rumus['ruang_pelayanan']);
$table->commit("body");

$table->addColumn("RINCIAN",1,1,null,null,"bold upperscore center");
$table->addColumn("KETERANGAN",1,1,null,null,"bold upperscore center");
$table->addColumn("TOTAL",2,1,null,null,"bold upperscore center");
$table->commit("body");

/*START - MEMASUKAN DETAIL RUMUS*/
$_basis_persen=$rumus['total_biaya'];
$_total_biaya_yg_harus_ditanggung=0;

$table->addSpace(4,1,"body");
$table->addColumn("KAMAR DAN MAKAN",4,1,null,null,"bold");


//makan dll
$opmk=($_FORMULA['jasa_sarana']+$_FORMULA['jasa_umum']+$_FORMULA['cito'])*$_basis_persen;
$_total_biaya_yg_harus_ditanggung+=$opmk;
$table->addColumn("Operasional, Kamar, dan Makan",1,1);
$table->addColumn("-",1,1,null,null,"center");
$table->addColumn(ArrayAdapter::format("money Rp. ",$opmk),2,1);
$table->commit("body");


//bmhp
$bmhp=$_FORMULA['bmhp']*$_basis_persen;
$_total_biaya_yg_harus_ditanggung+=$bmhp;
$table->addColumn("Obat dan Alat",1,1);
$table->addColumn("-",1,1,null,null,"center");
$table->addColumn(ArrayAdapter::format("money Rp. ",$bmhp),2,1);
$table->commit("body");


//dokter
$table->addSpace(4,1,"body");
$table->addColumn("TINDAKAN DOKTER",4,1,null,null,"bold");
$table->commit("body");
    $list_dokter_pakem=array();
    $list_dokter_pakem[] =   array("formula"=>$_FORMULA['dokter_igd'],"ket"=>"Dokter IGD","name"=>$rumus['nama_igd']);
    $list_dokter_pakem[] =   array("formula"=>$_FORMULA['dokter_anastesi'],"ket"=>"Dokter Anastesi","name"=>$rumus['nama_anastesi']);
    $list_dokter_pakem[] =   array("formula"=>$_FORMULA['dokter_anak'],"ket"=>"Dokter Anak","name"=>$rumus['nama_anak']);
    $list_dokter_pakem[] =   array("formula"=>$_FORMULA['dokter_spesialis'],"ket"=>"Dokter Spesialis","name"=>$rumus['nama_spesialis']);
    $list_dokter_pakem[] =   array("formula"=>$_FORMULA['dokter_umum'],"ket"=>"Dokter Umum","name"=>$rumus['nama_umum']);
    
    foreach($list_dokter_pakem as $k=>$v){
        if($v['formula']!=0){
            $uang=($v['formula'])*$_basis_persen;
            $_total_biaya_yg_harus_ditanggung+=$uang;
            $table->addColumn($v['name'],1,1);
            $table->addColumn($v['ket'],1,1,null,null,"center");
            $table->addColumn(ArrayAdapter::format("money Rp. ",$uang),2,1);
            $table->commit("body");
        }
    }
    
    //dokter konsul
    $uang=$_FORMULA['dokter_konsul']*$_basis_persen;
    if($uang>0){
        $_total_biaya_yg_harus_ditanggung+=$uang;
        if($rumus['nama_konsul_satu']!="" && $rumus['nama_konsul_dua']!=""){
            $table->addColumn($rumus['nama_konsul_satu'],1,1);
            $table->addColumn("Dokter Konsul I",1,1,null,null,"center");
            $table->addColumn(ArrayAdapter::format("money Rp. ",$uang/2),2,1);
            $table->commit("body");
            
            $table->addColumn($rumus['nama_konsul_dua'],1,1);
            $table->addColumn("Dokter Konsul II",1,1,null,null,"center");
            $table->addColumn(ArrayAdapter::format("money Rp. ",$uang/2),2,1);
            $table->commit("body");
        }else if($rumus['nama_konsul_satu']!=""){
            $table->addColumn($rumus['nama_konsul_satu'],1,1);
            $table->addColumn("Dokter Konsul I",1,1,null,null,"center");
            $table->addColumn(ArrayAdapter::format("money Rp. ",$uang),2,1);
            $table->commit("body");
        }else if($rumus['nama_konsul_dua']!=""){
            $table->addColumn($rumus['nama_konsul_dua'],1,1);
            $table->addColumn("Dokter Konsul II",1,1,null,null,"center");
            $table->addColumn(ArrayAdapter::format("money Rp. ",$uang),2,1);
            $table->commit("body");
        }else{
            $table->addColumn("-",1,1);
            $table->addColumn("Dokter Konsul",1,1,null,null,"center");
            $table->addColumn(ArrayAdapter::format("money Rp. ",$uang),2,1);
            $table->commit("body");
        }
    }
    
    //dokter dpjp
    $uang=$_FORMULA['dokter_dpjp']*$_basis_persen;
    $jumlah_dpjp=$rumus['nama_dpjp_satu']!=""?1:0;
    $jumlah_dpjp+=$rumus['nama_dpjp_dua']!=""?1:0;
    $jumlah_dpjp+=$rumus['nama_dpjp_tiga']!=""?1:0;
    if($jumlah_dpjp>0){
        $uang=$uang/$jumlah_dpjp;
    }
    
    if($rumus['nama_dpjp_satu']!=""){
        $biaya=$rumus['biaya_dpjp_satu']!=0?$rumus['biaya_dpjp_satu']:$uang;
        $_total_biaya_yg_harus_ditanggung+=$biaya;
        $table->addColumn($rumus['nama_dpjp_satu'],1,1);
        $table->addColumn("Dokter DPJP I",1,1,null,null,"left");
        $table->addColumn(ArrayAdapter::format("money Rp. ",$biaya),2,1);
        $table->commit("body");
    }
    
    if($rumus['nama_dpjp_dua']!=""){
        $biaya=$rumus['biaya_dpjp_dua']!=0?$rumus['biaya_dpjp_dua']:$uang;
        $_total_biaya_yg_harus_ditanggung+=$biaya;
        $table->addColumn($rumus['nama_dpjp_dua'],1,1);
        $table->addColumn("Dokter DPJP II",1,1,null,null,"left");
        $table->addColumn(ArrayAdapter::format("money Rp. ",$biaya),2,1);
        $table->commit("body");
    }
    
    if($rumus['nama_dpjp_tiga']!=""){
        $biaya=$rumus['biaya_dpjp_tiga']!=0?$rumus['biaya_dpjp_tiga']:$uang;
        $_total_biaya_yg_harus_ditanggung+=$biaya;
        $table->addColumn($rumus['nama_dpjp_tiga'],1,1);
        $table->addColumn("Dokter DPJP III",1,1,null,null,"left");
        $table->addColumn(ArrayAdapter::format("money Rp. ",$biaya),2,1);
        $table->commit("body");
    }
    /* kode ini untuk mengatasi dokter DPJP yang kosong */
    if($jumlah_dpjp==0){
        $biaya=$uang;
        $_total_biaya_yg_harus_ditanggung+=$biaya;
        $table->addColumn("",1,1);
        $table->addColumn("Dokter DPJP",1,1,null,null,"left");
        $table->addColumn(ArrayAdapter::format("money Rp. ",$biaya),2,1);
        $table->commit("body");
    }
    

//perawatan
$table->addSpace(4,1,"body");
$table->addColumn("TINDAKAN KEPERAWATAN",4,1,null,null,"bold");
$table->commit("body");
    //icu
    $list_ruang_pakem=array();
    $list_ruang_pakem['IGD']=$_FORMULA['perawat_igd'];
    $list_ruang_pakem['ICU']=$_FORMULA['perawat_icu'];
    $list_ruang_pakem['OK']=$_FORMULA['perawat_ok'];
    $list_ruang_pakem['Recovery Room dan Anastesi']=$_FORMULA['perawat_anastesi']+$_FORMULA['perawat_recovery_room'];
    
    foreach($list_ruang_pakem as $k=>$v){
        if($v!=0){
            $uang=($v)*$_basis_persen;
            $_total_biaya_yg_harus_ditanggung+=$uang;
            $table->addColumn($k,1,1);
            $table->addColumn("-",1,1,null,null,"left");
            $table->addColumn(ArrayAdapter::format("money Rp. ",$uang),2,1);
            $table->commit("body");
        }
    }
    
    //ruangan tidak pakem, berdasarkan lama rawat masing-masing ruangan
    $pembagi=abs($rumus['irna_a']+$rumus['irna_b']+$rumus['irna_b1']+$rumus['irna_c']+$rumus['irna_e']+$rumus['irna_f']+$rumus['irna_g']+$rumus['paviliun']);
    $persen=$_FORMULA['perawat_ruangan']/$pembagi;
    $list_ruangan=array();
    $list_ruangan['IRNA A']=abs($rumus['irna_a']);
    $list_ruangan['IRNA B']=abs($rumus['irna_b']);
    $list_ruangan['IRNA B1']=abs($rumus['irna_b1']);
    $list_ruangan['IRNA C']=abs($rumus['irna_c']);
    $list_ruangan['IRNA E']=abs($rumus['irna_e']);
    $list_ruangan['IRNA F']=abs($rumus['irna_f']);
    $list_ruangan['IRNA G']=abs($rumus['irna_g']);
    $list_ruangan['Paviliun']=abs($rumus['paviliun']);
    foreach($list_ruangan as $k=>$v){
        if($v!=0){
            $uang=ceil($persen*$v*$_basis_persen);
            $_total_biaya_yg_harus_ditanggung+=$uang;
            $table->addColumn($k,1,1);
            $table->addColumn($v." Hari",1,1,null,null,"left");
            $table->addColumn(ArrayAdapter::format("money Rp. ",$uang),2,1);
            $table->commit("body");
        }
    }
    

//penunjang
$table->addSpace(4,1,"body");
$table->addColumn("PENUNJANG",4,1,null,null,"bold");
$table->commit("body");
    //laboratory
    $lab=$_FORMULA['laboratory']*$_basis_persen;
    $_total_biaya_yg_harus_ditanggung+=$lab;
    $table->addColumn("Laboratory",1,1);
    $table->addColumn("-",1,1,null,null,"left");
    $table->addColumn(ArrayAdapter::format("money Rp. ",$lab),2,1);
    $table->commit("body");
    //radiology
    $rad=$_FORMULA['radiology']*$_basis_persen;
    $_total_biaya_yg_harus_ditanggung+=$rad;
    $table->addColumn("Radiology",1,1);
    $table->addColumn("-",1,1,null,null,"left");
    $table->addColumn(ArrayAdapter::format("money Rp. ",$rad),2,1);
    $table->commit("body");




/*END - MEMASUKAN DETAIL RUMUS*/

$table->addSpace(4,1,"body");
$table->addColumn("Total Tagihan",1,1);
$table->addColumn("-",1,1,null,null,"left");
$table->addColumn(ArrayAdapter::format("money Rp. ",$_total_biaya_yg_harus_ditanggung),2,1);
$table->commit("body");

$table->addColumn("Asuransi",1,1);
$table->addColumn($_ASURANSI,1,1);
$table->addColumn(ArrayAdapter::format("money Rp. ",$rumus['total_potongan']),2,1);
$table->commit("body");

$_DITANGGUNG=$_total_biaya_yg_harus_ditanggung-$rumus['total_potongan'];

if($_CARABAYAR=="BPJS"){
    $table->addColumn("Sharing BPJS",1,1);
    $table->addColumn("-",1,1);
    $table->addColumn(ArrayAdapter::format("money Rp. ",$_DITANGGUNG),2,1,null,null,"upperscore bold");
    $table->commit("body");    
}

$table->addColumn("Tagihan Pasien",1,1);
$table->addColumn("-",1,1,null,null,"left");
$table->addColumn(ArrayAdapter::format("money Rp. ",$_DITANGGUNG),2,1,null,null,"upperscore bold");
$table->commit("body");

$table->addSpace("4","1");
$table->commit("body");

$table->addColumn("PEMBAYARAN",2,1);
$table->addColumn(ArrayAdapter::format("money Rp. ", $_NILAI_BAYAR),2,1);
$table->commit("body");
loadLibrary("smis-libs-function-math");

$table->addColumn("TERBILANG : ",1,1,null,null,"upperscore underscore bold");
$table->addColumn(strtoupper(numbertell($_NILAI_BAYAR)." rupiah"),3,1,null,null,"upperscore  underscore bold");
$table->commit("body");

$_TANGGUNGAN=$_total_biaya_yg_harus_ditanggung;
/*
if($_NILAI_BAYAR*1<$_TANGGUNGAN){
	$HUTANG=$_TANGGUNGAN - $_NILAI_BAYAR;
	$table->addColumn("SISA PEMBAYARAN : ",3,1);
	$table->addColumn(ArrayAdapter::format("money Rp. ",$HUTANG),1,1);
	$table->commit("body");
}
$table->addSpace(4, 1);
$table->commit("body");
 * */
/*KODE TANDA TANGAN*/
global $user;
$kota=getSettings($db, "cashier-simple-kwitansi-town", "");
$table->addSpace(3,1);
$table->addColumn( $kota.", ".ArrayAdapter::format("date d M Y", date("Y-M-d")),1,1,NULL,NULL,"center");
$table->commit("body");
$table->addColumn("</br></br>", 4, 1,NULL,NULL,"center");
$table->commit("body");

$table->addSpace(3, 1);
$table->addColumn($user->getNameOnly(), 1, 1,NULL,NULL,"center");
$table->commit("body");


echo "<div>".$form->getHtml()."</div>";
echo "<div class='clear line'></div>";
echo "<div id='cetak_kwitansi_rumus_detail_area'>".$table->getHtml()."</div>";
echo addCSS("kasir/resource/css/kwitansi_rumus_detail.css",false);
echo addCSS("kasir/resource/css/kwitansi_print.css",false);
echo addJS("kasir/resource/js/kwitansi_rumus_detail.js",false);
?>