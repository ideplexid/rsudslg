<?php
global $db;
require_once 'smis-base/smis-include-service-consumer.php';
require_once 'smis-libs-class/ServiceProviderList.php';
require_once 'smis-libs-hrd/EmployeeResponder.php';



/* GRUP SUPER COMMAND */
if(isset($_POST['super_command'])){
    $super=new SuperCommand();
    if($_POST['super_command']=="edit_ok_nama_operasi"){
        $header=array ("Nama","Kelas","Tarif");
        $dktable = new Table ($header);
        $dktable->setName ( "edit_ok_nama_operasi" )
                ->setModel ( Table::$SELECT );
        $dkadapter = new SimpleAdapter ();
        $dkadapter->add ( "Nama", "nama" )
                  ->add ( "Kelas", "kelas", "unslug" )
                  ->add ( "Tarif", "tarif", "money Rp." );
        $tarif = new ServiceResponder ( $db, $dktable, $dkadapter, "get_tindakan_operasi","manajemen" );
		$super->addResponder ( "edit_ok_nama_operasi", $tarif);
    }
    $data=$super->initialize();
    if($data!=null){
        echo $data;
        return;
    }
}
/* END - GRUP SUPER COMMAND */



$header=array ('Tanggal',"Tindakan",'Biaya Operasi',"Operator","Asisten Operator","Anastesi","Ass. Anastesi" );
$uitable = new Table ( $header, "", NULL, true );
$uitable->setName ( "edit_ok" );

$service=new ServiceProviderList($db,"edit_ok");
$service->execute();
$ruangan=$service->getContent();
$df=getSettings($db,"cashier-default-kamar-operasi","");
foreach($ruangan as $k=>$v){
	if($v['value']==$df){
		$ruangan[$k]['default']="1";
		break;
	}
}

if (isset ( $_POST ['command'] )) {
	$adapter = new SimpleAdapter ();
	$adapter->add ( "Tanggal", "waktu", "date d M Y" );
	$adapter->add ( "Tindakan", "nama_tindakan" );
	$adapter->add ( "Operator", "nama_operator_satu" );
	$adapter->add ( "Biaya Operasi", "harga_operator_satu","money Rp." );
	$adapter->add ( "Asisten Operator", "nama_asisten_operator_satu" );
	$adapter->add ( "Anastesi", "nama_anastesi" );
	$adapter->add ( "Ass. Anastesi", "nama_asisten_anastesi" );
	$dbres = new ServiceResponder($db, $uitable, $adapter, "edit_ok",$_POST["ruang"]);
	$data = $dbres->command ( $_POST ['command'] );
	echo json_encode ( $data );
	return;
}

$noreg_pasien="";
if( isset($_POST['noreg']) && $_POST['noreg']!="" || isset($_POST['noreg_pasien']) && $_POST['noreg_pasien']!="" ){
    $noreg_pasien=isset($_POST['noreg'])?$_POST['noreg']:$_POST['noreg_pasien'];
}

$uitable->addModal("nama_pasien",  "hidden","", "","",NULL,true);
$uitable->addModal("alamat_pasien",  "hidden","", "","",NULL,true);
$uitable->addModal("nrm_pasien", "hidden", "", "","",NULL,true);
$uitable->addModal("noreg_pasien", "hidden","",  $noreg_pasien,"",NULL,true);
$uitable->addModal("carabayar", "hidden","",  "","",NULL,true);
$uitable->addModal("ruang", "select", "Ruangan", $ruangan);
$form=$uitable->getModal()->setTitle("Pasien")->getForm();
$uitable->clearContent();

$uitable->addModal("waktu", "datetime", "Waktu", date("Y-m-d H:i"),"",NULL,false);
$uitable->addModal("nama_tindakan", "chooser-edit_ok-edit_ok_nama_operasi-Jenis Tindakan", "Jenis Tindakan", "","y",null,false,null,true);
$uitable->addModal("harga_operator_satu", "money", "Biaya Operasi", "");
$uitable->addModal("nama_operator_satu",  "chooser-edit_ok-edit_ok_dokter-Dokter Operator","Operator I", "","",NULL,true);
$uitable->addModal("nama_asisten_operator_satu",  "chooser-edit_ok-edit_ok_asisten_dokter-Asisten Operator","Assisten Operator I", "","",NULL,true);
$uitable->addModal("nama_anastesi",  "chooser-edit_ok-edit_ok_anastesi-Dokter Anastesi","Dokter Anastesi", "","",NULL,true);
$uitable->addModal("nama_asisten_anastesi",  "chooser-edit_ok-edit_ok_asisten_anastesi-Asisten Anastesi","Assisten Anastesi II", "","",NULL,true);
$uitable->addModal("id_asisten_anastesi",  "hidden","", "","",NULL,true);
$uitable->addModal("id_operator_satu",  "hidden","", "","",NULL,true);
$uitable->addModal("id_anastesi",  "hidden","", "","",NULL,true);
$uitable->addModal("id_asisten_operator_satu",  "hidden","", "","",NULL,true);
$uitable->addModal("id", "hidden", "", "");

echo $uitable->getModal()->setComponentSize(Modal::$MEDIUM)->setTitle("Periksa")->getHtml();
echo $form ->getHtml();
echo $uitable->getHtml ();
echo addJS ( "framework/bootstrap/js/bootstrap-datetimepicker.js" );
echo addCSS ( "framework/bootstrap/css/bootstrap-datetimepicker.css" );echo addJS ( "framework/smis/js/table_action.js" );
echo addJS ( "kasir/resource/js/edit_ok.js",false );

?>
