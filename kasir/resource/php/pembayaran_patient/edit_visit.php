<?php
global $db;
require_once 'smis-base/smis-include-service-consumer.php';
require_once 'smis-libs-class/ServiceProviderList.php';


/* GRUP SUPER COMMAND */
if(isset($_POST['super_command'])){
    $super=new SuperCommand();
    if($_POST['super_command']=="tarif_visite"){
        $head=array ('Nama','Jabatan',"Kelas","Tarif" );
		$dkadapter = new SimpleAdapter ();
		if($_POST['separated']=="1") 
			$dkadapter->add ( "Nama", "nama_visite" );
		else 
			$dkadapter->add ( "Nama", "nama_dokter" );
		$dkadapter->add ( "Jabatan", "jabatan", "unslug" );
		$dkadapter->add ( "Kelas", "kelas", "unslug" );
		$dkadapter->add ( "Tarif", "tarif", "money Rp." );
		
		$dktable = new Table ( $head);
		$dktable->setName ( "tarif_visite" );
		$dktable->setModel ( Table::$SELECT );
		
		$tarif = new ServiceResponder ( $db, $dktable, $dkadapter, "get_visite" );
		$super->addResponder ( "tarif_visite", $tarif);
    }
    
    $data=$super->initialize();
    if($data!=null){
        echo $data;
        return;
    }
}
/* END - GRUP SUPER COMMAND */




$uitable = new Table ( NULL, "", NULL, true );
$uitable->setName ( "edit_visit" );

if(isset($_POST['header'])){
    $header=json_decode($_POST['header'],true);
    $uitable->setHeader($header);
}





if (isset ( $_POST ['command'] )) {
	$adapter = new SimpleAdapter ();
	$adapter->add ( "Tanggal", "waktu", "date d M Y H:i" );
    $adapter->add ( "Dokter", "nama_dokter" );
    $adapter->add ( "Dokter II", "nama_dokter_dua" );
    $adapter->add ( "Dokter III", "nama_dokter_tiga" );
    $adapter->add ( "Kelas", "kelas", "unslug" );
    $adapter->add ( "Harga", "harga", "money Rp." );
    $adapter->add ( "Visite", "nama_visite" );
	$dbres = new ServiceResponder($db, $uitable, $adapter, "edit_visit",$_POST["ruang"]);
	$data = $dbres->command ( $_POST ['command'] );
	echo json_encode ( $data );
	return;
}
$noreg_pasien="";
if( isset($_POST['noreg']) && $_POST['noreg']!="" || isset($_POST['noreg_pasien']) && $_POST['noreg_pasien']!="" ){
    $noreg_pasien=isset($_POST['noreg'])?$_POST['noreg']:$_POST['noreg_pasien'];
}


if(isset($_POST['ruangan']) && $_POST['ruangan']!=""){
    $df_ruangan=$_POST['ruangan'];    
}else{
    $df_ruangan=$ruangan[0]['value'];
}




$df_ruangan="";
$is_separated=0;
$jumlah_dokter=1;
$header_element=null;
if(isset($_POST['ruangan']) && $_POST['ruangan']!=""){
    $df_ruangan=$_POST['ruangan'];
    $serv=new ServiceConsumer($db,"setup_visit",NULL,$df_ruangan);
    $serv->execute();
    $content=$serv->getContent();
    $is_separated=$content['separated'];
    $jumlah_dokter=$content['jumlah_dokter'];
    $header_element=$content['header'];
    $uitable->setHeader($header_element);
}

$service=new ServiceProviderList($db,"edit_visit",$df_ruangan);
$service->execute();
$ruangan=$service->getContent();

$uitable->addModal("nama_pasien",  "hidden","", "","",NULL,true);
$uitable->addModal("nrm_pasien", "hidden", "", "","",NULL,true);
$uitable->addModal("noreg_pasien", "hidden","",  $noreg_pasien,"",NULL,true);
$uitable->addModal("carabayar", "hidden","",  "","",NULL,true);
$uitable->addModal("ruang", "select", "Ruangan", $ruangan);
$form=$uitable->getModal()->setTitle("Pasien")->getForm();
$uitable->clearContent();

$uitable->addModal ( "id", "hidden", "",  ""  );	
$uitable->addModal ( "waktu", "datetime", "Tanggal",  date("Y-m-d H:i")  );
if($is_separated=="1"){
    $uitable->addModal ( "nama_dokter", "chooser-edit_visit-dokter_visite-Pilih Dokter", "Dokter", "","n",null,false,null,true );
    $uitable->addModal ( "nama_visite", "chooser-edit_visit-tarif_visite-Pilih Visite", "Visite", "" ,"n",null,false,null,false);
}else{
    $uitable->addModal ( "nama_visite", "hidden", "", "" ,"y",null,false,null,true);
    $uitable->addModal ( "nama_dokter", "chooser-edit_visit-tarif_visite-Pilih Visite", "Dokter", "","n",null,false,null,false );
}

if($jumlah_dokter>=2){
    $uitable->addModal ( "nama_dokter_dua", "chooser-edit_visit-dokter_visite_dua-Pilih Dokter II", "Dokter II", "","n",null,false,null,false );
    $uitable->addModal ( "id_dokter_dua", "hidden", "", "" ,"n",null,false,null,false);
}
if($jumlah_dokter>=3){
    $uitable->addModal ( "nama_dokter_tiga", "chooser-edit_visit-dokter_visite_tiga-Pilih Dokter III", "Dokter III", "","n",null,false,null,false );
    $uitable->addModal ( "id_dokter_tiga", "hidden", "", "" ,"n",null,false,null,false);
}
$uitable->addModal ( "id_dokter", "hidden", "", "0" );
$uitable->addModal ( "id_visite", "hidden", "", "0" );
$uitable->addModal ( "kelas", "text", "Kelas", "", 'n', null, true );
$uitable->addModal ( "harga", "money", "Harga", "", 'y', null, true );

$separated=new Hidden("is_visite_separated", "", $is_separated);
$headelm=new Hidden("edit_visit_header_element", "", json_encode($header_element));

echo $uitable->getModal()->setTitle("Visite")->getHtml();
echo $form ->getHtml();
echo $uitable->getHtml ();
echo $separated->getHtml();
echo $headelm->getHtml();
echo addJS ( "framework/smis/js/table_action.js" );
echo addJS ( "framework/bootstrap/js/bootstrap-datetimepicker.js" );
echo addCSS ( "framework/bootstrap/css/bootstrap-datetimepicker.css" );
echo addJS ( "kasir/resource/js/edit_visit.js",false );

?>
