<?php
global $db;
require_once "smis-base/smis-include-duplicate.php";
require_once 'kasir/class/table/TableAsuransi.php';
$header=array ("ID","No. Kwitansi",'Tanggal','Nilai',"Asuransi",'No Bukti' ,"Terbayar");
$uitable = new TableAsuransi ( $header,"Pembayaran Asuransi",NULL,true );
$uitable->setPrintElementButtonEnable(true);
$uitable->setName ( "asuransi" );
$uitable->setFooterVisible(false);
$uitable->setDelButtonEnable( !(getSettings($db,"cashier-hide-del-asuransi","0")=="1") );

if (isset ( $_POST ['command'] )) {
	require_once "kasir/class/responder/PembayaranResponder.php";
	$adapter = new SummaryAdapter();
	$adapter ->addSummary("Nilai","nilai","money Rp.")
			 ->addFixValue("Tanggal","<strong>Total</strong>")
			 ->add ("ID","id","only-digit8")
			 ->add ("No. Kwitansi","no_kwitansi")
			 ->add ("Tanggal","waktu","date d M Y H:i")
			 ->add ("Nilai","nilai","money Rp.")
			 ->add ("Asuransi","nama_asuransi")
			 ->add ("No Bukti","no_bukti")
			 ->add ("Keterangan","keterangan")
			 ->add ("Terbayar","terklaim","trivial_1_Ya_Belum");
	
	$dbtable = new DBTable ( $db,"smis_ksr_bayar" );
	$dbtable ->setShowAll(true)
			 ->setFetchMethode ( DBTable::$ARRAY_FETCH )
			 ->addCustomKriteria ( "noreg_pasien","='" . $_POST ['noreg_pasien'] . "'" )
			 ->addCustomKriteria ( "metode","='asuransi'" );
	
	$dbres = new PembayaranResponder ( $dbtable,$uitable,$adapter );
	$dbres ->setMetodePembayaran("asuransi")
		   ->setDuplicate(false,"");
    $dbres ->setAutonomous(getSettings($db,"smis_autonomous_id",""));
    if(getSettings($db,"cashier-simple-kwitansi-use-own-number-separated","0")=="1"){
        $prefix = getSettings($db,"cashier-simple-kwitansi-use-own-number-prefix-asuransi","");
        $dbres  ->setPrefix($prefix);
    }
	if($dbres->isSave()){
		global $user;
		$dbres->addColumnFixValue("operator",$user->getNameOnly());
        
        /* berfungsi untuk melakukan service 
         * ke kasir agar datanya di cache untuk tagihan */
        require_once "kasir/snippet/update_total_tagihan.php";
	}
	$data = $dbres->command ( $_POST ['command'] );
	echo json_encode ( $data );
	return;
}
require_once ("smis-base/smis-include-service-consumer.php");
$noreg		= $_POST ['noreg_pasien'];
$responder 	= new ServiceResponder ( $db,$uitable,$adapter,"get_registered" );
$responder  ->addData("id",$noreg)
			->addData("command","edit");
$data 		= $responder->command ("edit");
$px			= $data['content'];

$uitable ->addModal ("id","hidden","","" )
		 ->addModal ("nama_pasien","hidden","",$_POST ['nama_pasien'] )
		 ->addModal ("noreg_pasien","hidden","",$_POST ['noreg_pasien'] )
		 ->addModal ("nrm_pasien","hidden","",$_POST ['nrm_pasien'] )
		 ->addModal ("id_asuransi","hidden","",$px['asuransi'] )
		 ->addModal ("nama_asuransi","hidden","",$px['nama_asuransi'])
		 ->addModal ("id_perusahaan","hidden","",$px['nama_perusahaan'] )
		 ->addModal ("nama_perusahaan","hidden","",$px['n_perusahaan'])
		 ->addModal ("waktu_masuk","hidden","",$px['tanggal'])
		 ->addModal ("waktu","datetime","Tanggal",date("Y-m-d H:i:s") )
		 ->addModal ("nilai","money","Nilai","" )
		 ->addModal ("terklaim","checkbox","Terbayar","0" )
		 ->addModal ("no_bukti","text","No. Bukti","" )
		 ->addModal ("keterangan","textarea","Keterangan","" )
		 ->addModal ("tgl_klaim","date","Tgl Klaim","0" );

$modal = $uitable->getModal ();
$modal ->setTitle ("Pembayaran Asuransi");
echo addJS  ("framework/bootstrap/js/bootstrap-datepicker.js");
echo addCSS ("framework/bootstrap/css/datepicker.css");
echo addJS  ("framework/bootstrap/js/bootstrap-datetimepicker.js");
echo addCSS ("framework/bootstrap/css/bootstrap-datetimepicker.css");
echo addJS  ("kasir/resource/js/asuransi.js",false);
echo $uitable ->getHtml ();
echo $modal	  ->getHtml ();

?>
