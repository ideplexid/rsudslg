<?php
	$noreg_pasien=$_POST['noreg_pasien'];
	$tb =  "SELECT SUM(nilai) as total 
            FROM smis_ksr_bayar WHERE 
            noreg_pasien='".$noreg_pasien."'  
            AND prop!='del'";   
         
	$tt =  "SELECT SUM(total) as total 
            FROM smis_ksr_kolektif 
            WHERE noreg_pasien='".$noreg_pasien."' 
            AND akunting_only=0 
            AND prop!='del'";
        
	$total_bayar = $db->get_var($tb);
	$total_tagih = $db->get_var($tt);
	$sisa        = $total_tagih-$total_bayar;
    
    loadLibrary("smis-libs-function-math");
    $round = getSettings($db,"smis-simple-rounding-uang","-1");
    $model = getSettings($db,"smis-simple-rounding-model","round");
    $sisa  = smis_money_round($sisa,$round,$model);
    
	$resp           = array();
	$resp['bayar']  = $total_bayar;
	$resp['tagih']  = $total_tagih;
	$resp['sisa']   = $sisa;
	
	$response = new ResponsePackage();
	$response ->setAlertVisible(false);
	$response ->setContent($resp);
	$response ->setStatus(ResponsePackage::$STATUS_OK);
	echo json_encode($response->getPackage());
?>