<?php
global $db;
global $user;
require_once "kasir/function/get_patient_by_noreg.php";
require_once ("smis-base/smis-include-service-consumer.php");
/** TODO SYNCH TGL PULANG */
/*MENGAMBIL DATA PASIEN di REGISTRASI*/
require_once "kasir/function/get_patient_by_noreg.php";
require_once "kasir/function/get_system_setup.php";
	$_noreg          = $_POST ['noreg_pasien'];
	$px              = get_patient_by_noreg($db,$_noreg);
	$_setup          = get_system_setup($db,$user,$_noreg,$px,"");

	$_NAMA_PASIEN	 =	$px['nama_pasien']." / ".$px['namapenanggungjawab'];
	$_ALAMAT_PASIEN	 =	$px['alamat_pasien'];
	$_NRMNORG_PASIEN =	ArrayAdapter::format("only-digit8",$px['nrm'])." / ".ArrayAdapter::format("only-digit8",$px['id']);
	$_TGL_MASUK		 =	ArrayAdapter::format("date d M Y H:i",$_setup['tgl_masuk']);
	$_TGL_KELUAR	 =	ArrayAdapter::format("date d M Y H:i",$_setup['tgl_pulang']);
	$_NOMOR_KWITANSI =  $_setup['no_kwitansi'];
	$_TGL_KWITANSI	 = 	$_setup['waktu_kwitansi'];
	$_NILAI_BAYAR	 =	$_setup['nilai_kwitansi'];

	$_INACBGs		=	$px['inacbg_bpjs'];
	$_DESC_INACBGs	=	$px['deskripsi_bpjs'];
	$_PLAFON_BPJS	=	$px['plafon_bpjs'];
	$_PLAFON_NAIK	=	$px['plafon_naik_kelas'];
	$_NAIK_KELAS	=	$px['naik_kelas_bpjs'];
	$_TOTAL_q 		=	"SELECT sum(nilai) as total FROM smis_ksr_bayar WHERE noreg_pasien='".$_noreg."' AND prop!='del'";
	$_TOTAL_BAYAR	= 	$db->get_var($_TOTAL_q);
	$_TOTAL_TAGIHAN =   $px['total_paket'];
/*END OF MENGAMBIL DATA PASIEN DI REGISTRASI DI SIMPAN DI $PX*/


/*LAST POSITION PASIEN*/
	$responder = new ServiceConsumer ( $db, "get_last_position",$params = array("noreg_pasien"	=> $_POST['noreg_pasien']),"medical_record" );
	$responder->execute();
	$_LAST_POSITION=ArrayAdapter::format("unslug",$responder->getContent());
/*END - LAST POSITION PASIEN*/

/*MEMBUAT TOMBOL CETAK*/
	$print=new Button("print_button_simple_kwitansi", "", " Cetak ");
	$print->addClass("btn btn-primary");
	$print->setIcon(" fa fa-print ");
	$print->setIsButton(BUtton::$ICONIC_TEXT);
	$print->setAction("kwitansi_paket_print()");
	$print->setAction("kwitansi_print('kwitansi_paket')");	
	if( $px['uri']=="0" && getSettings($db,"cashier-simple-kwitansi-cek-print-rj","0")=="1" || $px['uri']=="1" && getSettings($db,"cashier-simple-kwitansi-cek-print-ri","0")=="1"){
		$print->setAction("cek_kwitansi_print('kwitansi_paket')");
	}	
	$save=new Button("print_button_simple_kwitansi", "", " Simpan ");
	$save->addClass("btn btn-primary");
	$save->setIcon(" fa fa-save ");
	$save->setIsButton(BUtton::$ICONIC_TEXT);
	$save->setAction("kwitansi_paket_update_pembayaran()");
	
	$price=new Text("kwitansi_paket_harga","",$_TOTAL_TAGIHAN);
	$price->setModel(Text::$MONEY);
	$code=new Text("kwitansi_paket_kode_inacbg","",$_INACBGs);
	$code->addAtribute("placeholder","Code Inacbg");
	$desc=new Text("kwitansi_paket_desc_inacbg","",$_DESC_INACBGs);	
	$desc->addAtribute("placeholder","Description Inacbg");
	
	$inputgrup=new InputGroup("","");
	$inputgrup->addComponent($price);
	$inputgrup->addComponent($code);
	$inputgrup->addComponent($desc);
	$inputgrup->addComponent($save);
	$inputgrup->addComponent($print);	
/*AKHIR DARI MEMBUAT TOMBOL CETAK*/


/*MENGAMBIL DATA RUMAH SAKIT*/
	$autonomous=getSettings($db, "smis_autonomous_name", "SMIS");
	$nama_rs=getSettings($db, "cashier-simple-kwitansi-rs", $autonomous);
	$autonomous_address=getSettings($db, "smis_autonomous_address", "LOCALHOST");
	$alamat_rs=getSettings($db, "cashier-simple-kwitansi-address", $autonomous_address);
/*END - MENGAMBIL DATA RUMAH SAKIT*/


/* START - MENAMPILKAN HEADER */
	$table=new TablePrint("kwitansi_paket");
	$table->setMaxWidth(true);
    $table->setTableClass("kwitansi_print");
    
	$table->addColumn($nama_rs."</br>".$alamat_rs, 6, 1,NULL,NULL,"center bold");
	$table->commit("body");

	$table->addSpace(3,1);
	$table->addColumn("No. Kwitansi : ".$_NOMOR_KWITANSI);
	$table->commit("body");

	$table->addColumn("NAMA / PJ",1,1);
	$table->addColumn($_NAMA_PASIEN,2,1);
	$table->addColumn("TANGGAL MASUK",1,1,null,null,"bold");
	$table->commit("body");

	$table->addColumn("ALAMAT",1,1);
	$table->addColumn($_ALAMAT_PASIEN,2,1);
	$table->addColumn($_TGL_MASUK,1,1);
	$table->commit("body");

	$table->addColumn("NRM / NO. REG",1,1);
	$table->addColumn($_NRMNORG_PASIEN,2,1);
	$table->addColumn("TANGGAL KELUAR",1,1,null,null,"bold");
	$table->commit("body");

	$table->addSpace(3,1);
	$table->addColumn($_TGL_KELUAR);
	$table->commit("body");

	$table->addSpace(4,1);
	$table->commit("body");

	$table->addColumn("Tanggal",1,1);
	$table->addColumn(" : ".$_TGL_KWITANSI);
	$table->commit("body");

	$table->addColumn("INA-CBG ",1,1);
	$table->addColumn(" : ".$_INACBGs);
	$table->commit("body");

	$table->addColumn("Deskripsi ",1,1);
	$table->addColumn(" : ".$_DESC_INACBGs);
	$table->commit("body");

	$table->addColumn("KETERANGAN",1,1,null,null,"bold upperscore");
	$table->addColumn("RUANGAN",1,1,null,null,"bold upperscore");
	$table->addColumn("KELAS",1,1,null,null,"bold upperscore");
	$table->addColumn("TOTAL",1,1,null,null,"bold upperscore");
	$table->commit("body");
/* END - MENAMPILKAN HEADER */

/* START - MENAMPILKAN DATA TAGIHAN YANG HARUS DIBAYAR PASIEN */
	$_TANGGUNGAN=$_TOTAL_TAGIHAN - $_TOTAL_BAYAR;
	$table->addColumn("Biaya Paket",1,1);
	$table->addColumn($_LAST_POSITION,1,1);
	$table->addColumn("-",1,1);
	$table->addColumn(ArrayAdapter::format("money Rp.",$_TOTAL_TAGIHAN),1,1);
	$table->commit("body");
/* END - MENAMPILKAN DATA TAGIHAN YANG HARUS DIBAYAR PASIEN */

/* START - MENAMPILKAN DATA FOOTER */
	$table->addSpace("4","1");
	$table->commit("body");

	$table->addColumn("PEMBAYARAN",3,1);
	$table->addColumn(ArrayAdapter::format("money Rp. ", $_NILAI_BAYAR),1,1);
	$table->commit("body");
	loadLibrary("smis-libs-function-math");

	$table->addColumn("TERBILANG : ",1,1,null,null,"upperscore underscore bold");
	$table->addColumn(strtoupper(numbertell($_NILAI_BAYAR)." rupiah"),3,1,null,null,"upperscore  underscore bold");
	$table->commit("body");

	if($_TOTAL_BAYAR*1<$_TANGGUNGAN){
		$HUTANG=$_TANGGUNGAN - $_TOTAL_BAYAR;
		$table->addColumn("SISA PEMBAYARAN : ",3,1);
		$table->addColumn(ArrayAdapter::format("money Rp. ",$HUTANG),1,1);
		$table->commit("body");
	}
	$table->addSpace(4, 1);
	$table->commit("body");
/* END - MENAMPILKAN DATA FOOTER */
	
/*START - KODE TANDA TANGAN*/
	global $user;
	$kota=getSettings($db, "cashier-simple-kwitansi-town", "");
	$table->addSpace(3,1);
	$table->addColumn( $kota.", ".ArrayAdapter::format("date d M Y", date("Y-M-d")),1,1,NULL,NULL,"center");
	$table->commit("body");
	$table->addColumn("</br></br>", 4, 1,NULL,NULL,"center");
	$table->commit("body");

	$table->addSpace(3, 1);
	$table->addColumn($user->getNameOnly(), 1, 1,NULL,NULL,"center");
	$table->commit("body");
/*END - KODE TANDA TANGAN*/

echo $inputgrup->getHtml();
echo "<div id='cetak_kwitansi_paket_area'>".$table->getHtml()."</div>";
echo addCSS("kasir/resource/css/kwitansi_print.css",false);
echo addJS("kasir/resource/js/kwitansi_print.js",false);
echo addJS("kasir/resource/js/kwitansi_paket.js",false);
?>