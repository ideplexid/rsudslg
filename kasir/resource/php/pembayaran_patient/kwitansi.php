<?php
global $db;
require_once("smis-base/smis-include-service-consumer.php");
require_once("kasir/class/resource/Resume.php");
require_once("kasir/class/MapRuangan.php");

$header     = array ();
$adapter    = new SimpleAdapter ();
$uitable    = new Table ( $header );
$noreg      = $_POST ['noreg_pasien'];
$nrm        = $_POST['nrm_pasien'];
$nama       = $_POST['nama_pasien'];
$title      = getSettings($db, "smis_autonomous_title", "");
$subtitle   = getSettings($db, "smis_autonomous_sub_title", "");
$address    = getSettings($db, "smis_autonomous_address", "");
$abrev      = getSettings($db, "smis-rs-abrevation", "RS");
$query      = "SELECT ruangan, sum(nilai) as total, sum(if(jaspel=1,nilai,0)) as kena_jaspel FROM smis_ksr_kolektif WHERE prop!='del' AND akunting_only='0' AND noreg_pasien='".$noreg."' AND nrm_pasien='".$nrm."' GROUP BY ruangan";
$nokwi      = strtoupper ( $abrev."-".date( 'dmy-his' ) . "-" . substr ( md5 ( $nama.$noreg.date('dmyhis') ), 1, 4 ) );
$list       = $db->get_result($query);

$table  = new TablePrint("");
$table  ->setMaxWidth(false)
        ->addColumn ( $subtitle . "<br/>" . $title . "<br/>" . $address, 2, 1,"title","","center big bold uppercase fade_to_grey" )
        ->addColumn("No. Kwitansi", "1", "1",NULL,NULL,"bold")
        ->addColumn($nokwi, "1", "1",NULL,"nokwi")
        ->commit("body");
$table  ->addColumn("Nama", "1", "1",NULL,NULL,"bold")
        ->addColumn($nama, "1", "1")
        ->commit("body");
$table  ->addColumn("NRM", "1", "1",NULL,NULL,"bold")
        ->addColumn(ArrayAdapter::format("only-digit8", $nrm), "1", "1")
        ->commit("body");
$table  ->addColumn("No. Registrasi", "1", "1",NULL,NULL,"bold")
        ->addColumn(ArrayAdapter::format("only-digit8", $noreg), "1", "1")
        ->commit("body");
$table  ->addColumn("Biaya", "2", "1","body",NULL,"bold center fade_to_grey");
$kena_jaspel = 0;
$total       = 0;
foreach($list as $x){
	$kena_jaspel    += $x->kena_jaspel;
	$total          += $x->total;	
	$table  ->addColumn(MapRuangan::getRealName($x->ruangan), "1", "1")
            ->addColumn(ArrayAdapter::format("money Rp.", $x->total), "1", "1")
            ->commit("body");
}
loadLibrary("smis-libs-function-math");
$round          = getSettings($db,"smis-simple-rounding-uang","-1");
$model          = getSettings($db,"smis-simple-rounding-model","round");
$total          = smis_money_round($total,$round,$model);
$persen         = getSettings($db, "cashier-activate-jaspel","1")=="1"?getSettings($db, "cashier-jaspel-persen","10"):"0";
$inap           = isset($_POST['uri'])?$_POST['uri']:$_POST['inap'];
$jaspel         = $persen*$kena_jaspel*$inap/100;
if($kena_jaspel>0 && $persen>0){
	$table  ->addColumn(MapRuangan::getRealName($x->ruangan), "1", "1")
            ->addColumn(ArrayAdapter::format("money Rp.", $jaspel), "1", "1")
            ->commit("body");
}
$table  ->addColumn("Sub Total", "1", "1",NULL,NULL,"italic top_dash")
        ->addColumn(ArrayAdapter::format("money Rp.", $jaspel+$total), "1", "1",NULL,NULL,"italic top_dash")
        ->commit("body");

$query          = "SELECT SUM(nilai) as total from smis_ksr_bayar WHERE noreg_pasien='" . $noreg . "' AND prop!='del' AND metode='diskon'";
$total_diskon   = $db->get_var ( $query )*1;
if($total_diskon>0){
	$table  ->addColumn("Potongan", "1", "1",NULL,NULL,"italic top_dash")
            ->addColumn(ArrayAdapter::format("money Rp.", $total_diskon), "1", "1",NULL,NULL,"italic top_dash")
            ->commit("body");
	$table  ->addColumn("Sub Total - Potongan", "1", "1",NULL,NULL,"italic top_dash")
            ->addColumn(ArrayAdapter::format("money Rp.", $jaspel+$total-$total_diskon), "1", "1",NULL,NULL,"italic top_dash")
            ->commit("body");
}

$query          = "SELECT SUM(nilai) as total from smis_ksr_bayar WHERE noreg_pasien='" . $noreg . "' AND prop!='del' AND metode!='diskon'";
$total_dibayar  = $db->get_var ( $query );
$table          ->addColumn("Total Dibayar", "1", "1",NULL,NULL,"italic top_dash")
                ->addColumn(ArrayAdapter::format("money Rp.", $total_dibayar), "1", "1",NULL,NULL,"italic top_dash")
                ->commit("body");
$sisa_bayar     = $total + $jaspel - $total_dibayar - $total_diskon;
$table          ->addColumn("Sisa Bayar", "1", "1",NULL,NULL,"bold top_dash")
                ->addColumn($sisa_bayar>0?ArrayAdapter::format("money Rp.", $sisa_bayar):"Lunas", "1", "1",NULL,NULL,"bold top_dash")
                ->commit("body");
                
global $user;
$lokasi = getSettings($db, "smis-rs-footer", "");
$table  ->addColumn ( $lokasi.", ".ArrayAdapter::format("date d M Y", date("y-m-d")), 10, 1,"footer","","center fade_to_grey" )
        ->addColumn ( "</br></br>", 10, 1,"footer","","center fade_to_grey" )
        ->addColumn ( $user->getNameOnly(), 10, 1,"footer","","center fade_to_grey" );
echo "<div id='kwitansi'>".$table->getHtml()."</div>";