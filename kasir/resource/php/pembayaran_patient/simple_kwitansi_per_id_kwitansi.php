<?php
/*
dipakai jika biaya pamabyaran 
tiap-tiap tindakan sesuai dengan id kwitansi tertentu
*/

require_once ("smis-base/smis-include-service-consumer.php");

global $db;
global $user;

/*MEMBUAT TOMBOL CETAK*/
$btn=new Button("", "", " Cetak ");
$btn->addClass("btn btn-primary");
$btn->setIcon(" fa fa-print ");
$btn->setIsButton(BUtton::$ICONIC_TEXT);
$btn->setAction("simple_kwitansi_print()");
echo $btn->getHtml();
/*AKHIR DARI MEMBUAT TOMBOL CETAK*/

/*MENGAMBIL DATA PASIEN di REGISTRASI*/
$header=array ();
$adapter = new SimpleAdapter ();
$uitable = new Table ( $header );
$noreg=$_POST ['noreg_pasien'];
$responder = new ServiceResponder ( $db, $uitable, $adapter, "get_registered" );
$responder->addData("id", $noreg);
$responder->addData("command", "edit");
$data = $responder->command ( "edit" );
$px=$data['content'];
/*END OF MENGAMBIL DATA PASIEN DI REGISTRASI DI SIMPAN DI $PX*/

/*MENGAMBIL DATA TAGIHAN*/
$tampilan_ruangan=getSettings($db, "cashier-simple-kwitansi-place", "0")=="1"?"ruangan_kasir":"ruangan";
$dbtable=new DBTable($db,"smis_ksr_kolektif");
$dbtable->setShowAll(true);
$dbtable->setFetchMethode(DBTable::$ARRAY_FETCH);
$dbtable->setOrder(" urutan ASC, nama_grup ASC , ruangan ASC, dari ASC, sampai ASC ");
$dbtable->addCustomKriteria("noreg_pasien","='".$noreg."'");
$dbtable->addCustomKriteria("akunting_only","=0");

if(getSettings($db, "cashier-simple-kwitansi-per-kwitansi", "0")=="1"){
	$dbtable->addCustomKriteria("id_kwitansi","='".$_POST['id_kwitansi']."'");
}

$dt=$dbtable->view("","0");
$list=$dt['data'];
/*MENGAMBIL DATA TAGIHAN PASIEN DISIMPAN DI $LIST*/

/*MENGAMBIL DATA KOTA, CETAK TANGGAL, PENCETAK ,NAMA RS DAN ALAMAT RS*/
$kota=getSettings($db, "cashier-simple-kwitansi-town", "");
$pencetak=$user->getNameOnly();
$autonomous=getSettings($db, "smis_autonomous_name", "SMIS");
$nama_rs=getSettings($db, "cashier-simple-kwitansi-rs", $autonomous);
$autonomous_address=getSettings($db, "smis_autonomous_address", "LOCALHOST");
$alamat_rs=getSettings($db, "cashier-simple-kwitansi-address", $autonomous_address);
$tgl_pulang=($px['tanggal_pulang']=="0000-00-00 00:00:00" || $px['tanggal_pulang']=="0000-00-00") ?date("Y-m-d H:i"):$px['tanggal_pulang'];
$tgl_masuk= $px['tanggal'];
$kelamin=$px['kelamin']=="1"?"Perempuan":"Laki-Laki";
$last_noreg_q="SELECT id FROM smis_ksr_bayar WHERE noreg_pasien='".$noreg."' AND prop!='del' ORDER BY id DESC;";
$last_noreg=$db->get_var($last_noreg_q);
/*END MENGAMBIL DATA KOTA, CETAK TANGGAL, PENCETAK ,NAMA RS DAN ALAMAT RS*/


/*PEMBUATAN OBJECT CETAK MENGGUNAKAN TABLE PRINT*/
$tp=new TablePrint("cetak_kwitansi");
$tp->setTableClass("kwitansi_print");
$tp->setDefaultBootrapClass(false);
$tp->setMaxWidth(false);
$tp->setTableClass("cetak_kwitansi");
//pembuatan title
$tp->addColumn($nama_rs."</br>".$alamat_rs, 6, 1,NULL,NULL,"center bold");
$tp->commit("body");
//pembuatan front-header
$tp->addColumn("NAMA / PJ", 1, 1,NULL,NULL,"left ltop");
$tp->addColumn(" : ".$px['nama_pasien']." / ".$px['namapenanggungjawab']." / ".$kelamin, 2, 1,NULL,NULL,"left ltop ");
$tp->addColumn("No. Kwitansi", 2, 2,NULL,NULL,"left ltop f20 underlined");
$tp->addColumn( " : ".ArrayAdapter::format("only-digit8",$last_noreg), 1, 2,NULL,NULL,"left ltop f20 bold");
$tp->commit("body");

$tp->addColumn("ALAMAT", 1, 1,NULL,NULL,"left");
$tp->addColumn(" : ".$px['alamat_pasien'], 2, 1,NULL,NULL,"left ");
$tp->commit("body");

$tp->addColumn("NRM / NO. REG", 1, 1,NULL,NULL,"left");
$tp->addColumn(" : ".ArrayAdapter::format("only-digit8", $px['nrm'])." / ".ArrayAdapter::format("only-digit8", $px['id']), 2, 1,NULL,NULL,"left");
$tp->addColumn("TANGGAL MASUK", 2, 1,NULL,NULL,"");
$tp->addColumn( " <div class='ngedit' contenteditable='true'> ".ArrayAdapter::format("date d M Y", $tgl_masuk)."</div>", 1, 1,NULL,NULL,"left");
$tp->commit("body");

$tp->addColumn("JENIS PASIEN", 1, 1,NULL,NULL,"left lbottom");
$tp->addColumn(" : ".ArrayAdapter::format("unslug", $px['carabayar']), 2, 1,NULL,NULL,"left lbottom");
$tp->addColumn("TANGGAL KELUAR", 2, 1,NULL,NULL,"left lbottom");
$tp->addColumn( "<div class='ngedit' contenteditable='true'>".ArrayAdapter::format("date d M Y", $tgl_pulang)."</div>", 1, 1,NULL,NULL,"left lbottom");
$tp->commit("body");

$tp->addSpace(6, 1);
$tp->commit("body");

//pembuatan header content
$tp->addColumn("RUANGAN", 1, 1,NULL,NULL,"bold");
$tp->addColumn("TANGGAL", 1, 1,NULL,NULL,"bold");
$tp->addColumn("TINDAKAN", 1, 1,NULL,NULL,"bold");
$tp->addColumn("HRG. SAT", 1, 1,NULL,NULL,"bold");
$tp->addColumn("JML", 1, 1,NULL,NULL,"bold");
$tp->addColumn("SUBTOTAL", 1, 1,NULL,NULL,"bold");
$tp->commit("body");
//pembuatan content
$TOTAL_TAGIHAN=0;
foreach($list as $x){
	$tp->addColumn("&#09;".ArrayAdapter::format("unslug", $x[$tampilan_ruangan]), 1, 1);
	$tp->addColumn( $x['tanggal'], 1, 1);
	$tp->addColumn(ArrayAdapter::format("unslug", $x['nama_tagihan']), 1, 1);
	$tp->addColumn(ArrayAdapter::format("money Rp.", $x['total']/$x['quantity']), 1, 1);
	$tp->addColumn(" x ". $x['quantity'], 1, 1);
	$tp->addColumn(ArrayAdapter::format("money Rp.", $x['total']), 1, 1);
	$tp->commit("body");
	$TOTAL_TAGIHAN+=$x['total'];
}

//pembuatan footer
loadLibrary("smis-libs-function-math");
$total_say=numbertell($TOTAL_TAGIHAN);
$tp->addColumn("TOTAL TAGIHAN", 5, 1,NULL,NULL,"bold");
$tp->addColumn(ArrayAdapter::format("money Rp.",$TOTAL_TAGIHAN), 1, 1,NULL,NULL,"bold  ltop");
$tp->commit("body");
//footer total tagihan

$tp->addColumn("&nbsp;",6, 1,NULL,NULL,"");
$tp->commit("body");
$tp->addColumn("PEMBAYARAN",6, 1,NULL,NULL,"bold");
$tp->commit("body");

//pembuatan pembayaran
$dbtable=new DBTable($db, "smis_ksr_bayar");
$dbtable->setShowAll(true);
$dbtable->setFetchMethode(DBTable::$ARRAY_FETCH);
$urutan=getSettings($db, "cashier-simple-kwitansi-arrange-bayar", "1")=="1";

if($urutan) $dbtable->setOrder(" id DESC"); // terakhir diatas
else $dbtable->setOrder(" id ASC");// trakhir di bawah

$dbtable->addCustomKriteria("noreg_pasien","='".$noreg."'");
$dt=$dbtable->view("","0");
$list=$dt['data'];
$TOTAL_DIBAYAR=0;
$BAYAR_AKHIR=0;
$total=count($list);
$no=1;


foreach($list as $x){
	$TOTAL_DIBAYAR+=$x['nilai'];
	if($BAYAR_AKHIR==0 && $urutan){// pertama kali loop, terkahir diatas
		$BAYAR_AKHIR=$x['nilai'];
		$tp->addColumn ( " <strong>**</strong> &#09; [ ".ArrayAdapter::format ( "only-digit8", $x['id'])." ] - ".$x['metode']." - ".$x['keterangan'], 5, 1,NULL,NULL,"" );
		$tp->addColumn ( ArrayAdapter::format ( "money Rp.", $x['nilai']),1, 1,NULL,NULL,"bold" );
	}else if($no==$total && !$urutan){//terakhir kali loop, terakhir dibawah
		$BAYAR_AKHIR=$x['nilai'];
		$tp->addColumn ( " <strong>**</strong> &#09; [ ".ArrayAdapter::format ( "only-digit8", $x['id'])." ] - ".$x['metode']." - ".$x['keterangan'], 5, 1,NULL,NULL,"" );
		$tp->addColumn ( ArrayAdapter::format ( "money Rp.", $x['nilai']),1, 1,NULL,NULL,"bold" );
	}else{
		$tp->addColumn ( "  &#09; [ ".ArrayAdapter::format ( "only-digit8", $x['id'])." ] - ".$x['metode']." - ".$x['keterangan'], 5, 1 );
		$tp->addColumn ( ArrayAdapter::format ( "money Rp.", $x['nilai']),1, 1 );
	}	
	$tp->commit("body");
	$no++;
}

if(getSettings($db, "cashier-simple-kwitansi-sum-bayar", "1")=="1"){
	$tp->addColumn("TOTAL PEMBAYARAN", 5, 1,NULL,NULL,"bold ");
	$tp->addColumn(ArrayAdapter::format("money Rp.",$TOTAL_DIBAYAR), 1, 1,NULL,NULL,"bold  ltop");
	$tp->commit("body");
}

$tp->addColumn("&nbsp;",6, 1,NULL,NULL,"lbottom");
$tp->commit("body");


/*HERE COMMAND*/
$total_say=numbertell($BAYAR_AKHIR);

$tp->addColumn("TERBILANG", 1, 1,NULL,NULL,"left  lbottom");
$tp->addColumn(": ".$total_say." Rupiah", 5, 1,NULL,NULL,"left lbottom bold");
$tp->commit("body");
$tp->addSpace(3, 1);
$tp->addColumn(" <div class='ngedit' contenteditable='true'>".$kota.", ".ArrayAdapter::format("date d M Y", date("Y-M-d"))." </div>", 3, 1,NULL,NULL,"center");
$tp->commit("body");
$tp->addSpace(3, 1);
$tp->addColumn("</br></br>", 3, 1,NULL,NULL,"center");
$tp->commit("body");
$tp->addSpace(3, 1);
$tp->addColumn($pencetak, 3, 1,NULL,NULL,"center");
$tp->commit("body");
/*END PEMBUATAN OBJECT CETAK*/
echo "<div id='cetak_simple_kwitansi_area'>".$tp->getHtml()."</div>";
echo addCSS("kasir/css/kwitansi_print.css",false);
$font_size=getSettings($db, "cashier-simple-kwitansi-font-size", "12px");
$font_size_f20=getSettings($db, "cashier-simple-kwitansi-font-size-f20", "12px");
?>

<script type="text/javascript">
function simple_kwitansi_print(){
	$(".ngedit").prop('contenteditable', false );
	var html=$("#cetak_simple_kwitansi_area").html();
	var name=$("#noreg_pasien").val();
	$(".ngedit").prop('contenteditable', true );	
	smis_print_save(html, "simple_kwitansi", name);
}
</script>
<style type="text/css">
	table#cetak_kwitansi tr td.f20{font-size:<?php echo $font_size_f20; ?> !important;}
	table#cetak_kwitansi tr td, table#cetak_kwitansi tr th{font-size:<?php echo $font_size; ?> !important;}	
</style>
