<?php
global $user;
$table = new TablePrint("kwitansi_inacbg");
$table ->setMaxWidth(true);
$table ->setDefaultBootrapClass(false);

require_once "kasir/function/get_patient_by_noreg.php";
require_once "kasir/function/get_system_setup.php";
$noreg           = $_POST ['noreg_pasien'];
$px              = get_patient_by_noreg($db,$noreg);
$_setup          = get_system_setup($db,$user,$noreg,$px,"");

$table  ->addColumn("",1,1)
        ->addColumn("No.RM",1,1)
        ->addColumn(": ".$px['nrm'],3,1)
        ->addColumn("Tanggal Masuk",1,1)
        ->addColumn(": ".ArrayAdapter::format("date d M Y", $_setup['tgl_masuk']),3,1)
        ->commit("header");

$table  ->addColumn("",1,1)
        ->addColumn("Nama",1,1)
        ->addColumn(": ".$px['nama_pasien'],3,1)
        ->addColumn("Tanggal Keluar",1,1)
        ->addColumn(": ".ArrayAdapter::format("date d M Y", $_setup['tgl_pulang']),3,1)
        ->commit("header");

$ruangan_intensif = getSettings($db,"cashier-kwitansi-inacbg-intensif","");
$list_intensif =  explode(",",$ruangan_intensif);
$r_intensif = array();
foreach($list_intensif as $x){
    if(trim($x)!==""){
        $r_intensif[] = "'".trim($x)."'";
    }
}
$kriteria_intensif = implode(",",$r_intensif);


$table ->addSpace(5,1);
$table ->commit("header");
$prosedure_non_medis    = $db->get_var("SELECT SUM(total) FROM smis_ksr_kolektif WHERE noreg_pasien = '".$_POST['noreg_pasien']."' AND prop !='del' AND (jenis_tagihan='tindakan_dokter' OR jenis_tagihan='medical_checkup')");
$prosedure_bedah        = $db->get_var("SELECT SUM(total) FROM smis_ksr_kolektif WHERE noreg_pasien = '".$_POST['noreg_pasien']."' AND prop !='del' AND jenis_tagihan='ok'");
$konsultasi             = $db->get_var("SELECT SUM(total) FROM smis_ksr_kolektif WHERE noreg_pasien = '".$_POST['noreg_pasien']."' AND prop !='del' AND ( jenis_tagihan='visite_dokter' AND nama_tagihan LIKE 'Visite dr%' OR jenis_tagihan='konsul_dokter' AND nama_tagihan LIKE 'Konsul dr%' OR jenis_tagihan='konsultasi_dokter') ");
$tenaga_ahli            = $db->get_var("SELECT SUM(total) FROM smis_ksr_kolektif WHERE noreg_pasien = '".$_POST['noreg_pasien']."' AND prop !='del' AND ( jenis_tagihan='visite_dokter' AND nama_tagihan NOT LIKE 'Visite dr%' OR jenis_tagihan='konsul_dokter' AND nama_tagihan NOT LIKE 'Konsul dr%' OR jenis_tagihan='asuhan_farmasi' OR jenis_tagihan='asuhan_gizi'  ) ");
$keperawatan            = $db->get_var("SELECT SUM(total) FROM smis_ksr_kolektif WHERE noreg_pasien = '".$_POST['noreg_pasien']."' AND prop !='del' AND jenis_tagihan='tindakan_perawat' ");
$penunjang              = $db->get_var("SELECT SUM(total) FROM smis_ksr_kolektif WHERE noreg_pasien = '".$_POST['noreg_pasien']."' AND prop !='del' AND (jenis_tagihan='hemodialisa') ");
$patology              = $db->get_var("SELECT SUM(total) FROM smis_ksr_kolektif WHERE noreg_pasien = '".$_POST['noreg_pasien']."' AND prop !='del' AND (jenis_tagihan='patology') ");

$radiology              = $db->get_var("SELECT SUM(total) FROM smis_ksr_kolektif WHERE noreg_pasien = '".$_POST['noreg_pasien']."' AND prop !='del' AND jenis_tagihan='radiology' ");
$laboratory             = $db->get_var("SELECT SUM(total) FROM smis_ksr_kolektif WHERE noreg_pasien = '".$_POST['noreg_pasien']."' AND prop !='del' AND jenis_tagihan='laboratory' ");
$darah                  = $db->get_var("SELECT SUM(total) FROM smis_ksr_kolektif WHERE noreg_pasien = '".$_POST['noreg_pasien']."' AND prop !='del' AND jenis_tagihan='bank_darah' ");

$rehabilitasi           = $db->get_var("SELECT SUM(total) FROM smis_ksr_kolektif WHERE noreg_pasien = '".$_POST['noreg_pasien']."' AND prop !='del' AND jenis_tagihan='fisiotherapy' ");
$kamar                  = $db->get_var("SELECT SUM(total) FROM smis_ksr_kolektif WHERE noreg_pasien = '".$_POST['noreg_pasien']."' AND prop !='del' AND (jenis_tagihan='kamar_mayat' OR jenis_tagihan='administrasi' OR jenis_tagihan='registration'  OR (jenis_tagihan='bed' AND ruangan NOT IN (".$kriteria_intensif .") ) ) ");
$intensif               = $db->get_var("SELECT SUM(total) FROM smis_ksr_kolektif WHERE noreg_pasien = '".$_POST['noreg_pasien']."' AND prop !='del' AND jenis_tagihan='bed' AND ruangan IN (".$kriteria_intensif .")  ");


$obat                   = 0;
$kemoterapy             = 0;
$kronis                 = 0;


$dbtable = new DBTable($db,"smis_ksr_obat_resume_pasien");
$xdata = $dbtable ->select(array("noreg_pasien"=>$noreg));
$result = array();
if($xdata==null || $px['tutup_tagihan']=="0"){
    $serv = new ServiceConsumer($db,"get_resume_obat_pasien",array("noreg_pasien"=>$noreg));
    $serv ->setMode(ServiceConsumer::$CLEAN_BOTH_JOIN_SINGLE_ARRAY);
    $serv ->execute();    
    $result = $serv ->getContent();

    //insert ulang untuk data lama
    $x = array(
        "noreg_pasien"=>$noreg
    );
    $uptd = array(
        "noreg_pasien"=>$noreg,
        "detail_obat"=>json_encode($result)
    );
    $dbtable = new DBTable($db,"smis_ksr_obat_resume_pasien");
    $dbtable ->insertOrUpdate($uptd,$x);
}else{
    $result = json_decode($xdata->detail_obat,true);
}

foreach($result as $x){
    $obat        += $x['obat'];
    $kronis      += $x['obat_kronis'];
    $kemoterapy  += $x['obat_kemoterapi'];
}



$alkes                  = $db->get_var("SELECT SUM(total) FROM smis_ksr_kolektif WHERE noreg_pasien = '".$_POST['noreg_pasien']."' AND prop !='del' AND jenis_tagihan='alok' ");
$bmhp                   = $db->get_var("SELECT SUM(total) FROM smis_ksr_kolektif WHERE noreg_pasien = '".$_POST['noreg_pasien']."' AND prop !='del' AND (jenis_tagihan='oksigen_central' OR jenis_tagihan='oksigen_manual') ");
$alat                   = $db->get_var("SELECT SUM(total) FROM smis_ksr_kolektif WHERE noreg_pasien = '".$_POST['noreg_pasien']."' AND prop !='del' AND jenis_tagihan='elektromedis' ");

$total                   = $prosedure_non_medis+$prosedure_bedah+$konsultasi+$tenaga_ahli+$keperawatan+$penunjang+$radiology+$laboratory+$darah;
$total                  += $rehabilitasi+$kamar+$intensif+$obat+$kemoterapy+$kronis+$alkes+$bmhp+$alat+$patology;

$table  ->addColumn("No.",1,1)
        ->addColumn("Uraian",1,1)
        ->addColumn("Jumlah",1,1)
        ->addColumn("No.",1,1)
        ->addColumn("Uraian",1,1)
        ->addColumn("Jumlah",1,1)
        ->addColumn("No.",1,1)
        ->addColumn("Uraian",1,1)
        ->addColumn("Jumlah",1,1)
        ->commit("body");

$table  ->addColumn("1.",1,1)
        ->addColumn("Prosedur Non Bedah",1,1)
        ->addColumn(ArrayAdapter::format("money Rp.",$prosedure_non_medis),1,1)
        ->addColumn("2.",1,1)
        ->addColumn("Prosedur Bedah",1,1)
        ->addColumn(ArrayAdapter::format("money Rp.",$prosedure_bedah),1,1)
        ->addColumn("3.",1,1)
        ->addColumn("Konsultasi",1,1)
        ->addColumn(ArrayAdapter::format("money Rp.",$konsultasi),1,1)
        ->commit("body");

$table  ->addColumn("4.",1,1)
        ->addColumn("Tenaga Ahli",1,1)
        ->addColumn(ArrayAdapter::format("money Rp.",$tenaga_ahli),1,1)
        ->addColumn("5.",1,1)
        ->addColumn("Keperawatan",1,1)
        ->addColumn(ArrayAdapter::format("money Rp.",$keperawatan),1,1)
        ->addColumn("6.",1,1)
        ->addColumn("Penunjang",1,1)
        ->addColumn(ArrayAdapter::format("money Rp.",$penunjang),1,1)
        ->commit("body");

$table  ->addColumn("7.",1,1)
        ->addColumn("Radiology",1,1)
        ->addColumn(ArrayAdapter::format("money Rp.",$radiology),1,1)
        ->addColumn("8.",1,1)
        ->addColumn("Laboratory",1,1)
        ->addColumn(ArrayAdapter::format("money Rp.",$laboratory),1,1)
        ->addColumn("9.",1,1)
        ->addColumn("Pelayanan Darah",1,1)
        ->addColumn(ArrayAdapter::format("money Rp.",$darah),1,1)
        ->commit("body");

$table  ->addColumn("10.",1,1)
        ->addColumn("Rehabilitasi",1,1)
        ->addColumn(ArrayAdapter::format("money Rp.",$rehabilitasi),1,1)
        ->addColumn("11.",1,1)
        ->addColumn("Kamar dan Akomodasi",1,1)
        ->addColumn(ArrayAdapter::format("money Rp.",$kamar),1,1)
        ->addColumn("12.",1,1)
        ->addColumn("Rawat Intensif",1,1)
        ->addColumn(ArrayAdapter::format("money Rp.",$intensif),1,1)
        ->commit("body");

$table  ->addColumn("13.",1,1)
        ->addColumn("Obat",1,1)
        ->addColumn(ArrayAdapter::format("money Rp.",$obat),1,1)
        ->addColumn("14.",1,1)
        ->addColumn("Obat Kemoterapi",1,1)
        ->addColumn(ArrayAdapter::format("money Rp.",$kemoterapy),1,1)
        ->addColumn("15.",1,1)
        ->addColumn("Obat Kronis",1,1)
        ->addColumn(ArrayAdapter::format("money Rp.",$kronis),1,1)
        ->commit("body");

$table  ->addColumn("16.",1,1)
        ->addColumn("Alkes",1,1)
        ->addColumn(ArrayAdapter::format("money Rp.",$alkes),1,1)
        ->addColumn("17.",1,1)
        ->addColumn("BMHP",1,1)
        ->addColumn(ArrayAdapter::format("money Rp.",$bmhp),1,1)
        ->addColumn("18.",1,1)
        ->addColumn("Alat Medis",1,1)
        ->addColumn(ArrayAdapter::format("money Rp.",$alat),1,1)
        ->commit("body");


$table  ->addColumn("19.",1,1)
        ->addColumn("Patology",1,1)
        ->addColumn(ArrayAdapter::format("money Rp.",$patology),1,1)
        ->addColumn("",1,1)
        ->addColumn("",1,1)
        ->addColumn("",1,1)
        ->addColumn("",1,1)
        ->addColumn("",1,1)
        ->addColumn("",1,1)
        ->commit("body");

$table  ->addColumn("Total (1 s/d 19)",8,1)
        ->addColumn(ArrayAdapter::format("money Rp.",$total),1,1)
        ->commit("body");

require_once "kasir/function/kwitansi_print_button.php";
require_once "kasir/function/end_css_kwitansi.php";
echo kwitansi_print_button($db,"kwitansi_inacbg",$_px);

echo "</br></br></br>";
echo "<div id='cetak_kwitansi_inacbg_area'>".$table->getHtml()."</div>";
echo addCSS("kasir/resource/css/kwitansi_print.css",false);
echo addCSS("kasir/resource/css/kwitansi_inacbg.css",false);
echo addJS("kasir/resource/js/kwitansi_print.js",false);
echo end_css_kwitansi($db);