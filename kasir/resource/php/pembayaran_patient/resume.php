<?php
global $db;
require_once ("smis-base/smis-include-service-consumer.php");
require_once ("kasir/class/resource/Resume.php");
loadLibrary("smis-libs-function-math");
$round   = getSettings($db,"smis-simple-rounding-uang","-1");
$model   = getSettings($db,"smis-simple-rounding-model","round");
$header  = array();
$adapter = new SimpleAdapter ();
$uitable = new Table ( $header );
$noreg   = $_POST ['noreg_pasien'];
$nrm     = $_POST['nrm_pasien'];
$query   = "SELECT sum(nilai) as total, sum(if(jaspel=1,nilai,0)) as kena_jaspel 
            FROM smis_ksr_kolektif 
            WHERE prop != 'del' 
            AND akunting_only = 0
            AND noreg_pasien = '".$noreg."' 
            AND nrm_pasien = '".$nrm."'";
$r       = $db->get_row($query);
$total   = smis_money_round($r->total,$round,$model);
$jaspel  = $r->kena_jaspel;
$nama    = $_POST['nama_pasien'];
$inap    = isset($_POST['uri'])?$_POST['uri']:$_POST['inap'];
$persen  = getSettings($db, "cashier-activate-jaspel","1")=="1"?getSettings($db, "cashier-jaspel-persen","10"):"0";

/** proses pembuatan diskon yang diajukan dari ruangan */
require_once "kasir/function/invoke_pengajuan_diskon.php";
invoke_pengajuan_diskon($noreg,$total);

/** mengambil data pulang dan waktu pulang terakhir */
require_once "kasir/function/get_last_validate_keluar.php";
$keluar  = get_last_validate_keluar($noreg);

$serv = new ServiceConsumer($db,"get_gratis",array("noreg_pasien"=>$noreg),"registration");
$serv ->execute();
$serv ->setMode(ServiceConsumer::$SINGLE_MODE);
$result = $serv ->getContent();

/** data resume untuk pasien */
$resume  = new Resume($total, $jaspel, $noreg, $nama, $nrm, $inap,$persen,$keluar['waktu_keluar'],$keluar['cara_keluar'],$result["alasan_gratis"],$result['opsi_gratis']);
echo $resume->getHtml();

?>