<?php
global $db;
require_once "smis-base/smis-include-duplicate.php";
require_once 'kasir/class/table/TableAsuransi.php';
$header=array ("ID","No. Kwitansi",'Tanggal','Nilai',"Rekanan",'No Bukti' ,"Terbayar");
$uitable = new TableAsuransi ( $header, "Pembayaran Rekanan", NULL, true );
$uitable->setPrintElementButtonEnable(true);
$uitable->setName ( "rekanan" );
$uitable->setFooterVisible(false);
$uitable->setDelButtonEnable( !(getSettings($db, "cashier-hide-del-asuransi", "0")=="1") );

if (isset ( $_POST ['command'] )) {
	require_once "kasir/class/responder/PembayaranResponder.php";
	$adapter = new SummaryAdapter();
	$adapter->addSummary("Nilai", "nilai","money Rp.");
	$adapter->addFixValue("Tanggal", "<strong>Total</strong>");
	$adapter->add ( "ID", "id", "only-digit8" );
	$adapter->add ( "No. Kwitansi", "no_kwitansi" );
	$adapter->add ( "Tanggal", "waktu", "date d M Y H:i" );
	$adapter->add ( "Nilai", "nilai", "money Rp." );
	$adapter->add ( "Asuransi", "nama_asuransi" );
	$adapter->add ( "No Bukti", "no_bukti" );
	$adapter->add ( "Keterangan", "keterangan" );
	$adapter->add ( "Terbayar", "terklaim","trivial_1_Ya_Belum" );
	
	$dbtable = new DBTable ( $db, "smis_ksr_bayar" );
	$dbtable->setShowAll(true);
	$dbtable->setFetchMethode ( DBTable::$ARRAY_FETCH );
	$dbtable->addCustomKriteria ( "noreg_pasien", "='" . $_POST ['noreg_pasien'] . "'" );
	$dbtable->addCustomKriteria ( "metode", "='rekanan'" );
	
	$dbres = new PembayaranResponder ( $dbtable, $uitable, $adapter );
    $dbres->setMetodePembayaran("rekanan"); 
    $dbres->setDuplicate(false,"");
    $dbres->setAutonomous(getSettings($db,"smis_autonomous_id",""));
    if(getSettings($db,"cashier-simple-kwitansi-use-own-number-separated","0")=="1"){
        $prefix=getSettings($db,"cashier-simple-kwitansi-use-own-number-prefix-asuransi","");
        $dbres->setPrefix($prefix);
    }
	if($dbres->isSave()){
		global $user;
		$dbres->addColumnFixValue("operator", $user->getNameOnly());
        
        /* berfungsi untuk melakukan service 
         * ke kasir agar datanya di cache untuk tagihan */
        require_once "kasir/snippet/update_total_tagihan.php";
	}
	$data = $dbres->command ( $_POST ['command'] );
	echo json_encode ( $data );
	return;
}
require_once ("smis-base/smis-include-service-consumer.php");
$noreg=$_POST ['noreg_pasien'];
$responder = new ServiceResponder ( $db, $uitable, $adapter, "get_registered" );
$responder->addData("id", $noreg);
$responder->addData("command", "edit");
$data = $responder->command ( "edit" );
$px=$data['content'];

$uitable->addModal ( "id", "hidden", "", "" );
$uitable->addModal ( "nama_pasien", "hidden", "", $_POST ['nama_pasien'] );
$uitable->addModal ( "noreg_pasien", "hidden", "", $_POST ['noreg_pasien'] );
$uitable->addModal ( "nrm_pasien", "hidden", "", $_POST ['nrm_pasien'] );
$uitable->addModal ( "id_asuransi", "hidden", "", $px['asuransi'] );
$uitable->addModal ( "nama_asuransi", "hidden", "", $px['nama_asuransi']);
$uitable->addModal ( "id_perusahaan", "hidden", "", $px['nama_perusahaan'] );
$uitable->addModal ( "nama_perusahaan", "hidden", "", $px['n_perusahaan']);
$uitable->addModal ( "waktu", "datetime", "Tanggal", date ( "Y-m-d" ) );
$uitable->addModal ( "nilai", "money", "Nilai", "" );
$uitable->addModal ( "terklaim", "checkbox", "Terbayar", "0" );
$uitable->addModal ( "no_bukti", "text", "No. Bukti", "" );
$uitable->addModal ( "keterangan", "textarea", "Keterangan", "" );
$uitable->addModal ( "tgl_klaim", "date", "Tgl Klaim", "0" );

$modal = $uitable->getModal ();
$modal->setTitle ( "Pembayaran Asuransi" );
echo addJS ( "framework/bootstrap/js/bootstrap-datepicker.js" );
echo addCSS ( "framework/bootstrap/css/datepicker.css" );
echo addJS ( "framework/bootstrap/js/bootstrap-datetimepicker.js" );
echo addCSS ( "framework/bootstrap/css/bootstrap-datetimepicker.css" );
echo addJS ( "kasir/resource/js/rekanan.js",false );
echo $uitable->getHtml ();
echo $modal->getHtml ();

?>
