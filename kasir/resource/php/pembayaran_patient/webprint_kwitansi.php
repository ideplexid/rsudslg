<?php
	global $db;
	global $user;
	
	loadLibrary('smis-libs-function-math');
	
	$noreg 	= $_POST['noreg_pasien'];
	$nrm	= $_POST['nrm_pasien'];
	
	$type = array (
		'registration' => 'admin',
		'administrasi' => 'admin',
		'radiology' => 'penunjang',
		'fisiotherapy' => 'penunjang',
		'laboratory' => 'penunjang',
		'ambulan' => 'rawat',
		'gizi' => 'rawat',
		'tindakan_perawat' => 'rawat',
		'tindakan_perawat_igd' => 'rawat',
		'tindakan_igd' => 'rawat',
		'tindakan_dokter' => 'dokter',
		'konsul_dokter' => 'dokter',
		'visite' => 'dokter',
		'konsultasi_dokter' => 'dokter',
		'bed' => 'ruang',
		'penjualan_resep' => 'farmasi',
		'return_resep' => 'farmasi',
		'vk' => 'rawat',
		'ok' => 'operasi',
		'rr' => 'ruang',
		'alok' => 'farmasi',
		'oksigen_central' => 'rawat',
		'oksigen_manual' => 'rawat',
// 		'audiometry' => 'penunjang',
// 		'bronchoscopy' => 'penunjang',
// 		'faal_paru' => 'penunjang',
// 		'spirometry' => 'penunjang',
// 		'endoscopy' => 'penunjang',
// 		'ekg' => 'penunjang',
 		'darah' => 'penunjang',
	);
	
	$query="SELECT jenis_tagihan, sum(nilai) AS total, nama_dokter
			FROM smis_ksr_kolektif
			WHERE prop!='del' 
            AND noreg_pasien='$noreg' 
            AND akunting_only='0' 
            AND nrm_pasien='$nrm'
			GROUP BY jenis_tagihan";
	$list = $db->get_result($query);
	
	foreach($list as $row){
		$total += $row->total;	
		$biaya[$type[$row->jenis_tagihan]] += $row->total;
		
		if ($row->jenis_tagihan === 'registration') {
			$dokter = $row->nama_dokter;
		}
	}
				
	$label = array(
		'admin' => 'Pendaftaran dan Administrasi',
		'ruang' => 'Sewa Ruang Perawatan',
		'dokter' => 'Layanan Dokter',
		'rawat' => 'Layanan Keperawatan',
		'penunjang' => 'Layanan Penunjang',
		'farmasi' => 'Layanan Farmasi',
		'operasi' => 'Layanan Operasi'
	);
		
	foreach ($label as $k => $v){
		if (!isset($biaya[$k]))
			$biaya[$k] = 0;
		
		$biayaDetail[] = array(
			'label' => $v,
			'value' => number_format($biaya[$k], 0, ',', '.'),
		);
	}	
	
	$query = "SELECT nilai, metode
		FROM smis_ksr_bayar
		WHERE noreg_pasien = '$noreg' AND prop!='del'";
	$list = $db->get_result($query);
	
	foreach ($list as $row){
		if ($row->metode === 'diskon') {
			$diskon += $row->nilai;
		} else {
			$bayar = $row->nilai;
			$totalBayar += $bayar;						
		}
	}
	
	$terbayar = $totalBayar - $bayar;		
	$sisa = $total - $diskon - $terbayar;
	
	$tanggalInap = ArrayAdapter::format("date d/m/Y", $_POST['tanggal_inap']);
	if ($_POST['tanggal_pulang'] !== '0000-00-00') {
		$tanggalInap .= ' - ' . ArrayAdapter::format("date d/m/Y", $_POST['tanggal_pulang']); 
	}
	
	echo json_encode(array(
		'template' => json_decode(file_get_contents("kasir/webprint/kwitansi_detail.json"), true),
		'data' => array(
			'kode_dok' => $_POST['rawat_inap'] === '1' ? 'RI' : 'RJ',
			'noreg_pasien' => ArrayAdapter::format("only-digit8", $noreg),
			'nrm_pasien' => ArrayAdapter::format("only-digit6", $nrm),
			'nama_pasien' => ArrayAdapter::format("unslug", $_POST['nama_pasien']),
			'layanan' => ArrayAdapter::format("unslug", $_POST['layanan']),
			'jenis_pasien' => ArrayAdapter::format("unslug", $_POST['jenis_pasien']),
			'tanggal' => ArrayAdapter::format("date d-m-Y", date("y-m-d")),
			'kasir' => ArrayAdapter::format("unslug", $user->getNameOnly()),	
			'alamat_pasien'=> ArrayAdapter::format("unslug", $_POST['alamat_pasien']),
			'penanggung_jawab'=> ArrayAdapter::format("unslug", $_POST['penanggung_jawab']),
			'perusahaan'=> $_POST['perusahaan'],
			'dokter'=> ArrayAdapter::format("unslug", $dokter),
			'tanggal_inap'=> $tanggalInap,
			'biaya'=> number_format($total, 0, ',', '.'),
			'plafond'=> '0',
			'diskon'=> number_format($diskon, 0, ',', '.'),
			'terbayar'=> number_format($terbayar, 0, ',', '.'),
			'kekurangan'=> number_format($sisa, 0, ',', '.'),
			'bayar'=> number_format($bayar, 0, ',', '.'),
			'terbilang' => numbertell($bayar) . " Rupiah",
			'biaya_detail' => $biayaDetail
		)
	));	
?>