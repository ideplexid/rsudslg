<?php
	loadLibrary('smis-libs-function-math');
	global $db;
	global $user;
	
	$company_name = ArrayAdapter::format("unslug", getSettings($this->db, "smis_autonomous_title", ""));
	$company_address = ArrayAdapter::format("unslug", getSettings($this->db, "smis_autonomous_address", ""));
	
	$kode_dokumen = $_POST['rawat_inap'] === "1" ? "RI" : "RJ";
	$noreg_pasien = ArrayAdapter::format("only-digit6", $_POST['noreg_pasien']);
	$nrm_pasien = ArrayAdapter::format("only-digit6", $_POST['nrm_pasien']);
	$jenis_pasien = ArrayAdapter::format("unslug", $_POST['jenis_pasien']);
	$nama_pasien = strtoupper($_POST['nama_pasien']);
	$alamat_pasien = strtoupper($_POST['alamat_pasien']);
	$perusahaan = strtoupper($_POST['perusahaan']);
	$asuransi = strtoupper($_POST['asuransi']);
	$penanggung_jawab = strtoupper($_POST['penanggung_jawab']);
	$tanggal_inap = ArrayAdapter::format("date d-m-Y", $_POST['tanggal_inap']);
	$tanggal_pulang = $_POST['tanggal_pulang'];
	if ($tanggal_pulang != "0000-00-00") {
		$tanggal_inap .= " s/d " . ArrayAdapter::format("date d-m-Y", $tanggal_inap);
	} else {
		$tanggal_inap .= " *";
	}
	
	$dbtable = new DBTable($db, "smis_ksr_kolektif");
	$detail_tagihan = $dbtable->get_result("
		SELECT ruangan, tanggal, nama_tagihan, jenis_tagihan, quantity, nilai, keterangan
		FROM smis_ksr_kolektif
		WHERE noreg_pasien = '" . $noreg_pasien . "'
        AND prop!='del'
        AND akunting_only=0
		ORDER BY 
			CASE
				WHEN ruangan = 'registration' THEN 1
				WHEN ruangan = 'kasir' THEN 3
				ELSE 2
			END ASC,
			jenis_tagihan ASC,
			tanggal ASC
	");
	
	$biaya_detail = array();
	$nomor = 1;
	$total = 0;
	foreach ($detail_tagihan as $dt) {
		$ruangan = $dt->ruangan;
		$tanggal = $dt->tanggal;
		$nama_tagihan = $dt->nama_tagihan;
		$jumlah = $dt->quantity;
		$harga = $dt->nilai;
		$subtotal = $jumlah * $harga;
		$biaya_detail[] = array(
			'nomor'			=> number_format($nomor++, 0, ",", "."),
			'ruangan' 		=> ArrayAdapter::format("unslug", $ruangan),
			'tanggal' 		=> ArrayAdapter::format("date d-m-Y", $tanggal),
			'nama_tagihan'	=> ArrayAdapter::format("unslug", $nama_tagihan),
			'jumlah'		=> number_format($jumlah, 0, ",", "."),
			'harga'			=> number_format($harga, 2, ",", "."),
			'total'			=> number_format($subtotal, 2, ",", ".")
		);
		$total += $subtotal;
	}
	$terbilang = strtoupper(numbertell($total)) . "RUPIAH";
	$total = number_format($total, 2, ",", ".");
	$jumlah_item = $nomor - 1;
	global $user;
	$operator = $user->getNameOnly();
	$lokasi = strtoupper(getSettings($db, "smis-rs-footer", ""));
	echo json_encode(array(
		'template'	=> json_decode(file_get_contents("kasir/webprint/kwitansi_global.json"), true),
		'data'		=> array(
			'company_name'		=> $company_name,
			'company_address'	=> $company_address,
			'kode_dokumen'		=> $kode_dokumen,
			'noreg_pasien'		=> $noreg_pasien,
			'nrm_pasien'		=> $nrm_pasien,
			'jenis_pasien'		=> $jenis_pasien,
			'nama_pasien'		=> $nama_pasien,
			'alamat_pasien'		=> $alamat_pasien,
			'perusahaan'		=> $perusahaan,
			'asuransi'			=> $asuransi,
			'penanggung_jawab'	=> $penanggung_jawab,
			'tanggal_inap'		=> $tanggal_inap,
			'total'				=> $total,
			'terbilang'			=> $terbilang,
			'biaya_detail'		=> $biaya_detail,
			'operator'			=> $operator,
			'tanggal_cetak'		=> date("d-m-Y"),
			'lokasi'			=> $lokasi
		)
	));
?>