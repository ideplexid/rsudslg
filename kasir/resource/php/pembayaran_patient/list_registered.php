<?php
global $db;
global $CHANGE_COOKIE;
$CHANGE_COOKIE = false;

if(isset ( $_POST ['super_command'] ) &&  $_POST ['super_command']!=""){
		if(file_exists("kasir/snippet/".$_POST ['super_command'].".php")){
			/*all function in folder snippet,collect_tagihan_unit, collect_tagihan,pasien_pulang,aktifkan_ulang */
			require_once "kasir/snippet/".$_POST ['super_command'].".php";
		}else if(file_exists("kasir/resource/php/pembayaran_patient/".$_POST ['super_command'].".php")){
			/*	bed,cash,bank,asuransi,
			 * 	diskon,tagihan,resume,kwitansi,
			 * 	print_tagihan_bayangan,edit_bayangan,webprint_kwitansi,
			 * 	webprint_tagihan_global,rincian_kwitansi,
			 * 	tagihan_backup_input,simple_kwitansi,simple_kwitansi_escp 
			 *  kwitansi_paket*/
			require_once "kasir/resource/php/pembayaran_patient/".$_POST ['super_command'].".php";
		}else if(file_exists("kasir/resource/php/tambahan_biaya_pasien/".$_POST ['super_command'].".php")){
			/*tagihan_backup and periksa_dokter*/
			require_once "kasir/resource/php/tambahan_biaya_pasien/".$_POST ['super_command'].".php";
		}
		return;
}

/**
 * digunakan untuk menampilkan data pasien yang 
 * sesuai dengan ruanganya. rawat inap atau rawat jalan
 */
$header=array ("Tanggal",'Nama',"P/L",'NRM',"No Reg","Alamat" ,"Jenis","Ayah","Ibu","Suami","Istri","Carabayar","Plafon");
$uitable = new Table ( $header );
$uitable->setName ( "list_registered" );
$uitable->setModel ( Table::$SELECT );
if (isset ( $_POST ['command'] )) {
	require_once ("smis-base/smis-include-service-consumer.php");
	$adapter = new SimpleAdapter ();
	$adapter->add ( "Nama", "nama_pasien" );
	$adapter->add ( "NRM", "nrm", "digit8" );
	$adapter->add ( "No Reg", "id", "digit8" );
	$adapter->add ( "Alamat", "alamat_pasien" );
	$adapter->add ( "Tanggal", "tanggal","date d M Y" );
	$adapter->add ( "Ayah", "ayah" );
	$adapter->add ( "Ibu", "ibu" );
	$adapter->add ( "Suami", "suami" );
    $adapter->add ( "Carabayar", "carabayar","unslug" );
	$adapter->add ( "Plafon", "plafon_bpjs","money Rp." );
	$adapter->add ( "P/L", "kelamin","trivial_0_L_P" );
	$adapter->add ( "Istri", "istri" );
	$adapter->add ( "Jenis", "uri", "trivial_0_Rawat Jalan_Rawat Inap" );
	$service = "get_registered";
	if(isset($_POST['is_profnumber'])){
		$service = "get_registered_by_profile_number";
	}
	$responder = new ServiceResponder ( $db, $uitable, $adapter, $service );
	
	if(isset($_POST['uri']))
		$responder->addData("uri", $_POST['uri']);
	$data = $responder->command ( $_POST ['command'] );
	echo json_encode ( $data );
	return;
}
echo $uitable->getHtml ();

?>

<script type="text/javascript">
$(document).ready(function(){
	list_registered.view();
});
</script>