<?php
global $db;
require_once "smis-base/smis-include-duplicate.php";
require_once "kasir/class/table/TableDiskon.php";
$uitable = new TableDiskon ( array ('Tanggal','Nilai','Keterangan' ), "Diskon", NULL, true );
$uitable->setName ( "diskon" );
$uitable->setFooterVisible(false);

if (isset ( $_POST ['command'] )) {
    require_once "kasir/class/responder/PembayaranResponder.php";
	$adapter = new SummaryAdapter();
	$adapter->addSummary("Nilai", "nilai","money Rp.");
	$adapter->addFixValue("Tanggal", "<strong>Total</strong>");
	$adapter->add ( "Tanggal", "waktu", "date d M Y H:i" );
	$adapter->add ( "Nilai", "nilai", "money Rp." );
	$adapter->add ( "Keterangan", "keterangan" );
    $adapter->add ( "id_pengajuan_diskon", "id_pengajuan_diskon" );
	$dbtable = new DBTable ( $db, "smis_ksr_bayar" );
	$dbtable->setFetchMethode ( DBTable::$ARRAY_FETCH );
	$dbtable->setShowAll(true);
	
	$dbtable->addCustomKriteria ( "noreg_pasien", "='" . $_POST ['noreg_pasien'] . "'" );
	$dbtable->addCustomKriteria ( "metode", "='diskon'" );
	
	$dbres = new PembayaranResponder ( $dbtable, $uitable, $adapter );
	$dbres->setDuplicate(false,"");
    $dbres->setMetodePembayaran("diskon");
    $dbres->setAutonomous(getSettings($db,"smis_autonomous_id",""));
    if(getSettings($db,"cashier-simple-kwitansi-use-own-number-separated","0")=="1"){
        $prefix=getSettings($db,"cashier-simple-kwitansi-use-own-number-prefix-diskon","");
        $dbres->setPrefix($prefix);
    }
    if($dbres->isSave()){
		global $user;
		$dbres->addColumnFixValue("operator", $user->getNameOnly());
        /* berfungsi untuk melakukan service 
         * ke kasir agar datanya di cache untuk tagihan*/
        require_once "kasir/snippet/update_total_tagihan.php";
	}
	$data = $dbres->command ( $_POST ['command'] );
	echo json_encode ( $data );
	return;
}

$uitable->addModal ( "id", "hidden", "", "" );
$uitable->addModal ( "nama_pasien", "hidden", "", $_POST ['nama_pasien'] );
$uitable->addModal ( "noreg_pasien", "hidden", "", $_POST ['noreg_pasien'] );
$uitable->addModal ( "nrm_pasien", "hidden", "", $_POST ['nrm_pasien'] );
$uitable->addModal ( "waktu", "datetime", "Tanggal", date ( "Y-m-d H:i" ) );
$uitable->addModal ( "keterangan", "textarea", "Keterangan", "" );
$uitable->addModal ( "nilai", "money", "Nilai", "" );
$modal = $uitable->getModal ();
$modal->setTitle ( "Diskon" );

echo $uitable->getHtml ();
echo $modal->getHtml ();
echo addJS ( "framework/bootstrap/js/bootstrap-datetimepicker.js" );
echo addCSS ( "framework/bootstrap/css/bootstrap-datetimepicker.css" );
echo addJS ( "kasir/resource/js/diskon.js",false );
?>