<?php
global $db;
require_once 'smis-base/smis-include-service-consumer.php';
require_once 'smis-libs-class/ServiceProviderList.php';

/* GRUP SUPER COMMAND */
if(isset($_POST['super_command'])){
    $super=new SuperCommand();
    if($_POST['super_command']=="tarif_alok"){
        $head=array ("Nama","Satuan","Harga","Kategori" );
		$dktable = new Table ( $head, "", NULL, true );
		$dktable->setName ( "tarif_alok" );
		$dktable->setModel ( Table::$SELECT );
		$dkadapter = new SimpleAdapter ();
		$dkadapter->add ( "Nama", "nama" );
		$dkadapter->add ( "Satuan", "satuan" );
		$dkadapter->add ( "Harga", "harga", "money Rp." );
		$dkadapter->add ( "Kategori", "kategori" );
		$tarif = new ServiceResponder ( $db, $dktable, $dkadapter, "get_alok_provider",$_POST['ruangan'] );
		$super->addResponder ( "tarif_alok", $tarif);
    }
    
    $data=$super->initialize();
    if($data!=null){
        echo $data;
        return;
    }
}
/* END - GRUP SUPER COMMAND */


$header=array ('Tanggal','Nama',"Harga",'Jumlah' );
$uitable = new Table ( $header, "", NULL, true );
$uitable->setName ( "edit_alok" );

if (isset ( $_POST ['command'] )) {
	$adapter = new SimpleAdapter ();
    $adapter->add ( "Tanggal", "tanggal", "date d M Y" );
    $adapter->add ( "Nama", "nama" );
    $adapter->add ( "Satuan", "satuan" );
    $adapter->add ( "Jumlah", "jumlah", "number" );
    $adapter->add ( "Harga", "harga", "money Rp." );
    
	$dbres = new ServiceResponder($db, $uitable, $adapter, "edit_alok",$_POST["ruangan"]);
	$data = $dbres->command ( $_POST ['command'] );
	echo json_encode ( $data );
	return;
}

$noreg_pasien="";
if( isset($_POST['noreg']) && $_POST['noreg']!="" || isset($_POST['noreg_pasien']) && $_POST['noreg_pasien']!="" ){
    $noreg_pasien=isset($_POST['noreg'])?$_POST['noreg']:$_POST['noreg_pasien'];
}

$df_ruangan="";
$harga=0;
if(isset($_POST['ruangan']) && $_POST['ruangan']!=""){
    $df_ruangan=$_POST['ruangan'];
    /*service untuk mendapatkan harga alok*/
    $alok_harga_service = new ServiceConsumer ( $db, "get_alok", array ("ruangan" => $df_ruangan), "manajemen" );
    $alok_harga_service->execute ();
    $harga = $alok_harga_service->getContent ();
}

$service=new ServiceProviderList($db,"edit_alok",$df_ruangan);
$service->execute();
$ruangan=$service->getContent();

$uitable->addModal("nama_pasien",  "hidden","", "","",NULL,true);
$uitable->addModal("nrm_pasien", "hidden", "", "","",NULL,true);
$uitable->addModal("noreg_pasien", "hidden","",  $noreg_pasien,"",NULL,true);
$uitable->addModal("carabayar", "hidden","",  "","",NULL,true);
$uitable->addModal("ruang", "select", "Ruangan", $ruangan);
$form=$uitable->getModal()->setTitle("Pasien")->getForm();
$uitable->clearContent();

$uitable->addModal("id", "hidden", "", "","n");
$uitable->addModal ( "tanggal", "date", "Tanggal", date ( "Y-m-d" ) ,"n");
$uitable->addModal ( "nama", "chooser-edit_alok-tarif_alok-Daftar Alat Obat", "Obat", "","n",null,false,null,true );
$uitable->addModal ( "satuan", "hidden", "", "" );
$uitable->addModal ( "kode", "hidden", "", "" );
$uitable->addModal ( "harga", "money", "Harga", "", 'n', null, false,null,false,"jumlah" );
$uitable->addModal ( "jumlah", "text", "Jumlah", "1", 'n', "numeric",false,null,false,"save"  );

echo $uitable->getModal()->setTitle("Alat & Obat Kesehatan")->getHtml();
echo $form ->getHtml();
echo $uitable->getHtml ();
echo addJS ( "framework/bootstrap/js/bootstrap-datetimepicker.js" );
echo addCSS ( "framework/bootstrap/css/bootstrap-datetimepicker.css" );
echo addJS ( "framework/smis/js/table_action.js" );
echo addJS ( "kasir/resource/js/edit_alok.js",false );

?>
