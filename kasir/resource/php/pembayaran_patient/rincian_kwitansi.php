<?php
	global $db;
	global $user;
	
	require_once("smis-libs-out/php-excel/PHPExcel.php");
	require_once("smis-libs-out/php-excel/PHPExcel/IOFactory.php");
	loadLibrary('smis-libs-function-math');
		
	$noreg 	= $_POST['noreg_pasien'];
	$nrm	= $_POST['nrm_pasien'];
	
	$type = array (
		'registration' => 'admin',
		'administrasi' => 'admin',
		'radiology' => 'penunjang',
		'fisiotherapy' => 'penunjang',
		'laboratory' => 'penunjang',
		'ambulan' => 'rawat',
		'gizi' => 'rawat',
		'tindakan_perawat' => 'rawat',
		'tindakan_perawat_igd' => 'rawat',
		'tindakan_igd' => 'rawat',
		'tindakan_dokter' => 'dokter',
		'konsul_dokter' => 'dokter',
		'visite' => 'dokter',
		'konsultasi_dokter' => 'dokter',
		'bed' => 'ruang',
		'penjualan_resep' => 'farmasi',
		'return_resep' => 'farmasi',
		'vk' => 'rawat',
		'ok' => 'operasi',
		'rr' => 'ruang',
		'alok' => 'farmasi',
		'oksigen_central' => 'rawat',
		'oksigen_manual' => 'rawat',
 		'darah' => 'penunjang',
	);
	
	$query="SELECT jenis_tagihan, nama_tagihan, SUM(quantity) AS qty, nilai, SUM(nilai) AS subtotal, ruangan, keterangan, nama_dokter
		FROM smis_ksr_kolektif
		WHERE prop!='del' 
        AND noreg_pasien='$noreg' 
        AND nrm_pasien='$nrm'
		AND akunting_only=0
        GROUP BY nama_tagihan
		ORDER BY jenis_tagihan ASC, ruangan DESC";
	$list = $db->get_result($query);
	
	$jasa_pengirim_ok = 0;
	$sisipkan_asisten_operator_2 = false;
	$hak_asisten_operator_2 = 0;
	$sisipkan_pranata_anestesi = false;
	$sisipkan_instrument = false;
	$sisipkan_oomloop = false;
	$detail = array();
    $total=0;
    $subtotal=0;
    
	foreach($list as $row){
		$k = $type[$row->jenis_tagihan];
		
		$total += $row->subtotal;
		$subtotal[$k] += $row->subtotal;
						
		if ($row->jenis_tagihan === 'registration') {
			$dokter = $row->nama_dokter;
		}
		if ($row->jenis_tagihan === 'alok') {
			$detail[$k][$row->ruangan]['nama'] = 'Bahan dan Alkes/ ALOK';			
			$detail[$k][$row->ruangan]['total'] += $row->subtotal;
			$detail[$k][$row->ruangan]['data'][] = $row;
		} else if ($row->jenis_tagihan === "ok" && $row->ruangan === "kasir") {
			$jasa_pengirim_ok += $row->subtotal;
		} else if ($row->jenis_tagihan === "ok" && $row->ruangan !== "kasir" && strpos($row->nama_tagihan, "Asisten Operator II") !== false && $jasa_pengirim_ok > 0) {
			$row->nilai = strval($row->nilai + 0.4 * $jasa_pengirim_ok);
			$row->subtotal = strval($row->subtotal + 0.4 * $jasa_pengirim_ok);
			$detail[$k][] = $row;
			$sisipkan_asisten_operator_2 = true;
		} else if ($row->jenis_tagihan === "ok" && $row->ruangan !== "kasir" && strpos($row->nama_tagihan, "Pranata Anastesi I") !== false && strpos($row->nama_tagihan, "Pranata Anastesi II") === false && $jasa_pengirim_ok > 0) {
			$row->nilai = strval($row->nilai + 0.3 * $jasa_pengirim_ok);
			$row->subtotal = strval($row->subtotal + 0.3 * $jasa_pengirim_ok);
			$detail[$k][] = $row;
			$sisipkan_pranata_anestesi = true;
		} else if ($row->jenis_tagihan === "ok" && $row->ruangan !== "kasir" && strpos($row->nama_tagihan, "Instrument I") !== false && strpos($row->nama_tagihan, "Instrument II") === false && $jasa_pengirim_ok > 0) {
			$row->nilai = strval($row->nilai + 0.2 * $jasa_pengirim_ok);
			$row->subtotal = strval($row->subtotal + 0.2 * $jasa_pengirim_ok);
			$detail[$k][] = $row;
			$sisipkan_instrument = true;
		} else if ($row->jenis_tagihan === "ok" && $row->ruangan !== "kasir" && strpos($row->nama_tagihan, "Oomloop I") !== false && strpos($row->nama_tagihan, "Oomloop II") === false && $jasa_pengirim_ok > 0) {
			$row->nilai = strval($row->nilai + 0.1 * $jasa_pengirim_ok);
			$row->subtotal = strval($row->subtotal + 0.1 * $jasa_pengirim_ok);
			$detail[$k][] = $row;
			$sisipkan_oomloop = true;
		} else {
			$detail[$k][] = $row;
		}
	}
	if ($jasa_pengirim_ok > 0) {
		if ($sisipkan_asisten_operator_2 === false) {
			$detail['operasi'][] = (object) array(
				"jenis_tagihan" => "ok",
				"nama_tagihan" 	=> "Asisten Operator II",
				"qty" 			=> "1",
				"nilai" 		=> strval(0.4 * $jasa_pengirim_ok),
				"subtotal" 		=> strval(0.4 * $jasa_pengirim_ok),
				"ruangan" 		=> "kamar_operasi",
				"keterangan" 	=> "",
				"nama_dokter" 	=> "",
			);
		}
		if ($sisipkan_pranata_anestesi === false) {
			$detail['operasi'][] = (object) array(
				"jenis_tagihan" => "ok",
				"nama_tagihan" 	=> "Pranata Anastesi I",
				"qty" 			=> "1",
				"nilai" 		=> strval(0.3 * $jasa_pengirim_ok),
				"subtotal" 		=> strval(0.3 * $jasa_pengirim_ok),
				"ruangan" 		=> "kamar_operasi",
				"keterangan" 	=> "",
				"nama_dokter" 	=> "",
			);
		}
		if ($sisipkan_instrument === false) {
			$detail['operasi'][] = (object) array(
				"jenis_tagihan" => "ok",
				"nama_tagihan" 	=> "Instrument I",
				"qty" 			=> "1",
				"nilai" 		=> strval(0.2 * $jasa_pengirim_ok),
				"subtotal" 		=> strval(0.2 * $jasa_pengirim_ok),
				"ruangan" 		=> "kamar_operasi",
				"keterangan" 	=> "",
				"nama_dokter" 	=> "",
			);
		}
		if ($sisipkan_oomloop === false) {
			$detail['operasi'][] = (object) array(
				"jenis_tagihan" => "ok",
				"nama_tagihan" 	=> "Oomloop I",
				"qty" 			=> "1",
				"nilai" 		=> strval(0.1 * $jasa_pengirim_ok),
				"subtotal" 		=> strval(0.1 * $jasa_pengirim_ok),
				"ruangan" 		=> "kamar_operasi",
				"keterangan" 	=> "",
				"nama_dokter" 	=> "",
			);
		}
	}
	
	$query = "SELECT nilai, metode
		FROM smis_ksr_bayar
		WHERE noreg_pasien = '$noreg' AND prop!='del'";
	$list = $db->get_result($query);
	
	foreach ($list as $row){
		if ($row->metode === 'diskon') {
			$diskon += $row->nilai;
		} else {
			$bayar = $row->nilai;
			$totalBayar += $bayar;						
		}
	}
	
	$terbayar = $totalBayar - $bayar;		
	$sisa = $total - $diskon - $terbayar;
	
	$tanggalInap = ArrayAdapter::format("date d/m/Y", $_POST['tanggal_inap']);
	if ($_POST['tanggal_pulang'] !== '0000-00-00') {
		$tanggalInap .= ' - ' . ArrayAdapter::format("date d/m/Y", $_POST['tanggal_pulang']);
	}
	
	$reader = PHPExcel_IOFactory::createReader('Excel2007');
	$excel = $reader->load("kasir/webprint/rincian.template.xlsx");
	
    $logo_file=getSettings($db,"cashier-webprint-default-logo","");
    if($logo_file==""){
        $logo_file="kasir/webprint/logo.jpg";
    }else{
        loadLibrary("smis-libs-function-string");
        $logo_file=get_fileurl($logo_file);
    }
    
	$sheet = $excel->getSheet(0);
	addLogo($sheet, "C1",$logo_file);
	$sheet->setCellValue('B10', $_POST['rawat_inap'] === '1' ? 'RI. 01' : 'RJ. 01');
	$sheet->setCellValue('C11', ArrayAdapter::format("only-digit8", $noreg).' / '.ArrayAdapter::format("only-digit6", $nrm));
	$sheet->setCellValue('C12', ArrayAdapter::format("unslug", $_POST['nama_pasien']));
	$sheet->setCellValue('C13', ArrayAdapter::format("unslug", $_POST['alamat_pasien']));
	$sheet->setCellValue('C14', ArrayAdapter::format("unslug", $_POST['penanggung_jawab']));	
	$sheet->setCellValue('E11', ArrayAdapter::format("unslug", $_POST['layanan']).' / '.ArrayAdapter::format("unslug", $_POST['jenis_pasien']));
	$sheet->setCellValue('E12', ArrayAdapter::format("unslug", $_POST['perusahaan']));
	$sheet->setCellValue('E13', ArrayAdapter::format("unslug", $dokter));
	$sheet->setCellValue('E14', $tanggalInap);
	
	$total_row_num = 40;
// 	$sheet->setCellValue('G42', $plafon);
	$sheet->setCellValue('G43', $diskon);
	$sheet->setCellValue('G44', $terbayar);
	$sheet->setCellValue('G46', $bayar);
	$sheet->setCellValue('C47', ': ' . numbertell($bayar) . " Rupiah");
	
	$plafon_start_row_num = 42;
	$diskon_start_row_num = 43;
	$terbayar_start_row_num = 44;
	$bayar_start_row_num = 46;
	$ntbayar_start_row_num = 47;
	
	$sheet->setCellValue("F49", 'Jember, ' . date('d-m-Y'));
	$sheet->setCellValue("F53", ArrayAdapter::format("unslug", $user->getNameOnly()));
	
	$detailRow = array(
		'admin' => 18,
		'ruang' => 21,
		'dokter' => 24,
		'rawat' => 27,
		'penunjang' => 30,
		'farmasi' => 33,
		'operasi' => 36
	);
	$subtotalRowNum = array(
		'admin' => 0,
		'ruang' => 0,
		'dokter' => 0,
		'rawat' => 0,
		'penunjang' => 0,
		'farmasi' => 0,
		'operasi' => 0
	);
	
	$offset = 0;
	foreach ($detailRow as $k => $v) {
		if (isset($detail[$k])) {
			$start = $v + $offset;
			$i = 0;
			foreach ($detail[$k] as $k2 => $row) {
				$i = $v + $offset;
				$offset++;
				
				$sheet->insertNewRowBefore($i + 1, 1);
				
				if (is_numeric($k2)) {
					$sheet->setCellValue('B'.$i, $row->nama_tagihan);
					$sheet->setCellValue('D'.$i, kelas($row->ruangan));
					$sheet->setCellValue('E'.$i, $row->nilai);
					$sheet->setCellValue('F'.$i, $row->qty);
					$sheet->setCellValue('G'.$i, $row->subtotal);
				} else {
					$sheet->setCellValue('B'.$i, $row['nama']);
					$sheet->setCellValue('D'.$i, kelas($k2));
					$sheet->setCellValue('E'.$i, $row['total']);
					$sheet->setCellValue('F'.$i, 1);
					$sheet->setCellValue('G'.$i, $row['total']);
				}
			}
			$sheet->setCellValue('G'.($i+2), "=SUM(G" . $start . ":G" . $i . ")");
			$subtotalRowNum[$k] = $i + 2;
		} 
		// $sheet->removeRow($v + $offset, 1);
		// $offset--;
	}
	$total_row_num += $offset;
	$total_value = "=0";
	foreach ($subtotalRowNum as $key => $val) {
		if ($val != 0)
			$total_value .= "+G" . $val;
	}
	$sheet->setCellValue("G" . $total_row_num, $total_value);
	
	//rincian penunjang
	$sheet = $excel->getSheet(1);
	addLogo($sheet, "C1", $logo_file);
	$sheet->setCellValue('G20', $subtotal['penunjang']);
	$sheet->setCellValue("F22", 'Jember, ' . date('d-m-Y'));
	$sheet->setCellValue("F26", ArrayAdapter::format("unslug", $user->getNameOnly()));
	
	$i = 18;
	foreach ($detail['penunjang'] as $row) {		
		$sheet->insertNewRowBefore($i+1, 1);
		$sheet->setCellValue('B'.$i, $row->nama_tagihan);
		$sheet->setCellValue('D'.$i, kelas($row->ruangan));
		$i++;		
				
		$ket = json_decode($row->keterangan, true);		
		foreach ($ket['periksa'] as $k => $v) {
			$sheet->insertNewRowBefore($i+1, 1);
			$sheet->setCellValue('B'.$i, '    ' . $k);
			$sheet->setCellValue('E'.$i, $v);
			$sheet->setCellValue('F'.$i, 1);
			$sheet->setCellValue('G'.$i, $v);			
			$i++;
		}
		
		$sheet->insertNewRowBefore($i+1, 1);
		$sheet->setCellValue('F'.$i, 'Jumlah');
		$sheet->setCellValue('G'.$i, $row->subtotal);
		$sheet->getStyle("F$i:G$i")->getFont()->setBold(true);
		$i++;
	}
	$sheet->removeRow($i, 1);
	
		
	//rincian farmasi
	$sheet = $excel->getSheet(2);
	addLogo($sheet, "C1", $logo_file);
	$sheet->setCellValue('H20', $subtotal['farmasi']);
	$sheet->setCellValue("G22", 'Jember, ' . date('d-m-Y'));
	$sheet->setCellValue("G26", ArrayAdapter::format("unslug", $user->getNameOnly()));	
	
	$i = 18;
// 	$total = 0;
	foreach ($detail['farmasi'] as $k => $row) {
		$sheet->insertNewRowBefore($i+1, 1);
		
		if (is_numeric($k)) {
			$sheet->setCellValue('B'.$i, $row->nama_tagihan);
			$sheet->setCellValue('D'.$i, kelas($row->ruangan));
			$i++;
			
// 			$total += $row->subtotal;
			$ket = json_decode($row->keterangan, true);
			foreach ($ket['obat'] as $v) {			
				$sheet->insertNewRowBefore($i+1, 1);
				$sheet->setCellValue('B'.$i, '    ' . $v['nama_obat']);
				$sheet->setCellValue('E'.$i, $v['harga']);
				$sheet->setCellValue('F'.$i, $v['jumlah']);
				$sheet->setCellValue('G'.$i, $v['jaspel']);			
				$sheet->setCellValue('H'.$i, $v['total']);				
				$i++;
			}
			
			if (isset($ket['diskon'])) {
				$sheet->insertNewRowBefore($i+1, 1);
				$sheet->setCellValue('G'.$i, 'Diskon');
				$sheet->setCellValue('H'.$i, $ket['diskon']);
				$sheet->getStyle("G$i:H$i")->getFont()->setBold(true);
				$i++;
			}
			
			$sheet->insertNewRowBefore($i+1, 1);
			$sheet->setCellValue('G'.$i, 'Jumlah');
			$sheet->setCellValue('H'.$i, $row->subtotal);
			$sheet->getStyle("G$i:H$i")->getFont()->setBold(true);
			$i++;
		} else {
			$sheet->setCellValue('B'.$i, $row['nama']);
			$sheet->setCellValue('D'.$i, kelas($k));
			$i++;
			
			foreach ($row['data'] as $data) {
				$sheet->insertNewRowBefore($i+1, 1);
				$sheet->setCellValue('B'.$i, '    ' . $data->nama_tagihan);
				$sheet->setCellValue('E'.$i, $data->subtotal / $data->qty);
				$sheet->setCellValue('F'.$i, $data->qty);
// 				$sheet->setCellValue('G'.$i, $v['jaspel']);
				$sheet->setCellValue('H'.$i, $data->subtotal);
				$i++;
			}
			
			$sheet->insertNewRowBefore($i+1, 1);
			$sheet->setCellValue('G'.$i, 'Jumlah');
			$sheet->setCellValue('H'.$i, $row['total']);
			$sheet->getStyle("G$i:H$i")->getFont()->setBold(true);
			$i++;
		}		
	}
	$sheet->removeRow($i, 1);
	
	//kwitansi pembayaran
	$sheet = $excel->getSheet(3);
	addLogo($sheet, "B1", $logo_file);
	$sheet->setCellValue("F10", '=UPPER(TRIM(SUBSTITUTE(rincian!C' . ($total_row_num + 7) . ', ": ", "")))');
	$sheet->setCellValue("B16", "=rincian!G" . ($total_row_num + 6));
	$sheet->setCellValue("N16", "=rincian!F" . ($total_row_num + 9));
	$sheet->setCellValue("N20", "=rincian!F" . ($total_row_num + 13));
	
	//global
	$sheet = $excel->getSheet(4);
    $sheet->setCellValue("B1",getSettings($db,"smis_autonomous_title",""));
    $sheet->setCellValue("B2",getSettings($db,"smis_autonomous_address_mini",""));
    $sheet->setCellValue("B3",getSettings($db,"smis_autonomous_contact",""));
    
	if ($subtotalRowNum['admin'] != 0)
		$sheet->setCellValue("G13", "='rincian'!G" . $subtotalRowNum['admin']);
	else
		$sheet->setCellValue("G13", 0);
	if ($subtotalRowNum['ruang'] != 0)
		$sheet->setCellValue("G14", "='rincian'!G" . $subtotalRowNum['ruang']);
	else
		$sheet->setCellValue("G14", 0);
	if ($subtotalRowNum['dokter'] != 0)
		$sheet->setCellValue("G15", "='rincian'!G" . $subtotalRowNum['dokter']);
	else
		$sheet->setCellValue("G15", 0);
	if ($subtotalRowNum['rawat'] != 0)
		$sheet->setCellValue("G16", "='rincian'!G" . $subtotalRowNum['rawat']);
	else
		$sheet->setCellValue("G16", 0);
	if ($subtotalRowNum['penunjang'] != 0)
		$sheet->setCellValue("G17", "='rincian'!G" . $subtotalRowNum['penunjang']);
	else
		$sheet->setCellValue("G17", 0);
	if ($subtotalRowNum['farmasi'] != 0)
		$sheet->setCellValue("G18", "='rincian'!G" . $subtotalRowNum['farmasi']);
	else
		$sheet->setCellValue("G18", 0);
	if ($subtotalRowNum['operasi'] != 0)
		$sheet->setCellValue("G19", "='rincian'!G" . $subtotalRowNum['operasi']);
	else
		$sheet->setCellValue("G19", 0);
	
	$sheet->setCellValue("G22", "='rincian'!G" . ($plafon_start_row_num + $offset));
	$sheet->setCellValue("G23", "='rincian'!G" . ($diskon_start_row_num + $offset));
	$sheet->setCellValue("G24", "='rincian'!G" . ($terbayar_start_row_num + $offset));
	$sheet->setCellValue("G26", "='rincian'!G" . ($bayar_start_row_num + $offset));
	$sheet->setCellValue("C27", "='rincian'!C" . ($ntbayar_start_row_num + $offset));
	
	$sheet->setCellValue("F29", 'Jember, ' . date('d-m-Y'));
	$sheet->setCellValue("F33", ArrayAdapter::format("unslug", $user->getNameOnly()));
	
	$excel->setActiveSheetIndex(0);
	header('Content-type: application/vnd.ms-excel');	
	header("Content-Disposition: attachment; filename='RINCIAN_KWITANSI_" . ArrayAdapter::format("only-digit6", $noreg) . "_" . date("Ymd_his") . ".xlsx'");
	$writer = PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
	$writer->save('php://output');
		
	function addLogo($sheet, $coordinate, $path){
        $logo = new PHPExcel_Worksheet_Drawing();
		$logo->setPath($path);
		$logo->setCoordinates($coordinate);
		$logo->setOffsetX(0);
		$logo->setWorksheet($sheet);
	}
		
	function kelas($ruangan) {
		$text = ArrayAdapter::format('unslug', $ruangan);		
		if ($text == 'INSTALASI GAWAT DARURAT')
			return 'IGD';
		return $text;
	}
?>