<?php
global $db;
require_once 'kasir/class/table/TableCash.php';
require_once "smis-base/smis-include-duplicate.php";
$header=array ("ID","No. Kwitansi",'Tanggal','Nilai','Keterangan' );
$uitable = new TableCash ( $header, "Pembayaran Cash", NULL, true );
$uitable->setName ( "cash" );
$uitable->setPrintElementButtonEnable(true);
$uitable->setFooterVisible(false);
$uitable->setDelButtonEnable( !(getSettings($db, "cashier-hide-del-cash", "0")=="1") );
$uitable->setPrintElementButtonEnable(true)
		->setPDFElementButtonEnable(true);
if (isset ( $_POST ['command'] )) {
	require_once "kasir/class/responder/PembayaranResponder.php";
	$adapter = new SummaryAdapter();
	$adapter->addSummary("Nilai", "nilai","money Rp.");
	$adapter->addFixValue("Tanggal", "<strong>Total</strong>");
	$adapter->add ( "ID", "id", "only-digit8" );
	$adapter->add ( "No. Kwitansi", "no_kwitansi" );
	$adapter->add ( "Tanggal", "waktu", "date d M Y H:i" );
	$adapter->add ( "Nilai", "nilai", "money Rp." );
	$adapter->add ( "Keterangan", "keterangan" );
	$dbtable = new DBTable ( $db, "smis_ksr_bayar" );
	$dbtable->setShowAll(true);
	$dbtable->setFetchMethode ( DBTable::$ARRAY_FETCH );
	$dbtable->addCustomKriteria ( "noreg_pasien", "='" . $_POST ['noreg_pasien'] . "'" );
	$dbtable->addCustomKriteria ( "metode", "='cash'" );	
	$dbres = new PembayaranResponder ( $dbtable, $uitable, $adapter );
    $dbres->setDuplicate(false,"");
    $dbres->setMetodePembayaran("cash");
    $dbres->setAutonomous(getSettings($db,"smis_autonomous_id",""));
    if(getSettings($db,"cashier-simple-kwitansi-use-own-number-separated","0")=="1"){
        $prefix=getSettings($db,"cashier-simple-kwitansi-use-own-number-prefix-cash","");
        $dbres->setPrefix($prefix);
    }
    
    if($dbres->isSave()){
		global $user;
		$dbres->addColumnFixValue("operator", $user->getNameOnly());		
	}
	$data = $dbres->command ( $_POST ['command'] );	
	if($dbres->isSave()){
		/* 
		 * berfungsi untuk mengupdate id_kwitansi dari smis_ksr_kolektif
		 * jika fungsi per kwitansi di aktifkan
		 * */
		$success=$data['content']['success'];
		if( $success==1 && getSettings($db, "cashier-simple-kwitansi-per-kwitansi", "0")=="1"){
			$id=$data['content']['id'];
			$query="UPDATE smis_ksr_kolektif set id_kwitansi='".$id."' WHERE prop!='del' AND noreg_pasien='".$_POST['noreg_pasien']."' AND id_kwitansi=0";
			$db->query($query);
		}
        
        /* berfungsi untuk melakukan service 
         * ke kasir agar datanya di cache untuk tagihan*/
        require_once "kasir/snippet/update_total_tagihan.php";
	}
	echo json_encode ( $data );
	return;
}

$uitable->addModal ( "id", "hidden", "", "" );
$uitable->addModal ( "nama_pasien", "hidden", "", $_POST ['nama_pasien'] );
$uitable->addModal ( "noreg_pasien", "hidden", "", $_POST ['noreg_pasien'] );
$uitable->addModal ( "nrm_pasien", "hidden", "", $_POST ['nrm_pasien'] );
$uitable->addModal ( "waktu", "datetime", "Tanggal", date ( "Y-m-d H:i:s" ) );
$uitable->addModal ( "nilai", "money", "Nilai", "" );
$uitable->addModal ( "keterangan", "textarea", "Keterangan", "" );
$modal = $uitable->getModal ();
$modal->setTitle ( "Pembayaran Cash" );

echo $uitable->getHtml ();
echo $modal->getHtml ();
echo addJS ( "framework/bootstrap/js/bootstrap-datetimepicker.js" );
echo addCSS ( "framework/bootstrap/css/bootstrap-datetimepicker.css" );
echo addJS ( "kasir/resource/js/cash.js",false );
echo addCSS ("kasir/resource/css/kwitansi_bodong.css",false);

?>