<?php
/**
 * this file used to create default simple kwitansi
 * which have normal and standard kwitansi
 * but group the value based on it's name
 * 
 * @author : Nurul Huda
 * @license : LGPLv3
 * @copyright : goblooge@gmail.com
 * @since : 21 Des 2017
 * @version : 1.0.0
 * @database :  - smis_ksr_bayar
 *              - smis_ksr_kolektif
 * */
require_once ("smis-base/smis-include-service-consumer.php");
global $db;
global $user;
require_once "kasir/function/get_patient_by_noreg.php";
require_once "kasir/function/get_system_setup.php";
$_noreg      = $_POST ['noreg_pasien'];
$_px         = get_patient_by_noreg($db,$_noreg);
$_setup      = get_system_setup($db,$user,$_noreg,$_px);

$tp     = new TablePrint("simple_kwitansi_grup");
$tp     ->setDefaultBootrapClass(false)
        ->setMaxWidth(false)
        ->setTableClass("kwitansi_print");
$header = $_setup['nama_rs']."</br>".$_setup['alamat_rs'];      
require_once("kasir/function/get_header_kwitansi.php");
get_header_kwitansi($tp,$_setup,$_px,$header);


require_once "kasir/function/get_transaksi_kwitansi.php";
$tcont = $tp;
$tcont ->addColumn("RUANGAN", 1, 1,NULL,NULL,"bold")
       ->addColumn("TANGGAL", 1, 1,NULL,NULL,"bold")
       ->addColumn("TINDAKAN", 1, 1,NULL,NULL,"bold")
       ->addColumn("HRG. SAT", 1, 1,NULL,NULL,"bold")
       ->addColumn("JML", 1, 1,NULL,NULL,"bold")
       ->addColumn("SUBTOTAL", 1, 1,NULL,NULL,"bold")
       ->commit("body");
$list_tagihan   = get_transaksi_kwitansi_grup($db,$_noreg);
$TOTAL_TAGIHAN  = 0;
require_once "kasir/class/MapRuangan.php";
foreach($list_tagihan as $x){    
    $ruang = $x[$_setup['tampilan_ruangan']];
	$tcont ->addColumn("&#09;".MapRuangan::getRealName($ruang), 1, 1)
           ->addColumn( $x['tanggal'], 1, 1)
           ->addColumn(ArrayAdapter::format("unslug", $x['vnama_tagihan']), 1, 1)
           ->addColumn(ArrayAdapter::format("money Rp.", $x['total']/$x['quantity']), 1, 1)
           ->addColumn(" x ". $x['quantity'], 1, 1)
           ->addColumn(ArrayAdapter::format("money Rp.", $x['total']), 1, 1)
           ->commit("body");
	$TOTAL_TAGIHAN += $x['total'];
}

require_once "kasir/function/get_transaksi_diskon.php";
$diskon = get_transaksi_diskon($db,$_noreg);
if($diskon>0){
    $tcont  ->addColumn("SUB TOTAL TAGIHAN", 5, 1,NULL,NULL,"bold")
            ->addColumn(ArrayAdapter::format("money Rp.",$TOTAL_TAGIHAN), 1, 1,NULL,NULL,"bold  ltop")
            ->commit("body");
    $tcont  ->addColumn("DISKON", 5, 1,NULL,NULL,"bold")
            ->addColumn(ArrayAdapter::moneyFormat("zero-money Rp.",$diskon), 1, 1,NULL,NULL,"bold  ltop")
            ->commit("body");
    $TOTAL_TAGIHAN -= $diskon;
}
loadLibrary("smis-libs-function-math");
$round = getSettings($db,"smis-simple-rounding-uang","-1");
$model = getSettings($db,"smis-simple-rounding-model","round");
$TOTAL_TAGIHAN=smis_money_round($TOTAL_TAGIHAN,$round,$model);

$tcont  ->addColumn("TOTAL TAGIHAN", 5, 1,NULL,NULL,"bold")
        ->addColumn(ArrayAdapter::moneyFormat("money Rp.",$TOTAL_TAGIHAN), 1, 1,NULL,NULL,"bold  ltop")
        ->commit("body");
$tcont  ->addSpace(6, 1)
        ->commit("body");


$tcont  ->addColumn("PEMBAYARAN",6, 1,NULL,NULL,"bold")
        ->commit("body");
        
require_once "kasir/function/get_transaksi_bayar.php";        
$tbayar         = $tp;
$list_bayar     = get_transaksi_bayar($db,$_noreg,$_setup);
$TOTAL_DIBAYAR  = 0;
$BAYAR_AKHIR    = 0;
get_list_transaksi_bayar($tp,$list_bayar,$TOTAL_DIBAYAR,$BAYAR_AKHIR,$_setup);
if($_setup['set-sum-bayar']=="1"){
	$tbayar ->addColumn("TOTAL PEMBAYARAN", 5, 1,NULL,NULL,"bold ")
            ->addColumn(ArrayAdapter::format("money Rp.",$TOTAL_DIBAYAR), 1, 1,NULL,NULL,"bold  ltop")
            ->commit("body");
    $tbayar ->addSpace(6, 1,NULL,NULL,"lbottom")
            ->commit("body");
}

require_once "kasir/function/get_kwitansi_terbilang.php";
get_kwitansi_terbilang($tp,$BAYAR_AKHIR);
       
require_once "kasir/function/get_kwitansi_footer.php";
get_kwitansi_footer($tp,$_setup);

require_once "kasir/function/kwitansi_print_button.php";
require_once "kasir/function/end_css_kwitansi.php";
echo kwitansi_print_button($db,"simple_kwitansi_grup",$_px);
echo "<div id='cetak_simple_kwitansi_grup_area'>".$tp->getHtml()."</div>";
echo addCSS("kasir/resource/css/kwitansi_print.css",false);
echo addJS("kasir/resource/js/kwitansi_print.js",false);
echo end_css_kwitansi($db);


?>