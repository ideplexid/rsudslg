<?php

/**
 * this used for deterina weather simple kwitansi need to upgrade
 * so it could determine weather single data or multiple data 
 * 
 * @author      : Nurul Huda
 * @license     : LGPLv3
 * @copyright   : goblooge@gmail.com
 * @since       : 21 Des 2017
 * @version     : 1.0.0
 * @database    :   - smis_ksr_bayar
 *                  - smis_ksr_kolektif
 * */

global $db;
global $user;
require_once "smis-base/smis-include-service-consumer.php";
require_once "kasir/function/get_patient_by_noreg.php";
require_once "kasir/function/get_system_setup.php";
$_noreg      = $_POST ['noreg_pasien'];
$_px         = get_patient_by_noreg($db,$_noreg);
$_setup      = get_system_setup($db,$user,$_noreg,$_px);

$tp          = new TablePrint("simple_kwitansi_grup_dokter");
$tp          ->setDefaultBootrapClass(false)
             ->setMaxWidth(false)
             ->setTableClass("kwitansi_print");
$header      = $_setup['nama_rs']."</br>".$_setup['alamat_rs'];      
require_once("kasir/function/get_header_kwitansi.php");
get_header_kwitansi($tp,$_setup,$_px,$header);

/**space control */
$tp     ->addColumn("&nbsp;", 1, 1,NULL,NULL,"")
        ->addColumn("&nbsp;", 1, 1,NULL,NULL,"")
        ->addColumn("&nbsp;", 1, 1,NULL,NULL,"")
        ->addColumn("&nbsp;", 1, 1,NULL,NULL,"")
        ->addColumn("&nbsp;", 1, 1,NULL,NULL,"")
        ->addColumn("&nbsp;", 1, 1,NULL,NULL,"")
        ->commit("header");

$tcont = $tp;
$tcont ->addColumn("Uraian Pelayanan", 3, 1,NULL,NULL,"")
       ->addColumn("Satuan", 1, 1,NULL,NULL,"  center")
       ->addColumn("Quantity", 1, 1,NULL,NULL," center")
       ->addColumn("Jumlah", 1, 1,NULL,NULL,"  center")
       ->commit("body");
require_once "kasir/function/get_transaksi_kwitansi.php";
$list_tagihan   = get_transaksi_kwitansi($db,$_noreg);
$TOTAL_TAGIHAN  = 0;
$GRUP           = array();
foreach($list_tagihan as $x){
        $GRUP[$x['jenis_tagihan']][] = $x;
        $TOTAL_TAGIHAN += $x['total'];
}


require_once "kasir/function/sum_part_tagihan.php";
/** pendaftaran dan administrasi */
$nilai  = 0;
$nilai += sum_all_tagihan(isset($GRUP['registration'])?$GRUP['registration']:null);
$nilai += sum_all_tagihan(isset($GRUP['administrasi'])?$GRUP['administrasi']:null);


if($nilai>0){
        $tcont  ->addColumn("Pendaftaran & Administrasi", 3, 1,null,NULL,"left")
                ->addColumn("", 1, 1,NULL,NULL,"center")
                ->addColumn("1", 1, 1,NULL,NULL,"center")
                ->addColumn(ArrayAdapter::format("money Rp.",$nilai), 1, 1,NULL,NULL,"center")
                ->commit("body");
        $tcont  ->addColumn("", 5, 1,NULL,NULL,"center")
                ->addColumn(ArrayAdapter::format("money Rp.",$subtotal), 5, 1,"body",NULL,"center");
        $tcont  ->addSpace(10,1,"body");
}
/** Sewa Kamar Perawatan */
if(isset($GRUP['bed'])){
        require_once "kasir/class/MapRuangan.php";
       
        $tcont     ->addColumn("Sewa Kamar Perawatan", 10, 1,"body",NULL,"");
        $list      = get_per_ruang_as_one(isset($GRUP['bed'])?$GRUP['bed']:null);
        $subtotal  = 0;
        foreach($list as $x){
            
                $tcont  ->addColumn(MapRuangan::getRealName(ArrayAdapter::format("slug",$x['name'])), 3, 1,NULL,NULL," lg ")
                        ->addColumn(ArrayAdapter::format("money Rp.",$x['satuan']), 1, 1,NULL,NULL,"")
                        ->addColumn($x['quantity'], 1, 1,NULL,NULL,"center")
                        ->addColumn(ArrayAdapter::format("money Rp.",$x['jumlah']), 1, 1,NULL,NULL,"")
                        ->commit("body"); 
                $subtotal += $x['jumlah'];
        }
        $tcont  ->addColumn("Sub Total", 5, 1,NULL,NULL,"")
                ->addColumn(ArrayAdapter::format("money Rp.",$subtotal), 5, 1,"body",NULL,"");
        $tcont  ->addSpace(10,1,"body");
}

/** Sewa Alat Kesehatan */
if(isset($GRUP['alok']) || isset($GRUP['oksigen_central']) || isset($GRUP['oksigen_manual'])){
        $tcont      ->addColumn("Alat Kesehatan", 10, 1,"body",NULL,"");
        $subtotal   = 0;
        $alok       = sum_all_tagihan(isset($GRUP['alok'])?$GRUP['alok']:null);
        if($alok>0){
                $tcont     ->addColumn("Sewa Alat", 3, 1,NULL,NULL,"lg")
                           ->addColumn(ArrayAdapter::format("money Rp.",$alok), 1, 1,NULL,NULL,"")
                           ->addColumn(1, 1, 1,NULL,NULL,"center")
                           ->addColumn(ArrayAdapter::format("money Rp.",$alok), 1, 1,NULL,NULL,"")
                           ->commit("body"); 
                $subtotal += $alok;
        }

        $oksigen    = sum_all_tagihan(isset($GRUP['oksigen_manual'])?$GRUP['oksigen_manual']:null);
        $oksigen   += sum_all_tagihan(isset($GRUP['oksigen_central'])?$GRUP['oksigen_central']:null);
        if($oksigen>0){
                $tcont     ->addColumn("Oksigen", 3, 1,NULL,NULL,"lg")
                           ->addColumn(ArrayAdapter::format("money Rp.",$oksigen), 1, 1,NULL,NULL,"")
                           ->addColumn(1, 1, 1,NULL,NULL,"center")
                           ->addColumn(ArrayAdapter::format("money Rp.",$oksigen), 1, 1,NULL,NULL,"")
                           ->commit("body"); 
                $subtotal += $alok;
        }
        $tcont  ->addColumn("Sub Total", 5, 1,NULL,NULL,"")
                ->addColumn(ArrayAdapter::format("money Rp.",$subtotal), 5, 1,"body",NULL,"");
        $tcont  ->addSpace(10,1,"body");
}

/** Layanan Dokter */
if(isset($GRUP['visite_dokter']) || isset($GRUP['konsul_dokter']) || isset($GRUP['visite']) || isset($GRUP['konsul']) || isset($GRUP['konsultasi_dokter']) || isset($GRUP['tindakan_dokter']) ){
        $tcont      ->addColumn("Layanan Dokter", 10, 1,"body",NULL," italic ");
        $subtotal   = 0;
        /** tindakan dokter */
        $tindakan_dokter     = get_all_per_nama_as_total(isset($GRUP['tindakan_dokter'])?$GRUP['tindakan_dokter']:null);
        if(count($tindakan_dokter)>0){
                $tcont          ->addColumn("Tindakan Dokter : ", 10, 1,"body",NULL,"");
                foreach($tindakan_dokter as $x){
                        $name   = trim(substr($x['name'],strpos($x['name'],"Oleh")+4));
                        $tcont  ->addColumn($x['name'], 3, 1,NULL,NULL,"lg")
                                ->addColumn(ArrayAdapter::format("money Rp.",$x['satuan']), 1, 1,NULL,NULL,"")
                                ->addColumn($x['quantity'], 1, 1,NULL,NULL,"center")
                                ->addColumn(ArrayAdapter::format("money Rp.",$x['jumlah']), 1, 1,NULL,NULL,"")
                                ->commit("body"); 
                        $subtotal += $x['jumlah'];
                }
        }
        /**visite */
        $visite_dokter     = get_all_per_nama_as_total(isset($GRUP['visite_dokter'])?$GRUP['visite_dokter']:null);
        $visite            = get_all_per_nama_as_total(isset($GRUP['visite'])?$GRUP['visite']:null);
        $visite += $visite_dokter;
        if(count($visite)>0){
                $tcont          ->addColumn("Visite : ", 10, 1,"body",NULL,"");
                foreach($visite as $x){
                        $name   = trim(str_replace("Visite","",$x['name']));
                        $tcont  ->addColumn($name, 3, 1,NULL,NULL,"lg")
                                ->addColumn(ArrayAdapter::format("money Rp.",$x['satuan']), 1, 1,NULL,NULL,"")
                                ->addColumn($x['quantity'], 1, 1,NULL,NULL,"center")
                                ->addColumn(ArrayAdapter::format("money Rp.",$x['jumlah']), 1, 1,NULL,NULL,"")
                                ->commit("body"); 
                        $subtotal += $x['jumlah'];
                }
        }
        /**periksa atau konsultasi */
        $periksa    = get_all_per_nama_as_total(isset($GRUP['konsultasi_dokter'])?$GRUP['konsultasi_dokter']:null);
        if(count($periksa)>0){
                $tcont          ->addColumn("Periksa : ", 10, 1,"body",NULL,"");
                foreach($periksa as $x){
                        $name   = trim(str_replace("Periksa","",$x['name']));
                        $tcont  ->addColumn($name, 3, 1,NULL,NULL,"lg")
                                ->addColumn(ArrayAdapter::format("money Rp.",$x['satuan']), 1, 1,NULL,NULL,"")
                                ->addColumn($x['quantity'], 1, 1,NULL,NULL,"center")
                                ->addColumn(ArrayAdapter::format("money Rp.",$x['jumlah']), 1, 1,NULL,NULL,"")
                                ->commit("body"); 
                        $subtotal += $x['jumlah'];
                }
        }
        /** konsul */
        $konsul_dokter     = get_all_per_nama_as_total(isset($GRUP['konsul_dokter'])?$GRUP['konsul_dokter']:null);
        $konsul            = get_all_per_nama_as_total(isset($GRUP['konsul'])?$GRUP['konsul']:null);
        $konsul += $konsul_dokter;
        if(count($konsul)>0){
                $tcont          ->addColumn("Konsul : ", 10, 1,"body",NULL,"");
                foreach($konsul as $x){
                        $name   = trim(str_replace("Konsul","",$x['name']));
                        $tcont  ->addColumn($name, 3, 1,NULL,NULL,"lg")
                                ->addColumn(ArrayAdapter::format("money Rp.",$x['satuan']), 1, 1,NULL,NULL,"")
                                ->addColumn($x['quantity'], 1, 1,NULL,NULL,"center")
                                ->addColumn(ArrayAdapter::format("money Rp.",$x['jumlah']), 1, 1,NULL,NULL,"")
                                ->commit("body"); 
                        $subtotal += $x['jumlah'];
                }
        }
        
        $tcont  ->addColumn("Sub Total", 5, 1,NULL,NULL,"")
                ->addColumn(ArrayAdapter::format("money Rp.",$subtotal), 5, 1,"body",NULL,"");
        $tcont  ->addSpace(10,1,"body");
}

/**layanan keperawatan */
if(isset($GRUP['tindakan_perawat']) || isset($GRUP['tindakan_perawat_igd'])){
        $tcont           ->addColumn("Tindakan Keperawatan", 10, 1,"body",NULL,"");
        $perawat         = get_per_ruang_as_one(isset($GRUP['tindakan_perawat'])?$GRUP['tindakan_perawat']:null);
        $perawat_igd     = get_per_ruang_as_one(isset($GRUP['tindakan_perawat_igd'])?$GRUP['tindakan_perawat_igd']:null);
        $layanan_perawat = join_tagihan($perawat,$perawat_igd);
        $subtotal        = 0;
        foreach($layanan_perawat as $x){
                
                $tcont  ->addColumn(ArrayAdapter::format("unslug",$x['name']), 3, 1,NULL,NULL," lg ")
                        ->addColumn(ArrayAdapter::format("money Rp.",$x['satuan']), 1, 1,NULL,NULL,"")
                        ->addColumn($x['quantity'], 1, 1,NULL,NULL,"center")
                        ->addColumn(ArrayAdapter::format("money Rp.",$x['jumlah']), 1, 1,NULL,NULL,"")
                        ->commit("body"); 
                $subtotal += $x['jumlah'];
        }
        $tcont  ->addColumn("Sub Total", 5, 1,NULL,NULL,"")
                ->addColumn(ArrayAdapter::format("money Rp.",$subtotal), 5, 1,"body",NULL,"");
        $tcont  ->addSpace(10,1,"body");
}


/**layanan penunjang yaitu laboratory, radiology, ambulan, hemodialisa */
if(isset($GRUP['laboratory']) || isset($GRUP['radiology']) || isset($GRUP['hemodialisa']) || isset($GRUP['ambulan']) ){
        $tcont             ->addColumn("Layanan Penunjang", 10, 1,"body",NULL,"");
        $subtotal          = 0;
        /**laboratory */
        $laboratory        = sum_all_tagihan(isset($GRUP['laboratory'])?$GRUP['laboratory']:null);
        if($laboratory>0){
                $tcont     ->addColumn("Laboratory", 3, 1,NULL,NULL,"lg")
                           ->addColumn(ArrayAdapter::format("money Rp.",$laboratory), 1, 1,NULL,NULL,"")
                           ->addColumn("1", 1, 1,NULL,NULL,"center")
                           ->addColumn(ArrayAdapter::format("money Rp.",$laboratory), 1, 1,NULL,NULL,"")
                           ->commit("body"); 
                $subtotal += $laboratory;
        }

        /**radiology */
        $radiology         = sum_all_tagihan(isset($GRUP['radiology'])?$GRUP['radiology']:null);
        if($radiology>0){
                $tcont     ->addColumn("Radiology", 3, 1,NULL,NULL,"lg")
                           ->addColumn(ArrayAdapter::format("money Rp.",$radiology), 1, 1,NULL,NULL,"")
                           ->addColumn("1", 1, 1,NULL,NULL,"center")
                           ->addColumn(ArrayAdapter::format("money Rp.",$radiology), 1, 1,NULL,NULL,"")
                           ->commit("body"); 
                $subtotal += $radiology;
        }   

        /**fisiotherapy */
        $fisiotherapy      = sum_all_tagihan(isset($GRUP['fisiotherapy'])?$GRUP['fisiotherapy']:null);
        if($fisiotherapy>0){
                $tcont     ->addColumn("Fisiotherapy", 3, 1,NULL,NULL,"lg")
                           ->addColumn(ArrayAdapter::format("money Rp.",$fisiotherapy), 1, 1,NULL,NULL,"")
                           ->addColumn("1", 1, 1,NULL,NULL,"center")
                           ->addColumn(ArrayAdapter::format("money Rp.",$fisiotherapy), 1, 1,NULL,NULL,"")
                           ->commit("body"); 
                $subtotal += $fisiotherapy;
        }   

        /**hemodialisa */
        $hemodialisa        = sum_all_tagihan(isset($GRUP['hemodialisa'])?$GRUP['hemodialisa']:null);
        if($hemodialisa>0){
                $tcont     ->addColumn("Hemodialisa", 3, 1,NULL,NULL,"lg")
                           ->addColumn(ArrayAdapter::format("money Rp.",$hemodialisa), 1, 1,NULL,NULL,"")
                           ->addColumn("1", 1, 1,NULL,NULL,"center")
                           ->addColumn(ArrayAdapter::format("money Rp.",$hemodialisa), 1, 1,NULL,NULL,"")
                           ->commit("body"); 
                $subtotal += $radiology;
        }
        /**ambulan */
        $ambulan           = sum_all_tagihan(isset($GRUP['ambulan'])?$GRUP['ambulan']:null);
        if($ambulan>0){
                $tcont     ->addColumn("Ambulan", 3, 1,NULL,NULL,"lg")
                           ->addColumn(ArrayAdapter::format("money Rp.",$ambulan), 1, 1,NULL,NULL,"")
                           ->addColumn("1", 1, 1,NULL,NULL,"center")
                           ->addColumn(ArrayAdapter::format("money Rp.",$ambulan), 1, 1,NULL,NULL,"")
                           ->commit("body"); 
                $subtotal += $radiology;
        }
        $tcont  ->addColumn("Sub Total", 5, 1,NULL,NULL,"")
                ->addColumn(ArrayAdapter::format("money Rp.",$subtotal), 5, 1,"body",NULL,"");
        $tcont  ->addSpace(10,1,"body");
}

/** Layanan Elektromedis */

$nilai  = sum_all_tagihan(isset($GRUP['elektromedis'])?$GRUP['elektromedis']:null);
if($nilai>0){
        $tcont  ->addColumn("Elektromedis", 3, 1,NULL,NULL,"lg")
                ->addColumn("", 1, 1,NULL,NULL,"")
                ->addColumn("1", 1, 1,NULL,NULL,"center")
                ->addColumn(ArrayAdapter::format("money Rp.",$nilai), 1, 1,NULL,NULL,"")
                ->commit("body");
        $tcont  ->addColumn("", 5, 1,NULL,NULL,"")
                ->addColumn(ArrayAdapter::format("money Rp.",$nilai), 5, 1,"body",NULL,"");
        $tcont  ->addSpace(10,1,"body");
}

/** Layanan Darah */
$nilai  = sum_all_tagihan(isset($GRUP['bank_darah'])?$GRUP['bank_darah']:null);
if($nilai>0){
        $tcont  ->addColumn("Layanan Darah", 3, 1,NULL,NULL,"lg")
                ->addColumn("", 1, 1,NULL,NULL,"")
                ->addColumn("1", 1, 1,NULL,NULL,"center")
                ->addColumn(ArrayAdapter::format("money Rp.",$nilai), 1, 1,NULL,NULL,"")
                ->commit("body");
        $tcont  ->addColumn("", 5, 1,NULL,NULL,"")
                ->addColumn(ArrayAdapter::format("money Rp.",$nilai), 5, 1,"body",NULL,"");
        $tcont  ->addSpace(10,1,"body");
}

/** Layanan Gizi */
$asuhan   = sum_all_tagihan(isset($GRUP['asuhan_gizi'])?$GRUP['asuhan_gizi']:null);
$makanan  = sum_all_tagihan(isset($GRUP['pesanan_makanan'])?$GRUP['pesanan_makanan']:null);
$nilai    = $asuhan+$makanan;
if($nilai>0){
        $tcont  ->addColumn("Layanan Gizi", 10, 1,"body",NULL,"");
        $tcont  ->addColumn("Makan/Diet Khusus", 3, 1,NULL,NULL,"lg")
                ->addColumn("", 1, 1,NULL,NULL,"")
                ->addColumn("1", 1, 1,NULL,NULL,"center")
                ->addColumn(ArrayAdapter::format("money Rp.",$nilai), 1, 1,NULL,NULL,"")
                ->commit("body");
        $tcont  ->addColumn("", 5, 1,NULL,NULL,"")
                ->addColumn(ArrayAdapter::format("money Rp.",$nilai), 5, 1,"body",NULL,"");
        $tcont  ->addSpace(10,1,"body");
}

/** Layanan Lain-Lain */
$nilai  = sum_all_tagihan(isset($GRUP['umum'])?$GRUP['umum']:null);
if($nilai>0){
        $tcont  ->addColumn("Layanan Umum / Lain-Lain", 3, 1,NULL,NULL,"lg")
                ->addColumn("", 1, 1,NULL,NULL,"")
                ->addColumn("1", 1, 1,NULL,NULL,"center")
                ->addColumn(ArrayAdapter::format("money Rp.",$nilai), 1, 1,NULL,NULL,"")
                ->commit("body");
        $tcont  ->addColumn("", 5, 1,NULL,NULL,"")
                ->addColumn(ArrayAdapter::format("money Rp.",$nilai), 5, 1,"body",NULL,"");
        $tcont  ->addSpace(10,1,"body");
}

/** Layanan Farmasi */
$nilai  = sum_all_tagihan(isset($GRUP['penjualan_resep'])?$GRUP['penjualan_resep']:null);
$nilai += sum_all_tagihan(isset($GRUP['return_resep'])?$GRUP['return_resep']:null);
$nilai += sum_all_tagihan(isset($GRUP['asuhan_farmasi'])?$GRUP['asuhan_farmasi']:null);
if($nilai>0){
        $tcont  ->addColumn("Layanan Farmasi", 3, 1,NULL,NULL,"")
                ->addColumn("", 1, 1,NULL,NULL,"")
                ->addColumn("1", 1, 1,NULL,NULL,"center")
                ->addColumn(ArrayAdapter::format("money Rp.",$nilai), 1, 1,NULL,NULL,"")
                ->commit("body");
        $tcont  ->addSpace(10,1,"body");
}

/**Layanan OK */
if(isset($GRUP['ok'])){
        $list        = $GRUP['ok'];
        $_grup_ruang = array();
        foreach($list as $x){
                $nama_ruangan                 = $x['ruangan_map']; 
                $_grup_ruang[$nama_ruangan][] = $x;
        }
        
        foreach($_grup_ruang as $name=>$list){
                $tcont     ->addColumn("Layanan ".ucwords(strtolower(ArrayAdapter::format("unslug",$name))), 10, 1,"body",NULL,"");
                $subtotal  = 0;
                foreach($list as $x){
                        if($x['total']==0){
                                continue;
                        }
                        $tcont  ->addColumn($x['nama_tagihan'], 3, 1,NULL,NULL,"lg")
                                ->addColumn(ArrayAdapter::format("money Rp.",$x['total']/$x['quantity']), 1, 1,NULL,NULL,"")
                                ->addColumn($x['quantity'], 1, 1,NULL,NULL,"center")
                                ->addColumn(ArrayAdapter::format("money Rp.",x['total']), 1, 1,NULL,NULL,"")
                                ->commit("body"); 
                        $subtotal += $x['total'];
                }
                $tcont  ->addColumn("Sub Total", 5, 1,NULL,NULL,"")
                        ->addColumn(ArrayAdapter::format("money Rp.",$subtotal), 5, 1,"body",NULL,"");
                $tcont  ->addSpace(10,1,"body");
        }
}

require_once "kasir/function/get_transaksi_diskon.php";
$diskon = get_transaksi_diskon($db,$_noreg);
if($diskon>0){
    $tcont  ->addColumn("SUB TOTAL TAGIHAN", 5, 1,NULL,NULL,"")
            ->addColumn(ArrayAdapter::format("money Rp.",$TOTAL_TAGIHAN), 1, 1,NULL,NULL,"  ltop")
            ->commit("body");
    $tcont  ->addColumn("DISKON", 5, 1,NULL,NULL,"")
            ->addColumn(ArrayAdapter::moneyFormat("zero-money Rp.",$diskon), 1, 1,NULL,NULL,"  ltop")
            ->commit("body");
    $TOTAL_TAGIHAN -= $diskon;
}

loadLibrary("smis-libs-function-math");
$round          = getSettings($db,"smis-simple-rounding-uang","-1");
$model          = getSettings($db,"smis-simple-rounding-model","round");
$TOTAL_TAGIHAN  = smis_money_round($TOTAL_TAGIHAN,$round,$model);
$tcont  ->addColumn("TOTAL TAGIHAN", 5, 1,NULL,NULL,"")
        ->addColumn(ArrayAdapter::moneyFormat("money Rp.",$TOTAL_TAGIHAN), 1, 1,NULL,NULL,"  ltop")
        ->commit("body");
$tcont  ->addSpace(6, 1)
        ->commit("body");

require_once "kasir/function/get_transaksi_bayar.php";
$tcont  ->addColumn("PEMBAYARAN",6, 1,NULL,NULL,"")
        ->commit("body");
$tbayar         = $tp;
$list_bayar     = get_transaksi_bayar($db,$_noreg,$_setup);
$TOTAL_DIBAYAR  = 0;
$BAYAR_AKHIR    = 0;
get_list_transaksi_bayar($tp,$list_bayar,$TOTAL_DIBAYAR,$BAYAR_AKHIR,$_setup);
if($_setup['set-sum-bayar']=="1"){
	$tbayar ->addColumn("TOTAL PEMBAYARAN", 5, 1,NULL,NULL," ")
            ->addColumn(ArrayAdapter::format("money Rp.",$TOTAL_DIBAYAR), 1, 1,NULL,NULL,"  ltop")
            ->commit("body");
    $tbayar ->addSpace(6, 1,NULL,NULL,"lbottom")
            ->commit("body");
}

require_once "kasir/function/get_kwitansi_terbilang.php";
get_kwitansi_terbilang($tp,$BAYAR_AKHIR,"");
       
require_once "kasir/function/get_kwitansi_footer.php";
get_kwitansi_footer($tp,$_setup);

require_once "kasir/function/kwitansi_print_button.php";
require_once "kasir/function/end_css_kwitansi.php";
echo kwitansi_print_button($db,"simple_kwitansi_grup_dokter",$_px);
echo "<div id='cetak_simple_kwitansi_grup_dokter_area'>".$tp->getHtml()."</div>";
echo addCSS ("kasir/resource/css/kwitansi_print.css",false);
echo addJS  ("kasir/resource/js/kwitansi_print.js",false);
echo end_css_kwitansi($db);
?>