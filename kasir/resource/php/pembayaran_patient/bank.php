<?php
global $db;
require_once "smis-base/smis-include-duplicate.php";
require_once "kasir/class/responder/PembayaranResponder.php";
require_once 'kasir/class/responder/BankResponder.php';
require_once 'kasir/class/table/TableBank.php';
$header=array ("ID","No. Kwitansi",'Tanggal','Nilai',"Bank",'No Bukti',"Biaya","Dibayar","Keterangan","ID Billing" );
$uitable = new TableBank( $header, "Pembayaran Bank", NULL, true );
$uitable->setPrintElementButtonEnable(true);
$uitable->setFooterVisible(false);
$uitable->setName ( "bank" );
$uitable->setDelButtonEnable( !(getSettings($db, "cashier-hide-del-bank", "0")=="1") );
if (isset ( $_POST ['command'] )) {
	$adapter = new SummaryAdapter();
	$adapter->addSummary("Nilai", "nilai","money Rp.");
	$adapter->addSummary("Biaya", "nilai_tambahan","money Rp.");
	$adapter->addFixValue("Tanggal", "<strong>Total</strong>");
	$adapter->add ( "ID", "id", "only-digit8" );
	$adapter->add ( "No. Kwitansi", "no_kwitansi" );
	$adapter->add ( "Tanggal", "waktu", "date d M Y H:i" );
	$adapter->add ( "Nilai", "nilai", "money Rp." );
	$adapter->add ( "Bank", "nama_bank" );
	$adapter->add ( "No Bukti", "no_bukti" );
	$adapter->add ( "Keterangan", "keterangan" );
	$adapter->add ( "Biaya", "nilai_tambahan","money Rp." );
	$adapter->add ( "ID Billing", "idbilling" );
	$adapter->add ( "Dibayar", "di_bank","trivial_0_Tunai_Bank" );
	
	$dbtable = new DBTable ( $db, "smis_ksr_bayar" );
	$dbtable->setShowAll(true);
	$dbtable->setFetchMethode ( DBTable::$ARRAY_FETCH );
	$dbtable->addCustomKriteria ( "noreg_pasien", "='" . $_POST ['noreg_pasien'] . "'" );
	$dbtable->addCustomKriteria ( "metode", "='bank'" );
	
	$dbres = new BankResponder ( $dbtable, $uitable, $adapter );
    $dbres->setDuplicate(false,"");
    $dbres->setMetodePembayaran("bank");
    $dbres->setAutonomous(getSettings($db,"smis_autonomous_id",""));
    if(getSettings($db,"cashier-simple-kwitansi-use-own-number-separated","0")=="1"){
        $prefix=getSettings($db,"cashier-simple-kwitansi-use-own-number-prefix-bank","");
        $dbres->setPrefix($prefix);
    }
	if($dbres->isSave()){
		global $user;
		$dbres->addColumnFixValue("operator", $user->getNameOnly());
	}
	$data = $dbres->command ( $_POST ['command'] );
	if($dbres->isSave()){
		/*
		 * berfungsi untuk mengupdate id_kwitansi dari smis_ksr_kolektif
		 * jika fungsi per kwitansi di aktifkan
		 * */
		$success=$data['content']['success'];
		if( $success==1 && getSettings($db, "cashier-simple-kwitansi-per-kwitansi", "0")=="1"){
			$id=$data['content']['id'];
			$query="UPDATE smis_ksr_kolektif set id_kwitansi='".$id."' WHERE prop!='del' AND noreg_pasien!='".$_POST['noreg_pasien']."' AND id_kwitansi=0 ";
			$db->query($query);
		}
        
        /* berfungsi untuk melakukan service 
         * ke kasir agar datanya di cache untuk tagihan*/
        require_once "kasir/snippet/update_total_tagihan.php";
	}
	echo json_encode ( $data );
	return;
}

require_once 'kasir/class/adapter/BankAdapter.php';
$vtable = new DBTable ( $db, "smis_ksr_bank" );
$vtable->setOrder ( "bank ASC, rekening ASC, nama ASC " );
$vtable->setShowAll ( true );
$v = $vtable->view ( "", 0 );
$tvadapter = new BankAdapater ();
$bank = $tvadapter->getContent ( $v ['data'] );

$option=new OptionBuilder();
$option->add("Setoran Tunai");
$option->add("Bank Kartu Debit");
$option->add("Bank Kartu Kredit");

$uitable->addModal ( "id", "hidden", "", "" );
$uitable->addModal ( "nama_pasien", "hidden", "", $_POST ['nama_pasien'] );
$uitable->addModal ( "noreg_pasien", "hidden", "", $_POST ['noreg_pasien'] );
$uitable->addModal ( "nrm_pasien", "hidden", "", $_POST ['nrm_pasien'] );
$uitable->addModal ( "waktu", "datetime", "Tanggal", date ( "Y-m-d" ) );
$uitable->addModal ( "id_bank", "select", "Bank", $bank );
$uitable->addModal ( "no_bukti", "text", "No. Bukti", "" );
$uitable->addModal ( "nilai", "money", "Nilai", "" );
$uitable->addModal ( "keterangan", "select", "Keterangan", $option->getContent() );
$uitable->addModal ( "persen", "text", "Persentase Biaya", "" );
$uitable->addModal ( "nilai_tambahan", "money", "Nilai Biaya", "" );
//$uitable->addModal ( "di_bank", "checkbox", "Lewat Bank", "1" );

$modal = $uitable->getModal ();
$modal->setTitle ( "Pembayaran Bank" );

echo $uitable->getHtml ();
echo $modal->getHtml ();
echo addJS ( "framework/bootstrap/js/bootstrap-datetimepicker.js" );
echo addCSS ( "framework/bootstrap/css/bootstrap-datetimepicker.css" );
echo addJS ( "kasir/resource/js/bank.js",false );
echo addCSS ( "kasir/resource/css/bank.css",false );
?>