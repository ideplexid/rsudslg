<?php
global $db;
$header=array ('Ruangan','Layanan',"Tindakan",'Harga' ,"Jaspel");
$uitable = new Table( $header, "Kwitansi Bayangan", NULL, true );
$uitable->setName ( "bayangan" );
$uitable->setDelButtonEnable(false);
$uitable->setAddButtonEnable(false);
$uitable->setReloadButtonEnable(false);
$uitable->setPrintButtonEnable(false);
$uitable->setFooterVisible(false);

$create=new Button("", "", "");
$create->setIsButton(Button::$ICONIC);
$create->setIcon("fa fa-spinner");
$create->setClass("btn-primary");
$create->setAction("bayangan.revoke_last()");
$uitable->addHeaderButton($create);
if (isset ( $_POST ['command'] )) {
	require_once 'kasir/class/responder/BayanganResponder.php';
	require_once 'kasir/class/adapter/BayanganAdapter.php';
	$adapter = new BayanganAdapter ();
	$adapter->add ( "Ruangan", "ruangan" );
	$adapter->add ( "Layanan", "layanan" );
	$adapter->add ( "Tindakan", "tindakan" );
	$adapter->add ( "Harga", "harga","money Rp." );
	$adapter->add ( "Jaspel", "jaspel","money Rp." );
	$dbtable = new DBTable ( $db, "smis_ksr_bayangan" );
	$dbtable->setShowAll(true);
	$dbtable->addCustomKriteria("noreg", "='".$_POST['noreg']."'");
	$dbtable->setFetchMethode ( DBTable::$ARRAY_FETCH );
	$dbres = new BayanganResponder( $dbtable, $uitable, $adapter );
	$data = $dbres->command ( $_POST ['command'] );
	echo json_encode ( $data );
	return;
}

$uitable->addModal ( "id", "hidden", "", "" );
$uitable->addModal ( "ruangan", "text", "Ruangan", "" );
$uitable->addModal ( "layanan", "text", "Layanan", "" );
$uitable->addModal ( "tindakan", "text", "Tindakan", "" );
$uitable->addModal ( "harga", "money", "Nilai", "0" );
$uitable->addModal ( "jaspel", "money", "Jaspel", "0" );

$modal = $uitable->getModal ();
$modal->setTitle ( "Kwitansi Bayangan" );

$close=new Button("", "", "Batal");
$close	->addClass("btn-primary")
		->setIsButton(Button::$ICONIC_TEXT)
		->setIcon("fa fa-close")
		->setAction("bayangan.force_close()");
$load=new LoadingBar("bayangan_bar", "");
$loadmodal=new Modal("bayangan_modal", "", "Loading");
$loadmodal->addHTML($load->getHtml(),"after")->addFooter($close);

echo addJS ( "base-js/smis-base-loading.js");
echo $uitable->getHtml ();
echo $modal->getHtml ();
echo $loadmodal->getHtml();

echo addCSS ("kasir/resource/css/bayangan.css",false);
echo addJS  ("kasir/resource/js/bayangan.js",false);
?>