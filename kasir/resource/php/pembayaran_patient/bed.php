<?php
	global $db;
	$head=array ('Nama','Tanggal','Biaya',"Keterangan");
	$uitable = new Table ( $head, "Penggunaan Bed");
	$uitable->setName ( "bed" );
	$uitable->setFooterVisible(false);
	$uitable->setDelButtonEnable(false);
	$uitable->setAddButtonEnable(false);
	$uitable->setPrintButtonEnable(false);
	$uitable->setReloadButtonEnable(false);
	if(isset($_POST['command'])){
		require_once 'kasir/class/responder/BedResponder.php';
		$adapter = new SummaryAdapter();
		$adapter->addFixValue("Tanggal", "<strong>Total</strong>");
		$adapter->addSummary("Biaya", "total","money Rp.");
		$adapter->add ( "Nama", "nama_tagihan" );
		$adapter->add ( "Tanggal", "tanggal" );
		$adapter->add ( "Biaya", "total","money Rp." );
		$adapter->add ( "Keterangan", "keterangan" );
		$dbtable = new DBTable ( $db, "smis_ksr_kolektif" );
		$dbtable->addCustomKriteria("jenis_tagihan", "='bed'");
		$dbtable->addCustomKriteria("noreg_pasien", "='".$_POST['noreg_pasien']."'");
        $dbtable->addCustomKriteria("akunting_only", "=0");
		$dbtable->setShowAll(true);
		$dbres=new BedResponder($dbtable, $uitable, $adapter);
		$pack=$dbres->command($_POST['command']);
		echo json_encode($pack);
		return;
	}
	
	$uitable->addModal("id", "hidden", "", "");
	$uitable->addModal("id_unit", "hidden", "", "");
	$uitable->addModal("dari", "datetime", "Dari", "");
	$uitable->addModal("sampai", "datetime", "Sampai", "");
	$uitable->addModal("ruangan", "text", "Ruangan", "","n",NULL,true);
	
	$hiden=new Hidden("noreg_pasien_bed", "", $_POST['noreg_pasien']);
	echo $uitable->getHtml ();
	echo $hiden->getHtml();
	echo $uitable->getModal()->setTitle("Bed")->getHtml();	
	echo addJS ( "framework/bootstrap/js/bootstrap-datetimepicker.js" );
	echo addJS ( "framework/smis/js/table_action.js" );
	echo addJS ( "kasir/resource/js/bed.js",false );
	echo addCSS ( "framework/bootstrap/css/bootstrap-datetimepicker.css" );
?>
