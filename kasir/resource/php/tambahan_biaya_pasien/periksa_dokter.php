<?php
/**
 * this used for change the data of doctor treatment
 * so the data could be input from the cashier
 * 
 * @author 		: Nurul HUda
 * @since 		: 18 Jan 2016
 * @license 	: LGPLv2
 * @copyright 	: goblooge@gmail.com
 * @version 	: 3.0.1
 */
global $db;
require_once 'smis-base/smis-include-service-consumer.php';
require_once 'smis-libs-class/ServiceProviderList.php';
require_once 'smis-libs-hrd/EmployeeResponder.php';

$pheader 	 = array("Nama","Jabatan","NIP");
$dktable 	 = new Table($pheader);
$dktable 	 ->setName("dk_periksa_dokter")
		 	 ->setModel(Table::$SELECT);
$adapter 	 = new SimpleAdapter();
$adapter 	 ->add("Nama", "nama")
		 	 ->add("Jabatan", "nama_jabatan")
		 	 ->add("NIP", "nip");
$dokter	 	 = new EmployeeResponder($db, $dktable, $adapter, "dokter");
$pheader 	 = array("Nama","NRM","Alamat");
$pasientable = new Table($pheader);
$pasientable ->setName("px_periksa_dokter")
			 ->setModel(Table::$SELECT);
$adapter	 = new SimpleAdapter();
$adapter	 ->add("Nama", "nama_pasien")
			 ->add("NRM", "nrm","digit8")
			 ->add("Alamat", "alamat_pasien");
$pasien		 = new ServiceResponder($db, $pasientable, $adapter, "get_registered");
$super		 = new SuperCommand();
$super		 ->addResponder("px_periksa_dokter", $pasien);
$super		 ->addResponder("dk_periksa_dokter", $dokter);
$data		 = $super->initialize();
if($data != null){
	echo $data;
	return;
}


$header	 	 = array ('Tanggal',"Dokter",'Pasien',"NRM","No Reg",'Biaya' );
$uitable 	 = new Table ( $header, "", NULL, true );
$uitable 	 ->setName ( "periksa_dokter" );
	 
$service 	 = new ServiceProviderList($db,"periksa_dokter");
$service 	 ->execute();
$ruangan 	 = $service->getContent();

if (isset ( $_POST ['command'] )) {
	$adapter = new SimpleAdapter ();
	$adapter ->add ( "Tanggal", "waktu", "date d M Y" )
			 ->add ( "Pasien", "nama_pasien" )
			 ->add ( "NRM", "nrm_pasien", "digit6" )
			 ->add ( "No Reg", "noreg_pasien", "digit6" )
			 ->add ( "Dokter", "nama_dokter" )
			 ->add ( "Biaya", "harga","money Rp." );
	$dbres 	 = new ServiceResponder($db, $uitable, $adapter, "periksa_dokter",$_POST["ruang"]);
	$data	 = $dbres->command ( $_POST ['command'] );
	echo json_encode ( $data );
	return;
}

$noreg_pasien		= "";
if( isset($_POST['noreg']) && $_POST['noreg']!="" || isset($_POST['noreg_pasien']) && $_POST['noreg_pasien']!="" ){
	$noreg_pasien	= isset($_POST['noreg'])?$_POST['noreg']:$_POST['noreg_pasien'];
	echo addCSS("kasir/resource/css/periksa_dokter.css",false);
}

$uitable	->addModal("nama_pasien",  "chooser-periksa_dokter-px_periksa_dokter-Pilih Pasien","Nama", "","",NULL,true)
			->addModal("alamat_pasien",  "text","Alamat", "","",NULL,true)
			->addModal("nrm_pasien", "text", "NRM", "","",NULL,true)
			->addModal("noreg_pasien", "text","No Reg",  $noreg_pasien,"",NULL,true)
			->addModal("carabayar", "text","Jenis",  "","",NULL,true)
			->addModal("ruang", "select", "Ruangan", $ruangan);
$form		= $uitable->getModal()->setTitle("Pasien")->getForm();
$uitable	->clearContent();
$uitable	->addModal("waktu", "hidden", "", date("Y-m-d"),"",NULL,true)
			->addModal("nama_dokter",  "chooser-periksa_dokter-dk_periksa_dokter-Pilih Dokter","Dokter", "","",NULL,true)
			->addModal("id_dokter",  "hidden","", "","",NULL,true)
			->addModal("harga", "money", "Biaya", "")
			->addModal("id", "hidden", "", "");

echo $uitable	->getModal()->setTitle("Periksa")->getHtml();
echo $form 		->getHtml();
echo $uitable	->getHtml ();
echo addJS ("framework/smis/js/table_action.js");
echo addJS ("kasir/resource/js/periksa_dokter.js",false);
?>
