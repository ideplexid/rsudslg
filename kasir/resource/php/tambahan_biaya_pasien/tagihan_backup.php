<?php 
require_once 'smis-libs-class/MasterSlaveTemplate.php';
require_once 'kasir/class/responder/TagihanBackupResponder.php';
global $db;
global $user;

$jenis = new OptionBuilder();
$tagihan_backup = new MasterSlaveTemplate($db, "smis_ksr_tagihan", "kasir", "tagihan_backup");
$tagihan_responder = new TagihanBackupResponder($tagihan_backup->getDBtable(),$tagihan_backup->getUItable(),$tagihan_backup->getAdapter());
$tagihan_backup ->setDBresponder($tagihan_responder)
                ->setDateEnable(true)
				->getDBtable()
				->setOrder(" ruangan ASC, tanggal ASC ");

if(isset($_POST['noreg_pasien'])){
	$tagihan_backup	->getDBtable()
					->addCustomKriteria("noreg_pasien", "='".$_POST['noreg_pasien']."'");	
}

$uitable = $tagihan_backup->getUItable();
$header  = array("Jenis","Ruangan","Nama","Keterangan","Tanggal","Nilai","Jaspel","Total","Operator");
$uitable ->setHeader($header);
$uitable ->addModal("pxname", "chooser-tagihan_backup-pasien_tagihan_backup-Pilih Pasien", "Pasien", "","n",null,true);
$uitable ->addModal("pxnrm", "text", "NRM", "","n",null,true);
$uitable ->addModal("pxnoreg", "text", "No Reg", "","n",null,true);
$tagihan_backup ->addNoClear("nama_pasien");
$tagihan_backup ->addNoClear("nrm_pasien");
$tagihan_backup ->addNoClear("noreg_pasien");
$tagihan_backup ->addNoClear("pxname");
$tagihan_backup ->addNoClear("pxnrm");
$tagihan_backup ->addNoClear("pxnoreg");
$tagihan_backup ->getForm();
$tagihan_backup ->addFlag("noreg_pasien", "Nama Pasien", "Silakan Pilih Dulu Pasienya");

if($tagihan_backup->getDBResponder()->isPreload()){
    require_once "kasir/function/get_ruang_daftar.php";
    loadLibrary("smis-libs-function-medical");
    $replace=medical_service();
    $op=new OptionBuilder();
    foreach($replace as $id=>$name){
        $op->add($name,$id);
    }
    $uitable->addModal("kredit", "hidden", "", "")
            ->addModal("debet", "hidden", "", "")
            ->addModal("id", "hidden", "", "")
            ->addModal("id_tagihan", "hidden", "", "")
            ->addModal("nama_pasien", "hidden", "", "")
            ->addModal("nrm_pasien", "hidden", "", "")
            ->addModal("noreg_pasien", "hidden", "", "")
            ->addModal("nama_tagihan","chooser-tagihan_backup-tagihan_backup_master-Tagihan", "Tagihan", "")
            ->addModal("ruangan", "select", "Ruangan", getRuanganDaftar(),"n")
            ->addModal("jenis_tagihan", "select", "Jenis Tagihan", $op->getContent(),"n")
            ->addModal("keterangan","textarea", "Keterangan", "")
            ->addModal("tanggal", "date", "Tanggal", "","n")
            ->addModal("nilai", "money", "Biaya", "","n")
            ->addModal("jaspel", "checkbox", "Jaspel", "0");
}

$adapter = $tagihan_backup->getAdapter();
$adapter ->add("Jenis", "jenis_tagihan")
		 ->add("Ruangan", "ruangan","unslug")
		 ->add("Nama","nama_tagihan")
		 ->add("Tanggal","tanggal")
		 ->add("Keterangan","keterangan")
		 ->add("Jaspel","jaspel","trivial_1_V_-")
		 ->add("Keterangan","keterangan")
		 ->add("Operator","operator")
		 ->add("Nilai", "nilai","money Rp.")
		 ->add("Total", "total","money Rp.");
$tagihan_backup->setModalTitle("Tambah Tagihan");
$modal = $tagihan_backup->getModal();
$modal ->setComponentSize(Modal::$MEDIUM);

if($tagihan_backup->getDBResponder()->isSave()){
	$total = $_POST['nilai'];
	$tagihan_backup->getDBResponder()->addColumnFixValue("total", $total);
    $tagihan_backup->getDBResponder()->addColumnFixValue("operator", $user->getNameOnly());
}

$header=array ('Nama','Ruangan',"Jenis","Harga" ,"Jaspel");
$dbtable=new DBTable($db,"smis_ksr_tagihan_tambahan");
$uitable = new Table ( $header );
$uitable->setName ( "tagihan_backup_master" );
$uitable->setModel ( Table::$SELECT );
$adapter = new SimpleAdapter ();
$adapter->add ( "Nama", "nama" );
$adapter->add ( "Jenis", "jenis" );
$adapter->add ( "Ruangan", "ruangan", "unslug" );
$adapter->add ( "Harga", "harga","money Rp." );
$adapter->add ( "Jaspel", "jaspel", "trivial_1_x_" );
$responder = new DBResponder ( $dbtable, $uitable, $adapter );
$tagihan_backup->getSuperCommand()->addResponder("tagihan_backup_master", $responder);
$tagihan_backup->addSuperCommand("tagihan_backup_master", array());
$tagihan_backup->addSuperCommandArray("tagihan_backup_master", "id_tagihan", "id");
$tagihan_backup->addSuperCommandArray("tagihan_backup_master", "jenis_tagihan", "jenis");
$tagihan_backup->addSuperCommandArray("tagihan_backup_master", "nama_tagihan", "nama");
$tagihan_backup->addSuperCommandArray("tagihan_backup_master", "keterangan", "keterangan");
$tagihan_backup->addSuperCommandArray("tagihan_backup_master", "nilai", "biaya");
$tagihan_backup->addSuperCommandArray("tagihan_backup_master", "jaspel", "jaspel");
$tagihan_backup->addSuperCommandArray("tagihan_backup_master", "ruangan", "ruangan");
$tagihan_backup->addSuperCommandArray("tagihan_backup_master", "debet", "debet");
$tagihan_backup->addSuperCommandArray("tagihan_backup_master", "kredit", "kredit");

require_once ("smis-base/smis-include-service-consumer.php");
$header=array ('Nama','NRM',"No Reg","Alamat" ,"Jenis");
$uitable = new Table ( $header );
$uitable->setName ( "pasien_tagihan_backup" );
$uitable->setModel ( Table::$SELECT );
$adapter = new SimpleAdapter ();
$adapter->add ( "Nama", "nama_pasien" );
$adapter->add ( "NRM", "nrm", "digit8" );
$adapter->add ( "No Reg", "id", "digit8" );
$adapter->add ( "Alamat", "alamat_pasien" );
$adapter->add ( "Jenis", "uri", "trivial_0_Rawat Jalan_Rawat Inap" );
$responder = new ServiceResponder ( $db, $uitable, $adapter, "get_registered" );
$tagihan_backup->getSuperCommand()->addResponder("pasien_tagihan_backup", $responder);
$tagihan_backup->addSuperCommand("pasien_tagihan_backup", array());
$tagihan_backup->addSuperCommandArray("pasien_tagihan_backup", "nama_pasien", "nama_pasien");
$tagihan_backup->addSuperCommandArray("pasien_tagihan_backup", "nrm_pasien", "nrm");
$tagihan_backup->addSuperCommandArray("pasien_tagihan_backup", "noreg_pasien", "id");
$tagihan_backup->addSuperCommandArray("pasien_tagihan_backup", "pxname", "nama_pasien");
$tagihan_backup->addSuperCommandArray("pasien_tagihan_backup", "pxnrm", "nrm");
$tagihan_backup->addSuperCommandArray("pasien_tagihan_backup", "pxnoreg", "id");
$tagihan_backup->addSuperCommandArray("pasien_tagihan_backup", "smis_action_js", "tagihan_backup.view();","smis_action_js");
$tagihan_backup->addRegulerData("noreg_pasien", "pxnoreg","id-value");
$tagihan_backup->initialize();

?>