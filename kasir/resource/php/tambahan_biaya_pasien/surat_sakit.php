<?php
    global $db;
    require_once 'smis-libs-class/MasterTemplate.php';
    $master  = new MasterTemplate($db,"smis_ksr_sks","kasir","surat_sakit");
    $master  ->setDateEnable(true);
    $uitable = $master->getUItable();
    $uitable ->setAddButtonEnable(false);
    $uitable ->setEditButtonEnable(false);
    $uitable ->setDetailButtonEnable(true);    
    $uitable ->setHeader(array("No.","Tanggal","Nama","No.Reg","NRM","Dokter","Kondisi","Selama","Dari","Sampai"));
    $uitable ->addModal("id","hidden","","")
             ->addModal("tanggal","date","Tanggal","")
             ->addModal("nama_pasien","text","Nama Pasien","")
             ->addModal("nrm_pasien","text","NRM Pasien","")
             ->addModal("noreg_pasien","text","No. Reg Pasien","")
             ->addModal("umur","text","Umur","")
             ->addModal("jk","hidden","","")
             ->addModal("perusahaan","text","Perusahaan","")
             ->addModal("kondisi","checkbox","Perlu Istirahat","")
             ->addModal("dari","date","Dari","")
             ->addModal("sampai","date","Sampai","")
             ->addModal("selama","hidden","","")
             ->addModal("nama_dokter","text","Dokter","")
             ->addModal("id_dokter","hidden","","");
    $adapter = $master->getAdapter();
    $adapter ->setUseNumber(true,"No.","back.")
             ->add("Tanggal","tanggal","date d M Y")
             ->add("Nama","nama_pasien")
             ->add("No.Reg","noreg_pasien")
             ->add("NRM","nrm_pasien")
             ->add("Dokter","nama_dokter")
             ->add("Kondisi","kondisi","trivial_0_Sehat_Perlu Istirahat")
             ->add("Selama","selama","back Hari")
             ->add("Dari","dari","date d M Y")
             ->add("Sampai","sampai","date d M Y");
    $master  ->setModalTitle("Surat Keterangan Sakit");
    $master  ->initialize();
?>