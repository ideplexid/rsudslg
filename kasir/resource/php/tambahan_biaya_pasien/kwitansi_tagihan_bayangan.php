<?php
require_once ("smis-base/smis-include-service-consumer.php");
global $db;
global $user;
/*MENGAMBIL DATA PASIEN di REGISTRASI*/
$header=array ();
$adapter = new SimpleAdapter ();
$uitable = new Table ( $header );
$noreg=$_POST ['noreg_pasien'];
$responder = new ServiceResponder ( $db, $uitable, $adapter, "get_registered" );
$responder->addData("id", $noreg);
$responder->addData("command", "edit");
$data = $responder->command ( "edit" );
$px=$data['content'];
/*END OF MENGAMBIL DATA PASIEN DI REGISTRASI DI SIMPAN DI $PX*/

/*MEMBUAT TOMBOL CETAK*/
$btn=new Button("print_button_kwitansi_tagihan_bayangan", "", " Cetak ");
$btn->addClass("btn btn-primary");
$btn->setIcon(" fa fa-print ");
$btn->setIsButton(BUtton::$ICONIC_TEXT);
$btn->setAction("kwitansi_tagihan_bayangan_print()");
if( $px['uri']=="0" && getSettings($db,"cashier-simple-kwitansi-cek-print-rj","0")=="1" || $px['uri']=="1" && getSettings($db,"cashier-simple-kwitansi-cek-print-ri","0")=="1"){
	$btn->setAction("cek_kwitansi_tagihan_bayangan_print()");
}
echo $btn->getHtml();
/*AKHIR DARI MEMBUAT TOMBOL CETAK*/



/*MENGAMBIL DATA TAGIHAN*/
$tampilan_ruangan=getSettings($db, "cashier-simple-kwitansi-place", "0")=="1"?"ruangan_kasir":"ruangan";
$dbtable=new DBTable($db,"smis_ksr_kolektif");
$dbtable->setShowAll(true);
$dbtable->setFetchMethode(DBTable::$ARRAY_FETCH);
$dbtable->setOrder(" urutan ASC, nama_grup ASC , ruangan ASC, dari ASC, sampai ASC ");
$dbtable->addCustomKriteria("noreg_pasien","='".$noreg."'");
$dbtable->addCustomKriteria("hidden","='0'");
$dbtable->addCustomKriteria("akunting_only","='0'");
if(getSettings($db, "cashier-simple-kwitansi-per-kwitansi", "0")=="1"){
	$dbtable->addCustomKriteria("id_kwitansi","='0'");	//jika kwitansi sendiri - sendiri
}

$dt=$dbtable->view("","0");
$list=$dt['data'];
/*MENGAMBIL DATA TAGIHAN PASIEN DISIMPAN DI $LIST*/

/*MENGAMBIL DATA KOTA, CETAK TANGGAL, PENCETAK ,NAMA RS DAN ALAMAT RS*/

$kota=getSettings($db, "cashier-simple-kwitansi-town", "");
$pencetak=$user->getNameOnly();
$autonomous=getSettings($db, "smis_autonomous_name", "SMIS");
$nama_rs=getSettings($db, "cashier-simple-kwitansi-rs", $autonomous);
$autonomous_address=getSettings($db, "smis_autonomous_address", "LOCALHOST");
$alamat_rs=getSettings($db, "cashier-simple-kwitansi-address", $autonomous_address);
$tgl_pulang=($px['tanggal_pulang']=="0000-00-00 00:00:00" || $px['tanggal_pulang']=="0000-00-00") ?date("Y-m-d H:i"):$px['tanggal_pulang'];
$tgl_masuk= $px['tanggal'];
$kelamin=$px['kelamin']=="1"?"Perempuan":"Laki-Laki";
$_URUTAN=getSettings($db, "cashier-simple-kwitansi-arrange-bayar-last-show", "id DESC");
$last_noreg_q="SELECT id,no_kwitansi FROM smis_ksr_bayar WHERE noreg_pasien='".$noreg."' AND prop!='del' ORDER BY ".$_URUTAN;
$last_row=$db->get_row($last_noreg_q);
$_NOMOR_KWITANSI=ArrayAdapter::format("only-digit8",$last_row->id);
if(getSettings($db,"cashier-simple-kwitansi-use-own-number","id")!="id"){
	$_NOMOR_KWITANSI=$last_row->no_kwitansi;
}
/*END MENGAMBIL DATA KOTA, CETAK TANGGAL, PENCETAK ,NAMA RS DAN ALAMAT RS*/


/*PEMBUATAN OBJECT CETAK MENGGUNAKAN TABLE PRINT*/
$tp=new TablePrint("cetak_kwitansi_bayangan");
$tp->setDefaultBootrapClass(false);
$tp->setMaxWidth(false);
$tp->setTableClass("cetak_kwitansi_bayangan");
//pembuatan title
$HEADER=$nama_rs."</br>".strtolower("<font id='cetak_kwitansi_bayangan_header'>".$alamat_rs."</font>") ;
$tp->addColumn($HEADER, 6, 1,NULL,NULL,"center bold");
$tp->commit("body");
//pembuatan front-header
$tp->addColumn("NAMA / PJ", 1, 1,NULL,NULL,"left ltop");
$tp->addColumn(" : ".$px['nama_pasien']." / ".$px['namapenanggungjawab']." / ".$kelamin, 2, 1,NULL,NULL,"left ltop ");
$tp->addColumn("No. Kwitansi", 2, 1,NULL,NULL,"left ltop f20 underlined");
$tp->addColumn( " : ".$_NOMOR_KWITANSI, 1, 1,NULL,NULL,"left ltop f20 bold");
$tp->commit("body");

$tp->addColumn("ALAMAT", 1, 1,NULL,NULL,"left");
$tp->addColumn(" : ".$px['alamat_pasien'], 2, 1,NULL,NULL,"left ");
$tp->addColumn("RUANGAN", 2, 1,NULL,NULL,"left");
$tp->addColumn(" : ".ArrayAdapter::format("unslug",($px['uri']*1==0?$px['jenislayanan']:$px['kamar_inap'])), 2, 1,NULL,NULL,"left ");
$tp->commit("body");

$tp->addColumn("NRM / NO. REG", 1, 1,NULL,NULL,"left");
$tp->addColumn(" : ".ArrayAdapter::format("only-digit8", $px['nrm'])." / ".ArrayAdapter::format("only-digit8", $px['id']), 2, 1,NULL,NULL,"left");
$tp->addColumn("TANGGAL MASUK", 2, 1,NULL,NULL,"");
$tp->addColumn( " <div class='ngedit' > ".ArrayAdapter::format("date d M Y", $tgl_masuk)."</div>", 1, 1,NULL,NULL,"left");
$tp->commit("body");

$tp->addColumn("JENIS PASIEN", 1, 1,NULL,NULL,"left");
$tp->addColumn(" : ".ArrayAdapter::format("unslug", $px['carabayar'] . ($px['nama_asuransi']!=""?" / ".$px['nama_asuransi']:"")  ), 2, 1,NULL,NULL,"left");
$tp->addColumn("TANGGAL KELUAR", 2, 1,NULL,NULL,"left");
$tp->addColumn( "<div class='ngedit' >".ArrayAdapter::format("date d M Y", $tgl_pulang)."</div>", 1, 1,NULL,NULL,"left");
$tp->commit("body");

$tp->addColumn("DPJP", 1, 1,NULL,NULL,"left lbottom");
$tp->addColumn(" : ".strtoupper($px['nama_dokter']), 5, 1,NULL,NULL,"left lbottom");
$tp->commit("body");

$tp->addSpace(6, 1);
$tp->commit("body");

//pembuatan header content
$tp->addColumn("RUANGAN", 1, 1,NULL,NULL,"bold");
$tp->addColumn("TANGGAL", 1, 1,NULL,NULL,"bold");
$tp->addColumn("TINDAKAN", 1, 1,NULL,NULL,"bold");
$tp->addColumn("HRG. SAT", 1, 1,NULL,NULL,"bold");
$tp->addColumn("JML", 1, 1,NULL,NULL,"bold");
$tp->addColumn("SUBTOTAL", 1, 1,NULL,NULL,"bold");
$tp->commit("body");
//pembuatan content
$TOTAL_TAGIHAN=0;
foreach($list as $x){
	$tp->addColumn("&#09;".ArrayAdapter::format("unslug", $x[$tampilan_ruangan]), 1, 1);
	$tp->addColumn( $x['tanggal'], 1, 1);
	$tp->addColumn(ArrayAdapter::format("unslug", $x['nama_tagihan']), 1, 1);
	$tp->addColumn(ArrayAdapter::format("money Rp.", $x['nilai_by']/$x['quantity']), 1, 1);
	$tp->addColumn(" x ". $x['quantity'], 1, 1);
	$tp->addColumn(ArrayAdapter::format("money Rp.", $x['nilai_by']), 1, 1);
	$tp->commit("body");
	$TOTAL_TAGIHAN+=($x['nilai_by']);
}

//pembuatan footer
loadLibrary("smis-libs-function-math");
$total_say=numbertell($TOTAL_TAGIHAN);
$tp->addColumn("TOTAL TAGIHAN", 5, 1,NULL,NULL,"bold");
$tp->addColumn(ArrayAdapter::format("money Rp.",$TOTAL_TAGIHAN), 1, 1,NULL,NULL,"bold  ltop");
$tp->commit("body");
//footer total tagihan

$tp->addColumn("&nbsp;",6, 1,NULL,NULL,"");
$tp->commit("body");
$tp->addColumn("PEMBAYARAN",6, 1,NULL,NULL,"bold");
$tp->commit("body");

$BAYAR_AKHIR=$TOTAL_TAGIHAN;//mestinya 0



/*HERE COMMAND*/
$total_say=numbertell($BAYAR_AKHIR);

$tp->addColumn("TERBILANG", 1, 1,NULL,NULL,"left  lbottom");
$tp->addColumn(": ".$total_say." Rupiah", 5, 1,NULL,NULL,"left lbottom bold");
$tp->commit("body");
$tp->addSpace(3, 1);
$tp->addColumn(" <div class='ngedit'>".$kota.", ".ArrayAdapter::format("date d M Y", date("Y-M-d"))." </div>", 3, 1,NULL,NULL,"center");
$tp->commit("body");
$tp->addSpace(3, 1);
$tp->addColumn("</br></br>", 3, 1,NULL,NULL,"center");
$tp->commit("body");
$tp->addSpace(3, 1);
$tp->addColumn($pencetak, 3, 1,NULL,NULL,"center");
$tp->commit("body");
/*END PEMBUATAN OBJECT CETAK*/
echo "<div id='cetak_kwitansi_tagihan_bayangan_area'  contenteditable='true'>".$tp->getHtml()."</div>";
echo addCSS("kasir/resource/css/kwitansi_tagihan_bayangan.css",false);
$font_size=getSettings($db, "cashier-simple-kwitansi-font-size", "12px");
$font_size_f20=getSettings($db, "cashier-simple-kwitansi-font-size-f20", "12px");
?>

<script type="text/javascript">
function kwitansi_tagihan_bayangan_print(){
	var html_content=$("#cetak_kwitansi_tagihan_bayangan_area").html();
	var name=$("#noreg_pasien").val();
	if(typeof kwitansi_simpan == 'function'){
        var data={
			nama_pasien:$("#nama_pasien").val(),
			noreg_pasien:$("#noreg_pasien").val(),
			nrm_pasien:$("#nrm_pasien").val(),
			noref:$("#noreg_pasien").val()+"-"+(new Date().toLocaleString()),
			jenis:"Simple Kwitansi",
			html:html_content,
			html_print:html_content,
        };        
        kwitansi_simpan(data);
    }else {
        smis_print_save(html_content, "kwitansi_tagihan_bayangan", name);
    }
}

function cek_kwitansi_tagihan_bayangan_print(){
		if(!is_selesai_semua()){
			var title="Peringatan Kesalahan !!!";
			var text="Kwitansi Tidak Dapat di cetak karena masih Terdapat Ruangan Yang Belum Memulangkan dari Ruangan </br>";
			text+="<table class='table table-bordered table-hover table-striped table-condensed' >"+$("#table_layanan_table").html()+"</table>";
			showWarning(title,text);
		}else{
			kwitansi_tagihan_bayangan_print();
		}
}
</script>
<style type="text/css">
	table#cetak_kwitansi_bayangan tr td.f20{font-size:<?php echo $font_size_f20; ?> !important;}
	table#cetak_kwitansi_bayangan tr td, table#cetak_kwitansi_bayangan tr th{font-size:<?php echo $font_size; ?> !important;}	
</style>
