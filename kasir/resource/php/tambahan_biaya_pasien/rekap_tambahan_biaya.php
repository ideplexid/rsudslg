<?php
/**
 * this used for viewing the recapitulation of
 * additional tax, for patient that temporary 
 * used for helping cashier administration control
 * add some cash bill that not yet inputed by user
 * 
 * @version     : 5.0.0
 * @since       : 02 Feb 2017
 * @author      : Nurul Huda
 * @copyright   : goblooge@gmail.com
 * @license     : LGPLv3
 * @database    : smis_ksr_tagihan
 * */
global $db;
$header=array ("No.","Tanggal","No Reg",'Nama',"NRM","Jenis",
            "Ruangan","Nama Tagihan",'Keterangan',"Nilai","Jumlah","Jaspel",'Total',"Operator");
$uitable = new Table ( $header, "", NULL, true );
$uitable->setAddButtonEnable(false);
$uitable->setPrintButtonEnable(false);
$uitable->setEditButtonEnable(false);
$uitable->setName ( "rekap_tambahan_biaya" );
if (isset ( $_POST ['command'] )) {
	$adapter = new SimpleAdapter();
    $adapter->setUseNumber(true,"No.","back.");
	$adapter->add ( "No Reg", "noreg_pasien","only-digit8");
	$adapter->add ( "NRM", "nrm_pasien","only-digit8");	
	$adapter->add ( "Nama", "nama_pasien");
    $adapter->add ( "Nama Tagihan", "nama_tagihan");
	$adapter->add ( "Ruangan", "ruangan","unslug");
	$adapter->add ( "Tagihan", "tagihan","money Rp.");
	$adapter->add ( "Jenis", "jenis_tagihan", "unslug" );
	$adapter->add ( "Nilai", "nilai", "money Rp." );
	$adapter->add ( "Operator", "operator" );
	$adapter->add ( "Keterangan", "keterangan" );
	$adapter->add ( "Jumlah", "jumlah" );
	$adapter->add ( "Total", "total" );
    $adapter->add ( "Jaspel", "jaspel","trivial_1_V_-" );
	$adapter->add ( "Tanggal", "tanggal","date d M Y" );
	
	$dbtable = new DBTable ( $db, "smis_ksr_tagihan" );
	$dbres = new DBResponder ( $dbtable, $uitable, $adapter );
	if($dbres->isView()){
		if($_POST['dari']!="")
			$dbtable->addCustomKriteria(null, " tanggal>='".$_POST['dari']."' ");
		if($_POST['sampai']!="")
			$dbtable->addCustomKriteria(null, " tanggal<'".$_POST['sampai']."' ");
        if($_POST['nama']!="")
			$dbtable->addCustomKriteria(null, " nama_pasien LIKE '%".$_POST['nama']."%' ");
        if($_POST['nrm']!="")
			$dbtable->addCustomKriteria(null, " nrm_pasien = '".$_POST['nrm']."' ");
		if($_POST['noreg']!="")
			$dbtable->addCustomKriteria(null, " noreg_pasien = '".$_POST['noreg']."' ");
		
	}
	$data = $dbres->command ( $_POST ['command'] );
	if($data!=NULL)
		echo json_encode ( $data );
	return;
}

$uitable->addModal ( "dari", "date", "Dari", "" );
$uitable->addModal ( "sampai", "date", "Sampai", "" );
$uitable->addModal ( "nama", "text", "Nama", "" );
$uitable->addModal ( "nrm", "text", "NRM", "" );
$uitable->addModal ( "noreg", "text", "No. Reg", "" );
$form=$uitable->getModal ()->getForm();

$cari=new Button("", "", "Search");
$cari->setIsButton(Button::$ICONIC_TEXT);
$cari->setIcon("fa fa-search");
$cari->setClass("btn-primary");
$cari->setAction("rekap_tambahan_biaya.view()");
$cari->addAtribute("data-content","Search...");
$cari->addAtribute("data-toggle","popover");



$form->addElement("",$cari);

echo $form->getHtml ();
echo $uitable->getHtml ();
echo addJS ( "framework/bootstrap/js/bootstrap-datepicker.js" );
echo addCSS ( "framework/bootstrap/css/datepicker.css" );
echo addJS ( "kasir/resource/js/rekap_tambahan_biaya.js" ,false);
?>
