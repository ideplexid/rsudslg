<?php 
global $db;
global $user;
require_once "smis-libs-class/MasterTemplate.php";
require_once "kasir/function/get_ruang_daftar.php";

$status = new OptionBuilder();
$status ->add("","0","1")
        ->add("Setuju","1","0")
        ->add("Tolak","-1","0");

$master = new MasterTemplate($db,"smis_ksr_diskon","kasir","persetujuan_diskon");
$master ->getUItable()
        ->setHeader(array("ID","Nama","NRM","No. Reg","Setuju","Nilai","Persen","Kepada","Operator","Pengecek"))
        ->addModal("id","hidden","","")
        ->addModal("setuju","select","Jenis",$status->getContent())
        ->addModal("nilai","money","Nilai","")
        ->addModal("persen","text","Persen (%)","");
$master ->getAdapter()
        ->add("ID","id","only-digit8")
        ->add("Nama","nama_pasien")
        ->add("NRM","nrm_pasien")
        ->add("No. Reg","noreg_pasien")
        ->add("Setuju","setuju","trivial_1_Setuju_0_Belum_Tolak")
        ->add("Nilai","nilai","money Rp.")
        ->add("Persen","persen")
        ->add("Kepada","pegawai")
        ->add("Operator","operator")
        ->add("Pengecek","pengecek");
        
if($master ->getDBResponder()->isSave()){
    if($_POST['setuju']!="0"){
        $master->getDBResponder()->addColumnFixValue("pengecek",$user->getNameOnly());
    }else{
        $master->getDBResponder()->addColumnFixValue("pengecek","");
    }
}

$master ->setModalTitle("Biaya Tambahan")
        ->initialize();        

?>