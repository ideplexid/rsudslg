<?php
global $db;
require_once 'kasir/class/table/TableEditBayangan.php';
require_once 'kasir/class/responder/EditBayanganResponder.php';
require_once 'smis-base/smis-include-service-consumer.php';

$pheader=array("Nama","NRM","Alamat");
$pasientable=new Table($pheader);
$pasientable->setName("px_urutan_kwitansi");
$pasientable->setModel(Table::$SELECT);
$adapter=new SimpleAdapter();
$adapter->add("Nama", "nama_pasien");
$adapter->add("NRM", "nrm","digit8");
$adapter->add("Alamat", "alamat_pasien");
$pasien=new ServiceResponder($db, $pasientable, $adapter, "get_registered");
$super=new SuperCommand();
$super->addResponder("px_urutan_kwitansi", $pasien);
$data=$super->initialize();
if($data!=null){
	echo $data;
	return;
}

$header=array ("ID","No Kwitansi",'Tanggal','Nilai','Keterangan',"Operator","Metode","Urutan");
$uitable = new Table( $header, "", NULL, true );
$uitable->setName ( "urutan_kwitansi" );
$uitable->setEditButtonEnable(false);
$uitable->setAddButtonEnable(false);
$uitable->setPrintButtonEnable(false);
$uitable->setReloadButtonEnable(false);
$uitable->setFooterVisible(false);

$up=new Button("","","");
$up->addClass("btn-inverse");
$up->setIcon(" fa fa-caret-square-o-up");
$up->setIsButton(Button::$ICONIC);
$uitable->addContentButton("up",$up);

$down=new Button("","","");
$down->addClass("btn-info");
$down->setIcon(" fa fa-caret-square-o-down");
$down->setIsButton(Button::$ICONIC);
$uitable->addContentButton("down",$down);

if (isset ( $_POST ['command'] )) {
	$dbtable=new DBTable($db, "smis_ksr_bayar");
	$dbtable->addCustomKriteria("noreg_pasien", "='".$_POST['noreg_pasien']."'");
    $dbtable->setFetchMethode(DBTable::$ARRAY_FETCH);
    $_URUTAN=getSettings($db, "cashier-simple-kwitansi-arrange-bayar", "id DESC");
    $dbtable->setOrder($_URUTAN);
    $dbtable->setShowAll(true);
    
	$adapter=new SimpleAdapter();
	$adapter->setUseNumber(true, "No.","back.");
	$adapter->add ( "ID", "id","only-digit8");
	$adapter->add ( "No Kwitansi", "no_kwitansi");
	$adapter->add ( "Metode", "metode","unslug");
	$adapter->add ( "Carabayar", "carabayar","unslug");
	$adapter->add ( "Tanggal", "waktu", "date d M Y H:i" );
	$adapter->add ( "Nilai", "nilai", "money Rp." );
	$adapter->add ( "Operator", "operator" );
	$adapter->add ( "Keterangan", "keterangan" );
    $adapter->add ( "Urutan", "urutan" );
	
	$bres=new DBResponder($dbtable, $uitable, $adapter);
    if($bres->isSave()){
        $bres->addUpSaveWarp("urutan",1);
        if($_POST['kondisi']=="down"){
            $bres->addColumnFixValue("urutan","urutan-1");
        }else{
            $bres->addColumnFixValue("urutan","urutan+1");
        }
    }
	$p=$bres->command($_POST['command']);
	echo json_encode($p);
	return;
}

$loadall=new Button("", "", "");
$loadall->setClass("btn btn-primary");
$loadall->setAction("urutan_kwitansi.view()");
$loadall->setIsButton(Button::$ICONIC);
$loadall->setIcon(" fa fa-circle-o");

$uitable->addModal ( "nama_pasien", "chooser-urutan_kwitansi-px_urutan_kwitansi-Pilih Pasien", "Nama", "" );
$uitable->addModal ( "noreg_pasien", "text", "No. Reg", "" );
$uitable->addModal ( "nrm_pasien", "text", "NRM Pasien", "" );
$form=$uitable->getModal()->getForm();
$form->addElement("", $loadall->getHtml());
$uitable->clearContent();

$load=new LoadingBar("rekap_urutan_kwitansi_bar", "");
$mload=new Modal("rekap_urutan_kwitansi_modal", "", "Processing...");
$mload	->addHTML($load->getHtml(),"after");

echo $form->getHtml ();
echo $uitable->getHtml ();
echo addJS ( "base-js/smis-base-loading.js");
echo addJS("kasir/resource/js/urutan_kwitansi.js",false);
?>