<?php 
global $db;
require_once "smis-libs-class/MasterTemplate.php";
require_once "kasir/function/get_ruang_daftar.php";

loadLibrary("smis-libs-function-medical");
$replace=medical_service();
$op=new OptionBuilder();
$op->add("","");
foreach($replace as $id=>$name){
	$op->add($name,$id);
}
$ruangan=getRuanganDaftar();
$ruangan[]=array("name"=>"","value"=>"");

$master=new MasterTemplate($db,"smis_ksr_tagihan_tambahan","kasir","master_tagihan_tambahan");
$master ->getUItable()
        ->setHeader(array("Nama","Ruangan","Jenis","Biaya","Jaspel","Debet","Kredit"))
        ->addModal("id","hidden","","")
        ->addModal("nama","text","Nama","")
        ->addModal("ruangan","select","Ruangan",$ruangan)
        ->addModal("jenis","select","Jenis",$op->getContent())
        ->addModal("jaspel","checkbox","Jaspel","")
        ->addModal("keterangan","textarea","Keterangan","")
        ->addModal("biaya","money","Biaya","")
        ->addModal("debet","text","Debet","")
        ->addModal("kredit","text","Kredit","");
$master ->getAdapter()
        ->add("Nama","nama")
        ->add("Ruangan","ruangan")
        ->add("Jenis","jenis")
        ->add("Debet","debet")
        ->add("Kredit","kredit")
        ->add("Jaspel","jaspel","trivial_1_x_")
        ->add("Biaya","biaya","money Rp.");
$master->setModalTitle("Biaya Tambahan");
$master->initialize();        

?>