<?php 
global $db;
require_once 'smis-base/smis-include-service-consumer.php';
$uitable=new Table(array("No.",'Tanggal',"No Reg","NRM","Nama","Alamat","RI/RJ","Layanan"),"Pasien Rawat Jalan yang Seharusnya Sudah Dipulangkan");
$uitable->setName("pasien_lewat_tanggal");
$uitable->setFooterVisible(false);
$uitable->setAction(false);
if(isset($_POST['command'])){
    global $user;
	$adapter=new SimpleAdapter();
	$adapter->setUseNumber(true,"No.","back.");
	$adapter->add("Tanggal", "tanggal","date d M Y H:i");
	$adapter->add("Nama", "nama_pasien");
	$adapter->add("No Reg", "id","digit8");
	$adapter->add("NRM", "nrm","digit8");
	$adapter->add("Alamat", "alamat_pasien");
    $adapter->add("RI/RJ", "uri","trivial_0_URJ_URI");
	$adapter->add("Layanan", "jenislayanan","unslug");
	$dbres=new ServiceResponder($db, $uitable, $adapter, "get_outway");
    $dbres->addData("username",$user->getUsername());
	$p=$dbres->command($_POST['command']);
	echo json_encode($p);
	return;
}

$keluar=new OptionBuilder();
$keluar->add("");
$keluar->add("Dipulangkan Admin");
$keluar->add("Tidak Datang");
$keluar->add("Dipulangkan Hidup");
$keluar->add("Dipulangkan Mati <=48 Jam");
$keluar->add("Dipulangkan Mati >48 Jam");
$keluar->add("Pulang Paksa");
$keluar->add("Kabur");
$keluar->add("Rawat Inap");
$keluar->add("Pindah Kamar");
$keluar->add("Dirujuk Ke Poli Lain","Rujuk Poli Lain");
$keluar->add("Dirujuk Ke RS Lain","Rujuk RS Lain");
$uitable->addModal("carapulang", "select", "Pulang", $keluar->getContent());
$form=$uitable->getModal()->getForm();

$btn=new Button("", "", "Clear");
$btn->addClass("btn-primary");
$btn->setIsButton(Button::$ICONIC);
$btn->setIcon("fa fa-rocket");
$btn->setAction("pasien_lewat_tanggal.clear()");
$form->addElement("", $btn->getHtml());

echo $form->getHtml();
echo addJS ( "framework/smis/js/table_action.js" );
echo $uitable->getHtml();


?>
<script type="text/javascript">
var pasien_lewat_tanggal;
$(document).ready(function(){

	pasien_lewat_tanggal=new TableAction("pasien_lewat_tanggal","kasir","pasien_lewat_tanggal",new Array());
	pasien_lewat_tanggal.view();
	pasien_lewat_tanggal.clear=function(){
		var d=this.getViewData();
		d['super_command']="clear";
		d['carapulang']=$("#pasien_lewat_tanggal_carapulang").val();
		var self=this;
		showLoading();
		$.post("",d,function(res){
			var json=getContent(res);
			if(json==null) {
				
			}else{
				$("#"+self.prefix+"_list").html(json.list);
				$("#"+self.prefix+"_pagination").html(json.pagination);	
			}
			self.afterview(json);
			dismissLoading();
		});
	};
	
});

</script>