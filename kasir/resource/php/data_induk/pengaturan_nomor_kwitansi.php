<?php 

require_once 'smis-libs-class/MasterTemplate.php';
global $db;
show_error();
$mahasiswa=new MasterTemplate($db, "smis_adm_settings", "kasir", "pengaturan_nomor_kwitansi");
$mahasiswa->getDBtable()->addCustomKriteria("name ", " LIKE 'last-cashier-kwitansi-%' ");
$uitable=$mahasiswa->getUItable();
$header=array("ID","Nama","Value");
$uitable->setHeader($header);
$uitable->addModal("id", "hidden", "", "")
		->addModal("name", "text", "Nama", "","n",NULL,false)
		->addModal("value","text", "Value", "");
$adapter=$mahasiswa->getAdapter();
$adapter->add("ID", "id","digit8")
		->add("Nama", "name")
		->add("Value","value");
$mahasiswa->setModalTitle("Pengaturan Nomor Kwitansi");
$modal=$mahasiswa->getModal();
$modal->addFooter(new HTML("",""," <div style='float:left;'> Pastikan Namanya Berawalan : <strong>last-cashier-kwitansi-% </strong></div>"));
$mahasiswa->initialize();
?>