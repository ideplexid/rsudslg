<?php
$header=array ('Tanggal',"Nomor","Petugas",'Pasien',"NRM",'Keterangan' );
$uitable = new Table ( $header, "Kwitansi", NULL, true );
$btn = new Button ( "", "", "" );
$btn->setIsButton ( Button::$ICONIC );
$btn->setIcon ( "fa fa-print" );
$btn->setClass("btn-primary");
$uitable->addContentButton ( "show_kwitansi", $btn );
$uitable->setName ( "kwitansi" );
$uitable->setAddButtonEnable ( false );
$uitable->setEditButtonEnable ( false );
$uitable->setDelButtonEnable ( false );
$uitable->setReloadButtonEnable ( false );
if (isset ( $_POST ['command'] )) {
	$adapter = new SimpleAdapter ();
	$adapter->add ( "Tanggal", "tanggal", "date d M Y" );
	$adapter->add ( "Pasien", "nama_pasien" );
	$adapter->add ( "NRM", "nrm_pasien", "digit6" );
	$adapter->add ( "No Reg", "noreg_pasien", "digit6" );
	$adapter->add ( "Keterangan", "jenis" );
	$adapter->add ( "Petugas", "petugas" );
	$adapter->add ( "Nomor", "noref" );
	$dbtable = new DBTable ( $db, "smis_ksr_kwitansi" );
	$dbtable->setFetchMethode ( DBTable::$ARRAY_FETCH );
	$dbtable->setOrder ( "id DESC " );
	$dbres = new DBResponder ( $dbtable, $uitable, $adapter );
	if ($dbres->is ( "save" )) {
		global $user;
		$dbres->addColumnFixValue ( "petugas", $user->getNameOnly () );
		$dbres->addColumnFixValue ( "tanggal", date ( "Y-m-d" ) );
	}
	$data = $dbres->command ( $_POST ['command'] );
	echo json_encode ( $data );
	return;
}
echo $uitable->getHtml ();
echo addJS  ( "framework/smis/js/table_action.js" );
echo addJS  ( "kasir/resource/js/kwitansi.js",false );
echo addCSS ( "kasir/resource/css/kwitansi.css",false );

?>