<?php
/**
 * this is handling about element where used for patient management
 * @author goblooge
 *
 */
$uitable = new Table ( array (
		'Nama',
		'No Rekening',
		'Bank' 
), "Kasir : Bank", NULL, true );
$uitable->setName ( "mbank" );
if (isset ( $_POST ['command'] )) {
	$adapter = new SimpleAdapter ();
	$adapter->add ( "Nama", "nama" );
	$adapter->add ( "No Rekening", "rekening" );
	$adapter->add ( "Bank", "bank" );
	$dbtable = new DBTable ( $db, "smis_ksr_bank" );
	$dbtable->setFetchMethode ( DBTable::$ARRAY_FETCH );
	$dbres = new DBResponder ( $dbtable, $uitable, $adapter );
	$data = $dbres->command ( $_POST ['command'] );
	echo json_encode ( $data );
	return;
}
$uitable->addModal ( "id", "hidden", "", "" );
$uitable->addModal ( "nama", "text", "Nama", "" );
$uitable->addModal ( "rekening", "text", "Rekening", "" );
$uitable->addModal ( "bank", "text", "Bank", "" );
$modal = $uitable->getModal ();
$modal->setTitle ( "Bank" );

echo $uitable->getHtml ();
echo $modal->getHtml ();
echo addJS ( "framework/smis/js/table_action.js" );

?>
<script type="text/javascript">
var mbank;
//var employee;
$(document).ready(function(){
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
	var column=new Array('id','nama','rekening','bank');
	mbank=new TableAction("mbank","kasir","mbank",column);
	mbank.view();
	
});
</script>
