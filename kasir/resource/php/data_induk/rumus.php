<?php 
    global $db;	
	require_once 'smis-libs-class/MasterTemplate.php';
    $mnp=new MasterTemplate($db, "smis_ksr_rumus", "kasir", "rumus");
	$uitable=$mnp->getUItable();
    $head="<tr>
                <td colspan='4'>
                    X = Plafon Kelas Pasien Sesuai Ina CBG's.
                    Y = Plafon Kelas 1 Sesuai Ina CBG's.
                    T = Biaya Rumah Sakit.
                </td>
                <td colspan='4'>
                    U = Rumus Potongan.
                    Z = Tagihan Pasien / Yang Harus Dibayar Pasien.
                </td>
           </tr>";
    
    $uitable->addHeader("before",$head);
    $uitable->setPrintButtonEnable(false);
	$uitable->addHeaderElement("No.")
            ->addHeaderElement("Nama")
			->addHeaderElement("Rumus Biaya (T)")
            ->addHeaderElement("Rumus Potongan (U)")
			->addHeaderElement("Rumus Tagihan (Z)")
			->addHeaderElement("Keterangan");
	$uitable->addModal("id", "hidden", "", "")
			->addModal("nama", "text", "Nama", "")
			->addModal("rumus_biaya", "text", "Rumus Biaya (T)", "")
            ->addModal("rumus_potongan", "text", "Rumus Potongan (U)", "")
			->addModal("rumus_tagihan", "text", "Rumus Tagihan (Z)", "")
			->addModal("keterangan", "textarea", "Keterangan", "");
	$adapter=$mnp->getAdapter();
    $adapter->setUseNumber(true,"No.","back.");
	$adapter->add("Nama", "nama")
			->add("Rumus Biaya (T)", "rumus_biaya")
            ->add("Rumus Potongan (U)", "rumus_potongan")
			->add("Rumus Tagihan (Z)", "rumus_tagihan")
			->add("Keterangan", "keterangan");
	$mnp->getModal()->setTitle("Rumus Tagihan Pasien");
    $mnp->getModal()->setComponentSize(Modal::$MEDIUM);
	$mnp->initialize();
?>