<?php 
	require_once 'smis-libs-class/MasterTemplate.php';
	global $db;	
	if(isset($_POST['super_command']) && $_POST['super_command']=="backup"){
		$query="UPDATE smis_ksr_nonpasien SET harga_lama = harga";
		$db->query($query);
		$response=new ResponsePackage();
		$response->setStatus(ResponsePackage::$STATUS_OK);
		$response->setAlertContent("Berhasil","Harga Berhasil di Backup");
		echo json_encode($response->getPackage());
		return;
	}else if(isset($_POST['super_command']) && $_POST['super_command']=="apply"){
		$query="UPDATE smis_ksr_nonpasien SET harga = rencana_harga";
		$db->query($query);
		$response=new ResponsePackage();
		$response->setStatus(ResponsePackage::$STATUS_OK);
		$response->setAlertContent("Berhasil","Harga Berhasil di Terapkan");
		echo json_encode($response->getPackage());
		return;
	}else if(isset($_POST['super_command']) && $_POST['super_command']=="restore"){
		$query="UPDATE smis_ksr_nonpasien SET harga = harga_lama";
		$db->query($query);
		$response=new ResponsePackage();
		$response->setStatus(ResponsePackage::$STATUS_OK);
		$response->setAlertContent("Berhasil","Harga Berhasil di Kembalikan");
		echo json_encode($response->getPackage());
		return;
	}
	
	$restore = new Button("", "","Restore");
	$restore ->setIsButton(Button::$ICONIC_TEXT)
             ->setIcon(" fa fa-refresh")
             ->setClass("btn-primary")
             ->setAction("master_non_pasien.restore()");
	
	$backup = new Button("", "","Backup");
	$backup ->setIsButton(Button::$ICONIC_TEXT)
            ->setIcon(" fa fa-database")
            ->setClass("btn-primary")
            ->setAction("master_non_pasien.backup()");
	
	$apply = new Button("", "","Terapkan");
	$apply ->setIsButton(Button::$ICONIC_TEXT)
           ->setIcon(" fa fa-stop")
           ->setClass("btn-primary")
           ->setAction("master_non_pasien.apply()");
	
	$mnp = new MasterTemplate($db, "smis_ksr_nonpasien", "kasir", "master_non_pasien");
	$mnp ->setAutofocusOnMultiple("nama");
	$mnp ->setAutofocus(true);
	$mnp ->setNextEnter(true);
	$mnp ->setMultipleInput(true);
	$uitable = $mnp->getUItable();
	$uitable ->setPrintButtonEnable(false);
	$uitable ->setReloadButtonEnable(false);
	$uitable ->addFooterButton($backup);
	$uitable ->addFooterButton($restore);
	$uitable ->addFooterButton($apply);
	$uitable ->addHeaderElement("Nama")
			 ->addHeaderElement("Harga Lama")
			 ->addHeaderElement("Harga")
			 ->addHeaderElement("Rencana Harga")
			 ->addHeaderElement("Keterangan")
             ->addHeaderElement("Debet")
             ->addHeaderElement("Kredit");
	$uitable ->addModal("id", "hidden", "", "")
			 ->addModal("nama", "text", "Nama", "","y",NULL,false,NULL,true,"harga_lama")
			 ->addModal("harga_lama", "money", "Harga Lama", "","y",NULL,true,NULL,false,"harga")
			 ->addModal("harga", "money", "Harga", "","y",NULL,false,NULL,false,"rencana_harga")
			 ->addModal("rencana_harga", "money", "Rencana Harga", "","y",NULL,false,NULL,false,"keterangan")
			 ->addModal("keterangan", "textarea", "Keterangan", "","y",NULL,false,NULL,false,"")
             ->addModal("debet", "text", "Debet", "","y",NULL,false,NULL,true,"debet")
			 ->addModal("kredit", "text", "Kredit", "","y",NULL,false,NULL,true,"save");
	$adapter = $mnp->getAdapter();
	$adapter ->add("Nama", "nama")
			 ->add("Harga Lama", "harga_lama","money Rp.")
			 ->add("Harga", "harga","money Rp.")
			 ->add("Rencana Harga", "rencana_harga","money Rp.")
			 ->add("Keterangan", "keterangan")
             ->add("Debet", "debet")
             ->add("Kredit", "kredit");
	$mnp ->getModal()
         ->setTitle("Pembayaran Non Pasien");
	$mnp ->addResouce("js","kasir/resource/js/master_non_pasien.js");
	$mnp ->initialize();
?>