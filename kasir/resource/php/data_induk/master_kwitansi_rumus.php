<?php 
    global $db;	
	require_once 'smis-libs-class/MasterTemplate.php';
    $mnp=new MasterTemplate($db, "smis_ksr_tagihan_rumus", "kasir", "master_kwitansi_rumus");
	$uitable=$mnp->getUItable();
    $uitable->setPrintButtonEnable(false);
	$uitable->setAddButtonEnable(false);
	$uitable->setEditButtonEnable(false);
    $uitable->setDelButtonEnable(false);
	$uitable->setReloadButtonEnable(false);
	
    $btn=new Button("","","");
    $btn->setIsButton(Button::$ICONIC);
    $btn->setClass("btn-inverse");
    $btn->setIcon("fa fa-crosshairs");
    
    $uitable->addHeaderElement("No.")
            ->addHeaderElement("Nama Pasien")
			->addHeaderElement("NRM Pasien")
            ->addHeaderElement("Noreg Pasien")
			->addHeaderElement("Rumus")
            ->addHeaderElement("Plafon INCBG")
			->addHeaderElement("Plafon Kelas 1");
    $uitable->addContentButton("history",$btn);
	$adapter=$mnp->getAdapter();
    $adapter->setUseNumber(true,"No.","back.");
	$adapter->add("Nama Pasien", "nama_pasien")
			->add("NRM Pasien", "nrm_pasien")
            ->add("Noreg Pasien", "noreg_pasien")
			->add("Rumus", "nama_rumus")
			->add("Plafon INCBG", "tarif_plafon_inacbg","money Rp.")
            ->add("Plafon Kelas 1", "tarif_plafon_kelas1","money Rp.");
    $mnp->addResouce("js","kasir/resource/js/master_kwitansi_rumus.js");
    $mnp->addResouce("js","smis-base-js/smis-base-fetreefy-json.js");
	$mnp->initialize();
?>