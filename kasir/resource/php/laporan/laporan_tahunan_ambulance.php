<?php
global $db;
$header=array ("Tahun",'Bulan',"Jumlah");
$uitable = new Table ( $header, "", NULL, false );
$head="<tr><td colspan='20' style='text-align:center'><strong >Laporan Tahunan Ambulance</strong></td></tr>";
$uitable->addHeader("before", $head);
$head="<tr><td colspan='20' style='text-align:center'><strong id='tgl_laporan_tahunan_ambulance_print'></strong></td></tr>";
$uitable->addHeader("before", $head);
$uitable->setFooterVisible(false);
$uitable->setName ( "laporan_tahunan_ambulance" );
if (isset ( $_POST ['command'] )) {
	require_once 'kasir/class/adapter/LapCashAdapter.php';
	$adapter = new SummaryAdapter();
	$adapter->addSummary("Jumlah", "jumlah","money Rp.");
	$adapter->addFixValue("Tahun", "<strong>Total</strong>");
	$adapter->add ( "Tahun", "tahun");
	$adapter->add ( "Bulan", "bulan");
	$adapter->add ( "Jumlah", "jumlah","money Rp.");
	
	$dbtable = new DBTable ( $db, "smis_ksr_tagihan" );
	$dbtable->setFetchMethode ( DBTable::$ARRAY_FETCH );
	$dbres = new DBResponder( $dbtable, $uitable, $adapter );
	if($dbres->isView()){		
		$qv="SELECT SUM(total) as jumlah, YEAR(tanggal) as tahun, MONTH(tanggal) as bt, DATE_FORMAT(tanggal,'%M') as bulan FROM smis_ksr_tagihan ";
		$qc="SELECT * FROM smis_ksr_tagihan";
		$dbtable->setPreferredQuery(true, $qv, $qc);
		$dbtable->setUseWhereforView(true);
		$dbtable->setGroupBy(true, " tahun, bt ");
		$dbtable->setOrder(" tahun ASC, bt ASC ");		
		$dbtable->setShowAll(true);		
		$dbtable->addCustomKriteria(null, " jenis_tagihan>='ambulan' ");
		if($_POST['dari']!="")
			$dbtable->addCustomKriteria(null, " tanggal>='".$_POST['dari']."' ");
		if($_POST['sampai']!="")
			$dbtable->addCustomKriteria(null, " tanggal<'".$_POST['sampai']."' ");		
	}
	$data = $dbres->command ( $_POST ['command'] );
	if($data!=NULL)
		echo json_encode ( $data );
	return;
}

$uitable->addModal ( "dari", "datetime", "Dari", "" );
$uitable->addModal ( "sampai", "datetime", "Sampai", "" );
$form=$uitable->getModal ()->getForm();

$cari=new Button("", "", "Search");
$cari->setIsButton(Button::$ICONIC);
$cari->setIcon("fa fa-search");
$cari->setClass("btn-primary");
$cari->setAction("laporan_tahunan_ambulance.view()");
$cari->addAtribute("data-content","Search...");
$cari->addAtribute("data-toggle","popover");

$excel=new Button("", "", "Search");
$excel->setIsButton(Button::$ICONIC);
$excel->setIcon("fa fa-file-excel-o");
$excel->setClass("btn-primary");
$excel->setAction("laporan_tahunan_ambulance.excel()");
$excel->addAtribute("data-content","Print Excel");
$excel->addAtribute("data-toggle","popover");

$print=new Button("", "", "Search");
$print->setIsButton(Button::$ICONIC);
$print->setIcon("fa fa-print");
$print->setClass("btn-primary");
$print->setAction("laporan_tahunan_ambulance.print()");
$print->addAtribute("data-content","Print ");
$print->addAtribute("data-toggle","popover");

$form->addElement("",$cari);
$form->addElement("",$print);
$form->addElement("",$excel);


echo $form->getHtml ();
echo $uitable->getHtml ();
echo addJS ( "framework/bootstrap/js/bootstrap-datetimepicker.js" );
echo addCSS ( "framework/bootstrap/css/bootstrap-datetimepicker.css" );

?>

<script type="text/javascript">
	var laporan_tahunan_ambulance;
	//var employee;
	$(document).ready(function(){
		$(".mydatetime").datetimepicker({minuteStep:1});
		$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
		var column=new Array('dari','sampai');
		laporan_tahunan_ambulance=new TableAction("laporan_tahunan_ambulance","kasir","laporan_tahunan_ambulance",column);
		laporan_tahunan_ambulance.getRegulerData=function(){
			var reg_data={	
					page:this.page,
					action:this.action,
					super_command:this.super_command,
					prototype_name:this.prototype_name,
					prototype_slug:this.prototype_slug,
					dari:$("#"+this.prefix+"_dari").val(),
					sampai:$("#"+this.prefix+"_sampai").val()
					};
			var dr=getFormattedTime(reg_data['dari']);
			var sp=getFormattedTime(reg_data['sampai']);
			var hsl=dr+" - "+sp;
			$("#tgl_laporan_tahunan_ambulance_print").html(hsl);
			return reg_data;
		};
	});
	</script>