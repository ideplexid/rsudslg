<?php
/**
 * this used for viewing the history of payment
 * this page automatically filter the bank payment
 * 
 * @version     : 5.0.0
 * @since       : 14 May 2015
 * @author      : Nurul Huda
 * @copyright   : goblooge@gmail.com
 * @license     : LGPLv3
 * @database    : smis_ksr_bayar
 * */
global $db;
$header=array ("ID","No Kwitansi",'Tanggal',"Ruangan","Carabayar","Nama","Noreg","NRM",'Nilai',"Bank","Tambahan Bank","Tambahan Cash",'Keterangan',"Operator","Asal Data" );

$uitable = new Table ( $header, "", NULL, true );
$head="<tr><td colspan='20' style='text-align:center'><strong >Laporan Pembayaran Bank <span id='lap_bank_jenisx'></span> </strong></td></tr>";
$uitable->addHeader("before", $head);
$head="<tr><td colspan='20' style='text-align:center'><strong id='tgl_lap_bank_print'></strong></td></tr>";
$uitable->addHeader("before", $head);
$uitable->setFooterVisible(false);
$uitable->setName ( "lap_bank" );

$uitable->setEditButtonEnable(false);
$uitable->setAddButtonEnable(false);
$uitable->setDelButtonEnable(false);

$button = new Button("","bank_push_akunting","Post");
$button->setClass("btn-primary");
$button->setIcon("fa fa-upload");
$button->setIsButton(Button::$ICONIC_TEXT);
$button->setAction("lap_bank.post_to_all()");
$uitable->addContentButton("push_to_accounting",$button);

if (isset ( $_POST ['command'] )) {
	require_once 'kasir/class/adapter/LapBankAdapter.php';
	require_once 'kasir/class/responder/LaporanBankResponder.php';
	$adapter = new LapBankAdapter ();
	$adapter->add ( "ID", "id","only-digit8");
	$adapter->add ( "No Kwitansi", "no_kwitansi");	
	$adapter->add ( "Nama", "nama_pasien");
	$adapter->add ( "NRM", "nrm_pasien","digit6");
	$adapter->add ( "Noreg", "noreg_pasien","digit6");		
	//$adapter->add ( "Ruangan", "ruangan","unslug");
	$adapter->add ( "Carabayar", "carabayar","unslug");
	$adapter->add ( "Tanggal", "waktu", "date d M Y H:i" );
	$adapter->add ( "Nilai", "nilai", "money Rp." );
	$adapter->add ( "Operator", "operator" );
	$adapter->add ( "Keterangan", "keterangan" );
	$adapter->add ( "Asal Data", "origin","unslug");
	
	$dbtable = new DBTable ( $db, "smis_ksr_bayar" );
	$dbtable->setFetchMethode ( DBTable::$ARRAY_FETCH );
	$dbtable->addCustomKriteria ( "metode", " LIKE 'bank%'" );	
	$dbres = new LaporanBankResponder ( $dbtable, $uitable, $adapter );
	if($dbres->isView() || $_POST['command']=="excel"){
		if($_POST['dari_wkt']!="") 		$dbtable->addCustomKriteria(null,"(waktu>='".$_POST['dari_wkt']."' )");
		if($_POST['sampai_wkt']!="") 	$dbtable->addCustomKriteria(null,"(waktu<'".$_POST['sampai_wkt']."' )");
		if($_POST['dari_id']!="") 		$dbtable->addCustomKriteria(null,"(id>='".$_POST['dari_id']."' )");
		if($_POST['sampai_id']!="") 	$dbtable->addCustomKriteria(null,"(id<'".$_POST['sampai_id']."' )");
		if($_POST['dari_nokwi']!="") 	$dbtable->addCustomKriteria(null,"(no_kwitansi>='".$_POST['dari_nokwi']."' )");
		if($_POST['sampai_nokwi']!="") 	$dbtable->addCustomKriteria(null,"(no_kwitansi<'".$_POST['sampai_nokwi']."')");
		if($_POST['nrm']!="") 	        $dbtable->addCustomKriteria(null,"(nrm_pasien = '".$_POST['nrm']."' )");
		if($_POST['nama']!="") 	        $dbtable->addCustomKriteria(null,"(nama_pasien LIKE '%".$_POST['nama']."%')");
		if($_POST['filter_origin']!="")	$dbtable->addCustomKriteria("origin", " ='".$_POST['filter_origin']."' ");
        if($_POST['operator']!="")		$dbtable->addCustomKriteria("operator", " ='".$_POST['operator']."' ");
		if($_POST['jenis']!="") 	   {
			if(trim($_POST['jenis'])=="Rawat Inap"){
				$dbtable->addCustomKriteria(null,"(urji LIKE '%".$_POST['jenis']."%')");
				$dbtable->addCustomKriteria(null,"(metode NOT LIKE '%_resep')");
				$dbtable->addCustomKriteria(null,"(metode NOT LIKE '%_np')");
			}else if(trim($_POST['jenis'])=="Rawat Jalan - Non IGD"){
				$dbtable->addCustomKriteria(null,"(urji LIKE '%Rawat Jalan%')");
				$dbtable->addCustomKriteria(null,"(ruangan NOT LIKE '%gawat_darurat' AND ruangan NOT LIKE '%igd%')");
				$dbtable->addCustomKriteria(null,"(metode NOT LIKE '%_resep')");
				$dbtable->addCustomKriteria(null,"(metode NOT LIKE '%_np')");
			}else if(trim($_POST['jenis'])=="Rawat Jalan - IGD"){
				$dbtable->addCustomKriteria(null,"(urji LIKE '%Rawat Jalan%')");
				$dbtable->addCustomKriteria(null,"(ruangan LIKE '%gawat_darurat' OR ruangan LIKE '%igd%')");
				$dbtable->addCustomKriteria(null,"(metode NOT LIKE '%_resep')");
				$dbtable->addCustomKriteria(null,"(metode NOT LIKE '%_np')");
			}else if(trim($_POST['jenis'])=="Resep Non KIUP"){
				$dbtable->addCustomKriteria(null,"(metode LIKE '%_resep')");
			}else if(trim($_POST['jenis'])=="Non Pasien"){
				$dbtable->addCustomKriteria(null,"(metode LIKE '%_np')");
			}
		}
		$dbtable->setShowAll(true);
	}
	$data = $dbres->command ( $_POST ['command'] );
	if($data!=NULL)
		echo json_encode ( $data );
	return;
}

$query  = "SELECT DISTINCT origin FROM smis_ksr_bayar WHERE prop!='del' ";
$origin = new OptionBuilder();
$origin ->add(" - Semua - ","",1);
$res	= $db->get_result($query);
foreach($res as $x){
    if($x->origin==""){
        continue;
    }
    $origin->add(ArrayAdapter::slugFormat("unslug",$x->origin),$x->origin);
}

$query    = "SELECT DISTINCT operator FROM smis_ksr_bayar WHERE prop!='del' ";
$operator = new OptionBuilder();
$operator ->add(" - Semua - ","",1);
$res	  = $db->get_result($query);
foreach($res as $x){
    if($x->operator==""){
        continue;
    }
    $operator->add(ArrayAdapter::slugFormat("unslug",$x->operator),$x->operator);
}

$jenis   = new OptionBuilder();
$jenis   ->add("","",1)
	     ->add("Rawat Jalan - IGD","Rawat Jalan - IGD")
	     ->add("Rawat Jalan - Non IGD","Rawat Jalan - Non IGD")
	     ->add("Rawat Inap","Rawat Inap")
		 ->add("Resep Non KIUP","Resep Non KIUP")
	     ->add("Non Pasien","Non Pasien");
$uitable ->addModal("dari_wkt", "datetime", "Dari Waktu", "" )
		 ->addModal("sampai_wkt", "datetime", "Sampai Waktu", "" )
		 ->addModal("dari_id", "text", "Dari ID", "","n","numeric" )
		 ->addModal("sampai_id", "text", "Sampai ID","","n","numeric")
		 ->addModal("dari_nokwi", "text", "Dari No.KW", "","n","numeric" )
		 ->addModal("sampai_nokwi", "text", "Sampai No.KW","","n","numeric")
		 ->addModal("jenis", "select", "Jenis",$jenis->getContent(),"y","")
		 ->addModal("nrm", "text", "NRM", "","n","numeric" )
		 ->addModal("nama", "text", "Nama","","n")
		 ->addModal("filter_origin", "select", "Asal Data",$origin->getContent() )
         ->addModal("operator", "select", "Operator",$operator->getContent() )
         ->addModal ( "kriteria", "text", "Pencarian","" );
$form=$uitable->getModal ()->getForm();

$cari=new Button("", "", "Search");
$cari->setIsButton(Button::$ICONIC);
$cari->setIcon("fa fa-search");
$cari->setClass("btn-primary");
$cari->setAction("lap_bank.view()");
$cari->addAtribute("data-content","Search...");
$cari->addAtribute("data-toggle","popover");

$excel=new Button("", "", "Search");
$excel->setIsButton(Button::$ICONIC);
$excel->setIcon("fa fa-file-excel-o");
$excel->setClass("btn-primary");
$excel->setAction("lap_bank.excel()");
$excel->addAtribute("data-content","Print Excel");
$excel->addAtribute("data-toggle","popover");

$print=new Button("", "", "Search");
$print->setIsButton(Button::$ICONIC);
$print->setIcon("fa fa-print");
$print->setClass("btn-primary");
$print->setAction("lap_bank.print()");
$print->addAtribute("data-content","Print ");
$print->addAtribute("data-toggle","popover");

$form->addElement("",$cari);
$form->addElement("",$print);
$form->addElement("",$excel);
$form->addElement("",$button);

echo $form->getHtml ();
echo $uitable->getHtml ();
echo addJS ( "framework/bootstrap/js/bootstrap-datetimepicker.js" );
echo addCSS ( "framework/bootstrap/css/bootstrap-datetimepicker.css" );
echo addJS ( "kasir/resource/js/lap_bank.js" ,false);
?>
