<?php 

/**
 * this sistem used to resume
 * all income that incoming (even or not)
 * in given timeframe
 * 
 * @author      : Nurul Huda
 * @copyright   : goblooge@gmail.com
 * @since       : 17 Aug 2016
 * @database    : smis_ksr_bayar
 * @service     : get_piutang
 * @version     : 1.6.0
 * @license     : LGPLv3
 * */

global $db;
global $user;
require_once 'smis-base/smis-include-service-consumer.php';
if(isset($_POST['super_command']) && $_POST['super_command']=="total") {
	require_once 'smis-libs-class/ServiceProviderList.php';
	$dbtable=new DBTable($db, "smis_ksr_piutang");
	$dbtable->truncate();
	$serv=new ServiceProviderList($db, "get_piutang");
	$serv->execute();
	$data=$serv->getContent();
	$res=new ResponsePackage();
	$res->setStatus(ResponsePackage::$STATUS_OK);
	$res->setContent($data);
	echo json_encode($res->getPackage());
	return;
}


if(isset($_POST['super_command']) && $_POST['super_command']=="limit") {
	$entity=$_POST['entity'];
	$data=array();
	$data['awal']=$_POST['dari'];
	$data['akhir']=$_POST['sampai'];
    if(isset($_POST['origin']) && $_POST['origin']!=""){
        $data['origin']=$_POST['origin'];
    }
	$response=new ServiceConsumer($db, "get_piutang",$data,$entity);
	$response->setMode(ServiceConsumer::$SINGLE_MODE);
	$response->execute();
	$list=$response->getContent();
	$dbtable=new DBTable($db, "smis_ksr_piutang");
	foreach($list as $x){
		if($x['nilai']!=null && $x['nilai']!='0' && $x['nilai']!=0){
			$d['layanan']=$x['layanan'];
			$d['nilai']=$x['nilai'];
			$d['ruangan']=$x['ruangan'];
			$d['urjip']=$x['urjip'];
			$dbtable->insert($d);
		}
	}
	$res=new ResponsePackage();
	$res->setStatus(ResponsePackage::$STATUS_OK);
	$res->setContent($list);
	echo json_encode($res->getPackage());
	return;
}

$header=array("Ruangan","Radiology","Fisiotherapy","Laboratory","Darah","Ambulan","Gizi","Tindakan Perawat","Tindakan Dokter",
		"Konsul","Visite","Periksa","Bed","Penjualan Resep","Return Resep","VK","Operasi","Recovery Room",
		"Alat Obat","Bronchoscopy","Spirometry","Audiometry","Endoscopy","EKG","Administrasi","Karcis",
		"Oksigen Central","Oksigen Manual","Total");
$uitable=new Table($header);
$uitable->setName("laporan_piutang_per_jenis")
		->setActionEnable(false)
		->setFooterVisible(false);

if(isset($_POST['command']) && $_POST['command']!=""){
	$dbtable=new DBTable($db, "smis_vpiutang");
	$dbtable->setOrder(" ruangan ASC");
	$dbtable->setShowAll(true);
	
	$adapter=new SummaryAdapter();
	$adapter->addFixValue("Ruangan", "Total");
	$adapter->setRemoveZeroEnable(true);
	$adapter->addSummary("Radiology", "radiology","nonzero-money Rp.");
	$adapter->addSummary("Laboratory", "laboratory","nonzero-money Rp.");
	$adapter->addSummary("Fisiotherapy", "fisiotherapy","nonzero-money Rp.");
	$adapter->addSummary("Darah", "darah","nonzero-money Rp.");
	$adapter->addSummary("Ambulan", "ambulan","nonzero-money Rp.");
	$adapter->addSummary("Gizi", "gizi","nonzero-money Rp.");
	$adapter->addSummary("Tindakan Perawat", "tindakan_perawat","nonzero-money Rp.");
	$adapter->addSummary("Tindakan Dokter", "tindakan_dokter","nonzero-money Rp.");
	$adapter->addSummary("Konsul", "konsul","nonzero-money Rp.");
	$adapter->addSummary("Visite", "visite","nonzero-money Rp.");
	$adapter->addSummary("Periksa", "periksa","nonzero-money Rp.");
	$adapter->addSummary("Bed", "bed","nonzero-money Rp.");
	$adapter->addSummary("Penjualan Resep", "penjualan_resep","nonzero-money Rp.");
	$adapter->addSummary("Return Resep", "return_resep","nonzero-money Rp.");
	$adapter->addSummary("VK", "vk","nonzero-money Rp.");
	$adapter->addSummary("Operasi", "operasi","nonzero-money Rp.");
	$adapter->addSummary("Recovery Room", "recovery_room","nonzero-money Rp.");
	$adapter->addSummary("Alat Obat", "alat_obat","nonzero-money Rp.");
	$adapter->addSummary("Bronchoscopy", "bronchoscopy","nonzero-money Rp.");
	$adapter->addSummary("Spirometry", "spirometry","nonzero-money Rp.");
	$adapter->addSummary("Audiometry", "audiometry","nonzero-money Rp.");
	$adapter->addSummary("Endoscopy", "endoscopy","nonzero-money Rp.");
	$adapter->addSummary("EKG", "ekg","nonzero-money Rp.");
	$adapter->addSummary("Administrasi", "administrasi","nonzero-money Rp.");
	$adapter->addSummary("Karcis", "karcis","nonzero-money Rp.");
	$adapter->addSummary("Oksigen Central", "oksigen_central","nonzero-money Rp.");
	$adapter->addSummary("Oksigen Manual", "oksigen_manual","nonzero-money Rp.");
	$adapter->addSummary("Total", "total","nonzero-money Rp.");
	
	$adapter->add("Ruangan", "ruangan","unslug");
	$adapter->add("Radiology", "radiology","nonzero-money Rp.");
	$adapter->add("Laboratory", "laboratory","nonzero-money Rp.");
	$adapter->add("Fisiotherapy", "fisiotherapy","nonzero-money Rp.");
	$adapter->add("Darah", "darah","nonzero-money Rp.");
	$adapter->add("Ambulan", "ambulan","nonzero-money Rp.");
	$adapter->add("Gizi", "gizi","nonzero-money Rp.");
	$adapter->add("Tindakan Perawat", "tindakan_perawat","nonzero-money Rp.");
	$adapter->add("Tindakan Dokter", "tindakan_dokter","nonzero-money Rp.");
	$adapter->add("Konsul", "konsul","nonzero-money Rp.");
	$adapter->add("Visite", "visite","nonzero-money Rp.");
	$adapter->add("Periksa", "periksa","nonzero-money Rp.");
	$adapter->add("Bed", "bed","nonzero-money Rp.");
	$adapter->add("Penjualan Resep", "penjualan_resep","nonzero-money Rp.");
	$adapter->add("Return Resep", "return_resep","nonzero-money Rp.");
	$adapter->add("VK", "vk","nonzero-money Rp.");
	$adapter->add("Operasi", "operasi","nonzero-money Rp.");
	$adapter->add("Recovery Room", "recovery_room","nonzero-money Rp.");
	$adapter->add("Alat Obat", "alat_obat","nonzero-money Rp.");
	$adapter->add("Bronchoscopy", "bronchoscopy","nonzero-money Rp.");
	$adapter->add("Spirometry", "spirometry","nonzero-money Rp.");
	$adapter->add("Audiometry", "audiometry","nonzero-money Rp.");
	$adapter->add("Endoscopy", "endoscopy","nonzero-money Rp.");
	$adapter->add("EKG", "ekg","nonzero-money Rp.");
	$adapter->add("Administrasi", "administrasi","nonzero-money Rp.");
	$adapter->add("Karcis", "karcis","nonzero-money Rp.");
	$adapter->add("Oksigen Central", "oksigen_central","nonzero-money Rp.");
	$adapter->add("Oksigen Manual", "oksigen_manual","nonzero-money Rp.");
	$adapter->add("Total", "total","nonzero-money Rp.");
	$dbres=new DBResponder($dbtable, $uitable, $adapter);
	$hasil=$dbres->command($_POST['command']);
	echo json_encode($hasil);
	return;
}

$query="SELECT DISTINCT origin FROM smis_ksr_bayar WHERE prop!='del' ";
$origin=new OptionBuilder();
$origin->add(" - Semua - ","",1);
$res=$db->get_result($query);
foreach($res as $x){
    if($x->origin==""){
        continue;
    }
    $origin->add(ArrayAdapter::slugFormat("unslug",$x->origin),$x->origin);
}

$uitable->clearContent();
$uitable->addModal("dari", "date", "Dari", "")
		->addModal("sampai", "date", "Sampai", "")
        ->addModal( "origin", "select", "Asal Data",$origin->getContent() );
$action=new Button("","","View");
$action->setClass("btn-primary")
	   ->setIsButton(Button::$ICONIC)
	   ->setIcon("icon-white fa fa-circle-o-notch")
	   ->setAction("laporan_piutang_per_jenis.rekaptotal()");
$btn_froup=new ButtonGroup("");
$btn_froup->addButton($action);
$form=$uitable
	  ->getModal()
	  ->setTitle("kasir")
	  ->getForm()
	  ->addElement("",$btn_froup);

$close=new Button("", "", "Batal");
$close	->addClass("btn-primary")
		->setIsButton(Button::$ICONIC_TEXT)
		->setIcon("fa fa-close")
		->setAction("laporan_piutang_per_jenis.batal()");

$load=new LoadingBar("rekap_laporan_piutang_per_jenis_bar", "");
$modal=new Modal("rekap_laporan_piutang_per_jenis_modal", "", "Processing...");
$modal	->addHTML($load->getHtml(),"after")
		->addFooter($close);

echo $form->getHtml();
echo "<div class='clear'></div>";
echo "<div id='result_laporan_piutang_per_jenis'>".$uitable->getHtml()."</div>";
echo $modal->getHtml();
echo addJS("framework/bootstrap/js/bootstrap-datepicker.js");
echo addJS("framework/smis/js/table_action.js");
echo addCSS("framework/bootstrap/css/datepicker.css");
echo addJS ( "base-js/smis-base-loading.js");
echo addJS ( "kasir/resource/js/laporan_piutang_per_jenis.js",false );
?>
