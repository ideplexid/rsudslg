<?php
global $db;
$header=array ('No.',"No. Kwitansi","Tanggal","Nama","Keterangan","Operator",'Nilai');
$uitable = new Table ( $header, "", NULL, false );
$uitable->setFooterVisible(false);
$uitable->setName ( "lap_non_pasien" );
$head="<tr><td colspan='9' style='text-align:center'><strong >Laporan Pembayaran Tunai Non Pasien</strong></td></tr>";
$uitable->addHeader("before", $head);
$head="<tr><td colspan='9' style='text-align:center'><strong id='tgl_lap_non_pasien_print'></strong></td></tr>";
$uitable->addHeader("before", $head);



if (isset ( $_POST ['command'] )) {
	require_once 'kasir/class/adapter/LapBankAdapter.php';
	require_once 'kasir/class/responder/LaporanCashNonPasienResponder.php';
	$adapter = new SummaryAdapter();
	$adapter->addFixValue("Operator", "<strong>Total</strong>");
	$adapter->addSummary("Nilai", "nilai","money Rp.");
	$adapter->setUseNumber(true, "No.","back.");
	$adapter->add ( "No. Kwitansi", "id","only-digit6");
	$adapter->add ( "Tanggal", "tanggal","date d M Y H:i");
	$adapter->add ( "Nama", "nama");		
	$adapter->add ( "Keterangan", "keterangan");
	$adapter->add ( "Nilai", "nilai", "money Rp." );
	$adapter->add ( "Operator", "operator" );
	
	$dbtable = new DBTable ( $db, "smis_ksr_np" );
	$dbres = new LaporanCashNonPasienResponder ( $dbtable, $uitable, $adapter );
	if($dbres->isView() || $dbres->isExcel() ){
		if($_POST['dari_tgl']!="")      $dbtable->addCustomKriteria(null, " tanggal>='".$_POST['dari_tgl']."' ");
		if($_POST['sampai_tgl']!="")    $dbtable->addCustomKriteria(null, " tanggal<'".$_POST['sampai_tgl']."' ");		
        if($_POST['dari_no']!="")          $dbtable->addCustomKriteria(null, " id>='".$_POST['dari_no']."' ");
		if($_POST['sampai_no']!="")        $dbtable->addCustomKriteria(null, " id<'".$_POST['sampai_no']."' ");		
		$dbtable->setShowAll(true);
	}
	$data = $dbres->command ( $_POST ['command'] );
	if($data!=NULL)
		echo json_encode ( $data );
	return;
}

$uitable->addModal ( "dari_tgl", "datetime", "Dari Tgl", "" );
$uitable->addModal ( "sampai_tgl", "datetime", "Sampai Tgl", "" );
$uitable->addModal ( "dari_no", "text", "Dari No.", "" );
$uitable->addModal ( "sampai_no", "text", "Sampai No.", "" );
$form=$uitable->getModal ()->getForm();

$cari=new Button("", "", "Search");
$cari->setIsButton(Button::$ICONIC);
$cari->setIcon("fa fa-search");
$cari->setClass("btn-primary");
$cari->setAction("lap_non_pasien.view()");
$cari->addAtribute("data-content","Search...");
$cari->addAtribute("data-toggle","popover");
$print=new Button("", "", "Search");
$print->setIsButton(Button::$ICONIC);
$print->setIcon("fa fa-print");
$print->setClass("btn-primary");
$print->setAction("lap_non_pasien.print()");
$print->addAtribute("data-content","Print ");
$print->addAtribute("data-toggle","popover");
$excel=new Button("", "", "Search");
$excel->setIsButton(Button::$ICONIC);
$excel->setIcon("fa fa-file-excel-o");
$excel->setClass("btn-primary");
$excel->setAction("lap_non_pasien.excel()");
$excel->addAtribute("data-content","Print Excel");
$excel->addAtribute("data-toggle","popover");
$form->addElement("",$cari);
$form->addElement("",$print);
$form->addElement("",$excel);

echo $form->getHtml ();
echo $uitable->getHtml ();
echo addJS ( "framework/bootstrap/js/bootstrap-datetimepicker.js" );
echo addCSS ( "framework/bootstrap/css/bootstrap-datetimepicker.css" );

?>

<script type="text/javascript">
	var lap_non_pasien;
	//var employee;
	$(document).ready(function(){
		$(".mydatetime").datetimepicker({minuteStep:1});
		$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
		var column=new Array('dari_tgl','sampai_tgl');
		lap_non_pasien=new TableAction("lap_non_pasien","kasir","lap_non_pasien",column);
		lap_non_pasien.getRegulerData=function(){
			var reg_data={	
					page:this.page,
					action:this.action,
					super_command:this.super_command,
					prototype_name:this.prototype_name,
					prototype_slug:this.prototype_slug,
					dari_tgl:$("#"+this.prefix+"_dari_tgl").val(),
					sampai_tgl:$("#"+this.prefix+"_sampai_tgl").val(),
                    dari_no:$("#"+this.prefix+"_dari_no").val(),
					sampai_no:$("#"+this.prefix+"_sampai_no").val()
                };
			var title="";
            var dr=getFormattedTime(reg_data['dari_tgl']);
			var sp=getFormattedTime(reg_data['sampai_tgl']);
            var drno=$("#"+this.prefix+"_dari_no").val();
            var spno=$("#"+this.prefix+"_sampai_no").val();
            
			if(dr!="" && sp!=""){
                title+="[ "+dr+" - "+sp+" ] ";
            }
            
            if(drno!="" && spno!=""){
                title+="[ "+drno+" - "+spno+" ]";
            }
            
			$("#tgl_lap_non_pasien_print").html(title);
			return reg_data;
		};
	});
	</script>