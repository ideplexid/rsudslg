<?php
/**
 * this used for viewing the history of payment
 * this page automatically filter the cash payment
 * 
 * @version     : 5.0.0
 * @since       : 14 May 2015
 * @author      : Nurul Huda
 * @copyright   : goblooge@gmail.com
 * @license     : LGPLv4
 * @database    : smis_ksr_bayar
 * */
global $db;
$header=array ("ID","No Kwitansi",'Tanggal',"Nama",'Nilai','Keterangan',"Operator","Asal Data" );
$uitable = new Table ( $header, "", NULL, true );
$head="<tr><td colspan='20' style='text-align:center'><strong >Laporan Pembayaran Tunai Pasien</strong></td></tr>";
$uitable->addHeader("before", $head);
$head="<tr><td colspan='20' style='text-align:center'><strong id='tgl_lap_cash_resep_non_pasien_print'></strong></td></tr>";
$uitable->addHeader("before", $head);
$uitable->setFooterVisible(false);
$uitable->setName ( "lap_cash_resep_non_pasien" );
$uitable->setEditButtonEnable(false);
$uitable->setAddButtonEnable(false);
$uitable->setDelButtonEnable(false);

$button = new Button("","cash_push_akunting","Post");
$button->setClass("btn-primary");
$button->setIcon("fa fa-upload");
$button->setIsButton(Button::$ICONIC_TEXT);
$button->setAction("lap_cash_resep_non_pasien.post_to_all()");
$uitable->addContentButton("push_to_accounting",$button);

if (isset ( $_POST ['command'] )) {
	require_once 'kasir/class/adapter/LapCashAdapter.php';
	require_once 'kasir/class/responder/LaporanCashPerNomorResponder.php';
	$adapter = new LapCashAdapter ();
	$adapter->add ( "ID", "id","only-digit8");
	$adapter->add ( "No Kwitansi", "no_kwitansi");
	$adapter->add ( "Nama", "nama_pasien");
	$adapter->add ( "Tanggal", "waktu", "date d M Y H:i" );
	$adapter->add ( "Nilai", "nilai", "money Rp." );
	$adapter->add ( "Operator", "operator" );
	$adapter->add ( "Keterangan", "keterangan" );
	$adapter->add ( "Asal Data", "origin","unslug");
	
	$dbtable = new DBTable ( $db, "smis_ksr_bayar" );
	$dbtable->setFetchMethode ( DBTable::$ARRAY_FETCH );
	$dbtable->addCustomKriteria ( "metode", " LIKE 'cash%'" );	
	$dbtable->addCustomKriteria ( "noreg_pasien", " = ''" );	
	$dbres = new LaporanCashPerNomorResponder  ( $dbtable, $uitable, $adapter );
	if($dbres->isView() || $_POST['command']=="excel"){
		if($_POST['dari_wkt']!="") 		$dbtable->addCustomKriteria(null, " ( waktu>='".$_POST['dari_wkt']."' )");
		if($_POST['sampai_wkt']!="") 	$dbtable->addCustomKriteria(null, " ( waktu<'".$_POST['sampai_wkt']."' )");
		if($_POST['dari_id']!="") 		$dbtable->addCustomKriteria(null, " ( id>='".$_POST['dari_id']."' )");
		if($_POST['sampai_id']!="") 	$dbtable->addCustomKriteria(null, " ( id<'".$_POST['sampai_id']."' )");
		if($_POST['dari_nokwi']!="") 	$dbtable->addCustomKriteria(null, " ( no_kwitansi>='".$_POST['dari_nokwi']."' )");
		if($_POST['sampai_nokwi']!="") 	$dbtable->addCustomKriteria(null, " ( no_kwitansi<'".$_POST['sampai_nokwi']."')");
		if($_POST['nama']!="") 	        $dbtable->addCustomKriteria(null, " ( nama_pasien LIKE '%".$_POST['nama']."%')");
		if($_POST['origin']!="")		$dbtable->addCustomKriteria("origin", " ='".$_POST['origin']."' ");
        $dbtable->setShowAll(true);
	}
	$data = $dbres->command ( $_POST ['command'] );
	if($data!=NULL)
		echo json_encode ( $data );
	return;
}

$query="SELECT DISTINCT origin FROM smis_ksr_bayar WHERE prop!='del' ";
$origin=new OptionBuilder();
$origin->add(" - Semua - ","",1);
$res=$db->get_result($query);
foreach($res as $x){
    if($x->origin==""){
        continue;
    }
    $origin->add(ArrayAdapter::slugFormat("unslug",$x->origin),$x->origin);
}


$uitable->addModal ( "dari_wkt", "datetime", "Dari Waktu", "" );
$uitable->addModal ( "sampai_wkt", "datetime", "Sampai Waktu", "" );
$uitable->addModal ( "dari_id", "text", "Dari ID", "","n","numeric" );
$uitable->addModal ( "sampai_id", "text", "Sampai ID","","n","numeric");
$uitable->addModal ( "dari_nokwi", "text", "Dari No.KW", "","n","numeric" );
$uitable->addModal ( "sampai_nokwi", "text", "Sampai No.KW","","n","numeric");
$uitable->addModal ( "nama", "text", "Nama","","n");
$uitable->addModal ( "origin", "select", "Asal Data",$origin->getContent() );
$form=$uitable->getModal ()->getForm();

$cari=new Button("", "", "Search");
$cari->setIsButton(Button::$ICONIC);
$cari->setIcon("fa fa-search");
$cari->setClass("btn-primary");
$cari->setAction("lap_cash_resep_non_pasien.view()");
$cari->addAtribute("data-content","Search...");
$cari->addAtribute("data-toggle","popover");

$excel=new Button("", "", "Search");
$excel->setIsButton(Button::$ICONIC);
$excel->setIcon("fa fa-file-excel-o");
$excel->setClass("btn-primary");
$excel->setAction("lap_cash_resep_non_pasien.excel()");
$excel->addAtribute("data-content","Print Excel");
$excel->addAtribute("data-toggle","popover");

$print=new Button("", "", "Search");
$print->setIsButton(Button::$ICONIC);
$print->setIcon("fa fa-print");
$print->setClass("btn-primary");
$print->setAction("lap_cash_resep_non_pasien.print()");
$print->addAtribute("data-content","Print ");
$print->addAtribute("data-toggle","popover");

$form->addElement("",$cari);
$form->addElement("",$print);
$form->addElement("",$excel);
$form->addElement("",$button);

echo $form->getHtml ();
echo $uitable->getHtml ();
echo addJS ( "framework/bootstrap/js/bootstrap-datetimepicker.js" );
echo addCSS ( "framework/bootstrap/css/bootstrap-datetimepicker.css" );
echo addJS ( "base-js/smis-base-loading.js" );
echo addJS ( "kasir/resource/js/lap_cash_resep_non_pasien.js" ,false);

?>
