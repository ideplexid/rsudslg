<?php 
/**
 * this sistem used for create a report
 * of several patient that already go home 
 * on given time frame.
 * 
 * @author      : Nurul Huda
 * @copyright   : goblooge@gmail.com
 * @since       : 17 Aug 2016
 * @database    : smis_ksr_bayar
 * @service     : get_registered_all_by_pulang
 * @version     : 1.6.0
 * @license     : LGPLv3
 * */
 
global $db;
require_once 'smis-base/smis-include-service-consumer.php';
$uitable=new Table(array("No.",'Tanggal',"No Reg","NRM","Nama","Alamat","RI/RJ","Cara Bayar","Rawat Jalan","Rawat Inap","Tagihan","Asuransi","Bank","Cash","Diskon"),"");
$uitable->setName("laporan_uang_masuk");
$uitable->setAction(false);

if(isset($_POST['command']) && $_POST['command']=="excel"){
	require_once "kasir/snippet/create_excel_laporan_uang_masuk.php";
    return;
}

if(isset($_POST['command'])){	
	$adapter=new SummaryAdapter();
	$adapter->setUseNumber(true,"No.","back.");
	$adapter->add("Tanggal", "tanggal","date d M Y H:i");
	$adapter->add("Nama", "nama_pasien");
	$adapter->add("No Reg", "id","digit8");
	$adapter->add("NRM", "nrm","digit8");
	$adapter->add("Alamat", "alamat_pasien");
    $adapter->add("RI/RJ", "uri","trivial_0_URJ_URI");
	$adapter->add("Rawat Jalan", "jenislayanan","unslug");
    $adapter->add("Rawat Inap", "kamar_inap","unslug");    
    $adapter->add("Tagihan", "total_tagihan","money Rp.");
    $adapter->add("Asuransi", "tb_asuransi","money Rp.");
    $adapter->add("Bank", "tb_bank","money Rp.");
	$adapter->add("Cash", "tb_cash","money Rp.");
    $adapter->add("Diskon", "tb_diskon","money Rp.");
    $adapter->add("Cara Bayar", "carabayar","unslug");
    
    $adapter->addSummary("Tagihan", "total_tagihan","money Rp.");
	$adapter->addSummary("Asuransi", "tb_asuransi","money Rp.");
	$adapter->addSummary("Bank", "tb_bank","money Rp.");
	$adapter->addSummary("Cash", "tb_cash","money Rp.");
    $adapter->addSummary("Diskon", "tb_diskon","money Rp.");
    $adapter->addFixValue("Nama", "<strong>Total</strong>");
    
    $dbres=new ServiceResponder($db, $uitable, $adapter, "get_registered_all_by_pulang");
	$p=$dbres->command($_POST['command']);
	echo json_encode($p);
	return;
}

$option=new OptionBuilder();
$option->add("Semua","","1");
$option->add("Ada Asuransi","tb_asuransi","0");
$option->add("Ada Cash","tb_cash","0");
$option->add("Ada Bank","tb_bank","0");
$option->add("Ada Diskon","tb_diskon","0");

$query="SELECT DISTINCT origin FROM smis_ksr_bayar WHERE prop!='del' ";
$origin=new OptionBuilder();
$origin->add(" - Semua - ","",1);
$res=$db->get_result($query);
foreach($res as $x){
    if($x->origin==""){
        continue;
    }
    $origin->add(ArrayAdapter::slugFormat("unslug",$x->origin),$x->origin);
}


$uitable->addModal("dari", "date", "Dari", "");
$uitable->addModal("sampai", "date", "Sampai", "");
$uitable->addModal("filter_uang", "select", "Filter", $option->getContent());
$uitable->addModal( "origin", "select", "Asal Data",$origin->getContent() );
$form=$uitable->getModal()->getForm();

$btn=new Button("", "", "Tampilkan");
$btn->addClass("btn-primary");
$btn->setIsButton(Button::$ICONIC_TEXT);
$btn->setIcon("fa fa-refresh");
$btn->setAction("laporan_uang_masuk.clear()");

$excel=new Button("","","Excel");
$excel->setClass("btn-primary")
	->setIsButton(Button::$ICONIC_TEXT)
	->setIcon("fa fa-file-excel-o")
	->setAction("laporan_uang_masuk.excel()");

$form->addElement("", $btn->getHtml());
$form->addElement("", $excel->getHtml());

echo $form->getHtml();
echo addJS ( "framework/smis/js/table_action.js" );
echo addJS ( "framework/bootstrap/js/bootstrap-datepicker.js" );
echo addJS ( "kasir/resource/js/laporan_uang_masuk.js",false );
echo addCSS ( "framework/bootstrap/css/datepicker.css" );
echo $uitable->getHtml();


?>