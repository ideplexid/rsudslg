<?php
global $db;
$header=array ('No.',"No. Kwitansi","Tanggal","Nama","Keterangan","Operator",'Nilai');
$uitable = new Table ( $header, "", NULL, false );
$uitable->setFooterVisible(false);
$uitable->setName ( "lap_non_pasien_per_nomor" );
$head="<tr><td colspan='9' style='text-align:center'><strong >Laporan Pembayaran Tunai Non Pasien</strong></td></tr>";
$uitable->addHeader("before", $head);
$head="<tr><td colspan='9' style='text-align:center'><strong id='tgl_lap_non_pasien_per_nomor_print'></strong></td></tr>";
$uitable->addHeader("before", $head);



if (isset ( $_POST ['command'] )) {
	require_once 'kasir/class/adapter/LapCashAdapter.php';
	require_once 'kasir/class/responder/LaporanCashNonPasienResponder.php';
	$adapter = new SummaryAdapter();
	$adapter->addFixValue("Operator", "<strong>Total</strong>");
	$adapter->addSummary("Nilai", "nilai","money Rp.");
	$adapter->setUseNumber(true, "No.","back.");
	$adapter->add ( "No. Kwitansi", "id","only-digit6");
	$adapter->add ( "Tanggal", "tanggal","date d M Y");
	$adapter->add ( "Nama", "nama");		
	$adapter->add ( "Keterangan", "keterangan");
	$adapter->add ( "Nilai", "nilai", "money Rp." );
	$adapter->add ( "Operator", "operator" );
	
	$dbtable = new DBTable ( $db, "smis_ksr_np" );
	$dbres = new LaporanCashNonPasienResponder ( $dbtable, $uitable, $adapter );
	if($dbres->isView()){
		if($_POST['dari']!="")
			$dbtable->addCustomKriteria(null, " id>='".$_POST['dari']."' ");
		if($_POST['sampai']!="")
			$dbtable->addCustomKriteria(null, " id<'".$_POST['sampai']."' ");		
		$dbtable->setShowAll(true);
	}
	$data = $dbres->command ( $_POST ['command'] );
	if($data!=NULL)
		echo json_encode ( $data );
	return;
}

$uitable->addModal ( "dari", "text", "Dari", "" );
$uitable->addModal ( "sampai", "text", "Sampai", "" );
$form=$uitable->getModal ()->getForm();

$cari=new Button("", "", "Search");
$cari->setIsButton(Button::$ICONIC);
$cari->setIcon("fa fa-search");
$cari->setClass("btn-primary");
$cari->setAction("lap_non_pasien_per_nomor.view()");
$cari->addAtribute("data-content","Search...");
$cari->addAtribute("data-toggle","popover");
$print=new Button("", "", "Search");
$print->setIsButton(Button::$ICONIC);
$print->setIcon("fa fa-print");
$print->setClass("btn-primary");
$print->setAction("lap_non_pasien_per_nomor.print()");
$print->addAtribute("data-content","Print ");
$print->addAtribute("data-toggle","popover");
$excel=new Button("", "", "Search");
$excel->setIsButton(Button::$ICONIC);
$excel->setIcon("fa fa-file-excel-o");
$excel->setClass("btn-primary");
$excel->setAction("lap_non_pasien_per_nomor.excel()");
$excel->addAtribute("data-content","Print Excel");
$excel->addAtribute("data-toggle","popover");
$form->addElement("",$cari);
$form->addElement("",$print);
$form->addElement("",$excel);

echo $form->getHtml ();
echo $uitable->getHtml ();
echo addJS ( "framework/bootstrap/js/bootstrap-datepicker.js" );
echo addCSS ( "framework/bootstrap/css/datepicker.css" );

?>

<script type="text/javascript">
	var lap_non_pasien_per_nomor;
	//var employee;
	$(document).ready(function(){
		$(".mydate").datepicker();
		$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
		var column=new Array('dari','sampai');
		lap_non_pasien_per_nomor=new TableAction("lap_non_pasien_per_nomor","kasir","lap_non_pasien_per_nomor",column);
		lap_non_pasien_per_nomor.getRegulerData=function(){
			var reg_data={	
					page:this.page,
					action:this.action,
					super_command:this.super_command,
					prototype_name:this.prototype_name,
					prototype_slug:this.prototype_slug,
					dari:$("#"+this.prefix+"_dari").val(),
					sampai:$("#"+this.prefix+"_sampai").val()
					};
			var dr=getFormattedTime(reg_data['dari']);
			var sp=getFormattedTime(reg_data['sampai']);
			var hsl=dr+" - "+sp;
			$("#tgl_lap_non_pasien_per_nomor_print").html(hsl);
			return reg_data;
		};
	});
	</script>