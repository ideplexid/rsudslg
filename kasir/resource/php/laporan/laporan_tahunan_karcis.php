<?php

global $db;
require_once 'smis-base/smis-include-service-consumer.php';
$urjip=new ServiceConsumer($db, "get_urjip",array());
$urjip->setMode(ServiceConsumer::$MULTIPLE_MODE);
$urjip->setCached(true,"get_urjip");
$urjip->execute();
$content=$urjip->getContent();
$ruangan=array();
foreach ($content as $autonomous=>$ruang){
	foreach($ruang as $nama_ruang=>$jip){
		if($jip[$nama_ruang]=="URJ" || $jip[$nama_ruang]=="UP" || $jip[$nama_ruang]=="URJI"){
			$option=array();
			$option['value']=$nama_ruang;
			$option['name']=ArrayAdapter::format("unslug", $nama_ruang);
			$ruangan[]=$option;
		}
	}
}

$header=array ("Tahun",'Bulan',"Nilai");
foreach($ruangan as $one){
	$header[]=$one["name"];
}
$header[]="Total";
$uitable = new Table ( $header, "", NULL, false );
$head="<tr><td colspan='20' style='text-align:center'><strong >Laporan Tahunan Karcis</strong></td></tr>";
$uitable->addHeader("before", $head);
$head="<tr><td colspan='20' style='text-align:center'><strong id='tgl_laporan_tahunan_karcis_print'></strong></td></tr>";
$uitable->addHeader("before", $head);
$uitable->setFooterVisible(false);
$uitable->setName ( "laporan_tahunan_karcis" );

if (isset ( $_POST ['command'] )) {
	$adapter = new SummaryAdapter();
	$adapter->addSummary("Jumlah", "jumlah","money Rp.");
	$adapter->addFixValue("Tahun", "<strong>Total</strong>");
	$adapter->add ( "Tahun", "tahun");
	$adapter->add ( "Bulan", "bulan");
	$adapter->add ( "Nilai", "karcis","money Rp.");
	foreach($ruangan as $one){		
		$adapter->add ( $one['name'], $one['value'],"money Rp.");
		$adapter->addSummary( $one['name'], $one['value'],"money Rp.");
	}
	$adapter->add ( "Total", "total","money Rp.");
	$adapter->addSummary ( "Total", "total","money Rp.");
	$dbres = new ServiceResponder($db, $uitable, $adapter, "get_laporan_karcis","registration");
	$data = $dbres->command ( $_POST ['command'] );
	if($data!=NULL)
		echo json_encode ( $data );
	return;
}

$uitable->addModal ( "dari", "datetime", "Dari", "" );
$uitable->addModal ( "sampai", "datetime", "Sampai", "" );
$form=$uitable->getModal ()->getForm();

$cari=new Button("", "", "Search");
$cari->setIsButton(Button::$ICONIC);
$cari->setIcon("fa fa-search");
$cari->setClass("btn-primary");
$cari->setAction("laporan_tahunan_karcis.view()");
$cari->addAtribute("data-content","Search...");
$cari->addAtribute("data-toggle","popover");

$excel=new Button("", "", "Search");
$excel->setIsButton(Button::$ICONIC);
$excel->setIcon("fa fa-file-excel-o");
$excel->setClass("btn-primary");
$excel->setAction("laporan_tahunan_karcis.excel()");
$excel->addAtribute("data-content","Print Excel");
$excel->addAtribute("data-toggle","popover");

$print=new Button("", "", "Search");
$print->setIsButton(Button::$ICONIC);
$print->setIcon("fa fa-print");
$print->setClass("btn-primary");
$print->setAction("laporan_tahunan_karcis.print()");
$print->addAtribute("data-content","Print ");
$print->addAtribute("data-toggle","popover");

$form->addElement("",$cari);
$form->addElement("",$print);
$form->addElement("",$excel);


echo $form->getHtml ();
echo $uitable->getHtml ();
echo addJS ( "framework/bootstrap/js/bootstrap-datetimepicker.js" );
echo addCSS ( "framework/bootstrap/css/bootstrap-datetimepicker.css" );

?>

<script type="text/javascript">
	var laporan_tahunan_karcis;
	//var employee;
	$(document).ready(function(){
		$(".mydatetime").datetimepicker({minuteStep:1});
		$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
		var column=new Array('dari','sampai');
		laporan_tahunan_karcis=new TableAction("laporan_tahunan_karcis","kasir","laporan_tahunan_karcis",column);
		laporan_tahunan_karcis.getRegulerData=function(){
			var reg_data={	
					page:this.page,
					action:this.action,
					super_command:this.super_command,
					prototype_name:this.prototype_name,
					prototype_slug:this.prototype_slug,
					dari:$("#"+this.prefix+"_dari").val(),
					sampai:$("#"+this.prefix+"_sampai").val()
					};
			var dr=getFormattedTime(reg_data['dari']);
			var sp=getFormattedTime(reg_data['sampai']);
			var hsl=dr+" - "+sp;
			$("#tgl_laporan_tahunan_karcis_print").html(hsl);
			return reg_data;
		};
	});
	</script>