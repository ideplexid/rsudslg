<?php
/**
 * this used for viewing the history of payment
 * this page automatically filter the assurance payment
 * 
 * @version     : 5.0.0
 * @since       : 14 May 2015
 * @author      : Nurul Huda
 * @copyright   : goblooge@gmail.com
 * @license     : LGPLv3
 * @database    : smis_ksr_bayar
 * */
global $db;
require_once 'smis-base/smis-include-service-consumer.php';
require_once 'kasir/class/table/AsuransiTable.php';
$header  = array("Nama","Alamat","Telpon");
$uitable = new Table($header);
$uitable ->setModel(Table::$SELECT)
		 ->setName("lap_asuransi_asuransi");
$adapter = new SimpleAdapter();
$adapter ->add("Nama", "nama")
		 ->add("Alamat", "alamat")
		 ->add("Telpon", "telpon");
$service = new ServiceResponder($db, $uitable, $adapter, "get_asuransi");
$super	 = new SuperCommand();
$super   ->addResponder("lap_asuransi_asuransi", $service);
$data	 = $super->initialize();
if($data!=null){
	echo $data;
	return;
}

$header	 = array ("ID","No Kwitansi","Tanggal Masuk",'Tanggal Cetak',"Asuransi","Nama","Noreg","NRM","Ruangan","Carabayar",'Nilai',"No Bukti","Keterangan","Cair","Tanggal Cair","Asal Data" );
$uitable = new AsuransiTable ( $header, "", NULL, true);
$head	 = "<tr><td colspan='30' style='text-align:center'><strong>Laporan Pembayaran Asuransi <span id='lap_asuransi_jenisx'></span> </strong></td></tr>";
$uitable ->addHeader("before", $head);
$head	 = "<tr><td colspan='30' style='text-align:center'><strong id='tgl_lap_asuransi_print'></strong></td></tr>";
$uitable ->addHeader("before", $head)
		 ->setFooterVisible(false)
		 ->setAddButtonEnable(false)
		 ->setEditButtonEnable(true)
		 ->setDelButtonEnable(false)
		 ->setReloadButtonEnable(false)
		 ->setName ( "lap_asuransi" );

$load_kasir = new Button("", "", "Detail Tagihan");
$load_kasir ->setIsButton(Button::$ICONIC)
		 	->setIcon("fa fa-list-alt")
		 	->setClass("btn-info");
$upload  	= new Button("","asuransi_push_akunting","Post Piutang");
$upload  	->setClass("btn-primary")
		 	->setIcon("fa fa-upload")
		 	->setIsButton(Button::$ICONIC_TEXT)
		 	->setAction("lap_asuransi.post_to_all()");
$lunas   	= new Button("","push_to_lunas","Post Pelunasan");
$lunas   	->setClass("btn-info")
		 	->setIcon("fa fa-money")
		 	->setIsButton(Button::$ICONIC_TEXT)
		 	->setAction("lap_asuransi.push_to_lunas_all()");
$cair  	 	= new Button("","asuransi_cair","Cair");
$cair	 	->setClass("btn-success")
		 	->setIcon("fa fa-money")
		 	->setIsButton(Button::$ICONIC_TEXT);

$uitable 	->addHeaderButton($upload)
		 	->addHeaderButton($lunas);
$uitable 	->addContentButton('loading_kasir', $load_kasir)
		 	->addContentButton("push_to_accounting",$upload)
		 	->addContentButton("cair",$cair)
		 	->addContentButton("push_to_lunas",$lunas);
$uitable 	->setMaxContentButton(10,"Action");
if (isset ( $_POST ['command'] )) {
	require_once 'kasir/class/adapter/LapAsuransiAdapter.php';
	require_once 'kasir/class/responder/LaporanAsuransiResponder.php';
	$adapter = new LapAsuransiAdapter ();
	$adapter ->add ( "ID", "id","only-digit8")
			 ->add("No Kwitansi", "no_kwitansi")
			 ->add("Nama", "nama_pasien")
			 ->add("Asuransi", "nama_asuransi")
			 ->add("NRM", "nrm_pasien","digit6")
			 ->add("Noreg", "noreg_pasien","only-digit6")
			 ->add("Tanggal Masuk", "waktu_masuk", "date d M Y H:i" )
			 ->add("Tanggal Cetak", "waktu", "date d M Y H:i" )
			 ->add("Nilai", "nilai", "money Rp." )
			 ->add("Cair", "terklaim", "trivial_1_Ya_Belum" )
			 ->add("Tanggal Cair", "tgl_klaim", "date d M Y" )
			 ->add("Keterangan", "keterangan" )
			 ->add("No Bukti", "no_bukti" )
			 //->add("Ruangan", "ruangan","unslug")
			 ->add("Carabayar", "carabayar","unslug")
			 ->add("Tgl Klaim", "tgl_klaim","date d M Y")
             ->add("Asal Data", "origin","unslug")
             ->add("Keterangan", "keterangan");
	
	$dbtable = new DBTable ( $db, "smis_ksr_bayar" );
	$dbtable ->addCustomKriteria ( "metode", " LIKE 'asuransi%'" )
             ->setFetchMethode(DBTable::$ARRAY_FETCH);
    $dbtable ->setOrder(" waktu ASC ",true);
	$dbres   = new LaporanAsuransiResponder ( $dbtable, $uitable, $adapter );
	if($dbres->isView() || $dbres->isExcel()){
		if($_POST['dari']!="")  	    $dbtable->addCustomKriteria(null, " waktu>='".$_POST['dari']."' ");
		if($_POST['sampai']!="")	    $dbtable->addCustomKriteria(null, " waktu<'".$_POST['sampai']."' ");
		if($_POST['dari_masuk']!="")  	$dbtable->addCustomKriteria(null, " waktu_masuk>='".$_POST['dari_masuk']."' ");
		if($_POST['sampai_masuk']!="")	$dbtable->addCustomKriteria(null, " waktu_masuk<'".$_POST['sampai_masuk']."' ");
		if($_POST['dari_klaim']!="")  	$dbtable->addCustomKriteria(null, " tgl_klaim>='".$_POST['dari_klaim']."' ");
		if($_POST['sampai_klaim']!="")	$dbtable->addCustomKriteria(null, " tgl_klaim<'".$_POST['sampai_klaim']."' ");
		if($_POST['cair']!="")		    $dbtable->addCustomKriteria("terklaim", " ='".$_POST['cair']."' ");
        if($_POST['filter_origin']!="")	$dbtable->addCustomKriteria("origin", " ='".$_POST['filter_origin']."' ");
		if($_POST['asuransi']!="")		$dbtable->addCustomKriteria("nama_asuransi", " ='".$_POST['asuransi']."' ");
		if($_POST['nrm']!="") 	        $dbtable->addCustomKriteria(null,"(nrm_pasien = '".$_POST['nrm']."' )");
		if($_POST['nama']!="") 	        $dbtable->addCustomKriteria(null,"(nama_pasien LIKE '%".$_POST['nama']."%')");
		
		if($_POST['jenis']!="") 	   {
			if(trim($_POST['jenis'])=="Rawat Inap"){
				$dbtable->addCustomKriteria(null,"(urji LIKE '%".$_POST['jenis']."%')");
				$dbtable->addCustomKriteria(null,"(metode NOT LIKE '%_resep')");
				$dbtable->addCustomKriteria(null,"(metode NOT LIKE '%_np')");
			}else if(trim($_POST['jenis'])=="Rawat Jalan - Non IGD"){
				$dbtable->addCustomKriteria(null,"(urji LIKE '%Rawat Jalan%')");
				$dbtable->addCustomKriteria(null,"(ruangan NOT LIKE '%gawat_darurat' AND ruangan NOT LIKE '%igd%')");
				$dbtable->addCustomKriteria(null,"(metode NOT LIKE '%_resep')");
				$dbtable->addCustomKriteria(null,"(metode NOT LIKE '%_np')");
			}else if(trim($_POST['jenis'])=="Rawat Jalan - IGD"){
				$dbtable->addCustomKriteria(null,"(urji LIKE '%Rawat Jalan%')");
				$dbtable->addCustomKriteria(null,"(ruangan LIKE '%gawat_darurat' OR ruangan LIKE '%igd%')");
				$dbtable->addCustomKriteria(null,"(metode NOT LIKE '%_resep')");
				$dbtable->addCustomKriteria(null,"(metode NOT LIKE '%_np')");
			}else if(trim($_POST['jenis'])=="Resep Non KIUP"){
				$dbtable->addCustomKriteria(null,"(metode LIKE '%_resep')");
			}else if(trim($_POST['jenis'])=="Non Pasien"){
				$dbtable->addCustomKriteria(null,"(metode LIKE '%_np')");
			}
		}
		$dbtable->setShowAll(true);
	}
	$data = $dbres->command ( $_POST ['command'] );
	if($data!=NULL)
		echo json_encode ( $data );
	return;
}

$terklaim = new OptionBuilder();
$terklaim ->add("","","1")
		  ->add("Sudah","1","0")
		  ->add("Belum","0","0");
$query	  = "SELECT DISTINCT origin FROM smis_ksr_bayar WHERE prop!='del' ";
$origin	  = new OptionBuilder();
$origin   ->add(" - Semua - ","",1);
$res	  = $db->get_result($query);
foreach($res as $x){
    if($x->origin==""){
        continue;
    }
    $origin->add(ArrayAdapter::slugFormat("unslug",$x->origin),$x->origin);
}

$jenis   = new OptionBuilder();
$jenis   ->add("","",1)
	     ->add("Rawat Jalan - IGD","Rawat Jalan - IGD")
	     ->add("Rawat Jalan - Non IGD","Rawat Jalan - Non IGD")
	     ->add("Rawat Inap","Rawat Inap")
		 ->add("Resep Non KIUP","Resep Non KIUP")
	     ->add("Non Pasien","Non Pasien");
$uitable ->addModal ( "dari", "datetime", "Dari Cetak", "" )
		 ->addModal ( "sampai", "datetime", "Sampai Cetak", "" )
		 ->addModal ( "asuransi", "chooser-lap_asuransi-lap_asuransi_asuransi", "Asuransi", "" )
		 ->addModal ( "dari_masuk", "datetime", "Dari Masuk", "" )
		 ->addModal ( "sampai_masuk", "datetime", "Sampai Masuk", "" )
		 ->addModal ( "k_cair", "select", "Cair",$terklaim->getContent() )
		 ->addModal ( "dari_klaim", "date", "Dari Cair", "" )
         ->addModal ( "sampai_klaim", "date", "Sampai Cair", "" )
         ->addModal("jenis", "select", "Jenis",$jenis->getContent(),"y","")
		 ->addModal("nrm", "text", "NRM", "","n","numeric" )
		 ->addModal("nama", "text", "Nama","","n")
         ->addModal ( "filter_origin", "select", "Asal Data",$origin->getContent() )
         ->addModal ( "kriteria", "text", "Pencarian","" );
$form	 = $uitable->getModal ()->getForm();

$uitable ->clearContent();
$uitable ->addModal("id","hidden","","")
		 ->addModal("terklaim","checkbox","Terklaim","")
		 ->addModal("tgl_klaim","date","Tgl Klaim","")
		 ->addModal("no_bukti","text","No. Bukti","")
		 ->addModal("nilai","money","Nilai","")
		 ->addModal("keterangan","textarea","Keterangan","")
		 ->addModal("waktu","datetime","Waktu","","y",NULL,true);
$cari    = new Button("", "", "Search");
$cari    ->setIsButton(Button::$ICONIC)
	     ->setIcon("fa fa-search")
	     ->setClass("btn-primary")
	     ->setAction("lap_asuransi.view()")
	     ->addAtribute("data-content","Search...")
	     ->addAtribute("data-toggle","popover");
$excel   = new Button("", "", "Search");
$excel   ->setIsButton(Button::$ICONIC)
	     ->setIcon("fa fa-file-excel-o")
	     ->setClass("btn-primary")
	     ->setAction("lap_asuransi.excel()")
	     ->addAtribute("data-content","Print Excel")
	     ->addAtribute("data-toggle","popover");
$print   = new Button("", "", "Search");
$print   ->setIsButton(Button::$ICONIC)
	     ->setIcon("fa fa-print")
	     ->setClass("btn-primary")
	     ->setAction("lap_asuransi.print()")
	     ->addAtribute("data-content","Print ")
	     ->addAtribute("data-toggle","popover");
$form    ->addElement("",$cari)
	     ->addElement("",$print)
	     ->addElement("",$excel)
	     ->addElement("",$button);
$back    = new Button("", "", "");
$back    ->setIsButton(Button::$ICONIC)
	     ->setIcon("fa fa-toggle-up")
	     ->setClass("btn-primary")
	     ->setAction("reup_asuransi()");

echo "<div id='main_asuransi'>";
	echo $form		->getHtml ();	
	echo $uitable	->getModal()->setTitle("Klaim Asuransi")->getHtml();
	echo $uitable	->getHtml ();
echo "</div>";
echo "<div id='sub_menu_asuransi'>";
	echo $back->getHtml();
	echo "<div id='sub_menu_asuransi_content'>";
	echo "</div>";	
echo "</div>";

echo addJS  ("framework/bootstrap/js/bootstrap-datepicker.js");
echo addCSS ("framework/bootstrap/css/datepicker.css");
echo addJS  ("framework/bootstrap/js/bootstrap-datetimepicker.js");
echo addCSS ("framework/bootstrap/css/bootstrap-datetimepicker.css");
echo addJS  ("framework/bootstrap/js/bootstrap-datepicker.js");
echo addCSS ("framework/bootstrap/css/datepicker.css");
echo addJS  ("kasir/resource/js/lap_asuransi.js",false);
echo addCSS ("kasir/resource/css/lap_asuransi.css",false);