<?php
/**
 * this used for viewing the history of payment
 * this page automatically filter the cash payment
 * 
 * @version     : 5.0.0
 * @since       : 14 May 2015
 * @author      : Nurul Huda
 * @copyright   : goblooge@gmail.com
 * @license     : LGPLv4
 * @database    : smis_ksr_bayar
 * */
global $db;
$push    = new Button("","cash_push_akunting","Post");
$push    ->setClass("btn-primary")
		 ->setIcon("fa fa-upload")
		 ->setIsButton(Button::$ICONIC_TEXT)
		 ->setAction("lap_cash.post_to_all()");
		 
require_once ("kasir/class/table/CashTable.php");
$header	 = array ("ID","No Kwitansi",'Tanggal',"Ruangan","Carabayar","Nama","Noreg","NRM",'Nilai','Keterangan',"Operator","Asal Data" );
$uitable = new CashTable ( $header, "", NULL, true );
$head	 = "<tr><td colspan='20' style='text-align:center'><strong >Laporan Pembayaran Tunai <span id='lap_cash_jenisx'></span></strong></td></tr>";
$uitable ->addHeader("before", $head);
$head	 = "<tr><td colspan='20' style='text-align:center'><strong id='tgl_lap_cash_print'></strong></td></tr>";
$uitable ->addHeader("before",$head)
		 ->setFooterVisible(false)
		 ->setName("lap_cash")
		 ->setEditButtonEnable(false)
		 ->setAddButtonEnable(false)
		 ->setDelButtonEnable(false)
		 ->setPrintElementButtonEnable(true)
		 ->setPDFElementButtonEnable(true)
		 ->addContentButton("push_to_accounting",$push);
$uitable ->addHeaderButton($push);
if (isset ( $_POST ['command'] )) {
	require_once 'kasir/class/adapter/LapCashAdapter.php';
	require_once 'kasir/class/responder/LaporanCashPerNomorResponder.php';
	$adapter = new LapCashAdapter ();
	$adapter ->add("ID", "id","only-digit8")
			 ->add("No Kwitansi", "no_kwitansi")
			 ->add("Nama", "nama_pasien")
			 ->add("NRM", "nrm_pasien","digit6")
			 ->add("Noreg", "noreg_pasien","digit6")
			 //->add("Ruangan", "ruangan","unslug")
			 ->add("Carabayar", "carabayar","unslug")
			 ->add("Tanggal", "waktu", "date d M Y H:i")
			 ->add("Nilai", "nilai", "money Rp.")
			 ->add("Operator", "operator")
			 ->add("Keterangan", "keterangan")
			 ->add("Asal Data", "origin","unslug");
	
	$dbtable = new DBTable( $db, "smis_ksr_bayar" );
	$dbtable ->setFetchMethode( DBTable::$ARRAY_FETCH )
             ->addCustomKriteria( "metode", " LIKE 'cash%'" );
    $dbtable ->setOrder(" waktu ASC ",true);
	$dbres   = new LaporanCashPerNomorResponder  ( $dbtable, $uitable, $adapter );
	if($dbres->isView() || $_POST['command']=="excel"){
		if($_POST['dari_wkt']!="") 		$dbtable->addCustomKriteria(null,"(waktu>='".$_POST['dari_wkt']."' )");
		if($_POST['sampai_wkt']!="") 	$dbtable->addCustomKriteria(null,"(waktu<'".$_POST['sampai_wkt']."' )");
		if($_POST['dari_id']!="") 		$dbtable->addCustomKriteria(null,"(id>='".$_POST['dari_id']."' )");
		if($_POST['sampai_id']!="") 	$dbtable->addCustomKriteria(null,"(id<'".$_POST['sampai_id']."' )");
		if($_POST['dari_nokwi']!="") 	$dbtable->addCustomKriteria(null,"(no_kwitansi>='".$_POST['dari_nokwi']."' )");
		if($_POST['sampai_nokwi']!="") 	$dbtable->addCustomKriteria(null,"(no_kwitansi<'".$_POST['sampai_nokwi']."')");
		if($_POST['nrm']!="") 	        $dbtable->addCustomKriteria(null,"(nrm_pasien = '".$_POST['nrm']."' )");
		if($_POST['nama']!="") 	        $dbtable->addCustomKriteria(null,"(nama_pasien LIKE '%".$_POST['nama']."%')");
		if($_POST['filter_origin']!="")	$dbtable->addCustomKriteria("origin", " ='".$_POST['filter_origin']."' ");
        if($_POST['operator']!="")		$dbtable->addCustomKriteria("operator", " ='".$_POST['operator']."' ");
        if($_POST['jenis']!="") 	   {
			if(trim($_POST['jenis'])=="Rawat Inap"){
				$dbtable->addCustomKriteria(null,"(urji LIKE '%".$_POST['jenis']."%')");
				$dbtable->addCustomKriteria(null,"(metode NOT LIKE '%_resep')");
				$dbtable->addCustomKriteria(null,"(metode NOT LIKE '%_np')");
			}else if(trim($_POST['jenis'])=="Rawat Jalan - Non IGD"){
				$dbtable->addCustomKriteria(null,"(urji LIKE '%Rawat Jalan%')");
				$dbtable->addCustomKriteria(null,"(ruangan NOT LIKE '%gawat_darurat' AND ruangan NOT LIKE '%igd%')");
				$dbtable->addCustomKriteria(null,"(metode NOT LIKE '%_resep')");
				$dbtable->addCustomKriteria(null,"(metode NOT LIKE '%_np')");
			}else if(trim($_POST['jenis'])=="Rawat Jalan - IGD"){
				$dbtable->addCustomKriteria(null,"(urji LIKE '%Rawat Jalan%')");
				$dbtable->addCustomKriteria(null,"(ruangan LIKE '%gawat_darurat' OR ruangan LIKE '%igd%')");
				$dbtable->addCustomKriteria(null,"(metode NOT LIKE '%_resep')");
				$dbtable->addCustomKriteria(null,"(metode NOT LIKE '%_np')");
			}else if(trim($_POST['jenis'])=="Resep Non KIUP"){
				$dbtable->addCustomKriteria(null,"(metode LIKE '%_resep')");
			}else if(trim($_POST['jenis'])=="Non Pasien"){
				$dbtable->addCustomKriteria(null,"(metode LIKE '%_np')");
			}
		} 
		
		$dbtable->setShowAll(true);
	}
	$data = $dbres->command ( $_POST ['command'] );
	if($data!=NULL)
		echo json_encode ( $data );
	return;
}

$query  = "SELECT DISTINCT origin FROM smis_ksr_bayar WHERE prop!='del' ";
$origin = new OptionBuilder();
$origin ->add(" - Semua - ","",1);
$res	= $db->get_result($query);
foreach($res as $x){
    if($x->origin==""){
        continue;
    }
    $origin->add(ArrayAdapter::slugFormat("unslug",$x->origin),$x->origin);
}

$query    = "SELECT DISTINCT operator FROM smis_ksr_bayar WHERE prop!='del' ";
$operator = new OptionBuilder();
$operator ->add(" - Semua - ","",1);
$res	  = $db->get_result($query);
foreach($res as $x){
    if($x->operator==""){
        continue;
    }
    $operator->add(ArrayAdapter::slugFormat("unslug",$x->operator),$x->operator);
}

$jenis   = new OptionBuilder();
$jenis   ->add("","",1)
	     ->add("Rawat Jalan - IGD","Rawat Jalan - IGD")
	     ->add("Rawat Jalan - Non IGD","Rawat Jalan - Non IGD")
	     ->add("Rawat Inap","Rawat Inap")
		 ->add("Resep Non KIUP","Resep Non KIUP")
	     ->add("Non Pasien","Non Pasien");
$uitable ->addModal("dari_wkt", "datetime", "Dari Waktu", "" )
		 ->addModal("sampai_wkt", "datetime", "Sampai Waktu", "" )
		 ->addModal("dari_id", "text", "Dari ID", "","n","numeric" )
		 ->addModal("sampai_id", "text", "Sampai ID","","n","numeric")
		 ->addModal("dari_nokwi", "text", "Dari No.KW", "","n","numeric" )
		 ->addModal("sampai_nokwi", "text", "Sampai No.KW","","n","numeric")
		 ->addModal("jenis", "select", "Jenis",$jenis->getContent(),"y","")
		 ->addModal("nrm", "text", "NRM", "","n","numeric" )
		 ->addModal("nama", "text", "Nama","","n")
		 ->addModal("filter_origin", "select", "Asal Data",$origin->getContent() )
         ->addModal("operator", "select", "Operator",$operator->getContent() )
         ->addModal ( "kriteria", "text", "Pencarian","" );
$form   = $uitable->getModal ()->getForm();
$cari   = new Button("", "", "Search");
$cari   ->setIsButton(Button::$ICONIC)
	    ->setIcon("fa fa-search")
	    ->setClass("btn-primary")
	    ->setAction("lap_cash.view()")
	    ->addAtribute("data-content","Search...")
	    ->addAtribute("data-toggle","popover");
$excel  = new Button("", "", "Search");
$excel  ->setIsButton(Button::$ICONIC)
	    ->setIcon("fa fa-file-excel-o")
	    ->setClass("btn-primary")
	    ->setAction("lap_cash.excel()")
	    ->addAtribute("data-content","Print Excel")
	    ->addAtribute("data-toggle","popover");
$print  = new Button("", "", "Search");
$print  ->setIsButton(Button::$ICONIC)
	    ->setIcon("fa fa-print")
	    ->setClass("btn-primary")
	    ->setAction("lap_cash.print()")
	    ->addAtribute("data-content","Print ")
	    ->addAtribute("data-toggle","popover");
$form   ->addElement("",$cari)
	    ->addElement("",$print)
	    ->addElement("",$excel)
	    ->addElement("",$button);

echo $form	  ->getHtml ();
echo $uitable ->getHtml ();
echo addJS  ("framework/bootstrap/js/bootstrap-datetimepicker.js");
echo addCSS ("framework/bootstrap/css/bootstrap-datetimepicker.css");
echo addJS  ("base-js/smis-base-loading.js");
echo addJS  ("kasir/resource/js/lap_cash.js",false);
echo addCSS ("kasir/resource/css/kwitansi_bodong.css",false);