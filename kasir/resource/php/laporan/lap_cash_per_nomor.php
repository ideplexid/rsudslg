<?php
global $db;
$header=array ("ID","No Kwitansi",'Tanggal',"Ruangan","Carabayar","Nama","Noreg","NRM",'Nilai','Keterangan',"Operator" );
$uitable = new Table ( $header, "", NULL, false );
$head="<tr><td colspan='20' style='text-align:center'><strong >Laporan Pembayaran Tunai Pasien</strong></td></tr>";
$uitable->addHeader("before", $head);
$head="<tr><td colspan='20' style='text-align:center'><strong id='tgl_lap_cash_per_nomor_print'></strong></td></tr>";
$uitable->addHeader("before", $head);
$uitable->setFooterVisible(false);
$uitable->setName ( "lap_cash_per_nomor" );
if (isset ( $_POST ['command'] )) {
	require_once 'kasir/class/adapter/LapCashAdapter.php';
	require_once 'kasir/class/responder/LaporanCashPerNomorResponder.php';
	$adapter = new LapCashAdapter ();
	$adapter->add ( "ID", "id","only-digit8");
	$adapter->add ( "No Kwitansi", "no_kwitansi");
	$adapter->add ( "Nama", "nama_pasien");
	$adapter->add ( "NRM", "nrm_pasien","digit6");
	$adapter->add ( "Noreg", "noreg_pasien","digit6");	
	$adapter->add ( "Ruangan", "ruangan","unslug");
	$adapter->add ( "Carabayar", "carabayar","unslug");
	$adapter->add ( "Tanggal", "waktu", "date d M Y H:i" );
	$adapter->add ( "Nilai", "nilai", "money Rp." );
	$adapter->add ( "Operator", "operator" );
	$adapter->add ( "Keterangan", "keterangan" );
	
	$dbtable = new DBTable ( $db, "smis_ksr_bayar" );
	$dbtable->setFetchMethode ( DBTable::$ARRAY_FETCH );
	$dbtable->addCustomKriteria ( "metode", " LIKE 'cash%'" );	
	$dbres = new LaporanCashPerNomorResponder  ( $dbtable, $uitable, $adapter );
	if($dbres->isView()){
		if($_POST['dari_wkt']!="") 		$dbtable->addCustomKriteria(null, " ( waktu>='".$_POST['dari_wkt']."' )");
		if($_POST['sampai_wkt']!="") 	$dbtable->addCustomKriteria(null, " ( waktu<'".$_POST['sampai_wkt']."' )");
		if($_POST['dari_id']!="") 		$dbtable->addCustomKriteria(null, " ( id>='".$_POST['dari_id']."' )");
		if($_POST['sampai_id']!="") 	$dbtable->addCustomKriteria(null, " ( id<'".$_POST['sampai_id']."' )");
		if($_POST['dari_nokwi']!="") 	$dbtable->addCustomKriteria(null, " ( no_kwitansi>='".$_POST['dari_nokwi']."' )");
		if($_POST['sampai_nokwi']!="") 	$dbtable->addCustomKriteria(null, " ( no_kwitansi<'".$_POST['sampai_nokwi']."')");
		$dbtable->setShowAll(true);
	}
	$data = $dbres->command ( $_POST ['command'] );
	if($data!=NULL)
		echo json_encode ( $data );
	return;
}

$uitable->addModal ( "dari_wkt", "datetime", "Dari Waktu", "" );
$uitable->addModal ( "sampai_wkt", "datetime", "Sampai Waktu", "" );
$uitable->addModal ( "dari_id", "text", "Dari ID", "","n","numeric" );
$uitable->addModal ( "sampai_id", "text", "Sampai ID","","n","numeric");
$uitable->addModal ( "dari_nokwi", "text", "Dari No. Kwitansi", "","n","numeric" );
$uitable->addModal ( "sampai_nokwi", "text", "Sampai No. Kwitansi","","n","numeric");
$form=$uitable->getModal ()->getForm();

$cari=new Button("", "", "Search");
$cari->setIsButton(Button::$ICONIC);
$cari->setIcon("fa fa-search");
$cari->setClass("btn-primary");
$cari->setAction("lap_cash_per_nomor.view()");
$cari->addAtribute("data-content","Search...");
$cari->addAtribute("data-toggle","popover");

$excel=new Button("", "", "Search");
$excel->setIsButton(Button::$ICONIC);
$excel->setIcon("fa fa-file-excel-o");
$excel->setClass("btn-primary");
$excel->setAction("lap_cash_per_nomor.excel()");
$excel->addAtribute("data-content","Print Excel");
$excel->addAtribute("data-toggle","popover");

$print=new Button("", "", "Search");
$print->setIsButton(Button::$ICONIC);
$print->setIcon("fa fa-print");
$print->setClass("btn-primary");
$print->setAction("lap_cash_per_nomor.print()");
$print->addAtribute("data-content","Print ");
$print->addAtribute("data-toggle","popover");

$form->addElement("",$cari);
$form->addElement("",$print);
$form->addElement("",$excel);

echo $form->getHtml ();
echo $uitable->getHtml ();
echo addJS ( "framework/bootstrap/js/bootstrap-datetimepicker.js" );
echo addCSS ( "framework/bootstrap/css/bootstrap-datetimepicker.css" );

?>

<script type="text/javascript">
	var lap_cash_per_nomor;
	//var employee;
	$(document).ready(function(){
		$(".mydatetime").datetimepicker({minuteStep:1});
		$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
		var column=new Array('dari','sampai');
		lap_cash_per_nomor=new TableAction("lap_cash_per_nomor","kasir","lap_cash_per_nomor",column);
		lap_cash_per_nomor.getRegulerData=function(){
			var reg_data={	
					page:this.page,
					action:this.action,
					super_command:this.super_command,
					prototype_name:this.prototype_name,
					prototype_slug:this.prototype_slug,
					dari_wkt:$("#"+this.prefix+"_dari_wkt").val(),
					sampai_wkt:$("#"+this.prefix+"_sampai_wkt").val(),
					dari_id:$("#"+this.prefix+"_dari_id").val(),
					sampai_id:$("#"+this.prefix+"_sampai_id").val(),
					dari_nokwi:$("#"+this.prefix+"_dari_nokwi").val(),
					sampai_nokwi:$("#"+this.prefix+"_sampai_nokwi").val()
					};
			var dr=getFormattedTime(reg_data['dari_wkt']);
			var sp=getFormattedTime(reg_data['sampai_wkt']);
            var hsl="";
            if(reg_data['dari_wkt']!="" && reg_data['sampai_wkt']!="")
                hsl=" Waktu [ "+dr+" - "+sp+" ] ";			
            if(reg_data['dari_id']!="" && reg_data['sampai_id']!="")
                hsl+=" ID [ "+reg_data['dari_id']+" - "+reg_data['sampai_id']+" ] ";
			if(reg_data['dari_nokwi']!="" && reg_data['sampai_nokwi']!="")
                hsl+=" No. Kwitansi [ "+reg_data['dari_nokwi']+" - "+reg_data['sampai_nokwi']+" ] ";
			
            $("#tgl_lap_cash_per_nomor_print").html(hsl);
			return reg_data;
		};
	});
	</script>