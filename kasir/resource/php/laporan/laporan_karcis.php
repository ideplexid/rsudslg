<?php 
require_once 'smis-libs-class/MasterSlaveTemplate.php';
require_once 'smis-libs-class/MasterServiceTemplate.php';
require_once 'smis-libs-class/MasterSlaveServiceTemplate.php';
require_once 'smis-base/smis-include-service-consumer.php';
require_once 'registration/class/RegistrationResource.php';
global $db;


$button=new Button("", "", "");
$button->setAction("laporan_karcis.view()");
$button->setClass("btn-primary");
$button->setIsButton(Button::$ICONIC);
$button->setIcon("fa fa-refresh");

$cetak=new Button("", "", "");
$cetak->setAction("laporan_karcis.print()");
$cetak->setClass("btn-primary");
$cetak->setIsButton(Button::$ICONIC);
$cetak->setIcon("fa fa-print");

$laporan_karcis=new MasterSlaveServiceTemplate($db, "get_registered_all", "kasir", "laporan_karcis");
//$laporan_karcis->getDBtable()->setOrder(" tanggal ASC");
$uitable=$laporan_karcis->getUItable();
$uitable->setAction(false);
if(isset($_POST['dari']) && $_POST['dari']!="" && isset($_POST['sampai']) && $_POST['sampai']!=""){
	$responder=$laporan_karcis->getServiceResponder();
	$ser=new ServiceResponder();
	$ser->addData("dari",$_POST['dari']);
	$ser->addData("sampai",$_POST['sampai']);
	$ser->addData("order_by"," tanggal ASC "); 
			
	if($_POST['urji']!="") 			$ser->addData("uri",$_POST['uri']); 
	if($_POST['jenislayanan']!="") $ser->addData("jenislayanan",$_POST['jenislayanan']); 
	if($_POST['kamar_inap']!="") 	$ser->addData("kamar_inap",$_POST['kamar_inap']); 
	if($_POST['carabayar']!="") 	$ser->addData("carabayar",$_POST['carabayar']);	
	
}

$header=array("No.","Tanggal","Nama","NRM","No Reg","URI/URJ","Ruangan","Kamar Inap","Jenis Pasien","Karcis","Administrasi");
$uitable->setHeader($header);
$adapter=new SummaryAdapter();
$adapter->addFixValue("Ruangan", "<strong>Total</strong>");
$adapter->addSummary("Biaya", "biaya","money Rp.");
$adapter->setUseNumber(true, "No.","back.");
$adapter->add("Tanggal", "tanggal","date d M Y H:i")
		->add("Nama", "nama_pasien")
		->add("NRM", "nrm","only-digit8")
		->add("No Reg", "id","only-digit8")
		->add("Ruangan", "jenislayanan","unslug")
		->add("Jenis Pasien", "carabayar","unslug")
		->add("Kamar Inap", "kamar_inap","unslug")
		->add("URI/URJ", "uri","trivial_0_URJ_URI")
		->add("Karcis", "karcis","money Rp.")
		->add("Administrasi", "administrasi_inap","money Rp.");
$adapter->addSummary("Karcis","karcis","money Rp.")
		->addSummary("Administrasi","administrasi_inap","money Rp.");
$laporan_karcis->setAdapter($adapter);
$laporan_karcis ->addViewData("dari", "dari")
				->addViewData("sampai", "sampai")
				->addViewData("urji","urji")
				->addViewData("jenislayanan","jenislayanan")
				->addViewData("kamar_inap","kamar_inap")
				->addViewData("carabayar","carabayar");

if(!(isset($_POST['command']))){

	$urji=new OptionBuilder();
	$urji->add("","","1");
	$urji->add("Rawat Inap","1","0");
	$urji->add("Rawat Jalan","0","0");
	
	$service = new ServiceConsumer ( $db, "get_jenis_patient",NULL,"registration" );
	$service->execute ();
	$jenis_pasien = $service->getContent ();
	$cbayar=new OptionBuilder();
	$cbayar->add("","","1");
	foreach($jenis_pasien as $jp){
		$cbayar->add($jp['name'],$jp['value']);
	}
	
	/*untuk mendapatkan list tiap ruangan yang inap dan jalan*/
	$urjip=new ServiceConsumer($db, "get_urjip",array());
	$urjip->setMode(ServiceConsumer::$MULTIPLE_MODE);
    $urjip->setCached(true,"get_urjip");
	$urjip->execute();
	$content=$urjip->getContent();
	$uri=array();
	$urj=array();
	foreach ($content as $autonomous=>$ruang){
		foreach($ruang as $nama_ruang=>$jip){
			if($jip[$nama_ruang]=="URI" || $jip[$nama_ruang]=="URJI"){
				$option=array();
				$option['value']=$nama_ruang;
				$option['name']=ArrayAdapter::format("unslug", $nama_ruang);
				$uri[]=$option;
			}
			
			if($jip[$nama_ruang]=="URJ" || $jip[$nama_ruang]=="UP" || $jip[$nama_ruang]=="URJI"){
				$option=array();
				$option['value']=$nama_ruang;
				$option['name']=ArrayAdapter::format("unslug", $nama_ruang);
				$urj[]=$option;
			}
			
		}
	}
	$default=array("name"=>"","value"=>"","default"=>"1");
	$uri[]=$default;
	$urj[]=$default;

	$laporan_karcis ->addFlag("dari", "Tanggal Awal", "Silakan Masukan Tanggal Mulai")
					->addFlag("sampai", "Tanggal Akhir", "Silakan Masukan Tanggal Akhir")
					->addNoClear("dari")
					->addNoClear("sampai")
					->addNoClear("urji")
					->addNoClear("jenislayanan")
					->addNoClear("kamar_inap")
					->addNoClear("carabayar")
					->setDateEnable(true)
					->getUItable()
					->setActionEnable(false)
					->addModal("dari", "date", "Awal", "")
					->addModal("sampai", "date", "Akhir", "")
					->addModal("urji", "select", "URI/URJ", $urji->getContent())
					->addModal("jenislayanan", "select", "Ruangan", $urj)
					->addModal("kamar_inap", "select", "Kamar Inap", $uri)
					->addModal("carabayar", "select", "Jenis Pasien", $cbayar->getContent());
	$laporan_karcis->getForm()->addElement("", $button)->addElement("", $cetak);		
}

$laporan_karcis->initialize();
?>