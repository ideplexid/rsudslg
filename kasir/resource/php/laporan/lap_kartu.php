<?php 
require_once "smis-base/smis-include-service-consumer.php";
require_once 'smis-libs-class/MasterSlaveTemplate.php';
require_once 'smis-libs-class/MasterServiceTemplate.php';
require_once 'smis-libs-class/MasterSlaveServiceTemplate.php';
global $db;

$button=new Button("", "", "");
$button->setAction("lap_kartu.view()");
$button->setClass("btn-primary");
$button->setIsButton(Button::$ICONIC);
$button->setIcon("fa fa-refresh");

$cetak=new Button("", "", "");
$cetak->setAction("lap_kartu.print()");
$cetak->setClass("btn-primary");
$cetak->setIsButton(Button::$ICONIC);
$cetak->setIcon("fa fa-print");

$lap_kartu=new MasterSlaveServiceTemplate($db, "get_kartu", "kasir", "lap_kartu");
$lap_kartu->setEntity("registration");
$lap_kartu->getDBtable()->setOrder(" tanggal ASC ");
$uitable=$lap_kartu->getUItable();
$uitable->setAction(false);
if(isset($_POST['dari']) && $_POST['dari']!="" && isset($_POST['sampai']) && $_POST['sampai']!=""){
	$lap_kartu->getServiceResponder()
                  ->addData("dari",$_POST['dari'])
                  ->addData( "sampai",$_POST['sampai']);
}

$header=array("No.","Tanggal","Nama","NRM","Biaya");
$uitable->setHeader($header);
$adapter=new SummaryAdapter();
$adapter->addFixValue("Tanggal", "<strong>Total</strong>");
$adapter->addSummary("Biaya", "biaya","money Rp.");
$adapter->setUseNumber(true, "No.","back.");
$adapter->add("Tanggal", "tanggal","date d M Y H:i")
		->add("Nama", "nama_pasien")
		->add("NRM", "nrm_pasien","only-digit8")
		->add("Biaya", "biaya","money Rp.");
$adapter->addSummary("Biaya","biaya","money Rp.");
$lap_kartu->setAdapter($adapter);
$lap_kartu ->addViewData("dari", "dari")
				->addViewData("sampai", "sampai");

if(!isset($_POST['command']) || $_POST['command']==""){
	$lap_kartu ->addFlag("dari", "Tanggal Awal", "Silakan Masukan Tanggal Mulai")
					->addFlag("sampai", "Tanggal Akhir", "Silakan Masukan Tanggal Akhir")
					->addNoClear("dari")
					->addNoClear("sampai")
					->setDateEnable(true)
					->getUItable()
					->setActionEnable(false)
					->addModal("dari", "date", "Awal", "")
					->addModal("sampai", "date", "Akhir", "");
	$lap_kartu->getForm()->addElement("", $button)->addElement("", $cetak);		
}

$lap_kartu->initialize();
?>