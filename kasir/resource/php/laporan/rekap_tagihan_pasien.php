<?php

/**
 * masih belum jadi nunggu buatkan service */

global $db;
require_once "smis-base/smis-include-service-consumer.php";
require_once "smis-libs-class/MasterTemplate.php";
require_once "smis-libs-class/MasterServiceTemplate.php";
require_once "smis-libs-class/MasterSlaveServiceTemplate.php";

$button = new Button("","","");
$button ->addClass("btn-primary")
		->setIsButton(Button::$ICONIC)
		->setIcon("fa fa-print");

$init	= new Button("","","");
$init	->addClass("btn-primary")
		->setIsButton(Button::$ICONIC)
		->setIcon("fa fa-refresh");

$koreksi_carabayar = new Button("","","");
$koreksi_carabayar ->addClass("btn-primary")
				   ->setIsButton(Button::$ICONIC)
				   ->setIcon("fa fa-upload");

$header = array ("No.","No Reg","NRM",'Nama',"Tanggal","Jenis","Status","Layanan","Kamar","Cara Bayar","Nama Asuransi",'No. BPJS',"Plafon","Tagihan","Asuransi","Bank","Cash","Diskon");
$plafon = new MasterSlaveServiceTemplate($db, "get_registered_all", "kasir", "rekap_tagihan_pasien");
$plafon ->setEntity("registration");
$plafon ->setDateEnable(true);
if( isset($_POST['command']) && $_POST['command']=="list" ){	
	if(isset($_POST['sampai']) && $_POST['sampai']!="" )                    $plafon->getServiceResponder()->addData("sampai",$_POST['sampai']);	
	if(isset($_POST['dari']) && $_POST['dari']!="" )                        $plafon->getServiceResponder()->addData("dari",$_POST['dari']);	
	if(isset($_POST['uri']) && $_POST['uri']!="" )                          $plafon->getServiceResponder()->addData("uri",$_POST['uri']);	
	if(isset($_POST['selesai']) && $_POST['selesai']!="" )                  $plafon->getServiceResponder()->addData("selesai",$_POST['selesai']);
	if(isset($_POST['jenislayanan']) && $_POST['jenislayanan']!="" )        $plafon->getServiceResponder()->addData("jenislayanan",$_POST['jenislayanan']);
	if(isset($_POST['kamar_inap']) && $_POST['kamar_inap']!="" )            $plafon->getServiceResponder()->addData("kamar_inap",$_POST['kamar_inap']);		
	if(isset($_POST['jenis_pasien']) && $_POST['jenis_pasien']!="" )        $plafon->getServiceResponder()->addData("jenis_pasien",$_POST['jenis_pasien']);	
    if(isset($_POST['tanpa_tindakan']) && $_POST['tanpa_tindakan']!="" )    $plafon->getServiceResponder()->addData("tanpa_tindakan",$_POST['tindakan']);
    if(isset($_POST['noreg_pasien']) && $_POST['noreg_pasien']!="" )        $plafon->getServiceResponder()->addData("noreg_pasien",$_POST['noreg_pasien']);	
	if(isset($_POST['nrm_pasien']) && $_POST['nrm_pasien']!="" )            $plafon->getServiceResponder()->addData("nrm_pasien",$_POST['nrm_pasien']);	
}
$plafon->addResouce("js","kasir/resource/js/rekap_tagihan_pasien.js");
$plafon->addResouce("js","base-js/smis-base-loading.js","before",true);

require_once "kasir/class/adapter/RekapTagihanPasienAdapter.php";
$adapt  = new RekapTagihanPasienAdapter();
$adapt	->setUseNumber(true, "No.","back.");
$adapt	->addSummary("Tagihan","total_tagihan","money Rp.");
$adapt	->addFixValue("Tanggal","<strong>Total</strong>");
$plafon	->setAdapter($adapt);
$plafon ->getUItable()
		->addContentButton("print_tagihan_pasien",$button)
		->addContentButton("init_tagihan",$init)
		->addContentButton("init_koreksi_carabayar",$koreksi_carabayar)
		->setReloadButtonEnable(false)
		->setDelButtonEnable(false)
		->setAddButtonEnable(false)
		->setEditButtonEnable(false)
		->setHeader($header);

if(!isset($_POST['command'])){
	$uri = new OptionBuilder();
	$uri ->add("","","1")
		 ->add("URI","1","0")
		 ->add("URJ","0","0");

	$urjip = new ServiceConsumer($db, "get_urjip",array());
	$urjip ->setMode(ServiceConsumer::$MULTIPLE_MODE)
		   ->setCached(true,"get_urjip")
 		   ->execute();
	$content 		= $urjip->getContent();
	$ruangan	 	= array();
	$ruangan_inap	= array();
	foreach ($content as $autonomous=>$ruang){
		foreach($ruang as $nama_ruang=>$jip){
			$option 		 = array();
			$option['value'] = $nama_ruang;
			$option['name']  = ArrayAdapter::format("unslug", $nama_ruang);
			if($jip[$nama_ruang]=="URI"){
				$ruangan_inap[]=$option;
			}else{
				$ruangan[]=$option;
			}
		}
	}	
	$ruangan[]		 = array("nama"=>"","value"=>"","default"=>"1");
	$ruangan_inap[]	 = array("nama"=>"","value"=>"","default"=>"1");
	$selesai		 = new OptionBuilder();
	$selesai		 ->add("","","1")
					 ->add("Selesai","1","0")
					 ->add("Belangsung","0","0");
	$service		 = new ServiceConsumer($db,"get_format_kasir",NULL,"kasir");
	$service		 ->execute();
	$mode_kwitansi 	 = $service->getContent();
	$service 		 = new ServiceConsumer($db,"get_jenis_patient",NULL,"registration");
	$service 		 ->execute();
	$jenis_patient	 = $service->getContent();
	$jenis_patient[] = array("name"=>"","value"=>"","default"=>"1");
    
    $tindakan 	 = new OptionBuilder();
	$tindakan 	 ->addSingle("","1")
			  	 ->addSingle("Tanpa Tindakan","0");
    $plafon 	 ->getUItable()
				 ->addModal("dari", "date", "Dari", "","n")
				 ->addModal("sampai", "date", "Sampai", "","n")
				 ->addModal("selesai", "select", "Selesai", $selesai->getContent(),"y")
				 ->addModal("uri", "select", "Jenis", $uri->getContent(),"y")
				 ->addModal("jenislayanan", "select", "Layanan", $ruangan,"y")
				 ->addModal("kamar_inap", "select", "Kamar Inap", $ruangan_inap,"y")
				 ->addModal("jenis_pasien", "select", "Cara Bayar", $jenis_patient,"y")
				 ->addModal("kwitansi", "select", "Kwitansi", $mode_kwitansi,"y")
            	 ->addModal("tindakan", "select", "Tindakan", $tindakan->getContent(),"y")
            	 ->addModal("nrm_pasien", "text", "NRM", "","y")
				 ->addModal("noreg_pasien", "text", "No. Reg", "","y");
	$search 	 = new Button("","","");
	$search 	 ->addClass("btn-primary")
		 		 ->setIcon("fa fa-search")
		 		 ->setAction("rekap_tagihan_pasien.view()")
		 		 ->setIsButton(Button::$ICONIC);
	$all_init  	 = new Button("","","");
	$all_init  	 ->addClass("btn-primary")
			   	 ->setIsButton(Button::$ICONIC)
			   	 ->setIcon("fa fa-forward")
			   	 ->setAction("rekap_tagihan_pasien.init_all_tagihan_pasien()");
	$all_print 	 = new Button("","","");
	$all_print 	 ->addClass("btn-primary")
			   	 ->setIsButton(Button::$ICONIC)
			   	 ->setIcon("fa fa-print")
			   	 ->setAction("rekap_tagihan_pasien.init_all_print()");	
	$all_koreksi = new Button("","","");
	$all_koreksi ->addClass("btn-primary")
				 ->setIsButton(Button::$ICONIC)
				 ->setIcon("fa fa-upload")
				 ->setAction("rekap_tagihan_pasien.init_all_koreksi_carabayar()");

	$plafon	->getForm()
			->addElement("",$search)
			->addElement("",$all_init)
			->addElement("",$all_print)
			->addElement("",$all_koreksi);
	$plafon ->getUItable()->clearContent();
	$plafon ->addJSColumn("nama_pasien")
			->addJSColumn("nama_dokter")
			->addJSColumn("id")
			->addJSColumn("nrm")
			->addJSColumn("nobpjs")
			->addJSColumn("plafon_bpjs")
			->addJSColumn("diagnosa_bpjs")
			->addJSColumn("tindakan_bpjs")
			->addJSColumn("keterangan_bpjs")
			->addJSColumn("kunci_bpjs");
	$plafon ->getModal()->setTitle("Plafon");
	$plafon ->addViewData("dari","dari")
			->addViewData("sampai","sampai")
			->addViewData("uri","uri")
			->addViewData("selesai","selesai") 
			->addViewData("tindakan","tindakan")
			->addViewData("jenislayanan","jenislayanan")
			->addViewData("kamar_inap","kamar_inap")
			->addViewData("jenis_pasien","jenis_pasien")
			->addViewData("noreg_pasien","noreg_pasien")
			->addViewData("nrm_pasien","nrm_pasien");	
	$close  = new Button("", "", "Batal");
	$close	->addClass("btn-primary")
			->setIsButton(Button::$ICONIC_TEXT)
			->setIcon("fa fa-close")
			->setAction("location.reload()");
	$person = new LoadingBar("rtb_person_bar", "");
	$modal  = new Modal("rtb_load_modal", "", "Process in Progress");
	$modal	->addHTML($person->getHtml(),"after")
			->addFooter($close);
	echo "<div id='tagihan_place_init'></div>";
	echo $modal->getHtml();
}

$plafon->initialize();
?>

