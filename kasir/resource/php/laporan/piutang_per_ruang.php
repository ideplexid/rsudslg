<?php 

/**
 * this sistem used to resume
 * all income that incoming (even or not)
 * in given timeframe
 * 
 * @author      : Nurul Huda
 * @copyright   : goblooge@gmail.com
 * @since       : 17 Aug 2016
 * @database    : smis_ksr_bayar
 * @service     : get_piutang
 * @version     : 1.6.0
 * @license     : LGPLv3
 * */

global $db;
global $user;
require_once 'smis-base/smis-include-service-consumer.php';
if(isset($_POST['super_command']) && $_POST['super_command']=="total") {
    $data['dari']   = $_POST['dari'];
	$data['sampai'] = $_POST['sampai'];
	$serv           = new ServiceConsumer($db, "get_pasien_notified",$data,"registration");
	$serv ->execute();
	$data           = $serv->getContent();
	$res            = new ResponsePackage();
	$res ->setStatus(ResponsePackage::$STATUS_OK);
	$res ->setContent($data);
	echo json_encode($res->getPackage());
	return;
}


if(isset($_POST['super_command']) && $_POST['super_command']=="limit") {
    $id['noreg_pasien']     = $_POST['noreg_pasien'];
	$up['akunting_posted']  = $_POST['akunting_notify_date'];
    $dbtable                = new DBTable($db,"smis_ksr_kolektif");
    $dbtable ->update($up,$id);
    $res                    = new ResponsePackage();
	$res ->setStatus(ResponsePackage::$STATUS_OK);
	$res ->setContent("");
	echo json_encode($res->getPackage());
	return;
}

$query    = "SELECT DISTINCT ruangan FROM smis_ksr_kolektif ORDER BY ruangan ASC";
$ruangan  = $db->get_result($query);
$header       = array("Tanggal","Nama","NRM","No. Reg");
foreach($ruangan as $r){
    $header[] = ArrayAdapter::format("unslug",$r->ruangan);
}
$header[]   = "Total";
$uitable    = new Table($header);
$uitable=new Table($header);
$uitable->setName("piutang_per_ruang")
		->setActionEnable(false)
		->setFooterVisible(false);

if(isset($_POST['command']) && $_POST['command']!=""){
    $query="SELECT nama_pasien, noreg_pasien, nrm_pasien, ";
    foreach($ruangan as $r){
        $query .= " sum(if(ruangan='".$r->ruangan."',total,0)) as `".$r->ruangan."`, ";
    }
    $query.=" sum(total) as total, akunting_posted FROM smis_ksr_kolektif ";
    $qc="SELECT count(*) FROM smis_ksr_kolektif ";
    
	$dbtable=new DBTable($db, "smis_ksr_kolektif");
	$dbtable->setPreferredQuery(true,$query,$qc);
    $dbtable->setCountEnable(false);
    $dbtable->setGroupBy(true,"noreg_pasien");
	$dbtable->setShowAll(true);
    $dbtable->setUseWhereforView(true);
    $dbtable->addCustomKriteria(" akunting_only = ","0");
	$dbtable->addCustomKriteria(" akunting_posted >= ","'".$_POST['dari']."'");
	$dbtable->addCustomKriteria(" akunting_posted < ","'".$_POST['sampai']."'");
	$dbtable->addCustomKriteria(" akunting_posted != ","'0000-00-00 00:00:00'");
	
    $adapter=new SummaryAdapter();
	$adapter->add("Nama", "nama_pasien");
	$adapter->add("NRM", "nrm_pasien");
	$adapter->add("No. Reg", "noreg_pasien");
	$adapter->add("Tanggal", "akunting_posted","date d M Y H:i");
    $adapter->add("Total", "total","nonzero-money Rp.");
	$adapter->addFixValue("Nama", "Total");
    foreach($ruangan as $r){
        $adapter->addSummary(ArrayAdapter::format("unslug",$r->ruangan),$r->ruangan,"money Rp.");
        $adapter->add(ArrayAdapter::format("unslug",$r->ruangan),$r->ruangan,"money Rp.");
    }
	$adapter->addSummary("Total", "total","nonzero-money Rp.");	
	$dbres=new DBResponder($dbtable, $uitable, $adapter);
	$hasil=$dbres->command($_POST['command']);
	echo json_encode($hasil);
	return;
}


$uitable->clearContent();
$uitable->addModal("dari", "date", "Dari", "")
		->addModal("sampai", "date", "Sampai", "");
$action=new Button("","","View");
$action->setClass("btn-primary")
	   ->setIsButton(Button::$ICONIC)
	   ->setIcon("icon-white fa fa-circle-o-notch")
	   ->setAction("piutang_per_ruang.rekaptotal()");
$btn_froup=new ButtonGroup("");
$btn_froup->addButton($action);
$form=$uitable
	  ->getModal()
	  ->setTitle("kasir")
	  ->getForm()
	  ->addElement("",$btn_froup);

$close=new Button("", "", "Batal");
$close	->addClass("btn-primary")
		->setIsButton(Button::$ICONIC_TEXT)
		->setIcon("fa fa-close")
		->setAction("piutang_per_ruang.batal()");

$load=new LoadingBar("rekap_piutang_per_ruang_bar", "");
$modal=new Modal("rekap_piutang_per_ruang_modal", "", "Processing...");
$modal	->addHTML($load->getHtml(),"after")
		->addFooter($close);

echo $form->getHtml();
echo "<div class='clear'></div>";
echo "<div id='result_piutang_per_ruang'>".$uitable->getHtml()."</div>";
echo $modal->getHtml();
echo addJS("framework/bootstrap/js/bootstrap-datepicker.js");
echo addJS("framework/smis/js/table_action.js");
echo addCSS("framework/bootstrap/css/datepicker.css");
echo addJS ( "base-js/smis-base-loading.js");
echo addJS ( "kasir/resource/js/piutang_per_ruang.js",false );
?>
