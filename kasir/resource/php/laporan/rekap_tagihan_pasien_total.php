<?php 
global $db;
global $user;
require_once 'smis-base/smis-include-service-consumer.php';
if(isset($_POST['super_command']) && $_POST['super_command']=="total") {
	require_once 'smis-libs-class/ServiceProviderList.php';
	$dbtable=new DBTable($db, "smist_ksr_rekap_total");
	$serv=new ServiceConsumer($db, "get_patient_masuk");
	$serv->addData("command", "count");
	$serv->addData("dari", $_POST['dari']);
	$serv->addData("sampai", $_POST['sampai']);	
	$serv->execute();
	$data=$serv->getContent();
	$res=new ResponsePackage();
	$res->setStatus(ResponsePackage::$STATUS_OK);
	$res->setContent($data);
	echo json_encode($res->getPackage());
	return;
}

if(isset($_POST['super_command']) && $_POST['super_command']=="limit") {
	$data=array();
	$response=new ServiceConsumer($db, "get_patient_masuk");
	$response->addData("command", "list");
	$response->addData("dari", $_POST['dari']);
	$response->addData("sampai", $_POST['sampai']);
	$response->addData("halaman", $_POST['halaman']);	
	$response->setMode(ServiceConsumer::$SINGLE_MODE);
	$response->execute();
	$list=$response->getContent();
	$dbtable=new DBTable($db, "smist_ksr_rekap_total");
	require_once "smis-libs-bpjs/TotalTagihanServiceConsumer.php";
	
	foreach($list as $x){		
		$d['nrm_pasien']=$x['nrm'];
		$d['nama_pasien']=$x['nama_pasien'];
		$d['noreg_pasien']=$x['id'];
        $d['masuk']=$x['tanggal'];
		$d['pulang']=$x['tanggal_pulang'];
		$d['ruangan']=($x['uri']=="1"?$x['kamar_inap']:$x['jenislayanan']);
        $total=new TotalTagihanServiceConsumer($db,$d['noreg_pasien']);
        $total->execute();
        $d['tagihan']=$total->getContent();
        
        $query="SELECT 
                SUM(IF(metode LIKE 'cash%',nilai,0)) as cash, 
                SUM(IF(metode LIKE 'bank',nilai,0)) as bank, 
                SUM(IF(metode LIKE 'asuransi',nilai,0)) as asuransi, 
                SUM(IF(metode LIKE 'diskon',nilai,0)) as diskon
                FROM smis_ksr_bayar WHERE noreg_pasien='".$noreg."' AND prop!='del'";
        $bayar=$db->get_row($query);
        
        $d['asuransi']=$bayar->asuransi;
		$d['bank']=$bayar->bank;
		$d['cash']=$bayar->cash;
		$d['diskon']=$bayar->diskon;
        $d['selisih']=$d['asuransi']+$d['bank']+$d['cash']+$d['diskon']-$d['tagihan'];
        $dbtable->insertOrUpdate($data,array("noreg_pasien"=>$d['noreg_pasien']));
	}
	$res=new ResponsePackage();
	$res->setStatus(ResponsePackage::$STATUS_OK);
	$res->setContent($list);
	echo json_encode($res->getPackage());
	return;
}

$header=array(	"No.","NRM","No.Reg","Nama",
                "Masuk","Pulang","Ruang",
				"Tagihan","Asuransi","Bank","Cash","Diskon","Selisih");
$uitable=new Table($header);
$uitable->setName("rekap_tagihan_pasien_total")
		->setActionEnable(false);

if(isset($_POST['command']) && $_POST['command']=="excel"){
	require_once "kasir/snippet/create_excel_rekap_tagihan_pasien_total.php";
    return;
}

if(isset($_POST['command']) && $_POST['command']!=""){
	$dbtable=new DBTable($db, "smist_ksr_rekap_total");
	$dbtable->setOrder(" masuk ASC");
	
    if(isset($_POST['filter']) && $_POST['filter']!=""){
        switch (($_POST['filter'])){
           case "ada_asuransi"  :  $dbtable->addCustomKriteria(" asuransi "," >0 "); break;
           case "ada_bank"      :  $dbtable->addCustomKriteria(" bank "," >0 ");     break;
           case "ada_cash"      :  $dbtable->addCustomKriteria(" cash "," >0 ");     break;
           case "ada_diskon"    :  $dbtable->addCustomKriteria(" diskon "," >0 ");   break;
        }
    }
    
	$adapter=new SummaryAdapter();
	$adapter->setUseNumber(true, "No.","back.");
	$adapter->add("NRM", "nrm_pasien","digit8");
    $adapter->add("No.Reg", "noreg_pasien","digit8");
	$adapter->add("Nama", "nama_pasien");
	$adapter->add("Ruang", "ruangan","unslug");    
	$adapter->add("Masuk", "masuk","date d M Y");
	$adapter->add("Pulang", "pulang","date d M Y");    
	$adapter->add("Tagihan", "tagihan","money Rp.");
	$adapter->add("Asuransi", "asuransi","money Rp.");
	$adapter->add("Bank", "bank","money Rp.");
	$adapter->add("Cash", "cash","money Rp.");
    $adapter->add("Diskon", "diskon","money Rp.");
    $adapter->add("Selisih", "selisih","money Rp.");
    
    $adapter->addSummary("Selisih", "selisih","money Rp.");
    $adapter->addSummary("Tagihan", "tagihan","money Rp.");
	$adapter->addSummary("Asuransi", "asuransi","money Rp.");
	$adapter->addSummary("Bank", "bank","money Rp.");
	$adapter->addSummary("Cash", "cash","money Rp.");
    $adapter->addSummary("Diskon", "diskon","money Rp.");
    $adapter->addSummary("Selisih", "selisih","money Rp.");
    $adapter->addFixValue("Nama", "<strong>Total</strong>");
    
    $dbres=new DBResponder($dbtable, $uitable, $adapter);
	$hasil=$dbres->command($_POST['command']);
	echo json_encode($hasil);
	return;
}

$option=new OptionBuilder();
$option->add("Semua","","1");
$option->add("Ada Asuransi","ada_asuransi","0");
$option->add("Ada Cash","ada_cash","0");
$option->add("Ada Bank","ada_bank","0");
$option->add("Ada Diskon","ada_diskon","0");

$uitable->clearContent();
$uitable->addModal("dari", "date", "Dari", "")
		->addModal("sampai", "date", "Sampai", "")
        ->addModal("filter", "select", "Filter", $option->getContent());
$action=new Button("","","View");
$action->setClass("btn-primary")
	   ->setIsButton(Button::$ICONIC)
	   ->setIcon("icon-white fa fa-circle-o-notch")
	   ->setAction("rekap_tagihan_pasien_total.rekaptotal()");
$view=new Button("","","View");
$view->setClass("btn-primary")
		->setIsButton(Button::$ICONIC)
		->setIcon("fa fa-refresh")
		->setAction("rekap_tagihan_pasien_total.view()");
$excel=new Button("","","Excel");
$excel->setClass("btn-primary")
	->setIsButton(Button::$ICONIC)
	->setIcon("fa fa-file-excel-o")
	->setAction("rekap_tagihan_pasien_total.excel()");


$btng=new ButtonGroup("");
$btng->addButton($action);
$btng->addButton($view);
$btng->addButton($excel);

$form=$uitable
	  ->getModal()
	  ->setTitle("Medical Record")
	  ->getForm()
	  ->addElement("",$btng);

$close=new Button("", "", "Batal");
$close	->addClass("btn-primary")
		->setIsButton(Button::$ICONIC_TEXT)
		->setIcon("fa fa-close")
		->setAction("rekap_tagihan_pasien_total.batal()");

$load=new LoadingBar("rekap_rekap_tagihan_pasien_total_bar", "");
$modal=new Modal("rekap_rekap_tagihan_pasien_total_modal", "", "Processing...");
$modal	->addHTML($load->getHtml(),"after")
		->addFooter($close);

echo $form->getHtml();
echo "<div class='clear'></div>";
echo "<div id='result_rekap_tagihan_pasien_total'>".$uitable->getHtml()."</div>";
echo $modal->getHtml();
echo addJS("framework/bootstrap/js/bootstrap-datepicker.js");
echo addJS("framework/smis/js/table_action.js");
echo addCSS("framework/bootstrap/css/datepicker.css");
echo addJS ( "base-js/smis-base-loading.js");
?>

<script type="text/javascript">
	var rekap_tagihan_pasien_total;
	var rekap_tagihan_pasien_total_karyawan;
	var rekap_tagihan_pasien_total_data;
	var IS_rekap_tagihan_pasien_total_RUNNING;
	$(document).ready(function(){
		$('.mydate').datepicker();
		rekap_tagihan_pasien_total=new TableAction("rekap_tagihan_pasien_total","kasir","rekap_tagihan_pasien_total",new Array());
		rekap_tagihan_pasien_total.addRegulerData=function(data){
			data['dari']=$("#rekap_tagihan_pasien_total_dari").val();
			data['sampai']=$("#rekap_tagihan_pasien_total_sampai").val();
			data['filter']=$("#rekap_tagihan_pasien_total_filter").val();
			$("#dari_table_rekap_tagihan_pasien_total").html(getFormattedDate(data['dari']));
			$("#sampai_table_rekap_tagihan_pasien_total").html(getFormattedDate(data['sampai']));			
			return data;
		};

		rekap_tagihan_pasien_total.batal=function(){
			IS_rekap_tagihan_pasien_total_RUNNING=false;
			$("#rekap_rekap_tagihan_pasien_total_modal").modal("hide");
		};
		
		rekap_tagihan_pasien_total.afterview=function(json){
			if(json!=null){
				$("#kode_table_rekap_tagihan_pasien_total").html(json.nomor);
				$("#waktu_table_rekap_tagihan_pasien_total").html(json.waktu);
				rekap_tagihan_pasien_total_data=json;
			}
		};

		rekap_tagihan_pasien_total.rekaptotal=function(){
			if(IS_rekap_tagihan_pasien_total_RUNNING) return;
			$("#rekap_rekap_tagihan_pasien_total_bar").sload("true","Fetching total data",0);
			$("#rekap_rekap_tagihan_pasien_total_modal").modal("show");
			IS_rekap_tagihan_pasien_total_RUNNING=true;
			var d=this.getRegulerData();
			d['super_command']="total";
			$.post("",d,function(res){
				var all=getContent(res);
				if(all!=null) {
					var total=Number(all);
					rekap_tagihan_pasien_total.rekaploop(0,total);
				} else {
					$("#rekap_rekap_tagihan_pasien_total_modal").modal("hide");
					IS_rekap_tagihan_pasien_total_RUNNING=false;
				}
			});
		};

		rekap_tagihan_pasien_total.excel=function(){
			var d=this.getRegulerData();
			d['command']="excel";
			download(d);
		};

		rekap_tagihan_pasien_total.rekaploop=function(current,total){
			if(current>=total || !IS_rekap_tagihan_pasien_total_RUNNING) {
				$("#rekap_rekap_tagihan_pasien_total_modal").modal("hide");
				IS_rekap_tagihan_pasien_total_RUNNING=false;
				rekap_tagihan_pasien_total.view();
				return;
			}
			var d=this.getRegulerData();
			d['super_command']="limit";
			d['halaman']=current;
			$.post("",d,function(res){
				var ct=getContent(res);
				var u=ct[0]['nama_pasien']+"  "+ct[0]['nrm'];
				$("#rekap_rekap_tagihan_pasien_total_bar").sload("true"," Processing - "+u+"... [ "+(current+1)+" / "+total+" ] ",(current*100/total));
				setTimeout(function(){rekap_tagihan_pasien_total.rekaploop(++current,total)},300);
			});
		};
				
	});
</script>