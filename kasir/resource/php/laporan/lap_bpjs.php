<?php
global $db;
require_once 'smis-base/smis-include-service-consumer.php';


$header=array("Nama","Alamat","Telpon");
$uitable=new Table($header);
$uitable->setModel(Table::$SELECT);
$uitable->setName("lap_bpjs_BPJS");
$adapter=new SimpleAdapter();
$adapter->add("Nama", "nama");
$adapter->add("Alamat", "alamat");
$adapter->add("Telpon", "telpon");
$service=new ServiceResponder($db, $uitable, $adapter, "get_bpjs");
$super=new SuperCommand();
$super->addResponder("lap_bpjs_BPJS", $service);
$data=$super->initialize();
if($data!=null){
	echo $data;
	return;
}

$header=array ('Tanggal',"Asuransi BPJS","Nama","Noreg","NRM","Ruangan","Keterangan","Carabayar",'Nilai',"No Bukti","Cair","Asal Data" );
$uitable = new Table ( $header, "", NULL, true);
$head="<tr><td colspan='10' style='text-align:center'><strong >Laporan Pembayaran BPJS</strong></td></tr>";
$uitable->addHeader("before", $head);
$head="<tr><td colspan='10' style='text-align:center'><strong id='tgl_lap_bpjs_print'></strong></td></tr>";
$uitable->addHeader("before", $head);
$uitable->setFooterVisible(false);
$uitable->setAddButtonEnable(false);
$uitable->setEditButtonEnable(true);
$uitable->setDelButtonEnable(false);
$uitable->setReloadButtonEnable(false);

$load_kasir=new Button("", "", "");
$load_kasir->setIsButton(Button::$ICONIC);
$load_kasir->setIcon("fa fa-pencil");
$load_kasir->setClass("btn-primary");
$uitable->addContentButton('loading_kasir', $load_kasir);
$uitable->setName ( "lap_bpjs" );
if (isset ( $_POST ['command'] )) {
	require_once 'kasir/class/adapter/LapBPJSAdapter.php';
    require_once 'kasir/class/responder/LaporanBPJSResponder.php';
	$adapter = new LapBPJSAdapter ();
	$adapter->add ( "Nama", "nama_pasien");
	$adapter->add ( "Asuransi BPJS", "nama_asuransi");
	$adapter->add ( "NRM", "nrm_pasien","digit6");
	$adapter->add ( "Noreg", "noreg_pasien","digit6");	
	$adapter->add ( "Tanggal", "waktu", "date d M Y H:i" );
	$adapter->add ( "Nilai", "nilai", "money Rp." );
	$adapter->add ( "Cair", "terklaim", "trivial_1_Ya_Belum" );
	$adapter->add ( "Keterangan", "keterangan" );
	$adapter->add ( "No Bukti", "no_bukti" );
	//$adapter->add ( "Ruangan", "ruangan","unslug");
	$adapter->add ( "Carabayar", "carabayar","unslug");
	$adapter->add ( "Tgl Klaim", "tgl_klaim","date d M Y");
	$adapter->add ( "Asal Data", "origin","unslug");
	
	$dbtable = new DBTable ( $db, "smis_ksr_bayar" );
	$dbtable->setFetchMethode ( DBTable::$ARRAY_FETCH );
	$dbtable->addCustomKriteria ( "metode", "='asuransi'" );	
	$dbtable->addCustomKriteria ( "keterangan", " LIKE '%bpjs%'" );
	$dbres = new LaporanBPJSResponder ( $dbtable, $uitable, $adapter );
	if($dbres->isView() || $_POST['command']=="excel"){
		if($_POST['dari']!="")  	$dbtable->addCustomKriteria(null, " waktu>='".$_POST['dari']."' ");
		if($_POST['sampai']!="")	$dbtable->addCustomKriteria(null, " waktu<'".$_POST['sampai']."' ");
		if($_POST['cair']!="")		$dbtable->addCustomKriteria("terklaim", " ='".$_POST['cair']."' ");
		if($_POST['BPJS']!="")		$dbtable->addCustomKriteria("nama_BPJS", " ='".$_POST['BPJS']."' ");
        if($_POST['origin']!="")	$dbtable->addCustomKriteria("origin", " ='".$_POST['origin']."' ");
			$dbtable->setShowAll(true);
	}
	$data = $dbres->command ( $_POST ['command'] );
	echo json_encode ( $data );
	return;
}

$terklaim=new OptionBuilder();
$terklaim->add("","");
$terklaim->add("Sudah","1");
$terklaim->add("Belum","0");

$query="SELECT DISTINCT origin FROM smis_ksr_bayar WHERE prop!='del' ";
$origin=new OptionBuilder();
$origin->add(" - Semua - ","",1);
$res=$db->get_result($query);
foreach($res as $x){
    if($x->origin==""){
        continue;
    }
    $origin->add(ArrayAdapter::slugFormat("unslug",$x->origin),$x->origin);
}

$uitable->addModal ( "dari", "datetime", "Dari", "" );
$uitable->addModal ( "sampai", "datetime", "Sampai", "" );
$uitable->addModal ( "BPJS", "chooser-lap_bpjs-lap_bpjs_BPJS", "BPJS", "" );
$uitable->addModal ( "k_cair", "select", "Cair",$terklaim->getContent() );
$uitable->addModal ( "origin", "select", "Asal Data",$origin->getContent() );

$form=$uitable->getModal ()->getForm();
$uitable->clearContent();
$uitable->addModal("id","hidden","","");
$uitable->addModal("terklaim","checkbox","Terklaim","");
$uitable->addModal("tgl_klaim","date","Tgl Klaim","");
$uitable->addModal("no_bukti","text","No. Bukti","");
$uitable->addModal("nilai","money","Nilai","");
$uitable->addModal("keterangan","textarea","Keterangan","");
$uitable->addModal("waktu","datetime","Waktu","","y",NULL,true);

$cari=new Button("", "", "Search");
$cari->setIsButton(Button::$ICONIC);
$cari->setIcon("fa fa-search");
$cari->setClass("btn-primary");
$cari->setAction("lap_bpjs.view()");

$print=new Button("", "", "Search");
$print->setIsButton(Button::$ICONIC);
$print->setIcon("fa fa-print");
$print->setClass("btn-primary");
$print->setAction("lap_bpjs.print()");

$excel=new Button("", "", "Search");
$excel->setIsButton(Button::$ICONIC);
$excel->setIcon("fa fa-file-excel-o");
$excel->setClass("btn-primary");
$excel->setAction("lap_bpjs.excel()");
$excel->addAtribute("data-content","Print Excel");
$excel->addAtribute("data-toggle","popover");

$form->addElement("",$cari);
$form->addElement("",$print);
$form->addElement("",$excel);

$back=new Button("", "", "");
$back->setIsButton(Button::$ICONIC);
$back->setIcon("fa fa-toggle-up");
$back->setClass("btn-primary");
$back->setAction("reup_bpjs()");


echo "<div id='main_BPJS'>";
	echo $form->getHtml ();	
	echo $uitable->getModal()->setTitle("Klaim BPJS")->getHtml();
	echo $uitable->getHtml ();
echo "</div>";


echo "<div id='sub_menu_BPJS'>";
	echo $back->getHtml();
	echo "<div id='sub_menu_BPJS_content'>";
	echo "</div>";	
echo "</div>";


echo addJS ( "framework/bootstrap/js/bootstrap-datetimepicker.js" );
echo addCSS ( "framework/bootstrap/css/bootstrap-datetimepicker.css" );

echo addJS ( "kasir/resource/js/lap_bpjs.js",false );
echo addCSS ( "kasir/resource/css/lap_bpjs.css",false );

?>