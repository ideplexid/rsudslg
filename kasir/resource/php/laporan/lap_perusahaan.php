<?php
global $db;
require_once 'smis-base/smis-include-service-consumer.php';

$header=array("Nama","Alamat","Telpon");
$uitable=new Table($header);
$uitable->setModel(Table::$SELECT);
$uitable->setName("lap_perusahaan_perusahaan");
$adapter=new SimpleAdapter();
$adapter->add("Nama", "nama");
$adapter->add("Alamat", "alamat");
$adapter->add("Telpon", "telpon");
$service=new ServiceResponder($db, $uitable, $adapter, "get_perusahaan");
$super=new SuperCommand();
$super->addResponder("lap_perusahaan_perusahaan", $service);
$data=$super->initialize();
if($data!=null){
	echo $data;
	return;
}

$header=array ('Tanggal',"Asuransi","Perusahaan","Nama","Noreg","NRM","Ruangan","Carabayar",'Nilai',"No Bukti","Cair","Asal Data" );
$uitable = new Table ( $header, "", NULL, true);
$head="<tr><td colspan='10' style='text-align:center'><strong >Laporan Perusahaan</strong></td></tr>";
$uitable->addHeader("before", $head);
$head="<tr><td colspan='10' style='text-align:center'><strong id='tgl_lap_perusahaan_print'></strong></td></tr>";
$uitable->addHeader("before", $head);
$uitable->setFooterVisible(false);
$uitable->setAddButtonEnable(false);
$uitable->setEditButtonEnable(true);
$uitable->setDelButtonEnable(false);
$uitable->setReloadButtonEnable(false);

$load_kasir=new Button("", "", "");
$load_kasir->setIsButton(Button::$ICONIC);
$load_kasir->setIcon("fa fa-pencil");
$load_kasir->setClass("btn-primary");
$uitable->addContentButton('loading_kasir', $load_kasir);
$uitable->setName ( "lap_perusahaan" );
if (isset ( $_POST ['command'] )) {
	require_once 'kasir/adapter/LapPerusahaanAdapter.php';
    require_once "kasir/class/responder/LaporanPerusahaanResponder.php";
	$adapter = new LapperusahaanAdapter ();
	$adapter->add ( "Nama", "nama_pasien");
	$adapter->add ( "Perusahaan", "nama_perusahaan");
	$adapter->add ( "Asuransi", "nama_asuransi");
	$adapter->add ( "NRM", "nrm_pasien","digit6");
	$adapter->add ( "Noreg", "noreg_pasien","digit6");	
	$adapter->add ( "Tanggal", "waktu", "date d M Y H:i" );
	$adapter->add ( "Nilai", "nilai", "money Rp." );
	$adapter->add ( "Cair", "terklaim", "trivial_1_Ya_Belum" );
	$adapter->add ( "Keterangan", "keterangan" );
	$adapter->add ( "No Bukti", "no_bukti" );
	$adapter->add ( "Ruangan", "ruangan","unslug");
	$adapter->add ( "Carabayar", "carabayar","unslug");
	$adapter->add ( "Tgl Klaim", "tgl_klaim","date d M Y");
	$adapter->add ( "Asal Data", "origin","unslug");
	
	$dbtable = new DBTable ( $db, "smis_ksr_bayar" );
	$dbtable->setFetchMethode ( DBTable::$ARRAY_FETCH );
	$dbtable->addCustomKriteria ( "metode", "='asuransi'" );	
	$dbtable->addCustomKriteria ( "id_perusahaan", "!='0'" );	
	$dbres = new LaporanPerusahaanResponder ( $dbtable, $uitable, $adapter );
	if($dbres->isView() || $_POST['command']=="excel"){
		if($_POST['dari']!="")  	    $dbtable->addCustomKriteria(null, " waktu>='".$_POST['dari']."' ");
		if($_POST['sampai']!="")	    $dbtable->addCustomKriteria(null, " waktu<'".$_POST['sampai']."' ");
		if($_POST['cair']!="")		    $dbtable->addCustomKriteria("terklaim", " ='".$_POST['cair']."' ");
		if($_POST['origin']!="")		$dbtable->addCustomKriteria("origin", " ='".$_POST['origin']."' ");
        if($_POST['perusahaan']!="")	$dbtable->addCustomKriteria("nama_perusahaan", " ='".$_POST['perusahaan']."' ");
			$dbtable->setShowAll(true);
	}
	$data = $dbres->command ( $_POST ['command'] );
	echo json_encode ( $data );
	return;
}

$query="SELECT DISTINCT origin FROM smis_ksr_bayar WHERE prop!='del' ";
$origin=new OptionBuilder();
$origin->add(" - Semua - ","",1);
$res=$db->get_result($query);
foreach($res as $x){
    if($x->origin==""){
        continue;
    }
    $origin->add(ArrayAdapter::slugFormat("unslug",$x->origin),$x->origin);
}

$terklaim=new OptionBuilder();
$terklaim->add("","");
$terklaim->add("Sudah","1");
$terklaim->add("Belum","0");

$uitable->addModal ( "dari", "datetime", "Dari", "" );
$uitable->addModal ( "sampai", "datetime", "Sampai", "" );
$uitable->addModal ( "perusahaan", "chooser-lap_perusahaan-lap_perusahaan_perusahaan", "Perusahaan", "" );
$uitable->addModal ( "k_cair", "select", "Cair",$terklaim->getContent() );
$uitable->addModal ( "origin", "select", "Asal Data",$origin->getContent() );

$form=$uitable->getModal ()->getForm();
$uitable->clearContent();
$uitable->addModal("id","hidden","","");
$uitable->addModal("terklaim","checkbox","Terklaim","");
$uitable->addModal("tgl_klaim","date","Tgl Klaim","");
$uitable->addModal("no_bukti","text","No. Bukti","");
$uitable->addModal("nilai","money","Nilai","");
$uitable->addModal("keterangan","textarea","Keterangan","");
$uitable->addModal("waktu","datetime","Waktu","","y",NULL,true);

$cari=new Button("", "", "Search");
$cari->setIsButton(Button::$ICONIC);
$cari->setIcon("fa fa-search");
$cari->setClass("btn-primary");
$cari->setAction("lap_perusahaan.view()");

$print=new Button("", "", "Search");
$print->setIsButton(Button::$ICONIC);
$print->setIcon("fa fa-print");
$print->setClass("btn-primary");
$print->setAction("lap_perusahaan.print()");

$excel=new Button("", "", "Search");
$excel->setIsButton(Button::$ICONIC);
$excel->setIcon("fa fa-file-excel-o");
$excel->setClass("btn-primary");
$excel->setAction("lap_perusahaan.excel()");
$excel->addAtribute("data-content","Print Excel");
$excel->addAtribute("data-toggle","popover");

$form->addElement("",$cari);
$form->addElement("",$print);
$form->addElement("",$excel);

$back=new Button("", "", "");
$back->setIsButton(Button::$ICONIC);
$back->setIcon("fa fa-toggle-up");
$back->setClass("btn-primary");
$back->setAction("reup()");


echo "<div id='main_perusahaan'>";
	echo $form->getHtml ();	
	echo $uitable->getModal()->setTitle("Klaim Perusahaan")->getHtml();
	echo $uitable->getHtml ();
echo "</div>";


echo "<div id='sub_menu_perusahaan'>";
	echo $back->getHtml();
	echo "<div id='sub_menu_perusahaan_content'>";
	echo "</div>";	
echo "</div>";


echo addJS ( "framework/bootstrap/js/bootstrap-datetimepicker.js" );
echo addCSS ( "framework/bootstrap/css/bootstrap-datetimepicker.css" );

?>

<script type="text/javascript">
	var lap_perusahaan;
	var lap_perusahaan_perusahaan;

	function reup(){
		$("#sub_menu_perusahaan_content").html("");	
		$("#sub_menu_perusahaan").hide("fast");
		$("#main_perusahaan").show("fast");
		lap_perusahaan.view();
	}
	
	function load_registration(noreg,perusahaan_json){
		var data={
				page:"kasir",
				action:"pembayaran_patient",
				super_command:"",
				prototype_name:"",
				prototype_slug:"",
				prototype_implement:"",
				list_registered_select:noreg
			};
		showLoading();
		$.post("",data,function(res){
			$("#sub_menu_perusahaan_content").html(res);	
			$("#sub_menu_perusahaan").show("fast");
			$("#main_perusahaan").hide("fast");
			dismissLoading();
		});
	}
	
	
	//var employee;
	$(document).ready(function(){
		$(".mydatetime").datetimepicker({minuteStep:1});
		$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
		lap_perusahaan_perusahaan=new TableAction("lap_perusahaan_perusahaan","kasir","lap_perusahaan",Array());
		lap_perusahaan_perusahaan.setSuperCommand("lap_perusahaan_perusahaan");
		lap_perusahaan_perusahaan.selected=function(json){
			$("#lap_perusahaan_perusahaan").val(json.nama);
		};

		var column=new Array("id","nilai","terklaim","waktu","keterangan","no_bukti","tgl_klaim");
		lap_perusahaan=new TableAction("lap_perusahaan","kasir","lap_perusahaan",column);
		lap_perusahaan.loading_kasir=function(id){
			var data=this.getRegulerData();
			data['command']="edit";
			data['id']=id;
			showLoading();
			$.post("",data,function(res){
				var json=getContent(res);
				var noreg=json.noreg_pasien;
				load_registration(noreg,json);
				dismissLoading();
			});
		};

		
		lap_perusahaan.getRegulerData=function(){
			var reg_data={	
					page:this.page,
					action:this.action,
					super_command:this.super_command,
					prototype_name:this.prototype_name,
					prototype_slug:this.prototype_slug,
					dari:$("#"+this.prefix+"_dari").val(),
					sampai:$("#"+this.prefix+"_sampai").val(),
					cair:$("#"+this.prefix+"_k_cair").val(),
					perusahaan:$("#"+this.prefix+"_perusahaan").val()
					};
			var dr=getFormattedTime(reg_data['dari']);
			var sp=getFormattedTime(reg_data['sampai']);
			var hsl=dr+" - "+sp;
			$("#tgl_lap_perusahaan_print").html(hsl);
			return reg_data;
		};
	});
	</script>
	
	<style type="text/css">
		#sub_menu_perusahaan{display:none;}
		tbody#lap_perusahaan_list tr:last-child td:last-child div{display:none !important;}
	</style>