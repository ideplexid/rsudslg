<?php
global $db;

require_once("smis-base/smis-include-service-consumer.php");
require_once("kasir/class/responder/DokterServiceResponder.php");

$form = new Form("form", "", "Laporan Pendapatan Dokter Spesialis");
$dokterId = new Hidden("id_dokter", "id_dokter", "");

$dokterBtn = new Button("", "", "Pilih");
$dokterBtn->setClass("btn-info");
$dokterBtn->setAction("dokter_lps.chooser('dokter_lps', 'dokter_button', 'dokter_lps', dokter_lps)");
$dokterBtn->setIcon("icon-white icon-list-alt");
$dokterBtn->setIsButton(Button::$ICONIC);
$dokterBtn->setAtribute("id='dokter_browse'");

$dokterTxt = new Text("nama_dokter", "nama_dokter", "");
$dokterTxt->setClass("smis-one-option-input");
$dokterTxt->setAtribute("disabled='disabled'");

$dokterGroup = new InputGroup("");
$dokterGroup->addComponent($dokterTxt);
$dokterGroup->addComponent($dokterBtn);	

$fromDate = new Text("from_date", "from_date", "", Text::$DATE);	
$toDate = new Text("to_date", "to_date", "", Text::$DATE);

$potPdr = new Text("pot_pdr", "pot_pdr", "10");
$potObat = new Text("pot_obat", "pot_obat", "15");

$showBtn = new Button("", "", "Tampilkan");
$showBtn->setClass("btn-primary");
$showBtn->setIcon("icon-white icon-repeat");
$showBtn->setIsButton(Button::$ICONIC);
$showBtn->setAction("pendapatan_spesialis.view()");

$printBtn = new Button("", "", "Cetak");
$printBtn->setClass("btn-inverse");
$printBtn->setIcon("icon-white icon-print");
$printBtn->setIsButton(Button::$ICONIC);
$printBtn->setAction("pendapatan_spesialis.print()");

$btnGroup = new ButtonGroup("noprint");
$btnGroup->addButton($showBtn);
$btnGroup->addButton($printBtn);

$form->addElement("", $dokterId);
$form->addElement("Dokter", $dokterGroup);
$form->addElement("Waktu Awal", $fromDate);
$form->addElement("Waktu Akhir", $toDate);
$form->addElement("Pot PDR (%)", $potPdr);
$form->addElement("Pot Obat (%)", $potObat);
$form->addElement("", $btnGroup);
	
//dokter chooser:
$doctorTable = new Table(array("NIP", "Nama", "Jabatan"));
$doctorTable->setName("dokter_lps");
$doctorTable->setModel(Table::$SELECT);

$doctorAdapter = new SimpleAdapter();
$doctorAdapter->add("NIP", "nip");
$doctorAdapter->add("Nama", "nama");
$doctorAdapter->add("Jabatan", "nama_jabatan");
$dokterServiceResponder = new DokterServiceResponder($db, $doctorTable, $doctorAdapter, "employee");

$superCommand = new SuperCommand();
$superCommand->addResponder("dokter_lps", $dokterServiceResponder);
$init = $superCommand->initialize();
if ($init != null) {
	echo $init;
	return;
}	
			
$table = new Table(array("Noreg Pasien", "Nama Pasien", "PDR", "Pot PDR", "PDR - Pot", 
	"Biaya Obat", "Pot Obat", "Obat - Pot", "PDR + Obat", "Potongan", "Hak Dokter"));
$table->setName("pendapatan_spesialis");
$table->setAction(false);
$table->setFooterVisible(false);

$headers = array(
	"<tr class='show_print'>
		<td colspan='100' style='text-align:center;font-weight:bold;'>LAPORAN PENDAPATAN DOKTER SPESIALIS</td>
	</tr>", 
	"<tr class='show_print'>
		<td colspan='2'>Dokter</td> <td colspan='10' id='print_dokter_name'></td>
	</tr>",
	"<tr class='show_print'>
		<td colspan='2'>Potongan PDR</td><td colspan='10' id='print_pot_pdr'></td>
	</tr>",
	"<tr class='show_print'>
		<td colspan='2'>Potongan Biaya Obat</td><td colspan='10' id='print_pot_obat'></td>
	</tr>",
	"<tr class='show_print'>
		<td colspan='2'>Tanggal</td><td colspan='10' id='print_tanggal'></td>
	</tr>");
	
foreach ($headers as $header) {
	$table->addHeader("before", $header);
}
		
if (isset($_POST['command'])) {								
	$params['id_dokter'] = $_POST['id_dokter'];
	$params['from_date'] = $_POST['from_date'];
	$params['to_date'] = $_POST['to_date'];
			
	$service = new ServiceConsumer($db, "get_pendapatan_spesialis", $params, "depo_farmasi_irja");
	$service->setMode(ServiceConsumer::$CLEAN_BOTH);
	$contentResep = $service->execute()->getContent();
			
	$params['slug'] = 'poli_spesialis';
	$service = new ServiceConsumer($db, "get_pendapatan_spesialis", $params, "poli_spesialis_center");
	$service->setMode(ServiceConsumer::$CLEAN_BOTH);
	$contentRawat = $service->execute()->getContent();
	
	$potObat = $_POST['pot_obat'] / 100;
	$potPdr = $_POST['pot_pdr'] / 100;
			
	foreach ($contentResep as $val) {
		$noreg = intval($val['noreg_pasien']);
		$val['pot_obat'] = $potObat * $val['biaya_obat'];
		$val['net_obat'] = $val['biaya_obat'] - $val['pot_obat'];			
		$content[$noreg] = $val;
	}
	
	foreach ($contentRawat as $val) {
		$noreg = intval($val['noreg_pasien']);
		$val['pot_pdr'] = $potPdr * $val['pdr'];
		$val['net_pdr'] = $val['pdr'] - $val['pot_pdr'];			
		$content[$noreg] = isset($content[$noreg]) ? array_merge($val, $content[$noreg]) : $val;						
	}
		
	foreach ($content as &$val) {
		$val['total_pdr_obat'] = (isset($val['pdr']) ? $val['pdr'] : 0) + (isset($val['biaya_obat']) ? $val['biaya_obat'] : 0);
		$val['total_pot'] = (isset($val['pot_pdr']) ? $val['pot_pdr'] : 0) + (isset($val['pot_obat']) ? $val['pot_obat'] : 0);
		$val['total'] = $val['total_pdr_obat']  - $val['total_pot'];
	}
	
	ksort($content);
	
	$adapter = new SummaryAdapter();	
	$adapter->add("Noreg Pasien", "noreg_pasien");
	$adapter->add("Nama Pasien", "nama_pasien");
	$adapter->add("PDR", "pdr", "money Rp.");
	$adapter->add("Pot PDR", "pot_pdr", "money Rp.");
	$adapter->add("PDR - Pot", "net_pdr", "money Rp.");
	$adapter->add("Biaya Obat", "biaya_obat", "money Rp.");
	$adapter->add("Pot Obat", "pot_obat", "money Rp.");
	$adapter->add("Obat - Pot", "net_obat", "money Rp.");
	$adapter->add("PDR + Obat", "total_pdr_obat", "money Rp.");
	$adapter->add("Potongan", "total_pot", "money Rp.");
	$adapter->add("Hak Dokter", "total", "money Rp.");
	$adapter->addFixValue("Noreg Pasien", "<strong>Total</strong>");
	$adapter->addSummary("PDR", "pdr","number");
	$adapter->addSummary("PDR", "pdr", "money Rp.");
	$adapter->addSummary("Pot PDR", "pot_pdr", "money Rp.");
	$adapter->addSummary("PDR - Pot", "net_pdr", "money Rp.");
	$adapter->addSummary("Biaya Obat", "biaya_obat", "money Rp.");
	$adapter->addSummary("Pot Obat", "pot_obat", "money Rp.");
	$adapter->addSummary("Obat - Pot", "net_obat", "money Rp.");
	$adapter->addSummary("PDR + Obat", "total_pdr_obat", "money Rp.");
	$adapter->addSummary("Potongan", "total_pot", "money Rp.");
	$adapter->addSummary("Hak Dokter", "total", "money Rp.");
	$adapter->setUseID(false);		
	$uidata = $adapter->getContent($content);
	$table->setContent($uidata);
	$html = $table->getBodyContent();
	
	$pack = new ResponsePackage();		
	$pack->setContent(array('list'=>$html));
	$pack->setStatus(ResponsePackage::$STATUS_OK);
	echo json_encode($pack->getPackage());
	return;
}
		
echo $form->getHtml();	
echo $table->getHtml();
	
echo addJS("framework/smis/js/table_action.js");
echo addJS("framework/bootstrap/js/bootstrap-datepicker.js");
echo addCSS("framework/bootstrap/css/datepicker.css");
?>

<script type="text/javascript">
	var dokter_lps;
	var pendapatan_spesialis;
	
	$(document).ready(function() {
		$('[data-toggle="popover"]').popover({
			trigger: 'hover',
			'placement': 'top'
		});		
		$('.mydate').datepicker();
		
		dokter_lps = new TableAction("dokter_lps", "kasir", "laporan_pendapatan_spesialis", new Array());
		dokter_lps.setSuperCommand("dokter_lps");
		
		dokter_lps.selected = function(json) {
			$("#id_dokter").val(json.id);
			$("#nama_dokter").val(json.nama);
		};
		
		pendapatan_spesialis = new TableAction("pendapatan_spesialis", "kasir", "laporan_pendapatan_spesialis", new Array());
		pendapatan_spesialis.addViewData = function(data){
			$.each($('#form').serializeArray(), function(i, field) {
				data[field.name] = field.value;
			});
			$('#print_dokter_name').html(': ' + $('#nama_dokter').val());
			$('#print_pot_pdr').html(': ' + $('#pot_pdr').val() + '%');
			$('#print_pot_obat').html(': ' + $('#pot_obat').val() + '%');
			$('#print_tanggal').html(': ' + $('#from_date').val() + ' s/d ' + $('#to_date').val());
			return data;
		};
	});
</script>
<style type="text/css">	
	.show_print{ display:none; }
	.show_print td { border:none; }
	@media print{
			.show_print{ display:table-row !important; }
		}
</style>
