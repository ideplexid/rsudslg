<?php
	require_once("smis-base/smis-include-service-consumer.php");
	require_once("smis-base/smis-include-service-consumer.php");
	require_once("kasir/class/responder/DokterServiceResponder.php");
	global $db;
	
	$form = new Form("lpds_form", "", "Kasir : Laporan Pendapatan Obat Dokter Spesialis");
	$id_dokter_hidden = new Hidden("lpds_id_dokter", "lpds_id_dokter", "");
	$form->addElement("", $id_dokter_hidden);
	$nama_dokter_text = new Text("lpds_nama_dokter", "lpds_nama_dokter", "");
	$nama_dokter_text->setClass("smis-one-option-input");
	$nama_dokter_text->setAtribute("disabled='disabled'");
	$nama_dokter_button = new Button("", "", "Pilih");
	$nama_dokter_button->setClass("btn-info");
	$nama_dokter_button->setAction("dokter_lpds.chooser('dokter_lpds', 'dokter_lpds_button', 'dokter_lpds', dokter_lpds)");
	$nama_dokter_button->setIcon("icon-white icon-list-alt");
	$nama_dokter_button->setIsButton(Button::$ICONIC);
	$nama_dokter_button->setAtribute("id='dokter_browse'");
	$dokter_input_group = new InputGroup("");
	$dokter_input_group->addComponent($nama_dokter_text);
	$dokter_input_group->addComponent($nama_dokter_button);
	$form->addElement("Dokter", $dokter_input_group);
	$tanggal_from_text = new Text("lpds_tanggal_from", "lpds_tanggal_from", date("Y-m-d"));
	$tanggal_from_text->setClass("mydate");
	$tanggal_from_text->setAtribute("data-date-format='yyyy-mm-dd'");
	$form->addElement("Tanggal Awal", $tanggal_from_text);
	$tanggal_to_text = new Text("lpds_tanggal_to", "lpds_tanggal_to", date("Y-m-d"));
	$tanggal_to_text->setClass("mydate");
	$tanggal_to_text->setAtribute("data-date-format='yyyy-mm-dd'");
	$form->addElement("Tanggal Akhir", $tanggal_to_text);
	$potongan_obat_text = new Text("lpds_potongan_obat", "lpds_potongan_obat", "15");
	$form->addElement("Pot. Obat", $potongan_obat_text);

	$jenis_pasien_service_consumer = new ServiceConsumer(
		$db,
		"get_jenis_patient",
		null,
		"registration"
	);
	$jenis_pasien_service_consumer->execute();
	$jenis_pasien_option = $jenis_pasien_service_consumer->getContent();
	$jenis_pasien_option[] = array(
		"name"		=> "Semua",
		"value"		=> "%%",
		"default"	=> "1"
	);
	$jenis_pasien_select = new Select("lpds_jenis_pasien", "lpds_jenis_pasien", $jenis_pasien_option);
	$form->addElement("Jns. Pasien", $jenis_pasien_select);

	$show_button = new Button("", "", "Tampilkan");
	$show_button->setClass("btn-primary");
	$show_button->setIcon("icon-white icon-repeat");
	$show_button->setIsButton(Button::$ICONIC);
	$show_button->setAction("lpds.view()");
	$print_button = new Button("", "", "Unduh");
	$print_button->setClass("btn-inverse");
	$print_button->setIcon("fa fa-download");
	$print_button->setIsButton(Button::$ICONIC);
	$print_button->setAtribute("id='export_button'");
	$button_group = new ButtonGroup("noprint");
	$button_group->addButton($show_button);
	$button_group->addButton($print_button);
	$form->addElement("", $button_group);
	
	$table = new Table(
		array("No.", "No. Reg.", "No. RM", "Nama Pasien", "Jenis Pasien", "Biaya Obat", "Potongan (%)", "Potongan (Rp)", "Hak Dokter"),
		"",
		null,
		true
	);
	$table->setName("lpds");
	$table->setFooterVisible(false);
	$table->setAction(false);
	
	$dokter_lpds_table = new Table(
		array("No.", "Nama", "Jabatan")
	);
	$dokter_lpds_table->setName("dokter_lpds");
	$dokter_lpds_table->setModel(Table::$SELECT);
	$dokter_lpds_adapter = new SimpleAdapter(true, "No.");
	$dokter_lpds_adapter->add("Nama", "nama");
	$dokter_lpds_adapter->add("Jabatan", "nama_jabatan");
	$dokter_lpds_service_responder = new DokterServiceResponder(
		$db, 
		$dokter_lpds_table, 
		$dokter_lpds_adapter, 
		"employee"
	);
	$superCommand = new SuperCommand();
	$superCommand->addResponder("dokter_lpds", $dokter_lpds_service_responder);
	$init = $superCommand->initialize();
	if ($init != null) {
		echo $init;
		return;
	}	
	
	if (isset($_POST['command'])) {
		if ($_POST['command'] == "get_pasien_spesialis") {
			$tanggal_from = $_POST['tanggal_from'];
			$tanggal_to = $_POST['tanggal_to'];
			$jenis_pasien = $_POST['jenis_pasien'];
			$dbtable = new DBTable(
				$db,
				"smis_rwt_antrian_poli_spesialis_center"
			);
			$row = $dbtable->get_row("
				SELECT COUNT(*) AS 'jumlah'
				FROM smis_rwt_antrian_poli_spesialis_center
				WHERE waktu >= '" . $tanggal_from . " 00:00:00' AND waktu <= '" . $tanggal_to . " 23:59:59' AND prop NOT LIKE 'del' AND carabayar LIKE '" . $jenis_pasien . "'
			");
			$data = array();
			$data['jumlah'] = $row->jumlah;
			echo json_encode($data);
		} else if ($_POST['command'] == "get_info") {
			$num = $_POST['num'];
			$tanggal_from = $_POST['tanggal_from'];
			$tanggal_to = $_POST['tanggal_to'];
			$jenis_pasien = $_POST['jenis_pasien'];
			$dbtable = new DBTable(
				$db,
				"smis_rwt_antrian_poli_spesialis_center"
			);
			$row = $dbtable->get_row("
				SELECT no_register AS 'noreg_pasien', nrm_pasien, nama_pasien, carabayar AS 'jenis_pasien'
				FROM smis_rwt_antrian_poli_spesialis_center
				WHERE waktu >= '" . $tanggal_from . " 00:00:00' AND waktu <= '" . $tanggal_to . " 23:59:59' AND prop NOT LIKE 'del' AND carabayar LIKE '" . $jenis_pasien . "'
				LIMIT " . $num . ", 1
			");
			$noreg_pasien = $row->noreg_pasien;
			$nrm_pasien = $row->nrm_pasien;
			$nama_pasien = $row->nama_pasien;
			$jenis_pasien = $row->jenis_pasien;
			$id_dokter = $_POST['id_dokter'];
			$row = $dbtable->get_row("
				SELECT SUM(total) AS 'biaya_obat'
				FROM smis_ap_penjualan_resep
				WHERE tanggal >= '" . $tanggal_from . " 00:00:00' AND tanggal <= '" . $tanggal_to . " 23:59:59' AND id_dokter = '" . $id_dokter . "' AND noreg_pasien = '" . $noreg_pasien . "' AND tipe = 'resep' AND dibatalkan = '0'
			");
			$biaya_obat = $row->biaya_obat;
			$potongan_obat = round($_POST['pot_obat'] * $biaya_obat / 100);
			$hak_dokter = $biaya_obat - $potongan_obat;
			$html = "
				<tr id='data_" . $num . "'>
					<td id='data_" . $num . "_biaya_obat' style='display: none;'>" . $biaya_obat . "</td>
					<td id='data_" . $num . "_potongan_obat_persen' style='display: none;'>" . $_POST['pot_obat'] . "</td>
					<td id='data_" . $num . "_potongan_obat' style='display: none;'>" . $potongan_obat . "</td>
					<td id='data_" . $num . "_hak_dokter' style='display: none;'>" . $hak_dokter . "</td>
					<td id='data_" . $num . "_num'></td>
					<td id='data_" . $num . "_noreg'>" . ArrayAdapter::format("only-digit6", $noreg_pasien) . "</td>
					<td id='data_" . $num . "_nrm'>" . ArrayAdapter::format("only-digit6", $nrm_pasien) . "</td>
					<td id='data_" . $num . "_nama'>" . ArrayAdapter::format("unslug", $nama_pasien) . "</td>
					<td id='data_" . $num . "_jenis'>" . ArrayAdapter::format("unslug", $jenis_pasien) . "</td>
					<td id='data_" . $num . "_f_biaya_obat'>" . ArrayAdapter::format("money", $biaya_obat) . "</td>
					<td id='data_" . $num . "_f_potongan_obat_persen'><div class='right money-value'>" . $_POST['pot_obat'] . " %</div></td>
					<td id='data_" . $num . "_f_potongan_obat'>" . ArrayAdapter::format("money", $potongan_obat) . "</td>	
					<td id='data_" . $num . "_f_hak_dokter'>" . ArrayAdapter::format("money", $hak_dokter) . "</td>
				</tr>
			";
			$data = array();
			$data['html'] = $html;
			$data['show'] = $hak_dokter == 0 ? false : true;
			$data['noreg_pasien'] = ArrayAdapter::format("only-digit6", $noreg_pasien);
			$data['nrm_pasien'] = ArrayAdapter::format("only-digit6", $nrm_pasien);
			$data['nama_pasien'] = ArrayAdapter::format("unslug", $nama_pasien);
			echo json_encode($data);
		} else if ($_POST['command'] == "download") {
			require_once("smis-libs-out/php-excel/PHPExcel.php");
			
			$objPHPExcel = PHPExcel_IOFactory::load("kasir/templates/template_pendapatan_obat_spesialis.xlsx");
			$objPHPExcel->setActiveSheetIndexByName("PENDAPATAN OBAT SPESIALIS");
			$objWorksheet = $objPHPExcel->getActiveSheet();
			$data = json_decode($_POST['detail']);
			if ($_POST['num'] - 1 > 0)
				$objWorksheet->insertNewRowBefore(9, $_POST['num'] - 1);
			$objWorksheet->setCellValue("B3", ": " . $_POST['nama_dokter']);
			$objWorksheet->setCellValue("B4", ": " . ArrayAdapter::format("date d-m-Y", $_POST['tanggal_from']) . " s/d " . ArrayAdapter::format("date d-m-Y", $_POST['tanggal_to']));
			$objWorksheet->setCellValue("B5", ": " . $_POST['label_jenis_pasien']);
			
			$start_row_num = 8;
			$end_row_num = 8;
			$row_num = $start_row_num;
			foreach($data as $d) {
				$col_num = 0;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->noreg_pasien);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->nrm_pasien);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->nama_pasien);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->jenis_pasien);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->biaya_obat);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->potongan_obat);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->hak_dokter);
				$row_num++;
				$end_row_num++;
			}
			$objWorksheet->setCellValue("E" . $row_num, "=SUM(D" . $start_row_num . ":E" . ($end_row_num - 1) . ")");
			$objWorksheet->setCellValue("F" . $row_num, "=SUM(E" . $start_row_num . ":F" . ($end_row_num - 1) . ")");
			$objWorksheet->setCellValue("G" . $row_num, "=SUM(F" . $start_row_num . ":G" . ($end_row_num - 1) . ")");
			
			header("Content-type: application/vnd.ms-excel");	
			header("Content-Disposition: attachment; filename=PENDAPATAN_OBAT_SPESIALIS_" . date("Ymd_his") . ".xlsx");
			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");
			$objWriter->save("php://output");
		}
		return;
	}
	
	$loading_bar = new LoadingBar("loading_bar", "");
	$button = new Button("", "", "Batal");
	$button->addClass("btn-primary");
	$button->setIsButton(Button::$ICONIC_TEXT);
	$button->setIcon("fa fa-close");
	$button->setAction("lpds.cancel()");
	$loading_modal = new Modal("lpds_modal", "", "Proses..");
	$loading_modal->addHtml($loading_bar->getHtml(), "after");
	$loading_modal->addFooter($button);
	
	echo $loading_modal->getHtml();
	echo $form->getHtml();
	echo $table->getHtml();
	echo "<div id='info'></div>";
	echo addJS("framework/smis/js/table_action.js");
	echo addJS("framework/bootstrap/js/bootstrap-datepicker.js");
	echo addCSS("framework/bootstrap/css/datepicker.css");
	echo addJS ("base-js/smis-base-loading.js");
?>
<script type="text/javascript">
	function LPDSAction(name, page, action, columns) {
		this.initialize(name, page, action, columns);
	}
	LPDSAction.prototype.constructor = LPDSAction;
	LPDSAction.prototype = new TableAction();
	LPDSAction.prototype.getRegulerData = function() {
		var data = TableAction.prototype.getRegulerData.call(this);
		data['tanggal_from'] = $("#lpds_tanggal_from").val();
		data['tanggal_to'] = $("#lpds_tanggal_to").val();
		data['id_dokter'] = $("#lpds_id_dokter").val();
		data['nama_dokter'] = $("#lpds_nama_dokter").val();
		data['pot_obat'] = $("#lpds_potongan_obat").val();
		data['jenis_pasien'] = $("#lpds_jenis_pasien").val();
		data['label_jenis_pasien'] = $("#lpds_jenis_pasien option:selected").text();
		return data;
	};
	LPDSAction.prototype.view = function() {
		if ($("#lpds_tanggal_from").val() == "" || $("#lpds_tanggal_to").val() == "" || $("#lpds_potongan_obat").val() == "")
			return;
		FINISHED = false;
		number = 1;
		var self = this;
		$("#info").empty();
		var data = this.getRegulerData();
		$("#loading_bar").sload("true", "Harap ditunggu...", 0);
		$("#lpds_modal").smodal("show");
		data['command'] = "get_pasien_spesialis";
		$.post(
			"",
			data,
			function(response) {
				var json = JSON.parse(response);
				$("#lpds_list").empty();
				self.fillHtml(0, json.jumlah);
			}
		);
	};
	LPDSAction.prototype.fillHtml = function(num, limit) {
		if (FINISHED || num == limit) {
			if (FINISHED == false && num == limit) {
				this.finalize(limit);
			} else {
				$("#lpds_modal").smodal("hide");
				$("#info").html(
					"<div class='alert alert-block alert-inverse'>" +
						 "<center><strong>PROSES DIBATALKAN</strong></center>" +
					 "</div>"
				);
				$("#export_button").removeAttr("onclick");
			}
			return;
		}
		var self = this;
		var data = this.getRegulerData();
		data['command'] = "get_info";
		data['num'] = num;
		$.post(
			"",
			data,
			function(response) {
				var json = JSON.parse(response);
				if (json.show) {
					$("tbody#lpds_list").append(
						json.html
					);
					$("#data_" + num + "_num").html(number + ".");
					number++;
				}
				$("#loading_bar").sload("true", json.noreg_pasien + " - " + json.nrm_pasien + " - " + json.nama_pasien + " (" + (num + 1) + " / " + limit + ")", (num + 1) * 100 / limit - 1);
				self.fillHtml(num + 1, limit);
			}
		);
	};
	LPDSAction.prototype.finalize = function() {
		$("#loading_bar").sload("true", "Finalisasi...", 100);
		var total_biaya_obat = 0;
		var total_persentase = 0;
		var total_potongan = 0;
		var total_hak_dokter = 0;
		var nor = $("tbody#lpds_list").children("tr").length;
		for (var i = 0; i < nor; i++) {
			var prefix = $("tbody#lpds_list").children("tr").eq(i).prop("id");
			var biaya_obat = parseFloat($("#" + prefix + "_biaya_obat").text());
			total_biaya_obat = total_biaya_obat + biaya_obat;
			var potongan_obat_persen = parseFloat($("#" + prefix + "_potongan_obat_persen").text());
			total_persentase = total_persentase + potongan_obat_persen;
			var potongan_obat = parseFloat($("#" + prefix + "_potongan_obat").text());
			total_potongan = total_potongan + potongan_obat;
			var hak_dokter = parseFloat($("#" + prefix + "_hak_dokter").text());
			total_hak_dokter = total_hak_dokter + hak_dokter;
		}
		total_persentase = total_persentase / nor;
		$("tbody#lpds_list").append(
			"<tr>" +
				"<td colspan='5'><center><strong>T O T A L</strong></center></td>" +
				"<td><strong><div class='right money-value'>" + parseFloat(total_biaya_obat).formatMoney("2", ".", ",") + "</div></strong></td>" +
				"<td><strong><div class='right money-value'>" + total_persentase + " %</div></strong></td>" +
				"<td><strong><div class='right money-value'>" + parseFloat(total_potongan).formatMoney("2", ".", ",") + "</div></strong></td>" +
				"<td><strong><div class='right money-value'>" + parseFloat(total_hak_dokter).formatMoney("2", ".", ",") + "</div></strong></td>" +
			"</tr>"
		);
		$("#lpds_modal").smodal("hide");
		$("#info").html(
			"<div class='alert alert-block alert-info'>" +
				 "<center><strong>PROSES SELESAI</strong></center>" +
			 "</div>"
		);
		$("#export_button").removeAttr("onclick");
		$("#export_button").attr("onclick", "lpds.download()");
	};
	LPDSAction.prototype.cancel = function() {
		FINISHED = true;
	};
	LPDSAction.prototype.download = function() {
		var data = this.getRegulerData();
		data['command'] = "download";
		var detail = {};
		var nor = $("tbody#lpds_list").children("tr").length - 1;
		for (var i = 0; i < nor; i++) {
			var prefix = $("tbody#lpds_list").children("tr").eq(i).prop("id");
			var noreg_pasien = $("#" + prefix + "_noreg").text();
			var nrm_pasien = $("#" + prefix + "_nrm").text();
			var nama_pasien = $("#" + prefix + "_nama").text();
			var jenis_pasien = $("#" + prefix + "_jenis").text();
			var biaya_obat = parseFloat($("#" + prefix + "_biaya_obat").text());
			var potongan_obat = parseFloat($("#" + prefix + "_potongan_obat").text());
			var potongan_obat_persen = parseFloat($("#" + prefix + "_potongan_obat_persen").text());
			var hak_dokter = parseFloat($("#" + prefix + "_hak_dokter").text());
			var d_data = {};
			d_data['noreg_pasien'] = noreg_pasien;
			d_data['nrm_pasien'] = nrm_pasien;
			d_data['nama_pasien'] = nama_pasien;
			d_data['jenis_pasien'] = jenis_pasien;
			d_data['biaya_obat'] = biaya_obat;
			d_data['potongan_obat_persen'] = potongan_obat_persen;
			d_data['potongan_obat'] = potongan_obat;
			d_data['hak_dokter'] = hak_dokter;
			detail[i] = d_data;
		}
		data['num'] = nor;
		data['detail'] = JSON.stringify(detail);
		postForm(data);
	};
	
	function DokterLPDSAction(name, page, action, columns) {
		this.initialize(name, page, action, columns);
	}
	DokterLPDSAction.prototype.constructor = DokterLPDSAction;
	DokterLPDSAction.prototype = new TableAction();
	DokterLPDSAction.prototype.selected = function(json) {
		$("#lpds_id_dokter").val(json.id);
		$("#lpds_nama_dokter").val(json.nama);
		$("tbody#lpds_list").empty();
		$("tbody#lpds_list").append(
			"<tr><td colspan='8'><center><small><strong>LAPORAN BELUM DIHASILKAN</strong></small></center></td></tr>"
		);
	};
	
	var lpds;
	var dokter_lpds;
	var FINISHED;
	var number;
	$(document).ready(function() {
		$(".mydate").datepicker();
		$("#lpds_modal a.close").hide();
		$("tbody#lpds_list").append(
			"<tr><td colspan='9'><center><small><strong>LAPORAN BELUM DIHASILKAN</strong></small></center></td></tr>"
		);
		dokter_lpds = new DokterLPDSAction(
			"dokter_lpds",
			"kasir",
			"laporan_pendapatan_dokter_spesialis",
			new Array()
		);
		dokter_lpds.setSuperCommand("dokter_lpds");
		lpds = new LPDSAction(
			"lpds",
			"kasir",
			"laporan_pendapatan_dokter_spesialis",
			new Array()
		);
		$(document).keyup(function(e) {
			if (e.which == 27) {
				FINISHED = true;
			}
		});
	});
</script>