<?php
	require_once 'smis-base/smis-include-service-consumer.php';
	global $db;
	
	$form = new Form("", "", "Kasir : Laporan Rekap Pendapatan Per Pasien Poli Spesialis");
	$from_date_text = new Text("lrpps_from", "lrpps_from", date("Y-m-d"));
	$from_date_text->setClass("mydate");
	$from_date_text->setAtribute("data-date-format='yyyy-mm-dd'");
	$form->addELement("Tanggal Awal", $from_date_text);
	$to_date_text = new Text("lrpps_to", "lrpps_to", date("Y-m-d"));
	$to_date_text->setClass("mydate");
	$to_date_text->setAtribute("data-date-format='yyyy-mm-dd'");
	$form->addELement("Tanggal Akhir", $to_date_text);
	$jenis_pasien_dbtable = new DBTable(
		$db,
		"smis_rg_jenispasien",
		array("id","slug")
	);
	$jenis_pasien_dbtable->setShowAll(true);
	$package = $jenis_pasien_dbtable->view("","0");
	$data_jenis_pasien = $package['data'];
	$jenis_pasien_adapter = new SelectAdapter("nama", "slug");
	$jenis_pasien_option = $jenis_pasien_adapter->getContent($data_jenis_pasien);
	$jenis_pasien_option[] = array(
		'name'		=> "Semua",
		'value'		=> "%%",
		'default'	=> "1"
	);
	$jenis_pasien_select = new Select("lrpps_jenis_pasien", "lrpps_jenis_pasien", $jenis_pasien_option);
	$form->addELement("Jenis Pasien", $jenis_pasien_select);
	$view_button = new Button("", "", "Lihat");
	$view_button->setIsButton(Button::$ICONIC);
	$view_button->setClass("btn-info");
	$view_button->setIcon("icon-white icon-repeat");
	$view_button->setAction("lrpps.view()");
	$export_button = new Button("", "", "Ekspor Berkas Excel");
	$export_button->setIsButton(Button::$ICONIC);
	$export_button->setClass("btn-inverse");
	$export_button->setIcon("fa fa-download");
	$export_button->setAtribute("id='export_button'");
	$button_group = new ButtonGroup("");
	$button_group->addButton($view_button);
	$button_group->addButton($export_button);
	$form->addELement("", $button_group);
	
	$table = new Table(
		array(
			"No.", "Tgl./No. Resep", "No. Reg.", "No. RM", "Nama Pasien", "Jenis Pasien", "Perusahaan", "Asuransi",
			"IGD Sewa", "IGD Dokter PDR", "IGD PDR", "IGD Jasa Suntik", "IGD Obat", "IGD ECG", "IGD Tindakan", "IGD Billing",
			"Poli Umum Sewa", "Poli Umum Dokter PDR", "Poli Umum PDR", "Poli Umum Jasa Suntik", "Poli Umum ECG", "Poli Umum Treadmill", "Poli Umum Tindakan", "Poli Umum KIUP", "Poli Umum Administrasi", "Poli Umum Billing",
			"Poli Gigi", "Fisioterapi", "Gizi", "BKIA", "Laboratorium", "Depo Farmasi", "Radiologi", "CT Scan", "Echo", "Ambulan", "Jumlah", "Total Pembayaran", "PPn Obat"			
		),
		"",
		null,
		true
	);
	$table->setName("lrpps");
	$table->setAction(false);
	$table->setFooterVisible(false);
	$table->setHeaderVisible(false);
	$table->addHeader("before","
		<tr class='inverse'>
			<th rowspan='3' style='vertical-align: middle !important;'><small><center>No.</center></small></th>
			<th rowspan='3' style='vertical-align: middle !important;'><small><center>Tgl. / No. Resep</center></small></th>
			<th rowspan='3' style='vertical-align: middle !important;'><small><center>No. Reg.</center></small></th>
			<th rowspan='3' style='vertical-align: middle !important;'><small><center>No. RM</center></small></th>
			<th rowspan='3' style='vertical-align: middle !important;'><small><center>Nama Pasien</center></small></th>
			<th rowspan='3' style='vertical-align: middle !important;'><small><center>Jenis Pasien</center></small></th>
			<th rowspan='3' style='vertical-align: middle !important;'><small><center>Perusahaan</center></small></th>
			<th rowspan='3' style='vertical-align: middle !important;'><small><center>Asuransi</center></small></th>
			<th colspan='10' style='vertical-align: middle !important;'><small><center>IGD</center></small></th>
			<th colspan='12' style='vertical-align: middle !important;'><small><center>Poli Umum</center></small></th>
			<th colspan='4' style='vertical-align: middle !important;'><small><center>Poli Spesialis</center></small></th>
			<th rowspan='2' style='vertical-align: middle !important;'><small><center>Poli Gigi</center></small></th>
			<th rowspan='2' style='vertical-align: middle !important;'><small><center>Fisioterapi</center></small></th>
			<th rowspan='2' style='vertical-align: middle !important;'><small><center>Gizi</center></small></th>
			<th rowspan='2' style='vertical-align: middle !important;'><small><center>BKIA</center></small></th>
			<th rowspan='2' style='vertical-align: middle !important;'><small><center>Laboratorium</center></small></th>
			<th rowspan='2' style='vertical-align: middle !important;'><small><center>Depo Farmasi</center></small></th>
			<th rowspan='2' style='vertical-align: middle !important;'><small><center>Radiologi</center></small></th>
			<th rowspan='2' style='vertical-align: middle !important;'><small><center>CT Scan</center></small></th>
			<th rowspan='2' style='vertical-align: middle !important;'><small><center>Echo</center></small></th>
			<th rowspan='2' style='vertical-align: middle !important;'><small><center>Ambulan</center></small></th>
			<th rowspan='2' style='vertical-align: middle !important;'><small><center>Jumlah</center></small></th>
			<th rowspan='2' style='vertical-align: middle !important;'><small><center>PPn Obat</center></small></th>
			<th rowspan='3' style='vertical-align: middle !important;'><small><center>Tot. Pembayaran</center></small></th>
		</tr>
		<tr class='inverse'>
			<th style='vertical-align: middle !important;'><small><center>Sewa</center></small></th>
			<th colspan='4' style='vertical-align: middle !important;'><small><center>PDR</center></small></th>
			<th style='vertical-align: middle !important;'><small><center>Jasa Suntik</center></small></th>
			<th style='vertical-align: middle !important;'><small><center>Obat</center></small></th>
			<th style='vertical-align: middle !important;'><small><center>ECG</center></small></th>
			<th style='vertical-align: middle !important;'><small><center>Tindakan</center></small></th>
			<th style='vertical-align: middle !important;'><small><center>Billing</center></small></th>
			<th style='vertical-align: middle !important;'><small><center>Sewa Kamar</center></small></th>
			<th colspan='4' style='vertical-align: middle !important;'><small><center>PDR</center></small></th>
			<th style='vertical-align: middle !important;'><small><center>Jasa Suntik</center></small></th>
			<th style='vertical-align: middle !important;'><small><center>ECG</center></small></th>
			<th style='vertical-align: middle !important;'><small><center>Treadmill</center></small></th>
			<th style='vertical-align: middle !important;'><small><center>Tindakan</center></small></th>
			<th style='vertical-align: middle !important;'><small><center>KIUP</center></small></th>
			<th style='vertical-align: middle !important;'><small><center>Administrasi</center></small></th>
			<th style='vertical-align: middle !important;'><small><center>Billing</center></small></th>
			<th colspan='4' style='vertical-align: middle !important;'><small><center>PDR</center></small></th>
		</tr>
		<tr class='inverse'>
			<th style='vertical-align: middle !important;'><small>552.01.00</small></th>
			<th style='vertical-align: middle !important;'></th>
			<th style='vertical-align: middle !important;'></th>
			<th style='vertical-align: middle !important;'></th>
			<th style='vertical-align: middle !important;'><small>552.01.01</small></th>
			<th style='vertical-align: middle !important;'><small>552.01.02</small></th>
			<th style='vertical-align: middle !important;'><small>552.01.03</small></th>
			<th style='vertical-align: middle !important;'><small>552.01.04</small></th>
			<th style='vertical-align: middle !important;'><small>552.01.05</small></th>
			<th style='vertical-align: middle !important;'><small>560.04.</small></th>
			<th style='vertical-align: middle !important;'><small>552.02.01</small></th>
			<th style='vertical-align: middle !important;'></th>
			<th style='vertical-align: middle !important;'></th>
			<th style='vertical-align: middle !important;'></th>
			<th style='vertical-align: middle !important;'><small>552.02.01</small></th>
			<th style='vertical-align: middle !important;'><small>552.02.02</small></th>
			<th style='vertical-align: middle !important;'><small>552.02.04</small></th>
			<th style='vertical-align: middle !important;'><small>552.02.05</small></th>
			<th style='vertical-align: middle !important;'><small>552.02.06</small></th>
			<th style='vertical-align: middle !important;'><small>552.02.09</small></th>
			<th style='vertical-align: middle !important;'><small>552.02.</small></th>
			<th style='vertical-align: middle !important;'><small>552.02.09</small></th>
			<th style='vertical-align: middle !important;'></th>
			<th style='vertical-align: middle !important;'></th>
			<th style='vertical-align: middle !important;'></th>
			<th style='vertical-align: middle !important;'><small>552.02.01</small></th>
			<th style='vertical-align: middle !important;'><small>552.03.02</small></th>
			<th style='vertical-align: middle !important;'><small>552.05.05</small></th>
			<th style='vertical-align: middle !important;'><small>552.15.04</small></th>
			<th style='vertical-align: middle !important;'><small>554.02.</small></th>
			<th style='vertical-align: middle !important;'><small>555.01.</small></th>
			<th style='vertical-align: middle !important;'><small>556.01.01</small></th>
			<th style='vertical-align: middle !important;'></th>
			<th style='vertical-align: middle !important;'><small>150.02.00</small></th>
			<th style='vertical-align: middle !important;'><small>551.01.10</small></th>
			<th style='vertical-align: middle !important;'><small>559.01</small></th>
			<th style='vertical-align: middle !important;'></th>
			<th style='vertical-align: middle !important;'></th>
		</tr>
	");
	
	if (isset($_POST['command'])) {
		if ($_POST['command'] == "eksport_excel") {
			require_once("smis-libs-out/php-excel/PHPExcel.php");
			
			$objPHPExcel = PHPExcel_IOFactory::load("kasir/templates/template_jurnal_spesialis.xlsx");
			
			//Master Dokter:
			// $objPHPExcel->setActiveSheetIndexByName("MASTER DOKTER");
			// $objWorksheet = $objPHPExcel->getActiveSheet();
			// $dbtable = new DBTable($db, "smis_vhrd_employee");
			// $md_rows = $dbtable->get_result("
				// SELECT *
				// FROM smis_vhrd_employee
				// WHERE slug LIKE '%dokter%'
			// ");
			$md_start_row_num = 4;
			$md_end_row_num = 65;
			// $row_num = $md_start_row_num;
			// if (count($md_rows) > 1) {
				// $objWorksheet->insertNewRowBefore($row_num + 1, count($md_rows) - 1);
				// $md_end_row_num += count($md_rows) - 1;
			// }
			// foreach ($md_rows as $mdr) {
				// $col_num = "A";
				// $objWorksheet->setCellValue($col_num . $row_num, $mdr->id);
				// $col_num++;
				// $objWorksheet->setCellValue($col_num . $row_num, $mdr->kode);
				// $col_num++;
				// $objWorksheet->setCellValue($col_num . $row_num, strtoupper($mdr->nama));
				// $col_num++;
				// $objWorksheet->setCellValue($col_num . $row_num, "1");
				// $col_num_before = $col_num;
				// $col_num++;
				// $objWorksheet->setCellValue($col_num . $row_num, "=1-" . $col_num_before . $row_num);
				// $col_num++;
				// $objWorksheet->setCellValue($col_num . $row_num, "1");
				// $col_num_before = $col_num;
				// $col_num++;
				// $objWorksheet->setCellValue($col_num . $row_num, "=1-" . $col_num_before . $row_num);
				// $row_num++;
			// }
			
			//RJ-04:
			$objPHPExcel->setActiveSheetIndexByName("RJ-04");
			$objWorksheet = $objPHPExcel->getActiveSheet();
			if ($_POST['num_rows'] - 1 > 0)
				$objWorksheet->insertNewRowBefore(7, $_POST['num_rows'] - 1);
			
			setlocale(LC_ALL, 'IND');
			$objWorksheet->setCellValue("AN1", strtoupper(strftime('%B', mktime(0, 0, 0, DateTime::createFromFormat('Y-m-d', $_POST['to'])->format('m'), 1 ))));
			$objWorksheet->setCellValue("AN2", DateTime::createFromFormat('Y-m-d', $_POST['to'])->format('d-m-Y'));
			
			$data = json_decode($_POST['d_data']);
			$rj04_start_row_num = 6;
			$rj04_end_row_num = 6;
			$row_num = $rj04_start_row_num;
			$no = 1;
			$rekap_jasmed_start_row_num = 7;
			$rekap_jasmed_end_row_num = 7;
			$rekap_jasmed_row_num = $rekap_jasmed_start_row_num;
			foreach ($data as $d) {
				//number of copied rows:
				$number_copy_rows = 0;
				$arr_igd_id_dokter = explode(";", $d->igdiddokterpdr);
				$arr_igd_kode_dokter = explode(";", $d->igdkodedokterpdr);
				$arr_igd_nama_dokter = explode(";", $d->igdnamadokterpdr);
				$arr_igd_nominal_dokter = explode(";", $d->igdnominaldokterpdr);
				if (count($arr_igd_id_dokter) > $number_copy_rows)
					$number_copy_rows = count($arr_igd_id_dokter);
				$arr_poliumum_id_dokter = explode(";", $d->poliumumiddokterpdr);
				$arr_poliumum_kode_dokter = explode(";", $d->poliumumkodedokterpdr);
				$arr_poliumum_nama_dokter = explode(";", $d->poliumumnamadokterpdr);
				$arr_poliumum_nominal_dokter = explode(";", $d->poliumumnominaldokterpdr);
				if (count($arr_poliumum_id_dokter))
					$number_copy_rows = count($arr_poliumum_id_dokter);
				$arr_polispesialis_id_dokter = explode(";", $d->polispesialisiddokterpdr);
				$arr_polispesialis_kode_dokter = explode(";", $d->polispesialiskodedokterpdr);
				$arr_polispesialis_nama_dokter = explode(";", $d->polispesialisnamadokterpdr);
				$arr_polispesialis_nominal_dokter = explode(";", $d->polispesialisnominaldokterpdr);
				if (count($arr_polispesialis_id_dokter))
					$number_copy_rows = count($arr_polispesialis_id_dokter);
				
				for($i = 0; $i < $number_copy_rows; $i++) {
					$col_num = 0;
					$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $no);
					$col_num++;
					$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->tglresep);
					$col_num++;
					$objWorksheet->getCellByColumnAndRow($col_num, $row_num)->setValueExplicit(ArrayAdapter::format("only-digit6", $d->noreg), PHPExcel_Cell_DataType::TYPE_STRING);
					$col_num++;
					$objWorksheet->getCellByColumnAndRow($col_num, $row_num)->setValueExplicit(ArrayAdapter::format("only-digit6", $d->nrm), PHPExcel_Cell_DataType::TYPE_STRING);
					$col_num++;
					$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->nama);
					$col_num++;
					$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->jenis);
					$col_num++;
					$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->perusahaan);
					$col_num++;
					$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->asuransi);
					$col_num++;
					$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, "=AN" . $row_num);
					$col_num++;
					if ($i == 0)
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->igdsewa);
					else
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, 0);
					$col_num++;
					if ($i < count($arr_igd_id_dokter))
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $arr_igd_id_dokter[$i]);
					else
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, 0);
					$col_num++;
					if ($i < count($arr_igd_nominal_dokter))
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $arr_igd_nominal_dokter[$i]);
					else
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, 0);
					$col_num++;
					if ($i == 0)
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->igdjasasuntik);
					else
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, 0);
					$col_num++;
					if ($i == 0)
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->igdobat);
					else
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, 0);
					$col_num++;
					if ($i == 0)
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->igdecg);
					else
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, 0);
					$col_num++;
					if ($i == 0)
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->igdtindakan);
					else
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, 0);
					$col_num++;
					if ($i == 0)
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->igdbilling);
					else
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, 0);
					$col_num++;
					if ($i == 0)
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->poliumumsewakamar);
					else
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, 0);
					$col_num++;
					if ($i < count($arr_poliumum_id_dokter))
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $arr_poliumum_id_dokter[$i]);
					else
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, 0);
					$col_num++;
					if ($i < count($arr_poliumum_nominal_dokter))
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $arr_poliumum_nominal_dokter[$i]);
					else
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, 0);
					$col_num++;
					if ($i == 0)
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->poliumumjasasuntik);
					else
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, 0);
					$col_num++;
					if ($i == 0)
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->poliumumecg);
					else
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, 0);
					$col_num++;
					if ($i == 0)
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->poliumumtreadmill);
					else
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, 0);
					$col_num++;
					if ($i == 0)
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->poliumumtindakan);
					else
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, 0);
					$col_num++;
					if ($i == 0)
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->poliumumkiup);
					else
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, 0);
					$col_num++;
					if ($i == 0)
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->poliumumadministrasi);
					else
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, 0);
					$col_num++;
					if ($i == 0)
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->poliumumbilling);
					else
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, 0);
					$col_num++;
					if ($i < count($arr_polispesialis_id_dokter))
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $arr_polispesialis_id_dokter[$i]);
					else
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, 0);
					$col_num++;
					if ($i < count($arr_polispesialis_nominal_dokter))
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $arr_polispesialis_nominal_dokter[$i]);
					else
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, 0);
					$col_num++;
					if ($i == 0)
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->poligigi);
					else
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, 0);
					$col_num++;
					if ($i == 0)
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->fisioterapi);
					else
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, 0);
					$col_num++;
					if ($i == 0)
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->gizi);
					else
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, 0);
					$col_num++;
					if ($i == 0)
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->bkia);
					else
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, 0);
					$col_num++;
					if ($i == 0)
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->laboratorium);
					else
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, 0);
					$col_num++;
					if ($i == 0)
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->depofarmasi);
					else
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, 0);
					$col_num++;
					if ($i == 0)
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->radiologi);
					else
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, 0);
					$col_num++;
					if ($i == 0)
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->ctscan);
					else
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, 0);
					$col_num++;
					if ($i == 0)
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->echo);
					else
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, 0);
					$col_num++;
					if ($i == 0)
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->ambulan);
					else
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, 0);
					$col_num++;
					$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, "=SUM(J" . $row_num . ",L" . $row_num . ":R" . $row_num . ",T" . $row_num . ":AA" . $row_num . ",AC" . $row_num . ":AM" . $row_num . ")");
					$col_num++;
					if ($i == 0)
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->ppnobat);
					else
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, 0);
					
					//Kertas Kerja:
					$col_num+=3;
					$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, "=K" . $row_num);
					$col_num++;
					$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, "=VLOOKUP(AR" . $row_num . ", 'MASTER DOKTER'!A" . $md_start_row_num . ":I" . $md_end_row_num . ",2,FALSE)");
					$col_num++;
					$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, "=VLOOKUP(AR" . $row_num . ", 'MASTER DOKTER'!A" . $md_start_row_num . ":I" . $md_end_row_num . ",3,FALSE)");
					$col_num++;
					$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, "=L" . $row_num);
					$col_num++;
					$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, "=S" . $row_num);
					$col_num++;
					$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, "=VLOOKUP(AV" . $row_num . ", 'MASTER DOKTER'!A" . $md_start_row_num . ":I" . $md_end_row_num . ",2,FALSE)");
					$col_num++;
					$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, "=VLOOKUP(AV" . $row_num . ", 'MASTER DOKTER'!A" . $md_start_row_num . ":I" . $md_end_row_num . ",3,FALSE)");
					$col_num++;
					$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, "=T" . $row_num);
					$col_num++;
					$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, "=AB" . $row_num);
					$col_num++;
					$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, "=VLOOKUP(AZ" . $row_num . ", 'MASTER DOKTER'!A" . $md_start_row_num . ":I" . $md_end_row_num . ",2,FALSE)");
					$col_num++;
					$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, "=VLOOKUP(AZ" . $row_num . ", 'MASTER DOKTER'!A" . $md_start_row_num . ":I" . $md_end_row_num . ",3,FALSE)");
					$col_num++;
					$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, "=VLOOKUP(AZ" . $row_num . ", 'MASTER DOKTER'!A" . $md_start_row_num . ":I" . $md_end_row_num . ",8,FALSE)");
					$col_num++;
					$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, "=VLOOKUP(AZ" . $row_num . ", 'MASTER DOKTER'!A" . $md_start_row_num . ":I" . $md_end_row_num . ",9,FALSE)");
					$col_num++;
					$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, "=AC" . $row_num);
					
					//Rekap Jasmed:
					$objPHPExcel->setActiveSheetIndexByName("REKAP JASMED");
					$objWorksheet = $objPHPExcel->getActiveSheet();
					$objWorksheet->insertNewRowBefore($rekap_jasmed_row_num + 1, 1);
					$objWorksheet->setCellValue("A" . $rekap_jasmed_row_num, $d->tglresep);
					$objWorksheet->getCellByColumnAndRow(1, $rekap_jasmed_row_num)->setValueExplicit(ArrayAdapter::format("only-digit6", $d->noreg), PHPExcel_Cell_DataType::TYPE_STRING);
					$objWorksheet->getCellByColumnAndRow(2, $rekap_jasmed_row_num)->setValueExplicit(ArrayAdapter::format("only-digit6", $d->nrm), PHPExcel_Cell_DataType::TYPE_STRING);
					$objWorksheet->setCellValue("D" . $rekap_jasmed_row_num, $d->nama);
					$objWorksheet->setCellValue("E" . $rekap_jasmed_row_num, $d->jenis);
					$objWorksheet->setCellValue("F" . $rekap_jasmed_row_num, $d->perusahaan);
					$objWorksheet->setCellValue("G" . $rekap_jasmed_row_num, $d->asuransi);
					$objWorksheet->setCellValue("H" . $rekap_jasmed_row_num, $d->polispesialisnominalpdr);
					$objWorksheet->setCellValue("I" . $rekap_jasmed_row_num, "=" . $d->polispesialisnominaldokterpdr . "*VLOOKUP(" . $d->polispesialisiddokterpdr . ",'MASTER DOKTER'!A" . $md_start_row_num . ":'MASTER DOKTER'!G" . $md_end_row_num . ",5,FALSE)");
					$objWorksheet->setCellValue("J" . $rekap_jasmed_row_num, "=I" . $rekap_jasmed_row_num . "*50%");
					$objWorksheet->setCellValue("K" . $rekap_jasmed_row_num, "=J" . $rekap_jasmed_row_num);
					$objWorksheet->setCellValue("L" . $rekap_jasmed_row_num, "=IF(H" . $rekap_jasmed_row_num . " < 50000000, 5%, IF(H" . $rekap_jasmed_row_num . " < 250000000, 15%, IF(H" . $rekap_jasmed_row_num . " < 500000000, 25%, 30%)))");
					$objWorksheet->setCellValue("M" . $rekap_jasmed_row_num, "=J" . $rekap_jasmed_row_num . "*L" . $rekap_jasmed_row_num);
					$objWorksheet->setCellValue("N" . $rekap_jasmed_row_num, "=I" . $rekap_jasmed_row_num . "-M" . $rekap_jasmed_row_num);
					$objWorksheet->setCellValue("O" . $rekap_jasmed_row_num, "='RJ-05'!D1");
					$objWorksheet->setCellValue("P" . $rekap_jasmed_row_num, $d->polispesialisiddokterpdr);
					$objWorksheet->setCellValue("Q" . $rekap_jasmed_row_num, $d->polispesialisnamadokterpdr);
					$objWorksheet->setCellValue("R" . $rekap_jasmed_row_num, "=VLOOKUP(P" . $rekap_jasmed_row_num . ", 'MASTER DOKTER'!A" . $md_start_row_num . ":I" . $md_end_row_num . ",9,FALSE)");
					$rekap_jasmed_row_num++;
					$rekap_jasmed_end_row_num++;
					
					$objPHPExcel->setActiveSheetIndexByName("RJ-04");
					$objWorksheet = $objPHPExcel->getActiveSheet();
					$row_num++;
					$rj04_end_row_num++;
					$no++;
				}
			}
			for($col_num = 4, $alphabet = "E"; $alphabet != "BF"; $alphabet++, $col_num++) {
				if ($alphabet != "K" && $alphabet != "S" && $alphabet != "AB" 
				 && $alphabet != "AP" && $alphabet != "AQ" 
				 && $alphabet != "AR" && $alphabet != "AS" && $alphabet != "AT"
				 && $alphabet != "AV" && $alphabet != "AW" && $alphabet != "AX"
				 && $alphabet != "AZ" && $alphabet != "BA" && $alphabet != "BB" && $alphabet != "BC" && $alphabet != "BD")
					$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, "=SUM(" . $alphabet . "6:" . $alphabet . ($row_num - 1) . ")");
			}
			$row_num+=2;
			$objWorksheet->setCellValue("D" . $row_num, "JEMBER, TGL. " . date("d-m-Y"));
			$row_num+=5;
			global $user;
			$objWorksheet->setCellValue("D" . $row_num, strtoupper($user->getNameOnly()));
			
			//Rekapitulasi Per Dokter RJ-04:
			$row_num = $rj04_end_row_num + 11;
			if ($md_end_row_num - $md_start_row_num > 1)
				$objWorksheet->insertNewRowBefore($row_num + 1, $md_end_row_num - $md_start_row_num - 1);
			$rekap_pdr_start_row_num = $row_num;
			$rekap_pdr_end_row_num = $row_num;
			for ($i = 0; $i < $md_end_row_num - $md_start_row_num; $i++, $row_num++, $rekap_pdr_end_row_num++) {
				$objWorksheet->setCellValue("AR" . $row_num, "='MASTER DOKTER'!A" . ($i + 4));
				$objWorksheet->setCellValue("AS" . $row_num, "='MASTER DOKTER'!B" . ($i + 4));
				$objWorksheet->setCellValue("AT" . $row_num, "='MASTER DOKTER'!C" . ($i + 4));
				$objWorksheet->setCellValue("AU" . $row_num, "=SUMIF(AR" . $rj04_start_row_num . ":AR" . ($rj04_end_row_num - 1) . ",AR" . $row_num . ",AU" . $rj04_start_row_num . ":AU" . ($rj04_end_row_num - 1) . ")");
				
				$objWorksheet->setCellValue("AV" . $row_num, "='MASTER DOKTER'!A" . ($i + 4));
				$objWorksheet->setCellValue("AW" . $row_num, "='MASTER DOKTER'!B" . ($i + 4));
				$objWorksheet->setCellValue("AX" . $row_num, "='MASTER DOKTER'!C" . ($i + 4));
				$objWorksheet->setCellValue("AY" . $row_num, "=SUMIF(AV" . $rj04_start_row_num . ":AV" . ($rj04_end_row_num - 1) . ",AV" . $row_num . ",AY" . $rj04_start_row_num . ":AY" . ($rj04_end_row_num - 1) . ")");
				
				$objWorksheet->setCellValue("AZ" . $row_num, "='MASTER DOKTER'!A" . ($i + 4));
				$objWorksheet->setCellValue("BA" . $row_num, "='MASTER DOKTER'!B" . ($i + 4));
				$objWorksheet->setCellValue("BB" . $row_num, "='MASTER DOKTER'!C" . ($i + 4));
				$objWorksheet->setCellValue("BC" . $row_num, "='MASTER DOKTER'!H" . ($i + 4));
				$objWorksheet->setCellValue("BD" . $row_num, "='MASTER DOKTER'!I" . ($i + 4));
				$objWorksheet->setCellValue("BE" . $row_num, "=SUMIF(AZ" . $rj04_start_row_num . ":AZ" . ($rj04_end_row_num - 1) . ",AZ" . $row_num . ",BE" . $rj04_start_row_num . ":BE" . ($rj04_end_row_num - 1) . ")");
			}
			$objWorksheet->setCellValue("AU" . $rekap_pdr_end_row_num, "=SUM(AU" . $rekap_pdr_start_row_num . ":AU" . ($rekap_pdr_end_row_num - 1) . ")");
			$objWorksheet->setCellValue("AY" . $rekap_pdr_end_row_num, "=SUM(AY" . $rekap_pdr_start_row_num . ":AY" . ($rekap_pdr_end_row_num - 1) . ")");
			$objWorksheet->setCellValue("BE" . $rekap_pdr_end_row_num, "=SUM(BE" . $rekap_pdr_start_row_num . ":BE" . ($rekap_pdr_end_row_num - 1) . ")");
			
			//Sorting Rekap Jasmed:
			$objPHPExcel->setActiveSheetIndexByName("REKAP JASMED");
			$objWorksheet = $objPHPExcel->getActiveSheet();
			$objWorksheet->setAutoFilter("A" . ($rekap_jasmed_start_row_num-1) . ":R" . $rekap_jasmed_end_row_num);
			$objWorksheet->getProtection()->setSort(true);
			
			//RJ-05:
			$objPHPExcel->setActiveSheetIndexByName("RJ-05");
			$objWorksheet = $objPHPExcel->getActiveSheet();
			$objWorksheet->setCellValue("D1", "BRI-" . date("m.d") . " C");
			$row_num = 6;
			for (;$row_num <= 16; $row_num++) {
				$objWorksheet->setCellValue("D" . $row_num, "=SUMIF('RJ-04'!BD" . $rj04_start_row_num . ":'RJ-04'!BD" . ($rj04_end_row_num - 1) . ",B" . $row_num . ",'RJ-04'!BE" . $rj04_start_row_num . ":'RJ-04'!BE" . ($rj04_end_row_num - 1) . ")");
			}
			$row_num_rj04 = $rj04_end_row_num;
			for($col_num_rj04 = "J", $col_num = 3; $row_num <= 42; $col_num_rj04++) {
				if ($col_num_rj04 != "K" && $col_num_rj04 != "S" && $col_num_rj04 != "AB" && $col_num_rj04 != "AC") {
					$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, "='RJ-04'!" . $col_num_rj04 . $row_num_rj04);
					$row_num++;
				}
			}
			$row_num += 2;
			$objWorksheet->setCellValue("C" . $row_num, "JEMBER, TGL. " . date("d-m-Y"));
			$row_num += 5;
			global $user;
			$objWorksheet->setCellValue("C" . $row_num, strtoupper($user->getNameOnly()));
			
			//RJ-06:
			$objPHPExcel->setActiveSheetIndexByName("RJ-06");
			$objWorksheet = $objPHPExcel->getActiveSheet();
			$objWorksheet->setCellValue("G2", "M." . date("m.d") . "BRI");
			$rj06_polianak_start_row_num = 6;
			$rj06_polianak_end_row_num = 7;
			$rj06_poliobgyn_start_row_num = 8;
			$rj06_poliobgyn_end_row_num = 9;
			$rj06_polibedah_start_row_num = 10;
			$rj06_polibedah_end_row_num = 11;
			$rj06_polimata_start_row_num = 12;
			$rj06_polimata_end_row_num = 13;
			$rj06_politht_start_row_num = 14;
			$rj06_politht_end_row_num = 15;
			$rj06_polikulitkelamin_start_row_num = 16;
			$rj06_polikulitkelamin_end_row_num = 17;
			$rj06_polisyaraf_start_row_num = 18;
			$rj06_polisyaraf_end_row_num = 19;
			$rj06_poliortopedi_start_row_num = 20;
			$rj06_poliortopedi_end_row_num = 21;
			$rj06_polijantung_start_row_num = 22;
			$rj06_polijantung_end_row_num = 23;
			$rj06_poliparu_start_row_num = 24;
			$rj06_poliparu_end_row_num = 25;
			$rj06_polipd_start_row_num = 26;
			$rj06_polipd_end_row_num = 27;
			$objWorksheet->setCellValue("E" . $rj06_polianak_start_row_num, "='RJ-05'!D1");
			$objWorksheet->setCellValue("E" . $rj06_poliobgyn_start_row_num, "='RJ-05'!D1");
			$objWorksheet->setCellValue("E" . $rj06_polibedah_start_row_num, "='RJ-05'!D1");
			$objWorksheet->setCellValue("E" . $rj06_polimata_start_row_num, "='RJ-05'!D1");
			$objWorksheet->setCellValue("E" . $rj06_politht_start_row_num, "='RJ-05'!D1");
			$objWorksheet->setCellValue("E" . $rj06_polikulitkelamin_start_row_num, "='RJ-05'!D1");
			$objWorksheet->setCellValue("E" . $rj06_polisyaraf_start_row_num, "='RJ-05'!D1");
			$objWorksheet->setCellValue("E" . $rj06_poliortopedi_start_row_num, "='RJ-05'!D1");
			$objWorksheet->setCellValue("E" . $rj06_polijantung_start_row_num, "='RJ-05'!D1");
			$objWorksheet->setCellValue("E" . $rj06_poliparu_start_row_num, "='RJ-05'!D1");
			$objWorksheet->setCellValue("E" . $rj06_polipd_start_row_num, "='RJ-05'!D1");
			if ($md_end_row_num - $md_start_row_num > 1) {
				$jumlah_dokter_polianak = 0;
				$jumlah_dokter_poliobgyn = 0;
				$jumlah_dokter_polibedah = 0;
				$jumlah_dokter_polimata = 0;
				$jumlah_dokter_politht = 0;
				$jumlah_dokter_polikulitkelamin = 0;
				$jumlah_dokter_polisyaraf = 0;
				$jumlah_dokter_poliortopedi = 0;
				$jumlah_dokter_polijantung = 0;
				$jumlah_dokter_poliparu = 0;
				$jumlah_dokter_polipd = 0;
				$objPHPExcel->setActiveSheetIndexByName("MASTER DOKTER");
				$objWorksheet = $objPHPExcel->getActiveSheet();
				for($md_row_num = 4; $md_row_num <= 63; $md_row_num++) {
					$poliname = $objWorksheet->getCell("I" . $md_row_num)->getValue();
					if ($poliname == "POLI ANAK") $jumlah_dokter_polianak++;
					else if ($poliname == "POLI OBGYN") $jumlah_dokter_poliobgyn++;
					else if ($poliname == "POLI BEDAH") $jumlah_dokter_polibedah++;
					else if ($poliname == "POLI MATA") $jumlah_dokter_polimata++;
					else if ($poliname == "POLI THT") $jumlah_dokter_politht++;
					else if ($poliname == "POLI KULIT DAN KELAMIN") $jumlah_dokter_polikulitkelamin++;
					else if ($poliname == "POLI SYARAF") $jumlah_dokter_polisyaraf++;
					else if ($poliname == "POLI ORTOPEDI") $jumlah_dokter_poliortopedi++;
					else if ($poliname == "POLI JANTUNG") $jumlah_dokter_polijantung++;
					else if ($poliname == "POLI PARU") $jumlah_dokter_poliparu++;
					else if ($poliname == "POLI PENYAKIT DALAM") $jumlah_dokter_polipd++;
				}
				
				$objPHPExcel->setActiveSheetIndexByName("RJ-06");
				$objWorksheet = $objPHPExcel->getActiveSheet();
				if ($jumlah_dokter_polianak > 1) {
					$objWorksheet->insertNewRowBefore($rj06_polianak_start_row_num + 2, $jumlah_dokter_polianak - 1);
					$rj06_polianak_end_row_num += $jumlah_dokter_polianak - 1;
				}
				$rj06_poliobgyn_start_row_num = $rj06_polianak_end_row_num + 1;
				$rj06_poliobgyn_end_row_num = $rj06_poliobgyn_start_row_num + 1;
				if ($jumlah_dokter_poliobgyn > 1) {
					$objWorksheet->insertNewRowBefore($rj06_poliobgyn_start_row_num + 2, $jumlah_dokter_poliobgyn - 1);
					$rj06_poliobgyn_end_row_num = $rj06_poliobgyn_start_row_num + $jumlah_dokter_poliobgyn;
				}
				$rj06_polibedah_start_row_num = $rj06_poliobgyn_end_row_num + 1;
				$rj06_polibedah_end_row_num = $rj06_polibedah_start_row_num + 1;
				if ($jumlah_dokter_polibedah > 1) {
					$objWorksheet->insertNewRowBefore($rj06_polibedah_start_row_num + 2, $jumlah_dokter_polibedah - 1);
					$rj06_polibedah_end_row_num = $rj06_polibedah_start_row_num + $jumlah_dokter_polibedah;
				}
				$rj06_polimata_start_row_num = $rj06_polibedah_end_row_num + 1;
				$rj06_polimata_end_row_num = $rj06_polimata_start_row_num + 1;
				if ($jumlah_dokter_polimata > 1) {
					$objWorksheet->insertNewRowBefore($rj06_polimata_start_row_num + 2, $jumlah_dokter_polimata - 1);
					$rj06_polimata_end_row_num = $rj06_polimata_start_row_num + $jumlah_dokter_polimata;
				}
				$rj06_politht_start_row_num = $rj06_polimata_end_row_num + 1;
				$rj06_politht_end_row_num = $rj06_politht_start_row_num + 1;
				if ($jumlah_dokter_politht > 1) {
					$objWorksheet->insertNewRowBefore($rj06_politht_start_row_num + 2, $jumlah_dokter_politht - 1);
					$rj06_politht_end_row_num = $rj06_politht_start_row_num + $jumlah_dokter_politht;
				}
				$rj06_polikulitkelamin_start_row_num = $rj06_politht_end_row_num + 1;
				$rj06_polikulitkelamin_end_row_num = $rj06_polikulitkelamin_start_row_num + 1;
				if ($jumlah_dokter_polikulitkelamin > 1) {
					$objWorksheet->insertNewRowBefore($rj06_polikulitkelamin_start_row_num + 2, $jumlah_dokter_polikulitkelamin - 1);
					$rj06_polikulitkelamin_end_row_num = $rj06_polikulitkelamin_start_row_num + $jumlah_dokter_polikulitkelamin;
				}
				$rj06_polisyaraf_start_row_num = $rj06_polikulitkelamin_end_row_num + 1;
				$rj06_polisyaraf_end_row_num = $rj06_polisyaraf_start_row_num + 1;
				if ($jumlah_dokter_polisyaraf > 1) {
					$objWorksheet->insertNewRowBefore($rj06_polisyaraf_start_row_num + 2, $jumlah_dokter_polisyaraf - 1);
					$rj06_polisyaraf_end_row_num = $rj06_polisyaraf_start_row_num + $jumlah_dokter_polisyaraf;
				}
				$rj06_poliortopedi_start_row_num = $rj06_polisyaraf_end_row_num + 1;
				$rj06_poliortopedi_end_row_num = $rj06_poliortopedi_start_row_num + 1;
				if ($jumlah_dokter_poliortopedi > 1) {
					$objWorksheet->insertNewRowBefore($rj06_poliortopedi_start_row_num + 2, $jumlah_dokter_poliortopedi - 1);
					$rj06_poliortopedi_end_row_num = $rj06_poliortopedi_start_row_num + $jumlah_dokter_poliortopedi;
				}
				$rj06_polijantung_start_row_num = $rj06_poliortopedi_end_row_num + 1;
				$rj06_polijantung_end_row_num = $rj06_polijantung_start_row_num + 1;
				if ($jumlah_dokter_polijantung > 1) {
					$objWorksheet->insertNewRowBefore($rj06_polijantung_start_row_num + 2, $jumlah_dokter_polijantung - 1);
					$rj06_polijantung_end_row_num = $rj06_polijantung_start_row_num + $jumlah_dokter_polijantung;
				}
				$rj06_poliparu_start_row_num = $rj06_polijantung_end_row_num + 1;
				$rj06_poliparu_end_row_num = $rj06_poliparu_start_row_num + 1;
				if ($jumlah_dokter_poliparu > 1) {
					$objWorksheet->insertNewRowBefore($rj06_poliparu_start_row_num + 2, $jumlah_dokter_poliparu - 1);
					$rj06_poliparu_end_row_num = $rj06_poliparu_start_row_num + $jumlah_dokter_poliparu;	
				}
				$rj06_polipd_start_row_num = $rj06_poliparu_end_row_num + 1;
				$rj06_polipd_end_row_num = $rj06_polipd_start_row_num + 1;
				if ($jumlah_dokter_polipd > 1) {
					$objWorksheet->insertNewRowBefore($rj06_polipd_start_row_num + 2, $jumlah_dokter_polipd - 1);
					$rj06_polipd_end_row_num = $rj06_polipd_start_row_num + $jumlah_dokter_polipd;
				}
			}
			$polianak_row_num = $rj06_polianak_start_row_num + 1;
			$poliobgyn_row_num = $rj06_poliobgyn_start_row_num + 1;
			$polibedah_row_num = $rj06_polibedah_start_row_num + 1;
			$polimata_row_num = $rj06_polimata_start_row_num + 1;
			$politht_row_num = $rj06_politht_start_row_num + 1;
			$polikulitkelamin_row_num = $rj06_polikulitkelamin_start_row_num + 1;
			$polisyaraf_row_num = $rj06_polisyaraf_start_row_num + 1;
			$poliortopedi_row_num = $rj06_poliortopedi_start_row_num + 1;
			$polijantung_row_num = $rj06_polijantung_start_row_num + 1;
			$poliparu_row_num = $rj06_poliparu_start_row_num + 1;
			$polipd_row_num = $rj06_polipd_start_row_num + 1;
			for ($i = 0; $i < $md_end_row_num - $md_start_row_num; $i++) {
				$objPHPExcel->setActiveSheetIndexByName("MASTER DOKTER");
				$objWorksheet = $objPHPExcel->getActiveSheet();
				$poliname = $objWorksheet->getCell("I" . ($i + 4))->getValue();
				$current_row = "";
				if ($poliname == "POLI ANAK") { 
					$current_row = $polianak_row_num;
					$polianak_row_num++;
				} else if ($poliname == "POLI OBGYN") { 
					$current_row = $poliobgyn_row_num;
					$poliobgyn_row_num++;
				} else if ($poliname == "POLI BEDAH") { 
					$current_row = $polibedah_row_num;
					$polibedah_row_num++;
				} else if ($poliname == "POLI MATA") {
					$current_row = $polimata_row_num;
					$polimata_row_num++;
				} else if ($poliname == "POLI THT") {
					$current_row = $politht_row_num;
					$politht_row_num++;
				} else if ($poliname == "POLI KULIT DAN KELAMIN") { 
					$current_row = $polikulitkelamin_row_num;
					$polikulitkelamin_row_num++;
				} else if ($poliname == "POLI SYARAF") { 
					$current_row = $polisyaraf_row_num;
					$polisyaraf_row_num++;
				} else if ($poliname == "POLI ORTOPEDI") {
					$current_row = $poliortopedi_row_num;
					$poliortopedi_row_num++;
				} else if ($poliname == "POLI JANTUNG") {
					$current_row = $polijantung_row_num;
					$polijantung_row_num++;
				} else if ($poliname == "POLI PARU") { 
					$current_row = $poliparu_row_num;
					$poliparu_row_num++;
				} else if ($poliname == "POLI PENYAKIT DALAM") {
					$current_row = $polipd_row_num;
					$polipd_row_num++;
				}
				if ($current_row != "") {
					$objPHPExcel->setActiveSheetIndexByName("RJ-06");
					$objWorksheet = $objPHPExcel->getActiveSheet();
					$objWorksheet->setCellValue("J" . $current_row, "='MASTER DOKTER'!A" . ($i + 4));
					$objWorksheet->setCellValue("A" . $current_row, "='MASTER DOKTER'!B" . ($i + 4));
					$objWorksheet->setCellValue("B" . $current_row, "='MASTER DOKTER'!C" . ($i + 4));
					$objWorksheet->setCellValue("E" . $current_row, "='RJ-05'!D1");
					$objWorksheet->setCellValue("G" . $current_row, "=VLOOKUP(J" . $current_row . ",'RJ-04'!AZ" . $rekap_pdr_start_row_num . ":'RJ-04'!BE" . ($rekap_pdr_end_row_num - 1) . ",6,FALSE)*VLOOKUP(J" . $current_row . ",'MASTER DOKTER'!A" . $md_start_row_num . ":'MASTER DOKTER'!G" . $md_end_row_num . ",5,FALSE)");
					$objWorksheet->setCellValue("K" . $current_row, "=VLOOKUP(J" . $current_row . ",'RJ-04'!AZ" . $rekap_pdr_start_row_num . ":'RJ-04'!BE" . ($rekap_pdr_end_row_num - 1) . ",6,FALSE)");
					$objWorksheet->setCellValue("L" . $current_row, "=IF(K" . $current_row . " < 50000000, 5%, IF(K" . $current_row . " < 250000000, 15%, IF(K" . $current_row . " < 500000000, 25%, 30%)))");
					$objWorksheet->setCellValue("M" . $current_row, "=G" . $current_row);
					$objWorksheet->setCellValue("N" . $current_row, "=M" . $current_row . "*50%");
					$objWorksheet->setCellValue("O" . $current_row, "=L" . $current_row . "*N" . $current_row);
					$objWorksheet->setCellValue("P" . $current_row, "=M" . $current_row . "-O" . $current_row);
				}
			}
			$objPHPExcel->setActiveSheetIndexByName("RJ-06");
			$objWorksheet = $objPHPExcel->getActiveSheet();
			$objWorksheet->setCellValue("F" . $rj06_polianak_start_row_num, "=SUM(G" . ($rj06_polianak_start_row_num + 1) . ":G" . $rj06_polianak_end_row_num . ")");
			$objWorksheet->setCellValue("F" . $rj06_poliobgyn_start_row_num, "=SUM(G" . ($rj06_poliobgyn_start_row_num + 1) . ":G" . $rj06_poliobgyn_end_row_num . ")");
			$objWorksheet->setCellValue("F" . $rj06_polibedah_start_row_num, "=SUM(G" . ($rj06_polibedah_start_row_num + 1) . ":G" . $rj06_polibedah_end_row_num . ")");
			$objWorksheet->setCellValue("F" . $rj06_polimata_start_row_num, "=SUM(G" . ($rj06_polimata_start_row_num + 1) . ":G" . $rj06_polimata_end_row_num . ")");
			$objWorksheet->setCellValue("F" . $rj06_politht_start_row_num, "=SUM(G" . ($rj06_politht_start_row_num + 1) . ":G" . $rj06_politht_end_row_num . ")");
			$objWorksheet->setCellValue("F" . $rj06_polikulitkelamin_start_row_num, "=SUM(G" . ($rj06_polikulitkelamin_start_row_num + 1) . ":G" . $rj06_polikulitkelamin_end_row_num . ")");
			$objWorksheet->setCellValue("F" . $rj06_polisyaraf_start_row_num, "=SUM(G" . ($rj06_polisyaraf_start_row_num + 1) . ":G" . $rj06_polisyaraf_end_row_num . ")");
			$objWorksheet->setCellValue("F" . $rj06_poliortopedi_start_row_num, "=SUM(G" . ($rj06_poliortopedi_start_row_num + 1) . ":G" . $rj06_poliortopedi_end_row_num . ")");
			$objWorksheet->setCellValue("F" . $rj06_polijantung_start_row_num, "=SUM(G" . ($rj06_polijantung_start_row_num + 1) . ":G" . $rj06_polijantung_end_row_num . ")");
			$objWorksheet->setCellValue("F" . $rj06_poliparu_start_row_num, "=SUM(G" . ($rj06_poliparu_start_row_num + 1) . ":G" . $rj06_poliparu_end_row_num . ")");
			$objWorksheet->setCellValue("F" . $rj06_polipd_start_row_num, "=SUM(G" . ($rj06_polipd_start_row_num + 1) . ":G" . $rj06_polipd_end_row_num . ")");
			$objWorksheet->setCellValue("F" . ($rj06_polipd_end_row_num + 2), "=SUM(F" . $rj06_polianak_start_row_num . ":F" . $rj06_polipd_end_row_num . ")");
			$objWorksheet->setCellValue("G" . ($rj06_polipd_end_row_num + 2), "=SUM(G" . $rj06_polianak_start_row_num . ":G" . $rj06_polipd_end_row_num . ")");
			$objPHPExcel->setActiveSheetIndexByName("RJ-04");
			
			header("Content-type: application/vnd.ms-excel");	
			header("Content-Disposition: attachment; filename=JURNAL_RAJAL_POLI_SPESIALIS_" . strtoupper($_POST['jenis_pasien']) . "_" . $_POST['from'] . "_" . $_POST['to'] . "_" . date("Ymd_His") . ".xlsx");
			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");
			$objWriter->save("php://output");
		} else if ($_POST['command'] == "get_registered") {
			$params = array();
			$params['from'] = $_POST['from'];
			$params['to'] = $_POST['to'];
			$params['jenis_pasien'] = $_POST['jenis_pasien'];
			$params['uri'] = 0;
			$params['grup_transaksi'] = "rajal_poli_spesialis";
			$service = new ServiceConsumer($db, "get_jumlah_pasien", $params, "registration");
			$service->setMode(ServiceConsumer::$CLEAN_BOTH);
			$content = $service->execute()->getContent();
			$data = array();
			$data['jumlah'] = $content[0];
			$data['timestamp'] = date("d-m-Y H:i:s");
			echo json_encode($data);
		} else if ($_POST['command'] == "get_info") {
			// collect registration info:
			$params = array();
			$params['from'] = $_POST['from'];
			$params['to'] = $_POST['to'];
			$params['jenis_pasien'] = $_POST['jenis_pasien'];
			$params['num'] = $_POST['num'];
			$params['uri'] = 0;
			$params['grup_transaksi'] = "rajal_poli_spesialis";
			$service = new ServiceConsumer($db, "get_info_pasien", $params, "registration");
			$service->setMode(ServiceConsumer::$CLEAN_BOTH);
			$content = $service->execute()->getContent();
			$noreg_pasien = $content[0];
			$nrm_pasien = $content[1];
			$nama_pasien = $content[2];
			$tanggal = ArrayAdapter::format("date d", $content[3]);
			$jenis_pasien = $content[4];
			$perusahaan = $content[5];
			$asuransi = $content[6];
			
			$total = 0;
			
			// igd sewa diambil dari tindakan keperawatan yang mengandung kata 'Sewa %':
			$dbtable = new DBTable($db, "smis_rwt_rr_instalasi_gawat_darurat");
			$row = $dbtable->get_row("
				SELECT SUM(harga_tindakan) AS 'harga'
				FROM smis_rwt_tindakan_perawat_instalasi_gawat_darurat
				WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop NOT LIKE 'del' AND nama_tindakan LIKE 'Sewa %'
			");
			$igd_sewa = ArrayAdapter::format("money", "0");
			if ($row->harga != null) {
				$igd_sewa = ArrayAdapter::format("money", $row->harga);
				$total += $row->harga;
			}
			
			// igd pdr diambil dari konsultasi dokter igd:
			$dbtable = new DBTable($db, "smis_rwt_konsultasi_dokter_instalasi_gawat_darurat");
			$row = $dbtable->get_row("
				SELECT SUM(harga) AS 'harga', GROUP_CONCAT(id_dokter SEPARATOR ';') AS 'id_dokter', GROUP_CONCAT(nama_dokter SEPARATOR ';') AS 'nama_dokter', GROUP_CONCAT(harga SEPARATOR ';') AS 'nominal_dokter'
				FROM smis_rwt_konsultasi_dokter_instalasi_gawat_darurat
				WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop NOT LIKE 'del'
			");
			$igd_id_dokter_pdr = "-";
			$igd_kode_dokter_pdr = "-";
			$igd_nama_dokter_pdr = "-";
			$igd_nominal_dokter_pdr = "0";
			$igd_nominal_pdr = ArrayAdapter::format("money", "0");
			if ($row->harga != null) {
				$igd_id_dokter_pdr = $row->id_dokter;
				$igd_kode_dokter_pdr = "";
				foreach(explode(";", $igd_id_dokter_pdr) as $id_dokter) {
					$params = array();
					$params['id_karyawan'] = $id_dokter;
					$service = new ServiceConsumer($db, "get_kode", $params, "hrd");
					$service->setMode(ServiceConsumer::$CLEAN_BOTH);
					$content = $service->execute()->getContent();
					if ($content[0] != "")
						$igd_kode_dokter_pdr .= $content[0] . ";";
					else
						$igd_kode_dokter_pdr .= "-;";
				}
				$igd_kode_dokter_pdr = rtrim($igd_kode_dokter_pdr, ";");
				$igd_nama_dokter_pdr = $row->nama_dokter;
				$igd_nominal_dokter_pdr = $row->nominal_dokter;
				$igd_nominal_pdr = ArrayAdapter::format("money", $row->harga);
				$total += $row->harga;
			}
			
			// igd jasa suntik diambil dari tindakan keperawatan yang mengandung kata '%injeksi%' :
			$dbtable = new DBTable($db, "smis_rwt_tindakan_perawat_instalasi_gawat_darurat");
			$row = $dbtable->get_row("
				SELECT SUM(harga_tindakan) AS 'harga'
				FROM smis_rwt_tindakan_perawat_instalasi_gawat_darurat
				WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop NOT LIKE 'del' AND nama_tindakan LIKE '%injeksi%'
			");
			$igd_jasa_suntik = ArrayAdapter::format("money", "0");
			if ($row->harga != null) {
				$igd_jasa_suntik = ArrayAdapter::format("money", $row->harga);
				$total += $row->harga;
			}
			
			// igd obat diambil dari alok :
			$dbtable = new DBTable($db, "smis_rwt_alok_instalasi_gawat_darurat");
			$row = $dbtable->get_row("
				SELECT SUM(harga) AS 'harga'
				FROM smis_rwt_alok_instalasi_gawat_darurat
				WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop NOT LIKE 'del'
			");
			$igd_obat = ArrayAdapter::format("money", "0");
			if ($row->harga != null) {
				$igd_obat = ArrayAdapter::format("money", $row->harga);
				$total += $row->harga;
			}
			
			// igd ecg diambil dari tindakan keperawatan yang mengandung kata '%E.C.G.%' :
			$dbtable = new DBTable($db, "smis_rwt_tindakan_perawat_instalasi_gawat_darurat");
			$row = $dbtable->get_row("
				SELECT SUM(harga_tindakan) AS 'harga'
				FROM smis_rwt_tindakan_perawat_instalasi_gawat_darurat
				WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop NOT LIKE 'del' AND nama_tindakan LIKE '%E.C.G.%'
			");
			$igd_ecg = ArrayAdapter::format("money", "0");
			if ($row->harga != null) {
				$igd_ecg = ArrayAdapter::format("money", $row->harga);
				$total += $row->harga;
			}
			
			// igd tindakan diambil dari tindakan keperawatan yang tidak mengandung kata '%E.C.G.%' dan 'Sewa %' dan '%injeksi%' :
			$dbtable = new DBTable($db, "smis_rwt_tindakan_perawat_instalasi_gawat_darurat");
			$row = $dbtable->get_row("
				SELECT SUM(harga_tindakan) AS 'harga'
				FROM smis_rwt_tindakan_perawat_instalasi_gawat_darurat
				WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop NOT LIKE 'del' AND nama_tindakan NOT LIKE 'Sewa %' AND nama_tindakan NOT LIKE '%injeksi%' AND nama_tindakan NOT LIKE '%E.C.G.%'
			");
			$igd_tindakan = ArrayAdapter::format("money", "0");
			if ($row->harga != null) {
				$igd_tindakan = ArrayAdapter::format("money", $row->harga);
				$total += $row->harga;
			}
			
			// poli umum sewa kamar pasti 0 :
			$poliumum_sewa_kamar = ArrayAdapter::format("money", "0");
			
			// poliumum pdr diambil dari konsultasi dokter poliumum:
			$dbtable = new DBTable($db, "smis_rwt_konsultasi_dokter_poliumum");
			$row = $dbtable->get_row("
				SELECT SUM(harga) AS 'harga', GROUP_CONCAT(id_dokter SEPARATOR ';') AS 'id_dokter', GROUP_CONCAT(nama_dokter SEPARATOR ';') AS 'nama_dokter', GROUP_CONCAT(harga SEPARATOR ';') AS 'nominal_dokter'
				FROM smis_rwt_konsultasi_dokter_poliumum
				WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop NOT LIKE 'del'
			");
			$poliumum_id_dokter_pdr = "-";
			$poliumum_kode_dokter_pdr = "-";
			$poliumum_nama_dokter_pdr = "-";
			$poliumum_nominal_dokter_pdr = "0";
			$poliumum_nominal_pdr = ArrayAdapter::format("money", "0");
			if ($row->harga != null) {
				$poliumum_id_dokter_pdr = $row->id_dokter;
				$poliumum_kode_dokter_pdr = "";
				foreach(explode(";", $poliumum_id_dokter_pdr) as $id_dokter) {
					$params = array();
					$params['id_karyawan'] = $id_dokter;
					$service = new ServiceConsumer($db, "get_kode", $params, "hrd");
					$service->setMode(ServiceConsumer::$CLEAN_BOTH);
					$content = $service->execute()->getContent();
					if ($content[0] != "")
						$poliumum_kode_dokter_pdr .= $content[0] . ";";
					else
						$poliumum_kode_dokter_pdr .= "-;";
				}
				$poliumum_kode_dokter_pdr = rtrim($poliumum_kode_dokter_pdr, ";");
				$poliumum_nama_dokter_pdr = $row->nama_dokter;
				$poliumum_nominal_dokter_pdr = $row->nominal_dokter;
				$poliumum_nominal_pdr = ArrayAdapter::format("money", $row->harga);
				$total += $row->harga;
			}
			
			// poliumum jasa suntik diambil dari tindakan keperawatan yang mengandung kata '%injeksi%' :
			$dbtable = new DBTable($db, "smis_rwt_tindakan_perawat_poliumum");
			$row = $dbtable->get_row("
				SELECT SUM(harga_tindakan) AS 'harga'
				FROM smis_rwt_tindakan_perawat_poliumum
				WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop NOT LIKE 'del' AND nama_tindakan LIKE '%injeksi%'
			");
			$poliumum_jasa_suntik = ArrayAdapter::format("money", "0");
			if ($row->harga != null) {
				$poliumum_jasa_suntik = ArrayAdapter::format("money", $row->harga);
				$total += $row->harga;
			}
			
			// poliumum ecg diambil dari tindakan keperawatan yang mengandung kata '%E.C.G.%' :
			$dbtable = new DBTable($db, "smis_rwt_tindakan_perawat_poliumum");
			$row = $dbtable->get_row("
				SELECT SUM(harga_tindakan) AS 'harga'
				FROM smis_rwt_tindakan_perawat_poliumum
				WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop NOT LIKE 'del' AND nama_tindakan LIKE '%E.C.G.%'
			");
			$poliumum_ecg = ArrayAdapter::format("money", "0");
			if ($row->harga != null) {
				$poliumum_ecg = ArrayAdapter::format("money", $row->harga);
				$total += $row->harga;
			}
			
			// poliumum treadmill diambil dari tindakan keperawatan yang mengandung kata '%treadmill%' :
			$dbtable = new DBTable($db, "smis_rwt_tindakan_perawat_poliumum");
			$row = $dbtable->get_row("
				SELECT SUM(harga_tindakan) AS 'harga'
				FROM smis_rwt_tindakan_perawat_poliumum
				WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop NOT LIKE 'del' AND nama_tindakan LIKE '%treadmill%'
			");
			$poliumum_treadmill = ArrayAdapter::format("money", "0");
			if ($row->harga != null) {
				$poliumum_treadmill = ArrayAdapter::format("money", $row->harga);
				$total += $row->harga;
			}
			
			// poliumum tindakan diambil dari tindakan keperawatan yang tidak mengandung kata '%injeksi%', '%treadmill%', dan '%E.C.G.%' :
			$dbtable = new DBTable($db, "smis_rwt_tindakan_perawat_poliumum");
			$row = $dbtable->get_row("
				SELECT SUM(harga_tindakan) AS 'harga'
				FROM smis_rwt_tindakan_perawat_poliumum
				WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop NOT LIKE 'del' AND nama_tindakan NOT LIKE '%injeksi%' AND nama_tindakan NOT LIKE '%E.C.G.%' AND nama_tindakan NOT LIKE '%treadmill%'
			");
			$poliumum_tindakan = ArrayAdapter::format("money", "0");
			if ($row->harga != null) {
				$poliumum_tindakan = ArrayAdapter::format("money", $row->harga);
				$total += $row->harga;
			}
			
			// igd billing, poliumum kiup, administrasi dan billing diambil dari karcis:
			$dbtable = new DBTable($db, "smis_rg_layananpasien");
			$row = $dbtable->get_row("
				SELECT jenislayanan AS 'unit', karcis AS 'harga'
				FROM smis_rg_layananpasien
				WHERE id = '" . $noreg_pasien . "' AND prop NOT LIKE 'del'
			");
			$poliumum_kiup = ArrayAdapter::format("money", "0");
			$poliumum_administrasi = ArrayAdapter::format("money", "0");
			$poliumum_billing = ArrayAdapter::format("money", "0");
			$igd_billing = ArrayAdapter::format("money", "0");
			if ($row->harga != null) {
				// karcis 16.000: 5.000 kiup, 10.000 admin, 1.000 billing
				// karcis 15.000: 15.000 billing (unit = igd)
				// karcis 11.000: 10.000 admin, 1.000 billing
				// karcis 5.000 : 4.000 admin, 1.000 billing
				if ($row->unit == "instalasi_gawat_darurat") {
					if ($row->harga == 15000) {
						$igd_billing = ArrayAdapter::format("money", "15000");
					}
				} else {
					if ($row->harga == 16000) {
						$poliumum_kiup = ArrayAdapter::format("money", "5000");
						$poliumum_administrasi = ArrayAdapter::format("money", "10000");
						$poliumum_billing = ArrayAdapter::format("money", "1000");
					} else if ($row->harga == 15000) {
						$poliumum_kiup = ArrayAdapter::format("money", "5000");
						$poliumum_administrasi = ArrayAdapter::format("money", "10000");
					} else if ($row->harga == 11000) {
						$poliumum_administrasi = ArrayAdapter::format("money", "10000");
						$poliumum_billing = ArrayAdapter::format("money", "1000");
					} else if ($row->harga == 5000) {
						$poliumum_administrasi = ArrayAdapter::format("money", "4000");
						$poliumum_billing = ArrayAdapter::format("money", "1000");
					} else {
						$poliumum_kiup = ArrayAdapter::format("money", "5000");
						$poliumum_administrasi = ArrayAdapter::format("money", $row->harga - 6000);
						$poliumum_billing = ArrayAdapter::format("money", "1000");
					}
				}
				$total += $row->harga;
			}
			
			// poli spesialis pdr diambil dari konsultasi dokter poli spesialis:
			$dbtable = new DBTable($db, "smis_rwt_konsultasi_dokter_poli_spesialis_center");
			$row = $dbtable->get_row("
				SELECT SUM(harga) AS 'harga', GROUP_CONCAT(id_dokter SEPARATOR ';') AS 'id_dokter', GROUP_CONCAT(nama_dokter SEPARATOR ';') AS 'nama_dokter', GROUP_CONCAT(harga SEPARATOR ';') AS 'nominal_dokter'
				FROM smis_rwt_konsultasi_dokter_poli_spesialis_center
				WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop NOT LIKE 'del'
			");
			$poli_spesialis_id_dokter_pdr = "-";
			$poli_spesialis_kode_dokter_pdr = "-";
			$poli_spesialis_nama_dokter_pdr = "-";
			$poli_spesialis_nominal_dokter_pdr = "0";
			$poli_spesialis_nominal_pdr = ArrayAdapter::format("money", "0");
			if ($row->harga != null) {
				$poli_spesialis_id_dokter_pdr = $row->id_dokter;
				$poli_spesialis_kode_dokter_pdr = "";
				foreach(explode(";", $poli_spesialis_id_dokter_pdr) as $id_dokter) {
					$params = array();
					$params['id_karyawan'] = $id_dokter;
					$service = new ServiceConsumer($db, "get_kode", $params, "hrd");
					$service->setMode(ServiceConsumer::$CLEAN_BOTH);
					$content = $service->execute()->getContent();
					if ($content[0] != "")
						$poli_spesialis_kode_dokter_pdr .= $content[0] . ";";
					else
						$poli_spesialis_kode_dokter_pdr .= "-;";
				}
				$poli_spesialis_kode_dokter_pdr = rtrim($poli_spesialis_kode_dokter_pdr, ";");
				$poli_spesialis_nama_dokter_pdr = $row->nama_dokter;
				$poli_spesialis_nominal_dokter_pdr = $row->nominal_dokter;
				$poli_spesialis_nominal_pdr = ArrayAdapter::format("money", $row->harga);
				$total += $row->harga;
			}
			
			// poligigi diambil dari tindakan dokter :
			$dbtable = new DBTable($db, "smis_rwt_tindakan_dokter_poligigi");
			$row = $dbtable->get_row("
				SELECT SUM(harga) AS 'harga'
				FROM smis_rwt_tindakan_dokter_poligigi
				WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop NOT LIKE 'del'
			");
			$poligigi = ArrayAdapter::format("money", "0");
			if ($row->harga != null) {
				$poligigi = ArrayAdapter::format("money", $row->harga);
				$total += $row->harga;
			}
			
			// fisioterapi diampil dari pesanan:
			$dbtable = new DBTable($db, "smis_fst_pesanan");
			$row = $dbtable->get_row("
				SELECT SUM(biaya) AS 'harga'
				FROM smis_fst_pesanan
				WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop NOT LIKE 'del'
			");
			$fisioterapi = ArrayAdapter::format("money", "0");
			if ($row->harga != null) {
				$fisioterapi = ArrayAdapter::format("money", $row->harga);
				$total += $row->harga;
			}
			
			// gizi diambil dari asuhan gizi:
			$dbtable = new DBTable($db, "smis_gz_asuhan");
			$row = $dbtable->get_row("
				SELECT SUM(biaya) AS 'harga'
				FROM smis_gz_asuhan
				WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop NOT LIKE 'del'
			");
			$gizi = ArrayAdapter::format("money", "0");
			if ($row->harga != null) {
				$gizi = ArrayAdapter::format("money", $row->harga);
				$total += $row->harga;
			}
			
			// bkia diambil dari konsultasi dokter, tindakan dokter, dan tindakan keperawatan:
			$dbtable = new DBTable($db, "smis_rwt_konsultasi_dokter_bkia");
			$row = $dbtable->get_row("
				SELECT SUM(harga) AS 'harga'
				FROM smis_rwt_konsultasi_dokter_bkia
				WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop NOT LIKE 'del'
			");
			$bkia_konsultasi_dokter = 0;
			if ($row->harga != null) {
				$bkia_konsultasi_dokter = $row->harga;
				$total += $row->harga;
			}
			$dbtable = new DBTable($db, "smis_rwt_tindakan_dokter_bkia");
			$row = $dbtable->get_row("
				SELECT SUM(harga) AS 'harga'
				FROM smis_rwt_tindakan_dokter_bkia
				WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop NOT LIKE 'del'
			");
			$bkia_tindakan_dokter = 0;
			if ($row->harga != null) {
				$bkia_tindakan_dokter = $row->harga;
				$total += $row->harga;
			}
			$dbtable = new DBTable($db, "smis_rwt_tindakan_perawat_bkia");
			$row = $dbtable->get_row("
				SELECT SUM(harga_tindakan) AS 'harga'
				FROM smis_rwt_tindakan_perawat_bkia
				WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop NOT LIKE 'del'
			");
			$bkia_tindakan_perawat = 0;
			if ($row->harga != null) {
				$bkia_tindakan_perawat = $row->harga;
				$total += $row->harga;
			}
			$bkia = $bkia_konsultasi_dokter + $bkia_tindakan_dokter + $bkia_tindakan_perawat;
			if ($bkia == 0)
				$bkia = ArrayAdapter::format("money", "0");
			else
				$bkia = ArrayAdapter::format("money", $bkia);
			
			// laboratorium diambil dari pesanan laboratorium:
			$dbtable = new DBTable($db, "smis_lab_pesanan");
			$row = $dbtable->get_row("
				SELECT SUM(biaya) AS 'harga'
				FROM smis_lab_pesanan
				WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop NOT LIKE 'del'
			");
			$laboratorium = ArrayAdapter::format("money", "0");
			if ($row->harga != null) {
				$laboratorium = ArrayAdapter::format("money", $row->harga);
				$total += $row->harga;
			}
			
			// depo farmasi, ppn_obat dan nomor resep diambil dari penjualan resep:
			$dbtable = new DBTable($db, "smis_ap_penjualan_resep");
			$rows = $dbtable->get_result("
				SELECT nomor_resep, SUM(total) AS 'harga'
				FROM smis_ap_penjualan_resep
				WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop NOT LIKE 'del'
				GROUP BY nomor_resep
			");
			$tagihan_resep_kasir_row = $dbtable->get_row("
				SELECT SUM(total) AS 'harga'
				FROM smis_ksr_tagihan
				WHERE noreg_pasien = '" . $noreg_pasien . "' AND jenis_tagihan = 'penjualan_resep'
			");
			$depo_farmasi = ArrayAdapter::format("money", "0");
			$nomor_resep = "-";
			$nominal_resep = 0;
			if ($rows != null) {
				$nomor_resep = "";
				foreach($rows as $r) {
					$nominal_resep += $r->harga;
					$nomor_resep .= $r->nomor_resep . ";";
				}
				$nomor_resep = rtrim(";", $nomor_resep);
			}
			if ($tagihan_resep_kasir_row->harga != null) {
				$nominal_resep += $tagihan_resep_kasir_row->harga;
			}
			if ($nominal_resep != 0) {
				$depo_farmasi = ArrayAdapter::format("money", $nominal_resep);
				$ppn_obat = $nominal_resep / 1.1 * 0.1;
				if ($ppn_obat == 0)
					$ppn_obat = ArrayAdapter::format("money", "0");
				else
					$ppn_obat = ArrayAdapter::format("money", $ppn_obat);
			}
			$total += $nominal_resep;
			
			// radiologi, ctscan, dan echo diambil dari pesanan radiologi:
			$dbtable = new DBTable($db, "smis_rad_pesanan");
			$rows = $dbtable->get_result("
				SELECT *
				FROM smis_rad_pesanan
				WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop NOT LIKE 'del'
			");
			$radiologi = ArrayAdapter::format("money", "0");
			$ct_scan = ArrayAdapter::format("money", "0");
			$echo = ArrayAdapter::format("money", "0");
			if ($rows != null) {
				$v_radiologi = 0;
				$v_ct_scan = 0;
				$v_echo = 0;
				foreach($rows as $r) {
					$kelas = $r->kelas;
					$layanan_arr = json_decode($r->periksa, true);
					$harga_arr = json_decode($r->harga, true);
					foreach($layanan_arr as $key => $value) {
						if ($value == 1) {
							$id_layanan = str_replace("rad_", "", $key);
							$row_layanan = $dbtable->get_row("
								SELECT *
								FROM smis_rad_layanan
								WHERE id = '" . $id_layanan . "'
							");
							$harga_layanan = $harga_arr[$kelas . "_" . $key];
							if ($row_layanan->layanan == "ct scan: ct contrast" || $row_layanan->layanan == "ct scan: ct non-contrast")
								$v_ct_scan += $harga_layanan;
							else if ($row_layanan->layanan == "echo cardiography")
								$v_echo += $harga_layanan;
							else
								$v_radiologi += $harga_layanan;
						}
					}
				}
				if ($v_radiologi != 0)
					$radiologi = ArrayAdapter::format("money", $v_radiologi);
				if ($v_ct_scan != 0)
					$ct_scan = ArrayAdapter::format("money", $v_ct_scan);
				if ($v_echo != 0)
					$echo = ArrayAdapter::format("money", $v_echo);
				$total += $v_radiologi + $v_ct_scan + $v_echo;
			}
			
			// ambulan diambil dari ambulan:
			$dbtable = new DBTable($db, "smis_amb_ambulan");
			$row = $dbtable->get_row("
				SELECT SUM(gaji_sopir + biaya_perjalanan + biaya_perawat + biaya_dokter + biaya_alat + biaya_lain) AS 'harga'
				FROM smis_amb_ambulan
				WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop NOT LIKE 'del'
			");
			$ambulan = ArrayAdapter::format("money", "0");
			if ($row->harga != null) {
				$ambulan = ArrayAdapter::format("money", $row->harga);
				$total += $row->harga;
			}
			
			// total:
			$total = ArrayAdapter::format("money", $total);
			
			// pembayaran:
			$dbtable = new DBTable($db, "smis_ksr_bayar");
			$row = $dbtable->get_row("
				SELECT SUM(nilai) AS 'bayar'
				FROM smis_ksr_bayar
				WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop NOT LIKE 'del'
			");
			$bayar = ArrayAdapter::format("money", "0");
			if ($row->bayar != null) {
				$bayar = ArrayAdapter::format("money", $row->bayar);
			}
			
			$num = $_POST['num'];
			$html = "<tr id='data_" . $num . "'>";
				$html .= "<td id='data_" . $num . "_num'><small>" . ($num + 1) . "</small></td>";
				$html .= "<td id='data_" . $num . "_tglresep'><small>" . $tanggal . "/" . $nomor_resep . "</small></td>";
				$html .= "<td id='data_" . $num . "_noreg'><small>" . ArrayAdapter::format("only-digit6", $noreg_pasien) . "</small></td>";
				$html .= "<td id='data_" . $num . "_nrm'><small>" . ArrayAdapter::format("only-digit6", $nrm_pasien) . "</small></td>";
				$html .= "<td id='data_" . $num . "_nama'><small>" . ArrayAdapter::format("unslug", $nama_pasien) . "</small></td>";
				$html .= "<td id='data_" . $num . "_jenis'><small>" . ArrayAdapter::format("unslug", $jenis_pasien) . "</small></td>";
				$html .= "<td id='data_" . $num . "_perusahaan'><small>" . $perusahaan . "</small></td>";
				$html .= "<td id='data_" . $num . "_asuransi'><small>" . $asuransi . "</small></td>";
				$html .= "<td id='data_" . $num . "_igdsewa'><small>" . $igd_sewa . "</small></td>";
				$html .= "<td id='data_" . $num . "_igdiddokterpdr'><small>" . $igd_id_dokter_pdr . "</small></td>";
				$html .= "<td id='data_" . $num . "_igdkodedokterpdr'><small>" . $igd_kode_dokter_pdr . "</small></td>";
				$html .= "<td id='data_" . $num . "_igdnamadokterpdr'><small>" . $igd_nama_dokter_pdr . "</small></td>";
				$html .= "<td style='display: none;' id='data_" . $num . "_igdnominaldokterpdr'><small>" . $igd_nominal_dokter_pdr . "</small></td>";
				$html .= "<td id='data_" . $num . "_igdnominalpdr'><small>" . $igd_nominal_pdr . "</small></td>";
				$html .= "<td id='data_" . $num . "_igdjasasuntik'><small>" . $igd_jasa_suntik . "</small></td>";
				$html .= "<td id='data_" . $num . "_igdobat'><small>" . $igd_obat . "</small></td>";
				$html .= "<td id='data_" . $num . "_igdecg'><small>" . $igd_ecg . "</small></td>";
				$html .= "<td id='data_" . $num . "_igdtindakan'><small>" . $igd_tindakan . "</small></td>";
				$html .= "<td id='data_" . $num . "_igdbilling'><small>" . $igd_billing . "</small></td>";
				$html .= "<td id='data_" . $num . "_poliumumsewakamar'><small>" . $poliumum_sewa_kamar . "</small></td>";
				$html .= "<td id='data_" . $num . "_poliumumiddokterpdr'><small>" . $poliumum_id_dokter_pdr . "</small></td>";
				$html .= "<td id='data_" . $num . "_poliumumkodedokterpdr'><small>" . $poliumum_kode_dokter_pdr . "</small></td>";
				$html .= "<td id='data_" . $num . "_poliumumnamadokterpdr'><small>" . $poliumum_nama_dokter_pdr . "</small></td>";
				$html .= "<td style='display: none;' id='data_" . $num . "_poliumumnominaldokterpdr'><small>" . $poliumum_nominal_dokter_pdr . "</small></td>";
				$html .= "<td id='data_" . $num . "_poliumumnominalpdr'><small>" . $poliumum_nominal_pdr . "</small></td>";
				$html .= "<td id='data_" . $num . "_poliumumjasasuntik'><small>" . $poliumum_jasa_suntik . "</small></td>";
				$html .= "<td id='data_" . $num . "_poliumumecg'><small>" . $poliumum_ecg . "</small></td>";
				$html .= "<td id='data_" . $num . "_poliumumtreadmill'><small>" . $poliumum_treadmill . "</small></td>";
				$html .= "<td id='data_" . $num . "_poliumumtindakan'><small>" . $poliumum_tindakan . "</small></td>";
				$html .= "<td id='data_" . $num . "_poliumumkiup'><small>" . $poliumum_kiup . "</small></td>";
				$html .= "<td id='data_" . $num . "_poliumumadministrasi'><small>" . $poliumum_administrasi . "</small></td>";
				$html .= "<td id='data_" . $num . "_poliumumbilling'><small>" . $poliumum_billing . "</small></td>";
				$html .= "<td id='data_" . $num . "_polispesialisiddokterpdr'><small>" . $poli_spesialis_id_dokter_pdr . "</small></td>";
				$html .= "<td id='data_" . $num . "_polispesialiskodedokterpdr'><small>" . $poli_spesialis_kode_dokter_pdr . "</small></td>";
				$html .= "<td id='data_" . $num . "_polispesialisnamadokterpdr'><small>" . $poli_spesialis_nama_dokter_pdr . "</small></td>";
				$html .= "<td style='display: none;' id='data_" . $num . "_polispesialisnominaldokterpdr'><small>" . $poli_spesialis_nominal_dokter_pdr . "</small></td>";
				$html .= "<td id='data_" . $num . "_polispesialisnominalpdr'><small>" . $poli_spesialis_nominal_pdr . "</small></td>";
				$html .= "<td id='data_" . $num . "_poligigi'><small>" . $poligigi . "</small></td>";
				$html .= "<td id='data_" . $num . "_fisioterapi'><small>" . $fisioterapi . "</small></td>";
				$html .= "<td id='data_" . $num . "_gizi'><small>" . $gizi . "</small></td>";
				$html .= "<td id='data_" . $num . "_bkia'><small>" . $bkia . "</small></td>";
				$html .= "<td id='data_" . $num . "_laboratorium'><small>" . $laboratorium . "</small></td>";
				$html .= "<td id='data_" . $num . "_depofarmasi'><small>" . $depo_farmasi . "</small></td>";
				$html .= "<td id='data_" . $num . "_radiologi'><small>" . $radiologi . "</small></td>";
				$html .= "<td id='data_" . $num . "_ctscan'><small>" . $ct_scan . "</small></td>";
				$html .= "<td id='data_" . $num . "_echo'><small>" . $echo . "</small></td>";
				$html .= "<td id='data_" . $num . "_ambulan'><small>" . $ambulan . "</small></td>";
				$html .= "<td id='data_" . $num . "_total'><small>" . $total . "</small></td>";
				$html .= "<td id='data_" . $num . "_ppnobat'><small>" . $ppn_obat . "</small></td>";
				$html .= "<td id='data_" . $num . "_bayar'><small>" . $bayar . "</small></td>";
			$html .= "</tr>";
			$data = array();
			$data['html'] = $html;
			$data['noreg_pasien'] = ArrayAdapter::format("only-digit6", $noreg_pasien);
			$data['nrm_pasien'] = ArrayAdapter::format("only-digit6", $nrm_pasien);
			$data['nama_pasien'] = ArrayAdapter::format("unslug", $nama_pasien);
			echo json_encode($data);
		}
		return;
	} 
	
	$loading_bar = new LoadingBar("loading_bar", "");
	$button = new Button("", "", "Batal");
	$button->addClass("btn-primary");
	$button->setIsButton(Button::$ICONIC_TEXT);
	$button->setIcon("fa fa-close");
	$button->setAction("lrpps.cancel()");
	$loading_modal = new Modal("lrpps_modal", "", "Proses..");
	$loading_modal->addHtml($loading_bar->getHtml(), "after");
	$loading_modal->addFooter($button);
	
	echo $loading_modal->getHtml();
	echo $form->getHtml();
	echo $table->getHtml();
	echo "<div id='info'></div>";
	echo addJS("framework/smis/js/table_action.js");
	echo addJS("framework/bootstrap/js/bootstrap-datepicker.js");
	echo addCSS("framework/bootstrap/css/datepicker.css");
	echo addJS ("base-js/smis-base-loading.js");
?>
<script type="text/javascript">
	function LRPPSAction(name, page, action, columns) {
		this.initialize(name, page, action, columns);
	}
	LRPPSAction.prototype.constructor = LRPPSAction;
	LRPPSAction.prototype = new TableAction();
	LRPPSAction.prototype.getRegulerData = function() {
		var data = TableAction.prototype.getRegulerData.call(this);
		data['from'] = $("#lrpps_from").val();
		data['to'] = $("#lrpps_to").val();
		data['jenis_pasien'] = $("#lrpps_jenis_pasien").val();
		return data;
	};
	LRPPSAction.prototype.view = function() {
		var self = this;
		$("#info").empty();
		$("#loading_bar").sload("true", "Harap ditunggu...", 0);
		$("#lrpps_modal").smodal("show");
		FINISHED = false;
		var data = this.getRegulerData();
		data['command'] = "get_registered";
		$.post(
			"",
			data,
			function(response) {
				var json = JSON.parse(response);
				$("#lrpps_list").empty();
				self.fillHtml(0, json.jumlah);
			}
		);
	};
	LRPPSAction.prototype.fillHtml = function(num, limit) {
		if (FINISHED || num == limit) {
			if (FINISHED == false && num == limit) {
				this.finalize(limit);
			} else {
				$("#lrpps_modal").smodal("hide");
				$("#info").html(
					"<div class='alert alert-block alert-inverse'>" +
						 "<center><strong>PROSES DIBATALKAN</strong></center>" +
					 "</div>"
				);
				$("#export_button").removeAttr("onclick");
			}
			return;
		}
		var self = this;
		var data = this.getRegulerData();
		data['command'] = "get_info";
		data['num'] = num;
		$.post(
			"",
			data,
			function(response) {
				var json = JSON.parse(response);
				$("tbody#lrpps_list").append(
					json.html
				);
				$("#loading_bar").sload("true", json.noreg_pasien + " - " + json.nrm_pasien + " - " + json.nama_pasien + " (" + (num + 1) + " / " + limit + ")", (num + 1) * 100 / limit - 1);
				self.fillHtml(num + 1, limit);
			}
		);
	};
	LRPPSAction.prototype.finalize = function(limit) {
		$("#loading_bar").sload("true", "Finalisasi...", 100);
		var total_igdsewa = 0;
		var total_igdpdr = 0;
		var total_igdjasasuntik = 0;
		var total_igdobat = 0;
		var total_igdecg = 0;
		var total_igdtindakan = 0;
		var total_igdbilling = 0;
		var total_poliumumsewakamar = 0;
		var total_poliumumpdr = 0;
		var total_poliumumjasasuntik = 0;
		var total_poliumumecg = 0;
		var total_poliumumtreadmill = 0;
		var total_poliumumtindakan = 0;
		var total_poliumumkiup = 0;
		var total_poliumumadministrasi = 0;
		var total_poliumumbilling = 0;
		var total_polispesialispdr = 0;
		var total_poligigi = 0;
		var total_fisioterapi = 0;
		var total_gizi = 0;
		var total_bkia = 0;
		var total_laboratorium = 0;
		var total_depofarmasi = 0;
		var total_radiologi = 0;
		var total_ctscan = 0;
		var total_echo = 0;
		var total_ambulan = 0;
		var total_total = 0;
		var total_ppnobat = 0;
		var total_bayar = 0;
		for(var i = 0; i < limit; i++) {
			var prefix = $("tbody#lrpps_list").children("tr").eq(i).prop("id");
			var igd_sewa = parseFloat($("#" + prefix + "_igdsewa").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			total_igdsewa += igd_sewa;
			var igd_pdr = parseFloat($("#" + prefix + "_igdnominalpdr").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			total_igdpdr += igd_pdr;
			var igd_jasa_suntik = parseFloat($("#" + prefix + "_igdjasasuntik").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			total_igdjasasuntik += igd_jasa_suntik;
			var igd_obat = parseFloat($("#" + prefix + "_igdobat").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			total_igdobat += igd_obat;
			var igd_ecg = parseFloat($("#" + prefix + "_igdecg").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			total_igdecg += igd_ecg;
			var igd_tindakan = parseFloat($("#" + prefix + "_igdtindakan").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			total_igdtindakan += igd_tindakan;
			var igd_billing = parseFloat($("#" + prefix + "_igdbilling").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			total_igdbilling += igd_billing;
			var poliumum_sewa_kamar = parseFloat($("#" + prefix + "_poliumumsewakamar").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			total_poliumumsewakamar += poliumum_sewa_kamar;
			var poliumum_pdr = parseFloat($("#" + prefix + "_poliumumnominalpdr").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			total_poliumumpdr += poliumum_pdr;
			var poliumum_jasa_suntik = parseFloat($("#" + prefix + "_poliumumjasasuntik").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			total_poliumumjasasuntik += poliumum_jasa_suntik;
			var poliumum_ecg = parseFloat($("#" + prefix + "_poliumumecg").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			total_poliumumecg += poliumum_ecg;
			var poliumum_treadmill = parseFloat($("#" + prefix + "_poliumumtreadmill").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			total_poliumumtreadmill += poliumum_treadmill;
			var poliumum_tindakan = parseFloat($("#" + prefix + "_poliumumtindakan").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			total_poliumumtindakan += poliumum_tindakan;
			var poliumum_kiup = parseFloat($("#" + prefix + "_poliumumkiup").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			total_poliumumkiup += poliumum_kiup;
			var poliumum_administrasi = parseFloat($("#" + prefix + "_poliumumadministrasi").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			total_poliumumadministrasi += poliumum_administrasi;
			var poliumum_billing = parseFloat($("#" + prefix + "_poliumumbilling").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			total_poliumumbilling += poliumum_billing;
			var polispesialis_pdr = parseFloat($("#" + prefix + "_polispesialisnominalpdr").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			total_polispesialispdr += polispesialis_pdr;
			var poligigi = parseFloat($("#" + prefix + "_poligigi").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			total_poligigi += poligigi;
			var fisioterapi = parseFloat($("#" + prefix + "_fisioterapi").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			total_fisioterapi += fisioterapi;
			var gizi = parseFloat($("#" + prefix + "_gizi").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			total_gizi += gizi;
			var bkia = parseFloat($("#" + prefix + "_bkia").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			total_bkia += bkia;
			var laboratorium = parseFloat($("#" + prefix + "_laboratorium").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			total_laboratorium += laboratorium;
			var depo_farmasi = parseFloat($("#" + prefix + "_depofarmasi").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			total_depofarmasi += depo_farmasi;
			var radiologi = parseFloat($("#" + prefix + "_radiologi").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			total_radiologi += radiologi;
			var ctscan = parseFloat($("#" + prefix + "_ctscan").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			total_ctscan += ctscan;
			var echo = parseFloat($("#" + prefix + "_echo").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			total_echo += echo;
			var ambulan = parseFloat($("#" + prefix + "_ambulan").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			total_ambulan += ambulan;
			var total = parseFloat($("#" + prefix + "_total").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			total_total += total;
			var ppn_obat = parseFloat($("#" + prefix + "_ppnobat").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			total_ppnobat += ppn_obat;
			var bayar = parseFloat($("#" + prefix + "_bayar").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			total_bayar += bayar;
		}
		$("#lrpps_list").append(
			"<tr>" +
				"<td colspan='8'><center><small><strong>J U M L A H</strong></small></center></td>" +
				"<td><small><strong>" + parseFloat(total_igdsewa).formatMoney("2", ".", ",") + "</strong></small></td>" +
				"<td></td>" +
				"<td></td>" +
				"<td></td>" +
				"<td><small><strong><div class='right money-value'>" + parseFloat(total_igdpdr).formatMoney("2", ".", ",") + "</div></strong></small></td>" +
				"<td><small><strong><div class='right money-value'>" + parseFloat(total_igdjasasuntik).formatMoney("2", ".", ",") + "</div></strong></small></td>" +
				"<td><small><strong><div class='right money-value'>" + parseFloat(total_igdobat).formatMoney("2", ".", ",") + "</div></strong></small></td>" +
				"<td><small><strong><div class='right money-value'>" + parseFloat(total_igdecg).formatMoney("2", ".", ",") + "</div></strong></small></td>" +
				"<td><small><strong><div class='right money-value'>" + parseFloat(total_igdtindakan).formatMoney("2", ".", ",") + "</div></strong></small></td>" +
				"<td><small><strong><div class='right money-value'>" + parseFloat(total_igdbilling).formatMoney("2", ".", ",") + "</div></strong></small></td>" +
				"<td><small><strong><div class='right money-value'>" + parseFloat(total_poliumumsewakamar).formatMoney("2", ".", ",") + "</div></strong></small></td>" +
				"<td></td>" +
				"<td></td>" +
				"<td></td>" +
				"<td><small><strong><div class='right money-value'>" + parseFloat(total_poliumumpdr).formatMoney("2", ".", ",") + "</div></strong></small></td>" +
				"<td><small><strong><div class='right money-value'>" + parseFloat(total_poliumumjasasuntik).formatMoney("2", ".", ",") + "</div></strong></small></td>" +
				"<td><small><strong><div class='right money-value'>" + parseFloat(total_poliumumecg).formatMoney("2", ".", ",") + "</div></strong></small></td>" +
				"<td><small><strong><div class='right money-value'>" + parseFloat(total_poliumumtreadmill).formatMoney("2", ".", ",") + "</div></strong></small></td>" +
				"<td><small><strong><div class='right money-value'>" + parseFloat(total_poliumumtindakan).formatMoney("2", ".", ",") + "</div></strong></small></td>" +
				"<td><small><strong><div class='right money-value'>" + parseFloat(total_poliumumkiup).formatMoney("2", ".", ",") + "</div></strong></small></td>" +
				"<td><small><strong><div class='right money-value'>" + parseFloat(total_poliumumadministrasi).formatMoney("2", ".", ",") + "</div></strong></small></td>" +
				"<td><small><strong><div class='right money-value'>" + parseFloat(total_poliumumbilling).formatMoney("2", ".", ",") + "</div></strong></small></td>" +
				"<td></td>" +
				"<td></td>" +
				"<td></td>" +
				"<td><small><strong><div class='right money-value'>" + parseFloat(total_polispesialispdr).formatMoney("2", ".", ",") + "</div></strong></small></td>" +
				"<td><small><strong><div class='right money-value'>" + parseFloat(total_poligigi).formatMoney("2", ".", ",") + "</div></strong></small></td>" +
				"<td><small><strong><div class='right money-value'>" + parseFloat(total_fisioterapi).formatMoney("2", ".", ",") + "</div></strong></small></td>" +
				"<td><small><strong><div class='right money-value'>" + parseFloat(total_gizi).formatMoney("2", ".", ",") + "</div></strong></small></td>" +
				"<td><small><strong><div class='right money-value'>" + parseFloat(total_bkia).formatMoney("2", ".", ",") + "</div></strong></small></td>" +
				"<td><small><strong><div class='right money-value'>" + parseFloat(total_laboratorium).formatMoney("2", ".", ",") + "</div></strong></small></td>" +
				"<td><small><strong><div class='right money-value'>" + parseFloat(total_depofarmasi).formatMoney("2", ".", ",") + "</div></strong></small></td>" +
				"<td><small><strong><div class='right money-value'>" + parseFloat(total_radiologi).formatMoney("2", ".", ",") + "</div></strong></small></td>" +
				"<td><small><strong><div class='right money-value'>" + parseFloat(total_ctscan).formatMoney("2", ".", ",") + "</div></strong></small></td>" +
				"<td><small><strong><div class='right money-value'>" + parseFloat(total_echo).formatMoney("2", ".", ",") + "</div></strong></small></td>" +
				"<td><small><strong><div class='right money-value'>" + parseFloat(total_ambulan).formatMoney("2", ".", ",") + "</div></strong></small></td>" +
				"<td><small><strong><div class='right money-value'>" + parseFloat(total_total).formatMoney("2", ".", ",") + "</div></strong></small></td>" +
				"<td><small><strong><div class='right money-value'>" + parseFloat(total_ppnobat).formatMoney("2", ".", ",") + "</div></strong></small></td>" +
				"<td><small><strong><div class='right money-value'>" + parseFloat(total_bayar).formatMoney("2", ".", ",") + "</div></strong></small></td>" +
			"</tr>"
		);
		$("#lrpps_modal").smodal("hide");
		$("#info").html(
			"<div class='alert alert-block alert-info'>" +
				 "<center><strong>PROSES SELESAI</strong></center>" +
			 "</div>"
		);
		$("#export_button").removeAttr("onclick");
		$("#export_button").attr("onclick", "lrpps.export_xls()");
	};
	LRPPSAction.prototype.export_xls = function() {
		var num_rows = $("#lrpps_list").children("tr").length - 1;
		var d_data = {};
		for(var i = 0; i < num_rows; i++) {
			var prefix = $("tbody#lrpps_list").children("tr").eq(i).prop("id");
			var tglresep = $("#" + prefix + "_tglresep").text();
			var noreg = $("#" + prefix + "_noreg").text();
			var nrm = $("#" + prefix + "_nrm").text();
			var nama = $("#" + prefix + "_nama").text();
			var jenis = $("#" + prefix + "_jenis").text();
			var perusahaan = $("#" + prefix + "_perusahaan").text();
			var asuransi = $("#" + prefix + "_asuransi").text();
			var igdsewa = $("#" + prefix + "_igdsewa").text();
			var igdiddokterpdr = $("#" + prefix + "_igdiddokterpdr").text();
			var igdkodedokterpdr = $("#" + prefix + "_igdkodedokterpdr").text();
			var igdnamadokterpdr = $("#" + prefix + "_igdnamadokterpdr").text();
			var igdnominaldokterpdr = $("#" + prefix + "_igdnominaldokterpdr").text();
			var igdnominalpdr = $("#" + prefix + "_igdnominalpdr").text();
			var igdjasasuntik = $("#" + prefix + "_igdjasasuntik").text();
			var igdobat = $("#" + prefix + "_igdobat").text();
			var igdecg = $("#" + prefix + "_igdecg").text();
			var igdtindakan = $("#" + prefix + "_igdtindakan").text();
			var igdbilling = $("#" + prefix + "_igdbilling").text();
			var poliumumsewakamar = $("#" + prefix + "_poliumumsewakamar").text();
			var poliumumiddokterpdr = $("#" + prefix + "_poliumumiddokterpdr").text();
			var poliumumkodedokterpdr = $("#" + prefix + "_poliumumkodedokterpdr").text();
			var poliumumnamadokterpdr = $("#" + prefix + "_poliumumnamadokterpdr").text();
			var poliumumnominaldokterpdr = $("#" + prefix + "_poliumumnominaldokterpdr").text();
			var poliumumnominalpdr = $("#" + prefix + "_poliumumnominalpdr").text();
			var poliumumjasasuntik = $("#" + prefix + "_poliumumjasasuntik").text();
			var poliumumecg = $("#" + prefix + "_poliumumecg").text();
			var poliumumtreadmill = $("#" + prefix + "_poliumumtreadmill").text();
			var poliumumtindakan = $("#" + prefix + "_poliumumtindakan").text();
			var poliumumkiup = $("#" + prefix + "_poliumumkiup").text();
			var poliumumadministrasi = $("#" + prefix + "_poliumumadministrasi").text();
			var poliumumbilling = $("#" + prefix + "_poliumumbilling").text();
			var polispesialisiddokterpdr = $("#" + prefix + "_polispesialisiddokterpdr").text();
			var polispesialiskodedokterpdr = $("#" + prefix + "_polispesialiskodedokterpdr").text();
			var polispesialisnamadokterpdr = $("#" + prefix + "_polispesialisnamadokterpdr").text();
			var polispesialisnominaldokterpdr = $("#" + prefix + "_polispesialisnominaldokterpdr").text();
			var polispesialisnominalpdr = $("#" + prefix + "_polispesialisnominalpdr").text();
			var poligigi = $("#" + prefix + "_poligigi").text();
			var fisioterapi = $("#" + prefix + "_fisioterapi").text();
			var gizi = $("#" + prefix + "_gizi").text();
			var bkia = $("#" + prefix + "_bkia").text();
			var laboratorium = $("#" + prefix + "_laboratorium").text();
			var depofarmasi = $("#" + prefix + "_depofarmasi").text();
			var radiologi = $("#" + prefix + "_radiologi").text();
			var ctscan = $("#" + prefix + "_ctscan").text();
			var echo = $("#" + prefix + "_echo").text();
			var ambulan = $("#" + prefix + "_ambulan").text();
			var total = $("#" + prefix + "_total").text();
			var ppnobat = $("#" + prefix + "_ppnobat").text();
			
			var dd_data = {};
			
			dd_data['tglresep'] = tglresep;
			dd_data['noreg'] = noreg;
			dd_data['nrm'] = nrm;
			dd_data['nama'] = nama;
			dd_data['jenis'] = jenis;
			dd_data['perusahaan'] = perusahaan;
			dd_data['asuransi'] = asuransi;
			dd_data['igdsewa'] = igdsewa;
			dd_data['igdiddokterpdr'] = igdiddokterpdr;
			dd_data['igdkodedokterpdr'] = igdkodedokterpdr;
			dd_data['igdnamadokterpdr'] = igdnamadokterpdr;
			dd_data['igdnominaldokterpdr'] = igdnominaldokterpdr;
			dd_data['igdnominalpdr'] = igdnominalpdr.replace(/[^0-9-,]/g, '').replace(",", ".");
			dd_data['igdjasasuntik'] = igdjasasuntik.replace(/[^0-9-,]/g, '').replace(",", ".");
			dd_data['igdobat'] = igdobat.replace(/[^0-9-,]/g, '').replace(",", ".");
			dd_data['igdecg'] = igdecg.replace(/[^0-9-,]/g, '').replace(",", ".");
			dd_data['igdtindakan'] = igdtindakan.replace(/[^0-9-,]/g, '').replace(",", ".");
			dd_data['igdbilling'] = igdbilling.replace(/[^0-9-,]/g, '').replace(",", ".");
			dd_data['poliumumsewakamar'] = poliumumsewakamar.replace(/[^0-9-,]/g, '').replace(",", ".");
			dd_data['poliumumiddokterpdr'] = poliumumiddokterpdr;
			dd_data['poliumumkodedokterpdr'] = poliumumkodedokterpdr;
			dd_data['poliumumnamadokterpdr'] = poliumumnamadokterpdr;
			dd_data['poliumumnominaldokterpdr'] = poliumumnominaldokterpdr;
			dd_data['poliumumnominalpdr'] = poliumumnominalpdr.replace(/[^0-9-,]/g, '').replace(",", ".");
			dd_data['poliumumjasasuntik'] = poliumumjasasuntik.replace(/[^0-9-,]/g, '').replace(",", ".");
			dd_data['poliumumecg'] = poliumumecg.replace(/[^0-9-,]/g, '').replace(",", ".");
			dd_data['poliumumtreadmill'] = poliumumtreadmill.replace(/[^0-9-,]/g, '').replace(",", ".");
			dd_data['poliumumtindakan'] = poliumumtindakan.replace(/[^0-9-,]/g, '').replace(",", ".");
			dd_data['poliumumkiup'] = poliumumkiup.replace(/[^0-9-,]/g, '').replace(",", ".");
			dd_data['poliumumadministrasi'] = poliumumadministrasi.replace(/[^0-9-,]/g, '').replace(",", ".");
			dd_data['poliumumbilling'] = poliumumbilling.replace(/[^0-9-,]/g, '').replace(",", ".");
			dd_data['polispesialisiddokterpdr'] = polispesialisiddokterpdr;
			dd_data['polispesialiskodedokterpdr'] = polispesialiskodedokterpdr;
			dd_data['polispesialisnamadokterpdr'] = polispesialisnamadokterpdr;
			dd_data['polispesialisnominaldokterpdr'] = polispesialisnominaldokterpdr;
			dd_data['polispesialisnominalpdr'] = polispesialisnominalpdr.replace(/[^0-9-,]/g, '').replace(",", ".");
			dd_data['poligigi'] = poligigi.replace(/[^0-9-,]/g, '').replace(",", ".");
			dd_data['fisioterapi'] = fisioterapi.replace(/[^0-9-,]/g, '').replace(",", ".");
			dd_data['gizi'] = gizi.replace(/[^0-9-,]/g, '').replace(",", ".");
			dd_data['bkia'] = bkia.replace(/[^0-9-,]/g, '').replace(",", ".");
			dd_data['laboratorium'] = laboratorium.replace(/[^0-9-,]/g, '').replace(",", ".");
			dd_data['depofarmasi'] = depofarmasi.replace(/[^0-9-,]/g, '').replace(",", ".");
			dd_data['radiologi'] = radiologi.replace(/[^0-9-,]/g, '').replace(",", ".");
			dd_data['ctscan'] = ctscan.replace(/[^0-9-,]/g, '').replace(",", ".");
			dd_data['echo'] = echo.replace(/[^0-9-,]/g, '').replace(",", ".");
			dd_data['ambulan'] = ambulan.replace(/[^0-9-,]/g, '').replace(",", ".");
			dd_data['total'] = total.replace(/[^0-9-,]/g, '').replace(",", ".");
			dd_data['ppnobat'] = ppnobat.replace(/[^0-9-,]/g, '').replace(",", ".");
			
			d_data[i] = dd_data;
		}
		var data = this.getRegulerData();
		data['command'] = "eksport_excel";
		data['num_rows'] = num_rows;
		data['d_data'] = JSON.stringify(d_data);
		postForm(data);
	};
	LRPPSAction.prototype.cancel = function() {
		FINISHED = true;
	};
	
	var FINISHED;
	var lrpps;
	$(document).ready(function() {
		$('.mydate').datepicker();
		$("#lrpps_modal").on("show", function() {
			$("a.close").hide();
		});
		$("tbody#lrpps_list").append(
			"<tr><td colspan='47'><center><small><strong>LAPORAN BELUM DIHASILKAN</strong></small></center></td></tr>"
		);
		lrpps = new LRPPSAction(
			"lrpps",
			"kasir",
			"laporan_rekap_pendapatan_per_pasien_spesialis",
			new Array()
		);
		$(document).keyup(function(e) {
			if (e.which == 27) {
				FINISHED = true;
			}
		});
	});
</script>