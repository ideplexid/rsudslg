<?php	
	require_once 'smis-base/smis-include-service-consumer.php';
	global $db;
	
	$form = new Form("", "", "Kasir : Laporan Rekap Pendapatan Per Pasien Rawat Inap");
	$from_date_text = new Text("lrppi_from", "lrppi_from", date("Y-m-d"));
	$from_date_text->setClass("mydate");
	$from_date_text->setAtribute("data-date-format='yyyy-mm-dd'");
	$form->addELement("Tanggal Awal", $from_date_text);
	$to_date_text = new Text("lrppi_to", "lrppi_to", date("Y-m-d"));
	$to_date_text->setClass("mydate");
	$to_date_text->setAtribute("data-date-format='yyyy-mm-dd'");
	$form->addELement("Tanggal Akhir", $to_date_text);
	$jenis_pasien_dbtable = new DBTable(
		$db,
		"smis_rg_jenispasien",
		array("id","slug")
	);
	$jenis_pasien_dbtable->setShowAll(true);
	$package = $jenis_pasien_dbtable->view("","0");
	$data_jenis_pasien = $package['data'];
	$jenis_pasien_adapter = new SelectAdapter("nama", "slug");
	$jenis_pasien_option = $jenis_pasien_adapter->getContent($data_jenis_pasien);
	$jenis_pasien_option[] = array(
		'name'		=> "Semua",
		'value'		=> "%%",
		'default'	=> "1"
	);
	$jenis_pasien_select = new Select("lrppi_jenis_pasien", "lrppi_jenis_pasien", $jenis_pasien_option);
	$form->addELement("Jenis Pasien", $jenis_pasien_select);
	$view_button = new Button("", "", "Lihat");
	$view_button->setIsButton(Button::$ICONIC);
	$view_button->setClass("btn-info");
	$view_button->setIcon("icon-white icon-repeat");
	$view_button->setAction("lrppi.view()");
	$export_button = new Button("", "", "Ekspor Berkas Excel");
	$export_button->setIsButton(Button::$ICONIC);
	$export_button->setClass("btn-inverse");
	$export_button->setIcon("fa fa-download");
	$export_button->setAtribute("id='export_button'");
	$button_group = new ButtonGroup("");
	$button_group->addButton($view_button);
	$button_group->addButton($export_button);
	$form->addELement("", $button_group);
	
	$table = new Table(
		array(
			"No.", "Kelas", "No. Reg.", "No. RM", "Nama Pasien", "Jenis Pasien", "Perusahaan", "Asuransi",
			"RPA Sewa Kamar", "RPA Dokter PDR", "RPA PDR", "RPA Jasa Medis", "RPA Obat", "RPA ECG", "RPA Lavement", "RPA Lain-Lain",
			"RPB Sewa Kamar", "RPB Dokter PDR", "RPB PDR", "RPB Jasa Medis", "RPB Obat", "RPB ECG", "RPB Lavement", "RPB Lain-Lain",
			"ICU Sewa Kamar", "ICU Dokter PDR", "ICU PDR", "ICU Jasa Medis", "ICU Obat", "ICU ECG", "ICU Lain-Lain",
			"IGD Sewa Kamar", "IGD Dokter PDR", "IGD PDR", "IGD Jasa Medis", "IGD Obat", "IGD ECG",
			"OK Sewa Kamar", "OK Dokter PDR", "OK PDR", "OK Obat", "OK Anastesi",
			"VK Sewa Kamar", "VK Dokter PDR", "VK PDR", "VK Jasa Medis", "VK Obat", "VK ECG", "VK Jasa Bidan", "VK Lavement", "VK Lain-Lain",
			"Laboratorium", "PMI", "Depo Farmasi", "Rongent", "CT Scan", "Echo", "Fisioterapi", "Gizi", "Kendaraan", "Administrasi", "Billing",
			"HD Sewa Kamar", "HD Jasa Medis", "HD Obat", "HD Lain-Lain",
			"Jumlah", 
			"Pembayaran Tunai", "Pembayaran Bank", "Pembayaran Tagihan"
		),
		"",
		null,
		true
	);
	$table->setName("lrppi");
	$table->setAction(false);
	$table->setFooterVisible(false);
	$table->setHeaderVisible(false);
	$table->addHeader("before","
		<tr class='inverse'>
			<th rowspan='3' style='vertical-align: middle !important;'><small><center>No.</center></small></th>
			<th rowspan='3' style='vertical-align: middle !important;'><small><center>Kelas</center></small></th>
			<th rowspan='3' style='vertical-align: middle !important;'><small><center>No. Reg.</center></small></th>
			<th rowspan='3' style='vertical-align: middle !important;'><small><center>No. RM</center></small></th>
			<th rowspan='3' style='vertical-align: middle !important;'><small><center>Nama Pasien</center></small></th>
			<th rowspan='3' style='vertical-align: middle !important;'><small><center>Jenis Pasien</center></small></th>
			<th rowspan='3' style='vertical-align: middle !important;'><small><center>Perusahaan</center></small></th>
			<th rowspan='3' style='vertical-align: middle !important;'><small><center>Asuransi</center></small></th>
			<th colspan='10' style='vertical-align: middle !important;'><small><center>Pendapatan Perawatan A</center></small></th>
			<th colspan='10' style='vertical-align: middle !important;'><small><center>Pendapatan Perawatan B</center></small></th>
			<th colspan='9' style='vertical-align: middle !important;'><small><center>Pendapatan Perawatan ICU</center></small></th>
			<th colspan='8' style='vertical-align: middle !important;'><small><center>Pendapatan Perawatan IGD</center></small></th>
			<th colspan='8' style='vertical-align: middle !important;'><small><center>Pendapatan Perawatan OK / RPO</center></small></th>
			<th colspan='11' style='vertical-align: middle !important;'><small><center>Pendapatan Perawatan OBGYN / RPKK</center></small></th>
			<th colspan='11' style='vertical-align: middle !important;'><small><center>Lain-Lain</center></small></th>
			<th colspan='8' style='vertical-align: middle !important;'><small><center>Hemodialisa</center></small></th>
			<th rowspan='3' style='vertical-align: middle !important;'><small><center>Jumlah</center></small></th>
			<th colspan='3' style='vertical-align: middle !important;'><small><center>Tot. Pembayaran</center></small></th>
		</tr>
		<tr class='inverse'>
			<th style='vertical-align: middle !important;'><small><center>Sewa Kamar</center></small></th>
			<th colspan='4' style='vertical-align: middle !important;'><small><center>PDR</center></small></th>
			<th style='vertical-align: middle !important;'><small><center>Jasa Medis</center></small></th>
			<th style='vertical-align: middle !important;'><small><center>Alkes / Obat</center></small></th>
			<th style='vertical-align: middle !important;'><small><center>ECG / Stoom</center></small></th>
			<th style='vertical-align: middle !important;'><small><center>Lavement</center></small></th>
			<th style='vertical-align: middle !important;'><small><center>Lain-Lain</center></small></th>
			<th style='vertical-align: middle !important;'><small><center>Sewa Kamar</center></small></th>
			<th colspan='4' style='vertical-align: middle !important;'><small><center>PDR</center></small></th>
			<th style='vertical-align: middle !important;'><small><center>Jasa Medis</center></small></th>
			<th style='vertical-align: middle !important;'><small><center>Alkes / Obat</center></small></th>
			<th style='vertical-align: middle !important;'><small><center>ECG / Stoom</center></small></th>
			<th style='vertical-align: middle !important;'><small><center>Lavement</center></small></th>
			<th style='vertical-align: middle !important;'><small><center>Lain-Lain</center></small></th>
			<th style='vertical-align: middle !important;'><small><center>Sewa Kamar</center></small></th>
			<th colspan='4' style='vertical-align: middle !important;'><small><center>PDR</center></small></th>
			<th style='vertical-align: middle !important;'><small><center>Jasa Medis</center></small></th>
			<th style='vertical-align: middle !important;'><small><center>Alkes / Obat</center></small></th>
			<th style='vertical-align: middle !important;'><small><center>ECG / Stoom</center></small></th>
			<th style='vertical-align: middle !important;'><small><center>Lain-Lain</center></small></th>
			<th style='vertical-align: middle !important;'><small><center>Sewa Kamar</center></small></th>
			<th colspan='4' style='vertical-align: middle !important;'><small><center>PDR</center></small></th>
			<th style='vertical-align: middle !important;'><small><center>Jasa Medis</center></small></th>
			<th style='vertical-align: middle !important;'><small><center>Alkes / Obat</center></small></th>
			<th style='vertical-align: middle !important;'><small><center>ECG / Stoom</center></small></th>
			<th style='vertical-align: middle !important;'><small><center>Kamar</center></small></th>
			<th colspan='4' style='vertical-align: middle !important;'><small><center>PDR</center></small></th>
			<th style='vertical-align: middle !important;'><small><center>Jasa Medis</center></small></th>
			<th style='vertical-align: middle !important;'><small><center>Materi</center></small></th>
			<th style='vertical-align: middle !important;'><small><center>Anastesi</center></small></th>
			<th style='vertical-align: middle !important;'><small><center>Kamar / VK</center></small></th>
			<th colspan='4' style='vertical-align: middle !important;'><small><center>PDR</center></small></th>
			<th style='vertical-align: middle !important;'><small><center>Jasa Medis</center></small></th>
			<th style='vertical-align: middle !important;'><small><center>Alkes / Obat</center></small></th>
			<th style='vertical-align: middle !important;'><small><center>ECG</center></small></th>
			<th style='vertical-align: middle !important;'><small><center>Jasa Bidan</center></small></th>
			<th style='vertical-align: middle !important;'><small><center>Lavement</center></small></th>
			<th style='vertical-align: middle !important;'><small><center>Lain-Lain</center></small></th>
			<th style='vertical-align: middle !important;'><small><center>Laboratorium</center></small></th>
			<th style='vertical-align: middle !important;'><small><center>PMI</center></small></th>
			<th style='vertical-align: middle !important;'><small><center>Depo Farmasi</center></small></th>
			<th style='vertical-align: middle !important;'><small><center>Rontgen</center></small></th>
			<th style='vertical-align: middle !important;'><small><center>CT Scan</center></small></th>
			<th style='vertical-align: middle !important;'><small><center>Echo / 4D</center></small></th>
			<th style='vertical-align: middle !important;'><small><center>Fisioterapi</center></small></th>
			<th style='vertical-align: middle !important;'><small><center>Gizi</center></small></th>
			<th style='vertical-align: middle !important;'><small><center>Kendaraan</center></small></th>
			<th style='vertical-align: middle !important;'><small><center>Administrasi</center></small></th>
			<th style='vertical-align: middle !important;'><small><center>Billing</center></small></th>
			<th style='vertical-align: middle !important;'><small><center>Sewa Kamar</center></small></th>
			<th colspan='4' style='vertical-align: middle !important;'><small><center>PDR</center></small></th>
			<th style='vertical-align: middle !important;'><small><center>Jasa Medis</center></small></th>
			<th style='vertical-align: middle !important;'><small><center>Alkes / Obat</center></small></th>
			<th style='vertical-align: middle !important;'><small><center>Lain-Lain</center></small></th>
			<th style='vertical-align: middle !important;'><small><center>Tunai</center></small></th>
			<th style='vertical-align: middle !important;'><small><center>Bank</center></small></th>
			<th style='vertical-align: middle !important;'><small><center>Tagihan</center></small></th>
		</tr>
		<tr class='inverse'>
			<th style='vertical-align: middle !important;'><small>551.01.00</small></th>
			<th style='vertical-align: middle !important;'></th>
			<th style='vertical-align: middle !important;'></th>
			<th style='vertical-align: middle !important;'></th>
			<th style='vertical-align: middle !important;'><small>551.01.01</small></th>
			<th style='vertical-align: middle !important;'><small>551.01.02</small></th>
			<th style='vertical-align: middle !important;'><small>551.01.03</small></th>
			<th style='vertical-align: middle !important;'><small>551.01.04</small></th>
			<th style='vertical-align: middle !important;'><small>551.01.08</small></th>
			<th style='vertical-align: middle !important;'><small>560.01.09</small></th>
			<th style='vertical-align: middle !important;'><small>551.02.00</small></th>
			<th style='vertical-align: middle !important;'></th>
			<th style='vertical-align: middle !important;'></th>
			<th style='vertical-align: middle !important;'></th>
			<th style='vertical-align: middle !important;'><small>551.02.01</small></th>
			<th style='vertical-align: middle !important;'><small>551.02.02</small></th>
			<th style='vertical-align: middle !important;'><small>551.02.03</small></th>
			<th style='vertical-align: middle !important;'><small>551.02.04</small></th>
			<th style='vertical-align: middle !important;'><small>551.02.08</small></th>
			<th style='vertical-align: middle !important;'></th>
			<th style='vertical-align: middle !important;'><small>551.03.00</small></th>
			<th style='vertical-align: middle !important;'></th>
			<th style='vertical-align: middle !important;'></th>
			<th style='vertical-align: middle !important;'></th>
			<th style='vertical-align: middle !important;'><small>551.03.01</small></th>
			<th style='vertical-align: middle !important;'><small>551.03.02</small></th>
			<th style='vertical-align: middle !important;'><small>551.03.03</small></th>
			<th style='vertical-align: middle !important;'><small>551.01.09</small></th>
			<th style='vertical-align: middle !important;'><small>551.01.05</small></th>
			<th style='vertical-align: middle !important;'><small>552.01.00</small></th>
			<th style='vertical-align: middle !important;'></th>
			<th style='vertical-align: middle !important;'></th>
			<th style='vertical-align: middle !important;'></th>
			<th style='vertical-align: middle !important;'><small>552.01.01</small></th>
			<th style='vertical-align: middle !important;'><small>552.01.02</small></th>
			<th style='vertical-align: middle !important;'><small>552.01.03</small></th>
			<th style='vertical-align: middle !important;'><small>552.01.04</small></th>
			<th style='vertical-align: middle !important;'><small>553.00</small></th>
			<th style='vertical-align: middle !important;'></th>
			<th style='vertical-align: middle !important;'></th>
			<th style='vertical-align: middle !important;'></th>
			<th style='vertical-align: middle !important;'><small>553.01</small></th>
			<th style='vertical-align: middle !important;'></th>
			<th style='vertical-align: middle !important;'><small>553.03</small></th>
			<th style='vertical-align: middle !important;'><small>553.04</small></th>
			<th style='vertical-align: middle !important;'><small>554.00</small></th>
			<th style='vertical-align: middle !important;'></th>
			<th style='vertical-align: middle !important;'></th>
			<th style='vertical-align: middle !important;'></th>
			<th style='vertical-align: middle !important;'><small>554.01</small></th>
			<th style='vertical-align: middle !important;'><small>554.02</small></th>
			<th style='vertical-align: middle !important;'><small>554.03</small></th>
			<th style='vertical-align: middle !important;'><small>554.04</small></th>
			<th style='vertical-align: middle !important;'><small>554.05</small></th>
			<th style='vertical-align: middle !important;'><small>554.08</small></th>
			<th style='vertical-align: middle !important;'><small>554.09</small></th>
			<th style='vertical-align: middle !important;'><small>555.01</small></th>
			<th style='vertical-align: middle !important;'><small>555.03</small></th>
			<th style='vertical-align: middle !important;'><small>556.01.00</small></th>
			<th style='vertical-align: middle !important;'><small>557.01</small></th>
			<th style='vertical-align: middle !important;'><small>150.02.00</small></th>
			<th style='vertical-align: middle !important;'><small>551.01.10</small></th>
			<th style='vertical-align: middle !important;'><small>552.05</small></th>
			<th style='vertical-align: middle !important;'><small>552.15.04</small></th>
			<th style='vertical-align: middle !important;'><small>560.01</small></th>
			<th style='vertical-align: middle !important;'><small>560.02</small></th>
			<th style='vertical-align: middle !important;'><small>560.04.00</small></th>
			<th style='vertical-align: middle !important;'><small>551.04.00</small></th>
			<th style='vertical-align: middle !important;'></th>
			<th style='vertical-align: middle !important;'></th>
			<th style='vertical-align: middle !important;'></th>
			<th style='vertical-align: middle !important;'><small>551.04.01</small></th>
			<th style='vertical-align: middle !important;'><small>551.04.02</small></th>
			<th style='vertical-align: middle !important;'><small>551.04.03</small></th>
			<th style='vertical-align: middle !important;'><small>551.04.09</small></th>
			<th style='vertical-align: middle !important;'></th>
			<th style='vertical-align: middle !important;'></th>
			<th style='vertical-align: middle !important;'></th>
		</tr>
	");
	
	if (isset($_POST['command'])) {
		if ($_POST['command'] == "eksport_excel") {
			require_once("smis-libs-out/php-excel/PHPExcel.php");
			
			$objPHPExcel = PHPExcel_IOFactory::load("kasir/templates/template_jurnal_rawat_inap.xlsx");
			
			$md_start_row_num = 3;
			$md_end_row_num = 64;
			
			//RI-04:
			$objPHPExcel->setActiveSheetIndexByName("RI-04");
			$objWorksheet = $objPHPExcel->getActiveSheet();
			if ($_POST['num_rows'] - 1 > 0)
				$objWorksheet->insertNewRowBefore(7, $_POST['num_rows'] - 1);
			
			if ($_POST['jenis_pasien'] == "%%")
				$objWorksheet->setCellValue("E1", "PENDAPATAN KESELURUHAN PASIEN : KEPERAWATAN");
			else
				$objWorksheet->setCellValue("E1", "PENDAPATAN PASIEN " . ArrayAdapter::format("unslug", $_POST['jenis_pasien']) . " : KEPERAWATAN");
			setlocale(LC_ALL, 'IND');
			$objWorksheet->setCellValue("E2", "BULAN : " . strtoupper(strftime('%B', mktime(0, 0, 0, DateTime::createFromFormat('Y-m-d', $_POST['to'])->format('m'), 1 ))));
			
			$data = json_decode($_POST['d_data']);
			$ri04_start_row_num = 6;
			$ri04_end_row_num = 6;
			$row_num = $ri04_start_row_num;
			$no = 1;
			foreach ($data as $d) {
				//number of copied rows:
				$number_copy_rows = 0;
				$arr_rpa_id_dokter = explode(";", $d->rpa_id_dokter_pdr);
				$arr_rpa_kode_dokter = explode(";", $d->rpa_kode_dokter_pdr);
				$arr_rpa_nama_dokter = explode(";", $d->rpa_nama_dokter_pdr);
				$arr_rpa_harga_dokter = explode(";", $d->rpa_harga_dokter_pdr);
				if (count($arr_rpa_id_dokter) > $number_copy_rows)
					$number_copy_rows = count($arr_rpa_id_dokter);
				$arr_rpb_id_dokter = explode(";", $d->rpb_id_dokter_pdr);
				$arr_rpb_kode_dokter = explode(";", $d->rpb_kode_dokter_pdr);
				$arr_rpb_nama_dokter = explode(";", $d->rpb_nama_dokter_pdr);
				$arr_rpb_harga_dokter = explode(";", $d->rpb_harga_dokter_pdr);
				if (count($arr_rpb_id_dokter) > $number_copy_rows)
					$number_copy_rows = count($arr_rpb_id_dokter);
				$arr_icu_id_dokter = explode(";", $d->icu_id_dokter_pdr);
				$arr_icu_kode_dokter = explode(";", $d->icu_kode_dokter_pdr);
				$arr_icu_nama_dokter = explode(";", $d->icu_nama_dokter_pdr);
				$arr_icu_harga_dokter = explode(";", $d->icu_harga_dokter_pdr);
				if (count($arr_icu_id_dokter) > $number_copy_rows)
					$number_copy_rows = count($arr_icu_id_dokter);
				$arr_igd_id_dokter = explode(";", $d->igd_id_dokter_pdr);
				$arr_igd_kode_dokter = explode(";", $d->igd_kode_dokter_pdr);
				$arr_igd_nama_dokter = explode(";", $d->igd_nama_dokter_pdr);
				$arr_igd_harga_dokter = explode(";", $d->igd_harga_dokter_pdr);
				if (count($arr_igd_id_dokter) > $number_copy_rows)
					$number_copy_rows = count($arr_igd_id_dokter);
				$arr_ok_id_operator_1 = explode(";", $d->ok_id_operator_1);
				$arr_ok_kode_operator_1 = explode(";", $d->ok_kode_operator_1);
				$arr_ok_nama_operator_1 = explode(";", $d->ok_nama_operator_1);
				$arr_ok_harga_operator_1 = explode(";", $d->ok_h_operator_1);
				if (count($arr_ok_id_operator_1) > $number_copy_rows)
					$number_copy_rows = count($arr_ok_id_operator_1);
				$arr_ok_id_operator_2 = explode(";", $d->ok_id_operator_2);
				$arr_ok_kode_operator_2 = explode(";", $d->ok_kode_operator_2);
				$arr_ok_nama_operator_2 = explode(";", $d->ok_nama_operator_2);
				$arr_ok_harga_operator_2 = explode(";", $d->ok_h_operator_2);
				if (count($arr_ok_id_operator_2) > $number_copy_rows)
					$number_copy_rows = count($arr_ok_id_operator_2);
				$arr_ok_id_anestesi = explode(";", $d->ok_id_anestesi);
				$arr_ok_kode_anestesi = explode(";", $d->ok_kode_anestesi);
				$arr_ok_nama_anestesi = explode(";", $d->ok_nama_anestesi);
				$arr_ok_harga_anestesi = explode(";", $d->ok_h_anestesi);
				if (count($arr_ok_id_anestesi) > $number_copy_rows)
					$number_copy_rows = count($arr_ok_id_anestesi);
				$arr_rpkk_id_dokter = explode(";", $d->rpkk_id_dokter_pdr);
				$arr_rpkk_kode_dokter = explode(";", $d->rpkk_kode_dokter_pdr);
				$arr_rpkk_nama_dokter = explode(";", $d->rpkk_nama_dokter_pdr);
				$arr_rpkk_harga_dokter = explode(";", $d->rpkk_harga_dokter_pdr);
				if (count($arr_rpkk_id_dokter) > $number_copy_rows)
					$number_copy_rows = count($arr_rpkk_id_dokter);
				$arr_hemodialisa_id_dokter = explode(";", $d->hemodialisa_id_dokter_pdr);
				$arr_hemodialisa_kode_dokter = explode(";", $d->hemodialisa_kode_dokter_pdr);
				$arr_hemodialisa_nama_dokter = explode(";", $d->hemodialisa_nama_dokter_pdr);
				$arr_hemodialisa_harga_dokter = explode(";", $d->hemodialisa_harga_dokter_pdr);
				if (count($arr_hemodialisa_id_dokter) > $number_copy_rows)
					$number_copy_rows = count($arr_hemodialisa_id_dokter);
				$arr_id_dokter_rontgen = explode(";", $d->id_dokter_rontgen);
				$arr_kode_dokter_rontgen = explode(";", $d->kode_dokter_rontgen);
				$arr_nama_dokter_rontgen = explode(";", $d->nama_dokter_rontgen);
				$arr_harga_dokter_rontgen = explode(";", $d->harga_dokter_rontgen);
				if (count($arr_id_dokter_rontgen) > $number_copy_rows)
					$number_copy_rows = count($arr_id_dokter_rontgen);
				$arr_id_dokter_echo = explode(";", $d->id_dokter_echo);
				$arr_kode_dokter_echo = explode(";", $d->kode_dokter_echo);
				$arr_nama_dokter_echo = explode(";", $d->nama_dokter_echo);
				$arr_harga_dokter_echo = explode(";", $d->harga_dokter_echo);
				if (count($arr_id_dokter_rontgen) > $number_copy_rows)
					$number_copy_rows = count($arr_id_dokter_echo);
				if ($number_copy_rows > 1)
					$objWorksheet->insertNewRowBefore($row_num + 1, $number_copy_rows - 1);
				
				for ($i = 0; $i < $number_copy_rows; $i++) {
					$col_num = 0;
					$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $no);
					$col_num++;
					$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->kelas);
					$col_num++;
					$objWorksheet->getCellByColumnAndRow($col_num, $row_num)->setValueExplicit(ArrayAdapter::format("only-digit6", $d->noreg), PHPExcel_Cell_DataType::TYPE_STRING);
					$col_num++;
					$objWorksheet->getCellByColumnAndRow($col_num, $row_num)->setValueExplicit(ArrayAdapter::format("only-digit6", $d->nrm), PHPExcel_Cell_DataType::TYPE_STRING);
					$col_num++;
					$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->nama);
					$col_num++;
					$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->jenis);
					$col_num++;
					$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->perusahaan);
					$col_num++;
					$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->asuransi);
					$col_num++;
					if ($i == 0)
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->rpa_sewa_kamar);
					else
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, "");
					$col_num++;
					if ($i < count($arr_rpa_harga_dokter))
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $arr_rpa_harga_dokter[$i]);
					else
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, "");
					$col_num++;
					if ($i == 0)
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->rpa_jasa_medis);
					else
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, "");
					$col_num++;
					if ($i == 0)
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->rpa_alok);
					else
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, "");
					$col_num++;
					if ($i == 0)
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->rpa_ecg);
					else
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, "");
					$col_num++;
					if ($i == 0)
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->rpa_lavement);
					else
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, "");
					$col_num++;
					if ($i == 0)
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->rpa_lain);
					else
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, "");
					$col_num++;
					if ($i == 0)
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->rpb_sewa_kamar);
					else
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, "");
					$col_num++;
					if ($i < count($arr_rpb_harga_dokter))
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $arr_rpb_harga_dokter[$i]);
					else
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, "");
					$col_num++;
					if ($i == 0)
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->rpb_jasa_medis);
					else
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, "");
					$col_num++;
					if ($i == 0)
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->rpb_alok);
					else
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, "");
					$col_num++;
					if ($i == 0)
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->rpb_ecg);
					else
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, "");
					$col_num++;
					if ($i == 0)
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->rpb_lavement);
					else
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, "");
					$col_num++;
					if ($i == 0)
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->rpb_lain);
					else
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, "");
					$col_num++;
					if ($i == 0)
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->icu_sewa_kamar);
					else
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, "");
					$col_num++;
					if ($i < count($arr_icu_harga_dokter))
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $arr_icu_harga_dokter[$i]);
					else
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, "");
					$col_num++;
					if ($i == 0)
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->icu_jasa_medis);
					else
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, "");
					$col_num++;
					if ($i == 0)
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->icu_alok);
					else
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, "");
					$col_num++;
					if ($i == 0)
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->icu_ecg);
					else
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, "");
					$col_num++;
					if ($i == 0)
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->icu_lain);
					else
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, "");
					$col_num++;
					if ($i == 0)
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->igd_sewa_kamar);
					else
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, "");
					$col_num++;
					if ($i < count($arr_igd_harga_dokter))
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $arr_igd_harga_dokter[$i]);
					else
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, "");
					$col_num++;
					if ($i == 0)
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->igd_jasa_medis);
					else
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, "");
					$col_num++;
					if ($i == 0)
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->igd_alok);
					else
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, "");
					$col_num++;
					if ($i == 0)
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->icu_ecg);
					else
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, "");
					$col_num++;
					if ($i == 0)
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->ok_sewa_kamar);
					else
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, "");
					$col_num++;
					if ($i == 0)
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->ok_nominal_pdr);
					else
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, "");
					$col_num++;
					if ($i == 0)
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->ok_jasa_medis);
					else
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, "");
					$col_num++;
					if ($i == 0)
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->ok_alok);
					else
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, "");
					$col_num++;
					if ($i == 0)
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->ok_anestesi);
					else
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, "");
					$col_num++;
					if ($i == 0)
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->rpkk_sewa_kamar);
					else
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, "");
					$col_num++;
					if ($i < count($arr_rpkk_harga_dokter))
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $arr_rpkk_harga_dokter[$i]);
					else
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, "");
					$col_num++;
					if ($i == 0)
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->rpkk_jasa_medis);
					else
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, "");
					$col_num++;
					if ($i == 0)
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->rpkk_alok);
					else
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, "");
					$col_num++;
					if ($i == 0)
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->rpkk_ecg);
					else
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, "");
					$col_num++;
					if ($i == 0)
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->rpkk_jasa_bidan);
					else
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, "");
					$col_num++;
					if ($i == 0)
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->rpkk_lavement);
					else
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, "");
					$col_num++;
					if ($i == 0)
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->rpkk_lain);
					else
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, "");
					$col_num++;
					if ($i == 0)
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->laboratorium);
					else
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, "");
					$col_num++;
					if ($i == 0)
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->pmi);
					else
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, "");
					$col_num++;
					if ($i == 0)
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->depo_farmasi);
					else
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, "");
					$col_num++;
					if ($i == 0)
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->rontgen);
					else
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, "");
					$col_num++;
					if ($i == 0)
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->ctscan);
					else
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, "");
					$col_num++;
					if ($i == 0)
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->echo);
					else
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, "");
					$col_num++;
					if ($i == 0)
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->fisioterapi);
					else
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, "");
					$col_num++;
					if ($i == 0)
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->gizi);
					else
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, "");
					$col_num++;
					if ($i == 0)
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->ambulan);
					else
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, "");
					$col_num++;
					if ($i == 0)
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->administrasi);
					else
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, "");
					$col_num++;
					if ($i == 0)
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->billing);
					else
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, "");
					$col_num++;
					if ($i == 0)
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->hemodialisa_sewa_kamar);
					else
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, "");
					$col_num++;
					if ($i < count($arr_hemodialisa_harga_dokter))
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $arr_hemodialisa_harga_dokter[$i]);
					else
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, "");
					$col_num++;
					if ($i == 0)
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->hemodialisa_jasa_medis);
					else
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, "");
					$col_num++;
					if ($i == 0)
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->hemodialisa_alok);
					else
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, "");
					$col_num++;
					if ($i == 0)
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->hemodialisa_lain);
					else
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, "");
					$col_num++;
					$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, "=SUM(I" . $row_num . ":BJ" . $row_num . ")");
					$col_num++;
					if ($i == 0)
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->bayar_tunai);
					else
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, "");
					$col_num++;
					if ($i == 0)
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->bayar_bank);
					else
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, "");
					$col_num++;
					if ($i == 0)
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->bayar_asuransi);
					else
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, "");
					
					//kertas kerja pdr:
					$col_num += 3;
					if ($i < count($arr_rpa_id_dokter))
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $arr_rpa_id_dokter[$i]);
					else
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, "-");
					$col_num++;
					if ($i < count($arr_rpa_kode_dokter))
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $arr_rpa_kode_dokter[$i]);
					else
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, "-");
					$col_num++;
					if ($i < count($arr_rpa_nama_dokter))
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, ArrayAdapter::format("unslug", $arr_rpa_nama_dokter[$i]));
					else
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, "-");
					$col_num++;
					$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, "=J" . $row_num);
					$col_num++;
					if ($i < count($arr_rpb_id_dokter))
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $arr_rpb_id_dokter[$i]);
					else
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, "-");
					$col_num++;
					if ($i < count($arr_rpb_kode_dokter))
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $arr_rpb_kode_dokter[$i]);
					else
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, "-");
					$col_num++;
					if ($i < count($arr_rpb_nama_dokter))
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, ArrayAdapter::format("unslug", $arr_rpb_nama_dokter[$i]));
					else
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, "-");
					$col_num++;
					$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, "=Q" . $row_num);
					$col_num++;
					if ($i < count($arr_icu_id_dokter))
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $arr_icu_id_dokter[$i]);
					else
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, "-");
					$col_num++;
					if ($i < count($arr_icu_kode_dokter))
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $arr_icu_kode_dokter[$i]);
					else
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, "-");
					$col_num++;
					if ($i < count($arr_icu_nama_dokter))
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, ArrayAdapter::format("unslug", $arr_icu_nama_dokter[$i]));
					else
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, "-");
					$col_num++;
					$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, "=X" . $row_num);
					$col_num++;
					if ($i < count($arr_igd_id_dokter))
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $arr_igd_id_dokter[$i]);
					else
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, "-");
					$col_num++;
					if ($i < count($arr_igd_kode_dokter))
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $arr_igd_kode_dokter[$i]);
					else
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, "-");
					$col_num++;
					if ($i < count($arr_igd_nama_dokter))
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, ArrayAdapter::format("unslug", $arr_igd_nama_dokter[$i]));
					else
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, "-");
					$col_num++;
					$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, "=AD" . $row_num);
					$col_num++;
					if ($i < count($arr_ok_id_operator_1))
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $arr_ok_id_operator_1[$i]);
					else
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, "-");
					$col_num++;
					if ($i < count($arr_ok_kode_operator_1))
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $arr_ok_kode_operator_1[$i]);
					else
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, "-");
					$col_num++;
					if ($i < count($arr_ok_nama_operator_1))
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, ArrayAdapter::format("unslug", $arr_ok_nama_operator_1[$i]));
					else
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, "-");
					$col_num++;
					if ($i < count($arr_ok_harga_operator_1))
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $arr_ok_harga_operator_1[$i]);
					else
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, "");
					$col_num++;
					if ($i < count($arr_ok_id_operator_2))
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $arr_ok_id_operator_2[$i]);
					else
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, "-");
					$col_num++;
					if ($i < count($arr_ok_kode_operator_2))
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $arr_ok_kode_operator_2[$i]);
					else
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, "-");
					$col_num++;
					if ($i < count($arr_ok_nama_operator_2))
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, ArrayAdapter::format("unslug", $arr_ok_nama_operator_2[$i]));
					else
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, "-");
					$col_num++;
					if ($i < count($arr_ok_harga_operator_2))
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $arr_ok_harga_operator_2[$i]);
					else
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, "");
					$col_num++;
					if ($i < count($arr_ok_id_anestesi))
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $arr_ok_id_anestesi[$i]);
					else
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, "-");
					$col_num++;
					if ($i < count($arr_ok_kode_anestesi))
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $arr_ok_kode_anestesi[$i]);
					else
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, "-");
					$col_num++;
					if ($i < count($arr_ok_nama_anestesi))
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, ArrayAdapter::format("unslug", $arr_ok_nama_anestesi[$i]));
					else
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, "-");
					$col_num++;
					if ($i < count($arr_ok_harga_anestesi))
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $arr_ok_harga_anestesi[$i]);
					else
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, "");
					$col_num++;
					if ($i < count($arr_rpkk_id_dokter))
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $arr_rpkk_id_dokter[$i]);
					else
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, "-");
					$col_num++;
					if ($i < count($arr_rpkk_kode_dokter))
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $arr_rpkk_kode_dokter[$i]);
					else
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, "-");
					$col_num++;
					if ($i < count($arr_rpkk_nama_dokter))
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, ArrayAdapter::format("unslug", $arr_rpkk_nama_dokter[$i]));
					else
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, "-");
					$col_num++;
					$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, "=AN" . $row_num);
					$col_num++;
					if ($i < count($arr_hemodialisa_id_dokter))
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $arr_hemodialisa_id_dokter[$i]);
					else
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, "-");
					$col_num++;
					if ($i < count($arr_hemodialisa_kode_dokter))
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $arr_hemodialisa_kode_dokter[$i]);
					else
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, "-");
					$col_num++;
					if ($i < count($arr_hemodialisa_nama_dokter))
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, ArrayAdapter::format("unslug", $arr_hemodialisa_nama_dokter[$i]));
					else
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, "-");
					$col_num++;
					$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, "=BG" . $row_num);
					$col_num++;
					if ($i < count($arr_id_dokter_rontgen))
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $arr_id_dokter_rontgen[$i]);
					else
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, "-");
					$col_num++;
					if ($i < count($arr_kode_dokter_rontgen))
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $arr_kode_dokter_rontgen[$i]);
					else
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, "-");
					$col_num++;
					if ($i < count($arr_nama_dokter_rontgen))
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, ArrayAdapter::format("unslug", $arr_nama_dokter_rontgen[$i]));
					else
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, "-");
					$col_num++;
					if ($i < count($arr_harga_dokter_rontgen))
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $arr_harga_dokter_rontgen[$i]);
					else
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, "");
					$col_num++;
					if ($i < count($arr_id_dokter_echo))
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $arr_id_dokter_echo[$i]);
					else
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, "-");
					$col_num++;
					if ($i < count($arr_kode_dokter_echo))
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $arr_kode_dokter_echo[$i]);
					else
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, "-");
					$col_num++;
					if ($i < count($arr_nama_dokter_echo))
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, ArrayAdapter::format("unslug", $arr_nama_dokter_echo[$i]));
					else
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, "-");
					$col_num++;
					if ($i < count($arr_harga_dokter_echo))
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $arr_harga_dokter_echo[$i]);
					else
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, "");
					
					$row_num++;
					$ri04_end_row_num++;
					$no++;
				}
			}
			
			for($col_num = 8, $alphabet = "I"; $alphabet != "DI"; $alphabet++, $col_num++) {
				if ($alphabet != "BO" && $alphabet != "BP" 
				 && $alphabet != "BQ" && $alphabet != "BR" && $alphabet != "BS"
				 && $alphabet != "BU" && $alphabet != "BV" && $alphabet != "BW"
				 && $alphabet != "BY" && $alphabet != "BZ" && $alphabet != "CA"
				 && $alphabet != "CC" && $alphabet != "CD" && $alphabet != "CE"
				 && $alphabet != "CG" && $alphabet != "CH" && $alphabet != "CI"
				 && $alphabet != "CK" && $alphabet != "CL" && $alphabet != "CM"
				 && $alphabet != "CO" && $alphabet != "CP" && $alphabet != "CQ"
				 && $alphabet != "CS" && $alphabet != "CT" && $alphabet != "CU"
				 && $alphabet != "CW" && $alphabet != "CX" && $alphabet != "CY"
				 && $alphabet != "DA" && $alphabet != "DB" && $alphabet != "DC"
				 && $alphabet != "DE" && $alphabet != "DF" && $alphabet != "DG")
					$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, "=SUM(" . $alphabet . "6:" . $alphabet . ($row_num - 1) . ")");
			}
			
			$row_num+=2;
			$objWorksheet->setCellValue("A" . $row_num, "JEMBER, TGL. " . date("d-m-Y"));
			$row_num+=5;
			global $user;
			$objWorksheet->setCellValue("A" . $row_num, strtoupper($user->getNameOnly()));
			
			//Rekapitulasi Per Dokter RI-04:
			$row_num = $ri04_end_row_num + 11;
			if ($md_end_row_num - $md_start_row_num > 1)
				$objWorksheet->insertNewRowBefore($row_num + 1, $md_end_row_num - $md_start_row_num - 1);
			$rekap_pdr_start_row_num = $row_num;
			$rekap_pdr_end_row_num = $row_num;
			for ($i = 0; $i < $md_end_row_num - $md_start_row_num; $i++, $row_num++, $rekap_pdr_end_row_num++) {
				$objWorksheet->setCellValue("BQ" . $row_num, "='MASTER DOKTER'!A" . ($i + 3));
				$objWorksheet->setCellValue("BR" . $row_num, "='MASTER DOKTER'!B" . ($i + 3));
				$objWorksheet->setCellValue("BS" . $row_num, "='MASTER DOKTER'!C" . ($i + 3));
				$objWorksheet->setCellValue("BT" . $row_num, "=SUMIF(BQ" . $ri04_start_row_num . ":BQ" . ($ri04_end_row_num - 1) . ",BQ" . $row_num . ",BT" . $ri04_start_row_num . ":BT" . ($ri04_end_row_num - 1) . ")");
				
				$objWorksheet->setCellValue("BU" . $row_num, "='MASTER DOKTER'!A" . ($i + 3));
				$objWorksheet->setCellValue("BV" . $row_num, "='MASTER DOKTER'!B" . ($i + 3));
				$objWorksheet->setCellValue("BW" . $row_num, "='MASTER DOKTER'!C" . ($i + 3));
				$objWorksheet->setCellValue("BX" . $row_num, "=SUMIF(BU" . $ri04_start_row_num . ":BU" . ($ri04_end_row_num - 1) . ",BU" . $row_num . ",BX" . $ri04_start_row_num . ":BX" . ($ri04_end_row_num - 1) . ")");
				
				$objWorksheet->setCellValue("BY" . $row_num, "='MASTER DOKTER'!A" . ($i + 3));
				$objWorksheet->setCellValue("BZ" . $row_num, "='MASTER DOKTER'!B" . ($i + 3));
				$objWorksheet->setCellValue("CA" . $row_num, "='MASTER DOKTER'!C" . ($i + 3));
				$objWorksheet->setCellValue("CB" . $row_num, "=SUMIF(BY" . $ri04_start_row_num . ":BY" . ($ri04_end_row_num - 1) . ",BY" . $row_num . ",CB" . $ri04_start_row_num . ":CB" . ($ri04_end_row_num - 1) . ")");
				
				$objWorksheet->setCellValue("CC" . $row_num, "='MASTER DOKTER'!A" . ($i + 3));
				$objWorksheet->setCellValue("CD" . $row_num, "='MASTER DOKTER'!B" . ($i + 3));
				$objWorksheet->setCellValue("CE" . $row_num, "='MASTER DOKTER'!C" . ($i + 3));
				$objWorksheet->setCellValue("CF" . $row_num, "=SUMIF(CC" . $ri04_start_row_num . ":CC" . ($ri04_end_row_num - 1) . ",CC" . $row_num . ",CF" . $ri04_start_row_num . ":CF" . ($ri04_end_row_num - 1) . ")");
				
				$objWorksheet->setCellValue("CG" . $row_num, "='MASTER DOKTER'!A" . ($i + 3));
				$objWorksheet->setCellValue("CH" . $row_num, "='MASTER DOKTER'!B" . ($i + 3));
				$objWorksheet->setCellValue("CI" . $row_num, "='MASTER DOKTER'!C" . ($i + 3));
				$objWorksheet->setCellValue("CJ" . $row_num, "=SUMIF(CG" . $ri04_start_row_num . ":CG" . ($ri04_end_row_num - 1) . ",CG" . $row_num . ",CJ" . $ri04_start_row_num . ":CJ" . ($ri04_end_row_num - 1) . ")");
				
				$objWorksheet->setCellValue("CK" . $row_num, "='MASTER DOKTER'!A" . ($i + 3));
				$objWorksheet->setCellValue("CL" . $row_num, "='MASTER DOKTER'!B" . ($i + 3));
				$objWorksheet->setCellValue("CM" . $row_num, "='MASTER DOKTER'!C" . ($i + 3));
				$objWorksheet->setCellValue("CN" . $row_num, "=SUMIF(CK" . $ri04_start_row_num . ":CK" . ($ri04_end_row_num - 1) . ",CK" . $row_num . ",CN" . $ri04_start_row_num . ":CN" . ($ri04_end_row_num - 1) . ")");
				
				$objWorksheet->setCellValue("CO" . $row_num, "='MASTER DOKTER'!A" . ($i + 3));
				$objWorksheet->setCellValue("CP" . $row_num, "='MASTER DOKTER'!B" . ($i + 3));
				$objWorksheet->setCellValue("CQ" . $row_num, "='MASTER DOKTER'!C" . ($i + 3));
				$objWorksheet->setCellValue("CR" . $row_num, "=SUMIF(CO" . $ri04_start_row_num . ":CO" . ($ri04_end_row_num - 1) . ",CO" . $row_num . ",CR" . $ri04_start_row_num . ":CR" . ($ri04_end_row_num - 1) . ")");
				
				$objWorksheet->setCellValue("CS" . $row_num, "='MASTER DOKTER'!A" . ($i + 3));
				$objWorksheet->setCellValue("CT" . $row_num, "='MASTER DOKTER'!B" . ($i + 3));
				$objWorksheet->setCellValue("CU" . $row_num, "='MASTER DOKTER'!C" . ($i + 3));
				$objWorksheet->setCellValue("CV" . $row_num, "=SUMIF(CS" . $ri04_start_row_num . ":CS" . ($ri04_end_row_num - 1) . ",CS" . $row_num . ",CV" . $ri04_start_row_num . ":CV" . ($ri04_end_row_num - 1) . ")");
				
				$objWorksheet->setCellValue("CW" . $row_num, "='MASTER DOKTER'!A" . ($i + 3));
				$objWorksheet->setCellValue("CX" . $row_num, "='MASTER DOKTER'!B" . ($i + 3));
				$objWorksheet->setCellValue("CY" . $row_num, "='MASTER DOKTER'!C" . ($i + 3));
				$objWorksheet->setCellValue("CZ" . $row_num, "=SUMIF(CW" . $ri04_start_row_num . ":CW" . ($ri04_end_row_num - 1) . ",CW" . $row_num . ",CZ" . $ri04_start_row_num . ":CZ" . ($ri04_end_row_num - 1) . ")");
				
				$objWorksheet->setCellValue("DA" . $row_num, "='MASTER DOKTER'!A" . ($i + 3));
				$objWorksheet->setCellValue("DB" . $row_num, "='MASTER DOKTER'!B" . ($i + 3));
				$objWorksheet->setCellValue("DC" . $row_num, "='MASTER DOKTER'!C" . ($i + 3));
				$objWorksheet->setCellValue("DD" . $row_num, "=SUMIF(DA" . $ri04_start_row_num . ":DA" . ($ri04_end_row_num - 1) . ",DA" . $row_num . ",DD" . $ri04_start_row_num . ":DD" . ($ri04_end_row_num - 1) . ")");
				
				$objWorksheet->setCellValue("DE" . $row_num, "='MASTER DOKTER'!A" . ($i + 3));
				$objWorksheet->setCellValue("DF" . $row_num, "='MASTER DOKTER'!B" . ($i + 3));
				$objWorksheet->setCellValue("DG" . $row_num, "='MASTER DOKTER'!C" . ($i + 3));
				$objWorksheet->setCellValue("DH" . $row_num, "=SUMIF(DE" . $ri04_start_row_num . ":DE" . ($ri04_end_row_num - 1) . ",DE" . $row_num . ",DH" . $ri04_start_row_num . ":DH" . ($ri04_end_row_num - 1) . ")");
			}
			$objWorksheet->setCellValue("BT" . $rekap_pdr_end_row_num, "=SUM(BT" . $rekap_pdr_start_row_num . ":BT" . ($rekap_pdr_end_row_num - 1) . ")");
			$objWorksheet->setCellValue("BX" . $rekap_pdr_end_row_num, "=SUM(BX" . $rekap_pdr_start_row_num . ":BX" . ($rekap_pdr_end_row_num - 1) . ")");
			$objWorksheet->setCellValue("CB" . $rekap_pdr_end_row_num, "=SUM(CB" . $rekap_pdr_start_row_num . ":CB" . ($rekap_pdr_end_row_num - 1) . ")");
			$objWorksheet->setCellValue("CF" . $rekap_pdr_end_row_num, "=SUM(CF" . $rekap_pdr_start_row_num . ":CF" . ($rekap_pdr_end_row_num - 1) . ")");
			$objWorksheet->setCellValue("CJ" . $rekap_pdr_end_row_num, "=SUM(CJ" . $rekap_pdr_start_row_num . ":CJ" . ($rekap_pdr_end_row_num - 1) . ")");
			$objWorksheet->setCellValue("CN" . $rekap_pdr_end_row_num, "=SUM(CN" . $rekap_pdr_start_row_num . ":CN" . ($rekap_pdr_end_row_num - 1) . ")");
			$objWorksheet->setCellValue("CR" . $rekap_pdr_end_row_num, "=SUM(CR" . $rekap_pdr_start_row_num . ":CR" . ($rekap_pdr_end_row_num - 1) . ")");
			$objWorksheet->setCellValue("CV" . $rekap_pdr_end_row_num, "=SUM(CV" . $rekap_pdr_start_row_num . ":CV" . ($rekap_pdr_end_row_num - 1) . ")");
			$objWorksheet->setCellValue("CZ" . $rekap_pdr_end_row_num, "=SUM(CZ" . $rekap_pdr_start_row_num . ":CZ" . ($rekap_pdr_end_row_num - 1) . ")");
			$objWorksheet->setCellValue("DD" . $rekap_pdr_end_row_num, "=SUM(DD" . $rekap_pdr_start_row_num . ":DD" . ($rekap_pdr_end_row_num - 1) . ")");
			$objWorksheet->setCellValue("DH" . $rekap_pdr_end_row_num, "=SUM(DH" . $rekap_pdr_start_row_num . ":DH" . ($rekap_pdr_end_row_num - 1) . ")");
			
			//RI-05:
			$objPHPExcel->setActiveSheetIndexByName("RI-05");
			$objWorksheet = $objPHPExcel->getActiveSheet();
			$objWorksheet->setCellValue("D1", "BRI " . date("m") . "  B");
			$objWorksheet->setCellValue("D3", date("d-m-Y"));
			for($row_num = 7, $col_num = 3, $alphabet = "I"; $alphabet != "BJ"; $row_num++) {
				if ($row_num != 14 && $row_num != 22 && $row_num != 29 && $row_num != 35 && $row_num != 41 && $row_num != 50 && $row_num != 62 && $row_num != 68) {
					if ($row_num != 69)
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, "='RI-04'!" . $alphabet . $ri04_end_row_num);
					else 
						$objWorksheet->setCellValueByColumnAndRow($col_num - 1, $row_num, "='RI-04'!" . $alphabet . $ri04_end_row_num);
					$alphabet++;
				}
			}
			
			//RI-06:
			$objPHPExcel->setActiveSheetIndexByName("RI-06");
			$objWorksheet = $objPHPExcel->getActiveSheet();
			$objWorksheet->setCellValue("G1", strtoupper(strftime('%B', mktime(0, 0, 0, DateTime::createFromFormat('Y-m-d', $_POST['to'])->format('m'), 1 ))));
			$objWorksheet->setCellValue("G2", "M." . date("m.d") . "BRI");
			$objWorksheet->setCellValue("G3", date("d-m-Y"));
			$row_num = 7;
			$objWorksheet->setCellValue("E" . $row_num, "='RI-05'!D1");
			$objWorksheet->setCellValue("F" . $row_num++, "=SUM('RI-04'!BT" . $rekap_pdr_start_row_num . ":'RI-04'!BT" . ($rekap_pdr_end_row_num - 1) . ")");
			$objWorksheet->setCellValue("E" . $row_num, "='RI-05'!D1");
			$objWorksheet->setCellValue("F" . $row_num++, "=SUM('RI-04'!BX" . $rekap_pdr_start_row_num . ":'RI-04'!BX" . ($rekap_pdr_end_row_num - 1) . ")");
			$objWorksheet->setCellValue("E" . $row_num, "='RI-05'!D1");
			$objWorksheet->setCellValue("F" . $row_num++, "=SUM('RI-04'!CB" . $rekap_pdr_start_row_num . ":'RI-04'!CB" . ($rekap_pdr_end_row_num - 1) . ")");
			$objWorksheet->setCellValue("E" . $row_num, "='RI-05'!D1");
			$objWorksheet->setCellValue("F" . $row_num++, "=SUM('RI-04'!CF" . $rekap_pdr_start_row_num . ":'RI-04'!CF" . ($rekap_pdr_end_row_num - 1) . ")");
			$objWorksheet->setCellValue("E" . $row_num, "='RI-05'!D1");
			$objWorksheet->setCellValue("F" . $row_num++, "=SUM('RI-04'!CJ" . $rekap_pdr_start_row_num . ":'RI-04'!CJ" . ($rekap_pdr_end_row_num - 1) . ") + SUM('RI-04'!CN" . $rekap_pdr_start_row_num . ":'RI-04'!CN" . ($rekap_pdr_end_row_num - 1) . ") + SUM('RI-04'!CR" . $rekap_pdr_start_row_num . ":'RI-04'!CR" . ($rekap_pdr_end_row_num - 1) . ")");
			$objWorksheet->setCellValue("E" . $row_num, "='RI-05'!D1");
			$objWorksheet->setCellValue("F" . $row_num++, "=SUM('RI-04'!CV" . $rekap_pdr_start_row_num . ":'RI-04'!CV" . ($rekap_pdr_end_row_num - 1) . ")");
			$objWorksheet->setCellValue("E" . $row_num, "='RI-05'!D1");
			$objWorksheet->setCellValue("F" . $row_num++, "=SUM('RI-04'!CZ" . $rekap_pdr_start_row_num . ":'RI-04'!CZ" . ($rekap_pdr_end_row_num - 1) . ")");
			$objWorksheet->setCellValue("E" . $row_num, "='RI-05'!D1");
			$objWorksheet->setCellValue("F" . $row_num++, "=SUM('RI-04'!DD" . $rekap_pdr_start_row_num . ":'RI-04'!DD" . ($rekap_pdr_end_row_num - 1) . ")");
			$objWorksheet->setCellValue("E" . $row_num, "='RI-05'!D1");
			$objWorksheet->setCellValue("F" . $row_num++, "=SUM('RI-04'!DH" . $rekap_pdr_start_row_num . ":'RI-04'!DH" . ($rekap_pdr_end_row_num - 1) . ")");
			if ($md_end_row_num - $md_start_row_num > 1)
				$objWorksheet->insertNewRowBefore($row_num + 1, $md_end_row_num - $md_start_row_num - 1);
			for ($i = 0, $rekap_pdr_row_num = $rekap_pdr_start_row_num; $i < $md_end_row_num - $md_start_row_num; $i++, $row_num++, $rekap_pdr_row_num++) {
				$objWorksheet->setCellValue("J" . $row_num, "='MASTER DOKTER'!A" . ($i + 3));
				$objWorksheet->setCellValue("A" . $row_num, "='MASTER DOKTER'!B" . ($i + 3));
				$objWorksheet->setCellValue("B" . $row_num, "='MASTER DOKTER'!C" . ($i + 3));
				$objWorksheet->setCellValue("E" . $row_num, "='RI-05'!D1");
				$objWorksheet->setCellValue("G" . $row_num, "='RI-04'!BT" . $rekap_pdr_row_num 
														  . "+'RI-04'!BX" . $rekap_pdr_row_num 
														  . "+'RI-04'!CB" . $rekap_pdr_row_num 
														  . "+'RI-04'!CF" . $rekap_pdr_row_num
														  . "+'RI-04'!CJ" . $rekap_pdr_row_num
														  . "+'RI-04'!CN" . $rekap_pdr_row_num
														  . "+'RI-04'!CR" . $rekap_pdr_row_num
														  . "+'RI-04'!CV" . $rekap_pdr_row_num
														  . "+'RI-04'!CZ" . $rekap_pdr_row_num
														  . "+'RI-04'!DD" . $rekap_pdr_row_num
														  . "+'RI-04'!DH" . $rekap_pdr_row_num);
				$objWorksheet->setCellValue("K" . $row_num, "=G" . $row_num);
				$objWorksheet->setCellValue("L" . $row_num, "=IF(K" . $row_num . " < 50000000, 5%, IF(K" . $row_num . " < 250000000, 15%, IF(K" . $row_num . " < 500000000, 25%, 30%)))");
				$objWorksheet->setCellValue("M" . $row_num, "=K" . $row_num . "*'MASTER DOKTER'!E" . ($i + 3));
				$objWorksheet->setCellValue("N" . $row_num, "=M" . $row_num . "*50%");
				$objWorksheet->setCellValue("O" . $row_num, "=L" . $row_num . "*N" . $row_num);
				$objWorksheet->setCellValue("P" . $row_num, "=M" . $row_num . "-O" . $row_num);
			}
			$row_num++;
			$objWorksheet->setCellValue("F" . $row_num, "=SUM(F7:F" . ($row_num - 1) . ")");
			$objWorksheet->setCellValue("G" . $row_num, "=SUM(G7:G" . ($row_num - 1) . ")");
			$objPHPExcel->setActiveSheetIndexByName("RI-04");
			
			header("Content-type: application/vnd.ms-excel");	
			header("Content-Disposition: attachment; filename=JURNAL_RAWAT_INAP_" . strtoupper($_POST['jenis_pasien']) . "_" . $_POST['from'] . "_" . $_POST['to'] . "_" . date("Ymd_His") . ".xlsx");
			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");
			$objWriter->save("php://output");
		} else if ($_POST['command'] == "get_registered") {
			$params = array();
			$params['from'] = $_POST['from'];
			$params['to'] = $_POST['to'];
			$params['jenis_pasien'] = $_POST['jenis_pasien'];
			$params['grup_transaksi'] = "rawat_inap";
			$params['uri'] = 1;
			$service = new ServiceConsumer($db, "get_jumlah_pasien", $params, "registration");
			$service->setMode(ServiceConsumer::$CLEAN_BOTH);
			$content = $service->execute()->getContent();
			$data = array();
			$data['jumlah'] = $content[0];
			$data['timestamp'] = date("d-m-Y H:i:s");
			echo json_encode($data);
		} else if ($_POST['command'] == "get_info") {
			// collect registration info:
			$params = array();
			$params['from'] = $_POST['from'];
			$params['to'] = $_POST['to'];
			$params['jenis_pasien'] = $_POST['jenis_pasien'];
			$params['grup_transaksi'] = "rawat_inap";
			$params['uri'] = 1;
			$params['num'] = $_POST['num'];
			$service = new ServiceConsumer($db, "get_info_pasien", $params, "registration");
			$service->setMode(ServiceConsumer::$CLEAN_BOTH);
			$content = $service->execute()->getContent();
			$noreg_pasien = $content[0];
			$nrm_pasien = $content[1];
			$nama_pasien = $content[2];
			$tanggal = ArrayAdapter::format("date d", $content[3]);
			$jenis_pasien = $content[4];
			$perusahaan = $content[5];
			$asuransi = $content[6];
			
			$total = 0;
			
			// kelas pasien: RPA Argopuro, RPA Bromo, RPA Bromo Baru, RPA Kelud, RPB Argopuro, RPB Ijen, RPB Raung, ICU, IGD, Kamar Operasi, RPKK Argopuro, RPKK Kelud, RPKK Raung, RPKK Semeru, RPKK Wilis
			$kelas_pasien = "-";
			$last_datetime = null;
			$dbtable = new DBTable($db, "smis_rwt_antrian_rpa_argopuro");
			$row = $dbtable->get_row("
				SELECT *
				FROM smis_rwt_antrian_rpa_argopuro
				WHERE no_register = '" . $noreg_pasien . "'
			");
			if ($row != null) {
				$kelas_pasien = getSettings($db, "smis-rs-kelas-rpa_argopuro", "-");
				$last_datetime = new DateTime($row->waktu);
			}
			$dbtable = new DBTable($db, "smis_rwt_antrian_rpa_bromo");
			$row = $dbtable->get_row("
				SELECT *
				FROM smis_rwt_antrian_rpa_bromo
				WHERE no_register = '" . $noreg_pasien . "'
			");
			if ($row != null) {
				if ($last_datetime < new DateTime($row->waktu) || $last_datetime == "-") {
					$last_datetime = new DateTime($row->waktu);
					$kelas_pasien = getSettings($db, "smis-rs-kelas-rpa_bromo", "-");
				}
			}
			$dbtable = new DBTable($db, "smis_rwt_antrian_rpa_bromobaru");
			$row = $dbtable->get_row("
				SELECT *
				FROM smis_rwt_antrian_rpa_bromobaru
				WHERE no_register = '" . $noreg_pasien . "'
			");
			if ($row != null) {
				if ($last_datetime < new DateTime($row->waktu) || $last_datetime == "-") {
					$last_datetime = new DateTime($row->waktu);
					$kelas_pasien = getSettings($db, "smis-rs-kelas-rpa_bromobaru", "-");
				}
			}
			$dbtable = new DBTable($db, "smis_rwt_antrian_rpa_kelud");
			$row = $dbtable->get_row("
				SELECT *
				FROM smis_rwt_antrian_rpa_kelud
				WHERE no_register = '" . $noreg_pasien . "'
			");
			if ($row != null) {
				if ($last_datetime < new DateTime($row->waktu) || $last_datetime == "-") {
					$last_datetime = new DateTime($row->waktu);
					$kelas_pasien = getSettings($db, "smis-rs-kelas-rpa_kelud", "-");
				}
			}
			$dbtable = new DBTable($db, "smis_rwt_antrian_rpb_argopuro");
			$row = $dbtable->get_row("
				SELECT *
				FROM smis_rwt_antrian_rpb_argopuro
				WHERE no_register = '" . $noreg_pasien . "'
			");
			if ($row != null) {
				if ($last_datetime < new DateTime($row->waktu) || $last_datetime == "-") {
					$last_datetime = new DateTime($row->waktu);
					$kelas_pasien = getSettings($db, "smis-rs-kelas-rpb_argopuro", "-");
				}
			}
			$dbtable = new DBTable($db, "smis_rwt_antrian_rpb_ijen");
			$row = $dbtable->get_row("
				SELECT *
				FROM smis_rwt_antrian_rpb_ijen
				WHERE no_register = '" . $noreg_pasien . "'
			");
			if ($row != null) {
				if ($last_datetime < new DateTime($row->waktu) || $last_datetime == "-") {
					$last_datetime = new DateTime($row->waktu);
					$kelas_pasien = getSettings($db, "smis-rs-kelas-rpb_ijen", "-");
				}
			}
			$dbtable = new DBTable($db, "smis_rwt_antrian_rpb_raung");
			$row = $dbtable->get_row("
				SELECT *
				FROM smis_rwt_antrian_rpb_raung
				WHERE no_register = '" . $noreg_pasien . "'
			");
			if ($row != null) {
				if ($last_datetime < new DateTime($row->waktu) || $last_datetime == "-") {
					$last_datetime = new DateTime($row->waktu);
					$kelas_pasien = getSettings($db, "smis-rs-kelas-rpb_raung", "-");
				}
			}
			$dbtable = new DBTable($db, "smis_rwt_antrian_icu");
			$row = $dbtable->get_row("
				SELECT *
				FROM smis_rwt_antrian_icu
				WHERE no_register = '" . $noreg_pasien . "'
			");
			if ($row != null) {
				if ($last_datetime < new DateTime($row->waktu) || $last_datetime == "-") {
					$last_datetime = new DateTime($row->waktu);
					$kelas_pasien = getSettings($db, "smis-rs-kelas-icu", "-");
				}
			}
			$dbtable = new DBTable($db, "smis_rwt_antrian_instalasi_gawat_darurat");
			$row = $dbtable->get_row("
				SELECT *
				FROM smis_rwt_antrian_instalasi_gawat_darurat
				WHERE no_register = '" . $noreg_pasien . "'
			");
			if ($row != null) {
				if ($last_datetime < new DateTime($row->waktu) || $last_datetime == "-") {
					$last_datetime = new DateTime($row->waktu);
					$kelas_pasien = getSettings($db, "smis-rs-kelas-instalasi_gawat_darurat", "-");
				}
			}
			$dbtable = new DBTable($db, "smis_rwt_antrian_kamar_operasi");
			$row = $dbtable->get_row("
				SELECT *
				FROM smis_rwt_antrian_kamar_operasi
				WHERE no_register = '" . $noreg_pasien . "'
			");
			if ($row != null) {
				if ($last_datetime < new DateTime($row->waktu) || $last_datetime == "-") {
					$last_datetime = new DateTime($row->waktu);
					$kelas_pasien = getSettings($db, "smis-rs-kelas-kamar_operasi", "-");
				}
			}
			$dbtable = new DBTable($db, "smis_rwt_antrian_rpkk_argopuro");
			$row = $dbtable->get_row("
				SELECT *
				FROM smis_rwt_antrian_rpkk_argopuro
				WHERE no_register = '" . $noreg_pasien . "'
			");
			if ($row != null) {
				if ($last_datetime < new DateTime($row->waktu) || $last_datetime == "-") {
					$last_datetime = new DateTime($row->waktu);
					$kelas_pasien = getSettings($db, "smis-rs-kelas-rpkk_argopuro", "-");
				}
			}
			$dbtable = new DBTable($db, "smis_rwt_antrian_rpkk_kelud");
			$row = $dbtable->get_row("
				SELECT *
				FROM smis_rwt_antrian_rpkk_kelud
				WHERE no_register = '" . $noreg_pasien . "'
			");
			if ($row != null) {
				if ($last_datetime < new DateTime($row->waktu)) {
					$last_datetime = new DateTime($row->waktu);
					$kelas_pasien = getSettings($db, "smis-rs-kelas-rpkk_kelud", "-");
				}
			}
			$dbtable = new DBTable($db, "smis_rwt_antrian_rpkk_raung");
			$row = $dbtable->get_row("
				SELECT *
				FROM smis_rwt_antrian_rpkk_raung
				WHERE no_register = '" . $noreg_pasien . "'
			");
			if ($row != null) {
				if ($last_datetime < new DateTime($row->waktu)) {
					$last_datetime = new DateTime($row->waktu);
					$kelas_pasien = getSettings($db, "smis-rs-kelas-rpkk_raung", "-");
				}
			}
			$dbtable = new DBTable($db, "smis_rwt_antrian_rpkk_semeru");
			$row = $dbtable->get_row("
				SELECT *
				FROM smis_rwt_antrian_rpkk_semeru
				WHERE no_register = '" . $noreg_pasien . "'
			");
			if ($row != null) {
				if ($last_datetime < new DateTime($row->waktu)) {
					$last_datetime = new DateTime($row->waktu);
					$kelas_pasien = getSettings($db, "smis-rs-kelas-rpkk_semeru", "-");
				}
			}
			$dbtable = new DBTable($db, "smis_rwt_antrian_rpkk_wilis");
			$row = $dbtable->get_row("
				SELECT *
				FROM smis_rwt_antrian_rpkk_wilis
				WHERE no_register = '" . $noreg_pasien . "'
			");
			if ($row != null) {
				if ($last_datetime < new DateTime($row->waktu)) {
					$last_datetime = new DateTime($row->waktu);
					$kelas_pasien = getSettings($db, "smis-rs-kelas-rpkk_wilis", "-");
				}
			}
			
			//sewa kamar rpa: RPA Argopuro, RPA Bromo, RPA Bromo Baru, RPA Kelud
			$total_sewa_kamar_rpa = 0;
			$dbtable = new DBTable($db, "smis_rwt_bed_rpa_argopuro");
			$rows = $dbtable->get_result("
				SELECT waktu_masuk, waktu_keluar 
				FROM smis_rwt_bed_rpa_argopuro 
				WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop NOT LIKE 'del'
			");
			if ($rows != null) {
				foreach ($rows as $row) {					
					$start_date = new DateTime($row->waktu_masuk);
					$end_date = new DateTime($row->waktu_keluar);
					$interval = $start_date->diff($end_date);
					$los = $interval->days + 1;
					$harga_bed = getSettings($db, "smis-rs-uri-bed-rpa_argopuro", 0);
					$total_sewa_kamar_rpa += $los * $harga_bed;
				}
			}
			$dbtable = new DBTable($db, "smis_rwt_bed_rpa_bromo");
			$rows = $dbtable->get_result("
				SELECT waktu_masuk, waktu_keluar 
				FROM smis_rwt_bed_rpa_bromo 
				WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop NOT LIKE 'del'
			");
			if ($rows != null) {
				foreach ($rows as $row) {
					$start_date = new DateTime($row->waktu_masuk);
					$end_date = new DateTime($row->waktu_keluar);
					$interval = $start_date->diff($end_date);
					$los = $interval->days + 1;
					$harga_bed = getSettings($db, "smis-rs-uri-bed-rpa_bromo", 0);
					$total_sewa_kamar_rpa += $los * $harga_bed;
				}
			}
			$dbtable = new DBTable($db, "smis_rwt_bed_rpa_bromobaru");
			$rows = $dbtable->get_result("
				SELECT waktu_masuk, waktu_keluar 
				FROM smis_rwt_bed_rpa_bromobaru 
				WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop NOT LIKE 'del'
			");
			if ($rows != null) {
				foreach ($rows as $row) {
					$start_date = new DateTime($row->waktu_masuk);
					$end_date = new DateTime($row->waktu_keluar);
					$interval = $start_date->diff($end_date);
					$los = $interval->days + 1;
					$harga_bed = getSettings($db, "smis-rs-uri-bed-rpa_bromobaru", 0);
					$total_sewa_kamar_rpa += $los * $harga_bed;
				}
			}
			$dbtable = new DBTable($db, "smis_rwt_bed_rpa_kelud");
			$rows = $dbtable->get_result("
				SELECT waktu_masuk, waktu_keluar 
				FROM smis_rwt_bed_rpa_kelud 
				WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop NOT LIKE 'del'
			");
			if ($rows != null) {
				foreach ($rows as $row) {
					$start_date = new DateTime($row->waktu_masuk);
					$end_date = new DateTime($row->waktu_keluar);
					$interval = $start_date->diff($end_date);
					$los = $interval->days + 1;
					$harga_bed = getSettings($db, "smis-rs-uri-bed-rpa_kelud", 0);
					$total_sewa_kamar_rpa += $los * $harga_bed;
				}
			}
			$total += $total_sewa_kamar_rpa;
			if ($total_sewa_kamar_rpa == 0)
				$total_sewa_kamar_rpa = ArrayAdapter::format("money", "0");
			else
				$total_sewa_kamar_rpa = ArrayAdapter::format("money", $total_sewa_kamar_rpa);
			
			//pdr (konsultasi dokter dan konsul dokter) rpa: RPA Argopuro, RPA Bromo, RPA Bromo Baru, RPA Kelud
			$total_pdr_rpa = 0;
			$id_dokter_pdr_rpa = "-";
			$kode_dokter_pdr_rpa = "-";
			$nama_dokter_pdr_rpa = "-";
			$harga_dokter_pdr_rpa = "";
			$dbtable = new DBTable($db, "smis_rwt_konsultasi_dokter_rpa_argopuro");
			$rows = $dbtable->get_result("
				SELECT id_dokter, nama_dokter, SUM(harga) AS 'harga'
				FROM (
					SELECT id_dokter, nama_dokter, harga 
					FROM smis_rwt_konsultasi_dokter_rpa_argopuro 
					WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop NOT LIKE 'del'
					UNION ALL
					SELECT id_dokter, nama_dokter, harga 
					FROM smis_rwt_konsul_dokter_rpa_argopuro 
					WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop NOT LIKE 'del'
					
					UNION ALL
					
					SELECT id_dokter, nama_dokter, harga 
					FROM smis_rwt_konsultasi_dokter_rpa_bromo 
					WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop NOT LIKE 'del'
					UNION ALL
					SELECT id_dokter, nama_dokter, harga 
					FROM smis_rwt_konsul_dokter_rpa_bromo 
					WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop NOT LIKE 'del'
					
					UNION ALL
					
					SELECT id_dokter, nama_dokter, harga 
					FROM smis_rwt_konsultasi_dokter_rpa_bromobaru 
					WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop NOT LIKE 'del'
					UNION ALL
					SELECT id_dokter, nama_dokter, harga 
					FROM smis_rwt_konsul_dokter_rpa_bromobaru
					WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop NOT LIKE 'del'
					
					UNION ALL
					
					SELECT id_dokter, nama_dokter, harga 
					FROM smis_rwt_konsultasi_dokter_rpa_kelud
					WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop NOT LIKE 'del'
					UNION ALL
					SELECT id_dokter, nama_dokter, harga 
					FROM smis_rwt_konsul_dokter_rpa_kelud
					WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop NOT LIKE 'del'
				) v
				GROUP BY id_dokter
			");
			if ($rows != null) {
				$id_dokter_pdr_rpa = "";
				$kode_dokter_pdr_rpa = "";
				$nama_dokter_pdr_rpa = "";
				$harga_dokter_pdr_rpa = "";
				foreach ($rows as $row) {
					$total_pdr_rpa += $row->harga;
					$id_dokter_pdr_rpa .= $row->id_dokter . ";";
					$params = array();
					$params['id_karyawan'] = $row->id_dokter;
					$service = new ServiceConsumer($db, "get_kode", $params, "hrd");
					$service->setMode(ServiceConsumer::$CLEAN_BOTH);
					$content = $service->execute()->getContent();
					if ($content[0] != "")
						$kode_dokter_pdr_rpa .= $content[0] . ";";
					else
						$kode_dokter_pdr_rpa .= "-;";
					$nama_dokter_pdr_rpa .= $row->nama_dokter . ";";
					$harga_dokter_pdr_rpa .= $row->harga . ";";
				}
				$id_dokter_pdr_rpa = rtrim($id_dokter_pdr_rpa, ";");
				$kode_dokter_pdr_rpa = rtrim($kode_dokter_pdr_rpa, ";");
				$nama_dokter_pdr_rpa = rtrim($nama_dokter_pdr_rpa, ";");
				$harga_dokter_pdr_rpa = rtrim($harga_dokter_pdr_rpa, ";");
			}
			$total += $total_pdr_rpa;
			if ($total_pdr_rpa == 0)
				$total_pdr_rpa = ArrayAdapter::format("money", "0");
			else
				$total_pdr_rpa = ArrayAdapter::format("money", $total_pdr_rpa);
			
			//jasa medis (tindakan keperawatan non ecg dan non lavement) rpa: RPA Argopuro, RPA Bromo, RPA Bromo Baru, RPA Kelud
			$total_jasa_medis_rpa = 0;
			$dbtable = new DBTable($db, "smis_rwt_tindakan_perawat_rpa_argopuro");
			$rows = $dbtable->get_result("
				SELECT SUM(harga) AS 'harga'
				FROM (
					SELECT harga_tindakan AS 'harga'
					FROM smis_rwt_tindakan_perawat_rpa_argopuro
					WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop NOT LIKE 'del' AND nama_tindakan NOT LIKE '%E.C.G%' AND nama_tindakan NOT LIKE '%Lavement%'
					UNION ALL
					SELECT harga_tindakan AS 'harga'
					FROM smis_rwt_tindakan_perawat_rpa_bromo
					WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop NOT LIKE 'del' AND nama_tindakan NOT LIKE '%E.C.G%' AND nama_tindakan NOT LIKE '%Lavement%'
					UNION ALL
					SELECT harga_tindakan AS 'harga'
					FROM smis_rwt_tindakan_perawat_rpa_bromobaru
					WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop NOT LIKE 'del' AND nama_tindakan NOT LIKE '%E.C.G%' AND nama_tindakan NOT LIKE '%Lavement%'
					UNION ALL
					SELECT harga_tindakan AS 'harga'
					FROM smis_rwt_tindakan_perawat_rpa_kelud
					WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop NOT LIKE 'del' AND nama_tindakan NOT LIKE '%E.C.G%' AND nama_tindakan NOT LIKE '%Lavement%'
				) v
			");
			if ($rows != null) {
				foreach ($rows as $row) {
					$total_jasa_medis_rpa += $row->harga;
				}
			}
			$total += $total_jasa_medis_rpa;
			if ($total_jasa_medis_rpa == 0)
				$total_jasa_medis_rpa = ArrayAdapter::format("money", "0");
			else
				$total_jasa_medis_rpa = ArrayAdapter::format("money", $total_jasa_medis_rpa);
			
			//alok rpa: RPA Argopuro, RPA Bromo, RPA Bromo Baru, RPA Kelud
			$total_alok_rpa = 0;
			$dbtable = new DBTable($db, "smis_rwt_alok_rpa_argopuro");
			$rows = $dbtable->get_result("
				SELECT SUM(harga) AS 'harga'
				FROM (
					SELECT harga
					FROM smis_rwt_alok_rpa_argopuro
					WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop NOT LIKE 'del'
					UNION ALL
					SELECT harga
					FROM smis_rwt_alok_rpa_bromo
					WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop NOT LIKE 'del'
					UNION ALL
					SELECT harga
					FROM smis_rwt_alok_rpa_bromobaru
					WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop NOT LIKE 'del'
					UNION ALL
					SELECT harga
					FROM smis_rwt_alok_rpa_kelud
					WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop NOT LIKE 'del'
				) v
			");
			if ($rows != null) {
				foreach ($rows as $row) {
					$total_alok_rpa += $row->harga;
				}
			}
			$total += $total_alok_rpa;
			if ($total_alok_rpa == 0)
				$total_alok_rpa = ArrayAdapter::format("money", "0");
			else
				$total_alok_rpa = ArrayAdapter::format("money", $total_alok_rpa);
			
			//ecg (tindakan keperawatan %e.c.g.% rpa: RPA Argopuro, RPA Bromo, RPA Bromo Baru, RPA Kelud
			$total_ecg_rpa = 0;
			$dbtable = new DBTable($db, "smis_rwt_tindakan_perawat_rpa_argopuro");
			$rows = $dbtable->get_result("
				SELECT SUM(harga) AS 'harga'
				FROM (
					SELECT harga_tindakan AS 'harga'
					FROM smis_rwt_tindakan_perawat_rpa_argopuro
					WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop NOT LIKE 'del' AND nama_tindakan LIKE '%E.C.G%'
					UNION ALL
					SELECT harga_tindakan AS 'harga'
					FROM smis_rwt_tindakan_perawat_rpa_bromo
					WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop NOT LIKE 'del' AND nama_tindakan LIKE '%E.C.G%'
					UNION ALL
					SELECT harga_tindakan AS 'harga'
					FROM smis_rwt_tindakan_perawat_rpa_bromobaru
					WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop NOT LIKE 'del' AND nama_tindakan LIKE '%E.C.G%'
					UNION ALL
					SELECT harga_tindakan AS 'harga'
					FROM smis_rwt_tindakan_perawat_rpa_kelud
					WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop NOT LIKE 'del' AND nama_tindakan LIKE '%E.C.G%'
				) v
			");
			if ($rows != null) {
				foreach ($rows as $row) {
					$total_ecg_rpa += $row->harga;
				}
			}
			$total += $total_ecg_rpa;
			if ($total_ecg_rpa == 0)
				$total_ecg_rpa = ArrayAdapter::format("money", "0");
			else
				$total_ecg_rpa = ArrayAdapter::format("money", $total_ecg_rpa);
			
			//lavement (tindakan keperawatan %lavement% rpa: RPA Argopuro, RPA Bromo, RPA Bromo Baru, RPA Kelud
			$total_lavement_rpa = 0;
			$dbtable = new DBTable($db, "smis_rwt_tindakan_perawat_rpa_argopuro");
			$rows = $dbtable->get_result("
				SELECT SUM(harga) AS 'harga'
				FROM (
					SELECT harga_tindakan AS 'harga'
					FROM smis_rwt_tindakan_perawat_rpa_argopuro
					WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop NOT LIKE 'del' AND nama_tindakan LIKE '%Lavement%'
					UNION ALL
					SELECT harga_tindakan AS 'harga'
					FROM smis_rwt_tindakan_perawat_rpa_bromo
					WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop NOT LIKE 'del' AND nama_tindakan LIKE '%Lavement%'
					UNION ALL
					SELECT harga_tindakan AS 'harga'
					FROM smis_rwt_tindakan_perawat_rpa_bromobaru
					WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop NOT LIKE 'del' AND nama_tindakan LIKE '%Lavement%'
					UNION ALL
					SELECT harga_tindakan AS 'harga'
					FROM smis_rwt_tindakan_perawat_rpa_kelud
					WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop NOT LIKE 'del' AND nama_tindakan LIKE '%Lavement%'
				) v
			");
			if ($rows != null) {
				foreach ($rows as $row) {
					$total_lavement_rpa += $row->harga;
				}
			}
			$total += $total_lavement_rpa;
			if ($total_lavement_rpa == 0)
				$total_lavement_rpa = ArrayAdapter::format("money", "0");
			else
				$total_lavement_rpa = ArrayAdapter::format("money", $total_lavement_rpa);
			
			//lain-lain rpa: RPA Argopuro, RPA Bromo, RPA Bromo Baru, RPA Kelud
			$total_lain_rpa = ArrayAdapter::format("money", "0");
			
			//sewa kamar rpb: RPB Argopuro, RPB Ijen, RPB Raung
			$total_sewa_kamar_rpb = 0;
			$dbtable = new DBTable($db, "smis_rwt_bed_rpb_argopuro");
			$rows = $dbtable->get_result("
				SELECT waktu_masuk, waktu_keluar 
				FROM smis_rwt_bed_rpb_argopuro 
				WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop NOT LIKE 'del'
			");
			if ($rows != null) {
				foreach ($rows as $row) {
					$start_date = new DateTime($row->waktu_masuk);
					$end_date = new DateTime($row->waktu_keluar);
					$interval = $start_date->diff($end_date);
					$los = $interval->days + 1;
					$harga_bed = getSettings($db, "smis-rs-uri-bed-rpb_argopuro", 0);
					$total_sewa_kamar_rpb += $los * $harga_bed;
				}
			}
			$dbtable = new DBTable($db, "smis_rwt_bed_rpb_ijen");
			$rows = $dbtable->get_result("
				SELECT waktu_masuk, waktu_keluar 
				FROM smis_rwt_bed_rpb_ijen 
				WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop NOT LIKE 'del'
			");
			if ($rows != null) {
				foreach ($rows as $row) {
					$start_date = new DateTime($row->waktu_masuk);
					$end_date = new DateTime($row->waktu_keluar);
					$interval = $start_date->diff($end_date);
					$los = $interval->days + 1;
					$harga_bed = getSettings($db, "smis-rs-uri-bed-rpb_ijen", 0);
					$total_sewa_kamar_rpb += $los * $harga_bed;
				}
			}
			$dbtable = new DBTable($db, "smis_rwt_bed_rpb_raung");
			$rows = $dbtable->get_result("
				SELECT waktu_masuk, waktu_keluar 
				FROM smis_rwt_bed_rpb_raung
				WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop NOT LIKE 'del'
			");
			if ($rows != null) {
				foreach ($rows as $row) {
					$start_date = new DateTime($row->waktu_masuk);
					$end_date = new DateTime($row->waktu_keluar);
					$interval = $start_date->diff($end_date);
					$los = $interval->days + 1;
					$harga_bed = getSettings($db, "smis-rs-uri-bed-rpb_raung", 0);
					$total_sewa_kamar_rpb += $los * $harga_bed;
				}
			}
			$total += $total_sewa_kamar_rpb;
			if ($total_sewa_kamar_rpb == 0)
				$total_sewa_kamar_rpb = ArrayAdapter::format("money", "0");
			else
				$total_sewa_kamar_rpb = ArrayAdapter::format("money", $total_sewa_kamar_rpb);
			
			//pdr (konsultasi dokter dan konsul dokter) rpb: RPB Argopuro, RPB Ijen, RPB Raung
			$total_pdr_rpb = 0;
			$id_dokter_pdr_rpb = "-";
			$kode_dokter_pdr_rpb = "-";
			$nama_dokter_pdr_rpb = "-";
			$harga_dokter_pdr_rpb = "";
			$dbtable = new DBTable($db, "smis_rwt_konsultasi_dokter_rpb_argopuro");
			$rows = $dbtable->get_result("
				SELECT id_dokter, nama_dokter, SUM(harga) AS 'harga'
				FROM (
					SELECT id_dokter, nama_dokter, harga 
					FROM smis_rwt_konsultasi_dokter_rpb_argopuro 
					WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop NOT LIKE 'del'
					UNION ALL
					SELECT id_dokter, nama_dokter, harga 
					FROM smis_rwt_konsul_dokter_rpb_argopuro 
					WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop NOT LIKE 'del'
					
					UNION ALL
					
					SELECT id_dokter, nama_dokter, harga 
					FROM smis_rwt_konsultasi_dokter_rpb_ijen
					WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop NOT LIKE 'del'
					UNION ALL
					SELECT id_dokter, nama_dokter, harga 
					FROM smis_rwt_konsul_dokter_rpb_ijen
					WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop NOT LIKE 'del'
					
					UNION ALL
					
					SELECT id_dokter, nama_dokter, harga 
					FROM smis_rwt_konsultasi_dokter_rpb_raung
					WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop NOT LIKE 'del'
					UNION ALL
					SELECT id_dokter, nama_dokter, harga 
					FROM smis_rwt_konsul_dokter_rpb_raung
					WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop NOT LIKE 'del'
				) v
				GROUP BY id_dokter
			");
			if ($rows != null) {
				$id_dokter_pdr_rpb = "";
				$kode_dokter_pdr_rpb = "";
				$nama_dokter_pdr_rpb = "";
				$harga_dokter_pdr_rpb = "";
				foreach ($rows as $row) {
					$total_pdr_rpb += $row->harga;
					$id_dokter_pdr_rpb .= $row->id_dokter . ";";
					$params = array();
					$params['id_karyawan'] = $row->id_dokter;
					$service = new ServiceConsumer($db, "get_kode", $params, "hrd");
					$service->setMode(ServiceConsumer::$CLEAN_BOTH);
					$content = $service->execute()->getContent();
					if ($content[0] != "")
						$kode_dokter_pdr_rpb .= $content[0] . ";";
					else
						$kode_dokter_pdr_rpb .= "-;";
					$nama_dokter_pdr_rpb .= $row->nama_dokter . ";";
					$harga_dokter_pdr_rpb .= $row->harga . ";";
				}
				$id_dokter_pdr_rpb = rtrim($id_dokter_pdr_rpb, ";");
				$kode_dokter_pdr_rpb = rtrim($kode_dokter_pdr_rpb, ";");
				$nama_dokter_pdr_rpb = rtrim($nama_dokter_pdr_rpb, ";");
				$harga_dokter_pdr_rpb = rtrim($harga_dokter_pdr_rpb, ";");
			}
			$total += $total_pdr_rpb;
			if ($total_pdr_rpb == 0)
				$total_pdr_rpb = ArrayAdapter::format("money", "0");
			else
				$total_pdr_rpb = ArrayAdapter::format("money", $total_pdr_rpb);
			
			//jasa medis (tindakan keperawatan non ecg dan non lavement) rpb: RPB Argopuro, RPB Ijen, RPB Raung
			$total_jasa_medis_rpb = 0;
			$dbtable = new DBTable($db, "smis_rwt_tindakan_perawat_rpb_argopuro");
			$rows = $dbtable->get_result("
				SELECT SUM(harga) AS 'harga'
				FROM (
					SELECT harga_tindakan AS 'harga'
					FROM smis_rwt_tindakan_perawat_rpb_argopuro
					WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop NOT LIKE 'del' AND nama_tindakan NOT LIKE '%E.C.G%' AND nama_tindakan NOT LIKE '%Lavement%'
					UNION ALL
					SELECT harga_tindakan AS 'harga'
					FROM smis_rwt_tindakan_perawat_rpb_ijen
					WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop NOT LIKE 'del' AND nama_tindakan NOT LIKE '%E.C.G%' AND nama_tindakan NOT LIKE '%Lavement%'
					UNION ALL
					SELECT harga_tindakan AS 'harga'
					FROM smis_rwt_tindakan_perawat_rpb_raung
					WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop NOT LIKE 'del' AND nama_tindakan NOT LIKE '%E.C.G%' AND nama_tindakan NOT LIKE '%Lavement%'
				) v
			");
			if ($rows != null) {
				foreach ($rows as $row) {
					$total_jasa_medis_rpb += $row->harga;
				}
			}
			$total += $total_jasa_medis_rpb;
			if ($total_jasa_medis_rpb == 0)
				$total_jasa_medis_rpb = ArrayAdapter::format("money", "0");
			else
				$total_jasa_medis_rpb = ArrayAdapter::format("money", $total_jasa_medis_rpb);
			
			//alok rpb: RPB Argopuro, RPB Ijen, RPB Raung
			$total_alok_rpb = 0;
			$dbtable = new DBTable($db, "smis_rwt_alok_rpb_argopuro");
			$rows = $dbtable->get_result("
				SELECT SUM(harga) AS 'harga'
				FROM (
					SELECT harga
					FROM smis_rwt_alok_rpb_argopuro
					WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop NOT LIKE 'del'
					UNION ALL
					SELECT harga
					FROM smis_rwt_alok_rpb_ijen
					WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop NOT LIKE 'del'
					UNION ALL
					SELECT harga
					FROM smis_rwt_alok_rpb_raung
					WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop NOT LIKE 'del'
				) v
			");
			if ($rows != null) {
				foreach ($rows as $row) {
					$total_alok_rpb += $row->harga;
				}
			}
			$total += $total_alok_rpb;
			if ($total_alok_rpb == 0)
				$total_alok_rpb = ArrayAdapter::format("money", "0");
			else
				$total_alok_rpb = ArrayAdapter::format("money", $total_alok_rpb);
			
			//ecg (tindakan keperawatan "%e.c.g%") rpb: RPB Argopuro, RPB Ijen, RPB Raung
			$total_ecg_rpb = 0;
			$dbtable = new DBTable($db, "smis_rwt_tindakan_perawat_rpb_argopuro");
			$rows = $dbtable->get_result("
				SELECT SUM(harga) AS 'harga'
				FROM (
					SELECT harga_tindakan AS 'harga'
					FROM smis_rwt_tindakan_perawat_rpb_argopuro
					WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop NOT LIKE 'del' AND nama_tindakan LIKE '%E.C.G%'
					UNION ALL
					SELECT harga_tindakan AS 'harga'
					FROM smis_rwt_tindakan_perawat_rpb_ijen
					WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop NOT LIKE 'del' AND nama_tindakan LIKE '%E.C.G%'
					UNION ALL
					SELECT harga_tindakan AS 'harga'
					FROM smis_rwt_tindakan_perawat_rpb_raung
					WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop NOT LIKE 'del' AND nama_tindakan LIKE '%E.C.G%'
				) v
			");
			if ($rows != null) {
				foreach ($rows as $row) {
					$total_ecg_rpb += $row->harga;
				}
			}
			$total += $total_ecg_rpb;
			if ($total_ecg_rpb == 0)
				$total_ecg_rpb = ArrayAdapter::format("money", "0");
			else
				$total_ecg_rpb = ArrayAdapter::format("money", $total_ecg_rpb);
			
			//lavement (tindakan keperawatan "%lavement%") rpb: RPB Argopuro, RPB Ijen, RPB Raung
			$total_lavement_rpb = 0;
			$dbtable = new DBTable($db, "smis_rwt_tindakan_perawat_rpb_argopuro");
			$rows = $dbtable->get_result("
				SELECT SUM(harga) AS 'harga'
				FROM (
					SELECT harga_tindakan AS 'harga'
					FROM smis_rwt_tindakan_perawat_rpb_argopuro
					WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop NOT LIKE 'del' AND nama_tindakan LIKE '%Lavement%'
					UNION ALL
					SELECT harga_tindakan AS 'harga'
					FROM smis_rwt_tindakan_perawat_rpb_ijen
					WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop NOT LIKE 'del' AND nama_tindakan LIKE '%Lavement%'
					UNION ALL
					SELECT harga_tindakan AS 'harga'
					FROM smis_rwt_tindakan_perawat_rpb_raung
					WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop NOT LIKE 'del' AND nama_tindakan LIKE '%Lavement%'
				) v
			");
			if ($rows != null) {
				foreach ($rows as $row) {
					$total_lavement_rpb += $row->harga;
				}
			}
			$total += $total_lavement_rpb;
			if ($total_lavement_rpb == 0)
				$total_lavement_rpb = ArrayAdapter::format("money", "0");
			else
				$total_lavement_rpb = ArrayAdapter::format("money", $total_lavement_rpb);
			
			//lain-lain rpb: RPB Argopuro, RPB Ijen, RPB Raung
			$total_lain_rpb = ArrayAdapter::format("money", "0");
			
			//sewa kamar icu:
			$total_sewa_kamar_icu = 0;
			$dbtable = new DBTable($db, "smis_rwt_bed_icu");
			$rows = $dbtable->get_result("
				SELECT waktu_masuk, waktu_keluar 
				FROM smis_rwt_bed_icu 
				WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop NOT LIKE 'del'
			");
			if ($rows != null) {
				foreach ($rows as $row) {					
					$start_date = new DateTime($row->waktu_masuk);
					$end_date = new DateTime($row->waktu_keluar);
					$interval = $start_date->diff($end_date);
					$los = $interval->days + 1;
					$harga_bed = getSettings($db, "smis-rs-uri-bed-icu", 0);
					$total_sewa_kamar_icu += $los * $harga_bed;
				}
			}
			$total += $total_sewa_kamar_icu;
			if ($total_sewa_kamar_icu == 0)
				$total_sewa_kamar_icu = ArrayAdapter::format("money", "0");
			else
				$total_sewa_kamar_icu = ArrayAdapter::format("money", $total_sewa_kamar_icu);
			
			//pdr (konsultasi dokter dan konsul dokter) icu:
			$total_pdr_icu = 0;
			$id_dokter_pdr_icu = "-";
			$kode_dokter_pdr_icu = "-";
			$nama_dokter_pdr_icu = "-";
			$harga_dokter_pdr_icu = "";
			$dbtable = new DBTable($db, "smis_rwt_konsultasi_dokter_icu");
			$rows = $dbtable->get_result("
				SELECT id_dokter, nama_dokter, SUM(harga) AS 'harga'
				FROM (
					SELECT id_dokter, nama_dokter, harga 
					FROM smis_rwt_konsultasi_dokter_icu 
					WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop NOT LIKE 'del'
					UNION ALL
					SELECT id_dokter, nama_dokter, harga 
					FROM smis_rwt_konsul_dokter_icu
					WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop NOT LIKE 'del'
				) v
				GROUP BY id_dokter
			");
			if ($rows != null) {
				$id_dokter_pdr_icu = "";
				$kode_dokter_pdr_icu = "";
				$nama_dokter_pdr_icu = "";
				$harga_dokter_pdr_icu = "";
				foreach ($rows as $row) {
					$total_pdr_icu += $row->harga;
					$id_dokter_pdr_icu .= $row->id_dokter . ";";
					$params = array();
					$params['id_karyawan'] = $row->id_dokter;
					$service = new ServiceConsumer($db, "get_kode", $params, "hrd");
					$service->setMode(ServiceConsumer::$CLEAN_BOTH);
					$content = $service->execute()->getContent();
					if ($content[0] != "")
						$kode_dokter_pdr_icu .= $content[0] . ";";
					else
						$kode_dokter_pdr_icu .= "-;";
					$nama_dokter_pdr_icu .= $row->nama_dokter . ";";
					$harga_dokter_pdr_icu .= $row->harga . ";";
				}
				$id_dokter_pdr_icu = rtrim($id_dokter_pdr_icu, ";");
				$kode_dokter_pdr_icu = rtrim($kode_dokter_pdr_icu, ";");
				$nama_dokter_pdr_icu = rtrim($nama_dokter_pdr_icu, ";");
				$harga_dokter_pdr_icu = rtrim($harga_dokter_pdr_icu, ";");
			}
			$total += $total_pdr_icu;
			if ($total_pdr_icu == 0)
				$total_pdr_icu = ArrayAdapter::format("money", "0");
			else
				$total_pdr_icu = ArrayAdapter::format("money", $total_pdr_icu);
			
			//jasa medis (tindakan keperawatan non ecg) icu:
			$total_jasa_medis_icu = 0;
			$dbtable = new DBTable($db, "smis_rwt_tindakan_perawat_icu");
			$rows = $dbtable->get_result("
				SELECT SUM(harga) AS 'harga'
				FROM (
					SELECT harga_tindakan AS 'harga'
					FROM smis_rwt_tindakan_perawat_icu
					WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop NOT LIKE 'del' AND nama_tindakan NOT LIKE '%E.C.G%'
				) v
			");
			if ($rows != null) {
				foreach ($rows as $row) {
					$total_jasa_medis_icu += $row->harga;
				}
			}
			$total += $total_jasa_medis_icu;
			if ($total_jasa_medis_icu == 0)
				$total_jasa_medis_icu = ArrayAdapter::format("money", "0");
			else
				$total_jasa_medis_icu = ArrayAdapter::format("money", $total_jasa_medis_icu);
			
			//alok icu:
			$total_alok_icu = 0;
			$dbtable = new DBTable($db, "smis_rwt_alok_icu");
			$rows = $dbtable->get_result("
				SELECT SUM(harga) AS 'harga'
				FROM (
					SELECT harga
					FROM smis_rwt_alok_icu
					WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop NOT LIKE 'del'
				) v
			");
			if ($rows != null) {
				foreach ($rows as $row) {
					$total_alok_icu += $row->harga;
				}
			}
			$total += $total_alok_icu;
			if ($total_alok_icu == 0)
				$total_alok_icu = ArrayAdapter::format("money", "0");
			else
				$total_alok_icu = ArrayAdapter::format("money", $total_alok_icu);
			
			//ecg (tindakan keperawatan %e.c.g.% icu:
			$total_ecg_icu = 0;
			$dbtable = new DBTable($db, "smis_rwt_tindakan_perawat_icu");
			$rows = $dbtable->get_result("
				SELECT SUM(harga) AS 'harga'
				FROM (
					SELECT harga_tindakan AS 'harga'
					FROM smis_rwt_tindakan_perawat_icu
					WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop NOT LIKE 'del' AND nama_tindakan LIKE '%E.C.G%'
				) v
			");
			if ($rows != null) {
				foreach ($rows as $row) {
					$total_ecg_icu += $row->harga;
				}
			}
			$total += $total_ecg_icu;
			if ($total_ecg_icu == 0)
				$total_ecg_icu = ArrayAdapter::format("money", "0");
			else
				$total_ecg_icu = ArrayAdapter::format("money", $total_ecg_icu);
			
			//lain-lain icu:
			$total_lain_icu = ArrayAdapter::format("money", "0");
			
			//sewa kamar igd:
			$total_sewa_kamar_igd = 0;
			$dbtable = new DBTable($db, "smis_rwt_bed_instalasi_gawat_darurat");
			$rows = $dbtable->get_result("
				SELECT waktu_masuk, waktu_keluar 
				FROM smis_rwt_bed_instalasi_gawat_darurat 
				WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop NOT LIKE 'del'
			");
			if ($rows != null) {
				foreach ($rows as $row) {					
					$start_date = new DateTime($row->waktu_masuk);
					$end_date = new DateTime($row->waktu_keluar);
					$interval = $start_date->diff($end_date);
					$los = $interval->days + 1;
					$harga_bed = getSettings($db, "smis-rs-uri-bed-instalasi_gawat_darurat", 0);
					$total_sewa_kamar_igd += $los * $harga_bed;
				}
			}
			$total += $total_sewa_kamar_igd;
			if ($total_sewa_kamar_igd == 0)
				$total_sewa_kamar_igd = ArrayAdapter::format("money", "0");
			else
				$total_sewa_kamar_igd = ArrayAdapter::format("money", $total_sewa_kamar_igd);
			
			//pdr (konsultasi dokter dan konsul dokter) igd:
			$total_pdr_igd = 0;
			$id_dokter_pdr_igd = "-";
			$kode_dokter_pdr_igd = "-";
			$nama_dokter_pdr_igd = "-";
			$harga_dokter_pdr_igd = "-";
			$dbtable = new DBTable($db, "smis_rwt_konsultasi_dokter_instalasi_gawat_darurat");
			$rows = $dbtable->get_result("
				SELECT id_dokter, nama_dokter, SUM(harga) AS 'harga'
				FROM (
					SELECT id_dokter, nama_dokter, harga 
					FROM smis_rwt_konsultasi_dokter_instalasi_gawat_darurat 
					WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop NOT LIKE 'del'
					UNION ALL
					SELECT id_dokter, nama_dokter, harga 
					FROM smis_rwt_konsul_dokter_instalasi_gawat_darurat
					WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop NOT LIKE 'del'
				) v
				GROUP BY id_dokter
			");
			if ($rows != null) {
				$id_dokter_pdr_igd = "";
				$kode_dokter_pdr_igd = "";
				$nama_dokter_pdr_igd = "";
				$harga_dokter_pdr_igd = "";
				foreach ($rows as $row) {
					$total_pdr_igd += $row->harga;
					$id_dokter_pdr_igd .= $row->id_dokter . ";";
					$params = array();
					$params['id_karyawan'] = $row->id_dokter;
					$service = new ServiceConsumer($db, "get_kode", $params, "hrd");
					$service->setMode(ServiceConsumer::$CLEAN_BOTH);
					$content = $service->execute()->getContent();
					if ($content[0] != "")
						$kode_dokter_pdr_igd .= $content[0] . ";";
					else
						$kode_dokter_pdr_igd .= "-;";
					$nama_dokter_pdr_igd .= $row->nama_dokter . ";";
					$harga_dokter_pdr_igd .= $row->harga . ";";
				}
				$id_dokter_pdr_igd = rtrim($id_dokter_pdr_igd, ";");
				$kode_dokter_pdr_igd = rtrim($kode_dokter_pdr_igd, ";");
				$nama_dokter_pdr_igd = rtrim($nama_dokter_pdr_igd, ";");
				$harga_dokter_pdr_igd = rtrim($harga_dokter_pdr_igd, ";");
			}
			$total += $total_pdr_igd;
			if ($total_pdr_igd == 0)
				$total_pdr_igd = ArrayAdapter::format("money", "0");
			else
				$total_pdr_igd = ArrayAdapter::format("money", $total_pdr_igd);
			
			//jasa medis (tindakan keperawatan non ecg) igd:
			$total_jasa_medis_igd = 0;
			$dbtable = new DBTable($db, "smis_rwt_tindakan_perawat_instalasi_gawat_darurat");
			$rows = $dbtable->get_result("
				SELECT SUM(harga) AS 'harga'
				FROM (
					SELECT harga_tindakan AS 'harga'
					FROM smis_rwt_tindakan_perawat_instalasi_gawat_darurat
					WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop NOT LIKE 'del' AND nama_tindakan NOT LIKE '%E.C.G%'
				) v
			");
			if ($rows != null) {
				foreach ($rows as $row) {
					$total_jasa_medis_igd += $row->harga;
				}
			}
			$total += $total_jasa_medis_igd;
			if ($total_jasa_medis_igd == 0)
				$total_jasa_medis_igd = ArrayAdapter::format("money", "0");
			else
				$total_jasa_medis_igd = ArrayAdapter::format("money", $total_jasa_medis_igd);
			
			//alok igd:
			$total_alok_igd = 0;
			$dbtable = new DBTable($db, "smis_rwt_alok_igd");
			$rows = $dbtable->get_result("
				SELECT SUM(harga) AS 'harga'
				FROM (
					SELECT harga
					FROM smis_rwt_alok_instalasi_gawat_darurat
					WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop NOT LIKE 'del'
				) v
			");
			if ($rows != null) {
				foreach ($rows as $row) {
					$total_alok_igd += $row->harga;
				}
			}
			$total += $total_alok_igd;
			if ($total_alok_igd == 0)
				$total_alok_igd = ArrayAdapter::format("money", "0");
			else
				$total_alok_igd = ArrayAdapter::format("money", $total_alok_igd);
			
			//ecg (tindakan keperawatan %e.c.g.% igd:
			$total_ecg_igd = 0;
			$dbtable = new DBTable($db, "smis_rwt_tindakan_perawat_instalasi_gawat_darurat");
			$rows = $dbtable->get_result("
				SELECT SUM(harga) AS 'harga'
				FROM (
					SELECT harga_tindakan AS 'harga'
					FROM smis_rwt_tindakan_perawat_instalasi_gawat_darurat
					WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop NOT LIKE 'del' AND nama_tindakan LIKE '%E.C.G%'
				) v
			");
			if ($rows != null) {
				foreach ($rows as $row) {
					$total_ecg_igd += $row->harga;
				}
			}
			$total += $total_ecg_igd;
			if ($total_ecg_igd == 0)
				$total_ecg_igd = ArrayAdapter::format("money", "0");
			else
				$total_ecg_igd = ArrayAdapter::format("money", $total_ecg_igd);
			
			//sewa kamar ok:
			$total_sewa_kamar_ok = 0;
			$dbtable = new DBTable($db, "smis_rwt_bed_kamar_operasi");
			$rows = $dbtable->get_result("
				SELECT waktu_masuk, waktu_keluar 
				FROM smis_rwt_bed_kamar_operasi 
				WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop NOT LIKE 'del'
			");
			if ($rows != null) {
				foreach ($rows as $row) {					
					$start_date = new DateTime($row->waktu_masuk);
					$end_date = new DateTime($row->waktu_keluar);
					$interval = $start_date->diff($end_date);
					$los = $interval->days + 1;
					$harga_bed = getSettings($db, "smis-rs-uri-bed-kamar_operasi", 0);
					$total_sewa_kamar_ok += $los * $harga_bed;
				}
			}
			$total += $total_sewa_kamar_ok;
			if ($total_sewa_kamar_ok == 0)
				$total_sewa_kamar_ok = ArrayAdapter::format("money", "0");
			else
				$total_sewa_kamar_ok = ArrayAdapter::format("money", $total_sewa_kamar_ok);
			
			//pdr (operator I, operator II, anestesi) dan anestesi (asisten operator, asisten anestesi, asisten instrumentator, bidan, dan asisten omloop) ok:
			$total_pdr_ok = 0;
			$total_anestesi_ok = 0;
			$id_operator_1_ok = "-";
			$kode_operator_1_ok = "-";
			$nama_operator_1_ok = "-";
			$harga_operator_1_ok = 0;
			$h_operator_1_ok = "";
			$id_operator_2_ok = "-";
			$kode_operator_2_ok = "-";
			$nama_operator_2_ok = "-";
			$harga_operator_2_ok = 0;
			$h_operator_2_ok = "";
			$id_anestesi_ok = "-";
			$kode_anestesi_ok = "-";
			$nama_anestesi_ok = "-";
			$harga_anestesi_ok = 0;
			$h_anestesi_ok = "";
			$id_asop_1_ok = "-";
			$kode_asop_1_ok = "-";
			$nama_asop_1_ok = "-";
			$harga_asop_1_ok = 0;
			$h_asop_1_ok = "";
			$id_asop_2_ok = "-";
			$kode_asop_2_ok = "-";
			$nama_asop_2_ok = "-";
			$harga_asop_2_ok = 0;
			$h_asop_2_ok = "";
			$id_asnes_1_ok = "-";
			$kode_asnes_1_ok = "-";
			$nama_asnes_1_ok = "-";
			$harga_asnes_1_ok = 0;
			$h_asnes_1_ok = "";
			$id_asnes_2_ok = "-";
			$kode_asnes_2_ok = "-";
			$nama_asnes_2_ok = "-";
			$harga_asnes_2_ok = 0;
			$h_asnes_2_ok = "";
			$id_asinst_1_ok = "-";
			$kode_asinst_1_ok = "-";
			$nama_asinst_1_ok = "-";
			$harga_asinst_1_ok = 0;
			$h_asinst_1_ok = "";
			$id_asinst_2_ok = "-";
			$kode_asinst_2_ok = "-";
			$nama_asinst_2_ok = "-";
			$harga_asinst_2_ok = 0;
			$h_asinst_2_ok = "";
			$id_asomlo_1_ok = "-";
			$kode_asomlo_1_ok = "-";
			$nama_asomlo_1_ok = "-";
			$harga_asomlo_1_ok = 0;
			$h_asomlo_1_ok = "";
			$id_asomlo_2_ok = "-";
			$kode_asomlo_2_ok = "-";
			$nama_asomlo_2_ok = "-";
			$harga_asomlo_2_ok = 0;
			$h_asomlo_2_ok = "";
			$id_bidan_1_ok = "-";
			$kode_bidan_1_ok = "-";
			$nama_bidan_1_ok = "-";
			$harga_bidan_1_ok = 0;
			$h_bidan_1_ok = "";
			$id_bidan_2_ok = "-";
			$kode_bidan_2_ok = "-";
			$nama_bidan_2_ok = "-";
			$harga_bidan_2_ok = 0;
			$h_bidan_2_ok = "";
			$dbtable = new DBTable($db, "smis_rwt_ok_kamar_operasi");
			$rows = $dbtable->get_result("
				SELECT id_operator_satu, nama_operator_satu, harga_operator_satu, 
				       id_operator_dua, nama_operator_dua, harga_operator_dua, 
					   id_anastesi, nama_anastesi, harga_anastesi, 
					   id_asisten_anastesi, nama_asisten_anastesi, harga_asisten_anastesi, 
					   id_asisten_anastesi_dua, nama_asisten_anastesi_dua, harga_asisten_anastesi_dua,
					   id_instrument, nama_instrument, harga_instrument, 
				       id_oomloop_satu, nama_oomloop_satu, harga_oomloop_satu, 
					   id_oomloop_dua, nama_oomloop_dua, harga_oomloop_dua
				FROM smis_rwt_ok_kamar_operasi 
				WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop NOT LIKE 'del'
			");
			if ($rows != null) {
				$id_operator_1_ok = "";
				$kode_operator_1_ok = "";
				$nama_operator_1_ok = "";
				$id_operator_2_ok = "";
				$kode_operator_2_ok = "";
				$nama_operator_2_ok = "";
				$id_anestesi_ok = "";
				$kode_anestesi_ok = "";
				$nama_anestesi_ok = "";
				$id_asop_1_ok = "";
				$kode_asop_1_ok = "";
				$nama_asop_1_ok = "";
				$id_asop_2_ok = "";
				$kode_asop_2_ok = "";
				$nama_asop_2_ok = "";
				$id_asnes_1_ok = "";
				$kode_asnes_1_ok = "";
				$nama_asnes_1_ok = "";
				$id_asnes_2_ok = "";
				$kode_asnes_2_ok = "";
				$nama_asnes_2_ok = "";
				$id_asinst_1_ok = "";
				$kode_asinst_1_ok = "";
				$nama_asinst_1_ok = "";
				$id_asinst_2_ok = "";
				$kode_asinst_2_ok = "";
				$nama_asinst_2_ok = "";
				$id_asomlo_1_ok = "";
				$kode_asomlo_1_ok = "";
				$nama_asomlo_1_ok = "";
				$id_asomlo_2_ok = "";
				$kode_asomlo_2_ok = "";
				$nama_asomlo_2_ok = "";
				foreach ($rows as $row) {
					$harga_operator_1_ok = $row->harga_operator_satu;
					$h_operator_1_ok .= $row->harga_operator_satu . ";";
					$total_pdr_ok += $row->harga_operator_satu;
					$harga_operator_2_ok = $row->harga_operator_dua;
					$h_operator_2_ok .= $row->harga_operator_dua . ";";
					$total_pdr_ok += $row->harga_operator_dua;
					$harga_anestesi_ok = $row->harga_anastesi;
					$total_pdr_ok += $row->harga_anastesi;
					$harga_asop_1_ok += $row->harga_asisten_operator_satu;
					$h_asop_1_ok .= $row->harga_asisten_operator_satu . ";";
					$total_anestesi_ok += $row->harga_asisten_operator_satu;
					$harga_asop_2_ok += $row->harga_asisten_operator_dua;
					$h_asop_2_ok .= $row->harga_asisten_operator_dua . ";";
					$total_anestesi_ok += $row->harga_asisten_operator_dua;
					$harga_asnes_1_ok += $row->harga_asisten_anastesi;
					$h_asnes_1_ok .= $row->harga_asisten_anastesi . ";";
					$total_anestesi_ok += $row->harga_asisten_anastesi;
					$harga_asnes_2_ok += $row->harga_asisten_anastesi_dua;
					$h_asnes_2_ok .= $row->harga_asisten_anastesi_dua . ";";
					$total_anestesi_ok += $row->harga_asisten_anastesi_dua;
					$harga_asinst_1_ok += $row->harga_instrument;
					$h_asinst_1_ok .= $row->harga_instrument . ";";
					$total_anestesi_ok += $row->harga_instrument;
					$harga_asinst_2_ok += $row->harga_instrument_dua;
					$h_asinst_2_ok .= $row->harga_instrument_dua . ";";
					$total_anestesi_ok += $row->harga_instrument_dua;
					$harga_asomlo_1_ok += $row->harga_oomloop_satu;
					$h_asomlo_1_ok .= $row->harga_oomloop_satu . ";";
					$total_anestesi_ok += $row->harga_oomloop_satu;
					$harga_asomlo_2_ok += $row->harga_oomloop_dua;
					$h_asomlo_2_ok .= $row->harga_oomloop_dua . ";";
					$total_anestesi_ok += $row->harga_oomloop_dua;
					$harga_bidan_1_ok += $row->harga_bidan;
					$h_bidan_1_ok .= $row->harga_bidan . ";";
					$total_anestesi_ok += $row->harga_bidan;
					$harga_bidan_2_ok += $row->harga_bidan_dua;
					$h_bidan_2_ok .= $row->harga_bidan_dua . ";";
					$total_anestesi_ok += $row->harga_bidan_dua;
					$id_operator_1_ok .= $row->id_operator_satu . ";";
					$id_operator_2_ok .= $row->id_operator_dua . ";";
					$id_anestesi_ok .= $row->id_anastesi . ";";
					$id_asop_1_ok .= $row->id_asisten_operator_satu . ";";
					$id_asop_2_ok .= $row->id_asisten_operator_dua . ";";
					$id_asnes_1_ok .= $row->id_asisten_anastesi . ";";
					$id_asnes_2_ok .= $row->id_asisten_anastesi_dua . ";";
					$id_asinst_1_ok .= $row->id_instrument . ";";
					$id_asinst_2_ok .= $row->id_instrument_dua . ";";
					$id_asomlo_1_ok .= $row->id_oomloop_satu . ";";
					$id_asomlo_2_ok .= $row->id_oomloop_dua . ";";
					$id_bidan_1_ok .= $row->id_bidan . ";";
					$id_bidan_2_ok .= $row->id_bidan_dua . ";";
					$params = array();
					$params['id_karyawan'] = $row->id_operator_satu;
					$service = new ServiceConsumer($db, "get_kode", $params, "hrd");
					$service->setMode(ServiceConsumer::$CLEAN_BOTH);
					$content = $service->execute()->getContent();
					if ($content[0] != "")
						$kode_operator_1_ok .= $content[0] . ";";
					else
						$kode_operator_1_ok .= "-;";
					$params = array();
					$params['id_karyawan'] = $row->id_operator_dua;
					$service = new ServiceConsumer($db, "get_kode", $params, "hrd");
					$service->setMode(ServiceConsumer::$CLEAN_BOTH);
					$content = $service->execute()->getContent();
					if ($content[0] != "")
						$kode_operator_2_ok .= $content[0] . ";";
					else
						$kode_operator_2_ok .= "-;";
					$params = array();
					$params['id_karyawan'] = $row->id_anastesi;
					$service = new ServiceConsumer($db, "get_kode", $params, "hrd");
					$service->setMode(ServiceConsumer::$CLEAN_BOTH);
					$content = $service->execute()->getContent();
					if ($content[0] != "")
						$kode_anestesi_ok .= $content[0] . ";";
					else
						$kode_anestesi_ok .= "-;";
					$params = array();
					$params['id_karyawan'] = $row->id_asisten_operator_satu;
					$service = new ServiceConsumer($db, "get_kode", $params, "hrd");
					$service->setMode(ServiceConsumer::$CLEAN_BOTH);
					$content = $service->execute()->getContent();
					if ($content[0] != "")
						$kode_asop_1_ok .= $content[0] . ";";
					else
						$kode_asop_1_ok .= "-;";
					$params = array();
					$params['id_karyawan'] = $row->id_asisten_operator_dua;
					$service = new ServiceConsumer($db, "get_kode", $params, "hrd");
					$service->setMode(ServiceConsumer::$CLEAN_BOTH);
					$content = $service->execute()->getContent();
					if ($content[0] != "")
						$kode_asop_2_ok .= $content[0] . ";";
					else
						$kode_asop_2_ok .= "-;";
					$params = array();
					$params['id_karyawan'] = $row->id_asisten_anastesi;
					$service = new ServiceConsumer($db, "get_kode", $params, "hrd");
					$service->setMode(ServiceConsumer::$CLEAN_BOTH);
					$content = $service->execute()->getContent();
					if ($content[0] != "")
						$kode_asnes_1_ok .= $content[0] . ";";
					else
						$kode_asnes_1_ok .= "-;";
					$params = array();
					$params['id_karyawan'] = $row->id_asisten_anastesi_dua;
					$service = new ServiceConsumer($db, "get_kode", $params, "hrd");
					$service->setMode(ServiceConsumer::$CLEAN_BOTH);
					$content = $service->execute()->getContent();
					if ($content[0] != "")
						$kode_asnes_2_ok .= $content[0] . ";";
					else
						$kode_asnes_2_ok .= "-;";
					$params = array();
					$params['id_karyawan'] = $row->id_instrument;
					$service = new ServiceConsumer($db, "get_kode", $params, "hrd");
					$service->setMode(ServiceConsumer::$CLEAN_BOTH);
					$content = $service->execute()->getContent();
					if ($content[0] != "")
						$kode_asinst_1_ok .= $content[0] . ";";
					else
						$kode_asinst_1_ok .= "-;";
					$params = array();
					$params['id_karyawan'] = $row->id_instrument_dua;
					$service = new ServiceConsumer($db, "get_kode", $params, "hrd");
					$service->setMode(ServiceConsumer::$CLEAN_BOTH);
					$content = $service->execute()->getContent();
					if ($content[0] != "")
						$kode_asinst_2_ok .= $content[0] . ";";
					else
						$kode_asinst_2_ok .= "-;";
					$params = array();
					$params['id_karyawan'] = $row->id_oomloop_satu;
					$service = new ServiceConsumer($db, "get_kode", $params, "hrd");
					$service->setMode(ServiceConsumer::$CLEAN_BOTH);
					$content = $service->execute()->getContent();
					if ($content[0] != "")
						$kode_asomlo_1_ok .= $content[0] . ";";
					else
						$kode_asomlo_1_ok .= "-;";
					$params = array();
					$params['id_karyawan'] = $row->id_oomloop_dua;
					$service = new ServiceConsumer($db, "get_kode", $params, "hrd");
					$service->setMode(ServiceConsumer::$CLEAN_BOTH);
					$content = $service->execute()->getContent();
					if ($content[0] != "")
						$kode_asomlo_2_ok .= $content[0] . ";";
					else
						$kode_asomlo_2_ok .= "-;";
					$params = array();
					$params['id_karyawan'] = $row->id_bidan;
					$service = new ServiceConsumer($db, "get_kode", $params, "hrd");
					$service->setMode(ServiceConsumer::$CLEAN_BOTH);
					$content = $service->execute()->getContent();
					if ($content[0] != "")
						$kode_bidan_1_ok .= $content[0] . ";";
					else
						$kode_bidan_1_ok .= "-;";
					$params = array();
					$params['id_karyawan'] = $row->id_bidan_dua;
					$service = new ServiceConsumer($db, "get_kode", $params, "hrd");
					$service->setMode(ServiceConsumer::$CLEAN_BOTH);
					$content = $service->execute()->getContent();
					if ($content[0] != "")
						$kode_bidan_2_ok .= $content[0] . ";";
					else
						$kode_bidan_2_ok .= "-;";
					$nama_operator_1_ok .= $row->nama_operator_satu . ";";
					$nama_operator_2_ok .= $row->nama_operator_dua . ";";
					$nama_anestesi_ok .= $row->nama_anastesi . ";";
					$nama_asop_1_ok .= $row->nama_asisten_operator_satu . ";";
					$nama_asop_2_ok .= $row->nama_asisten_operator_dua . ";";
					$nama_asnes_1_ok .= $row->nama_asisten_anastesi . ";";
					$nama_asnes_2_ok .= $row->nama_asisten_anastesi_dua . ";";
					$nama_asinst_1_ok .= $row->nama_instrument . ";";
					$nama_asinst_2_ok .= $row->nama_instrument_dua . ";";
					$nama_asomlo_1_ok .= $row->nama_oomloop_satu . ";";
					$nama_asomlo_2_ok .= $row->nama_oomloop_dua . ";";
					$nama_bidan_1_ok .= $row->nama_bidan_satu . ";";
					$nama_bidan_2_ok .= $row->nama_bidan_dua . ";";
				}
				$id_operator_1_ok = rtrim($id_operator_1_ok, ";");
				$id_operator_2_ok = rtrim($id_operator_2_ok, ";");
				$id_anestesi_ok = rtrim($id_anestesi_ok, ";");
				$id_asop_1_ok = rtrim($id_asop_1_ok, ";");
				$id_asop_2_ok = rtrim($id_asop_2_ok, ";");
				$id_asnes_1_ok = rtrim($id_asnes_1_ok, ";");
				$id_asnes_2_ok = rtrim($id_asnes_2_ok, ";");
				$id_asinst_1_ok = rtrim($id_asinst_1_ok, ";");
				$id_asinst_2_ok = rtrim($id_asinst_2_ok, ";");
				$id_asomlo_1_ok = rtrim($id_asomlo_1_ok, ";");
				$id_asomlo_2_ok = rtrim($id_asomlo_2_ok, ";");
				$id_bidan_1_ok = rtrim($id_bidan_1_ok, ";");
				$id_bidan_2_ok = rtrim($id_bidan_2_ok, ";");
				$kode_operator_1_ok = rtrim($kode_operator_1_ok, ";");
				$kode_operator_2_ok = rtrim($kode_operator_2_ok, ";");
				$kode_anestesi_ok = rtrim($kode_anestesi_ok, ";");
				$kode_asop_1_ok = rtrim($kode_asop_1_ok, ";");
				$kode_asop_2_ok = rtrim($kode_asop_2_ok, ";");
				$kode_asnes_1_ok = rtrim($kode_asnes_1_ok, ";");
				$kode_asnes_2_ok = rtrim($kode_asnes_2_ok, ";");
				$kode_asinst_1_ok = rtrim($kode_asinst_1_ok, ";");
				$kode_asinst_2_ok = rtrim($kode_asinst_2_ok, ";");
				$kode_asomlo_1_ok = rtrim($kode_asomlo_1_ok, ";");
				$kode_asomlo_2_ok = rtrim($kode_asomlo_2_ok, ";");
				$kode_bidan_1_ok = rtrim($kode_bidan_1_ok, ";");
				$kode_bidan_2_ok = rtrim($kode_bidan_2_ok, ";");
				$nama_operator_1_ok = rtrim($nama_operator_1_ok, ";");
				$nama_operator_2_ok = rtrim($nama_operator_2_ok, ";");
				$nama_anestesi_ok = rtrim($nama_anestesi_ok, ";");
				$nama_asop_1_ok = rtrim($nama_asop_1_ok, ";");
				$nama_asop_2_ok = rtrim($nama_asop_2_ok, ";");
				$nama_asnes_1_ok = rtrim($nama_asnes_1_ok, ";");
				$nama_asnes_2_ok = rtrim($nama_asnes_2_ok, ";");
				$nama_asinst_1_ok = rtrim($nama_asinst_1_ok, ";");
				$nama_asinst_2_ok = rtrim($nama_asinst_2_ok, ";");
				$nama_asomlo_1_ok = rtrim($nama_asomlo_1_ok, ";");
				$nama_asomlo_2_ok = rtrim($nama_asomlo_2_ok, ";");
				$nama_bidan_1_ok = rtrim($nama_bidan_1_ok, ";");
				$nama_bidan_2_ok = rtrim($nama_bidan_2_ok, ";");
			}
			$total += $total_pdr_ok;
			if ($total_pdr_ok == 0)
				$total_pdr_ok = ArrayAdapter::format("money", "0");
			else
				$total_pdr_ok = ArrayAdapter::format("money", $total_pdr_ok);
			$total += $total_anestesi_ok;
			if ($total_anestesi_ok == 0)
				$total_anestesi_ok = ArrayAdapter::format("money", "0");
			else
				$total_anestesi_ok = ArrayAdapter::format("money", $total_anestesi_ok);
			
			//jasa medis (tindakan keperawatan) ok:
			$total_jasa_medis_ok = 0;
			$dbtable = new DBTable($db, "smis_rwt_tindakan_perawat_kamar_operasi");
			$rows = $dbtable->get_result("
				SELECT SUM(harga) AS 'harga'
				FROM (
					SELECT harga_tindakan AS 'harga'
					FROM smis_rwt_tindakan_perawat_kamar_operasi
					WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop NOT LIKE 'del'
				) v
			");
			if ($rows != null) {
				foreach ($rows as $row) {
					$total_jasa_medis_ok += $row->harga;
				}
			}
			$total += $total_jasa_medis_ok;
			if ($total_jasa_medis_ok == 0)
				$total_jasa_medis_ok = ArrayAdapter::format("money", "0");
			else
				$total_jasa_medis_ok = ArrayAdapter::format("money", $total_jasa_medis_ok);
			
			//materi (alok) ok:
			$total_alok_ok = 0;
			$dbtable = new DBTable($db, "smis_rwt_alok_kamar_operasi");
			$rows = $dbtable->get_result("
				SELECT SUM(harga) AS 'harga'
				FROM (
					SELECT harga
					FROM smis_rwt_alok_kamar_operasi
					WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop NOT LIKE 'del'
				) v
			");
			if ($rows != null) {
				foreach ($rows as $row) {
					$total_alok_ok += $row->harga;
				}
			}
			$total += $total_alok_ok;
			if ($total_alok_ok == 0)
				$total_alok_ok = ArrayAdapter::format("money", "0");
			else
				$total_alok_ok = ArrayAdapter::format("money", $total_alok_ok);
			
			//sewa kamar rpkk: RKK Argopuro, RKK Kelud, RKK Raung, RKK Semeru, RKK Wilis, RKK Ruang Bersalin, RKK Ruang Pathologis
			$total_sewa_kamar_rpkk = 0;
			$dbtable = new DBTable($db, "smis_rwt_bed_rpkk_argopuro");
			$rows = $dbtable->get_result("
				SELECT waktu_masuk, waktu_keluar 
				FROM smis_rwt_bed_rpkk_argopuro
				WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop NOT LIKE 'del'
			");
			if ($rows != null) {
				foreach ($rows as $row) {
					$start_date = new DateTime($row->waktu_masuk);
					$end_date = new DateTime($row->waktu_keluar);
					$interval = $start_date->diff($end_date);
					$los = $interval->days + 1;
					$harga_bed = getSettings($db, "smis-rs-uri-bed-rpkk_argopuro", 0);
					$total_sewa_kamar_rpkk += $los * $harga_bed;
				}
			}
			$dbtable = new DBTable($db, "smis_rwt_bed_rpkk_kelud");
			$rows = $dbtable->get_result("
				SELECT waktu_masuk, waktu_keluar 
				FROM smis_rwt_bed_rpkk_kelud 
				WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop NOT LIKE 'del'
			");
			if ($rows != null) {
				foreach ($rows as $row) {
					$start_date = new DateTime($row->waktu_masuk);
					$end_date = new DateTime($row->waktu_keluar);
					$interval = $start_date->diff($end_date);
					$los = $interval->days + 1;
					$harga_bed = getSettings($db, "smis-rs-uri-bed-rpkk_kelud", 0);
					$total_sewa_kamar_rpkk += $los * $harga_bed;
				}
			}
			$dbtable = new DBTable($db, "smis_rwt_bed_rpkk_raung");
			$rows = $dbtable->get_result("
				SELECT waktu_masuk, waktu_keluar 
				FROM smis_rwt_bed_rpkk_raung
				WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop NOT LIKE 'del'
			");
			if ($rows != null) {
				foreach ($rows as $row) {
					$start_date = new DateTime($row->waktu_masuk);
					$end_date = new DateTime($row->waktu_keluar);
					$interval = $start_date->diff($end_date);
					$los = $interval->days + 1;
					$harga_bed = getSettings($db, "smis-rs-uri-bed-rpkk_raung", 0);
					$total_sewa_kamar_rpkk += $los * $harga_bed;
				}
			}
			$dbtable = new DBTable($db, "smis_rwt_bed_rpkk_semeru");
			$rows = $dbtable->get_result("
				SELECT waktu_masuk, waktu_keluar 
				FROM smis_rwt_bed_rpkk_semeru
				WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop NOT LIKE 'del'
			");
			if ($rows != null) {
				foreach ($rows as $row) {
					$start_date = new DateTime($row->waktu_masuk);
					$end_date = new DateTime($row->waktu_keluar);
					$interval = $start_date->diff($end_date);
					$los = $interval->days + 1;
					$harga_bed = getSettings($db, "smis-rs-uri-bed-rpkk_semeru", 0);
					$total_sewa_kamar_rpkk += $los * $harga_bed;
				}
			}
			$dbtable = new DBTable($db, "smis_rwt_bed_rpkk_wilis");
			$rows = $dbtable->get_result("
				SELECT waktu_masuk, waktu_keluar 
				FROM smis_rwt_bed_rpkk_wilis
				WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop NOT LIKE 'del'
			");
			if ($rows != null) {
				foreach ($rows as $row) {
					$start_date = new DateTime($row->waktu_masuk);
					$end_date = new DateTime($row->waktu_keluar);
					$interval = $start_date->diff($end_date);
					$los = $interval->days + 1;
					$harga_bed = getSettings($db, "smis-rs-uri-bed-rpkk_wilis", 0);
					$total_sewa_kamar_rpkk += $los * $harga_bed;
				}
			}
			$dbtable = new DBTable($db, "smis_rwt_bed_ruang_bersalin");
			$rows = $dbtable->get_result("
				SELECT waktu_masuk, waktu_keluar 
				FROM smis_rwt_bed_ruang_bersalin
				WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop NOT LIKE 'del'
			");
			if ($rows != null) {
				foreach ($rows as $row) {
					$start_date = new DateTime($row->waktu_masuk);
					$end_date = new DateTime($row->waktu_keluar);
					$interval = $start_date->diff($end_date);
					$los = $interval->days + 1;
					$harga_bed = getSettings($db, "smis-rs-uri-bed-ruang_bersalin", 0);
					$total_sewa_kamar_rpkk += $los * $harga_bed;
				}
			}
			$dbtable = new DBTable($db, "smis_rwt_bed_ruang_pathologis");
			$rows = $dbtable->get_result("
				SELECT waktu_masuk, waktu_keluar 
				FROM smis_rwt_bed_ruang_pathologis
				WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop NOT LIKE 'del'
			");
			if ($rows != null) {
				foreach ($rows as $row) {
					$start_date = new DateTime($row->waktu_masuk);
					$end_date = new DateTime($row->waktu_keluar);
					$interval = $start_date->diff($end_date);
					$los = $interval->days + 1;
					$harga_bed = getSettings($db, "smis-rs-uri-bed-ruang_pathologis", 0);
					$total_sewa_kamar_rpkk += $los * $harga_bed;
				}
			}
			$total += $total_sewa_kamar_rpkk;
			if ($total_sewa_kamar_rpkk == 0)
				$total_sewa_kamar_rpkk = ArrayAdapter::format("money", "0");
			else
				$total_sewa_kamar_rpkk = ArrayAdapter::format("money", $total_sewa_kamar_rpkk);
			
			//pdr (konsultasi dokter dan konsul dokter) rpkk: RKK Argopuro, RKK Kelud, RKK Raung, RKK Semeru, RKK Wilis, RKK Ruang Bersalin, RKK Ruang Pathologis
			$total_pdr_rpkk = 0;
			$id_dokter_pdr_rpkk = "-";
			$kode_dokter_pdr_rpkk = "-";
			$nama_dokter_pdr_rpkk = "-";
			$harga_dokter_pdr_rpkk = "";
			$dbtable = new DBTable($db, "smis_rwt_konsultasi_dokter_rpkk_argopuro");
			$rows = $dbtable->get_result("
				SELECT id_dokter, nama_dokter, SUM(harga) AS 'harga'
				FROM (
					SELECT id_dokter, nama_dokter, harga 
					FROM smis_rwt_konsultasi_dokter_rpkk_argopuro 
					WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop NOT LIKE 'del'
					UNION ALL
					SELECT id_dokter, nama_dokter, harga 
					FROM smis_rwt_konsul_dokter_rpkk_argopuro 
					WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop NOT LIKE 'del'
					
					UNION ALL
					
					SELECT id_dokter, nama_dokter, harga 
					FROM smis_rwt_konsultasi_dokter_rpkk_kelud 
					WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop NOT LIKE 'del'
					UNION ALL
					SELECT id_dokter, nama_dokter, harga 
					FROM smis_rwt_konsul_dokter_rpkk_kelud 
					WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop NOT LIKE 'del'
					
					UNION ALL
					
					SELECT id_dokter, nama_dokter, harga 
					FROM smis_rwt_konsultasi_dokter_rpkk_raung 
					WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop NOT LIKE 'del'
					UNION ALL
					SELECT id_dokter, nama_dokter, harga 
					FROM smis_rwt_konsul_dokter_rpkk_raung 
					WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop NOT LIKE 'del'
					
					UNION ALL
					
					SELECT id_dokter, nama_dokter, harga 
					FROM smis_rwt_konsultasi_dokter_rpkk_semeru 
					WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop NOT LIKE 'del'
					UNION ALL
					SELECT id_dokter, nama_dokter, harga 
					FROM smis_rwt_konsul_dokter_rpkk_semeru 
					WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop NOT LIKE 'del'
					
					UNION ALL
					
					SELECT id_dokter, nama_dokter, harga 
					FROM smis_rwt_konsultasi_dokter_rpkk_wilis 
					WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop NOT LIKE 'del'
					UNION ALL
					SELECT id_dokter, nama_dokter, harga 
					FROM smis_rwt_konsul_dokter_rpkk_wilis 
					WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop NOT LIKE 'del'
					
					UNION ALL
					
					SELECT id_dokter, nama_dokter, harga 
					FROM smis_rwt_konsultasi_dokter_ruang_bersalin
					WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop NOT LIKE 'del'
					UNION ALL
					SELECT id_dokter, nama_dokter, harga 
					FROM smis_rwt_konsul_dokter_ruang_bersalin 
					WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop NOT LIKE 'del'
					
					UNION ALL
					
					SELECT id_dokter, nama_dokter, harga 
					FROM smis_rwt_konsultasi_dokter_ruang_pathologis
					WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop NOT LIKE 'del'
					UNION ALL
					SELECT id_dokter, nama_dokter, harga 
					FROM smis_rwt_konsul_dokter_ruang_pathologis
					WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop NOT LIKE 'del'
				) v
				GROUP BY id_dokter
			");
			if ($rows != null) {
				$id_dokter_pdr_rpkk = "";
				$kode_dokter_pdr_rpkk = "";
				$nama_dokter_pdr_rpkk = "";
				$harga_dokter_pdr_rpkk = "";
				foreach ($rows as $row) {
					$total_pdr_rpkk += $row->harga;
					$id_dokter_pdr_rpkk .= $row->id_dokter . ";";
					$params = array();
					$params['id_karyawan'] = $row->id_dokter;
					$service = new ServiceConsumer($db, "get_kode", $params, "hrd");
					$service->setMode(ServiceConsumer::$CLEAN_BOTH);
					$content = $service->execute()->getContent();
					if ($content[0] != "")
						$kode_dokter_pdr_rpkk .= $content[0] . ";";
					else
						$kode_dokter_pdr_rpkk .= "-;";
					$nama_dokter_pdr_rpkk .= $row->nama_dokter . ";";
					$harga_dokter_pdr_rpkk .= $row->harga . ";";
				}
				$id_dokter_pdr_rpkk = rtrim($id_dokter_pdr_rpkk, ";");
				$kode_dokter_pdr_rpkk = rtrim($kode_dokter_pdr_rpkk, ";");
				$nama_dokter_pdr_rpkk = rtrim($nama_dokter_pdr_rpkk, ";");
				$harga_dokter_pdr_rpkk = rtrim($harga_dokter_pdr_rpkk, ";");
			}
			$total += $total_pdr_rpkk;
			if ($total_pdr_rpkk == 0)
				$total_pdr_rpkk = ArrayAdapter::format("money", "0");
			else
				$total_pdr_rpkk = ArrayAdapter::format("money", $total_pdr_rpkk);
			
			//jasa medis (tindakan keperawatan non ecg dan non lavement) rpkk: RKK Argopuro, RKK Kelud, RKK Raung, RKK Semeru, RKK Wilis, RKK Ruang Bersalin, RKK Ruang Pathologis
			$total_jasa_medis_rpkk = 0;
			$dbtable = new DBTable($db, "smis_rwt_tindakan_perawat_rpkk_argopuro");
			$rows = $dbtable->get_result("
				SELECT SUM(harga) AS 'harga'
				FROM (
					SELECT harga_tindakan AS 'harga'
					FROM smis_rwt_tindakan_perawat_rpkk_argopuro
					WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop NOT LIKE 'del' AND nama_tindakan NOT LIKE '%E.C.G%' AND nama_tindakan NOT LIKE '%Lavement%' AND nama_tindakan NOT LIKE '%Bidan%'
					UNION ALL
					SELECT harga_tindakan AS 'harga'
					FROM smis_rwt_tindakan_perawat_rpkk_kelud
					WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop NOT LIKE 'del' AND nama_tindakan NOT LIKE '%E.C.G%' AND nama_tindakan NOT LIKE '%Lavement%' AND nama_tindakan NOT LIKE '%Bidan%'
					UNION ALL
					SELECT harga_tindakan AS 'harga'
					FROM smis_rwt_tindakan_perawat_rpkk_raung
					WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop NOT LIKE 'del' AND nama_tindakan NOT LIKE '%E.C.G%' AND nama_tindakan NOT LIKE '%Lavement%' AND nama_tindakan NOT LIKE '%Bidan%'
					UNION ALL
					SELECT harga_tindakan AS 'harga'
					FROM smis_rwt_tindakan_perawat_rpkk_semeru
					WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop NOT LIKE 'del' AND nama_tindakan NOT LIKE '%E.C.G%' AND nama_tindakan NOT LIKE '%Lavement%' AND nama_tindakan NOT LIKE '%Bidan%'
					UNION ALL
					SELECT harga_tindakan AS 'harga'
					FROM smis_rwt_tindakan_perawat_rpkk_wilis
					WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop NOT LIKE 'del' AND nama_tindakan NOT LIKE '%E.C.G%' AND nama_tindakan NOT LIKE '%Lavement%' AND nama_tindakan NOT LIKE '%Bidan%'
					UNION ALL
					SELECT harga_tindakan AS 'harga'
					FROM smis_rwt_tindakan_perawat_ruang_bersalin
					WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop NOT LIKE 'del' AND nama_tindakan NOT LIKE '%E.C.G%' AND nama_tindakan NOT LIKE '%Lavement%' AND nama_tindakan NOT LIKE '%Bidan%'
					UNION ALL
					SELECT harga_tindakan AS 'harga'
					FROM smis_rwt_tindakan_perawat_ruang_pathologis
					WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop NOT LIKE 'del' AND nama_tindakan NOT LIKE '%E.C.G%' AND nama_tindakan NOT LIKE '%Lavement%' AND nama_tindakan NOT LIKE '%Bidan%'
				) v
			");
			if ($rows != null) {
				foreach ($rows as $row) {
					$total_jasa_medis_rpkk += $row->harga;
				}
			}
			$total += $total_jasa_medis_rpkk;
			if ($total_jasa_medis_rpkk == 0)
				$total_jasa_medis_rpkk = ArrayAdapter::format("money", "0");
			else
				$total_jasa_medis_rpkk = ArrayAdapter::format("money", $total_jasa_medis_rpkk);
			
			//alok rpkk: RKK Argopuro, RKK Kelud, RKK Raung, RKK Semeru, RKK Wilis, RKK Ruang Bersalin, RKK Ruang Pathologis
			$total_alok_rpkk = 0;
			$dbtable = new DBTable($db, "smis_rwt_alok_rpkk_argopuro");
			$rows = $dbtable->get_result("
				SELECT SUM(harga) AS 'harga'
				FROM (
					SELECT harga
					FROM smis_rwt_alok_rpkk_argopuro
					WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop NOT LIKE 'del'
					UNION ALL
					SELECT harga
					FROM smis_rwt_alok_rpkk_kelud
					WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop NOT LIKE 'del'
					UNION ALL
					SELECT harga
					FROM smis_rwt_alok_rpkk_raung
					WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop NOT LIKE 'del'
					UNION ALL
					SELECT harga
					FROM smis_rwt_alok_rpkk_semeru
					WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop NOT LIKE 'del'
					UNION ALL
					SELECT harga
					FROM smis_rwt_alok_rpkk_wilis
					WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop NOT LIKE 'del'
					UNION ALL
					SELECT harga
					FROM smis_rwt_alok_ruang_bersalin
					WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop NOT LIKE 'del'
					UNION ALL
					SELECT harga
					FROM smis_rwt_alok_ruang_pathologis
					WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop NOT LIKE 'del'
				) v
			");
			if ($rows != null) {
				foreach ($rows as $row) {
					$total_alok_rpkk += $row->harga;
				}
			}
			$total += $total_alok_rpkk;
			if ($total_alok_rpkk == 0)
				$total_alok_rpkk = ArrayAdapter::format("money", "0");
			else
				$total_alok_rpkk = ArrayAdapter::format("money", $total_alok_rpkk);
			
			//ecg (tindakan keperawatan "%e.c.g%") rpkk: RKK Argopuro, RKK Kelud, RKK Raung, RKK Semeru, RKK Wilis, RKK Ruang Bersalin, RKK Ruang Pathologis
			$total_ecg_rpkk = 0;
			$dbtable = new DBTable($db, "smis_rwt_tindakan_perawat_rpkk_argopuro");
			$rows = $dbtable->get_result("
				SELECT SUM(harga) AS 'harga'
				FROM (
					SELECT harga_tindakan AS 'harga'
					FROM smis_rwt_tindakan_perawat_rpkk_argopuro
					WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop NOT LIKE 'del' AND nama_tindakan LIKE '%E.C.G%'
					UNION ALL
					SELECT harga_tindakan AS 'harga'
					FROM smis_rwt_tindakan_perawat_rpkk_kelud
					WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop NOT LIKE 'del' AND nama_tindakan LIKE '%E.C.G%'
					UNION ALL
					SELECT harga_tindakan AS 'harga'
					FROM smis_rwt_tindakan_perawat_rpkk_raung
					WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop NOT LIKE 'del' AND nama_tindakan LIKE '%E.C.G%'
					UNION ALL
					SELECT harga_tindakan AS 'harga'
					FROM smis_rwt_tindakan_perawat_rpkk_semeru
					WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop NOT LIKE 'del' AND nama_tindakan LIKE '%E.C.G%'
					UNION ALL
					SELECT harga_tindakan AS 'harga'
					FROM smis_rwt_tindakan_perawat_rpkk_wilis
					WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop NOT LIKE 'del' AND nama_tindakan LIKE '%E.C.G%'
					UNION ALL
					SELECT harga_tindakan AS 'harga'
					FROM smis_rwt_tindakan_perawat_ruang_bersalin
					WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop NOT LIKE 'del' AND nama_tindakan LIKE '%E.C.G%'
					UNION ALL
					SELECT harga_tindakan AS 'harga'
					FROM smis_rwt_tindakan_perawat_ruang_pathologis
					WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop NOT LIKE 'del' AND nama_tindakan LIKE '%E.C.G%'
				) v
			");
			if ($rows != null) {
				foreach ($rows as $row) {
					$total_ecg_rpkk += $row->harga;
				}
			}
			$total += $total_ecg_rpkk;
			if ($total_ecg_rpkk == 0)
				$total_ecg_rpkk = ArrayAdapter::format("money", "0");
			else
				$total_ecg_rpkk = ArrayAdapter::format("money", $total_ecg_rpkk);
			
			//jasa bidan (tindakan keperawatan "%Bidan%") rpkk: RKK Argopuro, RKK Kelud, RKK Raung, RKK Semeru, RKK Wilis, RKK Ruang Bersalin, RKK Ruang Pathologis
			$total_jasa_bidan_rpkk = 0;
			$dbtable = new DBTable($db, "smis_rwt_tindakan_perawat_rpkk_argopuro");
			$rows = $dbtable->get_result("
				SELECT SUM(harga) AS 'harga'
				FROM (
					SELECT harga_tindakan AS 'harga'
					FROM smis_rwt_tindakan_perawat_rpkk_argopuro
					WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop NOT LIKE 'del' AND nama_tindakan LIKE '%Bidan%'
					UNION ALL
					SELECT harga_tindakan AS 'harga'
					FROM smis_rwt_tindakan_perawat_rpkk_kelud
					WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop NOT LIKE 'del' AND nama_tindakan LIKE '%Bidan%'
					UNION ALL
					SELECT harga_tindakan AS 'harga'
					FROM smis_rwt_tindakan_perawat_rpkk_raung
					WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop NOT LIKE 'del' AND nama_tindakan LIKE '%Bidan%'
					UNION ALL
					SELECT harga_tindakan AS 'harga'
					FROM smis_rwt_tindakan_perawat_rpkk_semeru
					WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop NOT LIKE 'del' AND nama_tindakan LIKE '%Bidan%'
					UNION ALL
					SELECT harga_tindakan AS 'harga'
					FROM smis_rwt_tindakan_perawat_rpkk_wilis
					WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop NOT LIKE 'del' AND nama_tindakan LIKE '%Bidan%'
					UNION ALL
					SELECT harga_tindakan AS 'harga'
					FROM smis_rwt_tindakan_perawat_ruang_bersalin
					WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop NOT LIKE 'del' AND nama_tindakan LIKE '%Bidan%'
					UNION ALL
					SELECT harga_tindakan AS 'harga'
					FROM smis_rwt_tindakan_perawat_ruang_pathologis
					WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop NOT LIKE 'del' AND nama_tindakan LIKE '%Bidan%'
				) v
			");
			if ($rows != null) {
				foreach ($rows as $row) {
					$total_jasa_bidan_rpkk += $row->harga;
				}
			}
			$total += $total_jasa_bidan_rpkk;
			if ($total_jasa_bidan_rpkk == 0)
				$total_jasa_bidan_rpkk = ArrayAdapter::format("money", "0");
			else
				$total_jasa_bidan_rpkk = ArrayAdapter::format("money", $total_jasa_bidan_rpkk);
			
			//lavement (tindakan keperawatan "%lavement%") rpkk: RKK Argopuro, RKK Kelud, RKK Raung, RKK Semeru, RKK Wilis, RKK Ruang Bersalin, RKK Ruang Pathologis
			$total_lavement_rpkk = 0;
			$dbtable = new DBTable($db, "smis_rwt_tindakan_perawat_rpkk_argopuro");
			$rows = $dbtable->get_result("
				SELECT SUM(harga) AS 'harga'
				FROM (
					SELECT harga_tindakan AS 'harga'
					FROM smis_rwt_tindakan_perawat_rpkk_argopuro
					WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop NOT LIKE 'del' AND nama_tindakan LIKE '%Lavement%'
					UNION ALL
					SELECT harga_tindakan AS 'harga'
					FROM smis_rwt_tindakan_perawat_rpkk_kelud
					WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop NOT LIKE 'del' AND nama_tindakan LIKE '%Lavement%'
					UNION ALL
					SELECT harga_tindakan AS 'harga'
					FROM smis_rwt_tindakan_perawat_rpkk_raung
					WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop NOT LIKE 'del' AND nama_tindakan LIKE '%Lavement%'
					UNION ALL
					SELECT harga_tindakan AS 'harga'
					FROM smis_rwt_tindakan_perawat_rpkk_semeru
					WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop NOT LIKE 'del' AND nama_tindakan LIKE '%Lavement%'
					UNION ALL
					SELECT harga_tindakan AS 'harga'
					FROM smis_rwt_tindakan_perawat_rpkk_wilis
					WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop NOT LIKE 'del' AND nama_tindakan LIKE '%Lavement%'
					UNION ALL
					SELECT harga_tindakan AS 'harga'
					FROM smis_rwt_tindakan_perawat_ruang_bersalin
					WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop NOT LIKE 'del' AND nama_tindakan LIKE '%Lavement%'
					UNION ALL
					SELECT harga_tindakan AS 'harga'
					FROM smis_rwt_tindakan_perawat_ruang_pathologis
					WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop NOT LIKE 'del' AND nama_tindakan LIKE '%Lavement%'
				) v
			");
			if ($rows != null) {
				foreach ($rows as $row) {
					$total_lavement_rpkk += $row->harga;
				}
			}
			$total += $total_lavement_rpkk;
			if ($total_lavement_rpkk == 0)
				$total_lavement_rpkk = ArrayAdapter::format("money", "0");
			else
				$total_lavement_rpkk = ArrayAdapter::format("money", $total_lavement_rpkk);
			
			//lain-lain rpkk: RKK Argopuro, RKK Kelud, RKK Raung, RKK Semeru, RKK Wilis, RKK Ruang Bersalin, RKK Ruang Pathologis
			$total_lain_rpkk = ArrayAdapter::format("money", "0");
			
			//laboratorium:
			$dbtable = new DBTable($db, "smis_lab_pesanan");
			$row = $dbtable->get_row("
				SELECT SUM(biaya) AS 'harga'
				FROM smis_lab_pesanan
				WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop NOT LIKE 'del'
			");
			$laboratorium = ArrayAdapter::format("money", "0");
			if ($row->harga != null) {
				$laboratorium = ArrayAdapter::format("money", $row->harga);
				$total += $row->harga;
			}
			
			//pmi: kasir tagihan
			$dbtable = new DBTable($db, "smis_ksr_tagihan");
			$row = $dbtable->get_row("
				SELECT SUM(total) AS 'harga'
				FROM smis_ksr_tagihan
				WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop NOT LIKE 'del' AND jenis_tagihan = 'darah'
			");
			$pmi = ArrayAdapter::format("money", "0");
			if ($row->harga != null) {
				$pmi = ArrayAdapter::format("money", $row->harga);
				$total += $row->harga;
			}
			
			// depo farmasi:
			$dbtable = new DBTable($db, "smis_ap_penjualan_resep");
			$rows = $dbtable->get_result("
				SELECT nomor_resep, SUM(total) AS 'harga'
				FROM smis_ap_penjualan_resep
				WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop NOT LIKE 'del'
				GROUP BY nomor_resep
			");
			$tagihan_resep_kasir_row = $dbtable->get_row("
				SELECT SUM(total) AS 'harga'
				FROM smis_ksr_tagihan
				WHERE noreg_pasien = '" . $noreg_pasien . "' AND jenis_tagihan = 'penjualan_resep'
			");
			$depo_farmasi = ArrayAdapter::format("money", "0");
			$nomor_resep = "-";
			$nominal_resep = 0;
			if ($rows != null) {
				$nomor_resep = "";
				foreach($rows as $r) {
					$nominal_resep += $r->harga;
					$nomor_resep .= $r->nomor_resep . ";";
				}
				$nomor_resep = rtrim(";", $nomor_resep);
			}
			if ($tagihan_resep_kasir_row->harga != null) {
				$nominal_resep += $tagihan_resep_kasir_row->harga;
			}
			if ($nominal_resep != 0)
				$depo_farmasi = ArrayAdapter::format("money", $nominal_resep);
			$total += $nominal_resep;
			
			// radiologi, ctscan, dan echo:
			$dbtable = new DBTable($db, "smis_rad_pesanan");
			$rows = $dbtable->get_result("
				SELECT *
				FROM smis_rad_pesanan
				WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop NOT LIKE 'del'
			");
			$radiologi = ArrayAdapter::format("money", "0");
			$ct_scan = ArrayAdapter::format("money", "0");
			$echo = ArrayAdapter::format("money", "0");
			$id_dokter_radiologi = "-";
			$kode_dokter_radiologi = "-";
			$nama_dokter_radiologi = "-";
			$harga_dokter_radiologi = "";
			$id_dokter_echo = "-";
			$kode_dokter_echo = "-";
			$nama_dokter_echo = "-";
			$harga_dokter_echo = "";
			if ($rows != null) {
				$v_radiologi = 0;
				$v_ct_scan = 0;
				$v_echo = 0;
				$id_dokter_radiologi = "";
				$kode_dokter_radiologi = "";
				$nama_dokter_radiologi = "";
				$harga_dokter_radiologi = "";
				$id_dokter_echo = "";
				$kode_dokter_echo = "";
				$nama_dokter_echo = "";
				$harga_dokter_echo = "";
				foreach($rows as $r) {
					$id_dokter = $r->id_konsultan;
					$nama_dokter = $r->nama_konsultan;
					$params = array();
					$params['id_karyawan'] = $r->id_konsultan;
					$service = new ServiceConsumer($db, "get_kode", $params, "hrd");
					$service->setMode(ServiceConsumer::$CLEAN_BOTH);
					$content = $service->execute()->getContent();
					if ($content[0] != "")
						$kode_dokter = $content[0];
					else
						$kode_dokter = "-";
					$kelas = $r->kelas;
					$layanan_arr = json_decode($r->periksa, true);
					$harga_arr = json_decode($r->harga, true);
					foreach($layanan_arr as $key => $value) {
						if ($value == 1) {
							$id_layanan = str_replace("rad_", "", $key);
							$row_layanan = $dbtable->get_row("
								SELECT *
								FROM smis_rad_layanan
								WHERE id = '" . $id_layanan . "'
							");
							$harga_layanan = $harga_arr[$kelas . "_" . $key];
							if ($row_layanan->layanan == "ct scan: ct contrast" || $row_layanan->layanan == "ct scan: ct non-contrast")
								$v_ct_scan += $harga_layanan;
							else if ($row_layanan->layanan == "echo cardiography") {
								$v_echo += $harga_layanan;
								$id_dokter_echo .= $id_dokter . ";";
								$nama_dokter_echo .= $nama_dokter . ";";
								$kode_dokter_echo .= $kode_dokter . ";";
								$harga_dokter_echo .= $harga_layanan . ";";
							} else {
								$v_radiologi += $harga_layanan;
								$id_dokter_radiologi .= $id_dokter . ";";
								$nama_dokter_radiologi .= $nama_dokter . ";";
								$kode_dokter_radiologi .= $kode_dokter . ";";
								$harga_dokter_radiologi .= $harga_layanan . ";";
							}
						}
					}
					$id_dokter_radiologi = rtrim($id_dokter_radiologi, ";");
					$nama_dokter_radiologi = rtrim($nama_dokter_radiologi, ";");
					$kode_dokter_radiologi = rtrim($kode_dokter_radiologi, ";");
					$harga_dokter_radiologi = rtrim($harga_dokter_radiologi, ";");
					$id_dokter_echo = rtrim($id_dokter_echo, ";");
					$nama_dokter_echo = rtrim($nama_dokter_echo, ";");
					$kode_dokter_echo = rtrim($kode_dokter_echo, ";");
					$harga_dokter_echo = rtrim($harga_dokter_echo, ";");
				}
				if ($v_radiologi != 0)
					$radiologi = ArrayAdapter::format("money", $v_radiologi);
				if ($v_ct_scan != 0)
					$ct_scan = ArrayAdapter::format("money", $v_ct_scan);
				if ($v_echo != 0)
					$echo = ArrayAdapter::format("money", $v_echo);
				$total += $v_radiologi + $v_ct_scan + $v_echo;
			}
			
			// fisioterapi diampil dari pesanan:
			$dbtable = new DBTable($db, "smis_fst_pesanan");
			$row = $dbtable->get_row("
				SELECT SUM(biaya) AS 'harga'
				FROM smis_fst_pesanan
				WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop NOT LIKE 'del'
			");
			$fisioterapi = ArrayAdapter::format("money", "0");
			if ($row->harga != null) {
				$fisioterapi = ArrayAdapter::format("money", $row->harga);
				$total += $row->harga;
			}
			
			// gizi diambil dari asuhan gizi:
			$dbtable = new DBTable($db, "smis_gz_asuhan");
			$row = $dbtable->get_row("
				SELECT SUM(biaya) AS 'harga'
				FROM smis_gz_asuhan
				WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop NOT LIKE 'del'
			");
			$gizi = ArrayAdapter::format("money", "0");
			if ($row->harga != null) {
				$gizi = ArrayAdapter::format("money", $row->harga);
				$total += $row->harga;
			}
			
			// ambulan:
			$dbtable = new DBTable($db, "smis_amb_ambulan");
			$row = $dbtable->get_row("
				SELECT SUM(gaji_sopir + biaya_perjalanan + biaya_perawat + biaya_dokter + biaya_alat + biaya_lain) AS 'harga'
				FROM smis_amb_ambulan
				WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop NOT LIKE 'del'
			");
			$ambulan = ArrayAdapter::format("money", "0");
			if ($row->harga != null) {
				$ambulan = ArrayAdapter::format("money", $row->harga);
				$total += $row->harga;
			}
			
			//administrasi:
			$dbtable = new DBTable($db, "smis_ksr_tagihan");
			$row = $dbtable->get_row("
				SELECT SUM(total) AS 'harga'
				FROM smis_ksr_tagihan
				WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop NOT LIKE 'del' AND nama_tagihan LIKE '%Administrasi%'
			");
			$administrasi = ArrayAdapter::format("money", "0");
			if ($row->harga != null) {
				$administrasi = ArrayAdapter::format("money", $row->harga);
				$total += $row->harga;
			}

			//billing:
			$dbtable = new DBTable($db, "smis_rg_layananpasien");
			$row = $dbtable->get_row("
				SELECT karcis AS 'harga'
				FROM smis_rg_layananpasien
				WHERE id = '" . $noreg_pasien . "' AND prop NOT LIKE 'del'
			");
			$billing = ArrayAdapter::format("money", "0");
			if ($row->harga != null) {
				$billing = ArrayAdapter::format("money", $row->harga);
				$total += $row->harga;
			}
			
			//sewa kamar hemodialisa:
			$total_sewa_kamar_hemodialisa = 0;
			$dbtable = new DBTable($db, "smis_rwt_bed_hemodialisa");
			$rows = $dbtable->get_result("
				SELECT waktu_masuk, waktu_keluar 
				FROM smis_rwt_bed_hemodialisa 
				WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop NOT LIKE 'del'
			");
			if ($rows != null) {
				foreach ($rows as $row) {					
					$start_date = new DateTime($row->waktu_masuk);
					$end_date = new DateTime($row->waktu_keluar);
					$interval = $start_date->diff($end_date);
					$los = $interval->days + 1;
					$harga_bed = getSettings($db, "smis-rs-uri-bed-kamar_operasi", 0);
					$total_sewa_kamar_hemodialisa += $los * $harga_bed;
				}
			}
			$total += $total_sewa_kamar_hemodialisa;
			if ($total_sewa_kamar_hemodialisa == 0)
				$total_sewa_kamar_hemodialisa = ArrayAdapter::format("money", "0");
			else
				$total_sewa_kamar_hemodialisa = ArrayAdapter::format("money", $total_sewa_kamar_hemodialisa);
			
			//pdr hemodialisa:
			$total_pdr_hemodialisa = 0;
			$id_dokter_pdr_hemodialisa = "-";
			$kode_dokter_pdr_hemodialisa = "-";
			$nama_dokter_pdr_hemodialisa = "-";
			$harga_dokter_pdr_hemodialisa =  "";
			$dbtable = new DBTable($db, "smis_rwt_konsultasi_dokter_hemodialisa");
			$rows = $dbtable->get_result("
				SELECT id_dokter, nama_dokter, SUM(harga) AS 'harga'
				FROM (
					SELECT id_dokter, nama_dokter, harga 
					FROM smis_rwt_konsultasi_dokter_hemodialisa
					WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop NOT LIKE 'del'
				) v
				GROUP BY id_dokter
			");
			if ($rows != null) {
				$id_dokter_pdr_hemodialisa = "";
				$kode_dokter_pdr_hemodialisa = "";
				$nama_dokter_pdr_hemodialisa = "";
				$harga_dokter_pdr_hemodialisa = "";
				foreach ($rows as $row) {
					$total_pdr_hemodialisa += $row->harga;
					$id_dokter_pdr_hemodialisa .= $row->id_dokter . ";";
					$params = array();
					$params['id_karyawan'] = $row->id_dokter;
					$service = new ServiceConsumer($db, "get_kode", $params, "hrd");
					$service->setMode(ServiceConsumer::$CLEAN_BOTH);
					$content = $service->execute()->getContent();
					if ($content[0] != "")
						$kode_dokter_pdr_hemodialisa .= $content[0] . ";";
					else
						$kode_dokter_pdr_hemodialisa .= "-;";
					$nama_dokter_pdr_hemodialisa .= $row->nama_dokter . ";";
					$harga_dokter_pdr_hemodialisa .= $row->harga . ";";
				}
				$id_dokter_pdr_hemodialisa = rtrim($id_dokter_pdr_hemodialisa, ";");
				$kode_dokter_pdr_hemodialisa = rtrim($kode_dokter_pdr_hemodialisa, ";");
				$nama_dokter_pdr_hemodialisa = rtrim($nama_dokter_pdr_hemodialisa, ";");
				$harga_dokter_pdr_hemodialisa = rtrim($harga_dokter_pdr_hemodialisa, ";");
			}
			$total += $total_pdr_hemodialisa;
			if ($total_pdr_hemodialisa == 0)
				$total_pdr_hemodialisa = ArrayAdapter::format("money", "0");
			else
				$total_pdr_hemodialisa = ArrayAdapter::format("money", $total_pdr_hemodialisa);
			
			//jasa medis hemodialisa:
			$total_jasa_medis_hemodialisa = 0;
			$dbtable = new DBTable($db, "smis_rwt_tindakan_perawat_hemodialisa");
			$rows = $dbtable->get_result("
				SELECT SUM(harga) AS 'harga'
				FROM (
					SELECT harga_tindakan AS 'harga'
					FROM smis_rwt_tindakan_perawat_hemodialisa
					WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop NOT LIKE 'del'
				) v
			");
			if ($rows != null) {
				foreach ($rows as $row) {
					$total_jasa_medis_hemodialisa += $row->harga;
				}
			}
			$total += $total_jasa_medis_hemodialisa;
			if ($total_jasa_medis_hemodialisa == 0)
				$total_jasa_medis_hemodialisa = ArrayAdapter::format("money", "0");
			else
				$total_jasa_medis_hemodialisa = ArrayAdapter::format("money", $total_jasa_medis_hemodialisa);
			
			//alok hemodialisa:
			$total_alok_hemodialisa = 0;
			$dbtable = new DBTable($db, "smis_rwt_alok_hemodialisa");
			$rows = $dbtable->get_result("
				SELECT SUM(harga) AS 'harga'
				FROM (
					SELECT harga
					FROM smis_rwt_alok_hemodialisa
					WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop NOT LIKE 'del'
				) v
			");
			if ($rows != null) {
				foreach ($rows as $row) {
					$total_alok_hemodialisa += $row->harga;
				}
			}
			$total += $total_alok_hemodialisa;
			if ($total_alok_hemodialisa == 0)
				$total_alok_hemodialisa = ArrayAdapter::format("money", "0");
			else
				$total_alok_hemodialisa = ArrayAdapter::format("money", $total_alok_hemodialisa);
			
			//lain-lain hemodialisa:
			$total_lain_hemodialisa = ArrayAdapter::format("money", "0");
			
			//total:
			if ($total == 0)
				$total = ArrayAdapter::format("money", "0");
			else
				$total = ArrayAdapter::format("money", $total);
			
			// pembayaran tunai:
			$dbtable = new DBTable($db, "smis_ksr_bayar");
			$row = $dbtable->get_row("
				SELECT SUM(nilai) AS 'bayar'
				FROM smis_ksr_bayar
				WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop NOT LIKE 'del' AND metode LIKE '%cash%'
			");
			$bayar_tunai = ArrayAdapter::format("money", "0");
			if ($row->bayar != null) {
				$bayar_tunai = ArrayAdapter::format("money", $row->bayar);
			}
			
			// pembayaran bank:
			$dbtable = new DBTable($db, "smis_ksr_bayar");
			$row = $dbtable->get_row("
				SELECT SUM(nilai) AS 'bayar'
				FROM smis_ksr_bayar
				WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop NOT LIKE 'del' AND metode LIKE 'bank'
			");
			$bayar_bank = ArrayAdapter::format("money", "0");
			if ($row->bayar != null) {
				$bayar_bank = ArrayAdapter::format("money", $row->bayar);
			}
			
			// pembayaran tagihan:
			$dbtable = new DBTable($db, "smis_ksr_bayar");
			$row = $dbtable->get_row("
				SELECT SUM(nilai) AS 'bayar'
				FROM smis_ksr_bayar
				WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop NOT LIKE 'del' AND metode LIKE 'asuransi'
			");
			$bayar_asuransi = ArrayAdapter::format("money", "0");
			if ($row->bayar != null) {
				$bayar_asuransi = ArrayAdapter::format("money", $row->bayar);
			}
			
			$num = $_POST['num'];
			$html = "<tr id='data_" . $num . "'>";
				$html .= "<td id='data_" . $num . "_num'><small>" . ($num + 1) . "</small></td>";
				$html .= "<td id='data_" . $num . "_kelas'><small>" . ArrayAdapter::format("unslug", $kelas_pasien) . "</small></td>";
				$html .= "<td id='data_" . $num . "_noreg'><small>" . ArrayAdapter::format("only-digit6", $noreg_pasien) . "</small></td>";
				$html .= "<td id='data_" . $num . "_nrm'><small>" . ArrayAdapter::format("only-digit6", $nrm_pasien) . "</small></td>";
				$html .= "<td id='data_" . $num . "_nama'><small>" . ArrayAdapter::format("unslug", $nama_pasien) . "</small></td>";
				$html .= "<td id='data_" . $num . "_jenis'><small>" . ArrayAdapter::format("unslug", $jenis_pasien) . "</small></td>";
				$html .= "<td id='data_" . $num . "_perusahaan'><small>" . ArrayAdapter::format("unslug", $perusahaan) . "</small></td>";
				$html .= "<td id='data_" . $num . "_asuransi'><small>" . ArrayAdapter::format("unslug", $asuransi) . "</small></td>";
				$html .= "<td id='data_" . $num . "_sewa_kamar_rpa'><small>" . $total_sewa_kamar_rpa . "</small></td>";
				$html .= "<td id='data_" . $num . "_id_dokter_pdr_rpa'><small>" . $id_dokter_pdr_rpa . "</small></td>";
				$html .= "<td id='data_" . $num . "_kode_dokter_pdr_rpa'><small>" . $kode_dokter_pdr_rpa . "</small></td>";
				$html .= "<td id='data_" . $num . "_nama_dokter_pdr_rpa'><small>" . $nama_dokter_pdr_rpa . "</small></td>";
				$html .= "<td style='display: none;' id='data_" . $num . "_harga_dokter_pdr_rpa'><small>" . $harga_dokter_pdr_rpa . "</small></td>";
				$html .= "<td id='data_" . $num . "_nominal_pdr_rpa'><small>" . $total_pdr_rpa . "</small></td>";
				$html .= "<td id='data_" . $num . "_jasa_medis_rpa'><small>" . $total_jasa_medis_rpa . "</small></td>";
				$html .= "<td id='data_" . $num . "_alok_rpa'><small>" . $total_alok_rpa . "</small></td>";
				$html .= "<td id='data_" . $num . "_ecg_rpa'><small>" . $total_ecg_rpa . "</small></td>";
				$html .= "<td id='data_" . $num . "_lavement_rpa'><small>" . $total_lavement_rpa . "</small></td>";
				$html .= "<td id='data_" . $num . "_lain_rpa'><small>" . $total_lain_rpa . "</small></td>";
				$html .= "<td id='data_" . $num . "_sewa_kamar_rpb'><small>" . $total_sewa_kamar_rpb . "</small></td>";
				$html .= "<td id='data_" . $num . "_id_dokter_pdr_rpb'><small>" . $id_dokter_pdr_rpb . "</small></td>";
				$html .= "<td id='data_" . $num . "_kode_dokter_pdr_rpb'><small>" . $kode_dokter_pdr_rpb . "</small></td>";
				$html .= "<td id='data_" . $num . "_nama_dokter_pdr_rpb'><small>" . $nama_dokter_pdr_rpb . "</small></td>";
				$html .= "<td style='display: none;' id='data_" . $num . "_harga_dokter_pdr_rpb'><small>" . $harga_dokter_pdr_rpb . "</small></td>";
				$html .= "<td id='data_" . $num . "_nominal_pdr_rpb'><small>" . $total_pdr_rpb . "</small></td>";
				$html .= "<td id='data_" . $num . "_jasa_medis_rpb'><small>" . $total_jasa_medis_rpb . "</small></td>";
				$html .= "<td id='data_" . $num . "_alok_rpb'><small>" . $total_alok_rpb . "</small></td>";
				$html .= "<td id='data_" . $num . "_ecg_rpb'><small>" . $total_ecg_rpb . "</small></td>";
				$html .= "<td id='data_" . $num . "_lavement_rpb'><small>" . $total_lavement_rpb . "</small></td>";
				$html .= "<td id='data_" . $num . "_lain_rpb'><small>" . $total_lain_rpb . "</small></td>";
				$html .= "<td id='data_" . $num . "_sewa_kamar_icu'><small>" . $total_sewa_kamar_icu . "</small></td>";
				$html .= "<td id='data_" . $num . "_id_dokter_pdr_icu'><small>" . $id_dokter_pdr_icu . "</small></td>";
				$html .= "<td id='data_" . $num . "_kode_dokter_pdr_icu'><small>" . $kode_dokter_pdr_icu . "</small></td>";
				$html .= "<td id='data_" . $num . "_nama_dokter_pdr_icu'><small>" . $nama_dokter_pdr_icu . "</small></td>";
				$html .= "<td style='display: none;' id='data_" . $num . "_harga_dokter_pdr_icu'><small>" . $harga_dokter_pdr_icu . "</small></td>";
				$html .= "<td id='data_" . $num . "_nominal_pdr_icu'><small>" . $total_pdr_icu . "</small></td>";
				$html .= "<td id='data_" . $num . "_jasa_medis_icu'><small>" . $total_jasa_medis_icu . "</small></td>";
				$html .= "<td id='data_" . $num . "_alok_icu'><small>" . $total_alok_icu . "</small></td>";
				$html .= "<td id='data_" . $num . "_ecg_icu'><small>" . $total_ecg_icu . "</small></td>";
				$html .= "<td id='data_" . $num . "_lain_icu'><small>" . $total_lain_icu . "</small></td>";
				$html .= "<td id='data_" . $num . "_sewa_kamar_igd'><small>" . $total_sewa_kamar_igd . "</small></td>";
				$html .= "<td id='data_" . $num . "_id_dokter_pdr_igd'><small>" . $id_dokter_pdr_igd . "</small></td>";
				$html .= "<td id='data_" . $num . "_kode_dokter_pdr_igd'><small>" . $kode_dokter_pdr_igd . "</small></td>";
				$html .= "<td id='data_" . $num . "_nama_dokter_pdr_igd'><small>" . $nama_dokter_pdr_igd . "</small></td>";
				$html .= "<td style='display: none;' id='data_" . $num . "_harga_dokter_pdr_igd'><small>" . $harga_dokter_pdr_igd . "</small></td>";
				$html .= "<td id='data_" . $num . "_nominal_pdr_igd'><small>" . $total_pdr_igd . "</small></td>";
				$html .= "<td id='data_" . $num . "_jasa_medis_igd'><small>" . $total_jasa_medis_igd . "</small></td>";
				$html .= "<td id='data_" . $num . "_alok_igd'><small>" . $total_alok_igd . "</small></td>";
				$html .= "<td id='data_" . $num . "_ecg_igd'><small>" . $total_ecg_igd . "</small></td>";
				$html .= "<td id='data_" . $num . "_sewa_kamar_ok'><small>" . $total_sewa_kamar_ok . "</small></td>";
				$html .= "<td id='data_" . $num . "_id_operator_1_ok'><small>" . $id_operator_1_ok . "</small></td>";
				$html .= "<td style='display: none;' id='data_" . $num . "_id_operator_2_ok'><small>" . $id_operator_2_ok . "</small></td>";
				$html .= "<td style='display: none;' id='data_" . $num . "_id_anestesi_ok'><small>" . $id_anestesi_ok . "</small></td>";
				$html .= "<td style='display: none;' id='data_" . $num . "_id_asop_1_ok'><small>" . $id_asop_1_ok . "</small></td>";
				$html .= "<td style='display: none;' id='data_" . $num . "_id_asop_2_ok'><small>" . $id_asop_2_ok . "</small></td>";
				$html .= "<td style='display: none;' id='data_" . $num . "_id_asnes_1_ok'><small>" . $id_asnes_1_ok . "</small></td>";
				$html .= "<td style='display: none;' id='data_" . $num . "_id_asnes_2_ok'><small>" . $id_asnes_2_ok . "</small></td>";
				$html .= "<td style='display: none;' id='data_" . $num . "_id_asinst_1_ok'><small>" . $id_asinst_1_ok . "</small></td>";
				$html .= "<td style='display: none;' id='data_" . $num . "_id_asinst_2_ok'><small>" . $id_asinst_2_ok . "</small></td>";
				$html .= "<td style='display: none;' id='data_" . $num . "_id_asomlo_1_ok'><small>" . $id_asomlo_1_ok . "</small></td>";
				$html .= "<td style='display: none;' id='data_" . $num . "_id_asomlo_2_ok'><small>" . $id_asomlo_2_ok . "</small></td>";
				$html .= "<td style='display: none;' id='data_" . $num . "_id_bidan_1_ok'><small>" . $id_bidan_1_ok . "</small></td>";
				$html .= "<td style='display: none;' id='data_" . $num . "_id_bidan_2_ok'><small>" . $id_bidan_2_ok . "</small></td>";
				$html .= "<td id='data_" . $num . "_kode_operator_1_ok'><small>" . $kode_operator_1_ok . "</small></td>";
				$html .= "<td style='display: none;' id='data_" . $num . "_kode_operator_2_ok'><small>" . $kode_operator_2_ok . "</small></td>";
				$html .= "<td style='display: none;' id='data_" . $num . "_kode_anestesi_ok'><small>" . $kode_anestesi_ok . "</small></td>";
				$html .= "<td style='display: none;' id='data_" . $num . "_kode_asop_1_ok'><small>" . $kode_asop_1_ok . "</small></td>";
				$html .= "<td style='display: none;' id='data_" . $num . "_kode_asop_2_ok'><small>" . $kode_asop_2_ok . "</small></td>";
				$html .= "<td style='display: none;' id='data_" . $num . "_kode_asnes_1_ok'><small>" . $kode_asnes_1_ok . "</small></td>";
				$html .= "<td style='display: none;' id='data_" . $num . "_kode_asnes_2_ok'><small>" . $kode_asnes_2_ok . "</small></td>";
				$html .= "<td style='display: none;' id='data_" . $num . "_kode_asinst_1_ok'><small>" . $kode_asinst_1_ok . "</small></td>";
				$html .= "<td style='display: none;' id='data_" . $num . "_kode_asinst_2_ok'><small>" . $kode_asinst_2_ok . "</small></td>";
				$html .= "<td style='display: none;' id='data_" . $num . "_kode_asomlo_1_ok'><small>" . $kode_asomlo_1_ok . "</small></td>";
				$html .= "<td style='display: none;' id='data_" . $num . "_kode_asomlo_2_ok'><small>" . $kode_asomlo_2_ok . "</small></td>";
				$html .= "<td style='display: none;' id='data_" . $num . "_kode_bidan_1_ok'><small>" . $kode_bidan_1_ok . "</small></td>";
				$html .= "<td style='display: none;' id='data_" . $num . "_kode_bidan_2_ok'><small>" . $kode_bidan_2_ok . "</small></td>";
				$html .= "<td id='data_" . $num . "_nama_operator_1_ok'><small>" . $nama_operator_1_ok . "</small></td>";
				$html .= "<td style='display: none;' id='data_" . $num . "_nama_operator_2_ok'><small>" . $nama_operator_2_ok . "</small></td>";
				$html .= "<td style='display: none;' id='data_" . $num . "_nama_anestesi_ok'><small>" . $nama_anestesi_ok . "</small></td>";
				$html .= "<td style='display: none;' id='data_" . $num . "_nama_asop_1_ok'><small>" . $nama_asop_1_ok . "</small></td>";
				$html .= "<td style='display: none;' id='data_" . $num . "_nama_asop_2_ok'><small>" . $nama_asop_2_ok . "</small></td>";
				$html .= "<td style='display: none;' id='data_" . $num . "_nama_asnes_1_ok'><small>" . $nama_asnes_1_ok . "</small></td>";
				$html .= "<td style='display: none;' id='data_" . $num . "_nama_asnes_2_ok'><small>" . $nama_asnes_2_ok . "</small></td>";
				$html .= "<td style='display: none;' id='data_" . $num . "_nama_asinst_1_ok'><small>" . $nama_asinst_1_ok . "</small></td>";
				$html .= "<td style='display: none;' id='data_" . $num . "_nama_asinst_2_ok'><small>" . $nama_asinst_2_ok . "</small></td>";
				$html .= "<td style='display: none;' id='data_" . $num . "_nama_asomlo_1_ok'><small>" . $nama_asomlo_1_ok . "</small></td>";
				$html .= "<td style='display: none;' id='data_" . $num . "_nama_asomlo_2_ok'><small>" . $nama_asomlo_2_ok . "</small></td>";
				$html .= "<td style='display: none;' id='data_" . $num . "_nama_bidan_1_ok'><small>" . $nama_bidan_1_ok . "</small></td>";
				$html .= "<td style='display: none;' id='data_" . $num . "_nama_bidan_2_ok'><small>" . $nama_bidan_2_ok . "</small></td>";
				$html .= "<td style='display: none;' id='data_" . $num . "_harga_operator_1_ok'><small>" . $harga_operator_1_ok . "</small></td>";
				$html .= "<td style='display: none;' id='data_" . $num . "_harga_operator_2_ok'><small>" . $harga_operator_2_ok . "</small></td>";
				$html .= "<td style='display: none;' id='data_" . $num . "_harga_anestesi_ok'><small>" . $harga_anestesi_ok . "</small></td>";
				$html .= "<td id='data_" . $num . "_nominal_pdr_ok'><small>" . $total_pdr_ok . "</small></td>";
				$html .= "<td id='data_" . $num . "_jasa_medis_ok'><small>" . $total_jasa_medis_ok . "</small></td>";
				$html .= "<td id='data_" . $num . "_alok_ok'><small>" . $total_alok_ok . "</small></td>";
				$html .= "<td style='display: none;' id='data_" . $num . "_harga_asop_1_ok'><small>" . $harga_asop_1_ok . "</small></td>";
				$html .= "<td style='display: none;' id='data_" . $num . "_harga_asop_2_ok'><small>" . $harga_asop_2_ok . "</small></td>";
				$html .= "<td style='display: none;' id='data_" . $num . "_harga_asnes_1_ok'><small>" . $harga_asnes_1_ok . "</small></td>";
				$html .= "<td style='display: none;' id='data_" . $num . "_harga_asnes_2_ok'><small>" . $harga_asnes_2_ok . "</small></td>";
				$html .= "<td style='display: none;' id='data_" . $num . "_harga_asinst_1_ok'><small>" . $harga_asinst_1_ok . "</small></td>";
				$html .= "<td style='display: none;' id='data_" . $num . "_harga_asinst_2_ok'><small>" . $harga_asinst_2_ok . "</small></td>";
				$html .= "<td style='display: none;' id='data_" . $num . "_harga_asomlo_1_ok'><small>" . $harga_asomlo_1_ok . "</small></td>";
				$html .= "<td style='display: none;' id='data_" . $num . "_harga_asomlo_2_ok'><small>" . $harga_asomlo_2_ok . "</small></td>";
				$html .= "<td style='display: none;' id='data_" . $num . "_harga_bidan_1_ok'><small>" . $harga_bidan_1_ok . "</small></td>";
				$html .= "<td style='display: none;' id='data_" . $num . "_harga_bidan_2_ok'><small>" . $harga_bidan_2_ok . "</small></td>";
				$html .= "<td id='data_" . $num . "_anestesi_ok'><small>" . $total_anestesi_ok . "</small></td>";
				$html .= "<td style='display: none;' id='data_" . $num . "_h_operator_1_ok'><small>" . $h_operator_1_ok . "</small></td>";
				$html .= "<td style='display: none;' id='data_" . $num . "_h_operator_2_ok'><small>" . $h_operator_2_ok . "</small></td>";
				$html .= "<td style='display: none;' id='data_" . $num . "_h_anestesi_ok'><small>" . $h_anestesi_ok . "</small></td>";
				$html .= "<td style='display: none;' id='data_" . $num . "_h_asop_1_ok'><small>" . $h_asop_1_ok . "</small></td>";
				$html .= "<td style='display: none;' id='data_" . $num . "_h_asop_2_ok'><small>" . $h_asop_2_ok . "</small></td>";
				$html .= "<td style='display: none;' id='data_" . $num . "_h_asnes_1_ok'><small>" . $h_asnes_1_ok . "</small></td>";
				$html .= "<td style='display: none;' id='data_" . $num . "_h_asnes_2_ok'><small>" . $h_asnes_2_ok . "</small></td>";
				$html .= "<td style='display: none;' id='data_" . $num . "_h_asinst_1_ok'><small>" . $h_asinst_1_ok . "</small></td>";
				$html .= "<td style='display: none;' id='data_" . $num . "_h_asinst_2_ok'><small>" . $h_asinst_2_ok . "</small></td>";
				$html .= "<td style='display: none;' id='data_" . $num . "_h_asomlo_1_ok'><small>" . $h_asomlo_1_ok . "</small></td>";
				$html .= "<td style='display: none;' id='data_" . $num . "_h_asomlo_2_ok'><small>" . $h_asomlo_2_ok . "</small></td>";
				$html .= "<td style='display: none;' id='data_" . $num . "_h_bidan_1_ok'><small>" . $h_bidan_1_ok . "</small></td>";
				$html .= "<td style='display: none;' id='data_" . $num . "_h_bidan_2_ok'><small>" . $h_bidan_2_ok . "</small></td>";
				$html .= "<td id='data_" . $num . "_sewa_kamar_rpkk'><small>" . $total_sewa_kamar_rpkk . "</small></td>";
				$html .= "<td id='data_" . $num . "_id_dokter_pdr_rpkk'><small>" . $id_dokter_pdr_rpkk . "</small></td>";
				$html .= "<td id='data_" . $num . "_kode_dokter_pdr_rpkk'><small>" . $kode_dokter_pdr_rpkk . "</small></td>";
				$html .= "<td id='data_" . $num . "_nama_dokter_pdr_rpkk'><small>" . $nama_dokter_pdr_rpkk . "</small></td>";
				$html .= "<td style='display: none;' id='data_" . $num . "_harga_dokter_pdr_rpkk'><small>" . $harga_dokter_pdr_rpkk . "</small></td>";
				$html .= "<td id='data_" . $num . "_nominal_pdr_rpkk'><small>" . $total_pdr_rpkk . "</small></td>";
				$html .= "<td id='data_" . $num . "_jasa_medis_rpkk'><small>" . $total_jasa_medis_rpkk . "</small></td>";
				$html .= "<td id='data_" . $num . "_alok_rpkk'><small>" . $total_alok_rpkk . "</small></td>";
				$html .= "<td id='data_" . $num . "_ecg_rpkk'><small>" . $total_ecg_rpkk . "</small></td>";
				$html .= "<td id='data_" . $num . "_jasa_bidan_rpkk'><small>" . $total_jasa_bidan_rpkk . "</small></td>";
				$html .= "<td id='data_" . $num . "_lavement_rpkk'><small>" . $total_lavement_rpkk . "</small></td>";
				$html .= "<td id='data_" . $num . "_lain_rpkk'><small>" . $total_lain_rpkk . "</small></td>";
				$html .= "<td id='data_" . $num . "_laboratorium'><small>" . $laboratorium . "</small></td>";
				$html .= "<td id='data_" . $num . "_pmi'><small>" . $pmi . "</small></td>";
				$html .= "<td id='data_" . $num . "_depo_farmasi'><small>" . $depo_farmasi . "</small></td>";
				$html .= "<td id='data_" . $num . "_radiologi'><small>" . $radiologi . "</small></td>";
				$html .= "<td id='data_" . $num . "_ctscan'><small>" . $ct_scan . "</small></td>";
				$html .= "<td id='data_" . $num . "_echo'><small>" . $echo . "</small></td>";
				$html .= "<td style='display: none;' id='data_" . $num . "_id_dokter_radiologi'><small>" . $id_dokter_radiologi . "</small></td>";
				$html .= "<td style='display: none;' id='data_" . $num . "_nama_dokter_radiologi'><small>" . $nama_dokter_radiologi . "</small></td>";
				$html .= "<td style='display: none;' id='data_" . $num . "_kode_dokter_radiologi'><small>" . $kode_dokter_radiologi . "</small></td>";
				$html .= "<td style='display: none;' id='data_" . $num . "_harga_dokter_radiologi'><small>" . $harga_dokter_radiologi . "</small></td>";
				$html .= "<td style='display: none;' id='data_" . $num . "_id_dokter_echo'><small>" . $id_dokter_echo . "</small></td>";
				$html .= "<td style='display: none;' id='data_" . $num . "_nama_dokter_echo'><small>" . $nama_dokter_echo . "</small></td>";
				$html .= "<td style='display: none;' id='data_" . $num . "_kode_dokter_echo'><small>" . $kode_dokter_echo . "</small></td>";
				$html .= "<td style='display: none;' id='data_" . $num . "_harga_dokter_echo'><small>" . $harga_dokter_echo . "</small></td>";
				$html .= "<td id='data_" . $num . "_fisioterapi'><small>" . $fisioterapi . "</small></td>";
				$html .= "<td id='data_" . $num . "_gizi'><small>" . $gizi . "</small></td>";
				$html .= "<td id='data_" . $num . "_ambulan'><small>" . $ambulan . "</small></td>";
				$html .= "<td id='data_" . $num . "_administrasi'><small>" . $administrasi . "</small></td>";
				$html .= "<td id='data_" . $num . "_billing'><small>" . $billing . "</small></td>";
				$html .= "<td id='data_" . $num . "_sewa_kamar_hemodialisa'><small>" . $total_sewa_kamar_hemodialisa . "</small></td>";
				$html .= "<td id='data_" . $num . "_id_dokter_pdr_hemodialisa'><small>" . $id_dokter_pdr_hemodialisa . "</small></td>";
				$html .= "<td id='data_" . $num . "_kode_dokter_pdr_hemodialisa'><small>" . $kode_dokter_pdr_hemodialisa . "</small></td>";
				$html .= "<td id='data_" . $num . "_nama_dokter_pdr_hemodialisa'><small>" . $nama_dokter_pdr_hemodialisa . "</small></td>";
				$html .= "<td style='display: none;' id='data_" . $num . "_harga_dokter_pdr_hemodialisa'><small>" . $harga_dokter_pdr_hemodialisa . "</small></td>";
				$html .= "<td id='data_" . $num . "_nominal_pdr_hemodialisa'><small>" . $total_pdr_hemodialisa . "</small></td>";
				$html .= "<td id='data_" . $num . "_jasa_medis_hemodialisa'><small>" . $total_jasa_medis_hemodialisa . "</small></td>";
				$html .= "<td id='data_" . $num . "_alok_hemodialisa'><small>" . $total_alok_hemodialisa . "</small></td>";
				$html .= "<td id='data_" . $num . "_lain_hemodialisa'><small>" . $total_lain_hemodialisa . "</small></td>";
				$html .= "<td id='data_" . $num . "_total'><small>" . $total . "</small></td>";
				$html .= "<td id='data_" . $num . "_bayar_tunai'><small>" . $bayar_tunai . "</small></td>";
				$html .= "<td id='data_" . $num . "_bayar_bank'><small>" . $bayar_bank . "</small></td>";
				$html .= "<td id='data_" . $num . "_bayar_asuransi'><small>" . $bayar_asuransi . "</small></td>";
			$html .= "</tr>";
			$data = array();
			$data['html'] = $html;
			$data['noreg_pasien'] = ArrayAdapter::format("only-digit6", $noreg_pasien);
			$data['nrm_pasien'] = ArrayAdapter::format("only-digit6", $nrm_pasien);
			$data['nama_pasien'] = ArrayAdapter::format("unslug", $nama_pasien);
			echo json_encode($data);
		}
		return;
	} 
	
	$loading_bar = new LoadingBar("loading_bar", "");
	$button = new Button("", "", "Batal");
	$button->addClass("btn-primary");
	$button->setIsButton(Button::$ICONIC_TEXT);
	$button->setIcon("fa fa-close");
	$button->setAction("lrppi.cancel()");
	$loading_modal = new Modal("lrppi_modal", "", "Proses..");
	$loading_modal->addHtml($loading_bar->getHtml(), "after");
	$loading_modal->addFooter($button);
	
	echo $loading_modal->getHtml();
	echo $form->getHtml();
	echo $table->getHtml();
	echo "<div id='info'></div>";
	echo addJS("framework/smis/js/table_action.js");
	echo addJS("framework/bootstrap/js/bootstrap-datepicker.js");
	echo addCSS("framework/bootstrap/css/datepicker.css");
	echo addJS ("base-js/smis-base-loading.js");
?>
<script type="text/javascript">
	function LRPPSAction(name, page, action, columns) {
		this.initialize(name, page, action, columns);
	}
	LRPPSAction.prototype.constructor = LRPPSAction;
	LRPPSAction.prototype = new TableAction();
	LRPPSAction.prototype.getRegulerData = function() {
		var data = TableAction.prototype.getRegulerData.call(this);
		data['from'] = $("#lrppi_from").val();
		data['to'] = $("#lrppi_to").val();
		data['jenis_pasien'] = $("#lrppi_jenis_pasien").val();
		return data;
	};
	LRPPSAction.prototype.view = function() {
		var self = this;
		$("#info").empty();
		$("#loading_bar").sload("true", "Harap ditunggu...", 0);
		$("#lrppi_modal").smodal("show");
		FINISHED = false;
		var data = this.getRegulerData();
		data['command'] = "get_registered";
		$.post(
			"",
			data,
			function(response) {
				var json = JSON.parse(response);
				$("#lrppi_list").empty();
				self.fillHtml(0, json.jumlah);
			}
		);
	};
	LRPPSAction.prototype.fillHtml = function(num, limit) {
		if (FINISHED || num == limit) {
			if (FINISHED == false && num == limit) {
				this.finalize(limit);
			} else {
				$("#lrppi_modal").smodal("hide");
				$("#info").html(
					"<div class='alert alert-block alert-inverse'>" +
						 "<center><strong>PROSES DIBATALKAN</strong></center>" +
					 "</div>"
				);
				$("#export_button").removeAttr("onclick");
			}
			return;
		}
		var self = this;
		var data = this.getRegulerData();
		data['command'] = "get_info";
		data['num'] = num;
		$.post(
			"",
			data,
			function(response) {
				var json = JSON.parse(response);
				$("tbody#lrppi_list").append(
					json.html
				);
				$("#loading_bar").sload("true", json.noreg_pasien + " - " + json.nrm_pasien + " - " + json.nama_pasien + " (" + (num + 1) + " / " + limit + ")", (num + 1) * 100 / limit - 1);
				self.fillHtml(num + 1, limit);
			}
		);
	};
	LRPPSAction.prototype.finalize = function(limit) {
		$("#loading_bar").sload("true", "Finalisasi...", 100);
		var total_rpa_sewa_kamar = 0;
		var total_rpa_pdr = 0;
		var total_rpa_jasa_medis = 0;
		var total_rpa_alok = 0;
		var total_rpa_ecg = 0;
		var total_rpa_lavement = 0;
		var total_rpa_lain = 0;
		var total_rpb_sewa_kamar = 0;
		var total_rpb_pdr = 0;
		var total_rpb_jasa_medis = 0;
		var total_rpb_alok = 0;
		var total_rpb_ecg = 0;
		var total_rpb_lavement = 0;
		var total_rpb_lain = 0;
		var total_icu_sewa_kamar = 0;
		var total_icu_pdr = 0;
		var total_icu_jasa_medis = 0;
		var total_icu_alok = 0;
		var total_icu_ecg = 0;
		var total_icu_lain = 0;
		var total_igd_sewa_kamar = 0;
		var total_igd_pdr = 0;
		var total_igd_jasa_medis = 0;
		var total_igd_alok = 0;
		var total_igd_ecg = 0;
		var total_ok_sewa_kamar = 0;
		var total_ok_pdr = 0;
		var total_ok_jasa_medis = 0;
		var total_ok_alok = 0;
		var total_ok_anestesi = 0;
		var total_rpkk_sewa_kamar = 0;
		var total_rpkk_pdr = 0;
		var total_rpkk_jasa_medis = 0;
		var total_rpkk_alok = 0;
		var total_rpkk_ecg = 0;
		var total_rpkk_jasa_bidan = 0;
		var total_rpkk_lavement = 0;
		var total_rpkk_lain = 0;
		var total_laboratorium = 0;
		var total_pmi = 0;
		var total_depo_farmasi = 0;
		var total_rontgen = 0;
		var total_ctscan = 0;
		var total_echo = 0;
		var total_fisioterapi = 0;
		var total_gizi = 0;
		var total_ambulan = 0;
		var total_administrasi = 0;
		var total_billing = 0;
		var total_hemodialisa_sewa_kamar = 0;
		var total_hemodialisa_pdr = 0;
		var total_hemodialisa_jasa_medis = 0;
		var total_hemodialisa_alok = 0;
		var total_hemodialisa_lain = 0;
		var total_jumlah = 0;
		var total_bayar_tunai = 0;
		var total_bayar_bank = 0;
		var total_bayar_asuransi = 0;
		for(var i = 0; i < limit; i++) {
			var prefix = $("tbody#lrppi_list").children("tr").eq(i).prop("id");
			var rpa_sewa_kamar = parseFloat($("#" + prefix + "_sewa_kamar_rpa").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			total_rpa_sewa_kamar += rpa_sewa_kamar;
			var rpa_pdr = parseFloat($("#" + prefix + "_nominal_pdr_rpa").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			total_rpa_pdr += rpa_pdr;
			var rpa_jasa_medis = parseFloat($("#" + prefix + "_jasa_medis_rpa").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			total_rpa_jasa_medis += rpa_jasa_medis;
			var rpa_alok = parseFloat($("#" + prefix + "_alok_rpa").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			total_rpa_alok += rpa_alok;
			var rpa_ecg = parseFloat($("#" + prefix + "_ecg_rpa").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			total_rpa_ecg += rpa_ecg;
			var rpa_lavement = parseFloat($("#" + prefix + "_lavement_rpa").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			total_rpa_lavement += rpa_lavement;
			var rpa_lain = parseFloat($("#" + prefix + "_lain_rpa").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			total_rpa_lain += rpa_lain;
			var rpb_sewa_kamar = parseFloat($("#" + prefix + "_sewa_kamar_rpb").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			total_rpb_sewa_kamar += rpb_sewa_kamar;
			var rpb_pdr = parseFloat($("#" + prefix + "_nominal_pdr_rpb").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			total_rpb_pdr += rpb_pdr;
			var rpb_jasa_medis = parseFloat($("#" + prefix + "_jasa_medis_rpb").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			total_rpb_jasa_medis += rpb_jasa_medis;
			var rpb_alok = parseFloat($("#" + prefix + "_alok_rpb").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			total_rpb_alok += rpb_alok;
			var rpb_ecg = parseFloat($("#" + prefix + "_ecg_rpb").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			total_rpb_ecg += rpb_ecg;
			var rpb_lavement = parseFloat($("#" + prefix + "_lavement_rpb").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			total_rpb_lavement += rpb_lavement;
			var rpb_lain = parseFloat($("#" + prefix + "_lain_rpb").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			total_rpb_lain += rpb_lain;
			var icu_sewa_kamar = parseFloat($("#" + prefix + "_sewa_kamar_icu").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			total_icu_sewa_kamar += icu_sewa_kamar;
			var icu_pdr = parseFloat($("#" + prefix + "_nominal_pdr_icu").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			total_icu_pdr += icu_pdr;
			var icu_jasa_medis = parseFloat($("#" + prefix + "_jasa_medis_icu").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			total_icu_jasa_medis += icu_jasa_medis;
			var icu_alok = parseFloat($("#" + prefix + "_alok_icu").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			total_icu_alok += icu_alok;
			var icu_ecg = parseFloat($("#" + prefix + "_ecg_icu").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			total_icu_ecg += icu_ecg;
			var icu_lain = parseFloat($("#" + prefix + "_lain_icu").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			total_icu_lain += icu_lain;
			var igd_sewa_kamar = parseFloat($("#" + prefix + "_sewa_kamar_igd").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			total_igd_sewa_kamar += igd_sewa_kamar;
			var igd_pdr = parseFloat($("#" + prefix + "_nominal_pdr_igd").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			total_igd_pdr += igd_pdr;
			var igd_jasa_medis = parseFloat($("#" + prefix + "_jasa_medis_igd").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			total_igd_jasa_medis += igd_jasa_medis;
			var igd_alok = parseFloat($("#" + prefix + "_alok_igd").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			total_igd_alok += igd_alok;
			var igd_ecg = parseFloat($("#" + prefix + "_ecg_igd").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			total_igd_ecg += igd_ecg;
			var ok_sewa_kamar = parseFloat($("#" + prefix + "_sewa_kamar_ok").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			total_ok_sewa_kamar += ok_sewa_kamar;
			var ok_pdr = parseFloat($("#" + prefix + "_nominal_pdr_ok").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			total_ok_pdr += ok_pdr;
			var ok_jasa_medis = parseFloat($("#" + prefix + "_jasa_medis_ok").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			total_ok_jasa_medis += ok_jasa_medis;
			var ok_alok = parseFloat($("#" + prefix + "_alok_ok").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			total_ok_alok += ok_alok;
			var ok_anestesi = parseFloat($("#" + prefix + "_anestesi_ok").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			total_ok_anestesi += ok_anestesi;
			var rpkk_sewa_kamar = parseFloat($("#" + prefix + "_sewa_kamar_rpkk").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			total_rpkk_sewa_kamar += rpkk_sewa_kamar;
			var rpkk_pdr = parseFloat($("#" + prefix + "_nominal_pdr_rpkk").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			total_rpkk_pdr += rpkk_pdr;
			var rpkk_jasa_medis = parseFloat($("#" + prefix + "_jasa_medis_rpkk").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			total_rpkk_jasa_medis += rpkk_jasa_medis;
			var rpkk_alok = parseFloat($("#" + prefix + "_alok_rpkk").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			total_rpkk_alok += rpkk_alok;
			var rpkk_ecg = parseFloat($("#" + prefix + "_ecg_rpkk").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			total_rpkk_ecg += rpkk_ecg;
			var rpkk_jasa_bidan = parseFloat($("#" + prefix + "_jasa_bidan_rpkk").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			total_rpkk_jasa_bidan += rpkk_jasa_bidan;
			var rpkk_lavement = parseFloat($("#" + prefix + "_lavement_rpkk").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			total_rpkk_lavement += rpkk_lavement;
			var rpkk_lain = parseFloat($("#" + prefix + "_lain_rpkk").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			total_rpkk_lain += rpkk_lain;
			var laboratorium = parseFloat($("#" + prefix + "_laboratorium").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			total_laboratorium += laboratorium;
			var pmi = parseFloat($("#" + prefix + "_pmi").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			total_pmi += pmi;
			var depo_farmasi = parseFloat($("#" + prefix + "_depo_farmasi").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			total_depo_farmasi += depo_farmasi;
			var rontgen = parseFloat($("#" + prefix + "_rontgen").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			total_rontgen += rontgen;
			var ctscan = parseFloat($("#" + prefix + "_ctscan").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			total_ctscan += ctscan;
			var echo = parseFloat($("#" + prefix + "_echo").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			total_echo += echo;
			var fisioterapi = parseFloat($("#" + prefix + "_fisioterapi").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			total_fisioterapi += fisioterapi;
			var gizi = parseFloat($("#" + prefix + "_gizi").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			total_gizi += gizi;
			var ambulan = parseFloat($("#" + prefix + "_ambulan").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			total_ambulan += ambulan;
			var administrasi = parseFloat($("#" + prefix + "_administrasi").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			total_administrasi += administrasi;
			var billing = parseFloat($("#" + prefix + "_billing").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			total_billing += billing;
			var hemodialisa_sewa_kamar = parseFloat($("#" + prefix + "_sewa_kamar_hemodialisa").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			total_hemodialisa_sewa_kamar += hemodialisa_sewa_kamar;
			var hemodialisa_pdr = parseFloat($("#" + prefix + "_pdr_hemodialisa").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			total_hemodialisa_pdr += hemodialisa_pdr;
			var hemodialisa_jasa_medis = parseFloat($("#" + prefix + "_jasa_medis_hemodialisa").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			total_hemodialisa_jasa_medis += hemodialisa_jasa_medis;
			var hemodialisa_alok = parseFloat($("#" + prefix + "_alok_hemodialisa").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			total_hemodialisa_alok += hemodialisa_alok;
			var hemodialisa_lain = parseFloat($("#" + prefix + "_lain_hemodialisa").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			total_hemodialisa_lain += hemodialisa_lain;
			var jumlah = parseFloat($("#" + prefix + "_total").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			total_jumlah += jumlah;
			var bayar_tunai = parseFloat($("#" + prefix + "_bayar_tunai").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			total_bayar_tunai += bayar_tunai;
			var bayar_bank = parseFloat($("#" + prefix + "_bayar_bank").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			total_bayar_bank += bayar_bank;
			var bayar_asuransi = parseFloat($("#" + prefix + "_bayar_asuransi").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			total_bayar_asuransi += bayar_asuransi;
		}
		$("#lrppi_list").append(
			"<tr>" +
				"<td colspan='8'><center><small><strong>J U M L A H</strong></small></center></td>" +
				"<td><small><strong><div class='right money-value'>" + parseFloat(total_rpa_sewa_kamar).formatMoney("2", ".", ",") + "</div></strong></small></td>" +
				"<td></td>" +
				"<td></td>" +
				"<td></td>" +
				"<td><small><strong><div class='right money-value'>" + parseFloat(total_rpa_pdr).formatMoney("2", ".", ",") + "</div></strong></small></td>" +
				"<td><small><strong><div class='right money-value'>" + parseFloat(total_rpa_jasa_medis).formatMoney("2", ".", ",") + "</div></strong></small></td>" +
				"<td><small><strong><div class='right money-value'>" + parseFloat(total_rpa_alok).formatMoney("2", ".", ",") + "</div></strong></small></td>" +
				"<td><small><strong><div class='right money-value'>" + parseFloat(total_rpa_ecg).formatMoney("2", ".", ",") + "</div></strong></small></td>" +
				"<td><small><strong><div class='right money-value'>" + parseFloat(total_rpa_lavement).formatMoney("2", ".", ",") + "</div></strong></small></td>" +
				"<td><small><strong><div class='right money-value'>" + parseFloat(total_rpa_lain).formatMoney("2", ".", ",") + "</div></strong></small></td>" +
				"<td><small><strong><div class='right money-value'>" + parseFloat(total_rpb_sewa_kamar).formatMoney("2", ".", ",") + "</div></strong></small></td>" +
				"<td></td>" +
				"<td></td>" +
				"<td></td>" +
				"<td><small><strong><div class='right money-value'>" + parseFloat(total_rpb_pdr).formatMoney("2", ".", ",") + "</div></strong></small></td>" +
				"<td><small><strong><div class='right money-value'>" + parseFloat(total_rpb_jasa_medis).formatMoney("2", ".", ",") + "</div></strong></small></td>" +
				"<td><small><strong><div class='right money-value'>" + parseFloat(total_rpb_alok).formatMoney("2", ".", ",") + "</div></strong></small></td>" +
				"<td><small><strong><div class='right money-value'>" + parseFloat(total_rpb_ecg).formatMoney("2", ".", ",") + "</div></strong></small></td>" +
				"<td><small><strong><div class='right money-value'>" + parseFloat(total_rpb_lavement).formatMoney("2", ".", ",") + "</div></strong></small></td>" +
				"<td><small><strong><div class='right money-value'>" + parseFloat(total_rpb_lain).formatMoney("2", ".", ",") + "</div></strong></small></td>" +
				"<td><small><strong><div class='right money-value'>" + parseFloat(total_icu_sewa_kamar).formatMoney("2", ".", ",") + "</div></strong></small></td>" +
				"<td></td>" +
				"<td></td>" +
				"<td></td>" +
				"<td><small><strong><div class='right money-value'>" + parseFloat(total_icu_pdr).formatMoney("2", ".", ",") + "</div></strong></small></td>" +
				"<td><small><strong><div class='right money-value'>" + parseFloat(total_icu_jasa_medis).formatMoney("2", ".", ",") + "</div></strong></small></td>" +
				"<td><small><strong><div class='right money-value'>" + parseFloat(total_icu_alok).formatMoney("2", ".", ",") + "</div></strong></small></td>" +
				"<td><small><strong><div class='right money-value'>" + parseFloat(total_icu_ecg).formatMoney("2", ".", ",") + "</div></strong></small></td>" +
				"<td><small><strong><div class='right money-value'>" + parseFloat(total_icu_lain).formatMoney("2", ".", ",") + "</div></strong></small></td>" +
				"<td><small><strong><div class='right money-value'>" + parseFloat(total_igd_sewa_kamar).formatMoney("2", ".", ",") + "</div></strong></small></td>" +
				"<td></td>" +
				"<td></td>" +
				"<td></td>" +
				"<td><small><strong><div class='right money-value'>" + parseFloat(total_igd_pdr).formatMoney("2", ".", ",") + "</div></strong></small></td>" +
				"<td><small><strong><div class='right money-value'>" + parseFloat(total_igd_jasa_medis).formatMoney("2", ".", ",") + "</div></strong></small></td>" +
				"<td><small><strong><div class='right money-value'>" + parseFloat(total_igd_alok).formatMoney("2", ".", ",") + "</div></strong></small></td>" +
				"<td><small><strong><div class='right money-value'>" + parseFloat(total_igd_ecg).formatMoney("2", ".", ",") + "</div></strong></small></td>" +
				"<td><small><strong><div class='right money-value'>" + parseFloat(total_ok_sewa_kamar).formatMoney("2", ".", ",") + "</div></strong></small></td>" +
				"<td></td>" +
				"<td></td>" +
				"<td></td>" +
				"<td><small><strong><div class='right money-value'>" + parseFloat(total_ok_pdr).formatMoney("2", ".", ",") + "</div></strong></small></td>" +
				"<td><small><strong><div class='right money-value'>" + parseFloat(total_ok_jasa_medis).formatMoney("2", ".", ",") + "</div></strong></small></td>" +
				"<td><small><strong><div class='right money-value'>" + parseFloat(total_ok_alok).formatMoney("2", ".", ",") + "</div></strong></small></td>" +
				"<td><small><strong><div class='right money-value'>" + parseFloat(total_ok_anestesi).formatMoney("2", ".", ",") + "</div></strong></small></td>" +
				"<td><small><strong><div class='right money-value'>" + parseFloat(total_rpkk_sewa_kamar).formatMoney("2", ".", ",") + "</div></strong></small></td>" +
				"<td></td>" +
				"<td></td>" +
				"<td></td>" +
				"<td><small><strong><div class='right money-value'>" + parseFloat(total_rpkk_pdr).formatMoney("2", ".", ",") + "</div></strong></small></td>" +
				"<td><small><strong><div class='right money-value'>" + parseFloat(total_rpkk_jasa_medis).formatMoney("2", ".", ",") + "</div></strong></small></td>" +
				"<td><small><strong><div class='right money-value'>" + parseFloat(total_rpkk_alok).formatMoney("2", ".", ",") + "</div></strong></small></td>" +
				"<td><small><strong><div class='right money-value'>" + parseFloat(total_rpkk_ecg).formatMoney("2", ".", ",") + "</div></strong></small></td>" +
				"<td><small><strong><div class='right money-value'>" + parseFloat(total_rpkk_jasa_bidan).formatMoney("2", ".", ",") + "</div></strong></small></td>" +
				"<td><small><strong><div class='right money-value'>" + parseFloat(total_rpkk_lavement).formatMoney("2", ".", ",") + "</div></strong></small></td>" +
				"<td><small><strong><div class='right money-value'>" + parseFloat(total_rpkk_lain).formatMoney("2", ".", ",") + "</div></strong></small></td>" +
				"<td><small><strong><div class='right money-value'>" + parseFloat(total_laboratorium).formatMoney("2", ".", ",") + "</div></strong></small></td>" +
				"<td><small><strong><div class='right money-value'>" + parseFloat(total_pmi).formatMoney("2", ".", ",") + "</div></strong></small></td>" +
				"<td><small><strong><div class='right money-value'>" + parseFloat(total_depo_farmasi).formatMoney("2", ".", ",") + "</div></strong></small></td>" +
				"<td><small><strong><div class='right money-value'>" + parseFloat(total_rontgen).formatMoney("2", ".", ",") + "</div></strong></small></td>" +
				"<td><small><strong><div class='right money-value'>" + parseFloat(total_ctscan).formatMoney("2", ".", ",") + "</div></strong></small></td>" +
				"<td><small><strong><div class='right money-value'>" + parseFloat(total_echo).formatMoney("2", ".", ",") + "</div></strong></small></td>" +
				"<td><small><strong><div class='right money-value'>" + parseFloat(total_fisioterapi).formatMoney("2", ".", ",") + "</div></strong></small></td>" +
				"<td><small><strong><div class='right money-value'>" + parseFloat(total_gizi).formatMoney("2", ".", ",") + "</div></strong></small></td>" +
				"<td><small><strong><div class='right money-value'>" + parseFloat(total_ambulan).formatMoney("2", ".", ",") + "</div></strong></small></td>" +
				"<td><small><strong><div class='right money-value'>" + parseFloat(total_administrasi).formatMoney("2", ".", ",") + "</div></strong></small></td>" +
				"<td><small><strong><div class='right money-value'>" + parseFloat(total_billing).formatMoney("2", ".", ",") + "</div></strong></small></td>" +
				"<td><small><strong><div class='right money-value'>" + parseFloat(total_hemodialisa_sewa_kamar).formatMoney("2", ".", ",") + "</div></strong></small></td>" +
				"<td></td>" +
				"<td></td>" +
				"<td></td>" +
				"<td><small><strong><div class='right money-value'>" + parseFloat(total_hemodialisa_pdr).formatMoney("2", ".", ",") + "</div></strong></small></td>" +
				"<td><small><strong><div class='right money-value'>" + parseFloat(total_hemodialisa_jasa_medis).formatMoney("2", ".", ",") + "</div></strong></small></td>" +
				"<td><small><strong><div class='right money-value'>" + parseFloat(total_hemodialisa_alok).formatMoney("2", ".", ",") + "</div></strong></small></td>" +
				"<td><small><strong><div class='right money-value'>" + parseFloat(total_hemodialisa_lain).formatMoney("2", ".", ",") + "</div></strong></small></td>" +
				"<td><small><strong><div class='right money-value'>" + parseFloat(total_jumlah).formatMoney("2", ".", ",") + "</div></strong></small></td>" +
				"<td><small><strong><div class='right money-value'>" + parseFloat(total_bayar_tunai).formatMoney("2", ".", ",") + "</div></strong></small></td>" +
				"<td><small><strong><div class='right money-value'>" + parseFloat(total_bayar_bank).formatMoney("2", ".", ",") + "</div></strong></small></td>" +
				"<td><small><strong><div class='right money-value'>" + parseFloat(total_bayar_asuransi).formatMoney("2", ".", ",") + "</div></strong></small></td>" +
			"</tr>"
		);
		$("#lrppi_modal").smodal("hide");
		$("#info").html(
			"<div class='alert alert-block alert-info'>" +
				 "<center><strong>PROSES SELESAI</strong></center>" +
			 "</div>"
		);
		$("#export_button").removeAttr("onclick");
		$("#export_button").attr("onclick", "lrppi.export_xls()");
	};
	LRPPSAction.prototype.export_xls = function() {
		var num_rows = $("#lrppi_list").children("tr").length - 1;
		var d_data = {};
		for(var i = 0; i < num_rows; i++) {
			var prefix = $("tbody#lrppi_list").children("tr").eq(i).prop("id");
			var kelas = $("#" + prefix + "_kelas").text();
			var noreg = $("#" + prefix + "_noreg").text();
			var nrm = $("#" + prefix + "_nrm").text();
			var nama = $("#" + prefix + "_nama").text();
			var jenis = $("#" + prefix + "_jenis").text();
			var perusahaan = $("#" + prefix + "_perusahaan").text();
			var asuransi = $("#" + prefix + "_asuransi").text();
			var rpa_sewa_kamar = $("#" + prefix + "_sewa_kamar_rpa").text();
			var rpa_id_dokter_pdr = $("#" + prefix + "_id_dokter_pdr_rpa").text();
			var rpa_kode_dokter_pdr = $("#" + prefix + "_kode_dokter_pdr_rpa").text();
			var rpa_nama_dokter_pdr = $("#" + prefix + "_nama_dokter_pdr_rpa").text();
			var rpa_harga_dokter_pdr = $("#" + prefix + "_harga_dokter_pdr_rpa").text();
			var rpa_nominal_pdr = $("#" + prefix + "_nominal_pdr_rpa").text();
			var rpa_jasa_medis = $("#" + prefix + "_jasa_medis_rpa").text();
			var rpa_alok = $("#" + prefix + "_alok_rpa").text();
			var rpa_ecg = $("#" + prefix + "_ecg_rpa").text();
			var rpa_lavement = $("#" + prefix + "_lavement_rpa").text();
			var rpa_lain = $("#" + prefix + "_lain_rpa").text();
			var rpb_sewa_kamar = $("#" + prefix + "_sewa_kamar_rpb").text();
			var rpb_id_dokter_pdr = $("#" + prefix + "_id_dokter_pdr_rpb").text();
			var rpb_kode_dokter_pdr = $("#" + prefix + "_kode_dokter_pdr_rpb").text();
			var rpb_nama_dokter_pdr = $("#" + prefix + "_nama_dokter_pdr_rpb").text();
			var rpb_harga_dokter_pdr = $("#" + prefix + "_harga_dokter_pdr_rpb").text();
			var rpb_nominal_pdr = $("#" + prefix + "_nominal_pdr_rpb").text();
			var rpb_jasa_medis = $("#" + prefix + "_jasa_medis_rpb").text();
			var rpb_alok = $("#" + prefix + "_alok_rpb").text();
			var rpb_ecg = $("#" + prefix + "_ecg_rpb").text();
			var rpb_lavement = $("#" + prefix + "_lavement_rpb").text();
			var rpb_lain = $("#" + prefix + "_lain_rpb").text();
			var icu_sewa_kamar = $("#" + prefix + "_sewa_kamar_icu").text();
			var icu_id_dokter_pdr = $("#" + prefix + "_id_dokter_pdr_icu").text();
			var icu_kode_dokter_pdr = $("#" + prefix + "_kode_dokter_pdr_icu").text();
			var icu_nama_dokter_pdr = $("#" + prefix + "_nama_dokter_pdr_icu").text();
			var icu_harga_dokter_pdr = $("#" + prefix + "_harga_dokter_pdr_icu").text();
			var icu_nominal_pdr = $("#" + prefix + "_nominal_pdr_icu").text();
			var icu_jasa_medis = $("#" + prefix + "_jasa_medis_icu").text();
			var icu_alok = $("#" + prefix + "_alok_icu").text();
			var icu_ecg = $("#" + prefix + "_ecg_icu").text();
			var icu_lain = $("#" + prefix + "_lain_icu").text();
			var igd_sewa_kamar = $("#" + prefix + "_sewa_kamar_igd").text();
			var igd_id_dokter_pdr = $("#" + prefix + "_id_dokter_pdr_igd").text();
			var igd_kode_dokter_pdr = $("#" + prefix + "_kode_dokter_pdr_igd").text();
			var igd_nama_dokter_pdr = $("#" + prefix + "_nama_dokter_pdr_igd").text();
			var igd_harga_dokter_pdr = $("#" + prefix + "_harga_dokter_pdr_igd").text();
			var igd_nominal_pdr = $("#" + prefix + "_nominal_pdr_igd").text();
			var igd_jasa_medis = $("#" + prefix + "_jasa_medis_igd").text();
			var igd_alok = $("#" + prefix + "_alok_igd").text();
			var igd_ecg = $("#" + prefix + "_ecg_igd").text();
			var ok_sewa_kamar = $("#" + prefix + "_sewa_kamar_ok").text();
			var ok_id_operator_1 = $("#" + prefix + "_id_operator_1_ok").text();
			var ok_id_operator_2 = $("#" + prefix + "_id_operator_2_ok").text();
			var ok_id_anestesi = $("#" + prefix + "_id_anestesi_ok").text();
			var ok_id_asop_1 = $("#" + prefix + "_id_asop_1_ok").text();
			var ok_id_asop_2 = $("#" + prefix + "_id_asop_2_ok").text();
			var ok_id_asnes_1 = $("#" + prefix + "_id_asnes_1_ok").text();
			var ok_id_asnes_2 = $("#" + prefix + "_id_asnes_2_ok").text();
			var ok_id_asinst_1 = $("#" + prefix + "_id_asinst_1_ok").text();
			var ok_id_asinst_2 = $("#" + prefix + "_id_asinst_2_ok").text();
			var ok_id_asomlo_1 = $("#" + prefix + "_id_asomlo_1_ok").text();
			var ok_id_asomlo_2 = $("#" + prefix + "_id_asomlo_2_ok").text();
			var ok_id_bidan_1 = $("#" + prefix + "_id_bidan_1_ok").text();
			var ok_id_bidan_2 = $("#" + prefix + "_id_bidan_2_ok").text();
			var ok_kode_operator_1 = $("#" + prefix + "_kode_operator_1_ok").text();
			var ok_kode_operator_2 = $("#" + prefix + "_kode_operator_2_ok").text();
			var ok_kode_anestesi = $("#" + prefix + "_kode_anestesi_ok").text();
			var ok_kode_asop_1 = $("#" + prefix + "_kode_asop_1_ok").text();
			var ok_kode_asop_2 = $("#" + prefix + "_kode_asop_2_ok").text();
			var ok_kode_asnes_1 = $("#" + prefix + "_kode_asnes_1_ok").text();
			var ok_kode_asnes_2 = $("#" + prefix + "_kode_asnes_2_ok").text();
			var ok_kode_asinst_1 = $("#" + prefix + "_kode_asinst_1_ok").text();
			var ok_kode_asinst_2 = $("#" + prefix + "_kode_asinst_2_ok").text();
			var ok_kode_asomlo_1 = $("#" + prefix + "_kode_asomlo_1_ok").text();
			var ok_kode_asomlo_2 = $("#" + prefix + "_kode_asomlo_2_ok").text();
			var ok_kode_bidan_1 = $("#" + prefix + "_kode_bidan_1_ok").text();
			var ok_kode_bidan_2 = $("#" + prefix + "_kode_bidan_2_ok").text();
			var ok_nama_operator_1 = $("#" + prefix + "_nama_operator_1_ok").text();
			var ok_nama_operator_2 = $("#" + prefix + "_nama_operator_2_ok").text();
			var ok_nama_anestesi = $("#" + prefix + "_nama_anestesi_ok").text();
			var ok_nama_asop_1 = $("#" + prefix + "_nama_asop_1_ok").text();
			var ok_nama_asop_2 = $("#" + prefix + "_nama_asop_2_ok").text();
			var ok_nama_asnes_1 = $("#" + prefix + "_nama_asnes_1_ok").text();
			var ok_nama_asnes_2 = $("#" + prefix + "_nama_asnes_2_ok").text();
			var ok_nama_asinst_1 = $("#" + prefix + "_nama_asinst_1_ok").text();
			var ok_nama_asinst_2 = $("#" + prefix + "_nama_asinst_2_ok").text();
			var ok_nama_asomlo_1 = $("#" + prefix + "_nama_asomlo_1_ok").text();
			var ok_nama_asomlo_2 = $("#" + prefix + "_nama_asomlo_2_ok").text();
			var ok_nama_bidan_1 = $("#" + prefix + "_nama_bidan_1_ok").text();
			var ok_nama_bidan_2 = $("#" + prefix + "_nama_bidan_2_ok").text();
			var ok_harga_operator_1 = $("#" + prefix + "_harga_operator_1_ok").text();
			var ok_harga_operator_2 = $("#" + prefix + "_harga_operator_2_ok").text();
			var ok_harga_anestesi = $("#" + prefix + "_harga_anestesi_ok").text();
			var ok_harga_asop_1 = $("#" + prefix + "_harga_asop_1_ok").text();
			var ok_harga_asop_2 = $("#" + prefix + "_harga_asop_2_ok").text();
			var ok_harga_asnes_1 = $("#" + prefix + "_harga_asnes_1_ok").text();
			var ok_harga_asnes_2 = $("#" + prefix + "_harga_asnes_2_ok").text();
			var ok_harga_asinst_1 = $("#" + prefix + "_harga_asinst_1_ok").text();
			var ok_harga_asinst_2 = $("#" + prefix + "_harga_asinst_2_ok").text();
			var ok_harga_asomlo_1 = $("#" + prefix + "_harga_asomlo_1_ok").text();
			var ok_harga_asomlo_2 = $("#" + prefix + "_harga_asomlo_2_ok").text();
			var ok_harga_bidan_1 = $("#" + prefix + "_harga_bidan_1_ok").text();
			var ok_harga_bidan_2 = $("#" + prefix + "_harga_bidan_2_ok").text();
			var ok_h_operator_1 = $("#" + prefix + "_h_operator_1_ok").text();
			var ok_h_operator_2 = $("#" + prefix + "_h_operator_2_ok").text();
			var ok_h_anestesi = $("#" + prefix + "_h_anestesi_ok").text();
			var ok_h_asop_1 = $("#" + prefix + "_h_asop_1_ok").text();
			var ok_h_asop_2 = $("#" + prefix + "_h_asop_2_ok").text();
			var ok_h_asnes_1 = $("#" + prefix + "_h_asnes_1_ok").text();
			var ok_h_asnes_2 = $("#" + prefix + "_h_asnes_2_ok").text();
			var ok_h_asinst_1 = $("#" + prefix + "_h_asinst_1_ok").text();
			var ok_h_asinst_2 = $("#" + prefix + "_h_asinst_2_ok").text();
			var ok_h_asomlo_1 = $("#" + prefix + "_h_asomlo_1_ok").text();
			var ok_h_asomlo_2 = $("#" + prefix + "_h_asomlo_2_ok").text();
			var ok_h_bidan_1 = $("#" + prefix + "_h_bidan_1_ok").text();
			var ok_h_bidan_2 = $("#" + prefix + "_h_bidan_2_ok").text();
			var ok_nominal_pdr = $("#" + prefix + "_nominal_pdr_ok").text();
			var ok_jasa_medis = $("#" + prefix + "_jasa_medis_ok").text();
			var ok_alok = $("#" + prefix + "_alok_ok").text();
			var ok_anestesi = $("#" + prefix + "_anestesi_ok").text();
			var rpkk_sewa_kamar = $("#" + prefix + "_sewa_kamar_rpkk").text();
			var rpkk_id_dokter_pdr = $("#" + prefix + "_id_dokter_pdr_rpkk").text();
			var rpkk_kode_dokter_pdr = $("#" + prefix + "_kode_dokter_pdr_rpkk").text();
			var rpkk_nama_dokter_pdr = $("#" + prefix + "_nama_dokter_pdr_rpkk").text();
			var rpkk_harga_dokter_pdr = $("#" + prefix + "_harga_dokter_pdr_rpkk").text();
			var rpkk_nominal_pdr = $("#" + prefix + "_nominal_pdr_rpkk").text();
			var rpkk_jasa_medis = $("#" + prefix + "_jasa_medis_rpkk").text();
			var rpkk_alok = $("#" + prefix + "_alok_rpkk").text();
			var rpkk_ecg = $("#" + prefix + "_ecg_rpkk").text();
			var rpkk_jasa_bidan = $("#" + prefix + "_jasa_bidan_rpkk").text();
			var rpkk_lavement = $("#" + prefix + "_lavement_rpkk").text();
			var rpkk_lain = $("#" + prefix + "_lain_rpkk").text();
			var laboratorium = $("#" + prefix + "_laboratorium").text();
			var pmi = $("#" + prefix + "_pmi").text();
			var depo_farmasi = $("#" + prefix + "_depo_farmasi").text();
			var rontgen = $("#" + prefix + "_radiologi").text();
			var ctscan = $("#" + prefix + "_ctscan").text();
			var echo = $("#" + prefix + "_echo").text();
			var id_dokter_rontgen = $("#" + prefix + "_id_dokter_radiologi").text();
			var kode_dokter_rontgen = $("#" + prefix + "_kode_dokter_radiologi").text();
			var nama_dokter_rontgen = $("#" + prefix + "_nama_dokter_radiologi").text();
			var harga_dokter_rontgen = $("#" + prefix + "_harga_dokter_radiologi").text();
			var id_dokter_echo = $("#" + prefix + "_id_dokter_echo").text();
			var kode_dokter_echo = $("#" + prefix + "_kode_dokter_echo").text();
			var nama_dokter_echo = $("#" + prefix + "_nama_dokter_echo").text();
			var harga_dokter_echo = $("#" + prefix + "_harga_dokter_echo").text();
			var fisioterapi = $("#" + prefix + "_fisioterapi").text();
			var gizi = $("#" + prefix + "_gizi").text();
			var ambulan = $("#" + prefix + "_ambulan").text();
			var administrasi = $("#" + prefix + "_administrasi").text();
			var billing = $("#" + prefix + "_billing").text();
			var hemodialisa_sewa_kamar = $("#" + prefix + "_sewa_kamar_hemodialisa").text();
			var hemodialisa_id_dokter_pdr = $("#" + prefix + "_id_dokter_pdr_hemodialisa").text();
			var hemodialisa_kode_dokter_pdr = $("#" + prefix + "_kode_dokter_pdr_hemodialisa").text();
			var hemodialisa_nama_dokter_pdr = $("#" + prefix + "_nama_dokter_pdr_hemodialisa").text();
			var hemodialisa_harga_dokter_pdr = $("#" + prefix + "_harga_dokter_pdr_hemodialisa").text();
			var hemodialisa_nominal_pdr = $("#" + prefix + "_nominal_pdr_hemodialisa").text();
			var hemodialisa_jasa_medis = $("#" + prefix + "_jasa_medis_hemodialisa").text();
			var hemodialisa_alok = $("#" + prefix + "_alok_hemodialisa").text();
			var hemodialisa_lain = $("#" + prefix + "_lain_hemodialisa").text();
			var jumlah = $("#" + prefix + "_total").text();
			var bayar_tunai = $("#" + prefix + "_bayar_tunai").text();
			var bayar_bank = $("#" + prefix + "_bayar_bank").text();
			var bayar_asuransi = $("#" + prefix + "_bayar_asuransi").text();
			
			var dd_data = {};
			
			dd_data['kelas'] = kelas;
			dd_data['noreg'] = noreg;
			dd_data['nrm'] = nrm;
			dd_data['nama'] = nama;
			dd_data['jenis'] = jenis;
			dd_data['perusahaan'] = perusahaan;
			dd_data['asuransi'] = asuransi;
			dd_data['rpa_sewa_kamar'] = rpa_sewa_kamar.replace(/[^0-9-,]/g, '').replace(",", ".");
			dd_data['rpa_id_dokter_pdr'] = rpa_id_dokter_pdr;
			dd_data['rpa_kode_dokter_pdr'] = rpa_kode_dokter_pdr;
			dd_data['rpa_nama_dokter_pdr'] = rpa_nama_dokter_pdr;
			dd_data['rpa_harga_dokter_pdr'] = rpa_harga_dokter_pdr;
			dd_data['rpa_nominal_pdr'] = rpa_nominal_pdr.replace(/[^0-9-,]/g, '').replace(",", ".");
			dd_data['rpa_jasa_medis'] = rpa_jasa_medis.replace(/[^0-9-,]/g, '').replace(",", ".");
			dd_data['rpa_alok'] = rpa_alok.replace(/[^0-9-,]/g, '').replace(",", ".");
			dd_data['rpa_ecg'] = rpa_ecg.replace(/[^0-9-,]/g, '').replace(",", ".");
			dd_data['rpa_lavement'] = rpa_lavement.replace(/[^0-9-,]/g, '').replace(",", ".");
			dd_data['rpa_lain'] = rpa_lain.replace(/[^0-9-,]/g, '').replace(",", ".");
			dd_data['rpb_sewa_kamar'] = rpb_sewa_kamar.replace(/[^0-9-,]/g, '').replace(",", ".");
			dd_data['rpb_id_dokter_pdr'] = rpb_id_dokter_pdr;
			dd_data['rpb_kode_dokter_pdr'] = rpb_kode_dokter_pdr;
			dd_data['rpb_nama_dokter_pdr'] = rpb_nama_dokter_pdr;
			dd_data['rpb_harga_dokter_pdr'] = rpb_harga_dokter_pdr;
			dd_data['rpb_nominal_pdr'] = rpb_nominal_pdr.replace(/[^0-9-,]/g, '').replace(",", ".");
			dd_data['rpb_jasa_medis'] = rpb_jasa_medis.replace(/[^0-9-,]/g, '').replace(",", ".");
			dd_data['rpb_alok'] = rpb_alok.replace(/[^0-9-,]/g, '').replace(",", ".");
			dd_data['rpb_ecg'] = rpb_ecg.replace(/[^0-9-,]/g, '').replace(",", ".");
			dd_data['rpb_lavement'] = rpb_lavement.replace(/[^0-9-,]/g, '').replace(",", ".");
			dd_data['rpb_lain'] = rpb_lain.replace(/[^0-9-,]/g, '').replace(",", ".");
			dd_data['icu_sewa_kamar'] = icu_sewa_kamar.replace(/[^0-9-,]/g, '').replace(",", ".");
			dd_data['icu_id_dokter_pdr'] = icu_id_dokter_pdr;
			dd_data['icu_kode_dokter_pdr'] = icu_kode_dokter_pdr;
			dd_data['icu_nama_dokter_pdr'] = icu_nama_dokter_pdr;
			dd_data['icu_harga_dokter_pdr'] = icu_harga_dokter_pdr;
			dd_data['icu_nominal_pdr'] = icu_nominal_pdr.replace(/[^0-9-,]/g, '').replace(",", ".");
			dd_data['icu_jasa_medis'] = icu_jasa_medis.replace(/[^0-9-,]/g, '').replace(",", ".");
			dd_data['icu_alok'] = icu_alok.replace(/[^0-9-,]/g, '').replace(",", ".");
			dd_data['icu_ecg'] = icu_ecg.replace(/[^0-9-,]/g, '').replace(",", ".");
			dd_data['icu_lain'] = icu_lain.replace(/[^0-9-,]/g, '').replace(",", ".");
			dd_data['igd_sewa_kamar'] = igd_sewa_kamar.replace(/[^0-9-,]/g, '').replace(",", ".");
			dd_data['igd_id_dokter_pdr'] = igd_id_dokter_pdr;
			dd_data['igd_kode_dokter_pdr'] = igd_kode_dokter_pdr;
			dd_data['igd_nama_dokter_pdr'] = igd_nama_dokter_pdr;
			dd_data['igd_harga_dokter_pdr'] = igd_harga_dokter_pdr;
			dd_data['igd_nominal_pdr'] = igd_nominal_pdr.replace(/[^0-9-,]/g, '').replace(",", ".");
			dd_data['igd_jasa_medis'] = igd_jasa_medis.replace(/[^0-9-,]/g, '').replace(",", ".");
			dd_data['igd_alok'] = igd_alok.replace(/[^0-9-,]/g, '').replace(",", ".");
			dd_data['igd_ecg'] = igd_ecg.replace(/[^0-9-,]/g, '').replace(",", ".");
			dd_data['ok_sewa_kamar'] = ok_sewa_kamar.replace(/[^0-9-,]/g, '').replace(",", ".");
			dd_data['ok_id_operator_1'] = ok_id_operator_1;
			dd_data['ok_id_operator_2'] = ok_id_operator_2;
			dd_data['ok_id_anestesi'] = ok_id_anestesi;
			dd_data['ok_id_asop_1'] = ok_id_asop_1;
			dd_data['ok_id_asop_2'] = ok_id_asop_2;
			dd_data['ok_id_asnes_1'] = ok_id_asnes_1;
			dd_data['ok_id_asnes_2'] = ok_id_asnes_2;
			dd_data['ok_id_asinst_1'] = ok_id_asinst_1;
			dd_data['ok_id_asinst_2'] = ok_id_asinst_2;
			dd_data['ok_id_asomlo_1'] = ok_id_asomlo_1;
			dd_data['ok_id_asomlo_2'] = ok_id_asomlo_2;
			dd_data['ok_id_bidan_1'] = ok_id_bidan_1;
			dd_data['ok_id_bidan_2'] = ok_id_bidan_2;
			dd_data['ok_kode_operator_1'] = ok_kode_operator_1;
			dd_data['ok_kode_operator_2'] = ok_kode_operator_2;
			dd_data['ok_kode_anestesi'] = ok_kode_anestesi;
			dd_data['ok_kode_asop_1'] = ok_kode_asop_1;
			dd_data['ok_kode_asop_2'] = ok_kode_asop_2;
			dd_data['ok_kode_asnes_1'] = ok_kode_asnes_1;
			dd_data['ok_kode_asnes_2'] = ok_kode_asnes_2;
			dd_data['ok_kode_asinst_1'] = ok_kode_asinst_1;
			dd_data['ok_kode_asinst_2'] = ok_kode_asinst_2;
			dd_data['ok_kode_asomlo_1'] = ok_kode_asomlo_1;
			dd_data['ok_kode_asomlo_2'] = ok_kode_asomlo_2;
			dd_data['ok_kode_bidan_1'] = ok_kode_bidan_1;
			dd_data['ok_kode_bidan_2'] = ok_kode_bidan_2;
			dd_data['ok_nama_operator_1'] = ok_nama_operator_1;
			dd_data['ok_nama_operator_2'] = ok_nama_operator_2;
			dd_data['ok_nama_anestesi'] = ok_nama_anestesi;
			dd_data['ok_nama_asop_1'] = ok_nama_asop_1;
			dd_data['ok_nama_asop_2'] = ok_nama_asop_2;
			dd_data['ok_nama_asnes_1'] = ok_nama_asnes_1;
			dd_data['ok_nama_asnes_2'] = ok_nama_asnes_2;
			dd_data['ok_nama_asinst_1'] = ok_nama_asinst_1;
			dd_data['ok_nama_asinst_2'] = ok_nama_asinst_2;
			dd_data['ok_nama_asomlo_1'] = ok_nama_asomlo_1;
			dd_data['ok_nama_asomlo_2'] = ok_nama_asomlo_2;
			dd_data['ok_nama_bidan_1'] = ok_nama_bidan_1;
			dd_data['ok_nama_bidan_2'] = ok_nama_bidan_2;
			dd_data['ok_harga_operator_1'] = ok_harga_operator_1;
			dd_data['ok_harga_operator_2'] = ok_harga_operator_2;
			dd_data['ok_harga_anestesi'] = ok_harga_anestesi;
			dd_data['ok_harga_asop_1'] = ok_harga_asop_1;
			dd_data['ok_harga_asop_2'] = ok_harga_asop_2;
			dd_data['ok_harga_asnes_1'] = ok_harga_asnes_1;
			dd_data['ok_harga_asnes_2'] = ok_harga_asnes_2;
			dd_data['ok_harga_asinst_1'] = ok_harga_asinst_1;
			dd_data['ok_harga_asinst_2'] = ok_harga_asinst_2;
			dd_data['ok_harga_asomlo_1'] = ok_harga_asomlo_1;
			dd_data['ok_harga_asomlo_2'] = ok_harga_asomlo_2;
			dd_data['ok_harga_bidan_1'] = ok_harga_bidan_1;
			dd_data['ok_harga_bidan_2'] = ok_harga_bidan_2;
			dd_data['ok_h_operator_1'] = ok_h_operator_1;
			dd_data['ok_h_operator_2'] = ok_h_operator_2;
			dd_data['ok_h_anestesi'] = ok_h_anestesi;
			dd_data['ok_h_asop_1'] = ok_h_asop_1;
			dd_data['ok_h_asop_2'] = ok_h_asop_2;
			dd_data['ok_h_asnes_1'] = ok_h_asnes_1;
			dd_data['ok_h_asnes_2'] = ok_h_asnes_2;
			dd_data['ok_h_asinst_1'] = ok_h_asinst_1;
			dd_data['ok_h_asinst_2'] = ok_h_asinst_2;
			dd_data['ok_h_asomlo_1'] = ok_h_asomlo_1;
			dd_data['ok_h_asomlo_2'] = ok_h_asomlo_2;
			dd_data['ok_h_bidan_1'] = ok_h_bidan_1;
			dd_data['ok_h_bidan_2'] = ok_h_bidan_2;
			dd_data['ok_nominal_pdr'] = ok_nominal_pdr.replace(/[^0-9-,]/g, '').replace(",", ".");
			dd_data['ok_jasa_medis'] = ok_jasa_medis.replace(/[^0-9-,]/g, '').replace(",", ".");
			dd_data['ok_alok'] = ok_alok.replace(/[^0-9-,]/g, '').replace(",", ".");
			dd_data['ok_anestesi'] = ok_anestesi.replace(/[^0-9-,]/g, '').replace(",", ".");
			dd_data['rpkk_sewa_kamar'] = rpkk_sewa_kamar.replace(/[^0-9-,]/g, '').replace(",", ".");
			dd_data['rpkk_id_dokter_pdr'] = rpkk_id_dokter_pdr;
			dd_data['rpkk_kode_dokter_pdr'] = rpkk_kode_dokter_pdr;
			dd_data['rpkk_nama_dokter_pdr'] = rpkk_nama_dokter_pdr;
			dd_data['rpkk_harga_dokter_pdr'] = rpkk_harga_dokter_pdr;
			dd_data['rpkk_nominal_pdr'] = rpkk_nominal_pdr.replace(/[^0-9-,]/g, '').replace(",", ".");
			dd_data['rpkk_jasa_medis'] = rpkk_jasa_medis.replace(/[^0-9-,]/g, '').replace(",", ".");
			dd_data['rpkk_alok'] = rpkk_alok.replace(/[^0-9-,]/g, '').replace(",", ".");
			dd_data['rpkk_ecg'] = rpkk_ecg.replace(/[^0-9-,]/g, '').replace(",", ".");
			dd_data['rpkk_jasa_bidan'] = rpkk_jasa_bidan.replace(/[^0-9-,]/g, '').replace(",", ".");
			dd_data['rpkk_lavement'] = rpkk_lavement.replace(/[^0-9-,]/g, '').replace(",", ".");
			dd_data['rpkk_lain'] = rpkk_lain.replace(/[^0-9-,]/g, '').replace(",", ".");
			dd_data['laboratorium'] = laboratorium.replace(/[^0-9-,]/g, '').replace(",", ".");
			dd_data['pmi'] = pmi.replace(/[^0-9-,]/g, '').replace(",", ".");
			dd_data['depo_farmasi'] = depo_farmasi.replace(/[^0-9-,]/g, '').replace(",", ".");
			dd_data['rontgen'] = rontgen.replace(/[^0-9-,]/g, '').replace(",", ".");
			dd_data['ctscan'] = ctscan.replace(/[^0-9-,]/g, '').replace(",", ".");
			dd_data['echo'] = echo.replace(/[^0-9-,]/g, '').replace(",", ".");
			dd_data['id_dokter_rontgen'] = id_dokter_rontgen;
			dd_data['kode_dokter_rontgen'] = kode_dokter_rontgen;
			dd_data['nama_dokter_rontgen'] = nama_dokter_rontgen;
			dd_data['harga_dokter_rontgen'] = harga_dokter_rontgen;
			dd_data['id_dokter_echo'] = id_dokter_echo;
			dd_data['kode_dokter_echo'] = kode_dokter_echo;
			dd_data['nama_dokter_echo'] = nama_dokter_echo;
			dd_data['harga_dokter_echo'] = harga_dokter_echo;
			dd_data['fisioterapi'] = fisioterapi.replace(/[^0-9-,]/g, '').replace(",", ".");
			dd_data['gizi'] = gizi.replace(/[^0-9-,]/g, '').replace(",", ".");
			dd_data['ambulan'] = ambulan.replace(/[^0-9-,]/g, '').replace(",", ".");
			dd_data['administrasi'] = administrasi.replace(/[^0-9-,]/g, '').replace(",", ".");
			dd_data['billing'] = billing.replace(/[^0-9-,]/g, '').replace(",", ".");
			dd_data['hemodialisa_sewa_kamar'] = hemodialisa_sewa_kamar.replace(/[^0-9-,]/g, '').replace(",", ".");
			dd_data['hemodialisa_id_dokter_pdr'] = hemodialisa_id_dokter_pdr;
			dd_data['hemodialisa_kode_dokter_pdr'] = hemodialisa_kode_dokter_pdr;
			dd_data['hemodialisa_nama_dokter_pdr'] = hemodialisa_nama_dokter_pdr;
			dd_data['hemodialisa_harga_dokter_pdr'] = hemodialisa_harga_dokter_pdr;
			dd_data['hemodialisa_nominal_pdr'] = hemodialisa_nominal_pdr.replace(/[^0-9-,]/g, '').replace(",", ".");
			dd_data['hemodialisa_jasa_medis'] = hemodialisa_jasa_medis.replace(/[^0-9-,]/g, '').replace(",", ".");
			dd_data['hemodialisa_alok'] = hemodialisa_alok.replace(/[^0-9-,]/g, '').replace(",", ".");
			dd_data['hemodialisa_lain'] = hemodialisa_lain.replace(/[^0-9-,]/g, '').replace(",", ".");
			dd_data['jumlah'] = jumlah.replace(/[^0-9-,]/g, '').replace(",", ".");
			dd_data['bayar_tunai'] = bayar_tunai.replace(/[^0-9-,]/g, '').replace(",", ".");
			dd_data['bayar_bank'] = bayar_bank.replace(/[^0-9-,]/g, '').replace(",", ".");
			dd_data['bayar_asuransi'] = bayar_asuransi.replace(/[^0-9-,]/g, '').replace(",", ".");
			
			d_data[i] = dd_data;
		}
		var data = this.getRegulerData();
		data['command'] = "eksport_excel";
		data['num_rows'] = num_rows;
		data['d_data'] = JSON.stringify(d_data);
		data['jenis_pasien'] = $("#lrppi_jenis_pasien").val();
		postForm(data);
	};
	LRPPSAction.prototype.cancel = function() {
		FINISHED = true;
	};
	
	var FINISHED;
	var lrppi;
	$(document).ready(function() {
		$('.mydate').datepicker();
		$("#lrppi_modal").on("show", function() {
			$("a.close").hide();
		});
		$("tbody#lrppi_list").append(
			"<tr><td colspan='82'><center><small><strong>LAPORAN BELUM DIHASILKAN</strong></small></center></td></tr>"
		);
		lrppi = new LRPPSAction(
			"lrppi",
			"kasir",
			"laporan_rekap_pendapatan_per_pasien_rawat_inap",
			new Array()
		);
		$(document).keyup(function(e) {
			if (e.which == 27) {
				FINISHED = true;
			}
		});
	});
</script>