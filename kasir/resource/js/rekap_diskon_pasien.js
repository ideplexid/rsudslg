var REKAP_DISKON_CUR_NUMBER=0;
var REKAP_DISKON_TOT_NUMBER=0;
var dump_reinit=function(){
	rekap_diskon_pasien.view();
};
var _full_print="";	
var loop_init_all_tagihan_pasien=function(){rekap_diskon_pasien.loop_init_all_tagihan_pasien(); };
$(document).ready(function(){
	
	
	
	
	
	/*INIT PRINT TAGIHAN LOOP*/
	rekap_diskon_pasien.print_tagihan_pasien=function(id){
		var data=this.getRegulerData();
		data['action']="total_tagihan_kasir";
		data['noreg_pasien']=id;
		data['command']="show_kwitansi";
		data['file_mode']=$("#rekap_diskon_pasien_kwitansi").val();
		showLoading();
		$.post("",data,function(res){
			dismissLoading();
			smis_print(res);	
		});
	};
	
	rekap_diskon_pasien.loop_print=function(total,number){
		if(number>=total){
			$("#rtb_load_modal").modal("hide");
			smis_print(_full_print);
			return ;
		}
		var id=$("#rekap_diskon_pasien_list").children().eq(number).children().eq(1).html();
		var nama=$("#rekap_diskon_pasien_list").children().eq(number).children().eq(3).html();
		var data=this.getRegulerData();
		data['command']="show_kwitansi";
		data['action']="total_tagihan_kasir";
		data['file_mode']=$("#rekap_diskon_pasien_kwitansi").val();
		data['noreg_pasien']=id;
		$.post("",data,function(res){
			$("#rtb_person_bar").sload("true","Loading..."+nama+" [ "+(number+1)+" / "+total+" ]",(number)*100/total);
			_full_print+=("<div class='pagebreak'></div>"+res);
			rekap_diskon_pasien.loop_print(total,number+1)
		});
	};
	
	rekap_diskon_pasien.init_all_print=function(id){
		_full_print="";
		var total=$("#rekap_diskon_pasien_list").children().length-1;		
		$("#rtb_load_modal").modal("show");
		rekap_diskon_pasien.loop_print(total,0);
	};
	/*END INIT PRINT TAGIHAN LOOP*/
	
	
	
	/*INIT REKAP TAGIHAN LOOP*/
	rekap_diskon_pasien.init_all_tagihan_pasien=function(){
		REKAP_DISKON_TOT_NUMBER=$("#rekap_diskon_pasien_list").children().length-1;
		REKAP_DISKON_CUR_NUMBER=0;
		this.loop_init_all_tagihan_pasien();
	};
	
	rekap_diskon_pasien.loop_init_all_tagihan_pasien=function(){
		if(REKAP_DISKON_CUR_NUMBER>=REKAP_DISKON_TOT_NUMBER){
			rekap_diskon_pasien.view();
			return;	
		}
		var id=$("#rekap_diskon_pasien_list").children().eq(REKAP_DISKON_CUR_NUMBER).children().eq(1).html();
		var nama=$("#rekap_diskon_pasien_list").children().eq(REKAP_DISKON_CUR_NUMBER).children().eq(3).html();
		var title="<small>Processing..."+nama+" [ "+(REKAP_DISKON_CUR_NUMBER+1)+" / "+REKAP_DISKON_TOT_NUMBER+" ]</small>";
		REKAP_DISKON_CUR_NUMBER++;
		var data=this.getRegulerData();
		data['action']="total_tagihan_kasir";
		data['noreg_pasien']=Number(id);
		data['invoke']="loop_init_all_tagihan_pasien";
		data['title']=title;
		showLoading();
		$.post("",data,function(res){
			$("#tagihan_place_init").html(res);
			dismissLoading();
		});
	};
	
	rekap_diskon_pasien.init_tagihan=function(id){
		var data=this.getRegulerData();
		data['action']="total_tagihan_kasir";
		data['noreg_pasien']=id;
		data['invoke']="dump_reinit";
		showLoading();
		$.post("",data,function(res){
			$("#tagihan_place_init").html(res);
			dismissLoading();
		});
	};
	
	/*END - INIT REKAP TAGIHAN LOOP*/
	
	
	/*INIT KOREKSI CARABAYAR LOOP*/
	rekap_diskon_pasien.init_koreksi_carabayar=function(id){
		var data=this.getRegulerData();
		data['action']="koreksi_carabayar";
		data['noreg_pasien']=id;
		showLoading();
		$.post("",data,function(res){
			dismissLoading();
		});
	};
	
	
	rekap_diskon_pasien.init_all_koreksi_carabayar=function(){
		var total=$("#rekap_diskon_pasien_list").children().length-1;		
		$("#rtb_load_modal").modal("show");
		rekap_diskon_pasien.loop_init_all_koreksi_carabayar(total,0);
	};
	
	rekap_diskon_pasien.loop_init_all_koreksi_carabayar=function(total,number){
		if(number>=total){
			$("#rtb_load_modal").modal("hide");
			return ;
		}
		var id=$("#rekap_diskon_pasien_list").children().eq(number).children().eq(1).html();
		var nama=$("#rekap_diskon_pasien_list").children().eq(number).children().eq(3).html();
		var data=this.getRegulerData();
		data['action']="koreksi_carabayar";
		data['noreg_pasien']=id;
		$.post("",data,function(res){
			$("#rtb_person_bar").sload("true","Loading..."+nama+" [ "+(number+1)+" / "+total+" ]",(number)*100/total);
			rekap_diskon_pasien.loop_init_all_koreksi_carabayar(total,number+1)
		});		
	};
	/*END - INIT KOREKSI CARABAYAR LOOP*/
	
});