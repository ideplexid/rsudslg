var edit_tindakan_igd;
var px_edit_tindakan_igd;
var dokter_edit_tindakan_igd;
var tarif_keperawatan_igd;

var perawat_edit_tindakan_igd;
var perawat_edit_tindakan_igd_dua;
var perawat_edit_tindakan_igd_tiga;	
var perawat_edit_tindakan_igd_empat;
var perawat_edit_tindakan_igd_lima;	
var perawat_edit_tindakan_igd_enam;	
var perawat_edit_tindakan_igd_tujuh;	
var perawat_edit_tindakan_igd_delapan;	
var perawat_edit_tindakan_igd_sembilan;	
var perawat_edit_tindakan_igd_sepuluh;	

var NUMBER_PERAWAT_IGD;
var NUMBER_PERAWAT_IGD_TABLE_ACTION;
function reload_tarif_igd(next){
	var jumlah=Number($("#edit_tindakan_igd_jumlah").val());
	var satuan=getMoney("#edit_tindakan_igd_satuan");
	var total=jumlah*satuan;
	if($("#edit_tindakan_igd_jumlah").attr("type")=="text" ){
		$("#edit_tindakan_igd_harga_tindakan").maskMoney('mask',total);
	}else{
		$("#edit_tindakan_igd_harga_tindakan").val(total);
	}
	
	if(next=="jumlah" && $("#edit_tindakan_igd_jumlah").attr("type")=="text" ){
		$("#edit_tindakan_igd_jumlah").focus();
	}else if(next=="jumlah" ){
		$("#edit_tindakan_igd_save").focus();
	}
}

$(document).ready(function(){
    
    NUMBER_PERAWAT_IGD_TABLE_ACTION=new Array();
	NUMBER_PERAWAT_IGD=new Array();
	NUMBER_PERAWAT_IGD[1]="";
	NUMBER_PERAWAT_IGD[2]="_dua";
	NUMBER_PERAWAT_IGD[3]="_tiga";
	NUMBER_PERAWAT_IGD[4]="_empat";
	NUMBER_PERAWAT_IGD[5]="_lima";
	NUMBER_PERAWAT_IGD[6]="_enam";
	NUMBER_PERAWAT_IGD[7]="_tujuh";
	NUMBER_PERAWAT_IGD[8]="_delapan";
	NUMBER_PERAWAT_IGD[9]="_sembilan";
	NUMBER_PERAWAT_IGD[10]="_sepuluh";
    
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
    $('.mydatetime').datetimepicker({ minuteStep: 1});
    
    $("#edit_tindakan_igd_jumlah").on("keyup",function(e){
		reload_tarif_igd("save");
	});

	$("#edit_tindakan_igd_jumlah").on("change",function(e){
		reload_tarif_igd("save");
	});

	var column=new Array(
			'id','id_dokter',
			'nama_dokter','waktu','kelas','harga_tindakan',
			'nama_tindakan','id_tindakan',
			"jumlah","satuan",
            "jaspel","jaspel_lain_lain","jaspel_penunjang");
     for(var i=1;i<=NUMBER_PERAWAT_IGD.length;i++){
		var num=NUMBER_PERAWAT_IGD[i];
		column.push("nama_perawat"+num);
		column.push("id_perawat"+num);
     }		
       
	edit_tindakan_igd=new TableAction("edit_tindakan_igd","kasir","edit_tindakan_igd",column);
    edit_tindakan_igd.addNoClear("nama_pasien");
    edit_tindakan_igd.addNoClear("noreg_pasien");
    edit_tindakan_igd.addNoClear("nrm_pasien");
    edit_tindakan_igd.addNoClear("carabayar");
    edit_tindakan_igd.addNoClear("ruang");
    
	edit_tindakan_igd.addRegulerData=function(a){
		a['noreg_pasien']=$("#edit_tindakan_igd_noreg_pasien").val();
		a['nrm_pasien']=$("#edit_tindakan_igd_nrm_pasien").val();
		a['nama_pasien']=$("#edit_tindakan_igd_nama_pasien").val();
		a['carabayar']=$("#edit_tindakan_igd_carabayar").val();
		a['ruang']=$("#edit_tindakan_igd_ruang").val();
        a['header']=$("#edit_tindakan_igd_header_element").val();
		return a;
	};
	edit_tindakan_igd.show_add_form=function(){
		if($("#edit_tindakan_igd_noreg_pasien").val()==""){
			showWarning("Peringatan","Silakan Pilih Pasien Dahulu");
			return;
		}
		this.clear();
		this.show_form();
	};	
    
    edit_tindakan_igd.beforesave=function(){
		if($("#edit_tindakan_igd_nama_dokter").val()==""){
			$("#edit_tindakan_igd_id_dokter").val(0);
		}
		for(var i=1;i<=NUMBER_PERAWAT_IGD.length;i++){
			var num=NUMBER_PERAWAT_IGD[i];
			if($("#edit_tindakan_igd_nama_perawat"+num).val()==""){
				$("#edit_tindakan_igd_id_perawat"+num).val(0);
			}
		}		
	};
    
    edit_tindakan_igd.reload_view=function(){
        var d=list_registered.getRegulerData();
        d['super_command']="edit_tindakan_igd";
        d['ruangan']=$("#edit_tindakan_igd_ruang").val();
        d['noreg_pasien']=$("#noreg_pasien").val();
        showLoading();
        $.post("",d,function (res){
            $("#tindakan_igd").html(res);
            dismissLoading();
        });
    }
    
	px_edit_tindakan_igd=new TableAction("px_edit_tindakan_igd","kasir","get_registered_patient",column);
	px_edit_tindakan_igd.setSuperCommand("px_edit_tindakan_igd");
	px_edit_tindakan_igd.selected=function(json){
		$("#edit_tindakan_igd_nama_pasien").val(json.nama_pasien);
		$("#edit_tindakan_igd_alamat_pasien").val(json.alamat_pasien);
		$("#edit_tindakan_igd_noreg_pasien").val(json.id);
		$("#edit_tindakan_igd_nrm_pasien").val(json.nrm);
		$("#edit_tindakan_igd_carabayar").val(json.carabayar);
		edit_tindakan_igd.view();
	};
    
    dokter_edit_tindakan_igd=new TableAction("dokter_edit_tindakan_igd","kasir","get_employee",new Array());
	dokter_edit_tindakan_igd.setSuperCommand("dokter_edit_tindakan_igd");
    dokter_edit_tindakan_igd.setOwnChooserData(true);
	dokter_edit_tindakan_igd.addRegulerData=function(a){
        a['filter']="dokter";
        return a;
    };
    dokter_edit_tindakan_igd.selected=function(json){
		var nama=json.nama;
		var nip=json.id;		
		$("#edit_tindakan_igd_nama_dokter").val(nama);
		$("#edit_tindakan_igd_id_dokter").val(nip);
	};

	stypeahead("#edit_tindakan_igd_nama_dokter",3,dokter_edit_tindakan_igd,"nama",function(item){
		$("#edit_tindakan_igd_id_dokter").val(item.id);
		$("#edit_tindakan_igd_nama_dokter").val(item.nama);
		$("#edit_tindakan_igd_nama_perawat").focus();
	});
    
    tarif_keperawatan_igd=new TableAction("tarif_keperawatan_igd","kasir","edit_tindakan_igd",new Array());
	tarif_keperawatan_igd.setSuperCommand("tarif_keperawatan_igd");
	tarif_keperawatan_igd.addRegulerData=function(d){
		d['noreg_pasien']=$("#edit_tindakan_igd_noreg_pasien").val();
		return d;
	};
	tarif_keperawatan_igd.selected=function(json){
		var kelas=json.kelas;
		var nama=json.nama;
		var harga=Number(json.tarif);
		$("#edit_tindakan_igd_kelas").val(kelas);
		$("#edit_tindakan_igd_nama_tindakan").val(nama);
		$("#edit_tindakan_igd_satuan").maskMoney('mask',harga);
        $("#edit_tindakan_igd_jaspel").val(json.jaspel);
        $("#edit_tindakan_igd_jaspel_lain_lain").val(json.lain_lain);
        $("#edit_tindakan_igd_jaspel_penunjang").val(json.penunjang);
		reload_tarif_igd("jumlah");
	};
	stypeahead("#edit_tindakan_igd_nama_tindakan",3,tarif_keperawatan_igd,"nama",function(item){
		$("#edit_tindakan_igd_id_tindakan").val(item.id);
		$("#edit_tindakan_igd_kelas").val(item.kelas);
		$("#edit_tindakan_igd_nama_tindakan").val(item.name);
		$("#edit_tindakan_igd_satuan").maskMoney('mask',Number(item.tarif));
        $("#edit_tindakan_igd_jaspel").val(item.jaspel);
		$("#edit_tindakan_igd_jaspel_lain_lain").val(item.lain_lain);
        $("#edit_tindakan_igd_jaspel_penunjang").val(item.penunjang);
		reload_tarif_igd("jumlah");
	});
    
    
    for(var i=1;i<=NUMBER_PERAWAT_IGD.length;i++){
		var num=NUMBER_PERAWAT_IGD[i];
		var ptpi=new TableAction("perawat_edit_tindakan_igd"+num,"kasir","get_employee",new Array());
			ptpi.numero=num;
			ptpi.setSuperCommand("perawat_edit_tindakan_igd"+ptpi.numero);
            ptpi.setOwnChooserData(true);
            ptpi.addRegulerData=function(a){
                a['filter']="perawat";
                return a;
            };
			ptpi.selected=function(json){
				var nama=json.nama;
				var nip=json.id;
				$("#edit_tindakan_igd_nama_perawat"+this.numero).val(nama);
				$("#edit_tindakan_igd_id_perawat"+this.numero).val(nip);
			};
		$("#edit_tindakan_igd_nama_perawat"+num).data("numero",num);
		$("#edit_tindakan_igd_nama_perawat"+num).data("i",i);
		stypeahead("#edit_tindakan_igd_nama_perawat"+num,3,ptpi,"nama",function(item,id){
			var numero=$(id).data("numero");
			var the_i=Number($(id).data("i"));
			$("#edit_tindakan_igd_id_perawat"+numero).val(item.id);
			$("#edit_tindakan_igd_nama_perawat"+numero).val(item.nama);
			if(the_i<NUMBER_PERAWAT_IGD.length){
				var next_num=NUMBER_PERAWAT_IGD[the_i+1];
				if($("#edit_tindakan_igd_nama_perawat"+next_num).attr("type")=="hidden"){
					$("#edit_tindakan_igd_nama_tindakan").focus();
				}else{
					$("#edit_tindakan_igd_nama_perawat"+next_num).focus();
				}
			}
		});
		NUMBER_PERAWAT_IGD_TABLE_ACTION[i]=ptpi;
			
	}		
	
	/*STRAT NAMA PERAWAT*/
	perawat_edit_tindakan_igd=NUMBER_PERAWAT_IGD_TABLE_ACTION[1];
	perawat_edit_tindakan_igd_dua=NUMBER_PERAWAT_IGD_TABLE_ACTION[2];
	perawat_edit_tindakan_igd_tiga=NUMBER_PERAWAT_IGD_TABLE_ACTION[3];
	perawat_edit_tindakan_igd_empat=NUMBER_PERAWAT_IGD_TABLE_ACTION[4];
	perawat_edit_tindakan_igd_lima=NUMBER_PERAWAT_IGD_TABLE_ACTION[5];
	perawat_edit_tindakan_igd_enam=NUMBER_PERAWAT_IGD_TABLE_ACTION[6];
	perawat_edit_tindakan_igd_tujuh=NUMBER_PERAWAT_IGD_TABLE_ACTION[7];
	perawat_edit_tindakan_igd_delapan=NUMBER_PERAWAT_IGD_TABLE_ACTION[8];
	perawat_edit_tindakan_igd_sembilan=NUMBER_PERAWAT_IGD_TABLE_ACTION[9];
	perawat_edit_tindakan_igd_sepuluh=NUMBER_PERAWAT_IGD_TABLE_ACTION[10];
	/*END NAMA PERAWAT*/
    
	

	$("#edit_tindakan_igd_ruang").change(function(){
		edit_tindakan_igd.reload_view();
	});
        
	if($("#edit_tindakan_igd_noreg_pasien").val()!=""){
        var noreg_pasien=$("#edit_tindakan_igd_noreg_pasien").val();  
        px_edit_tindakan_igd.select(noreg_pasien);
    }
	
});