/**
 * this used for viewing the history of payment
 * this page automatically filter the assurance payment
 * 
 * @version     : 5.0.0
 * @since       : 14 May 2015
 * @author      : Nurul Huda
 * @copyright   : goblooge@gmail.com
 * @license     : LGPLv3
 * @used        : kasir/resource/php/laporan/lap_asuransi.php
 * */
var lap_asuransi;
var lap_asuransi_asuransi;

function reup_asuransi(){
    $("#sub_menu_asuransi_content").html("");	
    $("#sub_menu_asuransi").hide("fast");
    $("#main_asuransi").show("fast");
    lap_asuransi.view();
}
function load_registration_asuransi(noreg,asuransi_json){
    var data = {
            page                    : "kasir",
            action                  : "pembayaran_patient",
            super_command           : "",
            prototype_name          : "",
            prototype_slug          : "",
            prototype_implement     : "",
            list_registered_select  : noreg
        };
    showLoading();
    $.post("",data,function(res){
        $("#sub_menu_asuransi_content").html(res);	
        $("#sub_menu_asuransi").show("fast");
        $("#main_asuransi").hide("fast");
        dismissLoading();
    });
}

$(document).ready(function(){
    $(".mydate").datepicker();
    $(".mydatetime").datetimepicker({minuteStep:1});    
    $('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
    lap_asuransi_asuransi=new TableAction("lap_asuransi_asuransi","kasir","lap_asuransi",Array());
    lap_asuransi_asuransi.setSuperCommand("lap_asuransi_asuransi");
    lap_asuransi_asuransi.selected=function(json){
        $("#lap_asuransi_asuransi").val(json.nama);
    };

    var column   = new Array("id","nilai","terklaim","waktu","keterangan","no_bukti","tgl_klaim");
    lap_asuransi = new TableAction("lap_asuransi","kasir","lap_asuransi",column);
    lap_asuransi.loading_kasir=function(id){
        var data        = this.getRegulerData();
        data['command'] = "edit";
        data['id']      = id;
        showLoading();
        $.post("",data,function(res){
            var json    = getContent(res);
            var noreg   = json.noreg_pasien;
            load_registration_asuransi(noreg,json);
            dismissLoading();
        });
    };

    
    lap_asuransi.getRegulerData = function(){
        var reg_data = {	
                page            : this.page,
                action          : this.action,
                super_command   : this.super_command,
                prototype_name  : this.prototype_name,
                prototype_slug  : this.prototype_slug,
                dari            : $("#"+this.prefix+"_dari").val(),
                sampai          : $("#"+this.prefix+"_sampai").val(),
                dari_masuk      : $("#"+this.prefix+"_dari_masuk").val(),
                sampai_masuk    : $("#"+this.prefix+"_sampai_masuk").val(),
                dari_klaim      : $("#"+this.prefix+"_dari_klaim").val(),
                sampai_klaim    : $("#"+this.prefix+"_sampai_klaim").val(),
                cair            : $("#"+this.prefix+"_k_cair").val(),
                asuransi        : $("#"+this.prefix+"_asuransi").val(),
                filter_origin   : $("#"+this.prefix+"_filter_origin").val(),
                operator        : $("#"+this.prefix+"_operator").val(),
                jenis           : $("#"+this.prefix+"_jenis").val(),
                nrm             : $("#"+this.prefix+"_nrm").val(),
                nama            : $("#"+this.prefix+"_nama").val()
            };
        var dr  = getFormattedTime(reg_data['dari']);
        var sp  = getFormattedTime(reg_data['sampai']);
        var hsl = dr+" - "+sp;
        $("#tgl_lap_asuransi_print").html(hsl);
        var x = $("#lap_asuransi_jenis option:selected").text();
        $("#lap_asuransi_jenisx").html(x);
        return reg_data;
    };
    
    lap_asuransi.loop_post = function(total,number){
		if(number>=total){
			smis_loader.hideLoader();
			this.view();
			return ;
		}
		var id               = parseInt($("#"+this.prefix+"_list").children().eq(number).children().eq(6).children().eq(0).html());
		var nama             = $("#"+this.prefix+"_list").children().eq(number).children().eq(5).html();
		var data             = this.getRegulerData();
        data['action']       = "push_to_accounting";
        data['noreg_pasien'] = id;
        var self             = this;
        $.post("",data,function(res){
			smis_loader.updateLoader("true","Loading..."+nama+" [ "+(number+1)+" / "+total+" ]",(number)*100/total);
			self.loop_post(total,number+1)
		});
	};
    
    lap_asuransi.post_to_all = function(){
        var total = $("#"+this.prefix+"_list").children().length-1;		
		smis_loader.showLoader();
		this.loop_post(total,0);
    };
    
    lap_asuransi.push_to_accounting = function(id){
        var data        = this.getRegulerData();
        data['id']      = id;
        data['command'] = "edit";
        showLoading();
        $.post("",data,function(res){
            var json             = getContent(res);
            data['action']       = "push_to_accounting";
            data['noreg_pasien'] = json.noreg_pasien;
            showLoading();
            $.post("",data,function(res){
                dismissLoading();
                smis_alert("Success","Data Notified !!!","alert-info");
            });
            dismissLoading();            
        });
    };

    

    lap_asuransi.get_date = function(){
        var dateObj = new Date();
        var month = dateObj.getUTCMonth() + 1; //months from 1-12
        var day = dateObj.getUTCDate();
        var year = dateObj.getUTCFullYear();
        
        var newdate = year + "-" + (month<10?"0"+month:month) + "-" + day;
        return newdate;
    };

    lap_asuransi.cair   = function(id){
        var data              = this.getRegulerData();
        data['id']            = id;
        data['terklaim']      = "1";
        data['tgl_klaim']     = this.get_date();
        data['command']       = "save";
        showLoading();
        $.post("",data,function(res){
            var json          = getContent(res);
            lap_asuransi.view();
            dismissLoading();            
        });
    };

    /**masalah pelunasan asuransi */
    lap_asuransi.push_to_lunas = function(id){
        var data        = this.getRegulerData();
        data['id']      = id;
        data['action']  = "push_to_lunas";
        showLoading();
        $.post("",data,function(res){
            var json             = getContent(res);
            smis_alert("Success","Data Notified !!!","alert-info");
            dismissLoading();            
        });
    };

    lap_asuransi.push_to_lunas_all     = function(){        
        var list_id     = new Array();
        $( "#lap_asuransi_list .push_to_lunas_button" ).each(function(e) {
            var x       = $(this).data("id-lunas");
            list_id.push(x);
        });
        smis_loader.updateLoader('true',"Processing... ",0);
        smis_loader.showLoader();
        var data        = this.getRegulerData();
        data['action']  = "push_to_lunas";
        this.proceed_loop_lunas(list_id,data,0);
    };

    lap_asuransi.proceed_loop_lunas = function(list_id,data,current){
        if(!smis_loader.isShown() || current>=list_id.length){
            smis_loader.hideLoader();
            lap_asuransi.view();
            return;
        }        
        data['id']      = list_id[current];
        current++;
        $.post("",data,function(res){
            smis_loader.updateLoader('true',"Processing... "+current+" / "+list_id.length,current*100/list_id.length);
            lap_asuransi.proceed_loop_lunas(list_id,data,current);
        });
    };
});