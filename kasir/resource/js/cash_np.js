var cash_np;
$(document).ready(function(){
    $('.mydatetime').datetimepicker({ minuteStep: 1});
    $('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
    var column=new Array('id','waktu','nilai','keterangan');
    cash_np=new TableAction("cash_np","kasir","cash_np",column);    
    cash_np.getRegulerData=function(){
        var reg_data = {	
                page            : this.page,
                action          : this.action,
                super_command   : this.super_command,
                prototype_name  : this.prototype_name,
                prototype_slug  : this.prototype_slug,
                metode          : "cash_np",
                id_np           : $("#tagihan_non_pasien_parent_id").val(),
                nama_pasien     : $("#tagihan_non_pasien_parent_nama").val()
            };
        return reg_data;
    };
});