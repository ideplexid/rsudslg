var bank_resep;
$(document).ready(function(){
    $('.mydatetime').datetimepicker({ minuteStep: 1});
    $('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
    $("#bank_resep_persen, #bank_resep_nilai").on("change",function(){
        var nilai=Number(getMoney("#bank_resep_nilai"));
        var persen=Number($("#bank_resep_persen").val());
        var uang=nilai*persen/100;
        setMoney("#bank_resep_nilai_tambahan",uang);
    });
    
    var column=new Array('id','waktu','nilai','keterangan','no_resep','nilai_tambahan','di_bank','id_bank','no_bukti',"carabayar");
    bank_resep=new TableAction("bank_resep","kasir","bank_resep",column);
    bank_resep.postAction=function(id){
        reload_resep();
    };
    bank_resep.getRegulerData=function(){
        var reg_data = {	
                page            : this.page,
                action          : this.action,
                super_command   : this.super_command,
                prototype_name  : this.prototype_name,
                prototype_slug  : this.prototype_slug,
                metode          : "bank_resep",
                id_resep        : $("#"+this.prefix+"_id_resep").val(),
                nama_pasien     : $("#"+this.prefix+"_nama_pasien").val(),
                nrm_pasien      : $("#"+this.prefix+"_nrm_pasien").val(),
                noreg_pasien    : $("#"+this.prefix+"_noreg_pasien").val(),
                ruangan         : $("#ruang").val(),
                nama_bank       : $("#"+this.prefix+"_id_bank option:selected").text(),
            };
        return reg_data;
    };
    bank_resep.view();	
});