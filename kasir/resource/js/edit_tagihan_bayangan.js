var edit_tagihan_bayangan;
var IS_edit_tagihan_bayangan_RUNNING;
var px_edit_tagihan_bayangan;
$(document).ready(function(){
	var column=new Array("id","ruangan","id_unit","nama_tagihan","nama_grup","jenis_tagihan","kelas_by","nama_by","ruang_by","keterangan","tanggal","hidden","jaspel_by","nilai","nilai_by");
	edit_tagihan_bayangan=new TableAction("edit_tagihan_bayangan","kasir","edit_tagihan_bayangan",column);
	edit_tagihan_bayangan.setSuperCommand("edit_tagihan_bayangan");
	edit_tagihan_bayangan.addRegulerData=function(j){     
        j['nama_pasien']		= $("#edit_tagihan_bayangan_nama_pasien").val();
		j['nrm_pasien']			= $("#edit_tagihan_bayangan_nrm_pasien").val();
		j['noreg_pasien']		= $("#edit_tagihan_bayangan_noreg_pasien").val();
		j['noreg_pasien']		= $("#edit_tagihan_bayangan_noreg_pasien").val();
		j['hidden_all']			= $("#edit_tagihan_bayangan_hidden_all").val();
		j['jaspel_by_all']		= $("#edit_tagihan_bayangan_jaspel_by_all").val();
		j['nilai_by_all']		= $("#edit_tagihan_bayangan_nilai_by_all").val();
		j['kelas_by_all']		= $("#edit_tagihan_bayangan_kelas_by_all").val();
		j['jenis_tagihan_all']	= $("#edit_tagihan_bayangan_jenis_tagihan_all").val();
		j['ruang_by_all']		= $("#edit_tagihan_bayangan_ruang_by_all").val();
		j['ruang_asal_by_all']	= $("#edit_tagihan_bayangan_ruang_asal_by_all").val();
		j['nama_by_all']		= $("#edit_tagihan_bayangan_nama_by_all").val();
		j['namaruang_by_all']	= $("#edit_tagihan_bayangan_namaruang_by_all").val();
		return j;
	};
	edit_tagihan_bayangan.jaspel=function(id){
		var j=this.getRegulerData();
		j['command']="jaspel";
		j['id']=id;
		showLoading();
		$.post("",j,function(res){
			var json=getContent(res);
			edit_tagihan_bayangan.view();
			dismissLoading();
		});
	};
	
	edit_tagihan_bayangan.back=function(id){
		var j=this.getRegulerData();
		j['command']="back";
		j['id']=id;
		showLoading();
		$.post("",j,function(res){
			var json=getContent(res);
			edit_tagihan_bayangan.view();
			dismissLoading();
		});
	};
	
	edit_tagihan_bayangan.hidding=function(id){
		var j=this.getRegulerData();
		j['command']="hidden";
		j['id']=id;
		showLoading();
		$.post("",j,function(res){
			var json=getContent(res);
			edit_tagihan_bayangan.view();
			dismissLoading();
		});
	};
	
	edit_tagihan_bayangan.one=function(all,index,total){		
		$("#rekap_edit_tagihan_bayangan_bar").sload("true","Processing... [ "+index+" / "+total+" ] ",(index*100/total));		
		var j=this.getRegulerData();
		j['command']="load_one";
		j['id']=all[index];
		$.post("",j,function(res){
			index++;
			if(index==total){
				$("#rekap_edit_tagihan_bayangan_modal").smodal("hide");
				edit_tagihan_bayangan.view();
			}else{
				setTimeout(function(){ edit_tagihan_bayangan.one(all,index,total);}, 1000);
			}			
		});
	};
	
	edit_tagihan_bayangan.loadAll=function(){
		var j=this.getRegulerData();
		$("#rekap_edit_tagihan_bayangan_bar").sload("true","Fetching All Data",0);
		$("#rekap_edit_tagihan_bayangan_modal").smodal("show");
		j['command']="load_all";
		$.post("",j,function(res){
			var json=getContent(res);
			var total=json.length;
			if(total>0){
				edit_tagihan_bayangan.one(json,0,total);				
			}else{
				$("#rekap_edit_tagihan_bayangan_modal").smodal("hide");
				edit_tagihan_bayangan.view();
			}
		});
	};
	
	$("#edit_tagihan_bayangan_nilai_by_all, #edit_tagihan_bayangan_nama_by_all, #edit_tagihan_bayangan_namaruang_by_all , #edit_tagihan_bayangan_hidden_all, #edit_tagihan_bayangan_jaspel_by_all").on("change",function(){
		var tarif=$("#edit_tagihan_bayangan_nilai_by_all").val();
		var hidden=$("#edit_tagihan_bayangan_hidden_all").val();
		var jaspel=$("#edit_tagihan_bayangan_jaspel_by_all").val();
		var nama=$("#edit_tagihan_bayangan_nama_by_all").val();
		var nama_ruang=$("#edit_tagihan_bayangan_namaruang_by_all").val();
		
		$("#edit_tagihan_bayangan_jenis_tagihan_all").prop("disabled",true);
		if(tarif!="Biarkan" || hidden!="Biarkan" || jaspel!="Biarkan" || nama!="Biarkan" || nama_ruang!="Biarkan"){
			$("#edit_tagihan_bayangan_jenis_tagihan_all").prop("disabled",false);
		}
		
		$("#edit_tagihan_bayangan_kelas_by_all").prop("disabled",true);
		$("#edit_tagihan_bayangan_ruang_by_all").prop("disabled",true);
		
		if(tarif=="Ubah" || nama=="Ubah" || nama_ruang=="Ubah"){
			$("#edit_tagihan_bayangan_kelas_by_all").prop("disabled",false);
			$("#edit_tagihan_bayangan_ruang_by_all").prop("disabled",false);
		}
	});
	
	$("#edit_tagihan_bayangan_kelas_by,#edit_tagihan_bayangan_ruang_by").on("change",function(){
		var v=edit_tagihan_bayangan.getSaveData();
		v['id']=$("#edit_tagihan_bayangan_id").val();
		v['command']="edit_tagihan_bayangan_nilai";
		showLoading();
		$.post("",v,function(res){
			var j=getContent(res);
			if( j['harga']!="-1" && j['harga']!=-1){
				setMoney("#edit_tagihan_bayangan_nilai_by",j['harga']);
				$("#edit_tagihan_bayangan_nama_by").val(j['nama']);
			}
			dismissLoading();
		});
	});
	edit_tagihan_bayangan.view();
	
    px_edit_tagihan_bayangan=new TableAction("px_edit_tagihan_bayangan","kasir","edit_tagihan_bayangan",column);
	px_edit_tagihan_bayangan.setSuperCommand("px_edit_tagihan_bayangan");
	px_edit_tagihan_bayangan.selected=function(json){
		$("#edit_tagihan_bayangan_nama_pasien").val(json.nama_pasien);
		$("#edit_tagihan_bayangan_alamat_pasien").val(json.alamat_pasien);
		$("#edit_tagihan_bayangan_noreg_pasien").val(json.id);
		$("#edit_tagihan_bayangan_nrm_pasien").val(json.nrm);
		$("#edit_tagihan_bayangan_carabayar").val(json.carabayar);
		edit_tagihan_bayangan.view();
	};
});