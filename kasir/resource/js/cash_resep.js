var cash_resep;
$(document).ready(function(){
    $('.mydatetime').datetimepicker({ minuteStep: 1});
    $('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
    var column=new Array('id','waktu','nilai','keterangan','no_resep',"carabayar");
    cash_resep=new TableAction("cash_resep","kasir","cash_resep",column);
    cash_resep.postAction=function(id){
        reload_resep();
    };
            
    cash_resep.getRegulerData=function(){
        var reg_data = {	
                page            : this.page,
                action          : this.action,
                super_command   : this.super_command,
                prototype_name  : this.prototype_name,
                prototype_slug  : this.prototype_slug,
                metode          : "cash_resep",
                id_resep        : $("#"+this.prefix+"_id_resep").val(),
                nama_pasien     : $("#"+this.prefix+"_nama_pasien").val(),
                nrm_pasien      : $("#"+this.prefix+"_nrm_pasien").val(),
                ruangan         : $("#ruang").val(),
                noreg_pasien    : $("#"+this.prefix+"_noreg_pasien").val()
            };
        return reg_data;
    };
    cash_resep.view();	
});