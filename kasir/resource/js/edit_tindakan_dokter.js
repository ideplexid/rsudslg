var edit_tindakan_dokter;
var px_edit_tindakan_dokter;
var dokter_edit_tindakan_dokter;
var tarif_keperawatan_dokter;

var perawat_edit_tindakan_dokter;
var perawat_edit_tindakan_dokter_dua;
var perawat_edit_tindakan_dokter_tiga;	
var perawat_edit_tindakan_dokter_empat;
var perawat_edit_tindakan_dokter_lima;	
var perawat_edit_tindakan_dokter_enam;	
var perawat_edit_tindakan_dokter_tujuh;	
var perawat_edit_tindakan_dokter_delapan;	
var perawat_edit_tindakan_dokter_sembilan;	
var perawat_edit_tindakan_dokter_sepuluh;	

var NUMBER_PERAWAT_dokter;
var NUMBER_PERAWAT_dokter_TABLE_ACTION;
function reload_tarif_dokter(next){
	var jumlah=Number($("#edit_tindakan_dokter_jumlah").val());
	var satuan=getMoney("#edit_tindakan_dokter_satuan");
	var total=jumlah*satuan;
	if($("#edit_tindakan_dokter_jumlah").attr("type")=="text" ){
		$("#edit_tindakan_dokter_harga_tindakan").maskMoney('mask',total);
	}else{
		$("#edit_tindakan_dokter_harga_tindakan").val(total);
	}
	
	if(next=="jumlah" && $("#edit_tindakan_dokter_jumlah").attr("type")=="text" ){
		$("#edit_tindakan_dokter_jumlah").focus();
	}else if(next=="jumlah" ){
		$("#edit_tindakan_dokter_save").focus();
	}
}

$(document).ready(function(){
    
    NUMBER_PERAWAT_dokter_TABLE_ACTION=new Array();
	NUMBER_PERAWAT_dokter=new Array();
	NUMBER_PERAWAT_dokter[1]="";
	NUMBER_PERAWAT_dokter[2]="_dua";
	NUMBER_PERAWAT_dokter[3]="_tiga";
	NUMBER_PERAWAT_dokter[4]="_empat";
	NUMBER_PERAWAT_dokter[5]="_lima";
	NUMBER_PERAWAT_dokter[6]="_enam";
	NUMBER_PERAWAT_dokter[7]="_tujuh";
	NUMBER_PERAWAT_dokter[8]="_delapan";
	NUMBER_PERAWAT_dokter[9]="_sembilan";
	NUMBER_PERAWAT_dokter[10]="_sepuluh";
    
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
    $('.mydatetime').datetimepicker({ minuteStep: 1});
    
    $("#edit_tindakan_dokter_jumlah").on("keyup",function(e){
		reload_tarif_dokter("save");
	});

	$("#edit_tindakan_dokter_jumlah").on("change",function(e){
		reload_tarif_dokter("save");
	});
    
	var column=new Array(
			'id','id_dokter','nama_dokter','jenis_dokter',
            'nama_tindakan','harga_perawat',"nama_pasien",
			"noreg_pasien","nrm_pasien",'waktu','harga');
     for(var i=1;i<=NUMBER_PERAWAT_dokter.length;i++){
		var num=NUMBER_PERAWAT_dokter[i];
		column.push("nama_perawat"+num);
		column.push("id_perawat"+num);
	}		
       
	edit_tindakan_dokter=new TableAction("edit_tindakan_dokter","kasir","edit_tindakan_dokter",column);
    edit_tindakan_dokter.addNoClear("nama_pasien");
    edit_tindakan_dokter.addNoClear("noreg_pasien");
    edit_tindakan_dokter.addNoClear("nrm_pasien");
    edit_tindakan_dokter.addNoClear("carabayar");
    edit_tindakan_dokter.addNoClear("ruang");
    
	edit_tindakan_dokter.addRegulerData=function(a){
		a['noreg_pasien']=$("#edit_tindakan_dokter_noreg_pasien").val();
		a['nrm_pasien']=$("#edit_tindakan_dokter_nrm_pasien").val();
		a['nama_pasien']=$("#edit_tindakan_dokter_nama_pasien").val();
		a['carabayar']=$("#edit_tindakan_dokter_carabayar").val();
		a['ruang']=$("#edit_tindakan_dokter_ruang").val();
        a['header']=$("#edit_tindakan_dokter_header_element").val();
		return a;
	};
	edit_tindakan_dokter.show_add_form=function(){
		if($("#edit_tindakan_dokter_noreg_pasien").val()==""){
			showWarning("Peringatan","Silakan Pilih Pasien Dahulu");
			return;
		}
		this.clear();
		this.show_form();
	};	
    
    edit_tindakan_dokter.beforesave=function(){
		if($("#edit_tindakan_dokter_nama_dokter").val()==""){
			$("#edit_tindakan_dokter_id_dokter").val(0);
		}
		for(var i=1;i<=NUMBER_PERAWAT_dokter.length;i++){
			var num=NUMBER_PERAWAT_dokter[i];
			if($("#edit_tindakan_dokter_nama_perawat"+num).val()==""){
				$("#edit_tindakan_dokter_id_perawat"+num).val(0);
			}
		}		
	};
    
    edit_tindakan_dokter.reload_view=function(){
        var d=list_registered.getRegulerData();
        d['super_command']="edit_tindakan_dokter";
        d['ruangan']=$("#edit_tindakan_dokter_ruang").val();
        d['noreg_pasien']=$("#noreg_pasien").val();
        showLoading();
        $.post("",d,function (res){
            $("#tindakan_dokter").html(res);
            dismissLoading();
        });
    }
    
	px_edit_tindakan_dokter=new TableAction("px_edit_tindakan_dokter","kasir","get_registered_patient",column);
	px_edit_tindakan_dokter.setSuperCommand("px_edit_tindakan_dokter");
	px_edit_tindakan_dokter.selected=function(json){
		$("#edit_tindakan_dokter_nama_pasien").val(json.nama_pasien);
		$("#edit_tindakan_dokter_alamat_pasien").val(json.alamat_pasien);
		$("#edit_tindakan_dokter_noreg_pasien").val(json.id);
		$("#edit_tindakan_dokter_nrm_pasien").val(json.nrm);
		$("#edit_tindakan_dokter_carabayar").val(json.carabayar);
		edit_tindakan_dokter.view();
	};
    
    dokter_edit_tindakan_dokter=new TableAction("dokter_edit_tindakan_dokter","kasir","get_employee",new Array());
	dokter_edit_tindakan_dokter.setSuperCommand("dokter_edit_tindakan_dokter");
    dokter_edit_tindakan_dokter.setOwnChooserData(true);
	dokter_edit_tindakan_dokter.addRegulerData=function(a){
        a['filter']="dokter";
        return a;
    };
    dokter_edit_tindakan_dokter.selected=function(json){
		var nama=json.nama;
		var nip=json.id;		
		$("#edit_tindakan_dokter_nama_dokter").val(nama);
		$("#edit_tindakan_dokter_id_dokter").val(nip);
	};

	stypeahead("#edit_tindakan_dokter_nama_dokter",3,dokter_edit_tindakan_dokter,"nama",function(item){
		$("#edit_tindakan_dokter_id_dokter").val(item.id);
		$("#edit_tindakan_dokter_nama_dokter").val(item.nama);
		$("#edit_tindakan_dokter_nama_perawat").focus();
	});
    
    tarif_keperawatan_dokter=new TableAction("tarif_keperawatan_dokter","kasir","edit_tindakan_dokter",new Array());
	tarif_keperawatan_dokter.setSuperCommand("tarif_keperawatan_dokter");
	tarif_keperawatan_dokter.addRegulerData=function(d){
		d['noreg_pasien']=$("#edit_tindakan_dokter_noreg_pasien").val();
		return d;
	};
	tarif_keperawatan_dokter.selected=function(json){
		var kelas=json.kelas;
		var nama=json.nama;
		var harga=Number(json.tarif);
		$("#edit_tindakan_dokter_kelas").val(kelas);
		$("#edit_tindakan_dokter_nama_tindakan").val(nama);
		$("#edit_tindakan_dokter_satuan").maskMoney('mask',harga);
		reload_tarif_dokter("jumlah");
	};
	stypeahead("#edit_tindakan_dokter_nama_tindakan",3,tarif_keperawatan_dokter,"nama",function(item){
		$("#edit_tindakan_dokter_id_tindakan").val(item.id);
		$("#edit_tindakan_dokter_kelas").val(item.kelas);
		$("#edit_tindakan_dokter_nama_tindakan").val(item.name);
		$("#edit_tindakan_dokter_satuan").maskMoney('mask',Number(item.tarif));
		reload_tarif_dokter("jumlah");
	});
    
    
    for(var i=1;i<=NUMBER_PERAWAT_dokter.length;i++){
		var num=NUMBER_PERAWAT_dokter[i];
		var ptpi=new TableAction("perawat_edit_tindakan_dokter"+num,"kasir","get_employee",new Array());
			ptpi.numero=num;
			ptpi.setSuperCommand("perawat_edit_tindakan_dokter"+ptpi.numero);
            ptpi.setOwnChooserData(true);
            ptpi.addRegulerData=function(a){
                a['filter']="perawat";
                return a;
            };
			ptpi.selected=function(json){
				var nama=json.nama;
				var nip=json.id;
				$("#edit_tindakan_dokter_nama_perawat"+this.numero).val(nama);
				$("#edit_tindakan_dokter_id_perawat"+this.numero).val(nip);
			};
		$("#edit_tindakan_dokter_nama_perawat"+num).data("numero",num);
		$("#edit_tindakan_dokter_nama_perawat"+num).data("i",i);
		stypeahead("#edit_tindakan_dokter_nama_perawat"+num,3,ptpi,"nama",function(item,id){
			var numero=$(id).data("numero");
			var the_i=Number($(id).data("i"));
			$("#edit_tindakan_dokter_id_perawat"+numero).val(item.id);
			$("#edit_tindakan_dokter_nama_perawat"+numero).val(item.nama);
			if(the_i<NUMBER_PERAWAT_dokter.length){
				var next_num=NUMBER_PERAWAT_dokter[the_i+1];
				if($("#edit_tindakan_dokter_nama_perawat"+next_num).attr("type")=="hidden"){
					$("#edit_tindakan_dokter_nama_tindakan").focus();
				}else{
					$("#edit_tindakan_dokter_nama_perawat"+next_num).focus();
				}
			}
		});
		NUMBER_PERAWAT_dokter_TABLE_ACTION[i]=ptpi;
			
	}		
	
	/*STRAT NAMA PERAWAT*/
	perawat_edit_tindakan_dokter=NUMBER_PERAWAT_dokter_TABLE_ACTION[1];
	perawat_edit_tindakan_dokter_dua=NUMBER_PERAWAT_dokter_TABLE_ACTION[2];
	perawat_edit_tindakan_dokter_tiga=NUMBER_PERAWAT_dokter_TABLE_ACTION[3];
	perawat_edit_tindakan_dokter_empat=NUMBER_PERAWAT_dokter_TABLE_ACTION[4];
	perawat_edit_tindakan_dokter_lima=NUMBER_PERAWAT_dokter_TABLE_ACTION[5];
	perawat_edit_tindakan_dokter_enam=NUMBER_PERAWAT_dokter_TABLE_ACTION[6];
	perawat_edit_tindakan_dokter_tujuh=NUMBER_PERAWAT_dokter_TABLE_ACTION[7];
	perawat_edit_tindakan_dokter_delapan=NUMBER_PERAWAT_dokter_TABLE_ACTION[8];
	perawat_edit_tindakan_dokter_sembilan=NUMBER_PERAWAT_dokter_TABLE_ACTION[9];
	perawat_edit_tindakan_dokter_sepuluh=NUMBER_PERAWAT_dokter_TABLE_ACTION[10];
	/*END NAMA PERAWAT*/
    
    
    tarif_edit_tindakan_dokter=new TableAction("tarif_edit_tindakan_dokter","kasir","edit_tindakan_dokter",new Array());
	tarif_edit_tindakan_dokter.setSuperCommand("tarif_edit_tindakan_dokter");
	tarif_edit_tindakan_dokter.addRegulerData=function(d){
		d['noreg_pasien']=$("#tindakan_dokter_noreg_pasien").val();
		d['carabayar']=$("#edit_tindakan_dokter_carabayar").val();
		return d;
	};
	tarif_edit_tindakan_dokter.selected=function(json){
		$("#edit_tindakan_dokter_nama_dokter").val(json.nama_dokter);
		$("#edit_tindakan_dokter_id_dokter").val(json.id_dokter);
		setMoney("#edit_tindakan_dokter_harga",Number(json.tarif));
		setMoney("#edit_tindakan_dokter_harga_perawat",Number(json.perawat));
		$("#edit_tindakan_dokter_nama_tindakan").val(json.nama);
	};
	stypeahead("#edit_tindakan_dokter_nama_tindakan",3,tarif_edit_tindakan_dokter,"nama",function(item){
		$("#edit_tindakan_dokter_nama_dokter").val(item.nama_dokter);
		$("#edit_tindakan_dokter_id_dokter").val(item.id_dokter);
		setMoney("#edit_tindakan_dokter_harga",Number(item.tarif));
		setMoney("#edit_tindakan_dokter_harga_perawat",Number(item.perawat));
		$("#edit_tindakan_dokter_nama_perawat").focus();
	});

	$("#edit_tindakan_dokter_ruang").change(function(){
		edit_tindakan_dokter.reload_view();
	});
        
	if($("#edit_tindakan_dokter_noreg_pasien").val()!=""){
        var noreg_pasien=$("#edit_tindakan_dokter_noreg_pasien").val();  
        px_edit_tindakan_dokter.select(noreg_pasien);
    }
	
});