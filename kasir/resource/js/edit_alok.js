var edit_alok;
var px_edit_alok;
var tarif_alok;
$(document).ready(function(){
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
    $(".mydatetime").datetimepicker({minuteStep:1});
	var column=new Array('id','tanggal','nama','satuan','kode','harga','jumlah');
	edit_alok=new TableAction("edit_alok","kasir","edit_alok",column);
	edit_alok.addRegulerData=function(a){
		a['noreg_pasien']=$("#edit_alok_noreg_pasien").val();
		a['nrm_pasien']=$("#edit_alok_nrm_pasien").val();
		a['nama_pasien']=$("#edit_alok_nama_pasien").val();
		a['carabayar']=$("#edit_alok_carabayar").val();
		a['ruangan']=$("#edit_alok_ruang").val();
		return a;
	};
    
    edit_alok.addNoClear("nama_pasien");
    edit_alok.addNoClear("noreg_pasien");
    edit_alok.addNoClear("nrm_pasien");
    edit_alok.addNoClear("carabayar");
    edit_alok.addNoClear("ruangan");    
	edit_alok.show_add_form=function(){
		if($("#edit_alok_noreg_pasien").val()==""){
			showWarning("Peringatan","Silakan Pilih Pasien Dahulu");
			return;
		}
		this.clear();
		this.show_form();
	};
	
	px_edit_alok=new TableAction("px_edit_alok","kasir","get_registered_patient",column);
	px_edit_alok.setSuperCommand("px_edit_alok");
	px_edit_alok.selected=function(json){
		$("#edit_alok_nama_pasien").val(json.nama_pasien);
		$("#edit_alok_alamat_pasien").val(json.alamat_pasien);
		$("#edit_alok_noreg_pasien").val(json.id);
		$("#edit_alok_nrm_pasien").val(json.nrm);
		$("#edit_alok_carabayar").val(json.carabayar);
		edit_alok.view();
	};
    
    tarif_alok=new TableAction("tarif_alok","kasir","edit_alok",new Array());
	tarif_alok.setSuperCommand("tarif_alok");
    tarif_alok.addRegulerData=function(a){
		a['ruangan']=$("#edit_alok_ruang").val();
		return a;
	};
	tarif_alok.selected=function(json){
		$("#edit_alok_nama").val(json.nama);
		$("#edit_alok_satuan").val(json.satuan);
		$("#edit_alok_kode").val(json.kode);
		$("#edit_alok_harga").maskMoney('mask',Number(json.harga) );
	};

	stypeahead("#edit_alok_nama",3,tarif_alok,"nama",function(item){
		$("#edit_alok_nama").val(item.nama);
		$("#edit_alok_satuan").val(item.satuan);
		$("#edit_alok_kode").val(item.kode);
		$("#edit_alok_harga").maskMoney('mask',Number(item.harga) );	
		$("#edit_alok_jumlah").focus();				
	});
	

	$("#edit_alok_ruang").change(function(){        
        edit_alok.view();
	});
    
    if($("#edit_alok_noreg_pasien").val()!=""){
        var noreg_pasien=$("#edit_alok_noreg_pasien").val();  
        px_edit_alok.select(noreg_pasien);
    }
	
});