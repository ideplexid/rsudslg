var edit_alok_connected;
var px_edit_alok_connected;
var tarif_alok_connected;
$(document).ready(function(){
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
    $(".mydatetime").datetimepicker({minuteStep:1});
	var column=new Array('id','id_obat','tanggal','nama','satuan','kode','harga','jumlah',"sisa");
	edit_alok_connected=new TableAction("edit_alok_connected","kasir","edit_alok_connected",column);
	edit_alok_connected.addRegulerData=function(a){
		a['noreg_pasien']=$("#edit_alok_connected_noreg_pasien").val();
		a['nrm_pasien']=$("#edit_alok_connected_nrm_pasien").val();
		a['nama_pasien']=$("#edit_alok_connected_nama_pasien").val();
		a['carabayar']=$("#edit_alok_connected_carabayar").val();
		a['ruangan']=$("#edit_alok_connected_ruang").val();
		return a;
	};
    
    edit_alok_connected.addNoClear("nama_pasien");
    edit_alok_connected.addNoClear("noreg_pasien");
    edit_alok_connected.addNoClear("nrm_pasien");
    edit_alok_connected.addNoClear("carabayar");
    edit_alok_connected.addNoClear("ruangan");    
	edit_alok_connected.show_add_form=function(){
		if($("#edit_alok_connected_noreg_pasien").val()==""){
			showWarning("Peringatan","Silakan Pilih Pasien Dahulu");
			return;
		}
		this.clear();
		this.show_form();
	};
	
	px_edit_alok_connected=new TableAction("px_edit_alok_connected","kasir","get_registered_patient",column);
	px_edit_alok_connected.setSuperCommand("px_edit_alok_connected");
	px_edit_alok_connected.selected=function(json){
		$("#edit_alok_connected_nama_pasien").val(json.nama_pasien);
		$("#edit_alok_connected_alamat_pasien").val(json.alamat_pasien);
		$("#edit_alok_connected_noreg_pasien").val(json.id);
		$("#edit_alok_connected_nrm_pasien").val(json.nrm);
		$("#edit_alok_connected_carabayar").val(json.carabayar);
		edit_alok_connected.view();
	};
    
    tarif_alok_connected=new TableAction("tarif_alok_connected","kasir","edit_alok_connected",new Array());
	tarif_alok_connected.setSuperCommand("tarif_alok_connected");
    tarif_alok_connected.addRegulerData=function(a){
		a['ruangan']=$("#edit_alok_connected_ruang").val();
		return a;
	};
	tarif_alok_connected.selected=function(json){
		$("#edit_alok_connected_sisa").val(json.sisa);
		$("#edit_alok_connected_id_obat").val(json.id_obat);
		$("#edit_alok_connected_nama").val(json.nama);
		$("#edit_alok_connected_satuan").val(json.satuan);
		$("#edit_alok_connected_kode").val(json.kode);
		$("#edit_alok_connected_harga").maskMoney('mask',Number(json.harga) );
	};

	stypeahead("#edit_alok_connected_nama",3,tarif_alok_connected,"nama",function(item){
		$("#edit_alok_connected_nama").val(item.nama);
		$("#edit_alok_connected_satuan").val(item.satuan);
		$("#edit_alok_connected_kode").val(item.kode);
		$("#edit_alok_connected_harga").maskMoney('mask',Number(item.harga) );	
		$("#edit_alok_connected_jumlah").focus();				
	});
	

	$("#edit_alok_connected_ruang").change(function(){        
        edit_alok_connected.view();
	});
    
    if($("#edit_alok_connected_noreg_pasien").val()!=""){
        var noreg_pasien=$("#edit_alok_connected_noreg_pasien").val();  
        px_edit_alok_connected.select(noreg_pasien);
    }
	
});