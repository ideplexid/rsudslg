var edit_konsul;
var px_edit_konsul;
var dokter_konsul;
var dokter_konsul_dua;
var dokter_konsul_tiga;
var tarif_konsul;
$(document).ready(function(){
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
    $('.mydatetime').datetimepicker({ minuteStep: 1});
	var column=new Array(
			'id',"nama_pasien","noreg_pasien","nrm_pasien",
			'waktu','kelas','harga',
			'nama_dokter','id_dokter',
			'nama_dokter_dua','id_dokter_dua',
			'nama_dokter_tiga','id_dokter_tiga',
			"nama_konsul","id_konsul");
            
	edit_konsul=new TableAction("edit_konsul","kasir","edit_konsul",column);
    edit_konsul.addNoClear("nama_pasien");
    edit_konsul.addNoClear("noreg_pasien");
    edit_konsul.addNoClear("nrm_pasien");
    edit_konsul.addNoClear("carabayar");
    edit_konsul.addNoClear("ruang");
    
	edit_konsul.addRegulerData=function(a){
		a['noreg_pasien']=$("#edit_konsul_noreg_pasien").val();
		a['nrm_pasien']=$("#edit_konsul_nrm_pasien").val();
		a['nama_pasien']=$("#edit_konsul_nama_pasien").val();
		a['carabayar']=$("#edit_konsul_carabayar").val();
		a['ruang']=$("#edit_konsul_ruang").val();
        a['header']=$("#edit_konsul_header_element").val();
		return a;
	};
	edit_konsul.show_add_form=function(){
		if($("#edit_konsul_noreg_pasien").val()==""){
			showWarning("Peringatan","Silakan Pilih Pasien Dahulu");
			return;
		}
		this.clear();
		this.show_form();
	};	
	px_edit_konsul=new TableAction("px_edit_konsul","kasir","get_registered_patient",column);
	px_edit_konsul.setSuperCommand("px_edit_konsul");
    px_edit_konsul.selected=function(json){
		$("#edit_konsul_nama_pasien").val(json.nama_pasien);
		$("#edit_konsul_alamat_pasien").val(json.alamat_pasien);
		$("#edit_konsul_noreg_pasien").val(json.id);
		$("#edit_konsul_nrm_pasien").val(json.nrm);
		$("#edit_konsul_carabayar").val(json.carabayar);
        edit_konsul.view();
	};
	dokter_konsul=new TableAction("dokter_konsul","kasir","get_employee",column);
	dokter_konsul.setSuperCommand("dokter_konsul");
    dokter_konsul.setOwnChooserData(true);
	dokter_konsul.addRegulerData=function(a){
        a['filter']="dokter";
        return a;
    };
	dokter_konsul.selected=function(json){
		$("#edit_konsul_id_dokter").val(json.id);
		$("#edit_konsul_nama_dokter").val(json.nama);
	};
    
    
    
    edit_konsul.reload_view=function(){
        var d=list_registered.getRegulerData();
        d['super_command']="edit_konsul";
        d['ruangan']=$("#edit_konsul_ruang").val();
        d['noreg_pasien']=$("#noreg_pasien").val();
        showLoading();
        $.post("",d,function (res){
            $("#konsul").html(res);
            dismissLoading();
        });
    }
    
    tarif_konsul=new TableAction("tarif_konsul","kasir","edit_konsul",new Array());
	tarif_konsul.setSuperCommand("tarif_konsul");
	tarif_konsul.addRegulerData=function(d){
		d['noreg_pasien']=$("#edit_konsul_noreg_pasien").val();
        d['separated']=$("#is_konsul_separated").val();
		return d;
	};
	tarif_konsul.selected=function(json){
		var nama=json.nama_dokter;
		var id_dokter=json.id_dokter;		
		var kelas=json.kelas;
		var harga=Number(json.tarif);
		if($("#is_konsul_separated").val()!="1"){
            $("#edit_konsul_nama_dokter").val(nama);
			$("#edit_konsul_id_dokter").val(id_dokter);
		}				
		
		$("#edit_konsul_nama_konsul").val(json.nama_konsul);
		$("#edit_konsul_id_konsul").val(json.id);
		$("#edit_konsul_kelas").val(kelas);
		$("#edit_konsul_harga").maskMoney('mask',harga);
		
	};

	$("#edit_konsul_ruang").change(function(){
		edit_konsul.reload_view();
	});
    
    if($("#edit_konsul_noreg_pasien").val()!=""){
        var noreg_pasien=$("#edit_konsul_noreg_pasien").val();
        px_edit_konsul.select(noreg_pasien);
    }
    
});