function detail_tagihan_print(){
	var content=$("#print_area_tagihan").html();
	//var content=$("#kwitansi_html").html();
	//var noref=$("#nokwi").html();
	var data={
			nama_pasien:$("#nama_pasien").val(),
			noreg_pasien:$("#noreg_pasien").val(),
			nrm_pasien:$("#nrm_pasien").val(),
			noref:$("#noreg_pasien").val()+"-"+(new Date().toLocaleString()),
			jenis:"Tagihan Kasir",
			html:content,
			html_print:content,
			};
	if(typeof kwitansi_simpan == 'function')
		kwitansi_simpan(data);
    else smis_print(content);
	//smis_print_save(d, "kasir", "tagihan_kasir");
}

function detail_tagihan_print_bayangan(){
	var content=$("#print_area_tagihan_bayangan").html();
	//var content=$("#kwitansi_html").html();
	//var noref=$("#nokwi").html();
	var data={
			nama_pasien:$("#nama_pasien").val(),
			noreg_pasien:$("#noreg_pasien").val(),
			nrm_pasien:$("#nrm_pasien").val(),
			noref:$("#noreg_pasien").val()+"-"+(new Date().toLocaleString()),
			jenis:"Tagihan Bayangan Kasir",
			html:content,
			html_print:content,
			};
	if(typeof kwitansi_simpan == 'function')
		kwitansi_simpan(data);
	else smis_print(content);
	//smis_print_save(d, "kasir", "tagihan_kasir");
}

function detail_tagihan_show(){
	$(".tagihan_keterangan").removeClass("hide").removeClass("noprint");
}

function detail_tagihan_hide(){
	$(".tagihan_keterangan").addClass("hide").addClass("noprint");
}

function detail_tagihan_fetree(){
	$("#table_detail_tagihan").addClass("smis-print-table table table-bordered table-hover table-condensed ");
}

function detail_tagihan_simple(){
	$("#table_detail_tagihan").removeClass("smis-print-table table table-bordered table-hover table-condensed ");
}


function detail_tagihan_border(){
	$("#table_detail_tagihan").addClass("border_table_kasir").removeClass("noborder_table_kasir");
}


function detail_tagihan_noborder(){
	$("#table_detail_tagihan").removeClass("border_table_kasir").addClass("noborder_table_kasir");
}
