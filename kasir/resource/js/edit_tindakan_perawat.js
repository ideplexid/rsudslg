var edit_tindakan_perawat;
var px_edit_tindakan_perawat;
var tarif_keperawatan_perawat;

var perawat_edit_tindakan_perawat;
var perawat_edit_tindakan_perawat_dua;
var perawat_edit_tindakan_perawat_tiga;	
var perawat_edit_tindakan_perawat_empat;
var perawat_edit_tindakan_perawat_lima;	
var perawat_edit_tindakan_perawat_enam;	
var perawat_edit_tindakan_perawat_tujuh;	
var perawat_edit_tindakan_perawat_delapan;	
var perawat_edit_tindakan_perawat_sembilan;	
var perawat_edit_tindakan_perawat_sepuluh;	

var NUMBER_PERAWAT;
var NUMBER_PERAWAT_TABLE_ACTION;
function reload_tarif(next){
	var jumlah=Number($("#edit_tindakan_perawat_jumlah").val());
	var satuan=getMoney("#edit_tindakan_perawat_satuan");
	var total=jumlah*satuan;
	if($("#edit_tindakan_perawat_jumlah").attr("type")=="text" ){
		$("#edit_tindakan_perawat_harga_tindakan").maskMoney('mask',total);
	}else{
		$("#edit_tindakan_perawat_harga_tindakan").val(total);
	}
	
	if(next=="jumlah" && $("#edit_tindakan_perawat_jumlah").attr("type")=="text" ){
		$("#edit_tindakan_perawat_jumlah").focus();
	}else if(next=="jumlah" ){
		$("#edit_tindakan_perawat_save").focus();
	}
}

$(document).ready(function(){
    
    NUMBER_PERAWAT_TABLE_ACTION=new Array();
	NUMBER_PERAWAT=new Array();
	NUMBER_PERAWAT[1]="";
	NUMBER_PERAWAT[2]="_dua";
	NUMBER_PERAWAT[3]="_tiga";
	NUMBER_PERAWAT[4]="_empat";
	NUMBER_PERAWAT[5]="_lima";
	NUMBER_PERAWAT[6]="_enam";
	NUMBER_PERAWAT[7]="_tujuh";
	NUMBER_PERAWAT[8]="_delapan";
	NUMBER_PERAWAT[9]="_sembilan";
	NUMBER_PERAWAT[10]="_sepuluh";
    
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
    $('.mydatetime').datetimepicker({ minuteStep: 1});
    
    $("#edit_tindakan_perawat_jumlah").on("keyup",function(e){
		reload_tarif("save");
	});

	$("#edit_tindakan_perawat_jumlah").on("change",function(e){
		reload_tarif("save");
	});
    
	var column=new Array(
			'id','waktu','kelas','harga_tindakan',
			'nama_tindakan','id_tindakan',
			"jumlah","satuan",
            "jaspel","jaspel_lain_lain","jaspel_penunjang");
     for(var i=1;i<=NUMBER_PERAWAT.length;i++){
		var num=NUMBER_PERAWAT[i];
		column.push("nama_perawat"+num);
		column.push("id_perawat"+num);
	}		
       
	edit_tindakan_perawat=new TableAction("edit_tindakan_perawat","kasir","edit_tindakan_perawat",column);
    edit_tindakan_perawat.addNoClear("nama_pasien");
    edit_tindakan_perawat.addNoClear("noreg_pasien");
    edit_tindakan_perawat.addNoClear("nrm_pasien");
    edit_tindakan_perawat.addNoClear("carabayar");
    edit_tindakan_perawat.addNoClear("ruang");
    
	edit_tindakan_perawat.addRegulerData=function(a){
		a['noreg_pasien']=$("#edit_tindakan_perawat_noreg_pasien").val();
		a['nrm_pasien']=$("#edit_tindakan_perawat_nrm_pasien").val();
		a['nama_pasien']=$("#edit_tindakan_perawat_nama_pasien").val();
		a['carabayar']=$("#edit_tindakan_perawat_carabayar").val();
		a['ruang']=$("#edit_tindakan_perawat_ruang").val();
        a['header']=$("#edit_tindakan_perawat_header_element").val();
		return a;
	};
	edit_tindakan_perawat.show_add_form=function(){
		if($("#edit_tindakan_perawat_noreg_pasien").val()==""){
			showWarning("Peringatan","Silakan Pilih Pasien Dahulu");
			return;
		}
		this.clear();
		this.show_form();
	};	
    
    edit_tindakan_perawat.beforesave=function(){
		if($("#edit_tindakan_perawat_nama_dokter").val()==""){
			$("#edit_tindakan_perawat_id_dokter").val(0);
		}
		for(var i=1;i<=NUMBER_PERAWAT.length;i++){
			var num=NUMBER_PERAWAT[i];
			if($("#edit_tindakan_perawat_nama_perawat"+num).val()==""){
				$("#edit_tindakan_perawat_id_perawat"+num).val(0);
			}
		}		
	};
    
    edit_tindakan_perawat.reload_view=function(){
        var d=list_registered.getRegulerData();
        d['super_command']="edit_tindakan_perawat";
        d['ruangan']=$("#edit_tindakan_perawat_ruang").val();
        d['noreg_pasien']=$("#noreg_pasien").val();
        showLoading();
        $.post("",d,function (res){
            $("#tindakan_perawat").html(res);
            dismissLoading();
        });
    }
    
	px_edit_tindakan_perawat=new TableAction("px_edit_tindakan_perawat","kasir","get_registered_patient",column);
	px_edit_tindakan_perawat.setSuperCommand("px_edit_tindakan_perawat");
	px_edit_tindakan_perawat.selected=function(json){
		$("#edit_tindakan_perawat_nama_pasien").val(json.nama_pasien);
		$("#edit_tindakan_perawat_alamat_pasien").val(json.alamat_pasien);
		$("#edit_tindakan_perawat_noreg_pasien").val(json.id);
		$("#edit_tindakan_perawat_nrm_pasien").val(json.nrm);
		$("#edit_tindakan_perawat_carabayar").val(json.carabayar);
		edit_tindakan_perawat.view();
	};
    
    
    tarif_keperawatan_perawat=new TableAction("tarif_keperawatan_perawat","kasir","edit_tindakan_perawat",new Array());
	tarif_keperawatan_perawat.setSuperCommand("tarif_keperawatan_perawat");
	tarif_keperawatan_perawat.addRegulerData=function(d){
		d['noreg_pasien']=$("#edit_tindakan_perawat_noreg_pasien").val();
		return d;
	};
	tarif_keperawatan_perawat.selected=function(json){
		var kelas=json.kelas;
		var nama=json.nama;
		var harga=Number(json.tarif);
		$("#edit_tindakan_perawat_kelas").val(kelas);
		$("#edit_tindakan_perawat_nama_tindakan").val(nama);
		$("#edit_tindakan_perawat_satuan").maskMoney('mask',harga);
        $("#edit_tindakan_perawat_jaspel").val(json.jaspel);
        $("#edit_tindakan_perawat_jaspel_lain_lain").val(json.lain_lain);
        $("#edit_tindakan_perawat_jaspel_penunjang").val(json.penunjang);
		reload_tarif("jumlah");
	};
	stypeahead("#edit_tindakan_perawat_nama_tindakan",3,tarif_keperawatan_perawat,"nama",function(item){
		$("#edit_tindakan_perawat_id_tindakan").val(item.id);
		$("#edit_tindakan_perawat_kelas").val(item.kelas);
		$("#edit_tindakan_perawat_nama_tindakan").val(item.name);
		$("#edit_tindakan_perawat_satuan").maskMoney('mask',Number(item.tarif));
        $("#edit_tindakan_perawat_jaspel").val(item.jaspel);
        $("#edit_tindakan_perawat_jaspel_lain_lain").val(item.lain_lain);
        $("#edit_tindakan_perawat_jaspel_penunjang").val(item.penunjang);
		reload_tarif("jumlah");
	});
    
    
    for(var i=1;i<=NUMBER_PERAWAT.length;i++){
		var num=NUMBER_PERAWAT[i];
		var ptpi=new TableAction("perawat_edit_tindakan_perawat"+num,"kasir","get_employee",new Array());
			ptpi.numero=num;
			ptpi.setSuperCommand("perawat_edit_tindakan_perawat"+ptpi.numero);
            ptpi.setOwnChooserData(true);
            ptpi.addRegulerData=function(a){
                a['filter']="perawat";
                return a;
            };
			ptpi.selected=function(json){
				var nama=json.nama;
				var nip=json.id;
				$("#edit_tindakan_perawat_nama_perawat"+this.numero).val(nama);
				$("#edit_tindakan_perawat_id_perawat"+this.numero).val(nip);
			};
		$("#edit_tindakan_perawat_nama_perawat"+num).data("numero",num);
		$("#edit_tindakan_perawat_nama_perawat"+num).data("i",i);
		stypeahead("#edit_tindakan_perawat_nama_perawat"+num,3,ptpi,"nama",function(item,id){
			var numero=$(id).data("numero");
			var the_i=Number($(id).data("i"));
			$("#edit_tindakan_perawat_id_perawat"+numero).val(item.id);
			$("#edit_tindakan_perawat_nama_perawat"+numero).val(item.nama);
			if(the_i<NUMBER_PERAWAT.length){
				var next_num=NUMBER_PERAWAT[the_i+1];
				if($("#edit_tindakan_perawat_nama_perawat"+next_num).attr("type")=="hidden"){
					$("#edit_tindakan_perawat_nama_tindakan").focus();
				}else{
					$("#edit_tindakan_perawat_nama_perawat"+next_num).focus();
				}
			}
		});
		NUMBER_PERAWAT_TABLE_ACTION[i]=ptpi;
			
	}		
	
	/*STRAT NAMA PERAWAT*/
	perawat_edit_tindakan_perawat=NUMBER_PERAWAT_TABLE_ACTION[1];
	perawat_edit_tindakan_perawat_dua=NUMBER_PERAWAT_TABLE_ACTION[2];
	perawat_edit_tindakan_perawat_tiga=NUMBER_PERAWAT_TABLE_ACTION[3];
	perawat_edit_tindakan_perawat_empat=NUMBER_PERAWAT_TABLE_ACTION[4];
	perawat_edit_tindakan_perawat_lima=NUMBER_PERAWAT_TABLE_ACTION[5];
	perawat_edit_tindakan_perawat_enam=NUMBER_PERAWAT_TABLE_ACTION[6];
	perawat_edit_tindakan_perawat_tujuh=NUMBER_PERAWAT_TABLE_ACTION[7];
	perawat_edit_tindakan_perawat_delapan=NUMBER_PERAWAT_TABLE_ACTION[8];
	perawat_edit_tindakan_perawat_sembilan=NUMBER_PERAWAT_TABLE_ACTION[9];
	perawat_edit_tindakan_perawat_sepuluh=NUMBER_PERAWAT_TABLE_ACTION[10];
	/*END NAMA PERAWAT*/
    
	$("#edit_tindakan_perawat_ruang").change(function(){
		edit_tindakan_perawat.reload_view();
	});
        
	if($("#edit_tindakan_perawat_noreg_pasien").val()!=""){
        var noreg_pasien=$("#edit_tindakan_perawat_noreg_pasien").val();  
        px_edit_tindakan_perawat.select(noreg_pasien);
    }
	
});