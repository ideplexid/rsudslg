/**
 * this used for viewing the recapitulation of
 * additional tax, for patient that temporary 
 * used for helping cashier administration control
 * add some cash bill that not yet inputed by user
 * 
 * @version     : 5.0.0
 * @since       : 02 Feb 2017
 * @author      : Nurul Huda
 * @copyright   : goblooge@gmail.com
 * @license     : LGPLv3
 * @used        : kasir/resource/php/laporan/rekap_tambahan_biaya.php
 * */
var rekap_tambahan_biaya;
$(document).ready(function(){
    $(".mydate").datepicker();
    $('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
    var column=new Array('dari','sampai');
    rekap_tambahan_biaya=new TableAction("rekap_tambahan_biaya","kasir","rekap_tambahan_biaya",column);
    rekap_tambahan_biaya.addViewData=function(data){
        data['dari']=$("#"+this.prefix+"_dari").val();
        data['sampai']=$("#"+this.prefix+"_sampai").val();
        data['nrm']=$("#"+this.prefix+"_nrm").val();
        data['nama']=$("#"+this.prefix+"_nama").val();
        data['noreg']=$("#"+this.prefix+"_noreg").val();
        return data;
    };
    $("#rekap_tambahan_biaya_nama, #rekap_tambahan_biaya_nrm, #rekap_tambahan_biaya_noreg").keyup(function(e){
         if(e.keyCode==13){
            rekap_tambahan_biaya.view();
         }
    });
});