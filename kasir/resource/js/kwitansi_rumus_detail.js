var kwitansi_rumus_detail;
var rumus_kwitansi_rumus_detail;
$(document).ready(function(){
    var arr=new Array("id","nama_pasien","noreg_pasien","nrm_pasien",
                    "kelas_plafon","kode_inacbg","deskripsi_inacbg",
                    "ruang_pelayanan","id_rumus","nama_rumus","rumus_tagihan",
                    "rumus_biaya","rumus_potongan","tarif_plafon_inacbg","tarif_plafon_kelas1",
                    "total_tagihan","total_biaya","total_potongan");
    kwitansi_rumus_detail=new TableAction("kwitansi_rumus_detail","kasir","kwitansi_rumus_detail",arr);
    kwitansi_rumus_detail.postAction=function(){
        $("#kwitansi_rumus_detail_anchor").trigger("click");
    };
    rumus_kwitansi_rumus_detail=new TableAction("rumus_kwitansi_rumus_detail","kasir","kwitansi_rumus_detail",new Array());
    rumus_kwitansi_rumus_detail.setSuperCommand("rumus_kwitansi_rumus_detail");
    rumus_kwitansi_rumus_detail.selected=function(json){
        $("#kwitansi_rumus_detail_id_rumus").val(json.id);
        $("#kwitansi_rumus_detail_nama_rumus").val(json.nama);
        $("#kwitansi_rumus_detail_rumus_biaya").val(json.rumus_biaya);
        $("#kwitansi_rumus_detail_rumus_tagihan").val(json.rumus_tagihan);
        $("#kwitansi_rumus_detail_rumus_potongan").val(json.rumus_potongan);
        $("#kwitansi_rumus_detail_deskripsi_inacbg").val(json.keterangan);
    };
});