/**
 * this sistem used to resume
 * all income that incoming (even or not)
 * in given timeframe
 * 
 * @author      : Nurul Huda
 * @copyright   : goblooge@gmail.com
 * @since       : 17 Aug 2016
 * @used        : kasir/resource/php/laporan/piutang_per_ruang.php
 * @version     : 1.6.0
 * @license     : LGPLv3
 * */

var piutang_per_ruang;
var piutang_per_ruang_karyawan;
var piutang_per_ruang_data;
var IS_piutang_per_ruang_RUNNING;
$(document).ready(function(){
    $('.mydate').datepicker();
    piutang_per_ruang=new TableAction("piutang_per_ruang","kasir","piutang_per_ruang",new Array());
    piutang_per_ruang.addRegulerData=function(data){
        data['dari']=$("#piutang_per_ruang_dari").val();
        data['sampai']=$("#piutang_per_ruang_sampai").val();
        return data;
    };

    piutang_per_ruang.batal=function(){
        IS_piutang_per_ruang_RUNNING=false;
        $("#rekap_piutang_per_ruang_modal").modal("hide");
    };
    
    piutang_per_ruang.afterview=function(json){
        if(json!=null){
            $("#kode_table_piutang_per_ruang").html(json.nomor);
            $("#waktu_table_piutang_per_ruang").html(json.waktu);
            piutang_per_ruang_data=json;
        }
    };

    piutang_per_ruang.rekaptotal=function(){
        if(IS_piutang_per_ruang_RUNNING) return;
        $("#rekap_piutang_per_ruang_bar").sload("true","Fetching total data",0);
        $("#rekap_piutang_per_ruang_modal").modal("show");
        IS_piutang_per_ruang_RUNNING=true;
        var d=this.getRegulerData();
        d['super_command']="total";
        $.post("",d,function(res){
            var all=getContent(res);
            if(all!=null) {
                var total=all.length;
                piutang_per_ruang.rekaploop(0,all,total);
            } else {
                $("#rekap_piutang_per_ruang_modal").modal("hide");
                IS_piutang_per_ruang_RUNNING=false;
            }
        });
    };

    piutang_per_ruang.rekaploop=function(current,all,total){
        if(current>=total || !IS_piutang_per_ruang_RUNNING) {
            $("#rekap_piutang_per_ruang_modal").modal("hide");
            IS_piutang_per_ruang_RUNNING=false;
            piutang_per_ruang.view();
            return;
        }
        var one_patient=all[current];
        $("#rekap_piutang_per_ruang_bar").sload("true",one_patient['nama_pasien']+"... [ "+(current+1)+" / "+total+" ] ",(current*100/total));
        var d=this.getRegulerData();
        d['super_command']="limit";
        d['noreg_pasien']=one_patient['id'];
        d['akunting_notify_date']=one_patient['akunting_notify_date'];		
        $.post("",d,function(res){
            var ct=getContent(res);
            setTimeout(function(){piutang_per_ruang.rekaploop(++current,all,total)},300);
        });
    };
});