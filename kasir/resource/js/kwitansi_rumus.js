var kwitansi_rumus;
var rumus_kwitansi_rumus;
$(document).ready(function(){
    var arr=new Array("id","nama_pasien","noreg_pasien","nrm_pasien",
                    "kelas_plafon","kode_inacbg","deskripsi_inacbg",
                    "ruang_pelayanan","id_rumus","nama_rumus","rumus_tagihan",
                    "rumus_biaya","rumus_potongan","tarif_plafon_inacbg","tarif_plafon_kelas1",
                    "total_tagihan","total_biaya","total_potongan");
    kwitansi_rumus=new TableAction("kwitansi_rumus","kasir","kwitansi_rumus",arr);
    kwitansi_rumus.postAction=function(){
        $("#kwitansi_rumus_anchor").trigger("click");
    };
    rumus_kwitansi_rumus=new TableAction("rumus_kwitansi_rumus","kasir","kwitansi_rumus",new Array());
    rumus_kwitansi_rumus.setSuperCommand("rumus_kwitansi_rumus");
    rumus_kwitansi_rumus.selected=function(json){
        $("#kwitansi_rumus_id_rumus").val(json.id);
        $("#kwitansi_rumus_nama_rumus").val(json.nama);
        $("#kwitansi_rumus_rumus_biaya").val(json.rumus_biaya);
        $("#kwitansi_rumus_rumus_tagihan").val(json.rumus_tagihan);
        $("#kwitansi_rumus_rumus_potongan").val(json.rumus_potongan);
        $("#kwitansi_rumus_deskripsi_inacbg").val(json.keterangan);
    };
});