function cek_kwitansi_print(name){
    if(!is_selesai_semua()){
        var title="Peringatan Kesalahan !!!";
        var text="Kwitansi Tidak Dapat di cetak karena masih Terdapat Ruangan Yang Belum Memulangkan dari Ruangan </br>";
        text+="<table class='table table-bordered table-hover table-striped table-condensed' >"+$("#table_layanan_table").html()+"</table>";
        showWarning(title,text);
    }else{
        kwitansi_print(name);
    }
}

function kwitansi_print(name){
	$(".ngedit").prop('contenteditable', false );
	var html_content = $("#cetak_"+name+"_area").html();
	var name         = $("#noreg_pasien").val();
	$(".ngedit").prop('contenteditable', true );
    if(typeof kwitansi_simpan == 'function'){
        var data={
			nama_pasien     : $("#nama_pasien").val(),
			noreg_pasien    : $("#noreg_pasien").val(),
			nrm_pasien      : $("#nrm_pasien").val(),
			noref           : $("#noreg_pasien").val()+"-"+(new Date().toLocaleString()),
			jenis           : "Kwitansi Pasien",
			html            : html_content,
			html_print      : html_content,
        };
        kwitansi_simpan(data);
    }else{
        smis_print_save(html_content,"kwitansi_pasien",name);
    }
}