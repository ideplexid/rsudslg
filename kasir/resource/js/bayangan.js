var bayangan;
var IS_BAYANGAN_BACKUP_RUN=false;
$(document).ready(function(){
    var column=new Array('id','ruangan','layanan','tindakan','harga','jaspel');
    bayangan=new TableAction("bayangan","kasir","bayangan",column);
    bayangan.addRegulerData=function(reg_data){
        reg_data["nama"]=$("#nama_pasien").val();
        reg_data["noreg"]=$("#noreg_pasien").val();
        reg_data["nrm"]=$("#nrm_pasien").val();
        return reg_data;
    };

    bayangan.revoke_last=function(){
        if(IS_BAYANGAN_BACKUP_RUN) return;
        bootbox.confirm("Me Reload Bayangan Berarti Menghapus Bayangan Lama dan Mengganti dengan yang Baru ?", function(result) {
              if(result){
                    IS_BAYANGAN_BACKUP_RUN=true;
                    showLoading();
                    var tdata=bayangan.getRegulerData();
                    tdata['command']="revoke";
                    $.post("",tdata,function(res){
                        var json=getContent(res);
                        dismissLoading();
                        if(json=="1"){
                            bayangan.create_bayangan();
                        }else{
                            IS_BAYANGAN_BACKUP_RUN=false;
                            bootbox.confirm("Revoke Gagal, Anda Ingin Tetap Membuat Kwitansi Bayangan", function(result) {
                                  if(result){
                                     bayangan.create_bayangan();
                                  }
                            }); 
                        }				
                    });	  
              }
        });
    }

    bayangan.force_close=function(){
        IS_BAYANGAN_BACKUP_RUN=false;
        $("#bayangan_modal").modal("hide");
        bayangan.view();
    }
    
    bayangan.create_bayangan=function(){
        IS_BAYANGAN_BACKUP_RUN=true;
        $("#bayangan_modal").modal("show");
        var total=$("#print-detail-tagihan tbody tr").length;
        var current=0;
        $("#print-detail-tagihan tbody tr").each(function(index){
            current++;
            var tdata=bayangan.getRegulerData();
            tdata["command"]="save";
            tdata["ruangan"]=$(this).children().eq(0).html();
            tdata["layanan"]=$(this).children().eq(1).html();
            tdata["tindakan"]=$(this).children().eq(2).html();
            tdata["harga"]=$(this).children().eq(3).html().replace(" Rp. ","").unformatMoney();
            tdata["jaspel"]=$(this).children().eq(4).html().replace(" Rp. ","").unformatMoney();
            tdata["keterangan"]=$(this).children().eq(5).html();
            tdata["id"]="";				
            $.ajax({type:'POST',url:"", async: false,data:tdata,success:function(res){}});
            $("#bayangan_bar").sload("true","Processing...[ "+current+" / "+total+" ]",current*100/total);
            return IS_BAYANGAN_BACKUP_RUN;
        });
        $("#bayangan_modal").modal("hide");	
        IS_BAYANGAN_BACKUP_RUN=false;
        bayangan.view();
    };
    bayangan.view();	
});