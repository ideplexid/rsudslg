var list_registered;
var SELESAI_SEMUA		= "";
PEMBAYARAN_URI_URJ		= null;
var INAP				= '0';
var jsonSelected;
var GO_AUTOLOAD			= 0;
var FAKE_LOADER			= 0;

$(document).ready(function(){
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
	var column		= new Array('id','nrm','nama_rujukan');
	list_registered	= new TableAction("list_registered","kasir","list_registered",column);
	list_registered.getRegulerData  = function(){
		var reg_data = {	
					page				: this.page,
					action				: this.action,
					super_command		: this.super_command,
					prototype_name		: this.prototype_name,
					prototype_slug		: this.prototype_slug,
					prototype_implement	: this.prototype_implement
				};
		if(PEMBAYARAN_URI_URJ!=null){
			reg_data['uri']			= PEMBAYARAN_URI_URJ;
		}
		return reg_data;
    };
    list_registered.is_valid = function(){
        var nrm = $("#nrm_pasien").val();
        var noreg = $("#noreg_pasien").val();
        if(noreg.trim()!="" && nrm.trim()!=""){
            return true;
        }
        return false;
    }
	list_registered.selected=function(json){	
        $("#nrm_pasien").val(json.nrm);
		$("#noreg_pasien").val(json.id);
		$("#nama_pasien").val(json.nama_pasien);
		$("#penanggungjawab").val(json.namapenanggungjawab);
		$("#alamat_pasien").val(json.alamat_pasien); 
		$("#umur_pasien").val(json.umur);
		$("#pembayaran").val(json.carabayar);
		$("#asuransi").val(json.nama_asuransi);
		$("#perusahaan").val(json.n_perusahaan);
		$("#ruangan").val(json.jenislayanan);
        $("#jk_pasien").val(json.kelamin);

        if(json.selesai==1){
            showWarning("No Register Non Aktif","No Register "+json.id+" Telah Non Aktif tidak bisa dibuka kembali");
            $("#resume_anchor").trigger("click");
            $("#cash_anchor").parent().addClass("hide");
            $("#bank_anchor").parent().addClass("hide");
            $("#asurant_anchor").parent().addClass("hide");
            $("#discount_anchor").parent().addClass("hide");
            $("#tagihan_backup_inp_anchor").parent().addClass("hide");
            return;
        }
        $("#cash_anchor").parent().removeClass("hide");
        $("#bank_anchor").parent().removeClass("hide");
        $("#asurant_anchor").parent().removeClass("hide");
        $("#discount_anchor").parent().removeClass("hide");
        $("#tagihan_backup_inp_anchor").parent().removeClass("hide");

        list_registered.tombol_tutup(json.tutup_tagihan);		
		$("#layanan_modal").smodal('hide');
		if(json.uri=="1") {
			$("#ruangan").val(json.kamar_inap);
			$("#inap").val("Rawat Inap");
		}else {
			$("#ruangan").val(json.jenislayanan);
			$("#inap").val("Rawat Jalan");
		}
		INAP			= json.uri;
		jsonSelected 	= json;
		if($("#cashier_autoload").val()=="1" || $("#cashier_autoload").val()==1 || GO_AUTOLOAD==1){
			collect_tagihan(json.id,json.nama_pasien,json.nrm,json.asuransi,json.nama_asuransi,json.uri,json.tutup_tagihan);
		}else{
			var id_a=$("#rekapitulasi_tagihan_pasien_header > ul > li.active > a ").attr("id");
			$("#"+id_a).trigger("click");
		}      
		GO_AUTOLOAD		= 0;
	};
	
	list_registered.selecting=function(){
		var noreg=$("#noreg_pasien").val();
		if(noreg==""){
			showWarning("Pilih Pasien","Silakan Pilih Pasien Dahulu");
			return;
		}
		list_registered.select(noreg);
	};
	
	list_registered.autoload=function(noreg){
		GO_AUTOLOAD=1;
		list_registered.select(noreg);
	};

	list_registered.load_input=function(supercommand,id_element){
        if(!list_registered.is_valid()){
            return;
        }
		var d				= list_registered.getRegulerData();
		d['super_command']	= supercommand;
		d['noreg_pasien']	= $("#noreg_pasien").val();
        d['ruangan']		= $("#ruangan").val();
		showLoading();
		$.post("",d,function (res){
			$("#"+id_element).html(res);
			dismissLoading();
		});
	};

	list_registered.load_penunjang=function(supercommand,id_element){
        if(!list_registered.is_valid()){
            return;
        }
		var d				= list_registered.getRegulerData();
		d['super_command']	= supercommand;
		d['noreg_pasien']	= Number($("#noreg_pasien").val());
        d['nama_pasien']	= $("#nama_pasien").val();
		d['nrm_pasien']		= Number($("#nrm_pasien").val());
		d['jk']				= $("#jk_pasien").val();
		showLoading();
		$.post("",d,function (res){
			$("#"+id_element).html(res);
			dismissLoading();
		});
	};

	list_registered.load_kasir=function(supercommand,id_element){
        if(!list_registered.is_valid()){
            return;
        }
		var d				= list_registered.getRegulerData();
		d['super_command']	= supercommand;
		d['noreg_pasien']	= $("#noreg_pasien").val();
		d['noreg']			= $("#noreg_pasien").val();
		d['nama_pasien']	= $("#nama_pasien").val();
        d['nrm_pasien']		= $("#nrm_pasien").val();
        d['ruangan']		= $("#ruangan").val();
		showLoading();
		$.post("",d,function (res){
			$("#"+id_element).html(res);
			dismissLoading();
		});
    };
    
    list_registered.tutup_tagihan = function(status){
        if(!list_registered.is_valid()){
            return;
        }
        var d				= list_registered.getRegulerData();
		d['action']	        = "set_tutup_tagihan";
		d['noreg_pasien']	= $("#noreg_pasien").val();
		d['tutup_tagihan']	= status;
		showLoading();
		$.post("",d,function (res){   
            list_registered.tombol_tutup(status);
            dismissLoading();
		});
    };

    list_registered.tombol_tutup = function(status){
        if(status=="0"){
            $("#tutup_tagihan").removeClass("hide");
            $("#buka_tagihan").addClass("hide");
        }else{
            $("#buka_tagihan").removeClass("hide");
            $("#tutup_tagihan").addClass("hide");
        }
    };

});

function webprint_kwitansi(){
	var data = $.extend(list_registered.getRegulerData(), {		
		'noreg_pasien'		: jsonSelected.id,
		'nrm_pasien'		: jsonSelected.nrm,
		'nama_pasien'		: jsonSelected.nama_pasien,
		'layanan'			: jsonSelected.jenislayanan,
		'jenis_pasien'		: jsonSelected.carabayar,
		'alamat_pasien'		: jsonSelected.alamat_pasien,
		'penanggung_jawab'	: jsonSelected.namapenanggungjawab,
		'perusahaan'		: jsonSelected.n_perusahaan,
		'tanggal_inap'		: jsonSelected.tanggal,
		'tanggal_pulang'	: jsonSelected.tanggal_pulang,
		'rawat_inap'		: jsonSelected.uri,
		'super_command'		: 'webprint_tagihan_global'
	});
	showLoading();
	$.post('', data, function(res){
		var json = JSON.parse(res);
		webprint(json);
		dismissLoading();
	});	
}

function rincian_kwitansi(){
    if(!list_registered.is_valid()){
        return;
    }
	var data = $.extend(list_registered.getRegulerData(), {		
		'noreg_pasien'		: jsonSelected.id,
		'nrm_pasien'		: jsonSelected.nrm,
		'nama_pasien'		: jsonSelected.nama_pasien,
		'layanan'			: jsonSelected.jenislayanan,
		'jenis_pasien'		: jsonSelected.carabayar,
		'alamat_pasien'		: jsonSelected.alamat_pasien,
		'penanggung_jawab'	: jsonSelected.namapenanggungjawab,
		'perusahaan'		: jsonSelected.n_perusahaan,
		'tanggal_inap'		: jsonSelected.tanggal,
		'tanggal_pulang'	: jsonSelected.tanggal_pulang,
		'rawat_inap'		: jsonSelected.uri,
		'super_command'		: 'rincian_kwitansi'
	});		
	postForm(data);
}

function postForm(data) {
	if ($('#form_temp').length == 0) {
		$('body').append('<form id="form_temp" method="post" style="display:none"></form>');
	}
	
	var $form = $('#form_temp');
	$form.empty();
	$.each(data, function(i, val) {
		$('<input type="hidden">').attr('name', i).attr('value', val).appendTo($form);
	});
	$form.submit();
}

function hitung_ulang(){
    if(!list_registered.is_valid()){
        return;
    }
	var data				= list_registered.getRegulerData();
	data['noreg']			= $("#noreg_pasien").val();
	data['noreg_pasien']	= $("#noreg_pasien").val();
	data['nama_pasien']		= $("#nama_pasien").val();
	data['nrm_pasien']		= $("#nrm_pasien").val();
	data['inap']			= INAP;
	data['super_command']	= "resume";
	showLoading();
	$.post("",data,function(res){
        $("#resume_html").html(res);
        $(".mydatetime").datetimepicker({minuteStep:1});
        create_kwitansi(data);
        var dv = $("#cashier_out_checkbox_manual").attr("dv");
        $("#cashier_out_checkbox_manual").val(dv);
        cek_alasan_gratis();

		dismissLoading();
		return;
	});
}

function cek_alasan_gratis(){
    
    var out_check_manual = $("#cashier_out_checkbox_manual option:selected").text();
    if(out_check_manual.trim()=="Gratis"){
        $("#alasan_gratis").removeAttr("disabled");
        //alert("remove "+out_check_manual);
    }else{
        $("#alasan_gratis").val("");
        $("#alasan_gratis").prop('disabled', 'disabled');
        //alert(out_check_manual);
    }
}

function kwitansi_simpan(data){
    if(!list_registered.is_valid()){
        return;
    }
	data['page']		= "kasir";
	data['action']		= "kwitansi_pasien";
	data['command']		= "save";
	data['id']			= "";
	showLoading();
	$.post('',data,function(res){
		var json=getContent(res);
		dismissLoading();
		if(json!=null){
			if ('webprint' in data) {
				webprint(data.webprint);
			} else {
				smis_print(data.html_print);
			}			
		}
	});
}


list_registered.tagihan_kwitansi = function(){
    if(!list_registered.is_valid()){
        return;
    }
	var content					 = $("#kwitansi_html").html();
	var noref					 = $("#nokwi").html();
	var data = {
			nama_pasien			 : $("#nama_pasien").val(),
			noreg_pasien		 : $("#noreg_pasien").val(),
			nrm_pasien			 : $("#nrm_pasien").val(),
			noref				 : noref,
			jenis				 : "Pembayaran Kasir",
			html				 : content,
			html_print			 : content,
		};
	kwitansi_simpan(data, true);
}

list_registered.active_kasir=function(supercommand,id_tab){
	activeTab("#"+id_tab);
	list_registered.load_kasir(supercommand,id_tab);
}


function kwitansi(nomor){
    if(!list_registered.is_valid()){
        return;
    }
	var content	= $("#kwitansi_tagihan").html();
	var data 	= {
			nama_pasien		: $("#nama_pasien").val(),
			noreg_pasien	: $("#noreg_pasien").val(),
			nrm_pasien		: $("#nrm_pasien").val(),
			noref			: nomor,
			jenis			: "Pembayaran Kasir",
			html			: content,
			html_print		: content
		};
	dk_periksa_dokter(data);
}

function buka_kembali(e, polislug,noreg){
	var data				= list_registered.getRegulerData();
	data['noreg_pasien']	= noreg;
	data['polislug']		= polislug;
	data['super_command']	= "aktifkan_ulang";
	SELESAI_SEMUA		   += "0";
	showLoading();
	$.post('',data,function(res){
		$(e).parent().prev().html("<a href='#'>Belum Selesai</a>");
		$(e).parent().html("");
		dismissLoading();
	});
}

function is_selesai_semua(){
	var sls			= SELESAI_SEMUA;
	var selesai		= sls.indexOf("0")==-1;
	return selesai;
}

function pasien_pulang(nilai,sls,noreg){
    if(!list_registered.is_valid()){
        return;
    }
	nilai 				 = getMoney("#sisa_dibayar");
	var urji	 		 = $("#inap").val()=="Rawat Inap"?"uri":"urj";
	var out_check_ruang  = $("#cashier_out_check_"+urji).val();
    var out_check_manual = Number($("#cashier_out_checkbox_manual").val());
    var alasan_gratis    = $("#alasan_gratis").val();
	var can_go			 = 0;
	var selesai			 = sls.indexOf("0")==-1;

	var msg				 = "<ul><li>Anda yakin Memulangkan pasien ini !!, Proses ini Tidak Dapat Dikembalikan</li>";
	if(!selesai){
		msg				+= "<li> Masih Terdapat Ruangan yang belum selesai melakukan layanan, pasien ini tidak dapat dipulangkan</li>";
		can_go=0;
	}else{
		if(nilai>0){
			msg			+= "<li> Pasien Ini Masih Memiliki Hutang Sebesar "+$("#sisa_dibayar").val()+"</li>";
		}else{
			can_go		 = 1;
		}

		if($("#carapulang").val()!="Kabur" && nilai>0){
			msg			+= "<li> Pasien Ini Masih Memiliki Hutang , Jika tetap dipulangkan, cara pulangnya harus Kabur " +
							"atau jika memang dibebaskan dapat melakukan pengecualian dengan memilih Opsi <strong>\"Gratis atau Boleh Pulang Walau Belum Lunas</strong>\" " +
							"jika Opsi Pengecekan pemulangan Tersedia.</li>";
			can_go		 = 0;
		}else if($("#carapulang").val()=="Kabur" && nilai>0 && $("#sisa_dibayar").val()!="0,00"){
			msg			+= "<li> Pasien Ini Memiliki Hutang anda yakin ingin memulangkan (Kabur) </li>";
			msg			+= "<li> Peringatan : Proses Ini Tidak Dapat Dikembalikan </li>";
			can_go		 = 1;
		}
	}
	
	var free_ruang = false;
	if(out_check_ruang=="1" || out_check_ruang==1){
		msg+=" <li> <strong> Peringatan, Mode Pemulangan Tanpa Pengecekan Ruangan di Aktifkan, System Tidak akan Melakukan Pengecekan Pasien ketika pasien di pulangkan !! </strong> </li>";
		free_ruang = true;
	}

    var free_price 		= false;
    //alert(out_check_manual);
	if(out_check_manual>0){
		/**memilih untuk digratiskan */
		msg			   += "<li> Peringatan anda Memilih Opsi <strong>\"Gratis atau Boleh Pulang Walau Belum Lunas</strong>\" Pasien akan dipulangkan Tanpa Melakukan Cek pada Pembayaranya, Pastikan Anda Memilih Dengan Benar </li>";
		if(!free_ruang && !selesai){
			msg		   += "<li> Tidak dapat dikeluarkan karena masih ada ruangan yang aktif</li>";
			free_price 	= false;
		}else{
			free_price 	= true;
        }
        
        if(out_check_manual==1 && alasan_gratis==""){
            //alert("X");
            free_price 	= false;
            msg		   += "<li> <strong>Jika Gratis Harus memilih alasan gratis. silakan pilih alasan gratisnya !!! </strong></li>";
        }else{
            //alert("Y");
            msg		   += "<li> Pasien ini akan dipulangkan <strong> "+$("#cashier_out_checkbox_manual option:selected").text()+" ("+alasan_gratis+") </strong> !!! </li>";
        }
	}
	msg				   += "</ul>";
	
	bootbox.confirm(msg, function(result) {
	   if( 	result && (can_go==1 || free_ruang  || free_price ) ){
		   var data				 			= list_registered.getRegulerData();
		   data['noreg_pasien']	 			= noreg;
		   data['carapulang']	 			= $("#carapulang").val();
		   data['waktu_pulang']  			= $("#waktu_pulang").val();
           data['gratis']		 			= out_check_manual>0?1:0;
           data['opsi_gratis']		 	    = $("#cashier_out_checkbox_manual option:selected").text();
           data['alasan_gratis']		    = alasan_gratis;
		   data['super_command'] 			= "pasien_pulang";
		    $.post('',data,function(res){
				var json					= getContent(res);
				if(json=="ok"){
					LoadSmisPage({
						page				: "kasir",
						action				: "pembayaran_patient",
						prototype_name		: "",
						prototype_slug		: "",
						prototype_implement	: ""
					});
				}
			});
	   }
	}); 
}

function show_detail(id){
	var content = $("#"+id).html();
	showWarning("Detail",content);
}


function create_kwitansi(data){
    if(!list_registered.is_valid()){
        return;
    }
	data['super_command']	= "kwitansi";
	showLoading();
	$.post("",data,function(res){
		$("#kwitansi_html").html(res);
		dismissLoading();
		return;
	});
}

function collect_loop(set,curpoint,noreg,nama,nrm,id_asuransi,nama_asuransi,inap){
	var total				= set.length;
	var data				= list_registered.getRegulerData();
	var current				= set[curpoint];	
	
	/*beritahukan ke user kondisi sekarang dimana*/
	$("#rekap_ksr_bar").sload("true","[ "+curpoint+" / "+total+" ] Collecting From "+current.name+"...",(curpoint*100/total));
	
	data['noreg']			= noreg;
	data['noreg_pasien']	= noreg;
	data['nama_pasien']		= nama;
	data['nrm_pasien']		= nrm;
	data['id_asuransi']		= id_asuransi;
	data['nama_asuransi']	= nama_asuransi;
	data['inap']			= inap;
	data['slug']			= current.value;
	data['super_command']	= "collect_tagihan_unit";
	$.post("",data,function(res){
		var json			= getContent(res);
		$("#layanan_table_list").append(json.row);
		SELESAI_SEMUA+=""+json.selesai;
		
		if(curpoint+1>=total){
			$("#rekap_ksr_modal").modal("hide");
			hitung_ulang();
			var id_a=$("#rekapitulasi_tagihan_pasien_header > ul > li.active > a ").attr("id");
			$("#"+id_a).trigger("click");
            return;
		} else {
			curpoint++;
			setTimeout(function(){
				collect_loop(set,curpoint,noreg,nama,nrm,id_asuransi,nama_asuransi,inap);
			},100);
		}
	});
}

function collect_tagihan(noreg,nama,nrm,id_asuransi,nama_asuransi,inap,tutup_tagihan){
	var data				= list_registered.getRegulerData();
	data['noreg']			= noreg;
	data['noreg_pasien']	= noreg;
	data['nama_pasien']		= nama;
	data['nrm_pasien']		= nrm;
	data['id_asuransi']		= id_asuransi;
	data['nama_asuransi']	= nama_asuransi;
	data['inap']			= inap;
	data['super_command']	= "collect_tagihan";
	data['tutup_tagihan']	= tutup_tagihan;
    
	$("#rekap_ksr_bar").sload("true","Loading Data...",0);
	$("#rekap_ksr_modal").modal("show");
	$("#layanan_table_list").html("");
    FAKE_LOADER				= 1;
	loop_fake_loader(1);
    $.post('',data,function(res){
		var json			= getContent(res);
        FAKE_LOADER			= 0;
		if(json==null){
			return;
		}else{
			SELESAI_SEMUA	= "";
			collect_loop(json,0,noreg,nama,nrm,id_asuransi,nama_asuransi,inap);
		}
	});
}

function loop_fake_loader(number){
    number++;
    if(FAKE_LOADER>0){
        switch(number){
            case 2 : $("#rekap_ksr_bar").sload("true","Finding Data Patient...0%",0); break; 
            case 3 : $("#rekap_ksr_bar").sload("true","Finding Data Patient...50%",0); break;
            case 4 : $("#rekap_ksr_bar").sload("true","Finding Data Patient..100%",0); break;
            case 5 : $("#rekap_ksr_bar").sload("true","Collecting Room..0%",0); break;
            case 6 : $("#rekap_ksr_bar").sload("true","Collecting Room..30%",0); break;
            case 7 : $("#rekap_ksr_bar").sload("true","Collecting Room..76%",0); break;
            case 8 : $("#rekap_ksr_bar").sload("true","Collecting Room..99%",0); break;
            case 9 : $("#rekap_ksr_bar").sload("true","Collecting Room..100%",0); break;
            case 10 : $("#rekap_ksr_bar").sload("true","Filtering Patient Room...10%",0); break;
            case 11 : $("#rekap_ksr_bar").sload("true","Filtering Patient Room...34%",0); break;
            case 12 : $("#rekap_ksr_bar").sload("true","Filtering Patient Room...56%",0); break;
            case 13 : $("#rekap_ksr_bar").sload("true","Filtering Patient Room...85%",0); break;
            case 14 : $("#rekap_ksr_bar").sload("true","Filtering Patient Room...100%",0); break;
            case 15 : $("#rekap_ksr_bar").sload("true","Removing Unused Room....5%",0); break;
            case 16 : $("#rekap_ksr_bar").sload("true","Removing Unused Room....10%",0); break;
            case 17 : $("#rekap_ksr_bar").sload("true","Removing Unused Room....15%",0); break;
            case 18 : $("#rekap_ksr_bar").sload("true","Removing Unused Room....100%",0); break;
            case 19 : $("#rekap_ksr_bar").sload("true","Repackaging Data....18%",0); break;
            case 20 : $("#rekap_ksr_bar").sload("true","Repackaging Data....46%",0); break;
            case 21 : $("#rekap_ksr_bar").sload("true","Repackaging Data....78%",0); break;
            case 22 : $("#rekap_ksr_bar").sload("true","Preparing Data Crawler....30%",0); break;
            case 23 : $("#rekap_ksr_bar").sload("true","Preparing Data Crawler....90%",0); break;
            case 24 : $("#rekap_ksr_bar").sload("true","Preparing Data Crawler....95%",0); break;
            case 25 : $("#rekap_ksr_bar").sload("true","Preparing Data Crawler....100%",0); break;
            case 26 : $("#rekap_ksr_bar").sload("true","Crawler Start Execute.....100%",0); break;
            case 27 : $("#rekap_ksr_bar").sload("true","Network Too Busy....Waiting For Retrying...",0); break;
        }
        setTimeout(function(){
            loop_fake_loader(number);
        },500);
    }
     
}

function loop_collect_data(){
	if(IS_KSR_RUNNING) {
		return;
	}
	$("#rekap_rad_bar").sload("true","Fetching total data",0);
	$("#rekap_rad_modal").modal("show");
	IS_koreksi_pv_dokter_RUNNING = true;
	var d						 = this.getRegulerData();
	d['super_command']			 = "total";
	$.post("",d,function(res){
		var total				 = Number(getContent(res));
		if(total>0) {
			koreksi_pv_dokter.rekaploop(0,total);
		} else {
			$("#rekap_rad_modal").modal("hide");
			IS_koreksi_pv_dokter_RUNNING=false;
		}
	});
};