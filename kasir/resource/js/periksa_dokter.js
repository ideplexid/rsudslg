var periksa_dokter;
var px_periksa_dokter;
var dk_periksa_dokter;
$(document).ready(function(){
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
	var column=new Array("id","waktu","nama_dokter","id_dokter","harga");
	periksa_dokter=new TableAction("periksa_dokter","kasir","periksa_dokter",column);
	periksa_dokter.addRegulerData=function(a){
		a['noreg_pasien']=$("#periksa_dokter_noreg_pasien").val();
		a['nrm_pasien']=$("#periksa_dokter_nrm_pasien").val();
		a['nama_pasien']=$("#periksa_dokter_nama_pasien").val();
		a['carabayar']=$("#periksa_dokter_carabayar").val();
		a['ruang']=$("#periksa_dokter_ruang").val();
		return a;
	};
	periksa_dokter.show_add_form=function(){
		if($("#periksa_dokter_noreg_pasien").val()==""){
			showWarning("Peringatan","Silakan Pilih Pasien Dahulu");
			return;
		}
		this.clear();
		this.show_form();
	};	
	px_periksa_dokter=new TableAction("px_periksa_dokter","kasir","periksa_dokter",column);
	px_periksa_dokter.setSuperCommand("px_periksa_dokter");
	px_periksa_dokter.selected=function(json){
		$("#periksa_dokter_nama_pasien").val(json.nama_pasien);
		$("#periksa_dokter_alamat_pasien").val(json.alamat_pasien);
		$("#periksa_dokter_noreg_pasien").val(json.id);
		$("#periksa_dokter_nrm_pasien").val(json.nrm);
		$("#periksa_dokter_carabayar").val(json.carabayar);
		periksa_dokter.view();
	};
	dk_periksa_dokter=new TableAction("dk_periksa_dokter","kasir","periksa_dokter",column);
	dk_periksa_dokter.setSuperCommand("dk_periksa_dokter");
	dk_periksa_dokter.selected=function(json){
		$("#periksa_dokter_id_dokter").val(json.id);
		$("#periksa_dokter_nama_dokter").val(json.nama);
	};

	$("#periksa_dokter_ruang").change(function(){        
        var data=px_periksa_dokter.getRegulerData();
        data['action']="get_tarif_periksa";
        data['ruangan']=$("#periksa_dokter_ruang").val();
        showLoading();
        $.post("",data,function(res){
            var json=getContent(res);
            $("#periksa_dokter_harga").attr("dv",json);
            periksa_dokter.view();
            dismissLoading();
        });
	});
    
    if($("#periksa_dokter_noreg_pasien").val()!=""){
        var noreg_pasien=$("#periksa_dokter_noreg_pasien").val();  
        px_periksa_dokter.select(noreg_pasien);
    }
	
});