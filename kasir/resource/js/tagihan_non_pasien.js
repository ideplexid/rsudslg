var tagihan_non_pasien;
var dompet_np;
var master_non_pasien;
var master_ambulan;
$(document).ready(function(){
	$(".mydatetime").datetimepicker({minuteStep:1});
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
	var child_column=new Array('id','id_np','nama',"nama2","harga_satuan","jumlah","debet","kredit","unit","total");
	var parent_column=new Array('id','tanggal','nama',"keterangan","operator","id_dompet","nama_dompet","no_akun");
	tagihan_non_pasien=new ParentChildAction("tagihan_non_pasien","kasir","tagihan_non_pasien",parent_column,child_column,'id_np');
	
	tagihan_non_pasien.child.setEnableAutofocus(true);
	tagihan_non_pasien.child.setMultipleInput(true);
	tagihan_non_pasien.child.setNextEnter();
	tagihan_non_pasien.child.setFocusOnMultiple("child_nama");
	tagihan_non_pasien.parent.show_add_form=function(){
		var self=this;		
		bootbox.confirm("Anda yakin Ingin Menambahkan ?", function(result) {
		   if(result){
			   self.clear();
				var a=self.getSaveData();
				showLoading();
				$.ajax({url:"",data:a,type:'post',success:function(res){
					var json=getContent(res);
					if(json==null) return;
					var id_key=json.id;
					self.edit(id_key);
					tagihan_non_pasien.parent.after_form_shown();					
					dismissLoading();
				}});   
		   }
		}); 
	};

	tagihan_non_pasien.child.edit=function (id){
		var self		= this;
		var json_obj	= new Array();
		showLoading();	
		var edit_data	= this.getEditData(id);
		$.post('',edit_data,function(res){		
			var json	= getContent(res);
			if(json==null) return;
			for(var i=0;i<self.column.length;i++){
				if($.inArray(self.column[i],self.noclear)!=-1 && !self.edit_clear_for_no_clear){
					continue;
				}
				var name	= self.column[i];
				var the_id	= "#"+self.prefix+"_"+name;
				if( name in self.json_column && self.json_column.length>0){
					/** this function handling if there is a column that part of another column */
					var json_grup_name=self.json_column[name];
					if(json[json_grup_name]==""){
						continue;
					}
					if(!(json_grup_name in json_obj)){
						json_obj[json_grup_name]=$.parseJSON(json[json_grup_name]);
					}
					smis_edit(the_id,json_obj[json_grup_name][""+name]);  
					
					
				}else{
					/** this function handling reguler column **/
					smis_edit(the_id,json[""+name]);    
				}

				if(name=="unit" && json[""+name]=="ambulan"){
					smis_edit("#"+self.prefix+"_nama2",json["nama"]);    
				}
			}
			dismissLoading();
			self.disabledOnEdit(self.column_disabled_on_edit);
			self.show_form();

			if($("#tagihan_non_pasien_child_unit").val()=="ambulan"){
				$(".fcontainer_tagihan_non_pasien_child_nama").hide();	
				$(".fcontainer_tagihan_non_pasien_child_nama2").show();
			}else{
				$(".fcontainer_tagihan_non_pasien_child_nama").show();
				$(".fcontainer_tagihan_non_pasien_child_nama2").hide();
			}
		});
		return this;
	};

	
	tagihan_non_pasien.parent.edit=function (id){
		var self=this;
		var child=this.children;
		showLoading();	
		var edit_data=this.getEditData(id);
		$.post('',edit_data,function(res){		
			var json=getContent(res);
			if(json==null) return;
			for(var i=0;i<self.column.length;i++){
				if($.inArray(self.column[i],self.noclear)!=-1){
					continue;
				}
					
				var name=self.column[i];
				var the_id="#"+self.prefix+"_"+name;
				smis_edit(the_id,json[""+name]);
			}
			
			$("#"+self.prefix+"_add_form").smodal('show');
			child.view();
			tagihan_non_pasien.parent.after_form_shown();
			dismissLoading();
		});
	};
		
	tagihan_non_pasien.parent.after_form_shown=function(){
		asuransi_np.view();
		bank_np.view();
		cash_np.view();
		$("#tagihan_non_pasien_child_unit").trigger("change");
	};
	
	master_ambulan=new TableAction("master_ambulan","kasir","tagihan_non_pasien",new Array());
	master_ambulan.setSuperCommand("master_ambulan");
	master_ambulan.selected=function(json){
		$("#"+tagihan_non_pasien.child.prefix+"_nama2").val("AMBULAN-"+json.nama);
		setMoney("#"+tagihan_non_pasien.child.prefix+"_harga_satuan",Number(json.biaya_perjalanan));
		setMoney("#"+tagihan_non_pasien.child.prefix+"_total",Number(json.biaya_perjalanan));		
		$("#"+tagihan_non_pasien.child.prefix+"_id_layanan").val(json.id);
		$("#"+tagihan_non_pasien.child.prefix+"_jumlah").val(1);	
        //$("#"+tagihan_non_pasien.child.prefix+"_debet").val(json.debet);
        //$("#"+tagihan_non_pasien.child.prefix+"_kredit").val(json.kredit);
	};

	master_non_pasien=new TableAction("master_non_pasien","kasir","tagihan_non_pasien",new Array());
	master_non_pasien.setSuperCommand("master_non_pasien");
	master_non_pasien.selected=function(json){
		$("#"+tagihan_non_pasien.child.prefix+"_nama").val(json.nama);
		setMoney("#"+tagihan_non_pasien.child.prefix+"_harga_satuan",Number(json.harga));
		setMoney("#"+tagihan_non_pasien.child.prefix+"_total",Number(json.harga));
		$("#"+tagihan_non_pasien.child.prefix+"_id_layanan").val(json.id);
        $("#"+tagihan_non_pasien.child.prefix+"_debet").val(json.debet);
        $("#"+tagihan_non_pasien.child.prefix+"_kredit").val(json.kredit);
		$("#"+tagihan_non_pasien.child.prefix+"_jumlah").val(1);	
	};

	stypeahead("#"+tagihan_non_pasien.child.prefix+"_nama",3,master_non_pasien,"nama",function(json){
		$("#"+tagihan_non_pasien.child.prefix+"_nama").val(json.nama);
		setMoney("#"+tagihan_non_pasien.child.prefix+"_harga_satuan",Number(json.harga));
		setMoney("#"+tagihan_non_pasien.child.prefix+"_total",Number(json.harga));
		$("#"+tagihan_non_pasien.child.prefix+"_jumlah").val(1);	
		$("#"+tagihan_non_pasien.child.prefix+"_id_layanan").val(json.id);	
        $("#"+tagihan_non_pasien.child.prefix+"_debet").val(json.debet);
        $("#"+tagihan_non_pasien.child.prefix+"_kredit").val(json.kredit);
		$("#"+tagihan_non_pasien.child.prefix+"_jumlah").focus();
	});
    
    dompet_np=new TableAction("dompet_np","kasir","tagihan_non_pasien",new Array());
	dompet_np.setSuperCommand("dompet_np");
	dompet_np.selected=function(json){
		$("#"+tagihan_non_pasien.parent.prefix+"_id_dompet").val(json.id);
        $("#"+tagihan_non_pasien.parent.prefix+"_nama_dompet").val(json.nama);
		$("#"+tagihan_non_pasien.parent.prefix+"_no_akun").val(json.nomor_akun);
	};
    
    stypeahead("#"+tagihan_non_pasien.parent.prefix+"_nama_dompet",3,dompet_np,"nama",function(json){
		$("#"+tagihan_non_pasien.parent.prefix+"_id_dompet").val(json.id);
        $("#"+tagihan_non_pasien.parent.prefix+"_nama_dompet").val(json.nama);
		$("#"+tagihan_non_pasien.parent.prefix+"_no_akun").val(json.nomor_akun);
	});
    
    tagihan_non_pasien.parent.view();

	$("#tagihan_non_pasien_child_unit").on("change",function(){
		if($(this).val()=="ambulan"){
			$(".fcontainer_tagihan_non_pasien_child_nama").hide();
			$("#tagihan_non_pasien_child_nama").val("");		
			$(".fcontainer_tagihan_non_pasien_child_nama2").show();
		}else{
			$(".fcontainer_tagihan_non_pasien_child_nama").show();
			$(".fcontainer_tagihan_non_pasien_child_nama2").hide();
			$("#tagihan_non_pasien_child_nama2").val("");
		}
		
		setMoney("#"+tagihan_non_pasien.child.prefix+"_harga_satuan",0);
		setMoney("#"+tagihan_non_pasien.child.prefix+"_total",0);
		$("#tagihan_non_pasien_child_jumlah").val(1);
	});

	$("#tagihan_non_pasien_child_jumlah, #tagihan_non_pasien_child_harga_satuan").on("change",function(){
		var jml = Number($("#tagihan_non_pasien_child_jumlah").val());
		var money = getMoney("#tagihan_non_pasien_child_harga_satuan");
		var total = jml*money;
		setMoney("#"+tagihan_non_pasien.child.prefix+"_total",total);
	});
    
});