var surat_sakit;
var sks_pasien;
var sks_noreg               = $("#sks_noreg_pasien").val();
var sks_nama_pasien         = $("#sks_nama_pasien").val();
var sks_nrm_pasien          = $("#sks_nrm_pasien").val();
var sks_polislug            = $("#sks_polislug").val();
var sks_the_page            = $("#sks_the_page").val();
var sks_the_protoslug       = $("#sks_the_protoslug").val();
var sks_the_protoname       = $("#sks_the_protoname").val();
var sks_the_protoimplement  = $("#sks_the_protoimplement").val();
var sks_id_antrian          = $("#sks_id_antrian").val();
var sks_action              = $("#sks_action").val();

$(document).ready(function(){
    $('.mydatetime').datetimepicker({minuteStep:1});
    $('.mydate').datepicker();
    sks_pasien=new TableAction("sks_pasien",sks_the_page,sks_action,new Array());
    sks_pasien.setSuperCommand("sks_pasien");
    sks_pasien.setPrototipe(sks_the_protoname,sks_the_protoslug,sks_the_protoimplement);
    sks_pasien.selected=function(json){
        var nama=json.nama_pasien;
        var nrm=json.nrm;
        var noreg=json.id;		
        $("#"+sks_action+"_nama_pasien").val(nama);
        $("#"+sks_action+"_nrm_pasien").val(nrm);
        $("#"+sks_action+"_noreg_pasien").val(noreg);
    };

    dokter_asa=new TableAction("dokter_asa",sks_the_page,sks_action,new Array());
    dokter_asa.setSuperCommand("dokter_asa");
    dokter_asa.setPrototipe(sks_the_protoname,sks_the_protoslug,sks_the_protoimplement);
    dokter_asa.selected=function(json){
        $("#"+sks_action+"_nama_dokter").val(json.nama);
        $("#"+sks_action+"_id_dokter").val(json.id);
    };

    stypeahead("#"+sks_action+"_dokter_asa",3,dokter_asa,"nama",function(item){
        $("#"+sks_action+"_nama_dokter").val(json.nama);
        $("#"+sks_action+"_id_dokter").val(json.id);			
    });
    
    var column=new Array(
        "id","nama_pasien","noreg_pasien","nrm_pasien",
        "kondisi","dari","sampai","jk","umur",
        "ruangan","carabayar","id_dokter","nama_dokter",
        "tanggal","perusahaan"
    );

    surat_sakit=new TableAction(sks_action,sks_the_page,sks_action,column);
    surat_sakit.setPrototipe(sks_the_protoname,sks_the_protoslug,sks_the_protoimplement);
    surat_sakit.getRegulerData=function(){
        var reg_data = {	
            page                : this.page,
            action              : this.action,
            super_command       : this.super_command,
            prototype_name      : this.prototype_name,
            prototype_slug      : this.prototype_slug,
            prototype_implement : this.prototype_implement,
            polislug            : sks_polislug,
            noreg_pasien        : sks_noreg,
            nama_pasien         : sks_nama_pasien,
            nrm_pasien          : sks_nrm_pasien,
            id_antrian          : sks_id_antrian
        };
        return reg_data;
    };
    
    surat_sakit.setNextEnter();
    surat_sakit.setEnableAutofocus(true);
    window[sks_action]=surat_sakit;
    if(sks_action=="kasir"){
        surat_sakit.view();
    }else{
        surat_sakit.clear=function(){return;};
    };
    
    surat_sakit.cetak=function(){
      var id=$("#surat_sakit_id").val();
      surat_sakit.printelement(id);
    };
    
    surat_sakit.postAction=function(id){
        $("#surat_sakit_id").val(id);
    };
    
});