/**
 * this sistem used for create a report
 * of several patient that already go home 
 * on given time frame.
 * 
 * @author      : Nurul Huda
 * @copyright   : goblooge@gmail.com
 * @since       : 17 Aug 2016
 * @used        : kasir/resource/php/laporan/laporan_uang_masuk.php
 * @version     : 1.6.0
 * @license     : LGPLv3
 * */
 
var laporan_uang_masuk;
$(document).ready(function(){
    $(".mydate").datepicker();
	laporan_uang_masuk=new TableAction("laporan_uang_masuk","kasir","laporan_uang_masuk",new Array());
	laporan_uang_masuk.addRegulerData=function(a){
        a['dari']=$("#laporan_uang_masuk_dari").val();
        a['sampai']=$("#laporan_uang_masuk_sampai").val();
        a['filter_uang']=$("#laporan_uang_masuk_filter_uang").val();
        a['origin']=$("#laporan_uang_masuk_origin").val();
        return a;
    };
    laporan_uang_masuk.view();
	laporan_uang_masuk.clear=function(){
		var d=this.getViewData();
		d['super_command']="clear";
		d['carapulang']=$("#laporan_uang_masuk_carapulang").val();
		var self=this;
		showLoading();
		$.post("",d,function(res){
			var json=getContent(res);
			if(json==null) {
				
			}else{
				$("#"+self.prefix+"_list").html(json.list);
				$("#"+self.prefix+"_pagination").html(json.pagination);	
			}
			self.afterview(json);
			dismissLoading();
		});
	};
	
});