var asuransi_np;
var asuransi_np_asuransi;
var asuransi_np_perusahaan;
$(document).ready(function(){
    $('.mydatetime').datetimepicker({ minuteStep: 1});
    $('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
    $("#asuransi_np_persen, #asuransi_np_nilai").on("change",function(){
        var nilai=Number(getMoney("#asuransi_np_nilai"));
        var persen=Number($("#asuransi_np_persen").val());
        var uang=nilai*persen/100;
        setMoney("#asuransi_np_nilai_tambahan",uang);
    });

    asuransi_np_asuransi=new TableAction("asuransi_np_asuransi","kasir","asuransi_np",Array());
    asuransi_np_asuransi.setSuperCommand("asuransi_np_asuransi");
    asuransi_np_asuransi.selected=function(json){
        $("#asuransi_np_nama_asuransi").val(json.nama);
        $("#asuransi_np_id_asuransi").val(json.nama);
    };
    
    asuransi_np_perusahaan=new TableAction("asuransi_np_perusahaan","kasir","asuransi_np",Array());
    asuransi_np_perusahaan.setSuperCommand("asuransi_np_perusahaan");
    asuransi_np_perusahaan.selected=function(json){
        $("#asuransi_np_nama_perusahaan").val(json.nama);
        $("#asuransi_np_id_perusahaan").val(json.nama);
    };
    
    var column=new Array('id','waktu','nilai','keterangan','no_np',
                        'no_bukti','id_asuransi','nama_asuransi',"terklaim"
                        ,'id_perusahaan','nama_perusahaan'
                        );
    asuransi_np=new TableAction("asuransi_np","kasir","asuransi_np",column);
    asuransi_np.getRegulerData=function(){
        var reg_data = {	
                page            : this.page,
                action          : this.action,
                super_command   : this.super_command,
                prototype_name  : this.prototype_name,
                prototype_slug  : this.prototype_slug,
                metode          : "asuransi_np",
                id_np           : $("#tagihan_non_pasien_parent_id").val(),
                nama_pasien     : $("#tagihan_non_pasien_parent_nama").val(),
                nama_asuransi   : $("#"+this.prefix+"_id_asuransi option:selected").text(),
            };
        return reg_data;
    };
});