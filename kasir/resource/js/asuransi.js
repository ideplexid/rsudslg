var asuransi;
$(document).ready(function(){
    $('.mydatetime').datetimepicker({ minuteStep: 1});	
    $('.mydate').datepicker();
    $("#asuransi_terklaim").on("change",function(){
        if($("#asuransi_terklaim").is(":checked")){
            $("#asuransi_no_bukti").prop("disabled",false);
        }else{
            $("#asuransi_no_bukti").val("");
            $("#asuransi_no_bukti").prop("disabled",true);
        }
    });
    
    $('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
    var column  = new Array('id','waktu_masuk','waktu','nilai','keterangan','no_bukti','id_asuransi','nama_asuransi',"terklaim","tgl_klaim","nama_perusahaan","id_perusahaan");
    asuransi    = new TableAction("asuransi","kasir","asuransi",column);
    asuransi.getRegulerData = function(){
        var reg_data = {	
                page            : this.page,
                action          : this.action,
                super_command   : this.super_command,
                prototype_name  : this.prototype_name,
                prototype_slug  : this.prototype_slug,
                urji            : $("#inap").val(),
                carabayar       : $("#pembayaran").val(),
                ruangan         : $("#ruangan").val(),	
                noreg_pasien    : $("#"+this.prefix+"_noreg_pasien").val(),
                nama_pasien     : $("#"+this.prefix+"_nama_pasien").val(),
                nrm_pasien      : $("#"+this.prefix+"_nrm_pasien").val(),
                metode          : "asuransi"
            };
        return reg_data;
    };
    asuransi.printelement=function(id){
        var data        = this.getRegulerData();
        data['command'] = 'print-element';
        data['slug']    = 'print-element';
        data['id']      = id;
        $.post("",data,function(res){
            var json    = getContent(res);
            if(json==null){
                return;
            }
            kwitansi_simpan(json);            
        });
    };
    asuransi.show_add_form=function(){
        var a           = this.getRegulerData();
        if($("#nrm_pasien").val()=="" || $("#noreg_pasien").val()=="" || $("#nama_pasien").val()==""){
            showWarning("Kesalahan","Silakan Pilih Pasien Terlebih Dahulu");
            return ;
        }
        var self            = this;
        a['action']         = "get_sisa_bayar";
        a['noreg_pasien']   = $("#noreg_pasien").val();
        showLoading();
        $.post("",a,function(res){
            self.clear();
            dismissLoading();
            var ctx         = getContent(res);
            setMoney('#asuransi_nilai',Number(ctx.sisa));
            self.show_form();
        });	
    };
    asuransi.aftersave      = function(){
        hitung_ulang();
    };
    asuransi.view();	
});