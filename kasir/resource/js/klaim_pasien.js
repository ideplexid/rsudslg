var REKAP_KLAIM_CUR_NUMBER=0;
var REKAP_KLAIM_TOT_NUMBER=0;
var dump_reinit=function(){
	klaim_pasien.view();
};
var klaim_pasien_full_print="";	
var loop_init_all_tagihan_pasien=function(){klaim_pasien.loop_init_all_tagihan_pasien(); };
$(document).ready(function(){
		
	/*INIT PRINT TAGIHAN LOOP*/
	klaim_pasien.print_tagihan_pasien=function(id){
		var data=this.getRegulerData();
		data['action']="total_tagihan_kasir";
		data['noreg_pasien']=id;
		data['command']="show_kwitansi";
		data['file_mode']=$("#klaim_pasien_kwitansi").val();
		showLoading();
		$.post("",data,function(res){
			dismissLoading();
			smis_print(res);	
		});
	};
    
    klaim_pasien.reup=function(){
        $("#sub_menu_klaim_pasien_content").html("");	
        $("#sub_menu_klaim_pasien").hide("fast");
        $("#main_klaim_pasien").show("fast");
    };
    
    klaim_pasien.load_detail = function(id){
        var data={
                page:"kasir",
                action:"pembayaran_patient",
                super_command:"",
                prototype_name:"",
                prototype_slug:"",
                prototype_implement:"",
                list_registered_select:id
            };
        showLoading();
        $.post("",data,function(res){
            $("#sub_menu_klaim_pasien_content").html(res);
            var autoload=$("#autoload_claim_pasien").val();	
            $("#cashier_autoload").val(autoload);
            $("#sub_menu_klaim_pasien").show("fast");
            $("#main_klaim_pasien").hide("fast");
            dismissLoading();
        });
    };
    
    klaim_pasien.update_push_accounting = function(id){
        var data=klaim_pasien.getRegulerData();
        data['action']="update_push_accounting";
        data['noreg_pasien']=id;
        showLoading();
        $.post("",data,function(res){
            klaim_pasien.view();
            dismissLoading();
        });
    };
    
    klaim_pasien.update_unpush_accounting = function(id){
        var data=klaim_pasien.getRegulerData();
        data['action']="update_unpush_accounting";
        data['noreg_pasien']=id;
        showLoading();
        $.post("",data,function(res){
            var json=getContent(res);
            klaim_pasien.view();
            dismissLoading();
        });
    };
	
	klaim_pasien.loop_print=function(total,number){
		if(number>=total){
			$("#rtb_load_modal").modal("hide");
			smis_print(klaim_pasien_full_print);
			return ;
		}
		var id=$("#klaim_pasien_list").children().eq(number).children().eq(1).html();
		var nama=$("#klaim_pasien_list").children().eq(number).children().eq(3).html();
		var data=this.getRegulerData();
		data['command']="show_kwitansi";
		data['action']="total_tagihan_kasir";
		data['file_mode']=$("#klaim_pasien_kwitansi").val();
		data['noreg_pasien']=id;
		$.post("",data,function(res){
			$("#rtb_person_bar").sload("true","Loading..."+nama+" [ "+(number+1)+" / "+total+" ]",(number)*100/total);
			klaim_pasien_full_print+=("<div class='pagebreak'></div>"+res);
			klaim_pasien.loop_print(total,number+1)
		});
	};
    
    klaim_pasien.loop_post=function(total,number){
		if(number>=total){
			$("#rtb_load_modal").modal("hide");
			klaim_pasien.view();
			return ;
		}
		var id=$("#klaim_pasien_list").children().eq(number).children().eq(1).html();
		var nama=$("#klaim_pasien_list").children().eq(number).children().eq(3).html();
		var data=this.getRegulerData();
        data['action']="update_push_accounting";
        data['noreg_pasien']=id;
        $.post("",data,function(res){
			$("#rtb_person_bar").sload("true","Loading..."+nama+" [ "+(number+1)+" / "+total+" ]",(number)*100/total);
			klaim_pasien.loop_post(total,number+1)
		});
	};
    
    klaim_pasien.loop_unpost=function(total,number){
		if(number>=total){
			$("#rtb_load_modal").modal("hide");
			klaim_pasien.view();
			return ;
		}
		var id=$("#klaim_pasien_list").children().eq(number).children().eq(1).html();
		var nama=$("#klaim_pasien_list").children().eq(number).children().eq(3).html();
		var data=this.getRegulerData();
        data['action']="update_unpush_accounting";
        data['noreg_pasien']=id;
        $.post("",data,function(res){
			$("#rtb_person_bar").sload("true","Loading..."+nama+" [ "+(number+1)+" / "+total+" ]",(number)*100/total);
			klaim_pasien.loop_unpost(total,number+1)
		});
	};
	
	klaim_pasien.init_all_print=function(){
		klaim_pasien_full_print="";
		var total=$("#klaim_pasien_list").children().length-1;		
		$("#rtb_load_modal").modal("show");
		klaim_pasien.loop_print(total,0);
	};
    
    klaim_pasien.init_all_post=function(){
		var total=$("#klaim_pasien_list").children().length-1;		
		$("#rtb_load_modal").modal("show");
		klaim_pasien.loop_post(total,0);
	};
    
    klaim_pasien.init_all_unpost=function(){
		var total=$("#klaim_pasien_list").children().length-1;		
		$("#rtb_load_modal").modal("show");
		klaim_pasien.loop_unpost(total,0);
	};
    
	/*END INIT PRINT TAGIHAN LOOP*/
	
	
	
	/*INIT REKAP TAGIHAN LOOP*/
	klaim_pasien.init_all_tagihan_pasien=function(){
		REKAP_KLAIM_TOT_NUMBER=$("#klaim_pasien_list").children().length-1;
		REKAP_KLAIM_CUR_NUMBER=0;
		this.loop_init_all_tagihan_pasien();
	};
	
	klaim_pasien.loop_init_all_tagihan_pasien=function(){
		if(REKAP_KLAIM_CUR_NUMBER>=REKAP_KLAIM_TOT_NUMBER){
			klaim_pasien.view();
			return;	
		}
		var id=$("#klaim_pasien_list").children().eq(REKAP_KLAIM_CUR_NUMBER).children().eq(1).html();
		var nama=$("#klaim_pasien_list").children().eq(REKAP_KLAIM_CUR_NUMBER).children().eq(3).html();
		var title="<small>Processing..."+nama+" [ "+(REKAP_KLAIM_CUR_NUMBER+1)+" / "+REKAP_KLAIM_TOT_NUMBER+" ]</small>";
		REKAP_KLAIM_CUR_NUMBER++;
		var data=this.getRegulerData();
		data['action']="total_tagihan_kasir";
		data['noreg_pasien']=Number(id);
		data['invoke']="loop_init_all_tagihan_pasien";
		data['title']=title;
		showLoading();
		$.post("",data,function(res){
			$("#tagihan_place_init").html(res);
			dismissLoading();
		});
	};
	
	klaim_pasien.init_tagihan=function(id){
		var data=this.getRegulerData();
		data['action']="total_tagihan_kasir";
		data['noreg_pasien']=id;
		data['invoke']="dump_reinit";
		showLoading();
		$.post("",data,function(res){
			$("#tagihan_place_init").html(res);
			dismissLoading();
		});
	};
	
	/*END - INIT REKAP TAGIHAN LOOP*/
	
	
	/*INIT KOREKSI CARABAYAR LOOP*/
	klaim_pasien.init_koreksi_carabayar=function(id){
		var data=this.getRegulerData();
		data['action']="koreksi_carabayar";
		data['noreg_pasien']=id;
		showLoading();
		$.post("",data,function(res){
			dismissLoading();
		});
	};
	
	
	klaim_pasien.init_all_koreksi_carabayar=function(){
		var total=$("#klaim_pasien_list").children().length-1;		
		$("#rtb_load_modal").modal("show");
		klaim_pasien.loop_init_all_koreksi_carabayar(total,0);
	};
	
	klaim_pasien.loop_init_all_koreksi_carabayar=function(total,number){
		if(number>=total){
			$("#rtb_load_modal").modal("hide");
			return ;
		}
		var id=$("#klaim_pasien_list").children().eq(number).children().eq(1).html();
		var nama=$("#klaim_pasien_list").children().eq(number).children().eq(3).html();
		var data=this.getRegulerData();
		data['action']="koreksi_carabayar";
		data['noreg_pasien']=id;
		$.post("",data,function(res){
			$("#rtb_person_bar").sload("true","Loading..."+nama+" [ "+(number+1)+" / "+total+" ]",(number)*100/total);
			klaim_pasien.loop_init_all_koreksi_carabayar(total,number+1)
		});		
	};
	/*END - INIT KOREKSI CARABAYAR LOOP*/
	
	
	/*INIT ACTION*/
	$("#klaim_pasien_nrm_pasien, #klaim_pasien_noreg_pasien").keyup(function(e){
		var code = (e.keyCode ? e.keyCode : e.which);
		if (code==13) {
			e.preventDefault();
			 klaim_pasien.view();
		}
	});
	/*END - INIT ACTION*/
});