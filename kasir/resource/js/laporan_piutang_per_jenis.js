/**
 * this sistem used to resume
 * all income that incoming (even or not)
 * in given timeframe
 * 
 * @author      : Nurul Huda
 * @copyright   : goblooge@gmail.com
 * @since       : 17 Aug 2016
 * @used        : kasir/resource/php/laporan/laporan_piutang_per_jenis.php
 * @version     : 1.6.0
 * @license     : LGPLv3
 * */

var laporan_piutang_per_jenis;
var laporan_piutang_per_jenis_karyawan;
var laporan_piutang_per_jenis_data;
var IS_laporan_piutang_per_jenis_RUNNING;
$(document).ready(function(){
    $('.mydate').datepicker();
    laporan_piutang_per_jenis=new TableAction("laporan_piutang_per_jenis","kasir","laporan_piutang_per_jenis",new Array());
    laporan_piutang_per_jenis.addRegulerData=function(data){
        data['dari']=$("#laporan_piutang_per_jenis_dari").val();
        data['sampai']=$("#laporan_piutang_per_jenis_sampai").val();
        data['origin']=$("#laporan_piutang_per_jenis_origin").val();
        $("#dari_table_laporan_piutang_per_jenis").html(getFormattedDate(data['dari']));
        $("#sampai_table_laporan_piutang_per_jenis").html(getFormattedDate(data['sampai']));			
        return data;
    };

    laporan_piutang_per_jenis.batal=function(){
        IS_laporan_piutang_per_jenis_RUNNING=false;
        $("#rekap_laporan_piutang_per_jenis_modal").modal("hide");
    };
    
    laporan_piutang_per_jenis.afterview=function(json){
        if(json!=null){
            $("#kode_table_laporan_piutang_per_jenis").html(json.nomor);
            $("#waktu_table_laporan_piutang_per_jenis").html(json.waktu);
            laporan_piutang_per_jenis_data=json;
        }
    };

    laporan_piutang_per_jenis.rekaptotal=function(){
        if(IS_laporan_piutang_per_jenis_RUNNING) return;
        $("#rekap_laporan_piutang_per_jenis_bar").sload("true","Fetching total data",0);
        $("#rekap_laporan_piutang_per_jenis_modal").modal("show");
        IS_laporan_piutang_per_jenis_RUNNING=true;
        var d=this.getRegulerData();
        d['super_command']="total";
        $.post("",d,function(res){
            var all=getContent(res);
            if(all!=null) {
                var total=all.length;
                laporan_piutang_per_jenis.rekaploop(0,all,total);
            } else {
                $("#rekap_laporan_piutang_per_jenis_modal").modal("hide");
                IS_laporan_piutang_per_jenis_RUNNING=false;
            }
        });
    };

    laporan_piutang_per_jenis.rekaploop=function(current,all,total){
        if(current>=total || !IS_laporan_piutang_per_jenis_RUNNING) {
            $("#rekap_laporan_piutang_per_jenis_modal").modal("hide");
            IS_laporan_piutang_per_jenis_RUNNING=false;
            laporan_piutang_per_jenis.view();
            return;
        }
        var one_entity=all[current];
        $("#rekap_laporan_piutang_per_jenis_bar").sload("true",one_entity['name']+"... [ "+(current+1)+" / "+total+" ] ",(current*100/total));
        var d=this.getRegulerData();
        d['super_command']="limit";
        d['entity']=one_entity['value'];		
        $.post("",d,function(res){
            var ct=getContent(res);
            setTimeout(function(){laporan_piutang_per_jenis.rekaploop(++current,all,total)},300);
        });
    };
            
});