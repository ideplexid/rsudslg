$(document).ready(function(){
	$("#tagihan_backup_input_nilai, #tagihan_backup_input_jumlah ").on("change",function(){
		var uang=Number(getMoney("#tagihan_backup_input_nilai"));
		var jumlah=Number($("#tagihan_backup_input_jumlah").val());
		var total=uang*jumlah;
		setMoney("#tagihan_backup_input_subtotal",total);
	});
	
	tagihan_backup_input.after_form_shown=function(){
		$("#tagihan_backup_input_nilai").trigger("change");
	};

	tagihan_backup_input.addRegulerData = function(x){
		x['uri'] = $("#inap").val();
		x['ruangan'] = $("#ruangan").val();
		if(x['uri']=="Rawat Jalan"){
			x['uri'] = 0;
		}else{
			x['uri'] = 1;
		}
		x['noreg_pasien'] = $("#noreg_pasien").val();
		x['nama_pasien'] = $("#nama_pasien").val();
		x['nrm_pasien'] = $("#nrm_pasien").val();
		return x;
		
	};
    
    stypeahead("#tagihan_backup_input_nama_tagihan",3,tagihan_backup_master,"nama",function(item){
		tagihan_backup_master.selected(item);
	});
});