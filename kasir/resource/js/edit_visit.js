var edit_visit;
var px_edit_visit;
var dokter_visite;
var dokter_visite_dua;
var dokter_visite_tiga;
var tarif_visite;
$(document).ready(function(){
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
    $('.mydatetime').datetimepicker({ minuteStep: 1});
	var column=new Array(
			'id',"nama_pasien","noreg_pasien","nrm_pasien",
			'waktu','kelas','harga',
			'nama_dokter','id_dokter',
			'nama_dokter_dua','id_dokter_dua',
			'nama_dokter_tiga','id_dokter_tiga',
			"nama_visite","id_visite");
            
	edit_visit=new TableAction("edit_visit","kasir","edit_visit",column);
    edit_visit.addNoClear("nama_pasien");
    edit_visit.addNoClear("noreg_pasien");
    edit_visit.addNoClear("nrm_pasien");
    edit_visit.addNoClear("carabayar");
    edit_visit.addNoClear("ruang");
    
	edit_visit.addRegulerData=function(a){
		a['noreg_pasien']=$("#edit_visit_noreg_pasien").val();
		a['nrm_pasien']=$("#edit_visit_nrm_pasien").val();
		a['nama_pasien']=$("#edit_visit_nama_pasien").val();
		a['carabayar']=$("#edit_visit_carabayar").val();
		a['ruang']=$("#edit_visit_ruang").val();
        a['header']=$("#edit_visit_header_element").val();
		return a;
	};
	edit_visit.show_add_form=function(){
		if($("#edit_visit_noreg_pasien").val()==""){
			showWarning("Peringatan","Silakan Pilih Pasien Dahulu");
			return;
		}
		this.clear();
		this.show_form();
	};	
	px_edit_visit=new TableAction("px_edit_visit","kasir","get_registered_patient",column);
	px_edit_visit.setSuperCommand("px_edit_visit");
    px_edit_visit.selected=function(json){
		$("#edit_visit_nama_pasien").val(json.nama_pasien);
		$("#edit_visit_alamat_pasien").val(json.alamat_pasien);
		$("#edit_visit_noreg_pasien").val(json.id);
		$("#edit_visit_nrm_pasien").val(json.nrm);
		$("#edit_visit_carabayar").val(json.carabayar);
		edit_visit.view();
	};
	dokter_visite=new TableAction("dokter_visite","kasir","get_employee",column);
	dokter_visite.setSuperCommand("dokter_visite");
    dokter_visite.setOwnChooserData(true);
	dokter_visite.addRegulerData=function(a){
        a['filter']="dokter";
        return a;
    };
	dokter_visite.selected=function(json){
		$("#edit_visit_id_dokter").val(json.id);
		$("#edit_visit_nama_dokter").val(json.nama);
	};
    
    dokter_visite_dua=new TableAction("dokter_visite_dua","kasir","get_employee",column);
	dokter_visite_dua.setSuperCommand("dokter_visite_dua");
    dokter_visite_dua.setOwnChooserData(true);
    dokter_visite_dua.addRegulerData=function(a){
        a['filter']="dokter";
        return a;
    }
	dokter_visite_dua.selected=function(json){
		$("#edit_visit_id_dokter_dua").val(json.id);
		$("#edit_visit_nama_dokter_dua").val(json.nama);
	};
    
    dokter_visite_tiga=new TableAction("dokter_visite_tiga","kasir","get_employee",column);
	dokter_visite_tiga.setSuperCommand("dokter_visite_tiga");
    dokter_visite_tiga.setOwnChooserData(true);
    dokter_visite_tiga.addRegulerData=function(a){
        a['filter']="dokter";
        return a;
    }
	dokter_visite_tiga.selected=function(json){
		$("#edit_visit_id_dokter_tiga").val(json.id);
		$("#edit_visit_nama_dokter_tiga").val(json.nama);
	};
    
    edit_visit.reload_view=function(){
        var d=list_registered.getRegulerData();
        d['super_command']="edit_visit";
        d['ruangan']=$("#edit_visit_ruang").val();
        d['noreg_pasien']=$("#noreg_pasien").val();
        showLoading();
        $.post("",d,function (res){
            $("#visit").html(res);
            dismissLoading();
        });
    }
    
    tarif_visite=new TableAction("tarif_visite","kasir","edit_visit",new Array());
	tarif_visite.setSuperCommand("tarif_visite");
	tarif_visite.addRegulerData=function(d){
		d['noreg_pasien']=$("#edit_visit_noreg_pasien").val();
        d['separated']=$("#is_visite_separated").val();
		return d;
	};
	tarif_visite.selected=function(json){
		var nama=json.nama_dokter;
		var id_dokter=json.id_dokter;		
		var kelas=json.kelas;
		var harga=Number(json.tarif);
		if($("#is_visite_separated").val()!="1"){
            $("#edit_visit_nama_dokter").val(nama);
			$("#edit_visit_id_dokter").val(id_dokter);
		}				
		
		$("#edit_visit_nama_visite").val(json.nama_visite);
		$("#edit_visit_id_visite").val(json.id);
		$("#edit_visit_kelas").val(kelas);
		$("#edit_visit_harga").maskMoney('mask',harga);
		
	};

	$("#edit_visit_ruang").change(function(){
		edit_visit.reload_view();
	});
    
    if($("#edit_visit_noreg_pasien").val()!=""){
        var noreg_pasien=$("#edit_visit_noreg_pasien").val();
        px_edit_visit.select(noreg_pasien);
    }
    
});