/**
 * this used for viewing the history of payment
 * this page automatically filter the bank payment
 * 
 * @version     : 5.0.0
 * @since       : 14 May 2015
 * @author      : Nurul Huda
 * @copyright   : goblooge@gmail.com
 * @license     : LGPLv3
 * @used        : kasir/resource/php/laporan/lap_bank.php
 * */
var lap_bank;
$(document).ready(function(){
    $(".mydatetime").datetimepicker({minuteStep:1});
    $('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
    var column = new Array('dari','sampai',"origin");
    lap_bank   = new TableAction("lap_bank","kasir","lap_bank",column);
    lap_bank.getRegulerData = function(){
        var reg_data = {	
                page            : this.page,
                action          : this.action,
                super_command   : this.super_command,
                prototype_name  : this.prototype_name,
                prototype_slug  : this.prototype_slug,
                dari_wkt        : $("#"+this.prefix+"_dari_wkt").val(),
                sampai_wkt      : $("#"+this.prefix+"_sampai_wkt").val(),
                dari_id         : $("#"+this.prefix+"_dari_id").val(),
                sampai_id       : $("#"+this.prefix+"_sampai_id").val(),
                dari_nokwi      : $("#"+this.prefix+"_dari_nokwi").val(),
                sampai_nokwi    : $("#"+this.prefix+"_sampai_nokwi").val(),
                jenis           : $("#"+this.prefix+"_jenis").val(),
                nrm             : $("#"+this.prefix+"_nrm").val(),
                nama            : $("#"+this.prefix+"_nama").val(),
                filter_origin   : $("#"+this.prefix+"_filter_origin").val(),
                operator        : $("#"+this.prefix+"_operator").val()
        };
        var dr  = getFormattedTime(reg_data['dari_wkt']);
        var sp  = getFormattedTime(reg_data['sampai_wkt']);
        var hsl = "";
        if(reg_data['dari_wkt']!="" && reg_data['sampai_wkt']!="")
            hsl = " Waktu [ "+dr+" - "+sp+" ] ";			
        if(reg_data['dari_id']!="" && reg_data['sampai_id']!="")
            hsl+=" ID [ "+reg_data['dari_id']+" - "+reg_data['sampai_id']+" ] ";
        if(reg_data['dari_nokwi']!="" && reg_data['sampai_nokwi']!="")
            hsl+=" No. Kwitansi [ "+reg_data['dari_nokwi']+" - "+reg_data['sampai_nokwi']+" ] ";
        
        var x = $("#lap_bank_jenis option:selected").text();
        $("#lap_bank_jenisx").html(x);
        $("#tgl_lap_bank_print").html(hsl);
        return reg_data;
    };
    
    lap_bank.loop_post = function(total,number){
		if(number>=total){
			smis_loader.hideLoader();
			this.view();
			return ;
		}
		var id               = parseInt($("#"+this.prefix+"_list").children().eq(number).children().eq(6).children().eq(0).html());
		var nama             = $("#"+this.prefix+"_list").children().eq(number).children().eq(5).html();
		var data             = this.getRegulerData();
        data['action']       = "push_to_accounting";
        data['noreg_pasien'] = id;
        var self             = this;
        $.post("",data,function(res){
			smis_loader.updateLoader("true","Loading..."+nama+" [ "+(number+1)+" / "+total+" ]",(number)*100/total);
			self.loop_post(total,number+1)
		});
	};
    
    lap_bank.post_to_all = function(){
        var total = $("#"+this.prefix+"_list").children().length-1;		
		smis_loader.showLoader();
		this.loop_post(total,0);
    };
    
    lap_bank.push_to_accounting = function(id){
        var data        = this.getRegulerData();
        data['id']      = id;
        data['command'] = "edit";
        showLoading();
        $.post("",data,function(res){
            var json             = getContent(res);
            data['action']       = "push_to_accounting";
            data['noreg_pasien'] = json.noreg_pasien;
            showLoading();
            $.post("",data,function(res){
                dismissLoading();
                smis_alert("Success","Data Notified !!!","alert-info");
            });
            dismissLoading();            
        });
    };
});