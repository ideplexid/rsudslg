/**
 * this used for viewing the history of payment
 * this page automatically filter the cash payment
 * 
 * @version     : 5.0.0a
 * @since       : 14 May 2015
 * @author      : Nurul Huda
 * @copyright   : goblooge@gmail.com
 * @license     : LGPLv4
 * @used        : kasir/resource/php/laporan/lap_cash_resep_non_pasien.php
 * */
var lap_cash_resep_non_pasien;
$(document).ready(function(){
    $(".mydatetime").datetimepicker({minuteStep:1});
    $('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
    var column  = new Array('dari','sampai','origin');
    lap_cash_resep_non_pasien    = new TableAction("lap_cash_resep_non_pasien","kasir","lap_cash_resep_non_pasien",column);
    lap_cash_resep_non_pasien.getRegulerData = function(){
        var reg_data = {	
                page            : this.page,
                action          : this.action,
                super_command   : this.super_command,
                prototype_name  : this.prototype_name,
                prototype_slug  : this.prototype_slug,
                dari_wkt        : $("#"+this.prefix+"_dari_wkt").val(),
                sampai_wkt      : $("#"+this.prefix+"_sampai_wkt").val(),
                dari_id         : $("#"+this.prefix+"_dari_id").val(),
                sampai_id       : $("#"+this.prefix+"_sampai_id").val(),
                dari_nokwi      : $("#"+this.prefix+"_dari_nokwi").val(),
                sampai_nokwi    : $("#"+this.prefix+"_sampai_nokwi").val(),
                nama            : $("#"+this.prefix+"_nama").val(),
                origin          : $("#"+this.prefix+"_origin").val()
        };
        var dr  = getFormattedTime(reg_data['dari_wkt']);
        var sp  = getFormattedTime(reg_data['sampai_wkt']);
        var hsl = "";
        if(reg_data['dari_wkt']!="" && reg_data['sampai_wkt']!="")
            hsl = " Waktu [ "+dr+" - "+sp+" ] ";			
        if(reg_data['dari_id']!="" && reg_data['sampai_id']!="")
            hsl+=" ID [ "+reg_data['dari_id']+" - "+reg_data['sampai_id']+" ] ";
        if(reg_data['dari_nokwi']!="" && reg_data['sampai_nokwi']!="")
            hsl+=" No. Kwitansi [ "+reg_data['dari_nokwi']+" - "+reg_data['sampai_nokwi']+" ] ";
        
        $("#tgl_lap_cash_resep_non_pasien_print").html(hsl);
        return reg_data;
    };
    
    lap_cash_resep_non_pasien.loop_post = function(total,number){
		if(number>=total){
			smis_loader.hideLoader();
			this.view();
			return ;
		}
		var id               = parseInt($("#"+this.prefix+"_list").children().eq(number).children().eq(0).html());
		var nama             = $("#"+this.prefix+"_list").children().eq(number).children().eq(5).html();
		var data             = this.getRegulerData();
        data['action']       = "push_cash_resep_np_to_accounting";
        data['id']           = id;
        var self             = this;
        $.post("",data,function(res){
			smis_loader.updateLoader("true","Loading..."+nama+" [ "+(number+1)+" / "+total+" ]",(number)*100/total);
			self.loop_post(total,number+1)
		});
	};
    
    lap_cash_resep_non_pasien.post_to_all = function(){
        var total = $("#"+this.prefix+"_list").children().length-1;		
		smis_loader.showLoader();
		this.loop_post(total,0);
    };
    
    lap_cash_resep_non_pasien.push_to_accounting = function(id){
        var data        = this.getRegulerData();
        data['id']      = id;
        data['action'] = "push_cash_resep_np_to_accounting";
        showLoading();
        $.post("",data,function(res){
           getContent(res);
           dismissLoading();            
        });
    };
    
    
});