var bank;
$(document).ready(function(){
    $('.mydatetime').datetimepicker({ minuteStep: 1});
    $("#bank_persen, #bank_nilai").on("change",function(){
        var nilai=Number(getMoney("#bank_nilai"));
        var persen=Number($("#bank_persen").val());
        var uang=nilai*persen/100;
        setMoney("#bank_nilai_tambahan",uang);
    });

    $('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
    var column=new Array('id','waktu','nilai','nilai_tambahan','keterangan','di_bank','id_bank','no_bukti');
    bank=new TableAction("bank","kasir","bank",column);
    bank.getRegulerData=function(){
        var reg_data={	
                page:this.page,
                action:this.action,
                super_command:this.super_command,
                prototype_name:this.prototype_name,
                prototype_slug:this.prototype_slug,
                urji:$("#inap").val(),
                carabayar:$("#pembayaran").val(),
                ruangan:$("#ruangan").val(),	
                noreg_pasien:$("#"+this.prefix+"_noreg_pasien").val(),
                nama_pasien:$("#"+this.prefix+"_nama_pasien").val(),
                nrm_pasien:$("#"+this.prefix+"_nrm_pasien").val(),
                nama_bank:$("#"+this.prefix+"_id_bank option:selected").text(),
                metode:"bank"
                };
        return reg_data;
    };
    
    bank.excel_element=function(id){
        var data=this.getEditData(id);
        data['uri']=$("#inap").val();
        data['jenis_pasien']=$("#pembayaran").val();
        data['command']="excel-element";
        download(data);
    };
    
    bank.printelement=function(id){
        var data=this.getRegulerData();
        data['command']='print-element';
        data['slug']='print-element';
        data['id']=id;
        $.post("",data,function(res){
            var json=getContent(res);
            if(json==null) return;
            kwitansi_simpan(json);	
                        
        });
    };
    
    bank.excelelement=function(id){
        var data=this.getRegulerData();
        data['command']='excel-element';
        data['slug']='print-element';
        data['id']=id;
        $.post("",data,function(res){
            var json=getContent(res);
            if(json==null) return;
            kwitansi_simpan(json);	
                        
        });
    };
    
    bank.show_add_form=function(){
        var a=this.getRegulerData();
        if($("#nrm_pasien").val()=="" || $("#noreg_pasien").val()=="" || $("#nama_pasien").val()==""){
            showWarning("Kesalahan","Silakan Pilih Pasien Terlebih Dahulu");
            return ;
        }
        var self=this;
        a['action']="get_sisa_bayar";
        a['noreg_pasien']=$("#noreg_pasien").val();
        showLoading();
        $.post("",a,function(res){
            self.clear();
            dismissLoading();
            var ctx=getContent(res);
            setMoney('#bank_nilai',Number(ctx.sisa));
            self.show_form();
        });	
    };
    bank.aftersave=function(){	hitung_ulang(); };
    bank.view();	
});