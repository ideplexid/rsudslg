var kwitansi;
$(document).ready(function(){
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
	var column=new Array();
	kwitansi=new TableAction("kwitansi","kasir","kwitansi",column);
	kwitansi.view();
	kwitansi.show_kwitansi=function(id){
		var data={
				page:"kasir",
				action:"kwitansi",
				id:id,
				command:"edit"
            };
		showLoading();
		$.post('',data,function(res){
			var json=getContent(res);
			dismissLoading();
			showFullWarning(json.noref, json.html,"full_model");
		});
	};
});