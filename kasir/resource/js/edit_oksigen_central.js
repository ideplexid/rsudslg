var edit_oksigen_central;
var px_edit_oksigen_central;
$(document).ready(function(){
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
    $(".mydatetime").datetimepicker({minuteStep:1});
	var column=new Array('id','nama_pasien','nrm_pasien','noreg_pasien','mulai','selesai','skala');
	edit_oksigen_central=new TableAction("edit_oksigen_central","kasir","edit_oksigen_central",column);
	edit_oksigen_central.addRegulerData=function(a){
		a['noreg_pasien']=$("#edit_oksigen_central_noreg_pasien").val();
		a['nrm_pasien']=$("#edit_oksigen_central_nrm_pasien").val();
		a['nama_pasien']=$("#edit_oksigen_central_nama_pasien").val();
		a['carabayar']=$("#edit_oksigen_central_carabayar").val();
		a['ruang']=$("#edit_oksigen_central_ruang").val();
		return a;
	};
    
    edit_oksigen_central.addNoClear("nama_pasien");
    edit_oksigen_central.addNoClear("noreg_pasien");
    edit_oksigen_central.addNoClear("nrm_pasien");
    edit_oksigen_central.addNoClear("carabayar");
    edit_oksigen_central.addNoClear("ruang");
    
	edit_oksigen_central.show_add_form=function(){
		if($("#edit_oksigen_central_noreg_pasien").val()==""){
			showWarning("Peringatan","Silakan Pilih Pasien Dahulu");
			return;
		}
		this.clear();
		this.show_form();
	};	
	px_edit_oksigen_central=new TableAction("px_edit_oksigen_central","kasir","get_registered_patient",column);
	px_edit_oksigen_central.setSuperCommand("px_edit_oksigen_central");
	px_edit_oksigen_central.selected=function(json){
		$("#edit_oksigen_central_nama_pasien").val(json.nama_pasien);
		$("#edit_oksigen_central_alamat_pasien").val(json.alamat_pasien);
		$("#edit_oksigen_central_noreg_pasien").val(json.id);
		$("#edit_oksigen_central_nrm_pasien").val(json.nrm);
		$("#edit_oksigen_central_carabayar").val(json.carabayar);
		edit_oksigen_central.view();
	};
	

	$("#edit_oksigen_central_ruang").change(function(){        
        edit_oksigen_central.view();
	});
    
    if($("#edit_oksigen_central_noreg_pasien").val()!=""){
        var noreg_pasien=$("#edit_oksigen_central_noreg_pasien").val();  
        px_edit_oksigen_central.select(noreg_pasien);
    }
	
});