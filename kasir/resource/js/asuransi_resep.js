var asuransi_resep;
var asuransi_resep_asuransi;
var asuransi_resep_perusahaan;
$(document).ready(function(){
    $('.mydatetime').datetimepicker({ minuteStep: 1});
    $('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
    $("#asuransi_resep_persen, #asuransi_resep_nilai").on("change",function(){
        var nilai=Number(getMoney("#asuransi_resep_nilai"));
        var persen=Number($("#asuransi_resep_persen").val());
        var uang=nilai*persen/100;
        setMoney("#asuransi_resep_nilai_tambahan",uang);
    });

    asuransi_resep_asuransi=new TableAction("asuransi_resep_asuransi","kasir","asuransi_resep",Array());
    asuransi_resep_asuransi.setSuperCommand("asuransi_resep_asuransi");
    asuransi_resep_asuransi.selected=function(json){
        $("#asuransi_resep_nama_asuransi").val(json.nama);
        $("#asuransi_resep_id_asuransi").val(json.nama);
    };
    
    asuransi_resep_perusahaan=new TableAction("asuransi_resep_perusahaan","kasir","asuransi_resep",Array());
    asuransi_resep_perusahaan.setSuperCommand("asuransi_resep_perusahaan");
    asuransi_resep_perusahaan.selected=function(json){
        $("#asuransi_resep_nama_perusahaan").val(json.nama);
        $("#asuransi_resep_id_perusahaan").val(json.nama);
    };
    
    var column=new Array('id','waktu','nilai','keterangan','no_resep',
                        'no_bukti','id_asuransi','nama_asuransi',"terklaim"
                        ,'id_perusahaan','nama_perusahaan',"carabayar"
                        );
    asuransi_resep=new TableAction("asuransi_resep","kasir","asuransi_resep",column);
    asuransi_resep.postAction=function(id){
        reload_resep();
    };
    asuransi_resep.getRegulerData=function(){
        var reg_data = {	
                page            : this.page,
                action          : this.action,
                super_command   : this.super_command,
                prototype_name  : this.prototype_name,
                prototype_slug  : this.prototype_slug,
                metode          : "asuransi_resep",
                id_resep        : $("#"+this.prefix+"_id_resep").val(),
                nama_pasien     : $("#"+this.prefix+"_nama_pasien").val(),
                nrm_pasien      : $("#"+this.prefix+"_nrm_pasien").val(),
                noreg_pasien    : $("#"+this.prefix+"_noreg_pasien").val(),
                ruangan         : $("#ruang").val(),
                nama_asuransi   : $("#"+this.prefix+"_id_asuransi option:selected").text(),
            };
        return reg_data;
    };
    asuransi_resep.view();	
});