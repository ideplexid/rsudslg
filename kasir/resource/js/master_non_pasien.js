$(document).ready(function(){
	master_non_pasien.apply=function(){
		var a=this.getRegulerData();
		a['super_command']="apply";
		showLoading();
		$.post("",a,function(res){
			getContent(res);
			master_non_pasien.view();
			dismissLoading();
		});
	};
	
	master_non_pasien.backup=function(){
		var a=this.getRegulerData();
		a['super_command']="backup";
		showLoading();
		$.post("",a,function(res){
			getContent(res);
			master_non_pasien.view();
			dismissLoading();
		});
	};
	
	master_non_pasien.restore=function(){
		var a=this.getRegulerData();
		a['super_command']="restore";
		showLoading();
		$.post("",a,function(res){
			getContent(res);
			master_non_pasien.view();
			dismissLoading();
		});
	};
});