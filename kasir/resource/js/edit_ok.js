var edit_ok;
var edit_ok_pasien;
var edit_ok_dokter;
$(document).ready(function(){
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
    $('.mydatetime').datetimepicker({ minuteStep: 1});
	var column=new Array("id","waktu","nama_operator_satu",
	"harga_operator_satu","nama_asisten_operator_satu",
    "nama_anastesi","id_anastesi","id_operator_satu",
    "id_asisten_operator_satu","id_asisten_anastesi",
	"nama_asisten_anastesi","nama_tindakan");
	edit_ok=new TableAction("edit_ok","kasir","edit_ok",column);
	edit_ok.addRegulerData=function(a){
		a['noreg_pasien']=$("#edit_ok_noreg_pasien").val();
		a['nrm_pasien']=$("#edit_ok_nrm_pasien").val();
		a['nama_pasien']=$("#edit_ok_nama_pasien").val();
		a['carabayar']=$("#edit_ok_carabayar").val();
		a['ruang']=$("#edit_ok_ruang").val();
		return a;
	};
	edit_ok.show_add_form=function(){
		if($("#edit_ok_noreg_pasien").val()==""){
			showWarning("Peringatan","Silakan Pilih Pasien Dahulu");
			return;
		}
		this.clear();
		this.show_form();
	};

	$("#edit_ok_ruang").change(function(){
		edit_ok.view();
	});
	
	edit_ok_pasien=new TableAction("edit_ok_pasien","kasir","get_registered_patient",column);
	edit_ok_pasien.setSuperCommand("edit_ok_pasien");
	edit_ok_pasien.selected=function(json){
		$("#edit_ok_nama_pasien").val(json.nama_pasien);
		$("#edit_ok_alamat_pasien").val(json.alamat_pasien);
		$("#edit_ok_noreg_pasien").val(json.id);
		$("#edit_ok_nrm_pasien").val(json.nrm);
		$("#edit_ok_carabayar").val(json.carabayar);
		edit_ok.view();
	};
    
     
	edit_ok_dokter=new TableAction("edit_ok_dokter","kasir","get_employee",column);
	edit_ok_dokter.setSuperCommand("edit_ok_dokter");
    edit_ok_dokter.setOwnChooserData(true);
	edit_ok_dokter.selected=function(json){
		$("#edit_ok_id_operator_satu").val(json.id);
		$("#edit_ok_nama_operator_satu").val(json.nama);
	};
    edit_ok_dokter.addRegulerData=function(a){
        a['filter']="dokter";
        return a;
    };
    
    edit_ok_asisten_dokter=new TableAction("edit_ok_asisten_dokter","kasir","get_employee",column);
	edit_ok_asisten_dokter.setSuperCommand("edit_ok_asisten_dokter");
    edit_ok_asisten_dokter.setOwnChooserData(true);
	edit_ok_asisten_dokter.selected=function(json){
		$("#edit_ok_id_asisten_operator_satu").val(json.id);
		$("#edit_ok_nama_asisten_operator_satu").val(json.nama);
	};
    edit_ok_dokter.addRegulerData=function(a){
        a['filter']="perawat";
        return a;
    };
    
    
    edit_ok_anastesi=new TableAction("edit_ok_anastesi","kasir","get_employee",column);
	edit_ok_anastesi.setSuperCommand("edit_ok_anastesi");
    edit_ok_anastesi.setOwnChooserData(true);
	edit_ok_anastesi.selected=function(json){
		$("#edit_ok_id_anastesi").val(json.id);
		$("#edit_ok_nama_anastesi").val(json.nama);
	};
    edit_ok_anastesi.addRegulerData=function(a){
        a['filter']="dokter";
        return a;
    };
    
    edit_ok_asisten_anastesi=new TableAction("edit_ok_asisten_anastesi","kasir","get_employee",column);
	edit_ok_asisten_anastesi.setSuperCommand("edit_ok_asisten_anastesi");
    edit_ok_asisten_anastesi.setOwnChooserData(true);
	edit_ok_asisten_anastesi.selected=function(json){
		$("#edit_ok_id_asisten_anastesi").val(json.id);
		$("#edit_ok_nama_asisten_anastesi").val(json.nama);
	};
    edit_ok_asisten_anastesi.addRegulerData=function(a){
        a['filter']="perawat";
        return a;
    };
    
    
    edit_ok_nama_operasi=new TableAction("edit_ok_nama_operasi","kasir","edit_ok",new Array());
	edit_ok_nama_operasi.setSuperCommand("edit_ok_nama_operasi");
	edit_ok_nama_operasi.addRegulerData=function(d){
		d['noreg_pasien']=$("#ok_noreg_pasien").val();
		d['carabayar']=$("#carabayar").val();
		return d;
	};    
    edit_ok_nama_operasi.selected=function(json){
		var harga=Number(json.tarif);
		$("#edit_ok_nama_tindakan").val(json.nama);
		$("#edit_ok_harga_operator_satu").maskMoney('mask',harga);
	};
    

	$("#edit_ok_ruang").change(function(){
		edit_ok.view();
	});
    
    if($("#edit_ok_noreg_pasien").val()!=""){
        var noreg_pasien=$("#edit_ok_noreg_pasien").val();  
        edit_ok_pasien.select(noreg_pasien)
    }
		
});