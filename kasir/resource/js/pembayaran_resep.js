var pembayaran_resep;
var resep;
function reload_resep(){
    var id_resep=$("#id_resep").val();
    if(id_resep=="" || id_resep=="0"){
        pembayaran_resep.chooser('noresep','pembayaran_resep','resep',resep);
    }else{
        resep.select(id_resep);
    }
}

function print_kwitansi_resep(nomor){
    var content=$("#kwitansi_resep_holder").html();
    var data={
                page:"kasir",
                action:"kwitansi_resep",
                nama_pasien:$("#nama_pasien").val(),
                noreg_pasien:$("#noreg_pasien").val(),
                nrm_pasien:$("#nrm_pasien").val(),
                noref:nomor,
                jenis:"Pembayaran Resep",
                html:content,
                command:"save",
                id:""
            };

    showLoading();
    $.post('',data,function(res){
        var json=getContent(res);
        dismissLoading();
        if(json!=null){
            smis_print(content);
        }
    });
    
    
}

function lunas_resep(){
    if(!cek_lunas_resep()) return;
    var uang=Number($("#dibayar").val());
    setMoney("#resep_dibayar",uang);
    bayar_resep();
}

function cek_lunas_resep(){
    var uang=Number($("#dibayar").val());
    if(uang<0){
        showWarning("Kembalian", "Pasien ini Seharusnya Mendapakan Kembalian");
    }else if(uang==0){
        showWarning("Sudah Lunas", "Sudah Lunas Tinggal di cetak");
    }else{
        return true;
    }
    return false;
}

function bayar_resep(){
    if(!cek_lunas_resep()) return;
    var id_resep=$("#id_resep").val();
    var dibayar=Number(getMoney("#resep_dibayar"));
    var kurangnya=Number($("#dibayar").val());
     var data={
                page:"kasir",
                action:"cash_resep",
                super_command:"",
                prototype_name:"",
                prototype_slug:"",
                metode:"cash_resep",
                id_resep:id_resep,
                no_resep:id_resep,
                nama_pasien:$("#nama_pasien").val(),
                nrm_pasien:$("#nrm_pasien").val(),
                noreg_pasien:$("#noreg_pasien").val(),
                ruangan:$("#ruang").val(),
                command:"save",
                waktu:$("#waktu_resep").val(),
                keterangan:" Resep [ "+id_resep+" ] Pada "+$("#ruang option:selected").text()
        };
    if(dibayar>kurangnya){
        bootbox.confirm("Kembali  "+money(dibayar-kurangnya), function(result) {
               if(result){
                   data['nilai']=kurangnya;
                   $.post('',data,function(res){
                        var json=getContent(res);							
                        reload_resep();
                    });
               }
        });				
    }else{
          data['nilai']=dibayar;
        $.post('',data,function(res){
            var json=getContent(res);
            reload_resep();
        });
    }		
}

$(document).ready(function(){
    pembayaran_resep=new TableAction("pembayaran_resep", "kasir", "pembayaran_resep", new Array() );
    resep=new TableAction("resep","kasir","pembayaran_resep",new Array());
    resep.setSuperCommand("resep");
    resep.addRegulerData=function(a){
        a['ruang']=$("#ruang").val();
        return a;
    };
    resep.getEditData=function(id){
        var edit_data=this.getRegulerData();
        edit_data['command']="edit";
        edit_data['id']=id;
        edit_data['id_resep']=id;
        edit_data['nama_pasien']=$("#nama_pasien").val();
        edit_data['noreg_pasien']=$("#noreg_pasien").val();
        edit_data['nrm_pasien']=$("#nrm_pasien").val();
        return edit_data;
    };
    resep.selected=function(json){
        $("#id_resep").val(json.header.id);
        $("#nama_pasien").val(json.header.nama_pasien);
        $("#nrm_pasien").val(json.header.nrm_pasien);
        $("#noreg_pasien").val(json.header.noreg_pasien);
        $("#carabayar").val(json.header.carabayar);
        $("#dokter").val(json.header.nama_dokter);
        $("#jenis").val(json.header.jenis);
        $("#pembayaran_resep_content").html(json.tabulator);
        $("#kwitansi_resep_holder").html(json.print_kwitansi);
        formatMoney("#resep_dibayar");			
    };		
});