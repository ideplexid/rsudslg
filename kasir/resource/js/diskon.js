var diskon;
$(document).ready(function(){
    $(".mydatetime").datetimepicker({minuteStep:1});
    $('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
    var column=new Array('id','waktu','nilai','keterangan');
    diskon=new TableAction("diskon","kasir","diskon",column);
    diskon.getRegulerData=function(){
        var reg_data={	
                page:this.page,
                action:this.action,
                super_command:this.super_command,
                prototype_name:this.prototype_name,
                prototype_slug:this.prototype_slug,
                urji:$("#inap").val(),
                carabayar:$("#pembayaran").val(),
                ruangan:$("#ruangan").val(),	
                noreg_pasien:$("#"+this.prefix+"_noreg_pasien").val(),
                nama_pasien:$("#"+this.prefix+"_nama_pasien").val(),
                nrm_pasien:$("#"+this.prefix+"_nrm_pasien").val(),
                metode:"diskon"
                };
        return reg_data;
    };

    diskon.aftersave=function(){	hitung_ulang(); };
    diskon.view();	
});
