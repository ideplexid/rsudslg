var rekanan;
$(document).ready(function(){
    $('.mydatetime').datetimepicker({ minuteStep: 1});	
    $('.mydate').datepicker();
    $("#rekanan_terklaim").on("change",function(){
        if($("#rekanan_terklaim").is(":checked")){
            $("#rekanan_no_bukti").prop("disabled",false);
        }else{
            $("#rekanan_no_bukti").val("");
            $("#rekanan_no_bukti").prop("disabled",true);
        }
    });
    
    $('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
    var column=new Array('id','waktu','nilai','keterangan','no_bukti','id_asuransi','nama_asuransi',"terklaim","tgl_klaim","nama_perusahaan","id_perusahaan");
    rekanan=new TableAction("rekanan","kasir","rekanan",column);
    rekanan.getRegulerData=function(){
        var reg_data={	
                page:this.page,
                action:this.action,
                super_command:this.super_command,
                prototype_name:this.prototype_name,
                prototype_slug:this.prototype_slug,
                urji:$("#inap").val(),
                carabayar:$("#pembayaran").val(),
                ruangan:$("#ruangan").val(),	
                noreg_pasien:$("#"+this.prefix+"_noreg_pasien").val(),
                nama_pasien:$("#"+this.prefix+"_nama_pasien").val(),
                nrm_pasien:$("#"+this.prefix+"_nrm_pasien").val(),
                metode:"rekanan"
                };
        return reg_data;
    };
    rekanan.printelement=function(id){
        var data=this.getRegulerData();
        data['command']='print-element';
        data['slug']='print-element';
        data['id']=id;
        $.post("",data,function(res){
            var json=getContent(res);
            if(json==null) return;
            kwitansi_simpan(json);	
                        
        });
    };
    rekanan.show_add_form=function(){
        var a=this.getRegulerData();
        if($("#nrm_pasien").val()=="" || $("#noreg_pasien").val()=="" || $("#nama_pasien").val()==""){
            showWarning("Kesalahan","Silakan Pilih Pasien Terlebih Dahulu");
            return ;
        }
        var self=this;
        a['action']="get_sisa_bayar";
        a['noreg_pasien']=$("#noreg_pasien").val();
        showLoading();
        $.post("",a,function(res){
            self.clear();
            dismissLoading();
            var ctx=getContent(res);
            setMoney('#rekanan_nilai',Number(ctx.sisa));
            self.show_form();
        });	
    };
    rekanan.aftersave=function(){	hitung_ulang(); };
    rekanan.view();	
});