var edit_bed;
var px_edit_bed;
var nomor_bed;
$(document).ready(function(){
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
    $(".mydatetime").datetimepicker({minuteStep:1});
	var column=new Array('id',"nama_pasien","nama_tarif","noreg_pasien","nrm_pasien",'id_bed','nama_bed','waktu_keluar','waktu_masuk','harga','pilihan_tarif');
	edit_bed=new TableAction("edit_bed","kasir","edit_bed",column);
	edit_bed.addRegulerData=function(a){
		a['noreg_pasien']=$("#edit_bed_noreg_pasien").val();
		a['nrm_pasien']=$("#edit_bed_nrm_pasien").val();
		a['nama_pasien']=$("#edit_bed_nama_pasien").val();
		a['carabayar']=$("#edit_bed_carabayar").val();
		a['ruangan']=$("#edit_bed_ruang").val();
		return a;
	};
    
    edit_bed.addNoClear("nama_pasien");
    edit_bed.addNoClear("noreg_pasien");
    edit_bed.addNoClear("nrm_pasien");
    edit_bed.addNoClear("carabayar");
    edit_bed.addNoClear("ruangan");    
	edit_bed.show_add_form=function(){
		if($("#edit_bed_noreg_pasien").val()==""){
			showWarning("Peringatan","Silakan Pilih Pasien Dahulu");
			return;
		}
		this.clear();
		this.show_form();
	};
	
	px_edit_bed=new TableAction("px_edit_bed","kasir","get_registered_patient",column);
	px_edit_bed.setSuperCommand("px_edit_bed");
	px_edit_bed.selected=function(json){
		$("#edit_bed_nama_pasien").val(json.nama_pasien);
		$("#edit_bed_alamat_pasien").val(json.alamat_pasien);
		$("#edit_bed_noreg_pasien").val(json.id);
		$("#edit_bed_nrm_pasien").val(json.nrm);
		$("#edit_bed_carabayar").val(json.carabayar);
		edit_bed.view();
	};
    
    nomor_bed=new TableAction("nomor_bed","kasir","edit_bed",new Array());
	nomor_bed.setSuperCommand("nomor_bed");
    nomor_bed.addRegulerData=function(a){
		a['ruangan']=$("#edit_bed_ruang").val();
		return a;
	};
	nomor_bed.selected=function(json){
		$("#edit_bed_id_bed").val(json.id);
		$("#edit_bed_nama_bed").val(json.nama);				
	};
	

	$("#edit_bed_ruang").change(function(){        
        edit_bed.view();
	});
    
    if($("#edit_bed_noreg_pasien").val()!=""){
        var noreg_pasien=$("#edit_bed_noreg_pasien").val();  
        px_edit_bed.select(noreg_pasien);
    }
	
});