$(document).ready(function(){
    master_kwitansi_rumus.history=function(id){
        var data=this.getRegulerData();
        data['action']="detail_master_kwitansi_rumus";
        data['id']=id;
        showLoading();
        $.post("",data,function(res){
            var json=getContent(res);
            $("#smis-help-modal .modal-body").html(json);
            $("#smis-help-modal .modal-header h3").html("Detail Tagihan Rumus");
            $("pre#pre_kwitansi_rumus").fetreefy();
            $("#smis-help-modal").smodal("show");
            dismissLoading();
        });
    }
});