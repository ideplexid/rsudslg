/**
 * this used for viewing the history of payment
 * this page automatically filter the corp. payment
 * 
 * @version     : 5.0.0
 * @since       : 14 May 2015
 * @author      : Nurul Huda
 * @copyright   : goblooge@gmail.com
 * @license     : LGPLv3
 * @used        : kasir/resource/php/laporan/lap_perusahaan.php
 * */
var lap_perusahaan;
var lap_perusahaan_perusahaan;

function reup(){
    $("#sub_menu_perusahaan_content").html("");	
    $("#sub_menu_perusahaan").hide("fast");
    $("#main_perusahaan").show("fast");
    lap_perusahaan.view();
}

function load_registration(noreg,perusahaan_json){
    var data={
            page:"kasir",
            action:"pembayaran_patient",
            super_command:"",
            prototype_name:"",
            prototype_slug:"",
            prototype_implement:"",
            list_registered_select:noreg
        };
    showLoading();
    $.post("",data,function(res){
        $("#sub_menu_perusahaan_content").html(res);	
        $("#sub_menu_perusahaan").show("fast");
        $("#main_perusahaan").hide("fast");
        dismissLoading();
    });
}


//var employee;
$(document).ready(function(){
    $(".mydatetime").datetimepicker({minuteStep:1});
    $('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
    lap_perusahaan_perusahaan=new TableAction("lap_perusahaan_perusahaan","kasir","lap_perusahaan",Array());
    lap_perusahaan_perusahaan.setSuperCommand("lap_perusahaan_perusahaan");
    lap_perusahaan_perusahaan.selected=function(json){
        $("#lap_perusahaan_perusahaan").val(json.nama);
    };

    var column=new Array("id","nilai","terklaim","waktu","keterangan","no_bukti","tgl_klaim");
    lap_perusahaan=new TableAction("lap_perusahaan","kasir","lap_perusahaan",column);
    lap_perusahaan.loading_kasir=function(id){
        var data=this.getRegulerData();
        data['command']="edit";
        data['id']=id;
        showLoading();
        $.post("",data,function(res){
            var json=getContent(res);
            var noreg=json.noreg_pasien;
            load_registration(noreg,json);
            dismissLoading();
        });
    };

    
    lap_perusahaan.getRegulerData=function(){
        var reg_data={	
                page:this.page,
                action:this.action,
                super_command:this.super_command,
                prototype_name:this.prototype_name,
                prototype_slug:this.prototype_slug,
                dari:$("#"+this.prefix+"_dari").val(),
                sampai:$("#"+this.prefix+"_sampai").val(),
                cair:$("#"+this.prefix+"_k_cair").val(),
                perusahaan:$("#"+this.prefix+"_perusahaan").val(),
                origin:$("#"+this.prefix+"_origin").val()
                };
        var dr=getFormattedTime(reg_data['dari']);
        var sp=getFormattedTime(reg_data['sampai']);
        var hsl=dr+" - "+sp;
        $("#tgl_lap_perusahaan_print").html(hsl);
        return reg_data;
    };
});