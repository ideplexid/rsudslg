var lap_bpjs;
var lap_bpjs_BPJS;

function reup_bpjs(){
    $("#sub_menu_BPJS_content").html("");	
    $("#sub_menu_BPJS").hide("fast");
    $("#main_BPJS").show("fast");
    lap_bpjs.view();
}

function load_registration_bpjs(noreg,BPJS_json){
    var data={
            page:"kasir",
            action:"pembayaran_patient",
            super_command:"",
            prototype_name:"",
            prototype_slug:"",
            prototype_implement:"",
            list_registered_select:noreg
        };
    showLoading();
    $.post("",data,function(res){
        $("#sub_menu_BPJS_content").html(res);	
        $("#sub_menu_BPJS").show("fast");
        $("#main_BPJS").hide("fast");
        dismissLoading();
    });
}


//var employee;
$(document).ready(function(){
    $(".mydatetime").datetimepicker({minuteStep:1});
    $('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
    lap_bpjs_BPJS=new TableAction("lap_bpjs_BPJS","kasir","lap_bpjs",Array());
    lap_bpjs_BPJS.setSuperCommand("lap_bpjs_BPJS");
    lap_bpjs_BPJS.selected=function(json){
        $("#lap_bpjs_BPJS").val(json.nama);
    };

    var column=new Array("id","nilai","terklaim","waktu","keterangan","no_bukti","tgl_klaim");
    lap_bpjs=new TableAction("lap_bpjs","kasir","lap_bpjs",column);
    lap_bpjs.loading_kasir=function(id){
        var data=this.getRegulerData();
        data['command']="edit";
        data['id']=id;
        showLoading();
        $.post("",data,function(res){
            var json=getContent(res);
            var noreg=json.noreg_pasien;
            load_registration_bpjs(noreg,json);
            dismissLoading();
        });
    };

    
    lap_bpjs.getRegulerData=function(){
        var reg_data={	
                page:this.page,
                action:this.action,
                super_command:this.super_command,
                prototype_name:this.prototype_name,
                prototype_slug:this.prototype_slug,
                dari:$("#"+this.prefix+"_dari").val(),
                sampai:$("#"+this.prefix+"_sampai").val(),
                cair:$("#"+this.prefix+"_k_cair").val(),
                BPJS:$("#"+this.prefix+"_BPJS").val(),
                origin:$("#"+this.prefix+"_origin").val()
                };
        var dr=getFormattedTime(reg_data['dari']);
        var sp=getFormattedTime(reg_data['sampai']);
        var hsl=dr+" - "+sp;
        $("#tgl_lap_bpjs_print").html(hsl);
        return reg_data;
    };
});