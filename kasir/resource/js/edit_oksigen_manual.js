var edit_oksigen_manual;
var px_edit_oksigen_manual;
$(document).ready(function(){
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
    $(".mydatetime").datetimepicker({minuteStep:1});
	var column=new Array('id','nama_pasien','nrm_pasien','jumlah_liter','biaya_lain');
	edit_oksigen_manual=new TableAction("edit_oksigen_manual","kasir","edit_oksigen_manual",column);
	edit_oksigen_manual.addRegulerData=function(a){
		a['noreg_pasien']=$("#edit_oksigen_manual_noreg_pasien").val();
		a['nrm_pasien']=$("#edit_oksigen_manual_nrm_pasien").val();
		a['nama_pasien']=$("#edit_oksigen_manual_nama_pasien").val();
		a['carabayar']=$("#edit_oksigen_manual_carabayar").val();
		a['ruang']=$("#edit_oksigen_manual_ruang").val();
		return a;
	};
    
    edit_oksigen_manual.addNoClear("nama_pasien");
    edit_oksigen_manual.addNoClear("noreg_pasien");
    edit_oksigen_manual.addNoClear("nrm_pasien");
    edit_oksigen_manual.addNoClear("carabayar");
    edit_oksigen_manual.addNoClear("ruang");
    
	edit_oksigen_manual.show_add_form=function(){
		if($("#edit_oksigen_manual_noreg_pasien").val()==""){
			showWarning("Peringatan","Silakan Pilih Pasien Dahulu");
			return;
		}
		this.clear();
		this.show_form();
	};	
	px_edit_oksigen_manual=new TableAction("px_edit_oksigen_manual","kasir","get_registered_patient",column);
	px_edit_oksigen_manual.setSuperCommand("px_edit_oksigen_manual");
	px_edit_oksigen_manual.selected=function(json){
		$("#edit_oksigen_manual_nama_pasien").val(json.nama_pasien);
		$("#edit_oksigen_manual_alamat_pasien").val(json.alamat_pasien);
		$("#edit_oksigen_manual_noreg_pasien").val(json.id);
		$("#edit_oksigen_manual_nrm_pasien").val(json.nrm);
		$("#edit_oksigen_manual_carabayar").val(json.carabayar);
		edit_oksigen_manual.view();
	};
	

	$("#edit_oksigen_manual_ruang").change(function(){        
        edit_oksigen_manual.view();
	});
    
    if($("#edit_oksigen_manual_noreg_pasien").val()!=""){
        var noreg_pasien=$("#edit_oksigen_manual_noreg_pasien").val();  
        px_edit_oksigen_manual.select(noreg_pasien);
    }
	
});