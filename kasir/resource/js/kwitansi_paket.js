function kwitansi_paket_update_pembayaran(){
	var kode=$("#kwitansi_paket_kode_inacbg").val();
	var desc=$("#kwitansi_paket_desc_inacbg").val();
	var harga=getMoney("#kwitansi_paket_harga");
	
	if(kode=="" || desc=="" || harga=="" || harga==0){
		showWarning("Peringatan"," Kode Inacbg, Deskripsi Inacbg dan Harga Tidak Boleh Kosong ");
		return;
	}
	
	var data={};
	data['page']="kasir";
	data['action']="list_registered";
	data['super_command']="update_tagihan_paket";
	data['prototype_name']="";
	data['prototype_slug']="";
	data['prototype_implement']="";
	data['noreg_pasien']=$("#noreg_pasien").val();
	data['nama_pasien']=$("#nama_pasien").val();
	data['nrm_pasien']=$("#nrm_pasien").val();
	data['kode']=kode;
	data['desc']=desc;
	data['harga']=harga;
	showLoading();
	$.post("",data,function(res){
		$("#kwitansi_paket").html(res);
		dismissLoading();
	});
}

$(document).ready(function(){
	formatMoney("#kwitansi_paket_harga");
});