/**
 * this function used to handle every 
 * single patient system crawler 
 * system so the patient will be got the whole 
 * price that he need to pay.
 * 
 * @author 		: Nurul Huda
 * @copyright 	: goblooge@gmail.com
 * @lisence 	: Propietary
 * @since 		: 12 Jan 2015
 * @version 	: 5.0.2
 * @used 		: /kasir/class/template/TotalTagihan.php
 * 
 * 
 * */

var lks_action;
var lks_pasien;
var lks_noreg;
var lks_nama_pasien;
var lks_nrm_pasien;
var lks_polislug;
var lks_the_page;
var lks_the_protoslug;
var lks_the_protoname;
var lks_the_protoimplement;
var lks_prefix;
var lks_invoke;
var lks_timeout;
var lks_autoskipper;
var skipper=false;
var lks_cancel=false;
$(document).ready(function() {	
	lks_action				= $("#lks_action").val();
	lks_noreg				= $("#lks_noreg").val();
	lks_nama_pasien			= $("#lks_nama_pasien").val();
	lks_nrm_pasien			= $("#lks_nrm_pasien").val();
	lks_polislug			= $("#lks_polislug").val();
	lks_the_page			= $("#lks_the_page").val();
	lks_the_protoslug		= $("#lks_the_protoslug").val();
	lks_the_protoname		= $("#lks_the_protoname").val();
	lks_the_protoimplement	= $("#lks_the_protoimplement").val();
	lks_prefix				= $("#lks_prefix").val();
	lks_invoke				= $("#lks_invoke").val();
	lks_timeout				= Number($("#lks_timeout").val());
	lks_autoskipper			= Number($("#lks_autoskipper").val());
	lks_cancel				= false;
	var needed=$("#total_responder_data").html();
	var result=$.parseJSON(needed);
	try{
		collect_tagihan(result.noreg_pasien,result.nama_pasien,result.nrm_pasien,result.id_asuransi,result.nama_asuransi,result.inap);	
	}catch(err) {
		console.log( err.message);
	}
	
});

function doSkipper(){
	skipper=true;
}

function doCancel(){
	lks_cancel=true;
}

/*FUNGSI KOLEKSI TAGIHAN*/		
function collect_tagihan(noreg,nama,nrm,id_asuransi,nama_asuransi,inap){
	var data={	
			page:lks_the_page,
			action:lks_action,
			super_command:"",
			prototype_name:lks_the_protoname,
			prototype_slug:lks_polislug,
			prototype_implement:lks_the_protoimplement
	};
	data['noreg']=noreg;
	data['noreg_pasien']=noreg;
	data['nama_pasien']=nama;
	data['nrm_pasien']=nrm;
	data['id_asuransi']=id_asuransi;
	data['nama_asuransi']=nama_asuransi;
	data['inap']=inap;
	data['command']="collect_tagihan";
	showLoading();
	$.post('',data,function(res){
		dismissLoading();
		var json=getContent(res);
		if(json==null){
			return;
		}else{
			$("#rekap_ksr_bar").sload("true","Loading Data",0);
			$("#rekap_ksr_modal").modal("show");
			$("#skipper").html("");
			$(".countdown_boundary").hide();
			collect_loop(json,0,noreg,nama,nrm,id_asuransi,nama_asuransi,inap,0);
		}
		
	});
}
/*END - FUNGSI KOLEKSI TAGIHAN*/


/*FUNGSI KOLEKSI TAGIHAN LOOP*/		
function collect_loop(set,curpoint,noreg,nama,nrm,id_asuransi,nama_asuransi,inap,fail){
	/* this code used if user cancel the process 
	 * then we done.*/
	var current=set[curpoint];
	var total=set.length;
	if(lks_cancel){
		$("#rekap_ksr_modal").modal("hide");
		update_total_tagihan(noreg);
		return;
	}
	/*beritahukan kondisi saat ini sedang merequest bagian yang mana...*/
	$("#rekap_ksr_bar").sload("true","[ "+curpoint+" / "+total+" ] "+current.name+"...",(curpoint*100/total));	
	var data={	
		page:lks_the_page,
		action:lks_action,
		super_command:"",
		prototype_name:lks_the_protoname,
		prototype_slug:lks_polislug,
		prototype_implement:lks_the_protoimplement
	};
	
	data['noreg']=noreg;
	data['noreg_pasien']=noreg;
	data['nama_pasien']=nama;
	data['nrm_pasien']=nrm;
	data['id_asuransi']=id_asuransi;
	data['nama_asuransi']=nama_asuransi;
	data['inap']=inap;
	data['fail']=fail;
	data['slug']=current.value;
	data['command']="collect_tagihan_unit";
	$.post("",data,function(res){
		var json=getContent(res);
		if(typeof(json['fail'])!='undefined' || Number(json['fail'])>=0){
			/* kondisi koneksi gagal dan terjadi error */
			skipper=false;
			$("#error_msg_crawler").html(json['msg']);
			$(".countdown_boundary").show();
			countDown(Number(json['fail'])*10,"#countdown_crawler",function(){
				$(".countdown_boundary").hide();
				var failure=Number(json['fail']) ;
				if(skipper){
					skipper=false;
					curpoint++;
					failure=0;
					$("#skipper").append("<li> ["+json.code+"] "+current.name+" Skipped By User </li>" );
				}else if(failure>=lks_autoskipper){
					skipper=false;
					curpoint++;
					failure=0;
					$("#skipper").append("<li> ["+json.code+"] "+current.name+" Skipped By System (Fail "+lks_autoskipper+"x) </li>" );
				}
				
				if(curpoint+1>=total){
					/*cek apakah selanjutnya masih ada lagi kalau sudah tidak ada berarti selesai*/
					$("#rekap_ksr_modal").modal("hide");
					update_total_tagihan(noreg);
					return;
				} else {
					collect_loop(set,curpoint,noreg,nama,nrm,id_asuransi,nama_asuransi,inap,failure);
				}
			});
		}else{
			/* kondisi koneksi berhasil dan tidak terjadi timeout */
			$("#layanan_table_list").append(json.row);
			if(curpoint+1>=total){
				/*cek selanjutnya apakah masih ada lagi jika tidak ada maka selesai*/
				$("#rekap_ksr_modal").modal("hide");
				update_total_tagihan(noreg);
				return;
			} else {
				curpoint++;
				setTimeout(function(){
					collect_loop(set,curpoint,noreg,nama,nrm,id_asuransi,nama_asuransi,inap,1);
				},lks_timeout);
			}
		}
		
	});
}
/*END - FUNGSI KOLEKSI TAGIHAN LOOP*/	

/*START - FUNCTION COUNDOWN TIMER*/

function countDown(timer,element,callback){
	timer--;
	$(element).html(timer);
	if(timer==0 || skipper){
		callback();
	}else{
		setTimeout(function(){
			countDown(timer,element,callback);
		},1000);
	}
}
/*END - FUNCTION COUNDOWN TIMER*/

/*START UPDATE TOTAL TAGIHAN*/
function update_total_tagihan(noreg){
	var data={	
			page:lks_the_page,
			action:lks_action,
			super_command:"",
			prototype_name:lks_the_protoname,
			prototype_slug:lks_polislug,
			prototype_implement:lks_the_protoimplement
	};
	data['noreg_pasien']=noreg;
	data['command']="update_total_tagihan";
	showLoading();
	$.post("",data,function(res){
		var func=window[lks_invoke];
		if(typeof(func)=="function"){
			func();
		}else{
			show_tagihan_kasir(noreg);
		}
		dismissLoading();
	});
}
/*END - START UPDATE TOTAL TAGIHAN*/
	

/*FUNGSI END LOOP MENAMPILKAN TAGIHAN*/
function show_tagihan_kasir(noreg){
	var data={	
			page:lks_the_page,
			action:lks_action,
			super_command:"",
			prototype_name:lks_the_protoname,
			prototype_slug:lks_polislug,
			prototype_implement:lks_the_protoimplement
	};
	data['noreg_pasien']=noreg;
	data['command']="show_kwitansi";
	showLoading();
	$.post("",data,function(res){
		$("#kwitansi_total_responder").html(res);
		dismissLoading();
	});
}
/*END - FUNGSI END LOOP MENAMPILKAN TAGIHAN*/


