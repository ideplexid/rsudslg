var bank_np;
$(document).ready(function(){
    $('.mydatetime').datetimepicker({ minuteStep: 1});
    $('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
    $("#bank_np_persen, #bank_np_nilai").on("change",function(){
        var nilai=Number(getMoney("#bank_np_nilai"));
        var persen=Number($("#bank_np_persen").val());
        var uang=nilai*persen/100;
        setMoney("#bank_np_nilai_tambahan",uang);
    });
    
    var column=new Array('id','waktu','nilai','keterangan','no_resep','nilai_tambahan','di_bank','id_bank','no_bukti');
    bank_np=new TableAction("bank_np","kasir","bank_np",column);
    bank_np.getRegulerData=function(){
        var reg_data = {	
                page            : this.page,
                action          : this.action,
                super_command   : this.super_command,
                prototype_name  : this.prototype_name,
                prototype_slug  : this.prototype_slug,
                metode          : "bank_np",
                id_np           : $("#tagihan_non_pasien_parent_id").val(),
                nama_pasien     : $("#tagihan_non_pasien_parent_nama").val(),
                nama_bank       : $("#"+this.prefix+"_id_bank option:selected").text(),
            };
        return reg_data;
    };
});