var urutan_kwitansi;
var IS_urutan_kwitansi_RUNNING;
var px_urutan_kwitansi;
$(document).ready(function(){
	var column=new Array("id","ruangan","id_unit","nama_tagihan","nama_grup","jenis_tagihan","kelas_by","nama_by","ruang_by","keterangan","tanggal","hidden","jaspel_by","nilai","nilai_by");
	urutan_kwitansi=new TableAction("urutan_kwitansi","kasir","urutan_kwitansi",column);
	urutan_kwitansi.setSuperCommand("urutan_kwitansi");
	urutan_kwitansi.addRegulerData=function(j){     
        j['nama_pasien']		= $("#urutan_kwitansi_nama_pasien").val();
		j['nrm_pasien']			= $("#urutan_kwitansi_nrm_pasien").val();
		j['noreg_pasien']		= $("#urutan_kwitansi_noreg_pasien").val();
		j['noreg_pasien']		= $("#urutan_kwitansi_noreg_pasien").val();
		return j;
	};
	urutan_kwitansi.up=function(id){
        var a=this.getRegulerData();
        a['command']="save";
        a['kondisi']="up";
        a['id']=id;
        showLoading();
        $.post("",a,function(res){
            var a=getContent(res);
            urutan_kwitansi.view();
            dismissLoading();
        });
      
    };
    
    urutan_kwitansi.down=function(id){
        var a=this.getRegulerData();
        a['command']="save";
        a['kondisi']="down";
        a['id']=id;
        showLoading();
        $.post("",a,function(res){
            var a=getContent(res);
            urutan_kwitansi.view();
            dismissLoading();
        });
      
    };
    px_urutan_kwitansi=new TableAction("px_urutan_kwitansi","kasir","urutan_kwitansi",column);
	px_urutan_kwitansi.setSuperCommand("px_urutan_kwitansi");
	px_urutan_kwitansi.selected=function(json){
		$("#urutan_kwitansi_nama_pasien").val(json.nama_pasien);
		$("#urutan_kwitansi_alamat_pasien").val(json.alamat_pasien);
		$("#urutan_kwitansi_noreg_pasien").val(json.id);
		$("#urutan_kwitansi_nrm_pasien").val(json.nrm);
		$("#urutan_kwitansi_carabayar").val(json.carabayar);
		urutan_kwitansi.view();
	};
    
});