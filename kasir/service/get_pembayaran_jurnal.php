<?php
	global $db;
	
	if (isset($_POST['noreg_pasien'])) {
		$noreg_pasien = $_POST['noreg_pasien'];
		
		$dbtable = new DBTable($db, "smis_ksr_bayar");
		$rows = $dbtable->get_result("
			SELECT *
			FROM smis_ksr_bayar
			WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop NOT LIKE 'del'
		");
		$tunai = 0;
		$tagihan = 0;
		$bri_tunai = 0;
		$bri_edc = 0;
		$mandiri_tunai = 0;
		$mandiri_edc = 0;
		$bca_tunai = 0;
		$bca_edc = 0;
		$diskon = 0;
		foreach ($rows as $row) {
			if ($row->metode == "cash" || $row->metode == "cash_resep") 
				$tunai += $row->nilai;
			else if ($row->metode == "asuransi")
				$tagihan += $row->nilai;
			else if ($row->metode == "bank") {
				if (strpos($row->nama_bank, "Mandiri") != "") {
					if ($row->keterangan != "Setoran Tunai")
						$mandiri_edc += $row->nilai;
					else
						$mandiri_tunai += $row->nilai;
				} else if (strpos($row->nama_bank, "BRI") != "") {
					if ($row->keterangan != "Setoran Tunai")
						$bri_edc += $row->nilai;
					else
						$bri_tunai += $row->nilai;
				} else if (strpos($row->nama_bank, "BCA") != "") {
					if ($row->keterangan != "Setoran Tunai")
						$bca_edc += $row->nilai;
					else
						$bca_tunai += $row->nilai;
				} 
			} else if ($row->metode == "diskon")
				$diskon += $row->nilai;
		}
		
		$data = array();
		$data['data'] = array(
			"tunai"			=> $tunai,
			"tagihan"		=> $tagihan,
			"bri_tunai"		=> $bri_tunai,
			"bri_edc"		=> $bri_edc,
			"mandiri_tunai"	=> $mandiri_tunai,
			"mandiri_edc"	=> $mandiri_edc,
			"bca_tunai"		=> $bca_tunai,
			"bca_edc"		=> $bca_edc,
			"diskon"		=> $diskon
		);
		echo json_encode($data);
	}
?>