<?php
	global $db;
	
	if (isset($_POST['noreg_pasien'])) {
		$noreg_pasien = $_POST['noreg_pasien'];
		$filter = "WHERE prop NOT LIKE 'del' AND noreg_pasien = '" . $noreg_pasien . "'";
		if (isset($_POST['criteria']) && isset($_POST['criteria_conjunction'])) {
			$criteria = $_POST['criteria'];
			$conjunction = $_POST['criteria_conjunction'];
			$filter .= " AND (";
			foreach ($criteria as $k => $v) {
				$filter .= $k . " LIKE '" . $v . "' " . $criteria_conjunction;
			}
			$filter = rtrim($filter, " " . $criteria_conjunction) . ") ";
		}
		$dbtable = new DBTable($db, "smis_ksr_tagihan");
		$row = $dbtable->get_row("
			SELECT SUM(total) AS 'nominal'
			FROM smis_ksr_tagihan
			" . $filter . "
		");
		$nominal = 0;
		if ($row != null)
			$nominal = $row->nominal;
		$data = array();
		$data['data'] = array(
			"ruangan"	=> "kasir",
			"nominal"	=> $nominal
		);
		echo json_encode($data);
	}
?>