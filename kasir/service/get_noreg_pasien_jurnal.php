<?php
	global $db;
	
	if (isset($_POST['from']) 
	 && isset($_POST['to']) 
	 && isset($_POST['operator']) 
	 && isset($_POST['metode'])
	 && isset($_POST['command'])) {
		$from = $_POST['from'];
		$to = $_POST['to'];
		$operator = $_POST['operator'];
		$metode = $_POST['metode'];
		$command = $_POST['command'];
		$dbtable = new DBTable($db, "smis_ksr_bayar");
		if ($command == "get_number") {
			$row = $dbtable->get_row("
				SELECT COUNT(*) AS 'jumlah'
				FROM (
					SELECT DISTINCT noreg_pasien
					FROM smis_ksr_bayar
					WHERE prop NOT LIKE 'del' AND waktu >= '" . $from . "' AND waktu <= '" . $to . "' AND operator LIKE '" . $operator . "' AND metode LIKE '" . $metode . "'
				) v_bayar	
			");
			$data = array();
			$data['jumlah'] = 0;
			if ($row != null)
				$data['jumlah'] = $row->jumlah;
			echo json_encode($data);
		} else if ($command == "get_info" && isset($_POST['num'])) {
			$num = $_POST['num'];
			$row = $dbtable->get_row("
				SELECT DISTINCT noreg_pasien
				FROM smis_ksr_bayar
				WHERE prop NOT LIKE 'del' AND waktu >= '" . $from . "' AND waktu <= '" . $to . "' AND operator LIKE '" . $operator . "' AND metode LIKE '" . $metode . "'
				LIMIT " . $num . ", 1
			");
			$data = array();
			$data['noreg_pasien'] = $row->noreg_pasien;
			echo json_encode($data);
		}
	}
?>