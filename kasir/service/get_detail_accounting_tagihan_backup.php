<?php 
    global $db;
    $id      = $_POST['data'];
    
    $dbtable = new DBTable($db,"smis_ksr_tagihan");
    $x = $dbtable ->selectEventDel($id);
    
    //content untuk kredit
    $kredit=array();
    $kredit['akun']     = $x->kredit;
    $kredit['debet']    = 0;
    $kredit['kredit']   = $x->total;
    $kredit['ket']      = "Pendapatan Tambahan Kasir : ".$x->nama_tagihan." untuk pasien ".$x->nama_pasien." dengan No.Reg ".$x->noreg_pasien;
    $kredit['code']     = "kredit-backup-kasir-".$x->id;

    //content untuk debet
    $debet=array();
    $debet['akun']    = $x->debet;
    $debet['debet']   = $x->total;
    $debet['kredit']  = 0;
    $debet['ket']     = "Piutang Tambahan Kasir : ".$x->nama_tagihan." untuk pasien ".$x->nama_pasien." dengan No.Reg ".$x->noreg_pasien;
    $debet['code']    = "debet-backup-kasir-".$x->id;
    
    $list   = array();
    $list[] = $debet;
    $list[] = $kredit;

    //content untuk header
    $header=array();
    $header['tanggal']      = $x->tanggal;
    $header['keterangan']   = "Pendapatan Pasien tambahan dari Kasir ".$x->nama_tagihan." atas pasien ".$x->nama_pasien." dengan No.Reg ".$x->noreg_pasien;
    $header['code']         = "backup-kasir-".$x->id;
    $header['nomor']        = "TBK-kasir-".$x->id;
    $header['debet']        = $x->total;
    $header['kredit']       = $x->total;
    $header['io']           = "1";
    
    $transaction_karcis=array();
    $transaction_karcis['header']=$header;
    $transaction_karcis['content']=$list;
    
    $final=array();
    $final[]=$transaction_karcis;
        
    echo json_encode($final);
    
    /*notif that already change or crawler by akunting*/
    $update['akunting']=1;
    $id['id']=$_POST['data'];
    $dbtable->update($update,$id);
?>