<?php 
global $db;
loadLibrary("smis-libs-function-medical");
$noreg=$_POST['noreg_pasien'];
$response=array();
$query="SELECT ruangan,jenis_tagihan as layanan, SUM(nilai) as total FROM smis_ksr_tagihan WHERE noreg_pasien='".$noreg."' AND prop!='del' GROUP BY ruangan,jenis_tagihan";
$data=$db->get_result($query);
$response=array();
foreach($data as $one){
	$x=array();
	$x['layanan']=medical_layanan_mapping($one->layanan);
	$x['ruangan']=$one->ruangan;
	$x['nilai']=$one->total;
	$response[]=$x;
}


$query="SELECT id, metode, terklaim, SUM(nilai) as total FROM smis_ksr_bayar WHERE noreg_pasien='".$noreg."' AND prop!='del' GROUP BY metode, terklaim ";
$data=$db->get_result($query);
foreach($data as $one){
	$x=array();
	$x['layanan']=$one->metode;	
	if($one->metode=="asuransi" && $one->terklaim=="1"){
		$x['layanan']="asuransi_lunas";
	}else if($one->metode=="asuransi" && $one->terklaim=="0"){
		$x['layanan']="asuransi_belum_lunas";
	}
	$x['ruangan']="kasir";
	$x['nilai']=$one->total;
	$x['no_kwitansi']=$one->id;
	$response[]=$x;
}


echo json_encode($response);
?>