<?php 
    global $db;
    $id      = $_POST['data'];
    
    $dbtable = new DBTable($db,"smis_ksr_bayar");
    $x = $dbtable ->selectEventDel($id);
    $crby=ArrayAdapter::slugFormat("unslug",$x->carabayar);
    //content untuk kredit
    $kredit=array();
    $kredit['akun']     = getSettings($db,"cashier-acc-k-bank-".$x->id_bank,"");
    $kredit['debet']    = 0;
    $kredit['kredit']   = $x->nilai;
    $kredit['ket']      = "Pendapatan Bank ".$x->nama_bank." (".$x->no_bukti.") Pasien ".$crby." di Kasir - ".$x->nama_pasien." dengan No.Reg ".$x->noreg_pasien." dan No Kwitansi ".$x->no_kwitansi;
    $kredit['code']     = "kredit-bank-kasir-".$x->id;

    //content untuk debet
    $debet=array();
    $debet['akun']    = getSettings($db,"cashier-acc-d-bank-".$x->id_bank,"");
    $debet['debet']   = $x->nilai;
    $debet['kredit']  = 0;
    $debet['ket']     = "Uang  Bank ".$x->nama_bank." (".$x->no_bukti.") Pasien ".$crby." di Kasir - ".$x->nama_pasien." dengan No.Reg ".$x->noreg_pasien." dan No Kwitansi ".$x->no_kwitansi;
    $debet['code']    = "debet-bank-kasir-".$x->id;
    
    $list   = array();
    $list[] = $debet;
    $list[] = $kredit;

    //content untuk header
    $header=array();
    $header['tanggal']      = $x->waktu;
    $header['keterangan']   = "Pembayaran Bank ".$x->nama_bank." (".$x->no_bukti.") Pasien ".$crby." di Kasir ".$x->nama_pasien." dengan No.Reg ".$x->noreg_pasien." dan No Kwitansi ".$x->no_kwitansi;
    $header['code']         = "bank-kasir-".$x->id;
    $header['nomor']        = "BKP-".$x->id;
    $header['debet']        = $x->nilai;
    $header['kredit']       = $x->nilai;
    $header['io']           = "1";
    
    $transaction=array();
    $transaction['header']=$header;
    $transaction['content']=$list;
    
    $final=array();
    $final[]=$transaction;
        
    echo json_encode($final);
    
    /*notif that already change or crawler by akunting*/
    $update['akunting']=1;
    $id['id']=$_POST['data'];
    $dbtable->update($update,$id);
?>