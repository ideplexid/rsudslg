<?php 
    global $db;
    $id      = $_POST['data'];
    
    $dbtable = new DBTable($db,"smis_ksr_bayar");
    $x = $dbtable ->selectEventDel($id);
    $crby           = ArrayAdapter::slugFormat("unslug",$x->carabayar);
    $no_kwitansi    = $x->no_kwitansi==""?$x->id:$x->no_kwitansi;
    
    //content untuk kredit
    $kredit=array();
    $kredit['akun']     = getSettings($db,"cashier-acc-k-cash-resep-".$x->carabayar,"");
    $kredit['debet']    = 0;
    $kredit['kredit']   = $x->nilai;
    $kredit['ket']      = "Pendapatan Cash Resep Pasien ".$crby." Kasir - ".$x->nama_pasien." dengan No.Reg ".$x->noreg_pasien." dan No Kwitansi ".$no_kwitansi;
    $kredit['code']     = "kredit-cash-resep-kasir-".$x->id;

    //content untuk debet
    $debet=array();
    $debet['akun']    = getSettings($db,"cashier-acc-d-cash-resep-".$x->carabayar,"");
    $debet['debet']   = $x->nilai;
    $debet['kredit']  = 0;
    $debet['ket']     = "Uang Cash Pasien Resep ".$crby." Kasir - ".$x->nama_pasien." dengan No.Reg ".$x->noreg_pasien." dan No Kwitansi ".$no_kwitansi;
    $debet['code']    = "debet-cash-resep-kasir-".$x->id;
    
    $list   = array();
    $list[] = $debet;
    $list[] = $kredit;

    //content untuk header
    $header=array();
    $header['tanggal']      = $x->waktu;
    $header['keterangan']   = "Pembayaran Cash Resep Pasien ".$crby." Kasir ".$x->nama_pasien." dengan No.Reg ".$x->noreg_pasien." dan No Kwitansi ".$no_kwitansi;
    $header['code']         = "cash-resep-kasir-".$x->id;
    $header['nomor']        = "CRKP-".$x->id;
    $header['debet']        = $x->nilai;
    $header['kredit']       = $x->nilai;
    $header['io']           = "1";
    
    $transaction=array();
    $transaction['header']=$header;
    $transaction['content']=$list;
    
    $final=array();
    $final[]=$transaction;
        
    echo json_encode($final);
    
    /*notif that already change or crawler by akunting*/
    $update['akunting'] = 1;
    $id['id']           = $_POST['data'];
    $dbtable->update($update,$id);
?>