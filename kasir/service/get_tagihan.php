<?php
global $db;
$noreg                      = $_POST ['noreg_pasien'];

//tambahan biaya
$dbtable                    = new DBTable($db, "smis_ksr_tagihan");
$dbtable->setShowAll(true);
$dbtable->addCustomKriteria("noreg_pasien", "='".$noreg."'" );
$dbtable->setOrder(" jenis_tagihan ASC ");
$d                          = $dbtable->view("", "0");
$list                       = $d['data'];

//surat sakit
$dbtable                    = new DBTable($db, "smis_ksr_sks");
$dbtable->setShowAll(true);
$dbtable->addCustomKriteria("noreg_pasien", "='".$noreg."'" );
$d                          = $dbtable->view("", "0");
$list_ss                    = $d['data'];

$total                      = count($list)+count($list_ss);
$response ['selesai']       = "1";
$response ['exist']         = $total>0?"1":"0";
$response ['reverse']       = "0";
$response ['cara_keluar']   = "Selesai";
$data                       = array();
$result                     = array ();
$curjenis                   = "";


foreach($list as $one){
	if($curjenis=="" || $curjenis!=$one->jenis_tagihan){
		if($curjenis!=""){
			$data[$curjenis]=array(
					"jasa_pelayanan"    => "0",
					"result"            => $result	
				);
			$result=array();
		}
		$curjenis   = $one->jenis_tagihan;
	}
	$oneresult                      = array();
	$oneresult['waktu']             = ArrayAdapter::format("date d M Y", $one->tanggal);
	$oneresult['start']             = $one->tanggal;
	$oneresult['end']               = $one->tanggal;
	$crawler_mode                   = getSettings($db, "cashier-activate-crawler-cashier", "0");
	if($crawler_mode=="1"){
		/* Crawler per Item 
		 * Biaya adalah Nilai dan Jumlah adalah Jumlah
		 * artinya jumlah dan biaya ditampilkan sendiri*/
		$oneresult['nama']          = $one->nama_tagihan . " (" . ArrayAdapter::format("unslug", $one->ruangan) . ")";
		$oneresult['biaya']         = $one->nilai ;
		$oneresult['jumlah']        = $one->jumlah;
		$oneresult['keterangan']    = "Tagihan dari ".ArrayAdapter::format("unslug", $one->ruangan)." , ".$one->keterangan.", ( ".($one->jaspel=="1"?"J":"NJ")." ) ".$one->jumlah." x ".ArrayAdapter::format("only-money Rp.", $one->nilai);
	}else if($crawler_mode=="2"){
		/* Crawler per Item Per Summary 
		 * biaya adalah Nilai * Jumlah,
		 * sedangkan Jumlah adalah Jumlah
		 * sehingga pemanggil service untuk mendapatkan nilai 
		 * satuanya harus dibagi dulu dengan jumlah data*/
		$oneresult['nama']          = $one->nama_tagihan . " (" . ArrayAdapter::format("unslug", $one->ruangan) . ")";
		$oneresult['biaya']         = $one->nilai * $one->jumlah;
		$oneresult['jumlah']        = $one->jumlah;
		$oneresult['keterangan']    = "Tagihan dari ".ArrayAdapter::format("unslug", $one->ruangan)." , ".$one->keterangan.", ( ".($one->jaspel=="1"?"J":"NJ")." ) ".$one->jumlah." x ".ArrayAdapter::format("only-money Rp.", $one->nilai);
	}else{
		/* Crawler Summary Item
		 * biaya adalah Nilai * Jumlah , sedangkan jumlah dianggap 1.
		 * sehingga untuk mendapatkan nilai per itemnya tidak bisa */
		$oneresult['nama']          = $one->nama_tagihan . " (" . ArrayAdapter::format("unslug", $one->ruangan) . ") x ".$one->jumlah;
		$oneresult['biaya']         = $one->nilai * $one->jumlah;
		$oneresult['jumlah']        = 1;
		$oneresult['keterangan']    = "Tagihan dari ".ArrayAdapter::format("unslug", $one->ruangan)." , ".$one->keterangan.", ( ".($one->jaspel=="1"?"J":"NJ")." ) ".$one->jumlah." x ".ArrayAdapter::format("only-money Rp.", $one->nilai);
	}
	$oneresult['id']                = $one->id; 
	$oneresult['ruangan_kasir']     = $one->ruangan;
	$oneresult['jaspel']            = $one->jaspel;
    $oneresult['debet']             = $one->debet;
	$oneresult['kredit']            = $one->kredit;
	$oneresult['urjigd']        = $one->uri=="1"?"URI":($one->ruangan=="igd"?"IGD":"URJ");
	$oneresult['tanggal_tagihan'] = substr($one->tanggal,0,10);
	$result[]                       = $oneresult;
}

if(count($result)>0){
	$data[$curjenis]=array(
			"jasa_pelayanan"        => "0",
			"result"                => $result
	);
	$result=array();
}

/*surat_sakit*/
if(!isset($data['umum'])){
    $data['umum']=array(
			"jasa_pelayanan"        => "0",
			"result"                => array()
	);
	$result=array();
}
foreach($list_ss as $one){
	$oneresult                  = array();
	$oneresult['waktu']         = ArrayAdapter::format("date d M Y", $one->tanggal);
	$oneresult['start']         = $one->tanggal;
	$oneresult['end']           = $one->tanggal;
	$oneresult['nama']          = "Surat Keterangan Sakit";
	$oneresult['biaya']         = $one->tarif ;
	$oneresult['jumlah']        = "1";
	$oneresult['keterangan']    = "Tagihan dari ".ArrayAdapter::format("unslug", $one->ruangan);
	$oneresult['id']            = $one->id."_sks"; 
	$oneresult['ruangan_kasir'] = $one->ruangan;
	$oneresult['jaspel']        = 0;
    $oneresult['debet']         = getSettings($db,"cashier-surat-sakit-debet","");
	$oneresult['kredit']        = getSettings($db,"cashier-surat-sakit-kredit","");
	
	
    

	$data['umum']['result'][]   = $oneresult;
}

$response['data']                   = $data;
echo json_encode($response);


?>