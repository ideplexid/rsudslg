<?php 
global $db;

require_once 'smis-base/smis-include-service-consumer.php';
$urjip=new ServiceConsumer($db, "get_urjip",array());
$urjip->setMode(ServiceConsumer::$MULTIPLE_MODE);
$urjip->setCached(true,"get_urjip");
$urjip->execute();
$content=$urjip->getContent();
$ruangan=array();
foreach ($content as $autonomous=>$ruang){
	foreach($ruang as $nama_ruang=>$jip){
		if($jip[$nama_ruang]=="URI" || $jip[$nama_ruang]=="URJI"){
			$ruangan[]=$nama_ruang;
		}
	}
}

$awal=$_POST['awal'];
$akhir=$_POST['akhir'];
$origin=isset($_POST['origin'])?$_POST['origin']:"%";
$response=array();

/*tambahan biaya*/
$query="SELECT ruangan, jenis_tagihan as layanan, SUM(nilai) as total 
        FROM smis_ksr_tagihan 
        WHERE tanggal>='$awal' 
        AND tanggal<'$akhir'
        AND origin LIKE '".$origin."' 
        AND prop!='del' 
        GROUP BY ruangan,jenis_tagihan";
$data=$db->get_result($query);
foreach($data as $one){
	$x=array();
	$x['layanan']=$one->layanan;
	$x['ruangan']=$one->ruangan;
	$x['urjip']=in_array($one->ruangan, $ruangan)?"uri":"urj";
	$x['nilai']=$one->total;
	$response[]=$x;
}

/*surat sakit*/
$query="SELECT ruangan, 'umum' as layanan, SUM(tarif) as total 
        FROM smis_ksr_sks 
        WHERE tanggal>='$awal' 
        AND tanggal<'$akhir'
        AND origin LIKE '".$origin."' 
        AND prop!='del' 
        GROUP BY ruangan,jenis_tagihan";
$data=$db->get_result($query);
foreach($data as $one){
	$x=array();
	$x['layanan']=$one->layanan;
	$x['ruangan']=$one->ruangan;
	$x['urjip']=in_array($one->ruangan, $ruangan)?"uri":"urj";
	$x['nilai']=$one->total;
	$response[]=$x;
}

echo json_encode($response);
?>