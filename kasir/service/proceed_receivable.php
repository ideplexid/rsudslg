<?php 

/**langsung join rg_layanapasien */


show_error();
global $db;
$_grup_name=$_POST['grup_name'];
$_nama_pasien=$_POST['nama_pasien'];
$_noreg_pasien=$_POST['noreg_pasien'];
$_nrm_pasien=$_POST['nrm_pasien'];
$_entity=$_POST['entity'];

$_jaspel_persen=getSettings($db, "cashier-jaspel-persen", "10");

$dbtable = new DBTable($db,"smis_rg_layananpasien");
$register = $dbtable ->select($_noreg_pasien);


$dbtable=new DBTable($db,"smis_ksr_kolektif");
$list=$_POST['list'];
loadLibrary("smis-libs-function-medical");
$tipe=medical_service_slug();	
$replace=array();
foreach($tipe as $tp){
    $replace[$tp] = getSettings($db, "cashier-replace-tipe-".$tp, $tp);
    $replace[$tp."-urutan"] = getSettings($db, "cashier-replace-order-".$tp, "1");
}
$jaspel_jenis = getSettings($db, "cashier-jaspel-".$_grup_name, "0") == "1";
$__RESULT=0;
foreach($list as $x){
    
    /** special case for one record but have different group name 
     *  like operation, administration and registration bill **/
     
    if(isset($x['grup_name']) && $x['grup_name']!="" ){
       $_grup_name=$x['grup_name'];
    }else{
       $_grup_name=$_POST['grup_name'];
    }
    
    
    $save['tanggal_masuk']      = substr($register->tanggal,0,10); 
    $save['nama_grup']          = isset($replace[$_grup_name]) ? $replace[$_grup_name] : $_grup_name; 
    $save['nama_pasien']        = $_nama_pasien; 
    $save['id_unit']            = $x['id']; 
    $save['noreg_pasien']       = $_noreg_pasien; 
    $save['nrm_pasien']         = $_nrm_pasien; 
    $save['jenis_tagihan']      = $_grup_name; 
    $save['nama_tagihan']       = $x ['nama']; 
    $save['nama_by']            = $x ['nama']; 
    $save['ruang_by']           = $_entity; 
    $save['nilai_by']           = $x ['biaya']; 
    $save['dari']               = $x['start']; 
    $save['sampai']             = $x ['end']; 
    $save['ruangan_map']        = getSettings($db, "cashier-map-area-".$_entity, $_entity); 
    $save['ruangan']            = $_entity; 
    $save['urutan']             = $replace[$_grup_name."-urutan"]; 
    $save['ruangan_kasir']      = isset($x['ruangan_kasir'])?$x['ruangan_kasir']:$_entity; 
    $save['keterangan']         = $x['keterangan']; 
    $save['tanggal']            = $x['waktu']; 
    $save['nilai']              = $x['biaya']; 
    $save['quantity']           = isset($x['jumlah']) ? $x['jumlah'] : 1;
    
    

    $save['nama_dokter']        = isset($x['dokter'])?$x['dokter']:(isset($x['nama_dokter'])?$x['nama_dokter']:""); 
    $save['id_dokter']          = isset($x['id_dokter'])?$x['id_dokter']:"0"; 
    $save['jaspel_dokter']      = isset($x['jaspel_dokter'])?$x['jaspel_dokter']:"0"; 
    $save['jaspel_persen']      = isset($x['jaspel_persen'])?$x['jaspel_persen']:"0"; 

    $save['jaspel_persen_perawat']      = isset($x['jaspel_persen_perawat'])?$x['jaspel_persen_perawat']:"0"; 
    $save['jaspel_perawat']      = isset($x['jaspel_perawat'])?$x['jaspel_perawat']:"0"; 

    $save['urjigd']             = isset($x['urjigd'])?$x['urjigd']:"urj"; 
    $save['tanggal_tagihan']    = isset($x['tanggal_tagihan'])?$x['tanggal_tagihan']:""; 

    $save['prop']               = isset($x['prop'])?$x['prop']:"";
    $save['debet']              = $x['debet'];
    $save['kredit']             = $x['kredit'];
    
    /* jika dari atau sampainya yang terbaru malah kosong
     * mendhing sebaiknya dibiarkan kemungkinan petugas lupa 
     * di set up ulang bed */
    if($save['dari']    == "") unset($save['dari']);
    if($save['sampai']  == "") unset($save['sampai']);
        
    if(is_array($save['keterangan'])) {
        $save['keterangan'] = json_encode($save['keterangan']);
    }
    
    /* khusus jika ada jasa pelayanan di ruangan yang mana diambil 10% 
     * dari tagihan yang di jaspelkan, tetapi 10% bisa di set di masing-masing
     * jenis tagihan melalui menu kasir */
    $save['nilai']              = $x ['biaya'];
    $save['jaspel']             = $x['jaspel'] == "1" ? "1" : "0";//$econtent ['jasa_pelayanan'];
    $save['total']              = $x['biaya'] * (100 + $save['jaspel'] * $_jaspel_persen) / 100;
    $save['akunting_nama']      = isset($x['akunting_nama'])?$x['akunting_nama']:$save['nama_tagihan'];
    $save['akunting_only']      = isset($x['akunting_only'])?$x['akunting_only']:"0";
    $save['akunting_nilai']     = isset($x['akunting_nilai'])?$x['akunting_nilai']:$save['total'];
                            
    /* dipakai untuk memastikan uniq tidaknya sebuah data.
     * id_unit adalah id row dari setiap table ruangan masing-masing
     * noreg_pasien adalah noreg dari seorang pasien
     * jenis_tagihan adalah jenis tagihanya pakaha tindakan perawat, tindakan dokter
     * ruangan adalah nama ruanganya */
    $up = array(
        'id_unit' => $x['id'],
        'noreg_pasien' => $_noreg_pasien,
        'jenis_tagihan' => $_grup_name,	
        'ruangan' => $_entity,
    );
    
    
    if($dbtable->is_exist($up,true)){
        /* jika id_unit sama, noreg_pasien , jenis tagihan ada, ruanganya juga ada
         * berarti udah exist tinggal di aktifkan kembali */
        $proses=$dbtable->update($save, $up);
        if($proses!==false){
            $__RESULT++;
        }
    }else{
        /* ini berarti melakukan saving data baru */
        $save['nilai_by'] = $x['biaya'];
        $save['jaspel_by'] = $jaspel_ruang && $jaspel_jenis? "1" : "0";
        $proses=$dbtable->insert($save);
        if($proses!==false){
            $__RESULT++;
        }
    }
}

if($__RESULT==count($list)){
    $__RESULT=1;
}else{
    $__RESULT=0;
}
echo json_encode($__RESULT);

/* berfungsi untuk melakukan service ke kasir agar datanya di cache untuk tagihan */
require_once "kasir/snippet/update_total_tagihan.php";



?>