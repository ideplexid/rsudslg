<?php 
    global $db;
    $id      = $_POST['data'];
    
    $dbtable = new DBTable($db,"smis_ksr_bayar");
    $x = $dbtable ->selectEventDel($id);
    $crby=ArrayAdapter::slugFormat("unslug",$x->carabayar);
    $kode_acc_asuransi = $x->id_asuransi*1!=0?$x->id_asuransi:$x->carabayar ;
    
    //content untuk kredit
    $kredit=array();
    $kredit['akun']     = getSettings($db,"cashier-acc-k-asuransi-".$kode_acc_asuransi,"");
    $kredit['debet']    = 0;
    $kredit['kredit']   = $x->nilai;
    $kredit['ket']      = "Pendapatan Asuransi ".$x->nama_asuransi." (".$x->no_bukti.") Pasien ".$crby." di Kasir - ".$x->nama_pasien." dengan No.Reg ".$x->noreg_pasien." dan No Kwitansi ".$x->no_kwitansi;
    $kredit['code']     = "kredit-asuransi-kasir-".$x->id;

    //content untuk debet
    $debet=array();
    $debet['akun']    = getSettings($db,"cashier-acc-d-asuransi-".$kode_acc_asuransi,"");
    $debet['debet']   = $x->nilai;
    $debet['kredit']  = 0;
    $debet['ket']     = "Piutang Asuransi ".$x->nama_asuransi." (".$x->no_bukti.") Pasien ".$crby." di Kasir - ".$x->nama_pasien." dengan No.Reg ".$x->noreg_pasien." dan No Kwitansi ".$x->no_kwitansi;
    $debet['code']    = "debet-asuransi-kasir-".$x->id;
    
    $list   = array();
    $list[] = $debet;
    $list[] = $kredit;

    //content untuk header
    $header=array();
    $header['tanggal']      = $x->waktu;
    $header['keterangan']   = "Pembayaran Asuransi ".$x->nama_asuransi." (".$x->no_bukti.") Pasien ".$crby." di Kasir ".$x->nama_pasien." dengan No.Reg ".$x->noreg_pasien." dan No Kwitansi ".$x->no_kwitansi;
    $header['code']         = "asuransi-kasir-".$x->id;
    $header['nomor']        = "AKP-".$x->id;
    $header['debet']        = $x->nilai;
    $header['kredit']       = $x->nilai;
    $header['io']           = "1";
    
    $transaction=array();
    $transaction['header']=$header;
    $transaction['content']=$list;
    
    $final=array();
    $final[]=$transaction;
        
    echo json_encode($final);
    
    /*notif that already change or crawler by akunting*/
    $update['akunting']=1;
    $id['id']=$_POST['data'];
    $dbtable->update($update,$id);
?>