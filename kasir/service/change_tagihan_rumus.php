<?php 
global $db;
loadLibrary("smis-libs-function-time");

$up=array();
$up['noreg_pasien']=$_POST['noreg_pasien'];

$data=array();
$data['nama_pasien']        = $_POST['nama_pasien'];
$data['nrm_pasien']         = $_POST['nrm_pasien'];
$data['noreg_pasien']       = $_POST['noreg_pasien'];
$data['kelas_plafon']       = $_POST['kelas_plafon'];
$data['kode_inacbg']        = $_POST['kode_inacbg'];
$data['ruang_pelayanan']    = $_POST['ruang_pelayanan'];
$data['id_rumus']           = $_POST['id_rumus'];
$data['nama_rumus']         = $_POST['nama_rumus'];
$data['deskripsi_inacbg']   = $_POST['deskripsi_inacbg'];
$data['rumus_tagihan']      = $_POST['rumus_tagihan'];
$data['rumus_biaya']        = $_POST['rumus_biaya'];
$data['rumus_potongan']     = $_POST['rumus_potongan'];
$data['tarif_plafon_inacbg']= $_POST['tarif_plafon_inacbg'];
$data['tarif_plafon_kelas1']= $_POST['tarif_plafon_kelas1'];
$data['id_dpjp_satu']       = ($_POST['nama_dpjp_satu']=="")?0:$_POST['id_dpjp_satu'];
$data['nama_dpjp_satu']     = $_POST['nama_dpjp_satu'];
$data['biaya_dpjp_satu']    = $_POST['biaya_dpjp_satu'];
$data['id_dpjp_dua']        = ($_POST['nama_dpjp_dua']=="")?0:$_POST['id_dpjp_dua'];
$data['nama_dpjp_dua']      = $_POST['nama_dpjp_dua'];
$data['biaya_dpjp_dua']     = $_POST['biaya_dpjp_dua'];
$data['id_dpjp_tiga']       = ($_POST['nama_dpjp_tiga']=="")?0:$_POST['id_dpjp_tiga'];
$data['nama_dpjp_tiga']     = $_POST['nama_dpjp_tiga'];
$data['biaya_dpjp_tiga']    = $_POST['biaya_dpjp_tiga'];
$data['id_konsul_satu']     = ($_POST['nama_konsul_satu']=="")?0:$_POST['id_konsul_satu'];
$data['nama_konsul_satu']   = $_POST['nama_konsul_satu'];
$data['id_konsul_dua']      = ($_POST['nama_konsul_dua']=="")?0:$_POST['id_konsul_dua'];
$data['nama_konsul_dua']    = $_POST['nama_konsul_dua'];
$data['id_anastesi']        = ($_POST['nama_anastesi']=="")?0:$_POST['id_anastesi'];
$data['nama_anastesi']      = $_POST['nama_anastesi'];
$data['id_anak']            = ($_POST['nama_anak']=="")?0:$_POST['id_anak'];
$data['nama_anak']          = $_POST['nama_anak'];
$data['id_spesialis']       = ($_POST['nama_spesialis']=="")?0:$_POST['id_spesialis'];
$data['nama_spesialis']     = $_POST['nama_spesialis'];
$data['id_umum']            = ($_POST['nama_umum']=="")?0:$_POST['id_umum'];
$data['nama_umum']          = $_POST['nama_umum'];
$data['id_igd']             = ($_POST['nama_igd']=="")?0:$_POST['id_igd'];
$data['nama_igd']           = $_POST['nama_igd'];
$data['id_rumus_pv']        = ($_POST['keterangan_pv']=="")?0:$_POST['id_rumus_pv'];
$data['keterangan_pv']      = $_POST['keterangan_pv'];

$data['irna_a']             = isset($_POST['irna_a'])?$_POST['irna_a']:0;
$data['irna_b']             = isset($_POST['irna_b'])?$_POST['irna_b']:0;
$data['irna_b1']            = isset($_POST['irna_b1'])?$_POST['irna_b1']:0;
$data['irna_c']             = isset($_POST['irna_c'])?$_POST['irna_c']:0;
$data['irna_e']             = isset($_POST['irna_e'])?$_POST['irna_e']:0;
$data['irna_f']             = isset($_POST['irna_f'])?$_POST['irna_r']:0;
$data['irna_g']             = isset($_POST['irna_g'])?$_POST['irna_g']:0;
$data['paviliun']           = isset($_POST['paviliun'])?$_POST['paviliun']:0;

$data['durasi_non_kelas']   = isset($_POST['durasi_non_kelas'])?$_POST['durasi_non_kelas']:0;
$data['durasi_kelas_i']     = isset($_POST['durasi_kelas_i'])?$_POST['durasi_kelas_i']:0;
$data['durasi_kelas_ii']    = isset($_POST['durasi_kelas_ii'])?$_POST['durasi_kelas_ii']:0;
$data['durasi_kelas_iii']   = isset($_POST['durasi_kelas_iii'])?$_POST['durasi_kelas_iii']:0;
$data['durasi_kelas_vip']   = isset($_POST['durasi_kelas_vip'])?$_POST['durasi_kelas_vip']:0;
$data['durasi_kelas_vvip']  = isset($_POST['durasi_kelas_vvip'])?$_POST['durasi_kelas_vvip']:0;
$data['durasi_kelas_deluxe']= isset($_POST['durasi_kelas_deluxe'])?$_POST['durasi_kelas_deluxe']:0;
$data['durasi_kelas_nicu']  = isset($_POST['durasi_kelas_nicu'])?$_POST['durasi_kelas_nicu']:0;
$data['durasi_kelas_hcu']   = isset($_POST['durasi_kelas_hcu'])?$_POST['durasi_kelas_hcu']:0;
$data['history_ruang']      = isset($_POST['history_ruang'])?json_encode($_POST['history_ruang']):"";

$data['mulai_dpjp_satu']    = isset($_POST['mulai_dpjp_satu'])?$_POST['mulai_dpjp_satu']:"0000-00-00 00:00:00";
$data['mulai_dpjp_dua']     = isset($_POST['mulai_dpjp_dua'])?$_POST['mulai_dpjp_dua']:"0000-00-00 00:00:00";
$data['mulai_dpjp_tiga']    = isset($_POST['mulai_dpjp_tiga'])?$_POST['mulai_dpjp_tiga']:"0000-00-00 00:00:00";

$data['selesai_dpjp_satu']  = isset($_POST['selesai_dpjp_satu'])?$_POST['selesai_dpjp_satu']:"0000-00-00 00:00:00";
$data['selesai_dpjp_dua']   = isset($_POST['selesai_dpjp_dua'])?$_POST['selesai_dpjp_dua']:"0000-00-00 00:00:00";
$data['selesai_dpjp_tiga']  = isset($_POST['selesai_dpjp_tiga'])?$_POST['selesai_dpjp_tiga']:"0000-00-00 00:00:00";

$data['durasi_dpjp_satu']   = hour_different($_POST['mulai_dpjp_satu'],$_POST['selesai_dpjp_satu']);
$data['durasi_dpjp_dua']    = hour_different($_POST['mulai_dpjp_dua'],$_POST['selesai_dpjp_dua']);
$data['durasi_dpjp_tiga']   = hour_different($_POST['mulai_dpjp_tiga'],$_POST['selesai_dpjp_tiga']);




/* total tagihan yang harus dibayar oleh pasien */
$X=$_POST['tarif_plafon_inacbg'];
$Y=$_POST['tarif_plafon_kelas1'];
$rumusT=$_POST['rumus_biaya'];
$rumusT=str_replace("X",$X,$rumusT);
$rumusT=str_replace("Y",$Y,$rumusT);
eval( '$nilaiT= (' . $rumusT. ');' );
$T=$nilaiT;
$data['total_biaya']=$T;

/* adalah potongan pengurangan biaya dari pelayanan pasien */
$rumusU=$_POST['rumus_potongan'];
$rumusU=str_replace("X",$X,$rumusU);
$rumusU=str_replace("Y",$Y,$rumusU);
$rumusU=str_replace("T",$T,$rumusU);       
eval( '$nilaiU= (' . $rumusU. ');' );
$U=$nilaiU;
$data['total_potongan']=$U;

/* adalah biaya yang harus dibayar oleh pasien */
$rumusZ=$_POST['rumus_tagihan'];
$rumusZ=str_replace("X",$X,$rumusZ);
$rumusZ=str_replace("Y",$Y,$rumusZ);
$rumusZ=str_replace("T",$T,$rumusZ);
$rumusZ=str_replace("U",$U,$rumusZ);
eval( '$nilaiZ= (' . $rumusZ. ');' );
$Z=$nilaiZ;
$data["total_tagihan"]=$Z;

$dbtable=new DBTable($db,"smis_ksr_tagihan_rumus");
$dbtable->insertOrUpdate($data,$up);
?>