<?php 
    require_once "smis-base/smis-include-service-consumer.php";
    global $db;
    
    $data           = json_decode($_POST['data'],true);
    $id_resep       = $data['id_resep'];
    $entity         = $data['entity'];
    
    /**mengambil data detail persediaan */
    $service        = new ServiceConsumer($db,"get_detail_accounting_hpp_persediaan_penjualan",array("data"=>$id_resep),$entity);
    $service        ->execute();
    $rslt           = $service->getContent();
    $transaction    = $rslt[0];
    $total_debet    = $transaction['header']['debet'];
    $total_kredit   = $transaction['header']['kredit'];
    $total_bayar    = 0;

    /**mengambil data detail resep */
    $service        = new ServiceConsumer($db,"browse_resep",array("command"=>"edit","id"=>$id_resep),$entity);
    $service        ->execute();
    $resep          = $service->getContent();
    $total_resep    = $resep['header']['total'];
    /**memasukan data penjualan resep bebeas */
    $kredit                     = array();
    $kredit['akun']             = getSettings($db,"cashier-k-resep-bebas");
    $kredit['debet']            = 0;
    $kredit['kredit']           = $total_resep ;
    $kredit['ket']              = "Pendapatan Penjualan Resep Bebas  Kasir - A/n ".$resep['header']['nama_pasien']."  dan No Resep ".$id_resep;
    $kredit['code']             = "kredit-penjualan-resep-bebas-kasir-".$id_resep;
    $transaction['content'][]   = $kredit;
    $total_kredit              += $total_resep;
    /**mengambil data pembayaran tagihan resep */
    $dbtable  = new DBTable($db,"smis_ksr_bayar");
    $dbtable  ->setShowAll(true);
    $dbtable  ->addCustomKriteria(" id_resep","='".$id_resep."' " );
    $list     = $dbtable  ->view("",0);
    foreach($list['data'] as $x){
        /**kredit */
        $metode                   = trim(str_replace("_","-",$x->metode));
        $debet                    = array();
        $debet['akun']            = getSettings($db,"cashier-acc-d-".$metode."-".$x->carabayar,"");
        $debet['debet']           = $x->nilai;
        $debet['kredit']          = 0;
        $debet['ket']             = "Pembayaran Resep Bebas ".strtoupper($x->carabayar)." Kasir - ".$x->nama_pasien." dan No Resep ".$id_resep;
        $debet['code']            = "debet-".$metode."-resep-kasir-".$x->id;
        $total_bayar             += $x->nilai;        
        $transaction['content'][] = $debet;
        $total_debet             += $x->nilai;
    }
    if($total_bayar!=$total_resep){
        $sisa                     = abs($total_bayar-$total_resep);
        $dk['akun']               = getSettings($db,"cashier-kurang-lebih-bayar-resep-bebas","");
        $dk['debet']              = $total_bayar>$total_resep?0:$sisa;
        $dk['kredit']             = $total_bayar<$total_resep?0:$sisa;
        $dk['ket']                = "Lebih Kurang Bayar Resep Bebas  Kasir - ".$resep['header']['nama_pasien']." ID Resep ".$id_resep;
        $dk['code']               = "kurang-lebih-resep-kasir-".$id_resep;
        $transaction['content'][] = $dk;   
        $total_kredit            += $dk['kredit'];
        $total_debet             += $dk['debet']; 
    }
    $transaction['header']['keterangan']    = "Penjualan dan ".$transaction['header']['keterangan'];
    $transaction['header']['debet']         = $total_debet;
    $transaction['header']['kredit']        = $total_kredit;
    
    $final   = array();
    $final[] = $transaction;        
    echo json_encode($final);
?>