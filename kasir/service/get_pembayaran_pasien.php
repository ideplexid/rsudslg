<?php
	global $db;
	
	if (isset($_POST['noreg_pasien'])
	 && isset($_POST['jenis_pasien']) 
	 && isset($_POST['from']) 
     && isset($_POST['to']) 
	 && isset($_POST['operator'])) {
		$diskon_row = $db->get_row("
			SELECT SUM(nilai) AS 'nilai'
			FROM smis_ksr_bayar
			WHERE noreg_pasien = '" . $_POST['noreg_pasien'] . "' AND metode = 'diskon' 
				  AND operator LIKE '" . $_POST['operator'] . "' AND prop NOT LIKE 'del'
		");
		$cash_row = $db->get_row("
			SELECT SUM(nilai) AS 'nilai'
			FROM smis_ksr_bayar
			WHERE noreg_pasien = '" . $_POST['noreg_pasien'] . "' AND metode LIKE 'cash%'
				  AND operator LIKE '" . $_POST['operator'] . "' AND prop NOT LIKE 'del'
		");
		$mandiri_tunai_row = $db->get_row("
			SELECT SUM(nilai) AS 'nilai'
			FROM smis_ksr_bayar
			WHERE noreg_pasien = '" . $_POST['noreg_pasien'] . "' AND metode = 'bank' AND nama_bank LIKE '%Mandiri%' AND keterangan LIKE '%Setoran Tunai%'
				  AND operator LIKE '" . $_POST['operator'] . "' AND prop NOT LIKE 'del'
		");
		$mandiri_edc_row = $db->get_row("
			SELECT SUM(nilai) AS 'nilai'
			FROM smis_ksr_bayar
			WHERE noreg_pasien = '" . $_POST['noreg_pasien'] . "' AND metode = 'bank' AND nama_bank LIKE '%Mandiri%' AND keterangan NOT LIKE '%Setoran Tunai%'
				  AND operator LIKE '" . $_POST['operator'] . "' AND prop NOT LIKE 'del'
		");
		$bri_tunai_row = $db->get_row("
			SELECT SUM(nilai) AS 'nilai'
			FROM smis_ksr_bayar
			WHERE noreg_pasien = '" . $_POST['noreg_pasien'] . "' AND metode = 'bank' AND nama_bank LIKE '%BRI%' AND keterangan LIKE '%Setoran Tunai%'
				  AND operator LIKE '" . $_POST['operator'] . "' AND prop NOT LIKE 'del'
		");
		$bri_edc_row = $db->get_row("
			SELECT SUM(nilai) AS 'nilai'
			FROM smis_ksr_bayar
			WHERE noreg_pasien = '" . $_POST['noreg_pasien'] . "' AND metode = 'bank' AND nama_bank LIKE '%BRI%' AND keterangan NOT LIKE '%Setoran Tunai%'
				  AND operator LIKE '" . $_POST['operator'] . "' AND prop NOT LIKE 'del'
		");
		$bca_tunai_row = $db->get_row("
			SELECT SUM(nilai) AS 'nilai'
			FROM smis_ksr_bayar
			WHERE noreg_pasien = '" . $_POST['noreg_pasien'] . "' AND metode = 'bank' AND nama_bank LIKE '%BCA%' AND keterangan LIKE '%Setoran Tunai%'
				  AND operator LIKE '" . $_POST['operator'] . "' AND prop NOT LIKE 'del'
		");
		$bca_edc_row = $db->get_row("
			SELECT SUM(nilai) AS 'nilai'
			FROM smis_ksr_bayar
			WHERE noreg_pasien = '" . $_POST['noreg_pasien'] . "' AND metode = 'bank' AND nama_bank LIKE '%BCA%' AND keterangan NOT LIKE '%Setoran Tunai%'
				  AND operator LIKE '" . $_POST['operator'] . "' AND prop NOT LIKE 'del'
		");
		$tagihan_row = $db->get_row("
			SELECT SUM(nilai) AS 'nilai'
			FROM smis_ksr_bayar
			WHERE noreg_pasien = '" . $_POST['noreg_pasien'] . "' AND metode = 'asuransi'
				  AND operator LIKE '" . $_POST['operator'] . "' AND prop NOT LIKE 'del'
		");
		$data = array();
		$data['nilai_cash'] = $cash_row->nilai == "" ? 0 : $cash_row->nilai;
		$data['nilai_mandiri_tunai'] = $mandiri_tunai_row->nilai == "" ? 0 : $mandiri_tunai_row->nilai;
		$data['nilai_mandiri_edc'] = $mandiri_edc_row->nilai == "" ? 0 : $mandiri_edc_row->nilai;
		$data['nilai_bri_tunai'] = $bri_tunai_row->nilai == "" ? 0 : $bri_tunai_row->nilai;
		$data['nilai_bri_edc'] = $bri_edc_row->nilai == "" ? 0 : $bri_edc_row->nilai;
		$data['nilai_bca_tunai'] = $bca_tunai_row->nilai == "" ? 0 : $bca_tunai_row->nilai;
		$data['nilai_bca_edc'] = $bca_edc_row->nilai == "" ? 0 : $bca_edc_row->nilai;
		$data['nilai_tagihan_non_ptpn_12'] = $_POST['jenis_pasien'] != "bpjs_ptpn_12" && $_POST['jenis_pasien'] != "pensiunan_ptpn_12" ? $tagihan_row->nilai : 0;
		$data['nilai_tagihan_ptpn_12'] = $_POST['jenis_pasien'] == "bpjs_ptpn_12" || $_POST['jenis_pasien'] == "pensiunan_ptpn_12" ? $tagihan_row->nilai : 0;
		$data['nilai_diskon'] = $diskon_row->nilai == "" ? 0 : $diskon_row->nilai;
		echo json_encode($data);
	}
?>