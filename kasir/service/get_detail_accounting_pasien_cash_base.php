<?php 
global $db;
$_pst        = json_decode($_POST['data'],true);
$crby        = ArrayAdapter::slugFormat("unslug",$_pst['carabayar']);
$total_bayar = 0;
$total_tagih = 0;
$list        = array();

/*khusus yang bukan akunting saja, dan ini adalah tagihan asli pasien*/
$dbtable    = new DBTable($db,"smis_ksr_kolektif");
$dbtable->setShowAll(true);
$dbtable->addCustomKriteria(" noreg_pasien "," ='".$_pst['noreg_pasien']."' ");
$dbtable->addCustomKriteria(" akunting_only "," = 0 ");
$list_d     = $dbtable->view("",0);
$list_tagih = $list_d['data'];
foreach($list_tagih as $tagih){
    $kredit           = array();
    $kredit['akun']   = $tagih->kredit;
    $kredit['debet']  = 0;
    $kredit['kredit'] = $tagih->akunting_nilai;
    $kredit['ket']    = "Pendapatan ".$tagih->akunting_nama." pada ".$tagih->ruangan_map." Atas Pasien ".$tagih->nama_pasien." dengan No.Reg ".$tagih->noreg_pasien;
    $kredit['code']   = "debet-".$metode."-kasir-".$bayar->id;
    $list[]           = $kredit;
    $total_tagih     += $tagih->akunting_nilai;
}

/* hanya khusus yang tampilan akunting saja */
$dbtable->addCustomKriteria(" akunting_only "," = 1 ");
$list_d     = $dbtable->view("",0);
$list_tagih = $list_d['data'];
foreach($list_tagih as $tagih){
    $kredeb           = array();
    $kredeb['akun']   = $tagih->kredit==""?$tagih->debet:$tagih->kredit;
    $kredeb['debet']  = $tagih->debet!=""?$tagih->akunting_nilai:0;
    $kredeb['kredit'] = $tagih->kredit!=""?$tagih->akunting_nilai:0;
    $kredeb['ket']    = $tagih->akunting_nama." pada ".$tagih->ruangan_map." Atas Pasien ".$tagih->nama_pasien." dengan No.Reg ".$tagih->noreg_pasien;
    $kredeb['code']   = "debet-".$metode."-kasir-".$bayar->id;
    $total_tagih     += $kredeb['kredit']-$kredeb['debet'];
    $list[]           = $kredeb;
}

$dbtable    = new DBTable($db,"smis_ksr_bayar");
$dbtable->setShowAll(true);
$dbtable->addCustomKriteria(" noreg_pasien "," ='".$_pst['noreg_pasien']."' ");
$list_d     = $dbtable->view("",0);
$list_bayar = $list_d['data'];
foreach($list_bayar as $bayar){
    $metode           = trim(str_replace("_","-",$bayar->metode));
    $set_name         = "cashier-acc-d-".$metode."-".$_pst['carabayar'];
    if(startsWith($metode,"bank")){
        $set_name         = "cashier-acc-d-".$metode."-".$bayar->id_bank;
    }else if(startsWith($metode,"asuransi")){
        $kode_acc_asuransi = $bayar->id_asuransi*1!=0?$x->id_asuransi:$bayar->carabayar ;
        $set_name         = "cashier-acc-d-".$metode."-".$kode_acc_asuransi;
    }
     
    $debet            = array();
    $debet['akun']    = getSettings($db,$set_name,"");
    $debet['debet']   = $bayar->nilai;
    $debet['kredit']  = 0;
    $debet['ket']     = "Uang ".ArrayAdapter::format("unslug",$bayar->metode)." Pasien ".$_pst['carabayar']." Kasir - ".$bayar->nama_pasien." dengan No.Reg ".$bayar->noreg_pasien." dan No Kwitansi ".($bayar->no_kwitansi!=""?$bayar->no_kwitansi:$bayar->id);
    $debet['code']    = "debet-".$metode."-kasir-".$bayar->id;
    $list[]           = $debet;
    $total_bayar     += $bayar->nilai;
}


$sisa_bayar = round($total_bayar-$total_tagih);
if($sisa_bayar*1!=0){
    $kurang            = array();
    $kurang['akun']    = getSettings($db,"cashier-acc-".($sisa_bayar<0?"d":"k")."-kurang-".$_pst['carabayar'],"");
    $kurang['debet']   = $sisa_bayar<0?($sisa_bayar*-1):0;
    $kurang['kredit']  = $sisa_bayar>0?($sisa_bayar):0;
    $kurang['ket']     = ($sisa_bayar<0?"Kekurangan":"Kelebihan"). " Uang Pasien ".$_pst['carabayar']." Kasir - ".$_pst['nama_pasien']." dengan No.Reg ".$_pst['noreg_pasien'];
    $kurang['code']    = "debet-kurang-kasir-".$_pst['noreg_pasien'];
    $list[]            = $kurang;    
    $total_bayar      += $kurang['debet'];
    $total_tagih      += $kurang['kredit'];
}
    

//content untuk header
$tgl_mode               = getSettings($db,"cashier-acc-setup-tgl-model","tgl_notif");
$header                 = array();
$header['tanggal']      = $_pst[$tgl_mode];
$header['keterangan']   = "Pasien ".$_pst['nama_pasien']." , NRM ".$_pst['nrm_pasien']." dan No. Reg ".$_pst['noreg_pasien'];
$header['code']         = "cb-kasir-".$_pst['noreg_pasien'];
$header['nomor']        = "NCBP-".$_pst['noreg_pasien'];
$header['debet']        = $total_bayar;
$header['kredit']       = $total_tagih;
$header['io']           = "1";

$transaction=array();
$transaction['header']=$header;
$transaction['content']=$list;

$final=array();
$final[]=$transaction;
    
echo json_encode($final);

/*notif that already change or crawler by akunting*/
/*$update['akunting']=1;
$id['id']=$_POST['data'];
$dbtable->update($update,$id);*/

?>