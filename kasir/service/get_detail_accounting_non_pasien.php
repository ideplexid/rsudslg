<?php 
    global $db;
    $id      = $_POST['data'];
    
    $dbtable = new DBTable($db,"smis_ksr_np");
    $x = $dbtable ->selectEventDel($id);
    
    //content untuk header
    $header=array();
    $header['tanggal']      = $x->tanggal;
    $header['keterangan']   = "Pendapatan Non Pasien Atas Nama ".$x->nama." - ".$x->keterangan." dengan Nomor Transaksi ".$x->id;
    $header['code']         = "non-pasien-".$x->id;
    $header['nomor']        = "NPK-".$x->id;
    $header['debet']        = $x->nilai;
    $header['kredit']       = $x->nilai;
    $header['io']           = "1";
    
    
    $dbtable_npd = new DBTable($db,"smis_ksr_npd");
    $dbtable_npd->addCustomKriteria(" id_np "," ='".$id."' ");
    $dbtable_npd->setShowAll(true);
    $data=$dbtable_npd->view("",0);
    $listd=$data['data'];
    $list   = array();
    
    foreach($listd as $npd){
        //content untuk kredit
        $kredit=array();
        $kredit['akun']     = $npd->kredit;
        $kredit['debet']    = 0;
        $kredit['kredit']   = $npd->total;
        $kredit['ket']      = "Pendapatan Non Pasien : ".$npd->nama." x ".$npd->jumlah." Atas Nama ".$x->nama." dengan Nomor Transaksi ".$x->id;
        $kredit['code']     = "kredit-non-pasien-".$x->id."-".$npd->id;
        $list[] = $debet;
        
        //content untuk debet
        $debet=array();
        $debet['akun']    = $x->no_akun==""?$npd->debet:$x->no_akun;
        $debet['debet']   = $npd->total;
        $debet['kredit']  = 0;
        $debet['ket']      = "Pembayaran Non Pasien : ".$npd->nama." x ".$npd->jumlah." Atas Nama ".$x->nama." dengan Nomor Transaksi ".$x->id;
        $debet['code']     = "kredit-non-pasien-".$x->id."-".$npd->id;
        $list[] = $kredit;    
    }
    
    $transaction=array();
    $transaction['header']=$header;
    $transaction['content']=$list;
    
    $final=array();
    $final[]=$transaction;        
    echo json_encode($final);
    
    /*notif that already change or crawler by akunting*/;
    $update['akunting']=1;
    $id['id']=$_POST['data'];
    $dbtable->update($update,$id);
?>