<?php
show_error();
require_once 'kasir/class/responder/TotalResponder.php';
global $db;

$noreg = $_POST ['noreg_pasien'];
$nama = $_POST ['nama_pasien'];
$nrm = $_POST ['nrm_pasien'];
$invoke=isset($_POST['invoke'])?$_POST['invoke']:"null";
$head_title=isset($_POST['title'])?$_POST['title']:"Processing...";
$page = $_POST ['page'];
$action = $_POST ['action'];
$polislug = $_POST ['polislug'];
$pslug = $_POST ['prototype_slug'];
$pname = $_POST ['prototype_name'];
$pimplement = $_POST ['prototype_implement'];
$timeout 	 = isset($_POST['delay'])?$_POST['delay']:100;
$autoskipper = isset($_POST['skipper'])?$_POST['skipper']:100;


ob_start ();
$template = new TotalResponder ( $db, $polislug, $noreg, $nrm, $nama, $page, $action, $pslug, $pname, $pimplement ,$invoke,$head_title);
$template->setTimeout($timeout,$autoskipper);
$template->initialize ();
$result = ob_get_clean ();
echo json_encode ( $result );

?>