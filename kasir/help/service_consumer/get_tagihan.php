<?php 

/**
 * 
 * this provided manual for get_tagihan service
 * 
 * @version 1.0
 * @since 16 Nov 2014
 * @author goblooge
 * @license LGPL v3
 * @copyright Nurul Huda <goblooge@gmail.com>
 * 
 */

require_once 'smis-libs-class/ManualService.php';

$response=array();
$response['exist']="1"; 																									// 1 if the pasien exist in these autonomous, if these given 0, these consumer will stop here.
$response['selesai']="1"; 																									// if 1 given then it will print that patient have been done using these service.
$response['cara_keluar']="dipulangkan"; 																					// string contains how patien get out (it contains string an default should be 'dipulangkan')
$response['reverse']="0";																									// if 1 given than the consumer will print that user can back to this entity (default should be 0)	
$response['data']=array();																									// the data that needed to

$response['data']["tindakan_dokter"]=array();																				// tindakan_dokter can be change as you want to, it should be a variable
$response['data']["tindakan_dokter"]["jasa_pelayanan"]="1";																	// if set to 1 it will be add as Jasa Pelayanan in Cashier
$response['data']["tindakan_dokter"]["result"]=array();																		//the result of each bill should be here
$response['data']["tindakan_dokter"]["result"][0]=array();																	//the loop here
$response['data']["tindakan_dokter"]["result"][0]['waktu']="12 Jan 2014"; 													//the time, it contains String
$response['data']["tindakan_dokter"]["result"][0]['nama']="Tindakan Dokter Budi";											//the name of tindakan that needed to
$response['data']["tindakan_dokter"]["result"][0]['biaya']="190000";														//should int or string that look like integer (it will summary by consumer)
$response['data']["tindakan_dokter"]["result"][0]['keterangan']="...";														//just say everithing you like
$response['data']["tindakan_dokter"]["result"][0]['debet']="410.10";														//should int or string that look like integer (it will summary by consumer)
$response['data']["tindakan_dokter"]["result"][0]['kredit']="110.10";														//just say everithing you like
$response['data']["tindakan_dokter"]["result"][0]['akunting_nilai']="190000 (unset > ikut biaya* jumlah)";														//just say everithing you like
$response['data']["tindakan_dokter"]["result"][0]['akunting_only']="0 (unset akan bernilai 0 )";														//should int or string that look like integer (it will summary by consumer)
$response['data']["tindakan_dokter"]["result"][0]['akunting_name']="Tindakan Dokter Budi";														//just say everithing you like

$response['data']["tindakan_dokter"]["result"][0]['nama_dokter']="Dr. Budianto SpoG";														//just say everithing you like
$response['data']["tindakan_dokter"]["result"][0]['id_dokter']="11";														//just say everithing you like
$response['data']["tindakan_dokter"]["result"][0]['jaspel_dokter']="90000";											//just say everithing you like
$response['data']["tindakan_dokter"]["result"][0]['urjigd']="igd";														//just say everithing you like
$response['data']["tindakan_dokter"]["result"][0]['tanggal_tagihan']="2014-01-12 (format tanggal YYYY-MM-DD)";														//just say everithing you like







$response['data']["tindakan_dokter"]["result"][1]=array();																	//the loop here
$response['data']["tindakan_dokter"]["result"][1]['waktu']="12 Jan 2014"; 													//the time, it contains String
$response['data']["tindakan_dokter"]["result"][1]['nama']="Tindakan Dokter Ani";											//the name of tindakan that needed to
$response['data']["tindakan_dokter"]["result"][1]['biaya']="190000";														//should int or string that look like integer (it will summary by consumer)
$response['data']["tindakan_dokter"]["result"][1]['keterangan']="...";														//just say everithing you like
$response['data']["tindakan_dokter"]["result"][1]['debet']="420.10";														//should int or string that look like integer (it will summary by consumer)
$response['data']["tindakan_dokter"]["result"][1]['kredit']="110.10";														//just say everithing you like
$response['data']["tindakan_dokter"]["result"][1]['akunting_nilai']="190000";														//just say everithing you like
$response['data']["tindakan_dokter"]["result"][1]['akunting_only']="0";														//should int or string that look like integer (it will summary by consumer)
$response['data']["tindakan_dokter"]["result"][1]['akunting_name']="Tindakan Dokter Ani";														//just say everithing you like
$response['data']["tindakan_dokter"]["result"][1]['nama_dokter']="Dr. Budianto SpoG";														//just say everithing you like
$response['data']["tindakan_dokter"]["result"][1]['id_dokter']="11";														//just say everithing you like
$response['data']["tindakan_dokter"]["result"][1]['jaspel_dokter']="90000";											//just say everithing you like
$response['data']["tindakan_dokter"]["result"][1]['urjigd']="igd";														//just say everithing you like
$response['data']["tindakan_dokter"]["result"][1]['tanggal_tagihan']="2014-01-12 (format tanggal YYYY-MM-DD)";														//just say everithing you like


$response['data']["penjualan_resep"]=array();																				// tindakan_dokter can be change as you want to, it should be a variable
$response['data']["penjualan_resep"]["jasa_pelayanan"]="0";																	// if set to 1 it will be add as Jasa Pelayanan in Cashier
$response['data']["penjualan_resep"]["result"]=array();																		//the result of each bill should be here
$response['data']["penjualan_resep"]["result"][0]=array();																	//the loop here
$response['data']["penjualan_resep"]["result"][0]['waktu']="12 Jan 2014"; 													//the time, it contains String
$response['data']["penjualan_resep"]["result"][0]['nama']="Resep 1201 - Bodrex ";	
$response['data']["penjualan_resep"]["result"][0]['jumlah']="12 (ini adalah jumlah obatnya)" ;												//the name of tindakan that needed to
$response['data']["penjualan_resep"]["result"][0]['biaya']="12000 (tipe int, ini adalah harga satuan)";						//should int or string that look like integer (it will summary by consumer)
$response['data']["penjualan_resep"]["result"][0]['keterangan']="Bodrex 12 x 1.000";										//just say everithing you like
$response['data']["penjualan_resep"]["result"][0]['debet']="410.10";														//should int or string that look like integer (it will summary by consumer)
$response['data']["penjualan_resep"]["result"][0]['kredit']="110.10";
$response['data']["penjualan_resep"]["result"][0]['akunting_nilai']="6000 (harga total 500*12)";														//just say everithing you like
$response['data']["penjualan_resep"]["result"][0]['akunting_only']="0 (0 berarti utk autning dan tagihan)";														//should int or string that look like integer (it will summary by consumer)
$response['data']["penjualan_resep"]["result"][0]['akunting_name']="Pendapatan Bodrex";														//just say everithing you like
$response['data']["penjualan_resep"]["result"][0]['nama_dokter']="Dr. Budianto SpoG";														//just say everithing you like
$response['data']["penjualan_resep"]["result"][0]['id_dokter']="11";														//just say everithing you like
$response['data']["penjualan_resep"]["result"][0]['jaspel_dokter']="2000";											//just say everithing you like
$response['data']["penjualan_resep"]["result"][0]['urjigd']="igd";														//just say everithing you like
$response['data']["penjualan_resep"]["result"][0]['tanggal_tagihan']="2014-01-12 (format tanggal YYYY-MM-DD)";														//just say everithing you like


$response['data']["penjualan_resep"]["result"][1]=array();																	//the loop here
$response['data']["penjualan_resep"]["result"][1]['waktu']="12 Jan 2014"; 													//the time, it contains String
$response['data']["penjualan_resep"]["result"][1]['nama']="Resep 1201 - [R] Flu - Bodrex ( [R] kode untuk racikan)";	
$response['data']["penjualan_resep"]["result"][1]['jumlah']="3 (ini adalah jumlah obatnya)" ;												//the name of tindakan that needed to
$response['data']["penjualan_resep"]["result"][1]['biaya']="3000 (tipe int, ini adalah harga satuan)";						//should int or string that look like integer (it will summary by consumer)
$response['data']["penjualan_resep"]["result"][1]['keterangan']="Bodrex 3 x 1.000";
$response['data']["penjualan_resep"]["result"][1]['debet']="410.10";														//should int or string that look like integer (it will summary by consumer)
$response['data']["penjualan_resep"]["result"][1]['kredit']="110.10";														//just say everithing you like
$response['data']["penjualan_resep"]["result"][1]['akunting_nilai']="3600 (harga total)";														//just say everithing you like
$response['data']["penjualan_resep"]["result"][1]['akunting_only']="0";														//should int or string that look like integer (it will summary by consumer)
$response['data']["penjualan_resep"]["result"][1]['akunting_name']="Pendapatan Flu - Bordrex";														//just say everithing you like
$response['data']["penjualan_resep"]["result"][1]['nama_dokter']="Dr. Budianto SpoG";														//just say everithing you like
$response['data']["penjualan_resep"]["result"][1]['id_dokter']="11";														//just say everithing you like
$response['data']["penjualan_resep"]["result"][1]['jaspel_dokter']="1000";											//just say everithing you like
$response['data']["penjualan_resep"]["result"][1]['urjigd']="igd";														//just say everithing you like
$response['data']["penjualan_resep"]["result"][1]['tanggal_tagihan']="2014-01-12 (format tanggal YYYY-MM-DD)";														//just say everithing you like



$response['data']["penjualan_resep"]["result"][2]=array();																	//the loop here
$response['data']["penjualan_resep"]["result"][2]['waktu']="12 Jan 2014"; 													//the time, it contains String
$response['data']["penjualan_resep"]["result"][2]['nama']="";	
$response['data']["penjualan_resep"]["result"][2]['jumlah']="0 (utk akunting saja nilainya 0) " ;												//the name of tindakan that needed to
$response['data']["penjualan_resep"]["result"][2]['biaya']="0 (utk akunting saja nilainya 0)";						//should int or string that look like integer (it will summary by consumer)
$response['data']["penjualan_resep"]["result"][2]['keterangan']="";
$response['data']["penjualan_resep"]["result"][2]['debet']="410.10";														//should int or string that look like integer (it will summary by consumer)
$response['data']["penjualan_resep"]["result"][2]['kredit']="110.10";														//just say everithing you like
$response['data']["penjualan_resep"]["result"][2]['akunting_nilai']="8400 (harga total ppn 700*12)";														//just say everithing you like
$response['data']["penjualan_resep"]["result"][2]['akunting_only']="1 (kode satu untuk akunting saja)";														//should int or string that look like integer (it will summary by consumer)
$response['data']["penjualan_resep"]["result"][2]['akunting_name']="PPN Penjualan Bodrex";														//just say everithing you like
$response['data']["penjualan_resep"]["result"][2]['nama_dokter']="Dr. Budianto SpoG";														//just say everithing you like
$response['data']["penjualan_resep"]["result"][2]['id_dokter']="11";														//just say everithing you like
$response['data']["penjualan_resep"]["result"][2]['jaspel_dokter']="500";											//just say everithing you like
$response['data']["penjualan_resep"]["result"][2]['urjigd']="igd";														//just say everithing you like
$response['data']["penjualan_resep"]["result"][2]['tanggal_tagihan']="2014-01-12 (format tanggal YYYY-MM-DD)";														//just say everithing you like



$response['data']["return_resep"]=array();																				// tindakan_dokter can be change as you want to, it should be a variable
$response['data']["return_resep"]["jasa_pelayanan"]="0";																	// if set to 1 it will be add as Jasa Pelayanan in Cashier
$response['data']["return_resep"]["result"]=array();																		//the result of each bill should be here
$response['data']["return_resep"]["result"][0]=array();																		//the loop here
$response['data']["return_resep"]["result"][0]['waktu']="12 Jan 2014"; 														//the time, it contains String
$response['data']["return_resep"]["result"][0]['nama']="Resep No 1289 - Antangin JRG";										//the name of tindakan that needed to
$response['data']["return_resep"]["result"][0]['biaya']="40000 (harga satuan antangin)";	
$response['data']["return_resep"]["result"][0]['jumlah']="2 (ini adalah jumlah obatnya)" ;																//should int or string that look like integer (it will summary by consumer)
$response['data']["return_resep"]["result"][0]['keterangan']="Antangin 2 x 20.000";		
$response['data']["return_resep"]["result"][0]['debet']="410.10";														//should int or string that look like integer (it will summary by consumer)
$response['data']["return_resep"]["result"][0]['kredit']="110.10";														//just say everithing you like
$response['data']["return_resep"]["result"][0]['akunting_nilai']="40000";										//just say everithing you like
$response['data']["return_resep"]["result"][0]['akunting_only']="0 (kode 0 bukan untuk akunting saja)";														//should int or string that look like integer (it will summary by consumer)
$response['data']["return_resep"]["result"][0]['akunting_name']="Return Resep No 1289 - Antangin JRG";														//just say everithing you like
$response['data']["return_resep"]["result"][0]['nama_dokter']="Dr. Budianto SpoG";												//just say everithing you like
$response['data']["return_resep"]["result"][0]['id_dokter']="11";														//just say everithing you like
$response['data']["return_resep"]["result"][0]['jaspel_dokter']="0";											//just say everithing you like
$response['data']["return_resep"]["result"][0]['urjigd']="igd";														//just say everithing you like
$response['data']["return_resep"]["result"][0]['tanggal_tagihan']="2014-01-12 (format tanggal YYYY-MM-DD)";														//just say everithing you like


$response['data']["return_resep"]["result"][1]=array();																	//the loop here
$response['data']["return_resep"]["result"][1]['waktu']="12 Jan 2014"; 													//the time, it contains String
$response['data']["return_resep"]["result"][1]['nama']="";	
$response['data']["return_resep"]["result"][1]['jumlah']="0" ;								//the name of tindakan that needed to
$response['data']["return_resep"]["result"][1]['biaya']="0";						//should int or string that look like integer (it will summary by consumer)
$response['data']["return_resep"]["result"][1]['keterangan']="Return Bodrex 1 x 1.000";											//just say everithing you like
$response['data']["return_resep"]["result"][1]['debet']="410.10";														//should int or string that look like integer (it will summary by consumer)
$response['data']["return_resep"]["result"][1]['kredit']="110.10";														//just say everithing you like
$response['data']["return_resep"]["result"][1]['akunting_nilai']="500 (ppn x jumlah)";														//just say everithing you like
$response['data']["return_resep"]["result"][1]['akunting_only']="1 (kode 1 untuk akunting saja)";														//should int or string that look like integer (it will summary by consumer)
$response['data']["return_resep"]["result"][1]['akunting_name']="PPN Return Resep NO 128";														//just say everithing you like
$response['data']["return_resep"]["result"][1]['id_dokter']="11";														//just say everithing you like
$response['data']["return_resep"]["result"][1]['jaspel_dokter']="0";											//just say everithing you like
$response['data']["return_resep"]["result"][1]['urjigd']="igd";														//just say everithing you like
$response['data']["return_resep"]["result"][1]['tanggal_tagihan']="2014-01-12 (format tanggal YYYY-MM-DD)";														//just say everithing you like

$provided=array();
$provided['noreg_pasien']="1";

$descrption="
This Service Consumer have several goal :
1. Provide data Bill for patient
2. Count including 'Jasa Pelayanan'
3. Check Reversible Patient Registration
4. Check is Patient already Done The Treatment
5. Check How Patient get Out from the Treatment (Selesai, Dipulangkan, Kabur etc)
";

$author="
* @version 1.0
* @since 16 Nov 2014
* @author goblooge
* @license LGPL v3
* @copyright Nurul Huda &#60; goblooge@gmail.com &#62;
";

$man=new ManualService("get_tagihan");
$man->setAuthor($author);
$man->setDataNeeded($response);
$man->setDataProvided($provided);
$man->setDescription($descrption);

echo $man->getHtml();



?>