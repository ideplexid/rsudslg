<?php 

/**
 * 
 * this provided manual for browse_resep service
 * 
 * @version 1.0
 * @since 16 Nov 2014
 * @author goblooge
 * @license LGPL v3
 * @copyright Nurul Huda <goblooge@gmail.com>
 * 
 */

require_once 'smis-libs-class/ManualService.php';

$needed=array();
$needed['header']=array();
$needed['header']['id']="1 (id of recipe)";
$needed['header']['nrm_pasien']="2 (nrm of patient)";
$needed['header']['noreg_pasien']="3 (registration number of patient)";
$needed['header']['nama_pasien']="Nurul Huda S.Kom (patient name)";
$needed['header']['nama_dokter']="Eka Safitri Amd.keb (doctor name)";
$needed['header']['jenis']="Penjualan Resep - Penjualan Apotek";

$needed['detail_jadi']=array();
$needed['detail_jadi'][0]=array();
$needed['detail_jadi'][0]['nama']="Bodrex";
$needed['detail_jadi'][0]['jumlah']="2";
$needed['detail_jadi'][0]['satuan']="Tablet";
$needed['detail_jadi'][0]['satuan']="Kapsul";
$needed['detail_jadi'][0]['harga_satuan']="1000 (price of unit)";
$needed['detail_jadi'][0]['harga_total']="2000 (price of unit x jumlah)";
$needed['detail_jadi'][0]['diskon_persen']="20 (discount_percentage)";
$needed['detail_jadi'][0]['diskon_nominal']="400 (harga_total * diskon_persen% or just nominal)";
$needed['detail_jadi'][0]['harga']="1600";
$needed['detail_jadi'][1]=array();
$needed['detail_jadi'][1]['nama']="Antangin";
$needed['detail_jadi'][1]['jumlah']="3";
$needed['detail_jadi'][1]['satuan']="Kapsul";
$needed['detail_jadi'][1]['harga_satuan']="2000 (price of unit)";
$needed['detail_jadi'][1]['harga_total']="6000 (price of unit x jumlah)";
$needed['detail_jadi'][1]['diskon_persen']="0 (discount_percentage)";
$needed['detail_jadi'][1]['diskon_nominal']="1000 (harga_total * diskon_persen% or just nominal)";
$needed['detail_jadi'][1]['harga']="5000 (harga_total - diskon_nominal)";

$needed['detail_racikan']=array();
$needed['detail_racikan'][0]=array();
$needed['detail_racikan'][0]['nama']="Racikan Herbal";
$needed['detail_racikan'][0]['jumlah']="1";
$needed['detail_racikan'][0]['harga_satuan']="2500 (price of unit)";
$needed['detail_racikan'][0]['harga_subtotal']="5000 (price of unit x jumlah)";
$needed['detail_racikan'][0]['embalase']="3000 (price of embalase)";
$needed['detail_racikan'][0]['harga_total']="7500 (embalase + harga_subtotal)";
$needed['detail_racikan'][0]['diskon_persen']="0 (discount_percentage)";
$needed['detail_racikan'][0]['diskon_nominal']="1500 (harga_total * diskon_persen% or just nominal)";
$needed['detail_racikan'][0]['harga']="6000 (harga_total - diskon_nominal)";
$needed['detail_racikan'][1]=array();
$needed['detail_racikan'][1]['nama']="Racikan Sehat";
$needed['detail_racikan'][1]['jumlah']="2";
$needed['detail_racikan'][1]['harga_satuan']="4000 (price of unit)";
$needed['detail_racikan'][1]['harga_subtotal']="8000 (price of unit x jumlah)";
$needed['detail_racikan'][1]['embalase']="3000 (price of embalase)";
$needed['detail_racikan'][1]['harga_total']="11000 (embalase + harga_subtotal)";
$needed['detail_racikan'][1]['diskon_persen']="10 (discount_percentage)";
$needed['detail_racikan'][1]['diskon_nominal']="1100 (harga_total * diskon_persen%)";
$needed['detail_racikan'][1]['harga']="9900 (harga_total - diskon_nominal)";

$needed['detail_retur']=array();
$needed['detail_retur'][0]=array();
$needed['detail_retur'][0]['tanggal']="2014-12-12";
$needed['detail_retur'][0]['nominal_retur']="1500";
$needed['detail_retur'][1]=array();
$needed['detail_retur'][1]['tanggal']="2014-12-13";
$needed['detail_retur'][1]['nominal_retur']="2500";


$descrption="
This Service using ServiceResponder, with using Default data and
have one 'Needed Data' need to be overriden, the 'Edit' action.
the 'Edit' Action should provide :
1. The default real data from ServiceResponder and to header
2. detail of 'Obat Jadi'
3. detail of 'Obat Racikan'
4. detail of 'Retur Penjualan Resep'
";



$author="
* @version 1.0
* @since 16 Nov 2014
* @author goblooge
* @license LGPL v3
* @copyright Nurul Huda &#60; goblooge@gmail.com &#62;
";

$man=new ManualService("browse_resep",ManualService::$RESPOND_SERVICE);
$man->setAuthor($author);
$man->setDescription($descrption);
$man->addDataNeeded("edit", $needed);


echo $man->getHtml();



?>