<?php 


require_once 'smis-libs-class/ManualService.php';

$provided=array();
$provided['noreg_pasien']="123456 (noreg pasien)";

$needed = array();
$needed['obat']="20000000 (total obat pasien di aporek selain kemoterapi dan kronis) ";
$needed['obat_kemoterapi']="10000000 (total obat kemoterapi) ";
$needed['obat_kronis']="30000000 (total obat krnois) ";


$descrption="mendapatakan total jumlah obat pasien di apotek pada satu noreg tertentu, langsung julah per jenis obatnya";


$author="
* @version 1.0
* @since 26 Sept 2019
* @author goblooge
* @license LGPL v3
* @copyright Nurul Huda &#60; goblooge@gmail.com &#62;
";

$man=new ManualService("get_resume_obat_pasien",ManualService::$RESPOND_DEFAULT);
$man->setAuthor($author);
$man->setDescription($descrption);
$man->setDataProvided($provided);
$man->setDataNeeded($needed);

echo $man->getHtml();



?>