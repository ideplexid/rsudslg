<?php 

/**
 * 
 * this provided manual for proceed_receivable
 * 
 * @version 1.0
 * @since 10 Marc 2017
 * @author goblooge
 * @license LGPL v3
 * @copyright Nurul Huda <goblooge@gmail.com>
 * 
 */

require_once 'smis-libs-class/ManualService.php';

$response="1";
$provided=array();
$provided['grup_name']="tindakan_perawat (ini dalah grup name utama)";
$provided['nama_pasien']="Abdul Malik";
$provided['nrm_pasien']="121";
$provided['noreg_pasien']="1232";
$provided['entity']="poli_gigi (sesuaikan dengan entitynya, misal : farmasi, depo_farmasi)";
$provided['list']=array();
$provided['list'][0]=array();
$provided['list'][0]['waktu']="10 Maret 2017";
$provided['list'][0]['nama']="Cabut Nyawa";
$provided['list'][0]['biaya']="25000";
$provided['list'][0]['jumlah']="1";
$provided['list'][0]['id']="1 (id dari database asli tersebut)";
$provided['list'][0]['start']="2017-03-10 15:52:00";
$provided['list'][0]['end']="2017-03-10 15:52:00";
$provided['list'][0]['dokter']="Mbah Mukri";
$provided['list'][0]['prop']="del (kosong jika insert dan update, del jika dihapus)";
$provided['list'][0]['grup_name']="tindakan_perawat (jika sama dengan grup_name utama, boleh kosong)";
$provided['list'][0]['keterangan']="Cabut Nyawa oleh Mbah Mukri";

$provided['list'][0]['nama_dokter']="Dr. Aliando Castello";
$provided['list'][0]['id_dokter']="13";
$provided['list'][0]['jaspel_dokter']="5000";
$provided['list'][0]['urjigd']="igd";
$provided['list'][0]['tanggal_tagihan']="2017-03-10";



$provided['list'][1]=array();
$provided['list'][1]['waktu']="10 Maret 2017";
$provided['list'][1]['nama']="Konsultasi Masalah Tiket Masuk Surga";
$provided['list'][1]['biaya']="25000";
$provided['list'][1]['jumlah']="1";
$provided['list'][1]['id']="10 (id dari database asli tersebut)";
$provided['list'][1]['start']="2017-03-10 15:52:00";
$provided['list'][1]['end']="2017-03-10 15:52:00";
$provided['list'][1]['dokter']="Mbah Mukri";
$provided['list'][1]['prop']="";
$provided['list'][1]['grup_name']="konsul";
$provided['list'][1]['keterangan']="Tips dan Trik cara Menyuap Malaikat Ridwan, agar masuk surga";
$provided['list'][1]['nama_dokter']="Dr. Ridwan";
$provided['list'][1]['id_dokter']="11";
$provided['list'][1]['jaspel_dokter']="5000";
$provided['list'][1]['urjigd']="igd";
$provided['list'][1]['tanggal_tagihan']="2017-03-10";

$provided['list'][2]=array();
$provided['list'][2]['waktu']="10 Maret 2017";
$provided['list'][2]['nama']="Resep No. 9670 (ini untuk per resep)";
$provided['list'][2]['biaya']="5926.8";
$provided['list'][2]['jumlah']="1";
$provided['list'][2]['id']="10 (id dari database asli tersebut)";
$provided['list'][2]['start']="2017-03-10 15:52:00";
$provided['list'][2]['end']="2017-03-10 15:52:00";
$provided['list'][2]['dokter']="Mbah Mukri";
$provided['list'][2]['prop']="";
$provided['list'][2]['grup_name']="penjualan_resep";
$provided['list'][2]['keterangan']='{"dokter":"dr. Zakky Sukmajaya, SpOG","obat":[{"nama_obat":"LIDOCAIN 2% INJ","jumlah":"3","harga":"1584","jaspel":0,"total":4752},{"nama_obat":"SPUIT 5 CC (NIPRO)","jumlah":"1","harga":"1174.8","jaspel":0,"total":1174.8}]}';
$provided['list'][1]['nama_dokter']="Dr. Zakky Sukmajaya, SpOG";
$provided['list'][1]['id_dokter']="14";
$provided['list'][1]['jaspel_dokter']="1000";
$provided['list'][1]['urjigd']="igd";
$provided['list'][1]['tanggal_tagihan']="2017-03-10";

$provided['list'][2]=array();
$provided['list'][2]['waktu']="10 Maret 2017";
$provided['list'][2]['nama']="Resep No. 9670 (ini untuk per resep)";
$provided['list'][2]['biaya']="5926.8";
$provided['list'][2]['jumlah']="1";
$provided['list'][2]['id']="10 (id dari database asli tersebut)";
$provided['list'][2]['start']="2017-03-10 15:52:00";
$provided['list'][2]['end']="2017-03-10 15:52:00";
$provided['list'][2]['dokter']="Mbah Mukri";
$provided['list'][2]['prop']="";
$provided['list'][2]['grup_name']="penjualan_resep";
$provided['list'][2]['keterangan']='{"dokter":"dr. Zakky Sukmajaya, SpOG","obat":[{"nama_obat":"LIDOCAIN 2% INJ","jumlah":"3","harga":"1584","jaspel":0,"total":4752},{"nama_obat":"SPUIT 5 CC (NIPRO)","jumlah":"1","harga":"1174.8","jaspel":0,"total":1174.8}]}';
$provided['list'][1]['nama_dokter']="Dr. Zakky Sukmajaya, SpOG";
$provided['list'][1]['id_dokter']="14";
$provided['list'][1]['jaspel_dokter']="2000";
$provided['list'][1]['urjigd']="igd";
$provided['list'][1]['tanggal_tagihan']="2017-03-10";

$provided['list'][3]=array();
$provided['list'][3]['waktu']="10 Maret 2017";
$provided['list'][3]['nama']="Penjualan DAMABEN TABLET (ini untuk per obat)";
$provided['list'][3]['biaya']="8190 (ini lagsung biaya totalnya per obat bukan biaya per bijinya)";
$provided['list'][3]['jumlah']="15 (jumlah bijian obatnya)";
$provided['list'][3]['id']="J4894-OJ19173-PENDAPATAN (id perobatnya)";
$provided['list'][3]['start']="2017-03-10 15:52:00";
$provided['list'][3]['end']="2017-03-10 15:52:00";
$provided['list'][3]['dokter']="Mbah Mukri";
$provided['list'][3]['prop']="";
$provided['list'][3]['grup_name']="penjualan_resep";
$provided['list'][3]['keterangan']='Penjualan DAMABEN TABLET : 15 x 546,00';
$provided['list'][3]['nama_dokter']="Dr. Zakky Sukmajaya, SpOG";
$provided['list'][3]['id_dokter']="14";
$provided['list'][3]['jaspel_dokter']="1000";
$provided['list'][3]['urjigd']="uri";
$provided['list'][3]['tanggal_tagihan']="2017-03-10";

$provided['list'][4]=array();
$provided['list'][4]['waktu']="10 Maret 2017";
$provided['list'][4]['nama']="Penjualan FOLAVIT 400 (ini untuk per obat)";
$provided['list'][4]['biaya']="15246 (ini lagsung biaya totalnya per obat bukan biaya per bijinya)";
$provided['list'][4]['jumlah']="15";
$provided['list'][4]['id']="J4894-OJ19174-PENDAPATAN (id perobatnya)";
$provided['list'][4]['start']="2017-03-10 15:52:00";
$provided['list'][4]['end']="2017-03-10 15:52:00";
$provided['list'][4]['dokter']="Mbah Mukri";
$provided['list'][4]['prop']="";
$provided['list'][4]['grup_name']="penjualan_resep";
$provided['list'][4]['keterangan']='Penjualan FOLAVIT 400 : 15 x 1.016,40';
$provided['list'][4]['nama_dokter']="Dr. Zakky Sukmajaya, SpOG";
$provided['list'][4]['id_dokter']="14";
$provided['list'][4]['jaspel_dokter']="1000";
$provided['list'][4]['urjigd']="uri";
$provided['list'][4]['tanggal_tagihan']="2017-03-10";

$provided['list'][5]=array();
$provided['list'][5]['waktu']="10 Maret 2017";
$provided['list'][5]['nama']="Retur Resep No. 12345";
$provided['list'][5]['biaya']="-5926.8";
$provided['list'][5]['jumlah']="1";
$provided['list'][5]['id']="10 (id return resep)";
$provided['list'][5]['start']="2017-03-10 15:52:00";
$provided['list'][5]['end']="2017-03-10 15:52:00";
$provided['list'][5]['dokter']="Mbah Mukri";
$provided['list'][5]['prop']="";
$provided['list'][5]['grup_name']="return_resep";
$provided['list'][5]['keterangan']='Return Resep No. 12345, FOLAVIT 400 5 biji, DAMABEN TABLET 6 biji';
$provided['list'][5]['nama_dokter']="Dr. Zakky Sukmajaya, SpOG";
$provided['list'][5]['id_dokter']="14";
$provided['list'][5]['jaspel_dokter']="1000";
$provided['list'][5]['urjigd']="uri";
$provided['list'][5]['tanggal_tagihan']="2017-03-10";


$descrption="
1. group name utama boleh kosong atau (-), 
   tetapi jika kosong maka group name tiap list harus diisi, 
   berguna jika satu data di system menjadi beberapa data di tempat kasir, 
   seperti Registrasi, satu data di pendaftaran akan muncul dua di kasir yakni 
   karcis masuk dan administrasi rawat inap
2. list adalah array yang jumlahnya bisa lebih dari satu.
3. hasil dari system ini adalah 0 atau 1. 
   1 jika berhasil synchronize dan 0 jika gagal. 
";

$author="
* @version 1.0
* @since 8 Oct 2015
* @author goblooge
* @license LGPL v3
* @copyright Nurul Huda &#60; goblooge@gmail.com &#62;
";

$man=new ManualService("proceed_receivable");
$man->setAuthor($author);
$man->setDataNeeded($response);
$man->setDataProvided($provided);
$man->setDescription($descrption);

echo $man->getHtml();



?>