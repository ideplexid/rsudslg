<?php 

$tab=new Tabulator("laporan", "laporan",Tabulator::$LANDSCAPE);
$tab->setPartialLoad(true,"kasir","laporan","laporan",true);
$tab->add("lap_cash", "Cash", "",Tabulator::$TYPE_HTML," fa fa-money");
//$tab->add("lap_cash_per_nomor", "Cash/Nomor", "",Tabulator::$TYPE_HTML," fa fa-ticket");
$tab->add("lap_bank", "Bank", "",Tabulator::$TYPE_HTML," fa fa-university");
$tab->add("lap_asuransi", "Asuransi", "",Tabulator::$TYPE_HTML," fa fa-credit-card");
$tab->add("lap_perusahaan", "Perusahaan", "",Tabulator::$TYPE_HTML," fa fa-building");
$tab->add("lap_bpjs", "BPJS", "",Tabulator::$TYPE_HTML," fa fa-cc-visa");
$tab->add("lap_non_pasien", "Non Pasien", "",Tabulator::$TYPE_HTML, "fa fa-user");
$tab->add("lap_cash_resep_non_pasien", "Cash Resep Non Pasien", "",Tabulator::$TYPE_HTML, "fa fa-user");
//$tab->add("lap_non_pasien_per_nomor", "Non Pasien", "",Tabulator::$TYPE_HTML, " fa fa-cc-mastercard");
$tab->add("laporan_karcis", "Karcis", "",Tabulator::$TYPE_HTML," fa fa-tag");
          
/*LAPORAN TAHUNAN*/
$tab->add("laporan_tahunan_lain_lain", "Tahunan Lain - Lain", "",Tabulator::$TYPE_HTML,"fa fa-fort-awesome");
$tab->add("laporan_tahunan_karcis", "Tahunan Karcis", "",Tabulator::$TYPE_HTML,"fa fa-ticket");
$tab->add("laporan_tahunan_ambulance", "Tahunan Ambulance", "",Tabulator::$TYPE_HTML,"fa fa-ambulance");
//$tab->add("laporan_tahunan_kamar_mayat", "Tahunan Kamar Mayat", "",Tabulator::$TYPE_HTML,"fa fa-bed");
//$tab->add("laporan_tahunan_obat", "Tahunan Obat", "",Tabulator::$TYPE_HTML,"fa fa-eyedropper");
//$tab->add("laporan_tahunan_ruangan", "Tahunan Ruangan", "",Tabulator::$TYPE_HTML,"fa fa-suitcase");

/*laporan uang masuk*/
$tab->add("laporan_uang_masuk", "Laporan Uang Masuk", "",Tabulator::$TYPE_HTML,"fa fa-sign-in");
$tab->add("lap_kartu", "Laporan Kartu", "",Tabulator::$TYPE_HTML,"fa fa-credit-card-alt");


/*LAPORAN PIUTANG*/
//$tab->add("laporan_piutang_per_jenis", "Piutang/Jenis", "",Tabulator::$TYPE_HTML,"fa fa-line-chart");
//$tab->add("piutang_per_ruang", "Piutang/Ruang", "",Tabulator::$TYPE_HTML,"fa fa-university");
//$tab->add("piutang_per_grup_ruang", "Piutang/Grup", "",Tabulator::$TYPE_HTML,"fa fa-university");
$tab->add("rekap_diskon_pasien", "Rekap Diskon Pasien", "",Tabulator::$TYPE_HTML,"fa fa-percent");
$tab->add("rekap_tagihan_pasien", "Rekap Tagihan Pasien", "",Tabulator::$TYPE_HTML,"fa fa-calendar");
$tab->add("rekap_tagihan_pasien_total", "Rekap Tagihan Total", "",Tabulator::$TYPE_HTML,"fa fa-tag");

          
echo $tab ->getHtml();
?>