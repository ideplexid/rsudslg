<?php 

$tabs=new Tabulator("tambahan_biaya_pasien","tambahan_biaya_pasien",Tabulator::$LANDSCAPE_RIGHT);
$tabs->add("master_tagihan_tambahan","Master Tagihan Tambahan","",Tabulator::$TYPE_HTML," fa fa-list-alt");
$tabs->add("periksa_dokter","Periksa Dokter","",Tabulator::$TYPE_HTML," fa fa-user-md");
$tabs->add("tagihan_backup","Tagihan Pelengkap","",Tabulator::$TYPE_HTML," fa fa-money");
$tabs->add("rekap_tambahan_biaya","Rekapitulasi Tagihan Pelengkap","",Tabulator::$TYPE_HTML," fa fa-book");
$tabs->add("edit_tagihan_bayangan","Edit Bayangan","",Tabulator::$TYPE_HTML," fa fa-pencil");
$tabs->add("kwitansi_tagihan_bayangan","Kwitansi Bayangan","",Tabulator::$TYPE_HTML," fa fa-file","loadKwitansiTagihanBayangan()");
$tabs->add("urutan_kwitansi","Urutan Kwitansi","",Tabulator::$TYPE_HTML," fa fa-list");
$tabs->add("persetujuan_diskon","Persetujuan Diskon","",Tabulator::$TYPE_HTML," fa fa-percent");
$tabs->add("surat_sakit","Surat Sakit","",Tabulator::$TYPE_HTML," fa fa-bed");
$tabs->setPartialLoad(true,"kasir","tambahan_biaya_pasien","tambahan_biaya_pasien",true);
echo $tabs->getHtml();
echo addJS("kasir/resource/js/tambahan_biaya_pasien.js",false);

?>