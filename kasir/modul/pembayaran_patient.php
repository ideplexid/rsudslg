<?php
global $db;

$uri = new Button ( 'select', 'Rawat Inap', 'Rawat Inap' );
$uri ->setClass ( "btn-primary" )
     ->setAction ( "list_pasien.listing('1')" )
     ->setIcon(" fa fa-bed")
     ->setIsButton(Button::$ICONIC_TEXT);

$urj = new Button ( 'select', 'Rawat Jalan', 'Rawat Jalan' );
$urj ->setClass ( "btn-primary" )
     ->setAction ( "list_pasien.listing('0')" )
     ->setIcon(" fa fa-user")
     ->setIsButton(Button::$ICONIC_TEXT);


$reload = new Button ( '', '', '' );
$reload ->setClass ( "btn-primary" )
        ->setIsButton(Button::$ICONIC)
        ->setIcon("fa fa-refresh")
        ->setAction ( "list_registered.selecting()" );

$btn_group  = new ButtonGroup("");
$btn_group ->setMax(10,"Action");
$btn_group  ->addButton($uri)
            ->addButton($urj)
            ->addButton($reload);

require_once("kasir/class/MapRuangan.php");

$nama       = new Text ( 'nama_pasien', 'nama_pasien', "" );
$nrm        = new Text ( 'nrm_pasien', 'nrm_pasien', "" );
$noreg      = new Text ( 'noreg_pasien', 'noreg_pasien', "" );
$noprofile  = null;
$umur       = new Text ( 'umur_pasien', 'umur_pasien', "" );
$alamat     = new Text ( 'alamat_pasien', 'alamat_pasien', "" );
$penanggung = new Text ( 'penanggungjawab', 'penanggungjawab', "" );
$pembayaran = new Text ( 'pembayaran', 'pembayaran', "" );
$asuransi   = new Text ( 'asuransi', 'asuransi', "" );
$perusahaan = new Text ( 'perusahaan', 'perusahaan', "" );
$inap       = new Text ( 'inap', 'inap', "" );
$ruangan    = new Select ( 'ruangan', 'ruangan', MapRuangan::getRuangan() );
$jk         = new Hidden ( 'jk_pasien', 'jk_pasien', "" );


$nama   ->setDisabled ( true );
if(getSettings($db, "cashier-autocomplete-nrm", "0")=="1"){
	echo addJS("kasir/resource/js/autocompletenrm.js",false);
}else{
	$nrm->setDisabled ( true );
}

if(getSettings($db, "cashier-autocomplete-noreg", "0")=="1"){
	echo addJS("kasir/resource/js/autocompletenoreg.js",false);
}else{
	$noreg->setDisabled ( true );
}

if(getSettings($db, "cashier-autocomplete-profile", "0")=="1"){
	echo addJS("kasir/resource/js/autocompleteprofile.js",false);
	$noprofile = new Text ( 'profile_number', 'profile_number', "" );
}


$penanggung ->setDisabled ( true );
$umur       ->setDisabled ( true );
$alamat     ->setDisabled ( true );
$pembayaran ->setDisabled ( true );
$asuransi   ->setDisabled ( true );
$perusahaan ->setDisabled ( true );
$inap       ->setDisabled ( true );
$ruangan    ->setDisabled ( true );

$form   = new Form   ("form_student","","Pasien" );
if($noprofile!=null){
	$form   ->addElement ("No. Profile",$noprofile);
}
$form   ->addElement ("NRM",$nrm)
        ->addElement ("No Registrasi",$noreg)
        ->addElement ("Nama",$nama)
        ->addElement ("P. Jawab",$penanggung)
        ->addElement ("Umur",$umur)
        ->addElement ("Alamat",$alamat)
        ->addElement ("Pembayaran",$pembayaran)
        ->addElement ("Asuransi",$asuransi)
        ->addElement ("Perusahaan",$perusahaan)
        ->addElement ("Layanan",$inap)
        ->addElement ("Ruangan",$ruangan)
        ->addElement ("",$jk)
        ->addElement ("",$btn_group);

$modal_pasien = new Modal ( "layanan_modal", '', "Pilih Pasien" );
$modal_pasien ->setModalSize("full_model");

/* table of current content */
echo $form->getHtml ();
echo $modal_pasien->getHtml ();
echo addJS ("framework/smis/js/table_action.js");
echo addJS ("framework/bootstrap/js/bootstrap-datepicker.js");
echo addCSS("framework/bootstrap/css/datepicker.css");
echo addJS ("framework/smis/js/table_action.js");
echo addJS ("smis-libs-out/webprint/webprint.js", false);

echo "<div class='clear'></div>";
echo "<div class='line'></div>";
echo '<div id="detail_tagihan"></div>';

$close  = new Button("", "", "Batal");
$close	->addClass("btn-primary")
		->setIsButton(Button::$ICONIC_TEXT)
		->setIcon("fa fa-close")
		->setAction("koreksi_pv_dokter.batal()");
$load   = new LoadingBar("rekap_ksr_bar", "");
$modal  = new Modal("rekap_ksr_modal", "", "Processing...");
$modal	->addHTML($load->getHtml(),"after")
		->addFooter($close);
echo $modal ->getHtml();
echo addJS  ("base-js/smis-base-loading.js");
echo addJS  ("kasir/resource/js/list_registered.js",false);
echo addCSS ("kasir/resource/css/list_registered.css",false);
$header     = array();
$header[]   = "Ruang";
$header[]   = "Status";
$header[]   = "Aksi";


$tutup  = new Button ( 'tutup_tagihan', '', 'Tutup Tagihan' );
$tutup  ->setClass ( "btn-danger" )
        ->setAction ( "list_registered.tutup_tagihan('1')" )
        ->setIcon(" fa fa-money")
        ->setIsButton(Button::$ICONIC_TEXT);

$buka = new Button ( 'buka_tagihan', '', 'Buka Tagihan' );
$buka ->setClass ( "btn-warning" )
      ->setAction ( "list_registered.tutup_tagihan('0')" )
      ->setIcon(" fa fa-money")
      ->setIsButton(Button::$ICONIC_TEXT);

$table      = new Table($header);
$table  	->setAction(false)
        	->setFooterVisible(false)
        	->setName("layanan_table");



	$div = new RowSpan();
	$div ->addSpan($table->getHtml()."</br>".$tutup->getHtml()." ".$buka->getHtml(),"4",RowSpan::$TYPE_HTML)
		 ->addSpan("<div id='resume_html'></div>","4",RowSpan::$TYPE_HTML)
		 ->addSpan("<div id='kwitansi_html'></div>","4",RowSpan::$TYPE_HTML);
	$tabulation = new Tabulator ( "rekapitulasi_tagihan_pasien", "rekapitulasi_tagihan_pasien",getSettings($db,"cashier-activate-newmode-orientation",Tabulator::$POTRAIT) );
	$tabulation ->add ( "resume", "Resume", $div, Tabulator::$TYPE_COMPONENT,"fa fa-file-text","hitung_ulang()");
	
/*grup add, edit, delete tagihan*/
if(getSettings($db, "cashier-activate-input-bed", "0")=="1")
	$tabulation->add ( "bed", "Input Bed", "", Tabulator::$TYPE_HTML ,"fa fa-bed","list_registered.load_input('bed','bed')");
if(getSettings($db, "cashier-tagihan-backup", "0")=="1")
	$tabulation->add ( "tagihan_backup_inp", "Input Tambahan Biaya", "", Tabulator::$TYPE_HTML,"fa fa-book","list_registered.load_kasir('tagihan_backup_input','tagihan_backup_inp')" );
if(getSettings($db, "cashier-activate-input-edit-ok", "1")=="1")
	$tabulation->add ( "edit_ok", "Input Operasi", "", Tabulator::$TYPE_HTML ,"fa fa-book","list_registered.load_kasir('edit_ok','edit_ok')");
if(getSettings($db, "cashier-activate-input-periksa", "0")=="1")
	$tabulation->add ( "periksa_dokter", "Input Periksa", "", Tabulator::$TYPE_HTML,"fa fa-book","list_registered.load_kasir('periksa_dokter','periksa_dokter')" );
if(getSettings($db, "cashier-activate-input-visite", "0")=="1")
	$tabulation->add ( "visit", "Input Visite", "", Tabulator::$TYPE_HTML ,"fa fa-book","list_registered.load_input('edit_visit','visit')");
if(getSettings($db, "cashier-activate-input-konsul", "0")=="1")
	$tabulation->add ( "konsul", "Input Konsul", "", Tabulator::$TYPE_HTML ,"fa fa-book","list_registered.load_input('edit_konsul','konsul')");
if(getSettings($db, "cashier-activate-input-tindakan-igd", "0")=="1")
	$tabulation->add ( "tindakan_igd", "Input Tindakan IGD", "", Tabulator::$TYPE_HTML ,"fa fa-book","list_registered.load_input('edit_tindakan_igd','tindakan_igd')");
if(getSettings($db, "cashier-activate-input-tindakan-perawat", "0")=="1")
	$tabulation->add ( "tindakan_perawat", "Input Tindakan Perawat", "", Tabulator::$TYPE_HTML ,"fa fa-book","list_registered.load_input('edit_tindakan_perawat','tindakan_perawat')");
if(getSettings($db, "cashier-activate-input-tindakan-dokter", "0")=="1")
	$tabulation->add ( "tindakan_dokter", "Input Tindakan Dokter", "", Tabulator::$TYPE_HTML ,"fa fa-book","list_registered.load_input('edit_tindakan_dokter','tindakan_dokter')");
if(getSettings($db, "cashier-activate-input-oksigen-central", "0")=="1")
	$tabulation->add ( "oksigen_central", "Input O2 Central", "", Tabulator::$TYPE_HTML ,"fa fa-book","list_registered.load_input('edit_oksigen_central','oksigen_central')");
if(getSettings($db, "cashier-activate-input-oksigen-manual", "0")=="1")
	$tabulation->add ( "oksigen_manual", "Input O2 Manual", "", Tabulator::$TYPE_HTML ,"fa fa-book","list_registered.load_input('edit_oksigen_manual','oksigen_manual')");
if(getSettings($db, "cashier-activate-input-edit-alok", "0")=="1")
	$tabulation->add ( "alok", "Input Alat Obat", "", Tabulator::$TYPE_HTML ,"fa fa-book","list_registered.load_input('edit_alok','alok')");
if(getSettings($db, "cashier-activate-input-edit-alok-connected", "0")=="1")
	$tabulation->add ( "alok_connected", "Input Alat & Obat", "", Tabulator::$TYPE_HTML ,"fa fa-book","list_registered.load_input('edit_alok_connected','alok_connected')");
if(getSettings($db, "cashier-activate-input-edit-radiology", "0")=="1")
	$tabulation->add ( "tab_radiology", "Input Radiology", "", Tabulator::$TYPE_HTML ,"fa fa-book","list_registered.load_penunjang('edit_radiology','tab_radiology')");
if(getSettings($db, "cashier-activate-input-edit-laboratory", "0")=="1")
	$tabulation->add ( "tab_laboratory", "Input Laboratory", "", Tabulator::$TYPE_HTML ,"fa fa-book","list_registered.load_penunjang('edit_laboratory','tab_laboratory')");
if(getSettings($db, "cashier-activate-input-edit-elektromedis", "0")=="1")
	$tabulation->add ( "tab_elektromedis", "Input Elektromedis", "", Tabulator::$TYPE_HTML ,"fa fa-book","list_registered.load_penunjang('edit_elektromedis','tab_elektromedis')");
if(getSettings($db, "cashier-activate-input-edit-mcu", "0")=="1")
	$tabulation->add ( "tab_mcu", "Input Medical Checkup", "", Tabulator::$TYPE_HTML ,"fa fa-book","list_registered.load_penunjang('edit_mcu','tab_mcu')");

/*grup edit only*/
if(getSettings($db, "cashier-activate-input-edit-bed", "0")=="1")
	$tabulation->add ( "edit_bed", "Edit Bed", "", Tabulator::$TYPE_HTML ,"fa fa-pencil","list_registered.load_input('edit_bed','edit_bed')");
if(getSettings($db, "cashier-activate-shadow", "1")=="1")
	$tabulation->add ( "edit_bayangan", "Edit Tagihan Bayangan", "", Tabulator::$TYPE_HTML,"fa fa-pencil","list_registered.load_kasir('edit_bayangan','edit_bayangan')" );

/*grup input pembayaran*/
if(getSettings($db, "cashier-activate-cash", "1")=="1")
	$tabulation->add ( "cash", "Bayar Cash", "", Tabulator::$TYPE_HTML ,"fa fa-money","list_registered.load_kasir('cash','cash')");
if(getSettings($db, "cashier-activate-bank", "1")=="1")
	$tabulation->add ( "bank", "Bayar Bank", "", Tabulator::$TYPE_HTML ,"fa fa-money","list_registered.load_kasir('bank','bank')");
if(getSettings($db, "cashier-activate-assurant", "1")=="1")
	$tabulation->add ( "asurant", "Bayar Asuransi", "", Tabulator::$TYPE_HTML,"fa fa-money","list_registered.load_kasir('asuransi','asurant')");
if(getSettings($db, "cashier-activate-discount", "1")=="1")
	$tabulation->add ( "discount", "Diskon", "", Tabulator::$TYPE_HTML ,"fa fa-money","list_registered.load_kasir('diskon','discount')");

/*grup kwitansi yang sesuai dengan tagihan*/
if(getSettings($db, "cashier-activate-simple-kwitansi", "0")=="1")
	$tabulation->add ( "simple_kwitansi", "Kwitansi Normal", "", Tabulator::$TYPE_HTML ,"fa fa-file","list_registered.load_kasir('simple_kwitansi','simple_kwitansi')");
if(getSettings($db, "cashier-activate-simple-kwitansi-asuransi", "0")=="1")
	$tabulation->add ( "simple_kwitansi_asuransi", "Kwitansi Asuransi", "", Tabulator::$TYPE_HTML ,"fa fa-file","list_registered.load_kasir('simple_kwitansi_asuransi','simple_kwitansi_asuransi')");
if(getSettings($db, "cashier-activate-simple-kwitansi-grup", "0")=="1")
	$tabulation->add ( "simple_kwitansi_grup", "Kwitansi Group", "", Tabulator::$TYPE_HTML ,"fa fa-file","list_registered.load_kasir('simple_kwitansi_grup','simple_kwitansi_grup')");
if(getSettings($db, "cashier-activate-simple-kwitansi-grup-alok", "0")=="1")
	$tabulation->add ( "simple_kwitansi_grup_alok", "Kwitansi Grup Alat & Obat", "", Tabulator::$TYPE_HTML ,"fa fa-file","list_registered.load_kasir('simple_kwitansi_grup_alok','simple_kwitansi_grup_alok')");
if(getSettings($db, "cashier-activate-simple-kwitansi-grup-dokter", "0")=="1")
	$tabulation->add ( "simple_kwitansi_grup_dokter", "Kwitansi Grup Dokter", "", Tabulator::$TYPE_HTML ,"fa fa-file","list_registered.load_kasir('simple_kwitansi_grup_dokter','simple_kwitansi_grup_dokter')");
if(getSettings($db, "cashier-activate-simple-kwitansi-bank-jatim", "0")=="1")
	$tabulation->add ( "kwitansi_bank_jatim", "Kwitansi Bank Jatim", "", Tabulator::$TYPE_HTML,"fa fa-university","list_registered.load_kasir('kwitansi_bank_jatim','kwitansi_bank_jatim')" );
if(getSettings($db, "cashier-activate-simple-kwitansi-rincian", "0")=="1")
	$tabulation->add ( "kwitansi_rincian", "Kwitansi Rincian", "", Tabulator::$TYPE_HTML,"fa fa-university","list_registered.load_kasir('kwitansi_rincian','kwitansi_rincian')" );
	
if(getSettings($db, "cashier-activate-detail-kwitansi", "1")=="1")
	$tabulation->add ( "tagihan", "Kwitansi Detail", "", Tabulator::$TYPE_HTML ,"fa fa-file","list_registered.load_kasir('tagihan','tagihan')");
if(getSettings($db, "cashier-activate-simple-kwitansi-escp", "0")=="1")
	$tabulation->add ( "simple_kwitansi_escp", "Kwitansi ESCP", "", Tabulator::$TYPE_HTML ,"fa fa-file","list_registered.load_kasir('simple_kwitansi_escp','simple_kwitansi_escp')");

/*grup tagihan bayangan*/
if(getSettings($db, "cashier-activate-shadow", "1")=="1")
	$tabulation->add ( "tagihan_bayangan", "Kwitansi Bayangan Detail", "", Tabulator::$TYPE_HTML,"fa fa-user-secret","list_registered.load_kasir('print_tagihan_bayangan','tagihan_bayangan')" );
if(getSettings($db, "cashier-activate-simple-kwitansi-bayangan", "0")=="1")
	$tabulation->add ( "simple_kwitansi_bayangan", "Kwitansi Bayangan Normal ", "", Tabulator::$TYPE_HTML ,"fa fa-user-secret","list_registered.load_kasir('simple_kwitansi_bayangan','simple_kwitansi_bayangan')");
if(getSettings($db, "cashier-activate-simple-kwitansi-bayangan-group", "0")=="1")
	$tabulation->add ( "simple_kwitansi_bayangan_group", "Kwitansi Bayangan Group ", "", Tabulator::$TYPE_HTML ,"fa fa-user-secret","list_registered.load_kasir('simple_kwitansi_bayangan_group','simple_kwitansi_bayangan_group')");

/*grup kwitansi yang tidak sesuai dengan tagihan*/
if(getSettings($db, "cashier-activate-simple-kwitansi-bpjs", "1")=="1")
	$tabulation->add ( "sbpjs", "Kwitansi BPJS", "", Tabulator::$TYPE_HTML ,"fa fa-get-pocket","list_registered.load_kasir('simple_kwitansi_bpjs','sbpjs')");
if(getSettings($db, "cashier-activate-kwitansi-paket", "0")=="1")
	$tabulation->add ( "kwitansi_paket", "Kwitansi Paket", "", Tabulator::$TYPE_HTML ,"fa fa-get-pocket","list_registered.load_kasir('kwitansi_paket','kwitansi_paket')");
if(getSettings($db, "cashier-activate-kwitansi-rumus", "0")=="1")
	$tabulation->add ( "kwitansi_rumus", "Kwitansi Rumus", "", Tabulator::$TYPE_HTML ,"fa fa-get-pocket","list_registered.load_kasir('kwitansi_rumus','kwitansi_rumus')");
if(getSettings($db, "cashier-activate-kwitansi-rumus-detail", "0")=="1")
	$tabulation->add ( "kwitansi_rumus_detail", "Kwitansi Rumus Detail", "", Tabulator::$TYPE_HTML ,"fa fa-get-pocket","list_registered.load_kasir('kwitansi_rumus_detail','kwitansi_rumus_detail')");

/*form*/
if(getSettings($db, "cashier-activate-inacbgs-irja", "0")=="1")
	$tabulation->add ( "bpjs_irja", "Form InaCBG's IRJA", "", Tabulator::$TYPE_HTML ,"fa fa-briefcase","list_registered.load_kasir('inacbgs_irja','bpjs_irja')");
if(getSettings($db, "cashier-activate-inacbgs", "0")=="1")
	$tabulation->add ( "kwitansi_inacbg", "Form InaCBG's", "", Tabulator::$TYPE_HTML ,"fa fa-briefcase","list_registered.load_kasir('kwitansi_inacbg','kwitansi_inacbg')");
if(getSettings($db, "cashier-activate-simple-kwitansi-grup-harian", "0")=="1")
	$tabulation->add ( "simple_kwitansi_grup_harian", "Form InaCBG's IRNA", "", Tabulator::$TYPE_HTML ,"fa fa-briefcase","list_registered.load_kasir('simple_kwitansi_grup_harian','simple_kwitansi_grup_harian')");

/*dokumen pasien yang di scan dari pendaftaran*/
if(getSettings($db, "cashier-activate-document", "1")=="1")
	$tabulation->add ( "dokumen", "Dokumen", "", Tabulator::$TYPE_HTML ,"fa fa-file-zip-o","list_registered.load_kasir('dokumen_pasien','dokumen')");
		
echo $tabulation->getHtml();


$ksr_check_uri          = getSettings($db, "cashier-out-check-uri", "0");
$ksr_check_urj          = getSettings($db, "cashier-out-check-urj", "0");
$ksr_autoload           = getSettings($db, "cashier-real-time-tagihan", "0")=="2"?"0":"1";
$var_check_uri          = new Hidden("cashier_out_check_uri", "", $ksr_check_uri);
$var_check_urj          = new Hidden("cashier_out_check_urj", "", $ksr_check_urj);
$var_autoload           = new Hidden("cashier_autoload","",$ksr_autoload);
$var_cashier_reg_select = new Hidden("id_list_registered_select","",$_POST['list_registered_select']);


echo $var_check_uri          ->getHtml();
echo $var_check_urj          ->getHtml();
echo $var_autoload           ->getHtml();
echo $var_cashier_reg_select ->getHtml();
echo addJS  ("framework/bootstrap/js/bootstrap-datetimepicker.js");
echo addCSS ("framework/bootstrap/css/bootstrap-datetimepicker.css");
echo addJS  ("kasir/resource/js/pembayaran_patient.js",false);
if(getSettings($db,"cashier-show-back-action","1")=="0"){
	echo addCSS ("kasir/resource/css/hide_action_pengembalian_pasien.css",false);
}
?>