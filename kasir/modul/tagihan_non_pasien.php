<?php
global $db;
global $user;

if($_POST['super_command']!=""){
	$super=new SuperCommand();
	$dbtable=new DBTable($db, "smis_ksr_nonpasien");
	$uitable=new Table(array("Ket. Pembayaran","Harga","Keterangan"));
	$uitable->setModel(Table::$SELECT);
	$uitable->setName("master_non_pasien");
	$adapter=new SimpleAdapter();
	$adapter->add("Ket. Pembayaran", "nama");
	$adapter->add("Harga", "harga","money Rp.");
	$adapter->add("Keterangan", "keterangan");
	$res=new DBResponder($dbtable, $uitable, $adapter);
	$super->addResponder("master_non_pasien", $res);

	$dbtable=new DBTable($db, "smis_amb_sewa_ambulan");
	$uitable=new Table(array("Nama","Biaya"));
	$uitable->setModel(Table::$SELECT);
	$uitable->setName("master_ambulan");
	$adapter=new SimpleAdapter();
	$adapter->add("Nama", "nama");
	$adapter->add("Biaya", "biaya_perjalanan","money Rp.");
	$res=new DBResponder($dbtable, $uitable, $adapter);
	$super->addResponder("master_ambulan", $res);


	/*
require_once "smis-base/smis-include-service-consumer.php";
$uitable=new Table(array("Nama","Akun","Keterangan"),"");
$uitable->setName("dompet_np");
$uitable->setModel(Table::$SELECT);
$adapter=new SimpleAdapter();
$adapter->add("Nama","nama");
$adapter->add("Akun","nomor_akun");
$adapter->add("Keterangan","keterangan");
$responder=new ServiceResponder($db,$uitable,$adapter,"get_dompet","finance");
$super->addResponder("dompet_np", $responder);*/

	$a=$super->initialize();
	if($a!=NULL){
		echo $a;
		return;
	}

	
}





require_once 'kasir/class/table/TagihanNonPasienTable.php';
require_once ("smis-framework/smis/database/DBParentChildResponder.php");
$head=array('No.','No. Kwitansi','Tanggal',"Nama",'Keterangan','Nilai',"Operator");
$parent_table = new TagihanNonPasienTable ( $head, "", NULL, true );
$parent_table->setName ( "tagihan_non_pasien_parent" );
$parent_table->setActionName ( "tagihan_non_pasien.parent" );
$parent_table->setPrintElementButtonEnable(true);
$parent_table->setDelButtonEnable(getSettings($db,"cashier-non-pasien-enabled-delete","1")=="1");

$child_table = new Table (array("No.","Ket. Pembayaran","Harga Satuan","Jumlah","Total"));
$child_table->setName ( "tagihan_non_pasien_child" );
$child_table->setActionName ( "tagihan_non_pasien.child" );
$child_table->setClass ( "" );
$child_table->setFooterVisible(false);

/* this is respond when system have to response */
if (isset ( $_POST ['command'] )) {	
    require_once "kasir/class/responder/NonPasienParentResponder.php";
	// parent
	$parent_adapter = new SimpleAdapter();
	$parent_adapter->add("No. Kwitansi", "id","digit8");
	$parent_adapter->add("Tanggal", "tanggal","date d M Y H:i");
	$parent_adapter->add("Nama", "nama");
	$parent_adapter->add("Operator", "operator");
	$parent_adapter->add("Keterangan", "keterangan");
	$parent_adapter->add("Nilai", "nilai","money Rp.");
	$parent_adapter->add("Dompet", "nama_dompet");
	$parent_adapter->setUseNumber(true,"No.","back.");
	$parent_dbtable = new DBTable ( $db, "smis_ksr_np");
	$parent_dbtable->setOrder(" id DESC ");
	$parent = new NonPasienParentResponder ( $parent_dbtable, $parent_table, $parent_adapter );
	
	// child
	$child_adapter = new SummaryAdapter();
	$child_adapter->setUseNumber(true,"No.","back.");
	$child_adapter->addSummary("Total", "total","money Rp.");
	$child_adapter->addFixValue("Jumlah", "<strong>Total</strong>");
	$child_adapter->add("Ket. Pembayaran", "nama");
	$child_adapter->add("Harga Satuan", "harga_satuan","money Rp.");
	$child_adapter->add("Jumlah", "jumlah");
	$child_adapter->add("Total", "total","money Rp.");	
    
	$child_dbtable = new DBTable ( $db, "smis_ksr_npd");
	$child_dbtable->setShowAll(true);
	$child = new DBChildResponder( $child_dbtable, $child_table, $child_adapter, "id_np" );
	$dbres = new DBParentChildResponder ( $parent, $child );
	
	if($dbres->isChildProcess($_POST['super_command']) && $child->isSave()){
		$child->addColumnFixValue("total", $_POST['harga_satuan']*$_POST['jumlah']);
		if($_POST['unit']=="ambulan"){
			$_POST['nama']=$_POST['nama2'];
		}
	}else if($parent->isSave()){
		$query="SELECT sum(total) as jumlah FROM smis_ksr_npd WHERE id_np='".$_POST['id']."' AND prop!='del';";
		$total=$db->get_var($query);
		$parent->addColumnFixValue("nilai", $total);
		$parent->addColumnFixValue("operator", $user->getNameOnly());
	}
	
	$data = $dbres->command ( $_POST ['super_command'], $_POST ['command'] );
	echo json_encode ( $data );
	return;
}


$unit = new OptionBuilder();
$unit->add("Ambulan","ambulan");
$unit->add("Kasir","kasir");

$parent_table->addModal("id", "hidden", "", "");
$parent_table->addModal("id_np", "hidden", "", "");
$parent_table->addModal("nama", "text", "Nama", "","n",null,false,null,true,"tanggal");
$parent_table->addModal("tanggal", "datetime", "Tanggal", date("Y-m-d H:i"),"y",null,false,null,false,"nama");
$parent_table->addModal("keterangan", "text", "Keterangan", "","y",null,false,null,false,"operator");
$parent_table->addModal("operator", "text", "Operator", $user->getNameOnly(),"y",null,true,null,false,"nama_dompet");

//$parent_table->addModal("nama_dompet", "chooser-tagihan_non_pasien.parent-dompet_np-Dompet", "Dompet", "","y",null,false,null,false,"save");

$tab = new Tabulator("","",Tabulator::$POTRAIT);
$tab->add("child_tb","Tagihan",$child_table->getHtml());
$tab->add("tnp_cash","Cash","kasir/resource/php/tagihan_non_pasien/cash_np.php",Tabulator::$TYPE_INCLUDE);
$tab->add("tnp_bank","Bank","kasir/resource/php/tagihan_non_pasien/bank_np.php",Tabulator::$TYPE_INCLUDE);
$tab->add("tnp_asuransi","Asuransi","kasir/resource/php/tagihan_non_pasien/asuransi_np.php",Tabulator::$TYPE_INCLUDE);

$modal_parent = $parent_table->getModal ();
$modal_parent->setTitle ( "Pendapatan Non Pasien" );
$modal_parent->addBody ( "", new Html("","","<div class='clear'></div>") );
$modal_parent->addBody ( "detail_tagihan_non_pasien", $tab );
$modal_parent->setClass ( Modal::$FULL_MODEL );

/* Child Modal */
$child_table->addModal("id", "hidden", "", "");
$child_table->addModal("id_np", "hidden", "", "");
$child_table->addModal("debet", "hidden", "", "");
$child_table->addModal("kredit", "hidden", "", "");
$child_table->addModal("unit", "select", "Unit", $unit->getContent(),"n",null,false,null,true,"tanggal");
$child_table->addModal("nama", "chooser-tagihan_non_pasien.child-master_non_pasien-Keterangan Pembayaran", "Ket. Pembayaran", "","n",null,false,null,true);
$child_table->addModal("nama2", "chooser-tagihan_non_pasien.child-master_ambulan-Ambulan", "Ket. Pembayaran", "","n",null,false,null,true);
$child_table->addModal("id_layanan", "hidden", "", "","y",null,false,null,false,"harga_satuan");
$child_table->addModal("harga_satuan", "money", "Harga Satuan", "","y",null,false,null,false,"jumlah");
$child_table->addModal("jumlah", "text", "Jumlah", "1","y",null,false,null,false,"total");
$child_table->addModal("total", "money", "Total", "1","y",null,false,null,false,"save");

$modal_child = $child_table->getModal ();
$modal_child->setTitle ( "Keterangan Pembayaran" );
$modal_child->setComponentSize(Modal::$MEDIUM);

echo $parent_table->getHtml ();
echo $modal_parent->getHtml ();
echo $modal_child->getHtml ();

$uitable = new Table ( array(), "Pembayaran Cash Non Pasien", NULL, true );
$uitable->setName("cash_np");
$uitable->addModal ( "id", "hidden", "", "" );
$uitable->addModal ( "carabayar", "hidden", "", $this->carabayar );
$uitable->addModal ( "waktu", "datetime", "Tanggal", date ( "Y-m-d H:i" ) );
$uitable->addModal ( "nilai", "money", "Nilai", "" );
$uitable->addModal ( "keterangan", "textarea", "Keterangan", "" );
echo $uitable->getModal()->setTitle ( "Pembayaran Non Pasien" )->getHtml();

$uitable = new Table ( array(), "Pembayaran Bank Non Pasien", NULL, true );
$uitable->setName("bank_np");
require_once 'kasir/class/adapter/BankAdapter.php';
$vtable = new DBTable ( $db, "smis_ksr_bank" );
$vtable->setOrder ( "bank ASC, rekening ASC, nama ASC " );
$vtable->setShowAll ( true );
$v = $vtable->view ( "", 0 );
$tvadapter = new BankAdapater ();
$bank = $tvadapter->getContent ( $v ['data'] );
$uitable->addModal ( "id", "hidden", "", "" );
$uitable->addModal ( "waktu", "datetime", "Tanggal", date ( "Y-m-d H:i" ) );
$uitable->addModal ( "id_bank", "select", "Bank", $bank );
$uitable->addModal ( "no_bukti", "text", "No. Bukti", "" );
$uitable->addModal ( "nilai", "money", "Nilai", "" );
$uitable->addModal ( "persen", "text", "Persentase Biaya", "2.5" );
$uitable->addModal ( "nilai_tambahan", "money", "Nilai Biaya", "" );
$uitable->addModal ( "di_bank", "checkbox", "Lewat Bank", "1" );
$uitable->addModal ( "keterangan", "textarea", "Keterangan", "" );	
echo $uitable->getModal()->setTitle ( "Pembayaran Non Pasien" )->getHtml();

$uitable = new Table ( array(), "Pembayaran Asuransi Non Pasien", NULL, true );
$uitable->setName("asuransi_np");
$uitable->addModal ( "id", "hidden", "", "" );
$uitable->addModal ( "id_asuransi", "hidden", "", "" );
$uitable->addModal ( "id_perusahaan", "hidden", "", "" );
$uitable->addModal ( "waktu", "datetime", "Tanggal", date ( "Y-m-d H:i" ) );
$uitable->addModal ( "nama_asuransi", "chooser-asuransi_np-asuransi_np_asuransi-Asuransi", "Asuransi", "" );
$uitable->addModal ( "nama_perusahaan", "chooser-asuransi_np-asuransi_np_perusahaan-Perusahaan", "Perusahaan", "" );
$uitable->addModal ( "no_bukti", "text", "No. Bukti", "" );
$uitable->addModal ( "nilai", "money", "Nilai", "" );
$uitable->addModal ( "terklaim", "checkbox", "Terbayar", "0" );
$uitable->addModal ( "keterangan", "textarea", "Keterangan", "" );
echo $uitable->getModal()->setTitle ( "Pembayaran Non Pasien" )->getHtml();

echo addJS ( "framework/bootstrap/js/bootstrap-datetimepicker.js" );
echo addJS ( "framework/smis/js/table_action.js" );
echo addJS ( "framework/smis/js/child_parent.js" );
echo addJS ( "kasir/resource/js/tagihan_non_pasien.js",false );
echo addCSS ( "framework/bootstrap/css/bootstrap-datetimepicker.css" );
echo addCSS("kasir/resource/css/tagihan_non_pasien.css",false);
?>