<?php
require_once 'smis-framework/smis/api/SettingsBuilder.php';
require_once 'smis-base/smis-include-service-consumer.php';
global $db;
$settings = new SettingsBuilder( $db, "reg_settings", "kasir", "mapping_akunting","Settings Kasir" );
$settings->setShowDescription ( true );
$settings->setShowHelpButton(true);

if($settings->isGroupAdded("asuransi_piutang", "Asuransi - Piutang","fa fa-ticket")){    
    /*kode asuransi default yang di detailkan nama asuransi*/
    require_once "smis-base/smis-include-service-consumer.php";
    $serv       = new ServiceConsumer($db,"get_all_asuransi",NULL,"registration");
    $serv       ->execute();
    $list       = $serv->getContent();
    $settings   ->addSectionCurrent ("Kode Akun Per Asuransi");
    foreach($list as $x){
        $settings->addCurrent ( "cashier-acc-d-asuransi-".$x['id'], "Akun Debit Asuransi <strong>".$x['nama']."</strong>", "", "chooser-mapping_akunting-debet_asuransi_".$x['id']."-Debet", "",false,"","",true  );
        $settings->addSuperCommandAction("debet_asuransi_".$x['id'],"kode_akun");
        $settings->addSuperCommandArray("debet_asuransi_".$x['id'],"cashier-acc-d-asuransi-".$x['id'],"nomor",true);
        $settings->addCurrent ( "cashier-acc-k-asuransi-".$x['id'], "Akun Kredit Asuransi <strong>".$x['nama']."</strong>", "", "chooser-mapping_akunting-kredit_asuransi_".$x['id']."-Kredit", "",false,"","",true  );
        $settings->addSuperCommandAction("kredit_asuransi_".$x['id'],"kode_akun");
        $settings->addSuperCommandArray("kredit_asuransi_".$x['id'],"cashier-acc-k-asuransi-".$x['id'],"nomor",true);
        $settings->addCurrent ( "cashier-acc-d-asuransi-resep-".$x['id'], "Akun Debit Asuransi Resep <strong>".$x['nama']."</strong>", "", "chooser-mapping_akunting-debet_resep_asuransi_".$x['id']."-Debet", "",false,"","",true  );
        $settings->addSuperCommandAction("debet_resep_asuransi_".$x['id'],"kode_akun");
        $settings->addSuperCommandArray("debet_resep_asuransi_".$x['id'],"cashier-acc-d-asuransi-resep-".$x['id'],"nomor",true);
        $settings->addCurrent ( "cashier-acc-k-asuransi-resep-".$x['id'], "Akun Kredit Asuransi Resep <strong>".$x['nama']."</strong>", "", "chooser-mapping_akunting-kredit_resep_asuransi_".$x['id']."-Kredit", "",false,"","",true  );
        $settings->addSuperCommandAction("kredit_resep_asuransi_".$x['id'],"kode_akun");
        $settings->addSuperCommandArray("kredit_resep_asuransi_".$x['id'],"cashier-acc-k-asuransi-resep-".$x['id'],"nomor",true);
    }
    
    /*kode asuransi default yang tidak di detailkan nama asuransi, tetapi ada jenis pasienya*/
    $serv = new ServiceConsumer($this->db,"get_jenis_patient",NULL,"registration");
    $serv ->execute();
    $res  = $serv->getContent();
    $settings->addSectionCurrent ("Kode Akun Per Jenis Pasien tanpa Asuransi");
    foreach($res as $y){
        $x['id']    = $y['value'];
        $x['nama']  = $y['name'];
        $settings->addCurrent ( "cashier-acc-d-asuransi-".$x['id'], "Akun Debit Asuransi <strong>".$x['nama']."</strong>", "", "chooser-mapping_akunting-debet_asuransi_".$x['id']."-Debet", "",false,"","",true  );
        $settings->addSuperCommandAction("debet_asuransi_".$x['id'],"kode_akun");
        $settings->addSuperCommandArray("debet_asuransi_".$x['id'],"cashier-acc-d-asuransi-".$x['id'],"nomor",true);
        $settings->addCurrent ( "cashier-acc-k-asuransi-".$x['id'], "Akun Kredit Asuransi <strong>".$x['nama']."</strong>", "", "chooser-mapping_akunting-kredit_asuransi_".$x['id']."-Kredit", "",false,"","",true  );
        $settings->addSuperCommandAction("kredit_asuransi_".$x['id'],"kode_akun");
        $settings->addSuperCommandArray("kredit_asuransi_".$x['id'],"cashier-acc-k-asuransi-".$x['id'],"nomor",true);
        $settings->addCurrent ( "cashier-acc-d-asuransi-resep-".$x['id'], "Akun Debit Asuransi Resep <strong>".$x['nama']."</strong>", "", "chooser-mapping_akunting-debet_resep_asuransi_".$x['id']."-Debet", "",false,"","",true  );
        $settings->addSuperCommandAction("debet_resep_asuransi_".$x['id'],"kode_akun");
        $settings->addSuperCommandArray("debet_resep_asuransi_".$x['id'],"cashier-acc-d-asuransi-resep-".$x['id'],"nomor",true);
        $settings->addCurrent ( "cashier-acc-k-asuransi-resep-".$x['id'], "Akun Kredit Asuransi Resep <strong>".$x['nama']."</strong>", "", "chooser-mapping_akunting-kredit_resep_asuransi_".$x['id']."-Kredit", "",false,"","",true  );
        $settings->addSuperCommandAction("kredit_resep_asuransi_".$x['id'],"kode_akun");
        $settings->addSuperCommandArray("kredit_resep_asuransi_".$x['id'],"cashier-acc-k-asuransi-resep-".$x['id'],"nomor",true);
    }
    
    /*kode asuransi default yang tidak di detailkan nama asuransi dan tidak di detailkan jenis pasienya*/
    $settings->addSectionCurrent ("Kode Akun Resep");
    $settings->addCurrent ( "cashier-acc-d-asuransi-resep-", "Akun Debit Asuransi Resep  Non Pasien", "", "chooser-mapping_akunting-debet_resep_asuransi_-Debet", "",false,"","",true );
    $settings->addSuperCommandAction("debet_resep_asuransi_","kode_akun");
    $settings->addSuperCommandArray("debet_resep_asuransi_","cashier-acc-d-asuransi-resep-","nomor",true);    
    $settings->addCurrent ( "cashier-acc-k-asuransi-resep-", "Akun Kredit Asuransi Resep Non Pasien", "", "chooser-mapping_akunting-kredit_resep_asuransi_-Kredit", "",false,"","",true );
    $settings->addSuperCommandAction("kredit_resep_asuransi_","kode_akun");
    $settings->addSuperCommandArray("kredit_resep_asuransi_","cashier-acc-k-asuransi-resep-","nomor",true);
}

if($settings->isGroupAdded("asuransi_pelunasan", "Asuransi - Pelunasan","fa fa-ticket")){    
    /*kode asuransi default yang di detailkan nama asuransi*/
    require_once "smis-base/smis-include-service-consumer.php";
    $serv       = new ServiceConsumer($db,"get_all_asuransi",NULL,"registration");
    $serv       ->execute();
    $list       = $serv->getContent();
    $settings   ->addSectionCurrent ("Kode Akun Per Asuransi");
    foreach($list as $x){
        $settings->addCurrent ( "cashier-acc-d-asuransi-lunas-".$x['id'], "Akun Debit Asuransi <strong>".$x['nama']."</strong>", "", "chooser-mapping_akunting-debet_asuransi_lunas_".$x['id']."-Debet", "",false,"","",true  );
        $settings->addSuperCommandAction("debet_asuransi_lunas_".$x['id'],"kode_akun");
        $settings->addSuperCommandArray("debet_asuransi_lunas_".$x['id'],"cashier-acc-d-asuransi-lunas-".$x['id'],"nomor",true);
        $settings->addCurrent ( "cashier-acc-k-asuransi-lunas-".$x['id'], "Akun Kredit Asuransi <strong>".$x['nama']."</strong>", "", "chooser-mapping_akunting-kredit_asuransi_lunas_".$x['id']."-Kredit", "",false,"","",true  );
        $settings->addSuperCommandAction("kredit_asuransi_lunas_".$x['id'],"kode_akun");
        $settings->addSuperCommandArray("kredit_asuransi_lunas_".$x['id'],"cashier-acc-k-asuransi-lunas-".$x['id'],"nomor",true);
        $settings->addCurrent ( "cashier-acc-d-asuransi-resep-lunas-".$x['id'], "Akun Debit Asuransi Resep <strong>".$x['nama']."</strong>", "", "chooser-mapping_akunting-debet_resep_asuransi_lunas_".$x['id']."-Debet", "",false,"","",true  );
        $settings->addSuperCommandAction("debet_resep_asuransi_lunas_".$x['id'],"kode_akun");
        $settings->addSuperCommandArray("debet_resep_asuransi_lunas_".$x['id'],"cashier-acc-d-asuransi-resep-lunas-".$x['id'],"nomor",true);
        $settings->addCurrent ( "cashier-acc-k-asuransi-resep-lunas-".$x['id'], "Akun Kredit Asuransi Resep <strong>".$x['nama']."</strong>", "", "chooser-mapping_akunting-kredit_resep_asuransi_lunas_".$x['id']."-Kredit", "",false,"","",true  );
        $settings->addSuperCommandAction("kredit_resep_asuransi_lunas_".$x['id'],"kode_akun");
        $settings->addSuperCommandArray("kredit_resep_asuransi_lunas_".$x['id'],"cashier-acc-k-asuransi-resep-lunas-".$x['id'],"nomor",true);
    }
    
    /*kode asuransi default yang tidak di detailkan nama asuransi, tetapi ada jenis pasienya*/
    $serv = new ServiceConsumer($this->db,"get_jenis_patient",NULL,"registration");
    $serv ->execute();
    $res  = $serv->getContent();
    $settings->addSectionCurrent ("Kode Akun Per Jenis Pasien tanpa Asuransi");
    foreach($res as $y){
        $x['id']    = $y['value'];
        $x['nama']  = $y['name'];
        $settings->addCurrent ( "cashier-acc-d-asuransi-lunas-".$x['id'], "Akun Debit Asuransi <strong>".$x['nama']."</strong>", "", "chooser-mapping_akunting-debet_asuransi_lunas_".$x['id']."-Debet", "",false,"","",true  );
        $settings->addSuperCommandAction("debet_asuransi_lunas_".$x['id'],"kode_akun");
        $settings->addSuperCommandArray("debet_asuransi_lunas_".$x['id'],"cashier-acc-d-asuransi-lunas-".$x['id'],"nomor",true);
        $settings->addCurrent ( "cashier-acc-k-asuransi-lunas-".$x['id'], "Akun Kredit Asuransi <strong>".$x['nama']."</strong>", "", "chooser-mapping_akunting-kredit_asuransi_lunas_".$x['id']."-Kredit", "",false,"","",true  );
        $settings->addSuperCommandAction("kredit_asuransi_lunas_".$x['id'],"kode_akun");
        $settings->addSuperCommandArray("kredit_asuransi_lunas_".$x['id'],"cashier-acc-k-asuransi-lunas-".$x['id'],"nomor",true);
        $settings->addCurrent ( "cashier-acc-d-asuransi-resep-lunas-".$x['id'], "Akun Debit Asuransi Resep <strong>".$x['nama']."</strong>", "", "chooser-mapping_akunting-debet_resep_asuransi_lunas_".$x['id']."-Debet", "",false,"","",true  );
        $settings->addSuperCommandAction("debet_resep_asuransi_lunas_".$x['id'],"kode_akun");
        $settings->addSuperCommandArray("debet_resep_asuransi_lunas_".$x['id'],"cashier-acc-d-asuransi-resep-lunas-".$x['id'],"nomor",true);
        $settings->addCurrent ( "cashier-acc-k-asuransi-resep-lunas-".$x['id'], "Akun Kredit Asuransi Resep <strong>".$x['nama']."</strong>", "", "chooser-mapping_akunting-kredit_resep_asuransi_lunas_".$x['id']."-Kredit", "",false,"","",true  );
        $settings->addSuperCommandAction("kredit_resep_asuransi_lunas_".$x['id'],"kode_akun");
        $settings->addSuperCommandArray("kredit_resep_asuransi_lunas_".$x['id'],"cashier-acc-k-asuransi-resep-lunas-".$x['id'],"nomor",true);
    }
    
    /*kode asuransi default yang tidak di detailkan nama asuransi dan tidak di detailkan jenis pasienya*/
    $settings->addSectionCurrent ("Kode Akun Resep");
    $settings->addCurrent ( "cashier-acc-d-asuransi-resep-lunas-", "Akun Debit Asuransi Resep  Non Pasien", "", "chooser-mapping_akunting-debet_resep_asuransi_lunas_-Debet", "",false,"","",true );
    $settings->addSuperCommandAction("debet_resep_asuransi_lunas_","kode_akun");
    $settings->addSuperCommandArray("debet_resep_asuransi_lunas_","cashier-acc-d-asuransi-resep-lunas-","nomor",true);    
    $settings->addCurrent ( "cashier-acc-k-asuransi-resep-lunas-", "Akun Kredit Asuransi Resep Non Pasien", "", "chooser-mapping_akunting-kredit_resep_asuransi_lunas_-Kredit", "",false,"","",true );
    $settings->addSuperCommandAction("kredit_resep_asuransi_lunas_","kode_akun");
    $settings->addSuperCommandArray("kredit_resep_asuransi_lunas_","cashier-acc-k-asuransi-resep-lunas-","nomor",true);
}

if($settings->isGroupAdded("perusahaan", "Perusahaan","fa fa-building" )){
    require_once "smis-base/smis-include-service-consumer.php";
    $serv   = new ServiceConsumer($db,"get_all_perusahaan",NULL,"registration");
    $serv   ->execute();
    $list   = $serv->getContent();    
    $settings   ->addSectionCurrent ("Kode Akun Per Perusahaan");
    foreach($list as $x){
        $settings->addCurrent (  "cashier-acc-d-perusahaan-".$x['id'], "Akun Debit Perusahaan <strong>".$x['nama']."</strong>", "", "chooser-mapping_akunting-debet_perusahaan_".$x['id']."-Debet", "",false,"","",true);
        $settings->addSuperCommandAction("debet_perusahaan_".$x['id'],"kode_akun");
        $settings->addSuperCommandArray("debet_perusahaan_".$x['id'],"cashier-acc-d-perusahaan-".$x['id'],"nomor",true);
        $settings->addCurrent (  "cashier-acc-k-perusahaan-".$x['id'], "Akun Kredit Perusahaan <strong>".$x['nama']."</strong>", "", "chooser-mapping_akunting-kredit_perusahaan_".$x['id']."-Kredit", "",false,"","",true );
        $settings->addSuperCommandAction("kredit_perusahaan_".$x['id'],"kode_akun");
        $settings->addSuperCommandArray("kredit_perusahaan_".$x['id'],"cashier-acc-k-perusahaan-".$x['id'],"nomor",true);
        $settings->addCurrent (  "cashier-acc-d-perusahaan-resep-".$x['id'], "Akun Debit Perusahaan Resep <strong>".$x['nama']."</strong>", "", "chooser-mapping_akunting-debet_resep_perusahaan_".$x['id']."-Debet", "",false,"","",true);
        $settings->addSuperCommandAction("debet_resep_perusahaan_".$x['id'],"kode_akun");
        $settings->addSuperCommandArray("debet_resep_perusahaan_".$x['id'],"cashier-acc-d-perusahaan-resep-".$x['id'],"nomor",true);
        $settings->addCurrent (  "cashier-acc-k-perusahaan-resep-".$x['id'], "Akun Kredit Perusahaan Resep <strong>".$x['nama']."</strong>", "", "chooser-mapping_akunting-kredit_resep_perusahaan_".$x['id']."-Kredit", "",false,"","",true);
        $settings->addSuperCommandAction("kredit_resep_perusahaan_".$x['id'],"kode_akun");
        $settings->addSuperCommandArray("kredit_resep_perusahaan_".$x['id'],"cashier-acc-k-perusahaan-resep-".$x['id'],"nomor",true);
    }
    $settings   ->addSectionCurrent ("Kode Akun Resep Tanpa Perusahaan");
    $settings->addCurrent (  "cashier-acc-d-perusahaan-resep-", "Akun Debit Perusahaan Resep Non Pasien ", "", "chooser-mapping_akunting-debet_resep_perusahaan_-Debet", "",false,"","",true);
    $settings->addSuperCommandAction("debet_resep_perusahaan_","kode_akun");
    $settings->addSuperCommandArray("debet_resep_perusahaan_","cashier-acc-d-perusahaan-resep-","nomor",true); 
    $settings->addCurrent (  "cashier-acc-k-perusahaan-resep-", "Akun Kredit Perusahaan Resep Non Pasien", "", "chooser-mapping_akunting-kredit_resep_perusahaan_-Kredit", "",false,"","",true);
    $settings->addSuperCommandAction("kredit_resep_perusahaan_","kode_akun");
    $settings->addSuperCommandArray("kredit_resep_perusahaan_","cashier-acc-k-perusahaan-resep-","nomor",true);
}

if($settings->isGroupAdded("bank", "Bank","fa fa-university")){
    $dbtable = new DBTable ( $db, "smis_ksr_bank" );
    $dbtable->setShowAll(true);
    $data=$dbtable->view("",0);
    $list=$data['data'];
    $settings   ->addSectionCurrent ("Kode Akun Per Perusahaan");
    foreach($list as $x){
        $settings->addCurrent ( "cashier-acc-d-bank-".$x->id, "Akun Debit Bank <strong>".$x->bank."</strong>", "", "chooser-mapping_akunting-debet_bank_".$x->id."-Debet", "",false,"","",true );
        $settings->addSuperCommandAction("debet_bank_".$x->id,"kode_akun");
        $settings->addSuperCommandArray("debet_bank_".$x->id,"cashier-acc-d-bank-".$x->id,"nomor",true);
        $settings->addCurrent ( "cashier-acc-k-bank-".$x->id, "Akun Kredit Bank <strong>".$x->bank."</strong>", "", "chooser-mapping_akunting-kredit_bank_".$x->id."-Kredit", "",false,"","",true );
        $settings->addSuperCommandAction("kredit_bank_".$x->id,"kode_akun");
        $settings->addSuperCommandArray("kredit_bank_".$x->id,"cashier-acc-k-bank-".$x->id,"nomor",true);
        $settings->addCurrent ( "cashier-acc-d-bank-resep-".$x->id, "Akun Debit Bank Resep <strong>".$x->bank."</strong>", "", "chooser-mapping_akunting-debet_resep_bank_".$x->id."-Debet", "",false,"","",true );
        $settings->addSuperCommandAction("debet_resep_bank_".$x->id,"kode_akun");
        $settings->addSuperCommandArray("debet_resep_bank_".$x->id,"cashier-acc-d-bank-resep-".$x->id,"nomor",true);
        $settings->addCurrent ( "cashier-acc-k-bank-resep-".$x->id, "Akun Kredit Bank Resep <strong>".$x->bank."</strong>", "", "chooser-mapping_akunting-kredit_resep_bank_".$x->id."-Kredit", "",false,"","",true );
        $settings->addSuperCommandAction("kredit_resep_bank_".$x->id,"kode_akun");
        $settings->addSuperCommandArray("kredit_resep_bank_".$x->id,"cashier-acc-k-bank-resep-".$x->id,"nomor",true);
    }
    $settings   ->addSectionCurrent ("Kode Akun Resep Tanpa Perusahaan");
    $settings->addCurrent ( "cashier-acc-d-bank-resep-", "Akun Debit Bank Resep Non Pasien ", "", "chooser-mapping_akunting-debet_resep_bank_-Debet", "",false,"","",true );
    $settings->addSuperCommandAction("debet_resep_bank_","kode_akun");
    $settings->addSuperCommandArray("debet_resep_bank_","cashier-acc-d-bank-resep-","nomor",true); 
    $settings->addCurrent ( "cashier-acc-k-bank-resep-", "Akun Kredit Bank Resep Non Pasien", "", "chooser-mapping_akunting-kredit_resep_bank_-Kredit", "",false,"","",true );
    $settings->addSuperCommandAction("kredit_resep_bank_","kode_akun");
    $settings->addSuperCommandArray("kredit_resep_bank_","cashier-acc-k-bank-resep-","nomor",true);
}

if($settings->isGroupAdded("cash", "Cash","fa fa-money")){
    require_once "smis-base/smis-include-service-consumer.php";
    $serv=new ServiceConsumer($db,"get_jenis_patient",NULL,"registration");
    $serv->execute();
    $list=$serv->getContent();
    $settings   ->addSectionCurrent ("Kode Akun Per Jenis Pasien");
    foreach($list as $x){
        $settings->addCurrent ("cashier-acc-d-cash-".$x['value'], "Akun Debit Cash <strong>".$x['name']."</strong>", "", "chooser-mapping_akunting-debet_cash_".$x['value']."-Debet", "",false,"","",true);
        $settings->addSuperCommandAction("debet_cash_".$x['value'],"kode_akun");
        $settings->addSuperCommandArray("debet_cash_".$x['value'],"cashier-acc-d-cash-".$x['value'],"nomor",true);
        $settings->addCurrent ("cashier-acc-k-cash-".$x['value'], "Akun Kredit Cash <strong>".$x['name']."</strong>", "", "chooser-mapping_akunting-kredit_cash_".$x['value']."-Kredit", "",false,"","",true);
        $settings->addSuperCommandAction("kredit_cash_".$x['value'],"kode_akun");
        $settings->addSuperCommandArray("kredit_cash_".$x['value'],"cashier-acc-k-cash-".$x['value'],"nomor",true);
        $settings->addCurrent ("cashier-acc-d-cash-resep-".$x['value'], "Akun Debit Cash Resep <strong>".$x['name']."</strong>", "", "chooser-mapping_akunting-debet_resep_cash_".$x['value']."-Debet", "",false,"","",true);
        $settings->addSuperCommandAction("debet_resep_cash_".$x['value'],"kode_akun");
        $settings->addSuperCommandArray("debet_resep_cash_".$x['value'],"cashier-acc-d-cash-resep-".$x['value'],"nomor",true);
        $settings->addCurrent ("cashier-acc-k-cash-resep-".$x['value'], "Akun Kredit Cash Resep <strong>".$x['name']."</strong>", "", "chooser-mapping_akunting-kredit_resep_cash_".$x['value']."-Kredit", "",false,"","",true);
        $settings->addSuperCommandAction("kredit_resep_cash_".$x['value'],"kode_akun");
        $settings->addSuperCommandArray("kredit_resep_cash_".$x['value'],"cashier-acc-k-cash-resep-".$x['value'],"nomor",true);
    }
    $settings   ->addSectionCurrent ("Kode Akun Resep tanpa Jenis Pasien");
    $settings->addCurrent ("cashier-acc-d-cash-resep-", "Akun Debit Cash Resep Non Pasien ", "", "chooser-mapping_akunting-debet_resep_cash_-Debet", "",false,"","",true);
    $settings->addSuperCommandAction("debet_resep_cash_","kode_akun");
    $settings->addSuperCommandArray("debet_resep_cash_","cashier-acc-d-cash-resep-","nomor",true); 
    $settings->addCurrent ("cashier-acc-k-cash-resep-", "Akun Kredit Cash Resep Non Pasien", "", "chooser-mapping_akunting-kredit_resep_cash_-Kredit", "",false,"","",true);
    $settings->addSuperCommandAction("kredit_resep_cash_","kode_akun");
    $settings->addSuperCommandArray("kredit_resep_cash_","cashier-acc-k-cash-resep-","nomor",true);
}

if($settings->isGroup("diskon", "Diskon","fa fa-tag")){
    require_once "smis-base/smis-include-service-consumer.php";
    $serv=new ServiceConsumer($db,"get_jenis_patient",NULL,"registration");
    $serv->execute();
    $list=$serv->getContent();
    foreach($list as $x){
        $settings->addCurrent (  "cashier-acc-d-diskon-".$x['value'], "Akun Debit Cash <strong>".$x['name']."</strong>", "", "chooser-mapping_akunting-debet_diskon_".$x['value']."-Debet", "",false,"","",true );
        $settings->addSuperCommandAction("debet_diskon_".$x['value'],"kode_akun");
        $settings->addSuperCommandArray("debet_diskon_".$x['value'],"cashier-acc-d-diskon-".$x['value'],"nomor",true);
        $settings->addCurrent (  "cashier-acc-k-diskon-".$x['value'], "Akun Kredit Cash <strong>".$x['name']."</strong>", "", "chooser-mapping_akunting-kredit_diskon_".$x['value']."-Kredit", "",false,"","",true );
        $settings->addSuperCommandAction("kredit_diskon_".$x['value'],"kode_akun");
        $settings->addSuperCommandArray("kredit_diskon_".$x['value'],"cashier-acc-k-diskon-".$x['value'],"nomor",true);
        $settings->addCurrent (  "cashier-acc-d-diskon-resep-".$x['value'], "Akun Debit Cash Resep <strong>".$x['name']."</strong>", "", "chooser-mapping_akunting-debet_resep_diskon_".$x['value']."-Debet", "",false,"","",true );
        $settings->addSuperCommandAction("debet_resep_diskon_".$x['value'],"kode_akun");
        $settings->addSuperCommandArray("debet_resep_diskon_".$x['value'],"cashier-acc-d-diskon-resep-".$x['value'],"nomor",true);
        $settings->addCurrent (  "cashier-acc-k-diskon-resep-".$x['value'], "Akun Kredit Cash Resep <strong>".$x['name']."</strong>", "", "chooser-mapping_akunting-kredit_resep_diskon_".$x['value']."-Kredit", "",false,"","",true );
        $settings->addSuperCommandAction("kredit_resep_diskon_".$x['value'],"kode_akun");
        $settings->addSuperCommandArray("kredit_resep_diskon_".$x['value'],"cashier-acc-k-diskon-resep-".$x['value'],"nomor",true);
    }
}

if($settings->isGroupAdded("kurang_lebih", "Kurang Lebih Bayar","fa fa-minus")){
    require_once "smis-base/smis-include-service-consumer.php";
    $serv=new ServiceConsumer($db,"get_jenis_patient",NULL,"registration");
    $serv->execute();
    $list=$serv->getContent();
    foreach($list as $x){
        $settings->addCurrent (  "cashier-acc-d-kurang-".$x['value'], "Akun Debit Kurang Bayar <strong>".$x['name']."</strong>", "", "chooser-mapping_akunting-debet_kurang_".$x['value']."-Debet", "",false,"","",true );
        $settings->addSuperCommandAction("debet_kurang_".$x['value'],"kode_akun");
        $settings->addSuperCommandArray("debet_kurang_".$x['value'],"cashier-acc-d-kurang-".$x['value'],"nomor",true);
        $settings->addCurrent (  "cashier-acc-k-kurang-".$x['value'], "Akun Kredit Kurang Bayar <strong>".$x['name']."</strong>", "", "chooser-mapping_akunting-kredit_kurang_".$x['value']."-Kredit", "",false,"","",true );
        $settings->addSuperCommandAction("kredit_kurang_".$x['value'],"kode_akun");
        $settings->addSuperCommandArray("kredit_kurang_".$x['value'],"cashier-acc-k-kurang-".$x['value'],"nomor",true);
    }
    $settings->addCurrent (  "cashier-k-resep-bebas", "Kredit Penjualan Resep Bebas (Khusus 1 Notif Resep Bebas) ", "", "chooser-mapping_akunting-penjualan_resep_bebas-Akun Penjualan Resep Bebas", "Kode Akun resep Bebas Akunting",false,"","",true );  
    $settings->addSuperCommandAction("penjualan_resep_bebas","kode_akun");
    $settings->addSuperCommandArray("penjualan_resep_bebas","cashier-k-resep-bebas","nomor",true);
        
    $settings->addCurrent (  "cashier-kurang-lebih-bayar-resep-bebas", "Kurang Lebih Bayar Resep Bebas (Khusus 1 Notif Resep Bebas) ", "", "chooser-mapping_akunting-akun_kurang_lebih_bayar_resep_bebas-Akun Kurnag Lebih Bayar Resep Bebas", "Kode Akun Kurang Lebih Bayar Resep Bebas Akunting",false,"","",true);
    $settings->addSuperCommandAction("akun_kurang_lebih_bayar_resep_bebas","kode_akun");
    $settings->addSuperCommandArray("akun_kurang_lebih_bayar_resep_bebas","cashier-kurang-lebih-bayar-resep-bebas","nomor",true);
}

if($settings->isGroupAdded("setup", "Setup Lain","fa fa-cogs")){
    $opsi=new OptionBuilder();
    $opsi->add("Tanggal Masuk ","tgl_masuk");
    $opsi->add("Tanggal Pulang ","tgl_pulang");
    $opsi->add("Tanggal Posting ","tgl_notif");
    $settings->addCurrent ( "cashier-acc-setup-tgl-model", "Model Tanggal Pilihan pada Cash Base Cashier", $opsi->getContent(), "select", "");  
    $settings->addCurrent ( "cashier-acc-enabled-cash", "Auto Push Notify Bank ke Akunting ", "1", "checkbox", "");  
    $settings->addCurrent ( "cashier-acc-enabled-bank", "Auto Push Notify Asuransi ke Akunting ", "1", "checkbox", "");  
    $settings->addCurrent ( "cashier-acc-enabled-asuransi", "Auto Push Notify Asuransi ke Akunting ", "1", "checkbox", "");  
    $settings->addCurrent ( "cashier-acc-enabled-diskon", "Auto Push Notify Diskon ke Akunting ", "1", "checkbox", "" );  
    $settings->addCurrent ( "cashier-acc-enabled-cash-resep", "Auto Push Notify Cash Resep ke Akunting ", "1", "checkbox", "" );  
    $settings->addCurrent ( "cashier-acc-enabled-bank-resep", "Auto Push Notify Bank Resep ke Akunting ", "1", "checkbox", "");  
    $settings->addCurrent ( "cashier-acc-enabled-asuransi-resep", "Auto Push Notify Asuransi Resep ke Akunting ", "", "checkbox", "");  
    $settings->addCurrent ( "cashier-acc-enabled-diskon-resep", "Auto Push Notify Diskon Resep ke Akunting ", "1", "checkbox", "");    
}

require_once "smis-base/smis-include-service-consumer.php";
$serv = new ServiceConsumer($db,"get_acc_account",NULL,"accounting");
$serv ->execute();
$list = $serv->getContent();
$akun = new OptionBuilder();
foreach($list as $x){
    $akun->add($x['nomor']." - ".$x['nama'],$x['nomor']);
}

$settings->setPartialLoad(true);
$response = $settings->init ();
?>