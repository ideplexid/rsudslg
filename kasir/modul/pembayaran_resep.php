<?php
if (isset ( $_POST ['super_command'] ) && $_POST ['super_command'] == "resep") {
	require_once ("smis-base/smis-include-service-consumer.php");
	require_once 'kasir/class/responder/ResepResponder.php';
	$ruang=$_POST['ruang'];
	$header=array ("ID","Tanggal","Pasien","Dokter","Jenis");
	$dktable = new Table ( $header);
	$dktable->setName ( "resep" );
	$dktable->setModel ( Table::$SELECT );
	$dkadapter = new SimpleAdapter ();
	$dkadapter->add ( "ID", "id", "digit8" );
	$dkadapter->add ( "Tanggal", "tanggal", "date d M Y" );
	$dkadapter->add ( "Pasien", "nama_pasien" );
	$dkadapter->add ( "Dokter", "nama_dokter" );
	$dkadapter->add ( "Jenis", "tipe" );
	$tarif = new ResepResponder ( $db, $dktable, $dkadapter, "browse_resep",$ruang );
	$super = new SuperCommand ();
	$super->addResponder ( "resep", $tarif );
	$init = $super->initialize ();
	if ($init != null) {
		echo $init;
		return;
	}
}
require_once 'smis-base/smis-include-service-consumer.php';
require_once 'smis-libs-class/ServiceProviderList.php';
$list=new ServiceProviderList($db, "browse_resep");
$list->execute();
$ruang=$list->getContent();

$id_resep = new Text ( "id_resep", "", "" );
$nama_pasien = new Text ( "nama_pasien", "Nama", "" );
$nrm_pasien = new Text ( "nrm_pasien", "NRM", "" );
$noreg_pasien = new Text ( "noreg_pasien", "No. Reg", "" );
$dokter = new Text ( "dokter", "Dokter", "" );
$jenis = new Text ( "jenis", "Jenis", "" );
$carabayar = new Text ( "carabayar", "Carabayar", "" );
$ruangan= new Select("ruang", "Ruangan", $ruang);

$browse = new Button ( "", "", "" );
$browse->setIsButton ( Button::$ICONIC );
$browse->setIcon ( " fa fa-list-alt" );
$browse->setClass("btn-primary");
$browse->setAction ( "pembayaran_resep.chooser('noresep','pembayaran_resep','resep',resep)" );

$reload = new Button ( "", "", "" );
$reload->setIsButton ( Button::$ICONIC );
$reload->setIcon ( " fa fa-refresh" );
$reload->setClass("btn-primary");
$reload->setAction ( "reload_resep()" );

$bgroup = new ButtonGroup ( "" );
$bgroup->addButton ( $browse );
$bgroup->addElement ( $reload );

$id_resep->setDisabled ( true );
$nama_pasien->setDisabled ( true );
$nrm_pasien->setDisabled ( true );
$noreg_pasien->setDisabled ( true );
$dokter->setDisabled ( true );
$jenis->setDisabled ( true );
$carabayar->setDisabled ( true );

$form = new Form ( "", "", "Pembayaran Resep" );
$form->addElement ( "Ruangan", $ruangan);
$form->addElement ( "ID Resep", $id_resep );
$form->addElement ( "Dokter", $dokter );
$form->addElement ( "Jenis", $jenis );
$form->addElement ( "Nama", $nama_pasien );
$form->addElement ( "NRM", $nrm_pasien );
$form->addElement ( "No. Reg", $noreg_pasien );
$form->addElement ( "Carabayar", $carabayar );
$form->addElement ( "", $bgroup );

echo $form->getHtml ();
echo "<div class='clear'></div></br>";
echo "<div id='pembayaran_resep_content'></div>";
echo "<div id='kwitansi_resep_holder' class='hide'></div>";
echo addJS ( "framework/bootstrap/js/bootstrap-datepicker.js" );
echo addCSS ( "framework/bootstrap/css/datepicker.css" );
echo addJS ( "framework/smis/js/table_action.js" );
echo addJS ( "kasir/resource/js/pembayaran_resep.js",false );
?>
<style type="text/css">
<?php echo getSettings($db,"cashier-resep-kwitansi-css","");?>
</style>