<?php

/**
 * masih belum jadi nunggu buatkan service */

global $db;
require_once "smis-base/smis-include-service-consumer.php";
require_once 'smis-libs-class/MasterTemplate.php';
require_once 'smis-libs-class/MasterServiceTemplate.php';
require_once 'smis-libs-class/MasterSlaveServiceTemplate.php';
require_once 'kasir/class/template/KlaimPasienTemplate.php';

$print 	= new Button("","","Cetak");
$print	->addClass("btn-primary")
		->setIsButton(Button::$ICONIC)
		->setIcon("fa fa-print");
$init	= new Button("","","Crawler Ulang");
$init	->addClass("btn-primary")
		->setIsButton(Button::$ICONIC)
		->setIcon("fa fa-refresh");
$detail	= new Button("","","Detail Tagihan");
$detail	->addClass("btn-primary")
		->setIsButton(Button::$ICONIC)
		->setIcon("fa fa-list-alt");
$push	= new Button("","","Push Accounting");
$push	->addClass("btn-primary")
		->setIsButton(Button::$ICONIC)
		->setIcon("fa fa-upload")
		->setAction("klaim_pasien.init_all_post()");
$unpush	= new Button("","","Un Push Accounting");
$unpush ->addClass("btn-primary")
		->setIsButton(Button::$ICONIC)
		->setIcon("fa fa-download")
		->setAction("klaim_pasien.init_all_unpost()");
$koreksi_carabayar  = new Button("","","Koreksi Cara Bayar");
$koreksi_carabayar  ->addClass("btn-primary")
					->setIsButton(Button::$ICONIC)
					->setIcon("fa fa-backward");

$header = array ("No.","No Reg","NRM",'Nama',"Masuk","Pulang","Jenis","Status","Layanan","Kamar","Cara Bayar","Asuransi",'No. BPJS',"Plafon","Tagihan","Push");
$plafon = new KlaimPasienTemplate($db, "get_registered_all", "kasir", "klaim_pasien");
$plafon ->setEntity("registration");
$plafon ->setDateEnable(true);
if( isset($_POST['command']) && $_POST['command']=="list" ){	
	$plafon ->getServiceResponder()
            ->addData("noreg_pasien",$_POST['noreg_pasien']);	
	$plafon ->getServiceResponder()
            ->addData("nrm_pasien",$_POST['nrm_pasien']);	
	$plafon ->getServiceResponder()
            ->addData("jenis_pasien",$_POST['jenis_pasien']);
    if(isset($_POST['dari_pulang']) && $_POST['dari_pulang']!="" )                        
        $plafon->getServiceResponder()->addData("dari_pulang",$_POST['dari_pulang']);
    if(isset($_POST['sampai_pulang']) && $_POST['sampai_pulang']!="" )                        
        $plafon->getServiceResponder()->addData("sampai_pulang",$_POST['sampai_pulang']);
    if(isset($_POST['dari_masuk']) && $_POST['dari_masuk']!="" )                        
        $plafon->getServiceResponder()->addData("dari",$_POST['dari_masuk']);
    if(isset($_POST['sampai_masuk']) && $_POST['sampai_masuk']!="" )                        
        $plafon->getServiceResponder()->addData("sampai",$_POST['sampai_masuk']);
}
$plafon ->addResouce("js","kasir/resource/js/klaim_pasien.js");
$plafon ->addResouce("js","base-js/smis-base-loading.js","before",true);

$adapt=new SummaryAdapter();
$adapt	->setUseNumber(true, "No.","back.")
		->add("No Reg", "id","only-digit9")
		->add("NRM", "nrm","only-digit8")
		->add("Nama", "nama_pasien")
		->add("No. BPJS","nobpjs")
		->add("Cara Bayar","carabayar","unslug")
		->add("Layanan","jenislayanan","unslug")
		->add("Plafon","plafon_bpjs","money Rp.")
		->add("Masuk", "tanggal","date d M Y H:i")
		->add("Pulang", "tanggal_pulang","date d M Y H:i")
		->add("Kamar", "kamar_inap","unslug")
		->add("Jenis", "uri","trivial_0_URJ_URI")
		->add("Asuransi", "nama_asuransi")
		->add("Status", "selesai","trivial_0_Berlangsung_Selesai")
		->add("Tagihan", "total_tagihan","money Rp.")
        ->add("Push", "akunting_notify_date","date d M Y H:i");
$adapt  ->addSummary("Tagihan","total_tagihan","money Rp.");
$adapt  ->addFixValue("Tanggal","<strong>Total</strong>");
$plafon ->setAdapter($adapt);
$plafon ->getUItable()
		->setMaxContentButton(2,"Action")
        ->addContentButton("load_detail",$detail)
        ->addContentButton("update_push_accounting",$push)
		->addContentButton("update_unpush_accounting",$unpush)
		->addContentButton("print_tagihan_pasien",$print)
		->addContentButton("init_tagihan",$init)
		->addContentButton("init_koreksi_carabayar",$koreksi_carabayar)
		->setReloadButtonEnable(false)
		->setDelButtonEnable(false)
		->setAddButtonEnable(false)
		->setEditButtonEnable(false)
        ->addHeaderButton($push)
		->setHeader($header);

if(!isset($_POST['command'])){
	
	$service = new ServiceConsumer($db,"get_format_kasir",NULL,"kasir");
	$service->execute();
	$mode_kwitansi=$service->getContent();

	$service = new ServiceConsumer($db,"get_jenis_patient",NULL,"registration");
	$service->execute();
	$jenis_patient	 = $service->getContent();
	$jenis_patient[] = array("name"=>"","value"=>"","default"=>"1");
	
	$plafon ->getUItable()
            ->addModal("dari_masuk", "date", "Dari Masuk", "","n")
			->addModal("sampai_masuk", "date", "Sampai Masuk", "","n")
			->addModal("dari_pulang", "date", "Dari Pulang", "","n")
			->addModal("sampai_pulang", "date", "Sampai Pulang", "","n")
			->addModal("nrm_pasien", "text", "NRM", "","y")
			->addModal("noreg_pasien", "text", "No. Reg", "","y")
			->addModal("jenis_pasien", "select", "Cara Bayar", $jenis_patient,"y")
			->addModal("kwitansi", "select", "Kwitansi", $mode_kwitansi,"y");

	$view = new Button("","","Tampilkan");
	$view ->addClass("btn-primary")
		  ->setIcon("fa fa-search")
		  ->setAction("klaim_pasien.view()")
		  ->setIsButton(Button::$ICONIC_TEXT);

	$all_init = new Button("","","Crawler Data tagihan");
	$all_init ->addClass("btn-primary")
			  ->setIsButton(Button::$ICONIC_TEXT)
			  ->setIcon("fa fa-forward")
			  ->setAction("klaim_pasien.init_all_tagihan_pasien()");

	$all_print = new Button("","","Print Data Tagihan");
	$all_print ->addClass("btn-primary")
			   ->setIsButton(Button::$ICONIC_TEXT)
			   ->setIcon("fa fa-print")
			   ->setAction("klaim_pasien.init_all_print()");
	
	$all_koreksi = new Button("","","Koreksi Data Tagihan");
	$all_koreksi ->addClass("btn-primary")
				 ->setIsButton(Button::$ICONIC_TEXT)
				 ->setIcon("fa fa-backward")
				 ->setAction("klaim_pasien.init_all_koreksi_carabayar()");
	$btgrup = new ButtonGroup("","","");
	$btgrup ->addButton($all_init)
			->addButton($all_print)
			->addButton($all_koreksi)
			->addButton($push)
			->addButton($unpush)
			->setMax(2,"Action");
	$service = new ServiceConsumer($db,"get_jenis_patient",NULL,"registration");
	$service ->execute();
	$jenis_patient	 = $service->getContent();
	$jenis_patient[] = array("name"=>"","value"=>"","default"=>"1");
	
	$plafon	->getForm()
			->addElement("",$view)
			->addElement("",$btgrup);
	$plafon ->getUItable()
			->clearContent();
	$plafon	->addJSColumn("nama_pasien")
			->addJSColumn("nama_dokter")
			->addJSColumn("id")
			->addJSColumn("nrm")
			->addJSColumn("nobpjs")
			->addJSColumn("plafon_bpjs")
			->addJSColumn("diagnosa_bpjs")
			->addJSColumn("tindakan_bpjs")
			->addJSColumn("keterangan_bpjs")
			->addJSColumn("kunci_bpjs");
	$plafon ->getModal()
			->setTitle("Plafon");
	$plafon	->addViewData("dari_pulang","dari_pulang")
			->addViewData("sampai_pulang","sampai_pulang")
			->addViewData("dari_masuk","dari_masuk")
			->addViewData("sampai_masuk","sampai_masuk")
			->addViewData("noreg_pasien","noreg_pasien")
			->addViewData("nrm_pasien","nrm_pasien")
			->addViewData("jenis_pasien","jenis_pasien");
	
	$close	= new Button("", "", "Batal");
	$close	->addClass("btn-primary")
			->setIsButton(Button::$ICONIC_TEXT)
			->setIcon("fa fa-close")
			->setAction("location.reload()");
	$person = new LoadingBar("rtb_person_bar", "");
	$modal	= new Modal("rtb_load_modal", "", "Process in Progress");
	$modal	->addHTML($person->getHtml(),"after")
			->addFooter($close);
	echo "<div id='tagihan_place_init'></div>";
	echo $modal->getHtml();
    
    $back	= new Button("", "", "");
	$back	->setIsButton(Button::$ICONIC)
			->setIcon("fa fa-toggle-up")
			->setClass("btn-primary")
			->setAction("klaim_pasien.reup()");
    
    $hide	= new Hidden("autoload_claim_pasien","",getSettings($db,"cashier-klaim-pasien-autoload","0"));

    echo "<div id='sub_menu_klaim_pasien' class='hide'>";
        echo $back->getHtml();
        echo "<div id='sub_menu_klaim_pasien_content'>";
        echo "</div>";	
    echo "</div>";
    echo $hide->getHtml();
}

$plafon->initialize();
?>

