<?php
require_once 'smis-framework/smis/api/SettingsBuilder.php';
require_once 'smis-base/smis-include-service-consumer.php';
require_once "smis-libs-class/ServiceProviderList.php";
global $db;
$smis = new SettingsBuilder( $db, "reg_settings", "kasir", "settings","Settings Kasir" );
$smis ->setShowDescription ( true );
$smis ->setShowHelpButton(true);
if($smis->isGroupAdded("pembayaran_non_pasien", "Pembayaran Non Pasien","fa fa-user")){   
    $smis->addCurrent ( "cashier-non-pasien-enabled-delete", "Enabled The Delete", "1", "checkbox", "Enable Delete di Pembayaran Non Pasien" );    
}

if($smis->isGroupAdded("pembayaran_resep", "Pembayaran Resep","fa fa-file")){
    $smis   ->addCurrent("cashier-resep-show-last-id","Menampilkan ID Terakhir di Kwitansi", "0", "checkbox", "jika di centang akan tampil ID terakhir sebagai nomor kwitansi pada pembayaran resep, Gunakan #table_print_kwitansi ")
            ->addCurrent("cashier-resep-kwitansi-css","CSS Kwitansi", "", "textarea", "CSS dari kwitansi Resep")
            ->addCurrent("cashier-resep-activate-notify-lunas","Aktifkan Notifikasi Lunas", "0", "checkbox", "Obat Penjualan Bebas yang telah lunas, akan di notifikasi balik ke kasir");
}

if($smis->isGroupAdded("pembayaran_pasien", "Pembayaran Pasien","fa fa-money")){    
    $me = new OptionBuilder();
    $me ->add("Crawler per Item","1","1")
        ->add("Crawler per Item Per Summary","2","1")
        ->add("Crawler Summary Item","0","0");

    $orient = new OptionBuilder();
    $orient ->add("Potrait",Tabulator::$POTRAIT,1)
            ->add("Landscape Left",Tabulator::$LANDSCAPE_LEFT)
            ->add("Landscape Right",Tabulator::$LANDSCAPE_RIGHT);

    $service = new ServiceProviderList($db,"edit_ok");
    $service ->execute();
    $ruangan = $service->getContent();

    $crawler = new OptionBuilder();
    $crawler ->add("Crawler Tanpa Real Time","0","1");
    $crawler ->add("Real Time Dengan Crawler","1","0");
    $crawler ->add("Real Time Tanpa Crawler","2","0");
    

    $smis   ->addSectionCurrent("Behaviour & User Interface")
            ->addCurrent("cashier-autocomplete-nrm", "Buat NRM Supaya Autocomplete", "0", "checkbox", "Memilih Pasien dengan Autocomplete NRM" )
            ->addCurrent("cashier-autocomplete-noreg", "Buat No. Register Supaya Autocomplete", "0", "checkbox", "Memilih Pasien dengan Autocomplete No. Register" )
            ->addCurrent("cashier-autocomplete-profile", "Menampilkan Pencarian No. Profile", "0", "checkbox", "Memilih Pasien dengan Autocomplete No. Profile" )
            ->addCurrent("cashier-activate-newmode-orientation", "Mengaktifkan Orientasi Mode Baru", $orient->getContent(), "select", "Bentuk Map apakah Potrait, Landscape Right dan Landscape Left" )
            //->addCurrent("cashier-autoload", "Autoload Pasien Cashier", "1", "checkbox", "Autoload pasien di Kasir" )
            ->addCurrent("cashier-out-check-uri", "Kasir Memulangkan Tanpa Nge-Check Rawat Inap", "0", "checkbox", "Kasir Memulangkan Tanpa Nge-check status Rawat Inap" )
            ->addCurrent("cashier-out-check-urj", "Kasir Memulangkan Tanpa Nge-Check Rawat Jalan", "0", "checkbox", "Kasir Memulangkan Tanpa Nge-check status Rawat Jalan" )
            ->addCurrent("cashier-activate-out-force-mode", "Mengaktifkan Fitur Keluar Ruangan Sekaligus", "0", "checkbox", "Jika Fitur ini diaktifkan maka ketika keluar dari kasir, pasien tersebut akan otomatis keluar dari semua ruangan, tidak peduli data yang ada di ruangan. pastikan dua fitur diatas tecentang untuk mengaktifkan fitur ini (Kasir Memulangkan Tanpa Nge-Check Rawat Jalan, Kasir Memulangkan Tanpa Nge-Check Rawat Inap) " )
            ->addCurrent("cashier-out-check-manual", "Kasir Membebaskan Biaya Secara Manual", "0", "checkbox", "Pengecekan Tidak Bayar Secara Manual" )
            ->addCurrent("cashier-activate-crawler-cashier", "Metode Crawler Cashier",$me->getContent(), "select", "Mengubah Metode Crawler Cashier untuk service get_tagihan kasir.",false,"","crawler_mode")
            ->addCurrent("cashier-enabled-diskon-invoker", "Mengaktifkan Pemanggilan Diskon","0", "checkbox", "Mengaktifkan Pemanggilan Pengajuan Diskon")
            ->addCurrent("cashier-activate-document", "Menampilkan Dokumen Pasien", "1", "checkbox", "Menampilkan Dokumen Pasien yang telah di Upload dari pendaftaran sehingga bisa dicetak jika dibutuhkan seperti scan kartu BPJS, Scan Rujukan dll" )
            ->addCurrent("cashier-df-out-button","Nama Default Tombol Pulang", "Pulang", "text", "Default Nama Tombol Pulang")
            ->addCurrent("cashier-show-back-action","Tampilkan Tombol Pengembalian Pasien di Kasir", "1", "checkbox", "Jika di centang maka tombol pengembalian pasien di kasir akan muncul")
            ->addCurrent("cashier-real-time-tagihan", "Mengaktifkan Fitur Realtime Tagihan Kasir", $crawler ->getContent(), "select", "1. Opsi Crawler Tanpa Real Time Tagihan  \n 2. Realtime Dengan Crawler  \n 3. Realtime Tanpa Crawler " )
            ->addSectionCurrent("Payment Methode")
            ->addCurrent("cashier-activate-assurant", "Menampilkan Pembayaran Secara Asuransi", "1", "checkbox", "Menampilkan Pembayaran Secara Asuransi" )
            ->addCurrent("cashier-activate-bank", "Menampilkan Pembayaran Secara Bank", "1", "checkbox", "Menampilkan Pembayaran Bank" )
            ->addCurrent("cashier-activate-cash", "Menampilkan Pembayaran Secara Cash", "1", "checkbox", "Menampilkan Pembayaran Cash" )
            ->addCurrent("cashier-activate-discount", "Menampilkan Pembayaran Secara Diskon", "1", "checkbox", "Menampilkan Pembayaran Diskon" )
            ->addCurrent("cashier-print-out-times", "Berapa Kali Cetak Nota", "1", "text", "Minimal 1" )            
            ->addCurrent("cashier-hide-del-asuransi", "Hilangkan Delete dalam Asuran","0", "checkbox", "Tombol delete akan dihilangkan")
            ->addCurrent("cashier-hide-del-bank", "Hilangkan Delete dalam Bank","0", "checkbox", "Tombol delete akan dihilangkan")
            ->addCurrent("cashier-hide-del-cash", "Hilangkan Delete dalam Cash","0", "checkbox", "Tombol delete akan dihilangkan")
            ->addCurrent("cashier-webprint-asuransi","Mengaktifkan Web Print untuk Asuransi", "0", "checkbox", "jika diaktifkan maka cetak kwitansi untuk per bank akan menggunakan webprint")
            ->addCurrent("cashier-webprint-bank","Mengaktifkan Web Print untuk Bank", "0", "checkbox", "jika diaktifkan maka cetak kwitansi untuk per bank akan menggunakan webprint")
            ->addCurrent("cashier-webprint-cash","Mengaktifkan Web Print untuk Cash", "0", "checkbox", "jika diaktifkan maka cetak kwitansi untuk per cash akan menggunakan webprint")
            ->addCurrent("cashier-webprint-default-logo","Default Logo untuk Cetak Web Print", "", "file-single-image", "jika berisi sebuah data maka akan memakai logo yang di upload selain itu, memakai logo default")
            ->addSectionCurrent("Tagihan Tambahan")
            ->addCurrent("cashier-tagihan-backup", "Tampilkan Tagihan Tambahan", "0", "checkbox", "Menampilkan Tagihan Tambahan di Kasir" )
            ->addCurrent("cashier-tambahan-biaya-enabled-tagihan","Enabled Input Tagihan", "1", "checkbox", "jika dicentang maka input tagihan tambahan bisa diketik sendiri")
            ->addCurrent("cashier-tambahan-biaya-enabled-jenis","Enabled Jenis Tagihan", "1", "checkbox", "jika dicentang maka jenis tagihan tambahan bisa dipilih sendiri")
            ->addSectionCurrent("Editor Tagihan Pasien")
            ->addCurrent("cashier-activate-input-periksa", "Mengizinkan Input Periksa Dokter di Kasir", "0", "checkbox", "Dengan Mengaktifkan Menu ini maka akan Muncul Input Periksa Dokter di Pembayaran Pasien" )
            ->addCurrent("cashier-activate-input-edit-ok", "Mengizinkan Edit data Operasi di Kasir", "0", "checkbox", "Dengan Mengaktifkan Menu ini maka akan Muncul Edit Operasi di Pembayaran Pasien" )
            ->addCurrent("cashier-default-kamar-operasi", "Default Kamar Operasi",$ruangan, "select", "Default Kamar Operasi")
            ->addCurrent("cashier-activate-input-bed", "Menampilkan Bed", "0", "checkbox", "Menampilkan Bed" )
            ->addCurrent("cashier-activate-input-visite", "Mengizinkan Input Visit Dokter di Kasir", "0", "checkbox", "dengan mengaktifkan modul ini maka visit dokter dapat di inputkan dari kasir" )
            ->addCurrent("cashier-activate-input-konsul", "Mengizinkan Input Konsul Dokter di Kasir", "0", "checkbox", "dengan mengaktifkan modul ini maka konsul dokter dapat di inputkan dari kasir" )
            ->addCurrent("cashier-activate-input-tindakan-igd", "Mengizinkan Input Tindakan IGD di Kasir", "0", "checkbox", "dengan mengaktifkan modul ini maka tindakan igd dapat di inputkan dari kasir" )
            ->addCurrent("cashier-activate-input-tindakan-perawat", "Mengizinkan Input Tindakan Perawat di Kasir", "0", "checkbox", "dengan mengaktifkan modul ini maka tindakan perawat dapat di inputkan dari kasir" )
            ->addCurrent("cashier-activate-input-tindakan-dokter", "Mengizinkan Input Tindakan Dokter di Kasir", "0", "checkbox", "dengan mengaktifkan modul ini maka tindakan dokter dapat di inputkan dari kasir" )
            ->addCurrent("cashier-activate-input-oksigen-central", "Mengizinkan Input Oksigen Central di Kasir", "0", "checkbox", "dengan mengaktifkan modul ini maka Oksigen Central  dapat di inputkan dari kasir" )
            ->addCurrent("cashier-activate-input-oksigen-manual", "Mengizinkan Input Oksigen Manual di Kasir", "0", "checkbox", "dengan mengaktifkan modul ini maka Oksigen Manual  dapat di inputkan dari kasir" )
            ->addCurrent("cashier-activate-input-edit-bed", "Mengizinkan Input Bed di Kasir", "0", "checkbox", "dengan mengaktifkan modul ini maka Bed dapat di inputkan dari kasir" )
            ->addCurrent("cashier-activate-input-edit-alok", "Mengizinkan Input Alat Obat Kesehatan di Kasir", "0", "checkbox", "dengan mengaktifkan modul ini maka Alat Obat Kesehatan dapat di inputkan dari kasir" )
            ->addCurrent("cashier-activate-input-edit-alok-connected", "Mengizinkan Input Alat Obat Kesehatan yang Terkoneksi dengan Stok di Kasir", "0", "checkbox", "dengan mengaktifkan modul ini maka Alat Obat Kesehatan yang terkoneksi dengan stok dapat di inputkan dari kasir" )
            ->addCurrent("cashier-activate-input-edit-laboratory", "Mengizinkan Input Laboratory", "0", "checkbox", "dengan mengaktifkan modul ini maka Laboratory yang terkoneksi dengan stok dapat di inputkan dari kasir" )
            ->addCurrent("cashier-activate-input-edit-radiology", "Mengizinkan Input Radiology", "0", "checkbox", "dengan mengaktifkan modul ini maka Radiology yang terkoneksi dengan stok dapat di inputkan dari kasir" )
            ->addCurrent("cashier-activate-input-edit-elektromedis", "Mengizinkan Input Elektromedis", "0", "checkbox", "dengan mengaktifkan modul ini maka Elektromedis yang terkoneksi dengan stok dapat di inputkan dari kasir" )
            ->addCurrent("cashier-activate-input-edit-mcu", "Mengizinkan Input Medical Checkup", "0", "checkbox", "dengan mengaktifkan modul ini maka Medical Checkup yang terkoneksi dengan stok dapat di inputkan dari kasir" )
            ->addSectionCurrent("Kwitansi Real")
            ->addCurrent("cashier-activate-detail-kwitansi", "Tampilkan Kwitansi Detail", "0", "checkbox", "Menampilkan Kwitansi yang Detail" )
            ->addCurrent("cashier-activate-simple-kwitansi-bpjs", "Tampilkan Simple Kwitansi BPJS",0, "checkbox", "Menampilkan Simple Kwitansi  BPJS")
            ->addCurrent("cashier-activate-simple-kwitansi", "Tampilkan Simple Kwitansi",0, "checkbox", "Menampilkan Simple Kwitansi. Tapi Harus Harus Mode Baru")
            ->addCurrent("cashier-activate-simple-kwitansi-asuransi", "Tampilkan Simple Kwitansi Asuransi",0, "checkbox", "Menampilkan Simple Kwitansi Asuransi, perbedaanya adalah totalnya yang jadi pembilang. Tapi Harus Harus Mode Baru")
            ->addCurrent("cashier-activate-simple-kwitansi-grup", "Tampilkan Simple Kwitansi yg berdasarkan grup tindakan",0, "checkbox", "Menampilkan Simple Kwitansi berdasarkan jenis group")
            ->addCurrent("cashier-activate-simple-kwitansi-grup-alok", "Tampilkan Simple Kwitansi Grup yang Khusus Alok dijadikan satu ,yg berdasarkan grup tindakan",0, "checkbox", "Menampilkan Simple Kwitansi berdasarkan jenis group, kecuali alok jadi satu")
            ->addCurrent("cashier-activate-simple-kwitansi-grup-harian", "Tampilkan Simple Kwitansi Grup Harian",0, "checkbox", "Menampilkan Kwitansi Grup Harian di bagian Kasir")
            ->addCurrent("cashier-activate-simple-kwitansi-grup-dokter", "Tampilkan Simple Kwitansi Grup yang Khusus Dokter Dipisah Detail",0, "checkbox", "Menampilkan Kwitansi Khusus Dokter yang tidak digabung")
            ->addCurrent("cashier-activate-simple-kwitansi-bank-jatim", "Simple Kwitansi Bank",0, "checkbox", "Simple Kwitansi Bank Jatim")
            ->addCurrent("cashier-activate-simple-kwitansi-rincian", "Kwitansi Rincian",0, "checkbox", "Simple Kwitansi Perincian")
            ->addCurrent("cashier-activate-kwitansi-paket", "Menampilkan Kwitansi Paket",0, "checkbox", "Menampilkan Kwitansi Paket")
            ->addCurrent("cashier-activate-simple-kwitansi-escp", "Tampilkan Simple Kwitansi ESCP",0, "checkbox", "Menampilkan Simple Kwitansi - ESCP. Tapi Harus Harus Mode Baru")
            ->addCurrent("cashier-activate-inacbgs", "Format INA-CBG's",0, "checkbox", "Kwitansi INA-CBG's")
            ->addCurrent("cashier-activate-inacbgs-irja", "Format INA-CBG's IRJA",0, "checkbox", "Menampilkan Format Laporan INA-CBG's IRJA")
            ->addCurrent("cashier-activate-kwitansi-rumus", "Tampilkan Format Kwitansi Rumus",0, "checkbox", "Menampilkan Kwitansi Rumus Pelayanan")
            ->addCurrent("cashier-activate-kwitansi-rumus-detail", "Tampilkan Format Kwitansi Rumus Detail",0, "checkbox", "Menampilkan Kwitansi Rumus Pelayanan yang lebih detail menyertakan data tagihan dokter")
            ->addSectionCurrent("Kwitansi Bayangan")
            ->addCurrent("cashier-activate-shadow", "Menampilkan Tagihan Bayangan", "0", "checkbox", "Menampilkan Tagihan Bayangan" )
            ->addCurrent("cashier-activate-simple-kwitansi-bayangan", "Tampilkan Simple Kwitansi Bayangan",0, "checkbox", "Menampilkan Simple Kwitansi Bayangan, Harus Mode Baru")
            ->addCurrent("cashier-activate-simple-kwitansi-bayangan-group", "Tampilkan Simple Kwitansi Bayangan Group",0, "checkbox", "Ditampilkan Berdasarkan Groupnya")
            ->addCurrent("cashier-activate-bayangan-show-detail-bayar", "Tampilkan Detail Bayar Pada Kwitansi Bayangan",0, "checkbox", "Jika di centang maka kwitansi bayangan akan menampilkan detail pembayaran pasien");
}

if($smis->isGroupAdded("actived_area", "Ruangan Aktif","fa fa-university")){
    $smis     ->addCurrent ( "cashier-selective-area", "Gunakan Seleksi Area dikunjungi",0, "checkbox", "jika diaktifkan hanya area yang dikunjungi pasien yang akan masuk dalam seleksi")
              ->addCurrent ( "cashier-actived-area", "Gunakan Seleksi Area Aktif",0, "checkbox", "Gunakan Area Aktif dengan Setting Sebagai Berikut ini");
    $lis_get_tagihan = new ServiceProviderList($db, "get_tagihan");
    $lis_get_tagihan ->execute();
    $content         = $lis_get_tagihan->getContent();
    foreach($content as $x){
        $smis ->addSectionCurrent($x['name'])
              ->addCurrent ("cashier-actived-area-".$x['value'], "Aktifkan Crawler Ruangan ".$x['name'],1, "checkbox", "Ruangan ".$x['name']." Aktif dalam Crawler Kasir")
              ->addCurrent("cashier-map-area-".$x['value'], "Rename Nama Ruangan ".$x['name'],$x['name'], "text", "Map Nama Ruangan ".$x['name']);
    }
}

if($smis->isGroupAdded("markup"," Markup Tagihan","fa fa-adjust")){
    $smis   ->addCurrent("cashier-activate-jaspel", "Mengaktifkan Penggunaan Markup Tagihan", "1", "checkbox", "Mengaktifkan Penggunaan Jaspel pada System" )
            ->addCurrent("cashier-jaspel-persen", "Persentase Markup Tagihan", "10", "text", "Persentase dari Jasa Pelayanan" )
            ->addSectionCurrent("Komponen Layanan yang di Markup")
            ->addCurrent("cashier-jaspel-radiology", "Aktifkan Markup Untuk  Radiology", "1", "checkbox", "Layanan Radiology TerKena Markup" )
            ->addCurrent("cashier-jaspel-fisiotherapy", "Aktifkan Markup Untuk  Fisiotherapy", "1", "checkbox", "Layanan Fisiotherapy Kena Markup" )
            ->addCurrent("cashier-jaspel-laboratory", "Aktifkan Markup Untuk  Laboratory", "1", "checkbox", "Layanan Laboratory Kena Markup" )
            ->addCurrent("cashier-jaspel-ambulan", "Aktifkan Markup Untuk  Ambulance", "0", "checkbox", "Layanan Ambulance Kena Markup" )
            ->addCurrent("cashier-jaspel-gizi", "Aktifkan Markup Untuk  Asuhan Gizi", "1", "checkbox", "Layanan Asuhan Gizi Kena Markup" )
            ->addCurrent("cashier-jaspel-tindakan_perawat", "Aktifkan Markup Untuk  Tindakan Perawat", "1", "checkbox", "Layanan Tindakan Perawat Kena Markup" )
            ->addCurrent("cashier-jaspel-tindakan_perawat_igd", "Aktifkan Markup Untuk  Tindakan Perawat IGD", "1", "checkbox", "Layanan Tindakan Perawat IGD Kena Markup" )
            ->addCurrent("cashier-jaspel-tindakan_dokter", "Aktifkan Markup Untuk  Tindakan Dokter", "0", "checkbox", "Layanan Tindakan Dokter Kena Markup" )
            ->addCurrent("cashier-jaspel-konsul", "Aktifkan Markup Untuk  Konsul", "0", "checkbox", "Layanan Konsul Dokter Kena Markup" )
            ->addCurrent("cashier-jaspel-visite", "Aktifkan Markup Untuk  Visite", "0", "checkbox", "Layanan Visite  Dokter Kena Markup" )
            ->addCurrent("cashier-jaspel-konsultasi_dokter", "Aktifkan Markup Untuk  Periksa", "0", "checkbox", "Layanan Periksa Dokter Kena Markup" )
            ->addCurrent("cashier-jaspel-ok", "Aktifkan Markup Untuk  Operasi", "0", "checkbox", "Layanan Operasi Kena Markup" )
            ->addCurrent("cashier-jaspel-rr", "Aktifkan Markup Untuk  Recovery Room", "0", "checkbox", "Layanan Recovery Room Kena Markup" )
            ->addCurrent("cashier-jaspel-vk", "Aktifkan Markup Untuk  VK", "1", "checkbox", "Layanan VK Kena Markup" )
            ->addCurrent("cashier-jaspel-alok", "Aktifkan Markup Untuk  Alat Obat Kesehatan", "1", "checkbox", "Layanan Alat Obat KEsehatan Ruang Kena Markup" )
            ->addCurrent("cashier-jaspel-penjualan_resep", "Aktifkan Markup Untuk  Penjualan Resep", "0", "checkbox", "Layanan Penjualan Resep Kena Markup" )
            ->addCurrent("cashier-jaspel-return_resep", "Aktifkan Markup Untuk  Return Resep", "0", "checkbox", "Layanan Return Resep Memotong Jaspel" )
            ->addCurrent("cashier-jaspel-bed", "Aktifkan Markup Untuk  Bed", "1", "checkbox", "Layanan Audiometry Kena Markup" )
            ->addCurrent("cashier-jaspel-oksigen_central", "Aktifkan Markup Untuk  Oksigen Central", "0", "checkbox", "Layanan Oksigen Central Kena Markup" )
            ->addCurrent("cashier-jaspel-oksigen_manual", "Aktifkan Markup Untuk  Oksigen Manual", "0", "checkbox", "Layanan Oksigen Manual Kena Markup" )
            ->addCurrent("cashier-jaspel-darah", "Aktifkan Markup Untuk  Darah", "1", "checkbox", "Layanan Darah PMI Kena Markup" )
            ->addCurrent("cashier-jaspel-hemodialisa", "Jaspel Hemodialisa", "1", "checkbox", "Hemodialisa Kena Markup" );
    
    $smis    ->addSectionCurrent("Ruangan yang di Markup");
    $urjip   = new ServiceConsumer($db, "get_urjip",array());
    $urjip   ->setMode(ServiceConsumer::$MULTIPLE_MODE)
             ->setCached(true,"get_urjip")
             ->execute();
    $content = $urjip->getContent();
    $ruangan = array();
    foreach ($content as $autonomous=>$ruang){
        $def="0";
        foreach($ruang as $nama_ruang=>$jip){
            $def  = ($jip[$nama_ruang]=="URI" || $jip[$nama_ruang]=="UP")?"1":"0";
            $smis ->addCurrent ( "cashier-jaspel-ruang-".$nama_ruang, "Aktifkan Markup Untuk  ".ArrayAdapter::format("unslug", $nama_ruang), $def, "checkbox", "Ruang ".ArrayAdapter::format("unslug", $nama_ruang)." Kena Markup Harga" );
        }
    }
}


if($smis->isGroupAdded("kwitansi_detail", "Kwitansi Detail","fa fa-file-text-o")){
    loadLibrary("smis-libs-function-medical");
    $replace = medical_service();
    $order   = new OptionBuilder();
    $total   = count($replace);
    for($i=0;$i<$total;$i++){
        $order->addSingle($i);
    }    

    $jaspel = new OptionBuilder();
    $jaspel ->add("Side")
            ->add("Bottom");

    $model  = new OptionBuilder();
    $model  ->add("Fetree Mode")
            ->add("Simple Mode");
    
    $keterangan = new OptionBuilder();
    $keterangan ->add("Hide Mode")
                ->add("Show Mode");
    $smis   ->addSectionCurrent("Tampilan Kwitansi Detail")
            ->addCurrent("cashier-printout-table-model", "Defaul Table Model", $model->getContent(), "select", "Pilih Default Table Model" )
            ->addCurrent("cashier-printout-table-keterangan", "Default Description Visibility", $keterangan->getContent(), "select", "Pilih Default Table Description Visibility" )
            ->addCurrent("cashier-printout-header", "Tampilkan Header", "1", "checkbox", "Menampilkan Header" )
            ->addCurrent("cashier-printout-payment", "Tampilkan Detail Pembayaran", "1", "checkbox", "Menampilkan Detail Pembayaran" )
            ->addCurrent("smis-rs-abrevation", "Singkatan RS", "RS", "text", "Singkatan RS" )
            ->addCurrent("smis-rs-header", "Header Kwitansi", "", "file-single-image", "Header Kwitansi" )
            ->addCurrent("smis-rs-footer", "Footer Lokasi", "", "text", "Lokasi" )
            ->addCurrent("cashier-printout-title", "Tampilkan Judul Grup", "1", "checkbox", "Menampilkan Judul Grup" )
            ->addCurrent("cashier-printout-side", "Tampilkan Grup Disamping", "1", "checkbox", "Menampilkan Grup Secara Disebelah Samping" )
            ->addCurrent("cashier-printout-subtotal", "Tampilkan Sub Total Untuk Tiap Grup", "1", "checkbox", "Menampilkan Sub Total Setiap Grup" )
            ->addCurrent("cashier-printout-subtotal-jaspel", "Menampilkan Sub Total Jaspel", "1", "checkbox", "Menampilkan Sub Total Jaspel jika Terdapat Jaspel" )
            ->addCurrent("cashier-printout-space", "Buat Jarak Antar Sub Total", "1", "checkbox", "Buat Sebuah Jarak Antar Sub Total" )
            ->addCurrent("cashier-printout-discount-alone", "Tampilkan Diskon Sendiri", "0", "checkbox", "Tampilkan Diskon Sendiri" )
            ->addCurrent("cashier-printout-join-administrasi", "Gabungkan Administrasi dengan Jaspel", "0", "checkbox", "Gabungkan Administrasi dengan Jasa Pelayanan" )
            ->addCurrent("cashier-printout-blank-pj", "Biarkan Penanggung Jawab Kosong", "0", "checkbox", "Biarkan Penanggung Jawab Kosong" )
            ->addCurrent("cashier-printout-perawat", "Tambahkan Tanda Tangan Perawat", "0", "checkbox", "Tambahkan Tanda Tangan Perawat" )
            ->addCurrent("cashier-printout-hide-perawat-pj-on-jalan", "Hilangkan Perawat dan Penanggung Jawab di URJ", "0", "checkbox", "Menghilangkan Perawat dan Penanggung Jawab di Rawat Inap" )
            ->addCurrent("cashier-printout-sub-ruang", "Tampilkan Data Sub Ruang Untuk Tindakan Perawat dan Alkes", "0", "checkbox", "Tampilkan Data Sub Ruang Untuk Tindakan Perawat dan Alkes" )
            ->addCurrent("cashier-printout-show-response-time", "Tampilkan Response Time", "0", "checkbox", "Menampilkan response Time" )
            ->addCurrent("cashier-printout-jaspel-position", "Tampilkan Markup di ", $jaspel->getContent(), "select", "Tampilkan Markup di samping atau di bawah" );
    $smis   ->addSectionCurrent("Urutan Kwitansi Detail");
    foreach ($replace as $one=>$val){
        $smis ->addCurrent("cashier-replace-order-".$one, "Urutan ".$val,$order->getContent(), "select", "Urutan Kelompok Untuk ".$val);
    }
    $smis   ->addSectionCurrent("Pengelompokan Grup Jenis Taginan");
    foreach ($replace as $one=>$val){
        $smis ->addCurrent ("cashier-replace-tipe-".$one, "Kelompok Untuk ".$val,$one, "text", "Kelompok Untuk ".$val );
    }
}

if($smis->isGroupAdded("simple_kwitansi", "Kwitansi Sederhana","fa fa-file")){
    $pilih_ruangan = new OptionBuilder();
    $pilih_ruangan ->add("Ruangan - Sesuai Apa Adanya","0","1");
    $pilih_ruangan ->add("Ruangan - Sesuai dengan Pilihan","1","0");

    $nokwitansi = new OptionBuilder();
    $nokwitansi ->add("Gunakan ID","id","1")
                ->add("Gunakan Kwitansi Tahunan","yearly","0")
                ->add("Gunakan Kwitansi Bulanan","monthly","0")
                ->add("Gunakan Kwitansi Palimirma","palimirma","0");
    
    $smis   ->addSectionCurrent("Header Kwitansi")
            ->addCurrent("cashier-simple-kwitansi-header-file", "Gunakan File pada Header Simple Kwitansi","", "file-single-image", "jika diisi maka header pada kwitansi simple akan mengikuti file header")
            ->addCurrent("cashier-simple-kwitansi-header-file-width", "Lebar File Image","100%", "text", "Lebar dari File Image")
            ->addCurrent("cashier-simple-kwitansi-header-file-height", "Tinggi File Image","auto", "text", "Tinggi dari File Image")
            ->addCurrent("cashier-simple-kwitansi-show-no-kwitansi", "Tampilkan Nomor Kwitansi","1", "checkbox", "Menampilkan Nomor Kwitansi pada bagian header")
            ->addCurrent("cashier-simple-kwitansi-custom-title", "Tambahkan Judul ini di bagian Header","", "text", "jika diisi maka judul ini akan ditampilkan pada bagian header")
            ->addCurrent("cashier-simple-kwitansi-header-alamat", "Tambahkan Detail Alamat","", "checkbox", "jika dicentang maka akan tampil data detail alamat pasien")
            ->addSectionCurrent("Content")
            ->addCurrent("cashier-simple-kwitansi-place", "Ubah Ruangan Khusus Kasir sesuai Inputan",$pilih_ruangan->getContent(), "select", "jika ini di Check maka khusus ruangan yang tagihanya diinputkan di kasir akan sesuai dengan inputan pilihan ruanganya")
            ->addCurrent("cashier-simple-kwitansi-town", "Kota Ruangan","", "text", "Kota Entity ini")
            ->addCurrent("cashier-simple-kwitansi-rs", "Nama Perusahaan","", "text", "Nama Perusahaan")
            ->addCurrent("cashier-simple-kwitansi-address", "Alamat Perusahaan","", "text", "Alamat Perusahaan")
            ->addCurrent("cashier-simple-kwitansi-sum-bayar", "Tampilkan Total Bayar","1", "checkbox", "Menampilkan Total Pembayaran")
            ->addCurrent("cashier-simple-kwitansi-show-inacbg-shadow", "Menampilkan Kode INACBG pada Kwitansi Bayangan","0", "checkbox", "ketika di set kode inacbg akan muncul di kwitansi bayangan")
            ->addCurrent("cashier-simple-kwitansi-show-all-number-kwitansi", "Menampilkan seluruh nomor kwitansi pada semua pembayaran","1", "checkbox", "ketika di set maka nomor kwitansi dari masing-masing pemabyaran akan muncul pada bagian detail")
            ->addCurrent("cashier-simple-kwitansi-show-pembayar", "Tampilkan Pembayar","0", "checkbox", "Tampilkan Pembayar pada bagian footer")
            ->addSectionCurrent("Style")
            ->addCurrent("cashier-simple-kwitansi-font-size", "Ukuran Font","12px", "text", "Ukuran Cetak Font")
            ->addCurrent("cashier-simple-kwitansi-font-size-f20", "Ukuran Font khusus F20","12px", "text", "Ukuran Cetak Font Khusus Nomor Kwitansi")
            ->addCurrent("cashier-simple-kwitansi-letter-space", "Ukuran Jarak Font","normal", "text", "Ukuran Jarak Font Tulisan (12px, 15px, initial, inherit, <strong>normal</strong>) ")
            ->addCurrent("cashier-simple-kwitansi-space-footer", "Total Space di Bagian Footer Kwitansi ","3", "text", "gunakan angka yang  >= 3 dibawah itu akan di set default 3")
            ->addCurrent("cashier-simple-kwitansi-max-width", "Maksimum Width pada simple Kwitansi ","100%", "text", "lebar maksimum dari kwitansi Sederhana, bisa diisi dengan 1000px, 100%, 100em dst")
            ->addSectionCurrent("Number Rules")
            ->addCurrent("cashier-simple-kwitansi-use-own-number", "Aktifkan Penggunaan Nomor Kwitansi",$nokwitansi->getContent(), "select", "Penggunaan Kwitansi mengikuti Model, ID, Tahunan, Bulanan atau Harian")
            ->addCurrent("cashier-simple-kwitansi-use-own-number-separated", "Aktifkan Penggunaan Nomor Kwitansi Secara Terpisah","0", "checkbox", "Untuk Penggunaan Kwitansi, Akan dipisah antara Asauransi, Cash, Bank, dan Diskon")
            ->addCurrent("cashier-simple-kwitansi-use-own-number-prefix-asuransi", "Prefix Kwitansi Asuransi","", "text", "Prefix untuk Nomor Kwitansi Asuransi")
            ->addCurrent("cashier-simple-kwitansi-use-own-number-prefix-bank", "Prefix Kwitansi Bank","", "text", "Prefix untuk Nomor Kwitansi Bank")
            ->addCurrent("cashier-simple-kwitansi-use-own-number-prefix-cash", "Prefix Kwitansi Cash","", "text", "Prefix untuk Nomor Kwitansi Cash")
            ->addCurrent("cashier-simple-kwitansi-use-own-number-prefix-diskon", "Prefix Kwitansi Diskon","", "text", "Prefix untuk Nomor Kwitansi Diskon")
            ->addSectionCurrent("Order")
            ->addCurrent("cashier-simple-kwitansi-arrange-bayar", "Urutkan Pembayaran","id DESC", "text", "Mengurutkan Pembayaran - default (metode ASC, id DESC) ")
            ->addCurrent("cashier-simple-kwitansi-arrange-bayar-last-show", "Urutkan Pembayaran pada Bagian Atas Kwitansi","id DESC", "text", "Mengurutkan Pembayaran - default (metode DESC, id DESC) ")
            ->addSectionCurrent("Behaviour")
            ->addCurrent("cashier-simple-kwitansi-per-kwitansi", "Pembayaran Perkwitansi","0", "checkbox", "Jika ini diaktifkan maka pembayaran per kwitansi akan di aktifkan")
            ->addCurrent("cashier-simple-kwitansi-cek-print-ri", "Cek Status Rawat Inap","0", "checkbox", "jika diaktifkan maka Petugas Kasir tidak bisa mencetak kwitansi jika ruangan Rawat Inap belum mengeluarkan pasien dari ruanganya")
            ->addCurrent("cashier-simple-kwitansi-cek-print-rj", "Cek Status Rawat Jalan","0", "checkbox", "jika diaktifkan maka Petugas Kasir tidak bisa mencetak kwitansi jika ruangan Rawat Jalan belum mengeluarkan pasien dari ruanganya");
}


if($smis->isGroupAdded("form_inacbg", "Kwitansi InaCBG","fa fa-tag")){
    $smis->addCurrent ( "cashier-pelaksana-verivicator", "Pelaksana Verivikasi","", "text", "Pelaksana Verivikasi");
    $smis->addCurrent ( "cashier-kwitansi-inacbg-intensif", "Ruangan-Ruangan Intensif","", "text", "Slug Ruangan Intensif Pisahkan dengan koma, misal : icu, iccu, nicu, picu, hicu");
}


if($smis->isGroup("klaim_pasien", "Klaim Pasien","fa fa-users")){
    $smis->addCurrent("cashier-klaim-pasien-autoload","Auto Load Default Klaim Pasien", "0", "checkbox", "jika di centang maka saat load data detail pasien akan di crawler langsung ");
}

if($smis->isGroupAdded("layanan_data", "Service","fa fa-university")){
    $mode_service   = new OptionBuilder();
    $mode_service   ->add("Kwitansi Detail","tagihan.php","1")
                    ->add("Kwitansi Sederhana","simple_kwitansi.php","0")
                    ->add("Kwitansi Grup","simple_kwitansi_grup.php","0")
                    ->add("Kwitansi BPJS","tagihan_format_bpjs.php","0")
                    ->add("Kwitansi Rumus","kwitansi_rumus.php","0")
                    ->add("Kwitansi Rumus Detail","kwitansi_rumus_detail.php","0")
                    ->add("Form Ina CBG","simple_kwitansi_grup_harian","0");
    $smis   ->addSectionCurrent("Model Tampilan Kwitansi Ruangan")
            ->addCurrent("cashier-rawat-mode", "Model Tampilan Kwitansi di Rawat",$mode_service->getContent(), "select", "Tampilan Kwitansi di Rawat")
            ->addCurrent("cashier-rawat-mode-print", "Ijinkan Ruangan Untuk Mencetak","0", "checkbox", "Jika di centang, maka Tombol Cetak Kwitansi akan muncul di Ruangan")
            ->addSectionCurrent("Surat Sakit")
            ->addCurrent("cashier-surat-sakit-tarif","Tarif Surat Sakit", "0", "money", "Tarif Surat Sakit")
            ->addCurrent("cashier-surat-sakit-debet","Debet Surat Sakit", "", "text", "Debet Surat Sakit")
            ->addCurrent("cashier-surat-sakit-kredit","Kredit Surat Sakit", "", "text", "Kredit Surat Sakit")
            ->addCurrent("cashier-surat-sakit-prefix","Prefix Surat Sakit", "", "text", "Prefix Surat Sakit misal 'MCLAMONGAN-'");
}

if($smis->isGroupAdded("bridging_bank", "Bridging Bank","fa fa-university")){
    $mode_service   = new OptionBuilder();
    $smis   ->addSectionCurrent("Kode Bayar Rawat Jalan")
            ->addCurrent("cashier-bridging-bank-pendaftaran", "Pendaftaran","", "text", "Pendaftaran")
            ->addCurrent("cashier-bridging-bank-periksa-poli", "Pemeriksaan Poli","", "text", "Pemeriksaan Poli")
            ->addCurrent("cashier-bridging-bank-konsultasi-poli", "Konsultasi Poli","", "text", "KOnsultasi Poli")
            ->addCurrent("cashier-bridging-bank-tindakan", "Tindakan","", "text", "Tindakan")
            ->addCurrent("cashier-bridging-bank-laboratorium", "Laboratorium","", "text", "Laboratorium")
            ->addCurrent("cashier-bridging-bank-radiology", "Radiology","", "text", "Radiology")
            ->addCurrent("cashier-bridging-bank-penunjang", "Penunjang Lain","", "text", "Penunjang Lain")
            ->addCurrent("cashier-bridging-bank-farmasi", "Farmasi","", "text", "Farmasi")
            ->addCurrent("cashier-bridging-bank-lain-lain", "Lain-Lain","", "text", "Lain-Lain")
            ->addSectionCurrent("Kode Bayar Rawat Inap")
            ->addCurrent("cashier-bridging-bank-rawat-inap", "Rawat Inap","", "text", "Rawat Inap");
}

$smis->setPartialLoad(true);
$response = $smis->init ();
?>