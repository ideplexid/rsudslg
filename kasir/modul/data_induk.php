<?php 

$tabs=new Tabulator("data_induk","data_induk",Tabulator::$POTRAIT);
$tabs->add("master_non_pasien","Non Pasien","",Tabulator::$TYPE_HTML," fa fa-user");
$tabs->add("mbank","Data Bank","",Tabulator::$TYPE_HTML," fa fa-university");
$tabs->add("pengaturan_nomor_kwitansi","Nomor Kwitansi","",Tabulator::$TYPE_HTML," fa fa-ticket");
$tabs->add("pasien_lewat_tanggal","Pasien Lewat Tanggal","",Tabulator::$TYPE_HTML," fa fa-warning");
$tabs->add("kwitansi","Kwitansi Print","",Tabulator::$TYPE_HTML," fa fa-paperclip");
$tabs->add("rumus","Rumus","",Tabulator::$TYPE_HTML," fa fa-code-fork");
$tabs->add("master_kwitansi_rumus","Kwitansi Rumus","",Tabulator::$TYPE_HTML," fa fa-tag");
$tabs->setPartialLoad(true,"kasir","data_induk","data_induk",true);
echo $tabs->getHtml();

?>