<?php 


$tab=new Tabulator("jurnal", "jurnal",Tabulator::$POTRAIT);
$tab->setPartialLoad(true,"kasir","jurnal","jurnal",true);
$tab->add("laporan_pendapatan_dokter_spesialis", "Pendapatan Obat", "",Tabulator::$TYPE_HTML,"fa fa-ticket");
$tab->add("laporan_rekap_pendapatan_per_pasien_rawat_jalan", "Jurnal Poli Terpadu", "",Tabulator::$TYPE_HTML,"fa fa-files-o");
$tab->add("laporan_rekap_pendapatan_per_pasien_spesialis", "Jurnal Poli Spesialis", "",Tabulator::$TYPE_HTML,"fa fa-files-o");
$tab->add("laporan_rekap_pendapatan_per_pasien_rawat_inap", "Jurnal Rawat Inap", "",Tabulator::$TYPE_HTML,"fa fa-files-o");
$tab->add("laporan_pendapatan_spesialis", "Rekap Pendapatan Spesialis", "",Tabulator::$TYPE_HTML,"fa fa-ticket");
          
echo $tab ->getHtml();
?>