<?php 
	/*
	*
	*	Nama 		: Pengaturan Stok
	*	Author 		: Dwi Al Aji Suseno
	*	Keterangan	: modul ini dipanggil dari library inventory/modul
	*	created 	: 07 Oktober 2016
	*	updated 	: 07 Oktober 2016
	*
	*/
	require_once 'smis-libs-inventory/modul/PengaturanStok.php';		
	global $db;
	$pengaturan_stok = new PengaturanStok($db, "Kamar Mayat", "km", "kamar_mayat");
	$pengaturan_stok->initialize();	
?>