<?php
global $db;
require_once "smis-base/smis-include-synchronize-db.php";
require_once ("smis-base/smis-include-service-consumer.php");
require_once 'smis-libs-hrd/EmployeeResponder.php';

if(isset($_POST['super_command']) && $_POST['super_command']=="get_last_position"){
	require_once "kamar_mayat/snippet/get_last_position.php";
	return;
}

$header=array ('Nama','Jabatan',"NIP" );
$dktable = new Table ( $header );
$dktable->setName ( "dokter" );
$dktable->setModel ( Table::$SELECT );
$dkadapter = new SimpleAdapter ();
$dkadapter->add ( "Jabatan", "nama_jabatan" );
$dkadapter->add ( "Nama", "nama" );
$dkadapter->add ( "NIP", "nip" );
$dokter = new EmployeeResponder ( $db, $dktable, $dkadapter, "dokter" );

$header=array ('Nama','Jabatan',"NIP" );
$dktable = new Table ( $header );
$dktable->setName ( "petugas" );
$dktable->setModel ( Table::$SELECT );
$dkadapter = new SimpleAdapter ();
$dkadapter->add ( "Jabatan", "nama_jabatan" );
$dkadapter->add ( "Nama", "nama" );
$dkadapter->add ( "NIP", "nip" );
$petugas = new EmployeeResponder ( $db, $dktable, $dkadapter, "" );

/* PASIEN */
$header=array ('Nama','NRM',"No Reg" );
$ptable = new Table ($header);
$ptable->setName ( "pasien" );
$ptable->setModel ( Table::$SELECT );
$padapter = new SimpleAdapter ();
$padapter->add ( "Nama", "nama_pasien" );
$padapter->add ( "NRM", "nrm", "digit8" );
$padapter->add ( "No Reg", "id" );
$presponder = new ServiceResponder ( $db, $ptable, $padapter, "get_registered" );

/* PASIEN */
$header=array ('Nama','Harga',"Keterangan" );
$dbtable=new DBTable($db,"smis_km_master_layanan");
$ptable = new Table ($header);
$ptable->setName ( "master_layanan" );
$ptable->setModel ( Table::$SELECT );
$padapter = new SimpleAdapter ();
$padapter->add ( "Nama", "nama" );
$padapter->add ( "Harga", "harga", "money Rp." );
$padapter->add ( "Keterangan", "keterangan" );
$lresponder = new DBResponder ($dbtable, $ptable, $padapter );

$super = new SuperCommand ();
$super->addResponder ( "dokter", $dokter );
$super->addResponder ( "petugas", $petugas );
$super->addResponder ( "pasien", $presponder );
$super->addResponder ( "master_layanan", $lresponder );
$init = $super->initialize ();
if ($init != null) {
	echo $init;
	return;
}
$_synchronous=getSettings($db,"smis-kamar-mayat-autosynch-tagihan","0")=="1";
$header=array ('Tanggal',"Ruangan",'Dokter','Biaya',"Layanan","Keterangan","Carabayar");
$uitable=null;
if($_synchronous){
    $uitable = new TableSynchronous ( $header, "&nbsp;", NULL, true );
}else{
    $uitable = new Table ( $header, "&nbsp;", NULL, true );
}

$uitable->setName ( "layanan" );
/* this is respond when system have to response */
if (isset ( $_POST ['command'] )) {    
    $dbtable = new DBTable ( $db, "smis_km_pesanan_pasien" );
    $dbtable->addCustomKriteria ( "noreg_pasien", " ='" . $_POST ['noreg_pasien'] . "'" );
    $adapter=null;
    $dbres=null;
    if($_synchronous){
        require_once "kamar_mayat/class/responder/KamarMayatSynchronizeResponder.php";
        require_once "kamar_mayat/class/adapter/KamarMayatSynchronizeAdapter.php";
        $sync_adapter=new KamarMayatSynchronizeAdapter();
        $dbtable->activateTableSynchronous($_synchronous);
        $adapter = new SynchronousViewAdapter ();
        $dbres = new KamarMayatSynchronizeResponder ( $dbtable, $uitable, $adapter,$sync_adapter );
    }else{
        require_once "kamar_mayat/class/responder/KamarMayatResponder.php";
        $adapter = new SimpleAdapter ();
        $dbres = new KamarMayatResponder ( $dbtable, $uitable, $adapter );
        
    }
    
	$adapter->add("Tanggal","waktu","date d M Y");
	$adapter->add("Dokter","dokter");
	$adapter->add("Alamat","alamat");
	$adapter->add("Keterangan","keterangan");
	$adapter->add("Layanan","layanan");
	$adapter->add("Biaya","biaya","money Rp.");
	$adapter->add("Ruangan","ruangan","unslug");
	$adapter->add("Carabayar","carabayar","unslug");
	
	$data = $dbres->command ( $_POST ['command'] );
	echo json_encode ( $data );
	return;
}

$urjip=new ServiceConsumer($db, "get_urjip",array());
$urjip->setMode(ServiceConsumer::$MULTIPLE_MODE);
$urjip->setCached(true,"get_urjip");
$urjip->execute();
$content=$urjip->getContent();
$ruangan=array();
foreach ($content as $autonomous=>$ruang){
	foreach($ruang as $nama_ruang=>$jip){
		$option=array();
		$option['value']=$nama_ruang;
		$option['name']=ArrayAdapter::format("unslug", $nama_ruang);
		$ruangan[]=$option;
	}
}



$pemakaian=getSettings($db, "smis-rs-layanan-mobil", "0")=="1";
$uitable->addModal ( "id", "hidden", "", "" );
$uitable->addModal ( "id_dokter", "hidden", "", "" );
$uitable->addModal ( "id_bag", "hidden", "", "" );
$uitable->addModal ( "waktu", "datetime", "Waktu", date("Y-m-d H:i:s") );
$uitable->addModal ( "ruangan", "select", "Ruangan", $ruangan);
$uitable->addModal ( "dokter", "chooser-layanan-dokter-Dokter", "Dokter", "" );
$uitable->addModal ( "petugas", "chooser-layanan-petugas-Petugas", "Petugas", "" );
$uitable->addModal ( "layanan", "chooser-layanan-master_layanan-Layanan", "Layanan", "" );
$uitable->addModal ( "biaya", "money", "Biaya", "" );
$uitable->addModal ( "keterangan", "textarea", "Keterangan", "" );

$modal = $uitable->getModal ();
$modal->setTitle ( "Pemintaan Pasien" );

$nrm = new Text ( "nrm_pasien", "nrm_pasien", "" );
$noreg = new Text ( "noreg_pasien", "noreg_pasien", "" );
$nama = new Text ( "nama_pasien", "nama_pasien", "" );
$alamat = new Text ( "alamat_pasien", "alamat_pasien", "" );
$carabayar = new Text ( "carabayar", "carabayar", "" );

$action = new Button ( "", "", "Select" );
$action->setIsButton(Button::$ICONIC_TEXT);
$action->setClass(" btn-primary ");
$action->setIcon(" fa fa-check");
$action->setAction ( "layanan.chooser('proyek_plpp','nama_pasien','pasien',pasien)" );
$nrm->setDisabled ( true );
$noreg->setDisabled ( true );
$nama->setDisabled ( true );
$alamat->setDisabled ( true );
$carabayar->setDisabled ( true );

// form for proyek
$form = new Form ( "form_pasien", "", "Permintaan Pasien" );
$form->addElement ( "NRM", $nrm );
$form->addElement ( "No Registrasi", $noreg );
$form->addElement ( "Nama", $nama );
$form->addElement ( "Alamat", $alamat );
$form->addElement ( "Cara Bayar", $carabayar );
$form->addElement ( "", $action );
echo $form->getHtml ();

/* table of current content */
echo "<div id='table_content'>";
echo $uitable->getHtml ();
echo $modal->getHtml ();
echo "</div>";
echo addJS ( "framework/bootstrap/js/bootstrap-datetimepicker.js" );
echo addJS ( "framework/smis/js/table_action.js" );
echo addCSS ( "framework/bootstrap/css/bootstrap-datetimepicker.css" );
echo addJS ( "kamar_mayat/resource/js/layanan.js",false );
?>




