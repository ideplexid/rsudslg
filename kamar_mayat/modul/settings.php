<?php
    require_once 'smis-framework/smis/api/SettingsBuilder.php';
    global $db;
    setChangeCookie ( false );
    changeCookie ();
    $settings = new SettingsBuilder ( $db, "kamar_mayat", "kamar_mayat", "settings", "Settings Kamar Mayat" );
    $settings->setShowDescription ( true );
    $settings->addTabs ( "settings", "Setting" );
    $settings->addItem ( "settings", new SettingsItem ( $db, "smis-kamar-mayat-autosynch-tagihan", "Auto Synchronize Tagihan Kamar Mayat", "", "checkbox", "Aktifkan Auto Synchronize Tagihan ke Kasir" ) );
    $settings->addItem ( "settings", new SettingsItem ( $db, "km-aktifkan-tutup-tagihan", "Aktifkan Tutup Tagihan", "0", "checkbox", "mengaktifkan tutup tagihan" ) );
    $response = $settings->init ();