<?php 
global $db;
require_once "smis-libs-class/MasterTemplate.php";
$master=new MasterTemplate($db,"smis_km_master_layanan","kamar_mayat","master_layanan");
$master	->getAdapter()
		->add("Nama","nama")
		->add("Harga","harga","money Rp.")
		->add("Keterangan","keterangan");
$master ->getUItable()
		->addHeaderElement("Nama")
		->addHeaderElement("Harga")
		->addHeaderElement("Keterangan")
		->addModal("id","hidden","","")
		->addModal("nama","text","Nama","")
		->addModal("harga","money","Harga","")
        ->addModal("nominal_jaspel", "money", "Nominal Jaspel", "" )
        ->addModal("persen_jaspel", "text", "Persen Jaspel", "" )
        ->addModal("nominal_rs", "money", "Nominal BHP & RS", "" )
        ->addModal("persen_rs", "text", "Persen BHP & RS", "" )
        ->addModal("keterangan","textarea","Keterangan","");
$master ->addResouce("js","kamar_mayat/resource/js/master_layanan.js");
$master ->setModalTitle("Layanan");
$master ->setModalComponentSize(Modal::$MEDIUM);
$master ->initialize();