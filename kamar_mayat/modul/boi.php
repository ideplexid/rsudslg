<?php 
	/*
	*	
	*	Nama 		: Pengaturan Stok
	*	Author 		: Dwi Al Aji Suseno
	*	Keterangan	: modul ini dipanggil dari library inventory
	*	created 	: 07 Oktober 2016
	*	updated 	: 07 Oktober 2016
	*
	*/
	require_once 'smis-libs-inventory/boi.php';
	global $db;
	$boi = new BOI($db, "Kamar Mayat", "kamar_mayat", "km");
	$boi->initialize();
?>