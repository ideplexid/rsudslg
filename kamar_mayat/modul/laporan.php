<?php 
global $db;
require_once "smis-libs-class/MasterTemplate.php";
require_once "smis-libs-class/MasterSlaveTemplate.php";
require_once "kamar_mayat/class/responder/KamarMayatLaporanResponder.php";
$master=new MasterSlaveTemplate($db,"smis_km_pesanan_pasien","kamar_mayat","laporan");
$responder = new KamarMayatLaporanResponder($master->getDBTable(),$master->getUItable(),$master->getAdapter());
$master ->setDBResponder($responder);
$master ->setDateEnable(true);
$master	->getAdapter()
		->add("Tanggal","waktu","date d M Y H:i:s")
		->add("Ruang","ruangan","unslug")
        ->add("No.Reg","noreg_pasien")
        ->add("NRM","nrm_pasien")
		->add("Nama","nama_pasien")
        ->add("Jenis Layanan","layanan")
        ->add("Biaya","biaya","money Rp.")
		->add("Operator","petugas")
        ->add("Cara Bayar","carabayar");

$master ->getUItable()
		->addHeaderElement("Tanggal")
        ->addHeaderElement("Ruang")
		->addHeaderElement("No.Reg")
		->addHeaderElement("NRM")
		->addHeaderElement("Nama")
		->addHeaderElement("Jenis Layanan")
		->addHeaderElement("Biaya")
		->addHeaderElement("Operator")
		->addHeaderElement("Cara Bayar");
		
$master ->getUItable()
        ->setAction(false)
		->addModal("dari","date","Dari","")
        ->addModal("sampai","date","Sampai","");	
$form =   $master ->getForm()->setTitle("Laporan Kamar Mayat");

$button = new Button("","","View");
$button ->setIcon("fa fa-search");
$button ->setClass("btn btn-primary");
$button ->setIsButton(Button::$ICONIC_TEXT);
$button ->setAction("laporan.view()");

$excel = new Button("","","Excel");
$excel ->setIcon("fa fa-file-o-excel");
$excel ->setClass("btn btn-primary");
$excel ->setIsButton(Button::$ICONIC_TEXT);
$excel ->setAction("laporan.excel()");

$form ->addElement("",$button);
$form ->addElement("",$excel);


if($master ->getDBResponder()->isView() || $master ->getDBResponder()->isExcel()){
    $dbtable = $master ->getDBTable();
    if(isset($_POST['dari']) && $_POST['dari']!=""){
        $dbtable ->addCustomKriteria(NULL," DATE(waktu) >='".$_POST['dari']."'  ");
    }
    if(isset($_POST['sampai']) && $_POST['sampai']!=""){
        $dbtable ->addCustomKriteria(NULL," DATE(waktu) <='".$_POST['sampai']."'  ");        
    }
}
$master ->addRegulerData("dari","dari","id-value");
$master ->addRegulerData("sampai","sampai","id-value");
$master ->initialize();