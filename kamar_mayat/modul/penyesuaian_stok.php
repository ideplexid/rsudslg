<?php 
	/*
	*
	*	Nama 		: Penyesuaian Stok
	*	Author 		: Dwi Al Aji Suseno
	*	Keterangan	: modul ini dipanggil dari library inventory/modul
	*	created 	: 07 Oktober 2016
	*	updated 	: 07 Oktober 2016
	*
	*/
	require_once 'smis-libs-inventory/penyesuaian_stok.php';
	global $db;
	$penyesuaian_stok = new PenyesuaianStok($db, "Kamar Mayat", "kamar_mayat", "km");
	$penyesuaian_stok->initialize();
?>