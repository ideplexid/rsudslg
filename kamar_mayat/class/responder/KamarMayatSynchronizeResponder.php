<?php 

/**
 * this class used to communicate with 
 * the server how to synchronize the whole data
 * 
 * */

class KamarMayatSynchronizeResponder extends DBSynchronousResponder{
    public function synchronousData($original,$adapted,$id,$prop=""){
        $data=array();
        $data['grup_name']      = "kamar_mayat";
        $data['nama_pasien']    = $original->nama_pasien;
        $data['noreg_pasien']   = $original->noreg_pasien;
        $data['nrm_pasien']     = $original->nrm_pasien;
        $data['entity']         = "kamar_mayat";
        $data['list']           = $adapted;
        $serv = new ServiceConsumer($this->getDBTable()->get_db(),"proceed_receivable",$data,"kasir");
        $serv->execute();
        $hasil=$serv->getContent();
        return $hasil;
    }

    public function command($command){
        if($command=="cek_tutup_tagihan"){
			$pack=new ResponsePackage();
            $result = $this->cek_tutup_tagihan();
            
            if($result=="1"){
                $pack->setAlertVisible(true);
			    $pack->setAlertContent("Peringatan", "<strong>Pasien Telah Di Tutup Tagihanya Oleh Kasir untuk proses pembayaran</strong>, </br>silakan menghubungi kasir dahulu bila akan menambah, menubah, mengurangi dan menghapus tagihan pasien",Alert::$DANGER);
            }

			$pack->setContent($result);
			$pack->setStatus(ResponsePackage::$STATUS_OK);
			return $pack->getPackage();
		}else{
			return parent::command($command);
		}
    }

    public function cek_tutup_tagihan(){
        global $db;
        if(getSettings($db,"km-aktifkan-tutup-tagihan","0")=="0"){
            return 0;
        }
        require_once "smis-base/smis-include-service-consumer.php";
        $serv = new ServiceConsumer($this->getDBTable()->get_db(),"cek_tutup_tagihan",NULL,"registration");
        $serv ->addData("noreg_pasien",$_POST['noreg_pasien']);
        $serv ->execute();
        $result = $serv ->getContent();
        return $result;
    }

    public function delete(){
        $id['id']				= $_POST['id'];
        $one = $this->dbtable->select($id);
        $_POST['noreg_pasien'] = $one->noreg_pasien;
        $cek = $this->cek_tutup_tagihan();

        if($cek=="1"){
            $success['success']	= -1;
            return $success;
        }

		$result = parent::delete();
        //if(getSettings($db,"fisiotherapy-accounting-auto-notif","0")=="1") {
        //    $this-> synchronizeToAccounting($this->dbtable->get_db(), $_POST['id'] ,"del");
        //}
        return $result;
	}

}