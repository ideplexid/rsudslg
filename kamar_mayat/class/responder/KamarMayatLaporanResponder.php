<?php 
class KamarMayatLaporanResponder extends DBResponder{

    public function excel(){

        $this->dbtable->setShowAll(true);
        $this->dbtable->setOrder(" carabayar ASC, waktu ASC ",true);
        $list = $this->dbtable->view("","0");
        $data = $list['data'];

		require_once "smis-libs-out/php-excel/PHPExcel.php";
		$file = new PHPExcel ();
		
		$fillcabang = array();
		$fillcabang['borders'] = array();
		$fillcabang['borders']['allborders'] = array();
		$fillcabang['borders']['allborders']['style'] = PHPExcel_Style_Border::BORDER_THIN ;
		
		$file->setActiveSheetIndex ( 0 );
		$sheet = $file->getActiveSheet ( 0 );
        $sheet->setTitle ("Laporan Kamar Mayat");
        $last_column = PHPExcel_Cell::stringFromColumnIndex(count($this->excel_column) - 1);
        $sheet->mergeCells("A1:I1")->setCellValue ( "A" . 1, " LAPORAN PENDAPATAN KAMAR MAYAT - ".ArrayAdapter::format("date d M Y",$_POST['dari'])." - ".ArrayAdapter::format("date d M Y",$_POST['sampai']));

        $column_num = 2;
        $cur_carabayar = "";
        $total = 0;
        foreach($data as $x){
            $carabayar = $x->carabayar;
            if($cur_carabayar!=$carabayar){

                if($total>0){
                    $sheet->setCellValue("F".$column_num,"Total");
                    $sheet->setCellValue("G".$column_num,$total);
                    $sheet->getStyle ( 'A'.$column_num.':I'.$column_num )->applyFromArray ($fillcabang);
                    $sheet->getStyle ( "A".$column_num.":I".$column_num)->getFont ()->setBold ( true );
                    $sheet->getStyle('G'.$column_num)->getNumberFormat()->setFormatCode('#,##0.00');
                    $total=0;
                    $column_num++;
                }

                $column_num++;
                $cur_carabayar = $carabayar;
                $sheet->setCellValue("A".$column_num,"Tanggal");
                $sheet->setCellValue("B".$column_num,"Ruang");
                $sheet->setCellValue("C".$column_num,"No.Reg");
                $sheet->setCellValue("D".$column_num,"NRM");
                $sheet->setCellValue("E".$column_num,"Nama");
                $sheet->setCellValue("F".$column_num,"Jenis Layanan");
                $sheet->setCellValue("G".$column_num,"Biaya");
                $sheet->setCellValue("H".$column_num,"Operator");
                $sheet->setCellValue("I".$column_num,"Cara Bayar");
                $sheet->getStyle ( 'A'.$column_num.':I'.$column_num )->applyFromArray ($fillcabang);
                $sheet->getStyle ( "A".$column_num.":I".$column_num)->getFont ()->setBold ( true );
                $column_num++;
            }
            $sheet->setCellValue("A".$column_num,ArrayAdapter::format("date d M Y H:i",$x->waktu));
            $sheet->setCellValue("B".$column_num,ArrayAdapter::format("unslug",$x->ruangan));
            $sheet->setCellValue("C".$column_num,$x->noreg_pasien);
            $sheet->setCellValue("D".$column_num,$x->nrm_pasien);
            $sheet->setCellValue("E".$column_num,$x->nama_pasien);
            $sheet->setCellValue("F".$column_num,$x->layanan);
            $sheet->setCellValue("G".$column_num,$x->biaya);
            $sheet->setCellValue("H".$column_num,$x->petugas);
            $sheet->setCellValue("I".$column_num,ArrayAdapter::format("unslug",$x->carabayar));
            $sheet->getStyle( 'A'.$column_num.':I'.$column_num )->applyFromArray ($fillcabang);
            $sheet->getStyle('G'.$column_num)->getNumberFormat()->setFormatCode('#,##0.00');

            $total += $x->biaya;
            $column_num++;
        }

        if($total>0){
            $sheet->setCellValue("F".$column_num,"Total");
            $sheet->setCellValue("G".$column_num,$total);
            $sheet->getStyle ( 'A'.$column_num.':I'.$column_num )->applyFromArray ($fillcabang);
            $sheet->getStyle ( "A".$column_num.":I".$column_num)->getFont ()->setBold ( true );
            $sheet->getStyle('G'.$column_num)->getNumberFormat()->setFormatCode('#,##0.00');
            $total=0;
            $column_num++;
        }
        
        
		header ( 'Content-Type: application/vnd.ms-excel' );
		header ( 'Content-Disposition: attachment;filename="Laporan Kamar Mayat.xls"' );
		header ( 'Cache-Control: max-age=0' );
		$writer = PHPExcel_IOFactory::createWriter ( $file, 'Excel5' );
		$writer->save ( 'php://output' );

		return;
	}

}