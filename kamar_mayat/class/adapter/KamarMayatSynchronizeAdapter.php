<?php 

class KamarMayatSynchronizeAdapter extends SynchronousSenderAdapter{
    private $content;
    public function __construct(){
        parent::__construct();
        $this->content=array();
    }
    
    public function adapt($d){
        $result=array();
        $onedata ['waktu'] = ArrayAdapter::format ( "date d M Y", $d->waktu );
		$onedata ['nama'] = $d->layanan;
		$onedata ['biaya'] = $d->biaya;
		$onedata ['jumlah'] = 1;
		$onedata ['start'] = $d->tanggal;
		$onedata ['end'] = $d->tanggal;
		$onedata ['id'] = $d->id;
		$onedata ['keterangan'] = "Kamar Mayat - " . $d->layanan . " Senilai " . ArrayAdapter::format ( "only-money Rp.", $d->biaya ).", ".$d->keterangan;
		$onedata ['prop']       = $this->getProp();
        $this->content [] = $onedata;
        return $this->content;
    }
    
    public function getContent($data){
        parent::getContent($data);
        return $this->content;
    }
}

?>