$(document).ready(function(){

    $("#master_layanan_nominal_jaspel , #master_layanan_nominal_rs ").on("keyup",function(){
        var tarif = getMoney("#master_layanan_harga");
        var nominal_jaspel = getMoney("#master_layanan_nominal_jaspel");
        var nominal_rs = getMoney("#master_layanan_nominal_rs");

        var persen_rs = nominal_rs*100/tarif;
        var persen_jaspel = nominal_jaspel*100/tarif;
        
        $("#master_layanan_persen_jaspel").val(persen_jaspel.toFixed(3));
        $("#master_layanan_persen_rs").val(persen_rs.toFixed(3));
    });

    $("#master_layanan_harga, #master_layanan_persen_jaspel , #master_layanan_persen_rs").on("keyup",function(){
        var tarif = getMoney("#master_layanan_harga");

        var persen_rs = Number($("#master_layanan_persen_rs").val());
        var persen_jaspel = Number($("#master_layanan_persen_jaspel").val());
        
        var nominal_jaspel = persen_jaspel*tarif/100;
        var nominal_rs = persen_rs*tarif/100;
        
        setMoney("#master_layanan_nominal_jaspel",nominal_jaspel);
        setMoney("#master_layanan_nominal_rs",nominal_rs);
    });

});