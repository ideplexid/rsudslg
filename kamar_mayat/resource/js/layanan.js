/*ACTION SCRIPT*/
var dokter;
var layanan;
var pasien;
var master_layanan;
var petugas;
$(document).ready(function(){
	$(".mydatetime").datetimepicker({minuteStep:1});
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
	//setMoney("#layanan_biaya",0);
	var column = new Array(
			"id","id_dokter","layanan",
			"biaya","dokter","id_petugas","petugas",
			"ruangan","keterangan","waktu"		
		);
	layanan=new TableAction("layanan","kamar_mayat","layanan",column);
	layanan.getRegulerData=function(){
		var reg_data={	
				page:this.page,
				action:this.action,
				super_command:this.super_command,
				prototype_name:this.prototype_name,
				prototype_slug:this.prototype_slug,
				prototype_implement:this.prototype_implement
				};
		reg_data['noreg_pasien']=$("#noreg_pasien").val();
		reg_data['nama_pasien']=$("#nama_pasien").val();
		reg_data['nrm_pasien']=$("#nrm_pasien").val();
        reg_data['alamat']=$("#alamat_pasien").val();
        reg_data['carabayar']=$("#carabayar").val();
		return reg_data;
	};
	layanan.setMultipleInput(true);
	layanan.addNoClear("biaya");
	layanan.addNoClear("ruangan");
	layanan.show_add_form=function(){
		if($("#noreg_pasien").val()==""){
			showWarning("Silakan Pilih Pasien","Silakan Pilih Pasien Terlebih Dahulu");
			return;
		}
		
		this.clear();
		this.setLastPosition();
		this.show_form();
    };
    

    layanan.save = function(){
        showLoading();
        var cek = this.cekTutupTagihan();
        if(cek){
            TableAction.prototype.save.call(this);        
        }
        dismissLoading();
    };

    layanan.cekTutupTagihan = function(){
        var reg_data = {	
                page:this.page,
                action:this.action,
                super_command:this.super_command,
                prototype_name:this.prototype_name,
                prototype_slug:this.prototype_slug,
                prototype_implement:this.prototype_implement,
            };			
        var noreg                 = $("#noreg_pasien").val();
        if(noreg==""){
            smis_alert("Error","Silakan Pilih Pasien Terlebih Dahulu","alert-danger");
            return;
        }
        reg_data['command']        = 'cek_tutup_tagihan';
        reg_data['noreg_pasien']  = noreg;
        
        var res = $.ajax({
            type: "POST",
            url: "",
            data:reg_data,
            async: false
        }).responseText;

        var json = getContent(res);
        if(json=="1"){
            return false;
        }else{
            return true;
        }
    };
	
	layanan.setLastPosition=function(){
		var a=this.getRegulerData();
		a['super_command']="get_last_position";
		a['noreg_pasien']=$("#noreg_pasien").val();
		showLoading();
		$.post("",a,function(res){
			json=getContent(res);
			$("#layanan_ruangan").val(json);
			dismissLoading();
		});
	};
	
	master_layanan=new TableAction("master_layanan","kamar_mayat","layanan",new Array());
	master_layanan.setSuperCommand("master_layanan");
	master_layanan.setShowParentModalInChooser(false);
	master_layanan.selected=function(json){
		$("#layanan_layanan").val(json.nama);
		setMoney("#layanan_biaya",json.harga);
		layanan.view();
	};
	
	pasien=new TableAction("pasien","kamar_mayat","layanan",new Array());
	pasien.setSuperCommand("pasien");
	pasien.setShowParentModalInChooser(false);
	pasien.selected=function(json){
		var nama=json.nama_pasien;
		var nrm=json.nrm;
		var noreg=json.id;
		var alamat=json.alamat_pasien+" "+json.nama_kelurahan+" "+json.nama_kecamatan+" "+json.nama_kabupaten;
		$("#nama_pasien").val(nama);
		$("#nrm_pasien").val(nrm);
		$("#noreg_pasien").val(noreg);
		$("#gol_darah").val(json.gol_darah);
        $("#alamat_pasien").val(alamat);
        $("#carabayar").val(json.carabayar);
		layanan.view();
	};

	
	dokter=new TableAction("dokter","kamar_mayat","layanan",new Array());
	dokter.setSuperCommand("dokter");
	dokter.selected=function(json){
		var nama=json.nama;
		var nip=json.id;		
		$("#layanan_dokter").val(nama);
		$("#layanan_id_dokter").val(nip);
	};
    
    petugas=new TableAction("petugas","kamar_mayat","layanan",new Array());
	petugas.setSuperCommand("petugas");
	petugas.selected=function(json){
		var nama=json.nama;
		var nip=json.id;		
		$("#layanan_petugas").val(nama);
		$("#layanan_id_petugas").val(nip);
	};
	
	
});