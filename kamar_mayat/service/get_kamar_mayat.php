<?php 

global $db;

if(isset($_POST['command'])){
	$dbtablelayanan = new DBTable($db,'smis_km_pesanan_pasien');
	
	if(isset($_POST['dari'])){
		$dbtablelayanan->addCustomKriteria("DATE(waktu)>=","'".$_POST['dari']."'");
	}

	if(isset($_POST['sampai'])){
		$dbtablelayanan->addCustomKriteria("DATE(waktu)<=","'".$_POST['sampai']."'");
    }
    if(isset($_POST['download_token'])){
        $dbtablelayanan->setOrder("carabayar ASC, waktu ASC",true);
    }
	
	$responder=new ServiceProvider($dbtablelayanan);
	$data=$responder->command($_POST['command']);
	echo json_encode($data);
}