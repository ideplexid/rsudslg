<?php
	global $NAVIGATOR;

	// Administrator Top Menu
	$mr = new Menu ("fa fa-cutlery");
	$mr->addProperty ( 'title', 'Kamar Mayat' );
	$mr->addProperty ( 'name', 'Kamar Mayat' );
	$mr->addSubMenu ( "Layanan", "kamar_mayat", "layanan", "Layanan" ,"fa fa-bed");
	$mr->addSubMenu ( "Master Layanan", "kamar_mayat", "master_layanan", "Master Layanan" ,"fa fa-database");
	$mr->addSubMenu ( "Laporan", "kamar_mayat", "laporan", "Master Layanan" ,"fa fa-book");
	$mr->addSubMenu ( "Settings", "kamar_mayat", "settings", "Setting" ,"fa fa-cog");
	$mr->addSeparator ();
	require_once 'smis-libs-inventory/navigation.php';
	$inventory_navigator = new InventoryNavigator($NAVIGATOR, "", "Kamar Mayat", "kamar_mayat");
	
	// extend menu untuk RKBU, Pengaturan Stok, dan BOI
	$mr = $inventory_navigator->extendMenu($mr);
	$NAVIGATOR->addMenu ( $mr, 'kamar_mayat' );
?>
