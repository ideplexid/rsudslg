<?php
	global $db;
	global $user;
	require_once 'smis-libs-class/Policy.php';
	require_once 'smis-libs-inventory/policy.php';
	$inventory_policy = new InventoryPolicy("kamar_mayat", $user,"modul/");
	
	$policy=new Policy("kamar_mayat", $user);
	$policy->setDefaultPolicy(Policy::$DEFAULT_POLICY_RESTRICT);
	$policy->addPolicy("layanan","layanan",Policy::$DEFAULT_COOKIE_CHANGE,"modul/layanan");
	$policy->addPolicy("master_layanan","master_layanan",Policy::$DEFAULT_COOKIE_CHANGE,"modul/master_layanan");
	$policy->addPolicy("laporan","laporan",Policy::$DEFAULT_COOKIE_CHANGE,"modul/laporan");
	$policy->addPolicy("settings","settings",Policy::$DEFAULT_COOKIE_CHANGE,"modul/settings");
	$policy->combinePolicy($inventory_policy);
	$policy->initialize();