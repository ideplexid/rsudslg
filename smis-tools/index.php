<?php
global $db;
global $user;
setChangeCookie(false);
require_once 'smis-libs-class/Policy.php';
$policy=new Policy("smis-tools", $user);
$policy ->setDefaultPolicy(Policy::$DEFAULT_POLICY_ALLOW)
		->setDefaultCookie(Policy::$DEFAULT_COOKIE_KEEP)
		->initialize();
?>