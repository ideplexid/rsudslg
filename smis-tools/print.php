<?php 

require_once 'smis-libs-class/MasterTemplate.php';
global $db;
$print=new MasterTemplate($db, "smis_tools_print", "smis-tools", "print");
$print->getDBtable()->setOrder(" grup ASC ");
$uitable=$print->getUItable();
$uitable->setAddButtonEnable(false);
$uitable->setDelButtonEnable(false);
$header=array("Grup","Nama","Tanggal");
$uitable->setHeader($header);
$adapter=$print->getAdapter();
$adapter->add("Grup", "grup")
		->add("Nama", "nama")
		->add("Tanggal","tanggal","date d M Y H:i");
$print->initialize();

?>