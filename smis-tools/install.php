<?php
global $wpdb;
$query="
	CREATE TABLE IF NOT EXISTS `smis_tools_calendar` (
	  `id` int(11) NOT NULL AUTO_INCREMENT,
	  `user` varchar(32) NOT NULL,
	  `event` varchar(128) NOT NULL,
	  `mulai` datetime NOT NULL,
	  `selesai` datetime NOT NULL,
	  `keterangan` text NOT NULL,
	  `class` varchar(32) NOT NULL,
	  `prop` varchar(10) NOT NULL,
	  PRIMARY KEY (`id`)
	) ENGINE=InnoDB   ;
";
$wpdb->query($query);

$query="CREATE TABLE IF NOT EXISTS `smis_tools_satisfy` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `satisfy` varchar(10) NOT NULL,
  `komentar` text NOT NULL,
  `waktu` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `prop` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  ;";
$wpdb->query($query);


$query = "CREATE TABLE IF NOT EXISTS `smis_tools_share` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user` varchar(32) NOT NULL,
  `nama` varchar(32) NOT NULL,
  `keterangan` text NOT NULL,
  `file` text NOT NULL,
  `waktu` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `prop` varchar(32) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  ;";
$wpdb->query ( $query );

$query="CREATE TABLE IF NOT EXISTS `smis_base_feedback` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user` varchar(32) NOT NULL,
  `tanggal_tanya` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `isi_tanya` text NOT NULL,
  `tanggal_balas` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `isi_balas` text NOT NULL,
  `prop` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB;";
$wpdb->query ( $query );

$query="CREATE TABLE IF NOT EXISTS `smis_tools_print` (
	  `id` int(11) NOT NULL AUTO_INCREMENT,
	  `grup` varchar(32) NOT NULL,
	  `html` text NOT NULL,
	  `tanggal` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	  `nama` varchar(32) NOT NULL,
	  `prop` varchar(10) NOT NULL,
	  PRIMARY KEY (`id`)
	) ENGINE=InnoDB ;";
$wpdb->query ( $query );



?>
