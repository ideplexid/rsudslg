<?php 

$uitable=new Report(array('Time','Comment','Satisfaction'),"&nbsp;", NULL);
$uitable->setName("indicator");
$uitable->setDiagram(true);

/*this is respond when system have to response*/
if(isset($_POST['command'])){
	class myadapter extends ArrayAdapter{
		public function adapt($onerow){
			$array=array();
			$array['id']=$onerow->id;
			$array['Time']=self::format('date d-M-Y H:i:s', $onerow->waktu);
			$array['Comment']=$onerow->komentar;
			$array['Satisfaction']=$onerow->satisfy;
			return $array;
		}
	}
	
	class diagramadapter extends ArrayAdapter{
		public function adapt($onerow){
			$array=array();
			$array['Date']=$onerow->the_time;
			$array['biasa']=$onerow->biasa;
			$array['puas']=$onerow->puas;
			$array['tidak']=$onerow->tidak;
			return $array;
		}
	}
	
	$dbtable=new DBTable($db, "smis_tools_satisfy",array('satisfy'));
	$dbtable->setOrder("waktu DESC");
	$dbres=new DBReport($dbtable,$uitable,new myadapter(),'waktu',DBReport::$DATE);
	$diagram_view="sum(if(satisfy='biasa',1,0)) as biasa, sum(if(satisfy='puas',1,0)) as puas,sum(if(satisfy='tidak',1,0)) as tidak";
	$dbres->setDiagram(true, $diagram_view,null, new diagramadapter());
	
	$data=$dbres->command($_POST['command']);
	echo json_encode($data,JSON_NUMERIC_CHECK);
	return;
}

/*This is Modal Form and used for add and edit the table*/
$option=array(array("value"=>"%","name"=>"Semua"),array("value"=>"puas","name"=>"Puas"),array("value"=>"biasa","name"=>"Biasa"),array("value"=>"tidak","name"=>"Tidak Puas"));

$mcolumn=array('satisfy');
$mname=array("Kepuasan");
$mtype=array("select");
$mvalue=array($option);
$uitable->setModal($mcolumn, $mtype, $mname, $mvalue);
$modal=$uitable->getAdvanceModal();
$modal->setTitle("Satisfaction");

echo addCSS("framework/bootstrap/css/datepicker.css");
echo addCSS("framework/bootstrap/css/morris.css");
echo addJS("framework/jquery/raphael-min.js");
echo addJS("framework/bootstrap/js/morris.min.js");
echo addJS("framework/smis/js/table_action.js");
echo addJS("framework/smis/js/report_action.js");
echo addJS("framework/bootstrap/js/bootstrap-datepicker.js");


$r=new RowSpan();
$uitable->setHeightDiagram(260);
$r->addSpan($modal->getModalSkeleton(),5,RowSpan::$TYPE_HTML);
$r->addSpan($uitable->getDiagram("mychart"),7,RowSpan::$TYPE_HTML);
echo $r->getHtml();
echo $uitable->getHtml();
?>

<script type="text/javascript">

var indicator;
$(document).ready(function(){
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
	$('.mydate').datepicker();
	var column=new Array('satisfy');
	indicator=new ReportAction("indicator","smis-tools","report",column);
	indicator.setDiagramHolder("mychart");
	indicator.setXKey('Date');
	indicator.setYKey(['biasa','puas','tidak']);
	indicator.setLabels(["Biasa B","Puas S","Tidak S"]);
	
	indicator.sourcePieAdapter=function(json){
		var puas=0;
		var biasa=0;
		var tidak=0;
		$(json).each(function(idx, obj){
			puas+=obj.puas;
			biasa+=obj.biasa;
			tidak+=obj.tidak;
		});
		var so=[{label:"Puas",value:puas},{label:"Biasa",value:biasa},{label:"Tidak",value:tidak}];
		return so;
	};
	indicator.setTitle("Diagram Kepuasan");
});
</script>