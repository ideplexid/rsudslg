<?php
	global $PLUGINS;

	$init['name']='smis-tools';
	$init['path']=SMIS_DIR.$init['name']."/";
	$init['description']="Tools - Tools yang mungkin berguna bagi para user, 
			seperti calculator, calendar, contact manager, 
			file explorer, google maps , email, push calendar ";
	$init['require']="administrator";
	$init['service']="nothing";
	$init['version']="1.0.3";
	$init['number']="3";
	$init['type']="";
	
	$myplugins=new Plugin($init);
	$PLUGINS[$init['name']]=$myplugins;
?>