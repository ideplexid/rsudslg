<?php 

	global $NAVIGATOR;
	$mr = new Menu("fa fa-wrench");
	$mr->addProperty('title','Peralatan');
	$mr->addProperty('name','Peralatan');		
	$mr->addSubMenu("Calculator","smis-tools","calculator","Kalkulator Sederhana","fa fa-calculator");	
	$mr->addSubMenu("Calendar","smis-tools","kalender","Kalender","fa fa-calendar");
	$mr->addSubMenu ( "Share Data", "smis-tools", "share", "Share DATA" ,"fa fa-file-text");
	$mr->addSubMenu ( "Print", "smis-tools", "print", "Print" ,"fa fa-file-text");
	$mr->addSubMenu ( "Error Test", "smis-tools", "error", "Error Test" ,"fa fa-exclamation");
	$mr->addSubMenu ( "Draw Canvas", "smis-tools", "drawcanvas", "Drawing Canvas" ,"fa fa-paint-brush");
	$mr->addSeparator();
	$mr->addSubMenu("Satisfy Collector","smis-tools","indicator","Index Kepuasan","fa fa-files-o");
	$mr->addSubMenu("Satisfy Statistic","smis-tools","report","Kalender","fa fa-line-chart");
	$NAVIGATOR->addMenu($mr,'smis-tools');
?>
