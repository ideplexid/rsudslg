<?php
global $wpdb;
global $NUMBER;

if($NUMBER<2){
	$query="CREATE TABLE IF NOT EXISTS `smis_tool_print` (
		  `id` int(11) NOT NULL AUTO_INCREMENT,
		  `grup` varchar(32) NOT NULL,
		  `html` text NOT NULL,
		  `tanggal` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
		  `nama` varchar(32) NOT NULL,
		  `prop` varchar(10) NOT NULL,
		  PRIMARY KEY (`id`)
		) ENGINE=InnoDB ;";
	$wpdb->query ( $query );	
}

if($NUMBER<3){
	$query="alter table smis_tool_print rename smis_tools_print ;";
	$wpdb->query ( $query );
}

?>