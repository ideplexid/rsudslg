<?php
global $user;
global $db;
require_once 'smis-tools/kelas/NotificationTable.php';
$head=array ('Waktu','Jenis','Keterangan');
$uitable = new NotificationTable ( $head, "",$user->getUsername());
$uitable->setName ( "notification" );

if (isset ( $_POST ['command'] )) {
	$adapter = new SimpleAdapter ();
	$adapter->add ( "Waktu", "tanggal","date d M Y H:i" );
	$adapter->add ( "Jenis", "slug" );
	$adapter->add ( "Keterangan", "message" );
	$adapter->add ( "readby", "readby" );
	$adapter->add ( "page", "page" );
	$adapter->add ( "action", "action" );
	$dbtable = new DBTable ( $db, "smis_base_notification" );
	$dbtable->addCustomKriteria("user ", " LIKE '%".$user->getUsername()."%' ");
	$dbtable->setOrder("id DESC ");
	$dbres = new DBResponder ( $dbtable, $uitable, $adapter );
	if($dbres->is("ignore")){
		$query="UPDATE smis_base_notification set readby=concat(readby,' ".$user->getUsername()."') WHERE id='".$_POST['id']."'";
		$db->query($query);
		$r=new ResponsePackage();
		$r->setAlertContent("Saved", "Data Saved", ResponsePackage::$TIPE_INFO);
		$r->setStatus(ResponsePackage::$STATUS_OK);
		$r->setContent("");
		echo json_encode($r->getPackage());
		return;
	}
	
	if($dbres->is("ignoreall")){
		$query="UPDATE smis_base_notification 
				set readby=concat(readby,' ".$user->getUsername()."') 
				WHERE ( user LIKE '%".$user->getUsername()." %' OR  
				user LIKE '%".$user->getUsername()."' )				
				AND ( readby NOT LIKE '%".$user->getUsername()." %' AND readby NOT LIKE '%".$user->getUsername()."' )";
		$db->query($query);
		$r=new ResponsePackage();
		$r->setAlertContent("Saved", "Data Saved", ResponsePackage::$TIPE_INFO);
		$r->setStatus(ResponsePackage::$STATUS_OK);
		$r->setContent("");
		echo json_encode($r->getPackage());
		return;
	}
	
	$data = $dbres->command ( $_POST ['command'] );
	echo json_encode ( $data );
	return;
}

$uitable->addModal ( "id", "hidden", "", "" );
$uitable->addModal ( "nama", "text", "Nama", "" );
$uitable->addModal ( "slug", "text", "Slug", "" );
$uitable->addModal ( "keterangan", "textarea", "Keterangan", "" );
$modal = $uitable->getModal ();
$modal->setTitle ( "notification" );

echo $uitable->getHtml ();
echo $modal->getHtml ();
echo addJS ( "framework/smis/js/table_action.js" );

?>
<script type="text/javascript">
var notification;
$(document).ready(function(){
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
	var column=new Array();
	notification=new TableAction("notification","smis-tools","notification",column);
	notification.ignore=function(id){
		var d=this.getRegulerData();
		d['command']='ignore';
		d['id']=id;
		showLoading();
		$.post("",d,function(res){
			var json=getContent(res);
			notification.view();
			dismissLoading();
		});
	}

	notification.open=function(id,page,action){
		var d=this.getRegulerData();
		d['id']=id;
		d['command']='ignore';
		showLoading();
		$.post("",d,function(res){
			var json=getContent(res);
			$("#"+page+"__"+action).trigger("click");
			dismissLoading();
		});
	}

	notification.ignoreall=function(){
		var d=this.getRegulerData();
		d['command']='ignoreall';
		showLoading();
		$.post("",d,function(res){
			var json=getContent(res);
			notification.view();
			dismissLoading();
		});
	}
	notification.view();	
});
</script>
