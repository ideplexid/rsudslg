<?php 
require_once 'smis-libs-class/MasterTemplate.php';
global $db;
global $user;
$feedback = new MasterTemplate($db, "smis_base_feedback", "smis-tools", "feedback");
$feedback ->getDBtable() 
		  ->addCustomKriteria("user", "='".$user->getNameOnly()."'")
		  ->setOrder(" id DESC ");
$uitable  = $feedback->getUItable();
$uitable  ->setHeader(array("Waktu","Isi","Balas","Balasan"))
		  ->setDelButtonEnable(false)
		  ->setEditButtonEnable(true)
		  ->setPrintButtonEnable(false)
		  ->setReloadButtonEnable(false)
		  ->addModal("user", "hidden", "", $user->getNameOnly())
		  ->addModal("isi_tanya","textarea", "Tanya", "")
		  ->addModal("isi_balas","textarea", "Jawab", "","y",NULL,true)
		  ->addModal("id","hidden", "", "");
$adapter  = $feedback->getAdapter();
$adapter  ->add("Waktu", "tanggal_tanya","date d M Y H:i")
		  ->add("Isi","isi_tanya")
		  ->add("Balas", "tanggal_balas","date d M Y H:i")
		  ->add("Balasan", "isi_balas");
$feedback ->setModalTitle("Feedback");
$feedback ->initialize();
?>