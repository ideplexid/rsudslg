<?php
/**
 * this is handling about sharing data management
 * @author goblooge
 *
 */

global $db;
global $user;
$header=array ("User",'Tanggal','Nama','Keterangan','File');
$uitable = new Table ( $header, "Sharing Data", NULL, true );
$uitable->setName ( "share" );
/* this is respond when system have to response */
if (isset ( $_POST ['command'] )) {
	class myadapter extends ArrayAdapter {
		public function adapt($onerow) {
			$array = array ();
			$array ['id'] = $onerow->id;
			$array ['Nama'] = $onerow->nama;
			$array ['User'] = $onerow->user;
			$array ['Tanggal'] = self::format ( "date d M Y", $onerow->waktu );
			$array ['Keterangan'] = $onerow->keterangan;
			$link = "<a href='smis-upload/" . $onerow->file . "' class='btn'>Download</a>";
			$array ['File'] = $link;
			return $array;
		}
	}
	$adapter = new myadapter ();	
	$dbtable = new DBTable ( $db, "smis_tools_share");
	$dbres = new DBResponder ( $dbtable, $uitable, $adapter );
	$data = $dbres->command ( $_POST ['command'] );
	echo json_encode ( $data );
	return;
}



$uitable->addModal("id", "hidden", "", "");
$uitable->addModal("nama", "text", "Nama", "");
$uitable->addModal("keterangan", "textarea", "Keterangan", "");
$uitable->addModal("file", "files-single-all", "File", "");
$uitable->addModal("user", "hidden", "", $user->getUsername());
$modal = $uitable->getModal ();
$modal->setTitle ( "Document" );

echo $uitable->getHtml ();
echo $modal->getHtml ();
echo addJS ( "framework/bootstrap/js/bootstrap-datepicker.js" );
echo addJS ( "framework/smis/js/table_action.js" );
echo addCSS ( "framework/bootstrap/css/datepicker.css" );

?>
<script type="text/javascript">
var share;
$(document).ready(function(){
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
	$('#patient_datebirth').datepicker();
	var column=new Array('id','nama','keterangan','file','user');
	share=new TableAction("share","smis-tools","share",column);
	share.view();
});
</script>
