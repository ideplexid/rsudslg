<?php 
$uitable=new Table(array('Time','Comment','Satisfaction'),"Indicator Management", NULL,true);
$uitable->setName("indicator");
/*this is respond when system have to response*/
if(isset($_POST['command'])){	
	class myadapter extends ArrayAdapter{
		public function adapt($onerow){
			$array=array();
			$array['id']=$onerow->id;
			$array['Time']=self::format('date d-M-Y H:i:s', $onerow->waktu);
			$array['Comment']=$onerow->komentar;
			$array['Satisfaction']=$onerow->satisfy;				
			return $array;
		}
	}
	$adapter=new myadapter();	
	$dbtable=new DBTable($db, "smis_tools_satisfy",array('id','satisfy','komentar'));
	$dbtable->setOrder("waktu DESC");
	$dbres=new DBResponder($dbtable,$uitable,$adapter);	
	$data=$dbres->command($_POST['command']);
	echo json_encode($data);
	return;
}



$mcolumn=array('id','satisfy','komentar');
$mname=array("","","Masukan Kritik dan Saran Anda");
$mtype=array("hidden","hidden","textarea");
$mvalue=array("","","");

$puas=new Button("", "", "Puas");
$puas->setClass("btn-large btn-success");
$puas->setAction("kepuasan('puas')");

$biasa=new Button("", "", "Biasa");
$biasa->setClass("btn-large btn-warning");
$biasa->setAction("kepuasan('biasa')");

$tidak=new Button("", "", "Tidak");
$tidak->setClass("btn-large btn-danger");
$tidak->setAction("kepuasan('tidak')");

$btg=new ButtonGroup("newline");
$btg->addButton($puas);
$btg->addButton($biasa);
$btg->addButton($tidak);


$uitable->setModal($mcolumn, $mtype, $mname, $mvalue);
$modal=$uitable->getModal();
$modal->clearFooter();
$modal->addElement("Menurut Anda, Pelayanan Kami ?", $btg);
$modal->setModalSize(Modal::$FULL_MODEL);
$modal->setTitle("");
$modal->setAlwaysShow(true);

//echo $uitable->getHtml();
echo $modal->getHtml();
echo addJS("framework/smis/js/table_action.js");

?>
<script type="text/javascript">
function IndicatorAction(name,page,action,column){
	this.initialize(name, page, action, column);
}
IndicatorAction.prototype.constructor = IndicatorAction;
IndicatorAction.prototype=new TableAction();
IndicatorAction.prototype.save=function (){
	var self=this;
	var a=this.getSaveData();
	showLoading();
	$.ajax({url:"",data:a,type:'post',success:function(res){
		smis_alert("Terima Kasih", "Terima Kasih atas Kunjungan Anda, Kepuasan Anda menjadi Kebanggaan Kami", "alert-success");
		self.clear();
		dismissLoading();
	}});
};

var indicator;
$(document).ready(function(){
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
	var column=new Array('id','satisfy','komentar');
	indicator=new IndicatorAction("indicator","smis-tools","indicator",column);
	indicator.view();
});

function kepuasan(satisfy){
	$('#indicator_satisfy').val(satisfy);
	indicator.save();
}

</script>

<style type='text/css'>
#indicator_komentar{
	margin: 0px 0px 10px; width: 467px; height: 171px;
}

.smis_form label{
	width:100% !important;
}

.modal label{
	font-size:20px;
}

#indicator_komentar{
	width:917px;
}
</style>
