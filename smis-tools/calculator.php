<?php 
	echo addCSS("framework/bootstrap/css/bootstrap-calculator.css");
	echo addJS("framework/bootstrap/js/bootstrap-calculator.js");
?>
<div  id="calculator-wrapper">
        
      <div class="row-fluid">

        <div class="span6 well">
        	<legend>Safethree Calculator</legend>
        	<div class='row-fluid'>
          	<div class="span8">
            <div id="calculator-screen" class="uneditable-input"></div>
          </div>
          <div class="span1" style="text-align: center;">
            <div class="visible-phone">
              =
            </div>
            <div class="hidden-phone">
              =
            </div>
          </div>
          <div class="span3">
            <div id="calculator-result"  class="uneditable-input">0</div>
          </div>
        </div>
        	
          <div id="calc-board">
            <div class="row-fluid">
              <a href="#" class="btn btn-large btn-warning" data-constant="SIN">sin</a>
              <a href="#" class="btn btn-large btn-warning" data-constant="COS">cos</a>
              <a href="#" class="btn btn-large btn-warning" data-constant="MOD">md</a>
              <a href="#" class="btn btn-large btn-danger" data-method="reset">C</a>
            </div>
            <div class="row-fluid">
              <a href="#" class="btn btn-large">7</a>
              <a href="#" class="btn btn-large">8</a>
              <a href="#" class="btn btn-large">9</a>
              <a href="#" class="btn btn-large btn-inverse" data-constant="BRO">(</a>
              <a href="#" class="btn btn-large btn-inverse" data-constant="BRC">)</a>
            </div>
            <div class="row-fluid">
              <a href="#" class="btn btn-large">4</a>
              <a href="#" class="btn btn-large">5</a>
              <a href="#" class="btn btn-large">6</a>
              <a href="#" class="btn btn-large btn-success" data-constant="MIN">-</a>
              <a href="#" class="btn btn-large btn-success" data-constant="SUM">+</a>
            </div>
            <div class="row-fluid">
              <a href="#" class="btn btn-large">1</a>
              <a href="#" class="btn btn-large">2</a>
              <a href="#" class="btn btn-large">3</a>
              <a href="#" class="btn btn-large btn-success" data-constant="DIV">/</a>
              <a href="#" class="btn btn-large btn-success" data-constant="MULT">*</a>
            </div>
            <div class="row-fluid">
              <a href="#" class="btn btn-large btn-info">.</a>
              <a href="#" class="btn btn-large">0</a>
              <a href="#" class="btn btn-large btn-info" data-constant="PROC">%</a>
              <a href="#" class="btn btn-large btn-primary" data-method="calculate">=</a>
            </div>
          </div>
        </div>

        <div class="span6 well">
          
          <div>
          <legend>History</legend>
          <div id="calc-panel">
            <div id="calc-history">
              <ol id="calc-history-list"></ol>
            </div>
          </div>
          </div>
          
           </div>
          
        </div>

      </div>