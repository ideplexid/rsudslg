<?php 
	global $db;
	if(isset($_POST['command'])){
		$dbtable=new DBTable($db, "smis_tools_calendar");
		$adapter=new SimpleAdapter();
		$adapter->add("id","id");
		$adapter->add("title","event");
		$adapter->add("url","#","fix");
		$adapter->add("start","mulai","microsecond");
		$adapter->add("class","class");
		$adapter->add("end","selesai","microsecond");
		
		$uitable=new Table(array("id","title","url","class","start","end"), "");
		$uitable->setName("kalendar");
		$dbres=new DBResponder($dbtable, $uitable, $adapter);
		$dbres->setIncludeAdapterData(true);
		if($dbres->is("list")){
			$start=$_POST['mulai'];
			$end=$_POST['selesai'];
			$start= date("Y-m-d H:i:s",$start*1);
			$end= date("Y-m-d H:i:s",$end*1);
			$dbtable->addCustomKriteria("mulai", " >='".$start."'");
			$dbtable->addCustomKriteria( "smis_tools_calendar.mulai", " <='".$end."'");
			$dbtable->setShowAll(true);
		}
		$data=$dbres->command($_POST['command']);
		echo json_encode($data);
		return;
	}
	
	$option=new OptionBuilder();
	$option->add("Optional", "","");
	$option->add("Inverse", "event-inverse");
	$option->add("Info", "event-info");
	$option->add("Success", "event-success");
	$option->add("Special", "event-special");
	$option->add("Warning", "event-warning");
	$option->add("Important", "event-important");	
	
	$save=new Button("", "", "");
	$save->setIsButton(Button::$ICONIC);
	$save->setAction("kalender.save()");
	$save->setIcon("icon-white fa fa-save");
	$save->setClass("btn-primary");
	
	$smis_calendar_modal=new Modal("kalender_add_form", "smis-calendar", "kalender");
	$smis_calendar_modal->addModalElement("hidden", "", "", "id", "free", "y");
	$smis_calendar_modal->addModalElement("text", "", "Event Title", "event", "alphanumeric", "n");
	$smis_calendar_modal->addModalElement("datetime", date("Y-m-d H:i"), "Mulai", "mulai", "date", "n");
	$smis_calendar_modal->addModalElement("datetime", date("Y-m-d H:i"), "Selesai", "selesai", "date", "n");
	$smis_calendar_modal->addModalElement("select", $option->getContent(), "Label", "class", "", "");
	$smis_calendar_modal->addModalElement("summernote", "", "Deskripsi", "keterangan", "", "");
	$smis_calendar_modal->addFooter($save);
	$smis_show_events=new Modal("show_events", "", "");
	$smis_show_events->setModalClose(Modal::$MODAL_CLOSE_BOOTSTRAP);
	
	$ig=new InputGroup("");
	$st=new Text("kalender_kriteria", "", "");
	$ig->addComponent($st);
	
	$btn=new Button("", "", "Search");
	$btn->setIsButton(Button::$ICONIC);
	$btn->setIcon("icon-white ".Button::$icon_search);
	$btn->setAction("kalender.view()");
	$btn->setClass("btn-inverse");
	$ig->addComponent($btn);
	
	$btn=new Button("", "", "Add");
	$btn->setIsButton(Button::$ICONIC);
	$btn->setIcon("icon-white ".Button::$icon_plus_sign);
	$btn->setClass("btn-inverse");
	$btn->setAction("kalender.show_add_form()");
	$ig->addComponent($btn);
		
	
	$btgroup=new ButtonGroup("");
	$btn=new Button("prev_nav", "", "Prev");
	$btn->setIsButton(Button::$ICONIC);
	$btn->setIcon("icon-arrow-left icon-white");
	$btn->setClass(" btn-primary bnavigator");
	$btn->setAtribute("data-calendar-nav='prev'");
	$btgroup->addButton($btn);
	
	$btn=new Button("today_nav", "", "Today");
	$btn->setIsButton(Button::$ICONIC);
	$btn->setIcon("icon-minus icon-white");
	$btn->setClass(" btn-primary bnavigator");
	$btn->setAtribute("data-calendar-nav='today'");
	$btgroup->addButton($btn);
	
	$btn=new Button("next_nav", "", "Next");
	$btn->setAtribute("data-calendar-nav='next'");
	$btn->setIsButton(Button::$ICONIC);
	$btn->setIcon(" icon-arrow-right icon-white");
	$btn->setClass(" btn-primary bnavigator");
	$btgroup->addButton($btn);
	
	
	$btview=new ButtonGroup();
	$btn=new Button("prev_nav", "", "Tahunan");
	$btn->setIsButton(Button::$ICONIC_TEXT);
	$btn->setIcon("icon-book icon-white");
	$btn->setClass("  btn-info btview");
	$btn->setAtribute("data-calendar-view='year'");
	$btview->addButton($btn);
	
	$btn=new Button("today_nav", "", "Bulanan");
	$btn->setIsButton(Button::$ICONIC_TEXT);
	$btn->setIcon(" icon-calendar icon-white");
	$btn->setClass("  btn-info btview");
	$btn->setAtribute("data-calendar-view='month'");
	$btview->addButton($btn);
	
	$btn=new Button("next_nav", "", "Mingguan");
	$btn->setAtribute("data-calendar-view='week'");
	$btn->setIsButton(Button::$ICONIC_TEXT);
	$btn->setIcon("   icon-tasks icon-white");
	$btn->setClass(" btn-info btview");
	$btview->addButton($btn);
	
	$btn=new Button("next_nav", "", "Harian");
	$btn->setAtribute("data-calendar-view='day'");
	$btn->setIsButton(Button::$ICONIC_TEXT);
	$btn->setIcon(" icon-stop icon-white");
	$btn->setClass(" btn-info btview");
	$btview->addButton($btn);
	$btview->setMax(4, "Model");
	
	$time_tipe=new OptionBuilder();
	$time_tipe->add("", "one","1");
	$time_tipe->add("Harian", "day");
	$time_tipe->add("Mingguan", "week");
	$time_tipe->add("Bulanan", "month");
	$time_tipe->add("Tahunan", "year");
	$time=new Select("tipe_time", "", $time_tipe->getContent());
	
	
	$option->add("", "%","1");
	$tipe=new Select("tipe_user", "", $option->getContent());
	
	$dbtable=new DBTable($db, "smis_adm_user");
	$data=$dbtable->view("", "0");
	$dadapt=new SelectAdapter("realname", "username");
	$result=$dadapt->getContent($data['data']);
	
	$result[]=array("name"=>"","value"=>"%","default"=>"1");
	$user=new Select("kalender_user", "", $result);
	
	echo addCSS("framework/calendar/calendar.css");
	echo addJS("framework/calendar/underscore-min.js");
	echo addJS("framework/calendar/calendar.js");
	echo addJS("framework/smis/js/table_action.js");
	echo addCSS("framework/bootstrap/css/datepicker.css");
	echo addJS("framework/bootstrap/js/bootstrap-datepicker.js");
	echo addJS("framework/bootstrap/js/bootstrap-datetimepicker.js");
	echo addCSS("framework/bootstrap/css/bootstrap-datetimepicker.css");
	echo $smis_calendar_modal->getHtml();
	echo $smis_show_events->getHtml(); 
	
?>

<div class="page-header-calendar">
	<div class="pull-right form-inline">
		<?php echo $tipe->getHtml(); ?>
		<?php echo $user->getHtml();?>	
		<?php echo $ig->getHtml(); ?>
	</div>
	<div class='clear'></div>
	<div class="pull-right form-inline">
		<?php echo $time->getHtml(); ?>
		<?php echo $btgroup->getHtml(); ?>
		<?php  echo $btview->getHtml(); ?>	
	</div>
	
	<h3></h3>
</div>

<div id='wrapper_calender'>
	<div id='calendar' class='well'></div>
</div>



<script type="text/javascript">
	var options = {
			events_source: function(){ return getResource(); },
			view: 'month',
			modal: "#show_events",
			modal_type : "function",
			modal_data : function(id,e,mb){ 
					kalender.select(id);
				}, 
			modal_title : function (e) { return e.title; },
			tmpl_path:  "smis-framework/calendar/tmpls/",
			tmpl_cache: false,
			onAfterViewLoad: function(view) {
				$('.page-header h3').text(this.getTitle());
				$('.btn-group button').removeClass('active');
				$('button[data-calendar-view="' + view + '"]').addClass('active');
			},
			classes: {
				months: {
					general: 'label'
				}
			}
		};
	
	var calendar=null;
	var kalender;
		$(document).ready(function(){
			$('.mydate').datepicker();
			$('.mydatetime').datetimepicker({ minuteStep: 1});
			kalender=new TableAction("kalender","smis-tools","kalender",new Array("id","event","mulai","selesai","class","keterangan"));
			calendar = $("#calendar").calendar(options);
			kalender.view=function(){
				this.hide_add_form();
				calendar.view();
			};
			kalender.selected=function(json){
				var ket=json.keterangan;
				$("#show_events .modal-body").html(ket);
			};

			$('.bnavigator').each(function() {
				var $this = $(this);
				$this.click(function() {
					calendar.navigate($this.data('calendar-nav'));
				});
			});	
			$('.btview').each(function() {
				var $this = $(this);
				$this.click(function() {
					calendar.view($this.data('calendar-view'));
				});
			});	
			$('#first_day').change(function(){
				var value = $(this).val();
				value = value.length ? parseInt(value) : null;
				calendar.setOptions({first_day: value});
				calendar.view();
			});	
			$('#language').change(function(){
				calendar.setLanguage($(this).val());
				calendar.view();
			});	
			$('#events-in-modal').change(function(){
				var val = $(this).is(':checked') ? $(this).val() : null;
				calendar.setOptions({modal: val});
			});
			$('#show_events .modal-header, #show_events .modal-footer').click(function(e){
				//e.preventDefault();
				//e.stopPropagation();
			});
	});
	
	function getResource(){
		var nano=[];
		var data=kalender.getViewData();
		var date = new Date(), y = date.getFullYear(), m = date.getMonth();
		var firstDay = new Date(y, m, 1);
		var lastDay = new Date(y, m + 1, 1);
		var mulai=firstDay.getTime()/1000;
		var selesai=lastDay.getTime()/1000;
		if(calendar!=null){
			mulai=calendar.getStartDate().getTime()/1000;
			selesai=calendar.getEndDate().getTime()/1000;
		}
		data['mulai']=mulai;
		data['selesai']=selesai;
		$.ajax({
	        url: "",
	        type: 'POST',
	        data:data,
	        async: false,
	        cache: false,
	        timeout: 30000,
	        error: function(){
	            nano=[];
	        },
	        success: function(res){
		        var json=getContent(res);
		        if(json!=null){
			        nano=json.adapter;
			    }
	        }
	    });
	    return nano;
	}
</script>

<style>
		#wrapper_calender{	padding:60px !important;    }
		#calendar{	padding:0px !important;    }
</style>