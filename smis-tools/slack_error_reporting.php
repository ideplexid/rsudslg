<?php 
setChangeCookie(false);
global $db;
global $user;
require_once "smis-social-network/class/other/SlackMessage.php";
$res_php     = $_POST['res_php'];
$res_posting = $_POST['res_posting'];
$res_jscript = $_POST['res_jscript'];
$slack       = new SlackMessage($db);
$slack       ->addField("## Server Response ##",$res_php);
$slack       ->addField("## Data Post ##",$res_posting);
$slack       ->addField("## Javascript ##",$res_jscript);
$slack       ->setAuthorName($user->getNameOnly());
$slack       ->setTimeSecond();
$slack       ->setColor("#BCFFBC");
$slack       ->setTitle("Error Reporting on ".getSettings($db,"slack_name","smis"));
$result      = $slack->send();

$response=new ResponsePackage();
$response->setStatus(ResponsePackage::$STATUS_OK);
$response->setAlertVisible(true);
if($result){
    $response->setAlertContent("Send to Slack Success !!!","Sending to slack have been successfully" );
}else{
    $response->setAlertContent("Send to Slack Failed !!!","Sending to slack have failed successfully, check the slack setting in smis-social-network",ResponsePackage::$TIPE_DANGER );    
}
echo json_encode($response->getPackage());
?>