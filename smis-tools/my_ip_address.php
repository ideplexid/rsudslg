<?php
$externalContent = file_get_contents('http://checkip.dyndns.com/');
preg_match('/Current IP Address: \[?([:.0-9a-fA-F]+)\]?/', $externalContent, $m);
$externalIp      = $m[1];

global $db;
global $user;
require_once "smis-social-network/class/other/SlackMessage.php";
$slack       = new SlackMessage($db);
$slack       ->setAuthorName($user->getNameOnly());
$slack       ->setTimeSecond();
$slack       ->setColor("#BCFFBC");
$slack       ->setTitle(getSettings($db,"slack_name","smis")." - Public IP Adress : ".$externalIp);
$result      = $slack->send();

$response=new ResponsePackage();
$response->setStatus(ResponsePackage::$STATUS_OK);
$response->setAlertVisible(true);
if($result){
    $response->setAlertContent("Send to Slack Success !!!","Sending to slack have been successfully" );
}else{
    $response->setAlertContent("Send to Slack Failed !!!","Sending to slack have failed successfully, check the slack setting in smis-social-network",ResponsePackage::$TIPE_DANGER );    
}
echo json_encode($response->getPackage());

?>