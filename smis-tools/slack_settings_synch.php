<?php 
setChangeCookie(false);
global $db;
global $user;
require_once "smis-social-network/class/other/SlackMessage.php";
$settings    = $_POST['settings'];
$menu_loc    = $_POST['menu'];

$slack       = new SlackMessage($db);
foreach($settings as $key=>$value){
    $slack       ->addField($key,$value);
}

$slack       ->setAuthorName($user->getNameOnly());
$slack       ->setTimeSecond();
$slack       ->setColor("#dff0d8");
$slack       ->setTitle("Setting Menu ( ".$menu_loc." ) on  ".getSettings($db,"slack_name","smis"));
$result      = $slack->send();

$response=new ResponsePackage();
$response->setStatus(ResponsePackage::$STATUS_OK);
$response->setAlertVisible(true);
if($result){
    $response->setAlertContent("Send to Slack Success !!!","Sending to slack have been successfully",ResponsePackage::$TIPE_SUCCESS );
}else{
    $response->setAlertContent("Send to Slack Failed !!!","Sending to slack have failed successfully, check the slack setting in smis-social-network",ResponsePackage::$TIPE_DANGER );    
}
echo json_encode($response->getPackage());
?>