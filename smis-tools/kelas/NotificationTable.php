<?php 

class NotificationTable extends Table{
	private $user;
	public function __construct($header, $title,$user){
		parent::__construct($header, $title,NULL,true);
		$this->setAddButtonEnable(false);
		$this->setDelButtonEnable(false);
		$this->setPrintButtonEnable(false);
		$this->setEditButtonEnable(false);
		$this->setReloadButtonEnable(false);
		$this->user=$user;
		
		$btn=new Button("", "", "Ignore All");
		$btn->setIsButton(Button::$ICONIC_TEXT);
		$btn->setIcon("fa fa-thumb-tack");
		$btn->setClass("btn-primary");
		$btn->setAction("notification.ignoreall()");
		$this->addHeaderButton($btn);
	}

	public function getContentButton($id){
		$btg=new ButtonGroup("");
		$readby=$this->current_data['readby'];
		if(strpos($readby, $this->user)!==false){
			return new Label("", "", "<i class='maincolorbtn fa fa-check'></i>");
		}else{
			$btn=new Button("", ""," Ignore ");
			$btn->setIsButton(Button::$ICONIC_TEXT);
			$btn->setIcon("fa fa-thumb-tack");
			$btn->setClass("btn-primary");
			$btn->setAction($this->action.".ignore('".$id."')");
			$btg->addButton($btn);
			
			$btn=new Button("", "","Open");
			$btn->setIsButton(Button::$ICONIC_TEXT);
			$btn->setIcon("fa fa-book");
			$btn->setClass("btn-info");
			$btn->setAction($this->action.".open('".$id."','".$this->current_data['page']."','".$this->current_data['action']."')");
			$btg->addButton($btn);
		}
		return $btg;
	}

}

?>