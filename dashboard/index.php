<?php
	global $db;
	global $user;
	require_once 'smis-libs-class/Policy.php';
	require_once 'smis-libs-inventory/policy.php';
	$inventory_policy = new InventoryPolicy("gizi", $user);
	
	$policy=new Policy("dashboard", $user);
	$policy->setDefaultPolicy(Policy::$DEFAULT_POLICY_RESTRICT);
	$policy->addPolicy("dashboard_pendaftaran","dashboard_pendaftaran",Policy::$DEFAULT_COOKIE_CHANGE,"modul/dashboard_pendaftaran");
	$policy->initialize();
?>