<?php
	global $NAVIGATOR;

	$mr = new Menu ("fa fa-area-chart");
	$mr->addProperty ( 'title', 'Dashboard' );
	$mr->addProperty ( 'name', 'Dashboard' );

	$mr->addSubMenu ( "Pendaftaran", "dashboard", "dashboard_pendaftaran", "Pendaftaran" ,"fa fa-users");
	$mr->addLaravel("Pelayanan","dashboard","pelayanan","","fa fa-user");
	$mr->addLaravel("Antrian","dashboard","antrian","","fa fa-list");
	$NAVIGATOR->addMenu ( $mr, 'dashboard' );

