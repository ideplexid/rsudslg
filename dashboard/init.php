<?php
	global $PLUGINS;
	
	$init['name']        = "dashboard";
	$init['path']        = SMIS_DIR . $init['name'] . "/";
	$init['description'] = "Dashboard";
	$init['require']     = "administrator";
	$init['service']     = "";
	$init['version']     = "1.0.0";
	$init['number']      = "1";
	$init['type']        = "";
	
	$plugin = new Plugin($init);
	$PLUGINS[$init['name']] = $plugin;
?>