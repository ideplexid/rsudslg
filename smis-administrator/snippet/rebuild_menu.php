<?php
global $db;
require_once "smis-framework/smis/ui/composite/Navigator.php";
	require_once "smis-framework/smis/ui/composite/Menu.php";
	require_once "smis-framework/smis/api/Plugin.php";
	require_once "smis-framework/smis/log/User.php";
	require_once "smis-base/smis-base.php";
    
$dbtable = new DBTable($db,"smis_adm_menu");
$dbtable ->truncate();

global $NAVIGATOR;
$data 			= $NAVIGATOR->getMenus();
foreach ( $data as $key=>$menu ) {
    

    $menu_name 			= $menu->getName ();
    $menus_component 	= $menu->getComponent ();
    $menu_key 		    = $menu->getPage ();
    $href               = $menu->getHref()=="javascript:;"?"0":"1";
    $sub = array();
    $submenu_implement = "";
    foreach($menus_component as $submenu){
        $submenu_name 	= $submenu->getName();
        $submenu_key 	= $submenu->getAction();
        $submenu_implement = $submenu->getPrototypeImplement();
        $is_laravel = $submenu->getClass() == "smis_action" ? 0 : 1;
        $sub[] = array("name"=>$submenu_name,"slug"=>$submenu_key,"is_laravel"=>$is_laravel);
    }

    $data = array();
    $data['menu'] = $key;
    $data['menu_alias'] = $menu_name;
    $data['sub_menu'] = json_encode($sub);
    $data['prototype'] = $submenu_implement;
    $data['is_laravel'] = $href;
    $dbtable->insert($data);
}

$response = new ResponsePackage();
$response ->setStatus(ResponsePackage::$STATUS_OK);
$response ->setAlertVisible(true);
$response ->setAlertContent("Menu Disimpan","");

echo json_encode($response->getPackage());
