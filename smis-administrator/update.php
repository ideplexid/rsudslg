<?php
    global $wpdb;
    require_once "smis-libs-class/DBCreator.php";
    require_once "smis-administrator/resource/install/table/smis_adm_cronjob.php";
    require_once "smis-administrator/resource/install/table/smis_adm_directory.php";
    require_once "smis-administrator/resource/install/table/smis_adm_entity_response.php";
    require_once "smis-administrator/resource/install/table/smis_adm_error.php";
    require_once "smis-administrator/resource/install/table/smis_adm_log.php";
    require_once "smis-administrator/resource/install/table/smis_adm_prototype.php";
    require_once "smis-administrator/resource/install/table/smis_adm_service_requests.php";
    require_once "smis-administrator/resource/install/table/smis_adm_service_responds.php";
    require_once "smis-administrator/resource/install/table/smis_adm_settings.php";
    require_once "smis-administrator/resource/install/table/smis_adm_scheduled.php";
    require_once "smis-administrator/resource/install/table/smis_adm_upload.php";
    require_once "smis-administrator/resource/install/table/smis_adm_user.php";
    require_once "smis-administrator/resource/install/table/smis_base_notification.php";
    require_once "smis-administrator/resource/install/table/smis_adm_synch_provider.php";
    require_once "smis-administrator/resource/install/table/smis_adm_synch_consumer.php";
    require_once "smis-administrator/resource/install/table/smis_adm_log_synch_provider.php";
    require_once "smis-administrator/resource/install/table/smis_adm_log_synch_consumer.php";
    require_once "smis-administrator/resource/install/table/smis_adm_menu.php";
    
    require_once "smis-administrator/resource/install/view/smis_table.php";

?>