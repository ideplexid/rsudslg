<?php 
	/**
    *  this is default page when the requested page not found !!
    *  user can change this as long as still using smis alert alert
    */
	$error_msg="The Page You Want to Access was Not Found";				//you can change the content as you like, html code permited
	$error_title="This Page Not Found ";								//you can change ther title as you like, html code permited
	setChangeCookie(false);
	$res=new ResponsePackage();
	$res->setWarning(true,$error_title, $error_msg);
	$res->setStatus(ResponsePackage::$STATUS_FAIL);
	echo json_encode($res->getPackage());

?>