<?php 

function synch_match_build(DBController $wpdb,Database $db,DBTable $dbtable_synch,$code_name){
    $subject=$dbtable_synch->select(array("code_name"=>$code_name));
    $result=array();
    $result['prov_msg']="PAIR FOUND";
    if($subject==null){
        $result['prov_msg']="NO PAIR";
        return;
    }else{
        require_once "smis-libs-class/DBCreator.php";
        $dbcretor=new DBCreator($wpdb,$subject->table_name);
        $result['table_msg']="TABLE FOUND";
        $result['table_name']=$subject->table_name;
        if(!$dbcretor->isExist()){
            $result['table_msg']="NO TABLE";
        }else{
            $index_db=$dbcretor->getBuildDBIndexInformation();
            $column_db=$dbcretor->getDescription();
            
            ksort($index_db,SORT_STRING);
            ksort($column_db,SORT_STRING);
            
            $result['table_index']=$index_db;
            $result['table_column']=$column_db;
            
            $dbtable=new DBTable($db,$subject->table_name);        
            $result['table_data']=$dbtable->count("");
            $dbtable->addCustomKriteria(" origin ","=''");
            $result['table_origin_kosong']=$dbtable->count("");        
            $dbtable->addCustomKriteria(" origin_id ","=0");
            $result['table_origin_id_kosong']=$dbtable->count("");        
            $dbtable->setCustomKriteria(array());
            $dbtable->addCustomKriteria(" origin_id ","=0");
            $result['table_origin_id']=$dbtable->count("");        
            $result['table_valid']=($result['table_data']+$result['table_origin_id_kosong'])-($result['table_origin_id']+$result['table_origin_kosong']);
            $dbtable->setCustomKriteria(array());
            $dbtable->addCustomKriteria(" autonomous "," LIKE '%[".$subject->autonomous."]%' ");
            $result['table_synched']=$dbtable->count("");
            $result['table_unsynched']=$result['table_data']-$result['table_synched'];
        }
    }
    return $result;
}

?>