<?php
require_once ("smis-framework/smis/ui/composite/Navigator.php");
require_once ("smis-framework/smis/ui/composite/Menu.php");
require_once ("smis-framework/smis/api/Plugin.php");
require_once ("smis-base/smis-base.php");
show_error();
global $wpdb;
global $PLUGINS;

/* sorting the plugins */
$plug = array ();
$smisp = array();
foreach ( $PLUGINS as $pn ) {
	$pn_name=$pn->getName();
	if(startsWith($pn_name, "smis-"))
		$smisp[$pn_name] = $pn;
	else $plug [$pn_name] = $pn;
}
krsort($smisp);
ksort ( $plug );
foreach($smisp as $pn){
	array_unshift($plug , $pn);
}

/* end sorting the plugins */
if (isset ( $_POST ['command'] )) {
	if ($_POST ['command'] == "list") {
		$list = "";
		foreach ( $plug as $key => $p ) {
			$status 	= "Activated";
			$status 	= $p->getStatus ();
			$path 	= substr($p->getPath (),0,-1);
			$class 	= $p->getName ();
			$check 	= new CheckBox ( "", "", false );
			$check->setAtribute ( "plugins='" . $path . "'" );
			$check->setString ( "hide", "show", "detail" );			

			$compatbl_button 	= new Button("","test_compatible","Test");
			$compatbl_button 	->setAction("compatible('".$path."')")
					->setIcon("fa fa-check")
					->setIsButton(BUtton::$ICONIC_TEXT)
					->addClass("btn-danger")
					->addAtribute("data-plugins",$path);

			$pull_button 	= new Button("","pull_request","Pull");
			$pull_button 	->setAction("pull('".$path."')")
					->setIcon("fa fa-bitbucket")
					->setIsButton(BUtton::$ICONIC_TEXT)
					->addClass("btn-info")
					->addAtribute("data-plugins",$path);

			$pname=startsWith($class, "smis")?"<strong><i class='fa fa-asterisk'></i> ".str_replace("-", " ", $class)."</strong>":$class;
			
			$list .= "<tr>";
			$list .= "<td rowspan='1' class='" . $class . "-parent' >" . strtoupper ( str_replace ( "_", " ", $pname) ) . "</td>";
			$list .= "<td>" . $status ['status'] . "</td>";
			$list .= "<td>Version " . $p->getCurrentVersion () . " </td>";
			$list .= "<td>" . $status ['button'] . "</td>";
			$list .= "<td>" . $compatbl_button->getHtml () ."</td>";
			$list .= "<td>" . (file_exists($path."/.git/config") || $path=="smis-administrator"?($pull_button->getHtml ()):"") ."</td>";
			$list .= "<td>" . $check->getHtml () ."</td>";
			$list .= "</tr>";
			
			$list .= "<tr class=' detail " . $class . "'>";
			$list .= "<td>Description: </td>";
			$list .= "<td colspan='10'>" . $p->getDescription () . "</td>";
			$list .= "</tr>";
			
			$list .= "<tr class=' detail " . $class . "'>";
			$list .= "<td>Path: </td>";
			$list .= "<td colspan='10'>" . $p->getPath () . "</td>";
			$list .= "</tr>";
			
			$list .= "<tr class=' detail " . $class . "'>";
			$list .= "<td>Require: </td>";
			$list .= "<td colspan='10'>" . $p->getRequire () . "</td>";
			$list .= "</tr>";
			
			$list .= "<tr class=' detail " . $class . "'>";
			$list .= "<td>Service: </td>";
			$list .= "<td colspan='10'>" . $p->getService () . "</td>";
			$list .= "</tr>";
		}
		
		$user = new User ();
		$user->loadUser ( getSession('user') );
		$NAVIGATOR->setUser ( $user->getCapability (), json_decode ( $user->getMenu (), true ) ,$user);
		
		$json ['list'] = $list;
		echo json_encode ( $json );
	} else if ($_POST ['command'] == "clean") {
		clean_plugins ( $db );
		$pack 	= new ResponsePackage ();
		$pack	->setContent ( 1 )
			->setStatus ( ResponsePackage::$STATUS_OK )
			->setAlertVisible ( false )
			->setAlertContent ( "Cleaned Success", "Your Plugins Have Been Cleaned", ResponsePackage::$TIPE_INFO);
		echo json_encode ( $pack->getPackage () );
	}else if ($_POST ['command'] == "clean_database") {
		$database	= array();
		$database[]	= "smis_adm_error";
		$database[]	= "smis_adm_log";
		$database[]	= "smis_adm_entity_response";
		$database[]	= "smis_adm_service_responds";
		$database[]	= "smis_adm_service_requests";
		$database[]	= "smis_adm_cronjob";
		$database[]	= "smis_sb_log";
        		$dbtable		= new DBTable($db,"");
		foreach($database as $d){
			$dbtable->setName($d);
			$dbtable->truncate();
		}		
		$pack 	= new ResponsePackage ();
		$pack	->setContent(1)
			->setStatus(ResponsePackage::$STATUS_OK)
			->setAlertVisible(false)
			->setAlertContent("Cleaned Success", "Your Log Database is Cleaned", ResponsePackage::$TIPE_INFO);
		echo json_encode ( $pack->getPackage () );
	}else if ($_POST ['command'] == "compatible") {
		require_once "smis-administrator/function/compatible.php";
		$result 	= compatible_test($_POST['plugins'],$PLUGINS);
		$pack 	= new ResponsePackage ();
		$pack	->setContent(1)
			->setStatus(ResponsePackage::$STATUS_OK)
			->setWarning(true,"Testing Result",$result)
			->setAlertContent("Cleaned Success", "Your Log Database is Cleaned", ResponsePackage::$TIPE_INFO);
		echo json_encode ($pack->getPackage());
	}else if ($_POST ['command'] == "compatible_all") {
		require_once "smis-administrator/function/compatible.php";
		$result = "";
		foreach ( $PLUGINS as $pn ) {
			$path 	= substr($pn->getPath (),0,-1);
			$result .= compatible_test($path,$PLUGINS);
		}		
		$pack 	= new ResponsePackage ();
		$pack	->setContent(1)
			->setStatus(ResponsePackage::$STATUS_OK)
			->setWarning(true,"Testing All Plugins Result",$result);
		echo json_encode ($pack->getPackage());
	} else {
		$name 	= $_POST ['name'];
        		$plugin 	= $PLUGINS [$name];
		$command = $_POST ['command'];
		if ($command == "activate")
			$plugin->setActivation ( "true" );
		else if ($command == "deactivate")
			$plugin->setActivation ( "false" );
		else if ($command == "install")
			$plugin->install ();
		else if ($command == "uninstall")
			$plugin->uninstall ();
		else if ($command == "update")
			$plugin->update ();
		else if ($command == "repair")
			$plugin->repair ();
		$pack = new ResponsePackage ();
		$pack->setContent ( 1 );
		$pack->setStatus ( ResponsePackage::$STATUS_OK );
		$pack->setAlertVisible ( false );
        		$message=$plugin->getProcessMessage();
		if($message!=""){
            		$pack->setWarning(true,"Process Message",$message);
            		$pack->setContent ( 0 );
        		}
        		echo json_encode ( $pack->getPackage () );
	}
	return;
}

$clean_plugin 	= new Button("","","Clean Plugin");
$clean_plugin	->setIsButton(Button::$ICONIC_TEXT)
		->setClass("btn-primary")
		->setIcon("fa fa-archive")
		->setAction("clean()");

$clean_database	= new Button("","","Clean Database");
$clean_database	->setIsButton(Button::$ICONIC_TEXT)
		->setClass("btn-inverse")
		->setIcon("fa fa-database")
		->setAction("clean_database()");

$test_all	= new Button("","","Test All Plugins");
$test_all	->setIsButton(Button::$ICONIC_TEXT)
		->setClass("btn-danger")
		->setIcon("fa fa-check")
		->setAction("compatible_all()");


$build_menu 	= new Button("","","Build Menu");
$build_menu	    ->setIsButton(Button::$ICONIC_TEXT)
                ->setClass("btn-info")
                ->setIcon("fa fa-list-alt")
                ->setAction("rebuild_menu()");
                        
echo "<div class='smis-table-container'>";
	echo '<table class="plugintable table table-bordered table-striped table-hover table-condensed">';
		echo "<caption>Administrator : Plugin</caption>";
		echo "<thead>";
			echo "<tr>";
				echo "<th>Name</th>";
				echo "<th colspan='10'>".$clean_plugin->getHtml()." ".$clean_database->getHtml()." ".$test_all->getHtml()." ".$build_menu->getHtml()."</th>";
			echo "</tr>";
		echo "</thead>";
		echo "<tbody id='list'>";
		echo "</tbody>";
	echo "</table>";
echo "</div>";

echo addCSS 	("framework/bootstrap/css/bootstrap-switch.css" );
echo addJS 	("framework/bootstrap/js/bootstrap-switch.js" );
echo addJS	("administrator/resource/js/plugins.js");
echo addCSS	("administrator/resource/css/plugins.css");
?>
