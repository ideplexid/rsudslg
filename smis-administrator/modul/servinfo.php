<?php 


$tabulator=new Tabulator("servinfo", "Server Information");
$tabulator->add("phpinfo", "PHP", "",Tabulator::$TYPE_HTML,"fa fa-cloud-upload");
$tabulator->add("manual_service", "Manual Service", "",Tabulator::$TYPE_HTML,"fa fa-newspaper-o");
$tabulator->add("hardinfo", "Hardware", "",Tabulator::$TYPE_HTML,"fa fa-desktop");
$tabulator->setPartialLoad(true, "smis-administrator", "servinfo", "servinfo",true);
$tabulator->setPageName("smis_administrator");
echo $tabulator->getHtml();

?>