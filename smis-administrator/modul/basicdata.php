<?php

$tabs=new Tabulator("feednotif", "feednotif",Tabulator::$LANDSCAPE_RIGHT);
echo $tabs	->add("feedback_tab", "Feedback", "smis-administrator/resource/php/basicdata/feedback.php",Tabulator::$TYPE_INCLUDE," fa fa-envelope")
			->add("notif_tab", "Notification", "smis-administrator/resource/php/basicdata/admin_notification.php",Tabulator::$TYPE_INCLUDE," fa fa-lightbulb-o")
			->add("direct", "Direct Access", "smis-administrator/resource/php/basicdata/direct.php",Tabulator::$TYPE_INCLUDE," fa fa-lightbulb-o")
			->add("upload_manager", "Uploaded Files", "smis-administrator/resource/php/basicdata/upload.php",Tabulator::$TYPE_INCLUDE,"fa fa-upload")
            ->add("scheduled", "Scheduled Job", "smis-administrator/resource/php/basicdata/scheduled.php",Tabulator::$TYPE_INCLUDE,"fa fa-calendar-check-o")
            ->add("synchronize_consumer", "Synch Consumer", "smis-administrator/resource/php/basicdata/synchronize_consumer.php",Tabulator::$TYPE_INCLUDE,"fa fa-cloud-upload")
            ->add("synchronize_provider", "Synch Provider", "smis-administrator/resource/php/basicdata/synchronize_provider.php",Tabulator::$TYPE_INCLUDE,"fa fa-cloud-download")
            ->add("synchronize_settings", "Settings Synch", "smis-administrator/resource/php/basicdata/synchronize_settings.php",Tabulator::$TYPE_INCLUDE,"fa fa-cog")
            ->getHtml();

?>