<?php

require_once 'smis-framework/smis/api/SettingsBuilder.php';
global $db;
$smis = new SettingsBuilder($db, "smis_settings", "smis-administrator", "settings" );
$smis ->setShowDescription(true );
$smis ->setTabulatorMode(Tabulator::$POTRAIT );
$name = curPageURL ();
if(is_multisite()){
	$name  = getFolderUrl($name);
	$name .= "/smis-multisite/serverbus/smis-service-".MULTISITE.".php";
}

$smis->addTabs("system", "System"," fa fa-steam" );
if($smis->isGroup("system")){
    $rounding = new OptionBuilder();
    $rounding ->add("Round","round","1")
              ->add("Ceil","ceil")
              ->add("Floor","floor");
    
    $smis->addItem("system", new SettingsItem($db, "smis_serverbus_url", "Server Bus URL", $name, "text", "Serverbus URL of this system" ) );
    $smis->addItem("system", new SettingsItem($db, "smis_serverbus_key", "Server Bus Key", 'smis-key-serverbus', "text", "Serverbus Key of this system" ) );
    $smis->addItem("system", new SettingsItem($db, "smis_autonomous_key", "Autonomous Key", 'smis-key-autonomous', "text", "Autonomous Key of this system" ) );
    $smis->addItem("system", new SettingsItem($db, "smis_autonomous_name", "Autonomous Name", "SMIS", "text", "Autonomous Name of this system" ) );
    $smis->addItem("system", new SettingsItem($db, "smis_autonomous_id", "Autonomous ID", "smis", "text", "Autonmous ID of this system" ) );
    $smis->addItem("system", new SettingsItem($db, "smis_autonomous_timezone", "Autonomous Time Zone", "Asia/Jakarta", "text", "Timezone of Autonomous <a href='#' onclick=\"help('smis-administrator','timezone')\">(See Help)</a> " ) );
    $smis->addItem("system", new SettingsItem($db, "smis-simple-rounding-uang", "Rounding Uang Hingga","", "text", "Nilai Rounding -1 untuk tidak di rounding"));
    $smis->addItem("system", new SettingsItem($db, "smis-simple-rounding-model", "Model Rounding yang dipilih",$rounding->getContent(), "select", "Pilih Salah Satu Model Pembulatan (Ceil - Selalu keatas, Floor - Selalu Kebawah, Round - x >= 5 Keatas, x < 5 kebawah) "));
}

$smis->addTabs("running", "Customize","fa fa-yelp" );
if($smis->isGroup("running")){
    $realtime = new OptionBuilder ();
    $realtime->add("Super Computer (30 Second)", "30000", "0" );
    $realtime->add("Server PC (1 Minute)", "60000", "0" );
    $realtime->add("Desktop PC (3 Minute)", "180000", "1" );
    $realtime->add("Old PC (5 Minute)", "300000", "1" );

    $timout = new OptionBuilder ();
    $timout->add("Fast (5 minutes)", "400000", "0" );
    $timout->add("Middle (10 minutes)", "700000", "1" );
    $timout->add("Slow (15 minutes)", "1000000", "0" );

    $nption = new OptionBuilder ();
    $nption->add("Super Computer (1 Hour)", "1", "0" );
    $nption->add("Server PC (5 Hour)", "5", "0" );
    $nption->add("Desktop PC (12 Hour)", "12", "0" );
    $nption->add("Old PC (24 Hour)", "24", "1" );
    $nption->add("Every Time (Every Request, Testing Purpose)", "0", "0" );
    $nption->add("No Time (Disabled)", "-1", "0" );


    $smis->addItem("running", new SettingsItem($db, "smis-enable-registration", "Enable Registration", "0", "checkbox", "Enabled Other User to Register" ) );
    $smis->addItem("running", new SettingsItem($db, "smis-enable-realtime", "Enable Realtime", "0", "checkbox", "System Would Check to Server Regulary" ) );
    $smis->addItem("running", new SettingsItem($db, "smis-max-timout-ajax-request", "Max Ajax Time Out", "5000", "text", "Maximum time consume in milisecond of the ajax before define as Request Time Out (RTO), Do not fill less than 5000 ms" ) );
    
    
    $smis->addItem("running", new SettingsItem($db, "smis-realtime", "Realtime", $realtime->getContent (), "select", "Time Interval System Will Check Regulary" ) );
    $smis->addItem("running", new SettingsItem($db, "smis_autonomous_sound", "Enable Sound Notification", "0", "checkbox", "Sound When Notification Arrive (Must Enabled Realtime) " ) );
    $smis->addItem("running", new SettingsItem($db, "smis-enable-login-timeout", "Enabled Login Time Out", "1", "checkbox", "sistem akan otomatis logout jika tidak dipakai oleh user" ) );
    $smis->addItem("running", new SettingsItem($db, "smis-multisite-enabled", "Enable Multisite", "0", "checkbox", "Activated Site Map On Multisite" ) );
    $smis->addItem("running", new SettingsItem($db, "smis-login-timeout", "Time out", $timout->getContent (), "select", "waktu tunda sebelum user akan otomatis logout" ) );
    $smis->addItem("running", new SettingsItem($db, "smis-multiple-login", "Multiple Login", "0", "checkbox", "satu User bisa Login di beberapa komputer" ) );
    $smis->addItem("running", new SettingsItem($db, "smis-cronjob-time", "Cronjob Time", $nption->getContent (), "select", "System akan melakukan Cronjob dalam Periode Tertentu" ) );
    $smis->addItem("running", new SettingsItem($db, "smis-last-cronjob-time", "Last Cronjob Running", "", "text", "Terkhir Kali Kapan Cronjob Running , Format YYYY-MM-DD HH:ii:ss example 2017-01-01 12:00:45" ) );
}

$smis->addTabs("profile", "Profile"," fa fa-user-secret" );
if($smis->isGroup("profile")){
    $smis->addItem("profile", new SettingsItem($db, "smis_autonomous_logo", "Logo", "", "file-single-image", "Logo Default" ) );
    $smis->addItem("profile", new SettingsItem($db, "smis_autonomous_non_interlaced_logo", "Logo Non Interlaced", "", "file-single-image", "Logo untuk Non Interlaced (tanpa Alpha)" ) );
    $smis->addItem("profile", new SettingsItem($db, "smis_autonomous_bw_logo", "Logo Hitam Putih", "", "file-single-image", "Logo Hitam Putih" ) );
    $smis->addItem("profile", new SettingsItem($db, "smis_autonomous_loading_logo", "Logo Loading", "", "file-single-image", "Logo untuk Loading Bar" ) );
    $smis->addItem("profile", new SettingsItem($db, "smis_autonomous_dinas_logo", "Dinas Logo", "", "file-single-image", "Logo untuk Dinas" ) );
    $smis->addItem("profile", new SettingsItem($db, "smis_autonomous_title", "Nama", "", "text", "Nama dari Instansi" ) );
    $smis->addItem("profile", new SettingsItem($db, "smis_autonomous_sub_title", "Sub Nama", "", "text", "Sub Nama Instansi" ) );
    $smis->addItem("profile", new SettingsItem($db, "smis_autonomous_abbrevation", "Singkatan Instansi", "", "text", "nama singkatan dari instansi" ) );
    $smis->addItem("profile", new SettingsItem($db, "smis_autonomous_code", "No. Code", "", "text", "Code dari Instansi" ) );
    $smis->addItem("profile", new SettingsItem($db, "smis_autonomous_tanggal", "Tanggal Register", "", "date", "Tanggal Registrasi Instansi" ) );
    
    $smis->addItem("profile", new SettingsItem($db, "smis_autonomous_address", "Alamat Lengkap", "Alamat Lengkap Berserta Nomor Telponya", "textarea", "Alamat Lengkap Beserta Nomor Telponya" ) );
    $smis->addItem("profile", new SettingsItem($db, "smis_autonomous_address_mini", "Alamat Mini", "", "textarea", "Alamat Mini untuk Kebutuhan Header Kecil" ) );
    $smis->addItem("profile", new SettingsItem($db, "smis_autonomous_kodepos", "Kode Pos", "", "text", "Kode Pos" ) );
    $smis->addItem("profile", new SettingsItem($db, "smis_autonomous_contact", "Telp 1", "(0322) ", "textarea", "Telp. 1" ) );
    $smis->addItem("profile", new SettingsItem($db, "smis_autonomous_contact2", "Telp 2", "(0322) ", "textarea", "Telp. 1" ) );
    $smis->addItem("profile", new SettingsItem($db, "smis_autonomous_fax", "Fax", "", "text", "Nomor Faximile" ) );
    $smis->addItem("profile", new SettingsItem($db, "smis_autonomous_email", "E-Mail", "", "text", "Alamat E-mail" ) );
    $smis->addItem("profile", new SettingsItem($db, "smis_autonomous_website", "Website", "", "text", "URL Web" ) );
    $smis->addItem("profile", new SettingsItem($db, "smis_autonomous_npwp", "NPWP", "", "text", "Nomor NPWP" ) );    
    $smis->addItem("profile", new SettingsItem($db, "smis_autonomous_province", "Provinsi", "", "text", "Nama Provinsi" ) );
    $smis->addItem("profile", new SettingsItem($db, "smis_autonomous_town", "Kota / Kabupaten", "", "text", "Nama Kabupaten atau Kota" ) );
    $smis->addItem("profile", new SettingsItem($db, "smis_autonomous_district", "Kecamatan", "", "text", "Nama Kecamatan" ) );
    $smis->addItem("profile", new SettingsItem($db, "smis_autonomous_village", "Kelurahan", "", "text", "Nama Kelurahan" ) );
    $smis->addItem("profile", new SettingsItem($db, "smis_autonomous_type", "Jenis", "", "text", "Jenis dari Instansi" ) );
    $smis->addItem("profile", new SettingsItem($db, "smis_autonomous_class", "Kelas", "", "text", "Kelas dari Instansi" ) );
    $smis->addItem("profile", new SettingsItem($db, "smis_autonomous_direktur", "Nama Direktur", "", "text", "Nama Direktur dari Instansi" ) );
    $smis->addItem("profile", new SettingsItem($db, "smis_autonomous_pendidikan_direktur", "Pendidikan Direktur", "", "text", "Pendidikan Direktur dari Instansi" ) );
    $smis->addItem("profile", new SettingsItem($db, "smis_autonomous_penyelenggara", "Penyelenggara", "", "text", "Penyelenggara dari Instansi" ) );
}

$smis->addTabs("maintenance", "Maintenance"," fa fa-server" );
if($smis->isGroup("maintenance")){
    $smis->addItem("maintenance", new SettingsItem($db, "smis_maintenance_mode", "Maintenance Mode", "0", "checkbox", "Activated Maintenance Mode" ) );
    $smis->addItem("maintenance", new SettingsItem($db, "smis_maintenance_mode_msg", "Maintenance Message", "Sori Gan Lagi Maintenance", "summernote", "Message of Maintenance Mode" ) );
    $smis->addItem("maintenance", new SettingsItem($db, "smis_annoucement", "Announcement", "", "summernote", "Annoucement of all user" ) );
}

$smis->addTabs("wallpaper", "Wallpaper"," fa fa-image" );
if($smis->isGroup("wallpaper")){
    $smis->addItem("wallpaper", new SettingsItem($db, "background-wallpaper", "Background Wallpaper", "", "file-single-image", "Image Default Background Wallpaper" ) );
    $list_bg = getAllFileInDir("smis-theme/" );
    $wallpaper=new OptionBuilder();
    foreach($list_bg as $slug ) {
        $wallpaper->add($slug);
    }
    $smis->addItem("wallpaper", new SettingsItem($db, "smis-color-theme", "Color Theme", $wallpaper->getContent(), "select", "Default Colour Theme" ) );    
}

$smis->addTabs("developer", "Developer"," fa fa-user-secret" );
if($smis->isGroup("developer")){
    $smis->addItem("developer", new SettingsItem($db, "smis-debuggable-mode", "Debuggable", "0", "checkbox", "Enable Debuggable" ) );
}


$smis->addTabs("cronjob", "Cronjob"," fa fa-table" );
if($smis->isGroup("cronjob")){
    $list=getListActivePlugins(false);
    foreach($list as $name=>$link){
        if(file_exists($link."/cronjob.php")){
            $aname=ArrayAdapter::format("unslug",$name);
            $smis->addItem("cronjob", new SettingsItem($db, "smis-system-cronjob-".$name, "Cronjob untuk ".$aname, "1", "checkbox", "Cronjob Untuk ".$aname ) );
        }
    }
}

$smis->addTabs("phpini", "PHP Settings"," fa fa-cogs" );
if($smis->isGroup("phpini")){
    $smis->addItem("phpini", new SettingsItem($db, "override_php_ini", "Override PHP Ini", "0", "checkbox", "Override PHP.ini setting on running time" ) );
    $smis->addItem("phpini", new SettingsItem($db, "max_execution_time", "Max Execution Time", "30", "text", "default 30, in second" ) );
    $smis->addItem("phpini", new SettingsItem($db, "memory_limit", "Max Memory Limit", "16M", "text", "default 16M" ) );
}

$smis->addTabs("synch", "Synchronizer"," fa fa-upload" );
if($smis->isGroup("synch")){
    $smis->addItem("synch", new SettingsItem($db, "smis-adm-synch-times", "Berapa Table Synch dalam satu waktu", "1", "text", "default adalah 1, kurang dari 1 dianggap sebagai 1" ) );
}

$smis->setPartialLoad(true);
$response = $smis->init ();
?>