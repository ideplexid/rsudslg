<?php
	require_once ("smis-framework/smis/api/Plugin.php");
	require_once ("smis-base/smis-base.php");
	global $PLUGINS;
	$serverbus = null;
	foreach ( $PLUGINS as $pn ) {
		if ($pn->getName () == "smis-serverbus") {
			$serverbus = $pn;
		}
	}
	
	$tabulator = new Tabulator ( "log", "", Tabulator::$LANDSCAPE);
	if ($serverbus != null && $serverbus->is_actived ()) {
		$tabulator->add ( "autonomous", "Autonomous", "", Tabulator::$TYPE_HTML,"fa fa-desktop");
		$tabulator->add ( "tester", "Tester", "", Tabulator::$TYPE_HTML,"fa fa-terminal");
	}
	$tabulator->add ( "systemlog", "System Log", "", Tabulator::$TYPE_HTML ,"fa fa-tag");
	$tabulator->add ( "requestlog", "Request Log", "", Tabulator::$TYPE_HTML,"fa fa-tasks" );
	if ($serverbus != null && $serverbus->is_actived ()) {
		$tabulator->add ( "serverbus_log", "Server Bus Log", "", Tabulator::$TYPE_HTML ,"fa fa-server");
	}
	$tabulator->add ( "responselog", "Response Log", "", Tabulator::$TYPE_HTML ,"fa fa-reply");
	$tabulator->add ( "servicelog", "Service Log", "", Tabulator::$TYPE_HTML ,"fa fa-skyatlas");
	$tabulator->add ( "userlog", "User Log", "", Tabulator::$TYPE_HTML , "fa fa-user-secret");
	$tabulator->add ( "cronjobtime", "Cronjob Log", "", Tabulator::$TYPE_HTML , "fa fa-steam-square");
	$tabulator->add ( "consumer", "Synch Consumer", "", Tabulator::$TYPE_HTML , "fa fa-cloud-upload");
	$tabulator->add ( "provider", "Synch Provider", "", Tabulator::$TYPE_HTML , "fa fa-cloud-download");
	$tabulator->setPartialLoad(true, "smis-administrator", "log", "log",true);
	$tabulator->setPageName("smis_administrator");
	echo $tabulator->getHtml ();
?>