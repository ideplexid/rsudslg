<?php 

$tab=new Tabulator("database", "Database");
$tab->add("risma", "Risma", "", Tabulator::$TYPE_HTML , "fa fa-bolt")
	->add("anifia", "Anifia", "", Tabulator::$TYPE_HTML , "fa fa-table")
	->add("ametis", "Ametis", "", Tabulator::$TYPE_HTML , "fa fa-diamond")
    ->add("silvi", "Silvi", "", Tabulator::$TYPE_HTML , "fa fa-key")
	->add("tables", "Table", "",Tabulator::$TYPE_HTML," fa fa-table")
	->add("innodb", "Engine", "",Tabulator::$TYPE_HTML," fa fa-steam-square")
	->add("backup", "Backup", "",Tabulator::$TYPE_HTML," fa fa-database")
	->add("backup_history", "History", "",Tabulator::$TYPE_HTML," fa fa-sitemap")
	->add("backup_archive", "Archive", "",Tabulator::$TYPE_HTML," fa fa-file-zip-o")
	->add("locker", "Locker", "",Tabulator::$TYPE_HTML," fa fa-lock")
    ->add("dbresume", "Resume", "",Tabulator::$TYPE_HTML," fa fa-file");
$tab->setPartialLoad(true, "smis-administrator", "database", "database",true);
$tab->setPageName("smis_administrator");
echo $tab->getHtml();
?>