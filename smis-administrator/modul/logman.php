<?php 
	$tabulator = new Tabulator ( "logman", "logman", Tabulator::$POTRAIT);
	$tabulator->add ( "looqman", "Looqman", "", Tabulator::$TYPE_HTML , "fa fa-calendar");
	$tabulator->add ( "malik", "Malik", "", Tabulator::$TYPE_HTML , "fa fa-gg");
	$tabulator->add ( "dunbat", "Dunbat", "", Tabulator::$TYPE_HTML , "fa fa-rocket");
	$tabulator->setPartialLoad(true, "smis-administrator", "logman", "logman",true);
	$tabulator->setPageName("smis_administrator");
	echo $tabulator->getHtml ();
?>