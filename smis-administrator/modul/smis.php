<?php 

$library=new Tabulator("library", "library",Tabulator::$LANDSCAPE);
$library->add("lib_ui", "User Interface Library", "smis-administrator/resource/php/smis/outsource_ui.php",Tabulator::$TYPE_INCLUDE,"fa fa-book");
$library->add("lib_engine", "Engine Library", "smis-administrator/resource/php/smis/outsource_server.php",Tabulator::$TYPE_INCLUDE,"fa fa-cogs");

$license=new Tabulator("license", "license",Tabulator::$LANDSCAPE);
$license->add("apache_license", "Apache License v2.0", "smis-administrator/resource/php/smis/apache_license.php",Tabulator::$TYPE_INCLUDE,"fa fa-server");
$license->add("mit_license", "MIT License ", "smis-administrator/resource/php/smis/mit_license.php",Tabulator::$TYPE_INCLUDE,"fa fa-university");
$license->add("sil_license", "SIL OFL License v1.1", "smis-administrator/resource/php/smis/sil_license.php",Tabulator::$TYPE_INCLUDE,"fa fa-android");
$license->add("bsd_license", "Simplified BSD License", "smis-administrator/resource/php/smis/bsd_license.php",Tabulator::$TYPE_INCLUDE,"fa fa-codepen");
$license->add("gpl_license", "GPL License", "smis-administrator/resource/php/smis/gpl_license.php",Tabulator::$TYPE_INCLUDE," fa fa-linux");

$smis=new Tabulator("safethree_tab", "safethree_smis",Tabulator::$LANDSCAPE);
$smis->add("license_safethree", "License Safethreee", "smis-administrator/resource/php/smis/license_safethree.php",Tabulator::$TYPE_INCLUDE,"fa fa-file-text");
$smis->add("about_safethree", "About Safethree", "smis-administrator/resource/php/smis/about_safethree.php",Tabulator::$TYPE_INCLUDE,"fa fa-book");
$smis->add("file_safethree", "Base File of Safethree", "smis-administrator/resource/php/smis/file_safethree.php",Tabulator::$TYPE_INCLUDE,"fa fa-file-code-o");
$smis->add("credit_safethree", "Credit", "smis-administrator/resource/php/smis/credit_safethree.php",Tabulator::$TYPE_INCLUDE,"fa fa-list");

$tabulator=new Tabulator("about SMIS", "");
$tabulator->add("smis_", "Safethree", $smis,Tabulator::$TYPE_COMPONENT,"fa fa-circle-thin");
$tabulator->add("library_", "Outside Library", $library,Tabulator::$TYPE_COMPONENT,"fa fa-openid");
$tabulator->add("license_", "License", $license,Tabulator::$TYPE_COMPONENT,"fa fa-ticket");
echo $tabulator->getHtml();

?>