<?php 
	$tabs=new Tabulator("fileman", "");
	$tabs->add("sfman", "Salman", "smis-administrator/resource/php/filemanager/salman.php",Tabulator::$TYPE_INCLUDE,"fa fa-folder-open");
	$tabs->add("source_editor", "Source Code Editor", "smis-administrator/resource/php/filemanager/sceditor.php",Tabulator::$TYPE_INCLUDE,"fa fa-bitbucket");
	$tabs->add("jargit_manager", "Jargit", "smis-administrator/resource/php/filemanager/jargit.php",Tabulator::$TYPE_INCLUDE,"fa fa-git-square");
	$tabs->add("mysql_manager", "MySQL", "smis-administrator/resource/php/filemanager/mysql.php",Tabulator::$TYPE_INCLUDE,"fa fa-terminal");
	echo $tabs->getHtml();
?>