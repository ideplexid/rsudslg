<?php 
	$tabulator = new Tabulator ( "development", "development", Tabulator::$POTRAIT);
	$tabulator->add ( "riscti", "Riscti", "", Tabulator::$TYPE_HTML , "fa fa-wifi");
	$tabulator->add ( "silmei", "Silmei", "", Tabulator::$TYPE_HTML , "fa fa-sticky-note");
	$tabulator->add ( "santi", "Santi", "", Tabulator::$TYPE_HTML , "fa fa-file-code-o");
	$tabulator->add ( "rireist", "Rireist", "", Tabulator::$TYPE_HTML , "fa fa-terminal");
	$tabulator->setPartialLoad(true, "smis-administrator", "development", "development",true);
	$tabulator->setPageName("smis_administrator");
	echo $tabulator->getHtml ();
?>