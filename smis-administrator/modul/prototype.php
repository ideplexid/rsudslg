<?php
require_once ("smis-framework/smis/ui/composite/Navigator.php");
require_once ("smis-framework/smis/ui/composite/Menu.php");
require_once ("smis-framework/smis/api/Plugin.php");
require_once ("smis-base/smis-base.php");
require_once "smis-administrator/class/table/PrototypeTable.php";
global $PLUGINS;



$header=array ('Name',"Slug","Parent","Keterangan","Settings" );
$uitable = new PrototypeTable ($header, "Prototype");
$uitable->setName ( "prototype" );

/* this is respond when system have to response */
if (isset ( $_POST ['command'] )) {
	$adapter = new SimpleAdapter ();
	$adapter->add ( "Slug", "slug" );
	$adapter->add ( "Name", "nama" );
	$adapter->add ( "Parent", "parent", "strtoupper" );
	$adapter->add ( "Keterangan", "keterangan" );
	$adapter->add ( "Settings", "settings" );
	$adapter->add ( "status", "status" );
	$adapter->add ( "parent", "parent" );
	$adapter->add ( "slug", "slug" );
	$dbtable = new DBTable ( $db, "smis_adm_prototype" );
	$dbtable->setOrder ( "slug ASC, nama ASC " );
	$dbres = new DBResponder ( $dbtable, $uitable, $adapter );
	$command = $_POST ['command'];
	if ($command == "install") {
		$parent = $_POST ['parent'];
		require_once $parent . '/install.php';
		$command = "save";
	}
	if($command == "uninstall"){
		$parent = $_POST ['parent'];
		require_once $parent . '/uninstall.php';
		$command = "save";
	}
    $data = $dbres->command ( $command );
    if($dbres->isSave()){
        if($_POST['parent']=="rawat" && $_POST['jenis_kelas']=="1"){
            $slug = $_POST['slug'];
            $query = "update smis_rwt_bed_kamar_".$slug." set kelas_aplicare = '".$_POST['kelas_aplicare']."' ";
            $db->query($query);

            require_once "rawat/function/update_available_beds.php";
            update_available_beds($slug);
        }
    } 

	echo json_encode ( $data );
	return;
}
require_once 'smis-base/smis-include-service-consumer.php';


global $db;
$service    = new ServiceConsumer($db, "get_kelas");
$service    ->setCached(true,"get_kelas");
$service    ->execute ();
$kelas_serv = $service->getContent ();

$option = new OptionBuilder();
$option ->add("","","1");
foreach($kelas_serv  as $k){
    $option ->add($k['nama'],$k['slug']);
}

$parent = array ();
foreach ( $PLUGINS as $p ) {
	if (! $p->is_administrator () && $p->is_installed () && $p->is_actived () && $p->is_prototype () && ! $p->is_serverbus ()) {
		$parent [] = array (
				"name" => strtoupper ( $p->getName () ),
				"value" => $p->getname () 
		);
	}
}

$aplicare = new OptionBuilder();
$aplicare ->add("","","1");
$aplicare ->add("-","NON");
$aplicare ->add("VVIP","VVP");
$aplicare ->add("VIP","VIP");
$aplicare ->add("UTAMA","UTM");
$aplicare ->add("KELAS I","KL1");
$aplicare ->add("KELAS II","KL2");
$aplicare ->add("KELAS III","KL3");
$aplicare ->add("ICU","ICU");
$aplicare ->add("ICCU","ICC");
$aplicare ->add("NICU","NIC");
$aplicare ->add("PICU","PIC");
$aplicare ->add("IGD","IGD");
$aplicare ->add("UGD","UGD");
$aplicare ->add("RUANG BERSALIN","SAL");
$aplicare ->add("HCU","HCU");
$aplicare ->add("RUANG ISOLASI","ISO");

$jk = new OptionBuilder();
$jk ->add("","","1");
$jk ->add("1 Kelas","1","0");
$jk ->add("Multi Kelas","0","0");

$jenis_ruangan = new OptionBuilder();
$jenis_ruangan ->add("","","1");
$jenis_ruangan ->add("1 Kelas","1","0");
$jenis_ruangan ->add("Multi Kelas","0","0");

$jenis     = new OptionBuilder ();
$jenis	    ->add("","","1");
$jenis	    ->add("Unit Rawat Jalan","URJ")
            ->add("Unit Rawat Inap","URI")
            ->add("Unit Rawat Jalan / Inap","URJI")
            ->add("Unit Penunjang","UP");


/* This is Modal Form and used for add and edit the table */
$uitable->addModal("id","hidden","","");
$uitable->addModal('slug',"text","Slug","");
$uitable->addModal('nama',"text","Nama","");
$uitable->addModal('parent',"select","Prototype",$parent);
$uitable->addModal('keterangan',"textarea","Keterangan","");
$uitable->addModal("settings","textarea","Settings","");
$uitable->addModal("jenis_kelas","select","Jenis Kelas",$jk->getContent());
$uitable->addModal("kelas","select","Kelas",$option ->getContent());
$uitable->addModal("kelas_aplicare","select","Kelas Aplicare",$aplicare ->getContent());
$uitable->addModal("jenis_ruangan","select","Jenis Ruangan",$jenis->getContent());
$uitable->addModal("grup_ruangan","text","Grup","");
$modal = $uitable->getModal ();
$modal->setTitle ( "Prototype" );

echo $uitable->getHtml ();
echo $modal->getHtml ();
echo addJS ( "framework/smis/js/table_action.js" );

?>
<script type="text/javascript">
var prototype;
$(document).ready(function(){
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
	var column=new Array('id','slug','nama',"parent",'keterangan',"settings","jenis_ruangan","jenis_kelas","kelas","kelas_aplicare","grup_ruangan",);
	prototype=new TableAction("prototype","smis-administrator","prototype",column);
	prototype.setDisabledOnEdit(new Array("slug","parent"));
	
	
	$("#prototype_parent").on("change",function(){
        let dt = $("#prototype_parent").val();
        if(dt=="rawat"){
            $("#prototype_jenis_kelas").prop("disabled",false);
            $("#prototype_kelas").prop("disabled",false);
            $("#prototype_kelas_aplicare").prop("disabled",false);
            $("#prototype_grup_ruangan").prop("disabled",false);
            $("#prototype_jenis_ruangan").prop("disabled",false);

        }else{
            $("#prototype_jenis_kelas").prop("disabled",true);
            $("#prototype_kelas").prop("disabled",true);
            $("#prototype_kelas_aplicare").prop("disabled",true);
            $("#prototype_grup_ruangan").prop("disabled",true);
            $("#prototype_jenis_ruangan").prop("disabled",true);

            $("#prototype_jenis_kelas").val("");
            $("#prototype_kelas").val("");
            $("#prototype_kelas_aplicare").val("");
            $("#prototype_grup_ruangan").val("");
            $("#prototype_grup_ruangan").val("");

        }

        $("#prototype_jenis_kelas").trigger("change");
        $("#prototype_kelas").trigger("change");
        $("#prototype_kelas_aplicare").trigger("change");
        $("#prototype_grup_ruangan").trigger("change");
        $("#prototype_grup_ruangan").trigger("change");
    });

    $("#prototype_jenis_kelas").on("change",function(){
        let jk = $("#prototype_jenis_kelas").val();
        if(jk=="1"){
            $("#prototype_kelas").prop("disabled",false);
            $("#prototype_kelas_aplicare").prop("disabled",false);
        }else{
            $("#prototype_kelas").val("");
            $("#prototype_kelas").prop("disabled",true);
            $("#prototype_kelas_aplicare").val("");
            $("#prototype_kelas_aplicare").prop("disabled",true);
        }
    });
    

	prototype.install=function(id,slug,parent){
		var save_data=this.getRegulerData();
		save_data['command']="install";
		save_data['status']="installed";
		save_data['slug']=slug;
		save_data['parent']=parent;
		save_data['prototype_install']='y';
		save_data['id']=id;
		showLoading();
		$.post("",save_data,function(res){
			var json=getContent(res);
			prototype.view();
			dismissLoading();
		});
	};

	prototype.uninstall=function(id,slug,parent){
		var save_data=this.getRegulerData();
		save_data['status']="";
		save_data['command']="uninstall";
		save_data['slug']=slug;
		save_data['parent']=parent;
		save_data['prototype_install']='y';
		save_data['id']=id;
		showLoading();
		$.post("",save_data,function(res){
			var json=getContent(res);
			prototype.view();
			dismissLoading();
		});
	};

	prototype.actived=function(id){
		var save_data=this.getRegulerData();
		save_data['command']="save";
		save_data['status']="actived";
		save_data['id']=id;
		showLoading();
		$.post("",save_data,function(res){
			var json=getContent(res);
			prototype.view();
			dismissLoading();
		});
	};

	prototype.deactived=function(id){
		var save_data=this.getRegulerData();
		save_data['command']="save";
		save_data['status']="deactived";
		save_data['id']=id;
		showLoading();
		$.post("",save_data,function(res){
			var json=getContent(res);
			prototype.view();
			dismissLoading();
		});
	};

	prototype.view();

});
</script>