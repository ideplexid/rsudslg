<?php 
$a="<p align='justify'>This software include several Library for 
Safethree Engine that come from another Source, 
below is the list of the outsource that inlcuded in this software. 
every source come from LGPL, Apache License, The MIT License (MIT), 
and Several Owner License, Off Course Free Open Source Software.</p>";

echo "<div class='well'>".$a."</div>";

$content							= array();
$bootstrap							= array();
$bootstrap["Name"]					= "PclZip (Zip Module)";
$bootstrap["Version"]				= "2.8.2";
$bootstrap["License"]				= "License GNU/LGPL";
$bootstrap["Website"]				= "http://www.phpconcept.net";
$bootstrap["Author"]				= " Vincent Blavet";
$bootstrap["Description"]			= "This Library used for make a Zip file for Download in Administrator Backup Database";
$bootstrap["Files"]					= array();
$bootstrap["Files"][]				= "smis-libs-out/pclzip.lib.php";
$content[]							= $bootstrap;

$pmail								= array();
$pmail["Name"]						= "PHPMailer";
$pmail["Version"]					= "5.2.16";
$pmail["License"]					= "LGPL 2.1";
$pmail["Website"]					= "https://github.com/PHPMailer/PHPMailer";
$pmail["Author"]					= "Brent R. Matzelle - Marcus Bointon - Andy Prevost ";
$pmail["Description"]				= "This Library used for Sending Email Through PHP";
$pmail["Files"]						= array();
$mlist["smis-libs-out/php-mailer"]	= "smis-libs-out/php-mailer";
$list								= getAllFileInDir("smis-libs-out/php-mailer",true,false);
listFullPath($mlist, "smis-libs-out/php-mailer", $list);
$pmail["Files"]						= $mlist;
$content[]							= $pmail;

$git								= array();
$git["Name"]						= "Git (Git Module)";
$git["Version"]						= "0.1.4";
$git["License"]						= "MIT License";
$git["Website"]						= "http://github.com/kbjr/Git.php";
$git["Author"]						= " Copyright 2013 James Brumond";
$git["Description"]					= "This Library used for Jargit System in Administrator";
$git["Files"]						= array();
$git["Files"][]						= "smis-libs-out/Git.php";
$content[]							= $git;

$linfo								= array();
$linfo["Name"]						= "Linfo";
$linfo["Version"]					= "1.1";
$linfo["License"]					= "GPL";
$linfo["Website"]					= "http://github.com/jrgp/linfo";
$linfo["Author"]					= " Linfo © 2010 – 2015 Joseph Gillotti & friends";
$linfo["Description"]				= "This Library used for Hardware Detection in Administrator";
$linfo["Files"]						= array();
$llist["smis-libs-out/linfo"]		= "smis-libs-out/linfo";
$list								= getAllFileInDir("smis-libs-out/linfo",true,false);
listFullPath($llist, "smis-libs-out/linfo", $list);
$linfo["Files"]						= $llist;
$content[]							= $linfo;

$pexcel								= array();
$pexcel["Name"]						= "FPDF";
$pexcel["Version"]					= "1.81 - 2015-12-20";
$pexcel["License"]					= "Permissive License";
$pexcel["Website"]					= "www.fpdf.org";
$pexcel["Author"]					= "Olivier PLATHEY";
$pexcel["Description"]				= "This Library used for Creating Advance PDF in PHP";
$pexcel["Files"]					= array();
$xlist["smis-libs-out/fpdf"]		= "smis-libs-out/fpdf";
$list								= getAllFileInDir("smis-libs-out/fpdf",true,false);
listFullPath($xlist, "smis-libs-out/fpdf", $list);
$pexcel["Files"]					= $xlist;
$content[]							= $pexcel;

$pexcel								= array();
$pexcel["Name"]						= "PHPExcel";
$pexcel["Version"]					= "1.8.0, 2014-03-02";
$pexcel["License"]					= "LGPL";
$pexcel["Website"]					= "http://phpexcel.codeplex.com/";
$pexcel["Author"]					= "Copyright (c) 2006 - 2014 PHPExcel ";
$pexcel["Description"]				= "This Library used for Creating Advance Excel in PHP";
$pexcel["Files"]					= array();
$xlist["smis-libs-out/php-excel"]	= "smis-libs-out/php-excel";
$list								= getAllFileInDir("smis-libs-out/php-excel",true,false);
listFullPath($xlist, "smis-libs-out/php-excel", $list);
$pexcel["Files"]					= $xlist;
$content[]							= $pexcel;



$tpt=new TablePrint("Library of Engine");
$tpt->setTableClass("table table-bordered table-hover table-condensed");
$tpt->setMaxWidth(false);
$tpt->addColumn("<h2>LIST OF THE ENGINE LIBRARY</h2>", "10", "1",null,"","center");
$tpt->commit("title");
foreach($content as $lib){
	$slug=ArrayAdapter::format("slug", $lib['Name']);
	$tpt->addColumn("<h4>".$lib['Name']."</h>", 4, 1,null,"","center");
	$tpt->commit("body","head_lib ".$slug);
	$tpt->addColumn("Version", 1, 1);
	$tpt->addColumn($lib["Version"], 3, 1);
	$tpt->commit("body",$slug." tail");
	$tpt->addColumn("Website", 1, 1);
	$tpt->addColumn("<a target='_blank' href=\"".$lib['Website']."\" >".$lib['Website']."</a>", 3, 1);	
	$tpt->commit("body",$slug." tail");
	$tpt->addColumn("Author", 1, 1);
	$tpt->addColumn($lib["Author"], 3, 1);	
	$tpt->commit("body",$slug." tail");
	$tpt->addColumn("License", 1, 1);
	$tpt->addColumn($lib["License"], 3, 1);	
	$tpt->commit("body",$slug." tail");
	$tpt->addColumn($lib['Description'], 4, 1);
	$tpt->commit("body",$slug." tail");
	$tpt->addColumn("<strong><small>Included Files</small></strong>", 4, 1);
	$tpt->commit("body",$slug." tail");
	
	$the_files="<ul>";
	foreach($lib['Files'] as $file){
		$the_files.="<li><i>".$file."</i></li>";
	}
	$the_files.="</ul>";
	$tpt->addColumn($the_files, 4, 1);
	$tpt->commit("body",$slug." tail");
	
}
echo $tpt->getHtml();

?>