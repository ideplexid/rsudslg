<?php 
$a="
<p align='justify'>This software include several Library for User Interface that come from another Source, 
below is the list of the outsource that inlcuded in this software. 
every source come from LGPL, Apache License, The MIT License (MIT), 
and Several Owner License, Off Course Free Open Source Software.</p>";

echo "<div class='well'>".$a."</div>";

$content=array();
$bootstrap=array();
$bootstrap["Name"]="Bootstrap";
$bootstrap["Version"]="2.3.2";
$bootstrap["License"]="Apache License v2.0";
$bootstrap["Website"]="http://getbootstrap.com/2.3.2/";
$bootstrap["Author"]="Twitter @mdo and @fat";
$bootstrap["Description"]="This is The Based System of Safethree User Interface, used to make UI Of Table, Menu, etc";
$bootstrap["Files"]=array();
$bootstrap["Files"][]="smis-framework/bootstrap/js/bootstrap.min.js";
$bootstrap["Files"][]="smis-framework/bootstrap/css/bootstrap.min.css";
$content[]=$bootstrap;

$bcalendar=array();
$bcalendar["Name"]="Bootstrap Calendar";
$bcalendar["Version"]="-";
$bcalendar["License"]="MIT License";
$bcalendar["Website"]="http://bootstrap-calendar.azurewebsites.net/";
$bcalendar["Author"]="Twitter @serhioromano";
$bcalendar["Description"]="this library use to make Calendar View in THe Tools Section in Safethree";
$bcalendar["Files"]=array();
$bcalendar["Files"][]="smis-framework/bootstrap/js/bootstrap-calendar.js";
$bcalendar["Files"][]="smis-framework/bootstrap/css/bootstrap-calendar.css";
$bcalendar["Files"][]="smis-framework/bootstrap/img/border-image.png";
$bcalendar["Files"][]="smis-framework/bootstrap/img/dark_wood.png";
$bcalendar["Files"][]="smis-framework/bootstrap/img/glyphicons-halflings.png";
$bcalendar["Files"][]="smis-framework/bootstrap/img/glyphicons-halflings-white.png";
$bcalendar["Files"][]="smis-framework/bootstrap/img/icons.png";
$bcalendar["Files"][]="smis-framework/bootstrap/img/tick.png";
$bcalendar["Files"][]="smis-framework/bootstrap/calendar/tick.png";
$bcalendar["Files"][]="smis-framework/bootstrap/calendar/calendar.js";
$bcalendar["Files"][]="smis-framework/bootstrap/calendar/calendar.css";
$bcalendar["Files"][]="smis-framework/bootstrap/calendar/underscore-min.js";
$bcalendar["Files"][]="smis-framework/bootstrap/calendar/tmpls/day.html";
$bcalendar["Files"][]="smis-framework/bootstrap/calendar/tmpls/modal.html";
$bcalendar["Files"][]="smis-framework/bootstrap/calendar/tmpls/events-list.html";
$bcalendar["Files"][]="smis-framework/bootstrap/calendar/tmpls/month.html";
$bcalendar["Files"][]="smis-framework/bootstrap/calendar/tmpls/month-day.html";
$bcalendar["Files"][]="smis-framework/bootstrap/calendar/tmpls/week.html";
$bcalendar["Files"][]="smis-framework/bootstrap/calendar/tmpls/week-days.html";
$bcalendar["Files"][]="smis-framework/bootstrap/calendar/tmpls/year.html";
$bcalendar["Files"][]="smis-framework/bootstrap/calendar/tmpls/year-month.html";
$content[]=$bcalendar;

$bootstrap=array();
$bootstrap["Name"]="Bootstrap Calculator";
$bootstrap["Version"]="1.0.0.0";
$bootstrap["License"]="Apache License v2.0";
$bootstrap["Website"]="https://github.com/mensfeld/bootstrap-calculator/";
$bootstrap["Author"]="Maciej Mensfeld";
$bootstrap["Description"]="This Library Used to Provide Simple Calculator in Safethree";
$bootstrap["Files"]=array();
$bootstrap["Files"][]="smis-framework/bootstrap/js/bootstrap-calculator.js";
$bootstrap["Files"][]="smis-framework/bootstrap/css/bootstrap-calculator.css";
$content[]=$bootstrap;

$bootstrap=array();
$bootstrap["Name"]="Bootstrap Color Picker";
$bootstrap["Version"]="2.1";
$bootstrap["License"]="Apache License v2.0";
$bootstrap["Website"]="http://mjolnic.com/bootstrap-colorpicker/";
$bootstrap["Author"]="Twitter @eyecon and mjolnic";
$bootstrap["Description"]="This Library used to make a color Picker in Safethree System For Managing using Google Maps";
$bootstrap["Files"]=array();
$bootstrap["Files"][]="smis-framework/bootstrap/js/bootstrap-colorpicker.js";
$bootstrap["Files"][]="smis-framework/bootstrap/css/bootstrap-colorpicker.css";
$bootstrap["Files"][]="smis-framework/bootstrap/css/bootstrap-responsive.min.css";
$content[]=$bootstrap;

$bootstrap=array();
$bootstrap["Name"]="Bootstrap Date Picker";
$bootstrap["Version"]="-";
$bootstrap["License"]="Apache License v2.0";
$bootstrap["Website"]="http://www.eyecon.ro/bootstrap-datepicker/";
$bootstrap["Author"]="Stefan Petre";
$bootstrap["Description"]="This Library used to make a Date Picker in Safethree System ";
$bootstrap["Files"]=array();
$bootstrap["Files"][]="smis-framework/bootstrap/js/bootstrap-datepicker.js";
$bootstrap["Files"][]="smis-framework/bootstrap/css/datepicker.css";
$content[]=$bootstrap;

$bootstrap=array();
$bootstrap["Name"]="Bootstrap Date Time Picker";
$bootstrap["Version"]="-";
$bootstrap["License"]="Apache License v2.0";
$bootstrap["Website"]="http://tarruda.github.io/bootstrap-datetimepicker/";
$bootstrap["Author"]="Stefan Petre and improvements by Andrew Rowls";
$bootstrap["Description"]="This Library used to make a Date Time Picker in Safethree System ";
$bootstrap["Files"]=array();
$bootstrap["Files"][]="smis-framework/bootstrap/js/bootstrap-datetimepicker.js";
$bootstrap["Files"][]="smis-framework/bootstrap/css/bootstrap-datetimepicker.css";
$content[]=$bootstrap;

$bootstrap=array();
$bootstrap["Name"]="Bootstrap Switch";
$bootstrap["Version"]="-";
$bootstrap["License"]="Apache License v2.0";
$bootstrap["Website"]="http://www.bootstrap-switch.org/";
$bootstrap["Author"]="Created by Mattia Larentis and Mantained by Emanuele Marchi";
$bootstrap["Description"]="This Library used to make a Swicth Button in Safethree System ";
$bootstrap["Files"]=array();
$bootstrap["Files"][]="smis-framework/bootstrap/js/bootstrap-switch.js";
$bootstrap["Files"][]="smis-framework/bootstrap/css/bootstrap-switch.css";
$content[]=$bootstrap;

$bootstrap=array();
$bootstrap["Name"]="Bootstrap Slider";
$bootstrap["Version"]="2.0.0";
$bootstrap["License"]="MIT License ";
$bootstrap["Website"]="https://github.com/seiyria/bootstrap-slider";
$bootstrap["Author"]="Kyle Kemp, Rohit Kalkur, and contributors";
$bootstrap["Description"]="This Library used to make a Slider Button in Safethree System ";
$bootstrap["Files"]=array();
$bootstrap["Files"][]="smis-framework/bootstrap/js/bootstrap-slider.js";
$bootstrap["Files"][]="smis-framework/bootstrap/css/slider.css";
$content[]=$bootstrap;

$bootstrap=array();
$bootstrap["Name"]="Bootstrap Tags Input";
$bootstrap["Version"]="2.0.0";
$bootstrap["License"]="MIT License ";
$bootstrap["Website"]="http://timschlechter.github.io/bootstrap-tagsinput/";
$bootstrap["Author"]="timschlechter";
$bootstrap["Description"]="This Library used to make a Multiple Tags Input in Safethree System ";
$bootstrap["Files"]=array();
$bootstrap["Files"][]="smis-framework/bootstrap/js/bootstrap-tagsinput.js";
$bootstrap["Files"][]="smis-framework/bootstrap/css/bootstrap-tagsinput.css";
$content[]=$bootstrap;

$bootstrap=array();
$bootstrap["Name"]="BootBox";
$bootstrap["Version"]="3.3.0";
$bootstrap["License"]="MIT License ";
$bootstrap["Website"]="http://bootboxjs.com/";
$bootstrap["Author"]="Copyright (C) 2011-2015 by Nick Payne <nick@kurai.co.uk>";
$bootstrap["Description"]="This Library used to make a Dialog in Safethree System";
$bootstrap["Files"]=array();
$bootstrap["Files"][]="smis-framework/bootstrap/js/bootbox.min.js";
$content[]=$bootstrap;

$bootstrap=array();
$bootstrap["Name"]="Summernote";
$bootstrap["Version"]="0.6";
$bootstrap["License"]="MIT License ";
$bootstrap["Website"]="http://summernote.org/";
$bootstrap["Author"]="Summernote team";
$bootstrap["Description"]="This Library used to make a WYSIWYG Editor in Safethree System ";
$bootstrap["Files"]=array();
$bootstrap["Files"][]="smis-framework/bootstrap/js/summernote.min.js";
$bootstrap["Files"][]="smis-framework/bootstrap/css/summernote.css";
$content[]=$bootstrap;

$bootstrap=array();
$bootstrap["Name"]="Font Awesome";
$bootstrap["Version"]="4.5.0";
$bootstrap["License"]="SIL OFL 1.1 for Font License under folder /smis-framework/font-awesome/font/* , MIT License for CSS License under folder smis-framework/font-awesome/css/*";
$bootstrap["Website"]="http://fortawesome.github.io/";
$bootstrap["Author"]="Dave Gandy";
$bootstrap["Description"]="This Library used to Iconic in Safethree Button and etc";
$bootstrap["Files"]=array();
$bootstrap["Files"][]="smis-framework/fa/css/font-awesome.min.css";
$bootstrap["Files"][]="smis-framework/fa/fonts/FontAwesome.otf";
$bootstrap["Files"][]="smis-framework/fa/fonts/fontawesome-webfont.eot";
$bootstrap["Files"][]="smis-framework/fa/fonts/fontawesome-webfont.svg";
$bootstrap["Files"][]="smis-framework/fa/fonts/fontawesome-webfont.ttf";
$bootstrap["Files"][]="smis-framework/fa/fonts/fontawesome-webfont.woff";
$bootstrap["Files"][]="smis-framework/fa/fonts/fontawesome-webfont.woff2";
$content[]=$bootstrap;


$bootstrap=array();
$bootstrap["Name"]="jQuery";
$bootstrap["Version"]="2.1.1 ";
$bootstrap["License"]="MIT License";
$bootstrap["Website"]="https://jquery.org/";
$bootstrap["Author"]="jQuery Foundation, Inc.";
$bootstrap["Description"]="This Library used to as Javacript Based on Safethree System";
$bootstrap["Files"]=array();
$bootstrap["Files"][]="smis-framework/jquery/jquery-2.1.1.min.js";
$content[]=$bootstrap;

$bootstrap=array();
$bootstrap["Name"]="jQuery Visible";
$bootstrap["Version"]="1.0.0 ";
$bootstrap["License"]="MIT License";
$bootstrap["Website"]="https://jquery.org/";
$bootstrap["Author"]="Digital Fusion";
$bootstrap["Description"]="This Library used check wether element visible or not in a viewport";
$bootstrap["Files"]=array();
$bootstrap["Files"][]="smis-framework/bootstrap/js/jquery.visible.min.js";
$content[]=$bootstrap;

$bootstrap=array();
$bootstrap["Name"]="jQuery UI Widget";
$bootstrap["Version"]="1.10.4+amd";
$bootstrap["License"]="MIT License";
$bootstrap["Website"]="http://api.jqueryui.com/jQuery.widget/";
$bootstrap["Author"]="jQuery Foundation, Inc.";
$bootstrap["Description"]="This Library used to as Upload Based System Widget in Safethree System";
$bootstrap["Files"]=array();
$bootstrap["Files"][]="smis-framework/jquery/jquery.ui.widget.js";
$content[]=$bootstrap;

$bootstrap=array();
$bootstrap["Name"]="jQuery Select";
$bootstrap["Version"]="3.4.6";
$bootstrap["License"]="Apache License v2.0 or GPL v2";
$bootstrap["Website"]="https://select2.github.io/";
$bootstrap["Author"]="Igor Vaynberg";
$bootstrap["Description"]="This Library used to make a Auto Complete Select in Safethree System";
$bootstrap["Files"]=array();
$bootstrap["Files"][]="smis-framework/bootstrap/js/bootstrap-select.js";
$bootstrap["Files"][]="smis-framework/bootstrap/css/bootstrap-select.css";
$content[]=$bootstrap;

$bootstrap=array();
$bootstrap["Name"]="jQuery Mask Money";
$bootstrap["Version"]=" 3.0.2";
$bootstrap["License"]="Licensed MIT";
$bootstrap["Website"]="https://github.com/plentz/jquery-maskmoney";
$bootstrap["Author"]="Diego Plentz";
$bootstrap["Description"]="This Library used to make a Masking Money in Safethree";
$bootstrap["Files"]=array();
$bootstrap["Files"][]="smis-framework/jquery/jquery.maskMoney.min.js";
$content[]=$bootstrap;

$bootstrap=array();
$bootstrap["Name"]="jQuery Turn";
$bootstrap["Version"]=" 4.1.0";
$bootstrap["License"]="MIT License";
$bootstrap["Website"]="https://github.com/plentz/jquery-maskmoney";
$bootstrap["Author"]="Emmanuel Garcia ";
$bootstrap["Description"]="This Library used to make a Turn Animation in Safethree";
$bootstrap["Files"]=array();
$bootstrap["Files"][]="smis-framework/jquery/turn.min.js";
$content[]=$bootstrap;

$bootstrap=array();
$bootstrap["Name"]="Impress";
$bootstrap["Version"]=" 0.5.3";
$bootstrap["License"]="MIT and GPL Licenses";
$bootstrap["Website"]="http://bartaz.github.com/impress.js/";
$bootstrap["Author"]="Bartek Szopka";
$bootstrap["Description"]="This Library used to make a Slide Presentation Animation in Safethree (not yet used, just include)";
$bootstrap["Files"]=array();
$bootstrap["Files"][]="smis-framework/jquery/impress.js";
$content[]=$bootstrap;

$bootstrap=array();
$bootstrap["Name"]="jQuery File Upload";
$bootstrap["Version"]="9.9.3";
$bootstrap["License"]="MIT license.";
$bootstrap["Website"]="https://github.com/blueimp/jQuery-File-Upload";
$bootstrap["Author"]="Sebastian Tschan";
$bootstrap["Description"]="This Library used to make a Uploader in Safethree System";
$bootstrap["Files"]=array();
$bootstrap["Files"][]="smis-framework/bootstrap/js/jquery.fileupload.js";
$bootstrap["Files"][]="smis-framework/bootstrap/js/jquery.iframe-transport.js";
$bootstrap["Files"][]="smis-framework/bootstrap/css/jquery.fileupload.css";
$content[]=$bootstrap;

$bootstrap=array();
$bootstrap["Name"]="jQuery Barcode";
$bootstrap["Version"]="2.0.3";
$bootstrap["License"]="GPL license";
$bootstrap["Website"]="http://barcode-coder.com/";
$bootstrap["Author"]="DEMONTE Jean-Baptiste <jbdemonte@gmail.com>";
$bootstrap["Description"]="This Library used to make a Barcode Image in Safethree";
$bootstrap["Files"]=array();
$bootstrap["Files"][]="smis-framework/jquery/jquery-barcode.min.js";
$content[]=$bootstrap;


$bootstrap=array();
$bootstrap["Name"]="jQuery knob";
$bootstrap["Version"]=" 1.2.0 (15/07/2012)";
$bootstrap["License"]="Under MIT and GPL licenses";
$bootstrap["Website"]="https://github.com/blueimp/jQuery-File-Upload";
$bootstrap["Author"]="Anthony Terrien";
$bootstrap["Description"]="This Library used to for Use Controlling Javascript Function";
$bootstrap["Files"]=array();
$bootstrap["Files"][]="smis-framework/bootstrap/js/jquery.knob.js";
$content[]=$bootstrap;

$bootstrap=array();
$bootstrap["Name"]="jQuery TouchSwipe";
$bootstrap["Version"]=" 1.6.0";
$bootstrap["License"]="Under MIT or GPL licenses";
$bootstrap["Website"]="https://github.com/mattbryson/TouchSwipe-Jquery-Plugin";
$bootstrap["Author"]="Matt Bryson";
$bootstrap["Description"]="This library used to create swipe effect on mobile device";
$bootstrap["Files"]=array();
$bootstrap["Files"][]="smis-framework/bootstrap/js/jquery.knob.js";
$content[]=$bootstrap;

$bootstrap=array();
$bootstrap["Name"]="Morris";
$bootstrap["Version"]=" 1.2.0 (15/07/2012)";
$bootstrap["License"]="Simplified BSD License";
$bootstrap["Website"]="http://morrisjs.github.io/morris.js/";
$bootstrap["Author"]="Olly Smith @olly_smith";
$bootstrap["Description"]="This Library used to for Use for Creating Graphical Diagram such AreaDiagram, Pie Diagram , Scatter Diagram , Bar Diagram etc";
$bootstrap["Files"]=array();
$bootstrap["Files"][]="smis-framework/bootstrap/js/morris.min.js";
$bootstrap["Files"][]="smis-framework/bootstrap/css/morris.css";
$content[]=$bootstrap;

$bootstrap=array();
$bootstrap["Name"]="Raphael";
$bootstrap["Version"]=" 2.1.2 ";
$bootstrap["License"]="MIT License";
$bootstrap["Website"]="http://raphaeljs.com";
$bootstrap["Author"]="2008-2012 Dmitry Baranovskiy ";
$bootstrap["Description"]="This Library used to for Use for Creating Vector Based as Morris Library";
$bootstrap["Files"]=array();
$bootstrap["Files"][]="smis-framework/bootstrap/js/raphael-min.js";
$content[]=$bootstrap;

$bootstrap=array();
$bootstrap["Name"]="Code Mirror";
$bootstrap["Version"]=" 2.1.2 ";
$bootstrap["License"]="MIT License";
$bootstrap["Website"]="https://codemirror.net/";
$bootstrap["Author"]="Copyright (C) 2014 by Marijn Haverbeke <marijnh@gmail.com> and others ";
$bootstrap["Description"]="This Library used for Creating Source Code Editor in System";
$bootstrap["Files"]=array();
$bootstrap["Files"][]="smis-framework/codemirror/css/3024-night.css";
$bootstrap["Files"][]="smis-framework/codemirror/css/blackboard.css";
$bootstrap["Files"][]="smis-framework/codemirror/css/codemirror.css";
$bootstrap["Files"][]="smis-framework/codemirror/css/eclipse.css";
$bootstrap["Files"][]="smis-framework/codemirror/css/erlang-dark.css";
$bootstrap["Files"][]="smis-framework/codemirror/css/foldgutter.css";
$bootstrap["Files"][]="smis-framework/codemirror/css/fullscreen.css";
$bootstrap["Files"][]="smis-framework/codemirror/css/lesser-dark.css";
$bootstrap["Files"][]="smis-framework/codemirror/css/liquibyte.css";
$bootstrap["Files"][]="smis-framework/codemirror/css/night.css";
$bootstrap["Files"][]="smis-framework/codemirror/css/show-hint.css";
$bootstrap["Files"][]="smis-framework/codemirror/css/the-matrix.css";
$bootstrap["Files"][]="smis-framework/codemirror/css/zenburn.css";

$bootstrap["Files"][]="smis-framework/codemirror/js/active-line.js";
$bootstrap["Files"][]="smis-framework/codemirror/js/anyword-hint.js";
$bootstrap["Files"][]="smis-framework/codemirror/js/brace-fold.js";
$bootstrap["Files"][]="smis-framework/codemirror/js/clike.js";
$bootstrap["Files"][]="smis-framework/codemirror/js/closebrackets.js";
$bootstrap["Files"][]="smis-framework/codemirror/js/closetag.js";
$bootstrap["Files"][]="smis-framework/codemirror/js/codemirror.js";
$bootstrap["Files"][]="smis-framework/codemirror/js/comment-fold.js";
$bootstrap["Files"][]="smis-framework/codemirror/js/comment.js";
$bootstrap["Files"][]="smis-framework/codemirror/js/continuecomment.js";
$bootstrap["Files"][]="smis-framework/codemirror/js/css-hint.js";
$bootstrap["Files"][]="smis-framework/codemirror/js/css.js";
$bootstrap["Files"][]="smis-framework/codemirror/js/foldcode.js";
$bootstrap["Files"][]="smis-framework/codemirror/js/foldgutter.js";
$bootstrap["Files"][]="smis-framework/codemirror/js/fullscreen.js";
$bootstrap["Files"][]="smis-framework/codemirror/js/html-hint.js";
$bootstrap["Files"][]="smis-framework/codemirror/js/htmlmixed.js";
$bootstrap["Files"][]="smis-framework/codemirror/js/javascript-hint.js";
$bootstrap["Files"][]="smis-framework/codemirror/js/javascript.js";
$bootstrap["Files"][]="smis-framework/codemirror/js/loadmode.js";
$bootstrap["Files"][]="smis-framework/codemirror/js/markdown-fold.js";
$bootstrap["Files"][]="smis-framework/codemirror/js/markdown.js";
$bootstrap["Files"][]="smis-framework/codemirror/js/match-hightlighter.js";
$bootstrap["Files"][]="smis-framework/codemirror/js/matchbrackets.js";
$bootstrap["Files"][]="smis-framework/codemirror/js/matchtags.js";
$bootstrap["Files"][]="smis-framework/codemirror/js/meta.js";
$bootstrap["Files"][]="smis-framework/codemirror/js/php.js";
$bootstrap["Files"][]="smis-framework/codemirror/js/searchcursor.js";
$bootstrap["Files"][]="smis-framework/codemirror/js/shell.js";
$bootstrap["Files"][]="smis-framework/codemirror/js/show-hint.js";
$bootstrap["Files"][]="smis-framework/codemirror/js/sql.js";
$bootstrap["Files"][]="smis-framework/codemirror/js/sql-hint.js";
$bootstrap["Files"][]="smis-framework/codemirror/js/trailingspace.js";
$bootstrap["Files"][]="smis-framework/codemirror/js/xml-fold.js";
$bootstrap["Files"][]="smis-framework/codemirror/js/sml-hint.js";
$bootstrap["Files"][]="smis-framework/codemirror/js/xml.js";

$content[]=$bootstrap;

$tpt=new TablePrint("Library of User Interface");
$tpt->setTableClass("table table-bordered table-hover table-condensed");
$tpt->setMaxWidth(false);
$tpt->addColumn("<h2>LIST OF THE USER INTERFACE LIBRARY</h2>", "10", "1",null,"","center");
$tpt->commit("title");
foreach($content as $lib){
	$slug=ArrayAdapter::format("slug", $lib['Name']);
	$tpt->addColumn("<h4>".$lib['Name']."</h>", 4, 1,null,"","center");
	$tpt->commit("body","head_lib ".$slug);
	$tpt->addColumn("Version", 1, 1);
	$tpt->addColumn($lib["Version"], 3, 1);
	$tpt->commit("body",$slug." tail");
	$tpt->addColumn("Website", 1, 1);
	$tpt->addColumn("<a target='_blank' href=\"".$lib['Website']."\" >".$lib['Website']."</a>", 3, 1);	
	$tpt->commit("body",$slug." tail");
	$tpt->addColumn("Author", 1, 1);
	$tpt->addColumn($lib["Author"], 3, 1);	
	$tpt->commit("body",$slug." tail");
	$tpt->addColumn("License", 1, 1);
	$tpt->addColumn($lib["License"], 3, 1);	
	$tpt->commit("body",$slug." tail");
	$tpt->addColumn($lib['Description'], 4, 1);
	$tpt->commit("body",$slug." tail");
	$tpt->addColumn("<strong><small>Included Files</small></strong>", 4, 1);
	$tpt->commit("body",$slug." tail");
	
	
	$the_files="<ul>";
	foreach($lib['Files'] as $file){
		$the_files.="<li><i>".$file."</i></li>";
	}
	$the_files.="</ul>";
	$tpt->addColumn($the_files, 4, 1);
	$tpt->commit("body",$slug." tail");
	
}
echo $tpt->getHtml();
?>
