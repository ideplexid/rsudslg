<h4>List of Base File and Folder in Safethree</h4>
<p>this list of file and folder that essential and created by developer, 
in safethree and should not be change or replace in any condition.
changing the file name and file content is dangerous and fully prohibited, 
you should not do that unless you have permission from the developer
</p>

<?php 



require_once 'smis-libs-ui/TableText.php';

$plist=array();
$base=array();
$base["smis-administrator"]=1;
$base["smis-backup"]=1;
$base["smis-base"]=1;
$base["smis-base-css"]=1;
$base["smis-base-js"]=1;
$base["smis-base-res"]=0;
$base["smis-framework/smis"]=1;
$base["smis-framework/bootstrap"]=0;
$base["smis-framework/calendar"]=0;
$base["smis-framework/fa"]=0;
$base["smis-framework/jquery"]=0;
$base["smis-icons"]=0;
$base["smis-libs-class"]=1;
$base["smis-libs-function"]=1;
$base["smis-libs-maps"]=1;
$base["smis-libs-out"]=0;
$base["smis-libs-ui"]=1;
$base["smis-prototype"]=1;
$base["smis-serverbus"]=1;
$base["smis-theme"]=1;
$base["smis-tools"]=1;
$base["smis-upload"]=0;
$base["index.php"]=0;
$base["readme.txt"]=0;
$base["test.php"]=0;

foreach($base as $folder=>$value){
	$plist[$folder]=$folder;
	if($value==1){
		$list=getAllFileInDir($folder,true,false);
		listFullPath($plist, $folder, $list);
	}
}

$table=new TablePrint("");
$table->setDefaultBootrapClass(true);
$table->setMaxWidth(false);
$table	->addColumn("<strong>LOCATION</strong>", 1, 1)
		->addColumn("<strong>TYPE</strong>", 1, 1)
		->commit("header");
foreach($plist as $name=>$location){
	$type="FOLDER";
	if(endsWith($name, ".php")) $type="PHP";
	else if(endsWith($name, ".sql")) $type="SQL";
	else if(endsWith($name, ".css")) $type="CSS";
	else if(endsWith($name, ".js")) $type="JS";
	else if(endsWith($name, ".txt")) $type="TXT";
	$table	->addColumn($location, 1, 1)
			->addColumn($type, 1, 1)
			->commit("body");
	}

echo $table->getHtml();

?>
