<?php
require_once "smis-administrator/class/table/UserLogUITable.php";
if ($_POST ['command'] == "clean") {
	$db->query ( "truncate smis_adm_log;" );
	return "";
}

$header=array ('Action',"DBID",'User','IP','Database','Waktu',"Service","Code");
$uitable = new UserLogUITable ( $header, "User Log");
$uitable->setName ( "userlog" );
$button = new Button ( "", "", "" );
$button->setAction ( "userlog.clean()" );
$button->setIsButton ( Button::$ICONIC );
$button->setIcon("fa fa-trash");
$button->setClass("btn-primary");
$uitable->addFooterButton ( $button );

/* this is respond when system have to response */
if (isset ( $_POST ['command'] )) {
	require_once "smis-administrator/class/adapter/UserLogUIAdapter.php";
	require_once "smis-administrator/class/responder/UserLogResponder.php";
	$adapter = new UserLogUIAdapter ();
	$dbtable = new DBTable ( $db, "smis_adm_log" );
	$dbtable->setOrder ( "id DESC" );	
	if($_POST['database']!=""){
		$dbtable->addCustomKriteria("db", "='".$_POST['database']."'");
	}	
	if($_POST['id']!=""){
		//$dbtable->addCustomKriteria("original", " LIKE '%\"id\":\"".$_POST['id']."\"%' ");
		$dbtable->addCustomKriteria("dbid", " = '".$_POST['id']."' ");
	}	
	if($_POST['dari']!=""){
		$dbtable->addCustomKriteria(NULL, "waktu >='".$_POST['dari']."'");
	}	
	if($_POST['sampai']!=""){
		$dbtable->addCustomKriteria(NULL, "waktu <'".$_POST['sampai']."'");
	}
	$dbres = new UserLogResponder ( $dbtable, $uitable, $adapter );	
	$data = $dbres->command ( $_POST ['command'] );
	echo json_encode ( $data );
	return;
}

$uitable->addModal("database", "text", "Database","");
$uitable->addModal("id", "text", "ID","");
$uitable->addModal("dari", "date", "Dari","");
$uitable->addModal("sampai", "date", "Sampai","");
$form=$uitable->getModal()->getForm();
$button=new Button("", "", "");
$button->setIsButton(Button::$ICONIC);
$button->setAction("userlog.view()");
$button->setClass("btn-primary");
$button->setIcon("fa fa-refresh");
$form->addElement("", $button);

/* This is Modal Form and used for add and edit the table */
echo $form->getHtml();
echo $uitable->getHtml ();
echo addJS ( "framework/smis/js/table_action.js" );
echo addJS("base-js/smis-base-fetreefy-json.js");

echo addJS ( "framework/bootstrap/js/bootstrap-datepicker.js" );
echo addCSS ( "framework/bootstrap/css/datepicker.css" );
echo addCSS("administrator/resource/css/userlog.css");
echo addJS("administrator/resource/js/userlog.js");
?>