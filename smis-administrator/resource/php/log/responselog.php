<?php
require_once "smis-administrator/class/table/ResponseLogUITable.php";
$header=array ('Code','User','IP','Waktu');
$uitable = new ResponseLogUITable ( $header, "Response Log", NULL, true );
$uitable->setName ( "responselog" );
$button = new Button ( "", "", "" );
$button->setAction ( "responselog.clean()" );
$button->setIsButton ( Button::$ICONIC );
$button->setIcon("fa fa-trash");
$button->setClass("btn-primary");
$uitable->addFooterButton ( $button );

if (isset($_POST ['command']) && $_POST ['command'] == "clean") {
	$db->query ( "truncate smis_adm_service_responds;" );
	return "";
}

/* this is respond when system have to response */
if (isset ( $_POST ['command'] )) {
	require_once "smis-administrator/class/adapter/ResponseLogUIAdapter.php";	
	$adapter = new ResponseLogUIAdapter ();
	$dbtable = new DBTable ( $db, "smis_adm_service_responds" );
	$dbtable->setOrder ( "id DESC" );
	$dbres = new DBResponder ( $dbtable, $uitable, $adapter );
	$dbres->setUseAdapterForSelect(true);
	$data = $dbres->command ( $_POST ['command'] );
	echo json_encode ( $data );
	return;
}

/* This is Modal Form and used for add and edit the table */
echo $uitable->getHtml ();
echo addJS ( "framework/smis/js/table_action.js" );
echo addJS("base-js/smis-base-fetreefy-json.js");
echo addJS("administrator/resource/js/responselog.js");
?>
