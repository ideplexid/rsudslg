<?php 
require_once "smis-libs-class/MasterTemplate.php";
require_once "smis-administrator/class/responder/CronjobResponder.php";
global $db;
if ($_POST ['command'] == "clean") {
	$db->query ( "truncate smis_adm_cronjob;" );
	return "";
}
$mas=new MasterTemplate($db,"smis_adm_cronjob","smis-administrator","cronjobtime");
$uitable=$mas->getUItable();
$uitable->setDelButtonEnable(false);
$uitable->setAddButtonEnable(false);
$uitable->setReloadButtonEnable(false);
$uitable->setEditButtonEnable(false);
$uitable->setDetailButtonEnable(true);

$button = new Button ( "", "", "" );
$button->setAction ( "cronjobtime.clean()" );
$button->setIsButton ( Button::$ICONIC );
$button->setIcon("fa fa-trash");
$button->setClass("btn-primary");
$uitable->addFooterButton ( $button );

$uitable->setHeader(array("No.","Waktu","User"));
$adapter=$mas->getAdapter();
$adapter->setUseNumber(true,"No.","back.");
$adapter->add("Waktu","waktu"," date d M Y H:i:s");
$adapter->add("User","user");
$dbres=new CronJobResponder($mas->getDBtable(),$uitable,$adapter);
$mas->setDBresponder($dbres);
$mas->addResouce("js","administrator/resource/js/cronjobtime.js","after",true);
$mas->initialize();
?>