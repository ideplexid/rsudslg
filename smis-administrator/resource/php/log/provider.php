<?php

$header=array ('Waktu','Code Name','Service Code');
$uitable = new Table ( $header, "Synch Provider Log", NULL, true );
$uitable->setName ( "provider" );
$uitable->setAddButtonEnable(false);
$uitable->setEditButtonEnable(false);
$uitable->setReloadButtonEnable(false);
$uitable->setDelButtonEnable(false);
$uitable->setPrintButtonEnable(false);


$button = new Button ( "", "", "" );
$button->setAction ( "provider.clean()" );
$button->setIsButton ( Button::$ICONIC );
$button->setIcon("fa fa-trash");
$button->setClass("btn-primary");
$uitable->addFooterButton ( $button );

$button = new Button ( "", "", "" );
$button->setIsButton ( Button::$ICONIC );
$button->setIcon("fa fa-eye");
$button->setClass("btn-primary");
$uitable->addContentButton("show",$button);


if ($_POST ['command'] == "clean") {
	$db->query ( "truncate smis_adm_log_synch_provider;" );
	return "";
}

/* this is respond when system have to response */
if (isset ( $_POST ['command'] )) {
	$adapter = new SimpleAdapter ();
    $adapter->add("Code Name","code_name");
    $adapter->add("Service Code","service_code");
    $adapter->add("Waktu","waktu","date d M Y H:i:s");
    $adapter->add("Log","log");
    
	$dbtable = new DBTable ( $db, "smis_adm_log_synch_provider");
	$dbtable->setOrder ( "id DESC" );
	$dbres = new DBResponder ( $dbtable, $uitable, $adapter );
	$dbres->setUseAdapterForSelect(true);
	$data = $dbres->command ( $_POST ['command'] );
	echo json_encode ( $data );
	return;
}

/* This is Modal Form and used for add and edit the table */
echo $uitable->getHtml ();
echo addJS ( "framework/smis/js/table_action.js" );
echo addJS("administrator/resource/js/provider.js");

?>