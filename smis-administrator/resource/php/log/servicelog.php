<?php
require_once "smis-administrator/class/table/EntityLogUITable.php";
$header=array ('Waktu','Code','Path');
$uitable = new EntityLogUITable ( $header, "Request Log", NULL, true );
$uitable->setName ( "servicelog" );
$button = new Button ( "", "", "" );
$button->setAction ( "servicelog.clean()" );
$button->setIsButton ( Button::$ICONIC );
$button->setIcon("fa fa-trash");
$button->setClass("btn-primary");
$uitable->addFooterButton ( $button );

if ($_POST ['command'] == "clean") {
	$db->query ( "truncate smis_adm_entity_response;" );
	return "";
}

/* this is respond when system have to response */
if (isset ( $_POST ['command'] )) {
	require_once "smis-administrator/class/adapter/EntityLogUIAdapter.php";
	$adapter = new EntityLogUIAdapter ();
	$dbtable = new DBTable ( $db, "smis_adm_entity_response",array("id","waktu","post","code","path","response") );
	$dbtable->setOrder ( "id DESC" );
	$dbres = new DBResponder ( $dbtable, $uitable, $adapter );
	$dbres->setUseAdapterForSelect(true);
	$data = $dbres->command ( $_POST ['command'] );
	echo json_encode ( $data );
	return;
}

/* This is Modal Form and used for add and edit the table */
echo $uitable->getHtml ();
echo addJS ( "framework/smis/js/table_action.js" );
echo addJS("base-js/smis-base-fetreefy-json.js");
echo addJS("administrator/resource/js/servicelog.js");

?>