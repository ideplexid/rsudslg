<?php
require_once "smis-administrator/class/table/SystemLogUITable.php";
$header	 = array('User',"IP","Time","RTM","Page","Action","Command","Error","Service");
$uitable = new SystemLogUITable($header,"Log of System",NULL,true);
$uitable ->setName("systemlog");
$clean   = new Button("","","");
$clean   ->setAction("systemlog.clean()")
		 ->setIsButton(Button::$ICONIC)
		 ->setIcon("fa fa-trash")
		 ->setClass("btn-primary");
$uitable ->addFooterButton($clean);

if($_POST ['command'] == "clean") {
	$db->query("truncate smis_adm_error;");
	return "";
}

if($_POST ['command'] == "log_system") {	
	$set 	= $_POST ['settings'];
	setSettings($db,"smis_log_system",$set);	
	$pack 	= new ResponsePackage();
	$pack	->setStatus(ResponsePackage::$STATUS_OK)
			->setAlertVisible(true);
	if($set == "true") {
		$string = "Now System Will Logging Error,Trace ,and Save Query of Database";
		$warn 	= ResponsePackage::$TIPE_INFO;
	} else if($set=="false"){
		$string = "Now System Not Anymore Logging Error,Trace ,and Save Query of Database";
		$warn 	= ResponsePackage::$TIPE_INFO;
	}else{
		$string = "System Now Log Only When Error and Warning Exist";
		$warn 	= ResponsePackage::$TIPE_INFO;
	}	
	$pack	->setAlertContent("Settings Saved",$string,$warn);
	echo json_encode($pack->getPackage());
	return;
}

if(isset($_POST ['command'])) {
	require_once "smis-administrator/class/adapter/SystemLogUIAdapter.php";
	require_once "smis-administrator/class/responder/SystemLogResponder.php";
	$adapter 	= new SystemLogUIAdapter();
	$dbtable 	= new DBTable($db,"smis_adm_error");
	$dbtable	->setOrder("id DESC");
	$dbres 		= new SystemLogResponder($dbtable,$uitable,$adapter);
	$data 		= $dbres->command($_POST ['command']);
	echo json_encode($data);
	return;
}

/* This is Modal Form and used for add and edit the table */
echo $uitable->getHtml();
echo addJS("framework/smis/js/table_action.js");
echo addJS("administrator/resource/js/systemlog.js");
?>