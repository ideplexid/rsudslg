<?php 
global $db;
$SUB=strlen(SMIS_DATABASE)+2;
$title="<u>S</u>ilmei <u>i</u>s <u>L</u>ight <u>M</u>anual <u>E</u>d<u>i</u>tor";
$uitable = new Table ($head,"");
$uitable->setName("silmei");
if (isset ( $_POST ['command'] )) {
	if($_POST['command']=="load"){
        $filename=$_POST['filename'];
        $location=$_POST['location'];
        $file=$location."/help/user/".$filename.".php";
        $hasil="<strong>".$file."</strong> Not Exist !!!";
        if(file_exists($file)){
            $hasil=file_get_contents($file);
        }
        $pack=new ResponsePackage();
        $pack->setStatus(ResponsePackage::$STATUS_OK);
        $pack->setContent($hasil);
        echo json_encode($pack->getPackage());
    }else if($_POST['command']=="deploy"){
        $filename=$_POST['filename'];
        $location=$_POST['location'];
        $file=$location."/help/user/".$filename.".php";
        $hasil="Telah Disimpan pada File <strong>".$filename."</strong>";
        if(!file_put_contents($file,$_POST['hasil'])){
           $hasil="Gagal Menyimpan File <strong>".$filename."</strong>, Pastikan Folder <strong>".$location."/help/user/</strong> dan aksesnya Writeable"; 
        }
        $pack=new ResponsePackage();
        $pack->setStatus(ResponsePackage::$STATUS_OK);
        $pack->setContent("");
        $pack->setWarning(true,"Peringatan",$hasil);
        echo json_encode($pack->getPackage());
    }
    return;
}


$data=getAllFileInDir("./",false,false,false);
$options=new OptionBuilder();
$options->add("","","1");
foreach($data as $x){
    if(strpos($x,"smis")!==false || strpos($x,".")!==false){
        continue;
    }
    $options->add($x,$x,"0");
}
$uitable->addModal("location","select","Location",$options->getContent(),"n");
$uitable->addModal("filename","text","File Name","","n");

$button=new Button("","","Deploy");
$button->setClass("btn-primary");
$button->setAction("silmei.deploy()");
$button->setIsButton(Button::$ICONIC_TEXT);
$button->setIcon(" fa fa-download");

$load=new Button("","","Load");
$load->setClass("btn-success");
$load->setAction("silmei.load()");
$load->setIsButton(Button::$ICONIC_TEXT);
$load->setIcon(" fa fa-square");

$modal=$uitable->getModal();
$modal->addHTML("<div class='clear'><pre id='silmei_code'></pre></div>","after");
$modal->clearFooter();

$btngrup=new ButtonGroup();
$btngrup->addButton($button);
$btngrup->addButton($load);

$form=$modal->getForm();
$form->setTitle($title);
$form->addElement("",$btngrup);
$form->addElement("","<div id='silmei-summernote'></div>");

echo $form->getHtml();
echo addJS ( "framework/smis/js/table_action.js" );
echo addJS ( "administrator/resource/js/silmei.js" );
?>

