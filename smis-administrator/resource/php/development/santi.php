<?php 
global $db;
$SUB=strlen(SMIS_DATABASE)+2;
$title="<u>S</u>anti is <u>a</u> <u>N</u>ative <u>T</u>erminal <u>I</u>nterpreter";
$uitable = new Table ($head,"");
$uitable->setName("santi");
if (isset ( $_POST ['command'] )) {
	if($_POST['command']=="runcode"){
        $source=$_POST['source'];
        if(startsWith($source,"<?php")){
            $source=substr($source,5);
        }
        if(endsWith($source,"?>")){
            $source=substr($source,0,-2);
        }
        
        try{
            ob_start();
            eval($source);   
            $hasil=ob_get_clean(); 
        }catch(Exception $e){
            $hasil=$e->getMessage();
        }
        $pack=new ResponsePackage();
        $pack->setStatus(ResponsePackage::$STATUS_OK);
        $pack->setContent("");
        $pack->setWarning(true,"Exection Result !!",$hasil);
        echo json_encode($pack->getPackage());
    }
    return;
}


$theme=new OptionBuilder();
$theme->addSingle("default");
$theme->addSingle("3024-night");
$theme->addSingle("blackboard");
$theme->addSingle("eclipse","1");
$theme->addSingle("erlang-dark");
$theme->addSingle("lesser-dark");
$theme->addSingle("liquibyte");
$theme->addSingle("night");
$theme->addSingle("the-matrix");
$theme->addSingle("zenburn");	

$opsi=new OptionBuilder();
$opsi->add("On","y","1");
$opsi->add("Off","n","0");

$line=new OptionBuilder();
$line->add("On","y","0");
$line->add("Off","n","1");

$line=new OptionBuilder();
$line->add("On","y","0");
$line->add("Off","n","1");

$is=new OptionBuilder();
$is->add("Yes","true","0");
$is->add("No","false","0");

$uitable->addModal("theme", "select", "Theme", $theme->getContent());
$uitable->addModal("linenumber", "select", "Line Number", $opsi->getContent());
$uitable->addModal("linewrap", "select", "Line Wrapping", $line->getContent());
$uitable->addModal("actag", "select", "Auto Tag", $opsi->getContent());
$uitable->addModal("acbrac", "select", "Auto Bracket", $opsi->getContent());
$uitable->addModal("aline", "select", "Active Line", $opsi->getContent());
$uitable->addModal("tspace", "select", "Trailing Space", $opsi->getContent());
$uitable->addModal("foldgutter", "select", "Fold Gutter", $opsi->getContent());
$modal=$uitable->getModal();

$button=new Button("","","Run");
$button->setClass("btn-primary");
$button->setAction("santi.runcode()");
$button->setIsButton(Button::$ICONIC_TEXT);
$button->setIcon(" fa fa-play-circle");

$form=$modal->getForm();
$form->setTitle($title);
$form->addElement("",$button);

echo $form->getHtml();
echo "<div class='clear'></div>";
echo "<div>";
	echo "<div id='santi-sceditormirror'></div>";
echo "</div>";

echo addCSS("framework/codemirror/css/codemirror.css");
echo addCSS("framework/codemirror/css/show-hint.css");
echo addCSS("framework/codemirror/css/foldgutter.css");
echo addCSS("framework/codemirror/css/fullscreen.css");
echo addCSS("framework/codemirror/css/3024-night.css");
echo addCSS("framework/codemirror/css/blackboard.css");
echo addCSS("framework/codemirror/css/eclipse.css");
echo addCSS("framework/codemirror/css/erlang-dark.css");
echo addCSS("framework/codemirror/css/lesser-dark.css");
echo addCSS("framework/codemirror/css/liquibyte.css");
echo addCSS("framework/codemirror/css/night.css");
echo addCSS("framework/codemirror/css/the-matrix.css");
echo addCSS("framework/codemirror/css/zenburn.css");
echo addCSS("smis-base-css/smis-base-source.css",false);

echo addJS("framework/codemirror/js/codemirror.js");
echo addJS("framework/codemirror/js/xml-fold.js");
echo addJS("framework/codemirror/js/matchbrackets.js");
echo addJS("framework/codemirror/js/closebrackets.js");
echo addJS("framework/codemirror/js/matchtags.js");
echo addJS("framework/codemirror/js/closetag.js");
echo addJS("framework/codemirror/js/trailingspace.js");
echo addJS("framework/codemirror/js/active-line.js");
echo addJS("framework/codemirror/js/htmlmixed.js");
echo addJS("framework/codemirror/js/xml.js");
echo addJS("framework/codemirror/js/css.js");
echo addJS("framework/codemirror/js/clike.js");
echo addJS("framework/codemirror/js/php.js");
echo addJS("framework/codemirror/js/javascript.js");
echo addJS("framework/codemirror/js/show-hint.js");
echo addJS("framework/codemirror/js/xml-hint.js");
echo addJS("framework/codemirror/js/html-hint.js");
echo addJS("framework/codemirror/js/anyword-hint.js");
echo addJS("framework/codemirror/js/css-hint.js");
echo addJS("framework/codemirror/js/javascript-hint.js");
echo addJS("framework/codemirror/js/sql-hint.js");
echo addJS("framework/codemirror/js/foldcode.js");
echo addJS("framework/codemirror/js/foldgutter.js");
echo addJS("framework/codemirror/js/brace-fold.js");
echo addJS("framework/codemirror/js/xml-fold.js");
echo addJS("framework/codemirror/js/markdown-fold.js");
echo addJS("framework/codemirror/js/comment-fold.js");
echo addJS("framework/codemirror/js/comment.js");
echo addJS("framework/codemirror/js/continuecomment.js");
echo addJS("framework/codemirror/js/markdown.js");
echo addJS("framework/codemirror/js/fullscreen.js");
echo addJS("framework/codemirror/js/loadmode.js");
echo addJS("framework/codemirror/js/meta.js");
echo addJS("framework/codemirror/js/shell.js");
echo addJS("framework/codemirror/js/sql.js");
echo addJS("framework/codemirror/js/searchcursor.js");
echo addJS("framework/codemirror/js/match-highlighter.js");
echo addJS("smis-base-js/smis-base-source-editor.js",false);
echo addJS ( "framework/smis/js/table_action.js" );
echo addJS ( "administrator/resource/js/santi.js" );
echo addCSS ( "administrator/resource/css/santi.css" );
?>


