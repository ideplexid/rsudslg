<?php 
global $db;
$SUB=strlen(SMIS_DATABASE)+2;
$head=array("No","Nama","Total","Tipe","Engine","Version","Format","Waktu");
$uitable = new Table ($head,"<u>R</u>ireist <u>i</u>s <u>R</u>everse <u>E</u>ngineer of <u>S</u>MIS <u>T</u>able");

$btn=new Button("","","Reverse");
$btn->setClass(" btn-primary");
$btn->setIsButton(Button::$ICONIC_TEXT);
$btn->setIcon("fa fa-file-code-o");

$uitable->setName ( "rireist" )
		->setAddButtonEnable(false)
		->setReloadButtonEnable(false)
		->setEditButtonEnable(false)
		->setPrintButtonEnable(false)
		->setDelButtonEnable(false);
$uitable->addContentButton("reverse",$btn);
if (isset ( $_POST ['command'] )) {
	require_once "smis-administrator/class/responder/RireistResponder.php";
	$adapter = new SimpleAdapter ();
	$adapter->add("Nama", "table_name")
			->add("Total", "table_rows","number")
			->add("Waktu", "time","date d M Y, H:i:s")
			->add("Tipe", "table_type")
			->add("Engine", "engine")
			->add("Version", "version")
			->add("Format", "format")				
			->setUseNumber(true,"No");
	$dbtable = new DBTable ( $db, "smis_table" );
	
	$dbtable->setFetchMethode ( DBTable::$ARRAY_FETCH );
    $dbres = new RireistResponder ( $dbtable, $uitable, $adapter );
	$data = $dbres	->setUseAdapterForSelect(true)
		  		 	->command ( $_POST ['command'] );
	echo json_encode ( $data );
	return;
}


$data=getAllFileInDir("./",false,false,false);
$options=new OptionBuilder();
$options->add("","","1");
foreach($data as $x){
    if(strpos($x,"smis")!==false || strpos($x,".")!==false){
        continue;
    }
    $options->add($x,$x,"0");
}
$uitable->addModal("table","text","Table","","n");
$uitable->addModal("deploy","select","Deploy In",$options->getContent(),"n");

$button=new Button("","","Deploy");
$button->setClass("btn-primary");
$button->setAction("rireist.deploy()");
$button->setIsButton(Button::$ICONIC_TEXT);
$button->setIcon(" fa fa-building");
$modal=$uitable->getModal();
$modal->addHTML("<div class='clear'><pre id='rireist_code'></pre></div>","after");
$modal->clearFooter();
$modal->addFooter($button);
$modal->setTitle("Rireist");
echo $modal->getHtml();
echo $uitable->getHtml ();
echo addJS ( "framework/smis/js/table_action.js" );
echo addJS ( "administrator/resource/js/rireist.js" );
?>

