<?php 
global $db;
global $TIME_START;

$title="<u>R</u>iscti <u>i</u>s <u>S</u>erver <u>C</u>onnection <u>T</u>est <u>I</u>nformation";
$head=array("No.","Activity","Packet Data","Packet Send","Send Time (s)","Packet Receive","Process Time (s)","Packet Response","Response Time (s)","Packet Received");
$uitable = new Table ($head,"");
$uitable->setName("riscti");
$uitable->setFooterVisible(false);
$uitable->setActionEnable(false);
if (isset ( $_POST ['command'] )) {
	$start  = ArrayAdapter::format("date d M Y H:i:s",$TIME_START);  
    $result=array();
    $result['activity']="HTTP Request";
    if($_POST ['command']==2){
        $result['return']=$_POST['packet'];
        $result['activity']="Resend Back";
    }else if($_POST ['command']==3 || $_POST ['command']==4){        
        require_once "smis-base/smis-include-service-consumer.php";
        $result['activity']="Encrypt";
        $data=$_POST['packet'];
        $encrypt=new Encryption();
        $encrypt->setKey("system_testing");
        $encrypt->setText($data);
        $encrypt->encrypt();
        $result_enc=$encrypt->getCypher();
        if($_POST ['command']==4){
            $result['activity']="Encrypt-Decrypt";
            $encrypt->setCypher($result_enc);
            $encrypt->decrypt();
            $result_enc=$encrypt->getText();
        }
        $result['return']=$result_enc;
    }else if($_POST ['command']==5){
        $result['activity']="Service";
        require_once "smis-base/smis-include-service-consumer.php";
        require_once "smis-libs-class/ServiceProviderList.php";
        $serv=new ServiceProviderList($db,"synchronize_provider.php");
        $serv->execute();
        $result['return']=$serv->getContent();
    }
  
    $ending = microtime(true);
    $end    = ArrayAdapter::format("date d M Y H:i:s",$TIME_START);
    $diff   = round($ending-$TIME_START,5);
    
    
    $result['micro_receive']=$TIME_START;
    $result['micro_response']=$ending;
    
    $result['ttime_receive']=$start;
    $result['process_time']=$diff;
    $result['time_response']=$end;
    
    $pack=new ResponsePackage();
    $pack->setStatus(ResponsePackage::$STATUS_OK);
    $pack->setContent($result);
    echo json_encode($pack->getPackage());
    return;
}

$filter=new OptionBuilder();
$filter->add("Show All","0",1);
$filter->add("Send Time","1",0);
$filter->add("Process Time","2",0);
$filter->add("Send & Process Time","3",0);
$filter->add("Receive Time","4",0);
$filter->add("Send & Receive Time","5",0);
$filter->add("Process & Receive Time","6",0);
$filter->add("Send, Process & Receive Time","7",0);

$packet=new OptionBuilder();
$packet->add("0 B - No Packet", 0,0);
$packet->add("100 B - Tiny Packet", 100,0);
$packet->add("500 B - Mini Packet", 500,0);
$packet->add("1 KB - Little Packet", 1024,0);
$packet->add("100 KB - Normal Packet", 102400,1);
$packet->add("500 KB - Middle", 512000,0);
$packet->add("1 MB - Big Packet", 1048576,0);

$periode=new OptionBuilder();
$periode->add("100 ms - 10/s - Brute", 100,0);
$periode->add("250 ms - 4/s - Less Brute", 250,0);
$periode->add("500 ms - 2/s - Nearly Brute", 500,0);
$periode->add("800 ms - 5/4s - Default", 800,1);
$periode->add("1000 ms - 1/s - Normal", 1000,0);
$periode->add("3000 ms - 1/3s - Slow", 3000,0);
$periode->add("5000 ms - 1/5s - Slower", 5000,0);

$activity=new OptionBuilder();
$activity->add("HTTP Request", 1,1);
$activity->add("Resend Back", 2,0);
$activity->add("Encrypt", 3,0);
$activity->add("Encrypt-Decrypt", 4,0);
$activity->add("Service", 5,0);

$uitable->addModal("packet_data","select","Packet Data",$packet->getContent(),"n");
$uitable->addModal("running_count","text","Running Count","0","n",null,true);
$uitable->addModal("send_limit","text","Send Time","0.4","n",null,false);
$uitable->addModal("process_limit","text","Process Time","0.5","n",null,false);
$uitable->addModal("receive_limit","text","Response Time","0.6","n",null,false);
$uitable->addModal("filter","select","Filter",$filter->getContent(),"n",null,false);
$uitable->addModal("periode","select","Periode",$periode->getContent(),"n",null,false);
$uitable->addModal("activity","select","Activity",$activity->getContent(),"n",null,false);

$play=new Button("","","Run");
$play->setClass("btn-primary");
$play->setAction("riscti.run()");
$play->setIsButton(Button::$ICONIC_TEXT);
$play->setIcon(" fa fa-play");

$reset=new Button("","","Reset");
$reset->setClass("btn-success");
$reset->setAction("riscti.reset()");
$reset->setIsButton(Button::$ICONIC_TEXT);
$reset->setIcon(" fa fa-eject");

$stop=new Button("","","Stop");
$stop->setClass("btn-danger");
$stop->setAction("riscti.stop()");
$stop->setIsButton(Button::$ICONIC_TEXT);
$stop->setIcon(" fa fa-stop");

$clean=new Button("","","Clean");
$clean->setClass("btn-warning");
$clean->setAction("riscti.clean()");
$clean->setIsButton(Button::$ICONIC_TEXT);
$clean->setIcon(" fa fa-trash");

$modal=$uitable->getModal();
$modal->clearFooter();

$btngrup=new ButtonGroup();
$btngrup->addButton($play);
$btngrup->addButton($reset);
$btngrup->addButton($stop);
$btngrup->addButton($clean);
$btngrup->setMax(100,"");

$form=$modal->getForm();
$form->setTitle($title);
$form->addElement("",$btngrup);

echo $form->getHtml();
echo $uitable->getHtml();
echo addJS ( "framework/smis/js/table_action.js" );
echo addJS ( "administrator/resource/js/riscti.js" );
?>

