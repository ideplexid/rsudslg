<?php 

require_once "smis-libs-out/linfo/init.php";


if(!isset($_POST['command'])){
	echo addCSS("administrator/resource/css/hardinfo.css");
	echo addJS("administrator/resource/js/hardinfo.js");
	$reload=new Button("", "", "Reload");
	$reload->setIsButton(Button::$ICONIC_TEXT);
	$reload->setAction("reload_hard_info()");
	$reload->setIcon("fa fa-refresh");
	$reload->setClass("btn-primary");
	
	$realtime=new Button("", "", "Realtime");
	$realtime->setIsButton(Button::$ICONIC_TEXT);
	$realtime->setAction("realtime_hard_info()");
	$realtime->setIcon("fa fa-recycle");
	$realtime->setClass("btn-danger");
	
	$stop=new Button("", "", "Stop");
	$stop->setIsButton(Button::$ICONIC_TEXT);
	$stop->setAction("stop_real_time()");
	$stop->setIcon("fa fa-stop");
	$stop->setClass("btn-success");
	
	$btng=new ButtonGroup("");
	$btng->addButton($reload);
	$btng->addButton($realtime);
	$btng->addButton($stop);
	
	echo "<div class='hardinfo' id='hardinfo_inside' >";
	echo "</div>";
	
	echo "<div class='hide noprint' id='btn_hardinfo'>";
		echo $btng->getHtml();
	echo "</div>";
}else{
	$var="";
	try {
		ob_start();
		$linfo = new Linfo();
		$linfo->scan();
		$linfo->output();
		$var=ob_get_clean();
	}catch (LinfoFatalException $e) {
		$var=$e->getMessage()."\n";
	}
	
	$res=new ResponsePackage();
	$res->setStatus(ResponsePackage::$STATUS_OK);
	$res->setContent($var);
	echo json_encode($res->getPackage());
}

?>