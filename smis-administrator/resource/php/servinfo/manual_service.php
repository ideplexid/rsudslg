<?php 

if(isset($_POST['command']) && $_POST['command']='desc'){
	$plugins=$_POST['plugins'];
	$service=$_POST['service'];
	require_once $plugins.'/help/service_consumer/'.$service.".php";
	return;
	
}


$tree=new Tree("manual_service", "Manual Service", "");
$v=getAllPlugins();
ksort($v);
foreach ($v as $plugins=>$content){
	$id=$plugins;
	$name=ArrayAdapter::format("unslug", $plugins);
	$dir=$id."/help/service_consumer/";
	if(file_exists($dir)){	
		$tplug=new Tree($id,$name, "");
		$tplug->setClass("badge badge-info");
		$tplug->setIcon("fa fa-plug");
		$list_file=getAllFileInDir($dir);
		foreach($list_file as $fname){
			$filename = preg_replace('/\\.[^.\\s]{3,4}$/', '', $fname);
			$nm="<a href='#' onclick=\"manual_service('".$id."','".$filename."')\">".$filename."</a>";
			$tfile=new Tree($filename, "", $nm);
			$tplug->addChild($tfile);
		}
		if($tplug->countChild()>0)
			$tree->addChild($tplug);
	}
}

$the_tree=$tree->getHtml();
$the_page="<div id='manual_service_page'></div>";

$rowspan=new RowSpan();
$rowspan->addSpan($the_tree,3,RowSpan::$TYPE_HTML);
$rowspan->addSpan($the_page,9,RowSpan::$TYPE_HTML,"","well");

echo $rowspan->getHtml();

?>

<script type="text/javascript">
	function manual_service(plugins,service){
		var data={
			page:"smis-administrator",
			action:"manual_service",
			command:"desc",
			plugins:plugins,
			service:service
			};
		showLoading();
		$.post("",data,function(res){
			$("#manual_service_page").html(res);
			dismissLoading();
		});
	}
	
</script>