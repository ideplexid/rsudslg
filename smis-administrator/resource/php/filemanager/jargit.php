<?php 
	global $db;
	
	if(isset($_POST['command'])){
		require_once "smis-libs-out/Git.php";
		$response=new ResponsePackage();
		$response->setStatus(ResponsePackage::$STATUS_OK);
		
		if($_POST['command']=="save"){
			$user=$_POST["username"];
			$url=$_POST["url"];
			$branch=$_POST['branch'];
			$result="Saving Jargit Setting ....start\n";
			setSettings($db, "smis-jargit-username", $user);
			setSettings($db, "smis-jargit-repository", $url);
			setSettings($db, "smis-jargit-branch", $branch);
			$result.="Saving Jargit Setting ....done\n";
			$response->setContent($result);
		}else if($_POST['command']=="pull"){
			$git=Git::open('./');
			$user=$_POST["username"];
			$pass=$_POST["password"];
			$url=$_POST["url"];
			$branch=$_POST['branch'];			 
			ob_start();
			echo "pull to https://".$user."@".$url."  ".$branch."..... start\n";
			echo $git->pull("https://".$user.":".$pass."@".$url, $branch);
			echo "pull to https://".$user."@".$url."  ".$branch."..... done\n";
			$hasil=ob_get_clean();
			$response->setContent($hasil);
		}else if($_POST['command']=="push"){
			$git=Git::open('./');
			$user=$_POST["username"];
			$pass=$_POST["password"];
			$url=$_POST["url"];
			$branch=$_POST['branch'];			 
			ob_start();
			echo "push to https://".$user."@".$url."  ".$branch."..... start\n";
			echo $git->push("https://".$user.":".$pass."@".$url, $branch);
			echo "push to https://".$user."@".$url."  ".$branch."..... done\n";
			$hasil=ob_get_clean();
			$response->setContent($hasil);
		}else if($_POST['command']=="commit"){
			$git=Git::open('./');
			$comment=$_POST["comment"];
			$option=$_POST["option"];
			ob_start();
			echo "commit with comment \"".$comment."\" and option \"".$option."\"....start\n";
			echo $git->commit($comment,$option);
			echo "commit with comment \"".$comment."\" and option \"".$option."\" ....done\n";
			$hasil=ob_get_clean();
			$response->setContent($hasil);
		}else if($_POST['command']=="add"){
			$git=Git::open('./');
			$folder=$_POST['folder'];			 
			ob_start();
			echo "add file and folder...start\n";
			echo $git->add($folder);
			echo "add file and folder.... done\n";
			$hasil=ob_get_clean();
			$response->setContent($hasil);
		}
		
		echo json_encode($response->getPackage());
		return;
	}
	
	
	//echo $repo->pull("https://goblooge:eka_safitri@bitbucket.org/goblooge/unpatma.git", "master");
	//push --tags https://goblooge:eka_safitri@bitbucket.org/goblooge/unpatma.git master
	//$hasil=$repo->push("https://goblooge:eka_safitri@bitbucket.org/goblooge/unpatma.git", "master");
	//$repo->push("origin","master");	
	
	
	
	$username=getSettings($db, "smis-jargit-username", "goblooge");
	$password=getSettings($db, "smis-jargit-password", "eka_safitri");
	$branch=getSettings($db, "smis-jargit-branch", "master");
	$repository=getSettings($db, "smis-jargit-repository", "bitbucket.org/goblooge/rsukaliwates.git");
	
	$action=new OptionBuilder();
	$action->add("Pull","pull","1");
	$action->add("Add","add","0");
	$action->add("Commit","commit","0");
	$action->add("Push","push","0");
	
	$head=array();
	$uitable=new Table($head);
	$uitable->setName("jargit");
	$uitable->addModal("username", "text", "Username", $username)
			->addModal("password", "password", "Password", $password)
			->addModal("url", "text", "Repository", $repository)
			->addModal("command", "select", "Command", $action->getContent())
			->addModal("comment", "text", "Comment", "")
			->addModal("branch", "text", "Branch", $branch)
			->addModal("folder", "text", "Unit", "./")
			->addModal("option", "text", "Option", "-A","y");
	$form=$uitable->getModal()->getForm();
	
	$run=new Button("run", "", "Run");
	$run->setIsButton(Button::$ICONIC_TEXT);
	$run->setClass("btn-primary");
	$run->setIcon("fa fa-flash");
	$run->setAction("jargit.run()");
	
	$clear=new Button("clear", "", "Clear");
	$clear->setIsButton(Button::$ICONIC_TEXT);
	$clear->setClass("btn-primary");
	$clear->setIcon("fa fa-paint-brush");
	$clear->setAction("jargit.clear_action()");
	
	$save=new Button("pull", "", "Save");
	$save->setIsButton(Button::$ICONIC_TEXT);
	$save->setClass("btn-primary");
	$save->setIcon("fa fa-save");
	$save->setAction("jargit.save_action()");
	
	$btn=new ButtonGroup("");
	$btn->setMax(10, "");
	$btn->addButton($run);
	$btn->addButton($save);
	$btn->addButton($clear);
	$form->addElement("", $btn);
	
	echo $form->getHtml();
	echo "<div class='clear'></div>";
	echo "<pre id='jargit_msg'></pre>";
	addJS("framework/smis/js/table_action.js");
?>

<script type="text/javascript">
	var jargit;
	$(document).ready(function(){
		var a=new Array("username","password","url","branch","folder","option","comment","command");
		jargit=new TableAction("jargit","smis-administrator","jargit",a);
		jargit.save_action=function(d){
			var d=this.getSaveData();
			d['command']="pull";
			$.post("",d,function(res){
				var json=getContent(res);
				$("#jargit_msg").append(json);
			});
		};

		jargit.run=function(d){
			if(!jargit.checking()) return;
			var d=this.getSaveData();
			d['command']=$("#jargit_command").val();
			showLoading();
			$.post("",d,function(res){
				var json=getContent(res);
				$("#jargit_msg").append(json);
				dismissLoading();
			});
		};

		jargit.checking=function(){
			$("#jargit_folder").removeClass("error_field");
			$("#jargit_branch").removeClass("error_field");
			$("#jargit_comment").removeClass("error_field");
			
			var c=$("#jargit_command").val();
			if((c=="pull" || c=="push")  && $("#jargit_branch").val()==""){
				$("#jargit_branch").addClass("error_field");
				return false;
			}

			if(c=="add" &&  $("#jargit_folder").val()==""){
				$("#jargit_folder").addClass("error_field");
				return false;
			}

			if(c=="commit" &&  $("#jargit_comment").val()==""){
				$("#jargit_comment").addClass("error_field");
				return false;
			}

			return true;	
		};

		jargit.save_action=function(d){
			var d=this.getSaveData();
			d['command']="save";
			showLoading();
			$.post("",d,function(res){
				var json=getContent(res);
				$("#jargit_msg").append(json);
				dismissLoading();
			});
		};

		jargit.choose=function(){
			var command=$("#jargit_command").val();
			$("#jargit_folder, #jargit_branch, #jargit_option, #jargit_comment").prop("disabled",true);
			if(command=="push" || command=="pull"){
				$("#jargit_branch").prop("disabled",false);
			}if(command=="add"){
				$("#jargit_folder, #jargit_option").prop("disabled",false);
			}else if(command=="commit"){
				$("#jargit_comment").prop("disabled",false);
			}
		}

		jargit.clear_action=function(d){
			$("#jargit_msg").html("");
		};


		$("#jargit_command").on("change",function(){jargit.choose();});
		jargit.choose();
	});

</script>
