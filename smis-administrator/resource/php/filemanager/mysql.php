<?php 
global $wpdb;
if(isset($_POST['sql'])){
	
	$ct=array();
	$ct['log']="";
	$ct['result']="";
	
	$result=null;
	$sql=trim($_POST['sql']);
	$is_select=startsWith(strtolower($sql), "select"); 
	$is_limit=strpos(strtolower($sql),'limit') !== false;
	$is_tk= endsWith($sql,";");
	
	if($is_select && !$is_limit) {
		$ct['log'].="* Adding Limit 0,1 because is Select \n";
		if($is_tk) $sql=rtrim($sql, ";");
		$sql.=" limit 0,10;";
	}
	$start = microtime(true);
	
	if($is_select)	$result=$wpdb->get_results($sql,false);
	else $result=$wpdb->query($sql);
	
	$end = microtime(true);
	$ct['log'].="* Exceution Time :  " . ($end - $start) . " ms. \n";
	$error=$wpdb->getLastError();
	if($error!="") {
		$error=str_replace("\n", "", $error);
		$error=str_replace("<br>", "", $error);
		$error=str_replace("<font class='diff'>", "", $error);
		$error=str_replace("</font>", "", $error);		
		$error=str_replace("When executing:", " \n* Query :  ", $error);
		$ct['log'].="* ".$error;		
	}else{
		$ct['log'].="- - - - - - - - - - - - - - - - - - - - - - - - \n".$sql;
		$head=array();
		$adapter=new SimpleAdapter();
		foreach($result[0] as $k=>$v){
			if(is_string($k)) {
				$head[]=$k;
				$adapter->add($k, $k);
			}
		}
		$content=$adapter->getContent($result);
		$uitable=new Table($head,"",$content,false);
		$uitable->setFooterVisible(false);
		$ct['result']=$uitable->getHtml();
	}
	$response=new ResponsePackage();
	$response->setStatus(ResponsePackage::$STATUS_OK);
	$response->setContent($ct);
	echo json_encode($response->getPackage());	
	return;
}

$theme=new OptionBuilder();
$theme->addSingle("default");
$theme->addSingle("3024-night");
$theme->addSingle("blackboard");
$theme->addSingle("eclipse","1");
$theme->addSingle("erlang-dark");
$theme->addSingle("lesser-dark");
$theme->addSingle("liquibyte");
$theme->addSingle("night");
$theme->addSingle("the-matrix");
$theme->addSingle("zenburn");

$select=new Select("mysqleditor_theme", "Theme", $theme->getContent());
$textarea=new TextArea("mysqleditor", "Script", "");
$button=new Button("run", "Run", "Go");
$button->setClass("btn-primary");
$button->setIcon("fa fa-circle-o");
$button->setAction("mysql.runscript()");
$button->setIsButton(Button::$ICONIC);

$form=new Form("", "", "");
$form->addElement("", $select);
$form->addElement("", $button);
echo $form->getHtml();
echo "</br>";
echo "<div class='well' id='mysqleditor'></div>";
echo "</br>";

$d=new RowSpan();
echo "<pre id='mysql_status'></pre>";
echo "<div id='mysql_result'></div>";
echo $d->getHtml();

?>

<script type="text/javascript">
var mysqleditor;
var mysql;
CodeMirror.commands.autocomplete = function(cm) {
		cm.showHint({hint: CodeMirror.hint.anyword});
}
	
$(document).ready(function(){	
	mysql=new TableAction("mysql","smis-administrator","mysql");
	mysql.runscript=function(){
		var d=this.getRegulerData();
		d['sql']=mysqleditor.getValue();
		showLoading();
		$.post("",d,function(res){
			var json=getContent(res);
			if(json!=null){
				$("#mysql_status").html(json.log);
				$("#mysql_result").html(json.result);
			}else{
				$("#mysql_status").html("");
				$("#mysql_result").html("");
			}
			dismissLoading();
		});
	};
	
	mysqleditor = CodeMirror(document.getElementById("mysqleditor"), {
	  value: "",
	  indentUnit:5,
	  mode: "text/x-mysql",
      tabMode: "indent",
      matchBrackets: true,
	  lineNumbers: true,
	  autofocus:true,
      styleActiveLine: true,
	  viewportMargin: Infinity,
	  showTrailingSpace: true,
	  highlightSelectionMatches: {showToken: /\w/},
	  extraKeys: {"Ctrl-Space": "autocomplete","Ctrl-F11": function(cm) {cm.setOption("fullScreen", !cm.getOption("fullScreen"));}},
	  foldGutter: true,
	  gutters: ["CodeMirror-linenumbers", "CodeMirror-foldgutter","breakpoints"]		  
	});
	mysqleditor.setSize("100%", "200");

	mysqleditor.on("gutterClick", function(cm, n) {
	  var info = cm.lineInfo(n);
	  cm.setGutterMarker(n, "breakpoints", info.gutterMarkers ? null : makeMarker());
	});
	
	$("#mysqleditor_theme").on("change",function(){
		mysqleditor.setOption("theme", this.value);
	});
	mysqleditor.setOption("theme", $("#mysqleditor_theme").val());
});
</script>

<style type="text/css">
#script{ width:100%; height:100px; }
#mysql_result{ width: auto; height:auto; overflow-x:auto; }
</style>