<?php 
loadLibrary("smis-libs-function-file");
require_once 'smis-framework/smis/filebase/FileResponder.php';
require_once 'smis-framework/smis/filebase/FileAdapter.php';

$header=array("&nbsp;","Name","Size","Owner","User","Permission","Filetype","Type","Visibility","Action");
$uitable=new Table($header);
$uitable->setAction(false);
$uitable->setFooterVisible(false);
$uitable->setName("salman");

if(isset($_POST['command'])){
	$adapter=new FileAdapter("salman");
	$responder=new FileResponder("./","salman",$uitable,$adapter);
	$result=$responder->command($_POST['command']);
	echo json_encode($result);
	return;
}

$text=new Text("salman_folder", "Directory", $BASE);
$up=new Button("", "", "Up");
$up->setIsButton(Button::$ICONIC_TEXT);
$up->setIcon("fa fa-level-up");
$up->setClass("btn-primary");
$up->setAction("salman.levelup()");

$show=new Button("", "", "Open");
$show->setIsButton(Button::$ICONIC_TEXT);
$show->setIcon("fa fa-folder-open");
$show->setClass("btn-primary");
$show->setAction("salman.view()");

$add=new Button("", "", "New");
$add->setIsButton(Button::$ICONIC_TEXT);
$add->setIcon("fa fa-plus-circle");
$add->setClass("btn-primary");
$add->setAction("salman.show_add_form()");

$btg=new InputGroup("");
$btg->addComponent($text);
$btg->addComponent($show);
$btg->addComponent($up);
$btg->addComponent($add);


$form=new Form("salman_form", "", "<u>S</u>alman is <u>a</u> <u>L</u>igth File <u>Man</u>ager");
$form->addElement("Directory", $btg);

$type=new OptionBuilder();
$type->add("File");
$type->add("Directory");

 $output = array();
 $command = "cat /etc/passwd | cut -d\":\" -f1";
 $output =shell_exec($command);
 $userid=new OptionBuilder();
 $theuser=explode("\n", $output);
foreach ($theuser as $u){
	if($u!="")	$userid->addSingle($u,"www-data"==$u);
}
 
$uitable->addModal("oldname", "hidden", "", "");
$uitable->addModal("filename", "text", "Name", "","n");
$uitable->addModal("filetype", "select", "Type", $type->getContent(),"n");
$uitable->addModal("filemode", "text", "Permission", "644","n");
$uitable->addModal("fileowner", "select", "File Owner", $userid->getContent(),"n");
$uitable->addModal("isold", "hidden", "", "0");

echo $form->getHtml();
echo $uitable->getModal()->setTitle("File / Folder")->getHtml();
echo "<div class='clear'></div>";
echo $uitable->getHtml();
echo addJS("framework/smis/js/table_action.js");
echo addJS("framework/smis/js/file_action.js");
echo addJS("administrator/resource/js/salman.js");
echo addCSS("administrator/resource/css/salman.css");
?>
