<?php 
	echo addCSS("framework/codemirror/css/codemirror.css");
	echo addCSS("framework/codemirror/css/show-hint.css");
	echo addCSS("framework/codemirror/css/foldgutter.css");
	echo addCSS("framework/codemirror/css/fullscreen.css");
	echo addCSS("framework/codemirror/css/3024-night.css");
	echo addCSS("framework/codemirror/css/blackboard.css");
	echo addCSS("framework/codemirror/css/eclipse.css");
	echo addCSS("framework/codemirror/css/erlang-dark.css");
	echo addCSS("framework/codemirror/css/lesser-dark.css");
	echo addCSS("framework/codemirror/css/liquibyte.css");
	echo addCSS("framework/codemirror/css/night.css");
	echo addCSS("framework/codemirror/css/the-matrix.css");
	echo addCSS("framework/codemirror/css/zenburn.css");
	echo addCSS("smis-base-css/smis-base-source.css",false);
	
	echo addJS("framework/codemirror/js/codemirror.js");
	echo addJS("framework/codemirror/js/xml-fold.js");
	echo addJS("framework/codemirror/js/matchbrackets.js");
	echo addJS("framework/codemirror/js/closebrackets.js");
	echo addJS("framework/codemirror/js/matchtags.js");
	echo addJS("framework/codemirror/js/closetag.js");
	echo addJS("framework/codemirror/js/trailingspace.js");
	echo addJS("framework/codemirror/js/active-line.js");
	echo addJS("framework/codemirror/js/htmlmixed.js");
	echo addJS("framework/codemirror/js/xml.js");
	echo addJS("framework/codemirror/js/css.js");
	echo addJS("framework/codemirror/js/clike.js");
	echo addJS("framework/codemirror/js/php.js");
	echo addJS("framework/codemirror/js/javascript.js");
	echo addJS("framework/codemirror/js/show-hint.js");
	echo addJS("framework/codemirror/js/xml-hint.js");
	echo addJS("framework/codemirror/js/html-hint.js");
	echo addJS("framework/codemirror/js/anyword-hint.js");
	echo addJS("framework/codemirror/js/css-hint.js");
	echo addJS("framework/codemirror/js/javascript-hint.js");
	echo addJS("framework/codemirror/js/sql-hint.js");
	echo addJS("framework/codemirror/js/foldcode.js");
	echo addJS("framework/codemirror/js/foldgutter.js");
	echo addJS("framework/codemirror/js/brace-fold.js");
	echo addJS("framework/codemirror/js/xml-fold.js");
	echo addJS("framework/codemirror/js/markdown-fold.js");
	echo addJS("framework/codemirror/js/comment-fold.js");
	echo addJS("framework/codemirror/js/comment.js");
	echo addJS("framework/codemirror/js/continuecomment.js");
	echo addJS("framework/codemirror/js/markdown.js");
	echo addJS("framework/codemirror/js/fullscreen.js");
	echo addJS("framework/codemirror/js/loadmode.js");
	echo addJS("framework/codemirror/js/meta.js");
	echo addJS("framework/codemirror/js/shell.js");
	echo addJS("framework/codemirror/js/sql.js");
	echo addJS("framework/codemirror/js/searchcursor.js");
	echo addJS("framework/codemirror/js/match-highlighter.js");
	echo addJS("smis-base-js/smis-base-source-editor.js",false);
	echo addJS ( "framework/smis/js/table_action.js" );
	
	$theme=new OptionBuilder();
	$theme->addSingle("default");
	$theme->addSingle("3024-night");
	$theme->addSingle("blackboard");
	$theme->addSingle("eclipse","1");
	$theme->addSingle("erlang-dark");
	$theme->addSingle("lesser-dark");
	$theme->addSingle("liquibyte");
	$theme->addSingle("night");
	$theme->addSingle("the-matrix");
	$theme->addSingle("zenburn");	
	
	$opsi=new OptionBuilder();
	$opsi->add("On","y","1");
	$opsi->add("Off","n","0");
	
	$line=new OptionBuilder();
	$line->add("On","y","0");
	$line->add("Off","n","1");
	
	$line=new OptionBuilder();
	$line->add("On","y","0");
	$line->add("Off","n","1");
	
	$is=new OptionBuilder();
	$is->add("Yes","true","0");
	$is->add("No","false","0");
	
	
	$uitable=new Table(array());
	$uitable->setName("sceditormirror");
	$uitable->addModal("filename", "text", "File", "readme.txt");
	$uitable->addModal("filetype", "text", "Mode", "application/x-httpd-php","",NULL,true);
	$uitable->addModal("filesize", "text", "Size", "","",NULL,true);
	$uitable->addModal("filelines", "text", "Lines", "","",NULL,true);
	$uitable->addModal("fileread", "select", "Readable", $is->getContent(),"",NULL,true);
	$uitable->addModal("filewrite", "select", "Writable", $is->getContent(),"",NULL,true);
	$uitable->addModal("fileexec", "select", "Executable", $is->getContent(),"",NULL,true);
	$uitable->addModal("theme", "select", "Theme", $theme->getContent());
	$uitable->addModal("linenumber", "select", "Line Number", $opsi->getContent());
	$uitable->addModal("linewrap", "select", "Line Wrapping", $line->getContent());
	$uitable->addModal("actag", "select", "Auto Tag", $opsi->getContent());
	$uitable->addModal("acbrac", "select", "Auto Bracket", $opsi->getContent());
	$uitable->addModal("aline", "select", "Active Line", $opsi->getContent());
	$uitable->addModal("tspace", "select", "Trailing Space", $opsi->getContent());
	$uitable->addModal("foldgutter", "select", "Fold Gutter", $opsi->getContent());
	$form=$uitable->getModal()->getForm();
	
	$button=new Button("", "", "Load");
	$button->setIsButton(BUtton::$ICONIC_TEXT);
	$button->setIcon("fa fa-circle-o-notch");
	$button->setClass("btn-primary");
	$button->setAction("sceditor.scload()");
	
	$save=new Button("", "", "Save");
	$save->setIsButton(BUtton::$ICONIC_TEXT);
	$save->setIcon("fa fa-save");
	$save->setClass("btn-primary");
	$save->setAction("sceditor.scsave()");
	
	$form->addElement("", $button);
	$form->addElement("", $save);
	
	echo $form->getHtml();
	echo "<div class='clear'></div>";
	echo "<div class='well'>";
		echo "<div id='sceditormirror'></div>";
	echo "</div>";
?>
