<?php
global $db;

if(isset($_POST['super_command'])&& $_POST['super_command']=="user"){
	$header=array('Nama','Username',"Authority");
	$dbtable=new DBTable($db, "smis_adm_user");
	$dktable = new Table($header, "", NULL, true);
	$dktable->setName("user");
	$dktable->setModel(Table::$SELECT);
	$dkadapter = new SimpleAdapter();
	$dkadapter->add("Nama", "realname");
	$dkadapter->add("Username", "username");
	$dkadapter->add("Authority", "authority");
	$user= new DBResponder($dbtable, $dktable, $dkadapter, "dokter");
	
	$super = new SuperCommand();
	$super->addResponder("user", $user);
	$init = $super->initialize();
	if($init != null){
		echo $init;
		return;
	}
}

if(isset($_POST['command'])){
	require_once "smis-framework/smis/ui/composite/Navigator.php";
	require_once "smis-framework/smis/ui/composite/Menu.php";
	require_once "smis-framework/smis/api/Plugin.php";
	require_once "smis-framework/smis/log/User.php";
	require_once "smis-base/smis-base.php";
	global $NAVIGATOR;
	if($_POST['command']== 'show'){
		$userid 		= $_POST['userid'];
		$user 			= new User();
		$user			->loadUser($userid);
		$settings 		= $user->getMenu();
		$menus 			= $NAVIGATOR->getMenus();
		require_once 'smis-administrator/class/adapter/MenuAdapter.php';
		$menuadapter 	= new MenuAdapter($settings,$user->getCapability());
		$content 		= $menuadapter->getContent($menus);
		$accordion 		= new Tabulator("", "", Tabulator::$ACCORDION);
		foreach($content as $nama => $isi){
			$component 	= $isi['Component'];
			$result 	= "";
			foreach($component as $one_tick){
			   $result .= $one_tick."</br>";
			}
			$accordion  ->add(ArrayAdapter::format("slug", $isi['Name']), $isi['Name'], $result, Tabulator::$TYPE_HTML);
		}
		echo $accordion	->getHtml();
	} else if($_POST['command']== 'save'){
		$userid 		= $_POST['userid'];
		$menu 			= $_POST['menu'];
		$submenu 		= $_POST['submenu'];
		$value 			= $_POST['value'];
		$save_user 		= new User();
        $save_user		->setAutonomous(getSettings($db,"smis_autonomous_id",""));
		$save_user		->loadUser($userid);
		$settings 		= $save_user->getMenu();
		$saved_settings = addUserAuthorization($settings, $menu, $submenu, $value);
		$json_settings 	= json_encode($saved_settings);
		$save_user		->save("menu", $json_settings);
		$pack 			= new ResponsePackage();
		$pack			->setStatus(ResponsePackage::$STATUS_OK);
		$pack			->setAlertVisible(false);
		$pack			->setAlertContent("Settings Saved", "Menu $menu, Submenu $submenu and value $value Has Been Saved", ResponsePackage::$TIPE_INFO);
		$data			= $pack->getPackage();
		$data['t']		= $settings;
		echo json_encode($data);
	}
	return;
}

$select = new Button('authorityshow', 'show', 'show');
$select->setClass("btn-primary");
$select->setAction("authorization.chooser('authority','username','user',user)");
$select->setIcon("fa fa-check");
$select->setIsButton(Button::$ICONIC);
$uitable=new Table(array());
$uitable->setName("authorization");
$uitable->addModal("id", "hidden", "", "")
		->addModal("nama","text", "Nama", "")
		->addModal("username", "text", "Username", "")
		->addModal("authority", "text", "Authority", "");
$form=$uitable->getModal()->getForm();
$form->addElement("", $select);

echo $form->getHtml();
echo "<div id='clearer' class='clear'></div>";
echo "<div id='table_content' class='well'>";
echo "</div>";
echo addCSS("framework/bootstrap/css/bootstrap-switch.css");
echo addJS("framework/bootstrap/js/bootstrap-switch.js");
echo addJS("framework/smis/js/table_action.js");
echo addJS("administrator/resource/js/authorization.js");
echo addCSS("administrator/resource/css/authorization.css");
?>