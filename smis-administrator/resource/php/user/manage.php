<?php
global $db;
global $user;

require_once "smis-base/smis-include-duplicate.php";
$header	 = array('Name','Username','Email','Authority','Last IP','Last Time','Last Change','Foto',"Active");
$uitable = new Table($header ,"Manage User",NULL,true);
$btn 	 = new Button("","","Reset");
$btn 	 ->setIsButton(Button::$ICONIC)
	 	 ->setIcon("fa fa-refresh")
	 	 ->setClass("btn-info");
$access  = new Button("","","Access");
$access  ->setIsButton(Button::$ICONIC)
		 ->setIcon("fa fa-key")
		 ->setClass("btn-inverse");
$uitable ->addContentButton("reset",$btn)
		 ->addContentButton("access",$access)
		 ->setName("manage");
$uitable ->setReloadButtonEnable(false);
$uitable ->setPrintButtonEnable(false);
$uitable ->setClipboardButtonEnable(false);

/* this is respond when system have to response */
if(isset($_POST['command'])){
	require_once 'smis-administrator/class/adapter/ManageUserAdapter.php';
	require_once 'smis-administrator/class/responder/ManageUserResponder.php';
	$array	 = array('id','realname','username','email','authority','foto',"is_active");
	$adapter = new ManageUserAdapter();
	$dbtable = new DBTable($db,"smis_adm_user",$array);
	if($user->getCapability()=="operator"){
		$dbtable ->addCustomKriteria("authority","!='administrator'");
	}
	$dbres 	 = new ManageUserResponder($dbtable,$uitable,$adapter);
    $dbres	 ->setAutonomous(getSettings($db,"smis_autonomous_id",""));
    $dbres	 ->setDuplicate(false,"");
	$data 	 = $dbres->command($_POST['command']);
	echo json_encode($data);
	return;
}

/* This is Modal Form and used for add and edit the table */
$author  = new OptionBuilder();
$author  ->add("User","user")
		 ->add("Operator","operator");
if($user->getCapability()=="administrator"){
	$author ->add("Administrator","administrator");
}
$uitable ->addModal("id","hidden","","")
		 ->addModal("realname","text","Nama","")
		 ->addModal("username","text","Username","")
		 ->addModal("email","text","Email","")
		 ->addModal("authority","select","Authority",$author->getContent())
		 ->addModal("foto","file-single-image","Foto","");
$modal   = $uitable->getModal();
$modal   ->setTitle("User");

echo $uitable ->getHtml();
echo $modal	  ->getHtml();
echo addJS  ("framework/bootstrap/js/bootstrap-datepicker.js");
echo addJS  ("framework/smis/js/table_action.js");
echo addCSS ("framework/bootstrap/css/datepicker.css");
echo addJS  ("administrator/resource/js/manage.js");
echo addCSS ("administrator/resource/css/manage.css");

?>