<?php 
if(!isset($_POST['command'])){
    echo addJS("administrator/resource/js/ametis.js");
	$reload=new Button("", "", "Reload");
	$reload->setIsButton(Button::$ICONIC_TEXT);
	$reload->setAction("reload_ametis()");
	$reload->setIcon("fa fa-refresh");
	$reload->setClass("btn-primary");
	
	$realtime=new Button("", "", "Realtime");
	$realtime->setIsButton(Button::$ICONIC_TEXT);
	$realtime->setAction("realtime_ametis()");
	$realtime->setIcon("fa fa-recycle");
	$realtime->setClass("btn-danger");
	
	$stop=new Button("", "", "Stop");
	$stop->setIsButton(Button::$ICONIC_TEXT);
	$stop->setAction("stop_real_time_ametis()");
	$stop->setIcon("fa fa-stop");
	$stop->setClass("btn-success");
	
	$btng=new ButtonGroup("");
	$btng->addButton($reload);
	$btng->addButton($realtime);
	$btng->addButton($stop);
	
    $title="<u>A</u>metis is <u>M</u>ysql <u>E</u>ngine for <u>T</u>hread <u>I</u>nspection <u>S</u>ystem";
    
    echo "<h3>".$title."</h3>";
    echo "<div class='noprint' id='btn_ametis'>";
		echo $btng->getHtml();
	echo "</div>";
    echo "</br></br>";
	echo "<div class='hardinfo' id='ametis_inside' >";
	echo "</div>";
	
	
}else{
    global $db;
	$query="SHOW FULL PROCESSLIST;";
            
    $list=$db->get_result($query);
    $adapter=new SimpleAdapter();
    $adapter->add("ID","Id");
    $adapter->add("User","User");
    $adapter->add("Host","Host");
    $adapter->add("Command","Command");
    $adapter->add("DB","db");
    $adapter->add("Time","Time");
    $adapter->add("State","State");
    $adapter->add("Info","Info");
    $ctx=$adapter->getContent($list);
    
    $table=new Table(array("ID","User","Host","DB","Command","Time","State","Info"),"");
    $table->setContent($ctx);
    $table->setAction(false);
    $table->setFooterVisible(false);
    
    $res=new ResponsePackage();
	$res->setStatus(ResponsePackage::$STATUS_OK);
	$res->setContent($table->getHtml());
	echo json_encode($res->getPackage());
}

?>