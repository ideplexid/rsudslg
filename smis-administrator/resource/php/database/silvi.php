<?php 

global $db;
$query="SHOW VARIABLES;";
$title="<u>S</u>ilvi <u>i</u>s <u>L</u>ight <u>V</u>ariable <u>I</u>nspector";        
$list=$db->get_result($query);
$adapter=new SimpleAdapter();
$adapter->setUseNumber(true,"No.","back.");
$adapter->add("Name","Variable_name");
$adapter->add("Value","Value");
$ctx=$adapter->getContent($list);

$table=new Table(array("No.","Name","Value"),$title);
$table->setContent($ctx);
$table->setAction(false);
$table->setFooterVisible(false);
echo $table->getHtml();

?>