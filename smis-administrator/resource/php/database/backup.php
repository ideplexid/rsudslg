<?php 
global $db;
if(isset($_POST['super_command']) && $_POST['super_command']=="list_item"){
	$logtable=array();
	$logtable[]="smis_adm_entity_response";
	$logtable[]="smis_adm_error";
	$logtable[]="smis_adm_log";
	$logtable[]="smis_adm_service_request";
	$logtable[]="smis_adm_service_responds";
	$logtable[]="smis_sb_log";
	$logtable[]="smis_adm_upload";
	$hasil=NULL;
	if($_POST['mode']=="log"){		
		$hasil=$logtable;																	//backup only log
	}else{
		$dbtable = new DBTable ( $db, "smis_table" );
		$dbtable->setFetchMethode ( DBTable::$ARRAY_FETCH );
		$dbtable->setShowAll(true);
		if($_POST['mode']=="notlog"){														//backup all except log
			$data=$dbtable->view("", "0");
			$ldata=$data['data'];
			$hasil=array();
			foreach($ldata as $d){
				if(!in_array($d['id'], $logtable))
					$hasil[]=$d['id'];
			}
		}else{
			if($_POST['mode']=="kriteria"){
				$dbtable->addCustomKriteria(" id ", "LIKE '%".$_POST['kriteria']."%' ");		//backup all that accept by kriteria 
			}else if($_POST['mode']=="sql"){
				$dbtable->setCustomKriteria($_POST['kriteria']);
			}else {
				if(strpos($_POST['mode'], "serverbus")!==false){
					$dbtable->ConjuctionEachCustomKriteria(" OR ");
					$dbtable->addCustomKriteria(NULL, "id LIKE 'smis_sb_%' ");
				}
				if(strpos($_POST['mode'], "essential")!==false){
					$dbtable->ConjuctionEachCustomKriteria(" OR ");
					$dbtable->addCustomKriteria(NULL, "id LIKE 'smis_adm_%' ");
					$dbtable->addCustomKriteria(NULL, "id LIKE 'smis_base_%' ");
				}
				if(strpos($_POST['mode'], "tools")!==false){
					$dbtable->ConjuctionEachCustomKriteria(" OR ");
					$dbtable->addCustomKriteria(NULL, "id LIKE 'smis_tools_%' ");
				}
				if($_POST['mode']=="modul"){
					$dbtable->addCustomKriteria(NULL," id NOT LIKE 'smis_sb_%'  ");
					$dbtable->addCustomKriteria(NULL," id NOT LIKE 'smis_tools_%'  ");
					$dbtable->addCustomKriteria(NULL," id NOT LIKE 'smis_adm_%'  ");
					$dbtable->addCustomKriteria(NULL," id NOT LIKE 'smis_base_%'  ");
				}
			}			
			
			$data=$dbtable->view("", "0");
			$ldata=$data['data'];
			$hasil=array();
			foreach($ldata as $d){
				$hasil[]=$d['id'];
			}
		}
	}
	
	$res=new ResponsePackage();
	$dt=$res->setContent($hasil)
			->setStatus(ResponsePackage::$STATUS_OK)
			->getPackage();
	echo json_encode($dt);
	return;
}



if(isset($_POST['super_command']) && $_POST['super_command']=="do_backup"){
	loadLibrary("smis-libs-function-file");
	$tbl_name=$_POST['tbl_name'];
	$dbtable=new DBTable($db, $tbl_name);
	$dbtable->setDebuggable(true);
	$dbtable->setDelete(true);
	$filename="";
	if(!file_exists("smis-backup/".$_POST['folder'])){
		mkdir("smis-backup/".$_POST['folder']);
	}
	$ldata=NULL;
	if($_POST['max_load']=="-1"){		
		$filename="smis-backup/".$_POST['folder']."/single.sql";
		$data=$dbtable	->setMaximum($_POST['max_request'])
						->setFetchMethode(DBTable::$ARRAY_FETCH)
						->view("", $_POST['page_number']);
		$ldata=$data['data'];
		$query=$dbtable->convertToSQL($ldata);
		appendFile($filename, $query);
	}else if($_POST['max_load']=="-2"){
		$filename="smis-backup/".$_POST['folder']."/".$tbl_name.".sql";
		$data=$dbtable	->setMaximum($_POST['max_request'])
						->setFetchMethode(DBTable::$ARRAY_FETCH)
						->view("", $_POST['page_number']);
		$ldata=$data['data'];
		$query=$dbtable->convertToSQL($ldata);
		appendFile($filename, $query);
	}else{
		if(!file_exists("smis-backup/".$_POST['folder']."/".$tbl_name)){
			mkdir("smis-backup/".$_POST['folder']."/".$tbl_name);
		}
		$digit=$_POST['digit_file'];
		$digitn=$_POST['digit_name'];
		$dari=($_POST['file_step']-1)*$_POST['max_load']+1;
		$sampai=($_POST['file_step'])*$_POST['max_load'];
		$ndari=ArrayAdapter::format("only-digit".$digitn, $dari);
		$nsampai=ArrayAdapter::format("only-digit".$digitn, $sampai);
		$nfile=ArrayAdapter::format("only-digit".$digit, $_POST['file_step']);
		
		$file_num=$nfile."_[ ".$ndari." - ".$nsampai." ]";
		$filename="smis-backup/".$_POST['folder']."/".$tbl_name."/part_".$file_num.".sql";
		$data=$dbtable	->setMaximum($_POST['max_request'])
						->setFetchMethode(DBTable::$ARRAY_FETCH)
						->view("", $_POST['page_number']);
		$ldata=$data['data'];
		$query=$dbtable->convertToSQL($ldata);
		appendFile($filename, $query);
	}
	$data['filename']=$filename;
	$res=new ResponsePackage();
	$dt=$res->setContent($data)
			->setStatus(ResponsePackage::$STATUS_OK)
			->getPackage();
	echo json_encode($dt);
	return;
}

require_once 'smis-administrator/class/adapter/BackupAdapter.php';
if(!file_exists("smis-backup")){
	mkdir("smis-backup");
}

$button=new Button("", "", "");
$button	->setIsButton(Button::$ICONIC)
		->setIcon(" fa fa-database")
		->setClass("btn-primary");

$head=array("No","Nama","Total","Ukuran","Waktu");
$uitable = new Table ($head);
$uitable->addContentButton("do_backup", $button);



$uitable->setName ( "backup" )
		->setAddButtonEnable(false)
		->setReloadButtonEnable(false)
		->setEditButtonEnable(false)
		->setPrintButtonEnable(false)
		->setDelButtonEnable(false);

if (isset ( $_POST ['command'] )) {
	$adapter = new BackupAdapter ();
	$adapter->add("Nama", "table_name")
			->add("Total", "table_rows","number")
			->add("Ukuran", "size","number")
			->add("Waktu", "time","date d M Y, H:i:s")
			->setUseNumber(true,"No");
	$dbtable = new DBTable ( $db, "smis_table" );
	$dbtable->setFetchMethode ( DBTable::$ARRAY_FETCH );
	$dbres = new DBResponder ( $dbtable, $uitable, $adapter );
	$data = $dbres	->setUseAdapterForSelect(true)
		  		 	->command ( $_POST ['command'] );
	echo json_encode ( $data );
	return;
}
$max_reques=new OptionBuilder();
$max_reques	->add("1  	Data / Request","1","0")
			->add("5  	Data / Request","5","0")
			->add("10  	Data / Request","10","1")
			->add("50  	Data / Request","50","0")
			->add("100 	Data / Request","100","0")
			->add("300 	Data / Request","300","0")
			->add("500 	Data / Request","500","0")
			->add("1.000	Data / Request","1000","0")
			->add("2.000	Data / Request","2000","0")
			->add("3.000	Data / Request","3000","0")
			->add("5.000	Data / Request","5000","0")
			->add("10.000	Data / Request","10000","0");
$max_load=new OptionBuilder();
$max_load 	->add("10  	Data / File","10","1")
			->add("50  	Data / File","50","0")
			->add("100 	Data / File","100","0")
			->add("300 	Data / File","300","0")
			->add("500 	Data / File","500","0")
			->add("1.000	Data / File","1000","0")
			->add("2.000	Data / File","2000","0")
			->add("3.000	Data / File","3000","0")
			->add("5.000	Data / File","5000","0")
			->add("10.000	Data / File","10000","0")
			->add("1 Table / File (Recomended)","-2","0")
			->add("Single File (Not Recomended) ","-1","0");
$mode=new OptionBuilder();
$mode 	->add("All Table","alltable","1")
		->add("All Table Without Log","notlog","0")
		->add("Log Only","log","0")
		->add("Modul Only","modul","0")
		->add("Safethree Essential","essential","0")
		->add("Safethree Tools","tools","0")
		->add("Safethree Serverbus","serverbus","0")
		->add("Safethree Essential + Tools","essential-tools","0")
		->add("Safethree Essential + Serverbus","essential-serverbus","0")
		->add("Safethree Serverbus + Tools","serverbus-tools","0")
		->add("Safethree Essential + Serverbus + Tools","essential-serverbus-tools","0")
		->add("Criteria","kriteria","0")
		->add("SQL","sql","0");

$uitable->addModal("max_request", "select", "Data Request", $max_reques->getContent())
		->addModal("max_load", "select", "Data Load", $max_load->getContent())
		->addModal("load_mode", "select", "Filter Table", $mode->getContent())
		->addModal("criteria", "text", "Kriteria", "","",NULL,true);
$backup_action=new Button("log", "Run", "Running Backup");
$backup_action->setIsButton(Button::$ICONIC_TEXT)
			  ->setIcon(" fa fa-dot-circle-o")
			  ->setAction("backup_run()")
			  ->setClass("btn-primary");
$help=new Button("bantuan", "bantuan", "Bantuan");
$help->setIsButton(Button::$ICONIC_TEXT)
	 ->setIcon(" fa fa-question-circle")
	 ->setAction("help('smis-administrator','backup')")
	 ->setClass("btn-primary");


$form=$uitable->getModal()->getForm();
$form->addElement("", $backup_action);
$form->addElement("", $help);

$close=new Button("", "", "Batal");
$close	->addClass("btn-primary")
		->setIsButton(Button::$ICONIC_TEXT)
		->setIcon("fa fa-close")
		->setAction("location.reload()");

$load=new LoadingBar("database_bar", "");
$unit=new LoadingBar("unit_bar", "");
$modal=new Modal("sload_modal", "", "Backup in Progress");
$modal->addHTML($unit->getHtml(),"after")
	  ->addHTML($load->getHtml(),"after")
	  ->addFooter($close);

$logtbh=array("Table","File","Page","Limit");
$uilogable=new Table($logtbh);
$uilogable->setName("logtbl");
$uilogable->setFooterVisible(false);
$uilogable->setAction(false);

echo $form->getHtml();
echo "<div class='clear'></div>";
echo $uitable->getHtml ();
echo "<div class='clear'></div>";
echo $modal->getHtml();
echo addJS ( "base-js/smis-base-loading.js");
echo addJS ( "framework/smis/js/table_action.js" );
echo "<div class='clear'></div>";
echo $uilogable->getHtml()

?>
<script type="text/javascript">
var backup;
var all_table;
var total_backup=0;
var total_done=0;
var IS_BACKUP_RUNNING=false;


function backup_run(){
	if(IS_BACKUP_RUNNING){
		backupRunning();
		return;
	}
	var r_data=backup.getRegulerData();
	r_data['super_command']="list_item";
	r_data['mode']=$("#backup_load_mode").val();
	r_data['kriteria']=$("#backup_criteria").val();
	
	$.post('',r_data,function(res){
		var json=getContent(res);
		all_table=json;
		total_backup=all_table.length;
		total_done=0;
		var d=new Date();
		var year=d.getFullYear();
		var month=d.getMonth()+1;
		var day=d.getDate();
		var hour=d.getHours();
		var minute=d.getMinutes();
		var second=d.getSeconds();
		month=month<10?"0"+month:month;
		day=day<10?"0"+day:day;
		hour=hour<10?"0"+hour:hour;
		minute=minute<10?"0"+minute:minute;
		second=second<10?"0"+second:second;
		$("#logtbl_list").html("");
		var folder=year+"_"+month+"_"+day+"___"+hour+"__"+minute+"__"+second;
		fullBackup(folder);
	});
}


function recursiveBackup(folder,nama_table,page,total_page,max_load,sisa_max_load,file_step,digit_file,digit_name){
	var r_data=backup.getRegulerData();
	r_data['super_command']="do_backup";
	r_data['tbl_name']=nama_table;
	r_data['page_number']=page;
	r_data['max_load']=max_load;
	r_data['folder']=folder;
	r_data['file_step']=file_step;
	r_data['digit_file']=digit_file;
	r_data['digit_name']=digit_name;
	r_data['max_request']=$("#backup_max_request").val();	
	
	$.post('',r_data,function(res){
		var json=getContent(res);
		var html="<tr>";
			html+="<td>"+nama_table+"</td>";
			html+="<td>"+json.filename+"</td>";
			html+="<td>"+json.page_post+" / "+json.max_page+" ( "+json.asal_maxpost+" ) </td>";
			html+="<td>"+json.limit+"</td>";
		html+="</tr>";
		$("#logtbl_list").append(html);
		sisa_max_load=sisa_max_load-Number($("#backup_max_request").val());
		if(sisa_max_load<=0){
			sisa_max_load=max_load;
			file_step++;
		}
		var persen=Math.floor(page*100/total_page);
		$("#database_bar").sload("true",nama_table,persen);
		if(page<total_page) {
			recursiveBackup(folder,nama_table,++page,total_page,max_load,sisa_max_load,file_step,digit_file,digit_name);
		}else{
			$("#database_bar").sload("true","...",0);
			do_full_backup(folder);
		}
	});
}

function do_full_backup(folder){
	if(all_table.length>0){
		$("#sload_modal").modal("show");
		IS_BACKUP_RUNNING=true;
		var ps=total_done*100/total_backup;
		$("#unit_bar").sload("true","Database",ps);
		var d=all_table.splice(0, 1);
		var id=d[0];
		oneBackup(id,folder);
		total_done++;		
	}else{
		IS_BACKUP_RUNNING=false;
		$("#unit_bar").sload("true","",0);
		$("#sload_modal").modal("hide");
		 reload_history();
	}
}

function fullBackup(folder){
	
	if(Number($("#backup_max_request").val()) > Number($("#backup_max_load").val())  && Number($("#backup_max_load").val()>0) ){
		var html="<ul>";
		html+="<li>Nilai Data Request Harus Kurang Dari Sama dengan Nilai Data Load </li>";
		html+="<li>Semakin Besar nilai Data Request Semakin Besar Terjadi Kegagalan karena kehabisan Memory</li>";
		html+="<li>Semakin Kecil nilai Data Request Semakin Ringan Tetapi Semakin Lama Waktu Yang dibutuhkan</li>";
		html+="<li>Semakin Besar Nilai Data Load Semakin Sedikit Jumlah File yang dihasilkan, dan proses semakin cepat tetapi Semakin Besar Pula Kegagalan restore akibat kekurangan Memory</li>";
		html+="<li>Semakin Kecil Data Load Semakin Banyak File yang Dihasilkan dan proses semakin lambat tetapi Semakin Ringan dan Tingkat Keberhasilan Restore Semakin Besar</li>";
		html+="</ul>";
		showWarning("Error",html);
		return;
	}
	
	if(Number($("#backup_max_load").val()==-1) ){
		var html="<ul>";
		html+="<li>Opsi Ini Menggunakan Single File</li>";
		html+="<li>Sistem akan Menjadi semua backup ke dalam satu file saja</li>";
		html+="<li>Kekuranganya saat melakukan Import ke database lain harus menggunakan MySQL Workbench</li>";
		html+="</ul>";

		bootbox.confirm(html+" Anda Yakin ?? ", function(result) {
			if (result ) {                                             
				do_full_backup(folder);                         
			} 
		}); 
	}else{
		do_full_backup(folder);
	}
	
}

function oneBackup(id,folder){
	
	var e_data=backup.getEditData();
	e_data['command']="select";
	e_data['id']=id;

	$.post('',e_data,function(res){
		var json=getContent(res);
		var size=Number(json.tt);
		var nama_table=id;
		var page=0;
		var total_page=Math.floor(size/Number($("#backup_max_request").val()));
		var max_load=Number($("#backup_max_load").val());
		var sisa_max_load=max_load;
		var file_step=1;
		var digit_file=digit(Math.ceil(size/max_load));
		var digit_name=digit(size);


		$("#database_bar").sload("true",nama_table,0);
		recursiveBackup(folder,nama_table,page,total_page,max_load,sisa_max_load,file_step,digit_file,digit_name);
		
	});
	
}

function digit(digit_file){

	if(digit_file<10) digit_file=1;
	else if(digit_file<100) digit_file=2;
	else if(digit_file<1000) digit_file=3;
	else if(digit_file<10000) digit_file=4;
	else if(digit_file<100000) digit_file=5;
	else if(digit_file<1000000) digit_file=6;
	else if(digit_file<10000000) digit_file=7;
	else if(digit_file<100000000) digit_file=8;
	else if(digit_file<1000000000) digit_file=9;
	else if(digit_file<10000000000) digit_file=10;
	return digit_file;
}

function backupRunning(){
	var html="<ul>";
		html+="<li>Backup Sedang Berjalan Anda Tidak Dapat Membuat Backup Baru, Sampai Backup yang Sebelumnya Selesai</li>";
		html+="<li>Tekan F5 atau Refresh untuk Mengakhiri Backup yang Sedang Berjalan</li>";
		html+="<li>Atau Tunggu Sampai Selesai</li>";
	html+="</ul>";
	showWarning("Backup Sedang Berjalan",html);
}


function reload_history(){
	var history_data=backup.getRegulerData();
	history_data['action']="backup_history";
	showLoading();
	$.post("",history_data,function(res){
		$("#backup_history").html(res);
		dismissLoading();
	});
}

$(document).ready(function(){
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
	$("#backup_load_mode").on("change",function(){
		if($("#backup_load_mode").val()=="kriteria" || $("#backup_load_mode").val()=="sql") {
			$("#backup_criteria").prop('disabled', false);
			$("#backup_criteria").val("");
		} else { 
			$("#backup_criteria").prop('disabled', true);
			$("#backup_criteria").val("");
		}
	});
	var column=new Array('id');
	backup=new TableAction("backup","smis-administrator","backup",column);
	backup.view();

	backup.do_backup=function(id){
		$("#logtbl_list").html("");
		if(IS_BACKUP_RUNNING){
			backupRunning();
			return;
		}
		all_table=new Array();
		all_table.push(id);
		total_backup=all_table.length;
		total_done=0;
		var d=new Date();
		var folder=d.getFullYear()+"_"+(d.getMonth()+1)+"_"+d.getDate()+"___"+d.getHours()+"__"+d.getMinutes()+"__"+d.getSeconds();
		fullBackup(folder);
	};
});

</script>
<style type="text/css">
	#sload_modal > div.modal-header a.close {display:none;}
</style>
