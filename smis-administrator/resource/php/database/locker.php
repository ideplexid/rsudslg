<?php 

require_once 'smis-libs-class/MasterTemplate.php';
global $db;

$mahasiswa=new MasterTemplate($db, "smis_adm_settings", "smis-administrator", "locker");
$mahasiswa->getDBtable()->addCustomKriteria("name ", " LIKE 'smis-locker-%' ");
$uitable=$mahasiswa->getUItable();
$header=array("ID","Nama","Value");
$uitable->setHeader($header);
$uitable->addModal("id", "hidden", "", "")
		->addModal("name", "text", "Nama", "","n",NULL,true)
		->addModal("value","text", "Value", "");
$adapter=$mahasiswa->getAdapter();
$adapter->add("ID", "id","digit8")
		->add("Nama", "name")
		->add("Value","value");
$mahasiswa->initialize();
?>