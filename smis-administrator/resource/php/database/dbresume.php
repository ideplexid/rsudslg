<?php 
global $db;

$table=new TablePrint("");
$table->setDefaultBootrapClass(false);
$table->setMaxWidth(false);
$table->setTableClass("table table-bordered table-hover table-striped table-condensed ");
$table->addColumn(" <big>DATABASE RESUME STATUS OF ".strtoupper(SMIS_DATABASE)."</big>", "10", "1",null,null,"center")->commit("header");
$table->addColumn("<strong><big>Real Database Size</big></strong>", "10", "1",null,null,"center")->commit("body");
$table->addColumn("Storage Engine", "1", "1",null,null,"bold");
$table->addColumn("Data Size", "1", "1",null,null,"bold");
$table->addColumn("Index Size", "1", "1",null,null,"bold");
$table->addColumn("Table Size", "1", "1","body",null,"bold");

$query=" SELECT IFNULL(B.engine,'Total') as storage_engine,
CONCAT(LPAD(REPLACE(FORMAT(B.DSize/POWER(1024,pw),3),',',''),17,' '),' ',
SUBSTR(' KMGTP',pw+1,1),'B') as data_size, CONCAT(LPAD(REPLACE(
FORMAT(B.ISize/POWER(1024,pw),3),',',''),17,' '),' ',
SUBSTR(' KMGTP',pw+1,1),'B') as index_size, CONCAT(LPAD(REPLACE(
FORMAT(B.TSize/POWER(1024,pw),3),',',''),17,' '),' ',
SUBSTR(' KMGTP',pw+1,1),'B') as table_size
FROM (SELECT engine,SUM(data_length) DSize,SUM(index_length) ISize,
SUM(data_length+index_length) TSize FROM information_schema.tables
WHERE table_schema NOT IN ('mysql','information_schema','performance_schema')
AND engine IS NOT NULL GROUP BY engine WITH ROLLUP) B,
(SELECT 3 pw) A ORDER BY TSize";
$realsize=$db->get_result($query);
$total="";
foreach($realsize as $one){
	$table->addColumn($one->storage_engine, "1", "1",null,null,"");
	$table->addColumn($one->data_size, "1", "1",null,null,"");
	$table->addColumn($one->index_size, "1", "1",null,null,"");
	$table->addColumn($one->table_size, "1", "1","body",null,"");
	$total=$one->table_size;
}
$canfree=str_replace(" GB", "", $total)*1;
$query="SELECT SUM(size) as total FROM smis_table ";
$used=$db->get_var($query)*1/1073741824;
$free=$canfree-$used;

$table->addColumn("Total Used", "1", "1",null,null,"");
$table->addColumn("", "1", "1",null,null,"");
$table->addColumn("", "1", "1",null,null,"");
$table->addColumn(round($used,3)." GB", "1", "1","body",null,"");

$table->addColumn("Can Free", "1", "1",null,null,"");
$table->addColumn("", "1", "1",null,null,"");
$table->addColumn("", "1", "1",null,null,"");
$table->addColumn(round($free,3)." GB", "1", "1","body",null,"");


$table->addColumn("<strong><big>Recomendation</big></strong>", "10", "1",null,null,"center")->commit("body");
$table->addColumn("Storage Engine", "1", "1",null,null,"bold");
$table->addColumn("Name", "1", "1",null,null,"bold");
$table->addColumn("Value", "2", "1","body",null,"bold");
$query_recomendation="SELECT 'MyIsam' as engine, 'recommended key_buffer_size' as recom, CONCAT(ROUND(KBS/POWER(1024,
IF(PowerOf1024<0,0,IF(PowerOf1024>3,0,PowerOf1024)))+0.4999),
SUBSTR(' KMG',IF(PowerOf1024<0,0,
IF(PowerOf1024>3,0,PowerOf1024))+1,1))
as vsize FROM
(SELECT LEAST(POWER(2,32),KBS1) KBS
FROM (SELECT SUM(index_length) KBS1
FROM information_schema.tables
WHERE engine='MyISAM' AND
table_schema NOT IN ('information_schema','mysql')) AA ) A,
(SELECT 2 PowerOf1024) B
union
SELECT 'Innodb' as engine, 'recommended innodb_buffer_pool_size' as recom, CONCAT(ROUND(KBS/POWER(1024,
IF(PowerOf1024<0,0,IF(PowerOf1024>3,0,PowerOf1024)))+0.49999),
SUBSTR(' KMG',IF(PowerOf1024<0,0,
IF(PowerOf1024>3,0,PowerOf1024))+1,1)) as vsize
FROM (SELECT SUM(data_length+index_length) KBS FROM information_schema.tables
WHERE engine='InnoDB') A,
(SELECT 2 PowerOf1024) B;";
$hasil_key_recomendation=$db->get_result($query_recomendation);
foreach($hasil_key_recomendation as $one){
	$table->addColumn($one->engine, "1", "1",null,null,"");
	$table->addColumn($one->recom, "1", "1",null,null,"");
	$table->addColumn($one->vsize, "2", "1","body",null,"");
}

echo $table->getHtml();
?>