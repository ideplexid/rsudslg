<?php 

if(isset($_POST['super_command']) && $_POST['super_command']=="hapus"){
	loadLibrary("smis-libs-function-file");
	$res=new ResponsePackage();
	$res->setStatus(ResponsePackage::$STATUS_OK)
		->setAlertVisible(true);
	if(deleteFileDir("smis-backup/".$_POST['id'])){
		$res->setAlertContent("Berhasil", "Penghapusan Berhasil");
		$res->setContent(1);
	}else{
		$res->setAlertContent("Gagal", "Penghapusan Gagal ");
		$res->setContent(0);
	} 
	echo json_encode($res->getPackage());
	return;
}

if(isset($_POST['super_command']) && $_POST['super_command']=="download"){
	$path=$_POST['id'];
	header('Content-Type: application/zip');
	header("Content-Disposition: attachment; filename='".$path.".zip'");
	header('Content-Length: ' . filesize($path));
	header("Location: smis-backup/".$path);
	return;
}

$dt=getAllFileInDir("smis-backup");
$header=array("Waktu","Ukuran","Type");
$download=new Button("", "", "");
$download->setIsButton(Button::$ICONIC)
		 ->setIcon("fa fa-download")
		 ->setClass("btn-primary");
$remove=new Button("", "", "");
$remove->setIsButton(Button::$ICONIC)
	   ->setIcon("fa fa-remove")
	   ->setClass("btn-primary");
$uitable=new Table($header,"Archive");
$uitable->setName("backup_archive")
		->addContentButton("download", $download)
		->addContentButton("hapus", $remove)
		->setEditButtonEnable(false)
		->setDelButtonEnable(false)
		->setPrintButtonEnable(false)
		->setAddButtonEnable(false)
		->setReloadButtonEnable(false)
		->setFooterVisible(false);
require_once 'smis-administrator/class/adapter/ArchiveAdapter.php';
$adapter=new ArchiveAdapter();
$content=$adapter->getContent($dt);
$uitable->setContent($content);
echo $uitable->getHtml();
echo addJS("framework/smis/js/table_action.js");

?>

<script type="text/javascript">
	var backup_archive;
	$(document).ready(function(){
		backup_archive=new TableAction("backup_archive","smis-administrator","backup_archive",new Array());
		backup_archive.setSuperCommand("backup_archive");
		backup_archive.download=function(id){
			var data=this.getRegulerData();
			data['super_command']="download";
			data['id']=id;
			download(data);
		};
		backup_archive.hapus=function(id){
			var data=this.getRegulerData();
			data['super_command']="hapus";
			data['id']=id;
			var self=this;
			bootbox.confirm("Are you sure?", function(result) {
				$.post("",data,function(res){
					var json=getContent(res);
					if(json=="1") {
						var font_id=id.replace(".","_");
						$("#"+font_id).parent().parent().remove();
					}
				});
			}); 
		};
	});


</script>