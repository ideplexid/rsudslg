<?php 
global $db;
require_once 'smis-administrator/class/adapter/BackupAdapter.php';
$SUB=strlen(SMIS_DATABASE)+2;
$head=array("No","Nama","Total","Ukuran","IPT","Tipe","Engine","Version","Format","Waktu");
$uitable = new Table ($head);
$uitable->setName ( "innodb" )
		->setAddButtonEnable(false)
		->setReloadButtonEnable(false)
		->setEditButtonEnable(false)
		->setPrintButtonEnable(false)
		->setDelButtonEnable(false);
if (isset ( $_POST ['command'] )) {
	$adapter = new BackupAdapter ();
	$adapter->add("Nama", "table_name")
			->add("Total", "table_rows","number")
			->add("Ukuran", "size","number")
			->add("Waktu", "time","date d M Y, H:i:s")
			->add("IPT", "per_table","number")
			->add("Tipe", "table_type")
			->add("Engine", "engine")
			->add("Version", "version")
			->add("Format", "format")				
			->setUseNumber(true,"No");
	$dbtable = new DBTable ( $db, "smis_table" );
	$qv="SELECT smis_table. * , X.SPACE AS per_table
			FROM smis_table
			LEFT JOIN (
			SELECT DISTINCT SUBSTRING( TABLE_NAME, ".$SUB." ) AS TABLE_NAME, SPACE
			FROM INFORMATION_SCHEMA.INNODB_BUFFER_PAGE_LRU
			WHERE TABLE_NAME IS NOT NULL 
			AND TABLE_NAME NOT LIKE  '%SYS'
			)X ON smis_table.table_name = X.TABLE_NAME
	";
	$qc="SELECT count(*) as total
			FROM smis_table ";
	$dbtable->setPreferredQuery(true, $qv, $qc);
	$dbtable->setUseWhereforView(true);
	$dbtable->setFetchMethode ( DBTable::$ARRAY_FETCH );
	$dbres = new DBResponder ( $dbtable, $uitable, $adapter );
	$data = $dbres	->setUseAdapterForSelect(true)
		  		 	->command ( $_POST ['command'] );
	echo json_encode ( $data );
	return;
}
echo $uitable->getHtml ();
?>
<script type="text/javascript">
var innodb;
$(document).ready(function(){
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
	var column=new Array('id');
	innodb=new TableAction("innodb","smis-administrator","innodb",column);
	innodb.view();

});
</script>

