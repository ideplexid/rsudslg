<?php
global $db;

$dbtable=new DBTable($db, "smis_table");
$adapter=new SimpleAdapter();
$adapter->add("Nama", "table_name")
			->add("Total", "table_rows","number")
			->add("Ukuran", "size","number")
			->add("Waktu", "time","date d M Y, H:i:s")
			->setUseNumber(true,"No");

$head=array("No","Nama","Total","Ukuran","Waktu");
$uitable = new Table ($head);
$uitable->setModel(Table::$SELECT);
$uitable->setName ( "table" );
$super=new SuperCommand();
$super->addResponder("table", new DBResponder($dbtable, $uitable, $adapter));
$data=$super->initialize();
if($data!=null){
	echo $data;
	return;
}


if (isset ( $_POST ['command'] )) {
	if ($_POST ['command'] == 'show_tbl') {
		
		$table = $_POST ['table'];
		setcookie ( 'smis_table', $table );
		$dbtable = new DBTable ( $db, $table );
		$column = $dbtable->get_column ();
		$uitable = new Table ( $column, "", NULL, true );
		$uitable->setName ( "tables" );
		
		$number = 0;
		$mcolumn = array ();
		$mtype = array ();
		$mname = array ();
		$mvalue = array ();
		$colname = "";
		foreach ( $column as $n ) {
			$mcolumn [$number] = $n;
			$mtype [$number] = "text";
			$mname [$number] = $n;
			$mvalue [$number] = "";
			
			if ($number == 0)
				$colname = "'" . $n . "'";
			else
				$colname .= ",'" . $n . "'";
			
			$number ++;
		}
		
		$uitable->setModal ( $mcolumn, $mtype, $mname, $mvalue );
		$modal = $uitable->getModal ();
		$modal->setTitle ( $table );
		
		echo $uitable->getHtml ();
		echo $modal->getHtml ();
		echo addJS ( "framework/bootstrap/js/bootstrap-datepicker.js" );
		echo addJS ( "framework/smis/js/table_action.js" );
		echo addCSS ( "framework/bootstrap/css/datepicker.css" );
		
		?>
		<script type="text/javascript">
			var tables;
			$(document).ready(function(){
				$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
				column=new Array(<?php echo $colname; ?>);
				tables=new TableAction("tables","smis-administrator","tables",column);
				tables.view();
			});
		</script>
		<?php
	} else {
		$table = $_COOKIE ['smis_table'];
		$dbtable = new DBTable ( $db, $table );
		
		$column = $dbtable->get_column ();		
		$uitable = new Table ( $column, $table, NULL, true );
		$uitable->setName ( "tables" );
		$dbtable->setDelete ( true );
		
		class myadapter extends ArrayAdapter {
			public function adapt($onerow) {
				$array = get_object_vars ( $onerow );
				return $array;
			}
		}
		$adapter = new myadapter ();
		$dbres = new DBResponder ( $dbtable, $uitable, $adapter );
		$dbres->setExistIncludeDel(true);
		$data = $dbres->command ( $_POST ['command'] );
		echo json_encode ( $data );
	}
	return;
}


require_once 'smis-administrator/class/adapter/TableAdapter.php';

$query = "SHOW TABLES ";
$res = $db->get_result ( $query );
$adapt = new TableAdapter ();
$value = $adapt->getContent ( $res );
$tbl_name = new Text ( 'table_name', 'table_name', "" );
$tbl_name->setDisabled(true);

$button = new Button ( 'tbl_action', 'show', 'show' );
$button->setClass ( "btn-primary" );
$button->setIsButton(Button::$ICONIC);
$button->setIcon(" fa fa-database");
$button->setAction ( " tables.chooser('tables','tables_tables','table',table,'table') " );

echo "<div class='input-append'>";
echo $tbl_name->getHtml ();
echo $button->getHtml ();
echo "</div>";
echo "<div id='table_content'>";
echo "</div>";
echo addJS ( "framework/smis/js/table_action.js" );

?>
<script type="text/javascript">
var tables;
var table;

$(document).ready(function(){
	tables=new TableAction("tables", "smis-administrator", "tables", new Array());
	table=new TableAction("table", "smis-administrator", "tables", new Array());
	table.setSuperCommand("table");
	table.selected=function(json){
		$("#table_name").val(json.id);
		show_tbl();
	};
	
});

function show_tbl(){
	var data={	
			page:'smis-administrator',
			action:'tables',
			command:"show_tbl",
			table:$("#table_name").val()
			};
	$.post('',data,function(res){
		$("#table_content").html(res);
	});
	
	
	
}
</script>
<style type="text/css">
#smis-chooser-modal{
  left: 20% !important;
  width: 60% !important;
  margin: auto !important;
}
</style>