<?php 
if(!isset($_POST['command'])){
    echo addJS("administrator/resource/js/risma.js");
	$reload=new Button("", "", "Reload");
	$reload->setIsButton(Button::$ICONIC_TEXT);
	$reload->setAction("reload_risma()");
	$reload->setIcon("fa fa-refresh");
	$reload->setClass("btn-primary");
	
	$realtime=new Button("", "", "Realtime");
	$realtime->setIsButton(Button::$ICONIC_TEXT);
	$realtime->setAction("realtime_risma()");
	$realtime->setIcon("fa fa-recycle");
	$realtime->setClass("btn-danger");
	
	$stop=new Button("", "", "Stop");
	$stop->setIsButton(Button::$ICONIC_TEXT);
	$stop->setAction("stop_real_time_risma()");
	$stop->setIcon("fa fa-stop");
	$stop->setClass("btn-success");
	
	$btng=new ButtonGroup("");
	$btng->addButton($reload);
	$btng->addButton($realtime);
	$btng->addButton($stop);
	
    $title="<u>R</u>isma <u>is</u> <u>M</u>ySQL Status <u>A</u>gent";
    
    echo "<h3>".$title."</h3>";
    echo "<div class='noprint' id='btn_risma'>";
		echo $btng->getHtml();
	echo "</div>";
    echo "</br></br>";
	echo "<div class='hardinfo' id='risma_inside' >";
	echo "</div>";
	
	
}else{
    global $db;
	$query="SHOW STATUS;";
            
    $list=$db->get_result($query);
    $adapter=new SimpleAdapter();
    $adapter->setUseNumber(true,"No.","back.");
    $adapter->add("Name","Variable_name");
    $adapter->add("Value","Value");
    $ctx=$adapter->getContent($list);

    $table=new Table(array("No.","Name","Value"),$title);
    $table->setContent($ctx);
    $table->setAction(false);
    $table->setFooterVisible(false);
    
    $res=new ResponsePackage();
	$res->setStatus(ResponsePackage::$STATUS_OK);
	$res->setContent($table->getHtml());
	echo json_encode($res->getPackage());
}

?>