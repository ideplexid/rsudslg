<?php 


if(isset($_POST['super_command']) && $_POST['super_command']=="download"){	
	
	if(endsWith($_POST['path'], ".sql")){
		$filename=str_replace("/", "_", $_POST['path']);
		$filepath= "smis-backup/".$_POST['path'];
		
		header('Content-Type: application/sql');
		header("Content-Disposition: attachment; filename='".$filename."'");
		header('Content-Length: ' . filesize($filepath));
		header("Location: ".$filepath);
		return;
		
	}else{
		require_once "smis-libs-out/pclzip.lib.php";
		
		/**
		 * On ubuntu in some cases, there is a bug that gzopen does not exist and one must use gzopen64 instead
		 * Credit goes to the piwik team for this.
		 */
		if (!function_exists('gzopen')
				&& function_exists('gzopen64')) {
			function gzopen($filename , $mode = 'r', $use_include_path = 0 )
			{
				return gzopen64($filename , $mode, $use_include_path);
			}
		}
		
		$path="smis-backup/".$_POST['path'];
		$zipname=date("Y_m_d___h__i__s");
		$zipath= "smis-backup/".$zipname.".zip";
		$archive = new PclZip($zipath);
		
		if ($archive->create($path) == 0) {
			echo 'Error : '.$archive->errorInfo(true);
		}
		
		header('Content-Type: application/zip');
		header("Content-Disposition: attachment; filename='".$zipname.".zip'");
		header('Content-Length: ' . filesize($zipath));
		header("Location: ".$zipath);
		return;
	}
	
}

if(isset($_POST['super_command']) && $_POST['super_command']=="backup_action"){
	loadLibrary("smis-libs-function-file");
	$res=new ResponsePackage();
	$res->setStatus(ResponsePackage::$STATUS_OK)
		->setAlertVisible(true);
	if( isset($_POST['mode']) && $_POST['mode']=="remove" ){
		if(deleteFileDir("smis-backup/".$_POST['file'])){
			$res->setAlertContent("Berhasil", "Penghapusan Berhasil");
			$res->setContent(1);
		}else{
			$res->setAlertContent("Gagal", "Penghapusan Gagal ");
			$res->setContent(0);
		}
	}else{
		
	}
	echo json_encode($res->getPackage());
	return;
}

$dt=getAllFileInDir("smis-backup",true);

$tree=new Tree("backup_tree", "History Backup", "");

foreach($dt as $key_tanggal=>$isi_pertanggal){
	foreach($isi_pertanggal as $kode_tanggal=>$content_tanggal){
		$date=str_replace("___", " ", $kode_tanggal);
		$date=str_replace("__", ":", $date);
		$date=str_replace("_", "-", $date);
		$date=ArrayAdapter::format("date d M Y H:i:s", $date);
		$download="<a class='download_a' href='javascript:;' data-file='".$kode_tanggal."' ><i class='btn btn-mini btn-inverse fa fa-download'></i></a>";
		$hapus="<a class='hapus_a' href='javascript:;' data-file='".$kode_tanggal."' ><i class='btn btn-mini btn-danger fa fa-trash'></i></a>";
		$tree_tanggal=new Tree("backup_tree_".$kode_tanggal, $date, " $download  $hapus ");
		$tree_tanggal->setIcon("fa fa-calendar");
		foreach($content_tanggal as $key_db=>$content_db){
			if(!is_array($content_db)){
				$download="<a class='download_a' href='javascript:;' data-file='".$kode_tanggal."/".$content_db."'><i class='btn btn-mini btn-success fa fa-download'></i></a>";
				$hapus="<a class='hapus_a' href='javascript:;' data-file='".$kode_tanggal."/".$content_db."'><i class='btn btn-mini btn-danger fa fa-trash'></i></a>";
				$tree_perdb=new Tree("backup_tree_".$content_db."_".$content_db, $content_db, " $download  $hapus ");
				$tree_perdb->setIcon("fa fa-database");
				$tree_tanggal->addChild($tree_perdb);
				continue;
			}
			foreach($content_db as $nama_db=>$isi_db){
				$download="<a class='download_a' href='javascript:;' data-file='".$kode_tanggal."/".$nama_db."'><i class='btn btn-mini btn-success fa fa-download'></i></a>";
				$hapus="<a class='hapus_a' href='javascript:;' data-file='".$kode_tanggal."/".$nama_db."'><i class='btn btn-mini btn-danger fa fa-trash'></i></a>";
					
				$tree_perdb=new Tree("backup_tree_".$nama_db."_".$nama_db, $nama_db, " $download  $hapus ");
				$tree_perdb->setIcon("fa fa-database");
				foreach($isi_db as $nomor_part=>$nama_part){
					$download="<a class='download_a' href='javascript:;' data-file='".$kode_tanggal."/".$nama_db."/".$nama_part."' ><i class='btn btn-mini btn-primary fa fa-download'></i></a>";
					$hapus="<a class='hapus_a' href='javascript:;' data-file='".$kode_tanggal."/".$nama_db."/".$nama_part."' ><i class='btn btn-mini btn-danger fa fa-trash'></i></a>";	
					$tree_part=new Tree("backup_tree_".$nama_part, $nama_part, " $download  $hapus ");
					$tree_part 	->setIcon("fa fa-file");
					$tree_perdb->addChild($tree_part);
				}				
				$tree_tanggal->addChild($tree_perdb);
			}
		}
		$tree->addChild($tree_tanggal);
	}	
}


echo $tree->getHtml();
echo addJS("framework/smis/js/table_action.js");

?>

<script type="text/javascript">
	var backup_history;
	$(document).ready(function(){
		backup_history=new TableAction("backup_history","smis-administrator","backup_history",new Array());
		backup_history.setSuperCommand("backup_action");
		backup_history.remove_file=function(file,p){
			var self=this;
			bootbox.confirm("Are you sure?", function(result) {
				var e_data=self.getRegulerData();
				e_data['mode']="remove";
				e_data['file']=file;
				$.post("",e_data,function(res){
					var json=getContent(res);
					if(json=="1") $(p).remove();
				});
			}); 
		};
		
		$(".download_a").on("click",function(e){
			var d=$(this).data('file');
			var data=backup_history.getRegulerData();
			data['super_command']="download";
			data['path']=d;
			download(data);
			setTimeout(reload_archive, 5000);
		});
		$(".hapus_a").on("click",function(e){
			var p=$(this).parent().parent().parent();
			var d=$(this).data('file');
			backup_history.remove_file(d,p);
		});

		
	});

	function reload_archive(){		
		var history_data=backup_history.getRegulerData();
		history_data['action']="backup_archive";
		showLoading();
		$.post("",history_data,function(res){
			$("#backup_archive").html(res);
			dismissLoading();
		});
	}
	
</script>
