
<?php 

if(!isset($_POST['command'])){
    echo addJS("administrator/resource/js/anifia.js");
	$reload=new Button("", "", "Reload");
	$reload->setIsButton(Button::$ICONIC_TEXT);
	$reload->setAction("reload_anifia()");
	$reload->setIcon("fa fa-refresh");
	$reload->setClass("btn-primary");
	
	$realtime=new Button("", "", "Realtime");
	$realtime->setIsButton(Button::$ICONIC_TEXT);
	$realtime->setAction("realtime_anifia()");
	$realtime->setIcon("fa fa-recycle");
	$realtime->setClass("btn-danger");
	
	$stop=new Button("", "", "Stop");
	$stop->setIsButton(Button::$ICONIC_TEXT);
	$stop->setAction("stop_real_time_anifia()");
	$stop->setIcon("fa fa-stop");
	$stop->setClass("btn-success");
	
	$btng=new ButtonGroup("");
	$btng->addButton($reload);
	$btng->addButton($realtime);
	$btng->addButton($stop);
	
    $title="<u>A</u>nifia is <u>In</u>noDB Ma<u>i</u>n <u>F</u>ile <u>I</u>nspector <u>A</u>gent";
    
    echo "<h3>".$title."</h3>";
    echo "<div class='noprint' id='btn_anifia'>";
		echo $btng->getHtml();
	echo "</div>";
    echo "</br></br>";
	echo "<div class='hardinfo' id='anifia_inside' >";
	echo "</div>";
	
	
}else{
    global $db;
	$query="SELECT DISTINCT 
            TABLE_NAME, 
            SPACE FROM INFORMATION_SCHEMA.INNODB_BUFFER_PAGE_LRU
            WHERE TABLE_NAME IS NOT NULL AND TABLE_NAME NOT LIKE 'SYS%';";
            
    $list=$db->get_result($query);
    $adapter=new SimpleAdapter();
    $adapter->add("Name","TABLE_NAME");
    $adapter->add("Space","SPACE");
    $ctx=$adapter->getContent($list);
    
    $table=new Table(array("Name","Space"),"");
    $table->setContent($ctx);
    $table->setAction(false);
    $table->setFooterVisible(false);
    
    $res=new ResponsePackage();
	$res->setStatus(ResponsePackage::$STATUS_OK);
	$res->setContent($table->getHtml());
	echo json_encode($res->getPackage());
}

?>