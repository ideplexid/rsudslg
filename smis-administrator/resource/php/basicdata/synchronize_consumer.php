<?php
global $db;
require_once "smis-libs-class/MasterTemplate.php";
require_once "smis-administrator/class/responder/SynchronizeProviderResponder.php";
require_once "smis-administrator/class/responder/SynchronizeConsumerResponder.php";

$test_synch=new Button("","","Synch");
$test_synch->setIsButton(Button::$ICONIC);
$test_synch->setIcon("fa fa-cloud-upload");
$test_synch->setClass("btn-success");

$synch_all=new Button("","","Synch All");
$synch_all->setIsButton(Button::$ICONIC);
$synch_all->setIcon("fa fa-cloud-upload");
$synch_all->setClass("btn-success");

$test_table=new Button("","","Configure Table");
$test_table->setIsButton(Button::$ICONIC);
$test_table->setIcon("fa fa-table");
$test_table->setClass("btn-primary");

$repair=new Button("","","Repair table");
$repair->setIsButton(Button::$ICONIC);
$repair->setIcon("fa fa-refresh");
$repair->setClass("btn-success");

$match=new Button("","","Match Table");
$match->setIsButton(Button::$ICONIC);
$match->setIcon("fa fa-exchange");
$match->setClass("btn-success");

$reset=new Button("","","Reset");
$reset->setIsButton(Button::$ICONIC);
$reset->setIcon("fa fa-eraser");
$reset->setClass("btn-inverse");

$model=new OptionBuilder();
$model->add("Multiple","multiple","0");
$model->add("Single","single","0");
$model->add("","","1");

$synch=new Button("","","Synch All");
$synch->setIsButton(Button::$ICONIC);
$synch->setIcon("fa fa-cloud-upload");
$synch->setClass("btn-success");
$synch->setAction("synchronize_consumer.synch_all()");

$master=new MasterTemplate($db,"smis_adm_synch_consumer","smis-administrator","synchronize_consumer");
$master->getUItable()->setHeader(array("Name","Table","Autonomous","Uploaded","Uploading","Downloaded","Downloading","Last Fetch","Model"));
$master->getUItable()->setHelpButtonEnabled(true,"smis-administrator","synchronize_consumer");
$master->getUItable()->setReloadButtonEnable(false);
$master->getUItable()->setPrintButtonEnable(false);
$master->getUItable()->addHeaderButton($synch);

$master->getUItable()->addContentButton("test_table",$test_table)
                     ->addContentButton("repair_table",$repair)
                     ->addContentButton("test_match",$match)
                     ->addContentButton("test_synch",$test_synch)
                     ->addContentButton("test_synch_zero",$synch_all)
                     ->addContentButton("reset",$reset);
$master->getUItable()->addModal("id","hidden","","")
                     ->addModal("code_name","text","Name","")
                     ->addModal("table_name","text","Table Name","")
                     ->addModal("autonomous","text","Autonomous","")
                     ->addModal("round_time","text","Round Times","1")
                     ->addModal("synch_model","select","Synch Model",$model->getContent(),"n")
                     ->addModal("upload_consumer","checkbox","Consume Upload","")
                     ->addModal("upload_max_data","text","Max Upload","")
                     ->addModal("filter_origin_upload","text","Filter Upload","")
                     ->addModal("download_consumer","checkbox","Consume Download","")
                     ->addModal("download_max_data","text","Max Download","")
                     ->addModal("filter_origin_download","text","Filter Download","");
$master->getAdapter()->add("Name","code_name")
                     ->add("Table","table_name")
                     ->add("Autonomous","autonomous")
                     ->add("Uploaded","total_data_upload","back Times")
                     ->add("Downloaded","total_data_download","back Times")
                     ->add("Uploading","queue_data_upload","back Data")
                     ->add("Downloading","queue_data_download","back Data")
                     ->add("Model","synch_model")
                     ->add("Last Fetch","last_fetch","date d M Y H:i:s");
$responder=new SynchronizeConsumerResponder($master->getDBtable(),$master->getUItable(),$master->getAdapter());
$master->setDBresponder($responder);
$master->getDBtable()->setRealDelete(true);
$master->getDBtable()->setDelete(true);
$master->setDateTimeEnable(true);
$master->setModalTitle("Synch Consumer");
$master->setModalComponentSize(Modal::$MEDIUM);
$master->addResouce("js","base-js/smis-base-loading.js","before",true);
$master->addResouce("js","administrator/resource/js/synchronize_consumer.js","after",true);
$master->initialize();

?>