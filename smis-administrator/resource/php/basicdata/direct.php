<?php 
global $db;
require_once "smis-libs-class/MasterTemplate.php";
$header=array("No.","Provider","Service","Autonomous");
$master=new MasterTemplate($db,"smis_adm_directory","smis-administrator","direct");
$master ->getUItable()
        ->addModal("id","hidden","","")
        ->addModal("provider","text","Provider","")
        ->addModal("service","text","Service","")
        ->addModal("password","text","Password","")
        ->addModal("autonomous","text","Autonomous","")
        ->setHeader($header);
$master ->getAdapter()
        ->add("Provider","provider")
        ->add("Service","service","")
        ->add("Autonomous","autonomous","")
        ->setUseNumber(true,"No.","back.");
$master->setModalTitle("Directory");
$master->initialize();

?>