<?php 
require_once 'smis-libs-class/MasterTemplate.php';
global $db;
$notification=new MasterTemplate($db, "smis_base_notification", "smis-administrator", "admin_notification");
$notification->setTruncateEnable(true)->setModalSaveButtonEnable(false);
$notification->getDBtable() ->setOrder(" id DESC ");
$notification->setModalTitle("Notification");
$uitable=$notification->getUItable();
$header=array("Waktu","Halaman","Aksi","Message","User","Read By");
$uitable->setHeader($header)
		->setDelButtonEnable(false)
		->setEditButtonEnable(true)
		->setPrintButtonEnable(false)
		->setAddButtonEnable(false)
		->setReloadButtonEnable(false)
		->addModal("tanggal","text", "Tanggal", "","y",NULL,false)
		->addModal("slug","text", "Slug", "","y",NULL,false)
		->addModal("passkey","textarea", "Pass", "","y",NULL,false)
		->addModal("user","textarea", "User", "","y",NULL,false)
		->addModal("readby","textarea", "Read By", "","y",NULL,false)
		->addModal("message","textarea", "Message", "","y",NULL,false)
		->addModal("id","hidden", "", "");
$adapter=$notification->getAdapter();
$adapter->add("Waktu", "tanggal","date d M Y H:i")
		->add("User", "user")
		->add("Message","message")
		->add("Halaman", "page")
		->add("Read By", "readby")
		->add("Aksi", "action");
$notification->getModal()->setModalSize(Modal::$HALF_MODEL);
$notification->initialize();
?>