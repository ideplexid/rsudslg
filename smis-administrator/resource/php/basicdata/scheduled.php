<?php
global $db;
require_once "smis-libs-class/MasterTemplate.php";
$master=new MasterTemplate($db,"smis_adm_scheduled","smis-administrator","scheduled");
$master->getUItable()->setHeader(array("Name","Delay","Next","Last","Path"));
$master->getUItable()->setHelpButtonEnabled(true,"smis-administrator","scheduled");
$master->getUItable()->setReloadButtonEnable(false);
$master->getUItable()->setPrintButtonEnable(false);
$master->getUItable()->addModal("id","hidden","","")
                     ->addModal("name","text","Name","")
                     ->addModal("delay","text","Delay","")
                     ->addModal("next_run","datetime","Next Run","")
                     ->addModal("last_run","datetime","Last Run","")
                     ->addModal("path","text","Path","");
$master->getAdapter()->add("Name","name")
                     ->add("Delay","delay")
                     ->add("Next","next_run","date d M Y H:i:s")
                     ->add("Last","last_run","date d M Y H:i:s")
                     ->add("Path","path");
$master->setDateTimeEnable(true);
$master->setModalTitle("Scheduled Action");
$master->initialize();

?>