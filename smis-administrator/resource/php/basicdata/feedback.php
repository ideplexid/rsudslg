<?php 
require_once 'smis-libs-class/MasterTemplate.php';
global $db;
global $user;
$feedback=new MasterTemplate($db, "smis_base_feedback", "smis-administrator", "feedback");
$feedback->setTruncateEnable(true);
$feedback->getDBtable() ->setOrder(" id DESC ");
$feedback->getDBResponder()->addColumnFixValue("tanggal_balas",date("Y-m-d H:i:s"));
$uitable=$feedback->getUItable();
$header=array("User","Waktu","Isi","Balas","Balasan");
$uitable->setHeader($header)
		->setDelButtonEnable(false)
		->setEditButtonEnable(true)
		->setPrintButtonEnable(false)
		->setAddButtonEnable(false)
		->setReloadButtonEnable(false)
		->addModal("user","text", "User", "","y",NULL,true)
		->addModal("isi_tanya","textarea", "Tanya", "","y",NULL,true)
		->addModal("isi_balas","textarea", "Balas", "")
		->addModal("id","hidden", "", "");
$adapter=$feedback->getAdapter();
$adapter->add("Waktu", "tanggal_tanya","date d M Y H:i")
		->add("User", "user")
		->add("Isi","isi_tanya")
		->add("Balas", "tanggal_balas","date d M Y H:i")
		->add("Balasan", "isi_balas");
$feedback->initialize();
?>