<?php 
require_once 'smis-libs-class/MasterTemplate.php';
require_once 'smis-administrator/class/template/UploadTemplate.php';

global $db;
$kelamin=new OptionBuilder();
$upload=new UploadTemplate($db, "smis_adm_upload", "smis-administrator", "upload");
$header=array("Name","Code","User","Time","IP","Size","Filetype");
$button=new Button("", "", "");
$button->setIsButton(Button::$ICONIC);
$button->setClass("btn-primary");
$button->setIcon("fa fa-trash");

$down=new Button("", "", "");
$down->setIsButton(Button::$ICONIC);
$down->setClass("btn-primary");
$down->setIcon("fa fa-download");

$upload ->getUItable()
		->setAddButtonEnable(false)
		->setPrintButtonEnable(false)
		->setEditButtonEnable(false)
		->setEditButtonEnable(false)
		->setDelButtonEnable(false)
		->setHeader($header)
		->addContentButton("download_file", $down)
		->addContentButton("remove_file", $button);
$upload->getAdapter()->add("Name", "namefile")
					 ->add("Code", "linkfile","substr0-32")
					 ->add("User","user")
					 ->add("Time", "waktu","date d M Y H:i:s")
					 ->add("IP", "ip")
					 ->add("Size", "size","number")
					 ->add("Filetype", "filetype");
$upload->initialize();
?>