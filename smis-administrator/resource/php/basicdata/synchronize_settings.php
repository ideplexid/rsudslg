<?php
global $db;
require_once "smis-libs-class/MasterTemplate.php";
require_once "smis-libs-class/MasterSlaveTemplate.php";

$master=new MasterSlaveTemplate($db,"smis_adm_settings","smis-administrator","synchronize_settings");
$master->getUItable()->setHeader(array("Name","Enable","Up","Down","Autonomous"));
$master->getUItable()->setHelpButtonEnabled(true,"smis-administrator","synchronize_settings");
$master->getUItable()->setReloadButtonEnable(false);
$master->getUItable()->setPrintButtonEnable(false);
$master->getUItable()->setDelButtonEnable(false);
$master->getUItable()->setAddButtonEnable(false);

$duplicate=new OptionBuilder();
$duplicate->add("Enabled",1);
$duplicate->add("Disabled",0);
$duplicate->add("","","1");


$master->getUItable()->addModal("duplicate_filter","select","Enable",$duplicate->getContent())
                     ->addModal("synch_up_filter","text","Synch Up","")
                     ->addModal("synch_down_filter","text","Synch Down","");
$master->getForm();
$master->setAutoReload(true);
$master->getUItable()->addModal("id","hidden","","")
                     ->addModal("name","text","Name","","n",null,true)
                     ->addModal("duplicate","checkbox","Enabled","","y",null,false)
                     ->addModal("autonomous_up","textarea","Target Up","","n",null,false)
                     ->addModal("autonomous_down","textarea","Target Down","","n",null,false)
                     ->addModal("value","textarea","Value","","y",null,true);
$master->getAdapter()->add("Name","name")
                     ->add("Up","autonomous_up")
                     ->add("Down","autonomous_down")
                     ->add("Enable","duplicate","trivial_1_<i class='fa fa-check'></i>_")
                     ->add("Last Fetch","last_fetch","date d M Y H:i:s")
                     ->add("Updater","origin_updated")
                     ->add("Last Updated","time_updated","date d M Y H:i:s");

if($master->getDBResponder()->isView()){
    if(isset($_POST["synch_up_filter"]) && $_POST["synch_up_filter"]!=""){
        $master->getDBTable()->addCustomKriteria(" 	autonomous_up "," LIKE '%".$_POST["synch_up_filter"]."%'" );
    }
    
    if(isset($_POST["synch_down_filter"]) && $_POST["synch_down_filter"]!=""){
        $master->getDBTable()->addCustomKriteria(" autonomous_down "," LIKE '%".$_POST["synch_down_filter"]."%'" );
    }
    
    if(isset($_POST["duplicate_filter"]) && $_POST["duplicate_filter"]!=""){
        $master->getDBTable()->addCustomKriteria(" duplicate "," = '".$_POST["duplicate_filter"]."'" );
    }
}

$master->addViewData("duplicate_filter","duplicate_filter");
$master->addViewData("synch_up_filter","synch_up_filter");
$master->addViewData("synch_down_filter","synch_down_filter");

$master->setDateTimeEnable(true);
$master->setModalTitle("Settings Synch");
$master->setModalComponentSize(Modal::$MEDIUM);
$master->initialize();

?>