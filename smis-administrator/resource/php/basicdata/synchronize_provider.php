<?php
global $db;
require_once "smis-libs-class/MasterTemplate.php";
require_once "smis-administrator/class/responder/SynchronizeProviderResponder.php";

$model=new OptionBuilder();
$model->add("Multiple","multiple","0");
$model->add("Single","single","0");
$model->add("","","1");

$test_table=new Button("","","Configure Table");
$test_table->setIsButton(Button::$ICONIC);
$test_table->setIcon("fa fa-table");
$test_table->setClass("btn-primary");

$repair=new Button("","","Repair table");
$repair->setIsButton(Button::$ICONIC);
$repair->setIcon("fa fa-refresh");
$repair->setClass("btn-success");


$reset=new Button("","","Reset");
$reset->setIsButton(Button::$ICONIC);
$reset->setIcon("fa fa-eraser");
$reset->setClass("btn-inverse");

$master=new MasterTemplate($db,"smis_adm_synch_provider","smis-administrator","synchronize_provider");
$master->getUItable()->setHeader(array("Name","Table","Autonomous","Uploaded","Uploading","Downloaded","Downloading","Last Fetch","Model"));
$master->getUItable()->setHelpButtonEnabled(true,"smis-administrator","synchronize_provider");
$master->getUItable()->setReloadButtonEnable(false);
$master->getUItable()->setPrintButtonEnable(false);
$master->getUItable()->addContentButton("test_table",$test_table)
                     ->addContentButton("repair_table",$repair)
                     ->addContentButton("reset",$reset);
$master->getUItable()->addModal("id","hidden","","")
                     ->addModal("code_name","text","Name","")
                     ->addModal("table_name","text","Table Name","")
                     ->addModal("autonomous","text","Autonomous","")
                     ->addModal("synch_model","select","Synch Model",$model->getContent(),"n")
                     ->addModal("upload_provider","checkbox","Provide Upload","")
                     ->addModal("upload_max_data","text","Max Upload","")
                     ->addModal("filter_origin_upload","text","Filter Upload","")
                     ->addModal("download_provider","checkbox","Provide Download","")
                     ->addModal("download_max_data","text","Max Download","")
                     ->addModal("filter_origin_download","text","Filter Download","");
$master->getAdapter()->add("Name","code_name")
                     ->add("Table","table_name")
                     ->add("Autonomous","autonomous")
                     ->add("Uploaded","total_data_upload","back Times")
                     ->add("Downloaded","total_data_download","back Times")
                     ->add("Uploading","queue_data_upload","back Data")
                     ->add("Downloading","queue_data_download","back Data")
                     ->add("Model","synch_model")
                     ->add("Last Fetch","last_fetch","date d M Y H:i:s");
$responder=new SynchronizeProviderResponder($master->getDBtable(),$master->getUItable(),$master->getAdapter());
$master->setDBresponder($responder);

$master->setDateTimeEnable(true);
$master->setModalTitle("Synch Consumer");
$master->setModalComponentSize(Modal::$MEDIUM);
$master->addResouce("js","administrator/resource/js/synchronize_provider.js","after",true);
$master->initialize();

?>