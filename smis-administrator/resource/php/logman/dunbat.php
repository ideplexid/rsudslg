<?php 
show_error();
loadLibrary("smis-libs-function-file");
require_once 'smis-framework/smis/filebase/FileAdapter.php';
require_once 'smis-administrator/class/adapter/LogManajerAdapter.php';
require_once 'smis-administrator/class/adapter/DunbatAdapter.php';
require_once 'smis-framework/smis/filebase/FileResponder.php';
require_once 'smis-administrator/class/responder/DunbatResponder.php';


$header=array("&nbsp;","Name","Service","Hash","User","Process","Action");
$uitable=new Table($header);
$uitable->setAction(false);
$uitable->setFooterVisible(false);
$uitable->setName("dunbat");

if(isset($_POST['command'])){
	$adapter=new DunbatAdapter("dunbat",$_POST['database']);
	$responder=new DunbatResponder("smis-log/tablelog/","dunbat",$uitable,$adapter);
	$responder->setIsFileOnly(true);
	$result=$responder->command($_POST['command']);
	echo json_encode($result);
	return;
}

$text=new Hidden("dunbat_folder", "Directory", $BASE);
$database=new Text("dunbat_database", "Database", "");
$dbid=new Text("dunbat_dbid", "DBID", "");

$up=new Button("", "", "Up");
$up->setIsButton(Button::$ICONIC_TEXT);
$up->setIcon("fa fa-level-up");
$up->setClass("btn-primary");
$up->setAction("dunbat.levelup()");

$show=new Button("", "", "Open");
$show->setIsButton(Button::$ICONIC_TEXT);
$show->setIcon("fa fa-folder-open");
$show->setClass("btn-primary");
$show->setAction("dunbat.view()");

$btg=new ButtonGroup("");
$btg->addButton($show);
$btg->addButton($up);


$form=new Form("dunbat_form", "", "<u>D</u>unbat is <u>U</u>niversal <u>N</u>ative <u>B</u>ig Data <u>A</u>nalysis <u>T</u>ool");
$form->addElement("", $text);
$form->addElement("Database", $database);
$form->addElement("DBID", $dbid);
$form->addElement("", $btg);

$type=new OptionBuilder();
$type->add("File");
$type->add("Directory");

 $output = array();
 $command = "cat /etc/passwd | cut -d\":\" -f1";
 $output =shell_exec($command);
 $userid=new OptionBuilder();
 $theuser=explode("\n", $output);
foreach ($theuser as $u){
	if($u!="")	$userid->addSingle($u,"www-data"==$u);
}
 
$uitable->addModal("oldname", "hidden", "", "");
$uitable->addModal("filename", "text", "Name", "","n");
$uitable->addModal("filetype", "select", "Type", $type->getContent(),"n");
$uitable->addModal("filemode", "text", "Permission", "644","n");
$uitable->addModal("fileowner", "select", "File Owner", $userid->getContent(),"n");
$uitable->addModal("isold", "hidden", "", "0");

echo $form->getHtml();
echo $uitable->getModal()->setTitle("File / Folder")->getHtml();
echo "<div class='clear'></div>";
echo $uitable->getHtml();
echo addJS ( "framework/smis/js/table_action.js" );
echo addJS("base-js/smis-base-fetreefy-json.js");
echo addJS("framework/smis/js/table_action.js");
echo addJS("framework/smis/js/file_action.js");
echo addJS("administrator/resource/js/dunbat.js");
?>
