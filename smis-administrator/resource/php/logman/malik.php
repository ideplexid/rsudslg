<?php 
loadLibrary("smis-libs-function-file");
require_once 'smis-framework/smis/filebase/FileAdapter.php';
require_once 'smis-framework/smis/filebase/FileResponder.php';
require_once 'smis-administrator/class/responder/MalikResponder.php';
require_once 'smis-administrator/class/adapter/LogManajerAdapter.php';
require_once 'smis-administrator/class/adapter/MalikAdapter.php';


$header=array("&nbsp;","Name","Data Type","Autonomous","Entity","PHP","Query","Service","Message","Action");
$uitable=new Table($header);
$uitable->setAction(false);
$uitable->setFooterVisible(false);
$uitable->setName("malik");

if(isset($_POST['command'])){
	$adapter=new MalikAdapter("malik");
	$responder=new MalikResponder("smis-log/servicelog/","malik",$uitable,$adapter);
	$result=$responder->command($_POST['command']);
	echo json_encode($result);
	return;
}

$text=new Hidden("malik_folder", "Directory", $BASE);
$service=new Text("malik_service", "Service", "");
$hash=new Text("malik_hash", "Hash", "");

$up=new Button("", "", "Up");
$up->setIsButton(Button::$ICONIC_TEXT);
$up->setIcon("fa fa-level-up");
$up->setClass("btn-primary");
$up->setAction("malik.levelup()");

$show=new Button("", "", "Open");
$show->setIsButton(Button::$ICONIC_TEXT);
$show->setIcon("fa fa-folder-open");
$show->setClass("btn-primary");
$show->setAction("malik.view()");

$btg=new ButtonGroup("");
$btg->addButton($show);
$btg->addButton($up);


$form=new Form("malik_form", "", "<u>M</u>alik is <u>A</u>nalysis for <u>L</u>og with <u>I</u>nspection of <u>K</u>anban");
$form->addElement("", $text);
$form->addElement("Service", $service);
$form->addElement("Hash", $hash);
$form->addElement("", $btg);

$type=new OptionBuilder();
$type->add("File");
$type->add("Directory");

 $output = array();
 $command = "cat /etc/passwd | cut -d\":\" -f1";
 $output =shell_exec($command);
 $userid=new OptionBuilder();
 $theuser=explode("\n", $output);
foreach ($theuser as $u){
	if($u!="")	$userid->addSingle($u,"www-data"==$u);
}
 
$uitable->addModal("oldname", "hidden", "", "");
$uitable->addModal("filename", "text", "Name", "","n");
$uitable->addModal("filetype", "select", "Type", $type->getContent(),"n");
$uitable->addModal("filemode", "text", "Permission", "644","n");
$uitable->addModal("fileowner", "select", "File Owner", $userid->getContent(),"n");
$uitable->addModal("isold", "hidden", "", "0");

echo $form->getHtml();
echo $uitable->getModal()->setTitle("File / Folder")->getHtml();
echo "<div class='clear'></div>";
echo $uitable->getHtml();
echo addJS("framework/smis/js/table_action.js");
echo addJS("framework/smis/js/file_action.js");
echo addJS("administrator/resource/js/malik.js");
?>
