<?php 
    global $wpdb;
    $query="CREATE OR REPLACE VIEW smis_table AS 
                select 	information_schema.TABLES.TABLE_NAME AS id,
                        information_schema.TABLES.TABLE_NAME AS table_name,
                        information_schema.TABLES.TABLE_ROWS AS table_rows,
                        information_schema.TABLES.TABLE_TYPE AS table_type,
                        information_schema.TABLES.ENGINE AS engine,
                        information_schema.TABLES.VERSION AS version,
                        information_schema.TABLES.ROW_FORMAT AS format,
                        information_schema.TABLES.DATA_LENGTH AS size,
                        information_schema.TABLES.CREATE_TIME AS time,
                        information_schema.TABLES.TABLE_SCHEMA AS skema,
                        '' AS prop 
                from 
                    INFORMATION_SCHEMA.TABLES 
                        where ((information_schema.TABLES.TABLE_SCHEMA = '".SMIS_DATABASE."') 
                                and (information_schema.TABLES.TABLE_ROWS is not null));";
    $wpdb->query ( $query );
?>
