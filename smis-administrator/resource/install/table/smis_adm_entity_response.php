<?php 
    global $wpdb;
    require_once "smis-libs-class/DBCreator.php";
    $dbcreator=new DBCreator($wpdb,"smis_adm_entity_response");
    $dbcreator->addColumn("waktu","timestamp",DBCreator::$SIGN_NONE,false,DBCreator::$DEFAULT_CURRENT_TIMESTAMP,false,DBCreator::$ON_UPDATE_NONE,"");
    $dbcreator->addColumn("post","text",DBCreator::$SIGN_NONE,false,DBCreator::$DEFAULT_NONE,false,DBCreator::$ON_UPDATE_NONE,"");
    $dbcreator->addColumn("get","text",DBCreator::$SIGN_NONE,false,DBCreator::$DEFAULT_NONE,false,DBCreator::$ON_UPDATE_NONE,"");
    $dbcreator->addColumn("code","text",DBCreator::$SIGN_NONE,false,DBCreator::$DEFAULT_NONE,false,DBCreator::$ON_UPDATE_NONE,"");
    $dbcreator->addColumn("path","text",DBCreator::$SIGN_NONE,false,DBCreator::$DEFAULT_NONE,false,DBCreator::$ON_UPDATE_NONE,"");
    $dbcreator->addColumn("response","text",DBCreator::$SIGN_NONE,false,DBCreator::$DEFAULT_NONE,false,DBCreator::$ON_UPDATE_NONE,"");
    $dbcreator->setDuplicate(false);
    $dbcreator->initialize();
?>