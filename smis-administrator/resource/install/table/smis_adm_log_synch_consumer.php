<?php 
    global $wpdb;
    require_once "smis-libs-class/DBCreator.php";
    $dbcreator=new DBCreator($wpdb,"smis_adm_log_synch_consumer");
    $dbcreator->addColumn("code_name","varchar(32)",DBCreator::$SIGN_NONE,false,DBCreator::$DEFAULT_NONE,false,DBCreator::$ON_UPDATE_NONE,"");
    $dbcreator->addColumn("log","text",DBCreator::$SIGN_NONE,false,DBCreator::$DEFAULT_NONE,false,DBCreator::$ON_UPDATE_NONE,"");
    $dbcreator->addColumn("waktu","timestamp",DBCreator::$SIGN_NONE,false,DBCreator::$DEFAULT_CURRENT_TIMESTAMP,false,DBCreator::$ON_UPDATE_CURRENT_TIMESTAMP,"");
    $dbcreator->setDuplicate(false);
    $dbcreator->initialize();
?>