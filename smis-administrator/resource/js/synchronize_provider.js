$(document).ready(function(){
    
    synchronize_provider.reset=function(id){
        var a=this.getRegulerData();
        a['command']="reset";
        a['id']=id;
        showLoading();
        $.post("",a,function(res){
            var json=getContent(res);
            dismissLoading();
        });
    };
    
    synchronize_provider.test_table=function(id){
        var a=this.getRegulerData();
        a['command']="test_table";
        a['id']=id;
        showLoading();
        $.post("",a,function(res){
            var json=getContent(res);
            dismissLoading();
        });
    };
    
    synchronize_provider.repair_table=function(id){
        var a=this.getRegulerData();
        a['command']="repair_table";
        a['id']=id;
        
        var msg="Repairing Table would perform this several action : ";
        msg+="<ul>";
            msg+="<li>Update the Table Column so the structure fit the synch needed (id,prop,autonomous,duplicate,origin,origin_id,time_updated,origin_updated) </li>";
            msg+="<li>all row of table that doesn't have origin_id or origin_id=0 would be set as current table id a.k.a origin_id=id if the origin_id=0 (be carefull on multiple mode synch, can make data collision) </li>";
            msg+="<li>all row of table that doesn't have origin or origin='' would be set as current autonomous a.k.a origin='current_autonomous' if the origin='' (be carefull on multiple mode synch, can make data collision) </li>";
        msg+="</ul>";
        msg+="Do You Really want to proceed !!";
        
        bootbox.confirm({
            title: "Procedure Confirmation",
            message: msg,
            buttons: {
                confirm: {
                    label: '<i class="fa fa-play"></i> Yes',
                    className: 'btn-danger'
                },
                cancel: {
                    label: '<i class="fa fa-ban"></i> No',
                    className: 'btn-primary'
                }
            },
            callback: function (result) {
                if(result){
                    showLoading();
                    $.post("",a,function(res){
                        var json=getContent(res);
                        dismissLoading();
                    });
                }
            }
        });
    };
});