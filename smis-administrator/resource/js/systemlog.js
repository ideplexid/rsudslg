var systemlog;
$(document).ready(function(){
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});	
	$('#log_system').on('change', function (e) {
		systemlog.log_system(this.value);
	});	
	var column=new Array('name','alias','no_dtd','icd','id');
	systemlog=new TableAction("systemlog","smis-administrator","systemlog",column);
	systemlog.clean=function(){
		var data=this.getRegulerData();
		data['command']="clean";
		$.post("",data,function(res){
			systemlog.view();
		});
	};

	systemlog.log_system=function(log){
		var data=this.getRegulerData();
		data['command']="log_system";
		data['settings']=log;
		$.post("",data,function(res){
			getContent(res);
			systemlog.view();
		});
	};

	systemlog.slack=function(id){
		var data=this.getRegulerData();
		data['command']="slack";
		data['id']=id;
		showLoading();
		$.post("",data,function(res){
			getContent(res);
			dismissLoading();
		});
	};

	systemlog.show=function(id){
		var c=$("#modal_systemlog_"+id).html();
		showWarning("System Log : "+id,c);
	};
	systemlog.view();
});