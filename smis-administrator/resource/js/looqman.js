var looqman;
$(document).ready(function(){
	looqman=new FileAction("looqman","smis-administrator","looqman",new Array("oldname","fileowner","filename","filetype","filemode","isold"));
	looqman.view();
	
	looqman.openfile=function(file){
		var base=this.getRegulerData();
		base['filename']=file;
		base['command']="load";
		showLoading();
		$.post("",base,function(res){
			var json=getContent(res);
			if(json!=null && json!=""){
				fname=file.substring(0,19);
				showFullWarning("File "+fname,json,"half_model");
			}
			dismissLoading();
		});		
	};
	
	$("#looqman_folder").keypress(function(e){
		if(e.which==13){
			e.preventDefault();
			looqman.view();
		}
	});
	
});