var PAGE="smis-administrator";
var ACTION="plugins";

$(document).ready(function(){
	list();		
});

function init_button(){
	$(".detail").hide();
	$( ".pluginaction" ).click(function() {
		  var name=$(this).attr("name");
		  var action=$(this).attr("action");
		  setAction(name,action);
		  
	});
	$('.smis_switch').bootstrapSwitch();
	$('.smis_switch').on('switch-change', function (e, data) {
		var plugins=$(data.el).attr("plugins");
		var hide=data.value;
		if(!hide){
			$("."+plugins).hide("slow");
			$("."+plugins+"-parent").attr("rowspan","1");
		}else{
			$("."+plugins).show("slow");
			$("."+plugins+"-parent").attr("rowspan","5");
		}
	});
}

function compatible(plugins){
	$.post('',{
		page	: PAGE,
		action	: ACTION,
		command	: "compatible",
		plugins	: plugins
	},function(res){
		getContent(res);
	});
}

function compatible_all(){
	$.post('',{
		page	: PAGE,
		action	: ACTION,
		command	: "compatible_all"
	},function(res){
		getContent(res);
	});
}


function list(){
	$.post('',{
		page:PAGE,
		action:ACTION,
		command:"list"
	},function(res){
		var json=$.parseJSON(res);
		$("#list").html(json.list);
		init_button();
		
	});
}

function setAction(name,command){
	showLoading();
	$.post('',{
		page:PAGE,
		action:ACTION,
		command:command,
		name:name
	},function(res){
		var content=getContent(res);
		if(content==1){
			list();
		}
		dismissLoading();
	});
}

function clean(){
	showLoading();
	$.post('',{
		page:PAGE,
		action:ACTION,
		command:"clean"
	},function(res){
		var content=getContent(res);
		if(content==1){
			list();
		}
		dismissLoading();
	});
}

function clean_database(){
	showLoading();
	$.post('',{
		page:PAGE,
		action:ACTION,
		command:"clean_database"
	},function(res){
		var content=getContent(res);
		if(content==1){
			list();
		}
		dismissLoading();
	});
}

function rebuild_menu(){
    showLoading();
	$.post('',{
		page	: PAGE,
		action	: 'rebuild_menu',
		command	: "rebuild_menu"
	},function(res){
        getContent(res);
        dismissLoading();
	});
}
