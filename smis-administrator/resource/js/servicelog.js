var servicelog;
$(document).ready(function(){
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
	var column=new Array('name','alias','no_dtd','icd','id');
	servicelog=new TableAction("servicelog","smis-administrator","servicelog",column);
	servicelog.show=function(id){
		var data=this.getEditData(id);
		data['id']=id;
		data['command']="select";
		showLoading();
		$.post("",data,function(res){
			var json=getContent(res);
			showWarning("Service Log : "+id,json.tabs);
			$("pre").fetreefy();
			dismissLoading();
		});
	};
	servicelog.clean=function(){
		var data=this.getRegulerData();
		data['command']="clean";
		$.post("",data,function(res){
			servicelog.view();
		});
	};
	servicelog.view();
});