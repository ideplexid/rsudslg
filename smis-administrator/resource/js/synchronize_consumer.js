$(document).ready(function(){
    synchronize_consumer.test_synch=function(id){
        var a=this.getRegulerData();
        a['command']="test_synch";
        a['id']=id;
        showLoading();
        $.post("",a,function(res){
            var json=getContent(res);
            synchronize_consumer.view();
            dismissLoading();
        });
    };
    
    
    synchronize_consumer.synch_all=function(){
        var a=this.getRegulerData();
        a['command']="synch_all";
        showLoading();
        $.post("",a,function(res){
            var json=getContent(res);
            dismissLoading();
            if(json.length>0){
                smis_loader.showLoader();
                synchronize_consumer.test_synch_all(0,json);
            }
        });
    };
    
    synchronize_consumer.test_synch_all=function(current,alldata){
        var total=alldata.length;
        if(current>=total || !smis_loader.isShown()){
            smis_loader.hideLoader();
            synchronize_consumer.view();
        }else{
            var x=alldata[current];
            var nama=x['code_name']+" - "+x['table_name'];
            var id=x['id'];
            synchronize_consumer.test_synch_one(id,current,total,nama,alldata);    
        }
    };
    
    
    synchronize_consumer.test_synch_one=function(id,current,total,nama,alldata){
        var a=this.getRegulerData();
        a['command']="test_synch_zero";
        a['id']=id;
        $.post("",a,function(res){
            var json=getContent(res);
            var total_unsynch=Number(json.queue_data_download)+Number(json.queue_data_upload);
            var total_sync=Number(json.total_data_download)+Number(json.total_data_upload);                    
            if(total_unsynch>0 && smis_loader.isShown()){
                smis_loader.updateLoader('true',total_unsynch+" Data Remaining in "+nama+" [ "+current+" / "+total+" ] ",(total_sync-total_unsynch)*100/total_sync);
                setTimeout(function(){
                    synchronize_consumer.test_synch_one(id,current,total,nama,alldata);    
                },300);
            }else{
                setTimeout(function(){
                    current++;
                    synchronize_consumer.test_synch_all(current,alldata);    
                },300);
            }
        });
    };
    
    
    synchronize_consumer.test_synch_zero=function(id){
        var a=this.getRegulerData();
        a['command']="test_synch_zero";
        a['id']=id;
        smis_loader.showLoader();
        $.post("",a,function(res){
            var json=getContent(res);
            var total_unsynch=Number(json.queue_data_download)+Number(json.queue_data_upload);
            var total_sync=Number(json.total_data_download)+Number(json.total_data_upload);                    
            if(total_unsynch>0 && smis_loader.isShown()){
                console.log(total_unsynch);
                smis_loader.updateLoader('true',"Data Remaining "+total_unsynch,(total_sync-total_unsynch)*100/total_sync);
                setTimeout(function(){
                    synchronize_consumer.test_synch_zero(id);    
                },300);
            }else{
                smis_loader.hideLoader();
                synchronize_consumer.view();
            }
        });
    };
    
    synchronize_consumer.reset=function(id){
        var a=this.getRegulerData();
        a['command']="reset";
        a['id']=id;
        showLoading();
        $.post("",a,function(res){
            var json=getContent(res);
            dismissLoading();
        });
    };
    
    synchronize_consumer.test_match=function(id){
        var a=this.getRegulerData();
        a['command']="test_match";
        a['id']=id;
        showLoading();
        $.post("",a,function(res){
            var json=getContent(res);
            dismissLoading();
        });
    };
    
    synchronize_consumer.test_table=function(id){
        var a=this.getRegulerData();
        a['command']="test_table";
        a['id']=id;
        showLoading();
        $.post("",a,function(res){
            var json=getContent(res);
            dismissLoading();
        });
    };
    
    synchronize_consumer.repair_table=function(id){
        var a=this.getRegulerData();
        a['command']="repair_table";
        a['id']=id;
        
        var msg="Repairing Table would perform this several action : ";
        msg+="<ul>";
            msg+="<li>Update the Table Column so the structure fit the synch needed (id,prop,autonomous,duplicate,origin,origin_id,time_updated,origin_updated) </li>";
            msg+="<li>all row of table that doesn't have origin_id or origin_id=0 would be set as current table id a.k.a origin_id=id if the origin_id=0 (be carefull on multiple mode synch, can make data collision) </li>";
            msg+="<li>all row of table that doesn't have origin or origin='' would be set as current autonomous a.k.a origin='current_autonomous' if the origin='' (be carefull on multiple mode synch, can make data collision) </li>";
        msg+="</ul>";
        msg+="Do You Really want to proceed !!";
        
        bootbox.confirm({
            title: "Procedure Confirmation",
            message: msg,
            buttons: {
                confirm: {
                    label: '<i class="fa fa-play"></i> Yes',
                    className: 'btn-danger'
                },
                cancel: {
                    label: '<i class="fa fa-ban"></i> No',
                    className: 'btn-primary'
                }
            },
            callback: function (result) {
                if(result){
                    showLoading();
                    $.post("",a,function(res){
                        var json=getContent(res);
                        dismissLoading();
                    });
                }
            }
        });
    };
});