var responselog;
$(document).ready(function(){
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
	var column=new Array('name','alias','no_dtd','icd','id');
	responselog=new TableAction("responselog","smis-administrator","responselog",column);
	responselog.show=function(id){
		var data=this.getEditData(id);
		data['id']=id;
		data['command']="select";
		showLoading();
		$.post("",data,function(res){
			var json=getContent(res);
			showWarning("Response Log : "+id,json.tabs);
			$("pre").fetreefy();
			dismissLoading();
		});
	};
	responselog.clean=function(){
		var data=this.getRegulerData();
		data['command']="clean";
		$.post("",data,function(res){
			responselog.view();
		});
	};
	responselog.view();
});