$(document).ready(function(){
	reload_risma();
});
var IS_RUNNING_REALTIME_risma=false;
var IS_STILL_RUNNING_risma=false;

function patch_once(){
	var html=$("#btn_risma").html();
	$("#header").append(html);
}

function get_risma(){
	var reg_data={	
			page:"smis-administrator",
			action:"risma",
			command:"do",
			super_command:"",
			prototype_name:"",
			prototype_slug:"",
			prototype_implement:""
			};
	return reg_data;
}

function reload_risma(){
	if(IS_STILL_RUNNING_risma) return;
	IS_STILL_RUNNING_risma=true;
	showLoading();
	var d=get_risma();
	$.post("",d,function(res){
		var json=getContent(res);
		$("#risma_inside").html(json);
		patch_once();
		IS_STILL_RUNNING_risma=false;
		dismissLoading();
	});
}

function realtime_risma(){
	IS_RUNNING_REALTIME_risma=true;
	run_realtime_risma();
}

function run_realtime_risma(){
	if(IS_STILL_RUNNING_risma || !IS_RUNNING_REALTIME_risma) return;
	IS_STILL_RUNNING_risma=true;
	var d=get_risma();
	$.post("",d,function(res){
		var json=getContent(res);
		$("#risma_inside").html(json);
		patch_once();
		IS_STILL_RUNNING_risma=false;
		setTimeout(run_realtime_risma, 1000);
	});
}

function stop_real_time_risma(){
	IS_RUNNING_REALTIME_risma=false;
}
