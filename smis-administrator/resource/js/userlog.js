var userlog;
$(document).ready(function(){
	$('.mydate').datepicker();
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
	var column=new Array();
	userlog=new TableAction("userlog","smis-administrator","userlog",column);
	userlog.addViewData=function(d){
		d['database']=$("#userlog_database").val();
		d['id']=$("#userlog_id").val();
		d['dari']=$("#userlog_dari").val();
		d['sampai']=$("#userlog_sampai").val();
		return d;
	};
	userlog.view();
	userlog.restore=function(id){
		var params=this.getViewData();
		params['command']="restore";
		params['id']=id;
		$.post('',params,function(res){
			var json=getContent(res);
			if(json==null) {
				return;
			}
			userlog.view();
		});
	};

	userlog.clean=function(){
		var data=this.getRegulerData();
		data['command']="clean";
		$.post("",data,function(res){
			userlog.view();
		});
	};
	
	userlog.show=function(id){
		var edt=this.getEditData();
		edt['id']=id;
		showLoading();
		$.post("",edt,function(res){
			var json=getContent(res);
			var html="<font class='label label'>POST</font>";
			html+="<pre class='spjson'>"+json.post+"</pre>";
			html+="<div class='line clear'></div>";
			html+="<font class='label label-inverse'>ORIGINAL</font>";
			html+="<pre class='spjson'>"+json.original+"</pre>";
			html+="<div class='line clear'></div>";
			html+="<font class='label label-success'>EDDITED</font>";
			html+="<pre class='spjson'>"+json.edited+"</pre>";
			html+="<div class='line clear'></div>";
			html+="<font class='label label-info'>DIFFERENT</font>";
			html+="<div>"+json.compare+"</div>";
			showWarning("User Log : "+id,html);
			$(".spjson").fetreefy();
			dismissLoading();
		});
	};
	
	
});
