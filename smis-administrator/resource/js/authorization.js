var authorization;
var user;

$(document).ready(function(){
	authorization=new TableAction("authorization","smis-administrator","authorization");
	user=new TableAction("user","smis-administrator","authorization");
	user.setSuperCommand("user");
	user.selected=function(json){
		$("#authorization_nama").val(json.realname);
		$("#authorization_id").val(json.id);
		$("#authorization_username").val(json.username);
		$("#authorization_authority").val(json.authority);
		show();
	};

	authorization.onall=function(e){
		$("input[menu='"+e+"']").each(function(index,value){
			var a  = $(this).attr("submenu");
			var id = "#"+e+"_"+a;
			$(id).bootstrapSwitch('setState',true);
		});
	};

	authorization.ofall=function(e){
		$("input[menu='"+e+"']").each(function(index,value){
			var a  = $(this).attr("submenu");
			var id = "#"+e+"_"+a;
			$(id).bootstrapSwitch('setState',false);
		});
	};
});

function show(){
	showLoading();
	var data={	
			page		: 'smis-administrator',
			action		: 'authorization',
			command		: "show",
			userid		: $("#authorization_id").val()
			};
	$.post('',data,function(res){
		$("#table_content").html(res);
		$('.smis_switch').bootstrapSwitch();
		$('.smis_switch').on('switch-change', function (e, data) {
			var menu	= $(data.el).attr("menu");
			var submenu	= $(data.el).attr("submenu");
			var enabled	= data.value;
			activated(menu,submenu,enabled);
		});
		dismissLoading();
	});
}


function activated(menu,submenu,enabled){
	var value=0;
	if(enabled) value=1;
	var data={	
			page:'smis-administrator',
			action:'authorization',
			command:"save",
			userid:$("#authorization_id").val(),
			menu:menu,
			submenu:submenu,
			value:value
			};
	showLoading();
	$.post('',data,function(res){
		var content=getContent(res);
		dismissLoading();
	});
}