var silmei;
$(document).ready(function(){
	$('#silmei-summernote').summernote({
		  height: 400,
		  minHeight: 300,
		  onImageUpload: function(files, editor, welEditable) {
                sendFile(files[0], editor, welEditable);
            },
            toolbar: [
	          ['insert',['table','picture','link','video','hr']],
	          ['fontsize', ['fontsize']],
	          ['style', ['fontname','bold', 'italic', 'underline','strikethrough', 'clear']],
	          ['font', ['strikethrough', 'superscript', 'subscript']],
	          ['color', ['color']],
	          ['para', ['ul', 'ol', 'paragraph']],
	          ['height', ['height']],
	          ['misc', ['fullscreen','codeview','undo','redo','help']]
	        ]
  		}
	);
    
    $('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
	var column=new Array();
	silmei=new TableAction("silmei","smis-administrator","silmei",column);
    
    silmei.deploy=function(){
        var a=this.getRegulerData();
        a['command']="deploy";
        a['filename']=$("#silmei_filename").val();
        a['location']=$("#silmei_location").val();
        a['hasil']=$('#silmei-summernote').code();
        showLoading();
        $.post("",a,function(res){
            var content=getContent(res);
            $('#silmei-summernote').code(content);
            dismissLoading();
        });
    }
    
    silmei.load=function(){
        var a=this.getRegulerData();
        a['command']="load";
        a['filename']=$("#silmei_filename").val();
        a['location']=$("#silmei_location").val();
        showLoading();
        $.post("",a,function(res){
            var content=getContent(res);
            $('#silmei-summernote').code(content);
            dismissLoading();
        });
    }
    
    
    
});