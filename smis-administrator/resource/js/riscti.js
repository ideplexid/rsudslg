var riscti;
var $_100B;
var $_500B;
var $_1KB;
var $_100KB;
var $_500KB;
var $_1MB;
var $_riscti_running=false;
function formatDate(dateObj){
    var monthNames = [ "Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember" ];
    var curr_date = dateObj.getDate();
    var curr_month = dateObj.getMonth();
    curr_month = curr_month + 1;
    var curr_year = dateObj.getFullYear();
    var curr_min = dateObj.getMinutes();
    var curr_hr= dateObj.getHours();
    var curr_sc= dateObj.getSeconds();
    if(curr_month.toString().length == 1)
    curr_month = '0' + curr_month;      
    if(curr_date.toString().length == 1)
    curr_date = '0' + curr_date;
    if(curr_hr.toString().length == 1)
    curr_hr = '0' + curr_hr;
    if(curr_min.toString().length == 1)
    curr_min = '0' + curr_min;
    return curr_date+" "+monthNames[curr_month-1]+" "+curr_year+ " "+curr_hr+":"+curr_min+":"+curr_sc;
}

function packDataCreator(number){
    var kirim;
    if(number==0){
        return "";
    }
    
    if(number>=100){
        if($_100B==null){
            $_100B="";
            for(i=0;i<100;i++){
                $_100B+="a";
            }                
        }
        kirim=$_100B;
    }
    
    if(number>=500 ){
        if($_500B==null){
            $_500B="";
            for(i=0;i<5;i++){
                $_500B+=$_100B;
            }                
        }
        kirim=$_500B;  
    }
    
    if(number>=1024){
        if($_1KB==null){
            $_1KB="";
            for(i=0;i<2;i++){
                $_1KB+=$_500B;
            }
            $_1KB+="aaaaaaaaaaaa";             
        }
        kirim=$_1KB;  
    }
    
    if(number>=102400){
        if($_100KB==null){
            $_100KB="";
            for(i=0;i<100;i++){
                $_100KB+=$_1KB;
            }                
        }
        kirim=$_100KB;  
    }
    
    if(number>=512000){
        if($_500KB==null){
            $_500KB="";
            for(i=0;i<5;i++){
                $_500KB+=$_100KB;
            }    
        }        
        kirim=$_500KB;  
    }
    
    if(number>=1048576){
        if($_1MB==null){
            $_1MB="";
            for(i=0;i<2;i++){
                $_1MB+=$_500KB;
            }    
        }        
        kirim=$_1MB; 
    }
    
    return kirim;
}

$(document).ready(function(){
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
	var column=new Array('filter',"send_limit","process_limit","receive_limit","periode","running_count","packet_data");
	riscti=new TableAction("riscti","smis-administrator","riscti",column);
	riscti.view();
    
    riscti.reset=function(){
        riscti.clear();
        $("#riscti_list").html("");
    }
    
    riscti.clean=function(){
        $("#riscti_list").html("");
    }
    
    riscti.run=function(){
       $_riscti_running=true;
       riscti.running();
    }
    
    riscti.stop=function(){
        $_riscti_running=false;
    }
    
    
    
    riscti.running=function(){
        if($_riscti_running==false){
            return;
        }
        var filter=Number($("#riscti_filter").val());
        var s_limit=Number($("#riscti_send_limit").val());
        var p_limit=Number($("#riscti_process_limit").val());
        var r_limit=Number($("#riscti_receive_limit").val());
        var running_count=Number($("#riscti_running_count").val())+1;
        var periode=Number($("#riscti_periode").val());
        var a=this.getRegulerData();
        a['command']=$("#riscti_activity").val();
        a['packet']=packDataCreator(Number($("#riscti_packet_data").val()));
        var date_send=new Date();
        var pack_sent_time=date_send.getTime()/1000;
        $.post("",a,function(res){
            var json=getContent(res);
            var date_received=new Date();
            var pack_receive_time=date_received.getTime()/1000;
            
            var micro_receive=json.micro_receive;
            var micro_response=json.micro_response;
            var ttime_receive=json.ttime_receive;
            var process_time=json.process_time;
            var time_response=json.time_response;
            var activity=json.activity;
            
            var send_time=(micro_receive-pack_sent_time).toFixed(5);
            var response_time=(pack_receive_time-micro_response).toFixed(5);
            
            var showed=false;
            if(filter==0){
                showed=true;
            }

            if( (filter&1 || filter==0) && send_time>=s_limit ){
                send_time="<span class='badge badge-important'>"+send_time+"</span>";
                showed=true;
            }
            
            console.log((filter&2)+"  "+process_time+"  "+p_limit);
            if((filter&2 || filter==0) && process_time>=p_limit){
                process_time="<span class='badge badge-warning'>"+process_time+"</span>";
                showed=true;
            }

            if((filter&4 || filter==0) && response_time>=r_limit){
                response_time="<span class='badge badge-success'>"+response_time+"</span>";
                showed=true;
            }
            
            
            if(showed){
                var result="<tr>";
                    result+="<td>"+running_count+".</td>";
                    result+="<td>"+activity+".</td>";
                    result+="<td>"+a['packet'].length+" B</td>";
                    result+="<td>"+formatDate(date_send)+"</td>";
                    result+="<td>"+send_time+"</td>";
                    result+="<td>"+ttime_receive+"</td>";
                    result+="<td>"+process_time+"</td>";
                    result+="<td>"+time_response+"</td>";
                    result+="<td>"+response_time+"</td>";
                    result+="<td>"+formatDate(date_received)+"</td>";
                result+="</tr>";
                
                $("#riscti_list").prepend(result);
            }
            $("#riscti_running_count").val(running_count);
            setTimeout(function(){
                riscti.running();
            },periode);
        });
    };
    
    
    
});