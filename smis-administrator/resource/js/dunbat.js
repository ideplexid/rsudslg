var dunbat;
$(document).ready(function(){
	dunbat=new FileAction("dunbat","smis-administrator","dunbat",new Array("oldname","fileowner","filename","filetype","filemode","isold"));
	dunbat.view();
	
	dunbat.openfile=function(file){
		var base=this.getRegulerData();
		base['filename']=file;
		base['command']="load";
		showLoading();
		$.post("",base,function(res){
			var json=getContent(res);
			if(json!=null && json!=""){
				fname=file.substring(0,19);
				var html="<font class='label label'>POST</font>";
				html+="<pre class='spjson'>"+json.post+"</pre>";
				html+="<div class='line clear'></div>";
				html+="<font class='label label-inverse'>ORIGINAL</font>";
				html+="<pre class='spjson'>"+json.original+"</pre>";
				html+="<div class='line clear'></div>";
				html+="<font class='label label-success'>EDDITED</font>";
				html+="<pre class='spjson'>"+json.edited+"</pre>";
				html+="<div class='line clear'></div>";
				html+="<font class='label label-info'>DIFFERENT</font>";
				html+="<div>"+json.compare+"</div>";
				showFullWarning("File "+fname,html,"half_model");
				$(".spjson").fetreefy();	
			}
			dismissLoading();
		});		
	};
	
	dunbat.restore=function(file){
		var base=this.getRegulerData();
		base['filename']=file;
		base['command']="restore";
		showLoading();
		$.post("",base,function(res){
			var json=getContent(res);
			dunbat.view();
			dismissLoading();
		});		
	};
	
	dunbat.addRegulerData=function(d){
		d['folder']=this.getBaseFolder();
		d['database']=$("#dunbat_database").val();
		d['dbid']=$("#dunbat_dbid").val();
		return d;
	};
	
	$("#dunbat_folder").keypress(function(e){
		if(e.which==13){
			e.preventDefault();
			dunbat.view();
		}
	});
	
	$("#dunbat_database").keypress(function(e){
		if(e.which==13){
			$("#dunbat_dbid").focus();
		}
	});
	
	$("#dunbat_dbid").keypress(function(e){
		if(e.which==13){
			dunbat.view();
		}
	});
	
});