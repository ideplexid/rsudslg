$(document).ready(function(){
	reload_ametis();
});
var IS_RUNNING_REALTIME_ametis=false;
var IS_STILL_RUNNING_ametis=false;

function patch_once(){
	var html=$("#btn_ametis").html();
	$("#header").append(html);
}

function get_ametis(){
	var reg_data={	
			page:"smis-administrator",
			action:"ametis",
			command:"do",
			super_command:"",
			prototype_name:"",
			prototype_slug:"",
			prototype_implement:""
			};
	return reg_data;
}

function reload_ametis(){
	if(IS_STILL_RUNNING_ametis) return;
	IS_STILL_RUNNING_ametis=true;
	showLoading();
	var d=get_ametis();
	$.post("",d,function(res){
		var json=getContent(res);
		$("#ametis_inside").html(json);
		patch_once();
		IS_STILL_RUNNING_ametis=false;
		dismissLoading();
	});
}

function realtime_ametis(){
	IS_RUNNING_REALTIME_ametis=true;
	run_realtime_ametis();
}

function run_realtime_ametis(){
	if(IS_STILL_RUNNING_ametis || !IS_RUNNING_REALTIME_ametis) return;
	IS_STILL_RUNNING_ametis=true;
	var d=get_ametis();
	$.post("",d,function(res){
		var json=getContent(res);
		$("#ametis_inside").html(json);
		patch_once();
		IS_STILL_RUNNING_ametis=false;
		setTimeout(run_realtime_ametis, 1000);
	});
}

function stop_real_time_ametis(){
	IS_RUNNING_REALTIME_ametis=false;
}
