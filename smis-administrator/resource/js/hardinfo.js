$(document).ready(function(){
	reload_hard_info();
});
var IS_RUNNING_REALTIME=false;
var IS_STILL_RUNNING=false;

function patch_once(){
	var html=$("#btn_hardinfo").html();
	$("#header").append(html);
}

function get_hardinfo(){
	var reg_data={	
			page:"smis-administrator",
			action:"hardinfo",
			command:"do",
			super_command:"",
			prototype_name:"",
			prototype_slug:"",
			prototype_implement:""
			};
	return reg_data;
}

function reload_hard_info(){
	if(IS_STILL_RUNNING) return;
	IS_STILL_RUNNING=true;
	showLoading();
	var d=get_hardinfo();
	$.post("",d,function(res){
		var json=getContent(res);
		$("#hardinfo_inside").html(json);
		patch_once();
		IS_STILL_RUNNING=false;
		dismissLoading();
	});
}

function realtime_hard_info(){
	IS_RUNNING_REALTIME=true;
	run_realtime_hard_info();
}

function run_realtime_hard_info(){
	if(IS_STILL_RUNNING || !IS_RUNNING_REALTIME) return;
	IS_STILL_RUNNING=true;
	var d=get_hardinfo();
	$.post("",d,function(res){
		var json=getContent(res);
		$("#hardinfo_inside").html(json);
		patch_once();
		IS_STILL_RUNNING=false;
		setTimeout(run_realtime_hard_info, 1000);
	});
}

function stop_real_time(){
	IS_RUNNING_REALTIME=false;
}
