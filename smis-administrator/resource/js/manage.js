function ManageAction(name,page,action,column){
	this.initialize(name, page, action, column);
}
ManageAction.prototype.constructor = ManageAction;
ManageAction.prototype=new TableAction();
ManageAction.prototype.edit=function (id){
	var self=this;
	showLoading();	
	var edit_data=this.getEditData(id);
	$.post('',edit_data,function(res){		
		var json=getContent(res);
		if(json==null) return;
		for(var i=0;i<self.column.length;i++){
			var name=self.column[i];
			$("#"+self.prefix+"_"+name).val(json[""+name]);
		}
		$("#"+self.prefix+"_username").prop('disabled', true);
		$("#"+self.prefix+"_realname").prop('disabled', true);
		$("#"+self.prefix+"_add_form").smodal('show');
		dismissLoading();
	});
};
TableAction.prototype.reset=function (id){
	var self=this;
	showLoading();	
	var edit_data=this.getRegulerData(id);
	edit_data['command']="reset";
	edit_data['id']=id;
	$.post('',edit_data,function(res){		
		var json=getContent(res);
		dismissLoading();
	});
};


TableAction.prototype.access=function (id){
	var self=this;
	activeTab("#authoritytabs");
	user.select(id);//call from authorization.php
};

TableAction.prototype.clear=function(){	
	for(var i=0;i<this.column.length;i++){
		var name=this.column[i];
		$("#"+this.prefix+"_"+name).val("");
	}
	$("#"+this.prefix+"_username").prop('disabled', false);
	$("#"+this.prefix+"_realname").prop('disabled', false);
};


var manage;
$(document).ready(function(){
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
	var column=new Array('id','realname','username','email','authority','foto');
	manage=new ManageAction("manage","smis-administrator","manage",column);
	manage.view();
});