var requestlog;
$(document).ready(function(){
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
	var column=new Array('name','alias','no_dtd','icd','id');
	requestlog=new TableAction("requestlog","smis-administrator","requestlog",column);
	requestlog.show=function(id){
		var data=this.getEditData(id);
		data['id']=id;
		data['command']="select";
		showLoading();
		$.post("",data,function(res){
			var json=getContent(res);
			showWarning("Request Log : "+id,json.tabs);
			$("pre").fetreefy();
			dismissLoading();
		});
	};
	requestlog.clean=function(){
		var data=this.getRegulerData();
		data['command']="clean";
		$.post("",data,function(res){
			requestlog.view();
		});
	};
	requestlog.view();
});
