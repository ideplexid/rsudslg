var santi;
var sceditormirror;
CodeMirror.commands.autocomplete = function(cm) {
		cm.showHint({hint: CodeMirror.hint.anyword});
}
function makeMarker() {
		var marker = document.createElement("div");
		marker.style.color = "#822";
		marker.innerHTML = "●";
		return marker;
}

$(document).ready(function(){
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
	var column=new Array();
	santi=new TableAction("santi","smis-administrator","santi",column);    
    santi.runcode=function(){
        var a=this.getRegulerData();
        a['command']="runcode";
        a['source']=sceditormirror.getValue();
        showLoading();
        $.post("",a,function(res){
            var content=getContent(res);
            dismissLoading();
        });
    }    
	sceditormirror = CodeMirror(document.getElementById("santi-sceditormirror"), {
	  value: "<\?php \?>",
	  mode:  "application/x-httpd-php",
	  indentUnit:5,
	  electricChars:false,
	  lineNumbers: true,
	  lineWrapping: false,
	  autofocus:true,
      autoCloseTags: true,
	  styleActiveLine: true,
	  matchTags: {bothTags: true},
	  autoCloseBrackets: true,
	  viewportMargin: Infinity,
	  showTrailingSpace: true,
	  commentBlankLines:true,
	  highlightSelectionMatches: {showToken: /\w/},
	  extraKeys: {"Ctrl-Space": "autocomplete","Ctrl-F11": function(cm) {cm.setOption("fullScreen", !cm.getOption("fullScreen"));}},
	  foldGutter: true,
	  gutters: ["CodeMirror-linenumbers", "CodeMirror-foldgutter","breakpoints"]		  
	});

	sceditormirror.on("gutterClick", function(cm, n) {
	  var info = cm.lineInfo(n);
	  cm.setGutterMarker(n, "breakpoints", info.gutterMarkers ? null : makeMarker());
	});

	$("#santi_theme, #santi_linenumber, #santi_linewrap, #santi_actag, #santi_acbrac, #santi_aline, #santi_tspace, #santi_foldgutter").on("change",function(){
		var id=this.id;
		if(id=="santi_theme") sceditormirror.setOption("theme", this.value);
		if(id=="santi_linenumber") sceditormirror.setOption("lineNumbers", this.value=="y");
		if(id=="santi_linewrap") sceditormirror.setOption("lineWrapping", this.value=="y");
		if(id=="santi_actag") sceditormirror.setOption("autoCloseTags", this.value=="y");
		if(id=="santi_acbrac") sceditormirror.setOption("autoCloseBrackets", this.value=="y");
		if(id=="santi_aline") sceditormirror.setOption("styleActiveLine", this.value=="y");
		if(id=="santi_tspace") sceditormirror.setOption("showTrailingSpace", this.value=="y");
		if(id=="santi_foldgutter") sceditormirror.setOption("foldGutter", this.value=="y");
	});
    
    setTimeout(function(){  $("#santi_theme").trigger("change"); }, 1000);
   
    
});