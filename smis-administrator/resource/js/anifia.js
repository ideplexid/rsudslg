$(document).ready(function(){
	reload_anifia();
});
var IS_RUNNING_REALTIME_ANIFIA=false;
var IS_STILL_RUNNING_ANIFIA=false;

function patch_once(){
	var html=$("#btn_anifia").html();
	$("#header").append(html);
}

function get_anifia(){
	var reg_data={	
			page:"smis-administrator",
			action:"anifia",
			command:"do",
			super_command:"",
			prototype_name:"",
			prototype_slug:"",
			prototype_implement:""
			};
	return reg_data;
}

function reload_anifia(){
	if(IS_STILL_RUNNING_ANIFIA) return;
	IS_STILL_RUNNING_ANIFIA=true;
	showLoading();
	var d=get_anifia();
	$.post("",d,function(res){
		var json=getContent(res);
		$("#anifia_inside").html(json);
		patch_once();
		IS_STILL_RUNNING_ANIFIA=false;
		dismissLoading();
	});
}

function realtime_anifia(){
	IS_RUNNING_REALTIME_ANIFIA=true;
	run_realtime_anifia();
}

function run_realtime_anifia(){
	if(IS_STILL_RUNNING_ANIFIA || !IS_RUNNING_REALTIME_ANIFIA) return;
	IS_STILL_RUNNING_ANIFIA=true;
	var d=get_anifia();
	$.post("",d,function(res){
		var json=getContent(res);
		$("#anifia_inside").html(json);
		patch_once();
		IS_STILL_RUNNING_ANIFIA=false;
		setTimeout(run_realtime_anifia, 1000);
	});
}

function stop_real_time_anifia(){
	IS_RUNNING_REALTIME_ANIFIA=false;
}
