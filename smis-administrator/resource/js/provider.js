var provider;
$(document).ready(function(){
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
	var column=new Array('name','alias','no_dtd','icd','id');
	provider=new TableAction("provider","smis-administrator","provider",column);
	provider.show=function(id){
		var data=this.getEditData(id);
		data['id']=id;
		data['command']="select";
		showLoading();
		$.post("",data,function(res){
			var json=getContent(res);
			showWarning("Synch Provider Log : "+id,json.Log);
			dismissLoading();
		});
	};
	provider.clean=function(){
		var data=this.getRegulerData();
		data['command']="clean";
		$.post("",data,function(res){
			provider.view();
		});
	};
	provider.view();
});