var rireist;
$(document).ready(function(){
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
	var column=new Array('id',"deploy","table");
	rireist=new TableAction("rireist","smis-administrator","rireist",column);
	rireist.view();
    
    rireist.reverse=function(id){
        var a=this.getRegulerData();
        a['command']="reverse";
        a['id']=id;
        $.post("",a,function(res){
            var json=getContent(res);
            $("#rireist_code").html(json);
            $("#rireist_table").val(id);
            rireist.show_form();
        });
    }
    
    rireist.deploy=function(){
        if(!this.cekSave()){
            return;
        }
        var a=this.getSaveData();
        var self=this;
        a['command']="deploy";
        a['id']=$("#rireist_table").val();
        $("#"+this.prefix+"_add_form").smodal('hide');
        showLoading();
        $.post("",a,function(res){
            var json=getContent(res);
            dismissLoading();
        });
    }
    
});