var malik;
$(document).ready(function(){
	malik=new FileAction("malik","smis-administrator","malik",new Array("oldname","fileowner","filename","filetype","filemode","isold"));
	malik.view();
	
	malik.openfile=function(file){
		var base=this.getRegulerData();
		base['filename']=file;
		base['command']="load";
		showLoading();
		$.post("",base,function(res){
			var json=getContent(res);
			if(json!=null && json!=""){
				fname=file.substring(0,19);
				showFullWarning("File "+fname,json,"half_model");
			}
			dismissLoading();
		});		
	};
	
	malik.addRegulerData=function(d){
		d['folder']=this.getBaseFolder();
		d['hash']=$("#malik_hash").val();
		d['service']=$("#malik_service").val();
		return d;
	};
	
	$("#malik_folder").keypress(function(e){
		if(e.which==13){
			e.preventDefault();
			malik.view();
		}
	});
	
	$("#malik_service").keypress(function(e){
		if(e.which==13){
			$("#malik_hash").focus();
		}
	});
	
	$("#malik_hash").keypress(function(e){
		if(e.which==13){
			malik.view();
		}
	});
	
});