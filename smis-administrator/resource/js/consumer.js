var consumer;
$(document).ready(function(){
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
	var column=new Array('name','alias','no_dtd','icd','id');
	consumer=new TableAction("consumer","smis-administrator","consumer",column);
	consumer.show=function(id){
		var data=this.getEditData(id);
		data['id']=id;
		data['command']="select";
		showLoading();
		$.post("",data,function(res){
			var json=getContent(res);
			showWarning("Synch Consumer Log : "+id,"<pre>"+json.Log+"</pre>");
			dismissLoading();
		});
	};
	consumer.clean=function(){
		var data=this.getRegulerData();
		data['command']="clean";
		$.post("",data,function(res){
			consumer.view();
		});
	};
	consumer.view();
});