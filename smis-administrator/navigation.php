<?php
global $NAVIGATOR;
$admin      = new Menu("fa fa-user-secret");
$admin      ->addProperty('title',"Administration");
$admin      ->addProperty('name','Administrator');
$admin      ->addSubMenu("User Management","smis-administrator","user","Manage User" ,"fa fa-users");
$admin      ->addSubMenu("Plugins","smis-administrator","plugins","Plugins Mangement of this Autonomous","fa fa-plug");
$admin      ->addSubMenu("Prototype","smis-administrator","prototype","Prototype of Each Plugins" ,"fa fa-cog");
$admin      ->addSeparator();
$admin      ->addSubMenu("Database","smis-administrator","database","Database Management" ,"fa fa-database");
$admin      ->addSubMenu("Server Activity","smis-administrator","log","Log of SMIS" ,"fa fa-cloud");
$admin      ->addSubMenu("Server Management","smis-administrator","filemanager","File Manager System","fa fa-server");
$admin      ->addSubMenu("Server Information","smis-administrator","servinfo","Server Information","fa fa-info");
$admin      ->addSubMenu("Log Manager","smis-administrator","logman","Log File Manager","fa fa-server");
$admin      ->addSubMenu("About Safethree","smis-administrator","smis","About Safethree" ,"fa fa-asterisk");
$admin      ->addSeparator();
$admin      ->addSubMenu("Basic Data","smis-administrator","basicdata","Feedback and Notification User" ,"fa fa-lightbulb-o");
$admin      ->addSubMenu("Autonomous Settings","smis-administrator","settings","Settings of this Autonomous","fa fa-cogs");
$admin      ->addSubMenu("Development","smis-administrator","development","Page For Goblooge" ,"fa fa-user-circle");
$NAVIGATOR  ->addMenu($admin,'smis-administrator');

$operator   = new Menu("fa fa-users");
$operator   ->addProperty('title',"Operator");
$operator   ->addProperty('name','Operator');
$operator   ->addSubMenu("User Management","operator","user_operator","Acess User" ,"fa fa-lightbulb-o","","","smis-administrator");
$NAVIGATOR  ->addMenu($operator,'operator');

?>