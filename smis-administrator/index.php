<?php
global $db;
global $user;
require_once 'smis-libs-class/Policy.php';
$policy=new Policy("smis-administrator", $user);
if($user->getCapability()=="administrator"){
	if(file_exists("smis-administrator/".$_POST['action'].".php")){
		$policy->setDefaultCookie(Policy::$DEFAULT_COOKIE_CHANGE);
		$policy->allow($_POST['page'], $_POST['action']);
	}else if(file_exists("smis-administrator/snippet/".$_POST['action'].".php")){
		$policy->setDefaultCookie(Policy::$DEFAULT_COOKIE_CHANGE);
		$policy->allow($_POST['page'], "snippet/".$_POST['action']);
	}else if(file_exists("smis-administrator/modul/".$_POST['action'].".php")) {
		$policy->setDefaultCookie(Policy::$DEFAULT_COOKIE_KEEP);
		$policy->allow($_POST['page'], "modul/".$_POST['action']);
		setChangeCookie(false);
	}else{
        $folder=getAllFileInDir("smis-administrator/resource/php/",false,false);
		foreach($folder as $f){
			$path="smis-administrator/resource/php/".$f."/".$_POST['action'].".php";
            if(file_exists($path)){
                $policy->setDefaultCookie(Policy::$DEFAULT_COOKIE_KEEP);
				$policy->allow($_POST['page'], "resource/php/".$f."/".$_POST['action']);
				setChangeCookie(false);
				break;
			}
		}
	}	
}else if($user->getCapability()=="operator"){
	$policy->addPolicy("user_operator", "user_operator",Policy::$DEFAULT_COOKIE_KEEP,"modul/user_operator");
	$policy->addPolicy("manage", "user_operator",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/user/manage");
	$policy->addPolicy("authorization", "user_operator",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/user/authorization");
	$policy->addAlias("user_operator", "operator");
	$policy->addAlias("manage", "operator");
	$policy->addAlias("authorization", "operator");
	$policy->initialize();
}else{
	$policy->denied();
}
?>
