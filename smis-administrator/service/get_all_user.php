<?php 
global $db;

$autonomous=getSettings($db, "smis_autonomous_id", "");
$dbtable=new DBTable($db, "smis_adm_user");
$dbtable->setShowAll(true);
$data=$dbtable->view("","0");
$adapter=new SimpleAdapter();
$adapter->add("name", "realname");
$adapter->add("username", "username");
$adapter->add("email","email");
$adapter->add("autonomous", $autonomous,"fix");
$list=$adapter->getContent($data['data']);

echo json_encode($list);
?>