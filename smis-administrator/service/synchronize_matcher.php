<?php 
    global $db;
    global $wpdb;
    require_once "smis-administrator/function/synch_match_builder.php";
    $dbtable=new DBTable($db,"smis_adm_synch_provider");
    $result=synch_match_build($wpdb,$db,$dbtable,$_POST['code_name']);
    echo json_encode($result);
?>