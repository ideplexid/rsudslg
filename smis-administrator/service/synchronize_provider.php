<?php 

global $db;
global $user;
global $querylog;
require_once "smis-administrator/class/service/SynchronizeServiceProvider.php";
show_error();
$post=$_POST;
$autonomous_name=getSettings($db,"smis_autonomous_id","");
$dbtable=new DBTable($db,"smis_adm_synch_provider");

$kriteria['code_name']=$_POST['smis-synch-consumer-code-name'];
$provider=$dbtable->select($kriteria);

$serv=null;
if($provider->table_name!="smis_adm_settings"){
    $serv= new SynchronizeServiceProvider($db,$user, $provider,$post,$autonomous_name);
}else{
    require_once "smis-administrator/class/service/SynchronizeSettingsProvider.php";
    $serv= new SynchronizeSettingsProvider($db,$user, $provider,$post,$autonomous_name);
}

$serv->setProviderDBTable($dbtable);
$serv->initialize();

$log            =$serv->getLog();
$service_code   =$_POST["smis_code"];
$code_name      =$provider->code_name;
$querylog->saveSynchProvider($code_name,$log,$service_code);
?>