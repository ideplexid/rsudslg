<?php 

$alert=new Alert("help_backup", "Cara Membackup", "Modul ini dapat digunakan untuk membackup baik seluruh maupun sebagian data pada system. dengan memanfaatkan modul ini dapat dengan mudah melakukan maintenance dan persiapan jika terjadi kegagalan pada server. untuk melakukan restore dapat menggunakan System Restore atau menggunakan MySQL Workbench.");
$alert->setModel(Alert::$INVERSE);
$alert->addList("Keterangan", "\"<i>Data Request</i>\" adalah jumlah data yang akan di backup dalam satu kali proses request, semakin kecil semakin stabil tetapi semakin lama, semakin besar semakin cepat tetapi semakin rawan untuk terjadi kegagalan (tergantung limitasi kemampuan server) ");
$alert->addList("Keterangan", "\"<i>Data Load</i>\" adalah jumlah maksimal baris data dalam satu buah file, rekomendasi 1 file berisi 1 table, single file berarti semua data dimasukan dalam 1 file, tidak direkomendasi kecuali mengetahui bagaimana menggunakan MySQL Workbench ");
$alert->addList("Keterangan", "\"<i>Filter Table</i>\" adalah kelompok table yang akan dibackup (lihat keterangan kelompok table). ");
$alert->addList("Keterangan", "\"<i>Kriteria</i>\" adalah kriteria untuk memfilter table yang akan di backup (berfungsi pada Mode Table SQL dan Kriteria ) ");
$alert->addList("Keterangan", "pada Filter Table SQL, kriteria yang dipakai menggunakan Syntax MySQL seperti :  \"id LIKE 'smis_adm%' AND id LIKE '%service%'\" maka semua table yang berawalan 'smis_adm' dan mengandung kata 'service' akan diikutsertakan ");
$alert->addList("Keterangan", "pada Filter Table Criteria , kriteria yang dipakai menggunakan String Biasa yang mana jika nama Table Mengandung nama tersebut maka akan diikutsertakan seperti : \"adm\" maka semua table yang mengandung kata adm akan diikutsertaan");
$alert->addString("adapun filter table yang tersedia adalah sebagai berikut : ");
$alert->addList("Filter Table", "\"<i>All Table</i>\" Membackup semua Table yand ada dalam Database.");
$alert->addList("Filter Table", "\"<i>All Table Without Log</i>\" Membackup semua Table yand ada dalam Database kecuali Table Log.");
$alert->addList("Filter Table", "\"<i>Log Only</i>\" : Membackup semua Table yang termasuk dalam Kategori Table Log ");
$alert->addList("Filter Table", "\"<i>Modul Only</i>\" : Membackup semua Table Tidak Termasuk dalam Table Safethree Essential, Safethree Tools dan Safethree Serverbus");
$alert->addList("Filter Table", "\"<i>Safethree Essential</i>\" : Membackup semua Table yang termasuk dalam Safethree Essential");
$alert->addList("Filter Table", "\"<i>Safethree Serverbus</i>\" : Membackup semua Table yang termasuk dalam Safethree Serverbus");
$alert->addList("Filter Table", "\"<i>Safethree Tools</i>\" : Membackup semua Table yang termasuk dalam Safethree Tools");
$alert->addList("Filter Table", "\"<i>Safethree Essential + Serverbus</i>\" : Membackup semua Table yang termasuk dalam Safethree Essential dan Safethree Serverbus");
$alert->addList("Filter Table", "\"<i>Safethree Essential + Tools</i>\" : Membackup semua Table yang termasuk dalam Safethree Essential dan Safethree Tools");
$alert->addList("Filter Table", "\"<i>Safethree Serverbus + Tools</i>\" : Membackup semua Table yang termasuk dalam Safethree Serverbus dan Safethree Tools");
$alert->addList("Filter Table", "\"<i>Safethree Essential + Serverbus + Tools</i>\" : Membackup semua Table yang termasuk dalam Safethree Essential , Safethree Serverbus dan Safethree Tools");
$alert->addList("Filter Table", "\"<i>Kriteria</i>\" : Membackup semua Table yang namanya mengandung kata yang sesuai dengan yang di inputkan di bagian kriteria ");
$alert->addList("Table Group","Kelompok <strong>Table Log</strong> adalah table yang sebetulnya tidak dibutuhkan oleh user,
		tetapi dibutuhkan oleh system untuk mencatat setiap kejadian dalam system,
		selain itu digunakan pula untuk pelaporan kesalahan system, serta sebagai backup
		jika user melakukan kesalahan, sehingga mudah ditelusuri dan di-restore.
		adapaun yang termasuk dalam kelompok ini antara lain
		smis_adm_entity_response, smis_adm_service_request, smis_adm_service_responds,
		smis_adm_error, smis_adm_log, smis_adm_upload, smis_sb_log");
$alert->addList("Table Group","Kelompok <strong>Table Safethree Essential</strong>  adalah Table yang harus ada dalam System Kehilangan 1 Table Essential
		mengakibatkan Kegagalan System .
		adapaun yang termasuk dalam Table ini adalah, smis_adm_entity_response, smis_adm_error,
		smis_adm_log, smis_adm_prototype, smis_adm_service_requests, smis_adm_service_responds,
		smis_adm_settings, smis_adm_upload, smis_adm_user");
$alert->addList("Table Group","Kelompok <strong>Safethree Tools</strong>  adalah
		Table yang digunakan untuk Mendukung Funsionalitas dari System,
		Kehilangan Table ini tidak menyebabkan Kegagalan dalam System. tetapi Beberapa Fitur Pendukung Tidak Berfungsi
		adapaun yang termasuk dalam Table ini adalah smis_base_feedback, smis_base_notification,
		smis_tools_calendar, smis_tools_print, smis_tools_satisfy, smis_tools_share");
$alert->addList("Table Group","Kelompok <strong>Safethree Serverbus</strong>  adalah
		Table untuk System Autonomous yang digunakan Sebagai Server Bus, Table ini hanya wajib jika Autonomous menjadi Server Bus
		Kehilangan Table ini tidak menyebabkan Kegagalan dalam System Server Bus.
		adapaun yang termasuk dalam Table ini adalah smis_sb_autonomous, smis_sb_log, smis_adm_entity_response");


$alert->addList("Log Table", "\"<i>smis_adm_entity_response</i>\" : digunakan untuk mencatat setiap Hasil Response yang dilakukan oleh Autonomous-Autonomous ketika melayani Server Bus");
$alert->addList("Log Table", "\"<i>smis_adm_service_request</i>\" : digunakan untuk mencatat setiap Request yang dilakukan  Autonomous untuk meminta service kepada Server Bus");
$alert->addList("Log Table", "\"<i>smis_adm_service_responds</i>\" : digunakan untuk mencatat setiap Response yang dilakukan  Autonomous ketika menjawab service Server Bus");
$alert->addList("Log Table", "\"<i>smis_adm_error</i>\" : digunakan untuk mencatat setiap Aktivitas yang dilakukan oleh User Terhadap Sebuah Autonomous");
$alert->addList("Log Table", "\"<i>smis_adm_log</i>\" : digunakan untuk mencatat setiap Aktivitas yang dilakukan oleh User Terhadap Sebuah Data dalam Table (kecuali Table Safethree Essential)");
$alert->addList("Log Table", "\"<i>smis_sb_log</i>\" : digunakan untuk mencatat setiap aktivitas Server Bus dalam Melayani setiap Autonomous");
$alert->addList("Log Table", "\"<i>smis_adm_upload</i>\" : digunakan untuk mencatat setiap aktivitas Upload User");

echo $alert->setAlertMode(false)->getHTML();

?>