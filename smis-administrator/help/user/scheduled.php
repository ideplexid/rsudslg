
<p align="justify">
to use this feature user must invoke the index.php from console or terminal or command prompt (CMD).
so user could bind the php function with sceduled task or cron table in spesify Operating System.
</p>

</br>

<h4>In Windows</h4>
<p align="justify">
bind with a .bat file in , make sure that the working directory is in this index.php file directory.
use cd function inside the .bat file and paste this following code in the last line. afer that bind with Task Scheduler in Windows 
</p>
<pre>
# php.exe index.php username password
</pre>

<i>Example of Batch (.bat) file in Windows</i>
make a file name smis.bat, and copy paste this following code in the file

<pre>
cd c:/xampp/htdocs/smis/
php.exe index.php myuser mypassword
</pre>

<p align="justify"><i><strong>c:/xampp/htdocs/smis/</strong></i> should be change following this web 
full path in the folder. then you can bind the smis.bat file with task scheduler.
how to run task scheduler in windows, google it.</p>

</br>
<h4>In Linux / Mac / BSD (Unix Derivatives)</h4>
<p align="justify">
bind with a .sh file in , make sure that the working directory is in this index.php file directory.
use cd function inside the .sh file and paste this following code in the last line. then bind with Cron Table in Linux/Mac.
</p>
<pre>
# php index.php username password
</pre>

<i>Example of Shell (.sh) file in Linux</i>
make a file name smis.sh, and copy paste this following code in the file and don't forget to give +x acces in the file (read chmod)

<pre>
cd /var/www/html/smis/
php index.php myuser mypassword
</pre>
<i><strong>/var/www/html/smis/</strong></i> should be change following this web 
full path in the folder. then you can bind the smis.sh file with cron table.
how to run cron in linux/mac, you can read here.
<ul>
    <li><a target='_blank' href='http://www.goblooge.com/blog/membuat-cron-tab-job-di-linux-untuk-otomasi-sebuah-pekerjaan/'>Create Crontab Job in Linux</a></li>
    <li><a target='_blank' href='http://www.goblooge.com/blog/cara-menggunakan-syntax-crontab-job-untuk-otomasi-pekerjaan/'>Use Crontab Job for Task Automaton</li>
    <li><a target='_blank' href='http://www.goblooge.com/blog/mengatur-penggunaan-waktu-dalam-crontab-job-untuk-otomasi-pekerjaan/'>Manage Event Time in Crontab Job</li>
    <li><a target='_blank' href='http://www.goblooge.com/blog/memahami-set-up-dari-crontab-job-di-linux/'>Understanding Crontab Job in Linux</a></li>
</ul>

</br>
<h4>Post Script</h4>
<p align="justify">
Username and password that should be type literally in the .bat or .sh file. <i>&lt;myuser&gt;</i> and <i>&lt;mypassword&gt;</i> should be change with exist user
in the smis system. the best practice should use the user that have not administrator privilege, it's much better if this user doesn't have any privilege 
except control their own settings (for security reason)</p>