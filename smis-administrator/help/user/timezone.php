<div id='timezone'>
	<table class="doctable table alert alert-success">
		<caption>
			<strong>Africa</strong>
		</caption>
		<tbody class="tbody">
			<tr>
				<td>Africa/Abidjan</td>
				<td>Africa/Accra</td>
				<td>Africa/Addis_Ababa</td>
				<td>Africa/Algiers</td>
				<td>Africa/Asmara</td>
			</tr>

			<tr>
				<td>Africa/Asmera</td>
				<td>Africa/Bamako</td>
				<td>Africa/Bangui</td>
				<td>Africa/Banjul</td>
				<td>Africa/Bissau</td>
			</tr>

			<tr>
				<td>Africa/Blantyre</td>
				<td>Africa/Brazzaville</td>
				<td>Africa/Bujumbura</td>
				<td>Africa/Cairo</td>
				<td>Africa/Casablanca</td>
			</tr>

			<tr>
				<td>Africa/Ceuta</td>
				<td>Africa/Conakry</td>
				<td>Africa/Dakar</td>
				<td>Africa/Dar_es_Salaam</td>
				<td>Africa/Djibouti</td>
			</tr>

			<tr>
				<td>Africa/Douala</td>
				<td>Africa/El_Aaiun</td>
				<td>Africa/Freetown</td>
				<td>Africa/Gaborone</td>
				<td>Africa/Harare</td>
			</tr>

			<tr>
				<td>Africa/Johannesburg</td>
				<td>Africa/Juba</td>
				<td>Africa/Kampala</td>
				<td>Africa/Khartoum</td>
				<td>Africa/Kigali</td>
			</tr>

			<tr>
				<td>Africa/Kinshasa</td>
				<td>Africa/Lagos</td>
				<td>Africa/Libreville</td>
				<td>Africa/Lome</td>
				<td>Africa/Luanda</td>
			</tr>

			<tr>
				<td>Africa/Lubumbashi</td>
				<td>Africa/Lusaka</td>
				<td>Africa/Malabo</td>
				<td>Africa/Maputo</td>
				<td>Africa/Maseru</td>
			</tr>

			<tr>
				<td>Africa/Mbabane</td>
				<td>Africa/Mogadishu</td>
				<td>Africa/Monrovia</td>
				<td>Africa/Nairobi</td>
				<td>Africa/Ndjamena</td>
			</tr>

			<tr>
				<td>Africa/Niamey</td>
				<td>Africa/Nouakchott</td>
				<td>Africa/Ouagadougou</td>
				<td>Africa/Porto-Novo</td>
				<td>Africa/Sao_Tome</td>
			</tr>

			<tr>
				<td>Africa/Timbuktu</td>
				<td>Africa/Tripoli</td>
				<td>Africa/Tunis</td>
				<td>Africa/Windhoek</td>
				<td class="empty">&nbsp;</td>
			</tr>

		</tbody>

	</table>

	<table class="doctable table alert alert-warning">
		<caption>
			<strong>America</strong>
		</caption>

		<tbody class="tbody">
			<tr>
				<td>America/Adak</td>
				<td>America/Anchorage</td>
				<td>America/Anguilla</td>
				<td>America/Antigua</td>
				<td>America/Araguaina</td>
			</tr>

			<tr>
				<td>America/Argentina/Buenos_Aires</td>
				<td>America/Argentina/Catamarca</td>
				<td>America/Argentina/ComodRivadavia</td>
				<td>America/Argentina/Cordoba</td>
				<td>America/Argentina/Jujuy</td>
			</tr>

			<tr>
				<td>America/Argentina/La_Rioja</td>
				<td>America/Argentina/Mendoza</td>
				<td>America/Argentina/Rio_Gallegos</td>
				<td>America/Argentina/Salta</td>
				<td>America/Argentina/San_Juan</td>
			</tr>

			<tr>
				<td>America/Argentina/San_Luis</td>
				<td>America/Argentina/Tucuman</td>
				<td>America/Argentina/Ushuaia</td>
				<td>America/Aruba</td>
				<td>America/Asuncion</td>
			</tr>

			<tr>
				<td>America/Atikokan</td>
				<td>America/Atka</td>
				<td>America/Bahia</td>
				<td>America/Bahia_Banderas</td>
				<td>America/Barbados</td>
			</tr>

			<tr>
				<td>America/Belem</td>
				<td>America/Belize</td>
				<td>America/Blanc-Sablon</td>
				<td>America/Boa_Vista</td>
				<td>America/Bogota</td>
			</tr>

			<tr>
				<td>America/Boise</td>
				<td>America/Buenos_Aires</td>
				<td>America/Cambridge_Bay</td>
				<td>America/Campo_Grande</td>
				<td>America/Cancun</td>
			</tr>

			<tr>
				<td>America/Caracas</td>
				<td>America/Catamarca</td>
				<td>America/Cayenne</td>
				<td>America/Cayman</td>
				<td>America/Chicago</td>
			</tr>

			<tr>
				<td>America/Chihuahua</td>
				<td>America/Coral_Harbour</td>
				<td>America/Cordoba</td>
				<td>America/Costa_Rica</td>
				<td>America/Creston</td>
			</tr>

			<tr>
				<td>America/Cuiaba</td>
				<td>America/Curacao</td>
				<td>America/Danmarkshavn</td>
				<td>America/Dawson</td>
				<td>America/Dawson_Creek</td>
			</tr>

			<tr>
				<td>America/Denver</td>
				<td>America/Detroit</td>
				<td>America/Dominica</td>
				<td>America/Edmonton</td>
				<td>America/Eirunepe</td>
			</tr>

			<tr>
				<td>America/El_Salvador</td>
				<td>America/Ensenada</td>
				<td>America/Fort_Wayne</td>
				<td>America/Fortaleza</td>
				<td>America/Glace_Bay</td>
			</tr>

			<tr>
				<td>America/Godthab</td>
				<td>America/Goose_Bay</td>
				<td>America/Grand_Turk</td>
				<td>America/Grenada</td>
				<td>America/Guadeloupe</td>
			</tr>

			<tr>
				<td>America/Guatemala</td>
				<td>America/Guayaquil</td>
				<td>America/Guyana</td>
				<td>America/Halifax</td>
				<td>America/Havana</td>
			</tr>

			<tr>
				<td>America/Hermosillo</td>
				<td>America/Indiana/Indianapolis</td>
				<td>America/Indiana/Knox</td>
				<td>America/Indiana/Marengo</td>
				<td>America/Indiana/Petersburg</td>
			</tr>

			<tr>
				<td>America/Indiana/Tell_City</td>
				<td>America/Indiana/Vevay</td>
				<td>America/Indiana/Vincennes</td>
				<td>America/Indiana/Winamac</td>
				<td>America/Indianapolis</td>
			</tr>

			<tr>
				<td>America/Inuvik</td>
				<td>America/Iqaluit</td>
				<td>America/Jamaica</td>
				<td>America/Jujuy</td>
				<td>America/Juneau</td>
			</tr>

			<tr>
				<td>America/Kentucky/Louisville</td>
				<td>America/Kentucky/Monticello</td>
				<td>America/Knox_IN</td>
				<td>America/Kralendijk</td>
				<td>America/La_Paz</td>
			</tr>

			<tr>
				<td>America/Lima</td>
				<td>America/Los_Angeles</td>
				<td>America/Louisville</td>
				<td>America/Lower_Princes</td>
				<td>America/Maceio</td>
			</tr>

			<tr>
				<td>America/Managua</td>
				<td>America/Manaus</td>
				<td>America/Marigot</td>
				<td>America/Martinique</td>
				<td>America/Matamoros</td>
			</tr>

			<tr>
				<td>America/Mazatlan</td>
				<td>America/Mendoza</td>
				<td>America/Menominee</td>
				<td>America/Merida</td>
				<td>America/Metlakatla</td>
			</tr>

			<tr>
				<td>America/Mexico_City</td>
				<td>America/Miquelon</td>
				<td>America/Moncton</td>
				<td>America/Monterrey</td>
				<td>America/Montevideo</td>
			</tr>

			<tr>
				<td>America/Montreal</td>
				<td>America/Montserrat</td>
				<td>America/Nassau</td>
				<td>America/New_York</td>
				<td>America/Nipigon</td>
			</tr>

			<tr>
				<td>America/Nome</td>
				<td>America/Noronha</td>
				<td>America/North_Dakota/Beulah</td>
				<td>America/North_Dakota/Center</td>
				<td>America/North_Dakota/New_Salem</td>
			</tr>

			<tr>
				<td>America/Ojinaga</td>
				<td>America/Panama</td>
				<td>America/Pangnirtung</td>
				<td>America/Paramaribo</td>
				<td>America/Phoenix</td>
			</tr>

			<tr>
				<td>America/Port-au-Prince</td>
				<td>America/Port_of_Spain</td>
				<td>America/Porto_Acre</td>
				<td>America/Porto_Velho</td>
				<td>America/Puerto_Rico</td>
			</tr>

			<tr>
				<td>America/Rainy_River</td>
				<td>America/Rankin_Inlet</td>
				<td>America/Recife</td>
				<td>America/Regina</td>
				<td>America/Resolute</td>
			</tr>

			<tr>
				<td>America/Rio_Branco</td>
				<td>America/Rosario</td>
				<td>America/Santa_Isabel</td>
				<td>America/Santarem</td>
				<td>America/Santiago</td>
			</tr>

			<tr>
				<td>America/Santo_Domingo</td>
				<td>America/Sao_Paulo</td>
				<td>America/Scoresbysund</td>
				<td>America/Shiprock</td>
				<td>America/Sitka</td>
			</tr>

			<tr>
				<td>America/St_Barthelemy</td>
				<td>America/St_Johns</td>
				<td>America/St_Kitts</td>
				<td>America/St_Lucia</td>
				<td>America/St_Thomas</td>
			</tr>

			<tr>
				<td>America/St_Vincent</td>
				<td>America/Swift_Current</td>
				<td>America/Tegucigalpa</td>
				<td>America/Thule</td>
				<td>America/Thunder_Bay</td>
			</tr>

			<tr>
				<td>America/Tijuana</td>
				<td>America/Toronto</td>
				<td>America/Tortola</td>
				<td>America/Vancouver</td>
				<td>America/Virgin</td>
			</tr>

			<tr>
				<td>America/Whitehorse</td>
				<td>America/Winnipeg</td>
				<td>America/Yakutat</td>
				<td>America/Yellowknife</td>
				<td class="empty">&nbsp;</td>
			</tr>

		</tbody>

	</table>

	<table class="doctable table alert alert-inverse">
		<caption>
			<strong>Antarctica</strong>
		</caption>

		<tbody class="tbody">
			<tr>
				<td>Antarctica/Casey</td>
				<td>Antarctica/Davis</td>
				<td>Antarctica/DumontDUrville</td>
				<td>Antarctica/Macquarie</td>
				<td>Antarctica/Mawson</td>
			</tr>

			<tr>
				<td>Antarctica/McMurdo</td>
				<td>Antarctica/Palmer</td>
				<td>Antarctica/Rothera</td>
				<td>Antarctica/South_Pole</td>
				<td>Antarctica/Syowa</td>
			</tr>

			<tr>
				<td>Antarctica/Troll</td>
				<td>Antarctica/Vostok</td>
				<td class="empty">&nbsp;</td>
				<td class="empty">&nbsp;</td>
				<td class="empty">&nbsp;</td>
			</tr>

		</tbody>

	</table>

	<table class="doctable table alert alert-important">
		<caption>
			<strong>Arctic</strong>
		</caption>

		<tbody class="tbody">
			<tr>
				<td>Arctic/Longyearbyen</td>
			</tr>

		</tbody>

	</table>

	<table class="doctable table alert alert-inverse">
		<caption>
			<strong>Asia</strong>
		</caption>

		<tbody class="tbody">
			<tr>
				<td>Asia/Aden</td>
				<td>Asia/Almaty</td>
				<td>Asia/Amman</td>
				<td>Asia/Anadyr</td>
				<td>Asia/Aqtau</td>
			</tr>

			<tr>
				<td>Asia/Aqtobe</td>
				<td>Asia/Ashgabat</td>
				<td>Asia/Ashkhabad</td>
				<td>Asia/Baghdad</td>
				<td>Asia/Bahrain</td>
			</tr>

			<tr>
				<td>Asia/Baku</td>
				<td>Asia/Bangkok</td>
				<td>Asia/Beirut</td>
				<td>Asia/Bishkek</td>
				<td>Asia/Brunei</td>
			</tr>

			<tr>
				<td>Asia/Calcutta</td>
				<td>Asia/Chita</td>
				<td>Asia/Choibalsan</td>
				<td>Asia/Chongqing</td>
				<td>Asia/Chungking</td>
			</tr>

			<tr>
				<td>Asia/Colombo</td>
				<td>Asia/Dacca</td>
				<td>Asia/Damascus</td>
				<td>Asia/Dhaka</td>
				<td>Asia/Dili</td>
			</tr>

			<tr>
				<td>Asia/Dubai</td>
				<td>Asia/Dushanbe</td>
				<td>Asia/Gaza</td>
				<td>Asia/Harbin</td>
				<td>Asia/Hebron</td>
			</tr>

			<tr>
				<td>Asia/Ho_Chi_Minh</td>
				<td>Asia/Hong_Kong</td>
				<td>Asia/Hovd</td>
				<td>Asia/Irkutsk</td>
				<td>Asia/Istanbul</td>
			</tr>

			<tr>
				<td>Asia/Jakarta</td>
				<td>Asia/Jayapura</td>
				<td>Asia/Jerusalem</td>
				<td>Asia/Kabul</td>
				<td>Asia/Kamchatka</td>
			</tr>

			<tr>
				<td>Asia/Karachi</td>
				<td>Asia/Kashgar</td>
				<td>Asia/Kathmandu</td>
				<td>Asia/Katmandu</td>
				<td>Asia/Khandyga</td>
			</tr>

			<tr>
				<td>Asia/Kolkata</td>
				<td>Asia/Krasnoyarsk</td>
				<td>Asia/Kuala_Lumpur</td>
				<td>Asia/Kuching</td>
				<td>Asia/Kuwait</td>
			</tr>

			<tr>
				<td>Asia/Macao</td>
				<td>Asia/Macau</td>
				<td>Asia/Magadan</td>
				<td>Asia/Makassar</td>
				<td>Asia/Manila</td>
			</tr>

			<tr>
				<td>Asia/Muscat</td>
				<td>Asia/Nicosia</td>
				<td>Asia/Novokuznetsk</td>
				<td>Asia/Novosibirsk</td>
				<td>Asia/Omsk</td>
			</tr>

			<tr>
				<td>Asia/Oral</td>
				<td>Asia/Phnom_Penh</td>
				<td>Asia/Pontianak</td>
				<td>Asia/Pyongyang</td>
				<td>Asia/Qatar</td>
			</tr>

			<tr>
				<td>Asia/Qyzylorda</td>
				<td>Asia/Rangoon</td>
				<td>Asia/Riyadh</td>
				<td>Asia/Saigon</td>
				<td>Asia/Sakhalin</td>
			</tr>

			<tr>
				<td>Asia/Samarkand</td>
				<td>Asia/Seoul</td>
				<td>Asia/Shanghai</td>
				<td>Asia/Singapore</td>
				<td>Asia/Srednekolymsk</td>
			</tr>

			<tr>
				<td>Asia/Taipei</td>
				<td>Asia/Tashkent</td>
				<td>Asia/Tbilisi</td>
				<td>Asia/Tehran</td>
				<td>Asia/Tel_Aviv</td>
			</tr>

			<tr>
				<td>Asia/Thimbu</td>
				<td>Asia/Thimphu</td>
				<td>Asia/Tokyo</td>
				<td>Asia/Ujung_Pandang</td>
				<td>Asia/Ulaanbaatar</td>
			</tr>

			<tr>
				<td>Asia/Ulan_Bator</td>
				<td>Asia/Urumqi</td>
				<td>Asia/Ust-Nera</td>
				<td>Asia/Vientiane</td>
				<td>Asia/Vladivostok</td>
			</tr>

			<tr>
				<td>Asia/Yakutsk</td>
				<td>Asia/Yekaterinburg</td>
				<td>Asia/Yerevan</td>
				<td class="empty">&nbsp;</td>
				<td class="empty">&nbsp;</td>
			</tr>

		</tbody>

	</table>

	<table class="doctable table alert alert-info">
		<caption>
			<strong>Atlantic</strong>
		</caption>

		<tbody class="tbody">
			<tr>
				<td>Atlantic/Azores</td>
				<td>Atlantic/Bermuda</td>
				<td>Atlantic/Canary</td>
				<td>Atlantic/Cape_Verde</td>
				<td>Atlantic/Faeroe</td>
			</tr>

			<tr>
				<td>Atlantic/Faroe</td>
				<td>Atlantic/Jan_Mayen</td>
				<td>Atlantic/Madeira</td>
				<td>Atlantic/Reykjavik</td>
				<td>Atlantic/South_Georgia</td>
			</tr>

			<tr>
				<td>Atlantic/St_Helena</td>
				<td>Atlantic/Stanley</td>
				<td class="empty">&nbsp;</td>
				<td class="empty">&nbsp;</td>
				<td class="empty">&nbsp;</td>
			</tr>

		</tbody>

	</table>

	<table class="doctable table alert alert-warning">
		<caption>
			<strong>Australia</strong>
		</caption>

		<tbody class="tbody">
			<tr>
				<td>Australia/ACT</td>
				<td>Australia/Adelaide</td>
				<td>Australia/Brisbane</td>
				<td>Australia/Broken_Hill</td>
				<td>Australia/Canberra</td>
			</tr>

			<tr>
				<td>Australia/Currie</td>
				<td>Australia/Darwin</td>
				<td>Australia/Eucla</td>
				<td>Australia/Hobart</td>
				<td>Australia/LHI</td>
			</tr>

			<tr>
				<td>Australia/Lindeman</td>
				<td>Australia/Lord_Howe</td>
				<td>Australia/Melbourne</td>
				<td>Australia/North</td>
				<td>Australia/NSW</td>
			</tr>

			<tr>
				<td>Australia/Perth</td>
				<td>Australia/Queensland</td>
				<td>Australia/South</td>
				<td>Australia/Sydney</td>
				<td>Australia/Tasmania</td>
			</tr>

			<tr>
				<td>Australia/Victoria</td>
				<td>Australia/West</td>
				<td>Australia/Yancowinna</td>
				<td class="empty">&nbsp;</td>
				<td class="empty">&nbsp;</td>
			</tr>

		</tbody>

	</table>

	<table class="doctable table alert alert-info">
		<caption>
			<strong>Europe</strong>
		</caption>

		<tbody class="tbody">
			<tr>
				<td>Europe/Amsterdam</td>
				<td>Europe/Andorra</td>
				<td>Europe/Athens</td>
				<td>Europe/Belfast</td>
				<td>Europe/Belgrade</td>
			</tr>

			<tr>
				<td>Europe/Berlin</td>
				<td>Europe/Bratislava</td>
				<td>Europe/Brussels</td>
				<td>Europe/Bucharest</td>
				<td>Europe/Budapest</td>
			</tr>

			<tr>
				<td>Europe/Busingen</td>
				<td>Europe/Chisinau</td>
				<td>Europe/Copenhagen</td>
				<td>Europe/Dublin</td>
				<td>Europe/Gibraltar</td>
			</tr>

			<tr>
				<td>Europe/Guernsey</td>
				<td>Europe/Helsinki</td>
				<td>Europe/Isle_of_Man</td>
				<td>Europe/Istanbul</td>
				<td>Europe/Jersey</td>
			</tr>

			<tr>
				<td>Europe/Kaliningrad</td>
				<td>Europe/Kiev</td>
				<td>Europe/Lisbon</td>
				<td>Europe/Ljubljana</td>
				<td>Europe/London</td>
			</tr>

			<tr>
				<td>Europe/Luxembourg</td>
				<td>Europe/Madrid</td>
				<td>Europe/Malta</td>
				<td>Europe/Mariehamn</td>
				<td>Europe/Minsk</td>
			</tr>

			<tr>
				<td>Europe/Monaco</td>
				<td>Europe/Moscow</td>
				<td>Europe/Nicosia</td>
				<td>Europe/Oslo</td>
				<td>Europe/Paris</td>
			</tr>

			<tr>
				<td>Europe/Podgorica</td>
				<td>Europe/Prague</td>
				<td>Europe/Riga</td>
				<td>Europe/Rome</td>
				<td>Europe/Samara</td>
			</tr>

			<tr>
				<td>Europe/San_Marino</td>
				<td>Europe/Sarajevo</td>
				<td>Europe/Simferopol</td>
				<td>Europe/Skopje</td>
				<td>Europe/Sofia</td>
			</tr>

			<tr>
				<td>Europe/Stockholm</td>
				<td>Europe/Tallinn</td>
				<td>Europe/Tirane</td>
				<td>Europe/Tiraspol</td>
				<td>Europe/Uzhgorod</td>
			</tr>

			<tr>
				<td>Europe/Vaduz</td>
				<td>Europe/Vatican</td>
				<td>Europe/Vienna</td>
				<td>Europe/Vilnius</td>
				<td>Europe/Volgograd</td>
			</tr>

			<tr>
				<td>Europe/Warsaw</td>
				<td>Europe/Zagreb</td>
				<td>Europe/Zaporozhye</td>
				<td>Europe/Zurich</td>
				<td class="empty">&nbsp;</td>
			</tr>

		</tbody>

	</table>

	<table class="doctable table alert alert-warning">
		<caption>
			<strong>Indian</strong>
		</caption>

		<tbody class="tbody">
			<tr>
				<td>Indian/Antananarivo</td>
				<td>Indian/Chagos</td>
				<td>Indian/Christmas</td>
				<td>Indian/Cocos</td>
				<td>Indian/Comoro</td>
			</tr>

			<tr>
				<td>Indian/Kerguelen</td>
				<td>Indian/Mahe</td>
				<td>Indian/Maldives</td>
				<td>Indian/Mauritius</td>
				<td>Indian/Mayotte</td>
			</tr>

			<tr>
				<td>Indian/Reunion</td>
				<td class="empty">&nbsp;</td>
				<td class="empty">&nbsp;</td>
				<td class="empty">&nbsp;</td>
				<td class="empty">&nbsp;</td>
			</tr>

		</tbody>

	</table>

	<table class="doctable table alert alert-success">
		<caption>
			<strong>Pacific</strong>
		</caption>

		<tbody class="tbody">
			<tr>
				<td>Pacific/Apia</td>
				<td>Pacific/Auckland</td>
				<td>Pacific/Chatham</td>
				<td>Pacific/Chuuk</td>
				<td>Pacific/Easter</td>
			</tr>

			<tr>
				<td>Pacific/Efate</td>
				<td>Pacific/Enderbury</td>
				<td>Pacific/Fakaofo</td>
				<td>Pacific/Fiji</td>
				<td>Pacific/Funafuti</td>
			</tr>

			<tr>
				<td>Pacific/Galapagos</td>
				<td>Pacific/Gambier</td>
				<td>Pacific/Guadalcanal</td>
				<td>Pacific/Guam</td>
				<td>Pacific/Honolulu</td>
			</tr>

			<tr>
				<td>Pacific/Johnston</td>
				<td>Pacific/Kiritimati</td>
				<td>Pacific/Kosrae</td>
				<td>Pacific/Kwajalein</td>
				<td>Pacific/Majuro</td>
			</tr>

			<tr>
				<td>Pacific/Marquesas</td>
				<td>Pacific/Midway</td>
				<td>Pacific/Nauru</td>
				<td>Pacific/Niue</td>
				<td>Pacific/Norfolk</td>
			</tr>

			<tr>
				<td>Pacific/Noumea</td>
				<td>Pacific/Pago_Pago</td>
				<td>Pacific/Palau</td>
				<td>Pacific/Pitcairn</td>
				<td>Pacific/Pohnpei</td>
			</tr>

			<tr>
				<td>Pacific/Ponape</td>
				<td>Pacific/Port_Moresby</td>
				<td>Pacific/Rarotonga</td>
				<td>Pacific/Saipan</td>
				<td>Pacific/Samoa</td>
			</tr>

			<tr>
				<td>Pacific/Tahiti</td>
				<td>Pacific/Tarawa</td>
				<td>Pacific/Tongatapu</td>
				<td>Pacific/Truk</td>
				<td>Pacific/Wake</td>
			</tr>

			<tr>
				<td>Pacific/Wallis</td>
				<td>Pacific/Yap</td>
				<td class="empty">&nbsp;</td>
				<td class="empty">&nbsp;</td>
				<td class="empty">&nbsp;</td>
			</tr>

		</tbody>

	</table>

	<table class="doctable table alert alert-info">
		<caption>
			<strong>Others</strong>
		</caption>

		<tbody class="tbody">
			<tr>
				<td>Brazil/Acre</td>
				<td>Brazil/DeNoronha</td>
				<td>Brazil/East</td>
				<td>Brazil/West</td>
				<td>Canada/Atlantic</td>
			</tr>

			<tr>
				<td>Canada/Central</td>
				<td>Canada/East-Saskatchewan</td>
				<td>Canada/Eastern</td>
				<td>Canada/Mountain</td>
				<td>Canada/Newfoundland</td>
			</tr>

			<tr>
				<td>Canada/Pacific</td>
				<td>Canada/Saskatchewan</td>
				<td>Canada/Yukon</td>
				<td>CET</td>
				<td>Chile/Continental</td>
			</tr>

			<tr>
				<td>Chile/EasterIsland</td>
				<td>CST6CDT</td>
				<td>Cuba</td>
				<td>EET</td>
				<td>Egypt</td>
			</tr>

			<tr>
				<td>Eire</td>
				<td>EST</td>
				<td>EST5EDT</td>
				<td>Etc/GMT</td>
				<td>Etc/GMT+0</td>
			</tr>

			<tr>
				<td>Etc/GMT+1</td>
				<td>Etc/GMT+10</td>
				<td>Etc/GMT+11</td>
				<td>Etc/GMT+12</td>
				<td>Etc/GMT+2</td>
			</tr>

			<tr>
				<td>Etc/GMT+3</td>
				<td>Etc/GMT+4</td>
				<td>Etc/GMT+5</td>
				<td>Etc/GMT+6</td>
				<td>Etc/GMT+7</td>
			</tr>

			<tr>
				<td>Etc/GMT+8</td>
				<td>Etc/GMT+9</td>
				<td>Etc/GMT-0</td>
				<td>Etc/GMT-1</td>
				<td>Etc/GMT-10</td>
			</tr>

			<tr>
				<td>Etc/GMT-11</td>
				<td>Etc/GMT-12</td>
				<td>Etc/GMT-13</td>
				<td>Etc/GMT-14</td>
				<td>Etc/GMT-2</td>
			</tr>

			<tr>
				<td>Etc/GMT-3</td>
				<td>Etc/GMT-4</td>
				<td>Etc/GMT-5</td>
				<td>Etc/GMT-6</td>
				<td>Etc/GMT-7</td>
			</tr>

			<tr>
				<td>Etc/GMT-8</td>
				<td>Etc/GMT-9</td>
				<td>Etc/GMT0</td>
				<td>Etc/Greenwich</td>
				<td>Etc/UCT</td>
			</tr>

			<tr>
				<td>Etc/Universal</td>
				<td>Etc/UTC</td>
				<td>Etc/Zulu</td>
				<td>Factory</td>
				<td>GB</td>
			</tr>

			<tr>
				<td>GB-Eire</td>
				<td>GMT</td>
				<td>GMT+0</td>
				<td>GMT-0</td>
				<td>GMT0</td>
			</tr>

			<tr>
				<td>Greenwich</td>
				<td>Hongkong</td>
				<td>HST</td>
				<td>Iceland</td>
				<td>Iran</td>
			</tr>

			<tr>
				<td>Israel</td>
				<td>Jamaica</td>
				<td>Japan</td>
				<td>Kwajalein</td>
				<td>Libya</td>
			</tr>

			<tr>
				<td>MET</td>
				<td>Mexico/BajaNorte</td>
				<td>Mexico/BajaSur</td>
				<td>Mexico/General</td>
				<td>MST</td>
			</tr>

			<tr>
				<td>MST7MDT</td>
				<td>Navajo</td>
				<td>NZ</td>
				<td>NZ-CHAT</td>
				<td>Poland</td>
			</tr>

			<tr>
				<td>Portugal</td>
				<td>PRC</td>
				<td>PST8PDT</td>
				<td>ROC</td>
				<td>ROK</td>
			</tr>

			<tr>
				<td>Singapore</td>
				<td>Turkey</td>
				<td>UCT</td>
				<td>Universal</td>
				<td>US/Alaska</td>
			</tr>

			<tr>
				<td>US/Aleutian</td>
				<td>US/Arizona</td>
				<td>US/Central</td>
				<td>US/East-Indiana</td>
				<td>US/Eastern</td>
			</tr>

			<tr>
				<td>US/Hawaii</td>
				<td>US/Indiana-Starke</td>
				<td>US/Michigan</td>
				<td>US/Mountain</td>
				<td>US/Pacific</td>
			</tr>

			<tr>
				<td>US/Pacific-New</td>
				<td>US/Samoa</td>
				<td>UTC</td>
				<td>W-SU</td>
				<td>WET</td>
			</tr>

			<tr>
				<td>Zulu</td>
				<td class="empty">&nbsp;</td>
				<td class="empty">&nbsp;</td>
				<td class="empty">&nbsp;</td>
				<td class="empty">&nbsp;</td>
			</tr>

		</tbody>

	</table>
	<div>
		<style type="text/css">
#timezone td {
	font-size: 15px;
}
</style>