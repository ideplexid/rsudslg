<?php
    global $db;
    global $user;
    global $querylog;
    
    /**took how much sync table at a time */
    $table_sync = getSettings($db,"smis-adm-synch-times",1)*1;
    if($table_sync<0){
        $table_sync=1;
    }
    while($table_sync>0){
        $query="SELECT * FROM smis_adm_synch_consumer WHERE prop!='del' ORDER BY last_fetch ASC ";
        $consumer=$db->get_row($query);
        if($consumer!=null){
            require_once("smis-base/smis-include-service-consumer.php");
            require_once "smis-administrator/class/service/SynchronizeServiceConsumer.php";
            $autonomous_name = getSettings($db,"smis_autonomous_id","");
            $serv            = null;
            if($consumer->table_name=="smis_adm_settings"){
                require_once "smis-administrator/class/service/SynchronizeSettingsConsumer.php";
                $serv    = new SynchronizeSettingsConsumer($db,$user,$consumer,$autonomous_name);
            }else{
                $serv    = new SynchronizeServiceConsumer($db,$user,$consumer,$autonomous_name);            
            }
            /** ditambahkan supaya cek data dulu baik upload maupun download baru dhitung  */
            $serv->initialize();

            $message = "";
            $log     = "";
            $times           = $consumer->round_time;
            if($times*1 < 0){
                $times       = 1;          
                if($consumer->table_name!="smis_adm_settings"){
                    $loop_once = 0;
                    /** adaptive looping */
                    while($loop_once<2){ 
                        $loop_once++;
                        $num_upload     = $consumer->queue_data_upload;
                        $num_download   = $consumer->queue_data_download;
                        $is_upload      = $consumer->upload_consumer   == "1";
                        $is_download    = $consumer->download_consumer == "1";
                        $max_upload     = $consumer->upload_max_data;
                        $max_download   = $consumer->download_max_data;
                        if($is_upload && $num_upload>0 || $is_download && $num_download>0){
                            /**find data in upload that need to be synch */
                            $message    = "Adaptive Loop Processing !!\n";
                            $log       .= $message;
                            echo $message;
                            
                            $times_up   = ($is_upload)?(ceil($num_upload/$max_upload)):0;
                            $times_down = ($is_download)?(ceil($num_download/$max_download)):0;
                            $times      = ($times_up>$times_down)?$times_up:$times_down;
                            break;
                        }else if($loop_once==1){
                            /** if nothing to synch fetch at least once to make sure */
                            $message    = "Prefetch Process !!\n";
                            $log       .= $message;
                            echo $message;

                            $serv->initialize();
                            $consumer   = $db->get_row($query);
                        }else if($loop_once==2){
                            /**nothing to fetch because trying twice nothing to synch*/
                            $message    = "No Process !!\n";
                            $log       .= $message;
                            echo $message;                        
                            $times = 0;
                        }
                    }
                }           
            }
            $system="cls";
            if(strtoupper(substr(PHP_OS,0,3))!="WIN"){
                $system="clear";
            }
            $message  = "Table  : ".$consumer->table_name."\n";
            $message .= "Total Loop : ".$times." \n";
            $log     .= $message;
            echo $message;
                            
            for($x=0;$x<$times;$x++){
                $serv->initialize();
                system($system);
                $message  = "#".$consumer->table_name."# \n";
                $message .= "===== LOOP : ".$x."/".$times." === \n";
                $message .= $serv->getProsesResume();
                $elog     = $serv->getLog();
                if($elog!=""){
                    $message .= "====== LOG ".$x."/".$times." ===== \n";
                    $message .= $elog."\n";    
                }
                $message .= "= END LOOP : ".$x."/".$times." === \n\n";
                echo $message;
                $log     .= $message;
            }
            $code_name       = $consumer->code_name;
            $querylog->saveSynchConsumer($code_name,$log);
            
        }
        $table_sync--;
    }

    
?>