<?php
global $PLUGINS;
require_once ("smis-framework/smis/api/Plugin.php");
$init ['name'] = 'smis-administrator';
$init ['path'] = SMIS_DIR . "smis-administrator/";
$init ['description'] = "this plugins is base plugins that contain an administrator based sytem, 
					such as user management, server management etc and cannot be deactivated
					if you deactivated or remove this element from your system
					the sky will fall on your head.";
$init ['require'] = "";
$init ['service'] = "nothing";
$init ['version'] = "6.2.9";
$init ['number'] = "29";
$init ['type'] = "";

$myplugins = new Plugin ( $init );
$PLUGINS [$init ['name']] = $myplugins;
?>