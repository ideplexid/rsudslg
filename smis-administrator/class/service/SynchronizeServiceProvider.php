<?php 

class SynchronizeServiceProvider{
    /*@var Database*/
    protected $db;
    /*@var User*/
    protected $user;
    /*@var string*/
    protected $sync_id;
    /*@var string*/
    protected $code;
    /*@var string*/
    protected $table_name;
    /*@var string*/
    protected $autonomous;
    /*@var bool*/
    protected $is_upload;
    /*@var bool*/
    protected $is_download;
    /*@var int*/
    protected $max_upload;
    /*@var int*/
    protected $max_download;
    /*@var string*/
    protected $filter_upload;
    /*@var string*/
    protected $filter_download;
    
    /*@var int*/
    protected $total_uploaded;
    /*@var int*/
    protected $total_downloaded;
    /*@var int*/
    protected $queue_upload;
    /*@var int*/
    protected $queue_download;
    /*@var DBTable*/
    protected $dbtable;
    /*@var DBTable*/
    protected $provider_dbtable;    
    /*@var string*/
    protected $log;    
    /*@var string*/
    protected $synch_model;    
    /*@var array*/
    protected $post_data;
    /*@var string*/
    protected $current_autonomous_name;
    
    /*@var Array*/
    protected $response;
    
    
    public function __construct(Database $db,User $user, $provider, $data,$autonomous_name){
        $this->db                       = $db;
        $this->sync_id                  = $provider->id;
        $this->code                     = $provider->code_name;
        $this->table_name               = $provider->table_name;
        $this->autonomous               = $provider->autonomous;
        
        $this->is_upload                = $provider->upload_provider=="1";
        $this->max_upload               = $provider->upload_max_data;
        $this->filter_upload            = $provider->filter_origin_upload;
        $this->total_uploaded           = $provider->total_data_upload;
        $this->queue_upload             = $provider->queue_data_upload;
        
        $this->is_download              = $provider->download_provider=="1";
        $this->max_download             = $provider->download_max_data;
        $this->filter_download          = $provider->filter_origin_download;
        $this->total_downloaded         = $provider->total_data_download;
        $this->queue_download           = $provider->queue_data_download;
        $this->log                      = "";
        $this->post_data                = $data;
        $this->synch_model              = $provider->synch_model;
        $this->current_autonomous_name  = $autonomous_name;
        $this->response=array();
    }
    
    public function initialize(){
        $command=$this->post_data['smis-synch-consumer-command'];
        $this->initDBTable($this->db);
        $this->fetch();
        if($command=="upload"){
            /*upload process*/
            $array_success=$this->uploadProcess();
            $total_succes=count($array_success);
            $this->putResponse("smis-synch-total-success",$total_succes);
            $this->putResponse("smis-synch-data-success",$array_success);
            $this->postUpload($total_succes);
            
            /*download process*/
            $this->downloadProcess();
        }else if($command=="download"){
            $this->postDownload($this->post_data);
        }
        echo json_encode($this->response);
    }
    
    /**
     * @brief set the DBTable of provider so doesn't need to initialize
     * @param DBTable $dbtable 
     * @return this 
     */
    public function setProviderDBTable(DBTable $dbtable){
        $this->provider_dbtable=$dbtable;
        return $this;
    }
    
    protected function postDownload($post_data){
        if($post_data!=null && isset($post_data['smis-synch-total-success']) && isset($post_data['smis-synch-data-success']) ){
            $succes_count=$post_data['smis-synch-total-success'];
            $succes_data=$post_data['smis-synch-data-success'];
            foreach($succes_data as $x){
                $update['autonomous']="concat(autonomous,'[".$this->autonomous."]')";
                $warp['autonomous']=DBController::$NO_STRIP_ESCAPE_WRAP;
                $id=$x['id'];
                $result=$this->dbtable->update($update,$id,$warp);
                if($result===false){
                    $this->log("Post Download ",$this->dbtable->getLastError());
                }
            }
            $consumer_update['queue_data_download']=$this->getQueueDownload();
            $consumer_update['total_data_download']="total_data_download+".$succes_count;            
            $warp_consumer['queue_data_download']=DBController::$NO_STRIP_ESCAPE_WRAP;
            $warp_consumer['total_data_download']=DBController::$NO_STRIP_ESCAPE_WRAP;            
            $id_consumer['id']=$this->sync_id;
            $result=$this->provider_dbtable->update($consumer_update,$id_consumer,$warp_consumer);
            if($result===false){
                $this->log("Post Download ",$this->provider_dbtable->getLastError());
            }
        }
    }
    
    /**
     * @brief only notify the that this model had been fetch
     * @return  null
     */
    protected function fetch(){
        $dt=date("Y-m-d H:i:s");
        $update['last_fetch']=$dt;
        $id['id']=$this->sync_id;
        $result=$this->provider_dbtable->update($update,$id);
        if($result===false){
            $this->log("Fetch",$this->provider_dbtable->getLastError());
        }
    }
    
    
    /**
     * @brief put the data that need to be send to consumer
     * @param String $name 
     * @param mixed $value 
     * @return  this
     */
    protected function putResponse($name,$value){
        $this->response[$name]=$value;
        return $this;
    }
    
    protected function downloadProcess(){
        if($this->is_download){
            $this->queue_download = $this->getQueueDownload();
            $data_will_send=$this->getDownloadData();
            $this->putResponse("smis-synch-provider-download-data",$data_will_send);   
            $this->putResponse("smis-synch-provider-download-queue",$this->queue_download);
        }else{
            $this->putResponse("smis-synch-provider-download-data",array());   
            $this->putResponse("smis-synch-provider-download-queue",0);
        }
    }
    
    /**
     * @brief get the total data that need to be downloaded bay the consumer
     * @return  int total of the data
     */
    protected function getQueueDownload(){
        if($this->filter_download!=""){
            $this->dbtable->addCustomKriteria(" origin ","='".$this->filter_download."'");
        }
        $this->dbtable->addCustomKriteria(" autonomous "," NOT LIKE '%[".$this->autonomous."]%'");
        $total=$this->dbtable->count("");
        return $total;
    }
    
    /**
     * @brief get the current list data that need to be download
     *         by the consumer
     * @return  Array of data
     */
    protected function getDownloadData(){
        $this->dbtable->setMaximum($this->max_download);
        $data=$this->dbtable->view("","0");
        $list=$data['data'];
        return $list;
    }
    
    
    /**
     * @brief process store data in the upload data
     * @return  Array of id that successfully stored/confirm/checked
     */
    protected function uploadProcess(){
        $success_array=array();
        if($this->is_upload){
           $list=$this->getUploadData();
           $maximum_upload=$this->max_upload;
           foreach($list as $data){
                $kriteria['origin_id']=0;
                $kriteria['origin']="";
                if(strpos($data['autonomous'],"[".$this->current_autonomous_name."]")===false){
                    /* adding this autonomous name in database so it will 
                     * bind that this data have synch to thisd autonomous
                     * */
                    $data['autonomous'].="[".$this->current_autonomous_name."]";    
                }
                
                if(strpos($data['autonomous'],"[".$this->autonomous."]")===false){
                    /**
                     * if there is no autonomous that sent this data in the $data['autonomous']
                     * added by it self to prevent synch loop back to the sender
                     * */                        
                    $data['autonomous'].="[".$this->autonomous."]";
                }
                
                if($this->synch_model=="multiple"){
                    /* if using the multiple model the 
                     * id should not be used
                     * instead using origin_id and origin as the primary key
                     * */
                   unset($data['id']);
                   $kriteria['origin_id']=$this->getData("origin_id",$data,0);
                   $kriteria['origin']=$this->getData("origin",$data, "");
                }

                if($kriteria['origin_id']==0 || $kriteria['origin']==""){
                    /* if the origin id and the origin is not set, 
                     * then simply used the id */
                    unset($kriteria['origin_id']);
                    unset($kriteria['origin']);
                    $kriteria['id']=$this->getData("id",$data, 0);
                }
                    
                if($this->filter_upload!="" && ( !isset($kriteria['origin']) || $kriteria['origin']!=$this->filter_upload ) ){
                    /**
                    * if filter upload activated but this one have no origin 
                    * or the origin not same as the filter upload
                    * the discard, but say already synch
                    * */
                    
                    $result=array();
                    $result['id']=$kriteria;
                    $result['autonomous']=$data['autonomous'];                
                    $success_array[]=$result;
                    
                }else{
                    $the_id=0;
                    $result=false;
                    if(!$this->dbtable->is_exist($kriteria,true)){
                        $result=$this->dbtable->insert($data);
                        $the_id=$this->dbtable->get_inserted_id();
                    }else {
                        $old=$this->dbtable->select($kriteria,false,false);
                        $the_id=$current->id;
                        if($old->time_updated<$data['time_updated']){
                            /**
                             * if the incoming data is more new than the current data
                             * then update, and say that already synch */
                            
                           $result=$this->dbtable->update($data,$kriteria); 
                        }else{
                            /**
                             * if the incoming data older then the current
                             * simply discard , and say already synch */
                            $result=true;
                        }
                    }
                    
                    if($result===false){
                        $this->log("Upload Process",$this->dbtable->getLastError());
                    }else{
                        /* if this process success then say already success */
                        $result=array();
                        $result['id']=$kriteria;
                        $result['autonomous']=$data['autonomous'];                
                        $success_array[]=$result;
                    }
                   
                    //limited the process so not taking so long
                    if($maximum_upload<=0){
                       break;
                    }
                    $maximum_upload--;
                }
                
                
           }
        }
        return $success_array;
    }
    
    /**
     * @brief get data from array, is empty then give default
     * @param string $name 
     * @param Array $data 
     * @param mixed $default 
     * @return  mixed value
     */
    protected function getData($name,$data,$default){
		if(isset($data[$name]))
			return $data[$name];
		return $default;
	}
    
    
    /**
     * @brief update the current database tell, that already synch
     *          and how many datay synch
     * @param Array $response 
     * @return  null
     */
    protected function postUpload($succes_count){          
        $provider_update['queue_data_upload']=$this->getQueueUpload()-$succes_count;
        $provider_update['total_data_upload']="total_data_upload+".$succes_count;   
        $provider_update['total_data_upload']="total_data_upload+".$succes_count;   
        $warp_provider['total_data_upload']=DBController::$NO_STRIP_ESCAPE_WRAP;            
        $id_provider['id']=$this->sync_id;
        $result=$this->provider_dbtable->update($provider_update,$id_provider,$warp_provider);
        if($result===false){
            $this->log("Post Upload",$this->provider_dbtable->getLastError());
        }
    }
    
    /**
     * @brief get the total data how many data left 
     * that will be uploaded by consumer 
     * @return  int the total data
     */
    protected function getQueueUpload(){
        return $this->post_data['smis-synch-consumer-upload-queue'];
    }
    
    /**
     * @brief get the current data that uploaded by consumer
     * @return  Array of data
     */
    protected function getUploadData(){
        return $this->post_data['smis-synch-consumer-upload-data'];
    }
    
    /**
     * @brief init the database
     * @param Database $db 
     * @return  
     */
    protected function initDBTable($db){
        $this->dbtable=new DBTable($db,$this->table_name);
        $this->dbtable->setRealDelete(true);
        $this->dbtable->setDelete(true);   
        if($this->provider_dbtable==null){
            $this->provider_dbtable=new DBTable($db,"smis_adm_synch_provider");
        }
    }
    
    protected function log($title,$errorlog){
        $this->log.="###Provider Error In <strong>".$title."</strong>###";
        $this->log.="<pre>";
        $this->log.=$errorlog;
        $this->log.="</pre>";
    }
    
    public function getLog(){
        return $this->log;
    }
    
}

?>