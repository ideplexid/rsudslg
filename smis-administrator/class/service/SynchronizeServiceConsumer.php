<?php 

class SynchronizeServiceConsumer extends ServiceConsumer{
    /*@var Database*/
    protected $db;
    /*@var User*/
    protected $user;
    /*@var string*/
    protected $sync_id;
    /*@var string*/
    protected $code;
    /*@var string*/
    protected $table_name;
    /*@var string*/
    protected $autonomous;
    /*@var bool*/
    protected $is_upload;
    /*@var bool*/
    protected $is_download;
    /*@var int*/
    protected $max_upload;
    /*@var int*/
    protected $max_download;
    /*@var string*/
    protected $filter_upload;
    /*@var string*/
    protected $filter_download;
    
    /*@var int*/
    protected $total_uploaded;
    /*@var int*/
    protected $total_downloaded;
    /*@var int*/
    protected $queue_upload;
    /*@var int*/
    protected $queue_download;
    /*@var DBTable*/
    protected $dbtable;
    /*@var DBTable*/
    protected $consumer_dbtable;
    /*@var Array*/
    protected $upload_data;
    /*@var Array*/
    protected $download_data;
    /*@var Array*/
    protected $success_upload_data;
    /*@var Array*/
    protected $success_download_data;

    /*@var int*/
    protected $result_total_upload;
    /*@var int*/
    protected $result_total_download;
    /*@var int*/
    protected $result_success_upload;
    /*@var int*/
    protected $result_success_download;
    /*@var int*/
    protected $result_remaining_upload;
    /*@var int*/
    protected $result_remaining_download;
    

    /*@var Array*/
    protected $response;
    
    /*@var string*/
    protected $log;
    protected $current_autonomous_name;
    /*@var string*/
    protected $synch_model;   
    
    public function __construct(Database $db,User $user, $consumer,$current_autonomous_name){
        $this->db                        = $db;
        $this->sync_id                   = $consumer->id;
        $this->code                      = $consumer->code_name;
        $this->table_name                = $consumer->table_name;
        $this->autonomous                = $consumer->autonomous;
        
        $this->is_upload                 = $consumer->upload_consumer=="1";
        $this->max_upload                = $consumer->upload_max_data;
        $this->filter_upload             = $consumer->filter_origin_upload;
        $this->total_uploaded            = $consumer->total_data_upload;
        $this->queue_upload              = $consumer->queue_data_upload;
        
        $this->is_download               = $consumer->download_consumer=="1";
        $this->max_download              = $consumer->download_max_data;
        $this->filter_download           = $consumer->filter_origin_download;
        $this->total_downloaded          = $consumer->total_data_download;
        $this->queue_download            = $consumer->queue_data_download;
        $this->synch_model               = $consumer->synch_model;
        $this->log                       = "";
        $this->current_autonomous_name   = $current_autonomous_name;
        
        $this->result_total_upload       = -1;
        $this->result_total_download     = -1;
        $this->result_success_upload     = -1;
        $this->result_success_download   = -1;
        $this->result_remaining_upload   = -1;
        $this->result_remaining_download = -1;
        
        parent::__construct($db,"synchronize_provider",NULL,"smis-administrator",$consumer->autonomous,$user);
    }
    
    public function initialize(){
        $this->initDBTable($this->db);
        $this->fetch();
        
        /*upload process*/
        $response=$this->uploadProcess();
        $this->response=$response;
        $this->postUpload($response);
        
        /*download process*/
        $succes_data=$this->downloadProcess($response);
        $succes_count=count($succes_data);
        $this->postDownload($succes_count,$response);
        
        /*confirm download*/
        $this->confirmDownload($succes_data,$succes_count);
    }
    
    /**
     * @brief only notify the that this model had been fetch
     * @return  null
     */
    protected function fetch(){
        $dt=date("Y-m-d H:i:s");
        $update['last_fetch']=$dt;
        $id['id']=$this->sync_id;
        $result=$this->consumer_dbtable->update($update,$id);
        if($result===false){
            $this->log("Fetch",$this->consumer_dbtable->getLastError());
        }
    }
    
    protected function confirmDownload($data,$count){
        if($this->is_download && $count>0){
            $this->addData("smis-synch-consumer-upload-data",array());          //data that will sent to synch service provider
            $this->addData("smis-synch-consumer-upload-queue",0);   
            $this->addData("smis-synch-consumer-command","download");           //for synch service provider
            $this->addData("smis-synch-data-success",$data);          //data that will sent to synch service provider
            $this->addData("smis-synch-total-success",$count);
            $this->execute();
        }
    }
    
    /**
     * @brief process after download done
     * @param int $succes_count 
     * @param array $response is the data from provider
     * @return  null
     */
    protected function postDownload($succes_count,$response){          
        $provider_update['queue_data_download'] = $this->getQueueDownload($response)-$succes_count;
        $provider_update['total_data_download'] = "total_data_download+".$succes_count;            
        $warp_provider['total_data_download']   = DBController::$NO_STRIP_ESCAPE_WRAP;            
        $id_provider['id']                      = $this->sync_id;
        $result=$this->consumer_dbtable->update($provider_update,$id_provider,$warp_provider);
        if($result===false){
            $this->log("Post Download",$this->consumer_dbtable->getLastError());
        }
        $this->result_success_download          = $succes_count;
        $this->result_remaining_download        = $provider_update['queue_data_download'];
        
    }
    
    /**
     * @brief download process from provider
     * @param Array $response 
     * @return Array of success stored data that downloaded 
     */
    protected function downloadProcess(Array $response){
        $success_array=array();
        if($this->is_download){
           $this->download_data=$this->getDownloadData($response);
           $this->result_total_download = count($this->download_data);            
           $maximum_download=$this->max_download;
           foreach($this->download_data as $data){
                $kriteria['origin_id']=0;
                $kriteria['origin']="";
                
                if(strpos($data['autonomous'],"[".$this->current_autonomous_name."]")===false){
                    /* adding this autonomous name in database so it will 
                     * bind that this data have synch to this autonomous.
                     * */
                    $data['autonomous'].="[".$this->current_autonomous_name."]";
                }
                
                if(strpos($data['autonomous'],"[".$this->autonomous."]")===false){
                    /**
                     * if there is no autonomous that sent this data in the $data['autonomous']
                     * added by it self to prevent synch loop back to the sender
                     * */                        
                    $data['autonomous'].="[".$this->autonomous."]";
                }
                
                if($this->synch_model=="multiple"){
                    /* if using the multiple model the 
                     * id should not be used
                     * instead using origin_id and origin as the primary key
                     * */
                   unset($data['id']);
                   $kriteria['origin_id']=$this->getData("origin_id",$data,0);
                   $kriteria['origin']=$this->getData("origin",$data, "");
                }

                if($kriteria['origin_id']==0 || $kriteria['origin']==""){
                    /* if the origin id and the origin is not set, 
                     * then simply used the id */
                    unset($kriteria['origin_id']);
                    unset($kriteria['origin']);
                    $kriteria['id']=$this->getData("id",$data, 0);
                }
                    
                if($this->filter_upload!="" && ( !isset($kriteria['origin']) || $kriteria['origin']!=$this->filter_upload ) ){
                    /**
                    * if filter upload activated but this one have no origin 
                    * or the origin not same as the filter upload
                    * the discard, but say already synch
                    * */
                    
                    $result=array();
                    $result['id']=$kriteria;
                    $result['autonomous']=$data['autonomous'];                
                    $success_array[]=$result;
                }else{
                    $the_id=0;
                    $result=false;
                    if(!$this->dbtable->is_exist($kriteria,true)){
                        $result=$this->dbtable->insert($data);
                        $the_id=$this->dbtable->get_inserted_id();
                    }else {
                        $old=$this->dbtable->select($kriteria,false,false);
                        $the_id=$current->id;
                        if($old->time_updated<$data['time_updated']){
                            /**
                             * if the incoming data is more new than the current data
                             * then update, and say that already synch */
                            
                           $result=$this->dbtable->update($data,$kriteria); 
                        }else{
                            /**
                             * if the incoming data older then the current
                             * simply discard , and say already synch */
                            $result=true;
                        }
                    }
                   
                    if($result===false){
                        $this->log("Download Process",$this->dbtable->getLastError());
                    }else{
                        /* this process is success then say success */
                        $result=array();
                        $result['id']=$kriteria;
                        $result['autonomous']=$data['autonomous'];                
                        $success_array[]=$result;
                    }
                    
                    //limited the process so not taking so long
                    if($maximum_download<=0){
                       break;
                    }
                    $maximum_download--;
                }
                
           }
        }
        $this->success_download_data=$success_array;
        return $success_array;
    }
    
    /**
     * @brief get data from array, is empty then give default
     * @param string $name 
     * @param Array $data 
     * @param mixed $default 
     * @return  mixed value
     */
    protected function getData($name,$data,$default){
		if(isset($data[$name]))
			return $data[$name];
		return $default;
	}
    
    
    /**
     * @brief get total data that downloaded from the provider
     * @return  total
     */
    protected function getQueueDownload(Array $response){
        if(isset($response['smis-synch-provider-download-queue'])){
            return $response['smis-synch-provider-download-queue'];
        }
        return 0;
    }
    
    /**
     * @brief get all current data that need to be downloaded from the provider
     * @return  Array
     */
    protected function getDownloadData(Array $response){
        if(isset($response['smis-synch-provider-download-data'])){
            return $response['smis-synch-provider-download-data'];
        }
        return array();
    }
    
    
    /**
     * @brief process to uploading data to provider
     * @return  
     */
    protected function uploadProcess(){
        $this->addData("smis-synch-consumer-code-name",$this->code);                //data that will sent to synch service provider
        $this->addData("smis-synch-consumer-token","1");                            //for serverbus
        $this->addData("smis-synch-consumer-command","upload");                     //for synch service provider
        $this->addData("smis-synch-consumer-download",$this->is_download?"1":"0");  //for synch service provider, should upload or not
        
        if($this->is_upload){
            $this->queue_upload=$this->getQueueUpload();
            $this->upload_data=$this->getUploadData();
            $this->result_total_upload = count($this->upload_data);
            $this->addData("smis-synch-consumer-upload-data",$this->upload_data);          //data that will sent to synch service provider
            $this->addData("smis-synch-consumer-upload-queue",$this->queue_upload);     //data that will sent to synch service provider    
        }else{
            $this->addData("smis-synch-consumer-upload-data",array());          //data that will sent to synch service provider
            $this->addData("smis-synch-consumer-upload-queue",0);               //data that will sent to synch service provider
        }
        $this->execute();
        return $this->getContent();
    }
    
    /**
     * @brief update the current database tell, that already synch
     * @param Array $response 
     * @return  null
     */
    protected function postUpload($response){
        if($response!=null && isset($response['smis-synch-total-success']) && isset($response['smis-synch-data-success']) ){
            $succes_count=$response['smis-synch-total-success'];
            $this->success_upload_data=$response['smis-synch-data-success'];
            foreach($this->success_upload_data as $x){
                $update['autonomous']="concat(autonomous,'[".$this->autonomous."]')";
                $warp['autonomous']=DBController::$NO_STRIP_ESCAPE_WRAP;
                $id=$x['id'];
                $result=$this->dbtable->update($update,$id,$warp);
                if($result===false){
                    $this->log("Post Upload",$this->dbtable->getLastError());
                }
            }
            
            $consumer_update['queue_data_upload']=$this->queue_upload-$succes_count;
            $consumer_update['total_data_upload']="total_data_upload+".$succes_count;  
            $warp_consumer['queue_data_upload']=DBController::$NO_STRIP_ESCAPE_WRAP;
            $warp_consumer['total_data_upload']=DBController::$NO_STRIP_ESCAPE_WRAP;            
            $id_consumer['id']=$this->sync_id;
            $result=$this->consumer_dbtable->update($consumer_update,$id_consumer,$warp_consumer);
            if($result===false){
                $this->log("Post Upload ",$this->consumer_dbtable->getLastError());
            }

            $this->result_success_upload   = $succes_count;
            $this->result_remaining_upload = $consumer_update['queue_data_upload'];
            
        }
    }
    
    /**
     * @brief get the total data that need to be synch
     * @return  the data total that need to be synch
     */
    protected function getQueueUpload(){
        if($this->filter_upload!=""){
            $this->dbtable->addCustomKriteria(" origin ","='".$this->filter_upload."'");
        }
        $this->dbtable->addCustomKriteria(" autonomous "," NOT LIKE '%[".$this->autonomous."]%'");
        $total=$this->dbtable->count("");
        return $total;
    }
    
    /**
     * @brief get several data that need to be synch
     * @return  Array of data that not yet synch
     */
    protected function getUploadData(){
        $this->dbtable->setMaximum($this->max_upload);
        $data=$this->dbtable->view("","0");
        $list=$data['data'];
        return $list;
    }
    
    /**
     * @brief init the table of the database
     * @param Database $db 
     * @return  null
     */
    protected function initDBTable($db){
        $this->dbtable=new DBTable($db,$this->table_name);
        $this->dbtable->setRealDelete(true);
        $this->dbtable->setDelete(true);        
        $this->consumer_dbtable=new DBTable($db,"smis_adm_synch_consumer");
    }
    
    protected function log($title,$errorlog){
        $this->log.="###Consumer Error In <strong>".$title."</strong>###";
        $this->log.="<pre>";
        $this->log.=$errorlog;
        $this->log.="</pre>";
    }
    
    /**
     * @brief get the data log of the query
     * @return  
     */
    public function getLog(){
        return $this->log;
    }

    /**
     * @brief get the data log of the query
     * @return  
     */
    public function clearLog(){
        $this->log = "";
        return $this;
    }
    
    /**
     * @brief get the data that downloaded from the provider
     * @return  Array downlaoded data
     */
    public function getDownloadedData(){
        return $this->download_data;
    }
    
    /**
     * @brief get uploaded data to the provider
     * @return  array uploaded data
     */
    public function getUploadedData(){
        return $this->upload_data;
    }
    
    /**
     * @brief get the downloaded data that successfully synch
     *          with the provider
     * @return  array downloaded data 
     */
    public function getSuccessDownloadedData(){
        return $this->success_download_data;
    }
    
    /**
     * @brief get uploaded data that successfully synch to the provider
     * @return  array uploaded data
     */
    public function getSuccessUploadedData(){
        return $this->success_upload_data;
    }

    public function getProsesResume(){
        $resume  = "U/S-R : ".$this->result_total_upload."/".$this->result_success_upload." - ".$this->result_remaining_upload."\n";
        $resume .= "D/S-R : ".$this->result_total_download."/".$this->result_success_download." - ".$this->result_remaining_download."\n";
        return $resume;
    }
    
    /**
     * @brief get response to cek the data
     * @return  array response process
     */
    public function getResponseData(){
        return $this->response;
    }
}

?>