<?php 

class SynchronizeSettingsConsumer extends SynchronizeServiceConsumer{
    
    /**
     * @brief get the total data that need to be synch
     * @return  the data total that need to be synch
     */
    protected function getQueueUpload(){
        if($this->filter_upload!=""){
            $this->dbtable->addCustomKriteria(" origin ","='".$this->filter_upload."'");
        }
        $this->dbtable->addCustomKriteria(" duplicate "," =1 ");
        $this->dbtable->addCustomKriteria(" autonomous "," NOT LIKE '%[".$this->autonomous."]%' ");
        $this->dbtable->addCustomKriteria(" autonomous_up "," LIKE '%[".$this->autonomous."]%' ");
        $total=$this->dbtable->count("");
        return $total;
    }
    
    /**
     * @brief download process from provider
     * @param Array $response 
     * @return Array of success stored data that downloaded 
     */
    protected function downloadProcess(Array $response){
        $success_array=array();
        if($this->is_download){
           $this->download_data=$this->getDownloadData($response);
           $maximum_download=$this->max_download;
           foreach($this->download_data as $data){
                
                if(strpos($data['autonomous'],"[".$this->current_autonomous_name."]")===false){
                    /* adding this autonomous name in database so it will 
                     * bind that this data have synch to this autonomous.
                     * */
                    $data['autonomous'].="[".$this->current_autonomous_name."]";
                }
                
                if(strpos($data['autonomous'],"[".$this->autonomous."]")===false){
                    /**
                     * if there is no autonomous that sent this data in the $data['autonomous']
                     * added by it self to prevent synch loop back to the sender
                     * */                        
                    $data['autonomous'].="[".$this->autonomous."]";
                }
                
                
                $kriteria['name']=$data['name'];
        
                $the_id=0;
                $result=false;
                
               unset($data['id']);
               unset($data['autonomous_up']);
               unset($data['autonomous_down']);
               unset($data['duplicate']);
                
                if($this->dbtable->is_exist($kriteria,$this->is_exist_include_del)){
                    $old=$this->dbtable->select($kriteria);
                    $the_id=$current->id;
                    if($old->time_updated<$data['time_updated'] && strpos($old->autonomous_down,"[".$this->autonomous."]")!==false ){
                        /**
                         * if the incoming data is more new than the current data
                         * then update, and say that already synch , 
                         * in this case update all except the id, autonomous_up, autonomous_down, duplicate */
                       $result=$this->dbtable->update($data,$kriteria); 
                    }else{
                        /**
                         * if the incoming data older then the current
                         * simply discard , and say already synch */
                        $result=true;
                    }
                    
                    if($result===false){
                        $this->log("Download Process",$this->dbtable->getLastError());
                    }
                }else{
                    /*it's discard*/
                    $result=true;
                }
               
                
                
                //limited the process so not taking so long
                if($maximum_download<=0){
                   break;
                }
                $maximum_download--;
                
                if($result!==false){
                    $result=array();
                    $result['id']=$kriteria;
                    $result['autonomous']=$data['autonomous'];                
                    $success_array[]=$result;
                }
           }
        }
        $this->success_download_data=$success_array;
        return $success_array;
    }
    
}

?>