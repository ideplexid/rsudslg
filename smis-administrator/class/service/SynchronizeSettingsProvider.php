<?php 

class SynchronizeSettingsProvider extends SynchronizeServiceProvider{
   
    /**
     * @brief get the total data that need to be downloaded bay the consumer
     * @return  int total of the data
     */
    protected function getQueueDownload(){
        if($this->filter_download!=""){
            $this->dbtable->addCustomKriteria(" origin ","='".$this->filter_download."'");
        }
        $this->dbtable->addCustomKriteria(" duplicate "," =1 ");
        $this->dbtable->addCustomKriteria(" autonomous "," NOT LIKE '%[".$this->autonomous."]%' ");
        $this->dbtable->addCustomKriteria(" autonomous_down "," LIKE '%[".$this->autonomous."]%' ");
        $total=$this->dbtable->count("");
        return $total;
    }
    
    /**
     * @brief process store data in the upload data
     * @return  Array of id that successfully stored/confirm/checked
     */
    protected function uploadProcess(){
        $success_array=array();
        if($this->is_upload){
           $list=$this->getUploadData();
           $maximum_upload=$this->max_upload;
           foreach($list as $data){
                
                if(strpos($data['autonomous'],"[".$this->current_autonomous_name."]")===false){
                    /* adding this autonomous name in database so it will 
                     * bind that this data have synch to thisd autonomous
                     * */
                    $data['autonomous'].="[".$this->current_autonomous_name."]";    
                }
                
                if(strpos($data['autonomous'],"[".$this->autonomous."]")===false){
                    /**
                     * if there is no autonomous that sent this data in the $data['autonomous']
                     * added by it self to prevent synch loop back to the sender
                     * */                        
                    $data['autonomous'].="[".$this->autonomous."]";
                }
                
                $kriteria['name']=$data['name'];
                
                unset($data['id']);
                unset($data['autonomous_up']);
                unset($data['autonomous_down']);
                unset($data['duplicate']);
                
                $the_id=0;
                $result=false;
                if($this->dbtable->is_exist($kriteria,$this->is_exist_include_del)  ){
                    $old=$this->dbtable->select($kriteria);
                    $the_id=$current->id;
                    if($old->time_updated<$data['time_updated'] && strpos($old->autonomous_up,"[".$this->autonomous."]")!==false){
                        /**
                         * if the incoming data is more new than the current data
                         * then update, and say that already synch */
                        
                       $result=$this->dbtable->update($data,$kriteria); 
                    }else{
                        /**
                         * if the incoming data older then the current
                         * simply discard , and say already synch */
                        $result=true;
                    }
                    
                    if($result===false){
                        $this->log("Upload Process",$this->dbtable->getLastError());
                    }
                }else{
                    $result=true;
                }
                
                
               
                //limited the process so not taking so long
                if($maximum_upload<=0){
                   break;
                }
                $maximum_upload--;
                
                if($result!==false){
                    $result=array();
                    $result['id']=$kriteria;
                    $result['autonomous']=$data['autonomous'];                
                    $success_array[]=$result;
                }
           }
        }
        return $success_array;
    }
    
}

?>