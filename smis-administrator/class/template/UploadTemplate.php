<?php 

class UploadTemplate extends MasterTemplate{
		public function jsPreload(){
		?>
				<script type="text/javascript">
				var <?php echo $this->action; ?>;
				$(document).ready(function(){
					$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
					var column=new Array(<?php echo $this->getJSColumn(); ?>); 
					<?php echo $this->action; ?>=new TableAction("<?php echo $this->action; ?>","<?php echo $this->page; ?>","<?php echo $this->action; ?>",column);
					<?php if($this->autoreload) echo $this->action.".view();"; ?>
					<?php $this->onReady();?>
					<?php if($this->is_truncate){
						echo $this->action;?>.tuncate_db=function(){
							var data=this.getRegulerData();
							data['command']="tuncate_db";
							$.post("",data,function(res){
								<?php echo $this->action; ?>.view();
							});
						}
						
						<?php 
					}
					?>
					<?php  echo $this->action;?>.remove_file=function(id){
						var self=this;
						$.post("",{
							page:"smis-base",
							action:"smis-remove-file",
							id:id
						},function(res){
							var json=$.parseJSON(res);
							if(json.status=="1"){
								self.view();
							}else{
								showWarning("Error",json.msg);
							}
						});
					}

					<?php  echo $this->action;?>.download_file=function(id){
						showLoading();	
						var edit_data=this.getEditData(id);
						$.post('',edit_data,function(res){		
							var json=getContent(res);
							var filename=json.linkfile;
							dismissLoading();
							download_file(filename);
						});
					}
				});
				</script>
				
				<?php
				
				$this->getSuperCommandJavascript();			
		}
}

?>