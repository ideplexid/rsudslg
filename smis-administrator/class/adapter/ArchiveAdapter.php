<?php 

class ArchiveAdapter extends ArrayAdapter{

	public function adapt($d){
		$array=array();
		$unzip=str_replace(".", "_", $d);
		$rwti=substr($d, 0,22);
		$rwt=str_replace("___", " ", $rwti);
		$rwt=str_replace("__", ":", $rwt);
		$rwt=str_replace("_", "-", $rwt);
		$array['Waktu']=self::format("date d M Y H:i:s", $rwt);
		$array['Type']=strpos($d,'.sql') !== false?"Single":"Multiple";
		$array['Ukuran']="<font id='".$unzip."'>".self::format("number", filesize("smis-backup/".$d))."</font>";
		$array['id']=$d;
		return $array;
	}

}


?>