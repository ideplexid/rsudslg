<?php 

class LogManajerAdapter extends FileAdapter{
	
	private function correctName($filename){
		if(endsWith($filename,".json")){
			return substr($filename,0,19);
		}
		return $filename;
	}
	
	public function getLink($filename,$folder,$hidden,$istext){
		$icon=$this->getIcon($filename, $folder, $hidden, $istext);
		$hasil=array();
		$gname=$this->correctName($filename);
		if($folder=="directory"){
			$hasil['icon']="<a href='javascript:;' onclick=\"".$this->action.".directory('".$filename."')\" >".$icon."</a>";
			$hasil['name']="<a href='javascript:;' onclick=\"".$this->action.".directory('".$filename."')\" >".$gname."</a>";
		}else if($icon=="<i class='fa fa-file-image-o '></i>"){
			$hasil['icon']="<a href='javascript:;' onclick=\"".$this->action.".showimage('".$filename."')\" >".$icon."</a>";
			$hasil['name']="<a href='javascript:;' onclick=\"".$this->action.".showimage('".$filename."')\" >".$gname."</a>";
		}else if($icon=="<i class='fa fa-file-sound-o '></i>"){
			$hasil['icon']="<a href='javascript:;' onclick=\"".$this->action.".showaudio('".$filename."')\" >".$icon."</a>";
			$hasil['name']="<a href='javascript:;' onclick=\"".$this->action.".showaudio('".$filename."')\" >".$gname."</a>";
		}else{
			$hasil['icon']="<a href='javascript:;' onclick=\"".$this->action.".openfile('".$filename."')\" >".$icon."</a>";
			$hasil['name']="<a href='javascript:;' onclick=\"".$this->action.".openfile('".$filename."')\" >".$gname."</a>";
		}
		return $hasil;
	}
}

?>