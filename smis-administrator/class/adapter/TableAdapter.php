<?php 


class TableAdapter extends ArrayAdapter {
	public function adapt($onerow) {
		$row = get_object_vars ( $onerow );
		$array = array ();
		foreach ( $row as $name => $data ) {
			$array ['name'] = $data;
			$array ['value'] = $array ['name'];
			if ($data == 'smis_adm_log')
				return null;
			return $array;
		}
	}
	
	public function getContent($data) {
		$content = array ();
		$number = 0;
		foreach ( $data as $d ) {
			$adapt = $this->adapt ( $d );
			if ($adapt == null)
				continue;
			$content [$number] = $adapt;
			$number ++;
		}
		return $content;
	}
	
	public function reloadContent($content){
		$nomor=1;
		$hasil=array();
		global $db;
		$dbtable=new DBTable($db, "",array());
		
		foreach($content as $name){
			$dt=array();
			$dt['Nama']=self::format("strtoupper", $name);
			$dbtable->setName($name);
			$total=$dbtable->getNextID()-1;
			$dt['Total']=$total;
			$dt['Nomor']=$nomor++;
			$hasil[]=$dt;				
		}
		return $hasil;
	}
}

?>