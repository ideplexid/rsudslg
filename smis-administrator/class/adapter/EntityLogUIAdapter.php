<?php 

class EntityLogUIAdapter extends ArrayAdapter {
	
		public function adapt($onerow) {
			$array = array ();
			$array ['id'] = $onerow->id;
			$array ['Path'] = $onerow->path;
			$array ['Code'] = $onerow->code;
			$array ['Waktu'] = self::format ( 'date d-M-Y, H:i:s', $onerow->waktu );
			
			$post = $onerow->post;
			$get = $onerow->get;
			$response = str_replace ( "<", "&lt;", $onerow->response );
			
			$post_freetify=json_encode(json_decode($onerow->post,true),JSON_PRETTY_PRINT);
			$get_freetify=json_encode(json_decode($onerow->get,true),JSON_PRETTY_PRINT);
			$response_freetify=decodeUntilArray($onerow->response);
				

			$raw = "
			<div class='label label-primary'>POST</div>
			<div contenteditable='true'>$post</div></br>
			<div class='label label-info'>GET</div>
			<div contenteditable='true'>$get</div></br>
			<div class='label label-warning'>RESPONSE</div>
			<div contenteditable='true'>$response</div></br>
			";
				
			$fetreefy = "
			<div class='label label-primary'>POST</div>
			<pre>$post_freetify</pre></br>
			<div class='label label-info'>GET</div>
			<pre>$get_freetify</pre></br>
			<div class='label label-warning'>RESPONSE</div>
			<pre>$response_freetify</pre></br>
			";
			
			$tabulator=new Tabulator("", "");
			$tabulator->add("raw_servicelog", "Raw", $raw,Tabulator::$TYPE_HTML,"fa fa-file-text");
			$tabulator->add("fetreefy_servicelog", "Fetreefy", $fetreefy,Tabulator::$TYPE_HTML,"fa fa-female");
			$array['tabs']=$tabulator->getHtml();
			
			return $array;
		}
	}

?>