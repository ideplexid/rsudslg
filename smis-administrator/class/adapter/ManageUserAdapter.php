<?php 

class ManageUserAdapter extends ArrayAdapter {
	public function adapt($onerow) {
		$array = array ();
		$array ['id'] = $onerow->id;
		$array['Username'] = $onerow->username;
		$array ['Name'] =  $onerow->realname;
		$array ['Email'] = $onerow->email;
		$array ['Authority'] = $onerow->authority;
		$array ['Last Change'] = self::format ( "date d-M-Y", $onerow->last_change );
		$array  ['Last IP'] = $onerow->last_login_ip;
		$array  ['Last Time'] = self::format ( "date d-M-Y", $onerow->last_login_time );
		$array ['Active'] = $onerow->is_active == "active" ? "<font class='label label-info'><i class='icon-white icon-ok'></i></font>" : "";
		$foto = "";
		if ($onerow->foto != "") {
			$img = new Img ( "", '', "smis-upload/" . $onerow->foto );
			$img->setClass ( 'foto-user' );
			$foto = $img->getHtml ();
		}
		$array ['Foto'] = $foto;
		return $array;
	}
}

?>