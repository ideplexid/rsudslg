<?php 

class RequestLogUIAdapter extends ArrayAdapter {
	public function adapt($onerow) {
		$array = array ();
		$array ['id'] = $onerow->id;
		$request = json_decode ( $onerow->request );
		$array ['User'] = $request->user;
		$array ['IP'] = $request->ip;
		$array ['Code'] = $request->code;
		$array ['Waktu'] = self::format ( 'date d-M-Y, H:i:s', $onerow->waktu );
		
		$request = $onerow->request;
		$post = $onerow->post;
		$response = str_replace ( "<", "&lt;",$onerow->response);
		$raw= "
				<div id='modal_requestlog_" . $onerow->id . "' >
							<div class='label label-primary'>POST</div>
							<div contenteditable='true'>$post</div></br>
							<div class='label label-important'>REQUEST</div>
							<div contenteditable='true'>$request</div></br>
							<div class='label label-warning'>RESPONSE</div>
							<div contenteditable='true'>$response</div></br>
							</div>
							";
		
		$request = json_decode($onerow->request,true);
		$post = json_decode($onerow->post,true);
		$response = json_decode($onerow->response,true);			
		$request = json_encode($request,JSON_PRETTY_PRINT);
		$post = json_encode($post,JSON_PRETTY_PRINT);
		$response = str_replace ( "<", "&lt;",json_encode($response,JSON_PRETTY_PRINT) );
			
		
		
		$fetreery= "
				<div id='modal_requestlog_" . $onerow->id . "' >
					<div class='label label-primary'>POST</div>
					<pre>$post</pre></br>
					<div class='label label-important'>REQUEST</div>
					<pre>$request</pre></br>
					<div class='label label-warning'>RESPONSE</div>
					<pre>$response</pre></br>			
				</div>
		";
		
		$tabulator=new Tabulator("", "");
		$tabulator->add("raw_requestlog", "Raw", $raw,Tabulator::$TYPE_HTML,"fa fa-file-text");
		$tabulator->add("fetreefy_requestlog", "Fetreefy", $fetreery,Tabulator::$TYPE_HTML,"fa fa-female");
		$array['tabs']=$tabulator->getHtml();
		
		return $array;
	}
}

?>