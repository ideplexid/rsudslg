<?php 

class SystemLogUIAdapter extends ArrayAdapter {
	public function adapt($onerow) {
		$array 				 = array ();
		$array['id']		 = $onerow->id;
		$array['User']		 = "<small>".$onerow->user."</small>";
		$array['IP']		 = "<small>".$onerow->ip."</small>";
		$array['Time']		 = "<small>".self::format('date d-M-Y,H:i:s',$onerow->waktu)."</small>";
		$array['RTM']		 = round($onerow->time_usage,2)." s - ".$this->convert($onerow->memory_usage);
		$pack 				 = json_decode($onerow->post,true);
		$p['Page']			 = isset($pack['page'])?$pack['page']:"refresh";
		$p['Action']		 = isset($pack['action'])?$pack['action']:"refresh";
		$p['Command']		 = isset($pack['command'])?$pack['command']:"refresh";
		$array['Page']		 = "<div class='label label-inverse'>".$p['Page']. "</div>";
		$array['Action']	 = "<div class='label'>".$p['Action']. "</div>";
		$array['Command']	 = "<div class='label label-warning'>".$p['Command']. "</div>";
		
		$array_service 		 = json_decode($onerow->service,true);
		$post 				 = self::format("json",$onerow->post);
		$service 			 = self::format("array-val",$array_service);
		$query				 = self::format("json-val",$onerow->query);
		$error_query 		 = self::format("json-val",$onerow->error_query);
		$error_php 			 = self::format("json-val",$onerow->error_php);
		$error_log 			 = self::format("json-val",$onerow->error_log);
		$array['Error']		 = $error_query == ""?"" :" <div class='label label-warning'>Query</div> ";
		$array['Error']		.= $error_php == ""?"" :" <div class='label label-important'>PHP</div> ";
		$array['Service']	 = $service == ""?"" :" <div class='badge badge-inverse'>".count($array_service)."</div> ";
		
		$array['Error']		.= "<div class='hide' ><div id='modal_systemlog_".$onerow->id."' >
									<div class='label'>LOG</div>
									<div>$error_log</div></br></br>
									<div class='label label-warning'>ERROR QUERY</div>
									<div>$error_query</div></br></br>
									<div class='label label-important'>ERROR PHP</div>
									<div>$error_php</div></br></br>
									<div class='label label-info'>POST</div>
									<div>$post</div></br></br>
									<div class='label label-success'>QUERY</div>
									<div>$query</div></br></br>
									<div class='label label-inverse'>SERVICE</div>
									<div>$service</div>						
								</div></div>";
		
		return $array;
	}
    
    function convert($size){
        $unit=array('B','KB','MB','GB','TB','PB');
        return round(@round($size/pow(1024,($i=floor(log($size,1024)))),2)/8,2).' '.$unit[$i];
    }

}

?>