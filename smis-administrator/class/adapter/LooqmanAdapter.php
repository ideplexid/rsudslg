<?php 

class LooqmanAdapter extends LogManajerAdapter{
	
	public function adapt($file){
		$hasil=parent::adapt($file);		
		if(endsWith($file['name'],".json")){
			$command=str_replace(".json","",$file['name']);
			$command=substr($command,20);
			
			if(strpos($command,"_P_")!==false){
				$hasil['PHP']='<div class="label label-important">PHP</div>';
				$command=str_replace("_P_","",$command);
			}
			
			if(strpos($command,"_S_")!==false){
				$hasil['Service']='<div class="label label-inverse">Service</div>';
				$command=str_replace("_S_","",$command);
			}
			
			if(strpos($command,"_Q_")!==false){
				$hasil['Query']='<div class="label label-warning">Query</div>';
				$command=str_replace("_Q_","",$command);
			}
			if(strpos($command,"_M_")!==false){
				$hasil['Message']='<div class="label">Message</div>';
				$command=str_replace("_M_","",$command);
			}
			if($command=="") $hasil['Command']="REFRESH";
			else $hasil['Command']=strtoupper($command); 
						
		}
		return $hasil;
	}
	
	
	
}

?>