<?php 

class ResponseLogUIAdapter extends ArrayAdapter {
	public function adapt($onerow) {
		$array = array ();
		$array ['id'] = $onerow->id;
		$request = json_decode ( $onerow->post );
		$array ['User'] = $request->user;
		$array ['IP'] = $request->ip;
		$array ['Code'] = $request->code;
		$array ['Waktu'] = self::format ( 'date d-M-Y, H:i:s', $onerow->waktu );
		
		$erponse= $onerow->erponse;
		$request = $onerow->request;
		$post = $onerow->post;
		$response = str_replace ( "<", "&lt;", $onerow->response );
		$request_freetify=json_encode(json_decode($onerow->request,true),JSON_PRETTY_PRINT);
		$post_freetify=json_encode(json_decode($onerow->post,true),JSON_PRETTY_PRINT);
		$response_freetify=str_replace ( "<", "&lt;",json_encode(json_decode($onerow->response,true),JSON_PRETTY_PRINT));
			
		$path = self::format ( 'json', $onerow->path );
		
		$path="<div class='label label-success'>PATH</div>
					<div>$path</div></br>";
		
		$raw = "
					<div class='label label-primary'>POST</div>
					<div contenteditable='true'>$post</div></br>
					<div class='label label-important'>REQUEST</div>
					<div contenteditable='true'>$request</div></br>
					<div class='label label-warning'>RESPONSE</div>
					<div contenteditable='true'>$response</div></br>
				";
		
		$fetreefy = "
					<div class='label label-primary'>POST</div>
					<pre>$post_freetify</pre></br>
					<div class='label label-important'>REQUEST</div>
					<pre>$request_freetify</pre></br>
					<div class='label label-warning'>RESPONSE</div>
					<pre>$response_freetify</pre></br>
					";
		
		/*erponse*/
		$_ERPONSE=json_decode($erponse,true);
		$_ERPONSE_RESULT="";
		foreach($_ERPONSE as $E=>$V){
			$_ERPONSE_RESULT.="<div class='label'>".$E."</div>";
			$_ERPONSE_RESULT.="<pre>".$V."</pre></br>";
		}
		
		$tabulator=new Tabulator("", "");
		$tabulator->add("raw_path", "Path", $path,Tabulator::$TYPE_HTML,"fa fa-pinterest");
		$tabulator->add("raw_responselog", "Raw", $raw,Tabulator::$TYPE_HTML,"fa fa-file-text");
		$tabulator->add("fetreefy_responselog", "Fetreefy", $fetreefy,Tabulator::$TYPE_HTML,"fa fa-female");
		$tabulator->add("erponse_responselog", "Error Response", $_ERPONSE_RESULT,Tabulator::$TYPE_HTML,"fa fa-houzz");
		$array['tabs']=$tabulator->getHtml();
		
		return $array;
	}
}

?>