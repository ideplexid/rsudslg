<?php 

class MenuAdapter extends ArrayAdapter {
	private $setting;
	private $on_all;
	private $of_all;
	private $btgrup;
	private $authority;
	public function __construct($usersetting,$authority) {
		parent::__construct ();
		$this->authority = $authority;
		$this->setting 	 = $usersetting;
		$this->on_all	 = new Button("","","On All");
		$this->on_all	 ->setClass("btn-primary on-all");
		$this->on_all	 ->setIcon("fa fa-toggle-on");
		$this->on_all	 ->setIsButton(Button::$ICONIC_TEXT);
		$this->of_all	 = new Button("","","Off All");
		$this->of_all	 ->setClass("btn-danger off-all");
		$this->of_all	 ->setIcon("fa fa-toggle-off");
		$this->of_all	 ->setIsButton(Button::$ICONIC_TEXT);
		$this->btgrup	 = new ButtonGroup("","","");
		$this->btgrup	 ->addButton($this->on_all)
						 ->addButton($this->of_all);
	}

	public function getUserSettings($menu, $submenu) {
		return getUserAuthorization ( $this->setting, $menu, $submenu );
	}

	public function getContent($data) {
		$content 	= array ();
		foreach ( $data as $d ) {
			$adapt 	= $this->adapt ( $d );
			if ($adapt != null) {
				$number 			= count ( $content );
				$content [$number] 	= $adapt;
			}
		}
		return $content;
	}

	public function adapt($menu) {
		$menu_name 			= $menu->getName ();
		$menus_component 	= $menu->getComponent ();
		$component 			= array ();
		$first				= true;
		foreach ( $menus_component as $slug => $submenu ) {
			$menu_key 		= $submenu->getPage ();
			if($first){
				$this->of_all		->setAction("authorization.ofall('".$menu_key."')");
				$this->on_all		->setAction("authorization.onall('".$menu_key."')");
				$component[]		= $this->btgrup->getHTML();
				$first				= false;
			}
			if ($menu_key == "smis-administrator" || $menu_name == null || $menu_name == "" || ($menu_key=="operator" && $this->authority=="user") ){
				return null;
			}
			$submenu_desc 	= $submenu->getDescription ();
			$submenu_name 	= $submenu->getName ();
			$submenu_key 	= $submenu->getAction ();
			if ($submenu_name == "" || $submenu_name == null)
				continue;
			$value 			= $this->getUserSettings ( $menu_key, $submenu_key );
			$check 			= new CheckBox ( $menu_key."_".$submenu_key, $submenu_name, $value );
			$check			->setAtribute ( "menu='" . $menu_key . "' submenu='" . $submenu_key . "' " );
			$component[] 	= "<div class='clear'>" . $check->getHtml () . " <i class='i_desc'>(" . $submenu_desc . ")</i></div>";
		}
		$plugins 				= array ();
		$plugins ['Name'] 		= $menu_name;
		$plugins ['Component'] 	= $component;
		return $plugins;
	}
}

?>