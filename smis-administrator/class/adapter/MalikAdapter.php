<?php 

class MalikAdapter extends LogManajerAdapter{
	
	public function adapt($file){
		$hasil=parent::adapt($file);
		$fname=substr($file['name'],19);
		$fname=str_replace(".json","",$fname);
		$this->getType($fname,$hasil);
		
		return $hasil;
	}
	
	private function entityLog(&$filename,&$hasil){
		if(strpos($filename,"_P_")!==false){
			$hasil['PHP']='<div class="label label-important">PHP</div>';
			$filename=str_replace("_P_","",$filename);
		}
		
		if(strpos($filename,"_S_")!==false){
			$hasil['Service']='<div class="label label-inverse">Service</div>';
			$filename=str_replace("_S_","",$filename);
		}
		
		if(strpos($filename,"_Q_")!==false){
			$hasil['Query']='<div class="label label-warning">Query</div>';
			$filename=str_replace("_Q_","",$filename);
		}
		if(strpos($filename,"_M_")!==false){
			$hasil['Message']='<div class="label">Message</div>';
			$filename=str_replace("_M_","",$filename);
		}
		return $filename;
	}
	
	private function getType($filename,&$hasil){
		
		if($x=strpos($filename,"service_request")!==false){
			$hasil['Data Type']="<div class='label'>Request</div>";
			$hasil['Autonomous']=strtoupper(str_replace("_service_request_","",$filename));
		}else if(strpos($filename,"serverbus_log")!==false){
			$hasil['Data Type']="<div class='label label-inverse'>Server Bus</div>";
			$hasil['Autonomous']=strtoupper(str_replace("_serverbus_log_","",$filename));
		}else if(strpos($filename,"autonomous_log")!==false){
			$hasil['Data Type']="<div class='label label-warning'>Service Caller</div>";
			$autonomous=str_replace("_autonomous_log_","",$filename);
			if(strpos($autonomous,"_erponse_")!==false){
				$hasil['Entity']='<div class="label label-info">Entity</div>';
				$autonomous=str_replace("_erponse_","",$autonomous);
			}
			$hasil['Autonomous']=strtoupper($autonomous);
		}else if(strpos($filename,"entity_log")!==false){
			$filename=str_replace("_entity_log_","",$filename);
			$hasil['Data Type']="<div class='label label-important'>Response</div>";
			$pos=strpos($filename,"_entity_");
			$autonomous=substr($filename,0,$pos);
			$entity=substr($filename,$pos+8);
			$hasil['Autonomous']=strtoupper($autonomous);
			$hasil['Entity']=strtoupper(substr($this->entityLog($entity,$hasil),0,-1));
		}
		return $hasil;
	}
	
	
	
}

?>