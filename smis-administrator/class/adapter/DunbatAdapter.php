<?php 

class DunbatAdapter extends LogManajerAdapter{
	protected $dbname;
	public function __construct($filename,$database){
		parent::__construct($filename);
		$this->dbname=$database;
	}
	
	public function adapt($file){
		$hasil				= parent::adapt($file);
		$fname				= substr($file['name'],19);							//d-m-Y H:i:s is 19
		$fname				= str_replace(".json","",$fname);		
		$pos_db				= strpos($fname,$this->dbname);
		$fname				= substr($fname,$pos_db+strlen($this->dbname)+1);	//string _ is 1
		$pos_usr			= strpos($fname,"_usr_");
		$hasil['Process']	= $this->beautifyProcess(substr($fname,0,$pos_usr));
		$fname				= substr($fname,$pos_usr+5);						//string _usr_ is 5
		$pos_svc			= strpos($fname,"_svc_");
		$hasil['User']		= substr($fname,0,$pos_svc);			
		$fname				= substr($fname,$pos_svc+5);						//string _svc_ is 5
		$pos_code			= strpos($fname,"_code_");
		$hasil['Service']	= substr($fname,0,$pos_code);
		$fname				= substr($fname,$pos_code+6);						//string _code_ is 6
		$hasil['Hash']		= $fname;
		return $hasil;
	}
	
	public function beautifyProcess($process){
		switch($process){
			case "update" 	: $process = '<span class="label label-success ">update</span>'; 		break;
			case "insert" 	: $process = '<span class="label label-info ">insert</span>'; 			break;
			case "delete" 	: $process = '<span class="label label-important ">delete</span>'; 		break;
			case "restore" 	: $process = '<span class="label label-warning ">restore</span>'; 		break;
			case "Settings" : $process = '<span class="label label-inverse">setting</span>'; 		break;
		}
		return $process;
	}
	
	
	public function getAction($name,$is_folder,$is_binary,$is_hidden,$permi,$owner){
		$btngroup 	= new ButtonGroup("");
		$btngroup 	->setMax(10,"");
		$ren	  	= new Button("", "", "");
		$ren	  	->setIsButton(Button::$ICONIC)
				  	->setIcon("fa fa-pencil")
				  	->setClass("btn-primary btn-small")
				  	->setAction($this->action.".editfile('".$name."','".($is_folder?"Directory":"File")."','".$permi."','".$owner."')");
		$btngroup 	->addButton($ren);
		
		$download 	= new Button("", "", "");
		$download	->setIsButton(Button::$ICONIC)
					->setIcon("fa fa-download")
					->setClass("btn-primary btn-small")
					->setAction($this->action.".download('".$name."')");
		$btngroup	->addButton($download);
		
		$del		= new Button("", "", "");
		$del		->setIsButton(Button::$ICONIC)
					->setIcon("fa fa-trash")
					->setClass("btn-primary btn-small")
					->setAction($this->action.".removefile('".$name."')");
		$btngroup	->addButton($del);
		
		$ren		= new Button("", "", "");
		$ren		->setIsButton(Button::$ICONIC)
					->setIcon("fa fa-repeat")
					->setClass("btn-success btn-small")
					->setAction($this->action.".restore('".$name."')");
		$btngroup	->addButton($ren);
		return $btngroup ->getHtml();
	}	
}
?>