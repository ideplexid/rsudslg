<?php 

class UserLogUIAdapter extends ArrayAdapter {
	public function adapt($onerow) {
		$array = array ();
		$array ['id'] = $onerow->id;
		
		if ($onerow->action == 'insert')
			$class = 'label-info';
		else if ($onerow->action == 'update')
			$class = 'label-success';
		else if ($onerow->action == 'delete')
			$class = 'label-important';
		else if ($onerow->action == 'restore')
			$class = 'label-warning';
		
		$array ['Action'] = "<span class='label $class '>" . $onerow->action . "</span>";
		$array ['User'] = $onerow->user;
		$array ['IP'] = $onerow->ip;
		$array ['DBID'] = $onerow->dbid;
		$array ['Service'] = $onerow->service;
		$array ['Code'] = $onerow->code;		
		$array ['Database'] = $onerow->db;
		$array ['Waktu'] = self::format ( 'date d-M-Y, H:i:s', $onerow->waktu );
				
		return $array;
	}
	
}

?>