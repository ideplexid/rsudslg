<?php 

class SynchronizeProviderResponder extends DBResponder{
    
    public function command($command){
        if($command=="test_table"){
            $pack=new ResponsePackage();
            $content=$this->testTable();
            $pack->setWarning(true,"Configuration Result",$content);
            $pack->setStatus(ResponsePackage::$STATUS_OK);
            return $pack->getPackage();    
        }else if($command=="repair_table"){
            $pack=new ResponsePackage();
            $content=$this->repairTable();
            $pack->setWarning(true,"Repair Result",$content);
            $pack->setStatus(ResponsePackage::$STATUS_OK);
            return $pack->getPackage();
        }else if($command=="reset"){
            $pack=new ResponsePackage();
            $content=$this->reset();
            $pack->setWarning(true,"Reset Result",$content);
            $pack->setStatus(ResponsePackage::$STATUS_OK);
            return $pack->getPackage();         
        }else{
            return parent::command($command);
        }
    }
    
    /**
     * @brief repairing table for synchronize
     *          - repair table column for synch
     *          - repair origin_id to id
     *          - repair origin to this current autonomous
     * @return  string log
     */
    private function repairTable(){
        global $wpdb;
        require_once "smis-libs-class/DBCreator.php";
        $id=$_POST['id'];
        $consumer=$this->getDBTable()->select(array("id"=>$_POST['id']));    
        $dbcreator=new DBCreator($wpdb,$consumer->table_name);
        $log=$dbcreator->repairTableSynch();
        
        $dbtable=new DBTable($this->getDBTable()->get_db(),$consumer->table_name);
        $update_origin["origin"]=getSettings($this->getDBTable()->get_db(),"smis_autonomous_id","");
        $kriteria_origin['origin']="";
        $total_origin=$dbtable->update($update_origin,$kriteria_origin,$warp_origin);
        
        $update_origin_id["origin_id"]="id";
        $warp_origin_id['origin_id']=DBController::$NO_STRIP_WRAP;
        $kriteria_origin_id['origin_id']="0";
        $total_origin_id=$dbtable->update($update_origin_id,$kriteria_origin_id,$warp_origin_id);
        
        $log.=" # TOTAL ORIGIN UPDATE : ".$total_origin."</br>";
        $log.=" # TOTAL ORIGIN ID UPDATE : ".$total_origin_id;
        return $log;
    }
    
    
    private function testTable(){
        global $wpdb;
        require_once "smis-libs-class/DBCreator.php";
        $id=$_POST['id'];
        $consumer=$this->getDBTable()->select(array("id"=>$_POST['id']));    
        $dbcreator=new DBCreator($wpdb,$consumer->table_name);
        $desc=$dbcreator->getDescription();
        
        $test_column=array();
        $test_column["id"]=0;
        $test_column["prop"]=0;
        $test_column["autonomous"]=0;
        $test_column["duplicate"]=0;
        $test_column["origin"]=0;
        $test_column["origin_id"]=0;
        $test_column["time_updated"]=0;
        $test_column["origin_updated"]=0;
        
        $table=new TablePrint();
        $table->setDefaultBootrapClass(true);
        $table->setMaxWidth(false);
        $table->addColumn("<h4>".$consumer->table_name."<h4>",10,1);
        $table->commit("title");
        $table->addColumn("<strong>COLUMN INFO<strong>",10,1);
        $table->commit("title");
        $table->addColumn("Field",1,1);
        $table->addColumn("Type",1,1);
        $table->addColumn("Null",1,1);
        $table->addColumn("Key",1,1);
        $table->addColumn("Default",1,1);
        $table->addColumn("Extra",1,1);
        $table->commit("title");
        foreach($desc as $x){
            $table->addColumn($x["field"],1,1);
            $table->addColumn($x["type"],1,1);
            $table->addColumn($x["null"],1,1);
            $table->addColumn($x["key"],1,1);
            $table->addColumn($x["default"],1,1);
            $table->addColumn($x["extra"],1,1);
            $table->commit("body");   
            
            if(isset($test_column[$x['field']])){
                $test_column[$x['field']]=1;
            }
        }
        $table->addSpace(10,1);
        $table->commit("body");
        $table->addColumn("<strong>INDEX INFO<strong>",10,1);
        $table->commit("body");
        $index=$dbcreator->getBuildDBIndexInformation();
        foreach($index as $x=>$column){
            $table->addColumn($x,1,1);
            $table->addColumn(implode(", ",$column),10,1);
            $table->commit("body");
        }
        
        $table->addSpace(10,1);
        $table->commit("body");        
        $table->addColumn("<strong>COLUMN TEST<strong>",10,1);
        $table->commit("body");
        
        foreach($test_column as $name=>$value){
            $table->addColumn($name,1,1);
            $table->addColumn($value==1?"<i class='fa fa-check'></i>":"<i class='fa fa-times' style='color:red' ></i>",10,1);
            $table->commit("body");
        }
        
        $table->addSpace(10,1);
        $table->commit("body");
        $table->addColumn("<strong>DATA TEST<strong>",10,1);
        $table->commit("body");
        
        $dbtable=new DBTable($this->getDBTable()->get_db(),$consumer->table_name);
        $total_data=$dbtable->count("");
        $dbtable->addCustomKriteria(" origin ","=''");
        $origin_kosong=$dbtable->count("");        
        $dbtable->addCustomKriteria(" origin_id ","=0");
        $origin_dan_id_kosong=$dbtable->count("");        
        $dbtable->setCustomKriteria(array());
        $dbtable->addCustomKriteria(" origin_id ","=0");
        $origin_id_kosong=$dbtable->count("");
        
        
        $table->addColumn("Total Data",1,1);
        $table->addColumn(($total_data*1)." Data",10,1);
        $table->commit("body");
        $table->addColumn("<font style='color:".($origin_dan_id_kosong>0?"red":"")."'>Origin & Origin ID Invalid<font>",1,1);
        $table->addColumn(($origin_dan_id_kosong*1)." Data",10,1);
        $table->commit("body");
        $table->addColumn("<font style='color:".($origin_kosong-$origin_dan_id_kosong>0?"red":"")."'>Origin Invalid<font>",1,1);
        $table->addColumn(($origin_kosong-$origin_dan_id_kosong)." Data",10,1);
        $table->commit("body");
        $table->addColumn("<font style='color:".($origin_id_kosong-$origin_dan_id_kosong>0?"red":"")."'>Origin ID Invalid<font>",1,1);
        $table->addColumn(($origin_id_kosong-$origin_dan_id_kosong)." Data",10,1);
        $table->commit("body");
        $table->addColumn("Total Data Valid",1,1);
        $table->addColumn(($total_data+$origin_dan_id_kosong)-($origin_id_kosong+$origin_kosong)." Data",10,1);
        $table->commit("body");
        
        return $table->getHtml();
    }
    
    private function reset(){
        $id=$_POST['id'];
        $autonomous=$this->getDBTable()->select(array("id"=>$_POST['id']));    
        $dbtable=new DBTable($this->getDBTable()->get_db(),$autonomous->table_name);
        $dbtable->addCustomKriteria(" autonomous "," LIKE '%[".$autonomous->autonomous."]%'");
        $total_synch=$dbtable->count("");
        
        $update['autonomous']="replace(autonomous, '[".$autonomous->autonomous."]', '')";
        $warp['autonomous']=DBController::$NO_STRIP_ESCAPE_WRAP;
        $dbtable->update($update,array(),$warp);
        $dbtable->setCustomKriteria(null);
        $total_data=$dbtable->count("");
        
        return "Table <strong>".$autonomous->table_name."</strong> Synch State Resetted [ ".$total_synch." / ".$total_data." ]";
    }
    
}

?>