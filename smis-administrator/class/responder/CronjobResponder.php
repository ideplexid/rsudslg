<?php 

class CronJobResponder extends DBResponder{
    
    /* get the data detail */
    public function detail(){
       $this->getDBTable()->setFetchMethode(DBTable::$ARRAY_FETCH);
       $x=$this->select();
       $id=$_POST['id'];
       $table=new TablePrint("");
       $table->setDefaultBootrapClass(true);
       $table->addColumn("CRONJOB INVOKER : [ <strong>".$id."</strong> ]",2,1,"title");
       $table->setMaxWidth(false);
       
       $table->addColumn("USER INVOKER",1,1);
       $table->addColumn($x['user'],1,1);
       $table->commit("header");
       $table->addColumn("TIME INVOKER",1,1);
       $table->addColumn(ArrayAdapter::format("date d M Y H:i:s",$x['waktu']),1,1);
       $table->commit("header");
           
       $file=$x['cronjob'];
       $ux=json_decode($file);
       $number=0;
       foreach($ux as $n=>$v){
           $number++;
           $table->addColumn("INVOKE #".$number,1,1);
           $table->addColumn($v,1,1);
           $table->commit("body");
       }
       return $table->getHtml();
    }
	
    
}

?>