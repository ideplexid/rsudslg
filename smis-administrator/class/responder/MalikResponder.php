<?php 

class MalikResponder extends FileResponder{
	
	protected function correctBase(){
		$BASE=parent::correctBase();
		$newbase="";
		if(isset($_POST['service']) && $_POST['service']!="" ){			
			if(isset($_POST['hash']) && strlen($_POST['hash'])==32 ){
				$BASE=$this->path;
				$hash=str_split($_POST['hash']);
				$fhash=implode("/",$hash);
				$newbase=$BASE.$_POST['service']."/".$fhash."/";
			}
		}		
		if($newbase=="" || !file_exists($newbase) ){
			$this->base=$BASE;			
		}else{
			$this->base=$newbase;
		}
		
		return $this->base;
	}
	
	protected function loadFile(ResponsePackage $res){
		parent::loadFile($res);
		$content=$res->getContent();
		$obj=json_decode($content);
		$filename=$this->base."/".$_POST['filename'];
		$string=$this->adapt($obj,$filename);	
		$res->setContent($string);
	}
	
	
	protected function adapt($obj,$filename){
		$fname=substr(basename($filename,".json"),19);
		if(startsWith($fname,"_service_request_")){
			return $this->requestAdapt($obj);
		}else if(startsWith($fname,"_serverbus_log_")){
			return $this->serverbusAdapt($obj);
		}else if(startsWith($fname,"_autonomous_log_")){
			return $this->autonomousAdapt($obj);
		}else if(startsWith($fname,"_entity_log_")){
			return $this->entityAdapt($obj);
		}
	}
	
	/**
	 * @brief this is used for formatting entity log
	 * 			the file must start with _entitiy_log_
	 * @param Object $onerow 
	 * @return  string
	 */
	public function entityAdapt($onerow) {
		$post = $onerow->post;
		$get = $onerow->get;
		$response = str_replace ( "<", "&lt;", $onerow->response );
		
		$post_freetify=json_encode(json_decode($onerow->post,true),JSON_PRETTY_PRINT);
		$get_freetify=json_encode(json_decode($onerow->get,true),JSON_PRETTY_PRINT);
		$response_freetify=decodeUntilArray($onerow->response);
		
		$array_service = json_decode ( $onerow->service, true );
		$service = ArrayAdapter::format ( "array-val", $array_service );
		$query = ArrayAdapter::format ( "json-val", $onerow->query );
		$error_query = ArrayAdapter::format ( "json-val", $onerow->error_query );
		$error_php = ArrayAdapter::format ( "json-val", $onerow->error_php );
		$error_log = ArrayAdapter::format ( "json-val", $onerow->error_log );
		
		
		$preceeding="<div class='label'>LOG</div>
					 <div>$error_log</div></br></br>
					 <div class='label label-warning'>ERROR QUERY</div>
					 <div>$error_query</div></br></br>
					 <div class='label label-important'>ERROR PHP</div>
					 <div>$error_php</div></br></br>
					 <div class='label label-success'>QUERY</div>
					 <div>$query</div></br></br>
					 <div class='label label-inverse'>SERVICE</div>
					 <div>$service</div>";
		
		$raw = "<div class='label label-primary'>POST</div>
				<div contenteditable='true'>$post</div></br>
				<div class='label label-info'>POST</div>
				<div contenteditable='true'>$get</div></br>
				<div class='label label-warning'>RESPONSE</div>
				<div contenteditable='true'>$response</div></br>";
					
		$fetreefy = "<div class='label label-primary'>POST</div>
					 <pre>$post_freetify</pre></br>
					 <div class='label label-info'>GET</div>
					 <pre>$get_freetify</pre></br>
					 <div class='label label-warning'>RESPONSE</div>
					 <pre>$response_freetify</pre></br>";
		
		$tabulator=new Tabulator("", "");
		$tabulator->add("raw_servicelog", "Raw", $raw,Tabulator::$TYPE_HTML,"fa fa-file-text");
		$tabulator->add("fetreefy_servicelog", "Fetreefy", $fetreefy,Tabulator::$TYPE_HTML,"fa fa-female");
		$tabulator->add("log_servicelog", "Log", $preceeding,Tabulator::$TYPE_HTML,"fa fa-file-o");
		$result=$tabulator->getHtml();
		return $result;
	}
	
	/**
	 * @brief this adaption used for adapt the autonomous caller
	 * 			file. the must be starts with _autonomous_log_
	 * @param Object $onerow 
	 * @return  string
	 */
	public function autonomousAdapt($onerow) {
		$request = $onerow->request;
		$erponse= $onerow->erponse;
		$post = $onerow->post;
		$response = str_replace ( "<", "&lt;", $onerow->response );
		$request_freetify=json_encode(json_decode($onerow->request,true),JSON_PRETTY_PRINT);
		$post_freetify=json_encode(json_decode($onerow->post,true),JSON_PRETTY_PRINT);
		$response_freetify=str_replace ( "<", "&lt;",json_encode(json_decode($onerow->response,true),JSON_PRETTY_PRINT));
		$path = ArrayAdapter::format ( 'json', $onerow->path );
		$path="<div class='label label-success'>PATH</div>
					<div>$path</div></br>";
		$raw = "<div class='label label-primary'>POST</div>
				<div contenteditable='true'>$post</div></br>
				<div class='label label-important'>REQUEST</div>
				<div contenteditable='true'>$request</div></br>
				<div class='label label-warning'>RESPONSE</div>
				<div contenteditable='true'>$response</div></br>";
		
		$fetreefy = "<div class='label label-primary'>POST</div>
					 <pre>$post_freetify</pre></br>
					 <div class='label label-important'>REQUEST</div>
					 <pre>$request_freetify</pre></br>
					 <div class='label label-warning'>RESPONSE</div>
					 <pre>$response_freetify</pre></br>";
		
		/*erponse*/
		$_ERPONSE=json_decode($erponse,true);
		$_ERPONSE_RESULT="";
		foreach($_ERPONSE as $E=>$V){
			$_ERPONSE_RESULT.="<div class='label'>".$E."</div>";
			$_ERPONSE_RESULT.="<pre>".$V."</pre></br>";
		}
		
		$tabulator=new Tabulator("", "");
		$tabulator->add("raw_path", "Path", $path,Tabulator::$TYPE_HTML,"fa fa-pinterest");
		$tabulator->add("raw_responselog", "Raw", $raw,Tabulator::$TYPE_HTML,"fa fa-file-text");
		$tabulator->add("fetreefy_responselog", "Fetreefy", $fetreefy,Tabulator::$TYPE_HTML,"fa fa-female");
		$tabulator->add("erponse_responselog", "Error Response", $_ERPONSE_RESULT,Tabulator::$TYPE_HTML,"fa fa-houzz");
		$result=$tabulator->getHtml();		
		return $result;
	}
	
	/**
	 * @brief this used to format the file
	 * 			of the serverbus log 
	 * 			the file must start with _serverbus_log_
	 * @param Object $onerow 
	 * @return  string
	 */
	public function serverbusAdapt($onerow){
		$autonomous=json_decode($onerow->autonomous);
		$at=array();
		foreach($autonomous as $a){
			$at[$a->name]=$a->address;
		}
		$responder=ArrayAdapter::format("array", $at);
		
		$key_server=$onerow->key_server;	
		$key_responder=ArrayAdapter::format("json", $onerow->key_responder);
		$key_requester=$onerow->key_requester;
						
		$request=$onerow->request;
		$response=str_replace("<","&lt;",$onerow->response);
		$raw_request=$onerow->raw_request;
		$raw_response=$onerow->raw_response;
		$successor_send=$onerow->successor_send;
		$successor_get=str_replace("<","&lt;",$onerow->successor_get);
		
			
		$RESP="		<div class='label label-warning'>ENCRYPTED</div></br></br>
					<div><div contenteditable='true'>$raw_response</div></div>
					<div class='label'>RAW</div>
					<div><div contenteditable='true'>$response</div></div></br>
					<div class='label label-success'>FETREEFY</div>
					<div><pre>".json_encode(json_decode($response,true),JSON_PRETTY_PRINT)."</pre></div></br>
					";
		
		$REQ="		<div class='label label-important'>ENCRYPTED</div>
					<div><div contenteditable='true'>$raw_request</div></div></br>
					<div class='label'>RAW</div>
					<div><div contenteditable='true'>$request</div></div></br>
					<div class='label label-success'>FETREEFY</div>
					<div><pre>".json_encode(json_decode($request,true),JSON_PRETTY_PRINT)."</pre></div></br>
					";
		
		$SUCCESSOR="<div class='label'>SEND</div>
					<div><div contenteditable='true'>$successor_send</div></div></br>
					<div class='label label-success'>SEND FETREEFY</div>
					<div><pre>".json_encode(json_decode($successor_send,true),JSON_PRETTY_PRINT)."</pre></div></br>
					<div class='label'>GET</div>
					<div><div contenteditable='true'>$successor_get</div></div></br>
					<div class='label label-success'>GET FETREEFY</div>
					<div><pre>".json_encode(json_decode($successor_get,true),JSON_PRETTY_PRINT)."</pre></div></br>
		";
		
		$RESPOND="<div class='label label-success'>Autonomous</div>
					<div>$responder</div>
					";
		
		$key['Key Serverbus']=$key_server;
		$key['Key Autonomous Consumer']=$key_requester;
		$key['Keys Autonomous Responder']=$key_responder;
			
		
		$KEY=ArrayAdapter::format("array", $key);
		$tabulator=new Tabulator("", "");
		$tabulator->add("key_".$onerow->id, "Key", $KEY, Tabulator::$TYPE_HTML,"fa fa-key");
		$tabulator->add("responder_".$onerow->id, "Autonomous Responder", $RESPOND, Tabulator::$TYPE_HTML,"fa fa-desktop");
		$tabulator->add("request_".$onerow->id, "Request", $REQ, Tabulator::$TYPE_HTML,"fa fa-long-arrow-right");
		$tabulator->add("successor_".$onerow->id, "Successor", $SUCCESSOR, Tabulator::$TYPE_HTML,"fa fa-arrows-h");
		$tabulator->add("response_".$onerow->id, "Response", $RESP, Tabulator::$TYPE_HTML,"fa fa-long-arrow-left");
		$result=$tabulator->getHtml();			
		return $result;
	}
		
	
	/**
	 * @brief format the reqeust data 
	 * 			from the file. this file must start with
	 * 			_service_request_
	 * @param Object $onerow 
	 * @return  string
	 */
	public function requestAdapt($onerow) {
		$request = $onerow->request;
		$post = $onerow->post;
		$response = str_replace ( "<", "&lt;",$onerow->response);
		$raw= "<div class='label label-primary'>POST</div>
			   <div contenteditable='true'>$post</div></br>
			   <div class='label label-important'>REQUEST</div>
			   <div contenteditable='true'>$request</div></br>
			   <div class='label label-warning'>RESPONSE</div>
			   <div contenteditable='true'>$response</div></br>";
		
		$request = json_decode($onerow->request,true);
		$post = json_decode($onerow->post,true);
		$response = json_decode($onerow->response,true);			
		$request = json_encode($request,JSON_PRETTY_PRINT);
		$post = json_encode($post,JSON_PRETTY_PRINT);
		$response = str_replace ( "<", "&lt;",json_encode($response,JSON_PRETTY_PRINT) );
			
		
		
		$fetreery= "<div class='label label-primary'>POST</div>
					<pre>$post</pre></br>
					<div class='label label-important'>REQUEST</div>
					<pre>$request</pre></br>
					<div class='label label-warning'>RESPONSE</div>
					<pre>$response</pre></br>";
		
		$tabulator=new Tabulator("", "");
		$tabulator->add("raw_requestlog", "Raw", $raw,Tabulator::$TYPE_HTML,"fa fa-file-text");
		$tabulator->add("fetreefy_requestlog", "Fetreefy", $fetreery,Tabulator::$TYPE_HTML,"fa fa-female");
		$result=$tabulator->getHtml();
		
		return $result;
	}
	
	
}

?>