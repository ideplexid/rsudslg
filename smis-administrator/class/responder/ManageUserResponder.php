<?php 

class ManageUserResponder extends DuplicateResponder {
	public function command($command) {
		$pack = new ResponsePackage ();
		$content = NULL;
		$status = 'not-command'; // not-authorized, not-command, fail, success
		$alert = array ();
			
		if ($command == 'save') {
			$content = $this->save ();
			$pack->setContent ( $content );
			$pack->setStatus ( ResponsePackage::$STATUS_OK );

			if ($_POST ['id'] == '' || $_POST ['id'] == '0') {
				$pack->setAlertVisible ( false );
				$pack->setWarning ( false, "User Saved,", "User Saved, The Password is 'default' " );
			} else {
				$pack->setAlertContent ( "User Saved,", "User Saved", ResponsePackage::$TIPE_INFO);
				$pack->setAlertVisible ( false);
			}
		} else if ($command == 'reset') {
			$content = $this->reset ();
			$pack->setContent ( $content );
			$pack->setStatus ( ResponsePackage::$STATUS_OK );
			$pack->setAlertVisible ( false );
			$pack->setWarning ( true, "Password Resetted", "Password Successfully Reset, The Password is 'default' " );
		} else {
			$pack = parent::command ( $command );
			return $pack;
		}
		return $pack->getPackage ();
	}
	public function reset() {
		$id ['id'] = $_POST ['id'];
		$data ['password'] = md5 ( "default" );
		$result = $this->dbtable->update ( $data, $id );
		$success ['type'] = 'update';
		$success ['id'] = $id ['id'];
		$success ['success'] = 1;
		if ($result === false)
			$success ['success'] = 0;
		return $success;
	}
	public function postToArray() {
		$s = parent::postToArray ();
		if (isset ( $_POST ['id'] ) && isset ( $_POST ['command'] ) && $_POST ['command'] == 'save') {
			if ($_POST ['id'] == '0' || $_POST ['id'] == '') {
				$s ['password'] = md5 ( 'default' );
			} else if ($_POST ['id'] != '0' && $_POST ['id'] != '') {
				if (isset ( $s ["username"] ))
					unset ( $s ["username"] );
				if (isset ( $s ["realname"] ))
					unset ( $s ["realname"] );
			}
		}
		return $s;
	}
}
?>