<?php 

class SynchronizeConsumerResponder extends SynchronizeProviderResponder{
    
    public function command($command){
        if($command=="test_synch"){
            $pack=new ResponsePackage();
            $content=$this->testSynch();
            $pack->setWarning(true,"Synch Result",$content);
            $pack->setStatus(ResponsePackage::$STATUS_OK);
            return $pack->getPackage();             
        }else if($command=="test_synch_zero"){
            $pack=new ResponsePackage();
            $content=$this->testSynchUntilZero();
            $pack->setContent($content);
            $pack->setStatus(ResponsePackage::$STATUS_OK);
            return $pack->getPackage();             
        }else if($command=="synch_all"){
            $pack=new ResponsePackage();
            $content=$this->SynchAll();
            $pack->setContent($content);
            $pack->setStatus(ResponsePackage::$STATUS_OK);
            return $pack->getPackage();             
        }else if($command=="test_match"){
            $pack=new ResponsePackage();
            $content=$this->testMatch();
            $pack->setWarning(true,"Comparation Result",$content);
            $pack->setStatus(ResponsePackage::$STATUS_OK);
            return $pack->getPackage();             
        }else{
            return parent::command($command);
        }
    }    
    
    private function SynchAll(){
        $this->getDBTable()->setShowAll(false);
        $data=$this->getDBTable()->view("",0);
        $list=$data['data'];
        return $list;
    }
    
    /**
     * @brief this function used to test is the data match or not
     * @return  string match result
     */
    private function testMatch(){
        global $wpdb;
        global $db;
        $id=$_POST['id'];
        $consumer=$this->getDBTable()->select(array("id"=>$_POST['id']));    
        $_name=$consumer->code_name;
        $_autonomous=$consumer->autonomous;
        
        require_once("smis-base/smis-include-service-consumer.php");        
        $service=new ServiceConsumer($db,"synchronize_matcher",null,"smis-administrator",$_autonomous);
        $service->addData("code_name",$_name);
        $service->addData("smis-synch-consumer-token","1");       
        $service->execute();
        $provider=$service->getContent();
        
        require_once "smis-administrator/function/synch_match_builder.php";
        $dbtable=new DBTable($db,"smis_adm_synch_consumer");
        $consumer=synch_match_build($wpdb,$db,$dbtable,$_name);
        
        $tp=new TablePrint();
        $tp->setMaxWidth(false);
        $tp->setDefaultBootrapClass(true);
        
        $tp->addColumn(strtoupper($_name),5,1,"title");
        $tp->addColumn("",1,1);
        $tp->addColumn(strtoupper(getSettings($db,"smis_autonomous_id","")),1,1);
        $tp->addColumn(strtoupper($_autonomous),1,1);
        $tp->addColumn("",1,1);
        $tp->commit("title");
        
        $tp->addColumn("Pair",1,1);
        $tp->addColumn($consumer['prov_msg'],1,1);
        $tp->addColumn($provider['prov_msg'],1,1);
        $tp->addColumn($this->match($consumer['prov_msg'],$provider['prov_msg']),1,1);
        $tp->commit("body");
        
        $tp->addColumn("Table",1,1);
        $tp->addColumn($consumer['table_msg'],1,1);
        $tp->addColumn($provider['table_msg'],1,1);
        $tp->addColumn($this->match($consumer['table_msg'],$provider['table_msg']),1,1);
        $tp->commit("body");
        
        $tp->addColumn("Table Name",1,1);
        $tp->addColumn($consumer['table_name'],1,1);
        $tp->addColumn($provider['table_name'],1,1);
        $tp->addColumn($this->match($consumer['table_name'],$provider['table_name']),1,1);
        $tp->commit("body");
        
        $tp->addSpace(5,1,"body");
        $tp->addColumn("<strong>TABLE KEY</strong>",4,1,"body");
        $tbl_index_consumer=$consumer['table_index'];
        $tbl_index_provider=$provider['table_index'];              
        $tp->addColumn("Total Key",1,1);
        $tp->addColumn(count($tbl_index_consumer),1,1);
        $tp->addColumn(count($tbl_index_provider),1,1);
        $tp->addColumn($this->match(count($tbl_index_consumer),count($tbl_index_provider)),1,1);
        $tp->commit("body");
        $key_match=array();
        foreach($tbl_index_consumer as $key=>$value){
            if(!isset($key_match[$key])){
                $key_match[$key]=array();
            }
            $key_match[$key]['consumer']=implode(", ",$value);
        }        
        foreach($tbl_index_provider as $key=>$value){
            if(!isset($key_match[$key])){
                $key_match[$key]=array();
            }
            $key_match[$key]['provider']=implode(", ",$value);
        }        
        foreach($key_match as $key=>$value){
            $tp->addColumn($key,1,1);
            $tp->addColumn($value['consumer'],1,1);
            $tp->addColumn($value['provider'],1,1);
            $tp->addColumn($this->match($value['consumer'],$value['provider']),1,1);
            $tp->commit("body");
        }
        
        $tp->addSpace(5,1,"body");
        $tp->addColumn("<strong>TABLE COLUMN</strong>",4,1,"body");
        $tbl_column_consumer=$consumer['table_column'];
        $tbl_column_provider=$provider['table_column'];
        $tp->addColumn("Total Column",1,1);
        $tp->addColumn(count($tbl_column_consumer),1,1);
        $tp->addColumn(count($tbl_column_provider),1,1);
        $tp->addColumn($this->match(count($tbl_column_consumer),count($tbl_column_provider)),1,1);
        $tp->commit("body");
        $column_match=array();
        foreach($tbl_column_consumer as $key=>$value){
            if(!isset($column_match[$key])){
                $column_match[$key]=array();
            }
            $val=" field = ".$value['field'];
            $val.="\n type = ".$value['type'];
            $val.="\n null = ".$value['null'];
            $val.="\n key = ".$value['key'];
            $val.="\n default = ".$value['default'];
            $val.="\n extra = ".$value['extra'];
            $column_match[$key]['consumer']=$val;
        }        
        foreach($tbl_column_provider as $key=>$value){
            if(!isset($column_match[$key])){
                $column_match[$key]=array();
            }
            $val=" field = ".$value['field'];
            $val.="\n type = ".$value['type'];
            $val.="\n null = ".$value['null'];
            $val.="\n key = ".$value['key'];
            $val.="\n default = ".$value['default'];
            $val.="\n extra = ".$value['extra'];
            $column_match[$key]['provider']=$val;
        }        
        foreach($column_match as $key=>$value){
            $tp->addColumn($key,1,1);
            $tp->addColumn($value['consumer'],1,1);
            $tp->addColumn($value['provider'],1,1);
            $tp->addColumn($this->match($value['consumer'],$value['provider']),1,1);
            $tp->commit("body");
        }
        
        
        $tp->addSpace(5,1,"body");
        $tp->addColumn("<strong>DATA INFORMATION</strong>",4,1,"body");
        $tp->addColumn("Total Row",1,1);
        $tp->addColumn($consumer['table_data'],1,1);
        $tp->addColumn($provider['table_data'],1,1);
        $tp->addColumn("-",1,1);
        $tp->commit("body");
        
        $tp->addColumn("Origin Empty",1,1);
        $tp->addColumn($consumer['table_origin_kosong'],1,1);
        $tp->addColumn($provider['table_origin_kosong'],1,1);
        $tp->addColumn("-",1,1);
        $tp->commit("body");
        
        $tp->addColumn("ID Empty",1,1);
        $tp->addColumn($consumer['table_origin_id'],1,1);
        $tp->addColumn($provider['table_origin_id'],1,1);
        $tp->addColumn("-",1,1);
        $tp->commit("body");
        
        $tp->addColumn("ID & Origin Empty",1,1);
        $tp->addColumn($consumer['table_origin_id_kosong'],1,1);
        $tp->addColumn($provider['table_origin_id_kosong'],1,1);
        $tp->addColumn("-",1,1);
        $tp->commit("body");
        
        $tp->addColumn("Data Valid",1,1);
        $tp->addColumn($consumer['table_valid'],1,1);
        $tp->addColumn($provider['table_valid'],1,1);
        $tp->addColumn("-",1,1);
        $tp->commit("body");
        
        $tp->addColumn("Data Synched",1,1);
        $tp->addColumn($consumer['table_synched'],1,1);
        $tp->addColumn($provider['table_synched'],1,1);
        $tp->addColumn("-",1,1);
        $tp->commit("body");
        
        $tp->addColumn("Data Not Synched",1,1);
        $tp->addColumn($consumer['table_unsynched'],1,1);
        $tp->addColumn($provider['table_unsynched'],1,1);
        $tp->addColumn("-",1,1);
        $tp->commit("body");
        
        return $tp->getHtml();
    }
    
    
    private function match($a,$b){
        if($a==$b){
            return "<i class='fa fa-check'></i>";
        }
        return "<i class='fa fa-times' style='color:red'></i>";
    }
    
    
    
    private function testSynch(){
        $id=$_POST['id'];
        $serv=$this->runTest($_POST['id']);
        $log                = $serv->getLog();
        $code_name          = $consumer->code_name;
        $download           = $serv->getDownloadedData();
        $upload             = $serv->getUploadedData();
        $success_upload     = $serv->getSuccessUploadedData();
        $success_download   = $serv->getSuccessDownloadedData();
        $response           = $serv->getResponseData();
        
        $hasil="<h3>".$code_name."</h3>";
        $hasil.="<h4>LOG</h4>";
        $hasil.="<pre>".$log."</pre>";
        $hasil.="<h4>RESPONSE</h4>";
        $hasil.="<pre>".json_encode($response)."</pre>"; 
        $hasil.="<h5>UPLOAD DATA</h5>";
        $hasil.="<pre>".json_encode($upload)."</pre>";        
        $hasil.="<h5>SUCCESS UPLOAD DATA</h5>";
        $hasil.="<pre>".json_encode($success_upload)."</pre>";
        $hasil.="<h5>DOWNLOAD DATA</h5>";
        $hasil.="<pre>".json_encode($download)."</pre>";        
        $hasil.="<h5>SUCCESS DOWNLOAD DATA</h5>";
        $hasil.="<pre>".json_encode($success_download)."</pre>";
        return $hasil;
    }
    
    private function testSynchUntilZero(){
        $id=$_POST['id'];
        $serv=$this->runTest($_POST['id']);
        $consumer=$this->getDBTable()->select(array("id"=>$_POST['id']));
        return $consumer;
    }
    
    private function runTest($id){
        $consumer=$this->getDBTable()->select(array("id"=>$id));
        require_once("smis-base/smis-include-service-consumer.php");
        require_once "smis-administrator/class/service/SynchronizeServiceConsumer.php";
        $autonomous_name=getSettings($this->getDBTable()->get_db(),"smis_autonomous_id","");
        global $user;        
        $serv=null;            
        if($consumer->table_name!="smis_adm_settings"){
            $serv=new SynchronizeServiceConsumer($this->getDBTable()->get_db(),$user,$consumer,$autonomous_name);
        }else{
            require_once "smis-administrator/class/service/SynchronizeSettingsConsumer.php";
            $serv=new SynchronizeSettingsConsumer($this->getDBTable()->get_db(),$user,$consumer,$autonomous_name);
        }
        $serv->initialize();
        return $serv;
    }
}

?>