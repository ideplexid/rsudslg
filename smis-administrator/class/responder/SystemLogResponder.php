<?php

class SystemLogResponder extends DBResponder{
    public function command($command){
        if($command!="slack"){
            return parent::command($command);
        }else{            
            $this->response_pack=new ResponsePackage();
            $this->response_pack->setStatus(ResponsePackage::$STATUS_OK);
            $this->response_pack->setAlertVisible(true);
            $this->slack();
            return $this->response_pack->getPackage();
        }
    }

    public function slack(){
        global $user;
        $row     = $this->getDBTable()->select($_POST['id']);
        if($row != null){
            $query = str_replace("\n","",$row->error_query);
            $query = strip_tags(implode(json_decode($query,true),"\n -------------------------------- \n\n"));
            $query = str_replace("When executing: ","---------------------------------\n",$query);
            
            $php = implode(json_decode($row->error_php,true),"\n #-#-#-#-#-#-#-# \n");
            $php = str_replace("</strong>"," -> ",$php);
            $php = str_replace("</li>","\n - - - - - - - - - \n",$php);
            $php = str_replace("</br>","\n - - - - - - - - - \n",$php);
            $php = strip_tags($php);
            
            require_once "smis-social-network/class/other/SlackMessage.php";
            $slack       = new SlackMessage($this->getDBTable()->get_db());
            $slack       ->addField("#POST",json_encode(json_decode($row->post,true), JSON_PRETTY_PRINT));
            $slack       ->addField("#QUERY",$query);
            $slack       ->addField("#PHP",$php);
            $slack       ->setAuthorName($user->getNameOnly());
            $slack       ->setTimeSecond();
            $slack       ->setColor("#FFCCCC");
            $slack       ->setTitle("System Log - ".$_POST['id']." on ".getSettings($this->getDBTable()->get_db(),"slack_name","smis"));
            $result      = $slack->send();
            if($result){
                $this->response_pack->setAlertContent("Send to Slack Success !!!","Sending to slack have been successfully",ResponsePackage::$TIPE_SUCCESS );
            }else{
                $this->response_pack->setAlertContent("Send to Slack Failed !!!","Sending to slack have failed successfully, check the slack setting in smis-social-network",ResponsePackage::$TIPE_DANGER );    
            }
        }
        
    }
}

?>