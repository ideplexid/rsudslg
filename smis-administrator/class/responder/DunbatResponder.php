<?php 

class DunbatResponder extends FileResponder{
	
	public function command($command){
		if($command!="restore"){
			return parent::command($command);
		}else{
			$this->response_pack->setStatus(ResponsePackage::$STATUS_OK);
			$this->response_pack->setAlertVisible(true);		
			if ($this->restore($this->response_pack)) {
				$this->response_pack->setAlertContent ( "Data Restored", "The Data Have Been Restored", "alert-success" );
			} else {
				$this->response_pack->setAlertContent ( "Data Not Restored", "The Data Have Not Restored Yet", "alert-danger" );
			}
			$this->response_pack->setContent ("");			
			return $this->response_pack->getPackage();
		}
	}
	
	/**
	 * @brief this used for restoring the database through the system
	 * 			1. get the file 
	 * 			2. open it 
	 * 			3. decode it from json
	 * 			4. take the db and the original
	 * 			5. save the change to log
	 * @param ResponsePackage $res 
	 * @return  
	 */
	protected function restore(ResponsePackage $res){		
		$filename=$this->base."/".$_POST['filename'];
		if(!file_exists($filename)){
			return false;
		}
		$file=file_get_contents($filename);
		$log=json_decode($file);
		$table = $log->db;
		$original = $log->original;
		global $db;
		$rttable = new DBTable ( $db, $table );		
		if ($original == "") {
			$restored = json_decode ( $log->edited, true );
			$id ['id'] = $restored ['id'];
			$restored = null;
		} else {
			$restored = json_decode ( $original, true );
			$id ['id'] = $restored ['id'];
		}		
		return $rttable->restore ( $restored, $id ) !== false;
	}
	
	protected function correctBase(){
		$newbase="";
		if(	!isset($_POST['database']) || !isset($_POST['dbid']) || isset($_POST['dbid']) && $_POST['dbid']=="" && isset($_POST['database']) && $_POST['database']==""  ){
			return $this->path;
		}
		
		if(isset($_POST['dbid']) && $_POST['dbid']!="" && isset($_POST['database']) ){			
			$fhash=str_replace("_","/",$_POST['database']);
			$dbid=str_split($_POST['dbid']."");
			$dbidpath=implode("/",$dbid);
			$newbase=$this->path.$fhash."/".$dbidpath."/";
		}
		
		if($newbase=="" || !file_exists($newbase) ){
			$this->base=$this->path;		
			$this->response_pack->setWarning(true,"No Log Found","Log Not Found for Database [ <strong>".$_POST['database']."</strong> ] and id [ <strong>".$_POST['dbid']."</strong> ], you could : </br>1. check the database name and the id, usually mistype </br>2. the log file was removed </br>3. Log Setting not in File but in Database, Used Server Activity Instead </br>4. Setup the <u>smis-base/smis-config.php</u> to activate <strong><i>define('LOG_SAVE_ONFILE','1');</i></strong>");
		}else{
			$this->base=$newbase;
		}
		return $this->base;
	}
	
	protected function loadFile(ResponsePackage $res){
		parent::loadFile($res);
		$content=$res->getContent();
		$obj=json_decode($content);
		$string=$this->adapt($obj);		
		$res->setContent($string);
	}
	
	public function compared($original, $eddited) {
		$ori = json_decode ( $original, true );
		$edt = json_decode ( $eddited, true );			
		if ($edt == null || $edt == "" || ! is_array ( $edt ))
			return "";
			
		$rsl = array ();
		foreach ( $edt as $k => $edt_v ) {
			$ori_v = $ori [$k];
			if ($edt_v != $ori_v) {
				$rsl [$k] = "<text class='diff'>" . str_replace ( "<", "&lt;",$edt_v) . "</text>";
			} else
				$rsl [$k] = str_replace ( "<", "&lt;",$edt_v);
		}
		return $rsl;
	}
	
	
	protected function adapt($row){
		$pst=json_decode($row->post);
		$ori=json_decode(str_replace ( "<", "&lt;", $row->original));
		$edt=json_decode(str_replace ( "<", "&lt;", $row->edited));
		$acom=$this->compared($row->original, $row->edited);		
		$a['post']=json_encode($pst,JSON_PRETTY_PRINT);
		$a['original']=json_encode($ori,JSON_PRETTY_PRINT);
		$a['edited']=json_encode($edt,JSON_PRETTY_PRINT);
		$a['compare']=ArrayAdapter::format("array", $acom);
		return $a;
	}
	
}

?>