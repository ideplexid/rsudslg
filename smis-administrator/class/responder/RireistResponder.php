<?php 

class RireistResponder extends DBResponder{
    
    public function command($command){
        if($command=="reverse"){
            $this->response_pack=new ResponsePackage();
            $ct=$this->reverseTable();
            
            $this->response_pack->setContent($ct);
            $this->response_pack->setStatus(ResponsePackage::$STATUS_OK);
            return $this->response_pack->getPackage();
        }else if($command=="deploy"){
            $this->response_pack=new ResponsePackage();
            $this->deploy();
            $this->response_pack->setContent("");
            $this->response_pack->setStatus(ResponsePackage::$STATUS_OK);
            return $this->response_pack->getPackage();
        }else{
            return parent::command($command);
        }
    }
    
    private function reverseTable($newline="&#10;"){
        global $wpdb;
        $table=$_POST['id'];
        require_once "smis-libs-class/DBCreator.php";
        $tbl=$this->dbtable->select(array("id"=>$table));
        $dbcreator=new DBCreator($wpdb,$table,$tbl['engine']);
        $result=$dbcreator->reverseEngineer($newline);
        return $result;
    }
    
    private function deploy(){
        $data="<?php\n".$this->reverseTable("\n")."\n?>";
        $deploy=$_POST['deploy'];
        $table=$_POST['id'];
        $path=$deploy;
        if(!file_exists($path)){
             $this->response_pack->setWarning(true,"Deployment Failed","Folder <strong>".$path."</strong> Not Exist");
        }else{
            $path=$path."/resource/";
            if(!file_exists($path)) {
                $result=createFolder($path);
                if($result===false){
                    $this->response_pack->setWarning(true,"Deployment Failed","Error Creating : <strong>".$path."</strong>");
                    return;
                }
            }
            $path=$path."install/";
            if(!file_exists($path)) {
                $result=createFolder($path);
                if($result===false){
                    $this->response_pack->setWarning(true,"Deployment Failed","Error Creating : <strong>".$path."</strong>");
                    return;
                }
            }
            $path=$path."table/";
            if(!file_exists($path)) {
                $result=createFolder($path);
                if($result===false){
                    $this->response_pack->setWarning(true,"Deployment Failed","Error Creating : <strong>".$path."</strong>");
                    return;
                }
            }
            $path=$path.$table.".php";
            $result=file_put_contents($path, $data);
            if($result===false){
                $this->response_pack->setWarning(true,"Deployment Failed","Error Creating : <strong>".$path."</strong>");
                return;
            }
            $this->response_pack->setWarning(true,"Deployment Success","File Created in :  <strong>".$path."</strong>");
            return;
        }
       
    }
}


 ?>