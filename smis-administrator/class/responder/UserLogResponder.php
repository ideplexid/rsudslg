<?php 

class UserLogResponder extends DBResponder{	
	public function command($command){
		if($command!="restore"){
			return parent::command($command);
		}else{			
			$response = new ResponsePackage ();
			$response->setStatus ( "ok" );
			$response->setAlertVisible ( true );
			
			if ($this->restore()) {
				$response->setAlertContent ( "Data Restored", "The Data Have Been Restored", "alert-success" );
			} else {
				$response->setAlertContent ( "Data Not Restored", "The Data Have Not Restored Yet", "alert-danger" );
			}
			$response->setContent ("");
			return $response->getPackage();
		}
	}	
	private function restore(){
		$log = $this->getDBTable()->select ( $_POST ['id'], false );
		$table = $log->db;
		$original = $log->original;
		$rttable = new DBTable ( $this->getDBTable()->get_db(), $table );		
		if ($original == "") {
			$restored = json_decode ( $log->edited, true );
			$id ['id'] = $restored ['id'];
			$restored = null;
		} else {
			$restored = json_decode ( $original, true );
			$id ['id'] = $restored ['id'];
		}		
		return $rttable->restore ( $restored, $id ) !== false;
	}
	
	public function compared($original, $eddited) {
		$ori = json_decode ( $original, true );
		$edt = json_decode ( $eddited, true );			
		if ($edt == null || $edt == "" || ! is_array ( $edt ))
			return "";
			
		$rsl = array ();
		foreach ( $edt as $k => $edt_v ) {
			$ori_v = $ori [$k];
			if ($edt_v != $ori_v) {
				$rsl [$k] = "<text class='diff'>" . str_replace ( "<", "&lt;",$edt_v) . "</text>";
			} else
				$rsl [$k] = str_replace ( "<", "&lt;",$edt_v);
		}
		return $rsl;
	}
	
	public function edit(){
		$row=parent::edit();
		$a=array();
		$pst=json_decode($row->post);
		$ori=json_decode(str_replace ( "<", "&lt;", $row->original));
		$edt=json_decode(str_replace ( "<", "&lt;", $row->edited));
		$acom=$this->compared($row->original, $row->edited);		
		$a['post']=json_encode($pst,JSON_PRETTY_PRINT);
		$a['original']=json_encode($ori,JSON_PRETTY_PRINT);
		$a['edited']=json_encode($edt,JSON_PRETTY_PRINT);
		$a['compare']=ArrayAdapter::format("array", $acom);
		return $a;
	}
}

?>