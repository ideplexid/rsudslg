<?php 

class LooqmanResponder extends FileResponder{
	
	
	protected function loadFile(ResponsePackage $res){
		parent::loadFile($res);
		$content=$res->getContent();
		$obj=json_decode($content);
		$string=$this->adapt($obj);
		$res->setContent($string);
	}
	
	protected function adapt($onerow){
		$pack = json_decode ( $onerow->post, true );
		$p ['Page'] = isset ( $pack ['page'] ) ? $pack ['page'] : "refresh";
		$p ['Action'] = isset ( $pack ['action'] ) ? $pack ['action'] : "refresh";
		$p ['Command'] = isset ( $pack ['command'] ) ? $pack ['command'] : "refresh";
				
		$array_service = json_decode ( $onerow->service, true );
		$post = ArrayAdapter::format ( "json", $onerow->post );
		$service = ArrayAdapter::format ( "array-val", $array_service );
		$query = ArrayAdapter::format ( "json-val", $onerow->query );
		$error_query = ArrayAdapter::format ( "json-val", $onerow->error_query );
		$error_php = ArrayAdapter::format ( "json-val", $onerow->error_php );
		$error_log = ArrayAdapter::format ( "json-val", $onerow->error_log );
		
		
		$preview="<small>User : " . $onerow->user . "</small></br>";
		$preview.="<small>IP : " . $onerow->ip . "</small></br>";
		$preview.="<small>Waktu : " . ArrayAdapter::format ( 'date d-M-Y, H:i:s', $onerow->waktu ) . "</small></br>";
		$preview.="<small>Page : " . $p ['Page'] . "</small></br>";
		$preview.="<small>Action : " . $p ['Action'] . "</small></br>";
		$preview.="<small>Command : " . $p ['Command'] . "</small></br>";
		
		$result= "<div id='modal_systemlog_" . $onerow->id . "' >
					<div class='label'>PREVIEW</div>
					<div>$preview</div>
					
					</br></br>
					<div class='label'>LOG</div>
					<div>$error_log</div></br></br>
					<div class='label label-warning'>ERROR QUERY</div>
					<div>$error_query</div></br></br>
					<div class='label label-important'>ERROR PHP</div>
					<div>$error_php</div></br></br>
					<div class='label label-info'>POST</div>
					<div>$post</div></br></br>
					<div class='label label-success'>QUERY</div>
					<div>$query</div></br></br>
					<div class='label label-inverse'>SERVICE</div>
					<div>$service</div>						
				</div>";
		
		return $result;
	}
	
}

?>