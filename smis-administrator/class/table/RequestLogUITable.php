<?php 

class RequestLogUITable extends Table {
	public function getHeaderButton() {
		return "";
	}
	public function getContentButton($id) {
		$btn_group = parent::getContentButton($id);
		$btn = new Button ( "", "", "" );
		$btn->setClass ( "btn btn-info" );
		$btn->setAction ( $this->name . ".show('" . $id . "')" );
		$btn->setIsButton ( Button::$ICONIC );
		$btn->setIcon ( "icon-white  icon-resize-full" );
		$btn_group->addButton ( $btn );
		return $btn_group;
	}
}

?>