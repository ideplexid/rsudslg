<?php 

class SystemLogUITable extends Table {
	public function getHeaderButton() {
		global $db;
		$set = getSettings ( $db, "smis_log_system", "false" );
		
		$option=new OptionBuilder();
		$option->add("All","true",$set =="true"?"1":"0");
		$option->add("None","false",$set =="false"?"1":"0");
		$option->add("Selective","selective",$set =="selective"?"1":"0");
		
		$select = new Select("log_system", "Error Log", $option->getContent());
		return $select->getHtml ();
	}
	public function getContentButton($id) {
		$btn_group 	= new ButtonGroup ( 'noprint' );
		$btn 		= new Button ( "", "", "Show" );
		$btn->setClass ( "btn btn-info" );
		$btn->setAction ( $this->name . ".show('" . $id . "')" );
		$btn->setIsButton ( Button::$ICONIC_TEXT );
		$btn->setIcon ( "fa fa-eye" );
		$btn_group->addButton ( $btn );
		
		global $db;
		if(getSettings ( $db, "slack_enable", "0" )=="1"){
			$btn 		= new Button ( "", "", "Slack" );
			$btn->setClass ( "btn btn-success" );
			$btn->setAction ( $this->name . ".slack('" . $id . "')" );
			$btn->setIsButton ( Button::$ICONIC_TEXT );
			$btn->setIcon ( "fa fa-slack" );
			$btn_group->addButton ( $btn );
		}
		return $btn_group;
	}
}

?>