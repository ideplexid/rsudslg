<?php 

class PrototypeTable extends Table {
	public function getContentButton($id) {
		$btn_group = new ButtonGroup ( 'noprint' );
		$btn_group->setMax(10,"");
		if ($this->model == self::$EDIT) {
			$btn_group->add ( "", "", "", $this->action . ".edit('" . $id . "')", "btn-warning","", "fa fa-pencil" );
			$btn_group->add ( "", "", "", $this->action . ".del('" . $id . "')", "btn-danger","", "fa fa-remove" );
			
			$status = $this->current_data ['status'];
			$parent = $this->current_data ['parent'];
			$slug = $this->current_data ['slug'];
			if ($status == "") {
				$btn_group->add ( "", "", "", $this->action . ".install('" . $id . "','" . $slug . "','" . $parent . "')", "btn-success","", "fa fa-plug" );
			} else if ($status == "installed" || $status == "deactived") {
				$btn_group->add ( "", "", "", $this->action . ".uninstall('" . $id . "','" . $slug . "','" . $parent . "')", "btn-inverse","", "fa fa-eject" );
				$btn_group->add ( "", "", "", $this->action . ".actived('" . $id . "')", "btn-success","", "fa fa-play" );
			} else if ($status == "actived") {
				$btn_group->add ( "", "", "Deactived", $this->action . ".deactived('" . $id . "')", "btn-inverse","", "fa fa-stop" );
			}
		}
		return $btn_group;
	}
}
?>