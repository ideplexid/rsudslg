<?php 

class UserLogUITable extends Table {
	public function getHeaderButton() {
		return "Restored";
	}
	public function getContentButton($id) {
		$btn_group = new ButtonGroup ( 'noprint' );
		$btn = new Button ( "", "", "" );
		$btn->setClass ( "btn btn-success" );
		$btn->setAction ( $this->name . ".restore('" . $id . "')" );
		$btn->setIsButton ( Button::$ICONIC );
		$btn->setIcon ( "icon-white icon-repeat" );
		$btn_group->addButton ( $btn );
		$btn = new Button ( "", "", "" );
		$btn->setClass ( "btn btn-info" );
		$btn->setAction ( $this->name . ".show('" . $id . "')" );
		$btn->setIsButton ( Button::$ICONIC );
		$btn->setIcon ( "icon-white  icon-resize-full" );
		$btn_group->addButton ( $btn );
		return $btn_group;
	}
}


?>