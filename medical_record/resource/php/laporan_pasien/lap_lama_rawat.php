<?php 
global $db;
global $user;
require_once 'smis-base/smis-include-service-consumer.php';
if(isset($_POST['super_command']) && $_POST['super_command']=="total") {
	require_once 'smis-libs-class/ServiceProviderList.php';
	$dbtable=new DBTable($db, "smis_mr_pasien_pulang");
	$dbtable->truncate();
	$serv=new ServiceConsumer($db, "get_patient_lama_rawat");
	$serv->addData("command", "count");
	$serv->addData("dari", $_POST['dari']);
	$serv->addData("sampai", $_POST['sampai']);	
	$serv->execute();
	$data=$serv->getContent();
	$res=new ResponsePackage();
	$res->setStatus(ResponsePackage::$STATUS_OK);
	$res->setContent($data);
	echo json_encode($res->getPackage());
	return;
}


if(isset($_POST['super_command']) && $_POST['super_command']=="limit") {
	$data=array();
	$response=new ServiceConsumer($db, "get_patient_lama_rawat");
	$response->addData("command", "list");
	$response->addData("dari", $_POST['dari']);
	$response->addData("sampai", $_POST['sampai']);
	$response->addData("halaman", $_POST['halaman']);	
	$response->setMode(ServiceConsumer::$SINGLE_MODE);
	$response->execute();
	$list=$response->getContent();
	$dbtable=new DBTable($db, "smis_mr_pasien_pulang");
	$diagnosa=new DBTable($db,"smis_mr_diagnosa");
	$operasi=new DBTable($db,"smis_mr_operasi");
	loadLibrary("smis-libs-function-time");
	foreach($list as $x){		
		$d['nrm']=$x['nrm'];
		$d['nama_pasien']=$x['nama_pasien'];
		$d['noreg']=$x['id'];
		$d['tgl_masuk']=$x['tanggal'];
		$d['dokter_pj']=$x['nama_dokter'];
		$d['tgl_pulang']=$x['tanggal_pulang'];
		$d['ruang']=$x['kamar_inap'];
        $d['carabayar']=$x['carabayar'];
        $d['carapulang']=$x['carapulang'];
        $d['n_perusahaan']=$x['n_perusahaan'];
        $d['n_asuransi']=$x['nama_asuransi'];
		$d['kunjungan']=$x['barulama']=="0"?"Baru":"Lama";
        $start=$_POST['dari'];
        $end=$_POST['sampai'];
        if($x['tanggal']>$start){
            $start=$x['tanggal'];
        }
        if($x['tanggal_pulang']<$end && $x['tanggal_pulang']!="0000-00-00 00:00:00"){
            $end=$x['tanggal_pulang'];
        }
        $durasi=day_diff_only($start,$end);
        $d['durasi']=$durasi;
        
		$diag=$diagnosa->select(array("noreg_pasien"=>$x['id']));
		if($diag!=NULL){
			$d['diagnosa']=$diag->diagnosa;
			$d['kode_diagnosa']=$diag->kode_icd;
            $d['kasus']=$diag->kasus;
		}
		$op=$operasi->select(array("noreg_pasien"=>$x['id']));
		if($op!=NULL){
			$d['dokter_bedah']=$op->dokter_operator;
			$d['dokter_anastesi']=$op->dokter_anastesi;
			$d['tgl_tindakan']=$op->tanggal_mulai;
			$d['tindakan']=$op->tindakan;
			$d['bedah']="1";
		}		
		if($op==NULL || $op->dokter_operator=="" || $op->dokter_anastesi=="" || $op->tanggal_mulai=="0000-00-00 00:00:00"){
			$o=new ServiceConsumer($db, "get_ok",NULL,$_POST['ruang_ok']);
			$o->addData("noreg_pasien", $x['id']);
			$o->setMode(ServiceConsumer::$SINGLE_MODE);
			$o->execute();
			$okp=$o->getContent();
			if($okp!=NULL){
				$d['dokter_bedah']=$okp['nama_operator_satu'];
				$d['dokter_anastesi']=$okp['nama_anastesi'];
				$d['tgl_tindakan']=$okp['waktu'];
				$d['bedah']="1";
			}
		}
		$dbtable->insert($d);		
	}
	$res=new ResponsePackage();
	$res->setStatus(ResponsePackage::$STATUS_OK);
	$res->setContent($list);
	echo json_encode($res->getPackage());
	return;
}

$header=array(	"No.","NRM","Nama","Ruang","Masuk","Pulang",
				"ICD10","Diagnosa","Dokter DPJP","Kasus Bedah","ICD9","Tindakan",
				"Tanggal Tindakan","Dokter Bedah","Dokter Anastesi","Durasi");
$uitable=new Table($header);
$uitable->setName("lap_lama_rawat")
		->setActionEnable(false)
		->setFooterVisible(false);

if(isset($_POST['command']) && $_POST['command']=="excel"){
	require_once "medical_record/snippet/create_excel_lap_lama_rawat.php";
    return;
}

if(isset($_POST['command']) && $_POST['command']!=""){
	$dbtable=new DBTable($db, "smis_mr_pasien_pulang");
	$dbtable->setOrder(" tgl_pulang ASC");
	$dbtable->setShowAll(true);
	
	$adapter=new SimpleAdapter();
	$adapter->setUseNumber(true, "No.","back.");
	$adapter->add("NRM", "nrm","digit8");
	$adapter->add("Nama", "nama_pasien");
	$adapter->add("Ruang", "ruang","unslug");
	$adapter->add("ICD10", "kode_diagnosa");
	$adapter->add("Diagnosa", "diagnosa");
	$adapter->add("Dokter DPJP", "dokter_pj");
	$adapter->add("Kasus Bedah", "bedah","trivial_1_<i class='fa fa-check'></i>_");
	$adapter->add("ICD9", "kode_tindakan");
	$adapter->add("Tindakan", "tindakan");
	$adapter->add("Tanggal Tindakan", "tgl_tindakan","date d M Y");
	$adapter->add("Dokter Bedah", "dokter_bedah");
	$adapter->add("Dokter Anastesi", "dokter_anastesi");
	$adapter->add("Masuk", "tgl_masuk","date d M Y H:i");
	$adapter->add("Pulang", "tgl_pulang","date d M Y H:i");
    $adapter->add("Durasi", "durasi");
	
	$dbres=new DBResponder($dbtable, $uitable, $adapter);
	$hasil=$dbres->command($_POST['command']);
	echo json_encode($hasil);
	return;
}

$detail=new OptionBuilder();
$detail->add("Hilangkan Detail Asuransi","0",1);
$detail->add("Tampilkan Detail Asuransi","1",0);

$separator=new OptionBuilder();
$separator->add("Colon",",",1);
$separator->add("Semi Colon",";",0);

$uitable->clearContent();
$uitable->addModal("dari", "date", "Dari", "")
		->addModal("sampai", "date", "Sampai", "")
		->addModal("separator", "select","Separator", $separator->getContent())
        ->addModal("detail", "select","Detail", $detail->getContent())
		->addModal("ruang_ok", "text", "Ruang Operasi", getSettings($db,"mr-slug-ruang-operasi",""),"",null,true);
$action=new Button("","","View");
$action->setClass("btn-primary")
	   ->setIsButton(Button::$ICONIC)
	   ->setIcon("icon-white fa fa-circle-o-notch")
	   ->setAction("lap_lama_rawat.rekaptotal()");
$view=new Button("","","View");
$view->setClass("btn-primary")
		->setIsButton(Button::$ICONIC)
		->setIcon("fa fa-refresh")
		->setAction("lap_lama_rawat.view()");
$excel=new Button("","","Excel");
$excel->setClass("btn-primary")
	->setIsButton(Button::$ICONIC)
	->setIcon("fa fa-file-excel-o")
	->setAction("lap_lama_rawat.excel()");

$btng=new ButtonGroup("");
$btng->addButton($action);
$btng->addButton($view);
$btng->addButton($excel);

$form=$uitable
	  ->getModal()
	  ->setTitle("Medical Record")
	  ->getForm()
	  ->addElement("",$btng);

$close=new Button("", "", "Batal");
$close	->addClass("btn-primary")
		->setIsButton(Button::$ICONIC_TEXT)
		->setIcon("fa fa-close")
		->setAction("lap_lama_rawat.batal()");

$load=new LoadingBar("rekap_lap_lama_rawat_bar", "");
$modal=new Modal("rekap_lap_lama_rawat_modal", "", "Processing...");
$modal	->addHTML($load->getHtml(),"after")
		->addFooter($close);

echo $form->getHtml();
echo "<div class='clear'></div>";
echo "<div id='result_lap_lama_rawat'>".$uitable->getHtml()."</div>";
echo $modal->getHtml();
echo addJS("framework/bootstrap/js/bootstrap-datepicker.js");
echo addJS("framework/smis/js/table_action.js");
echo addCSS("framework/bootstrap/css/datepicker.css");
echo addJS ( "base-js/smis-base-loading.js");
?>

<script type="text/javascript">
	var lap_lama_rawat;
	var lap_lama_rawat_karyawan;
	var lap_lama_rawat_data;
	var IS_lap_lama_rawat_RUNNING;
	$(document).ready(function(){
		$('.mydate').datepicker();
		lap_lama_rawat=new TableAction("lap_lama_rawat","medical_record","lap_lama_rawat",new Array());
		lap_lama_rawat.addRegulerData=function(data){
			data['dari']=$("#lap_lama_rawat_dari").val();
			data['sampai']=$("#lap_lama_rawat_sampai").val();
			data['ruang_ok']=$("#lap_lama_rawat_ruang_ok").val();
			data['separator']=$("#lap_lama_rawat_separator").val();
            data['detail']=$("#lap_lama_rawat_detail").val();
			$("#dari_table_lap_lama_rawat").html(getFormattedDate(data['dari']));
			$("#sampai_table_lap_lama_rawat").html(getFormattedDate(data['sampai']));			
			return data;
		};

		lap_lama_rawat.batal=function(){
			IS_lap_lama_rawat_RUNNING=false;
			$("#rekap_lap_lama_rawat_modal").modal("hide");
		};
		
		lap_lama_rawat.afterview=function(json){
			if(json!=null){
				$("#kode_table_lap_lama_rawat").html(json.nomor);
				$("#waktu_table_lap_lama_rawat").html(json.waktu);
				lap_lama_rawat_data=json;
			}
		};

		lap_lama_rawat.rekaptotal=function(){
			if(IS_lap_lama_rawat_RUNNING) return;
			$("#rekap_lap_lama_rawat_bar").sload("true","Fetching total data",0);
			$("#rekap_lap_lama_rawat_modal").modal("show");
			IS_lap_lama_rawat_RUNNING=true;
			var d=this.getRegulerData();
			d['super_command']="total";
			$.post("",d,function(res){
				var all=getContent(res);
				if(all!=null) {
					var total=Number(all);
					lap_lama_rawat.rekaploop(0,total);
				} else {
					$("#rekap_lap_lama_rawat_modal").modal("hide");
					IS_lap_lama_rawat_RUNNING=false;
				}
			});
		};

		lap_lama_rawat.excel=function(){
			var d=this.getRegulerData();
			d['command']="excel";
			download(d);
		};

		lap_lama_rawat.rekaploop=function(current,total){
			if(current>=total || !IS_lap_lama_rawat_RUNNING) {
				$("#rekap_lap_lama_rawat_modal").modal("hide");
				IS_lap_lama_rawat_RUNNING=false;
				lap_lama_rawat.view();
				return;
			}
			var d=this.getRegulerData();
			d['super_command']="limit";
			d['halaman']=current;
			$.post("",d,function(res){
				var ct=getContent(res);
				var u=ct[0]['nama_pasien']+"  "+ct[0]['nrm'];
				$("#rekap_lap_lama_rawat_bar").sload("true"," Processing - "+u+"... [ "+(current+1)+" / "+total+" ] ",(current*100/total));
				setTimeout(function(){lap_lama_rawat.rekaploop(++current,total)},300);
			});
		};
				
	});
</script>