<?php

global $db;
require_once "smis-base/smis-include-service-consumer.php";
$uitable = new Table(array("Poli/Ruangan","Jan","Feb","Mar","Apr","Mei","Jun","Jul","Agu","Sep","Okt","Nop","Des","Total"),"");
$uitable->setName('lap_kunjungan_rawat_jalan');
$uitable->setAction(false);
$uitable->setFooterVisible(false);


if (isset($_POST ['command']) && $_POST ['command'] != "") {
    if($_POST['command'] == 'list') {
        if(isset($_POST['sumber']) && $_POST['sumber'] != "") {
            $serv=new ServiceConsumer($db,"get_total_patient_in_year_rg", NULL, "registration");
            $serv->addData("tahun",$_POST['tahun']);
            $serv->execute();
            $data=$serv->getContent();
            $arr = array();
            if(isset($_POST['ruangan']) && $_POST['ruangan'] != '') {
                $i = 0;
                foreach($data as $key => $value) {            
                    if($data[$key]['status'] != 'deactived' && $data[$key]['ruangan'] == $_POST['ruangan']){
                        $arr[] = $data[$key];
                    }
                    $i++;
                }
            } else {
                $i = 0;
                foreach($data as $key => $value) {            
                    if($data[$key]['status'] != 'deactived'){
                        $arr[] = $data[$key];
                    }
                    $i++;
                }
            }
            $adapter=new SimpleAdapter();
            $adapter->add("Poli/Ruangan","ruangan","unslug");
            $adapter->add("Jan","jan");
            $adapter->add("Feb","feb");
            $adapter->add("Mar","mar");
            $adapter->add("Apr","apr");
            $adapter->add("Mei","mei");
            $adapter->add("Jun","jun");
            $adapter->add("Jul","jul");
            $adapter->add("Agu","ags");
            $adapter->add("Sep","sep");
            $adapter->add("Okt","okt");
            $adapter->add("Nop","nov");
            $adapter->add("Des","des");
            $adapter->add("Total","total");
            $ready=$adapter->getContent($arr);
            
            $uitable->setContent($ready);
            $content['list'] = $uitable->getBodyContent();
            $response = new ResponsePackage ();
            $response->setAlertVisible ( false );
            $response->setStatus ( ResponsePackage::$STATUS_OK );
            $response->setContent($content);
            echo json_encode ( $response->getPackage () );
            return;
        } else{
            $serv=new ServiceConsumer($db,"get_kunjungan_rj");
            $serv->addData("tahun",$_POST['tahun']);
            $serv->setMode(ServiceConsumer::$CLEAN_BOTH_JOIN_SINGLE_ARRAY);
            $serv->execute();
            $data=$serv->getContent();
            $arr = array();
            if(isset($_POST['ruangan']) && $_POST['ruangan'] != '') {
                $i = 0;
                foreach($data as $key => $value) {            
                    if($data[$key]['urji'] != 'URI' && $data[$key]['ruangan'] == $_POST['ruangan']){
                        $arr[] = $data[$key];
                    }
                    $i++;
                }
            } else {
                $i = 0;
                foreach($data as $key => $value) {            
                    if($data[$key]['urji'] != 'URI'){
                        $arr[] = $data[$key];
                    }
                    $i++;
                }
            }
            $adapter=new SimpleAdapter();
            $adapter->add("Poli/Ruangan","ruangan","unslug");
            $adapter->add("Jan","jan");
            $adapter->add("Feb","feb");
            $adapter->add("Mar","mar");
            $adapter->add("Apr","apr");
            $adapter->add("Mei","mei");
            $adapter->add("Jun","jun");
            $adapter->add("Jul","jul");
            $adapter->add("Agu","ags");
            $adapter->add("Sep","sep");
            $adapter->add("Okt","okt");
            $adapter->add("Nop","nov");
            $adapter->add("Des","des");
            $adapter->add("Total","total");
            $ready=$adapter->getContent($arr);
            
            $uitable->setContent($ready);
            $content['list'] = $uitable->getBodyContent();
            $response = new ResponsePackage ();
            $response->setAlertVisible ( false );
            $response->setStatus ( ResponsePackage::$STATUS_OK );
            $response->setContent($content);
            echo json_encode ( $response->getPackage () );
            return;
        }
    }
    if($_POST['command'] == 'excel') {
        if(isset($_POST['sumber']) && $_POST['sumber'] != ""){
            $serv=new ServiceConsumer($db,"get_total_patient_in_year_rg", NULL, "registration");
            $serv->addData("tahun",$_POST['tahun']);
            $serv->execute();
            $data=$serv->getContent();
            $arr = array();
            if(isset($_POST['ruangan']) && $_POST['ruangan'] != '') {
                $i = 0;
                foreach($data as $key => $value) {            
                    if($data[$key]['status'] != 'deactived' && $data[$key]['ruangan'] == $_POST['ruangan']){
                        $arr[] = $data[$key];
                    }
                    $i++;
                }
            } else {
                $i = 0;
                foreach($data as $key => $value) {            
                    if($data[$key]['status'] != 'deactived'){
                        $arr[] = $data[$key];
                    }
                    $i++;
                }
            }
        } else {
            $serv=new ServiceConsumer($db,"get_kunjungan_rj");
            $serv->addData("tahun",$_POST['tahun']);
            $serv->setMode(ServiceConsumer::$CLEAN_BOTH_JOIN_SINGLE_ARRAY);
            $serv->execute();
            $data=$serv->getContent();
            $arr = array();
            if(isset($_POST['ruangan']) && $_POST['ruangan'] != '') {
                $i = 0;
                foreach($data as $key => $value) {            
                    if($data[$key]['urji'] != 'URI' && $data[$key]['ruangan'] == $_POST['ruangan']){
                        $arr[] = $data[$key];
                    }
                    $i++;
                }
            } else {
                $i = 0;
                foreach($data as $key => $value) {            
                    if($data[$key]['urji'] != 'URI'){
                        $arr[] = $data[$key];
                    }
                    $i++;
                }
            }
        }
        
        require_once ("smis-libs-out/php-excel/PHPExcel.php");
        $file = new PHPExcel ();
        $file->getProperties ()->setCreator ( "Rumah Sakit Syamrabu" );
        $file->getProperties ()->setLastModifiedBy ( "Rumah Sakit Bagian Administrator" );
        $file->getProperties ()->setTitle ( "Laporan Kunjungan Rawat Jalan" );
        $file->getProperties ()->setSubject ( "Laporan Kunjungan Rawat Jalan" );
        $file->getProperties ()->setDescription ( "Laporan Kunjungan Rawat Jalan Generated From system" );
        $file->getProperties ()->setKeywords ( "Laporan Kunjungan Rawat Jalan" );
        $file->getProperties ()->setCategory ( "Laporan Kunjungan Rawat Jalan" );
        $sheet = $file->getActiveSheet ();
        
        $index = 1;
        $sheet->setCellValue("A".$index,"LAPORAN JUMLAH KUNJUNGAN RAWAT JALAN");
        $sheet->mergeCells('A'.$index.':'.'O'.$index);
        $sheet->getStyle('A'.$index.':'.'O'.$index)->getFont()->setBold(true);
        $sheet->getStyle('A'.$index.':'.'O'.$index)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $index++;
        $sheet->setCellValue("A".$index,"TAHUN ".$_POST['tahun']."");
        $sheet->mergeCells('A'.$index.':'.'O'.$index);
        $sheet->getStyle('A'.$index.':'.'O'.$index)->getFont()->setBold(true);
        $sheet->getStyle('A'.$index.':'.'O'.$index)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $index = $index + 2;
        
        $sheet->setCellValue("A".$index,"No.");
        $sheet->setCellValue("B".$index,"Poli/Ruangan");
        $sheet->setCellValue("C".$index,"Januari");    
        $sheet->setCellValue("D".$index,"Februari");    
        $sheet->setCellValue("E".$index,"Maret");
        $sheet->setCellValue("F".$index,"April");
        $sheet->setCellValue("G".$index,"Mei");
        $sheet->setCellValue("H".$index,"Juni");
        $sheet->setCellValue("I".$index,"Juli");
        $sheet->setCellValue("J".$index,"Agustus");
        $sheet->setCellValue("K".$index,"September");
        $sheet->setCellValue("L".$index,"Oktober");
        $sheet->setCellValue("M".$index,"November");
        $sheet->setCellValue("N".$index,"Desember");
        $sheet->setCellValue("O".$index,"Total");
        $sheet->getStyle('A'.$index.':'.'O'.$index)->getFont()->setBold(true);
        $sheet->getStyle('A'.$index.':'.'O'.$index)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        
        $no = 1;
        foreach($arr as $x) {
            $index++;
            $sheet->setCellValue("A".$index, $no);
            $sheet->setCellValue("B".$index, ArrayAdapter::format("unslug", $x["ruangan"]));
            $sheet->setCellValue("C".$index, $x["jan"]);    
            $sheet->setCellValue("D".$index, $x["feb"]);    
            $sheet->setCellValue("E".$index, $x["mar"]);
            $sheet->setCellValue("F".$index, $x["apr"]);
            $sheet->setCellValue("G".$index, $x["mei"]);
            $sheet->setCellValue("H".$index, $x["jun"]);
            $sheet->setCellValue("I".$index, $x["jul"]);
            $sheet->setCellValue("J".$index, $x["ags"]);
            $sheet->setCellValue("K".$index, $x["sep"]);
            $sheet->setCellValue("L".$index, $x["okt"]);
            $sheet->setCellValue("M".$index, $x["nov"]);
            $sheet->setCellValue("N".$index, $x["des"]);
            $sheet->setCellValue("O".$index, $x["total"]);
            $sheet->getStyle('A'.$index.':'.'O'.$index)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $no++;
        }
        foreach(range('A','O') as $columnID) {
            $sheet->getColumnDimension($columnID)->setAutoSize(true);
        }
        
        header ( 'Content-Type: application/vnd.ms-excel' );
        header ( 'Content-Disposition: attachment;filename="Laporan Kunjungan Rawat Jalan.xls"' );
        header ( 'Cache-Control: max-age=0' );
        $writer = PHPExcel_IOFactory::createWriter ( $file, 'Excel5' );
        $writer->save ( 'php://output' );
		return;
    }
}

/*==============Filter Dropdown Sumber Data================*/
$sumber = array();
$sumber[0]['value'] = 'registration';
$sumber[0]['name'] = 'Dari Pendaftaran Saja';
$sumber[1]['value'] = '';
$sumber[1]['name'] = ' - SEMUA - ';
$uitable->addModal( "sumber", "select", "Sumber Data", $sumber );
/*========================================================*/

/*==============Filter Dropdown Tahun================*/
$tahun = new ServiceConsumer($db,"get_thn_kunjungan_rj");
$tahun->execute();
$content = $tahun->getContent();
$thn = array();
foreach($content as $con){
    $option = array();
    $option['value'] = $con["tahun"];
    $option['name'] = $con["tahun"];
    $thn[] =$option;
}
$uitable->addModal( "tahun", "select", "Tahun", $thn );
/*====================================================*/

/*================Filter Dropdown Ruangan-==============*/
$ruang = new ServiceConsumer($db, "get_total_patient_in_year");
$ruang->setMode(ServiceConsumer::$CLEAN_BOTH_JOIN_SINGLE_ARRAY);
$ruang->execute();
$content = $ruang->getContent();
$ruangan = array();
foreach($content as $key => $value) {
    if($content[$key]['urji'] != 'URI') {
        $option = array();
        $option['value'] = $content[$key]['ruangan'];
        $option['name'] = ArrayAdapter::format("unslug", $content[$key]['ruangan']);
        $ruangan[] = $option;
    }
}
$option['value']="";
$option['name']=" - SEMUA - ";
$ruangan[] = $option;
$uitable->addModal( "ruangan", "select", "Layanan", $ruangan );
/*=========================================================*/

$modal = $uitable->getModal ();
$form=$modal->getForm();

$v = new Button("", "", "");
$v->addClass("btn-primary")
		->setIsButton(Button::$ICONIC)
		->setIcon("fa fa-refresh")
		->setAction("lap_kunjungan_rawat_jalan.view()");
$form->addElement("", $v);

$excel = new Button("", "", "");
$excel->addClass("btn-primary")
		->setIsButton(Button::$ICONIC)
		->setIcon("fa fa-file-excel-o")
		->setAction("lap_kunjungan_rawat_jalan.excel()");
$form->addElement("", $excel);

echo $form->getHtml();
echo $uitable->getHtml ();
echo $modal->getHtml ();

echo addJS ( "framework/smis/js/table_action.js" );

?>

<script type="text/javascript">
var lap_kunjungan_rawat_jalan;
$(document).ready(function(){
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
	var column=new Array();
	lap_kunjungan_rawat_jalan = new TableAction("lap_kunjungan_rawat_jalan","medical_record","lap_kunjungan_rawat_jalan",column);
    lap_kunjungan_rawat_jalan.addViewData = function(a) {
        a['sumber'] = $("#lap_kunjungan_rawat_jalan_sumber").val();
        a['tahun'] = $("#lap_kunjungan_rawat_jalan_tahun").val();
        a['ruangan'] = $("#lap_kunjungan_rawat_jalan_ruangan").val();
        return a;
    };
    
    lap_kunjungan_rawat_jalan.excel = function() {
        var data = this.getRegulerData();
        data['command'] = 'excel';
        data['sumber'] = $("#lap_kunjungan_rawat_jalan_sumber").val();
        data['tahun'] = $("#lap_kunjungan_rawat_jalan_tahun").val();
        data['ruangan'] = $("#lap_kunjungan_rawat_jalan_ruangan").val();
        postForm(data);
    }
});
</script>