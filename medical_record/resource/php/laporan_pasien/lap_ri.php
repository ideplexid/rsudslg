<?php 
global $db;
global $user;
require_once 'smis-base/smis-include-service-consumer.php';
if(isset($_POST['super_command']) && $_POST['super_command']=="total") {
	require_once 'smis-libs-class/ServiceProviderList.php';
	$query=" DELETE FROM smis_mr_lap_ri";
    $db->get_var($query);
    //$dbtable=new DBTable($db, "smis_mr_lap_ri");
	//$dbtable->truncate();
	$serv=new ServiceConsumer($db, "get_patient_rawat_inap");
	$serv->addData("command", "count");
	$serv->addData("dari", $_POST['dari']);
	$serv->addData("sampai", $_POST['sampai']);	
	$serv->execute();
	$data=$serv->getContent();
	$res=new ResponsePackage();
	$res->setStatus(ResponsePackage::$STATUS_OK);
	$res->setContent($data);
	echo json_encode($res->getPackage());
	return;
}


if(isset($_POST['super_command']) && $_POST['super_command']=="limit") {
	$data=array();
	$response=new ServiceConsumer($db, "get_patient_rawat_inap");
	$response->addData("command", "list");
	$response->addData("dari", $_POST['dari']);
	$response->addData("sampai", $_POST['sampai']);
	$response->addData("halaman", $_POST['halaman']);	
	$response->setMode(ServiceConsumer::$SINGLE_MODE);
	$response->execute();
	$list=$response->getContent();
	$dbtable=new DBTable($db, "smis_mr_lap_ri");
	$diagnosa=new DBTable($db,"smis_mr_diagnosa");
	$igd=getSettings($db, "smis-rs-igd-mr", "igd");
	$icu=getSettings($db, "smis-rs-icu-mr", "icu");
	$ok=getSettings($db, "smis-rs-ok-mr", "ok");
	$vk=getSettings($db, "smis-rs-vk-mr", "vk");
	
	foreach($list as $x){		
		$d['nrm_pasien']=$x['nrm'];
		$d['nama_pasien']=$x['nama_pasien'];
		$d['tgl_kunj']=$x['tanggal'];
		$d['dokter']=$x['nama_dokter'];
		$d['tgl_keluar']=$x['tanggal_pulang'];
		$d['ruangan']=$x['kamar_inap'];
		$d['barulama']=$x['barulama'];
		$d['jk']=$x['kelamin'];
		$d['carabayar']=$x['carabayar']." - ".$x['nama_asuransi'];
		$d['carapulang']=$x['carapulang'];		
		$d['kelurahan']=$x['nama_kelurahan'];
		$d['kecamatan']=$x['nama_kecamatan'];
		$d['kabupaten']=$x['nama_kabupaten'];
		$d['propinsi']=$x['nama_provinsi'];
		
		$query=" SELECT diagnosa FROM smis_mr_diagnosa WHERE  
				ruangan='".$igd."' AND prop!='del' AND noreg_pasien='".$x['id']."'
                ORDER BY tanggal DESC";
		$d['diagnosa_igd']=$db->get_var($query);
		
		$query=" SELECT diagnosa FROM smis_mr_diagnosa WHERE
				ruangan!='".$igd."' 
				AND ruangan!='".$ok."' 
				AND ruangan NOT LIKE 'poli%'
				AND prop!='del' AND noreg_pasien='".$x['id']."' 
                ORDER BY tanggal DESC";
		$d['diagnosa_utama']=$db->get_var($query);
		$dbtable->insert($d);		
	}
	
	$res=new ResponsePackage();
	$res->setStatus(ResponsePackage::$STATUS_OK);
	$res->setContent($list);
	echo json_encode($res->getPackage());
	return;
}

$header=array(	"No.","Tgl Kunj","Tgl Keluar","NRM","Nama",
				"Diagnosa IGD","Diagnosa Utama","DPJP","L/B","JK",
				"Kelurahan","Kecamatan","Kabupaten","Propinsi",
				"Carapulang","Carabayar");
$uitable=new Table($header);
$uitable->setName("lap_ri")
		->setActionEnable(false)
		->setFooterVisible(false);

if(isset($_POST['command']) && $_POST['command']!=""){
	$dbtable=new DBTable($db, "smis_mr_lap_ri");
	$dbtable->setOrder(" tgl_kunj ASC");
	$dbtable->setShowAll(true);
	
	$adapter=new SimpleAdapter();
	$adapter->setUseNumber(true, "No.","back.");
	$adapter->add("NRM", "nrm_pasien","only-digit8");
	$adapter->add("Nama", "nama_pasien");
	$adapter->add("Ruang", "ruang","unslug");
	$adapter->add("Tgl Kunj", "tgl_kunj","date d-M-Y H:i");
	$adapter->add("Tgl Keluar", "tgl_keluar","date d-M-Y H:i");
	$adapter->add("Diagnosa IGD", "diagnosa_igd");
	$adapter->add("Diagnosa Utama", "diagnosa_utama");
	$adapter->add("DPJP", "dokter");	
	$adapter->add("L/B", "barulama","trivial_0_Baru_Lama");
	$adapter->add("JK", "jk","trivial_0_Laki_Pria");
	$adapter->add("Kelurahan", "kelurahan");
	$adapter->add("Kecamatan", "kecamatan");
	$adapter->add("Kabupaten", "kabupaten");
	$adapter->add("Propinsi", "propinsi");	
	$adapter->add("Carabayar", "carabayar");
	$adapter->add("Carapulang", "carapulang");
	
	require_once "medical_record/class/responder/LapRIResponder.php";
	$dbres = new LapRIResponder($dbtable, $uitable, $adapter);
	$hasil=$dbres->command($_POST['command']);
	echo json_encode($hasil);
	return;
}

$uitable->clearContent();
$uitable->addModal("dari", "date", "Dari", "")
		->addModal("sampai", "date", "Sampai", "");
$action=new Button("","","View");
$action->setClass("btn-primary")
	   ->setIsButton(Button::$ICONIC)
	   ->setIcon("icon-white fa fa-circle-o-notch")
	   ->setAction("lap_ri.rekaptotal()");
$view=new Button("","","View");
$view->setClass("btn-primary")
		->setIsButton(Button::$ICONIC)
		->setIcon("fa fa-refresh")
		->setAction("lap_ri.view()");
$excel=new Button("","","Excel");
$excel->setClass("btn-primary")
	->setIsButton(Button::$ICONIC)
	->setIcon("fa fa-file-excel-o")
	->setAction("lap_ri.excel()");

$btng=new ButtonGroup("");
$btng->addButton($action);
$btng->addButton($view);
$btng->addButton($excel);

$form=$uitable
	  ->getModal()
	  ->setTitle("Medical Record")
	  ->getForm()
	  ->addElement("",$btng);

$close=new Button("", "", "Batal");
$close	->addClass("btn-primary")
		->setIsButton(Button::$ICONIC_TEXT)
		->setIcon("fa fa-close")
		->setAction("lap_ri.batal()");

$load=new LoadingBar("rekap_lap_ri_bar", "");
$modal=new Modal("rekap_lap_ri_modal", "", "Processing...");
$modal	->addHTML($load->getHtml(),"after")
		->addFooter($close);

echo $form->getHtml();
echo "<div class='clear'></div>";
echo "<div id='result_lap_ri'>".$uitable->getHtml()."</div>";
echo $modal->getHtml();
echo addJS("framework/bootstrap/js/bootstrap-datepicker.js");
echo addJS("framework/smis/js/table_action.js");
echo addCSS("framework/bootstrap/css/datepicker.css");
echo addJS ( "base-js/smis-base-loading.js");
?>

<script type="text/javascript">
	var lap_ri;
	var lap_ri_karyawan;
	var lap_ri_data;
	var IS_lap_ri_RUNNING;
	$(document).ready(function(){
		$('.mydate').datepicker();
		lap_ri=new TableAction("lap_ri","medical_record","lap_ri",new Array());
		lap_ri.addRegulerData=function(data){
			data['dari']=$("#lap_ri_dari").val();
			data['sampai']=$("#lap_ri_sampai").val();
			$("#dari_table_lap_ri").html(getFormattedDate(data['dari']));
			$("#sampai_table_lap_ri").html(getFormattedDate(data['sampai']));			
			return data;
		};

		lap_ri.batal=function(){
			IS_lap_ri_RUNNING=false;
			$("#rekap_lap_ri_modal").modal("hide");
		};
		
		lap_ri.afterview=function(json){
			if(json!=null){
				$("#kode_table_lap_ri").html(json.nomor);
				$("#waktu_table_lap_ri").html(json.waktu);
				lap_ri_data=json;
			}
		};

		lap_ri.rekaptotal=function(){
			if(IS_lap_ri_RUNNING) return;
			$("#rekap_lap_ri_bar").sload("true","Fetching total data",0);
			$("#rekap_lap_ri_modal").modal("show");
			IS_lap_ri_RUNNING=true;
			var d=this.getRegulerData();
			d['super_command']="total";
			$.post("",d,function(res){
				var all=getContent(res);
				if(all!=null) {
					var total=Number(all);
					lap_ri.rekaploop(0,total);
				} else {
					$("#rekap_lap_ri_modal").modal("hide");
					IS_lap_ri_RUNNING=false;
				}
			});
		};

		lap_ri.excel=function(){
			var d=this.getRegulerData();
			d['command']="excel";
			download(d);
		};

		lap_ri.rekaploop=function(current,total){
			if(current>=total || !IS_lap_ri_RUNNING) {
				$("#rekap_lap_ri_modal").modal("hide");
				IS_lap_ri_RUNNING=false;
				lap_ri.view();
				return;
			}
			var d=this.getRegulerData();
			d['super_command']="limit";
			d['halaman']=current;
			$.post("",d,function(res){
				var ct=getContent(res);
				var u=ct[0]['nama_pasien']+"  "+ct[0]['nrm'];
				$("#rekap_lap_ri_bar").sload("true"," Processing - "+u+"... [ "+(current+1)+" / "+total+" ] ",(current*100/total));
				setTimeout(function(){lap_ri.rekaploop(++current,total)},300);
			});
		};
				
	});
</script>