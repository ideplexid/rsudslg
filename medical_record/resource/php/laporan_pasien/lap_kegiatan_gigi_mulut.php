<?php
require_once "smis-base/smis-include-service-consumer.php";

global $db;
$serv=new ServiceConsumer($db,"get_jenis_patient",NULL,"registration");
$serv->execute();
$data=$serv->getContent();

$header = array ();
$header [] = "No.";
$header [] = "Layanan";
$headname0="";
$headname1="";

foreach($data as $x){
    $header [] = "Baru ".$x['value'];
    $header [] = "Lama ".$x['value'];
    $header [] = "Jumlah ".$x['value'];
    $headname0.="<th colspan='3'>".ArrayAdapter::slugFormat("unslug",$x['value'])."</th>";
    $headname1.="<th>Baru</th><th>Lama</th><th>Baru + Lama</th>";
}
$header [] = "Jumlah";
$uitable = new Table ( $header, "", NULL, false );
$uitable->setHeaderVisible ( false );
$uitable->setFooterVisible ( false );
$uitable->setName("lap_kegiatan_gigi_mulut");

$head0="<tr>
            <th rowspan='2'>No.</th>
            <th rowspan='2'>Layanan</th>
            $headname0
            <th rowspan='2'>Jumlah</th>
        </tr>";
$head1="<tr>
            $headname1
        </tr>";
$uitable->addHeader("after",$head0);
$uitable->addHeader("after",$head1);
if (isset ( $_POST ['command'] )) {
	require_once ("medical_record/class/adapter/LapKegiatanGigiMulutAdapter.php");
	require_once ("medical_record/class/responder/LapKegiatanGigiMulutResponder.php");
	$adapter = new LapKegiatanGigiMulutAdapter();
	$dbtable = new DBTable ( $db, "smis_mr_gigimulut" );
	
	if (isset ( $_POST ['dari'] ) && isset ( $_POST ['sampai'] ) && $_POST ['dari'] != "" && $_POST ['sampai'] != "") {
		$dbtable->addCustomKriteria ( "'" . $_POST ['dari'] . "'", "<=tanggal" );
		$dbtable->addCustomKriteria ( "'" . $_POST ['sampai'] . "'", ">tanggal" );
		$dbtable->setShowAll ( true );
	}
    if(isset ( $_POST ['origin'] ) && $_POST ['origin'] != "") {
        $dbtable->addCustomKriteria ( " origin ", "='".$_POST['origin']."'" );
    }
    
	$dbtable->setOrder ( " tanggal ASC " );
    $dbres = new LapKegiatanGigiMulutResponder ( $dbtable, $uitable, $adapter );
	$data = $dbres->command ( $_POST ['command'] );
	echo json_encode ( $data );
	return;
}

$query="SELECT DISTINCT origin FROM smis_mr_diagnosa WHERE prop!='del' ";
$option=new OptionBuilder();
$option->add(" - Semua - ","",1);

$res=$db->get_result($query);
foreach($res as $x){
    if($x->origin==""){
        continue;
    }
    $option->add(ArrayAdapter::slugFormat("unslug",$x->origin),$x->origin);
}

$uitable->addModal ( "dari", "datetime", "Dari", "" );
$uitable->addModal ( "sampai", "datetime", "Sampai", "" );
$uitable->addModal ( "origin", "select", "Asal", $option->getContent() );
$modal = $uitable->getModal ();
$form = $modal->getForm ();

$button = new Button ( "", "", "" );
$button->setIsButton ( Button::$ICONIC );
$button->setIcon ( "fa fa-refresh" );
$button->setClass("btn-primary");
$button->setAction ( "lap_kegiatan_gigi_mulut.view()" );
$form->addElement ( "", $button );
$button = new Button ( "", "", "" );
$button->setIsButton ( Button::$ICONIC );
$button->setClass("btn-primary");
$button->setIcon ( "fa fa-print" );
$button->setAction ( "smis_print($('#print_table_lap_kegiatan_gigi_mulut').html())" );
$form->addElement ( "", $button );
$excel = new Button ( "", "", "" );
$excel->setIsButton ( Button::$ICONIC );
$excel->setIcon ( "fa fa-file-excel-o");
$excel->setAction ( "lap_kegiatan_gigi_mulut.excel()" );
$excel->setClass("btn-primary");
$form->addElement ( "", $excel );
echo $form->getHtml ();

echo $uitable->getHtml ();
echo addJS ( "framework/smis/js/table_action.js" );
echo addJS ( "framework/bootstrap/js/bootstrap-datetimepicker.js" );
echo addCSS ( "framework/bootstrap/css/bootstrap-datetimepicker.css" );

?>
<script type="text/javascript">
var lap_kegiatan_gigi_mulut;
//var employee;
$(document).ready(function(){
	$('.mydatetime').datetimepicker({minuteStep:1});
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
	var column=new Array('id','nama','keterangan','slug');
	lap_kegiatan_gigi_mulut=new TableAction("lap_kegiatan_gigi_mulut","medical_record","lap_kegiatan_gigi_mulut",column);
	lap_kegiatan_gigi_mulut.getRegulerData=function(){
		var reg_data={	
				page:this.page,
				action:this.action,
				super_command:this.super_command,
				prototype_name:this.prototype_name,
				prototype_slug:this.prototype_slug,
				prototype_implement:this.prototype_implement,
				dari:$("#"+this.prefix+"_dari").val(),
				sampai:$("#"+this.prefix+"_sampai").val(),
				origin:$("#"+this.prefix+"_origin").val()
				};
		return reg_data;
	};
	
});
</script>

<style type="text/css">
#table_lap_kegiatan_gigi_mulut {
	font-size: 12px;
}
</style>