<?php 
//YUNAN
setChangeCookie ( false );
$header = array ();
$header [] = "TANGGAL";
$header [] = "EXO TETAP";
$header [] = "EXO SULUNG";
$header [] = "TAMBAL.T";
$header [] = "TAMBAL.S";
$header [] = "PREMED";
$header [] = "GUSI";
$header [] = "ABSES";
$header [] = "SCALLING";
$header [] = "PEREMPUAN";
$header [] = "LAKI-LAKI";

$uitable = new Table ( $header, "", NULL, false );
$uitable->setName ( "lap_kasus" );
$uitable->setHeaderVisible ( false );
$uitable->setFooterVisible ( false );

if( isset ($_POST['command']))
{
    require_once ("medical_record/class/adapter/LapKasusAdapter.php");
    $adapter = new LapKasusAdapter();
    $dbtable = new DBTable ( $db, "smis_mr_diagnosa" );
    
    if (isset ( $_POST ['dari'] ) && isset ( $_POST ['sampai'] ) && $_POST ['dari'] != "" && $_POST ['sampai'] != "")
    {
        $dbtable->addCustomKriteria ( "'" . $_POST ['dari'] . "'", "<=tanggal" );
		$dbtable->addCustomKriteria ( "'" . $_POST ['sampai'] . "'", ">tanggal" );
		$dbtable->setShowAll ( true );
    }
    
    $dbtable->setOrder ( " tanggal ASC " );
	$dbres = new DBResponder ( $dbtable, $uitable, $adapter );
	$data = $dbres->command ( $_POST ['command'] );
	echo json_encode ( $data );
	return;
}

$uitable->addModal ( "dari", "datetime", "Dari", "" );
$uitable->addModal ( "sampai", "datetime", "Sampai", "" );
$uitable->addModal ( "ruangan", "hidden", "", "igd" );

$modal = $uitable->getModal ();
$form = $modal->getForm ();

$button = new Button ( "", "", "" );
$button->setIsButton ( Button::$ICONIC );
$button->setIcon ( "fa fa-refresh" );
$button->setClass("btn-primary");
$button->setAction ( "lap_igd_triage.view()" );
$form->addElement ( "", $button );
//$button = new Button ( "", "", "" );
//$button->setIsButton ( Button::$ICONIC );
//$button->setClass("btn-primary");
//$button->setIcon ( "fa fa-print" );
//$button->setAction ( "smis_print($('#print_table_lap_igd_triage').html())" );
//$form->addElement ( "", $button );
echo $form->getHtml ();

echo $uitable->getHtml ();
echo addJS ( "framework/smis/js/table_action.js" );
echo addJS ( "framework/bootstrap/js/bootstrap-datetimepicker.js" );
echo addCSS ( "framework/bootstrap/css/bootstrap-datetimepicker.css" );
?>