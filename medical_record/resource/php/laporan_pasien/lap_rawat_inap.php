<?php 
global $db;
global $user;
require_once 'smis-base/smis-include-service-consumer.php';
if(isset($_POST['super_command']) && $_POST['super_command']=="total") {
	require_once 'smis-libs-class/ServiceProviderList.php';
	$dbtable=new DBTable($db, "smis_mr_rawat_inap");
	$dbtable->truncate();
	$serv=new ServiceConsumer($db, "get_patient_rawat_inap");
	$serv->addData("command", "count");
	$serv->addData("dari", $_POST['dari']);
	$serv->addData("sampai", $_POST['sampai']);	
	$serv->execute();
	$data=$serv->getContent();
	$res=new ResponsePackage();
	$res->setStatus(ResponsePackage::$STATUS_OK);
	$res->setContent($data);
	echo json_encode($res->getPackage());
	return;
}


if(isset($_POST['super_command']) && $_POST['super_command']=="limit") {
	$data=array();
	$response=new ServiceConsumer($db, "get_patient_rawat_inap");
	$response->addData("command", "list");
	$response->addData("dari", $_POST['dari']);
	$response->addData("sampai", $_POST['sampai']);
	$response->addData("halaman", $_POST['halaman']);	
	$response->setMode(ServiceConsumer::$SINGLE_MODE);
	$response->execute();
	$list=$response->getContent();
	$dbtable=new DBTable($db, "smis_mr_rawat_inap");
	$diagnosa=new DBTable($db,"smis_mr_diagnosa");
		
	foreach($list as $x){		
		$d['nrm_pasien']=$x['nrm'];
		$d['nama_pasien']=$x['nama_pasien'];
		$d['noreg_pasien']=$x['id'];
		$d['tanggal']=$x['tanggal'];
		$d['ruangan']=$x['kamar_inap'];
		$d['dokter_pj']=$x['nama_dokter'];
		
		$diag=$diagnosa->select(array("noreg_pasien"=>$x['id']));
		if($diag!=NULL){
			$d['diagnosa']=$diag->diagnosa;
		}
		$dbtable->insert($d);		
	}
	$res=new ResponsePackage();
	$res->setStatus(ResponsePackage::$STATUS_OK);
	$res->setContent($list);
	echo json_encode($res->getPackage());
	return;
}

$header=array("No.","No Reg","Tanggal","NRM","Nama","Ruang","Dokter DPJP","Diagnosa");
$uitable=new Table($header);
$uitable->setName("lap_rawat_inap")
		->setActionEnable(false)
		->setFooterVisible(false);

if(isset($_POST['command']) && $_POST['command']=="excel"){
	loadLibrary("smis-libs-function-export");
	$dbtable=new DBTable($db, "smis_mr_rawat_inap");
	$dbtable->setOrder(" tgl_pulang ASC");
	$dbtable->setFetchMethode(DBTable::$ARRAY_FETCH);
	$dbtable->setShowAll(true);
	
	$adapter=new SimpleAdapter();
	$adapter->setUseNumber(true, "No.","back.");
	$adapter->add("NRM", "nrm","only-digit8");
	$adapter->add("Nama", "nama_pasien");
	$adapter->add("Ruang", "ruang","unslug");
	$adapter->add("Masuk", "tgl_masuk","date d/m/Y");
	$adapter->add("Kode Diagnosa", "kode_diagnosa");
	$adapter->add("Diagnosa", "diagnosa");
	$adapter->add("Dokter DPJP", "dokter_pj");
	$dt=$dbtable->view("", "0");
	$ctx=$adapter->getContent($dt['data']);
	createExcel("pasien_pulang_inap", $ctx);
	return;
		
}

if(isset($_POST['command']) && $_POST['command']!=""){
	$dbtable=new DBTable($db, "smis_mr_rawat_inap");
	$dbtable->setOrder(" tanggal ASC");
	$dbtable->setShowAll(true);
	if(isset($_POST['ruangan']) && $_POST['ruangan']!="%"){
		$dbtable->addCustomKriteria(" kamar_inap  ", " ='".$_POST['ruangan']."' ");
	}
	
	$adapter=new SimpleAdapter();
	$adapter->setUseNumber(true, "No.","back.");
	$adapter->add("NRM", "nrm_pasien","digit8");
	$adapter->add("No Reg", "noreg_pasien","digit8");
	$adapter->add("Nama", "nama_pasien");
	$adapter->add("Ruang", "ruangan","unslug");
	$adapter->add("Tanggal", "tanggal","date d M Y");
	$adapter->add("Diagnosa", "diagnosa");	
	$adapter->add("Dokter DPJP", "dokter_pj");
	
	$dbres=new DBResponder($dbtable, $uitable, $adapter);
	$hasil=$dbres->command($_POST['command']);
	echo json_encode($hasil);
	return;
}

$urjip=new ServiceConsumer($db, "get_urjip",array());
$urjip->setMode(ServiceConsumer::$MULTIPLE_MODE);
$urjip->setCached(true,"get_urjip");
$urjip->execute();
$content=$urjip->getContent();
$ruangan=array();
$ruangan[]=array("name"=>" - - Semua - - ","value"=>"%","default"=>"1");
foreach ($content as $autonomous=>$ruang){
	foreach($ruang as $nama_ruang=>$jip){
		if($jip[$nama_ruang]=="URI"){
			$option=array();
			$option['value']=$nama_ruang;
			if ($jip['name'] != null)
				$option['name']=$jip['name'];
			else
				$option['name']=ArrayAdapter::format("unslug", $nama_ruang);
			$ruangan[]=$option;
		}
	}
}

$uitable->clearContent();
$uitable->addModal("dari", "date", "Dari", "")
		->addModal("sampai", "date", "Sampai", "")
		->addModal("ruangan", "select", "Ruangan", $ruangan);
$action=new Button("","","View");
$action->setClass("btn-primary")
	   ->setIsButton(Button::$ICONIC)
	   ->setIcon("icon-white fa fa-circle-o-notch")
	   ->setAction("lap_rawat_inap.rekaptotal()");
$view=new Button("","","View");
$view->setClass("btn-primary")
		->setIsButton(Button::$ICONIC)
		->setIcon("fa fa-refresh")
		->setAction("lap_rawat_inap.view()");

$btng=new ButtonGroup("");
$btng->addButton($action);
$btng->addButton($view);

$form=$uitable
	  ->getModal()
	  ->setTitle("Medical Record")
	  ->getForm()
	  ->addElement("",$btng);

$close=new Button("", "", "Batal");
$close	->addClass("btn-primary")
		->setIsButton(Button::$ICONIC_TEXT)
		->setIcon("fa fa-close")
		->setAction("lap_rawat_inap.batal()");

$load=new LoadingBar("rekap_lap_rawat_inap_bar", "");
$modal=new Modal("rekap_lap_rawat_inap_modal", "", "Processing...");
$modal	->addHTML($load->getHtml(),"after")
		->addFooter($close);

echo $form->getHtml();
echo "<div class='clear'></div>";
echo "<div id='result_lap_rawat_inap'>".$uitable->getHtml()."</div>";
echo $modal->getHtml();
echo addJS("framework/bootstrap/js/bootstrap-datepicker.js");
echo addJS("framework/smis/js/table_action.js");
echo addCSS("framework/bootstrap/css/datepicker.css");
echo addJS ( "base-js/smis-base-loading.js");
?>

<script type="text/javascript">
	var lap_rawat_inap;
	var lap_rawat_inap_karyawan;
	var lap_rawat_inap_data;
	var IS_lap_rawat_inap_RUNNING;
	$(document).ready(function(){
		$('.mydate').datepicker();
		lap_rawat_inap=new TableAction("lap_rawat_inap","medical_record","lap_rawat_inap",new Array());
		lap_rawat_inap.addRegulerData=function(data){
			data['dari']=$("#lap_rawat_inap_dari").val();
			data['sampai']=$("#lap_rawat_inap_sampai").val();
			data['ruangan']=$("#lap_rawat_inap_ruangan").val();
			$("#dari_table_lap_rawat_inap").html(getFormattedDate(data['dari']));
			$("#sampai_table_lap_rawat_inap").html(getFormattedDate(data['sampai']));			
			return data;
		};

		lap_rawat_inap.batal=function(){
			IS_lap_rawat_inap_RUNNING=false;
			$("#rekap_lap_rawat_inap_modal").modal("hide");
		};
		
		lap_rawat_inap.afterview=function(json){
			if(json!=null){
				$("#kode_table_lap_rawat_inap").html(json.nomor);
				$("#waktu_table_lap_rawat_inap").html(json.waktu);
				lap_rawat_inap_data=json;
			}
		};

		lap_rawat_inap.rekaptotal=function(){
			if(IS_lap_rawat_inap_RUNNING) return;
			$("#rekap_lap_rawat_inap_bar").sload("true","Fetching total data",0);
			$("#rekap_lap_rawat_inap_modal").modal("show");
			IS_lap_rawat_inap_RUNNING=true;
			var d=this.getRegulerData();
			d['super_command']="total";
			$.post("",d,function(res){
				var all=getContent(res);
				if(all!=null) {
					var total=Number(all);
					lap_rawat_inap.rekaploop(0,total);
				} else {
					$("#rekap_lap_rawat_inap_modal").modal("hide");
					IS_lap_rawat_inap_RUNNING=false;
				}
			});
		};

		lap_rawat_inap.excel=function(){
			var d=this.getRegulerData();
			d['command']="excel";
			download(d);
		};

		lap_rawat_inap.rekaploop=function(current,total){
			if(current>=total || !IS_lap_rawat_inap_RUNNING) {
				$("#rekap_lap_rawat_inap_modal").modal("hide");
				IS_lap_rawat_inap_RUNNING=false;
				lap_rawat_inap.view();
				return;
			}
			var d=this.getRegulerData();
			d['super_command']="limit";
			d['halaman']=current;
			$.post("",d,function(res){
				var ct=getContent(res);
				var u=ct[0]['nama_pasien']+"  "+ct[0]['nrm'];
				$("#rekap_lap_rawat_inap_bar").sload("true"," Processing - "+u+"... [ "+(current+1)+" / "+total+" ] ",(current*100/total));
				setTimeout(function(){lap_rawat_inap.rekaploop(++current,total)},300);
			});
		};
				
	});
</script>