<?php 
	require_once 'smis-libs-class/MasterTemplate.php';
	require_once 'smis-libs-class/MasterServiceTemplate.php';
    require_once 'smis-base/smis-include-service-consumer.php';
	global $db;
	
	$paktif=new MasterServiceTemplate($db, "get_patient_aktif", "medical_record", "pasien_aktif");
	$header=array("No.","Waktu","NRM","No Reg","Nama","Alamat","Layanan","Pembayaran","Kedatangan","URI/URJ");
	$paktif ->getUItable()
            ->setAction(false)
            ->setHeader($header);	
    $adapter=$paktif->getAdapter();
	$adapter->setUseNumber(true,"No.","back.")
            ->add("Waktu", "tanggal","date d M Y H:i:s")
			->add("No Reg", "id","digit8")
			->add("NRM", "nrm","digit8")
			->add("Alamat", "alamat_pasien")
			->add("Nama","nama_pasien")
			->add("Pembayaran","carabayar")
			->add("Layanan","jenislayanan","unslug")
			->add("Kedatangan","caradatang")
			->add("Kepulangan","carapulang","exception__Berlangsung")
			->add("URI/URJ","uri","trivial_1_Rawat Inap_Rawat Jalan");
	$btn=new Button("", "", "");
	$paktif->initialize();
?>