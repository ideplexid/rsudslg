<?php 
global $db;
global $user;
loadLibrary("smis-libs-function-medical");
require_once 'smis-base/smis-include-service-consumer.php';
if(isset($_POST['super_command']) && $_POST['super_command']=="total") {
	require_once 'smis-libs-class/ServiceProviderList.php';
	$dbtable=new DBTable($db, "smis_mr_pasien_pulang");
	$dbtable->truncate();
	$serv=new ServiceConsumer($db, "get_patient_masuk");
	$serv->addData("command", "count");
	$serv->addData("dari", $_POST['dari']);
	$serv->addData("sampai", $_POST['sampai']);	
	$serv->execute();
	$data=$serv->getContent();
	$res=new ResponsePackage();
	$res->setStatus(ResponsePackage::$STATUS_OK);
	$res->setContent($data);
	echo json_encode($res->getPackage());
	return;
}


if(isset($_POST['super_command']) && $_POST['super_command']=="limit") {
	$data=array();
	$response=new ServiceConsumer($db, "get_patient_masuk");
	$response->addData("command", "list");
	$response->addData("dari", $_POST['dari']);
	$response->addData("sampai", $_POST['sampai']);
	$response->addData("halaman", $_POST['halaman']);	
	$response->setMode(ServiceConsumer::$SINGLE_MODE);
	$response->execute();
	$list=$response->getContent();
	$dbtable=new DBTable($db, "smis_mr_pasien_pulang");
	$diagnosa=new DBTable($db,"smis_mr_diagnosa");
	$operasi=new DBTable($db,"smis_mr_operasi");
	
	//$ok=new DBTable($db,"smis_rwt_ok_ruang_operasi");
	
	foreach($list as $x){		
		$d['nrm']           = $x['nrm'];
		$d['nama_pasien']   = $x['nama_pasien'];
		$d['noreg']         = $x['id'];
		$d['tgl_masuk']     = $x['tanggal'];
		$d['uri']           = $x['uri'];
		$d['jk']            = $x['kelamin'];		
		$d['kelurahan']     = $x['nama_kelurahan'];
		$d['kecamatan']     = $x['nama_kecamatan'];
		$d['kabupaten']     = $x['nama_kabupaten'];
		$d['pekerjaan']     = $x['pekerjaan'];
		$d['agama']         = $x['agama'];
		$d['pendidikan']    = $x['pendidikan'];		
		$d['dokter_pj']     = $x['nama_dokter'];
		$d['tgl_pulang']    = $x['tanggal_pulang'];
		$d['ruang']=$x['uri']=="0"?$x['jenislayanan']:$x['kamar_inap'];
		$d['carabayar']     = $x['carabayar'];
        $d['carapulang']    = $x['carapulang'];
        $d['caradatang']    = $x['caradatang']=="Rujukan"?$x['rujukan']:$x['caradatang'];
        $d['n_perusahaan']  = $x['n_perusahaan'];
        $d['n_asuransi']    = $x['nama_asuransi'];
        $d['kunjungan']     = $x['barulama']=="0"?"Baru":"Lama";
        $d['rujukan']       = $x['rujukan'];
        $d['gol_umur']      = $x['gol_umur'];
        $d['profile_number']= $x['profile_number'];
        
		$diag=$diagnosa->select(array("noreg_pasien"=>$x['id']));
		if($diag!=NULL){
			$d['diagnosa']      = $diag->diagnosa;
			$d['kode_diagnosa'] = $diag->kode_icd;
			$d['kasus']         = $diag->kasus;
		}
		$op=$operasi->select(array("noreg_pasien"=>$x['id']));
		if($op!=NULL){
			$d['dokter_bedah']      = $op->dokter_operator;
			$d['dokter_anastesi']   = $op->dokter_anastesi;
			$d['tgl_tindakan']      = $op->tanggal_mulai;
			$d['tindakan']          = $op->tindakan;
			$d['bedah']             = "1";
		}		
		if($op==NULL || $op->dokter_operator=="" || $op->dokter_anastesi=="" || $op->tanggal_mulai=="0000-00-00 00:00:00"){
			$o=new ServiceConsumer($db, "get_ok",NULL,$_POST['ruang_ok']);
			$o->addData("noreg_pasien", $x['id']);
			$o->setMode(ServiceConsumer::$SINGLE_MODE);
			$o->execute();
			$okp=$o->getContent();
			if($okp!=NULL){
				$d['dokter_bedah']      = $okp['nama_operator_satu'];
				$d['dokter_anastesi']   = $okp['nama_anastesi'];
				$d['tgl_tindakan']      = $okp['waktu'];
				$d['bedah']             = "1";
			}
		}
		$dbtable->insert($d);		
	}
	$res=new ResponsePackage();
	$res->setStatus(ResponsePackage::$STATUS_OK);
	$res->setContent($list);
	echo json_encode($res->getPackage());
	return;
}

$header=array(	"No.","Tanggal","NRM", "No. Profile","Nama","Kelamin","Kelurahan",
				"Kecamatan","Kabupaten","Pendidikan",
				"Pekerjaan","Agama","Dokter",
				"Diagnosa","ICD","Penyakit","Rujukan",
				"Kasus","Pelayanan", "Gol. Umur");
$uitable=new Table($header);
$uitable->setName("lap_kunjungan")
		->setActionEnable(false)
		->setFooterVisible(false);

if(isset($_POST['command']) && $_POST['command']=="excel"){
	require_once "medical_record/snippet/create_excel_lap_masuk.php";
    return;
}

if(isset($_POST['command']) && $_POST['command']!=""){
	$dbtable=new DBTable($db, "smis_mr_pasien_pulang");
	$dbtable->setOrder(" tgl_pulang ASC");
	$dbtable->setShowAll(true);
	
	if(isset($_POST['pelayanan']) && $_POST['pelayanan']!="" ){
		$dbtable->addCustomKriteria("uri","='". $_POST['pelayanan']."'");
	}
	
	if(isset($_POST['kelamin']) && $_POST['kelamin']!="" ){
		$dbtable->addCustomKriteria("jk","='". $_POST['kelamin']."'");
	}
    
    if(isset($_POST['golongan_umur']) && $_POST['golongan_umur']!="" ){
		$dbtable->addCustomKriteria("gol_umur","='". $_POST['golongan_umur']."'");
	}
	
	if(isset($_POST['order']) && $_POST['order']!="" ){
		$dbtable->setOrder($_POST['order'],true);
	}
	
	
	$adapter=new SimpleAdapter();
	$adapter->setUseNumber(true, "No.","back.");
	$adapter->add("Tanggal", "tgl_masuk","date d M Y");
	$adapter->add("NRM", "nrm","only-digit8");
	$adapter->add("No. Profile", "profile_number");
	$adapter->add("Nama", "nama_pasien");
	$adapter->add("Kelamin", "jk","trivial_0_L_P");
	$adapter->add("Kelurahan", "kelurahan");
	$adapter->add("Kecamatan", "kecamatan");
	$adapter->add("Kabupaten", "kabupaten");
	$adapter->add("Pendidikan", "pendidikan");
	$adapter->add("Pekerjaan", "pekerjaan");
	$adapter->add("Agama", "agama");
	$adapter->add("Kasus", "kasus");
	$adapter->add("Pelayanan", "uri","trivial_0_URJ_URI");
	$adapter->add("ICD", "kode_diagnosa");
	$adapter->add("Diagnosa", "diagnosa");
	$adapter->add("Rujukan", "rujukan");
	$adapter->add("Dokter", "dokter_pj");
	$adapter->add("Gol. Umur", "gol_umur");
	
	$dbres=new DBResponder($dbtable, $uitable, $adapter);
	$hasil=$dbres->command($_POST['command']);
	echo json_encode($hasil);
	return;
}

$pelayanan=new OptionBuilder();
$pelayanan->add("","","1");
$pelayanan->add("URI","1");
$pelayanan->add("URJ","0");

$kelamin=new OptionBuilder();
$kelamin->add("","","1");
$kelamin->add("Perempuan","1");
$kelamin->add("Laki-Laki","0");

$umur = get_medical_gol_umur();
$gol_umur = new OptionBuilder();
$gol_umur->add(" - SEMUA - ", "", "1");
for($i = 0; $i < sizeof($umur); $i++) {
     $gol_umur->add($umur[$i]['value'], $umur[$i]['value'], "0");
}

$order=new OptionBuilder();
$order->add("","","1");
$order->add("Kecamatan","kecamatan ASC, kabupaten ASC");
$order->add("Kabupaten","kabupaten ASC, kecamatan ASC");
$order->add("Pendidikan","pendidikan ASC");
$order->add("Pekerjaan","pekerjaan ASC");
$order->add("Dokter","dokter_pj ASC");
$order->add("Diagnosa","diagnosa ASC");
$order->add("ICD","kode_icd ASC");
$order->add("Kasus","kasus ASC");
$order->add("Tanggal Masuk","tgl_masuk ASC");

$separator=new OptionBuilder();
$separator->add("Colon",",",1);
$separator->add("Semi Colon",";",0);

$detail=new OptionBuilder();
$detail->add("Hilangkan Detail Asuransi","0",1);
$detail->add("Tampilkan Detail Asuransi","1",0);

$uitable->clearContent();
$uitable->addModal("dari", "date", "Dari", "")
		->addModal("sampai", "date", "Sampai", "")
		->addModal("pelayanan", "select","Pelayanan", $pelayanan->getContent())
		->addModal("separator", "select","Separator", $separator->getContent())
		->addModal("kelamin", "select","Kelamin", $kelamin->getContent())
		->addModal("order", "select","Urutkan", $order->getContent())
        ->addModal("detail", "select","Detail", $detail->getContent())
        ->addModal("golongan_umur", "select","Gol. Umur", $gol_umur->getContent())
		->addModal("ruang_ok", "text", "Ruang Operasi", getSettings($db,"mr-slug-ruang-operasi",""),"",null,true);
$action=new Button("","","View");
$action->setClass("btn-primary")
	   ->setIsButton(Button::$ICONIC)
	   ->setIcon("icon-white fa fa-circle-o-notch")
	   ->setAction("lap_kunjungan.rekaptotal()");
$view=new Button("","","View");
$view->setClass("btn-primary")
		->setIsButton(Button::$ICONIC)
		->setIcon("fa fa-refresh")
		->setAction("lap_kunjungan.view()");
$excel=new Button("","","Excel");
$excel->setClass("btn-primary")
	->setIsButton(Button::$ICONIC)
	->setIcon("fa fa-file-excel-o")
	->setAction("lap_kunjungan.excel()");

$btng=new ButtonGroup("");
$btng->addButton($action);
$btng->addButton($view);
$btng->addButton($excel);

$form=$uitable
	  ->getModal()
	  ->setTitle("Medical Record")
	  ->getForm()
	  ->addElement("",$btng);

$close=new Button("", "", "Batal");
$close	->addClass("btn-primary")
		->setIsButton(Button::$ICONIC_TEXT)
		->setIcon("fa fa-close")
		->setAction("lap_kunjungan.batal()");

$load=new LoadingBar("rekap_lap_kunjungan_bar", "");
$modal=new Modal("rekap_lap_kunjungan_modal", "", "Processing...");
$modal	->addHTML($load->getHtml(),"after")
		->addFooter($close);

echo $form->getHtml();
echo "<div class='clear'></div>";
echo "<div id='result_lap_kunjungan'>".$uitable->getHtml()."</div>";
echo $modal->getHtml();
echo addJS("framework/bootstrap/js/bootstrap-datepicker.js");
echo addJS("framework/smis/js/table_action.js");
echo addCSS("framework/bootstrap/css/datepicker.css");
echo addJS ( "base-js/smis-base-loading.js");
?>

<script type="text/javascript">
	var lap_kunjungan;
	var lap_kunjungan_karyawan;
	var lap_kunjungan_data;
	var IS_lap_kunjungan_RUNNING;
	$(document).ready(function(){
		$('.mydate').datepicker();
		lap_kunjungan=new TableAction("lap_kunjungan","medical_record","lap_kunjungan",new Array());
		lap_kunjungan.addRegulerData=function(data){
			data['dari']            = $("#lap_kunjungan_dari").val();
			data['sampai']          = $("#lap_kunjungan_sampai").val();
			data['ruang_ok']        = $("#lap_kunjungan_ruang_ok").val();
			data['separator']       = $("#lap_kunjungan_separator").val();
			data['pelayanan']       = $("#lap_kunjungan_pelayanan").val();
			data['kelamin']         = $("#lap_kunjungan_kelamin").val();
			data['order']           = $("#lap_kunjungan_order").val();
			data['detail']          = $("#lap_kunjungan_detail").val();
			data['golongan_umur']   = $("#lap_kunjungan_golongan_umur").val();
			$("#dari_table_lap_kunjungan").html(getFormattedDate(data['dari']));
			$("#sampai_table_lap_kunjungan").html(getFormattedDate(data['sampai']));			
			return data;
		};

		lap_kunjungan.batal=function(){
			IS_lap_kunjungan_RUNNING=false;
			$("#rekap_lap_kunjungan_modal").modal("hide");
		};
		
		lap_kunjungan.afterview=function(json){
			if(json!=null){
				$("#kode_table_lap_kunjungan").html(json.nomor);
				$("#waktu_table_lap_kunjungan").html(json.waktu);
				lap_kunjungan_data=json;
			}
		};

		lap_kunjungan.rekaptotal=function(){
			if(IS_lap_kunjungan_RUNNING) return;
			$("#rekap_lap_kunjungan_bar").sload("true","Fetching total data",0);
			$("#rekap_lap_kunjungan_modal").modal("show");
			IS_lap_kunjungan_RUNNING=true;
			var d=this.getRegulerData();
			d['super_command']="total";
			$.post("",d,function(res){
				var all=getContent(res);
				if(all!=null) {
					var total=Number(all);
					lap_kunjungan.rekaploop(0,total);
				} else {
					$("#rekap_lap_kunjungan_modal").modal("hide");
					IS_lap_kunjungan_RUNNING=false;
				}
			});
		};

		lap_kunjungan.excel=function(){
			var d=this.getRegulerData();
			d['command']="excel";
			download(d);
		};

		lap_kunjungan.rekaploop=function(current,total){
			if(current>=total || !IS_lap_kunjungan_RUNNING) {
				$("#rekap_lap_kunjungan_modal").modal("hide");
				IS_lap_kunjungan_RUNNING=false;
				lap_kunjungan.view();
				return;
			}
			var d=this.getRegulerData();
			d['super_command']="limit";
			d['halaman']=current;
			$.post("",d,function(res){
				var ct=getContent(res);
				var u=ct[0]['nama_pasien']+"  "+ct[0]['nrm'];
				$("#rekap_lap_kunjungan_bar").sload("true"," Processing - "+u+"... [ "+(current+1)+" / "+total+" ] ",(current*100/total));
				setTimeout(function(){lap_kunjungan.rekaploop(++current,total)},300);
			});
		};
				
	});
</script>