<?php

/**
 * menampilkan data 
 * pasien diagnosa pasien
 * dan dihitung persentasenya 
 * 
 * @author 		: Nurul Huda
 * @copyright 	: goblooge@gmail.com
 * @license 	: LGPLv2
 * @database 	: - smis_mr_diagnosa
 * @since		: 06 Juli 2017
 * @version		: 1.0.0
 * 
 * */

global $db;
require_once 'smis-libs-class/MasterSlaveTemplate.php';
require_once 'medical_record/class/adapter/LaporanPersentaseDiagnosaAdapter.php';
require_once 'medical_record/class/responder/LapPersenDiagnosaResponder.php';
require_once ("smis-base/smis-include-service-consumer.php");

$header=array ('No.',"Diagnosa","Nama","ICD","Laki","Perempuan","Jumlah","Persentase");
$pemreg=new MasterSlaveTemplate($db, "smis_mr_diagnosa", "medical_record", "lap_persen_diagnosa");
$pemreg->setDateEnable(true);


$uitable=$pemreg->getUItable();
$uitable=new Table($header);
$uitable->setHeader($header);
$uitable->setAction(false);
$uitable->setFooterVisible(false);

$uitable->setName("lap_persen_diagnosa");
$pemreg->getDBtable()->setOrder(" tanggal DESC ");
$pemreg->setUITable($uitable);
if( ($pemreg->getDBResponder()->isView() || $pemreg->getDBResponder()->isExcel()) && $_POST['super_command']==""){
    
    $view="SELECT count(*) as total, jk, nama_icd, kode_icd, sebab_sakit FROM smis_mr_diagnosa ";
    $pemreg	->getDBtable()
            ->setPreferredQuery(true,$view,"")
            ->setCountEnable(false)
            ->setGroupBy(true," nama_icd,kode_icd,sebab_sakit,jk ")
            ->setShowAll(true)
            ->setUseWhereForView(true)
			->addCustomKriteria(NULL, "tanggal>='".$_POST['dari']."'")
            ->addCustomKriteria(NULL, "tanggal<'".$_POST['sampai']."'");
            
    if (isset ( $_POST ['origin'] ) && $_POST ['origin'] != '') {
        $pemreg	->getDBtable()
                ->addCustomKriteria ( " origin ", "='".$_POST['origin']."'" );
    }
    if (isset ( $_POST ['pelayanan'] ) && $_POST ['pelayanan'] != '') {
        $pemreg	->getDBtable()
                ->addCustomKriteria ( " urji ", "='".$_POST['pelayanan']."'" );
    }
    if (isset ( $_POST ['ruangan'] ) && $_POST ['ruangan'] != '') {
        $pemreg	->getDBtable()
                ->addCustomKriteria ( "ruangan", " = '".$_POST ['ruangan']."'" );
    }
    if (isset ( $_POST ['jk'] ) && $_POST ['jk'] != '') {
        $pemreg	->getDBtable()
                ->addCustomKriteria ( "jk", " = '".$_POST ['jk']."'" );
    }
    if (isset ( $_POST ['kunjungan'] ) && $_POST ['kunjungan'] != '') {
        $pemreg	->getDBtable()
                ->addCustomKriteria ( "kunjungan", " = '".$_POST ['kunjungan']."'" );
    }
}

$query="SELECT DISTINCT origin FROM smis_mr_diagnosa WHERE prop!='del' ";
$origin=new OptionBuilder();
$origin->add(" - SEMUA - ","",1);

$res=$db->get_result($query);
foreach($res as $x){
    if($x->origin==""){
        continue;
    }
    $origin->add(ArrayAdapter::slugFormat("unslug",$x->origin),$x->origin);
}

$pelayanan=new OptionBuilder();
$pelayanan->add("- SEMUA -", "", "1");
$pelayanan->add("URI", "1", "0");
$pelayanan->add("URJ", "0", "0");

if(isset($_POST['super_command']) && $_POST['super_command'] == 'pelayanan') {
    if($_POST['pelayanan'] == "0") {
        $urjip=new ServiceConsumer($db, "get_urjip",array());
        $urjip->setMode(ServiceConsumer::$MULTIPLE_MODE);
        $urjip->setCached(true,"get_urjip");
        $urjip->execute();
        $content=$urjip->getContent();
        $ruangan=array();
        $option['value'] = '';
        $option['name'] = '- SEMUA -';
        $ruangan[]=$option;
        foreach ($content as $autonomous=>$ruang){
            foreach($ruang as $nama_ruang=>$jip){
                if($jip[$nama_ruang] != "UP" && $jip[$nama_ruang] != "URI"){
                    $option=array();
                    $option['value']=$nama_ruang;
                    $option['name']=ArrayAdapter::format("unslug", $nama_ruang);
                    $ruangan[]=$option;
                }
            }
        }
        echo json_encode($ruangan);
        return;
    } else if($_POST['pelayanan'] == "1") {
        $urjip=new ServiceConsumer($db, "get_urjip",array());
        $urjip->setMode(ServiceConsumer::$MULTIPLE_MODE);
        $urjip->setCached(true,"get_urjip");
        $urjip->execute();
        $content=$urjip->getContent();
        $ruangan=array();
        $option['value'] = '';
        $option['name'] = '- SEMUA -';
        $ruangan[]=$option;
        foreach ($content as $autonomous=>$ruang){
            foreach($ruang as $nama_ruang=>$jip){
                if($jip[$nama_ruang] != "UP" && $jip[$nama_ruang] == "URI"){
                    $option=array();
                    $option['value']=$nama_ruang;
                    $option['name']=ArrayAdapter::format("unslug", $nama_ruang);
                    $ruangan[]=$option;
                }
            }
        }
        echo json_encode($ruangan);
        return;
    } else {
        $ruangan = array();
        $option['value'] = '';
        $option['name'] = '- SEMUA -';
        $ruangan[]=$option;
        echo json_encode($ruangan);
        return;
    }
} else {
    $ruangan = array();
    $option['value'] = '';
    $option['name'] = '- SEMUA -';
    $ruangan[]=$option;
}

$jk = new OptionBuilder();
$jk->add(" - SEMUA - ","",1);
$jk->add("Laki-Laki","0",0);
$jk->add("Perempuan","1",0);

$kunjungan = new OptionBuilder();
$kunjungan->add(" - SEMUA - ","",1);
$kunjungan->add("Baru","Baru",0);
$kunjungan->add("Lama","Lama",0);

$pemreg  ->addFlag("dari", "Pilih Tanggal Dari", "Masukan Tanggal Awal Dahulu")
		 ->addFlag("sampai", "Pilih Tanggal Sampai", "Masukan Tanggal Akhir Dahulu")
		 ->addNoClear("dari")
		 ->addNoClear("sampai")
		 ->setDateTimeEnable(true)
		 ->getUItable()
		 ->addModal("dari", "date", "Dari", "")
		 ->addModal("sampai", "date", "Sampai", "")
         ->addModal ( "origin", "select", "Asal", $origin->getContent() )     
         ->addModal ( "pelayanan", "select", "Layanan", $pelayanan->getContent() )    
         ->addModal ( "ruangan", "select", "Ruangan", $ruangan )  
         ->addModal ( "jk", "select", "Jenis Kelamin", $jk->getContent() )
         ->addModal ( "kunjungan", "select", "Baru Lama", $kunjungan->getContent() );       
$form=$pemreg ->getForm();

$button = new Button ( "", "", "" );
$button->setIsButton ( Button::$ICONIC );
$button->setIcon ( "icon-white " . BUtton::$icon_refresh );
$button->setAction ( "lap_persen_diagnosa.view()" );
$button->setClass("btn-primary");
$form->addElement ( "", $button );

$excel = new Button ( "", "", "" );
$excel->setIsButton ( Button::$ICONIC );
$excel->setIcon ( "fa fa-file-excel-o");
$excel->setAction ( "lap_persen_diagnosa.excel()" );
$excel->setClass("btn-primary");
$form->addElement ( "", $excel );

$button = new Button ( "", "", "" );
$button->setIsButton ( Button::$ICONIC );
$button->setIcon ( "icon-white " . BUtton::$icon_print );
$button->setAction ( "smis_print($('#print_table_lap_persen_diagnosa').html())" );
$button->setClass("btn-primary");
$form->addElement ( "", $button );

$adapter=new LaporanPersentaseDiagnosaAdapter();
$pemreg->setAdapter($adapter);
$pemreg ->addRegulerData("dari","dari","id-value")
        ->addRegulerData("sampai","sampai","id-value")
        ->addRegulerData("pelayanan","pelayanan","id-value")
        ->addRegulerData("ruangan","ruangan","id-value")
        ->addRegulerData("jk","jk","id-value")
        ->addRegulerData("kunjungan","kunjungan","id-value");
        
$pemreg->addResouce("js", "medical_record/resource/js/lap_persen_diagnosa.js", "before", false);
        
$responder=new LapPersenDiagnosaResponder($pemreg->getDBtable(),$pemreg->getUItable(),$pemreg->getAdapter());
$pemreg->setDBresponder($responder);
$pemreg->initialize();
?>


