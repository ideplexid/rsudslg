<?php
setChangeCookie ( false );
global $db;
require_once ("smis-base/smis-include-service-consumer.php");
$uitable = new Report ( array ('No', 'DTD','Diagnosa','L',"P","L1","P1","Baru","Lama"), "", NULL );
$uitable->setDiagram ( true );
$uitable->setName ( "morbiditas" );
/* this is respond when system have to response */
if (isset ( $_POST ['command'] )) {
	require_once 'medical_record/class/adapter/MorbiditasAdapter.php';
    require_once 'medical_record/class/responder/MorbiditasResponder.php';
	//$dbtable = new DBTable ( $db, "smis_mr_diagnosa", array ('ruangan'));
	$dbtable = new DBTable ( $db, "smis_vmr_morbiditas");
    
	if (isset ( $_POST ['filter_ruangan'] ) && $_POST ['filter_ruangan'] != '')
		$dbtable->addCustomKriteria ( "ruangan", " = '" . $_POST ['filter_ruangan'] . "'" );
	$dbtable->setOrder ( "tanggal DESC" );
    $carabayar=json_decode($_POST['jenis_pasien'],true);
    foreach($carabayar as $x=>$y){
        $uitable->addHeaderElement($y);
    }

    $adapter=new MorbiditasAdapter();
    $adapter->setCarabayar($carabayar);
	$dbres = new MorbiditasResponder ( $dbtable, $uitable, $adapter, 'tanggal', DBReport::$DATE );
	$dbres->setCarabayar($carabayar);
	// $dbres->setDiagram(true, $diagram_view,null, $diagramadapter);
	$dbres->setDiagram ( false, null, null, null );
	$data = $dbres->command ( $_POST ['command'] );
    if($data!=null){
        echo json_encode ( $data, JSON_NUMERIC_CHECK );    
    }
	return;
}

$service = new ServiceConsumer ( $db, "get_carabayar" );
$service->execute ();
$carabayar=$service->getContent ();

$html="";
$total_carabayar=0;
$slug=array();
foreach($carabayar as $x){
    $total_carabayar++;
    $html.="<th>".$x['nama']."</th>";
    $slug[$x['slug']]=$x['nama'];
    $uitable->addHeaderElement($x['nama']);
}

$uitable->addHeader ( "before", "<tr>
		<th rowspan='2'>No.</th>
		<th rowspan='2'>DTD</th>
		<th rowspan='2'>Diagnosa</th>
		<th colspan='2'>Kunjungan Baru</th>
		<th colspan='2'>Kunjungan Lama</th>
		<th colspan='2'>Jenis Kasus</th>
		<th colspan='".$total_carabayar."'>Jenis Pasien</th>		
		</tr>" );
$uitable->addHeader ( "before", "<tr>
		<th>Laki</th>
		<th>Perempuan</th>
		<th>Laki</th>
		<th>Perempuan</th>
		<th>Baru</th>
		<th>Lama</th>
		".$html."
		</tr>" );
$uitable->setHeaderVisible ( false );


/* This is Modal Form and used for add and edit the table */
loadClass ( "ServiceProviderList" );
$service = new ServiceProviderList ( $db, "push_antrian" );
$service->execute ();
$ruangan = $service->getContent ();
$ruang['value'] = NULL;
$ruang['name'] = ' - SEMUA - ';
$ruang['default'] = '0';
$ruangan[] = $ruang;

$uitable->addModal ( "filter_ruangan", "select", "Ruang", $ruangan );
$modal = $uitable->getAdvanceModal ();
$modal->setTitle ( "morbiditas" );
$carabayar_hidden=new Hidden("morbiditas_jenis_pasien","",json_encode($slug));

$excel=new Button("","","");
$excel->setIsButton(Button::$ICONIC);
$excel->setIcon(" fa fa-file-excel-o");
$excel->setAction("morbiditas.excel()");
$excel->setClass("btn-primary");
$modal->addFooter($excel);

echo addCSS("framework/bootstrap/css/morris.css");
echo addJS("framework/jquery/raphael-min.js");
echo addJS("framework/bootstrap/js/morris.min.js");
echo addJS ( "framework/smis/js/table_action.js" );
echo addJS ( "framework/smis/js/report_action.js" );
echo addJS ( "framework/bootstrap/js/bootstrap-datepicker.js" );
echo addCSS ( "framework/bootstrap/css/datepicker.css" );

echo $modal->getModalSkeleton();
echo "<div class='line clear'></div>";
echo $uitable->getHtml();
echo $carabayar_hidden->getHtml();

?>

<script type="text/javascript">
var morbiditas;
$(document).ready(function(){
    $('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
    $('.mydate').datepicker();
    var column=new Array();
    morbiditas=new ReportAction("morbiditas","medical_record","morbiditas",column);
    morbiditas.addRegulerData=function(a){
      a['jenis_pasien']=$("#morbiditas_jenis_pasien").val(); 
      return a; 
    };
});
</script>