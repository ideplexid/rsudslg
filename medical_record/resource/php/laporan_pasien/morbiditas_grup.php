<?php

setChangeCookie ( false );
global $db;
require_once ("smis-base/smis-include-service-consumer.php");
require_once 'medical_record/class/adapter/MorbiditasGrupAdapter.php';
require_once 'medical_record/class/responder/MorbiditasGrupResponder.php';

$header = array(
            "No.",
            "No. DTD",
            "No. Daftar Terperinci",
            "Golongan Sebab-Sebab Sakit",
            "28hr Lk",
            "28hr Pr",
            "1th Lk",
            "1th Pr",
            "4th Lk",
            "4th Pr",
            "14th Lk",
            "14th Pr",
            "24th Lk",
            "24th Pr",
            "44th Lk",
            "44th Pr",
            "65th Lk",
            "65th Pr",
            "l65th Lk",
            "l65th Pr",
            "Kasus Baru Lk",
            "Kasus Baru Pr",
            "Total",
            "Jumlah Kunjungan"
        );
        
$uitable = new Table($header, "", NULL);
$uitable->setName("morbiditas_grup");
$uitable->setHeaderVisible(false);
$uitable->setFooterVisible(false);
$uitable->setAction(false);

$uitable->addHeader ( "before", "<tr>
                        <th rowspan='3'>No.</th>
                        <th rowspan='3'>No. DTD</th>
                        <th rowspan='3'>No. Daftar Terperinci</th>
                        <th rowspan='3'>Golongan Sebab-Sebab Sakit</th>
                        <th colspan='16'>Kasus Baru Menurut Golongan Umur</th>
                        <th colspan='2' rowspan='2'>Jumlah Kasus Baru</th>
                        <th rowspan='3'>Total</th>
                        <th rowspan='3'>Jumlah Kunjungan</th>
                    </tr>" );
$uitable->addHeader ( "before", "<tr>
                        <th colspan='2'>0-28 HR</th>
                        <th colspan='2'>28-1TH</th>
                        <th colspan='2'>1-4TH</th>
                        <th colspan='2'>5-14TH</th>
                        <th colspan='2'>15-24TH</th>
                        <th colspan='2'>25-44TH</th>
                        <th colspan='2'>45-65TH</th>
                        <th colspan='2'>>65TH</th>
                    </tr>" );
$uitable->addHeader ( "before", "<tr>
                        <th>Lk</th>
                        <th>Pr</th>
                        <th>Lk</th>
                        <th>Pr</th>
                        <th>Lk</th>
                        <th>Pr</th>
                        <th>Lk</th>
                        <th>Pr</th>
                        <th>Lk</th>
                        <th>Pr</th>
                        <th>Lk</th>
                        <th>Pr</th>
                        <th>Lk</th>
                        <th>Pr</th>
                        <th>Lk</th>
                        <th>Pr</th>
                        <th>Lk</th>
                        <th>Pr</th>
                    </tr>" );
                    
if(isset($_POST['command'])) {
    $dbtable = new DBTable ( $db, "smis_vmr_morbiditas_grup");
    $dbtable->addCustomKriteria ( "status_ruangan", "='actived'" );
    if(isset($_POST['dari']) && isset($_POST['sampai'])) {
        $dbtable->addCustomKriteria ( NULL, " tanggal >= '".$_POST ['dari']."' AND tanggal <= '".$_POST ['sampai']."'" );
    }
    if (isset ( $_POST ['filter_ruangan'] ) && $_POST ['filter_ruangan'] != '') {
        $dbtable->addCustomKriteria ( "ruangan", " = '".$_POST ['filter_ruangan']."'" );
    }
	$dbtable->setOrder ( "grup ASC" );
    $dbtable->setShowAll(true);
    
    $data = $dbtable->view("", 0);
    $size = sizeof($data['data']);
    
    $adapter = new MorbiditasGrupAdapter();
    $adapter->setDatasize($size);
    
    $dbres = new MorbiditasGrupResponder($dbtable, $uitable, $adapter);
    $res = $dbres->command($_POST ['command']);
    if($res != null){
        echo json_encode ( $res, JSON_NUMERIC_CHECK );    
    }
	return;
}

/*$query="SELECT DISTINCT origin FROM smis_vmr_morbiditas_grup WHERE prop!='del ORDER BY origin ASC' ";
$origin=new OptionBuilder();
$origin->add(" - SEMUA - ","",1);
$res=$db->get_result($query);
foreach($res as $x){
    if($x->origin==""){
        continue;
    }
    $origin->add(ArrayAdapter::slugFormat("unslug",$x->origin),$x->origin);
}
$uitable->addModal( "filter_origin", "select", "Asal", $origin->getContent() );*/

$query="SELECT DISTINCT ruangan FROM smis_vmr_morbiditas_grup WHERE prop!='del' AND status_ruangan = 'actived' ORDER BY ruangan ASC ";
$ruangan=new OptionBuilder();
$ruangan->add(" - SEMUA - ","",1);
$res=$db->get_result($query);
foreach($res as $x){
    if($x->ruangan==""){
        continue;
    }
    $ruangan->add(ArrayAdapter::slugFormat("unslug",$x->ruangan),$x->ruangan);
}
$uitable->addModal( "filter_ruangan", "select", "Ruangan", $ruangan->getContent() );

$uitable->addModal("dari", "date", "Dari", "")
		->addModal("sampai", "date", "Sampai", "");
        
$modal = $uitable->getModal();
$form = $modal->getForm ();

$button = new Button ( "", "", "" );
$button->setIsButton ( Button::$ICONIC );
$button->setIcon ( "fa fa-refresh" );
$button->setClass("btn-primary");
$button->setAction ( "morbiditas_grup.view()" );
$form->addElement ( "", $button );

$excel=new Button("","","");
$excel->setIsButton(Button::$ICONIC);
$excel->setIcon(" fa fa-file-excel-o");
$excel->setAction("morbiditas_grup.excel()");
$excel->setClass("btn-primary");
$form->addElement ( "", $excel );

echo "<h2>Morbiditas</h2>";
echo $form->getHtml();
echo $uitable->getHtml();

echo addJS("framework/bootstrap/js/bootstrap-datepicker.js");
echo addJS("framework/smis/js/table_action.js");
echo addCSS("framework/bootstrap/css/datepicker.css");

?>

<script type="text/javascript">

var morbiditas_grup;
$(document).ready(function(){
    $('.mydate').datepicker();
    var column = new Array();
    morbiditas_grup = new TableAction("morbiditas_grup", "medical_record", "morbiditas_grup", column);
    
    morbiditas_grup.addRegulerData = function(data) {
        data['dari'] = $("#morbiditas_grup_dari").val();
        data['sampai'] = $("#morbiditas_grup_sampai").val();
        data['filter_ruangan'] = $("#morbiditas_grup_filter_ruangan").val();
        return data;
    };
});

</script>