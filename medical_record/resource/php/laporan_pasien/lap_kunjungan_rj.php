<?php 

global $db;
require_once 'smis-base/smis-include-service-consumer.php';
require_once 'medical_record/class/responder/LapKunRJResponder.php';

$head=array ('No.', 'Poli/Ruang', 'Jan', 'Feb', 'Mar', 'Apr', 'Mei', 'Jun', 'Jul', 'Ags', 'Sep', 'Okt', 'Nov', 'Des', 'Total');
$uitable = new Table ( $head, "Laporan Kunjungan Rawat Jalan", NULL, true );
$uitable->setName ( "lap_kunjungan_rj" );
$uitable->setAction(false);
$uitable->setFooterVisible(false);

//Dropdown Layanan
$layanan = new ServiceConsumer($db, "get_list_layanan",array());
$layanan->setMode(ServiceConsumer::$MULTIPLE_MODE);
$layanan->setCached(true,"get_list_layanan");
$layanan->execute();
$content=$layanan->getContent();
$ruangan = array();
foreach($content as $jenislayanan) {
    foreach($jenislayanan as $x) {
        foreach($x as $y) {
            $option = array();
            $option['value'] = $y["jenislayanan"];
            $option['name'] = ArrayAdapter::format("unslug", $y["jenislayanan"]);
            $ruangan[] =$option;
        }
    }
}
$option['value']="";
$option['name']=" - SEMUA - ";
$ruangan[] = $option;
$uitable->addModal( "layanan", "select", "Layanan", $ruangan );

//Dropdown Tahun
$tahun = new ServiceConsumer($db, "get_thn_kunjungan_rj", array());
$tahun->setMode(ServiceConsumer::$MULTIPLE_MODE);
$tahun->setCached(true,"get_thn_kunjungan_rj");
$tahun->execute();
$content=$tahun->getContent();
$thn = array();
foreach($content as $con){
    foreach($con as $t){
        foreach($t as $list){
            $option = array();
            $option['value'] = $list["tahun"];
            $option['name'] = $list["tahun"];
            $thn[] =$option;
        }
    }
}
/*$option['value']="";
$option['name']=" - SEMUA - ";
$thn[] = $option;*/
$uitable->addModal( "tahun", "select", "Tahun", $thn );

$modal = $uitable->getModal ();
$form=$modal->getForm();

$view=new Button("", "", "");
$view->addClass("btn-primary")
		->setIsButton(Button::$ICONIC)
		->setIcon("fa fa-refresh")
		->setAction("lap_kunjungan_rj.view()");
$form->addElement("", $view);

$excel=new Button("", "", "");
$excel->addClass("btn-primary")
		->setIsButton(Button::$ICONIC)
		->setIcon("fa fa-file-excel-o")
		->setAction("lap_kunjungan_rj.excel()");
$form->addElement("", $excel);

if (isset ( $_POST ['command'] )) {
	$adapter = new SimpleAdapter ();
	$adapter->setUseNumber(true,"No.","back.");
	$adapter->add ( "Poli/Ruang", "layanan","unslug" );
	$adapter->add ( "Jan", "jan" );
	$adapter->add ( "Feb", "feb" );
	$adapter->add ( "Mar", "mar" );
	$adapter->add ( "Apr", "apr" );
	$adapter->add ( "Mei", "mei" );
	$adapter->add ( "Jun", "jun" );
	$adapter->add ( "Jul", "jul" );
	$adapter->add ( "Ags", "ags" );
	$adapter->add ( "Sep", "sep" );
	$adapter->add ( "Okt", "okt" );
	$adapter->add ( "Nov", "nov" );
	$adapter->add ( "Des", "des" );
	$adapter->add ( "Total", "total" );
	//$dbres = new ServiceResponder($db, $uitable, $adapter, "get_kunjungan_rj", "registration");
	$dbres = new LapKunRJResponder($db, $uitable, $adapter, "get_kunjungan_rj", "registration");
	$data = $dbres->command ( $_POST ['command'] );
	echo json_encode ( $data );
	return;
}

echo $form->getHtml();
echo $uitable->getHtml ();
echo $modal->getHtml ();

echo addJS ( "framework/smis/js/table_action.js" );

?>

<script type="text/javascript">
var lap_kunjungan_rj;
$(document).ready(function(){
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
	var column=new Array();
	lap_kunjungan_rj = new TableAction("lap_kunjungan_rj","medical_record","lap_kunjungan_rj",column);
    lap_kunjungan_rj.addViewData = function(a) {
        a['layanan'] = $("#lap_kunjungan_rj_layanan").val();
        a['tahun'] = $("#lap_kunjungan_rj_tahun").val();
        return a;
    };
    
    $('#lap_kunjungan_rj_layanan').on('change', function() {
        lap_kunjungan_rj.view();
    });
	//lap_kunjungan_rj.view();	
});
</script>