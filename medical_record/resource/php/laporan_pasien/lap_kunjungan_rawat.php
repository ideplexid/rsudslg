<?php 
require_once "smis-base/smis-include-service-consumer.php";
$table=new Table(array("Ruangan","Jan","Feb","Mar","Apr","Mei","Jun","Jul","Agu","Sep","Okt","Nop","Des","Total"),"");
$table->setName ( "lap_kunjungan_rawat" );
$table->setAction(false);
$table->setFooterVisible(false);

if (isset($_POST ['command']) && $_POST ['command'] != ""){
    $serv=new ServiceConsumer($db,"get_total_patient_in_year");
    $serv->addData("tahun",$_POST['tahun']);
    $serv->setMode(ServiceConsumer::$CLEAN_BOTH_JOIN_SINGLE_ARRAY);
    $serv->execute();
    $data=$serv->getContent();
    $arr = array();
    if(isset($_POST['ruangan']) && $_POST['ruangan'] != '') {
        $i = 0;
        foreach($data as $key => $value) {            
            if($data[$key]['urji'] != 'URI' && $data[$key]['ruangan'] == $_POST['ruangan']){
                $arr[] = $data[$key];
            }
            $i++;
        }
    } else {
        $i = 0;
        foreach($data as $key => $value) {            
            if($data[$key]['urji'] != 'URI'){
                $arr[] = $data[$key];
            }
            $i++;
        }
    }
    
    /*echo "<pre>";
    var_dump($arr);
    echo "</pre>";*/

    $adapter=new SimpleAdapter();
    $adapter->add("Ruangan","ruangan","unslug");
    $adapter->add("Jan","january");
    $adapter->add("Feb","february");
    $adapter->add("Mar","march");
    $adapter->add("Apr","april");
    $adapter->add("Mei","may");
    $adapter->add("Jun","juny");
    $adapter->add("Jul","july");
    $adapter->add("Agu","august");
    $adapter->add("Sep","september");
    $adapter->add("Okt","october");
    $adapter->add("Nop","november");
    $adapter->add("Des","december");
    $adapter->add("Total","total");
    $ready=$adapter->getContent($arr);
    
    $table->setContent($ready);
    $content['list'] = $table->getBodyContent();
    $response = new ResponsePackage ();
	$response->setAlertVisible ( false );
	$response->setStatus ( ResponsePackage::$STATUS_OK );
	$response->setContent($content);
	echo json_encode ( $response->getPackage () );
	return;
}


//Filter Dropdown Tahun
$tahun = new ServiceConsumer($db,"get_thn_kunjungan_rj");
$tahun->execute();
$content = $tahun->getContent();
$thn = array();
foreach($content as $con){
    $option = array();
    $option['value'] = $con["tahun"];
    $option['name'] = $con["tahun"];
    $thn[] =$option;
}
$table->addModal( "tahun", "select", "Tahun", $thn );

//Filter Dropdown Ruangan
$ruang = new ServiceConsumer($db, "get_total_patient_in_year");
$ruang->setMode(ServiceConsumer::$CLEAN_BOTH_JOIN_SINGLE_ARRAY);
$ruang->execute();
$content = $ruang->getContent();
$ruangan = array();
foreach($content as $key => $value) {
    if($content[$key]['urji'] != 'URI') {
        $option = array();
        $option['value'] = $content[$key]['ruangan'];
        $option['name'] = ArrayAdapter::format("unslug", $content[$key]['ruangan']);
        $ruangan[] = $option;
    }
}
$option['value']="";
$option['name']=" - SEMUA - ";
$ruangan[] = $option;
$table->addModal( "ruangan", "select", "Layanan", $ruangan );

$modal = $table->getModal ();
$form=$modal->getForm();

$v = new Button("", "", "");
$v->addClass("btn-primary")
		->setIsButton(Button::$ICONIC)
		->setIcon("fa fa-refresh")
		->setAction("lap_kunjungan_rawat.view()");
$form->addElement("", $v);

echo $form->getHtml();
echo $table->getHtml ();
echo $modal->getHtml ();

echo addJS ( "framework/smis/js/table_action.js" );
?>

<script type="text/javascript">
var lap_kunjungan_rawat;
$(document).ready(function(){
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
	var column=new Array();
	lap_kunjungan_rawat = new TableAction("lap_kunjungan_rawat","medical_record","lap_kunjungan_rawat",column);
    lap_kunjungan_rawat.addViewData = function(a) {
        a['tahun'] = $("#lap_kunjungan_rawat_tahun").val();
        a['ruangan'] = $("#lap_kunjungan_rawat_ruangan").val();
        return a;
    };
});
</script>