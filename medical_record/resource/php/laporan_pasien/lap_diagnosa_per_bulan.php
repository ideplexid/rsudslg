<?php

global $db;
require_once 'medical_record/class/responder/LapDiagnosaPerBulanResponder.php';

$head=array ('No.','Tahun','Bulan','Kode ICD', "Nama ICD","Jumlah");
$uitable = new Table ( $head, "", NULL, true );
$uitable->setName ( "lap_diagnosa_per_bulan" );
$uitable->setAction(false);
$uitable->setFooterVisible(false);

$uitable->addModal("dari", "date", "Dari", "");
$uitable->addModal("sampai", "date", "Sampai", "");

$modal = $uitable->getModal ();
$form=$modal->getForm();
$form->setTitle("Laporan Diagnosa Per Bulan");

$view = new Button("", "", "");
$view->addClass("btn-primary")
		->setIsButton(Button::$ICONIC)
		->setIcon("fa fa-search")
		->setAction("lap_diagnosa_per_bulan.view()");
$form->addElement("", $view);

$excel=new Button("", "", "");
$excel->addClass("btn-primary")
		->setIsButton(Button::$ICONIC)
		->setIcon("fa fa-file-excel-o")
		->setAction("lap_diagnosa_per_bulan.excel()");
$form->addElement("", $excel);

if (isset ( $_POST ['command'] )) {
	$adapter = new SimpleAdapter ();
	$adapter->setUseNumber(true,"No.","back.");
	$adapter->add ( "Tahun", "tahun" );
	$adapter->add ( "Bulan", "bulan","month-id" );
	$adapter->add ( "Kode ICD", "kode_icd" );
	$adapter->add ( "Nama ICD", "nama_icd" );
	$adapter->add ( "Jumlah", "jumlah" );
    
    
    $dbtable = new DBTable($db,"smis_mr_vdiagnosa_bulan");
    
    $dbresponder = new LapDiagnosaPerBulanResponder($dbtable,$uitable,$adapter);
    
    if($dbresponder->isView() || $dbresponder->isExcel()) {
        $dari = $_POST["dari"];
        $sampai = $_POST["sampai"];
        
        $qv = " SELECT '' as id, '' as prop, YEAR(tanggal) AS tahun, MONTH(tanggal) AS bulan, kode_icd, nama_icd, count(*) as jumlah 
        FROM `smis_mr_diagnosa` 
        WHERE prop != 'del' AND tanggal >= '$dari' AND tanggal < '$sampai'
        GROUP BY tahun, bulan, kode_icd, nama_icd
        ORDER BY tahun, bulan, kode_icd, nama_icd ";
        
        $qc = "select count(*) as total from smis_mr_diagnosa ";
        
        $dbtable->setPreferredQuery(true, $qv, $qc);
        $dbtable->setUseWhereforView(false);
        $dbtable->setShowAll(true);
    }
    
	$data = $dbresponder->command ( $_POST ['command'] );
    if($data != NULL) {
        echo json_encode ( $data );
    }
	return;
}
echo $form->getHtml();
echo $uitable->getHtml ();
echo $modal->getHtml ();


echo addJS("framework/bootstrap/js/bootstrap-datepicker.js");
echo addJS ( "framework/smis/js/table_action.js" );
echo addCSS("framework/bootstrap/css/datepicker.css");

?>

<script type="text/javascript">
var lap_diagnosa_per_bulan;
$(document).ready(function(){
    $('.mydate').datepicker();
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
	var column = new Array();
	lap_diagnosa_per_bulan=new TableAction("lap_diagnosa_per_bulan","medical_record","lap_diagnosa_per_bulan",column);
	lap_diagnosa_per_bulan.view();	
    lap_diagnosa_per_bulan.addViewData = function(a){
        a["dari"] = $('#lap_diagnosa_per_bulan_dari').val();
        a["sampai"] = $('#lap_diagnosa_per_bulan_sampai').val();
        return a;
    };
});
</script>