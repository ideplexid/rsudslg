<?php 
global $db;
global $user;
require_once 'smis-base/smis-include-service-consumer.php';
if(isset($_POST['super_command']) && $_POST['super_command']=="total") {
	require_once 'smis-libs-class/ServiceProviderList.php';
	$dbtable=new DBTable($db, "smist_mr_lap_cauti");
	$dbtable->truncate();
	$serv=new ServiceConsumer($db, "get_patient_masuk");
	$serv->addData("command", "count");
	$serv->addData("dari", $_POST['dari']);
	$serv->addData("sampai", $_POST['sampai']);	
    $serv->addData("kamar_inap", $_POST['ruangan']);	
    
	$serv->execute();
	$data=$serv->getContent();
	$res=new ResponsePackage();
	$res->setStatus(ResponsePackage::$STATUS_OK);
	$res->setContent($data);
	echo json_encode($res->getPackage());
	return;
}

if(isset($_POST['super_command']) && $_POST['super_command']=="limit") {
	$data=array();
	$response=new ServiceConsumer($db, "get_patient_masuk");
	$response->addData("command", "list");
	$response->addData("dari", $_POST['dari']);
	$response->addData("sampai", $_POST['sampai']);
	$response->addData("halaman", $_POST['halaman']);	
	$response->setMode(ServiceConsumer::$SINGLE_MODE);
	$response->execute();
	$list=$response->getContent();
	$dbtable=new DBTable($db, "smist_mr_lap_cauti");
	$cauti=new DBTable($db,"smis_mr_cauti");
	loadLibrary("smis-libs-function-time");
	foreach($list as $x){		
		$d['nrm_pasien']=$x['nrm'];
		$d['nama_pasien']=$x['nama_pasien'];
		$d['noreg_pasien']=$x['id'];
        $d['ruangan']=$x['kamar_inap'];
        $d['masuk']=$x['tanggal'];
		$d['pulang']=$x['tanggal_pulang'];
		$d['ruangan']=$x['kamar_inap'];
        
        $insert=false;
        
        /*pasang infus*/
        $pasang=array();
        $pasang['kegiatan']="Pasang Catheter";
        $pasang['noreg_pasien']=$d['noreg_pasien'];
        $psg=$cauti->select($pasang);
        if($psg!=null){
            $d['pasang']=$psg->tanggal;
            $insert=true;
        }
        
        /*lepas infus*/
        $lepas=array();
        $lepas['kegiatan']="Lepas Catheter";
        $lepas['noreg_pasien']=$d['noreg_pasien'];
        $lps=$cauti->select($lepas);
        if($lps!=null){
            $d['lepas']=$lps->tanggal;
        }
		
        $query="SELECT isk FROM smis_mr_cauti 
                WHERE noreg_pasien='".$d['noreg_pasien']."' 
                      AND prop!='del'
                      AND isk !=''
                ORDER BY id DESC LIMIT 0,1";
        $var=$db->get_var($query);
        if($var!=null){
            $d['infeksi']=$var;
        }
        
        if($insert){
            if(isset($d['pasang']) && isset($d['lepas']) )
                $d['lama']=day_diff_only( $d['pasang'], $d['lepas']);
            $dbtable->insert($d);		
        }
	}
	$res=new ResponsePackage();
	$res->setStatus(ResponsePackage::$STATUS_OK);
	$res->setContent($list);
	echo json_encode($res->getPackage());
	return;
}

$header=array(	"No.","NRM","No.Reg","Nama","Masuk","Pulang","Ruang",
				"Pasang","Lepas","Durasi","Infeksi");
$uitable=new Table($header);
$uitable->setName("lap_cauti")
		->setActionEnable(false);

if(isset($_POST['command']) && $_POST['command']=="excel"){
	require_once "medical_record/snippet/create_excel_lap_cauti.php";
    return;
}

if(isset($_POST['command']) && $_POST['command']!=""){
	$dbtable=new DBTable($db, "smist_mr_lap_cauti");
	$dbtable->setOrder(" masuk ASC");
	if(isset($_POST['ruangan']) && $_POST['ruangan']!="" ){
        $dbtable->addCustomKriteria("ruangan","='". $_POST['ruangan']."'");
    }
	
	$adapter=new SimpleAdapter();
	$adapter->setUseNumber(true, "No.","back.");
	$adapter->add("NRM", "nrm_pasien","digit8");
    $adapter->add("No.Reg", "noreg_pasien","digit8");
	$adapter->add("Nama", "nama_pasien");
	$adapter->add("Ruang", "ruangan","unslug");    
	$adapter->add("Masuk", "masuk","date d M Y");
	$adapter->add("Pulang", "pulang","date d M Y");    
	$adapter->add("Pasang", "pasang","date d M Y");
	$adapter->add("Lepas", "lepas","date d M Y");
	$adapter->add("Durasi", "lama");
	$adapter->add("Infeksi", "infeksi");    
    $dbres=new DBResponder($dbtable, $uitable, $adapter);
	$hasil=$dbres->command($_POST['command']);
	echo json_encode($hasil);
	return;
}

$urjip=new ServiceConsumer($db, "get_urjip",array());
$urjip->setMode(ServiceConsumer::$MULTIPLE_MODE);
$urjip->setCached(true,"get_urjip");
$urjip->execute();
$content=$urjip->getContent();
$ruangan=array();
$ruangan[]=array("name"=>"--SEMUA--","value"=>"","default"=>"1");
foreach ($content as $autonomous=>$ruang){
    foreach($ruang as $nama_ruang=>$jip){
        if($jip[$nama_ruang]=="URI" || $jip[$nama_ruang]=="URJI"){
            $option=array();
            $option['value']=$nama_ruang;
            $option['name']=ArrayAdapter::format("unslug", $nama_ruang);
            $ruangan[]=$option;
        }
    }
}

$uitable->clearContent();
$uitable->addModal("dari", "date", "Dari", "")
		->addModal("sampai", "date", "Sampai", "")
        ->addModal("ruangan", "select", "Ruangan", $ruangan);
$action=new Button("","","View");
$action->setClass("btn-primary")
	   ->setIsButton(Button::$ICONIC)
	   ->setIcon("icon-white fa fa-circle-o-notch")
	   ->setAction("lap_cauti.rekaptotal()");
$view=new Button("","","View");
$view->setClass("btn-primary")
		->setIsButton(Button::$ICONIC)
		->setIcon("fa fa-refresh")
		->setAction("lap_cauti.view()");
$excel=new Button("","","Excel");
$excel->setClass("btn-primary")
	->setIsButton(Button::$ICONIC)
	->setIcon("fa fa-file-excel-o")
	->setAction("lap_cauti.excel()");

$btng=new ButtonGroup("");
$btng->addButton($action);
$btng->addButton($view);
$btng->addButton($excel);

$form=$uitable
	  ->getModal()
	  ->setTitle("Medical Record")
	  ->getForm()
	  ->addElement("",$btng);

$close=new Button("", "", "Batal");
$close	->addClass("btn-primary")
		->setIsButton(Button::$ICONIC_TEXT)
		->setIcon("fa fa-close")
		->setAction("lap_cauti.batal()");

$load=new LoadingBar("rekap_lap_cauti_bar", "");
$modal=new Modal("rekap_lap_cauti_modal", "", "Processing...");
$modal	->addHTML($load->getHtml(),"after")
		->addFooter($close);

echo $form->getHtml();
echo "<div class='clear'></div>";
echo "<div id='result_lap_cauti'>".$uitable->getHtml()."</div>";
echo $modal->getHtml();
echo addJS("framework/bootstrap/js/bootstrap-datepicker.js");
echo addJS("framework/smis/js/table_action.js");
echo addCSS("framework/bootstrap/css/datepicker.css");
echo addJS ( "base-js/smis-base-loading.js");
?>

<script type="text/javascript">
	var lap_cauti;
	var lap_cauti_karyawan;
	var lap_cauti_data;
	var IS_lap_cauti_RUNNING;
	$(document).ready(function(){
		$('.mydate').datepicker();
		lap_cauti=new TableAction("lap_cauti","medical_record","lap_cauti",new Array());
		lap_cauti.addRegulerData=function(data){
			data['dari']=$("#lap_cauti_dari").val();
			data['sampai']=$("#lap_cauti_sampai").val();
            data['ruangan']=$("#lap_cauti_ruangan").val();
			$("#dari_table_lap_cauti").html(getFormattedDate(data['dari']));
			$("#sampai_table_lap_cauti").html(getFormattedDate(data['sampai']));			
			return data;
		};

		lap_cauti.batal=function(){
			IS_lap_cauti_RUNNING=false;
			$("#rekap_lap_cauti_modal").modal("hide");
		};
		
		lap_cauti.afterview=function(json){
			if(json!=null){
				$("#kode_table_lap_cauti").html(json.nomor);
				$("#waktu_table_lap_cauti").html(json.waktu);
				lap_cauti_data=json;
			}
		};

		lap_cauti.rekaptotal=function(){
			if(IS_lap_cauti_RUNNING) return;
			$("#rekap_lap_cauti_bar").sload("true","Fetching total data",0);
			$("#rekap_lap_cauti_modal").modal("show");
			IS_lap_cauti_RUNNING=true;
			var d=this.getRegulerData();
			d['super_command']="total";
			$.post("",d,function(res){
				var all=getContent(res);
				if(all!=null) {
					var total=Number(all);
					lap_cauti.rekaploop(0,total);
				} else {
					$("#rekap_lap_cauti_modal").modal("hide");
					IS_lap_cauti_RUNNING=false;
				}
			});
		};

		lap_cauti.excel=function(){
			var d=this.getRegulerData();
			d['command']="excel";
			download(d);
		};

		lap_cauti.rekaploop=function(current,total){
			if(current>=total || !IS_lap_cauti_RUNNING) {
				$("#rekap_lap_cauti_modal").modal("hide");
				IS_lap_cauti_RUNNING=false;
				lap_cauti.view();
				return;
			}
			var d=this.getRegulerData();
			d['super_command']="limit";
			d['halaman']=current;
			$.post("",d,function(res){
				var ct=getContent(res);
				var u=ct[0]['nama_pasien']+"  "+ct[0]['nrm'];
				$("#rekap_lap_cauti_bar").sload("true"," Processing - "+u+"... [ "+(current+1)+" / "+total+" ] ",(current*100/total));
				setTimeout(function(){lap_cauti.rekaploop(++current,total)},300);
			});
		};
				
	});
</script>