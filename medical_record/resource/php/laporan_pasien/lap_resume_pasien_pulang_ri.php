<?php 
global $db;
global $user;
require_once 'smis-base/smis-include-service-consumer.php';
if(isset($_POST['super_command']) && $_POST['super_command']=="total") {
	require_once 'smis-libs-class/ServiceProviderList.php';
	$dbtable=new DBTable($db, "smis_mr_resume_ri");
	$dbtable->truncate();
	$serv=new ServiceConsumer($db, "get_mutasi_patient");
	$serv->addData("command", "count");
	$serv->addData("dari", $_POST['dari']);
	$serv->addData("sampai", $_POST['sampai']);	
	$serv->execute();
	$data=$serv->getContent();
	$res=new ResponsePackage();
	$res->setStatus(ResponsePackage::$STATUS_OK);
	$res->setContent($data);
	echo json_encode($res->getPackage());
	return;
}


if(isset($_POST['super_command']) && $_POST['super_command']=="limit") {
	loadLibrary("smis-libs-function-time");
	loadLibrary("smis-libs-function-medical");
	$data=array();
	$response=new ServiceConsumer($db, "get_mutasi_patient");
	$response->addData("command", "list");
	$response->addData("dari", $_POST['dari']);
	$response->addData("sampai", $_POST['sampai']);
	$response->addData("halaman", $_POST['halaman']);	
	$response->setMode(ServiceConsumer::$SINGLE_MODE);
	$response->execute();
	$list=$response->getContent();
	
	foreach($list as $x){
		$usia=medical_secondary_gol_umur($x['tgl_lahir'],$x['tanggal']);
		if($x['kelamin']=="0" || $x['kelamin']==0){
			/* PRIA */
			$masuk=is_date_in_beetween($_POST['dari'],$_POST['sampai'],$x['tanggal_inap'])?1:0;
			$query="INSERT INTO smis_mr_resume_ri (usia,masuk_l,masuk_t) 
					VALUES ('$usia','$masuk','$masuk') 
					ON DUPLICATE KEY UPDATE masuk_l=masuk_l+$masuk, masuk_t=masuk_t+$masuk; ";
			$db->query($query);
			if($x['carapulang']!=""){
				$hidup=strpos($x['carapulang'],"Mati")!==false?0:1;
				$mati=($hidup==1)?0:1;
				$mk48=strpos($x['carapulang'],"<=48")!==false?1:0;
				$ml48=strpos($x['carapulang'],">48")!==false?1:0;
				
				$query="INSERT INTO smis_mr_resume_ri (usia,hidup_l,mati_l,mk48_l,ml48_l,hidup_t,mati_t,mk48_t,ml48_t,keluar_l,keluar_t) 
					VALUES ('$usia','$hidup','$mati','$mk48','$ml48','$hidup','$mati','$mk48','$ml48','1','1') 
					ON DUPLICATE KEY UPDATE hidup_l=hidup_l+$hidup,
					mati_l=mati_l+$mati, mk48_l=mk48_l+$mk48, ml48_l=ml48_l+$ml48,
					hidup_t=hidup_t+$hidup,
					mati_t=mati_t+$mati, mk48_t=mk48_t+$mk48, ml48_t=ml48_t+$ml48,
					keluar_l=keluar_l+1, keluar_t=keluar_t+1; ";
				$db->query($query);
			}				
		}else{
			/* WANITA */
			$masuk=is_date_in_beetween($_POST['dari'],$_POST['sampai'],$x['tanggal_inap'])?1:0;
			$query="INSERT INTO smis_mr_resume_ri (usia,masuk_p,masuk_t) 
					VALUES ('$usia','$masuk','$masuk') 
					ON DUPLICATE KEY UPDATE masuk_p=masuk_p+$masuk, masuk_t=masuk_t+$masuk; ";
					$db->query($query);
			if($x['carapulang']!=""){
				$hidup=strpos($x['carapulang'],"Mati")!==false?0:1;
				$mati=($hidup==1)?0:1;
				$mk48=strpos($x['carapulang'],"<=48")!==false?1:0;
				$ml48=strpos($x['carapulang'],">48")!==false?1:0;
				$query="INSERT INTO smis_mr_resume_ri (usia,hidup_p,mati_p,mk48_p,ml48_p,hidup_t,mati_t,mk48_t,ml48_t,keluar_p,keluar_t) 
						VALUES ('$usia','$hidup','$mati','$mk48','$ml48','$hidup','$mati','$mk48','$ml48','1','1') 
						ON DUPLICATE KEY UPDATE hidup_p=hidup_p+$hidup,
						mati_p=mati_p+$mati, mk48_p=mk48_p+$mk48, ml48_p=ml48_p+$ml48,
						hidup_t=hidup_t+$hidup,
						mati_t=mati_t+$mati, mk48_t=mk48_t+$mk48, ml48_t=ml48_t+$ml48,
						keluar_p=keluar_p+1, keluar_t=keluar_t+1; ";
				$db->query($query);
			}		
		}
	}
	$res=new ResponsePackage();
	$res->setStatus(ResponsePackage::$STATUS_OK);
	$res->setContent($list);
	echo json_encode($res->getPackage());
	return;
}


$header0="<tr>
				<th rowspan='4'>Usia</th>
				<th colspan='3' rowspan='3'>Pasien Masuk</th>
				<th colspan='15'>Pasien Keluar</th>
			</tr>";

$header1="<tr>
				<th colspan='3' rowspan='2'>Total Keluar</th>
				<th colspan='3' rowspan='2'>Hidup</th>
				<th colspan='9'>Mati</th>
			</tr>";


$header2="<tr>
				<th colspan='3'>Total Mati</th>
				<th colspan='3'>Mati <= 48 Jam</th>
				<th colspan='3'>Mati > 48 Jam</th>
			</tr>";
$header3="<tr>
				<th>L</th>	<th>P</th> <th>L+P</th>
				<th>L</th>	<th>P</th> <th>L+P</th>
				<th>L</th>	<th>P</th> <th>L+P</th>
				<th>L</th>	<th>P</th> <th>L+P</th>
				<th>L</th>	<th>P</th> <th>L+P</th>
				<th>L</th>	<th>P</th> <th>L+P</th>
			</tr>";
$header=array(	"Usia","Masuk_L","Masuk_P","Masuk_T","Keluar_L","Keluar_P","Keluar_T","Hidup_L","Hidup_P",
				"Hidup_T","Mati_L","Mati_P","Mati_T","K48_L","K48_P","K48_T",
				"L48_L","L48_P","L48_T");
$uitable=new Table($header);
$uitable->addHeader("before",$header0);
$uitable->addHeader("before",$header1);
$uitable->addHeader("before",$header2);
$uitable->addHeader("before",$header3);
$uitable->setName("lap_resume_pasien_pulang_ri")
		->setActionEnable(false)
		->setFooterVisible(false)
		->setHeaderVisible(false);


if(isset($_POST['command']) && $_POST['command']!=""){
	$dbtable=new DBTable($db, "smis_mr_resume_ri");
	$dbtable->setOrder(" usia ASC");
	$dbtable->setShowAll(true);
	
	
	$adapter=new SummaryAdapter();
	$adapter->setRemoveZeroEnable(true);
	$adapter->add("Usia", "usia");
	$adapter->add("Masuk_L", "masuk_l");
	$adapter->add("Masuk_P", "masuk_p");
	$adapter->add("Masuk_T", "masuk_t");
	$adapter->add("Keluar_L", "keluar_l");
	$adapter->add("Keluar_P", "keluar_p");
	$adapter->add("Keluar_T", "keluar_t");
	$adapter->add("Hidup_L", "hidup_l");
	$adapter->add("Hidup_P", "hidup_p");
	$adapter->add("Hidup_T", "hidup_t");	
	$adapter->add("Mati_L", "mati_l");
	$adapter->add("Mati_P", "mati_p");
	$adapter->add("Mati_T", "mati_t");	
	$adapter->add("K48_L", "mk48_l");
	$adapter->add("K48_P", "mk48_p");
	$adapter->add("K48_T", "mk48_t");	
	$adapter->add("L48_L", "ml48_l");
	$adapter->add("L48_P", "ml48_p");
	$adapter->add("L48_T", "ml48_t");
	$adapter->addFixValue("Usia","<strong>Jumlah</strong>");
	$adapter->addSummary("Masuk_L", "masuk_l");
	$adapter->addSummary("Masuk_P", "masuk_p");
	$adapter->addSummary("Masuk_T", "masuk_t");	
	$adapter->addSummary("Hidup_L", "hidup_l");
	$adapter->addSummary("Hidup_P", "hidup_p");
	$adapter->addSummary("Hidup_T", "hidup_t");	
	$adapter->addSummary("Mati_L", "mati_l");
	$adapter->addSummary("Mati_P", "mati_p");
	$adapter->addSummary("Mati_T", "mati_t");	
	$adapter->addSummary("K48_L", "mk48_l");
	$adapter->addSummary("K48_P", "mk48_p");
	$adapter->addSummary("K48_T", "mk48_t");	
	$adapter->addSummary("L48_L", "ml48_l");
	$adapter->addSummary("L48_P", "ml48_p");
	$adapter->addSummary("L48_T", "ml48_t");
	$adapter->addSummary("Keluar_L", "keluar_l");
	$adapter->addSummary("Keluar_P", "keluar_p");
	$adapter->addSummary("Keluar_T", "keluar_t");
	
	$dbres=new DBResponder($dbtable, $uitable, $adapter);
	$hasil=$dbres->command($_POST['command']);
	echo json_encode($hasil);
	return;
}

$uitable->clearContent();
$uitable->addModal("dari", "date", "Dari", "")
		->addModal("sampai", "date", "Sampai", "");
$action=new Button("","","View");
$action->setClass("btn-primary")
	   ->setIsButton(Button::$ICONIC)
	   ->setIcon("icon-white fa fa-circle-o-notch")
	   ->setAction("lap_resume_pasien_pulang_ri.rekaptotal()");
$view=new Button("","","View");
$view->setClass("btn-primary")
		->setIsButton(Button::$ICONIC)
		->setIcon("fa fa-refresh")
		->setAction("lap_resume_pasien_pulang_ri.view()");
		
$btng=new ButtonGroup("");
$btng->addButton($action);
$btng->addButton($view);

$form=$uitable
	  ->getModal()
	  ->setTitle("Medical Record")
	  ->getForm()
	  ->addElement("",$btng);

$close=new Button("", "", "Batal");
$close	->addClass("btn-primary")
		->setIsButton(Button::$ICONIC_TEXT)
		->setIcon("fa fa-close")
		->setAction("lap_resume_pasien_pulang_ri.batal()");

$load=new LoadingBar("rekap_lap_resume_pasien_pulang_ri_bar", "");
$modal=new Modal("rekap_lap_resume_pasien_pulang_ri_modal", "", "Processing...");
$modal	->addHTML($load->getHtml(),"after")
		->addFooter($close);

echo $form->getHtml();
echo "<div class='clear'></div>";
echo "<div id='result_lap_resume_pasien_pulang_ri'>".$uitable->getHtml()."</div>";
echo $modal->getHtml();
echo addJS("framework/bootstrap/js/bootstrap-datepicker.js");
echo addJS("framework/smis/js/table_action.js");
echo addCSS("framework/bootstrap/css/datepicker.css");
echo addJS ( "base-js/smis-base-loading.js");
echo addJS ( "medical_record/resource/js/lap_resume_pasien_pulang.js",false);
?>