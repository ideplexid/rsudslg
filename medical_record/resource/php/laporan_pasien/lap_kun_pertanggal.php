<?php 
global $db;
global $user;
loadLibrary("smis-libs-function-time");

if(isset($_POST['super_command']) && $_POST['super_command']=="total") {
	$dari     = $_POST['dari'];
    $sampai   = $_POST['sampai'];
    $hasil=createDateRangeArray($dari,$sampai);
    $dbtable=new DBTable($db,"smist_mr_lap_kunjptgl");
    $dbtable->truncate();
    $result=array();
    $result["data"]=&$hasil;
    $result["total"]=count($hasil);
    $pack=new ResponsePackage();
    $pack->setStatus(ResponsePackage::$STATUS_OK);
    $pack->setContent($result);
    echo json_encode($pack->getPackage());
	return;
}

require_once "smis-base/smis-include-service-consumer.php";
$header = array('No.',"Ruangan","Tanggal","Masuk",'Rujukan','Carabayar');
$uitable= new Table($header);
$uitable->setName("lap_kun_pertanggal")
		->setActionEnable(false)
		->setFooterVisible(true);

if(isset($_POST['super_command']) && $_POST['super_command']=="limit") {
    loadLibrary("smis-libs-function-time");
    $sampai = add_date($_POST['tanggal'],1);
    $serv=new ServiceConsumer($db,"get_summary_patient_by_data");
    $serv->addData("dari",$_POST['tanggal']);
    $serv->addData("sampai",$sampai);
    $serv->addData("carabayar",$_POST['carabayar']);
    $serv->addData("origin",$_POST['origin']);
    $serv->setMode(ServiceConsumer::$CLEAN_BOTH_JOIN_SINGLE_ARRAY);
    $serv->execute();
    $content=$serv->getContent();
    $dbtable=new DBTable($db,"smist_mr_lap_kunjptgl");
    foreach($content as $x){
        $save=array();
        $save['ruangan']=$x['ruangan'];
        $save['masuk']=$x['masuk'];
        $save['rujukan']=$x['rujukan'];
        $save['tanggal']=$_POST['tanggal'];
        $save['carabayar']=$_POST['carabayar'];
        if($save['masuk']==0 && $save['rujukan']==0)
            continue;
        $dbtable->insert($save);
    }
    $pack=new ResponsePackage();
    $pack->setStatus(ResponsePackage::$STATUS_OK);
    echo json_encode($pack->getPackage());
    return;
}


if(isset($_POST['command']) && $_POST['command']!=""){
    $adapter=new SimpleAdapter();
    $adapter->setUseNumber(true,"No.","back.");
    $adapter->add("Tanggal","tanggal","date d M Y");
    $adapter->add("Ruangan","ruangan","unslug");
    $adapter->add("Masuk","masuk");
    $adapter->add("Rujukan","rujukan");
    $adapter->add("Carabayar","carabayar","unslug");
    require_once "medical_record/class/responder/LapKunjunganPerTanggalResponder.php";
    $dbtable=new DBTable($db,"smist_mr_lap_kunjptgl");
    $responder=new LapKunjunganPerTanggalResponder($dbtable,$uitable,$adapter);
    $data=$responder->command($_POST['command']);
    if($data!=null){
        echo json_encode($data);   
    }
    return;
}

$query="SELECT DISTINCT origin FROM smis_mr_diagnosa WHERE prop!='del' ";
$origin=new OptionBuilder();
$origin->add(" - Semua - ","",1);
$res=$db->get_result($query);
foreach($res as $x){
    if($x->origin==""){
        continue;
    }
    $origin->add(ArrayAdapter::slugFormat("unslug",$x->origin),$x->origin);
}

$service = new ServiceConsumer ( $db, "get_carabayar" );
$service->execute ();
$dataasuransi= $service->getContent ();
$adapter=new SelectAdapter("nama", "slug");
$jenis_pembayaran=$adapter->getContent($dataasuransi);
$jenis_pembayaran[]=array("name"=>"","value"=>"%","default"=>"1");


$uitable->addModal("dari", "date", "Dari", "")
		->addModal("sampai", "date", "Sampai", "")
        ->addModal("carabayar", "select", "Carabayar", $jenis_pembayaran)
        ->addModal ( "origin", "select", "Asal Data",$origin->getContent() );
$action=new Button("","","Proses");
$action->setClass("btn-primary")
	   ->setIsButton(Button::$ICONIC_TEXT)
	   ->setIcon("icon-white fa fa-circle-o-notch")
	   ->setAction("lap_kun_pertanggal.rekaptotal()");

$excel=new Button("","","Excel");
$excel ->setClass("btn-primary")
	   ->setIsButton(Button::$ICONIC_TEXT)
	   ->setIcon(" fa fa-file-excel-o")
	   ->setAction("lap_kun_pertanggal.excel()");

$btn_froup=new ButtonGroup("");
$btn_froup->addButton($action);
$btn_froup->addButton($excel);
$form=$uitable
	  ->getModal()
	  ->setTitle("registration")
	  ->getForm()
	  ->addElement("",$btn_froup);

$close=new Button("", "", "Batal");
$close	->addClass("btn-primary")
		->setIsButton(Button::$ICONIC_TEXT)
		->setIcon("fa fa-close")
		->setAction("lap_kun_pertanggal.batal()");

echo $form->getHtml();
echo "<div class='clear'></div>";
echo "<div id='result_lap_kun_pertanggal'>".$uitable->getHtml()."</div>";
echo addJS("framework/bootstrap/js/bootstrap-datepicker.js");
echo addJS("framework/smis/js/table_action.js");
echo addCSS("framework/bootstrap/css/datepicker.css");
echo addJS ( "base-js/smis-base-loading.js");
echo addJS("medical_record/resource/js/lap_kun_pertanggal.js",false);
?>