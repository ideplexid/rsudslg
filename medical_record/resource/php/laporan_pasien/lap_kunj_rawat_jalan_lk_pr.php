<?php

global $db;
require_once "smis-base/smis-include-service-consumer.php";
require_once "medical_record/class/adapter/LapKunjRawatJalanLkPrAdapter.php";

$slug_igd = getSettings($db, "smis-mr-slug-igd", "");
$not_igd = "not_igd";

$header0="<tr>
            <th rowspan='2' style='text-align: center; vertical-align: middle;'>Ruangan</th>
            <th colspan='2'>Januari</th>
            <th colspan='2'>Februari</th>
            <th colspan='2'>Maret</th>
            <th colspan='2'>April</th>
            <th colspan='2'>Mei</th>
            <th colspan='2'>Juni</th>
            <th colspan='2'>Juli</th>
            <th colspan='2'>Agustus</th>
            <th colspan='2'>September</th>
            <th colspan='2'>Oktober</th>
            <th colspan='2'>November</th>
            <th colspan='2'>Desember</th>
            <th colspan='2'>Jumlah</th>
        </tr>";

$header1="<tr>
            <th>L</th>
            <th>P</th>
            <th>L</th>
            <th>P</th>
            <th>L</th>
            <th>P</th>
            <th>L</th>
            <th>P</th>
            <th>L</th>
            <th>P</th>
            <th>L</th>
            <th>P</th>
            <th>L</th>
            <th>P</th>
            <th>L</th>
            <th>P</th>
            <th>L</th>
            <th>P</th>
            <th>L</th>
            <th>P</th>
            <th>L</th>
            <th>P</th>
            <th>L</th>
            <th>P</th>
            <th>L</th>
            <th>P</th>
        </tr>";
$header = array("ruangan", 
                "jan_lk", "jan_pr",
                "feb_lk", "feb_pr",
                "mar_lk", "mar_pr",
                "apr_lk", "apr_pr",
                "mei_lk", "mei_pr",
                "jun_lk", "jun_pr",
                "jul_lk", "jul_pr",
                "ags_lk", "ags_pr",
                "sep_lk", "sep_pr",
                "okt_lk", "okt_pr",
                "nov_lk", "nov_pr",
                "des_lk", "des_pr",
                "total_lk", "total_pr"
                );
$uitable = new Table($header, "", NULL);
$uitable->setName('lap_kunj_rawat_jalan_lk_pr');
$uitable->setAction(false);
$uitable->setHeaderVisible(false);
$uitable->setFooterVisible(false);
$uitable->addHeader("before",$header0);
$uitable->addHeader("before",$header1);

if (isset($_POST ['command']) && $_POST ['command'] != "") {
    if($_POST['command'] == 'list') {
        if(isset($_POST['sumber']) && $_POST['sumber'] != "") {
            $serv=new ServiceConsumer($db,"get_total_patient_lk_pr_in_year", NULL, "registration");
            $serv->addData("tahun",$_POST['tahun']);
            $serv->execute();
            $data=$serv->getContent();
            $arr = array();
            if(isset($_POST['ruangan']) && $_POST['ruangan'] != '') {
                $i = 0;
                foreach($data as $key => $value) {            
                    if($data[$key]['status'] != 'deactived'){
                        if($_POST['ruangan'] == 'not_igd') {
                            if($data[$key]['ruangan'] != $slug_igd) {
                                $arr[] = $data[$key];
                            }
                        }
                        else if($data[$key]['ruangan'] == $_POST['ruangan']) {
                            $arr[] = $data[$key];
                        }
                    }
                    $i++;
                }
            } else {
                $i = 0;
                foreach($data as $key => $value) {            
                    if($data[$key]['status'] != 'deactived'){
                        $arr[] = $data[$key];
                    }
                    $i++;
                }
            }
            $adapter=new LapKunjRawatJalanLkPrAdapter();
            $ready=$adapter->getContent($arr);
            
            $uitable->setContent($ready);
            $content['list'] = $uitable->getBodyContent();
            $response = new ResponsePackage ();
            $response->setAlertVisible ( false );
            $response->setStatus ( ResponsePackage::$STATUS_OK );
            $response->setContent($content);
            echo json_encode ( $response->getPackage () );
            return;
        } else {
            $serv=new ServiceConsumer($db,"get_kunjungan_rj_lk_pr");
            $serv->addData("tahun",$_POST['tahun']);
            $serv->setMode(ServiceConsumer::$CLEAN_BOTH_JOIN_SINGLE_ARRAY);
            $serv->execute();
            $data=$serv->getContent();
            $arr = array();
            if(isset($_POST['ruangan']) && $_POST['ruangan'] != '') {
                $i = 0;
                foreach($data as $key => $value) {            
                    if($data[$key]['urji'] != 'URI' && sizeof($data[$key]) != 2){
                        if($_POST['ruangan'] == 'not_igd') {
                            if($data[$key]['ruangan'] != $slug_igd) {
                                $arr[] = $data[$key];
                            }
                        } else if($data[$key]['ruangan'] == $_POST['ruangan']) {
                            $arr[] = $data[$key];
                        }
                    }
                    $i++;
                }
            } else {
                $i = 0;
                foreach($data as $key => $value) {            
                    if($data[$key]['urji'] != 'URI' && sizeof($data[$key]) != 2){
                        $arr[] = $data[$key];
                    }
                    $i++;
                }
            }
            $adapter=new LapKunjRawatJalanLkPrAdapter();
            $ready=$adapter->getContent($arr);
            
            $uitable->setContent($ready);
            $content['list'] = $uitable->getBodyContent();
            $response = new ResponsePackage ();
            $response->setAlertVisible ( false );
            $response->setStatus ( ResponsePackage::$STATUS_OK );
            $response->setContent($content);
            echo json_encode ( $response->getPackage () );
            return;
        }
    }
    if($_POST['command'] == 'excel') {
        if(isset($_POST['sumber']) && $_POST['sumber'] != ""){
            $serv=new ServiceConsumer($db,"get_total_patient_lk_pr_in_year", NULL, "registration");
            $serv->addData("tahun",$_POST['tahun']);
            $serv->execute();
            $data=$serv->getContent();
            $arr = array();
            if(isset($_POST['ruangan']) && $_POST['ruangan'] != '') {
                $i = 0;
                foreach($data as $key => $value) {            
                    if($data[$key]['status'] != 'deactived'){
                        if($_POST['ruangan'] == $not_igd) {
                            if($data[$key]['ruangan'] != $slug_igd) {
                                $arr[] = $data[$key];
                            }
                        }
                        else if($data[$key]['ruangan'] == $_POST['ruangan']) {
                            $arr[] = $data[$key];
                        }
                    }
                    $i++;
                }
            } else {
                $i = 0;
                foreach($data as $key => $value) {            
                    if($data[$key]['status'] != 'deactived'){
                        $arr[] = $data[$key];
                    }
                    $i++;
                }
            }
        } else {
            $serv=new ServiceConsumer($db,"get_kunjungan_rj_lk_pr");
            $serv->addData("tahun",$_POST['tahun']);
            $serv->setMode(ServiceConsumer::$CLEAN_BOTH_JOIN_SINGLE_ARRAY);
            $serv->execute();
            $data=$serv->getContent();
            $arr = array();
            if(isset($_POST['ruangan']) && $_POST['ruangan'] != '') {
                $i = 0;
                foreach($data as $key => $value) {            
                    if($data[$key]['urji'] != 'URI' && $data[$key]['ruangan'] == $_POST['ruangan'] && sizeof($data[$key]) != 2){
                        $arr[] = $data[$key];
                    }
                    $i++;
                }
            } else {
                $i = 0;
                foreach($data as $key => $value) {            
                    if($data[$key]['urji'] != 'URI' && sizeof($data[$key]) != 2){
                        $arr[] = $data[$key];
                    }
                    $i++;
                }
            }
        }
        
        require_once ("smis-libs-out/php-excel/PHPExcel.php");
        $file = new PHPExcel ();
        $file->getProperties ()->setCreator ( "SIMRS" );
        $file->getProperties ()->setLastModifiedBy ( "Rumah Sakit Bagian Administrator" );
        $file->getProperties ()->setTitle ( "Laporan Kunjungan Rawat Jalan" );
        $file->getProperties ()->setSubject ( "Laporan Kunjungan Rawat Jalan" );
        $file->getProperties ()->setDescription ( "Laporan Kunjungan Rawat Jalan Generated From system" );
        $file->getProperties ()->setKeywords ( "Laporan Kunjungan Rawat Jalan" );
        $file->getProperties ()->setCategory ( "Laporan Kunjungan Rawat Jalan" );
        $sheet = $file->getActiveSheet ();
        
        $index = 1;
        $sheet->setCellValue("A".$index,"LAPORAN JUMLAH KUNJUNGAN RAWAT JALAN");
        $sheet->mergeCells('A'.$index.':'.'AA'.$index);
        $sheet->getStyle('A'.$index.':'.'AA'.$index)->getFont()->setBold(true);
        $sheet->getStyle('A'.$index.':'.'AA'.$index)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $index++;
        $sheet->setCellValue("A".$index,"TAHUN ".$_POST['tahun']."");
        $sheet->mergeCells('A'.$index.':'.'AA'.$index);
        $sheet->getStyle('A'.$index.':'.'AA'.$index)->getFont()->setBold(true);
        $sheet->getStyle('A'.$index.':'.'AA'.$index)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $index = $index + 2;
        
        $m_index = $index + 1;
        $sheet->mergeCells("A".$index.":A".$m_index)->setCellValue("A".$index,"Ruangan");
        $sheet->mergeCells("B".$index.":C".$index)->setCellValue("B".$index,"Januari");    
        $sheet->mergeCells("D".$index.":E".$index)->setCellValue("D".$index,"Februari");    
        $sheet->mergeCells("F".$index.":G".$index)->setCellValue("F".$index,"Maret");
        $sheet->mergeCells("H".$index.":I".$index)->setCellValue("H".$index,"April");
        $sheet->mergeCells("J".$index.":K".$index)->setCellValue("J".$index,"Mei");
        $sheet->mergeCells("L".$index.":M".$index)->setCellValue("L".$index,"Juni");
        $sheet->mergeCells("N".$index.":O".$index)->setCellValue("N".$index,"Juli");
        $sheet->mergeCells("P".$index.":Q".$index)->setCellValue("P".$index,"Agustus");
        $sheet->mergeCells("R".$index.":S".$index)->setCellValue("R".$index,"September");
        $sheet->mergeCells("T".$index.":U".$index)->setCellValue("T".$index,"Oktober");
        $sheet->mergeCells("V".$index.":W".$index)->setCellValue("V".$index,"November");
        $sheet->mergeCells("X".$index.":Y".$index)->setCellValue("X".$index,"Desember");
        $sheet->mergeCells("Z".$index.":AA".$index)->setCellValue("Z".$index,"Total");
        $sheet->getStyle('A'.$index.':'.'AA'.$index)->getFont()->setBold(true);
        $sheet->getStyle('A'.$index.':'.'AA'.$index)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('A'.$index.':'.'AA'.$index)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $index++;
        
        $sheet->setCellValue("B".$index,"L");
        $sheet->setCellValue("C".$index,"P");
        $sheet->setCellValue("D".$index,"L");
        $sheet->setCellValue("E".$index,"P");
        $sheet->setCellValue("F".$index,"L");
        $sheet->setCellValue("G".$index,"P");
        $sheet->setCellValue("H".$index,"L");
        $sheet->setCellValue("I".$index,"P");
        $sheet->setCellValue("J".$index,"L");
        $sheet->setCellValue("K".$index,"P");
        $sheet->setCellValue("L".$index,"L");
        $sheet->setCellValue("M".$index,"P");
        $sheet->setCellValue("N".$index,"L");
        $sheet->setCellValue("O".$index,"P");
        $sheet->setCellValue("P".$index,"L");
        $sheet->setCellValue("Q".$index,"P");
        $sheet->setCellValue("R".$index,"L");
        $sheet->setCellValue("S".$index,"P");
        $sheet->setCellValue("T".$index,"L");
        $sheet->setCellValue("U".$index,"P");
        $sheet->setCellValue("V".$index,"L");
        $sheet->setCellValue("W".$index,"P");
        $sheet->setCellValue("X".$index,"L");
        $sheet->setCellValue("Y".$index,"P");
        $sheet->setCellValue("Z".$index,"L");
        $sheet->setCellValue("AA".$index,"P");
        $sheet->getStyle('A'.$index.':'.'AA'.$index)->getFont()->setBold(true);
        $sheet->getStyle('A'.$index.':'.'AA'.$index)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        
        $sum_start = $index + 1;
        foreach($arr as $x) {
            $index++;
            $sheet->setCellValue("A".$index, ArrayAdapter::format("unslug", $x["ruangan"]));
            $sheet->setCellValue("B".$index, $x["jan_lk"]);    
            $sheet->setCellValue("C".$index, $x["jan_pr"]);
            $sheet->setCellValue("D".$index, $x["feb_lk"]);    
            $sheet->setCellValue("E".$index, $x["feb_pr"]);
            $sheet->setCellValue("F".$index, $x["mar_lk"]);    
            $sheet->setCellValue("G".$index, $x["mar_pr"]);
            $sheet->setCellValue("H".$index, $x["apr_lk"]);    
            $sheet->setCellValue("I".$index, $x["apr_pr"]);
            $sheet->setCellValue("J".$index, $x["mei_lk"]);    
            $sheet->setCellValue("K".$index, $x["mei_pr"]);
            $sheet->setCellValue("L".$index, $x["jun_lk"]);    
            $sheet->setCellValue("M".$index, $x["jun_pr"]);
            $sheet->setCellValue("N".$index, $x["jul_lk"]);    
            $sheet->setCellValue("O".$index, $x["jul_pr"]);
            $sheet->setCellValue("P".$index, $x["ags_lk"]);    
            $sheet->setCellValue("Q".$index, $x["ags_pr"]);
            $sheet->setCellValue("R".$index, $x["sep_lk"]);    
            $sheet->setCellValue("S".$index, $x["sep_pr"]);
            $sheet->setCellValue("T".$index, $x["okt_lk"]);    
            $sheet->setCellValue("U".$index, $x["okt_pr"]);
            $sheet->setCellValue("V".$index, $x["nov_lk"]);    
            $sheet->setCellValue("W".$index, $x["nov_pr"]);
            $sheet->setCellValue("X".$index, $x["des_lk"]);    
            $sheet->setCellValue("Y".$index, $x["des_pr"]);
            $sheet->setCellValue("Z".$index, $x["total_lk"]);    
            $sheet->setCellValue("AA".$index, $x["total_pr"]);
            $sheet->getStyle('A'.$index.':'.'AA'.$index)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $no++;
        }
        $sum_end = $index;
        
        $index++;
        $sheet->setCellValue("A".$index, "TOTAL");
        $sheet->setCellValue("B".$index, "=sum(B".$sum_start.":B".$sum_end.")");    
        $sheet->setCellValue("C".$index, "=sum(C".$sum_start.":C".$sum_end.")");
        $sheet->setCellValue("D".$index, "=sum(D".$sum_start.":D".$sum_end.")");    
        $sheet->setCellValue("E".$index, "=sum(E".$sum_start.":E".$sum_end.")");
        $sheet->setCellValue("F".$index, "=sum(F".$sum_start.":F".$sum_end.")");    
        $sheet->setCellValue("G".$index, "=sum(G".$sum_start.":G".$sum_end.")");
        $sheet->setCellValue("H".$index, "=sum(H".$sum_start.":H".$sum_end.")");    
        $sheet->setCellValue("I".$index, "=sum(I".$sum_start.":I".$sum_end.")");
        $sheet->setCellValue("J".$index, "=sum(J".$sum_start.":J".$sum_end.")");    
        $sheet->setCellValue("K".$index, "=sum(K".$sum_start.":K".$sum_end.")");
        $sheet->setCellValue("L".$index, "=sum(L".$sum_start.":L".$sum_end.")");    
        $sheet->setCellValue("M".$index, "=sum(M".$sum_start.":M".$sum_end.")");
        $sheet->setCellValue("N".$index, "=sum(N".$sum_start.":N".$sum_end.")");    
        $sheet->setCellValue("O".$index, "=sum(O".$sum_start.":O".$sum_end.")");
        $sheet->setCellValue("P".$index, "=sum(P".$sum_start.":P".$sum_end.")");    
        $sheet->setCellValue("Q".$index, "=sum(Q".$sum_start.":Q".$sum_end.")");
        $sheet->setCellValue("R".$index, "=sum(R".$sum_start.":R".$sum_end.")");    
        $sheet->setCellValue("S".$index, "=sum(S".$sum_start.":S".$sum_end.")");
        $sheet->setCellValue("T".$index, "=sum(T".$sum_start.":T".$sum_end.")");    
        $sheet->setCellValue("U".$index, "=sum(U".$sum_start.":U".$sum_end.")");
        $sheet->setCellValue("V".$index, "=sum(V".$sum_start.":V".$sum_end.")");    
        $sheet->setCellValue("W".$index, "=sum(W".$sum_start.":W".$sum_end.")");
        $sheet->setCellValue("X".$index, "=sum(X".$sum_start.":X".$sum_end.")");    
        $sheet->setCellValue("Y".$index, "=sum(Y".$sum_start.":Y".$sum_end.")");
        $sheet->setCellValue("Z".$index, "=sum(Z".$sum_start.":Z".$sum_end.")");    
        $sheet->setCellValue("AA".$index, "=sum(AA".$sum_start.":AA".$sum_end.")");
        $sheet->getStyle('A'.$index.':'.'AA'.$index)->getFont()->setBold(true);
        $sheet->getStyle('A'.$index.':'.'AA'.$index)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        
        foreach(range('A','AA') as $columnID) {
            $sheet->getColumnDimension($columnID)->setAutoSize(true);
        }
        
        header ( 'Content-Type: application/vnd.ms-excel' );
        header ( 'Content-Disposition: attachment;filename="Laporan Kunjungan Rawat Jalan.xls"' );
        header ( 'Cache-Control: max-age=0' );
        $writer = PHPExcel_IOFactory::createWriter ( $file, 'Excel5' );
        $writer->save ( 'php://output' );
		return;
    }
}

/*==============Filter Dropdown Sumber Data================*/
$sumber = array();
$sumber[0]['value'] = 'registration';
$sumber[0]['name'] = 'Dari Pendaftaran Saja';
$sumber[1]['value'] = '';
$sumber[1]['name'] = ' - SEMUA - ';
$uitable->addModal( "sumber", "select", "Sumber Data", $sumber );
/*========================================================*/

/*==============Filter Dropdown Tahun================*/
$tahun = new ServiceConsumer($db,"get_thn_kunjungan_rj");
$tahun->execute();
$content = $tahun->getContent();
$thn = array();
foreach($content as $con){
    $option = array();
    $option['value'] = $con["tahun"];
    $option['name'] = $con["tahun"];
    $thn[] =$option;
}
$uitable->addModal( "tahun", "select", "Tahun", $thn );
/*====================================================*/

/*================Filter Dropdown Ruangan-==============*/
$ruang = new ServiceConsumer($db, "get_total_patient_in_year");
$ruang->setMode(ServiceConsumer::$CLEAN_BOTH_JOIN_SINGLE_ARRAY);
$ruang->execute();
$content = $ruang->getContent();
$ruangan = array();
foreach($content as $key => $value) {
    if($content[$key]['urji'] != 'URI' && $content[$key]['urji'] != '') {
        $option = array();
        $option['value'] = $content[$key]['ruangan'];
        $option['name'] = ArrayAdapter::format("unslug", $content[$key]['ruangan']);
        $ruangan[] = $option;
    }
}
$option['value']=$not_igd;
$option['name']="SEMUA KECUALI IGD";
$ruangan[] = $option;
$option1['value']="";
$option1['name']=" - SEMUA - ";
$ruangan[] = $option1;

$uitable->addModal( "ruangan", "select", "Layanan", $ruangan );
/*=========================================================*/

$modal = $uitable->getModal ();
$form=$modal->getForm();

$v = new Button("", "", "");
$v->addClass("btn-primary")
		->setIsButton(Button::$ICONIC)
		->setIcon("fa fa-refresh")
		->setAction("lap_kunj_rawat_jalan_lk_pr.view()");
$form->addElement("", $v);

$excel = new Button("", "", "");
$excel->addClass("btn-primary")
		->setIsButton(Button::$ICONIC)
		->setIcon("fa fa-file-excel-o")
		->setAction("lap_kunj_rawat_jalan_lk_pr.excel()");
$form->addElement("", $excel);

echo $form->getHtml();
echo $uitable->getHtml ();
echo $modal->getHtml ();

echo addJS ( "framework/smis/js/table_action.js" );

?>

<script type="text/javascript">
var lap_kunj_rawat_jalan_lk_pr;
$(document).ready(function(){
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
	var column=new Array();
	lap_kunj_rawat_jalan_lk_pr = new TableAction("lap_kunj_rawat_jalan_lk_pr","medical_record","lap_kunj_rawat_jalan_lk_pr",column);
    lap_kunj_rawat_jalan_lk_pr.addViewData = function(a) {
        a['sumber'] = $("#lap_kunj_rawat_jalan_lk_pr_sumber").val();
        a['tahun'] = $("#lap_kunj_rawat_jalan_lk_pr_tahun").val();
        a['ruangan'] = $("#lap_kunj_rawat_jalan_lk_pr_ruangan").val();
        return a;
    };
    
    lap_kunj_rawat_jalan_lk_pr.excel = function() {
        var data = this.getRegulerData();
        data['command'] = 'excel';
        data['sumber'] = $("#lap_kunj_rawat_jalan_lk_pr_sumber").val();
        data['tahun'] = $("#lap_kunj_rawat_jalan_lk_pr_tahun").val();
        data['ruangan'] = $("#lap_kunj_rawat_jalan_lk_pr_ruangan").val();
        postForm(data);
    }
});
</script>