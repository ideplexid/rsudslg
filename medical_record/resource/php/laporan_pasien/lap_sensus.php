<?php 

/**
 * this data used for handling 
 * sensus of patient
 * the data will be separated in 3 process
 *  - count the total
 *  - took detail of every patient
 *  - convert data into excel
 * 
 * @author 		: Nurul Huda
 * @license 	: Apache v3
 * @copyright 	: goblooge@gmail.com
 * @snippet 	: snippet/get_sensus_detail.php
 * 				  snipeet/get_sensus_total.php
 * 				  snippet/get_sensus_excel.php
 * @database 	: smis_mr_sensus_harian
 * @since		: 29 Oct 2016
 * @version		: 1.0.0
 * */

global $db;
require_once 'smis-base/smis-include-service-consumer.php';

$header=array();
$header[]="No.";
$header[]="No Reg";
$header[]="No Profile";
$header[]="NRM";
$header[]="Nama";
$header[]="Alamat";
$header[]="L/P";
$header[]="Diagnosa Masuk";
$header[]="Diagnosa Ruang";
$header[]="Cara Pulang";
$header[]="Cara Bayar";
$header[]="Asal";
$header[]="Ruangan";
$header[]="Pindah Ke";
$header[]="Umur";
$header[]="Kelas";
$header[]="Masuk";
$header[]="Keluar";
$header[]="Durasi";
$header[]="DPJP";
$header[]="ICD Masuk";
$header[]="ICD Keluar";
$header[]="Kunjungan RS";

$uitable=new Table($header);
$uitable->setName("lap_sensus")
		->setActionEnable(false)
		->setFooterVisible(true);

if(isset($_POST['command']) && $_POST['command']!=""){
	$dbtable=new DBTable($db, "smis_mr_sensus");
		
	$adapter=new SimpleAdapter();
	$adapter->add("No.","id","back.");
	$adapter->add("No Reg","noreg_pasien","only-digit6");
	$adapter->add("No Profile","profile_number");
	$adapter->add("NRM","nrm_pasien","only-digit6");
	$adapter->add("Nama","nama_pasien");
	$adapter->add("Alamat","alamat");
	$adapter->add("L/P","jk", "trivial_0_L_P");
	$adapter->add("Diagnosa Masuk","diagnosa_masuk");
	$adapter->add("Diagnosa Ruang","diagnosa_ruang");
	$adapter->add("Cara Pulang","carapulang");
	$adapter->add("Cara Bayar","carabayar");
    $adapter->add("Asal","asal_ruangan","unslug");
    $adapter->add("Ruangan","ruangan","unslug");
	$adapter->add("Pindah Ke","pindah_ke","unslug");
	$adapter->add("Umur","umur");
	$adapter->add("Kelas","kelas","unslug");
	$adapter->add("Masuk","tgl_masuk","date d M Y H:i");
	$adapter->add("Keluar","tgl_pulang","date d M Y H:i");
	$adapter->add("Durasi","durasi","back Hari");
    $adapter->add("DPJP","dpjp");
    $adapter->add("ICD Masuk","icd_masuk");
    $adapter->add("ICD Keluar","icd_ruang");
    $adapter->add("Kunjungan RS","kunjungan_rs");
	
	$dbres=new DBResponder($dbtable, $uitable, $adapter);
	$hasil=$dbres->command($_POST['command']);
	echo json_encode($hasil);
	return;
}

$pelayanan=new OptionBuilder();
$pelayanan->add(" - SEMUA - ","",1);
$pelayanan->add("URI", "1", "0");
$pelayanan->add("URJ", "0", "0");

if(isset($_POST['super_command']) && $_POST['super_command'] == 'pelayanan') {
    if($_POST['pelayanan'] == "0") {
        $urjip=new ServiceConsumer($db, "get_urjip",array());
        $urjip->setMode(ServiceConsumer::$MULTIPLE_MODE);
        $urjip->setCached(true,"get_urjip");
        $urjip->execute();
        $content=$urjip->getContent();
        $ruangan=array();
        foreach ($content as $autonomous=>$ruang){
            foreach($ruang as $nama_ruang=>$jip){
                if($jip[$nama_ruang] != "UP" && $jip[$nama_ruang] != "URI"){
                    $option=array();
                    $option['value']=$nama_ruang;
                    $option['name']=ArrayAdapter::format("unslug", $nama_ruang);
                    $ruangan[]=$option;
                }
            }
        }
        echo json_encode($ruangan);
        return;
    } else if($_POST['pelayanan'] == "1") {
        $urjip=new ServiceConsumer($db, "get_urjip",array());
        $urjip->setMode(ServiceConsumer::$MULTIPLE_MODE);
        $urjip->setCached(true,"get_urjip");
        $urjip->execute();
        $content=$urjip->getContent();
        $ruangan=array();
        foreach ($content as $autonomous=>$ruang){
            foreach($ruang as $nama_ruang=>$jip){
                if($jip[$nama_ruang] != "UP" && $jip[$nama_ruang] == "URI"){
                    $option=array();
                    $option['value']=$nama_ruang;
                    $option['name']=ArrayAdapter::format("unslug", $nama_ruang);
                    $ruangan[]=$option;
                }
            }
        }
        echo json_encode($ruangan);
        return;
    } else {
        $ruangan = array();
        echo json_encode($ruangan);
        return;
    }
} else {
    $ruangan = array();
    $ruangan[]=$option;
}

$uitable->clearContent();
$uitable->addModal("dari", "datetime", "Dari", "")
		->addModal("sampai", "datetime", "Sampai", "")
        ->addModal ("pelayanan", "select", "Layanan", $pelayanan->getContent())
		->addModal("ruangan", "select", "Ruangan", $ruangan);
$action=new Button("","","Tarik");
$action->setClass("btn-primary")
	   ->setIsButton(Button::$ICONIC_TEXT)
	   ->setIcon("icon-white fa fa-circle-o-notch")
	   ->setAction("lap_sensus.rekaptotal()");
$view=new Button("","","Tampilkan");
$view->setClass("btn-primary")
		->setIsButton(Button::$ICONIC_TEXT)
		->setIcon("fa fa-refresh")
		->setAction("lap_sensus.view()");
$excel=new Button("","","Excel");
$excel->setClass("btn-primary")
		->setIsButton(Button::$ICONIC_TEXT)
		->setIcon("fa fa-file-excel-o")
		->setAction("lap_sensus.excel()");


$btng=new ButtonGroup("");
$btng->addButton($action);
$btng->addButton($view);
$btng->addButton($excel);

$form=$uitable
	  ->getModal()
	  ->setTitle("Medical Record")
	  ->getForm()
	  ->addElement("",$btng);

$close=new Button("", "", "Batal");
$close	->addClass("btn-primary")
		->setIsButton(Button::$ICONIC_TEXT)
		->setIcon("fa fa-close")
		->setAction("lap_sensus.batal()");

$load=new LoadingBar("rekap_lap_sensus_bar", "");
$modal=new Modal("rekap_lap_sensus_modal", "", "Processing...");
$modal	->addHTML($load->getHtml(),"after")
		->addFooter($close);

echo $form->getHtml();
echo "<div class='clear'></div>";
echo "<div id='result_lap_sensus'>".$uitable->getHtml()."</div>";
echo $modal->getHtml();
echo addJS("framework/bootstrap/js/bootstrap-datetimepicker.js");
echo addJS("framework/smis/js/table_action.js");
echo addCSS("framework/bootstrap/css/bootstrap-datetimepicker.css");
echo addJS ( "base-js/smis-base-loading.js");
echo addJS ( "medical_record/resource/js/lap_sensus.js",false);
?>