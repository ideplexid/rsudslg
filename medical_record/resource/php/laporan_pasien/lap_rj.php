<?php 
global $db;
global $user;
require_once 'smis-base/smis-include-service-consumer.php';
if(isset($_POST['super_command']) && $_POST['super_command']=="total") {
	require_once 'smis-libs-class/ServiceProviderList.php';
	$dbtable=new DBTable($db, "smis_mr_lap_rj");
	$dbtable->truncate();
	$serv=new ServiceConsumer($db, "get_patient_rawat_jalan");
	$serv->addData("command", "count");
	$serv->addData("dari", $_POST['dari']);
	$serv->addData("sampai", $_POST['sampai']);	
	$serv->execute();
	$data=$serv->getContent();
	$res=new ResponsePackage();
	$res->setStatus(ResponsePackage::$STATUS_OK);
	$res->setContent($data);
	echo json_encode($res->getPackage());
	return;
}


if(isset($_POST['super_command']) && $_POST['super_command']=="limit") {
	$data=array();
	$response=new ServiceConsumer($db, "get_patient_rawat_jalan");
	$response->addData("command", "list");
	$response->addData("dari", $_POST['dari']);
	$response->addData("sampai", $_POST['sampai']);
	$response->addData("halaman", $_POST['halaman']);	
	$response->setMode(ServiceConsumer::$SINGLE_MODE);
	$response->execute();
	$list=$response->getContent();
	$dbtable=new DBTable($db, "smis_mr_lap_rj");
	$diagnosa=new DBTable($db,"smis_mr_diagnosa");
	$igd=getSettings($db, "smis-rs-igd-mr", "igd");
	$icu=getSettings($db, "smis-rs-icu-mr", "icu");
	$ok=getSettings($db, "smis-rs-ok-mr", "ok");
	$vk=getSettings($db, "smis-rs-vk-mr", "vk");
	
	foreach($list as $x){		
		$d['nrm_pasien']=$x['nrm'];
		$d['nama_pasien']=$x['nama_pasien'];
		$d['tgl_kunj']=$x['tanggal'];		
		$d['ruangan']=$x['jenislayanan'];
		$d['barulama']=$x['barulama'];
		$d['jk']=$x['kelamin'];
		$d['kelurahan']=$x['nama_kelurahan'];
		$d['kecamatan']=$x['nama_kecamatan'];
		$d['kabupaten']=$x['nama_kabupaten'];
		$d['propinsi']=$x['nama_provinsi'];
		$query=" SELECT diagnosa,nama_dokter FROM smis_mr_diagnosa WHERE noreg_pasien='".$x['id']."' ";
		$a=$db->get_row($query);
		$d['diagnosa']=$a->diagnosa;
		$d['dokter']=$a->dokter;
		$dbtable->insert($d);		
	}
	
	$res=new ResponsePackage();
	$res->setStatus(ResponsePackage::$STATUS_OK);
	$res->setContent($list);
	echo json_encode($res->getPackage());
	return;
}

$header=array(	"No.","Tgl Kunj","NRM","Nama","Ruangan",
				"Diagnosa","Dokter","L/B","JK",
				"Kelurahan","Kecamatan","Kabupaten","Propinsi");
$uitable=new Table($header);
$uitable->setName("lap_rj")
		->setActionEnable(false)
		->setFooterVisible(false);

if(isset($_POST['command']) && $_POST['command']!=""){
	$dbtable=new DBTable($db, "smis_mr_lap_rj");
	$dbtable->setOrder(" tgl_kunj ASC");
	$dbtable->setShowAll(true);
	
	$adapter=new SimpleAdapter();
	$adapter->setUseNumber(true, "No.","back.");
	$adapter->add("NRM", "nrm_pasien");
	$adapter->add("Nama", "nama_pasien");
	$adapter->add("Ruangan", "ruangan","unslug");
	$adapter->add("Tgl Kunj", "tgl_kunj","date d/m/Y H:i");
	$adapter->add("Diagnosa", "diagnosa");
	$adapter->add("Dokter", "dokter");	
	$adapter->add("L/B", "barulama","trivial_0_Baru_Lama");
	$adapter->add("JK", "jk","trivial_0_Laki_Pria");
	$adapter->add("Kelurahan", "kelurahan");
	$adapter->add("Kecamatan", "kecamatan");
	$adapter->add("Kabupaten", "kabupaten");
	$adapter->add("Propinsi", "propinsi");
	
	require_once "medical_record/class/responder/LapRJResponder.php";
	$dbres = new LapRJResponder($dbtable, $uitable, $adapter);
	$hasil=$dbres->command($_POST['command']);
	echo json_encode($hasil);
	return;
}

$uitable->clearContent();
$uitable->addModal("dari", "date", "Dari", "")
		->addModal("sampai", "date", "Sampai", "");
$action=new Button("","","View");
$action->setClass("btn-primary")
	   ->setIsButton(Button::$ICONIC)
	   ->setIcon("icon-white fa fa-circle-o-notch")
	   ->setAction("lap_rj.rekaptotal()");
$view=new Button("","","View");
$view->setClass("btn-primary")
		->setIsButton(Button::$ICONIC)
		->setIcon("fa fa-refresh")
		->setAction("lap_rj.view()");
$excel=new Button("","","Excel");
$excel->setClass("btn-primary")
	->setIsButton(Button::$ICONIC)
	->setIcon("fa fa-file-excel-o")
	->setAction("lap_rj.excel()");

$btng=new ButtonGroup("");
$btng->addButton($action);
$btng->addButton($view);
$btng->addButton($excel);

$form=$uitable
	  ->getModal()
	  ->setTitle("Medical Record")
	  ->getForm()
	  ->addElement("",$btng);

$close=new Button("", "", "Batal");
$close	->addClass("btn-primary")
		->setIsButton(Button::$ICONIC_TEXT)
		->setIcon("fa fa-close")
		->setAction("lap_rj.batal()");

$load=new LoadingBar("rekap_lap_rj_bar", "");
$modal=new Modal("rekap_lap_rj_modal", "", "Processing...");
$modal	->addHTML($load->getHtml(),"after")
		->addFooter($close);

echo $form->getHtml();
echo "<div class='clear'></div>";
echo "<div id='result_lap_rj'>".$uitable->getHtml()."</div>";
echo $modal->getHtml();
echo addJS("framework/bootstrap/js/bootstrap-datepicker.js");
echo addJS("framework/smis/js/table_action.js");
echo addCSS("framework/bootstrap/css/datepicker.css");
echo addJS ( "base-js/smis-base-loading.js");
?>

<script type="text/javascript">
	var lap_rj;
	var lap_rj_karyawan;
	var lap_rj_data;
	var IS_lap_rj_RUNNING;
	$(document).ready(function(){
		$('.mydate').datepicker();
		lap_rj=new TableAction("lap_rj","medical_record","lap_rj",new Array());
		lap_rj.addRegulerData=function(data){
			data['dari']=$("#lap_rj_dari").val();
			data['sampai']=$("#lap_rj_sampai").val();
			$("#dari_table_lap_rj").html(getFormattedDate(data['dari']));
			$("#sampai_table_lap_rj").html(getFormattedDate(data['sampai']));			
			return data;
		};

		lap_rj.batal=function(){
			IS_lap_rj_RUNNING=false;
			$("#rekap_lap_rj_modal").modal("hide");
		};
		
		lap_rj.afterview=function(json){
			if(json!=null){
				$("#kode_table_lap_rj").html(json.nomor);
				$("#waktu_table_lap_rj").html(json.waktu);
				lap_rj_data=json;
			}
		};

		lap_rj.rekaptotal=function(){
			if(IS_lap_rj_RUNNING) return;
			$("#rekap_lap_rj_bar").sload("true","Fetching total data",0);
			$("#rekap_lap_rj_modal").modal("show");
			IS_lap_rj_RUNNING=true;
			var d=this.getRegulerData();
			d['super_command']="total";
			$.post("",d,function(res){
				var all=getContent(res);
				if(all!=null) {
					var total=Number(all);
					lap_rj.rekaploop(0,total);
				} else {
					$("#rekap_lap_rj_modal").modal("hide");
					IS_lap_rj_RUNNING=false;
				}
			});
		};

		lap_rj.excel=function(){
			var d=this.getRegulerData();
			d['command']="excel";
			download(d);
		};

		lap_rj.rekaploop=function(current,total){
			if(current>=total || !IS_lap_rj_RUNNING) {
				$("#rekap_lap_rj_modal").modal("hide");
				IS_lap_rj_RUNNING=false;
				lap_rj.view();
				return;
			}
			var d=this.getRegulerData();
			d['super_command']="limit";
			d['halaman']=current;
			$.post("",d,function(res){
				var ct=getContent(res);
				var u=ct[0]['nama_pasien']+"  "+ct[0]['nrm'];
				$("#rekap_lap_rj_bar").sload("true"," Processing - "+u+"... [ "+(current+1)+" / "+total+" ] ",(current*100/total));
				setTimeout(function(){lap_rj.rekaploop(++current,total)},300);
			});
		};
				
	});
</script>