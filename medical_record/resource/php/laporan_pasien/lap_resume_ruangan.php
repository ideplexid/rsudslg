<?php
global $db;
show_error();
require_once 'smis-base/smis-include-service-consumer.php';
if( isset ( $_POST ['super_command'] ) &&  $_POST ['super_command']=="collect_ruang" ){
	require_once "smis-libs-class/ServiceProviderList.php";
	$query="truncate smis_mr_resume_ruangan";
	$db->query($query);
	$list_iklin=new ServiceProviderList($db, "get_resume_ruangan");
	$list_iklin->execute();
	$content=$list_iklin->getContent();
	$pack=new ResponsePackage();
	$pack->setContent($content);
	$pack->setStatus(ResponsePackage::$STATUS_OK);
	echo json_encode($pack->getPackage());
	return;
}

if (isset ( $_POST ['super_command'] ) && $_POST ['super_command'] == "sync") {
	$service = new ServiceConsumer ( $db, "get_resume_ruangan",NULL,$_POST['ruang'] );
	$service->setMode(ServiceConsumer::$SINGLE_MODE);
	$service->addData("dari", $_POST['dari']);
	$service->addData("sampai", $_POST['sampai']);
	$service->execute ();
	$content = $service->getContent ();
	$dbtable=new DBTable($db, "smis_mr_resume_ruangan");
	$dbtable->insert($content);
	
	$response = new ResponsePackage ();
	$response->setAlertVisible ( false );
	$response->setStatus ( ResponsePackage::$STATUS_OK );
	$response->setContent($content);
	echo json_encode ( $response->getPackage () );
	return;
}


$head=array ("No.",'Ruangan',"Berlangsung",'Hidup',"Mati <= 48","Mati > 48",
				"Pulang Paksa","Kabur","Rawat Inap","Pindah Kamar",
				"Poli Lain","RS Lain","Perujuk","Total Datang","Tidak Datang","Total");
$uitable = new Table ( $head, "", NULL, true );
$uitable->setName ( "lap_resume_ruangan" );
$uitable->setFooterVisible(false);
$uitable->setAction(false);
if (isset ( $_POST ['command'] )) {	
	$adapter = new SummaryAdapter();
	$adapter->addFixValue("Ruangan", "<strong>Total</strong>");
	$adapter->addSummary ( "Berlangsung", "berlangsung" );
	$adapter->addSummary ( "Hidup", "hidup" );
	$adapter->addSummary ( "Mati <= 48", "mati_k48" );
	$adapter->addSummary ( "Mati > 48", "mati_l48" );
	$adapter->addSummary ( "Pulang Paksa", "pulang_paksa" );
	$adapter->addSummary ( "Kabur", "kabur" );
	$adapter->addSummary ( "Rawat Inap", "rawat_inap" );
	$adapter->addSummary ( "Pindah Kamar", "pindah_kamar" );
	$adapter->addSummary ( "Poli Lain", "poli_lain" );
	$adapter->addSummary ( "RS Lain", "rs_lain" );
	$adapter->addSummary ( "Perujuk", "perujuk" );
	$adapter->addSummary ( "Total", "total" );
	$adapter->addSummary ( "Tidak Datang", "tdk_datang" );
	$adapter->addSummary ( "Total Datang", "total_datang" );
	
	$adapter->setUseNumber(true, "No.","back.");
	$adapter->add ( "Ruangan", "ruangan","unslug");
	$adapter->add ( "Berlangsung", "berlangsung" );
	$adapter->add ( "Hidup", "hidup" );
	$adapter->add ( "Mati <= 48", "mati_k48" );
	$adapter->add ( "Mati > 48", "mati_l48" );
	$adapter->add ( "Pulang Paksa", "pulang_paksa" );
	$adapter->add ( "Kabur", "kabur" );
	$adapter->add ( "Rawat Inap", "rawat_inap" );
	$adapter->add ( "Pindah Kamar", "pindah_kamar" );
	$adapter->add ( "Poli Lain", "poli_lain" );
	$adapter->add ( "RS Lain", "rs_lain" );
	$adapter->add ( "Perujuk", "perujuk" );
	$adapter->add ( "Total", "total" );
	$adapter->add ( "Tidak Datang", "tdk_datang" );
	$adapter->add ( "Total Datang", "total_datang" );
	
	$dbtable = new DBTable ( $db, "smis_mr_resume_ruangan" );
	$dbtable->setShowAll(true);
	$dbres = new DBResponder ( $dbtable, $uitable, $adapter );
	$data = $dbres->command ( $_POST ['command'] );
	echo json_encode ( $data );
	return;
}

$uitable->addModal ( "dari", "date", "Dari", "" );
$uitable->addModal ( "sampai", "date", "Sampai", "" );
$modal = $uitable->getModal ();
$form=$modal->getForm();
$view=new Button("", "", "");
$view->addClass("btn-primary")
	 ->setIsButton(Button::$ICONIC)
	 ->setIcon("fa fa-refresh")
	 ->setAction("lap_resume_ruangan.collect_ruang()");
$form->addElement("", $view);

$load=new LoadingBar("rekap_mr_bar", "");
$modal=new Modal("rekap_mr_modal", "", "Processing...");
$modal	->addHTML($load->getHtml(),"after");

echo $form->getHtml();
echo $uitable->getHtml ();
echo $modal->getHtml ();
echo addJS ( "framework/smis/js/table_action.js" );
echo addJS ( "framework/bootstrap/js/bootstrap-datepicker.js" );
echo addCSS ( "framework/bootstrap/css/datepicker.css" );
echo addJS ( "base-js/smis-base-loading.js");
?>
<script type="text/javascript">

var lap_resume_ruangan;
//var employee;
$(document).ready(function(){
	$(".mydate").datepicker();
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
	var column=new Array('id','nama','keterangan','slug');
	lap_resume_ruangan=new TableAction("lap_resume_ruangan","medical_record","lap_resume_ruangan",column);
	lap_resume_ruangan.sync=function(set,curpoint){
		var total=set.length;
		var data=this.getRegulerData();
		var current=set[curpoint];	
		data['ruang']=current.value;	
		data['super_command']="sync";
		data['dari']=$("#lap_resume_ruangan_dari").val();
		data['sampai']=$("#lap_resume_ruangan_sampai").val();		
		$.post('',data,function(res){
			
			$("#rekap_mr_bar").sload("true","[ "+curpoint+" / "+total+" ] "+current.name+"...",(curpoint*100/total));
			var json=getContent(res);
			$("#layanan_table_list").append(json.row);
			
			if(curpoint+1>=total){
				$("#rekap_mr_modal").modal("hide");
				lap_resume_ruangan.view();
				return;
			} else {
				curpoint++;
				setTimeout(function(){
					lap_resume_ruangan.sync(set,curpoint);
				},500);
			}
			
		});
	};

	lap_resume_ruangan.collect_ruang=function(){
		var data=this.getRegulerData();
		data['super_command']="collect_ruang";
		showLoading();
		$.post('',data,function(res){
			dismissLoading();
			var json=getContent(res);
			if(json==null){
				return;
			}else{
				$("#rekap_mr_bar").sload("true","Loading Data",0);
				$("#rekap_mr_modal").modal("show");
				$("#lap_resume_ruangan_list").html("");
				lap_resume_ruangan.sync(json,0);
			}
			
		});
	}
	

});
</script>
