<?php
require_once "smis-base/smis-include-service-consumer.php";
require_once "medical_record/class/responder/ProlanisResponder.php";
$uitable = new Table ( array("No.","Tanggal","Nama","No. BPJS","Alamat","Telp","GDS","GDP","GDPP","Tensi","Diagnosa"), "", NULL, false );
$uitable->setName("lap_prolanis");
if (isset ( $_POST ['command'] )) {
	$adapter = new SimpleAdapter();
    $adapter->setUseNumber(true,"No.","back.");
    $adapter->add("Tanggal","tanggal","date d M Y");
	$adapter->add("Nama","nama");
	$adapter->add("No. BPJS","no_bpjs");
	$adapter->add("Alamat","alamat");
	$adapter->add("Telp","no_telp");
	$adapter->add("GDS","gds");
	$adapter->add("GDP","gdp");
    $adapter->add("GDPP","gdpp");
    $adapter->add("Tensi","tensi");
    $adapter->add("Diagnosa","diagnosa");
	$dbtable = new DBTable ( $db, "smis_mr_prolanis" );
	
	if (isset ( $_POST ['dari'] ) && isset ( $_POST ['sampai'] ) && $_POST ['dari'] != "" && $_POST ['sampai'] != "") {
		$dbtable->addCustomKriteria ( "'" . $_POST ['dari'] . "'", "<=tanggal" );
		$dbtable->addCustomKriteria ( "'" . $_POST ['sampai'] . "'", ">tanggal" );
		$dbtable->setShowAll ( true );
	}
    if(isset ( $_POST ['origin'] ) && $_POST ['origin'] != "") {
        $dbtable->addCustomKriteria ( " origin ", "='".$_POST['origin']."'" );
    }
    
	$dbtable->setOrder ( " tanggal ASC " );
	//$dbres = new DBResponder ( $dbtable, $uitable, $adapter );
    $dbres = new ProlanisResponder ( $dbtable, $uitable, $adapter );
	$data = $dbres->command ( $_POST ['command'] );
    if($data!=null){
        echo json_encode ( $data );
	}
	return;
}

$query="SELECT DISTINCT origin FROM smis_mr_diagnosa WHERE prop!='del' ";
$option=new OptionBuilder();
$option->add(" - Semua - ","",1);

$res=$db->get_result($query);
foreach($res as $x){
    if($x->origin==""){
        continue;
    }
    $option->add(ArrayAdapter::slugFormat("unslug",$x->origin),$x->origin);
}

$uitable->addModal ( "dari", "datetime", "Dari", "" );
$uitable->addModal ( "sampai", "datetime", "Sampai", "" );
$uitable->addModal( "origin", "select", "Asal", $option->getContent() );
$modal = $uitable->getModal ();
$form = $modal->getForm ();

$button = new Button ( "", "", "" );
$button->setIsButton ( Button::$ICONIC );
$button->setIcon ( "fa fa-refresh" );
$button->setClass("btn-primary");
$button->setAction ( "lap_prolanis.view()" );
$form->addElement ( "", $button );
$button = new Button ( "", "", "" );
$button->setIsButton ( Button::$ICONIC );
$button->setClass("btn-primary");
$button->setIcon ( "fa fa-print" );
$button->setAction ( "smis_print($('#print_table_lap_prolanis').html())" );
$form->addElement ( "", $button );
$button = new Button ( "", "", "" );
$button->setIsButton ( Button::$ICONIC );
$button->setClass("btn-primary");
$button->setIcon ( "fa fa-download" );
$button->setAction ( "lap_prolanis.excel()" );
$form->addElement ( "", $button );
echo $form->getHtml ();

echo $uitable->getHtml ();
echo addJS ( "framework/smis/js/table_action.js" );
echo addJS ( "framework/bootstrap/js/bootstrap-datetimepicker.js" );
echo addCSS ( "framework/bootstrap/css/bootstrap-datetimepicker.css" );

?>
<script type="text/javascript">
var lap_prolanis;
$(document).ready(function(){
	$('.mydatetime').datetimepicker({minuteStep:1});
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
	var column=new Array('id','nama','keterangan','slug');
	lap_prolanis=new TableAction("lap_prolanis","medical_record","lap_prolanis",column);
	lap_prolanis.getRegulerData=function(){
		var reg_data={	
				page:this.page,
				action:this.action,
				super_command:this.super_command,
				prototype_name:this.prototype_name,
				prototype_slug:this.prototype_slug,
				prototype_implement:this.prototype_implement,
				dari:$("#"+this.prefix+"_dari").val(),
				sampai:$("#"+this.prefix+"_sampai").val(),
                origin:$("#"+this.prefix+"_origin").val()
				};
		return reg_data;
	};
});
</script>
