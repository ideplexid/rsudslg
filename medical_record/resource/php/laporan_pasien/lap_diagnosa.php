<?php
loadLibrary("smis-libs-function-math");
require_once ("smis-framework/smis/template/ModulTemplate.php");
require_once ("smis-base/smis-include-service-consumer.php");
require_once 'smis-libs-hrd/EmployeeResponder.php';

 class LapDiagnosa extends ModulTemplate {
	
     public function __construct($db, $polislug, $poliname) {
		parent::__construct ();
		$this->polislug = $polislug;
		$this->poliname = $poliname;
		$this->db = $db;
		$uitable = new Table ( array(), "", NULL, false );
		$uitable->setDelButtonEnable(false);
		$uitable->setEditButtonEnable(false);
		$uitable->setReloadButtonEnable(false);
		$uitable->setAddButtonEnable(false);
		$this->uitable=$uitable;
		$header=array ("Nomor Profile",'Tanggal','No. Reg','NRM',"Pasien", "Gol Umur","Jenis Kelamin","Dokter",'Diagnosa',"ICD","Penyakit","Ruangan","Carabayar","Alamat","Propinsi","Kabupaten","Kecamatan","Kelurahan" );
		$this->uitable->setHeader ( $header );
		$this->uitable->setName ( "lap_diagnosa" );
	}
    
   
	private function detail(Table &$uitable,ArrayAdapter &$adapter,DBTable &$dbtable){
		$adapter->addFixValue("Tanggal","<total>Total</total>");
		$adapter->addSummary("Total","harga_total","money Rp.");
		$adapter->setUseNumber(true, "No.","back.");
        $adapter->add("Sebutan", "sebutan");
        $adapter->add("Nomor Profile", "profile_number");
		$adapter->add("Tanggal", "waktu","date d M Y");
		$adapter->add("Pasien", "nama_pasien");
		$adapter->add("Gol Umur", "gol_umur");
		$adapter->add("No. Reg", "noreg_pasien");
		$adapter->add("NRM", "nrm_pasien");
        $adapter->add("Carabayar", "carabayar","unslug");
		$adapter->add ( "Dokter", "nama_dokter" );
		$adapter->add ( "Diagnosa", "diagnosa" );
		$adapter->add ( "ICD", "kode_icd" );
		$adapter->add ( "Penyakit", "nama_icd" );
		$adapter->add ( "Kelas", "kelas", "unslug" );
		$adapter->add ( "Ruangan", "ruangan","unslug" );
		$adapter->add ( "Jenis Kelamin", "jk", "trivial_0_L_P" );
		$adapter->add ( "Jumlah", "jumlah" );	
		$adapter->add ( "Alamat", "alamat" );	
        $adapter->add ( "Propinsi", "propinsi" );
		$adapter->add ( "Kabupaten", "kabupaten" );
		$adapter->add ( "Kecamatan", "kecamatan" );
		$adapter->add ( "Kelurahan", "kelurahan" );
		$adapter->addSummary("Jumlah","jumlah");
		/*if($_POST['command']=="excel"){
			$adapter->add("Tanggal", "waktu","date d/m/Y");
			$adapter->addFixValue("Tanggal","Total");
			$adapter->addSummary("Jumlah","jumlah");
            $adapter->add ("Sebutan", "sebutan");
            $adapter->add ("Nomor Profile", "profile_number");
            $adapter->add ("Nama Pasien", "nama_pasien");
            $adapter->add ("Gol Umur", "gol_umur");
            $adapter->add ("No. Reg", "");
            $adapter->add ("NRM", "");
            $adapter->add ( "Dokter", "" );
            $adapter->add ( "Diagnosa", "" );
            $adapter->add ( "ICD", "" );
            $adapter->add ( "Penyakit", "" );
            $adapter->add ( "Kelas", "" );
            $adapter->add ( "Ruangan", "" );
            $adapter->add ( "L/P", "" );
            $adapter->add ("Jumlah", "" );
            $adapter->add ("Cara Bayar", "" );
            $adapter->add ( "Alamat", "alamat" );	
            $adapter->add ( "Propinsi", "" );
            $adapter->add ( "Kabupaten", "" );
            $adapter->add ( "Kecamatan", "" );
            $adapter->add ( "Kelurahan", "" );  
            $adapter->add("Carabayar", "");         
            $adapter->addFixValue("No", "");
		}*/
	}
    
    private function makeSyntax($group){
        $syntax=array();
        $syntax['waktu']        =in_array("tanggal",$group)     !==false?" tanggal "    :"'-'";
        $syntax['carabayar']    =in_array("carabayar",$group)   !==false?" carabayar "  :"'-'";
        $syntax['nama_dokter']  =in_array("nama_dokter",$group) !==false?" nama_dokter ":"'-'";
        $syntax['kode_icd']     =in_array("kode_icd",$group)    !==false?" kode_icd "   :"'-'";
        $syntax['nama_icd']     =in_array("kode_icd",$group)    !==false?" nama_icd "   :"'-'";
        $syntax['ruangan']      =in_array("ruangan",$group)     !==false?" ruangan "    :"'-'";        
        $syntax['propinsi']     =in_array("propinsi",$group)    !==false?" propinsi "   :"'-'";
        $syntax['kabupaten']    =in_array("kabupaten",$group)   !==false?" kabupaten "  :"'-'";
        $syntax['kecamatan']    =in_array("kecamatan",$group)   !==false?" kecamatan "  :"'-'";
        $syntax['kelurahan']    =in_array("kelurahan",$group)   !==false?" kelurahan "  :"'-'";
        $syntax['jumlah']       =" sum(1) ";
        $syntax['noreg_pasien'] =" '-' ";
        $syntax['nrm_pasien']   =" '-' ";
        $syntax['nama_pasien']  =" '-' ";
        
        $total=count($syntax);
        $result="SELECT ";
        foreach($syntax as $x=>$v){
            $total--;
            $result.=" ".$v." as ".$x;
            if($total>0){
                $result.=",";
            }
        }
        $result.=" FROM smis_mr_diagnosa";
        return $result;
    }
	
    
    public function getCaraBayar(){
		$ser=new ServiceConsumer($this->db,"get_carabayar",NULL,"registration");
		$ser->execute();
		$content=$ser->getContent();
		$result=array();
		$result[]=array("name"=>"","value"=>"%","default"=>"1");
		foreach($content as $x){
			$result[]=array("name"=>$x['nama'],"value"=>$x['slug'],"default"=>"0");
		}
		return $result;
	}
    
    protected function group_by($map){
        $keys=array_keys($map);
        $option=new OptionBuilder();
        $option->add("","","1");
        foreach($map as $k=>$v){
            $option->add($k,$v,"0");
        }
        return $option;
    }
    
    protected function order_by($map){
        $option=new OptionBuilder();
        $option->add("","","1");
        $keys=array_keys($map);
        $array=combine($keys,false,true,1);
        foreach($array as $x){
            $index=count($x);
            $name="Order By ";
            $value="";
            foreach($x as $i){
                $index--;
                $name.=$i;
                $value.=$map[$i];
                if($index>0){
                    $name.=" - ";
                    $value.=",";
                }
            }
            $option->add($name,$value,"0",count($x)." Orders Combination");
        }
        return $option;
    }
	
	public function command($command) {		
		$this->dbtable = new DBTable ( $this->db, 'smis_mr_diagnosa' );
		$this->dbtable->addCustomKriteria(NULL,"tanggal>='".$_POST['dari']."'");
		$this->dbtable->addCustomKriteria(NULL,"tanggal<'".$_POST['sampai']."'");
		$this->dbtable->addCustomKriteria("carabayar"," LIKE '".$_POST['carabayar']."'");
		
		if($_POST['nama_dokter']!="")   $this->dbtable->addCustomKriteria("nama_dokter"," = '".$_POST['nama_dokter']."'");
        if($_POST['ruangan']!="")       $this->dbtable->addCustomKriteria("ruangan"," = '".$_POST['ruangan']."'");		
        if($_POST['diagnosa']!="")      $this->dbtable->addCustomKriteria("kode_icd"," = '".$_POST['diagnosa']."'");		
        if($_POST['propinsi']!="")      $this->dbtable->addCustomKriteria("propinsi"," LIKE '%".$_POST['propinsi']."'");
		if($_POST['kabupaten']!="")     $this->dbtable->addCustomKriteria("kabupaten"," LIKE '%".$_POST['kabupaten']."'");
		if($_POST['kecamatan']!="")     $this->dbtable->addCustomKriteria("kecamatan"," LIKE '%".$_POST['kecamatan']."'");
		if($_POST['kelurahan']!="")     $this->dbtable->addCustomKriteria("kelurahan"," LIKE '%".$_POST['kelurahan']."'");
		
        
		$this->dbtable->setOrder ( " tanggal ASC " );
        require_once 'medical_record/class/adapter/LapDiagnosaAdapter.php';
		//$adapter=new SummaryAdapter();
		$adapter=new LapDiagnosaAdapter();
		
		if(isset($_POST['grup']) && $_POST['grup']!="" && ( count($_POST['grup'])>0 && !in_array("",$_POST['grup']) || count($_POST['grup'])>1) ){
            $GRUP=array_diff($_POST['grup'],array(""));
            $qv=$this->makeSyntax($GRUP);            
            $qc="SELECT COUNT(*) as total FROM smis_mr_diagnosa";
            $this->dbtable->setPreferredQuery(true,$qv,$qc);
            $this->dbtable->setMaskingCount(true," SELECT count(*) as total FROM ([query])X ");
            $this->dbtable->setGroupBy(true,implode(",",$GRUP));
            $this->dbtable->setUseWhereforView(true);
            //$this->detail($this->uitable,$adapter,$this->dbtable);
            if(!in_array('tanggal',$GRUP)){
                $adapter->add("Tanggal","waktu");
            }
        }else{
			
			$qv="SELECT * , tanggal as waktu, 1 as jumlah	FROM smis_mr_diagnosa";			
			$qc="SELECT COUNT(*) as total FROM smis_mr_diagnosa";
			$this->dbtable->setPreferredQuery(true,$qv,$qc);
			$this->dbtable->setUseWhereforView(true);
			//$this->detail($this->uitable,$adapter,$this->dbtable);
		}
		
		if(isset($_POST['orderby']) && $_POST['orderby']!="" ){
			$this->dbtable->setOrder($_POST['orderby'],true);
		}
		
		if(isset($_POST['fix_carabayar'])){
			$this->fixCaraBayar();
		}
        require_once 'medical_record/class/responder/LapDiagnosaResponder.php';
		$this->dbres = new LapDiagnosaResponder( $this->dbtable, $this->uitable, $adapter,$this->polislug );
		
		
		$data = $this->dbres->command ( $_POST ['command'] );
		if($data!=null){
			echo json_encode ( $data );
		}
	}
	
	protected function fixCarabayar(){		
		$query="ALTER TABLE `smis_mr_diagnosa` 
				ADD `carabayar` VARCHAR(32) NOT NULL AFTER `id`;";
		$this->db->query($query);
		$query="UPDATE smis_mr_diagnosa a
				LEFT JOIN smis_rgv_layananpasien b
				ON a.noreg_pasien=b.id
				SET a.carabayar=b.carabayar WHERE a.carabayar='' ";
		$this->db->query($query);
	}
	
	
	public function superCommand($super_command) {

		$dkadapter = new SimpleAdapter ();
		$dkadapter->add ( "Jabatan", "nama_jabatan" );
		$dkadapter->add ( "Nama", "nama" );
		$dkadapter->add ( "NIP", "nip" );
		$header=array ('Nama','Jabatan',"NIP" );
		$dktable = new Table ( $header);
		$dktable->setName ( "dokter_lap_diagnosa" );
		$dktable->setModel ( Table::$SELECT );
		$dokter = new EmployeeResponder ( $this->db, $dktable, $dkadapter, "dokter" );
		
        $dkadapter = new SimpleAdapter ();
		$dkadapter->add ( "ICD", "icd" );
		$dkadapter->add ( "Nama", "nama" );
		$dkadapter->add ( "Sebab", "sebab" );
		$header=array ('ICD','Nama',"Sebab" );
		$dktable = new Table ( $header);
		$dktable->setName ( "diagnosa_lap_diagnosa" );
		$dktable->setModel ( Table::$SELECT );
        $dbtable=new DBTable($this->db,"smis_mr_icd");
		$diagnosa = new DBResponder ( $dbtable, $dktable, $dkadapter );
        
        $dkadapter = new SimpleAdapter ();
		$dkadapter->add ( "ID", "id" );
		$dkadapter->add ( "Nama", "nama" );
		$header=array ("ID",'Nama' );
		$dktable = new Table ( $header);
		$dktable->setName ( "propinsi_lap_diagnosa" );
		$dktable->setModel ( Table::$SELECT );
		$propinsi = new ServiceResponder ( $this->db,$dktable, $dkadapter, "get_propinsi","registration" );
        
        $dkadapter = new SimpleAdapter ();
		$dkadapter->add ( "ID", "id" );
		$dkadapter->add ( "Nama", "nama" );
        $dkadapter->add ( "Propinsi", "propinsi" );
		$header=array ("ID",'Nama',"Propinsi" );
		$dktable = new Table ( $header);
		$dktable->setName ( "kabupaten_lap_diagnosa" );
		$dktable->setModel ( Table::$SELECT );
		$kabupaten = new ServiceResponder ( $this->db,$dktable, $dkadapter, "get_kabupaten","registration" );
       
        $dkadapter = new SimpleAdapter ();
		$dkadapter->add ( "ID", "id" );
		$dkadapter->add ( "Nama", "nama" );
        $dkadapter->add ( "Propinsi", "propinsi" );
		$dkadapter->add ( "Kabupaten", "kabupaten" );
		$header=array ("ID",'Nama',"Propinsi","Kabupaten" );
		$dktable = new Table ( $header);
		$dktable->setName ( "kecamatan_lap_diagnosa" );
		$dktable->setModel ( Table::$SELECT );
		$kecamatan = new ServiceResponder ( $this->db,$dktable, $dkadapter, "get_kecamatan","registration" );
       
        $dkadapter = new SimpleAdapter ();
		$dkadapter->add ( "ID", "id" );
		$dkadapter->add ( "Nama", "nama" );
        $dkadapter->add ( "Propinsi", "propinsi" );
		$dkadapter->add ( "Kabupaten", "kabupaten" );
		$dkadapter->add ( "Kecamatan", "kecamatan" );
		$header=array ("ID",'Nama',"Propinsi","Kabupaten","Kecamatan" );
		$dktable = new Table ( $header);
		$dktable->setName ( "kelurahan_lap_diagnosa" );
		$dktable->setModel ( Table::$SELECT );
		$kelurahan = new ServiceResponder ( $this->db,$dktable, $dkadapter, "get_kelurahan","registration" );
       
        
		$super = new SuperCommand ();
		$super->addResponder ( "dokter_lap_diagnosa", $dokter );
        $super->addResponder ( "diagnosa_lap_diagnosa", $diagnosa );
		$super->addResponder ( "propinsi_lap_diagnosa", $propinsi );
        $super->addResponder ( "kabupaten_lap_diagnosa", $kabupaten );
        $super->addResponder ( "kecamatan_lap_diagnosa", $kecamatan );
		$super->addResponder ( "kelurahan_lap_diagnosa", $kelurahan );
		$init = $super->initialize ();
		if ($init != null) {
			echo $init;
			return;
		}
	}
	
	/* when it's star build */
	public function phpPreload() {
		
        loadClass ( "ServiceProviderList" );
		$service = new ServiceProviderList ( $this->db, "push_antrian" );
		$service->execute ();
		$data = $service->getContent ();
		$ruangan = array();
		foreach ($data as $d) {
			if (get_entity_name($this->db, $d['value']) != null) {
				$ruangan[] = array(
					"name" 	=> get_entity_name($this->db, $d['value']),
					"value"	=> $d['value']
				);
			} else {
				$ruangan[] = array(
					"name" 	=> $d['name'],
					"value"	=> $d['value']
				);
			}
		}
        $ruangan[]=array("name"=>"","value"=>"","default"=>"1");
        
        $syntax['waktu']=strpos($group,"tanggal")!==false?" tanggal ":"'-'";
        $syntax['carabayar']=strpos($group,"carabayar")!==false?" carabayar ":"'-'";
        $syntax['nama_dokter']=strpos($group,"nama_dokter")!==false?" nama_dokter ":"'-'";
        $syntax['kode_icd']=strpos($group,"kode_icd")!==false?" kode_icd ":"'-'";
        $syntax['ruangan']=strpos($group,"ruangan")!==false?" ruangan ":"'-'";        
        $syntax['propinsi']=strpos($group,"")!==false?" propinsi ":"'-'";
        $syntax['kabupaten']=strpos($group,"")!==false?" kabupaten ":"'-'";
        $syntax['kecamatan']=strpos($group,"")!==false?" kecamatan ":"'-'";
        $syntax['kelurahan']=strpos($group,"")!==false?" kelurahan ":"'-'";
        
        
		$map['Tanggal']="tanggal";
        $map['Carabayar']="carabayar";
        $map['Dokter']="nama_dokter";
        $map['ICD']="kode_icd";
        $map['Ruangan']="ruangan";
        $map['Propinsi']="propinsi";
        $map['Kabupaten']="kabupaten";
        $map['Kecamatan']="kecamatan";
        $map['Kelurahan']="kelurahan";
        $group=$this->group_by($map);
		
        $omap['Tanggal']="tanggal ASC ";
        $omap['Carabayar']="carabayar ASC ";
        //$omap['Dokter']="nama_dokter ASC";
        $omap['ICD']=" nama_icd ASC";
        $omap['Ruangan']=" ruangan ASC ";
        $omap['Wilayah']=" propinsi ASC, kabupaten ASC, kecamatan ASC, kelurahan ASC ";
        $omap['Jumlah']=" jumlah DESC ";
        $ordered=$this->order_by($omap);
		
		$carabayar=$this->getCaraBayar();
		$this->uitable->addModal("dari", "date", "Dari", "");
		$this->uitable->addModal("sampai", "date", "Sampai", "");
		$this->uitable->setFooterVisible(true);
		$this->uitable->addModal("carabayar", "select", "Cara Bayar",$carabayar);
		$this->uitable->addModal("ruangan", "select", "Ruangan",$ruangan);
        $this->uitable->addModal("nama_dokter", "chooser-lap_diagnosa-dokter_lap_diagnosa-Dokter", "Dokter","");
		$this->uitable->addModal("diagnosa", "chooser-lap_diagnosa-diagnosa_lap_diagnosa-Diagnosa", "Diagnosa","");
		
        $this->uitable->addModal("propinsi", "chooser-lap_diagnosa-propinsi_lap_diagnosa-Propinsi", "Propinsi","");
		$this->uitable->addModal("kabupaten", "chooser-lap_diagnosa-kabupaten_lap_diagnosa-Kabupaten", "Kabupaten","");
		$this->uitable->addModal("kecamatan", "chooser-lap_diagnosa-kecamatan_lap_diagnosa-Kecamatan", "Kecamatan","");
		$this->uitable->addModal("kelurahan", "chooser-lap_diagnosa-kelurahan_lap_diagnosa-Kelurahan", "Keluarahan","");
		$this->uitable->addModal("orderby", "select", "Order By",$ordered->getContent());		
        $this->uitable->addModal("grup", "multiple-select", "Grup",$group->getContent());
		
        $form=$this->uitable->getModal()->getForm();
		
		$btg=new ButtonGroup("");
		$btg->setMax(100,"");
		$btn=new Button("", "", "View");
		$btn->setAction("lap_diagnosa.view()");
		$btn->setClass("btn btn-primary");
		$btn->setIcon(" fa fa-refresh");
		$btn->setIsButton(Button::$ICONIC_TEXT);
		$btg->addButton($btn);
		
		$btn=new Button("", "", "Print");
		$btn->setAction("lap_diagnosa.print()");
		$btn->setClass("btn btn-primary");
		$btn->setIcon(" fa fa-print");
		$btn->setIsButton(Button::$ICONIC_TEXT);
		$btg->addButton($btn);
		
		$btn=new Button("", "", "Excel");
		$btn->setAction("lap_diagnosa.excel()");
		$btn->setClass("btn btn-primary");
		$btn->setIcon(" fa fa-file-excel-o");
		$btn->setIsButton(Button::$ICONIC_TEXT);
		$btg->addButton($btn);
		
		$btn=new Button("", "", "Fiksasi Carabayar");
		$btn->setAction("lap_diagnosa.fix_carabayar()");
		$btn->setClass("btn btn-primary");
		$btn->setIcon(" fa fa-money");
		$btn->setIsButton(Button::$ICONIC_TEXT);
		$btg->addButton($btn);
		
		$form->addElement("", $btg);
		
		
		echo $form->getHtml();
		echo $this->uitable->getHtml ();
		echo addJS ( "framework/bootstrap/js/bootstrap-datepicker.js" );
		echo addJS ( "framework/smis/js/table_action.js" );
		echo addCSS ( "framework/bootstrap/css/datepicker.css" );
	}
	public function jsPreLoad() {
		?>
		<script type="text/javascript">
		var lap_diagnosa;		
		var dokter_lap_diagnosa;
        var propinsi_lap_diagnosa;
        var kabupaten_lap_diagnosa;
        var kecamatan_lap_diagnosa;
        var kelurahan_lap_diagnosa;
        
		$(document).ready(function(){
			$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
			$('.mydate').datepicker();
			var column=new Array("");
			lap_diagnosa=new TableAction("lap_diagnosa","medical_record","lap_diagnosa",column);
			lap_diagnosa.addViewData=function(save_data){
				save_data['command']="list";
				save_data['dari']=$("#lap_diagnosa_dari").val();
				save_data['sampai']=$("#lap_diagnosa_sampai").val();
				save_data['carabayar']=$("#lap_diagnosa_carabayar").val();
				save_data['nama_dokter']=$("#lap_diagnosa_nama_dokter").val();
				save_data['grup']=$("#lap_diagnosa_grup").val();
				save_data['orderby']=$("#lap_diagnosa_orderby").val();
                save_data['ruangan']=$("#lap_diagnosa_ruangan").val();
                save_data['diagnosa']=$("#lap_diagnosa_diagnosa").val();
                save_data['propinsi']=$("#lap_diagnosa_propinsi").val();
                save_data['kabupaten']=$("#lap_diagnosa_kabupaten").val();
                save_data['kecamatan']=$("#lap_diagnosa_kecamatan").val();
                save_data['kelurahan']=$("#lap_diagnosa_kelurahan").val();
				return save_data;
			};	
			
			
			dokter_lap_diagnosa=new TableAction("dokter_lap_diagnosa","medical_record","lap_diagnosa",new Array());
			dokter_lap_diagnosa.setSuperCommand("dokter_lap_diagnosa");
			dokter_lap_diagnosa.selected=function(json){
				var nama=json.nama;
				$("#lap_diagnosa_nama_dokter").val(nama);
			};
            
            diagnosa_lap_diagnosa=new TableAction("diagnosa_lap_diagnosa","medical_record","lap_diagnosa",new Array());
			diagnosa_lap_diagnosa.setSuperCommand("diagnosa_lap_diagnosa");
			diagnosa_lap_diagnosa.selected=function(json){
				var nama=json.icd;
				$("#lap_diagnosa_diagnosa").val(nama);
			};
            
            propinsi_lap_diagnosa=new TableAction("propinsi_lap_diagnosa","medical_record","lap_diagnosa",new Array());
			propinsi_lap_diagnosa.setSuperCommand("propinsi_lap_diagnosa");
			propinsi_lap_diagnosa.selected=function(json){
				$("#lap_diagnosa_propinsi").val(json.nama);
			};
            
            kabupaten_lap_diagnosa=new TableAction("kabupaten_lap_diagnosa","medical_record","lap_diagnosa",new Array());
			kabupaten_lap_diagnosa.setSuperCommand("kabupaten_lap_diagnosa");
			kabupaten_lap_diagnosa.selected=function(json){
				$("#lap_diagnosa_kabupaten").val(json.nama);
                $("#lap_diagnosa_propinsi").val(json.propinsi);
			};            
            kabupaten_lap_diagnosa.addRegulerData=function(a){
              a['propinsi']=$("#lap_diagnosa_propinsi").val(); 
              return a;
            };
            
            kecamatan_lap_diagnosa=new TableAction("kecamatan_lap_diagnosa","medical_record","lap_diagnosa",new Array());
			kecamatan_lap_diagnosa.setSuperCommand("kecamatan_lap_diagnosa");
			kecamatan_lap_diagnosa.selected=function(json){
				$("#lap_diagnosa_kecamatan").val(json.nama);
                $("#lap_diagnosa_propinsi").val(json.propinsi);
                $("#lap_diagnosa_kabupaten").val(json.kabupaten);
			};            
            kecamatan_lap_diagnosa.addRegulerData=function(a){
              a['propinsi']=$("#lap_diagnosa_propinsi").val(); 
              a['kabupaten']=$("#lap_diagnosa_kabupaten").val(); 
              return a;
            };
            
            kelurahan_lap_diagnosa=new TableAction("kelurahan_lap_diagnosa","medical_record","lap_diagnosa",new Array());
			kelurahan_lap_diagnosa.setSuperCommand("kelurahan_lap_diagnosa");
			kelurahan_lap_diagnosa.selected=function(json){
                $("#lap_diagnosa_kelurahan").val(json.kabupaten);
				$("#lap_diagnosa_kecamatan").val(json.kecamatan);
                $("#lap_diagnosa_propinsi").val(json.propinsi);
                $("#lap_diagnosa_kabupaten").val(json.kabupaten);
			};            
            kelurahan_lap_diagnosa.addRegulerData=function(a){
              a['kecamatan']=$("#lap_diagnosa_kecamatan").val(); 
              a['propinsi']=$("#lap_diagnosa_propinsi").val(); 
              a['kabupaten']=$("#lap_diagnosa_kabupaten").val(); 
              return a;
            };

			lap_diagnosa.fix_carabayar=function(id){
				var a=this.getRegulerData();
				a['command']="list";
				a['fix_carabayar']="1";
				a['id']=id;
				var self=this;
				showLoading();
				$.post("",a,function(res){
					var json=getContent(res);
					$("#"+self.prefix+"_list").html(json.list);
					$("#"+self.prefix+"_pagination").html(json.pagination);	
					dismissLoading();
				});
			};
			
			
			
		});
		</script>
<?php
	}
}
global $db;

$obj=new LapDiagnosa ( $db, "medical_record", "Rekam Medis" );
$obj->initialize();
?>
