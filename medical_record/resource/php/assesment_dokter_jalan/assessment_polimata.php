<?php 

global $db;
global $user;
require_once "smis-libs-class/MasterTemplate.php";
require_once "smis-libs-class/MasterSlaveTemplate.php";
require_once "medical_record/class/table/AssesmentDokterRJTable.php";

$id_dokter  =  getSettings($db,"mr-id-dokter-".$user->getUsername(),"");
$nm_dokter  =  getSettings($db,"mr-nama-dokter-".$user->getUsername(),"");

$file_url_bola_mata         =  get_fileurl(getSettings($db,"smis-assesment-dokter-polimata-pergerakan-bola-mata",""));
$file_url_segmen_anterior   =  get_fileurl(getSettings($db,"smis-assesment-dokter-polimata-segmen-anterior-ods",""));
$file_url_funduskopi        =  get_fileurl(getSettings($db,"smis-assesment-dokter-polimata-funduskopi-ods",""));

$header     = array("No.","Tanggal","Dokter","No. Reg","D. Kerja","D. Sementara");
$uitable    = new AssesmentDokterRJTable($header);
$uitable ->setNoReg($_POST['noreg_pasien'])
         ->setDokter($id_dokter,$nm_dokter)
         ->setName("assessment_polimata")
         ->setDetailButtonEnable(true);
         
$ms = new MasterSlaveTemplate($db,"smis_mr_polimata","medical_record","assessment_polimata");
$ms ->getDBtable()
    ->setOrder(" waktu DESC ",true);

if($_POST['nrm_pasien'] != NULL || $_POST['nrm_pasien'] != '') {
    $ms ->getDBtable()
        ->addCustomKriteria(" nrm_pasien "," ='".$_POST['nrm_pasien']."' ");
}
    
$ms ->addJSColumn("id",true)
    ->addJSColumn("waktu",false)
    ->addJSColumn("keluhan_utama",false)
    ->addJSColumn("riwayat_penyakit_skrg",false)
    ->addJSColumn("riwayat_penyakit_dulu",false)
    ->addJSColumn("riwayat_penyakit_keluarga",false)
    ->addJSColumn("riwayat_pengobatan",false)
    ->addJSColumn("riwayat_alergi",false)
    
    ->addJSColumn("keadaan_umum",false)
    ->addJSColumn("kesadaran",false)
    ->addJSColumn("kesadaran_compos_mentis",false)
    ->addJSColumn("kesadaran_apatis",false)
    ->addJSColumn("kesadaran_somnolen",false)
    ->addJSColumn("kesadaran_sopor",false)
    ->addJSColumn("kesadaran_sopor_coma",false)
    ->addJSColumn("kesadaran_coma",false)
    
    ->addJSColumn("gcs",false)
    ->addJSColumn("td",false)
    ->addJSColumn("suhu",false)
    ->addJSColumn("nadi",false)
    ->addJSColumn("pernafasan",false)
    
    ->addJSColumn("persepsi_thd_penyakit",false)
    ->addJSColumn("persepsi_thd_penyakit_cobaan",false)
    ->addJSColumn("persepsi_thd_penyakit_hukuman",false)
    
    ->addJSColumn("ekspresi_thd_penyakit",false)
    ->addJSColumn("ekspresi_thd_penyakit_murung",false)
    ->addJSColumn("ekspresi_thd_penyakit_gelisah",false)
    ->addJSColumn("ekspresi_thd_penyakit_marah",false)
    
    ->addJSColumn("gangguan_konsep_diri",false)
    ->addJSColumn("reaksi_anak_interaksi",false)
    ->addJSColumn("reaksi_anak_interaksi_kooperatif",false)
    ->addJSColumn("reaksi_anak_interaksi_tdk_kooperatif",false)
    ->addJSColumn("reaksi_anak_interaksi_curiga",false)
    
    ->addJSColumn("penurunan_bb",false)
    ->addJSColumn("asupan_mkn_berkurang",false)
    ->addJSColumn("pasien_diagnosa_khusus",false)
    
    ->addJSColumn("assesmen_jatuh",false)
    ->addJSColumn("assesmen_jatuh_tdk_beresiko",false)
    ->addJSColumn("assesmen_jatuh_resiko_rendah",false)
    ->addJSColumn("assesmen_jatuh_resiko_tinggi",false)
    
    ->addJSColumn("assesmen_nyeri_waktu",false)
    ->addJSColumn("assesmen_nyeri_skala",false)
    ->addJSColumn("assesmen_nyeri_lokasi",false)
    
    ->addJSColumn("visus_od",false)
    ->addJSColumn("visus_os",false)
    ->addJSColumn("tekanan_bola_mata_od",false)
    ->addJSColumn("tekanan_bola_mata_os",false)
    ->addJSColumn("gambar_pergerakan_bola_mata",false)
    ->addJSColumn("gambar_segmen_anterior_ods",false)
    
    ->addJSColumn("palpebra_kanan",false)
    ->addJSColumn("palpebra_kiri",false)
    ->addJSColumn("konjungtiva_kanan",false)
    ->addJSColumn("konjungtiva_kiri",false)
    ->addJSColumn("kornea_kanan",false)
    ->addJSColumn("kornea_kiri",false)
    ->addJSColumn("bilik_mata_depan_kanan",false)
    ->addJSColumn("bilik_mata_depan_kiri",false)
    ->addJSColumn("iris_kanan",false)
    ->addJSColumn("iris_kiri",false)
    ->addJSColumn("pupil_kanan",false)
    ->addJSColumn("pupil_kiri",false)
    ->addJSColumn("lensa_kanan",false)
    ->addJSColumn("lensa_kiri",false)
    
    ->addJSColumn("gambar_funduskopi_ods",false)
    ->addJSColumn("laboratorium",false)
    ->addJSColumn("radiologi",false)
    
    ->addJSColumn("diagnosis_kerja",false)
    ->addJSColumn("diagnosis_banding",false)
    
    ->addJSColumn("perencanaan_diagnosis",false)
    ->addJSColumn("perencanaan_terapi",false)
    ->addJSColumn("perencanaan_monitoring",false)
    ->addJSColumn("perencanaan_edukasi",false);
    
$ms ->setUITable($uitable);
$ms ->getUItable()
    ->addModal("id_dokter","hidden","",$id_dokter)
    ->addModal("nama_dokter","hidden","",$nm_dokter);
$ms ->addNoClear("nama_dokter")
    ->addNoClear("id_dokter");
$ms ->getForm();
$ms ->getUItable()
    ->clearContent();

// For Dropdown List
$ya_tidak = array(
    array("name"=>"","value"=>"","default"=> 1),
    array("name"=>"Ya","value"=>"Ya"),
    array("name"=>"Tidak","value"=>"Tidak"),
);
$score_jatuh = array(
    array("name"=>"","value"=>"","default"=> 1),
    array("name"=>"Tidak beresiko (Total 0 - 24)","value"=>"Tidak beresiko (Total 0 - 24)"),
    array("name"=>"Resiko Rendah (Total 25 - 50)","value"=>"Resiko Rendah (Total 25 - 50)"),
    array("name"=>"Resiko Tinggi (Total >= 51)","value"=>"Resiko Tinggi (Total >= 51)"),
);
    
$ms ->getUItable()
    ->addModal("id","hidden","","" )
    ->addModal("waktu","datetime","Waktu",date("Y-m-d H:i:s") )
    
    ->addModal("", "label", "<strong>ANAMNESIS</strong>", "")
    ->addModal("keluhan_utama","textarea","Keluhan Utama","" )
    ->addModal("riwayat_penyakit_skrg","textarea","Riwayat Penyakit Sekarang","" )
    ->addModal("riwayat_penyakit_dulu","textarea","Riwayat Penyakit Dulu","" )
    ->addModal("riwayat_penyakit_keluarga","textarea","Riwayat Penyakit Keluarga","" )
    ->addModal("riwayat_pengobatan","textarea","Riwayat Pengobatan","" )
    ->addModal("riwayat_alergi","textarea","Riwayat Alergi","" )
    
    ->addModal("", "label", "<strong>PEMERIKSAAN FISIK</strong>", "")
    ->addModal("keadaan_umum","text","Keadaan Umum","" )
    ->addModal("", "label", "Kesadaran", "")
    ->addModal("kesadaran_compos_mentis","checkbox","Compos Mentis","" )
    ->addModal("kesadaran_apatis","checkbox","Apatis","" )
    ->addModal("kesadaran_somnolen","checkbox","Somnolen","" )
    ->addModal("kesadaran_sopor","checkbox","Sopor","" )
    ->addModal("kesadaran_sopor_coma","checkbox","Sopor Coma","" )
    ->addModal("kesadaran_coma","checkbox","Coma","" )
    ->addModal("gcs","text","GCS","" )
    ->addModal("", "label", "Tanda Vital:", "")
    ->addModal("td","text","TD (mmHg)","" )
    ->addModal("suhu","text","Suhu (*C)","" )
    ->addModal("nadi","text","Nadi (x/mnt)","" )
    ->addModal("pernafasan","text","Pernafasan (x/mnt)","" )
    
    ->addModal("", "label", "<strong>PSIKO SOSIAL SPIRITUAL</strong>", "")
    ->addModal("", "label", "Persepsi klien terhadap penyakitnya", "")
    ->addModal("persepsi_thd_penyakit_cobaan","checkbox","Cobaan Tuhan","" )
    ->addModal("persepsi_thd_penyakit_hukuman","checkbox","Hukuman","" )
    
    ->addModal("", "label", "Ekspresi klien terhadap penyakitnya", "")
    ->addModal("ekspresi_thd_penyakit_murung","checkbox","Murung / diam","" )
    ->addModal("ekspresi_thd_penyakit_gelisah","checkbox","Gelisah","" )
    ->addModal("ekspresi_thd_penyakit_marah","checkbox","Marah / menangis","" )
    
    ->addModal("gangguan_konsep_diri", "select", "Gangguan konsep diri", $ya_tidak)
    
    ->addModal("", "label", "Reaksi anak interaksi", "")
    ->addModal("reaksi_anak_interaksi_kooperatif","checkbox","Kooperatif","" )
    ->addModal("reaksi_anak_interaksi_tdk_kooperatif","checkbox","Tidak kooperatif","" )
    ->addModal("reaksi_anak_interaksi_curiga","checkbox","Curiga","" )
    
    ->addModal("", "label", "<strong>Skrining Gizi Awal</strong>", "")
    ->addModal("", "label", "Apakah pasien mengalami penurunan berat badan yang tidak diinginkan dalam 6 bulan terakhir ?", "")
    ->addModal("penurunan_bb", "select", "", $ya_tidak)
    ->addModal("penurunan_bb_score","text","Jika Ya, berikan score","" )
    
    ->addModal("", "label", "Apakah asupan makan berkurang karena tidak nafsu makan ?", "")
    ->addModal("asupan_mkn_berkurang", "select", "", $ya_tidak)
    ->addModal("asupan_mkn_berkurang_score","text","Jika Ya, berikan score","" )
    
    ->addModal("pasien_diagnosa_khusus","text","Pasien dengan diagnosa khusus","" )
    ->addModal("pasien_diagnosa_khusus_score","text","Score","" )
    ->addModal("jumlah_score","text","Jumlah Score","" )
    
    ->addModal("", "label", "<strong>Assesmen Jatuh</strong>", "")
    ->addModal("assesmen_jatuh", "select", "Score Jatuh", $score_jatuh)
    
    ->addModal("", "label", "<strong>Assesmen Nyeri</strong>", "")
    ->addModal("assesmen_nyeri_waktu","text","Waktu","" )
    ->addModal("assesmen_nyeri_skala","text","Skala","" )
    ->addModal("assesmen_nyeri_lokasi","text","Lokasi","" )
    
    ->addModal("", "label", "<strong>Assesmen Status Lokalis</strong>", "")
    ->addModal("", "label", "Visus", "")
    ->addModal("visus_od","text","OD","" )
    ->addModal("visus_os","text","OS","" )
    
    ->addModal("", "label", "Tekanan Bola Mata", "")
    ->addModal("tekanan_bola_mata_od","text","OD","" )
    ->addModal("tekanan_bola_mata_os","text","OS","" )
    
    ->addModal("file_gambar_pergerakan_bola_mata","hidden","",$file_url_bola_mata)
    ->addModal("gambar_pergerakan_bola_mata","draw-component-assessment_polimata_file_gambar_pergerakan_bola_mata","Gambar Pergerakan Bola Mata","")
    
    ->addModal("file_gambar_segmen_anterior_ods","hidden","",$file_url_segmen_anterior)
    ->addModal("gambar_segmen_anterior_ods","draw-component-assessment_polimata_file_gambar_segmen_anterior_ods","Gambar Segmen Anterior ODS","")
    
    ->addModal("", "label", "Palpebra", "")
    ->addModal("palpebra_kanan","text","Kanan","" )
    ->addModal("palpebra_kiri","text","Kiri","" )
    
    ->addModal("", "label", "Konjungtiva", "")
    ->addModal("konjungtiva_kanan","text","Kanan","" )
    ->addModal("konjungtiva_kiri","text","Kiri","" )
    
    ->addModal("", "label", "Kornea", "")
    ->addModal("kornea_kanan","text","Kanan","" )
    ->addModal("kornea_kiri","text","Kiri","" )
    
    ->addModal("", "label", "Bilik Mata Depan", "")
    ->addModal("bilik_mata_depan_kanan","text","Kanan","" )
    ->addModal("bilik_mata_depan_kiri","text","Kiri","" )
    
    ->addModal("", "label", "Iris", "")
    ->addModal("iris_kanan","text","Kanan","" )
    ->addModal("iris_kiri","text","Kiri","" )
    
    ->addModal("", "label", "Pupil", "")
    ->addModal("pupil_kanan","text","Kanan","" )
    ->addModal("pupil_kiri","text","Kiri","" )
    
    ->addModal("", "label", "Lensa", "")
    ->addModal("lensa_kanan","text","Kanan","" )
    ->addModal("lensa_kiri","text","Kiri","" )
    
    ->addModal("file_gambar_funduskopi_ods","hidden","",$file_url_funduskopi)
    ->addModal("gambar_funduskopi_ods","draw-component-assessment_polimata_file_gambar_funduskopi_ods","Gambar Funduskopi ODS","")
    
    ->addModal("", "label", "Pemeriksaan Penunjang", "")
    ->addModal("laboratorium","textarea","Laboratorium","" )
    ->addModal("radiologi","textarea","Radiologi","" )
    
    ->addModal("diagnosis_kerja","textarea","Diagnosis Kerja","" )
    ->addModal("diagnosis_banding","textarea","Diagnosis Banding","" )
    
    ->addModal("", "label", "Penatalaksanaan / Perencanaan Pelayanan", "")
    ->addModal("", "label", "(terapi, tindakan, konsultasi, pemeriksaan penunnjang lanjutan, edukasi, dsb)", "")
    ->addModal("perencanaan_diagnosis","textarea","Perencanaan Diagnosis","" )
    ->addModal("perencanaan_terapi","textarea","Perencanaan Terapi","" )
    ->addModal("perencanaan_monitoring","textarea","Perencanaan Monitoring","" )
    ->addModal("perencanaan_edukasi","textarea","Perencanaan Edukasi","" );
    
$ms ->getAdapter()
    ->setUseNumber(true,"No.","back.")
    ->add("Tanggal Assesmen","waktu","date d M Y H:i")
    ->add("Dokter","nama_dokter")
    ->add("No. Reg","noreg_pasien")
    ->add("NRM","nrm_pasien")
    ->add("id_dokter","id_dokter");
    
$ms ->setTableActionClass("AssesmentDokterJalanAction")
    ->setDateTimeEnable(true)
    ->setModalTitle("Assessment Poli Mata")
    ->setModalComponentSize(Modal::$MEDIUM)
    ->setModalSize(Modal::$HALF_MODEL)
    ->setAutoReload(true)
    ->initialize();  

?>