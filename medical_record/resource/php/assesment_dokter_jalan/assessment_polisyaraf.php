<?php

global $db;
global $user;
require_once "smis-libs-class/MasterTemplate.php";
require_once "smis-libs-class/MasterSlaveTemplate.php";
require_once "medical_record/class/table/AssesmentDokterRJTable.php";

$id_dokter  =  getSettings($db,"mr-id-dokter-".$user->getUsername(),"");
$nm_dokter  =  getSettings($db,"mr-nama-dokter-".$user->getUsername(),"");

$header     = array("No.","Tanggal Assesmen","Dokter","No. Reg","NRM");
$uitable    = new AssesmentDokterRJTable($header);
$uitable ->setNoReg($_POST['noreg_pasien'])
         ->setDokter($id_dokter,$nm_dokter)
         ->setName("assessment_polisyaraf")
         ->setDetailButtonEnable(true);
         
$ms = new MasterSlaveTemplate($db,"smis_mr_assesmen_syaraf","medical_record","assessment_polisyaraf");
$ms ->getDBtable()
    ->setOrder(" waktu DESC ",true);

if($_POST['nrm_pasien'] != NULL || $_POST['nrm_pasien'] != '') {
    $ms ->getDBtable()
        ->addCustomKriteria(" nrm_pasien "," ='".$_POST['nrm_pasien']."' ");
}

$ms ->addJSColumn("id",true)
    ->addJSColumn("waktu",false)
    
    ->addJSColumn("keluhan_utama",false)
    ->addJSColumn("riwayat_penyakit_skrg",false)
    ->addJSColumn("riwayat_pengobatan",false)
    
    ->addJSColumn("mata_anamnesis",false)
    ->addJSColumn("mata_ikaterus",false)
    ->addJSColumn("mata_reflek_pupil",false)
    ->addJSColumn("mata_ukuran_pupil",false)
    ->addJSColumn("mata_iso_unisocore",false)
    ->addJSColumn("mata_lain",false)
    
    ->addJSColumn("tht_tonsil",false)
    ->addJSColumn("tht_faring",false)
    ->addJSColumn("tht_lidah",false)
    ->addJSColumn("tht_bibir",false)
    
    ->addJSColumn("leher_jvp",false)
    ->addJSColumn("leher_pembesaran_kelenjar",false)
    
    ->addJSColumn("torak_simetris_asimetris",false)
    
    ->addJSColumn("cor_s1",false)
    ->addJSColumn("cor_s2",false)
    ->addJSColumn("cor_reguler",false)
    ->addJSColumn("cor_ireguler",false)
    ->addJSColumn("cor_mur",false)
    ->addJSColumn("cor_besar",false)
    
    ->addJSColumn("pulmo_suara_nafas",false)
    ->addJSColumn("pulmo_ronchi",false)
    ->addJSColumn("pulmo_wheezing",false)
    
    ->addJSColumn("abdomen_hepar",false)
    ->addJSColumn("abdomen_lien",false)
    
    ->addJSColumn("extremitas_hangat",false)
    ->addJSColumn("extremitas_dingin",false)
    ->addJSColumn("extremitas_edema",false)
    
    ->addJSColumn("kranium",false)
    ->addJSColumn("kranium_lain",false)
    ->addJSColumn("kranium_keterangan",false)
    
    ->addJSColumn("korpus_vertebra",false)
    ->addJSColumn("korpus_vertebra_lain",false)
    ->addJSColumn("korpus_vertebra_keterangan",false)
    
    ->addJSColumn("tanda_selaput_otak_kaku_kuduk",false)
    ->addJSColumn("tanda_selaput_otak_kernig_sign",false)
    ->addJSColumn("tanda_selaput_otak_brudzinski_neck_sign",false)
    ->addJSColumn("tanda_selaput_otak_brudzinski_leg_sign",false)
    ->addJSColumn("tanda_selaput_otak_lain",false)
    ->addJSColumn("tanda_selaput_otak_keterangan",false)
    
    ->addJSColumn("syaraf_otak_kanan",false)
    ->addJSColumn("syaraf_otak_kiri",false)
    
    ->addJSColumn("motorik",false)
    ->addJSColumn("motorik_kaki_kanan",false)
    ->addJSColumn("motorik_kaki_kiri",false)
    
    ->addJSColumn("refleks",false)
    ->addJSColumn("refleks_kaki_kanan",false)
    ->addJSColumn("refleks_kaki_kiri",false)
    
    ->addJSColumn("sensorik",false)
    ->addJSColumn("sensorik_kaki_kanan",false)
    ->addJSColumn("sensorik_kaki_kiri",false)
    
    ->addJSColumn("vegentatif",false)
    
    ->addJSColumn("luhur_kesadaran",false)
    ->addJSColumn("luhur_reaksi_emosi",false)
    ->addJSColumn("luhur_intelek",false)
    ->addJSColumn("luhur_proses_berpikir",false)
    ->addJSColumn("luhur_psikomotorik",false)
    ->addJSColumn("luhur_psikosensorik",false)
    ->addJSColumn("luhur_bicara_bahasa",false)
    
    ->addJSColumn("mental_refleks",false)
    ->addJSColumn("mental_memegang",false)
    ->addJSColumn("mental_menetek",false)
    ->addJSColumn("mental_snout_refleks",false)
    ->addJSColumn("mental_glabola",false)
    ->addJSColumn("mental_palmomental",false)
    ->addJSColumn("mental_korneomandibular",false)
    ->addJSColumn("mental_kaki_tolik",false)
    ->addJSColumn("mental_lain",false)
    
    ->addJSColumn("nyeri_tekan_syaraf",false)
    ->addJSColumn("tanda_laseque",false)
    ->addJSColumn("lain_lain",false)
    
    ->addJSColumn("diagnosa_kerja",false)
    ->addJSColumn("terapi_tindakan",false)
    
    ->addJSColumn("penunjang_neurovaskuler",false)
    ->addJSColumn("penunjang_neuroimaging",false)
    ->addJSColumn("penunjang_elektrodiagnostik",false)
    ->addJSColumn("penunjang_lab",false)
    ->addJSColumn("penunjang_lain",false)
    
    ->addJSColumn("waktu_pulang",false)
    ->addJSColumn("waktu_kontrol_klinik",false)
    ->addJSColumn("dirawat_diruang",false);
    
$ms ->setUITable($uitable);
$ms ->getUItable()
    ->addModal("id_dokter","hidden","",$id_dokter)
    ->addModal("nama_dokter","hidden","",$nm_dokter);
$ms ->addNoClear("nama_dokter")
    ->addNoClear("id_dokter");
$ms ->getForm();
$ms ->getUItable()
    ->clearContent();
    
// For Dropdown List
$mata_iso_unisocore = array(
    array("name"=>"","value"=>"","default"=> 1),
    array("name"=>"Isocore","value"=>"Isocore"),
    array("name"=>"Uniscore","value"=>"Uniscore"),
);
$torak = array(
    array("name"=>"","value"=>"","default"=> 1),
    array("name"=>"Simetris","value"=>"Simetris"),
    array("name"=>"Asimetris","value"=>"Asimetris"),
);
$kranium = array(
    array("name"=>"","value"=>"","default"=> 1),
    array("name"=>"Inspeksi","value"=>"Inspeksi"),
    array("name"=>"Palpasi","value"=>"Palpasi"),
    array("name"=>"Perkusi","value"=>"Perkusi"),
    array("name"=>"Auskultasi","value"=>"Auskultasi"),
    array("name"=>"Transibuminasi","value"=>"Transibuminasi"),
);
$korpus_vertebra = array(
    array("name"=>"","value"=>"","default"=> 1),
    array("name"=>"Inspeksi","value"=>"Inspeksi"),
    array("name"=>"Palpasi","value"=>"Palpasi"),
    array("name"=>"Perkusi","value"=>"Perkusi"),
    array("name"=>"Mobilitas","value"=>"Mobilitas"),
);
$motorik = array(
    array("name"=>"","value"=>"","default"=> 1),
    array("name"=>"Tenaga","value"=>"Tenaga"),
    array("name"=>"Tonus","value"=>"Tonus"),
    array("name"=>"Koordinasi","value"=>"Koordinasi"),
    array("name"=>"Gerakan Involunter","value"=>"Gerakan Involunter"),
    array("name"=>"Langkah dan Gaya Jalan","value"=>"Langkah dan Gaya Jalan"),
);
$refleks = array(
    array("name"=>"","value"=>"","default"=> 1),
    array("name"=>"Fisiologik","value"=>"Fisiologik"),
    array("name"=>"Patologik","value"=>"Patologik"),
);
$sensorik = array(
    array("name"=>"","value"=>"","default"=> 1),
    array("name"=>"Permukaan","value"=>"Permukaan"),
    array("name"=>"Dalam","value"=>"Dalam"),
);
    
$ms ->getUItable()
    ->addModal("id","hidden","","" )
    ->addModal("waktu","datetime","Waktu",date("Y-m-d H:i:s") )
    
    ->addModal("", "label", "<strong>ANAMNESES</strong>", "")
    ->addModal("keluhan_utama","textarea","Keluhan Utama","")
    ->addModal("riwayat_penyakit_skrg","textarea","Riwayat Penyakit Sekarang","")
    ->addModal("riwayat_pengobatan","textarea","Riwayat Pengobatan","")
    
    ->addModal("", "label", "<strong>PEMERIKSAAN FISIK</strong>", "")
    ->addModal("", "label", "<strong>Pemeriksaan Umum</strong>", "")
    ->addModal("", "label", "Mata", "")
    ->addModal("mata_anamnesis","text","Anemis","")
    ->addModal("mata_ikaterus","text","Ikterus","")
    ->addModal("mata_reflek_pupil","text","Reflek Pupil","")
    ->addModal("mata_ukuran_pupil","text","Ukuran Reflek Pupil (mm)","")
    ->addModal("mata_iso_unisocore", "select", "Isocore", $mata_iso_unisocore)
    ->addModal("mata_lain", "text", "Lain-lain", "")
    
    ->addModal("", "label", "THT", "")
    ->addModal("tht_tonsil","text","Tonsil","")
    ->addModal("tht_faring","text","Faring","")
    ->addModal("tht_lidah","text","Lidah","")
    ->addModal("tht_bibir","text","Bibir","")
    
    ->addModal("", "label", "Leher", "")
    ->addModal("leher_jvp","text","JVP","")
    ->addModal("leher_pembesaran_kelenjar","text","Pembesaran Kelenjar","")
    
    ->addModal("", "label", "Torak", "")
    ->addModal("torak_simetris_asimetris", "select", "Torak", $torak)
    
    ->addModal("", "label", "Cor", "")
    ->addModal("cor_s1","text","S1","")
    ->addModal("cor_s2","text","S2","")
    ->addModal("cor_reguler","text","Reguler","")
    ->addModal("cor_ireguler","text","Ireguler","")
    ->addModal("cor_mur","text","Mur-mur","")
    ->addModal("cor_besar","text","Besar","")
    
    ->addModal("", "label", "Pulmo", "")
    ->addModal("pulmo_suara_nafas","text","Suara Nafas","")
    ->addModal("pulmo_ronchi","text","Ronchi","")
    ->addModal("pulmo_wheezing","text","Wheezing","")
    
    ->addModal("", "label", "Abdomen", "")
    ->addModal("abdomen_hepar","text","Hepar","")
    ->addModal("abdomen_lien","text","Lien","")
    
    ->addModal("", "label", "Extremitas", "")
    ->addModal("extremitas_hangat","text","Hangat","")
    ->addModal("extremitas_dingin","text","Dingin","")
    ->addModal("extremitas_edema","text","Edema","")
    
    ->addModal("", "label", "<strong>Pemeriksaan Neurologik</strong>", "")
    ->addModal("", "label", "Kranium", "")
    ->addModal("kranium", "select", "Kranium", $kranium)
    ->addModal("kranium_lain","text","Lain-lain","")
    ->addModal("kranium_keterangan","text","Keterangan","")
    
    ->addModal("", "label", "Korpus Vertebra", "")
    ->addModal("korpus_vertebra", "select", "Kranium", $korpus_vertebra)
    ->addModal("korpus_vertebra_lain","text","Lain-lain","")
    ->addModal("korpus_vertebra_keterangan","text","Keterangan","")
    
    ->addModal("", "label", "Tanda-tanda perangsangan selaput otak", "")
    ->addModal("tanda_selaput_otak_kaku_kuduk","text","Kaku Kuduk","")
    ->addModal("tanda_selaput_otak_kernig_sign","text","Kernig's Sign","")
    ->addModal("tanda_selaput_otak_brudzinski_neck_sign","text","Brudzinski's neck sign","")
    ->addModal("tanda_selaput_otak_brudzinski_leg_sign","text","Brudzinski's leg sign","")
    ->addModal("tanda_selaput_otak_lain","text","Lain-lain","")
    
    ->addModal("", "label", "Syaraf Otak", "")
    ->addModal("syaraf_otak_kanan","text","Kanan","")
    ->addModal("syaraf_otak_kiri","text","Kiri","")
    
    ->addModal("", "label", "Motorik", "")
    ->addModal("motorik_kaki_kanan", "select", "Kanan", $motorik)
    ->addModal("motorik_kaki_kiri", "select", "Kiri", $motorik)
    
    ->addModal("", "label", "Refleks", "")
    ->addModal("motorik_kaki_kanan", "select", "Kanan", $refleks)
    ->addModal("motorik_kaki_kiri", "select", "Kiri", $refleks)
    
    ->addModal("", "label", "Sensorik", "")
    ->addModal("sensorik_kaki_kanan", "select", "Kanan", $sensorik)
    ->addModal("sensorik_kaki_kiri", "select", "Kiri", $sensorik)
    
    ->addModal("vegentatif","text","Vegetatif","")
    
    ->addModal("", "label", "Fungsi Luhur", "")
    ->addModal("luhur_kesadaran","text","Kesadaran","")
    ->addModal("luhur_reaksi_emosi","text","Reaksi emosi","")
    ->addModal("luhur_intelek","text","Fungsi intelek","")
    ->addModal("luhur_proses_berpikir","text","Proses berpikir","")
    ->addModal("luhur_psikomotorik","text","Fungsi psikomotorik","")
    ->addModal("luhur_psikosensorik","text","Fungsi psikosensorik","")
    ->addModal("luhur_bicara_bahasa","text","Fungsi bicara dan bahasa","")
    
    ->addModal("", "label", "Tanda-tanda kemunduran mental", "")
    ->addModal("mental_memegang","text","Refleks memegang","")
    ->addModal("mental_menetek","text","Refleks menetek","")
    ->addModal("mental_snout_refleks","text","Snout refleks","")
    ->addModal("mental_glabola","text","Refleks glabola","")
    ->addModal("mental_palmomental","text","Refleks palmomental","")
    ->addModal("mental_korneomandibular","text","Refleks korneomandibular","")
    ->addModal("mental_kaki_tolik","text","Refleks kaki tolik","")
    ->addModal("mental_lain","text","Lain-lain","")
    
    ->addModal("nyeri_tekan_syaraf","text","Nyeri tekan syaraf","")
    ->addModal("tanda_laseque","text","Tanda laseque","")
    ->addModal("lain_lain","text","Lain-lain","")
    
    ->addModal("diagnosa_kerja","textarea","<strong>DIAGNOSA KERJA / DIAGNOSA BANDING</strong>","")
    ->addModal("terapi_tindakan","textarea","<strong>TERAPI / TINDAKAN</strong>","")
    
    ->addModal("", "label", "<strong>PEMERIKSAAN PENUNJANG</strong>", "")
    ->addModal("penunjang_neurovaskuler","text","Neurovaskuler","")
    ->addModal("penunjang_neuroimaging","text","Neuroimaging","")
    ->addModal("penunjang_elektrodiagnostik","text","Elektrodiagnostik","")
    ->addModal("penunjang_lab","text","Laboratorium","")
    ->addModal("penunjang_lain","text","Lain-lain","")
    
    ->addModal("", "label", "<strong>DISPOSISI</strong>", "")
    ->addModal("waktu_pulang","datetime","Tanggal Pulang","")
    ->addModal("waktu_kontrol_klinik","datetime","Tanggal Kontrol","")
    ->addModal("dirawat","text","Di Rawat","");
    
$ms ->getAdapter()
    ->setUseNumber(true,"No.","back.")
    ->add("Tanggal Assesmen","waktu","date d M Y H:i")
    ->add("Dokter","nama_dokter")
    ->add("No. Reg","noreg_pasien")
    ->add("NRM","nrm_pasien")
    ->add("id_dokter","id_dokter");
    
$ms ->setTableActionClass("AssesmentDokterJalanAction")
    ->setDateTimeEnable(true)
    ->setModalTitle("Assessment Poli Syaraf")
    ->setModalComponentSize(Modal::$MEDIUM)
    ->setModalSize(Modal::$HALF_MODEL)
    ->setAutoReload(true)
    ->initialize();

?>