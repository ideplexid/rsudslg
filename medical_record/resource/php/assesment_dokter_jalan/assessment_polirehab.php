<?php

global $db;
global $user;
require_once "smis-libs-class/MasterTemplate.php";
require_once "smis-libs-class/MasterSlaveTemplate.php";
require_once "medical_record/class/table/AssesmentDokterRJTable.php";

$id_dokter      =  getSettings($db,"mr-id-dokter-".$user->getUsername(),"");
$nm_dokter      =  getSettings($db,"mr-nama-dokter-".$user->getUsername(),"");

$header     = array("No.","Tanggal Assesmen","Dokter","No. Reg","NRM");
$uitable    = new AssesmentDokterRJTable($header);
$uitable ->setNoReg($_POST['noreg_pasien'])
         ->setDokter($id_dokter,$nm_dokter)
         ->setName("assessment_polirehab")
         ->setDetailButtonEnable(true);
         
$ms = new MasterSlaveTemplate($db,"smis_mr_polirehab","medical_record","assessment_polirehab");
$ms ->getDBtable()
    ->setOrder(" waktu DESC ",true)
    ->addCustomKriteria(" nrm_pasien "," ='".$_POST['nrm_pasien']."' ");
    
$ms ->addJSColumn("id",true)
    ->addJSColumn("waktu",false)
    
    ->addJSColumn("keluhan_utama",false)
    ->addJSColumn("riwayat_penyakit",false)
    ->addJSColumn("riwayat_pengobatan",false)
    ->addJSColumn("pemeriksaan_fisik",false)
    ->addJSColumn("diagnosis_rehab_medis",false)
    ->addJSColumn("program_rehab_medis",false)
    ->addJSColumn("terapi",false)
    
    ->addJSColumn("tanggal_pulang",false)
    ->addJSColumn("tanggal_kontrol",false)
    ->addJSColumn("dirawat",false);
    
$ms ->setUITable($uitable);
$ms ->getUItable()
    ->addModal("id_dokter","hidden","",$id_dokter)
    ->addModal("nama_dokter","hidden","",$nm_dokter);
$ms ->addNoClear("nama_dokter")
    ->addNoClear("id_dokter");
$ms ->getForm();
$ms ->getUItable()
    ->clearContent();
    
$ms ->getUItable()
    ->addModal("id","hidden","","" )
    ->addModal("waktu","datetime","Waktu",date("Y-m-d H:i:s") )
    
    ->addModal("", "label", "<strong>ANAMNESES</strong>", "")
    ->addModal("keluhan_utama","textarea","Keluhan Utama","")
    ->addModal("riwayat_penyakit","textarea","Riwayat Penyakit","")
    ->addModal("riwayat_pengobatan","textarea","Riwayat Pengobatan","")
    
    ->addModal("pemeriksaan_fisik","textarea","PEMERIKSAAN FISIK","")
    ->addModal("diagnosis_rehab_medis","textarea","DIAGNOSIS REHABILITASI MEDIS","")
    ->addModal("program_rehab_medis","textarea","PROGRAM REHABILITASI MEDIS","")
    ->addModal("terapi","textarea","TERAPI / TINDAKAN","")
    
    ->addModal("", "label", "<strong>DISPOSISI</strong>", "")
    ->addModal("tanggal_pulang","datetime","Tanggal Pulang","")
    ->addModal("tanggal_kontrol","datetime","Tanggal Kontrol","")
    ->addModal("dirawat","text","Di Rawat","");
    
$ms ->getAdapter()
    ->setUseNumber(true,"No.","back.")
    ->add("Tanggal Assesmen","waktu","date d M Y H:i")
    ->add("Dokter","nama_dokter")
    ->add("No. Reg","noreg_pasien")
    ->add("NRM","nrm_pasien")
    ->add("id_dokter","id_dokter");
    
$ms ->setTableActionClass("AssesmentDokterJalanAction")
    ->setDateTimeEnable(true)
    ->setModalTitle("Assessment Poli Rehabilitas Medis / Fisiotherapi")
    ->setModalComponentSize(Modal::$MEDIUM)
    ->setModalSize(Modal::$HALF_MODEL)
    ->setAutoReload(true)
    ->initialize();  

?>