<?php

global $db;
global $user;
require_once "smis-libs-class/MasterTemplate.php";
require_once "smis-libs-class/MasterSlaveTemplate.php";
require_once "medical_record/class/table/AssesmentDokterRJTable.php";

$id_dokter  =  getSettings($db,"mr-id-dokter-".$user->getUsername(),"");
$nm_dokter  =  getSettings($db,"mr-nama-dokter-".$user->getUsername(),"");

$header     = array("No.","Tanggal Assesmen","Dokter","No. Reg","NRM");
$uitable    = new AssesmentDokterRJTable($header);
$uitable ->setNoReg($_POST['noreg_pasien'])
         ->setDokter($id_dokter,$nm_dokter)
         ->setName("assessment_polipsikologi")
         ->setDetailButtonEnable(true);
         
$ms = new MasterSlaveTemplate($db,"smis_mr_assesmen_psikologi","medical_record","assessment_polipsikologi");
$ms ->getDBtable()
    ->setOrder(" waktu DESC ",true);

if($_POST['nrm_pasien'] != NULL || $_POST['nrm_pasien'] != '') {
    $ms ->getDBtable()
        ->addCustomKriteria(" nrm_pasien "," ='".$_POST['nrm_pasien']."' ");
}

$ms ->addJSColumn("id",true)
    ->addJSColumn("waktu",false)
    
    ->addJSColumn("obeservasi",false)
    ->addJSColumn("auto_anamenese",false)
    ->addJSColumn("hetero_anamenese",false)
    ->addJSColumn("keluhan_utama",false)
    ->addJSColumn("mulai_terjadi",false)
    ->addJSColumn("riwayat_penyakit",false)
    ->addJSColumn("keluhan_psikosomatis",false)
    
    ->addJSColumn("penyebab",false)
    ->addJSColumn("faktor_keluarga",false)
    ->addJSColumn("faktor_pekerjaan",false)
    ->addJSColumn("faktor_premorboid",false)
    
    ->addJSColumn("riwayat_napza",false)
    ->addJSColumn("napza_lama_pemakaian",false)
    ->addJSColumn("napza_jenis_zat",false)
    ->addJSColumn("napza_cara_pakai",false)
    ->addJSColumn("napza_latarbelakang_pakai",false)
    
    ->addJSColumn("kesan_umum",false)
    ->addJSColumn("kesadaran",false)
    ->addJSColumn("mood",false)
    ->addJSColumn("proses_dan_isi_pikiran",false)
    ->addJSColumn("kecerdasan",false)
    ->addJSColumn("dorongan",false)
    ->addJSColumn("judgment",false)
    ->addJSColumn("attention",false)
    ->addJSColumn("psikomotorik",false)
    
    ->addJSColumn("orientasi_diri",false)
    ->addJSColumn("orientasi_wkt",false)
    ->addJSColumn("orientasi_tempat",false)
    ->addJSColumn("orientasi_ruang",false)
    
    ->addJSColumn("bb",false)
    ->addJSColumn("tb",false)
    ->addJSColumn("lila",false)
    ->addJSColumn("lk",false)
    ->addJSColumn("status_gizi",false)
    
    ->addJSColumn("tes_iq",false)
    ->addJSColumn("tes_bakat",false)
    ->addJSColumn("tes_kepribadian",false)
    ->addJSColumn("tes_okupasi",false)
    ->addJSColumn("tes_perkembangan",false)
    
    ->addJSColumn("perkembangan_fisik",false)
    ->addJSColumn("perkembangan_kognitif",false)
    ->addJSColumn("perkembangan_emosi",false)
    ->addJSColumn("perkembangan_sosial",false)
    ->addJSColumn("perkembangan_motorik",false)
    
    ->addJSColumn("raport",false)
    ->addJSColumn("ekspresi",false)
    ->addJSColumn("reaksi",false)
    ->addJSColumn("komunikasi",false)
    ->addJSColumn("lain_lain",false)
    
    ->addJSColumn("waktu_pulang",false)
    ->addJSColumn("waktu_kontrol_klinik",false)
    ->addJSColumn("dirawat_diruang",false);
    
$ms ->setUITable($uitable);
$ms ->getUItable()
    ->addModal("id_dokter","hidden","",$id_dokter)
    ->addModal("nama_dokter","hidden","",$nm_dokter);
$ms ->addNoClear("nama_dokter")
    ->addNoClear("id_dokter");
$ms ->getForm();
$ms ->getUItable()
    ->clearContent();
    
// For Dropdown List
$status_gizi = array(
    array("name"=>"","value"=>"","default"=> 1),
    array("name"=>"Baik","value"=>"Baik"),
    array("name"=>"Sedang","value"=>"Sedang"),
    array("name"=>"Kurang","value"=>"Kurang"),
    array("name"=>"Jelek/Gizi Buruk","value"=>"Jelek/Gizi Buruk"),
);
    
$ms ->getUItable()
    ->addModal("id","hidden","","" )
    ->addModal("waktu","datetime","Waktu",date("Y-m-d H:i:s") )
    
    ->addModal("", "label", "<strong>DATA PSIKOLOGI</strong>", "")
    ->addModal("obeservasi","textarea","<strong>1. OBSERVASI</strong>","")
    
    ->addModal("", "label", "<strong>2. WAWANCARA</strong>", "")
    ->addModal("auto_anamenese","textarea","Auto Anamnese","")
    ->addModal("hetero_anamenese","textarea","Allo / Hetero Anamnese","")
    ->addModal("keluhan_utama","textarea","Keluhan Utama","")
    ->addModal("mulai_terjadi","textarea","Kapan Mulai Terjadi","")
    ->addModal("riwayat_penyakit","textarea","Riwayat Penyakit","")
    ->addModal("keluhan_psikosomatis","textarea","Keluhan Psikosomatis","")
    
    ->addModal("", "label", "<strong>3. PREDISPOSISI FAKTOR</strong>", "")
    ->addModal("penyebab","textarea","Penyebab / Pencetus","")
    ->addModal("faktor_keluarga","textarea","Faktor Keluarga","")
    ->addModal("faktor_pekerjaan","textarea","Faktor Pekerjaan / Sosial","")
    ->addModal("faktor_premorboid","textarea","Faktor Premorbid","")
    ->addModal("riwayat_napza","textarea","Riwayat NAPZA","")
    ->addModal("napza_jenis_zat","text","Jenis Zat","")
    ->addModal("napza_lama_pemakaian","text","Lama Pemakaian","")
    ->addModal("napza_latarbelakang_pakai","text","Latar Belakang Pemakaian","")
    
    ->addModal("", "label", "<strong>STATUS PSIKOLOGI</strong>", "")
    ->addModal("kesan_umum","text","Kesan Umum","")
    ->addModal("kesadaran","text","Kesadaran","")
    ->addModal("mood","text","Mood/Afek/Ekspresi","")
    ->addModal("proses_dan_isi_pikiran","text","Proses & Isi Pikiran","")
    ->addModal("kecerdasan","text","Kecerdasan","")
    ->addModal("dorongan","text","Dorongan (Insting, Drive, Motivasi, Semangat)","")
    ->addModal("judgment","text","Judgement (Pemahaman)","")
    ->addModal("attention","text","Attention (Perhatian)","")
    ->addModal("psikomotorik","text","Psikomotorik","")
    
    ->addModal("", "label", "Orientasi:", "")
    ->addModal("orientasi_diri","text","Diri","")
    ->addModal("orientasi_wkt","text","Waktu","")
    ->addModal("orientasi_tempat","text","Tempat","")
    ->addModal("orientasi_ruang","text","Ruang","")
    
    ->addModal("", "label", "<strong>STATUS ANTROPOMETRIS</strong>", "")
    ->addModal("bb","text","Berat Badan (Kg)","")
    ->addModal("tb","text","Tinggi Badan (cm)","")
    ->addModal("lila","text","LILA (cm)","")
    ->addModal("lk","text","LK(cm)","")
    ->addModal("status_gizi", "select", "Status Gizi", $status_gizi)
    
    ->addModal("", "label", "<strong>PEMERIKSAAN PSIKOLOGI</strong>", "")
    ->addModal("tes_iq","text","Tes IQ","")
    ->addModal("tes_bakat","text","Tes Bakat/Minat","")
    ->addModal("tes_kepribadian","text","Tes Kepribadian","")
    ->addModal("tes_okupasi","text","Tes Okupasi","")
    ->addModal("tes_perkembangan","text","Tes Perkembangan","")
    
    ->addModal("diagnosa_kerja","textarea","<strong>PSIKO DIAGNOSA KERJA / PSIKO DIAGNOSA BANDING</strong>","")
    ->addModal("terapi","textarea","<strong>TERAPI / TINDAKAN PSIKOLOGI</strong>","")
    ->addModal("tindakan","textarea","<strong>RENCANA TINDAKAN</strong>","")
    
    ->addModal("", "label", "<strong>Observasi/Wawancara</strong>", "")
    ->addModal("raport","text","Report","")
    ->addModal("ekspresi","text","Ekspresi","")
    ->addModal("reaksi","text","Reaksi","")
    ->addModal("komunikasi","text","Komunikasi","")
    ->addModal("lain_lain","text","Lain-lain","")
    
    ->addModal("", "label", "<strong>Observasi/Wawancara</strong>", "")
    ->addModal("perkembangan_fisik","text","Perkembangan Fisik","")
    ->addModal("perkembangan_kognitif","text","Perkembangan Kognitif","")
    ->addModal("perkembangan_emosi","text","Perkembangan Emosi","")
    ->addModal("perkembangan_sosial","text","Perkembangan Sosial","")
    ->addModal("perkembangan_motorik","text","Perkembangan Motorik","")
    
    ->addModal("", "label", "<strong>DISPOSISI</strong>", "")
    ->addModal("waktu_pulang","datetime","Tanggal Pulang","")
    ->addModal("waktu_kontrol_klinik","datetime","Tanggal Kontrol","")
    ->addModal("dirawat","text","Di Rawat","");
    
$ms ->getAdapter()
    ->setUseNumber(true,"No.","back.")
    ->add("Tanggal Assesmen","waktu","date d M Y H:i")
    ->add("Dokter","nama_dokter")
    ->add("No. Reg","noreg_pasien")
    ->add("NRM","nrm_pasien")
    ->add("id_dokter","id_dokter");
    
$ms ->setTableActionClass("AssesmentDokterJalanAction")
    ->setDateTimeEnable(true)
    ->setModalTitle("Assessment Poli Psikologi")
    ->setModalComponentSize(Modal::$MEDIUM)
    ->setModalSize(Modal::$HALF_MODEL)
    ->setAutoReload(true)
    ->initialize();

?>