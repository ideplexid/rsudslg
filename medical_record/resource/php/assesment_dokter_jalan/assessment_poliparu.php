<?php

global $db;
global $user;
require_once "smis-libs-class/MasterTemplate.php";
require_once "smis-libs-class/MasterSlaveTemplate.php";
require_once "medical_record/class/table/AssesmentDokterRJTable.php";

$id_dokter      =  getSettings($db,"mr-id-dokter-".$user->getUsername(),"");
$nm_dokter      =  getSettings($db,"mr-nama-dokter-".$user->getUsername(),"");
$file_url_paru  =  get_fileurl(getSettings($db,"smis-assesment-dokter-poliparu-paru",""));
$file_url_perut =  get_fileurl(getSettings($db,"smis-assesment-dokter-poliparu-perut",""));

$header     = array("No.","Tanggal Assesmen","Dokter","No. Reg","NRM");
$uitable    = new AssesmentDokterRJTable($header);
$uitable ->setNoReg($_POST['noreg_pasien'])
         ->setDokter($id_dokter,$nm_dokter)
         ->setName("assessment_poliparu")
         ->setDetailButtonEnable(true);

$ms = new MasterSlaveTemplate($db,"smis_mr_poliparu","medical_record","assessment_poliparu");
$ms ->getDBtable()
    ->setOrder(" waktu DESC ",true)
    ->addCustomKriteria(" nrm_pasien "," ='".$_POST['nrm_pasien']."' ");
    
$ms ->addJSColumn("id",true)
    ->addJSColumn("waktu",false)
    ->addJSColumn("keluhan_utama",false)
    ->addJSColumn("riwayat_penyakit",false)
    ->addJSColumn("riwayat_pengobatan",false)
    
    ->addJSColumn("konjungtiva_anemia",false)
    ->addJSColumn("ikterus",false)
    ->addJSColumn("reflex_pupil",false)
    ->addJSColumn("edema_palpebrae",false)
    
    ->addJSColumn("bibir",false)
    ->addJSColumn("lidah",false)
    ->addJSColumn("tonsil",false)
    ->addJSColumn("faring",false)
    
    ->addJSColumn("jvp",false)
    ->addJSColumn("pembesaran_kelenjar",false)
    ->addJSColumn("kaku_kuduk",false)
    
    ->addJSColumn("torak",false)
    ->addJSColumn("s1",false)
    ->addJSColumn("s2",false)
    ->addJSColumn("reguler",false)
    ->addJSColumn("ireguler",false)
    ->addJSColumn("murmur",false)
    ->addJSColumn("gallop",false)
    ->addJSColumn("jantung_lain",false)
    
    ->addJSColumn("vesikuler",false)
    ->addJSColumn("ronki",false)
    ->addJSColumn("wheezing",false)
    ->addJSColumn("perkusi",false)
    ->addJSColumn("paru_lain",false)
    ->addJSColumn("file_gambar_paru",false)
    ->addJSColumn("gambar_paru",false)
    
    ->addJSColumn("distensi",false)
    ->addJSColumn("meteorisme",false)
    ->addJSColumn("peristaltik",false)
    ->addJSColumn("ascites",false)
    ->addJSColumn("nyeri_tekan",false)
    ->addJSColumn("abdomen_lokasi",false)
    ->addJSColumn("nyeri_tekan",false)
    ->addJSColumn("abdomen_lokasi",false)
    
    ->addJSColumn("hepar",false)
    ->addJSColumn("ren",false)
    ->addJSColumn("undulasi",false)
    ->addJSColumn("lien",false)
    ->addJSColumn("abdomen_lain",false)
    
    ->addJSColumn("extremitas",false)
    ->addJSColumn("edema",false)
    ->addJSColumn("extremitas_lain",false)
    
    ->addJSColumn("laboratorium",false)
    ->addJSColumn("ekg",false)
    ->addJSColumn("xray",false)
    
    ->addJSColumn("diagnosa_kerja",false)
    ->addJSColumn("terapi",false)
    ->addJSColumn("rencana_kerja",false)
    
    ->addJSColumn("tanggal_pulang",false)
    ->addJSColumn("tanggal_kontrol",false)
    ->addJSColumn("dirawat",false);
    
$ms ->setUITable($uitable);
$ms ->getUItable()
    ->addModal("id_dokter","hidden","",$id_dokter)
    ->addModal("nama_dokter","hidden","",$nm_dokter);
$ms ->addNoClear("nama_dokter")
    ->addNoClear("id_dokter");
$ms ->getForm();
$ms ->getUItable()
    ->clearContent();

// For Dropdown List
$positif_negatif = array(
    array("name"=>"","value"=>"","default"=> 1),
    array("name"=>"Positif (+)","value"=>"Positif"),
    array("name"=>"Negatif (-)","value"=>"Negatif"),
);
$torak = array(
    array("name"=>"","value"=>"","default"=> 1),
    array("name"=>"Simetris","value"=>"simetris"),
    array("name"=>"Asimetris","value"=>"asimetris"),
);
$perkusi = array(
    array("name"=>"","value"=>"","default"=> 1),
    array("name"=>"Sonor","value"=>"sonor"),
    array("name"=>"Redup","value"=>"redup"),
    array("name"=>"Hipersonor","value"=>"hipersonor"),
);
$abdomen = array(
    array("name"=>"","value"=>"","default"=> 1),
    array("name"=>"Normal","value"=>"normal"),
    array("name"=>"Meningkat","value"=>"meningkat"),
    array("name"=>"Menurun","value"=>"menurun"),
);
$ren = array(
    array("name"=>"","value"=>"","default"=> 1),
    array("name"=>"Tidak teraba","value"=>"Tidak teraba"),
    array("name"=>"Teraba","value"=>"Teraba"),
);
$extremitas = array(
    array("name"=>"","value"=>"","default"=> 1),
    array("name"=>"Hangat","value"=>"Hangat"),
    array("name"=>"Dingin","value"=>"Dingin"),
);
    
$ms ->getUItable()
    ->addModal("id","hidden","","" )
    ->addModal("waktu","datetime","Waktu",date("Y-m-d H:i:s") )
    
    ->addModal("", "label", "<strong>ANAMNESES</strong>", "")
    ->addModal("keluhan_utama","textarea","Keluhan Utama","")
    ->addModal("riwayat_penyakit","textarea","Riwayat Penyakit","")
    ->addModal("riwayat_pengobatan","textarea","Riwayat Pengobatan","")
    
    ->addModal("", "label", "<strong>PEMERIKSAAN FISIK</strong>", "")
    ->addModal("", "label", "<strong>Mata</strong>", "")
    ->addModal("konjungtiva_anemia","text","Konjungtiva Anemia","")
    ->addModal("ikterus","text","Ikterus","")
    ->addModal("reflex_pupil","text","Reflex Pupil","")
    ->addModal("edema_palpebrae","text","Edema Palpebrae","")
    
    ->addModal("", "label", "<strong>THT</strong>", "")
    ->addModal("bibir","text","Bibir","")
    ->addModal("lidah","text","Lidah","")
    ->addModal("tonsil","text","Tonsil","")
    ->addModal("faring","text","Faring","")
    
    ->addModal("", "label", "<strong>Leher</strong>", "")
    ->addModal("jvp","text","JVP","")
    ->addModal("pembesaran_kelenjar","text","Pembesaran Kelenjar","")
    ->addModal("kaku_kuduk", "select", "Kaku Kuduk", $positif_negatif)
    
    ->addModal("", "label", "<strong>Torak</strong>", "")
    ->addModal("torak","select","Torak",$torak)
    
    ->addModal("", "label", "<strong>Jantung</strong>", "")
    ->addModal("s1","text","S1","")
    ->addModal("s2","text","S2","")
    ->addModal("reguler","text","Reguler","")
    ->addModal("ireguler","text","Ireguler","")
    ->addModal("murmur","text","Murmur","")
    ->addModal("gallop","select","Gallop (S3/S4)",$positif_negatif)
    ->addModal("jantung_lain","text","Lain-lain","")
    
    ->addModal("", "label", "<strong>Paru</strong>", "")
    ->addModal("vesikuler","text","Vesikuler","")
    ->addModal("ronki","text","Ronki","")
    ->addModal("perkusi","select","Perkusi",$perkusi)
    ->addModal("wheezing","text","Wheezing","")
    ->addModal("paru_lain","text","Lain-lain","")
    
    ->addModal("", "label", "<strong>Abdomen</strong>", "")
    ->addModal("distensi", "select", "Distensi", $positif_negatif)
    ->addModal("meteorisme", "select", "Meteorisme", $positif_negatif)
    ->addModal("peristaltik", "text", "Peristaltik", "")
    ->addModal("abdomen", "select", "Abdomen", $abdomen)
    ->addModal("ascites", "select", "Ascites", $positif_negatif)
    ->addModal("nyeri_tekan", "select", "Nyeri Tekan", $positif_negatif)
    ->addModal("abdomen_lokasi", "text", "Lokasi", "")
    
    ->addModal("", "label", "<strong>Hepar</strong>", "")
    ->addModal("hepar", "text", "Hepar", "")
    ->addModal("ren", "select", "Ren", $ren)
    ->addModal("undulasi", "select", "Undulasi", $positif_negatif)
    ->addModal("lien", "text", "Lien", "")
    ->addModal("abdomen_lain", "text", "Lain-lain", "")
    ->addModal("file_gambar_perut","hidden","",$file_url_perut)
    ->addModal("gambar_paru","draw-component-assessment_poliparu_file_gambar_perut","Gambar Perut","")
    
    ->addModal("", "label", "<strong>Extremitas</strong>", "")
    ->addModal("extremitas", "select", "Extremitas", $extremitas)
    ->addModal("edema", "text", "Edema", "")
    ->addModal("extremitas_lain", "text", "Lain-lain", "")
    
    ->addModal("", "label", "<strong>HASIL PEMERIKSAAN PENUNJANG</strong>", "")
    ->addModal("laboratorium", "textarea", "Laboratorium", "")
    ->addModal("ekg", "textarea", "EKG", "")
    ->addModal("xray", "textarea", "X-Ray", "")
    ->addModal("file_gambar_paru","hidden","",$file_url_paru)
    ->addModal("gambar_paru","draw-component-assessment_poliparu_file_gambar_paru","Gambar Paru","")
    
    ->addModal("diagnosa_kerja", "textarea", "DIAGNOSA KERJA", "")
    ->addModal("terapi", "textarea", "TERAPI/TINDAKAN", "")
    ->addModal("rencana_kerja", "textarea", "RENCANA KERJA", "")
    
    ->addModal("", "label", "<strong>DISPOSISI</strong>", "")
    ->addModal("tanggal_pulang","datetime","Tanggal Pulang","")
    ->addModal("tanggal_kontrol","datetime","Tanggal Kontrol","")
    ->addModal("dirawat","text","Di Rawat","");
    
$ms ->getAdapter()
    ->setUseNumber(true,"No.","back.")
    ->add("Tanggal Assesmen","waktu","date d M Y H:i")
    ->add("Dokter","nama_dokter")
    ->add("No. Reg","noreg_pasien")
    ->add("NRM","nrm_pasien")
    ->add("id_dokter","id_dokter");
    
$ms ->setTableActionClass("AssesmentDokterJalanAction")
    ->setDateTimeEnable(true)
    ->setModalTitle("Assessment Poli Paru")
    ->setModalComponentSize(Modal::$MEDIUM)
    ->setModalSize(Modal::$HALF_MODEL)
    ->setAutoReload(true)
    ->initialize();  

?>