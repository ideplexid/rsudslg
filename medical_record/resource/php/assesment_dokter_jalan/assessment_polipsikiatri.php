<?php

global $db;
global $user;
require_once "smis-libs-class/MasterTemplate.php";
require_once "smis-libs-class/MasterSlaveTemplate.php";
require_once "medical_record/class/table/AssesmentDokterRJTable.php";

$id_dokter  =  getSettings($db,"mr-id-dokter-".$user->getUsername(),"");
$nm_dokter  =  getSettings($db,"mr-nama-dokter-".$user->getUsername(),"");

$header     = array("No.","Tanggal Assesmen","Dokter","No. Reg","NRM");
$uitable    = new AssesmentDokterRJTable($header);
$uitable ->setNoReg($_POST['noreg_pasien'])
         ->setDokter($id_dokter,$nm_dokter)
         ->setName("assessment_polipsikiatri")
         ->setDetailButtonEnable(true);
         
$ms = new MasterSlaveTemplate($db,"smis_mr_assesmen_psikiatri","medical_record","assessment_polipsikiatri");
$ms ->getDBtable()
    ->setOrder(" waktu DESC ",true);

if($_POST['nrm_pasien'] != NULL || $_POST['nrm_pasien'] != '') {
    $ms ->getDBtable()
        ->addCustomKriteria(" nrm_pasien "," ='".$_POST['nrm_pasien']."' ");
}

$ms ->addJSColumn("id",true)
    ->addJSColumn("waktu",false)
    
    ->addJSColumn("wawancara",false)
    ->addJSColumn("keluhan_utama",false)
    ->addJSColumn("riwayat_penyakit_skrg",false)
    ->addJSColumn("auto_anamnesis",false)
    ->addJSColumn("hetero_namnesis",false)
    
    ->addJSColumn("faktor_penyebab",false)
    ->addJSColumn("faktor_keluarga",false)
    ->addJSColumn("fungsi_kerja",false)
    ->addJSColumn("riwayat_napza",false)
    ->addJSColumn("napza_lama_pemakaian",false)
    ->addJSColumn("napza_jenis_zat",false)
    ->addJSColumn("napza_cara_pemakaian",false)
    ->addJSColumn("napza_latarbelakang_pemakaian",false)
    ->addJSColumn("faktor_premorbid",false)
    ->addJSColumn("faktor_organik",false)
    
    ->addJSColumn("kesan_umum",false)
    ->addJSColumn("kesadaran",false)
    ->addJSColumn("mood",false)
    ->addJSColumn("proses_pikir",false)
    ->addJSColumn("pencerapan",false)
    ->addJSColumn("intelegensi",false)
    ->addJSColumn("dorongan_insting",false)
    ->addJSColumn("psikomotor",false)
    
    ->addJSColumn("penyakit_dalam",false)
    ->addJSColumn("neurologi",false)
    ->addJSColumn("diagnosis_kerja",false)
    ->addJSColumn("terapi",false)
    ->addJSColumn("rencana_kerja",false)
    
    ->addJSColumn("waktu_pulang",false)
    ->addJSColumn("waktu_kontrol_klinik",false)
    ->addJSColumn("dirawat_diruang",false);
    
$ms ->setUITable($uitable);
$ms ->getUItable()
    ->addModal("id_dokter","hidden","",$id_dokter)
    ->addModal("nama_dokter","hidden","",$nm_dokter);
$ms ->addNoClear("nama_dokter")
    ->addNoClear("id_dokter");
$ms ->getForm();
$ms ->getUItable()
    ->clearContent();
    
// For Dropdown List
$ada_tidak = array(
    array("name"=>"","value"=>"","default"=> 1),
    array("name"=>"Ada","value"=>"Ada"),
    array("name"=>"Tidak","value"=>"Tidak"),
);
    
$ms ->getUItable()
    ->addModal("id","hidden","","" )
    ->addModal("waktu","datetime","Waktu",date("Y-m-d H:i:s") )
    
    ->addModal("", "label", "<strong>ANAMNESES</strong>", "")
    ->addModal("wawancara","textarea","Wawancara","")
    ->addModal("keluhan_utama","textarea","Keluhan Utama","")
    ->addModal("riwayat_penyakit_skrg","textarea","Riwayat Penyakit Sekarang","")
    ->addModal("auto_anamnesis","textarea","Auto Anamnesis","")
    ->addModal("hetero_namnesis","textarea","Hetero Anamnesis","")
    
    ->addModal("faktor_penyebab","text","Faktor Penyebab","")
    ->addModal("faktor_keluarga","text","Faktor Keluarga","")
    ->addModal("fungsi_kerja","text","Faktor Kerja/Sosial","")
    ->addModal("riwayat_napza","select","Faktor Penyebab", $ada_tidak)
    ->addModal("", "label", "Jelaskan jika Ada", "")
    ->addModal("napza_lama_pemakaian","text","- Lama Pemakaian","")
    ->addModal("napza_jenis_zat","text","- Jenis Zat","")
    ->addModal("napza_cara_pemakaian","text","- Cara Pemakaian","")
    ->addModal("napza_latarbelakang_pemakaian","text","- Latar Belakang Pemakaian","")
    ->addModal("faktor_premorbid","text","Faktor Premorbid","")
    ->addModal("faktor_organik","text","Faktor Organik","")
    
    ->addModal("", "label", "<strong>STATUS PSIKIATRI</strong>", "")
    ->addModal("kesan_umum","text","Kesan Umum","")
    ->addModal("kesadaran","text","Kesadaran","")
    ->addModal("mood","text","Mood","")
    ->addModal("proses_pikir","text","Proses Pikir","")
    ->addModal("pencerapan","text","Pencerapan","")
    ->addModal("intelegensi","text","Intelegensi","")
    ->addModal("dorongan_insting","text","Dorongan Insting","")
    ->addModal("psikomotor","text","Psikomotor","")
    
    ->addModal("penyakit_dalam","textarea","STATUS PENYAKIT DALAM","")
    ->addModal("neurologi","textarea","STATUS NEUROLOGI","")
    ->addModal("diagnosis_kerja","textarea","DIAGNOSIS KERJA / DIAGNOSIS BANDING","")
    ->addModal("terapi","textarea","TERAPI","")
    ->addModal("rencana_kerja","textarea","RENCANA KERJA","")
    
    ->addModal("", "label", "<strong>DISPOSISI</strong>", "")
    ->addModal("waktu_pulang","datetime","Tanggal Pulang","")
    ->addModal("waktu_kontrol_klinik","datetime","Tanggal Kontrol","")
    ->addModal("dirawat","text","Di Rawat","");
    
$ms ->getAdapter()
    ->setUseNumber(true,"No.","back.")
    ->add("Tanggal Assesmen","waktu","date d M Y H:i")
    ->add("Dokter","nama_dokter")
    ->add("No. Reg","noreg_pasien")
    ->add("NRM","nrm_pasien")
    ->add("id_dokter","id_dokter");
    
$ms ->setTableActionClass("AssesmentDokterJalanAction")
    ->setDateTimeEnable(true)
    ->setModalTitle("Assessment Poli Psikiatri")
    ->setModalComponentSize(Modal::$MEDIUM)
    ->setModalSize(Modal::$HALF_MODEL)
    ->setAutoReload(true)
    ->initialize();

?>