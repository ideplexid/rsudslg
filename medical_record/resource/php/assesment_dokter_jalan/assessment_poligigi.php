<?php   
    global $db;
    global $user;
    require_once "smis-libs-class/MasterTemplate.php";
    require_once "smis-libs-class/MasterSlaveTemplate.php";
    require_once "medical_record/class/table/AssesmentDokterRJTable.php";
    
    $id_dokter  =  getSettings($db,"mr-id-dokter-".$user->getUsername(),"");
    $nm_dokter  =  getSettings($db,"mr-nama-dokter-".$user->getUsername(),"");
    $file_url   =  get_fileurl(getSettings($db,"smis-assesment-dokter-poligigi-odontogram",""));
    
    $header     = array("No.","Tanggal","Dokter","No. Reg","D. Kerja","D. Sementara");
    $uitable    = new AssesmentDokterRJTable($header);
    $uitable ->setNoReg($_POST['noreg_pasien'])
             ->setDokter($id_dokter,$nm_dokter)
             ->setName("assessment_poligigi")
             ->setDetailButtonEnable(true);
    $ms = new MasterSlaveTemplate($db,"smis_mr_poligigi","medical_record","assessment_poligigi");
    $ms ->getDBtable()
        ->setOrder(" tanggal DESC ",true)
        ->addCustomKriteria(" nrm_pasien "," ='".$_POST['nrm_pasien']."' ");
    $ms ->addJSColumn("id",true)
        ->addJSColumn("tanggal",false)
        ->addJSColumn("keluhan",false)
        ->addJSColumn("penyakit",false)
        ->addJSColumn("pengobatan",false)
        ->addJSColumn("file_odontogram",false)
        ->addJSColumn("odontogram",false)
        ->addJSColumn("diagnosa_sementara",false)
        ->addJSColumn("diagnosa_kerja",false)
        ->addJSColumn("rencana_kerja",false)
        ->addJSColumn("terapi_tindakan",false)
        ->addJSColumn("tanggal_pulang",false)
        ->addJSColumn("tanggal_kontrol",false)
        ->addJSColumn("dirawat",false);    
    $ms ->setUITable($uitable);
    $ms ->getUItable()
        ->addModal("id_dokter","hidden","",$id_dokter)
        ->addModal("nama_dokter","hidden","",$nm_dokter);
    $ms ->addNoClear("nama_dokter")
        ->addNoClear("id_dokter");
    $ms ->getForm();
    $ms ->getUItable()
        ->clearContent();
    $ms ->getUItable()
        ->addModal("id","hidden","","" )
        ->addModal("tanggal","datetime","Tanggal",date("Y-m-d H:i:s") )
        ->addModal("keluhan","textarea","Keluhan","")
        ->addModal("penyakit","textarea","Penyakit","")
        ->addModal("pengobatan","textarea","Pengobatan","")
        ->addModal("file_odontogram","hidden","",$file_url)
        ->addModal("odontogram","draw-component-assessment_poligigi_file_odontogram","Odontogram","")
        ->addModal("diagnosa_sementara","text","Diagnosa Sementara","")
        ->addModal("diagnosa_kerja","text","Diagnosa Kerja","")
        ->addModal("rencana_kerja","text","Rencana Kerja","")
        ->addModal("terapi_tindakan","text","Terapi Tindakan","")
        ->addModal("tanggal_pulang","datetime","Tanggal Pulang","")
        ->addModal("tanggal_kontrol","datetime","Tanggal Kontrol","")
        ->addModal("dirawat","text","Di Rawat","");
    $ms ->getAdapter()
        ->setUseNumber(true,"No.","back.")
        ->add("Tanggal","tanggal","date d M Y H:i")
        ->add("Dokter","nama_dokter")
        ->add("No. Reg","noreg_pasien")
        ->add("D. Sementara","diagnosa_sementara")
        ->add("D. Kerja","diagnosa_kerja")
        ->add("id_dokter","id_dokter");
    $ms ->setTableActionClass("AssesmentDokterJalanAction")
        ->setDateTimeEnable(true)
        ->setModalTitle("Assessment Poli Gigi & Mulut")
        ->setModalComponentSize(Modal::$MEDIUM)
        ->setModalSize(Modal::$HALF_MODEL)
        ->setAutoReload(true)
        ->initialize();   
?>