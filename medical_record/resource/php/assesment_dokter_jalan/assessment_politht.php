<?php

global $db;
global $user;
require_once "smis-libs-class/MasterTemplate.php";
require_once "smis-libs-class/MasterSlaveTemplate.php";
require_once "medical_record/class/table/AssesmentDokterRJTable.php";

$id_dokter  =  getSettings($db,"mr-id-dokter-".$user->getUsername(),"");
$nm_dokter  =  getSettings($db,"mr-nama-dokter-".$user->getUsername(),"");
$file_url   =  get_fileurl(getSettings($db,"smis-assesment-dokter-politht",""));

$header     = array("No.","Tanggal Assesmen","Dokter","No. Reg","NRM");
$uitable    = new AssesmentDokterRJTable($header);
$uitable ->setNoReg($_POST['noreg_pasien'])
         ->setDokter($id_dokter,$nm_dokter)
         ->setName("assessment_politht")
         ->setDetailButtonEnable(true);
         
$ms = new MasterSlaveTemplate($db,"smis_mr_politht","medical_record","assessment_politht");
$ms ->getDBtable()
    ->setOrder(" waktu DESC ",true)
    ->addCustomKriteria(" nrm_pasien "," ='".$_POST['nrm_pasien']."' ");
    
$ms ->addJSColumn("id",true)
    ->addJSColumn("waktu",false)
    ->addJSColumn("keluhan_utama",false)
    ->addJSColumn("keluhan_tambahan",false)
    ->addJSColumn("riwayat_penyakit",false)
    
    ->addJSColumn("telinga_sekret_kanan",false)
    ->addJSColumn("telinga_tuli_kanan",false)
    ->addJSColumn("telinga_tumor_kanan",false)
    ->addJSColumn("telinga_tinitus_kanan",false)
    ->addJSColumn("telinga_sakit_kanan",false)
    ->addJSColumn("telinga_korpus_alienum_kanan",false)
    ->addJSColumn("telinga_vertigo_kanan",false)
    ->addJSColumn("telinga_buntu_kanan",false)
    ->addJSColumn("telinga_gatal_kanan",false)
    
    ->addJSColumn("telinga_sekret_kiri",false)
    ->addJSColumn("telinga_tuli_kiri",false)
    ->addJSColumn("telinga_tumor_kiri",false)
    ->addJSColumn("telinga_tinitus_kiri",false)
    ->addJSColumn("telinga_sakit_kiri",false)
    ->addJSColumn("telinga_korpus_alienum_kiri",false)
    ->addJSColumn("telinga_vertigo_kiri",false)
    ->addJSColumn("telinga_buntu_kiri",false)
    ->addJSColumn("telinga_gatal_kiri",false)
    
    ->addJSColumn("telinga_daun_kanan",false)
    ->addJSColumn("telinga_liang_kanan",false)
    ->addJSColumn("telinga_discharge_kanan",false)
    ->addJSColumn("telinga_membran_timpani_kanan",false)
    ->addJSColumn("telinga_tumor_kanan",false)
    ->addJSColumn("telinga_mastoid_kanan",false)
    ->addJSColumn("telinga_tes_pendengaran_kanan",false)
    ->addJSColumn("telinga_berbisik_kanan",false)
    ->addJSColumn("telinga_weber_kanan",false)
    ->addJSColumn("telinga_rinne_kanan",false)
    ->addJSColumn("telinga_schwabach_kanan",false)
    
    ->addJSColumn("telinga_daun_kiri",false)
    ->addJSColumn("telinga_liang_kiri",false)
    ->addJSColumn("telinga_discharge_kiri",false)
    ->addJSColumn("telinga_membran_timpani_kiri",false)
    ->addJSColumn("telinga_tumor_kiri",false)
    ->addJSColumn("telinga_mastoid_kiri",false)
    ->addJSColumn("telinga_tes_pendengaran_kiri",false)
    ->addJSColumn("telinga_berbisik_kiri",false)
    ->addJSColumn("telinga_weber_kiri",false)
    ->addJSColumn("telinga_rinne_kiri",false)
    ->addJSColumn("telinga_schwabach_kiri",false)
    
    ->addJSColumn("hidung_sekret_kanan",false)
    ->addJSColumn("hidung_tersumbat_kanan",false)
    ->addJSColumn("hidung_tumor_kanan",false)
    ->addJSColumn("hidung_pilek_kanan",false)
    ->addJSColumn("hidung_sakit_kanan",false)
    ->addJSColumn("hidung_korpus_alienum_kanan",false)
    ->addJSColumn("hidung_bersin_kanan",false)
    
    ->addJSColumn("hidung_sekret_kiri",false)
    ->addJSColumn("hidung_tersumbat_kiri",false)
    ->addJSColumn("hidung_tumor_kiri",false)
    ->addJSColumn("hidung_pilek_kiri",false)
    ->addJSColumn("hidung_sakit_kiri",false)
    ->addJSColumn("hidung_korpus_alienum_kiri",false)
    ->addJSColumn("hidung_bersin_kiri",false)
    
    ->addJSColumn("hidung_luar_kanan",false)
    ->addJSColumn("hidung_kavum_nasi_kanan",false)
    ->addJSColumn("hidung_septum_kanan",false)
    ->addJSColumn("hidung_discharge_kanan",false)
    ->addJSColumn("hidung_mukosa_kanan",false)
    ->addJSColumn("hidung_tumor_kanan",false)
    ->addJSColumn("hidung_konka_kanan",false)
    ->addJSColumn("hidung_sinus_kanan",false)
    ->addJSColumn("hidung_koana_kanan",false)
    ->addJSColumn("hidung_naso_endoskopi_kanan",false)
    
    ->addJSColumn("hidung_luar_kiri",false)
    ->addJSColumn("hidung_kavum_nasi_kiri",false)
    ->addJSColumn("hidung_septum_kiri",false)
    ->addJSColumn("hidung_discharge_kiri",false)
    ->addJSColumn("hidung_mukosa_kiri",false)
    ->addJSColumn("hidung_tumor_kiri",false)
    ->addJSColumn("hidung_konka_kiri",false)
    ->addJSColumn("hidung_sinus_kiri",false)
    ->addJSColumn("hidung_koana_kiri",false)
    ->addJSColumn("hidung_naso_endoskopi_kiri",false)
    
    ->addJSColumn("tenggorokan_riak",false)
    ->addJSColumn("tenggorokan_gangguan",false)
    ->addJSColumn("tenggorokan_suara",false)
    ->addJSColumn("tenggorokan_tumor",false)
    ->addJSColumn("tenggorokan_batuk",false)
    ->addJSColumn("tenggorokan_korpus_alienum",false)
    ->addJSColumn("tenggorokan_sesak_nafas",false)
    ->addJSColumn("tenggorokan_mukosa",false)
    ->addJSColumn("tenggorokan_dinding_belakang",false)
    
    ->addJSColumn("laring_epiglotis",false)
    ->addJSColumn("laring_aritenoid",false)
    ->addJSColumn("laring_plika_ventrikuloris",false)
    ->addJSColumn("laring_endoskopi",false)
    ->addJSColumn("laring_plika_vokalis",false)
    ->addJSColumn("laring_rima_glotis",false)
    
    ->addJSColumn("diagnosa_kerja",false)
    ->addJSColumn("kelenjar_limpe_leher",false)
    ->addJSColumn("terapi",false)
    
    ->addJSColumn("neurovaskuler",false)
    ->addJSColumn("neuroimaging",false)
    ->addJSColumn("elektrodiagnostik",false)
    ->addJSColumn("laboratorium",false)
    ->addJSColumn("foto",false)
    ->addJSColumn("audiogram",false)
    ->addJSColumn("oae",false)
    ->addJSColumn("lain",false)
    
    ->addJSColumn("tanggal_pulang",false)
    ->addJSColumn("tanggal_kontrol",false)
    ->addJSColumn("dirawat",false);
    
$ms ->setUITable($uitable);
$ms ->getUItable()
    ->addModal("id_dokter","hidden","",$id_dokter)
    ->addModal("nama_dokter","hidden","",$nm_dokter);
$ms ->addNoClear("nama_dokter")
    ->addNoClear("id_dokter");
$ms ->getForm();
$ms ->getUItable()
    ->clearContent();
    
$ms ->getUItable()
    ->addModal("id","hidden","","" )
    ->addModal("waktu","datetime","Waktu",date("Y-m-d H:i:s") )
    ->addModal("keluhan_utama","textarea","Keluhan Utama","")
    ->addModal("keluhan_tambahan","textarea","Keluhan Tambahan","")
    ->addModal("riwayat_penyakit","textarea","Riwayat Penyakit","")
    
    /* TELINGA */
    ->addModal("", "label", "<strong>TELINGA</strong>", "")
    ->addModal("", "label", "<strong>Kanan</strong>", "")
    ->addModal("telinga_sekret_kanan","text","Sekret","")
    ->addModal("telinga_tuli_kanan","text","Tuli","")
    ->addModal("telinga_tumor_kanan","text","Tumor","")
    ->addModal("telinga_tinitus_kanan","text","Tinitus","")
    ->addModal("telinga_sakit_kanan","text","Sakit","")
    ->addModal("telinga_korpus_alienum_kanan","text","Korpus Alienum","")
    ->addModal("telinga_vertigo_kanan","text","Vertigo","")
    ->addModal("telinga_buntu_kanan","text","Buntu","")
    ->addModal("telinga_gatal_kanan","text","Gatal","")
    
    ->addModal("telinga_daun_kanan","text","Daun Telinga","")
    ->addModal("telinga_liang_kanan","text","Liang Telinga","")
    ->addModal("telinga_discharge_kanan","text","Discharge","")
    ->addModal("telinga_membran_timpani_kanan","text","Membrana Timpani","")
    ->addModal("telinga_tumor_kanan","text","Tumor","")
    ->addModal("telinga_mastoid_kanan","text","Mastoid","")
    ->addModal("telinga_tes_pendengaran_kanan","text","Tes Pendengaran","")
    ->addModal("telinga_berbisik_kanan","text","Berbisik","")
    ->addModal("telinga_weber_kanan","text","Weber","")
    ->addModal("telinga_rinne_kanan","text","Rinne","")
    ->addModal("telinga_schwabach_kanan","text","Schwabach","")
    
    ->addModal("", "label", "<strong>Kiri</strong>", "")
    ->addModal("telinga_sekret_kiri","text","Sekret","")
    ->addModal("telinga_tuli_kiri","text","Tuli","")
    ->addModal("telinga_tumor_kiri","text","Tumor","")
    ->addModal("telinga_tinitus_kiri","text","Tinitus","")
    ->addModal("telinga_sakit_kiri","text","Sakit","")
    ->addModal("telinga_korpus_alienum_kiri","text","Korpus Alienum","")
    ->addModal("telinga_vertigo_kiri","text","Vertigo","")
    ->addModal("telinga_buntu_kiri","text","Buntu","")
    ->addModal("telinga_gatal_kiri","text","Gatal","")
    
    ->addModal("telinga_daun_kiri","text","Daun Telinga","")
    ->addModal("telinga_liang_kiri","text","Liang Telinga","")
    ->addModal("telinga_discharge_kiri","text","Discharge","")
    ->addModal("telinga_membran_timpani_kiri","text","Membrana Timpani","")
    ->addModal("telinga_tumor_kiri","text","Tumor","")
    ->addModal("telinga_mastoid_kiri","text","Mastoid","")
    ->addModal("telinga_tes_pendengaran_kiri","text","Tes Pendengaran","")
    ->addModal("telinga_berbisik_kiri","text","Berbisik","")
    ->addModal("telinga_weber_kiri","text","Weber","")
    ->addModal("telinga_rinne_kiri","text","Rinne","")
    ->addModal("telinga_schwabach_kiri","text","Schwabach","")
    
    /* HIDUNG */
    ->addModal("", "label", "<strong>HIDUNG</strong>", "")
    ->addModal("", "label", "<strong>Kanan</strong>", "")
    ->addModal("hidung_sekret_kanan","text","Sekret","")
    ->addModal("hidung_tersumbat_kanan","text","Tersumbat","")
    ->addModal("hidung_tumor_kanan","text","Tumor","")
    ->addModal("hidung_pilek_kanan","text","Pilek","")
    ->addModal("hidung_sakit_kanan","text","Sakit","")
    ->addModal("hidung_korpus_alienum_kanan","text","Korpus Alienum","")
    ->addModal("hidung_bersin_kanan","text","Bersin","")
    
    ->addModal("hidung_luar_kanan","text","Hidung Luar","")
    ->addModal("hidung_kavum_nasi_kanan","text","Kavum Nasi","")
    ->addModal("hidung_septum_kanan","text","Septum","")
    ->addModal("hidung_discharge_kanan","text","Discharge","")
    ->addModal("hidung_mukosa_kanan","text","Mukosa","")
    ->addModal("hidung_tumor_kanan","text","Tumor","")
    ->addModal("hidung_konka_kanan","text","Konka","")
    ->addModal("hidung_sinus_kanan","text","Sinus","")
    ->addModal("hidung_koana_kanan","text","Koana","")
    ->addModal("hidung_naso_endoskopi_kanan","text","Naso Endoskopi","")
    
    ->addModal("", "label", "<strong>Kiri</strong>", "")
    ->addModal("hidung_sekret_kiri","text","Sekret","")
    ->addModal("hidung_tersumbat_kiri","text","Tersumbat","")
    ->addModal("hidung_tumor_kiri","text","Tumor","")
    ->addModal("hidung_pilek_kiri","text","Pilek","")
    ->addModal("hidung_sakit_kiri","text","Sakit","")
    ->addModal("hidung_korpus_alienum_kiri","text","Korpus Alienum","")
    ->addModal("hidung_bersin_kiri","text","Bersin","")
    
    ->addModal("hidung_luar_kiri","text","Hidung Luar","")
    ->addModal("hidung_kavum_nasi_kiri","text","Kavum Nasi","")
    ->addModal("hidung_septum_kiri","text","Septum","")
    ->addModal("hidung_discharge_kiri","text","Discharge","")
    ->addModal("hidung_mukosa_kiri","text","Mukosa","")
    ->addModal("hidung_tumor_kiri","text","Tumor","")
    ->addModal("hidung_konka_kiri","text","Konka","")
    ->addModal("hidung_sinus_kiri","text","Sinus","")
    ->addModal("hidung_koana_kiri","text","Koana","")
    ->addModal("hidung_naso_endoskopi_kiri","text","Naso Endoskopi","")
    
     /* TENGGOROK */
    ->addModal("", "label", "<strong>TENGGOROK</strong>", "")
    ->addModal("tenggorokan_riak","text","Riak","")
    ->addModal("tenggorokan_gangguan","text","Gangguan","")
    ->addModal("tenggorokan_suara","text","Suara","")
    ->addModal("tenggorokan_tumor","text","Tumor","")
    ->addModal("tenggorokan_batuk","text","Batuk","")
    ->addModal("tenggorokan_korpus_alienum","text","Korpus Alienum","")
    ->addModal("tenggorokan_sesak_nafas","text","Sesak Nafas","")
    ->addModal("tenggorokan_mukosa","text","Mukosa","")
    ->addModal("tenggorokan_dinding_belakang","text","Dinding belakang","")
    
    /* LARING */
    ->addModal("", "label", "<strong>TENGGOROK</strong>", "")
    ->addModal("laring_epiglotis","text","Epiglotis","")
    ->addModal("laring_aritenoid","text","Aritenoid","")
    ->addModal("laring_plika_ventrikuloris","text","Plika Ventrikuloris","")
    ->addModal("laring_endoskopi","text","Endoskopi","")
    ->addModal("laring_plika_vokalis","text","Plika Vokalis","")
    ->addModal("laring_rima_glotis","text","Rima Glotis","")
    
    ->addModal("kelenjar_limpe_leher","textarea","Kelenjar limpe leher","")
    ->addModal("diagnosa_kerja","textarea","Rima DIAGNOSA KERJA","")
    ->addModal("terapi","textarea","TERAPI","")
    
    /* USUL PEMERIKSAAN PENUNJANG */
    ->addModal("", "label", "<strong>USUL PEMERIKSAAN PENUNJANG</strong>", "")
    ->addModal("neurovaskuler","text","NeuroVaskuler","")
    ->addModal("neuroimaging","text","Neuroimaging","")
    ->addModal("elektrodiagnostik","text","Elektrodiagnostik","")
    ->addModal("laboratorium","text","Laboratorium","")
    ->addModal("foto","text","Foto","")
    ->addModal("audiogram","text","Audiogram","")
    ->addModal("oae","text","OAE","")
    ->addModal("lain","text","Lain-lain","")
    
    ->addModal("", "label", "<strong>DISPOSISI</strong>", "")
    ->addModal("tanggal_pulang","datetime","Tanggal Pulang","")
    ->addModal("tanggal_kontrol","datetime","Tanggal Kontrol","")
    ->addModal("dirawat","text","Di Rawat","");
    
$ms ->getAdapter()
    ->setUseNumber(true,"No.","back.")
    ->add("Tanggal Assesmen","waktu","date d M Y H:i")
    ->add("Dokter","nama_dokter")
    ->add("No. Reg","noreg_pasien")
    ->add("NRM","nrm_pasien")
    ->add("id_dokter","id_dokter");
    
$ms ->setTableActionClass("AssesmentDokterJalanAction")
    ->setDateTimeEnable(true)
    ->setModalTitle("Assessment Poli THT")
    ->setModalComponentSize(Modal::$MEDIUM)
    ->setModalSize(Modal::$HALF_MODEL)
    ->setAutoReload(true)
    ->initialize();  

?>