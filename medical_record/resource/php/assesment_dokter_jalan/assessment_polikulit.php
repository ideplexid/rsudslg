<?php

global $db;
global $user;
require_once "smis-libs-class/MasterTemplate.php";
require_once "smis-libs-class/MasterSlaveTemplate.php";
require_once "medical_record/class/table/AssesmentDokterRJTable.php";

$id_dokter              =  getSettings($db,"mr-id-dokter-".$user->getUsername(),"");
$nm_dokter              =  getSettings($db,"mr-nama-dokter-".$user->getUsername(),"");
$file_url_dermatologi   =  get_fileurl(getSettings($db,"smis-assesment-dokter-polikulit-dermatologi",""));

$header     = array("No.","Tanggal Assesmen","Dokter","No. Reg","NRM");
$uitable    = new AssesmentDokterRJTable($header);
$uitable ->setNoReg($_POST['noreg_pasien'])
         ->setDokter($id_dokter,$nm_dokter)
         ->setName("assessment_polikulit")
         ->setDetailButtonEnable(true);

$ms = new MasterSlaveTemplate($db,"smis_mr_polikulit","medical_record","assessment_polikulit");
$ms ->getDBtable()
    ->setOrder(" waktu DESC ",true)
    ->addCustomKriteria(" nrm_pasien "," ='".$_POST['nrm_pasien']."' ");
    
$ms ->addJSColumn("id",true)
    ->addJSColumn("waktu",false)
    ->addJSColumn("keluhan_utama",false)
    ->addJSColumn("riwayat_penyakit",false)
    ->addJSColumn("riwayat_pengobatan",false)
    
    ->addJSColumn("status_internus",false)
    
    ->addJSColumn("lokasi",false)
    ->addJSColumn("stigmata_atopik",false)
    ->addJSColumn("mukosa",false)
    ->addJSColumn("rambut",false)
    ->addJSColumn("kuku",false)
    ->addJSColumn("fungsi_kelenjar_keringat",false)
    ->addJSColumn("kelenjar_limfe",false)
    ->addJSColumn("syaraf",false)
    ->addJSColumn("bentuk_kelainan_kulit",false)
    ->addJSColumn("file_gambar_dermatologi",false)
    ->addJSColumn("gambar_dermatologi",false)
    
    ->addJSColumn("laboratorium",false)
    ->addJSColumn("histopatologi_tgl",false)
    ->addJSColumn("histopatologi_no_pa",false)
    
    ->addJSColumn("rencana_awal",false)
    ->addJSColumn("diagnosis",false)
    ->addJSColumn("terapi",false)
    ->addJSColumn("rencana_kerja",false)
    
    ->addJSColumn("tanggal_pulang",false)
    ->addJSColumn("tanggal_kontrol",false)
    ->addJSColumn("dirawat",false);
    
$ms ->setUITable($uitable);
$ms ->getUItable()
    ->addModal("id_dokter","hidden","",$id_dokter)
    ->addModal("nama_dokter","hidden","",$nm_dokter);
$ms ->addNoClear("nama_dokter")
    ->addNoClear("id_dokter");
$ms ->getForm();
$ms ->getUItable()
    ->clearContent();
    
$ms ->getUItable()
    ->addModal("id","hidden","","" )
    ->addModal("waktu","datetime","Waktu",date("Y-m-d H:i:s") )
    
    ->addModal("", "label", "<strong>ANAMNESES</strong>", "")
    ->addModal("keluhan_utama","textarea","Keluhan Utama","")
    ->addModal("riwayat_penyakit","textarea","Riwayat Penyakit","")
    ->addModal("riwayat_pengobatan","textarea","Riwayat Pengobatan","")
    
    ->addModal("status_internus","textarea","STATUS INTERNUS","")
    
    ->addModal("", "label", "<strong>STATUS DERMATOLOGI</strong>", "")
    ->addModal("lokasi","textarea","Lokasi","")
    ->addModal("stigmata_atopik","textarea","Stigmata Atopik","")
    ->addModal("mukosa","textarea","Mukosa","")
    ->addModal("rambut","textarea","Rambut","")
    ->addModal("kuku","textarea","Kuku","")
    ->addModal("fungsi_kelenjar_keringat","textarea","Fungsi Kelenjar Keringat","")
    ->addModal("kelenjar_limfe","textarea","Kelenjar Limfe","")
    ->addModal("syaraf","textarea","Syaraf","")
    ->addModal("bentuk_kelainan_kulit","textarea","Bentuk Kelainan Kulit (Eflorisensi)","")
    ->addModal("file_gambar_dermatologi","hidden","",$file_url_dermatologi)
    ->addModal("gambar_dermatologi","draw-component-assessment_polikulit_file_gambar_dermatologi","Gambar Dermatologi","")
    
    ->addModal("", "label", "<strong>HASIL PEMERIKSAAN PENUNJANG</strong>", "")
    ->addModal("laboratorium","textarea","Laboratorium","")
    ->addModal("histopatologi_tgl","text","Histopatologi Tanggal","")
    ->addModal("histopatologi_no_pa","text","Histopatologi No.PA","")
    
    ->addModal("rencana_awal","textarea","RENCANA AWAL","")
    ->addModal("diagnosis","textarea","DIAGNOSA (Kode ICD-X)","")
    ->addModal("terapi","textarea","TERAPI?TINDAKAN","")
    ->addModal("rencana_kerja","textarea","RENCANA KERJA","")
    
    ->addModal("", "label", "<strong>DISPOSISI</strong>", "")
    ->addModal("tanggal_pulang","datetime","Tanggal Pulang","")
    ->addModal("tanggal_kontrol","datetime","Tanggal Kontrol","")
    ->addModal("dirawat","text","Di Rawat","");
    
$ms ->getAdapter()
    ->setUseNumber(true,"No.","back.")
    ->add("Tanggal Assesmen","waktu","date d M Y H:i")
    ->add("Dokter","nama_dokter")
    ->add("No. Reg","noreg_pasien")
    ->add("NRM","nrm_pasien")
    ->add("id_dokter","id_dokter");
    
$ms ->setTableActionClass("AssesmentDokterJalanAction")
    ->setDateTimeEnable(true)
    ->setModalTitle("Assessment Poli Kulit dan Kelamin")
    ->setModalComponentSize(Modal::$MEDIUM)
    ->setModalSize(Modal::$HALF_MODEL)
    ->setAutoReload(true)
    ->initialize();  

?>