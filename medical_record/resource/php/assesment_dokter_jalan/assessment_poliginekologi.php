<?php

global $db;
global $user;
require_once "smis-libs-class/MasterTemplate.php";
require_once "smis-libs-class/MasterSlaveTemplate.php";
require_once "medical_record/class/table/AssesmentDokterRJTable.php";

$id_dokter  =  getSettings($db,"mr-id-dokter-".$user->getUsername(),"");
$nm_dokter  =  getSettings($db,"mr-nama-dokter-".$user->getUsername(),"");

$header     = array("No.","Tanggal Assesmen","Dokter","No. Reg","D. Kerja","D. Sementara");
$uitable    = new AssesmentDokterRJTable($header);
$uitable ->setNoReg($_POST['noreg_pasien'])
         ->setDokter($id_dokter,$nm_dokter)
         ->setName("assessment_poliginekologi")
         ->setDetailButtonEnable(true);
         
$ms = new MasterSlaveTemplate($db,"smis_mr_poliginekologi","medical_record","assessment_poliginekologi");
$ms ->getDBtable()
    ->setOrder(" waktu DESC ",true);
    
if($_POST['nrm_pasien'] != NULL || $_POST['nrm_pasien'] != '') {
    $ms ->getDBtable()
        ->addCustomKriteria(" nrm_pasien "," ='".$_POST['nrm_pasien']."' ");
}
    
$ms ->addJSColumn("id",true)
    ->addJSColumn("waktu",false)
    
    ->addJSColumn("rujukan_dari",false)
    ->addJSColumn("surat_rujukan",false)
    ->addJSColumn("surat_rujukan_lengkap",false)
    ->addJSColumn("diagnosa",false)
    ->addJSColumn("obat_tindakan",false)
    
    ->addJSColumn("keluhan_utama",false)
    
    ->addJSColumn("keadaan_umum",false)
    
    ->addJSColumn("kesadaran",false)
    ->addJSColumn("kesadaran_composmentis",false)
    ->addJSColumn("kesadaran_apatis",false)
    ->addJSColumn("kesadaran_somnolen",false)
    ->addJSColumn("kesadaran_sopor",false)
    ->addJSColumn("kesadaran_coma",false)
    
    ->addJSColumn("gcs",false)
    ->addJSColumn("td",false)
    ->addJSColumn("suhu",false)
    ->addJSColumn("nadi",false)
    ->addJSColumn("rr",false)
    
    ->addJSColumn("anemis",false)
    ->addJSColumn("cyanosis",false)
    ->addJSColumn("dyspneu",false)
    ->addJSColumn("ikterus",false)
    
    ->addJSColumn("kepala",false)
    ->addJSColumn("toraks",false)
    ->addJSColumn("pemeriksaan_abdomen",false)
    
    ->addJSColumn("pemeriksaan_genetalia",false)
    ->addJSColumn("genetalia_v_t",false)
    ->addJSColumn("genetalia_p",false)
    ->addJSColumn("genetalia_cu",false)
    ->addJSColumn("genetalia_ap_ka",false)
    ->addJSColumn("genetalia_ap_ki",false)
    ->addJSColumn("genetalia_cd",false)
    ->addJSColumn("genetalia_inspekulo",false)
    ->addJSColumn("genetalia_douglas",false)
    
    ->addJSColumn("usg",false)
    ->addJSColumn("ct_scan",false)
    ->addJSColumn("laboratorium",false)
    ->addJSColumn("diagnosis",false)
    ->addJSColumn("terapi",false);
    
$ms ->setUITable($uitable);
$ms ->getUItable()
    ->addModal("id_dokter","hidden","",$id_dokter)
    ->addModal("nama_dokter","hidden","",$nm_dokter);
$ms ->addNoClear("nama_dokter")
    ->addNoClear("id_dokter");
$ms ->getForm();
$ms ->getUItable()
    ->clearContent();
    
// For Dropdown List
$ada_tidak = array(
    array("name"=>"","value"=>"","default"=> 1),
    array("name"=>"Ada","value"=>"Ada"),
    array("name"=>"Tidak","value"=>"Tidak"),
);
$lengkap = array(
    array("name"=>"","value"=>"","default"=> 1),
    array("name"=>"Lengkap","value"=>"Lengkap"),
    array("name"=>"Tidak","value"=>"Tidak"),
);
    
$ms ->getUItable()
    ->addModal("id","hidden","","" )
    ->addModal("waktu","datetime","Waktu",date("Y-m-d H:i:s") )
    
    ->addModal("rujukan_dari","text","Rujukan Dari","" )
    ->addModal("surat_rujukan","select","Surat Rujukan",$ada_tidak )
    ->addModal("surat_rujukan_lengkap","select","",$lengkap )
    ->addModal("diagnosa","text","Diagnosa","" )
    ->addModal("obat_tindakan","text","Obat / Tindakan","" )
    
    ->addModal("", "label", "<strong>ANAMNESA </strong>", "")
    ->addModal("keluhan_utama","textarea","Keluhan Utama","" )
    
    ->addModal("", "label", "<strong>PEMERIKSAAN FISIK</strong>", "")
    ->addModal("keadaan_umum","textarea","Keadaan Umum","" )
    ->addModal("", "label", "Kesadaran", "")
    ->addModal("kesadaran_composmentis","checkbox","Composmentis","" )
    ->addModal("kesadaran_apatis","checkbox","Apatis","" )
    ->addModal("kesadaran_somnolen","checkbox","Somnolen","" )
    ->addModal("kesadaran_sopor","checkbox","Sopor","" )
    ->addModal("kesadaran_coma","checkbox","Coma","" )
    
    ->addModal("gcs","text","GCS","" )
    
    ->addModal("", "label", "Tanda Vital", "")
    ->addModal("anemis","checkbox","Anemis","" )
    ->addModal("cyanosis","checkbox","Cyanosis","" )
    ->addModal("dyspneu","checkbox","Dyspneu","" )
    ->addModal("ikterus","checkbox","Ikterus","" )
    ->addModal("gcs","text","GCS","" )
    ->addModal("td","text","TD (mmHg)","" )
    ->addModal("suhu","text","Suhu (*C)","" )
    ->addModal("nadi","text","Nadi (x/mnt)","" )
    ->addModal("rr","text","RR (x/mnt)", "" )
    
    ->addModal("kepala","text","Kepala", "" )
    ->addModal("toraks","text","Toraks", "" )
    
    ->addModal("pemeriksaan_abdomen","text","Pemeriksaan Abdomen", "" )
    
    ->addModal("pemeriksaan_genetalia","text","Pemeriksaan Genetalia", "" )
    ->addModal("genetalia_v_t","text","V/T", "" )
    ->addModal("genetalia_p","text","P", "" )
    ->addModal("genetalia_cu","text","CU", "" )
    ->addModal("genetalia_ap_ka","text","AP Ka", "" )
    ->addModal("genetalia_ap_ki","text","AP Ki", "" )
    ->addModal("genetalia_cd","text","CD", "" )
    ->addModal("genetalia_inspekulo","text","Inspekulo", "" )
    ->addModal("genetalia_douglas","text","Fungsi Douglas", "" )
    
    ->addModal("", "label", "<strong>PEMERIKSAAN FISIK</strong>", "")
    ->addModal("usg","textarea","USG", "" )
    ->addModal("ct_scan","textarea","CT Scan", "" )
    ->addModal("laboratorium","textarea","Laboratorium", "" )
    ->addModal("diagnosis","textarea","Diagnosa", "" )
    ->addModal("terapi","textarea","Terapi", "" );
    
$ms ->getAdapter()
    ->setUseNumber(true,"No.","back.")
    ->add("Tanggal Assesmen","waktu","date d M Y H:i")
    ->add("Dokter","nama_dokter")
    ->add("No. Reg","noreg_pasien")
    ->add("NRM","nrm_pasien")
    ->add("id_dokter","id_dokter");
    
$ms ->setTableActionClass("AssesmentDokterJalanAction")
    ->setDateTimeEnable(true)
    ->setModalTitle("Assessment Poli Kandungan - Ginekologi")
    ->setModalComponentSize(Modal::$MEDIUM)
    ->setModalSize(Modal::$HALF_MODEL)
    ->setAutoReload(true)
    ->initialize();  

?>