<?php

global $db;
global $user;
require_once "smis-libs-class/MasterTemplate.php";
require_once "smis-libs-class/MasterSlaveTemplate.php";
require_once "medical_record/class/table/AssesmentDokterRJTable.php";

$id_dokter  =  getSettings($db,"mr-id-dokter-".$user->getUsername(),"");
$nm_dokter  =  getSettings($db,"mr-nama-dokter-".$user->getUsername(),"");

$header     = array("No.","Tanggal Assesmen","Dokter","No. Reg","NRM");
$uitable    = new AssesmentDokterRJTable($header);
$uitable ->setNoReg($_POST['noreg_pasien'])
         ->setDokter($id_dokter,$nm_dokter)
         ->setName("assessment_polianak")
         ->setDetailButtonEnable(true);
         
$ms = new MasterSlaveTemplate($db,"smis_mr_assesmen_anak","medical_record","assessment_polianak");
$ms ->getDBtable()
    ->setOrder(" waktu DESC ",true);

if($_POST['nrm_pasien'] != NULL || $_POST['nrm_pasien'] != '') {
    $ms ->getDBtable()
        ->addCustomKriteria(" nrm_pasien "," ='".$_POST['nrm_pasien']."' ");
}
    
$ms ->addJSColumn("id",true)
    ->addJSColumn("waktu",false)
    
    ->addJSColumn("anamneses_utama",false)
    ->addJSColumn("anamneses_penyakit_skrg",false)
    ->addJSColumn("anamneses_pengobatan",false)
    
    ->addJSColumn("mata_anemia",false)
    ->addJSColumn("mata_ikterus",false)
    ->addJSColumn("mata_reflek_pupil",false)
    ->addJSColumn("mata_edema_palbebrae",false)
    
    ->addJSColumn("tht_tonsil",false)
    ->addJSColumn("tht_faring",false)
    ->addJSColumn("tht_lidah",false)
    ->addJSColumn("tht_bibir",false)
    
    ->addJSColumn("leher_jvp",false)
    ->addJSColumn("leher_pembesaran_kelenjar",false)
    ->addJSColumn("kaku_kuduk",false)
    
    ->addJSColumn("torak",false)
    
    ->addJSColumn("jantung_s1_s2",false)
    ->addJSColumn("jantung_reguler",false)
    ->addJSColumn("jantung_ireguler",false)
    ->addJSColumn("jantung_murmur",false)
    ->addJSColumn("jantung_lain",false)
    
    ->addJSColumn("paru_suara_nafas",false)
    ->addJSColumn("paru_ronki",false)
    ->addJSColumn("paru_wheezing",false)
    ->addJSColumn("paru_lain",false)
    
    ->addJSColumn("abdomen_distensi",false)
    ->addJSColumn("abdomen_meteorismus",false)
    ->addJSColumn("abdomen_peristaltik",false)
    ->addJSColumn("abdomen_arcites",false)
    ->addJSColumn("abdomen_nyeri",false)
    ->addJSColumn("abdomen_nyeri_lokasi",false)
    
    ->addJSColumn("hepar",false)
    ->addJSColumn("lien",false)
    ->addJSColumn("status_gizi",false)
    
    ->addJSColumn("extremitas_hangat",false)
    ->addJSColumn("extremitas_dingin",false)
    ->addJSColumn("extremitas_edema",false)
    ->addJSColumn("extremitas_lain",false)
    
    ->addJSColumn("rencana_kerja",false)
    ->addJSColumn("hasil_periksa_penunjang",false)
    ->addJSColumn("diagnosis",false)
    ->addJSColumn("terapi",false)
    ->addJSColumn("hasil_pembedahan",false)
    ->addJSColumn("rekomendasi",false)
    ->addJSColumn("catatan",false)
    
    ->addJSColumn("tanggal_pulang",false)
    ->addJSColumn("tanggal_kontrol",false)
    ->addJSColumn("dirawat",false);
    
$ms ->setUITable($uitable);
$ms ->getUItable()
    ->addModal("id_dokter","hidden","",$id_dokter)
    ->addModal("nama_dokter","hidden","",$nm_dokter);
$ms ->addNoClear("nama_dokter")
    ->addNoClear("id_dokter");
$ms ->getForm();
$ms ->getUItable()
    ->clearContent();
    
// For Dropdown List
$positif_negatif = array(
    array("name"=>"","value"=>"","default"=> 1),
    array("name"=>"Positif (+)","value"=>"Positif"),
    array("name"=>"Negatif (-)","value"=>"Negatif"),
);
$torak = array(
    array("name"=>"","value"=>"","default"=> 1),
    array("name"=>"Simetris","value"=>"simetris"),
    array("name"=>"Asimetris","value"=>"asimetris"),
);
$jantung = array(
    array("name"=>"","value"=>"","default"=> 1),
    array("name"=>"Reguler","value"=>"Reguler"),
    array("name"=>"Ireguler","value"=>"Ireguler"),
);
$abdomen = array(
    array("name"=>"","value"=>"","default"=> 1),
    array("name"=>"Normal","value"=>"Normal"),
    array("name"=>"Meningkat","value"=>"Meningkat"),
    array("name"=>"Menurun","value"=>"Menurun"),
);
$extremitas = array(
    array("name"=>"","value"=>"","default"=> 1),
    array("name"=>"Hangat","value"=>"Hangat"),
    array("name"=>"Dingin","value"=>"Dingin"),
);
    
$ms ->getUItable()
    ->addModal("id","hidden","","" )
    ->addModal("waktu","datetime","Waktu",date("Y-m-d H:i:s") )
    
    ->addModal("", "label", "<strong>ANAMNESES</strong>", "")
    ->addModal("anamneses_utama","textarea","Keluhan Utama","")
    ->addModal("anamneses_penyakit_skrg","textarea","Riwayat Penyakit Sekarang","")
    ->addModal("anamneses_pengobatan","textarea","Riwayat Pengobatan","")
    
    ->addModal("", "label", "<strong>PEMERIKSAAN FISIK</strong>", "")
    ->addModal("", "label", "<strong>Mata</strong>", "")
    ->addModal("mata_anemia","text","Anemia","")
    ->addModal("mata_ikterus","text","Ikterus","")
    ->addModal("mata_reflek_pupil","text","Reflek Pupil","")
    ->addModal("mata_edema_palbebrae","text","Edema Palpebrae","")
    
    ->addModal("", "label", "<strong>THT</strong>", "")
    ->addModal("tht_tonsil","text","Tonsil","")
    ->addModal("tht_faring","text","Faring","")
    ->addModal("tht_lidah","text","Lidah","")
    ->addModal("tht_bibir","text","Bibir","")
    
    ->addModal("", "label", "<strong>Leher</strong>", "")
    ->addModal("leher_jvp","text","JVP","")
    ->addModal("leher_pembesaran_kelenjar","text","Pembesaran Kelenjar","")
    ->addModal("kaku_kuduk", "select", "Kaku Kuduk", $positif_negatif)
    
    ->addModal("", "label", "<strong>Torak</strong>", "")
    ->addModal("torak","select","Torak",$torak)
    
    ->addModal("", "label", "<strong>Jantung</strong>", "")
    ->addModal("jantung","select","Jantung",$jantung)
    ->addModal("jantung_s1_s2","text","S1, S2","")
    ->addModal("jantung_murmur","text","Murmur","")
    ->addModal("jantung_lain","text","Lain-lain","")
    
    ->addModal("", "label", "<strong>Paru</strong>", "")
    ->addModal("paru_suara_nafas","text","Suara Nafas","")
    ->addModal("paru_ronki","text","Ronki","")
    ->addModal("paru_wheezing","text","Wheezing","")
    ->addModal("paru_lain","text","Lain-lain","")
    
    ->addModal("", "label", "<strong>Abdomen</strong>", "")
    ->addModal("abdomen_distensi", "select", "Distensi", $positif_negatif)
    ->addModal("abdomen_meteorismus", "select", "Meteorismus", $positif_negatif)
    ->addModal("abdomen", "select", "Abdomen", $abdomen)
    ->addModal("abdomen_arcites", "select", "Arcites", $positif_negatif)
    ->addModal("abdomen_nyeri", "select", "Nyeri", $positif_negatif)
    ->addModal("abdomen_nyeri_lokasi","text","Lokasi","")
    
    ->addModal("hepar","textarea","<strong>Hepar</strong>","")
    ->addModal("lien","textarea","<strong>Lien</strong>","")
    ->addModal("status_gizi","textarea","<strong>Status Gizi</strong>","")
    ->addModal("extremitas_edema","text","Edema","")
    ->addModal("extremitas_lain","text","Lain-lain","")
    
    ->addModal("rencana_kerja", "textarea", "RENCANA KERJA", "")
    ->addModal("hasil_periksa_penunjang", "textarea", "HASIL PEMERIKSAAN PENUNJANG", "")
    ->addModal("diagnosis", "textarea", "DIAGNOSA", "")
    ->addModal("terapi", "textarea", "TERAPI", "")
    ->addModal("hasil_pembedahan", "textarea", "HASIL PEMBEDAHAN", "")
    ->addModal("rekomendasi", "textarea", "REKOMENDASI (SARAN)", "")
    ->addModal("catatan", "textarea", "CATATAN PENTING", "")
    
    ->addModal("", "label", "<strong>Extremitas</strong>", "")
    ->addModal("extremitas", "select", "Extremitas", $extremitas)
    
    ->addModal("", "label", "<strong>DISPOSISI</strong>", "")
    ->addModal("tanggal_pulang","datetime","Tanggal Pulang","")
    ->addModal("tanggal_kontrol","datetime","Tanggal Kontrol","")
    ->addModal("dirawat","text","Di Rawat","");
    
$ms ->getAdapter()
    ->setUseNumber(true,"No.","back.")
    ->add("Tanggal Assesmen","waktu","date d M Y H:i")
    ->add("Dokter","nama_dokter")
    ->add("No. Reg","noreg_pasien")
    ->add("NRM","nrm_pasien")
    ->add("id_dokter","id_dokter");
    
$ms ->setTableActionClass("AssesmentDokterJalanAction")
    ->setDateTimeEnable(true)
    ->setModalTitle("Assessment Poli Anak")
    ->setModalComponentSize(Modal::$MEDIUM)
    ->setModalSize(Modal::$HALF_MODEL)
    ->setAutoReload(true)
    ->initialize();

?>