<?php

global $db;
global $user;
require_once "smis-libs-class/MasterTemplate.php";
require_once "smis-libs-class/MasterSlaveTemplate.php";
require_once "medical_record/class/table/AssesmentDokterRJTable.php";

$id_dokter              =  getSettings($db,"mr-id-dokter-".$user->getUsername(),"");
$nm_dokter              =  getSettings($db,"mr-nama-dokter-".$user->getUsername(),"");
$file_url_neurologis    =  get_fileurl(getSettings($db,"smis-assesment-dokter-poliorthopedi-neurologis",""));

$header     = array("No.","Tanggal Assesmen","Dokter","No. Reg","NRM");
$uitable    = new AssesmentDokterRJTable($header);
$uitable ->setNoReg($_POST['noreg_pasien'])
         ->setDokter($id_dokter,$nm_dokter)
         ->setName("assessment_poliorthopedi")
         ->setDetailButtonEnable(true);

$ms = new MasterSlaveTemplate($db,"smis_mr_poliorthopedi","medical_record","assessment_poliorthopedi");
$ms ->getDBtable()
    ->setOrder(" waktu DESC ",true)
    ->addCustomKriteria(" nrm_pasien "," ='".$_POST['nrm_pasien']."' ");
    
$ms ->addJSColumn("id",true)
    ->addJSColumn("waktu",false)
    ->addJSColumn("keluhan_medis",false)
    ->addJSColumn("anamneses",false)
    ->addJSColumn("riwayat_penyakit",false)
    
    ->addJSColumn("status_generalis",false)
    ->addJSColumn("inspeksi",false)
    ->addJSColumn("palpasi",false)
    ->addJSColumn("rom",false)
    ->addJSColumn("motoris",false)
    ->addJSColumn("sensoris",false)
    ->addJSColumn("reflek",false)
    ->addJSColumn("file_gambar_neurologis",false)
    ->addJSColumn("gambar_neurologis",false)
    
    ->addJSColumn("laboratorium",false)
    ->addJSColumn("foto",false)
    ->addJSColumn("mri",false)
    ->addJSColumn("ct_scan",false)
    ->addJSColumn("emg_cp",false)
    ->addJSColumn("lain",false)
    
    ->addJSColumn("diagnosa_kerja",false)
    ->addJSColumn("terapi",false)
    ->addJSColumn("rencana_kerja",false)
    
    ->addJSColumn("tanggal_pulang",false)
    ->addJSColumn("tanggal_kontrol",false)
    ->addJSColumn("dirawat",false);
    
$ms ->setUITable($uitable);
$ms ->getUItable()
    ->addModal("id_dokter","hidden","",$id_dokter)
    ->addModal("nama_dokter","hidden","",$nm_dokter);
$ms ->addNoClear("nama_dokter")
    ->addNoClear("id_dokter");
$ms ->getForm();
$ms ->getUItable()
    ->clearContent();
    
$ms ->getUItable()
    ->addModal("id","hidden","","" )
    ->addModal("waktu","datetime","Waktu",date("Y-m-d H:i:s") )
    ->addModal("keluhan_medis","textarea","Keluhan Utama","")
    ->addModal("anamneses","textarea","Keluhan Tambahan","")
    ->addModal("riwayat_penyakit","textarea","Riwayat Penyakit","")
    
    ->addModal("", "label", "<strong>PEMERIKSAAN FISIK</strong>", "")
    ->addModal("status_generalis","textarea","STATUS GENERALIS","")
    
    ->addModal("", "label", "STATUS LOKALIS", "")
    ->addModal("inspeksi","text","Inspeksi","")
    ->addModal("palpasi","text","Palpasi","")
    ->addModal("rom","text","ROM","")
    
    ->addModal("", "label", "STATUS NEUROLOGIS", "")
    ->addModal("motoris","text","Motoris","")
    ->addModal("sensoris","text","Sensoris","")
    ->addModal("reflek","text","Reflek","")
    ->addModal("file_gambar_neurologis","hidden","",$file_url_neurologis)
    ->addModal("gambar_neurologis","draw-component-assessment_poliorthopedi_file_gambar_neurologis","Gambar Neurologis","")
    
    ->addModal("", "label", "<strong>HASIL PEMERIKSAAN PENUNJANG</strong>", "")
    ->addModal("laboratorium","textarea","Laboratorium","")
    ->addModal("foto","textarea","Foto","")
    ->addModal("mri","textarea","MRI","")
    ->addModal("ct_scan","textarea","CT Scan","")
    ->addModal("emg_cp","textarea","EMG-CP","")
    ->addModal("lain","textarea","Lain-lain","")
    
    ->addModal("diagnosa_kerja","textarea","Diagnosa Kerja","")
    ->addModal("terapi","textarea","Terapi","")
    ->addModal("rencana_kerja","textarea","Rencana Kerja","")
    
    ->addModal("", "label", "<strong>DISPOSISI</strong>", "")
    ->addModal("tanggal_pulang","datetime","Tanggal Pulang","")
    ->addModal("tanggal_kontrol","datetime","Tanggal Kontrol","")
    ->addModal("dirawat","text","Di Rawat","");
    
$ms ->getAdapter()
    ->setUseNumber(true,"No.","back.")
    ->add("Tanggal Assesmen","waktu","date d M Y H:i")
    ->add("Dokter","nama_dokter")
    ->add("No. Reg","noreg_pasien")
    ->add("NRM","nrm_pasien")
    ->add("id_dokter","id_dokter");
    
$ms ->setTableActionClass("AssesmentDokterJalanAction")
    ->setDateTimeEnable(true)
    ->setModalTitle("Assessment Poli Orthopedi")
    ->setModalComponentSize(Modal::$MEDIUM)
    ->setModalSize(Modal::$HALF_MODEL)
    ->setAutoReload(true)
    ->initialize();  

?>