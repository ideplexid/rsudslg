<?php

global $db;
global $user;
require_once "smis-libs-class/MasterTemplate.php";
require_once "smis-libs-class/MasterSlaveTemplate.php";
require_once "medical_record/class/table/AssesmentDokterRJTable.php";

$id_dokter  =  getSettings($db,"mr-id-dokter-".$user->getUsername(),"");
$nm_dokter  =  getSettings($db,"mr-nama-dokter-".$user->getUsername(),"");

$header     = array("No.","Tanggal Assesmen","Dokter","No. Reg","NRM");
$uitable    = new AssesmentDokterRJTable($header);
$uitable ->setNoReg($_POST['noreg_pasien'])
         ->setDokter($id_dokter,$nm_dokter)
         ->setName("assessment_polijantung")
         ->setDetailButtonEnable(true);
         
$ms = new MasterSlaveTemplate($db,"smis_mr_assesmen_jantung","medical_record","assessment_polijantung");
$ms ->getDBtable()
    ->setOrder(" waktu DESC ",true);

if($_POST['nrm_pasien'] != NULL || $_POST['nrm_pasien'] != '') {
    $ms ->getDBtable()
        ->addCustomKriteria(" nrm_pasien "," ='".$_POST['nrm_pasien']."' ");
}

$ms ->addJSColumn("id",true)
    ->addJSColumn("waktu",false)
    
    ->addJSColumn("keluhan_utama",false)
    ->addJSColumn("riwayat_penyakit_skrg",false)
    ->addJSColumn("riwayat_pengobatan",false)
    
    ->addJSColumn("torak",false)
    ->addJSColumn("torak_ket",false)
    
    ->addJSColumn("iktus_kordis",false)
    ->addJSColumn("iktus_kordis_normal",false)
    ->addJSColumn("iktus_kordis_melebar",false)
    ->addJSColumn("iktus_kordis_ket",false)
    ->addJSColumn("iktus_kordis_lokasi",false)
    
    ->addJSColumn("pulpasi_apex",false)
    ->addJSColumn("pulpasi_prekordium",false)
    ->addJSColumn("pulpasi_epigastrum",false)
    ->addJSColumn("pulpasi_ket",false)
    
    ->addJSColumn("palpasi_iktus_kordis",false)
    ->addJSColumn("palpasi_iktus_kordis_normal",false)
    ->addJSColumn("palpasi_iktus_kordis_kuat_angkat",false)
    ->addJSColumn("palpasi_iktus_kordis_meluas",false)
    ->addJSColumn("palpasi_iktus_kordis_ket",false)
    ->addJSColumn("palpasi_iktus_kordis_lokasi",false)
    
    ->addJSColumn("palpasi_thrill",false)
    ->addJSColumn("palpasi_thrill_sistolik",false)
    ->addJSColumn("palpasi_thrill_diastolik",false)
    ->addJSColumn("palpasi_thrill_ket",false)
    ->addJSColumn("palpasi_thrill_lokasi",false)
    
    ->addJSColumn("perkusi_batas_atas",false)
    ->addJSColumn("perkusi_batas_bawah",false)
    ->addJSColumn("perkusi_batas_kanan",false)
    ->addJSColumn("perkusi_batas_kiri",false)
    
    ->addJSColumn("sju_s1",false)
    ->addJSColumn("sju_s2",false)
    ->addJSColumn("sju_reguler",false)
    ->addJSColumn("sju_ireguler",false)
    
    ->addJSColumn("extra_systole",false)
    ->addJSColumn("gallop",false)
    
    ->addJSColumn("murmur_fase_sistolik",false)
    ->addJSColumn("murmur_fase_diastolik",false)
    ->addJSColumn("murmur_fase_ket",false)
    
    ->addJSColumn("murmur_lokasi_apex",false)
    ->addJSColumn("murmur_lokasi_ics_2_kiri_psl_kiri",false)
    ->addJSColumn("murmur_lokasi_ics_2_kanan_psl_kanan",false)
    ->addJSColumn("murmur_lokasi_ics_4_psl_kiri",false)
    ->addJSColumn("murmur_lokasi_ket",false)
    
    ->addJSColumn("murmur_kualitas_rumbling",false)
    ->addJSColumn("murmur_kualitas_blowing",false)
    ->addJSColumn("murmur_kualitas_ejection",false)
    ->addJSColumn("murmur_kualitas_ket",false)
    
    ->addJSColumn("murmur_grade_1",false)
    ->addJSColumn("murmur_grade_2",false)
    ->addJSColumn("murmur_grade_3",false)
    ->addJSColumn("murmur_grade_4",false)
    ->addJSColumn("murmur_grade_5",false)
    ->addJSColumn("murmur_grade_6",false)
    
    ->addJSColumn("murmur_penjalaran_axilla",false)
    ->addJSColumn("murmur_penjalaran_punggung",false)
    ->addJSColumn("murmur_penjalaran_ket",false)
    
    ->addJSColumn("murmur_opening_snap",false)
    ->addJSColumn("murmur_friction_rub",false)
    
    ->addJSColumn("paru_vesikuler",false)
    ->addJSColumn("paru_ronki",false)
    ->addJSColumn("paru_wheezing",false)
    ->addJSColumn("paru_ket",false)
    
    ->addJSColumn("ecg",false)
    ->addJSColumn("thorax_foto",false)
    ->addJSColumn("echocardiografi",false)
    ->addJSColumn("diagnosis",false)
    ->addJSColumn("terapi",false)
    ->addJSColumn("rencana_kerja",false)
    
    ->addJSColumn("waktu_pulang",false)
    ->addJSColumn("waktu_kontrol_klinik",false)
    ->addJSColumn("dirawat_diruang",false);
    
$ms ->setUITable($uitable);
$ms ->getUItable()
    ->addModal("id_dokter","hidden","",$id_dokter)
    ->addModal("nama_dokter","hidden","",$nm_dokter);
$ms ->addNoClear("nama_dokter")
    ->addNoClear("id_dokter");
$ms ->getForm();
$ms ->getUItable()
    ->clearContent();
    
// For Dropdown List
$positif_negatif = array(
    array("name"=>"","value"=>"","default"=> 1),
    array("name"=>"Positif (+)","value"=>"Positif"),
    array("name"=>"Negatif (-)","value"=>"Negatif"),
);
$torak = array(
    array("name"=>"","value"=>"","default"=> 1),
    array("name"=>"Simetris","value"=>"simetris"),
    array("name"=>"Asimetris","value"=>"asimetris"),
);
$iktus_kordis = array(
    array("name"=>"","value"=>"","default"=> 1),
    array("name"=>"Normal","value"=>"Normal"),
    array("name"=>"Melebar ke","value"=>"Melebar ke"),
);
    
$ms ->getUItable()
    ->addModal("id","hidden","","" )
    ->addModal("waktu","datetime","Waktu",date("Y-m-d H:i:s") )
    
    ->addModal("", "label", "<strong>ANAMNESES</strong>", "")
    ->addModal("anamneses_utama","textarea","Keluhan Utama","")
    ->addModal("anamneses_penyakit_skrg","textarea","Riwayat Penyakit Sekarang","")
    ->addModal("anamneses_pengobatan","textarea","Riwayat Pengobatan","")
    
    ->addModal("", "label", "<strong>PEMERIKSAAN FISIK</strong>", "")
    ->addModal("", "label", "<strong>Torak</strong>", "")
    ->addModal("torak","select","Torak",$torak)
    
    ->addModal("", "label", "<strong>Jantung</strong>", "")
    ->addModal("", "label", "<strong>Inspeksi</strong>", "")
    ->addModal("", "label", "Iktus Kordis", "")
    ->addModal("iktus_kordis_normal","checkbox","Normal","")
    ->addModal("iktus_kordis_melebar","checkbox","Melebar ke","")
    ->addModal("iktus_kordis_ket","text","Keterangan","")
    
    ->addModal("", "label", "<strong>Pulsasi</strong>", "")
    ->addModal("pulpasi_apex","checkbox","Apex","")
    ->addModal("pulpasi_prekordium","checkbox","Prekordium","")
    ->addModal("pulpasi_epigastrum","checkbox","Epigastrum","")
    ->addModal("pulpasi_ket","text","Keterangan","")
    
    ->addModal("", "label", "<strong>palpasi</strong>", "")
    ->addModal("", "label", "Iktus Kordis", "")
    ->addModal("palpasi_iktus_kordis_normal","checkbox","Normal","")
    ->addModal("palpasi_iktus_kordis_kuat_angkat","checkbox","Kuat angkat","")
    ->addModal("palpasi_iktus_kordis_meluas","checkbox","Meluas","")
    ->addModal("palpasi_iktus_kordis_lokasi","text","Lokasi","")
    
    ->addModal("", "label", "Thrill", "")
    ->addModal("palpasi_thrill_sistolik","checkbox","Sistolik","")
    ->addModal("palpasi_thrill_diastolik","checkbox","Diastolik","")
    ->addModal("palpasi_thrill_lokasi","text","Lokasi","")
    
    ->addModal("", "label", "Thrill", "")
    ->addModal("perkusi_batas_atas","text","Batas Atas","")
    ->addModal("perkusi_batas_bawah","text","Batas Bawah","")
    ->addModal("perkusi_batas_kanan","text","Batas Kanan","")
    ->addModal("perkusi_batas_kiri","text","Batas Kiri","")
    
    ->addModal("", "label", "Auskultasi", "")
    ->addModal("", "label", "Suara Jantung Utama", "")
    ->addModal("sju_s1","text","S1","")
    ->addModal("sju_s2","text","S2","")
    ->addModal("sju_reguler","checkbox","Reguler","")
    ->addModal("sju_ireguler","checkbox","Ireguler","")
    
    ->addModal("extra_systole", "select", "Extra Systole", $positif_negatif)
    ->addModal("gallop", "select", "Gallop", $positif_negatif)
    
    ->addModal("", "label", "Suara Jantung Tambahan", "")
    ->addModal("", "label", "1. Murmur", "")
    ->addModal("", "label", "Fase", "")
    ->addModal("murmur_fase_sistolik","checkbox","Sistolik","")
    ->addModal("murmur_fase_diastolik","checkbox","Diastolik","")
    ->addModal("murmur_fase_ket","text","Lainnya","")
    
    ->addModal("", "label", "Lokasi", "")
    ->addModal("murmur_lokasi_apex","checkbox","Apex","")
    ->addModal("murmur_lokasi_ics_2_kiri_psl_kiri","checkbox","ICS II Kiri PSL Kiri","")
    ->addModal("murmur_lokasi_ics_2_kanan_psl_kanan","checkbox","ICS II Kanan PSL Kanan","")
    ->addModal("murmur_lokasi_ics_4_psl_kiri","checkbox","ICS IV PSL Kiri","")
    ->addModal("murmur_lokasi_ket","text","Lainnya","")
    
    ->addModal("", "label", "Kualitas", "")
    ->addModal("murmur_kualitas_rumbling","checkbox","Rumbling","")
    ->addModal("murmur_kualitas_blowing","checkbox","Blowing","")
    ->addModal("murmur_kualitas_ejection","checkbox","Ejection","")
    ->addModal("murmur_kualitas_ket","text","Lainnya","")
    
    ->addModal("", "label", "Grade", "")
    ->addModal("murmur_grade_1","checkbox","I","")
    ->addModal("murmur_grade_2","checkbox","II","")
    ->addModal("murmur_grade_3","checkbox","III","")
    ->addModal("murmur_grade_4","checkbox","IV","")
    ->addModal("murmur_grade_5","checkbox","V","")
    ->addModal("murmur_grade_6","checkbox","VI","")
    
    ->addModal("", "label", "Penjalaran", "")
    ->addModal("murmur_penjalaran_axilla","checkbox","Axilla","")
    ->addModal("murmur_penjalaran_punggung","checkbox","Punggung","")
    ->addModal("murmur_penjalaran_ket","text","Lainnya","")
    
    ->addModal("murmur_opening_snap", "select", "Opneing Snap", $positif_negatif)
    ->addModal("murmur_friction_rub", "select", "Friction Rub", $positif_negatif)
    
    ->addModal("", "label", "<strong>Paru</strong>", "")
    ->addModal("paru_vesikuler","checkbox","Vesikuler","")
    ->addModal("paru_ronki","checkbox","Ronki","")
    ->addModal("paru_wheezing","checkbox","Wheezing","")
    ->addModal("paru_ket","text","Lainnya","")
    
    ->addModal("", "label", "<strong>HASIL PEMERIKSAAN PENUNJANG</strong>", "")
    ->addModal("ecg","textarea","ECG","")
    ->addModal("thorax_foto","textarea","THORAX FOTO","")
    ->addModal("echocardiografi","textarea","ECHOCARDIOGRAFI","")
    ->addModal("diagnosis","textarea","DIAGNOSIS","")
    ->addModal("terapi","textarea","TERAPI / TINDAKAN","")
    ->addModal("rencana_kerja","textarea","RENCANA KERJA","")
    
    ->addModal("", "label", "<strong>DISPOSISI</strong>", "")
    ->addModal("waktu_pulang","datetime","Tanggal Pulang","")
    ->addModal("waktu_kontrol_klinik","datetime","Tanggal Kontrol","")
    ->addModal("dirawat","text","Di Rawat","");
    
$ms ->getAdapter()
    ->setUseNumber(true,"No.","back.")
    ->add("Tanggal Assesmen","waktu","date d M Y H:i")
    ->add("Dokter","nama_dokter")
    ->add("No. Reg","noreg_pasien")
    ->add("NRM","nrm_pasien")
    ->add("id_dokter","id_dokter");
    
$ms ->setTableActionClass("AssesmentDokterJalanAction")
    ->setDateTimeEnable(true)
    ->setModalTitle("Assessment Poli Jantung")
    ->setModalComponentSize(Modal::$MEDIUM)
    ->setModalSize(Modal::$HALF_MODEL)
    ->setAutoReload(true)
    ->initialize();

?>