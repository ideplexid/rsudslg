<?php

global $db;
global $user;
require_once "smis-libs-class/MasterTemplate.php";
require_once "smis-libs-class/MasterSlaveTemplate.php";
require_once "medical_record/class/table/AssesmentDokterRJTable.php";

$id_dokter  =  getSettings($db,"mr-id-dokter-".$user->getUsername(),"");
$nm_dokter  =  getSettings($db,"mr-nama-dokter-".$user->getUsername(),"");

$header     = array("No.","Tanggal Assesmen","Dokter","No. Reg","NRM");
$uitable    = new AssesmentDokterRJTable($header);
$uitable ->setNoReg($_POST['noreg_pasien'])
         ->setDokter($id_dokter,$nm_dokter)
         ->setName("assessment_polianestesi")
         ->setDetailButtonEnable(true);
         
$ms = new MasterSlaveTemplate($db,"smis_mr_polianestesi","medical_record","assessment_polianestesi");
$ms ->getDBtable()
    ->setOrder(" waktu DESC ",true);

if($_POST['nrm_pasien'] != NULL || $_POST['nrm_pasien'] != '') {
    $ms ->getDBtable()
        ->addCustomKriteria(" nrm_pasien "," ='".$_POST['nrm_pasien']."' ");
}

$ms ->addJSColumn("id",true)
    ->addJSColumn("waktu",false)
    
    ->addJSColumn("pra_anestesi",false)
    ->addJSColumn("rencana_tindakan",false)
    ->addJSColumn("tanggal_rencana_tindakan",false)
    ->addJSColumn("tempat",false)
    ->addJSColumn("spesialis_bedah",false)
    ->addJSColumn("asisten_bedah",false)
    ->addJSColumn("spesialis_anestesiologi",false)
    ->addJSColumn("asisten_anestesi",false)
    
    ->addJSColumn("anamnesa_dari",false)
    ->addJSColumn("anamnesa_dari_ket",false)
    ->addJSColumn("riwayat_anestesi",false)
    ->addJSColumn("riwayat_anestesi_ket",false)
    ->addJSColumn("komplikasi",false)
    ->addJSColumn("komplikasi_ket",false)
    ->addJSColumn("obat_dikonsumsi",false)
    ->addJSColumn("riwayat_alergi",false)
    ->addJSColumn("riwayat_alergi_ket",false)
    
    ->addJSColumn("bb",false)
    ->addJSColumn("tb",false)
    ->addJSColumn("bmi",false)
    ->addJSColumn("td",false)
    ->addJSColumn("rr",false)
    ->addJSColumn("nadi",false)
    ->addJSColumn("suhu",false)
    ->addJSColumn("skor_nyeri",false)
    
    ->addJSColumn("jalan_nafas_bebas",false)
    ->addJSColumn("alat_bantu_nafas",false)
    ->addJSColumn("buka_mulut",false)
    ->addJSColumn("leher",false)
    ->addJSColumn("gerak_leher",false)
    ->addJSColumn("malampathy",false)
    ->addJSColumn("obesitas",false)
    ->addJSColumn("massa",false)
    
    ->addJSColumn("pernafasan",false)
    ->addJSColumn("kardiovaskuler",false)
    ->addJSColumn("neuro",false)
    ->addJSColumn("renal",false)
    ->addJSColumn("hepato",false)
    ->addJSColumn("merokok",false)
    ->addJSColumn("alkohol",false)
    ->addJSColumn("lain_lain",false)
    
    ->addJSColumn("hb_het",false)
    ->addJSColumn("fungsi_ginjal",false)
    ->addJSColumn("fungsi_hati",false)
    ->addJSColumn("serum_elektrolit",false)
    ->addJSColumn("faal_hemostatis_bt",false)
    ->addJSColumn("faal_hemostatis_ct",false)
    ->addJSColumn("faal_hemostatis_lain",false)
    
    ->addJSColumn("echocardiografi",false)
    ->addJSColumn("ekg",false)
    ->addJSColumn("foto_radiologi",false)
    ->addJSColumn("evaluasi_faal_paru",false)
    ->addJSColumn("penunjang_lain",false)
    
    ->addJSColumn("psa_asa",false)
    ->addJSColumn("penyulit",false)
    ->addJSColumn("komplikasi",false)
    ->addJSColumn("rencana_anestesi",false);
    
$ms ->setUITable($uitable);
$ms ->getUItable()
    ->addModal("id_dokter","hidden","",$id_dokter)
    ->addModal("nama_dokter","hidden","",$nm_dokter);
$ms ->addNoClear("nama_dokter")
    ->addNoClear("id_dokter");
$ms ->getForm();
$ms ->getUItable()
    ->clearContent();
    
// For Dropdown List
$ya_tidak = array(
    array("name"=>"","value"=>"","default"=> 1),
    array("name"=>"Ya","value"=>"Ya"),
    array("name"=>"Tidak","value"=>"Tidak"),
);
$anamnesa = array(
    array("name"=>"","value"=>"","default"=> 1),
    array("name"=>"Pasien","value"=>"Pasien"),
    array("name"=>"Keluarga","value"=>"Keluarga"),
    array("name"=>"Lainnya","value"=>"Lainnya"),
);
$leher = array(
    array("name"=>"","value"=>"","default"=> 1),
    array("name"=>"Pendek","value"=>"Pendek"),
    array("name"=>"Tidak","value"=>"Tidak"),
);
$gerak_leher = array(
    array("name"=>"","value"=>"","default"=> 1),
    array("name"=>"Bebas","value"=>"Bebas"),
    array("name"=>"Terbatas","value"=>"Terbatas"),
);

$ms ->getUItable()
    ->addModal("id","hidden","","" )
    ->addModal("waktu","datetime","Waktu",date("Y-m-d H:i:s") )
    
    ->addModal("pra_anestesi", "text", "Diagnosis Pra-Anestesi", "")
    ->addModal("rencana_tindakan", "text", "Rencana Tindakan", "")
    ->addModal("tanggal_rencana_tindakan", "date", "Tanggal", "")
    ->addModal("tempat", "text", "Tempat", "")
    ->addModal("spesialis_bedah", "text", "Tempat", "")
    ->addModal("asisten_bedah", "text", "Tempat", "")
    ->addModal("spesialis_anestesiologi", "text", "Tempat", "")
    ->addModal("asisten_anestesi", "text", "Tempat", "")
    
    ->addModal("", "label", "<strong>ASESMEN PRA-ANESTESI</strong>", "")
    ->addModal("anamnesa_dari", "select", "Anamnesa dari", $anamnesa)
    ->addModal("anamnesa_dari_ket", "text", "Lainnya", "")
    ->addModal("riwayat_anestesi", "select", "Riwayat Anestesi", $ya_tidak)
    ->addModal("riwayat_anestesi_ket", "text", "(Sebutkan jika ada)", "")
    ->addModal("komplikasi", "select", "Komplikasi", $ya_tidak)
    ->addModal("komplikasi_ket", "text", "(Sebutkan jika ada)", "")
    ->addModal("obat_dikonsumsi", "text", "Obat yang sedang dikonsumsi", "")
    ->addModal("riwayat_alergi", "select", "Riwayat Alergi", $ya_tidak)
    ->addModal("riwayat_alergi_ket", "text", "(Sebutkan jika ada)", "")
    
    ->addModal("bb", "text", "BB", "")
    ->addModal("tb", "text", "TB", "")
    ->addModal("bmi", "text", "BMI (k/p)", "")
    ->addModal("td", "text", "TD mmHg", "")
    ->addModal("rr", "text", "RR (x/mnt)", "")
    ->addModal("nadi", "text", "Nadi (x/mnt)", "")
    ->addModal("suhu", "text", "Suhu (*C)", "")
    ->addModal("skor_nyeri", "text", "Skor Nyeri (k/p)", "")
    
    ->addModal("", "label", "<strong>Evaluasi Jalur Nafas</strong>", "")
    ->addModal("jalan_nafas_bebas", "select", "Bebas", $ya_tidak)
    ->addModal("alat_bantu_nafas", "text", "Alat bantu nafas (jika ada)", "")
    ->addModal("buka_mulut", "text", "Buka mulut (cm)", "")
    ->addModal("leher", "select", "Leher", $leher)
    ->addModal("gerak_leher", "select", "Leher", $gerak_leher)
    ->addModal("malampathy", "text", "Malampathy (k/p)", "")
    ->addModal("obesitas", "select", "Obesitas", $ya_tidak)
    ->addModal("massa", "select", "Massa", $ya_tidak)
    
    ->addModal("", "label", "<strong>Fungsi Sistem Organ</strong>", "")
    ->addModal("pernafasan", "checkbox", "Pernafasan","")
    ->addModal("merokok", "select", "Merokok", $ya_tidak)
    ->addModal("kardiovaskuler", "checkbox", "Kardiovaskuler")
    ->addModal("alkohol", "select", "Alkohol", $ya_tidak)
    ->addModal("neuro", "checkbox", "Neuro/Muskuluskeletal","")
    ->addModal("renal", "checkbox", "Renal/Endokrin","")
    ->addModal("hepato", "checkbox", "Hepato/Gastrointestinal","")
    ->addModal("lain_lain", "checkbox", "Lain-lain","")
    
    ->addModal("", "label", "<strong>Pemeriksaan Laboratorium</strong>", "")
    ->addModal("hb_het", "textarea", "Hb/Het", "")
    ->addModal("fungsi_ginjal", "textarea", "Fungsi Ginjal", "")
    ->addModal("fungsi_hati", "textarea", "Fungsi Hati", "")
    ->addModal("serum_elektrolit", "textarea", "Serum Elektrolit", "")
    ->addModal("faal_hemostatis_bt", "textarea", "Faal Hemostasis <strong>BT<strong>", "")
    ->addModal("faal_hemostatis_ct", "textarea", "Faal Hemostasis <strong>CT<strong>", "")
    ->addModal("faal_hemostatis_lain", "textarea", "Lain-lain", "")
    
    ->addModal("", "label", "<strong>Pemeriksaan Penunjang</strong>", "")
    ->addModal("echocardiografi", "textarea", "Echocardiografi", "")
    ->addModal("ekg", "textarea", "EKG", "")
    ->addModal("foto_radiologi", "textarea", "Foto Radiologi", "")
    ->addModal("evaluasi_faal_paru", "textarea", "Evaluasi Faal Paru", "")
    ->addModal("penunjang_lain", "textarea", "Lain-lain", "")
    
    ->addModal("", "label", "<strong>SIMPULAN ASESMEN PRA-ANESTESI</strong>", "")
    ->addModal("psa_asa", "textarea", "PSA ASA", "")
    ->addModal("penyulit", "textarea", "Penyulit", "")
    ->addModal("komplikasi", "textarea", "Komplikasi", "")
    ->addModal("rencana_anestesi", "textarea", "Rencana Tindakan Anestesi (Kelayakan Anestesi)", "");
    
$ms ->getAdapter()
    ->setUseNumber(true,"No.","back.")
    ->add("Tanggal Assesmen","waktu","date d M Y H:i")
    ->add("Dokter","nama_dokter")
    ->add("No. Reg","noreg_pasien")
    ->add("NRM","nrm_pasien")
    ->add("id_dokter","id_dokter");
    
$ms ->setTableActionClass("AssesmentDokterJalanAction")
    ->setDateTimeEnable(true)
    ->setModalTitle("Assessment Poli Anestesi")
    ->setModalComponentSize(Modal::$MEDIUM)
    ->setModalSize(Modal::$HALF_MODEL)
    ->setAutoReload(true)
    ->initialize();


?>