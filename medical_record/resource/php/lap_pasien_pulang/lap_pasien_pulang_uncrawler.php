<?php

global $db;
global $user;

require_once 'smis-base/smis-include-service-consumer.php';

$header=array(	"No.","NRM","No.Profile","No.Reg","Nama","Ruang","Masuk","Pulang",
				"ICD10","Diagnosa","Dokter DPJP","Kasus Bedah","ICD9","Tindakan",
				"Tanggal Tindakan","Dokter Bedah","Dokter Anastesi");
$uitable=new Table($header);
$uitable->setName("lap_pasien_pulang_uncrawler")
		->setActionEnable(false)
		->setFooterVisible(false);
        
if (isset ( $_POST ['command'] )) {
    if(isset($_POST['dari']) && $_POST['dari']!="" && isset($_POST['sampai']) && $_POST['sampai']!="") {
        $qv = "
                SELECT 
                    a.nrm_pasien, 
                    a.profile_number, 
                    a.noreg_pasien, 
                    a.nama_pasien, 
                    a.ruangan, 
                    a.tgl_masuk, 
                    a.tgl_keluar,
                    a.kode_icd, 
                    a.nama_icd, 
                    a.nama_dokter, 
                    c.kasus,
                    c.icd_tindakan, 
                    c.nama_tindakan, 
                    c.tanggal_mulai, 
                    c.dokter_operator, 
                    c.dokter_anastesi
                FROM 
                    smis_mr_diagnosa a
                    LEFT JOIN 
                        smis_mr_diagnosa b 
                    ON 
                        (a.noreg_pasien = b.noreg_pasien AND a.id < b.id)
                    LEFT JOIN
                        smis_mr_operasi c
                    ON (a.noreg_pasien = c.noreg_pasien)
                WHERE 
                    b.id IS NULL
                    AND a.tgl_keluar >= '".$_POST['dari']."'
                    AND a.tgl_keluar <= '".$_POST['sampai']."'
            ";
        $qc = "SELECT count(*) FROM smis_mr_diagnosa";	
    }
    
    $dbtable = new DBTable ( $db, "smis_mr_diagnosa");
    $dbtable->setPreferredQuery(true, $qv, $qc);
    $dbtable->setUseWhereforView(false);
    $dbtable->setOrder(" tgl_keluar ASC");
    $dbtable->setShowAll(true);
    
    $adapter=new SimpleAdapter();
	$adapter->setUseNumber(true, "No.","back.");
	$adapter->add("NRM", "nrm_pasien");
	$adapter->add("No.Profile", "profile_number");
	$adapter->add("No.Reg", "noreg_pasien");
	$adapter->add("Nama", "nama_pasien");
	$adapter->add("Ruang", "ruangan","unslug");
	$adapter->add("Masuk", "tgl_masuk","date d M Y H:i");
	$adapter->add("Pulang", "tgl_keluar","date d M Y H:i");
	$adapter->add("ICD10", "kode_icd");
	$adapter->add("Diagnosa", "nama_icd");
	$adapter->add("Dokter DPJP", "nama_dokter");
	$adapter->add("Kasus Bedah", "kasus");
	$adapter->add("ICD9", "icd_tindakan");
	$adapter->add("Tindakan", "nama_tindakan");
	$adapter->add("Tanggal Tindakan", "tanggal_mulai","date d M Y");
	$adapter->add("Dokter Bedah", "dokter_operator");
	$adapter->add("Dokter Anastesi", "dokter_anastesi");
    
    require_once "medical_record/class/responder/LapPasienPulangUncrawlerResponder.php";
    $dbres = new LapPasienPulangUncrawlerResponder($dbtable, $uitable, $adapter);
	$hasil = $dbres->command($_POST['command']);
	echo json_encode($hasil);
	return;
}

$uitable->clearContent();
$uitable->addModal("dari", "datetime", "Dari", "")
		->addModal("sampai", "datetime", "Sampai", "");
        
$view=new Button("","","View");
$view->setClass("btn-primary")
		->setIsButton(Button::$ICONIC)
		->setIcon("fa fa-refresh")
		->setAction("lap_pasien_pulang_uncrawler.view()");
        
$excel=new Button("","","Excel");
$excel->setClass("btn-primary")
	->setIsButton(Button::$ICONIC)
	->setIcon("fa fa-file-excel-o")
	->setAction("lap_pasien_pulang_uncrawler.excel()");

$btng=new ButtonGroup("");
$btng->addButton($view);
$btng->addButton($excel);

$form=$uitable
	  ->getModal()
	  ->setTitle("Medical Record")
	  ->getForm()
	  ->addElement("",$btng);
      
echo "<h2>Laporan Pasien Pulang</h2>";
echo $form->getHtml();
echo $uitable->getHtml();

echo addJS("framework/bootstrap/js/bootstrap-datetimepicker.js");
echo addJS("framework/smis/js/table_action.js");
echo addCSS("framework/bootstrap/css/bootstrap-datetimepicker.css");

?>

<script>

var lap_pasien_pulang_uncrawler;

$(document).ready(function(){
	$('.mydatetime').datetimepicker({minuteStep:1});
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
	lap_pasien_pulang_uncrawler=new TableAction("lap_pasien_pulang_uncrawler","medical_record","lap_pasien_pulang_uncrawler",new Array());
	lap_pasien_pulang_uncrawler.getRegulerData=function(){
		var reg_data={	
				page:this.page,
				action:this.action,
				super_command:this.super_command,
				prototype_name:this.prototype_name,
				prototype_slug:this.prototype_slug,
				prototype_implement:this.prototype_implement,
				dari:$("#"+this.prefix+"_dari").val(),
				sampai:$("#"+this.prefix+"_sampai").val()
				};
		return reg_data;
	};
	
});

</script>