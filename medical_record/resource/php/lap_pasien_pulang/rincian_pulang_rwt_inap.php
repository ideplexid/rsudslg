<?php

global $db;
require_once "smis-base/smis-include-service-consumer.php";

$header = array("no", 
                "tanggal",
                "nrm",
                "noreg",
                "profile_number",
                "nama_pasien",
                "jk",
                "tanggal_inap",
                "tanggal_pulang",
                "carapulang",
                "jumlah_lama_dirawat"
                );
                
$uitable = new Table ( $header, "", NULL, false );
$uitable->setName ( "rincian_pulang_rwt_inap" );

$uitable->addHeader ( "before", "<tr>
                                    <th>NO</th>
                                    <th>TANGGAL</th>
                                    <th>NRM</th>
                                    <th>NO. REG</th>
                                    <th>NO. PROFILE</th>
                                    <th>NAMA PASIEN</th>
                                    <th>JENIS KELAMIN</th>
                                    <th>TANGGAL INAP</th>
                                    <th>TANGGAL PULANG</th>
                                    <th>CARAPULANG</th>
                                    <th>JUMLAH LAMA DIRAWAT</th>
                                </tr>" );
                                
$uitable->setHeaderVisible ( false );
$uitable->setFooterVisible( false );

if (isset ( $_POST ['command'] )) {
    /****** Get Pasien *********/
    $serv = new ServiceConsumer($db,"get_registered_all_by_pulang", NULL, "registration");
    $serv->addData("command", "list");
    $serv->addData("setshowall", "1");
    $serv->addData("dari", $_POST['dari']);
    $serv->addData("sampai", $_POST['sampai']);
    $serv->execute();
    $data = $serv->getContent();
    /******  *********/
    
    require_once "medical_record/class/adapter/RincianPulangRwtInapAdapter.php";
    $adapter = new RincianPulangRwtInapAdapter();
    $adapter->setDariSampai($_POST['dari'], $_POST['sampai']);
    $ready = $adapter->getContent($data['data']);
    
    if($_POST['command'] == 'list') {
        $uitable->setContent($ready);
        $content['list'] = $uitable->getBodyContent();
        $response = new ResponsePackage ();
        $response->setAlertVisible ( false );
        $response->setStatus ( ResponsePackage::$STATUS_OK );
        $response->setContent($content);
        echo json_encode ( $response->getPackage () );
        return;
                
        $dbres = new DBResponder ( $dbtable, $uitable, $adapter );
        $data = $dbres->command ( $_POST ['command'] );
        echo json_encode ( $data );
        return;
    } else if($_POST['command'] == 'excel') {
        require_once ("smis-libs-out/php-excel/PHPExcel.php");
        $file = new PHPExcel ();
        $file->getProperties ()->setCreator ( "Rumah Sakit" );
        $file->getProperties ()->setLastModifiedBy ( "Rumah Sakit Bagian Administrator" );
        $file->getProperties ()->setTitle ( "Rincian Pulang Rawat Inap" );
        $file->getProperties ()->setSubject ( "Rincian Pulang Rawat Inap" );
        $file->getProperties ()->setDescription ( "Rincian Pulang Rawat Inap Generated From system" );
        $file->getProperties ()->setKeywords ( "Rincian Pulang Rawat Inap" );
        $file->getProperties ()->setCategory ( "Rincian Pulang Rawat Inap" );
        
        $sheet = $file->getActiveSheet ();
        $i = 1;
        
        $objDrawing = new PHPExcel_Worksheet_Drawing();
        $objDrawing->setName('logo_img');
        $objDrawing->setDescription('logo_img');
        $objDrawing->setPath(getLogoNonInterlaced());
        $objDrawing->setCoordinates('A'.$i);
        $objDrawing->setOffsetX(20); 
        $objDrawing->setOffsetY(5);
        $objDrawing->setWidth(100); 
        $objDrawing->setHeight(100);
        $objDrawing->setWorksheet($sheet);
        $i++;
        
        $sheet->setCellValue("C".$i, getSettings($db,"smis_autonomous_title"));
        $sheet->getStyle("C".$i)->getFont()->setBold(true)->setSize(14);
        $i++;
        $sheet->setCellValue("C".$i, getSettings($db,"smis_autonomous_address"));
        $sheet->getStyle("C".$i)->getFont()->setBold(true)->setSize(14);
        $i++;
        $sheet->setCellValue("C".$i, getSettings($db,"smis_autonomous_contact"));
        $sheet->getStyle("C".$i)->getFont()->setBold(true)->setSize(14);
        $i += 3;
        $sheet->mergeCells("A".$i.":K".$i)->setCellValue("A".$i,"RINCIAN Pulang RAWAT INAP");
        $sheet->getStyle("A".$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle("A".$i)->getFont()->setBold(true)->setSize(14);
        $i++;
        $sheet->mergeCells("A".$i.":K".$i)->setCellValue("A".$i, strtoupper(ArrayAdapter::dateFormat("date d M Y", $_POST['dari']))." - ".strtoupper(ArrayAdapter::dateFormat("date d M Y", $_POST['sampai'])));
        $sheet->getStyle("A".$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle("A".$i)->getFont()->setBold(true);
        $i += 3;
        
        $border_start = $i;
        $sheet->setCellValue("A".$i, "NO");
        $sheet->setCellValue("B".$i, "TANGGAL");
        $sheet->setCellValue("C".$i, "NRM");
        $sheet->setCellValue("D".$i, "NO. REG");
        $sheet->setCellValue("E".$i, "NO. PROFILE");
        $sheet->setCellValue("F".$i, "NAMA PASIEN");
        $sheet->setCellValue("G".$i, "JENIS KELAMIN");
        $sheet->setCellValue("H".$i, "TANGGAL INAP");
        $sheet->setCellValue("I".$i, "TANGGAL PULANG");
        $sheet->setCellValue("J".$i, "CARAPULANG");
        $sheet->setCellValue("K".$i, "LAMA DIRAWAT");
        $sheet->getStyle("A".$i.":K".$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle("A".$i.":K".$i)->getFont()->setBold(true);
        $i++;
        
        foreach($ready as $x) {
            $sheet->setCellValue("A".$i, $x["no"]);
            $sheet->setCellValue("B".$i, $x["tanggal"]);
            $sheet->setCellValue("C".$i, $x["nrm"]);
            $sheet->setCellValue("D".$i, $x["noreg"]);
            $sheet->setCellValue("E".$i, $x["profile_number"]);
            $sheet->setCellValue("F".$i, $x["nama_pasien"]);
            $sheet->setCellValue("G".$i, $x["jk"]);
            $sheet->setCellValue("H".$i, $x["tanggal_inap"]);
            $sheet->setCellValue("I".$i, $x["tanggal_pulang"]);
            $sheet->setCellValue("J".$i, $x["carapulang"]);
            $sheet->setCellValue("K".$i, $x["jumlah_lama_dirawat"]);
            $sheet->getStyle("A".$i.":K".$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $i++;
        }
        
        $border_end = $i - 1;
        $thin = array ();
        $thin['borders']=array();
        $thin['borders']['allborders']=array();
        $thin['borders']['allborders']['style']=PHPExcel_Style_Border::BORDER_THIN ;
        $sheet  ->getStyle ( "A".$border_start.":K".$border_end )->applyFromArray ($thin);
        
        $filename = "Rincian Pulang Rawat Inap ".strtoupper(ArrayAdapter::dateFormat("date d M Y", $_POST['dari']))." - ".strtoupper(ArrayAdapter::dateFormat("date d M Y", $_POST['sampai'])).".xls";
        
        header ( 'Content-Type: application/vnd.ms-excel' );
        header ( 'Content-Disposition: attachment;filename="'.$filename.'"' );
        header ( 'Cache-Control: max-age=0' );
        $writer = PHPExcel_IOFactory::createWriter ( $file, 'Excel5' );
        $writer->save ( 'php://output' );
		return;
    }
}

$uitable->addModal( "dari", "date", "Dari", "" );
$uitable->addModal( "sampai", "date", "Sampai", "" );

$modal = $uitable->getModal ();
$form=$modal->getForm();

$v = new Button("", "", "");
$v->addClass("btn-primary")
		->setIsButton(Button::$ICONIC)
		->setIcon("fa fa-refresh")
		->setAction("rincian_pulang_rwt_inap.view()");
$form->addElement("", $v);

$excel = new Button("", "", "");
$excel->addClass("btn-primary")
		->setIsButton(Button::$ICONIC)
		->setIcon("fa fa-file-excel-o")
		->setAction("rincian_pulang_rwt_inap.excel()");
$form->addElement("", $excel);

echo "<h2><strong>Rincian Pulang Rawat Inap</h2>";
echo $form->getHtml();
echo $uitable->getHtml ();

echo addJS ( "framework/smis/js/table_action.js" );
echo addJS ( "framework/bootstrap/js/bootstrap-datepicker.js" );
echo addCSS ( "framework/bootstrap/css/datepicker.css" );

?>

<script>

var rincian_pulang_rwt_inap;

$(document).ready(function(){
	$('.mydate').datepicker();
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
	rincian_pulang_rwt_inap=new TableAction("rincian_pulang_rwt_inap","medical_record","rincian_pulang_rwt_inap",new Array());
	rincian_pulang_rwt_inap.getRegulerData=function(){
		var reg_data={	
				page:this.page,
				action:this.action,
				super_command:this.super_command,
				prototype_name:this.prototype_name,
				prototype_slug:this.prototype_slug,
				prototype_implement:this.prototype_implement,
				dari:$("#"+this.prefix+"_dari").val(),
				sampai:$("#"+this.prefix+"_sampai").val()
				};
		return reg_data;
	};
	
});

</script>