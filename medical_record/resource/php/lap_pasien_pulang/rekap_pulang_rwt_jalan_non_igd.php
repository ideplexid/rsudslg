<?php

global $db;
require_once "smis-base/smis-include-service-consumer.php";
$slug_igd = getSettings($db, "smis-mr-slug-igd", "");

$header = array("no", "ruangan", "baru_l", "baru_p","lama_l", "lama_p", "jumlah_l", "jumlah_p", "keterangan");

$uitable = new Table ( $header, "", NULL, false );
$uitable->setName ( "rekap_pulang_rwt_jalan_non_igd" );

$uitable->addHeader ( "before", "<tr>
                                    <th rowspan='2'>NO.</th>
                                    <th rowspan='2'>UPF / RUANGAN</th>
                                    <th colspan='2'>BARU</th>
                                    <th colspan='2'>LAMA</th>
                                    <th colspan='2'>JUMLAH</th>
                                    <th rowspan='2'>KETERANGAN</th>
                                </tr>" );
$uitable->addHeader ( "before", "<tr>
                                    <th>L</th>
                                    <th>P</th>
                                    <th>L</th>
                                    <th>P</th>
                                    <th>L</th>
                                    <th>P</th>
								</tr>" );

$uitable->setHeaderVisible ( false );
$uitable->setFooterVisible( false );

if (isset ( $_POST ['command'] )) {
    $serv = new ServiceConsumer($db,"get_lap_pulang_patient", NULL, "registration");
    $serv->addData("dari", $_POST['dari']);
    $serv->addData("sampai", $_POST['sampai']);
    $serv->addData("uri", "0");
    $serv->addData("slug_igd", $slug_igd);
    $serv->addData("igd", "not");
    $serv->execute();
    $data = $serv->getContent();
    
    $arr = array();
    foreach($data as $d => $key) {
        if($key['ruangan'] != 'hd_hemodialisa' && $key['ruangan'] != 'laboratory'  && $key['ruangan'] != 'radiology' && $key['ruangan'] != 'fisiotherapy' && $key['ruangan'] != 'elektromedis'  && $key['ruangan'] != 'bank_darah') {
             $arr[] = $key;
        }
    }
    
    require_once "medical_record/class/adapter/RekapPulangRawtJlnNonIGDAdapter.php";
    $adapter = new RekapPulangRawtJlnNonIGDAdapter();
    $ready = $adapter->getContent($arr);
    
    if($_POST['command'] == 'list') {
        $uitable->setContent($ready);
        $content['list'] = $uitable->getBodyContent();
        $response = new ResponsePackage ();
        $response->setAlertVisible ( false );
        $response->setStatus ( ResponsePackage::$STATUS_OK );
        $response->setContent($content);
        echo json_encode ( $response->getPackage () );
        return;
                
        $dbres = new DBResponder ( $dbtable, $uitable, $adapter );
        $data = $dbres->command ( $_POST ['command'] );
        echo json_encode ( $data );
        return;
    } else if($_POST['command'] == 'excel') {
        require_once ("smis-libs-out/php-excel/PHPExcel.php");
        $file = new PHPExcel ();
        $file->getProperties ()->setCreator ( "Rumah Sakit Syamrabu" );
        $file->getProperties ()->setLastModifiedBy ( "Rumah Sakit Bagian Administrator" );
        $file->getProperties ()->setTitle ( "Rekap Laporan Pulang Rawat Jalan Non IGD" );
        $file->getProperties ()->setSubject ( "Rekap Laporan Pulang Rawat Jalan Non IGD" );
        $file->getProperties ()->setDescription ( "Rekap Laporan Pulang Rawat Jalan Non IGD Generated From system" );
        $file->getProperties ()->setKeywords ( "Rekap Laporan Pulang Rawat Jalan Non IGD" );
        $file->getProperties ()->setCategory ( "Rekap Laporan Pulang Rawat Jalan Non IGD" );
        
        $sheet = $file->getActiveSheet ();
        $i = 1;
        
        $objDrawing = new PHPExcel_Worksheet_Drawing();
        $objDrawing->setName('logo_img');
        $objDrawing->setDescription('logo_img');
        $objDrawing->setPath(getLogoNonInterlaced());
        $objDrawing->setCoordinates('A'.$i);
        $objDrawing->setOffsetX(20); 
        $objDrawing->setOffsetY(5);
        $objDrawing->setWidth(100); 
        $objDrawing->setHeight(100);
        $objDrawing->setWorksheet($sheet);
        $i++;
        
        $sheet->setCellValue("C".$i, getSettings($db,"smis_autonomous_title"));
        $sheet->getStyle("C".$i)->getFont()->setBold(true)->setSize(14);
        $i++;
        $sheet->setCellValue("C".$i, getSettings($db,"smis_autonomous_address"));
        $sheet->getStyle("C".$i)->getFont()->setBold(true)->setSize(14);
        $i++;
        $sheet->setCellValue("C".$i, getSettings($db,"smis_autonomous_contact"));
        $sheet->getStyle("C".$i)->getFont()->setBold(true)->setSize(14);
        $i += 3;
        $sheet->mergeCells("A".$i.":I".$i)->setCellValue("A".$i,"REKAP PULANG RAWAT JALAN NON IGD");
        $sheet->getStyle("A".$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle("A".$i)->getFont()->setBold(true)->setSize(14);
        $i++;
        $sheet->mergeCells("A".$i.":I".$i)->setCellValue("A".$i, strtoupper(ArrayAdapter::dateFormat("date d M Y", $_POST['dari']))." - ".strtoupper(ArrayAdapter::dateFormat("date d M Y", $_POST['sampai'])));
        $sheet->getStyle("A".$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle("A".$i)->getFont()->setBold(true);
        $i += 3;
        
        $border_start = $i;
        $ips = $i + 1;
        $sheet->mergeCells("A".$i.":A".$ips)->setCellValue("A".$i, "NO.");
        $sheet->mergeCells("B".$i.":B".$ips)->setCellValue("B".$i, "UPF / RUANGAN");
        $sheet->mergeCells("C".$i.":D".$i)->setCellValue("C".$i, "BARU");
        $sheet->mergeCells("E".$i.":F".$i)->setCellValue("E".$i, "LAMA");
        $sheet->mergeCells("G".$i.":H".$i)->setCellValue("G".$i, "JUMLAH");
        $sheet->setCellValue("I".$i, "KETERANGAN");
        $sheet->getStyle("A".$i.":I".$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle("A".$i.":I".$i)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $sheet->getStyle("A".$i.":I".$i)->getFont()->setBold(true);
        $i++;
        
        $sheet->setCellValue("C".$i, "L");
        $sheet->setCellValue("D".$i, "P");
        $sheet->setCellValue("E".$i, "L");
        $sheet->setCellValue("F".$i, "P");
        $sheet->setCellValue("G".$i, "L");
        $sheet->setCellValue("H".$i, "P");
        $sheet->getStyle("A".$i.":I".$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle("A".$i.":I".$i)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $sheet->getStyle("A".$i.":I".$i)->getFont()->setBold(true);
        $i++;
        
        foreach($ready as $x) {
            $sheet->setCellValue("A".$i, $x["no"]);
            $sheet->getStyle("A".$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $sheet->setCellValue("B".$i, ArrayAdapter::format("unslug", $x["ruangan"]));
            $sheet->setCellValue("C".$i, $x["baru_l"]);
            $sheet->setCellValue("D".$i, $x["baru_p"]);
            $sheet->setCellValue("E".$i, $x["lama_l"]);
            $sheet->setCellValue("F".$i, $x["lama_p"]);
            $sheet->setCellValue("G".$i, $x["jumlah_l"]);
            $sheet->setCellValue("H".$i, $x["jumlah_p"]);
            $sheet->setCellValue("I".$i, $x["keterangan"]);
            $sheet->getStyle("C".$i.":I".$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $i++;
        }
        
        $border_end = $i - 1;
        $thin = array ();
        $thin['borders']=array();
        $thin['borders']['allborders']=array();
        $thin['borders']['allborders']['style']=PHPExcel_Style_Border::BORDER_THIN ;
        $sheet  ->getStyle ( "A".$border_start.":I".$border_end )->applyFromArray ($thin);
        
        $filename = "Rekap Pulang Rawat Jalan Non IGD ".strtoupper(ArrayAdapter::dateFormat("date d M Y", $_POST['dari']))." - ".strtoupper(ArrayAdapter::dateFormat("date d M Y", $_POST['sampai'])).".xls";
        
        header ( 'Content-Type: application/vnd.ms-excel' );
        header ( 'Content-Disposition: attachment;filename="'.$filename.'"' );
        header ( 'Cache-Control: max-age=0' );
        $writer = PHPExcel_IOFactory::createWriter ( $file, 'Excel5' );
        $writer->save ( 'php://output' );
		return;
    }
}

$uitable->addModal( "dari", "date", "Dari", "" );
$uitable->addModal( "sampai", "date", "Sampai", "" );

$modal = $uitable->getModal ();
$form=$modal->getForm();

$v = new Button("", "", "");
$v->addClass("btn-primary")
		->setIsButton(Button::$ICONIC)
		->setIcon("fa fa-refresh")
		->setAction("rekap_pulang_rwt_jalan_non_igd.view()");
$form->addElement("", $v);

$excel = new Button("", "", "");
$excel->addClass("btn-primary")
		->setIsButton(Button::$ICONIC)
		->setIcon("fa fa-file-excel-o")
		->setAction("rekap_pulang_rwt_jalan_non_igd.excel()");
$form->addElement("", $excel);

echo "<h2><strong>Rekap Pulang Rawat Jalan Non IGD</h2>";
echo $form->getHtml();
echo $uitable->getHtml ();

echo addJS ( "framework/smis/js/table_action.js" );
echo addJS ( "framework/bootstrap/js/bootstrap-datepicker.js" );
echo addCSS ( "framework/bootstrap/css/datepicker.css" );

?>

<script>

var rekap_pulang_rwt_jalan_non_igd;

$(document).ready(function(){
	$('.mydate').datepicker();
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
	rekap_pulang_rwt_jalan_non_igd=new TableAction("rekap_pulang_rwt_jalan_non_igd","medical_record","rekap_pulang_rwt_jalan_non_igd",new Array());
	rekap_pulang_rwt_jalan_non_igd.getRegulerData=function(){
		var reg_data={	
				page:this.page,
				action:this.action,
				super_command:this.super_command,
				prototype_name:this.prototype_name,
				prototype_slug:this.prototype_slug,
				prototype_implement:this.prototype_implement,
				dari:$("#"+this.prefix+"_dari").val(),
				sampai:$("#"+this.prefix+"_sampai").val()
				};
		return reg_data;
	};
	
});

</script>