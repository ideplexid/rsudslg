<?php

global $db;
require_once "smis-base/smis-include-service-consumer.php";
$slug_igd = getSettings($db, "smis-mr-slug-igd", "");

$header = array("no", 
                "tanggal", 
                "nrm", 
                "noreg", 
                "profile_number", 
                "nama_pasien", 
                "ruangan", 
                "baru_l", 
                "baru_p",
                "lama_l", 
                "lama_p", 
                "keterangan");
                
$uitable = new Table ( $header, "", NULL, false );
$uitable->setName ( "rincian_pulang_rwt_jalan_non_igd" );

$uitable->addHeader ( "before", "<tr>
                                    <th rowspan='2'>NO.</th>
                                    <th rowspan='2'>TANGGAL</th>
                                    <th rowspan='2'>NRM</th>
                                    <th rowspan='2'>NO. REG</th>
                                    <th rowspan='2'>NO. PROFILE</th>
                                    <th rowspan='2'>NAMA</th>
                                    <th rowspan='2'>UPF / RUANGAN</th>
                                    <th colspan='2'>BARU</th>
                                    <th colspan='2'>LAMA</th>
                                    <th rowspan='2'>KETERANGAN</th>
                                </tr>" );
                                
$uitable->addHeader ( "before", "<tr>
                                    <th>L</th>
                                    <th>P</th>
                                    <th>L</th>
                                    <th>P</th>
								</tr>" );
                                
$uitable->setHeaderVisible ( false );
$uitable->setFooterVisible( false );

if (isset ( $_POST ['command'] )) {
    /****** Get Pasien *********/
    $serv = new ServiceConsumer($db,"get_registered_all_by_pulang", NULL, "registration");
    $serv->addData("command", "list");
    $serv->addData("setshowall", "1");
    $serv->addData("dari", $_POST['dari']);
    $serv->addData("sampai", $_POST['sampai']);
    $serv->execute();
    $data = $serv->getContent();
    /******  *********/
    
    require_once "medical_record/class/adapter/RincianPulangRwtJalanNonIGDAdapter.php";
    $adapter = new RincianPulangRwtJalanNonIGDAdapter();
    $adapter->setSlugIGD($slug_igd);
    $ready = $adapter->getContent($data['data']);
    
    if($_POST['command'] == 'list') {
        $uitable->setContent($ready);
        $content['list'] = $uitable->getBodyContent();
        $response = new ResponsePackage ();
        $response->setAlertVisible ( false );
        $response->setStatus ( ResponsePackage::$STATUS_OK );
        $response->setContent($content);
        echo json_encode ( $response->getPackage () );
        return;
                
        $dbres = new DBResponder ( $dbtable, $uitable, $adapter );
        $data = $dbres->command ( $_POST ['command'] );
        echo json_encode ( $data );
        return;
    } else if($_POST['command'] == 'excel') {
        require_once ("smis-libs-out/php-excel/PHPExcel.php");
        $file = new PHPExcel ();
        $file->getProperties ()->setCreator ( "Rumah Sakit Syamrabu" );
        $file->getProperties ()->setLastModifiedBy ( "Rumah Sakit Bagian Administrator" );
        $file->getProperties ()->setTitle ( "Rincian Laporan Kunjungan Rawat Jalan Non IGD" );
        $file->getProperties ()->setSubject ( "Rincian Laporan Kunjungan Rawat Jalan Non IGD" );
        $file->getProperties ()->setDescription ( "Rincian Laporan Kunjungan Rawat Jalan Non IGD Generated From system" );
        $file->getProperties ()->setKeywords ( "Rincian Laporan Kunjungan Rawat Jalan Non IGD" );
        $file->getProperties ()->setCategory ( "Rincian Laporan Kunjungan Rawat Jalan Non IGD" );
        
        $sheet = $file->getActiveSheet ();
        $i = 1;
        
        $objDrawing = new PHPExcel_Worksheet_Drawing();
        $objDrawing->setName('logo_img');
        $objDrawing->setDescription('logo_img');
        $objDrawing->setPath(getLogoNonInterlaced());
        $objDrawing->setCoordinates('A'.$i);
        $objDrawing->setOffsetX(20); 
        $objDrawing->setOffsetY(5);
        $objDrawing->setWidth(100); 
        $objDrawing->setHeight(100);
        $objDrawing->setWorksheet($sheet);
        $i++;
        
        $sheet->setCellValue("C".$i, getSettings($db,"smis_autonomous_title"));
        $sheet->getStyle("C".$i)->getFont()->setBold(true)->setSize(14);
        $i++;
        $sheet->setCellValue("C".$i, getSettings($db,"smis_autonomous_address"));
        $sheet->getStyle("C".$i)->getFont()->setBold(true)->setSize(14);
        $i++;
        $sheet->setCellValue("C".$i, getSettings($db,"smis_autonomous_contact"));
        $sheet->getStyle("C".$i)->getFont()->setBold(true)->setSize(14);
        $i += 3;
        $sheet->mergeCells("A".$i.":L".$i)->setCellValue("A".$i,"RINCIAN PULANG RAWAT JALAN NON IGD");
        $sheet->getStyle("A".$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle("A".$i)->getFont()->setBold(true)->setSize(14);
        $i++;
        $sheet->mergeCells("A".$i.":L".$i)->setCellValue("A".$i, strtoupper(ArrayAdapter::dateFormat("date d M Y", $_POST['dari']))." - ".strtoupper(ArrayAdapter::dateFormat("date d M Y", $_POST['sampai'])));
        $sheet->getStyle("A".$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle("A".$i)->getFont()->setBold(true);
        $i += 3;
        
        $border_start = $i;
        $ips = $i + 1;
        $sheet->mergeCells("A".$i.":A".$ips)->setCellValue("A".$i, "NO.");
        $sheet->mergeCells("B".$i.":B".$ips)->setCellValue("B".$i, "TANGGAL");
        $sheet->mergeCells("C".$i.":C".$ips)->setCellValue("C".$i, "NRM");
        $sheet->mergeCells("D".$i.":D".$ips)->setCellValue("D".$i, "NO. REG");
        $sheet->mergeCells("E".$i.":E".$ips)->setCellValue("E".$i, "NO. PROFILE");
        $sheet->mergeCells("F".$i.":F".$ips)->setCellValue("F".$i, "NAMA");
        $sheet->mergeCells("G".$i.":G".$ips)->setCellValue("G".$i, "UPF / RUANGAN");
        $sheet->mergeCells("H".$i.":I".$i)->setCellValue("H".$i, "BARU");
        $sheet->mergeCells("J".$i.":K".$i)->setCellValue("J".$i, "LAMA");
        $sheet->mergeCells("L".$i.":L".$ips)->setCellValue("L".$i, "KETERANGAN");
        $sheet->getStyle("A".$i.":L".$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle("A".$i.":L".$i)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $sheet->getStyle("A".$i.":L".$i)->getFont()->setBold(true);
        $i++;
        
        $sheet->setCellValue("H".$i, "L");
        $sheet->setCellValue("I".$i, "P");
        $sheet->setCellValue("J".$i, "L");
        $sheet->setCellValue("K".$i, "P");
        $sheet->getStyle("A".$i.":L".$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle("A".$i.":L".$i)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $sheet->getStyle("A".$i.":L".$i)->getFont()->setBold(true);
        $i++;
        
        foreach($ready as $x) {
            $sheet->setCellValue("A".$i, $x["no"]);
            $sheet->getStyle("A".$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $sheet->setCellValue("B".$i, $x["tanggal"]);
            $sheet->setCellValue("C".$i, $x["nrm"]);
            $sheet->setCellValue("D".$i, $x["noreg"]);
            $sheet->setCellValue("E".$i, $x["profile_number"]);
            $sheet->getStyle("B".$i.":E".$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $sheet->setCellValue("F".$i, $x["nama_pasien"]);
            $sheet->setCellValue("G".$i, ArrayAdapter::format("unslug", $x["ruangan"]));
            $sheet->setCellValue("H".$i, $x["baru_l"]);
            $sheet->setCellValue("I".$i, $x["baru_p"]);
            $sheet->setCellValue("J".$i, $x["lama_l"]);
            $sheet->setCellValue("K".$i, $x["lama_p"]);
            $sheet->setCellValue("L".$i, $x["keterangan"]);
            $sheet->getStyle("H".$i.":L".$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $i++;
        }
        
        $border_end = $i - 1;
        $thin = array ();
        $thin['borders']=array();
        $thin['borders']['allborders']=array();
        $thin['borders']['allborders']['style']=PHPExcel_Style_Border::BORDER_THIN ;
        $sheet  ->getStyle ( "A".$border_start.":L".$border_end )->applyFromArray ($thin);
        
        $filename = "Rincian Pulang Rawat Jalan Non IGD ".strtoupper(ArrayAdapter::dateFormat("date d M Y", $_POST['dari']))." - ".strtoupper(ArrayAdapter::dateFormat("date d M Y", $_POST['sampai'])).".xls";
        
        header ( 'Content-Type: application/vnd.ms-excel' );
        header ( 'Content-Disposition: attachment;filename="'.$filename.'"' );
        header ( 'Cache-Control: max-age=0' );
        $writer = PHPExcel_IOFactory::createWriter ( $file, 'Excel5' );
        $writer->save ( 'php://output' );
		return;
    }
}

$uitable->addModal( "dari", "date", "Dari", "" );
$uitable->addModal( "sampai", "date", "Sampai", "" );

$modal = $uitable->getModal ();
$form=$modal->getForm();

$v = new Button("", "", "");
$v->addClass("btn-primary")
		->setIsButton(Button::$ICONIC)
		->setIcon("fa fa-refresh")
		->setAction("rincian_pulang_rwt_jalan_non_igd.view()");
$form->addElement("", $v);

$excel = new Button("", "", "");
$excel->addClass("btn-primary")
		->setIsButton(Button::$ICONIC)
		->setIcon("fa fa-file-excel-o")
		->setAction("rincian_pulang_rwt_jalan_non_igd.excel()");
$form->addElement("", $excel);

echo "<h2><strong>Rincian Pulang Rawat Jalan Non IGD</h2>";
echo $form->getHtml();
echo $uitable->getHtml ();

echo addJS ( "framework/smis/js/table_action.js" );
echo addJS ( "framework/bootstrap/js/bootstrap-datepicker.js" );
echo addCSS ( "framework/bootstrap/css/datepicker.css" );

?>

<script>

var rincian_pulang_rwt_jalan_non_igd;

$(document).ready(function(){
	$('.mydate').datepicker();
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
	rincian_pulang_rwt_jalan_non_igd=new TableAction("rincian_pulang_rwt_jalan_non_igd","medical_record","rincian_pulang_rwt_jalan_non_igd",new Array());
	rincian_pulang_rwt_jalan_non_igd.getRegulerData=function(){
		var reg_data={	
				page:this.page,
				action:this.action,
				super_command:this.super_command,
				prototype_name:this.prototype_name,
				prototype_slug:this.prototype_slug,
				prototype_implement:this.prototype_implement,
				dari:$("#"+this.prefix+"_dari").val(),
				sampai:$("#"+this.prefix+"_sampai").val()
				};
		return reg_data;
	};
	
});

</script>