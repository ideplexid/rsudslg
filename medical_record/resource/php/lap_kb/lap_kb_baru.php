<?php
setChangeCookie ( false );
$header = array ();
$header [] = "No";
$header [] = "Met";
$header [] = "IC";
$header [] = "Klinik";
$header [] = "Dokter";
$header [] = "Bidan";
$header [] = "Total";
$header [] = "Praski";

$uitable = new Table ( $header, "", NULL, false );
$uitable->setName ( "lap_kb_baru" );
$uitable->setFooterVisible ( false );
$uitable->addHeader ( "before", "<tr>
								<th rowspan='2'>No</th>
								<th rowspan='2'>Metode Kontrasepsi</th>
								<th rowspan='2'>Jumlah Pemberian Informed Consent</th>
								<th colspan='4'>Jumlah Peserta KB Baru Yang Dilayani Oleh</th>
								<th rowspan='2'>Jumlah Peserta KB Baru Keluarga Pra S dan KS I</th>
							</tr>" );
$uitable->addHeader ( "before", "<tr>
								<th>KLINIK KB</th>
								<th>DOKTER PRAKTEK SWASTA</th>
								<th>BIDAN PRAKTEK SWASTA</th>
								<th>TOTAL</th>			
								</tr>" );
$uitable->setHeaderVisible ( false );
class KBBaru extends ArrayAdapter {
	private $iud;
	private $mow;
	private $mop;
	private $kondom;
	private $implant;
	private $suntikan;
	private $pil;
	private $jumlah;
	public function __construct() {
		$this->iud = array (
				"No" => 1,
				"Met" => "IUD",
				"IC" => 0,
				"Klinik" => 0,
				"Dokter" => 0,
				"Bidan" => 0,
				"Total" => 0,
				"Praski" => 0 
		);
		$this->mow = array (
				"No" => 2,
				"Met" => "MOW",
				"IC" => 0,
				"Klinik" => 0,
				"Dokter" => 0,
				"Bidan" => 0,
				"Total" => 0,
				"Praski" => 0 
		);
		$this->mop = array (
				"No" => 3,
				"Met" => "MOP",
				"IC" => 0,
				"Klinik" => 0,
				"Dokter" => 0,
				"Bidan" => 0,
				"Total" => 0,
				"Praski" => 0 
		);
		$this->kondom = array (
				"No" => 4,
				"Met" => "Kondom",
				"IC" => 0,
				"Klinik" => 0,
				"Dokter" => 0,
				"Bidan" => 0,
				"Total" => 0,
				"Praski" => 0 
		);
		$this->implant = array (
				"No" => 5,
				"Met" => "Implant",
				"IC" => 0,
				"Klinik" => 0,
				"Dokter" => 0,
				"Bidan" => 0,
				"Total" => 0,
				"Praski" => 0 
		);
		$this->suntikan = array (
				"No" => 6,
				"Met" => "Suntikan",
				"IC" => 0,
				"Klinik" => 0,
				"Dokter" => 0,
				"Bidan" => 0,
				"Total" => 0,
				"Praski" => 0 
		);
		$this->pil = array (
				"No" => 7,
				"Met" => "PIL",
				"IC" => 0,
				"Klinik" => 0,
				"Dokter" => 0,
				"Bidan" => 0,
				"Total" => 0,
				"Praski" => 0 
		);
		$this->jumlah = array (
				"No" => 8,
				"Met" => "Jumlah",
				"IC" => 0,
				"Klinik" => 0,
				"Dokter" => 0,
				"Bidan" => 0,
				"Total" => 0,
				"Praski" => 0 
		);
	}
	public function adapt($d) {
		$cur_array = null;
		if ($d->metode == "IUD") {
			$cur_array = &$this->iud;
		} else if ($d->metode == "MOW") {
			$cur_array = &$this->mow;
		} else if ($d->metode == "MOP") {
			$cur_array = &$this->mop;
		} else if ($d->metode == "Kondom") {
			$cur_array = &$this->kondom;
		} else if ($d->metode == "Implant") {
			$cur_array = &$this->implant;
		} else if ($d->metode == "Suntikan") {
			$cur_array = &$this->suntikan;
		} else if ($d->metode == "PIL") {
			$cur_array = &$this->pil;
		}
		
		if ($d->informed == "1") {
			$cur_array ['IC'] ++;
			$this->jumlah ['IC'] ++;
		}
		if ($d->petugas == "Klinik") {
			$cur_array ['Klinik'] ++;
			$this->jumlah ['Klinik'] ++;
		}
		if ($d->petugas == "Dokter") {
			$cur_array ['Dokter'] ++;
			$this->jumlah ['Dokter'] ++;
		}
		if ($d->petugas == "Bidan") {
			$cur_array ['Bidan'] ++;
			$this->jumlah ['Bidan'] ++;
		}
		if ($d->praski == "1") {
			$cur_array ['Praski'] ++;
			$this->jumlah ['Praski'] ++;
		}
		$cur_array ['Total'] ++;
		$this->jumlah ['Total'] ++;
		return null;
	}
	public function getContent($data) {
		parent::getContent ( $data );
		
		$result = array ();
		$result [] = $this->iud;
		$result [] = $this->mow;
		$result [] = $this->mop;
		$result [] = $this->kondom;
		$result [] = $this->implant;
		$result [] = $this->suntikan;
		$result [] = $this->pil;
		$result [] = $this->jumlah;
		$final = $this->removeZero ( $result );
		return $final;
	}
	public function removeZero($gdata) {
		$hasil = array ();
		foreach ( $gdata as $onedata ) {
			$one_hasil = array ();
			foreach ( $onedata as $name => $value ) {
				if (is_string ( $value ) || $value > 0) {
					$one_hasil [$name] = $value;
				} else {
					$one_hasil [$name] = "";
				}
			}
			$hasil [] = $one_hasil;
		}
		return $hasil;
	}
}

if (isset ( $_POST ['command'] )) {
	$adapter = new KBBaru ();
	$dbtable = new DBTable ( $db, "smis_mr_kb" );
	if (isset ( $_POST ['dari'] ) && isset ( $_POST ['sampai'] ) && $_POST ['dari'] != "" && $_POST ['sampai'] != "") {
		$dari = $_POST ['dari'];
		$sampai = $_POST ['sampai'];
		$dbtable->addCustomKriteria ( "'" . $dari . "'", "<=tanggal" );
		$dbtable->addCustomKriteria ( "'" . $sampai . "'", ">=tanggal" );
		$dbtable->setShowAll ( true );
	}
    if(isset ( $_POST ['origin'] ) && $_POST ['origin'] != "") {
        $dbtable->addCustomKriteria ( " origin ", "='".$_POST['origin']."'" );
    }
	$dbtable->addCustomKriteria ( "pelayanan", "='KB Baru'" );
	$dbtable->setOrder ( " tanggal ASC " );
	require_once "medical_record/class/responder/KBResponder.php";
	$dbres = new KBResponder ( $dbtable, $uitable, $adapter );
	$data = $dbres->command ( $_POST ['command'] );
	echo json_encode ( $data );
	return;
}
$query="SELECT DISTINCT origin FROM smis_mr_diagnosa WHERE prop!='del' ";
$option=new OptionBuilder();
$option->add(" - Semua - ","",1);

$res=$db->get_result($query);
foreach($res as $x){
    if($x->origin==""){
        continue;
    }
    $option->add(ArrayAdapter::slugFormat("unslug",$x->origin),$x->origin);
}

$uitable->addModal ( "dari", "date", "Dari", "" );
$uitable->addModal ( "sampai", "date", "Sampai", "" );
$uitable->addModal ( "ruangan", "hidden", "", "igd" );
$uitable->addModal( "origin", "select", "Asal", $option->getContent() );

$modal = $uitable->getModal ();
$form = $modal->getForm ();

$button = new Button ( "", "", "" );
$button->setIsButton ( Button::$ICONIC );
$button->setIcon ( "icon-white " . BUtton::$icon_refresh );
$button->setAction ( "lap_kb_baru.view()" );
$button->setClass("btn-primary");
$form->addElement ( "", $button );

$button = new Button ( "", "", "" );
$button->setIsButton ( Button::$ICONIC );
$button->setIcon ( "icon-white " . BUtton::$icon_print );
$button->setAction ( "smis_print($('#print_table_lap_kb_baru').html())" );
$button->setClass("btn-primary");
$form->addElement ( "", $button );

$excel = new Button ( "", "", "Download" );
$excel->setIsButton ( Button::$ICONIC_TEXT );
$excel->setIcon ( "fa fa-file-excel-o " );
$excel->setAction ( "lap_kb_baru.excel()" );
$excel->setClass("btn-primary");
$form->addElement ( "", $excel );


echo $form->getHtml ();

echo $uitable->getHtml ();
echo addJS ( "framework/smis/js/table_action.js" );
echo addJS ( "framework/bootstrap/js/bootstrap-datepicker.js" );
echo addCSS ( "framework/bootstrap/css/datepicker.css" );

?>
<script type="text/javascript">
var lap_kb_baru;
//var employee;
$(document).ready(function(){
	$('.mydate').datepicker();
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
	var column=new Array('id','nama','keterangan','slug');
	lap_kb_baru=new TableAction("lap_kb_baru","medical_record","lap_kb_baru",column);
	lap_kb_baru.getRegulerData=function(){
		var reg_data={	
				page:this.page,
				action:this.action,
				super_command:this.super_command,
				prototype_name:this.prototype_name,
				prototype_slug:this.prototype_slug,
				prototype_implement:this.prototype_implement,
				dari:$("#"+this.prefix+"_dari").val(),
				sampai:$("#"+this.prefix+"_sampai").val(),
				origin:$("#"+this.prefix+"_origin").val()
				};
		return reg_data;
	};
	lap_kb_baru.view();
	
});
</script>

<style type="text/css">
#table_lap_kb_baru {
	font-size: 12px;
}
</style>