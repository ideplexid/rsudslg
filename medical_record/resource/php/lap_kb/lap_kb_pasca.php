<?php
setChangeCookie ( false );
$header = array ();
$header [] = "No";
$header [] = "Pasca";
$header [] = "Jumlah Pasca";
$header [] = "KB Pasca";
$header [] = "IUD";
$header [] = "MOW";
$header [] = "MOP";
$header [] = "Implant";
$header [] = "Suntik";
$header [] = "PIL";
$header [] = "Kondom";
$header [] = "Persentase";

$uitable = new Table ( $header, "", NULL, false );
$uitable->setName ( "lap_kb_pasca" );
$uitable->setFooterVisible ( false );

/*
 * $uitable->addHeader("before", "<tr> <th rowspan='2'>No</th> <th rowspan='2'>Metode Kontrasepsi</th> <th rowspan='2'>Jumlah Pemberian Informed Consent</th> <th colspan='4'>Jumlah Peserta KB Baru Yang Dilayani Oleh</th> <th rowspan='2'>Jumlah Peserta KB Baru Keluarga Pra S dan KS I</th> </tr>"); $uitable->addHeader("before", "<tr> <th>KLINIK KB</th> <th>DOKTER PRAKTEK SWASTA</th> <th>BIDAN PRAKTEK SWASTA</th> <th>TOTAL</th> </tr>"); $uitable->setHeaderVisible(false);
 */
class KBPasca extends ArrayAdapter {
	private $persalinan;
	private $keguguran;
	public function __construct() {
		$this->persalinan = array (
				"No" => 1,
				"Pasca" => "Persalinan",
				"Jumlah Pasca" => 0,
				"KB Pasca" => 0,
				"IUD" => 0,
				"MOP" => 0,
				"MOW" => 0,
				"Implant" => 0,
				"Suntik" => 0,
				"PIL" => 0,
				"Kondom" => 0,
				"Persen" => 0 
		);
		$this->keguguran = array (
				"No" => 2,
				"Pasca" => "Keguguran",
				"Jumlah Pasca" => 0,
				"KB Pasca" => 0,
				"IUD" => 0,
				"MOP" => 0,
				"MOW" => 0,
				"Implant" => 0,
				"Suntik" => 0,
				"PIL" => 0,
				"Kondom" => 0,
				"Persen" => 0 
		);
	}
	public function adapt($d) {
		$cur_array = null;
		if ($d->pasca == "Persalinan")
			$cur_array = &$this->persalinan;
		else
			$cur_array = &$this->keguguran;
		$cur_array ['Jumlah Pasca'] ++;
		if ($d->pelayanan != "Non") {
			$cur_array ['KB Pasca'] ++;
			$cur_array [$d->metode] ++;
		}
		$cur_array ['Persentase'] = ($cur_array ['KB Pasca'] * 100 / $cur_array ['Jumlah Pasca']) . " %";
		return null;
	}
	public function getContent($data) {
		parent::getContent ( $data );
		$result = array ();
		$result [] = $this->persalinan;
		$result [] = $this->keguguran;
		$final = $this->removeZero ( $result );
		return $final;
	}
	public function removeZero($gdata) {
		$hasil = array ();
		foreach ( $gdata as $onedata ) {
			$one_hasil = array ();
			foreach ( $onedata as $name => $value ) {
				if (is_string ( $value ) || $value > 0) {
					$one_hasil [$name] = $value;
				} else {
					$one_hasil [$name] = "";
				}
			}
			$hasil [] = $one_hasil;
		}
		return $hasil;
	}
}

if (isset ( $_POST ['command'] )) {
	$adapter = new KBPasca ();
	$dbtable = new DBTable ( $db, "smis_mr_kb" );
	if (isset ( $_POST ['dari'] ) && isset ( $_POST ['sampai'] ) && $_POST ['dari'] != "" && $_POST ['sampai'] != "") {
		$dari = $_POST ['dari'];
		$sampai = $_POST ['sampai'];
		$dbtable->addCustomKriteria ( "'" . $dari . "'", "<=tanggal" );
		$dbtable->addCustomKriteria ( "'" . $sampai . "'", ">=tanggal" );
		$dbtable->setShowAll ( true );
	}
    if(isset ( $_POST ['origin'] ) && $_POST ['origin'] != "") {
        $dbtable->addCustomKriteria ( " origin ", "='".$_POST['origin']."'" );
    }
	$dbtable->addCustomKriteria ( "pasca", "!='Normal'" );
	$dbtable->setOrder ( " tanggal ASC " );
	$dbres = new DBResponder ( $dbtable, $uitable, $adapter );
	$data = $dbres->command ( $_POST ['command'] );
	echo json_encode ( $data );
	return;
}

$query="SELECT DISTINCT origin FROM smis_mr_diagnosa WHERE prop!='del' ";
$option=new OptionBuilder();
$option->add(" - Semua - ","",1);

$res=$db->get_result($query);
foreach($res as $x){
    if($x->origin==""){
        continue;
    }
    $option->add(ArrayAdapter::slugFormat("unslug",$x->origin),$x->origin);
}

$uitable->addModal ( "dari", "date", "Dari", "" );
$uitable->addModal ( "sampai", "date", "Sampai", "" );
$uitable->addModal ( "ruangan", "hidden", "", "igd" );
$uitable->addModal( "origin", "select", "Asal", $option->getContent() );

$modal = $uitable->getModal ();
$form = $modal->getForm ();

$button = new Button ( "", "", "" );
$button->setIsButton ( Button::$ICONIC );
$button->setIcon ( "icon-white " . BUtton::$icon_refresh );
$button->setAction ( "lap_kb_pasca.view()" );
$button->setClass("btn-primary");

$form->addElement ( "", $button );
$button = new Button ( "", "", "" );
$button->setIsButton ( Button::$ICONIC );
$button->setIcon ( "icon-white " . BUtton::$icon_print );
$button->setAction ( "smis_print($('#print_table_lap_kb_pasca').html())" );
$button->setClass("btn-primary");

$form->addElement ( "", $button );
echo $form->getHtml ();

echo $uitable->getHtml ();
echo addJS ( "framework/smis/js/table_action.js" );
echo addJS ( "framework/bootstrap/js/bootstrap-datepicker.js" );
echo addCSS ( "framework/bootstrap/css/datepicker.css" );

?>
<script type="text/javascript">
var lap_kb_pasca;
//var employee;
$(document).ready(function(){
	$('.mydate').datepicker();
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
	var column=new Array('id','nama','keterangan','slug');
	lap_kb_pasca=new TableAction("lap_kb_pasca","medical_record","lap_kb_pasca",column);
	lap_kb_pasca.getRegulerData=function(){
		var reg_data={	
				page:this.page,
				action:this.action,
				super_command:this.super_command,
				prototype_name:this.prototype_name,
				prototype_slug:this.prototype_slug,
				prototype_implement:this.prototype_implement,
				dari:$("#"+this.prefix+"_dari").val(),
				sampai:$("#"+this.prefix+"_sampai").val(),
                origin:$("#"+this.prefix+"_origin").val()
				};
		return reg_data;
	};
	lap_kb_pasca.view();
	
});
</script>

<style type="text/css">
#table_lap_kb_pasca {
	font-size: 12px;
}
</style>