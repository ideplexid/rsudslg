<?php

global $db;
require_once 'smis-base/smis-include-service-consumer.php';

$header = array ();
$header [] = "no";
$header [] = "tanggal";
$header [] = "nama_pasien";
$header [] = "nrm_pasien";
$header [] = "noreg_pasien";
$header [] = "alamat";
$header [] = "umur_pasien";
$header [] = "hamil_ke";
$header [] = "umur_kehamilan";
$header [] = "letak_janin_kep";
$header [] = "letak_janin_su";
$header [] = "letak_janin_lin";
$header [] = "metode_spt";
$header [] = "metode_ve";
$header [] = "metode_sc";
$header [] = "metode_forcep";
$header [] = "metode_oksitosin";
$header [] = "metode_amniotomi";
$header [] = "bayi_lahir_hidup";
$header [] = "bayi_lk";
$header [] = "bayi_pr";
$header [] = "bayi_kembar";
$header [] = "bayi_mati_krg_7_hr";
$header [] = "bayi_mati_lbh_7_hr";
$header [] = "ibu_mati";
$header [] = "rujukan";

$uitable = new Table ( $header, "", NULL, false );
$uitable->setName ( "lap_persalinan" );
$uitable->addHeader ( "before", "<tr>
								<th rowspan='3'>No</th>
								<th rowspan='3'>Tanggal</th>
								<th rowspan='3'>Nama Pasien</th>
								<th rowspan='3'>NRM Pasien</th>
								<th rowspan='3'>Noreg Pasien</th>
                                <th rowspan='3'>Alamat</th>
								<th rowspan='3'>Umur</th>
								<th rowspan='3'>Hamil</th>
								<th rowspan='3'>Umur Kehamilan</th>
								<th colspan='3'>Letak Janin</th>
								<th colspan='6'>Metode Persalinan</th>
								<th colspan='6'>Bayi</th>
								<th rowspan='3'>Ibu Mati 42 Hari</th>
								<th rowspan='3'>Rujukan</th>
                                </tr>" );
$uitable->addHeader ( "before", "<tr>
								<th rowspan='2'>Kep</th>
								<th rowspan='2'>SU</th>
								<th rowspan='2'>Lin</th>
								<th rowspan='2'>Spt</th>
								<th rowspan='2'>VE</th>
								<th rowspan='2'>SC</th>
								<th rowspan='2'>Forcep</th>
								<th rowspan='2'>Oksitosin</th>
								<th rowspan='2'>Amniotomi</th>
								<th rowspan='2'>Lahir Hidup</th>
								<th rowspan='2'>L</th>
								<th rowspan='2'>P</th>
								<th rowspan='2'>Kembar</th>
								<th colspan='2'>Mati</th>
                                </tr>" );
$uitable->addHeader ( "before", "<tr>
								<th>< 7 Hari</th>
								<th>> 7 Hari</th>
                                </tr>" );
$uitable->setHeaderVisible ( false );
$uitable->setFooterVisible ( false );

if(isset($_POST['command'])) {
    require_once 'medical_record/class/adapter/LapPersalinanAdapter.php';
    $adapter = new LapPersalinanAdapter();
	$dbtable = new DBTable ( $db, "smis_mr_persalinan" );
    
    if(isset ( $_POST ['dari'] ) && $_POST ['dari'] != "") {
        $dbtable->addCustomKriteria ( " tanggal >=", "'".$_POST['dari']."'" );
    }
    if(isset ( $_POST ['sampai'] ) && $_POST ['sampai'] != "") {
        $dbtable->addCustomKriteria ( " tanggal ", "<='".$_POST['sampai']."'" );
    }
    if(isset ( $_POST ['origin'] ) && $_POST ['origin'] != "") {
        $dbtable->addCustomKriteria ( " origin ", "='".$_POST['origin']."'" );
    }
    $dbtable->setShowAll ( true );
    $dbtable->setOrder ( " tanggal ASC " );
    require_once 'medical_record/class/responder/LapPersalinanResponder.php';
	$dbres = new LapPersalinanResponder ( $dbtable, $uitable, $adapter );
	$data = $dbres->command ( $_POST ['command'] );
	echo json_encode ( $data );
	return;
}

$query="SELECT DISTINCT origin FROM smis_mr_persalinan WHERE prop!='del' ";
$origin=new OptionBuilder();
$origin->add(" - Semua - ","",1);
$res=$db->get_result($query);
foreach($res as $x){
    if($x->origin==""){
        continue;
    }
    $origin->add(ArrayAdapter::slugFormat("unslug",$x->origin),$x->origin);
}
loadClass ( "ServiceProviderList" );
$service = new ServiceProviderList ( $db, "push_antrian" );
$service->execute ();
$ruangan = $service->getContent ();
$ruang = array();
$option['value'] = '';
     $option['name'] = '- SEMUA -';
     $option['default'] = '1';
     $ruang[] = $option;
foreach($ruangan as $r) {
	 $option = array();
     $option['value'] = $r['value'];
     $option['name'] = $r['name'];
     $option['default'] = '0';
     $ruang[] = $option;
}

$uitable->addModal ( "dari", "date", "Dari", "" );
$uitable->addModal ( "sampai", "date", "Sampai", "" );
$uitable->addModal ( "origin", "select", "Asal Data",$origin->getContent() );

$modal = $uitable->getModal ();
$form = $modal->getForm ();

$button = new Button ( "", "", "" );
$button->setIsButton ( Button::$ICONIC );
$button->setIcon ( "fa fa-refresh" );
$button->setClass("btn-primary");
$button->setAction ( "lap_persalinan.view()" );
$form->addElement ( "", $button );

$button_excel = new Button ( "", "", "" );
$button_excel->setIsButton ( Button::$ICONIC );
$button_excel->setIcon ( "fa fa-file-excel-o" );
$button_excel->setClass("btn-primary");
$button_excel->setAction ( "lap_persalinan.excel()" );
$form->addElement ( "", $button_excel );

echo $form->getHtml ();
echo $uitable->getHtml ();

echo addJS ("framework/bootstrap/js/bootstrap-datepicker.js");
echo addJS ("framework/smis/js/table_action.js");
echo addCSS ("framework/bootstrap/css/datepicker.css");
echo addJS ( "medical_record/resource/js/lap_persalinan.js", false );

?>