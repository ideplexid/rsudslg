<?php
setChangeCookie ( false );
$header = array ();
$header [] = "No";
$header [] = "Met";
$header [] = "Ganti";
$header [] = "K-IUD";
$header [] = "K-IMPLANT";
$header [] = "D-IUD";
$header [] = "D-IMPLANT";
$header [] = "B-IUD";
$header [] = "B-IMPLANT";
$header [] = "T-IUD";
$header [] = "T-IMPLANT";
$header [] = "PIL";
$header [] = "SUNTIK";
$header [] = "KONDOM";
$header [] = "Praski";

$uitable = new Table ( $header, "", NULL, false );
$uitable->setName ( "lap_kb_ulang" );
$uitable->setFooterVisible ( false );
$uitable->addHeader ( "before", "<tr>
								<th rowspan='3'>NO</th>
								<th rowspan='3'>METODE KONTRASEPSI</th>
								<th rowspan='3'>JUMLAH GANTI CARA</th>
								<th colspan='8'>JUMLAH PEMASANGAN ULANG IUD DAN IMPLANT OLEH</th>
								<th rowspan='2' colspan='3'>JUMLAH PEMBERIAN KONTRASEPSI ULANG</th>
								<th rowspan='3'>JUMLAH PELAYANAN ULANG KELUARGA PRA S DAN KS I</th>
							</tr>" );
$uitable->addHeader ( "before", "<tr>
								<th colspan='2'>KLINIK KB</th>
								<th colspan='2'>DOKTER PRAKTEK SWASTA</th>
								<th colspan='2'>BIDAN PRAKTEK SWASTA</th>
								<th colspan='2'>TOTAL</th>			
								</tr>" );
$uitable->addHeader ( "before", "<tr>
								<th>IUD</th>
								<th>IMPLANT</th>
								<th>IUD</th>
								<th>IMPLANT</th>
								<th>IUD</th>
								<th>IMPLANT</th>
								<th>IUD</th>
								<th>IMPLANT</th>
								<th>PIL</th>
								<th>SUNTIK</th>
								<th>KONDOM</th>
								</tr>" );

$uitable->setHeaderVisible ( false );
class KBUlang extends ArrayAdapter {
	private $k_iud;
	private $k_implant;
	private $d_iud;
	private $d_implant;
	private $b_iud;
	private $b_implant;
	private $t_iud;
	private $t_implant;
	private $mow;
	private $mop;
	private $kondom;
	private $suntikan;
	private $pil;
	private $jumlah;
	public function __construct() {
		$this->iud = array (
				"No" => 1,
				"Met" => "IUD",
				"Ganti" => 0,
				"K-IUD" => 0,
				"K-IMPLANT" => 0,
				"D-IUD" => 0,
				"D-IMPLANT" => 0,
				"B-IUD" => 0,
				"B-IMPLANT" => 0,
				"T-IUD" => 0,
				"T-IMPLANT" => 0,
				"PIL" => 0,
				"SUNTIK" => 0,
				"KONDOM" => 0,
				"Praski" => 0 
		);
		$this->mow = array (
				"No" => 2,
				"Met" => "MOW",
				"Ganti" => 0,
				"K-IUD" => 0,
				"K-IMPLANT" => 0,
				"D-IUD" => 0,
				"D-IMPLANT" => 0,
				"B-IUD" => 0,
				"B-IMPLANT" => 0,
				"T-IUD" => 0,
				"T-IMPLANT" => 0,
				"PIL" => 0,
				"SUNTIK" => 0,
				"KONDOM" => 0,
				"Praski" => 0 
		);
		$this->mop = array (
				"No" => 3,
				"Met" => "MOP",
				"Ganti" => 0,
				"K-IUD" => 0,
				"K-IMPLANT" => 0,
				"D-IUD" => 0,
				"D-IMPLANT" => 0,
				"B-IUD" => 0,
				"B-IMPLANT" => 0,
				"T-IUD" => 0,
				"T-IMPLANT" => 0,
				"PIL" => 0,
				"SUNTIK" => 0,
				"KONDOM" => 0,
				"Praski" => 0 
		);
		$this->kondom = array (
				"No" => 4,
				"Met" => "KONDOM",
				"Ganti" => 0,
				"K-IUD" => 0,
				"K-IMPLANT" => 0,
				"D-IUD" => 0,
				"D-IMPLANT" => 0,
				"B-IUD" => 0,
				"B-IMPLANT" => 0,
				"T-IUD" => 0,
				"T-IMPLANT" => 0,
				"PIL" => 0,
				"SUNTIK" => 0,
				"KONDOM" => 0,
				"Praski" => 0 
		);
		$this->implant = array (
				"No" => 5,
				"Met" => "IMPLANT",
				"Ganti" => 0,
				"K-IUD" => 0,
				"K-IMPLANT" => 0,
				"D-IUD" => 0,
				"D-IMPLANT" => 0,
				"B-IUD" => 0,
				"B-IMPLANT" => 0,
				"T-IUD" => 0,
				"T-IMPLANT" => 0,
				"PIL" => 0,
				"SUNTIK" => 0,
				"KONDOM" => 0,
				"Praski" => 0 
		);
		$this->suntikan = array (
				"No" => 6,
				"Met" => "SUNTIKAN",
				"Ganti" => 0,
				"K-IUD" => 0,
				"K-IMPLANT" => 0,
				"D-IUD" => 0,
				"D-IMPLANT" => 0,
				"B-IUD" => 0,
				"B-IMPLANT" => 0,
				"T-IUD" => 0,
				"T-IMPLANT" => 0,
				"PIL" => 0,
				"SUNTIK" => 0,
				"KONDOM" => 0,
				"Praski" => 0 
		);
		$this->pil = array (
				"No" => 7,
				"Met" => "PIL",
				"Ganti" => 0,
				"K-IUD" => 0,
				"K-IMPLANT" => 0,
				"D-IUD" => 0,
				"D-IMPLANT" => 0,
				"B-IUD" => 0,
				"B-IMPLANT" => 0,
				"T-IUD" => 0,
				"T-IMPLANT" => 0,
				"PIL" => 0,
				"SUNTIK" => 0,
				"KONDOM" => 0,
				"Praski" => 0 
		);
		$this->jumlah = array (
				"No" => 8,
				"Met" => "JUMLAH",
				"Ganti" => 0,
				"K-IUD" => 0,
				"K-IMPLANT" => 0,
				"D-IUD" => 0,
				"D-IMPLANT" => 0,
				"B-IUD" => 0,
				"B-IMPLANT" => 0,
				"T-IUD" => 0,
				"T-IMPLANT" => 0,
				"PIL" => 0,
				"SUNTIK" => 0,
				"KONDOM" => 0,
				"Praski" => 0 
		);
	}
	public function adapt($d) {
		$cur_array = null;
		
		if ($d->metode == "IUD") {
			$cur_array = &$this->iud;
		} else if ($d->metode == "MOW") {
			$cur_array = &$this->mow;
		} else if ($d->metode == "MOP") {
			$cur_array = &$this->mop;
		} else if ($d->metode == "Kondom") {
			$cur_array = &$this->kondom;
		} else if ($d->metode == "Implant") {
			$cur_array = &$this->implant;
		} else if ($d->metode == "Suntikan") {
			$cur_array = &$this->suntikan;
		} else if ($d->metode == "PIL") {
			$cur_array = &$this->pil;
		}
		
		if ($d->petugas == "Klinik" && $d->metode == "IUD") {
			$cur_array ['K-IUD'] ++;
			$cur_array ['T-IUD'] ++;
		}
		if ($d->petugas == "Klinik" && $d->metode == "Implant") {
			$cur_array ['K-IMPLANT'] ++;
			$cur_array ['T-IMPLANT'] ++;
		}
		if ($d->petugas == "Dokter" && $d->metode == "IUD") {
			$cur_array ['D-IUD'] ++;
			$cur_array ['T-IUD'] ++;
		}
		if ($d->petugas == "Dokter" && $d->metode == "Implant") {
			$cur_array ['D-IMPLANT'] ++;
			$cur_array ['T-IMPLANT'] ++;
		}
		if ($d->petugas == "Bidan" && $d->metode == "IUD") {
			$cur_array ['B-IUD'] ++;
			$cur_array ['T-IUD'] ++;
		}
		if ($d->petugas == "Bidan" && $d->metode == "Implant") {
			$cur_array ['B-IMPLANT'] ++;
			$cur_array ['T-IMPLANT'] ++;
		}
		if ($d->metode == "PIL") {
			$cur_array ['PIL'] ++;
		}
		if ($d->metode == "Suntikan") {
			$cur_array ['SUNTIK'] ++;
		}
		if ($d->metode == "Kondom") {
			$cur_array ['KONDOM'] ++;
		}
		if ($d->praski == "1") {
			$cur_array ['Praski'] ++;
			$this->jumlah ['Praski'] ++;
		}
		if ($d->ganti_cara == "1") {
			$cur_array ['Ganti'] ++;
			$this->jumlah ['Ganti'] ++;
		}
		
		return null;
	}
	public function getContent($data) {
		parent::getContent ( $data );
		
		$result = array ();
		$result [] = $this->iud;
		$result [] = $this->mow;
		$result [] = $this->mop;
		$result [] = $this->kondom;
		$result [] = $this->implant;
		$result [] = $this->suntikan;
		$result [] = $this->pil;
		$result [] = $this->jumlah;
		$final = $this->removeZero ( $result );
		return $final;
	}
	public function removeZero($gdata) {
		$hasil = array ();
		foreach ( $gdata as $onedata ) {
			$one_hasil = array ();
			foreach ( $onedata as $name => $value ) {
				if (is_string ( $value ) || $value > 0) {
					$one_hasil [$name] = $value;
				} else {
					$one_hasil [$name] = "";
				}
			}
			$hasil [] = $one_hasil;
		}
		return $hasil;
	}
}

if (isset ( $_POST ['command'] )) {
	$adapter = new KBUlang ();
	$dbtable = new DBTable ( $db, "smis_mr_kb" );
	if (isset ( $_POST ['dari'] ) && isset ( $_POST ['sampai'] ) && $_POST ['dari'] != "" && $_POST ['sampai'] != "") {
		$dari = $_POST ['dari'];
		$sampai = $_POST ['sampai'];
		$dbtable->addCustomKriteria ( "'" . $dari . "'", "<=tanggal" );
		$dbtable->addCustomKriteria ( "'" . $sampai . "'", ">=tanggal" );
		$dbtable->setShowAll ( true );
	}
    if(isset ( $_POST ['origin'] ) && $_POST ['origin'] != "") {
        $dbtable->addCustomKriteria ( " origin ", "='".$_POST['origin']."'" );
    }
	$dbtable->addCustomKriteria ( "pelayanan", "='KB Ulang'" );
	$dbtable->setOrder ( " tanggal ASC " );
	
	$dbres = new DBResponder ( $dbtable, $uitable, $adapter );
	$data = $dbres->command ( $_POST ['command'] );
	echo json_encode ( $data );
	return;
}

$query="SELECT DISTINCT origin FROM smis_mr_diagnosa WHERE prop!='del' ";
$option=new OptionBuilder();
$option->add(" - Semua - ","",1);

$res=$db->get_result($query);
foreach($res as $x){
    if($x->origin==""){
        continue;
    }
    $option->add(ArrayAdapter::slugFormat("unslug",$x->origin),$x->origin);
}

$uitable->addModal ( "dari", "date", "Dari", "" );
$uitable->addModal ( "sampai", "date", "Sampai", "" );
$uitable->addModal ( "ruangan", "hidden", "", "igd" );
$uitable->addModal( "origin", "select", "Asal", $option->getContent() );

$modal = $uitable->getModal ();
$form = $modal->getForm ();

$button = new Button ( "", "", "" );
$button->setIsButton ( Button::$ICONIC );
$button->setIcon ( "icon-white " . BUtton::$icon_refresh );
$button->setAction ( "lap_kb_ulang.view()" );
$button->setClass("btn-primary");

$form->addElement ( "", $button );
$button = new Button ( "", "", "" );
$button->setIsButton ( Button::$ICONIC );
$button->setIcon ( "icon-white " . BUtton::$icon_print );
$button->setAction ( "smis_print($('#print_table_lap_kb_ulang').html())" );
$button->setClass("btn-primary");

$form->addElement ( "", $button );
echo $form->getHtml ();

echo $uitable->getHtml ();
echo addJS ( "framework/smis/js/table_action.js" );
echo addJS ( "framework/bootstrap/js/bootstrap-datepicker.js" );
echo addCSS ( "framework/bootstrap/css/datepicker.css" );

?>
<script type="text/javascript">
var lap_kb_ulang;
$(document).ready(function(){
	$('.mydate').datepicker();
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
	var column=new Array('id','nama','keterangan','slug');
	lap_kb_ulang=new TableAction("lap_kb_ulang","medical_record","lap_kb_ulang",column);
	lap_kb_ulang.getRegulerData=function(){
		var reg_data={	
				page:this.page,
				action:this.action,
				super_command:this.super_command,
				prototype_name:this.prototype_name,
				prototype_slug:this.prototype_slug,
				prototype_implement:this.prototype_implement,
				dari:$("#"+this.prefix+"_dari").val(),
				sampai:$("#"+this.prefix+"_sampai").val(),
                origin:$("#"+this.prefix+"_origin").val()
				};
		return reg_data;
	};
	lap_kb_ulang.view();
	
});
</script>

<style type="text/css">
#table_lap_kb_ulang {
	font-size: 12px;
}
</style>