<?php

/**
 * menampilkan data 
 * pasien yang melakukan 
 * imunisasi
 * 
 * @author 		: Nurul Huda
 * @copyright 	: goblooge@gmail.com
 * @license 	: LGPLv2
 * @database 	: - smis_mr_imunisasi
 * @since		: 06 Juli 2017
 * @version		: 1.0.0
 * 
 * */

global $db;
require_once 'smis-libs-class/MasterSlaveTemplate.php';

$header=array ('No.',"Nama","L/P","Tanggal","Lahir","Ibu","Alamat","HBO","BCG","P1","P2","P3","P4","D1","D2","D3","Campak");
$pemreg=new MasterSlaveTemplate($db, "smis_mr_imunisasi", "medical_record", "lap_imunisasi");
$pemreg->setDateEnable(true);
$uitable=$pemreg->getUItable();
$uitable=new Table($header);
$uitable->setHeader($header);
$uitable->setAction(false);
$uitable->addHeader("before","<tr><th rowspan='2'>No.</th><th rowspan='2'>Nama</th><th rowspan='2'>L/P</th><th rowspan='2'>Tanggal</th><th rowspan='2'>Lahir</th><th rowspan='2'>Ibu</th><th rowspan='2'>Alamat</th><th rowspan='2'>HBO</th><th rowspan='2'>BCG</th><th colspan='4'>POLIO</th><th colspan='3'>DPT Hb Hib</th><th rowspan='2'>Campak</th></tr>");
$uitable->addHeader("before","<tr><th>I</th><th>II</th><th>III</th><th>IV</th><th>I</th><th>II</th><th>III</th></tr>");
$uitable->setHeaderVisible(false);



$uitable->setName("lap_imunisasi");
$pemreg->getDBtable()->setOrder(" tanggal DESC ");
$pemreg->setUITable($uitable);
if($pemreg->getDBResponder()->isView() && $_POST['super_command']==""){
	$pemreg	->getDBtable()
			->addCustomKriteria(NULL, "tanggal>='".$_POST['dari']."'")
            ->addCustomKriteria(NULL, "tanggal<'".$_POST['sampai']."'")
            ->addCustomKriteria ( " origin ", "='".$_POST['origin']."'" );
}

$query="SELECT DISTINCT origin FROM smis_mr_diagnosa WHERE prop!='del' ";
$option=new OptionBuilder();
$option->add(" - Semua - ","",1);

$res=$db->get_result($query);
foreach($res as $x){
    if($x->origin==""){
        continue;
    }
    $option->add(ArrayAdapter::slugFormat("unslug",$x->origin),$x->origin);
}

$pemreg  ->addFlag("dari", "Pilih Tanggal Dari", "Masukan Tanggal Awal Dahulu")
		 ->addFlag("sampai", "Pilih Tanggal Sampai", "Masukan Tanggal Akhir Dahulu")
		 ->addNoClear("dari")
		 ->addNoClear("sampai")
		 ->setDateTimeEnable(true)
		 ->getUItable()
		 ->addModal("dari", "date", "Dari", "")
		 ->addModal("sampai", "date", "Sampai", "")
         ->addModal ( "origin", "select", "Asal", $option->getContent() );         
$form=$pemreg ->getForm();

$button = new Button ( "", "", "" );
$button->setIsButton ( Button::$ICONIC );
$button->setIcon ( "icon-white " . BUtton::$icon_refresh );
$button->setAction ( "lap_imunisasi.view()" );
$button->setClass("btn-primary");

$form->addElement ( "", $button );
$button = new Button ( "", "", "" );
$button->setIsButton ( Button::$ICONIC );
$button->setIcon ( "icon-white " . BUtton::$icon_print );
$button->setAction ( "smis_print($('#print_table_lap_imunisasi').html())" );
$button->setClass("btn-primary");
$form->addElement ( "", $button );

$adapter=new SummaryAdapter();
$adapter->setUseNumber(true, "No.","back.");
$adapter->add("Tanggal", "tanggal","date d M Y H:i")
        ->add("Nama", "nama_pasien")
        ->add("L/P", "jk","trivial_0_L_P")
        ->add("Lahir", "tgl_lahir","date d M Y")
        ->add("Ibu", "nama_ibu")
        ->add("Alamat", "alamat")
        ->add("HBO","hbo","trivial_1_v_")
        ->add("P1", "polio1","trivial_1_v_")
        ->add("P2", "polio2","trivial_1_v_")
        ->add("P3", "polio3","trivial_1_v_")
        ->add("P4", "polio4","trivial_1_v_")
        ->add("D1", "dpthbhib1","trivial_1_v_")
        ->add("D2", "dpthbhib2","trivial_1_v_")
        ->add("D3", "dpthbhib3","trivial_1_v_")
        ->add("Campak", "campak","trivial_1_v_")
        ->add("BCG", "bcg","trivial_1_v_");
$adapter->addFixValue("Nama","Total");
$adapter->addSummary("HBO","hbo")
        ->addSummary("P1", "polio1")
        ->addSummary("P2", "polio2")
        ->addSummary("P3", "polio3")
        ->addSummary("P4", "polio4")
        ->addSummary("D1", "dpthbhib1")
        ->addSummary("D2", "dpthbhib2")
        ->addSummary("D3", "dpthbhib3")
        ->addSummary("Campak", "campak")
        ->addSummary("BCG", "bcg");
$pemreg->setAdapter($adapter);
$pemreg ->addRegulerData("dari","dari","id-value")
        ->addRegulerData("sampai","sampai","id-value");
$pemreg->initialize();
?>

