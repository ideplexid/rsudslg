<?php

global $db;
require_once "smis-base/smis-include-service-consumer.php";
require_once "medical_record/function/date_range.php";

$slug_igd = getSettings($db, "smis-mr-slug-igd", "");

$header = array("ruangan",
                "masuk_rs_l",
                "masuk_rs_p",
                "masuk_rs_total",
                "keluar_hidup_l",
                "keluar_hidup_p",
                "keluar_hidup_total",
                "mati_k48_l",
                "mati_k48_p",
                "mati_k48_total",
                "mati_l48_l",
                "mati_l48_p",
                "mati_l48_total",
                "hari_perawatan",
                "lama_rawat",
                "bor"
                );
                
$uitable = new Table ( $header, "", NULL, false );
$uitable->setName ( "efisiensi_mutu_layanan_ruangan" );

$uitable->addHeader ( "before", "<tr>
                                    <th rowspan='2'>RUANGAN</th>
                                    <th colspan='3'>MASUK RUMAH SAKIT</th>
                                    <th colspan='3'>KELUAR HIDUP</th>
                                    <th colspan='3'>PASIEN MATI <48 JAM</th>
                                    <th colspan='3'>PASIEN MATI >48 JAM</th>
                                    <th rowspan='2'>HARI PERAWATAN</th>
                                    <th rowspan='2'>LAMA RAWAT</th>
                                    <th rowspan='2'>BOR</th>
                                </tr>" );
                                
$uitable->addHeader ( "before", "<tr>
                                    <th>L</th>
                                    <th>P</th>
                                    <th>JUMLAH</th>
                                    <th>L</th>
                                    <th>P</th>
                                    <th>JUMLAH</th>
                                    <th>L</th>
                                    <th>P</th>
                                    <th>JUMLAH</th>
                                    <th>L</th>
                                    <th>P</th>
                                    <th>JUMLAH</th>
								</tr>" );
                                
$uitable->setHeaderVisible ( false );
$uitable->setFooterVisible( false );

if (isset ( $_POST ['command'] )) {
    /****** Get Ruangan dan Jumlah Bed pada Ruangan *********/
    $urjip=new ServiceConsumer($db, "get_urjip",array());
    $urjip->setMode(ServiceConsumer::$MULTIPLE_MODE);
    $urjip->execute();
    $content=$urjip->getContent();

    $ruangan=array();
    $i = 0;
    foreach ($content as $autonomous=>$ruang){
        foreach($ruang as $nama_ruang=>$jip){
            if($jip[$nama_ruang] == "URI"){
                $option=array();
                $option['value']=$nama_ruang;
                $option['name']=ArrayAdapter::format("unslug", $nama_ruang);
                $ruangan[]=$option;
            }
        }
    }
    
    $total_bed = 0;
    $i = 0;
    foreach($ruangan as $r) {
         $serv = new ServiceConsumer($db,"get_bed_only", NULL, $r['value']);
         $serv->execute();
         $data = $serv->getContent();
         $ruangan[$i]['jumlah_bed'] = sizeof($data['list']);
         $total_bed = $total_bed + sizeof($data['list']);
         $i++;
    }
    /******  *********/
    
    /****** Get Pasien *********/
    $serv = new ServiceConsumer($db,"get_patient_lama_rawat", NULL, "registration");
    $serv->addData("command", "list");
    $serv->addData("setshowall", "1");
    $serv->addData("dari", $_POST['dari']);
    $serv->addData("sampai", $_POST['sampai']);
    $serv->execute();
    $data = $serv->getContent();
    /******  *********/
    
    require_once "medical_record/class/adapter/EfisiensiMutuLayananRuanganAdapter.php";
    $adapter = new EfisiensiMutuLayananRuanganAdapter();
    $adapter->setJumlahTT($total_bed);
    $adapter->setRuangan($ruangan);
    $adapter->setDariSampai($_POST['dari'], $_POST['sampai']);
    $ready = $adapter->getContent($data['data']);
    
    if($_POST['command'] == 'list') {
        $uitable->setContent($ready);
        $content['list'] = $uitable->getBodyContent();
        $response = new ResponsePackage ();
        $response->setAlertVisible ( false );
        $response->setStatus ( ResponsePackage::$STATUS_OK );
        $response->setContent($content);
        echo json_encode ( $response->getPackage () );
        return;
                
        $dbres = new DBResponder ( $dbtable, $uitable, $adapter );
        $data = $dbres->command ( $_POST ['command'] );
        echo json_encode ( $data );
        return;
    } else if($_POST['command'] == 'excel') {
        require_once ("smis-libs-out/php-excel/PHPExcel.php");
        $file = new PHPExcel ();
        $file->getProperties ()->setCreator ( "Rumah Sakit" );
        $file->getProperties ()->setLastModifiedBy ( "Rumah Sakit Bagian Administrator" );
        $file->getProperties ()->setTitle ( "Efisiensi Mutu Layanan Ruangan" );
        $file->getProperties ()->setSubject ( "Efisiensi Mutu Layanan Ruangan" );
        $file->getProperties ()->setDescription ( "Efisiensi Mutu Layanan Ruangan Generated From system" );
        $file->getProperties ()->setKeywords ( "Efisiensi Mutu Layanan Ruangan" );
        $file->getProperties ()->setCategory ( "Efisiensi Mutu Layanan Ruangan" );
        
        $sheet = $file->getActiveSheet ();
        $i = 1;
        
        $objDrawing = new PHPExcel_Worksheet_Drawing();
        $objDrawing->setName('logo_img');
        $objDrawing->setDescription('logo_img');
        $objDrawing->setPath(getLogoNonInterlaced());
        $objDrawing->setCoordinates('A'.$i);
        $objDrawing->setOffsetX(20); 
        $objDrawing->setOffsetY(5);
        $objDrawing->setWidth(100); 
        $objDrawing->setHeight(100);
        $objDrawing->setWorksheet($sheet);
        $i++;
        
        $sheet->setCellValue("C".$i, getSettings($db,"smis_autonomous_title"));
        $sheet->getStyle("C".$i)->getFont()->setBold(true)->setSize(14);
        $i++;
        $sheet->setCellValue("C".$i, getSettings($db,"smis_autonomous_address"));
        $sheet->getStyle("C".$i)->getFont()->setBold(true)->setSize(14);
        $i++;
        $sheet->setCellValue("C".$i, getSettings($db,"smis_autonomous_contact"));
        $sheet->getStyle("C".$i)->getFont()->setBold(true)->setSize(14);
        $i += 3;
        $sheet->mergeCells("A".$i.":P".$i)->setCellValue("A".$i,"EFISIENSI MUTU LAYANAN RUANGAN");
        $sheet->getStyle("A".$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle("A".$i)->getFont()->setBold(true)->setSize(14);
        $i++;
        $sheet->mergeCells("A".$i.":P".$i)->setCellValue("A".$i, strtoupper(ArrayAdapter::dateFormat("date d M Y", $_POST['dari']))." - ".strtoupper(ArrayAdapter::dateFormat("date d M Y", $_POST['sampai'])));
        $sheet->getStyle("A".$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle("A".$i)->getFont()->setBold(true);
        $i += 3;
        
        $border_start = $i;
        $ips = $i + 1;
        $sheet->mergeCells("A".$i.":A".$ips)->setCellValue("A".$i, "RUANGAN");
        $sheet->mergeCells("B".$i.":D".$i)->setCellValue("B".$i, "MASUK RUMAH SAKIT");
        $sheet->mergeCells("E".$i.":G".$i)->setCellValue("E".$i, "KELUAR HIDUP");
        $sheet->mergeCells("H".$i.":J".$i)->setCellValue("H".$i, "PASIEN MATI <48 JAM");
        $sheet->mergeCells("K".$i.":M".$i)->setCellValue("K".$i, "PASIEN MATI >48 JAM");
        $sheet->mergeCells("N".$i.":N".$ips)->setCellValue("N".$i, "HARI PERAWATAN");
        $sheet->mergeCells("O".$i.":O".$ips)->setCellValue("O".$i, "LAMA RAWAT");
        $sheet->mergeCells("P".$i.":P".$ips)->setCellValue("P".$i, "BOR");
        $sheet->getStyle("A".$i.":P".$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle("A".$i.":P".$i)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $sheet->getStyle("A".$i.":P".$i)->getFont()->setBold(true);
        $i++;
        
        $sheet->setCellValue("B".$i, "L");
        $sheet->setCellValue("C".$i, "P");
        $sheet->setCellValue("D".$i, "JUMLAH");
        $sheet->setCellValue("E".$i, "L");
        $sheet->setCellValue("F".$i, "P");
        $sheet->setCellValue("G".$i, "JUMLAH");
        $sheet->setCellValue("H".$i, "L");
        $sheet->setCellValue("I".$i, "P");
        $sheet->setCellValue("J".$i, "JUMLAH");
        $sheet->setCellValue("K".$i, "L");
        $sheet->setCellValue("L".$i, "P");
        $sheet->setCellValue("M".$i, "JUMLAH");
        $sheet->getStyle("A".$i.":M".$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle("A".$i.":M".$i)->getFont()->setBold(true);
        $i++;
        
        foreach($ready as $x) {
            $sheet->setCellValue("A".$i, $x["ruangan"]);
            $sheet->setCellValue("B".$i, $x["masuk_rs_l"]);
            $sheet->setCellValue("C".$i, $x["masuk_rs_p"]);
            $sheet->setCellValue("D".$i, $x["masuk_rs_total"]);
            $sheet->setCellValue("E".$i, $x["keluar_hidup_l"]);
            $sheet->setCellValue("F".$i, $x["keluar_hidup_p"]);
            $sheet->setCellValue("G".$i, $x["keluar_hidup_total"]);
            $sheet->setCellValue("H".$i, $x["mati_k48_l"]);
            $sheet->setCellValue("I".$i, $x["mati_k48_p"]);
            $sheet->setCellValue("J".$i, $x["mati_k48_total"]);
            $sheet->setCellValue("K".$i, $x["mati_l48_l"]);
            $sheet->setCellValue("L".$i, $x["mati_l48_p"]);
            $sheet->setCellValue("M".$i, $x["mati_l48_total"]);
            $sheet->setCellValue("N".$i, $x["hari_perawatan"]);
            $sheet->setCellValue("O".$i, $x["lama_rawat"]);
            $sheet->setCellValue("P".$i, $x["bor"]);
            $sheet->getStyle("B".$i.":P".$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $i++;
        }
        $border_end = $i - 1;
        $thin = array ();
        $thin['borders']=array();
        $thin['borders']['allborders']=array();
        $thin['borders']['allborders']['style']=PHPExcel_Style_Border::BORDER_THIN ;
        $sheet  ->getStyle ( "A".$border_start.":P".$border_end )->applyFromArray ($thin);
        
        $filename = "Efisiensi Mutu Layanan Ruangan ".strtoupper(ArrayAdapter::dateFormat("date d M Y", $_POST['dari']))." - ".strtoupper(ArrayAdapter::dateFormat("date d M Y", $_POST['sampai'])).".xls";
        
        header ( 'Content-Type: application/vnd.ms-excel' );
        header ( 'Content-Disposition: attachment;filename="'.$filename.'"' );
        header ( 'Cache-Control: max-age=0' );
        $writer = PHPExcel_IOFactory::createWriter ( $file, 'Excel5' );
        $writer->save ( 'php://output' );
		return;
    }
}

$uitable->addModal( "dari", "date", "Dari", "" );
$uitable->addModal( "sampai", "date", "Sampai", "" );

$modal = $uitable->getModal ();
$form=$modal->getForm();

$v = new Button("", "", "");
$v->addClass("btn-primary")
		->setIsButton(Button::$ICONIC)
		->setIcon("fa fa-refresh")
		->setAction("efisiensi_mutu_layanan_ruangan.view()");
$form->addElement("", $v);

$excel = new Button("", "", "");
$excel->addClass("btn-primary")
		->setIsButton(Button::$ICONIC)
		->setIcon("fa fa-file-excel-o")
		->setAction("efisiensi_mutu_layanan_ruangan.excel()");
$form->addElement("", $excel);

echo "<h2><strong>Efisiensi Mutu Layanan Ruangan</h2>";
echo $form->getHtml();
echo $uitable->getHtml ();

echo addJS ( "framework/smis/js/table_action.js" );
echo addJS ( "framework/bootstrap/js/bootstrap-datepicker.js" );
echo addCSS ( "framework/bootstrap/css/datepicker.css" );

?>

<script>

var efisiensi_mutu_layanan_ruangan;

$(document).ready(function(){
	$('.mydate').datepicker();
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
	efisiensi_mutu_layanan_ruangan=new TableAction("efisiensi_mutu_layanan_ruangan","medical_record","efisiensi_mutu_layanan_ruangan",new Array());
	efisiensi_mutu_layanan_ruangan.getRegulerData=function(){
		var reg_data={	
				page:this.page,
				action:this.action,
				super_command:this.super_command,
				prototype_name:this.prototype_name,
				prototype_slug:this.prototype_slug,
				prototype_implement:this.prototype_implement,
				dari:$("#"+this.prefix+"_dari").val(),
				sampai:$("#"+this.prefix+"_sampai").val()
				};
		return reg_data;
	};
	
});

</script>