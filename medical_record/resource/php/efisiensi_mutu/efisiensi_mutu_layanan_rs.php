<?php

global $db;
require_once "smis-base/smis-include-service-consumer.php";

$header = array("NO.",
                "URAIAN",
                "PENCAPAIAN",
                "STANDAR"
                );
                
$uitable = new Table ( $header, "", NULL, false );
$uitable->setName ( "efisiensi_mutu_layanan_rs" );

$uitable->setActionEnable( false );
$uitable->setFooterVisible( false );

if (isset ( $_POST ['command'] )) {
    /****** Get Total Bed *********/
    $urjip=new ServiceConsumer($db, "get_urjip",array());
    $urjip->setMode(ServiceConsumer::$MULTIPLE_MODE);
    $urjip->execute();
    $content=$urjip->getContent();

    $ruangan=array();
    foreach ($content as $autonomous=>$ruang){
        foreach($ruang as $nama_ruang=>$jip){
            if($jip[$nama_ruang] == "URI"){
                $option=array();
                $option['value']=$nama_ruang;
                $option['name']=ArrayAdapter::format("unslug", $nama_ruang);
                $ruangan[]=$option;
            }
        }
    }
    $total_bed = 0;
    foreach($ruangan as $r) {
         $serv = new ServiceConsumer($db,"get_bed_only", NULL, $r['value']);
         $serv->execute();
         $data = $serv->getContent();
         $total_bed = $total_bed + sizeof($data['list']);
    }
    /******  *********/
    
    /****** Get Pasien *********/
    $serv = new ServiceConsumer($db,"get_patient_lama_rawat", NULL, "registration");
    $serv->addData("command", "list");
    $serv->addData("setshowall", "1");
    $serv->addData("dari", $_POST['dari']);
    $serv->addData("sampai", $_POST['sampai']);
    $serv->execute();
    $data = $serv->getContent();
    /******  *********/
    
    require_once "medical_record/class/adapter/EfisienMutuLayananRSAdapter.php";
    $adapter = new EfisienMutuLayananRSAdapter();
    $adapter->setJumlahTT($total_bed);
    $adapter->setDariSampai($_POST['dari'], $_POST['sampai']);
    $ready = $adapter->getContent($data['data']);
    
    if($_POST['command'] == 'list') {
        $uitable->setContent($ready);
        $content['list'] = $uitable->getBodyContent();
        $response = new ResponsePackage ();
        $response->setAlertVisible ( false );
        $response->setStatus ( ResponsePackage::$STATUS_OK );
        $response->setContent($content);
        echo json_encode ( $response->getPackage () );
        return;
                
        $dbres = new DBResponder ( $dbtable, $uitable, $adapter );
        $data = $dbres->command ( $_POST ['command'] );
        echo json_encode ( $data );
        return;
    } else if($_POST['command'] == 'excel') {
        require_once ("smis-libs-out/php-excel/PHPExcel.php");
        $file = new PHPExcel ();
        $file->getProperties ()->setCreator ( "Rumah Sakit" );
        $file->getProperties ()->setLastModifiedBy ( "Rumah Sakit Bagian Administrator" );
        $file->getProperties ()->setTitle ( "Efisiensi Mutu Layanan RS" );
        $file->getProperties ()->setSubject ( "Efisiensi Mutu Layanan RS" );
        $file->getProperties ()->setDescription ( "Efisiensi Mutu Layanan RS Generated From system" );
        $file->getProperties ()->setKeywords ( "Efisiensi Mutu Layanan RS" );
        $file->getProperties ()->setCategory ( "Efisiensi Mutu Layanan RS" );
        
        $sheet = $file->getActiveSheet ();
        $i = 1;
        
        $objDrawing = new PHPExcel_Worksheet_Drawing();
        $objDrawing->setName('logo_img');
        $objDrawing->setDescription('logo_img');
        $objDrawing->setPath(getLogoNonInterlaced());
        $objDrawing->setCoordinates('A'.$i);
        $objDrawing->setOffsetX(20); 
        $objDrawing->setOffsetY(5);
        $objDrawing->setWidth(100); 
        $objDrawing->setHeight(100);
        $objDrawing->setWorksheet($sheet);
        $i++;
        
        $sheet->setCellValue("C".$i, getSettings($db,"smis_autonomous_title"));
        $sheet->getStyle("C".$i)->getFont()->setBold(true)->setSize(14);
        $i++;
        $sheet->setCellValue("C".$i, getSettings($db,"smis_autonomous_address"));
        $sheet->getStyle("C".$i)->getFont()->setBold(true)->setSize(14);
        $i++;
        $sheet->setCellValue("C".$i, getSettings($db,"smis_autonomous_contact"));
        $sheet->getStyle("C".$i)->getFont()->setBold(true)->setSize(14);
        $i += 3;
        $sheet->mergeCells("A".$i.":D".$i)->setCellValue("A".$i,"EFISIENSI MUTU LAYANAN RUMAH SAKIT");
        $sheet->getStyle("A".$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle("A".$i)->getFont()->setBold(true)->setSize(14);
        $i++;
        $sheet->mergeCells("A".$i.":D".$i)->setCellValue("A".$i, strtoupper(ArrayAdapter::dateFormat("date d M Y", $_POST['dari']))." - ".strtoupper(ArrayAdapter::dateFormat("date d M Y", $_POST['sampai'])));
        $sheet->getStyle("A".$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle("A".$i)->getFont()->setBold(true);
        $i += 3;
        
        $border_start = $i;
        $sheet->setCellValue("A".$i, "NO.");
        $sheet->setCellValue("B".$i, "URAIAN");
        $sheet->setCellValue("C".$i, "PENCAPAIAN");
        $sheet->setCellValue("D".$i, "STANDAR");
        $sheet->getStyle("A".$i.":D".$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle("A".$i.":D".$i)->getFont()->setBold(true);
        $i++;
        
        foreach($ready as $x) {
            $sheet->setCellValue("A".$i, $x["NO."]);
            $sheet->setCellValue("B".$i, $x["URAIAN"]);
            $sheet->setCellValue("C".$i, $x["PENCAPAIAN"]);
            $sheet->setCellValue("D".$i, $x["STANDAR"]);
            $sheet->getStyle("A".$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $sheet->getStyle("C".$i.":D".$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $i++;
        }
        
        $border_end = $i - 1;
        $thin = array ();
        $thin['borders']=array();
        $thin['borders']['allborders']=array();
        $thin['borders']['allborders']['style']=PHPExcel_Style_Border::BORDER_THIN ;
        $sheet  ->getStyle ( "A".$border_start.":D".$border_end )->applyFromArray ($thin);
        
        $filename = "Efisiensi Mutu Layanan RS ".strtoupper(ArrayAdapter::dateFormat("date d M Y", $_POST['dari']))." - ".strtoupper(ArrayAdapter::dateFormat("date d M Y", $_POST['sampai'])).".xls";
        
        header ( 'Content-Type: application/vnd.ms-excel' );
        header ( 'Content-Disposition: attachment;filename="'.$filename.'"' );
        header ( 'Cache-Control: max-age=0' );
        $writer = PHPExcel_IOFactory::createWriter ( $file, 'Excel5' );
        $writer->save ( 'php://output' );
		return;
    }
}

$uitable->addModal( "dari", "date", "Dari", "" );
$uitable->addModal( "sampai", "date", "Sampai", "" );

$modal = $uitable->getModal ();
$form=$modal->getForm();

$v = new Button("", "", "");
$v->addClass("btn-primary")
		->setIsButton(Button::$ICONIC)
		->setIcon("fa fa-refresh")
		->setAction("efisiensi_mutu_layanan_rs.view()");
$form->addElement("", $v);

$excel = new Button("", "", "");
$excel->addClass("btn-primary")
		->setIsButton(Button::$ICONIC)
		->setIcon("fa fa-file-excel-o")
		->setAction("efisiensi_mutu_layanan_rs.excel()");
$form->addElement("", $excel);

echo "<h2><strong>Efisiensi Mutu Layanan RS</h2>";
echo $form->getHtml();
echo $uitable->getHtml ();

echo addJS ( "framework/smis/js/table_action.js" );
echo addJS ( "framework/bootstrap/js/bootstrap-datepicker.js" );
echo addCSS ( "framework/bootstrap/css/datepicker.css" );

?>

<script>

var efisiensi_mutu_layanan_rs;

$(document).ready(function(){
	$('.mydate').datepicker();
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
	efisiensi_mutu_layanan_rs=new TableAction("efisiensi_mutu_layanan_rs","medical_record","efisiensi_mutu_layanan_rs",new Array());
	efisiensi_mutu_layanan_rs.getRegulerData=function(){
		var reg_data={	
				page:this.page,
				action:this.action,
				super_command:this.super_command,
				prototype_name:this.prototype_name,
				prototype_slug:this.prototype_slug,
				prototype_implement:this.prototype_implement,
				dari:$("#"+this.prefix+"_dari").val(),
				sampai:$("#"+this.prefix+"_sampai").val()
				};
		return reg_data;
	};
	
});

</script>