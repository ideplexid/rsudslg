<?php
setChangeCookie ( false );
$uitable = new Table ( array (
		'Nomor',
		'Tanggal',
		'Noreg',
		'Nama',
		"Alamat",
		"L",
		"P",
		"Ruang",
		"Diagnosa",
		"Tindakan",
		"Spesialisasi",
		"Operator",
		"Asisten",
		"Instrument",
		"Jenis Operasi" 
), "", NULL, false );
$uitable->setName ( "lap_operasi_bedah" );
$uitable->addHeader ( "before", "<tr>
								<th rowspan='2'>Nomor</th>
								<th rowspan='2'>Tanggal</th>
								<th rowspan='2'>Noreg</th>
								<th rowspan='2'>Nama</th>
								<th rowspan='2'>Alamat</th>
								<th colspan='2'>Umur</th>
								<th rowspan='2'>Ruang</th>
								<th rowspan='2'>Diagnosa</th>
								<th rowspan='2'>Tindakan</th>
								<th rowspan='2'>Spesialisasi</th>
								<th rowspan='2'>Operator</th>
								<th rowspan='2'>Asisten</th>
								<th rowspan='2'>Instrument</th>
								<th rowspan='2'>Jenis Operasi</th>
							</tr>" );
$uitable->addHeader ( "before", "<tr>
								<th rowspan='2'>L</th>
								<th rowspan='2'>P</th>
								</tr>" );
$uitable->setHeaderVisible ( false );
class BedahOperasiAdapter extends ArrayAdapter {
	public function adapt($d) {
		$a = array ();
		$this->number ++;
		$a ['Nomor'] = $this->number;
		$a ['id'] = $d->id;
		$a ['Tanggal'] = self::format ( "date d M Y", $d->tanggal_mulai );
		$a ['Noreg'] = self::format ( "digit6", $d->noreg_pasien );
		$a ['Nama'] = $d->nama_pasien;
		$a ['Alamat'] = $d->alamat;
		$a ['L'] = $d->jk == "1" ? $d->umur : "";
		$a ['P'] = $d->jk == "0" ? $d->umur : "";
		$a ['Operator'] = $d->dokter_operator;
		$a ['Asisten'] = $d->asisten_operator;
		$a ['Diagnosa'] = $d->diagnosa;
		$a ['Tindakan'] = $d->tindakan;
		$a ['Jenis Operasi'] = $d->jenis_operasi;
		$a ['Spesialisasi'] = $d->spesialisasi;
		$a ['Ruang'] = $d->asal_ruang;
		$a ['Instrument'] = $d->instrument;
		
		return $a;
	}
}

if (isset ( $_POST ['command'] )) {
	$adapter = new BedahOperasiAdapter ();
	$dbtable = new DBTable ( $db, "smis_mr_operasi" );
	if (isset ( $_POST ['dari'] ) && isset ( $_POST ['sampai'] ) && $_POST ['dari'] != "" && $_POST ['sampai'] != "") {
		$dari = $_POST ['dari'];
		$sampai = $_POST ['sampai'];
		$dbtable->addCustomKriteria ( "'" . $dari . "'", "<=tanggal_mulai" );
		$dbtable->addCustomKriteria ( "'" . $sampai . "'", ">=tanggal_mulai" );
	}
	$dbtable->setOrder ( " tanggal_mulai ASC " );
	$dbres = new DBResponder ( $dbtable, $uitable, $adapter );
	$data = $dbres->command ( $_POST ['command'] );
	echo json_encode ( $data );
	return;
}

$uitable->addModal ( "dari", "date", "Dari", "" );
$uitable->addModal ( "sampai", "date", "Sampai", "" );
$uitable->addModal ( "ruangan", "hidden", "", "operasi" );

$modal = $uitable->getModal ();
$form = $modal->getForm ();

$button = new Button ( "", "", "" );
$button->setIsButton ( Button::$ICONIC );
$button->setIcon ( "fa fa-refresh ");
$button->setClass( " btn-primary ");
$button->setAction ( "lap_operasi_bedah.view()" );
$form->addElement ( "", $button );

echo $form->getHtml ();

echo $uitable->getHtml ();
echo addJS ( "framework/smis/js/table_action.js" );
echo addJS ( "framework/bootstrap/js/bootstrap-datepicker.js" );
echo addCSS ( "framework/bootstrap/css/datepicker.css" );

?>
<script type="text/javascript">
var lap_operasi_bedah;
//var employee;
$(document).ready(function(){
	$('.mydate').datepicker();
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
	var column=new Array('id','nama','keterangan','slug');
	lap_operasi_bedah=new TableAction("lap_operasi_bedah","medical_record","lap_operasi_bedah",column);
	lap_operasi_bedah.getRegulerData=function(){
		var reg_data={	
				page:this.page,
				action:this.action,
				super_command:this.super_command,
				prototype_name:this.prototype_name,
				prototype_slug:this.prototype_slug,
				prototype_implement:this.prototype_implement,
				dari:$("#"+this.prefix+"_dari").val(),
				sampai:$("#"+this.prefix+"_sampai").val()
				};
		return reg_data;
	};
	lap_operasi_bedah.view();
	
});
</script>
