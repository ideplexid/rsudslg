<?php
setChangeCookie ( false );
$uitable = new Table ( array (
		'Nomor',
		'Tanggal',
		'Spesialisasi',
		"Anastesi",
		"KH",
		"BS",
		"SD",
		"KC",
		"Dokter",
		"Poli",
		"Ruang",
		"RS Lain" 
), "", NULL, false );
$uitable->setName ( "lap_operasi_kamar" );
$uitable->addHeader ( "before", "<tr>
		<th rowspan='2'>Nomor</th>
		<th rowspan='2'>Tanggal</th>
		<th rowspan='2'>Spesialisasi</th>
		<th rowspan='2'>Anastesi</th>
		<th colspan='4'>Golongan Pembedahan</th>
		<th rowspan='2'>Dokter</th>
		<th colspan='3'>Asal Pasien</th>
		</tr>" );
$uitable->addHeader ( "before", "<tr>
		<th>KH</th>
		<th>BS</th>
		<th>SD</th>
		<th>KC</th>		
		<th>Poli</th>
		<th>Ruang</th>
		<th>RS Lain</th>
		</tr>" );
$uitable->setHeaderVisible ( false );


if (isset ( $_POST ['command'] )) {
	require_once 'medical_record/class/adapter/KamarOperasiAdapter.php';
	$adapter = new KamarOperasiAdapter ();
	$dbtable = new DBTable ( $db, "smis_mr_operasi" );
	if (isset ( $_POST ['dari'] ) && isset ( $_POST ['sampai'] ) && $_POST ['dari'] != "" && $_POST ['sampai'] != "") {
		$dari = $_POST ['dari'];
		$sampai = $_POST ['sampai'];
		$dbtable->addCustomKriteria ( "'" . $dari . "'", "<=tanggal_mulai" );
		$dbtable->addCustomKriteria ( "'" . $sampai . "'", ">=tanggal_mulai" );
	}
	$dbtable->setOrder ( " tanggal_mulai ASC " );
	$dbres = new DBResponder ( $dbtable, $uitable, $adapter );
	$data = $dbres->command ( $_POST ['command'] );
	echo json_encode ( $data );
	return;
}

$uitable->addModal ( "dari", "date", "Dari", "" );
$uitable->addModal ( "sampai", "date", "Sampai", "" );
$uitable->addModal ( "ruangan", "hidden", "", "operasi" );

$modal = $uitable->getModal ();
$form = $modal->getForm ();

$button = new Button ( "", "", "" );
$button->setIsButton ( Button::$ICONIC );
$button->setIcon ( "fa fa-refresh ");
$button->setClass( " btn-primary ");
$button->setAction ( "lap_operasi_kamar.view()" );
$form->addElement ( "", $button );

echo $form->getHtml ();

echo $uitable->getHtml ();
echo addJS ( "framework/smis/js/table_action.js" );
echo addJS ( "framework/bootstrap/js/bootstrap-datepicker.js" );
echo addCSS ( "framework/bootstrap/css/datepicker.css" );

?>
<script type="text/javascript">
var lap_operasi_kamar;
//var employee;
$(document).ready(function(){
	$('.mydate').datepicker();
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
	var column=new Array('id','nama','keterangan','slug');
	lap_operasi_kamar=new TableAction("lap_operasi_kamar","medical_record","lap_operasi_kamar",column);
	lap_operasi_kamar.getRegulerData=function(){
		var reg_data={	
				page:this.page,
				action:this.action,
				super_command:this.super_command,
				prototype_name:this.prototype_name,
				prototype_slug:this.prototype_slug,
				prototype_implement:this.prototype_implement,
				dari:$("#"+this.prefix+"_dari").val(),
				sampai:$("#"+this.prefix+"_sampai").val()
				};
		return reg_data;
	};
	lap_operasi_kamar.view();
	
});
</script>
