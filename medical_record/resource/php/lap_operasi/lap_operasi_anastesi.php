<?php
setChangeCookie ( false );
$head=array ('Tanggal','Noreg','Nama',"Alamat","L","P","LA","RA","TIVA","INHALASI","INTUB","Dokter","Asisten","Diagnosa" );
$uitable = new Table ( $head, "", NULL, false );
$uitable->setName ( "lap_operasi_anastesi" );
$uitable->addHeader ( "before", "<tr>
								<th rowspan='3'>Tanggal</th>
								<th rowspan='3'>Noreg</th>
								<th rowspan='3'>Nama</th>
								<th rowspan='3'>Alamat</th>
								<th colspan='2'>Umur</th>
								<th colspan='5'>Jenis Anastesi</th>
								<th colspan='2'>Petugas</th>
								<th rowspan='3'>Diagnosa</th>
							</tr>" );
$uitable->addHeader ( "before", "<tr>
								<th rowspan='2'>L</th>
								<th rowspan='2'>P</th>
								<th rowspan='2'>LA</th>
								<th rowspan='2'>RA</th>
								<th colspan='3'>GA</th>
								<th rowspan='2'>Dokter</th>
								<th rowspan='2'>Asisten</th>
								</tr>" );
$uitable->addHeader ( "before", "<tr>
								<th>TIVA</th>
								<th>Inhalasi</th>
								<th>Intub</th>
								</tr>" );
$uitable->setHeaderVisible ( false );
class AnastesiOperasiAdapter extends ArrayAdapter {
	public function adapt($d) {
		$a = array ();
		$this->number ++;
		$a ['Nomor'] = $this->number;
		$a ['id'] = $d->id;
		$a ['Tanggal'] = self::format ( "date d M Y", $d->tanggal_mulai );
		$a ['Noreg'] = self::format ( "digit6", $d->noreg_pasien );
		$a ['Nama'] = $d->nama_pasien;
		$a ['Alamat'] = $d->alamat;
		$a ['L'] = $d->jk == "1" ? $d->umur : "";
		$a ['P'] = $d->jk == "0" ? $d->umur : "";
		$a ['RA'] = $d->anastesi == "Regional Anastesi" ? "x" : "";
		$a ['LA'] = $d->anastesi == "Local Anastesi" ? "x" : "";
		$a ['TIVA'] = $d->anastesi == "General Anastesi (TIVA)" ? "x" : "";
		$a ['INHALASI'] = $d->anastesi == "General Anastesi (Inhalasi)" ? "x" : "";
		$a ['INTUB'] = $d->anastesi == "General Anastesi (Intub)" ? "x" : "";
		$a ['Dokter'] = $d->dokter_anastesi;
		$a ['Asisten'] = $d->asisten_anastesi;
		$a ['Diagnosa'] = $d->diagnosa;
		return $a;
	}
}

if (isset ( $_POST ['command'] )) {
	$adapter = new AnastesiOperasiAdapter ();
	$dbtable = new DBTable ( $db, "smis_mr_operasi" );
	if (isset ( $_POST ['dari'] ) && isset ( $_POST ['sampai'] ) && $_POST ['dari'] != "" && $_POST ['sampai'] != "") {
		$dari = $_POST ['dari'];
		$sampai = $_POST ['sampai'];
		$dbtable->addCustomKriteria ( "'" . $dari . "'", "<=tanggal_mulai" );
		$dbtable->addCustomKriteria ( "'" . $sampai . "'", ">=tanggal_mulai" );
	}
	$dbtable->setOrder ( " tanggal_mulai ASC " );
	$dbres = new DBResponder ( $dbtable, $uitable, $adapter );
	$data = $dbres->command ( $_POST ['command'] );
	echo json_encode ( $data );
	return;
}

$uitable->addModal ( "dari", "date", "Dari", "" );
$uitable->addModal ( "sampai", "date", "Sampai", "" );
$uitable->addModal ( "ruangan", "hidden", "", "operasi" );

$modal = $uitable->getModal ();
$form = $modal->getForm ();

$button = new Button ( "", "", "" );
$button->setIsButton ( Button::$ICONIC );
$button->setIcon ( "fa fa-refresh ");
$button->setClass( " btn-primary ");
$button->setAction ( "lap_operasi_anastesi.view()" );
$form->addElement ( "", $button );

echo $form->getHtml ();
echo $uitable->getHtml ();
echo addJS ( "framework/smis/js/table_action.js" );
echo addJS ( "framework/bootstrap/js/bootstrap-datepicker.js" );
echo addCSS ( "framework/bootstrap/css/datepicker.css" );

?>
<script type="text/javascript">
var lap_operasi_anastesi;
//var employee;
$(document).ready(function(){
	$('.mydate').datepicker();
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
	var column=new Array('id','nama','keterangan','slug');
	lap_operasi_anastesi=new TableAction("lap_operasi_anastesi","medical_record","lap_operasi_anastesi",column);
	lap_operasi_anastesi.getRegulerData=function(){
		var reg_data={	
				page:this.page,
				action:this.action,
				super_command:this.super_command,
				prototype_name:this.prototype_name,
				prototype_slug:this.prototype_slug,
				prototype_implement:this.prototype_implement,
				dari:$("#"+this.prefix+"_dari").val(),
				sampai:$("#"+this.prefix+"_sampai").val()
				};
		return reg_data;
	};
	lap_operasi_anastesi.view();
	
});
</script>
