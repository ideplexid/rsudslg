<?php

global $db;
require_once ("smis-base/smis-include-service-consumer.php");
require_once "medical_record/class/service/RuanganService.php";

$header = array("tanggal",
                "ivl",
                "cvl",
                "uc",
                "ett",
                "pasien_tirah_baring",
                "luka_bersih",
                "luka_kotor",
                "iadp",
                "vap",
                "isk",
                "plebitis",
                "decubitus",
                "ido"
                );
                
$uitable = new Table ( $header, "", NULL, false );
$uitable->setName ( "laporan_ppi_harian_per_ruang" );

$uitable->addHeader ( "before", "<tr>
                                    <th rowspan='2'>TANGGAL</th>
                                    <th colspan='4'>PEMAKAIAN ALAT</th>
                                    <th rowspan='2'>PASIEN TIRAH BARING</th>
                                    <th colspan='2'>JUMLAH PASIEN OPERASI</th>
                                    <th colspan='6'>INSIDEN RATE</th>
                                </tr>" );
                                
$uitable->addHeader ( "before", "<tr>
                                    <th>IVL</th>
                                    <th>CVL</th>
                                    <th>UC</th>
                                    <th>ETT</th>
                                    <th>LUKA BERSIH</th>
                                    <th>LUKA KOTOR</th>
                                    <th>IADP</th>
                                    <th>VAP</th>
                                    <th>ISK</th>
                                    <th>PLEBITIS</th>
                                    <th>DEKUBITUS</th>
                                    <th>IDO</th>
								</tr>" );
                                
$uitable->setHeaderVisible ( false );
$uitable->setFooterVisible( false );

if (isset ( $_POST ['command'] )) {
    /****** Get Jenis Perawatan *********/
    if($_POST['ruang'] != "") {
        $query_jenis_perawatan = "SELECT * FROM smis_mr_jenis_perawatan WHERE ruangan = '".$_POST['ruang']."' AND tanggal >='".$_POST['dari']."' AND tanggal <= '".$_POST['sampai']."' AND prop != 'del';";
    } else {
        $query_jenis_perawatan = "SELECT * FROM smis_mr_jenis_perawatan WHERE tanggal >='".$_POST['dari']."' AND tanggal <= '".$_POST['sampai']."'  AND prop != 'del';";
    }
    $jenis_perawatan = $db->get_result($query_jenis_perawatan);
    /****** *********/
    
    /****** Get Infeksi *********/
    if($_POST['ruang'] != "") {
        $query_infeksi = "SELECT * FROM smis_mr_infeksi WHERE ruangan = '".$_POST['ruang']."' AND tanggal >='".$_POST['dari']."' AND tanggal <= '".$_POST['sampai']."'  AND prop != 'del';";
    } else {
        $query_infeksi = "SELECT * FROM smis_mr_infeksi WHERE tanggal >='".$_POST['dari']."' AND tanggal <= '".$_POST['sampai']."'  AND prop != 'del';";
    }
    $infeksi = $db->get_result($query_infeksi);
    /****** *********/
    
    require_once "medical_record/class/adapter/LaporanPPIHarianPerRuangAdapter.php";
    $adapter = new LaporanPPIHarianPerRuangAdapter();
    $adapter->setDataJenisPerawatan($jenis_perawatan);
    $adapter->setDataInfeksi($infeksi);
    $adapter->setDariSampai($_POST['dari'], $_POST['sampai']);
    $ready = $adapter->getContent();
    
    if($_POST['command'] == 'list') {
        $uitable->setContent($ready);
        $content['list'] = $uitable->getBodyContent();
        $response = new ResponsePackage ();
        $response->setAlertVisible ( false );
        $response->setStatus ( ResponsePackage::$STATUS_OK );
        $response->setContent($content);
        echo json_encode ( $response->getPackage () );
        return;
                
        $dbres = new DBResponder ( $dbtable, $uitable, $adapter );
        $data = $dbres->command ( $_POST ['command'] );
        echo json_encode ( $data );
        return;
    } else if($_POST['command'] == 'excel') {
        require_once ("smis-libs-out/php-excel/PHPExcel.php");
        $file = new PHPExcel ();
        $file->getProperties ()->setCreator ( "Rumah Sakit" );
        $file->getProperties ()->setLastModifiedBy ( "Rumah Sakit Bagian Administrator" );
        $file->getProperties ()->setTitle ( "Laporan PPI Harian Per Ruang" );
        $file->getProperties ()->setSubject ( "Laporan PPI Harian Per Ruang" );
        $file->getProperties ()->setDescription ( "Laporan PPI Harian Per Ruang Generated From system" );
        $file->getProperties ()->setKeywords ( "Laporan PPI Harian Per Ruang" );
        $file->getProperties ()->setCategory ( "Laporan PPI Harian Per Ruang" );
        
        $sheet = $file->getActiveSheet ();
        $i = 1;
        
        $objDrawing = new PHPExcel_Worksheet_Drawing();
        $objDrawing->setName('logo_img');
        $objDrawing->setDescription('logo_img');
        $objDrawing->setPath(getLogoNonInterlaced());
        $objDrawing->setCoordinates('A'.$i);
        $objDrawing->setOffsetX(20); 
        $objDrawing->setOffsetY(5);
        $objDrawing->setWidth(100); 
        $objDrawing->setHeight(100);
        $objDrawing->setWorksheet($sheet);
        $i++;
        
        $sheet->setCellValue("C".$i, getSettings($db,"smis_autonomous_title"));
        $sheet->getStyle("C".$i)->getFont()->setBold(true)->setSize(14);
        $i++;
        $sheet->setCellValue("C".$i, getSettings($db,"smis_autonomous_address"));
        $sheet->getStyle("C".$i)->getFont()->setBold(true)->setSize(14);
        $i++;
        $sheet->setCellValue("C".$i, getSettings($db,"smis_autonomous_contact"));
        $sheet->getStyle("C".$i)->getFont()->setBold(true)->setSize(14);
        $i += 3;
        $sheet->mergeCells("A".$i.":P".$i)->setCellValue("A".$i,"REKAP FORMULIR HARIAN");
        $sheet->getStyle("A".$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle("A".$i)->getFont()->setBold(true)->setSize(14);
        $i++;
        $sheet->mergeCells("A".$i.":P".$i)->setCellValue("A".$i, strtoupper(ArrayAdapter::dateFormat("date d M Y", $_POST['dari']))." - ".strtoupper(ArrayAdapter::dateFormat("date d M Y", $_POST['sampai'])));
        $sheet->getStyle("A".$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle("A".$i)->getFont()->setBold(true);
        $i += 3;
        
        $sheet->setCellValue("A".$i, "RUANG");
        $sheet->setCellValue("B".$i, ":");
        $sheet->setCellValue("C".$i, ArrayAdapter::slugFormat("unslug", $_POST['ruang']));
        $i += 2;
        
        $border_start = $i;
        $ips = $i + 1;
        $sheet->mergeCells("A".$i.":A".$ips)->setCellValue("A".$i, "TANGGAL");
        $sheet->mergeCells("B".$i.":E".$i)->setCellValue("B".$i, "PEMAKAIAN ALAT");
        $sheet->mergeCells("F".$i.":F".$ips)->setCellValue("F".$i, "PASIEN TIRAH BARING");
        $sheet->mergeCells("G".$i.":H".$i)->setCellValue("G".$i, "JUMLAH PASIEN OPERASI");
        $sheet->mergeCells("I".$i.":N".$i)->setCellValue("I".$i, "INSIDEN RATE");
        $sheet->getStyle("A".$i.":N".$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle("A".$i.":N".$i)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $sheet->getStyle("A".$i.":N".$i)->getFont()->setBold(true);
        $i++;
        
        $sheet->setCellValue("B".$i, "IVL");
        $sheet->setCellValue("C".$i, "CVL");
        $sheet->setCellValue("D".$i, "UC");
        $sheet->setCellValue("E".$i, "ETT");
        $sheet->setCellValue("G".$i, "LUKA BERSIH");
        $sheet->setCellValue("H".$i, "LUKA KOTOR");
        $sheet->setCellValue("I".$i, "IADP");
        $sheet->setCellValue("J".$i, "VAP");
        $sheet->setCellValue("K".$i, "ISK");
        $sheet->setCellValue("L".$i, "PLEBITIS");
        $sheet->setCellValue("M".$i, "DECUBITIS");
        $sheet->setCellValue("N".$i, "IDO");
        $sheet->getStyle("A".$i.":N".$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle("A".$i.":N".$i)->getFont()->setBold(true);
        $i++;
        
        foreach($ready as $x) {
            $sheet->setCellValue("A".$i, $x["tanggal"]);
            $sheet->setCellValue("B".$i, $x["ivl"]);
            $sheet->setCellValue("C".$i, $x["cvl"]);
            $sheet->setCellValue("D".$i, $x["uc"]);
            $sheet->setCellValue("E".$i, $x["ett"]);
            $sheet->setCellValue("F".$i, $x["pasien_tirah_baring"]);
            $sheet->setCellValue("G".$i, $x["luka_bersih"]);
            $sheet->setCellValue("H".$i, $x["luka_kotor"]);
            $sheet->setCellValue("I".$i, $x["iadp"]);
            $sheet->setCellValue("J".$i, $x["vap"]);
            $sheet->setCellValue("K".$i, $x["isk"]);
            $sheet->setCellValue("L".$i, $x["plebitis"]);
            $sheet->setCellValue("M".$i, $x["decubitus"]);
            $sheet->setCellValue("N".$i, $x["ido"]);
            $sheet->getStyle("A".$i.":O".$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $i++;
        }
        $border_end = $i - 1;
        $thin = array ();
        $thin['borders']=array();
        $thin['borders']['allborders']=array();
        $thin['borders']['allborders']['style']=PHPExcel_Style_Border::BORDER_THIN ;
        $sheet  ->getStyle ( "A".$border_start.":N".$border_end )->applyFromArray ($thin);
        
        $filename = "REKAP FORMULIR HARIAN ".strtoupper(ArrayAdapter::dateFormat("date d M Y", $_POST['dari']))." - ".strtoupper(ArrayAdapter::dateFormat("date d M Y", $_POST['sampai'])).".xls";
        
        header ( 'Content-Type: application/vnd.ms-excel' );
        header ( 'Content-Disposition: attachment;filename="'.$filename.'"' );
        header ( 'Cache-Control: max-age=0' );
        $writer = PHPExcel_IOFactory::createWriter ( $file, 'Excel5' );
        $writer->save ( 'php://output' );
		return;
    }
}

$uitable->addModal( "dari", "datetime", "Dari", "" );
$uitable->addModal( "sampai", "datetime", "Sampai", "" );
$service = new RuanganService ( $db );
$service->execute ();
$ruangan = $service->getContent ();
$option = array();
$option['value'] = '';
$option['name'] = '- SEMUA -';
$ruangan[] = $option;
$uitable->addModal( "ruang", "select", "Ruangan", $ruangan );

$modal = $uitable->getModal ();
$form=$modal->getForm();

$v = new Button("", "", "");
$v->addClass("btn-primary")
		->setIsButton(Button::$ICONIC)
		->setIcon("fa fa-refresh")
		->setAction("laporan_ppi_harian_per_ruang.view()");
$form->addElement("", $v);

$excel = new Button("", "", "");
$excel->addClass("btn-primary")
		->setIsButton(Button::$ICONIC)
		->setIcon("fa fa-file-excel-o")
		->setAction("laporan_ppi_harian_per_ruang.excel()");
$form->addElement("", $excel);

echo "<h2><strong>REKAP FORMULIR HARIAN</h2>";
echo $form->getHtml();
echo $uitable->getHtml ();

echo addJS ( "framework/smis/js/table_action.js" );
echo addJS ( "framework/bootstrap/js/bootstrap-datetimepicker.js" );
echo addCSS ( "framework/bootstrap/css/bootstrap-datetimepicker.css" );

?>

<script>

var laporan_ppi_harian_per_ruang;

$(document).ready(function(){
	$(".mydatetime").datetimepicker({ minuteStep: 1});
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
	laporan_ppi_harian_per_ruang=new TableAction("laporan_ppi_harian_per_ruang","medical_record","laporan_ppi_harian_per_ruang",new Array());
	laporan_ppi_harian_per_ruang.getRegulerData=function(){
		var reg_data={	
				page:this.page,
				action:this.action,
				super_command:this.super_command,
				prototype_name:this.prototype_name,
				prototype_slug:this.prototype_slug,
				prototype_implement:this.prototype_implement,
				dari:$("#"+this.prefix+"_dari").val(),
				sampai:$("#"+this.prefix+"_sampai").val(),
				ruang:$("#"+this.prefix+"_ruang").val()
				};
		return reg_data;
	};
	
});

</script>