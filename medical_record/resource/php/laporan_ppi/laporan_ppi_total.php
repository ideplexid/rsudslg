<?php

global $db;
require_once ("smis-base/smis-include-service-consumer.php");
require_once "medical_record/class/service/RuanganService.php";

$header = array("no",
                "ruangan",
                "ivl",
                "cvl",
                "uc",
                "ett",
                "pasien_tirah_baring",
                "iadp",
                "vap",
                "isk",
                "plebitis",
                "decubitus",
                "ir_iadp",
                "ir_vap",
                "ir_isk",
                "ir_plebitis",
                "ir_decubitus"
                );
                
$uitable = new Table ( $header, "", NULL, false );
$uitable->setName ( "laporan_ppi_total" );

$uitable->addHeader ( "before", "<tr>
                                    <th rowspan='2'>NO</th>
                                    <th rowspan='2'>RUANGAN</th>
                                    <th colspan='4'>PEMAKAIAN ALAT</th>
                                    <th rowspan='2'>PASIEN TIRAH BARING</th>
                                    <th colspan='5'>INSIDEN RATE</th>
                                    <th colspan='5'>INDEX RATE</th>
                                </tr>" );
                                
$uitable->addHeader ( "before", "<tr>
                                    <th>IVL</th>
                                    <th>CVL</th>
                                    <th>UC</th>
                                    <th>ETT</th>
                                    <th>IADP</th>
                                    <th>VAP</th>
                                    <th>ISK</th>
                                    <th>PLEBITIS</th>
                                    <th>DEKUBITUS</th>
                                    <th>IADP</th>
                                    <th>VAP</th>
                                    <th>ISK</th>
                                    <th>PLEBITIS</th>
                                    <th>DEKUBITUS</th>
								</tr>" );
                                
$uitable->setHeaderVisible ( false );
$uitable->setFooterVisible( false );

if (isset ( $_POST ['command'] )) {
    /****** Get Jenis Perawatan *********/
    $query_jenis_perawatan = "
                            SELECT ruangan,
                            SUM(IF(jenis_perawatan='IVL',1,0)) as ivl,
                            SUM(IF(jenis_perawatan='CVL',1,0)) as cvl,
                            SUM(IF(jenis_perawatan='UC',1,0)) as uc,
                            SUM(IF(jenis_perawatan='ETT',1,0)) as ett,
                            SUM(IF(jenis_perawatan='Pasien Tirah Baring',1,0)) as pasien_tirah_baring,
                            SUM(IF(jenis_luka='Luka Bersih',1,0)) as luka_bersih,
                            SUM(IF(jenis_luka='Luka Kotor',1,0)) as luka_kotor
                            FROM smis_mr_jenis_perawatan
                            WHERE tanggal >= '".$_POST['dari']."' AND tanggal <='".$_POST['sampai']."'  AND prop != 'del'
                            GROUP BY ruangan;
                            ";
    $jenis_perawatan = $db->get_result($query_jenis_perawatan);
    /****** *********/
    
    /****** Get Infeksi *********/
    $query_infeksi = "
                    SELECT ruangan,
                    SUM(IF(jenis_infeksi='IADP',1,0)) as iadp,
                    SUM(IF(jenis_infeksi='VAP',1,0)) as vap,
                    SUM(IF(jenis_infeksi='ISK',1,0)) as isk,
                    SUM(IF(jenis_infeksi='Plebitis',1,0)) as plebitis,
                    SUM(IF(jenis_infeksi='Decubitus',1,0)) as decubitus,
                    SUM(IF(jenis_infeksi='IDO',1,0)) as ido
                    FROM smis_mr_infeksi
                    WHERE tanggal >= '".$_POST['dari']."' AND tanggal <='".$_POST['sampai']."'  AND prop != 'del'
                    GROUP BY ruangan;
                    ";
    $infeksi = $db->get_result($query_infeksi);
    /****** *********/
    
    /****** Get Ruangan *********/
    $service = new RuanganService ( $db );
    $service->execute ();
    $ruangan = $service->getContent ();
    /****** *********/
    
    require_once "medical_record/class/adapter/LaporanPPITotalAdapter.php";
    $adapter = new LaporanPPITotalAdapter();
    $adapter->setDataJenisPerawatan($jenis_perawatan);
    $adapter->setDataInfeksi($infeksi);
    $adapter->setDataRuangan($ruangan);
    $ready = $adapter->getContent();
    
    if($_POST['command'] == 'list') {
        $uitable->setContent($ready);
        $content['list'] = $uitable->getBodyContent();
        $response = new ResponsePackage ();
        $response->setAlertVisible ( false );
        $response->setStatus ( ResponsePackage::$STATUS_OK );
        $response->setContent($content);
        echo json_encode ( $response->getPackage () );
        return;
                
        $dbres = new DBResponder ( $dbtable, $uitable, $adapter );
        $data = $dbres->command ( $_POST ['command'] );
        echo json_encode ( $data );
        return;
    } else {
        require_once ("smis-libs-out/php-excel/PHPExcel.php");
        $file = new PHPExcel ();
        $file->getProperties ()->setCreator ( "Rumah Sakit" );
        $file->getProperties ()->setLastModifiedBy ( "Rumah Sakit Bagian Administrator" );
        $file->getProperties ()->setTitle ( "Rekap Hasil Surveilan" );
        $file->getProperties ()->setSubject ( "Rekap Hasil Surveilan" );
        $file->getProperties ()->setDescription ( "Rekap Hasil Surveilan Generated From system" );
        $file->getProperties ()->setKeywords ( "Rekap Hasil Surveilan" );
        $file->getProperties ()->setCategory ( "Rekap Hasil Surveilan" );
        
        $sheet = $file->getActiveSheet ();
        $i = 1;
        
        $objDrawing = new PHPExcel_Worksheet_Drawing();
        $objDrawing->setName('logo_img');
        $objDrawing->setDescription('logo_img');
        $objDrawing->setPath(getLogoNonInterlaced());
        $objDrawing->setCoordinates('A'.$i);
        $objDrawing->setOffsetX(20); 
        $objDrawing->setOffsetY(5);
        $objDrawing->setWidth(100); 
        $objDrawing->setHeight(100);
        $objDrawing->setWorksheet($sheet);
        $i++;
        
        $sheet->setCellValue("C".$i, getSettings($db,"smis_autonomous_title"));
        $sheet->getStyle("C".$i)->getFont()->setBold(true)->setSize(14);
        $i++;
        $sheet->setCellValue("C".$i, getSettings($db,"smis_autonomous_address"));
        $sheet->getStyle("C".$i)->getFont()->setBold(true)->setSize(14);
        $i++;
        $sheet->setCellValue("C".$i, getSettings($db,"smis_autonomous_contact"));
        $sheet->getStyle("C".$i)->getFont()->setBold(true)->setSize(14);
        $i += 3;
        $sheet->mergeCells("A".$i.":P".$i)->setCellValue("A".$i,"REKAP HASIL SURVEILAN");
        $sheet->getStyle("A".$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle("A".$i)->getFont()->setBold(true)->setSize(14);
        $i++;
        $sheet->mergeCells("A".$i.":P".$i)->setCellValue("A".$i, strtoupper(ArrayAdapter::dateFormat("date d M Y", $_POST['dari']))." - ".strtoupper(ArrayAdapter::dateFormat("date d M Y", $_POST['sampai'])));
        $sheet->getStyle("A".$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle("A".$i)->getFont()->setBold(true);
        $i += 3;
        
        $border_start = $i;
        $ips = $i + 1;
        $sheet->mergeCells("A".$i.":A".$ips)->setCellValue("A".$i, "NO");
        $sheet->mergeCells("B".$i.":B".$ips)->setCellValue("B".$i, "RUANGAN");
        $sheet->mergeCells("C".$i.":F".$i)->setCellValue("C".$i, "PEMAKAIAN ALAT");
        $sheet->mergeCells("G".$i.":G".$ips)->setCellValue("G".$i, "PASIEN TIRAH BARING");
        $sheet->mergeCells("H".$i.":L".$i)->setCellValue("H".$i, "INSIDEN RATE");
        $sheet->mergeCells("M".$i.":Q".$i)->setCellValue("M".$i, "INDEX RATE");
        $sheet->getStyle("A".$i.":Q".$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle("A".$i.":Q".$i)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $sheet->getStyle("A".$i.":Q".$i)->getFont()->setBold(true);
        $i++;
        
        $sheet->setCellValue("C".$i, "IVL");
        $sheet->setCellValue("D".$i, "CVL");
        $sheet->setCellValue("E".$i, "UC");
        $sheet->setCellValue("F".$i, "ETT");
        $sheet->setCellValue("H".$i, "IADP");
        $sheet->setCellValue("I".$i, "VAP");
        $sheet->setCellValue("J".$i, "ISK");
        $sheet->setCellValue("K".$i, "PLEBITIS");
        $sheet->setCellValue("L".$i, "DECUBITIS");
        $sheet->setCellValue("M".$i, "IADP");
        $sheet->setCellValue("N".$i, "VAP");
        $sheet->setCellValue("O".$i, "ISK");
        $sheet->setCellValue("P".$i, "PLEBITIS");
        $sheet->setCellValue("Q".$i, "DECUBITIS");
        $sheet->getStyle("A".$i.":Q".$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle("A".$i.":Q".$i)->getFont()->setBold(true);
        $i++;
        
        foreach($ready as $x) {
            $sheet->setCellValue("A".$i, $x["no"]);
            $sheet->setCellValue("B".$i, $x["ruangan"]);
            $sheet->setCellValue("C".$i, $x["ivl"]);
            $sheet->setCellValue("D".$i, $x["cvl"]);
            $sheet->setCellValue("E".$i, $x["uc"]);
            $sheet->setCellValue("F".$i, $x["ett"]);
            $sheet->setCellValue("G".$i, $x["pasien_tirah_baring"]);
            $sheet->setCellValue("H".$i, $x["iadp"]);
            $sheet->setCellValue("I".$i, $x["vap"]);
            $sheet->setCellValue("J".$i, $x["isk"]);
            $sheet->setCellValue("K".$i, $x["plebitis"]);
            $sheet->setCellValue("L".$i, $x["decubitus"]);
            $sheet->setCellValue("M".$i, $x["ir_iadp"]);
            $sheet->setCellValue("N".$i, $x["ir_vap"]);
            $sheet->setCellValue("O".$i, $x["ir_isk"]);
            $sheet->setCellValue("P".$i, $x["ir_plebitis"]);
            $sheet->setCellValue("Q".$i, $x["ir_decubitus"]);
            $sheet->getStyle("A".$i.":Q".$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $i++;
        }
        
        $border_end = $i - 1;
        $thin = array ();
        $thin['borders']=array();
        $thin['borders']['allborders']=array();
        $thin['borders']['allborders']['style']=PHPExcel_Style_Border::BORDER_THIN ;
        $sheet  ->getStyle ( "A".$border_start.":Q".$border_end )->applyFromArray ($thin);
        
        $filename = "REKAP HASIL SURVEILAN ".strtoupper(ArrayAdapter::dateFormat("date d M Y", $_POST['dari']))." - ".strtoupper(ArrayAdapter::dateFormat("date d M Y", $_POST['sampai'])).".xls";
        
        header ( 'Content-Type: application/vnd.ms-excel' );
        header ( 'Content-Disposition: attachment;filename="'.$filename.'"' );
        header ( 'Cache-Control: max-age=0' );
        $writer = PHPExcel_IOFactory::createWriter ( $file, 'Excel5' );
        $writer->save ( 'php://output' );
		return;
    }
}

$uitable->addModal( "dari", "datetime", "Dari", "" );
$uitable->addModal( "sampai", "datetime", "Sampai", "" );

$modal = $uitable->getModal ();
$form=$modal->getForm();

$v = new Button("", "", "");
$v->addClass("btn-primary")
		->setIsButton(Button::$ICONIC)
		->setIcon("fa fa-refresh")
		->setAction("laporan_ppi_total.view()");
$form->addElement("", $v);

$excel = new Button("", "", "");
$excel->addClass("btn-primary")
		->setIsButton(Button::$ICONIC)
		->setIcon("fa fa-file-excel-o")
		->setAction("laporan_ppi_total.excel()");
$form->addElement("", $excel);

echo "<h2><strong>REKAP HASIL SURVEILAN</h2>";
echo $form->getHtml();
echo $uitable->getHtml ();

echo addJS ( "framework/smis/js/table_action.js" );
echo addJS ( "framework/bootstrap/js/bootstrap-datetimepicker.js" );
echo addCSS ( "framework/bootstrap/css/bootstrap-datetimepicker.css" );

?>

<script>

var laporan_ppi_total;

$(document).ready(function(){
	$(".mydatetime").datetimepicker({ minuteStep: 1});
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
	laporan_ppi_total=new TableAction("laporan_ppi_total","medical_record","laporan_ppi_total",new Array());
	laporan_ppi_total.getRegulerData=function(){
		var reg_data={	
				page:this.page,
				action:this.action,
				super_command:this.super_command,
				prototype_name:this.prototype_name,
				prototype_slug:this.prototype_slug,
				prototype_implement:this.prototype_implement,
				dari:$("#"+this.prefix+"_dari").val(),
				sampai:$("#"+this.prefix+"_sampai").val()
				};
		return reg_data;
	};
	
});

</script>