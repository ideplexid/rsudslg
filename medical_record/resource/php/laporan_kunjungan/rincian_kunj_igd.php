<?php

global $db;

$header = array("no", 
                "tanggal",
                "nrm",
                "noreg",
                "profile_number",
                "nama_pasien",
                "bedah_rujukan_l", 
                "bedah_rujukan_p",
                "bedah_nonrujukan_l", 
                "bedah_nonrujukan_p",
                "bedah_doa_l",
                "bedah_doa_p",
                "bedah_dor_l",
                "bedah_dor_p",
                "nonbedah_rujukan_l",
                "nonbedah_rujukan_p",
                "nonbedah_nonrujukan_l", 
                "nonbedah_nonrujukan_p",
                "nonbedah_doa_l",
                "nonbedah_doa_p",
                "nonbedah_dor_l",
                "nonbedah_dor_p",
                "kebidanan_rujukan_l",
                "kebidanan_rujukan_p",
                "kebidanan_nonrujukan_l", 
                "kebidanan_nonrujukan_p",
                "kebidanan_doa_l",
                "kebidanan_doa_p",
                "kebidanan_dor_l",
                "kebidanan_dor_p"
                );
                
$uitable = new Table ( $header, "", NULL, false );
$uitable->setName ( "rincian_kunj_igd" );

$uitable->addHeader ( "before", "<tr>
                                    <th rowspan='3'>NO.</th>
                                    <th rowspan='3'>TANGGAL</th>
                                    <th rowspan='3'>NRM</th>
                                    <th rowspan='3'>NO. REGISTER</th>
                                    <th rowspan='3'>NO. PROFILE</th>
                                    <th rowspan='3'>NAMA</th>
                                    <th colspan='4'>BEDAH</th>
                                    <th colspan='4'>MENINGGAL</th>
                                    <th colspan='4'>NON BEDAH</th>
                                    <th colspan='4'>MENINGGAL</th>
                                    <th colspan='4'>KEBIDANAN</th>
                                    <th colspan='4'>MENINGGAL</th>
                                </tr>" );
                                
$uitable->addHeader ( "before", "<tr>
                                    <th colspan='2'>RUJUKAN</th>
                                    <th colspan='2'>NON RUJUKAN</th>
                                    <th colspan='2'>DOA</th>
                                    <th colspan='2'>DOR</th>
                                    <th colspan='2'>RUJUKAN</th>
                                    <th colspan='2'>NON RUJUKAN</th>
                                    <th colspan='2'>DOA</th>
                                    <th colspan='2'>DOR</th>
                                    <th colspan='2'>RUJUKAN</th>
                                    <th colspan='2'>NON RUJUKAN</th>
                                    <th colspan='2'>DOA</th>
                                    <th colspan='2'>DOR</th>
								</tr>" );
                                
$uitable->addHeader ( "before", "<tr>
                                    <th>L</th>
                                    <th>P</th>
                                    <th>L</th>
                                    <th>P</th>
                                    <th>L</th>
                                    <th>P</th>
                                    <th>L</th>
                                    <th>P</th>
                                    <th>L</th>
                                    <th>P</th>
                                    <th>L</th>
                                    <th>P</th>
                                    <th>L</th>
                                    <th>P</th>
                                    <th>L</th>
                                    <th>P</th>
                                    <th>L</th>
                                    <th>P</th>
                                    <th>L</th>
                                    <th>P</th>
                                    <th>L</th>
                                    <th>P</th>
                                    <th>L</th>
                                    <th>P</th>
								</tr>" );
                                
$uitable->setHeaderVisible ( false );
$uitable->setFooterVisible( false );

if (isset ( $_POST ['command'] )) {
    $dbtable = new DBTable ( $db, "smis_mr_igd");
    if(isset($_POST['dari']) && $_POST['dari']!="") {
        $dbtable->addCustomKriteria(""," tanggal >= '".$_POST['dari']."' ");
    }
    if(isset($_POST['sampai']) && $_POST['sampai']!="") {
        $dbtable->addCustomKriteria(""," tanggal <= '".$_POST['sampai']."' ");
    }
    $dbtable->setOrder(" tanggal ASC");	
    $dbtable->setShowAll(true);
    
    require_once "medical_record/class/adapter/RincianKunjIGDAdapter.php";
	$adapter = new RincianKunjIGDAdapter();
    
    require_once "medical_record/class/responder/RincianKunjIGDResponder.php";
    $dbres = new RincianKunjIGDResponder($dbtable, $uitable, $adapter);
	$hasil = $dbres->command($_POST['command']);
	echo json_encode($hasil);
	return;
}

$uitable->clearContent();
$uitable->addModal("dari", "date", "Dari", "")
		->addModal("sampai", "date", "Sampai", "");
        
$view=new Button("","","View");
$view->setClass("btn-primary")
		->setIsButton(Button::$ICONIC)
		->setIcon("fa fa-refresh")
		->setAction("rincian_kunj_igd.view()");
        
$excel=new Button("","","Excel");
$excel->setClass("btn-primary")
	->setIsButton(Button::$ICONIC)
	->setIcon("fa fa-file-excel-o")
	->setAction("rincian_kunj_igd.excel()");

$btng=new ButtonGroup("");
$btng->addButton($view);
$btng->addButton($excel);

$form=$uitable
	  ->getModal()
	  ->setTitle("Medical Record")
	  ->getForm()
	  ->addElement("",$btng);
      
echo "<h2>Rincian Kunjungan IGD</h2>";
echo $form->getHtml();
echo $uitable->getHtml();

echo addJS("framework/bootstrap/js/bootstrap-datepicker.js");
echo addJS("framework/smis/js/table_action.js");
echo addCSS("framework/bootstrap/css/datepicker.css");

?>

<script>

var rincian_kunj_igd;

$(document).ready(function(){
	$('.mydate').datepicker();
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
	rincian_kunj_igd=new TableAction("rincian_kunj_igd","medical_record","rincian_kunj_igd",new Array());
	rincian_kunj_igd.getRegulerData=function(){
		var reg_data={	
				page:this.page,
				action:this.action,
				super_command:this.super_command,
				prototype_name:this.prototype_name,
				prototype_slug:this.prototype_slug,
				prototype_implement:this.prototype_implement,
				dari:$("#"+this.prefix+"_dari").val(),
				sampai:$("#"+this.prefix+"_sampai").val()
				};
		return reg_data;
	};
	
});

</script>