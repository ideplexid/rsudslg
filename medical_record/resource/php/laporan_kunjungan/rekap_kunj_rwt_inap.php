<?php

global $db;
require_once "smis-base/smis-include-service-consumer.php";
require_once "medical_record/function/date_range.php";

$slug_igd = getSettings($db, "smis-mr-slug-igd", "");

$header = array("jumlah_tt", "masuk_l", "masuk_p", "keluar_hidup_l","keluar_hidup_p", "keluar_mati_l", "keluar_mati_p", "mati_k48_l", "mati_k48_p", "mati_l48_l", "mati_l48_p", "jumlah_lama_dirawat", "jumlah_hari_perawatan");

$uitable = new Table ( $header, "", NULL, false );
$uitable->setName ( "rekap_kunj_rwt_inap" );

$uitable->addHeader ( "before", "<tr>
                                    <th rowspan='2'>JUMLAH TEMPAT TIDUR</th>
                                    <th colspan='2'>MASUK RUMAH SAKIT</th>
                                    <th colspan='2'>KELUAR HIDUP</th>
                                    <th colspan='2'>KELUAR MATI</th>
                                    <th colspan='2'>PASIEN MATI <48 JAM</th>
                                    <th colspan='2'>PASIEN MATI >48 JAM</th>
                                    <th rowspan='2'>JUMLAH LAMA DIRAWAT</th>
                                    <th rowspan='2'>JUMLAH HARI PERAWATAN</th>
                                </tr>" );
$uitable->addHeader ( "before", "<tr>
                                    <th>L</th>
                                    <th>P</th>
                                    <th>L</th>
                                    <th>P</th>
                                    <th>L</th>
                                    <th>P</th>
                                    <th>L</th>
                                    <th>P</th>
                                    <th>L</th>
                                    <th>P</th>
								</tr>" );

$uitable->setHeaderVisible ( false );
$uitable->setFooterVisible( false );

if (isset ( $_POST ['command'] )) {
    /****** Get Total Bed *********/
    $urjip=new ServiceConsumer($db, "get_urjip",array());
    $urjip->setMode(ServiceConsumer::$MULTIPLE_MODE);
    $urjip->execute();
    $content=$urjip->getContent();

    $ruangan=array();
    foreach ($content as $autonomous=>$ruang){
        foreach($ruang as $nama_ruang=>$jip){
            if($jip[$nama_ruang] == "URI"){
                $option=array();
                $option['value']=$nama_ruang;
                $option['name']=ArrayAdapter::format("unslug", $nama_ruang);
                $ruangan[]=$option;
            }
        }
    }
    $total_bed = 0;
    foreach($ruangan as $r) {
         $serv = new ServiceConsumer($db,"get_bed_only", NULL, $r['value']);
         $serv->execute();
         $data = $serv->getContent();
         $total_bed = $total_bed + sizeof($data['list']);
    }
    /******  *********/
    
    /****** Get Pasien *********/
    $serv = new ServiceConsumer($db,"get_patient_lama_rawat", NULL, "registration");
    $serv->addData("command", "list");
    $serv->addData("setshowall", "1");
    $serv->addData("dari", $_POST['dari']);
    $serv->addData("sampai", $_POST['sampai']);
    $serv->execute();
    $data = $serv->getContent();
    /******  *********/
    
    require_once "medical_record/class/adapter/RekapKunjRwtInapAdapter.php";
    $adapter = new RekapKunjRwtInapAdapter();
    $adapter->setJumlahTT($total_bed);
    $adapter->setDariSampai($_POST['dari'], $_POST['sampai']);
    $ready = $adapter->getContent($data['data']);
    
    if($_POST['command'] == 'list') {
        $uitable->setContent($ready);
        $content['list'] = $uitable->getBodyContent();
        $response = new ResponsePackage ();
        $response->setAlertVisible ( false );
        $response->setStatus ( ResponsePackage::$STATUS_OK );
        $response->setContent($content);
        echo json_encode ( $response->getPackage () );
        return;
                
        $dbres = new DBResponder ( $dbtable, $uitable, $adapter );
        $data = $dbres->command ( $_POST ['command'] );
        echo json_encode ( $data );
        return;
    } else if($_POST['command'] == 'excel') {
        require_once ("smis-libs-out/php-excel/PHPExcel.php");
        $file = new PHPExcel ();
        $file->getProperties ()->setCreator ( "Rumah Sakit" );
        $file->getProperties ()->setLastModifiedBy ( "Rumah Sakit Bagian Administrator" );
        $file->getProperties ()->setTitle ( "Rekap Kunjungan Rawat Inap" );
        $file->getProperties ()->setSubject ( "Rekap Kunjungan Rawat Inap" );
        $file->getProperties ()->setDescription ( "Rekap Kunjungan Rawat Inap Generated From system" );
        $file->getProperties ()->setKeywords ( "Rekap Kunjungan Rawat Inap" );
        $file->getProperties ()->setCategory ( "Rekap Kunjungan Rawat Inap" );
        
        $sheet = $file->getActiveSheet ();
        $i = 1;
        
        $objDrawing = new PHPExcel_Worksheet_Drawing();
        $objDrawing->setName('logo_img');
        $objDrawing->setDescription('logo_img');
        $objDrawing->setPath(getLogoNonInterlaced());
        $objDrawing->setCoordinates('A'.$i);
        $objDrawing->setOffsetX(20); 
        $objDrawing->setOffsetY(5);
        $objDrawing->setWidth(100); 
        $objDrawing->setHeight(100);
        $objDrawing->setWorksheet($sheet);
        $i++;
        
        $sheet->setCellValue("C".$i, getSettings($db,"smis_autonomous_title"));
        $sheet->getStyle("C".$i)->getFont()->setBold(true)->setSize(14);
        $i++;
        $sheet->setCellValue("C".$i, getSettings($db,"smis_autonomous_address"));
        $sheet->getStyle("C".$i)->getFont()->setBold(true)->setSize(14);
        $i++;
        $sheet->setCellValue("C".$i, getSettings($db,"smis_autonomous_contact"));
        $sheet->getStyle("C".$i)->getFont()->setBold(true)->setSize(14);
        $i += 3;
        $sheet->mergeCells("A".$i.":M".$i)->setCellValue("A".$i,"REKAP KUNJUNGAN RAWAT INAP");
        $sheet->getStyle("A".$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle("A".$i)->getFont()->setBold(true)->setSize(14);
        $i++;
        $sheet->mergeCells("A".$i.":M".$i)->setCellValue("A".$i, strtoupper(ArrayAdapter::dateFormat("date d M Y", $_POST['dari']))." - ".strtoupper(ArrayAdapter::dateFormat("date d M Y", $_POST['sampai'])));
        $sheet->getStyle("A".$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle("A".$i)->getFont()->setBold(true);
        $i += 3;
        
        $border_start = $i;
        $ips = $i + 1;
        $sheet->mergeCells("A".$i.":A".$ips)->setCellValue("A".$i, "JUMLAH TEMPAT TIDUR");
        $sheet->mergeCells("B".$i.":C".$i)->setCellValue("B".$i, "MASUK RUMAH SAKIT");
        $sheet->mergeCells("D".$i.":E".$i)->setCellValue("D".$i, "KELUAR HIDUP");
        $sheet->mergeCells("F".$i.":G".$i)->setCellValue("F".$i, "KELUAR MATI");
        $sheet->mergeCells("H".$i.":I".$i)->setCellValue("H".$i, "PASIEN MATI <48 JAM");
        $sheet->mergeCells("J".$i.":K".$i)->setCellValue("J".$i, "PASIEN MATI >48 JAM");
        $sheet->mergeCells("L".$i.":L".$ips)->setCellValue("L".$i, "JUMLAH LAMA DIRAWAT");
        $sheet->mergeCells("M".$i.":M".$ips)->setCellValue("M".$i, "JUMLAH HARI PERAWATAN");
        $sheet->getStyle("A".$i.":M".$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle("A".$i.":M".$i)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $sheet->getStyle("A".$i.":M".$i)->getFont()->setBold(true);
        $i++;
        
        $sheet->setCellValue("B".$i, "L");
        $sheet->setCellValue("C".$i, "P");
        $sheet->setCellValue("D".$i, "L");
        $sheet->setCellValue("E".$i, "P");
        $sheet->setCellValue("F".$i, "L");
        $sheet->setCellValue("G".$i, "P");
        $sheet->setCellValue("H".$i, "L");
        $sheet->setCellValue("I".$i, "P");
        $sheet->setCellValue("J".$i, "L");
        $sheet->setCellValue("K".$i, "P");
        $sheet->getStyle("A".$i.":M".$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle("A".$i.":M".$i)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $sheet->getStyle("A".$i.":M".$i)->getFont()->setBold(true);
        $i++;
        
        foreach($ready as $x) {
            $sheet->setCellValue("A".$i, $x["jumlah_tt"]);
            $sheet->setCellValue("B".$i, $x["masuk_l"]);
            $sheet->setCellValue("C".$i, $x["masuk_p"]);
            $sheet->setCellValue("D".$i, $x["keluar_hidup_l"]);
            $sheet->setCellValue("E".$i, $x["keluar_hidup_p"]);
            $sheet->setCellValue("F".$i, $x["keluar_mati_l"]);
            $sheet->setCellValue("G".$i, $x["keluar_mati_p"]);
            $sheet->setCellValue("H".$i, $x["mati_k48_l"]);
            $sheet->setCellValue("I".$i, $x["mati_k48_p"]);
            $sheet->setCellValue("J".$i, $x["mati_l48_l"]);
            $sheet->setCellValue("K".$i, $x["mati_l48_p"]);
            $sheet->setCellValue("L".$i, $x["jumlah_lama_dirawat"]);
            $sheet->setCellValue("M".$i, $x["jumlah_hari_perawatan"]);
            $sheet->getStyle("A".$i.":M".$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $i++;
        }
        
        $border_end = $i - 1;
        $thin = array ();
        $thin['borders']=array();
        $thin['borders']['allborders']=array();
        $thin['borders']['allborders']['style']=PHPExcel_Style_Border::BORDER_THIN ;
        $sheet  ->getStyle ( "A".$border_start.":M".$border_end )->applyFromArray ($thin);
        
        $filename = "Rekap Kunjungan Rawat Inap ".strtoupper(ArrayAdapter::dateFormat("date d M Y", $_POST['dari']))." - ".strtoupper(ArrayAdapter::dateFormat("date d M Y", $_POST['sampai'])).".xls";
        
        header ( 'Content-Type: application/vnd.ms-excel' );
        header ( 'Content-Disposition: attachment;filename="'.$filename.'"' );
        header ( 'Cache-Control: max-age=0' );
        $writer = PHPExcel_IOFactory::createWriter ( $file, 'Excel5' );
        $writer->save ( 'php://output' );
		return;
    }
}

$uitable->addModal( "dari", "date", "Dari", "" );
$uitable->addModal( "sampai", "date", "Sampai", "" );

$modal = $uitable->getModal ();
$form=$modal->getForm();

$v = new Button("", "", "");
$v->addClass("btn-primary")
		->setIsButton(Button::$ICONIC)
		->setIcon("fa fa-refresh")
		->setAction("rekap_kunj_rwt_inap.view()");
$form->addElement("", $v);

$excel = new Button("", "", "");
$excel->addClass("btn-primary")
		->setIsButton(Button::$ICONIC)
		->setIcon("fa fa-file-excel-o")
		->setAction("rekap_kunj_rwt_inap.excel()");
$form->addElement("", $excel);

echo "<h2><strong>Rekap Kunjungan Rawat Inap</h2>";
echo $form->getHtml();
echo $uitable->getHtml ();

echo addJS ( "framework/smis/js/table_action.js" );
echo addJS ( "framework/bootstrap/js/bootstrap-datepicker.js" );
echo addCSS ( "framework/bootstrap/css/datepicker.css" );
echo addJS ( "medical_record/resource/js/rekap_kunj_rwt_inap.js", false );

?>