<?php
global $db;
require_once 'smis-base/smis-include-service-consumer.php';
require_once 'smis-libs-class/MasterTemplate.php';
require_once 'smis-libs-class/MasterSlaveTemplate.php';
require_once 'smis-libs-class/ServiceProviderList.php';
require_once 'medical_record/class/adapter/LapGigiMulutAdapter.php';
require_once 'medical_record/class/responder/RL33Responder.php';

$periksa = new MasterSlaveTemplate($db, "smis_mr_gigimulut", "medical_record", "rl33");

$excel=new Button("", "", "");
$excel->setAction("rl33.excel()");
$excel->setClass("btn-primary");
$excel->setIsButton(Button::$ICONIC);
$excel->setIcon("fa fa-file-excel-o");

$periksa->setDateEnable(true);
$dbtable=$periksa->getDBtable();
$qv="SELECT 
		SUM(tumpatan_gigi_tetap) as tumpatan_gigi_tetap,
		SUM(tumpatan_gigi_sulung) as tumpatan_gigi_sulung,
		SUM(pengobatan_pulpa) as pengobatan_pulpa,
		SUM(IF(pencabutan_gigi_tetap!='',1,0)) as pencabutan_gigi_tetap,
		SUM(IF(pencabutan_gigi_sulung!='',1,0)) as pencabutan_gigi_sulung,
		SUM(IF(pengobatan_periodontal!='',1,0)) as pengobatan_periodontal,
		SUM(IF(pengobatan_abses!='',1,0)) as pengobatan_abses,
		SUM(pembersihan_karang_gigi) as pembersihan_karang_gigi,
		SUM(prothese_lengkap) as prothese_lengkap,
		SUM(prothese_sebagian) as prothese_sebagian,
		SUM(prothese_cekat) as prothese_cekat,
		SUM(IF(orthodonti!='',1,0) ) as orthodonti,
		SUM(jacket_bridge) as jacket_bridge,
		SUM(IF(bedah_mulut!='',1,0) ) as bedah_mulut	
		FROM smis_mr_gigimulut
		";
$qc="SELECT 1 as total FROM smis_mr_gigimulut ";
$dbtable->setPreferredQuery(true, $qv, $qc);
$dbtable->setUseWhereforView(true);

$header=array ("No.","Layanan",'Total');
$btn=$periksa->addFlag("bulan", "Pilih Bulan", "Silakan Pilih Bulan Dahulu")
			  ->addFlag("tahun", "Pilih Tahun", "Silakan Pilih Tahun Dahulu")
			  ->addNoClear("bulan")
			  ->addNoClear("tahun")
			  ->getUItable()
			  ->setHeader($header)
			  ->setAddButtonEnable(false)
			  ->getHeaderButton();

$bulan_option = new OptionBuilder();
$bulan_option->add("Semua", "%%", "1");
$bulan_option->add("Januari", "1");
$bulan_option->add("Februari", "2");
$bulan_option->add("Maret", "3");
$bulan_option->add("April", "4");
$bulan_option->add("Mei", "5");
$bulan_option->add("Juni", "6");
$bulan_option->add("Juli", "7");
$bulan_option->add("Agustus", "8");
$bulan_option->add("September", "9");
$bulan_option->add("Oktober", "10");
$bulan_option->add("Nopember", "11");
$bulan_option->add("Desember", "12");

$tahun_rows = $db->get_result("
	SELECT DISTINCT YEAR(tanggal) tahun
	FROM smis_mr_gigimulut
	WHERE prop = '' AND YEAR(tanggal) > 0
	ORDER BY YEAR(tanggal) ASC
");
$tahun_option = new OptionBuilder();
if ($tahun_rows != null) {
	foreach ($tahun_rows as $tahun_row) {
		if (date("Y") == $tahun_row->tahun)
			$tahun_option->add($tahun_row->tahun, $tahun_row->tahun, "1");
		else
			$tahun_option->add($tahun_row->tahun, $tahun_row->tahun);
	}
}

$periksa->getUItable()
		 ->setAction(false)
		 ->setFooterVisible(false)
		 ->addModal("bulan", "select", "Bulan", $bulan_option->getContent())
		 ->addModal("tahun", "select", "Tahun", $tahun_option->getContent());
$periksa->getForm(true,"")
		->addElement("", $btn)
        ->addElement("", $excel);
$gm = new LapGigiMulutAdapter();
$periksa->setAdapter($gm);
$periksa->addViewData("bulan", "bulan")
		->addViewData("tahun", "tahun");

$periksa->setDBresponder(new RL33Responder($dbtable, $periksa->getUItable(), $periksa->getAdapter()));

if($periksa->getDBResponder()->isView()){
	$dbtable->addCustomKriteria(NULL, "MONTH(tanggal) LIKE '".$_POST['bulan']."'");
	$dbtable->addCustomKriteria(NULL, "YEAR(tanggal) = '".$_POST['tahun']."'");
	$dbtable->setFetchMethode(DBTable::$ARRAY_FETCH);
}
$periksa->initialize();