<?php
	require_once("smis-base/smis-include-service-consumer.php");
	global $db;

	$head = array('Kode RS', 'Nama RS', 'Kode Propinsi', 'Kab/Kota', 'Tahun', 'No. Urut', 'Spesialisasi', 'Total', 'Khusus', 'Besar', 'Sedang', 'Kecil');
	$uitable = new Table($head, "", null, true);
	$uitable->setName("rl36");
	$uitable->setAction(false);
	$uitable->setFooterVisible(false);

	if (isset($_POST ['command'])) {
		if ($_POST['command'] == "view") {
			$tahun = $_POST['tahun'];
			$bulan = $_POST['bulan'];
			$html = "";

			$kode_rs = "";
			$nama_rs = "";
			$kode_propinsi = "";
			$kabupaten_kota = "";
			$info_rs_row = $db->get_row("
				SELECT 
					*
				FROM 
					smis_mr_data_dasar_rs
				WHERE 
					prop = ''
				LIMIT 
					0, 1
			");
			if ($info_rs_row != null) {
				$kode_rs = $info_rs_row->koders;
				$nama_rs = $info_rs_row->nama_rs;
				$kode_propinsi = $info_rs_row->kode_provinsi;
				$kabupaten_kota = $info_rs_row->kota_kabupaten;
			}

			$jumlah_total_bedah = 0;
			$jumlah_khusus_bedah = 0;
			$jumlah_besar_bedah = 0;
			$jumlah_sedang_bedah = 0;
			$jumlah_kecil_bedah = 0;

			$jumlah_khusus_bedah_row = $db->get_row("
				SELECT COUNT(*) jumlah
				FROM smis_mr_operasi
				WHERE prop = '' AND ((YEAR(tanggal_mulai) = '" . $tahun . "' AND MONTH(tanggal_mulai) LIKE '" . $bulan . "') OR (YEAR(tanggal_selesai) = '" . $tahun . "') AND MONTH(tanggal_selesai) LIKE '" . $bulan . "') AND spesialisasi = 'Bedah' AND jenis_operasi = 'Khusus'
			");
			if ($jumlah_khusus_bedah_row != null)
				$jumlah_khusus_bedah = $jumlah_khusus_bedah_row->jumlah;

			$jumlah_besar_bedah_row = $db->get_row("
				SELECT COUNT(*) jumlah
				FROM smis_mr_operasi
				WHERE prop = '' AND ((YEAR(tanggal_mulai) = '" . $tahun . "' AND MONTH(tanggal_mulai) LIKE '" . $bulan . "') OR (YEAR(tanggal_selesai) = '" . $tahun . "') AND MONTH(tanggal_selesai) LIKE '" . $bulan . "') AND spesialisasi = 'Bedah' AND jenis_operasi = 'Besar'
			");
			if ($jumlah_besar_bedah_row != null)
				$jumlah_besar_bedah = $jumlah_besar_bedah_row->jumlah;

			$jumlah_sedang_bedah_row = $db->get_row("
				SELECT COUNT(*) jumlah
				FROM smis_mr_operasi
				WHERE prop = '' AND ((YEAR(tanggal_mulai) = '" . $tahun . "' AND MONTH(tanggal_mulai) LIKE '" . $bulan . "') OR (YEAR(tanggal_selesai) = '" . $tahun . "') AND MONTH(tanggal_selesai) LIKE '" . $bulan . "') AND spesialisasi = 'Bedah' AND jenis_operasi = 'Sedang'
			");
			if ($jumlah_sedang_bedah_row != null)
				$jumlah_sedang_bedah = $jumlah_sedang_bedah_row->jumlah;

			$jumlah_kecil_bedah_row = $db->get_row("
				SELECT COUNT(*) jumlah
				FROM smis_mr_operasi
				WHERE prop = '' AND ((YEAR(tanggal_mulai) = '" . $tahun . "' AND MONTH(tanggal_mulai) LIKE '" . $bulan . "') OR (YEAR(tanggal_selesai) = '" . $tahun . "') AND MONTH(tanggal_selesai) LIKE '" . $bulan . "') AND spesialisasi = 'Bedah' AND jenis_operasi = 'Kecil'
			");
			if ($jumlah_kecil_bedah_row != null)
				$jumlah_kecil_bedah = $jumlah_kecil_bedah_row->jumlah;

			$jumlah_total_bedah = $jumlah_khusus_bedah + $jumlah_besar_bedah + $jumlah_sedang_bedah + $jumlah_kecil_bedah;

			$jumlah_total_obgyn = 0;
			$jumlah_khusus_obgyn = 0;
			$jumlah_besar_obgyn = 0;
			$jumlah_sedang_obgyn = 0;
			$jumlah_kecil_obgyn = 0;

			$jumlah_khusus_obgyn_row = $db->get_row("
				SELECT COUNT(*) jumlah
				FROM smis_mr_operasi
				WHERE prop = '' AND ((YEAR(tanggal_mulai) = '" . $tahun . "' AND MONTH(tanggal_mulai) LIKE '" . $bulan . "') OR (YEAR(tanggal_selesai) = '" . $tahun . "') AND MONTH(tanggal_selesai) LIKE '" . $bulan . "') AND spesialisasi = 'Obstetrik & Ginekologi' AND jenis_operasi = 'Khusus'
			");
			if ($jumlah_khusus_obgyn_row != null)
				$jumlah_khusus_obgyn = $jumlah_khusus_obgyn_row->jumlah;

			$jumlah_besar_obgyn_row = $db->get_row("
				SELECT COUNT(*) jumlah
				FROM smis_mr_operasi
				WHERE prop = '' AND ((YEAR(tanggal_mulai) = '" . $tahun . "' AND MONTH(tanggal_mulai) LIKE '" . $bulan . "') OR (YEAR(tanggal_selesai) = '" . $tahun . "') AND MONTH(tanggal_selesai) LIKE '" . $bulan . "') AND spesialisasi = 'Obstetrik & Ginekologi' AND jenis_operasi = 'Besar'
			");
			if ($jumlah_besar_obgyn_row != null)
				$jumlah_besar_obgyn = $jumlah_besar_obgyn_row->jumlah;

			$jumlah_sedang_obgyn_row = $db->get_row("
				SELECT COUNT(*) jumlah
				FROM smis_mr_operasi
				WHERE prop = '' AND ((YEAR(tanggal_mulai) = '" . $tahun . "' AND MONTH(tanggal_mulai) LIKE '" . $bulan . "') OR (YEAR(tanggal_selesai) = '" . $tahun . "') AND MONTH(tanggal_selesai) LIKE '" . $bulan . "') AND spesialisasi = 'Obstetrik & Ginekologi' AND jenis_operasi = 'Sedang'
			");
			if ($jumlah_sedang_obgyn_row != null)
				$jumlah_sedang_obgyn = $jumlah_sedang_obgyn_row->jumlah;

			$jumlah_kecil_obgyn_row = $db->get_row("
				SELECT COUNT(*) jumlah
				FROM smis_mr_operasi
				WHERE prop = '' AND ((YEAR(tanggal_mulai) = '" . $tahun . "' AND MONTH(tanggal_mulai) LIKE '" . $bulan . "') OR (YEAR(tanggal_selesai) = '" . $tahun . "') AND MONTH(tanggal_selesai) LIKE '" . $bulan . "') AND spesialisasi = 'Obstetrik & Ginekologi' AND jenis_operasi = 'Kecil'
			");
			if ($jumlah_kecil_obgyn_row != null)
				$jumlah_kecil_obgyn = $jumlah_kecil_obgyn_row->jumlah;

			$jumlah_total_obgyn = $jumlah_khusus_obgyn + $jumlah_besar_obgyn + $jumlah_sedang_obgyn + $jumlah_kecil_obgyn;

			$jumlah_total_bedah_saraf = 0;
			$jumlah_khusus_bedah_saraf = 0;
			$jumlah_besar_bedah_saraf = 0;
			$jumlah_sedang_bedah_saraf = 0;
			$jumlah_kecil_bedah_saraf = 0;

			$jumlah_khusus_bedah_saraf_row = $db->get_row("
				SELECT COUNT(*) jumlah
				FROM smis_mr_operasi
				WHERE prop = '' AND ((YEAR(tanggal_mulai) = '" . $tahun . "' AND MONTH(tanggal_mulai) LIKE '" . $bulan . "') OR (YEAR(tanggal_selesai) = '" . $tahun . "') AND MONTH(tanggal_selesai) LIKE '" . $bulan . "') AND spesialisasi = 'Bedah Saraf' AND jenis_operasi = 'Khusus'
			");
			if ($jumlah_khusus_bedah_saraf_row != null)
				$jumlah_khusus_bedah_saraf = $jumlah_khusus_bedah_saraf_row->jumlah;

			$jumlah_besar_bedah_saraf_row = $db->get_row("
				SELECT COUNT(*) jumlah
				FROM smis_mr_operasi
				WHERE prop = '' AND ((YEAR(tanggal_mulai) = '" . $tahun . "' AND MONTH(tanggal_mulai) LIKE '" . $bulan . "') OR (YEAR(tanggal_selesai) = '" . $tahun . "') AND MONTH(tanggal_selesai) LIKE '" . $bulan . "') AND spesialisasi = 'Bedah Saraf' AND jenis_operasi = 'Besar'
			");
			if ($jumlah_besar_bedah_saraf_row != null)
				$jumlah_besar_bedah_saraf = $jumlah_besar_bedah_saraf_row->jumlah;

			$jumlah_sedang_bedah_saraf_row = $db->get_row("
				SELECT COUNT(*) jumlah
				FROM smis_mr_operasi
				WHERE prop = '' AND ((YEAR(tanggal_mulai) = '" . $tahun . "' AND MONTH(tanggal_mulai) LIKE '" . $bulan . "') OR (YEAR(tanggal_selesai) = '" . $tahun . "') AND MONTH(tanggal_selesai) LIKE '" . $bulan . "') AND spesialisasi = 'Bedah Saraf' AND jenis_operasi = 'Sedang'
			");
			if ($jumlah_sedang_bedah_saraf_row != null)
				$jumlah_sedang_bedah_saraf = $jumlah_sedang_bedah_saraf_row->jumlah;

			$jumlah_kecil_bedah_saraf_row = $db->get_row("
				SELECT COUNT(*) jumlah
				FROM smis_mr_operasi
				WHERE prop = '' AND ((YEAR(tanggal_mulai) = '" . $tahun . "' AND MONTH(tanggal_mulai) LIKE '" . $bulan . "') OR (YEAR(tanggal_selesai) = '" . $tahun . "') AND MONTH(tanggal_selesai) LIKE '" . $bulan . "') AND spesialisasi = 'Bedah Saraf' AND jenis_operasi = 'Kecil'
			");
			if ($jumlah_kecil_bedah_saraf_row != null)
				$jumlah_kecil_bedah_saraf = $jumlah_kecil_bedah_saraf_row->jumlah;

			$jumlah_total_bedah_saraf = $jumlah_khusus_bedah_saraf + $jumlah_besar_bedah_saraf + $jumlah_sedang_bedah_saraf + $jumlah_kecil_bedah_saraf;

			$jumlah_total_tht = 0;
			$jumlah_khusus_tht = 0;
			$jumlah_besar_tht = 0;
			$jumlah_sedang_tht = 0;
			$jumlah_kecil_tht = 0;

			$jumlah_khusus_tht_row = $db->get_row("
				SELECT COUNT(*) jumlah
				FROM smis_mr_operasi
				WHERE prop = '' AND ((YEAR(tanggal_mulai) = '" . $tahun . "' AND MONTH(tanggal_mulai) LIKE '" . $bulan . "') OR (YEAR(tanggal_selesai) = '" . $tahun . "') AND MONTH(tanggal_selesai) LIKE '" . $bulan . "') AND spesialisasi = 'T H T' AND jenis_operasi = 'Khusus'
			");
			if ($jumlah_khusus_tht_row != null)
				$jumlah_khusus_tht = $jumlah_khusus_tht_row->jumlah;

			$jumlah_besar_tht_row = $db->get_row("
				SELECT COUNT(*) jumlah
				FROM smis_mr_operasi
				WHERE prop = '' AND ((YEAR(tanggal_mulai) = '" . $tahun . "' AND MONTH(tanggal_mulai) LIKE '" . $bulan . "') OR (YEAR(tanggal_selesai) = '" . $tahun . "') AND MONTH(tanggal_selesai) LIKE '" . $bulan . "') AND spesialisasi = 'T H T' AND jenis_operasi = 'Besar'
			");
			if ($jumlah_besar_tht_row != null)
				$jumlah_besar_tht = $jumlah_besar_tht_row->jumlah;

			$jumlah_sedang_tht_row = $db->get_row("
				SELECT COUNT(*) jumlah
				FROM smis_mr_operasi
				WHERE prop = '' AND ((YEAR(tanggal_mulai) = '" . $tahun . "' AND MONTH(tanggal_mulai) LIKE '" . $bulan . "') OR (YEAR(tanggal_selesai) = '" . $tahun . "') AND MONTH(tanggal_selesai) LIKE '" . $bulan . "') AND spesialisasi = 'T H T' AND jenis_operasi = 'Sedang'
			");
			if ($jumlah_sedang_tht_row != null)
				$jumlah_sedang_tht = $jumlah_sedang_tht_row->jumlah;

			$jumlah_kecil_tht_row = $db->get_row("
				SELECT COUNT(*) jumlah
				FROM smis_mr_operasi
				WHERE prop = '' AND ((YEAR(tanggal_mulai) = '" . $tahun . "' AND MONTH(tanggal_mulai) LIKE '" . $bulan . "') OR (YEAR(tanggal_selesai) = '" . $tahun . "') AND MONTH(tanggal_selesai) LIKE '" . $bulan . "') AND spesialisasi = 'T H T' AND jenis_operasi = 'Kecil'
			");
			if ($jumlah_kecil_tht_row != null)
				$jumlah_kecil_tht = $jumlah_kecil_tht_row->jumlah;

			$jumlah_total_tht = $jumlah_khusus_tht + $jumlah_besar_tht + $jumlah_sedang_tht + $jumlah_kecil_tht;

			$jumlah_total_mata = 0;
			$jumlah_khusus_mata = 0;
			$jumlah_besar_mata = 0;
			$jumlah_sedang_mata = 0;
			$jumlah_kecil_mata = 0;

			$jumlah_khusus_mata_row = $db->get_row("
				SELECT COUNT(*) jumlah
				FROM smis_mr_operasi
				WHERE prop = '' AND ((YEAR(tanggal_mulai) = '" . $tahun . "' AND MONTH(tanggal_mulai) LIKE '" . $bulan . "') OR (YEAR(tanggal_selesai) = '" . $tahun . "') AND MONTH(tanggal_selesai) LIKE '" . $bulan . "') AND spesialisasi = 'Mata' AND jenis_operasi = 'Khusus'
			");
			if ($jumlah_khusus_mata_row != null)
				$jumlah_khusus_mata = $jumlah_khusus_mata_row->jumlah;

			$jumlah_besar_mata_row = $db->get_row("
				SELECT COUNT(*) jumlah
				FROM smis_mr_operasi
				WHERE prop = '' AND ((YEAR(tanggal_mulai) = '" . $tahun . "' AND MONTH(tanggal_mulai) LIKE '" . $bulan . "') OR (YEAR(tanggal_selesai) = '" . $tahun . "') AND MONTH(tanggal_selesai) LIKE '" . $bulan . "') AND spesialisasi = 'Mata' AND jenis_operasi = 'Besar'
			");
			if ($jumlah_besar_mata_row != null)
				$jumlah_besar_mata = $jumlah_besar_mata_row->jumlah;

			$jumlah_sedang_mata_row = $db->get_row("
				SELECT COUNT(*) jumlah
				FROM smis_mr_operasi
				WHERE prop = '' AND ((YEAR(tanggal_mulai) = '" . $tahun . "' AND MONTH(tanggal_mulai) LIKE '" . $bulan . "') OR (YEAR(tanggal_selesai) = '" . $tahun . "') AND MONTH(tanggal_selesai) LIKE '" . $bulan . "') AND spesialisasi = 'Mata' AND jenis_operasi = 'Sedang'
			");
			if ($jumlah_sedang_mata_row != null)
				$jumlah_sedang_mata = $jumlah_sedang_mata_row->jumlah;

			$jumlah_kecil_mata_row = $db->get_row("
				SELECT COUNT(*) jumlah
				FROM smis_mr_operasi
				WHERE prop = '' AND ((YEAR(tanggal_mulai) = '" . $tahun . "' AND MONTH(tanggal_mulai) LIKE '" . $bulan . "') OR (YEAR(tanggal_selesai) = '" . $tahun . "') AND MONTH(tanggal_selesai) LIKE '" . $bulan . "') AND spesialisasi = 'Mata' AND jenis_operasi = 'Kecil'
			");
			if ($jumlah_kecil_mata_row != null)
				$jumlah_kecil_mata = $jumlah_kecil_mata_row->jumlah;

			$jumlah_total_mata = $jumlah_khusus_mata + $jumlah_besar_mata + $jumlah_sedang_mata + $jumlah_kecil_mata;

			$jumlah_total_kk = 0;
			$jumlah_khusus_kk = 0;
			$jumlah_besar_kk = 0;
			$jumlah_sedang_kk = 0;
			$jumlah_kecil_kk = 0;

			$jumlah_khusus_kk_row = $db->get_row("
				SELECT COUNT(*) jumlah
				FROM smis_mr_operasi
				WHERE prop = '' AND ((YEAR(tanggal_mulai) = '" . $tahun . "' AND MONTH(tanggal_mulai) LIKE '" . $bulan . "') OR (YEAR(tanggal_selesai) = '" . $tahun . "') AND MONTH(tanggal_selesai) LIKE '" . $bulan . "') AND spesialisasi = 'Kulit & Kelamin' AND jenis_operasi = 'Khusus'
			");
			if ($jumlah_khusus_kk_row != null)
				$jumlah_khusus_kk = $jumlah_khusus_kk_row->jumlah;

			$jumlah_besar_kk_row = $db->get_row("
				SELECT COUNT(*) jumlah
				FROM smis_mr_operasi
				WHERE prop = '' AND ((YEAR(tanggal_mulai) = '" . $tahun . "' AND MONTH(tanggal_mulai) LIKE '" . $bulan . "') OR (YEAR(tanggal_selesai) = '" . $tahun . "') AND MONTH(tanggal_selesai) LIKE '" . $bulan . "') AND spesialisasi = 'Kulit & Kelamin' AND jenis_operasi = 'Besar'
			");
			if ($jumlah_besar_kk_row != null)
				$jumlah_besar_kk = $jumlah_besar_kk_row->jumlah;

			$jumlah_sedang_kk_row = $db->get_row("
				SELECT COUNT(*) jumlah
				FROM smis_mr_operasi
				WHERE prop = '' AND ((YEAR(tanggal_mulai) = '" . $tahun . "' AND MONTH(tanggal_mulai) LIKE '" . $bulan . "') OR (YEAR(tanggal_selesai) = '" . $tahun . "') AND MONTH(tanggal_selesai) LIKE '" . $bulan . "') AND spesialisasi = 'Kulit & Kelamin' AND jenis_operasi = 'Sedang'
			");
			if ($jumlah_sedang_kk_row != null)
				$jumlah_sedang_kk = $jumlah_sedang_kk_row->jumlah;

			$jumlah_kecil_kk_row = $db->get_row("
				SELECT COUNT(*) jumlah
				FROM smis_mr_operasi
				WHERE prop = '' AND ((YEAR(tanggal_mulai) = '" . $tahun . "' AND MONTH(tanggal_mulai) LIKE '" . $bulan . "') OR (YEAR(tanggal_selesai) = '" . $tahun . "') AND MONTH(tanggal_selesai) LIKE '" . $bulan . "') AND spesialisasi = 'Kulit & Kelamin' AND jenis_operasi = 'Kecil'
			");
			if ($jumlah_kecil_kk_row != null)
				$jumlah_kecil_kk = $jumlah_kecil_kk_row->jumlah;

			$jumlah_total_kk = $jumlah_khusus_kk + $jumlah_besar_kk + $jumlah_sedang_kk + $jumlah_kecil_kk;

			$jumlah_total_gigi_mulut = 0;
			$jumlah_khusus_gigi_mulut = 0;
			$jumlah_besar_gigi_mulut = 0;
			$jumlah_sedang_gigi_mulut = 0;
			$jumlah_kecil_gigi_mulut = 0;

			$jumlah_khusus_gigi_mulut_row = $db->get_row("
				SELECT COUNT(*) jumlah
				FROM smis_mr_operasi
				WHERE prop = '' AND ((YEAR(tanggal_mulai) = '" . $tahun . "' AND MONTH(tanggal_mulai) LIKE '" . $bulan . "') OR (YEAR(tanggal_selesai) = '" . $tahun . "') AND MONTH(tanggal_selesai) LIKE '" . $bulan . "') AND spesialisasi = 'Gigi & Mulut' AND jenis_operasi = 'Khusus'
			");
			if ($jumlah_khusus_gigi_mulut_row != null)
				$jumlah_khusus_gigi_mulut = $jumlah_khusus_gigi_mulut_row->jumlah;

			$jumlah_besar_gigi_mulut_row = $db->get_row("
				SELECT COUNT(*) jumlah
				FROM smis_mr_operasi
				WHERE prop = '' AND ((YEAR(tanggal_mulai) = '" . $tahun . "' AND MONTH(tanggal_mulai) LIKE '" . $bulan . "') OR (YEAR(tanggal_selesai) = '" . $tahun . "') AND MONTH(tanggal_selesai) LIKE '" . $bulan . "') AND spesialisasi = 'Gigi & Mulut' AND jenis_operasi = 'Besar'
			");
			if ($jumlah_besar_gigi_mulut_row != null)
				$jumlah_besar_gigi_mulut = $jumlah_besar_gigi_mulut_row->jumlah;

			$jumlah_sedang_gigi_mulut_row = $db->get_row("
				SELECT COUNT(*) jumlah
				FROM smis_mr_operasi
				WHERE prop = '' AND ((YEAR(tanggal_mulai) = '" . $tahun . "' AND MONTH(tanggal_mulai) LIKE '" . $bulan . "') OR (YEAR(tanggal_selesai) = '" . $tahun . "') AND MONTH(tanggal_selesai) LIKE '" . $bulan . "') AND spesialisasi = 'Gigi & Mulut' AND jenis_operasi = 'Sedang'
			");
			if ($jumlah_sedang_gigi_mulut_row != null)
				$jumlah_sedang_gigi_mulut = $jumlah_sedang_gigi_mulut_row->jumlah;

			$jumlah_kecil_gigi_mulut_row = $db->get_row("
				SELECT COUNT(*) jumlah
				FROM smis_mr_operasi
				WHERE prop = '' AND ((YEAR(tanggal_mulai) = '" . $tahun . "' AND MONTH(tanggal_mulai) LIKE '" . $bulan . "') OR (YEAR(tanggal_selesai) = '" . $tahun . "') AND MONTH(tanggal_selesai) LIKE '" . $bulan . "') AND spesialisasi = 'Gigi & Mulut' AND jenis_operasi = 'Kecil'
			");
			if ($jumlah_kecil_gigi_mulut_row != null)
				$jumlah_kecil_gigi_mulut = $jumlah_kecil_gigi_mulut_row->jumlah;

			$jumlah_total_gigi_mulut = $jumlah_khusus_gigi_mulut + $jumlah_besar_gigi_mulut + $jumlah_sedang_gigi_mulut + $jumlah_kecil_gigi_mulut;

			$jumlah_total_bedah_anak = 0;
			$jumlah_khusus_bedah_anak = 0;
			$jumlah_besar_bedah_anak = 0;
			$jumlah_sedang_bedah_anak = 0;
			$jumlah_kecil_bedah_anak = 0;

			$jumlah_khusus_bedah_anak_row = $db->get_row("
				SELECT COUNT(*) jumlah
				FROM smis_mr_operasi
				WHERE prop = '' AND ((YEAR(tanggal_mulai) = '" . $tahun . "' AND MONTH(tanggal_mulai) LIKE '" . $bulan . "') OR (YEAR(tanggal_selesai) = '" . $tahun . "') AND MONTH(tanggal_selesai) LIKE '" . $bulan . "') AND spesialisasi = 'Bedah Anak' AND jenis_operasi = 'Khusus'
			");
			if ($jumlah_khusus_bedah_anak_row != null)
				$jumlah_khusus_bedah_anak = $jumlah_khusus_bedah_anak_row->jumlah;

			$jumlah_besar_bedah_anak_row = $db->get_row("
				SELECT COUNT(*) jumlah
				FROM smis_mr_operasi
				WHERE prop = '' AND ((YEAR(tanggal_mulai) = '" . $tahun . "' AND MONTH(tanggal_mulai) LIKE '" . $bulan . "') OR (YEAR(tanggal_selesai) = '" . $tahun . "') AND MONTH(tanggal_selesai) LIKE '" . $bulan . "') AND spesialisasi = 'Bedah Anak' AND jenis_operasi = 'Besar'
			");
			if ($jumlah_besar_bedah_anak_row != null)
				$jumlah_besar_bedah_anak = $jumlah_besar_bedah_anak_row->jumlah;

			$jumlah_sedang_bedah_anak_row = $db->get_row("
				SELECT COUNT(*) jumlah
				FROM smis_mr_operasi
				WHERE prop = '' AND ((YEAR(tanggal_mulai) = '" . $tahun . "' AND MONTH(tanggal_mulai) LIKE '" . $bulan . "') OR (YEAR(tanggal_selesai) = '" . $tahun . "') AND MONTH(tanggal_selesai) LIKE '" . $bulan . "') AND spesialisasi = 'Bedah Anak' AND jenis_operasi = 'Sedang'
			");
			if ($jumlah_sedang_bedah_anak_row != null)
				$jumlah_sedang_bedah_anak = $jumlah_sedang_bedah_anak_row->jumlah;

			$jumlah_kecil_bedah_anak_row = $db->get_row("
				SELECT COUNT(*) jumlah
				FROM smis_mr_operasi
				WHERE prop = '' AND ((YEAR(tanggal_mulai) = '" . $tahun . "' AND MONTH(tanggal_mulai) LIKE '" . $bulan . "') OR (YEAR(tanggal_selesai) = '" . $tahun . "') AND MONTH(tanggal_selesai) LIKE '" . $bulan . "') AND spesialisasi = 'Bedah Anak' AND jenis_operasi = 'Kecil'
			");
			if ($jumlah_kecil_bedah_anak_row != null)
				$jumlah_kecil_bedah_anak = $jumlah_kecil_bedah_anak_row->jumlah;

			$jumlah_total_bedah_anak = $jumlah_khusus_bedah_anak + $jumlah_besar_bedah_anak + $jumlah_sedang_bedah_anak + $jumlah_kecil_bedah_anak;

			$jumlah_total_kardiovaskuler = 0;
			$jumlah_khusus_kardiovaskuler = 0;
			$jumlah_besar_kardiovaskuler = 0;
			$jumlah_sedang_kardiovaskuler = 0;
			$jumlah_kecil_kardiovaskuler = 0;

			$jumlah_khusus_kardiovaskuler_row = $db->get_row("
				SELECT COUNT(*) jumlah
				FROM smis_mr_operasi
				WHERE prop = '' AND ((YEAR(tanggal_mulai) = '" . $tahun . "' AND MONTH(tanggal_mulai) LIKE '" . $bulan . "') OR (YEAR(tanggal_selesai) = '" . $tahun . "') AND MONTH(tanggal_selesai) LIKE '" . $bulan . "') AND spesialisasi = 'Kardiovaskuler' AND jenis_operasi = 'Khusus'
			");
			if ($jumlah_khusus_kardiovaskuler_row != null)
				$jumlah_khusus_kardiovaskuler = $jumlah_khusus_kardiovaskuler_row->jumlah;

			$jumlah_besar_kardiovaskuler_row = $db->get_row("
				SELECT COUNT(*) jumlah
				FROM smis_mr_operasi
				WHERE prop = '' AND ((YEAR(tanggal_mulai) = '" . $tahun . "' AND MONTH(tanggal_mulai) LIKE '" . $bulan . "') OR (YEAR(tanggal_selesai) = '" . $tahun . "') AND MONTH(tanggal_selesai) LIKE '" . $bulan . "') AND spesialisasi = 'Kardiovaskuler' AND jenis_operasi = 'Besar'
			");
			if ($jumlah_besar_kardiovaskuler_row != null)
				$jumlah_besar_kardiovaskuler = $jumlah_besar_kardiovaskuler_row->jumlah;

			$jumlah_sedang_kardiovaskuler_row = $db->get_row("
				SELECT COUNT(*) jumlah
				FROM smis_mr_operasi
				WHERE prop = '' AND ((YEAR(tanggal_mulai) = '" . $tahun . "' AND MONTH(tanggal_mulai) LIKE '" . $bulan . "') OR (YEAR(tanggal_selesai) = '" . $tahun . "') AND MONTH(tanggal_selesai) LIKE '" . $bulan . "') AND spesialisasi = 'Kardiovaskuler' AND jenis_operasi = 'Sedang'
			");
			if ($jumlah_sedang_kardiovaskuler_row != null)
				$jumlah_sedang_kardiovaskuler = $jumlah_sedang_kardiovaskuler_row->jumlah;

			$jumlah_kecil_kardiovaskuler_row = $db->get_row("
				SELECT COUNT(*) jumlah
				FROM smis_mr_operasi
				WHERE prop = '' AND ((YEAR(tanggal_mulai) = '" . $tahun . "' AND MONTH(tanggal_mulai) LIKE '" . $bulan . "') OR (YEAR(tanggal_selesai) = '" . $tahun . "') AND MONTH(tanggal_selesai) LIKE '" . $bulan . "') AND spesialisasi = 'Kardiovaskuler' AND jenis_operasi = 'Kecil'
			");
			if ($jumlah_kecil_kardiovaskuler_row != null)
				$jumlah_kecil_kardiovaskuler = $jumlah_kecil_kardiovaskuler_row->jumlah;

			$jumlah_total_kardiovaskuler = $jumlah_khusus_kardiovaskuler + $jumlah_besar_kardiovaskuler + $jumlah_sedang_kardiovaskuler + $jumlah_kecil_kardiovaskuler;

			$jumlah_total_bedah_orthopedi = 0;
			$jumlah_khusus_bedah_orthopedi = 0;
			$jumlah_besar_bedah_orthopedi = 0;
			$jumlah_sedang_bedah_orthopedi = 0;
			$jumlah_kecil_bedah_orthopedi = 0;

			$jumlah_khusus_bedah_orthopedi_row = $db->get_row("
				SELECT COUNT(*) jumlah
				FROM smis_mr_operasi
				WHERE prop = '' AND ((YEAR(tanggal_mulai) = '" . $tahun . "' AND MONTH(tanggal_mulai) LIKE '" . $bulan . "') OR (YEAR(tanggal_selesai) = '" . $tahun . "') AND MONTH(tanggal_selesai) LIKE '" . $bulan . "') AND spesialisasi = 'Bedah Orthopedi' AND jenis_operasi = 'Khusus'
			");
			if ($jumlah_khusus_bedah_orthopedi_row != null)
				$jumlah_khusus_bedah_orthopedi = $jumlah_khusus_bedah_orthopedi_row->jumlah;

			$jumlah_besar_bedah_orthopedi_row = $db->get_row("
				SELECT COUNT(*) jumlah
				FROM smis_mr_operasi
				WHERE prop = '' AND ((YEAR(tanggal_mulai) = '" . $tahun . "' AND MONTH(tanggal_mulai) LIKE '" . $bulan . "') OR (YEAR(tanggal_selesai) = '" . $tahun . "') AND MONTH(tanggal_selesai) LIKE '" . $bulan . "') AND spesialisasi = 'Bedah Orthopedi' AND jenis_operasi = 'Besar'
			");
			if ($jumlah_besar_bedah_orthopedi_row != null)
				$jumlah_besar_bedah_orthopedi = $jumlah_besar_bedah_orthopedi_row->jumlah;

			$jumlah_sedang_bedah_orthopedi_row = $db->get_row("
				SELECT COUNT(*) jumlah
				FROM smis_mr_operasi
				WHERE prop = '' AND ((YEAR(tanggal_mulai) = '" . $tahun . "' AND MONTH(tanggal_mulai) LIKE '" . $bulan . "') OR (YEAR(tanggal_selesai) = '" . $tahun . "') AND MONTH(tanggal_selesai) LIKE '" . $bulan . "') AND spesialisasi = 'Bedah Orthopedi' AND jenis_operasi = 'Sedang'
			");
			if ($jumlah_sedang_bedah_orthopedi_row != null)
				$jumlah_sedang_bedah_orthopedi = $jumlah_sedang_bedah_orthopedi_row->jumlah;

			$jumlah_kecil_bedah_orthopedi_row = $db->get_row("
				SELECT COUNT(*) jumlah
				FROM smis_mr_operasi
				WHERE prop = '' AND ((YEAR(tanggal_mulai) = '" . $tahun . "' AND MONTH(tanggal_mulai) LIKE '" . $bulan . "') OR (YEAR(tanggal_selesai) = '" . $tahun . "') AND MONTH(tanggal_selesai) LIKE '" . $bulan . "') AND spesialisasi = 'Bedah Orthopedi' AND jenis_operasi = 'Kecil'
			");
			if ($jumlah_kecil_bedah_orthopedi_row != null)
				$jumlah_kecil_bedah_orthopedi = $jumlah_kecil_bedah_orthopedi_row->jumlah;

			$jumlah_total_bedah_orthopedi = $jumlah_khusus_bedah_orthopedi + $jumlah_besar_bedah_orthopedi + $jumlah_sedang_bedah_orthopedi + $jumlah_kecil_bedah_orthopedi;

			$jumlah_total_thorak = 0;
			$jumlah_khusus_thorak = 0;
			$jumlah_besar_thorak = 0;
			$jumlah_sedang_thorak = 0;
			$jumlah_kecil_thorak = 0;

			$jumlah_khusus_thorak_row = $db->get_row("
				SELECT COUNT(*) jumlah
				FROM smis_mr_operasi
				WHERE prop = '' AND ((YEAR(tanggal_mulai) = '" . $tahun . "' AND MONTH(tanggal_mulai) LIKE '" . $bulan . "') OR (YEAR(tanggal_selesai) = '" . $tahun . "') AND MONTH(tanggal_selesai) LIKE '" . $bulan . "') AND spesialisasi = 'Thorak' AND jenis_operasi = 'Khusus'
			");
			if ($jumlah_khusus_thorak_row != null)
				$jumlah_khusus_thorak = $jumlah_khusus_thorak_row->jumlah;

			$jumlah_besar_thorak_row = $db->get_row("
				SELECT COUNT(*) jumlah
				FROM smis_mr_operasi
				WHERE prop = '' AND ((YEAR(tanggal_mulai) = '" . $tahun . "' AND MONTH(tanggal_mulai) LIKE '" . $bulan . "') OR (YEAR(tanggal_selesai) = '" . $tahun . "') AND MONTH(tanggal_selesai) LIKE '" . $bulan . "') AND spesialisasi = 'Thorak' AND jenis_operasi = 'Besar'
			");
			if ($jumlah_besar_thorak_row != null)
				$jumlah_besar_thorak = $jumlah_besar_thorak_row->jumlah;

			$jumlah_sedang_thorak_row = $db->get_row("
				SELECT COUNT(*) jumlah
				FROM smis_mr_operasi
				WHERE prop = '' AND ((YEAR(tanggal_mulai) = '" . $tahun . "' AND MONTH(tanggal_mulai) LIKE '" . $bulan . "') OR (YEAR(tanggal_selesai) = '" . $tahun . "') AND MONTH(tanggal_selesai) LIKE '" . $bulan . "') AND spesialisasi = 'Thorak' AND jenis_operasi = 'Sedang'
			");
			if ($jumlah_sedang_thorak_row != null)
				$jumlah_sedang_thorak = $jumlah_sedang_thorak_row->jumlah;

			$jumlah_kecil_thorak_row = $db->get_row("
				SELECT COUNT(*) jumlah
				FROM smis_mr_operasi
				WHERE prop = '' AND ((YEAR(tanggal_mulai) = '" . $tahun . "' AND MONTH(tanggal_mulai) LIKE '" . $bulan . "') OR (YEAR(tanggal_selesai) = '" . $tahun . "') AND MONTH(tanggal_selesai) LIKE '" . $bulan . "') AND spesialisasi = 'Thorak' AND jenis_operasi = 'Kecil'
			");
			if ($jumlah_kecil_thorak_row != null)
				$jumlah_kecil_thorak = $jumlah_kecil_thorak_row->jumlah;

			$jumlah_total_thorak = $jumlah_khusus_thorak + $jumlah_besar_thorak + $jumlah_sedang_thorak + $jumlah_kecil_thorak;

			$jumlah_total_digestive = 0;
			$jumlah_khusus_digestive = 0;
			$jumlah_besar_digestive = 0;
			$jumlah_sedang_digestive = 0;
			$jumlah_kecil_digestive = 0;

			$jumlah_khusus_digestive_row = $db->get_row("
				SELECT COUNT(*) jumlah
				FROM smis_mr_operasi
				WHERE prop = '' AND ((YEAR(tanggal_mulai) = '" . $tahun . "' AND MONTH(tanggal_mulai) LIKE '" . $bulan . "') OR (YEAR(tanggal_selesai) = '" . $tahun . "') AND MONTH(tanggal_selesai) LIKE '" . $bulan . "') AND spesialisasi = 'Digestive' AND jenis_operasi = 'Khusus'
			");
			if ($jumlah_khusus_digestive_row != null)
				$jumlah_khusus_digestive = $jumlah_khusus_digestive_row->jumlah;

			$jumlah_besar_digestive_row = $db->get_row("
				SELECT COUNT(*) jumlah
				FROM smis_mr_operasi
				WHERE prop = '' AND ((YEAR(tanggal_mulai) = '" . $tahun . "' AND MONTH(tanggal_mulai) LIKE '" . $bulan . "') OR (YEAR(tanggal_selesai) = '" . $tahun . "') AND MONTH(tanggal_selesai) LIKE '" . $bulan . "') AND spesialisasi = 'Digestive' AND jenis_operasi = 'Besar'
			");
			if ($jumlah_besar_digestive_row != null)
				$jumlah_besar_digestive = $jumlah_besar_digestive_row->jumlah;

			$jumlah_sedang_digestive_row = $db->get_row("
				SELECT COUNT(*) jumlah
				FROM smis_mr_operasi
				WHERE prop = '' AND ((YEAR(tanggal_mulai) = '" . $tahun . "' AND MONTH(tanggal_mulai) LIKE '" . $bulan . "') OR (YEAR(tanggal_selesai) = '" . $tahun . "') AND MONTH(tanggal_selesai) LIKE '" . $bulan . "') AND spesialisasi = 'Digestive' AND jenis_operasi = 'Sedang'
			");
			if ($jumlah_sedang_digestive_row != null)
				$jumlah_sedang_digestive = $jumlah_sedang_digestive_row->jumlah;

			$jumlah_kecil_digestive_row = $db->get_row("
				SELECT COUNT(*) jumlah
				FROM smis_mr_operasi
				WHERE prop = '' AND ((YEAR(tanggal_mulai) = '" . $tahun . "' AND MONTH(tanggal_mulai) LIKE '" . $bulan . "') OR (YEAR(tanggal_selesai) = '" . $tahun . "') AND MONTH(tanggal_selesai) LIKE '" . $bulan . "') AND spesialisasi = 'Digestive' AND jenis_operasi = 'Kecil'
			");
			if ($jumlah_kecil_digestive_row != null)
				$jumlah_kecil_digestive = $jumlah_kecil_digestive_row->jumlah;

			$jumlah_total_digestive = $jumlah_khusus_digestive + $jumlah_besar_digestive + $jumlah_sedang_digestive + $jumlah_kecil_digestive;

			$jumlah_total_urologi = 0;
			$jumlah_khusus_urologi = 0;
			$jumlah_besar_urologi = 0;
			$jumlah_sedang_urologi = 0;
			$jumlah_kecil_urologi = 0;

			$jumlah_khusus_urologi_row = $db->get_row("
				SELECT COUNT(*) jumlah
				FROM smis_mr_operasi
				WHERE prop = '' AND ((YEAR(tanggal_mulai) = '" . $tahun . "' AND MONTH(tanggal_mulai) LIKE '" . $bulan . "') OR (YEAR(tanggal_selesai) = '" . $tahun . "') AND MONTH(tanggal_selesai) LIKE '" . $bulan . "') AND spesialisasi = 'Urologi' AND jenis_operasi = 'Khusus'
			");
			if ($jumlah_khusus_urologi_row != null)
				$jumlah_khusus_urologi = $jumlah_khusus_urologi_row->jumlah;

			$jumlah_besar_urologi_row = $db->get_row("
				SELECT COUNT(*) jumlah
				FROM smis_mr_operasi
				WHERE prop = '' AND ((YEAR(tanggal_mulai) = '" . $tahun . "' AND MONTH(tanggal_mulai) LIKE '" . $bulan . "') OR (YEAR(tanggal_selesai) = '" . $tahun . "') AND MONTH(tanggal_selesai) LIKE '" . $bulan . "') AND spesialisasi = 'Urologi' AND jenis_operasi = 'Besar'
			");
			if ($jumlah_besar_urologi_row != null)
				$jumlah_besar_urologi = $jumlah_besar_urologi_row->jumlah;

			$jumlah_sedang_urologi_row = $db->get_row("
				SELECT COUNT(*) jumlah
				FROM smis_mr_operasi
				WHERE prop = '' AND ((YEAR(tanggal_mulai) = '" . $tahun . "' AND MONTH(tanggal_mulai) LIKE '" . $bulan . "') OR (YEAR(tanggal_selesai) = '" . $tahun . "') AND MONTH(tanggal_selesai) LIKE '" . $bulan . "') AND spesialisasi = 'Urologi' AND jenis_operasi = 'Sedang'
			");
			if ($jumlah_sedang_urologi_row != null)
				$jumlah_sedang_urologi = $jumlah_sedang_urologi_row->jumlah;

			$jumlah_kecil_urologi_row = $db->get_row("
				SELECT COUNT(*) jumlah
				FROM smis_mr_operasi
				WHERE prop = '' AND ((YEAR(tanggal_mulai) = '" . $tahun . "' AND MONTH(tanggal_mulai) LIKE '" . $bulan . "') OR (YEAR(tanggal_selesai) = '" . $tahun . "') AND MONTH(tanggal_selesai) LIKE '" . $bulan . "') AND spesialisasi = 'Urologi' AND jenis_operasi = 'Kecil'
			");
			if ($jumlah_kecil_urologi_row != null)
				$jumlah_kecil_urologi = $jumlah_kecil_urologi_row->jumlah;

			$jumlah_total_urologi = $jumlah_khusus_urologi + $jumlah_besar_urologi + $jumlah_sedang_urologi + $jumlah_kecil_urologi;

			$jumlah_total_lain_lain = 0;
			$jumlah_khusus_lain_lain = 0;
			$jumlah_besar_lain_lain = 0;
			$jumlah_sedang_lain_lain = 0;
			$jumlah_kecil_lain_lain = 0;

			$jumlah_khusus_lain_lain_row = $db->get_row("
				SELECT COUNT(*) jumlah
				FROM smis_mr_operasi
				WHERE prop = '' AND ((YEAR(tanggal_mulai) = '" . $tahun . "' AND MONTH(tanggal_mulai) LIKE '" . $bulan . "') OR (YEAR(tanggal_selesai) = '" . $tahun . "') AND MONTH(tanggal_selesai) LIKE '" . $bulan . "') AND spesialisasi = 'Lain-Lain' AND jenis_operasi = 'Khusus'
			");
			if ($jumlah_khusus_lain_lain_row != null)
				$jumlah_khusus_lain_lain = $jumlah_khusus_lain_lain_row->jumlah;

			$jumlah_besar_lain_lain_row = $db->get_row("
				SELECT COUNT(*) jumlah
				FROM smis_mr_operasi
				WHERE prop = '' AND ((YEAR(tanggal_mulai) = '" . $tahun . "' AND MONTH(tanggal_mulai) LIKE '" . $bulan . "') OR (YEAR(tanggal_selesai) = '" . $tahun . "') AND MONTH(tanggal_selesai) LIKE '" . $bulan . "') AND spesialisasi = 'Lain-Lain' AND jenis_operasi = 'Besar'
			");
			if ($jumlah_besar_lain_lain_row != null)
				$jumlah_besar_lain_lain = $jumlah_besar_lain_lain_row->jumlah;

			$jumlah_sedang_lain_lain_row = $db->get_row("
				SELECT COUNT(*) jumlah
				FROM smis_mr_operasi
				WHERE prop = '' AND ((YEAR(tanggal_mulai) = '" . $tahun . "' AND MONTH(tanggal_mulai) LIKE '" . $bulan . "') OR (YEAR(tanggal_selesai) = '" . $tahun . "') AND MONTH(tanggal_selesai) LIKE '" . $bulan . "') AND spesialisasi = 'Lain-Lain' AND jenis_operasi = 'Sedang'
			");
			if ($jumlah_sedang_lain_lain_row != null)
				$jumlah_sedang_lain_lain = $jumlah_sedang_lain_lain_row->jumlah;

			$jumlah_kecil_lain_lain_row = $db->get_row("
				SELECT COUNT(*) jumlah
				FROM smis_mr_operasi
				WHERE prop = '' AND ((YEAR(tanggal_mulai) = '" . $tahun . "' AND MONTH(tanggal_mulai) LIKE '" . $bulan . "') OR (YEAR(tanggal_selesai) = '" . $tahun . "') AND MONTH(tanggal_selesai) LIKE '" . $bulan . "') AND spesialisasi = 'Lain-Lain' AND jenis_operasi = 'Kecil'
			");
			if ($jumlah_kecil_lain_lain_row != null)
				$jumlah_kecil_lain_lain = $jumlah_kecil_lain_lain_row->jumlah;

			$jumlah_total_lain_lain = $jumlah_khusus_lain_lain + $jumlah_besar_lain_lain + $jumlah_sedang_lain_lain + $jumlah_kecil_lain_lain;	

			$html .= "<tr>" .
						"<td id='rl36_kode_rs'>" . $kode_rs . "</td>" .
						"<td id='rl36_nama_rs'>" . $nama_rs . "</td>" .
						"<td id='rl36_kode_propinsi'>" . $kode_propinsi . "</td>" .
						"<td id='rl36_kabupaten_kota'>" . $kabupaten_kota . "</td>" .
						"<td id='rl36_tahun'>" . $tahun . "</td>" .
						"<td id='rl36_nomor'>1</td>" .
						"<td id='rl36_spesialisasi'><small>Bedah</small></td>" .
						"<td id='rl36_f_total'><div align='right'>" . $jumlah_total_bedah . "</div></td>" .
						"<td id='rl36_total' style='display:none;'>" . $jumlah_total_bedah . "</td>" .
						"<td id='rl36_f_khusus'><div align='right'>" . $jumlah_khusus_bedah . "</div></td>" .
						"<td id='rl36_khusus' style='display:none;'>" . $jumlah_khusus_bedah . "</td>" .
						"<td id='rl36_f_besar'><div align='right'>" . $jumlah_besar_bedah . "</div></td>" .
						"<td id='rl36_besar' style='display:none;'>" . $jumlah_besar_bedah . "</td>" .
						"<td id='rl36_f_sedang'><div align='right'>" . $jumlah_sedang_bedah . "</div></td>" .
						"<td id='rl36_sedang' style='display:none;'>" . $jumlah_sedang_bedah . "</td>" .
						"<td id='rl36_f_kecil'><div align='right'>" . $jumlah_kecil_bedah . "</div></td>" .
						"<td id='rl36_kecil' style='display:none;'>" . $jumlah_kecil_bedah . "</td>" .
					"</tr>";
			$html .= "<tr>" .
						"<td id='rl36_kode_rs'>" . $kode_rs . "</td>" .
						"<td id='rl36_nama_rs'>" . $nama_rs . "</td>" .
						"<td id='rl36_kode_propinsi'>" . $kode_propinsi . "</td>" .
						"<td id='rl36_kabupaten_kota'>" . $kabupaten_kota . "</td>" .
						"<td id='rl36_tahun'>" . $tahun . "</td>" .
						"<td id='rl36_nomor'>2</td>" .
						"<td id='rl36_spesialisasi'><small>Obstetrik & Ginekologi</small></td>" .
						"<td id='rl36_f_total'><div align='right'>" . $jumlah_total_obgyn . "</div></td>" .
						"<td id='rl36_total' style='display:none;'>" . $jumlah_total_obgyn . "</td>" .
						"<td id='rl36_f_khusus'><div align='right'>" . $jumlah_khusus_obgyn . "</div></td>" .
						"<td id='rl36_khusus' style='display:none;'>" . $jumlah_khusus_obgyn . "</td>" .
						"<td id='rl36_f_besar'><div align='right'>" . $jumlah_besar_obgyn . "</div></td>" .
						"<td id='rl36_besar' style='display:none;'>" . $jumlah_besar_obgyn . "</td>" .
						"<td id='rl36_f_sedang'><div align='right'>" . $jumlah_sedang_obgyn . "</div></td>" .
						"<td id='rl36_sedang' style='display:none;'>" . $jumlah_sedang_obgyn . "</td>" .
						"<td id='rl36_f_kecil'><div align='right'>" . $jumlah_kecil_obgyn . "</div></td>" .
						"<td id='rl36_kecil' style='display:none;'>" . $jumlah_kecil_obgyn . "</td>" .
					"</tr>";
			$html .= "<tr>" .
						"<td id='rl36_kode_rs'>" . $kode_rs . "</td>" .
						"<td id='rl36_nama_rs'>" . $nama_rs . "</td>" .
						"<td id='rl36_kode_propinsi'>" . $kode_propinsi . "</td>" .
						"<td id='rl36_kabupaten_kota'>" . $kabupaten_kota . "</td>" .
						"<td id='rl36_tahun'>" . $tahun . "</td>" .
						"<td id='rl36_nomor'>3</td>" .
						"<td id='rl36_spesialisasi'><small>Bedah Saraf</small></td>" .
						"<td id='rl36_f_total'><div align='right'>" . $jumlah_total_bedah_saraf . "</div></td>" .
						"<td id='rl36_total' style='display:none;'>" . $jumlah_total_bedah_saraf . "</td>" .
						"<td id='rl36_f_khusus'><div align='right'>" . $jumlah_khusus_bedah_saraf . "</div></td>" .
						"<td id='rl36_khusus' style='display:none;'>" . $jumlah_khusus_bedah_saraf . "</td>" .
						"<td id='rl36_f_besar'><div align='right'>" . $jumlah_besar_bedah_saraf . "</div></td>" .
						"<td id='rl36_besar' style='display:none;'>" . $jumlah_besar_bedah_saraf . "</td>" .
						"<td id='rl36_f_sedang'><div align='right'>" . $jumlah_sedang_bedah_saraf . "</div></td>" .
						"<td id='rl36_sedang' style='display:none;'>" . $jumlah_sedang_bedah_saraf . "</td>" .
						"<td id='rl36_f_kecil'><div align='right'>" . $jumlah_kecil_bedah_saraf . "</div></td>" .
						"<td id='rl36_kecil' style='display:none;'>" . $jumlah_kecil_bedah_saraf . "</td>" .
					"</tr>";
			$html .= "<tr>" .
						"<td id='rl36_kode_rs'>" . $kode_rs . "</td>" .
						"<td id='rl36_nama_rs'>" . $nama_rs . "</td>" .
						"<td id='rl36_kode_propinsi'>" . $kode_propinsi . "</td>" .
						"<td id='rl36_kabupaten_kota'>" . $kabupaten_kota . "</td>" .
						"<td id='rl36_tahun'>" . $tahun . "</td>" .
						"<td id='rl36_nomor'>4</td>" .
						"<td id='rl36_spesialisasi'><small>T H T</small></td>" .
						"<td id='rl36_f_total'><div align='right'>" . $jumlah_total_tht . "</div></td>" .
						"<td id='rl36_total' style='display:none;'>" . $jumlah_total_tht . "</td>" .
						"<td id='rl36_f_khusus'><div align='right'>" . $jumlah_khusus_tht . "</div></td>" .
						"<td id='rl36_khusus' style='display:none;'>" . $jumlah_khusus_tht . "</td>" .
						"<td id='rl36_f_besar'><div align='right'>" . $jumlah_besar_tht . "</div></td>" .
						"<td id='rl36_besar' style='display:none;'>" . $jumlah_besar_tht . "</td>" .
						"<td id='rl36_f_sedang'><div align='right'>" . $jumlah_sedang_tht . "</div></td>" .
						"<td id='rl36_sedang' style='display:none;'>" . $jumlah_sedang_tht . "</td>" .
						"<td id='rl36_f_kecil'><div align='right'>" . $jumlah_kecil_tht . "</div></td>" .
						"<td id='rl36_kecil' style='display:none;'>" . $jumlah_kecil_tht . "</td>" .
					"</tr>";
			$html .= "<tr>" .
						"<td id='rl36_kode_rs'>" . $kode_rs . "</td>" .
						"<td id='rl36_nama_rs'>" . $nama_rs . "</td>" .
						"<td id='rl36_kode_propinsi'>" . $kode_propinsi . "</td>" .
						"<td id='rl36_kabupaten_kota'>" . $kabupaten_kota . "</td>" .
						"<td id='rl36_tahun'>" . $tahun . "</td>" .
						"<td id='rl36_nomor'>5</td>" .
						"<td id='rl36_spesialisasi'><small>Mata</small></td>" .
						"<td id='rl36_f_total'><div align='right'>" . $jumlah_total_mata . "</div></td>" .
						"<td id='rl36_total' style='display:none;'>" . $jumlah_total_mata . "</td>" .
						"<td id='rl36_f_khusus'><div align='right'>" . $jumlah_khusus_mata . "</div></td>" .
						"<td id='rl36_khusus' style='display:none;'>" . $jumlah_khusus_mata . "</td>" .
						"<td id='rl36_f_besar'><div align='right'>" . $jumlah_besar_mata . "</div></td>" .
						"<td id='rl36_besar' style='display:none;'>" . $jumlah_besar_mata . "</td>" .
						"<td id='rl36_f_sedang'><div align='right'>" . $jumlah_sedang_mata . "</div></td>" .
						"<td id='rl36_sedang' style='display:none;'>" . $jumlah_sedang_mata . "</td>" .
						"<td id='rl36_f_kecil'><div align='right'>" . $jumlah_kecil_mata . "</div></td>" .
						"<td id='rl36_kecil' style='display:none;'>" . $jumlah_kecil_mata . "</td>" .
					"</tr>";
			$html .= "<tr>" .
						"<td id='rl36_kode_rs'>" . $kode_rs . "</td>" .
						"<td id='rl36_nama_rs'>" . $nama_rs . "</td>" .
						"<td id='rl36_kode_propinsi'>" . $kode_propinsi . "</td>" .
						"<td id='rl36_kabupaten_kota'>" . $kabupaten_kota . "</td>" .
						"<td id='rl36_tahun'>" . $tahun . "</td>" .
						"<td id='rl36_nomor'>6</td>" .
						"<td id='rl36_spesialisasi'><small>Kulit & Kelamin</small></td>" .
						"<td id='rl36_f_total'><div align='right'>" . $jumlah_total_kk . "</div></td>" .
						"<td id='rl36_total' style='display:none;'>" . $jumlah_total_kk . "</td>" .
						"<td id='rl36_f_khusus'><div align='right'>" . $jumlah_khusus_kk . "</div></td>" .
						"<td id='rl36_khusus' style='display:none;'>" . $jumlah_khusus_kk . "</td>" .
						"<td id='rl36_f_besar'><div align='right'>" . $jumlah_besar_kk . "</div></td>" .
						"<td id='rl36_besar' style='display:none;'>" . $jumlah_besar_kk . "</td>" .
						"<td id='rl36_f_sedang'><div align='right'>" . $jumlah_sedang_kk . "</div></td>" .
						"<td id='rl36_sedang' style='display:none;'>" . $jumlah_sedang_kk . "</td>" .
						"<td id='rl36_f_kecil'><div align='right'>" . $jumlah_kecil_kk . "</div></td>" .
						"<td id='rl36_kecil' style='display:none;'>" . $jumlah_kecil_kk . "</td>" .
					"</tr>";
			$html .= "<tr>" .
						"<td id='rl36_kode_rs'>" . $kode_rs . "</td>" .
						"<td id='rl36_nama_rs'>" . $nama_rs . "</td>" .
						"<td id='rl36_kode_propinsi'>" . $kode_propinsi . "</td>" .
						"<td id='rl36_kabupaten_kota'>" . $kabupaten_kota . "</td>" .
						"<td id='rl36_tahun'>" . $tahun . "</td>" .
						"<td id='rl36_nomor'>7</td>" .
						"<td id='rl36_spesialisasi'><small>Gigi & Mulut</small></td>" .
						"<td id='rl36_f_total'><div align='right'>" . $jumlah_total_gigi_mulut . "</div></td>" .
						"<td id='rl36_total' style='display:none;'>" . $jumlah_total_gigi_mulut . "</td>" .
						"<td id='rl36_f_khusus'><div align='right'>" . $jumlah_khusus_gigi_mulut . "</div></td>" .
						"<td id='rl36_khusus' style='display:none;'>" . $jumlah_khusus_gigi_mulut . "</td>" .
						"<td id='rl36_f_besar'><div align='right'>" . $jumlah_besar_gigi_mulut . "</div></td>" .
						"<td id='rl36_besar' style='display:none;'>" . $jumlah_besar_gigi_mulut . "</td>" .
						"<td id='rl36_f_sedang'><div align='right'>" . $jumlah_sedang_gigi_mulut . "</div></td>" .
						"<td id='rl36_sedang' style='display:none;'>" . $jumlah_sedang_gigi_mulut . "</td>" .
						"<td id='rl36_f_kecil'><div align='right'>" . $jumlah_kecil_gigi_mulut . "</div></td>" .
						"<td id='rl36_kecil' style='display:none;'>" . $jumlah_kecil_gigi_mulut . "</td>" .
					"</tr>";
			$html .= "<tr>" .
						"<td id='rl36_kode_rs'>" . $kode_rs . "</td>" .
						"<td id='rl36_nama_rs'>" . $nama_rs . "</td>" .
						"<td id='rl36_kode_propinsi'>" . $kode_propinsi . "</td>" .
						"<td id='rl36_kabupaten_kota'>" . $kabupaten_kota . "</td>" .
						"<td id='rl36_tahun'>" . $tahun . "</td>" .
						"<td id='rl36_nomor'>8</td>" .
						"<td id='rl36_spesialisasi'><small>Bedah Anak</small></td>" .
						"<td id='rl36_f_total'><div align='right'>" . $jumlah_total_bedah_anak . "</div></td>" .
						"<td id='rl36_total' style='display:none;'>" . $jumlah_total_bedah_anak . "</td>" .
						"<td id='rl36_f_khusus'><div align='right'>" . $jumlah_khusus_bedah_anak . "</div></td>" .
						"<td id='rl36_khusus' style='display:none;'>" . $jumlah_khusus_bedah_anak . "</td>" .
						"<td id='rl36_f_besar'><div align='right'>" . $jumlah_besar_bedah_anak . "</div></td>" .
						"<td id='rl36_besar' style='display:none;'>" . $jumlah_besar_bedah_anak . "</td>" .
						"<td id='rl36_f_sedang'><div align='right'>" . $jumlah_sedang_bedah_anak . "</div></td>" .
						"<td id='rl36_sedang' style='display:none;'>" . $jumlah_sedang_bedah_anak . "</td>" .
						"<td id='rl36_f_kecil'><div align='right'>" . $jumlah_kecil_bedah_anak . "</div></td>" .
						"<td id='rl36_kecil' style='display:none;'>" . $jumlah_kecil_bedah_anak . "</td>" .
					"</tr>";
			$html .= "<tr>" .
						"<td id='rl36_kode_rs'>" . $kode_rs . "</td>" .
						"<td id='rl36_nama_rs'>" . $nama_rs . "</td>" .
						"<td id='rl36_kode_propinsi'>" . $kode_propinsi . "</td>" .
						"<td id='rl36_kabupaten_kota'>" . $kabupaten_kota . "</td>" .
						"<td id='rl36_tahun'>" . $tahun . "</td>" .
						"<td id='rl36_nomor'>9</td>" .
						"<td id='rl36_spesialisasi'><small>Kardiovaskuler</small></td>" .
						"<td id='rl36_f_total'><div align='right'>" . $jumlah_total_kardiovaskuler . "</div></td>" .
						"<td id='rl36_total' style='display:none;'>" . $jumlah_total_kardiovaskuler . "</td>" .
						"<td id='rl36_f_khusus'><div align='right'>" . $jumlah_khusus_kardiovaskuler . "</div></td>" .
						"<td id='rl36_khusus' style='display:none;'>" . $jumlah_khusus_kardiovaskuler . "</td>" .
						"<td id='rl36_f_besar'><div align='right'>" . $jumlah_besar_kardiovaskuler . "</div></td>" .
						"<td id='rl36_besar' style='display:none;'>" . $jumlah_besar_kardiovaskuler . "</td>" .
						"<td id='rl36_f_sedang'><div align='right'>" . $jumlah_sedang_kardiovaskuler . "</div></td>" .
						"<td id='rl36_sedang' style='display:none;'>" . $jumlah_sedang_kardiovaskuler . "</td>" .
						"<td id='rl36_f_kecil'><div align='right'>" . $jumlah_kecil_kardiovaskuler . "</div></td>" .
						"<td id='rl36_kecil' style='display:none;'>" . $jumlah_kecil_kardiovaskuler . "</td>" .
					"</tr>";
			$html .= "<tr>" .
						"<td id='rl36_kode_rs'>" . $kode_rs . "</td>" .
						"<td id='rl36_nama_rs'>" . $nama_rs . "</td>" .
						"<td id='rl36_kode_propinsi'>" . $kode_propinsi . "</td>" .
						"<td id='rl36_kabupaten_kota'>" . $kabupaten_kota . "</td>" .
						"<td id='rl36_tahun'>" . $tahun . "</td>" .
						"<td id='rl36_nomor'>10</td>" .
						"<td id='rl36_spesialisasi'><small>Bedah Orthopedi</small></td>" .
						"<td id='rl36_f_total'><div align='right'>" . $jumlah_total_bedah_orthopedi . "</div></td>" .
						"<td id='rl36_total' style='display:none;'>" . $jumlah_total_bedah_orthopedi . "</td>" .
						"<td id='rl36_f_khusus'><div align='right'>" . $jumlah_khusus_bedah_orthopedi . "</div></td>" .
						"<td id='rl36_khusus' style='display:none;'>" . $jumlah_khusus_bedah_orthopedi . "</td>" .
						"<td id='rl36_f_besar'><div align='right'>" . $jumlah_besar_bedah_orthopedi . "</div></td>" .
						"<td id='rl36_besar' style='display:none;'>" . $jumlah_besar_bedah_orthopedi . "</td>" .
						"<td id='rl36_f_sedang'><div align='right'>" . $jumlah_sedang_bedah_orthopedi . "</div></td>" .
						"<td id='rl36_sedang' style='display:none;'>" . $jumlah_sedang_bedah_orthopedi . "</td>" .
						"<td id='rl36_f_kecil'><div align='right'>" . $jumlah_kecil_bedah_orthopedi . "</div></td>" .
						"<td id='rl36_kecil' style='display:none;'>" . $jumlah_kecil_bedah_orthopedi . "</td>" .
					"</tr>";
			$html .= "<tr>" .
						"<td id='rl36_kode_rs'>" . $kode_rs . "</td>" .
						"<td id='rl36_nama_rs'>" . $nama_rs . "</td>" .
						"<td id='rl36_kode_propinsi'>" . $kode_propinsi . "</td>" .
						"<td id='rl36_kabupaten_kota'>" . $kabupaten_kota . "</td>" .
						"<td id='rl36_tahun'>" . $tahun . "</td>" .
						"<td id='rl36_nomor'>11</td>" .
						"<td id='rl36_spesialisasi'><small>Thorak</small></td>" .
						"<td id='rl36_f_total'><div align='right'>" . $jumlah_total_thorak . "</div></td>" .
						"<td id='rl36_total' style='display:none;'>" . $jumlah_total_thorak . "</td>" .
						"<td id='rl36_f_khusus'><div align='right'>" . $jumlah_khusus_thorak . "</div></td>" .
						"<td id='rl36_khusus' style='display:none;'>" . $jumlah_khusus_thorak . "</td>" .
						"<td id='rl36_f_besar'><div align='right'>" . $jumlah_besar_thorak . "</div></td>" .
						"<td id='rl36_besar' style='display:none;'>" . $jumlah_besar_thorak . "</td>" .
						"<td id='rl36_f_sedang'><div align='right'>" . $jumlah_sedang_thorak . "</div></td>" .
						"<td id='rl36_sedang' style='display:none;'>" . $jumlah_sedang_thorak . "</td>" .
						"<td id='rl36_f_kecil'><div align='right'>" . $jumlah_kecil_thorak . "</div></td>" .
						"<td id='rl36_kecil' style='display:none;'>" . $jumlah_kecil_thorak . "</td>" .
					"</tr>";
			$html .= "<tr>" .
						"<td id='rl36_kode_rs'>" . $kode_rs . "</td>" .
						"<td id='rl36_nama_rs'>" . $nama_rs . "</td>" .
						"<td id='rl36_kode_propinsi'>" . $kode_propinsi . "</td>" .
						"<td id='rl36_kabupaten_kota'>" . $kabupaten_kota . "</td>" .
						"<td id='rl36_tahun'>" . $tahun . "</td>" .
						"<td id='rl36_nomor'>12</td>" .
						"<td id='rl36_spesialisasi'><small>Digestive</small></td>" .
						"<td id='rl36_f_total'><div align='right'>" . $jumlah_total_digestive . "</div></td>" .
						"<td id='rl36_total' style='display:none;'>" . $jumlah_total_digestive . "</td>" .
						"<td id='rl36_f_khusus'><div align='right'>" . $jumlah_khusus_digestive . "</div></td>" .
						"<td id='rl36_khusus' style='display:none;'>" . $jumlah_khusus_digestive . "</td>" .
						"<td id='rl36_f_besar'><div align='right'>" . $jumlah_besar_digestive . "</div></td>" .
						"<td id='rl36_besar' style='display:none;'>" . $jumlah_besar_digestive . "</td>" .
						"<td id='rl36_f_sedang'><div align='right'>" . $jumlah_sedang_digestive . "</div></td>" .
						"<td id='rl36_sedang' style='display:none;'>" . $jumlah_sedang_digestive . "</td>" .
						"<td id='rl36_f_kecil'><div align='right'>" . $jumlah_kecil_digestive . "</div></td>" .
						"<td id='rl36_kecil' style='display:none;'>" . $jumlah_kecil_digestive . "</td>" .
					"</tr>";
			$html .= "<tr>" .
						"<td id='rl36_kode_rs'>" . $kode_rs . "</td>" .
						"<td id='rl36_nama_rs'>" . $nama_rs . "</td>" .
						"<td id='rl36_kode_propinsi'>" . $kode_propinsi . "</td>" .
						"<td id='rl36_kabupaten_kota'>" . $kabupaten_kota . "</td>" .
						"<td id='rl36_tahun'>" . $tahun . "</td>" .
						"<td id='rl36_nomor'>13</td>" .
						"<td id='rl36_spesialisasi'><small>Urologi</small></td>" .
						"<td id='rl36_f_total'><div align='right'>" . $jumlah_total_urologi . "</div></td>" .
						"<td id='rl36_total' style='display:none;'>" . $jumlah_total_urologi . "</td>" .
						"<td id='rl36_f_khusus'><div align='right'>" . $jumlah_khusus_urologi . "</div></td>" .
						"<td id='rl36_khusus' style='display:none;'>" . $jumlah_khusus_urologi . "</td>" .
						"<td id='rl36_f_besar'><div align='right'>" . $jumlah_besar_urologi . "</div></td>" .
						"<td id='rl36_besar' style='display:none;'>" . $jumlah_besar_urologi . "</td>" .
						"<td id='rl36_f_sedang'><div align='right'>" . $jumlah_sedang_urologi . "</div></td>" .
						"<td id='rl36_sedang' style='display:none;'>" . $jumlah_sedang_urologi . "</td>" .
						"<td id='rl36_f_kecil'><div align='right'>" . $jumlah_kecil_urologi . "</div></td>" .
						"<td id='rl36_kecil' style='display:none;'>" . $jumlah_kecil_urologi . "</td>" .
					"</tr>";
			$html .= "<tr>" .
						"<td id='rl36_kode_rs'>" . $kode_rs . "</td>" .
						"<td id='rl36_nama_rs'>" . $nama_rs . "</td>" .
						"<td id='rl36_kode_propinsi'>" . $kode_propinsi . "</td>" .
						"<td id='rl36_kabupaten_kota'>" . $kabupaten_kota . "</td>" .
						"<td id='rl36_tahun'>" . $tahun . "</td>" .
						"<td id='rl36_nomor'>14</td>" .
						"<td id='rl36_spesialisasi'><small>Lain-Lain</small></td>" .
						"<td id='rl36_f_total'><div align='right'>" . $jumlah_total_lain_lain . "</div></td>" .
						"<td id='rl36_total' style='display:none;'>" . $jumlah_total_lain_lain . "</td>" .
						"<td id='rl36_f_khusus'><div align='right'>" . $jumlah_khusus_lain_lain . "</div></td>" .
						"<td id='rl36_khusus' style='display:none;'>" . $jumlah_khusus_lain_lain . "</td>" .
						"<td id='rl36_f_besar'><div align='right'>" . $jumlah_besar_lain_lain . "</div></td>" .
						"<td id='rl36_besar' style='display:none;'>" . $jumlah_besar_lain_lain . "</td>" .
						"<td id='rl36_f_sedang'><div align='right'>" . $jumlah_sedang_lain_lain . "</div></td>" .
						"<td id='rl36_sedang' style='display:none;'>" . $jumlah_sedang_lain_lain . "</td>" .
						"<td id='rl36_f_kecil'><div align='right'>" . $jumlah_kecil_lain_lain . "</div></td>" .
						"<td id='rl36_kecil' style='display:none;'>" . $jumlah_kecil_lain_lain . "</td>" .
					"</tr>";
			$data['html'] = $html;
			echo json_encode($data);
		} else if ($_POST['command'] == "export_xls") {
			require_once ("smis-libs-out/php-excel/PHPExcel.php");

		    $file = new PHPExcel ();
		    $file->getProperties ()->setCreator ( "PT. Inovasi Ide Utama" );
		    $file->getProperties ()->setTitle ( "RL 3.6 - Pembedahan" );
		    $file->getProperties ()->setSubject ( "RL 3.6 - Pembedahan" );
		    $file->getProperties ()->setDescription ( "RL 3.6 - Pembedahan Generated by System" );
		    $file->getProperties ()->setKeywords ( "RL 3.6 - Pembedahan" );
		    $file->getProperties ()->setCategory ( "RL 3.6 - Pembedahan" );

		    $sheet = $file->getActiveSheet ();
		    $i = 1;

		    $sheet->mergeCells("A".$i.":L".$i)->setCellValue("A".$i,"RL 3.6 - Pembedahan");
		    $sheet->getStyle("A".$i.":L".$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		    $sheet->getStyle("A".$i)->getFont()->setBold(true);
		    $i = $i + 2;

		    $border_start = $i;
		    
		    $sheet->setCellValue("A".$i,"KODE PROPINSI");
		    $sheet->setCellValue("B".$i,"KAB/KOTA");
		    $sheet->setCellValue("C".$i,"KODE RS");
		    $sheet->setCellValue("D".$i,"NAMA RS");
		    $sheet->setCellValue("E".$i,"TAHUN");
		    $sheet->setCellValue("F".$i,"NO. URUT");
		    $sheet->setCellValue("G".$i,"SPESIALISASI");
		    $sheet->setCellValue("H".$i,"TOTAL");
		    $sheet->setCellValue("I".$i,"KHUSUS");
		    $sheet->setCellValue("J".$i,"BESAR");
		    $sheet->setCellValue("K".$i,"SEDANG");
		    $sheet->setCellValue("L".$i,"KECIL");
		    $sheet->getStyle("A".$i.":L".$i)->getFont()->setBold(true);
		    $sheet->getStyle("A".$i.":L".$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		    $sheet->getStyle("A".$i.":L".$i)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

		    $data = json_decode($_POST['detail']);
		    foreach ($data as $d) {
			    $i = $i + 1;
			    $sheet->setCellValue("A".$i,$d->kode_propinsi);
			    $sheet->setCellValue("B".$i,$d->kabupaten_kota);
			    $sheet->setCellValue("C".$i,$d->kode_propinsi);
			    $sheet->setCellValue("D".$i,$d->nama_rs);
			    $sheet->setCellValue("E".$i,$d->tahun);
			    $sheet->setCellValue("F".$i,$d->nomor);
			    $sheet->setCellValue("G".$i,$d->spesialisasi);
			    $sheet->setCellValue("H".$i,$d->total);
			    $sheet->setCellValue("I".$i,$d->khusus);
			    $sheet->setCellValue("J".$i,$d->besar);
			    $sheet->setCellValue("K".$i,$d->sedang);
			    $sheet->setCellValue("L".$i,$d->kecil);
			}

		    $thin = array ();
		    $thin['borders']=array();
		    $thin['borders']['allborders']=array();
		    $thin['borders']['allborders']['style']=PHPExcel_Style_Border::BORDER_THIN ;
		    $sheet->getStyle ( "A".$border_start.":L".$i )->applyFromArray ($thin);

		    foreach (range('A', 'L') as $columnID) {
		        $sheet->getColumnDimension($columnID)->setAutoSize(true);
		    }

		    header ( 'Content-Type: application/vnd.ms-excel' );
		    header ( 'Content-Disposition: attachment;filename="RL 3.6 - Pembedahan.xls"' );
		    header ( 'Cache-Control: max-age=0' );
		    $writer = PHPExcel_IOFactory::createWriter ( $file, 'Excel5' );
		    $writer->save ( 'php://output' );
		}
		return;
	}
	$modal = $uitable->getModal();
	$form = $modal->getForm();
	$form->setTitle("RL 3.6 - Pembedahan");

	$bulan_option = new OptionBuilder();
	$bulan_option->add("Semua", "%%", "1");
	$bulan_option->add("Januari", "1");
	$bulan_option->add("Februari", "2");
	$bulan_option->add("Maret", "3");
	$bulan_option->add("April", "4");
	$bulan_option->add("Mei", "5");
	$bulan_option->add("Juni", "6");
	$bulan_option->add("Juli", "7");
	$bulan_option->add("Agustus", "8");
	$bulan_option->add("September", "9");
	$bulan_option->add("Oktober", "10");
	$bulan_option->add("Nopember", "11");
	$bulan_option->add("Desember", "12");
	$bulan_select = new Select("rl36_bulan_filter", "rl36_bulan_filter", $bulan_option->getContent());
	$form->addElement("Bulan", $bulan_select);
	$tahun_rows = $db->get_result("
		SELECT DISTINCT YEAR(tanggal_mulai) tahun
		FROM smis_mr_operasi
		WHERE prop = '' AND YEAR(tanggal_mulai) > 0
		ORDER BY YEAR(tanggal_mulai) ASC
	");
	$tahun_option = new OptionBuilder();
	if ($tahun_rows != null) {
		foreach ($tahun_rows as $tahun_row) {
			if (date("Y") == $tahun_row->tahun)
				$tahun_option->add($tahun_row->tahun, $tahun_row->tahun, "1");
			else
				$tahun_option->add($tahun_row->tahun, $tahun_row->tahun);
		}
	}
	$tahun_select = new Select("rl36_tahun_filter", "rl36_tahun_filter", $tahun_option->getContent());
	$form->addElement("Tahun", $tahun_select);
	$excel_button = new Button("", "", "");
	$excel_button 	->addClass("btn-primary")
					->setIsButton(Button::$ICONIC)
					->setIcon("fa fa-file-excel-o")
					->setAction("rl36.excel()");
	$process_button = new Button("", "", "");
	$process_button	->addClass("btn-inverse")
					-> setIsButton(Button::$ICONIC)
					->setIcon("fa fa-circle-o")
					->setAction("rl36.view()");
	$button_group = new ButtonGroup("noprint");
	$button_group->addButton($process_button);
	$button_group->addButton($excel_button);
	$form->addElement("", $button_group);

	echo $form->getHtml();
	echo $uitable->getHtml();
	echo addJS("framework/smis/js/table_action.js");
	echo addJS("medical_record/resource/js/rl36.js", false);
?>