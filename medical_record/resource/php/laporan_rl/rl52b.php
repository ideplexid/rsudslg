<?php
global $db;
show_error();
require_once 'smis-base/smis-include-service-consumer.php';
if( isset ( $_POST ['super_command'] ) &&  $_POST ['super_command']=="collect_ruang" ){
	require_once "smis-libs-class/ServiceProviderList.php";
	$query="UPDATE smis_mr_rl52b SET jumlah=0";
	$db->query($query);
	$list_iklin=new ServiceProviderList($db, "get_rl52");
	$list_iklin->execute();
	$content=$list_iklin->getContent();
	$pack=new ResponsePackage();
	$pack->setContent($content);
	$pack->setStatus(ResponsePackage::$STATUS_OK);
	echo json_encode($pack->getPackage());
	return;
}

if (isset ( $_POST ['super_command'] ) && $_POST ['super_command'] == "sync") {
	require_once 'medical_record/class/adapter/SyncRL52BAdapter.php';
	$service = new ServiceConsumer ( $db, "get_rl52",NULL,$_POST['ruang'] );
	$service->addData("dari", $_POST['dari']);
	$service->addData("sampai", $_POST['sampai']);
	$service->addData("origin", $_POST['origin']);
	$service->execute ();
	$content = $service->getContent ();
	$adapter = new SyncRL52BAdapter ();
	$table_sync = $adapter->getContent($content);	
	foreach($table_sync as $one){
		$query="UPDATE smis_mr_rl52b SET jumlah=jumlah+".$one['jumlah']." WHERE layanan='".$one['layanan']."' OR layanan='TOTAL'";
		$db->query($query);
	}
	$response = new ResponsePackage ();
	$response->setAlertVisible ( false );
	$response->setStatus ( ResponsePackage::$STATUS_OK );
	$response->setContent($content);
	echo json_encode ( $response->getPackage () );
	return;
}


$head=array ("No.",'Pelayanan','Jumlah');
$uitable = new Table ( $head, "", NULL, true );
$uitable->setName ( "rl52b" );
$uitable->setFooterVisible(false);
$uitable->setAction(false);
if (isset ( $_POST ['command'] )) {	
	$adapter = new SimpleAdapter ();
	$adapter->add ( "No.", "id","back." );
	$adapter->add ( "Pelayanan", "layanan");
	$adapter->add ( "Jumlah", "jumlah" );
	$dbtable = new DBTable ( $db, "smis_mr_rl52b" );
	$dbtable->setShowAll(true);
	$dbres = new DBResponder ( $dbtable, $uitable, $adapter );
	$data = $dbres->command ( $_POST ['command'] );
	echo json_encode ( $data );
	return;
}

$query="SELECT DISTINCT origin FROM smis_mr_diagnosa WHERE prop!='del' ";
$option=new OptionBuilder();
$option->add(" - Semua - ","",1);

$res=$db->get_result($query);
foreach($res as $x){
    if($x->origin==""){
        continue;
    }
    $option->add(ArrayAdapter::slugFormat("unslug",$x->origin),$x->origin);
}

$uitable->addModal ( "dari", "date", "Dari", "" );
$uitable->addModal ( "sampai", "date", "Sampai", "" );
$uitable->addModal( "origin", "select", "Asal", $option->getContent() );
$modal = $uitable->getModal ();
$form=$modal->getForm();
$view=new Button("", "", "");
$view->addClass("btn-primary")
	 ->setIsButton(Button::$ICONIC)
	 ->setIcon("fa fa-refresh")
	 ->setAction("rl52b.collect_ruang()");
$form->addElement("", $view);

$load=new LoadingBar("rekap_mr_rl52b_bar", "");
$modal=new Modal("rekap_mr_rl52b_modal", "", "Processing...");
$modal	->addHTML($load->getHtml(),"after");

echo $form->getHtml();
echo $uitable->getHtml ();
echo $modal->getHtml ();
echo addJS ( "framework/smis/js/table_action.js" );
echo addJS ( "framework/bootstrap/js/bootstrap-datepicker.js" );
echo addCSS ( "framework/bootstrap/css/datepicker.css" );
echo addJS ( "base-js/smis-base-loading.js");
?>
<script type="text/javascript">

var rl52b;
//var employee;
$(document).ready(function(){
	$(".mydate").datepicker();
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
	var column=new Array('id','nama','keterangan','slug');
	rl52b=new TableAction("rl52b","medical_record","rl52b",column);
	rl52b.sync=function(set,curpoint){
		var total=set.length;
		var data=this.getRegulerData();
		var current=set[curpoint];	
		data['ruang']=current.value;	
		data['super_command']="sync";
		data['dari']=$("#rl52b_dari").val();
		data['sampai']=$("#rl52b_sampai").val();		
		data['origin']=$("#rl52b_origin").val();		
		$.post('',data,function(res){
			
			$("#rekap_mr_rl52b_bar").sload("true","[ "+curpoint+" / "+total+" ] "+current.name+"...",(curpoint*100/total));
			var json=getContent(res);
			$("#layanan_table_list").append(json.row);
			
			if(curpoint+1>=total){
				$("#rekap_mr_rl52b_modal").modal("hide");
				rl52b.view();
				return;
			} else {
				curpoint++;
				setTimeout(function(){
					rl52b.sync(set,curpoint);
				},500);
			}
			
		});
	};

	rl52b.collect_ruang=function(){
		var data=this.getRegulerData();
		data['super_command']="collect_ruang";
		showLoading();
		$.post('',data,function(res){
			dismissLoading();
			var json=getContent(res);
			if(json==null){
				return;
			}else{
				$("#rekap_mr_rl52b_bar").sload("true","Loading Data",0);
				$("#rekap_mr_rl52b_modal").modal("show");
				$("#rl52b_list").html("");
				rl52b.sync(json,0);
			}
			
		});
	}
	

});
</script>
