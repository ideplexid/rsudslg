<?php
	require_once("smis-base/smis-include-service-consumer.php");
	global $db;

	$head = array('Kode Propinsi', 'Kab/Kota', 'Kode RS', 'Nama RS', 'Tahun', 'No.', 'Jenis Tindakan', 'Jumlah');
	$uitable = new Table($head, "", null, true);
	$uitable->setName("rl39");
	$uitable->setAction(false);
	$uitable->setFooterVisible(false);
	$uitable->setHeaderVisible(false);
	$uitable->addHeader("before", "
		<tr>
			<th>Kode Propinsi</th>
			<th>Kab/Kota</th>
			<th>Kode RS</th>
			<th>Nama RS</th>
			<th>Tahun</th>
			<th>No.</th>
			<th>Jenis Tindakan</th>
			<th>Jumlah</th>
		</tr>
	");

	if (isset($_POST ['command'])) {
		if ($_POST['command'] == "view") {
			$tahun = $_POST['tahun'];
			$bulan = $_POST['bulan'];
			$html = "";

			$kode_rs = "";
			$nama_rs = "";
			$kode_propinsi = "";
			$kabupaten_kota = "";
			$info_rs_row = $db->get_row("
				SELECT 
					*
				FROM 
					smis_mr_data_dasar_rs
				WHERE 
					prop = ''
				LIMIT 
					0, 1
			");
			if ($info_rs_row != null) {
				$kode_rs = $info_rs_row->koders;
				$nama_rs = $info_rs_row->nama_rs;
				$kode_propinsi = $info_rs_row->kode_provinsi;
				$kabupaten_kota = $info_rs_row->kota_kabupaten;
			}

			$medis_gait_analyzer = 0;
			$medis_e_m_g = 0;
			$medis_uro_dinamic = 0;
			$medis_side_back = 0;
			$medis_e_n_tree = 0;
			$medis_spyrometer = 0;
			$medis_static_bicycle = 0;
			$medis_tread_mill = 0;
			$medis_body_platysmograf = 0;
			$medis_lain_lain = 0;
			$fisioterapi_latihan_fisik = 0;
			$fisioterapi_aktinoterapi = 0;$fisioterapi_elektroterapi = 0;
			$fisioterapi_hidroterapi = 0;
			$fisioterapi_traksi_lumbal_cervical = 0;
			$fisioterapi_lain_lain = 0;
			$okupasiterapi_snoosien_room = 0;
			$okupasiterapi_sensori_integrasi = 0;
			$okupasiterapi_latihan_aktivitas_kehidupan_sehari_hari = 0;
			$okupasiterapi_proper_body_mekanik = 0;
			$okupasiterapi_pembuatan_alat_lontar_adaptasi_alat = 0;
			$okupasiterapi_analisa_persiapan_kerja = 0;
			$okupasiterapi_latihan_relaksasi = 0;
			$okupasiterapi_analisa_intervensi_persepsi_kognitif_psikomot = 0;
			$okupasiterapi_lain_lain = 0;
			$terapi_wicara_fungsi_bicara = 0;
			$terapi_wicara_fungsi_bahasa_laku = 0;
			$terapi_wicara_fungsi_menelan = 0;
			$terapi_wicara_lain_lain = 0;
			$psikologi_psikolog_anak = 0;
			$psikologi_psikolog_dewasa = 0;
			$psikologi_lain_lain = 0;
			$sosial_medis_evaluasi_lingkungan_rumah = 0;
			$sosial_medis_evaluasi_ekonomi = 0;
			$sosial_medis_evaluasi_pekerjaan = 0;
			$sosial_medis_lain_lain = 0;
			$ortotik_prostetik_pembuatan_alat_bantu = 0;
			$ortotik_prostetik_pembuatan_alat_anggota_tiruan = 0;
			$ortotik_prostetik_lain_lain = 0;
			$kunjungan_rumah = 0;

			$rehabilitasi_medik_row = $db->get_row("
				SELECT 
					SUM(IF(medis_gait_analyzer = 1, 1, 0)) medis_gait_analyzer,
					SUM(IF(medis_e_m_g = 1, 1, 0)) medis_e_m_g,
					SUM(IF(medis_uro_dinamic = 1, 1, 0)) medis_uro_dinamic,
					SUM(IF(medis_side_back = 1, 1, 0)) medis_side_back,
					SUM(IF(medis_e_n_tree = 1, 1, 0)) medis_e_n_tree,
					SUM(IF(medis_spyrometer = 1, 1, 0)) medis_spyrometer,
					SUM(IF(medis_static_bicycle = 1, 1, 0)) medis_static_bicycle,
					SUM(IF(medis_tread_mill = 1, 1, 0)) medis_tread_mill,
					SUM(IF(medis_body_platysmograf = 1, 1, 0)) medis_body_platysmograf,
					SUM(IF(medis_lain_lain = 1, 1, 0)) medis_lain_lain,
					SUM(IF(fisioterapi_latihan_fisik = 1, 1, 0)) fisioterapi_latihan_fisik,
					SUM(IF(fisioterapi_aktinoterapi = 1, 1, 0)) fisioterapi_aktinoterapi,
					SUM(IF(fisioterapi_elektroterapi = 1, 1, 0)) fisioterapi_elektroterapi,
					SUM(IF(fisioterapi_hidroterapi = 1, 1, 0)) fisioterapi_hidroterapi,
					SUM(IF(fisioterapi_traksi_lumbal_cervical = 1, 1, 0)) fisioterapi_traksi_lumbal_cervical,
					SUM(IF(fisioterapi_lain_lain = 1, 1, 0)) fisioterapi_lain_lain,
					SUM(IF(okupasiterapi_snoosien_room = 1, 1, 0)) okupasiterapi_snoosien_room,
					SUM(IF(okupasiterapi_sensori_integrasi = 1, 1, 0)) okupasiterapi_sensori_integrasi,
					SUM(IF(okupasiterapi_latihan_aktivitas_kehidupan_sehari_hari = 1, 1, 0)) okupasiterapi_latihan_aktivitas_kehidupan_sehari_hari,
					SUM(IF(okupasiterapi_proper_body_mekanik = 1, 1, 0)) okupasiterapi_proper_body_mekanik,
					SUM(IF(okupasiterapi_pembuatan_alat_lontar_adaptasi_alat = 1, 1, 0)),
					SUM(IF(okupasiterapi_analisa_persiapan_kerja = 1, 1, 0)) okupasiterapi_analisa_persiapan_kerja,
					SUM(IF(okupasiterapi_latihan_relaksasi = 1, 1, 0)) okupasiterapi_latihan_relaksasi,
					SUM(IF(okupasiterapi_analisa_intervensi_persepsi_kognitif_psikomot = 1, 1, 0)) okupasiterapi_analisa_intervensi_persepsi_kognitif_psikomot,
					SUM(IF(okupasiterapi_lain_lain = 1, 1, 0)) okupasiterapi_lain_lain,
					SUM(IF(terapi_wicara_fungsi_bicara = 1, 1, 0)) terapi_wicara_fungsi_bicara,
					SUM(IF(terapi_wicara_fungsi_bahasa_laku = 1, 1, 0)) terapi_wicara_fungsi_bahasa_laku,
					SUM(IF(terapi_wicara_fungsi_menelan = 1, 1, 0)) terapi_wicara_fungsi_menelan,
					SUM(IF(terapi_wicara_lain_lain = 1, 1, 0)) terapi_wicara_lain_lain,
					SUM(IF(psikologi_psikolog_anak = 1, 1, 0)) psikologi_psikolog_anak,
					SUM(IF(psikologi_psikolog_dewasa = 1, 1, 0)) psikologi_psikolog_dewasa,
					SUM(IF(psikologi_lain_lain = 1, 1, 0)) psikologi_lain_lain,
					SUM(IF(sosial_medis_evaluasi_lingkungan_rumah = 1, 1, 0)) sosial_medis_evaluasi_lingkungan_rumah,
					SUM(IF(sosial_medis_evaluasi_ekonomi = 1, 1, 0)) sosial_medis_evaluasi_ekonomi,
					SUM(IF(sosial_medis_evaluasi_pekerjaan = 1, 1, 0)) sosial_medis_evaluasi_pekerjaan,
					SUM(IF(sosial_medis_lain_lain = 1, 1, 0)) sosial_medis_lain_lain,
					SUM(IF(ortotik_prostetik_pembuatan_alat_bantu = 1, 1, 0)) ortotik_prostetik_pembuatan_alat_bantu,
					SUM(IF(ortotik_prostetik_pembuatan_alat_anggota_tiruan = 1, 1, 0)) ortotik_prostetik_pembuatan_alat_anggota_tiruan,
					SUM(IF(ortotik_prostetik_lain_lain = 1, 1, 0)) ortotik_prostetik_lain_lain,
					SUM(IF(kunjungan_rumah = 1, 1, 0)) kunjungan_rumah
				FROM 
					smis_mr_rehabilitasi_medik
				WHERE
					prop = '' AND YEAR(tanggal) = '" . $tahun . "' AND MONTH(tanggal) LIKE '" . $bulan . "'
			");
			if ($rehabilitasi_medik_row != null) {
				$medis_gait_analyzer = $rehabilitasi_medik_row->medis_gait_analyzer;
				$medis_e_m_g = $rehabilitasi_medik_row->medis_e_m_g;
				$medis_uro_dinamic = $rehabilitasi_medik_row->medis_uro_dinamic;
				$medis_side_back = $rehabilitasi_medik_row->medis_side_back;
				$medis_e_n_tree = $rehabilitasi_medik_row->medis_e_n_tree;
				$medis_spyrometer = $rehabilitasi_medik_row->medis_spyrometer;
				$medis_static_bicycle = $rehabilitasi_medik_row->medis_static_bicycle;
				$medis_tread_mill = $rehabilitasi_medik_row->medis_tread_mill;
				$medis_body_platysmograf = $rehabilitasi_medik_row->medis_body_platysmograf;
				$medis_lain_lain = $rehabilitasi_medik_row->medis_lain_lain;
				$fisioterapi_latihan_fisik = $rehabilitasi_medik_row->fisioterapi_latihan_fisik;
				$fisioterapi_aktinoterapi = $rehabilitasi_medik_row->fisioterapi_aktinoterapi;
				$fisioterapi_elektroterapi = $rehabilitasi_medik_row->fisioterapi_elektroterapi;
				$fisioterapi_hidroterapi = $rehabilitasi_medik_row->fisioterapi_hidroterapi;
				$fisioterapi_traksi_lumbal_cervical = $rehabilitasi_medik_row->fisioterapi_traksi_lumbal_cervical;
				$fisioterapi_lain_lain = $rehabilitasi_medik_row->fisioterapi_lain_lain;
				$okupasiterapi_snoosien_room = $rehabilitasi_medik_row->okupasiterapi_snoosien_room;
				$okupasiterapi_sensori_integrasi = $rehabilitasi_medik_row->okupasiterapi_sensori_integrasi;
				$okupasiterapi_latihan_aktivitas_kehidupan_sehari_hari = $rehabilitasi_medik_row->okupasiterapi_latihan_aktivitas_kehidupan_sehari_hari;
				$okupasiterapi_proper_body_mekanik = $rehabilitasi_medik_row->okupasiterapi_proper_body_mekanik;
				$okupasiterapi_pembuatan_alat_lontar_adaptasi_alat = $rehabilitasi_medik_row->okupasiterapi_pembuatan_alat_lontar_adaptasi_alat;
				$okupasiterapi_analisa_persiapan_kerja = $rehabilitasi_medik_row->okupasiterapi_analisa_persiapan_kerja;
				$okupasiterapi_latihan_relaksasi = $rehabilitasi_medik_row->okupasiterapi_latihan_relaksasi;
				$okupasiterapi_analisa_intervensi_persepsi_kognitif_psikomot = $rehabilitasi_medik_row->okupasiterapi_analisa_intervensi_persepsi_kognitif_psikomot;
				$okupasiterapi_lain_lain = $rehabilitasi_medik_row->okupasiterapi_lain_lain;
				$terapi_wicara_fungsi_bicara = $rehabilitasi_medik_row->terapi_wicara_fungsi_bicara;
				$terapi_wicara_fungsi_bahasa_laku = $rehabilitasi_medik_row->terapi_wicara_fungsi_bahasa_laku;
				$terapi_wicara_fungsi_menelan = $rehabilitasi_medik_row->terapi_wicara_fungsi_menelan;
				$terapi_wicara_lain_lain = $rehabilitasi_medik_row->terapi_wicara_lain_lain;
				$psikologi_psikolog_anak = $rehabilitasi_medik_row->psikologi_psikolog_anak;
				$psikologi_psikolog_dewasa = $rehabilitasi_medik_row->psikologi_psikolog_dewasa;
				$psikologi_lain_lain = $rehabilitasi_medik_row->psikologi_lain_lain;
				$sosial_medis_evaluasi_lingkungan_rumah = $rehabilitasi_medik_row->sosial_medis_evaluasi_lingkungan_rumah;
				$sosial_medis_evaluasi_ekonomi = $rehabilitasi_medik_row->sosial_medis_evaluasi_ekonomi;
				$sosial_medis_evaluasi_pekerjaan = $rehabilitasi_medik_row->sosial_medis_evaluasi_pekerjaan;
				$sosial_medis_lain_lain = $rehabilitasi_medik_row->sosial_medis_lain_lain;
				$ortotik_prostetik_pembuatan_alat_bantu = $rehabilitasi_medik_row->ortotik_prostetik_pembuatan_alat_bantu;
				$ortotik_prostetik_pembuatan_alat_anggota_tiruan = $rehabilitasi_medik_row->ortotik_prostetik_pembuatan_alat_anggota_tiruan;
				$ortotik_prostetik_lain_lain = $rehabilitasi_medik_row->ortotik_prostetik_lain_lain;
				$kunjungan_rumah = $rehabilitasi_medik_row->kunjungan_rumah;

			}

			$html .= 	"<tr class='data_rl39'>" .
							"<td id='rl39_kode_propinsi'>" . $kode_propinsi . "</td>" .
							"<td id='rl39_kabupaten_kota'>" . $kabupaten_kota . "</td>" .
							"<td id='rl39_kode_rs'>" . $kode_rs . "</td>" .
							"<td id='rl39_nama_rs'>" . $nama_rs . "</td>" .
							"<td id='rl39_tahun'>" . $tahun . "</td>" .
							"<td id='rl39_nomor'>1</td>" .
							"<td id='rl39_jenis_tindakan'><strong>Medis</strong></td>" .
							"<td id='rl39_f_dirujuk'><div align='right'></div></td>" .
							"<td id='rl39_dirujuk' style='display:none;'></td>" .
						"</tr>";
			$html .= 	"<tr class='data_rl39'>" .
							"<td id='rl39_kode_propinsi'>" . $kode_propinsi . "</td>" .
							"<td id='rl39_kabupaten_kota'>" . $kabupaten_kota . "</td>" .
							"<td id='rl39_kode_rs'>" . $kode_rs . "</td>" .
							"<td id='rl39_nama_rs'>" . $nama_rs . "</td>" .
							"<td id='rl39_tahun'>" . $tahun . "</td>" .
							"<td id='rl39_nomor'>1.1</td>" .
							"<td id='rl39_jenis_tindakan'>Gait Analyzer</td>" .
							"<td id='rl39_f_jumlah'><div align='right'>" . $medis_gait_analyzer . "</div></td>" .
							"<td id='rl39_jumlah' style='display:none;'>" . $medis_gait_analyzer . "</td>" .
						"</tr>";
			$html .= 	"<tr class='data_rl39'>" .
							"<td id='rl39_kode_propinsi'>" . $kode_propinsi . "</td>" .
							"<td id='rl39_kabupaten_kota'>" . $kabupaten_kota . "</td>" .
							"<td id='rl39_kode_rs'>" . $kode_rs . "</td>" .
							"<td id='rl39_nama_rs'>" . $nama_rs . "</td>" .
							"<td id='rl39_tahun'>" . $tahun . "</td>" .
							"<td id='rl39_nomor'>1.2</td>" .
							"<td id='rl39_jenis_tindakan'>E M G</td>" .
							"<td id='rl39_f_jumlah'><div align='right'>" . $medis_e_m_g . "</div></td>" .
							"<td id='rl39_jumlah' style='display:none;'>" . $medis_e_m_g . "</td>" .
						"</tr>";
			$html .= 	"<tr class='data_rl39'>" .
							"<td id='rl39_kode_propinsi'>" . $kode_propinsi . "</td>" .
							"<td id='rl39_kabupaten_kota'>" . $kabupaten_kota . "</td>" .
							"<td id='rl39_kode_rs'>" . $kode_rs . "</td>" .
							"<td id='rl39_nama_rs'>" . $nama_rs . "</td>" .
							"<td id='rl39_tahun'>" . $tahun . "</td>" .
							"<td id='rl39_nomor'>1.3</td>" .
							"<td id='rl39_jenis_tindakan'>Uro Dinamic</td>" .
							"<td id='rl39_f_jumlah'><div align='right'>" . $medis_uro_dinamic . "</div></td>" .
							"<td id='rl39_jumlah' style='display:none;'>" . $medis_uro_dinamic . "</td>" .
						"</tr>";
			$html .= 	"<tr class='data_rl39'>" .
							"<td id='rl39_kode_propinsi'>" . $kode_propinsi . "</td>" .
							"<td id='rl39_kabupaten_kota'>" . $kabupaten_kota . "</td>" .
							"<td id='rl39_kode_rs'>" . $kode_rs . "</td>" .
							"<td id='rl39_nama_rs'>" . $nama_rs . "</td>" .
							"<td id='rl39_tahun'>" . $tahun . "</td>" .
							"<td id='rl39_nomor'>1.4</td>" .
							"<td id='rl39_jenis_tindakan'>Side Back</td>" .
							"<td id='rl39_f_jumlah'><div align='right'>" . $medis_side_back . "</div></td>" .
							"<td id='rl39_jumlah' style='display:none;'>" . $medis_side_back . "</td>" .
						"</tr>";
			$html .= 	"<tr class='data_rl39'>" .
							"<td id='rl39_kode_propinsi'>" . $kode_propinsi . "</td>" .
							"<td id='rl39_kabupaten_kota'>" . $kabupaten_kota . "</td>" .
							"<td id='rl39_kode_rs'>" . $kode_rs . "</td>" .
							"<td id='rl39_nama_rs'>" . $nama_rs . "</td>" .
							"<td id='rl39_tahun'>" . $tahun . "</td>" .
							"<td id='rl39_nomor'>1.5</td>" .
							"<td id='rl39_jenis_tindakan'>E N Tree</td>" .
							"<td id='rl39_f_jumlah'><div align='right'>" . $medis_e_n_tree . "</div></td>" .
							"<td id='rl39_jumlah' style='display:none;'>" . $medis_e_n_tree . "</td>" .
						"</tr>";
			$html .= 	"<tr class='data_rl39'>" .
							"<td id='rl39_kode_propinsi'>" . $kode_propinsi . "</td>" .
							"<td id='rl39_kabupaten_kota'>" . $kabupaten_kota . "</td>" .
							"<td id='rl39_kode_rs'>" . $kode_rs . "</td>" .
							"<td id='rl39_nama_rs'>" . $nama_rs . "</td>" .
							"<td id='rl39_tahun'>" . $tahun . "</td>" .
							"<td id='rl39_nomor'>1.6</td>" .
							"<td id='rl39_jenis_tindakan'>Spyrometer</td>" .
							"<td id='rl39_f_jumlah'><div align='right'>" . $medis_spyrometer . "</div></td>" .
							"<td id='rl39_jumlah' style='display:none;'>" . $medis_spyrometer . "</td>" .
						"</tr>";
			$html .= 	"<tr class='data_rl39'>" .
							"<td id='rl39_kode_propinsi'>" . $kode_propinsi . "</td>" .
							"<td id='rl39_kabupaten_kota'>" . $kabupaten_kota . "</td>" .
							"<td id='rl39_kode_rs'>" . $kode_rs . "</td>" .
							"<td id='rl39_nama_rs'>" . $nama_rs . "</td>" .
							"<td id='rl39_tahun'>" . $tahun . "</td>" .
							"<td id='rl39_nomor'>1.7</td>" .
							"<td id='rl39_jenis_tindakan'>Static Bicycle</td>" .
							"<td id='rl39_f_jumlah'><div align='right'>" . $medis_static_bicycle . "</div></td>" .
							"<td id='rl39_jumlah' style='display:none;'>" . $medis_static_bicycle . "</td>" .
						"</tr>";
			$html .= 	"<tr class='data_rl39'>" .
							"<td id='rl39_kode_propinsi'>" . $kode_propinsi . "</td>" .
							"<td id='rl39_kabupaten_kota'>" . $kabupaten_kota . "</td>" .
							"<td id='rl39_kode_rs'>" . $kode_rs . "</td>" .
							"<td id='rl39_nama_rs'>" . $nama_rs . "</td>" .
							"<td id='rl39_tahun'>" . $tahun . "</td>" .
							"<td id='rl39_nomor'>1.8</td>" .
							"<td id='rl39_jenis_tindakan'>Tread Mill</td>" .
							"<td id='rl39_f_jumlah'><div align='right'>" . $medis_tread_mill . "</div></td>" .
							"<td id='rl39_jumlah' style='display:none;'>" . $medis_tread_mill . "</td>" .
						"</tr>";
			$html .= 	"<tr class='data_rl39'>" .
							"<td id='rl39_kode_propinsi'>" . $kode_propinsi . "</td>" .
							"<td id='rl39_kabupaten_kota'>" . $kabupaten_kota . "</td>" .
							"<td id='rl39_kode_rs'>" . $kode_rs . "</td>" .
							"<td id='rl39_nama_rs'>" . $nama_rs . "</td>" .
							"<td id='rl39_tahun'>" . $tahun . "</td>" .
							"<td id='rl39_nomor'>1.9</td>" .
							"<td id='rl39_jenis_tindakan'>Body Platysmograf</td>" .
							"<td id='rl39_f_jumlah'><div align='right'>" . $medis_body_platysmograf . "</div></td>" .
							"<td id='rl39_jumlah' style='display:none;'>" . $medis_body_platysmograf . "</td>" .
						"</tr>";

			$html .= 	"<tr class='data_rl39'>" .
							"<td id='rl39_kode_propinsi'>" . $kode_propinsi . "</td>" .
							"<td id='rl39_kabupaten_kota'>" . $kabupaten_kota . "</td>" .
							"<td id='rl39_kode_rs'>" . $kode_rs . "</td>" .
							"<td id='rl39_nama_rs'>" . $nama_rs . "</td>" .
							"<td id='rl39_tahun'>" . $tahun . "</td>" .
							"<td id='rl39_nomor'>2</td>" .
							"<td id='rl39_jenis_tindakan'><strong>Fisioterapi</strong></td>" .
							"<td id='rl39_f_dirujuk'><div align='right'></div></td>" .
							"<td id='rl39_dirujuk' style='display:none;'></td>" .
						"</tr>";
			$html .= 	"<tr class='data_rl39'>" .
							"<td id='rl39_kode_propinsi'>" . $kode_propinsi . "</td>" .
							"<td id='rl39_kabupaten_kota'>" . $kabupaten_kota . "</td>" .
							"<td id='rl39_kode_rs'>" . $kode_rs . "</td>" .
							"<td id='rl39_nama_rs'>" . $nama_rs . "</td>" .
							"<td id='rl39_tahun'>" . $tahun . "</td>" .
							"<td id='rl39_nomor'>2.1</td>" .
							"<td id='rl39_jenis_tindakan'>Latihan Fisik</td>" .
							"<td id='rl39_f_jumlah'><div align='right'>" . $fisioterapi_latihan_fisik . "</div></td>" .
							"<td id='rl39_jumlah' style='display:none;'>" . $fisioterapi_latihan_fisik . "</td>" .
						"</tr>";
			$html .= 	"<tr class='data_rl39'>" .
							"<td id='rl39_kode_propinsi'>" . $kode_propinsi . "</td>" .
							"<td id='rl39_kabupaten_kota'>" . $kabupaten_kota . "</td>" .
							"<td id='rl39_kode_rs'>" . $kode_rs . "</td>" .
							"<td id='rl39_nama_rs'>" . $nama_rs . "</td>" .
							"<td id='rl39_tahun'>" . $tahun . "</td>" .
							"<td id='rl39_nomor'>2.2</td>" .
							"<td id='rl39_jenis_tindakan'>Aktinoterapi</td>" .
							"<td id='rl39_f_jumlah'><div align='right'>" . $fisioterapi_aktinoterapi . "</div></td>" .
							"<td id='rl39_jumlah' style='display:none;'>" . $fisioterapi_aktinoterapi . "</td>" .
						"</tr>";
			$html .= 	"<tr class='data_rl39'>" .
							"<td id='rl39_kode_propinsi'>" . $kode_propinsi . "</td>" .
							"<td id='rl39_kabupaten_kota'>" . $kabupaten_kota . "</td>" .
							"<td id='rl39_kode_rs'>" . $kode_rs . "</td>" .
							"<td id='rl39_nama_rs'>" . $nama_rs . "</td>" .
							"<td id='rl39_tahun'>" . $tahun . "</td>" .
							"<td id='rl39_nomor'>2.3</td>" .
							"<td id='rl39_jenis_tindakan'>Elektroterapi</td>" .
							"<td id='rl39_f_jumlah'><div align='right'>" . $fisioterapi_elektroterapi . "</div></td>" .
							"<td id='rl39_jumlah' style='display:none;'>" . $fisioterapi_elektroterapi . "</td>" .
						"</tr>";
			$html .= 	"<tr class='data_rl39'>" .
							"<td id='rl39_kode_propinsi'>" . $kode_propinsi . "</td>" .
							"<td id='rl39_kabupaten_kota'>" . $kabupaten_kota . "</td>" .
							"<td id='rl39_kode_rs'>" . $kode_rs . "</td>" .
							"<td id='rl39_nama_rs'>" . $nama_rs . "</td>" .
							"<td id='rl39_tahun'>" . $tahun . "</td>" .
							"<td id='rl39_nomor'>2.4</td>" .
							"<td id='rl39_jenis_tindakan'>Hidroterapi</td>" .
							"<td id='rl39_f_jumlah'><div align='right'>" . $fisioterapi_hidroterapi . "</div></td>" .
							"<td id='rl39_jumlah' style='display:none;'>" . $fisioterapi_hidroterapi . "</td>" .
						"</tr>";
			$html .= 	"<tr class='data_rl39'>" .
							"<td id='rl39_kode_propinsi'>" . $kode_propinsi . "</td>" .
							"<td id='rl39_kabupaten_kota'>" . $kabupaten_kota . "</td>" .
							"<td id='rl39_kode_rs'>" . $kode_rs . "</td>" .
							"<td id='rl39_nama_rs'>" . $nama_rs . "</td>" .
							"<td id='rl39_tahun'>" . $tahun . "</td>" .
							"<td id='rl39_nomor'>2.5</td>" .
							"<td id='rl39_jenis_tindakan'>Traksi Lumbal & Cervical</td>" .
							"<td id='rl39_f_jumlah'><div align='right'>" . $fisioterapi_traksi_lumbal_cervical . "</div></td>" .
							"<td id='rl39_jumlah' style='display:none;'>" . $fisioterapi_traksi_lumbal_cervical . "</td>" .
						"</tr>";
			$html .= 	"<tr class='data_rl39'>" .
							"<td id='rl39_kode_propinsi'>" . $kode_propinsi . "</td>" .
							"<td id='rl39_kabupaten_kota'>" . $kabupaten_kota . "</td>" .
							"<td id='rl39_kode_rs'>" . $kode_rs . "</td>" .
							"<td id='rl39_nama_rs'>" . $nama_rs . "</td>" .
							"<td id='rl39_tahun'>" . $tahun . "</td>" .
							"<td id='rl39_nomor'>2.6</td>" .
							"<td id='rl39_jenis_tindakan'>Lain-Lain</td>" .
							"<td id='rl39_f_jumlah'><div align='right'>" . $fisioterapi_lain_lain . "</div></td>" .
							"<td id='rl39_jumlah' style='display:none;'>" . $fisioterapi_lain_lain . "</td>" .
						"</tr>";

			$html .= 	"<tr class='data_rl39'>" .
							"<td id='rl39_kode_propinsi'>" . $kode_propinsi . "</td>" .
							"<td id='rl39_kabupaten_kota'>" . $kabupaten_kota . "</td>" .
							"<td id='rl39_kode_rs'>" . $kode_rs . "</td>" .
							"<td id='rl39_nama_rs'>" . $nama_rs . "</td>" .
							"<td id='rl39_tahun'>" . $tahun . "</td>" .
							"<td id='rl39_nomor'>3</td>" .
							"<td id='rl39_jenis_tindakan'><strong>Okupasiterapi</strong></td>" .
							"<td id='rl39_f_dirujuk'><div align='right'></div></td>" .
							"<td id='rl39_dirujuk' style='display:none;'></td>" .
						"</tr>";
			$html .= 	"<tr class='data_rl39'>" .
							"<td id='rl39_kode_propinsi'>" . $kode_propinsi . "</td>" .
							"<td id='rl39_kabupaten_kota'>" . $kabupaten_kota . "</td>" .
							"<td id='rl39_kode_rs'>" . $kode_rs . "</td>" .
							"<td id='rl39_nama_rs'>" . $nama_rs . "</td>" .
							"<td id='rl39_tahun'>" . $tahun . "</td>" .
							"<td id='rl39_nomor'>3.1</td>" .
							"<td id='rl39_jenis_tindakan'>Snoosien Room</td>" .
							"<td id='rl39_f_jumlah'><div align='right'>" . $okupasiterapi_snoosien_room . "</div></td>" .
							"<td id='rl39_jumlah' style='display:none;'>" . $okupasiterapi_snoosien_room . "</td>" .
						"</tr>";
			$html .= 	"<tr class='data_rl39'>" .
							"<td id='rl39_kode_propinsi'>" . $kode_propinsi . "</td>" .
							"<td id='rl39_kabupaten_kota'>" . $kabupaten_kota . "</td>" .
							"<td id='rl39_kode_rs'>" . $kode_rs . "</td>" .
							"<td id='rl39_nama_rs'>" . $nama_rs . "</td>" .
							"<td id='rl39_tahun'>" . $tahun . "</td>" .
							"<td id='rl39_nomor'>3.2</td>" .
							"<td id='rl39_jenis_tindakan'>Sensori Integrasi</td>" .
							"<td id='rl39_f_jumlah'><div align='right'>" . $okupasiterapi_sensori_integrasi . "</div></td>" .
							"<td id='rl39_jumlah' style='display:none;'>" . $okupasiterapi_sensori_integrasi . "</td>" .
						"</tr>";
			$html .= 	"<tr class='data_rl39'>" .
							"<td id='rl39_kode_propinsi'>" . $kode_propinsi . "</td>" .
							"<td id='rl39_kabupaten_kota'>" . $kabupaten_kota . "</td>" .
							"<td id='rl39_kode_rs'>" . $kode_rs . "</td>" .
							"<td id='rl39_nama_rs'>" . $nama_rs . "</td>" .
							"<td id='rl39_tahun'>" . $tahun . "</td>" .
							"<td id='rl39_nomor'>3.3</td>" .
							"<td id='rl39_jenis_tindakan'>Latihan Aktivitas Kehidupan Sehari-hari</td>" .
							"<td id='rl39_f_jumlah'><div align='right'>" . $okupasiterapi_latihan_aktivitas_kehidupan_sehari_hari . "</div></td>" .
							"<td id='rl39_jumlah' style='display:none;'>" . $okupasiterapi_latihan_aktivitas_kehidupan_sehari_hari . "</td>" .
						"</tr>";
			$html .= 	"<tr class='data_rl39'>" .
							"<td id='rl39_kode_propinsi'>" . $kode_propinsi . "</td>" .
							"<td id='rl39_kabupaten_kota'>" . $kabupaten_kota . "</td>" .
							"<td id='rl39_kode_rs'>" . $kode_rs . "</td>" .
							"<td id='rl39_nama_rs'>" . $nama_rs . "</td>" .
							"<td id='rl39_tahun'>" . $tahun . "</td>" .
							"<td id='rl39_nomor'>3.4</td>" .
							"<td id='rl39_jenis_tindakan'>Proper Body Mekanik</td>" .
							"<td id='rl39_f_jumlah'><div align='right'>" . $okupasiterapi_proper_body_mekanik . "</div></td>" .
							"<td id='rl39_jumlah' style='display:none;'>" . $okupasiterapi_proper_body_mekanik . "</td>" .
						"</tr>";
			$html .= 	"<tr class='data_rl39'>" .
							"<td id='rl39_kode_propinsi'>" . $kode_propinsi . "</td>" .
							"<td id='rl39_kabupaten_kota'>" . $kabupaten_kota . "</td>" .
							"<td id='rl39_kode_rs'>" . $kode_rs . "</td>" .
							"<td id='rl39_nama_rs'>" . $nama_rs . "</td>" .
							"<td id='rl39_tahun'>" . $tahun . "</td>" .
							"<td id='rl39_nomor'>3.5</td>" .
							"<td id='rl39_jenis_tindakan'>Pembuatan Alat Lontar & Adaptasi Alat</td>" .
							"<td id='rl39_f_jumlah'><div align='right'>" . $okupasiterapi_pembuatan_alat_lontar_adaptasi_alat . "</div></td>" .
							"<td id='rl39_jumlah' style='display:none;'>" . $okupasiterapi_pembuatan_alat_lontar_adaptasi_alat . "</td>" .
						"</tr>";
			$html .= 	"<tr class='data_rl39'>" .
							"<td id='rl39_kode_propinsi'>" . $kode_propinsi . "</td>" .
							"<td id='rl39_kabupaten_kota'>" . $kabupaten_kota . "</td>" .
							"<td id='rl39_kode_rs'>" . $kode_rs . "</td>" .
							"<td id='rl39_nama_rs'>" . $nama_rs . "</td>" .
							"<td id='rl39_tahun'>" . $tahun . "</td>" .
							"<td id='rl39_nomor'>3.6</td>" .
							"<td id='rl39_jenis_tindakan'>Analisa Persiapan Kerja</td>" .
							"<td id='rl39_f_jumlah'><div align='right'>" . $okupasiterapi_analisa_persiapan_kerja . "</div></td>" .
							"<td id='rl39_jumlah' style='display:none;'>" . $okupasiterapi_analisa_persiapan_kerja . "</td>" .
						"</tr>";
			$html .= 	"<tr class='data_rl39'>" .
							"<td id='rl39_kode_propinsi'>" . $kode_propinsi . "</td>" .
							"<td id='rl39_kabupaten_kota'>" . $kabupaten_kota . "</td>" .
							"<td id='rl39_kode_rs'>" . $kode_rs . "</td>" .
							"<td id='rl39_nama_rs'>" . $nama_rs . "</td>" .
							"<td id='rl39_tahun'>" . $tahun . "</td>" .
							"<td id='rl39_nomor'>3.7</td>" .
							"<td id='rl39_jenis_tindakan'>Latihan Relaksasi</td>" .
							"<td id='rl39_f_jumlah'><div align='right'>" . $okupasiterapi_latihan_relaksasi . "</div></td>" .
							"<td id='rl39_jumlah' style='display:none;'>" . $okupasiterapi_latihan_relaksasi . "</td>" .
						"</tr>";
			$html .= 	"<tr class='data_rl39'>" .
							"<td id='rl39_kode_propinsi'>" . $kode_propinsi . "</td>" .
							"<td id='rl39_kabupaten_kota'>" . $kabupaten_kota . "</td>" .
							"<td id='rl39_kode_rs'>" . $kode_rs . "</td>" .
							"<td id='rl39_nama_rs'>" . $nama_rs . "</td>" .
							"<td id='rl39_tahun'>" . $tahun . "</td>" .
							"<td id='rl39_nomor'>3.8</td>" .
							"<td id='rl39_jenis_tindakan'>Analisa & Intervensi, Persepsi, Kognitif, Psikomot</td>" .
							"<td id='rl39_f_jumlah'><div align='right'>" . $okupasiterapi_analisa_intervensi_persepsi_kognitif_psikomot . "</div></td>" .
							"<td id='rl39_jumlah' style='display:none;'>" . $okupasiterapi_analisa_intervensi_persepsi_kognitif_psikomot . "</td>" .
						"</tr>";
			$html .= 	"<tr class='data_rl39'>" .
							"<td id='rl39_kode_propinsi'>" . $kode_propinsi . "</td>" .
							"<td id='rl39_kabupaten_kota'>" . $kabupaten_kota . "</td>" .
							"<td id='rl39_kode_rs'>" . $kode_rs . "</td>" .
							"<td id='rl39_nama_rs'>" . $nama_rs . "</td>" .
							"<td id='rl39_tahun'>" . $tahun . "</td>" .
							"<td id='rl39_nomor'>3.9</td>" .
							"<td id='rl39_jenis_tindakan'>Lain-Lain</td>" .
							"<td id='rl39_f_jumlah'><div align='right'>" . $okupasiterapi_lain_lain . "</div></td>" .
							"<td id='rl39_jumlah' style='display:none;'>" . $okupasiterapi_lain_lain . "</td>" .
						"</tr>";
			$html .= 	"<tr class='data_rl39'>" .
							"<td id='rl39_kode_propinsi'>" . $kode_propinsi . "</td>" .
							"<td id='rl39_kabupaten_kota'>" . $kabupaten_kota . "</td>" .
							"<td id='rl39_kode_rs'>" . $kode_rs . "</td>" .
							"<td id='rl39_nama_rs'>" . $nama_rs . "</td>" .
							"<td id='rl39_tahun'>" . $tahun . "</td>" .
							"<td id='rl39_nomor'>4</td>" .
							"<td id='rl39_jenis_tindakan'><strong>Terapi Wicara</strong></td>" .
							"<td id='rl39_f_dirujuk'><div align='right'></div></td>" .
							"<td id='rl39_dirujuk' style='display:none;'></td>" .
						"</tr>";
			$html .= 	"<tr class='data_rl39'>" .
							"<td id='rl39_kode_propinsi'>" . $kode_propinsi . "</td>" .
							"<td id='rl39_kabupaten_kota'>" . $kabupaten_kota . "</td>" .
							"<td id='rl39_kode_rs'>" . $kode_rs . "</td>" .
							"<td id='rl39_nama_rs'>" . $nama_rs . "</td>" .
							"<td id='rl39_tahun'>" . $tahun . "</td>" .
							"<td id='rl39_nomor'>4.1</td>" .
							"<td id='rl39_jenis_tindakan'>Fungsi Bicara</td>" .
							"<td id='rl39_f_jumlah'><div align='right'>" . $terapi_wicara_fungsi_bicara . "</div></td>" .
							"<td id='rl39_jumlah' style='display:none;'>" . $terapi_wicara_fungsi_bicara . "</td>" .
						"</tr>";
			$html .= 	"<tr class='data_rl39'>" .
							"<td id='rl39_kode_propinsi'>" . $kode_propinsi . "</td>" .
							"<td id='rl39_kabupaten_kota'>" . $kabupaten_kota . "</td>" .
							"<td id='rl39_kode_rs'>" . $kode_rs . "</td>" .
							"<td id='rl39_nama_rs'>" . $nama_rs . "</td>" .
							"<td id='rl39_tahun'>" . $tahun . "</td>" .
							"<td id='rl39_nomor'>4.2</td>" .
							"<td id='rl39_jenis_tindakan'>Fungsi Bahasa / Laku</td>" .
							"<td id='rl39_f_jumlah'><div align='right'>" . $terapi_wicara_fungsi_bahasa_laku . "</div></td>" .
							"<td id='rl39_jumlah' style='display:none;'>" . $terapi_wicara_fungsi_bahasa_laku . "</td>" .
						"</tr>";
			$html .= 	"<tr class='data_rl39'>" .
							"<td id='rl39_kode_propinsi'>" . $kode_propinsi . "</td>" .
							"<td id='rl39_kabupaten_kota'>" . $kabupaten_kota . "</td>" .
							"<td id='rl39_kode_rs'>" . $kode_rs . "</td>" .
							"<td id='rl39_nama_rs'>" . $nama_rs . "</td>" .
							"<td id='rl39_tahun'>" . $tahun . "</td>" .
							"<td id='rl39_nomor'>4.3</td>" .
							"<td id='rl39_jenis_tindakan'>Fungsi Menelan</td>" .
							"<td id='rl39_f_jumlah'><div align='right'>" . $terapi_wicara_fungsi_menelan . "</div></td>" .
							"<td id='rl39_jumlah' style='display:none;'>" . $terapi_wicara_fungsi_menelan . "</td>" .
						"</tr>";
			$html .= 	"<tr class='data_rl39'>" .
							"<td id='rl39_kode_propinsi'>" . $kode_propinsi . "</td>" .
							"<td id='rl39_kabupaten_kota'>" . $kabupaten_kota . "</td>" .
							"<td id='rl39_kode_rs'>" . $kode_rs . "</td>" .
							"<td id='rl39_nama_rs'>" . $nama_rs . "</td>" .
							"<td id='rl39_tahun'>" . $tahun . "</td>" .
							"<td id='rl39_nomor'>4.4</td>" .
							"<td id='rl39_jenis_tindakan'>Lain-Lain</td>" .
							"<td id='rl39_f_jumlah'><div align='right'>" . $terapi_wicara_lain_lain . "</div></td>" .
							"<td id='rl39_jumlah' style='display:none;'>" . $terapi_wicara_lain_lain . "</td>" .
						"</tr>";
			$html .= 	"<tr class='data_rl39'>" .
							"<td id='rl39_kode_propinsi'>" . $kode_propinsi . "</td>" .
							"<td id='rl39_kabupaten_kota'>" . $kabupaten_kota . "</td>" .
							"<td id='rl39_kode_rs'>" . $kode_rs . "</td>" .
							"<td id='rl39_nama_rs'>" . $nama_rs . "</td>" .
							"<td id='rl39_tahun'>" . $tahun . "</td>" .
							"<td id='rl39_nomor'>5</td>" .
							"<td id='rl39_jenis_tindakan'><strong>Psikologi</strong></td>" .
							"<td id='rl39_f_dirujuk'><div align='right'></div></td>" .
							"<td id='rl39_dirujuk' style='display:none;'></td>" .
						"</tr>";
			$html .= 	"<tr class='data_rl39'>" .
							"<td id='rl39_kode_propinsi'>" . $kode_propinsi . "</td>" .
							"<td id='rl39_kabupaten_kota'>" . $kabupaten_kota . "</td>" .
							"<td id='rl39_kode_rs'>" . $kode_rs . "</td>" .
							"<td id='rl39_nama_rs'>" . $nama_rs . "</td>" .
							"<td id='rl39_tahun'>" . $tahun . "</td>" .
							"<td id='rl39_nomor'>5.1</td>" .
							"<td id='rl39_jenis_tindakan'>Psikolog Anak</td>" .
							"<td id='rl39_f_jumlah'><div align='right'>" . $psikologi_psikolog_anak . "</div></td>" .
							"<td id='rl39_jumlah' style='display:none;'>" . $psikologi_psikolog_anak . "</td>" .
						"</tr>";
			$html .= 	"<tr class='data_rl39'>" .
							"<td id='rl39_kode_propinsi'>" . $kode_propinsi . "</td>" .
							"<td id='rl39_kabupaten_kota'>" . $kabupaten_kota . "</td>" .
							"<td id='rl39_kode_rs'>" . $kode_rs . "</td>" .
							"<td id='rl39_nama_rs'>" . $nama_rs . "</td>" .
							"<td id='rl39_tahun'>" . $tahun . "</td>" .
							"<td id='rl39_nomor'>5.2</td>" .
							"<td id='rl39_jenis_tindakan'>Psikolog Dewasa</td>" .
							"<td id='rl39_f_jumlah'><div align='right'>" . $psikologi_psikolog_dewasa . "</div></td>" .
							"<td id='rl39_jumlah' style='display:none;'>" . $psikologi_psikolog_dewasa . "</td>" .
						"</tr>";
			$html .= 	"<tr class='data_rl39'>" .
							"<td id='rl39_kode_propinsi'>" . $kode_propinsi . "</td>" .
							"<td id='rl39_kabupaten_kota'>" . $kabupaten_kota . "</td>" .
							"<td id='rl39_kode_rs'>" . $kode_rs . "</td>" .
							"<td id='rl39_nama_rs'>" . $nama_rs . "</td>" .
							"<td id='rl39_tahun'>" . $tahun . "</td>" .
							"<td id='rl39_nomor'>5.3</td>" .
							"<td id='rl39_jenis_tindakan'>Lain-Lain</td>" .
							"<td id='rl39_f_jumlah'><div align='right'>" . $psikologi_lain_lain . "</div></td>" .
							"<td id='rl39_jumlah' style='display:none;'>" . $psikologi_lain_lain . "</td>" .
						"</tr>";
			$html .= 	"<tr class='data_rl39'>" .
							"<td id='rl39_kode_propinsi'>" . $kode_propinsi . "</td>" .
							"<td id='rl39_kabupaten_kota'>" . $kabupaten_kota . "</td>" .
							"<td id='rl39_kode_rs'>" . $kode_rs . "</td>" .
							"<td id='rl39_nama_rs'>" . $nama_rs . "</td>" .
							"<td id='rl39_tahun'>" . $tahun . "</td>" .
							"<td id='rl39_nomor'>6</td>" .
							"<td id='rl39_jenis_tindakan'><strong>Sosial Medis</strong></td>" .
							"<td id='rl39_f_dirujuk'><div align='right'></div></td>" .
							"<td id='rl39_dirujuk' style='display:none;'></td>" .
						"</tr>";
			$html .= 	"<tr class='data_rl39'>" .
							"<td id='rl39_kode_propinsi'>" . $kode_propinsi . "</td>" .
							"<td id='rl39_kabupaten_kota'>" . $kabupaten_kota . "</td>" .
							"<td id='rl39_kode_rs'>" . $kode_rs . "</td>" .
							"<td id='rl39_nama_rs'>" . $nama_rs . "</td>" .
							"<td id='rl39_tahun'>" . $tahun . "</td>" .
							"<td id='rl39_nomor'>6.1</td>" .
							"<td id='rl39_jenis_tindakan'>Evaluasi Lingkungan Rumah</td>" .
							"<td id='rl39_f_jumlah'><div align='right'>" . $sosial_medis_evaluasi_lingkungan_rumah . "</div></td>" .
							"<td id='rl39_jumlah' style='display:none;'>" . $sosial_medis_evaluasi_lingkungan_rumah . "</td>" .
						"</tr>";
			$html .= 	"<tr class='data_rl39'>" .
							"<td id='rl39_kode_propinsi'>" . $kode_propinsi . "</td>" .
							"<td id='rl39_kabupaten_kota'>" . $kabupaten_kota . "</td>" .
							"<td id='rl39_kode_rs'>" . $kode_rs . "</td>" .
							"<td id='rl39_nama_rs'>" . $nama_rs . "</td>" .
							"<td id='rl39_tahun'>" . $tahun . "</td>" .
							"<td id='rl39_nomor'>6.2</td>" .
							"<td id='rl39_jenis_tindakan'>Evaluasi Ekonomi</td>" .
							"<td id='rl39_f_jumlah'><div align='right'>" . $sosial_medis_evaluasi_ekonomi . "</div></td>" .
							"<td id='rl39_jumlah' style='display:none;'>" . $sosial_medis_evaluasi_ekonomi . "</td>" .
						"</tr>";
			$html .= 	"<tr class='data_rl39'>" .
							"<td id='rl39_kode_propinsi'>" . $kode_propinsi . "</td>" .
							"<td id='rl39_kabupaten_kota'>" . $kabupaten_kota . "</td>" .
							"<td id='rl39_kode_rs'>" . $kode_rs . "</td>" .
							"<td id='rl39_nama_rs'>" . $nama_rs . "</td>" .
							"<td id='rl39_tahun'>" . $tahun . "</td>" .
							"<td id='rl39_nomor'>6.3</td>" .
							"<td id='rl39_jenis_tindakan'>Evaluasi Pekerjaan</td>" .
							"<td id='rl39_f_jumlah'><div align='right'>" . $sosial_medis_evaluasi_pekerjaan . "</div></td>" .
							"<td id='rl39_jumlah' style='display:none;'>" . $sosial_medis_evaluasi_pekerjaan . "</td>" .
						"</tr>";
			$html .= 	"<tr class='data_rl39'>" .
							"<td id='rl39_kode_propinsi'>" . $kode_propinsi . "</td>" .
							"<td id='rl39_kabupaten_kota'>" . $kabupaten_kota . "</td>" .
							"<td id='rl39_kode_rs'>" . $kode_rs . "</td>" .
							"<td id='rl39_nama_rs'>" . $nama_rs . "</td>" .
							"<td id='rl39_tahun'>" . $tahun . "</td>" .
							"<td id='rl39_nomor'>6.4</td>" .
							"<td id='rl39_jenis_tindakan'>Lain-Lain</td>" .
							"<td id='rl39_f_jumlah'><div align='right'>" . $terapi_wicara_lain_lain . "</div></td>" .
							"<td id='rl39_jumlah' style='display:none;'>" . $terapi_wicara_lain_lain . "</td>" .
						"</tr>";
			$html .= 	"<tr class='data_rl39'>" .
							"<td id='rl39_kode_propinsi'>" . $kode_propinsi . "</td>" .
							"<td id='rl39_kabupaten_kota'>" . $kabupaten_kota . "</td>" .
							"<td id='rl39_kode_rs'>" . $kode_rs . "</td>" .
							"<td id='rl39_nama_rs'>" . $nama_rs . "</td>" .
							"<td id='rl39_tahun'>" . $tahun . "</td>" .
							"<td id='rl39_nomor'>7</td>" .
							"<td id='rl39_jenis_tindakan'><strong>Ortotik Prostetik</strong></td>" .
							"<td id='rl39_f_dirujuk'><div align='right'></div></td>" .
							"<td id='rl39_dirujuk' style='display:none;'></td>" .
						"</tr>";
			$html .= 	"<tr class='data_rl39'>" .
							"<td id='rl39_kode_propinsi'>" . $kode_propinsi . "</td>" .
							"<td id='rl39_kabupaten_kota'>" . $kabupaten_kota . "</td>" .
							"<td id='rl39_kode_rs'>" . $kode_rs . "</td>" .
							"<td id='rl39_nama_rs'>" . $nama_rs . "</td>" .
							"<td id='rl39_tahun'>" . $tahun . "</td>" .
							"<td id='rl39_nomor'>7.1</td>" .
							"<td id='rl39_jenis_tindakan'>Pembuatan Alat Bantu</td>" .
							"<td id='rl39_f_jumlah'><div align='right'>" . $ortotik_prostetik_pembuatan_alat_bantu . "</div></td>" .
							"<td id='rl39_jumlah' style='display:none;'>" . $ortotik_prostetik_pembuatan_alat_bantu . "</td>" .
						"</tr>";
			$html .= 	"<tr class='data_rl39'>" .
							"<td id='rl39_kode_propinsi'>" . $kode_propinsi . "</td>" .
							"<td id='rl39_kabupaten_kota'>" . $kabupaten_kota . "</td>" .
							"<td id='rl39_kode_rs'>" . $kode_rs . "</td>" .
							"<td id='rl39_nama_rs'>" . $nama_rs . "</td>" .
							"<td id='rl39_tahun'>" . $tahun . "</td>" .
							"<td id='rl39_nomor'>7.2</td>" .
							"<td id='rl39_jenis_tindakan'>Pembuatan Alat Anggota Tiruan</td>" .
							"<td id='rl39_f_jumlah'><div align='right'>" . $ortotik_prostetik_pembuatan_alat_anggota_tiruan . "</div></td>" .
							"<td id='rl39_jumlah' style='display:none;'>" . $ortotik_prostetik_pembuatan_alat_anggota_tiruan . "</td>" .
						"</tr>";
			$html .= 	"<tr class='data_rl39'>" .
							"<td id='rl39_kode_propinsi'>" . $kode_propinsi . "</td>" .
							"<td id='rl39_kabupaten_kota'>" . $kabupaten_kota . "</td>" .
							"<td id='rl39_kode_rs'>" . $kode_rs . "</td>" .
							"<td id='rl39_nama_rs'>" . $nama_rs . "</td>" .
							"<td id='rl39_tahun'>" . $tahun . "</td>" .
							"<td id='rl39_nomor'>7.3</td>" .
							"<td id='rl39_jenis_tindakan'>Lain-Lain</td>" .
							"<td id='rl39_f_jumlah'><div align='right'>" . $ortotik_prostetik_lain_lain . "</div></td>" .
							"<td id='rl39_jumlah' style='display:none;'>" . $ortotik_prostetik_lain_lain . "</td>" .
						"</tr>";
			$html .= 	"<tr class='data_rl39'>" .
							"<td id='rl39_kode_propinsi'>" . $kode_propinsi . "</td>" .
							"<td id='rl39_kabupaten_kota'>" . $kabupaten_kota . "</td>" .
							"<td id='rl39_kode_rs'>" . $kode_rs . "</td>" .
							"<td id='rl39_nama_rs'>" . $nama_rs . "</td>" .
							"<td id='rl39_tahun'>" . $tahun . "</td>" .
							"<td id='rl39_nomor'>8</td>" .
							"<td id='rl39_jenis_tindakan'><strong>Kunjungan Rumah</strong></td>" .
							"<td id='rl39_f_jumlah'><div align='right'>" . $kunjungan_rumah . "</div></td>" .
							"<td id='rl39_jumlah' style='display:none;'>" . $kunjungan_rumah . "</td>" .
						"</tr>";

			$data['html'] = $html;
			echo json_encode($data);
		} else if ($_POST['command'] == "export_xls") {
			require_once ("smis-libs-out/php-excel/PHPExcel.php");

		    $file = new PHPExcel ();
		    $file->getProperties ()->setCreator ( "PT. Inovasi Ide Utama" );
		    $file->getProperties ()->setTitle ( "RL 3.9 - Rehabilitasi Medik" );
		    $file->getProperties ()->setSubject ( "RL 3.9 - Rehabilitasi Medik" );
		    $file->getProperties ()->setDescription ( "RL 3.9 - Rehabilitasi Medik Generated by System" );
		    $file->getProperties ()->setKeywords ( "RL 3.9 - Rehabilitasi Medik" );
		    $file->getProperties ()->setCategory ( "RL 3.9 - Rehabilitasi Medik" );

		    $sheet = $file->getActiveSheet ();
		    $i = 1;

		    $sheet->mergeCells("A".$i.":H".$i)->setCellValue("A".$i,"RL 3.9 - Rehabilitasi Medik");
		    $sheet->getStyle("A".$i.":H".$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		    $sheet->getStyle("A".$i)->getFont()->setBold(true);
		    $i = $i + 2;

		    $border_start = $i;
		    
		    $sheet->setCellValue("A".$i,"KODE PROPINSI");
		    $sheet->setCellValue("B".$i,"KAB/KOTA");
		    $sheet->setCellValue("C".$i,"KODE RS");
		    $sheet->setCellValue("D".$i,"NAMA RS");
		    $sheet->setCellValue("E".$i,"TAHUN");
		    $sheet->setCellValue("F".$i,"NO.");
		    $sheet->setCellValue("G".$i,"JENIS TINDAKAN");
		    $sheet->setCellValue("H".$i,"JUMLAH");
		    $sheet->getStyle("A".$i.":H".$i)->getFont()->setBold(true);
		    $sheet->getStyle("A".$i.":H".$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		    $sheet->getStyle("A".$i.":H".$i)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

		    $data = json_decode($_POST['detail']);
		    foreach ($data as $d) {
		    	$i = $i + 1;
			    $sheet->setCellValue("A".$i,$d->kode_propinsi);
			    $sheet->setCellValue("B".$i,$d->kabupaten_kota);
			    $sheet->setCellValue("C".$i,$d->kode_rs);
			    $sheet->setCellValue("D".$i,$d->nama_rs);
			    $sheet->setCellValue("E".$i,$d->tahun);
			    $sheet->setCellValue("F".$i,"'" . $d->nomor);
			    $sheet->setCellValue("G".$i,$d->jenis_tindakan);
			    $sheet->setCellValue("H".$i,$d->jumlah);
			}

		    $thin = array ();
		    $thin['borders']=array();
		    $thin['borders']['allborders']=array();
		    $thin['borders']['allborders']['style']=PHPExcel_Style_Border::BORDER_THIN ;
		    $sheet->getStyle ( "A".$border_start.":H".$i )->applyFromArray ($thin);

		    foreach (range('A', 'H') as $columnID) {
		        $sheet->getColumnDimension($columnID)->setAutoSize(true);
		    }

		    header ( 'Content-Type: application/vnd.ms-excel' );
		    header ( 'Content-Disposition: attachment;filename="RL 3.9 - Rehabilitasi Medik.xls"' );
		    header ( 'Cache-Control: max-age=0' );
		    $writer = PHPExcel_IOFactory::createWriter ( $file, 'Excel5' );
		    $writer->save ( 'php://output' );
		}

		return;
	}
	$modal = $uitable->getModal();
	$form = $modal->getForm();
	$form->setTitle("RL 3.9 - Rehabilitasi Medik");

	$bulan_option = new OptionBuilder();
	$bulan_option->add("Semua", "%%", "1");
	$bulan_option->add("Januari", "1");
	$bulan_option->add("Februari", "2");
	$bulan_option->add("Maret", "3");
	$bulan_option->add("April", "4");
	$bulan_option->add("Mei", "5");
	$bulan_option->add("Juni", "6");
	$bulan_option->add("Juli", "7");
	$bulan_option->add("Agustus", "8");
	$bulan_option->add("September", "9");
	$bulan_option->add("Oktober", "10");
	$bulan_option->add("Nopember", "11");
	$bulan_option->add("Desember", "12");
	$bulan_select = new Select("rl39_bulan_filter", "rl39_bulan_filter", $bulan_option->getContent());
	$form->addElement("Bulan", $bulan_select);
	$tahun_rows = $db->get_result("
		SELECT DISTINCT YEAR(tanggal) tahun
		FROM smis_mr_rehabilitasi_medik
		WHERE prop = ''
		ORDER BY YEAR(tanggal) ASC
	");
	$tahun_option = new OptionBuilder();
	if ($tahun_rows != null) {
		foreach ($tahun_rows as $tahun_row) {
			if (date("Y") == $tahun_row->tahun)
				$tahun_option->add($tahun_row->tahun, $tahun_row->tahun, "1");
			else
				$tahun_option->add($tahun_row->tahun, $tahun_row->tahun);
		}
	}
	$tahun_select = new Select("rl39_tahun_filter", "rl39_tahun_filter", $tahun_option->getContent());
	$form->addElement("Tahun", $tahun_select);
	$excel_button = new Button("", "", "");
	$excel_button 	->addClass("btn-primary")
					->setIsButton(Button::$ICONIC)
					->setIcon("fa fa-file-excel-o")
					->setAction("rl39.excel()");
	$process_button = new Button("", "", "");
	$process_button	->addClass("btn-inverse")
					-> setIsButton(Button::$ICONIC)
					->setIcon("fa fa-circle-o")
					->setAction("rl39.view()");
	$button_group = new ButtonGroup("noprint");
	$button_group->addButton($process_button);
	$button_group->addButton($excel_button);
	$form->addElement("", $button_group);

	echo $form->getHtml();
	echo $uitable->getHtml();
	echo addJS("framework/smis/js/table_action.js");
	echo addJS("medical_record/resource/js/rl39.js", false);
?>