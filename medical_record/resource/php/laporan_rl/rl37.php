<?php
	require_once("smis-base/smis-include-service-consumer.php");
	global $db;

	$head = array('Kode RS', 'Nama RS', 'Kode Propinsi', 'Kab/Kota', 'Tahun', 'No. Urut', 'Jenis Kegiatan', 'Jumlah');
	$uitable = new Table($head, "", null, true);
	$uitable->setName("rl37");
	$uitable->setAction(false);
	$uitable->setFooterVisible(false);

	if (isset($_POST ['command'])) {
		if ($_POST['command'] == "view") {
			$tahun = $_POST['tahun'];
			$bulan = $_POST['bulan'];

			$kode_rs = "";
			$nama_rs = "";
			$kode_propinsi = "";
			$kabupaten_kota = "";
			$info_rs_row = $db->get_row("
				SELECT 
					*
				FROM 
					smis_mr_data_dasar_rs
				WHERE 
					prop = ''
				LIMIT 
					0, 1
			");
			if ($info_rs_row != null) {
				$kode_rs = $info_rs_row->koders;
				$nama_rs = $info_rs_row->nama_rs;
				$kode_propinsi = $info_rs_row->kode_provinsi;
				$kabupaten_kota = $info_rs_row->kota_kabupaten;
			}

			$jenis_kegiatan = array(
				"RADIODIAGNOSTIK - Foto tanpa bahan kontras" 	=> 0,
				"RADIODIAGNOSTIK - Foto dengan bahan kontras" 	=> 0,
				"RADIODIAGNOSTIK - Foto dengan rol film" 		=> 0,
				"RADIODIAGNOSTIK - Flouroskopi" 				=> 0,
				"RADIODIAGNOSTIK - Foto Gigi" 					=> 0,
				"RADIODIAGNOSTIK - C.T. Scan" 					=> 0,
				"RADIODIAGNOSTIK - Lymphografi" 				=> 0,
				"RADIODIAGNOSTIK - Angiograpi" 					=> 0,
				"RADIODIAGNOSTIK - Lain-Lain" 					=> 0,
				"RADIOTHERAPI - Radiotherapi" 					=> 0,
				"RADIOTHERAPI - Lain-Lain" 						=> 0,
				"KEDOKTERAN NUKLIR - Diagnostik" 				=> 0,
				"KEDOKTERAN NUKLIR - Therapi" 					=> 0,
				"KEDOKTERAN NUKLIR - Lain-Lain" 				=> 0,
				"IMAGING/PENCITRAAN - USG" 						=> 0,
				"IMAGING/PENCITRAAN - MRI" 						=> 0,
				"IMAGING/PENCITRAAN - Lain-Lain" 				=> 0
			);

			$data_rows = $db->get_result("
				SELECT 
					jenis_kegiatan, periksa
				FROM
					smis_rad_pesanan
				WHERE
					prop = '' AND YEAR(tanggal) = '" . $tahun . "' AND MONTH(tanggal) LIKE '" . $bulan . "'
			");
			if ($data_rows != null)
				foreach ($data_rows as $dr) {
					$arr_periksa = json_decode($dr->periksa, true);
					$jumlah_pemeriksaan = 0;
					if (count($arr_periksa) > 0) {
						foreach ($arr_periksa as $key => $value)
							if ($value == "1")
								$jumlah_pemeriksaan += 1;
					}
					$jenis_kegiatan[$dr->jenis_kegiatan] += $jumlah_pemeriksaan;
				}

			$html_radiodiagnostik = 	"<tr>" .
											"<td colspan='8'>RADIODIAGNOSTIK</td>" .
										"</tr>";
			$nomor = 1;
			foreach ($jenis_kegiatan as $nama_kegiatan => $jumlah) {
				if (strpos($nama_kegiatan, "RADIODIAGNOSTIK") !== false) {
					$kegiatan = explode(" - ", $nama_kegiatan)[1];
					$html_radiodiagnostik .= 	"<tr class='rl37_radiodiagnostik'>" .
													"<td id='rl37_kode_rs'><small>" . $kode_rs . "</small></td>" .
													"<td id='rl37_nama_rs'><small>" . $nama_rs . "</small></td>" .
													"<td id='rl37_kode_propinsi'><small>" . $kode_propinsi . "</small></td>" .
													"<td id='rl37_kabupaten_kota'><small>" . $kabupaten_kota . "</small></td>" .
													"<td id='rl37_tahun'><small>" . $tahun . "</small></td>" .
													"<td id='rl37_nomor'><small>" . ($nomor++) . "</small></td>" .
													"<td id='rl37_jenis_kegiatan'><small>" . $kegiatan . "</small></td>" .
													"<td id='rl37_jumlah' style='display:none;'>" . $jumlah . "</td>" .
													"<td id='rl37_f_jumlah'><div align='right'><small>" . $jumlah . "</small></div></td>" .
												"</tr>";
				}
			}
			$html_radiotherapi = 		"<tr>" .
											"<td colspan='8'>RADIOTHERAPI</td>" .
										"</tr>";
			$nomor = 1;
			foreach ($jenis_kegiatan as $nama_kegiatan => $jumlah) {
				if (strpos($nama_kegiatan, "RADIOTHERAPI") !== false) {
					$kegiatan = explode(" - ", $nama_kegiatan)[1];
					$html_radiotherapi .= 	"<tr class='rl37_radiotherapi'>" .
												"<td id='rl37_kode_rs'><small>" . $kode_rs . "</small></td>" .
												"<td id='rl37_nama_rs'><small>" . $nama_rs . "</small></td>" .
												"<td id='rl37_kode_propinsi'><small>" . $kode_propinsi . "</small></td>" .
												"<td id='rl37_kabupaten_kota'><small>" . $kabupaten_kota . "</small></td>" .
												"<td id='rl37_tahun'><small>" . $tahun . "</small></td>" .
												"<td id='rl37_nomor'><small>" . ($nomor++) . "</small></td>" .
												"<td id='rl37_jenis_kegiatan'><small>" . $kegiatan . "</small></td>" .
												"<td id='rl37_jumlah' style='display:none;'>" . $jumlah . "</td>" .
												"<td id='rl37_f_jumlah'><div align='right'><small>" . $jumlah . "</small></div></td>" .
											"</tr>";
				}
			}
			$html_kedokteran_nuklir = 	"<tr>" .
											"<td colspan='8'>KEDOKTERAN NUKLIR</td>" .
										"</tr>";
			$nomor = 1;
			foreach ($jenis_kegiatan as $nama_kegiatan => $jumlah) {
				if (strpos($nama_kegiatan, "KEDOKTERAN NUKLIR") !== false) {
					$kegiatan = explode(" - ", $nama_kegiatan)[1];
					$html_kedokteran_nuklir .= 	"<tr class='rl37_kedokteran_nuklir'>" .
													"<td id='rl37_kode_rs'><small>" . $kode_rs . "</small></td>" .
													"<td id='rl37_nama_rs'><small>" . $nama_rs . "</small></td>" .
													"<td id='rl37_kode_propinsi'><small>" . $kode_propinsi . "</small></td>" .
													"<td id='rl37_kabupaten_kota'><small>" . $kabupaten_kota . "</small></td>" .
													"<td id='rl37_tahun'><small>" . $tahun . "</small></td>" .
													"<td id='rl37_nomor'><small>" . ($nomor++) . "</small></td>" .
													"<td id='rl37_jenis_kegiatan'><small>" . $kegiatan . "</small></td>" .
													"<td id='rl37_jumlah' style='display:none;'>" . $jumlah . "</td>" .
													"<td id='rl37_f_jumlah'><div align='right'><small>" . $jumlah . "</small></div></td>" .
												"</tr>";
				}
			}
			$html_imaging = 			"<tr>" .
											"<td colspan='8'>IMAGING/PENCITRAAN</td>" .
										"</tr>";
			$nomor = 1;
			foreach ($jenis_kegiatan as $nama_kegiatan => $jumlah) {
				if (strpos($nama_kegiatan, "IMAGING/PENCITRAAN") !== false) {
					$kegiatan = explode(" - ", $nama_kegiatan)[1];
					$html_imaging .= 	"<tr class='rl37_imaging'>" .
											"<td id='rl37_kode_rs'><small>" . $kode_rs . "</small></td>" .
											"<td id='rl37_nama_rs'><small>" . $nama_rs . "</small></td>" .
											"<td id='rl37_kode_propinsi'><small>" . $kode_propinsi . "</small></td>" .
											"<td id='rl37_kabupaten_kota'><small>" . $kabupaten_kota . "</small></td>" .
											"<td id='rl37_tahun'><small>" . $tahun . "</small></td>" .
											"<td id='rl37_nomor'><small>" . ($nomor++) . "</small></td>" .
											"<td id='rl37_jenis_kegiatan'><small>" . $kegiatan . "</small></td>" .
											"<td id='rl37_jumlah' style='display:none;'>" . $jumlah . "</td>" .
											"<td id='rl37_f_jumlah'><div align='right'><small>" . $jumlah . "</small></div></td>" .
										"</tr>";
				}
			}
			$html = $html_radiodiagnostik . $html_radiotherapi . $html_kedokteran_nuklir . $html_imaging;
			$data['html'] = $html;
			echo json_encode($data);
		} else if ($_POST['command'] == "export_xls") {
			require_once ("smis-libs-out/php-excel/PHPExcel.php");

		    $file = new PHPExcel ();
		    $file->getProperties ()->setCreator ( "PT. Inovasi Ide Utama" );
		    $file->getProperties ()->setTitle ( "RL 3.7 - Radiology" );
		    $file->getProperties ()->setSubject ( "RL 3.7 - Radiology" );
		    $file->getProperties ()->setDescription ( "RL 3.7 - Radiology Generated by System" );
		    $file->getProperties ()->setKeywords ( "RL 3.7 - Radiology" );
		    $file->getProperties ()->setCategory ( "RL 3.7 - Radiology" );

		    $sheet = $file->getActiveSheet ();
		    $i = 1;

		    $sheet->mergeCells("A".$i.":H".$i)->setCellValue("A".$i,"RL 3.7 - Radiology");
		    $sheet->getStyle("A".$i.":H".$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		    $sheet->getStyle("A".$i)->getFont()->setBold(true);
		    $i = $i + 2;

		    $border_start = $i;
		    
		    $sheet->setCellValue("A".$i,"KODE PROPINSI");
		    $sheet->setCellValue("B".$i,"KAB/KOTA");
		    $sheet->setCellValue("C".$i,"KODE RS");
		    $sheet->setCellValue("D".$i,"NAMA RS");
		    $sheet->setCellValue("E".$i,"TAHUN");
		    $sheet->setCellValue("F".$i,"NO. URUT");
		    $sheet->setCellValue("G".$i,"JENIS KEGIATAN");
		    $sheet->setCellValue("H".$i,"JUMLAH");
		    $sheet->getStyle("A".$i.":H".$i)->getFont()->setBold(true);
		    $sheet->getStyle("A".$i.":H".$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		    $sheet->getStyle("A".$i.":H".$i)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);


		    $data_radiodiagnostik = json_decode($_POST['detail_radiodiagnostik']);
		    $i = $i + 1;
		    $sheet->mergeCells("A".$i.":H".$i)->setCellValue("A".$i,"RADIODIAGNOSTIK");
		    foreach ($data_radiodiagnostik as $d) {
			    $i = $i + 1;
			    $sheet->setCellValue("A".$i,$d->kode_propinsi);
			    $sheet->setCellValue("B".$i,$d->kabupaten_kota);
			    $sheet->setCellValue("C".$i,$d->kode_propinsi);
			    $sheet->setCellValue("D".$i,$d->nama_rs);
			    $sheet->setCellValue("E".$i,$d->tahun);
			    $sheet->setCellValue("F".$i,$d->nomor);
			    $sheet->setCellValue("G".$i,$d->jenis_kegiatan);
			    $sheet->setCellValue("H".$i,$d->jumlah);
			}

			$data_radiotherapi = json_decode($_POST['detail_radiotherapi']);
		    $i = $i + 1;
		    $sheet->mergeCells("A".$i.":H".$i)->setCellValue("A".$i,"RADIOTHERAPI");
		    foreach ($data_radiotherapi as $d) {
			    $i = $i + 1;
			    $sheet->setCellValue("A".$i,$d->kode_propinsi);
			    $sheet->setCellValue("B".$i,$d->kabupaten_kota);
			    $sheet->setCellValue("C".$i,$d->kode_propinsi);
			    $sheet->setCellValue("D".$i,$d->nama_rs);
			    $sheet->setCellValue("E".$i,$d->tahun);
			    $sheet->setCellValue("F".$i,$d->nomor);
			    $sheet->setCellValue("G".$i,$d->jenis_kegiatan);
			    $sheet->setCellValue("H".$i,$d->jumlah);
			}

			$data_kedokteran_nuklir = json_decode($_POST['detail_kedokteran_nuklir']);
		    $i = $i + 1;
		    $sheet->mergeCells("A".$i.":H".$i)->setCellValue("A".$i,"KEDOKTERAN NUKLIR");
		    foreach ($data_kedokteran_nuklir as $d) {
			    $i = $i + 1;
			    $sheet->setCellValue("A".$i,$d->kode_propinsi);
			    $sheet->setCellValue("B".$i,$d->kabupaten_kota);
			    $sheet->setCellValue("C".$i,$d->kode_propinsi);
			    $sheet->setCellValue("D".$i,$d->nama_rs);
			    $sheet->setCellValue("E".$i,$d->tahun);
			    $sheet->setCellValue("F".$i,$d->nomor);
			    $sheet->setCellValue("G".$i,$d->jenis_kegiatan);
			    $sheet->setCellValue("H".$i,$d->jumlah);
			}

			$data_imaging = json_decode($_POST['detail_imaging']);
		    $i = $i + 1;
		    $sheet->mergeCells("A".$i.":H".$i)->setCellValue("A".$i,"IMAGING/PENCITRAAN");
		    foreach ($data_imaging as $d) {
			    $i = $i + 1;
			    $sheet->setCellValue("A".$i,$d->kode_propinsi);
			    $sheet->setCellValue("B".$i,$d->kabupaten_kota);
			    $sheet->setCellValue("C".$i,$d->kode_propinsi);
			    $sheet->setCellValue("D".$i,$d->nama_rs);
			    $sheet->setCellValue("E".$i,$d->tahun);
			    $sheet->setCellValue("F".$i,$d->nomor);
			    $sheet->setCellValue("G".$i,$d->jenis_kegiatan);
			    $sheet->setCellValue("H".$i,$d->jumlah);
			}

		    $thin = array ();
		    $thin['borders']=array();
		    $thin['borders']['allborders']=array();
		    $thin['borders']['allborders']['style']=PHPExcel_Style_Border::BORDER_THIN ;
		    $sheet->getStyle ( "A".$border_start.":H".$i )->applyFromArray ($thin);

		    foreach (range('A', 'H') as $columnID) {
		        $sheet->getColumnDimension($columnID)->setAutoSize(true);
		    }

		    header ( 'Content-Type: application/vnd.ms-excel' );
		    header ( 'Content-Disposition: attachment;filename="RL 3.7 - Radiology.xls"' );
		    header ( 'Cache-Control: max-age=0' );
		    $writer = PHPExcel_IOFactory::createWriter ( $file, 'Excel5' );
		    $writer->save ( 'php://output' );
		}
		return;
	}
	$modal = $uitable->getModal();
	$form = $modal->getForm();
	$form->setTitle("RL 3.7 - Radiology");

	$bulan_option = new OptionBuilder();
	$bulan_option->add("Semua", "%%", "1");
	$bulan_option->add("Januari", "1");
	$bulan_option->add("Februari", "2");
	$bulan_option->add("Maret", "3");
	$bulan_option->add("April", "4");
	$bulan_option->add("Mei", "5");
	$bulan_option->add("Juni", "6");
	$bulan_option->add("Juli", "7");
	$bulan_option->add("Agustus", "8");
	$bulan_option->add("September", "9");
	$bulan_option->add("Oktober", "10");
	$bulan_option->add("Nopember", "11");
	$bulan_option->add("Desember", "12");
	$bulan_select = new Select("rl37_bulan_filter", "rl37_bulan_filter", $bulan_option->getContent());
	$form->addElement("Bulan", $bulan_select);
	$tahun_rows = $db->get_result("
		SELECT DISTINCT YEAR(tanggal) tahun
		FROM smis_rad_pesanan
		WHERE prop = '' AND YEAR(tanggal) > 0
		ORDER BY YEAR(tanggal) ASC
	");
	$tahun_option = new OptionBuilder();
	if ($tahun_rows != null) {
		foreach ($tahun_rows as $tahun_row) {
			if (date("Y") == $tahun_row->tahun)
				$tahun_option->add($tahun_row->tahun, $tahun_row->tahun, "1");
			else
				$tahun_option->add($tahun_row->tahun, $tahun_row->tahun);
		}
	}
	$tahun_select = new Select("rl37_tahun_filter", "rl37_tahun_filter", $tahun_option->getContent());
	$form->addElement("Tahun", $tahun_select);
	$excel_button = new Button("", "", "");
	$excel_button 	->addClass("btn-primary")
					->setIsButton(Button::$ICONIC)
					->setIcon("fa fa-file-excel-o")
					->setAction("rl37.excel()");
	$process_button = new Button("", "", "");
	$process_button	->addClass("btn-inverse")
					-> setIsButton(Button::$ICONIC)
					->setIcon("fa fa-circle-o")
					->setAction("rl37.view()");
	$button_group = new ButtonGroup("noprint");
	$button_group->addButton($process_button);
	$button_group->addButton($excel_button);
	$form->addElement("", $button_group);

	echo $form->getHtml();
	echo $uitable->getHtml();
	echo addJS("framework/smis/js/table_action.js");
	echo addJS("medical_record/resource/js/rl37.js", false);
?>