<?php
	require_once("smis-base/smis-include-service-consumer.php");
	global $db;

	$head = array(
				'Kode RS', 'Kode Propinsi', 'Kab/Kota', 'Nama RS', 'Tahun', 'No. Urut', 'Jenis Pelayanan', 
				'Pasien Awal Tahun',
				'Pasien Masuk',
				'Pasien Keluar Hidup',
				'Pasien Keluar Mati < 48 Jam',
				'Pasien Keluar Mati >= 48 Jam',
				'Jumlah Lama Dirawat',
				'Pasien Akhir Tahun',
				'Jumlah Hari Perawatan',
				'Rincian HP Kelas VVIP',
				'Rincian HP Kelas VIP',
				'Rincian HP Kelas I',
				'Rincian HP Kelas II',
				'Rincian HP Kelas III',
				'Rincian HP Kelas Khusus'
			);
	$uitable = new Table($head, "", null, true);
	$uitable->setName("rl31");
	$uitable->setAction(false);
	$uitable->setFooterVisible(false);
	$uitable->setHeaderVisible(false);
	$uitable->addHeader("before", "
		<tr>
			<th rowspan='3'>Kode RS</th>
			<th rowspan='3'>Kode Propinsi</th>
			<th rowspan='3'>Kab/Kota</th>
			<th rowspan='3'>Nama RS</th>
			<th rowspan='3'>Tahun</th>
			<th rowspan='2'>No.</th>
			<th rowspan='2'>Jenis Pelayanan</th>
			<th rowspan='2'>Pasien Awal Tahun</th>
			<th rowspan='2'>Pasien Masuk</th>
			<th rowspan='2'>Pasien Keluar Hidup</th>
			<th colspan='2'>Pasien Keluar Mati</th>
			<th rowspan='2'>Jumlah Lama Dirawat</th>
			<th rowspan='2'>Pasien Akhir Tahun</th>
			<th rowspan='2'>Jumlah Hari Perawatan</th>
			<th colspan='6'>Rincian Hari Perawatan Per Kelas</th>
		</tr>
		<tr>
			<th>&lt; 48 Jam</th>
			<th>&ge; 48 Jam</th>
			<th>VVIP</th>
			<th>VIP</th>
			<th>I</th>
			<th>II</th>
			<th>III</th>
			<th>Kelas Khusus</th>
		</tr>
		<tr>
			<th>1</th>
			<th>2</th>
			<th>3</th>
			<th>4</th>
			<th>5</th>
			<th>6</th>
			<th>7</th>
			<th>8</th>
			<th>9</th>
			<th>10</th>
			<th>11</th>
			<th>12</th>
			<th>13</th>
			<th>14</th>
			<th>15</th>
			<th>16</th>
		</tr>
	");

	if (isset($_POST ['command'])) {
		$data_layanan_rawat_inap = array(
			"Penyakit Dalam" => array(
				'label'						=> "spesialisasi",
				'pasien_awal' 				=> 0,
				'pasien_masuk'				=> 0,
				'pasien_keluar_hidup'		=> 0,
				'pasien_keluar_mati_k48'	=> 0,
				'pasien_keluar_mati_l48'	=> 0,
				'los'						=> 0,
				'pasien_akhir'				=> 0,
				'jumlah_hp_total'			=> 0,
				'jumlah_hp_vvip'			=> 0,
				'jumlah_hp_vip'				=> 0,
				'jumlah_hp_i'				=> 0,
				'jumlah_hp_ii'				=> 0,
				'jumlah_hp_iii'				=> 0,
				'jumlah_hp_khusus'			=> 0
			),
		    "Kesehatan Anak" => array(
		    	'label'						=> "spesialisasi",
				'pasien_awal' 				=> 0,
				'pasien_masuk'				=> 0,
				'pasien_keluar_hidup'		=> 0,
				'pasien_keluar_mati_k48'	=> 0,
				'pasien_keluar_mati_l48'	=> 0,
				'los'						=> 0,
				'pasien_akhir'				=> 0,
				'jumlah_hp_total'			=> 0,
				'jumlah_hp_vvip'			=> 0,
				'jumlah_hp_vip'				=> 0,
				'jumlah_hp_i'				=> 0,
				'jumlah_hp_ii'				=> 0,
				'jumlah_hp_iii'				=> 0,
				'jumlah_hp_khusus'			=> 0
			),
			"Obstetri" => array(
				'label'						=> "spesialisasi",
				'pasien_awal' 				=> 0,
				'pasien_masuk'				=> 0,
				'pasien_keluar_hidup'		=> 0,
				'pasien_keluar_mati_k48'	=> 0,
				'pasien_keluar_mati_l48'	=> 0,
				'los'						=> 0,
				'pasien_akhir'				=> 0,
				'jumlah_hp_total'			=> 0,
				'jumlah_hp_vvip'			=> 0,
				'jumlah_hp_vip'				=> 0,
				'jumlah_hp_i'				=> 0,
				'jumlah_hp_ii'				=> 0,
				'jumlah_hp_iii'				=> 0,
				'jumlah_hp_khusus'			=> 0
			),
		    "Ginekologi" => array(
		    	'label'						=> "spesialisasi",
				'pasien_awal' 				=> 0,
				'pasien_masuk'				=> 0,
				'pasien_keluar_hidup'		=> 0,
				'pasien_keluar_mati_k48'	=> 0,
				'pasien_keluar_mati_l48'	=> 0,
				'los'						=> 0,
				'pasien_akhir'				=> 0,
				'jumlah_hp_total'			=> 0,
				'jumlah_hp_vvip'			=> 0,
				'jumlah_hp_vip'				=> 0,
				'jumlah_hp_i'				=> 0,
				'jumlah_hp_ii'				=> 0,
				'jumlah_hp_iii'				=> 0,
				'jumlah_hp_khusus'			=> 0
			),
		    "Bedah" => array(
		    	'label'						=> "spesialisasi",
				'pasien_awal' 				=> 0,
				'pasien_masuk'				=> 0,
				'pasien_keluar_hidup'		=> 0,
				'pasien_keluar_mati_k48'	=> 0,
				'pasien_keluar_mati_l48'	=> 0,
				'los'						=> 0,
				'pasien_akhir'				=> 0,
				'jumlah_hp_total'			=> 0,
				'jumlah_hp_vvip'			=> 0,
				'jumlah_hp_vip'				=> 0,
				'jumlah_hp_i'				=> 0,
				'jumlah_hp_ii'				=> 0,
				'jumlah_hp_iii'				=> 0,
				'jumlah_hp_khusus'			=> 0
			),
		    "Bedah Orthopedi" => array(
		    	'label'						=> "spesialisasi",
				'pasien_awal' 				=> 0,
				'pasien_masuk'				=> 0,
				'pasien_keluar_hidup'		=> 0,
				'pasien_keluar_mati_k48'	=> 0,
				'pasien_keluar_mati_l48'	=> 0,
				'los'						=> 0,
				'pasien_akhir'				=> 0,
				'jumlah_hp_total'			=> 0,
				'jumlah_hp_vvip'			=> 0,
				'jumlah_hp_vip'				=> 0,
				'jumlah_hp_i'				=> 0,
				'jumlah_hp_ii'				=> 0,
				'jumlah_hp_iii'				=> 0,
				'jumlah_hp_khusus'			=> 0
			),
		    "Bedah Saraf" => array(
		    	'label'						=> "spesialisasi",
				'pasien_awal' 				=> 0,
				'pasien_masuk'				=> 0,
				'pasien_keluar_hidup'		=> 0,
				'pasien_keluar_mati_k48'	=> 0,
				'pasien_keluar_mati_l48'	=> 0,
				'los'						=> 0,
				'pasien_akhir'				=> 0,
				'jumlah_hp_total'			=> 0,
				'jumlah_hp_vvip'			=> 0,
				'jumlah_hp_vip'				=> 0,
				'jumlah_hp_i'				=> 0,
				'jumlah_hp_ii'				=> 0,
				'jumlah_hp_iii'				=> 0,
				'jumlah_hp_khusus'			=> 0
			),
		    "Luka Bakar" => array(
		    	'label'						=> "spesialisasi",
				'pasien_awal' 				=> 0,
				'pasien_masuk'				=> 0,
				'pasien_keluar_hidup'		=> 0,
				'pasien_keluar_mati_k48'	=> 0,
				'pasien_keluar_mati_l48'	=> 0,
				'los'						=> 0,
				'pasien_akhir'				=> 0,
				'jumlah_hp_total'			=> 0,
				'jumlah_hp_vvip'			=> 0,
				'jumlah_hp_vip'				=> 0,
				'jumlah_hp_i'				=> 0,
				'jumlah_hp_ii'				=> 0,
				'jumlah_hp_iii'				=> 0,
				'jumlah_hp_khusus'			=> 0
			),
		    "S a r a f" => array(
		    	'label'						=> "spesialisasi",
				'pasien_awal' 				=> 0,
				'pasien_masuk'				=> 0,
				'pasien_keluar_hidup'		=> 0,
				'pasien_keluar_mati_k48'	=> 0,
				'pasien_keluar_mati_l48'	=> 0,
				'los'						=> 0,
				'pasien_akhir'				=> 0,
				'jumlah_hp_total'			=> 0,
				'jumlah_hp_vvip'			=> 0,
				'jumlah_hp_vip'				=> 0,
				'jumlah_hp_i'				=> 0,
				'jumlah_hp_ii'				=> 0,
				'jumlah_hp_iii'				=> 0,
				'jumlah_hp_khusus'			=> 0
			),
		    "J i w a" => array(
		    	'label'						=> "spesialisasi",
				'pasien_awal' 				=> 0,
				'pasien_masuk'				=> 0,
				'pasien_keluar_hidup'		=> 0,
				'pasien_keluar_mati_k48'	=> 0,
				'pasien_keluar_mati_l48'	=> 0,
				'los'						=> 0,
				'pasien_akhir'				=> 0,
				'jumlah_hp_total'			=> 0,
				'jumlah_hp_vvip'			=> 0,
				'jumlah_hp_vip'				=> 0,
				'jumlah_hp_i'				=> 0,
				'jumlah_hp_ii'				=> 0,
				'jumlah_hp_iii'				=> 0,
				'jumlah_hp_khusus'			=> 0
			),
		    "Psikologi" => array(
		    	'label'						=> "spesialisasi",
				'pasien_awal' 				=> 0,
				'pasien_masuk'				=> 0,
				'pasien_keluar_hidup'		=> 0,
				'pasien_keluar_mati_k48'	=> 0,
				'pasien_keluar_mati_l48'	=> 0,
				'los'						=> 0,
				'pasien_akhir'				=> 0,
				'jumlah_hp_total'			=> 0,
				'jumlah_hp_vvip'			=> 0,
				'jumlah_hp_vip'				=> 0,
				'jumlah_hp_i'				=> 0,
				'jumlah_hp_ii'				=> 0,
				'jumlah_hp_iii'				=> 0,
				'jumlah_hp_khusus'			=> 0
			),
		    "Penatalaksana Pnyguna. NAPZA" => array(
		    	'label'						=> "spesialisasi",
				'pasien_awal' 				=> 0,
				'pasien_masuk'				=> 0,
				'pasien_keluar_hidup'		=> 0,
				'pasien_keluar_mati_k48'	=> 0,
				'pasien_keluar_mati_l48'	=> 0,
				'los'						=> 0,
				'pasien_akhir'				=> 0,
				'jumlah_hp_total'			=> 0,
				'jumlah_hp_vvip'			=> 0,
				'jumlah_hp_vip'				=> 0,
				'jumlah_hp_i'				=> 0,
				'jumlah_hp_ii'				=> 0,
				'jumlah_hp_iii'				=> 0,
				'jumlah_hp_khusus'			=> 0
			),
		    "T H T" => array(
		    	'label'						=> "spesialisasi",
				'pasien_awal' 				=> 0,
				'pasien_masuk'				=> 0,
				'pasien_keluar_hidup'		=> 0,
				'pasien_keluar_mati_k48'	=> 0,
				'pasien_keluar_mati_l48'	=> 0,
				'los'						=> 0,
				'pasien_akhir'				=> 0,
				'jumlah_hp_total'			=> 0,
				'jumlah_hp_vvip'			=> 0,
				'jumlah_hp_vip'				=> 0,
				'jumlah_hp_i'				=> 0,
				'jumlah_hp_ii'				=> 0,
				'jumlah_hp_iii'				=> 0,
				'jumlah_hp_khusus'			=> 0
			),
		    "M a t a" => array(
		    	'label'						=> "spesialisasi",
				'pasien_awal' 				=> 0,
				'pasien_masuk'				=> 0,
				'pasien_keluar_hidup'		=> 0,
				'pasien_keluar_mati_k48'	=> 0,
				'pasien_keluar_mati_l48'	=> 0,
				'los'						=> 0,
				'pasien_akhir'				=> 0,
				'jumlah_hp_total'			=> 0,
				'jumlah_hp_vvip'			=> 0,
				'jumlah_hp_vip'				=> 0,
				'jumlah_hp_i'				=> 0,
				'jumlah_hp_ii'				=> 0,
				'jumlah_hp_iii'				=> 0,
				'jumlah_hp_khusus'			=> 0
			),
		    "Kulit & Kelamin" => array(
		    	'label'						=> "spesialisasi",
				'pasien_awal' 				=> 0,
				'pasien_masuk'				=> 0,
				'pasien_keluar_hidup'		=> 0,
				'pasien_keluar_mati_k48'	=> 0,
				'pasien_keluar_mati_l48'	=> 0,
				'los'						=> 0,
				'pasien_akhir'				=> 0,
				'jumlah_hp_total'			=> 0,
				'jumlah_hp_vvip'			=> 0,
				'jumlah_hp_vip'				=> 0,
				'jumlah_hp_i'				=> 0,
				'jumlah_hp_ii'				=> 0,
				'jumlah_hp_iii'				=> 0,
				'jumlah_hp_khusus'			=> 0
			),
		    "Kardiologi" => array(
		    	'label'						=> "spesialisasi",
				'pasien_awal' 				=> 0,
				'pasien_masuk'				=> 0,
				'pasien_keluar_hidup'		=> 0,
				'pasien_keluar_mati_k48'	=> 0,
				'pasien_keluar_mati_l48'	=> 0,
				'los'						=> 0,
				'pasien_akhir'				=> 0,
				'jumlah_hp_total'			=> 0,
				'jumlah_hp_vvip'			=> 0,
				'jumlah_hp_vip'				=> 0,
				'jumlah_hp_i'				=> 0,
				'jumlah_hp_ii'				=> 0,
				'jumlah_hp_iii'				=> 0,
				'jumlah_hp_khusus'			=> 0
			),
		    "Paru-paru" => array(
		    	'label'						=> "spesialisasi",
				'pasien_awal' 				=> 0,
				'pasien_masuk'				=> 0,
				'pasien_keluar_hidup'		=> 0,
				'pasien_keluar_mati_k48'	=> 0,
				'pasien_keluar_mati_l48'	=> 0,
				'los'						=> 0,
				'pasien_akhir'				=> 0,
				'jumlah_hp_total'			=> 0,
				'jumlah_hp_vvip'			=> 0,
				'jumlah_hp_vip'				=> 0,
				'jumlah_hp_i'				=> 0,
				'jumlah_hp_ii'				=> 0,
				'jumlah_hp_iii'				=> 0,
				'jumlah_hp_khusus'			=> 0
			),
		    "Geriatri" => array(
		    	'label'						=> "spesialisasi",
				'pasien_awal' 				=> 0,
				'pasien_masuk'				=> 0,
				'pasien_keluar_hidup'		=> 0,
				'pasien_keluar_mati_k48'	=> 0,
				'pasien_keluar_mati_l48'	=> 0,
				'los'						=> 0,
				'pasien_akhir'				=> 0,
				'jumlah_hp_total'			=> 0,
				'jumlah_hp_vvip'			=> 0,
				'jumlah_hp_vip'				=> 0,
				'jumlah_hp_i'				=> 0,
				'jumlah_hp_ii'				=> 0,
				'jumlah_hp_iii'				=> 0,
				'jumlah_hp_khusus'			=> 0
			),
		    "Radioterapi" => array(
		    	'label'						=> "spesialisasi",
				'pasien_awal' 				=> 0,
				'pasien_masuk'				=> 0,
				'pasien_keluar_hidup'		=> 0,
				'pasien_keluar_mati_k48'	=> 0,
				'pasien_keluar_mati_l48'	=> 0,
				'los'						=> 0,
				'pasien_akhir'				=> 0,
				'jumlah_hp_total'			=> 0,
				'jumlah_hp_vvip'			=> 0,
				'jumlah_hp_vip'				=> 0,
				'jumlah_hp_i'				=> 0,
				'jumlah_hp_ii'				=> 0,
				'jumlah_hp_iii'				=> 0,
				'jumlah_hp_khusus'			=> 0
			),
		    "Kedokteran Nuklir" => array(
		    	'label'						=> "spesialisasi",
				'pasien_awal' 				=> 0,
				'pasien_masuk'				=> 0,
				'pasien_keluar_hidup'		=> 0,
				'pasien_keluar_mati_k48'	=> 0,
				'pasien_keluar_mati_l48'	=> 0,
				'los'						=> 0,
				'pasien_akhir'				=> 0,
				'jumlah_hp_total'			=> 0,
				'jumlah_hp_vvip'			=> 0,
				'jumlah_hp_vip'				=> 0,
				'jumlah_hp_i'				=> 0,
				'jumlah_hp_ii'				=> 0,
				'jumlah_hp_iii'				=> 0,
				'jumlah_hp_khusus'			=> 0
			),
		    "K u s t a" => array(
		    	'label'						=> "spesialisasi",
				'pasien_awal' 				=> 0,
				'pasien_masuk'				=> 0,
				'pasien_keluar_hidup'		=> 0,
				'pasien_keluar_mati_k48'	=> 0,
				'pasien_keluar_mati_l48'	=> 0,
				'los'						=> 0,
				'pasien_akhir'				=> 0,
				'jumlah_hp_total'			=> 0,
				'jumlah_hp_vvip'			=> 0,
				'jumlah_hp_vip'				=> 0,
				'jumlah_hp_i'				=> 0,
				'jumlah_hp_ii'				=> 0,
				'jumlah_hp_iii'				=> 0,
				'jumlah_hp_khusus'			=> 0
			),
		    "Rehabilitasi Medik" => array(
		    	'label'						=> "spesialisasi",
				'pasien_awal' 				=> 0,
				'pasien_masuk'				=> 0,
				'pasien_keluar_hidup'		=> 0,
				'pasien_keluar_mati_k48'	=> 0,
				'pasien_keluar_mati_l48'	=> 0,
				'los'						=> 0,
				'pasien_akhir'				=> 0,
				'jumlah_hp_total'			=> 0,
				'jumlah_hp_vvip'			=> 0,
				'jumlah_hp_vip'				=> 0,
				'jumlah_hp_i'				=> 0,
				'jumlah_hp_ii'				=> 0,
				'jumlah_hp_iii'				=> 0,
				'jumlah_hp_khusus'			=> 0
			),
		    "Isolasi" => array(
		    	'label'						=> "spesialisasi",
				'pasien_awal' 				=> 0,
				'pasien_masuk'				=> 0,
				'pasien_keluar_hidup'		=> 0,
				'pasien_keluar_mati_k48'	=> 0,
				'pasien_keluar_mati_l48'	=> 0,
				'los'						=> 0,
				'pasien_akhir'				=> 0,
				'jumlah_hp_total'			=> 0,
				'jumlah_hp_vvip'			=> 0,
				'jumlah_hp_vip'				=> 0,
				'jumlah_hp_i'				=> 0,
				'jumlah_hp_ii'				=> 0,
				'jumlah_hp_iii'				=> 0,
				'jumlah_hp_khusus'			=> 0
			),
		    "I C U" => array(
		    	'label'						=> "spesialisasi",
				'pasien_awal' 				=> 0,
				'pasien_masuk'				=> 0,
				'pasien_keluar_hidup'		=> 0,
				'pasien_keluar_mati_k48'	=> 0,
				'pasien_keluar_mati_l48'	=> 0,
				'los'						=> 0,
				'pasien_akhir'				=> 0,
				'jumlah_hp_total'			=> 0,
				'jumlah_hp_vvip'			=> 0,
				'jumlah_hp_vip'				=> 0,
				'jumlah_hp_i'				=> 0,
				'jumlah_hp_ii'				=> 0,
				'jumlah_hp_iii'				=> 0,
				'jumlah_hp_khusus'			=> 0
			),
		    "I C C U" => array(
		    	'label'						=> "spesialisasi",
				'pasien_awal' 				=> 0,
				'pasien_masuk'				=> 0,
				'pasien_keluar_hidup'		=> 0,
				'pasien_keluar_mati_k48'	=> 0,
				'pasien_keluar_mati_l48'	=> 0,
				'los'						=> 0,
				'pasien_akhir'				=> 0,
				'jumlah_hp_total'			=> 0,
				'jumlah_hp_vvip'			=> 0,
				'jumlah_hp_vip'				=> 0,
				'jumlah_hp_i'				=> 0,
				'jumlah_hp_ii'				=> 0,
				'jumlah_hp_iii'				=> 0,
				'jumlah_hp_khusus'			=> 0
			),
		    "NICU / PICU" => array(
		    	'label'						=> "spesialisasi",
				'pasien_awal' 				=> 0,
				'pasien_masuk'				=> 0,
				'pasien_keluar_hidup'		=> 0,
				'pasien_keluar_mati_k48'	=> 0,
				'pasien_keluar_mati_l48'	=> 0,
				'los'						=> 0,
				'pasien_akhir'				=> 0,
				'jumlah_hp_total'			=> 0,
				'jumlah_hp_vvip'			=> 0,
				'jumlah_hp_vip'				=> 0,
				'jumlah_hp_i'				=> 0,
				'jumlah_hp_ii'				=> 0,
				'jumlah_hp_iii'				=> 0,
				'jumlah_hp_khusus'			=> 0
			),
		    "Umum" => array(
		    	'label'						=> "spesialisasi",
				'pasien_awal' 				=> 0,
				'pasien_masuk'				=> 0,
				'pasien_keluar_hidup'		=> 0,
				'pasien_keluar_mati_k48'	=> 0,
				'pasien_keluar_mati_l48'	=> 0,
				'los'						=> 0,
				'pasien_akhir'				=> 0,
				'jumlah_hp_total'			=> 0,
				'jumlah_hp_vvip'			=> 0,
				'jumlah_hp_vip'				=> 0,
				'jumlah_hp_i'				=> 0,
				'jumlah_hp_ii'				=> 0,
				'jumlah_hp_iii'				=> 0,
				'jumlah_hp_khusus'			=> 0
			),
		    "Gigi & Mulut" => array(
		    	'label'						=> "spesialisasi",
				'pasien_awal' 				=> 0,
				'pasien_masuk'				=> 0,
				'pasien_keluar_hidup'		=> 0,
				'pasien_keluar_mati_k48'	=> 0,
				'pasien_keluar_mati_l48'	=> 0,
				'los'						=> 0,
				'pasien_akhir'				=> 0,
				'jumlah_hp_total'			=> 0,
				'jumlah_hp_vvip'			=> 0,
				'jumlah_hp_vip'				=> 0,
				'jumlah_hp_i'				=> 0,
				'jumlah_hp_ii'				=> 0,
				'jumlah_hp_iii'				=> 0,
				'jumlah_hp_khusus'			=> 0
			),
		    "Pelayanan Rawat Darurat" => array(
		    	'label'						=> "spesialisasi",
				'pasien_awal' 				=> 0,
				'pasien_masuk'				=> 0,
				'pasien_keluar_hidup'		=> 0,
				'pasien_keluar_mati_k48'	=> 0,
				'pasien_keluar_mati_l48'	=> 0,
				'los'						=> 0,
				'pasien_akhir'				=> 0,
				'jumlah_hp_total'			=> 0,
				'jumlah_hp_vvip'			=> 0,
				'jumlah_hp_vip'				=> 0,
				'jumlah_hp_i'				=> 0,
				'jumlah_hp_ii'				=> 0,
				'jumlah_hp_iii'				=> 0,
				'jumlah_hp_khusus'			=> 0
			),
		    "Perinatologi" => array(
		    	'label'						=> "spesialisasi",
				'pasien_awal' 				=> 0,
				'pasien_masuk'				=> 0,
				'pasien_keluar_hidup'		=> 0,
				'pasien_keluar_mati_k48'	=> 0,
				'pasien_keluar_mati_l48'	=> 0,
				'los'						=> 0,
				'pasien_akhir'				=> 0,
				'jumlah_hp_total'			=> 0,
				'jumlah_hp_vvip'			=> 0,
				'jumlah_hp_vip'				=> 0,
				'jumlah_hp_i'				=> 0,
				'jumlah_hp_ii'				=> 0,
				'jumlah_hp_iii'				=> 0,
				'jumlah_hp_khusus'			=> 0
			),
			"Urologi" => array(
				'label'						=> "spesialisasi",
				'pasien_awal' 				=> 0,
				'pasien_masuk'				=> 0,
				'pasien_keluar_hidup'		=> 0,
				'pasien_keluar_mati_k48'	=> 0,
				'pasien_keluar_mati_l48'	=> 0,
				'los'						=> 0,
				'pasien_akhir'				=> 0,
				'jumlah_hp_total'			=> 0,
				'jumlah_hp_vvip'			=> 0,
				'jumlah_hp_vip'				=> 0,
				'jumlah_hp_i'				=> 0,
				'jumlah_hp_ii'				=> 0,
				'jumlah_hp_iii'				=> 0,
				'jumlah_hp_khusus'			=> 0
			),
			"Bedah Plastik" => array(
				'label'						=> "spesialisasi",
				'pasien_awal' 				=> 0,
				'pasien_masuk'				=> 0,
				'pasien_keluar_hidup'		=> 0,
				'pasien_keluar_mati_k48'	=> 0,
				'pasien_keluar_mati_l48'	=> 0,
				'los'						=> 0,
				'pasien_akhir'				=> 0,
				'jumlah_hp_total'			=> 0,
				'jumlah_hp_vvip'			=> 0,
				'jumlah_hp_vip'				=> 0,
				'jumlah_hp_i'				=> 0,
				'jumlah_hp_ii'				=> 0,
				'jumlah_hp_iii'				=> 0,
				'jumlah_hp_khusus'			=> 0
			),
			"ICU (Ext.)" => array(
				'label'						=> "spesialisasi2",
				'pasien_awal' 				=> 0,
				'pasien_masuk'				=> 0,
				'pasien_keluar_hidup'		=> 0,
				'pasien_keluar_mati_k48'	=> 0,
				'pasien_keluar_mati_l48'	=> 0,
				'los'						=> 0,
				'pasien_akhir'				=> 0,
				'jumlah_hp_total'			=> 0,
				'jumlah_hp_vvip'			=> 0,
				'jumlah_hp_vip'				=> 0,
				'jumlah_hp_i'				=> 0,
				'jumlah_hp_ii'				=> 0,
				'jumlah_hp_iii'				=> 0,
				'jumlah_hp_khusus'			=> 0
			),
			"Isolasi (Ext.)" => array(
				'label'						=> "spesialisasi2",
				'pasien_awal' 				=> 0,
				'pasien_masuk'				=> 0,
				'pasien_keluar_hidup'		=> 0,
				'pasien_keluar_mati_k48'	=> 0,
				'pasien_keluar_mati_l48'	=> 0,
				'los'						=> 0,
				'pasien_akhir'				=> 0,
				'jumlah_hp_total'			=> 0,
				'jumlah_hp_vvip'			=> 0,
				'jumlah_hp_vip'				=> 0,
				'jumlah_hp_i'				=> 0,
				'jumlah_hp_ii'				=> 0,
				'jumlah_hp_iii'				=> 0,
				'jumlah_hp_khusus'			=> 0
			),
			"NICU (Ext.)" => array(
				'label'						=> "spesialisasi2",
				'pasien_awal' 				=> 0,
				'pasien_masuk'				=> 0,
				'pasien_keluar_hidup'		=> 0,
				'pasien_keluar_mati_k48'	=> 0,
				'pasien_keluar_mati_l48'	=> 0,
				'los'						=> 0,
				'pasien_akhir'				=> 0,
				'jumlah_hp_total'			=> 0,
				'jumlah_hp_vvip'			=> 0,
				'jumlah_hp_vip'				=> 0,
				'jumlah_hp_i'				=> 0,
				'jumlah_hp_ii'				=> 0,
				'jumlah_hp_iii'				=> 0,
				'jumlah_hp_khusus'			=> 0
			),
			"PICU (Ext.)" => array(
				'label'						=> "spesialisasi2",
				'pasien_awal' 				=> 0,
				'pasien_masuk'				=> 0,
				'pasien_keluar_hidup'		=> 0,
				'pasien_keluar_mati_k48'	=> 0,
				'pasien_keluar_mati_l48'	=> 0,
				'los'						=> 0,
				'pasien_akhir'				=> 0,
				'jumlah_hp_total'			=> 0,
				'jumlah_hp_vvip'			=> 0,
				'jumlah_hp_vip'				=> 0,
				'jumlah_hp_i'				=> 0,
				'jumlah_hp_ii'				=> 0,
				'jumlah_hp_iii'				=> 0,
				'jumlah_hp_khusus'			=> 0
			)
		);
		if ($_POST['command'] == "view") {
			$tahun = $_POST['tahun'];
			$bulan = $_POST['bulan'];
			$html = "";

			$kode_rs = "";
			$nama_rs = "";
			$kode_propinsi = "";
			$kabupaten_kota = "";
			$info_rs_row = $db->get_row("
				SELECT 
					*
				FROM 
					smis_mr_data_dasar_rs
				WHERE 
					prop = ''
				LIMIT 
					0, 1
			");
			if ($info_rs_row != null) {
				$kode_rs = $info_rs_row->koders;
				$nama_rs = $info_rs_row->nama_rs;
				$kode_propinsi = $info_rs_row->kode_provinsi;
				$kabupaten_kota = $info_rs_row->kota_kabupaten;
			}

			$nomor = 1;
			$total_pasien_awal = 0;
			$total_pasien_masuk = 0;
			$total_pasien_keluar_hidup = 0;
			$total_pasien_keluar_mati_k48 = 0;
			$total_pasien_keluar_mati_l48 = 0;
			$total_los = 0;
			$total_pasien_akhir = 0;
			$total_jumlah_hp_total = 0;
			$total_jumlah_hp_vvip = 0;
			$total_jumlah_hp_vip = 0;
			$total_jumlah_hp_i = 0;
			$total_jumlah_hp_ii = 0;
			$total_jumlah_hp_iii = 0;
			$total_jumlah_hp_khusus = 0;
			foreach ($data_layanan_rawat_inap as $layanan => $data) {
				/// a. Pasien Awal
				/// a.1. Filter Bulanan
				if ($bulan != "%%") {
					/// a.1.1. Pasien Tanggal Inap Sebelum Tanggal 1 Bulan Pilihan
					$row = $db->get_row("
						SELECT 
							COUNT(*) jumlah
						FROM 
							smis_rg_layananpasien
						WHERE 
							prop = '' AND 
							uri = 1 AND 
							DATE(tanggal_inap) < '" . $tahun . "-" . $bulan . "-01' AND
							(
								DATE(tanggal_pulang) = '0000-00-00' OR 
								DATE(tanggal_pulang) >= '" . $tahun . "-" . $bulan . "-01'
							) AND
							(last_spesialisasi = '" . $layanan . "' OR last_spesialisasi2 = '" . $layanan . "') AND
							carapulang NOT LIKE 'Tidak Datang'
					");
					if ($row != null)
						$data['pasien_awal'] += $row->jumlah;
				/// a.2. Filter Tahunan
				} else {
					/// a.2.1. Pasien Tanggal Inap Sebelum Tanggal 1 Bulan Januari Tahun Pilihan
					$row = $db->get_row("
						SELECT 
							COUNT(*) jumlah, SUM(lama_dirawat) los
						FROM 
							smis_rg_layananpasien
						WHERE 
							prop = '' AND 
							uri = 1 AND 
							DATE(tanggal_inap) < '" . $tahun . "-01-01' AND
							(
								DATE(tanggal_pulang) = '0000-00-00' OR 
								DATE(tanggal_pulang) >= '" . $tahun . "-01-01'
							) AND
							(last_spesialisasi = '" . $layanan . "' OR last_spesialisasi2 = '" . $layanan . "') AND
							carapulang NOT LIKE 'Tidak Datang'
					");
					if ($row != null)
						$data['pasien_awal'] += $row->jumlah;
				}

				/// b. Pasien Masuk
				/// b.1. Filter Bulanan
				if ($bulan != "%%") {
					/// b.1.1. Pasien Inap Setelah Tanggal 1 Bulan Pilihan dan Sebelum Tanggal 1 Bulan Berikutnya
					$start_date = $tahun . "-" . $bulan . "-01";
					$end_date = $tahun . "-" . $bulan . "-" . cal_days_in_month(CAL_GREGORIAN, $bulan, $tahun);
					if ($bulan == date("m") && $tahun == date("Y"))
						$end_date = date("Y-m-d");
					$row = $db->get_row("
						SELECT 
							COUNT(*) jumlah
						FROM 
							smis_rg_layananpasien
						WHERE 
							prop = '' AND 
							uri = 1 AND 
							DATE(tanggal_inap) > '" . $start_date . "' AND 
							DATE(tanggal_inap) <= '" . $end_date . "' AND 
							(last_spesialisasi = '" . $layanan . "' OR last_spesialisasi2 = '" . $layanan . "') AND
							carapulang NOT LIKE 'Tidak Datang'
					");
					if ($row != null)
						$data['pasien_masuk'] += $row->jumlah;
				/// b.2. Filter Tahunan
				} else {
					/// b.2.1. Pasien Inap Setelah Tanggal 1 Bulan Pilihan dan Sebelum Tanggal 1 Januari Tahun Berikutnya
					$start_date = $tahun . "-" . $bulan . "-01";
					$end_date = ($tahun + 1) . "-" . $bulan . "-01";
					$row = $db->get_row("
						SELECT 
							COUNT(*) jumlah
						FROM 
							smis_rg_layananpasien
						WHERE 
							prop = '' AND 
							uri = 1 AND 
							DATE(tanggal_inap) > '" . $start_date . "' AND 
							DATE(tanggal_inap) < '" . $end_date . "' AND 
							(last_spesialisasi = '" . $layanan . "' OR last_spesialisasi2 = '" . $layanan . "') AND
							carapulang NOT LIKE 'Tidak Datang'
					");
					if ($row != null)
						$data['pasien_masuk'] += $row->jumlah;
				}

				/// c. Pasien Keluar Hidup
				/// c.1. Filter Bulanan
				if ($bulan != "%%") {
					/// c.1.1. Pasien Keluar Hidup Rentang Waktu Bulan Pilihan
					$start_date = $tahun . "-" . $bulan . "-01";
					$end_date = $tahun . "-" . $bulan . "-" . cal_days_in_month(CAL_GREGORIAN, $bulan, $tahun);
					if ($bulan == date("m") && $tahun == date("Y"))
						$end_date = date("Y-m-d");
					$row = $db->get_row("
						SELECT 
							COUNT(*) jumlah
						FROM 
							smis_rg_layananpasien
						WHERE 
							prop = '' AND 
							uri = 1 AND 
							DATE(tanggal_pulang) >= '" . $start_date . "' AND 
							DATE(tanggal_pulang) <= '" . $end_date . "' AND 
							(last_spesialisasi = '" . $layanan . "' OR last_spesialisasi2 = '" . $layanan . "') AND
							carapulang = 'Dipulangkan Hidup'
					");
					if ($row != null)
						$data['pasien_keluar_hidup'] += $row->jumlah;
				/// c.2. Filter Tahunan
				} else {
					/// c.2.1. Pasien Keluar Hidup Rentang Waktu Tahun Pilihan
					$start_date = $tahun . "-" . $bulan . "-01";
					$end_date = ($tahun + 1) . "-" . $bulan . "-01";
					$row = $db->get_row("
						SELECT 
							COUNT(*) jumlah
						FROM 
							smis_rg_layananpasien
						WHERE 
							prop = '' AND 
							uri = 1 AND 
							DATE(tanggal_pulang) >= '" . $start_date . "' AND 
							DATE(tanggal_pulang) < '" . $end_date . "' AND 
							(last_spesialisasi = '" . $layanan . "' OR last_spesialisasi2 = '" . $layanan . "') AND
							carapulang = 'Dipulangkan Hidup'
					");
					if ($row != null)
						$data['pasien_keluar_hidup'] += $row->jumlah;
				}

				/// d. Pasien Keluar Mati < 48 Jam
				/// d.1. Filter Bulanan
				if ($bulan != "%%") {
					/// d.1.1. Pasien Keluar Mati < 48 Jam Rentang Waktu Bulan Pilihan
					$start_date = $tahun . "-" . $bulan . "-01";
					$end_date = $tahun . "-" . $bulan . "-" . cal_days_in_month(CAL_GREGORIAN, $bulan, $tahun);
					if ($bulan == date("m") && $tahun == date("Y"))
						$end_date = date("Y-m-d");
					$row = $db->get_row("
						SELECT 
							COUNT(*) jumlah
						FROM 
							smis_rg_layananpasien
						WHERE 
							prop = '' AND 
							uri = 1 AND 
							DATE(tanggal_pulang) >= '" . $start_date . "' AND 
							DATE(tanggal_pulang) <= '" . $end_date . "' AND 
							(last_spesialisasi = '" . $layanan . "' OR last_spesialisasi2 = '" . $layanan . "') AND
							(
								carapulang = 'Dipulangkan Mati <=48 Jam' OR
								carapulang = 'Dipulangkan Mati <=24 Jam'
							)
					");
					if ($row != null)
						$data['pasien_keluar_mati_k48'] += $row->jumlah;
				/// d.2. Filter Tahunan
				} else {
					/// d.2.1. Pasien Keluar Mati < 48 Jam Rentang Waktu Tahun Pilihan
					$start_date = $tahun . "-" . $bulan . "-01";
					$end_date = ($tahun + 1) . "-" . $bulan . "-01";
					$row = $db->get_row("
						SELECT 
							COUNT(*) jumlah
						FROM 
							smis_rg_layananpasien
						WHERE 
							prop = '' AND 
							uri = 1 AND 
							DATE(tanggal_pulang) >= '" . $start_date . "' AND 
							DATE(tanggal_pulang) < '" . $end_date . "' AND 
							(last_spesialisasi = '" . $layanan . "' OR last_spesialisasi2 = '" . $layanan . "') AND
							(
								carapulang = 'Dipulangkan Mati <=48 Jam' OR
								carapulang = 'Dipulangkan Mati <=24 Jam'
							)
					");
					if ($row != null)
						$data['pasien_keluar_mati_k48'] += $row->jumlah;
				}

				/// e. Pasien Keluar Mati >= 48 Jam
				/// e.1. Filter Bulanan
				if ($bulan != "%%") {
					/// e.1.1. Pasien Keluar Mati >= 48 Jam Rentang Waktu Bulan Pilihan
					$start_date = $tahun . "-" . $bulan . "-01";
					$end_date = $tahun . "-" . $bulan . "-" . cal_days_in_month(CAL_GREGORIAN, $bulan, $tahun);
					if ($bulan == date("m") && $tahun == date("Y"))
						$end_date = date("Y-m-d");
					$row = $db->get_row("
						SELECT 
							COUNT(*) jumlah
						FROM 
							smis_rg_layananpasien
						WHERE 
							prop = '' AND 
							uri = 1 AND 
							DATE(tanggal_pulang) >= '" . $start_date . "' AND 
							DATE(tanggal_pulang) <= '" . $end_date . "' AND 
							(last_spesialisasi = '" . $layanan . "' OR last_spesialisasi2 = '" . $layanan . "') AND
							carapulang = 'Dipulangkan Mati >48 Jam'
					");
					if ($row != null)
						$data['pasien_keluar_mati_l48'] += $row->jumlah;
				/// e.2. Filter Tahunan
				} else {
					/// e.2.1. Pasien Keluar Mati >= 48 Jam Rentang Waktu Tahun Pilihan
					$start_date = $tahun . "-" . $bulan . "-01";
					$end_date = ($tahun + 1) . "-" . $bulan . "-01";
					$row = $db->get_row("
						SELECT 
							COUNT(*) jumlah
						FROM 
							smis_rg_layananpasien
						WHERE 
							prop = '' AND 
							uri = 1 AND 
							DATE(tanggal_pulang) >= '" . $start_date . "' AND 
							DATE(tanggal_pulang) < '" . $end_date . "' AND 
							(last_spesialisasi = '" . $layanan . "' OR last_spesialisasi2 = '" . $layanan . "') AND
							carapulang = 'Dipulangkan Mati >48 Jam'
					");
					if ($row != null)
						$data['pasien_keluar_mati_l48'] += $row->jumlah;
				}

				/// f. Jumlah HP, Rincian HP, dan LOS
				/// f.1 Filter Bulanan
				if ($bulan != "%%") {
					$days = cal_days_in_month(CAL_GREGORIAN, $bulan, $tahun);
					if ($bulan == date("m") && $tahun == date("Y"))
						$days = date("d");

					for ($i = 1; $i <= $days; $i++) {
						$tanggal = $tahun . "-" . $bulan . "-" . ArrayAdapter::format("only-digit2", $i);

						/// f.1.1. LOS
						$lama_dirawat_rows = $db->get_result("
		                    SELECT 
		                    	DATE(tanggal_inap) tanggal_mulai, DATE(tanggal_pulang) tanggal_selesai
		                    FROM 
		                    	smis_rg_layananpasien
		                    WHERE 
		                    	prop = '' AND 
		                    	carapulang NOT LIKE 'Tidak Datang' AND 
		                    	DATE(tanggal_pulang) = '" . $tanggal . "' AND
			                    (last_spesialisasi = '" . $layanan . "' OR last_spesialisasi2 = '" . $layanan . "') AND
			                    uri = 1
		                ");
		                if ($lama_dirawat_rows != null) {
		                    foreach ($lama_dirawat_rows as $ldr) {
		                        $tanggal_mulai = strtotime(ArrayAdapter::format("date Y-m-d", $ldr->tanggal_mulai));
		                        $tanggal_selesai = strtotime(ArrayAdapter::format("date Y-m-d", $ldr->tanggal_selesai));
		                        $datediff = $tanggal_selesai - $tanggal_mulai;
		                        $los = ceil($datediff / (60 * 60 * 24));
		                        if ($los < 1)
		                            $los = 1;
		                        $data['los'] += $los;
		                    }
		                }

		                /// f.1.2. HP VVIP
		                $jumlah_pasien_aktif_row = $db->get_row("
		                    SELECT 
		                        COUNT(id) jumlah_hari
		                    FROM 
		                        smis_rg_layananpasien
		                    WHERE 
		                        prop = '' AND 
		                        uri = 1 AND
		                        carapulang NOT LIKE 'Tidak Datang' AND  
		                        DATE(tanggal_inap) <= '" . $tanggal . "' AND 
		                        DATE(tanggal_pulang) = '0000-00-00' AND
		                        (last_spesialisasi = '" . $layanan . "' OR last_spesialisasi2 = '" . $layanan . "') AND
		                        last_kelas LIKE 'vvip'
		                ");
		                $jumlah_pasien_pulang_setelah_tanggal_row = $db->get_row("
		                    SELECT 
		                        COUNT(id) jumlah_hari
		                    FROM 
		                        smis_rg_layananpasien
		                    WHERE 
		                        prop = '' AND 
		                        uri = 1 AND
		                        carapulang NOT LIKE 'Tidak Datang' AND 
		                        DATE(tanggal_inap) <= '" . $tanggal . "' AND 
		                        DATE(tanggal_pulang) > '" . $tanggal . "' AND
		                        (last_spesialisasi = '" . $layanan . "' OR last_spesialisasi2 = '" . $layanan . "') AND
		                        last_kelas LIKE 'vvip'
		                ");
		                if ($jumlah_pasien_aktif_row != null)
		                    $data['jumlah_hp_vvip'] += $jumlah_pasien_aktif_row->jumlah_hari;
		                if ($jumlah_pasien_pulang_setelah_tanggal_row != null)
		                    $data['jumlah_hp_vvip'] += $jumlah_pasien_pulang_setelah_tanggal_row->jumlah_hari;

		                /// f.1.3. HP VIP
		                $jumlah_pasien_aktif_row = $db->get_row("
		                    SELECT 
		                        COUNT(id) jumlah_hari
		                    FROM 
		                        smis_rg_layananpasien
		                    WHERE 
		                        prop = '' AND 
		                        uri = 1 AND
		                        carapulang NOT LIKE 'Tidak Datang' AND  
		                        DATE(tanggal_inap) <= '" . $tanggal . "' AND 
		                        DATE(tanggal_pulang) = '0000-00-00' AND
		                        (last_spesialisasi = '" . $layanan . "' OR last_spesialisasi2 = '" . $layanan . "') AND
		                        last_kelas LIKE 'vip'
		                ");
		                $jumlah_pasien_pulang_setelah_tanggal_row = $db->get_row("
		                    SELECT 
		                        COUNT(id) jumlah_hari
		                    FROM 
		                        smis_rg_layananpasien
		                    WHERE 
		                        prop = '' AND 
		                        uri = 1 AND
		                        carapulang NOT LIKE 'Tidak Datang' AND 
		                        DATE(tanggal_inap) <= '" . $tanggal . "' AND 
		                        DATE(tanggal_pulang) > '" . $tanggal . "' AND
		                        (last_spesialisasi = '" . $layanan . "' OR last_spesialisasi2 = '" . $layanan . "') AND
		                        last_kelas LIKE 'vip'
		                ");
		                if ($jumlah_pasien_aktif_row != null)
		                    $data['jumlah_hp_vip'] += $jumlah_pasien_aktif_row->jumlah_hari;
		                if ($jumlah_pasien_pulang_setelah_tanggal_row != null)
		                    $data['jumlah_hp_vip'] += $jumlah_pasien_pulang_setelah_tanggal_row->jumlah_hari;

		                /// f.1.4. HP Kelas I
		                $jumlah_pasien_aktif_row = $db->get_row("
		                    SELECT 
		                        COUNT(id) jumlah_hari
		                    FROM 
		                        smis_rg_layananpasien
		                    WHERE 
		                        prop = '' AND 
		                        uri = 1 AND
		                        carapulang NOT LIKE 'Tidak Datang' AND  
		                        DATE(tanggal_inap) <= '" . $tanggal . "' AND 
		                        DATE(tanggal_pulang) = '0000-00-00' AND
		                        (last_spesialisasi = '" . $layanan . "' OR last_spesialisasi2 = '" . $layanan . "') AND
		                        last_kelas LIKE 'kelas_i'
		                ");
		                $jumlah_pasien_pulang_setelah_tanggal_row = $db->get_row("
		                    SELECT 
		                        COUNT(id) jumlah_hari
		                    FROM 
		                        smis_rg_layananpasien
		                    WHERE 
		                        prop = '' AND 
		                        uri = 1 AND
		                        carapulang NOT LIKE 'Tidak Datang' AND 
		                        DATE(tanggal_inap) <= '" . $tanggal . "' AND 
		                        DATE(tanggal_pulang) > '" . $tanggal . "' AND
		                        (last_spesialisasi = '" . $layanan . "' OR last_spesialisasi2 = '" . $layanan . "') AND
		                        last_kelas LIKE 'kelas_i'
		                ");
		                if ($jumlah_pasien_aktif_row != null)
		                    $data['jumlah_hp_i'] += $jumlah_pasien_aktif_row->jumlah_hari;
		                if ($jumlah_pasien_pulang_setelah_tanggal_row != null)
		                    $data['jumlah_hp_i'] += $jumlah_pasien_pulang_setelah_tanggal_row->jumlah_hari;

		                /// f.1.5. HP Kelas II
		                $jumlah_pasien_aktif_row = $db->get_row("
		                    SELECT 
		                        COUNT(id) jumlah_hari
		                    FROM 
		                        smis_rg_layananpasien
		                    WHERE 
		                        prop = '' AND 
		                        uri = 1 AND
		                        carapulang NOT LIKE 'Tidak Datang' AND  
		                        DATE(tanggal_inap) <= '" . $tanggal . "' AND 
		                        DATE(tanggal_pulang) = '0000-00-00' AND
		                        (last_spesialisasi = '" . $layanan . "' OR last_spesialisasi2 = '" . $layanan . "') AND
		                        last_kelas LIKE 'kelas_ii'
		                ");
		                $jumlah_pasien_pulang_setelah_tanggal_row = $db->get_row("
		                    SELECT 
		                        COUNT(id) jumlah_hari
		                    FROM 
		                        smis_rwt_antrian_" . $ruangan . "
		                    WHERE 
		                        prop = '' AND 
		                        uri = 1 AND
		                        carapulang NOT LIKE 'Tidak Datang' AND 
		                        DATE(tanggal_inap) <= '" . $tanggal . "' AND 
		                        DATE(tanggal_pulang) > '" . $tanggal . "' AND
		                        (last_spesialisasi = '" . $layanan . "' OR last_spesialisasi2 = '" . $layanan . "') AND
		                        last_kelas LIKE 'kelas_ii'
		                ");
		                if ($jumlah_pasien_aktif_row != null)
		                    $data['jumlah_hp_ii'] += $jumlah_pasien_aktif_row->jumlah_hari;
		                if ($jumlah_pasien_pulang_setelah_tanggal_row != null)
		                    $data['jumlah_hp_ii'] += $jumlah_pasien_pulang_setelah_tanggal_row->jumlah_hari;

		                /// f.1.6. HP Kelas III
		                $jumlah_pasien_aktif_row = $db->get_row("
		                    SELECT 
		                        COUNT(id) jumlah_hari
		                    FROM 
		                        smis_rg_layananpasien
		                    WHERE 
		                        prop = '' AND 
		                        uri = 1 AND
		                        carapulang NOT LIKE 'Tidak Datang' AND  
		                        DATE(tanggal_inap) <= '" . $tanggal . "' AND 
		                        DATE(tanggal_pulang) = '0000-00-00' AND
		                        (last_spesialisasi = '" . $layanan . "' OR last_spesialisasi2 = '" . $layanan . "') AND
		                        last_kelas LIKE 'kelas_iii'
		                ");
		                $jumlah_pasien_pulang_setelah_tanggal_row = $db->get_row("
		                    SELECT 
		                        COUNT(id) jumlah_hari
		                    FROM 
		                        smis_rg_layananpasien
		                    WHERE 
		                        prop = '' AND 
		                        uri = 1 AND
		                        carapulang NOT LIKE 'Tidak Datang' AND 
		                        DATE(tanggal_inap) <= '" . $tanggal . "' AND 
		                        DATE(tanggal_pulang) > '" . $tanggal . "' AND
		                        (last_spesialisasi = '" . $layanan . "' OR last_spesialisasi2 = '" . $layanan . "') AND
		                        last_kelas LIKE 'kelas_iii'
		                ");
		                if ($jumlah_pasien_aktif_row != null)
		                    $data['jumlah_hp_iii'] += $jumlah_pasien_aktif_row->jumlah_hari;
		                if ($jumlah_pasien_pulang_setelah_tanggal_row != null)
		                    $data['jumlah_hp_iii'] += $jumlah_pasien_pulang_setelah_tanggal_row->jumlah_hari;

		                /// f.1.7. HP Kelas Khusus
		                $jumlah_pasien_aktif_row = $db->get_row("
		                    SELECT 
		                        COUNT(id) jumlah_hari
		                    FROM 
		                        smis_rg_layananpasien
		                    WHERE 
		                        prop = '' AND 
		                        uri = 1 AND
		                        carapulang NOT LIKE 'Tidak Datang' AND  
		                        DATE(tanggal_inap) <= '" . $tanggal . "' AND 
		                        DATE(tanggal_pulang) = '0000-00-00' AND
		                        (last_spesialisasi = '" . $layanan . "' OR last_spesialisasi2 = '" . $layanan . "') AND
		                        last_kelas LIKE 'icu'
		                ");
		                $jumlah_pasien_pulang_setelah_tanggal_row = $db->get_row("
		                    SELECT 
		                        COUNT(id) jumlah_hari
		                    FROM 
		                        smis_rg_layananpasien
		                    WHERE 
		                        prop = '' AND 
		                        uri = 1 AND
		                        carapulang NOT LIKE 'Tidak Datang' AND 
		                        DATE(tanggal_inap) <= '" . $tanggal . "' AND 
		                        DATE(tanggal_pulang) > '" . $tanggal . "' AND
		                        (last_spesialisasi = '" . $layanan . "' OR last_spesialisasi2 = '" . $layanan . "') AND
		                        last_kelas LIKE 'icu'
		                ");
		                if ($jumlah_pasien_aktif_row != null)
		                    $data['jumlah_hp_khusus'] += $jumlah_pasien_aktif_row->jumlah_hari;
		                if ($jumlah_pasien_pulang_setelah_tanggal_row != null)
		                    $data['jumlah_hp_khusus'] += $jumlah_pasien_pulang_setelah_tanggal_row->jumlah_hari;

		                /// f.1.8 HP Total
		                $data['jumlah_hp_total'] = $data['jumlah_hp_vvip'] + $data['jumlah_hp_vip'] + $data['jumlah_hp_i'] + $data['jumlah_hp_ii'] + $data['jumlah_hp_iii'] + $data['jumlah_hp_khusus'];
					}
				/// f.2 Filter Tahunan	
				} else {
					for ($j = 1; $j <= 12; $j++) {
						$days = cal_days_in_month(CAL_GREGORIAN, $j, $tahun);
						if ($j == date("m") && $tahun == date("Y"))
							$days = date("d");

						for ($i = 1; $i <= $days; $i++) {
							$tanggal = $tahun . "-" . $j . "-" . ArrayAdapter::format("only-digit2", $i);

							/// f.2.1. LOS
							$lama_dirawat_rows = $db->get_result("
			                    SELECT 
			                    	DATE(tanggal_inap) tanggal_mulai, DATE(tanggal_pulang) tanggal_selesai
			                    FROM 
			                    	smis_rg_layananpasien
			                    WHERE 
			                    	prop = '' AND 
			                    	carapulang NOT LIKE 'Tidak Datang' AND 
			                    	DATE(tanggal_pulang) = '" . $tanggal . "' AND
			                    	(last_spesialisasi = '" . $layanan . "' OR last_spesialisasi2 = '" . $layanan . "') AND
			                    	uri = 0
			                ");
			                if ($lama_dirawat_rows != null) {
			                    foreach ($lama_dirawat_rows as $ldr) {
			                        $tanggal_mulai = strtotime(ArrayAdapter::format("date Y-m-d", $ldr->tanggal_mulai));
			                        $tanggal_selesai = strtotime(ArrayAdapter::format("date Y-m-d", $ldr->tanggal_selesai));
			                        $datediff = $tanggal_selesai - $tanggal_mulai;
			                        $los = ceil($datediff / (60 * 60 * 24));
			                        if ($los < 1)
			                            $los = 1;
			                        $data['los'] += $los;
			                    }
			                }

			                 /// f.2.2. HP VVIP
			                $jumlah_pasien_aktif_row = $db->get_row("
			                    SELECT 
			                        COUNT(id) jumlah_hari
			                    FROM 
			                        smis_rg_layananpasien
			                    WHERE 
			                        prop = '' AND 
			                        uri = 1 AND
			                        carapulang NOT LIKE 'Tidak Datang' AND  
			                        DATE(tanggal_inap) <= '" . $tanggal . "' AND 
			                        DATE(tanggal_pulang) = '0000-00-00' AND
			                        (last_spesialisasi = '" . $layanan . "' OR last_spesialisasi2 = '" . $layanan . "') AND
			                        last_kelas LIKE 'vvip'
			                ");
			                $jumlah_pasien_pulang_setelah_tanggal_row = $db->get_row("
			                    SELECT 
			                        COUNT(id) jumlah_hari
			                    FROM 
			                        smis_rg_layananpasien
			                    WHERE 
			                        prop = '' AND 
			                        uri = 1 AND
			                        carapulang NOT LIKE 'Tidak Datang' AND 
			                        DATE(tanggal_inap) <= '" . $tanggal . "' AND 
			                        DATE(tanggal_pulang) > '" . $tanggal . "' AND
			                        (last_spesialisasi = '" . $layanan . "' OR last_spesialisasi2 = '" . $layanan . "') AND
			                        last_kelas LIKE 'vvip'
			                ");
			                if ($jumlah_pasien_aktif_row != null)
			                    $data['jumlah_hp_vvip'] += $jumlah_pasien_aktif_row->jumlah_hari;
			                if ($jumlah_pasien_pulang_setelah_tanggal_row != null)
			                    $data['jumlah_hp_vvip'] += $jumlah_pasien_pulang_setelah_tanggal_row->jumlah_hari;

			                /// f.2.3. HP VIP
			                $jumlah_pasien_aktif_row = $db->get_row("
			                    SELECT 
			                        COUNT(id) jumlah_hari
			                    FROM 
			                        smis_rg_layananpasien
			                    WHERE 
			                        prop = '' AND 
			                        uri = 1 AND
			                        carapulang NOT LIKE 'Tidak Datang' AND  
			                        DATE(tanggal_inap) <= '" . $tanggal . "' AND 
			                        DATE(tanggal_pulang) = '0000-00-00' AND
			                        (last_spesialisasi = '" . $layanan . "' OR last_spesialisasi2 = '" . $layanan . "') AND
			                        last_kelas LIKE 'vip'
			                ");
			                $jumlah_pasien_pulang_setelah_tanggal_row = $db->get_row("
			                    SELECT 
			                        COUNT(id) jumlah_hari
			                    FROM 
			                        smis_rg_layananpasien
			                    WHERE 
			                        prop = '' AND 
			                        uri = 1 AND
			                        carapulang NOT LIKE 'Tidak Datang' AND 
			                        DATE(tanggal_inap) <= '" . $tanggal . "' AND 
			                        DATE(tanggal_pulang) > '" . $tanggal . "' AND
			                        (last_spesialisasi = '" . $layanan . "' OR last_spesialisasi2 = '" . $layanan . "') AND
			                        last_kelas LIKE 'vip'
			                ");
			                if ($jumlah_pasien_aktif_row != null)
			                    $data['jumlah_hp_vip'] += $jumlah_pasien_aktif_row->jumlah_hari;
			                if ($jumlah_pasien_pulang_setelah_tanggal_row != null)
			                    $data['jumlah_hp_vip'] += $jumlah_pasien_pulang_setelah_tanggal_row->jumlah_hari;

			                /// f.2.4. HP Kelas I
			                $jumlah_pasien_aktif_row = $db->get_row("
			                    SELECT 
			                        COUNT(id) jumlah_hari
			                    FROM 
			                        smis_rg_layananpasien
			                    WHERE 
			                        prop = '' AND 
			                        uri = 1 AND
			                        carapulang NOT LIKE 'Tidak Datang' AND  
			                        DATE(tanggal_inap) <= '" . $tanggal . "' AND 
			                        DATE(tanggal_pulang) = '0000-00-00' AND
			                        (last_spesialisasi = '" . $layanan . "' OR last_spesialisasi2 = '" . $layanan . "') AND
			                        last_kelas LIKE 'kelas_i'
			                ");
			                $jumlah_pasien_pulang_setelah_tanggal_row = $db->get_row("
			                    SELECT 
			                        COUNT(id) jumlah_hari
			                    FROM 
			                        smis_rg_layananpasien
			                    WHERE 
			                        prop = '' AND 
			                        uri = 1 AND
			                        carapulang NOT LIKE 'Tidak Datang' AND 
			                        DATE(tanggal_inap) <= '" . $tanggal . "' AND 
			                        DATE(tanggal_pulang) > '" . $tanggal . "' AND
			                        (last_spesialisasi = '" . $layanan . "' OR last_spesialisasi2 = '" . $layanan . "') AND
			                        last_kelas LIKE 'kelas_i'
			                ");
			                if ($jumlah_pasien_aktif_row != null)
			                    $data['jumlah_hp_i'] += $jumlah_pasien_aktif_row->jumlah_hari;
			                if ($jumlah_pasien_pulang_setelah_tanggal_row != null)
			                    $data['jumlah_hp_i'] += $jumlah_pasien_pulang_setelah_tanggal_row->jumlah_hari;

			                /// f.2.5. HP Kelas II
			                $jumlah_pasien_aktif_row = $db->get_row("
			                    SELECT 
			                        COUNT(id) jumlah_hari
			                    FROM 
			                        smis_rg_layananpasien
			                    WHERE 
			                        prop = '' AND 
			                        uri = 1 AND
			                        carapulang NOT LIKE 'Tidak Datang' AND  
			                        DATE(tanggal_inap) <= '" . $tanggal . "' AND 
			                        DATE(tanggal_pulang) = '0000-00-00' AND
			                        (last_spesialisasi = '" . $layanan . "' OR last_spesialisasi2 = '" . $layanan . "') AND
			                        last_kelas LIKE 'kelas_ii'
			                ");
			                $jumlah_pasien_pulang_setelah_tanggal_row = $db->get_row("
			                    SELECT 
			                        COUNT(id) jumlah_hari
			                    FROM 
			                        smis_rg_layananpasien
			                    WHERE 
			                        prop = '' AND 
			                        uri = 1 AND
			                        carapulang NOT LIKE 'Tidak Datang' AND 
			                        DATE(tanggal_inap) <= '" . $tanggal . "' AND 
			                        DATE(tanggal_pulang) > '" . $tanggal . "' AND
			                        (last_spesialisasi = '" . $layanan . "' OR last_spesialisasi2 = '" . $layanan . "') AND
			                        last_kelas LIKE 'kelas_ii'
			                ");
			                if ($jumlah_pasien_aktif_row != null)
			                    $data['jumlah_hp_ii'] += $jumlah_pasien_aktif_row->jumlah_hari;
			                if ($jumlah_pasien_pulang_setelah_tanggal_row != null)
			                    $data['jumlah_hp_ii'] += $jumlah_pasien_pulang_setelah_tanggal_row->jumlah_hari;

			                /// f.2.6. HP Kelas III
			                $jumlah_pasien_aktif_row = $db->get_row("
			                    SELECT 
			                        COUNT(id) jumlah_hari
			                    FROM 
			                        smis_rg_layananpasien
			                    WHERE 
			                        prop = '' AND 
			                        uri = 1 AND
			                        carapulang NOT LIKE 'Tidak Datang' AND  
			                        DATE(tanggal_inap) <= '" . $tanggal . "' AND 
			                        DATE(tanggal_pulang) = '0000-00-00' AND
			                        (last_spesialisasi = '" . $layanan . "' OR last_spesialisasi2 = '" . $layanan . "') AND
			                        last_kelas LIKE 'kelas_iii'
			                ");
			                $jumlah_pasien_pulang_setelah_tanggal_row = $db->get_row("
			                    SELECT 
			                        COUNT(id) jumlah_hari
			                    FROM 
			                        smis_rg_layananpasien
			                    WHERE 
			                        prop = '' AND 
			                        uri = 1 AND
			                        carapulang NOT LIKE 'Tidak Datang' AND 
			                        DATE(tanggal_inap) <= '" . $tanggal . "' AND 
			                        DATE(tanggal_pulang) > '" . $tanggal . "' AND
			                        (last_spesialisasi = '" . $layanan . "' OR last_spesialisasi2 = '" . $layanan . "') AND
			                        last_kelas LIKE 'kelas_iii'
			                ");
			                if ($jumlah_pasien_aktif_row != null)
			                    $data['jumlah_hp_iii'] += $jumlah_pasien_aktif_row->jumlah_hari;
			                if ($jumlah_pasien_pulang_setelah_tanggal_row != null)
			                    $data['jumlah_hp_iii'] += $jumlah_pasien_pulang_setelah_tanggal_row->jumlah_hari;

			                /// f.2.7. HP Kelas Khusus
			                $jumlah_pasien_aktif_row = $db->get_row("
			                    SELECT 
			                        COUNT(id) jumlah_hari
			                    FROM 
			                        smis_rg_layananpasien
			                    WHERE 
			                        prop = '' AND 
			                        uri = 1 AND
			                        carapulang NOT LIKE 'Tidak Datang' AND  
			                        DATE(tanggal_inap) <= '" . $tanggal . "' AND 
			                        DATE(tanggal_pulang) = '0000-00-00' AND
			                        (last_spesialisasi = '" . $layanan . "' OR last_spesialisasi2 = '" . $layanan . "') AND
			                        last_kelas LIKE 'icu'
			                ");
			                $jumlah_pasien_pulang_setelah_tanggal_row = $db->get_row("
			                    SELECT 
			                        COUNT(id) jumlah_hari
			                    FROM 
			                        smis_rg_layananpasien
			                    WHERE 
			                        prop = '' AND 
			                        uri = 1 AND
			                        carapulang NOT LIKE 'Tidak Datang' AND 
			                        DATE(tanggal_inap) <= '" . $tanggal . "' AND 
			                        DATE(tanggal_pulang) > '" . $tanggal . "' AND
			                        (last_spesialisasi = '" . $layanan . "' OR last_spesialisasi2 = '" . $layanan . "') AND
			                        last_kelas LIKE 'icu'
			                ");
			                if ($jumlah_pasien_aktif_row != null)
			                    $data['jumlah_hp_khusus'] += $jumlah_pasien_aktif_row->jumlah_hari;
			                if ($jumlah_pasien_pulang_setelah_tanggal_row != null)
			                    $data['jumlah_hp_khusus'] += $jumlah_pasien_pulang_setelah_tanggal_row->jumlah_hari;

			                /// f.2.8 HP Total
		                	$data['jumlah_hp_total'] = $data['jumlah_hp_vvip'] + $data['jumlah_hp_vip'] + $data['jumlah_hp_i'] + $data['jumlah_hp_ii'] + $data['jumlah_hp_iii'] + $data['jumlah_hp_khusus'];
						}
					}
				}

				$data['pasien_akhir'] = $data['pasien_awal'] + $data['pasien_masuk'] - $data['pasien_keluar_hidup'] - $data['pasien_keluar_mati_k48'] - $data['pasien_keluar_mati_l48'];

				$total_pasien_awal += $data['pasien_awal'];
				$total_pasien_masuk += $data['pasien_masuk'];
				$total_pasien_keluar_hidup += $data['pasien_keluar_hidup'];
				$total_pasien_keluar_mati_k48 += $data['pasien_keluar_mati_k48'];
				$total_pasien_keluar_mati_l48 += $data['pasien_keluar_mati_l48'];
				$total_los += $data['los'];
				$total_pasien_akhir += $data['pasien_akhir'];
				$total_jumlah_hp_total += $data['jumlah_hp_total'];
				$total_jumlah_hp_vvip += $data['jumlah_hp_vvip'];
				$total_jumlah_hp_vip += $data['jumlah_hp_vip'];
				$total_jumlah_hp_i += $data['jumlah_hp_i'];
				$total_jumlah_hp_ii += $data['jumlah_hp_ii'];
				$total_jumlah_hp_iii += $data['jumlah_hp_iii'];
				$total_jumlah_hp_khusus += $data['jumlah_hp_khusus'];

				$html .= 	"<tr class='data_rl31'>" .
								"<td id='rl31_kode_propinsi'>" . $kode_propinsi . "</td>" .
								"<td id='rl31_kabupaten_kota'>" . $kabupaten_kota . "</td>" .
								"<td id='rl31_kode_rs'>" . $kode_rs . "</td>" .
								"<td id='rl31_nama_rs'>" . $nama_rs . "</td>" .
								"<td id='rl31_tahun'>" . $tahun . "</td>" .
								"<td id='rl31_nomor'>" . $nomor++ . "</td>" .
								"<td id='rl31_jenis_layanan'>" . $layanan . "</td>" .
								"<td id='rl31_f_pasien_awal'><div align='right'>" . $data['pasien_awal'] . "</div></td>" .
								"<td id='rl31_pasien_awal' style='display:none;'>" . $data['pasien_awal'] . "</td>" .
								"<td id='rl31_f_pasien_masuk'><div align='right'>" . $data['pasien_masuk'] . "</div></td>" .
								"<td id='rl31_pasien_masuk' style='display:none;'>" . $data['pasien_masuk'] . "</td>" .
								"<td id='rl31_f_pasien_keluar_hidup'><div align='right'>" . $data['pasien_keluar_hidup'] . "</div></td>" .
								"<td id='rl31_pasien_keluar_hidup' style='display:none;'>" . $data['pasien_keluar_hidup'] . "</td>" .
								"<td id='rl31_f_pasien_keluar_mati_k48'><div align='right'>" . $data['pasien_keluar_mati_k48'] . "</div></td>" .
								"<td id='rl31_pasien_keluar_mati_k48' style='display:none;'>" . $data['pasien_keluar_mati_k48'] . "</td>" .
								"<td id='rl31_f_pasien_keluar_mati_l48'><div align='right'>" . $data['pasien_keluar_mati_l48'] . "</div></td>" .
								"<td id='rl31_pasien_keluar_mati_l48' style='display:none;'>" . $data['pasien_keluar_mati_l48'] . "</td>" .
								"<td id='rl31_f_los'><div align='right'>" . $data['los'] . "</div></td>" .
								"<td id='rl31_los' style='display:none;'>" . $data['los'] . "</td>" .
								"<td id='rl31_f_pasien_akhir'><div align='right'>" . $data['pasien_akhir'] . "</div></td>" .
								"<td id='rl31_pasien_akhir' style='display:none;'>" . $data['pasien_akhir'] . "</td>" .
								"<td id='rl31_f_jumlah_hp_total'><div align='right'>" . $data['jumlah_hp_total'] . "</div></td>" .
								"<td id='rl31_jumlah_hp_total' style='display:none;'>" . $data['jumlah_hp_total'] . "</td>" .
								"<td id='rl31_f_jumlah_hp_vvip'><div align='right'>" . $data['jumlah_hp_vvip'] . "</div></td>" .
								"<td id='rl31_jumlah_hp_vvip' style='display:none;'>" . $data['jumlah_hp_vvip'] . "</td>" .
								"<td id='rl31_f_jumlah_hp_vip'><div align='right'>" . $data['jumlah_hp_vip'] . "</div></td>" .
								"<td id='rl31_jumlah_hp_vip' style='display:none;'>" . $data['jumlah_hp_vip'] . "</td>" .
								"<td id='rl31_f_jumlah_hp_i'><div align='right'>" . $data['jumlah_hp_i'] . "</div></td>" .
								"<td id='rl31_jumlah_hp_i' style='display:none;'>" . $data['jumlah_hp_i'] . "</td>" .
								"<td id='rl31_f_jumlah_hp_ii'><div align='right'>" . $data['jumlah_hp_ii'] . "</div></td>" .
								"<td id='rl31_jumlah_hp_ii' style='display:none;'>" . $data['jumlah_hp_ii'] . "</td>" .
								"<td id='rl31_f_jumlah_hp_iii'><div align='right'>" . $data['jumlah_hp_iii'] . "</div></td>" .
								"<td id='rl31_jumlah_hp_iii' style='display:none;'>" . $data['jumlah_hp_iii'] . "</td>" .
								"<td id='rl31_f_jumlah_hp_khusus'><div align='right'>" . $data['jumlah_hp_khusus'] . "</div></td>" .
								"<td id='rl31_jumlah_hp_khusus' style='display:none;'>" . $data['jumlah_hp_khusus'] . "</td>" .
							"</tr>";
			}
			$html .= 	"<tr class='data_rl31'>" .
							"<td id='rl31_kode_propinsi'>" . $kode_propinsi . "</td>" .
							"<td id='rl31_kabupaten_kota'>" . $kabupaten_kota . "</td>" .
							"<td id='rl31_kode_rs'>" . $kode_rs . "</td>" .
							"<td id='rl31_nama_rs'>" . $nama_rs . "</td>" .
							"<td id='rl31_tahun'>" . $tahun . "</td>" .
							"<td id='rl31_nomor'>99</td>" .
							"<td id='rl31_jenis_layanan'>T O T A L</td>" .
							"<td id='rl31_f_pasien_awal'><div align='right'>" . $total_pasien_awal . "</div></td>" .
							"<td id='rl31_pasien_awal' style='display:none;'>" . $total_pasien_awal . "</td>" .
							"<td id='rl31_f_pasien_masuk'><div align='right'>" . $total_pasien_masuk . "</div></td>" .
							"<td id='rl31_pasien_masuk' style='display:none;'>" . $total_pasien_masuk . "</td>" .
							"<td id='rl31_f_pasien_keluar_hidup'><div align='right'>" . $total_pasien_keluar_hidup . "</div></td>" .
							"<td id='rl31_pasien_keluar_hidup' style='display:none;'>" . $total_pasien_keluar_hidup . "</td>" .
							"<td id='rl31_f_pasien_keluar_mati_k48'><div align='right'>" . $total_pasien_keluar_mati_k48 . "</div></td>" .
							"<td id='rl31_pasien_keluar_mati_k48' style='display:none;'>" . $total_pasien_keluar_mati_k48 . "</td>" .
							"<td id='rl31_f_pasien_keluar_mati_l48'><div align='right'>" . $total_pasien_keluar_mati_l48 . "</div></td>" .
							"<td id='rl31_pasien_keluar_mati_l48' style='display:none;'>" . $total_pasien_keluar_mati_l48 . "</td>" .
							"<td id='rl31_f_los'><div align='right'>" . $total_los . "</div></td>" .
							"<td id='rl31_los' style='display:none;'>" . $total_los . "</td>" .
							"<td id='rl31_f_pasien_akhir'><div align='right'>" . $total_pasien_akhir . "</div></td>" .
							"<td id='rl31_pasien_akhir' style='display:none;'>" . $total_pasien_akhir . "</td>" .
							"<td id='rl31_f_jumlah_hp_total'><div align='right'>" . $total_jumlah_hp_total . "</div></td>" .
							"<td id='rl31_jumlah_hp_total' style='display:none;'>" . $total_jumlah_hp_total . "</td>" .
							"<td id='rl31_f_jumlah_hp_vvip'><div align='right'>" . $total_jumlah_hp_vvip . "</div></td>" .
							"<td id='rl31_jumlah_hp_vvip' style='display:none;'>" . $total_jumlah_hp_vvip . "</td>" .
							"<td id='rl31_f_jumlah_hp_vip'><div align='right'>" . $total_jumlah_hp_vip . "</div></td>" .
							"<td id='rl31_jumlah_hp_vip' style='display:none;'>" . $total_jumlah_hp_vip . "</td>" .
							"<td id='rl31_f_jumlah_hp_i'><div align='right'>" . $total_jumlah_hp_i . "</div></td>" .
							"<td id='rl31_jumlah_hp_i' style='display:none;'>" . $total_jumlah_hp_i . "</td>" .
							"<td id='rl31_f_jumlah_hp_ii'><div align='right'>" . $total_jumlah_hp_ii . "</div></td>" .
							"<td id='rl31_jumlah_hp_ii' style='display:none;'>" . $total_jumlah_hp_ii . "</td>" .
							"<td id='rl31_f_jumlah_hp_iii'><div align='right'>" . $total_jumlah_hp_iii . "</div></td>" .
							"<td id='rl31_jumlah_hp_iii' style='display:none;'>" . $total_jumlah_hp_iii . "</td>" .
							"<td id='rl31_f_jumlah_hp_khusus'><div align='right'>" . $total_jumlah_hp_khusus . "</div></td>" .
							"<td id='rl31_jumlah_hp_khusus' style='display:none;'>" . $total_jumlah_hp_khusus . "</td>" .
						"</tr>";

			$data = array();
			$data['html'] = $html;
			echo json_encode($data);
		} else if ($_POST['command'] == "export_xls") {
			require_once ("smis-libs-out/php-excel/PHPExcel.php");

		    $file = new PHPExcel ();
		    $file->getProperties ()->setCreator ( "PT. Inovasi Ide Utama" );
		    $file->getProperties ()->setTitle ( "RL 3.1 - Kegiatan Pelayanan Rawat Inap" );
		    $file->getProperties ()->setSubject ( "RL 3.1 - Kegiatan Pelayanan Rawat Inap" );
		    $file->getProperties ()->setDescription ( "RL 3.1 - Kegiatan Pelayanan Rawat Inap Generated by System" );
		    $file->getProperties ()->setKeywords ( "RL 3.1 - Kegiatan Pelayanan Rawat Inap" );
		    $file->getProperties ()->setCategory ( "RL 3.1 - Kegiatan Pelayanan Rawat Inap" );

		    $sheet = $file->getActiveSheet ();
		    $i = 1;

		    $sheet->mergeCells("A".$i.":U".$i)->setCellValue("A".$i,"RL 3.1 - Kegiatan Pelayanan Rawat Inap");
		    $sheet->getStyle("A".$i.":U".$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		    $sheet->getStyle("A".$i)->getFont()->setBold(true);
		    $i = $i + 2;

		    $border_start = $i;
		    
		    $sheet->mergeCells("A".$i.":A".($i + 2))->setCellValue("A".$i, "KODE RS");
		    $sheet->mergeCells("B".$i.":B".($i + 2))->setCellValue("B".$i, "KODE PROPINSI");
		    $sheet->mergeCells("C".$i.":C".($i + 2))->setCellValue("C".$i, "KAB/KOTA");
		    $sheet->mergeCells("D".$i.":D".($i + 2))->setCellValue("D".$i, "NAMA RS");
		    $sheet->mergeCells("E".$i.":E".($i + 2))->setCellValue("E".$i, "TAHUN");
		    $sheet->mergeCells("F".$i.":F".($i + 1))->setCellValue("F".$i, "NO.");
		    $sheet->setCellValue("F".($i + 2), "1");
		    $sheet->mergeCells("G".$i.":G".($i + 1))->setCellValue("G".$i, "JENIS PELAYANAN");
		    $sheet->setCellValue("G".($i + 2), "2");
		    $sheet->mergeCells("H".$i.":H".($i + 1))->setCellValue("H".$i, "PASIEN AWAL TAHUN");
		    $sheet->setCellValue("H".($i + 2), "3");
		    $sheet->mergeCells("I".$i.":I".($i + 1))->setCellValue("I".$i, "PASIEN MASUK");
		    $sheet->setCellValue("I".($i + 2), "4");
		    $sheet->mergeCells("J".$i.":J".($i + 1))->setCellValue("J".$i, "PASIEN KELUAR HIDUP");
		    $sheet->setCellValue("J".($i + 2), "5");
		    $sheet->mergeCells("K".$i.":L".$i)->setCellValue("K".$i, "PASIEN KELUAR MATI");
		    $sheet->setCellValue("K".($i + 1), "< 48 Jam");
		    $sheet->setCellValue("K".($i + 2), "6");
		    $sheet->setCellValue("L".($i + 1), ">= 48 Jam");
		    $sheet->setCellValue("L".($i + 2), "7");
		    $sheet->mergeCells("M".$i.":M".($i + 1))->setCellValue("M".$i, "Jumlah Lama Dirawat");
		    $sheet->setCellValue("M".($i + 2), "8");
		    $sheet->mergeCells("N".$i.":N".($i + 1))->setCellValue("N".$i, "Pasien Akhir Tahun");
		    $sheet->setCellValue("N".($i + 2), "9");
		    $sheet->mergeCells("O".$i.":O".($i + 1))->setCellValue("O".$i, "Jumlah Hari Perawatan");
		    $sheet->setCellValue("O".($i + 2), "10");
		    $sheet->mergeCells("P".$i.":U".$i)->setCellValue("P".$i, "RINCIAN HARI PERAWATAN PER KELAS");
		    $sheet->setCellValue("P".($i + 1), "VVIP");
		    $sheet->setCellValue("P".($i + 2), "11");
		    $sheet->setCellValue("Q".($i + 1), "VIP");
		    $sheet->setCellValue("Q".($i + 2), "12");
		    $sheet->setCellValue("R".($i + 1), "I");
		    $sheet->setCellValue("R".($i + 2), "13");
		    $sheet->setCellValue("S".($i + 1), "II");
		    $sheet->setCellValue("S".($i + 2), "14");
		    $sheet->setCellValue("T".($i + 1), "III");
		    $sheet->setCellValue("T".($i + 2), "15");
		    $sheet->setCellValue("U".($i + 1), "KELAS KHUSUS");
		    $sheet->setCellValue("U".($i + 2), "16");
		    $sheet->getStyle("A".$i.":U".($i + 2))->getFont()->setBold(true);
		    $sheet->getStyle("A".$i.":U".($i + 2))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		    $sheet->getStyle("A".$i.":U".($i + 2))->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
		    $i = $i + 2;

		    $kode_rs = "";
			$nama_rs = "";
			$kode_propinsi = "";
			$kabupaten_kota = "";
			$info_rs_row = $db->get_row("
				SELECT 
					*
				FROM 
					smis_mr_data_dasar_rs
				WHERE 
					prop = ''
				LIMIT 
					0, 1
			");
			if ($info_rs_row != null) {
				$kode_rs = $info_rs_row->koders;
				$nama_rs = $info_rs_row->nama_rs;
				$kode_propinsi = $info_rs_row->kode_provinsi;
				$kabupaten_kota = $info_rs_row->kota_kabupaten;
			}

		    $data = json_decode($_POST['detail']);
		    foreach ($data as $d) {
			    $i = $i + 1;
			    $sheet->setCellValue("A".$i,$d->kode_rs);
			    $sheet->setCellValue("B".$i,$d->kode_propinsi);
			    $sheet->setCellValue("C".$i,$d->kabupaten_kota);
			    $sheet->setCellValue("D".$i,$d->nama_rs);
			    $sheet->setCellValue("E".$i,$d->tahun);
			    $sheet->setCellValue("F".$i,$d->nomor);
			    $sheet->setCellValue("G".$i,$d->jenis_layanan);
			    $sheet->setCellValue("H".$i,$d->pasien_awal);
			    $sheet->setCellValue("I".$i,$d->pasien_masuk);
			    $sheet->setCellValue("J".$i,$d->pasien_keluar_hidup);
			    $sheet->setCellValue("K".$i,$d->pasien_keluar_mati_k48);
			    $sheet->setCellValue("L".$i,$d->pasien_keluar_mati_l48);
			    $sheet->setCellValue("M".$i,$d->los);
			    $sheet->setCellValue("N".$i,$d->pasien_akhir);
			    $sheet->setCellValue("O".$i,$d->jumlah_hp_total);
			    $sheet->setCellValue("P".$i,$d->jumlah_hp_vvip);
			    $sheet->setCellValue("Q".$i,$d->jumlah_hp_vip);
			    $sheet->setCellValue("R".$i,$d->jumlah_hp_i);
			    $sheet->setCellValue("S".$i,$d->jumlah_hp_ii);
			    $sheet->setCellValue("T".$i,$d->jumlah_hp_iii);
			    $sheet->setCellValue("U".$i,$d->jumlah_hp_khusus);
			}

		    $thin = array ();
		    $thin['borders']=array();
		    $thin['borders']['allborders']=array();
		    $thin['borders']['allborders']['style']=PHPExcel_Style_Border::BORDER_THIN ;
		    $sheet->getStyle ( "A".$border_start.":U".$i )->applyFromArray ($thin);

		    foreach (range('A', 'U') as $columnID) {
		        $sheet->getColumnDimension($columnID)->setAutoSize(true);
		    }

		    header ( 'Content-Type: application/vnd.ms-excel' );
		    header ( 'Content-Disposition: attachment;filename="RL 3.1 - Kegiatan Pelayanan Rawat Inap.xls"' );
		    header ( 'Cache-Control: max-age=0' );
		    $writer = PHPExcel_IOFactory::createWriter ( $file, 'Excel5' );
		    $writer->save ( 'php://output' );
		}
		return;
	}
	$modal = $uitable->getModal();
	$form = $modal->getForm();
	$form->setTitle("RL 3.1 - Kegiatan Pelayanan Rawat Inap");

	$bulan_option = new OptionBuilder();
	$bulan_option->add("Semua", "%%", "1");
	$bulan_option->add("Januari", "1");
	$bulan_option->add("Februari", "2");
	$bulan_option->add("Maret", "3");
	$bulan_option->add("April", "4");
	$bulan_option->add("Mei", "5");
	$bulan_option->add("Juni", "6");
	$bulan_option->add("Juli", "7");
	$bulan_option->add("Agustus", "8");
	$bulan_option->add("September", "9");
	$bulan_option->add("Oktober", "10");
	$bulan_option->add("Nopember", "11");
	$bulan_option->add("Desember", "12");
	$bulan_select = new Select("rl31_bulan_filter", "rl31_bulan_filter", $bulan_option->getContent());
	$form->addElement("Bulan", $bulan_select);
	$tahun_rows = $db->get_result("
		SELECT DISTINCT YEAR(tanggal) tahun
		FROM smis_rg_layananpasien
		WHERE prop = '' AND YEAR(tanggal) > 0
		ORDER BY YEAR(tanggal) ASC
	");
	$tahun_option = new OptionBuilder();
	if ($tahun_rows != null) {
		foreach ($tahun_rows as $tahun_row) {
			if (date("Y") == $tahun_row->tahun)
				$tahun_option->add($tahun_row->tahun, $tahun_row->tahun, "1");
			else
				$tahun_option->add($tahun_row->tahun, $tahun_row->tahun);
		}
	}
	$tahun_select = new Select("rl31_tahun_filter", "rl31_tahun_filter", $tahun_option->getContent());
	$form->addElement("Tahun", $tahun_select);
	$excel_button = new Button("", "", "");
	$excel_button 	->addClass("btn-primary")
					->setIsButton(Button::$ICONIC)
					->setIcon("fa fa-file-excel-o")
					->setAction("rl31.excel()");
	$process_button = new Button("", "", "");
	$process_button	->addClass("btn-inverse")
					-> setIsButton(Button::$ICONIC)
					->setIcon("fa fa-circle-o")
					->setAction("rl31.view()");
	$button_group = new ButtonGroup("noprint");
	$button_group->addButton($process_button);
	$button_group->addButton($excel_button);
	$form->addElement("", $button_group);

	echo $form->getHtml();
	echo $uitable->getHtml();
	echo addJS("framework/smis/js/table_action.js");
	echo addJS("medical_record/resource/js/rl31.js", false);
?>