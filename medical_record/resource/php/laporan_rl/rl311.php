<?php
show_error();
global $db;
require_once 'smis-base/smis-include-service-consumer.php';
require_once 'smis-libs-class/MasterTemplate.php';
require_once 'smis-libs-class/MasterSlaveTemplate.php';
require_once 'smis-libs-class/ServiceProviderList.php';
require_once 'medical_record/class/adapter/LapRL311Adapter.php';
require_once 'medical_record/class/responder/RL311Responder.php';

$periksa=new MasterSlaveTemplate($db, "smis_mr_jiwa", "medical_record", "rl311");
$periksa->setDateEnable(true);
$dbtable=$periksa->getDBtable();
$qv = "
	SELECT 
		smis_mr_data_dasar_rs.koders,
		smis_mr_data_dasar_rs.nama_rs,
		smis_mr_data_dasar_rs.kode_provinsi,
		smis_mr_data_dasar_rs.kota_kabupaten,
		YEAR(smis_mr_jiwa.tanggal) tahun,
		SUM(smis_mr_jiwa.psikotes) psikotes,
		SUM(smis_mr_jiwa.konsultasi) konsultasi,
		SUM(smis_mr_jiwa.medikamentosa) medikamentosa,
		SUM(smis_mr_jiwa.elektro_medik) elektro_medik,
		SUM(smis_mr_jiwa.psikoterapi) psikoterapi,
		SUM(smis_mr_jiwa.play_therapy) play_therapy,
		SUM(smis_mr_jiwa.rehab_medik_psikiatrik) rehab_medik_psikiatrik
	FROM 
		smis_mr_jiwa, (
			SELECT 
				*
			FROM 
				smis_mr_data_dasar_rs
			WHERE 
				prop = ''
			LIMIT 
				0, 1
		) smis_mr_data_dasar_rs
";
$qc="SELECT 1 as total FROM smis_mr_jiwa";
$dbtable->setPreferredQuery(true, $qv, $qc);
$dbtable->setUseWhereForView(true);
$dbtable->setShowAll(true);

$header=array ("No.", "Kode Propinsi", "Kab/Kota", "Kode RS", "Nama RS", "Tahun","Layanan",'Total');
$btn=$periksa->addFlag("bulan", "Pilih Bulan", "Silakan Pilih Bulan")
			  ->addFlag("tahun", "Pilih Tahun", "Silakan Pilih Tahun")
			  ->addNoClear("bulan")
			  ->addNoClear("tahun")
			  ->getUItable()
			  ->setHeader($header)
			  ->setAddButtonEnable(false)
			  ->setPrintButtonEnable(false)
			  ->getHeaderButton();

$excel=new Button("", "", "");
$excel->setAction("rl311.excel()");
$excel->setClass("btn-primary");
$excel->setIsButton(Button::$ICONIC);
$excel->setIcon("fa fa-file-excel-o");

$bulan_option = new OptionBuilder();
$bulan_option->add("Semua", "%%", "1");
$bulan_option->add("Januari", "1");
$bulan_option->add("Februari", "2");
$bulan_option->add("Maret", "3");
$bulan_option->add("April", "4");
$bulan_option->add("Mei", "5");
$bulan_option->add("Juni", "6");
$bulan_option->add("Juli", "7");
$bulan_option->add("Agustus", "8");
$bulan_option->add("September", "9");
$bulan_option->add("Oktober", "10");
$bulan_option->add("Nopember", "11");
$bulan_option->add("Desember", "12");
$tahun_rows = $db->get_result("
	SELECT DISTINCT YEAR(tanggal) tahun
	FROM smis_mr_jiwa
	WHERE prop = ''
	ORDER BY YEAR(tanggal) ASC
");
$tahun_option = new OptionBuilder();
if ($tahun_rows != null) {
	foreach ($tahun_rows as $tahun_row) {
		if (date("Y") == $tahun_row->tahun)
			$tahun_option->add($tahun_row->tahun, $tahun_row->tahun, "1");
		else
			$tahun_option->add($tahun_row->tahun, $tahun_row->tahun);
	}
}

$periksa->getUItable()
		 ->setAction(false)
		 ->setFooterVisible(false)
		 ->addModal("bulan", "select", "Bulan", $bulan_option->getContent())
		 ->addModal("tahun", "select", "Tahun", $tahun_option->getContent());
$periksa->getForm(true,"")
		->addElement("", $btn)
        ->addElement("", $excel);
        
$gm=new LapRL311Adapter();
$periksa->setAdapter($gm);
$periksa->addViewData("bulan", "bulan")
		->addViewData("tahun", "tahun");
        
$periksa->setDBresponder(new RL311Responder($dbtable, $periksa->getUItable(), $periksa->getAdapter()));

if($periksa->getDBResponder()->isView()){
	$dbtable->addCustomKriteria(NULL, " MONTH(tanggal) LIKE '".$_POST['bulan']."' ");
	$dbtable->addCustomKriteria(NULL, " YEAR(tanggal) = '".$_POST['tahun']."' ");
	$dbtable->setFetchMethode(DBTable::$ARRAY_FETCH);
}
$periksa->initialize();
?>