<?php
    require_once("smis-base/smis-include-service-consumer.php");
    global $db;

    $head = array('Kode Propinsi', 'Kab/Kota', 'Kode RS', 'Nama RS', 'Bulan', 'Tahun', 'No. Urut', 'Kode ICD 10', 'Deskripsi', 'Kasus Baru Menurut Jenis Kelamin LK', 'Kasus Baru Menurut Jenis Kelamin PR', 'Jumlah Kasus Baru', 'Jumlah Kunjungan');
    $uitable = new Table($head, "", null, true);
    $uitable->setName("rl5_4");
    $uitable->setAction(false);
    $uitable->setFooterVisible(false);

    if (isset($_POST ['command'])) {
        $adapter = new SimpleAdapter();
        $adapter->setUseNumber(true, "No. Urut", "back.");
        $adapter->add("Kode Propinsi", "kode_provinsi");
        $adapter->add("Kab/Kota", "kota_kabupaten");
        $adapter->add("Kode RS", "koders");
        $adapter->add("Nama RS", "nama_rs");
        $adapter->add("Bulan", "bulan", "month-id");
        $adapter->add("Tahun", "tahun");
        $adapter->add("Kode ICD 10", "kode_icd");
        $adapter->add("Deskripsi", "nama_icd");
        $adapter->add("Kasus Baru Menurut Jenis Kelamin LK", "jml_kasus_baru_lk", "number");
        $adapter->add("Kasus Baru Menurut Jenis Kelamin PR", "jml_kasus_baru_pr", "number");
        $adapter->add("Jumlah Kasus Baru", "jml_kasus_baru", "number");
        $adapter->add("Jumlah Kunjungan", "jml_kunjungan", "number");

        $dbtable = new DBTable($db, "smis_mr_data_dasar_rs");
        $dbtable->setShowAll(true);
        $query_value = "
            SELECT w.kode_provinsi, w.kota_kabupaten, w.koders, w.nama_rs, v.*
            FROM 
            (
                SELECT bulan, tahun, kode_icd, nama_icd, COUNT(noreg_pasien) jml_kunjungan, SUM(kasus_baru_pr) jml_kasus_baru_pr, SUM(kasus_baru_lk) jml_kasus_baru_lk, SUM(kasus_baru_pr) + SUM(kasus_baru_lk) jml_kasus_baru 
                FROM 
                (
                    SELECT b.noreg_pasien, MONTH(c.tanggal) bulan, YEAR(c.tanggal) tahun, b.kode_icd, b.nama_icd, IF(b.jk = 1 AND b.kasus = 'Baru', 1, 0) kasus_baru_pr, IF(b.jk = 0 AND b.kasus = 'Baru', 1, 0) kasus_baru_lk
                    FROM
                    ( 
                        (
                            SELECT noreg_pasien, MAX(id) id_terakhir
                            FROM smis_mr_diagnosa
                            WHERE prop = ''
                            GROUP BY noreg_pasien
                        ) a INNER JOIN smis_mr_diagnosa b ON a.id_terakhir = b.id
                    ) INNER JOIN smis_rg_layananpasien c ON b.noreg_pasien = c.id
                    WHERE c.uri = 0
                ) c
                WHERE bulan LIKE '" . $_POST['bulan'] . "' AND tahun = '" . $_POST['tahun'] . "'
                GROUP BY bulan, tahun, kode_icd, nama_icd
                ORDER BY jml_kunjungan DESC
                LIMIT 0, 10
            ) v, smis_mr_data_dasar_rs w
        ";
        $query_count = "
            SELECT COUNT(*)
            FROM (
                " . $query_value . "
            ) v
        ";
        $dbtable->setPreferredQuery(true, $query_value, $query_count);

        $dbresponder = new DBResponder($dbtable, $uitable, $adapter);
        $data = $dbresponder->command($_POST['command']);
        echo json_encode($data);
        return;
    }
    $modal = $uitable->getModal();
    $form = $modal->getForm();
    $form->setTitle("RL 5.4 - 10 Besar Penyakit Rawat Jalan");

    $tahun_rows = $db->get_result("
        SELECT DISTINCT YEAR(tanggal) tahun
        FROM smis_rg_layananpasien
        WHERE prop = '' AND carapulang NOT LIKE 'TIDAK DATANG' AND YEAR(tanggal) > 0
        ORDER BY YEAR(tanggal) ASC
    ");
    $tahun_option = new OptionBuilder();
    if ($tahun_rows != null) {
        foreach ($tahun_rows as $tahun_row) {
            if (date("Y") == $tahun_row->tahun)
                $tahun_option->add($tahun_row->tahun, $tahun_row->tahun, "1");
            else
                $tahun_option->add($tahun_row->tahun, $tahun_row->tahun);
        }
    }
    $tahun_select = new Select("rl5_4_tahun", "rl5_4_tahun", $tahun_option->getContent());
    $form->addElement("Tahun", $tahun_select);
    $bulan_option = new OptionBuilder();
    $bulan_option->add("Semua", "%%");
    for ($i = 1; $i <= 12; $i++) {
        if (date("m") == $i)
            $bulan_option->add(ArrayAdapter::format("month-id", $i), $i, "1");
        else
            $bulan_option->add(ArrayAdapter::format("month-id", $i), $i);
    }
    $bulan_select = new Select("rl5_4_bulan", "rl5_4_bulan", $bulan_option->getContent());
    $form->addElement("Bulan", $bulan_select);

    $excel_button = new Button("", "", "");
    $excel_button   ->addClass("btn-primary")
                    ->setIsButton(Button::$ICONIC)
                    ->setIcon("fa fa-file-excel-o")
                    ->setAction("rl5_4.excel()");
    $process_button = new Button("", "", "");
    $process_button ->addClass("btn-inverse")
                    -> setIsButton(Button::$ICONIC)
                    ->setIcon("fa fa-circle-o")
                    ->setAction("rl5_4.view()");
    $button_group = new ButtonGroup("noprint");
    $button_group->addButton($process_button);
    $button_group->addButton($excel_button);
    $form->addElement("", $button_group);

    echo "<div class='alert alert-block alert-inverse noprint'>" .
            "<h4>Informasi</h4>" .
            "10 besar penyakit didapatkan dari input diagnosa pada ruangan terakhir rawat jalan dan kasus baru diambil dari input kasus pada diagnosa ruang rawat jalan" .
          "</div>";
    echo $form->getHtml();
    echo $uitable->getHtml();
    echo $modal->getHtml();
    echo addJS("framework/smis/js/table_action.js");
    echo addJS("medical_record/resource/js/rl5_4.js", false);
?>