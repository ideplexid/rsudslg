<?php
	require_once("smis-base/smis-include-service-consumer.php");
	global $db;

	$head = array('Kode Propinsi', 'Kab/Kota', 'Kode RS', 'Nama RS', 'Tahun', 'No.', 'Jenis Tindakan', 'Jumlah');
	$uitable = new Table($head, "", null, true);
	$uitable->setName("rl312");
	$uitable->setAction(false);
	$uitable->setFooterVisible(false);
	$uitable->setHeaderVisible(false);
	$uitable->addHeader("before", "
		<tr>
			<th rowspan='3'>Kode Propinsi</th>
			<th rowspan='3'>Kab/Kota</th>
			<th rowspan='3'>Kode RS</th>
			<th rowspan='3'>Nama RS</th>
			<th rowspan='3'>Tahun</th>
			<th rowspan='2'>No.</th>
			<th rowspan='2'>Metoda</th>
			<th colspan='2'>Konseling</th>
			<th colspan='4'>KB Baru dengan Cara Masuk</th>
			<th colspan='3'>KB Baru dengan Kondisi</th>
			<th rowspan='2'>Kunjungan Ulang</th>
			<th colspan='2'>Keluhan Efek Samping</th>
		</tr>
		<tr>
			<th>ANC</th>
			<th>Pasca Persalinan</th>
			<th>Bukan Rujukan</th>
			<th>Rujukan Rawat Inap</th>
			<th>Rujukan Rawat Jalan</th>
			<th>Total</th>
			<th>Pasca Persalinan/Nifas</th>
			<th>Abortus</th>
			<th>Lainnya</th>
			<th>Jumlah</th>
			<th>Dirujuk</th>
		</tr>
		<tr>
			<th>1</th>
			<th>2</th>
			<th>3</th>
			<th>4</th>
			<th>5</th>
			<th>6</th>
			<th>7</th>
			<th>8</th>
			<th>9</th>
			<th>10</th>
			<th>11</th>
			<th>12</th>
			<th>13</th>
			<th>14</th>
		</tr>
	");

	if (isset($_POST ['command'])) {
		if ($_POST['command'] == "view") {
			$tahun = $_POST['tahun'];
			$bulan = $_POST['bulan'];
			$html = "";

			$kode_rs = "";
			$nama_rs = "";
			$kode_propinsi = "";
			$kabupaten_kota = "";
			$info_rs_row = $db->get_row("
				SELECT 
					*
				FROM 
					smis_mr_data_dasar_rs
				WHERE 
					prop = ''
				LIMIT 
					0, 1
			");
			if ($info_rs_row != null) {
				$kode_rs = $info_rs_row->koders;
				$nama_rs = $info_rs_row->nama_rs;
				$kode_propinsi = $info_rs_row->kode_provinsi;
				$kabupaten_kota = $info_rs_row->kota_kabupaten;
			}

			$metode_arr = array(
				"I U D",
				"PIL KB",
				"Kondom",
				"Obat Vaginal",
				"MO Pria",
				"MO Wanita",
				"Suntikan",
				"Implant"
			);

			$nomor = 1;
			foreach ($metode_arr as $metode) {
				$konseling_anc = 0;
				$row = $db->get_row("
					SELECT COUNT(*) jumlah
					FROM smis_mr_keluarga_berencana
					WHERE prop = '' AND YEAR(tanggal) = '" . $tahun . "' AND MONTH(tanggal) LIKE '" . $bulan . "' AND metode = '" . $metode . "' AND konseling LIKE 'ANC'
				");
				if ($row != null)
					$konseling_anc = $row->jumlah;

				$konseling_pasca_persalinan = 0;
				$row = $db->get_row("
					SELECT COUNT(*) jumlah
					FROM smis_mr_keluarga_berencana
					WHERE prop = '' AND YEAR(tanggal) = '" . $tahun . "' AND MONTH(tanggal) LIKE '" . $bulan . "' AND metode = '" . $metode . "' AND konseling LIKE 'Pasca Persalinan'
				");
				if ($row != null)
					$konseling_pasca_persalinan = $row->jumlah;

				$kb_baru_cara_masuk_bukan_rujukan = 0;
				$row = $db->get_row("
					SELECT COUNT(*) jumlah
					FROM smis_mr_keluarga_berencana
					WHERE prop = '' AND YEAR(tanggal) = '" . $tahun . "' AND MONTH(tanggal) LIKE '" . $bulan . "' AND metode = '" . $metode . "' AND kb_baru_cara_masuk LIKE 'Bukan Rujukan'
				");
				if ($row != null)
					$kb_baru_cara_masuk_bukan_rujukan = $row->jumlah;
				$kb_baru_cara_masuk_rujukan_ri = 0;
				$row = $db->get_row("
					SELECT COUNT(*) jumlah
					FROM smis_mr_keluarga_berencana
					WHERE prop = '' AND YEAR(tanggal) = '" . $tahun . "' AND MONTH(tanggal) LIKE '" . $bulan . "' AND metode = '" . $metode . "' AND kb_baru_cara_masuk LIKE 'Rujukan Rawat Inap'
				");
				if ($row != null)
					$kb_baru_cara_masuk_rujukan_ri = $row->jumlah;
				$kb_baru_cara_masuk_rujukan_rj = 0;
				$row = $db->get_row("
					SELECT COUNT(*) jumlah
					FROM smis_mr_keluarga_berencana
					WHERE prop = '' AND YEAR(tanggal) = '" . $tahun . "' AND MONTH(tanggal) LIKE '" . $bulan . "' AND metode = '" . $metode . "' AND kb_baru_cara_masuk LIKE 'Rujukan Rawat Jalan'
				");
				if ($row != null)
					$kb_baru_cara_masuk_rujukan_rj = $row->jumlah;
				$kb_baru_cara_masuk_total = $kb_baru_cara_masuk_bukan_rujukan + $kb_baru_cara_masuk_rujukan_ri + $kb_baru_cara_masuk_rujukan_rj;

				$kb_baru_kondisi_pasca_persalinan = 0;
				$row = $db->get_row("
					SELECT COUNT(*) jumlah
					FROM smis_mr_keluarga_berencana
					WHERE prop = '' AND YEAR(tanggal) = '" . $tahun . "' AND MONTH(tanggal) LIKE '" . $bulan . "' AND metode = '" . $metode . "' AND kb_baru_kondisi LIKE 'Pasca Persalinan/Nifas'
				");
				if ($row != null)
					$kb_baru_kondisi_pasca_persalinan = $row->jumlah;
				$kb_baru_kondisi_abortus = 0;
				$row = $db->get_row("
					SELECT COUNT(*) jumlah
					FROM smis_mr_keluarga_berencana
					WHERE prop = '' AND YEAR(tanggal) = '" . $tahun . "' AND MONTH(tanggal) LIKE '" . $bulan . "' AND metode = '" . $metode . "' AND kb_baru_kondisi LIKE 'Abortus'
				");
				if ($row != null)
					$kb_baru_kondisi_abortus = $row->jumlah;
				$kb_baru_kondisi_lain = 0;
				$row = $db->get_row("
					SELECT COUNT(*) jumlah
					FROM smis_mr_keluarga_berencana
					WHERE prop = '' AND YEAR(tanggal) = '" . $tahun . "' AND MONTH(tanggal) LIKE '" . $bulan . "' AND metode = '" . $metode . "' AND kb_baru_kondisi LIKE 'Lainnya'
				");
				if ($row != null)
					$kb_baru_kondisi_lain = $row->jumlah;

				$kunjungan_ulang = 0;
				$row = $db->get_row("
					SELECT COUNT(*) jumlah
					FROM smis_mr_keluarga_berencana
					WHERE prop = '' AND YEAR(tanggal) = '" . $tahun . "' AND MONTH(tanggal) LIKE '" . $bulan . "' AND metode = '" . $metode . "' AND kunjungan_ulang = '1'
				");
				if ($row != null)
					$kunjungan_ulang = $row->jumlah;

				$keluhan_efek_samping = 0;
				$row = $db->get_row("
					SELECT COUNT(*) jumlah
					FROM smis_mr_keluarga_berencana
					WHERE prop = '' AND YEAR(tanggal) = '" . $tahun . "' AND MONTH(tanggal) LIKE '" . $bulan . "' AND metode = '" . $metode . "' AND keluhan_efek_samping = '1'
				");
				if ($row != null)
					$keluhan_efek_samping = $row->jumlah;

				$keluhan_efek_samping_dirujuk = 0;
				$row = $db->get_row("
					SELECT COUNT(*) jumlah
					FROM smis_mr_keluarga_berencana
					WHERE prop = '' AND YEAR(tanggal) = '" . $tahun . "' AND MONTH(tanggal) LIKE '" . $bulan . "' AND metode = '" . $metode . "' AND keluhan_efek_samping_dirujuk = '1'
				");
				if ($row != null)
					$keluhan_efek_samping_dirujuk = $row->jumlah;

				$html .= 	"<tr class='data_rl312'>" .
								"<td id='rl312_kode_propinsi'>" . $kode_propinsi . "</td>" .
								"<td id='rl312_kabupaten_kota'>" . $kabupaten_kota . "</td>" .
								"<td id='rl312_kode_rs'>" . $kode_rs . "</td>" .
								"<td id='rl312_nama_rs'>" . $nama_rs . "</td>" .
								"<td id='rl312_tahun'>" . $tahun . "</td>" .
								"<td id='rl312_nomor'>" . ($nomor++) . "</td>" .
								"<td id='rl312_metode'>" . $metode . "</td>" .
								"<td id='rl312_f_konseling_anc'><div align='right'>" . $konseling_anc . "</div></td>" .
								"<td id='rl312_konseling_anc' style='display:none;'>" . $konseling_anc . "</td>" .
								"<td id='rl312_f_konseling_pasca_persalinan'><div align='right'>" . $konseling_pasca_persalinan . "</div></td>" .
								"<td id='rl312_konseling_pasca_persalinan' style='display:none;'>" . $konseling_pasca_persalinan . "</td>" .
								"<td id='rl312_f_kb_baru_cara_masuk_bukan_rujukan'><div align='right'>" . $kb_baru_cara_masuk_bukan_rujukan . "</div></td>" .
								"<td id='rl312_kb_baru_cara_masuk_bukan_rujukan' style='display:none;'>" . $kb_baru_cara_masuk_bukan_rujukan . "</td>" .
								"<td id='rl312_f_kb_baru_cara_masuk_rujukan_ri'><div align='right'>" . $kb_baru_cara_masuk_rujukan_ri . "</div></td>" .
								"<td id='rl312_kb_baru_cara_masuk_rujukan_ri' style='display:none;'>" . $kb_baru_cara_masuk_rujukan_ri . "</td>" .
								"<td id='rl312_f_kb_baru_cara_masuk_rujukan_rj'><div align='right'>" . $kb_baru_cara_masuk_rujukan_rj . "</div></td>" .
								"<td id='rl312_kb_baru_cara_masuk_rujukan_rj' style='display:none;'>" . $kb_baru_cara_masuk_rujukan_rj . "</td>" .
								"<td id='rl312_f_kb_baru_cara_masuk_total'><div align='right'>" . $kb_baru_cara_masuk_total . "</div></td>" .
								"<td id='rl312_kb_baru_cara_masuk_total' style='display:none;'>" . $kb_baru_cara_masuk_total . "</td>" .
								"<td id='rl312_f_kb_baru_kondisi_pasca_persalinan'><div align='right'>" . $kb_baru_kondisi_pasca_persalinan . "</div></td>" .
								"<td id='rl312_kb_baru_kondisi_pasca_persalinan' style='display:none;'>" . $kb_baru_kondisi_pasca_persalinan . "</td>" .
								"<td id='rl312_f_kb_baru_kondisi_abortus'><div align='right'>" . $kb_baru_kondisi_abortus . "</div></td>" .
								"<td id='rl312_kb_baru_kondisi_abortus' style='display:none;'>" . $kb_baru_kondisi_abortus . "</td>" .
								"<td id='rl312_f_kb_baru_kondisi_lain'><div align='right'>" . $kb_baru_kondisi_lain . "</div></td>" .
								"<td id='rl312_kb_baru_kondisi_lain' style='display:none;'>" . $kb_baru_kondisi_lain . "</td>" .
								"<td id='rl312_f_kunjungan_ulang'><div align='right'>" . $kunjungan_ulang . "</div></td>" .
								"<td id='rl312_kunjungan_ulang' style='display:none;'>" . $kunjungan_ulang . "</td>" .
								"<td id='rl312_f_keluhan_efek_samping'><div align='right'>" . $keluhan_efek_samping . "</div></td>" .
								"<td id='rl312_keluhan_efek_samping' style='display:none;'>" . $keluhan_efek_samping . "</td>" .
								"<td id='rl312_f_keluhan_efek_samping_dirujuk'><div align='right'>" . $keluhan_efek_samping_dirujuk . "</div></td>" .
								"<td id='rl312_keluhan_efek_samping_dirujuk' style='display:none;'>" . $keluhan_efek_samping_dirujuk . "</td>" .
							"</tr>";
			}

			$data['html'] = $html;
			echo json_encode($data);
		} else if ($_POST['command'] == "export_xls") {
			require_once ("smis-libs-out/php-excel/PHPExcel.php");

		    $file = new PHPExcel ();
		    $file->getProperties ()->setCreator ( "PT. Inovasi Ide Utama" );
		    $file->getProperties ()->setTitle ( "RL 3.12 - Keluarga Berencana" );
		    $file->getProperties ()->setSubject ( "RL 3.12 - Keluarga Berencana" );
		    $file->getProperties ()->setDescription ( "RL 3.12 - Keluarga Berencana Generated by System" );
		    $file->getProperties ()->setKeywords ( "RL 3.12 - Keluarga Berencana" );
		    $file->getProperties ()->setCategory ( "RL 3.12 - Keluarga Berencana" );

		    $sheet = $file->getActiveSheet ();
		    $i = 1;

		    $sheet->mergeCells("A".$i.":S".$i)->setCellValue("A".$i,"RL 3.12 - Keluarga Berencana");
		    $sheet->getStyle("A".$i.":S".$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		    $sheet->getStyle("A".$i)->getFont()->setBold(true);
		    $i = $i + 2;

		    $border_start = $i;
		    
		    $sheet->mergeCells("A".$i.":A".($i + 2))->setCellValue("A".$i,"KODE PROPINSI");
		    $sheet->mergeCells("B".$i.":B".($i + 2))->setCellValue("B".$i,"KAB/KOTA");
		    $sheet->mergeCells("C".$i.":C".($i + 2))->setCellValue("C".$i,"KODE RS");
		    $sheet->mergeCells("D".$i.":D".($i + 2))->setCellValue("D".$i,"NAMA RS");
		    $sheet->mergeCells("E".$i.":E".($i + 2))->setCellValue("E".$i,"TAHUN");
		    $sheet->mergeCells("F".$i.":F".($i + 1))->setCellValue("F".$i,"NO.");
		    $sheet->setCellValue("F".($i + 2),"1");
		    $sheet->mergeCells("G".$i.":G".($i + 1))->setCellValue("G".$i,"METODA");
		    $sheet->setCellValue("G".($i + 2),"2");
		    $sheet->mergeCells("H".$i.":I".$i)->setCellValue("H".$i,"KONSELING");
		    $sheet->setCellValue("H".($i + 1),"ANC");
		    $sheet->setCellValue("H".($i + 2),"3");
		    $sheet->setCellValue("I".($i + 1),"PASCA PERSALINAN");
		    $sheet->setCellValue("I".($i + 2),"4");
		    $sheet->mergeCells("J".$i.":M".$i)->setCellValue("J".$i,"KB BARU DENGAN CARA MASUK");
		    $sheet->setCellValue("J".($i + 1),"BUKAN RUJUKAN");
		    $sheet->setCellValue("J".($i + 2),"5");
		    $sheet->setCellValue("K".($i + 1),"RUJUKAN RAWAT INAP");
		    $sheet->setCellValue("K".($i + 2),"6");
		    $sheet->setCellValue("L".($i + 1),"RUJUKAN RAWAT JALAN");
		    $sheet->setCellValue("L".($i + 2),"7");
		    $sheet->setCellValue("M".($i + 1),"TOTAL");
		    $sheet->setCellValue("M".($i + 2),"8");
		    $sheet->mergeCells("N".$i.":P".$i)->setCellValue("N".$i,"KB BARU DENGAN KONDISI");
		    $sheet->setCellValue("N".($i + 1),"PASCA PERSALINAN/NIFAS");
		    $sheet->setCellValue("N".($i + 2),"9");
		    $sheet->setCellValue("O".($i + 1),"ABORTUS");
		    $sheet->setCellValue("O".($i + 2),"10");
		    $sheet->setCellValue("P".($i + 1),"LAINNYA");
		    $sheet->setCellValue("P".($i + 2),"11");
		    $sheet->mergeCells("Q".$i.":Q".($i + 1))->setCellValue("Q".$i,"KUNJUGNAN ULANG");
		    $sheet->setCellValue("Q".($i + 2),"12");
		    $sheet->mergeCells("R".$i.":S".$i)->setCellValue("R".$i,"KELUHAN EFEK SAMPING");
		    $sheet->setCellValue("R".($i + 1),"JUMLAH");
		    $sheet->setCellValue("R".($i + 2),"13");
		    $sheet->setCellValue("S".($i + 1),"DIRUJUK");
		    $sheet->setCellValue("S".($i + 2),"14");
		    $sheet->getStyle("A".$i.":S".($i + 2))->getFont()->setBold(true);
		    $sheet->getStyle("A".$i.":S".($i + 2))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		    $sheet->getStyle("A".$i.":S".($i + 2))->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
		    $i = $i + 2;

		    $data = json_decode($_POST['detail']);
		    foreach ($data as $d) {
		    	$i = $i + 1;
			    $sheet->setCellValue("A".$i,$d->kode_propinsi);
			    $sheet->setCellValue("B".$i,$d->kabupaten_kota);
			    $sheet->setCellValue("C".$i,$d->kode_rs);
			    $sheet->setCellValue("D".$i,$d->nama_rs);
			    $sheet->setCellValue("E".$i,$d->tahun);
			    $sheet->setCellValue("F".$i,$d->nomor);
				$sheet->setCellValue("G".$i,$d->metode);
				$sheet->setCellValue("H".$i,$d->konseling_anc);
				$sheet->setCellValue("I".$i,$d->konseling_pasca_persalinan);
				$sheet->setCellValue("J".$i,$d->kb_baru_cara_masuk_bukan_rujukan);
				$sheet->setCellValue("K".$i,$d->kb_baru_cara_masuk_rujukan_ri);
				$sheet->setCellValue("L".$i,$d->kb_baru_cara_masuk_rujukan_rj);
				$sheet->setCellValue("M".$i,$d->kb_baru_cara_masuk_total);
				$sheet->setCellValue("N".$i,$d->kb_baru_kondisi_pasca_persalinan);
				$sheet->setCellValue("O".$i,$d->kb_baru_kondisi_abortus);
				$sheet->setCellValue("P".$i,$d->kb_baru_kondisi_lain);
				$sheet->setCellValue("Q".$i,$d->kunjungan_ulang);
				$sheet->setCellValue("R".$i,$d->keluhan_efek_samping);
				$sheet->setCellValue("S".$i,$d->keluhan_efek_samping_dirujuk);
			}

		    $thin = array ();
		    $thin['borders']=array();
		    $thin['borders']['allborders']=array();
		    $thin['borders']['allborders']['style']=PHPExcel_Style_Border::BORDER_THIN ;
		    $sheet->getStyle ( "A".$border_start.":S".$i )->applyFromArray ($thin);

		    foreach (range('A', 'S') as $columnID) {
		        $sheet->getColumnDimension($columnID)->setAutoSize(true);
		    }

		    header ( 'Content-Type: application/vnd.ms-excel' );
		    header ( 'Content-Disposition: attachment;filename="RL 3.12 - Keluarga Berencana.xls"' );
		    header ( 'Cache-Control: max-age=0' );
		    $writer = PHPExcel_IOFactory::createWriter ( $file, 'Excel5' );
		    $writer->save ( 'php://output' );
		}

		return;
	}
	$modal = $uitable->getModal();
	$form = $modal->getForm();
	$form->setTitle("RL 3.12 - Keluarga Berencana");

	$bulan_option = new OptionBuilder();
	$bulan_option->add("Semua", "%%", "1");
	$bulan_option->add("Januari", "1");
	$bulan_option->add("Februari", "2");
	$bulan_option->add("Maret", "3");
	$bulan_option->add("April", "4");
	$bulan_option->add("Mei", "5");
	$bulan_option->add("Juni", "6");
	$bulan_option->add("Juli", "7");
	$bulan_option->add("Agustus", "8");
	$bulan_option->add("September", "9");
	$bulan_option->add("Oktober", "10");
	$bulan_option->add("Nopember", "11");
	$bulan_option->add("Desember", "12");
	$bulan_select = new Select("rl312_bulan_filter", "rl312_bulan_filter", $bulan_option->getContent());
	$form->addElement("Bulan", $bulan_select);
	$tahun_rows = $db->get_result("
		SELECT DISTINCT YEAR(tanggal) tahun
		FROM smis_mr_keluarga_berencana
		WHERE prop = ''
		ORDER BY YEAR(tanggal) ASC
	");
	$tahun_option = new OptionBuilder();
	if ($tahun_rows != null) {
		foreach ($tahun_rows as $tahun_row) {
			if (date("Y") == $tahun_row->tahun)
				$tahun_option->add($tahun_row->tahun, $tahun_row->tahun, "1");
			else
				$tahun_option->add($tahun_row->tahun, $tahun_row->tahun);
		}
	}
	$tahun_select = new Select("rl312_tahun_filter", "rl312_tahun_filter", $tahun_option->getContent());
	$form->addElement("Tahun", $tahun_select);
	$excel_button = new Button("", "", "");
	$excel_button 	->addClass("btn-primary")
					->setIsButton(Button::$ICONIC)
					->setIcon("fa fa-file-excel-o")
					->setAction("rl312.excel()");
	$process_button = new Button("", "", "");
	$process_button	->addClass("btn-inverse")
					-> setIsButton(Button::$ICONIC)
					->setIcon("fa fa-circle-o")
					->setAction("rl312.view()");
	$button_group = new ButtonGroup("noprint");
	$button_group->addButton($process_button);
	$button_group->addButton($excel_button);
	$form->addElement("", $button_group);

	echo $form->getHtml();
	echo $uitable->getHtml();
	echo addJS("framework/smis/js/table_action.js");
	echo addJS("medical_record/resource/js/rl312.js", false);
?>