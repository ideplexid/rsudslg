<?php 
require_once 'smis-libs-class/MasterSlaveTemplate.php';
require_once 'medical_record/class/responder/RL32Responder.php';
global $db;

$button=new Button("", "", "");
$button->setAction("rl32.view()");
$button->setClass("btn-primary");
$button->setIsButton(Button::$ICONIC);
$button->setIcon("fa fa-refresh");

$cetak=new Button("", "", "");
$cetak->setAction("rl32.print()");
$cetak->setClass("btn-primary");
$cetak->setIsButton(Button::$ICONIC);
$cetak->setIcon("fa fa-print");

$excel=new Button("", "", "");
$excel->setAction("rl32.excel()");
$excel->setClass("btn-primary");
$excel->setIsButton(Button::$ICONIC);
$excel->setIcon("fa fa-file-excel-o");

$rl32=new MasterSlaveTemplate($db, "smis_mr_igd", "medical_record", "rl32");
if(isset($_POST['bulan']) && $_POST['bulan']!="" && isset($_POST['tahun']) && $_POST['tahun']!=""){
	$qv="SELECT pola_kasus,
	sum(if(ds=1,1,0)) as non_rujukan,
	sum(if(ds=0,1,0)) as rujukan,
	sum(if(kelanjutan='MRS',1,0)) as dirawat,
	sum(if(kelanjutan='RJK',1,0)) as dirujuk,
	sum(if(kelanjutan='RJ' AND triage NOT LIKE 'Hitam%',1,0)) as rawat_jalan,
	sum(if(triage='Hitam Mati',1,0)) as mati,
	sum(if(triage='Hitam DOA',1,0)) as doa
	FROM smis_mr_igd
	 ";
	$qc="SELECT pola_kasus FROM smis_mr_igd ";
	
	$rl32	->getDBtable()
			->setShowAll(true)
			->setPreferredQuery(true, $qv, $qc)
            ->setGroupBy(true, " pola_kasus ")
            ->setUseWhereforView(true)
			->addCustomKriteria(NULL, "MONTH(tanggal) LIKE '".$_POST['bulan']."'")
			->addCustomKriteria(NULL, "YEAR(tanggal) = '".$_POST['tahun']."'");
}

$uitable=$rl32->getUItable();
$header=array("No.","Jenis","Rujukan","Non-Rujukan","Tindak Lanjut Dirawat","Tindak Lanjut Dirujuk","Pulang Rawat Jalan","Meninggal di UGD","DOA");
$uitable->setHeader($header);

$adapter=new SimpleAdapter();
$adapter->setUseNumber(true, "No.","back.");
$adapter->add("Jenis", "pola_kasus")
		->add("Rujukan", "rujukan")
		->add("Non-Rujukan", "non_rujukan")
		->add("Tindak Lanjut Dirawat", "dirawat")
		->add("Tindak Lanjut Dirujuk", "dirujuk")
		->add("Pulang Rawat Jalan", "rawat_jalan")
		->add("Meninggal di UGD", "mati")
		->add("DOA", "doa");
$rl32->setAdapter($adapter);

$bulan_option = new OptionBuilder();
$bulan_option->add("Semua", "%%", "1");
$bulan_option->add("Januari", "1");
$bulan_option->add("Februari", "2");
$bulan_option->add("Maret", "3");
$bulan_option->add("April", "4");
$bulan_option->add("Mei", "5");
$bulan_option->add("Juni", "6");
$bulan_option->add("Juli", "7");
$bulan_option->add("Agustus", "8");
$bulan_option->add("September", "9");
$bulan_option->add("Oktober", "10");
$bulan_option->add("Nopember", "11");
$bulan_option->add("Desember", "12");

$tahun_rows = $db->get_result("
	SELECT DISTINCT YEAR(tanggal) tahun
	FROM smis_mr_igd
	WHERE prop = '' AND YEAR(tanggal) > 0
	ORDER BY YEAR(tanggal) ASC
");
$tahun_option = new OptionBuilder();
if ($tahun_rows != null) {
	foreach ($tahun_rows as $tahun_row) {
		if (date("Y") == $tahun_row->tahun)
			$tahun_option->add($tahun_row->tahun, $tahun_row->tahun, "1");
		else
			$tahun_option->add($tahun_row->tahun, $tahun_row->tahun);
	}
}

$rl32->addFlag("bulan", "Bulan", "Silakan Pilih Bulan")
->addFlag("tahun", "Tahun", "Silakan Pilih Tahun")
->addNoClear("bulan")
->addNoClear("tahun")
->setDateEnable(true)
->getUItable()
->setActionEnable(false)
->setFooterVisible(false)
->addModal("bulan", "select", "Bulan", $bulan_option->getContent())
->addModal("tahun", "select", "Tahun", $tahun_option->getContent());
//->addModal( "origin", "select", "Asal", $option->getContent() );
$rl32->getForm()->addElement("", $button)->addElement("", $cetak)->addElement("", $excel);

$rl32->setDBresponder(new RL32Responder($rl32->getDBtable(), $rl32->getUItable(), $rl32->getAdapter()));

$rl32 ->addViewData("bulan", "bulan")
		->addViewData("tahun", "tahun");
$rl32->initialize();
?>