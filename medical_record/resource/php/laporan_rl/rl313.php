<?php
	require_once("smis-base/smis-include-service-consumer.php");
	global $db;

	$head = array('No.', 'Golongan Obat', 'Jumlah Item Obat', 'Jumlah Item Obat Tersedia', 'Jumlah Item Obat Formularium Tersedia');
	$uitable_1 = new Table($head, "", null, true);
	$uitable_1->setName("rl313_pengadaan");
	$uitable_1->setAction(false);
	$uitable_1->setFooterVisible(false);
	$uitable_1->setHeaderVisible(false);
	$uitable_1->addHeader("before", "
		<tr>
			<th>A.</th>
			<th colspan='4'>Pengadaan Obat</th>
		</tr>
		<tr>
			<th>No.</th>
			<th>Golongan Obat</th>
			<th>Jumlah Item Obat</th>
			<th>Jumlah Item Obat yang Tersedia di Rumah Sakit</th>
			<th>Jumlah Item Obat Formularium Tersedia di Rumah Sakit</th>
		</tr>
	");

	$head = array('No.', 'Golongan Obat', 'Rawat Jalan', 'IGD', 'Rawat Inap');
	$uitable_2 = new Table($head, "", null, true);
	$uitable_2->setName("rl313_penjualan");
	$uitable_2->setAction(false);
	$uitable_2->setFooterVisible(false);
	$uitable_2->setHeaderVisible(false);
	$uitable_2->addHeader("before", "
		<tr>
			<th>B.</th>
			<th colspan='4'>Penulisan dan Pelayanan Resep</th>
		</tr>
		<tr>
			<th>No.</th>
			<th>Golongan Obat</th>
			<th>Rawat Jalan</th>
			<th>IGD</th>
			<th>Rawat Inap</th>
		</tr>
	");

	if (isset($_POST ['command'])) {
		if ($_POST['command'] == "view") {
			$tahun = $_POST['tahun'];
			$bulan = $_POST['bulan'];

			$html_1 = "";
			$html_2 = "";

			$id_obat_generik_csv = "";
			$rows = $db->get_result("
				SELECT id
				FROM smis_pr_barang
				WHERE prop = '' AND medis = 1 AND inventaris = 0 AND generik = 1
			");
			if ($rows != null) {
				foreach ($rows as $row)
					$id_obat_generik_csv .= $row->id . ",";
				$id_obat_generik_csv = rtrim($id_obat_generik_csv, ",");
			}
			$id_obat_generik_formularium_csv = "";
			$rows = $db->get_result("
				SELECT id
				FROM smis_pr_barang
				WHERE prop = '' AND medis = 1 AND inventaris = 0 AND generik = 1 AND formularium = 1
			");
			if ($rows != null) {
				foreach ($rows as $row)
					$id_obat_generik_formularium_csv .= $row->id . ",";
				$id_obat_generik_formularium_csv = rtrim($id_obat_generik_formularium_csv, ",");
			}
			$id_obat_non_generik_formularium_csv = "";
			$rows = $db->get_result("
				SELECT id
				FROM smis_pr_barang
				WHERE prop = '' AND medis = 1 AND inventaris = 0 AND generik = 0 AND formularium = 1
			");
			if ($rows != null) {
				foreach ($rows as $row)
					$id_obat_non_generik_formularium_csv .= $row->id . ",";
				$id_obat_non_generik_formularium_csv = rtrim($id_obat_non_generik_formularium_csv, ",");
			}
			$id_obat_non_generik_non_formularium_csv = "";
			$rows = $db->get_result("
				SELECT id
				FROM smis_pr_barang
				WHERE prop = '' AND medis = 1 AND inventaris = 0 AND generik = 0 AND formularium = 0
			");
			if ($rows != null) {
				foreach ($rows as $row)
					$id_obat_non_generik_non_formularium_csv .= $row->id . ",";
				$id_obat_non_generik_non_formularium_csv = rtrim($id_obat_non_generik_non_formularium_csv, ",");
			}

			$jumlah_item_obat_generik = 0;
			$jumlah_item_tersedia_obat_generik = 0;
			$jumlah_item_formularium_obat_generik = 0;
			$rawat_jalan_obat_generik = 0;
			$igd_obat_generik = 0;
			$rawat_inap_obat_generik = 0;
			if ($id_obat_generik_csv != "") {
				$params = array(
					"bulan" 		=> $bulan,
					"tahun"			=> $tahun,
					"id_obat_csv"	=> $id_obat_generik_csv
				);
				$consumer_service = new ServiceConsumer(
					$db,
					"get_penerimaan_obat",
					$params,
					"gudang_farmasi"
				);
				$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
				$content = $consumer_service->execute()->getContent();
				$jumlah_item_obat_generik = (float)$content[0];

				$params = array(
					"bulan" 		=> $bulan,
					"tahun"			=> $tahun,
					"id_obat_csv"	=> $id_obat_generik_csv
				);
				$consumer_service = new ServiceConsumer(
					$db,
					"get_persediaan_obat",
					$params,
					"gudang_farmasi"
				);
				$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
				$content = $consumer_service->execute()->getContent();
				$jumlah_item_tersedia_obat_generik += (float)$content[0];
				$consumer_service = new ServiceConsumer(
					$db,
					"get_persediaan_obat",
					$params,
					"depo_farmasi_igd"
				);
				$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
				$content = $consumer_service->execute()->getContent();
				$jumlah_item_tersedia_obat_generik += (float)$content[0];
				$consumer_service = new ServiceConsumer(
					$db,
					"get_persediaan_obat",
					$params,
					"depo_farmasi_irja_irna"
				);
				$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
				$content = $consumer_service->execute()->getContent();
				$jumlah_item_tersedia_obat_generik += (float)$content[0];
				$consumer_service = new ServiceConsumer(
					$db,
					"get_persediaan_obat",
					$params,
					"depo_farmasi_ok"
				);
				$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
				$content = $consumer_service->execute()->getContent();
				$jumlah_item_tersedia_obat_generik += (float)$content[0];

				$params = array(
					"bulan" 		=> $bulan,
					"tahun"			=> $tahun,
					"id_obat_csv"	=> $id_obat_generik_csv,
					"uri"			=> "0"
				);
				$consumer_service = new ServiceConsumer(
					$db,
					"get_penjualan",
					$params,
					"depo_farmasi_irja_irna"
				);
				$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
				$content = $consumer_service->execute()->getContent();
				$rawat_jalan_obat_generik += (float)$content[0];

				$params = array(
					"bulan" 		=> $bulan,
					"tahun"			=> $tahun,
					"id_obat_csv"	=> $id_obat_generik_csv,
					"uri"			=> "0"
				);
				$consumer_service = new ServiceConsumer(
					$db,
					"get_penjualan",
					$params,
					"depo_farmasi_ok"
				);
				$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
				$content = $consumer_service->execute()->getContent();
				$rawat_jalan_obat_generik += (float)$content[0];

				$params = array(
					"bulan" 		=> $bulan,
					"tahun"			=> $tahun,
					"id_obat_csv"	=> $id_obat_generik_csv,
					"uri"			=> "%%"
				);
				$consumer_service = new ServiceConsumer(
					$db,
					"get_penjualan",
					$params,
					"depo_farmasi_igd"
				);
				$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
				$content = $consumer_service->execute()->getContent();
				$igd_obat_generik = (float)$content[0];

				$params = array(
					"bulan" 		=> $bulan,
					"tahun"			=> $tahun,
					"id_obat_csv"	=> $id_obat_generik_csv,
					"uri"			=> "1"
				);
				$consumer_service = new ServiceConsumer(
					$db,
					"get_penjualan",
					$params,
					"depo_farmasi_irja_irna"
				);
				$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
				$content = $consumer_service->execute()->getContent();
				$rawat_inap_obat_generik += (float)$content[0];

				$params = array(
					"bulan" 		=> $bulan,
					"tahun"			=> $tahun,
					"id_obat_csv"	=> $id_obat_generik_csv,
					"uri"			=> "1"
				);
				$consumer_service = new ServiceConsumer(
					$db,
					"get_penjualan",
					$params,
					"depo_farmasi_ok"
				);
				$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
				$content = $consumer_service->execute()->getContent();
				$rawat_inap_obat_generik += (float)$content[0];
			}
			if ($id_obat_generik_formularium_csv != "") {
				$params = array(
					"bulan" 		=> $bulan,
					"tahun"			=> $tahun,
					"id_obat_csv"	=> $id_obat_generik_formularium_csv
				);
				$consumer_service = new ServiceConsumer(
					$db,
					"get_persediaan_obat",
					$params,
					"gudang_farmasi"
				);
				$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
				$content = $consumer_service->execute()->getContent();
				$jumlah_item_formularium_obat_generik += (float)$content[0];
				$consumer_service = new ServiceConsumer(
					$db,
					"get_persediaan_obat",
					$params,
					"depo_farmasi_igd"
				);
				$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
				$content = $consumer_service->execute()->getContent();
				$jumlah_item_formularium_obat_generik += (float)$content[0];
				$consumer_service = new ServiceConsumer(
					$db,
					"get_persediaan_obat",
					$params,
					"depo_farmasi_irja_irna"
				);
				$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
				$content = $consumer_service->execute()->getContent();
				$jumlah_item_formularium_obat_generik += (float)$content[0];
				$consumer_service = new ServiceConsumer(
					$db,
					"get_persediaan_obat",
					$params,
					"depo_farmasi_ok"
				);
				$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
				$content = $consumer_service->execute()->getContent();
				$jumlah_item_formularium_obat_generik += (float)$content[0];
			}

			$jumlah_item_obat_non_generik_formularium = 0;
			$jumlah_item_tersedia_obat_non_generik_formularium = 0;
			$jumlah_item_formularium_obat_non_generik_formularium = 0;
			$rawat_jalan_obat_non_generik_formularium = 0;
			$igd_obat_non_generik_formularium = 0;
			$rawat_inap_obat_non_generik_formularium = 0;
			if ($id_obat_non_generik_formularium_csv != "") {
				$params = array(
					"bulan" 		=> $bulan,
					"tahun"			=> $tahun,
					"id_obat_csv"	=> $id_obat_non_generik_formularium_csv
				);
				$consumer_service = new ServiceConsumer(
					$db,
					"get_penerimaan_obat",
					$params,
					"gudang_farmasi"
				);
				$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
				$content = $consumer_service->execute()->getContent();
				$jumlah_item_obat_non_generik_formularium = (float)$content[0];

				$params = array(
					"bulan" 		=> $bulan,
					"tahun"			=> $tahun,
					"id_obat_csv"	=> $id_obat_non_generik_formularium_csv
				);
				$consumer_service = new ServiceConsumer(
					$db,
					"get_persediaan_obat",
					$params,
					"gudang_farmasi"
				);
				$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
				$content = $consumer_service->execute()->getContent();
				$jumlah_item_tersedia_obat_non_generik_formularium += (float)$content[0];
				$consumer_service = new ServiceConsumer(
					$db,
					"get_persediaan_obat",
					$params,
					"depo_farmasi_igd"
				);
				$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
				$content = $consumer_service->execute()->getContent();
				$jumlah_item_tersedia_obat_non_generik_formularium += (float)$content[0];
				$consumer_service = new ServiceConsumer(
					$db,
					"get_persediaan_obat",
					$params,
					"depo_farmasi_irja_irna"
				);
				$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
				$content = $consumer_service->execute()->getContent();
				$jumlah_item_tersedia_obat_non_generik_formularium += (float)$content[0];
				$consumer_service = new ServiceConsumer(
					$db,
					"get_persediaan_obat",
					$params,
					"depo_farmasi_ok"
				);
				$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
				$content = $consumer_service->execute()->getContent();
				$jumlah_item_tersedia_obat_non_generik_formularium += (float)$content[0];

				$jumlah_item_formularium_obat_non_generik_formularium = $jumlah_item_tersedia_obat_non_generik_formularium;

				$params = array(
					"bulan" 		=> $bulan,
					"tahun"			=> $tahun,
					"id_obat_csv"	=> $id_obat_non_generik_formularium_csv,
					"uri"			=> "0"
				);
				$consumer_service = new ServiceConsumer(
					$db,
					"get_penjualan",
					$params,
					"depo_farmasi_irja_irna"
				);
				$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
				$content = $consumer_service->execute()->getContent();
				$rawat_jalan_obat_non_generik_formularium += (float)$content[0];

				$params = array(
					"bulan" 		=> $bulan,
					"tahun"			=> $tahun,
					"id_obat_csv"	=> $id_obat_non_generik_formularium_csv,
					"uri"			=> "0"
				);
				$consumer_service = new ServiceConsumer(
					$db,
					"get_penjualan",
					$params,
					"depo_farmasi_ok"
				);
				$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
				$content = $consumer_service->execute()->getContent();
				$rawat_jalan_obat_non_generik_formularium += (float)$content[0];

				$params = array(
					"bulan" 		=> $bulan,
					"tahun"			=> $tahun,
					"id_obat_csv"	=> $id_obat_non_generik_formularium_csv,
					"uri"			=> "%%"
				);
				$consumer_service = new ServiceConsumer(
					$db,
					"get_penjualan",
					$params,
					"depo_farmasi_igd"
				);
				$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
				$content = $consumer_service->execute()->getContent();
				$igd_obat_non_generik_formularium = (float)$content[0];

				$params = array(
					"bulan" 		=> $bulan,
					"tahun"			=> $tahun,
					"id_obat_csv"	=> $id_obat_non_generik_formularium_csv,
					"uri"			=> "1"
				);
				$consumer_service = new ServiceConsumer(
					$db,
					"get_penjualan",
					$params,
					"depo_farmasi_irja_irna"
				);
				$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
				$content = $consumer_service->execute()->getContent();
				$rawat_inap_obat_non_generik_formularium += (float)$content[0];

				$params = array(
					"bulan" 		=> $bulan,
					"tahun"			=> $tahun,
					"id_obat_csv"	=> $id_obat_non_generik_formularium_csv,
					"uri"			=> "1"
				);
				$consumer_service = new ServiceConsumer(
					$db,
					"get_penjualan",
					$params,
					"depo_farmasi_ok"
				);
				$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
				$content = $consumer_service->execute()->getContent();
				$rawat_inap_obat_non_generik_formularium += (float)$content[0];
			}

			$jumlah_item_obat_non_generik_non_formularium = 0;
			$jumlah_item_tersedia_obat_non_generik_non_formularium = 0;
			$jumlah_item_formularium_obat_non_generik_non_formularium = 0;
			$rawat_jalan_obat_non_generik_non_formularium = 0;
			$igd_obat_non_generik_non_formularium = 0;
			$rawat_inap_obat_non_generik_non_formularium = 0;
			if ($id_obat_non_generik_non_formularium_csv != "") {
				$params = array(
					"bulan" 		=> $bulan,
					"tahun"			=> $tahun,
					"id_obat_csv"	=> $id_obat_non_generik_non_formularium_csv
				);
				$consumer_service = new ServiceConsumer(
					$db,
					"get_penerimaan_obat",
					$params,
					"gudang_farmasi"
				);
				$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
				$content = $consumer_service->execute()->getContent();
				$jumlah_item_obat_non_generik_non_formularium = (float)$content[0];

				$params = array(
					"bulan" 		=> $bulan,
					"tahun"			=> $tahun,
					"id_obat_csv"	=> $id_obat_non_generik_non_formularium_csv
				);
				$consumer_service = new ServiceConsumer(
					$db,
					"get_persediaan_obat",
					$params,
					"gudang_farmasi"
				);
				$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
				$content = $consumer_service->execute()->getContent();
				$jumlah_item_tersedia_obat_non_generik_non_formularium += (float)$content[0];
				$consumer_service = new ServiceConsumer(
					$db,
					"get_persediaan_obat",
					$params,
					"depo_farmasi_igd"
				);
				$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
				$content = $consumer_service->execute()->getContent();
				$jumlah_item_tersedia_obat_non_generik_non_formularium += (float)$content[0];
				$consumer_service = new ServiceConsumer(
					$db,
					"get_persediaan_obat",
					$params,
					"depo_farmasi_irja_irna"
				);
				$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
				$content = $consumer_service->execute()->getContent();
				$jumlah_item_tersedia_obat_non_generik_non_formularium += (float)$content[0];
				$consumer_service = new ServiceConsumer(
					$db,
					"get_persediaan_obat",
					$params,
					"depo_farmasi_ok"
				);
				$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
				$content = $consumer_service->execute()->getContent();
				$jumlah_item_tersedia_obat_non_generik_non_formularium += (float)$content[0];

				$params = array(
					"bulan" 		=> $bulan,
					"tahun"			=> $tahun,
					"id_obat_csv"	=> $id_obat_non_generik_non_formularium_csv,
					"uri"			=> "0"
				);
				$consumer_service = new ServiceConsumer(
					$db,
					"get_penjualan",
					$params,
					"depo_farmasi_irja_irna"
				);
				$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
				$content = $consumer_service->execute()->getContent();
				$rawat_jalan_obat_non_generik_non_formularium += (float)$content[0];

				$params = array(
					"bulan" 		=> $bulan,
					"tahun"			=> $tahun,
					"id_obat_csv"	=> $id_obat_non_generik_non_formularium_csv,
					"uri"			=> "0"
				);
				$consumer_service = new ServiceConsumer(
					$db,
					"get_penjualan",
					$params,
					"depo_farmasi_ok"
				);
				$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
				$content = $consumer_service->execute()->getContent();
				$rawat_jalan_obat_non_generik_non_formularium += (float)$content[0];

				$params = array(
					"bulan" 		=> $bulan,
					"tahun"			=> $tahun,
					"id_obat_csv"	=> $id_obat_non_generik_non_formularium_csv,
					"uri"			=> "%%"
				);
				$consumer_service = new ServiceConsumer(
					$db,
					"get_penjualan",
					$params,
					"depo_farmasi_igd"
				);
				$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
				$content = $consumer_service->execute()->getContent();
				$igd_obat_non_generik_non_formularium = (float)$content[0];

				$params = array(
					"bulan" 		=> $bulan,
					"tahun"			=> $tahun,
					"id_obat_csv"	=> $id_obat_non_generik_non_formularium_csv,
					"uri"			=> "1"
				);
				$consumer_service = new ServiceConsumer(
					$db,
					"get_penjualan",
					$params,
					"depo_farmasi_irja_irna"
				);
				$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
				$content = $consumer_service->execute()->getContent();
				$rawat_inap_obat_non_generik_non_formularium += (float)$content[0];

				$params = array(
					"bulan" 		=> $bulan,
					"tahun"			=> $tahun,
					"id_obat_csv"	=> $id_obat_non_generik_non_formularium_csv,
					"uri"			=> "1"
				);
				$consumer_service = new ServiceConsumer(
					$db,
					"get_penjualan",
					$params,
					"depo_farmasi_ok"
				);
				$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
				$content = $consumer_service->execute()->getContent();
				$rawat_inap_obat_non_generik_non_formularium += (float)$content[0];
			}

			$html_1 .= 	"<tr class='data_rl313_pengadaan'>" .
							"<td id='nomor'>1</td>"	.
							"<td id='golongan_obat'>Obat Generik (Formularium + Non-Formularium)</td>"	.
							"<td id='jumlah_item_obat' style='display:none;'>" . $jumlah_item_obat_generik . "</td>" .
							"<td id='f_jumlah_item_obat'><div align='right'>" . $jumlah_item_obat_generik . "</div></td>" .	
							"<td id='jumlah_item_obat_tersedia' style='display:none;'>" . $jumlah_item_tersedia_obat_generik . "</td>" .
							"<td id='f_jumlah_item_obat_tersedia'><div align='right'>" . $jumlah_item_tersedia_obat_generik . "</div></td>" .
							"<td id='jumlah_item_obat_formularium' style='display:none;'>" . $jumlah_item_formularium_obat_generik . "</td>" .
							"<td id='f_jumlah_item_obat_formularium'><div align='right'>" . $jumlah_item_formularium_obat_generik . "</div></td>" .
						"</tr>";
			$html_1 .= 	"<tr class='data_rl313_pengadaan'>" .
							"<td id='nomor'>2</td>"	.
							"<td id='golongan_obat'>Obat Non-Generik Formularium</td>"	.
							"<td id='jumlah_item_obat' style='display:none;'>" . $jumlah_item_obat_non_generik_formularium . "</td>" .
							"<td id='f_jumlah_item_obat'><div align='right'>" . $jumlah_item_obat_non_generik_formularium . "</div></td>" .	
							"<td id='jumlah_item_obat_tersedia' style='display:none;'>" . $jumlah_item_tersedia_obat_non_generik_formularium . "</td>" .
							"<td id='f_jumlah_item_obat_tersedia'><div align='right'>" . $jumlah_item_tersedia_obat_non_generik_formularium . "</div></td>" .
							"<td id='jumlah_item_obat_formularium' style='display:none;'>" . $jumlah_item_formularium_obat_non_generik_formularium . "</td>" .
							"<td id='f_jumlah_item_obat_formularium'><div align='right'>" . $jumlah_item_formularium_obat_non_generik_formularium . "</div></td>" .
						"</tr>";
			$html_1 .= 	"<tr class='data_rl313_pengadaan'>" .
							"<td id='nomor'>3</td>"	.
							"<td id='golongan_obat'>Obat Non-Generik Non-Formularium</td>"	.
							"<td id='jumlah_item_obat' style='display:none;'>" . $jumlah_item_obat_non_generik_non_formularium . "</td>" .
							"<td id='f_jumlah_item_obat'><div align='right'>" . $jumlah_item_obat_non_generik_non_formularium . "</div></td>" .	
							"<td id='jumlah_item_obat_tersedia' style='display:none;'>" . $jumlah_item_tersedia_obat_non_generik_non_formularium . "</td>" .
							"<td id='f_jumlah_item_obat_tersedia'><div align='right'>" . $jumlah_item_tersedia_obat_non_generik_non_formularium . "</div></td>" .
							"<td id='jumlah_item_obat_formularium' style='display:none;'>" . $jumlah_item_formularium_obat_non_generik_non_formularium . "</td>" .
							"<td id='f_jumlah_item_obat_formularium'><div align='right'>" . $jumlah_item_formularium_obat_non_generik_non_formularium . "</div></td>" .
						"</tr>";
			$total_jumlah_item_obat = $jumlah_item_obat_generik + $jumlah_item_obat_non_generik_formularium + $jumlah_item_obat_non_generik_non_formularium;
			$total_jumlah_item_obat_tersedia = $jumlah_item_tersedia_obat_generik + $jumlah_item_tersedia_obat_non_generik_formularium + $jumlah_item_tersedia_obat_non_generik_non_formularium;
			$total_jumlah_item_obat_formularium = $jumlah_item_formularium_obat_generik + $jumlah_item_formularium_obat_non_generik_formularium + $jumlah_item_formularium_obat_non_generik_non_formularium;
			$html_1 .= 	"<tr>" .
							"<td colspan='2'><strong><center>Total</center></strong></td>" .
							"<td><strong><div align='right'>" . $total_jumlah_item_obat . "</div></strong></td>" .	
							"<td><strong><div align='right'>" . $total_jumlah_item_obat_tersedia . "</div></strong></td>" .	
							"<td><strong><div align='right'>" . $total_jumlah_item_obat_formularium . "</div></strong></td>" .	
						"</tr>";

			$html_2 .= 	"<tr class='data_rl313_penjualan'>" .
							"<td id='nomor'>1</td>"	.
							"<td id='golongan_obat'>Obat Generik (Formularium + Non-Formularium)</td>"	.
							"<td id='rawat_jalan' style='display:none;'>" . $rawat_jalan_obat_generik . "</td>" .
							"<td id='f_rawat_jalan'><div align='right'>" . $rawat_jalan_obat_generik . "</div></td>" .	
							"<td id='igd' style='display:none;'>" . $igd_obat_generik . "</td>" .
							"<td id='f_igd'><div align='right'>" . $igd_obat_generik . "</div></td>" .
							"<td id='rawat_inap' style='display:none;'>" . $rawat_inap_obat_generik . "</td>" .
							"<td id='f_rawat_inap'><div align='right'>" . $rawat_inap_obat_generik . "</div></td>" .
						"</tr>";
			$html_2 .= 	"<tr class='data_rl313_penjualan'>" .
							"<td id='nomor'>2</td>"	.
							"<td id='golongan_obat'>Obat Non-Generik Formularium</td>"	.
							"<td id='rawat_jalan' style='display:none;'>" . $rawat_jalan_obat_non_generik_formularium . "</td>" .
							"<td id='f_rawat_jalan'><div align='right'>" . $rawat_jalan_obat_non_generik_formularium . "</div></td>" .	
							"<td id='igd' style='display:none;'>" . $igd_obat_non_generik_formularium . "</td>" .
							"<td id='f_igd'><div align='right'>" . $igd_obat_non_generik_formularium . "</div></td>" .
							"<td id='rawat_inap' style='display:none;'>" . $rawat_inap_obat_non_generik_formularium . "</td>" .
							"<td id='f_rawat_inap'><div align='right'>" . $rawat_inap_obat_non_generik_formularium . "</div></td>" .
						"</tr>";
			$html_2 .= 	"<tr class='data_rl313_penjualan'>" .
							"<td id='nomor'>3</td>"	.
							"<td id='golongan_obat'>Obat Non-Generik Non-Formularium</td>"	.
							"<td id='rawat_jalan' style='display:none;'>" . $rawat_jalan_obat_non_generik_non_formularium . "</td>" .
							"<td id='f_rawat_jalan'><div align='right'>" . $rawat_jalan_obat_non_generik_non_formularium . "</div></td>" .	
							"<td id='igd' style='display:none;'>" . $igd_obat_non_generik_non_formularium . "</td>" .
							"<td id='f_igd'><div align='right'>" . $igd_obat_non_generik_non_formularium . "</div></td>" .
							"<td id='rawat_inap' style='display:none;'>" . $rawat_inap_obat_non_generik_non_formularium . "</td>" .
							"<td id='f_rawat_inap'><div align='right'>" . $rawat_inap_obat_non_generik_non_formularium . "</div></td>" .
						"</tr>";
			$total_rawat_jalan = $rawat_jalan_obat_generik + $rawat_jalan_obat_non_generik_formularium + $rawat_jalan_obat_non_generik_non_formularium;
			$total_igd = $igd_obat_generik + $igd_obat_non_generik_formularium + $igd_obat_non_generik_non_formularium;
			$total_rawat_inap = $rawat_inap_obat_generik + $rawat_inap_obat_non_generik_formularium + $rawat_inap_obat_non_generik_non_formularium;
			$html_2 .= 	"<tr>" .
							"<td colspan='2'><strong><center>Total</center></strong></td>" .
							"<td><strong><div align='right'>" . $total_rawat_jalan . "</div></strong></td>" .	
							"<td><strong><div align='right'>" . $total_igd . "</div></strong></td>" .	
							"<td><strong><div align='right'>" . $total_rawat_inap . "</div></strong></td>" .
						"</tr>";

			$data['html_1'] = $html_1;
			$data['html_2'] = $html_2;
			echo json_encode($data);
		} else if ($_POST['command'] == "export_xls") {
			require_once ("smis-libs-out/php-excel/PHPExcel.php");

		    $file = new PHPExcel ();
		    $file->getProperties ()->setCreator ( "PT. Inovasi Ide Utama" );
		    $file->getProperties ()->setTitle ( "RL 3.13 - Farmasi Rumah Sakit" );
		    $file->getProperties ()->setSubject ( "RL 3.13 - Farmasi Rumah Sakit" );
		    $file->getProperties ()->setDescription ( "RL 3.13 - Farmasi Rumah Sakit Generated by System" );
		    $file->getProperties ()->setKeywords ( "RL 3.13 - Farmasi Rumah Sakit" );
		    $file->getProperties ()->setCategory ( "RL 3.13 - Farmasi Rumah Sakit" );

		    $nama_instansi = getSettings($db, "smis_autonomous_name", "Rumah Sakit");
		    $tahun = $_POST['tahun'];
		    $bulan = $_POST['bulan'];

		    $sheet = $file->getActiveSheet ();

		    $i = 1;
		    $sheet->mergeCells("A".$i.":E".$i)->setCellValue("A".$i,$nama_instansi);
		    $sheet->getStyle("A".$i.":E".$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		    $sheet->getStyle("A".$i)->getFont()->setBold(true);
		    $i = $i + 1;
		    $sheet->mergeCells("A".$i.":E".$i)->setCellValue("A".$i,"RL 3.13 - Farmasi Rumah Sakit");
		    $sheet->getStyle("A".$i.":E".$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		    $sheet->getStyle("A".$i)->getFont()->setBold(true);
		    $i = $i + 1;
		    $sheet->mergeCells("A".$i.":E".$i)->setCellValue("A".$i,"Tahun : " . $tahun);
		    $sheet->getStyle("A".$i.":E".$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		    $sheet->getStyle("A".$i)->getFont()->setBold(true);
		   
		    $i = $i + 2;
		    $sheet->setCellValue("A".$i,"A");
		    $sheet->getStyle("A".$i)->getFont()->setBold(true);
		    $sheet->mergeCells("B".$i.":E".$i)->setCellValue("B".$i,"Pengadaan Obat");
		    $sheet->getStyle("B".$i)->getFont()->setBold(true);

		    $i = $i + 1;
		    $border_start = $i;
		    $sheet->setCellValue("A".$i,"NO");
		    $sheet->setCellValue("B".$i,"GOLONGAN OBAT");
		    $sheet->setCellValue("C".$i,"JUMLAH ITEM OBAT");
		    $sheet->setCellValue("D".$i,"JUMLAH ITEM OBAT YANG TERSEDIA DI RUMAH SAKIT");
		    $sheet->setCellValue("E".$i,"JUMLAH ITEM OBAT FORMULARIUM TERSEDIA DI RUMAH SAKIT");
		    $sheet->getStyle("A".$i.":E".$i)->getFont()->setBold(true);
		    $sheet->getStyle("A".$i.":E".$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		    $sheet->getStyle("A".$i.":E".$i)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
		    $i = $i + 1;
		    $sheet->setCellValue("A".$i,"1");
		    $sheet->setCellValue("B".$i,"2");
		    $sheet->setCellValue("C".$i,"3");
		    $sheet->setCellValue("D".$i,"4");
		    $sheet->setCellValue("E".$i,"5");
		    $sheet->getStyle("A".$i.":E".$i)->getFont()->setBold(true);
		    $sheet->getStyle("A".$i.":E".$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		    $sheet->getStyle("A".$i.":E".$i)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

		    $data_pengadaan = json_decode($_POST['detail_pengadaan']);
		    $total_jumlah_item_obat = 0;
		    $total_jumlah_item_obat_tersedia = 0;
		    $total_jumlah_item_obat_formularium = 0;
		    foreach ($data_pengadaan as $dp) {
		    	$i = $i + 1;
			    $sheet->setCellValue("A".$i,$dp->nomor);
			    $sheet->setCellValue("B".$i,$dp->golongan_obat);
			    $sheet->setCellValue("C".$i,$dp->jumlah_item_obat);
			    $sheet->setCellValue("D".$i,$dp->jumlah_item_obat_tersedia);
			    $sheet->setCellValue("E".$i,$dp->jumlah_item_obat_formularium);
			    $total_jumlah_item_obat += $dp->jumlah_item_obat;
			    $total_jumlah_item_obat_tersedia += $dp->jumlah_item_obat_tersedia;
			    $total_jumlah_item_obat_formularium += $dp->jumlah_item_obat_formularium;
		    }
		    $i = $i + 1;
		    $sheet->mergeCells("A".$i.":B".$i)->setCellValue("A".$i,"TOTAL");
		    $sheet->getStyle("A".$i.":B".$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		    $sheet->getStyle("A".$i)->getFont()->setBold(true);
		    $sheet->setCellValue("C".$i,$total_jumlah_item_obat);
		    $sheet->getStyle("C".$i)->getFont()->setBold(true);
		    $sheet->setCellValue("D".$i,$total_jumlah_item_obat_tersedia);
		    $sheet->getStyle("D".$i)->getFont()->setBold(true);
		    $sheet->setCellValue("E".$i,$total_jumlah_item_obat_formularium);
		    $sheet->getStyle("E".$i)->getFont()->setBold(true);

		    $thin = array ();
		    $thin['borders']=array();
		    $thin['borders']['allborders']=array();
		    $thin['borders']['allborders']['style']=PHPExcel_Style_Border::BORDER_THIN ;
		    $sheet->getStyle ( "A".$border_start.":E".$i )->applyFromArray ($thin);

		    $i = $i + 2;
		    $sheet->setCellValue("A".$i,"B");
		    $sheet->getStyle("A".$i)->getFont()->setBold(true);
		    $sheet->mergeCells("B".$i.":E".$i)->setCellValue("B".$i,"Penjualan Obat");
		    $sheet->getStyle("B".$i)->getFont()->setBold(true);

		    $i = $i + 1;
		    $border_start = $i;
		    $sheet->setCellValue("A".$i,"NO");
		    $sheet->setCellValue("B".$i,"GOLONGAN OBAT");
		    $sheet->setCellValue("C".$i,"RAWAT JALAN");
		    $sheet->setCellValue("D".$i,"IGD");
		    $sheet->setCellValue("E".$i,"RAWAT INAP");
		    $sheet->getStyle("A".$i.":E".$i)->getFont()->setBold(true);
		    $sheet->getStyle("A".$i.":E".$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		    $sheet->getStyle("A".$i.":E".$i)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
		    $i = $i + 1;
		    $sheet->setCellValue("A".$i,"1");
		    $sheet->setCellValue("B".$i,"2");
		    $sheet->setCellValue("C".$i,"3");
		    $sheet->setCellValue("D".$i,"4");
		    $sheet->setCellValue("E".$i,"5");
		    $sheet->getStyle("A".$i.":E".$i)->getFont()->setBold(true);
		    $sheet->getStyle("A".$i.":E".$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		    $sheet->getStyle("A".$i.":E".$i)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

		    $data_penjualan = json_decode($_POST['detail_penjualan']);
		    $total_rawat_jalan = 0;
		    $total_igd = 0;
		    $total_rawat_inap = 0;
		    foreach ($data_penjualan as $dp) {
		    	$i = $i + 1;
			    $sheet->setCellValue("A".$i,$dp->nomor);
			    $sheet->setCellValue("B".$i,$dp->golongan_obat);
			    $sheet->setCellValue("C".$i,$dp->rawat_jalan);
			    $sheet->setCellValue("D".$i,$dp->igd);
			    $sheet->setCellValue("E".$i,$dp->rawat_inap);
			    $total_rawat_jalan += $dp->rawat_jalan;
			    $total_igd += $dp->igd;
			    $total_rawat_inap += $dp->rawat_inap;
		    }
		    $i = $i + 1;
		    $sheet->mergeCells("A".$i.":B".$i)->setCellValue("A".$i,"TOTAL");
		    $sheet->getStyle("A".$i.":B".$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		    $sheet->getStyle("A".$i)->getFont()->setBold(true);
		    $sheet->setCellValue("C".$i,$total_rawat_jalan);
		    $sheet->getStyle("C".$i)->getFont()->setBold(true);
		    $sheet->setCellValue("D".$i,$total_igd);
		    $sheet->getStyle("D".$i)->getFont()->setBold(true);
		    $sheet->setCellValue("E".$i,$total_rawat_inap);
		    $sheet->getStyle("E".$i)->getFont()->setBold(true);

		    $thin = array ();
		    $thin['borders']=array();
		    $thin['borders']['allborders']=array();
		    $thin['borders']['allborders']['style']=PHPExcel_Style_Border::BORDER_THIN ;
		    $sheet->getStyle ( "A".$border_start.":E".$i )->applyFromArray ($thin);

		    foreach (range('A', 'E') as $columnID) {
		        $sheet->getColumnDimension($columnID)->setAutoSize(true);
		    }

		    header ( 'Content-Type: application/vnd.ms-excel' );
		    header ( 'Content-Disposition: attachment;filename="RL 3.13 - Farmasi Rumah Sakit.xls"' );
		    header ( 'Cache-Control: max-age=0' );
		    $writer = PHPExcel_IOFactory::createWriter ( $file, 'Excel5' );
		    $writer->save ( 'php://output' );
		}

		return;
	}

	$modal = $uitable_1->getModal();
	$form = $modal->getForm();
	$form->setTitle("RL 3.13 - Farmasi Rumah Sakit");
	$bulan_option = new OptionBuilder();
	$bulan_option->add("Semua", "%%", "1");
	$bulan_option->add("Januari", "1");
	$bulan_option->add("Februari", "2");
	$bulan_option->add("Maret", "3");
	$bulan_option->add("April", "4");
	$bulan_option->add("Mei", "5");
	$bulan_option->add("Juni", "6");
	$bulan_option->add("Juli", "7");
	$bulan_option->add("Agustus", "8");
	$bulan_option->add("September", "9");
	$bulan_option->add("Oktober", "10");
	$bulan_option->add("Nopember", "11");
	$bulan_option->add("Desember", "12");
	$bulan_select = new Select("rl313_bulan_filter", "rl313_bulan_filter", $bulan_option->getContent());
	$form->addElement("Bulan", $bulan_select);

	$tahun_option = new OptionBuilder();
	$consumer_service = new ServiceConsumer(
		$db,
		"get_tahun_penjualan_list",
		null
	);
	$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
	$tahun_penjualan = $consumer_service->execute()->getContent();
	
	$consumer_service = new ServiceConsumer(
		$db,
		"get_tahun_penerimaan_list",
		null
	);
	$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
	$tahun_penerimaan = $consumer_service->execute()->getContent();

	$tahun_list = array_unique(array_merge($tahun_penjualan, $tahun_penerimaan));
	sort($tahun_list);
	if (count($tahun_list) > 0) {
		foreach ($tahun_list as $tahun) {
			if ($tahun == date("Y"))
				$tahun_option->add($tahun, $tahun, "1");
			else
				$tahun_option->addSingle($tahun);
		}
	}
	$tahun_select = new Select("rl313_tahun_filter", "rl313_tahun_filter", $tahun_option->getContent());
	$form->addElement("Tahun", $tahun_select);
	$excel_button = new Button("", "", "");
	$excel_button 	->addClass("btn-primary")
					->setIsButton(Button::$ICONIC)
					->setIcon("fa fa-file-excel-o")
					->setAction("rl313.excel()");
	$process_button = new Button("", "", "");
	$process_button	->addClass("btn-inverse")
					-> setIsButton(Button::$ICONIC)
					->setIcon("fa fa-circle-o")
					->setAction("rl313.view()");
	$button_group = new ButtonGroup("noprint");
	$button_group->addButton($process_button);
	$button_group->addButton($excel_button);
	$form->addElement("", $button_group);

	echo $form->getHtml();
	echo $uitable_1->getHtml();
	echo $uitable_2->getHtml();
	echo addJS("framework/smis/js/table_action.js");
	echo addJS("medical_record/resource/js/rl313.js", false);
?>