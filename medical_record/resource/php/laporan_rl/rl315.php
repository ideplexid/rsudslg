<?php
	require_once("smis-base/smis-include-service-consumer.php");
	global $db;

	$head = array(
		'Kode Propinsi', 'Kab/Kota', 'Kode RS', 'Nama RS', 'Tahun', 'No.', 'Jenis Spesialisasi', 
		'Rujukan Diterima dari Puskesmas', 'Rujukan Diterima dari Faskes Lain', 'Rujukan Diterima dari RS Lain',
		'Dikembalikan ke Puskesmas', 'Dikembalikan ke Faskes Lain', 'Dikembalikan ke RS Asal',
		'Dirujuk - Pasien Rujukan', 'Dirujuk - Pasien Datang Sendiri, Dirujuk - Diterima Kembali'
	);
	$uitable = new Table($head, "", null, true);
	$uitable->setName("rl315");
	$uitable->setAction(false);
	$uitable->setFooterVisible(false);
	$uitable->setHeaderVisible(false);
	$uitable->addHeader("before", "
		<tr>
			<th rowspan='3'>Kode Propinsi</th>
			<th rowspan='3'>Kab/Kota</th>
			<th rowspan='3'>Kode RS</th>
			<th rowspan='3'>Nama RS</th>
			<th rowspan='3'>Tahun</th>
			<th rowspan='2'>No.</th>
			<th rowspan='2'>Cara Pembayaran</th>
			<th colspan='2'>Pasien Rawat Inap</th>
			<th rowspan='2'>Jumlah Pasien Rawat Jalan</th>
			<th colspan='3'>Jumlah Pasien Rawat Jalan</th>
		</tr>
		<tr>
			<th>Jumlah Pasien Keluar</th>
			<th>Jumlah Lama Dirawat</th>
			<th>Laboratorium</th>
			<th>Radiologi</th>
			<th>Lain-Lain</th>
		</tr>
		<tr>
			<th>1</th>
			<th>2</th>
			<th>3</th>
			<th>4</th>
			<th>5</th>
			<th>6</th>
			<th>7</th>
			<th>8</th>
		</tr>
	");

	if (isset($_POST ['command'])) {
		if ($_POST['command'] == "view") {
			$tahun = $_POST['tahun'];
			$bulan = $_POST['bulan'];
			$html = "";

			$kode_rs = "";
			$nama_rs = "";
			$kode_propinsi = "";
			$kabupaten_kota = "";
			$info_rs_row = $db->get_row("
				SELECT 
					*
				FROM 
					smis_mr_data_dasar_rs
				WHERE 
					prop = ''
				LIMIT 
					0, 1
			");
			if ($info_rs_row != null) {
				$kode_rs = $info_rs_row->koders;
				$nama_rs = $info_rs_row->nama_rs;
				$kode_propinsi = $info_rs_row->kode_provinsi;
				$kabupaten_kota = $info_rs_row->kota_kabupaten;
			}

			$sql_view = "
				SELECT
					a.id noreg_pasien, 
					a.uri, 
					a.carapulang, 
					a.carabayar, 
					CASE WHEN c.nama IS NULL THEN '' ELSE c.nama END asuransi, 
					CASE WHEN c.pemerintah IS NULL THEN 0 ELSE c.pemerintah END pemerintah, 
					CASE WHEN a.uri = 1 THEN (
						CASE WHEN a.tanggal_pulang = a.tanggal_inap THEN 1 ELSE DATEDIFF(a.tanggal_pulang, a.tanggal_inap) END
					) ELSE ( 
						CASE WHEN a.tanggal_pulang = a.tanggal THEN 1 ELSE DATEDIFF(a.tanggal_pulang, a.tanggal) END 
					) END los,
					a.opsi_gratis, 
					a.alasan_gratis, 
					CASE WHEN GROUP_CONCAT(DISTINCT b.metode) IS NULL THEN '' ELSE GROUP_CONCAT(DISTINCT b.metode ORDER BY b.metode ASC) END metode_bayar_csv,
					COUNT(DISTINCT b.metode) jumlah_metode_bayar,
					CASE WHEN a.jenislayanan LIKE 'lab%' THEN 1 ELSE 0 END laboratorium,
					CASE WHEN a.jenislayanan LIKE 'rad%' THEN 1 ELSE 0 END radiologi
				FROM
					smis_rg_layananpasien a LEFT JOIN smis_ksr_bayar b ON a.id = b.noreg_pasien LEFT JOIN smis_rg_asuransi c ON a.asuransi = c.id
				WHERE
					a.prop = '' AND 
					b.prop = '' AND
					a.selesai = 1 AND 
					a.carapulang NOT LIKE 'Tidak Datang' AND 
					a.carapulang NOT LIKE '' AND
					b.metode NOT LIKE '' AND
					MONTH(a.tanggal_pulang) LIKE '" . $bulan . "' AND 
					YEAR(a.tanggal_pulang) = " . $tahun . "
				GROUP BY
					a.id
			";

			$pri_jumlah_pasien_keluar = 0;
			$pri_jumlah_pasien_keluar_row = $db->get_row("
				SELECT 
					COUNT(*) jumlah
				FROM (
					" . $sql_view . "
				) v
				WHERE 
					uri = 1 AND
					carabayar = 'umum' AND
					asuransi = '' AND
					pemerintah = 0 AND
					opsi_gratis NOT LIKE '%Gratis%' AND
					alasan_gratis = '' AND
					(metode_bayar_csv LIKE '%cash%' OR metode_bayar_csv LIKE '%bank%' AND metode_bayar_csv NOT LIKE '%asuransi%')
			");
			if ($pri_jumlah_pasien_keluar_row != null)
				$pri_jumlah_pasien_keluar = $pri_jumlah_pasien_keluar_row->jumlah;
			$pri_jumlah_lama_dirawat = 0;
			$pri_jumlah_lama_dirawat_row = $db->get_row("
				SELECT 
					SUM(los) jumlah
				FROM (
					" . $sql_view . "
				) v
				WHERE 
					uri = 1 AND
					carabayar = 'umum' AND
					asuransi = '' AND
					pemerintah = 0 AND
					opsi_gratis NOT LIKE '%Gratis%' AND
					alasan_gratis = '' AND
					(metode_bayar_csv LIKE '%cash%' OR metode_bayar_csv LIKE '%bank%' AND metode_bayar_csv NOT LIKE '%asuransi%')
			");
			if ($pri_jumlah_lama_dirawat_row != null)
				$pri_jumlah_lama_dirawat = $pri_jumlah_lama_dirawat_row->jumlah == "" ? 0 : $pri_jumlah_lama_dirawat_row->jumlah;
			$jumlah_pasien_rawat_jalan = 0;
			$jumlah_pasien_rawat_jalan_row = $db->get_row("
				SELECT 
					COUNT(*) jumlah
				FROM (
					" . $sql_view . "
				) v
				WHERE 
					uri = 0 AND
					carabayar = 'umum' AND
					asuransi = '' AND
					pemerintah = 0 AND
					opsi_gratis NOT LIKE '%Gratis%' AND
					alasan_gratis = '' AND
					(metode_bayar_csv LIKE '%cash%' OR metode_bayar_csv LIKE '%bank%' AND metode_bayar_csv NOT LIKE '%asuransi%')
			");
			if ($jumlah_pasien_rawat_jalan_row != null)
				$jumlah_pasien_rawat_jalan = $jumlah_pasien_rawat_jalan_row->jumlah;
			$jprj_laboratorium = 0;
			$jprj_laboratorium_row = $db->get_row("
				SELECT 
					SUM(laboratorium) jumlah
				FROM (
					" . $sql_view . "
				) v
				WHERE 
					uri = 0 AND
					carabayar = 'umum' AND
					asuransi = '' AND
					pemerintah = 0 AND
					opsi_gratis NOT LIKE '%Gratis%' AND
					alasan_gratis = '' AND
					(metode_bayar_csv LIKE '%cash%' OR metode_bayar_csv LIKE '%bank%' AND metode_bayar_csv NOT LIKE '%asuransi%')
			");
			if ($jprj_laboratorium_row != null)
				$jprj_laboratorium = $jprj_laboratorium_row->jumlah == "" ? 0 : $jprj_laboratorium_row->jumlah;
			$jprj_radiologi = 0;
			$jprj_radiologi_row = $db->get_row("
				SELECT 
					SUM(radiologi) jumlah
				FROM (
					" . $sql_view . "
				) v
				WHERE 
					uri = 0 AND
					carabayar = 'umum' AND
					asuransi = '' AND
					pemerintah = 0 AND
					opsi_gratis NOT LIKE '%Gratis%' AND
					alasan_gratis = '' AND
					(metode_bayar_csv LIKE '%cash%' OR metode_bayar_csv LIKE '%bank%' AND metode_bayar_csv NOT LIKE '%asuransi%')
			");
			if ($jprj_radiologi_row != null)
				$jprj_radiologi = $jprj_radiologi_row->jumlah == "" ? 0 : $jprj_radiologi_row->jumlah;
			$jprj_lain = 0;
			$jprj_lain_row = $db->get_row("
				SELECT 
					COUNT(*) jumlah
				FROM (
					" . $sql_view . "
				) v
				WHERE 
					laboratorium = 0 AND
					radiologi = 0 AND
					uri = 0 AND
					carabayar = 'umum' AND
					asuransi = '' AND
					pemerintah = 0 AND
					opsi_gratis NOT LIKE '%Gratis%' AND
					alasan_gratis = '' AND
					(metode_bayar_csv LIKE '%cash%' OR metode_bayar_csv LIKE '%bank%' AND metode_bayar_csv NOT LIKE '%asuransi%')
			");
			if ($jprj_lain_row != null)
				$jprj_lain = $jprj_lain_row->jumlah;

			$html .= 	"<tr class='data_rl315'>" .
							"<td id='rl315_kode_propinsi'>" . $kode_propinsi . "</td>" .
							"<td id='rl315_kabupaten_kota'>" . $kabupaten_kota . "</td>" .
							"<td id='rl315_kode_rs'>" . $kode_rs . "</td>" .
							"<td id='rl315_nama_rs'>" . $nama_rs . "</td>" .
							"<td id='rl315_tahun'>" . $tahun . "</td>" .
							"<td id='rl315_nomor'>1</td>" .
							"<td id='rl315_cara_pembayaran'>Membayar Sendiri</td>" .
							"<td id='rl315_pri_jumlah_pasien_keluar' style='display: none;'>" . $pri_jumlah_pasien_keluar . "</td>" .
							"<td id='rl315_f_pri_jumlah_pasien_keluar'><div align='right'>" . $pri_jumlah_pasien_keluar . "</div></td>" .
							"<td id='rl315_pri_jumlah_lama_dirawat' style='display: none;'>" . $pri_jumlah_lama_dirawat . "</td>" .
							"<td id='rl315_f_pri_jumlah_lama_dirawat'><div align='right'>" . $pri_jumlah_lama_dirawat . "</div></td>" .
							"<td id='rl315_jumlah_pasien_rawat_jalan' style='display: none;'>" . $jumlah_pasien_rawat_jalan . "</td>" .
							"<td id='rl315_f_jumlah_pasien_rawat_jalan'><div align='right'>" . $jumlah_pasien_rawat_jalan . "</div></td>" .
							"<td id='rl315_jprj_laboratorium' style='display: none;'>" . $jprj_laboratorium . "</td>" .
							"<td id='rl315_f_jprj_laboratorium'><div align='right'>" . $jprj_laboratorium . "</div></td>" .
							"<td id='rl315_jprj_radiologi' style='display: none;'>" . $jprj_radiologi . "</td>" .
							"<td id='rl315_f_jprj_radiologi'><div align='right'>" . $jprj_radiologi . "</div></td>" .
							"<td id='rl315_jprj_lain' style='display: none;'>" . $jprj_lain . "</td>" .
							"<td id='rl315_f_jprj_lain'><div align='right'>" . $jprj_lain . "</div></td>" .
						"</tr>";

			$pri_jumlah_pasien_keluar = "";
			$pri_jumlah_pasien_keluar = "";
			$pri_jumlah_lama_dirawat = "";
			$pri_jumlah_lama_dirawat = "";
			$jumlah_pasien_rawat_jalan = "";
			$jumlah_pasien_rawat_jalan = "";
			$jprj_laboratorium = "";
			$jprj_laboratorium = "";
			$jprj_radiologi = "";
			$jprj_radiologi = "";
			$jprj_lain = "";
			$jprj_lain = "";

			$html .= 	"<tr class='data_rl315'>" .
							"<td id='rl315_kode_propinsi'>" . $kode_propinsi . "</td>" .
							"<td id='rl315_kabupaten_kota'>" . $kabupaten_kota . "</td>" .
							"<td id='rl315_kode_rs'>" . $kode_rs . "</td>" .
							"<td id='rl315_nama_rs'>" . $nama_rs . "</td>" .
							"<td id='rl315_tahun'>" . $tahun . "</td>" .
							"<td id='rl315_nomor'>2</td>" .
							"<td id='rl315_cara_pembayaran'>Asuransi :</td>" .
							"<td id='rl315_pri_jumlah_pasien_keluar' style='display: none;'>" . $pri_jumlah_pasien_keluar . "</td>" .
							"<td id='rl315_f_pri_jumlah_pasien_keluar'><div align='right'>" . $pri_jumlah_pasien_keluar . "</div></td>" .
							"<td id='rl315_pri_jumlah_lama_dirawat' style='display: none;'>" . $pri_jumlah_lama_dirawat . "</td>" .
							"<td id='rl315_f_pri_jumlah_lama_dirawat'><div align='right'>" . $pri_jumlah_lama_dirawat . "</div></td>" .
							"<td id='rl315_jumlah_pasien_rawat_jalan' style='display: none;'>" . $jumlah_pasien_rawat_jalan . "</td>" .
							"<td id='rl315_f_jumlah_pasien_rawat_jalan'><div align='right'>" . $jumlah_pasien_rawat_jalan . "</div></td>" .
							"<td id='rl315_jprj_laboratorium' style='display: none;'>" . $jprj_laboratorium . "</td>" .
							"<td id='rl315_f_jprj_laboratorium'><div align='right'>" . $jprj_laboratorium . "</div></td>" .
							"<td id='rl315_jprj_radiologi' style='display: none;'>" . $jprj_radiologi . "</td>" .
							"<td id='rl315_f_jprj_radiologi'><div align='right'>" . $jprj_radiologi . "</div></td>" .
							"<td id='rl315_jprj_lain' style='display: none;'>" . $jprj_lain . "</td>" .
							"<td id='rl315_f_jprj_lain'><div align='right'>" . $jprj_lain . "</div></td>" .
						"</tr>";

			$pri_jumlah_pasien_keluar = 0;
			$pri_jumlah_pasien_keluar_row = $db->get_row("
				SELECT 
					COUNT(*) jumlah
				FROM (
					" . $sql_view . "
				) v
				WHERE 
					uri = 1 AND
					carabayar NOT LIKE 'umum' AND
					asuransi LIKE '%%' AND
					pemerintah = 1 AND
					opsi_gratis NOT LIKE '%Gratis%' AND
					alasan_gratis = '' AND
					(metode_bayar_csv NOT LIKE '%cash%' AND metode_bayar_csv NOT LIKE '%bank%' AND metode_bayar_csv LIKE '%asuransi%')
			");
			if ($pri_jumlah_pasien_keluar_row != null)
				$pri_jumlah_pasien_keluar = $pri_jumlah_pasien_keluar_row->jumlah;
			$pri_jumlah_lama_dirawat = 0;
			$pri_jumlah_lama_dirawat_row = $db->get_row("
				SELECT 
					SUM(los) jumlah
				FROM (
					" . $sql_view . "
				) v
				WHERE 
					uri = 1 AND
					carabayar NOT LIKE 'umum' AND
					asuransi LIKE '%%' AND
					pemerintah = 1 AND
					opsi_gratis NOT LIKE '%Gratis%' AND
					alasan_gratis = '' AND
					(metode_bayar_csv NOT LIKE '%cash%' AND metode_bayar_csv NOT LIKE '%bank%' AND metode_bayar_csv LIKE '%asuransi%')
			");
			if ($pri_jumlah_lama_dirawat_row != null)
				$pri_jumlah_lama_dirawat = $pri_jumlah_lama_dirawat_row->jumlah == "" ? 0 : $pri_jumlah_lama_dirawat_row->jumlah;
			$jumlah_pasien_rawat_jalan = 0;
			$jumlah_pasien_rawat_jalan_row = $db->get_row("
				SELECT 
					COUNT(*) jumlah
				FROM (
					" . $sql_view . "
				) v
				WHERE 
					uri = 0 AND
					carabayar NOT LIKE 'umum' AND
					asuransi LIKE '%%' AND
					pemerintah = 1 AND
					opsi_gratis NOT LIKE '%Gratis%' AND
					alasan_gratis = '' AND
					(metode_bayar_csv NOT LIKE '%cash%' AND metode_bayar_csv NOT LIKE '%bank%' AND metode_bayar_csv LIKE '%asuransi%')
			");
			if ($jumlah_pasien_rawat_jalan_row != null)
				$jumlah_pasien_rawat_jalan = $jumlah_pasien_rawat_jalan_row->jumlah;
			$jprj_laboratorium = 0;
			$jprj_laboratorium_row = $db->get_row("
				SELECT 
					SUM(laboratorium) jumlah
				FROM (
					" . $sql_view . "
				) v
				WHERE 
					uri = 0 AND
					carabayar NOT LIKE 'umum' AND
					asuransi LIKE '%%' AND
					pemerintah = 1 AND
					opsi_gratis NOT LIKE '%Gratis%' AND
					alasan_gratis = '' AND
					(metode_bayar_csv NOT LIKE '%cash%' AND metode_bayar_csv NOT LIKE '%bank%' AND metode_bayar_csv LIKE '%asuransi%')
			");
			if ($jprj_laboratorium_row != null)
				$jprj_laboratorium = $jprj_laboratorium_row->jumlah == "" ? 0 : $jprj_laboratorium_row->jumlah;
			$jprj_radiologi = 0;
			$jprj_radiologi_row = $db->get_row("
				SELECT 
					SUM(radiologi) jumlah
				FROM (
					" . $sql_view . "
				) v
				WHERE 
					uri = 0 AND
					carabayar NOT LIKE 'umum' AND
					asuransi LIKE '%%' AND
					pemerintah = 1 AND
					opsi_gratis NOT LIKE '%Gratis%' AND
					alasan_gratis = '' AND
					(metode_bayar_csv NOT LIKE '%cash%' AND metode_bayar_csv NOT LIKE '%bank%' AND metode_bayar_csv LIKE '%asuransi%')
			");
			if ($jprj_radiologi_row != null)
				$jprj_radiologi = $jprj_radiologi_row->jumlah == "" ? 0 : $jprj_radiologi_row->jumlah;
			$jprj_lain = 0;
			$jprj_lain_row = $db->get_row("
				SELECT 
					COUNT(*) jumlah
				FROM (
					" . $sql_view . "
				) v
				WHERE 
					laboratorium = 0 AND
					radiologi = 0 AND
					uri = 0 AND
					carabayar NOT LIKE 'umum' AND
					asuransi LIKE '%%' AND
					pemerintah = 1 AND
					opsi_gratis NOT LIKE '%Gratis%' AND
					alasan_gratis = '' AND
					(metode_bayar_csv NOT LIKE '%cash%' AND metode_bayar_csv NOT LIKE '%bank%' AND metode_bayar_csv LIKE '%asuransi%')
			");
			if ($jprj_lain_row != null)
				$jprj_lain = $jprj_lain_row->jumlah;

			$html .= 	"<tr class='data_rl315'>" .
							"<td id='rl315_kode_propinsi'>" . $kode_propinsi . "</td>" .
							"<td id='rl315_kabupaten_kota'>" . $kabupaten_kota . "</td>" .
							"<td id='rl315_kode_rs'>" . $kode_rs . "</td>" .
							"<td id='rl315_nama_rs'>" . $nama_rs . "</td>" .
							"<td id='rl315_tahun'>" . $tahun . "</td>" .
							"<td id='rl315_nomor'>2.1</td>" .
							"<td id='rl315_cara_pembayaran'>Asuransi Pemerintah</td>" .
							"<td id='rl315_pri_jumlah_pasien_keluar' style='display: none;'>" . $pri_jumlah_pasien_keluar . "</td>" .
							"<td id='rl315_f_pri_jumlah_pasien_keluar'><div align='right'>" . $pri_jumlah_pasien_keluar . "</div></td>" .
							"<td id='rl315_pri_jumlah_lama_dirawat' style='display: none;'>" . $pri_jumlah_lama_dirawat . "</td>" .
							"<td id='rl315_f_pri_jumlah_lama_dirawat'><div align='right'>" . $pri_jumlah_lama_dirawat . "</div></td>" .
							"<td id='rl315_jumlah_pasien_rawat_jalan' style='display: none;'>" . $jumlah_pasien_rawat_jalan . "</td>" .
							"<td id='rl315_f_jumlah_pasien_rawat_jalan'><div align='right'>" . $jumlah_pasien_rawat_jalan . "</div></td>" .
							"<td id='rl315_jprj_laboratorium' style='display: none;'>" . $jprj_laboratorium . "</td>" .
							"<td id='rl315_f_jprj_laboratorium'><div align='right'>" . $jprj_laboratorium . "</div></td>" .
							"<td id='rl315_jprj_radiologi' style='display: none;'>" . $jprj_radiologi . "</td>" .
							"<td id='rl315_f_jprj_radiologi'><div align='right'>" . $jprj_radiologi . "</div></td>" .
							"<td id='rl315_jprj_lain' style='display: none;'>" . $jprj_lain . "</td>" .
							"<td id='rl315_f_jprj_lain'><div align='right'>" . $jprj_lain . "</div></td>" .
						"</tr>";

			$pri_jumlah_pasien_keluar = 0;
			$pri_jumlah_pasien_keluar_row = $db->get_row("
				SELECT 
					COUNT(*) jumlah
				FROM (
					" . $sql_view . "
				) v
				WHERE 
					uri = 1 AND
					carabayar NOT LIKE 'umum' AND
					asuransi LIKE '%%' AND
					pemerintah = 0 AND
					opsi_gratis NOT LIKE '%Gratis%' AND
					alasan_gratis = '' AND
					(metode_bayar_csv NOT LIKE '%cash%' AND metode_bayar_csv NOT LIKE '%bank%' AND metode_bayar_csv LIKE '%asuransi%')
			");
			if ($pri_jumlah_pasien_keluar_row != null)
				$pri_jumlah_pasien_keluar = $pri_jumlah_pasien_keluar_row->jumlah;
			$pri_jumlah_lama_dirawat = 0;
			$pri_jumlah_lama_dirawat_row = $db->get_row("
				SELECT 
					SUM(los) jumlah
				FROM (
					" . $sql_view . "
				) v
				WHERE 
					uri = 1 AND
					carabayar NOT LIKE 'umum' AND
					asuransi LIKE '%%' AND
					pemerintah = 0 AND
					opsi_gratis NOT LIKE '%Gratis%' AND
					alasan_gratis = '' AND
					(metode_bayar_csv NOT LIKE '%cash%' AND metode_bayar_csv NOT LIKE '%bank%' AND metode_bayar_csv LIKE '%asuransi%')
			");
			if ($pri_jumlah_lama_dirawat_row != null)
				$pri_jumlah_lama_dirawat = $pri_jumlah_lama_dirawat_row->jumlah == "" ? 0 : $pri_jumlah_lama_dirawat_row->jumlah;
			$jumlah_pasien_rawat_jalan = 0;
			$jumlah_pasien_rawat_jalan_row = $db->get_row("
				SELECT 
					COUNT(*) jumlah
				FROM (
					" . $sql_view . "
				) v
				WHERE 
					uri = 0 AND
					carabayar NOT LIKE 'umum' AND
					asuransi LIKE '%%' AND
					pemerintah = 0 AND
					opsi_gratis NOT LIKE '%Gratis%' AND
					alasan_gratis = '' AND
					(metode_bayar_csv NOT LIKE '%cash%' AND metode_bayar_csv NOT LIKE '%bank%' AND metode_bayar_csv LIKE '%asuransi%')
			");
			if ($jumlah_pasien_rawat_jalan_row != null)
				$jumlah_pasien_rawat_jalan = $jumlah_pasien_rawat_jalan_row->jumlah;
			$jprj_laboratorium = 0;
			$jprj_laboratorium_row = $db->get_row("
				SELECT 
					SUM(laboratorium) jumlah
				FROM (
					" . $sql_view . "
				) v
				WHERE 
					uri = 0 AND
					carabayar NOT LIKE 'umum' AND
					asuransi LIKE '%%' AND
					pemerintah = 0 AND
					opsi_gratis NOT LIKE '%Gratis%' AND
					alasan_gratis = '' AND
					(metode_bayar_csv NOT LIKE '%cash%' AND metode_bayar_csv NOT LIKE '%bank%' AND metode_bayar_csv LIKE '%asuransi%')
			");
			if ($jprj_laboratorium_row != null)
				$jprj_laboratorium = $jprj_laboratorium_row->jumlah == "" ? 0 : $jprj_laboratorium_row->jumlah;
			$jprj_radiologi = 0;
			$jprj_radiologi_row = $db->get_row("
				SELECT 
					SUM(radiologi) jumlah
				FROM (
					" . $sql_view . "
				) v
				WHERE 
					uri = 0 AND
					carabayar NOT LIKE 'umum' AND
					asuransi LIKE '%%' AND
					pemerintah = 0 AND
					opsi_gratis NOT LIKE '%Gratis%' AND
					alasan_gratis = '' AND
					(metode_bayar_csv NOT LIKE '%cash%' AND metode_bayar_csv NOT LIKE '%bank%' AND metode_bayar_csv LIKE '%asuransi%')
			");
			if ($jprj_radiologi_row != null)
				$jprj_radiologi = $jprj_radiologi_row->jumlah == "" ? 0 : $jprj_radiologi_row->jumlah;
			$jprj_lain = 0;
			$jprj_lain_row = $db->get_row("
				SELECT 
					COUNT(*) jumlah
				FROM (
					" . $sql_view . "
				) v
				WHERE 
					laboratorium = 0 AND
					radiologi = 0 AND
					uri = 0 AND
					carabayar NOT LIKE 'umum' AND
					asuransi LIKE '%%' AND
					pemerintah = 0 AND
					opsi_gratis NOT LIKE '%Gratis%' AND
					alasan_gratis = '' AND
					(metode_bayar_csv NOT LIKE '%cash%' AND metode_bayar_csv NOT LIKE '%bank%' AND metode_bayar_csv LIKE '%asuransi%')
			");
			if ($jprj_lain_row != null)
				$jprj_lain = $jprj_lain_row->jumlah;

			$html .= 	"<tr class='data_rl315'>" .
							"<td id='rl315_kode_propinsi'>" . $kode_propinsi . "</td>" .
							"<td id='rl315_kabupaten_kota'>" . $kabupaten_kota . "</td>" .
							"<td id='rl315_kode_rs'>" . $kode_rs . "</td>" .
							"<td id='rl315_nama_rs'>" . $nama_rs . "</td>" .
							"<td id='rl315_tahun'>" . $tahun . "</td>" .
							"<td id='rl315_nomor'>2.2</td>" .
							"<td id='rl315_cara_pembayaran'>Asuransi Swasta</td>" .
							"<td id='rl315_pri_jumlah_pasien_keluar' style='display: none;'>" . $pri_jumlah_pasien_keluar . "</td>" .
							"<td id='rl315_f_pri_jumlah_pasien_keluar'><div align='right'>" . $pri_jumlah_pasien_keluar . "</div></td>" .
							"<td id='rl315_pri_jumlah_lama_dirawat' style='display: none;'>" . $pri_jumlah_lama_dirawat . "</td>" .
							"<td id='rl315_f_pri_jumlah_lama_dirawat'><div align='right'>" . $pri_jumlah_lama_dirawat . "</div></td>" .
							"<td id='rl315_jumlah_pasien_rawat_jalan' style='display: none;'>" . $jumlah_pasien_rawat_jalan . "</td>" .
							"<td id='rl315_f_jumlah_pasien_rawat_jalan'><div align='right'>" . $jumlah_pasien_rawat_jalan . "</div></td>" .
							"<td id='rl315_jprj_laboratorium' style='display: none;'>" . $jprj_laboratorium . "</td>" .
							"<td id='rl315_f_jprj_laboratorium'><div align='right'>" . $jprj_laboratorium . "</div></td>" .
							"<td id='rl315_jprj_radiologi' style='display: none;'>" . $jprj_radiologi . "</td>" .
							"<td id='rl315_f_jprj_radiologi'><div align='right'>" . $jprj_radiologi . "</div></td>" .
							"<td id='rl315_jprj_lain' style='display: none;'>" . $jprj_lain . "</td>" .
							"<td id='rl315_f_jprj_lain'><div align='right'>" . $jprj_lain . "</div></td>" .
						"</tr>";

			$pri_jumlah_pasien_keluar = 0;
			$pri_jumlah_pasien_keluar_row = $db->get_row("
				SELECT 
					COUNT(*) jumlah
				FROM (
					" . $sql_view . "
				) v
				WHERE 
					uri = 1 AND
					carabayar LIKE '%%' AND
					asuransi LIKE '%%' AND
					pemerintah LIKE '%%' AND
					opsi_gratis NOT LIKE '%Gratis%' AND
					alasan_gratis = '' AND
					(metode_bayar_csv LIKE '%cash%' AND metode_bayar_csv LIKE '%bank%' AND metode_bayar_csv LIKE '%asuransi%') AND
					jumlah_metode_bayar > 1
			");
			if ($pri_jumlah_pasien_keluar_row != null)
				$pri_jumlah_pasien_keluar = $pri_jumlah_pasien_keluar_row->jumlah;
			$pri_jumlah_lama_dirawat = 0;
			$pri_jumlah_lama_dirawat_row = $db->get_row("
				SELECT 
					SUM(los) jumlah
				FROM (
					" . $sql_view . "
				) v
				WHERE 
					uri = 1 AND
					carabayar LIKE '%%' AND
					asuransi LIKE '%%' AND
					pemerintah LIKE '%%' AND
					opsi_gratis NOT LIKE '%Gratis%' AND
					alasan_gratis = '' AND
					(metode_bayar_csv LIKE '%cash%' AND metode_bayar_csv LIKE '%bank%' AND metode_bayar_csv LIKE '%asuransi%') AND
					jumlah_metode_bayar > 1
			");
			if ($pri_jumlah_lama_dirawat_row != null)
				$pri_jumlah_lama_dirawat = $pri_jumlah_lama_dirawat_row->jumlah == "" ? 0 : $pri_jumlah_lama_dirawat_row->jumlah;
			$jumlah_pasien_rawat_jalan = 0;
			$jumlah_pasien_rawat_jalan_row = $db->get_row("
				SELECT 
					COUNT(*) jumlah
				FROM (
					" . $sql_view . "
				) v
				WHERE 
					uri = 0 AND
					carabayar LIKE '%%' AND
					asuransi LIKE '%%' AND
					pemerintah LIKE '%%' AND
					opsi_gratis NOT LIKE '%Gratis%' AND
					alasan_gratis = '' AND
					(metode_bayar_csv LIKE '%cash%' AND metode_bayar_csv LIKE '%bank%' AND metode_bayar_csv LIKE '%asuransi%') AND
					jumlah_metode_bayar > 1
			");
			if ($jumlah_pasien_rawat_jalan_row != null)
				$jumlah_pasien_rawat_jalan = $jumlah_pasien_rawat_jalan_row->jumlah;
			$jprj_laboratorium = 0;
			$jprj_laboratorium_row = $db->get_row("
				SELECT 
					SUM(laboratorium) jumlah
				FROM (
					" . $sql_view . "
				) v
				WHERE 
					uri = 0 AND
					carabayar LIKE '%%' AND
					asuransi LIKE '%%' AND
					pemerintah LIKE '%%' AND
					opsi_gratis NOT LIKE '%Gratis%' AND
					alasan_gratis = '' AND
					(metode_bayar_csv LIKE '%cash%' AND metode_bayar_csv LIKE '%bank%' AND metode_bayar_csv LIKE '%asuransi%') AND
					jumlah_metode_bayar > 1
			");
			if ($jprj_laboratorium_row != null)
				$jprj_laboratorium = $jprj_laboratorium_row->jumlah == "" ? 0 : $jprj_laboratorium_row->jumlah;
			$jprj_radiologi = 0;
			$jprj_radiologi_row = $db->get_row("
				SELECT 
					SUM(radiologi) jumlah
				FROM (
					" . $sql_view . "
				) v
				WHERE 
					uri = 0 AND
					carabayar LIKE '%%' AND
					asuransi LIKE '%%' AND
					pemerintah LIKE '%%' AND
					opsi_gratis NOT LIKE '%Gratis%' AND
					alasan_gratis = '' AND
					(metode_bayar_csv LIKE '%cash%' AND metode_bayar_csv LIKE '%bank%' AND metode_bayar_csv LIKE '%asuransi%') AND
					jumlah_metode_bayar > 1
			");
			if ($jprj_radiologi_row != null)
				$jprj_radiologi = $jprj_radiologi_row->jumlah == "" ? 0 : $jprj_radiologi_row->jumlah;
			$jprj_lain = 0;
			$jprj_lain_row = $db->get_row("
				SELECT 
					COUNT(*) jumlah
				FROM (
					" . $sql_view . "
				) v
				WHERE 
					laboratorium = 0 AND
					radiologi = 0 AND
					uri = 0 AND
					carabayar LIKE '%%' AND
					asuransi LIKE '%%' AND
					pemerintah LIKE '%%' AND
					opsi_gratis NOT LIKE '%Gratis%' AND
					alasan_gratis = '' AND
					(metode_bayar_csv LIKE '%cash%' AND metode_bayar_csv LIKE '%bank%' AND metode_bayar_csv LIKE '%asuransi%') AND
					jumlah_metode_bayar > 1
			");
			if ($jprj_lain_row != null)
				$jprj_lain = $jprj_lain_row->jumlah;

			$html .= 	"<tr class='data_rl315'>" .
							"<td id='rl315_kode_propinsi'>" . $kode_propinsi . "</td>" .
							"<td id='rl315_kabupaten_kota'>" . $kabupaten_kota . "</td>" .
							"<td id='rl315_kode_rs'>" . $kode_rs . "</td>" .
							"<td id='rl315_nama_rs'>" . $nama_rs . "</td>" .
							"<td id='rl315_tahun'>" . $tahun . "</td>" .
							"<td id='rl315_nomor'>3</td>" .
							"<td id='rl315_cara_pembayaran'>Keringanan (Cost Sharing)</td>" .
							"<td id='rl315_pri_jumlah_pasien_keluar' style='display: none;'>" . $pri_jumlah_pasien_keluar . "</td>" .
							"<td id='rl315_f_pri_jumlah_pasien_keluar'><div align='right'>" . $pri_jumlah_pasien_keluar . "</div></td>" .
							"<td id='rl315_pri_jumlah_lama_dirawat' style='display: none;'>" . $pri_jumlah_lama_dirawat . "</td>" .
							"<td id='rl315_f_pri_jumlah_lama_dirawat'><div align='right'>" . $pri_jumlah_lama_dirawat . "</div></td>" .
							"<td id='rl315_jumlah_pasien_rawat_jalan' style='display: none;'>" . $jumlah_pasien_rawat_jalan . "</td>" .
							"<td id='rl315_f_jumlah_pasien_rawat_jalan'><div align='right'>" . $jumlah_pasien_rawat_jalan . "</div></td>" .
							"<td id='rl315_jprj_laboratorium' style='display: none;'>" . $jprj_laboratorium . "</td>" .
							"<td id='rl315_f_jprj_laboratorium'><div align='right'>" . $jprj_laboratorium . "</div></td>" .
							"<td id='rl315_jprj_radiologi' style='display: none;'>" . $jprj_radiologi . "</td>" .
							"<td id='rl315_f_jprj_radiologi'><div align='right'>" . $jprj_radiologi . "</div></td>" .
							"<td id='rl315_jprj_lain' style='display: none;'>" . $jprj_lain . "</td>" .
							"<td id='rl315_f_jprj_lain'><div align='right'>" . $jprj_lain . "</div></td>" .
						"</tr>";

			$pri_jumlah_pasien_keluar = "";
			$pri_jumlah_pasien_keluar = "";
			$pri_jumlah_lama_dirawat = "";
			$pri_jumlah_lama_dirawat = "";
			$jumlah_pasien_rawat_jalan = "";
			$jumlah_pasien_rawat_jalan = "";
			$jprj_laboratorium = "";
			$jprj_laboratorium = "";
			$jprj_radiologi = "";
			$jprj_radiologi = "";
			$jprj_lain = "";
			$jprj_lain = "";

			$html .= 	"<tr class='data_rl315'>" .
							"<td id='rl315_kode_propinsi'>" . $kode_propinsi . "</td>" .
							"<td id='rl315_kabupaten_kota'>" . $kabupaten_kota . "</td>" .
							"<td id='rl315_kode_rs'>" . $kode_rs . "</td>" .
							"<td id='rl315_nama_rs'>" . $nama_rs . "</td>" .
							"<td id='rl315_tahun'>" . $tahun . "</td>" .
							"<td id='rl315_nomor'>4</td>" .
							"<td id='rl315_cara_pembayaran'>Gratis :</td>" .
							"<td id='rl315_pri_jumlah_pasien_keluar' style='display: none;'>" . $pri_jumlah_pasien_keluar . "</td>" .
							"<td id='rl315_f_pri_jumlah_pasien_keluar'><div align='right'>" . $pri_jumlah_pasien_keluar . "</div></td>" .
							"<td id='rl315_pri_jumlah_lama_dirawat' style='display: none;'>" . $pri_jumlah_lama_dirawat . "</td>" .
							"<td id='rl315_f_pri_jumlah_lama_dirawat'><div align='right'>" . $pri_jumlah_lama_dirawat . "</div></td>" .
							"<td id='rl315_jumlah_pasien_rawat_jalan' style='display: none;'>" . $jumlah_pasien_rawat_jalan . "</td>" .
							"<td id='rl315_f_jumlah_pasien_rawat_jalan'><div align='right'>" . $jumlah_pasien_rawat_jalan . "</div></td>" .
							"<td id='rl315_jprj_laboratorium' style='display: none;'>" . $jprj_laboratorium . "</td>" .
							"<td id='rl315_f_jprj_laboratorium'><div align='right'>" . $jprj_laboratorium . "</div></td>" .
							"<td id='rl315_jprj_radiologi' style='display: none;'>" . $jprj_radiologi . "</td>" .
							"<td id='rl315_f_jprj_radiologi'><div align='right'>" . $jprj_radiologi . "</div></td>" .
							"<td id='rl315_jprj_lain' style='display: none;'>" . $jprj_lain . "</td>" .
							"<td id='rl315_f_jprj_lain'><div align='right'>" . $jprj_lain . "</div></td>" .
						"</tr>";

			$pri_jumlah_pasien_keluar = 0;
			$pri_jumlah_pasien_keluar_row = $db->get_row("
				SELECT 
					COUNT(*) jumlah
				FROM (
					" . $sql_view . "
				) v
				WHERE 
					uri = 1 AND
					opsi_gratis LIKE '%Gratis%' AND
					alasan_gratis LIKE '%Kartu Sehat%'
			");
			if ($pri_jumlah_pasien_keluar_row != null)
				$pri_jumlah_pasien_keluar = $pri_jumlah_pasien_keluar_row->jumlah;
			$pri_jumlah_lama_dirawat = 0;
			$pri_jumlah_lama_dirawat_row = $db->get_row("
				SELECT 
					SUM(los) jumlah
				FROM (
					" . $sql_view . "
				) v
				WHERE 
					uri = 1 AND
					opsi_gratis LIKE '%Gratis%' AND
					alasan_gratis LIKE '%Kartu Sehat%'
			");
			if ($pri_jumlah_lama_dirawat_row != null)
				$pri_jumlah_lama_dirawat = $pri_jumlah_lama_dirawat_row->jumlah == "" ? 0 : $pri_jumlah_lama_dirawat_row->jumlah;
			$jumlah_pasien_rawat_jalan = 0;
			$jumlah_pasien_rawat_jalan_row = $db->get_row("
				SELECT 
					COUNT(*) jumlah
				FROM (
					" . $sql_view . "
				) v
				WHERE 
					uri = 0 AND
					opsi_gratis LIKE '%Gratis%' AND
					alasan_gratis LIKE '%Kartu Sehat%'
			");
			if ($jumlah_pasien_rawat_jalan_row != null)
				$jumlah_pasien_rawat_jalan = $jumlah_pasien_rawat_jalan_row->jumlah;
			$jprj_laboratorium = 0;
			$jprj_laboratorium_row = $db->get_row("
				SELECT 
					SUM(laboratorium) jumlah
				FROM (
					" . $sql_view . "
				) v
				WHERE 
					uri = 0 AND
					opsi_gratis LIKE '%Gratis%' AND
					alasan_gratis LIKE '%Kartu Sehat%'
			");
			if ($jprj_laboratorium_row != null)
				$jprj_laboratorium = $jprj_laboratorium_row->jumlah == "" ? 0 : $jprj_laboratorium_row->jumlah;
			$jprj_radiologi = 0;
			$jprj_radiologi_row = $db->get_row("
				SELECT 
					SUM(radiologi) jumlah
				FROM (
					" . $sql_view . "
				) v
				WHERE 
					uri = 0 AND
					opsi_gratis LIKE '%Gratis%' AND
					alasan_gratis LIKE '%Kartu Sehat%'
			");
			if ($jprj_radiologi_row != null)
				$jprj_radiologi = $jprj_radiologi_row->jumlah == "" ? 0 : $jprj_radiologi_row->jumlah;
			$jprj_lain = 0;
			$jprj_lain_row = $db->get_row("
				SELECT 
					COUNT(*) jumlah
				FROM (
					" . $sql_view . "
				) v
				WHERE 
					laboratorium = 0 AND
					radiologi = 0 AND
					uri = 0 AND
					opsi_gratis LIKE '%Gratis%' AND
					alasan_gratis LIKE '%Kartu Sehat%'
			");
			if ($jprj_lain_row != null)
				$jprj_lain = $jprj_lain_row->jumlah;

			$html .= 	"<tr class='data_rl315'>" .
							"<td id='rl315_kode_propinsi'>" . $kode_propinsi . "</td>" .
							"<td id='rl315_kabupaten_kota'>" . $kabupaten_kota . "</td>" .
							"<td id='rl315_kode_rs'>" . $kode_rs . "</td>" .
							"<td id='rl315_nama_rs'>" . $nama_rs . "</td>" .
							"<td id='rl315_tahun'>" . $tahun . "</td>" .
							"<td id='rl315_nomor'>4.1</td>" .
							"<td id='rl315_cara_pembayaran'>Kartu Sehat</td>" .
							"<td id='rl315_pri_jumlah_pasien_keluar' style='display: none;'>" . $pri_jumlah_pasien_keluar . "</td>" .
							"<td id='rl315_f_pri_jumlah_pasien_keluar'><div align='right'>" . $pri_jumlah_pasien_keluar . "</div></td>" .
							"<td id='rl315_pri_jumlah_lama_dirawat' style='display: none;'>" . $pri_jumlah_lama_dirawat . "</td>" .
							"<td id='rl315_f_pri_jumlah_lama_dirawat'><div align='right'>" . $pri_jumlah_lama_dirawat . "</div></td>" .
							"<td id='rl315_jumlah_pasien_rawat_jalan' style='display: none;'>" . $jumlah_pasien_rawat_jalan . "</td>" .
							"<td id='rl315_f_jumlah_pasien_rawat_jalan'><div align='right'>" . $jumlah_pasien_rawat_jalan . "</div></td>" .
							"<td id='rl315_jprj_laboratorium' style='display: none;'>" . $jprj_laboratorium . "</td>" .
							"<td id='rl315_f_jprj_laboratorium'><div align='right'>" . $jprj_laboratorium . "</div></td>" .
							"<td id='rl315_jprj_radiologi' style='display: none;'>" . $jprj_radiologi . "</td>" .
							"<td id='rl315_f_jprj_radiologi'><div align='right'>" . $jprj_radiologi . "</div></td>" .
							"<td id='rl315_jprj_lain' style='display: none;'>" . $jprj_lain . "</td>" .
							"<td id='rl315_f_jprj_lain'><div align='right'>" . $jprj_lain . "</div></td>" .
						"</tr>";

			$pri_jumlah_pasien_keluar = 0;
			$pri_jumlah_pasien_keluar_row = $db->get_row("
				SELECT 
					COUNT(*) jumlah
				FROM (
					" . $sql_view . "
				) v
				WHERE 
					uri = 1 AND
					opsi_gratis LIKE '%Gratis%' AND
					alasan_gratis LIKE '%Keterangan Tidak Mampu%'
			");
			if ($pri_jumlah_pasien_keluar_row != null)
				$pri_jumlah_pasien_keluar = $pri_jumlah_pasien_keluar_row->jumlah;
			$pri_jumlah_lama_dirawat = 0;
			$pri_jumlah_lama_dirawat_row = $db->get_row("
				SELECT 
					SUM(los) jumlah
				FROM (
					" . $sql_view . "
				) v
				WHERE 
					uri = 1 AND
					opsi_gratis LIKE '%Gratis%' AND
					alasan_gratis LIKE '%Keterangan Tidak Mampu%'
			");
			if ($pri_jumlah_lama_dirawat_row != null)
				$pri_jumlah_lama_dirawat = $pri_jumlah_lama_dirawat_row->jumlah == "" ? 0 : $pri_jumlah_lama_dirawat_row->jumlah;
			$jumlah_pasien_rawat_jalan = 0;
			$jumlah_pasien_rawat_jalan_row = $db->get_row("
				SELECT 
					COUNT(*) jumlah
				FROM (
					" . $sql_view . "
				) v
				WHERE 
					uri = 0 AND
					opsi_gratis LIKE '%Gratis%' AND
					alasan_gratis LIKE '%Keterangan Tidak Mampu%'
			");
			if ($jumlah_pasien_rawat_jalan_row != null)
				$jumlah_pasien_rawat_jalan = $jumlah_pasien_rawat_jalan_row->jumlah;
			$jprj_laboratorium = 0;
			$jprj_laboratorium_row = $db->get_row("
				SELECT 
					SUM(laboratorium) jumlah
				FROM (
					" . $sql_view . "
				) v
				WHERE 
					uri = 0 AND
					opsi_gratis LIKE '%Gratis%' AND
					alasan_gratis LIKE '%Keterangan Tidak Mampu%'
			");
			if ($jprj_laboratorium_row != null)
				$jprj_laboratorium = $jprj_laboratorium_row->jumlah == "" ? 0 : $jprj_laboratorium_row->jumlah;
			$jprj_radiologi = 0;
			$jprj_radiologi_row = $db->get_row("
				SELECT 
					SUM(radiologi) jumlah
				FROM (
					" . $sql_view . "
				) v
				WHERE 
					uri = 0 AND
					opsi_gratis LIKE '%Gratis%' AND
					alasan_gratis LIKE '%Keterangan Tidak Mampu%'
			");
			if ($jprj_radiologi_row != null)
				$jprj_radiologi = $jprj_radiologi_row->jumlah == "" ? 0 : $jprj_radiologi_row->jumlah;
			$jprj_lain = 0;
			$jprj_lain_row = $db->get_row("
				SELECT 
					COUNT(*) jumlah
				FROM (
					" . $sql_view . "
				) v
				WHERE 
					laboratorium = 0 AND
					radiologi = 0 AND
					uri = 0 AND
					opsi_gratis LIKE '%Gratis%' AND
					alasan_gratis LIKE '%Keterangan Tidak Mampu%'
			");
			if ($jprj_lain_row != null)
				$jprj_lain = $jprj_lain_row->jumlah;

			$html .= 	"<tr class='data_rl315'>" .
							"<td id='rl315_kode_propinsi'>" . $kode_propinsi . "</td>" .
							"<td id='rl315_kabupaten_kota'>" . $kabupaten_kota . "</td>" .
							"<td id='rl315_kode_rs'>" . $kode_rs . "</td>" .
							"<td id='rl315_nama_rs'>" . $nama_rs . "</td>" .
							"<td id='rl315_tahun'>" . $tahun . "</td>" .
							"<td id='rl315_nomor'>4.2</td>" .
							"<td id='rl315_cara_pembayaran'>Keterangan Tidak Mampu</td>" .
							"<td id='rl315_pri_jumlah_pasien_keluar' style='display: none;'>" . $pri_jumlah_pasien_keluar . "</td>" .
							"<td id='rl315_f_pri_jumlah_pasien_keluar'><div align='right'>" . $pri_jumlah_pasien_keluar . "</div></td>" .
							"<td id='rl315_pri_jumlah_lama_dirawat' style='display: none;'>" . $pri_jumlah_lama_dirawat . "</td>" .
							"<td id='rl315_f_pri_jumlah_lama_dirawat'><div align='right'>" . $pri_jumlah_lama_dirawat . "</div></td>" .
							"<td id='rl315_jumlah_pasien_rawat_jalan' style='display: none;'>" . $jumlah_pasien_rawat_jalan . "</td>" .
							"<td id='rl315_f_jumlah_pasien_rawat_jalan'><div align='right'>" . $jumlah_pasien_rawat_jalan . "</div></td>" .
							"<td id='rl315_jprj_laboratorium' style='display: none;'>" . $jprj_laboratorium . "</td>" .
							"<td id='rl315_f_jprj_laboratorium'><div align='right'>" . $jprj_laboratorium . "</div></td>" .
							"<td id='rl315_jprj_radiologi' style='display: none;'>" . $jprj_radiologi . "</td>" .
							"<td id='rl315_f_jprj_radiologi'><div align='right'>" . $jprj_radiologi . "</div></td>" .
							"<td id='rl315_jprj_lain' style='display: none;'>" . $jprj_lain . "</td>" .
							"<td id='rl315_f_jprj_lain'><div align='right'>" . $jprj_lain . "</div></td>" .
						"</tr>";

			$pri_jumlah_pasien_keluar = 0;
			$pri_jumlah_pasien_keluar_row = $db->get_row("
				SELECT 
					COUNT(*) jumlah
				FROM (
					" . $sql_view . "
				) v
				WHERE 
					uri = 1 AND
					opsi_gratis LIKE '%Gratis%' AND
					alasan_gratis LIKE '%Lain-Lain%'
			");
			if ($pri_jumlah_pasien_keluar_row != null)
				$pri_jumlah_pasien_keluar = $pri_jumlah_pasien_keluar_row->jumlah;
			$pri_jumlah_lama_dirawat = 0;
			$pri_jumlah_lama_dirawat_row = $db->get_row("
				SELECT 
					SUM(los) jumlah
				FROM (
					" . $sql_view . "
				) v
				WHERE 
					uri = 1 AND
					opsi_gratis LIKE '%Gratis%' AND
					alasan_gratis LIKE '%Lain-Lain%'
			");
			if ($pri_jumlah_lama_dirawat_row != null)
				$pri_jumlah_lama_dirawat = $pri_jumlah_lama_dirawat_row->jumlah == "" ? 0 : $pri_jumlah_lama_dirawat_row->jumlah;
			$jumlah_pasien_rawat_jalan = 0;
			$jumlah_pasien_rawat_jalan_row = $db->get_row("
				SELECT 
					COUNT(*) jumlah
				FROM (
					" . $sql_view . "
				) v
				WHERE 
					uri = 0 AND
					opsi_gratis LIKE '%Gratis%' AND
					alasan_gratis LIKE '%Lain-Lain%'
			");
			if ($jumlah_pasien_rawat_jalan_row != null)
				$jumlah_pasien_rawat_jalan = $jumlah_pasien_rawat_jalan_row->jumlah;
			$jprj_laboratorium = 0;
			$jprj_laboratorium_row = $db->get_row("
				SELECT 
					SUM(laboratorium) jumlah
				FROM (
					" . $sql_view . "
				) v
				WHERE 
					uri = 0 AND
					opsi_gratis LIKE '%Gratis%' AND
					alasan_gratis LIKE '%Lain-Lain%'
			");
			if ($jprj_laboratorium_row != null)
				$jprj_laboratorium = $jprj_laboratorium_row->jumlah == "" ? 0 : $jprj_laboratorium_row->jumlah;
			$jprj_radiologi = 0;
			$jprj_radiologi_row = $db->get_row("
				SELECT 
					SUM(radiologi) jumlah
				FROM (
					" . $sql_view . "
				) v
				WHERE 
					uri = 0 AND
					opsi_gratis LIKE '%Gratis%' AND
					alasan_gratis LIKE '%Lain-Lain%'
			");
			if ($jprj_radiologi_row != null)
				$jprj_radiologi = $jprj_radiologi_row->jumlah == "" ? 0 : $jprj_radiologi_row->jumlah;
			$jprj_lain = 0;
			$jprj_lain_row = $db->get_row("
				SELECT 
					COUNT(*) jumlah
				FROM (
					" . $sql_view . "
				) v
				WHERE 
					laboratorium = 0 AND
					radiologi = 0 AND
					uri = 0 AND
					opsi_gratis LIKE '%Gratis%' AND
					alasan_gratis LIKE '%Lain-Lain%'
			");
			if ($jprj_lain_row != null)
				$jprj_lain = $jprj_lain_row->jumlah;

			$html .= 	"<tr class='data_rl315'>" .
							"<td id='rl315_kode_propinsi'>" . $kode_propinsi . "</td>" .
							"<td id='rl315_kabupaten_kota'>" . $kabupaten_kota . "</td>" .
							"<td id='rl315_kode_rs'>" . $kode_rs . "</td>" .
							"<td id='rl315_nama_rs'>" . $nama_rs . "</td>" .
							"<td id='rl315_tahun'>" . $tahun . "</td>" .
							"<td id='rl315_nomor'>4.3</td>" .
							"<td id='rl315_cara_pembayaran'>Lain-Lain</td>" .
							"<td id='rl315_pri_jumlah_pasien_keluar' style='display: none;'>" . $pri_jumlah_pasien_keluar . "</td>" .
							"<td id='rl315_f_pri_jumlah_pasien_keluar'><div align='right'>" . $pri_jumlah_pasien_keluar . "</div></td>" .
							"<td id='rl315_pri_jumlah_lama_dirawat' style='display: none;'>" . $pri_jumlah_lama_dirawat . "</td>" .
							"<td id='rl315_f_pri_jumlah_lama_dirawat'><div align='right'>" . $pri_jumlah_lama_dirawat . "</div></td>" .
							"<td id='rl315_jumlah_pasien_rawat_jalan' style='display: none;'>" . $jumlah_pasien_rawat_jalan . "</td>" .
							"<td id='rl315_f_jumlah_pasien_rawat_jalan'><div align='right'>" . $jumlah_pasien_rawat_jalan . "</div></td>" .
							"<td id='rl315_jprj_laboratorium' style='display: none;'>" . $jprj_laboratorium . "</td>" .
							"<td id='rl315_f_jprj_laboratorium'><div align='right'>" . $jprj_laboratorium . "</div></td>" .
							"<td id='rl315_jprj_radiologi' style='display: none;'>" . $jprj_radiologi . "</td>" .
							"<td id='rl315_f_jprj_radiologi'><div align='right'>" . $jprj_radiologi . "</div></td>" .
							"<td id='rl315_jprj_lain' style='display: none;'>" . $jprj_lain . "</td>" .
							"<td id='rl315_f_jprj_lain'><div align='right'>" . $jprj_lain . "</div></td>" .
						"</tr>";

			$data['html'] = $html;
			echo json_encode($data);
		} else if ($_POST['command'] == "export_xls") {
			require_once ("smis-libs-out/php-excel/PHPExcel.php");

		    $file = new PHPExcel ();
		    $file->getProperties ()->setCreator ( "PT. Inovasi Ide Utama" );
		    $file->getProperties ()->setTitle ( "RL 3.15 - Cara Bayar" );
		    $file->getProperties ()->setSubject ( "RL 3.15 - Cara Bayar" );
		    $file->getProperties ()->setDescription ( "RL 3.15 - Cara Bayar Generated by System" );
		    $file->getProperties ()->setKeywords ( "RL 3.15 - Cara Bayar" );
		    $file->getProperties ()->setCategory ( "RL 3.15 - Cara Bayar" );

		    $sheet = $file->getActiveSheet ();
		    $i = 1;

		    $sheet->mergeCells("A".$i.":M".$i)->setCellValue("A".$i,"RL 3.15 - Cara Bayar");
		    $sheet->getStyle("A".$i.":M".$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		    $sheet->getStyle("A".$i)->getFont()->setBold(true);
		    $i = $i + 2;

		    $border_start = $i;
		    
		    $sheet->mergeCells("A".$i.":A".($i + 2))->setCellValue("A".$i,"Kode Propinsi");
		    $sheet->mergeCells("B".$i.":B".($i + 2))->setCellValue("B".$i,"Kab/Kota");
		    $sheet->mergeCells("C".$i.":C".($i + 2))->setCellValue("C".$i,"Kode RS");
		    $sheet->mergeCells("D".$i.":D".($i + 2))->setCellValue("D".$i,"Nama RS");
		    $sheet->mergeCells("E".$i.":E".($i + 2))->setCellValue("E".$i,"Tahun");
		    $sheet->mergeCells("F".$i.":F".($i + 1))->setCellValue("F".$i,"No.");
		    $sheet->setCellValue("F".($i + 2),"1");
		    $sheet->mergeCells("G".$i.":G".($i + 1))->setCellValue("G".$i,"Cara Pembayaran");
		    $sheet->setCellValue("G".($i + 2),"2");
		    $sheet->mergeCells("H".$i.":I".$i)->setCellValue("H".$i,"Pasien Rawat Inap");
		    $sheet->setCellValue("H".($i + 1),"Jumlah Pasien Keluar");
		    $sheet->setCellValue("H".($i + 2),"3");
		    $sheet->setCellValue("I".($i + 1),"Jumlah Lama Dirawat");
		    $sheet->setCellValue("I".($i + 2),"4");
		    $sheet->mergeCells("J".$i.":J".($i + 1))->setCellValue("J".$i,"Jumlah Pasien Rawat Jalan");
		    $sheet->setCellValue("J".($i + 2),"5");
		    $sheet->mergeCells("K".$i.":M".$i)->setCellValue("K".$i,"Jumlah Pasien Rawat Jalan");
		    $sheet->setCellValue("K".($i + 1),"Laboratorium");
		    $sheet->setCellValue("K".($i + 2),"6");
		    $sheet->setCellValue("L".($i + 1),"Radiologi");
		    $sheet->setCellValue("L".($i + 2),"7");
		    $sheet->setCellValue("M".($i + 1),"Lain-Lain");
		    $sheet->setCellValue("M".($i + 2),"8");
		    $sheet->getStyle("A".$i.":M".($i + 2))->getFont()->setBold(true);
		    $sheet->getStyle("A".$i.":M".($i + 2))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		    $sheet->getStyle("A".$i.":M".($i + 2))->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
		    $i = $i + 2;

		    $data = json_decode($_POST['detail']);
		    foreach ($data as $d) {
		    	$i = $i + 1;
		    	$sheet->setCellValue("A".$i,$d->kode_propinsi);
				$sheet->setCellValue("B".$i,$d->kabupaten_kota);
				$sheet->setCellValue("C".$i,$d->kode_rs);
				$sheet->setCellValue("D".$i,$d->nama_rs);
				$sheet->setCellValue("E".$i,$d->tahun);
				$sheet->setCellValue("F".$i,"'" . $d->nomor);
				$sheet->setCellValue("G".$i,$d->cara_pembayaran);
				$sheet->setCellValue("H".$i,$d->pri_jumlah_pasien_keluar);
				$sheet->setCellValue("I".$i,$d->pri_jumlah_lama_dirawat);
				$sheet->setCellValue("J".$i,$d->jumlah_pasien_rawat_jalan);
				$sheet->setCellValue("K".$i,$d->jprj_laboratorium);
				$sheet->setCellValue("L".$i,$d->jprj_radiologi);
				$sheet->setCellValue("M".$i,$d->jprj_lain);
			}

		    $thin = array ();
		    $thin['borders']=array();
		    $thin['borders']['allborders']=array();
		    $thin['borders']['allborders']['style']=PHPExcel_Style_Border::BORDER_THIN ;
		    $sheet->getStyle ( "A".$border_start.":M".$i )->applyFromArray ($thin);

		    foreach (range('A', 'M') as $columnID) {
		        $sheet->getColumnDimension($columnID)->setAutoSize(true);
		    }

		    header ( 'Content-Type: application/vnd.ms-excel' );
		    header ( 'Content-Disposition: attachment;filename="RL 3.15 - Cara Bayar.xls"' );
		    header ( 'Cache-Control: max-age=0' );
		    $writer = PHPExcel_IOFactory::createWriter ( $file, 'Excel5' );
		    $writer->save ( 'php://output' );
		}

		return;
	}
	$modal = $uitable->getModal();
	$form = $modal->getForm();
	$form->setTitle("RL 3.15 - Cara Bayar");

	$bulan_option = new OptionBuilder();
	$bulan_option->add("Semua", "%%", "1");
	$bulan_option->add("Januari", "1");
	$bulan_option->add("Februari", "2");
	$bulan_option->add("Maret", "3");
	$bulan_option->add("April", "4");
	$bulan_option->add("Mei", "5");
	$bulan_option->add("Juni", "6");
	$bulan_option->add("Juli", "7");
	$bulan_option->add("Agustus", "8");
	$bulan_option->add("September", "9");
	$bulan_option->add("Oktober", "10");
	$bulan_option->add("Nopember", "11");
	$bulan_option->add("Desember", "12");
	$bulan_select = new Select("rl315_bulan_filter", "rl315_bulan_filter", $bulan_option->getContent());
	$form->addElement("Bulan", $bulan_select);
	$tahun_rows = $db->get_result("
		SELECT tahun
		FROM (
			SELECT DISTINCT YEAR(tanggal) tahun
			FROM smis_rg_layananpasien
			WHERE prop = '' AND YEAR(tanggal) > 0
			UNION
			SELECT DISTINCT YEAR(tanggal_pulang) tahun
			FROM smis_rg_layananpasien
			WHERE prop = '' AND YEAR(tanggal_pulang) > 0
		) v
		ORDER BY tahun ASC
	");
	$tahun_option = new OptionBuilder();
	if ($tahun_rows != null) {
		foreach ($tahun_rows as $tahun_row) {
			if (date("Y") == $tahun_row->tahun)
				$tahun_option->add($tahun_row->tahun, $tahun_row->tahun, "1");
			else
				$tahun_option->add($tahun_row->tahun, $tahun_row->tahun);
		}
	}
	$tahun_select = new Select("rl315_tahun_filter", "rl315_tahun_filter", $tahun_option->getContent());
	$form->addElement("Tahun", $tahun_select);
	$excel_button = new Button("", "", "");
	$excel_button 	->addClass("btn-primary")
					->setIsButton(Button::$ICONIC)
					->setIcon("fa fa-file-excel-o")
					->setAction("rl315.excel()");
	$process_button = new Button("", "", "");
	$process_button	->addClass("btn-inverse")
					-> setIsButton(Button::$ICONIC)
					->setIcon("fa fa-circle-o")
					->setAction("rl315.view()");
	$button_group = new ButtonGroup("noprint");
	$button_group->addButton($process_button);
	$button_group->addButton($excel_button);
	$form->addElement("", $button_group);

	echo $form->getHtml();
	echo $uitable->getHtml();
	echo addJS("framework/smis/js/table_action.js");
	echo addJS("medical_record/resource/js/rl315.js", false);
?>