<?php
    require_once("smis-base/smis-include-service-consumer.php");
    global $db;

    $head = array('Kode RS', 'Kode Propinsi', 'Kab/Kota', 'Nama RS', 'No. Urut', 'Kualifikasi Pendidikan', 'Keadaan Laki-Laki', 'Keadaan Perempuan', 'Kebutuhan Laki-Laki', 'Kebutuhan Perempuan', 'Kekurangan Laki-Laki', 'Kekurangan Perempuan', 'Jumlah Keadaan', 'Jumlah Kebutuhan', 'Jumlah Kekurangan');
    $uitable = new Table($head, "", null, true);
    $uitable->setName("rl2");
    $uitable->setAction(false);
    $uitable->setFooterVisible(false);

    if (isset($_POST ['command'])) {
        $adapter = new SimpleAdapter(true, "No. Urut");
        $adapter->add("Kode RS", "koders");
        $adapter->add("Kode Propinsi", "kode_provinsi");
        $adapter->add("Kab/Kota", "kota_kabupaten");
        $adapter->add("Nama RS", "nama_rs");
        $adapter->add("Kualifikasi Pendidikan", "pendidikan");
        $adapter->add("Keadaan Laki-Laki", "tersedia_lk", "number");
        $adapter->add("Keadaan Perempuan", "tersedia_pr", "number");
        $adapter->add("Kebutuhan Laki-Laki", "butuh_lk", "number");
        $adapter->add("Kebutuhan Perempuan", "butuh_pr", "number");
        $adapter->add("Kekurangan Laki-Laki", "kurang_lk", "number");
        $adapter->add("Kekurangan Perempuan", "kurang_pr", "number");
        $adapter->add("Jumlah Keadaan", "jumlah_keadaan", "number");
        $adapter->add("Jumlah Kebutuhan", "jumlah_kebutuhan", "number");
        $adapter->add("Jumlah Kekurangan", "jumlah_kekurangan", "number");

        $dbtable = new DBTable($db, "smis_mr_data_dasar_rs");
        $dbtable->setShowAll(true);
        $query_value = "
            SELECT w.kode_provinsi, w.kota_kabupaten, w.koders, w.nama_rs, v.*, (v.tersedia_pr + v.tersedia_lk) jumlah_keadaan, (v.butuh_pr + v.butuh_lk) jumlah_kebutuhan, (v.kurang_pr + v.kurang_lk) jumlah_kekurangan
            FROM smis_vhrd_pendidikan v, smis_mr_data_dasar_rs w
            WHERE v.prop = ''
            ORDER BY v.pendidikan ASC
        ";
        $query_count = "
            SELECT COUNT(*)
            FROM (
                " . $query_value . "
            ) v
        ";
        $dbtable->setPreferredQuery(true, $query_value, $query_count);

        $dbresponder = new DBResponder($dbtable, $uitable, $adapter);
        $data = $dbresponder->command($_POST['command']);
        echo json_encode($data);
        return;
    }
    $modal = $uitable->getModal();
    $form = $modal->getForm();
    $form->setTitle("RL 2 - Ketenagaan");

    $excel_button = new Button("", "", "");
    $excel_button   ->addClass("btn-primary")
                    ->setIsButton(Button::$ICONIC)
                    ->setIcon("fa fa-file-excel-o")
                    ->setAction("rl2.excel()");
    $process_button = new Button("", "", "");
    $process_button ->addClass("btn-inverse")
                    -> setIsButton(Button::$ICONIC)
                    ->setIcon("fa fa-circle-o")
                    ->setAction("rl2.view()");
    $button_group = new ButtonGroup("noprint");
    $button_group->addButton($process_button);
    $button_group->addButton($excel_button);
    $form->addElement("", $button_group);

    echo $form->getHtml();
    echo $uitable->getHtml();
    echo $modal->getHtml();
    echo addJS("framework/smis/js/table_action.js");
    echo addJS("medical_record/resource/js/rl2.js", false);
?>