<?php
	require_once("smis-base/smis-include-service-consumer.php");
	require_once("medical_record/library/MedicalRecordLibrary.php");
	global $db;

	if (isset($_POST ['command'])) {
		$data_diagnosa = MedicalRecordLibrary::getDataDiagnosaSebabLuar();
		if ($_POST['command'] == "get_jumlah") {
			$data['jumlah'] = count($data_diagnosa);
			echo json_encode($data);
		} else if ($_POST['command'] == "get_data") {
			$num = $_POST['num'];
			$tahun = $_POST['tahun'];
			$sebab = $data_diagnosa[$num]['sebab'];
			$dtd = $data_diagnosa[$num]['dtd'];
			$kode_icd = $data_diagnosa[$num]['kode_icd'];

			$kode_rs = "";
			$nama_rs = "";
			$kode_propinsi = "";
			$kabupaten_kota = "";
			$info_rs_row = $db->get_row("
				SELECT 
					*
				FROM 
					smis_mr_data_dasar_rs
				WHERE 
					prop = ''
				LIMIT 
					0, 1
			");
			if ($info_rs_row != null) {
				$kode_rs = $info_rs_row->koders;
				$nama_rs = $info_rs_row->nama_rs;
				$kode_propinsi = $info_rs_row->kode_provinsi;
				$kabupaten_kota = $info_rs_row->kota_kabupaten;
			}

			$jml_pasien_0hr_6hr_l = 0;
			$jml_pasien_0hr_6hr_p = 0;
			$jml_pasien_6hr_28hr_l = 0;
			$jml_pasien_6hr_28hr_p = 0;
			$jml_pasien_28hr_1th_l = 0;
			$jml_pasien_28hr_1th_p = 0;
			$jml_pasien_1th_4th_l = 0;
			$jml_pasien_1th_4th_p = 0;
			$jml_pasien_4th_14th_l = 0;
			$jml_pasien_4th_14th_p = 0;
			$jml_pasien_14th_24th_l = 0;
			$jml_pasien_14th_24th_p = 0;
			$jml_pasien_24th_44th_l = 0;
			$jml_pasien_24th_44th_p = 0;
			$jml_pasien_44th_64th_l = 0;
			$jml_pasien_44th_64th_p = 0;
			$jml_pasien_l64th_l = 0;
			$jml_pasien_l64th_p = 0;
			$jml_pasien_keluar_l = 0;
			$jml_pasien_keluar_p = 0;
			$jml_pasien_keluar = 0;
			$jml_pasien_keluar_mati = 0;
			$html = "";

			$data_diagnosa_rows = $db->get_result("
				SELECT 
					a.tanggal, DATEDIFF(b.tanggal, c.tgl_lahir) usia_hari, a.sebab_diagnosa_external_cause, CASE WHEN a.jk = 0 THEN 'L' ELSE 'P' END jk, b.carapulang
				FROM
					(smis_mr_diagnosa a LEFT JOIN smis_rg_layananpasien b ON a.noreg_pasien = b.id) LEFT JOIN smis_rg_patient c ON b.nrm = c.id
				WHERE
					a.prop = '' AND b.prop = '' AND c.prop = '' AND YEAR(b.tanggal_pulang) = '" . $tahun . "' AND a.urji = '1' AND a.sebab_diagnosa_external_cause LIKE '" . $sebab . "' AND b.carapulang NOT LIKE 'Tidak Datang' AND b.carapulang NOT LIKE 'Rawat Inap' AND b.carapulang NOT LIKE ''
			");
			if ($data_diagnosa_rows != null) {
				foreach ($data_diagnosa_rows as $ddr) {
					if ($ddr->jk == "L") {
						if ($ddr->usia_hari <= 6)
							$jml_pasien_0hr_6hr_l += 1;
						else if ($ddr->usia_hari <= 28)
							$jml_pasien_6hr_28hr_l += 1;
						else if (($ddr->usia_hari / 365) <= 1)
							$jml_pasien_28hr_1th_l += 1;
						else if (($ddr->usia_hari / 365) <= 4)
							$jml_pasien_1th_4th_l += 1;
						else if (($ddr->usia_hari / 365) <= 14)
							$jml_pasien_4th_14th_l += 1;
						else if (($ddr->usia_hari / 365) <= 24)
							$jml_pasien_14th_24th_l += 1;
						else if (($ddr->usia_hari / 365) <= 44)
							$jml_pasien_24th_44th_l += 1;
						else if (($ddr->usia_hari / 365) <= 64)
							$jml_pasien_44th_64th_l += 1;
						else
							$jml_pasien_l64th_l += 1;
					} else if ($ddr->jk == "P") {
						if ($ddr->usia_hari <= 6)
							$jml_pasien_0hr_6hr_p += 1;
						else if ($ddr->usia_hari <= 28)
							$jml_pasien_6hr_28hr_p += 1;
						else if (($ddr->usia_hari / 365) <= 1)
							$jml_pasien_28hr_1th_p += 1;
						else if (($ddr->usia_hari / 365) <= 4)
							$jml_pasien_1th_4th_p += 1;
						else if (($ddr->usia_hari / 365) <= 14)
							$jml_pasien_4th_14th_p += 1;
						else if (($ddr->usia_hari / 365) <= 24)
							$jml_pasien_14th_24th_p += 1;
						else if (($ddr->usia_hari / 365) <= 44)
							$jml_pasien_24th_44th_p += 1;
						else if (($ddr->usia_hari / 365) <= 64)
							$jml_pasien_44th_64th_p += 1;
						else
							$jml_pasien_l64th_p += 1;
					}

					$jml_pasien_keluar += 1;
					if ($ddr->jk == "L")
						$jml_pasien_keluar_l += 1;
					else if ($ddr->jk == "P")
						$jml_pasien_keluar_p += 1;

					if (strpos($ddr->carapulang, "Dipulangkan Mati") !== false)
						$jml_pasien_keluar_mati += 1;
				}				
			}
			$html = "
				<tr>
					<td id='rl4a_2_kode_propinsi'><small>" . $kode_propinsi . "</small></td>
					<td id='rl4a_2_kabupaten_kota'><small>" . $kabupaten_kota . "</small></td>
					<td id='rl4a_2_kode_rs'><small>" . $kode_rs . "</small></td>
					<td id='rl4a_2_nama_rs'><small>" . $nama_rs . "</small></td>
					<td id='rl4a_2_tahun'><small>" . $tahun . "</small></td>
					<td id='rl4a_2_nomor'><small>" . ($num + 1) . "</small></td>
					<td id='rl4a_2_dtd'><small>" . $dtd . "</small></td>
					<td id='rl4a_2_kode_icd'><small>" . $kode_icd . "</small></td>
					<td id='rl4a_2_sebab'><small>" . $sebab . "</small></td>
					<td id='rl4a_2_jml_pasien_0hr_6hr_l'><small>" . $jml_pasien_0hr_6hr_l . "</small></td>
					<td id='rl4a_2_jml_pasien_0hr_6hr_p'><small>" . $jml_pasien_0hr_6hr_p . "</small></td>
					<td id='rl4a_2_jml_pasien_6hr_28hr_l'><small>" . $jml_pasien_6hr_28hr_l . "</small></td>
					<td id='rl4a_2_jml_pasien_6hr_28hr_p'><small>" . $jml_pasien_6hr_28hr_p . "</small></td>
					<td id='rl4a_2_jml_pasien_28hr_1th_l'><small>" . $jml_pasien_28hr_1th_l . "</small></td>
					<td id='rl4a_2_jml_pasien_28hr_1th_p'><small>" . $jml_pasien_28hr_1th_p . "</small></td>
					<td id='rl4a_2_jml_pasien_1th_4th_l'><small>" . $jml_pasien_1th_4th_l . "</small></td>
					<td id='rl4a_2_jml_pasien_1th_4th_p'><small>" . $jml_pasien_1th_4th_p . "</small></td>
					<td id='rl4a_2_jml_pasien_4th_14th_l'><small>" . $jml_pasien_4th_14th_l . "</small></td>
					<td id='rl4a_2_jml_pasien_4th_14th_p'><small>" . $jml_pasien_4th_14th_p . "</small></td>
					<td id='rl4a_2_jml_pasien_14th_24th_l'><small>" . $jml_pasien_14th_24th_l . "</small></td>
					<td id='rl4a_2_jml_pasien_14th_24th_p'><small>" . $jml_pasien_14th_24th_p . "</small></td>
					<td id='rl4a_2_jml_pasien_24th_44th_l'><small>" . $jml_pasien_24th_44th_l . "</small></td>
					<td id='rl4a_2_jml_pasien_24th_44th_p'><small>" . $jml_pasien_24th_44th_p . "</small></td>
					<td id='rl4a_2_jml_pasien_44th_64th_l'><small>" . $jml_pasien_44th_64th_l . "</small></td>
					<td id='rl4a_2_jml_pasien_44th_64th_p'><small>" . $jml_pasien_44th_64th_p . "</small></td>
					<td id='rl4a_2_jml_pasien_l64th_l'><small>" . $jml_pasien_l64th_l . "</small></td>
					<td id='rl4a_2_jml_pasien_l64th_p'><small>" . $jml_pasien_l64th_p . "</small></td>
					<td id='rl4a_2_jml_pasien_keluar_l'><small>" . $jml_pasien_keluar_l . "</small></td>
					<td id='rl4a_2_jml_pasien_keluar_p'><small>" . $jml_pasien_keluar_p . "</small></td>
					<td id='rl4a_2_jml_pasien_keluar'><small>" . $jml_pasien_keluar . "</small></td>
					<td id='rl4a_2_jml_pasien_keluar_mati'><small>" . $jml_pasien_keluar_mati . "</small></td>
				</tr>
			";
			$data['kode_icd'] = $kode_icd;
			$data['dtd'] = $dtd;
			$data['sebab'] = $sebab;
			$data['html'] = $html;
			echo json_encode($data);
		} else if ($_POST['command'] == "export_xls") {
			require_once ("smis-libs-out/php-excel/PHPExcel.php");

		    $file = new PHPExcel ();
		    $file->getProperties ()->setCreator ( "PT. Inovasi Ide Utama" );
		    $file->getProperties ()->setTitle ( "RL 4A - Penyakit Rawat Inap (Sebab Luar)" );
		    $file->getProperties ()->setSubject ( "RL 4A - Penyakit Rawat Inap (Sebab Luar)" );
		    $file->getProperties ()->setDescription ( "RL 4A - Penyakit Rawat Inap (Sebab Luar) Generated by System" );
		    $file->getProperties ()->setKeywords ( "RL 4A - Penyakit Rawat Inap (Sebab Luar)" );
		    $file->getProperties ()->setCategory ( "RL 4A - Penyakit Rawat Inap (Sebab Luar)" );

		    $sheet = $file->getActiveSheet ();
		    $sheet->setTitle("RL 4A - Penyakit RI Sebab Luar");
		    $i = 1;

		    $sheet->mergeCells("A".$i.":AE".$i)->setCellValue("A".$i,"RL 4A - Penyakit Rawat Inap (Sebab Luar)");
		    $sheet->getStyle("A".$i.":AE".$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		    $sheet->getStyle("A".$i)->getFont()->setBold(true);
		    $i = $i + 2;

		    $border_start = $i;
		    $sheet->setCellValue("A".$i,"KODE PROPINSI");
		    $sheet->setCellValue("B".$i,"KAB/KOTA");
		    $sheet->setCellValue("C".$i,"KODE RS");
		    $sheet->setCellValue("D".$i,"NAMA RS");
		    $sheet->setCellValue("E".$i,"TAHUN");
		    $sheet->setCellValue("F".$i,"NO.");
		    $sheet->setCellValue("G".$i,"NO. DTD");
		    $sheet->setCellValue("H".$i,"NO. DAFTAR TERPERINCI");
		    $sheet->setCellValue("I".$i,"GOLONGAN SEBAB PENYAKIT");
		    $sheet->setCellValue("J".$i,"JMLH PAS. YG HIDUP & MATI MENURUT GOL UMUR & JSEX_0-<=6hr_L");
		    $sheet->setCellValue("K".$i,"JMLH PAS. YG HIDUP & MATI MENURUT GOL UMUR & JSEX_0-<=6hr_P");
		    $sheet->setCellValue("L".$i,"JMLH PAS. YG HIDUP & MATI MENURUT GOL UMUR & SEX_>6-<=28hr_L");
		    $sheet->setCellValue("M".$i,"JMLH PAS. YG HIDUP & MATI MENURUT GOL UMUR & SEX_>6-<=28hr_P");
		    $sheet->setCellValue("N".$i,"JMLH PAS. YG HIDUP & MATI MENURUT GOL UMUR & SEX_>28hr-<=1th_L");
		    $sheet->setCellValue("O".$i,"JMLH PAS. YG HIDUP & MATI MENURUT GOL UMUR & SEX_>28hr-<=1th_P");
		    $sheet->setCellValue("P".$i,"JMLH PAS. YG HIDUP & MATI MENURUT GOL UMUR & SEX_>1-<=4th_L");
		    $sheet->setCellValue("Q".$i,"JMLH PAS. YG HIDUP & MATI MENURUT GOL UMUR & SEX_>1-<=4th_P");
		    $sheet->setCellValue("R".$i,"JMLH PAS. YG HIDUP & MATI MENURUT GOL UMUR & SEX_>4-<=14th_L");
		    $sheet->setCellValue("S".$i,"JMLH PAS. YG HIDUP & MATI MENURUT GOL UMUR & SEX_>4-<=14th_P");
		    $sheet->setCellValue("T".$i,"JMLH PAS. YG HIDUP & MATI MENURUT GOL UMUR & SEX_>14-<=24th_L");
		    $sheet->setCellValue("U".$i,"JMLH PAS. YG HIDUP & MATI MENURUT GOL UMUR & SEX_>14-<=24th_P");
		    $sheet->setCellValue("V".$i,"JMLH PAS. YG HIDUP & MATI MENURUT GOL UMUR & SEX_>24-<=44th_L");
		    $sheet->setCellValue("W".$i,"JMLH PAS. YG HIDUP & MATI MENURUT GOL UMUR & SEX_>24-<=44th_P");
		    $sheet->setCellValue("X".$i,"JMLH PAS. YG HIDUP & MATI MENURUT GOL UMUR & SEX_>44-<=64th_L");
		    $sheet->setCellValue("Y".$i,"JMLH PAS. YG HIDUP & MATI MENURUT GOL UMUR & SEX_>44-<=64th_P");
		    $sheet->setCellValue("Z".$i,"JMLH PAS. YG HIDUP & MATI MENURUT GOL UMUR & SEX_>64_L");
		    $sheet->setCellValue("AA".$i,"JMLH PAS. YG HIDUP & MATI MENURUT GOL UMUR & SEX_>64_P");
		    $sheet->setCellValue("AB".$i,"PASIEN KELUAR (HIDUP&MATI) MENURUT JENIS KELAMIN_LK");
		    $sheet->setCellValue("AC".$i,"PASIEN KELUAR (HIDUP&MATI) MENURUT JENIS KELAMIN_PR");
		    $sheet->setCellValue("AD".$i,"JML PASIEN KELUAR HIDUP&MATI_(23+24)");
		    $sheet->setCellValue("AE".$i,"JUMLAH PASIEN KELUAR MATI");
		    $sheet->getStyle("A".$i.":AE".$i)->getFont()->setBold(true);
		    $sheet->getStyle("A".$i.":AE".$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		    $sheet->getStyle("A".$i.":AE".$i)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

		    $data = json_decode($_POST['detail']);
		    foreach ($data as $d) {
			    $i = $i + 1;
			    $sheet->setCellValue("A".$i,$d->kode_propinsi);
			    $sheet->setCellValue("B".$i,$d->kabupaten_kota);
			    $sheet->setCellValue("C".$i,$d->kode_propinsi);
			    $sheet->setCellValue("D".$i,$d->nama_rs);
			    $sheet->setCellValue("E".$i,$d->tahun);
			    $sheet->setCellValue("F".$i,$d->nomor);
			    $sheet->setCellValue("G".$i,$d->dtd);
			    $sheet->setCellValue("H".$i,$d->kode_icd);
			    $sheet->setCellValue("I".$i,$d->sebab);
			    $sheet->setCellValue("J".$i,$d->jml_pasien_0hr_6hr_l);
			    $sheet->setCellValue("K".$i,$d->jml_pasien_0hr_6hr_p);
			    $sheet->setCellValue("L".$i,$d->jml_pasien_6hr_28hr_l);
			    $sheet->setCellValue("M".$i,$d->jml_pasien_6hr_28hr_p);
			    $sheet->setCellValue("N".$i,$d->jml_pasien_28hr_1th_l);
			    $sheet->setCellValue("O".$i,$d->jml_pasien_28hr_1th_p);
			    $sheet->setCellValue("P".$i,$d->jml_pasien_1th_4th_l);
			    $sheet->setCellValue("Q".$i,$d->jml_pasien_1th_4th_p);
			    $sheet->setCellValue("R".$i,$d->jml_pasien_4th_14th_l);
			    $sheet->setCellValue("S".$i,$d->jml_pasien_4th_14th_p);
			    $sheet->setCellValue("T".$i,$d->jml_pasien_14th_24th_l);
			    $sheet->setCellValue("U".$i,$d->jml_pasien_14th_24th_p);
			    $sheet->setCellValue("V".$i,$d->jml_pasien_24th_44th_l);
			    $sheet->setCellValue("W".$i,$d->jml_pasien_24th_44th_p);
			    $sheet->setCellValue("X".$i,$d->jml_pasien_44th_64th_l);
			    $sheet->setCellValue("Y".$i,$d->jml_pasien_44th_64th_p);
			    $sheet->setCellValue("Z".$i,$d->jml_pasien_l64th_l);
			    $sheet->setCellValue("AA".$i,$d->jml_pasien_l64th_p);
			    $sheet->setCellValue("AB".$i,$d->jml_pasien_keluar_l);
			    $sheet->setCellValue("AC".$i,$d->jml_pasien_keluar_p);
			    $sheet->setCellValue("AD".$i,$d->jml_pasien_keluar);
			    $sheet->setCellValue("AE".$i,$d->jml_pasien_keluar_mati);
			}

		    $thin = array ();
		    $thin['borders']=array();
		    $thin['borders']['allborders']=array();
		    $thin['borders']['allborders']['style']=PHPExcel_Style_Border::BORDER_THIN ;
		    $sheet->getStyle ( "A".$border_start.":AE".$i )->applyFromArray ($thin);

		    foreach (range('A', 'AE') as $columnID) {
		        $sheet->getColumnDimension($columnID)->setAutoSize(true);
		    }

		    header ( 'Content-Type: application/vnd.ms-excel' );
		    header ( 'Content-Disposition: attachment;filename="RL 4A - Penyakit Rawat Inap (Sebab Luar).xls"' );
		    header ( 'Cache-Control: max-age=0' );
		    $writer = PHPExcel_IOFactory::createWriter ( $file, 'Excel5' );
		    $writer->save ( 'php://output' );
		}
		return;
	}

	$head = array(
		'Kode Propinsi', 'Kab./Kota', 'Kode RS', 'Nama RS', 'Tahun', 'No. Urut', 'No. DTD', 'No. Daftar Terperinci', 'Golongan Sebab Penyakit', 
		'Jml. Pas. 0H-6H L', 'Jml. Pas. 0H-6H P', 
		'Jml. Pas. >6H-28H L', 'Jml. Pas. >6H-28H P', 
		'Jml. Pas. >28H-1T L', 'Jml. Pas. >28H-1T P',
		'Jml. Pas. >1T-4T L', 'Jml. Pas. >1T-4T P',
		'Jml. Pas. >4T-14T L', 'Jml. Pas. >4T-14T P',
		'Jml. Pas. >14T-24T L', 'Jml. Pas. >14T-24T P',
		'Jml. Pas. >24T-44T L', 'Jml. Pas. >24T-44T P',
		'Jml. Pas. >44T-64T L', 'Jml. Pas. >44T-64T P',
		'Jml. Pas. >64T L', 'Jml. Pas. >64T P',
		'Jml. Pas. Keluar L', 'Jml. Pas. Keluar P', 
		'Jml. Pas. Keluar', 'Jml. Pas. Keluar Mati'
	);
	$uitable = new Table($head, "", null, true);
	$uitable->setName("rl4a_2");
	$uitable->setAction(false);
	$uitable->setFooterVisible(false);
	$uitable->setHeaderVisible(false);
	$uitable->addHeader("before", "
		<tr>
			<th rowspan='3'>Kode Propinsi</th>
			<th rowspan='3'>Kab/Kota</th>
			<th rowspan='3'>Kode RS</th>
			<th rowspan='3'>Nama RS</th>
			<th rowspan='3'>Tahun</th>
			<th rowspan='3'>No. Urut</th>
			<th rowspan='3'>No. DTD</th>
			<th rowspan='3'>No. Daftar Terperinci</th>
			<th rowspan='3'>Golongan Sebab Penyakit</th>
			<th colspan='18'>Jumlah Pasien (Hidup dan Mati) Berdasarkan Golongan Umur dan Jenis Kelamin</th>
			<th colspan='2' rowspan='2'>Jumlah Pasien Keluar (Hidup dan Mati) Berdsarkan Jenis Kelamin</th>
			<th rowspan='3'>Jumlah Pasien Keluar (Hidup dan Mati)</th>
			<th rowspan='3'>Jumlah Pasien Keluar Mati</th>
		</tr>
		<tr>
			<th colspan='2'>0 Hari s/d 6 Hari</th>
			<th colspan='2'>6 Hari s/d 28 Hari</th>
			<th colspan='2'>28 Hari s/d 1 Tahun</th>
			<th colspan='2'>1 Tahun s/d 4 Tahun</th>
			<th colspan='2'>4 Tahun s/d 14 Tahun</th>
			<th colspan='2'>14 Tahun s/d 24 Tahun</th>
			<th colspan='2'>24 Tahun s/d 44 Tahun</th>
			<th colspan='2'>44 Tahun s/d 64 Tahun</th>
			<th colspan='2'>Lebih dari 64 Tahun</th>
		</tr>
		<tr>
			<th>L</th>
			<th>P</th>
			<th>L</th>
			<th>P</th>
			<th>L</th>
			<th>P</th>
			<th>L</th>
			<th>P</th>
			<th>L</th>
			<th>P</th>
			<th>L</th>
			<th>P</th>
			<th>L</th>
			<th>P</th>
			<th>L</th>
			<th>P</th>
			<th>L</th>
			<th>P</th>
			<th>L</th>
			<th>P</th>
		</tr>
	");

	$modal = $uitable->getModal();
	$form = $modal->getForm();
	$form->setTitle("RL 4A - Penyakit Rawat Inap (Sebab Luar)");

	$tahun_rows = $db->get_result("
		SELECT DISTINCT YEAR(tanggal) tahun
		FROM smis_mr_diagnosa
		WHERE prop = ''
		ORDER BY YEAR(tanggal) ASC
	");
	$tahun_option = new OptionBuilder();
	if ($tahun_rows != null) {
		foreach ($tahun_rows as $tahun_row) {
			if (date("Y") == $tahun_row->tahun)
				$tahun_option->add($tahun_row->tahun, $tahun_row->tahun, "1");
			else
				$tahun_option->add($tahun_row->tahun, $tahun_row->tahun);
		}
	}
	$tahun_select = new Select("rl4a_2_tahun_filter", "rl4a_2_tahun_filter", $tahun_option->getContent());
	$form->addElement("Tahun", $tahun_select);
	$excel_button = new Button("", "", "");
	$excel_button 	->addClass("btn-primary")
					->setIsButton(Button::$ICONIC)
					->setIcon("fa fa-file-excel-o")
					->setAtribute("id='rl4a_2_export_btn'");
	$process_button = new Button("", "", "");
	$process_button	->addClass("btn-inverse")
					-> setIsButton(Button::$ICONIC)
					->setIcon("fa fa-circle-o")
					->setAction("rl4a_2.view()");
	$button_group = new ButtonGroup("noprint");
	$button_group->addButton($process_button);
	$button_group->addButton($excel_button);
	$form->addElement("", $button_group);

	$loading_bar = new LoadingBar("rl4a_2_loading_bar", "");
	$button = new Button("", "", "Batal");
	$button->addClass("btn-primary");
	$button->setIsButton(Button::$ICONIC_TEXT);
	$button->setIcon("fa fa-close");
	$button->setAction("rl4a_2.cancel()");
	$loading_modal = new Modal("loading_modal", "", "Proses..");
	$loading_modal->addHtml($loading_bar->getHtml(), "after");
	$loading_modal->addFooter($button);
	
	echo $loading_modal->getHtml();
	echo $form->getHtml();
	echo $uitable->getHtml();
	echo "<div id='rl4a_2_info'></div>";
	echo addJS("framework/smis/js/table_action.js");
	echo addJS("base-js/smis-base-loading.js");
	echo addJS("medical_record/resource/js/rl4a_2.js", false);
?>