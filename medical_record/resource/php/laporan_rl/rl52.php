<?php
	require_once("smis-base/smis-include-service-consumer.php");
	global $db;

	$head = array('Kode RS', 'Nama RS', 'Bulan', 'Tahun', 'Kab/Kota', 'Kode Propinsi', 'No.', 'Jenis Kegiatan', 'Jumlah', 'L', 'P');
	$uitable = new Table($head, "", null, true);
	$uitable->setName("rl52");
	$uitable->setAction(false);
	$uitable->setFooterVisible(false);

	if (isset($_POST ['command'])) {
		if ($_POST ['command'] == "get_jumlah_jenis_kegiatan") {
			$row = $db->get_row("
				SELECT 
					COUNT(*) jumlah
				FROM
					smis_mr_jenis_kegiatan_rl52
				WHERE
					prop = ''
			");
			$jumlah = 0;
			if ($row != null)
				$jumlah = $row->jumlah;
			$data['jumlah'] = $jumlah;
			echo json_encode($data);
		} else if ($_POST ['command'] == "get_info") {
			$kode_rs = "";
			$nama_rs = "";
			$kode_propinsi = "";
			$kabupaten_kota = "";
			$info_rs_row = $db->get_row("
				SELECT 
					*
				FROM 
					smis_mr_data_dasar_rs
				WHERE 
					prop = ''
				LIMIT 
					0, 1
			");
			if ($info_rs_row != null) {
				$kode_rs = $info_rs_row->koders;
				$nama_rs = $info_rs_row->nama_rs;
				$kode_propinsi = $info_rs_row->kode_provinsi;
				$kabupaten_kota = $info_rs_row->kota_kabupaten;
			}

			$bulan = $_POST['bulan'];
			$label_bulan = $_POST['label_bulan'];
			$tahun = $_POST['tahun'];
			$num = $_POST['num'];
			$row = $db->get_row("
				SELECT 
					*
				FROM
					smis_mr_jenis_kegiatan_rl52
				WHERE
					prop = ''
				ORDER BY
					nama ASC
				LIMIT 
					" . $num . ", 1
			");
			$html = "";
			$jenis_kegiatan = "";
			$jumlah_l = 0;
			$jumlah_p = 0;
			$total = 0;
			if ($row != null) {
				$jenis_kegiatan = $row->nama;
				$proto_rawat_rows = $db->get_result("
					SELECT 
						*
					FROM
						smis_adm_prototype
					WHERE
						prop = '' AND parent = 'rawat' AND status = 'actived'
				");
				if ($proto_rawat_rows != null) {
					foreach ($proto_rawat_rows as $proto_rawat) {
						$slug_rawat = $proto_rawat->slug;
						$urjip = getSettings($db, "smis-rs-urjip-" . $slug_rawat, "");
						if ($urjip == "URJ" || $urjip == "URJI") {
							$rl52_l_row = $db->get_row("
								SELECT 
									COUNT(*) jumlah
								FROM 
									smis_rwt_antrian_" . $slug_rawat . "
								WHERE
									prop = '' AND rl52 = '" . $jenis_kegiatan . "' AND jk = '0' AND MONTH(waktu) LIKE '" . $bulan . "' AND YEAR(waktu) = '" . $tahun . "'
							");
							if ($rl52_l_row != null)
								$jumlah_l += $rl52_l_row->jumlah;

							$rl52_p_row = $db->get_row("
								SELECT 
									COUNT(*) jumlah
								FROM 
									smis_rwt_antrian_" . $slug_rawat . "
								WHERE
									prop = '' AND rl52 = '" . $jenis_kegiatan . "' AND jk = '1' AND MONTH(waktu) LIKE '" . $bulan . "' AND YEAR(waktu) = '" . $tahun . "'
							");
							if ($rl52_p_row != null)
								$jumlah_p += $rl52_p_row->jumlah;
						}
					} //. End of Proto. Rawat Iteration
				}
				$total = $jumlah_l + $jumlah_p;
			}

			$html = "
				<tr id='data_rl52'>
					<td id='rl52_kode_rs'>" . $kode_rs . "</td>
					<td id='rl52_nama_rs'>" . $nama_rs . "</td>
					<td id='rl52_bulan'>" . $label_bulan . "</td>
					<td id='rl52_tahun'>" . $tahun . "</td>
					<td id='rl52_kab_kota'>" . $kabupaten_kota . "</td>
					<td id='rl52_kode_prov'>" . $kode_propinsi . "</td>
					<td id='rl52_nomor'></td>
					<td id='rl52_jenis_kegiatan'>" . $jenis_kegiatan . "</td>
					<td id='rl52_jumlah'>" . ArrayAdapter::format("number", $total) . "</td>
					<td id='rl52_l'>" . ArrayAdapter::format("number", $jumlah_l) . "</td>
					<td id='rl52_p'>" . ArrayAdapter::format("number", $jumlah_p) . "</td>
				</tr>
			";

			$data['html'] = $html;
			echo json_encode($data);
		} else {
			$adapter = new SimpleAdapter();
			$dbtable = new DBTable($db, "smis_rg_layananpasien");
			$dbresponder = new DBResponder(
				$dbtable,
				$uitable,
				$adapter
			);
			$data = $dbresponder->command($_POST['command']);
			echo json_encode($data);
		}
		return;
	}
	$modal = $uitable->getModal();
	$form = $modal->getForm();
	$form->setTitle("RL 5.2 - Data Kunjungan Rawat Jalan");

	$tahun_rows = $db->get_result("
		SELECT DISTINCT YEAR(tanggal) tahun
		FROM smis_rg_layananpasien
		WHERE prop = '' AND carapulang NOT LIKE 'TIDAK DATANG' AND YEAR(tanggal) > 0
		ORDER BY YEAR(tanggal) ASC
	");
	$tahun_option = new OptionBuilder();
	if ($tahun_rows != null) {
		foreach ($tahun_rows as $tahun_row) {
			if (date("Y") == $tahun_row->tahun)
				$tahun_option->add($tahun_row->tahun, $tahun_row->tahun, "1");
			else
				$tahun_option->add($tahun_row->tahun, $tahun_row->tahun);
		}
	}
	$tahun_select = new Select("rl52_tahun", "rl52_tahun", $tahun_option->getContent());
	$form->addElement("Tahun", $tahun_select);
	$bulan_option = new OptionBuilder();
	$bulan_option->add("Semua Bulan", "%%", "1");
	for ($i = 1; $i <= 12; $i++)
		$bulan_option->add(ArrayAdapter::format("month-id", $i), $i);
	$bulan_select = new Select("rl52_bulan", "rl52_bulan", $bulan_option->getContent());
	$form->addElement("Bulan", $bulan_select);

	$excel_button = new Button("", "", "");
	$excel_button 	->addClass("btn-primary")
					->setIsButton(Button::$ICONIC)
					->setIcon("fa fa-file-excel-o")
					->setAction("rl52.excel()");
	$process_button = new Button("", "", "");
	$process_button	->addClass("btn-inverse")
					-> setIsButton(Button::$ICONIC)
					->setIcon("fa fa-circle-o")
					->setAction("rl52.view()");
	$button_group = new ButtonGroup("noprint");
	$button_group->addButton($process_button);
	$button_group->addButton($excel_button);
	$form->addElement("", $button_group);

	echo "<div class='alert alert-block alert-inverse noprint'>" .
            "<h4>Informasi</h4>" .
            "Jenis kegiatan diambil berdasarkan setting jenis kegiatan di masing-masing ruangan" .
          "</div>";
	echo $form->getHtml();
	echo $uitable->getHtml();
	echo $modal->getHtml();
	echo addJS("framework/smis/js/table_action.js");
	echo addJS("medical_record/resource/js/rl52.js", false);
?>