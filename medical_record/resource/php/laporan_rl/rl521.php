<?php
	require_once("smis-base/smis-include-service-consumer.php");
	global $db;

	$head = array('Kode RS', 'Nama RS', 'Bulan', 'Tahun', 'Kab/Kota', 'Kode Propinsi', 'No.', 'Jenis Kegiatan', 'Jumlah', 'L', 'P');
	$uitable = new Table($head, "", null, true);
	$uitable->setName("rl521");
	$uitable->setAction(false);
	$uitable->setFooterVisible(false);

	if (isset($_POST ['command'])) {
		$adapter = new SimpleAdapter();
		$adapter->setUseNumber(true, "No.", "back.");
		$adapter->add("Kode RS", "kode_rs");
		$adapter->add("Nama RS", "nama_rs");
		$adapter->add("Bulan", "bulan", "month-id");
		$adapter->add("Tahun", "tahun");
		$adapter->add("Kab/Kota", "kota_kabupaten");
		$adapter->add("Kode Propinsi", "kode_provinsi");
		$adapter->add("Jenis Kegiatan", "jenis_kegiatan");
		$adapter->add("Jumlah", "jumlah", "number");
		$adapter->add("L", "l", "number");
		$adapter->add("P", "p", "number");

		$dbtable = new DBTable($db, "smis_mr_data_dasar_rs");
		$dbtable->setShowAll(true);
		$filter = " AND YEAR(tanggal) = '" . $_POST['tahun'] . "' ";
		if ($_POST['bulan'] == 0)
			$filter .= " AND MONTH(tanggal) > 0 ";
		else
			$filter .= " AND MONTH(tanggal) = " . $_POST['bulan'] . " ";
		$query_value = "
			SELECT w.koders as kode_rs, w.nama_rs, w.kota_kabupaten, w.kode_provinsi, v.bulan, v.tahun, v.jenis_kegiatan, SUM(v.l + v.p) jumlah, SUM(v.l) l, SUM(v.p) p
			FROM (
				SELECT month(b.tanggal) bulan, year(b.tanggal) tahun, jenis_kegiatan, IF(kelamin = 0, 1, 0) l, IF(kelamin = 1, 1, 0) p 
				FROM smis_rg_layananpasien b 
				WHERE b.prop = '' AND b.carapulang NOT LIKE 'TIDAK DATANG' AND uri = '1' " . $filter . "
			) v, smis_mr_data_dasar_rs w
			GROUP BY kode_rs, nama_rs, bulan, tahun, kota_kabupaten, kode_provinsi, jenis_kegiatan 
		";
		$query_count = "
			SELECT COUNT(*)
			FROM (
				" . $query_value . "
			) v
		";
		$dbtable->setPreferredQuery(true, $query_value, $query_count);

	    $dbresponder = new DBResponder($dbtable, $uitable, $adapter);
		$data = $dbresponder->command($_POST['command']);
		echo json_encode($data);
		return;
	}
	$modal = $uitable->getModal();
	$form = $modal->getForm();
	$form->setTitle("RL 5.2.1 - Data Kunjungan Rawat Inap");

	$tahun_rows = $db->get_result("
		SELECT DISTINCT YEAR(tanggal) tahun
		FROM smis_rg_layananpasien
		WHERE prop = '' AND carapulang NOT LIKE 'TIDAK DATANG' AND YEAR(tanggal) > 0
		ORDER BY YEAR(tanggal) ASC
	");
	$tahun_option = new OptionBuilder();
	if ($tahun_rows != null) {
		foreach ($tahun_rows as $tahun_row) {
			if (date("Y") == $tahun_row->tahun)
				$tahun_option->add($tahun_row->tahun, $tahun_row->tahun, "1");
			else
				$tahun_option->add($tahun_row->tahun, $tahun_row->tahun);
		}
	}
	$tahun_select = new Select("rl521_tahun", "rl521_tahun", $tahun_option->getContent());
	$form->addElement("Tahun", $tahun_select);
	$bulan_option = new OptionBuilder();
	$bulan_option->add("Semua Bulan", "0", "1");
	for ($i = 1; $i <= 12; $i++)
		$bulan_option->add(ArrayAdapter::format("month-id", $i), $i);
	$bulan_select = new Select("rl521_bulan", "rl521_bulan", $bulan_option->getContent());
	$form->addElement("Bulan", $bulan_select);

	$excel_button = new Button("", "", "");
	$excel_button 	->addClass("btn-primary")
					->setIsButton(Button::$ICONIC)
					->setIcon("fa fa-file-excel-o")
					->setAction("rl521.excel()");
	$process_button = new Button("", "", "");
	$process_button	->addClass("btn-inverse")
					-> setIsButton(Button::$ICONIC)
					->setIcon("fa fa-circle-o")
					->setAction("rl521.view()");
	$button_group = new ButtonGroup("noprint");
	$button_group->addButton($process_button);
	$button_group->addButton($excel_button);
	$form->addElement("", $button_group);

	echo "<div class='alert alert-block alert-inverse noprint'>" .
            "<h4>Informasi</h4>" .
            "Jenis kegiatan diambil berdasarkan setting jenis kegiatan di masing-masing ruangan" .
          "</div>";
	echo $form->getHtml();
	echo $uitable->getHtml();
	echo $modal->getHtml();
	echo addJS("framework/smis/js/table_action.js");
	echo addJS("medical_record/resource/js/rl521.js", false);
?>