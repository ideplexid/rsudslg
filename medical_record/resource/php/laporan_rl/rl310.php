<?php
	require_once("smis-base/smis-include-service-consumer.php");
	global $db;

	$head = array('Kode RS', 'Nama RS', 'Kode Propinsi', 'Kab/Kota', 'Tahun', 'No. Urut', 'Jenis Kegiatan', 'Jumlah');
	$uitable = new Table($head, "", null, true);
	$uitable->setName("rl310");
	$uitable->setAction(false);
	$uitable->setFooterVisible(false);

	if (isset($_POST ['command'])) {
		if ($_POST['command'] == "view") {
			$tahun = $_POST['tahun'];
			$bulan = $_POST['bulan'];

			$kode_rs = "";
			$nama_rs = "";
			$kode_propinsi = "";
			$kabupaten_kota = "";
			$info_rs_row = $db->get_row("
				SELECT 
					*
				FROM 
					smis_mr_data_dasar_rs
				WHERE 
					prop = ''
				LIMIT 
					0, 1
			");
			if ($info_rs_row != null) {
				$kode_rs = $info_rs_row->koders;
				$nama_rs = $info_rs_row->nama_rs;
				$kode_propinsi = $info_rs_row->kode_provinsi;
				$kabupaten_kota = $info_rs_row->kota_kabupaten;
			}

			$jumlah_ekg = 0;
			$jumlah_ekg_row = $db->get_row("
				SELECT 
					COUNT(id) jumlah
				FROM 
					smis_mr_pkhusus
				WHERE 
					prop = '' AND YEAR(tanggal) = '" . $tahun . "' AND MONTH(tanggal) LIKE '" . $bulan . "' AND layanan_khusus LIKE 'Elektro Kardiographi (EKG)'
			");
			if ($jumlah_ekg_row != null)
				$jumlah_ekg = $jumlah_ekg_row->jumlah;
			$jumlah_emg = 0;
			$jumlah_emg_row = $db->get_row("
				SELECT 
					COUNT(id) jumlah
				FROM 
					smis_mr_pkhusus
				WHERE 
					prop = '' AND YEAR(tanggal) = '" . $tahun . "' AND MONTH(tanggal) LIKE '" . $bulan . "' AND layanan_khusus LIKE 'Elektro Myographi (EMG)'
			");
			if ($jumlah_emg_row != null)
				$jumlah_emg = $jumlah_emg_row->jumlah;
			$jumlah_ecg = 0;
			$jumlah_ecg_row = $db->get_row("
				SELECT 
					COUNT(id) jumlah
				FROM 
					smis_mr_pkhusus
				WHERE 
					prop = '' AND YEAR(tanggal) = '" . $tahun . "' AND MONTH(tanggal) LIKE '" . $bulan . "' AND layanan_khusus LIKE 'Echo Cardiographi (ECG)'
			");
			if ($jumlah_ecg_row != null)
				$jumlah_ecg = $jumlah_ecg_row->jumlah;
			$jumlah_endoskopi = 0;
			$jumlah_endoskopi_row = $db->get_row("
				SELECT 
					COUNT(id) jumlah
				FROM 
					smis_mr_pkhusus
				WHERE 
					prop = '' AND YEAR(tanggal) = '" . $tahun . "' AND MONTH(tanggal) LIKE '" . $bulan . "' AND layanan_khusus LIKE 'Endoskopi'
			");
			if ($jumlah_endoskopi_row != null)
				$jumlah_endoskopi = $jumlah_endoskopi_row->jumlah;
			$jumlah_hemodialisa = 0;
			$jumlah_hemodialisa_row = $db->get_row("
				SELECT 
					COUNT(id) jumlah
				FROM 
					smis_mr_pkhusus
				WHERE 
					prop = '' AND YEAR(tanggal) = '" . $tahun . "' AND MONTH(tanggal) LIKE '" . $bulan . "' AND layanan_khusus LIKE 'Hemodialisa'
			");
			if ($jumlah_hemodialisa_row != null)
				$jumlah_hemodialisa = $jumlah_hemodialisa_row->jumlah;
			$jumlah_densometri_tulang = 0;
			$jumlah_densometri_tulang_row = $db->get_row("
				SELECT 
					COUNT(id) jumlah
				FROM 
					smis_mr_pkhusus
				WHERE 
					prop = '' AND YEAR(tanggal) = '" . $tahun . "' AND MONTH(tanggal) LIKE '" . $bulan . "' AND layanan_khusus LIKE 'Densometri Tulang'
			");
			if ($jumlah_densometri_tulang_row != null)
				$jumlah_densometri_tulang = $jumlah_densometri_tulang_row->jumlah;
			$jumlah_pungsi = 0;
			$jumlah_pungsi_row = $db->get_row("
				SELECT 
					COUNT(id) jumlah
				FROM 
					smis_mr_pkhusus
				WHERE 
					prop = '' AND YEAR(tanggal) = '" . $tahun . "' AND MONTH(tanggal) LIKE '" . $bulan . "' AND layanan_khusus LIKE 'Pungsi'
			");
			if ($jumlah_pungsi_row != null)
				$jumlah_pungsi = $jumlah_pungsi_row->jumlah;
			$jumlah_spirometri = 0;
			$jumlah_spirometri_row = $db->get_row("
				SELECT 
					COUNT(id) jumlah
				FROM 
					smis_mr_pkhusus
				WHERE 
					prop = '' AND YEAR(tanggal) = '" . $tahun . "' AND MONTH(tanggal) LIKE '" . $bulan . "' AND layanan_khusus LIKE 'Spirometri'
			");
			if ($jumlah_spirometri_row != null)
				$jumlah_spirometri = $jumlah_spirometri_row->jumlah;
			$jumlah_tes_kulit = 0;
			$jumlah_tes_kulit_row = $db->get_row("
				SELECT 
					COUNT(id) jumlah
				FROM 
					smis_mr_pkhusus
				WHERE 
					prop = '' AND YEAR(tanggal) = '" . $tahun . "' AND MONTH(tanggal) LIKE '" . $bulan . "' AND layanan_khusus LIKE 'Tes Kulit/Alergi/Histamin'
			");
			if ($jumlah_tes_kulit_row != null)
				$jumlah_tes_kulit = $jumlah_tes_kulit_row->jumlah;
			$jumlah_topometri = 0;
			$jumlah_topometri_row = $db->get_row("
				SELECT 
					COUNT(id) jumlah
				FROM 
					smis_mr_pkhusus
				WHERE 
					prop = '' AND YEAR(tanggal) = '" . $tahun . "' AND MONTH(tanggal) LIKE '" . $bulan . "' AND layanan_khusus LIKE 'Topometri'
			");
			if ($jumlah_topometri_row != null)
				$jumlah_topometri = $jumlah_topometri_row->jumlah;
			$jumlah_akupuntur = 0;
			$jumlah_akupuntur_row = $db->get_row("
				SELECT 
					COUNT(id) jumlah
				FROM 
					smis_mr_pkhusus
				WHERE 
					prop = '' AND YEAR(tanggal) = '" . $tahun . "' AND MONTH(tanggal) LIKE '" . $bulan . "' AND layanan_khusus LIKE 'Akupuntur'
			");
			if ($jumlah_akupuntur_row != null)
				$jumlah_akupuntur = $jumlah_akupuntur_row->jumlah;
			$jumlah_hiperbarik = 0;
			$jumlah_hiperbarik_row = $db->get_row("
				SELECT 
					COUNT(id) jumlah
				FROM 
					smis_mr_pkhusus
				WHERE 
					prop = '' AND YEAR(tanggal) = '" . $tahun . "' AND MONTH(tanggal) LIKE '" . $bulan . "' AND layanan_khusus LIKE 'Hiperbarik'
			");
			if ($jumlah_hiperbarik_row != null)
				$jumlah_hiperbarik = $jumlah_hiperbarik_row->jumlah;
			$jumlah_herbal = 0;
			$jumlah_herbal_row = $db->get_row("
				SELECT 
					COUNT(id) jumlah
				FROM 
					smis_mr_pkhusus
				WHERE 
					prop = '' AND YEAR(tanggal) = '" . $tahun . "' AND MONTH(tanggal) LIKE '" . $bulan . "' AND layanan_khusus LIKE 'Herbal / jamu'
			");
			if ($jumlah_herbal_row != null)
				$jumlah_herbal = $jumlah_herbal_row->jumlah;
			$jumlah_lain = 0;
			$jumlah_lain_row = $db->get_row("
				SELECT 
					COUNT(id) jumlah
				FROM 
					smis_mr_pkhusus
				WHERE 
					prop = '' AND YEAR(tanggal) = '" . $tahun . "' AND MONTH(tanggal) LIKE '" . $bulan . "' AND layanan_khusus LIKE 'Lain-lain'
			");
			if ($jumlah_lain_row != null)
				$jumlah_lain = $jumlah_lain_row->jumlah;

			$html = "<tr>" .
						"<td id='rl310_kode_rs'><small>" . $kode_rs . "</small></td>" .
						"<td id='rl310_nama_rs'><small>" . $nama_rs . "</small></td>" .
						"<td id='rl310_kode_propinsi'><small>" . $kode_propinsi . "</small></td>" .
						"<td id='rl310_kabupaten_kota'><small>" . $kabupaten_kota . "</small></td>" .
						"<td id='rl310_tahun'><small>" . $tahun . "</small></td>" .
						"<td id='rl310_nomor'><small>1</small></td>" .
						"<td id='rl310_jenis_kegiatan'><small>Elektro Kardiographi (EKG)</small></td>" .
						"<td id='rl310_jumlah' style='display:none;'>" . $jumlah_ekg . "</td>" .
						"<td id='rl310_f_jumlah'><div align='right'><small>" . $jumlah_ekg . "</small></div></td>" .
					"</tr>" .
					"<tr>" .
						"<td id='rl310_kode_rs'><small>" . $kode_rs . "</small></td>" .
						"<td id='rl310_nama_rs'><small>" . $nama_rs . "</small></td>" .
						"<td id='rl310_kode_propinsi'><small>" . $kode_propinsi . "</small></td>" .
						"<td id='rl310_kabupaten_kota'><small>" . $kabupaten_kota . "</small></td>" .
						"<td id='rl310_tahun'><small>" . $tahun . "</small></td>" .
						"<td id='rl310_nomor'><small>2</small></td>" .
						"<td id='rl310_jenis_kegiatan'><small>Elektro Myographi (EMG)</small></td>" .
						"<td id='rl310_jumlah' style='display:none;'>" . $jumlah_emg . "</td>" .
						"<td id='rl310_f_jumlah'><div align='right'><small>" . $jumlah_emg . "</small></div></td>" .
					"</tr>" .
					"<tr>" .
						"<td id='rl310_kode_rs'><small>" . $kode_rs . "</small></td>" .
						"<td id='rl310_nama_rs'><small>" . $nama_rs . "</small></td>" .
						"<td id='rl310_kode_propinsi'><small>" . $kode_propinsi . "</small></td>" .
						"<td id='rl310_kabupaten_kota'><small>" . $kabupaten_kota . "</small></td>" .
						"<td id='rl310_tahun'><small>" . $tahun . "</small></td>" .
						"<td id='rl310_nomor'><small>3</small></td>" .
						"<td id='rl310_jenis_kegiatan'><small>Elektro Cardiographi (ECG)</small></td>" .
						"<td id='rl310_jumlah' style='display:none;'>" . $jumlah_ecg . "</td>" .
						"<td id='rl310_f_jumlah'><div align='right'><small>" . $jumlah_ecg . "</small></div></td>" .
					"</tr>" .
					"<tr>" .
						"<td id='rl310_kode_rs'><small>" . $kode_rs . "</small></td>" .
						"<td id='rl310_nama_rs'><small>" . $nama_rs . "</small></td>" .
						"<td id='rl310_kode_propinsi'><small>" . $kode_propinsi . "</small></td>" .
						"<td id='rl310_kabupaten_kota'><small>" . $kabupaten_kota . "</small></td>" .
						"<td id='rl310_tahun'><small>" . $tahun . "</small></td>" .
						"<td id='rl310_nomor'><small>4</small></td>" .
						"<td id='rl310_jenis_kegiatan'><small>Endoskopi (semua bentuk)</small></td>" .
						"<td id='rl310_jumlah' style='display:none;'>" . $jumlah_endoskopi . "</td>" .
						"<td id='rl310_f_jumlah'><div align='right'><small>" . $jumlah_endoskopi . "</small></div></td>" .
					"</tr>" .
					"<tr>" .
						"<td id='rl310_kode_rs'><small>" . $kode_rs . "</small></td>" .
						"<td id='rl310_nama_rs'><small>" . $nama_rs . "</small></td>" .
						"<td id='rl310_kode_propinsi'><small>" . $kode_propinsi . "</small></td>" .
						"<td id='rl310_kabupaten_kota'><small>" . $kabupaten_kota . "</small></td>" .
						"<td id='rl310_tahun'><small>" . $tahun . "</small></td>" .
						"<td id='rl310_nomor'><small>5</small></td>" .
						"<td id='rl310_jenis_kegiatan'><small>Hemodialisa</small></td>" .
						"<td id='rl310_jumlah' style='display:none;'>" . $jumlah_hemodialisa . "</td>" .
						"<td id='rl310_f_jumlah'><div align='right'><small>" . $jumlah_hemodialisa . "</small></div></td>" .
					"</tr>" .
					"<tr>" .
						"<td id='rl310_kode_rs'><small>" . $kode_rs . "</small></td>" .
						"<td id='rl310_nama_rs'><small>" . $nama_rs . "</small></td>" .
						"<td id='rl310_kode_propinsi'><small>" . $kode_propinsi . "</small></td>" .
						"<td id='rl310_kabupaten_kota'><small>" . $kabupaten_kota . "</small></td>" .
						"<td id='rl310_tahun'><small>" . $tahun . "</small></td>" .
						"<td id='rl310_nomor'><small>6</small></td>" .
						"<td id='rl310_jenis_kegiatan'><small>Densometri Tulang</small></td>" .
						"<td id='rl310_jumlah' style='display:none;'>" . $jumlah_densometri_tulang . "</td>" .
						"<td id='rl310_f_jumlah'><div align='right'><small>" . $jumlah_densometri_tulang . "</small></div></td>" .
					"</tr>" .
					"<tr>" .
						"<td id='rl310_kode_rs'><small>" . $kode_rs . "</small></td>" .
						"<td id='rl310_nama_rs'><small>" . $nama_rs . "</small></td>" .
						"<td id='rl310_kode_propinsi'><small>" . $kode_propinsi . "</small></td>" .
						"<td id='rl310_kabupaten_kota'><small>" . $kabupaten_kota . "</small></td>" .
						"<td id='rl310_tahun'><small>" . $tahun . "</small></td>" .
						"<td id='rl310_nomor'><small>7</small></td>" .
						"<td id='rl310_jenis_kegiatan'><small>Pungsi</small></td>" .
						"<td id='rl310_jumlah' style='display:none;'>" . $jumlah_pungsi . "</td>" .
						"<td id='rl310_f_jumlah'><div align='right'><small>" . $jumlah_pungsi . "</small></div></td>" .
					"</tr>" .
					"<tr>" .
						"<td id='rl310_kode_rs'><small>" . $kode_rs . "</small></td>" .
						"<td id='rl310_nama_rs'><small>" . $nama_rs . "</small></td>" .
						"<td id='rl310_kode_propinsi'><small>" . $kode_propinsi . "</small></td>" .
						"<td id='rl310_kabupaten_kota'><small>" . $kabupaten_kota . "</small></td>" .
						"<td id='rl310_tahun'><small>" . $tahun . "</small></td>" .
						"<td id='rl310_nomor'><small>8</small></td>" .
						"<td id='rl310_jenis_kegiatan'><small>Spirometri</small></td>" .
						"<td id='rl310_jumlah' style='display:none;'>" . $jumlah_spirometri . "</td>" .
						"<td id='rl310_f_jumlah'><div align='right'><small>" . $jumlah_spirometri . "</small></div></td>" .
					"</tr>" .
					"<tr>" .
						"<td id='rl310_kode_rs'><small>" . $kode_rs . "</small></td>" .
						"<td id='rl310_nama_rs'><small>" . $nama_rs . "</small></td>" .
						"<td id='rl310_kode_propinsi'><small>" . $kode_propinsi . "</small></td>" .
						"<td id='rl310_kabupaten_kota'><small>" . $kabupaten_kota . "</small></td>" .
						"<td id='rl310_tahun'><small>" . $tahun . "</small></td>" .
						"<td id='rl310_nomor'><small>9</small></td>" .
						"<td id='rl310_jenis_kegiatan'><small>Tes Kulit/Alergi/Histamin</small></td>" .
						"<td id='rl310_jumlah' style='display:none;'>" . $jumlah_tes_kulit . "</td>" .
						"<td id='rl310_f_jumlah'><div align='right'><small>" . $jumlah_tes_kulit . "</small></div></td>" .
					"</tr>" .
					"<tr>" .
						"<td id='rl310_kode_rs'><small>" . $kode_rs . "</small></td>" .
						"<td id='rl310_nama_rs'><small>" . $nama_rs . "</small></td>" .
						"<td id='rl310_kode_propinsi'><small>" . $kode_propinsi . "</small></td>" .
						"<td id='rl310_kabupaten_kota'><small>" . $kabupaten_kota . "</small></td>" .
						"<td id='rl310_tahun'><small>" . $tahun . "</small></td>" .
						"<td id='rl310_nomor'><small>10</small></td>" .
						"<td id='rl310_jenis_kegiatan'><small>Topometri</small></td>" .
						"<td id='rl310_jumlah' style='display:none;'>" . $jumlah_topometri . "</td>" .
						"<td id='rl310_f_jumlah'><div align='right'><small>" . $jumlah_topometri . "</small></div></td>" .
					"</tr>" .
					"<tr>" .
						"<td id='rl310_kode_rs'><small>" . $kode_rs . "</small></td>" .
						"<td id='rl310_nama_rs'><small>" . $nama_rs . "</small></td>" .
						"<td id='rl310_kode_propinsi'><small>" . $kode_propinsi . "</small></td>" .
						"<td id='rl310_kabupaten_kota'><small>" . $kabupaten_kota . "</small></td>" .
						"<td id='rl310_tahun'><small>" . $tahun . "</small></td>" .
						"<td id='rl310_nomor'><small>11</small></td>" .
						"<td id='rl310_jenis_kegiatan'><small>Akupuntur</small></td>" .
						"<td id='rl310_jumlah' style='display:none;'>" . $jumlah_akupuntur . "</td>" .
						"<td id='rl310_f_jumlah'><div align='right'><small>" . $jumlah_akupuntur . "</small></div></td>" .
					"</tr>" .
					"<tr>" .
						"<td id='rl310_kode_rs'><small>" . $kode_rs . "</small></td>" .
						"<td id='rl310_nama_rs'><small>" . $nama_rs . "</small></td>" .
						"<td id='rl310_kode_propinsi'><small>" . $kode_propinsi . "</small></td>" .
						"<td id='rl310_kabupaten_kota'><small>" . $kabupaten_kota . "</small></td>" .
						"<td id='rl310_tahun'><small>" . $tahun . "</small></td>" .
						"<td id='rl310_nomor'><small>12</small></td>" .
						"<td id='rl310_jenis_kegiatan'><small>Hiperbarik</small></td>" .
						"<td id='rl310_jumlah' style='display:none;'>" . $jumlah_hiperbarik . "</td>" .
						"<td id='rl310_f_jumlah'><div align='right'><small>" . $jumlah_hiperbarik . "</small></div></td>" .
					"</tr>" .
					"<tr>" .
						"<td id='rl310_kode_rs'><small>" . $kode_rs . "</small></td>" .
						"<td id='rl310_nama_rs'><small>" . $nama_rs . "</small></td>" .
						"<td id='rl310_kode_propinsi'><small>" . $kode_propinsi . "</small></td>" .
						"<td id='rl310_kabupaten_kota'><small>" . $kabupaten_kota . "</small></td>" .
						"<td id='rl310_tahun'><small>" . $tahun . "</small></td>" .
						"<td id='rl310_nomor'><small>13</small></td>" .
						"<td id='rl310_jenis_kegiatan'><small>Herbal / jamu</small></td>" .
						"<td id='rl310_jumlah' style='display:none;'>" . $jumlah_herbal . "</td>" .
						"<td id='rl310_f_jumlah'><div align='right'><small>" . $jumlah_herbal . "</small></div></td>" .
					"</tr>" .
					"<tr>" .
						"<td id='rl310_kode_rs'><small>" . $kode_rs . "</small></td>" .
						"<td id='rl310_nama_rs'><small>" . $nama_rs . "</small></td>" .
						"<td id='rl310_kode_propinsi'><small>" . $kode_propinsi . "</small></td>" .
						"<td id='rl310_kabupaten_kota'><small>" . $kabupaten_kota . "</small></td>" .
						"<td id='rl310_tahun'><small>" . $tahun . "</small></td>" .
						"<td id='rl310_nomor'><small>14</small></td>" .
						"<td id='rl310_jenis_kegiatan'><small>Lain-lain</small></td>" .
						"<td id='rl310_jumlah' style='display:none;'>" . $jumlah_lain . "</td>" .
						"<td id='rl310_f_jumlah'><div align='right'><small>" . $jumlah_lain . "</small></div></td>" .
					"</tr>";
			
			$data['html'] = $html;
			echo json_encode($data);
		} else if ($_POST['command'] == "export_xls") {
			require_once ("smis-libs-out/php-excel/PHPExcel.php");

		    $file = new PHPExcel ();
		    $file->getProperties ()->setCreator ( "PT. Inovasi Ide Utama" );
		    $file->getProperties ()->setTitle ( "RL 3.10 - Pelayanan Khusus" );
		    $file->getProperties ()->setSubject ( "RL 3.10 - Pelayanan Khusus" );
		    $file->getProperties ()->setDescription ( "RL 3.10 - Pelayanan Khusus Generated by System" );
		    $file->getProperties ()->setKeywords ( "RL 3.10 - Pelayanan Khusus" );
		    $file->getProperties ()->setCategory ( "RL 3.10 - Pelayanan Khusus" );

		    $sheet = $file->getActiveSheet ();
		    $i = 1;

		    $sheet->mergeCells("A".$i.":H".$i)->setCellValue("A".$i,"RL 3.10 - Pelayanan Khusus");
		    $sheet->getStyle("A".$i.":H".$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		    $sheet->getStyle("A".$i)->getFont()->setBold(true);
		    $i = $i + 2;

		    $border_start = $i;
		    
		    $sheet->setCellValue("A".$i,"KODE PROPINSI");
		    $sheet->setCellValue("B".$i,"KAB/KOTA");
		    $sheet->setCellValue("C".$i,"KODE RS");
		    $sheet->setCellValue("D".$i,"NAMA RS");
		    $sheet->setCellValue("E".$i,"TAHUN");
		    $sheet->setCellValue("F".$i,"NO. URUT");
		    $sheet->setCellValue("G".$i,"JENIS KEGIATAN");
		    $sheet->setCellValue("H".$i,"JUMLAH");
		    $sheet->getStyle("A".$i.":H".$i)->getFont()->setBold(true);
		    $sheet->getStyle("A".$i.":H".$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		    $sheet->getStyle("A".$i.":H".$i)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);


		    $data = json_decode($_POST['detail']);
		    foreach ($data as $d) {
			    $i = $i + 1;
			    $sheet->setCellValue("A".$i,$d->kode_propinsi);
			    $sheet->setCellValue("B".$i,$d->kabupaten_kota);
			    $sheet->setCellValue("C".$i,$d->kode_propinsi);
			    $sheet->setCellValue("D".$i,$d->nama_rs);
			    $sheet->setCellValue("E".$i,$d->tahun);
			    $sheet->setCellValue("F".$i,$d->nomor);
			    $sheet->setCellValue("G".$i,$d->jenis_kegiatan);
			    $sheet->setCellValue("H".$i,$d->jumlah);
			}

		    $thin = array ();
		    $thin['borders']=array();
		    $thin['borders']['allborders']=array();
		    $thin['borders']['allborders']['style']=PHPExcel_Style_Border::BORDER_THIN ;
		    $sheet->getStyle ( "A".$border_start.":H".$i )->applyFromArray ($thin);

		    foreach (range('A', 'H') as $columnID) {
		        $sheet->getColumnDimension($columnID)->setAutoSize(true);
		    }

		    header ( 'Content-Type: application/vnd.ms-excel' );
		    header ( 'Content-Disposition: attachment;filename="RL 3.10 - Pelayanan Khusus.xls"' );
		    header ( 'Cache-Control: max-age=0' );
		    $writer = PHPExcel_IOFactory::createWriter ( $file, 'Excel5' );
		    $writer->save ( 'php://output' );
		}
		return;
	}
	$modal = $uitable->getModal();
	$form = $modal->getForm();
	$form->setTitle("RL 3.10 - Pelayanan Khusus");

	$bulan_option = new OptionBuilder();
	$bulan_option->add("Semua", "%%", "1");
	$bulan_option->add("Januari", "1");
	$bulan_option->add("Februari", "2");
	$bulan_option->add("Maret", "3");
	$bulan_option->add("April", "4");
	$bulan_option->add("Mei", "5");
	$bulan_option->add("Juni", "6");
	$bulan_option->add("Juli", "7");
	$bulan_option->add("Agustus", "8");
	$bulan_option->add("September", "9");
	$bulan_option->add("Oktober", "10");
	$bulan_option->add("Nopember", "11");
	$bulan_option->add("Desember", "12");
	$bulan_select = new Select("rl310_bulan_filter", "rl310_bulan_filter", $bulan_option->getContent());
	$form->addElement("Bulan", $bulan_select);
	$tahun_rows = $db->get_result("
		SELECT DISTINCT YEAR(tanggal) tahun
		FROM smis_mr_pkhusus
		WHERE prop = '' AND YEAR(tanggal) > 0
		ORDER BY YEAR(tanggal) ASC
	");
	$tahun_option = new OptionBuilder();
	if ($tahun_rows != null) {
		foreach ($tahun_rows as $tahun_row) {
			if (date("Y") == $tahun_row->tahun)
				$tahun_option->add($tahun_row->tahun, $tahun_row->tahun, "1");
			else
				$tahun_option->add($tahun_row->tahun, $tahun_row->tahun);
		}
	}
	$tahun_select = new Select("rl310_tahun_filter", "rl310_tahun_filter", $tahun_option->getContent());
	$form->addElement("Tahun", $tahun_select);
	$excel_button = new Button("", "", "");
	$excel_button 	->addClass("btn-primary")
					->setIsButton(Button::$ICONIC)
					->setIcon("fa fa-file-excel-o")
					->setAction("rl310.excel()");
	$process_button = new Button("", "", "");
	$process_button	->addClass("btn-inverse")
					-> setIsButton(Button::$ICONIC)
					->setIcon("fa fa-circle-o")
					->setAction("rl310.view()");
	$button_group = new ButtonGroup("noprint");
	$button_group->addButton($process_button);
	$button_group->addButton($excel_button);
	$form->addElement("", $button_group);

	echo $form->getHtml();
	echo $uitable->getHtml();
	echo addJS("framework/smis/js/table_action.js");
	echo addJS("medical_record/resource/js/rl310.js", false);
?>