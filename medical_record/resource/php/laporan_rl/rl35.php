<?php
	require_once("smis-base/smis-include-service-consumer.php");
	global $db;

	$head = array(
				'Kode Propinsi', 'Kab/Kota', 'Kode RS', 'Nama RS', 'Tahun', 'No. Urut', 'Jenis Kegiatan', 
				'Rujukan - Rumah Sakit', 'Rujukan - Bidan', 'Rujukan - Puskesmas', 'Rujukan - Faskes Lainnya', 'Rujukan - Medis - Mati', 'Rujukan Medis - Jumlah Total', 
				'Rujukan - Non-Medis - Mati', 'Rujukan - Non-Medis - Jumlah Total',
				'Non-Rujukan - Mati', 'Non-Rujukan - Jumlah Total',
				'Dirujuk'
			);
	$uitable = new Table($head, "", null, true);
	$uitable->setName("rl35");
	$uitable->setAction(false);
	$uitable->setFooterVisible(false);
	$uitable->setHeaderVisible(false);
	$uitable->addHeader("before", "
		<tr>
			<th rowspan='3'>Kode Propinsi</th>
			<th rowspan='3'>Kab/Kota</th>
			<th rowspan='3'>Kode RS</th>
			<th rowspan='3'>Nama RS</th>
			<th rowspan='3'>Tahun</th>
			<th rowspan='3'>No. Urut</th>
			<th rowspan='3'>Jenis Kegiatan</th>
			<th colspan='8'>Rujukan</th>
			<th colspan='2'>Non-Rujukan</th>
			<th rowspan='3'>Dirujuk</th>
		</tr>
		<tr>
			<th colspan='6'>Medis</th>
			<th colspan='2'>Non-Medis</th>			
			<th rowspan='2'>Mati</th>
			<th rowspan='2'>Jumlah Total</th>
		</tr>
		<tr>
			<th>Rumah Sakit</th>
			<th>Bidan</th>
			<th>Puskesmas</th>
			<th>Faskes Lainnya</th>
			<th>Mati</th>
			<th>Jumlah Total</th>
			<th>Mati</th>
			<th>Jumlah Total</th>
		</tr>
	");

	if (isset($_POST ['command'])) {
		$data_kategori_perinatologi = array(
			'Bayi Lahir Hidup', 'Kematian Perinatal', 'Sebab Kematian'
		);
		if ($_POST['command'] == "view") {
			$tahun = $_POST['tahun'];
			$bulan = $_POST['bulan'];
			$html = "";

			$kode_rs = "";
			$nama_rs = "";
			$kode_propinsi = "";
			$kabupaten_kota = "";
			$info_rs_row = $db->get_row("
				SELECT 
					*
				FROM 
					smis_mr_data_dasar_rs
				WHERE 
					prop = ''
				LIMIT 
					0, 1
			");
			if ($info_rs_row != null) {
				$kode_rs = $info_rs_row->koders;
				$nama_rs = $info_rs_row->nama_rs;
				$kode_propinsi = $info_rs_row->kode_provinsi;
				$kabupaten_kota = $info_rs_row->kota_kabupaten;
			}

			$data_perinatologi = array(
				">= 2500 gram" => array(
					'kategori'	=> 'Bayi Lahir Hidup',
					"rm-rs" 	=> array( 
						'cara_datang'			=> 'Rujukan',
						'konj_asal_rujukan'		=> 'LIKE',
						'asal_rujukan'			=> 'RS',
						'bayi_lahir_hidup'		=> '>= 2500 gram',
						'kematian_perinatal'	=> '',
						'sebab_kematian'		=> '',
						'dirujuk'				=> '%%'
					),
					"rm-bidan" 	=> array( 
						'cara_datang'			=> 'Rujukan',
						'konj_asal_rujukan'		=> 'LIKE',
						'asal_rujukan'			=> 'Bidan',
						'bayi_lahir_hidup'		=> '>= 2500 gram',
						'kematian_perinatal'	=> '',
						'sebab_kematian'		=> '',
						'dirujuk'				=> '%%'
					),
					"rm-puskesmas" 	=> array( 
						'cara_datang'			=> 'Rujukan',
						'konj_asal_rujukan'		=> 'LIKE',
						'asal_rujukan'			=> 'Puskesmas',
						'bayi_lahir_hidup'		=> '>= 2500 gram',
						'kematian_perinatal'	=> '',
						'sebab_kematian'		=> '',
						'dirujuk'				=> '%%'
					),
					"rm-faskes" 	=> array( 
						'cara_datang'			=> 'Rujukan',
						'konj_asal_rujukan'		=> 'LIKE',
						'asal_rujukan'			=> 'Faskes Lainnya',
						'bayi_lahir_hidup'		=> '>= 2500 gram',
						'kematian_perinatal'	=> '',
						'sebab_kematian'		=> '',
						'dirujuk'				=> '%%'
					),
					"rm-mati" 		=> array( 
						'cara_datang'			=> 'Rujukan',
						'konj_asal_rujukan'		=> 'NOT LIKE',
						'asal_rujukan'			=> 'Non Medis',
						'bayi_lahir_hidup'		=> '>= 2500 gram',
						'kematian_perinatal'	=> '%Mati%',
						'sebab_kematian'		=> '%%',
						'dirujuk'				=> '%%'
					),
					"rm-total" 		=> array( 
						'cara_datang'			=> 'Rujukan',
						'konj_asal_rujukan'		=> 'NOT LIKE',
						'asal_rujukan'			=> 'Non Medis',
						'bayi_lahir_hidup'		=> '>= 2500 gram',
						'kematian_perinatal'	=> '%%',
						'sebab_kematian'		=> '%%',
						'dirujuk'				=> '%%'
					),
					"rnm-mati" 		=> array( 
						'cara_datang'			=> 'Rujukan',
						'konj_asal_rujukan'		=> 'LIKE',
						'asal_rujukan'			=> 'Non Medis',
						'bayi_lahir_hidup'		=> '>= 2500 gram',
						'kematian_perinatal'	=> '%Mati%',
						'sebab_kematian'		=> '%%',
						'dirujuk'				=> '%%'
					),
					"rnm-total" 	=> array(
						'cara_datang'			=> 'Rujukan',
						'konj_asal_rujukan'		=> 'LIKE',
						'asal_rujukan'			=> 'Non Medis',
						'bayi_lahir_hidup'		=> '>= 2500 gram',
						'kematian_perinatal'	=> '%%',
						'sebab_kematian'		=> '%%',
						'dirujuk'				=> '%%'
					),
					"nr-mati" 		=> array( 
						'cara_datang'			=> 'Non-Rujukan',
						'konj_asal_rujukan'		=> 'LIKE',
						'asal_rujukan'			=> '%%',
						'bayi_lahir_hidup'		=> '>= 2500 gram',
						'kematian_perinatal'	=> '%Mati%',
						'sebab_kematian'		=> '%%',
						'dirujuk'				=> '%%'
					),
					"nr-total" 		=> array(
						'cara_datang'			=> 'Non-Rujukan',
						'konj_asal_rujukan'		=> 'LIKE',
						'asal_rujukan'			=> '%%',
						'bayi_lahir_hidup'		=> '>= 2500 gram',
						'kematian_perinatal'	=> '%%',
						'sebab_kematian'		=> '%%',
						'dirujuk'				=> '%%'
					),
					"dirujuk" 		=> array( 
						'cara_datang'			=> '%%',
						'konj_asal_rujukan'		=> 'LIKE',
						'asal_rujukan'			=> '%%',
						'bayi_lahir_hidup'		=> '>= 2500 gram',
						'kematian_perinatal'	=> '%%',
						'sebab_kematian'		=> '%%',
						'dirujuk'				=> '1'
					)
				),
				"< 2500 gram" => array(
					'kategori'	=> 'Bayi Lahir Hidup',
					"rm-rs" 	=> array( 
						'cara_datang'			=> 'Rujukan',
						'konj_asal_rujukan'		=> 'LIKE',
						'asal_rujukan'			=> 'RS',
						'bayi_lahir_hidup'		=> '< 2500 gram',
						'kematian_perinatal'	=> '',
						'sebab_kematian'		=> '',
						'dirujuk'				=> '%%'
					),
					"rm-bidan" 	=> array( 
						'cara_datang'			=> 'Rujukan',
						'konj_asal_rujukan'		=> 'LIKE',
						'asal_rujukan'			=> 'Bidan',
						'bayi_lahir_hidup'		=> '< 2500 gram',
						'kematian_perinatal'	=> '',
						'sebab_kematian'		=> '',
						'dirujuk'				=> '%%'
					),
					"rm-puskesmas" 	=> array( 
						'cara_datang'			=> 'Rujukan',
						'konj_asal_rujukan'		=> 'LIKE',
						'asal_rujukan'			=> 'Puskesmas',
						'bayi_lahir_hidup'		=> '< 2500 gram',
						'kematian_perinatal'	=> '',
						'sebab_kematian'		=> '',
						'dirujuk'				=> '%%'
					),
					"rm-faskes" 	=> array( 
						'cara_datang'			=> 'Rujukan',
						'konj_asal_rujukan'		=> 'LIKE',
						'asal_rujukan'			=> 'Faskes Lainnya',
						'bayi_lahir_hidup'		=> '< 2500 gram',
						'kematian_perinatal'	=> '',
						'sebab_kematian'		=> '',
						'dirujuk'				=> '%%'
					),
					"rm-mati" 		=> array( 
						'cara_datang'			=> 'Rujukan',
						'konj_asal_rujukan'		=> 'NOT LIKE',
						'asal_rujukan'			=> 'Non Medis',
						'bayi_lahir_hidup'		=> '< 2500 gram',
						'kematian_perinatal'	=> '%Mati%',
						'sebab_kematian'		=> '%%',
						'dirujuk'				=> '%%'
					),
					"rm-total" 		=> array( 
						'cara_datang'			=> 'Rujukan',
						'konj_asal_rujukan'		=> 'NOT LIKE',
						'asal_rujukan'			=> 'Non Medis',
						'bayi_lahir_hidup'		=> '< 2500 gram',
						'kematian_perinatal'	=> '%%',
						'sebab_kematian'		=> '%%',
						'dirujuk'				=> '%%'
					),
					"rnm-mati" 		=> array( 
						'cara_datang'			=> 'Rujukan',
						'konj_asal_rujukan'		=> 'LIKE',
						'asal_rujukan'			=> 'Non Medis',
						'bayi_lahir_hidup'		=> '< 2500 gram',
						'kematian_perinatal'	=> '%Mati%',
						'sebab_kematian'		=> '%%',
						'dirujuk'				=> '%%'
					),
					"rnm-total" 	=> array(
						'cara_datang'			=> 'Rujukan',
						'konj_asal_rujukan'		=> 'LIKE',
						'asal_rujukan'			=> 'Non Medis',
						'bayi_lahir_hidup'		=> '< 2500 gram',
						'kematian_perinatal'	=> '%%',
						'sebab_kematian'		=> '%%',
						'dirujuk'				=> '%%'
					),
					"nr-mati" 		=> array( 
						'cara_datang'			=> 'Non-Rujukan',
						'konj_asal_rujukan'		=> 'LIKE',
						'asal_rujukan'			=> '%%',
						'bayi_lahir_hidup'		=> '< 2500 gram',
						'kematian_perinatal'	=> '%Mati%',
						'sebab_kematian'		=> '%%',
						'dirujuk'				=> '%%'
					),
					"nr-total" 		=> array(
						'cara_datang'			=> 'Non-Rujukan',
						'konj_asal_rujukan'		=> 'LIKE',
						'asal_rujukan'			=> '%%',
						'bayi_lahir_hidup'		=> '< 2500 gram',
						'kematian_perinatal'	=> '%%',
						'sebab_kematian'		=> '%%',
						'dirujuk'				=> '%%'
					),
					"dirujuk" 		=> array( 
						'cara_datang'			=> '%%',
						'konj_asal_rujukan'		=> 'LIKE',
						'asal_rujukan'			=> '%%',
						'bayi_lahir_hidup'		=> '< 2500 gram',
						'kematian_perinatal'	=> '%%',
						'sebab_kematian'		=> '%%',
						'dirujuk'				=> '1'
					)
				),

				"Kelahiran Mati" => array(
					'kategori'	=> 'Kematian Perinatal',
					"rm-rs" 	=> array( 
						'cara_datang'			=> 'Rujukan',
						'konj_asal_rujukan'		=> 'LIKE',
						'asal_rujukan'			=> 'RS',
						'bayi_lahir_hidup'		=> '',
						'kematian_perinatal'	=> 'Kelahiran Mati',
						'sebab_kematian'		=> '%%',
						'dirujuk'				=> '%%'
					),
					"rm-bidan" 	=> array( 
						'cara_datang'			=> 'Rujukan',
						'konj_asal_rujukan'		=> 'LIKE',
						'asal_rujukan'			=> 'Bidan',
						'bayi_lahir_hidup'		=> '',
						'kematian_perinatal'	=> 'Kelahiran Mati',
						'sebab_kematian'		=> '%%',
						'dirujuk'				=> '%%'
					),
					"rm-puskesmas" 	=> array( 
						'cara_datang'			=> 'Rujukan',
						'konj_asal_rujukan'		=> 'LIKE',
						'asal_rujukan'			=> 'Puskesmas',
						'bayi_lahir_hidup'		=> '',
						'kematian_perinatal'	=> 'Kelahiran Mati',
						'sebab_kematian'		=> '%%',
						'dirujuk'				=> '%%'
					),
					"rm-faskes" 	=> array( 
						'cara_datang'			=> 'Rujukan',
						'konj_asal_rujukan'		=> 'LIKE',
						'asal_rujukan'			=> 'Faskes Lainnya',
						'bayi_lahir_hidup'		=> '',
						'kematian_perinatal'	=> 'Kelahiran Mati',
						'sebab_kematian'		=> '%%',
						'dirujuk'				=> '%%'
					),
					"rm-mati" 		=> array( 
						'cara_datang'			=> 'Rujukan',
						'konj_asal_rujukan'		=> 'NOT LIKE',
						'asal_rujukan'			=> 'Non Medis',
						'bayi_lahir_hidup'		=> '',
						'kematian_perinatal'	=> 'Kelahiran Mati',
						'sebab_kematian'		=> '%%',
						'dirujuk'				=> '%%'
					),
					"rm-total" 		=> array( 
						'cara_datang'			=> 'Rujukan',
						'konj_asal_rujukan'		=> 'NOT LIKE',
						'asal_rujukan'			=> 'Non Medis',
						'bayi_lahir_hidup'		=> '',
						'kematian_perinatal'	=> 'Kelahiran Mati',
						'sebab_kematian'		=> '%%',
						'dirujuk'				=> '%%'
					),
					"rnm-mati" 		=> array( 
						'cara_datang'			=> 'Rujukan',
						'konj_asal_rujukan'		=> 'LIKE',
						'asal_rujukan'			=> 'Non Medis',
						'bayi_lahir_hidup'		=> '',
						'kematian_perinatal'	=> 'Kelahiran Mati',
						'sebab_kematian'		=> '%%',
						'dirujuk'				=> '%%'
					),
					"rnm-total" 	=> array(
						'cara_datang'			=> 'Rujukan',
						'konj_asal_rujukan'		=> 'LIKE',
						'asal_rujukan'			=> 'Non Medis',
						'bayi_lahir_hidup'		=> '',
						'kematian_perinatal'	=> 'Kelahiran Mati',
						'sebab_kematian'		=> '%%',
						'dirujuk'				=> '%%'
					),
					"nr-mati" 		=> array( 
						'cara_datang'			=> 'Non-Rujukan',
						'konj_asal_rujukan'		=> 'LIKE',
						'asal_rujukan'			=> '%%',
						'bayi_lahir_hidup'		=> '',
						'kematian_perinatal'	=> 'Kelahiran Mati',
						'sebab_kematian'		=> '%%',
						'dirujuk'				=> '%%'
					),
					"nr-total" 		=> array(
						'cara_datang'			=> 'Non-Rujukan',
						'konj_asal_rujukan'		=> 'LIKE',
						'asal_rujukan'			=> '%%',
						'bayi_lahir_hidup'		=> '',
						'kematian_perinatal'	=> 'Kelahiran Mati',
						'sebab_kematian'		=> '%%',
						'dirujuk'				=> '%%'
					),
					"dirujuk" 		=> array( 
						'cara_datang'			=> '%%',
						'konj_asal_rujukan'		=> 'LIKE',
						'asal_rujukan'			=> '%%',
						'bayi_lahir_hidup'		=> '%%',
						'kematian_perinatal'	=> 'Kelahiran Mati',
						'sebab_kematian'		=> '%%',
						'dirujuk'				=> '1'
					)
				),
				"Mati Neonatal < 7 hari" => array(
					'kategori'	=> 'Kematian Perinatal',
					"rm-rs" 	=> array( 
						'cara_datang'			=> 'Rujukan',
						'konj_asal_rujukan'		=> 'LIKE',
						'asal_rujukan'			=> 'RS',
						'bayi_lahir_hidup'		=> '',
						'kematian_perinatal'	=> 'Mati Neonatal < 7 hari',
						'sebab_kematian'		=> '%%',
						'dirujuk'				=> '%%'
					),
					"rm-bidan" 	=> array( 
						'cara_datang'			=> 'Rujukan',
						'konj_asal_rujukan'		=> 'LIKE',
						'asal_rujukan'			=> 'Bidan',
						'bayi_lahir_hidup'		=> '',
						'kematian_perinatal'	=> 'Mati Neonatal < 7 hari',
						'sebab_kematian'		=> '%%',
						'dirujuk'				=> '%%'
					),
					"rm-puskesmas" 	=> array( 
						'cara_datang'			=> 'Rujukan',
						'konj_asal_rujukan'		=> 'LIKE',
						'asal_rujukan'			=> 'Puskesmas',
						'bayi_lahir_hidup'		=> '',
						'kematian_perinatal'	=> 'Mati Neonatal < 7 hari',
						'sebab_kematian'		=> '%%',
						'dirujuk'				=> '%%'
					),
					"rm-faskes" 	=> array( 
						'cara_datang'			=> 'Rujukan',
						'konj_asal_rujukan'		=> 'LIKE',
						'asal_rujukan'			=> 'Faskes Lainnya',
						'bayi_lahir_hidup'		=> '',
						'kematian_perinatal'	=> 'Mati Neonatal < 7 hari',
						'sebab_kematian'		=> '%%',
						'dirujuk'				=> '%%'
					),
					"rm-mati" 		=> array( 
						'cara_datang'			=> 'Rujukan',
						'konj_asal_rujukan'		=> 'NOT LIKE',
						'asal_rujukan'			=> 'Non Medis',
						'bayi_lahir_hidup'		=> '',
						'kematian_perinatal'	=> 'Mati Neonatal < 7 hari',
						'sebab_kematian'		=> '%%',
						'dirujuk'				=> '%%'
					),
					"rm-total" 		=> array( 
						'cara_datang'			=> 'Rujukan',
						'konj_asal_rujukan'		=> 'NOT LIKE',
						'asal_rujukan'			=> 'Non Medis',
						'bayi_lahir_hidup'		=> '',
						'kematian_perinatal'	=> 'Mati Neonatal < 7 hari',
						'sebab_kematian'		=> '%%',
						'dirujuk'				=> '%%'
					),
					"rnm-mati" 		=> array( 
						'cara_datang'			=> 'Rujukan',
						'konj_asal_rujukan'		=> 'LIKE',
						'asal_rujukan'			=> 'Non Medis',
						'bayi_lahir_hidup'		=> '',
						'kematian_perinatal'	=> 'Mati Neonatal < 7 hari',
						'sebab_kematian'		=> '%%',
						'dirujuk'				=> '%%'
					),
					"rnm-total" 	=> array(
						'cara_datang'			=> 'Rujukan',
						'konj_asal_rujukan'		=> 'LIKE',
						'asal_rujukan'			=> 'Non Medis',
						'bayi_lahir_hidup'		=> '',
						'kematian_perinatal'	=> 'Mati Neonatal < 7 hari',
						'sebab_kematian'		=> '%%',
						'dirujuk'				=> '%%'
					),
					"nr-mati" 		=> array( 
						'cara_datang'			=> 'Non-Rujukan',
						'konj_asal_rujukan'		=> 'LIKE',
						'asal_rujukan'			=> '%%',
						'bayi_lahir_hidup'		=> '',
						'kematian_perinatal'	=> 'Mati Neonatal < 7 hari',
						'sebab_kematian'		=> '%%',
						'dirujuk'				=> '%%'
					),
					"nr-total" 		=> array(
						'cara_datang'			=> 'Non-Rujukan',
						'konj_asal_rujukan'		=> 'LIKE',
						'asal_rujukan'			=> '%%',
						'bayi_lahir_hidup'		=> '',
						'kematian_perinatal'	=> 'Mati Neonatal < 7 hari',
						'sebab_kematian'		=> '%%',
						'dirujuk'				=> '%%'
					),
					"dirujuk" 		=> array( 
						'cara_datang'			=> '%%',
						'konj_asal_rujukan'		=> 'LIKE',
						'asal_rujukan'			=> '%%',
						'bayi_lahir_hidup'		=> '%%',
						'kematian_perinatal'	=> 'Mati Neonatal < 7 hari',
						'sebab_kematian'		=> '%%',
						'dirujuk'				=> '1'
					)
				),

				"Asphyxia" => array(
					'kategori'	=> 'Sebab Kematian',
					"rm-rs" 	=> array( 
						'cara_datang'			=> 'Rujukan',
						'konj_asal_rujukan'		=> 'LIKE',
						'asal_rujukan'			=> 'RS',
						'bayi_lahir_hidup'		=> '',
						'kematian_perinatal'	=> '%%',
						'sebab_kematian'		=> 'Asphyxia',
						'dirujuk'				=> '%%'
					),
					"rm-bidan" 	=> array( 
						'cara_datang'			=> 'Rujukan',
						'konj_asal_rujukan'		=> 'LIKE',
						'asal_rujukan'			=> 'Bidan',
						'bayi_lahir_hidup'		=> '',
						'kematian_perinatal'	=> '%%',
						'sebab_kematian'		=> 'Asphyxia',
						'dirujuk'				=> '%%'
					),
					"rm-puskesmas" 	=> array( 
						'cara_datang'			=> 'Rujukan',
						'konj_asal_rujukan'		=> 'LIKE',
						'asal_rujukan'			=> 'Puskesmas',
						'bayi_lahir_hidup'		=> '',
						'kematian_perinatal'	=> '%%',
						'sebab_kematian'		=> 'Asphyxia',
						'dirujuk'				=> '%%'
					),
					"rm-faskes" 	=> array( 
						'cara_datang'			=> 'Rujukan',
						'konj_asal_rujukan'		=> 'LIKE',
						'asal_rujukan'			=> 'Faskes Lainnya',
						'bayi_lahir_hidup'		=> '',
						'kematian_perinatal'	=> '%%',
						'sebab_kematian'		=> 'Asphyxia',
						'dirujuk'				=> '%%'
					),
					"rm-mati" 		=> array( 
						'cara_datang'			=> 'Rujukan',
						'konj_asal_rujukan'		=> 'NOT LIKE',
						'asal_rujukan'			=> 'Non Medis',
						'bayi_lahir_hidup'		=> '',
						'kematian_perinatal'	=> '%%',
						'sebab_kematian'		=> 'Asphyxia',
						'dirujuk'				=> '%%'
					),
					"rm-total" 		=> array( 
						'cara_datang'			=> 'Rujukan',
						'konj_asal_rujukan'		=> 'NOT LIKE',
						'asal_rujukan'			=> 'Non Medis',
						'bayi_lahir_hidup'		=> '',
						'kematian_perinatal'	=> '%%',
						'sebab_kematian'		=> 'Asphyxia',
						'dirujuk'				=> '%%'
					),
					"rnm-mati" 		=> array( 
						'cara_datang'			=> 'Rujukan',
						'konj_asal_rujukan'		=> 'LIKE',
						'asal_rujukan'			=> 'Non Medis',
						'bayi_lahir_hidup'		=> '',
						'kematian_perinatal'	=> '%%',
						'sebab_kematian'		=> 'Asphyxia',
						'dirujuk'				=> '%%'
					),
					"rnm-total" 	=> array(
						'cara_datang'			=> 'Rujukan',
						'konj_asal_rujukan'		=> 'LIKE',
						'asal_rujukan'			=> 'Non Medis',
						'bayi_lahir_hidup'		=> '',
						'kematian_perinatal'	=> '%%',
						'sebab_kematian'		=> 'Asphyxia',
						'dirujuk'				=> '%%'
					),
					"nr-mati" 		=> array( 
						'cara_datang'			=> 'Non-Rujukan',
						'konj_asal_rujukan'		=> 'LIKE',
						'asal_rujukan'			=> '%%',
						'bayi_lahir_hidup'		=> '',
						'kematian_perinatal'	=> '%%',
						'sebab_kematian'		=> 'Asphyxia',
						'dirujuk'				=> '%%'
					),
					"nr-total" 		=> array(
						'cara_datang'			=> 'Non-Rujukan',
						'konj_asal_rujukan'		=> 'LIKE',
						'asal_rujukan'			=> '%%',
						'bayi_lahir_hidup'		=> '',
						'kematian_perinatal'	=> '%%',
						'sebab_kematian'		=> 'Asphyxia',
						'dirujuk'				=> '%%'
					),
					"dirujuk" 		=> array( 
						'cara_datang'			=> '%%',
						'konj_asal_rujukan'		=> 'LIKE',
						'asal_rujukan'			=> '%%',
						'bayi_lahir_hidup'		=> '%%',
						'kematian_perinatal'	=> '%%',
						'sebab_kematian'		=> 'Asphyxia',
						'dirujuk'				=> '1'
					)
				),
				"Trauma Kelahiran" => array(
					'kategori'	=> 'Sebab Kematian',
					"rm-rs" 	=> array( 
						'cara_datang'			=> 'Rujukan',
						'konj_asal_rujukan'		=> 'LIKE',
						'asal_rujukan'			=> 'RS',
						'bayi_lahir_hidup'		=> '',
						'kematian_perinatal'	=> '%%',
						'sebab_kematian'		=> 'Trauma Kelahiran',
						'dirujuk'				=> '%%'
					),
					"rm-bidan" 	=> array( 
						'cara_datang'			=> 'Rujukan',
						'konj_asal_rujukan'		=> 'LIKE',
						'asal_rujukan'			=> 'Bidan',
						'bayi_lahir_hidup'		=> '',
						'kematian_perinatal'	=> '%%',
						'sebab_kematian'		=> 'Trauma Kelahiran',
						'dirujuk'				=> '%%'
					),
					"rm-puskesmas" 	=> array( 
						'cara_datang'			=> 'Rujukan',
						'konj_asal_rujukan'		=> 'LIKE',
						'asal_rujukan'			=> 'Puskesmas',
						'bayi_lahir_hidup'		=> '',
						'kematian_perinatal'	=> '%%',
						'sebab_kematian'		=> 'Trauma Kelahiran',
						'dirujuk'				=> '%%'
					),
					"rm-faskes" 	=> array( 
						'cara_datang'			=> 'Rujukan',
						'konj_asal_rujukan'		=> 'LIKE',
						'asal_rujukan'			=> 'Faskes Lainnya',
						'bayi_lahir_hidup'		=> '',
						'kematian_perinatal'	=> '%%',
						'sebab_kematian'		=> 'Trauma Kelahiran',
						'dirujuk'				=> '%%'
					),
					"rm-mati" 		=> array( 
						'cara_datang'			=> 'Rujukan',
						'konj_asal_rujukan'		=> 'NOT LIKE',
						'asal_rujukan'			=> 'Non Medis',
						'bayi_lahir_hidup'		=> '',
						'kematian_perinatal'	=> '%%',
						'sebab_kematian'		=> 'Trauma Kelahiran',
						'dirujuk'				=> '%%'
					),
					"rm-total" 		=> array( 
						'cara_datang'			=> 'Rujukan',
						'konj_asal_rujukan'		=> 'NOT LIKE',
						'asal_rujukan'			=> 'Non Medis',
						'bayi_lahir_hidup'		=> '',
						'kematian_perinatal'	=> '%%',
						'sebab_kematian'		=> 'Trauma Kelahiran',
						'dirujuk'				=> '%%'
					),
					"rnm-mati" 		=> array( 
						'cara_datang'			=> 'Rujukan',
						'konj_asal_rujukan'		=> 'LIKE',
						'asal_rujukan'			=> 'Non Medis',
						'bayi_lahir_hidup'		=> '',
						'kematian_perinatal'	=> '%%',
						'sebab_kematian'		=> 'Trauma Kelahiran',
						'dirujuk'				=> '%%'
					),
					"rnm-total" 	=> array(
						'cara_datang'			=> 'Rujukan',
						'konj_asal_rujukan'		=> 'LIKE',
						'asal_rujukan'			=> 'Non Medis',
						'bayi_lahir_hidup'		=> '',
						'kematian_perinatal'	=> '%%',
						'sebab_kematian'		=> 'Trauma Kelahiran',
						'dirujuk'				=> '%%'
					),
					"nr-mati" 		=> array( 
						'cara_datang'			=> 'Non-Rujukan',
						'konj_asal_rujukan'		=> 'LIKE',
						'asal_rujukan'			=> '%%',
						'bayi_lahir_hidup'		=> '',
						'kematian_perinatal'	=> '%%',
						'sebab_kematian'		=> 'Trauma Kelahiran',
						'dirujuk'				=> '%%'
					),
					"nr-total" 		=> array(
						'cara_datang'			=> 'Non-Rujukan',
						'konj_asal_rujukan'		=> 'LIKE',
						'asal_rujukan'			=> '%%',
						'bayi_lahir_hidup'		=> '',
						'kematian_perinatal'	=> '%%',
						'sebab_kematian'		=> 'Trauma Kelahiran',
						'dirujuk'				=> '%%'
					),
					"dirujuk" 		=> array( 
						'cara_datang'			=> '%%',
						'konj_asal_rujukan'		=> 'LIKE',
						'asal_rujukan'			=> '%%',
						'bayi_lahir_hidup'		=> '%%',
						'kematian_perinatal'	=> '%%',
						'sebab_kematian'		=> 'Trauma Kelahiran',
						'dirujuk'				=> '1'
					)
				),
				"BBLR" => array(
					'kategori'	=> 'Sebab Kematian',
					"rm-rs" 	=> array( 
						'cara_datang'			=> 'Rujukan',
						'konj_asal_rujukan'		=> 'LIKE',
						'asal_rujukan'			=> 'RS',
						'bayi_lahir_hidup'		=> '',
						'kematian_perinatal'	=> '%%',
						'sebab_kematian'		=> 'BBLR',
						'dirujuk'				=> '%%'
					),
					"rm-bidan" 	=> array( 
						'cara_datang'			=> 'Rujukan',
						'konj_asal_rujukan'		=> 'LIKE',
						'asal_rujukan'			=> 'Bidan',
						'bayi_lahir_hidup'		=> '',
						'kematian_perinatal'	=> '%%',
						'sebab_kematian'		=> 'BBLR',
						'dirujuk'				=> '%%'
					),
					"rm-puskesmas" 	=> array( 
						'cara_datang'			=> 'Rujukan',
						'konj_asal_rujukan'		=> 'LIKE',
						'asal_rujukan'			=> 'Puskesmas',
						'bayi_lahir_hidup'		=> '',
						'kematian_perinatal'	=> '%%',
						'sebab_kematian'		=> 'BBLR',
						'dirujuk'				=> '%%'
					),
					"rm-faskes" 	=> array( 
						'cara_datang'			=> 'Rujukan',
						'konj_asal_rujukan'		=> 'LIKE',
						'asal_rujukan'			=> 'Faskes Lainnya',
						'bayi_lahir_hidup'		=> '',
						'kematian_perinatal'	=> '%%',
						'sebab_kematian'		=> 'BBLR',
						'dirujuk'				=> '%%'
					),
					"rm-mati" 		=> array( 
						'cara_datang'			=> 'Rujukan',
						'konj_asal_rujukan'		=> 'NOT LIKE',
						'asal_rujukan'			=> 'Non Medis',
						'bayi_lahir_hidup'		=> '',
						'kematian_perinatal'	=> '%%',
						'sebab_kematian'		=> 'BBLR',
						'dirujuk'				=> '%%'
					),
					"rm-total" 		=> array( 
						'cara_datang'			=> 'Rujukan',
						'konj_asal_rujukan'		=> 'NOT LIKE',
						'asal_rujukan'			=> 'Non Medis',
						'bayi_lahir_hidup'		=> '',
						'kematian_perinatal'	=> '%%',
						'sebab_kematian'		=> 'BBLR',
						'dirujuk'				=> '%%'
					),
					"rnm-mati" 		=> array( 
						'cara_datang'			=> 'Rujukan',
						'konj_asal_rujukan'		=> 'LIKE',
						'asal_rujukan'			=> 'Non Medis',
						'bayi_lahir_hidup'		=> '',
						'kematian_perinatal'	=> '%%',
						'sebab_kematian'		=> 'BBLR',
						'dirujuk'				=> '%%'
					),
					"rnm-total" 	=> array(
						'cara_datang'			=> 'Rujukan',
						'konj_asal_rujukan'		=> 'LIKE',
						'asal_rujukan'			=> 'Non Medis',
						'bayi_lahir_hidup'		=> '',
						'kematian_perinatal'	=> '%%',
						'sebab_kematian'		=> 'BBLR',
						'dirujuk'				=> '%%'
					),
					"nr-mati" 		=> array( 
						'cara_datang'			=> 'Non-Rujukan',
						'konj_asal_rujukan'		=> 'LIKE',
						'asal_rujukan'			=> '%%',
						'bayi_lahir_hidup'		=> '',
						'kematian_perinatal'	=> '%%',
						'sebab_kematian'		=> 'BBLR',
						'dirujuk'				=> '%%'
					),
					"nr-total" 		=> array(
						'cara_datang'			=> 'Non-Rujukan',
						'konj_asal_rujukan'		=> 'LIKE',
						'asal_rujukan'			=> '%%',
						'bayi_lahir_hidup'		=> '',
						'kematian_perinatal'	=> '%%',
						'sebab_kematian'		=> 'BBLR',
						'dirujuk'				=> '%%'
					),
					"dirujuk" 		=> array( 
						'cara_datang'			=> '%%',
						'konj_asal_rujukan'		=> 'LIKE',
						'asal_rujukan'			=> '%%',
						'bayi_lahir_hidup'		=> '%%',
						'kematian_perinatal'	=> '%%',
						'sebab_kematian'		=> 'BBLR',
						'dirujuk'				=> '1'
					)
				),
				"Tetanus Neonatorum" => array(
					'kategori'	=> 'Sebab Kematian',
					"rm-rs" 	=> array( 
						'cara_datang'			=> 'Rujukan',
						'konj_asal_rujukan'		=> 'LIKE',
						'asal_rujukan'			=> 'RS',
						'bayi_lahir_hidup'		=> '',
						'kematian_perinatal'	=> '%%',
						'sebab_kematian'		=> 'Tetanus Neonatorum',
						'dirujuk'				=> '%%'
					),
					"rm-bidan" 	=> array( 
						'cara_datang'			=> 'Rujukan',
						'konj_asal_rujukan'		=> 'LIKE',
						'asal_rujukan'			=> 'Bidan',
						'bayi_lahir_hidup'		=> '',
						'kematian_perinatal'	=> '%%',
						'sebab_kematian'		=> 'Tetanus Neonatorum',
						'dirujuk'				=> '%%'
					),
					"rm-puskesmas" 	=> array( 
						'cara_datang'			=> 'Rujukan',
						'konj_asal_rujukan'		=> 'LIKE',
						'asal_rujukan'			=> 'Puskesmas',
						'bayi_lahir_hidup'		=> '',
						'kematian_perinatal'	=> '%%',
						'sebab_kematian'		=> 'Tetanus Neonatorum',
						'dirujuk'				=> '%%'
					),
					"rm-faskes" 	=> array( 
						'cara_datang'			=> 'Rujukan',
						'konj_asal_rujukan'		=> 'LIKE',
						'asal_rujukan'			=> 'Faskes Lainnya',
						'bayi_lahir_hidup'		=> '',
						'kematian_perinatal'	=> '%%',
						'sebab_kematian'		=> 'Tetanus Neonatorum',
						'dirujuk'				=> '%%'
					),
					"rm-mati" 		=> array( 
						'cara_datang'			=> 'Rujukan',
						'konj_asal_rujukan'		=> 'NOT LIKE',
						'asal_rujukan'			=> 'Non Medis',
						'bayi_lahir_hidup'		=> '',
						'kematian_perinatal'	=> '%%',
						'sebab_kematian'		=> 'Tetanus Neonatorum',
						'dirujuk'				=> '%%'
					),
					"rm-total" 		=> array( 
						'cara_datang'			=> 'Rujukan',
						'konj_asal_rujukan'		=> 'NOT LIKE',
						'asal_rujukan'			=> 'Non Medis',
						'bayi_lahir_hidup'		=> '',
						'kematian_perinatal'	=> '%%',
						'sebab_kematian'		=> 'Tetanus Neonatorum',
						'dirujuk'				=> '%%'
					),
					"rnm-mati" 		=> array( 
						'cara_datang'			=> 'Rujukan',
						'konj_asal_rujukan'		=> 'LIKE',
						'asal_rujukan'			=> 'Non Medis',
						'bayi_lahir_hidup'		=> '',
						'kematian_perinatal'	=> '%%',
						'sebab_kematian'		=> 'Tetanus Neonatorum',
						'dirujuk'				=> '%%'
					),
					"rnm-total" 	=> array(
						'cara_datang'			=> 'Rujukan',
						'konj_asal_rujukan'		=> 'LIKE',
						'asal_rujukan'			=> 'Non Medis',
						'bayi_lahir_hidup'		=> '',
						'kematian_perinatal'	=> '%%',
						'sebab_kematian'		=> 'Tetanus Neonatorum',
						'dirujuk'				=> '%%'
					),
					"nr-mati" 		=> array( 
						'cara_datang'			=> 'Non-Rujukan',
						'konj_asal_rujukan'		=> 'LIKE',
						'asal_rujukan'			=> '%%',
						'bayi_lahir_hidup'		=> '',
						'kematian_perinatal'	=> '%%',
						'sebab_kematian'		=> 'Tetanus Neonatorum',
						'dirujuk'				=> '%%'
					),
					"nr-total" 		=> array(
						'cara_datang'			=> 'Non-Rujukan',
						'konj_asal_rujukan'		=> 'LIKE',
						'asal_rujukan'			=> '%%',
						'bayi_lahir_hidup'		=> '',
						'kematian_perinatal'	=> '%%',
						'sebab_kematian'		=> 'Tetanus Neonatorum',
						'dirujuk'				=> '%%'
					),
					"dirujuk" 		=> array( 
						'cara_datang'			=> '%%',
						'konj_asal_rujukan'		=> 'LIKE',
						'asal_rujukan'			=> '%%',
						'bayi_lahir_hidup'		=> '%%',
						'kematian_perinatal'	=> '%%',
						'sebab_kematian'		=> 'Tetanus Neonatorum',
						'dirujuk'				=> '1'
					)
				),
				"Kelainan Congenital" => array(
					'kategori'	=> 'Sebab Kematian',
					"rm-rs" 	=> array( 
						'cara_datang'			=> 'Rujukan',
						'konj_asal_rujukan'		=> 'LIKE',
						'asal_rujukan'			=> 'RS',
						'bayi_lahir_hidup'		=> '',
						'kematian_perinatal'	=> '%%',
						'sebab_kematian'		=> 'Kelainan Congenital',
						'dirujuk'				=> '%%'
					),
					"rm-bidan" 	=> array( 
						'cara_datang'			=> 'Rujukan',
						'konj_asal_rujukan'		=> 'LIKE',
						'asal_rujukan'			=> 'Bidan',
						'bayi_lahir_hidup'		=> '',
						'kematian_perinatal'	=> '%%',
						'sebab_kematian'		=> 'Kelainan Congenital',
						'dirujuk'				=> '%%'
					),
					"rm-puskesmas" 	=> array( 
						'cara_datang'			=> 'Rujukan',
						'konj_asal_rujukan'		=> 'LIKE',
						'asal_rujukan'			=> 'Puskesmas',
						'bayi_lahir_hidup'		=> '',
						'kematian_perinatal'	=> '%%',
						'sebab_kematian'		=> 'Kelainan Congenital',
						'dirujuk'				=> '%%'
					),
					"rm-faskes" 	=> array( 
						'cara_datang'			=> 'Rujukan',
						'konj_asal_rujukan'		=> 'LIKE',
						'asal_rujukan'			=> 'Faskes Lainnya',
						'bayi_lahir_hidup'		=> '',
						'kematian_perinatal'	=> '%%',
						'sebab_kematian'		=> 'Kelainan Congenital',
						'dirujuk'				=> '%%'
					),
					"rm-mati" 		=> array( 
						'cara_datang'			=> 'Rujukan',
						'konj_asal_rujukan'		=> 'NOT LIKE',
						'asal_rujukan'			=> 'Non Medis',
						'bayi_lahir_hidup'		=> '',
						'kematian_perinatal'	=> '%%',
						'sebab_kematian'		=> 'Kelainan Congenital',
						'dirujuk'				=> '%%'
					),
					"rm-total" 		=> array( 
						'cara_datang'			=> 'Rujukan',
						'konj_asal_rujukan'		=> 'NOT LIKE',
						'asal_rujukan'			=> 'Non Medis',
						'bayi_lahir_hidup'		=> '',
						'kematian_perinatal'	=> '%%',
						'sebab_kematian'		=> 'Kelainan Congenital',
						'dirujuk'				=> '%%'
					),
					"rnm-mati" 		=> array( 
						'cara_datang'			=> 'Rujukan',
						'konj_asal_rujukan'		=> 'LIKE',
						'asal_rujukan'			=> 'Non Medis',
						'bayi_lahir_hidup'		=> '',
						'kematian_perinatal'	=> '%%',
						'sebab_kematian'		=> 'Kelainan Congenital',
						'dirujuk'				=> '%%'
					),
					"rnm-total" 	=> array(
						'cara_datang'			=> 'Rujukan',
						'konj_asal_rujukan'		=> 'LIKE',
						'asal_rujukan'			=> 'Non Medis',
						'bayi_lahir_hidup'		=> '',
						'kematian_perinatal'	=> '%%',
						'sebab_kematian'		=> 'Kelainan Congenital',
						'dirujuk'				=> '%%'
					),
					"nr-mati" 		=> array( 
						'cara_datang'			=> 'Non-Rujukan',
						'konj_asal_rujukan'		=> 'LIKE',
						'asal_rujukan'			=> '%%',
						'bayi_lahir_hidup'		=> '',
						'kematian_perinatal'	=> '%%',
						'sebab_kematian'		=> 'Kelainan Congenital',
						'dirujuk'				=> '%%'
					),
					"nr-total" 		=> array(
						'cara_datang'			=> 'Non-Rujukan',
						'konj_asal_rujukan'		=> 'LIKE',
						'asal_rujukan'			=> '%%',
						'bayi_lahir_hidup'		=> '',
						'kematian_perinatal'	=> '%%',
						'sebab_kematian'		=> 'Kelainan Congenital',
						'dirujuk'				=> '%%'
					),
					"dirujuk" 		=> array( 
						'cara_datang'			=> '%%',
						'konj_asal_rujukan'		=> 'LIKE',
						'asal_rujukan'			=> '%%',
						'bayi_lahir_hidup'		=> '%%',
						'kematian_perinatal'	=> '%%',
						'sebab_kematian'		=> 'Kelainan Congenital',
						'dirujuk'				=> '1'
					)
				),
				"ISPA" => array(
					'kategori'	=> 'Sebab Kematian',
					"rm-rs" 	=> array( 
						'cara_datang'			=> 'Rujukan',
						'konj_asal_rujukan'		=> 'LIKE',
						'asal_rujukan'			=> 'RS',
						'bayi_lahir_hidup'		=> '',
						'kematian_perinatal'	=> '%%',
						'sebab_kematian'		=> 'ISPA',
						'dirujuk'				=> '%%'
					),
					"rm-bidan" 	=> array( 
						'cara_datang'			=> 'Rujukan',
						'konj_asal_rujukan'		=> 'LIKE',
						'asal_rujukan'			=> 'Bidan',
						'bayi_lahir_hidup'		=> '',
						'kematian_perinatal'	=> '%%',
						'sebab_kematian'		=> 'ISPA',
						'dirujuk'				=> '%%'
					),
					"rm-puskesmas" 	=> array( 
						'cara_datang'			=> 'Rujukan',
						'konj_asal_rujukan'		=> 'LIKE',
						'asal_rujukan'			=> 'Puskesmas',
						'bayi_lahir_hidup'		=> '',
						'kematian_perinatal'	=> '%%',
						'sebab_kematian'		=> 'ISPA',
						'dirujuk'				=> '%%'
					),
					"rm-faskes" 	=> array( 
						'cara_datang'			=> 'Rujukan',
						'konj_asal_rujukan'		=> 'LIKE',
						'asal_rujukan'			=> 'Faskes Lainnya',
						'bayi_lahir_hidup'		=> '',
						'kematian_perinatal'	=> '%%',
						'sebab_kematian'		=> 'ISPA',
						'dirujuk'				=> '%%'
					),
					"rm-mati" 		=> array( 
						'cara_datang'			=> 'Rujukan',
						'konj_asal_rujukan'		=> 'NOT LIKE',
						'asal_rujukan'			=> 'Non Medis',
						'bayi_lahir_hidup'		=> '',
						'kematian_perinatal'	=> '%%',
						'sebab_kematian'		=> 'ISPA',
						'dirujuk'				=> '%%'
					),
					"rm-total" 		=> array( 
						'cara_datang'			=> 'Rujukan',
						'konj_asal_rujukan'		=> 'NOT LIKE',
						'asal_rujukan'			=> 'Non Medis',
						'bayi_lahir_hidup'		=> '',
						'kematian_perinatal'	=> '%%',
						'sebab_kematian'		=> 'ISPA',
						'dirujuk'				=> '%%'
					),
					"rnm-mati" 		=> array( 
						'cara_datang'			=> 'Rujukan',
						'konj_asal_rujukan'		=> 'LIKE',
						'asal_rujukan'			=> 'Non Medis',
						'bayi_lahir_hidup'		=> '',
						'kematian_perinatal'	=> '%%',
						'sebab_kematian'		=> 'ISPA',
						'dirujuk'				=> '%%'
					),
					"rnm-total" 	=> array(
						'cara_datang'			=> 'Rujukan',
						'konj_asal_rujukan'		=> 'LIKE',
						'asal_rujukan'			=> 'Non Medis',
						'bayi_lahir_hidup'		=> '',
						'kematian_perinatal'	=> '%%',
						'sebab_kematian'		=> 'ISPA',
						'dirujuk'				=> '%%'
					),
					"nr-mati" 		=> array( 
						'cara_datang'			=> 'Non-Rujukan',
						'konj_asal_rujukan'		=> 'LIKE',
						'asal_rujukan'			=> '%%',
						'bayi_lahir_hidup'		=> '',
						'kematian_perinatal'	=> '%%',
						'sebab_kematian'		=> 'ISPA',
						'dirujuk'				=> '%%'
					),
					"nr-total" 		=> array(
						'cara_datang'			=> 'Non-Rujukan',
						'konj_asal_rujukan'		=> 'LIKE',
						'asal_rujukan'			=> '%%',
						'bayi_lahir_hidup'		=> '',
						'kematian_perinatal'	=> '%%',
						'sebab_kematian'		=> 'ISPA',
						'dirujuk'				=> '%%'
					),
					"dirujuk" 		=> array( 
						'cara_datang'			=> '%%',
						'konj_asal_rujukan'		=> 'LIKE',
						'asal_rujukan'			=> '%%',
						'bayi_lahir_hidup'		=> '%%',
						'kematian_perinatal'	=> '%%',
						'sebab_kematian'		=> 'ISPA',
						'dirujuk'				=> '1'
					)
				),
				"Diare" => array(
					'kategori'	=> 'Sebab Kematian',
					"rm-rs" 	=> array( 
						'cara_datang'			=> 'Rujukan',
						'konj_asal_rujukan'		=> 'LIKE',
						'asal_rujukan'			=> 'RS',
						'bayi_lahir_hidup'		=> '',
						'kematian_perinatal'	=> '%%',
						'sebab_kematian'		=> 'Diare',
						'dirujuk'				=> '%%'
					),
					"rm-bidan" 	=> array( 
						'cara_datang'			=> 'Rujukan',
						'konj_asal_rujukan'		=> 'LIKE',
						'asal_rujukan'			=> 'Bidan',
						'bayi_lahir_hidup'		=> '',
						'kematian_perinatal'	=> '%%',
						'sebab_kematian'		=> 'Diare',
						'dirujuk'				=> '%%'
					),
					"rm-puskesmas" 	=> array( 
						'cara_datang'			=> 'Rujukan',
						'konj_asal_rujukan'		=> 'LIKE',
						'asal_rujukan'			=> 'Puskesmas',
						'bayi_lahir_hidup'		=> '',
						'kematian_perinatal'	=> '%%',
						'sebab_kematian'		=> 'Diare',
						'dirujuk'				=> '%%'
					),
					"rm-faskes" 	=> array( 
						'cara_datang'			=> 'Rujukan',
						'konj_asal_rujukan'		=> 'LIKE',
						'asal_rujukan'			=> 'Faskes Lainnya',
						'bayi_lahir_hidup'		=> '',
						'kematian_perinatal'	=> '%%',
						'sebab_kematian'		=> 'Diare',
						'dirujuk'				=> '%%'
					),
					"rm-mati" 		=> array( 
						'cara_datang'			=> 'Rujukan',
						'konj_asal_rujukan'		=> 'NOT LIKE',
						'asal_rujukan'			=> 'Non Medis',
						'bayi_lahir_hidup'		=> '',
						'kematian_perinatal'	=> '%%',
						'sebab_kematian'		=> 'Diare',
						'dirujuk'				=> '%%'
					),
					"rm-total" 		=> array( 
						'cara_datang'			=> 'Rujukan',
						'konj_asal_rujukan'		=> 'NOT LIKE',
						'asal_rujukan'			=> 'Non Medis',
						'bayi_lahir_hidup'		=> '',
						'kematian_perinatal'	=> '%%',
						'sebab_kematian'		=> 'Diare',
						'dirujuk'				=> '%%'
					),
					"rnm-mati" 		=> array( 
						'cara_datang'			=> 'Rujukan',
						'konj_asal_rujukan'		=> 'LIKE',
						'asal_rujukan'			=> 'Non Medis',
						'bayi_lahir_hidup'		=> '',
						'kematian_perinatal'	=> '%%',
						'sebab_kematian'		=> 'Diare',
						'dirujuk'				=> '%%'
					),
					"rnm-total" 	=> array(
						'cara_datang'			=> 'Rujukan',
						'konj_asal_rujukan'		=> 'LIKE',
						'asal_rujukan'			=> 'Non Medis',
						'bayi_lahir_hidup'		=> '',
						'kematian_perinatal'	=> '%%',
						'sebab_kematian'		=> 'Diare',
						'dirujuk'				=> '%%'
					),
					"nr-mati" 		=> array( 
						'cara_datang'			=> 'Non-Rujukan',
						'konj_asal_rujukan'		=> 'LIKE',
						'asal_rujukan'			=> '%%',
						'bayi_lahir_hidup'		=> '',
						'kematian_perinatal'	=> '%%',
						'sebab_kematian'		=> 'Diare',
						'dirujuk'				=> '%%'
					),
					"nr-total" 		=> array(
						'cara_datang'			=> 'Non-Rujukan',
						'konj_asal_rujukan'		=> 'LIKE',
						'asal_rujukan'			=> '%%',
						'bayi_lahir_hidup'		=> '',
						'kematian_perinatal'	=> '%%',
						'sebab_kematian'		=> 'Diare',
						'dirujuk'				=> '%%'
					),
					"dirujuk" 		=> array( 
						'cara_datang'			=> '%%',
						'konj_asal_rujukan'		=> 'LIKE',
						'asal_rujukan'			=> '%%',
						'bayi_lahir_hidup'		=> '%%',
						'kematian_perinatal'	=> '%%',
						'sebab_kematian'		=> 'Diare',
						'dirujuk'				=> '1'
					)
				),
				"Lain-lain" => array(
					'kategori'	=> 'Sebab Kematian',
					"rm-rs" 	=> array( 
						'cara_datang'			=> 'Rujukan',
						'konj_asal_rujukan'		=> 'LIKE',
						'asal_rujukan'			=> 'RS',
						'bayi_lahir_hidup'		=> '',
						'kematian_perinatal'	=> '%%',
						'sebab_kematian'		=> 'Lain-lain',
						'dirujuk'				=> '%%'
					),
					"rm-bidan" 	=> array( 
						'cara_datang'			=> 'Rujukan',
						'konj_asal_rujukan'		=> 'LIKE',
						'asal_rujukan'			=> 'Bidan',
						'bayi_lahir_hidup'		=> '',
						'kematian_perinatal'	=> '%%',
						'sebab_kematian'		=> 'Lain-lain',
						'dirujuk'				=> '%%'
					),
					"rm-puskesmas" 	=> array( 
						'cara_datang'			=> 'Rujukan',
						'konj_asal_rujukan'		=> 'LIKE',
						'asal_rujukan'			=> 'Puskesmas',
						'bayi_lahir_hidup'		=> '',
						'kematian_perinatal'	=> '%%',
						'sebab_kematian'		=> 'Lain-lain',
						'dirujuk'				=> '%%'
					),
					"rm-faskes" 	=> array( 
						'cara_datang'			=> 'Rujukan',
						'konj_asal_rujukan'		=> 'LIKE',
						'asal_rujukan'			=> 'Faskes Lainnya',
						'bayi_lahir_hidup'		=> '',
						'kematian_perinatal'	=> '%%',
						'sebab_kematian'		=> 'Lain-lain',
						'dirujuk'				=> '%%'
					),
					"rm-mati" 		=> array( 
						'cara_datang'			=> 'Rujukan',
						'konj_asal_rujukan'		=> 'NOT LIKE',
						'asal_rujukan'			=> 'Non Medis',
						'bayi_lahir_hidup'		=> '',
						'kematian_perinatal'	=> '%%',
						'sebab_kematian'		=> 'Lain-lain',
						'dirujuk'				=> '%%'
					),
					"rm-total" 		=> array( 
						'cara_datang'			=> 'Rujukan',
						'konj_asal_rujukan'		=> 'NOT LIKE',
						'asal_rujukan'			=> 'Non Medis',
						'bayi_lahir_hidup'		=> '',
						'kematian_perinatal'	=> '%%',
						'sebab_kematian'		=> 'Lain-lain',
						'dirujuk'				=> '%%'
					),
					"rnm-mati" 		=> array( 
						'cara_datang'			=> 'Rujukan',
						'konj_asal_rujukan'		=> 'LIKE',
						'asal_rujukan'			=> 'Non Medis',
						'bayi_lahir_hidup'		=> '',
						'kematian_perinatal'	=> '%%',
						'sebab_kematian'		=> 'Lain-lain',
						'dirujuk'				=> '%%'
					),
					"rnm-total" 	=> array(
						'cara_datang'			=> 'Rujukan',
						'konj_asal_rujukan'		=> 'LIKE',
						'asal_rujukan'			=> 'Non Medis',
						'bayi_lahir_hidup'		=> '',
						'kematian_perinatal'	=> '%%',
						'sebab_kematian'		=> 'Lain-lain',
						'dirujuk'				=> '%%'
					),
					"nr-mati" 		=> array( 
						'cara_datang'			=> 'Non-Rujukan',
						'konj_asal_rujukan'		=> 'LIKE',
						'asal_rujukan'			=> '%%',
						'bayi_lahir_hidup'		=> '',
						'kematian_perinatal'	=> '%%',
						'sebab_kematian'		=> 'Lain-lain',
						'dirujuk'				=> '%%'
					),
					"nr-total" 		=> array(
						'cara_datang'			=> 'Non-Rujukan',
						'konj_asal_rujukan'		=> 'LIKE',
						'asal_rujukan'			=> '%%',
						'bayi_lahir_hidup'		=> '',
						'kematian_perinatal'	=> '%%',
						'sebab_kematian'		=> 'Lain-lain',
						'dirujuk'				=> '%%'
					),
					"dirujuk" 		=> array( 
						'cara_datang'			=> '%%',
						'konj_asal_rujukan'		=> 'LIKE',
						'asal_rujukan'			=> '%%',
						'bayi_lahir_hidup'		=> '%%',
						'kematian_perinatal'	=> '%%',
						'sebab_kematian'		=> 'Lain-lain',
						'dirujuk'				=> '1'
					)
				),
			);

			$nomor = 1;
			foreach ($data_kategori_perinatologi as $kategori) {
				$html .= 	"<tr>" .
								"<td>" . $kode_propinsi . "</td>" .
								"<td>" . $kabupaten_kota . "</td>" .
								"<td>" . $kode_rs . "</td>" .
								"<td>" . $nama_rs . "</td>" .
								"<td>" . $tahun . "</td>" .
								"<td>" . $nomor . "</td>" .
								"<td colspan='12'>" . $kategori . "</td>" .
							"</tr>";
				$subnomor = 1;
				foreach ($data_perinatologi as $nama_kegiatan => $dp) {
					if ($kategori == $dp['kategori']) {
						$rm_rs = 0;
						$rm_rs_row = $db->get_row("
							SELECT 
								COUNT(*) jumlah
							FROM
								smis_mr_perinatologi
							WHERE 
								prop = '' AND 
								cara_datang LIKE '" . $dp['rm-rs']['cara_datang'] . "' AND 
								asal_rujukan " . $dp['rm-rs']['konj_asal_rujukan'] . " '" . $dp['rm-rs']['asal_rujukan'] . "' AND
								bayi_lahir_hidup LIKE '" . $dp['rm-rs']['bayi_lahir_hidup'] . "' AND
								kematian_perinatal LIKE '" . $dp['rm-rs']['kematian_perinatal'] . "' AND
								sebab_kematian LIKE '" . $dp['rm-rs']['sebab_kematian'] . "' AND 
								dirujuk LIKE '" . $dp['rm-rs']['dirujuk'] . "' AND
								YEAR(tanggal) = '" . $tahun . "' AND
								MONTH(tanggal) LIKE '" . $bulan . "'
						");
						if ($rm_rs_row != null)
							$rm_rs = $rm_rs_row->jumlah;
						$rm_bidan = 0;
						$rm_bidan_row = $db->get_row("
							SELECT 
								COUNT(*) jumlah
							FROM
								smis_mr_perinatologi
							WHERE 
								prop = '' AND 
								cara_datang LIKE '" . $dp['rm-bidan']['cara_datang'] . "' AND 
								asal_rujukan " . $dp['rm-bidan']['konj_asal_rujukan'] . " '" . $dp['rm-bidan']['asal_rujukan'] . "' AND
								bayi_lahir_hidup LIKE '" . $dp['rm-bidan']['bayi_lahir_hidup'] . "' AND
								kematian_perinatal LIKE '" . $dp['rm-bidan']['kematian_perinatal'] . "' AND
								sebab_kematian LIKE '" . $dp['rm-bidan']['sebab_kematian'] . "' AND 
								dirujuk LIKE '" . $dp['rm-bidan']['dirujuk'] . "' AND
								YEAR(tanggal) = '" . $tahun . "' AND
								MONTH(tanggal) LIKE '" . $bulan . "'
						");
						if ($rm_bidan_row != null)
							$rm_bidan = $rm_bidan_row->jumlah;
						$rm_puskesmas = 0;
						$rm_puskesmas_row = $db->get_row("
							SELECT 
								COUNT(*) jumlah
							FROM
								smis_mr_perinatologi
							WHERE 
								prop = '' AND 
								cara_datang LIKE '" . $dp['rm-puskesmas']['cara_datang'] . "' AND 
								asal_rujukan " . $dp['rm-puskesmas']['konj_asal_rujukan'] . " '" . $dp['rm-puskesmas']['asal_rujukan'] . "' AND
								bayi_lahir_hidup LIKE '" . $dp['rm-puskesmas']['bayi_lahir_hidup'] . "' AND
								kematian_perinatal LIKE '" . $dp['rm-puskesmas']['kematian_perinatal'] . "' AND
								sebab_kematian LIKE '" . $dp['rm-puskesmas']['sebab_kematian'] . "' AND 
								dirujuk LIKE '" . $dp['rm-puskesmas']['dirujuk'] . "' AND
								YEAR(tanggal) = '" . $tahun . "' AND
								MONTH(tanggal) LIKE '" . $bulan . "'
						");
						if ($rm_puskesmas_row != null)
							$rm_puskesmas = $rm_puskesmas_row->jumlah;
						$rm_faskes = 0;
						$rm_faskes_row = $db->get_row("
							SELECT 
								COUNT(*) jumlah
							FROM
								smis_mr_perinatologi
							WHERE 
								prop = '' AND 
								cara_datang LIKE '" . $dp['rm-faskes']['cara_datang'] . "' AND 
								asal_rujukan " . $dp['rm-faskes']['konj_asal_rujukan'] . " '" . $dp['rm-faskes']['asal_rujukan'] . "' AND
								bayi_lahir_hidup LIKE '" . $dp['rm-faskes']['bayi_lahir_hidup'] . "' AND
								kematian_perinatal LIKE '" . $dp['rm-faskes']['kematian_perinatal'] . "' AND
								sebab_kematian LIKE '" . $dp['rm-faskes']['sebab_kematian'] . "' AND 
								dirujuk LIKE '" . $dp['rm-faskes']['dirujuk'] . "' AND
								YEAR(tanggal) = '" . $tahun . "' AND
								MONTH(tanggal) LIKE '" . $bulan . "'
						");
						if ($rm_faskes_row != null)
							$rm_faskes = $rm_faskes_row->jumlah;
						$rm_mati = 0;
						$rm_mati_row = $db->get_row("
							SELECT 
								COUNT(*) jumlah
							FROM
								smis_mr_perinatologi
							WHERE 
								prop = '' AND 
								cara_datang LIKE '" . $dp['rm-mati']['cara_datang'] . "' AND 
								asal_rujukan " . $dp['rm-mati']['konj_asal_rujukan'] . " '" . $dp['rm-mati']['asal_rujukan'] . "' AND
								bayi_lahir_hidup LIKE '" . $dp['rm-mati']['bayi_lahir_hidup'] . "' AND
								kematian_perinatal LIKE '" . $dp['rm-mati']['kematian_perinatal'] . "' AND
								sebab_kematian LIKE '" . $dp['rm-mati']['sebab_kematian'] . "' AND 
								dirujuk LIKE '" . $dp['rm-mati']['dirujuk'] . "' AND
								YEAR(tanggal) = '" . $tahun . "' AND
								MONTH(tanggal) LIKE '" . $bulan . "'
						");
						if ($rm_mati_row != null)
							$rm_mati = $rm_mati_row->jumlah;
						$rm_total = 0;
						$rm_total_row = $db->get_row("
							SELECT 
								COUNT(*) jumlah
							FROM
								smis_mr_perinatologi
							WHERE 
								prop = '' AND 
								cara_datang LIKE '" . $dp['rm-total']['cara_datang'] . "' AND 
								asal_rujukan " . $dp['rm-total']['konj_asal_rujukan'] . " '" . $dp['rm-total']['asal_rujukan'] . "' AND
								bayi_lahir_hidup LIKE '" . $dp['rm-total']['bayi_lahir_hidup'] . "' AND
								kematian_perinatal LIKE '" . $dp['rm-total']['kematian_perinatal'] . "' AND
								sebab_kematian LIKE '" . $dp['rm-total']['sebab_kematian'] . "' AND 
								dirujuk LIKE '" . $dp['rm-total']['dirujuk'] . "' AND
								YEAR(tanggal) = '" . $tahun . "' AND
								MONTH(tanggal) LIKE '" . $bulan . "'
						");
						if ($rm_total_row != null)
							$rm_total = $rm_total_row->jumlah;
						$rnm_mati = 0;
						$rnm_mati_row = $db->get_row("
							SELECT 
								COUNT(*) jumlah
							FROM
								smis_mr_perinatologi
							WHERE 
								prop = '' AND 
								cara_datang LIKE '" . $dp['rnm-mati']['cara_datang'] . "' AND 
								asal_rujukan " . $dp['rnm-mati']['konj_asal_rujukan'] . " '" . $dp['rnm-mati']['asal_rujukan'] . "' AND
								bayi_lahir_hidup LIKE '" . $dp['rnm-mati']['bayi_lahir_hidup'] . "' AND
								kematian_perinatal LIKE '" . $dp['rnm-mati']['kematian_perinatal'] . "' AND
								sebab_kematian LIKE '" . $dp['rnm-mati']['sebab_kematian'] . "' AND 
								dirujuk LIKE '" . $dp['rnm-mati']['dirujuk'] . "' AND
								YEAR(tanggal) = '" . $tahun . "' AND
								MONTH(tanggal) LIKE '" . $bulan . "'
						");
						if ($rnm_mati_row != null)
							$rnm_mati = $rnm_mati_row->jumlah;
						$rnm_total = 0;
						$rnm_total_row = $db->get_row("
							SELECT 
								COUNT(*) jumlah
							FROM
								smis_mr_perinatologi
							WHERE 
								prop = '' AND 
								cara_datang LIKE '" . $dp['rnm-total']['cara_datang'] . "' AND 
								asal_rujukan " . $dp['rnm-total']['konj_asal_rujukan'] . " '" . $dp['rnm-total']['asal_rujukan'] . "' AND
								bayi_lahir_hidup LIKE '" . $dp['rnm-total']['bayi_lahir_hidup'] . "' AND
								kematian_perinatal LIKE '" . $dp['rnm-total']['kematian_perinatal'] . "' AND
								sebab_kematian LIKE '" . $dp['rnm-total']['sebab_kematian'] . "' AND 
								dirujuk LIKE '" . $dp['rnm-total']['dirujuk'] . "' AND
								YEAR(tanggal) = '" . $tahun . "' AND
								MONTH(tanggal) LIKE '" . $bulan . "'
						");
						if ($rnm_total_row != null)
							$rnm_total = $rnm_total_row->jumlah;
						$nr_mati = 0;
						$nr_mati_row = $db->get_row("
							SELECT 
								COUNT(*) jumlah
							FROM
								smis_mr_perinatologi
							WHERE 
								prop = '' AND 
								cara_datang LIKE '" . $dp['nr-mati']['cara_datang'] . "' AND 
								asal_rujukan " . $dp['nr-mati']['konj_asal_rujukan'] . " '" . $dp['nr-mati']['asal_rujukan'] . "' AND
								bayi_lahir_hidup LIKE '" . $dp['nr-mati']['bayi_lahir_hidup'] . "' AND
								kematian_perinatal LIKE '" . $dp['nr-mati']['kematian_perinatal'] . "' AND
								sebab_kematian LIKE '" . $dp['nr-mati']['sebab_kematian'] . "' AND 
								dirujuk LIKE '" . $dp['nr-mati']['dirujuk'] . "' AND
								YEAR(tanggal) = '" . $tahun . "' AND
								MONTH(tanggal) LIKE '" . $bulan . "'
						");
						if ($nr_mati_row != null)
							$nr_mati = $nr_mati_row->jumlah;
						$nr_total = 0;
						$nr_total_row = $db->get_row("
							SELECT 
								COUNT(*) jumlah
							FROM
								smis_mr_perinatologi
							WHERE 
								prop = '' AND 
								cara_datang LIKE '" . $dp['nr-total']['cara_datang'] . "' AND 
								asal_rujukan " . $dp['nr-total']['konj_asal_rujukan'] . " '" . $dp['nr-total']['asal_rujukan'] . "' AND
								bayi_lahir_hidup LIKE '" . $dp['nr-total']['bayi_lahir_hidup'] . "' AND
								kematian_perinatal LIKE '" . $dp['nr-total']['kematian_perinatal'] . "' AND
								sebab_kematian LIKE '" . $dp['nr-total']['sebab_kematian'] . "' AND 
								dirujuk LIKE '" . $dp['nr-total']['dirujuk'] . "' AND
								YEAR(tanggal) = '" . $tahun . "' AND
								MONTH(tanggal) LIKE '" . $bulan . "'
						");
						if ($nr_total_row != null)
							$nr_total = $nr_total_row->jumlah;
						$dirujuk = 0;
						$dirujuk_row = $db->get_row("
							SELECT 
								COUNT(*) jumlah
							FROM
								smis_mr_perinatologi
							WHERE 
								prop = '' AND 
								cara_datang LIKE '" . $dp['dirujuk']['cara_datang'] . "' AND 
								asal_rujukan " . $dp['dirujuk']['konj_asal_rujukan'] . " '" . $dp['dirujuk']['asal_rujukan'] . "' AND
								bayi_lahir_hidup LIKE '" . $dp['dirujuk']['bayi_lahir_hidup'] . "' AND
								kematian_perinatal LIKE '" . $dp['dirujuk']['kematian_perinatal'] . "' AND
								sebab_kematian LIKE '" . $dp['dirujuk']['sebab_kematian'] . "' AND 
								dirujuk LIKE '" . $dp['dirujuk']['dirujuk'] . "' AND
								YEAR(tanggal) = '" . $tahun . "' AND
								MONTH(tanggal) LIKE '" . $bulan . "'
						");
						if ($dirujuk_row != null)
							$dirujuk = $dirujuk_row->jumlah;

						$html .= 	"<tr class='data_rl35'>" .
										"<td id='rl35_kategori' style='display: none;'>" . $kategori . "</td>" .
										"<td id='rl35_kode_propinsi'>" . $kode_propinsi . "</td>" .
										"<td id='rl35_kabupaten_kota'>" . $kabupaten_kota . "</td>" .
										"<td id='rl35_kode_rs'>" . $kode_rs . "</td>" .
										"<td id='rl35_nama_rs'>" . $nama_rs . "</td>" .
										"<td id='rl35_tahun'>" . $tahun . "</td>" .
										"<td id='rl35_nomor'>" . $nomor . "." . ($subnomor++) . "</td>" .
										"<td id='rl35_jenis_kegiatan'>" . $nama_kegiatan . "</td>" .
										"<td id='rl35_f_rujukan_medis_rs'><div align='right'>" . $rm_rs . "</div></td>" .
										"<td id='rl35_rujukan_medis_rs' style='display:none;'>" . $rm_rs . "</td>" .
										"<td id='rl35_f_rujukan_medis_bidan'><div align='right'>" . $rm_bidan . "</div></td>" .
										"<td id='rl35_rujukan_medis_bidan' style='display:none;'>" . $rm_bidan . "</td>" .	
										"<td id='rl35_f_rujukan_medis_puskesmas'><div align='right'>" . $rm_puskesmas . "</div></td>" .
										"<td id='rl35_rujukan_medis_puskesmas' style='display:none;'>" . $rm_puskesmas . "</td>" .
										"<td id='rl35_f_rujukan_medis_faskes_lainnya'><div align='right'>" . $rm_faskes . "</div></td>" .
										"<td id='rl35_rujukan_medis_faskes_lainnya' style='display:none;'>" . $rm_faskes . "</td>" .
										"<td id='rl35_f_rujukan_medis_mati'><div align='right'>" . $rm_mati . "</div></td>" .
										"<td id='rl35_rujukan_medis_mati' style='display:none;'>" . $rm_mati . "</td>" .
										"<td id='rl35_f_rujukan_medis_jumlah_total'><div align='right'>" . $rm_total . "</div></td>" .
										"<td id='rl35_rujukan_medis_jumlah_total' style='display:none;'>" . $rm_total . "</td>" .
										"<td id='rl35_f_rujukan_non_medis_mati'><div align='right'>" . $rnm_mati . "</div></td>" .
										"<td id='rl35_rujukan_non_medis_mati' style='display:none;'>" . $rnm_mati . "</td>" .
										"<td id='rl35_f_rujukan_non_medis_jumlah_total'><div align='right'>" . $rnm_total . "</div></td>" .
										"<td id='rl35_rujukan_non_medis_jumlah_total' style='display:none;'>" . $rnm_total . "</td>" .
										"<td id='rl35_f_non_rujukan_mati'><div align='right'>" . $nr_mati . "</div></td>" .
										"<td id='rl35_non_rujukan_mati' style='display:none;'>" . $nr_mati . "</td>" .
										"<td id='rl35_f_non_rujukan_jumlah_total'><div align='right'>" . $nr_total . "</div></td>" .
										"<td id='rl35_non_rujukan_jumlah_total' style='display:none;'>" . $nr_total . "</td>" .
										"<td id='rl35_f_dirujuk'><div align='right'>" . $dirujuk . "</div></td>" .
										"<td id='rl35_dirujuk' style='display:none;'>" . $dirujuk . "</td>" .
									"</tr>";
					}
				}
				$nomor++;
			}
			$data['html'] = $html;
			echo json_encode($data);
		} else if ($_POST['command'] == "export_xls") {
			require_once ("smis-libs-out/php-excel/PHPExcel.php");

		    $file = new PHPExcel ();
		    $file->getProperties ()->setCreator ( "PT. Inovasi Ide Utama" );
		    $file->getProperties ()->setTitle ( "RL 3.5 - Perinatologi" );
		    $file->getProperties ()->setSubject ( "RL 3.5 - Perinatologi" );
		    $file->getProperties ()->setDescription ( "RL 3.5 - Perinatologi Generated by System" );
		    $file->getProperties ()->setKeywords ( "RL 3.5 - Perinatologi" );
		    $file->getProperties ()->setCategory ( "RL 3.5 - Perinatologi" );

		    $sheet = $file->getActiveSheet ();
		    $i = 1;

		    $sheet->mergeCells("A".$i.":R".$i)->setCellValue("A".$i,"RL 3.5 - Perinatologi");
		    $sheet->getStyle("A".$i.":R".$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		    $sheet->getStyle("A".$i)->getFont()->setBold(true);
		    $i = $i + 2;

		    $border_start = $i;
		    
		    $sheet->setCellValue("A".$i,"KODE PROPINSI");
		    $sheet->setCellValue("B".$i,"KAB/KOTA");
		    $sheet->setCellValue("C".$i,"KODE RS");
		    $sheet->setCellValue("D".$i,"NAMA RS");
		    $sheet->setCellValue("E".$i,"TAHUN");
		    $sheet->setCellValue("F".$i,"NO. URUT");
		    $sheet->setCellValue("G".$i,"JENIS KEGIATAN");
		    $sheet->setCellValue("H".$i,"RM RUMAH SAKIT");
		    $sheet->setCellValue("I".$i,"RM BIDAN");
		    $sheet->setCellValue("J".$i,"RM PUSKESMAS");
		    $sheet->setCellValue("K".$i,"RM FASKES LAINNYA");
		    $sheet->setCellValue("L".$i,"RM MATI");
		    $sheet->setCellValue("M".$i,"RM TOTAL");
		    $sheet->setCellValue("N".$i,"RNM MATI");
		    $sheet->setCellValue("O".$i,"RNM TOTAL");
		    $sheet->setCellValue("P".$i,"NR MATI");
		    $sheet->setCellValue("Q".$i,"NR TOTAL");
		    $sheet->setCellValue("R".$i,"DIRUJUK");
		    $sheet->getStyle("A".$i.":R".$i)->getFont()->setBold(true);
		    $sheet->getStyle("A".$i.":R".$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		    $sheet->getStyle("A".$i.":R".$i)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

		    $kode_rs = "";
			$nama_rs = "";
			$kode_propinsi = "";
			$kabupaten_kota = "";
			$info_rs_row = $db->get_row("
				SELECT 
					*
				FROM 
					smis_mr_data_dasar_rs
				WHERE 
					prop = ''
				LIMIT 
					0, 1
			");
			if ($info_rs_row != null) {
				$kode_rs = $info_rs_row->koders;
				$nama_rs = $info_rs_row->nama_rs;
				$kode_propinsi = $info_rs_row->kode_provinsi;
				$kabupaten_kota = $info_rs_row->kota_kabupaten;
			}

		    $data = json_decode($_POST['detail']);
		    $nomor = 1;
		    foreach ($data_kategori_perinatologi as $kategori) {
		    	$i = $i + 1;
			    $sheet->setCellValue("A".$i,$kode_propinsi);
			    $sheet->setCellValue("B".$i,$kabupaten_kota);
			    $sheet->setCellValue("C".$i,$kode_rs);
			    $sheet->setCellValue("D".$i,$nama_rs);
			    $sheet->setCellValue("E".$i,$_POST['tahun']);
			    $sheet->setCellValue("F".$i,$nomor++);
			    $sheet->mergeCells("G".$i.":R".$i)->setCellValue("G".$i,$kategori);
			    foreach ($data as $d) {
			    	if ($d->kategori == $kategori) {
					    $i = $i + 1;
					    $sheet->setCellValue("A".$i,$d->kode_propinsi);
					    $sheet->setCellValue("B".$i,$d->kabupaten_kota);
					    $sheet->setCellValue("C".$i,$d->kode_rs);
					    $sheet->setCellValue("D".$i,$d->nama_rs);
					    $sheet->setCellValue("E".$i,$d->tahun);
					    $sheet->setCellValue("F".$i,$d->nomor);
					    $sheet->setCellValue("G".$i,$d->jenis_kegiatan);
					    $sheet->setCellValue("H".$i,$d->rujukan_medis_rs);
					    $sheet->setCellValue("I".$i,$d->rujukan_medis_bidan);
					    $sheet->setCellValue("J".$i,$d->rujukan_medis_puskesmas);
					    $sheet->setCellValue("K".$i,$d->rujukan_medis_faskes_lainnya);
					    $sheet->setCellValue("L".$i,$d->rujukan_medis_mati);
					    $sheet->setCellValue("M".$i,$d->rujukan_medis_jumlah_total);
					    $sheet->setCellValue("N".$i,$d->rujukan_non_medis_mati);
					    $sheet->setCellValue("O".$i,$d->rujukan_non_medis_jumlah_total);
					    $sheet->setCellValue("P".$i,$d->non_rujukan_mati);
					    $sheet->setCellValue("Q".$i,$d->non_rujukan_jumlah_total);
					    $sheet->setCellValue("R".$i,$d->dirujuk);
					}
				}
			}

		    $thin = array ();
		    $thin['borders']=array();
		    $thin['borders']['allborders']=array();
		    $thin['borders']['allborders']['style']=PHPExcel_Style_Border::BORDER_THIN ;
		    $sheet->getStyle ( "A".$border_start.":R".$i )->applyFromArray ($thin);

		    foreach (range('A', 'R') as $columnID) {
		        $sheet->getColumnDimension($columnID)->setAutoSize(true);
		    }

		    header ( 'Content-Type: application/vnd.ms-excel' );
		    header ( 'Content-Disposition: attachment;filename="RL 3.5 - Perinatologi.xls"' );
		    header ( 'Cache-Control: max-age=0' );
		    $writer = PHPExcel_IOFactory::createWriter ( $file, 'Excel5' );
		    $writer->save ( 'php://output' );
		}

		return;
	}
	$modal = $uitable->getModal();
	$form = $modal->getForm();
	$form->setTitle("RL 3.5 - Perinatologi");

	$bulan_option = new OptionBuilder();
	$bulan_option->add("Semua", "%%", "1");
	$bulan_option->add("Januari", "1");
	$bulan_option->add("Februari", "2");
	$bulan_option->add("Maret", "3");
	$bulan_option->add("April", "4");
	$bulan_option->add("Mei", "5");
	$bulan_option->add("Juni", "6");
	$bulan_option->add("Juli", "7");
	$bulan_option->add("Agustus", "8");
	$bulan_option->add("September", "9");
	$bulan_option->add("Oktober", "10");
	$bulan_option->add("Nopember", "11");
	$bulan_option->add("Desember", "12");
	$bulan_select = new Select("rl35_bulan_filter", "rl35_bulan_filter", $bulan_option->getContent());
	$form->addElement("Bulan", $bulan_select);
	$tahun_rows = $db->get_result("
		SELECT DISTINCT YEAR(tanggal) tahun
		FROM smis_mr_perinatologi
		WHERE prop = ''
		ORDER BY YEAR(tanggal) ASC
	");
	$tahun_option = new OptionBuilder();
	if ($tahun_rows != null) {
		foreach ($tahun_rows as $tahun_row) {
			if (date("Y") == $tahun_row->tahun)
				$tahun_option->add($tahun_row->tahun, $tahun_row->tahun, "1");
			else
				$tahun_option->add($tahun_row->tahun, $tahun_row->tahun);
		}
	}
	$tahun_select = new Select("rl35_tahun_filter", "rl35_tahun_filter", $tahun_option->getContent());
	$form->addElement("Tahun", $tahun_select);
	$excel_button = new Button("", "", "");
	$excel_button 	->addClass("btn-primary")
					->setIsButton(Button::$ICONIC)
					->setIcon("fa fa-file-excel-o")
					->setAction("rl35.excel()");
	$process_button = new Button("", "", "");
	$process_button	->addClass("btn-inverse")
					-> setIsButton(Button::$ICONIC)
					->setIcon("fa fa-circle-o")
					->setAction("rl35.view()");
	$button_group = new ButtonGroup("noprint");
	$button_group->addButton($process_button);
	$button_group->addButton($excel_button);
	$form->addElement("", $button_group);

	echo $form->getHtml();
	echo $uitable->getHtml();
	echo addJS("framework/smis/js/table_action.js");
	echo addJS("medical_record/resource/js/rl35.js", false);
?>