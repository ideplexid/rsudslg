<?php
setChangeCookie ( false );
global $db;
require_once ("smis-base/smis-include-service-consumer.php");
$header=array ('No','Diagnosa', '28hr',"1th","4th","14th","24th","44th","65th","l65th","Jumlah","L","P","Total" );
$uitable = new Report ( $header, "", NULL );
$uitable->setSeriesEnabled(false);
$uitable->setRangeEnabled(false);
$uitable->setModelEnabled(false);

$query="SELECT DISTINCT origin FROM smis_mr_stp WHERE prop!='del' ";
$option=new OptionBuilder();
$option->add(" - Semua - ","",1);
$res=$db->get_result($query);
foreach($res as $x){
    if($x->origin==""){
        continue;
    }
    $option->add(ArrayAdapter::slugFormat("unslug",$x->origin),$x->origin);
}
$uitable->addModal( "filter_origin", "select", "Asal", $option->getContent() );

$uitable->addHeader ( "before", "<tr>
		<th rowspan='3'>No.</th>
		<th rowspan='3'>Penyakit</th>
		<th colspan='11'>Rawat Jalan Kasus Baru</th>
		<th rowspan='3'>Total Kunjungan</th>
		</tr>" );
$uitable->addHeader ( "before", "<tr>
		<th colspan='8'>Golongan Umur</th>
		<th rowspan='2'>Jumlah</th>
		<th colspan='2'>Total</th>
		</tr>" );
$uitable->addHeader ( "before", "<tr>
		<th>0-28 HR</th>
		<th>28-1TH</th>
		<th>1-4TH</th>
		<th>5-14TH</th>
		<th>15-24TH</th>
		<th>25-44TH</th>
		<th>45-65TH</th>
		<th>>65TH</th>
		<th>L</th>
		<th>P</th>
		</tr>" );
$uitable->setHeaderVisible ( false );
$uitable->setName ( "rl54" );
$uitable->setDiagram ( true );

/* this is respond when system have to response */
if (isset ( $_POST ['command'] )) {
	$adapter=new SimpleAdapter(true, "No");
	$adapter->add("Diagnosa", "nama_icd");
	$adapter->add("28hr", "u28hr");
	$adapter->add("1th", "u1th");
	$adapter->add("4th", "u4th");
	$adapter->add("14th", "u14th");
	$adapter->add("24th", "u24th");
	$adapter->add("44th", "u44th");
	$adapter->add("65th", "u65th");
	$adapter->add("l65th", "ul65th");
	$adapter->add("Jumlah", "jumlah");
	$adapter->add("L", "L");
	$adapter->add("P", "P");
	$adapter->add("Total", "total");
	$adapter->setRemoveZeroEnable(true);
	
	$qv="SELECT 
			nama_icd,
			SUM(u28hr) as u28hr,
			SUM(u1th) as u1th,
			SUM(u4th) as u4th,
			SUM(u14th) as u14th,
			SUM(u24th) as u24th,
			SUM(u44th) as u44th,
			SUM(u65th) as u65th,
			SUM(ul65th) as ul65th,
			SUM(jumlah) as jumlah,
			SUM(L) as L,
			SUM(P) as P,
			SUM(total) as total
			FROM smis_mr_stp
			";
	$qc="SELECT count(*) FROM smis_mr_stp";
	
	$dbtable = new DBTable ( $db, "smis_mr_stp");
    $dbtable->addCustomKriteria(" urji "," =0 ");
    if(isset($_POST['filter_origin']) && $_POST['filter_origin'] != "") {
        $dbtable->addCustomKriteria ( " origin ", "LIKE '".$_POST['filter_origin']."'" );
    }
	$dbtable->setPreferredQuery(true, $qv, $qc);
	//$dbtable->setUseWhereforView(true);
	$dbtable->setGroupByForCount(true);
	$dbtable->setOrder ( "jumlah DESC, nama_icd ASC" );
	$dbtable->setGroupBy(true, " nama_icd ");
    
	$dbres = new DBReport ( $dbtable, $uitable, $adapter, 'tanggal', DBReport::$DATE );
	$dbres->setDiagram ( false, null, null, null );
    
	$data = $dbres->command ( $_POST ['command'] );
	echo json_encode ( $data, JSON_NUMERIC_CHECK );
	return;
}

/* This is Modal Form and used for add and edit the table */
$modal = $uitable->getAdvanceModal ();
$modal->setTitle ( "Surveilance Terpadu" );
$btn=new Button("", "", "");
$btn->setIsButton(Button::$ICONIC);
$btn->setClass("btn btn-info");
$btn->setIcon("icon-white icon-print");
$btn->setAction("rl54.print()");

//$form=$modal->joinFooterAndForm();
$modal->addElement("",$btn);


echo addJS ( "framework/smis/js/table_action.js" );
echo addJS ( "framework/smis/js/report_action.js" );
echo addJS ( "framework/bootstrap/js/bootstrap-datepicker.js" );
echo addCSS ( "framework/bootstrap/css/datepicker.css" );
echo $modal->getModalSkeleton();
echo "<div class='line clear'></div>";
echo $uitable->getHtml();
?>
<script type="text/javascript">

var rl54;
$(document).ready(function(){
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
	$('.mydate').datepicker();
	var column=new Array('ruangan');
	rl54=new ReportAction("rl54","medical_record","rl54",column);
    
    rl54.addRegulerData = function(data) {
        data['filter_origin'] = $("#rl54_filter_origin").val();
        return data;
    };
});
</script>