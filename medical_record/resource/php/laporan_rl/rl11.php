<?php
    global $db;

    $kode_rs = "";
    $tanggal_registrasi = "";
    $nama_rs = "";
    $kabupaten_kota = "";
    $jenis_rs = "";
    $kelas_rs = "";
    $kepemilikan_rs = "";
    $nama_direktur = "";
    $nama_penyelenggara = "";
    $status_penyelenggara = "";
    $jumlah_layanan = "";
    $jumlah_sdm = "";
    $jumlah_alat_kesehatan = "";
    $alamat_rs = "";
    $email = "";
    $nomor_surat_penetapan_kelas = "";
    $masa_berlaku_surat_penetapan_kelas = "";
    $versi_akreditasi = "";
    $masa_berlaku_akreditasi = "";
    $jumlah_ambulan_transportasi = "";
    $jumlah_ambulan_jenasah = "";
    $nomor_surat_izin_operasional_rs = "";
    $masa_berlaku_surat_izin_operasional_rs = "";
    $nomor_surat_izin_hemodialisa = "";
    $masa_berlaku_surat_izin_hemodialisa = "";
    $jumlah_tempat_tidur_perinatologi = "";
    $jumlah_tempat_tidur_kelas_vvip = "";
    $jumlah_tempat_tidur_kelas_vip = "";
    $jumlah_tempat_tidur_kelas_i = "";
    $jumlah_tempat_tidur_kelas_ii = "";
    $jumlah_tempat_tidur_kelas_iii = "";
    $jumlah_tempat_tidur_icu = "";
    $jumlah_tempat_tidur_picu = "";
    $jumlah_tempat_tidur_nicu = "";
    $jumlah_tempat_tidur_hcu = "";
    $jumlah_tempat_tidur_ruang_isolasi = "";
    $kelengkapan_simrs = "";
    $kelengkapan_bank_darah = "";
    $kelengkapan_layanan_unggulan = "";
    $kelengkapan_peralatan_canggih = "";
    $kelengkapan_kerjasama_bpjs = "";
    $kelengkapan_blud = "";
    $kelengkapan_kemampuan_pelayanan_gawat_darurat = "";
    $row = $db->get_row("
        SELECT *
        FROM smis_mr_data_dasar_rs
        WHERE prop = ''
        ORDER BY id DESC
        LIMIT 0, 1
    ");
    if ($row != null) {
        $kode_rs = $row->koders;
        $tanggal_registrasi = ArrayAdapter::format("date d-m-Y", $row->tanggal_registrasi);
        $nama_rs = $row->nama_rs;
        $kabupaten_kota = $row->kota_kabupaten;
        $jenis_rs = $row->jenis_rs;
        $kelas_rs = $row->kelas_rs;
        $kepemilikan_rs = $row->kepemilikan_rs;
        $nama_direktur = $row->nama_direktur;
        $nama_penyelenggara = $row->nama_penyelenggara;
        $status_penyelenggara = $row->status_penyelenggara;
        $jumlah_layanan = $row->jumlah_layanan;
        $jumlah_sdm = $row->jumlah_sdm;
        $jumlah_alat_kesehatan = $row->jumlah_alat_kesehatan;
        $alamat_rs = $row->alamat_rs;
        $email = $row->email_rs;
        $nomor_surat_penetapan_kelas = $row->nomor_surat_penetapan_kelas;
        $masa_berlaku_surat_penetapan_kelas = ArrayAdapter::format("date d-m-Y", $row->masa_berlaku_surat_penetapan_kelas);
        $versi_akreditasi = $row->versi_akreditasi;
        $masa_berlaku_akreditasi = ArrayAdapter::format("date d-m-Y", $row->masa_berlaku_akreditasi);
        $jumlah_ambulan_transportasi = $row->ambulan_transportasi_baik;
        $jumlah_ambulan_jenasah = $row->ambulan_jenasah_baik;
        $nomor_surat_izin_operasional_rs = $row->nomor_surat_izin_operasional_rs;
        $masa_berlaku_surat_izin_operasional_rs = ArrayAdapter::format("date d-m-Y", $row->masa_berlaku_surat_izin_operasional_rs);
        $nomor_surat_izin_hemodialisa = $row->nomor_surat_izin_hemodialisa;
        $masa_berlaku_surat_izin_hemodialisa = ArrayAdapter::format("date d-m-Y", $row->masa_berlaku_surat_izin_hemodialisa);
        $jumlah_tempat_tidur_perinatologi = $row->jml_tempat_tidur_perinatologi;
        $jumlah_tempat_tidur_kelas_vvip = $row->jml_tempat_tidur_kelas_vvip;
        $jumlah_tempat_tidur_kelas_vip = $row->jml_tempat_tidur_kelas_vip;
        $jumlah_tempat_tidur_kelas_i = $row->jml_tempat_tidur_kelas_i;
        $jumlah_tempat_tidur_kelas_ii = $row->jml_tempat_tidur_kelas_ii;
        $jumlah_tempat_tidur_kelas_iii = $row->jml_tempat_tidur_kelas_iii;
        $jumlah_tempat_tidur_icu = $row->jml_tempat_tidur_icu;
        $jumlah_tempat_tidur_picu = $row->jml_tempat_tidur_picu;
        $jumlah_tempat_tidur_nicu = $row->jml_tempat_tidur_nicu;
        $jumlah_tempat_tidur_hcu = $row->jml_tempat_tidur_hcu;
        $jumlah_tempat_tidur_ruang_isolasi = $row->jml_tempat_tidur_ruang_isolasi;
        $kelengkapan_simrs = $row->kelengkapan_simrs;
        $kelengkapan_bank_darah = $row->kelengkapan_bank_darah;
        $kelengkapan_layanan_unggulan = $row->kelengkapan_layanan_unggulan;
        $kelengkapan_peralatan_canggih = $row->kelengkapan_peralatan_canggih;
        $kelengkapan_kerjasama_bpjs = $row->kelengkapan_kerjasama_bpjs;
        $kelengkapan_blud = $row->kelengkapan_blud;
        $kelengkapan_kemampuan_pelayanan_gawat_darurat = $row->kelengkapan_kemampuan_pelayanan_gawat_darurat;
    }

    echo "<div class='alert alert-block alert-inverse noprint'>" .
            "<h4>Informasi</h4>" .
            "RL 1.1 ditampilkan sesuai dengan identitas rumah sakit yang diinput di Menu Rekam Medis - Submenu Input Data - Tab Data Dasar RS" .
          "</div>";

    echo "<div>";
        echo "<button type='button' class='btn btn-primary' onclick='rl11.excel()'><i class='fa fa-file-excel-o'></i> Excel</button>";
        echo "&nbsp";
        echo "<button type='button' class='btn btn-primary' onclick='rl11.cetak()'><i class='fa fa-print'></i> Print</button>";
    echo "</div>";

    echo "<div id='table_content'>";
        echo "<div class='smis-table-container'>";
            echo "<div class='' id='print_table_preview_neraca_lajur'>";
                echo "<h3 align='center'><strong>RL 1.1 - Identitas/Data Rumah Sakit</strong></h3>";
                echo "<table data-fix-header='n' class='table table-bordered table-hover table-striped table-condensed'>";
                    echo "<thead class='thead-default'>";
                        echo "<tr class='inverse'>";
                            echo "<th class='center' rowspan='2'>Kode RS</th>";
                            echo "<th class='center' rowspan='2'>Tgl. Registrasi</th>";
                            echo "<th class='center' rowspan='2'>Nama RS</th>";
                            echo "<th class='center' rowspan='2'>Nama Kab/Kota</th>";
                            echo "<th class='center' rowspan='2'>Jenis RS</th>";
                            echo "<th class='center' rowspan='2'>Kelas RS</th>";
                            echo "<th class='center' rowspan='2'>Kepemilikan</th>";
                            echo "<th class='center' rowspan='2'>Nama Direktur</th>";
                            echo "<th class='center' rowspan='2'>Nama Penyelenggara</th>";
                            echo "<th class='center' rowspan='2'>Status Penyelenggara</th>";
                            echo "<th class='center' rowspan='2'>Jumlah Layanan</th>";
                            echo "<th class='center' rowspan='2'>Jumlah SDM</th>";
                            echo "<th class='center' rowspan='2'>Jumlah Alat Kesehatan</th>";
                            echo "<th class='center' rowspan='2'>Alamat RS</th>";
                            echo "<th class='center' rowspan='2'>Email</th>";
                            echo "<th class='center' colspan='2'>Surat Penetapan Kelas</th>";
                            echo "<th class='center' colspan='2'>Akreditasi</th>";
                            echo "<th class='center' colspan='2'>Ambulan</th>";
                            echo "<th class='center' colspan='2'>Surat Ijin Operasional RS</th>";
                            echo "<th class='center' colspan='2'>Surat Ijin Hemodialisa</th>";
                            echo "<th class='center' colspan='11'>Tempat Tidur</th>";
                            echo "<th class='center' colspan='7'>Kelengkapan</th>";
                        echo "</tr>";
                        echo "<tr class='inverse'>";
                            echo "<th class='center'>Nomor</th>";
                            echo "<th class='center'>Masa Berlaku</th>";
                            echo "<th class='center'>Versi</th>";
                            echo "<th class='center'>Masa Berlaku</th>";
                            echo "<th class='center'>Ambulan Transportasi</th>";
                            echo "<th class='center'>Ambulan Jenasah</th>";
                            echo "<th class='center'>Nomor</th>";
                            echo "<th class='center'>Masa Berlaku</th>";
                            echo "<th class='center'>Nomor</th>";
                            echo "<th class='center'>Masa Berlaku</th>";
                            echo "<th class='center'>Perinatologi</th>";
                            echo "<th class='center'>Kelas VVIP</th>";
                            echo "<th class='center'>Kelas VIP</th>";
                            echo "<th class='center'>Kelas I</th>";
                            echo "<th class='center'>Kelas II</th>";
                            echo "<th class='center'>Kelas III</th>";
                            echo "<th class='center'>ICU</th>";
                            echo "<th class='center'>PICU</th>";
                            echo "<th class='center'>NICU</th>";
                            echo "<th class='center'>HCU</th>";
                            echo "<th class='center'>Ruang Isolasi</th>";
                            echo "<th class='center'>SIMRS</th>";
                            echo "<th class='center'>Bank Darah</th>";
                            echo "<th class='center'>Layanan Unggulan</th>";
                            echo "<th class='center'>Peralatan Canggih</th>";
                            echo "<th class='center'>Kerjasama BPJS</th>";
                            echo "<th class='center'>BLUD</th>";
                            echo "<th class='center'>Kemampuan Pelayanan Gawat Darurat</th>";
                        echo "</tr>";
                    echo "</thead>";
                    echo "<tbody>";
                        echo "<tr>";
                            echo "<td>" . $kode_rs . "</td>";
                            echo "<td>" . $tanggal_registrasi . "</td>";
                            echo "<td>" . $nama_rs . "</td>";
                            echo "<td>" . $kabupaten_kota . "</td>";
                            echo "<td>" . $jenis_rs . "</td>";
                            echo "<td>" . $kelas_rs . "</td>";
                            echo "<td>" . $kepemilikan_rs . "</td>";
                            echo "<td>" . $nama_direktur . "</td>";
                            echo "<td>" . $nama_penyelenggara . "</td>";
                            echo "<td>" . $status_penyelenggara . "</td>";
                            echo "<td>" . $jumlah_layanan . "</td>";
                            echo "<td>" . $jumlah_sdm . "</td>";
                            echo "<td>" . $jumlah_alat_kesehatan . "</td>";
                            echo "<td>" . $alamat_rs . "</td>";
                            echo "<td>" . $email . "</td>";
                            echo "<td>" . $nomor_surat_penetapan_kelas . "</td>";
                            echo "<td>" . $masa_berlaku_surat_penetapan_kelas . "</td>";
                            echo "<td>" . $versi_akreditasi . "</td>";
                            echo "<td>" . $masa_berlaku_akreditasi . "</td>";
                            echo "<td>" . $jumlah_ambulan_transportasi . "</td>";
                            echo "<td>" . $jumlah_ambulan_jenasah . "</td>";
                            echo "<td>" . $nomor_surat_izin_operasional_rs . "</td>";
                            echo "<td>" . $masa_berlaku_surat_izin_operasional_rs . "</td>";
                            echo "<td>" . $nomor_surat_izin_hemodialisa . "</td>";
                            echo "<td>" . $masa_berlaku_surat_izin_hemodialisa . "</td>";
                            echo "<td>" . $jumlah_tempat_tidur_perinatologi . "</td>";
                            echo "<td>" . $jumlah_tempat_tidur_kelas_vvip . "</td>";
                            echo "<td>" . $jumlah_tempat_tidur_kelas_vip . "</td>";
                            echo "<td>" . $jumlah_tempat_tidur_kelas_i . "</td>";
                            echo "<td>" . $jumlah_tempat_tidur_kelas_ii . "</td>";
                            echo "<td>" . $jumlah_tempat_tidur_kelas_iii . "</td>";
                            echo "<td>" . $jumlah_tempat_tidur_icu . "</td>";
                            echo "<td>" . $jumlah_tempat_tidur_picu . "</td>";
                            echo "<td>" . $jumlah_tempat_tidur_nicu . "</td>";
                            echo "<td>" . $jumlah_tempat_tidur_hcu . "</td>";
                            echo "<td>" . $jumlah_tempat_tidur_ruang_isolasi . "</td>";
                            echo "<td>" . $kelengkapan_simrs . "</td>";
                            echo "<td>" . $kelengkapan_bank_darah . "</td>";
                            echo "<td>" . $kelengkapan_layanan_unggulan . "</td>";
                            echo "<td>" . $kelengkapan_peralatan_canggih . "</td>";
                            echo "<td>" . $kelengkapan_kerjasama_bpjs . "</td>";
                            echo "<td>" . $kelengkapan_blud . "</td>";
                            echo "<td>" . $kelengkapan_kemampuan_pelayanan_gawat_darurat . "</td>";
                        echo "</tr>";
                    echo "</tbody>";
                echo "</table>";
            echo "</div>";
        echo "</div>";
    echo "</div>";

    echo addJS ( "framework/smis/js/table_action.js" );
    echo addJS ( "framework/smis/js/report_action.js" );
    echo addJS ( "medical_record/resource/js/rl11.js", false);
?>
<style type="text/css">
    .center {
        text-align: center;
        vertical-align: middle;
    }
</style>