<?php
	require_once("smis-base/smis-include-service-consumer.php");
	global $db;

	$head = array(
		'Kode Propinsi', 'Kab/Kota', 'Kode RS', 'Nama RS', 'Tahun', 'No.', 'Jenis Spesialisasi', 
		'Rujukan Diterima dari Puskesmas', 'Rujukan Diterima dari Faskes Lain', 'Rujukan Diterima dari RS Lain',
		'Dikembalikan ke Puskesmas', 'Dikembalikan ke Faskes Lain', 'Dikembalikan ke RS Asal',
		'Dirujuk - Pasien Rujukan', 'Dirujuk - Pasien Datang Sendiri, Dirujuk - Diterima Kembali'
	);
	$uitable = new Table($head, "", null, true);
	$uitable->setName("rl314");
	$uitable->setAction(false);
	$uitable->setFooterVisible(false);
	$uitable->setHeaderVisible(false);
	$uitable->addHeader("before", "
		<tr>
			<th rowspan='3'>Kode Propinsi</th>
			<th rowspan='3'>Kab/Kota</th>
			<th rowspan='3'>Kode RS</th>
			<th rowspan='3'>Nama RS</th>
			<th rowspan='3'>Tahun</th>
			<th rowspan='2'>No.</th>
			<th rowspan='2'>Jenis Spesialisasi</th>
			<th colspan='6'>Rujukan</th>
			<th colspan='3'>Dirujuk</th>
		</tr>
		<tr>
			<th>Diterima dari Puskesmas</th>
			<th>Diterima dari Fasilitas Kes. Lain</th>
			<th>Diterima dari RS Lain</th>
			<th>Dikembalikan ke Puskesmas</th>
			<th>Dikembalikan ke Fasilitas Kes. Lain</th>
			<th>Dikembalikan ke RS Lain</th>
			<th>Pasien Rujukan</th>
			<th>Pasien Datang Sendiri</th>
			<th>Diterima Kembali</th>
		</tr>
		<tr>
			<th>1</th>
			<th>2</th>
			<th>3</th>
			<th>4</th>
			<th>5</th>
			<th>6</th>
			<th>7</th>
			<th>8</th>
			<th>9</th>
			<th>10</th>
			<th>11</th>
		</tr>
	");

	if (isset($_POST ['command'])) {
		if ($_POST['command'] == "view") {
			$tahun = $_POST['tahun'];
			$bulan = $_POST['bulan'];
			$html = "";

			$kode_rs = "";
			$nama_rs = "";
			$kode_propinsi = "";
			$kabupaten_kota = "";
			$info_rs_row = $db->get_row("
				SELECT 
					*
				FROM 
					smis_mr_data_dasar_rs
				WHERE 
					prop = ''
				LIMIT 
					0, 1
			");
			if ($info_rs_row != null) {
				$kode_rs = $info_rs_row->koders;
				$nama_rs = $info_rs_row->nama_rs;
				$kode_propinsi = $info_rs_row->kode_provinsi;
				$kabupaten_kota = $info_rs_row->kota_kabupaten;
			}

			$spesialisasi_arr = array(
				"Penyakit Dalam" 			=> array("Penyakit Dalam"),
				"Bedah" 					=> array("Bedah"),
				"Kesehatan Anak"			=> array("Kesehatan Anak (Neonatal)", "Kesehatan Anak Lainnya)"),
				"Obstetri dan Ginekologi"	=> array("Obstetri & Ginekologi (Ibu Hamil)", "Obstetri & Ginekologi Lainnya)"),
				"Keluarga Berencana"		=> array("Keluarga Berencana"),
				"Saraf"						=> array("Saraf", "Bedah Saraf"),
				"Jiwa"						=> array("Jiwa"),
				"THT"						=> array("THT"),
				"Mata"						=> array("Mata"),
				"Kulit dan Kelamin"			=> array("Kulit dan Kelamin"),
				"Gigi dan Mulut"			=> array("Gigi & Mulut"),
				"Radiologi"					=> array("Radiologi"),
				"Paru-Paru"					=> array("Paru - Paru"),
				"Spesialisasi Lain"			=> array(
					"Napza", 
					"Psikologi", 
					"Geriatri", 
					"Kardiologi", 
					"Bedah Orthopedi", 
					"Kusta", 
					"Umum", 
					"Rawat Darurat", 
					"Rehabilitasi Medik", 
					"Akupungtur Medik", 
					"Konsultasi Gizi", 
					"Day Care", 
					"Lain - Lain"
				)
			);

			$nomor = 1;
			foreach ($spesialisasi_arr as $spesialisasi => $jenis_kegiatan_arr) {
				$jumlah_diterima_dari_puskesmas = 0;
				$jumlah_diterima_dari_rs_lain = 0;
				$jumlah_diterima_dari_faskes_lain = 0;
				$jumlah_dikembalikan_ke_puskesmas = 0;
				$jumlah_dikembalikan_ke_rs_lain = 0;
				$jumlah_dikembalikan_ke_faskes_lain = 0;
				$jumlah_dirujuk_pasien_rujukan = 0;
				$jumlah_dirujuk_pasien_datang_sendiri = 0;
				$jumlah_dirujuk_pasien_diterima_kembali = 0;
				foreach ($jenis_kegiatan_arr as $jenis_kegiatan) {
					$jumlah_diterima_dari_puskesmas_row = $db->get_row("
						SELECT 
							COUNT(*) jumlah
						FROM 
							smis_rg_layananpasien 
						WHERE
							prop = '' AND 
							caradatang LIKE 'Rujukan' AND 
							rujukan LIKE 'Puskesmas' AND 
							jenis_kegiatan LIKE '" . $jenis_kegiatan . "' AND 
							YEAR(tanggal) = '" . $tahun . "' AND 
							MONTH(tanggal) LIKE '" . $bulan . "'
					");
					if ($jumlah_diterima_dari_puskesmas_row != null)
						$jumlah_diterima_dari_puskesmas += $jumlah_diterima_dari_puskesmas_row->jumlah;

					$jumlah_diterima_dari_faskes_lain_row = $db->get_row("
						SELECT 
							COUNT(*) jumlah
						FROM 
							smis_rg_layananpasien 
						WHERE
							prop = '' AND 
							caradatang LIKE 'Rujukan' AND 
							rujukan NOT LIKE 'Puskesmas' AND 
							rujukan NOT LIKE 'RS Lain' AND 
							jenis_kegiatan LIKE '" . $jenis_kegiatan . "' AND 
							YEAR(tanggal) = '" . $tahun . "' AND 
							MONTH(tanggal) LIKE '" . $bulan . "'
					");
					if ($jumlah_diterima_dari_faskes_lain_row != null)
						$jumlah_diterima_dari_faskes_lain += $jumlah_diterima_dari_faskes_lain_row->jumlah;

					$jumlah_diterima_dari_rs_lain_row = $db->get_row("
						SELECT 
							COUNT(*) jumlah
						FROM 
							smis_rg_layananpasien 
						WHERE
							prop = '' AND 
							caradatang LIKE 'Rujukan' AND 
							rujukan LIKE 'RS Lain' AND 
							jenis_kegiatan LIKE '" . $jenis_kegiatan . "' AND 
							YEAR(tanggal) = '" . $tahun . "' AND 
							MONTH(tanggal) LIKE '" . $bulan . "'
					");
					if ($jumlah_diterima_dari_rs_lain_row != null)
						$jumlah_diterima_dari_rs_lain += $jumlah_diterima_dari_rs_lain_row->jumlah;

					$jumlah_dikembalikan_ke_puskesmas_row = $db->get_row("
						SELECT 
							COUNT(*) jumlah
						FROM 
							smis_rg_layananpasien 
						WHERE
							prop = '' AND 
							caradatang LIKE 'Rujukan' AND 
							rujukan NOT LIKE 'Puskesmas' AND 
							rujukan NOT LIKE 'RS Lain' AND 
							jenis_kegiatan LIKE '" . $jenis_kegiatan . "' AND 
							carapulang LIKE 'Dikembalikan ke Perujuk' AND
							YEAR(tanggal_pulang) = '" . $tahun . "' AND 
							MONTH(tanggal_pulang) LIKE '" . $bulan . "'
					");
					if ($jumlah_dikembalikan_ke_puskesmas_row != null)
						$jumlah_dikembalikan_ke_puskesmas += $jumlah_dikembalikan_ke_puskesmas_row->jumlah;

					$jumlah_dikembalikan_ke_faskes_lain_row = $db->get_row("
						SELECT 
							COUNT(*) jumlah
						FROM 
							smis_rg_layananpasien 
						WHERE
							prop = '' AND 
							caradatang LIKE 'Rujukan' AND 
							rujukan LIKE 'RS Lain' AND 
							jenis_kegiatan LIKE '" . $jenis_kegiatan . "' AND 
							carapulang LIKE 'Dikembalikan ke Perujuk' AND
							YEAR(tanggal_pulang) = '" . $tahun . "' AND 
							MONTH(tanggal_pulang) LIKE '" . $bulan . "'
					");
					if ($jumlah_dikembalikan_ke_faskes_lain_row != null)
						$jumlah_dikembalikan_ke_faskes_lain += $jumlah_dikembalikan_ke_faskes_lain_row->jumlah;

					$jumlah_dikembalikan_ke_rs_lain_row = $db->get_row("
						SELECT 
							COUNT(*) jumlah
						FROM 
							smis_rg_layananpasien 
						WHERE
							prop = '' AND 
							caradatang LIKE 'Rujukan' AND 
							rujukan LIKE 'RS Lain' AND 
							jenis_kegiatan LIKE '" . $jenis_kegiatan . "' AND 
							carapulang LIKE 'Dikembalikan ke Perujuk' AND
							YEAR(tanggal_pulang) = '" . $tahun . "' AND 
							MONTH(tanggal_pulang) LIKE '" . $bulan . "'
					");
					if ($jumlah_dikembalikan_ke_rs_lain_row != null)
						$jumlah_dikembalikan_ke_rs_lain += $jumlah_dikembalikan_ke_rs_lain_row->jumlah;

					$jumlah_dirujuk_pasien_rujukan_row = $db->get_row("
						SELECT 
							COUNT(*) jumlah
						FROM 
							smis_rg_layananpasien 
						WHERE
							prop = '' AND 
							caradatang LIKE 'Rujukan' AND 
							jenis_kegiatan LIKE '" . $jenis_kegiatan . "' AND 
							carapulang LIKE 'Dirujuk%' AND
							YEAR(tanggal_pulang) = '" . $tahun . "' AND 
							MONTH(tanggal_pulang) LIKE '" . $bulan . "'
					");
					if ($jumlah_dirujuk_pasien_rujukan_row != null)
						$jumlah_dirujuk_pasien_rujukan += $jumlah_dirujuk_pasien_rujukan_row->jumlah;

					$jumlah_dirujuk_pasien_datang_sendiri_row = $db->get_row("
						SELECT 
							COUNT(*) jumlah
						FROM 
							smis_rg_layananpasien 
						WHERE
							prop = '' AND 
							caradatang LIKE 'Datang Sendiri' AND 
							jenis_kegiatan LIKE '" . $jenis_kegiatan . "' AND 
							carapulang LIKE 'Dirujuk%' AND
							YEAR(tanggal_pulang) = '" . $tahun . "' AND 
							MONTH(tanggal_pulang) LIKE '" . $bulan . "'
					");
					if ($jumlah_dirujuk_pasien_datang_sendiri_row != null)
						$jumlah_dirujuk_pasien_datang_sendiri += $jumlah_dirujuk_pasien_datang_sendiri_row->jumlah;

					$jumlah_dirujuk_pasien_diterima_kembali_row = $db->get_row("
						SELECT 
							COUNT(*) jumlah
						FROM 
							smis_rg_layananpasien 
						WHERE
							prop = '' AND 
							caradatang LIKE 'Diterima Kembali' AND 
							jenis_kegiatan LIKE '" . $jenis_kegiatan . "' AND 
							carapulang LIKE 'Dirujuk%' AND
							YEAR(tanggal_pulang) = '" . $tahun . "' AND 
							MONTH(tanggal_pulang) LIKE '" . $bulan . "'
					");
					if ($jumlah_dirujuk_pasien_diterima_kembali_row != null)
						$jumlah_dirujuk_pasien_diterima_kembali += $jumlah_dirujuk_pasien_diterima_kembali_row->jumlah;
				}
				$html .= 	"<tr class='data_rl314'>" .
								"<td id='rl314_kode_propinsi'>" . $kode_propinsi . "</td>" .
								"<td id='rl314_kabupaten_kota'>" . $kabupaten_kota . "</td>" .
								"<td id='rl314_kode_rs'>" . $kode_rs . "</td>" .
								"<td id='rl314_nama_rs'>" . $nama_rs . "</td>" .
								"<td id='rl314_tahun'>" . $tahun . "</td>" .
								"<td id='rl314_nomor'>" . $nomor++ . "</td>" .
								"<td id='rl314_spesialisasi'>" . $spesialisasi . "</td>" .
								"<td id='rl314_diterima_dari_puskesmas' style='display: none;'>" . $jumlah_diterima_dari_puskesmas . "</td>" .
								"<td id='rl314_f_diterima_dari_puskesmas'><div align='right'>" . $jumlah_diterima_dari_puskesmas . "</div></td>" .
								"<td id='rl314_diterima_dari_faskes_lain' style='display: none;'>" . $jumlah_diterima_dari_faskes_lain . "</td>" .
								"<td id='rl314_f_diterima_dari_faskes_lain'><div align='right'>" . $jumlah_diterima_dari_faskes_lain . "</div></td>" .
								"<td id='rl314_diterima_dari_rs_lain' style='display: none;'>" . $jumlah_diterima_dari_rs_lain . "</td>" .
								"<td id='rl314_f_diterima_dari_rs_lain'><div align='right'>" . $jumlah_diterima_dari_rs_lain . "</div></td>" .
								"<td id='rl314_dikembalikan_ke_puskesmas' style='display: none;'>" . $jumlah_dikembalikan_ke_puskesmas . "</td>" .
								"<td id='rl314_f_dikembalikan_ke_puskesmas'><div align='right'>" . $jumlah_dikembalikan_ke_puskesmas . "</div></td>" .
								"<td id='rl314_dikembalikan_ke_faskes_lain' style='display: none;'>" . $jumlah_dikembalikan_ke_faskes_lain . "</td>" .
								"<td id='rl314_f_dikembalikan_ke_faskes_lain'><div align='right'>" . $jumlah_dikembalikan_ke_faskes_lain . "</div></td>" .
								"<td id='rl314_dikembalikan_ke_rs_lain' style='display: none;'>" . $jumlah_dikembalikan_ke_rs_lain . "</td>" .
								"<td id='rl314_f_dikembalikan_ke_rs_lain'><div align='right'>" . $jumlah_dikembalikan_ke_rs_lain . "</div></td>" .
								"<td id='rl314_dirujuk_pasien_rujukan' style='display: none;'>" . $jumlah_dirujuk_pasien_rujukan . "</td>" .
								"<td id='rl314_f_dirujuk_pasien_rujukan'><div align='right'>" . $jumlah_dirujuk_pasien_rujukan . "</div></td>" .
								"<td id='rl314_dirujuk_pasien_datang_sendiri' style='display: none;'>" . $jumlah_dirujuk_pasien_datang_sendiri . "</td>" .
								"<td id='rl314_f_dirujuk_pasien_datang_sendiri'><div align='right'>" . $jumlah_dirujuk_pasien_datang_sendiri . "</div></td>" .
								"<td id='rl314_dirujuk_pasien_diterima_kembali' style='display: none;'>" . $jumlah_dirujuk_pasien_diterima_kembali . "</td>" .
								"<td id='rl314_f_dirujuk_pasien_diterima_kembali'><div align='right'>" . $jumlah_dirujuk_pasien_diterima_kembali . "</div></td>" .
							"</tr>";
			}

			$data['html'] = $html;
			echo json_encode($data);
		} else if ($_POST['command'] == "export_xls") {
			require_once ("smis-libs-out/php-excel/PHPExcel.php");

		    $file = new PHPExcel ();
		    $file->getProperties ()->setCreator ( "PT. Inovasi Ide Utama" );
		    $file->getProperties ()->setTitle ( "RL 3.14 - Rujukan" );
		    $file->getProperties ()->setSubject ( "RL 3.14 - Rujukan" );
		    $file->getProperties ()->setDescription ( "RL 3.14 - Rujukan Generated by System" );
		    $file->getProperties ()->setKeywords ( "RL 3.14 - Rujukan" );
		    $file->getProperties ()->setCategory ( "RL 3.14 - Rujukan" );

		    $sheet = $file->getActiveSheet ();
		    $i = 1;

		    $sheet->mergeCells("A".$i.":P".$i)->setCellValue("A".$i,"RL 3.14 - Rujukan");
		    $sheet->getStyle("A".$i.":P".$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		    $sheet->getStyle("A".$i)->getFont()->setBold(true);
		    $i = $i + 2;

		    $border_start = $i;
		    
		    $sheet->mergeCells("A".$i.":A".($i + 2))->setCellValue("A".$i,"Kode Propinsi");
		    $sheet->mergeCells("B".$i.":B".($i + 2))->setCellValue("B".$i,"Kab/Kota");
		    $sheet->mergeCells("C".$i.":C".($i + 2))->setCellValue("C".$i,"Kode RS");
		    $sheet->mergeCells("D".$i.":D".($i + 2))->setCellValue("D".$i,"Nama RS");
		    $sheet->mergeCells("E".$i.":E".($i + 2))->setCellValue("E".$i,"Tahun");
		    $sheet->mergeCells("F".$i.":F".($i + 1))->setCellValue("F".$i,"No.");
		    $sheet->setCellValue("F".($i + 2),"1");
		    $sheet->mergeCells("G".$i.":G".($i + 1))->setCellValue("G".$i,"Jenis Spesialisasi");
		    $sheet->setCellValue("G".($i + 2),"2");
		    $sheet->mergeCells("H".$i.":M".$i)->setCellValue("H".$i,"Rujukan");
		    $sheet->setCellValue("H".($i + 1),"Diterima dari Puskesmas");
		    $sheet->setCellValue("H".($i + 2),"3");
		    $sheet->setCellValue("I".($i + 1),"Diterima dari Fasilitas Kes. Lain");
		    $sheet->setCellValue("I".($i + 2),"4");
		    $sheet->setCellValue("J".($i + 1),"Diterima dari RS Lain");
		    $sheet->setCellValue("J".($i + 2),"5");
		    $sheet->setCellValue("K".($i + 1),"Dikembalikan ke Puskesmas");
		    $sheet->setCellValue("K".($i + 2),"6");
		    $sheet->setCellValue("L".($i + 1),"Dikembalikan ke Fasilitas Kes. Lain");
		    $sheet->setCellValue("L".($i + 2),"7");
		    $sheet->setCellValue("M".($i + 1),"Dikembalikan ke RS Lain");
		    $sheet->setCellValue("M".($i + 2),"8");
		    $sheet->mergeCells("N".$i.":P".$i)->setCellValue("N".$i,"Dirujuk");
		    $sheet->setCellValue("N".($i + 1),"Pasien Rujukan");
		    $sheet->setCellValue("N".($i + 2),"9");
		    $sheet->setCellValue("O".($i + 1),"Pasien Datang Sendiri");
		    $sheet->setCellValue("O".($i + 2),"10");
		    $sheet->setCellValue("P".($i + 1),"Diterima Kembali");
		    $sheet->setCellValue("P".($i + 2),"11");
		    $sheet->getStyle("A".$i.":P".($i + 2))->getFont()->setBold(true);
		    $sheet->getStyle("A".$i.":P".($i + 2))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		    $sheet->getStyle("A".$i.":P".($i + 2))->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
		    $i = $i + 2;

		    $data = json_decode($_POST['detail']);
		    foreach ($data as $d) {
		    	$i = $i + 1;
		    	$sheet->setCellValue("A".$i,$d->kode_propinsi);
				$sheet->setCellValue("B".$i,$d->kabupaten_kota);
				$sheet->setCellValue("C".$i,$d->kode_rs);		
				$sheet->setCellValue("D".$i,$d->nama_rs);		
				$sheet->setCellValue("E".$i,$d->tahun);		
				$sheet->setCellValue("F".$i,$d->nomor);		
				$sheet->setCellValue("G".$i,$d->spesialisasi);	
				$sheet->setCellValue("H".$i,$d->jumlah_diterima_dari_puskesmas);
				$sheet->setCellValue("I".$i,$d->jumlah_diterima_dari_faskes_lain);
				$sheet->setCellValue("J".$i,$d->jumlah_diterima_dari_rs_lain);	
				$sheet->setCellValue("K".$i,$d->jumlah_dikembalikan_ke_puskesmas);
				$sheet->setCellValue("L".$i,$d->jumlah_dikembalikan_ke_faskes_lain);
				$sheet->setCellValue("M".$i,$d->jumlah_dikembalikan_ke_rs_lain);
				$sheet->setCellValue("N".$i,$d->jumlah_dirujuk_pasien_rujukan);
				$sheet->setCellValue("O".$i,$d->jumlah_dirujuk_pasien_datang_sendiri);
				$sheet->setCellValue("P".$i,$d->jumlah_dirujuk_pasien_diterima_kembali);
			}

		    $thin = array ();
		    $thin['borders']=array();
		    $thin['borders']['allborders']=array();
		    $thin['borders']['allborders']['style']=PHPExcel_Style_Border::BORDER_THIN ;
		    $sheet->getStyle ( "A".$border_start.":P".$i )->applyFromArray ($thin);

		    foreach (range('A', 'P') as $columnID) {
		        $sheet->getColumnDimension($columnID)->setAutoSize(true);
		    }

		    header ( 'Content-Type: application/vnd.ms-excel' );
		    header ( 'Content-Disposition: attachment;filename="RL 3.14 - Rujukan.xls"' );
		    header ( 'Cache-Control: max-age=0' );
		    $writer = PHPExcel_IOFactory::createWriter ( $file, 'Excel5' );
		    $writer->save ( 'php://output' );
		}

		return;
	}
	$modal = $uitable->getModal();
	$form = $modal->getForm();
	$form->setTitle("RL 3.14 - Rujukan");

	$bulan_option = new OptionBuilder();
	$bulan_option->add("Semua", "%%", "1");
	$bulan_option->add("Januari", "1");
	$bulan_option->add("Februari", "2");
	$bulan_option->add("Maret", "3");
	$bulan_option->add("April", "4");
	$bulan_option->add("Mei", "5");
	$bulan_option->add("Juni", "6");
	$bulan_option->add("Juli", "7");
	$bulan_option->add("Agustus", "8");
	$bulan_option->add("September", "9");
	$bulan_option->add("Oktober", "10");
	$bulan_option->add("Nopember", "11");
	$bulan_option->add("Desember", "12");
	$bulan_select = new Select("rl314_bulan_filter", "rl314_bulan_filter", $bulan_option->getContent());
	$form->addElement("Bulan", $bulan_select);
	$tahun_rows = $db->get_result("
		SELECT tahun
		FROM (
			SELECT DISTINCT YEAR(tanggal) tahun
			FROM smis_rg_layananpasien
			WHERE prop = '' AND YEAR(tanggal) > 0
			UNION
			SELECT DISTINCT YEAR(tanggal_pulang) tahun
			FROM smis_rg_layananpasien
			WHERE prop = '' AND YEAR(tanggal_pulang) > 0
		) v
		ORDER BY tahun ASC
	");
	$tahun_option = new OptionBuilder();
	if ($tahun_rows != null) {
		foreach ($tahun_rows as $tahun_row) {
			if (date("Y") == $tahun_row->tahun)
				$tahun_option->add($tahun_row->tahun, $tahun_row->tahun, "1");
			else
				$tahun_option->add($tahun_row->tahun, $tahun_row->tahun);
		}
	}
	$tahun_select = new Select("rl314_tahun_filter", "rl314_tahun_filter", $tahun_option->getContent());
	$form->addElement("Tahun", $tahun_select);
	$excel_button = new Button("", "", "");
	$excel_button 	->addClass("btn-primary")
					->setIsButton(Button::$ICONIC)
					->setIcon("fa fa-file-excel-o")
					->setAction("rl314.excel()");
	$process_button = new Button("", "", "");
	$process_button	->addClass("btn-inverse")
					-> setIsButton(Button::$ICONIC)
					->setIcon("fa fa-circle-o")
					->setAction("rl314.view()");
	$button_group = new ButtonGroup("noprint");
	$button_group->addButton($process_button);
	$button_group->addButton($excel_button);
	$form->addElement("", $button_group);

	echo $form->getHtml();
	echo $uitable->getHtml();
	echo addJS("framework/smis/js/table_action.js");
	echo addJS("medical_record/resource/js/rl314.js", false);
?>