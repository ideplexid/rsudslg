<?php
	require_once("smis-base/smis-include-service-consumer.php");
	global $db;

	$head = array('Kode RS', 'Nama RS', 'Kode Propinsi', 'Kab/Kota', 'Tahun', 'BOR', 'AVLOS', 'BTO', 'TOI', 'NDR', 'GDR', 'Rerata Kunjungan');
	$uitable = new Table($head, "", null, true);
	$uitable->setName("rl12");
	$uitable->setAction(false);
	$uitable->setFooterVisible(false);

	if (isset($_POST ['command'])) {
		if ($_POST['command'] == "view") {
			$tahun = $_POST['tahun'];

			$kode_rs = "";
			$nama_rs = "";
			$kode_propinsi = "";
			$kabupaten_kota = "";
			$info_rs_row = $db->get_row("
				SELECT 
					*
				FROM 
					smis_mr_data_dasar_rs
				WHERE 
					prop = ''
				LIMIT 
					0, 1
			");
			if ($info_rs_row != null) {
				$kode_rs = $info_rs_row->koders;
				$nama_rs = $info_rs_row->nama_rs;
				$kode_propinsi = $info_rs_row->kode_provinsi;
				$kabupaten_kota = $info_rs_row->kota_kabupaten;
			}

			/// BOR = (Jumlah HP : (Jumlah Hari Setahun x Jumlah TT)) x 100%
            $jumlah_hp = 0;

			$period = new DatePeriod(
			     new DateTime($tahun . "-01-01"),
			     new DateInterval('P1D'),
			     new DateTime(($tahun + 1) . "-01-01")
			);
			foreach ($period as $key => $value) {
			    $date = $value->format('Y-m-d');

			    /// a. Jumlah Pasien Aktif :
				$row = $db->get_row("
					SELECT 
						COUNT(id) jumlah_hari
			    	FROM 
			    		smis_rg_layananpasien
			    	WHERE 
			    		prop = '' AND 
			    		carapulang NOT LIKE 'Tidak Datang' AND 
			    		uri = 1 AND 
			    		DATE(tanggal_inap) <= '" . $date . "' AND 
			    		tanggal_inap <> '0000-00-00 00:00:00' AND
			    		tanggal_pulang = '0000-00-00 00:00:00'
				");
				if ($row != null)
					$jumlah_hp += $row->jumlah_hari;

				/// b. Jumlah Pasien Pulang yang Tanggal Pulang Sesudah 'Sampai' :
				$row = $db->get_row("
					SELECT 
						COUNT(id) jumlah_hari
			    	FROM 
			    		smis_rg_layananpasien
			    	WHERE 
			    		prop = '' AND 
			    		carapulang NOT LIKE 'Tidak Datang' AND 
			    		uri = 1 AND 
			    		DATE(tanggal_inap) <= '" . $date . "' AND 
			    		tanggal_inap <> '0000-00-00 00:00:00' AND
			    		DATE(tanggal_pulang) > '" . $date . "'
				");
				if ($row != null)
					$jumlah_hp += $row->jumlah_hari;				    
			}

            $jumlah_hari_setahun = 0;
			for ($bulan = 1; $bulan <= 12; $bulan++)
				$jumlah_hari_setahun += cal_days_in_month(CAL_GREGORIAN, $bulan, $tahun);

			$jumlah_tt = 0;
			$jumlah_tt_row = $db->get_row("
				SELECT 
					SUM(jumlah_tt) jumlah
				FROM 
					smis_mr_kamar_tidur
				WHERE 
					prop = '' AND tahun = '" . $tahun . "'
			");
			if ($jumlah_tt_row != null)
				$jumlah_tt = $jumlah_tt_row->jumlah;

			$bor = round(($jumlah_hp / ($jumlah_hari_setahun * $jumlah_tt)) * 100, 2);
			//. End of BOR

			/// AVLOS = Jumlah LOS : Jumlah Pasien Keluar
			$lama_dirawat_rows = $db->get_result("
                SELECT 
                	DATE(tanggal_inap) tanggal_mulai, DATE(tanggal_pulang) tanggal_selesai
                FROM 
                	smis_rg_layananpasien
                WHERE 
                	prop = '' AND carapulang NOT LIKE 'Tidak Datang' AND YEAR(tanggal_pulang) = '" . $tahun . "' AND uri = 1;
            ");
            $total_los = 0;
            if ($lama_dirawat_rows != null) {
                foreach ($lama_dirawat_rows as $ldr) {
                    $tanggal_mulai = strtotime(ArrayAdapter::format("date Y-m-d", $ldr->tanggal_mulai));
                    $tanggal_selesai = strtotime(ArrayAdapter::format("date Y-m-d", $ldr->tanggal_selesai));
                    $datediff = $tanggal_selesai - $tanggal_mulai;
                    $los = ceil($datediff / (60 * 60 * 24));
                    if ($los < 1)
                        $los = 1;
                    $total_los += $los;
                }
            }
            $jumlah_pasien_keluar_row = $db->get_row("
            	SELECT 
            		COUNT(id) jumlah
            	FROM 
            		smis_rg_layananpasien
            	WHERE 
            		prop = '' AND carapulang NOT LIKE 'Tidak Datang' AND YEAR(tanggal_pulang) = '" . $tahun . "' AND uri = 1
            ");
            $jumlah_pasien_keluar = 0;
            if ($jumlah_pasien_keluar_row != null)
            	$jumlah_pasien_keluar = $jumlah_pasien_keluar_row->jumlah;
			$avlos = round($total_los / $jumlah_pasien_keluar, 2);
			//, End of AVLOS

			/// BTO = Jumlah Pasien Keluar : Jumlah TT
			$bto = round($jumlah_pasien_keluar / $jumlah_tt, 2);
			//. End of BTO

			/// TOI = ((Jumlah TT x Jumlah Hari Setahun) - Jumlah HP) : Jumlah Pasien Keluar
			$toi = round((($jumlah_tt * $jumlah_hari_setahun) - $jumlah_hp) / $jumlah_pasien_keluar, 2);
			//. End Of TOI

			/// NDR = (Jumlah 'Pasien Mati > 48 Jam' : Jumlah Pasien Keluar) x 1000 Permil
			$jumlah_pasien_mati_l48_row = $db->get_row("
            	SELECT 
            		COUNT(id) jumlah
            	FROM 
            		smis_rg_layananpasien
            	WHERE 
            		prop = '' AND carapulang LIKE 'Dipulangkan Mati%>%48 Jam' AND YEAR(tanggal_pulang) = '" . $tahun . "' AND uri = 1
            ");
            $jumlah_pasien_mati_l48 = 0;
            if ($jumlah_pasien_mati_l48_row != null)
            	$jumlah_pasien_mati_l48 = $jumlah_pasien_mati_l48_row->jumlah;
			$ndr = round(($jumlah_pasien_mati_l48 / $jumlah_pasien_keluar) * 1000, 2);
			//. End of NDR

			/// GDR = (Jumlah 'Pasien Mati' : Jumlah Pasien Keluar) x 1000 Permil
			$jumlah_pasien_mati_row = $db->get_row("
            	SELECT 
            		COUNT(id) jumlah
            	FROM 
            		smis_rg_layananpasien
            	WHERE 
            		prop = '' AND carapulang LIKE 'Dipulangkan Mati%' AND YEAR(tanggal_pulang) = '" . $tahun . "' AND uri = 1
            ");
            $jumlah_pasien_mati = 0;
            if ($jumlah_pasien_mati_row != null)
            	$jumlah_pasien_mati = $jumlah_pasien_mati_row->jumlah;
			$gdr = round(($jumlah_pasien_mati / $jumlah_pasien_keluar) * 1000, 2);
			//. End of GDR

			/// Rerata Kunjungan = Jumlah Kunjungan Pasien RJ Non-IGD : Jumlah Hari Buka RJ
			$jumlah_hari_buka_rj = 0;
			$jumlah_kunjungan_pasien_rj = 0;

			for ($bulan = 1; $bulan <= 12; $bulan++) {
				$jumlah_hari = cal_days_in_month(CAL_GREGORIAN, $bulan, $tahun);
				for ($hari = 1; $hari <= $jumlah_hari; $hari++) {
					$tanggal = $hari . "-" . $bulan . "-" . $tahun;
					$nama_hari = date("D", strtotime($tanggal));
					if ($nama_hari != "Sun") {
						$jumlah_hari_buka_rj += 1;
						$tanggal = $tahun . "-" . $bulan . "-" . $hari;
						$slug_igd = "igd";
						$jumlah_kunjungan_row = $db->get_row("
			            	SELECT 
			            		COUNT(id) jumlah
			            	FROM 
			            		smis_rg_layananpasien
			            	WHERE 
			            		prop = '' AND carapulang NOT LIKE 'Tidak Datang' AND DATE(tanggal) = '" . $tanggal . "' AND uri = 0 AND jenislayanan NOT LIKE '" . $slug_igd . "'
			            ");
			            if ($jumlah_kunjungan_row != null)
			            	$jumlah_kunjungan_pasien_rj += $jumlah_kunjungan_row->jumlah;
					}
				}
			}

			$rerata_kunjungan = round($jumlah_kunjungan_pasien_rj / $jumlah_hari_buka_rj, 2);
			//. End of Rerata Kunjungan

			$html = "<tr>" .
						"<td id='rl12_kode_rs'>" . $kode_rs . "</td>" .
						"<td id='rl12_nama_rs'>" . $nama_rs . "</td>" .
						"<td id='rl12_kode_propinsi'>" . $kode_propinsi . "</td>" .
						"<td id='rl12_kabupaten_kota'>" . $kabupaten_kota . "</td>" .
						"<td id='rl12_tahun'>" . $tahun . "</td>" .
						"<td id='rl12_f_bor'><div align='right'>" . $bor . " %</div></td>" .
						"<td id='rl12_bor' style='display:none;'>" . $bor . "</td>" .
						"<td id='rl12_f_avlos'><div align='right'>" . $avlos . " hari</div></td>" .
						"<td id='rl12_avlos' style='display:none;'>" . $avlos . "</td>" .
						"<td id='rl12_f_bto'><div align='right'>" . $bto . " hari</div></td>" .
						"<td id='rl12_bto' style='display:none;'>" . $bto . "</td>" .
						"<td id='rl12_f_toi'><div align='right'>" . $toi . " hari</div></td>" .
						"<td id='rl12_toi' style='display:none;'>" . $toi . "</td>" .
						"<td id='rl12_f_ndr'><div align='right'>" . $ndr . " &#8240;</div></td>" .
						"<td id='rl12_ndr' style='display:none;'>" . $ndr . "</td>" .
						"<td id='rl12_f_gdr'><div align='right'>" . $gdr . " &#8240;</div></td>" .
						"<td id='rl12_gdr' style='display:none;'>" . $gdr . "</td>" .
						"<td id='rl12_f_rerata_kunjungan'><div align='right'>" . $rerata_kunjungan . "</div></td>" .
						"<td id='rl12_rerata_kunjungan' style='display:none;'>" . $rerata_kunjungan . "</td>" .
					"</tr>";
			$data['html'] = $html;
			echo json_encode($data);
		} else if ($_POST['command'] == "export_xls") {
			require_once ("smis-libs-out/php-excel/PHPExcel.php");

		    $kode_rs = $_POST['kode_rs'];
		    $nama_rs = $_POST['nama_rs'];
		    $kode_propinsi = $_POST['kode_propinsi'];
		    $kabupaten_kota = $_POST['kabupaten_kota'];
		    $tahun = $_POST['tahun'];
		    $bor = $_POST['bor'];
		    $avlos = $_POST['avlos'];
		    $bto = $_POST['bto'];
		    $toi = $_POST['toi'];
		    $ndr = $_POST['ndr'];
		    $gdr = $_POST['gdr'];
		    $rerata_kunjungan = $_POST['rerata_kunjungan'];

		    $file = new PHPExcel ();
		    $file->getProperties ()->setCreator ( "PT. Inovasi Ide Utama" );
		    $file->getProperties ()->setTitle ( "RL 1.2 - Indikator Pelayanan" );
		    $file->getProperties ()->setSubject ( "RL 1.2 - Indikator Pelayanan" );
		    $file->getProperties ()->setDescription ( "RL 1.2 - Indikator Pelayanan Generated by System" );
		    $file->getProperties ()->setKeywords ( "RL 1.2 - Indikator Pelayanan" );
		    $file->getProperties ()->setCategory ( "RL 1.2 - Indikator Pelayanan" );

		    $sheet = $file->getActiveSheet ();
		    $i = 1;

		    $sheet->mergeCells("A".$i.":L".$i)->setCellValue("A".$i,"RL 1.2 - Indikator Pelayanan");
		    $sheet->getStyle("A".$i.":L".$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		    $sheet->getStyle("A".$i)->getFont()->setBold(true);
		    $i = $i + 2;

		    $border_start = $i;
		    $sheet->setCellValue("A".$i,"KODE RS");
		    $sheet->setCellValue("B".$i,"NAMA RS");
		    $sheet->setCellValue("C".$i,"KODE PROPINSI");
		    $sheet->setCellValue("D".$i,"KAB/KOTA");
		    $sheet->setCellValue("E".$i,"TAHUN");
		    $sheet->setCellValue("F".$i,"BOR");
		    $sheet->setCellValue("G".$i,"AVLOS");
		    $sheet->setCellValue("H".$i,"BTO");
		    $sheet->setCellValue("I".$i,"TOI");
		    $sheet->setCellValue("J".$i,"NDR");
		    $sheet->setCellValue("K".$i,"GDR");
		    $sheet->setCellValue("L".$i,"RERATA KUNJUNGAN");
		    
		    $sheet->getStyle("A".$i.":L".$i)->getFont()->setBold(true);
		    $sheet->getStyle("A".$i.":L".$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		    $sheet->getStyle("A".$i.":L".$i)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
		    $i = $i + 1;

		    $sheet->setCellValue("A".$i, $kode_rs);
		    $sheet->setCellValue("B".$i, $nama_rs);
		    $sheet->setCellValue("C".$i, $kode_propinsi);
		    $sheet->setCellValue("D".$i, $kabupaten_kota);
		    $sheet->setCellValue("E".$i, $tahun);
		    $sheet->setCellValue("F".$i, $bor);
		    $sheet->setCellValue("G".$i, $avlos);
		    $sheet->setCellValue("H".$i, $bto);
		    $sheet->setCellValue("I".$i, $toi);
		    $sheet->setCellValue("J".$i, $ndr);
		    $sheet->setCellValue("K".$i, $gdr);
		    $sheet->setCellValue("L".$i, $rerata_kunjungan);
		    $thin = array ();
		    $thin['borders']=array();
		    $thin['borders']['allborders']=array();
		    $thin['borders']['allborders']['style']=PHPExcel_Style_Border::BORDER_THIN ;
		    $sheet->getStyle ( "A".$border_start.":L".$i )->applyFromArray ($thin);

		    foreach (range('A', 'L') as $columnID) {
		        $sheet->getColumnDimension($columnID)->setAutoSize(true);
		    }

		    header ( 'Content-Type: application/vnd.ms-excel' );
		    header ( 'Content-Disposition: attachment;filename="RL 1.2 - Indikator Pelayanan.xls"' );
		    header ( 'Cache-Control: max-age=0' );
		    $writer = PHPExcel_IOFactory::createWriter ( $file, 'Excel5' );
		    $writer->save ( 'php://output' );
		}
		return;
	}
	$modal = $uitable->getModal();
	$form = $modal->getForm();
	$form->setTitle("RL 1.2 - Indikator Pelayanan");

	$tahun_rows = $db->get_result("
		SELECT DISTINCT YEAR(tanggal) tahun
		FROM smis_rg_layananpasien
		WHERE prop = '' AND carapulang NOT LIKE 'TIDAK DATANG' AND YEAR(tanggal) > 0
		ORDER BY YEAR(tanggal) ASC
	");
	$tahun_option = new OptionBuilder();
	if ($tahun_rows != null) {
		foreach ($tahun_rows as $tahun_row) {
			if (date("Y") == $tahun_row->tahun)
				$tahun_option->add($tahun_row->tahun, $tahun_row->tahun, "1");
			else
				$tahun_option->add($tahun_row->tahun, $tahun_row->tahun);
		}
	}
	$tahun_select = new Select("rl12_tahun_filter", "rl12_tahun_filter", $tahun_option->getContent());
	$form->addElement("Tahun", $tahun_select);
	$help_button = new Button("", "", "");
	$help_button 	->addClass("btn-success")
					->setIsButton(Button::$ICONIC)
					->setIcon("fa fa-question-circle")
					->setAction("rl12.help()");
	$excel_button = new Button("", "", "");
	$excel_button 	->addClass("btn-primary")
					->setIsButton(Button::$ICONIC)
					->setIcon("fa fa-file-excel-o")
					->setAction("rl12.excel()");
	$process_button = new Button("", "", "");
	$process_button	->addClass("btn-inverse")
					-> setIsButton(Button::$ICONIC)
					->setIcon("fa fa-circle-o")
					->setAction("rl12.view()");
	$button_group = new ButtonGroup("noprint");
	$button_group->addButton($help_button);
	$button_group->addButton($process_button);
	$button_group->addButton($excel_button);
	$form->addElement("", $button_group);

	echo $form->getHtml();
	echo $uitable->getHtml();
	echo addJS("framework/smis/js/table_action.js");
	echo addJS("medical_record/resource/js/rl12.js", false);
?>