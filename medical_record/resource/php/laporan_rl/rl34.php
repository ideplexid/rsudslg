<?php

global $db;

$header=array(	"TAHUN",
                "KODE PROPINSI",
                "KODE RS",
                "NAMA RS",
                "KAB/KOTA",
                "NO",
                "JENIS KEGIATAN",
                "RM RUMAH SAKIT",
                "RM BIDAN",
                "RM PUSKESMAS",
                "RM FASKES LAINNYA",
                "RM HIDUP",
                "RM MATI",
                "RM TOTAL",
                "RNM HIDUP",
                "RNM MATI",
                "RNM TOTAL",
                "NR HIDUP",
                "NR MATI",
                "NR TOTAL",
                "DIRUJUK"
            );
            
$jenis_kegiatan = array();
$jenis_kegiatan[] = "1 - Persalinan Normal";
$jenis_kegiatan[] = "2 - Sectio Caesaria";
$jenis_kegiatan[] = "3 - Persalinan dengan Komplikasi";
$jenis_kegiatan[] = "3.1 - Pendarahan Sebelum Persalinan";
$jenis_kegiatan[] = "3.2 - Pendarahan Sesudah Persalinan";
$jenis_kegiatan[] = "3.3 - Pre Eclampsi";
$jenis_kegiatan[] = "3.4 - Eclampsi";
$jenis_kegiatan[] = "3.5 - Infeksi";
$jenis_kegiatan[] = "3.6 - Lain-Lain";
$jenis_kegiatan[] = "4 - Abortus";
$jenis_kegiatan[] = "5.1 - Imunisasi-TT1";
$jenis_kegiatan[] = "5.2 - Imunisasi-TT2";
            
$uitable=new Table($header);
$uitable->setName("rl34")
		->setActionEnable(false)
		->setFooterVisible(false);
        
if(isset($_POST['command']) && $_POST['command']) {
    $dbtable = new DBTable ( $db, "smis_mr_persalinan");
    if(isset($_POST['bulan']) && $_POST['bulan']!="") {
        $dbtable->addCustomKriteria(""," MONTH(tanggal) LIKE '".$_POST['bulan']."' ");
    }
    if(isset($_POST['tahun']) && $_POST['tahun']!="") {
        $dbtable->addCustomKriteria(""," YEAR(tanggal) = '".$_POST['tahun']."' ");
    }
    $dbtable->setShowAll(true);
    
    require_once "medical_record/class/adapter/RL34Adapter.php";
	$adapter = new RL34Adapter();
    
    $adapter->setTahun($_POST['tahun']);

    $profil_instansi_row = $db->get_row("
        SELECT *
        FROM smis_mr_data_dasar_rs
        WHERE prop = ''
        LIMIT 0, 1
    ");

    $kode_propinsi = "";
    $kode_rs = "";
    $nama_rs = "";
    $kab_kota = "";

    if ($profil_instansi_row != null) {
        $kode_propinsi = $profil_instansi_row->kode_provinsi;
        $kode_rs = $profil_instansi_row->koders;
        $nama_rs = $profil_instansi_row->nama_rs;
        $kab_kota = $profil_instansi_row->kota_kabupaten;
    }

    $adapter->setKodePropinsi($kode_propinsi);
    $adapter->setKodeRS($kode_rs);
    $adapter->setNamaRS($nama_rs);
    $adapter->setKabKota($kab_kota);
    $adapter->setJenisKegiatan($jenis_kegiatan);
    
    require_once "medical_record/class/responder/RL34Responder.php";
    $dbres = new RL34Responder($dbtable, $uitable, $adapter);
	$hasil = $dbres->command($_POST['command']);
	echo json_encode($hasil);
	return;
}

$bulan_option = new OptionBuilder();
$bulan_option->add("Semua", "%%", "1");
$bulan_option->add("Januari", "1");
$bulan_option->add("Februari", "2");
$bulan_option->add("Maret", "3");
$bulan_option->add("April", "4");
$bulan_option->add("Mei", "5");
$bulan_option->add("Juni", "6");
$bulan_option->add("Juli", "7");
$bulan_option->add("Agustus", "8");
$bulan_option->add("September", "9");
$bulan_option->add("Oktober", "10");
$bulan_option->add("Nopember", "11");
$bulan_option->add("Desember", "12");

$tahun_rows = $db->get_result("
    SELECT DISTINCT YEAR(tanggal) tahun
    FROM smis_mr_persalinan
    WHERE prop = '' AND YEAR(tanggal) > 0
    ORDER BY YEAR(tanggal) ASC
");
$tahun_option = new OptionBuilder();
if ($tahun_rows != null) {
    foreach ($tahun_rows as $tahun_row) {
        if (date("Y") == $tahun_row->tahun)
            $tahun_option->add($tahun_row->tahun, $tahun_row->tahun, "1");
        else
            $tahun_option->add($tahun_row->tahun, $tahun_row->tahun);
    }
}

$uitable->clearContent();
$uitable->addModal("bulan", "select", "Bulan", $bulan_option->getContent())
		->addModal("tahun", "select", "Tahun", $tahun_option->getContent());
        
$view=new Button("","","View");
$view->setClass("btn-primary")
		->setIsButton(Button::$ICONIC)
		->setIcon("fa fa-refresh")
		->setAction("rl34.view()");
        
$excel=new Button("","","Excel");
$excel->setClass("btn-primary")
	->setIsButton(Button::$ICONIC)
	->setIcon("fa fa-file-excel-o")
	->setAction("rl34.excel()");

$btng=new ButtonGroup("");
$btng->addButton($view);
$btng->addButton($excel);

$form=$uitable
	  ->getModal()
	  ->setTitle("Medical Record")
	  ->getForm()
	  ->addElement("",$btng);
      
echo "<h2>RL 3.4. KEBIDANAN</h2>";
echo $form->getHtml();
echo $uitable->getHtml();

echo addJS("framework/bootstrap/js/bootstrap-datepicker.js");
echo addJS("framework/smis/js/table_action.js");
echo addCSS("framework/bootstrap/css/datepicker.css");

?>

<script>

var rl34;

$(document).ready(function(){
	$('.mydate').datepicker();
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
	rl34=new TableAction("rl34","medical_record","rl34",new Array());
	rl34.getRegulerData=function(){
		var reg_data={	
				page:this.page,
				action:this.action,
				super_command:this.super_command,
				prototype_name:this.prototype_name,
				prototype_slug:this.prototype_slug,
				prototype_implement:this.prototype_implement,
				bulan:$("#"+this.prefix+"_bulan").val(),
				tahun:$("#"+this.prefix+"_tahun").val()
				};
		return reg_data;
	};
	
});

</script>