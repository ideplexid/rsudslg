<?php

$header = array ();
$header [] = "Tanggal";
$header [] = "P-RJ";
$header [] = "P-MRS";
$header [] = "P-DS";
$header [] = "P-Lain";
$header [] = "P-Dokter";
$header [] = "P-PKM";
$header [] = "P-PRBD";
$header [] = "P-JML";
$header [] = "S-RJ";
$header [] = "S-MRS";
$header [] = "S-Lain";
$header [] = "S-DS";
$header [] = "S-Dokter";
$header [] = "S-PKM";
$header [] = "S-PRBD";
$header [] = "S-JML";
$header [] = "M-RJ";
$header [] = "M-MRS";
$header [] = "M-Lain";
$header [] = "M-DS";
$header [] = "M-Dokter";
$header [] = "M-PKM";
$header [] = "M-PRBD";
$header [] = "M-JML";
$header [] = "Total";

$uitable = new Table ( $header, "", NULL, false );
$uitable->setName ( "lap_igd_data" );

$uitable->setFooterVisible ( false );

$uitable->addHeader ( "before", "<tr>
								<th rowspan='3'>Tanggal</th>
								<th colspan='8'>Pagi</th>
								<th colspan='8'>Sore</th>
								<th colspan='8'>Malam</th>
								<th rowspan='3'>Total</th>
							</tr>" );
$uitable->addHeader ( "before", "<tr>
								<th rowspan='2'>RJ</th>
								<th rowspan='2'>MRS</th>
								<th colspan='5'>KIRIMAN</th>								
								<th rowspan='2'>JML</th>
		
								<th rowspan='2'>RJ</th>
								<th rowspan='2'>MRS</th>
								<th colspan='5'>KIRIMAN</th>								
								<th rowspan='2'>JML</th>
		
								<th rowspan='2'>RJ</th>
								<th rowspan='2'>MRS</th>
								<th colspan='5'>KIRIMAN</th>
								<th rowspan='2'>JML</th>
								</tr>" );

$uitable->addHeader ( "before", "<tr>
								<th>DS</th>
								<th>Dokter</th>
								<th>PKM/RS</th>
								<th>PR/BD</th>
								<th>Lainya</th>
								<th>DS</th>
								<th>Dokter</th>
								<th>PKM/RS</th>
								<th>PR/BD</th>
								<th>Lainya</th>
								<th>DS</th>
								<th>Dokter</th>
								<th>PKM/RS</th>
								<th>PR/BD</th>
								<th>Lainya</th>
		</tr>" );
$uitable->setHeaderVisible ( false );

if (isset ( $_POST ['command'] )) {
	require_once 'medical_record/class/adapter/TriageIGDData.php';
	$adapter = new TriageIGDData ();
	$dbtable = new DBTable ( $db, "smis_vmr_igd" );
	if (isset ( $_POST ['dari'] ) && isset ( $_POST ['sampai'] ) && $_POST ['dari'] != "" && $_POST ['sampai'] != "") {
		$dari = $_POST ['dari'];
		$sampai = $_POST ['sampai'];
		$dbtable->addCustomKriteria ( "'" . $dari . "'", "<=tgl_lap" );
		$dbtable->addCustomKriteria ( "'" . $sampai . "'", ">tgl_lap" );
		$dbtable->setShowAll ( true );
	}
	$dbtable->setOrder ( " tgl_lap ASC " );
	
	$dbres = new DBResponder ( $dbtable, $uitable, $adapter );
	$data = $dbres->command ( $_POST ['command'] );
	echo json_encode ( $data );
	return;
}

$uitable->addModal ( "dari", "date", "Dari", "" );
$uitable->addModal ( "sampai", "date", "Sampai", "" );
$uitable->addModal ( "ruangan", "hidden", "", "igd" );

$modal = $uitable->getModal ();
$form = $modal->getForm ();

$button = new Button ( "", "", "" );
$button->setIsButton ( Button::$ICONIC );
$button->setIcon ( "fa fa-refresh" );
$button->setAction ( "lap_igd_data.view()" );
$button->setClass("btn-primary");
$form->addElement ( "", $button );
$button = new Button ( "", "", "" );
$button->setIsButton ( Button::$ICONIC );
$button->setClass("btn-primary");
$button->setIcon ( "fa fa-print ");
$button->setAction ( "smis_print($('#print_table_lap_igd_data').html())" );
$form->addElement ( "", $button );
echo $form->getHtml ();

echo $uitable->getHtml ();
echo addJS ( "framework/smis/js/table_action.js" );
echo addJS ( "framework/bootstrap/js/bootstrap-datepicker.js" );
echo addCSS ( "framework/bootstrap/css/datepicker.css" );

?>
<script type="text/javascript">
var lap_igd_data;
//var employee;
$(document).ready(function(){
	$('.mydate').datepicker();
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
	var column=new Array('id','nama','keterangan','slug');
	lap_igd_data=new TableAction("lap_igd_data","medical_record","lap_igd_data",column);
	lap_igd_data.getRegulerData=function(){
		var reg_data={	
				page:this.page,
				action:this.action,
				super_command:this.super_command,
				prototype_name:this.prototype_name,
				prototype_slug:this.prototype_slug,
				prototype_implement:this.prototype_implement,
				dari:$("#"+this.prefix+"_dari").val(),
				sampai:$("#"+this.prefix+"_sampai").val()
				};
		return reg_data;
	};
	
	
});
</script>

<style type="text/css">
#table_lap_igd_data {
	font-size: 12px;
}
</style>