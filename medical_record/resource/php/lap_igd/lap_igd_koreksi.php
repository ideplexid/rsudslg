<?php 
require_once 'smis-libs-class/MasterSlaveTemplate.php';
global $db;


$button=new Button("", "", "");
$button->setAction("lap_igd_koreksi.view()");
$button->setClass("btn-primary");
$button->setIsButton(Button::$ICONIC);
$button->setIcon("fa fa-refresh");

$cetak=new Button("", "", "");
$cetak->setAction("lap_igd_koreksi.print()");
$cetak->setClass("btn-primary");
$cetak->setIsButton(Button::$ICONIC);
$cetak->setIcon("fa fa-print");

$koreksi=new MasterSlaveTemplate($db, "smis_mr_igd", "medical_record", "lap_igd_koreksi");
$koreksi->getDBtable()
		->setOrder("tanggal ASC");

if($koreksi->getDBResponder()->isView()){
	$dbtable=$koreksi->getDBtable();
	if(isset($_POST['dari']) && $_POST['dari']!="" ) 
		$dbtable->addCustomKriteria(NULL, "tanggal>='".$_POST['dari']."'");
	if(isset($_POST['sampai']) && $_POST['sampai']!="")
		$dbtable->addCustomKriteria(NULL, "tanggal<'".$_POST['sampai']."'");
	if(isset($_POST['f_kasus']) && $_POST['f_kasus']!="%")
		$dbtable->addCustomKriteria(NULL, " pola_kasus LIKE '".$_POST['f_kasus']."'");
	if(isset($_POST['f_triage']) && $_POST['f_triage']!="%")
		$dbtable->addCustomKriteria(NULL, " triage LIKE '".$_POST['f_triage']."'");
	
	if(isset($_POST['f_tlk']) && $_POST['f_tlk']!="%")
		$dbtable->addCustomKriteria(NULL, " menolak_mrs LIKE '".$_POST['f_tlk']."'");
	if(isset($_POST['f_ver']) && $_POST['f_ver']!="%")
		$dbtable->addCustomKriteria(NULL, " ver LIKE '".$_POST['f_ver']."'");
	if(isset($_POST['f_ds']) && $_POST['f_ds']!="%")
		$dbtable->addCustomKriteria(NULL, " kiriman LIKE '".$_POST['f_ds']."'");
	if(isset($_POST['f_kelanjutan']) && $_POST['f_kelanjutan']!="%")
		$dbtable->addCustomKriteria(NULL, " kelanjutan LIKE '".$_POST['f_kelanjutan']."'");
	if(isset($_POST['f_abh']) && $_POST['f_abh']!="%")
		$dbtable->addCustomKriteria(NULL, " alat_bantu_hidup LIKE '".$_POST['f_abh']."'");
	if(isset($_POST['f_carabayar']) && $_POST['f_carabayar']!="%")
		$dbtable->addCustomKriteria(NULL, " carabayar LIKE '".$_POST['f_carabayar']."'");
}


$uitable=$koreksi->getUItable();
$header=array("No.","Tanggal","Ditangani","RT","Nama","NRM","Noreg","P. Kasus","Triage","RJK","T.MRS","Ver","DS","KRM","Kelanjutan","ABH","Cara Bayar");
$uitable->setHeader($header);

$adapter=new SummaryAdapter();
$adapter->addFixValue("Tanggal", "<strong>Total</strong>");
$adapter->setUseNumber(true, "No.","back.");
$adapter->add("Tanggal", "tanggal","date d M Y H:i")
		->add("Ditangani", "ditangani","date d M Y H:i")
		->add("RT", "response","back mnt")
		->add("Nama", "nama_pasien")
		->add("NRM", "nrm_pasien","only-digit8")
		->add("Noreg", "noreg_pasien","only-digit8")
		->add("P. Kasus", "pola_kasus")
		->add("Triage", "triage")
		->add("RJK", "rujuk")
		->add("T.MRS", "menolak_mrs","trivial_1_V_")
		->add("Ver", "ver","trivial_1_V_")
		->add("Shift", "shift")
		->add("DS", "ds","trivial_1_V")
		->add("KRM", "kiriman")
		->add("Kelanjutan", "kelanjutan")
		->add("ABH", "alat_bantu_hidup","trivial_1_V_")
		->add("Cara Bayar", "carabayar","unslug");

$adapter->addSummary("T.MRK", "tolak_mrs")
		->addSummary("VER", "ver")
		->addSummary("DS", "ds")
		->addSummary("ABH", "alat_bantu_hidup");
		
$koreksi->setAdapter($adapter);


$koreksi->getUItable()
		->setAddButtonEnable(false)
		->setDelButtonEnable(false)
		->setReloadButtonEnable(false)		
		->setActionEnable(true);


if($koreksi->getDBResponder()->isPreload()){
	
		require_once "smis-base/smis-include-service-consumer.php";
		$ser=new ServiceConsumer($db,"get_jenis_patient",NULL,"registration");
		$ser->execute();
		$ctx=$ser->getContent();
		$ctx[]=array("name"=>"-- Kosong --","value"=>"");
		$ctx[]=array("name"=>"-- Semua --","value"=>"%","default"=>"1");
		
		$kelanjutan = new OptionBuilder ();		
		$kelanjutan->add ( "Rawat Jalan", "RJ" );
		$kelanjutan->add ( "Rujuk ke RS Lain", "RJK" );
		$kelanjutan->add ( "Masuk Rumah Sakit (Rawat Inap)", "MRS");
		$klj=$kelanjutan->getContent();
		$kelanjutan->add ( " -- Kosong --", "" );
		$kelanjutan->add ( " -- Semua --", "%","1" );
		
		
		$kiriman = new OptionBuilder ();		
		$kiriman->add ( "Dokter", "Dokter" );
		$kiriman->add ( "PKM/RS", "PKM" );
		$kiriman->add ( "PR/BD", "PR/BD");
		$kiriman->add ( "Lainya", "Lain" );
		$krm=$kiriman->getContent();
		$kiriman->add ( "Datang Sendiri", "" );
		$kiriman->add ( " -- Semua --", "%","1" );
		
		$pola_kasus = new OptionBuilder ();
		$pola_kasus->add ( "Bedah - Penganiayaan", "Bedah - Penganiayaan" );
		$pola_kasus->add ( "Bedah - Kecelakaan Kerja", "Bedah - Kecelakaan Kerja" );
		$pola_kasus->add ( "Bedah - KLL", "Bedah - KLL");
		$pola_kasus->add ( "Bedah - Non Trauma", "Bedah - Non Trauma" );
		$pola_kasus->add ( "Medis (Non Bedah)", "Medis" );
		$pola_kasus->add ( "VK (Kebidanan)", "VK");
		$pola_kasus->add ( "Anak", "Anak" );
		$pola_kasus->add ( "Psikiatrik", "Psikiatrik");
		$pk=$pola_kasus->getContent();
		$pola_kasus->add ( " -- Kosong --", "" );
		$pola_kasus->add ( " -- Semua --", "%","1" );
		
		$triage = new OptionBuilder ();
		$triage->add ( "Merah", "Merah" );
		$triage->add ( "Kuning", "Kuning");
		$triage->add ( "Hijau", "Hijau" );
		$triage->add ( "Hitam DOA", "Hitam DOA" );
		$triage->add ( "Hitam Mati", "Hitam Mati" );
		$trg=$triage->getContent();		
		$triage->add ( " -- Kosong --", "" );
		$triage->add ( " -- Semua --", "%","1" );
		
		$yt = new OptionBuilder ();
		$yt->add(" -- Semua --","%","1");
		$yt->add("Ya","1");
		$yt->add("Tidak","0");
		
		
$koreksi->addFlag("dari", "Tanggal Awal", "Silakan Masukan Tanggal Mulai")
		->addFlag("sampai", "Tanggal Akhir", "Silakan Masukan Tanggal Akhir")
		->addNoClear("dari")
		->addNoClear("sampai")
		->setDateTimeEnable(true)
		->getUItable()
		->setAddButtonEnable(false)
		->setDelButtonEnable(false)
		->setReloadButtonEnable(false)		
		->setActionEnable(true)
		->setFooterVisible(true)
		->addModal("dari", "datetime", "Awal", "")
		->addModal("sampai", "datetime", "Akhir", "")
		->addModal("f_kasus", "select", "P. Kasus", $pola_kasus->getContent())
		->addModal("f_triage", "select", "Triage", $triage->getContent())
		->addModal("f_tlk", "select", "T. MRS", $yt->getContent())
		->addModal("f_ver", "select", "Ver", $yt->getContent())
		->addModal("f_ds", "select", "Kedatangan", $kiriman->getContent())
		->addModal("f_kelanjutan", "select", "Kelanjutan", $kelanjutan->getContent())
		->addModal("f_abh", "select", "ABH", $yt->getContent())
		->addModal("f_carabayar", "select", "Carabayar", $ctx);
$koreksi->getForm()
		->addElement("", $button)
		->addElement("", $cetak);

$koreksi->getUItable()
		->addModal("id","hidden","","")
		->addModal("nama_pasien","text","Nama Pasien","","n",null,true)
		->addModal("nrm_pasien","text","NRM","","n",null,true)
		->addModal("noreg_pasien","text","No Reg","","n",null,true)
		->addModal("pola_kasus","select","Pola Kasus",$pk)
		->addModal("triage","select","Triage",$trg)
		->addModal("menolak_mrs","checkbox","Menolak MRS","")
		->addModal("ver","checkbox","Ver","")
		->addModal("ds","checkbox","Datang Sendiri","")
		->addModal("kiriman","select","Kiriman",$krm)
		->addModal("rujuk","text","Rujuk","")
		->addModal("kelanjutan","select","Kelanjutan",$klj)
		->addModal("alat_bantu_hidup","checkbox","Alat Bantu Hidup","");
}


$koreksi->setModalTitle("Data IGD");
$koreksi->getModal()->setComponentSize(Modal::$MEDIUM);
$koreksi->addViewData("dari", "dari")
		->addViewData("sampai", "sampai")
		->addViewData("f_kasus", "f_kasus")
		->addViewData("f_triage", "f_triage")
		->addViewData("f_tlk", "f_tlk")
		->addViewData("f_ver", "f_ver")
		->addViewData("f_ds", "f_ds")
		->addViewData("f_kelanjutan", "f_kelanjutan")
		->addViewData("f_abh", "f_abh")
		->addViewData("f_carabayar", "f_carabayar");
$koreksi->addResouce("js","medical_record/resource/js/lap_igd_koreksi.js","after");
$koreksi->initialize();
?>