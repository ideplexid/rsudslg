<?php

$header = array ();
$header [] = "Jenis Layanan";
$header [] = "Rujukan";
$header [] = "Non Rujukan";
$header [] = "Lk";
$header [] = "Pr";
$header [] = "MRS";
$header [] = "Dirujuk";
$header [] = "Rawat Jalan";
$header [] = "DOA";
$header [] = "Meninggal";
$header [] = "Total";

$uitable = new Table ( $header, "", NULL, false );
$uitable->setName ( "lap_igd_kunjungan" );

$uitable->setFooterVisible ( false );

$uitable->addHeader ( "before", "<tr>
								<th rowspan='2'>Jenis Layanan</th>
                                <th colspan='2'>Cara Datang</th>
                                <th colspan='2'>Jenis Kelamin</th>
								<th colspan='5'>Tindak Lanjut Pelayanan</th>
								<th rowspan='2'>Total</th>
							</tr>" );
$uitable->addHeader ( "before", "<tr>
								<th>Rujukan</th>
								<th>Non Rujukan</th>
								<th>Laki-Laki</th>
								<th>Perempuan</th>
								<th>MRS</th>
								<th>Dirujuk</th>
								<th>Pulang</th>								
								<th>Mati (Sebelum dirawat)</th>
								<th>Mati (Setelah dirawat)</th>
								</tr>" );

$uitable->setHeaderVisible ( false );


if (isset ( $_POST ['command'] )) {
	require_once 'medical_record/class/adapter/KunjunganIGDAdapter.php';
	$adapter = new KunjunganIGDAdapter ();
	$dbtable = new DBTable ( $db, "smis_mr_igd" );
	if (isset ( $_POST ['dari'] ) && isset ( $_POST ['sampai'] ) && $_POST ['dari'] != "" && $_POST ['sampai'] != "") {
		$dari = $_POST ['dari'];
		$sampai = $_POST ['sampai'];
		$dbtable->addCustomKriteria ( "'" . $dari . "'", "<=tanggal" );
		$dbtable->addCustomKriteria ( "'" . $sampai . "'", ">tanggal" );
		$dbtable->setShowAll ( true );
	}	
	$dbres = new DBResponder ( $dbtable, $uitable, $adapter );
	$data = $dbres->command ( $_POST ['command'] );
	echo json_encode ( $data );
	return;
}

$uitable->addModal ( "dari", "datetime", "Dari", "" );
$uitable->addModal ( "sampai", "datetime", "Sampai", "" );
$uitable->addModal ( "ruangan", "hidden", "", "igd" );

$modal = $uitable->getModal ();
$form = $modal->getForm ();

$button = new Button ( "", "", "" );
$button->setIsButton ( Button::$ICONIC );
$button->setIcon ( "fa fa-refresh" );
$button->setAction ( "lap_igd_kunjungan.view()" );
$button->setClass("btn-primary");
$form->addElement ( "", $button );
$button = new Button ( "", "", "" );
$button->setIsButton ( Button::$ICONIC );
$button->setClass("btn-primary");
$button->setIcon ( "fa fa-print ");
$button->setAction ( "smis_print($('#print_table_lap_igd_kunjungan').html())" );
$form->addElement ( "", $button );
echo $form->getHtml ();

echo $uitable->getHtml ();
echo addJS ( "framework/smis/js/table_action.js" );
echo addJS ( "framework/bootstrap/js/bootstrap-datetimepicker.js" );
echo addCSS ( "framework/bootstrap/css/bootstrap-datetimepicker.css" );

?>
<script type="text/javascript">
var lap_igd_kunjungan;
//var employee;
$(document).ready(function(){
	$('.mydatetime').datetimepicker({minuteStep:1});
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
	var column=new Array('id','nama','keterangan','slug');
	lap_igd_kunjungan=new TableAction("lap_igd_kunjungan","medical_record","lap_igd_kunjungan",column);
	lap_igd_kunjungan.getRegulerData=function(){
		var reg_data={	
				page:this.page,
				action:this.action,
				super_command:this.super_command,
				prototype_name:this.prototype_name,
				prototype_slug:this.prototype_slug,
				prototype_implement:this.prototype_implement,
				dari:$("#"+this.prefix+"_dari").val(),
				sampai:$("#"+this.prefix+"_sampai").val()
				};
		return reg_data;
	};
	
});
</script>

<style type="text/css">
#table_lap_igd_kunjungan {
	font-size: 12px;
}
</style>