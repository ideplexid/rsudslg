Penjelasan Laporan IGD

<ul>
	<li>
		<p align='justify'><strong>Rekap Data</strong> : Menyajikan data mentah dari Laporan IGD, semua data mentah dari inputan petugas IGD ada disini. berfungsi juga sebagai data pembanding jika ada ketidak sama'an</p>
	</li>
	<li>
		<p align='justify'><strong>Laporan Triage</strong> : Menyajikan semua laporan Triage dari Pasien. untuk Triage yang tidak diisi , dan pola kasus yang tidak diisi. akan miss dari laporan ini. tetapi data bisa dikoreksi dengan menggunakan data di rekap data</p>
	</li>
	<li>
		<p align='justify'><strong>Laporan Response Time</strong> : Menyajikan data Laporan IGD berdasarkan Response Time terhadap pasien.</p>
	</li>
	<li>
		<p align='justify'><strong>Laporan Kunjungan</strong> : Menyajikan Rekapitulasi Data pasien IGD beradasarkan Pola Kasus dan Kelanjutan-nya.</p>
	</li>
	<li>
		<p align='justify'><strong>Laporan Shift</strong> : Menyajikan data Laporan IGD per Shift.</p>
	</li>
</ul>


<p align='justify'>untuk <i>Laporan Rekap Data, Laporan Triage, Laporan Response Time,</i> dan <i> Laporan Kunjungan </i> memiliki jam dalam rentangnya. sehingga jika dipilih tanggal dari  
<strong>1 Januari 2016 00:00</strong>, dan tanggal sampai <strong>1 Februari 2016 00:00</strong>, maka data yang ditarik
adalah dari <strong>1 Januari 2016 00:00</strong> sampai <strong>31 Januari 2016 59:59</strong>, karena logika komputer mulai dari sama dengan hingga  kurang dari sampai <strong>(dari&lt;= tanggal &lt;sampai)</strong>, 
atau dengan kata lain <strong>1 Januari 2016 00:00 &lt;= tanggal &lt; 1 Februari 2016 00:00</strong>
untuk data <strong>1 Februari 2016 00:00</strong> tepat tidak diikutkan.</p>

<p align='justify'>pengecualian <i>Laporan Shift</i>, hanya memiliki tanggal saja. hal ini dikarenakan ada 
perbedaan pada pengambilan pengelompokan data. yaitu pasien antara jam 00:00 - 07:00
masuk sebagai pasien kemarinya (ikut shift malam kemarin). sehingga misalnya pasien yang 
masuk pada <strong>15 januari 2016 00:00 - 15 januari 2016 06:59</strong>, akan dianggap ikut 
laporan Shift Malam tangal <strong>14 Januari 2016</strong>. jadi untuk mencocokan data dengan laporan Rekap 
yang lain. misalnya di bagian Laporan Shift ditarik <strong>1 Januari 2015 - 1 Februari 2016</strong>.
maka di laporan lain agar datanya sama harus ditari dari <strong>1 Januari 2015 00:00 - 1 Februari 2016 07:00</strong>,
karena data untuk tanggal <strong>1 Februari 2016 00:00 - 1 Februari 2016 07:00</strong> ikut laporan di Shift Malam tanggal <strong>31 Januari 2016</strong>.
</p>