<?php

$header = array ();
$header [] = "No.";
$header [] = "Pasien";
$header [] = "NRM";
$header [] = "Noreg";
$header [] = "Masuk";
$header [] = "Ditangani";
$header [] = "Response Time";
$header [] = "Jenis Pasien";
$header [] = "Pola Kasus";
$header [] = "Triage";
$header [] = "MRS";
$header [] = "Menolak MRS";
$header [] = "Kiriman";
$header [] = "Alat Bantu Hidup";
$header [] = "Tuna Daksa";
$header [] = "Tuna Grahita";
$header [] = "Tuna Netra";
$header [] = "Tuna Rungu";
$header [] = "Tuna Wicara";
$header [] = "Tuna Laras";
$header [] = "VER";

$uitable = new Table ( $header, "", NULL, false );
$uitable->setName ( "lap_igd_response" );

$uitable->setFooterVisible ( false );

if (isset ( $_POST ['command'] )) {
	$adapter = new SimpleAdapter();
	$adapter->setUseNumber(true, "No.","back.");
	$adapter->add("Pasien", "nama_pasien");
	$adapter->add("NRM", "nrm_pasien","digit7");
	$adapter->add("Noreg", "noreg_pasien","digit8");
	$adapter->add("Masuk", "tanggal","date d M Y H:i");
	$adapter->add("Ditangani", "ditangani","date d M Y H:i");
	$adapter->add("Response Time", "response","back Menit");
	$adapter->add("Jenis Pasien", "carabayar","unslug");
	$adapter->add("Pola Kasus", "pola_kasus");
	$adapter->add("Triage", "triage");
	$adapter->add("MRS", "kelanjutan","trivial_1_<i class='fa fa-check'></i>_ ");
	$adapter->add("Menolak MRS", "menolak_mrs","trivial_1_<i class='fa fa-check'></i>_ ");
	$adapter->add("VER", "ver","trivial_1_<i class='fa fa-check'></i>_ ");
	$adapter->add("Alat Bantu Hidup", "alat_bantu_hidup","trivial_1_<i class='fa fa-check'></i>_ ");
	$adapter->add("Kiriman", "kiriman");
	$adapter->add("Tuna Daksa", "tunadaksa","trivial_1_<i class='fa fa-check'></i>_ ");
	$adapter->add("Tuna Rungu", "tunarungu","trivial_1_<i class='fa fa-check'></i>_ ");
	$adapter->add("Tuna Grahita", "tunagrahita","trivial_1_<i class='fa fa-check'></i>_ ");
	$adapter->add("Tuna Netra", "tunanetra","trivial_1_<i class='fa fa-check'></i>_ ");
	$adapter->add("Tuna Wicara", "tunawicara","trivial_1_<i class='fa fa-check'></i>_ ");
	$adapter->add("Tuna Laras", "tunalaras","trivial_1_<i class='fa fa-check'></i>_ ");
	
	
	$dbtable = new DBTable ( $db, "smis_mr_igd" );
	
	if (isset ( $_POST ['dari'] ) && isset ( $_POST ['sampai'] ) && $_POST ['dari'] != "" && $_POST ['sampai'] != "") {
		$dari = $_POST ['dari'];
		$sampai = $_POST ['sampai'];
		$dbtable->addCustomKriteria ( "'" . $dari . "'", "<=tanggal" );
		$dbtable->addCustomKriteria ( "'" . $sampai . "'", ">tanggal" );
		$dbtable->setShowAll ( true );
	}
	$dbtable->setOrder ( " tanggal ASC " );
	
	$dbres = new DBResponder ( $dbtable, $uitable, $adapter );
	$data = $dbres->command ( $_POST ['command'] );
	echo json_encode ( $data );
	return;
}

$uitable->addModal ( "dari", "datetime", "Dari", "" );
$uitable->addModal ( "sampai", "datetime", "Sampai", "" );
$uitable->addModal ( "ruangan", "hidden", "", "igd" );

$modal = $uitable->getModal ();
$form = $modal->getForm ();

$button = new Button ( "", "", "" );
$button->setIsButton ( Button::$ICONIC );
$button->setIcon ( "fa fa-refresh" );
$button->setAction ( "lap_igd_response.view()" );
$button->setClass("btn-primary");
$form->addElement ( "", $button );
$button = new Button ( "", "", "" );
$button->setIsButton ( Button::$ICONIC );
$button->setClass("btn-primary");
$button->setIcon ( "fa fa-print ");
$button->setAction ( "smis_print($('#print_table_lap_igd_response').html())" );
$form->addElement ( "", $button );
echo $form->getHtml ();

echo $uitable->getHtml ();
echo addJS ( "framework/smis/js/table_action.js" );
echo addJS ( "framework/bootstrap/js/bootstrap-datetimepicker.js" );
echo addCSS ( "framework/bootstrap/css/bootstrap-datetimepicker.css" );

?>
<script type="text/javascript">
var lap_igd_response;
//var employee;
$(document).ready(function(){
	$('.mydatetime').datetimepicker({minuteStep:1});
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
	var column=new Array();
	lap_igd_response=new TableAction("lap_igd_response","medical_record","lap_igd_response",column);
	lap_igd_response.getRegulerData=function(){
		var reg_data={	
				page:this.page,
				action:this.action,
				super_command:this.super_command,
				prototype_name:this.prototype_name,
				prototype_slug:this.prototype_slug,
				prototype_implement:this.prototype_implement,
				dari:$("#"+this.prefix+"_dari").val(),
				sampai:$("#"+this.prefix+"_sampai").val()
				};
		return reg_data;
	};
	
	
});
</script>

<style type="text/css">
#table_lap_igd_response {
	font-size: 12px;
}
</style>