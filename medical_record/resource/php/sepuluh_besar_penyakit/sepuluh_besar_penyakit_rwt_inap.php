<?php

global $db;
require_once "smis-base/smis-include-service-consumer.php";
$slug_igd = getSettings($db, "smis-mr-slug-igd", "");

$header = array("No.", "Diagnosa", "Kode ICD", "Laki-Laki","Perempuan", "Jumlah");

$uitable = new Table ( $header, "", NULL, false );
$uitable->setName ( "sepuluh_besar_penyakit_rwt_inap" );

$uitable->setHeaderVisible ( true );
$uitable->setFooterVisible( false );

if (isset ( $_POST ['command'] )) {
    $query = "
            SELECT
                diagnosa,
                nama_icd,
                kode_icd,
                SUM(IF(jk = '0', 1,0)) AS laki,
                SUM(IF(jk = '1', 1,0)) AS perempuan,
                COUNT(*) AS jumlah
            FROM 
                smis_mr_diagnosa
            WHERE
                prop != 'del'
                AND urji = '1'
                AND tanggal >= '".$_POST['dari']."'
                AND tanggal <= '".$_POST['sampai']."'
                AND ruangan != '".$slug_igd."'
            GROUP BY
                kode_icd
            ORDER BY 
                jumlah DESC
            ";
    $result = $db->get_result($query);
    
    require_once "medical_record/class/adapter/SepuluhBesarPenyakitRwtInapAdapter.php";
    $adapter = new SepuluhBesarPenyakitRwtInapAdapter();
    $ready = $adapter->getContent($result);
    
    if($_POST['command'] == 'list') {
        $uitable->setContent($ready);
        $content['list'] = $uitable->getBodyContent();
        $response = new ResponsePackage ();
        $response->setAlertVisible ( false );
        $response->setStatus ( ResponsePackage::$STATUS_OK );
        $response->setContent($content);
        echo json_encode ( $response->getPackage () );
        return;
                
        $dbres = new DBResponder ( $dbtable, $uitable, $adapter );
        $data = $dbres->command ( $_POST ['command'] );
        echo json_encode ( $data );
        return;
    } else if($_POST['command'] == 'excel') {
        require_once ("smis-libs-out/php-excel/PHPExcel.php");
        $file = new PHPExcel ();
        $file->getProperties ()->setCreator ( "Rumah Sakit Syamrabu" );
        $file->getProperties ()->setLastModifiedBy ( "Rumah Sakit Bagian Administrator" );
        $file->getProperties ()->setTitle ( "10 Besar Penyakit Rawat Inap" );
        $file->getProperties ()->setSubject ( "10 Besar Penyakit Rawat Inap" );
        $file->getProperties ()->setDescription ( "10 Besar Penyakit Rawat Inap Generated From system" );
        $file->getProperties ()->setKeywords ( "10 Besar Penyakit Rawat Inap" );
        $file->getProperties ()->setCategory ( "10 Besar Penyakit Rawat Inap" );
        
        $sheet = $file->getActiveSheet ();
        $i = 1;
        
        $objDrawing = new PHPExcel_Worksheet_Drawing();
        $objDrawing->setName('logo_img');
        $objDrawing->setDescription('logo_img');
        $objDrawing->setPath(getLogoNonInterlaced());
        $objDrawing->setCoordinates('A'.$i);
        $objDrawing->setOffsetX(20); 
        $objDrawing->setOffsetY(5);
        $objDrawing->setWidth(100); 
        $objDrawing->setHeight(100);
        $objDrawing->setWorksheet($sheet);
        $i++;
        
        $sheet->setCellValue("C".$i, getSettings($db,"smis_autonomous_title"));
        $sheet->getStyle("C".$i)->getFont()->setBold(true)->setSize(14);
        $i++;
        $sheet->setCellValue("C".$i, getSettings($db,"smis_autonomous_address"));
        $sheet->getStyle("C".$i)->getFont()->setBold(true)->setSize(14);
        $i++;
        $sheet->setCellValue("C".$i, getSettings($db,"smis_autonomous_contact"));
        $sheet->getStyle("C".$i)->getFont()->setBold(true)->setSize(14);
        $i += 3;
        $sheet->mergeCells("A".$i.":F".$i)->setCellValue("A".$i,"10 Besar Penyakit Rawat Inap");
        $sheet->getStyle("A".$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle("A".$i)->getFont()->setBold(true)->setSize(14);
        $i++;
        $sheet->mergeCells("A".$i.":F".$i)->setCellValue("A".$i, strtoupper(ArrayAdapter::dateFormat("date d M Y", $_POST['dari']))." - ".strtoupper(ArrayAdapter::dateFormat("date d M Y", $_POST['sampai'])));
        $sheet->getStyle("A".$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle("A".$i)->getFont()->setBold(true);
        $i += 3;
        
        $border_start = $i;
        $ips = $i + 1;
        $sheet->setCellValue("A".$i, "NO.");
        $sheet->setCellValue("B".$i, "DIAGNOSA");
        $sheet->setCellValue("C".$i, "KODE ICD");
        $sheet->setCellValue("D".$i, "LAKI-LAKI");
        $sheet->setCellValue("E".$i, "PEREMPUAN");
        $sheet->setCellValue("F".$i, "JUMLAH");
        $sheet->getStyle("A".$i.":F".$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle("A".$i.":F".$i)->getFont()->setBold(true);
        $i++;
        
        foreach($ready as $x) {
            $sheet->setCellValue("A".$i, $x["No."]);
            $sheet->getStyle("A".$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $sheet->setCellValue("B".$i, $x["Diagnosa"]);
            $sheet->setCellValue("C".$i, $x["Kode ICD"]);
            $sheet->setCellValue("D".$i, $x["Laki-Laki"]);
            $sheet->setCellValue("E".$i, $x["Perempuan"]);
            $sheet->setCellValue("F".$i, $x["Jumlah"]);
            $sheet->getStyle("C".$i.":F".$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $i++;
        }
        
        $border_end = $i - 1;
        $thin = array ();
        $thin['borders']=array();
        $thin['borders']['allborders']=array();
        $thin['borders']['allborders']['style']=PHPExcel_Style_Border::BORDER_THIN ;
        $sheet  ->getStyle ( "A".$border_start.":F".$border_end )->applyFromArray ($thin);
        
        $filename = "10 Besar Penyakit Rawat Inap ".strtoupper(ArrayAdapter::dateFormat("date d M Y", $_POST['dari']))." - ".strtoupper(ArrayAdapter::dateFormat("date d M Y", $_POST['sampai'])).".xls";
        
        header ( 'Content-Type: application/vnd.ms-excel' );
        header ( 'Content-Disposition: attachment;filename="'.$filename.'"' );
        header ( 'Cache-Control: max-age=0' );
        $writer = PHPExcel_IOFactory::createWriter ( $file, 'Excel5' );
        $writer->save ( 'php://output' );
		return;
    }
}

$uitable->addModal( "dari", "date", "Dari", "" );
$uitable->addModal( "sampai", "date", "Sampai", "" );

$modal = $uitable->getModal ();
$form=$modal->getForm();

$v = new Button("", "", "");
$v->addClass("btn-primary")
		->setIsButton(Button::$ICONIC)
		->setIcon("fa fa-refresh")
		->setAction("sepuluh_besar_penyakit_rwt_inap.view()");
$form->addElement("", $v);

$excel = new Button("", "", "");
$excel->addClass("btn-primary")
		->setIsButton(Button::$ICONIC)
		->setIcon("fa fa-file-excel-o")
		->setAction("sepuluh_besar_penyakit_rwt_inap.excel()");
$form->addElement("", $excel);

echo "<h2><strong>10 Besar Penyakit Rawat Inap</h2>";
echo $form->getHtml();
echo $uitable->getHtml ();

echo addJS ( "framework/smis/js/table_action.js" );
echo addJS ( "framework/bootstrap/js/bootstrap-datepicker.js" );
echo addCSS ( "framework/bootstrap/css/datepicker.css" );
echo addJS ( "medical_record/resource/js/sepuluh_besar_penyakit_rwt_inap.js", false );

?>