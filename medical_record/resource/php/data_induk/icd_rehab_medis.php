<?php
    global $db;
    require_once 'smis-libs-class/MasterTemplate.php';
    $icd_rehab_medik = new MasterTemplate(
        $db, 
        "smis_mr_icd_rehab_medis", 
        "medical_record", 
        "icd_rehab_medis"
    );
    $uitable = $icd_rehab_medik->getUItable();
    $uitable->setPrintButtonEnable(false);
    $uitable->setReloadButtonEnable(false);
    $uitable->addHeaderElement("Grup")
            ->addHeaderElement("Kode ICD")
            ->addHeaderElement("No DTD")
            ->addHeaderElement("Nama")
            ->addHeaderElement("Sebab");
    $uitable->addModal ( "id", "hidden", "", "" )
            ->addModal ( "nama", "text", "Nama", "" )
            ->addModal ( "grup", "text", "Grup", "" )
            ->addModal ( "icd", "text", "Kode ICD", "" )
            ->addModal ( "dtd", "text", "No DTD", "" )
            ->addModal ( "sebab", "text", "Sebab", "" );
    $adapter = $icd_rehab_medik->getAdapter();
    $adapter->add ( "Nama", "nama" )
            ->add ( "Kode ICD", "icd" )
            ->add ( "No DTD", "dtd" )
            ->add ( "Grup", "grup" )
            ->add ( "Sebab", "sebab" );
    $icd_rehab_medik->setModalTitle("Penyakit");
    $icd_rehab_medik->initialize();
?>