<?php

global $db;
require_once ("smis-base/smis-include-service-consumer.php");

$uitable = new Table ( array (
		'Ruangan',
        'Bed',
        'Kelompok Layanan',
        'Kelas',
        'Diakui'
), "Management Bed", NULL, true );
$uitable->setName ( "management_bed" );

if (isset ( $_POST ['command'] )) {
    if($_POST['command'] == 'get_bed_kelompok_layanan') {
        $ruangan = $_POST['ruangan'];
        
        $service = new ServiceConsumer($db, "get_bed_only", $data, $ruangan);
        $service->execute();
        $bed = $service->getContent();
        $list_bed = array();
        foreach($bed['list'] as $b) {
            $list_bed[]['bed'] = $b['nama'];
        }
        
        $kelompok_layanan = getSettings($db, "smis-rs-rl13-layanan-".$ruangan, "");
        
        $data['bed'] = $list_bed;
        $data['kelompok_layanan'] = $kelompok_layanan;
        
        echo json_encode($data);
        return;
    } else {
        $adapter = new SimpleAdapter ();
        $adapter->add ( "Ruangan", "ruangan", "unslug" );
        $adapter->add ( "Bed", "bed" );
        $adapter->add ( "Kelompok Layanan", "kelompok_layanan" );
        $adapter->add ( "Kelas", "kelas", "unslug" );
        $adapter->add ( "Diakui", "diakui", "trivial_1_Ya_Tidak" );
        $dbtable = new DBTable ( $db, "smis_mr_management_bed" );
        if(isset($_POST['filter_ruangan']) && $_POST['filter_ruangan'] != '') {
            $dbtable->addCustomKriteria("ruangan", "= '".$_POST['filter_ruangan']."'");
        }
        $dbres = new DBResponder ( $dbtable, $uitable, $adapter );
        $data = $dbres->command ( $_POST ['command'] );
        echo json_encode ( $data );
        return;
    }
}

loadClass ( "ServiceProviderList" );
$service = new ServiceProviderList ( $db, "push_antrian" );
$service->execute ();
$ruangan = $service->getContent ();

$bed = new OptionBuilder();
$bed->add("", "", "1");
$uitable->addModal("id", "hidden", "", "");
$uitable->addModal("ruangan", "select", "Ruangan", $ruangan, "n", null, false, null, false, null);
$uitable->addModal("bed", "select", "Bed", $bed->getContent(), "n", null, false, null, false, null);
$uitable->addModal("kelompok_layanan", "text", "Kelompok Layanan", "", "n", null, true, null, false, null);
$service = new ServiceConsumer ( $db, "get_kelas" );
$service->execute ();
$kelas = $service->getContent ();
$option_kelas = new OptionBuilder ();
foreach ( $kelas as $k ) {
    $nama = $k ['nama'];
    $slug = $k ['slug'];
    $option_kelas->add ( $nama, $slug );
}
$uitable->addModal("kelas", "select", "Kelas", $option_kelas->getContent(), "n", null, false, null, false, null);
$uitable->addModal("diakui", "checkbox", "Diakui", "", "y", null, false, null, false, null);

$modal = $uitable->getModal();
$modal->setTitle ( "Management Bed" );

$filter_ruangan = new OptionBuilder();
$filter_ruangan->add("", "", "1");
foreach($ruangan as $r) {
     $filter_ruangan->add($r['name'], $r['value'], "0");
}
$uitable->clearContent();
$uitable->addModal("filter_ruangan", "select", "Ruangan", $filter_ruangan->getContent());

$form = $uitable->getModal()->getForm();
		
$btn=new Button("", "", "");
$btn->setClass("btn-primary");
$btn->setIcon("fa fa-refresh");
$btn->setIsButton(Button::$ICONIC);
$btn->setAction("management_bed.view()");
$form->addElement("", $btn);

echo $form->getHtml();
echo $uitable->getHtml ();
echo $modal->getHtml ();

echo addJS ( "framework/smis/js/table_action.js" );

?>

<script type="text/javascript">
    var management_bed;
    
    $(document).ready(function() {
        
        
        var column = new Array("id", "ruangan", "bed", "kelompok_layanan", "kelas", "diakui");
		management_bed = new TableAction("management_bed", "medical_record", "management_bed",column);
        management_bed.getRegulerData=function(){
            var reg_data={	
                    page:this.page,
                    action:this.action,
                    super_command:this.super_command,
                    filter_ruangan:$("#management_bed_filter_ruangan").val()
                };
            return reg_data;
        };
        
        $("#management_bed_ruangan").change(function(){
            var data = management_bed.getRegulerData();
            data['command'] = 'get_bed_kelompok_layanan';
            data['ruangan'] = $("#management_bed_ruangan").val();
            showLoading();
            $.post(
                "",
                data,
                function(response) {
                    json = JSON.parse(response);
                    select = document.getElementById('management_bed_bed');
                    $("#management_bed_bed").empty();
                    for (var i = 0; i<json['bed'].length; i++){
                        var opt = document.createElement('option');
                        opt.value = json['bed'][i]['bed'];
                        opt.innerHTML = json['bed'][i]['bed'];
                        select.appendChild(opt);
                    }
                    $("#management_bed_kelompok_layanan").val(json['kelompok_layanan']);
                    dismissLoading();
                }
            );
        });
        management_bed.view();
    });
</script>