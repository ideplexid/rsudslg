<?php
	global $db;
	
	$table = new Table(
		array("No.", "Penyelenggara"),
		"",
		null,
		true
	);
	$table->setName("penyelenggara_rs");
	$table->setReloadButtonEnable(false);
	$table->setPrintButtonEnable(false);
	
	if (isset($_POST['command'])) {
		$adapter = new SimpleAdapter(true, "No.");
		$adapter->add("Penyelenggara", "penyelenggara");
		$dbtable = new DBTable($db, "smis_mr_penyelenggara_rs");
		$dbtable->setOrder(" id DESC ");
		$dbresponder = new DBResponder(
			$dbtable,
			$table,
			$adapter
		);
		$data = $dbresponder->command($_POST['command']);
		echo json_encode($data);
		return;
	}
	
	$modal = new Modal("penyelenggara_rs_add_form", "smis_form_container", "penyelenggara_rs");
	$modal->setTitle("Data Jenis RS");
	$id_hidden = new Hidden("penyelenggara_rs_id", "penyelenggara_rs_id", "");
	$modal->addElement("", $id_hidden);
	$penyelenggara_text = new Text("penyelenggara_rs_penyelenggara", "penyelenggara_rs_penyelenggara", "");
	$modal->addElement("Penyelenggara", $penyelenggara_text);
	$save_button = new Button("", "", "Simpan");
	$save_button->setClass("btn-success");
	$save_button->setAction("penyelenggara_rs.save()");
	$save_button->setIcon("fa fa-floppy-o");
	$save_button->setIsButton(Button::$ICONIC);
	$modal->addFooter($save_button);
	
	echo $modal->getHtml();
	echo $table->getHtml();
	echo addJS("framework/smis/js/table_action.js");
?>
<script type="text/javascript">
	var penyelenggara_rs;
	$(document).ready(function() {
		var columns = new Array('id', 'penyelenggara');
		penyelenggara_rs = new TableAction(
			"penyelenggara_rs",
			"medical_record",
			"penyelenggara_rs",
			columns
		);
		penyelenggara_rs.view();
	});
</script>