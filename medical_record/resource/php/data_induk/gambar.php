<?php
/**
 * this source code used for handling 
 * the database of ICD Diagnostic
 * 
 * @author 		: Nurul Huda
 * @license 	: Apache v3
 * @version 	: 1.0.0
 * @since 		: 24 Sept 2016
 * @copyright 	: goblooge@gmail.com
 * @database	: smis_mr_icd
 * 
 * */
 
global $db;
require_once 'smis-libs-class/MasterTemplate.php';
$ictindakan=new MasterTemplate($db, "smis_mr_gambar", "medical_record", "gambar");
$uitable=$ictindakan->getUItable();
$uitable->setPrintButtonEnable(false);
$uitable->setReloadButtonEnable(false);
$uitable->addHeaderElement("Sebab");
$uitable->addModal ( "id", "hidden", "", "" )
		->addModal ( "nama", "text", "Nama", "" )
		->addModal ( "gambar", "text", "Gambar", "smis-upload/".getSettings($db,"smis_autonomous_logo","") )
		->addModal ( "stroke", "draw-component-gambar_gambar", "Stroke", "" );
$adapter=$ictindakan->getAdapter();
$adapter->add ( "Nama", "nama" );
$ictindakan->setModalTitle("Diagnosa");
$ictindakan->initialize();
?>