<?php 
/**
 * this source code used for handling 
 * the database of ICD Procedure
 * 
 * @author 		: Nurul Huda
 * @license 	: Apache v3
 * @version 	: 1.0.0
 * @since 		: 24 Sept 2016
 * @copyright 	: goblooge@gmail.com
 * @database	: smis_mr_icdtindakan
 * 
 * */
 
global $db;
require_once 'smis-libs-class/MasterTemplate.php';
$ictindakan=new MasterTemplate($db, "smis_mr_icdtindakan", "medical_record", "icdtindakan");
$uitable=$ictindakan->getUItable();
$uitable->setPrintButtonEnable(false);
$uitable->setReloadButtonEnable(false);
$uitable->addHeaderElement("Nama")
		->addHeaderElement("Kode ICD")
		->addHeaderElement("DTD")
		->addHeaderElement("Grup");
$uitable->addModal("id", "hidden", "", "")
		->addModal("nama", "text", "Nama", "")
		->addModal("icd", "text", "ICD", "")
		->addModal("grup", "text", "Grup", "")
		->addModal("dtd","text", "DTD", "");
$adapter=$ictindakan->getAdapter();
$adapter->add("Nama", "nama")
		->add("DTD","dtd")
		->add("Kode ICD","icd")
		->add("Grup", "grup");
$ictindakan->setModalTitle("Tindakan");		
$ictindakan->initialize();

?>