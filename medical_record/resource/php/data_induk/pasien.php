<?php
global $db;
require_once("smis-base/smis-include-service-consumer.php");
require_once 'smis-libs-class/MasterTemplate.php';
require_once 'smis-libs-class/MasterServiceTemplate.php';
require_once 'smis-libs-class/MasterSlaveServiceTemplate.php';

$label=new Button("","","");
$label->addClass("btn-primary");
$label->setIsButton(Button::$ICONIC);
$label->setIcon("fa fa-trello");

$header=array ("No.","NRM",'Nama',"Tgl Lahir","Alamat","Kelurahan","Kecamatan","Kabupaten","Provinsi",'Tanggal');
$pasien=new MasterSlaveServiceTemplate($db, "get_patient", "medical_record", "pasien");
$pasien->setEntity("registration");
$pasien->setDateEnable(true);
if( isset($_POST['command']) && $_POST['command']=="list" ){	
	$pasien->getServiceResponder()->addData("nama_pasien",$_POST['nama_pasien']);	
	$pasien->getServiceResponder()->addData("nrm_pasien",$_POST['nrm_pasien']);			
}
$adapt=new SummaryAdapter();
$adapt	->setUseNumber(true, "No.","back.")
		->add("Tanggal", "tanggal","date d M Y")
		->add("NRM", "id","only-digit8")
		->add("Nama", "nama")
		->add("Tgl Lahir","tgl_lahir","date d M Y")
		->add("Alamat","alamat")
		->add("Kelurahan","nama_kelurahan")
		->add("Kecamatan","nama_kecamatan")
		->add("Kabupaten", "nama_kabupaten")
		->add("Provinsi", "nama_provinsi");
$adapt->addSummary("Tagihan","total_tagihan","money Rp.");
$adapt->addFixValue("Tanggal","<strong>Total</strong>");
$pasien->setAdapter($adapt);
$pasien ->getUItable()
		->addContentButton("print_label",$label)
		->setReloadButtonEnable(false)
		->setDelButtonEnable(false)
		->setAddButtonEnable(false)
		->setEditButtonEnable(false)
		->setHeader($header);
		
if(!isset($_POST['command'])){;
	$pasien ->getUItable()
			->addModal("nrm_pasien", "text", "NRM", "","y")
			->addModal("nama_pasien", "text", "Nama Pasien", "","y");
	$btn=new Button("","","");
	$btn->addClass("btn-primary");
	$btn->setIcon("fa fa-refresh");
	$btn->setAction("pasien.view()");
	$btn->setIsButton(Button::$ICONIC);
	$pasien	->getForm()
			->addElement("",$btn);
	$pasien->getUItable()->clearContent();
	$pasien->addViewData("nama_pasien","nama_pasien");
	$pasien->addViewData("nrm_pasien","nrm_pasien");
}
$pasien->addResouce("js","medical_record/resource/js/pasien.js","after");
$pasien->initialize();
?>

