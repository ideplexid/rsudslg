<?php 
	global $db;
	require_once 'smis-libs-class/MasterTemplate.php';
	$jenis_kegiatan_rl52 = new MasterTemplate($db, "smis_mr_jenis_kegiatan_rl52", "medical_record", "jenis_kegiatan_rl52");
	$uitable = $jenis_kegiatan_rl52->getUItable();
	$uitable->setPrintButtonEnable(false);
	$uitable->setReloadButtonEnable(false);
	$uitable->addHeaderElement("ID");
	$uitable->addHeaderElement("Jenis Kegiatan");
	$uitable->addModal("id", "hidden", "", "")
			->addModal("nama", "text", "Jns. Kegiatan", "");
	$adapter = $jenis_kegiatan_rl52->getAdapter();
	$adapter->add("ID", "id");
	$adapter->add("Jenis Kegiatan", "nama");
	$jenis_kegiatan_rl52->setModalTitle("Data Jenis Kegiatan");
	$jenis_kegiatan_rl52->initialize();
?>