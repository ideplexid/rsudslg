<?php
	global $db;
	
	$table = new Table(
		array("No.", "Kepemilikan"),
		"",
		null,
		true
	);
	$table->setName("kepemilikan_rs");
	$table->setReloadButtonEnable(false);
	$table->setPrintButtonEnable(false);
	
	if (isset($_POST['command'])) {
		$adapter = new SimpleAdapter(true, "No.");
		$adapter->add("Kepemilikan", "kepemilikan");
		$dbtable = new DBTable($db, "smis_mr_kepemilikan_rs");
		$dbtable->setOrder(" id DESC ");
		$dbresponder = new DBResponder(
			$dbtable,
			$table,
			$adapter
		);
		$data = $dbresponder->command($_POST['command']);
		echo json_encode($data);
		return;
	}
	
	$modal = new Modal("kepemilikan_rs_add_form", "smis_form_container", "kepemilikan_rs");
	$modal->setTitle("Data Jenis RS");
	$id_hidden = new Hidden("kepemilikan_rs_id", "kepemilikan_rs_id", "");
	$modal->addElement("", $id_hidden);
	$kepemilikan_text = new Text("kepemilikan_rs_kepemilikan", "kepemilikan_rs_kepemilikan", "");
	$modal->addElement("Kepemilikan", $kepemilikan_text);
	$save_button = new Button("", "", "Simpan");
	$save_button->setClass("btn-success");
	$save_button->setAction("kepemilikan_rs.save()");
	$save_button->setIcon("fa fa-floppy-o");
	$save_button->setIsButton(Button::$ICONIC);
	$modal->addFooter($save_button);
	
	echo $modal->getHtml();
	echo $table->getHtml();
	echo addJS("framework/smis/js/table_action.js");
?>
<script type="text/javascript">
	var kepemilikan_rs;
	$(document).ready(function() {
		var columns = new Array('id', 'kepemilikan');
		kepemilikan_rs = new TableAction(
			"kepemilikan_rs",
			"medical_record",
			"kepemilikan_rs",
			columns
		);
		kepemilikan_rs.view();
	});
</script>