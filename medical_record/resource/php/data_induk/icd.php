<?php
/**
 * this source code used for handling 
 * the database of ICD Diagnostic
 * 
 * @author 		: Nurul Huda
 * @license 	: Apache v3
 * @version 	: 1.0.0
 * @since 		: 24 Sept 2016
 * @copyright 	: goblooge@gmail.com
 * @database	: smis_mr_icd
 * 
 * */
 
global $db;
require_once 'smis-libs-class/MasterTemplate.php';
$ictindakan=new MasterTemplate($db, "smis_mr_icd", "medical_record", "icd");
$uitable=$ictindakan->getUItable();
$uitable->setPrintButtonEnable(false);
$uitable->setReloadButtonEnable(false);
$uitable->addHeaderElement("Grup")
		->addHeaderElement("Kode ICD")
		->addHeaderElement("No DTD")
		->addHeaderElement("Nama")
		->addHeaderElement("Sebab");
$uitable->addModal ( "id", "hidden", "", "" )
		->addModal ( "nama", "text", "Nama", "" )
		->addModal ( "grup", "text", "Grup", "" )
		->addModal ( "icd", "text", "Kode ICD", "" )
		->addModal ( "dtd", "text", "No DTD", "" )
		->addModal ( "sebab", "text", "Sebab", "" );
$adapter=$ictindakan->getAdapter();
$adapter->add ( "Nama", "nama" )
		->add ( "Kode ICD", "icd" )
		->add ( "No DTD", "dtd" )
		->add ( "Grup", "grup" )
		->add ( "Sebab", "sebab" );
$ictindakan->setModalTitle("Penyakit");
$ictindakan->initialize();
?>