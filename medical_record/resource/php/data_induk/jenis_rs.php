<?php
	global $db;
	
	$table = new Table(
		array("No.", "Jenis RS", "Uraian"),
		"",
		null,
		true
	);
	$table->setName("jenis_rs");
	$table->setReloadButtonEnable(false);
	$table->setPrintButtonEnable(false);
	
	if (isset($_POST['command'])) {
		$adapter = new SimpleAdapter(true, "No.");
		$adapter->add("Jenis RS", "jenis_rs");
		$adapter->add("Uraian", "uraian");
		$dbtable = new DBTable($db, "smis_mr_jenis_rs");
		$dbtable->setOrder(" id DESC ");
		$dbresponder = new DBResponder(
			$dbtable,
			$table,
			$adapter
		);
		$data = $dbresponder->command($_POST['command']);
		echo json_encode($data);
		return;
	}
	
	$modal = new Modal("jenis_rs_add_form", "smis_form_container", "jenis_rs");
	$modal->setTitle("Data Jenis RS");
	$id_hidden = new Hidden("jenis_rs_id", "jenis_rs_id", "");
	$modal->addElement("", $id_hidden);
	$jenis_rs_text = new Text("jenis_rs_jenis_rs", "jenis_rs_jenis_rs", "");
	$modal->addElement("Jenis RS", $jenis_rs_text);
	$uraian_textarea = new TextArea("jenis_rs_uraian", "jenis_rs_uraian", "");
	$uraian_textarea->setLine(2);
	$modal->addElement("Uraian", $uraian_textarea);
	$save_button = new Button("", "", "Simpan");
	$save_button->setClass("btn-success");
	$save_button->setAction("jenis_rs.save()");
	$save_button->setIcon("fa fa-floppy-o");
	$save_button->setIsButton(Button::$ICONIC);
	$modal->addFooter($save_button);
	
	echo $modal->getHtml();
	echo $table->getHtml();
	echo addJS("framework/smis/js/table_action.js");
?>
<script type="text/javascript">
	var jenis_rs;
	$(document).ready(function() {
		var columns = new Array('id', 'jenis_rs', 'uraian');
		jenis_rs = new TableAction(
			"jenis_rs",
			"medical_record",
			"jenis_rs",
			columns
		);
		jenis_rs.view();
	});
</script>