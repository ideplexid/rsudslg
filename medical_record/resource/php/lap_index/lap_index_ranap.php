<?php 
global $db;
global $user;
require_once 'smis-base/smis-include-service-consumer.php';
if(isset($_POST['super_command']) && $_POST['super_command']=="total") {
	require_once 'smis-libs-class/ServiceProviderList.php';
	$dbtable=new DBTable($db, "smist_mr_lap_ranap");
	//$dbtable->truncate();
    $query = "DELETE FROM smist_mr_lap_ranap";
    $db->get_var($query);
	$serv=new ServiceConsumer($db, "get_patient_pulang");
	$serv->addData("command", "count");
	$serv->addData("dari", $_POST['dari']);
	$serv->addData("sampai", $_POST['sampai']);	
	$serv->execute();
	$data=$serv->getContent();
	$res=new ResponsePackage();
	$res->setStatus(ResponsePackage::$STATUS_OK);
	$res->setContent($data);
	echo json_encode($res->getPackage());
	return;
}

if(isset($_POST['super_command']) && $_POST['super_command']=="limit") {
	$data=array();
	$response=new ServiceConsumer($db, "get_patient_pulang");
	$response->addData("command", "list");
	$response->addData("dari", $_POST['dari']);
	$response->addData("sampai", $_POST['sampai']);
	$response->addData("halaman", $_POST['halaman']);	
	$response->setMode(ServiceConsumer::$SINGLE_MODE);
	$response->execute();
	$list=$response->getContent();
	$dbtable=new DBTable($db, "smist_mr_lap_ranap");
	$diagnosa=new DBTable($db,"smis_mr_diagnosa");
	$operasi=new DBTable($db,"smis_mr_operasi");
	
	//$ok=new DBTable($db,"smis_rwt_ok_ruang_operasi");
	
	foreach($list as $x){		
		$d['nrm']=$x['nrm'];
		$d['nama_pasien']=$x['nama_pasien'];
		$d['noreg']=$x['id'];
		$d['tgl_masuk']=$x['tanggal'];
		$d['dokter_pj']=$x['nama_dokter'];
		$d['tgl_pulang']=$x['tanggal_pulang'];
		$d['ruang']=$x['kamar_inap'];
        $d['carabayar']=$x['carabayar'];
        $d['carapulang']=$x['carapulang'];
        $d['n_perusahaan']=$x['n_perusahaan'];
        $d['n_asuransi']=$x['nama_asuransi'];
		$d['kunjungan']=$x['barulama']=="0"?"Baru":"Lama";
        $d['umur']=$x['umur'];
        $d['jk']=$x['kelamin'];
        /*checking diagnosa awal*/
        $query="SELECT * FROM smis_mr_diagnosa WHERE noreg_pasien='".$x['id']."' AND prop!='del' ORDER BY id ASC";
		$diag=$db->get_row($query);
        if($diag!=NULL){
			$d['diagnosa']=$diag->diagnosa;
			$d['kode_diagnosa']=$diag->kode_icd;
            $d['kasus']=$diag->kasus;
            
            /*cheking diagnosa akhir*/
            $query="SELECT * FROM smis_mr_diagnosa WHERE noreg_pasien='".$x['id']."' AND prop!='del' ORDER BY id DESC";
            $diag2=$db->get_row($query);
            if($diag2!=NULL && $diag->id!=$diag2->id){
                $d['diagnosa_akhir']=$diag2->diagnosa;
                $d['kode_diagnosa_akhir']=$diag2->kode_icd;
            } else {
                $d['diagnosa_akhir']=$diag->diagnosa;
                $d['kode_diagnosa_akhir']=$diag->kode_icd;
            }
		}
        
		$op=$operasi->select(array("noreg_pasien"=>$x['id']));
		if($op!=NULL){
			$d['dokter_bedah']=$op->dokter_operator;
			$d['dokter_anastesi']=$op->dokter_anastesi;
			$d['tgl_tindakan']=$op->tanggal_mulai;
			$d['tindakan']=$op->tindakan;
			$d['bedah']="1";
		}		
		if($op==NULL || $op->dokter_operator=="" || $op->dokter_anastesi=="" || $op->tanggal_mulai=="0000-00-00 00:00:00"){
			$o=new ServiceConsumer($db, "get_ok",NULL,$_POST['ruang_ok']);
			$o->addData("noreg_pasien", $x['id']);
			$o->setMode(ServiceConsumer::$SINGLE_MODE);
			$o->execute();
			$okp=$o->getContent();
			if($okp!=NULL){
				$d['dokter_bedah']=$okp['nama_operator_satu'];
				$d['dokter_anastesi']=$okp['nama_anastesi'];
				$d['tgl_tindakan']=$okp['waktu'];
				$d['bedah']="1";
			}
		}
		$dbtable->insert($d);		
	}
	$res=new ResponsePackage();
	$res->setStatus(ResponsePackage::$STATUS_OK);
	$res->setContent($list);
	echo json_encode($res->getPackage());
	return;
}

$header=array(	"No.","NRM","Nama","Umur","Ruang","Masuk","Pulang",
				"ICD10 Awal","Diagnosa Awal","ICD10 Akhir","Diagnosa Akhir","Dokter DPJP","Kasus Bedah","ICD9","Tindakan",
				"Tanggal Tindakan","Dokter Bedah","Dokter Anastesi");
$uitable=new Table($header);
$uitable->setName("lap_index_ranap")
		->setActionEnable(false);

if(isset($_POST['command']) && $_POST['command']=="excel"){
	require_once "medical_record/snippet/create_excel_lap_index_ranap.php";
    return;
}

if(isset($_POST['command']) && $_POST['command']!=""){
	$dbtable=new DBTable($db, "smist_mr_lap_ranap");
	$dbtable->setOrder(" tgl_pulang ASC");
	
	$adapter=new SimpleAdapter();
	$adapter->setUseNumber(true, "No.","back.");
	$adapter->add("NRM", "nrm","digit8");
	$adapter->add("Nama", "nama_pasien");
    $adapter->add("Umur", "umur");
	$adapter->add("Ruang", "ruang","unslug");
	$adapter->add("ICD10 Awal", "kode_diagnosa");
	$adapter->add("Diagnosa Awal", "diagnosa");
	$adapter->add("ICD10 Akhir", "kode_diagnosa_akhir");
	$adapter->add("Diagnosa Akhir", "diagnosa_akhir");
	$adapter->add("Dokter DPJP", "dokter_pj");
	$adapter->add("Kasus Bedah", "bedah","trivial_1_<i class='fa fa-check'></i>_");
	$adapter->add("ICD9", "kode_tindakan");
	$adapter->add("Tindakan", "tindakan");
	$adapter->add("Tanggal Tindakan", "tgl_tindakan","date d M Y");
	$adapter->add("Dokter Bedah", "dokter_bedah");
	$adapter->add("Dokter Anastesi", "dokter_anastesi");
	$adapter->add("Masuk", "tgl_masuk","date d M Y H:i");
	$adapter->add("Pulang", "tgl_pulang","date d M Y H:i");
	
	$dbres=new DBResponder($dbtable, $uitable, $adapter);
	$hasil=$dbres->command($_POST['command']);
	echo json_encode($hasil);
	return;
}


$uitable->clearContent();
$uitable->addModal("dari", "date", "Dari", "")
		->addModal("sampai", "date", "Sampai", "")
		->addModal("ruang_ok", "text", "Ruang Operasi", getSettings($db,"mr-slug-ruang-operasi",""),"",null,true);
$action=new Button("","","View");
$action->setClass("btn-primary")
	   ->setIsButton(Button::$ICONIC)
	   ->setIcon("icon-white fa fa-circle-o-notch")
	   ->setAction("lap_index_ranap.rekaptotal()");
$view=new Button("","","View");
$view->setClass("btn-primary")
		->setIsButton(Button::$ICONIC)
		->setIcon("fa fa-refresh")
		->setAction("lap_index_ranap.view()");
$excel=new Button("","","Excel");
$excel->setClass("btn-primary")
	->setIsButton(Button::$ICONIC)
	->setIcon("fa fa-file-excel-o")
	->setAction("lap_index_ranap.excel()");

$btng=new ButtonGroup("");
$btng->addButton($action);
$btng->addButton($view);
$btng->addButton($excel);

$form=$uitable
	  ->getModal()
	  ->setTitle("Medical Record")
	  ->getForm()
	  ->addElement("",$btng);

$close=new Button("", "", "Batal");
$close	->addClass("btn-primary")
		->setIsButton(Button::$ICONIC_TEXT)
		->setIcon("fa fa-close")
		->setAction("lap_index_ranap.batal()");

$load=new LoadingBar("rekap_lap_index_ranap_bar", "");
$modal=new Modal("rekap_lap_index_ranap_modal", "", "Processing...");
$modal	->addHTML($load->getHtml(),"after")
		->addFooter($close);

echo $form->getHtml();
echo "<div class='clear'></div>";
echo "<div id='result_lap_index_ranap'>".$uitable->getHtml()."</div>";
echo $modal->getHtml();
echo addJS("framework/bootstrap/js/bootstrap-datepicker.js");
echo addJS("framework/smis/js/table_action.js");
echo addCSS("framework/bootstrap/css/datepicker.css");
echo addJS ( "base-js/smis-base-loading.js");
?>

<script type="text/javascript">
	var lap_index_ranap;
	var lap_index_ranap_karyawan;
	var lap_index_ranap_data;
	var IS_lap_index_ranap_RUNNING;
	$(document).ready(function(){
		$('.mydate').datepicker();
		lap_index_ranap=new TableAction("lap_index_ranap","medical_record","lap_index_ranap",new Array());
		lap_index_ranap.addRegulerData=function(data){
			data['dari']=$("#lap_index_ranap_dari").val();
			data['sampai']=$("#lap_index_ranap_sampai").val();
			data['ruang_ok']=$("#lap_index_ranap_ruang_ok").val();			
			$("#dari_table_lap_index_ranap").html(getFormattedDate(data['dari']));
			$("#sampai_table_lap_index_ranap").html(getFormattedDate(data['sampai']));			
			return data;
		};

		lap_index_ranap.batal=function(){
			IS_lap_index_ranap_RUNNING=false;
			$("#rekap_lap_index_ranap_modal").modal("hide");
		};
		
		lap_index_ranap.afterview=function(json){
			if(json!=null){
				$("#kode_table_lap_index_ranap").html(json.nomor);
				$("#waktu_table_lap_index_ranap").html(json.waktu);
				lap_index_ranap_data=json;
			}
		};

		lap_index_ranap.rekaptotal=function(){
			if(IS_lap_index_ranap_RUNNING) return;
			$("#rekap_lap_index_ranap_bar").sload("true","Fetching total data",0);
			$("#rekap_lap_index_ranap_modal").modal("show");
			IS_lap_index_ranap_RUNNING=true;
			var d=this.getRegulerData();
			d['super_command']="total";
			$.post("",d,function(res){
				var all=getContent(res);
				if(all!=null) {
					var total=Number(all);
					lap_index_ranap.rekaploop(0,total);
				} else {
					$("#rekap_lap_index_ranap_modal").modal("hide");
					IS_lap_index_ranap_RUNNING=false;
				}
			});
		};

		lap_index_ranap.excel=function(){
			var d=this.getRegulerData();
			d['command']="excel";
			download(d);
		};

		lap_index_ranap.rekaploop=function(current,total){
			if(current>=total || !IS_lap_index_ranap_RUNNING) {
				$("#rekap_lap_index_ranap_modal").modal("hide");
				IS_lap_index_ranap_RUNNING=false;
				lap_index_ranap.view();
				return;
			}
			var d=this.getRegulerData();
			d['super_command']="limit";
			d['halaman']=current;
			$.post("",d,function(res){
				var ct=getContent(res);
				var u=ct[0]['nama_pasien']+"  "+ct[0]['nrm'];
				$("#rekap_lap_index_ranap_bar").sload("true"," Processing - "+u+"... [ "+(current+1)+" / "+total+" ] ",(current*100/total));
				setTimeout(function(){lap_index_ranap.rekaploop(++current,total)},300);
			});
		};
				
	});
</script>