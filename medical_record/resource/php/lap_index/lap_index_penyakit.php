<?php 
global $db;
global $user;
require_once 'smis-base/smis-include-service-consumer.php';
require_once 'smis-libs-hrd/EmployeeResponder.php';

$dkadapter = new SimpleAdapter ();
$dkadapter->add ( "Nama", "nama" );
$dkadapter->add ( "Kode ICD", "icd" );
$dkadapter->add ( "Keterangan", "sebab" );
$header=array ('Nama','Kode ICD',"Keterangan" );
$dktable = new Table ( $header);
$dktable->setName ( "penyakit_lap_index_penyakit" );
$dktable->setModel ( Table::$SELECT );
$dbtable=new DBTable($db,"smis_mr_icd"); 
$penyakit = new DBResponder( $dbtable, $dktable, $dkadapter,"penyakit");
$super = new SuperCommand ();
$super->addResponder ( "penyakit_lap_index_penyakit", $penyakit );
$init = $super->initialize ();
if ($init != null) {
	echo $init;
	return;
}


if(isset($_POST['super_command']) && $_POST['super_command']=="total") {
	require_once 'smis-libs-class/ServiceProviderList.php';
	$dbtable=new DBTable($db, "smis_mr_diagnosa");
	$dbtable->addCustomKriteria(NULL," tanggal >='".$_POST['dari']."' ");
	$dbtable->addCustomKriteria(NULL," tanggal <='".$_POST['sampai']."' ");
	$dbtable->addCustomKriteria(NULL," ( carapulang ='' OR gol_umur='TGL LAHIR TDK VALID' OR gol_umur='' ) ");
	$q=$dbtable->getQueryCount("");
	$total=$db->get_var($q);
	$res=new ResponsePackage();
	$res->setStatus(ResponsePackage::$STATUS_OK);
	$res->setContent($total);
	echo json_encode($res->getPackage());
	return;
}


if(isset($_POST['super_command']) && $_POST['super_command']=="limit") {
	$data=array();
	
	$dbtable=new DBTable($db, "smis_mr_diagnosa");
	$dbtable->addCustomKriteria(NULL," tanggal >='".$_POST['dari']."' ");
	$dbtable->addCustomKriteria(NULL," tanggal <='".$_POST['sampai']."' ");
	$dbtable->addCustomKriteria(NULL," ( carapulang ='' OR gol_umur='TGL LAHIR TDK VALID' OR gol_umur='' ) ");
	if($_POST['urji']!="-1"){
		$dbtable->addCustomKriteria(NULL," urji ='".$_POST['urji']."' ");
	}
	$dbtable->setMaximum(1);
	$data=$dbtable->view("",$_POST['halaman']);
	$list=$data['data'];
    loadLibrary("smis-libs-function-time");
	loadLibrary("smis-libs-function-medical");
	foreach($list as $x){		
		$response=new ServiceConsumer($db, "get_registered",NULL,"registration");
		$response->addData("command", "select");
		$response->addData("id", $x->noreg_pasien);
		$response->execute();
		$one=$response->getContent();
		if($one==NULL)
			continue;
		$up=array();
		$up['tgl_masuk']=$one['tanggal'];
		$up['tgl_keluar']=$one['tanggal_pulang'];
		$up['carapulang']=$one['carapulang'];	
		$up['carabayar']=$one['carabayar'];	
		$up['gol_umur']=medical_gol_umur_index_penyakit($up['tgl_masuk'],$one['tgl_lahir']);	
		$up['lama_dirawat']=day_diff_only($up['tgl_keluar'],$up['tgl_masuk']);
		$dbtable->update($up,array("id"=>$x->id));
	}
	$res=new ResponsePackage();
	$res->setStatus(ResponsePackage::$STATUS_OK);
	$res->setContent($list);
	echo json_encode($res->getPackage());
	return;
}
	
$header=array(	"No.","Tanggal", "Pasien", "NRM","Propinsi","Kabupaten","Kecamatan","Kelurahan","Ruangan","Masuk",
				"Keluar","Lama Dirawat","L","P",
				"0 - 28 HR","28 HR - 1 TH","1-4 TH",
				"5-14 TH","15-24 TH","25-44 TH","45-59 TH","60-64 TH","65 TH+",
				"Diagnosa Sekunder","Kelas",
				"Dipulangkan Mati <=48 Jam","Dipulangkan Mati >48 Jam",
				"Pulang Paksa","Kabur",
				"Rujuk RS Lain","Dikembalikan ke Perujuk",
				"Dipulangkan Hidup");
$uitable=new Table($header);
$uitable->setName("lap_index_penyakit")
		->setActionEnable(false)
		->setFooterVisible(false);

if(isset($_POST['command']) && $_POST['command']=="excel"){
	require_once "medical_record/snippet/create_excel_lap_index_penyakit.php";
	return;
}



if(isset($_POST['command']) && $_POST['command']!=""){	
	$query_view="
			SELECT nama_icd, tanggal, nama_pasien, nrm_pasien, propinsi, kabupaten, kecamatan, kelurahan, tgl_masuk,tgl_keluar,lama_dirawat,
			if(gol_umur='0 - 28 HR',1,0) as '28hr',
			if(gol_umur='28 HR - 1 TH',1,0) as '1th',
			if(gol_umur='1-4 TH',1,0) as '4th',
			if(gol_umur='5-14 TH',1,0) as '14th',
			if(gol_umur='15-24 TH',1,0) as '24th',
			if(gol_umur='25-44 TH',1,0) as '44th',
			if(gol_umur='45-59 TH',1,0) as '59th',
			if(gol_umur='60-64 TH',1,0) as '64th',
			if(gol_umur='65 TH+',1,0) as '65th',
			if(jk=0,1,0) as laki,
			if(jk=1,1,0) as pr,
			keterangan,
			ruangan as ruangan,
			ifnull(kelas,ruangan) as kelas,
			if(carapulang='Dipulangkan Mati <=48 Jam' OR carapulang='Dipulangkan Mati < 1 Jam Post Operasi',1,0) as mk48,
			if(carapulang='Dipulangkan Mati >48 Jam',1,0) as ml48,
			if(carapulang='Pulang Paksa',1,0) as paksa,
			if(carapulang='Kabur',1,0) as kabur,
			if(carapulang='Rujuk RS Lain',1,0) as rslain,
			if(carapulang='Dikembalikan ke Perujuk',1,0) as perujuk,
			if(carapulang='Dipulangkan Hidup',1,0) as hidup
			FROM smis_mr_diagnosa			
	";
	$query_count="SELECT COUNT(*) FROM smis_mr_diagnosa";
	
	$dbtable=new DBTable($db, "smis_mr_diagnosa");
	$dbtable->setPreferredQuery(true,$query_view,$query_count);
	$dbtable->setUseWhereforView(true);
	$dbtable->setOrder(" tanggal ASC");	
	$dbtable->addCustomKriteria(" nama_icd "," like '".$_POST['nama_penyakit']."%' ");
    $dbtable->addCustomKriteria(NULL," tanggal >='".$_POST['dari']."' ");
	$dbtable->addCustomKriteria(NULL," tanggal <'".$_POST['sampai']."' ");
	if($_POST['urji']!="-1"){
		$dbtable->addCustomKriteria(" urji "," = '".$_POST['urji']."' ");
	}
	$adapter=new SimpleAdapter();
	$adapter->setUseNumber(true, "No.","back.");
	$adapter->add("Tanggal", "tanggal","date d M Y");
    $adapter->add("Pasien", "nama_pasien");
	$adapter->add("NRM", "nrm_pasien","only-digit8");
	$adapter->add("Propinsi", "propinsi");
	$adapter->add("Kabupaten", "kabupaten");
	$adapter->add("Kecamatan", "kecamatan");
	$adapter->add("Kelurahan", "kelurahan");
	$adapter->add("Ruangan", "ruangan","unslug");
	$adapter->add("Masuk", "tgl_masuk","date d M Y");
	$adapter->add("Keluar", "tgl_keluar","date d M Y");
	$adapter->add("Lama Dirawat", "lama_dirawat","back Hari");	
	$adapter->add("L", "laki","trivial_0__<i class='fa fa-check'></>");
	$adapter->add("P", "pr","trivial_0__<i class='fa fa-check'></>");	
	$adapter->add("0 - 28 HR", "28hr","trivial_0__<i class='fa fa-check'></>");
	$adapter->add("28 HR - 1 TH", "1th","trivial_0__<i class='fa fa-check'></>");
	$adapter->add("1-4 TH", "4th","trivial_0__<i class='fa fa-check'></>");
	$adapter->add("5-14 TH", "14th","trivial_0__<i class='fa fa-check'></>");
	$adapter->add("15-24 TH", "24th","trivial_0__<i class='fa fa-check'></>");
	$adapter->add("25-44 TH", "44th","trivial_0__<i class='fa fa-check'></>");
	$adapter->add("45-59 TH", "59th","trivial_0__<i class='fa fa-check'></>");
	$adapter->add("60-64 TH", "64th","trivial_0__<i class='fa fa-check'></>");
	$adapter->add("65 TH+", "65th","trivial_0__<i class='fa fa-check'></>");
	$adapter->add("Diagnosa Sekunder", "keterangan");
	$adapter->add("Kelas","kelas", "unslug");
	$adapter->add("Dipulangkan Mati <=48 Jam", "mk48","trivial_0__<i class='fa fa-check'></>");
	$adapter->add("Dipulangkan Mati >48 Jam", "ml48","trivial_0__<i class='fa fa-check'></>");
	$adapter->add("Pulang Paksa", "paksa","trivial_0__<i class='fa fa-check'></>");
	$adapter->add("Kabur", "kabur","trivial_0__<i class='fa fa-check'></>");
	$adapter->add("Rujuk RS Lain", "rslain","trivial_0__<i class='fa fa-check'></>");
	$adapter->add("Dikembalikan ke Perujuk", "perujuk","trivial_0__<i class='fa fa-check'></>");
	$adapter->add("Dipulangkan Hidup", "hidup","trivial_0__<i class='fa fa-check'></>");
	
	
	if($_POST['command']=="last_count"){
		$dbtable->setShowAll(true);
		$query=$dbtable->getQueryView("", "0");
		
		$adapter=new SimpleAdapter();
		$adapter->setUseNumber(true, "No.","back.");
		$adapter->add("Tanggal", "tanggal","date d M Y");
        $adapter->add("Pasien", "nama_pasien");
		$adapter->add("NRM", "nrm_pasien","only-digit8");
        $adapter->add("Propinsi", "propinsi");
        $adapter->add("Kabupaten", "kabupaten");
        $adapter->add("Kecamatan", "kecamatan");
        $adapter->add("Kelurahan", "kelurahan");
		$adapter->add("Ruangan", "ruangan","unslug");
		$adapter->add("Masuk", "tgl_masuk","date d M Y");
		$adapter->add("Keluar", "tgl_keluar","date d M Y");
		$adapter->add("Lama Dirawat", "lama_dirawat","back Hari");
		$adapter->add("0 - 28 HR", "28hr");
		$adapter->add("28 HR - 1 TH", "1th");
		$adapter->add("1-4 TH", "4th");
		$adapter->add("5-14 TH", "14th");
		$adapter->add("15-24 TH", "24th");
		$adapter->add("25-44 TH", "44th");
		$adapter->add("45-59 TH", "59th");
		$adapter->add("60-64 TH", "64th");
		$adapter->add("65 TH+", "65th");
		$adapter->add("Diagnosa Sekunder", "keterangan");
		$adapter->add("Kelas","kelas", "unslug");
		$adapter->add("Dipulangkan Mati <=48 Jam", "mk48");
		$adapter->add("Dipulangkan Mati >48 Jam", "ml48");
		$adapter->add("Pulang Paksa", "paksa");
		$adapter->add("Kabur", "kabur");
		$adapter->add("Rujuk RS Lain", "rslain");
		$adapter->add("Dikembalikan ke Perujuk", "perujuk");
		$adapter->add("Dipulangkan Hidup", "hidup");
		$adapter->add("L", "laki");
		$adapter->add("P", "pr");
		
		$q="SELECT '' as nama_icd, '' as tanggal,'' as nrm_pasien, '' as nama_pasien,'' as propinsi, '' as kabupaten, '' as kecamatan, '' as kelurahan,
			'' as tgl_masuk,'' as tgl_keluar,'' as lama_dirawat,
			sum(28hr) as '28hr',
			sum(1th) as '1th',
			sum(4th) as '4th',
			sum(14th) as '14th',
			sum(24th) as '24th',
			sum(44th) as '44th',
			sum(59th) as '59th',
			sum(64th) as '64th',
			sum(65th) as '65th',
			'' as ruangan,
			'' as keterangan,
			'' as kelas,
			sum(laki) as laki,
			sum(pr) as pr,
			sum(mk48) as mk48,
			sum(ml48) as ml48,
			sum(paksa) as paksa,
			sum(kabur) as kabur,
			sum(rslain) as rslain,
			sum(perujuk) as perujuk,
			sum(hidup) as hidup,
			'' as prop		
		FROM (".$query['query'].") as X ";
		
		$one=$db->get_result($q,false);
		$adapter->setRemoveZeroEnable(true);
		$content=$adapter->getContent($one);
		$content[0]['No.']="";
		$content[0]['Tanggal']="<strong>TOTAL</strong>";
		$content[0]['NRM']="";
		$content[0]['Masuk']="";
		$content[0]['Keluar']="";
		$content[0]['Lama Dirawat']="";
		$uitable->setContent($content);
		$list=$uitable->getBodyContent();
		
		$res=new ResponsePackage();
		$res->setContent($list);
		$res->setStatus(ResponsePackage::$STATUS_OK);
		echo json_encode($res->getPackage());
		return;
	}
	
	$dbres=new DBResponder($dbtable, $uitable, $adapter);
	$hasil=$dbres->command($_POST['command']);
	echo json_encode($hasil);
	return;
}

$urji=new OptionBuilder();
$urji->add("","","1");
$urji->add("Rawat Jalan","0","0");
$urji->add("Rawat Inap","1","0");

$uitable->clearContent();
$uitable->addModal("dari", "date", "Dari", "")
		->addModal("sampai", "date", "Sampai", "")
		->addModal("urji", "select", "URJ/URI", $urji->getContent())
		->addModal("nama_penyakit", "chooser-lap_index_penyakit-penyakit_lap_index_penyakit-penyakit", "Penyakit", "");
$action=new Button("","","View");
$action->setClass("btn-primary")
	   ->setIsButton(Button::$ICONIC)
	   ->setIcon("icon-white fa fa-circle-o-notch")
	   ->setAction("lap_index_penyakit.rekaptotal()");
$view=new Button("","","View");
$view->setClass("btn-primary")
		->setIsButton(Button::$ICONIC)
		->setIcon("fa fa-refresh")
		->setAction("lap_index_penyakit.multiview()");
$excel=new Button("","","Excel");
$excel->setClass("btn-primary")
	->setIsButton(Button::$ICONIC)
	->setIcon("fa fa-file-excel-o")
	->setAction("lap_index_penyakit.excel()");

$btng=new ButtonGroup("");
$btng->addButton($action);
$btng->addButton($view);
$btng->addButton($excel);

$form=$uitable
	  ->getModal()
	  ->setTitle("Medical Record")
	  ->getForm()
	  ->addElement("",$btng);

$close=new Button("", "", "Batal");
$close	->addClass("btn-primary")
		->setIsButton(Button::$ICONIC_TEXT)
		->setIcon("fa fa-close")
		->setAction("lap_index_penyakit.batal()");

$load=new LoadingBar("rekap_lap_index_penyakit_bar", "");
$modal=new Modal("rekap_lap_index_penyakit_modal", "", "Processing...");
$modal	->addHTML($load->getHtml(),"after")
		->addFooter($close);

echo $form->getHtml();
echo "<div class='clear'></div>";
echo "<div id='result_lap_index_penyakit'>".$uitable->getHtml()."</div>";
echo $modal->getHtml();
echo addJS("framework/bootstrap/js/bootstrap-datepicker.js");
echo addJS("framework/smis/js/table_action.js");
echo addCSS("framework/bootstrap/css/datepicker.css");
echo addJS ( "base-js/smis-base-loading.js");
echo addJS ( "medical_record/resource/js/lap_index_penyakit.js",false);

?>
