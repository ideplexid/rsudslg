<?php 
global $db;
global $user;
require_once 'smis-base/smis-include-service-consumer.php';
if(isset($_POST['super_command']) && $_POST['super_command']=="total") {
	require_once 'smis-libs-class/ServiceProviderList.php';
	$dbtable=new DBTable($db, "smist_mr_lap_rajal");
	//$dbtable->truncate();
    $query = "DELETE FROM smist_mr_lap_rajal";
    $db->get_var($query);
	$serv=new ServiceConsumer($db, "get_patient_pulang");
	$serv->addData("command", "count");
	$serv->addData("dari", $_POST['dari']);
	$serv->addData("sampai", $_POST['sampai']);	
    $serv->addData("uri", "0");	
	$serv->execute();
	$data=$serv->getContent();
	$res=new ResponsePackage();
	$res->setStatus(ResponsePackage::$STATUS_OK);
	$res->setContent($data);
	echo json_encode($res->getPackage());
	return;
}

if(isset($_POST['super_command']) && $_POST['super_command']=="limit") {
	$data=array();
	$response=new ServiceConsumer($db, "get_patient_pulang");
	$response->addData("command", "list");
	$response->addData("dari", $_POST['dari']);
	$response->addData("sampai", $_POST['sampai']);
	$response->addData("halaman", $_POST['halaman']);
    $response->addData("uri", "0");	
	$response->setMode(ServiceConsumer::$SINGLE_MODE);
	$response->execute();
	$list=$response->getContent();
	$dbtable=new DBTable($db, "smist_mr_lap_rajal");
	$diagnosa=new DBTable($db,"smis_mr_diagnosa");
	$operasi=new DBTable($db,"smis_mr_operasi");
	
	foreach($list as $x){		
		$d['nrm']=$x['nrm'];
		$d['nama_pasien']=$x['nama_pasien'];
		$d['noreg']=$x['id'];
		$d['tgl_masuk']=$x['tanggal'];
		$d['dokter_pj']=$x['nama_dokter'];
		$d['tgl_pulang']=$x['tanggal_pulang'];
		$d['ruang']=$x['jenislayanan'];
        $d['carabayar']=$x['carabayar'];
        $d['carapulang']=$x['carapulang'];
        $d['n_perusahaan']=$x['n_perusahaan'];
        $d['n_asuransi']=$x['nama_asuransi'];
		$d['kunjungan']=$x['barulama']=="0"?"Baru":"Lama";
        $d['kelurahan']=$x['nama_kelurahan'];
        $d['kecamatan']=$x['nama_kecamatan'];
        $d['kabupaten']=$x['nama_kabupaten'];
        $d['umur']=$x['umur'];
        $d['jk']=$x['kelamin'];
        /*checking diagnosa awal*/
        $query="SELECT * FROM smis_mr_diagnosa WHERE noreg_pasien='".$x['id']."' AND prop!='del' ORDER BY id ASC";
		$diag=$db->get_row($query);
        if($diag!=NULL){
			$d['diagnosa']=$diag->diagnosa;
			$d['kode_diagnosa']=$diag->kode_icd;
            $d['kasus']=$diag->kasus;
            if($d['dokter_pj']==""){
                $d['dokter_pj']=$diag->nama_dokter;
            }
		}
        
		$op=$operasi->select(array("noreg_pasien"=>$x['id']));
		if($op!=NULL){
			$d['dokter_bedah']=$op->dokter_operator;
			$d['dokter_anastesi']=$op->dokter_anastesi;
			$d['tgl_tindakan']=$op->tanggal_mulai;
			$d['tindakan']=$op->tindakan;
			$d['bedah']="1";
		}		
		
		$dbtable->insert($d);		
	}
	$res=new ResponsePackage();
	$res->setStatus(ResponsePackage::$STATUS_OK);
	$res->setContent($list);
	echo json_encode($res->getPackage());
	return;
}

$header=array(	"No.","NRM","Nama","Umur","Ruang","Masuk",
				"ICD10","Diagnosa","Dokter DPJP","ICD9","Tindakan",
				"Dokter Bedah","Dokter Anastesi");
$uitable=new Table($header);
$uitable->setName("lap_index_rajal")
		->setActionEnable(false);

if(isset($_POST['command']) && $_POST['command']=="excel"){
	require_once "medical_record/snippet/create_excel_lap_index_rajal.php";
    return;
}

if(isset($_POST['command']) && $_POST['command']!=""){
	$dbtable=new DBTable($db, "smist_mr_lap_rajal");
	$dbtable->setOrder(" tgl_pulang ASC");
	
	$adapter=new SimpleAdapter();
	$adapter->setUseNumber(true, "No.","back.");
	$adapter->add("NRM", "nrm","digit8");
	$adapter->add("Nama", "nama_pasien");
    $adapter->add("Umur", "umur");
	$adapter->add("Ruang", "ruang","unslug");
	$adapter->add("ICD10", "kode_diagnosa");
	$adapter->add("Diagnosa", "diagnosa");
	$adapter->add("Dokter DPJP", "dokter_pj");
	$adapter->add("ICD9", "kode_tindakan");
	$adapter->add("Tindakan", "tindakan");
	$adapter->add("Tanggal Tindakan", "tgl_tindakan","date d M Y");
	$adapter->add("Dokter Bedah", "dokter_bedah");
	$adapter->add("Dokter Anastesi", "dokter_anastesi");
	$adapter->add("Masuk", "tgl_masuk","date d M Y H:i");
	
	$dbres=new DBResponder($dbtable, $uitable, $adapter);
	$hasil=$dbres->command($_POST['command']);
	echo json_encode($hasil);
	return;
}


$uitable->clearContent();
$uitable->addModal("dari", "date", "Dari", "")
		->addModal("sampai", "date", "Sampai", "")
		->addModal("ruang_ok", "text", "Ruang Operasi", getSettings($db,"mr-slug-ruang-operasi",""),"",null,true);
$action=new Button("","","View");
$action->setClass("btn-primary")
	   ->setIsButton(Button::$ICONIC)
	   ->setIcon("icon-white fa fa-circle-o-notch")
	   ->setAction("lap_index_rajal.rekaptotal()");
$view=new Button("","","View");
$view->setClass("btn-primary")
		->setIsButton(Button::$ICONIC)
		->setIcon("fa fa-refresh")
		->setAction("lap_index_rajal.view()");
$excel=new Button("","","Excel");
$excel->setClass("btn-primary")
	->setIsButton(Button::$ICONIC)
	->setIcon("fa fa-file-excel-o")
	->setAction("lap_index_rajal.excel()");

$btng=new ButtonGroup("");
$btng->addButton($action);
$btng->addButton($view);
$btng->addButton($excel);

$form=$uitable
	  ->getModal()
	  ->setTitle("Medical Record")
	  ->getForm()
	  ->addElement("",$btng);

$close=new Button("", "", "Batal");
$close	->addClass("btn-primary")
		->setIsButton(Button::$ICONIC_TEXT)
		->setIcon("fa fa-close")
		->setAction("lap_index_rajal.batal()");

$load=new LoadingBar("rekap_lap_index_rajal_bar", "");
$modal=new Modal("rekap_lap_index_rajal_modal", "", "Processing...");
$modal	->addHTML($load->getHtml(),"after")
		->addFooter($close);

echo $form->getHtml();
echo "<div class='clear'></div>";
echo "<div id='result_lap_index_rajal'>".$uitable->getHtml()."</div>";
echo $modal->getHtml();
echo addJS("framework/bootstrap/js/bootstrap-datepicker.js");
echo addJS("framework/smis/js/table_action.js");
echo addCSS("framework/bootstrap/css/datepicker.css");
echo addJS ( "base-js/smis-base-loading.js");
echo addJS ( "medical_record/resource/js/lap_index_rajal.js",false);
?>