<?php 
global $db;
global $user;
require_once 'smis-base/smis-include-service-consumer.php';
show_error();
$dkadapter = new SimpleAdapter ();
$dkadapter->add ( "Nama", "nama" );
$dkadapter->add ( "Kode ICD", "icd" );
$dkadapter->add ( "Keterangan", "sebab" );
$header=array ('Nama','Kode ICD',"Keterangan" );
$dktable = new Table ( $header);
$dktable->setName ( "tindakan_lap_index_operasi" );
$dktable->setModel ( Table::$SELECT );
$dbtable=new DBTable($db,"smis_mr_icdtindakan"); 
$tindakan = new DBResponder( $dbtable, $dktable, $dkadapter,"tindakan");
$super = new SuperCommand ();
$super->addResponder ( "tindakan_lap_index_operasi", $tindakan );
$init = $super->initialize ();
if ($init != null) {
	echo $init;
	return;
}


if(isset($_POST['super_command']) && $_POST['super_command']=="total") {
	require_once 'smis-libs-class/ServiceProviderList.php';
	$dbtable=new DBTable($db, "smis_mr_operasi");
	$dbtable->addCustomKriteria(NULL," tanggal_mulai >='".$_POST['dari']."' ");
	$dbtable->addCustomKriteria(NULL," tanggal_mulai <'".$_POST['sampai']."' ");
	$dbtable->addCustomKriteria(NULL," ( carapulang ='' OR gol_umur_io='TGL LAHIR TDK VALID' OR gol_umur_io='' ) ");
	$q=$dbtable->getQueryCount("");
	$total=$db->get_var($q);
	$res=new ResponsePackage();
	$res->setStatus(ResponsePackage::$STATUS_OK);
	$res->setContent($total);
	echo json_encode($res->getPackage());
	return;
}


if(isset($_POST['super_command']) && $_POST['super_command']=="limit") {
	$data=array();
	
	$dbtable=new DBTable($db, "smis_mr_operasi");
	$dbtable->addCustomKriteria(NULL," tanggal_mulai >='".$_POST['dari']."' ");
	$dbtable->addCustomKriteria(NULL," tanggal_selesai <'".$_POST['sampai']."' ");
	$dbtable->addCustomKriteria(NULL," ( carapulang ='' OR gol_umur_io='TGL LAHIR TDK VALID' OR gol_umur_io='TGL LAHIR TDK VALID') ");
	$dbtable->setMaximum(1);
	$data=$dbtable->view("",$_POST['halaman']);
	$list=$data['data'];
	
	
	loadLibrary("smis-libs-function-time");
	loadLibrary("smis-libs-function-medical");
	foreach($list as $x){		
		$response=new ServiceConsumer($db, "get_registered",NULL,"registration");
		$response->addData("command", "select");
		$response->addData("id", $x->noreg_pasien);
		$response->execute();
		$one=$response->getContent();
		if($one==NULL)
			continue;
		$up=array();
		$up['tanggal_mrs']=$one['tanggal'];
		$up['tanggal_kmrs']=$one['tanggal_pulang'];
		$up['carapulang']=$one['carapulang'];	
		$up['carabayar']=$one['carabayar'];		
		$up['alamat']=$one['alamat_pasien'];		
		$up['gol_umur_io']=medical_gol_umur_index_penyakit($up['tanggal_mrs'],$one['tgl_lahir']);	
		$up['lama_dirawat']=day_diff_only($up['tanggal_kmrs'],$up['tanggal_mrs']);
		$dbtable->update($up,array("id"=>$x->id));
	}
	$res=new ResponsePackage();
	$res->setStatus(ResponsePackage::$STATUS_OK);
	$res->setContent($list);
	echo json_encode($res->getPackage());
	return;
}

$service=new ServiceConsumer($db,"get_jenis_patient",NULL,"registration");
$service->execute();
$carabayar=$service->getContent();
$total=count($carabayar);

$head0="<tr>
			<th rowspan='2'>No.</th> <th rowspan='2'>No RM</th> <th rowspan='2'>Ruangan</th> <th colspan='3'>Tanggal</th>
			<th rowspan='2'>Durasi</th> <th colspan='2'>Seks</th> <th colspan='9'>Golongan Umur</th> <th rowspan='2'>Domisili</th>
			<th colspan='$total'>Carabayar</th>
			<th colspan='2'>Hasil</th> <th rowspan='2'>Diagnosa</th> <th colspan='2'>Kasus</th> <th rowspan='2'>Ahli Bedah</th>
		</tr>";
$_result="";
$_summary="";
$_query="";
foreach($carabayar as $x){
	$_result.="<th>".$x['name']."</th>";
	$_query.="if(carabayar='".$x['name']."',1,0 ) as ".$x['value'].", ";
	$_summary.="sum(".$x['value'].") as ".$x['value'].", ";
}
$head1="<tr>
			<th>Masuk</th> <th>Keluar</th> <th>Operasi</th>
			<th>L</th> <th>P</th>
			<th>0 - 28 HR</th> <th>28 HR - 1 TH</th> <th>1-4 TH</th>
			<th>5-14 TH</th> <th>15-24 TH</th>
			<th>25-44 TH</th> <th>45-59 TH</th> <th>60-64 TH</th> <th>65 TH+</th>
			".$_result."
			<th>Meninggal</th>
			<th>Sembuh</th>
			<th>Lama</th>
			<th>Baru</th>
		</tr>";
			
				
$uitable=new Table(array());
$uitable->addHeader("before",$head0);
$uitable->addHeader("before",$head1);
$uitable->addHeaderElement("No.");
$uitable->addHeaderElement("NRM");
$uitable->addHeaderElement("Ruangan");
$uitable->addHeaderElement("Masuk");
$uitable->addHeaderElement("Keluar");
$uitable->addHeaderElement("Operasi");
$uitable->addHeaderElement("Durasi");
$uitable->addHeaderElement("L");
$uitable->addHeaderElement("P");
$uitable->addHeaderElement("0 - 28 HR");
$uitable->addHeaderElement("28 HR - 1 TH");
$uitable->addHeaderElement("1-4 TH");
$uitable->addHeaderElement("5-14 TH");
$uitable->addHeaderElement("15-24 TH");
$uitable->addHeaderElement("25-44 TH");
$uitable->addHeaderElement("45-59 TH");
$uitable->addHeaderElement("60-64 TH");
$uitable->addHeaderElement("65 TH+");
$uitable->addHeaderElement("Domisili");
foreach($carabayar as $x){
	$uitable->addHeaderElement($x['name']);
}
$uitable->addHeaderElement("Meninggal");
$uitable->addHeaderElement("Sembuh");
$uitable->addHeaderElement("Diagnosa");
$uitable->addHeaderElement("Lama");
$uitable->addHeaderElement("Baru");
$uitable->addHeaderElement("Dokter");
$uitable->setHeaderVisible(false);
$uitable->setName("lap_index_operasi")
		->setActionEnable(false)
		->setFooterVisible(false);

if(isset($_POST['command']) && $_POST['command']!=""){	
	$query_view="
			SELECT 
			".$_query."
			nama_tindakan,alamat,
			tanggal_mulai,nrm_pasien,tanggal_mrs,tanggal_kmrs,lama_dirawat,
			if(gol_umur_io='0 - 28 HR',1,0) as '28hr',
			if(gol_umur_io='28 HR - 1 TH',1,0) as '1th',
			if(gol_umur_io='1-4 TH',1,0) as '4th',
			if(gol_umur_io='5-14 TH',1,0) as '14th',
			if(gol_umur_io='15-24 TH',1,0) as '24th',
			if(gol_umur_io='25-44 TH',1,0) as '44th',
			if(gol_umur_io='45-59 TH',1,0) as '59th',
			if(gol_umur_io='60-64 TH',1,0) as '64th',
			if(gol_umur_io='65 TH+',1,0) as '65th',
			if(jk=0,1,0) as laki,
			if(jk=1,1,0) as pr,
			dokter_operator,
			diagnosa,
			ruangan as ruangan,
			if(kasus=0,1,0) as baru,
			if(kasus=1,1,0) as lama,
			ifnull(kelas,ruangan) as kelas,
			if(carapulang LIKE 'Dipulangkan Mati%',1,0) as meninggal,
			if(carapulang NOT LIKE 'Dipulangkan Mati%',1,0) as sembuh
			FROM smis_mr_operasi			
	";
	$query_count="SELECT COUNT(*) FROM smis_mr_operasi";
	
	$dbtable=new DBTable($db, "smis_mr_operasi");
	$dbtable->setPreferredQuery(true,$query_view,$query_count);
	$dbtable->setUseWhereforView(true);
	$dbtable->setOrder(" tanggal_mulai ASC");	
	$dbtable->addCustomKriteria(" nama_tindakan "," like '".$_POST['nama_tindakan']."%' ");
	$adapter=new SimpleAdapter();
	$adapter->setUseNumber(true, "No.","back.");
	$adapter->add("NRM", "nrm_pasien","only-digit8");
	$adapter->add("Ruangan", "ruangan","unslug");
	$adapter->add("Masuk", "tanggal_mrs","date d M Y");
	$adapter->add("Operasi", "tanggal_mulai","date d M Y");
	$adapter->add("Keluar", "tanggal_kmrs","date d M Y");
	$adapter->add("Durasi", "lama_dirawat","back Hari");	
	$adapter->add("L", "laki","trivial_0__<i class='fa fa-check'></>");
	$adapter->add("P", "pr","trivial_0__<i class='fa fa-check'></>");	
	$adapter->add("0 - 28 HR", "28hr","trivial_0__<i class='fa fa-check'></>");
	$adapter->add("28 HR - 1 TH", "1th","trivial_0__<i class='fa fa-check'></>");
	$adapter->add("1-4 TH", "4th","trivial_0__<i class='fa fa-check'></>");
	$adapter->add("5-14 TH", "14th","trivial_0__<i class='fa fa-check'></>");
	$adapter->add("15-24 TH", "24th","trivial_0__<i class='fa fa-check'></>");
	$adapter->add("25-44 TH", "44th","trivial_0__<i class='fa fa-check'></>");
	$adapter->add("45-59 TH", "59th","trivial_0__<i class='fa fa-check'></>");
	$adapter->add("60-64 TH", "64th","trivial_0__<i class='fa fa-check'></>");
	$adapter->add("65 TH+", "65th","trivial_0__<i class='fa fa-check'></>");
	$adapter->add("Diagnosa", "diagnosa");
	$adapter->add("Domisili", "alamat");
	$adapter->add("Dokter", "dokter_operator");
	$adapter->add("Kelas","kelas", "unslug");
	$adapter->add("Lama", "lama","trivial_0__<i class='fa fa-check'></>");
	$adapter->add("Baru", "baru","trivial_0__<i class='fa fa-check'></>");
	$adapter->add("Meninggal", "meninggal","trivial_0__<i class='fa fa-check'></>");
	$adapter->add("Sembuh", "sembuh","trivial_0__<i class='fa fa-check'></>");
	foreach($carabayar as $x){
		$adapter->add($x['name'], $x['value'],"trivial_0__<i class='fa fa-check'></>");
	}
	
	if($_POST['command']=="last_count"){
		
		$adapter=new SimpleAdapter();
		$adapter->setUseNumber(true, "No.","back.");
		$adapter->add("Operasi", "tanggal_mulai","date d M Y");
		$adapter->add("NRM", "nrm_pasien","only-digit8");
		$adapter->add("Ruangan", "ruangan","unslug");
		$adapter->add("Masuk", "tanggal_mrs","date d M Y");
		$adapter->add("Keluar", "tanggal_kmrs","date d M Y");
		$adapter->add("Durasi", "lama_dirawat","back Hari");
		$adapter->add("0 - 28 HR", "28hr");
		$adapter->add("28 HR - 1 TH", "1th");
		$adapter->add("1-4 TH", "4th");
		$adapter->add("5-14 TH", "14th");
		$adapter->add("15-24 TH", "24th");
		$adapter->add("25-44 TH", "44th");
		$adapter->add("45-59 TH", "59th");
		$adapter->add("60-64 TH", "64th");
		$adapter->add("65 TH+", "65th");
		$adapter->add("Diagnosa", "diagnosa");
		$adapter->add("Kelas","kelas", "unslug");
		$adapter->add("Meninggal", "meninggal");
		$adapter->add("Sembuh", "sembuh");
		$adapter->add("L", "laki");
		$adapter->add("P", "pr");
		$adapter->add("Lama", "lama");
		$adapter->add("Baru", "baru");
		foreach($carabayar as $x){
			$adapter->add($x['name'], $x['value']);
		}
		
		$dbtable->setShowAll(true);
		$query=$dbtable->getQueryView("", "0");
		$q="SELECT ".$_summary." 
			'' as nama_tindakan, '' as tanggal_mulai,'' as nrm_pasien,
			'' as tanggal_mrs,'' as tanggal_kmrs,'' as lama_dirawat,alamat,
			sum(28hr) as '28hr',
			sum(1th) as '1th',
			sum(4th) as '4th',
			sum(14th) as '14th',
			sum(24th) as '24th',
			sum(44th) as '44th',
			sum(59th) as '59th',
			sum(64th) as '64th',
			sum(65th) as '65th',
			'' as ruangan,
			'' as diagnosa,
			'' as kelas,
			sum(laki) as laki,
			sum(pr) as pr,
			sum(sembuh) as sembuh,
			sum(meninggal) as meninggal,
			sum(lama) as lama,
			sum(baru) as baru,
			'' as prop		
		FROM (".$query['query'].") as X ";
		
		$one=$db->get_result($q,false);
		$adapter->setRemoveZeroEnable(true);
		$content=$adapter->getContent($one);
		$content[0]['No.']="<strong>TOTAL</strong>";
		$content[0]['Operasi']="";
		$content[0]['NRM']="";
		$content[0]['Masuk']="";
		$content[0]['Keluar']="";
		$content[0]['Durasi']="";
		$uitable->setContent($content);
		$list=$uitable->getBodyContent();
		
		$res=new ResponsePackage();
		$res->setContent($list);
		$res->setStatus(ResponsePackage::$STATUS_OK);
		echo json_encode($res->getPackage());
		return;
	}
	
	
	
	$dbres=new DBResponder($dbtable, $uitable, $adapter);
	$hasil=$dbres->command($_POST['command']);
	echo json_encode($hasil);
	return;
}

$uitable->clearContent();
$uitable->addModal("dari", "date", "Dari", "")
		->addModal("sampai", "date", "Sampai", "")
		->addModal("nama_tindakan", "chooser-lap_index_operasi-tindakan_lap_index_operasi-tindakan", "Tindakan", "");
$action=new Button("","","View");
$action->setClass("btn-primary")
	   ->setIsButton(Button::$ICONIC)
	   ->setIcon("icon-white fa fa-circle-o-notch")
	   ->setAction("lap_index_operasi.rekaptotal()");
$view=new Button("","","View");
$view->setClass("btn-primary")
		->setIsButton(Button::$ICONIC)
		->setIcon("fa fa-refresh")
		->setAction("lap_index_operasi.multiview()");
$excel=new Button("","","Excel");
$excel->setClass("btn-primary")
	->setIsButton(Button::$ICONIC)
	->setIcon("fa fa-file-excel-o")
	->setAction("lap_index_operasi.excel()");

$btng=new ButtonGroup("");
$btng->addButton($action);
$btng->addButton($view);
//$btng->addButton($excel);

$form=$uitable
	  ->getModal()
	  ->setTitle("Medical Record")
	  ->getForm()
	  ->addElement("",$btng);

$close=new Button("", "", "Batal");
$close	->addClass("btn-primary")
		->setIsButton(Button::$ICONIC_TEXT)
		->setIcon("fa fa-close")
		->setAction("lap_index_operasi.batal()");

$load=new LoadingBar("rekap_lap_index_operasi_bar", "");
$modal=new Modal("rekap_lap_index_operasi_modal", "", "Processing...");
$modal	->addHTML($load->getHtml(),"after")
		->addFooter($close);

echo $form->getHtml();
echo "<div class='clear'></div>";
echo "<div id='result_lap_index_operasi'>".$uitable->getHtml()."</div>";
echo $modal->getHtml();
echo addJS("framework/bootstrap/js/bootstrap-datepicker.js");
echo addJS("framework/smis/js/table_action.js");
echo addCSS("framework/bootstrap/css/datepicker.css");
echo addJS ( "base-js/smis-base-loading.js");
?>

<script type="text/javascript">
	var lap_index_operasi;
	var tindakan_lap_index_operasi;
	var IS_lap_index_operasi_RUNNING;
	$(document).ready(function(){
		$('.mydate').datepicker();
		lap_index_operasi=new TableAction("lap_index_operasi","medical_record","lap_index_operasi",new Array());
		lap_index_operasi.addRegulerData=function(data){
			data['dari']=$("#lap_index_operasi_dari").val();
			data['sampai']=$("#lap_index_operasi_sampai").val();
			data['nama_tindakan']=$("#lap_index_operasi_nama_tindakan").val();
			$("#dari_table_lap_index_operasi").html(getFormattedDate(data['dari']));
			$("#sampai_table_lap_index_operasi").html(getFormattedDate(data['sampai']));							
			return data;
		};

		lap_index_operasi.batal=function(){
			IS_lap_index_operasi_RUNNING=false;
			$("#rekap_lap_index_operasi_modal").modal("hide");
		};
		
		lap_index_operasi.afterview=function(json){
			if(json!=null){
				$("#kode_table_lap_index_operasi").html(json.nomor);
				$("#waktu_table_lap_index_operasi").html(json.waktu);
				lap_index_operasi_data=json;
			}
		};

		lap_index_operasi.rekaptotal=function(){
			if(IS_lap_index_operasi_RUNNING) return;
			$("#rekap_lap_index_operasi_bar").sload("true","Fetching total data",0);
			$("#rekap_lap_index_operasi_modal").modal("show");
			IS_lap_index_operasi_RUNNING=true;
			var d=this.getRegulerData();
			d['super_command']="total";
			$.post("",d,function(res){
				var all=getContent(res);
				if(all!=null) {
					var total=Number(all);
					lap_index_operasi.rekaploop(0,total);
				} else {
					$("#rekap_lap_index_operasi_modal").modal("hide");
					IS_lap_index_operasi_RUNNING=false;
				}
			});
		};

		lap_index_operasi.excel=function(){
			var d=this.getRegulerData();
			d['command']="excel";
			download(d);
		};

		lap_index_operasi.rekaploop=function(current,total){
			if(current>=total || !IS_lap_index_operasi_RUNNING) {
				//$("#rekap_lap_index_operasi_modal").modal("hide");
				IS_lap_index_operasi_RUNNING=false;
				lap_index_operasi.multiview();
				return;
			}
			var d=this.getRegulerData();
			d['super_command']="limit";
			d['halaman']=current;
			$.post("",d,function(res){
				var ct=getContent(res);
				var u="";//ct[0]['nama_pasien']+"  "+ct[0]['nrm_pasien'];
				$("#rekap_lap_index_operasi_bar").sload("true"," Processing - "+u+"... [ "+(current+1)+" / "+total+" ] ",(current*100/total));
				setTimeout(function(){lap_index_operasi.rekaploop(++current,total)},300);
			});
		};
		
		lap_index_operasi.multiview=function(){
			$("#rekap_lap_index_operasi_bar").sload("true","Viewing data...",0);
			$("#rekap_lap_index_operasi_modal").modal("show");
			IS_lap_index_operasi_RUNNING=true;
			var d=this.getViewData();
			$("#lap_index_operasi_list").html("");
			d['command']="multipart_view";
			$.post("",d,function(res){
				var total=Number(getContent(res));
				if(total==0){
					$("#rekap_lap_index_operasi_modal").modal("hide");
					IS_lap_index_operasi_RUNNING=false;
				}else{
					var pages=Math.ceil(total/5);
					lap_index_operasi.multipart_loop(0,pages);
				}
			});			
		};
		
		lap_index_operasi.multipart_loop=function(current,total){
			if(current>=total || !IS_lap_index_operasi_RUNNING) {
				IS_lap_index_operasi_RUNNING=false;
				lap_index_operasi.last_count();
				return;
			}
			$("#rekap_lap_index_operasi_bar").sload("true","Page Number [ "+(current+1)+" / "+total+" ] ",(current*100/total));
			var d=this.getViewData();
			d['number']=current;
			d['max']=5;		
			$.post("",d,function(res){
				var ct=getContent(res);
				$("#lap_index_operasi_list").append(ct.list);
				setTimeout(function(){lap_index_operasi.multipart_loop(++current,total)},300);
			});
		};
		
		lap_index_operasi.last_count=function(){
			var d=this.getRegulerData();
			d['command']="last_count";
			$.post("",d,function(res){
				var ct=getContent(res);
				$("#lap_index_operasi_list").append(ct);
				$("#rekap_lap_index_operasi_modal").modal("hide");
			});
		};
		

		
		tindakan_lap_index_operasi=new TableAction("tindakan_lap_index_operasi","medical_record","lap_index_operasi",new Array());
		tindakan_lap_index_operasi.setSuperCommand("tindakan_lap_index_operasi");
		tindakan_lap_index_operasi.selected=function(json){
			$("#lap_index_operasi_nama_tindakan").val(json.nama);
			lap_index_operasi.multiview();
		};
	});
</script>