<?php

global $db;
global $user;
require_once 'smis-base/smis-include-service-consumer.php';
require_once 'hrd/library/class/responder/EmployeeResponder.php';

/* Chooser Penyakit*/
$dkadapter = new SimpleAdapter ();
$dkadapter->add ( "Nama", "nama" );
$dkadapter->add ( "Kode ICD", "icd" );
$dkadapter->add ( "Keterangan", "sebab" );
$header=array ('Nama','Kode ICD',"Keterangan" );
$dktable = new Table ( $header);
$dktable->setName ( "penyakit_laporan_index_penyakit" );
$dktable->setModel ( Table::$SELECT );
$dbtable=new DBTable($db,"smis_mr_icd"); 
$penyakit = new DBResponder( $dbtable, $dktable, $dkadapter,"penyakit");
$super = new SuperCommand ();
$super->addResponder ( "penyakit_laporan_index_penyakit", $penyakit );
$init = $super->initialize ();
if ($init != null) {
	echo $init;
	return;
}

$header=array(	"No.","Tanggal","NRM","Nama Pasien", "Alamat","Tgl Lahir","Ruangan","Masuk",
				"Keluar","Lama Dirawat","L","P",
				"0 - 28 HR","28 HR - 1 TH","1-4 TH",
				"5-14 TH","15-24 TH","25-44 TH","45-65 TH","65 TH+",
				"Diagnosa","Kelas",
				"Dipulangkan Mati <=48 Jam","Dipulangkan Mati >48 Jam",
				"Pulang Paksa","Kabur",
				"Rujuk RS Lain","Dikembalikan ke Perujuk",
				"Dipulangkan Hidup", "Rujukan Dokter", "Rujukan Bidan", "Rujukan Rumah Sakit", "Datang Sendiri"
            );
$uitable=new Table($header);
$uitable->setName("laporan_index_penyakit")
		->setActionEnable(false);
		//->setFooterVisible(false);

if(isset($_POST['command']) && $_POST['command']) {
    $dbtable = new DBTable ( $db, "smis_mr_diagnosa");
    if(isset($_POST['dari']) && $_POST['dari']!="") {
        $dbtable->addCustomKriteria(""," tanggal >= '".$_POST['dari']."' ");
    }
    if(isset($_POST['sampai']) && $_POST['sampai']!="") {
        $dbtable->addCustomKriteria(""," tanggal <= '".$_POST['sampai']."' ");
    }
	$dbtable->addCustomKriteria(" nama_icd "," like '".$_POST['nama_penyakit']."%' ");
	if(isset($_POST['urji']) && $_POST['urji']!=""){
		$dbtable->addCustomKriteria(" urji "," = '".$_POST['urji']."' ");
	}
    $dbtable->setOrder(" nama_icd ASC");	
    //$dbtable->setShowAll(true);
    
    require_once "medical_record/class/adapter/LapIndexPenyakitAdapter.php";
	$adapter = new LapIndexPenyakitAdapter();
    
    //require_once "medical_record/class/responder/LapIndexPenyakitResponder.php";
    //$dbres = new LapIndexPenyakitResponder($dbtable, $uitable, $adapter);
    require_once "medical_record/class/responder/LapIndexPenyakitPartialResponder.php";
    $dbres = new LapIndexPenyakitPartialResponder($dbtable, $uitable, $adapter);
	$hasil = $dbres->command($_POST['command']);
	echo json_encode($hasil);
	return;
}

$urji=new OptionBuilder();
$urji->add("","","1");
$urji->add("Rawat Jalan","0","0");
$urji->add("Rawat Inap","1","0");

$uitable->clearContent();
$uitable->addModal("dari", "date", "Dari", "")
		->addModal("sampai", "date", "Sampai", "")
		->addModal("urji", "select", "URJ/URI", $urji->getContent())
		->addModal("nama_penyakit", "chooser-laporan_index_penyakit-penyakit_laporan_index_penyakit-penyakit", "Penyakit", "");

$view=new Button("","","View");
$view->setClass("btn-primary")
		->setIsButton(Button::$ICONIC)
		->setIcon("fa fa-refresh")
		->setAction("laporan_index_penyakit.view()");
        
$excel=new Button("","","Excel");
$excel->setClass("btn-primary")
	->setIsButton(Button::$ICONIC)
	->setIcon("fa fa-file-excel-o")
	->setAction("laporan_index_penyakit.excel()");

$btng=new ButtonGroup("");
$btng->addButton($view);
$btng->addButton($excel);

$form=$uitable
	  ->getModal()
	  ->setTitle("Medical Record")
	  ->getForm()
	  ->addElement("",$btng);
      
echo "<h2>Laporan Index Penyakit</h2>";
echo $form->getHtml();
echo $uitable->getHtml();

echo addJS("framework/bootstrap/js/bootstrap-datepicker.js");
echo addJS("framework/smis/js/table_action.js");
echo addCSS("framework/bootstrap/css/datepicker.css");
echo addJS ( "medical_record/resource/js/laporan_index_penyakit.js",false);

?>