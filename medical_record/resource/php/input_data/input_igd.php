<?php

global $db;
require_once ("smis-base/smis-include-service-consumer.php");

if(isset($_POST['super_command'])) {
    $super = new SuperCommand ();
    if($_POST['super_command'] == 'igd_pasien') {
        $aname=array ('Sebutan','Nama','NRM',"No Reg", "Tgl Masuk" );
        $ptable = new Table ( $aname);
        $ptable->setName ( "igd_pasien" );
        $ptable->setModel ( Table::$SELECT );
        $padapter = new SimpleAdapter ();
        $padapter->add ( "Sebutan", "sebutan" );
        $padapter->add ( "Nama", "nama_pasien" );
        $padapter->add ( "NRM", "nrm", "digit8" );
        $padapter->add ( "No Reg", "id" );
        $padapter->add ( "Tgl Masuk", "tanggal", "date d M Y" );
        $presponder = new ServiceResponder ( $db, $ptable, $padapter, "get_registered_all" );
        $super->addResponder ( "igd_pasien", $presponder );
    }
    $init = $super->initialize ();
    if ($init != null) {
        echo $init;
        return;
    }
}

$head=array ('No.','Tanggal','Ditangani','Dikeluarkan','Pasien','No. Reg','NRM','Pola Kasus','Triage','Kelanjutan','Kiriman','Rujukan');
$uitable = new Table ( $head, "", NULL, true );
$uitable->setName ( "input_igd" );

if (isset ( $_POST ['command'] )) {
	$adapter = new SimpleAdapter ();
	$adapter->setUseNumber(true, "No.", "back");
	$adapter->add ( "Tanggal", "tanggal","date d M Y H:i" );
	$adapter->add ( "Ditangani", "ditangani","date d M Y H:i" );
	$adapter->add ( "Dikeluarkan", "dikeluarkan","date d M Y H:i" );
	$adapter->add ( "Pasien", "nama_pasien" );
	$adapter->add ( "No. Reg", "noreg_pasien" );
	$adapter->add ( "NRM", "nrm_pasien" );
	$adapter->add ( "Pola Kasus", "pola_kasus" );
	$adapter->add ( "Triage", "triage" );
	$adapter->add ( "Kelanjutan", "kelanjutan" );
	$adapter->add ( "Kiriman", "kiriman" );
	$adapter->add ( "Rujukan", "rujuk" );
    
	$dbtable = new DBTable ( $db, "smis_mr_igd" );
    
    if(isset ( $_POST ['dari'] ) && $_POST ['dari'] != "") {
        $dbtable->addCustomKriteria ( " tanggal >=", "'".$_POST['dari']."'" );
    }
    if(isset ( $_POST ['sampai'] ) && $_POST ['sampai'] != "") {
        $dbtable->addCustomKriteria ( " tanggal <=", "'".$_POST['sampai']."'" );
    }
    
    $dbtable->setOrder ( " tanggal ASC " );
	$dbres = new DBResponder ( $dbtable, $uitable, $adapter );
	$data = $dbres->command ( $_POST ['command'] );
	echo json_encode ( $data );
	return;
}

loadClass ( "ServiceProviderList" );
$service = new ServiceProviderList ( $db, "push_antrian" );
$service->execute ();
$ruangan = $service->getContent ();
$option = array();
$option['value'] = '';
$option['name'] = '';
$option['default'] = '1';
$ruangan[] = $option;

$kelanjutan = new OptionBuilder ();
$kelanjutan->add ( "", "" );
$kelanjutan->add ( "Rawat Jalan", "RJ" );
$kelanjutan->add ( "Rujuk ke RS Lain", "RJK" );
$kelanjutan->add ( "Masuk Rumah Sakit (Rawat Inap)", "MRS" );

$kiriman = new OptionBuilder ();
$kiriman->add ( "", "" );
$kiriman->add ( "Dokter", "Dokter" );
$kiriman->add ( "PKM/RS", "PKM" );
$kiriman->add ( "PR/BD", "PR/BD" );
$kiriman->add ( "Lainya", "Lain" );

$pola_kasus = new OptionBuilder ();
$pola_kasus->add ( "", "" );
$pola_kasus->add ( "Bedah - Penganiayaan", "Bedah - Penganiayaan" );
$pola_kasus->add ( "Bedah - Kecelakaan Kerja", "Bedah - Kecelakaan Kerja" );
$pola_kasus->add ( "Bedah - KLL", "Bedah - KLL" );
$pola_kasus->add ( "Bedah - Non Trauma", "Bedah - Non Trauma" );
$pola_kasus->add ( "Medis (Non Bedah)", "Medis" );
$pola_kasus->add ( "VK (Kebidanan)", "VK" );
$pola_kasus->add ( "Anak", "Anak" );
$pola_kasus->add ( "Psikiatrik", "Psikiatrik" );

$triage = new OptionBuilder ();
$triage->add ( "", "" );
$triage->add ( "P1 - Pasien Gawat - Merah", "Merah" );
$triage->add ( "P2 - Pasien Darurat - Kuning", "Kuning" );
$triage->add ( "P3 - Pasien Tidak Gawat & Tidak Darurat - Hijau", "Hijau" );
$triage->add ( "P0 - Hitam DOA", "Hitam DOA" );
$triage->add ( "P0 - Hitam Mati", "Hitam Mati" );

$sift = new OptionBuilder ();
$sift->add ( "", "" );
$sift->add ( "Pagi", "Pagi" );
$sift->add ( "Sore", "Sore" );
$sift->add ( "Malam", "Malam" );

$uitable->addModal ( "id", "hidden", "", "" );
$uitable->addModal ( "umur_pasien", "hidden", "", "" );
$uitable->addModal ( "jk", "hidden", "", "" );
$uitable->addModal ( "tanggal", "datetime", "Tanggal", date ( "Y-m-d H:i:s" ), "y", NULL,false  );
$uitable->addModal ( "ditangani", "datetime", "Ditangani", date ( "Y-m-d H:i:s" ), "y", NULL,false  );
$uitable->addModal ( "dikeluarkan", "datetime", "Dikeluarkan", date ( "Y-m-d H:i:s" ), "y", NULL,false  );
$uitable->addModal ( "response", "text", "Response Time (Menit)", "","y",NULL,true  );
$uitable->addModal ( "nama_pasien", "chooser-input_igd-igd_pasien-Pasien", "Nama Pasien", "","n",NULL,true  );
$uitable->addModal ( "sebutan", "hidden", "", "" );
$uitable->addModal ( "nrm_pasien", "text", "NRM", "","y",NULL,true  );
$uitable->addModal ( "profile_number", "hidden", "", "" );
$uitable->addModal ( "noreg_pasien", "text", "No. Reg", "","y",NULL,true  );
$uitable->addModal ( "ruangan", "select", "Ruangan", $ruangan,"y",NULL,true  );
$uitable->addModal ( "sift", "select", "Waktu", $sift->getContent () );
$uitable->addModal ( "pola_kasus", "select", "Pola Kasus", $pola_kasus->getContent () );
$uitable->addModal ( "triage", "select", "Triage", $triage->getContent () );
$uitable->addModal ( "kelanjutan", "select", "Kelanjutan", $kelanjutan->getContent () );
$uitable->addModal ( "kiriman", "select", "Kiriman", $kiriman->getContent () );
$uitable->addModal ( "ds", "checkbox", "DS", $this->ds );
$uitable->addModal ( "rujuk", "text", "Rujukan" );
$uitable->addModal ( "carabayar", "text", "Jenis Pasien", "","y",NULL,true  );
$uitable->addModal ( "menolak_mrs", "checkbox", "Menolak MRS", $the_row ['menolak_mrs']);
$uitable->addModal ( "alat_bantu_hidup", "checkbox", "Menggunakan Alat Bantu Hidup" );
$uitable->addModal ( "tunanetra", "checkbox", "Tunanetra" );
$uitable->addModal ( "tunadaksa", "checkbox", "Tunadaksa" );
$uitable->addModal ( "tunawicara", "checkbox", "Tunawicara" );
$uitable->addModal ( "tunarungu", "checkbox", "Tunarungu" );
$uitable->addModal ( "tunagrahita", "checkbox", "Tunagrahita" );
$uitable->addModal ( "tunalaras", "checkbox", "Tunalaras" );		
$uitable->addModal ( "ver", "checkbox", "Ver" );

$modal = $uitable->getModal ();
$modal->setTitle ( "IGD" );

$tbl=new Table(array());
$tbl->setName("header");
$tbl->addModal("dari", "datetime", "Dari", "");
$tbl->addModal("sampai", "datetime", "Sampai", "");

$form=$tbl->getModal()->getForm();
$btn=new Button("", "", "");
$btn->setClass("btn-primary");
$btn->setIsButton(Button::$ICONIC);
$btn->setIcon("fa fa-refresh");
$btn->setAction("input_igd.view()");
$form->addElement("",$btn);

echo $form->getHtml();
echo $uitable->getHtml ();
echo $modal->getHtml ();

echo addJS ( "framework/smis/js/table_action.js" );
echo addJS ( "framework/bootstrap/js/bootstrap-datetimepicker.js" );
echo addCSS ( "framework/bootstrap/css/bootstrap-datetimepicker.css" );
echo addJS ( "medical_record/resource/js/input_igd.js", false );

?>

<style type="text/css">
#input_igd_add_form_form>div {clear: both; width: 100%; }
#input_igd_add_form_form>div label { 	width: 250px; }
#input_igd_triage>option:nth-child(2){background:#c0392b !important; color:white;}
#input_igd_triage>option:nth-child(3){background:#f39c12 !important;color:white;}
#input_igd_triage>option:nth-child(4){background:#2ecc71 !important;color:white;}
#input_igd_triage>option:nth-child(5){background:#34495e !important;color:white;}
#input_igd_triage>option:nth-child(6){background:#34495e !important;color:white;}
</style>