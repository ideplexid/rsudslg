<?php
	require_once("registration/class/RegistrationResource.php");
	global $db;

	$form = new Form("", "", "");
	$tanggal_from_text = new Text("data_kunjungan_tanggal_from", "data_kunjungan_tanggal_from", "01" . date("-m-Y"));
	$tanggal_from_text->setClass("mydate");
	$tanggal_from_text->setAtribute("data-date-format='dd-mm-yyyy'");
	$form->addElement("Tgl. Dari", $tanggal_from_text);
	$tanggal_to_text = new Text("data_kunjungan_tanggal_to", "data_kunjungan_tanggal_to", date("d-m-Y"));
	$tanggal_to_text->setClass("mydate");
	$tanggal_to_text->setAtribute("data-date-format='dd-mm-yyyy'");
	$form->addElement("Tgl. Sampai", $tanggal_to_text);
	$uri_option = new OptionBuilder();
	$uri_option->add("Semua", "%%", "1");
	$uri_option->add("Rawat Jalan", "0");
	$uri_option->add("Rawat Inap", "1");
	$uri_select = new Select("data_kunjungan_uri", "data_kunjungan_uri", $uri_option->getContent());
	$form->addElement("RJ/RI", $uri_select);
	$ruangan_irja = RegistrationResource::getRuanganDaftar();
	$ruangan_irna = RegistrationResource::getURI($db);
	$ruangan_arr = array_merge($ruangan_irja, $ruangan_irna);
	$ruangan_option = new OptionBuilder();
	$ruangan_option->add("SEMUA", "%%", "1");
	if (count($ruangan_arr) > 0) {
		foreach ($ruangan_arr as $ruangan)
			$ruangan_option->add($ruangan['name'], $ruangan['value']);
	}
	$ruangan_select = new Select("data_kunjungan_ruangan", "data_kunjungan_ruangan", $ruangan_option->getContent());
	$form->addElement("Ruangan", $ruangan_select);
    loadLibrary("smis-libs-function-medical");
    $carapulang_option = medical_carapulang();
    $carapulang_option[] = array(
         'name' 	=> "Semua",
         'value'	=> "%%",
         'default'	=> "0",
         'grup'		=> null
    );
	$carapulang_select = new Select("data_kunjungan_carapulang", "data_kunjungan_carapulang", $carapulang_option);
	$form->addElement("Cara Keluar", $carapulang_select);
	$show_button = new Button("", "", "Tampilkan");
	$show_button->setClass("btn-primary");
	$show_button->setIcon("icon-white icon-repeat");
	$show_button->setIsButton(Button::$ICONIC);
	$show_button->setAction("data_kunjungan.view()");
	$download_button = new Button("", "", "Excel");
	$download_button->setClass("btn-inverse");
	$download_button->setIcon("fa fa-download");
	$download_button->setIsButton(Button::$ICONIC);
	$download_button->setAction("data_kunjungan.export_xls()");
	$button_group = new ButtonGroup("");
	$button_group->addButton($show_button);
	$button_group->addButton($download_button);
	$form->addElement("", $button_group);

	$table = new Table(
		array(
			"No.", 
			"No. Registrasi", 
			"No. RM", 
			"Nama Pasien", 
			"RJ/RI", 
			"Tanggal Masuk", 
			"Cara Masuk", 
			"Ruangan Rawat Jalan", 
			"Jenis Kegiatan",
			"Tanggal Inap (MRS)",
			"Ruangan Inap",
			"Tanggal Keluar (KRS)",
			"Cara Keluar",
			"Lama Dirawat",
			"Cara Bayar",
			"Status",
			"Perubahan Terakhir - Waktu",
			"Perubahan Terakhir - Username"
		),
		"",
		null,
		true
	);
	$table->setHeaderVisible(false);
	$table->setName("data_kunjungan");
	$table->setAddButtonEnable(false);
	$table->setReloadButtonEnable(false);
	$table->setPrintButtonEnable(false);
	$table->addHeader("after", "
		<tr class='inverse'>
			<th rowspan='2' style='vertical-align: middle !important;'>
				<center><small>No.</small></center>
			</th>
			<th rowspan='2' style='vertical-align: middle !important;'>
				<center><small>No. Registrasi</small></center>
			</th>
			<th rowspan='2' style='vertical-align: middle !important;'>
				<center><small>No. RM</small></center>
			</th>
			<th rowspan='2' style='vertical-align: middle !important;'>
				<center><small>Nama Pasien</small></center>
			</th>
			<th rowspan='2' style='vertical-align: middle !important;'>
				<center><small>RJ/RI</small></center>
			</th>
			<th rowspan='2' style='vertical-align: middle !important;'>
				<center><small>Tanggal Masuk</small></center>
			</th>
			<th rowspan='2' style='vertical-align: middle !important;'>
				<center><small>Cara Masuk</small></center>
			</th>
			<th rowspan='2' style='vertical-align: middle !important;'>
				<center><small>Ruangan Rawat Jalan</small></center>
			</th>
			<th rowspan='2' style='vertical-align: middle !important;'>
				<center><small>Jenis Kegiatan</small></center>
			</th>
			<th rowspan='2' style='vertical-align: middle !important;'>
				<center><small>Tanggal Inap (MRS)</small></center>
			</th>
			<th rowspan='2' style='vertical-align: middle !important;'>
				<center><small>Ruangan Inap</small></center>
			</th>
			<th rowspan='2' style='vertical-align: middle !important;'>
				<center><small>Tanggal Keluar (KRS)</small></center>
			</th>
			<th rowspan='2' style='vertical-align: middle !important;'>
				<center><small>Cara Keluar</small></center>
			</th>
			<th rowspan='2' style='vertical-align: middle !important;'>
				<center><small>Lama Dirawat</small></center>
			</th>
			<th rowspan='2' style='vertical-align: middle !important;'>
				<center><small>Cara Bayar</small></center>
			</th>
			<th rowspan='2' style='vertical-align: middle !important;'>
				<center><small>Status</small></center>
			</th>
			<th colspan='2' style='vertical-align: middle !important;'>
				<center><small>Perubahan Terakhir</small></center>
			</th>
			<th rowspan='2'></th>
		</tr>
		<tr class='inverse'>
			<th colspan='1' style='vertical-align: middle !important;'>
				<center><small>Waktu</small></center>
			</th>
			<th colspan='1' style='vertical-align: middle !important;'>
				<center><small>Nama Operator</small></center>
			</th>
		</tr>
	");

	if (isset($_POST['command'])) {
		$adapter = new SimpleAdapter(true, "No.");
		$adapter->add("No. Registrasi", "id");
		$adapter->add("No. RM", "nrm");
		$adapter->add("Nama Pasien", "nama_pasien");
		$adapter->add("RJ/RI", "uri", "trivial_1_Rawat Inap_Rawat Jalan");
		$adapter->add("Tanggal Masuk", "tanggal", "date d-m-Y, H:i:s");
		$adapter->add("Cara Masuk", "caradatang");
		$adapter->add("Ruangan Rawat Jalan", "jenislayanan", "unslug");
		$adapter->add("Jenis Kegiatan", "jenis_kegiatan");
		$adapter->add("Tanggal Inap (MRS)", "tanggal_inap", "date d-m-Y, H:i:s");
		$adapter->add("Ruangan Inap", "kamar_inap", "unslug");
		$adapter->add("Tanggal Keluar (KRS)", "tanggal_pulang", "date d-m-Y, H:i:s");
		$adapter->add("Cara Keluar", "carapulang");
		$adapter->add("Lama Dirawat", "lama_dirawat", "number");
		$adapter->add("Cara Bayar", "carabayar", "unslug");
		$adapter->add("Status", "selesai", "trivial_0_Aktif_Tidak Aktif");
		$adapter->add("Perubahan Terakhir - Waktu", "last_edit_timestamp", "date d-m-Y, H:i");
		$adapter->add("Perubahan Terakhir - Username", "last_edit_username");
		$dbtable = new DBTable($db, "smis_rg_layananpasien");
		if ($_POST['command'] == "list") {
			$filter = " AND DATE(tanggal) >= '" . date("Y-m-d", strtotime($_POST['tanggal_from'])) . "' 
						AND DATE(tanggal) <= '" . date("Y-m-d", strtotime($_POST['tanggal_to'])) . "' 
						AND (
							jenislayanan LIKE '" . $_POST['ruangan'] . "' OR
							kamar_inap LIKE '" . $_POST['ruangan'] . "' OR
							last_ruangan LIKE '" . $_POST['ruangan'] . "'
					  ) AND carapulang LIKE '" . $_POST['carapulang'] . "'
					    AND uri LIKE '" . $_POST['uri'] . "' ";
			if (isset($_POST['kriteria'])) {
				$filter .= " AND (
								id LIKE '%" . $_POST['kriteria'] . "%' OR 
								nrm LIKE '%" . $_POST['kriteria'] . "%' OR 
								nama_pasien LIKE '%" . $_POST['kriteria'] . "%'
							 ) ";
			}
			$query_value = "
				SELECT *
				FROM smis_rg_layananpasien
				WHERE prop = '' " . $filter . "
			";
			$query_count = "
				SELECT COUNT(*)
				FROM (
					" . $query_value . "
				) v
			";
			$dbtable->setPreferredQuery(true, $query_value, $query_count);
		} else if ($_POST['command'] == "save") {
			/// Kalkulasi Lama Dirawat
	        if ($_POST['uri'] == 0) {
	            /// Kalkulasi Lama Dirawat Pasien Rawat Jalan :
	            $tanggal_mulai = strtotime(ArrayAdapter::format("date Y-m-d", $_POST['tanggal']));
	            $tanggal_selesai = strtotime(ArrayAdapter::format("date Y-m-d", $_POST['tanggal_pulang']));
	            $datediff = $tanggal_selesai - $tanggal_mulai;
	            $los = ceil($datediff / (60 * 60 * 24));
	            if ($los < 1)
                    $los = 1;
	            $_POST['lama_dirawat'] = $los;
	        } else {
	            /// Kalkulasi Lama Dirawat Pasien Rawat Inap :
	            $tanggal_mulai = strtotime(ArrayAdapter::format("date Y-m-d", $_POST['tanggal_inap']));
	            $tanggal_selesai = strtotime(ArrayAdapter::format("date Y-m-d", $_POST['tanggal_pulang']));
	            $datediff = $tanggal_selesai - $tanggal_mulai;
	            $los = ceil($datediff / (60 * 60 * 24));
	            if ($los < 1)
                    $los = 1;
	            $_POST['lama_dirawat'] = $los;
	        }
	        $_POST['last_edit_timestamp'] = date("Y-m-d H:i:s");
	        global $user;
	        $_POST['last_edit_username'] = $user->getNameOnly();
		    //. Akhir Kalkulasi Lama Dirawat
		} else if ($_POST['command'] == "cancel") {
			$_POST['command'] = "save";
		}
		$dbresponder = new DBResponder(
			$dbtable,
			$table,
			$adapter
		);
		$data = $dbresponder->command($_POST['command']);
		echo json_encode($data);
		return;
	}

	$modal = new Modal("data_kunjungan_add_form", "smis_form_container", "data_kunjungan");
	$modal->setTitle("Data Kunjungan");
	$id_hidden = new Hidden("kunjungan_id", "kunjungan_id", "");
	$modal->addElement("", $id_hidden);
	$noreg_pasien_text = new Text("kunjungan_noreg_pasien", "kunjungan_noreg_pasien", "");
	$noreg_pasien_text->setAtribute("disabled='disabled'");
	$modal->addElement("No. Registrasi", $noreg_pasien_text);
	$nrm_pasien_text = new Text("kunjungan_nrm_pasien", "kunjungan_nrm_pasien", "");
	$nrm_pasien_text->setAtribute("disabled='disabled'");
	$modal->addElement("No. RM", $nrm_pasien_text);
	$nama_pasien_text = new Text("kunjungan_nama_pasien", "kunjungan_nama_pasien", "");
	$nama_pasien_text->setAtribute("disabled='disabled'");
	$modal->addElement("Nama Pasien", $nama_pasien_text);
	$uri_hidden = new Hidden("kunjungan_uri", "kunjungan_uri", "");
	$modal->addElement("", $uri_hidden);
	$uri_label_text = new Text("kunjungan_uri_label", "kunjungan_uri_label", "");
	$uri_label_text->setAtribute("disabled='disabled'");
	$modal->addElement("RJ/RI", $uri_label_text);
	$tanggal_masuk_text = new Text("kunjungan_tanggal", "kunjungan_tanggal", "");
	$tanggal_masuk_text->setAtribute("disabled='disabled'");
	$modal->addElement("Tgl. Masuk", $tanggal_masuk_text);
	$caramasuk_text = new Text("kunjungan_caramasuk", "kunjungan_caramasuk", "");
	$caramasuk_text->setAtribute("disabled='disabled'");
	$modal->addElement("Cara Masuk", $caramasuk_text);
	$ruangan_rj_text = new Text("kunjungan_ruang_rj", "kunjungan_ruang_rj", "");
	$ruangan_rj_text->setAtribute("disabled='disabled'");
	$modal->addElement("Ruang RJ", $ruangan_rj_text);
	$jenis_kegiatan_select = new Select("kunjungan_jenis_kegiatan", "kunjungan_jenis_kegiatan", get_rl52_option());
	$modal->addElement("Jenis Kegiatan", $jenis_kegiatan_select);
	$tanggal_inap_text = new Text("kunjungan_tanggal_inap", "kunjung_tanggal_inap", "");
	$tanggal_inap_text->setClass("mydatetime");
	$tanggal_inap_text->setAtribute("data-date-format='dd-mm-yyyy hh:ii'");
	$modal->addElement("Tgl. Inap (MRS)", $tanggal_inap_text);
	$ruang_inap_terakhir_text = new Text("kunjungan_last_ruangan", "kunjungan_last_ruangan", "");
	$ruang_inap_terakhir_text->setAtribute("disabled='disabled'");
	$modal->addElement("Ruang Inap Terakhir", $ruang_inap_terakhir_text);
	$tanggal_pulang_text = new Text("kunjungan_tanggal_pulang", "kunjung_tanggal_pulang", "");
	$tanggal_pulang_text->setClass("mydatetime");
	$tanggal_pulang_text->setAtribute("data-date-format='dd-mm-yyyy hh:ii'");
	$modal->addElement("Tgl. Pulang (KRS)", $tanggal_pulang_text);
	$carapulang_option = medical_carapulang();
    $carapulang_option[] = array(
         'name' 	=> "",
         'value'	=> "",
         'default'	=> "0",
         'grup'		=> null
    );
	$carapulang_select = new Select("kunjungan_carapulang", "kunjungan_carapulang", $carapulang_option);
	$modal->addElement("Cara Keluar", $carapulang_select);
	$lama_dirawat_text = new Text("kunjungan_lama_dirawat", "kunjungan_lama_dirawat", "");
	$lama_dirawat_text->setAtribute("disabled='disabled'");
	$modal->addElement("Lama Dirawat", $lama_dirawat_text);
	$carabayar_text = new Text("kunjungan_carabayar", "kunjungan_carabayar", "");
	$carabayar_text->setAtribute("disabled='disabled'");
	$modal->addElement("Cara Bayar", $carabayar_text);
	$status_text = new Text("kunjungan_status", "kunjungan_status", "");
	$status_text->setAtribute("disabled='disabled'");
	$modal->addElement("Status", $status_text);
	$save_button = new Button("", "", "Simpan");
	$save_button->setClass("btn-success");
	$save_button->setAction("data_kunjungan.save()");
	$modal->addFooter($save_button);

	echo $modal->getHtml();
	echo $form->getHtml();
	echo $table->getHtml();
	echo addJS("framework/smis/js/table_action.js");
	echo addJS("framework/bootstrap/js/bootstrap-datepicker.js");
	echo addJS("framework/bootstrap/js/bootstrap-datetimepicker.js");
	echo addJS("medical_record/resource/js/data_kunjungan.js", false);
	echo addCSS("framework/bootstrap/css/datepicker.css");
	echo addCSS("framework/bootstrap/css/bootstrap-datetimepicker.css");
	echo addCSS("medical_record/resource/css/data_kunjungan.css", false);
?>