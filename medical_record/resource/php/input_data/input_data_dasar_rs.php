<?php
	global $db;

	if (isset($_POST['command'])) {
		if ($_POST['command'] == "view") {
			$data = array();
			$dbtable = new DBTable($db, "smis_mr_data_dasar_rs");
			$row = $dbtable->get_row("
				SELECT *
				FROM smis_mr_data_dasar_rs
				WHERE prop = ''
				ORDER BY id DESC
				LIMIT 0, 1
			");
			if ($row != null) {
				$data = array(
					'koders'										=> $row->koders,
					'tanggal_registrasi'							=> ArrayAdapter::format("date d-m-Y", $row->tanggal_registrasi),
					'nama_rs'										=> $row->nama_rs,
					'jenis_rs'										=> $row->jenis_rs,
					'kelas_rs'										=> $row->kelas_rs,
					'kepemilikan_rs'								=> $row->kepemilikan_rs,
					'nama_direktur'									=> $row->nama_direktur,
					'nama_penyelenggara'							=> $row->nama_penyelenggara,
					'status_penyelenggara'							=> $row->status_penyelenggara,
					'jumlah_layanan'								=> $row->jumlah_layanan,
					'jumlah_sdm'									=> $row->jumlah_sdm,
					'jumlah_alat_kesehatan'							=> $row->jumlah_alat_kesehatan,
					'alamat_rs'										=> $row->alamat_rs,
					'kota_kabupaten'								=> $row->kota_kabupaten,							
					'kode_provinsi'									=> $row->kode_provinsi,
					'no_telp'										=> $row->no_telpon,
					'fax'											=> $row->fax,
					'email_rs'										=> $row->email_rs,
					'no_telp_bag_umum'								=> $row->no_telpon_bag_umum,
					'website'										=> $row->website,
					'luas_tanah'									=> $row->luas_tanah,
					'luas_bangunan'									=> $row->luas_bangunan,
					'nomor_surat_penetapan_kelas'					=> $row->nomor_surat_penetapan_kelas,
					'masa_berlaku_surat_penetapan_kelas'			=> ArrayAdapter::format("date d-m-Y", $row->masa_berlaku_surat_penetapan_kelas),
					'versi_akreditasi'								=> $row->versi_akreditasi,
					'status_akreditasi'								=> $row->status_akreditasi,
					'masa_berlaku_akreditasi'						=> ArrayAdapter::format("date d-m-Y", $row->masa_berlaku_akreditasi),
					'ambulan_transportasi_baik'						=> $row->ambulan_transportasi_baik,
					'ambulan_transportasi_rusak_ringan'				=> $row->ambulan_transportasi_rusak_ringan,
					'ambulan_transportasi_rusak_berat'				=> $row->ambulan_transportasi_rusak_berat,
					'ambulan_jenasah_baik'							=> $row->ambulan_jenasah_baik,
					'ambulan_jenasah_rusak_ringan'					=> $row->ambulan_jenasah_rusak_ringan,
					'ambulan_jenasah_rusak_berat'					=> $row->ambulan_jenasah_rusak_berat,
					'nomor_surat_izin_operasional_rs'				=> $row->nomor_surat_izin_operasional_rs,
					'tanggal_surat_izin_operasional_rs'				=> ArrayAdapter::format("date d-m-Y", $row->tanggal_surat_izin_operasional_rs),
					'oleh_izin_operasional_rs'						=> $row->oleh_izin_operasional_rs,
					'masa_berlaku_surat_izin_operasional_rs'		=> ArrayAdapter::format("date d-m-Y", $row->masa_berlaku_surat_izin_operasional_rs),
					'sifat_izin_operasional_rs'						=> $row->sifat_izin_operasional_rs,
					'nomor_surat_izin_hemodialisa'					=> $row->nomor_surat_izin_hemodialisa,
					'tanggal_surat_izin_hemodialisa'				=> ArrayAdapter::format("date d-m-Y", $row->tanggal_surat_izin_hemodialisa),
					'oleh_izin_hemodialisa'							=> $row->oleh_izin_hemodialisa,
					'masa_berlaku_surat_izin_hemodialisa'			=> ArrayAdapter::format("date d-m-Y", $row->masa_berlaku_surat_izin_hemodialisa),
					'sifat_izin_hemodialisa'						=> $row->sifat_izin_hemodialisa,
					'jml_tempat_tidur_perinatologi'					=> $row->jml_tempat_tidur_perinatologi,
					'jml_tempat_tidur_kelas_vvip'					=> $row->jml_tempat_tidur_kelas_vvip,
					'jml_tempat_tidur_kelas_vip'					=> $row->jml_tempat_tidur_kelas_vip,
					'jml_tempat_tidur_kelas_i'						=> $row->jml_tempat_tidur_kelas_i,
					'jml_tempat_tidur_kelas_ii'						=> $row->jml_tempat_tidur_kelas_ii,
					'jml_tempat_tidur_kelas_iii'					=> $row->jml_tempat_tidur_kelas_iii,
					'jml_tempat_tidur_icu'							=> $row->jml_tempat_tidur_icu,
					'jml_tempat_tidur_picu'							=> $row->jml_tempat_tidur_picu,
					'jml_tempat_tidur_nicu'							=> $row->jml_tempat_tidur_nicu,
					'jml_tempat_tidur_iccu'							=> $row->jml_tempat_tidur_iccu,
					'jml_tempat_tidur_hcu'							=> $row->jml_tempat_tidur_hcu,
					'jml_tempat_tidur_ruang_isolasi'				=> $row->jml_tempat_tidur_ruang_isolasi,
					'kelengkapan_simrs'								=> $row->kelengkapan_simrs,
					'kelengkapan_bank_darah'						=> $row->kelengkapan_bank_darah,
					'kelengkapan_layanan_unggulan'					=> $row->kelengkapan_layanan_unggulan,
					'kelengkapan_peralatan_canggih'					=> $row->kelengkapan_peralatan_canggih,
					'kelengkapan_kerjasama_bpjs'					=> $row->kelengkapan_kerjasama_bpjs,
					'kelengkapan_blud'								=> $row->kelengkapan_blud,
					'kelengkapan_kemampuan_pelayanan_gawat_darurat'	=> $row->kelengkapan_kemampuan_pelayanan_gawat_darurat
				);
			}
			echo json_encode($data);
		} else if ($_POST['command'] == "save") {
			$response_package = new ResponsePackage();
			$dbtable = new DBTable($db, "smis_mr_data_dasar_rs");
			$data = array(
				'koders'										=> $_POST['koders'],
				'tanggal_registrasi'							=> date_format(DateTime::createFromFormat("d-m-Y", $_POST['tanggal_registrasi']), "Y-m-d"),
				'nama_rs'										=> $_POST['nama_rs'],
				'jenis_rs'										=> $_POST['jenis_rs'],
				'kelas_rs'										=> $_POST['kelas_rs'],
				'kepemilikan_rs'								=> $_POST['kepemilikan_rs'],
				'nama_direktur'									=> $_POST['nama_direktur'],
				'nama_penyelenggara'							=> $_POST['nama_penyelenggara'],
				'status_penyelenggara'							=> $_POST['status_penyelenggara'],
				'jumlah_layanan'								=> $_POST['jumlah_layanan'],
				'jumlah_sdm'									=> $_POST['jumlah_sdm'],
				'jumlah_alat_kesehatan'							=> $_POST['jumlah_alat_kesehatan'],
				'alamat_rs'										=> $_POST['alamat_rs'],
				'kota_kabupaten'								=> $_POST['kota_kabupaten'],							
				'kode_provinsi'									=> $_POST['kode_provinsi'],
				'no_telpon'										=> $_POST['no_telp'],
				'fax'											=> $_POST['fax'],
				'email_rs'										=> $_POST['email_rs'],
				'no_telpon_bag_umum'							=> $_POST['no_telp_bag_umum'],
				'website'										=> $_POST['website'],
				'luas_tanah'									=> $_POST['luas_tanah'],
				'luas_bangunan'									=> $_POST['luas_bangunan'],
				'nomor_surat_penetapan_kelas'					=> $_POST['nomor_penetapan_kelas'],
				'masa_berlaku_surat_penetapan_kelas'			=> date_format(DateTime::createFromFormat("d-m-Y", $_POST['masa_berlaku_penetapan_kelas']), "Y-m-d"),
				'versi_akreditasi'								=> $_POST['versi_akreditasi'],
				'status_akreditasi'								=> $_POST['status_akreditasi'],
				'masa_berlaku_akreditasi'						=> date_format(DateTime::createFromFormat("d-m-Y", $_POST['masa_berlaku_akreditasi']), "Y-m-d"),
				'ambulan_transportasi_baik'						=> $_POST['ambulan_transportasi_baik'],
				'ambulan_transportasi_rusak_ringan'				=> $_POST['ambulan_transportasi_rusak_ringan'],
				'ambulan_transportasi_rusak_berat'				=> $_POST['ambulan_transportasi_rusak_berat'],
				'ambulan_jenasah_baik'							=> $_POST['ambulan_jenasah_baik'],
				'ambulan_jenasah_rusak_ringan'					=> $_POST['ambulan_jenasah_rusak_ringan'],
				'ambulan_jenasah_rusak_berat'					=> $_POST['ambulan_jenasah_rusak_berat'],
				'nomor_surat_izin_operasional_rs'				=> $_POST['nomor_surat_izin_operasional_rs'],
				'tanggal_surat_izin_operasional_rs'				=> date_format(DateTime::createFromFormat("d-m-Y", $_POST['tanggal_surat_izin_operasional_rs']), "Y-m-d"),
				'oleh_izin_operasional_rs'						=> $_POST['oleh_izin_operasional_rs'],
				'masa_berlaku_surat_izin_operasional_rs'		=> date_format(DateTime::createFromFormat("d-m-Y", $_POST['masa_berlaku_surat_izin_operasional_rs']), "Y-m-d"),
				'sifat_izin_operasional_rs'						=> $_POST['sifat_izin_operasional_rs'],
				'nomor_surat_izin_hemodialisa'					=> $_POST['nomor_surat_izin_hemodialisa'],
				'tanggal_surat_izin_hemodialisa'				=> date_format(DateTime::createFromFormat("d-m-Y", $_POST['tanggal_surat_izin_hemodialisa']), "Y-m-d"),
				'oleh_izin_hemodialisa'							=> $_POST['oleh_izin_hemodialisa'],
				'masa_berlaku_surat_izin_hemodialisa'			=> date_format(DateTime::createFromFormat("d-m-Y", $_POST['masa_berlaku_surat_izin_hemodialisa']), "Y-m-d"),
				'sifat_izin_hemodialisa'						=> $_POST['sifat_izin_hemodialisa'],
				'jml_tempat_tidur_perinatologi'					=> $_POST['jml_tempat_tidur_perinatologi'],
				'jml_tempat_tidur_kelas_vvip'					=> $_POST['jml_tempat_tidur_kelas_vvip'],
				'jml_tempat_tidur_kelas_vip'					=> $_POST['jml_tempat_tidur_kelas_vip'],
				'jml_tempat_tidur_kelas_i'						=> $_POST['jml_tempat_tidur_kelas_i'],
				'jml_tempat_tidur_kelas_ii'						=> $_POST['jml_tempat_tidur_kelas_ii'],
				'jml_tempat_tidur_kelas_iii'					=> $_POST['jml_tempat_tidur_kelas_iii'],
				'jml_tempat_tidur_icu'							=> $_POST['jml_tempat_tidur_icu'],
				'jml_tempat_tidur_picu'							=> $_POST['jml_tempat_tidur_picu'],
				'jml_tempat_tidur_nicu'							=> $_POST['jml_tempat_tidur_nicu'],
				'jml_tempat_tidur_iccu'							=> $_POST['jml_tempat_tidur_iccu'],
				'jml_tempat_tidur_hcu'							=> $_POST['jml_tempat_tidur_hcu'],
				'jml_tempat_tidur_ruang_isolasi'				=> $_POST['jml_tempat_tidur_ruang_isolasi'],
				'kelengkapan_simrs'								=> $_POST['kelengkapan_simrs'],
				'kelengkapan_bank_darah'						=> $_POST['kelengkapan_bank_darah'],
				'kelengkapan_layanan_unggulan'					=> $_POST['kelengkapan_layanan_unggulan'],
				'kelengkapan_peralatan_canggih'					=> $_POST['kelengkapan_peralatan_canggih'],
				'kelengkapan_kerjasama_bpjs'					=> $_POST['kelengkapan_kerjasama_bpjs'],
				'kelengkapan_blud'								=> $_POST['kelengkapan_blud'],
				'kelengkapan_kemampuan_pelayanan_gawat_darurat'	=> $_POST['kelengkapan_kemampuan_pelayanan_gawat_darurat']
			);

			$row = $dbtable->get_row("
				SELECT *
				FROM smis_mr_data_dasar_rs
				WHERE prop = ''
				LIMIT 0, 1
			");
			$success = false;
			$response_package->setAlertContent("Proses Gagal", "Data Gagal Disimpan", ResponsePackage::$TIPE_DANGER);
			$action = "unknown";
			if ($row != null) {
				$id['id'] = $row->id;
				$dbtable->update($data, $id);
				$action = "update";
				$success = true;
				$response_package->setAlertContent("Proses Berhasil", "Data Telah Diperbarui", ResponsePackage::$TIPE_INFO);
			} else {
				$dbtable->insert($data);
				$action = "insert";
				$success = true;
				$response_package->setAlertContent("Proses Berhasil", "Data Telah Disimpan", ResponsePackage::$TIPE_INFO);
			}

			$response_package->setContent(
				array(
					'success' => $success, 
					'action' => $action
				)
			);
			$response_package->setStatus(ResponsePackage::$STATUS_OK);
			$response_package->setAlertVisible(true);
			echo json_encode($response_package->getPackage());
		}
		return;
	}

	$informasi_rs_form = new Form("ddrs_informasi_rs", "", "Informasi RS");
	$kode_rs_text = new Text("ddrs_kode_rs", "ddrs_kode_rs", "");
	$informasi_rs_form->addElement("Kode RS", $kode_rs_text);
	$tanggal_registrasi_text = new Text("ddrs_tanggal_registrasi", "ddrs_tanggal_registrasi", "");
	$tanggal_registrasi_text->setAtribute("data-date-format='dd-mm-yyyy'");
	$tanggal_registrasi_text->setClass("mydate");
	$informasi_rs_form->addElement("Tgl. Registrasi", $tanggal_registrasi_text);
	$nama_rs_text = new Text("ddrs_nama_rs", "ddrs_nama_rs", "");
	$informasi_rs_form->addElement("Nama RS", $nama_rs_text);
	$jenis_rs_rows = $db->get_result("
		SELECT *
		FROM smis_mr_jenis_rs
		WHERE prop = ''
		ORDER BY jenis_rs ASC
	");
	$jenis_rs_option = new OptionBuilder();
	$jenis_rs_option->add("", "", "1");
	if ($jenis_rs_rows != null)
		foreach ($jenis_rs_rows as $jenis_rs_row)
			$jenis_rs_option->addSingle($jenis_rs_row->jenis_rs);
	$jenis_rs_select = new Select("ddrs_jenis_rs", "ddrs_jenis_rs", $jenis_rs_option->getContent());
	$informasi_rs_form->addElement("Jenis RS", $jenis_rs_select);
	$kelas_rs_option = new OptionBuilder();
	$kelas_rs_option->add("", "", "1");
	$kelas_rs_option->addSingle("A");
	$kelas_rs_option->addSingle("B");
	$kelas_rs_option->addSingle("C");
	$kelas_rs_option->addSingle("D");
	$kelas_rs_option->addSingle("1");
	$kelas_rs_option->addSingle("2");
	$kelas_rs_option->addSingle("3");
	$kelas_rs_option->addSingle("4");
	$kelas_rs_option->addSingle("Belum Ditetapkan");
	$kelas_rs_select = new Select("ddrs_kelas_rs", "ddrs_kelas_rs", $kelas_rs_option->getContent());
	$informasi_rs_form->addElement("Kelas RS", $kelas_rs_select);
	$kepemilikan_rs_rows = $db->get_result("
		SELECT *
		FROM smis_mr_kepemilikan_rs
		WHERE prop = ''
		ORDER BY kepemilikan ASC
	");
	$kepemilikan_rs_option = new OptionBuilder();
	$kepemilikan_rs_option->add("", "", "1");
	if ($kepemilikan_rs_rows != null)
		foreach ($kepemilikan_rs_rows as $kepemilikan_rs_row)
			$kepemilikan_rs_option->addSingle($kepemilikan_rs_row->kepemilikan);
	$kepemilikan_rs_select = new Select("ddrs_kepemilikan_rs", "ddrs_kepemilikan_rs", $kepemilikan_rs_option->getContent());
	$informasi_rs_form->addElement("Kepemilikan", $kepemilikan_rs_select);
	$nama_direktur_rs_text = new Text("ddrs_nama_direktur_rs", "ddrs_nama_direktur_rs", "");
	$informasi_rs_form->addElement("Nama Direktur", $nama_direktur_rs_text);
	$penyelenggara_rs_rows = $db->get_result("
		SELECT *
		FROM smis_mr_penyelenggara_rs
		WHERE prop = ''
		ORDER BY penyelenggara ASC
	");
	$penyelenggara_rs_option = new OptionBuilder();
	$penyelenggara_rs_option->add("", "", "1");
	if ($penyelenggara_rs_rows != null)
		foreach ($penyelenggara_rs_rows as $penyelenggara_rs_row)
			$penyelenggara_rs_option->addSingle($penyelenggara_rs_row->penyelenggara);
	$penyelenggara_rs_select = new Select("ddrs_penyelenggara_rs", "ddrs_penyelenggara_rs", $penyelenggara_rs_option->getContent());
	$informasi_rs_form->addElement("Penyelenggara", $penyelenggara_rs_select);
	$status_penyelenggara_text = new Text("ddrs_status_penyelenggara", "ddrs_status_penyelenggara", "");
	$informasi_rs_form->addElement("Status Penyelenggara", $status_penyelenggara_text);
	$jumlah_layanan_text = new Text("ddrs_jumlah_layanan", "ddrs_jumlah_layanan", "");
	$informasi_rs_form->addElement("Jml. Layanan", $jumlah_layanan_text);
	$jumlah_sdm_text = new Text("ddrs_jumlah_sdm", "ddrs_jumlah_sdm", "");
	$informasi_rs_form->addElement("Jml. SDM", $jumlah_sdm_text);
	$jumlah_alkes_text = new Text("ddrs_jumlah_alkes", "ddrs_jumlah_alkes", "");
	$informasi_rs_form->addElement("Jml. Alat Kesehatan", $jumlah_alkes_text);

	$lokasi_rs_form = new Form("ddrs_alamat_lokasi_rs", "", "Alamat/Lokasi RS");
	$alamat_rs_text = new Text("ddrs_alamat_rs", "ddrs_alamat_rs", "");
	$kota_kab_text = new Text("ddrs_kota_kab", "ddrs_kota_kab", "");
	$kode_provinsi_text = new Text("ddrs_kode_provinsi", "ddrs_kode_provinsi", "");
	$telepon_text = new Text("ddrs_telepon", "ddrs_telepon", "");
	$fax_text = new Text("ddrs_fax", "ddrs_fax", "");
	$email_text = new Text("ddrs_email", "ddrs_email", "");
	$telp_bag_umum_text = new Text("ddrs_telp_bag_umum", "ddrs_telp_bag_umum", "");
	$website_text = new Text("ddrs_website", "ddrs_website", "");
	$lokasi_rs_form->addElement("", "
		<div id='ddrs_info_lain_rs' class='clear'>
			<div class='panel panel-default'>
				<div class='panel-body'>
					<div class='ddrs_alamat_rs'> 
						<label>Alamat RS</label>
						" . $alamat_rs_text->getHtml() . "
					</div>
					<div class='ddrs_kota_kab'> 
						<label>Kota/Kab.</label>
						" . $kota_kab_text->getHtml() . "
					</div>
					<div class='ddrs_kode_provinsi'> 
						<label>Kode Prov.</label>
						" . $kode_provinsi_text->getHtml() . "
					</div>
					<div class='ddrs_telepon'> 
						<label>No. Telp.</label>
						" . $telepon_text->getHtml() . "
					</div>
					<div class='ddrs_fax'> 
						<label>Fax</label>
						" . $fax_text->getHtml() . "
					</div>
					<div class='ddrs_email'> 
						<label>Email</label>
						" . $email_text->getHtml() . "
					</div>
					<div class='ddrs_telp_bag_umum'> 
						<label>Telp. Bag. Umum</label>
						" . $telp_bag_umum_text->getHtml() . "
					</div>
					<div class='ddrs_website'> 
						<label>Website</label>
						" . $website_text->getHtml() . "
					</div>
				</div>
			</div>
		</div>
	");
	$luas_tanah_text = new Text("ddrs_luas_tanah", "ddrs_luas_tanah", "");
	$luas_bangunan_text = new Text("ddrs_luas_bangunan", "ddrs_luas_bangunan", "");
	$lokasi_rs_form->addElement("", "
		<div id='ddrs_luas_rs' class='clear'>
			<div class='panel panel-default'>
				<div class='panel-heading'>Luas Rumah Sakit</div>
				<div class='panel-body'>
					<div class='ddrs_luas_tanah'> 
						<label>Luas Tanah (m<sup>2</sup>)</label>
						" . $luas_tanah_text->getHtml() . "
					</div>
					<div class='ddrs_bangunan_tanah'> 
						<label>Luas Bangunan (m<sup>2</sup>)</label>
						" . $luas_bangunan_text->getHtml() . "
					</div>
				</div>
			</div>
		</div>
	");

	$penetapan_kelas_form = new Form("ddrs_penetapan_kelas", "", "Penetapan Kelas");
	$nomor_text = new Text("ddrs_nomor_penetapan_kelas", "ddrs_nomor_penetapan_kelas", "");
	$penetapan_kelas_form->addElement("Nomor", $nomor_text);
	$masa_berlaku_text = new Text("ddrs_masa_berlaku_penetapan_kelas", "ddrs_masa_berlaku_penetapan_kelas", "");
	$masa_berlaku_text->setAtribute("data-date-format='dd-mm-yyyy'");
	$masa_berlaku_text->setClass("mydate");
	$penetapan_kelas_form->addElement("Masa Berlaku", $masa_berlaku_text);

	$akreditasi_rs_form = new Form("ddrs_akreditasi_rs", "", "Akreditasi RS");
	$edisi_option = new OptionBuilder();
	$edisi_option->add("", "", "1");
	$edisi_option->addSingle("Versi 12");
	$edisi_option->addSingle("SNARS");
	$edisi_select = new Select("ddrs_edisi", "ddrs_edisi", $edisi_option->getContent());
	$akreditasi_rs_form->addElement("Versi", $edisi_select);
	$status_text = new Text("ddrs_status", "ddrs_status", "");
	$akreditasi_rs_form->addElement("Status", $status_text);
	$masa_berlaku_text = new Text("ddrs_masa_berlaku_akreditasi", "ddrs_masa_berlaku_akreditasi", "");
	$masa_berlaku_text->setAtribute("data-date-format='dd-mm-yyyy'");
	$masa_berlaku_text->setClass("mydate");
	$akreditasi_rs_form->addElement("Masa Berlaku", $masa_berlaku_text);

	$jumlah_ambulance_form = new Form("ddrs_jumlah_ambulance", "", "Jumlah Ambulance");
	$kondisi_baik_text = new Text("ddrs_at_kondisi_baik", "ddrs_at_kondisi_baik", "");
	$kondisi_rusak_ringan_text = new Text("ddrs_at_kondisi_rusak_ringan", "ddrs_at_kondisi_rusak_ringan", "");
	$kondisi_rusak_berat_text = new Text("ddrs_at_kondisi_rusak_berat", "ddrs_at_kondisi_rusak_berat", "");
	$jumlah_ambulance_form->addElement("", "
		<div id='ddrs_ambulance_transportasi' class='clear'>
			<div class='panel panel-default'>
				<div class='panel-heading'>Ambulance Transportasi</div>
				<div class='panel-body'>
					<div class='ddrs_kondisi_baik'> 
						<label>Kondisi Baik</label>
						" . $kondisi_baik_text->getHtml() . "
					</div>
					<div class='ddrs_kondisi_rusak_ringan'> 
						<label>Kondisi Rusak Ringan</label>
						" . $kondisi_rusak_ringan_text->getHtml() . "
					</div>
					<div class='ddrs_kondisi_rusak_berat'> 
						<label>Kondisi Rusak Berat</label>
						" . $kondisi_rusak_berat_text->getHtml() . "
					</div>
				</div>
			</div>	
		</div>
	");
	$kondisi_baik_text = new Text("ddrs_aj_kondisi_baik", "ddrs_aj_kondisi_baik", "");
	$kondisi_rusak_ringan_text = new Text("ddrs_aj_kondisi_rusak_ringan", "ddrs_aj_kondisi_rusak_ringan", "");
	$kondisi_rusak_berat_text = new Text("ddrs_aj_kondisi_rusak_berat", "ddrs_aj_kondisi_rusak_berat", "");
	$jumlah_ambulance_form->addElement("", "
		<div id='ddrs_ambulance_jenazah' class='clear'>
			<div class='panel panel-default'>
				<div class='panel-heading'>Ambulance Jenazah</div>
				<div class='panel-body'>
					<div class='ddrs_kondisi_baik'> 
						<label>Kondisi Baik</label>
						" . $kondisi_baik_text->getHtml() . "
					</div>
					<div class='ddrs_kondisi_rusak_ringan'> 
						<label>Kondisi Rusak Ringan</label>
						" . $kondisi_rusak_ringan_text->getHtml() . "
					</div>
					<div class='ddrs_kondisi_rusak_berat'> 
						<label>Kondisi Rusak Berat</label>
						" . $kondisi_rusak_berat_text->getHtml() . "
					</div>
				</div>
			</div>	
		</div>
	");

	$surat_izin_operasional_form = new Form("ddrs_surat_izin_operasional", "", "Surat Izin Operasional");
	$nomor_text = new Text("ddrs_nomor_sio", "ddrs_nomor_sio", "");
	$tanggal_text = new Text("ddrs_tanggal_sio", "ddrs_tanggal_sio", "");
	$tanggal_text->setAtribute("data-date-format='dd-mm-yyyy'");
	$tanggal_text->setClass("mydate");
	$oleh_text = new Text("ddrs_oleh_sio", "ddrs_oleh_sio", "");
	$sifat_text = new Text("ddrs_sifat_sio", "ddrs_sifat_sio", "");
	$masa_berlaku_text = new Text("ddrs_masa_berlaku_sio", "ddrs_masa_berlaku_sio", "");
	$masa_berlaku_text->setAtribute("data-date-format='dd-mm-yyyy'");
	$masa_berlaku_text->setClass("mydate");
	$surat_izin_operasional_form->addElement("", "
		<div id='ddrs_surat_izin_operasional_rumah_sakit' class='clear'>
			<div class='panel panel-default'>
				<div class='panel-heading'>Surat Izin Operasional Rumah Sakit</div>
				<div class='panel-body'>
					<div class='ddrs_nomor_sio'> 
						<label>Nomor</label>
						" . $nomor_text->getHtml() . "
					</div>
					<div class='ddrs_tanggal_sio'> 
						<label>Tanggal</label>
						" . $tanggal_text->getHtml() . "
					</div>
					<div class='ddrs_oleh_sio'> 
						<label>Oleh</label>
						" . $oleh_text->getHtml() . "
					</div>
					<div class='ddrs_sifat_sio'> 
						<label>Sifat</label>
						" . $sifat_text->getHtml() . "
					</div>
					<div class='ddrs_masa_berlaku_sio'> 
						<label>Masa Berlaku</label>
						" . $masa_berlaku_text->getHtml() . "
					</div>
				</div>
			</div>	
		</div>
	");	
	$nomor_text = new Text("ddrs_nomor_sioh", "ddrs_nomor_sioh", "");
	$tanggal_text = new Text("ddrs_tanggal_sioh", "ddrs_tanggal_sioh", "");
	$tanggal_text->setAtribute("data-date-format='dd-mm-yyyy'");
	$tanggal_text->setClass("mydate");
	$oleh_text = new Text("ddrs_oleh_sioh", "ddrs_oleh_sioh", "");
	$sifat_text = new Text("ddrs_sifat_sioh", "ddrs_sifat_sioh", "");
	$masa_berlaku_text = new Text("ddrs_masa_berlaku_sioh", "ddrs_masa_berlaku_sioh", "");
	$masa_berlaku_text->setAtribute("data-date-format='dd-mm-yyyy'");
	$masa_berlaku_text->setClass("mydate");
	$surat_izin_operasional_form->addElement("", "
		<div id='ddrs_surat_izin_operasional_hemodialisa' class='clear'>
			<div class='panel panel-default'>
				<div class='panel-heading'>Surat Izin Operasional Hemodialisa</div>
				<div class='panel-body'>
					<div class='ddrs_nomor_sioh'> 
						<label>Nomor</label>
						" . $nomor_text->getHtml() . "
					</div>
					<div class='ddrs_tanggal_sioh'> 
						<label>Tanggal</label>
						" . $tanggal_text->getHtml() . "
					</div>
					<div class='ddrs_oleh_sioh'> 
						<label>Oleh</label>
						" . $oleh_text->getHtml() . "
					</div>
					<div class='ddrs_sifat_sioh'> 
						<label>Sifat</label>
						" . $sifat_text->getHtml() . "
					</div>
					<div class='ddrs_masa_berlaku_sioh'> 
						<label>Masa Berlaku</label>
						" . $masa_berlaku_text->getHtml() . "
					</div>
				</div>
			</div>	
		</div>
	");

	$jumlah_tempat_tidur_form = new Form("ddrs_jumlah_tempat_tidur", "", "Jumlah Tempat Tidur");
	$jtt_perinatologi_text = new Text("ddrs_jtt_perinatologi", "ddrs_jtt_perinatologi", "");
	$jumlah_tempat_tidur_form->addElement("Perinatologi", $jtt_perinatologi_text);
	$jtt_kelas_vvip_text = new Text("ddrs_jtt_kelas_vvip", "ddrs_jtt_kelas_vvip", "");
	$jumlah_tempat_tidur_form->addElement("Kelas VVIP", $jtt_kelas_vvip_text);
	$jtt_kelas_vip_text = new Text("ddrs_jtt_kelas_vip", "ddrs_jtt_kelas_vip", "");
	$jumlah_tempat_tidur_form->addElement("Kelas VIP", $jtt_kelas_vip_text);
	$jtt_kelas_i_text = new Text("ddrs_jtt_kelas_i", "ddrs_jtt_kelas_i", "");
	$jumlah_tempat_tidur_form->addElement("Kelas I", $jtt_kelas_i_text);
	$jtt_kelas_ii_text = new Text("ddrs_jtt_kelas_ii", "ddrs_jtt_kelas_ii", "");
	$jumlah_tempat_tidur_form->addElement("Kelas II", $jtt_kelas_ii_text);
	$jtt_kelas_iii_text = new Text("ddrs_jtt_kelas_iii", "ddrs_jtt_kelas_iii", "");
	$jumlah_tempat_tidur_form->addElement("Kelas III", $jtt_kelas_iii_text);
	$jtt_icu_text = new Text("ddrs_jtt_icu", "ddrs_jtt_icu", "");
	$jumlah_tempat_tidur_form->addElement("ICU", $jtt_icu_text);
	$jtt_picu_text = new Text("ddrs_jtt_picu", "ddrs_jtt_picu", "");
	$jumlah_tempat_tidur_form->addElement("PICU", $jtt_picu_text);
	$jtt_nicu_text = new Text("ddrs_jtt_nicu", "ddrs_jtt_nicu", "");
	$jumlah_tempat_tidur_form->addElement("NICU", $jtt_nicu_text);
	$jtt_iccu_text = new Text("ddrs_jtt_iccu", "ddrs_jtt_iccu", "");
	$jumlah_tempat_tidur_form->addElement("ICCU", $jtt_iccu_text);
	$jtt_hcu_text = new Text("ddrs_jtt_hcu", "ddrs_jtt_hcu", "");
	$jumlah_tempat_tidur_form->addElement("HCU", $jtt_hcu_text);
	$jtt_ruang_isolasi_text = new Text("ddrs_jtt_ruang_isolasi", "ddrs_jtt_ruang_isolasi", "");
	$jumlah_tempat_tidur_form->addElement("Ruang Isolasi", $jtt_ruang_isolasi_text);

	$kelengkapan_form = new Form("ddrs_kelengkapan", "", "Kelengkapan");
	$simrs_text = new Text("ddrs_simrs", "ddrs_simrs", "");
	$kelengkapan_form->addElement("SIMRS", $simrs_text);
	$bank_darah_text = new Text("ddrs_bank_darah", "ddrs_bank_darah", "");
	$kelengkapan_form->addElement("Bank Darah/UTDRS", $bank_darah_text);
	$layanan_unggulan_text = new Text("ddrs_layanan_unggulan", "ddrs_layanan_unggulan", "");
	$kelengkapan_form->addElement("Layanan Unggulan", $layanan_unggulan_text);
	$peralatan_canggih_text = new Text("ddrs_peralatan_canggih", "ddrs_peralatan_canggih", "");
	$kelengkapan_form->addElement("Peralatan Canggih", $peralatan_canggih_text);
	$ks_bpjs_text = new Text("ddrs_ks_bpjs", "ddrs_ks_bpjs", "");
	$kelengkapan_form->addElement("Kerja Sama BPJS", $ks_bpjs_text);
	$blud_text = new Text("ddrs_blud", "ddrs_blud", "");
	$kelengkapan_form->addElement("BLUD", $blud_text);
	$klgd_option = new OptionBuilder();
	$klgd_option->add("", "", "1");
	$klgd_option->addSingle("Level 1");
	$klgd_option->addSingle("Level 2");
	$klgd_option->addSingle("Level 3");
	$klgd_option->addSingle("Level 4");
	$klgd_option->addSingle("Level 5");
	$klgd_select = new Select("ddrs_klgd", "ddrs_klgd", $klgd_option->getContent());
	$kelengkapan_form->addElement("Kemampuan Pelayanan Gawat Darurat", $klgd_select);

	$save_button = new Button("", "", "Simpan");
	$save_button->setClass("btn-success");
	$save_button->setAction("ddrs.save()");
	$save_button->setIcon("fa fa-floppy-o");
	$save_button->setIsButton(Button::$ICONIC);

	echo "<div class='clear'>";
	echo $informasi_rs_form->getHtml();
	echo "</div>";
	echo "<div class='clear'>";
	echo $lokasi_rs_form->getHtml();
	echo "</div>";
	echo "<div class='clear'>";
	echo $penetapan_kelas_form->getHtml();
	echo "</div>";
	echo "<div class='clear'>";
	echo $akreditasi_rs_form->getHtml();
	echo "</div>";
	echo "<div class='clear'>";
	echo $jumlah_ambulance_form->getHtml();
	echo "</div>";
	echo "<div class='clear'>";
	echo $surat_izin_operasional_form->getHtml();
	echo "</div>";
	echo "<div class='clear'>";
	echo $jumlah_tempat_tidur_form->getHtml();
	echo "</div>";
	echo "<div class='clear'>";
	echo $kelengkapan_form->getHtml();
	echo "<p class='pull-right'>" . $save_button->getHtml() . "</p>";
	echo "</div>";
	echo addJS("framework/bootstrap/js/bootstrap-datepicker.js");
	echo addCSS("framework/bootstrap/css/datepicker.css");
	echo addJS("framework/smis/js/table_action.js");
?>
<script type="text/javascript">
	function DDRSAction(name, page, action, column) {
		this.initialize(name, page, action, column);
	}
	DDRSAction.prototype.constructor = DDRSAction;
	DDRSAction.prototype = new TableAction();
	DDRSAction.prototype.getSaveData = function() {
		var data = this.getRegulerData();

		data['koders'] = $("#ddrs_kode_rs").val();
		data['tanggal_registrasi'] = $("#ddrs_tanggal_registrasi").val();
		data['nama_rs'] = $("#ddrs_nama_rs").val();
		data['jenis_rs'] = $("#ddrs_jenis_rs").val();
		data['kelas_rs'] = $("#ddrs_kelas_rs").val();
		data['kepemilikan_rs'] = $("#ddrs_kepemilikan_rs").val();
		data['nama_direktur'] = $("#ddrs_nama_direktur_rs").val();
		data['nama_penyelenggara'] = $("#ddrs_penyelenggara_rs").val();
		data['status_penyelenggara'] = $("#ddrs_status_penyelenggara").val();
		data['jumlah_layanan'] = $("#ddrs_jumlah_layanan").val();
		data['jumlah_sdm'] = $("#ddrs_jumlah_sdm").val();
		data['jumlah_alat_kesehatan'] = $("#ddrs_jumlah_alkes").val();

		data['alamat_rs'] = $("#ddrs_alamat_rs").val();
		data['kota_kabupaten'] = $("#ddrs_kota_kab").val();
		data['kode_provinsi'] = $("#ddrs_kode_provinsi").val();
		data['no_telp'] = $("#ddrs_telepon").val();
		data['fax'] = $("#ddrs_fax").val();
		data['email_rs'] = $("#ddrs_email").val();
		data['no_telp_bag_umum'] = $("#ddrs_telp_bag_umum").val();
		data['website'] = $("#ddrs_website").val();
		data['luas_tanah'] = $("#ddrs_luas_tanah").val();
		data['luas_bangunan'] = $("#ddrs_luas_bangunan").val();

		data['nomor_penetapan_kelas'] = $("#ddrs_nomor_penetapan_kelas").val();
		data['masa_berlaku_penetapan_kelas'] = $("#ddrs_masa_berlaku_penetapan_kelas").val();

		data['versi_akreditasi'] = $("#ddrs_edisi").val();
		data['status_akreditasi'] = $("#ddrs_status").val();
		data['masa_berlaku_akreditasi'] = $("#ddrs_masa_berlaku_akreditasi").val();

		data['ambulan_transportasi_baik'] = $("#ddrs_at_kondisi_baik").val();
		data['ambulan_transportasi_rusak_ringan'] = $("#ddrs_at_kondisi_rusak_ringan").val();
		data['ambulan_transportasi_rusak_berat'] = $("#ddrs_at_kondisi_rusak_berat").val();
		data['ambulan_jenasah_baik'] = $("#ddrs_aj_kondisi_baik").val();
		data['ambulan_jenasah_rusak_ringan'] = $("#ddrs_aj_kondisi_rusak_ringan").val();
		data['ambulan_jenasah_rusak_berat'] = $("#ddrs_aj_kondisi_rusak_berat").val();

		data['nomor_surat_izin_operasional_rs'] = $("#ddrs_nomor_sio").val();
		data['tanggal_surat_izin_operasional_rs'] = $("#ddrs_tanggal_sio").val();
		data['oleh_izin_operasional_rs'] = $("#ddrs_oleh_sio").val();
		data['masa_berlaku_surat_izin_operasional_rs'] = $("#ddrs_masa_berlaku_sio").val();
		data['sifat_izin_operasional_rs'] = $("#ddrs_sifat_sio").val();
		data['nomor_surat_izin_hemodialisa'] = $("#ddrs_nomor_sioh").val();
		data['tanggal_surat_izin_hemodialisa'] = $("#ddrs_tanggal_sioh").val();
		data['oleh_izin_hemodialisa'] = $("#ddrs_oleh_sioh").val();
		data['masa_berlaku_surat_izin_hemodialisa'] = $("#ddrs_masa_berlaku_sioh").val();
		data['sifat_izin_hemodialisa'] = $("#ddrs_sifat_sioh").val();

		data['jml_tempat_tidur_perinatologi'] = $("#ddrs_jtt_perinatologi").val();
		data['jml_tempat_tidur_kelas_vvip'] = $("#ddrs_jtt_kelas_vvip").val();
		data['jml_tempat_tidur_kelas_vip'] = $("#ddrs_jtt_kelas_vip").val();
		data['jml_tempat_tidur_kelas_i'] = $("#ddrs_jtt_kelas_i").val();
		data['jml_tempat_tidur_kelas_ii'] = $("#ddrs_jtt_kelas_ii").val();
		data['jml_tempat_tidur_kelas_iii'] = $("#ddrs_jtt_kelas_iii").val();
		data['jml_tempat_tidur_icu'] = $("#ddrs_jtt_icu").val();
		data['jml_tempat_tidur_picu'] = $("#ddrs_jtt_picu").val();
		data['jml_tempat_tidur_nicu'] = $("#ddrs_jtt_nicu").val();
		data['jml_tempat_tidur_iccu'] = $("#ddrs_jtt_iccu").val();
		data['jml_tempat_tidur_hcu'] = $("#ddrs_jtt_hcu").val();
		data['jml_tempat_tidur_ruang_isolasi'] = $("#ddrs_jtt_ruang_isolasi").val();

		data['kelengkapan_simrs'] = $("#ddrs_simrs").val();
		data['kelengkapan_bank_darah'] = $("#ddrs_bank_darah").val();
		data['kelengkapan_layanan_unggulan'] = $("#ddrs_layanan_unggulan").val();
		data['kelengkapan_peralatan_canggih'] = $("#ddrs_peralatan_canggih").val();
		data['kelengkapan_kerjasama_bpjs'] = $("#ddrs_ks_bpjs").val();
		data['kelengkapan_blud'] = $("#ddrs_blud").val();
		data['kelengkapan_kemampuan_pelayanan_gawat_darurat'] = $("#ddrs_klgd").val();
		return data;
	};
	DDRSAction.prototype.view = function() {
		var data = this.getRegulerData();
		data['command'] = "view";
		$.post(
			"",
			data,
			function(response) {
				var json = JSON.parse(response);
				if (json == null) return;
				$("#ddrs_kode_rs").val(json.koders);
				$("#ddrs_tanggal_registrasi").val(json.tanggal_registrasi);
				$("#ddrs_nama_rs").val(json.nama_rs);
				$("#ddrs_jenis_rs").val(json.jenis_rs);
				$("#ddrs_kelas_rs").val(json.kelas_rs);
				$("#ddrs_kepemilikan_rs").val(json.kepemilikan_rs);
				$("#ddrs_nama_direktur_rs").val(json.nama_direktur);
				$("#ddrs_penyelenggara_rs").val(json.nama_penyelenggara);
				$("#ddrs_status_penyelenggara").val(json.status_penyelenggara);
				$("#ddrs_jumlah_layanan").val(json.jumlah_layanan);
				$("#ddrs_jumlah_sdm").val(json.jumlah_sdm);
				$("#ddrs_jumlah_alkes").val(json.jumlah_alat_kesehatan);

				$("#ddrs_alamat_rs").val(json.alamat_rs);
				$("#ddrs_kota_kab").val(json.kota_kabupaten);
				$("#ddrs_kode_provinsi").val(json.kode_provinsi);
				$("#ddrs_telepon").val(json.no_telp);
				$("#ddrs_fax").val(json.fax);
				$("#ddrs_email").val(json.email_rs);
				$("#ddrs_telp_bag_umum").val(json.no_telp_bag_umum);
				$("#ddrs_website").val(json.website);
				$("#ddrs_luas_tanah").val(json.luas_tanah);
				$("#ddrs_luas_bangunan").val(json.luas_bangunan);

				$("#ddrs_nomor_penetapan_kelas").val(json.nomor_surat_penetapan_kelas);
				$("#ddrs_masa_berlaku_penetapan_kelas").val(json.masa_berlaku_surat_penetapan_kelas);

				$("#ddrs_edisi").val(json.versi_akreditasi);
				$("#ddrs_status").val(json.status_akreditasi);
				$("#ddrs_masa_berlaku_akreditasi").val(json.masa_berlaku_akreditasi);

				$("#ddrs_at_kondisi_baik").val(json.ambulan_transportasi_baik);
				$("#ddrs_at_kondisi_rusak_ringan").val(json.ambulan_transportasi_rusak_ringan);
				$("#ddrs_at_kondisi_rusak_berat").val(json.ambulan_transportasi_rusak_berat);
				$("#ddrs_aj_kondisi_baik").val(json.ambulan_jenasah_baik);
				$("#ddrs_aj_kondisi_rusak_ringan").val(json.ambulan_jenasah_rusak_ringan);
				$("#ddrs_aj_kondisi_rusak_berat").val(json.ambulan_jenasah_rusak_berat);

				$("#ddrs_nomor_sio").val(json.nomor_surat_izin_operasional_rs);
				$("#ddrs_tanggal_sio").val(json.tanggal_surat_izin_operasional_rs);
				$("#ddrs_oleh_sio").val(json.oleh_izin_operasional_rs);
				$("#ddrs_masa_berlaku_sio").val(json.masa_berlaku_surat_izin_operasional_rs);
				$("#ddrs_sifat_sio").val(json.sifat_izin_operasional_rs);
				$("#ddrs_nomor_sioh").val(json.nomor_surat_izin_hemodialisa);
				$("#ddrs_tanggal_sioh").val(json.tanggal_surat_izin_hemodialisa);
				$("#ddrs_oleh_sioh").val(json.oleh_izin_hemodialisa);
				$("#ddrs_masa_berlaku_sioh").val(json.masa_berlaku_surat_izin_hemodialisa);
				$("#ddrs_sifat_sioh").val(json.sifat_izin_hemodialisa);

				$("#ddrs_jtt_perinatologi").val(json.jml_tempat_tidur_perinatologi);
				$("#ddrs_jtt_kelas_vvip").val(json.jml_tempat_tidur_kelas_vvip);
				$("#ddrs_jtt_kelas_vip").val(json.jml_tempat_tidur_kelas_vip);
				$("#ddrs_jtt_kelas_i").val(json.jml_tempat_tidur_kelas_i);
				$("#ddrs_jtt_kelas_ii").val(json.jml_tempat_tidur_kelas_ii);
				$("#ddrs_jtt_kelas_iii").val(json.jml_tempat_tidur_kelas_iii);
				$("#ddrs_jtt_icu").val(json.jml_tempat_tidur_icu);
				$("#ddrs_jtt_picu").val(json.jml_tempat_tidur_picu);
				$("#ddrs_jtt_nicu").val(json.jml_tempat_tidur_nicu);
				$("#ddrs_jtt_iccu").val(json.jml_tempat_tidur_iccu);
				$("#ddrs_jtt_hcu").val(json.jml_tempat_tidur_hcu);
				$("#ddrs_jtt_ruang_isolasi").val(json.jml_tempat_tidur_ruang_isolasi);

				$("#ddrs_simrs").val(json.kelengkapan_simrs);
				$("#ddrs_bank_darah").val(json.kelengkapan_bank_darah);
				$("#ddrs_layanan_unggulan").val(json.kelengkapan_layanan_unggulan);
				$("#ddrs_peralatan_canggih").val(json.kelengkapan_peralatan_canggih);
				$("#ddrs_ks_bpjs").val(json.kelengkapan_kerjasama_bpjs);
				$("#ddrs_blud").val(json.kelengkapan_blud);
				$("#ddrs_klgd").val(json.kelengkapan_kemampuan_pelayanan_gawat_darurat);
			}
		);
	};
	DDRSAction.prototype.save = function() {
		var data = this.getSaveData();
		data['command'] = "save";
		$.post(
			"",
			data,
			function(response) {
				var json = getContent(response);
			}
		);
	};

	var ddrs;
	$(document).ready(function() {
		$(".mydate").datepicker();
		ddrs = new DDRSAction(
			"ddrs",
			"medical_record",
			"input_data_dasar_rs",
			new Array()
		);
		ddrs.view();
	});
</script>
<style type="text/css">
	form#ddrs_informasi_rs label, 
	form#ddrs_alamat_lokasi_rs label, 
	form#ddrs_penetapan_kelas label,
	form#ddrs_akreditasi_rs label, 
	form#ddrs_jumlah_ambulance label,
	form#ddrs_surat_izin_operasional label,
	form#ddrs_jumlah_tempat_tidur label {
	    float: left;
	    width: 150px;
	    height: 20px;
	}
	form#ddrs_kelengkapan label {
	    float: left;
	    width: 275px;
	    height: 20px;
	}
	form#ddrs_alamat_lokasi_rs > div, 
	form#ddrs_jumlah_ambulance > div,
	form#ddrs_surat_izin_operasional > div {
		width: 47%;
	}
</style>