<?php
global $db;

require_once 'smis-libs-hrd/EmployeeResponder.php';
require_once ("smis-base/smis-include-service-consumer.php");
require_once "medical_record/class/service/RuanganService.php";
$super = new SuperCommand ();

$phead=array ("No. Reg.", "NRM", "Nama", "Alamat", "Umur", "L/P");
$ptable = new Table ( $phead, "", NULL, true );
$ptable->setName ( "lo_pasien" );
$ptable->setModel ( Table::$SELECT );
$padapter = new SimpleAdapter ();
$padapter->add ( "No. Reg.", "id" );
$padapter->add ( "NRM", "nrm", "digit8" );
$padapter->add ( "Nama", "nama_pasien" );
$padapter->add ( "Alamat", "alamat_pasien" );
$padapter->add ( "Umur", "umur" );
$padapter->add ( "L/P", "kelamin", "trivial_0_Laki-Laki_Perempuan" );
$presponder = new ServiceResponder ( $db, $ptable, $padapter, "get_registered_all" );
$super->addResponder ( "lo_pasien", $presponder );

$eadapt = new SimpleAdapter ();
$eadapt->add ( "Jabatan", "nama_jabatan" );
$eadapt->add ( "Nama", "nama" );
$eadapt->add ( "NIP", "nip" );

$head_karyawan=array ('Nama','Jabatan',"NIP");
$dktable = new Table ( $head_karyawan, "", NULL, true );
$dktable->setName ( "dokter_operator" );
$dktable->setModel ( Table::$SELECT );
$employee = new EmployeeResponder ( $db, $dktable, $eadapt, "dokter" );
$super->addResponder ( "dokter_operator", $employee );

$dktable = new Table ($head_karyawan, "", NULL, true );
$dktable->setName ( "dokter_anastesi" );
$dktable->setModel ( Table::$SELECT );
$employee = new EmployeeResponder ( $db, $dktable, $eadapt, "dokter" );
$super->addResponder ( "dokter_anastesi", $employee );

$dktable = new Table ( $head_karyawan, "", NULL, true );
$dktable->setName ( "asisten_operator" );
$dktable->setModel ( Table::$SELECT );
$employee = new EmployeeResponder ( $db, $dktable, $eadapt, "" );
$super->addResponder ( "asisten_operator", $employee );

$dktable = new Table ( $head_karyawan, "", NULL, true );
$dktable->setName ( "asisten_anastesi" );
$dktable->setModel ( Table::$SELECT );
$employee = new EmployeeResponder ( $db, $dktable, $eadapt, "" );
$super->addResponder ( "asisten_anastesi", $employee );

$dktable = new Table ( $head_karyawan, "", NULL, true );
$dktable->setName ( "perawat_rr" );
$dktable->setModel ( Table::$SELECT );
$employee = new EmployeeResponder ( $db, $dktable, $eadapt, "" );
$super->addResponder ( "perawat_rr", $employee );

$dktable = new Table ( $head_karyawan, "", NULL, true );
$dktable->setName ( "instrument" );
$dktable->setModel ( Table::$SELECT );
$employee = new EmployeeResponder ( $db, $dktable, $eadapt, "" );
$super->addResponder ( "instrument", $employee );

$array=array("Nama","Kode","Keterangan");
$dktable = new Table ( $array, "", NULL, true );
$dktable->setName ( "diagnosa_operasi" );
$dktable->setModel ( Table::$SELECT );
$dbtable=new DBTable($db,"smis_mr_icd");
$eadapt=new SimpleAdapter();
$eadapt->add("Nama","nama");
$eadapt->add("Kode","icd");
$eadapt->add("Keterangan","sebab");
$diagnosa = new DBResponder ( $dbtable, $dktable, $eadapt, "diagnosa_operasi" );
$super->addResponder ( "diagnosa_operasi", $diagnosa );

$dktable = new Table ( $array, "", NULL, true );
$dktable->setName ( "tindakan_operasi" );
$dktable->setModel ( Table::$SELECT );
$dbtable=new DBTable($db,"smis_mr_icdtindakan");
$eadapt=new SimpleAdapter();
$eadapt->add("Nama","nama");
$eadapt->add("Kode","icd");
$eadapt->add("Keterangan","terjemah");
$tindakan = new DBResponder ( $dbtable, $dktable, $eadapt, "tindakan_operasi" );
$super->addResponder ( "tindakan_operasi", $tindakan );


$init = $super->initialize ();
if ($init != null) {
	echo $init;
	return;
}

$head=array ("Mulai","Selesai",'Nama','NRM','Jenis Operasi',"Operator","Anastesi");
$uitable = new Table ( $head, "Input Data Operasi", NULL, true );
$uitable->setName ( "input_operasi" );
if (isset ( $_POST ['command'] )) {
	$adapter = new SimpleAdapter ();
	$adapter->add ( "Mulai", "tanggal_mulai","date d M Y H:i");
	$adapter->add ( "Selesai", "tanggal_selesai","date d M Y H:i" );
	$adapter->add ( "Nama", "nama_pasien" );
	$adapter->add ( "NRM", "nrm_pasien","only-digit8" );
	$adapter->add ( "Jenis Operasi", "jenis_operasi" );
	$adapter->add ( "Operator", "dokter_operator" );
	$adapter->add ( "Anastesi", "dokter_anastesi" );
	
	$dbtable = new DBTable ( $db, "smis_mr_operasi" );
	$dbres = new DBResponder ( $dbtable, $uitable, $adapter );
	$data = $dbres->command ( $_POST ['command'] );
	echo json_encode ( $data );
	return;
}

$jk=new OptionBuilder();
$jk->add("Laki-Laki","0","1");
$jk->add("Perempuan","1");

$service = new RuanganService ( $db );
$service->execute ();
$ruangan = $service->getContent ();


$spesialisasi = new OptionBuilder ();
$spesialisasi->add ( "Bedah Umum", "Bedah Umum", $the_row ['spesialisasi'] == "Bedah Umum" ? "1" : "0" );
$spesialisasi->add ( "Obgyn", "Obgyn", $the_row ['spesialisasi'] == "Obgyn" ? "1" : "0" );
$spesialisasi->add ( "THT", "THT", $the_row ['spesialisasi'] == "THT" ? "1" : "0" );
$spesialisasi->add ( "Mata", "Mata", $the_row ['spesialisasi'] == "Mata" ? "1" : "0" );
$spesialisasi->add ( "Orthopedy", "Orthopedy", $the_row ['spesialisasi'] == "Orthopedy" ? "1" : "0" );
$spesialisasi->add ( "Urology", "Urology", $the_row ['spesialisasi'] == "Urology" ? "1" : "0" );
$spesialisasi->add ( "Bedah Saraf", "Bedah Saraf", $the_row ['spesialisasi'] == "Bedah Saraf" ? "1" : "0" );

$anastesi = new OptionBuilder ();
$anastesi->add ( "General Anastesi (TIVA)", "General Anastesi (TIVA)", $the_row ['anastesi'] == "General Anastesi (TIVA)" ? "1" : "0" );
$anastesi->add ( "General Anastesi (Inhalasi)", "General Anastesi (Inhalasi)", $the_row ['anastesi'] == "General Anastesi (Inhalasi)" ? "1" : "0" );
$anastesi->add ( "General Anastesi (Intub)", "General Anastesi (Intub)", $the_row ['anastesi'] == "General Anastesi (Intub)" ? "1" : "0" );
$anastesi->add ( "Local Anastesi", "Local Anastesi", $the_row ['anastesi'] == "Local Anastesi" ? "1" : "0" );
$anastesi->add ( "Regional Anastesi", "Regional Anastesi", $the_row ['anastesi'] == "Regional Anastesi" ? "1" : "0" );

$jenis_operasi = new OptionBuilder ();
$jenis_operasi->add ( "Kecil", "Kecil", $the_row ['jenis_operasi'] == "Kecil" ? "1" : "0" );
$jenis_operasi->add ( "Besar", "Besar", $the_row ['jenis_operasi'] == "Besar" ? "1" : "0" );
$jenis_operasi->add ( "Besar 1", "Besar 1", $the_row ['jenis_operasi'] == "Besar 1" ? "1" : "0" );
$jenis_operasi->add ( "Besar 2", "Besar 2", $the_row ['jenis_operasi'] == "Besar 2" ? "1" : "0" );
$jenis_operasi->add ( "Besar 3", "Besar 3", $the_row ['jenis_operasi'] == "Besar 3" ? "1" : "0" );
$jenis_operasi->add ( "Sedang", "Sedang", $the_row ['jenis_operasi'] == "Sedang" ? "1" : "0" );
$jenis_operasi->add ( "Sedang 1", "Sedang 1", $the_row ['jenis_operasi'] == "Sedang 1" ? "1" : "0" );
$jenis_operasi->add ( "Sedang 2", "Sedang 2", $the_row ['jenis_operasi'] == "Sedang 2" ? "1" : "0" );
$jenis_operasi->add ( "Sedang 3", "Sedang 3", $the_row ['jenis_operasi'] == "Sedang 3" ? "1" : "0" );
$jenis_operasi->add ( "Khusus 1", "Khusus 1", $the_row ['jenis_operasi'] == "Khusus 1" ? "1" : "0" );
$jenis_operasi->add ( "Khusus 2", "Khusus 2", $the_row ['jenis_operasi'] == "Khusus 2" ? "1" : "0" );
$jenis_operasi->add ( "Khusus 3", "Khusus 3", $the_row ['jenis_operasi'] == "Khusus 3" ? "1" : "0" );

$uitable->addModal ( "id", "hidden", "", "" );
$uitable->addModal ( "tanggal_mulai", "datetime", "Mulai", "","y",null,false,null,true,"tanggal_selesai" );
$uitable->addModal ( "tanggal_selesai", "datetime", "Selesai", "","y",null,false,null,false,"ruangan" );
$uitable->addModal ( "ruangan", "select", "Ruangan", $ruangan,"y",null,false,null,false,"nama_pasien");
$uitable->addModal ( "nama_pasien", "chooser-input_operasi-lo_pasien-Pasien", "Nama", "","y",null,true,null,false,"spesialisasi" );
$uitable->addModal ( "noreg_pasien", "text", "No. Reg.", "","y",null,true,null,false );
$uitable->addModal ( "nrm_pasien", "text", "NRM", "","y",null,true,null,false );
$uitable->addModal ( "alamat", "text", "Alamat", "","y",null,true,null,false );
$uitable->addModal ( "umur", "text", "Umur", "","y",null,true,null,false );
$uitable->addModal ( "jk", "select", "Jenis Kelamin", $jk->getContent(),"y",null,true,null,false);
$uitable->addModal ( "spesialisasi", "select", "Spesialisasi",$spesialisasi->getContent() ,"y",null,false,null,false,"jenis_operasi");
$uitable->addModal ( "jenis_operasi", "select", "Jenis Operasi", $jenis_operasi->getContent(),"y",null,false,null,false,"anastesi" );
$uitable->addModal ( "anastesi", "select", "Anastesi", $anastesi->getContent(),"y",null,false,null,false,"dokter_operator" );
//$uitable->addModal ( "gol_bedah", "text", "Gol Bedah", "" );
$uitable->addModal ( "dokter_operator", "chooser-input_operasi-dokter_operator", "Dokter Operator", "" );
$uitable->addModal ( "asisten_operator", "chooser-input_operasi-asisten_operator", "Asisten Operator", "" );
$uitable->addModal ( "instrument", "chooser-input_operasi-instrument", "Instrument", "" );
$uitable->addModal ( "dokter_anastesi", "chooser-input_operasi-dokter_anastesi", "Dokter Anastesi", "" );
$uitable->addModal ( "asisten_anastesi", "chooser-input_operasi-asisten_anastesi", "Asisten Anastesi", "" );
$uitable->addModal ( "perawat_rr", "chooser-input_operasi-perawat_rr", "Perawat RR", "" );
$uitable->addModal ( "diagnosa", "text", "Diagnosa", "","y",null,false,null,false,"nama_diagnosa" );
$uitable->addModal ( "nama_diagnosa", "chooser-input_operasi-diagnosa_operasi-Coding Diagnosa", "ICD Diagnosa", "","y",null,true,null,false,"icd_diagnosa" );
$uitable->addModal ( "icd_diagnosa", "text", "Kode ICD Diagnosa ", "","y",null,true,null,false,"ket_diagnosa" );
$uitable->addModal ( "ket_diagnosa", "text", "Keterangan", "","y",null,true,null,false,"tindakan" );
$uitable->addModal ( "tindakan", "text", "Tindakan", "","y",null,false,null,false,"nama_tindakan" );
$uitable->addModal ( "nama_tindakan", "chooser-input_operasi-tindakan_operasi-Coding Tindakan", "ICD Tindakan", "","y",null,true,null,false,"icd_tindakan" );
$uitable->addModal ( "icd_tindakan", "text", "Kode ICD Tindakan ", "","y",null,true,null,false,"ket_tindakan" );
$uitable->addModal ( "ket_tindakan", "text", "Keterangan ", "","y",null,true,null,false,"save" );

$modal = $uitable->getModal ();
$modal->setTitle ( "Input Operasi" );
//$modal->setComponentSize(Modal::$MEDIUM);
$modal->setModalSize(Modal::$FULL_MODEL);
echo $uitable->getHtml ();
echo $modal->getHtml ();
echo addJS ( "framework/smis/js/table_action.js" );
echo addJS ( "framework/bootstrap/js/bootstrap-datetimepicker.js" );
echo addCSS ( "framework/bootstrap/css/bootstrap-datetimepicker.css" );
?>
<script type="text/javascript">
var pasien;
var diagnosa_operasi;
var tindakan_operasi;
var input_operasi;
var lo_pasien;
var dokter_operator;
var dokter_anastesi;
var asisten_operator;
var asisten_anastesi;
var instrument;
var perawat_rr;
var lo_action="input_operasi";
var lo_the_page="medical_record";
$(document).ready(function(){
	$('.mydatetime').datetimepicker({ minuteStep: 1});
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
	var column=new Array('id','tanggal_mulai','tanggal_selesai','ruangan','nama_pasien',
			'nrm_pasien', 'noreg_pasien','alamat','umur','jk','spesialisasi','anastesi','dokter_operator',
			'asisten_operator','dokter_anastesi','asisten_anastesi','jenis_operasi',
			'instrument','perawat_rr','diagnosa','nama_diagnosa','icd_diagnosa','ket_diagnosa',
			'tindakan','nama_tindakan','icd_tindakan','ket_tindakan');
	input_operasi=new TableAction("input_operasi","medical_record","input_operasi",column);
	//input_operasi.setEnableAutofocus(true);
	input_operasi.setMultipleInput(true);
	input_operasi.setNextEnter();
	input_operasi.view();	

	$("#smis-chooser-modal").on("show", function() {
		if ($("#smis-chooser-modal .modal-header h3").text() == "PASIEN") {
			$("#smis-chooser-modal").removeClass("half_model");
			$("#smis-chooser-modal").removeClass("full_model");
			$("#smis-chooser-modal").addClass("full_model");
		} else {
			$("#smis-chooser-modal").removeClass("half_model");
			$("#smis-chooser-modal").removeClass("full_model");
			$("#smis-chooser-modal").addClass("half_model");
		}
	});

	lo_pasien=new TableAction("lo_pasien",lo_the_page,lo_action,new Array());
	lo_pasien.setSuperCommand("lo_pasien");
	lo_pasien.selected = function(json) {
		$("#" + lo_action + "_noreg_pasien").val(json.id);
		$("#" + lo_action + "_nrm_pasien").val(json.nrm);
		$("#" + lo_action + "_nama_pasien").val(json.nama_pasien);
		$("#" + lo_action + "_alamat").val(json.alamat_pasien);
		$("#" + lo_action + "_umur").val(json.umur);
		$("#" + lo_action + "_jk").val(json.kelamin);
	};	

	dokter_operator=new TableAction("dokter_operator",lo_the_page,lo_action,new Array());
	dokter_operator.setSuperCommand("dokter_operator");
	dokter_operator.selected=function(json){
		$("#"+lo_action+"_dokter_operator").val(json.nama);
	};

	dokter_anastesi=new TableAction("dokter_anastesi",lo_the_page,lo_action,new Array());
	dokter_anastesi.setSuperCommand("dokter_anastesi");
	dokter_anastesi.selected=function(json){
		$("#"+lo_action+"_dokter_anastesi").val(json.nama);
	};

	asisten_operator=new TableAction("asisten_operator",lo_the_page,lo_action,new Array());
	asisten_operator.setSuperCommand("asisten_operator");
	asisten_operator.selected=function(json){
		$("#"+lo_action+"_asisten_operator").val(json.nama);
	};

	asisten_anastesi=new TableAction("asisten_anastesi",lo_the_page,lo_action,new Array());
	asisten_anastesi.setSuperCommand("asisten_anastesi");
	asisten_anastesi.selected=function(json){
		$("#"+lo_action+"_asisten_anastesi").val(json.nama);
	};

	instrument=new TableAction("instrument",lo_the_page,lo_action,new Array());
	instrument.setSuperCommand("instrument");
	instrument.selected=function(json){
		$("#"+lo_action+"_instrument").val(json.nama);
	};

	perawat_rr=new TableAction("perawat_rr",lo_the_page,lo_action,new Array());
	perawat_rr.setSuperCommand("perawat_rr");
	perawat_rr.selected=function(json){
		$("#"+lo_action+"_perawat_rr").val(json.nama);
	};
	
	diagnosa_operasi=new TableAction("diagnosa_operasi",lo_the_page,lo_action,new Array());
	diagnosa_operasi.setSuperCommand("diagnosa_operasi");
	diagnosa_operasi.selected=function(json){
		$("#"+lo_action+"_nama_diagnosa").val(json.nama);
		$("#"+lo_action+"_icd_diagnosa").val(json.icd);
		$("#"+lo_action+"_ket_diagnosa").val(json.sebab);
	};
	
	tindakan_operasi=new TableAction("tindakan_operasi",lo_the_page,lo_action,new Array());
	tindakan_operasi.setSuperCommand("tindakan_operasi");
	tindakan_operasi.selected=function(json){
		$("#"+lo_action+"_nama_tindakan").val(json.nama);
		$("#"+lo_action+"_icd_tindakan").val(json.icd);
		$("#"+lo_action+"_ket_tindakan").val(json.terjemah);
	};
	

	stypeahead("#"+lo_action+"_dokter_operator",3,dokter_operator,"nama",function(item){
		$("#"+lo_action+"_dokter_operator").val(item.nama);
		$("#"+lo_action+"_asisten_operator").focus();					
	});

	stypeahead("#"+lo_action+"_dokter_anastesi",3,dokter_anastesi,"nama",function(item){
		$("#"+lo_action+"_dokter_anastesi").val(item.nama);
		$("#"+lo_action+"_asisten_anastesi").focus();					
	});

	stypeahead("#"+lo_action+"_asisten_operator",3,asisten_operator,"nama",function(item){
		$("#"+lo_action+"_asisten_operator").val(item.nama);
		$("#"+lo_action+"_instrument").focus();					
	});

	stypeahead("#"+lo_action+"_instrument",3,instrument,"nama",function(item){
		$("#"+lo_action+"_instrument").val(item.nama);
		$("#"+lo_action+"_dokter_anastesi").focus();					
	});
	
	stypeahead("#"+lo_action+"_perawat_rr",3,perawat_rr,"nama",function(item){
		$("#"+lo_action+"_perawat_rr").val(item.nama);
		$("#"+lo_action+"_diagnosa_operasi").focus();					
	});
	

	stypeahead("#"+lo_action+"_asisten_anastesi",3,asisten_anastesi,"nama",function(item){
		$("#"+lo_action+"_asisten_anastesi").val(item.nama);
		$("#"+lo_action+"_perawat_rr").focus();					
	});
	
	stypeahead("#"+lo_action+"_nama_diagnosa",3,diagnosa_operasi,"nama",function(item){
		$("#"+lo_action+"_nama_diagnosa").val(item.nama);
		$("#"+lo_action+"_icd_diagnosa").val(item.icd);
		$("#"+lo_action+"_ket_diagnosa").val(item.sebab);
		$("#"+lo_action+"_tindakan").focus();					
	});
    
    
});
</script>
