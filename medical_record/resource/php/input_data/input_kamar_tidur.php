<?php
	global $db;

	$table = new Table(
		array("No.", "Tahun", "Nama Ruangan", "Jenis Layanan", "Total TT", "Kelas VVIP", "Kelas VIP", "Kelas I", "Kelas II", "Kelas III", "Kelas Khusus"),
		"Kamar Tidur",
		null,
		true
	);
	$table->setName("kamar_tidur");
	$table->setPrintButtonEnable(false);
	$table->setReloadButtonEnable(false);

	if (isset($_POST['command'])) {
		if ($_POST['command'] == "check_data") {
			$tahun = $_POST['tahun'];
			$nama_ruangan = $_POST['nama_ruangan'];
			$jenis_layanan = $_POST['jenis_layanan'];
			$row = $db->get_row("
				SELECT COUNT(*) jumlah
				FROM smis_mr_kamar_tidur
				WHERE prop = '' AND tahun = '" . $tahun . "' AND jenis_layanan = '" . $jenis_layanan . "' AND nama_ruangan = '" . $nama_ruangan . "'
			");
			$jumlah = 0;
			if ($row != null)
				$jumlah = $row->jumlah;
			echo json_encode($jumlah);
			return;
		}
		$adapter = new SimpleAdapter(true, "No.");
		$adapter->add("Tahun", "tahun");
		$adapter->add("Nama Ruangan", "nama_ruangan");
		$adapter->add("Jenis Layanan", "jenis_layanan");
		$adapter->add("Total TT", "jumlah_tt", "number");
		$adapter->add("Kelas VVIP", "jumlah_vvip", "number");
		$adapter->add("Kelas VIP", "jumlah_vip", "number");
		$adapter->add("Kelas I", "jumlah_kelas_i", "number");
		$adapter->add("Kelas II", "jumlah_kelas_ii", "number");
		$adapter->add("Kelas III", "jumlah_kelas_iii", "number");
		$adapter->add("Kelas Khusus", "jumlah_kelas_khusus", "number");
		$dbtable = new DBTable($db, "smis_mr_kamar_tidur");
		$dbreponder = new DBResponder(
			$dbtable,
			$table,
			$adapter
		);
		$data = $dbreponder->command($_POST['command']);
		echo json_encode($data);
		return;
	}

	$modal = new Modal("kamar_tidur_add_form", "smis_form_container", "kamar_tidur");
	$modal->setTitle("Data Kamar Tidur");
	$id_hidden = new Hidden("kamar_tidur_id", "kamar_tidur_id", "");
	$modal->addElement("", $id_hidden);
	$tahun_text = new Text("kamar_tidur_tahun", "kamar_tidur_tahun", date("Y"));
	$modal->addElement("Tahun", $tahun_text);
	$nama_ruangan_text = new Text("kamar_tidur_nama_ruangan", "kamar_tidur_nama_ruangan", "");
	$modal->addElement("Nama Ruangan", $nama_ruangan_text);
	$jenis_layanan_option = new OptionBuilder();
	$jenis_layanan_option->addSingle("Penyakit Dalam");
	$jenis_layanan_option->addSingle("Kesehatan Anak");
	$jenis_layanan_option->addSingle("Obstetri");
	$jenis_layanan_option->addSingle("Genekologi");
	$jenis_layanan_option->addSingle("Bedah");
	$jenis_layanan_option->addSingle("Bedah Orthopedi");
	$jenis_layanan_option->addSingle("Bedah Saraf");
	$jenis_layanan_option->addSingle("Luka Bakar");
	$jenis_layanan_option->addSingle("Saraf");
	$jenis_layanan_option->addSingle("Jiwa");
	$jenis_layanan_option->addSingle("Psikologi");
	$jenis_layanan_option->addSingle("Penatalaksana Penyalahgunaan NAPZA");
	$jenis_layanan_option->addSingle("THT");
	$jenis_layanan_option->addSingle("Mata");
	$jenis_layanan_option->addSingle("Kulit & Kelamin");
	$jenis_layanan_option->addSingle("Kardiologi");
	$jenis_layanan_option->addSingle("Paru-Paru");
	$jenis_layanan_option->addSingle("Geriatri");
	$jenis_layanan_option->addSingle("Radioterapi");
	$jenis_layanan_option->addSingle("Kedokteran Nuklir");
	$jenis_layanan_option->addSingle("Kusta");
	$jenis_layanan_option->addSingle("Rehabilitasi Medik");
	$jenis_layanan_option->addSingle("Isolasi");
	$jenis_layanan_option->addSingle("ICU");
	$jenis_layanan_option->addSingle("ICCU");
	$jenis_layanan_option->addSingle("NICU / PICU");
	$jenis_layanan_option->addSingle("Umum");
	$jenis_layanan_option->addSingle("Gigi & Mulut");
	$jenis_layanan_option->addSingle("Pelayanan Rawat Darurat");
	$jenis_layanan_option->addSingle("Perinatologi/Bayi");
	$jenis_layanan_select = new Select("kamar_tidur_jenis_layanan", "kamar_tidur_jenis_layanan", $jenis_layanan_option->getContent());
	$modal->addElement("Jenis Layanan", $jenis_layanan_select);
	$jumlah_vvip_text = new Text("kamar_tidur_jumlah_vvip", "kamar_tidur_jumlah_vvip", "0");
	$modal->addElement("Jml. VVIP", $jumlah_vvip_text);
	$jumlah_vip_text = new Text("kamar_tidur_jumlah_vip", "kamar_tidur_jumlah_vip", "0");
	$modal->addElement("Jml. VIP", $jumlah_vip_text);
	$jumlah_kelas_i_text = new Text("kamar_tidur_jumlah_kelas_i", "kamar_tidur_jumlah_kelas_i", "0");
	$modal->addElement("Jml. Kls. I", $jumlah_kelas_i_text);
	$jumlah_kelas_ii_text = new Text("kamar_tidur_jumlah_kelas_ii", "kamar_tidur_jumlah_kelas_ii", "0");
	$modal->addElement("Jml. Kls. II", $jumlah_kelas_ii_text);
	$jumlah_kelas_iii_text = new Text("kamar_tidur_jumlah_kelas_iii", "kamar_tidur_jumlah_kelas_iii", "0");
	$modal->addElement("Jml. Kls. III", $jumlah_kelas_iii_text);
	$jumlah_kelas_khusus_text = new Text("kamar_tidur_jumlah_kelas_khusus", "kamar_tidur_jumlah_kelas_khusus", "0");
	$modal->addElement("Jml. Kls. Khusus", $jumlah_kelas_khusus_text);
	$jumlah_tt_text = new Text("kamar_tidur_jumlah_tt", "kamar_tidur_jumlah_tt", "0");
	$jumlah_tt_text->setAtribute("disabled='disabled'");
	$modal->addElement("Total TT", $jumlah_tt_text);
	$save_button = new Button("", "", "Simpan");
	$save_button->setClass("btn-success");
	$save_button->setAction("kamar_tidur.save()");
	$save_button->setIcon("fa fa-floppy-o");
	$save_button->setIsButton(Button::$ICONIC);
	$modal->addFooter($save_button);
	
	echo $modal->getHtml();
	echo $table->getHtml();
	echo addJS("framework/smis/js/table_action.js");
	echo addJS("medical_record/resource/js/input_kamar_tidur.js", false);
?>