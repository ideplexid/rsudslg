<?php
global $db;
require_once 'smis-base/smis-include-service-consumer.php';
require_once 'smis-libs-hrd/EmployeeResponder.php';

if(isset($_POST['super_command'])) {
    $super = new SuperCommand ();
    if($_POST['super_command'] == 'icdtindakan') {
        $array_mui=array ("Nama",'DTD','Kode');
        $muitable = new Table ( $array_mui);
        $muitable->setModel ( Table::$BOTH );
        $muitable->setName ( "icdtindakan" );
        $muitable->setDelButtonEnable(false);
        $muitable->setPrintButtonEnable(false);
        $muitable->setReloadButtonEnable(false);

        $madapter = new SimpleAdapter ();
        $madapter->add ( "Nama", "nama" );
        $madapter->add ( "Kode", "icd" );
        $madapter->add ( "DTD", "dtd" );
        $madapter->add ( "Grup", "grup" );

        $dbtable=new DBTable($db, "smis_mr_icdtindakan");
        $mresponder = new DBResponder($dbtable, $muitable, $madapter );
        $super->addResponder ( "icdtindakan", $mresponder );
    } else if($_POST['super_command'] == 'icdpasien') {
        $phead=array ('Nama','NRM',"No Reg");
        $ptable = new Table($phead, "", NULL, true);
        $ptable->setName("icdpasien");
        $ptable->setModel(Table::$SELECT);
        $padapter = new SimpleAdapter ();
        $padapter->add("Nama", "nama_pasien");
        $padapter->add("NRM", "nrm_pasien", "digit8");
        $padapter->add("No Reg", "no_register");
        $presponder = new ServiceResponder($db, $ptable, $padapter, "get_room_patient", $_POST['ruang']);
        $super->addResponder("icdpasien", $presponder);
    } else if($_POST['super_command'] == 'icddokter') {
        $header=array (	'Nama','Jabatan',"NIP");
        $dktable = new Table($header, "", NULL, true);
        $dktable->setName("icddokter");
        $dktable->setModel(Table::$SELECT);
        $dkadapter = new SimpleAdapter ();
        $dkadapter->add("Jabatan", "nama_jabatan");
        $dkadapter->add("Nama", "nama");
        $dkadapter->add("NIP", "nip");
        $dkresponder = new EmployeeResponder($db, $dktable, $dkadapter, "dokter");
        $super->addResponder("icddokter", $dkresponder);
    } else if($_POST['super_command'] == 'icd_tindakan_dokter') {
        $dktable    = new Table(array ("Nama","Kelas","Pasien"), "", NULL, true);
		$dktable    ->setName("icd_tindakan_dokter")
				    ->setModel(Table::$SELECT);
		$dkadapter  = new SimpleAdapter ();
		$dkadapter  ->add("Nama","nama")
				    ->add("Kelas","kelas","unslug")
				    ->add("Pasien","jenis_pasien","unslug");
        $dkresponder = new ServiceResponder($db, $dktable, $dkadapter, "get_tindakan_dokter");
        $super->addResponder("icd_tindakan_dokter", $dkresponder);
    }
    
    $init = $super->initialize ();
    if ($init != null) {
        echo $init;
        return;
    }
}

$head=array ('Tanggal Input', 'Tanggal Kunjungan', 'No. Reg. Pasien', 'NRM Pasien', 'Nama Pasien',"Tindakan",'Dokter',"Kode ICD","Nama","DTD","Grup");
$uitable = new Table ( $head, "", NULL, true );
$uitable->setName ( "tindakan" );
if (isset ( $_POST ['command'] )) {
	$adapter = new SimpleAdapter ();
    $adapter->add ( "Tanggal Kunjungan", "waktu_register","date d M Y" );
	$adapter->add ( "Tanggal Input", "waktu","date d M Y" );
    $adapter->add ( "No. Reg. Pasien", "noreg_pasien" );
    $adapter->add ( "NRM Pasien", "nrm_pasien", "digit8" );
	$adapter->add ( "Nama Pasien", "nama_pasien" );
	$adapter->add ( "Dokter", "nama_dokter" );
	$adapter->add ( "Tindakan", "nama_tindakan" );
	$adapter->add ( "Kode ICD", "kode_icd" );
	$adapter->add ( "Nama", "nama_icd" );
	$adapter->add ( "DTD", "kode_dtd" );
	$adapter->add ( "Grup", "kode_grup" );
	
	$dbres = new ServiceResponder($db, $uitable, $adapter, "get_tindakan",$_POST['ruang']) ;	
	$data = $dbres->command ( $_POST ['command'] );
	echo json_encode ( $data );
	return;
}

$urjip = new ServiceConsumer($db, "get_urjip", array());
$urjip->setMode(ServiceConsumer::$MULTIPLE_MODE);
$urjip->setCached(true, "get_urjip");
$urjip->execute();
$content = $urjip->getContent();
$ruangan = array();
foreach ($content as $autonomous => $ruang) {
    foreach ($ruang as $nama_ruang => $jip){
        if ($jip[$nama_ruang] == "URI" || $jip[$nama_ruang] == "URJ" || $jip[$nama_ruang] == "URJI"){
            $option = array();
            $option['value'] = $nama_ruang;
            if ($jip['name'] != null)
                $option['name'] = $jip['name'];
            else
                $option['name'] = ArrayAdapter::format("unslug", $nama_ruang);
            $ruangan[] = $option;
        }
    }
}

$tbl=new Table(array());
$tbl->setName("header");
$tbl->addModal("ruang", "select", "Ruang", $ruangan);

$form=$tbl->getModal()->getForm();
$form->setTitle("Tindakan");
$btn=new Button("", "", "");
$btn->setClass("btn-primary");
$btn->setIsButton(Button::$ICONIC);
$btn->setIcon("fa fa-refresh");
$btn->setAction("tindakan.view()");
$form->addElement("",$btn);

$uitable->addModal ( "id", "hidden", "", "" );
$uitable->addModal ( "waktu", "datetime", "Tgl. Input", date ( "Y-m-d H:i" ),"y",NULL,false  );
$uitable->addModal ( "nama_pasien", "chooser-tindakan-icdpasien", "Pasien", "","y",NULL,true  );
$uitable->addModal ( "nrm_pasien", "text", "NRM", "","y",NULL,true  );
$uitable->addModal ( "noreg_pasien", "text", "Noreg", "","y",NULL,true  );
$uitable->addModal ( "nama_dokter", "chooser-tindakan-icddokter", "Dokter", "","y",NULL,true  );
$uitable->addModal ( "id_dokter", "hidden", ""  );
$uitable->addModal ( "jenis_dokter", "hidden", ""  );
$uitable->addModal ( "nama_tindakan", "chooser-tindakan-icd_tindakan_dokter", "Tindakan", "","y",NULL,true  );
$uitable->addModal ( "harga", "hidden", ""  );
$uitable->addModal ( "nama_icd", "chooser-tindakan-icdtindakan", "Nama ICD", "","y",NULL,true);
$uitable->addModal ( "kode_icd", "text", "Kode ICD", "","y",NULL,true  );
$uitable->addModal ( "kode_grup", "text", "Grup", "","y",NULL,true  );
$uitable->addModal ( "kode_dtd", "text", "No DTD", "","y",NULL,true  );

$modal = $uitable->getModal ();
$modal->setTitle ( "tindakan" );

/*$muitable	->addModal("id", "hidden", "", "")
			->addModal("nama", "text", "Nama", "")
			->addModal("icd", "text", "ICD", "")
			->addModal("grup", "text", "Grup", "")
			->addModal("dtd","text", "DTD", "");
$mu_modal=$muitable->getModal();
$mu_modal->setTitle("Kode ICD");*/

echo $form->getHtml();
echo $uitable->getHtml ();
echo $modal->getHtml ();
//echo $mu_modal->getHtml ();

echo addJS ( "framework/smis/js/table_action.js" );
echo addJS ( "framework/bootstrap/js/bootstrap-datetimepicker.js" );
echo addCSS ( "framework/bootstrap/css/bootstrap-datetimepicker.css" );

?>
<script type="text/javascript">
var tindakan;
var icdtindakan;
var icdpasien;
var icddokter;
var icd_tindakan_dokter;
$(document).ready(function(){
    $('.mydatetime').datetimepicker({ minuteStep: 1});
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
	var column=new Array('id',"nama_tindakan",'harga','nama_pasien','nrm_pasien','noreg_pasien','nama_dokter','id_dokter','jenis_dokter','waktu',"kode_icd","nama_icd","kode_dtd","kode_grup");
	tindakan=new TableAction("tindakan","medical_record","tindakan",column);	
	tindakan.addRegulerData=function(reg_data){
		reg_data['ruang']=$("#header_ruang").val();
		return reg_data;
	};

	icdtindakan=new TableAction("icdtindakan","medical_record","tindakan",new Array("id","nama","icd","grup","dtd"));
	icdtindakan.setSuperCommand("icdtindakan");
	icdtindakan.selected=function(json){
        console.log(json);
		$("#tindakan_kode_icd").val(json.icd);
		$("#tindakan_nama_icd").val(json.nama);
		$("#tindakan_kode_dtd").val(json.dtd);						
		$("#tindakan_kode_grup").val(json.grup);						
	};
    
    icdpasien=new TableAction("icdpasien","medical_record","tindakan",new Array());
    icdpasien.addRegulerData=function(data) {
        data['ruang'] = $("#header_ruang").val();
        data['command'] = 'list';
        return data;
    };
	icdpasien.setSuperCommand("icdpasien");
	icdpasien.selected=function(json){
        console.log(json);
		$("#tindakan_nama_pasien").val(json.nama_pasien);
		$("#tindakan_nrm_pasien").val(json.nrm_pasien);
		$("#tindakan_noreg_pasien").val(json.no_register);
	};
    
    icddokter=new TableAction("icddokter","medical_record","tindakan",new Array());
    icddokter.setSuperCommand("icddokter");
    icddokter.selected=function(json){
        console.log(json);
        $("#tindakan_nama_dokter").val(json.nama);
    };
    
    icd_tindakan_dokter=new TableAction("icd_tindakan_dokter","medical_record","tindakan",new Array());
    icd_tindakan_dokter.setSuperCommand("icd_tindakan_dokter");
    icd_tindakan_dokter.selected=function(json){
        console.log(json);
        $("#tindakan_nama_tindakan").val(json.nama);
        $("#tindakan_harga").val(json.tarif);
    };
	
});
</script>
