<?php
/**
 * this source code used for handling
 * operation report
 * 
 * @author 		: Nurul Huda
 * @license 	: Apache v3
 * @version 	: 1.0.0
 * @since 		: 24 Sept 2016
 * @copyright 	: goblooge@gmail.com
 * @database	: smis_mr_iklin
 * 
 * */
 
global $db;
require_once ("smis-base/smis-include-service-consumer.php");
$uitable = new Report ( array ('No','Nama','No. RM',"Jenis","Keterangan" ), "", NULL );
$uitable->addHeader ( "before", "
		<tr> 
			<th></th> 
			<th colspan='2'>Pasien Infeksi Luka Operasi</th> 
			<th></th> 
			<th></th> 
		</tr>" );
$uitable->setName ( "operasi" );
$uitable->setDiagram ( true );

/* this is respond when system have to response */
if (isset ( $_POST ['command'] )) {
	
	$adapter=new SimpleAdapter();
	$adapter->setUseNumber(true,"No");
	$adapter->add("Nama","nama_pasien");
	$adapter->add("No. RM","nrm_pasien");
	$adapter->add("Jenis","operasi_jenis");
	$adapter->add("Keterangan","operasi_ket");
	
	$diagramadapter = new SimpleAdapter ();
	$diagramadapter->add ( "Date", "the_time" );
	$diagramadapter->add ( "ya", "ya" );
	$diagramadapter->add ( "tidak", "tidak" );
	$diagramadapter->setUseID ( false );
	$dbtable = new DBTable ( $db, "smis_mr_iklin", array (
			'ruangan' 
	) );
	if (isset ( $_POST ['ruangan'] ))
		$dbtable->addCustomKriteria ( "ruangan", "='" . $_POST ['ruangan'] . "'" );
	$dbtable->setOrder ( "operasi_kapan DESC" );
	$dbtable->addCustomKriteria ( "operasi_infeksi", "='1'" );
	
	$dbres = new DBReport ( $dbtable, $uitable, $adapter, 'operasi_kapan', DBReport::$DATE );
	// $diagram_view="sum(if(operasi_darah='1' AND operasi_infeksi='1',1,0)) as ya, sum(if(operasi_darah='1' AND operasi_infeksi='0',1,0)) as tidak";
	$dbres->setDiagram ( false, null, null, null );
	$data = $dbres->command ( $_POST ['command'] );
	echo json_encode ( $data, JSON_NUMERIC_CHECK );
	return;
}

/* This is Modal Form and used for add and edit the table */
loadClass ( "ServiceProviderList" );
$service = new ServiceProviderList ( $db, "push_antrian" );
$service->execute ();
$ruangan = $service->getContent ();

$uitable->addModal ( "ruangan", "select", "Ruang", $ruangan );
$modal = $uitable->getAdvanceModal ();
$modal->setTitle ( "operasi" );

echo addCSS("framework/bootstrap/css/morris.css");
echo addJS("framework/jquery/raphael-min.js");
echo addJS("framework/bootstrap/js/morris.min.js");
echo addJS ( "framework/smis/js/table_action.js" );
echo addJS ( "framework/smis/js/report_action.js" );
echo addJS ( "framework/bootstrap/js/bootstrap-datepicker.js" );
echo addCSS ( "framework/bootstrap/css/datepicker.css" );
echo addJS("medical_record/resource/js/operasi.js",false);
echo $modal->joinFooterAndForm()->getHtml();
echo $uitable->getHtml();
?>