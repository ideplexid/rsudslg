<?php
/**
 * this source code used for handling
 * tranfusion report
 * 
 * @author 		: Nurul Huda
 * @license 	: Apache v3
 * @version 	: 1.0.0
 * @since 		: 24 Sept 2016
 * @copyright 	: goblooge@gmail.com
 * @database	: smis_mr_iklin
 * 
 * */
 
$header = array ();
$header [] = "Tanggal";
$header [] = "Whole Blood";
$header [] = "PRC";
$header [] = "Plasma";
$header [] = "Komp. Lain";
$header [] = "Jumlah";
$uitable = new Table ( $header, "", NULL, false );
$uitable->setName ( "lap_transfusi" );
$uitable->setFooterVisible ( false );

if (isset ( $_POST ['command'] )) {
	require_once "medical_record/class/adapter/LapTransfusiAdapter.php";
	$adapter = new LapTransfusiAdapter ();
	$dbtable = new DBTable ( $db, "smis_mr_iklin" );
	if (isset ( $_POST ['dari'] ) && isset ( $_POST ['sampai'] ) && $_POST ['dari'] != "" && $_POST ['sampai'] != "") {
		$dari = $_POST ['dari'];
		$sampai = $_POST ['sampai'];
		$dbtable->addCustomKriteria ( "'" . $dari . "'", "<=transfusi_kapan" );
		$dbtable->addCustomKriteria ( "'" . $sampai . "'", ">=transfusi_kapan" );
		$dbtable->addCustomKriteria ( "transfusi_darah", "=1" );
		$dbtable->setShowAll ( true );
	}
	$dbtable->setOrder ( " transfusi_kapan ASC " );
	$dbres = new DBResponder ( $dbtable, $uitable, $adapter );
	$data = $dbres->command ( $_POST ['command'] );
	echo json_encode ( $data );
	return;
}

$uitable->addModal ( "dari", "date", "Dari", "" );
$uitable->addModal ( "sampai", "date", "Sampai", "" );

$modal = $uitable->getModal ();
$form = $modal->getForm ();

$button = new Button ( "", "", "" );
$button->setIsButton ( Button::$ICONIC );
$button->setIcon ( "fa fa-refresh" );
$button->setClass("btn-primary");
$button->setAction ( "lap_transfusi.view()" );
$form->addElement ( "", $button );
$button = new Button ( "", "", "" );
$button->setIsButton ( Button::$ICONIC );
$button->setClass("btn-primary");
$button->setIcon ( "fa fa-print" );
$button->setAction ( "smis_print($('#print_table_lap_transfusi').html())" );
$form->addElement ( "", $button );
echo $form->getHtml ();

echo $uitable->getHtml ();
echo addJS ( "framework/smis/js/table_action.js" );
echo addJS ( "framework/bootstrap/js/bootstrap-datepicker.js" );
echo addCSS ( "framework/bootstrap/css/datepicker.css" );
echo addJS("medical_record/resource/js/lap_transfusi.js",false);
echo addCSS("medical_record/resource/css/lap_transfusi.css",false);
?>
