<?php
/**
 * this source code used for handling
 * transfusion detail report
 * 
 * @author 		: Nurul Huda
 * @license 	: Apache v3
 * @version 	: 1.0.0
 * @since 		: 24 Sept 2016
 * @copyright 	: goblooge@gmail.com
 * @database	: smis_mr_iklin
 * 
 * */
 
global $db;
require_once ("smis-base/smis-include-service-consumer.php");
$uitable = new Report ( array ('No','Nama','No. RM',"Ya","Tidak","Keterangan" ), "", NULL );
$uitable->addHeader ( "before", "<tr><th></th><th colspan='2'>Pasien Tranfusi Darah</th><th colspan='2'>Mengalami Infeksi</th><th></th></tr>" );
$uitable->setName ( "transfusi" );
$uitable->setDiagram ( true );

/* this is respond when system have to response */
if (isset ( $_POST ['command'] )) {
	
	$adapter=new SimpleAdapter();
	$adapter->setUseNumber(true,"No");
	$adapter->add("Nama","nama_pasien");
	$adapter->add("No. RM","nrm_pasien");
	$adapter->add("Ya","transfusi_infeksi","trivial_1_X_");
	$adapter->add("Tidak","transfusi_infeksi","trivial_0_X_");
	$adapter->add("Keterangan","transfusi_ket");
	
	$diagramadapter = new SimpleAdapter ();
	$diagramadapter->add ( "Date", "the_time" );
	$diagramadapter->add ( "ya", "ya" );
	$diagramadapter->add ( "tidak", "tidak" );
	$diagramadapter->setUseID ( false );
	$dbtable = new DBTable ( $db, "smis_mr_iklin", array ('ruangan') );
	if (isset ( $_POST ['ruangan'] ))
		$dbtable->addCustomKriteria ( "ruangan", "='" . $_POST ['ruangan'] . "'" );
	$dbtable->setOrder ( "transfusi_kapan DESC" );
	$dbtable->addCustomKriteria ( "transfusi_darah", "='1'" );
	
	$dbres = new DBReport ( $dbtable, $uitable, $adapter, 'transfusi_kapan', DBReport::$DATE );
	$diagram_view = "sum(if(transfusi_darah='1' AND transfusi_infeksi='1',1,0)) as ya, sum(if(transfusi_darah='1' AND transfusi_infeksi='0',1,0)) as tidak";
	$dbres->setDiagram ( true, $diagram_view, null, $diagramadapter );
	$data = $dbres->command ( $_POST ['command'] );
	echo json_encode ( $data, JSON_NUMERIC_CHECK );
	return;
}

/* This is Modal Form and used for add and edit the table */
loadClass ( "ServiceProviderList" );
$service = new ServiceProviderList ( $db, "push_antrian" );
$service->execute ();
$ruangan = $service->getContent ();

$uitable->addModal ( "ruangan", "select", "Ruang", $ruangan );
$uitable->setModelDiagram(Report::$DIAGRAM_BAR);
$modal = $uitable->getAdvanceModal ();
$modal->setTitle ( "Transfusi" );

echo $modal->joinFooterAndForm()->getHtml();
echo "<div class='line clear'></div>";
echo $uitable->getDiagram("transfusi_chart");
echo "<div class='line clear'></div>";
echo $uitable->getHtml();

echo addCSS("framework/bootstrap/css/morris.css");
echo addJS("framework/jquery/raphael-min.js");
echo addJS("framework/bootstrap/js/morris.min.js");
echo addJS ( "framework/smis/js/table_action.js" );
echo addJS ( "framework/smis/js/report_action.js" );
echo addJS ( "framework/bootstrap/js/bootstrap-datepicker.js" );
echo addCSS ( "framework/bootstrap/css/datepicker.css" );
echo addJS("medical_record/resource/js/transfusi.js",false);
?>