<?php

require_once 'medical_record/class/adapter/LaporanAsesmenAnakAdapter.php';
require_once 'medical_record/class/responder/LapAsesmenAnakResponder.php';

global $db;
$header = array("No.","Pasien","No. Reg", "NRM", "Tanggal Masuk", "Dokter", "Ruangan", "Tanggal Asesmen");
//$uitable = new Table($header,"Laporan Assesmen Anak",NULL,true);
$uitable = new Table($header,"",NULL,true);
$uitable->setName("laporan_asesmen_anak");
$uitable->setExcelButtonEnable(false);
$uitable->setAddButtonEnable(false);
$uitable->setPrintButtonEnable(false);
$uitable->setEditButtonEnable(false);
$uitable->setDelButtonEnable(false);
//$uitable->setDetailButtonEnable(true);
$btn=new Button("", "", "");
$btn->addClass("btn-warning")
    ->setIsButton(Button::$ICONIC)
    ->setIcon("fa fa-eye");
$uitable->addContentButton("show", $btn);
$excel=new Button("", "", "");
$excel->addClass("btn-success")
    ->setIsButton(Button::$ICONIC)
    ->setIcon("fa fa-file-excel-o ");
$uitable->addContentButton("d_excel", $excel);

if(isset($_POST['command']))
{
    //$adapter = new SimpleAdapter(true,"No.");
    $adapter = new LaporanAsesmenAnakAdapter();
    $adapter->setUseNumber(true, "No.","back.");
    $adapter->add("Pasien","nama_pasien");
    $adapter->add("No. Reg","noreg_pasien");
    $adapter->add("NRM","nrm_pasien");
    $adapter->add("Tanggal Masuk","waktu_masuk", "date d M Y");
    $adapter->add("Dokter","nama_dokter");
    $adapter->add("Ruangan","ruangan", "unslug");
    $adapter->add("Tanggal Asesmen","waktu", "date d M Y");
    
    //For Excel
    $adapter->add("nama_pasien","nama_pasien");
    $adapter->add("jk","jk", "trivial_0_Laki-Laki_Perempuan");
    $adapter->add("tgl_lahir_pasien","tgl_lahir_pasien", "date d M Y");
    $adapter->add("alamat_pasien","alamat_pasien");
    $adapter->add("tgl_asesmen","waktu", "date d M Y");
    $adapter->add("jam_asesmen","waktu", "date H:i:s");
    $adapter->add("tanggal_masuk","waktu_masuk", "date d M Y");
    $adapter->add("bb","bb");
    $adapter->add("tb","tb");
    $adapter->add("lila","lila");
    $adapter->add("lk","lk");
    $adapter->add("anamneses_utama","anamneses_utama");
    $adapter->add("anamneses_penyakit_skrg","anamneses_penyakit_skrg");
    $adapter->add("anamneses_pengobatan","anamneses_pengobatan");
    $adapter->add("mata_anemia","mata_anemia");
    $adapter->add("mata_ikterus","mata_ikterus");
    $adapter->add("mata_reflek_pupil","mata_reflek_pupil");
    $adapter->add("mata_edema_palbebrae","mata_edema_palbebrae");
    $adapter->add("tht_tonsil","tht_tonsil");
    $adapter->add("tht_faring","tht_faring");
    $adapter->add("tht_lidah","tht_lidah");
    $adapter->add("tht_bibir","tht_bibir");
    $adapter->add("leher_jvp","leher_jvp");
    $adapter->add("leher_pembesaran_kelenjar","leher_pembesaran_kelenjar");
    $adapter->add("kaku_kuduk","kaku_kuduk");
    $adapter->add("torak_simetris","torak_simetris");
    $adapter->add("torak_asimetris","torak_asimetris");
    $adapter->add("jantung_s1_s2","jantung_s1_s2");
    $adapter->add("jantung_reguler","jantung_reguler");
    $adapter->add("jantung_ireguler","jantung_ireguler");
    $adapter->add("jantung_murmur","jantung_murmur");
    $adapter->add("jantung_lain","jantung_lain");
    $adapter->add("paru_suara_nafas","paru_suara_nafas");
    $adapter->add("paru_ronki","paru_ronki");
    $adapter->add("paru_wheezing","paru_wheezing");
    $adapter->add("paru_lain","paru_lain");
    $adapter->add("abdomen_distensi","abdomen_distensi");
    $adapter->add("abdomen_meteorismus","abdomen_meteorismus");
    $adapter->add("abdomen_peristaltik","abdomen_peristaltik");
    $adapter->add("abdomen_arcites","abdomen_arcites");
    $adapter->add("abdomen_nyeri","abdomen_nyeri");
    $adapter->add("abdomen_nyeri_lokasi","abdomen_nyeri_lokasi");
    $adapter->add("hepar","hepar");
    $adapter->add("lien","lien");
    $adapter->add("status_gizi","status_gizi");
    $adapter->add("extremitas_hangat","extremitas_hangat");
    $adapter->add("extremitas_dingin","extremitas_dingin");
    $adapter->add("extremitas_edema","extremitas_edema");
    $adapter->add("extremitas_lain","extremitas_lain");
    $adapter->add("rencana_kerja","rencana_kerja");
    $adapter->add("hasil_periksa_penunjang","hasil_periksa_penunjang");
    $adapter->add("diagnosis","diagnosis");
    $adapter->add("terapi","terapi");
    $adapter->add("hasil_pembedahan","hasil_pembedahan");
    $adapter->add("rekomendasi","rekomendasi");
    $adapter->add("catatan","catatan");
    $adapter->add("tgl_pulang","waktu_pulang", "date d M Y");
    $adapter->add("jam_pulang","waktu_pulang", "date H:i:s");
    $adapter->add("tgl_kontrol_klinik","tgl_kontrol_klinik", "date d M Y");
    $adapter->add("dirawat_diruang","dirawat_diruang");
    
    $dbtable = new DBTable($db,"smis_mr_assesmen_anak");
    if(isset($_POST['dari']) && isset($_POST['sampai'])) {
        $dbtable->addCustomKriteria(NULL, "waktu_masuk >= '".$_POST['dari']."' AND waktu_masuk <= '".$_POST['sampai']."'");
    }
    $dbresponder = new LapAsesmenAnakResponder($dbtable,$uitable,$adapter);
    $data = $dbresponder->command($_POST ['command']);
	echo json_encode($data);	
	return;
}

$uitable->clearContent();
$uitable->addModal("dari", "date", "Dari", "")
		->addModal("sampai", "date", "Sampai", "");
 
$modal = $uitable->getModal();
$form = $modal->getForm ();

$button = new Button ( "", "", "" );
$button->setIsButton ( Button::$ICONIC );
$button->setIcon ( "fa fa-refresh" );
$button->setClass("btn-primary");
$button->setAction ( "laporan_asesmen_anak.view()" );
$form->addElement ( "", $button );

echo "<h2>Laporan Asesmen Anak</h2>";
echo $form->getHtml();
echo $uitable->getHtml();

echo addJS("framework/bootstrap/js/bootstrap-datepicker.js");
echo addJS("framework/smis/js/table_action.js");
echo addCSS("framework/bootstrap/css/datepicker.css");

?>

<script type="text/javascript">

var laporan_asesmen_anak;
$(document).ready(function(){
    $('.mydate').datepicker();
    var column = new Array('nama_pasien','noreg_pasien','nrm_pasien','nama_dokter','ruangan');
    laporan_asesmen_anak = new TableAction("laporan_asesmen_anak", "medical_record", "asesmen_anak", column);
    //laporan_asesmen_anak.view();
    laporan_asesmen_anak.addRegulerData = function(data) {
        data['dari'] = $("#laporan_asesmen_anak_dari").val();
        data['sampai'] = $("#laporan_asesmen_anak_sampai").val();
        return data;
    };
    laporan_asesmen_anak.show=function(id){
        var data=$("#laporan_asesmen_anak_"+id).html();
        showWarning("Laporan Asesmen Anak ", data);
    };
    laporan_asesmen_anak.d_excel=function(id){
        var data=this.getViewData();
        data['id'] = id;
        data['command']="excel";
        download(data);
    }
});

</script>