<?php

require_once 'medical_record/class/adapter/LaporanAsesmenPsikologiAdapter.php';
require_once 'medical_record/class/responder/LapAsesmenPsikologiResponder.php';

global $db;
$header = array("No.","Pasien","No. Reg","NRM", "Tanggal Masuk", "Dokter", "Ruangan", "Tanggal Asesmen");
$uitable = new Table($header,"",NULL,true);
$uitable->setName("laporan_asesmen_psikologi");
$uitable->setExcelButtonEnable(false);
$uitable->setAddButtonEnable(false);
$uitable->setPrintButtonEnable(false);
$uitable->setEditButtonEnable(false);
$uitable->setDelButtonEnable(false);
//$uitable->setDetailButtonEnable(true);
$btn=new Button("", "", "");
$btn->addClass("btn-warning")
    ->setIsButton(Button::$ICONIC)
    ->setIcon("fa fa-eye");
$uitable->addContentButton("show", $btn);
$excel=new Button("", "", "");
$excel->addClass("btn-success")
    ->setIsButton(Button::$ICONIC)
    ->setIcon("fa fa-file-excel-o ");
$uitable->addContentButton("d_excel", $excel);

if(isset($_POST['command']))
{
    //$adapter = new SimpleAdapter(true,"No.");
    $adapter = new LaporanAsesmenPsikologiAdapter();
    $adapter->setUseNumber(true, "No.","back.");
    $adapter->add("Pasien","nama_pasien");
    $adapter->add("No. Reg","noreg_pasien");
    $adapter->add("NRM","nrm_pasien");
    $adapter->add("Tanggal Masuk","waktu_masuk", "date d M Y");
    $adapter->add("Dokter","nama_dokter");
    $adapter->add("Ruangan","ruangan", "unslug");
    $adapter->add("Tanggal Asesmen","waktu", "date d M Y");
    
    //For Excel
    $adapter->add("nama_pasien","nama_pasien");
    $adapter->add("jk","jk", "trivial_0_Laki-Laki_Perempuan");
    $adapter->add("tgl_lahir_pasien","tgl_lahir_pasien", "date d M Y");
    $adapter->add("alamat_pasien","alamat_pasien");
    $adapter->add("tgl_asesmen","waktu", "date d M Y");
    $adapter->add("jam_asesmen","waktu", "date H:i:s");
    $adapter->add("tanggal_masuk","waktu_masuk", "date d M Y");
    $adapter->add("anak_ke","anak_ke");
    $adapter->add("dari_saudara","dari_saudara");
    $adapter->add("status_nikah","status_nikah");
    $adapter->add("pend_terakhir","pend_terakhir");
    $adapter->add("obeservasi","obeservasi");
    $adapter->add("wawancara","wawancara");
    $adapter->add("wawancara_ket","wawancara_ket");
    $adapter->add("keluhan_utama","keluhan_utama");
    $adapter->add("mulai_terjadi","mulai_terjadi");
    $adapter->add("riwayat_penyakit","riwayat_penyakit");
    $adapter->add("keluhan_psikosomatis","keluhan_psikosomatis");
    $adapter->add("penyebab","penyebab");
    $adapter->add("faktor_keluarga","faktor_keluarga");
    $adapter->add("faktor_pekerjaan","faktor_pekerjaan");
    $adapter->add("faktor_premorboid","faktor_premorboid");
    $adapter->add("riwayat_napza","riwayat_napza");
    $adapter->add("napza_lama_pemakaian","napza_lama_pemakaian");
    $adapter->add("napza_jenis_zat","napza_jenis_zat");
    $adapter->add("napza_cara_pakai","napza_cara_pakai");
    $adapter->add("napza_latarbelakang_pakai","napza_latarbelakang_pakai");
    $adapter->add("kesan_umum","kesan_umum");
    $adapter->add("kesadaran","kesadaran");
    $adapter->add("mood","mood");
    $adapter->add("proses_dan_isi_pikiran","proses_dan_isi_pikiran");
    $adapter->add("kecerdasan","kecerdasan");
    $adapter->add("dorongan","dorongan");
    $adapter->add("judgment","judgment");
    $adapter->add("attention","attention");
    $adapter->add("psikomotorik","psikomotorik");
    $adapter->add("orientasi_diri","orientasi_diri");
    $adapter->add("orientasi_wkt","orientasi_wkt");
    $adapter->add("orientasi_tempat","orientasi_tempat");
    $adapter->add("orientasi_ruang","orientasi_ruang");
    $adapter->add("bb","bb");
    $adapter->add("tb","tb");
    $adapter->add("lila","lila");
    $adapter->add("lk","lk");
    $adapter->add("status_gizi","status_gizi");
    $adapter->add("tes_iq","tes_iq");
    $adapter->add("tes_bakat","tes_bakat");
    $adapter->add("tes_kepribadian","tes_kepribadian");
    $adapter->add("tes_okupasi","tes_okupasi");
    $adapter->add("tes_perkembangan","tes_perkembangan");
    $adapter->add("perkembangan_fisik","perkembangan_fisik");
    $adapter->add("perkembangan_kognitif","perkembangan_kognitif");
    $adapter->add("perkembangan_emosi","perkembangan_emosi");
    $adapter->add("perkembangan_sosial","perkembangan_sosial");
    $adapter->add("perkembangan_motorik","perkembangan_motorik");
    $adapter->add("report","report");
    $adapter->add("ekspresi","ekspresi");
    $adapter->add("reaksi","reaksi");
    $adapter->add("komunikasi","komunikasi");
    $adapter->add("lain_lain","lain_lain");
    $adapter->add("tgl_pulang","waktu_pulang", "date d M Y");
    $adapter->add("jam_pulang","waktu_pulang", "date H:i:s");
    $adapter->add("tgl_kontrol_klinik","waktu_kontrol_klinik", "date d M Y");
    $adapter->add("dirawat_diruang","dirawat_diruang");
    
    $dbtable = new DBTable($db,"smis_mr_assesmen_psikologi");
    if(isset($_POST['dari']) && isset($_POST['sampai'])) {
        $dbtable->addCustomKriteria(NULL, "waktu_masuk >= '".$_POST['dari']."' AND waktu_masuk <= '".$_POST['sampai']."'");
    }
    $dbresponder = new LapAsesmenPsikologiResponder($dbtable,$uitable,$adapter);
    $data = $dbresponder->command($_POST ['command']);
	echo json_encode($data);	
	return;
}

$uitable->clearContent();
$uitable->addModal("dari", "date", "Dari", "")
		->addModal("sampai", "date", "Sampai", "");
 
$modal = $uitable->getModal();
$form = $modal->getForm ();

$button = new Button ( "", "", "" );
$button->setIsButton ( Button::$ICONIC );
$button->setIcon ( "fa fa-refresh" );
$button->setClass("btn-primary");
$button->setAction ( "laporan_asesmen_psikologi.view()" );
$form->addElement ( "", $button );

echo "<h2>Laporan Assesmen Psikologi</h2>";
echo $form->getHtml();
echo $uitable->getHtml();

echo addJS("framework/bootstrap/js/bootstrap-datepicker.js");
echo addJS("framework/smis/js/table_action.js");
echo addCSS("framework/bootstrap/css/datepicker.css");

?>

<script type="text/javascript">

var laporan_asesmen_psikologi;
$(document).ready(function(){
    $('.mydate').datepicker();
    var column = new Array('nama_pasien','noreg_pasien','nrm_pasien','nama_dokter','ruangan');
    laporan_asesmen_psikologi = new TableAction("laporan_asesmen_psikologi", "medical_record", "asesmen_psikologi", column);
    //laporan_asesmen_psikologi.view();
    laporan_asesmen_psikologi.addRegulerData = function(data) {
        data['dari'] = $("#laporan_asesmen_psikologi_dari").val();
        data['sampai'] = $("#laporan_asesmen_psikologi_sampai").val();
        return data;
    };
    laporan_asesmen_psikologi.show=function(id){
        var data=$("#laporan_asesmen_psikologi_"+id).html();
        showWarning("Laporan Asesmen Psikologi ", data);
    };
    laporan_asesmen_psikologi.d_excel=function(id){
        var data=this.getViewData();
        data['id'] = id;
        data['command']="excel";
        download(data);
    }
});

</script>