<?php

require_once 'medical_record/class/adapter/LaporanAsesmenPsikiatriAdapter.php';
require_once 'medical_record/class/responder/LapAsesmenPsikiatriResponder.php';

global $db;
$header = array("No.","Pasien","No. Reg","NRM", "Tanggal Masuk", "Dokter", "Ruangan", "Tanggal Asesmen");
$uitable = new Table($header,"",NULL,true);
$uitable->setName("laporan_asesmen_psikiatri");
$uitable->setExcelButtonEnable(false);
$uitable->setAddButtonEnable(false);
$uitable->setPrintButtonEnable(false);
$uitable->setEditButtonEnable(false);
$uitable->setDelButtonEnable(false);
//$uitable->setDetailButtonEnable(true);
$btn=new Button("", "", "");
$btn->addClass("btn-warning")
    ->setIsButton(Button::$ICONIC)
    ->setIcon("fa fa-eye");
$uitable->addContentButton("show", $btn);
$excel=new Button("", "", "");
$excel->addClass("btn-success")
    ->setIsButton(Button::$ICONIC)
    ->setIcon("fa fa-file-excel-o ");
$uitable->addContentButton("d_excel", $excel);

if(isset($_POST['command']))
{
    //$adapter = new SimpleAdapter(true,"No.");
    $adapter = new LaporanAsesmenPsikiatriAdapter();
    $adapter->setUseNumber(true, "No.","back.");
    $adapter->add("Pasien","nama_pasien");
    $adapter->add("No. Reg","noreg_pasien");
    $adapter->add("NRM","nrm_pasien");
    $adapter->add("Tanggal Masuk","waktu_masuk", "date d M Y");
    $adapter->add("Dokter","nama_dokter");
    $adapter->add("Ruangan","ruangan", "unslug");
    $adapter->add("Tanggal Asesmen","waktu", "date d M Y");
    
    //For Excel
    $adapter->add("nama_pasien","nama_pasien");
    $adapter->add("jk","jk", "trivial_0_Laki-Laki_Perempuan");
    $adapter->add("tgl_lahir_pasien","tgl_lahir_pasien", "date d M Y");
    $adapter->add("alamat_pasien","alamat_pasien");
    $adapter->add("tgl_asesmen","waktu", "date d M Y");
    $adapter->add("jam_asesmen","waktu", "date H:i:s");
    $adapter->add("tanggal_masuk","waktu_masuk", "date d M Y");
    $adapter->add("keluhan_utama","keluhan_utama");
    $adapter->add("riwayat_penyakit_skrg","riwayat_penyakit_skrg");
    $adapter->add("auto_anamnesis","auto_anamnesis");
    $adapter->add("hetero_namnesis","hetero_namnesis");
    $adapter->add("faktor_penyebab","faktor_penyebab");
    $adapter->add("faktor_keluarga","faktor_keluarga");
    $adapter->add("fungsi_kerja","fungsi_kerja");
    $adapter->add("riwayat_napza","riwayat_napza");
    $adapter->add("napza_lama_pemakaian","napza_lama_pemakaian");
    $adapter->add("napza_jenis_zat","napza_jenis_zat");
    $adapter->add("napza_cara_pemakaian","napza_cara_pemakaian");
    $adapter->add("napza_latarbelakang_pemakaian","napza_latarbelakang_pemakaian");
    $adapter->add("faktor_premorbid","faktor_premorbid");
    $adapter->add("faktor_organik","faktor_organik");
    $adapter->add("kesan_umum","kesan_umum");
    $adapter->add("kesadaran","kesadaran");
    $adapter->add("mood","mood");
    $adapter->add("proses_pikir","proses_pikir");
    $adapter->add("pencerapan","pencerapan");
    $adapter->add("dorongan_insting","dorongan_insting");
    $adapter->add("psikomotor","psikomotor");
    $adapter->add("penyakit_dalam","penyakit_dalam");
    $adapter->add("neurologi","neurologi");
    $adapter->add("diagnosis_kerja","diagnosis_kerja");
    $adapter->add("terapi","terapi");
    $adapter->add("rencana_kerja","rencana_kerja");
    $adapter->add("tgl_pulang","waktu_pulang", "date d M Y");
    $adapter->add("jam_pulang","waktu_pulang", "date H:i:s");
    $adapter->add("tgl_kontrol_klinik","waktu_kontrol_klinik", "date d M Y");
    $adapter->add("dirawat_diruang","dirawat_diruang");
    
    $dbtable = new DBTable($db,"smis_mr_assesmen_psikiatri");
    if(isset($_POST['dari']) && isset($_POST['sampai'])) {
        $dbtable->addCustomKriteria(NULL, "waktu_masuk >= '".$_POST['dari']."' AND waktu_masuk <= '".$_POST['sampai']."'");
    }
    $dbresponder = new LapAsesmenPsikiatriResponder($dbtable,$uitable,$adapter);
    $data = $dbresponder->command($_POST ['command']);
	echo json_encode($data);	
	return;
}

$uitable->clearContent();
$uitable->addModal("dari", "date", "Dari", "")
		->addModal("sampai", "date", "Sampai", "");
 
$modal = $uitable->getModal();
$form = $modal->getForm ();

$button = new Button ( "", "", "" );
$button->setIsButton ( Button::$ICONIC );
$button->setIcon ( "fa fa-refresh" );
$button->setClass("btn-primary");
$button->setAction ( "laporan_asesmen_psikiatri.view()" );
$form->addElement ( "", $button );

echo "<h2>Laporan Assesmen Psikiatri</h2>";
echo $form->getHtml();
echo $uitable->getHtml();

echo addJS("framework/bootstrap/js/bootstrap-datepicker.js");
echo addJS("framework/smis/js/table_action.js");
echo addCSS("framework/bootstrap/css/datepicker.css");

?>

<script type="text/javascript">

var laporan_asesmen_psikiatri;
$(document).ready(function(){
    $('.mydate').datepicker();
    var column = new Array('nama_pasien','noreg_pasien','nrm_pasien','nama_dokter','ruangan');
    laporan_asesmen_psikiatri = new TableAction("laporan_asesmen_psikiatri", "medical_record", "asesmen_psikiatri", column);
    //laporan_asesmen_psikiatri.view();
    laporan_asesmen_psikiatri.addRegulerData = function(data) {
        data['dari'] = $("#laporan_asesmen_psikiatri_dari").val();
        data['sampai'] = $("#laporan_asesmen_psikiatri_sampai").val();
        return data;
    };
    laporan_asesmen_psikiatri.show=function(id){
        var data=$("#laporan_asesmen_psikiatri_"+id).html();
        showWarning("Laporan Asesmen Psikiatri ", data);
    };
    laporan_asesmen_psikiatri.d_excel=function(id){
        var data=this.getViewData();
        data['id'] = id;
        data['command']="excel";
        download(data);
    }
});

</script>