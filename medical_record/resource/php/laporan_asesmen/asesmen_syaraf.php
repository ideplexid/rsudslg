<?php

require_once 'medical_record/class/adapter/LaporanAsesmenSyarafAdapter.php';
require_once 'medical_record/class/responder/LapAsesmenSyarafResponder.php';

global $db;
$header = array("No.","Pasien","No. Reg","NRM", "Tanggal Masuk", "Dokter", "Ruangan", "Tanggal Asesmen");
$uitable = new Table($header,"",NULL,true);
$uitable->setName("laporan_asesmen_syaraf");
$uitable->setExcelButtonEnable(false);
$uitable->setAddButtonEnable(false);
$uitable->setPrintButtonEnable(false);
$uitable->setEditButtonEnable(false);
$uitable->setDelButtonEnable(false);
//$uitable->setDetailButtonEnable(true);
$btn=new Button("", "", "");
$btn->addClass("btn-warning")
    ->setIsButton(Button::$ICONIC)
    ->setIcon("fa fa-eye");
$uitable->addContentButton("show", $btn);
$excel=new Button("", "", "");
$excel->addClass("btn-success")
    ->setIsButton(Button::$ICONIC)
    ->setIcon("fa fa-file-excel-o ");
$uitable->addContentButton("d_excel", $excel);

if(isset($_POST['command']))
{
    //$adapter = new SimpleAdapter(true,"No.");
    $adapter = new LaporanAsesmenSyarafAdapter();
    $adapter->setUseNumber(true, "No.","back.");
    $adapter->add("Pasien","nama_pasien");
    $adapter->add("No. Reg","noreg_pasien");
    $adapter->add("NRM","nrm_pasien");
    $adapter->add("Tanggal Masuk","waktu_masuk", "date d M Y");
    $adapter->add("Dokter","nama_dokter");
    $adapter->add("Ruangan","ruangan", "unslug");
    $adapter->add("Tanggal Asesmen","waktu", "date d M Y");
    
    //For Excel
    $adapter->add("nama_pasien","nama_pasien");
    $adapter->add("jk","jk", "trivial_0_Laki-Laki_Perempuan");
    $adapter->add("tgl_lahir_pasien","tgl_lahir_pasien", "date d M Y");
    $adapter->add("alamat_pasien","alamat_pasien");
    $adapter->add("tgl_asesmen","waktu", "date d M Y");
    $adapter->add("jam_asesmen","waktu", "date H:i:s");
    $adapter->add("tanggal_masuk","waktu_masuk", "date d M Y");
    $adapter->add("mata_anamnesis","mata_anamnesis");
    $adapter->add("mata_ikaterus","mata_ikaterus");
    $adapter->add("mata_reflek_pupil","mata_reflek_pupil");
    $adapter->add("mata_ukuran_pupil","mata_ukuran_pupil");
    $adapter->add("mata_iso_unisocore","mata_iso_unisocore");
    $adapter->add("mata_lain","mata_lain");
    $adapter->add("tht_tonsil","tht_tonsil");
    $adapter->add("tht_faring","tht_faring");
    $adapter->add("tht_lidah","tht_lidah");
    $adapter->add("tht_bibir","tht_bibir");
    $adapter->add("leher_jvp","leher_jvp");
    $adapter->add("leher_pembesaran_kelenjar","leher_pembesaran_kelenjar");
    $adapter->add("torak_simetris_asimetris","torak_simetris_asimetris");
    $adapter->add("cor_s1_s2","cor_s1_s2");
    $adapter->add("cor_reguler","cor_reguler");
    $adapter->add("cor_ireguler","cor_ireguler");
    $adapter->add("cor_mur","cor_mur");
    $adapter->add("cor_besar","cor_besar");
    $adapter->add("pulmo_suara_nafas","pulmo_suara_nafas");
    $adapter->add("pulmo_ronchi","pulmo_ronchi");
    $adapter->add("pulmo_wheezing","pulmo_wheezing");
    $adapter->add("abdomen_hepar","abdomen_hepar");
    $adapter->add("abdomen_lien","abdomen_lien");
    $adapter->add("extremitas_hangat","extremitas_hangat");
    $adapter->add("extremitas_dingin","extremitas_dingin");
    $adapter->add("extremitas_edema","extremitas_edema");
    $adapter->add("kranium","kranium");
    $adapter->add("kranium_lain","kranium_lain");
    $adapter->add("kranium_keterangan","kranium_keterangan");
    $adapter->add("korpus_vertebra","korpus_vertebra");
    $adapter->add("korpus_vertebra_lain","korpus_vertebra_lain");
    $adapter->add("korpus_vertebra_keterangan","korpus_vertebra_keterangan");
    $adapter->add("tanda_selaput_otak","tanda_selaput_otak");
    $adapter->add("tanda_selaput_otak_lain","tanda_selaput_otak_lain");
    $adapter->add("tanda_selaput_otak_keterangan","tanda_selaput_otak_keterangan");
    $adapter->add("syaraf_otak_kanan","syaraf_otak_kanan");
    $adapter->add("syaraf_otak_kiri","syaraf_otak_kiri");
    $adapter->add("motorik","motorik");
    $adapter->add("motorik_kaki_kanan","motorik_kaki_kanan");
    $adapter->add("motorik_kaki_kiri","motorik_kaki_kiri");
    $adapter->add("refleks","refleks");
    $adapter->add("refleks_kaki_kanan","refleks_kaki_kanan");
    $adapter->add("refleks_kaki_kiri","refleks_kaki_kiri");
    $adapter->add("sensorik","sensorik");
    $adapter->add("sensorik_kaki_kanan","sensorik_kaki_kanan");
    $adapter->add("sensorik_kaki_kiri","sensorik_kaki_kiri");
    $adapter->add("vegentatif","vegentatif");
    $adapter->add("luhur_kesadaran","luhur_kesadaran");
    $adapter->add("luhur_reaksi_emosi","luhur_reaksi_emosi");
    $adapter->add("luhur_intelek","luhur_intelek");
    $adapter->add("luhur_proses_berpikir","luhur_proses_berpikir");
    $adapter->add("luhur_psikomotorik","luhur_psikomotorik");
    $adapter->add("luhur_psikosensorik","luhur_psikosensorik");
    $adapter->add("luhur_bicara_bahasa","luhur_bicara_bahasa");
    $adapter->add("mental_refleks","mental_refleks");
    $adapter->add("mental_memegang","mental_memegang");
    $adapter->add("mental_menetek","mental_menetek");
    $adapter->add("mental_snout_refleks","mental_snout_refleks");
    $adapter->add("mental_glabola","mental_glabola");
    $adapter->add("mental_palmomental","mental_palmomental");
    $adapter->add("mental_korneomandibular","mental_korneomandibular");
    $adapter->add("mental_kaki_tolik","mental_kaki_tolik");
    $adapter->add("mental_lain","mental_lain");
    $adapter->add("nyeri_tekan_syaraf","nyeri_tekan_syaraf");
    $adapter->add("tanda_laseque","tanda_laseque");
    $adapter->add("lain_lain","lain_lain");
    $adapter->add("diagnosa_kerja","diagnosa_kerja");
    $adapter->add("diagnosa_kerja_icd","diagnosa_kerja_icd");
    $adapter->add("diagnosa_kerja_ket","diagnosa_kerja_ket");
    $adapter->add("terapi_tindakan","terapi_tindakan");
    $adapter->add("terapi_tindakan_icd","terapi_tindakan_icd");
    $adapter->add("terapi_tindakan_ket","terapi_tindakan_ket");
    $adapter->add("penunjang_neurovaskuler","penunjang_neurovaskuler");
    $adapter->add("penunjang_neuroimaging","penunjang_neuroimaging");
    $adapter->add("penunjang_elektrodiagnostik","penunjang_elektrodiagnostik");
    $adapter->add("penunjang_lab","penunjang_lab");
    $adapter->add("penunjang_lain","penunjang_lain");
    $adapter->add("tgl_pulang","waktu_pulang", "date d M Y");
    $adapter->add("jam_pulang","waktu_pulang", "date H:i:s");
    $adapter->add("tgl_kontrol_klinik","tgl_kontrol_klinik", "date d M Y");
    $adapter->add("dirawat_diruang","dirawat_diruang");
    
    $dbtable = new DBTable($db,"smis_mr_assesmen_syaraf");
    if(isset($_POST['dari']) && isset($_POST['sampai'])) {
        $dbtable->addCustomKriteria(NULL, "waktu_masuk >= '".$_POST['dari']."' AND waktu_masuk <= '".$_POST['sampai']."'");
    }
    $dbresponder = new LapAsesmenSyarafResponder($dbtable,$uitable,$adapter);
    $data = $dbresponder->command($_POST ['command']);
	echo json_encode($data);	
	return;
}

$uitable->clearContent();
$uitable->addModal("dari", "date", "Dari", "")
		->addModal("sampai", "date", "Sampai", "");
 
$modal = $uitable->getModal();
$form = $modal->getForm ();

$button = new Button ( "", "", "" );
$button->setIsButton ( Button::$ICONIC );
$button->setIcon ( "fa fa-refresh" );
$button->setClass("btn-primary");
$button->setAction ( "laporan_asesmen_syaraf.view()" );
$form->addElement ( "", $button );

echo "<h2>Laporan Assesmen Syaraf</h2>";
echo $form->getHtml();
echo $uitable->getHtml();

echo addJS("framework/bootstrap/js/bootstrap-datepicker.js");
echo addJS("framework/smis/js/table_action.js");
echo addCSS("framework/bootstrap/css/datepicker.css");

?>

<script type="text/javascript">

var laporan_asesmen_syaraf;
$(document).ready(function(){
    $('.mydate').datepicker();
    var column = new Array('nama_pasien','noreg_pasien','nrm_pasien','nama_dokter','ruangan');
    laporan_asesmen_syaraf = new TableAction("laporan_asesmen_syaraf", "medical_record", "asesmen_syaraf", column);
    //laporan_asesmen_syaraf.view();
    laporan_asesmen_syaraf.addRegulerData = function(data) {
        data['dari'] = $("#laporan_asesmen_syaraf_dari").val();
        data['sampai'] = $("#laporan_asesmen_syaraf_sampai").val();
        return data;
    };
    laporan_asesmen_syaraf.show=function(id){
        var data=$("#laporan_asesmen_syaraf_"+id).html();
        showWarning("Laporan Asesmen Syaraf ", data);
    };
    laporan_asesmen_syaraf.d_excel=function(id){
        var data=this.getViewData();
        data['id'] = id;
        data['command']="excel";
        download(data);
    }
});

</script>