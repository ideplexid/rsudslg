<?php

require_once 'medical_record/class/adapter/LaporanAsesmenPerawatAdapter.php';
require_once 'medical_record/class/responder/LapAsesmenPerawatResponder.php';

global $db;
$header = array("No.","Pasien","No. Reg","NRM", "Tanggal Masuk", "Dokter", "Perawat", "Ruangan", "Tanggal Asesmen");
$uitable = new Table($header,"",NULL,true);
$uitable->setName("laporan_asesmen_perawat");
$uitable->setExcelButtonEnable(false);
$uitable->setAddButtonEnable(false);
$uitable->setPrintButtonEnable(false);
$uitable->setEditButtonEnable(false);
$uitable->setDelButtonEnable(false);
//$uitable->setDetailButtonEnable(true);
$btn=new Button("", "", "");
$btn->addClass("btn-warning")
    ->setIsButton(Button::$ICONIC)
    ->setIcon("fa fa-eye");
$uitable->addContentButton("show", $btn);
$excel=new Button("", "", "");
$excel->addClass("btn-success")
    ->setIsButton(Button::$ICONIC)
    ->setIcon("fa fa-file-excel-o ");
$uitable->addContentButton("d_excel", $excel);

if(isset($_POST['command']))
{
    //$adapter = new SimpleAdapter(true,"No.");
    $adapter = new LaporanAsesmenPerawatAdapter();
    $adapter->setUseNumber(true, "No.","back.");
    $adapter->add("Pasien","nama_pasien");
    $adapter->add("No. Reg","noreg_pasien");
    $adapter->add("NRM","nrm_pasien");
    $adapter->add("Tanggal Masuk","waktu_masuk", "date d M Y");
    $adapter->add("Dokter","nama_dokter");
    $adapter->add("Perawat","nama_perawat");
    $adapter->add("Ruangan","ruangan", "unslug");
    $adapter->add("Tanggal Asesmen","waktu", "date d M Y");
    
    //For Excel
    $adapter->add("nama_pasien","nama_pasien");
    $adapter->add("jk","jk", "trivial_0_Laki-Laki_Perempuan");
    $adapter->add("tgl_lahir_pasien","tgl_lahir_pasien", "date d M Y");
    $adapter->add("alamat_pasien","alamat_pasien");
    $adapter->add("tgl_asesmen","waktu", "date d M Y");
    $adapter->add("jam_asesmen","waktu", "date H:i:s");
    $adapter->add("tanggal_masuk","waktu_masuk", "date d M Y");
    $adapter->add("nama_dokter","nama_dokter");
    $adapter->add("nama_dokter_dpjp","nama_dokter_dpjp");
    $adapter->add("nama_perawat","nama_perawat");
    $adapter->add("diagnosa_perawat","diagnosa_perawat");
    $adapter->add("bb","bb");
    $adapter->add("tb","tb");
    $adapter->add("rujukan","rujukan");
    $adapter->add("kedatangan","kedatangan");
    $adapter->add("tanda_vital","tanda_vital");
    $adapter->add("tekanan_darah","tekanan_darah");
    $adapter->add("frek_nafas","frek_nafas");
    $adapter->add("nadi","nadi");
    $adapter->add("suhu","suhu");
    $adapter->add("rkpr_sumber_data","rkpr_sumber_data");
    $adapter->add("rkpr_sumber_data_lain","rkpr_sumber_data_lain");
    $adapter->add("rkpr_keluhan_utama","rkpr_keluhan_utama");
    $adapter->add("rkpr_keadaan_umum","rkpr_keadaan_umum");
    $adapter->add("rkpr_gizi","rkpr_gizi");
    $adapter->add("rkpr_tindakan_resusitasi","rkpr_tindakan_resusitasi");
    $adapter->add("rkpr_tindakan_resusitasi_lain","rkpr_tindakan_resusitasi_lain");
    $adapter->add("rkpr_saturasi_o2","rkpr_saturasi_o2");
    $adapter->add("rkpr_pada","rkpr_pada");
    $adapter->add("psks_status_nikah","psks_status_nikah");
    $adapter->add("psks_anak","psks_anak");
    $adapter->add("psks_jumlah_anak","psks_jumlah_anak");
    $adapter->add("psks_pend_akhir","psks_pend_akhir");
    $adapter->add("psks_warganegara","psks_warganegara");
    $adapter->add("psks_pekerjaan","psks_pekerjaan");
    $adapter->add("psks_agama","psks_agama");
    $adapter->add("psks_tinggal_bersama","psks_tinggal_bersama");
    $adapter->add("psks_tinggal_bersama_nama","psks_tinggal_bersama_nama");
    $adapter->add("psks_tinggal_bersama_telp","psks_tinggal_bersama_telp");
    $adapter->add("rks_riwayat_penyakit_hipertensi","rks_riwayat_penyakit_hipertensi");
    $adapter->add("rks_riwayat_penyakit_diabetes","rks_riwayat_penyakit_diabetes");
    $adapter->add("rks_riwayat_penyakit_jantung","rks_riwayat_penyakit_jantung");
    $adapter->add("rks_riwayat_penyakit_stroke","rks_riwayat_penyakit_stroke");
    $adapter->add("rks_riwayat_penyakit_dialisis","rks_riwayat_penyakit_dialisis");
    $adapter->add("rks_riwayat_penyakit_asthma","rks_riwayat_penyakit_asthma");
    $adapter->add("rks_riwayat_penyakit_kejang","rks_riwayat_penyakit_kejang");
    $adapter->add("rks_riwayat_penyakit_hati","rks_riwayat_penyakit_hati");
    $adapter->add("rks_riwayat_penyakit_kanker","rks_riwayat_penyakit_kanker");
    $adapter->add("rks_riwayat_penyakit_tbc","rks_riwayat_penyakit_tbc");
    $adapter->add("rks_riwayat_penyakit_glaukoma","rks_riwayat_penyakit_glaukoma");
    $adapter->add("rks_riwayat_penyakit_std","rks_riwayat_penyakit_std");
    $adapter->add("rks_riwayat_penyakit_pendarahan","rks_riwayat_penyakit_pendarahan");
    $adapter->add("rks_riwayat_penyakit_lain","rks_riwayat_penyakit_lain");
    $adapter->add("rks_riwayat_penyakit_lain_lain","rks_riwayat_penyakit_lain_lain");
    $adapter->add("rks_pengobatan_saat_ini","rks_pengobatan_saat_ini");
    $adapter->add("rks_pengobatan_saat_ini_ket","rks_pengobatan_saat_ini_ket");
    $adapter->add("rks_riwayat_operasi","rks_riwayat_operasi");
    $adapter->add("rks_riwayat_operasi_ket","rks_riwayat_operasi_ket");
    $adapter->add("rks_riwayat_transfusi","rks_riwayat_transfusi");
    $adapter->add("rks_reaksi_transfusi","rks_reaksi_transfusi");
    $adapter->add("rks_reaksi_transfusi_ket","rks_reaksi_transfusi_ket");
    $adapter->add("rks_riwayat_penyakit_keluarga","rks_riwayat_penyakit_keluarga");
    $adapter->add("rks_ketergantungan","rks_ketergantungan");
    $adapter->add("rks_obat","rks_obat");
    $adapter->add("rks_jenis_jmlh_perhari","rks_jenis_jmlh_perhari");
    $adapter->add("nutrisi_makan_perhari","nutrisi_makan_perhari");
    $adapter->add("nutrisi_minum_perhari","nutrisi_minum_perhari");
    $adapter->add("nutrisi_diet_khusus","nutrisi_diet_khusus");
    $adapter->add("nutrisi_keluhan","nutrisi_keluhan");
    $adapter->add("nutrisi_keluhan_ket","nutrisi_keluhan_ket");
    $adapter->add("nutrisi_perubahan_bb","nutrisi_perubahan_bb");
    $adapter->add("nutrisi_perubahan_bb_penurunan","nutrisi_perubahan_bb_penurunan");
    $adapter->add("nutrisi_perubahan_bb_kenaikan","nutrisi_perubahan_bb_kenaikan");
    $adapter->add("alergi","alergi");
    $adapter->add("alergi_obat_tipereaksi","alergi_obat_tipereaksi");
    $adapter->add("alergi_makanan_tipereaksi","alergi_makanan_tipereaksi");
    $adapter->add("alergi_lain_tipereaksi","alergi_lain_tipereaksi");
    $adapter->add("nyeri","nyeri");
    $adapter->add("sifat_nyeri","sifat_nyeri");
    $adapter->add("nyeri_lokasi","nyeri_lokasi");
    $adapter->add("nyeri_sejak","nyeri_sejak");
    $adapter->add("skala_nyeri","skala_nyeri");
    $adapter->add("get_up_go_test_jalan_bungkuk","get_up_go_test_jalan_bungkuk");
    $adapter->add("get_up_go_test_topang_duduk","get_up_go_test_topang_duduk");
    $adapter->add("get_up_go_test","get_up_go_test");
    $adapter->add("bicara","bicara");
    $adapter->add("penerjemah","penerjemah");
    $adapter->add("bahasa","bahasa");
    $adapter->add("bhs_isyarat","bhs_isyarat");
    $adapter->add("hambatan_belajar","hambatan_belajar");
    $adapter->add("hambatan_belajar_ket","hambatan_belajar_ket");
    $adapter->add("catatan_lain","catatan_lain");
    
    $dbtable = new DBTable($db,"smis_mr_assesmen_awal_perawat");
    if(isset($_POST['dari']) && isset($_POST['sampai'])) {
        $dbtable->addCustomKriteria(NULL, "waktu_masuk >= '".$_POST['dari']."' AND waktu_masuk <= '".$_POST['sampai']."'");
    }
    $dbresponder = new LapAsesmenPerawatResponder($dbtable,$uitable,$adapter);
    $data = $dbresponder->command($_POST ['command']);
	echo json_encode($data);	
	return;
}

$uitable->clearContent();
$uitable->addModal("dari", "date", "Dari", "")
		->addModal("sampai", "date", "Sampai", "");
 
$modal = $uitable->getModal();
$form = $modal->getForm ();

$button = new Button ( "", "", "" );
$button->setIsButton ( Button::$ICONIC );
$button->setIcon ( "fa fa-refresh" );
$button->setClass("btn-primary");
$button->setAction ( "laporan_asesmen_perawat.view()" );
$form->addElement ( "", $button );

echo "<h2>Laporan Assesmen Perawat</h2>";
echo $form->getHtml();
echo $uitable->getHtml();

echo addJS("framework/bootstrap/js/bootstrap-datepicker.js");
echo addJS("framework/smis/js/table_action.js");
echo addCSS("framework/bootstrap/css/datepicker.css");
?>

<script type="text/javascript">

var laporan_asesmen_perawat;
$(document).ready(function(){
    $('.mydate').datepicker();
    var column = new Array('nama_pasien','noreg_pasien','nrm_pasien','nama_dokter','ruangan');
    laporan_asesmen_perawat = new TableAction("laporan_asesmen_perawat", "medical_record", "asesmen_perawat", column);
    //laporan_asesmen_perawat.view();
    laporan_asesmen_perawat.addRegulerData = function(data) {
        data['dari'] = $("#laporan_asesmen_perawat_dari").val();
        data['sampai'] = $("#laporan_asesmen_perawat_sampai").val();
        return data;
    };
    laporan_asesmen_perawat.show=function(id){
        var data=$("#laporan_asesmen_perawat_"+id).html();
        showWarning("Laporan Asesmen Perawat ", data);
    };
    laporan_asesmen_perawat.d_excel=function(id){
        var data=this.getViewData();
        data['id'] = id;
        data['command']="excel";
        download(data);
    }
});

</script>