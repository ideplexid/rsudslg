<?php

require_once 'medical_record/class/adapter/LaporanAsesmenJantungAdapter.php';
require_once 'medical_record/class/responder/LapAsesmenJantungResponder.php';

global $db;
$header = array("No.","Pasien","No. Reg","NRM", "Tanggal Masuk", "Dokter", "Ruangan", "Tanggal Asesmen");
$uitable = new Table($header,"",NULL,true);
$uitable->setName("laporan_asesmen_jantung");
$uitable->setExcelButtonEnable(false);
$uitable->setAddButtonEnable(false);
$uitable->setPrintButtonEnable(false);
$uitable->setEditButtonEnable(false);
$uitable->setDelButtonEnable(false);
//$uitable->setDetailButtonEnable(true);
$btn=new Button("", "", "");
$btn->addClass("btn-warning")
    ->setIsButton(Button::$ICONIC)
    ->setIcon("fa fa-eye");
$uitable->addContentButton("show", $btn);
$excel=new Button("", "", "");
$excel->addClass("btn-success")
    ->setIsButton(Button::$ICONIC)
    ->setIcon("fa fa-file-excel-o ");
$uitable->addContentButton("d_excel", $excel);

if(isset($_POST['command']))
{
    //$adapter = new SimpleAdapter(true,"No.");
    $adapter = new LaporanAsesmenJantungAdapter();
    $adapter->setUseNumber(true, "No.","back.");
    $adapter->add("Pasien","nama_pasien");
    $adapter->add("No. Reg","noreg_pasien");
    $adapter->add("NRM","nrm_pasien");
    $adapter->add("Tanggal Masuk","waktu_masuk", "date d M Y");
    $adapter->add("Dokter","nama_dokter");
    $adapter->add("Ruangan","ruangan", "unslug");
    $adapter->add("Tanggal Asesmen","waktu", "date d M Y");
    
    //For Excel
    $adapter->add("nama_pasien","nama_pasien");
    $adapter->add("jk","jk", "trivial_0_Laki-Laki_Perempuan");
    $adapter->add("tgl_lahir_pasien","tgl_lahir_pasien", "date d M Y");
    $adapter->add("alamat_pasien","alamat_pasien");
    $adapter->add("tgl_asesmen","waktu", "date d M Y");
    $adapter->add("jam_asesmen","waktu", "date H:i:s");
    $adapter->add("tanggal_masuk","waktu_masuk", "date d M Y");
    $adapter->add("anamneses_utama","anamneses_utama");
    $adapter->add("anamneses_penyakit_skrg","anamneses_penyakit_skrg");
    $adapter->add("anamneses_pengobatan","anamneses_pengobatan");
    $adapter->add("torak","torak");
    $adapter->add("torak_ket","torak_ket");
    $adapter->add("iktus_kordis","iktus_kordis");
    $adapter->add("iktus_kordis_ket","iktus_kordis_ket");
    $adapter->add("iktus_kordis_lokasi","iktus_kordis_lokasi");
    $adapter->add("pulpasi","pulpasi");
    $adapter->add("pulpasi_ket","pulpasi_ket");
    $adapter->add("palpasi_iktus_kordis","palpasi_iktus_kordis");
    $adapter->add("palpasi_iktus_kordis_ket","palpasi_iktus_kordis_ket");
    $adapter->add("palpasi_iktus_kordis_lokasi","palpasi_iktus_kordis_lokasi");
    $adapter->add("palpasi_thrill","palpasi_thrill");
    $adapter->add("palpasi_thrill_ket","palpasi_thrill_ket");
    $adapter->add("palpasi_thrill_lokasi","palpasi_thrill_lokasi");
    $adapter->add("perkusi_batas_atas","perkusi_batas_atas");
    $adapter->add("perkusi_batas_bawah","perkusi_batas_bawah");
    $adapter->add("perkusi_batas_kanan","perkusi_batas_kanan");
    $adapter->add("perkusi_batas_kiri","perkusi_batas_kiri");
    $adapter->add("sju","sju");
    $adapter->add("sju_s1","sju_s1");
    $adapter->add("sju_s2","sju_s2");
    $adapter->add("extra_systole","extra_systole");
    $adapter->add("gallop","gallop");
    $adapter->add("murmur_fase","murmur_fase");
    $adapter->add("murmur_fase_ket","murmur_fase_ket");
    $adapter->add("murmur_lokasi","murmur_lokasi");
    $adapter->add("murmur_lokasi_ket","murmur_lokasi_ket");
    $adapter->add("murmur_kualitas","murmur_kualitas");
    $adapter->add("murmur_kualitas_ket","murmur_kualitas_ket");
    $adapter->add("murmur_grade","murmur_grade");
    $adapter->add("murmur_penjalaran","murmur_penjalaran");
    $adapter->add("murmur_penjalaran_ket","murmur_penjalaran_ket");
    $adapter->add("murmur_opening_snap","murmur_opening_snap");
    $adapter->add("murmur_friction_rub","murmur_friction_rub");
    $adapter->add("paru","paru");
    $adapter->add("paru_ket","paru_ket");
    $adapter->add("ecg","ecg");
    $adapter->add("thorax_foto","thorax_foto");
    $adapter->add("echocardiografi","echocardiografi");
    $adapter->add("diagnosis","diagnosis");
    $adapter->add("terapi","terapi");
    $adapter->add("rencana_kerja","rencana_kerja");
    $adapter->add("tgl_pulang","waktu_pulang", "date d M Y");
    $adapter->add("jam_pulang","waktu_pulang", "date H:i:s");
    $adapter->add("tgl_kontrol_klinik","waktu_kontrol_klinik", "date d M Y");
    $adapter->add("dirawat_diruang","dirawat_diruang");
    
    $dbtable = new DBTable($db,"smis_mr_assesmen_jantung");
    if(isset($_POST['dari']) && isset($_POST['sampai'])) {
        $dbtable->addCustomKriteria(NULL, "waktu_masuk >= '".$_POST['dari']."' AND waktu_masuk <= '".$_POST['sampai']."'");
    }
    $dbresponder = new LapAsesmenJantungResponder($dbtable,$uitable,$adapter);
    $data = $dbresponder->command($_POST ['command']);
	echo json_encode($data);	
	return;
}

$uitable->clearContent();
$uitable->addModal("dari", "date", "Dari", "")
		->addModal("sampai", "date", "Sampai", "");
 
$modal = $uitable->getModal();
$form = $modal->getForm ();

$button = new Button ( "", "", "" );
$button->setIsButton ( Button::$ICONIC );
$button->setIcon ( "fa fa-refresh" );
$button->setClass("btn-primary");
$button->setAction ( "laporan_asesmen_jantung.view()" );
$form->addElement ( "", $button );

echo "<h2>Laporan Asesmen Jantung</h2>";
echo $form->getHtml();
echo $uitable->getHtml();

echo addJS("framework/bootstrap/js/bootstrap-datepicker.js");
echo addJS("framework/smis/js/table_action.js");
echo addCSS("framework/bootstrap/css/datepicker.css");

?>

<script type="text/javascript">

var laporan_asesmen_jantung;
$(document).ready(function(){
    $('.mydate').datepicker();
    var column = new Array('nama_pasien','noreg_pasien','nrm_pasien','nama_dokter','ruangan');
    laporan_asesmen_jantung = new TableAction("laporan_asesmen_jantung", "medical_record", "asesmen_jantung", column);
    //laporan_asesmen_jantung.view();
    laporan_asesmen_jantung.addRegulerData = function(data) {
        data['dari'] = $("#laporan_asesmen_jantung_dari").val();
        data['sampai'] = $("#laporan_asesmen_jantung_sampai").val();
        return data;
    };
    laporan_asesmen_jantung.show=function(id){
        var data=$("#laporan_asesmen_jantung_"+id).html();
        showWarning("Laporan Asesmen Jantung ", data);
    };
    laporan_asesmen_jantung.d_excel=function(id){
        var data=this.getViewData();
        data['id'] = id;
        data['command']="excel";
        download(data);
    }
});

</script>