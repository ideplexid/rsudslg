<?php 

global $db;


$uitable=new Table(array());
$uitable->setName("analisa_kuisioner_rj");
if(isset($_POST['command']) && $_POST['command']!=""){
	$result=array();
	$result['chart_area']="CHART AREA";
	$result['print_area']="PRINT AREA";
	$result['saran_area']="SARAN AREA";
	
	/*KODE FOR GETTING ALL DATA*/
	$query="SELECT * FROM smis_mr_quisioner_rj WHERE prop!='del' AND tanggal>='".$_POST['dari']."' AND tanggal<'".$_POST['sampai']."';";
	$set=$db->get_result($query,false);
	$set_chart=array();
	$set_saran=array();
	$set_print=array();
	/*END OF GETTING DATA*/
	$list=array();
	$list['a1_pend_cepat']			=" 1. Kecepatan Pendaftaran";
	$list['a2_pend_ramah']			=" 2. Keramahan Petugas";
	$list['a3_pend_jelas']			=" 3. Kejelasan Tulisan Pada Kartu Obat";
	$list['b1_keu_cepat']			=" 4. Kecepatan Pelayanan Petugas";
	$list['b2_keu_ramah']			=" 5. Keramahan Petugas";
	$list['b3_keu_jelas']			=" 6. Kejelasan Perhitungan Keuangan";
	$list['c1_lama_tunggu']			=" 7. Lama Menunggu Untuk Pemeriksaan";
	$list['c2_nyaman_tunggu']		=" 8. Kenyamanan Ruang Tunggu";
	$list['c3_bersih_tunggu']		=" 9. Kebersihan Ruang Tunggu";
	$list['c4_indah_taman']			="10. Keindahan Taman";
	$list['c5_jelas_papan']			="11. Kejelasan Papan Petunjuk";
	$list['d1_poli_cepat_perawat']	="12. Kecepatan Perawat Membantu";
	$list['d2_poli_ramah_perawat']	="13. Keramahan Perawat";
	$list['d3_poli_jelas_perawat']	="14. Kejelasan Informasi dari Perawat";
	$list['d4_poli_cepat_dokter']	="15. Kecepatan Pemeriksaan Dokter";
	$list['d5_poli_ramah_dokter']	="16. Keramahan Dokter";
	$list['d6_poli_jelas_dokter']	="17. Kejelasan Informasi dari Dokter";
	$list['d7_poli_rapi_tempat']	="18. Kerapihan tempat Periksa";
	$list['d8_poli_bersih_tempat']	="19. Kebersihan Tempat Periksa";
	$list['e1_lab_cepat']			="20. Kecepatan Pemeriksaaan";
	$list['e2_lab_ramah']			="21. Keramahan Petugas";
	$list['e3_lab_jelas']			="22. Kejelasan Informasi";
	$list['f1_rad_cepat']			="23. Kecepatan Pemeriksaaan";
	$list['f2_rad_ramah']			="24. Keramahan Petugas";
	$list['f3_rad_jelas']			="25. Kejelasan Informasi";
	$list['g1_igd_cepat']			="26. Kecepatan Pelayanan";
	$list['g2_igd_ramah']			="27. Keramahan Petugas";
	$list['g3_igd_obat']			="28. Pelayanan Obat";
	$list['g4_igd_jelas']			="29. Kejelasan Informasi";
	$list['h1_fisio_cepat']			="30. Kecepatan Pelayanan";
	$list['h2_fisio_ramah']			="31. Keramahan Petugas";
	$list['h3_fisio_jelas']			="32. Kejelasan Informasi";
	$list['h4_fisio_lengkap']		="33. Kelengkapan Alat Terapi";
	
	$title=array();
	$title['a1_pend_cepat']="PENDAFTARAN";
	$title['b1_keu_cepat']="KEUANGAN";
	$title['c1_lama_tunggu']="MENUNGGU";
	$title['d1_poli_cepat_perawat']="POLIKLINIK";
	$title['e1_lab_cepat']="LABORATORIUM";
	$title['f1_rad_cepat']="RADIOLOGI";
	$title['g1_igd_cepat']="INSTALASI GAWAT DARURAT";
	$title['h1_fisio_cepat']="FISIOTHERAPY";
	foreach($set as $one){
		foreach($list as $l=>$c){
			if(!isset($set_chart[$l])){
				$set_chart[$l]=array();
				$set_chart[$l]['count_sb']=0;
				$set_chart[$l]['count_ba']=0;
				$set_chart[$l]['count_ku']=0;
				$set_chart[$l]['count_bu']=0;
				$set_chart[$l]['jumlah']=0;
			}
			$set_chart[$l]['count_sb']+=($one[$l]=="4"?1:0);
			$set_chart[$l]['count_ba']+=($one[$l]=="3"?1:0);
			$set_chart[$l]['count_ku']+=($one[$l]=="2"?1:0);
			$set_chart[$l]['count_bu']+=($one[$l]=="1"?1:0);
			$set_chart[$l]['jumlah']+=($one[$l]!="0"?1:0);				
		}
	}
	
	/*START KODE FOR PRINT AREA*/	
	$table=new TablePrint("analisa_kuisioner_rj");
	$table->setMaxWidth(false);
	$table->setTableClass("analisa_kuisioner");
	$table->setDefaultBootrapClass(false);
	$table->setTableClass("tbl_analisa_kuisioner_rj");
	$table->addColumn("RESUME HASIL KUISIONER RAWAT JALAN",6,1,NULL,NULL,"center  bold f20");
	$table->commit("body");
	$table->addColumn("DARI ",1,1);
	$table->addColumn(" : ",1,1);
	$table->addColumn(ArrayAdapter::format("date d M Y", $_POST['dari']),4,1);
	$table->commit("body");
	$table->addColumn("SAMPAI ",1,1);
	$table->addColumn(" : ",1,1);
	$table->addColumn(ArrayAdapter::format("date d M Y", $_POST['sampai']),4,1);
	$table->commit("body");	
	$table->addColumn("KRITERIA",1,1,NULL,NULL,"left bold ltop lbottom");
	$table->addColumn("BURUK",1,1,NULL,NULL,"left lbottom ltop bold lleft");
	$table->addColumn("KURANG",1,1,NULL,NULL,"left lbottom ltop bold lleft");
	$table->addColumn("BAIK",1,1,NULL,NULL,"left lbottom ltop bold lleft");
	$table->addColumn("SANGAT BAIK",1,1,NULL,NULL,"left ltop lbottom bold lleft");
	$table->addColumn("TOTAL",1,1,NULL,NULL,"left lbottom ltop bold lleft");
	$table->commit("body");
	foreach($set_chart as $scid=>$sc){
		$sbaik=$sc['count_sb'];
		$baik=$sc['count_ba'];
		$kurang=$sc['count_ku'];
		$buruk=$sc['count_bu'];		
		$p_sbaik=round($sc['count_sb']*100/$sc['jumlah'],2);
		$p_baik=round($sc['count_ba']*100/$sc['jumlah'],2);
		$p_kurang=round($sc['count_ku']*100/$sc['jumlah'],2);
		$p_buruk=100-$p_kurang-$p_baik-$p_sbaik;		
		if(isset($title[$scid])){
			$table->addColumn($title[$scid],1,1,NULL,NULL,"left ltop italic");
			$table->addColumn("",1,1,NULL,NULL,"left ltop bold lleft");
			$table->addColumn("",1,1,NULL,NULL,"left ltop bold lleft");
			$table->addColumn("",1,1,NULL,NULL,"left ltop bold lleft");
			$table->addColumn("",1,1,NULL,NULL,"left ltop bold lleft");
			$table->addColumn("",1,1,NULL,NULL,"left ltop bold lleft");
			$table->commit("body");
		}		
		$table->addColumn($list[$scid],1,1,NULL,NULL,"gleft");
		$table->addColumn($sbaik." (".$p_sbaik."%) ",1,1,NULL,NULL,"lleft");
		$table->addColumn($baik." (".$p_baik."%) ",1,1,NULL,NULL,"lleft");
		$table->addColumn($kurang." (".$p_kurang."%) ",1,1,NULL,NULL,"lleft");
		$table->addColumn($buruk." (".$p_buruk."%) ",1,1,NULL,NULL,"lleft");
		$table->addColumn($sc['jumlah']." (100%)",1,1,NULL,NULL,"lleft");
		$table->commit("body");
	}
	$result['print_area']=$table->getHtml();
	/*END OF KODE FOR PRINT AREA*/
	
	
	/*START KODE FOR CHART AREA*/
	$chart="";
	foreach($set_chart as $scid=>$sc){
		$sbaik=$sc['count_sb'];
		$baik=$sc['count_ba'];
		$kurang=$sc['count_ku'];
		$buruk=$sc['count_bu'];
		$p_sbaik=round($sc['count_sb']*100/$sc['jumlah'],2);
		$p_baik=round($sc['count_ba']*100/$sc['jumlah'],2);
		$p_kurang=round($sc['count_ku']*100/$sc['jumlah'],2);
		$p_buruk=100-$p_kurang-$p_baik-$p_sbaik;
		if(isset($title[$scid])){
			$chart.="</br></br><h4>".$title[$scid]."</h4>";
		}
		$chart.=$list[$scid]." ( ".$sc['jumlah']." Orang ) ";
		$div="
			<div class='progress'>
			  <div class='bar bar-info' style='width: ".$p_sbaik."%;'> SANGAT BAIK [ ".$sbaik." Orang / ".$p_sbaik."% ] </div>
			  <div class='bar bar-success' style='width: ".$p_baik."%;'> BAIK [ ".$baik." Orang / ".$p_baik."% ] </div>
			  <div class='bar bar-warning' style='width: ".$p_kurang."%;'> KURANG [ ".$kurang." Orang / ".$p_kurang."% ] </div>
			  <div class='bar bar-danger' style='width: ".$p_buruk."%;'> BURUK [ ".$buruk." Orang / ".$p_buruk."% ] </div>
			 </div>
		";
		$chart.=$div;
	}
	$result['chart_area']=$chart;
	/*END OF KODE FOR CHART AREA*/
	
	/*START OF KODE FOR SARAN AREA*/
	$table=new TablePrint("analisa_kuisioner_rj_saran");
	$table->setMaxWidth(false);
	$table->setDefaultBootrapClass(false);
	$table->setTableClass("analisa_kuisioner_rj_saran");
	$table->addColumn("SARAN MASUK KUISIONER RAWAT JALAN",6,1,NULL,NULL,"center bold f20");
	$table->commit("body");
	$table->addColumn("DARI ",1,1);
	$table->addColumn(" : ",1,1);
	$table->addColumn(ArrayAdapter::format("date d M Y", $_POST['dari']),4,1);
	$table->commit("body");
	$table->addColumn("SAMPAI ",1,1);
	$table->addColumn(" : ",1,1);
	$table->addColumn(ArrayAdapter::format("date d M Y", $_POST['sampai']),4,1);
	$table->commit("body");
	$table->addColumn("NO.",1,1,NULL,NULL,"left bold ltop lbottom");
	$table->addColumn("NAMA",1,1,NULL,NULL,"left bold ltop lbottom");
	$table->addColumn("TANGGAL",1,1,NULL,NULL,"left bold ltop lbottom");
	$table->addColumn("SARAN",3,1,NULL,NULL,"left bold ltop lbottom");
	$table->commit("body");
	$no=0;
	foreach($set as $one){
		$table->addColumn((++$no).".",1,1);
		$table->addColumn($one['nama'],1,1);
		$table->addColumn(ArrayAdapter::format("date d M Y", $one['tanggal']),1,1);
		$table->addColumn($one['saran'],3,1);
		$table->commit("body");
	}
	$result['saran_area']=$table->getHtml();
	/*END OF KODE FOR SARAN AREA*/
	
	$pack=new ResponsePackage();
	$pack->setStatus(ResponsePackage::$STATUS_OK);
	$pack->setAlertVisible(false);
	$pack->setContent($result);
	echo json_encode($pack->getPackage());
	return;
}

$uitable->addModal("dari", "date", "Dari", "");
$uitable->addModal("sampai", "date", "Sampai", "");
$form=$uitable->getModal()->getForm()->setTitle("Analisa Kuisioner RI");


$load=new Button("", "", "Load");
$load->addClass("btn btn-primary");
$load->setIsButton(Button::$ICONIC_TEXT);
$load->setIcon("fa fa-circle-o");
$load->setAction("analisa_kuisioner_rj.load()");

$print_list=new Button("", "", "List");
$print_list->addClass("btn btn-primary");
$print_list->setIsButton(Button::$ICONIC_TEXT);
$print_list->setIcon("fa fa-bar-chart");
$print_list->setAction("analisa_kuisioner_rj.print_list()");

$print_chart=new Button("", "", "Chart");
$print_chart->addClass("btn btn-primary");
$print_chart->setIsButton(Button::$ICONIC_TEXT);
$print_chart->setIcon("fa fa-file");
$print_chart->setAction("analisa_kuisioner_rj.print_chart()");

$print_saran=new Button("", "", "Saran");
$print_saran->addClass("btn btn-primary");
$print_saran->setIsButton(Button::$ICONIC_TEXT);
$print_saran->setIcon("fa fa-book");
$print_saran->setAction("analisa_kuisioner_rj.print_saran()");

$grup=new ButtonGroup("");
$grup->addButton( $print_list);
$grup->addButton($print_chart);
$grup->addButton($print_saran);
$form->addElement("", $load);
$form->addElement("", $grup);

$tabs=new Tabulator("analisa_kuisioner_tabulator","");
$tabs->add("analisa_kuisioner_rj_tabs_chart", "Chart", "<div id='analisa_kuisioner_rj_chart_area'></div>",Tabulator::$TYPE_HTML,"fa fa-bar-chart");
$tabs->add("analisa_kuisioner_rj_tabs_print", "List", "<div id='analisa_kuisioner_rj_print_area'></div>",Tabulator::$TYPE_HTML,"fa fa-file");
$tabs->add("analisa_kuisioner_rj_tabs_saran", "Saran", "<div id='analisa_kuisioner_rj_saran_area'></div>",Tabulator::$TYPE_HTML,"fa fa-book");

echo $form->getHtml();
echo "<div class='clear'></div>";
echo $tabs->getHtml();
echo addJS ( "framework/bootstrap/js/bootstrap-datepicker.js" );
echo addJS ( "framework/smis/js/table_action.js" );
echo addCSS ( "framework/bootstrap/css/datepicker.css" );
echo addCSS ( "medical_record/resource/css/analisa_kuisioner_rj.css",false );
?>
<script type="text/javascript">
var analisa_kuisioner_rj;
$(document).ready(function(){
	$(".mydate").datepicker();
	analisa_kuisioner_rj=new TableAction("analisa_kuisioner_rj","medical_record","analisa_kuisioner_rj", new Array());
	analisa_kuisioner_rj.addRegulerData=function(a){
		a['dari']=$("#analisa_kuisioner_rj_dari").val();
		a['sampai']=$("#analisa_kuisioner_rj_sampai").val();
		return a;
	};

	analisa_kuisioner_rj.print_list=function(){
		var html=$("#analisa_kuisioner_rj_print_area").html();
		smis_print(html);
	};

	analisa_kuisioner_rj.print_chart=function(){
		var html=$("#analisa_kuisioner_rj_chart_area").html();
		smis_print(html);
	};

	analisa_kuisioner_rj.print_saran=function(){
		var html=$("#analisa_kuisioner_rj_saran_area").html();
		smis_print(html);
	};
	
	analisa_kuisioner_rj.load=function(){
		var a=this.getRegulerData();
		a['command']="load";
		showLoading();
		$.post("",a,function(res){
			var json=getContent(res);
			dismissLoading();
			if(json!=null){
				$("#analisa_kuisioner_rj_chart_area").html(json.chart_area);
				$("#analisa_kuisioner_rj_print_area").html(json.print_area);
				$("#analisa_kuisioner_rj_saran_area").html(json.saran_area);
			}else{
				$("#analisa_kuisioner_rj_chart_area").html("");
				$("#analisa_kuisioner_rj_print_area").html("");
				$("#analisa_kuisioner_rj_saran_area").html("");
			}
		});
	};
});
</script>
