<?php 

global $db;


$uitable=new Table(array());
$uitable->setName("analisa_kuisioner_ri");
if(isset($_POST['command']) && $_POST['command']!=""){
	$result=array();
	$result['chart_area']="CHART AREA";
	$result['print_area']="PRINT AREA";
	$result['saran_area']="SARAN AREA";
	
	/*KODE FOR GETTING ALL DATA*/
	$query="SELECT * FROM smis_mr_quisioner_ri WHERE prop!='del' AND tanggal>='".$_POST['dari']."' AND tanggal<'".$_POST['sampai']."';";
	$set=$db->get_result($query,false);
	$set_chart=array();
	$set_saran=array();
	$set_print=array();
	/*END OF GETTING DATA*/
	$list=array();
	$list['a1_pend_cepat']				=" 1. Kecepatan Pendaftaran";
	$list['a2_pend_ramah']				=" 2. Keramahan Petugas Pendaftaran";
	$list['b1_dok_cepat']				=" 3. Kecepatan Dokter dalam Menangani Keluhan Penyakit Anda";
	$list['b2_dok_ramah']				=" 4. Keramahan Dokter";
	$list['b3_dok_tanggap']				=" 5. Tanggapan Dokter Terhadap Keluhan Anda";
	$list['b4_dok_jelas']				=" 6. Kejelasan Informasi";
	$list['c1_perawat_cepat']			=" 7. Kecepatan Perawat Untuk Memberikan Bantuan Ketika Anda Perlukan";
	$list['c2_perawat_ramah']			=" 8. Keramahan Perawat";
	$list['c3_perawat_jelas']			=" 9. Kejelasan Informasi Tentang Tindakan - Tindakan Perawat yang dilakukan";
	$list['c4_perawat_teratur']			="10. Keteraturan Pengukuran Tekanan Darah dan Suhu Tubuh";
	$list['d1_makan_menu']				="11. Menu yang dihidangkan";
	$list['d2_makan_tata']				="12. Penataan Makanan atau Penampilan Makanan";
	$list['d3_makan_tepat']				="13. Ketepatan Waktu Penyajian Makanan";
	$list['e1_nyaman_bersih']			="14. Kebersihan Ruangan";
	$list['e2_nyaman_bising']			="15. Kebisingan";
	$list['e3_nyaman_nyamuk']			="16. Gangguan dari Nyamuk";
	$list['e4_nyaman_rapi']				="17. Perapihan Tempat Tidur";
	$list['e5_nyaman_terang']			="18. Penerangan di Kamar Anda";
	$list['e6_nyaman_bersih_wc']		="19. Kebersihan Kamar Mandi / WC";
	$list['e7_nyaman_air_wc']			="20. Persediaan Air di Kamar Mandi / WC";
	$list['e8_nyaman_sampah']			="21. Pembuangan Sampah dari Keranjang Sampah di Kamar Anda";
	$list['f1_sarana_cukup_alat']		="22. Kecukupan Peralatan di RS ini Untuk Pemeriksaan/Mengobati Penyakit Anda";
	$list['f2_sarana_obat_lengkap']		="23. Kelengkapan Obat Oleh Rumah Sakit";
	$list['f3_sarana_cepat_keuangan']	="24. Kecepatan Petugas Dalam Melayani Administrasi Keuangan (Tidak Lbeih dari 15 Menit) ";
	$list['f4_sarana_jelas_biaya']		="25. Kejelasan Perincian Biaya";
	$list['f5_sarana_sesuai_harga']		="26. Kesesuaian Harga Obat-Obatan";
	
	$title=array();
	$title['a1_pend_cepat']="PENDAFTARAN";
	$title['b1_dok_cepat']="DOKTER";
	$title['c1_perawat_cepat']="PERAWAT";
	$title['d1_makan_menu']="MAKANAN";
	$title['e1_nyaman_bersih']="KENYAMANAN & KEBERSIHAN";
	$title['f1_sarana_cukup_alat']="SARANA MEDIS";
	
	foreach($set as $one){
		foreach($list as $l=>$c){
			if(!isset($set_chart[$l])){
				$set_chart[$l]=array();
				$set_chart[$l]['count_sb']=0;
				$set_chart[$l]['count_ba']=0;
				$set_chart[$l]['count_ku']=0;
				$set_chart[$l]['count_bu']=0;
				$set_chart[$l]['jumlah']=0;
			}
			$set_chart[$l]['count_sb']+=($one[$l]=="4"?1:0);
			$set_chart[$l]['count_ba']+=($one[$l]=="3"?1:0);
			$set_chart[$l]['count_ku']+=($one[$l]=="2"?1:0);
			$set_chart[$l]['count_bu']+=($one[$l]=="1"?1:0);
			$set_chart[$l]['jumlah']+=($one[$l]!="0"?1:0);				
		}
	}
	
	/*START KODE FOR PRINT AREA*/	
	$table=new TablePrint("analisa_kuisioner_ri");
	$table->setMaxWidth(false);
	$table->setTableClass("analisa_kuisioner");
	$table->setDefaultBootrapClass(false);
	$table->setTableClass("tbl_analisa_kuisioner_rj");
	$table->addColumn("RESUME HASIL KUISIONER RAWAT INAP",6,1,NULL,NULL,"center bold f20");
	$table->commit("body");
	$table->addColumn("DARI ",1,1);
	$table->addColumn(" : ",1,1);
	$table->addColumn(ArrayAdapter::format("date d M Y", $_POST['dari']),4,1);
	$table->commit("body");
	$table->addColumn("SAMPAI ",1,1);
	$table->addColumn(" : ",1,1);
	$table->addColumn(ArrayAdapter::format("date d M Y", $_POST['sampai']),4,1);
	$table->commit("body");	
	$table->addColumn("KRITERIA",1,1,NULL,NULL,"left bold ltop lbottom");
	$table->addColumn("BURUK",1,1,NULL,NULL,"left lbottom ltop bold lleft");
	$table->addColumn("KURANG",1,1,NULL,NULL,"left lbottom ltop bold lleft");
	$table->addColumn("BAIK",1,1,NULL,NULL,"left lbottom ltop bold lleft");
	$table->addColumn("SANGAT BAIK",1,1,NULL,NULL,"left ltop lbottom bold lleft");
	$table->addColumn("TOTAL",1,1,NULL,NULL,"left lbottom ltop bold lleft");
	$table->commit("body");
	foreach($set_chart as $scid=>$sc){
		$sbaik=$sc['count_sb'];
		$baik=$sc['count_ba'];
		$kurang=$sc['count_ku'];
		$buruk=$sc['count_bu'];		
		$p_sbaik=round($sc['count_sb']*100/$sc['jumlah'],2);
		$p_baik=round($sc['count_ba']*100/$sc['jumlah'],2);
		$p_kurang=round($sc['count_ku']*100/$sc['jumlah'],2);
		$p_buruk=100-$p_kurang-$p_baik-$p_sbaik;		
		if(isset($title[$scid])){
			$table->addColumn($title[$scid],1,1,NULL,NULL,"left ltop italic");
			$table->addColumn("",1,1,NULL,NULL,"left ltop bold lleft");
			$table->addColumn("",1,1,NULL,NULL,"left ltop bold lleft");
			$table->addColumn("",1,1,NULL,NULL,"left ltop bold lleft");
			$table->addColumn("",1,1,NULL,NULL,"left ltop bold lleft");
			$table->addColumn("",1,1,NULL,NULL,"left ltop bold lleft");
			$table->commit("body");
		}		
		$table->addColumn($list[$scid],1,1,NULL,NULL,"gleft");
		$table->addColumn($sbaik." (".$p_sbaik."%) ",1,1,NULL,NULL,"lleft");
		$table->addColumn($baik." (".$p_baik."%) ",1,1,NULL,NULL,"lleft");
		$table->addColumn($kurang." (".$p_kurang."%) ",1,1,NULL,NULL,"lleft");
		$table->addColumn($buruk." (".$p_buruk."%) ",1,1,NULL,NULL,"lleft");
		$table->addColumn($sc['jumlah']." (100%)",1,1,NULL,NULL,"lleft");
		$table->commit("body");
	}
	$result['print_area']=$table->getHtml();
	/*END OF KODE FOR PRINT AREA*/
	
	
	/*START KODE FOR CHART AREA*/
	$chart="";
	foreach($set_chart as $scid=>$sc){
		$sbaik=$sc['count_sb'];
		$baik=$sc['count_ba'];
		$kurang=$sc['count_ku'];
		$buruk=$sc['count_bu'];
		$p_sbaik=round($sc['count_sb']*100/$sc['jumlah'],2);
		$p_baik=round($sc['count_ba']*100/$sc['jumlah'],2);
		$p_kurang=round($sc['count_ku']*100/$sc['jumlah'],2);
		$p_buruk=100-$p_kurang-$p_baik-$p_sbaik;
		if(isset($title[$scid])){
			$chart.="</br></br><h4>".$title[$scid]."</h4>";
		}
		$chart.=$list[$scid]." ( ".$sc['jumlah']." Orang ) ";
		$div="
			<div class='progress'>
			  <div class='bar bar-info' style='width: ".$p_sbaik."%;'> SANGAT BAIK [ ".$sbaik." Orang / ".$p_sbaik."% ] </div>
			  <div class='bar bar-success' style='width: ".$p_baik."%;'> BAIK [ ".$baik." Orang / ".$p_baik."% ] </div>
			  <div class='bar bar-warning' style='width: ".$p_kurang."%;'> KURANG [ ".$kurang." Orang / ".$p_kurang."% ] </div>
			  <div class='bar bar-danger' style='width: ".$p_buruk."%;'> BURUK [ ".$buruk." Orang / ".$p_buruk."% ] </div>
			 </div>
		";
		$chart.=$div;
	}
	$result['chart_area']=$chart;
	/*END OF KODE FOR CHART AREA*/
	
	/*START OF KODE FOR SARAN AREA*/
	$table=new TablePrint("analisa_kuisioner_ri_saran");
	$table->setMaxWidth(false);
	$table->setDefaultBootrapClass(false);
	$table->setTableClass("analisa_kuisioner_rj_saran");
	$table->addColumn("SARAN MASUK KUISIONER RAWAT INAP",6,1,NULL,NULL,"center bold f20");
	$table->commit("body");
	$table->addColumn("DARI ",1,1);
	$table->addColumn(" : ",1,1);
	$table->addColumn(ArrayAdapter::format("date d M Y", $_POST['dari']),4,1);
	$table->commit("body");
	$table->addColumn("SAMPAI ",1,1);
	$table->addColumn(" : ",1,1);
	$table->addColumn(ArrayAdapter::format("date d M Y", $_POST['sampai']),4,1);
	$table->commit("body");
	$table->addColumn("NO.",1,1,NULL,NULL,"left bold ltop lbottom");
	$table->addColumn("NAMA",1,1,NULL,NULL,"left bold ltop lbottom");
	$table->addColumn("TANGGAL",1,1,NULL,NULL,"left bold ltop lbottom");
	$table->addColumn("SARAN",3,1,NULL,NULL,"left bold ltop lbottom");
	$table->commit("body");
	$no=0;
	foreach($set as $one){
		$table->addColumn((++$no).".",1,1);
		$table->addColumn($one['nama'],1,1);
		$table->addColumn(ArrayAdapter::format("date d M Y", $one['tanggal']),1,1);
		$table->addColumn($one['saran'],3,1);
		$table->commit("body");
	}
	$result['saran_area']=$table->getHtml();
	/*END OF KODE FOR SARAN AREA*/
	
	$pack=new ResponsePackage();
	$pack->setStatus(ResponsePackage::$STATUS_OK);
	$pack->setAlertVisible(false);
	$pack->setContent($result);
	echo json_encode($pack->getPackage());
	return;
}

$uitable->addModal("dari", "date", "Dari", "");
$uitable->addModal("sampai", "date", "Sampai", "");
$form=$uitable->getModal()->getForm()->setTitle("Analisa Kuisioner RI");


$load=new Button("", "", "Load");
$load->addClass("btn btn-primary");
$load->setIsButton(Button::$ICONIC_TEXT);
$load->setIcon("fa fa-circle-o");
$load->setAction("analisa_kuisioner_ri.load()");

$print_list=new Button("", "", "List");
$print_list->addClass("btn btn-primary");
$print_list->setIsButton(Button::$ICONIC_TEXT);
$print_list->setIcon("fa fa-bar-chart");
$print_list->setAction("analisa_kuisioner_ri.print_list()");

$print_chart=new Button("", "", "Chart");
$print_chart->addClass("btn btn-primary");
$print_chart->setIsButton(Button::$ICONIC_TEXT);
$print_chart->setIcon("fa fa-file");
$print_chart->setAction("analisa_kuisioner_ri.print_chart()");

$print_saran=new Button("", "", "Saran");
$print_saran->addClass("btn btn-primary");
$print_saran->setIsButton(Button::$ICONIC_TEXT);
$print_saran->setIcon("fa fa-book");
$print_saran->setAction("analisa_kuisioner_ri.print_saran()");

$grup=new ButtonGroup("");
$grup->addButton( $print_list);
$grup->addButton($print_chart);
$grup->addButton($print_saran);
$form->addElement("", $load);
$form->addElement("", $grup);

$tabs=new Tabulator("analisa_kuisioner_tabulator","");
$tabs->add("analisa_kuisioner_ri_tabs_chart", "Chart", "<div id='analisa_kuisioner_ri_chart_area'></div>",Tabulator::$TYPE_HTML,"fa fa-bar-chart");
$tabs->add("analisa_kuisioner_ri_tabs_print", "List", "<div id='analisa_kuisioner_ri_print_area'></div>",Tabulator::$TYPE_HTML,"fa fa-file");
$tabs->add("analisa_kuisioner_ri_tabs_saran", "Saran", "<div id='analisa_kuisioner_ri_saran_area'></div>",Tabulator::$TYPE_HTML,"fa fa-book");

echo $form->getHtml();
echo "<div class='clear'></div>";
echo $tabs->getHtml();
echo addJS ( "framework/bootstrap/js/bootstrap-datepicker.js" );
echo addJS ( "framework/smis/js/table_action.js" );
echo addCSS ( "framework/bootstrap/css/datepicker.css" );
echo addCSS ( "medical_record/resource/css/analisa_kuisioner_ri.css",false );
?>
<script type="text/javascript">
var analisa_kuisioner_ri;
$(document).ready(function(){
	$(".mydate").datepicker();
	analisa_kuisioner_ri=new TableAction("analisa_kuisioner_ri","medical_record","analisa_kuisioner_ri", new Array());
	analisa_kuisioner_ri.addRegulerData=function(a){
		a['dari']=$("#analisa_kuisioner_ri_dari").val();
		a['sampai']=$("#analisa_kuisioner_ri_sampai").val();
		return a;
	};

	analisa_kuisioner_ri.print_list=function(){
		var html=$("#analisa_kuisioner_ri_print_area").html();
		smis_print(html);
	};

	analisa_kuisioner_ri.print_chart=function(){
		var html=$("#analisa_kuisioner_ri_chart_area").html();
		smis_print(html);
	};

	analisa_kuisioner_ri.print_saran=function(){
		var html=$("#analisa_kuisioner_ri_saran_area").html();
		smis_print(html);
	};
	
	analisa_kuisioner_ri.load=function(){
		var a=this.getRegulerData();
		a['command']="load";
		showLoading();
		$.post("",a,function(res){
			var json=getContent(res);
			dismissLoading();
			if(json!=null){
				$("#analisa_kuisioner_ri_chart_area").html(json.chart_area);
				$("#analisa_kuisioner_ri_print_area").html(json.print_area);
				$("#analisa_kuisioner_ri_saran_area").html(json.saran_area);
			}else{
				$("#analisa_kuisioner_ri_chart_area").html("");
				$("#analisa_kuisioner_ri_print_area").html("");
				$("#analisa_kuisioner_ri_saran_area").html("");
			}
		});
	};
});
</script>
