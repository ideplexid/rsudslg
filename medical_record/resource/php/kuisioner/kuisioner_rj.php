<?php 
global $db;
require_once 'smis-libs-class/MasterTemplate.php';
$choose=new OptionBuilder();
$choose->add("","","1");
$choose->add("Buruk","1");
$choose->add("Kurang","2");
$choose->add("Baik","3");
$choose->add("Sangat Baik","4");
$kusioner_rj=new MasterTemplate($db, "smis_mr_quisioner_rj", "medical_record", "kuisioner_rj");
$kusioner_rj->getUItable()->addHeaderElement("No.");
$kusioner_rj->getUItable()->addHeaderElement("Nama");
$kusioner_rj->getUItable()->addHeaderElement("Tanggal");
$kusioner_rj->getUItable()->addHeaderElement("Saran");
$kusioner_rj->getAdapter()->add("Nama","nama");
$kusioner_rj->getAdapter()->add("Tanggal","tanggal","date d M Y");
$kusioner_rj->getAdapter()->add("Saran","saran");
$kusioner_rj->getAdapter()->setUseNumber(true, "No.","back.");
$kusioner_rj->getUItable()->addModal("id", "hidden", "", "");
$kusioner_rj->getUItable()->addModal("tanggal", "date", "Tanggal", date("Y-m-d"));
$kusioner_rj->getUItable()->addModal("nama", "text", "Nama", "","y",NULL,false,NULL,true,"a1_pend_cepat");

$kusioner_rj->getUItable()->addModal("", "label", "<strong>PENDAFTARAN</strong>", "");
$kusioner_rj->getUItable()->addModal("a1_pend_cepat", "select", " 1. Kecepatan Pendaftaran", $choose->getContent(),"y",NULL,false,NULL,false,"a2_pend_ramah");
$kusioner_rj->getUItable()->addModal("a2_pend_ramah", "select", " 2. Keramahan Petugas", $choose->getContent(),"y",NULL,false,NULL,false,"a3_pend_jelas");
$kusioner_rj->getUItable()->addModal("a3_pend_jelas", "select", " 3. Kejelasan Tulisan Pada Kartu Obat", $choose->getContent(),"y",NULL,false,NULL,false,"b1_keu_cepat");

$kusioner_rj->getUItable()->addModal("", "label", "<strong>KEUANGAN</strong>", "");
$kusioner_rj->getUItable()->addModal("b1_keu_cepat", "select", " 4. Kecepatan Pelayanan Petugas", $choose->getContent(),"y",NULL,false,NULL,false,"b2_keu_ramah");
$kusioner_rj->getUItable()->addModal("b2_keu_ramah", "select", " 5. Keramahan Petugas", $choose->getContent(),"y",NULL,false,NULL,false,"b3_keu_jelas");
$kusioner_rj->getUItable()->addModal("b3_keu_jelas", "select", " 6. Kejelasan Perhitungan Keuangan", $choose->getContent(),"y",NULL,false,NULL,false,"c1_lama_tunggu");

$kusioner_rj->getUItable()->addModal("", "label", "<strong>MENUNGGU</strong>", "");
$kusioner_rj->getUItable()->addModal("c1_lama_tunggu", "select", " 7. Lama Menunggu Untuk Pemeriksaan", $choose->getContent(),"y",NULL,false,NULL,false,"c2_nyaman_tunggu");
$kusioner_rj->getUItable()->addModal("c2_nyaman_tunggu", "select", " 8. Kenyamanan Ruang Tunggu", $choose->getContent(),"y",NULL,false,NULL,false,"c3_bersih_tunggu");
$kusioner_rj->getUItable()->addModal("c3_bersih_tunggu", "select", " 9. Kebersihan Ruang Tunggu", $choose->getContent(),"y",NULL,false,NULL,false,"c4_indah_taman");
$kusioner_rj->getUItable()->addModal("c4_indah_taman", "select", "10. Keindahan Taman", $choose->getContent(),"y",NULL,false,NULL,false,"c5_jelas_papan");
$kusioner_rj->getUItable()->addModal("c5_jelas_papan", "select", "11. Kejelasan Papan Petunjuk", $choose->getContent(),"y",NULL,false,NULL,false,"d1_poli_cepat_perawat");

$kusioner_rj->getUItable()->addModal("", "label", "<strong>POLIKLINIK</strong>", "");
$kusioner_rj->getUItable()->addModal("d1_poli_cepat_perawat", "select", "12. Kecepatan Perawat Membantu", $choose->getContent(),"y",NULL,false,NULL,false,"d2_poli_ramah_perawat");
$kusioner_rj->getUItable()->addModal("d2_poli_ramah_perawat", "select", "13. Keramahan Perawat", $choose->getContent(),"y",NULL,false,NULL,false,"d3_poli_jelas_perawat");
$kusioner_rj->getUItable()->addModal("d3_poli_jelas_perawat", "select", "14. Kejelasan Informasi dari Perawat", $choose->getContent(),"y",NULL,false,NULL,false,"d4_poli_cepat_dokter");
$kusioner_rj->getUItable()->addModal("d4_poli_cepat_dokter", "select", "15. Kecepatan Pemeriksaan Dokter", $choose->getContent(),"y",NULL,false,NULL,false,"d5_poli_ramah_dokter");
$kusioner_rj->getUItable()->addModal("d5_poli_ramah_dokter", "select", "16. Keramahan Dokter", $choose->getContent(),"y",NULL,false,NULL,false,"d6_poli_jelas_dokter");
$kusioner_rj->getUItable()->addModal("d6_poli_jelas_dokter", "select", "17. Kejelasan Informasi dari Dokter", $choose->getContent(),"y",NULL,false,NULL,false,"d7_poli_rapi_tempat");
$kusioner_rj->getUItable()->addModal("d7_poli_rapi_tempat", "select", "18. Kerapihan tempat Periksa", $choose->getContent(),"y",NULL,false,NULL,false,"d8_poli_bersih_tempat");
$kusioner_rj->getUItable()->addModal("d8_poli_bersih_tempat", "select", "19. Kebersihan Tempat Periksa", $choose->getContent(),"y",NULL,false,NULL,false,"e1_lab_cepat");

$kusioner_rj->getUItable()->addModal("", "label", "<strong>LABORATORIUM</strong>", "");
$kusioner_rj->getUItable()->addModal("e1_lab_cepat", "select", "20. Kecepatan Pemeriksaaan", $choose->getContent(),"y",NULL,false,NULL,false,"e2_lab_ramah");
$kusioner_rj->getUItable()->addModal("e2_lab_ramah", "select", "21. Keramahan Petugas", $choose->getContent(),"y",NULL,false,NULL,false,"e3_lab_jelas");
$kusioner_rj->getUItable()->addModal("e3_lab_jelas", "select", "22. Kejelasan Informasi", $choose->getContent(),"y",NULL,false,NULL,false,"f1_rad_cepat");

$kusioner_rj->getUItable()->addModal("", "label", "<strong>RADIOLOGI</strong>", "");
$kusioner_rj->getUItable()->addModal("f1_rad_cepat", "select", "23. Kecepatan Pemeriksaaan", $choose->getContent(),"y",NULL,false,NULL,false,"f2_rad_ramah");
$kusioner_rj->getUItable()->addModal("f2_rad_ramah", "select", "24. Keramahan Petugas", $choose->getContent(),"y",NULL,false,NULL,false,"f3_rad_jelas");
$kusioner_rj->getUItable()->addModal("f3_rad_jelas", "select", "25. Kejelasan Informasi", $choose->getContent(),"y",NULL,false,NULL,false,"g1_igd_cepat");

$kusioner_rj->getUItable()->addModal("", "label", "<strong>INSTALASI GAWAT DARURAT</strong>", "");
$kusioner_rj->getUItable()->addModal("g1_igd_cepat", "select", "26. Kecepatan Pelayanan", $choose->getContent(),"y",NULL,false,NULL,false,"g2_igd_ramah");
$kusioner_rj->getUItable()->addModal("g2_igd_ramah", "select", "27. Keramahan Petugas", $choose->getContent(),"y",NULL,false,NULL,false,"g3_igd_obat");
$kusioner_rj->getUItable()->addModal("g3_igd_obat", "select", "28. Pelayanan Obat", $choose->getContent(),"y",NULL,false,NULL,false,"g4_igd_jelas");
$kusioner_rj->getUItable()->addModal("g4_igd_jelas", "select", "29. Kejelasan Informasi", $choose->getContent(),"y",NULL,false,NULL,false,"h1_fisio_cepat");

$kusioner_rj->getUItable()->addModal("", "label", "<strong>FISIOTHERAPY</strong>", "");
$kusioner_rj->getUItable()->addModal("h1_fisio_cepat", "select", "30. Kecepatan Pelayanan", $choose->getContent(),"y",NULL,false,NULL,false,"h2_fisio_ramah");
$kusioner_rj->getUItable()->addModal("h2_fisio_ramah", "select", "31. Keramahan Petugas", $choose->getContent(),"y",NULL,false,NULL,false,"h3_fisio_jelas");
$kusioner_rj->getUItable()->addModal("h3_fisio_jelas", "select", "32. Kejelasan Informasi", $choose->getContent(),"y",NULL,false,NULL,false,"h4_fisio_lengkap");
$kusioner_rj->getUItable()->addModal("h4_fisio_lengkap", "select", "33. Kelengkapan Alat Terapi", $choose->getContent(),"y",NULL,false,NULL,false,"saran");
$kusioner_rj->getUItable()->addModal("saran", "textarea", "Saran", "","y",NULL,false,NULL,false,"save");
$kusioner_rj->setDateEnable(true);
$kusioner_rj->getModal()->setModalSize(Modal::$HALF_MODEL)->setComponentSize(Modal::$BIG)->setTitle("Kuisioner Rawat Jalan");
$kusioner_rj->setNextEnter(true);
$kusioner_rj->setMultipleInput(true);
$kusioner_rj->setAutofocus(true);
$kusioner_rj->setAutofocusOnMultiple("nama");
$kusioner_rj->addNoClear("tanggal");
$kusioner_rj->initialize();
?>