<?php 
global $db;
require_once 'smis-libs-class/MasterTemplate.php';
$choose=new OptionBuilder();
$choose->add("","","1");
$choose->add("Buruk","1");
$choose->add("Kurang","2");
$choose->add("Baik","3");
$choose->add("Sangat Baik","4");
$kusioner_ri=new MasterTemplate($db, "smis_mr_quisioner_ri", "medical_record", "kuisioner_ri");
$kusioner_ri->getUItable()->addHeaderElement("No.");
$kusioner_ri->getUItable()->addHeaderElement("Nama");
$kusioner_ri->getUItable()->addHeaderElement("Tanggal");
$kusioner_ri->getUItable()->addHeaderElement("Saran");
$kusioner_ri->getAdapter()->add("Nama","nama");
$kusioner_ri->getAdapter()->add("Tanggal","tanggal","date d M Y");
$kusioner_ri->getAdapter()->add("Saran","saran");
$kusioner_ri->getAdapter()->setUseNumber(true, "No.","back.");
$kusioner_ri->getUItable()->addModal("id", "hidden", "", "");
$kusioner_ri->getUItable()->addModal("tanggal", "date", "Tanggal", date("Y-m-d"));
$kusioner_ri->getUItable()->addModal("nama", "text", "Nama", "","y",NULL,false,NULL,true,"a1_pend_cepat");

$kusioner_ri->getUItable()->addModal("", "label", "<strong>PENDAFTARAN</strong>", "");
$kusioner_ri->getUItable()->addModal("a1_pend_cepat", "select", " 1. Kecepatan Pendaftaran", $choose->getContent(),"y",NULL,false,NULL,false,"a2_pend_ramah");
$kusioner_ri->getUItable()->addModal("a2_pend_ramah", "select", " 2. Keramahan Petugas Pendaftaran", $choose->getContent(),"y",NULL,false,NULL,false,"b1_dok_cepat");

$kusioner_ri->getUItable()->addModal("", "label", "<strong>DOKTER</strong>", "");
$kusioner_ri->getUItable()->addModal("b1_dok_cepat", "select", " 3. Kecepatan Dokter dalam Menangani Keluhan Penyakit Anda", $choose->getContent(),"y",NULL,false,NULL,false,"b2_dok_ramah");
$kusioner_ri->getUItable()->addModal("b2_dok_ramah", "select", " 4. Keramahan Dokter", $choose->getContent(),"y",NULL,false,NULL,false,"b3_dok_tanggap");
$kusioner_ri->getUItable()->addModal("b3_dok_tanggap", "select", " 5. Tanggapan Dokter Terhadap Keluhan Anda", $choose->getContent(),"y",NULL,false,NULL,false,"b4_dok_jelas");
$kusioner_ri->getUItable()->addModal("b4_dok_jelas", "select", " 6. Kejelasan Informasi", $choose->getContent(),"y",NULL,false,NULL,false,"c1_perawat_cepat");

$kusioner_ri->getUItable()->addModal("", "label", "<strong>PERAWAT</strong>", "");
$kusioner_ri->getUItable()->addModal("c1_perawat_cepat", "select", " 7. Kecepatan Perawat Untuk Memberikan Bantuan Ketika Anda Perlukan", $choose->getContent(),"y",NULL,false,NULL,false,"c2_perawat_ramah");
$kusioner_ri->getUItable()->addModal("c2_perawat_ramah", "select", " 8. Keramahan Perawat", $choose->getContent(),"y",NULL,false,NULL,false,"c3_perawat_jelas");
$kusioner_ri->getUItable()->addModal("c3_perawat_jelas", "select", " 9. Kejelasan Informasi Tentang Tindakan - Tindakan Perawat yang dilakukan", $choose->getContent(),"y",NULL,false,NULL,false,"c4_perawat_teratur");
$kusioner_ri->getUItable()->addModal("c4_perawat_teratur", "select", "10. Keteraturan Pengukuran Tekanan Darah dan Suhu Tubuh", $choose->getContent(),"y",NULL,false,NULL,false,"d1_makan_menu");

$kusioner_ri->getUItable()->addModal("", "label", "<strong>MAKANAN</strong>", "");
$kusioner_ri->getUItable()->addModal("d1_makan_menu", "select", "11. Menu yang dihidangkan", $choose->getContent(),"y",NULL,false,NULL,false,"d2_makan_tata");
$kusioner_ri->getUItable()->addModal("d2_makan_tata", "select", "12. Penataan Makanan atau Penampilan Makanan", $choose->getContent(),"y",NULL,false,NULL,false,"d3_makan_tepat");
$kusioner_ri->getUItable()->addModal("d3_makan_tepat", "select", "13. Ketepatan Waktu Penyajian Makanan", $choose->getContent(),"y",NULL,false,NULL,false,"e1_nyaman_bersih");

$kusioner_ri->getUItable()->addModal("", "label", "<strong>KENYAMANAN & KEBERSIHAN</strong>", "");
$kusioner_ri->getUItable()->addModal("e1_nyaman_bersih", "select", "14. Kebersihan Ruangan", $choose->getContent(),"y",NULL,false,NULL,false,"e2_nyaman_bising");
$kusioner_ri->getUItable()->addModal("e2_nyaman_bising", "select", "15. Kebisingan", $choose->getContent(),"y",NULL,false,NULL,false,"e3_nyaman_nyamuk");
$kusioner_ri->getUItable()->addModal("e3_nyaman_nyamuk", "select", "16. Gangguan dari Nyamuk", $choose->getContent(),"y",NULL,false,NULL,false,"e4_nyaman_rapi");
$kusioner_ri->getUItable()->addModal("e4_nyaman_rapi", "select", "17. Perapihan Tempat Tidur", $choose->getContent(),"y",NULL,false,NULL,false,"e5_nyaman_terang");
$kusioner_ri->getUItable()->addModal("e5_nyaman_terang", "select", "18. Penerangan di Kamar Anda", $choose->getContent(),"y",NULL,false,NULL,false,"e6_nyaman_bersih_wc");
$kusioner_ri->getUItable()->addModal("e6_nyaman_bersih_wc", "select", "19. Kebersihan Kamar Mandi / WC", $choose->getContent(),"y",NULL,false,NULL,false,"e7_nyaman_air_wc");
$kusioner_ri->getUItable()->addModal("e7_nyaman_air_wc", "select", "20. Persediaan Air di Kamar Mandi / WC", $choose->getContent(),"y",NULL,false,NULL,false,"e8_nyaman_sampah");
$kusioner_ri->getUItable()->addModal("e8_nyaman_sampah", "select", "21. Pembuangan Sampah dari Keranjang Sampah di Kamar Anda", $choose->getContent(),"y",NULL,false,NULL,false,"f1_sarana_cukup_alat");

$kusioner_ri->getUItable()->addModal("", "label", "<strong>SARANA MEDIS</strong>", "");
$kusioner_ri->getUItable()->addModal("f1_sarana_cukup_alat", "select", "22. Kecukupan Peralatan di RS ini Untuk Pemeriksaan/Mengobati Penyakit Anda", $choose->getContent(),"y",NULL,false,NULL,false,"f2_sarana_obat_lengkap");
$kusioner_ri->getUItable()->addModal("f2_sarana_obat_lengkap", "select", "23. Kelengkapan Obat Oleh Rumah Sakit", $choose->getContent(),"y",NULL,false,NULL,false,"f3_sarana_cepat_keuangan");
$kusioner_ri->getUItable()->addModal("f3_sarana_cepat_keuangan", "select", "24. Kecepatan Petugas Dalam Melayani Administrasi Keuangan (Tidak Lbeih dari 15 Menit) ", $choose->getContent(),"y",NULL,false,NULL,false,"f4_sarana_jelas_biaya");
$kusioner_ri->getUItable()->addModal("f4_sarana_jelas_biaya", "select", "25. Kejelasan Perincian Biaya", $choose->getContent(),"y",NULL,false,NULL,false,"f5_sarana_sesuai_harga");
$kusioner_ri->getUItable()->addModal("f5_sarana_sesuai_harga", "select", "26. Kesesuaian Harga Obat-Obatan", $choose->getContent(),"y",NULL,false,NULL,false,"saran");
$kusioner_ri->getUItable()->addModal("saran", "textarea", "Saran", "","y",NULL,false,NULL,false,"save");
$kusioner_ri->setDateEnable(true);
$kusioner_ri->getModal()->setModalSize(Modal::$HALF_MODEL)->setComponentSize(Modal::$BIG)->setTitle("Kuisioner Rawat Inap");
$kusioner_ri->setNextEnter(true);
$kusioner_ri->setMultipleInput(true);
$kusioner_ri->setAutofocus(true);
$kusioner_ri->setAutofocusOnMultiple("nama");
$kusioner_ri->addNoClear("tanggal");
$kusioner_ri->initialize();
?>