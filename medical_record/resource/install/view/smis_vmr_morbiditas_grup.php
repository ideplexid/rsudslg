<?php

global $wpdb;
$query = '
		CREATE OR REPLACE VIEW smis_vmr_morbiditas_grup AS 
            SELECT
                smis_mr_diagnosa.id as id, 
                smis_mr_diagnosa.kasus as kasus, 
                smis_mr_diagnosa.nama_icd as nama_icd, 
                smis_mr_diagnosa.sebab_sakit as sebab_sakit, 
                smis_mr_diagnosa.kode_icd as kode_icd, 
                smis_mr_diagnosa.diagnosa as diagnosa, 
                smis_mr_diagnosa.urji as urji,
                smis_mr_diagnosa.origin as origin,
                IF(smis_mr_diagnosa.gol_umur="0 - 28 HR" AND smis_mr_diagnosa.kasus="Baru" AND smis_mr_diagnosa.jk = 0,1,0) as u28hr_lk,
                IF(smis_mr_diagnosa.gol_umur="28 HR - 1 TH" AND smis_mr_diagnosa.kasus="Baru" AND smis_mr_diagnosa.jk = 0,1,0) as u1th_lk,
                IF(smis_mr_diagnosa.gol_umur="1-4 TH" AND smis_mr_diagnosa.kasus="Baru" AND smis_mr_diagnosa.jk = 0,1,0 ) as u4th_lk,
                IF(smis_mr_diagnosa.gol_umur="5-14 TH" AND smis_mr_diagnosa.kasus="Baru" AND smis_mr_diagnosa.jk = 0,1,0) as u14th_lk,
                IF(smis_mr_diagnosa.gol_umur="15-24 TH" AND smis_mr_diagnosa.kasus="Baru" AND smis_mr_diagnosa.jk = 0,1,0) as u24th_lk,
                IF(smis_mr_diagnosa.gol_umur="25-44 TH" AND smis_mr_diagnosa.kasus="Baru" AND smis_mr_diagnosa.jk = 0,1,0) as u44th_lk,
                IF(smis_mr_diagnosa.gol_umur="45-65 TH" AND smis_mr_diagnosa.kasus="Baru" AND smis_mr_diagnosa.jk = 0,1,0) as u65th_lk,
                IF(smis_mr_diagnosa.gol_umur=">65 TH" AND smis_mr_diagnosa.kasus="Baru" AND smis_mr_diagnosa.jk = 0,1,0) as ul65th_lk,
                IF(smis_mr_diagnosa.gol_umur="0 - 28 HR" AND smis_mr_diagnosa.kasus="Baru" AND smis_mr_diagnosa.jk = 1,1,0) as u28hr_pr,
                IF(smis_mr_diagnosa.gol_umur="28 HR - 1 TH" AND smis_mr_diagnosa.kasus="Baru" AND smis_mr_diagnosa.jk = 1,1,0) as u1th_pr,
                IF(smis_mr_diagnosa.gol_umur="1-4 TH" AND smis_mr_diagnosa.kasus="Baru" AND smis_mr_diagnosa.jk = 1,1,0 ) as u4th_pr,
                IF(smis_mr_diagnosa.gol_umur="5-14 TH" AND smis_mr_diagnosa.kasus="Baru" AND smis_mr_diagnosa.jk = 1,1,0) as u14th_pr,
                IF(smis_mr_diagnosa.gol_umur="15-24 TH" AND smis_mr_diagnosa.kasus="Baru" AND smis_mr_diagnosa.jk = 1,1,0) as u24th_pr,
                IF(smis_mr_diagnosa.gol_umur="25-44 TH" AND smis_mr_diagnosa.kasus="Baru" AND smis_mr_diagnosa.jk = 1,1,0) as u44th_pr,
                IF(smis_mr_diagnosa.gol_umur="45-65 TH" AND smis_mr_diagnosa.kasus="Baru" AND smis_mr_diagnosa.jk = 1,1,0) as u65th_pr,
                IF(smis_mr_diagnosa.gol_umur=">65 TH" AND smis_mr_diagnosa.kasus="Baru" AND smis_mr_diagnosa.jk = 1,1,0) as ul65th_pr,
                IF(smis_mr_diagnosa.kasus="Baru" AND smis_mr_diagnosa.jk=0,1,0) as kasus_baru_lk,
                IF(smis_mr_diagnosa.kasus="Baru" AND smis_mr_diagnosa.jk=1,1,0) as kasus_baru_pr,
                IF(smis_mr_diagnosa.kasus="Baru",1,0) as kasus_baru_jmlh,
                IF(smis_mr_diagnosa.kunjungan="Baru" OR smis_mr_diagnosa.kunjungan="Lama",1,0) as jmlh_kunjungan,
                smis_mr_diagnosa.ruangan as ruangan,
                smis_mr_diagnosa.tanggal as tanggal,
                smis_mr_diagnosa.prop as prop,
                smis_mr_icd.dtd as dtd,
                smis_mr_icd.grup as grup,
                smis_adm_prototype.status as status_ruangan
            FROM 
                smis_mr_diagnosa
                LEFT JOIN 
                    smis_mr_icd 
                ON 
                    smis_mr_diagnosa.kode_icd = smis_mr_icd.icd
            LEFT JOIN 
                smis_adm_prototype
            ON 
                smis_adm_prototype.slug = smis_mr_diagnosa.ruangan
	 ';
	$wpdb->query ( $query );

?>