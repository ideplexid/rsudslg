<?php 

global $wpdb;
$query = '
		CREATE OR REPLACE VIEW smis_vmr_rl54 AS SELECT
			id, 
			kasus, 
			nama_icd, 
			sebab_sakit, 
			kode_icd, 
			diagnosa, 
            urji,
            origin,
			IF(gol_umur="0 - 28 HR" AND kasus="Baru" AND jk = 0,1,0) as u28hr_lk,
			IF(gol_umur="28 HR - 1 TH" AND kasus="Baru" AND jk = 0,1,0)) as u1th_lk,
			IF(gol_umur="1-4 TH" AND kasus="Baru" AND jk = 0,1,0 ) as u4th_lk,
			IF(gol_umur="5-14 TH" AND kasus="Baru" AND jk = 0,1,0) as u14th_lk,
			IF(gol_umur="15-24 TH" AND kasus="Baru" AND jk = 0,1,0) as u24th_lk,
			IF(gol_umur="25-44 TH" AND kasus="Baru" AND jk = 0,1,0) as u44th_lk,
			IF(gol_umur="45-65 TH" AND kasus="Baru" AND jk = 0,1,0) as u65th_lk,
			IF(gol_umur=">65 TH" AND kasus="Baru" AND jk = 0,1,0) as ul65th_lk,
            IF(gol_umur="0 - 28 HR" AND kasus="Baru" AND jk = 1,1,0) as u28hr_pr,
			IF(gol_umur="28 HR - 1 TH" AND kasus="Baru" AND jk = 1,1,0) as u1th_pr,
			IF(gol_umur="1-4 TH" AND kasus="Baru" AND jk = 1,1,0 ) as u4th_pr,
			IF(gol_umur="5-14 TH" AND kasus="Baru" AND jk = 1,1,0) as u14th_pr,
			IF(gol_umur="15-24 TH" AND kasus="Baru" AND jk = 1,1,0) as u24th_pr,
			IF(gol_umur="25-44 TH" AND kasus="Baru" AND jk = 1,1,0) as u44th_pr,
			IF(gol_umur="45-65 TH" AND kasus="Baru" AND jk = 1,1,0) as u65th_pr,
			IF(gol_umur=">65 TH" AND kasus="Baru" AND jk = 1,1,0) as ul65th_pr,
            IF(kasus="Baru" AND jk=0,1,0) as kasus_baru_lk,
			IF(kasus="Baru" AND jk=1,1,0) as kasus_baru_pr,
			IF(kasus="Baru",1,0) as kasus_baru_jmlh,
            IF(kasus="Lama" AND jk=0,1,0) as kasus_lama_lk,
			IF(kasus="Lama" AND jk=1,1,0) as kasus_lama_pr,
			IF(kasus="Lama",1,0) as kasus_lama_jmlh,
			IF(kasus="Lama" OR kasus="Baru",1,0) as kasus_jmlh,
            IF(kunjungan="Baru" AND jk=0,1,0) as pasien_baru_lk,
			IF(kunjungan="Baru" AND jk=1,1,0) as pasien_baru_pr,
			IF(kunjungan="Baru",1,0) as pasien_baru_jmlh,
            IF(kunjungan="Lama" AND jk=0,1,0) as pasien_lama_lk,
			IF(kunjungan="Lama" AND jk=1,1,0) as pasien_lama_pr,
			IF(kunjungan="Lama",1,0) as pasien_lama_jmlh,
			IF(kunjungan="Baru" OR kunjungan="Lama",1,0) as jmlh_total,
			ruangan,
			tanggal,
			prop
			FROM smis_mr_diagnosa
			WHERE prop!="del"
	 ';
	$wpdb->query ( $query );
    
?>