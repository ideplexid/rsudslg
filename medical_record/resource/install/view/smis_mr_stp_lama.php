<?php 
global $wpdb;

$query = '
	CREATE OR REPLACE VIEW smis_mr_stp_lama AS SELECT
        smis_mr_diagnosa.id as id, 
			smis_mr_diagnosa.kasus as kasus, 
			smis_mr_diagnosa.nama_icd as nama_icd, 
			smis_mr_diagnosa.kode_icd as kode_icd, 
			smis_mr_diagnosa.diagnosa as diagnosa, 
            smis_mr_diagnosa.urji as urji,
            smis_mr_diagnosa.origin as origin,
			sebab_sakit as jenis_penyakit,
			if(gol_umur="0 - 28 HR" AND smis_mr_diagnosa.kasus="Lama",1,0) as u28hr,
			if(gol_umur="28 HR - 1 TH" AND smis_mr_diagnosa.kasus="Lama",1,0) as u1th,
			if(gol_umur="1-5 TH" AND smis_mr_diagnosa.kasus="Lama",1,0 ) as u4th,
			if(gol_umur="4-14 TH" AND smis_mr_diagnosa.kasus="Lama",1,0) as u14th,
			if(gol_umur="15-24 TH" AND smis_mr_diagnosa.kasus="Lama",1,0) as u24th,
			if(gol_umur="25-44 TH" AND smis_mr_diagnosa.kasus="Lama",1,0) as u44th,
			if(gol_umur="45-65 TH" AND smis_mr_diagnosa.kasus="Lama",1,0) as u65th,
			if(gol_umur=">65 TH" AND smis_mr_diagnosa.kasus="Lama",1,0) as ul65th,
			if(smis_mr_diagnosa.kasus="Lama",1,0) as jumlah,
			if(smis_mr_diagnosa.kasus="Lama" AND jk=0,1,0) as L,
			if(smis_mr_diagnosa.kasus="Lama" AND jk=1,1,0) as P,
			if(smis_mr_diagnosa.kasus="Lama",1,0) as total,
			smis_mr_diagnosa.ruangan as ruangan,
			smis_mr_diagnosa.tanggal as tanggal,
			smis_mr_diagnosa.prop as prop
			FROM smis_mr_diagnosa
			WHERE smis_mr_diagnosa.prop!="del"
	';
	$wpdb->query ( $query );
?>