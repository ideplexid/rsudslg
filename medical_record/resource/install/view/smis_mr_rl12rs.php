<?php 
global $wpdb;
$query="create or replace view smis_mr_rl12rs as SELECT
	'' as id,
	concat('** ','Rumah Sakit') as ruangan,
	sum(jhp)as jhp,
	sum(jtt)as jtt,
	jh as jh,
	sum(jp)as jp,
	sum(jph)as jph,
	sum(jpm48)as jpm48,
	sum(jpm)as jpm,
	sum(jhp)*100/(sum(jtt)*jh ) as bor,
	sum(jhp)/sum(jp) as los,
	(sum(jtt)*jh -sum(jhp))/sum(jp) as toi,
	sum(jp)/sum(jtt) as bto,
	sum(jpm48)*100/sum(jp) as ndr,
	sum(jpm)*100/sum(jp) as gdr,
	sum(jp)/jh as rata_hari,
	sum(jp)*30/jh as rata_bulan,
	'' as prop
	FROM `smis_mr_rl12` ";
	$wpdb->query ( $query );
?>