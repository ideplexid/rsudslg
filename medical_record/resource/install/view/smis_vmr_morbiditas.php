<?php

global $wpdb;
$query = '
		CREATE OR REPLACE VIEW smis_vmr_morbiditas AS SELECT
			smis_mr_diagnosa.*, 
            smis_mr_icd.dtd as dtd,
            smis_mr_icd.grup as grup
            FROM `smis_mr_diagnosa` 
            LEFT JOIN `smis_mr_icd` 
            ON smis_mr_diagnosa.kode_icd = smis_mr_icd.icd
	 ';
	$wpdb->query ( $query );

?>