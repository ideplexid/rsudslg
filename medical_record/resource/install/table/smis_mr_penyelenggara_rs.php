<?php
	require_once "smis-libs-class/DBCreator.php";

	$dbcreator = new DBCreator($wpdb, "smis_mr_penyelenggara_rs", DBCreator::$ENGINE_MYISAM);
	$dbcreator->addColumn("penyelenggara", "varchar(256)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->initialize();
?>