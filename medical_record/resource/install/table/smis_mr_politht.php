<?php

require_once "smis-libs-class/DBCreator.php";
$dbcreator=new DBCreator($wpdb,"smis_mr_politht",DBCreator::$ENGINE_MYISAM);

$dbcreator->addColumn("waktu", "datetime", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("waktu_masuk", "datetime", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("nama_pasien", "varchar(32)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("nrm_pasien", "int(11)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("noreg_pasien", "int(11)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("uri_pasien", "int(11)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("tgl_lahir_pasien", "date", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("umur_pasien", "varchar(32)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("alamat_pasien", "varchar(64)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("ruangan", "varchar(32)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("carabayar", "varchar(32)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");

$dbcreator->addColumn("keluhan_utama", "text", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("keluhan_tambahan", "text", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("riwayat_penyakit", "text", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");

$dbcreator->addColumn("telinga_sekret_kanan", "varchar(32)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("telinga_tuli_kanan", "varchar(32)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("telinga_tumor_kanan", "varchar(32)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("telinga_tinitus_kanan", "varchar(32)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("telinga_sakit_kanan", "varchar(32)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("telinga_korpus_alienum_kanan", "varchar(32)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("telinga_vertigo_kanan", "varchar(32)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("telinga_buntu_kanan", "varchar(32)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("telinga_gatal_kanan", "varchar(32)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");

$dbcreator->addColumn("telinga_sekret_kiri", "varchar(32)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("telinga_tuli_kiri", "varchar(32)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("telinga_tumor_kiri", "varchar(32)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("telinga_tinitus_kiri", "varchar(32)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("telinga_sakit_kiri", "varchar(32)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("telinga_korpus_alienum_kiri", "varchar(32)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("telinga_vertigo_kiri", "varchar(32)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("telinga_buntu_kiri", "varchar(32)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("telinga_gatal_kiri", "varchar(32)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");

$dbcreator->addColumn("telinga_daun_kanan", "varchar(32)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("telinga_liang_kanan", "varchar(32)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("telinga_discharge_kanan", "varchar(32)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("telinga_membran_timpani_kanan", "varchar(32)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("telinga_tumor_kanan", "varchar(32)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("telinga_mastoid_kanan", "varchar(32)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("telinga_tes_pendengaran_kanan", "varchar(32)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("telinga_berbisik_kanan", "varchar(32)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("telinga_weber_kanan", "varchar(32)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("telinga_rinne_kanan", "varchar(32)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("telinga_schwabach_kanan", "varchar(32)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");

$dbcreator->addColumn("telinga_daun_kiri", "varchar(32)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("telinga_liang_kiri", "varchar(32)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("telinga_discharge_kiri", "varchar(32)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("telinga_membran_timpani_kiri", "varchar(32)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("telinga_tumor_kiri", "varchar(32)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("telinga_mastoid_kiri", "varchar(32)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("telinga_tes_pendengaran_kiri", "varchar(32)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("telinga_berbisik_kiri", "varchar(32)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("telinga_weber_kiri", "varchar(32)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("telinga_rinne_kiri", "varchar(32)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("telinga_schwabach_kiri", "varchar(32)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");

$dbcreator->addColumn("hidung_sekret_kanan", "varchar(32)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("hidung_tersumbat_kanan", "varchar(32)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("hidung_tumor_kanan", "varchar(32)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("hidung_pilek_kanan", "varchar(32)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("hidung_sakit_kanan", "varchar(32)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("hidung_korpus_alienum_kanan", "varchar(32)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("hidung_bersin_kanan", "varchar(32)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");

$dbcreator->addColumn("hidung_sekret_kiri", "varchar(32)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("hidung_tersumbat_kiri", "varchar(32)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("hidung_tumor_kiri", "varchar(32)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("hidung_pilek_kiri", "varchar(32)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("hidung_sakit_kiri", "varchar(32)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("hidung_korpus_alienum_kiri", "varchar(32)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("hidung_bersin_kiri", "varchar(32)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");

$dbcreator->addColumn("hidung_luar_kanan", "varchar(32)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("hidung_kavum_nasi_kanan", "varchar(32)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("hidung_septum_kanan", "varchar(32)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("hidung_discharge_kanan", "varchar(32)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("hidung_mukosa_kanan", "varchar(32)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("hidung_tumor_kanan", "varchar(32)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("hidung_konka_kanan", "varchar(32)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("hidung_sinus_kanan", "varchar(32)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("hidung_koana_kanan", "varchar(32)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("hidung_naso_endoskopi_kanan", "varchar(32)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");

$dbcreator->addColumn("hidung_luar_kiri", "varchar(32)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("hidung_kavum_nasi_kiri", "varchar(32)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("hidung_septum_kiri", "varchar(32)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("hidung_discharge_kiri", "varchar(32)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("hidung_mukosa_kiri", "varchar(32)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("hidung_tumor_kiri", "varchar(32)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("hidung_konka_kiri", "varchar(32)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("hidung_sinus_kiri", "varchar(32)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("hidung_koana_kiri", "varchar(32)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("hidung_naso_endoskopi_kiri", "varchar(32)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");

$dbcreator->addColumn("tenggorokan_riak", "varchar(32)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("tenggorokan_gangguan", "varchar(32)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("tenggorokan_suara", "varchar(32)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("tenggorokan_tumor", "varchar(32)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("tenggorokan_batuk", "varchar(32)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("tenggorokan_korpus_alienum", "varchar(32)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("tenggorokan_sesak_nafas", "varchar(32)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("tenggorokan_mukosa", "varchar(32)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("tenggorokan_dinding_belakang", "varchar(32)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");

$dbcreator->addColumn("laring_epiglotis", "varchar(32)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("laring_aritenoid", "varchar(32)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("laring_plika_ventrikuloris", "varchar(32)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("laring_endoskopi", "varchar(32)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("laring_plika_vokalis", "varchar(32)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("laring_rima_glotis", "varchar(32)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");

$dbcreator->addColumn("diagnosa_kerja", "text", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("kelenjar_limpe_leher", "text", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("terapi", "text", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");

$dbcreator->addColumn("neurovaskuler", "varchar(32)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("neuroimaging", "varchar(32)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("elektrodiagnostik", "varchar(32)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("laboratorium", "varchar(32)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("foto", "varchar(32)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("audiogram", "varchar(32)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("oae", "varchar(32)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("lain", "varchar(32)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");

$dbcreator->addColumn("tanggal_pulang", "datetime", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("tanggal_kontrol", "datetime", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("dirawat", "varchar(64)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("id_dokter", "int(11)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("nama_dokter", "varchar(64)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("ttd_dokter", "text", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");

$dbcreator->initialize();

?>