<?php

require_once "smis-libs-class/DBCreator.php";
$dbcreator=new DBCreator($wpdb,"smis_mr_assesmen_psikologi",DBCreator::$ENGINE_INNODB);

$dbcreator->addColumn("waktu_masuk", "datetime", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("jk", "tinyint(1)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("alamat_pasien", "varchar(32)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("waktu", "datetime", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("nama_pasien", "varchar(32)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("nrm_pasien", "int(11)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("noreg_pasien", "int(11)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("umur_pasien", "varchar(32)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("uri_pasien", "int(11)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("tgl_lahir_pasien", "date", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("ruangan", "varchar(32)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("carabayar", "varchar(32)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");

$dbcreator->addColumn("anak_ke", "int(11)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("dari_saudara", "int(11)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("status_nikah", "varchar(32)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("pend_terakhir", "varchar(32)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");

$dbcreator->addColumn("obeservasi", "text", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("auto_anamenese", "text", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("hetero_anamenese", "text", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("wawancara", "text", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("wawancara_ket", "text", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("keluhan_utama", "text", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("mulai_terjadi", "text", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("riwayat_penyakit", "text", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("keluhan_psikosomatis", "text", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");

$dbcreator->addColumn("penyebab", "varchar(32)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("faktor_keluarga", "varchar(32)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("faktor_pekerjaan", "varchar(32)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("faktor_premorboid", "varchar(32)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");

$dbcreator->addColumn("riwayat_napza", "varchar(32)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("napza_lama_pemakaian", "varchar(32)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("napza_jenis_zat", "varchar(32)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("napza_cara_pakai", "varchar(32)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("napza_latarbelakang_pakai", "varchar(32)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");

$dbcreator->addColumn("kesan_umum", "varchar(32)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("kesadaran", "varchar(32)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("mood", "varchar(32)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("proses_dan_isi_pikiran", "varchar(32)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("kecerdasan", "varchar(32)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("dorongan", "varchar(32)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("judgment", "varchar(32)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("attention", "varchar(32)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("psikomotorik", "varchar(32)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");

$dbcreator->addColumn("orientasi_diri", "varchar(32)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("orientasi_wkt", "varchar(32)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("orientasi_tempat", "varchar(32)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("orientasi_ruang", "varchar(32)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");

$dbcreator->addColumn("bb", "int(11)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("tb", "int(11)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("lila", "int(11)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("lk", "int(11)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("status_gizi", "varchar(32)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");

$dbcreator->addColumn("tes_iq", "varchar(32)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("tes_bakat", "varchar(32)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("tes_kepribadian", "varchar(32)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("tes_okupasi", "varchar(32)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("tes_perkembangan", "varchar(32)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");

$dbcreator->addColumn("diagnosa_kerja", "text", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("terapi", "text", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("tindakan", "text", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");

$dbcreator->addColumn("perkembangan_fisik", "varchar(32)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("perkembangan_kognitif", "varchar(32)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("perkembangan_emosi", "varchar(32)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("perkembangan_sosial", "varchar(32)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("perkembangan_motorik", "varchar(32)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");

$dbcreator->addColumn("raport", "varchar(32)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("ekspresi", "varchar(32)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("reaksi", "varchar(32)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("komunikasi", "varchar(32)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("lain_lain", "varchar(32)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");

$dbcreator->addColumn("waktu_pulang", "datetime", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("waktu_kontrol_klinik", "datetime", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("dirawat_diruang", "varchar(32)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("id_dokter", "int(11)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("nama_dokter", "varchar(32)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("ttd_dokter", "text", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");

$dbcreator->setDuplicate(false);
$dbcreator->initialize();

?>