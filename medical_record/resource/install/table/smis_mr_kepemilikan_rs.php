<?php
	require_once "smis-libs-class/DBCreator.php";

	$dbcreator = new DBCreator($wpdb, "smis_mr_kepemilikan_rs", DBCreator::$ENGINE_MYISAM);
	$dbcreator->addColumn("kepemilikan", "varchar(256)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->initialize();
?>