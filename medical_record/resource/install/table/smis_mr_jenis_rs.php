<?php
	require_once "smis-libs-class/DBCreator.php";

	$dbcreator = new DBCreator($wpdb, "smis_mr_jenis_rs", DBCreator::$ENGINE_MYISAM);
	$dbcreator->addColumn("jenis_rs", "varchar(256)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("uraian", "varchar(512)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->initialize();
?>