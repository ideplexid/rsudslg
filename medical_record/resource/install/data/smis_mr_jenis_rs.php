<?php
	global $wpdb;

	$query = "
		INSERT INTO `smis_mr_jenis_rs` (`id`, `prop`, `jenis_rs`, `uraian`, `autonomous`, `duplicate`, `origin`, `origin_id`, `time_updated`, `origin_updated`) VALUES(1, '', 'RSU', 'Rumah Sakit Umum', '', 0, '', 0, '0000-00-00 00:00:00', '');
		INSERT INTO `smis_mr_jenis_rs` (`id`, `prop`, `jenis_rs`, `uraian`, `autonomous`, `duplicate`, `origin`, `origin_id`, `time_updated`, `origin_updated`) VALUES(2, '', 'RS Jiwa/RSKO', 'Rumah Sakit Jiwa / Ketergantungan Obat', '', 0, '', 0, '0000-00-00 00:00:00', '');
		INSERT INTO `smis_mr_jenis_rs` (`id`, `prop`, `jenis_rs`, `uraian`, `autonomous`, `duplicate`, `origin`, `origin_id`, `time_updated`, `origin_updated`) VALUES(3, '', 'RSB', 'Rumah Sakit Bersalin', '', 0, '', 0, '0000-00-00 00:00:00', '');
		INSERT INTO `smis_mr_jenis_rs` (`id`, `prop`, `jenis_rs`, `uraian`, `autonomous`, `duplicate`, `origin`, `origin_id`, `time_updated`, `origin_updated`) VALUES(4, '', 'RS Mata', 'Rumah Sakit Mata', '', 0, '', 0, '0000-00-00 00:00:00', '');
		INSERT INTO `smis_mr_jenis_rs` (`id`, `prop`, `jenis_rs`, `uraian`, `autonomous`, `duplicate`, `origin`, `origin_id`, `time_updated`, `origin_updated`) VALUES(5, '', 'RS Kanker', 'Rumah Sakit Kanker', '', 0, '', 0, '0000-00-00 00:00:00', '');
		INSERT INTO `smis_mr_jenis_rs` (`id`, `prop`, `jenis_rs`, `uraian`, `autonomous`, `duplicate`, `origin`, `origin_id`, `time_updated`, `origin_updated`) VALUES(6, '', 'RSTP', 'Rumah Sakit Tuberkulosa Paru', '', 0, '', 0, '0000-00-00 00:00:00', '');
		INSERT INTO `smis_mr_jenis_rs` (`id`, `prop`, `jenis_rs`, `uraian`, `autonomous`, `duplicate`, `origin`, `origin_id`, `time_updated`, `origin_updated`) VALUES(7, '', 'RS Kusta', 'Rumah Sakit Kusta', '', 0, '', 0, '0000-00-00 00:00:00', '');
		INSERT INTO `smis_mr_jenis_rs` (`id`, `prop`, `jenis_rs`, `uraian`, `autonomous`, `duplicate`, `origin`, `origin_id`, `time_updated`, `origin_updated`) VALUES(8, '', 'RS Penyakit Infeksi', 'Rumah Sakit Penyakit Infeksi', '', 0, '', 0, '0000-00-00 00:00:00', '');
		INSERT INTO `smis_mr_jenis_rs` (`id`, `prop`, `jenis_rs`, `uraian`, `autonomous`, `duplicate`, `origin`, `origin_id`, `time_updated`, `origin_updated`) VALUES(9, '', 'RSOP', 'Rumah Sakit Orthopedi', '', 0, '', 0, '0000-00-00 00:00:00', '');
		INSERT INTO `smis_mr_jenis_rs` (`id`, `prop`, `jenis_rs`, `uraian`, `autonomous`, `duplicate`, `origin`, `origin_id`, `time_updated`, `origin_updated`) VALUES(10, '', 'RSK P. Dalam', 'Rumah Sakit Penyakit Dalam', '', 0, '', 0, '0000-00-00 00:00:00', '');
		INSERT INTO `smis_mr_jenis_rs` (`id`, `prop`, `jenis_rs`, `uraian`, `autonomous`, `duplicate`, `origin`, `origin_id`, `time_updated`, `origin_updated`) VALUES(11, '', 'RSK Bedah', 'Rumah Sakit', '', 0, '', 0, '0000-00-00 00:00:00', '');
		INSERT INTO `smis_mr_jenis_rs` (`id`, `prop`, `jenis_rs`, `uraian`, `autonomous`, `duplicate`, `origin`, `origin_id`, `time_updated`, `origin_updated`) VALUES(12, '', 'RS Jantung', 'Rumah Sakit Jantung', '', 0, '', 0, '0000-00-00 00:00:00', '');
		INSERT INTO `smis_mr_jenis_rs` (`id`, `prop`, `jenis_rs`, `uraian`, `autonomous`, `duplicate`, `origin`, `origin_id`, `time_updated`, `origin_updated`) VALUES(13, '', 'RSK THT', 'Rumah Sakit Khusus THT', '', 0, '', 0, '0000-00-00 00:00:00', '');
		INSERT INTO `smis_mr_jenis_rs` (`id`, `prop`, `jenis_rs`, `uraian`, `autonomous`, `duplicate`, `origin`, `origin_id`, `time_updated`, `origin_updated`) VALUES(14, '', 'RS Stroke', 'Rumah Sakit Stroke', '', 0, '', 0, '0000-00-00 00:00:00', '');
		INSERT INTO `smis_mr_jenis_rs` (`id`, `prop`, `jenis_rs`, `uraian`, `autonomous`, `duplicate`, `origin`, `origin_id`, `time_updated`, `origin_updated`) VALUES(15, '', 'RSAB', 'Rumah Sakit Anak dan Bunda', '', 0, '', 0, '0000-00-00 00:00:00', '');
		INSERT INTO `smis_mr_jenis_rs` (`id`, `prop`, `jenis_rs`, `uraian`, `autonomous`, `duplicate`, `origin`, `origin_id`, `time_updated`, `origin_updated`) VALUES(16, '', 'RSIA', 'Rumah Sakit Ibu dan Anak', '', 0, '', 0, '0000-00-00 00:00:00', '');
		INSERT INTO `smis_mr_jenis_rs` (`id`, `prop`, `jenis_rs`, `uraian`, `autonomous`, `duplicate`, `origin`, `origin_id`, `time_updated`, `origin_updated`) VALUES(17, '', 'RSK Anak', 'Rumah Sakit Khusus Anak', '', 0, '', 0, '0000-00-00 00:00:00', '');
		INSERT INTO `smis_mr_jenis_rs` (`id`, `prop`, `jenis_rs`, `uraian`, `autonomous`, `duplicate`, `origin`, `origin_id`, `time_updated`, `origin_updated`) VALUES(18, '', 'RSK Syaraf', 'Rumah Sakit Khusus Syaraf', '', 0, '', 0, '0000-00-00 00:00:00', '');
		INSERT INTO `smis_mr_jenis_rs` (`id`, `prop`, `jenis_rs`, `uraian`, `autonomous`, `duplicate`, `origin`, `origin_id`, `time_updated`, `origin_updated`) VALUES(19, '', 'RSK GM', 'Rumah Sakit Khusus Gigi dan Mulut', '', 0, '', 0, '0000-00-00 00:00:00', '');
	";
	$wpdb->query($query);
?>