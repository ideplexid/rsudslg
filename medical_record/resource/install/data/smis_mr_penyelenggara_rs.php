<?php
	global $wpdb;

	$query = "
		INSERT INTO `smis_mr_penyelenggara_rs` (`id`, `prop`, `penyelenggara`, `autonomous`, `duplicate`, `origin`, `origin_id`, `time_updated`, `origin_updated`) VALUES(1, '', 'BUMN', '', 0, '', 0, '0000-00-00 00:00:00', '');
		INSERT INTO `smis_mr_penyelenggara_rs` (`id`, `prop`, `penyelenggara`, `autonomous`, `duplicate`, `origin`, `origin_id`, `time_updated`, `origin_updated`) VALUES(2, '', 'Kementerian Kesehatan', '', 0, '', 0, '0000-00-00 00:00:00', '');
		INSERT INTO `smis_mr_penyelenggara_rs` (`id`, `prop`, `penyelenggara`, `autonomous`, `duplicate`, `origin`, `origin_id`, `time_updated`, `origin_updated`) VALUES(3, '', 'Kementerian yang lain', '', 0, '', 0, '0000-00-00 00:00:00', '');
		INSERT INTO `smis_mr_penyelenggara_rs` (`id`, `prop`, `penyelenggara`, `autonomous`, `duplicate`, `origin`, `origin_id`, `time_updated`, `origin_updated`) VALUES(4, '', 'Organisasi Budha', '', 0, '', 0, '0000-00-00 00:00:00', '');
		INSERT INTO `smis_mr_penyelenggara_rs` (`id`, `prop`, `penyelenggara`, `autonomous`, `duplicate`, `origin`, `origin_id`, `time_updated`, `origin_updated`) VALUES(5, '', 'Organisasi Hindu', '', 0, '', 0, '0000-00-00 00:00:00', '');
		INSERT INTO `smis_mr_penyelenggara_rs` (`id`, `prop`, `penyelenggara`, `autonomous`, `duplicate`, `origin`, `origin_id`, `time_updated`, `origin_updated`) VALUES(6, '', 'Organisasi Islam', '', 0, '', 0, '0000-00-00 00:00:00', '');
		INSERT INTO `smis_mr_penyelenggara_rs` (`id`, `prop`, `penyelenggara`, `autonomous`, `duplicate`, `origin`, `origin_id`, `time_updated`, `origin_updated`) VALUES(7, '', 'Organisasi Khatolik', '', 0, '', 0, '0000-00-00 00:00:00', '');
		INSERT INTO `smis_mr_penyelenggara_rs` (`id`, `prop`, `penyelenggara`, `autonomous`, `duplicate`, `origin`, `origin_id`, `time_updated`, `origin_updated`) VALUES(8, '', 'Organisasi Protestan', '', 0, '', 0, '0000-00-00 00:00:00', '');
		INSERT INTO `smis_mr_penyelenggara_rs` (`id`, `prop`, `penyelenggara`, `autonomous`, `duplicate`, `origin`, `origin_id`, `time_updated`, `origin_updated`) VALUES(9, '', 'Organisasi Sosial', '', 0, '', 0, '0000-00-00 00:00:00', '');
		INSERT INTO `smis_mr_penyelenggara_rs` (`id`, `prop`, `penyelenggara`, `autonomous`, `duplicate`, `origin`, `origin_id`, `time_updated`, `origin_updated`) VALUES(10, '', 'Pemkab', '', 0, '', 0, '0000-00-00 00:00:00', '');
		INSERT INTO `smis_mr_penyelenggara_rs` (`id`, `prop`, `penyelenggara`, `autonomous`, `duplicate`, `origin`, `origin_id`, `time_updated`, `origin_updated`) VALUES(11, '', 'Pemkot', '', 0, '', 0, '0000-00-00 00:00:00', '');
		INSERT INTO `smis_mr_penyelenggara_rs` (`id`, `prop`, `penyelenggara`, `autonomous`, `duplicate`, `origin`, `origin_id`, `time_updated`, `origin_updated`) VALUES(12, '', 'Pemprop', '', 0, '', 0, '0000-00-00 00:00:00', '');
		INSERT INTO `smis_mr_penyelenggara_rs` (`id`, `prop`, `penyelenggara`, `autonomous`, `duplicate`, `origin`, `origin_id`, `time_updated`, `origin_updated`) VALUES(13, '', 'Perorangan', '', 0, '', 0, '0000-00-00 00:00:00', '');
		INSERT INTO `smis_mr_penyelenggara_rs` (`id`, `prop`, `penyelenggara`, `autonomous`, `duplicate`, `origin`, `origin_id`, `time_updated`, `origin_updated`) VALUES(14, '', 'Perusahaan', '', 0, '', 0, '0000-00-00 00:00:00', '');
		INSERT INTO `smis_mr_penyelenggara_rs` (`id`, `prop`, `penyelenggara`, `autonomous`, `duplicate`, `origin`, `origin_id`, `time_updated`, `origin_updated`) VALUES(15, '', 'POLRI', '', 0, '', 0, '0000-00-00 00:00:00', '');
		INSERT INTO `smis_mr_penyelenggara_rs` (`id`, `prop`, `penyelenggara`, `autonomous`, `duplicate`, `origin`, `origin_id`, `time_updated`, `origin_updated`) VALUES(16, '', 'Swasta/Lainnya', '', 0, '', 0, '0000-00-00 00:00:00', '');
		INSERT INTO `smis_mr_penyelenggara_rs` (`id`, `prop`, `penyelenggara`, `autonomous`, `duplicate`, `origin`, `origin_id`, `time_updated`, `origin_updated`) VALUES(17, '', 'TNI AD', '', 0, '', 0, '0000-00-00 00:00:00', '');
		INSERT INTO `smis_mr_penyelenggara_rs` (`id`, `prop`, `penyelenggara`, `autonomous`, `duplicate`, `origin`, `origin_id`, `time_updated`, `origin_updated`) VALUES(18, '', 'TNI AL', '', 0, '', 0, '0000-00-00 00:00:00', '');
		INSERT INTO `smis_mr_penyelenggara_rs` (`id`, `prop`, `penyelenggara`, `autonomous`, `duplicate`, `origin`, `origin_id`, `time_updated`, `origin_updated`) VALUES(19, '', 'TNI AU', '', 0, '', 0, '0000-00-00 00:00:00', '');
	";
	$wpdb->query($query);
?>