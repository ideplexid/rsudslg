/**
 * this js used for handle request function in 
 * resume medis. each function hold unique request based in service
 * 
 * @author 		: Nurul Huda
 * @license 	: LGPLv3
 * @version 	: 1.0.0
 * @since 		: 24 Sept 2016
 * @copyright 	: goblooge@gmail.com
 * @used 		: medical_record/resource/laporan/resume_medis.php
 * */

var pasien;
var resume_medis;
var diagnosa;
var lab;
var rad;
var obat;
var tindakan;
var RESUME_MEDIS_NOREG_PASIEN=0;
$(document).ready(function(){
	resume_medis=new TableAction("resume_medis","medical_record","resume_medis");
	resume_medis.addRegulerData=function(reg_data){
		reg_data['nrm']=$("#pasien_nrm").val();
		return reg_data;
	};
	resume_medis.file_data=function(id){
		var d = this.getRegulerData();
		d['noreg_pasien'] = id;
		d['command'] = "file_resume_medis"
		showLoading();
		$.post("",d,function(res){
			var j = getContent(res);
			if(j!=null && j!=""){
				window.open(j, '_blank').focus();
			}
			dismissLoading();
		});
	}
	resume_medis.diagnosa=function(id){
		var data=this.getRegulerData();
		data['super_command']="diagnosa";
		data['noreg_pasien']=id;
		$.post("",data,function(res){
			RESUME_MEDIS_NOREG_PASIEN=id;
			showFullWarning("Diagnosa",res,"full_model");
			diagnosa.view();
		});			
	};
	resume_medis.tindakan=function(id){
		var data=this.getRegulerData();
		data['super_command']="tindakan";
		data['noreg_pasien']=id;
		$.post("",data,function(res){
			RESUME_MEDIS_NOREG_PASIEN=id;
			showFullWarning("Tindakan",res,"full_model");
			tindakan.view();
		});			
	};

	resume_medis.lab=function(id){
		var data=this.getRegulerData();
		data['super_command']="lab";
		data['noreg_pasien']=id;
		$.post("",data,function(res){
			RESUME_MEDIS_NOREG_PASIEN=id;
			showFullWarning("Laboratory",res,"full_model");
			lab.view();
		});			
	};

	resume_medis.rad=function(id){
		var data=this.getRegulerData();
		data['super_command']="rad";
		data['noreg_pasien']=id;
		$.post("",data,function(res){
			RESUME_MEDIS_NOREG_PASIEN=id;
			showFullWarning("Radiology",res,"full_model");
			rad.view();
		});			
	};

	resume_medis.obat=function(id){
		var data=this.getRegulerData();
		data['super_command']="obat";
		data['noreg_pasien']=id;
		$.post("",data,function(res){
			RESUME_MEDIS_NOREG_PASIEN=id;
			showFullWarning("Obat",res,"full_model");
			obat.view();
		});			
	};
	
	pasien=new TableAction("pasien","medical_record","resume_medis");
	pasien.setSuperCommand("pasien");
	pasien.selected=function(json){
		$("#pasien_pasien").val(json.nama);	
		$("#pasien_alamat").val(json.alamat);	
		$("#pasien_nrm").val(json.id);
		var kelamin=json.kelamin=="0"?"Laki-Laki":"Perempuan";	
		$("#pasien_jk").val(kelamin);
		resume_medis.view();
	};

	diagnosa=new TableAction("diagnosa","medical_record","resume_medis");
	diagnosa.setSuperCommand("diagnosa");
	diagnosa.addRegulerData=function(reg_data){
		reg_data['noreg_pasien']=RESUME_MEDIS_NOREG_PASIEN;
		return reg_data;
	};

	tindakan=new TableAction("tindakan","medical_record","resume_medis");
	tindakan.setSuperCommand("tindakan");
	tindakan.addRegulerData=function(reg_data){
		reg_data['noreg_pasien']=RESUME_MEDIS_NOREG_PASIEN;
		return reg_data;
	};

	lab=new TableAction("lab","medical_record","resume_medis");
	lab.setSuperCommand("lab");
	lab.hasil=function(id){
		var r = this.getRegulerData();
		r['command'] = "hasil";
		r['id'] = id;
		showLoading();
		$.post("",r,function(res){
			var json = getContent(res);
			if(json!=""){
				window.open(json, '_blank').focus();
			}
			dismissLoading();
		});
	};
	lab.addRegulerData=function(reg_data){
		reg_data['noreg_pasien']=RESUME_MEDIS_NOREG_PASIEN;
		return reg_data;
	};

	rad=new TableAction("rad","medical_record","resume_medis");
	rad.setSuperCommand("rad");
	rad.hasil=function(id){
		var hasil=$("#hasil_rad_"+id).html();
		//$("#print_table_rsm_rad").html(hasil);
		//$("#table_rsm_rad").hide();
		showWarning("Hasil Radiology",hasil);

		//$("#rad_modal .modal-body").html(hasil);
		//$("#rad_modal").smodal("show");

	};
	rad.addRegulerData=function(reg_data){
		reg_data['noreg_pasien']=RESUME_MEDIS_NOREG_PASIEN;
		return reg_data;
	};

	

	obat=new TableAction("obat","medical_record","resume_medis");
	obat.setSuperCommand("obat");
	obat.hasil=function(id){
		var hasil=$("#hasil_obat_"+id).html();
		$("#obat_modal .modal-body").html(hasil);
		$("#obat_modal").smodal("show");
	};
	obat.addRegulerData=function(reg_data){
		reg_data['noreg_pasien']=RESUME_MEDIS_NOREG_PASIEN;
		return reg_data;
	};

	
});