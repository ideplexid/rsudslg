/**
 * this source code used for handling
 * infus report
 * 
 * @author 		: Nurul Huda
 * @license 	: Apache v3
 * @version 	: 1.0.0
 * @since 		: 24 Sept 2016
 * @copyright 	: goblooge@gmail.com
 * @used		: medical_record/resource/php/indikator_klinis/infus.php
 * 
 * */
 

var infus;
$(document).ready(function(){
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
	$('.mydate').datepicker();
	var column=new Array('ruangan');
	infus=new ReportAction("infus","medical_record","infus",column);
	infus.setXKey('Date');
	infus.setYKey(['ya','tidak']);
	infus.setLabels(["Ya","Tidak"]);
	infus.setDiagramHolder("infus_chart");
	infus.sourcePieAdapter=function(json){
		var ya=0;
		var tidak=0;
		$(json).each(function(idx, obj){
			ya+=obj.ya;
			tidak+=obj.tidak;
		});
		var so=[{"Name":"Ya",'Total':ya},{"Name":"Tidak","Total":tidak}];
		return so;
	};
	infus.initDiagram("");
	infus.setTitle("Diagram Infus");
});