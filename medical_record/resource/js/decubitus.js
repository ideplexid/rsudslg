/**
 * this source code used for handling
 * decubitus report
 * 
 * @author 		: Nurul Huda
 * @license 	: Apache v3
 * @version 	: 1.0.0
 * @since 		: 24 Sept 2016
 * @copyright 	: goblooge@gmail.com
 * @used		: medical_record/resource/php/indikator_klinis/decubitus.php
 * 
 * */
var decubitus;
$(document).ready(function(){
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
	$('.mydate').datepicker();
	var column=new Array('ruangan');
	decubitus=new ReportAction("decubitus","medical_record","decubitus",column);
	decubitus.setXKey('Date');
	decubitus.setYKey(['ya','tidak']);
	decubitus.setLabels(["Ya","Tidak"]);
	decubitus.setDiagramHolder("chart_decubitus");
	decubitus.initDiagram("");
	decubitus.setTitle("Diagram Decubitus");
});