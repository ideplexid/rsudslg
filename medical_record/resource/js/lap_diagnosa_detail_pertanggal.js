var lap_diagnosa_detail_pertanggal;
$(document).ready(function(){
	$('.mydate').datepicker();
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
	var column=new Array('id','nama','keterangan','slug');
	lap_diagnosa_detail_pertanggal=new TableAction("lap_diagnosa_detail_pertanggal","medical_record","lap_diagnosa_detail_pertanggal",column);
	lap_diagnosa_detail_pertanggal.getRegulerData=function(){
		var reg_data={	
				page:this.page,
				action:this.action,
				super_command:this.super_command,
				prototype_name:this.prototype_name,
				prototype_slug:this.prototype_slug,
				prototype_implement:this.prototype_implement,
				dari:$("#"+this.prefix+"_dari").val(),
				sampai:$("#"+this.prefix+"_sampai").val()
				};
		return reg_data;
	};
	lap_diagnosa_detail_pertanggal.lap_diagnosa_detail_pertanggal_count=function(){
		$("#rload_modal_pertanggal").modal("show");
		var clear_data=this.getRegulerData();
		clear_data['super_command']="count";
		$.post("",clear_data,function(res){
			var json=getContent(res);
			var num=Number(json);
			$("#rload_modal_pertanggal").modal("show");			
			setTimeout(lap_diagnosa_detail_pertanggal.lap_diagnosa_detail_pertanggal_posting(num-1,num),100);	
		});
	};

	lap_diagnosa_detail_pertanggal.lap_diagnosa_detail_pertanggal_posting=function(number,total){
		if(number<0) {
			$("#rload_modal_pertanggal").modal("hide");
			lap_diagnosa_detail_pertanggal.reload();
			return;
		}
		$("#lap_diag_person_bar_pertanggal").sload("true","Loading... [ "+(total-number)+" / "+total+" ]",(total-number)*100/total);
		var clear_data=this.getRegulerData();
		clear_data['super_command']="posting";
		clear_data['limit']=number;
		$.post("",clear_data,function(res){
			var json=getContent(res);
			number--;
			setTimeout(lap_diagnosa_detail_pertanggal.lap_diagnosa_detail_pertanggal_posting(number,total),100);
		});
	};

	lap_diagnosa_detail_pertanggal.view=function(){	
		var self=this;
		var view_data=this.getViewData();
		if(view_data==null){
			showWarning("Error",this.view_data_null_message);
			return;
		}
		showLoading();
		$.post('',view_data,function(res){
			var json=getContent(res);
			if(json==null) {
				
			}else{
				$("#"+self.prefix+"_list").html(json.list);
				$("#"+self.prefix+"_pagination").html(json.pagination);	
			}
			self.afterview(json);
			dismissLoading();

			$(".lap_diag_title_h5").parents().attr("colspan","4");
			$(".lap_diag_title_h5").parentsUntil("tr").siblings().remove();
		});
	};
	
});