var data_kunjungan;
$(document).ready(function() {
	$(".mydate").datepicker();
	$(".mydatetime").datetimepicker({
		minuteStep: 1
	});
	data_kunjungan = new TableAction(
		"data_kunjungan",
		"medical_record",
		"data_kunjungan",
		new Array("id", "jenis_kegiatan", "tanggal_inap", "tanggal_pulang", "carapulang")
	);
	data_kunjungan.addViewData = function(data) {
		data['tanggal_from'] = $("#data_kunjungan_tanggal_from").val();
		data['tanggal_to'] = $("#data_kunjungan_tanggal_to").val();
		data['carapulang'] = $("#data_kunjungan_carapulang").val();
		data['carapulang_label'] = $("#data_kunjungan_carapulang option:selected").text();
		data['ruangan'] = $("#data_kunjungan_ruangan").val();
		data['ruangan_label'] = $("#data_kunjungan_ruangan option:selected").text();
		data['uri'] = $("#data_kunjungan_uri").val();
		data['uri_label'] = $("#data_kunjungan_uri option:selected").text();
		return data;
	};
	data_kunjungan.edit = function(id) {
		showLoading();
		var data = this.getRegulerData();
		data['id'] = id;
		data['command'] = "edit";
		$.post(
			"",
			data,
			function(response) {
				var json = getContent(response);
				if (json == null) return;
				$("#kunjungan_id").val(id);
				$("#kunjungan_noreg_pasien").val(json.id);
				$("#kunjungan_nrm_pasien").val(json.nrm);
				$("#kunjungan_nama_pasien").val(json.nama_pasien);
				$("#kunjungan_uri").val(json.uri);
				if (json.uri == 1)
					$("#kunjungan_uri_label").val("Rawat Inap");
				else
					$("#kunjungan_uri_label").val("Rawat Jalan");
				var waktu_masuk_parts = json.tanggal.split(" ");
				var tanggal_masuk_parts = waktu_masuk_parts[0].split("-");
				var tanggal_masuk = tanggal_masuk_parts[2] + "-" + tanggal_masuk_parts[1] + "-" + tanggal_masuk_parts[0] + " " + waktu_masuk_parts[1];
				$("#kunjungan_tanggal").val(tanggal_masuk);
				$("#kunjungan_caramasuk").val(json.caradatang);
				var ruang_rj = json.jenislayanan.replace("_", " ").toUpperCase();
				$("#kunjungan_ruang_rj").val(ruang_rj);
				$("#kunjungan_jenis_kegiatan").val(json.jenis_kegiatan);
				var waktu_inap_parts = json.tanggal_inap.split(" ");
				var tanggal_inap_parts = waktu_inap_parts[0].split("-");
				var jam_inap_parts = waktu_inap_parts[1].split(":");
				var tanggal_inap = tanggal_inap_parts[2] + "-" + tanggal_inap_parts[1] + "-" + tanggal_inap_parts[0] + " " + jam_inap_parts[0] + ":" + jam_inap_parts[1];
				$("#kunjungan_tanggal_inap").val(tanggal_inap);
				var ruang_inap_terakhir = json.last_ruangan.replace("_", " ").toUpperCase();
				$("#kunjungan_last_ruangan").val(ruang_inap_terakhir);
				var waktu_pulang_parts = json.tanggal_pulang.split(" ");
				var tanggal_pulang_parts = waktu_pulang_parts[0].split("-");
				var jam_pulang_parts = waktu_pulang_parts[1].split(":");
				var tanggal_pulang = tanggal_pulang_parts[2] + "-" + tanggal_pulang_parts[1] + "-" + tanggal_pulang_parts[0] + " " + jam_pulang_parts[0] + ":" + jam_pulang_parts[1];
				$("#kunjungan_tanggal_pulang").val(tanggal_pulang);
				$("#kunjungan_carapulang").val(json.carapulang);
				$("#kunjungan_lama_dirawat").val(Math.round(json.lama_dirawat));
				var carabayar = json.carabayar.replace("_", " ").toUpperCase();
				$("#kunjungan_carabayar").val(carabayar);
				if (json.selsai == 0)
					$("#kunjungan_status").val("Aktif");
				else
					$("#kunjungan_status").val("Tidak Aktif");
				$("#data_kunjungan_add_form").smodal("show");
				dismissLoading();
			}
		);
	};
	data_kunjungan.getSaveData = function() {
		var data = this.getRegulerData();
		data['command'] = "save";
		data['id'] = $("#kunjungan_id").val();
		data['uri'] = $("#kunjungan_uri").val();
		data['jenis_kegiatan'] = $("#kunjungan_jenis_kegiatan").val();
		var tanggal_parts = $("#kunjungan_tanggal").val().split(" ")[0].split("-");
		var jam = $("#kunjungan_tanggal").val().split(" ")[1];
		var tanggal = tanggal_parts[2] + "-" + tanggal_parts[1] + "-" + tanggal_parts[0] + " " + jam;
		data['tanggal'] = tanggal;
		var tanggal_inap_parts = $("#kunjungan_tanggal_inap").val().split(" ")[0].split("-");
		var jam_inap = $("#kunjungan_tanggal_inap").val().split(" ")[1] + ":00";
		var tanggal_inap = tanggal_inap_parts[2] + "-" + tanggal_inap_parts[1] + "-" + tanggal_inap_parts[0] + " " + jam_inap;
		data['tanggal_inap'] = tanggal_inap;
		var tanggal_pulang_parts = $("#kunjungan_tanggal_pulang").val().split(" ")[0].split("-");
		var jam_pulang = $("#kunjungan_tanggal_pulang").val().split(" ")[1] + ":00";
		var tanggal_pulang = tanggal_pulang_parts[2] + "-" + tanggal_pulang_parts[1] + "-" + tanggal_pulang_parts[0] + " " + jam_pulang;
		data['tanggal_pulang'] = tanggal_pulang;
		data['carapulang'] = $("#kunjungan_carapulang").val();
		return data;
	};
	data_kunjungan.del = function(id) {
		var self = this;
		var data = this.getRegulerData();
		data['command'] = "cancel";
		data['id'] = id;
		data['carapulang'] = "Tidak Datang";
		bootbox.confirm(
			"Yakin membatalkan kunjungan ini (Tidak Datang)?",
			function(result) {
				if (result) {
					showLoading();
					$.post(
						"",
						data,
						function() {
							self.view();
							dismissLoading();
						}			
					);
				}
			}
		);
	};
	data_kunjungan.export_xls = function() {
		var data = this.getViewData();
        data['action'] = "create_excel_data_kunjungan";
        download(data);
	};
	data_kunjungan.view();
});