var rl35;
$(document).ready(function() {
	$("#rl35_list").html("<tr><td colspan='18'><center>DATA BELUM DITAMPILKAN</center></td></tr>");
	rl35 = new TableAction(
		"rl35",
		"medical_record",
		"rl35",
		new Array()
	);
	rl35.view = function() {
		var data = this.getRegulerData();
		data['command'] = "view";
		data['tahun'] = $("#rl35_tahun_filter").val();
		data['bulan'] = $("#rl35_bulan_filter").val();
		showLoading();
		$.post(
			"",
			data,
			function(response) {
				var json = JSON.parse(response);
				if (json == null) {
					dismissLoading();
					return;
				}
				$("#rl35_list").html(json.html);
				dismissLoading();
			}
		);
	};
	rl35.excel = function() {
		if ($("#rl35_tahun").length) {
			showLoading();
			var num_rows = $("#rl35_list tr.data_rl35").length;
			var detail = {};
			for (var i = 0; i < num_rows; i++) {
				detail[i] = {
					'kategori'						: $("#rl35_list tr.data_rl35:eq(" + i + ") td#rl35_kategori").text(),
					'kode_propinsi'	 				: $("#rl35_list tr.data_rl35:eq(" + i + ") td#rl35_kode_propinsi").text(),
					'kabupaten_kota' 				: $("#rl35_list tr.data_rl35:eq(" + i + ") td#rl35_kabupaten_kota").text(),
					'kode_rs'		 				: $("#rl35_list tr.data_rl35:eq(" + i + ") td#rl35_kode_rs").text(),
					'nama_rs'		 				: $("#rl35_list tr.data_rl35:eq(" + i + ") td#rl35_nama_rs").text(),
					'tahun' 		 				: $("#rl35_list tr.data_rl35:eq(" + i + ") td#rl35_tahun").text(),
					'nomor' 		 				: $("#rl35_list tr.data_rl35:eq(" + i + ") td#rl35_nomor").text(),
					'jenis_kegiatan'				: $("#rl35_list tr.data_rl35:eq(" + i + ") td#rl35_jenis_kegiatan").text(),
					'rujukan_medis_rs'				: $("#rl35_list tr.data_rl35:eq(" + i + ") td#rl35_rujukan_medis_rs").text(),
					'rujukan_medis_bidan'			: $("#rl35_list tr.data_rl35:eq(" + i + ") td#rl35_rujukan_medis_bidan").text(),
					'rujukan_medis_puskesmas'		: $("#rl35_list tr.data_rl35:eq(" + i + ") td#rl35_rujukan_medis_puskesmas").text(),
					'rujukan_medis_faskes_lainnya'	: $("#rl35_list tr.data_rl35:eq(" + i + ") td#rl35_rujukan_medis_faskes_lainnya").text(),
					'rujukan_medis_mati'			: $("#rl35_list tr.data_rl35:eq(" + i + ") td#rl35_rujukan_medis_mati").text(),
					'rujukan_medis_jumlah_total'	: $("#rl35_list tr.data_rl35:eq(" + i + ") td#rl35_rujukan_medis_jumlah_total").text(),
					'rujukan_non_medis_mati'		: $("#rl35_list tr.data_rl35:eq(" + i + ") td#rl35_rujukan_non_medis_mati").text(),
					'rujukan_non_medis_jumlah_total': $("#rl35_list tr.data_rl35:eq(" + i + ") td#rl35_rujukan_non_medis_jumlah_total").text(),
					'non_rujukan_mati'				: $("#rl35_list tr.data_rl35:eq(" + i + ") td#rl35_non_rujukan_mati").text(),
					'non_rujukan_jumlah_total'		: $("#rl35_list tr.data_rl35:eq(" + i + ") td#rl35_non_rujukan_jumlah_total").text(),
					'dirujuk'						: $("#rl35_list tr.data_rl35:eq(" + i + ") td#rl35_dirujuk").text()
				};
			}
			var data = this.getRegulerData();
			data['command'] = "export_xls";
			data['tahun'] = $("#rl35_tahun_filter").val();
			data['detail'] = JSON.stringify(detail);
			postForm(data);
			dismissLoading();
		}
	};
});