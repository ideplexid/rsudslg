var asesmen_perawat;
var asp_pasien;
var dokter_asp;
//var dokter_dpjp_asp;
var perawat_asp;

var asp_noreg=$("#asp_noreg_pasien").val();
var asp_nama_pasien=$("#asp_nama_pasien").val();
var asp_nrm_pasien=$("#asp_nrm_pasien").val();
var asp_polislug=$("#asp_polislug").val();
var asp_the_page=$("#asp_the_page").val();
var asp_the_protoslug=$("#asp_the_protoslug").val();
var asp_the_protoname=$("#asp_the_protoname").val();
var asp_the_protoimplement=$("#asp_the_protoimplement").val();
var asp_id_antrian=$("#asp_id_antrian").val();
var asp_action=$("#asp_action").val();

$(document).ready(function() {
    $('.mydate').datepicker();
    $('.mydatetime').datetimepicker();
    
    asp_pasien=new TableAction("asp_pasien",asp_the_page,asp_action,new Array());
    asp_pasien.setSuperCommand("asp_pasien");
    asp_pasien.setPrototipe(asp_the_protoname,asp_the_protoslug,asp_the_protoimplement);
    
    asp_pasien.selected=function(json){
        var nama=json.nama_pasien;
        var nrm=json.nrm;
        var noreg=json.id;		
        $("#"+asp_action+"_nama_pasien").val(nama);
        $("#"+asp_action+"_nrm_pasien").val(nrm);
        $("#"+asp_action+"_noreg_pasien").val(noreg);
    };
    
    dokter_asp=new TableAction("dokter_asp",asp_the_page,asp_action,new Array());
    dokter_asp.setSuperCommand("dokter_asp");
    dokter_asp.setPrototipe(asp_the_protoname,asp_the_protoslug,asp_the_protoimplement);
    dokter_asp.selected=function(json){
        $("#"+asp_action+"_nama_dokter").val(json.nama);
        $("#"+asp_action+"_id_dokter").val(json.id);
    };

    stypeahead("#"+asp_action+"_dokter_asp",3,dokter_asp,"nama",function(item){
        $("#"+asp_action+"_nama_dokter").val(json.nama);
        $("#"+asp_action+"_id_dokter").val(json.id);			
    });
    
    /*dokter_dpjp_asp=new TableAction("dokter_dpjp_asp",asp_the_page,asp_action,new Array());
    dokter_dpjp_asp.setSuperCommand("dokter_dpjp_asp");
    dokter_dpjp_asp.setPrototipe(asp_the_protoname,asp_the_protoslug,asp_the_protoimplement);
    dokter_dpjp_asp.selected=function(json){
        $("#"+asp_action+"_nama_dokter_dpjp").val(json.nama);
        $("#"+asp_action+"_id_dokter_dpjp").val(json.id);
    };

    stypeahead("#"+asp_action+"dokter_dpjp_asp",3,dokter_dpjp_asp,"nama",function(item){
        $("#"+asp_action+"_nama_dokter_dpjp").val(json.nama);
        $("#"+asp_action+"_id_dokter_dpjp").val(json.id);			
    });*/
    
    perawat_asp=new TableAction("perawat_asp",asp_the_page,asp_action,new Array());
    perawat_asp.setSuperCommand("perawat_asp");
    perawat_asp.setPrototipe(asp_the_protoname,asp_the_protoslug,asp_the_protoimplement);
    perawat_asp.selected=function(json){
        $("#"+asp_action+"_nama_perawat").val(json.nama);
        $("#"+asp_action+"_id_perawat").val(json.id);
    };

    stypeahead("#"+asp_action+"perawat_asp",3,perawat_asp,"nama",function(item){
        $("#"+asp_action+"_nama_perawat").val(json.nama);
        $("#"+asp_action+"_id_perawat").val(json.id);			
    });
    
    var column=new Array(
        "id","waktu","ruangan", "waktu_masuk",
        "nama_pasien","noreg_pasien","nrm_pasien",
        "alamat_pasien","umur_pasien","jk", "carabayar","tgl_lahir_pasien",
        "id_dokter","nama_dokter",
        //"id_dokter_dpjp", "nama_dokter_dpjp",
        "id_perawat", "nama_perawat", "diagnosa_perawat",
        "bb", "tb", "rujukan", "caradatang", 
        "tekanan_darah", "frek_nafas", "nadi", "suhu", 
        "rkpr_sumber_data", "rkpr_sumber_data_lain", "rkpr_keluhan_utama", "rkpr_keadaan_umum", "rkpr_gizi", 
        "rkpr_tindakan_resusitasi", "rkpr_tindakan_resusitasi_lain", "rkpr_saturasi_o2", "rkpr_pada", 
        "psks_status_nikah", "psks_anak", "psks_jumlah_anak", "psks_pend_akhir", "psks_warganegara", 
        "psks_pekerjaan", "psks_agama", "psks_tinggal_bersama", "psks_tinggal_bersama_nama", "psks_tinggal_bersama_telp", 
        "rks_riwayat_penyakit", "rks_riwayat_penyakit_hipertensi", "rks_riwayat_penyakit_diabetes", "rks_riwayat_penyakit_jantung", 
        "rks_riwayat_penyakit_stroke", "rks_riwayat_penyakit_dialisis", "rks_riwayat_penyakit_asthma", "rks_riwayat_penyakit_kejang", 
        "rks_riwayat_penyakit_hati", "rks_riwayat_penyakit_kanker", "rks_riwayat_penyakit_tbc", "rks_riwayat_penyakit_glaukoma", 
        "rks_riwayat_penyakit_std", "rks_riwayat_penyakit_pendarahan", "rks_riwayat_penyakit_lain", "rks_riwayat_penyakit_lain_lain", 
        "rks_pengobatan_saat_ini", "rks_pengobatan_saat_ini_ket", "rks_riwayat_operasi", "rks_riwayat_operasi_ket", "rks_riwayat_transfusi", 
        "rks_reaksi_transfusi", "rks_reaksi_transfusi_ket", "rks_riwayat_penyakit_keluarga", "rks_ketergantungan", "rks_obat", "rks_jenis_jmlh_perhari", 
        "nutrisi_makan_perhari", "nutrisi_minum_perhari", "nutrisi_diet_khusus", "nutrisi_keluhan", "nutrisi_keluhan_ket", 
        "nutrisi_perubahan_bb", "nutrisi_perubahan_bb_penurunan", "nutrisi_perubahan_bb_kenaikan", 
        "alergi", "alergi_obat_tipereaksi", "alergi_makanan_tipereaksi", "alergi_lain_tipereaksi", 
        "nyeri", "sifat_nyeri", "skala_nyeri", "nyeri_lokasi", "nyeri_sejak", 
        "get_up_go_test", "get_up_go_test_jalan_bungkuk", "get_up_go_test_topang_duduk", 
        "bicara", "bicara_ket", "penerjemah", "bahasa", "bhs_isyarat", "hambatan_belajar", "hambatan_belajar_ket", "catatan_lain", 
    );
    
    asesmen_perawat=new TableAction(asp_action,asp_the_page,asp_action,column);
    asesmen_perawat.setPrototipe(asp_the_protoname,asp_the_protoslug,asp_the_protoimplement);
    asesmen_perawat.getRegulerData=function(){
        var reg_data={	
                page:this.page,
                action:this.action,
                super_command:this.super_command,
                prototype_name:this.prototype_name,
                prototype_slug:this.prototype_slug,
                prototype_implement:this.prototype_implement,
                polislug:asp_polislug,
                noreg_pasien:asp_noreg,
                nama_pasien:asp_nama_pasien,
                nrm_pasien:asp_nrm_pasien,
                id_antrian:asp_id_antrian
                };
        return reg_data;
    };
});
