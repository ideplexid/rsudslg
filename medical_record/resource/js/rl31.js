var rl31;
$(document).ready(function() {
	$("#rl31_list").html("<tr><td colspan='21'><center>DATA BELUM DITAMPILKAN</center></td></tr>");
	rl31 = new TableAction(
		"rl31",
		"medical_record",
		"rl31",
		new Array()
	);
	rl31.view = function() {
		var data = this.getRegulerData();
		data['command'] = "view";
		data['tahun'] = $("#rl31_tahun_filter").val();
		data['bulan'] = $("#rl31_bulan_filter").val();
		showLoading();
		$.post(
			"",
			data,
			function(response) {
				var json = JSON.parse(response);
				if (json == null) {
					dismissLoading();
					return;
				}
				$("#rl31_list").html(json.html);
				dismissLoading();
			}
		);
	};
	rl31.excel = function() {
		if ($("#rl31_tahun").length) {
			showLoading();
			var num_rows = $("#rl31_list tr.data_rl31").length;
			var detail = {};
			for (var i = 0; i < num_rows; i++) {
				detail[i] = {
					'kode_propinsi'	 				: $("#rl31_list tr.data_rl31:eq(" + i + ") td#rl31_kode_propinsi").text(),
					'kabupaten_kota' 				: $("#rl31_list tr.data_rl31:eq(" + i + ") td#rl31_kabupaten_kota").text(),
					'kode_rs'		 				: $("#rl31_list tr.data_rl31:eq(" + i + ") td#rl31_kode_rs").text(),
					'nama_rs'		 				: $("#rl31_list tr.data_rl31:eq(" + i + ") td#rl31_nama_rs").text(),
					'tahun' 		 				: $("#rl31_list tr.data_rl31:eq(" + i + ") td#rl31_tahun").text(),
					'nomor' 		 				: $("#rl31_list tr.data_rl31:eq(" + i + ") td#rl31_nomor").text(),
					'jenis_layanan'					: $("#rl31_list tr.data_rl31:eq(" + i + ") td#rl31_jenis_layanan").text(),
					'pasien_awal'					: $("#rl31_list tr.data_rl31:eq(" + i + ") td#rl31_pasien_awal").text(),
					'pasien_masuk'					: $("#rl31_list tr.data_rl31:eq(" + i + ") td#rl31_pasien_masuk").text(),
					'pasien_keluar_hidup'			: $("#rl31_list tr.data_rl31:eq(" + i + ") td#rl31_pasien_keluar_hidup").text(),
					'pasien_keluar_mati_k48'		: $("#rl31_list tr.data_rl31:eq(" + i + ") td#rl31_pasien_keluar_mati_k48").text(),
					'pasien_keluar_mati_l48'		: $("#rl31_list tr.data_rl31:eq(" + i + ") td#rl31_pasien_keluar_mati_l48").text(),
					'los'							: $("#rl31_list tr.data_rl31:eq(" + i + ") td#rl31_los").text(),
					'pasien_akhir'					: $("#rl31_list tr.data_rl31:eq(" + i + ") td#rl31_pasien_akhir").text(),
					'jumlah_hp_total'				: $("#rl31_list tr.data_rl31:eq(" + i + ") td#rl31_jumlah_hp_total").text(),
					'jumlah_hp_vvip'				: $("#rl31_list tr.data_rl31:eq(" + i + ") td#rl31_jumlah_hp_vvip").text(),
					'jumlah_hp_vip'					: $("#rl31_list tr.data_rl31:eq(" + i + ") td#rl31_jumlah_hp_vip").text(),
					'jumlah_hp_i'					: $("#rl31_list tr.data_rl31:eq(" + i + ") td#rl31_jumlah_hp_i").text(),
					'jumlah_hp_ii'					: $("#rl31_list tr.data_rl31:eq(" + i + ") td#rl31_jumlah_hp_ii").text(),
					'jumlah_hp_iii'					: $("#rl31_list tr.data_rl31:eq(" + i + ") td#rl31_jumlah_hp_iii").text(),
					'jumlah_hp_khusus'				: $("#rl31_list tr.data_rl31:eq(" + i + ") td#rl31_jumlah_hp_khusus").text(),
				};
			}
			var data = this.getRegulerData();
			data['command'] = "export_xls";
			data['tahun'] = $("#rl31_tahun_filter").val();
			data['detail'] = JSON.stringify(detail);
			postForm(data);
			dismissLoading();
		}
	};
});