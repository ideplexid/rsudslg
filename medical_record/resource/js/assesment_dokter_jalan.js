var assesment_dokter_jalan;
var pasien_dokter_jalan;
$(document).ready(function(){
    assesment_dokter_jalan       = new TableAction("assesment_dokter_jalan","medical_record","assesment_dokter_jalan",new Array());
    pasien_dokter_jalan          = new TableAction("pasien_dokter_jalan","medical_record","assesment_dokter_jalan",new Array());
    pasien_dokter_jalan.setSuperCommand("pasien_dokter_jalan");
    pasien_dokter_jalan.selected = function(json){
        $("#"+assesment_dokter_jalan.prefix+"_nama_pasien").val(json.nama_pasien);
        $("#"+assesment_dokter_jalan.prefix+"_nrm_pasien").val(json.nrm);
        $("#"+assesment_dokter_jalan.prefix+"_carabayar").val(json.carabayar);
        $("#"+assesment_dokter_jalan.prefix+"_noreg_pasien").val(json.id);
    };
});