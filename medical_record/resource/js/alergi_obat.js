var alergi_obat;
var aler_dokter;
var aler_perawat;

var aler_noreg;
var aler_nama_pasien;
var aler_nrm_pasien;
var aler_polislug;
var aler_the_page;
var aler_the_protoslug;
var aler_the_protoname;
var aler_the_protoimplement;
$(document).ready(function() {
	
	aler_noreg=$("#aler_noreg").val();
	aler_nama_pasien=$("#aler_nama").val();
	aler_nrm_pasien=$("#aler_nrm").val();
	aler_polislug=$("#aler_polislug").val();
	aler_the_page=$("#aler_page").val();
	aler_the_protoslug=$("#aler_protoslug").val();
	aler_the_protoname=$("#aler_poliname").val();
	aler_the_protoimplement=$("#aler_implement").val();
	
	$(".mydatetime").datetimepicker({minuteStep:1});	
	var column=new Array(
		"id","waktu","jk",
		"carabayar","ruangan","nama_pasien",
		"noreg_pasien","nrm_pasien","nama_obat",
		'id_obat',"id_dokter","nama_dokter",
		"id_perawat","nama_perawat",
		"keterangan"
		);
	alergi_obat=new TableAction("alergi_obat",aler_the_page,"alergi_obat",column);
	alergi_obat.setPrototipe(aler_the_protoname,aler_the_protoslug,aler_the_protoimplement);
	alergi_obat.setEnableAutofocus(true);
	alergi_obat.setNextEnter();
	alergi_obat.getRegulerData=function(){
		var reg_data={	
				page:this.page,
				action:this.action,
				super_command:this.super_command,
				prototype_name:this.prototype_name,
				prototype_slug:this.prototype_slug,
				prototype_implement:this.prototype_implement,
				polislug:aler_polislug,
				noreg_pasien:aler_noreg,
				nama_pasien:aler_nama_pasien,
				nrm_pasien:aler_nrm_pasien,
				jk:$("#alergi_obat_jk").val()
				};
		return reg_data;
	};
	alergi_obat.view();
	
	aler_dokter=new TableAction("aler_dokter",mr_the_page,"alergi_obat",new Array());
	aler_dokter.setSuperCommand("aler_dokter");
	aler_dokter.setPrototipe(mr_the_protoname,mr_the_protoslug,mr_the_protoimplement);
	aler_dokter.selected=function(json){
		var nama=json.nama;
		var nip=json.id;		
		$("#alergi_obat_nama_dokter").val(nama);
		$("#alergi_obat_id_dokter").val(nip);
	};
	
	aler_perawat=new TableAction("aler_perawat",mr_the_page,"alergi_obat",new Array());
	aler_perawat.setSuperCommand("aler_perawat");
	aler_perawat.setPrototipe(mr_the_protoname,mr_the_protoslug,mr_the_protoimplement);
	aler_perawat.selected=function(json){
		var nama=json.nama;
		var nip=json.id;		
		$("#alergi_obat_nama_perawat").val(nama);
		$("#alergi_obat_id_perawat").val(nip);
	};
	
});