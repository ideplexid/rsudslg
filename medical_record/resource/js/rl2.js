var rl2;
$(document).ready(function() {
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
	var column = new Array();
	rl2 = new TableAction("rl2", "medical_record", "rl2", column);
    rl2.getRegulerData = function() {
    	var data = TableAction.prototype.getRegulerData.call(this);
        data['bulan'] = $("#rl2_bulan").val();
        data['label_bulan'] = $("#rl2_bulan option:selected").text();
        data['tahun'] = $("#rl2_tahun").val();
        return data;
    };
    rl2.excel = function() {
    	showLoading();
		var num_rows = $("#rl2_list").children("tr").length;
		var d_data = {};
		for (var i = 0; i < num_rows; i++) {
			var kode_rs = $("tbody#rl2_list tr:eq(" + i + ") td:eq(0)").text();
			var kode_prov = $("tbody#rl2_list tr:eq(" + i + ") td:eq(1)").text();
			var kab_kota = $("tbody#rl2_list tr:eq(" + i + ") td:eq(2)").text();
			var nama_rs = $("tbody#rl2_list tr:eq(" + i + ") td:eq(3)").text();
			var nomor = $("tbody#rl2_list tr:eq(" + i + ") td:eq(4)").text();
			var kualifikasi_pendidikan = $("tbody#rl2_list tr:eq(" + i + ") td:eq(5)").text();
			var keadaan_lk = parseFloat($("tbody#rl2_list tr:eq(" + i + ") td:eq(6)").text().replace(".", ""));
			var keadaan_pl = parseFloat($("tbody#rl2_list tr:eq(" + i + ") td:eq(7)").text().replace(".", ""));
			var kebutuhan_lk = parseFloat($("tbody#rl2_list tr:eq(" + i + ") td:eq(8)").text().replace(".", ""));
			var kebutuhan_pl = parseFloat($("tbody#rl2_list tr:eq(" + i + ") td:eq(9)").text().replace(".", ""));
			var kekurangan_lk = parseFloat($("tbody#rl2_list tr:eq(" + i + ") td:eq(10)").text().replace(".", ""));
			var kekurangan_pl = parseFloat($("tbody#rl2_list tr:eq(" + i + ") td:eq(11)").text().replace(".", ""));
			var jumlah_keadaan = parseFloat($("tbody#rl2_list tr:eq(" + i + ") td:eq(12)").text().replace(".", ""));
			var jumlah_kebutuhan = parseFloat($("tbody#rl2_list tr:eq(" + i + ") td:eq(13)").text().replace(".", ""));
			var jumlah_kekurangan = parseFloat($("tbody#rl2_list tr:eq(" + i + ") td:eq(14)").text().replace(".", ""));
			
			d_data[i] = {
				'kode_rs': kode_rs,
				'kode_prov': kode_prov,
				'kab_kota': kab_kota,
				'nama_rs': nama_rs,
				'nomor': nomor,
				'kualifikasi_pendidikan': kualifikasi_pendidikan,
				'keadaan_lk': keadaan_lk,
				'keadaan_pl': keadaan_pl,
				'kebutuhan_lk': kebutuhan_lk,
				'kebutuhan_pl': kebutuhan_pl,
				'kekurangan_lk': kekurangan_lk,
				'kekurangan_pl': kekurangan_pl,
				'jumlah_keadaan': jumlah_keadaan,
				'jumlah_kebutuhan': jumlah_kebutuhan,
				'jumlah_kekurangan': jumlah_kekurangan
			};
		}
		var data = this.getRegulerData();
		data['action'] = "create_excel_rl2";
		data['d_data'] = JSON.stringify(d_data);
		data['num_rows'] = num_rows;
		download(data);
		dismissLoading();
    };
    rl2.view();
});