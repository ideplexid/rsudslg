var rl53;
$(document).ready(function() {
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
	var column = new Array();
	rl53 = new TableAction("rl53", "medical_record", "rl53", column);
    rl53.getRegulerData = function() {
    	var data = TableAction.prototype.getRegulerData.call(this);
        data['bulan'] = $("#rl53_bulan").val();
        data['label_bulan'] = $("#rl53_bulan option:selected").text();
        data['tahun'] = $("#rl53_tahun").val();
        return data;
    };
    rl53.excel = function() {
    	showLoading();
		var num_rows = $("#rl53_list").children("tr").length;
		var d_data = {};
		for (var i = 0; i < num_rows; i++) {
			var kode_prov = $("tbody#rl53_list tr:eq(" + i + ") td:eq(0)").text();
			var kab_kota = $("tbody#rl53_list tr:eq(" + i + ") td:eq(1)").text();
			var kode_rs = $("tbody#rl53_list tr:eq(" + i + ") td:eq(2)").text();
			var nama_rs = $("tbody#rl53_list tr:eq(" + i + ") td:eq(3)").text();
			var bulan = $("tbody#rl53_list tr:eq(" + i + ") td:eq(4)").text();
			var tahun = $("tbody#rl53_list tr:eq(" + i + ") td:eq(5)").text();
			var nomor = $("tbody#rl53_list tr:eq(" + i + ") td:eq(6)").text();
			var kode_icd = $("tbody#rl53_list tr:eq(" + i + ") td:eq(7)").text();
			var nama_icd = $("tbody#rl53_list tr:eq(" + i + ") td:eq(8)").text();
			var jml_keluar_hidup_lk = parseFloat($("tbody#rl53_list tr:eq(" + i + ") td:eq(9)").text().replace(".", ""));
			var jml_keluar_hidup_pr = parseFloat($("tbody#rl53_list tr:eq(" + i + ") td:eq(10)").text().replace(".", ""));
			var jml_keluar_mati_lk = parseFloat($("tbody#rl53_list tr:eq(" + i + ") td:eq(11)").text().replace(".", ""));
			var jml_keluar_mati_pr = parseFloat($("tbody#rl53_list tr:eq(" + i + ") td:eq(12)").text().replace(".", ""));
			var total = parseFloat($("tbody#rl53_list tr:eq(" + i + ") td:eq(13)").text().replace(".", ""));
			
			d_data[i] = {
				"kode_prov"				: kode_prov,
				"kab_kota"				: kab_kota,
				"kode_rs"				: kode_rs,
				"nama_rs"				: nama_rs,
				"bulan"					: bulan,
				"tahun"					: tahun,
				"nomor"					: nomor,
				"kode_icd"				: kode_icd,
				"nama_icd"				: nama_icd,
				"jml_keluar_hidup_lk"	: jml_keluar_hidup_lk,
				"jml_keluar_hidup_pr"	: jml_keluar_hidup_pr,
				"jml_keluar_mati_lk"	: jml_keluar_mati_lk,
				"jml_keluar_mati_pr"	: jml_keluar_mati_pr,
				"total"					: total
			};
		}
		var data = this.getRegulerData();
		data['action'] = "create_excel_rl53";
		data['d_data'] = JSON.stringify(d_data);
		data['num_rows'] = num_rows;
		download(data);
		dismissLoading();
    };
    rl53.view();
});