var assesment_syaraf;

var asy_pasien;
var asy_noreg=$("#asy_noreg_pasien").val();
var asy_nama_pasien=$("#asy_nama_pasien").val();
var asy_nrm_pasien=$("#asy_nrm_pasien").val();
var asy_polislug=$("#asy_polislug").val();
var asy_the_page=$("#asy_the_page").val();
var asy_the_protoslug=$("#asy_the_protoslug").val();
var asy_the_protoname=$("#asy_the_protoname").val();
var asy_the_protoimplement=$("#asy_the_protoimplement").val();
var asy_id_antrian=$("#asy_id_antrian").val();
var asy_action=$("#asy_action").val();
var diagnosa_asy;
var dokter_asy;
var terapi_asy;


$(document).ready(function() {
    $('.mydate').datepicker();
    $('.mydatetime').datetimepicker();
    asy_pasien=new TableAction("asy_pasien",asy_the_page,asy_action,new Array());
    asy_pasien.setSuperCommand("asy_pasien");
    asy_pasien.setPrototipe(asy_the_protoname,asy_the_protoslug,asy_the_protoimplement);
    asy_pasien.selected=function(json){
        var nama=json.nama_pasien;
        var nrm=json.nrm;
        var noreg=json.id;		
        $("#"+asy_action+"_nama_pasien").val(nama);
        $("#"+asy_action+"_nrm_pasien").val(nrm);
        $("#"+asy_action+"_noreg_pasien").val(noreg);
    };

    dokter_asy=new TableAction("dokter_asy",asy_the_page,asy_action,new Array());
    dokter_asy.setSuperCommand("dokter_asy");
    dokter_asy.setPrototipe(asy_the_protoname,asy_the_protoslug,asy_the_protoimplement);
    dokter_asy.selected=function(json){
        $("#"+asy_action+"_nama_dokter").val(json.nama);
        $("#"+asy_action+"_id_dokter").val(json.id);
    };

    stypeahead("#"+asy_action+"_dokter_asy",3,dokter_asy,"nama",function(item){
        $("#"+asy_action+"_nama_dokter").val(json.nama);
        $("#"+asy_action+"_id_dokter").val(json.id);			
    });
    
    
    diagnosa_asy=new TableAction("diagnosa_asy",asy_the_page,asy_action, new Array());
    diagnosa_asy.setSuperCommand("diagnosa_asy");
    diagnosa_asy.setPrototipe(asy_the_protoname,asy_the_protoslug,asy_the_protoimplement);
    diagnosa_asy.selected=function(json){
        $("#"+asy_action+"_diagnosa_kerja").val(json.nama);
        $("#"+asy_action+"_diagnosa_kerja_icd").val(json.icd);
        $("#"+asy_action+"_diagnosa_kerja_sebab").val(json.sebab);
        
    };
    stypeahead("#"+asy_action+"diagnosa_asy",3,diagnosa_asy,"nama",function(item){
        $("#"+asy_action+"_diagnosa_kerja").val(json.nama);
        $("#"+asy_action+"_diagnosa_kerja_icd").val(json.icd);
        $("#"+asy_action+"_diagnosa_kerja_sebab").val(json.sebab);		
    });
    
    terapi_asy=new TableAction("terapi_asy",asy_the_page,asy_action, new Array());
    terapi_asy.setSuperCommand("terapi_asy");
    terapi_asy.setPrototipe(asy_the_protoname,asy_the_protoslug,asy_the_protoimplement);
    terapi_asy.selected=function(json){
        $("#"+asy_action+"_terapi_tindakan").val(json.nama);
        $("#"+asy_action+"_terapi_tindakan_icd").val(json.icd);
        $("#"+asy_action+"_terapi_tindakan_sebab").val(json.sebab);
        
    };
    stypeahead("#"+asy_action+"terapi_asy",3,terapi_asy,"nama",function(item){
        $("#"+asy_action+"_terapi_tindakan").val(json.nama);
        $("#"+asy_action+"_terapi_tindakan_icd").val(json.icd);
        $("#"+asy_action+"_terapi_tindakan_sebab").val(json.sebab);		
    });
    

    var column=new Array(
            "id","waktu","ruangan", "waktu_masuk",
            "nama_pasien","noreg_pasien","nrm_pasien",
            "alamat","umur","jk","carabayar","tgl_lahir_pasien",
            "id_dokter","nama_dokter",
            "mata_anamnesis", "mata_ikaterus", "mata_reflek_pupil", "mata_ukuran_pupil", "mata_iso_unisocore", "mata_lain",
            "tht_tonsil", "tht_faring", "tht_lidah", "tht_bibir",
            "leher_jvp", "leher_pembesaran_kelenjar",
            "torak_simetris_asimetris",
            "cor_s1_s2", "cor_reguler", "cor_ireguler", "cor_mur", "cor_besar",
            "pulmo_suara_nafas", "pulmo_ronchi", "pulmo_wheezing",
            "abdomen_hepar", "abdomen_lien",
            "extremitas_hangat",
            "extremitas_dingin",
            "extremitas_edema",
            "kranium", "kranium_lain", "kranium_keterangan",
            "korpus_vertebra", "korpus_vertebra_lain", "korpus_vertebra_keterangan",
            "tanda_selaput_otak", "tanda_selaput_otak_lain", "tanda_selaput_otak_keterangan",
            "syaraf_otak_kanan", "syaraf_otak_kiri",
            "motorik", "motorik_kaki_kanan", "motorik_kaki_kiri",
            "refleks", "refleks_kaki_kanan", "refleks_kaki_kiri",
            "sensorik", "sensorik_kaki_kanan", "sensorik_kaki_kiri",
            "vegentatif",
            "luhur_kesadaran", "luhur_reaksi_emosi", "luhur_intelek", "luhur_proses_berpikir", "luhur_psikomotorik", "luhur_psikosensorik", "luhur_bicara_bahasa",
            "mental_refleks", "mental_memegang", "mental_menetek", "mental_snout_refleks", "mental_snout_refleks", "mental_glabola","mental_palmomental", "mental_korneomandibular", "mental_kaki_tolik", "mental_lain",
            "nyeri_tekan_syaraf",
            "tanda_laseque",
            "lain_lain",
            "diagnosa_kerja_icd", "diagnosa_kerja", "diagnosa_kerja_sebab", "diagnosa_kerja_ket",
            "terapi_tindakan_icd", "terapi_tindakan", "terapi_tindakan_sebab", "terapi_tindakan_ket",
            "penunjang_neurovaskuler", "penunjang_neuroimaging", "penunjang_elektrodiagnostik", "penunjang_lab", "penunjang_lain",
            "waktu_pulang",
            "tgl_kontrol_klinik",
            "dirawat_diruang",
            );
    assesment_syaraf=new TableAction(asy_action,asy_the_page,asy_action,column);
    assesment_syaraf.setPrototipe(asy_the_protoname,asy_the_protoslug,asy_the_protoimplement);
    assesment_syaraf.getRegulerData=function(){
        var reg_data={	
                page:this.page,
                action:this.action,
                super_command:this.super_command,
                prototype_name:this.prototype_name,
                prototype_slug:this.prototype_slug,
                prototype_implement:this.prototype_implement,
                polislug:asy_polislug,
                noreg_pasien:asy_noreg,
                nama_pasien:asy_nama_pasien,
                nrm_pasien:asy_nrm_pasien,
                id_antrian:asy_id_antrian
                };
        return reg_data;
    };

    assesment_syaraf.setNextEnter();
    assesment_syaraf.setEnableAutofocus(true);
    window[asy_action]=assesment_syaraf;
    if(asy_action=="medical_record"){
        assesment_syaraf.view();
    }else{
        assesment_syaraf.clear=function(){return;};
    }
});