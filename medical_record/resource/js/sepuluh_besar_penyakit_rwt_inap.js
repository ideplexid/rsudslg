var sepuluh_besar_penyakit_rwt_inap;

$(document).ready(function(){
	$('.mydate').datepicker();
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
	sepuluh_besar_penyakit_rwt_inap=new TableAction("sepuluh_besar_penyakit_rwt_inap", "medical_record", "sepuluh_besar_penyakit_rwt_inap",new Array());
	sepuluh_besar_penyakit_rwt_inap.getRegulerData=function(){
		var reg_data={	
				page:this.page,
				action:this.action,
				super_command:this.super_command,
				prototype_name:this.prototype_name,
				prototype_slug:this.prototype_slug,
				prototype_implement:this.prototype_implement,
				dari:$("#"+this.prefix+"_dari").val(),
				sampai:$("#"+this.prefix+"_sampai").val()
				};
		return reg_data;
	};
	
});