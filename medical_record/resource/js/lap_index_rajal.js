var lap_index_rajal;
var lap_index_rajal_karyawan;
var lap_index_rajal_data;
var IS_lap_index_rajal_RUNNING;
$(document).ready(function(){
    $('.mydate').datepicker();
    lap_index_rajal=new TableAction("lap_index_rajal","medical_record","lap_index_rajal",new Array());
    lap_index_rajal.addRegulerData=function(data){
        data['dari']=$("#lap_index_rajal_dari").val();
        data['sampai']=$("#lap_index_rajal_sampai").val();
        data['ruang_ok']=$("#lap_index_rajal_ruang_ok").val();			
        $("#dari_table_lap_index_rajal").html(getFormattedDate(data['dari']));
        $("#sampai_table_lap_index_rajal").html(getFormattedDate(data['sampai']));			
        return data;
    };

    lap_index_rajal.batal=function(){
        IS_lap_index_rajal_RUNNING=false;
        $("#rekap_lap_index_rajal_modal").modal("hide");
    };
    
    lap_index_rajal.afterview=function(json){
        if(json!=null){
            $("#kode_table_lap_index_rajal").html(json.nomor);
            $("#waktu_table_lap_index_rajal").html(json.waktu);
            lap_index_rajal_data=json;
        }
    };

    lap_index_rajal.rekaptotal=function(){
        if(IS_lap_index_rajal_RUNNING) return;
        $("#rekap_lap_index_rajal_bar").sload("true","Fetching total data",0);
        $("#rekap_lap_index_rajal_modal").modal("show");
        IS_lap_index_rajal_RUNNING=true;
        var d=this.getRegulerData();
        d['super_command']="total";
        $.post("",d,function(res){
            var all=getContent(res);
            if(all!=null) {
                var total=Number(all);
                lap_index_rajal.rekaploop(0,total);
            } else {
                $("#rekap_lap_index_rajal_modal").modal("hide");
                IS_lap_index_rajal_RUNNING=false;
            }
        });
    };

    lap_index_rajal.excel=function(){
        var d=this.getRegulerData();
        d['command']="excel";
        download(d);
    };

    lap_index_rajal.rekaploop=function(current,total){
        if(current>=total || !IS_lap_index_rajal_RUNNING) {
            $("#rekap_lap_index_rajal_modal").modal("hide");
            IS_lap_index_rajal_RUNNING=false;
            lap_index_rajal.view();
            return;
        }
        var d=this.getRegulerData();
        d['super_command']="limit";
        d['halaman']=current;
        $.post("",d,function(res){
            var ct=getContent(res);
            var u=ct[0]['nama_pasien']+"  "+ct[0]['nrm'];
            $("#rekap_lap_index_rajal_bar").sload("true"," Processing - "+u+"... [ "+(current+1)+" / "+total+" ] ",(current*100/total));
            setTimeout(function(){lap_index_rajal.rekaploop(++current,total)},300);
        });
    };
            
});