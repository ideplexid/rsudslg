var asesment_psikologi;

var apg_pasien;
var dokter_apg;
var apg_noreg=$("#apg_noreg_pasien").val();
var apg_nama_pasien=$("#apg_nama_pasien").val();
var apg_nrm_pasien=$("#apg_nrm_pasien").val();
var apg_polislug=$("#apg_polislug").val();
var apg_the_page=$("#apg_the_page").val();
var apg_the_protoslug=$("#apg_the_protoslug").val();
var apg_the_protoname=$("#apg_the_protoname").val();
var apg_the_protoimplement=$("#apg_the_protoimplement").val();
var apg_id_antrian=$("#apg_id_antrian").val();
var apg_action=$("#apg_action").val();

$(document).ready(function() {
    $('.mydate').datepicker();
    $('.mydatetime').datetimepicker();
    apg_pasien=new TableAction("apg_pasien",apg_the_page,apg_action,new Array());
    apg_pasien.setSuperCommand("apg_pasien");
    apg_pasien.setPrototipe(apg_the_protoname,apg_the_protoslug,apg_the_protoimplement);
    apg_pasien.selected=function(json){
        var nama=json.nama_pasien;
        var nrm=json.nrm;
        var noreg=json.id;		
        $("#"+apg_action+"_nama_pasien").val(nama);
        $("#"+apg_action+"_nrm_pasien").val(nrm);
        $("#"+apg_action+"_noreg_pasien").val(noreg);
    };
    
    dokter_apg=new TableAction("dokter_apg",apg_the_page,apg_action,new Array());
    dokter_apg.setSuperCommand("dokter_apg");
    dokter_apg.setPrototipe(apg_the_protoname,apg_the_protoslug,apg_the_protoimplement);
    dokter_apg.selected=function(json){
        $("#"+apg_action+"_nama_dokter").val(json.nama);
        $("#"+apg_action+"_id_dokter").val(json.id);
    };

    stypeahead("#"+apg_action+"_dokter_apg",3,dokter_apg,"nama",function(item){
        $("#"+apg_action+"_nama_dokter").val(json.nama);
        $("#"+apg_action+"_id_dokter").val(json.id);			
    });
    
    var column=new Array(
        "id","waktu","ruangan", "waktu_masuk",
        "nama_pasien","noreg_pasien","nrm_pasien",
        "alamat","umur","jk","carabayar","tgl_lahir_pasien",
        "id_dokter","nama_dokter",
        "anak_ke", "dari_saudara", "status_nikah", "pend_terakhir", 
        "obeservasi","wawancara", "wawancara_ket","keluhan_utama", "mulai_terjadi", "riwayat_penyakit", "keluhan_psikosomatis", 
        "penyebab", "faktor_keluarga", "faktor_pekerjaan", "faktor_premorboid",
        "riwayat_napza","napza_lama_pemakaian","napza_jenis_zat", "napza_cara_pakai", "napza_latarbelakang_pakai", 
        "kesan_umum", "kesadaran","mood", "proses_dan_isi_pikiran", "kecerdasan", "dorongan", "judgment", "attention", 
        "psikomotorik", "orientasi_diri", "orientasi_wkt","orientasi_tempat","orientasi_ruang",
        "bb", "tb", "lila", "lk","status_gizi", 
        "tes_iq", "tes_bakat", "tes_kepribadian", "tes_okupasi", "tes_perkembangan", 
        "perkembangan_fisik", "perkembangan_kognitif", "perkembangan_emosi","perkembangan_sosial","perkembangan_motorik",
        "report", "ekspresi", "reaksi", "komunikasi","lain_lain", 
        "waktu_pulang", "waktu_kontrol_klinik", "dirawat_diruang", 
    );
    
    assesment_psikologi=new TableAction(apg_action,apg_the_page,apg_action,column);
    assesment_psikologi.setPrototipe(apg_the_protoname,apg_the_protoslug,apg_the_protoimplement);
    assesment_psikologi.getRegulerData=function(){
        var reg_data={	
                page:this.page,
                action:this.action,
                super_command:this.super_command,
                prototype_name:this.prototype_name,
                prototype_slug:this.prototype_slug,
                prototype_implement:this.prototype_implement,
                polislug:apg_polislug,
                noreg_pasien:apg_noreg,
                nama_pasien:apg_nama_pasien,
                nrm_pasien:apg_nrm_pasien,
                id_antrian:apg_id_antrian
                };
        return reg_data;
    };
    
    assesment_psikologi.setNextEnter();
    assesment_psikologi.setEnableAutofocus(true);
    window[apg_action]=assesment_psikologi;
    if(apg_action=="medical_record"){
        assesment_psikologi.view();
    }else{
        assesment_psikologi.clear=function(){return;};
    }
});