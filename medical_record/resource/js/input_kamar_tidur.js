var kamar_tidur;
$(document).ready(function() {
	var columns = new Array("id", "tahun", "nama_ruangan", "jenis_layanan", "jumlah_tt", "jumlah_vvip", "jumlah_vip", "jumlah_kelas_i", "jumlah_kelas_ii", "jumlah_kelas_iii", "jumlah_kelas_khusus");
	kamar_tidur = new TableAction(
		"kamar_tidur",
		"medical_record",
		"input_kamar_tidur",
		columns
	);
	kamar_tidur.show_add_form = function() {
		$("#kamar_tidur_tahun").removeAttr("disabled");
		$("#kamar_tidur_nama_ruangan").removeAttr("disabled");
		$("#kamar_tidur_jenis_layanan").removeAttr("disabled");
		TableAction.prototype.show_add_form.call(this);
	};
	kamar_tidur.edit = function(id) {
		$("#kamar_tidur_tahun").removeAttr("disabled");
		$("#kamar_tidur_tahun").attr("disabled", "disabled");
		$("#kamar_tidur_nama_ruangan").removeAttr("disabled");
		$("#kamar_tidur_nama_ruangan").attr("disabled", "disabled");
		$("#kamar_tidur_jenis_layanan").removeAttr("disabled");
		$("#kamar_tidur_jenis_layanan").attr("disabled", "disabled");
		TableAction.prototype.edit.call(this, id);
	};
	kamar_tidur.save = function() {
		$(".error_field").removeClass("error_field");
		var self = this;
		if ($("#kamar_tidur_id").val() == "") {
			var data = this.getRegulerData();
			data['tahun'] = $("#kamar_tidur_tahun").val();
			data['jenis_layanan'] = $("#kamar_tidur_jenis_layanan").val();
			if (data['jenis_layanan'] == "") {
				$("#kamar_tidur_jenis_layanan").addClass("error_field");
				$("#modal_alert_kamar_tidur_add_form").html(
					"<div class='alert alert-block alert-danger'>" +
						"<h4>Peringatan</h4>" +
						"</br><strong>Jenis Layanan</strong> tidak boleh kosong." +
					"</div>"
				);
				return;
			}
			data['nama_ruangan'] = $("#kamar_tidur_nama_ruangan").val();
			if (data['nama_ruangan'] == "") {
				$("#kamar_tidur_nama_ruangan").addClass("error_field");
				$("#modal_alert_kamar_tidur_add_form").html(
					"<div class='alert alert-block alert-danger'>" +
						"<h4>Peringatan</h4>" +
						"</br><strong>Nama Ruangan</strong> tidak boleh kosong." +
					"</div>"
				);
				return;
			}
			data['command'] = "check_data";
			$.post(
				"",
				data,
				function(response) {
					var json = JSON.parse(response);
					if (json == null) return;
					if (json == 0) {
						$("#modal_alert_kamar_tidur_add_form").html("");
						TableAction.prototype.save.call(self);
					}
					else {
						$("#kamar_tidur_tahun").addClass("error_field");
						$("#kamar_tidur_nama_ruangan").addClass("error_field");
						$("#kamar_tidur_jenis_layanan").addClass("error_field");
						$("#modal_alert_kamar_tidur_add_form").html(
							"<div class='alert alert-block alert-danger'>" +
								"<h4>Peringatan</h4>" +
								"</br><strong>Nama Ruangan</strong>, <strong>Jenis Layanan</strong>, dan <strong>Tahun</strong> sudah tersimpan pada sistem." +
							"</div>"
						);
					}
				}
			);	
		} else
			TableAction.prototype.save.call(this);
	};
	kamar_tidur.view();
	$("#kamar_tidur_jumlah_vvip, #kamar_tidur_jumlah_vip, #kamar_tidur_jumlah_kelas_i, #kamar_tidur_jumlah_kelas_ii, #kamar_tidur_jumlah_kelas_iii, #kamar_tidur_jumlah_kelas_khusus").on("keyup", function() {
		var value = $(this).val();

		var jumlah_vvip = 0;
		var jumlah_vip = 0;
		var jumlah_kelas_i = 0;
		var jumlah_kelas_ii = 0;
		var jumlah_kelas_iii = 0;
		var jumlah_kelas_khusus = 0;

		if ($("#kamar_tidur_jumlah_vvip").val().length > 0 && $.isNumeric($("#kamar_tidur_jumlah_vvip").val()))
			jumlah_vvip = parseFloat($("#kamar_tidur_jumlah_vvip").val());
		if ($("#kamar_tidur_jumlah_vip").val().length > 0 && $.isNumeric($("#kamar_tidur_jumlah_vip").val()))
			jumlah_vip = parseFloat($("#kamar_tidur_jumlah_vip").val());
		if ($("#kamar_tidur_jumlah_kelas_i").val().length > 0 && $.isNumeric($("#kamar_tidur_jumlah_kelas_i").val()))
			jumlah_kelas_i = parseFloat($("#kamar_tidur_jumlah_kelas_i").val());
		if ($("#kamar_tidur_jumlah_kelas_ii").val().length > 0 && $.isNumeric($("#kamar_tidur_jumlah_kelas_ii").val()))
			jumlah_kelas_ii = parseFloat($("#kamar_tidur_jumlah_kelas_ii").val());
		if ($("#kamar_tidur_jumlah_kelas_iii").val().length > 0 && $.isNumeric($("#kamar_tidur_jumlah_kelas_iii").val()))
			jumlah_kelas_iii = parseFloat($("#kamar_tidur_jumlah_kelas_iii").val());
		if ($("#kamar_tidur_jumlah_kelas_khusus").val().length > 0 && $.isNumeric($("#kamar_tidur_jumlah_kelas_khusus").val()))
			jumlah_kelas_khusus = parseFloat($("#kamar_tidur_jumlah_kelas_khusus").val());
		var total = jumlah_vvip + jumlah_vip + jumlah_kelas_i + jumlah_kelas_ii + jumlah_kelas_iii + jumlah_kelas_khusus;
		$("#kamar_tidur_jumlah_tt").val(total);
	});
});