var rl315;
$(document).ready(function() {
	$("#rl315_list").html("<tr><td colspan='13'><center>DATA BELUM DITAMPILKAN</center></td></tr>");
	rl315 = new TableAction(
		"rl315",
		"medical_record",
		"rl315",
		new Array()
	);
	rl315.view = function() {
		var data = this.getRegulerData();
		data['command'] = "view";
		data['tahun'] = $("#rl315_tahun_filter").val();
		data['bulan'] = $("#rl315_bulan_filter").val();
		showLoading();
		$.post(
			"",
			data,
			function(response) {
				var json = JSON.parse(response);
				if (json == null) {
					dismissLoading();
					return;
				}
				$("#rl315_list").html(json.html);
				dismissLoading();
			}
		);
	};
	rl315.excel = function() {
		if ($("#rl315_tahun").length) {
			showLoading();
			var num_rows = $("#rl315_list tr.data_rl315").length;
			var detail = {};
			for (var i = 0; i < num_rows; i++) {
				detail[i] = {
					"kode_propinsi" 			: $("#rl315_list tr.data_rl315:eq(" + i + ") td#rl315_kode_propinsi").text(),
					"kabupaten_kota" 			: $("#rl315_list tr.data_rl315:eq(" + i + ") td#rl315_kabupaten_kota").text(),
					"kode_rs" 					: $("#rl315_list tr.data_rl315:eq(" + i + ") td#rl315_kode_rs").text(),
					"nama_rs" 					: $("#rl315_list tr.data_rl315:eq(" + i + ") td#rl315_nama_rs").text(),
					"tahun" 					: $("#rl315_list tr.data_rl315:eq(" + i + ") td#rl315_tahun").text(),
					"nomor" 					: $("#rl315_list tr.data_rl315:eq(" + i + ") td#rl315_nomor").text(),
					"cara_pembayaran" 			: $("#rl315_list tr.data_rl315:eq(" + i + ") td#rl315_cara_pembayaran").text(),
					"pri_jumlah_pasien_keluar" 	: $("#rl315_list tr.data_rl315:eq(" + i + ") td#rl315_pri_jumlah_pasien_keluar").text(),
					"pri_jumlah_lama_dirawat" 	: $("#rl315_list tr.data_rl315:eq(" + i + ") td#rl315_pri_jumlah_lama_dirawat").text(),
					"jumlah_pasien_rawat_jalan" : $("#rl315_list tr.data_rl315:eq(" + i + ") td#rl315_jumlah_pasien_rawat_jalan").text(),
					"jprj_laboratorium" 		: $("#rl315_list tr.data_rl315:eq(" + i + ") td#rl315_jprj_laboratorium").text(),
					"jprj_radiologi" 			: $("#rl315_list tr.data_rl315:eq(" + i + ") td#rl315_jprj_radiologi").text(),
					"jprj_lain" 				: $("#rl315_list tr.data_rl315:eq(" + i + ") td#rl315_jprj_lain").text()
				};
			}
			var data = this.getRegulerData();
			data['command'] = "export_xls";
			data['tahun'] = $("#rl315_tahun_filter").val();
			data['detail'] = JSON.stringify(detail);
			postForm(data);
			dismissLoading();
		}
	};
});