/**
 * this source code used for handling
 * sentinel report
 * 
 * @author 		: Nurul Huda
 * @license 	: Apache v3
 * @version 	: 1.0.0
 * @since 		: 24 Sept 2016
 * @copyright 	: goblooge@gmail.com
 * @used		: medical_record/resource/php/indikator_klinis/sentinel.php
 * 
 * */
var sentinel;
$(document).ready(function(){
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
	$('.mydate').datepicker();
	var column=new Array('ruangan');
	sentinel=new ReportAction("sentinel","medical_record","sentinel",column);
	sentinel.setXKey('Date');
	sentinel.setYKey(['ya','tidak']);
	sentinel.setLabels(["Ya","Tidak"]);
	sentinel.setDiagramHolder("chart_sentinel");
	sentinel.initDiagram("");
	sentinel.setTitle("Diagram sentinel");
});