var rl37;
$(document).ready(function() {
	$("#rl37_list").html("<tr><td colspan='8'><center>DATA BELUM DITAMPILKAN</center></td></tr>");
	rl37 = new TableAction(
		"rl37",
		"medical_record",
		"rl37",
		new Array()
	);
	rl37.view = function() {
		var data = this.getRegulerData();
		data['command'] = "view";
		data['tahun'] = $("#rl37_tahun_filter").val();
		data['bulan'] = $("#rl37_bulan_filter").val();
		showLoading();
		$.post(
			"",
			data,
			function(response) {
				var json = JSON.parse(response);
				if (json == null) {
					dismissLoading();
					return;
				}
				$("#rl37_list").html(json.html);
				dismissLoading();
			}
		);
	};
	rl37.excel = function() {
		if ($("#rl37_tahun").length) {
			showLoading();
			var num_dradiodiagnostik_rows = $("#rl37_list tr.rl37_radiodiagnostik").length;
			var detail_radiodiagnostik = {};
			for (var i = 0; i < num_dradiodiagnostik_rows; i++) {
				detail_radiodiagnostik[i] = {
					'kode_propinsi'	 		: $("#rl37_list tr.rl37_radiodiagnostik:eq(" + i + ") td#rl37_kode_propinsi").text(),
					'kabupaten_kota' 		: $("#rl37_list tr.rl37_radiodiagnostik:eq(" + i + ") td#rl37_kabupaten_kota").text(),
					'kode_rs'		 		: $("#rl37_list tr.rl37_radiodiagnostik:eq(" + i + ") td#rl37_kode_rs").text(),
					'nama_rs'		 		: $("#rl37_list tr.rl37_radiodiagnostik:eq(" + i + ") td#rl37_nama_rs").text(),
					'tahun' 		 		: $("#rl37_list tr.rl37_radiodiagnostik:eq(" + i + ") td#rl37_tahun").text(),
					'nomor' 		 		: $("#rl37_list tr.rl37_radiodiagnostik:eq(" + i + ") td#rl37_nomor").text(),
					'jenis_kegiatan'		: $("#rl37_list tr.rl37_radiodiagnostik:eq(" + i + ") td#rl37_jenis_kegiatan").text(),
					'jumlah'				: $("#rl37_list tr.rl37_radiodiagnostik:eq(" + i + ") td#rl37_jumlah").text()
				};
			}
			var num_dradiotherapi_rows = $("#rl37_list tr.rl37_radiotherapi").length;
			var detail_radiotherapi = {};
			for (var i = 0; i < num_dradiotherapi_rows; i++) {
				detail_radiotherapi[i] = {
					'kode_propinsi'	 		: $("#rl37_list tr.rl37_radiotherapi:eq(" + i + ") td#rl37_kode_propinsi").text(),
					'kabupaten_kota' 		: $("#rl37_list tr.rl37_radiotherapi:eq(" + i + ") td#rl37_kabupaten_kota").text(),
					'kode_rs'		 		: $("#rl37_list tr.rl37_radiotherapi:eq(" + i + ") td#rl37_kode_rs").text(),
					'nama_rs'		 		: $("#rl37_list tr.rl37_radiotherapi:eq(" + i + ") td#rl37_nama_rs").text(),
					'tahun' 		 		: $("#rl37_list tr.rl37_radiotherapi:eq(" + i + ") td#rl37_tahun").text(),
					'nomor' 		 		: $("#rl37_list tr.rl37_radiotherapi:eq(" + i + ") td#rl37_nomor").text(),
					'jenis_kegiatan'		: $("#rl37_list tr.rl37_radiotherapi:eq(" + i + ") td#rl37_jenis_kegiatan").text(),
					'jumlah'				: $("#rl37_list tr.rl37_radiotherapi:eq(" + i + ") td#rl37_jumlah").text()
				};
			}
			var num_dkedokteran_nuklir_rows = $("#rl37_list tr.rl37_kedokteran_nuklir").length;
			var detail_kedokteran_nuklir = {};
			for (var i = 0; i < num_dkedokteran_nuklir_rows; i++) {
				detail_kedokteran_nuklir[i] = {
					'kode_propinsi'	 		: $("#rl37_list tr.rl37_kedokteran_nuklir:eq(" + i + ") td#rl37_kode_propinsi").text(),
					'kabupaten_kota' 		: $("#rl37_list tr.rl37_kedokteran_nuklir:eq(" + i + ") td#rl37_kabupaten_kota").text(),
					'kode_rs'		 		: $("#rl37_list tr.rl37_kedokteran_nuklir:eq(" + i + ") td#rl37_kode_rs").text(),
					'nama_rs'		 		: $("#rl37_list tr.rl37_kedokteran_nuklir:eq(" + i + ") td#rl37_nama_rs").text(),
					'tahun' 		 		: $("#rl37_list tr.rl37_kedokteran_nuklir:eq(" + i + ") td#rl37_tahun").text(),
					'nomor' 		 		: $("#rl37_list tr.rl37_kedokteran_nuklir:eq(" + i + ") td#rl37_nomor").text(),
					'jenis_kegiatan'		: $("#rl37_list tr.rl37_kedokteran_nuklir:eq(" + i + ") td#rl37_jenis_kegiatan").text(),
					'jumlah'				: $("#rl37_list tr.rl37_kedokteran_nuklir:eq(" + i + ") td#rl37_jumlah").text()
				};
			}
			var num_dimaging_rows = $("#rl37_list tr.rl37_imaging").length;
			var detail_imaging = {};
			for (var i = 0; i < num_dimaging_rows; i++) {
				detail_imaging[i] = {
					'kode_propinsi'	 		: $("#rl37_list tr.rl37_imaging:eq(" + i + ") td#rl37_kode_propinsi").text(),
					'kabupaten_kota' 		: $("#rl37_list tr.rl37_imaging:eq(" + i + ") td#rl37_kabupaten_kota").text(),
					'kode_rs'		 		: $("#rl37_list tr.rl37_imaging:eq(" + i + ") td#rl37_kode_rs").text(),
					'nama_rs'		 		: $("#rl37_list tr.rl37_imaging:eq(" + i + ") td#rl37_nama_rs").text(),
					'tahun' 		 		: $("#rl37_list tr.rl37_imaging:eq(" + i + ") td#rl37_tahun").text(),
					'nomor' 		 		: $("#rl37_list tr.rl37_imaging:eq(" + i + ") td#rl37_nomor").text(),
					'jenis_kegiatan'		: $("#rl37_list tr.rl37_imaging:eq(" + i + ") td#rl37_jenis_kegiatan").text(),
					'jumlah'				: $("#rl37_list tr.rl37_imaging:eq(" + i + ") td#rl37_jumlah").text()
				};
			}
			var data = this.getRegulerData();
			data['command'] = "export_xls";
			data['detail_radiodiagnostik'] = JSON.stringify(detail_radiodiagnostik);
			data['detail_radiotherapi'] = JSON.stringify(detail_radiotherapi);
			data['detail_kedokteran_nuklir'] = JSON.stringify(detail_kedokteran_nuklir);
			data['detail_imaging'] = JSON.stringify(detail_imaging);
			postForm(data);
			dismissLoading();
		}
	};
});