/**
 * this source code used for handling
 * transfusion detail report
 * 
 * @author 		: Nurul Huda
 * @license 	: Apache v3
 * @version 	: 1.0.0
 * @since 		: 24 Sept 2016
 * @copyright 	: goblooge@gmail.com
 * @used		: medical_record/resource/php/indikator_klinis/tranfusi.php
 * 
 * */
var transfusi;
$(document).ready(function(){
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
	$('.mydate').datepicker();
	var column=new Array('ruangan');
	transfusi=new ReportAction("transfusi","medical_record","transfusi",column);
	transfusi.setDiagramHolder("transfusi_chart");
	transfusi.setXKey('Date');
	transfusi.setYKey(['ya','tidak']);
	transfusi.setLabels(["Ya","Tidak"]);
	transfusi.sourcePieAdapter=function(json){
		var ya=0;
		var tidak=0;
		$(json).each(function(idx, obj){
			ya+=obj.ya;
			tidak+=obj.tidak;
		});
		var so=[{"Name":"Ya",'Total':ya},{"Name":"Tidak","Total":tidak}];
		return so;
	};
	transfusi.initDiagram("");
	transfusi.setTitle("Diagram Tranfusi Darah");
});