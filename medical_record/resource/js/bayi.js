/**
 * this source code used for handling
 * babies report
 * 
 * @author 		: Nurul Huda
 * @license 	: Apache v3
 * @version 	: 1.0.0
 * @since 		: 24 Sept 2016
 * @copyright 	: goblooge@gmail.com
 * @used		: medical_record/resource/php/indikator_klinis/bayi.php
 * 
 * */
 
var bayi;
$(document).ready(function(){
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
	$('.mydate').datepicker();
	var column=new Array('ruangan');
	bayi=new ReportAction("bayi","medical_record","bayi",column);
	bayi.setDiagramHolder("bayi_chart");
	bayi.setXKey('Date');
	bayi.setYKey(['ya','tidak']);
	bayi.setLabels(["Ya","Tidak"]);
	
	bayi.initDiagram("");
	bayi.setTitle("Bayi Lahir BB < 2000 Gram");
});