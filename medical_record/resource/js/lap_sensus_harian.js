var lap_sensus_harian;
var lap_sensus_harian_data;
var is_lap_sensus_harian_running;

$(document).ready(function() {
    $('.mydate').datepicker();
    lap_sensus_harian = new TableAction("lap_sensus_harian", "medical_record", "lap_sensus_harian", new Array());
    lap_sensus_harian.addRegulerData = function(data) {
        data['dari']=$("#lap_sensus_harian_dari").val().concat(" 00:00");
        
        //find data['sampai'] from data['dari'] + 1 day
        var dari = data['dari'];
        dari = new Date(dari);
        var sampai = dari.setDate(dari.getDate()+1);
        sampai = new Date(sampai);
        var month = String(sampai.getMonth() + 1);
        if(month.length < 2) month = '0' + month;
        var day = String(sampai.getDate());
        if(day.length < 2) day = '0' + day;
        var year = String(sampai.getFullYear());
        var tgl_sampai = `${year}-${month}-${day}`;
        tgl_sampai = tgl_sampai.concat(" 00:00");
		data['sampai']= tgl_sampai;
        
        data['pelayanan']   = $("#lap_sensus_harian_pelayanan").val();
        data['ruangan'] = $("#lap_sensus_harian_ruangan").val();
        $("#dari_table_lap_sensus").html(getFormattedDate(data['dari']));
        return data;
    };
    
    lap_sensus_harian.batal = function() {
        is_lap_sensus_harian_running = false;
        $('#rekap_lap_sensus_harian_modal').modal("hide");
    };
    
    lap_sensus_harian.afterview = function(json) {
        if(json!=null){
			$("#kode_table_lap_sensus_harian").html(json.nomor);
			$("#waktu_table_lap_sensus_harian").html(json.waktu);
			lap_sensus_harian_data = json;
		}
    };
    
    lap_sensus_harian.rekaptotal = function() {
        if(is_lap_sensus_harian_running) return;
        $("#rekap_lap_sensus_harian_bar").sload("true","Fetching total data",0);
        $("#rekap_lap_sensus_harian_modal").modal("show");
        is_lap_sensus_harian_running = true;
        var d = this.getRegulerData();
        d['action']="get_sensus_harian_total";
        $.post("",d,function(res){
			var all = getContent(res);
			if(all != null) {
				var total = Number(all);
				lap_sensus_harian.rekaploop(0,total);
			} else {
				$("#rekap_lap_sensus_harian_modal").modal("hide");
				is_lap_sensus_harian_running = false;
			}
		});
    };
    
    lap_sensus_harian.rekaploop = function(current,total){
		if(current >= total || !is_lap_sensus_harian_running) {
			$("#rekap_lap_sensus_harian_modal").modal("hide");
			is_lap_sensus_harian_running = false;
			lap_sensus_harian.view();
			return;
		}
		var d = this.getRegulerData();
		d['action'] = "get_sensus_harian_detail";
		d['halaman'] = current;
		$.post("",d,function(res){
			var ct = getContent(res);
			var u = ct['nama_pasien']+"  "+ct['nrm_pasien'];
			$("#rekap_lap_sensus_harian_bar").sload("true"," Processing - "+u+"... [ "+(current+1)+" / "+total+" ] ",(current*100/total));
			setTimeout(function(){lap_sensus_harian.rekaploop(++current,total)},300);
		});
	};
    
    lap_sensus_harian.excel = function() {
		var d = this.getRegulerData();
		d['command'] = "excel";
		d['action'] = "get_sensus_harian_excel";
		download(d);
	};
    
    $("#lap_sensus_harian_pelayanan").change(function(){
        var data = lap_sensus_harian.getRegulerData();
        data['super_command'] = 'pelayanan';
        showLoading();
        $.post(
            "",
            data,
            function(response) {
                json = JSON.parse(response);
                console.log(json);
                select = document.getElementById('lap_sensus_harian_ruangan');
                $("#lap_sensus_harian_ruangan").empty();
                for (var i = 0; i<json.length; i++){
                    var opt = document.createElement('option');
                    opt.value = json[i]['value'];
                    opt.innerHTML = json[i]['name'];
                    select.appendChild(opt);
                }
                dismissLoading();
            }
        );
    });
    
});