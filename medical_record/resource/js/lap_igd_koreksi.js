$(document).ready(function(res){
	$("#lap_igd_koreksi_ds").on("change",function(x){
		if($(this). prop("checked") == true){
			$("#lap_igd_koreksi_kiriman").val("");
			$("#lap_igd_koreksi_rujuk").val("");
			$("#lap_igd_koreksi_kiriman").prop("disabled",true);
			$("#lap_igd_koreksi_rujuk").prop("disabled",true);
		}else{
			$("#lap_igd_koreksi_kiriman").prop("disabled",false);
			$("#lap_igd_koreksi_rujuk").prop("disabled",false);				
		}
	});
});