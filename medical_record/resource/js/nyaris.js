/**
 * this source code used for handling
 * nearly injury
 * 
 * @author 		: Nurul Huda
 * @license 	: Apache v3
 * @version 	: 1.0.0
 * @since 		: 24 Sept 2016
 * @copyright 	: goblooge@gmail.com
 * @used		: medical_record/resource/php/indikator_klinis/nyaris.php
 * 
 * */
 
var nyaris;
$(document).ready(function(){
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
	$('.mydate').datepicker();
	var column=new Array('ruangan');
	nyaris=new ReportAction("nyaris","medical_record","nyaris",column);
	nyaris.setXKey('Date');
	nyaris.setYKey(['ya','tidak']);
	nyaris.setLabels(["Ya","Tidak"]);
	nyaris.setDiagramHolder("chart_nyaris");
	nyaris.initDiagram("");
	nyaris.setTitle("Diagram Nyaris Cedera");
});