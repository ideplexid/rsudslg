var rl5_4;
$(document).ready(function() {
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
	var column = new Array();
	rl5_4 = new TableAction("rl5_4", "medical_record", "rl5_4", column);
    rl5_4.getRegulerData = function() {
    	var data = TableAction.prototype.getRegulerData.call(this);
        data['bulan'] = $("#rl5_4_bulan").val();
        data['label_bulan'] = $("#rl5_4_bulan option:selected").text();
        data['tahun'] = $("#rl5_4_tahun").val();
        return data;
    };
    rl5_4.excel = function() {
    	showLoading();
		var num_rows = $("#rl5_4_list").children("tr").length;
		var d_data = {};
		for (var i = 0; i < num_rows; i++) {
			var kode_prov = $("tbody#rl5_4_list tr:eq(" + i + ") td:eq(0)").text();
			var kab_kota = $("tbody#rl5_4_list tr:eq(" + i + ") td:eq(1)").text();
			var kode_rs = $("tbody#rl5_4_list tr:eq(" + i + ") td:eq(2)").text();
			var nama_rs = $("tbody#rl5_4_list tr:eq(" + i + ") td:eq(3)").text();
			var bulan = $("tbody#rl5_4_list tr:eq(" + i + ") td:eq(4)").text();
			var tahun = $("tbody#rl5_4_list tr:eq(" + i + ") td:eq(5)").text();
			var nomor = $("tbody#rl5_4_list tr:eq(" + i + ") td:eq(6)").text();
			var kode_icd = $("tbody#rl5_4_list tr:eq(" + i + ") td:eq(7)").text();
			var nama_icd = $("tbody#rl5_4_list tr:eq(" + i + ") td:eq(8)").text();
			var jml_kasus_baru_lk = parseFloat($("tbody#rl5_4_list tr:eq(" + i + ") td:eq(9)").text().replace(".", ""));
			var jml_kasus_baru_pr = parseFloat($("tbody#rl5_4_list tr:eq(" + i + ") td:eq(10)").text().replace(".", ""));
			var jml_kasus_baru = parseFloat($("tbody#rl5_4_list tr:eq(" + i + ") td:eq(11)").text().replace(".", ""));
			var jml_kunjungan = parseFloat($("tbody#rl5_4_list tr:eq(" + i + ") td:eq(12)").text().replace(".", ""));
			
			d_data[i] = {
				"kode_prov"			: kode_prov,
				"kab_kota"			: kab_kota,
				"kode_rs"			: kode_rs,
				"nama_rs"			: nama_rs,
				"bulan"				: bulan,
				"tahun"				: tahun,
				"nomor"				: nomor,
				"kode_icd"			: kode_icd,
				"nama_icd"			: nama_icd,
				"jml_kasus_baru_lk"	: jml_kasus_baru_lk,
				"jml_kasus_baru_pr"	: jml_kasus_baru_pr,
				"jml_kasus_baru"	: jml_kasus_baru,
				"jml_kunjungan"		: jml_kunjungan
			};
		}
		var data = this.getRegulerData();
		data['action'] = "create_excel_rl5_4";
		data['d_data'] = JSON.stringify(d_data);
		data['num_rows'] = num_rows;
		download(data);
		dismissLoading();
    };
    rl5_4.view();
});