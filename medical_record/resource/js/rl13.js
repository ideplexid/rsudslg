var rl13;
$(document).ready(function() {
	$("#rl13_list").html("<tr><td colspan='14'><center>DATA BELUM DITAMPILKAN</center></td></tr>");
	rl13 = new TableAction(
		"rl13",
		"medical_record",
		"rl13",
		new Array()
	);
	rl13.view = function() {
		var data = this.getRegulerData();
		data['command'] = "view";
		data['tahun'] = $("#rl13_tahun_filter").val();
		showLoading();
		$.post(
			"",
			data,
			function(response) {
				var json = JSON.parse(response);
				if (json == null) {
					dismissLoading();
					return;
				}
				$("#rl13_list").html(json.html);
				dismissLoading();
			}
		);
	};
	rl13.excel = function() {
		if ($("#rl13_tahun").length) {
			var data = this.getRegulerData();
			data['command'] = "export_xls";
			var num_rows = $("#rl13_list tr").length;
			var detail = {};
			for (var i = 0; i < num_rows; i++) {
				detail[i] = {
					'nomor' 		 		: $("#rl13_list tr:eq(" + i + ") td#rl13_nomor").text(),
					'kode_rs'		 		: $("#rl13_list tr:eq(" + i + ") td#rl13_kode_rs").text(),
					'nama_rs'		 		: $("#rl13_list tr:eq(" + i + ") td#rl13_nama_rs").text(),
					'kode_propinsi'	 		: $("#rl13_list tr:eq(" + i + ") td#rl13_kode_propinsi").text(),
					'kabupaten_kota' 		: $("#rl13_list tr:eq(" + i + ") td#rl13_kabupaten_kota").text(),
					'tahun' 		 		: $("#rl13_list tr:eq(" + i + ") td#rl13_tahun").text(),
					'jenis_layanan'	 		: $("#rl13_list tr:eq(" + i + ") td#rl13_jenis_layanan").text(),
					'jumlah_tt'		 		: $("#rl13_list tr:eq(" + i + ") td#rl13_jumlah_tt").text(),
					'jumlah_vvip'	 		: $("#rl13_list tr:eq(" + i + ") td#rl13_jumlah_vvip").text(),
					'jumlah_vip'	 		: $("#rl13_list tr:eq(" + i + ") td#rl13_jumlah_vip").text(),
					'jumlah_kelas_i' 		: $("#rl13_list tr:eq(" + i + ") td#rl13_jumlah_kelas_i").text(),
					'jumlah_kelas_ii' 		: $("#rl13_list tr:eq(" + i + ") td#rl13_jumlah_kelas_ii").text(),
					'jumlah_kelas_iii' 		: $("#rl13_list tr:eq(" + i + ") td#rl13_jumlah_kelas_iii").text(),
					'jumlah_kelas_khusus' 	: $("#rl13_list tr:eq(" + i + ") td#rl13_jumlah_kelas_khusus").text()
				};
			}
			data['detail'] = JSON.stringify(detail);
			showLoading();
			postForm(data);
			dismissLoading();
		}
	};
});