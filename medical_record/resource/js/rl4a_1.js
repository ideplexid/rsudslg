var rl4a_1;
var FINISHED;
$(document).ready(function() {
	$("#rl4a_1_list").html("<tr><td colspan='32'><center>DATA BELUM DITAMPILKAN</center></td></tr>");
	$("#loading_modal").on("show", function() {
		$("a.close").hide();
	});
	$("#loading_modal").on("hide", function() {
		$("a.close").show();
	});
	rl4a_1 = new TableAction(
		"rl4a_1",
		"medical_record",
		"rl4a_1",
		new Array()
	);
	rl4a_1.view = function() {
		if ($("#lpo_tanggal_from").val() == "" || $("#lpo_tanggal_to").val() == "")
			return;
		var self = this;
		$("#rl4a_1_info").empty();
		$("#rl4a_1_loading_bar").sload("true", "Harap ditunggu...", 0);
		$("#loading_modal").smodal("show");
		FINISHED = false;
		var data = this.getRegulerData();
		data['command'] = "get_jumlah";
		$.post(
			"",
			data,
			function(response) {
				var json = JSON.parse(response);
				if (json == null) return;
				$("#rl4a_1_list").empty();
				self.fillHtml(0, json.jumlah);
			}
		);
	};
	rl4a_1.fillHtml = function(num, limit) {
		if (FINISHED || num == limit) {
			if (FINISHED == false && num == limit) {
				this.finalize();
			} else {
				$("#loading_modal").smodal("hide");
				$("#rl4a_1_info").html(
					"<div class='alert alert-block alert-inverse'>" +
						 "<center><strong>PROSES DIBATALKAN</strong></center>" +
					 "</div>"
				);
				$("#rl4a_1_export_btn").removeAttr("onclick");
			}
			return;
		}
		var self = this;
		var data = this.getRegulerData();
		data['command'] = "get_data";
		data['tahun'] = $("#rl4a_1_tahun_filter").val();
		data['num'] = num;
		$.post(
			"",
			data,
			function(response) {
				var json = JSON.parse(response);
				if (json == null) return;
				$("tbody#rl4a_1_list").append(
					json.html
				);
				$("#rl4a_1_loading_bar").sload("true", json.dtd + " - " + json.kode_icd + " - " + json.sebab + " (" + (num + 1) + " / " + limit + ")", (num + 1) * 100 / limit - 1);
				self.fillHtml(num + 1, limit);
			}
		);
	};
	rl4a_1.finalize = function() {
		var num_rows = $("tbody#lpo_list tr").length;
		for (var i = 0; i < num_rows; i++)
			$("tbody#rl4a_1_list tr:eq(" + i + ") td#nomor").html("<small>" + (i + 1) + "</small>");
		$("#loading_modal").smodal("hide");
		$("#rl4a_1_info").html(
			"<div class='alert alert-block alert-info'>" +
				 "<center><strong>PROSES SELESAI</strong></center>" +
			 "</div>"
		);
		$("#rl4a_1_export_btn").removeAttr("onclick");
		$("#rl4a_1_export_btn").attr("onclick", "rl4a_1.excel()");
	};
	rl4a_1.cancel = function() {
		FINISHED = true;
	};
	rl4a_1.excel = function() {
		if ($("#rl4a_1_tahun").length) {
			var data = this.getRegulerData();
			data['command'] = "export_xls";
			var num_rows = $("#rl4a_1_list tr").length;
			var detail = {};
			for (var i = 0; i < num_rows; i++) {
				detail[i] = {
					'kode_propinsi'	 		: $("#rl4a_1_list tr:eq(" + i + ") td#rl4a_1_kode_propinsi").text(),
					'kabupaten_kota' 		: $("#rl4a_1_list tr:eq(" + i + ") td#rl4a_1_kabupaten_kota").text(),
					'kode_rs'		 		: $("#rl4a_1_list tr:eq(" + i + ") td#rl4a_1_kode_rs").text(),
					'nama_rs'		 		: $("#rl4a_1_list tr:eq(" + i + ") td#rl4a_1_nama_rs").text(),
					'tahun' 		 		: $("#rl4a_1_list tr:eq(" + i + ") td#rl4a_1_tahun").text(),
					'nomor' 		 		: $("#rl4a_1_list tr:eq(" + i + ") td#rl4a_1_nomor").text(),
					'dtd'	 				: $("#rl4a_1_list tr:eq(" + i + ") td#rl4a_1_dtd").text(),
					'kode_icd'		 		: $("#rl4a_1_list tr:eq(" + i + ") td#rl4a_1_kode_icd").text(),
					'sebab'	 				: $("#rl4a_1_list tr:eq(" + i + ") td#rl4a_1_sebab").text(),
					'jml_pasien_0hr_6hr_l'	: $("#rl4a_1_list tr:eq(" + i + ") td#rl4a_1_jml_pasien_0hr_6hr_l").text(),
					'jml_pasien_0hr_6hr_p'	: $("#rl4a_1_list tr:eq(" + i + ") td#rl4a_1_jml_pasien_0hr_6hr_p").text(),
					'jml_pasien_6hr_28hr_l'	: $("#rl4a_1_list tr:eq(" + i + ") td#rl4a_1_jml_pasien_6hr_28hr_l").text(),
					'jml_pasien_6hr_28hr_p'	: $("#rl4a_1_list tr:eq(" + i + ") td#rl4a_1_jml_pasien_6hr_28hr_p").text(),
					'jml_pasien_28hr_1th_l'	: $("#rl4a_1_list tr:eq(" + i + ") td#rl4a_1_jml_pasien_28hr_1th_l").text(),
					'jml_pasien_28hr_1th_p'	: $("#rl4a_1_list tr:eq(" + i + ") td#rl4a_1_jml_pasien_28hr_1th_p").text(),
					'jml_pasien_1th_4th_l'	: $("#rl4a_1_list tr:eq(" + i + ") td#rl4a_1_jml_pasien_1th_4th_l").text(),
					'jml_pasien_1th_4th_p'	: $("#rl4a_1_list tr:eq(" + i + ") td#rl4a_1_jml_pasien_1th_4th_p").text(),
					'jml_pasien_4th_14th_l'	: $("#rl4a_1_list tr:eq(" + i + ") td#rl4a_1_jml_pasien_4th_14th_l").text(),
					'jml_pasien_4th_14th_p'	: $("#rl4a_1_list tr:eq(" + i + ") td#rl4a_1_jml_pasien_4th_14th_p").text(),
					'jml_pasien_14th_24th_l': $("#rl4a_1_list tr:eq(" + i + ") td#rl4a_1_jml_pasien_14th_24th_l").text(),
					'jml_pasien_14th_24th_p': $("#rl4a_1_list tr:eq(" + i + ") td#rl4a_1_jml_pasien_14th_24th_p").text(),
					'jml_pasien_24th_44th_l': $("#rl4a_1_list tr:eq(" + i + ") td#rl4a_1_jml_pasien_24th_44th_l").text(),
					'jml_pasien_24th_44th_p': $("#rl4a_1_list tr:eq(" + i + ") td#rl4a_1_jml_pasien_24th_44th_p").text(),
					'jml_pasien_44th_64th_l': $("#rl4a_1_list tr:eq(" + i + ") td#rl4a_1_jml_pasien_44th_64th_l").text(),
					'jml_pasien_44th_64th_p': $("#rl4a_1_list tr:eq(" + i + ") td#rl4a_1_jml_pasien_44th_64th_p").text(),
					'jml_pasien_l64th_l'	: $("#rl4a_1_list tr:eq(" + i + ") td#rl4a_1_jml_pasien_l64th_l").text(),
					'jml_pasien_l64th_p'	: $("#rl4a_1_list tr:eq(" + i + ") td#rl4a_1_jml_pasien_l64th_p").text(),
					'jml_pasien_keluar_l'	: $("#rl4a_1_list tr:eq(" + i + ") td#rl4a_1_jml_pasien_keluar_l").text(),
					'jml_pasien_keluar_p'	: $("#rl4a_1_list tr:eq(" + i + ") td#rl4a_1_jml_pasien_keluar_p").text(),
					'jml_pasien_keluar'		: $("#rl4a_1_list tr:eq(" + i + ") td#rl4a_1_jml_pasien_keluar").text(),
					'jml_pasien_keluar_mati': $("#rl4a_1_list tr:eq(" + i + ") td#rl4a_1_jml_pasien_keluar_mati").text()
				};
			}
			data['detail'] = JSON.stringify(detail);
			showLoading();
			postForm(data);
			dismissLoading();
		}
	};
	$(document).keyup(function(e) {
		if (e.which == 27) {
			FINISHED = true;
		}
	});
});