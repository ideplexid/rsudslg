//$(document).ready(function() {
    $("#lap_persen_diagnosa_pelayanan").change(function(){
        var data = lap_persen_diagnosa.getRegulerData();
        data['super_command'] = 'pelayanan';
        showLoading();
        $.post(
            "",
            data,
            function(response) {
                json = JSON.parse(response);
                console.log(json);
                select = document.getElementById('lap_persen_diagnosa_ruangan');
                $("#lap_persen_diagnosa_ruangan").empty();
                for (var i = 0; i<json.length; i++){
                    var opt = document.createElement('option');
                    opt.value = json[i]['value'];
                    opt.innerHTML = json[i]['name'];
                    select.appendChild(opt);
                }
                dismissLoading();
            }
        );
    });
//});