var lap_kun_pertanggal;
var lap_kun_pertanggal_karyawan;
var lap_kun_pertanggal_data;
var IS_lap_kun_pertanggal_RUNNING;
$(document).ready(function(){
    $('.mydate').datepicker();
    lap_kun_pertanggal=new TableAction("lap_kun_pertanggal","medical_record","lap_kun_pertanggal",new Array());
    lap_kun_pertanggal.addRegulerData=function(data){
        data['dari']=$("#"+this.prefix+"_dari").val();
        data['sampai']=$("#"+this.prefix+"_sampai").val();
        data['carabayar']=$("#"+this.prefix+"_carabayar").val();
        data['origin']=$("#"+this.prefix+"_origin").val();
        return data;
    };

    lap_kun_pertanggal.batal=function(){
        smis_loader.cancel();
    };
    
    lap_kun_pertanggal.afterview=function(json){
        if(json!=null){
            $("#kode_table_lap_kun_pertanggal").html(json.nomor);
            $("#waktu_table_lap_kun_pertanggal").html(json.waktu);
            lap_kun_pertanggal_data=json;
        }
    };

    lap_kun_pertanggal.rekaptotal=function(){
        if(IS_lap_kun_pertanggal_RUNNING) return;
        smis_loader.onCancel=function(){
            IS_lap_kun_pertanggal_RUNNING=false;
        };
        
        smis_loader.showLoader();
        smis_loader.updateLoader('true',"Fetching total data",0);
        
        IS_lap_kun_pertanggal_RUNNING=true;
        var d=this.getRegulerData();
        d['super_command']="total";
        $.post("",d,function(res){
            var json=getContent(res);
            if(json.total>0) {
                lap_kun_pertanggal.rekaploop(0,json.total,json.data);
            } else {
                smis_loader.cancel();
            }
        });
    };

    lap_kun_pertanggal.rekaploop=function(current,total,data){
        if(current>=total || !IS_lap_kun_pertanggal_RUNNING) {
            smis_loader.cancel();
            IS_lap_kun_pertanggal_RUNNING=false;
            lap_kun_pertanggal.view();
            return;
        }
        smis_loader.updateLoader('true',"Processing "+getFormattedDate(data[current])+"... [ "+current+" / "+total+" ] ",(current*100/total));
        var d=this.getRegulerData();
        d['super_command']="limit";
        d['tanggal']=data[current];
        d['limit_start']=current;			
        $.post("",d,function(res){
            var ct=getContent(res);
            setTimeout(function(){lap_kun_pertanggal.rekaploop(++current,total,data)},100);
        });
    };
            
});