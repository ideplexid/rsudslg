var rl313;
$(document).ready(function() {
	$("#rl313_pengadaan_list").html("<tr><td colspan='5'><center>DATA BELUM DITAMPILKAN</center></td></tr>");
	$("#rl313_penjualan_list").html("<tr><td colspan='5'><center>DATA BELUM DITAMPILKAN</center></td></tr>");
	rl313 = new TableAction(
		"rl313",
		"medical_record",
		"rl313",
		new Array()
	);
	rl313.view = function() {
		var data = this.getRegulerData();
		data['command'] = "view";
		data['tahun'] = $("#rl313_tahun_filter").val();
		data['bulan'] = $("#rl313_bulan_filter").val();
		showLoading();
		$.post(
			"",
			data,
			function(response) {
				var json = JSON.parse(response);
				if (json == null) {
					dismissLoading();
					return;
				}
				$("#rl313_pengadaan_list").html(json.html_1);
				$("#rl313_penjualan_list").html(json.html_2);
				dismissLoading();
			}
		);
	};
	rl313.excel = function() {
		showLoading();
		var data = this.getRegulerData();
		data['command'] = "export_xls";
		data['tahun'] = $("#rl313_tahun_filter").val();
		data['bulan'] = $("#rl313_bulan_filter option:selected").text();
		var num_rows = $("#rl313_pengadaan_list tr.data_rl313_pengadaan").length;
		var detail = {};
		for (var i = 0; i < num_rows; i++) {
			detail[i] = {
				'nomor'							: $("#rl313_pengadaan_list tr.data_rl313_pengadaan:eq(" + i + ") td#nomor").text(),
				'golongan_obat'					: $("#rl313_pengadaan_list tr.data_rl313_pengadaan:eq(" + i + ") td#golongan_obat").text(),
				'jumlah_item_obat'				: $("#rl313_pengadaan_list tr.data_rl313_pengadaan:eq(" + i + ") td#jumlah_item_obat").text(),
				'jumlah_item_obat_tersedia'		: $("#rl313_pengadaan_list tr.data_rl313_pengadaan:eq(" + i + ") td#jumlah_item_obat_tersedia").text(),
				'jumlah_item_obat_formularium'	: $("#rl313_pengadaan_list tr.data_rl313_pengadaan:eq(" + i + ") td#jumlah_item_obat_formularium").text()
			};
		}
		data['detail_pengadaan'] = JSON.stringify(detail);

		num_rows = $("#rl313_penjualan_list tr.data_rl313_penjualan").length;
		detail = {};
		for (var i = 0; i < num_rows; i++) {
			detail[i] = {
				'nomor'			: $("#rl313_penjualan_list tr.data_rl313_penjualan:eq(" + i + ") td#nomor").text(),
				'golongan_obat'	: $("#rl313_penjualan_list tr.data_rl313_penjualan:eq(" + i + ") td#golongan_obat").text(),
				'rawat_jalan'	: $("#rl313_penjualan_list tr.data_rl313_penjualan:eq(" + i + ") td#rawat_jalan").text(),
				'igd'			: $("#rl313_penjualan_list tr.data_rl313_penjualan:eq(" + i + ") td#igd").text(),
				'rawat_inap'	: $("#rl313_penjualan_list tr.data_rl313_penjualan:eq(" + i + ") td#rawat_inap").text()
			};
		}
		data['detail_penjualan'] = JSON.stringify(detail);

		postForm(data);
		dismissLoading();
	};
});