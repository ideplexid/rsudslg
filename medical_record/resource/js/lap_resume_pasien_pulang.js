var lap_resume_pasien_pulang_ri;
var lap_resume_pasien_pulang_ri_karyawan;
var lap_resume_pasien_pulang_ri_data;
var IS_lap_resume_pasien_pulang_ri_RUNNING;
$(document).ready(function(){
    $('.mydate').datepicker();
    lap_resume_pasien_pulang_ri=new TableAction("lap_resume_pasien_pulang_ri","medical_record","lap_resume_pasien_pulang_ri",new Array());
    lap_resume_pasien_pulang_ri.addRegulerData=function(data){
        data['dari']=$("#lap_resume_pasien_pulang_ri_dari").val();
        data['sampai']=$("#lap_resume_pasien_pulang_ri_sampai").val();
        $("#dari_table_lap_resume_pasien_pulang_ri").html(getFormattedDate(data['dari']));
        $("#sampai_table_lap_resume_pasien_pulang_ri").html(getFormattedDate(data['sampai']));			
        return data;
    };

    lap_resume_pasien_pulang_ri.batal=function(){
        IS_lap_resume_pasien_pulang_ri_RUNNING=false;
        $("#rekap_lap_resume_pasien_pulang_ri_modal").modal("hide");
    };
    
    lap_resume_pasien_pulang_ri.afterview=function(json){
        if(json!=null){
            $("#kode_table_lap_resume_pasien_pulang_ri").html(json.nomor);
            $("#waktu_table_lap_resume_pasien_pulang_ri").html(json.waktu);
            lap_resume_pasien_pulang_ri_data=json;
        }
    };

    lap_resume_pasien_pulang_ri.rekaptotal=function(){
        if(IS_lap_resume_pasien_pulang_ri_RUNNING) return;
        $("#rekap_lap_resume_pasien_pulang_ri_bar").sload("true","Fetching total data",0);
        $("#rekap_lap_resume_pasien_pulang_ri_modal").modal("show");
        IS_lap_resume_pasien_pulang_ri_RUNNING=true;
        var d=this.getRegulerData();
        d['super_command']="total";
        $.post("",d,function(res){
            var all=getContent(res);
            if(all!=null) {
                var total=Number(all);
                lap_resume_pasien_pulang_ri.rekaploop(0,total);
            } else {
                $("#rekap_lap_resume_pasien_pulang_ri_modal").modal("hide");
                IS_lap_resume_pasien_pulang_ri_RUNNING=false;
            }
        });
    };

    lap_resume_pasien_pulang_ri.excel=function(){
        var d=this.getRegulerData();
        d['command']="excel";
        download(d);
    };

    lap_resume_pasien_pulang_ri.rekaploop=function(current,total){
        if(current>=total || !IS_lap_resume_pasien_pulang_ri_RUNNING) {
            $("#rekap_lap_resume_pasien_pulang_ri_modal").modal("hide");
            IS_lap_resume_pasien_pulang_ri_RUNNING=false;
            lap_resume_pasien_pulang_ri.view();
            return;
        }
        var d=this.getRegulerData();
        d['super_command']="limit";
        d['halaman']=current;
        $.post("",d,function(res){
            var ct=getContent(res);
            var u=ct[0]['nama_pasien']+"  "+ct[0]['nrm'];
            $("#rekap_lap_resume_pasien_pulang_ri_bar").sload("true"," Processing - "+u+"... [ "+(current+1)+" / "+total+" ] ",(current*100/total));
            setTimeout(function(){lap_resume_pasien_pulang_ri.rekaploop(++current,total)},300);
        });
    };
            
});