var rl39;
$(document).ready(function() {
	$("#rl39_list").html("<tr><td colspan='8'><center>DATA BELUM DITAMPILKAN</center></td></tr>");
	rl39 = new TableAction(
		"rl39",
		"medical_record",
		"rl39",
		new Array()
	);
	rl39.view = function() {
		var data = this.getRegulerData();
		data['command'] = "view";
		data['tahun'] = $("#rl39_tahun_filter").val();
		data['bulan'] = $("#rl39_bulan_filter").val();
		showLoading();
		$.post(
			"",
			data,
			function(response) {
				var json = JSON.parse(response);
				if (json == null) {
					dismissLoading();
					return;
				}
				$("#rl39_list").html(json.html);
				dismissLoading();
			}
		);
	};
	rl39.excel = function() {
		if ($("#rl39_tahun").length) {
			showLoading();
			var num_rows = $("#rl39_list tr.data_rl39").length;
			var detail = {};
			for (var i = 0; i < num_rows; i++) {
				detail[i] = {
					"kode_propinsi"	 	: $("#rl39_list tr.data_rl39:eq(" + i + ") td#rl39_kode_propinsi").text(),
					"kabupaten_kota" 	: $("#rl39_list tr.data_rl39:eq(" + i + ") td#rl39_kabupaten_kota").text(),
					"kode_rs"		 	: $("#rl39_list tr.data_rl39:eq(" + i + ") td#rl39_kode_rs").text(),
					"nama_rs"		 	: $("#rl39_list tr.data_rl39:eq(" + i + ") td#rl39_nama_rs").text(),
					"tahun" 		 	: $("#rl39_list tr.data_rl39:eq(" + i + ") td#rl39_tahun").text(),
					"nomor" 		 	: $("#rl39_list tr.data_rl39:eq(" + i + ") td#rl39_nomor").text(),
					"jenis_tindakan"	: $("#rl39_list tr.data_rl39:eq(" + i + ") td#rl39_jenis_tindakan").text(),
					"jumlah"			: $("#rl39_list tr.data_rl39:eq(" + i + ") td#rl39_jumlah").text()
				};
			}
			var data = this.getRegulerData();
			data['command'] = "export_xls";
			data['tahun'] = $("#rl39_tahun_filter").val();
			data['detail'] = JSON.stringify(detail);
			postForm(data);
			dismissLoading();
		}
	};
});