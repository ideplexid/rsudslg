var rl38;
$(document).ready(function() {
	$("#rl38_list").html("<tr><td colspan='9'><center>DATA BELUM DITAMPILKAN</center></td></tr>");
	rl38 = new TableAction(
		"rl38",
		"medical_record",
		"rl38",
		new Array()
	);
	rl38.view = function() {
		var data = this.getRegulerData();
		data['command'] = "view";
		data['tahun'] = $("#rl38_tahun_filter").val();
		data['bulan'] = $("#rl38_bulan_filter").val();
		showLoading();
		$.post(
			"",
			data,
			function(response) {
				var json = JSON.parse(response);
				if (json == null) {
					dismissLoading();
					return;
				}
				$("#rl38_list").html(json.html);
				dismissLoading();
			}
		);
	};
	rl38.excel = function() {
		if ($("#rl38_tahun").length) {
			showLoading();
			var num_rows = $("#rl38_list tr.data_rl38").length;
			var detail = {};
			for (var i = 0; i < num_rows; i++) {
				detail[i] = {
					'kode_propinsi'		: $("#rl38_list tr.data_rl38:eq(" + i + ") td#rl38_kode_propinsi").text(),
					'kabupaten_kota'	: $("#rl38_list tr.data_rl38:eq(" + i + ") td#rl38_kabupaten_kota").text(),
					'kode_rs'			: $("#rl38_list tr.data_rl38:eq(" + i + ") td#rl38_kode_rs").text(),
					'nama_rs'			: $("#rl38_list tr.data_rl38:eq(" + i + ") td#rl38_nama_rs").text(),
					'tahun' 			: $("#rl38_list tr.data_rl38:eq(" + i + ") td#rl38_tahun").text(),
					'nomor' 			: $("#rl38_list tr.data_rl38:eq(" + i + ") td#rl38_nomor").text(),
					'jenis_kegiatan'	: $("#rl38_list tr.data_rl38:eq(" + i + ") td#rl38_jenis_kegiatan").text(),
					'jumlah'			: $("#rl38_list tr.data_rl38:eq(" + i + ") td#rl38_jumlah").text()
				};
			}
			var data = this.getRegulerData();
			data['command'] = "export_xls";
			data['tahun'] = $("#rl38_tahun_filter").val();
			data['detail'] = JSON.stringify(detail);
			postForm(data);
			dismissLoading();
		}
	};
});