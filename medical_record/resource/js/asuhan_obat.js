var asuhan_obat;
var aso_dokter;
var aso_perawat;

var aso_noreg;
var aso_nama_pasien;
var aso_nrm_pasien;
var aso_polislug;
var aso_the_page;
var aso_the_protoslug;
var aso_the_protoname;
var aso_the_protoimplement;
$(document).ready(function() {
	
	aso_noreg=$("#aso_noreg").val();
	aso_nama_pasien=$("#aso_nama").val();
	aso_nrm_pasien=$("#aso_nrm").val();
	aso_polislug=$("#aso_polislug").val();
	aso_the_page=$("#aso_page").val();
	aso_the_protoslug=$("#aso_protoslug").val();
	aso_the_protoname=$("#aso_poliname").val();
	aso_the_protoimplement=$("#aso_implement").val();
	
	$(".mydatetime").datetimepicker({minuteStep:1});	
	var column=new Array(
		"id","waktu","jk",
		"carabayar","ruangan","nama_pasien",
		"noreg_pasien","nrm_pasien","nama_obat",
		'id_obat',"id_dokter","nama_dokter",
		"id_perawat","nama_perawat",
		"jumlah","keterangan","metode"
		);
	asuhan_obat=new TableAction("asuhan_obat",aso_the_page,"asuhan_obat",column);
	asuhan_obat.setPrototipe(aso_the_protoname,aso_the_protoslug,aso_the_protoimplement);
	asuhan_obat.setEnableAutofocus(true);
	asuhan_obat.setNextEnter();
	asuhan_obat.getRegulerData=function(){
		var reg_data={	
				page:this.page,
				action:this.action,
				super_command:this.super_command,
				prototype_name:this.prototype_name,
				prototype_slug:this.prototype_slug,
				prototype_implement:this.prototype_implement,
				polislug:aso_polislug,
				noreg_pasien:aso_noreg,
				nama_pasien:aso_nama_pasien,
				nrm_pasien:aso_nrm_pasien,
				jk:$("#asuhan_obat_jk").val()
				};
		return reg_data;
	};
	asuhan_obat.view();
	
	aso_dokter=new TableAction("aso_dokter",mr_the_page,"asuhan_obat",new Array());
	aso_dokter.setSuperCommand("aso_dokter");
	aso_dokter.setPrototipe(mr_the_protoname,mr_the_protoslug,mr_the_protoimplement);
	aso_dokter.selected=function(json){
		var nama=json.nama;
		var nip=json.id;		
		$("#asuhan_obat_nama_dokter").val(nama);
		$("#asuhan_obat_id_dokter").val(nip);
	};
	
	aso_perawat=new TableAction("aso_perawat",mr_the_page,"asuhan_obat",new Array());
	aso_perawat.setSuperCommand("aso_perawat");
	aso_perawat.setPrototipe(mr_the_protoname,mr_the_protoslug,mr_the_protoimplement);
	aso_perawat.selected=function(json){
		var nama=json.nama;
		var nip=json.id;		
		$("#asuhan_obat_nama_perawat").val(nama);
		$("#asuhan_obat_id_perawat").val(nip);
	};
	
});