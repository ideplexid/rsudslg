var rl52;
$(document).ready(function() {
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
	var column = new Array();
	rl52 = new TableAction("rl52", "medical_record", "rl52", column);
    rl52.getRegulerData = function() {
    	var data = TableAction.prototype.getRegulerData.call(this);
        data['bulan'] = $("#rl52_bulan").val();
        data['label_bulan'] = $("#rl52_bulan option:selected").text();
        data['tahun'] = $("#rl52_tahun").val();
        return data;
    };
    rl52.view = function() {
    	showLoading();
    	$("#rl52_list").empty();
    	var self = this;
    	var data = this.getRegulerData();
    	data['command'] = "get_jumlah_jenis_kegiatan";
    	$.post(
    		"",
    		data,
    		function(response) {
    			var json = JSON.parse(response);
    			if (json == null) {
    				dismissLoading();
    				return;
    			}
    			self.fillHtml(0, json.jumlah);
    		}
    	);
    };
    rl52.fillHtml = function(index, limit) {
    	if (index == limit) {
    		this.finalize();
    		return;
    	}
    	var self = this;
    	var data = this.getRegulerData();
    	data['command'] = "get_info";
    	data['num'] = index;
    	$.post(
    		"",
    		data,
    		function(response) {
    			var json = JSON.parse(response);
    			if (json == null) {
    				dismissLoading();
    				return;
    			}
    			$("#rl52_list").append(json.html);
    			self.fillHtml(index + 1, limit);
    		}
    	);
    };
    rl52.finalize = function() {
    	var num_rows = $("#rl52_list").children("tr").length;
		for (var i = 0; i < num_rows; i++) {
			var kode_rs = $("tbody#rl52_list tr:eq(" + i + ") td#rl52_nomor").html(i + 1);
		}
    	dismissLoading();
    };
    rl52.excel = function() {
    	showLoading();
		var num_rows = $("#rl52_list").children("tr").length;
		var d_data = {};
		for (var i = 0; i < num_rows; i++) {
			var kode_rs = $("tbody#rl52_list tr:eq(" + i + ") td:eq(0)").text();
			var nama_rs = $("tbody#rl52_list tr:eq(" + i + ") td:eq(1)").text();
			var bulan = $("tbody#rl52_list tr:eq(" + i + ") td:eq(2)").text();
			var tahun = $("tbody#rl52_list tr:eq(" + i + ") td:eq(3)").text();
			var kab_kota = $("tbody#rl52_list tr:eq(" + i + ") td:eq(4)").text();
			var kode_prov = $("tbody#rl52_list tr:eq(" + i + ") td:eq(5)").text();
			var nomor = $("tbody#rl52_list tr:eq(" + i + ") td:eq(6)").text();
			var jenis_kegiatan = $("tbody#rl52_list tr:eq(" + i + ") td:eq(7)").text();
			var jumlah = parseFloat($("tbody#rl52_list tr:eq(" + i + ") td:eq(8)").text().replace(".", ""));
			var l = parseFloat($("tbody#rl52_list tr:eq(" + i + ") td:eq(9)").text().replace(".", ""));
			var p = parseFloat($("tbody#rl52_list tr:eq(" + i + ") td:eq(10)").text().replace(".", ""));
			
			d_data[i] = {
				"kode_rs"			: kode_rs,
				"nama_rs"			: nama_rs,
				"bulan"				: bulan,
				"tahun"				: tahun,
				"kab_kota"			: kab_kota,
				"kode_prov"			: kode_prov,
				"nomor"				: nomor,
				"jenis_kegiatan"	: jenis_kegiatan,
				"jumlah"			: jumlah,
				"l"					: l,
				"p"					: p
			};
		}
		var data = this.getRegulerData();
		data['action'] = "create_excel_rl52";
		data['d_data'] = JSON.stringify(d_data);
		data['num_rows'] = num_rows;
		download(data);
		dismissLoading();
    };
    rl52.view();
});