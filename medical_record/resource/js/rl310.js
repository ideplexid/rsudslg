var rl310;
$(document).ready(function() {
	$("#rl310_list").html("<tr><td colspan='8'><center>DATA BELUM DITAMPILKAN</center></td></tr>");
	rl310 = new TableAction(
		"rl310",
		"medical_record",
		"rl310",
		new Array()
	);
	rl310.view = function() {
		var data = this.getRegulerData();
		data['command'] = "view";
		data['tahun'] = $("#rl310_tahun_filter").val();
		data['bulan'] = $("#rl310_bulan_filter").val();
		showLoading();
		$.post(
			"",
			data,
			function(response) {
				var json = JSON.parse(response);
				if (json == null) {
					dismissLoading();
					return;
				}
				$("#rl310_list").html(json.html);
				dismissLoading();
			}
		);
	};
	rl310.excel = function() {
		if ($("#rl310_tahun").length) {
			showLoading();
			var num_rows = $("#rl310_list tr").length;
			var detail = {};
			for (var i = 0; i < num_rows; i++) {
				detail[i] = {
					'kode_propinsi'	 		: $("#rl310_list tr:eq(" + i + ") td#rl310_kode_propinsi").text(),
					'kabupaten_kota' 		: $("#rl310_list tr:eq(" + i + ") td#rl310_kabupaten_kota").text(),
					'kode_rs'		 		: $("#rl310_list tr:eq(" + i + ") td#rl310_kode_rs").text(),
					'nama_rs'		 		: $("#rl310_list tr:eq(" + i + ") td#rl310_nama_rs").text(),
					'tahun' 		 		: $("#rl310_list tr:eq(" + i + ") td#rl310_tahun").text(),
					'nomor' 		 		: $("#rl310_list tr:eq(" + i + ") td#rl310_nomor").text(),
					'jenis_kegiatan'		: $("#rl310_list tr:eq(" + i + ") td#rl310_jenis_kegiatan").text(),
					'jumlah'				: $("#rl310_list tr:eq(" + i + ") td#rl310_jumlah").text()
				};
			}
			var data = this.getRegulerData();
			data['command'] = "export_xls";
			data['detail'] = JSON.stringify(detail);
			postForm(data);
			dismissLoading();
		}
	};
});