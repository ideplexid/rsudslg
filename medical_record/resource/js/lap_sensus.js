/*
 * this javascript used as handling of patient daily sensus
 * the data will be separated in 3 process
 *  - count the total
 *  - took detail of every patient
 *  - convert data into excel
 * 
 * @author 		: Nurul Huda
 * @license 	: Apache v3
 * @copyright 	: goblooge@gmail.com
 * @since		: 29 Oct 2016
 * @version		: 1.0.0
 * @action 		: medical_record/resource/php/laporan_pasien/lap_sensus.php
 * 				  snippet/get_sensus_detail.php
 * 				  snipeet/get_sensus_total.php
 * 				  snippet/get_sensus_excel.php
 * 
 * */

var lap_sensus;
var lap_sensus_data;
var IS_lap_sensus_RUNNING;
$(document).ready(function(){
	$('.mydatetime').datetimepicker({minuteStep:1});
	lap_sensus=new TableAction("lap_sensus","medical_record","lap_sensus",new Array());
	lap_sensus.addRegulerData=function(data){
		data['dari']        = $("#lap_sensus_dari").val();
		data['sampai']      = $("#lap_sensus_sampai").val();
		data['pelayanan']   = $("#lap_sensus_pelayanan").val();
		data['ruangan']     = $("#lap_sensus_ruangan").val();
		$("#dari_table_lap_sensus").html(getFormattedDate(data['dari']));
		$("#sampai_table_lap_sensus").html(getFormattedDate(data['sampai']));			
		return data;
	};

	lap_sensus.batal=function(){
		IS_lap_sensus_RUNNING=false;
		$("#rekap_lap_sensus_modal").modal("hide");
	};
	
	lap_sensus.afterview=function(json){
		if(json!=null){
			$("#kode_table_lap_sensus").html(json.nomor);
			$("#waktu_table_lap_sensus").html(json.waktu);
			lap_sensus_data=json;
		}
	};

	lap_sensus.rekaptotal=function(){
		if(IS_lap_sensus_RUNNING) return;
		$("#rekap_lap_sensus_bar").sload("true","Fetching total data",0);
		$("#rekap_lap_sensus_modal").modal("show");
		IS_lap_sensus_RUNNING=true;
		var d=this.getRegulerData();
		d['action']="get_sensus_total";
		$.post("",d,function(res){
			var all=getContent(res);
			if(all!=null) {
				var total=Number(all);
				lap_sensus.rekaploop(0,total);
			} else {
				$("#rekap_lap_sensus_modal").modal("hide");
				IS_lap_sensus_RUNNING=false;
			}
		});
	};

	lap_sensus.excel=function(){
		var d=this.getRegulerData();
		d['command']="excel";
		d['action']="get_sensus_excel";
		download(d);
	};

	lap_sensus.rekaploop=function(current,total){
		if(current>=total || !IS_lap_sensus_RUNNING) {
			$("#rekap_lap_sensus_modal").modal("hide");
			IS_lap_sensus_RUNNING=false;
			lap_sensus.view();
			return;
		}
		var d=this.getRegulerData();
		d['action']="get_sensus_detail";
		d['halaman']=current;
		$.post("",d,function(res){
			var ct=getContent(res);
			var u=ct['nama_pasien']+"  "+ct['nrm_pasien'];
			$("#rekap_lap_sensus_bar").sload("true"," Processing - "+u+"... [ "+(current+1)+" / "+total+" ] ",(current*100/total));
			setTimeout(function(){lap_sensus.rekaploop(++current,total)},300);
		});
	};
	
	/*$("#lap_sensus_ruangan").on("change",function(){
		lap_sensus.rekaptotal(); 
	});*/
    
    $("#lap_sensus_pelayanan").change(function(){
        var data = lap_sensus.getRegulerData();
        data['super_command'] = 'pelayanan';
        showLoading();
        $.post(
            "",
            data,
            function(response) {
                json = JSON.parse(response);
                console.log(json);
                select = document.getElementById('lap_sensus_ruangan');
                $("#lap_sensus_ruangan").empty();
                for (var i = 0; i<json.length; i++){
                    var opt = document.createElement('option');
                    opt.value = json[i]['value'];
                    opt.innerHTML = json[i]['name'];
                    select.appendChild(opt);
                }
                dismissLoading();
            }
        );
    });
			
});