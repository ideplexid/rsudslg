var laporan_index_penyakit;
var penyakit_laporan_index_penyakit;

$(document).ready(function() {
    $('.mydate').datepicker();
    laporan_index_penyakit=new TableAction("laporan_index_penyakit","medical_record","laporan_index_penyakit",new Array());
    laporan_index_penyakit.addRegulerData=function(data){
        data['dari']=$("#laporan_index_penyakit_dari").val();
        data['sampai']=$("#laporan_index_penyakit_sampai").val();
        data['urji']=$("#laporan_index_penyakit_urji").val();
        data['nama_penyakit']=$("#laporan_index_penyakit_nama_penyakit").val();						
        return data;
    };
    
    penyakit_laporan_index_penyakit=new TableAction("penyakit_laporan_index_penyakit","medical_record","laporan_index_penyakit",new Array());
    penyakit_laporan_index_penyakit.setSuperCommand("penyakit_laporan_index_penyakit");
    penyakit_laporan_index_penyakit.selected=function(json){
        $("#laporan_index_penyakit_nama_penyakit").val(json.nama);
    };
});