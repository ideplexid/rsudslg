var rl36;
$(document).ready(function() {
	$("#rl36_list").html("<tr><td colspan='12'><center>DATA BELUM DITAMPILKAN</center></td></tr>");
	rl36 = new TableAction(
		"rl36",
		"medical_record",
		"rl36",
		new Array()
	);
	rl36.view = function() {
		var data = this.getRegulerData();
		data['command'] = "view";
		data['tahun'] = $("#rl36_tahun_filter").val();
		data['bulan'] = $("#rl36_bulan_filter").val();
		showLoading();
		$.post(
			"",
			data,
			function(response) {
				var json = JSON.parse(response);
				if (json == null) {
					dismissLoading();
					return;
				}
				$("#rl36_list").html(json.html);
				dismissLoading();
			}
		);
	};
	rl36.excel = function() {
		if ($("#rl36_tahun").length) {
			showLoading();
			var num_rows = $("#rl36_list tr").length;
			var detail = {};
			for (var i = 0; i < num_rows; i++) {
				detail[i] = {
					'kode_propinsi'	 		: $("#rl36_list tr:eq(" + i + ") td#rl36_kode_propinsi").text(),
					'kabupaten_kota' 		: $("#rl36_list tr:eq(" + i + ") td#rl36_kabupaten_kota").text(),
					'kode_rs'		 		: $("#rl36_list tr:eq(" + i + ") td#rl36_kode_rs").text(),
					'nama_rs'		 		: $("#rl36_list tr:eq(" + i + ") td#rl36_nama_rs").text(),
					'tahun' 		 		: $("#rl36_list tr:eq(" + i + ") td#rl36_tahun").text(),
					'nomor' 		 		: $("#rl36_list tr:eq(" + i + ") td#rl36_nomor").text(),
					'spesialisasi'			: $("#rl36_list tr:eq(" + i + ") td#rl36_spesialisasi").text(),
					'total'					: $("#rl36_list tr:eq(" + i + ") td#rl36_total").text(),
					'khusus'				: $("#rl36_list tr:eq(" + i + ") td#rl36_khusus").text(),
					'besar'					: $("#rl36_list tr:eq(" + i + ") td#rl36_besar").text(),
					'sedang'				: $("#rl36_list tr:eq(" + i + ") td#rl36_sedang").text(),
					'kecil'					: $("#rl36_list tr:eq(" + i + ") td#rl36_kecil").text()
				};
			}
			var data = this.getRegulerData();
			data['command'] = "export_xls";
			data['detail'] = JSON.stringify(detail);
			postForm(data);
			dismissLoading();
		}
	};
});