var assesment_anak;

var asa_pasien;
var asa_noreg=$("#asa_noreg_pasien").val();
var asa_nama_pasien=$("#asa_nama_pasien").val();
var asa_nrm_pasien=$("#asa_nrm_pasien").val();
var asa_polislug=$("#asa_polislug").val();
var asa_the_page=$("#asa_the_page").val();
var asa_the_protoslug=$("#asa_the_protoslug").val();
var asa_the_protoname=$("#asa_the_protoname").val();
var asa_the_protoimplement=$("#asa_the_protoimplement").val();
var asa_id_antrian=$("#asa_id_antrian").val();
var asa_action=$("#asa_action").val();

$(document).ready(function(){
    $('.mydate').datepicker();
    $('.mydatetime').datetimepicker();
    asa_pasien=new TableAction("asa_pasien",asa_the_page,asa_action,new Array());
    asa_pasien.setSuperCommand("asa_pasien");
    asa_pasien.setPrototipe(asa_the_protoname,asa_the_protoslug,asa_the_protoimplement);
    asa_pasien.selected=function(json){
        var nama=json.nama_pasien;
        var nrm=json.nrm;
        var noreg=json.id;		
        $("#"+asa_action+"_nama_pasien").val(nama);
        $("#"+asa_action+"_nrm_pasien").val(nrm);
        $("#"+asa_action+"_noreg_pasien").val(noreg);
    };

    dokter_asa=new TableAction("dokter_asa",asa_the_page,asa_action,new Array());
    dokter_asa.setSuperCommand("dokter_asa");
    dokter_asa.setPrototipe(asa_the_protoname,asa_the_protoslug,asa_the_protoimplement);
    dokter_asa.selected=function(json){
        $("#"+asa_action+"_nama_dokter").val(json.nama);
        $("#"+asa_action+"_id_dokter").val(json.id);
    };

    stypeahead("#"+asa_action+"_dokter_asa",3,dokter_asa,"nama",function(item){
        $("#"+asa_action+"_nama_dokter").val(json.nama);
        $("#"+asa_action+"_id_dokter").val(json.id);			
    });
    
    /*
    diagnosa_asa=new TableAction("diagnosa_asa",asa_the_page,asa_action, new Array());
    diagnosa_asa.setSuperCommand("diagnosa_asa");
    diagnosa_asa.setPrototipe(asa_the_protoname,asa_the_protoslug,asa_the_protoimplement);
    diagnosa_asa.selected=function(json){
        $("#"+asa_action+"_diagnosa_kerja").val(json.nama);
        $("#"+asa_action+"_id_diagnosa_kerja").val(json.id);
    };
    stypeahead("#"+asa_action+"diagnosa_asa",3,diagnosa_asa,"nama",function(item){
        $("#"+asa_action+"_diagnosa_kerja").val(json.nama);
        $("#"+asa_action+"_id_diagnosa_kerja").val(json.id);			
    });
    */

    var column=new Array(
        "id","waktu","waktu_masuk","ruangan",
        "nama_pasien","noreg_pasien","nrm_pasien",
        "alamat","umur","jk","carabayar","tgl_lahir_pasien",
        "id_dokter","nama_dokter",
        "bb", "tb", "lila", "lk",
        "anamneses_utama", "anamneses_penyakit_skrg", "anamneses_pengobatan",
        "mata_anemia", "mata_ikterus", "mata_reflek_pupil", "mata_edema_palbebrae", 
        "tht_tonsil", "tht_faring", "tht_lidah", "tht_bibir", 
        "leher_jvp", "leher_pembesaran_kelenjar", 
        "kaku_kuduk", 
        "torak_simetris", "torak_asimetris", 
        "jantung_s1_s2", "jantung_reguler", "jantung_ireguler", "jantung_murmur", "jantung_lain", 
        "paru_suara_nafas", "paru_ronki", "paru_wheezing", "paru_lain", 
        "abdomen_distensi", "abdomen_meteorismus", "abdomen_peristaltik", "abdomen_arcites", "abdomen_nyeri", "abdomen_nyeri_lokasi",
        "hepar", "lien", "status_gizi", 
        "extremitas_hangat", "extremitas_dingin", "extremitas_edema", "extremitas_lain", 
        "rencana_kerja", "hasil_periksa_penunjang", "diagnosis", "terapi", "hasil_pembedahan", "rekomendasi", "catatan", 
        "waktu_pulang", "tgl_kontrol_klinik", "dirawat_diruang",
    );

    assesment_anak=new TableAction(asa_action,asa_the_page,asa_action,column);
    assesment_anak.setPrototipe(asa_the_protoname,asa_the_protoslug,asa_the_protoimplement);
    assesment_anak.getRegulerData=function(){
        var reg_data={	
                page:this.page,
                action:this.action,
                super_command:this.super_command,
                prototype_name:this.prototype_name,
                prototype_slug:this.prototype_slug,
                prototype_implement:this.prototype_implement,
                polislug:asa_polislug,
                noreg_pasien:asa_noreg,
                nama_pasien:asa_nama_pasien,
                nrm_pasien:asa_nrm_pasien,
                id_antrian:asa_id_antrian
                };
        return reg_data;
    };
    
    assesment_anak.setNextEnter();
    assesment_anak.setEnableAutofocus(true);
    window[asa_action]=assesment_anak;
    if(asa_action=="medical_record"){
        assesment_anak.view();
    }else{
        assesment_anak.clear=function(){return;};
    }
});