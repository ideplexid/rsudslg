var input_igd;
var igd_pasien;

$(document).ready(function(){
    $('.mydatetime').datetimepicker({minuteStep:1});
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
    
	var column=new Array(
                        'id',
                        'tanggal',
                        'ditangani',
                        'dikeluarkan',
                        'response',
                        'nama_pasien',
                        'sebutan',
                        'nrm_pasien',
                        'profile_number',
                        'noreg_pasien',
                        'ruangan',
                        'pola_kasus',
                        'triage',
                        'rujuk',
                        'menolak_mrs',
                        'ver',
                        'sift',
                        'ds',
                        'kelanjutan',
                        'kiriman',
                        'alat_bantu_hidup',
                        'tunadaksa',
                        'tunagrahita',
                        'tunarungu',
                        'tunanetra',
                        'tunalaras',
                        'tunawicara',
                        'carabayar'
                        );
	input_igd=new TableAction("input_igd","medical_record","input_igd",column);	
	input_igd.addRegulerData=function(reg_data){
		reg_data['dari']=$("#header_dari").val();
		reg_data['sampai']=$("#header_sampai").val();
		return reg_data;
	};
    
    $("#input_igd_ditangani, #input_igd_tanggal").on("change",function(){
				var s_date = $("#input_igd_tanggal").val().replace(/-/g, "/");
				var e_date = $("#input_igd_ditangani").val().replace(/-/g, "/");
				var timeStart = new Date(s_date).getTime();
				var timeEnd = new Date(e_date).getTime();
				var hourDiff = timeEnd - timeStart; //in ms
				var minDiff = Math.floor(hourDiff / 60 / 1000); //in minutes
				$("#input_igd_response").val(minDiff);
			});
    
    igd_pasien=new TableAction("igd_pasien","medical_record","input_igd",new Array());
    igd_pasien.setSuperCommand("igd_pasien");
    igd_pasien.selected=function(json){
        console.log(json);
        var alamat = json.alamat_pasien+' '+json.nama_kelurahan+json.nama_kecamatan+' '+json.nama_kabupaten+' '+json.nama_provinsi;
        $("#input_igd_nama_pasien").val(json.nama_pasien);
        $("#input_igd_sebutan").val(json.sebutan);
        $("#input_igd_nrm_pasien").val(json.nrm);
        $("#input_igd_noreg_pasien").val(json.id);
        $("#input_igd_profile_number").val(json.profile_number);
        $("#input_igd_no_telp").val(json.telpon);
        $("#input_igd_no_bpjs").val(json.nobpjs);
        $("#input_igd_alamat").val(alamat);
        if(json.kamar_inap == "" || json.kamar_inap == null) {
            $("#input_igd_ruangan").val(json.jenislayanan);
        } else {
            $("#input_igd_ruangan").val(json.kamar_inap);
        }
    };
	
});