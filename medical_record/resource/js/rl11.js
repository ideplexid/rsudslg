var rl11;
$(document).ready(function(){
    var column = new Array('');
    rl11 = new ReportAction("rl11", "medical_record", "rl11", column);
    
    rl11.cetak = function(){
        var data = this.getRegulerData();
        data['action'] = "rl11";
        showLoading();
        $.post("", data, function(res) {
            smis_print(res);
            dismissLoading();
        });
    };
    
    rl11.excel = function() {
        var data = this.getViewData();
        data['action'] = "create_excel_rl11";
        download(data);
    };
    
});