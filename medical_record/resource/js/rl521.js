var rl521;
$(document).ready(function() {
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
	var column = new Array();
	rl521 = new TableAction("rl521", "medical_record", "rl521", column);
    rl521.getRegulerData = function() {
    	var data = TableAction.prototype.getRegulerData.call(this);
        data['bulan'] = $("#rl521_bulan").val();
        data['label_bulan'] = $("#rl521_bulan option:selected").text();
        data['tahun'] = $("#rl521_tahun").val();
        return data;
    };
    rl521.excel = function() {
    	showLoading();
		var num_rows = $("#rl521_list").children("tr").length;
		var d_data = {};
		for (var i = 0; i < num_rows; i++) {
			var kode_rs = $("tbody#rl521_list tr:eq(" + i + ") td:eq(0)").text();
			var nama_rs = $("tbody#rl521_list tr:eq(" + i + ") td:eq(1)").text();
			var bulan = $("tbody#rl521_list tr:eq(" + i + ") td:eq(2)").text();
			var tahun = $("tbody#rl521_list tr:eq(" + i + ") td:eq(3)").text();
			var kab_kota = $("tbody#rl521_list tr:eq(" + i + ") td:eq(4)").text();
			var kode_prov = $("tbody#rl521_list tr:eq(" + i + ") td:eq(5)").text();
			var nomor = $("tbody#rl521_list tr:eq(" + i + ") td:eq(6)").text();
			var jenis_kegiatan = $("tbody#rl521_list tr:eq(" + i + ") td:eq(7)").text();
			var jumlah = parseFloat($("tbody#rl521_list tr:eq(" + i + ") td:eq(8)").text().replace(".", ""));
			var l = parseFloat($("tbody#rl521_list tr:eq(" + i + ") td:eq(9)").text().replace(".", ""));
			var p = parseFloat($("tbody#rl521_list tr:eq(" + i + ") td:eq(10)").text().replace(".", ""));
			
			d_data[i] = {
				"kode_rs"			: kode_rs,
				"nama_rs"			: nama_rs,
				"bulan"				: bulan,
				"tahun"				: tahun,
				"kab_kota"			: kab_kota,
				"kode_prov"			: kode_prov,
				"nomor"				: nomor,
				"jenis_kegiatan"	: jenis_kegiatan,
				"jumlah"			: jumlah,
				"l"					: l,
				"p"					: p
			};
		}
		var data = this.getRegulerData();
		data['action'] = "create_excel_rl521";
		data['d_data'] = JSON.stringify(d_data);
		data['num_rows'] = num_rows;
		download(data);
		dismissLoading();
    };
    rl521.view();
});