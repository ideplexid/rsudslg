function AssesmentDokterJalanAction(name,page,action,column){
	TableAction.call( this, name,page,action,column);
}
AssesmentDokterJalanAction.prototype.constructor    = AssesmentDokterJalanAction;
AssesmentDokterJalanAction.prototype                = Object.create( TableAction.prototype );
AssesmentDokterJalanAction.prototype.getRegulerData = function(){
    var reg_data            = TableAction.prototype.getRegulerData.call(this);
    var prf                 = "assesment_dokter_jalan";
    reg_data['nama_pasien'] = $("#"+prf+"_nama_pasien").val();
	reg_data['noreg_pasien']= $("#"+prf+"_noreg_pasien").val();
    reg_data['nrm_pasien']  = $("#"+prf+"_nrm_pasien").val();
	reg_data['carabayar']   = $("#"+prf+"_carabayar").val();
	return reg_data;
};