var lap_persalinan;
$(document).ready(function() {
    $('.mydate').datepicker();
    lap_persalinan = new TableAction("lap_persalinan","medical_record","lap_persalinan",new Array());
    lap_persalinan.getRegulerData = function() {
        var reg_data={	
				page:this.page,
				action:this.action,
				super_command:this.super_command,
				prototype_name:this.prototype_name,
				prototype_slug:this.prototype_slug,
				prototype_implement:this.prototype_implement,
				dari:$("#lap_persalinan_dari").val(),
				sampai:$("#lap_persalinan_sampai").val(),
                origin:$("#lap_persalinan_origin").val(),
				};
		return reg_data;
    }
});
