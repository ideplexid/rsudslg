
var cauti_perawat;
var cauti_noreg;
var cauti_nama_pasien;
var cauti_nrm_pasien;
var cauti_polislug;
var cauti_the_page;
var cauti_the_protoslug;
var cauti_the_protoname;
var cauti_the_protoimplement;
$(document).ready(function() {
	
	cauti_noreg=$("#cauti_noreg").val();
	cauti_nama_pasien=$("#cauti_nama").val();
	cauti_nrm_pasien=$("#cauti_nrm").val();
	cauti_polislug=$("#cauti_polislug").val();
	cauti_the_page=$("#cauti_page").val();
	cauti_the_protoslug=$("#cauti_protoslug").val();
	cauti_the_protoname=$("#cauti_poliname").val();
	cauti_the_protoimplement=$("#cauti_implement").val();
	 
	$(".mydate").datepicker();	
	var column=new Array(
		"id","tanggal",
		"ruangan","nama_pasien",
		"noreg_pasien","nrm_pasien","nama_obat",
		"nama_perawat",
		"kegiatan","jenis_cath","nocath","aseptik",
        "kondisi","bladder","sambungan","perineal","gelas_ukur",
        "indikasi","isk","kuman","pyuria","keluar"
		);
	cauti=new TableAction("cauti",cauti_the_page,"cauti",column);
	cauti.setPrototipe(cauti_the_protoname,cauti_the_protoslug,cauti_the_protoimplement);
	cauti.setEnableAutofocus(true);
	cauti.setNextEnter();
	cauti.getRegulerData=function(){
		var reg_data={	
				page:this.page,
				action:this.action,
				super_command:this.super_command,
				prototype_name:this.prototype_name,
				prototype_slug:this.prototype_slug,
				prototype_implement:this.prototype_implement,
				polislug:cauti_polislug,
				noreg_pasien:cauti_noreg,
				nama_pasien:cauti_nama_pasien,
				nrm_pasien:cauti_nrm_pasien
            };
		return reg_data;
	};
	cauti.view();
	
	
	cauti_perawat=new TableAction("cauti_perawat",cauti_the_page,"cauti",new Array());
	cauti_perawat.setSuperCommand("cauti_perawat");
	cauti_perawat.setPrototipe(cauti_the_protoname,cauti_the_protoslug,cauti_the_protoimplement);
	cauti_perawat.selected=function(json){
		var nama=json.nama;
		var nip=json.id;		
		$("#cauti_nama_perawat").val(nama);
		$("#cauti_id_perawat").val(nip);
	};
	
});