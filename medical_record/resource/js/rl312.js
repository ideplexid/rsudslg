var rl312;
$(document).ready(function() {
	$("#rl312_list").html("<tr><td colspan='19'><center>DATA BELUM DITAMPILKAN</center></td></tr>");
	rl312 = new TableAction(
		"rl312",
		"medical_record",
		"rl312",
		new Array()
	);
	rl312.view = function() {
		var data = this.getRegulerData();
		data['command'] = "view";
		data['tahun'] = $("#rl312_tahun_filter").val();
		data['bulan'] = $("#rl312_bulan_filter").val();
		showLoading();
		$.post(
			"",
			data,
			function(response) {
				var json = JSON.parse(response);
				if (json == null) {
					dismissLoading();
					return;
				}
				$("#rl312_list").html(json.html);
				dismissLoading();
			}
		);
	};
	rl312.excel = function() {
		console.log("WOI");
		if ($("#rl312_tahun").length) {
			showLoading();
			var num_rows = $("#rl312_list tr.data_rl312").length;
			var detail = {};
			for (var i = 0; i < num_rows; i++) {
				detail[i] = {
					'kode_propinsi' 					: $("#rl312_list tr.data_rl312:eq(" + i + ") td#rl312_kode_propinsi").html(),
					'kabupaten_kota' 					: $("#rl312_list tr.data_rl312:eq(" + i + ") td#rl312_kabupaten_kota").html(),
					'kode_rs' 							: $("#rl312_list tr.data_rl312:eq(" + i + ") td#rl312_kode_rs").html(),
					'nama_rs' 							: $("#rl312_list tr.data_rl312:eq(" + i + ") td#rl312_nama_rs").html(),
					'tahun' 							: $("#rl312_list tr.data_rl312:eq(" + i + ") td#rl312_tahun").html(),
					'nomor' 							: $("#rl312_list tr.data_rl312:eq(" + i + ") td#rl312_nomor").html(),
					'metode' 							: $("#rl312_list tr.data_rl312:eq(" + i + ") td#rl312_metode").html(),
					'konseling_anc' 					: $("#rl312_list tr.data_rl312:eq(" + i + ") td#rl312_konseling_anc").html(),
					'konseling_pasca_persalinan' 		: $("#rl312_list tr.data_rl312:eq(" + i + ") td#rl312_konseling_pasca_persalinan").html(),
					'kb_baru_cara_masuk_bukan_rujukan' 	: $("#rl312_list tr.data_rl312:eq(" + i + ") td#rl312_kb_baru_cara_masuk_bukan_rujukan").html(),
					'kb_baru_cara_masuk_rujukan_ri' 	: $("#rl312_list tr.data_rl312:eq(" + i + ") td#rl312_kb_baru_cara_masuk_rujukan_ri").html(),
					'kb_baru_cara_masuk_rujukan_rj' 	: $("#rl312_list tr.data_rl312:eq(" + i + ") td#rl312_kb_baru_cara_masuk_rujukan_rj").html(),
					'kb_baru_cara_masuk_total' 			: $("#rl312_list tr.data_rl312:eq(" + i + ") td#rl312_kb_baru_cara_masuk_total").html(),
					'kb_baru_kondisi_pasca_persalinan' 	: $("#rl312_list tr.data_rl312:eq(" + i + ") td#rl312_kb_baru_kondisi_pasca_persalinan").html(),
					'kb_baru_kondisi_abortus' 			: $("#rl312_list tr.data_rl312:eq(" + i + ") td#rl312_kb_baru_kondisi_abortus").html(),
					'kb_baru_kondisi_lain' 				: $("#rl312_list tr.data_rl312:eq(" + i + ") td#rl312_kb_baru_kondisi_lain").html(),
					'kunjungan_ulang' 					: $("#rl312_list tr.data_rl312:eq(" + i + ") td#rl312_kunjungan_ulang").html(),
					'keluhan_efek_samping' 				: $("#rl312_list tr.data_rl312:eq(" + i + ") td#rl312_keluhan_efek_samping").html(),
					'keluhan_efek_samping_dirujuk' 		: $("#rl312_list tr.data_rl312:eq(" + i + ") td#rl312_keluhan_efek_samping_dirujuk").html()
				};
			}
			var data = this.getRegulerData();
			data['command'] = "export_xls";
			data['tahun'] = $("#rl312_tahun_filter").val();
			data['detail'] = JSON.stringify(detail);
			postForm(data);
			dismissLoading();
		}
	};
});