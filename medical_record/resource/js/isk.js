/**
 * this source code used for handling
 * urinary tract infection
 * 
 * @author 		: Nurul Huda
 * @license 	: Apache v3
 * @version 	: 1.0.0
 * @since 		: 24 Sept 2016
 * @copyright 	: goblooge@gmail.com
 * @used		: medical_record/resource/php/indikator_klinis/isk.php
 * 
 * */
 
var isk;
$(document).ready(function(){
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
	$('.mydate').datepicker();
	var column=new Array('ruangan');
	isk=new ReportAction("isk","medical_record","isk",column);
	isk.setDiagramHolder("isk_chart");
	isk.setXKey('Date');
	isk.setYKey(['ya','tidak']);
	isk.setLabels(["Ya","Tidak"]);
	
	isk.initDiagram("");
	isk.setTitle("Pasien dengan ISK");
});