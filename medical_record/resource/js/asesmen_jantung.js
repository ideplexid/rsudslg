var asesment_jantung;

var apj_pasien;
var dokter_apj;
var apj_noreg=$("#apj_noreg_pasien").val();
var apj_nama_pasien=$("#apj_nama_pasien").val();
var apj_nrm_pasien=$("#apj_nrm_pasien").val();
var apj_polislug=$("#apj_polislug").val();
var apj_the_page=$("#apj_the_page").val();
var apj_the_protoslug=$("#apj_the_protoslug").val();
var apj_the_protoname=$("#apj_the_protoname").val();
var apj_the_protoimplement=$("#apj_the_protoimplement").val();
var apj_id_antrian=$("#apj_id_antrian").val();
var apj_action=$("#apj_action").val();

$(document).ready(function(){
    $('.mydate').datepicker();
    $('.mydatetime').datetimepicker();
    
    apj_pasien=new TableAction("apj_pasien",apj_the_page,apj_action,new Array());
    apj_pasien.setSuperCommand("apj_pasien");
    apj_pasien.setPrototipe(apj_the_protoname,apj_the_protoslug,apj_the_protoimplement);
    apj_pasien.selected=function(json){
        var nama=json.nama_pasien;
        var nrm=json.nrm;
        var noreg=json.id;		
        $("#"+apj_action+"_nama_pasien").val(nama);
        $("#"+apj_action+"_nrm_pasien").val(nrm);
        $("#"+apj_action+"_noreg_pasien").val(noreg);
    };
    
    dokter_apj=new TableAction("dokter_apj",apj_the_page,apj_action,new Array());
    dokter_apj.setSuperCommand("dokter_apj");
    dokter_apj.setPrototipe(apj_the_protoname,apj_the_protoslug,apj_the_protoimplement);
    dokter_apj.selected=function(json){
        $("#"+apj_action+"_nama_dokter").val(json.nama);
        $("#"+apj_action+"_id_dokter").val(json.id);
    };

    stypeahead("#"+apj_action+"_dokter_apj",3,dokter_apj,"nama",function(item){
        $("#"+apj_action+"_nama_dokter").val(json.nama);
        $("#"+apj_action+"_id_dokter").val(json.id);			
    });
    
    var column=new Array(
        "id","waktu","ruangan", "waktu_masuk", 
        "nama_pasien","noreg_pasien","nrm_pasien",
        "alamat","umur","jk","carabayar","tgl_lahir_pasien",
        "id_dokter","nama_dokter",
        "keluhan_utama", "riwayat_penyakit_skrg", "riwayat_pengobatan", 
        "torak", "torak_ket", "iktus_kordis", "iktus_kordis_ket", "iktus_kordis_lokasi", 
        "pulpasi", "pulpasi_ket", "palpasi_iktus_kordis", "palpasi_iktus_kordis_ket", "palpasi_iktus_kordis_lokasi", 
        "palpasi_thrill", "palpasi_thrill_ket", "palpasi_thrill_lokasi", 
        "perkusi_batas_atas", "perkusi_batas_bawah", "perkusi_batas_kanan", "perkusi_batas_kiri", 
        "sju", "sju_s1", "sju_s2", "extra_systole", "gallop",
        "murmur_fase", "murmur_fase_ket", "murmur_lokasi", "murmur_lokasi_ket", 
        "murmur_kualitas", "murmur_kualitas_ket", "murmur_grade", "murmur_penjalaran", "murmur_penjalaran_ket", 
        "murmur_opening_snap", "murmur_friction_rub", 
        "paru", "paru_ket", "ecg", "thorax_foto", "echocardiografi", 
        "diagnosis", "terapi", "rencana_kerja",
        "waktu_pulang", "waktu_kontrol_klinik", "dirawat_diruang", 
    );
    
    assesment_jantung=new TableAction(apj_action,apj_the_page,apj_action,column);
    assesment_jantung.setPrototipe(apj_the_protoname,apj_the_protoslug,apj_the_protoimplement);
    assesment_jantung.getRegulerData=function(){
        var reg_data={	
                page:this.page,
                action:this.action,
                super_command:this.super_command,
                prototype_name:this.prototype_name,
                prototype_slug:this.prototype_slug,
                prototype_implement:this.prototype_implement,
                polislug:apj_polislug,
                noreg_pasien:apj_noreg,
                nama_pasien:apj_nama_pasien,
                nrm_pasien:apj_nrm_pasien,
                id_antrian:apj_id_antrian
                };
        return reg_data;
    };
    
    assesment_jantung.setNextEnter();
    assesment_jantung.setEnableAutofocus(true);
    window[apj_action]=assesment_jantung;
    if(apj_action=="medical_record"){
        assesment_jantung.view();
    }else{
        assesment_jantung.clear=function(){return;};
    }
});