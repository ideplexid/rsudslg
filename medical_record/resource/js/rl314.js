var rl314;
$(document).ready(function() {
	$("#rl314_list").html("<tr><td colspan='16'><center>DATA BELUM DITAMPILKAN</center></td></tr>");
	rl314 = new TableAction(
		"rl314",
		"medical_record",
		"rl314",
		new Array()
	);
	rl314.view = function() {
		var data = this.getRegulerData();
		data['command'] = "view";
		data['tahun'] = $("#rl314_tahun_filter").val();
		data['bulan'] = $("#rl314_bulan_filter").val();
		showLoading();
		$.post(
			"",
			data,
			function(response) {
				var json = JSON.parse(response);
				if (json == null) {
					dismissLoading();
					return;
				}
				$("#rl314_list").html(json.html);
				dismissLoading();
			}
		);
	};
	rl314.excel = function() {
		if ($("#rl314_tahun").length) {
			showLoading();
			var num_rows = $("#rl314_list tr.data_rl314").length;
			var detail = {};
			for (var i = 0; i < num_rows; i++) {
				detail[i] = {
					"kode_propinsi" 							: $("#rl314_list tr.data_rl314:eq(" + i + ") td#rl314_kode_propinsi").text(), 
					"kabupaten_kota" 							: $("#rl314_list tr.data_rl314:eq(" + i + ") td#rl314_kabupaten_kota").text(), 
					"kode_rs" 									: $("#rl314_list tr.data_rl314:eq(" + i + ") td#rl314_kode_rs").text(), 
					"nama_rs" 									: $("#rl314_list tr.data_rl314:eq(" + i + ") td#rl314_nama_rs").text(), 
					"tahun" 									: $("#rl314_list tr.data_rl314:eq(" + i + ") td#rl314_tahun").text(), 
					"nomor" 									: $("#rl314_list tr.data_rl314:eq(" + i + ") td#rl314_nomor").text(), 
					"spesialisasi" 								: $("#rl314_list tr.data_rl314:eq(" + i + ") td#rl314_spesialisasi").text(), 
					"jumlah_diterima_dari_puskesmas" 			: $("#rl314_list tr.data_rl314:eq(" + i + ") td#rl314_diterima_dari_puskesmas").text(), 
					"jumlah_diterima_dari_faskes_lain" 			: $("#rl314_list tr.data_rl314:eq(" + i + ") td#rl314_diterima_dari_faskes_lain").text(), 
					"jumlah_diterima_dari_rs_lain" 				: $("#rl314_list tr.data_rl314:eq(" + i + ") td#rl314_diterima_dari_rs_lain").text(), 
					"jumlah_dikembalikan_ke_puskesmas" 			: $("#rl314_list tr.data_rl314:eq(" + i + ") td#rl314_dikembalikan_ke_puskesmas").text(), 
					"jumlah_dikembalikan_ke_faskes_lain" 		: $("#rl314_list tr.data_rl314:eq(" + i + ") td#rl314_dikembalikan_ke_faskes_lain").text(), 
					"jumlah_dikembalikan_ke_rs_lain" 			: $("#rl314_list tr.data_rl314:eq(" + i + ") td#rl314_dikembalikan_ke_rs_lain").text(), 
					"jumlah_dirujuk_pasien_rujukan" 			: $("#rl314_list tr.data_rl314:eq(" + i + ") td#rl314_dirujuk_pasien_rujukan").text(), 
					"jumlah_dirujuk_pasien_datang_sendiri" 		: $("#rl314_list tr.data_rl314:eq(" + i + ") td#rl314_dirujuk_pasien_datang_sendiri").text(), 
					"jumlah_dirujuk_pasien_diterima_kembali"	: $("#rl314_list tr.data_rl314:eq(" + i + ") td#rl314_dirujuk_pasien_diterima_kembali").text() 
				};
			}
			var data = this.getRegulerData();
			data['command'] = "export_xls";
			data['tahun'] = $("#rl314_tahun_filter").val();
			data['detail'] = JSON.stringify(detail);
			postForm(data);
			dismissLoading();
		}
	};
});