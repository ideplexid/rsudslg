var lap_index_penyakit;
var penyakit_lap_index_penyakit;
var IS_lap_index_penyakit_RUNNING;
$(document).ready(function(){
    $('.mydate').datepicker();
    lap_index_penyakit=new TableAction("lap_index_penyakit","medical_record","lap_index_penyakit",new Array());
    lap_index_penyakit.addRegulerData=function(data){
        data['dari']=$("#lap_index_penyakit_dari").val();
        data['sampai']=$("#lap_index_penyakit_sampai").val();
        data['urji']=$("#lap_index_penyakit_urji").val();
        data['nama_penyakit']=$("#lap_index_penyakit_nama_penyakit").val();
        $("#dari_table_lap_index_penyakit").html(getFormattedDate(data['dari']));
        $("#sampai_table_lap_index_penyakit").html(getFormattedDate(data['sampai']));							
        return data;
    };

    lap_index_penyakit.batal=function(){
        IS_lap_index_penyakit_RUNNING=false;
        $("#rekap_lap_index_penyakit_modal").modal("hide");
    };
    
    lap_index_penyakit.afterview=function(json){
        if(json!=null){
            $("#kode_table_lap_index_penyakit").html(json.nomor);
            $("#waktu_table_lap_index_penyakit").html(json.waktu);
            lap_index_penyakit_data=json;
        }
    };

    lap_index_penyakit.rekaptotal=function(){
        if(IS_lap_index_penyakit_RUNNING) return;
        $("#rekap_lap_index_penyakit_bar").sload("true","Fetching total data",0);
        $("#rekap_lap_index_penyakit_modal").modal("show");
        IS_lap_index_penyakit_RUNNING=true;
        var d=this.getRegulerData();
        d['super_command']="total";
        $.post("",d,function(res){
            var all=getContent(res);
            if(all!=null) {
                var total=Number(all);
                lap_index_penyakit.rekaploop(0,total);
            } else {
                $("#rekap_lap_index_penyakit_modal").modal("hide");
                IS_lap_index_penyakit_RUNNING=false;
            }
        });
    };

    lap_index_penyakit.excel=function(){
        var d=this.getRegulerData();
        d['command']="excel";
        download(d);
    };

    lap_index_penyakit.rekaploop=function(current,total){
        if(current>=total || !IS_lap_index_penyakit_RUNNING) {
            //$("#rekap_lap_index_penyakit_modal").modal("hide");
            IS_lap_index_penyakit_RUNNING=false;
            lap_index_penyakit.multiview();
            return;
        }
        var d=this.getRegulerData();
        d['super_command']="limit";
        d['halaman']=current;
        $.post("",d,function(res){
            var ct=getContent(res);
            var u=ct[0]['nama_pasien']+"  "+ct[0]['nrm_pasien'];
            $("#rekap_lap_index_penyakit_bar").sload("true"," Processing - "+u+"... [ "+(current+1)+" / "+total+" ] ",(current*100/total));
            setTimeout(function(){lap_index_penyakit.rekaploop(++current,total)},300);
        });
    };
    
    lap_index_penyakit.multiview=function(){
        $("#rekap_lap_index_penyakit_bar").sload("true","Viewing data...",0);
        $("#rekap_lap_index_penyakit_modal").modal("show");
        IS_lap_index_penyakit_RUNNING=true;
        var d=this.getViewData();
        $("#lap_index_penyakit_list").html("");
        d['command']="multipart_view";
        $.post("",d,function(res){
            var total=Number(getContent(res));
            if(total==0){
                $("#rekap_lap_index_penyakit_modal").modal("hide");
                IS_lap_index_penyakit_RUNNING=false;
            }else{
                var pages=Math.ceil(total/5);
                lap_index_penyakit.multipart_loop(0,pages);
            }
        });			
    };
    
    lap_index_penyakit.multipart_loop=function(current,total){
        if(current>=total || !IS_lap_index_penyakit_RUNNING) {
            IS_lap_index_penyakit_RUNNING=false;
            lap_index_penyakit.last_count();
            return;
        }
        $("#rekap_lap_index_penyakit_bar").sload("true","Page Number [ "+(current+1)+" / "+total+" ] ",(current*100/total));
        var d=this.getViewData();
        d['number']=current;
        d['max']=5;		
        $.post("",d,function(res){
            var ct=getContent(res);
            $("#lap_index_penyakit_list").append(ct.list);
            setTimeout(function(){lap_index_penyakit.multipart_loop(++current,total)},300);
        });
    };
    
    lap_index_penyakit.last_count=function(){
        var d=this.getRegulerData();
        d['command']="last_count";
        $.post("",d,function(res){
            var ct=getContent(res);
            $("#lap_index_penyakit_list").append(ct);
            $("#rekap_lap_index_penyakit_modal").modal("hide");
        });
    };
    

    
    penyakit_lap_index_penyakit=new TableAction("penyakit_lap_index_penyakit","medical_record","lap_index_penyakit",new Array());
    penyakit_lap_index_penyakit.setSuperCommand("penyakit_lap_index_penyakit");
    penyakit_lap_index_penyakit.selected=function(json){
        $("#lap_index_penyakit_nama_penyakit").val(json.nama);
        lap_index_penyakit.multiview();
    };
});