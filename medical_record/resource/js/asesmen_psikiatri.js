var asesment_psikiatri;

var apk_pasien;
var dokter_apk;
var apk_noreg=$("#apk_noreg_pasien").val();
var apk_nama_pasien=$("#apk_nama_pasien").val();
var apk_nrm_pasien=$("#apk_nrm_pasien").val();
var apk_polislug=$("#apk_polislug").val();
var apk_the_page=$("#apk_the_page").val();
var apk_the_protoslug=$("#apk_the_protoslug").val();
var apk_the_protoname=$("#apk_the_protoname").val();
var apk_the_protoimplement=$("#apk_the_protoimplement").val();
var apk_id_antrian=$("#apk_id_antrian").val();
var apk_action=$("#apk_action").val();

$(document).ready(function(){
    $('.mydate').datepicker();
    $('.mydatetime').datetimepicker();
    
    apk_pasien=new TableAction("apk_pasien",apk_the_page,apk_action,new Array());
    apk_pasien.setSuperCommand("apk_pasien");
    apk_pasien.setPrototipe(apk_the_protoname,apk_the_protoslug,apk_the_protoimplement);
    apk_pasien.selected=function(json){
        var nama=json.nama_pasien;
        var nrm=json.nrm;
        var noreg=json.id;		
        $("#"+apk_action+"_nama_pasien").val(nama);
        $("#"+apk_action+"_nrm_pasien").val(nrm);
        $("#"+apk_action+"_noreg_pasien").val(noreg);
    };
    
    dokter_apk=new TableAction("dokter_apk",apk_the_page,apk_action,new Array());
    dokter_apk.setSuperCommand("dokter_apk");
    dokter_apk.setPrototipe(apk_the_protoname,apk_the_protoslug,apk_the_protoimplement);
    dokter_apk.selected=function(json){
        $("#"+apk_action+"_nama_dokter").val(json.nama);
        $("#"+apk_action+"_id_dokter").val(json.id);
    };

    stypeahead("#"+apk_action+"_dokter_apk",3,dokter_apk,"nama",function(item){
        $("#"+apk_action+"_nama_dokter").val(json.nama);
        $("#"+apk_action+"_id_dokter").val(json.id);			
    });
    
    var column=new Array(
        "id","waktu","ruangan", "waktu_masuk",
        "nama_pasien","noreg_pasien","nrm_pasien",
        "alamat","umur","jk","carabayar","tgl_lahir_pasien",
        "id_dokter","nama_dokter",
        "wawancara", "keluhan_utama", "riwayat_penyakit_skrg", "auto_anamnesis", "hetero_namnesis", 
        "faktor_penyebab", "faktor_keluarga", "fungsi_kerja", "riwayat_napza","napza_lama_pemakaian", 
        "napza_jenis_zat", "napza_cara_pemakaian", "napza_latarbelakang_pemakaian", 
        "faktor_premorbid", "faktor_organik", "kesan_umum", "kesadaran", "mood", "proses_pikir",
        "pencerapan", "dorongan_insting", "psikomotor", "penyakit_dalam", "neurologi", 
        "diagnosis_kerja", "terapi", "rencana_kerja", 
        "waktu_pulang", "waktu_kontrol_klinik", "dirawat_diruang", 
    );
    
    assesment_psikiatri=new TableAction(apk_action,apk_the_page,apk_action,column);
    assesment_psikiatri.setPrototipe(apk_the_protoname,apk_the_protoslug,apk_the_protoimplement);
    assesment_psikiatri.getRegulerData=function(){
        var reg_data={	
                page:this.page,
                action:this.action,
                super_command:this.super_command,
                prototype_name:this.prototype_name,
                prototype_slug:this.prototype_slug,
                prototype_implement:this.prototype_implement,
                polislug:apk_polislug,
                noreg_pasien:apk_noreg,
                nama_pasien:apk_nama_pasien,
                nrm_pasien:apk_nrm_pasien,
                id_antrian:apk_id_antrian
                };
        return reg_data;
    };
    
    assesment_psikiatri.setNextEnter();
    assesment_psikiatri.setEnableAutofocus(true);
    window[apk_action]=assesment_psikiatri;
    if(apk_action=="medical_record"){
        assesment_psikiatri.view();
    }else{
        assesment_psikiatri.clear=function(){return;};
    }
});