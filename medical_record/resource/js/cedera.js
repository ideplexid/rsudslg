/**
 * this source code used for handling
 * injury reporting
 * 
 * @author 		: Nurul Huda
 * @license 	: Apache v3
 * @version 	: 1.0.0
 * @since 		: 24 Sept 2016
 * @copyright 	: goblooge@gmail.com
 * @used		: medical_record/resource/php/indikator_klinis/cedera.php
 * 
 * */
var cedera;
$(document).ready(function(){
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
	$('.mydate').datepicker();
	var column=new Array('ruangan');
	cedera=new ReportAction("cedera","medical_record","cedera",column);
	cedera.setXKey('Date');
	cedera.setYKey(['ya','tidak']);
	cedera.setLabels(["Ya","Tidak"]);
	cedera.setDiagramHolder("chart_cedera");
	cedera.initDiagram("");
	cedera.setTitle("Diagram Cedera");
});