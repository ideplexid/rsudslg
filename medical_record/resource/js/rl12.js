var rl12;
$(document).ready(function() {
	$("#rl12_list").html("<tr><td colspan='12'><center>DATA BELUM DITAMPILKAN</center></td></tr>");
	rl12 = new TableAction(
		"rl12",
		"medical_record",
		"rl12",
		new Array()
	);
	rl12.view = function() {
		var data = this.getRegulerData();
		data['command'] = "view";
		data['tahun'] = $("#rl12_tahun_filter").val();
		showLoading();
		$.post(
			"",
			data,
			function(response) {
				var json = JSON.parse(response);
				if (json == null) {
					dismissLoading();
					return;
				}
				$("#rl12_list").html(json.html);
				dismissLoading();
			}
		);
	};
	rl12.excel = function() {
		if ($("#rl12_tahun").length) {
			var data = this.getRegulerData();
			data['command'] = "export_xls";
			data['kode_rs'] = $("#rl12_kode_rs").text();
			data['nama_rs'] = $("#rl12_nama_rs").text();
			data['kode_propinsi'] = $("#rl12_kode_propinsi").text();
			data['kabupaten_kota'] = $("#rl12_kabupaten_kota").text();
			data['tahun'] = $("#rl12_tahun").text();
			data['bor'] = $("#rl12_bor").text();
			data['avlos'] = $("#rl12_avlos").text();
			data['toi'] = $("#rl12_toi").text();
			data['bto'] = $("#rl12_bto").text();
			data['ndr'] = $("#rl12_ndr").text();
			data['gdr'] = $("#rl12_gdr").text();
			data['rerata_kunjungan'] = $("#rl12_rerata_kunjungan").text();
			showLoading();
			postForm(data);
			dismissLoading();
		}
	};
	rl12.help = function() {
		bootbox.alert(
			"<h3><b>Keterangan</b></h3>" +
			"<b>BOR</b> = Jumlah hari perawatan / (jumlah tempat tidur x jumlah hari dalam 1 periode) x 100%" +
			"<br/><b>LOS</b> = Jumlah lama dirawat pasien keluar / jumlah semua pasien keluar" +
			"<br/><b>BTO</b> = Jumlah semua pasien keluar / jumlah bed" +
			"<br/><b>TOI</b> = ((Jumlah tempat tidur x hari dalam periode) - hari perawatan dalam periode) / jumlah semua pasien keluar" +
			"<br/><b>NDR</b> = jumlah pasien meninggal > 48 jam / jumlah semua pasien keluar x 1000 permil" +
			"<br/><b>GDR</b> = jumlah semua pasien yang meninggal / jumlah semua pasien keluar x 1000 permil"
		);
	};
});