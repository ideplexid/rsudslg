
var plebitis_perawat;
var plebitis_noreg;
var plebitis_nama_pasien;
var plebitis_nrm_pasien;
var plebitis_polislug;
var plebitis_the_page;
var plebitis_the_protoslug;
var plebitis_the_protoname;
var plebitis_the_protoimplement;
$(document).ready(function() {
	
	plebitis_noreg=$("#plebitis_noreg").val();
	plebitis_nama_pasien=$("#plebitis_nama").val();
	plebitis_nrm_pasien=$("#plebitis_nrm").val();
	plebitis_polislug=$("#plebitis_polislug").val();
	plebitis_the_page=$("#plebitis_page").val();
	plebitis_the_protoslug=$("#plebitis_protoslug").val();
	plebitis_the_protoname=$("#plebitis_poliname").val();
	plebitis_the_protoimplement=$("#plebitis_implement").val();
	
    
	$(".mydate").datepicker();	
	var column=new Array(
		"id","tanggal",
		"ruangan","nama_pasien",
		"noreg_pasien","nrm_pasien","nama_obat",
		"nama_perawat",
		"kegiatan","lokasi","noiv","jenis_cairan",
        "plebitis","gol_obat","keluar"
		);
	plebitis=new TableAction("plebitis",plebitis_the_page,"plebitis",column);
	plebitis.setPrototipe(plebitis_the_protoname,plebitis_the_protoslug,plebitis_the_protoimplement);
	plebitis.setEnableAutofocus(true);
	plebitis.setNextEnter();
	plebitis.getRegulerData=function(){
		var reg_data={	
				page:this.page,
				action:this.action,
				super_command:this.super_command,
				prototype_name:this.prototype_name,
				prototype_slug:this.prototype_slug,
				prototype_implement:this.prototype_implement,
				polislug:plebitis_polislug,
				noreg_pasien:plebitis_noreg,
				nama_pasien:plebitis_nama_pasien,
				nrm_pasien:plebitis_nrm_pasien
            };
		return reg_data;
	};
	plebitis.view();
	
	
	plebitis_perawat=new TableAction("plebitis_perawat",plebitis_the_page,"plebitis",new Array());
	plebitis_perawat.setSuperCommand("plebitis_perawat");
	plebitis_perawat.setPrototipe(plebitis_the_protoname,plebitis_the_protoslug,plebitis_the_protoimplement);
	plebitis_perawat.selected=function(json){
		var nama=json.nama;
		var nip=json.id;		
		$("#plebitis_nama_perawat").val(nama);
		$("#plebitis_id_perawat").val(nip);
	};
	
});