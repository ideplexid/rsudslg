<?php
global $PLUGINS;

$init ['name'] = 'medical_record';
$init ['path'] = SMIS_DIR . "medical_record/";
$init ['description'] = "Rekam Medis, Tambahan Odontogram";
$init ['require'] = "administrator, registration, rawat";
$init ['service'] = "";
$init ['version'] = "4.9.8";
$init ['number'] = "91";
$init ['type'] = "";
$myplugins = new Plugin ( $init );
$PLUGINS [$init ['name']] = $myplugins;
?>