<?php 

function get_detail_assessment_rajal(){
    $list                   = array();
    $list['polianak']       = "Poli Anak"; 
    $list['polianestesi']   = "Poli Anestesi"; 
    $list['polibedah']      = "Poli Bedah Umum"; 
    $list['polibedahanak']  = "Poli Bedah Anak"; 
    $list['poliplastik']    = "Poli Bedah Plastik dan Rawat Luka"; 
    $list['poligigi']       = "Poli Gigi dan Mulut"; 
    $list['poligizi']       = "Poli Gizi"; 
    $list['polijantung']    = "Poli Jantung";
    $list['poliobstetri']   = "Poli Kandungan - Assesmen Obstetri"; 
    $list['poliginekologi'] = "Poli Kandungan - Assesmen Ginekologi"; 
    $list['polikulit']      = "Poli Kulit dan Kelamin"; 
    $list['polimata']       = "Poli Mata"; 
    $list['poliorthopedi']  = "Poli Orthopedi"; 
    $list['polipdp']        = "Poli PDP"; 
    $list['poliparu']       = "Poli Paru"; 
    $list['polipsikologi']  = "Poli Psikologi"; 
    $list['polipsikiatri']  = "Poli Jiwa (Psikiatri)"; 
    $list['polirawatluka']  = "Poli Rawat Luka"; 
    $list['polirehab']      = "Poli Rehab Medik / Fisiotherapy"; 
    $list['polisyaraf']     = "Poli Syaraf"; 
    $list['politbdots']     = "Poli TB Dots"; 
    $list['politht']        = "Poli THT"; 
    $list['poliurologi']    = "Poli Bedah Urologi"; 
    $list['polivct']        = "Poli VCT"; 
    $list['polidalam']      = "Poli Penyakit Dalam";
    return $list;
}

?>