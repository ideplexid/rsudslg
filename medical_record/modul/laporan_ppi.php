<?php

$tab = new Tabulator ( "laporan_ppi", "Laporan PPI", Tabulator::$LANDSCAPE_RIGHT);
$tab->add ( "laporan_ppi_harian_per_ruang", "Laporan Harian Per Ruang", "", Tabulator::$TYPE_HTML,"fa fa-file" );
$tab->add ( "laporan_ppi_total", "Laporan Total", "", Tabulator::$TYPE_HTML,"fa fa-file" );

$tab->setPartialLoad(true, "medical_record", "laporan_ppi", "laporan_ppi",true);
echo $tab->getHtml ();

?>