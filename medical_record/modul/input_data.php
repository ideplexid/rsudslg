<?php 
	$tabs=new Tabulator("input_data","input_data",Tabulator::$LANDSCAPE_RIGHT);
	$tabs->add("input_data_dasar_rs", "Data Dasar RS", "", Tabulator::$TYPE_HTML, "fa fa-building-o");
    $tabs->add("input_kamar_tidur","Kamar Tidur","",Tabulator::$TYPE_HTML,"fa fa-bed");
	$tabs->add("data_kunjungan", "Data Kunjungan", "", Tabulator::$TYPE_HTML, "fa fa-ticket");
	$tabs->add("diagnosa","Diagnosa","",Tabulator::$TYPE_HTML,"fa fa-plus-square");
	$tabs->add("input_operasi","Operasi","",Tabulator::$TYPE_HTML,"fa fa-bed");
	$tabs->add("tindakan","Tindakan","",Tabulator::$TYPE_HTML,"fa fa-heartbeat");
    if(getSettings($db, "smis-mr-tambah-laporan-igd") == '1' || getSettings($db, "smis-mr-tambah-laporan-igd") == 1) {
        $tabs->add("input_igd","IGD","",Tabulator::$TYPE_HTML,"fa fa-warning");
    }
    $tabs->add("input_infeksi", "Infeksi","",Tabulator::$TYPE_HTML,"fa fa-medkit");
    $tabs->add("input_jenis_perawatan", "Jenis Perawatan","",Tabulator::$TYPE_HTML,"fa fa-medkit");
    $tabs->setPartialLoad(true, "medical_record", "input_data", "input_data",true);
	echo $tabs->getHtml();
?>

