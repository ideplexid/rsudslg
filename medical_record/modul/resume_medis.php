<?php 
/**
 * this source code used for handling 
 * all patient history in Hospital
 * it's will call all registration of the patient 
 * in entire time. and also will show spesific treatment
 * drugs and etc that patient given by the hospital.
 * 
 * @author 		: Nurul Huda
 * @license 	: LGPLv3
 * @version 	: 1.0.0
 * @since 		: 24 Sept 2016
 * @copyright 	: goblooge@gmail.com
 * @database	: N/A
 * @service		: - get_patient
 * 				  - get_registered_all
 * 				  - get_patient
 * 				  - get_lab
 * 				  - get_riwayat_obat
 * 				  - get_rad
 * 				  - get_tindakan
 * */



global $db;
if(isset($_POST['command']) && $_POST['command']=="file_resume_medis"){
	$noreg = $_POST['noreg_pasien'];
	$query = "SELECT file_rm FROM smis_mr_diagnosa WHERE noreg_pasien = '".$noreg."' and file_rm!='' LIMIT 0,1";
	$file_rm = $db->get_var($query);
	$r = new ResponsePackage();
	$r->setStatus(ResponsePackage::$STATUS_OK);
	if($file_rm==null){
		$r->setAlertVisible(true);
		$r->setAlertContent("file tidak ditemukan","","alert-danger");
	}else{
		$r->setContent("smis-upload/".$file_rm);
	}	
	echo json_encode($r->getPackage());
	return;
}



 if(isset($_POST['command']) && $_POST['command']=="hasil"){
	$r = new ResponsePackage();
	$r->setContent("");
	$r->setStatus(ResponsePackage::$STATUS_OK);
	if(file_exists("smis-temp/Hasil_Lab_".$_POST['id'].".pdf")){
		$r->setContent("smis-temp/Hasil_Lab_".$_POST['id'].".pdf");
	}else{
		$r->setAlertContent("Belum Ada Hasil","Hasil Lab belum tersedia");
		$r->setAlertVisible(true);
	}
	echo json_encode($r->getPackage());
	return;
}
 
global $db;
require_once("smis-base/smis-include-service-consumer.php");
require_once("medical_record/class/adapter/LabAdapter.php");
require_once("medical_record/class/adapter/RadAdapter.php");

$head=array("NRM","Nama","L/P","Alamat");
$formtable=new Table($head);
$formtable->setName("pasien")
		  ->setModel(Table::$SELECT);
$adapter = new SimpleAdapter ();
$adapter->add ( "Nama", "nama" )
		->add ( "NRM", "id", "digit8" )
		->add ( "Alamat", "alamat" )
		->add ( "L/P", "kelamin", "trivial_0_L_P" );
$responder_pasien = new ServiceResponder ( $db, $formtable, $adapter, "get_patient" );

$head=array("Ruang","Tanggal","Dokter","Tindakan","ICD","Kode ICD");
$tindakantble=new Table($head,"",NULL,false);
$tindakantble->setName("tindakan")
		     ->setModel(Table::$NONE);
$adapter = new SimpleAdapter ();
$adapter->add ( "Ruang", "smis_entity" )
		->add ( "Dokter", "nama_dokter" )
		->add ( "Tindakan", "nama_tindakan" )
		->add ( "Tanggal", "tanggal","date d M Y H:i" )
		->add ( "ICD", "nama_icd" )
		->add ( "Kode ICD", "kode_icd" )
		->add ( "Tanggal", "waktu", "date d M Y" );
$responder_tindakan= new ServiceResponder ( $db, $tindakantble, $adapter, "get_tindakan" );
$responder_tindakan->setMode(ServiceConsumer::$JOIN_ENTITY);





$head=array("Tanggal","No","Dokter","Petugas","Konsultan","Hasil");
$labtble=new Table($head,"",NULL,false);
$labtble->setName("lab")
		->setModel(Table::$NONE);
$adapter = new LabAdapter ();
$adapter->add ( "Tanggal", "tanggal","date d M Y " )
		->add ( "No", "no_lab" )
		->add ( "Dokter", "nama_dokter" )
		->add ( "Petugas", "nama_petugas" )
		->add ( "konsultan", "nama_konsultan" );
$responder_lab= new ServiceResponder ( $db, $labtble, $adapter, "get_lab" );

$head=array("Tanggal","No","Dokter","Petugas","Konsultan","Hasil");
$radtble=new Table($head,"",NULL,false);
$radtble->setName("rad")->setModel(Table::$NONE);
$adapter = new RadAdapter ();
$adapter->add ( "Tanggal", "tanggal","date d M Y " )
		->add ( "No", "no_lab" )
		->add ( "Dokter", "nama_dokter" )
		->add ( "Petugas", "nama_petugas" )
		->add ( "konsultan", "nama_konsultan" );
$responder_rad= new ServiceResponder ( $db, $radtble, $adapter, "get_rad" );


$head=array("Tanggal","Dokter","Diagnosa Primer","Diagnosa Sekunder","Tensi","Nadi","Suhu","Respiration Rate","Gula Darah","Keadaan Umum","Keadaan Luka","Nyeri");
$diagnosatable=new Table($head,"",NULL,false);
$diagnosatable->setName("diagnosa")
			  ->setFooterVisible(false);
$adapter = new SimpleAdapter ();
$adapter->add ( "Tanggal", "tanggal","date d M Y" )
		->add ( "Dokter", "nama_dokter" )
		->add ( "Diagnosa Primer", "diagnosa" )
		->add ( "Diagnosa Sekunder", "keterangan" )
		->add ( "Tensi", "tensi" )
		->add ( "Nadi", "nadi" )
		->add ( "Suhu", "suhu" )
		->add ( "Respiration Rate", "rr" )
		->add ( "Nyeri", "nyeri" )
		->add ( "Keadaan Umum", "keadaan_umum" )
		->add ( "Keadaan Luka", "keadaan_luka" )
		->add ( "Gula Darah", "gula_darah" );
$dbtable=new DBTable($db, "smis_mr_diagnosa");
$dbtable->setShowAll(true);

if(isset($_POST['noreg_pasien']))
	$dbtable->addCustomKriteria("noreg_pasien", "='".$_POST['noreg_pasien']."'");
$responder_diagnosa = new DBResponder($dbtable, $diagnosatable, $adapter);


$head=array("Racikan","Obat","Jumlah");
$obatable=new Table($head,"",NULL,false);
$obatable->setName("obat")
			  ->setFooterVisible(false);
$adapter = new SimpleAdapter ();
$adapter->add ( "Racikan", "Racikan" )
		->add ( "Obat", "Obat" )
		->add ( "Jumlah", "Jumlah" );
$responder_obat= new ServiceResponder ( $db, $obatable, $adapter, "get_riwayat_obat" );
$responder_obat->setMode(ServiceConsumer::$JOIN_ENTITY);


$supercommand=new SuperCommand();
$supercommand->addResponder("pasien", $responder_pasien)
			 ->addResponder("diagnosa", $responder_diagnosa)
			 ->addResponder("tindakan", $responder_tindakan)
			 ->addResponder("rad", $responder_rad)
			 ->addResponder("obat", $responder_obat)
			 ->addResponder("lab", $responder_lab);
$superdata=$supercommand->initialize();
if($superdata!=null){
	echo $superdata;
	return;
}

$diagnosa=new Button("rsm_diagnosa", "rsm_diagnosa", "Diagnosa");
$diagnosa->setIsButton(Button::$ICONIC)
		->setClass("btn-primary")
		->setIcon("fa fa-book");
$tindakan=new Button("rsm_tindakan", "rsm_tindakan", "Tindakan");
$tindakan->setIsButton(Button::$ICONIC)
		 ->setClass("btn-primary")
		 ->setIcon("fa fa-stethoscope");
$lab=new Button("rsm_lab", "rsm_lab", "Laboratory");
$lab->setIsButton(Button::$ICONIC)
	->setClass("btn-primary")
	->setIcon("fa fa-eyedropper");
$rad=new Button("rsm_rad", "rsm_rad", "Radiology");
$rad->setIsButton(Button::$ICONIC)
	->setClass("btn-primary")
	->setIcon("fa fa-child");
$obat=new Button("rsm_obat", "rsm_obat", "Obat");
$obat->setIsButton(Button::$ICONIC)
	->setClass("btn-primary")
	->setIcon("fa fa-square");

$file_data=new Button("file_data", "file_data", "Hasil Scan RM");
$file_data->setIsButton(Button::$ICONIC)
	->setClass("btn-primary")
	->setIcon("fa fa-square");

$header=array("Kunjungan Ke","Tanggal","Nomor Register","Umur");
$uitable=new Table($header);
$uitable->setName("resume_medis")
		->setMaxContentButton(1, "Rekam Medis")
		->setEditButtonEnable(false)
		->setDelButtonEnable(false)
		->setAddButtonEnable(false)
		->addContentButton("file_data", $file_data)
		->addContentButton("diagnosa", $diagnosa)
		->addContentButton("tindakan", $tindakan)
		->addContentButton("rad", $rad)
		->addContentButton("lab", $lab)
		->addContentButton("obat", $obat);

if(isset($_POST['command'])){
	$radapter=new SimpleAdapter();
	$radapter->add("Kunjungan Ke", "no_kunjungan")
			 ->add("Tanggal", "tanggal","date d M Y")
			 ->add("Nomor Register", "id","digit8")
			 ->add("Kunjungan Ke", "no_kunjungan")
			 ->add("Umur", "umur");	
	$serv=new ServiceResponder ( $db, $uitable, $radapter, "get_registered_all" );
	$data=$serv->command($_POST['command']);
	echo json_encode($data);
	return;
}

$formtable->addModal("pasien", "chooser-resume_medis-pasien","Nama", "","y",null,true)
		  ->addModal("nrm", "text","NRM", "","y",null,true)
		  ->addModal("jk", "text","Jenis Kelamin", "","y",null,true)
		  ->addModal("alamat", "text","Alamat", "","y",null,true);
$form=$formtable->getModal()->getForm();
$form->setTitle("Pasien");

echo addJS ( "framework/smis/js/table_action.js" );
echo $form->getHtml();
echo $uitable->getHtml();

$modal=new Modal("lab_modal", "Hasil Lab", "Hasil Lab");
$modal->setModalSize(Modal::$HALF_MODEL);
echo $modal->getHtml();

$modal=new Modal("rad_modal", "Hasil Rad", "Hasil Rad");
$modal->setModalSize(Modal::$HALF_MODEL);
echo $modal->getHtml();
echo addJS("medical_record/resource/js/resume_medis.js",false);
echo addCSS("medical_record/resource/css/resume_medis.css",false);
?>