<?php

$tab = new Tabulator ( "10_besar_penyakit", "10_besar_penyakit", Tabulator::$LANDSCAPE_RIGHT);
$tab->add ( "10_besar_penyakit_rwt_jalan_non_igd", "10 Besar Penyakit Rawat Jalan Non IGD", "", Tabulator::$TYPE_HTML,"fa fa-medkit" );
$tab->add ( "10_besar_penyakit_rwt_inap", "10 Besar Penyakit Rawat Inap", "", Tabulator::$TYPE_HTML,"fa fa-medkit" );
$tab->add ( "10_besar_penyakit_igd", "10 Besar Penyakit IGD", "", Tabulator::$TYPE_HTML,"fa fa-medkit" );

$tab->setPartialLoad(true, "medical_record", "10_besar_penyakit", "10_besar_penyakit",true);
echo $tab->getHtml ();

?>