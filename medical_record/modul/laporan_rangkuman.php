<?php 

$tabs=new Tabulator("laporan_rangkuman","laporan_rangkuman",Tabulator::$LANDSCAPE_RIGHT);
$tabs->add("rl12","RL 1.02  - Indikator Pelayanan",Tabulator::$TYPE_HTML,"fa fa-file-o");
$tabs->add("rl13","RL 1.03  - Tempat Tidur",Tabulator::$TYPE_HTML,"fa fa-bed");
$tabs->add("rl2","RL 2.00  - Kepegawaian",Tabulator::$TYPE_HTML,"fa fa-user-md");
$tabs->add("rl32","RL 3.02  - Rawat Darurat",Tabulator::$TYPE_HTML,"fa fa-file-o");
$tabs->add("rl33","RL 3.03  - Gigi & Mulut",Tabulator::$TYPE_HTML,"fa fa-file-o");
$tabs->add("rl310","RL 3.10  - Laporan Pelayanan Khusus",Tabulator::$TYPE_HTML,"fa fa-file-o");
$tabs->add("rl311","RL 3.11  - Laporan Kesehatan Jiwa",Tabulator::$TYPE_HTML,"fa fa-file-o");
$tabs->add("rl314","RL 3.14  - Rujukan",Tabulator::$TYPE_HTML,"fa fa-file-o");
$tabs->add("stp_uri", "RL 4A - Surveilance Terpadu Rawat Inap", "", Tabulator::$TYPE_HTML,"fa fa-bed" );
$tabs->add("stp_urj", "RL 4B - Surveilance Terpadu Rawat Jalan ", "", Tabulator::$TYPE_HTML,"fa fa-wheelchair" );
$tabs->add("rl51","RL 5.01  - Pengunjung",Tabulator::$TYPE_HTML,"fa fa-bar-chart");
$tabs->add("rl52","RL 5.02  - Pengunjung Rawat Jalan",Tabulator::$TYPE_HTML,"fa fa-bar-chart");
$tabs->add("rl52b","RL 5.02B - Jenis Kegiatan",Tabulator::$TYPE_HTML,"fa fa-bar-chart");
$tabs->add("rl53","RL 5.03B - Rank Penyakit Baru RI",Tabulator::$TYPE_HTML,"fa fa-bar-chart");
$tabs->add("rl54","RL 5.04B - Rank Penyakit Baru RJ",Tabulator::$TYPE_HTML,"fa fa-bar-chart");
$tabs->add("rl53_lama","RL 5.03L - Rank Penyakit Lama RI",Tabulator::$TYPE_HTML,"fa fa-bar-chart");
$tabs->add("rl54_lama","RL 5.04L - Rank Penyakit Lama RJ",Tabulator::$TYPE_HTML,"fa fa-bar-chart");


?>