<?php
    global $db;
    $tab = new Tabulator ( "laporan_pasien", "laporan_pasien", Tabulator::$LANDSCAPE_RIGHT );
    $tab->add ( "lap_rj", "Rangkuman R. Jalan", "", Tabulator::$TYPE_HTML," fa fa-users" );
    $tab->add ( "lap_ri", "Rangkuman R. Inap", "", Tabulator::$TYPE_HTML," fa fa-bed" );
    $tab->add ( "lap_rawat_jalan", "Laporan R. Jalan", "", Tabulator::$TYPE_HTML," fa fa-users" );
    $tab->add ( "lap_rawat_inap", "Laporan R. Inap", "", Tabulator::$TYPE_HTML," fa fa-bed" );
    $tab->add ( "lap_pulang", "Laporan Pasien Pulang", "", Tabulator::$TYPE_HTML," fa fa-sign-out" );
    $tab->add ( "lap_resume_pasien_pulang_ri", "Rangkuman Pasien Pulang", "", Tabulator::$TYPE_HTML," fa fa-file-excel-o" );
    $tab->add ( "lap_kunjungan", "Lap. Kunjungan", "", Tabulator::$TYPE_HTML," fa fa-sign-in" );
    $tab->add ( "lap_kun_pertanggal", "Lap. Kunjungan/Tanggal", "", Tabulator::$TYPE_HTML," fa fa-bar-chart-o" );
    //$tab->add ( "lap_kunjungan_rj", "Lap. Kunjungan RJ", "", Tabulator::$TYPE_HTML," fa fa-sign-in" );
    //$tab->add ( "lap_kunjungan_rawat", "Lap. Kunjungan Rawat", "", Tabulator::$TYPE_HTML," fa fa-sign-in" );
    //$tab->add ( "lap_kunjungan_rawat_jalan", "Lap. Kunjungan Rawat Jalan", "", Tabulator::$TYPE_HTML," fa fa-sign-in" );
    $tab->add ( "lap_kunj_rawat_jalan_lk_pr", "Lap. Kunjungan Rawat Jalan", "", Tabulator::$TYPE_HTML," fa fa-sign-in" );
    
    
    if(getSettings($db,"smis-mr-lap-pasien-tampil-sensus-range","0")=="1") {
        $tab->add ( "lap_sensus", "Laporan Sensus", "", Tabulator::$TYPE_HTML," fa fa-list-alt" );
    }
    
    $tab->add ( "lap_diagnosa", "Lap. Diagnosa", "", Tabulator::$TYPE_HTML," fa fa-user-md" );
    $tab->add ( "lap_diagnosa_per_bulan", "Lap. Diagnosa Per Bulan", "", Tabulator::$TYPE_HTML," fa fa-user-md" );
    $tab->add ( "lap_diagnosa_detail_pertanggal", "Lap. Diagnosa/Tanggal", "", Tabulator::$TYPE_HTML," fa fa-calendar" );
    $tab->add ( "lap_persen_diagnosa", "Lap. Persentase Diagnosa", "", Tabulator::$TYPE_HTML," fa fa-percent" );
    $tab->add ( "lap_prolanis", "Lap. Prolanis", "", Tabulator::$TYPE_HTML," fa fa-list-alt" );

    $tab->add ( "pasien_aktif", "Pasien Aktif", "", Tabulator::$TYPE_HTML," fa fa-user" );
    $tab->add ( "lap_resume_ruangan", "Resume Ruangan", "", Tabulator::$TYPE_HTML," fa fa-file" );
    $tab->add ( "morbiditas", "Morbiditas", "", Tabulator::$TYPE_HTML," fa fa-user-times" );
    if(getSettings($db,"smis-mr-lap-pasien-tampil-morbiditas-grup","0")=="1") {
        $tab->add ( "morbiditas_grup", "Morbiditas Grup", "", Tabulator::$TYPE_HTML," fa fa-user-times" );
    }
    $tab->add ( "odontogram", "Odontogram", "", Tabulator::$TYPE_HTML," fa fa-user-md" );

    $tab->add ( "lap_lama_rawat", "Lap. Lama Rawat", "", Tabulator::$TYPE_HTML," fa fa-bed" );
    $tab->add ( "lap_plebitis", "Lap. Plebitis", "", Tabulator::$TYPE_HTML,"fa fa-eyedropper" );
    $tab->add ( "lap_cauti", "Lap. Cauti", "", Tabulator::$TYPE_HTML,"fa fa-file-o" );
    $tab->add ( "lap_kasus", "Lap. Kasus", "", Tabulator::$TYPE_HTML,"fa fa-file-o" );
    $tab->add ( "lap_kegiatan_gigi_mulut", "Lap. Kegiatan Gigi Mulut", "", Tabulator::$TYPE_HTML,"fa fa-list-alt" );

    //$tab->add ( "", "", "", Tabulator::$TYPE_HTML,"" );
    $tab->setPartialLoad(true, "medical_record", "laporan_pasien", "laporan_pasien",true);	
    echo $tab->getHtml ();
?>