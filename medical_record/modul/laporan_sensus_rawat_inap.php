<?php
    require_once "smis-base/smis-include-service-consumer.php";
	global $db;

    if (isset($_POST['command'])) {
    	if ($_POST['command'] == "view_pasien_masuk") {
    		$kelas = getSettings($db, "smis-rs-kelas-" . $_POST['ruangan'], "");
    		$rows = $db->get_result("
    			SELECT *
    			FROM smis_rwt_antrian_" . $_POST['ruangan'] . "
    			WHERE prop = '' AND DATE(waktu) = '" . $_POST['tanggal'] . "' AND (
    				asal LIKE 'POLI%' OR asal LIKE 'IGD' OR asal LIKE 'Pendaftaran'
    			)
    		");
    		$html = "";
    		$jumlah = 0;
    		if ($rows != null) {
    			$nomor = 1;
    			foreach ($rows as $row) {
    				$html .= "
    					<tr>
    						<td>" . $nomor++ . "</td>
    						<td>" . $row->nama_pasien . "</td>
    						<td>" . $row->nrm_pasien . "</td>
    						<td>" . ArrayAdapter::format("unslug", $kelas) . "</td>
    					</tr>
    				";
    				$jumlah++;
    			}
    		}
    		if ($html == "")
    			$html = "<tr><td colspan='4'><center>Tidak Ada Data</center></td></tr>";
    		$data['html'] = $html;
    		$data['jumlah'] = $jumlah;
    		echo json_encode($data);
    	} else if ($_POST['command'] == "view_pasien_pindahan") {
    		$rows = $db->get_result("
    			SELECT *
    			FROM smis_rwt_antrian_" . $_POST['ruangan'] . "
    			WHERE prop = '' AND DATE(waktu) = '" . $_POST['tanggal'] . "' AND (
    				asal LIKE 'RUANG%'
    			)
    		");
    		$html = "";
    		$jumlah = 0;
    		if ($rows != null) {
    			$nomor = 1;
    			foreach ($rows as $row) {
    				$kelas = getSettings($db, "smis-rs-kelas-" . ArrayAdapter::format("slug", $row->asal), "");
    				$html .= "
    					<tr>
    						<td>" . $nomor++ . "</td>
    						<td>" . $row->nama_pasien . "</td>
    						<td>" . $row->nrm_pasien . "</td>
    						<td>" . ArrayAdapter::format("unslug", $kelas) . "</td>
    						<td>" . ArrayAdapter::format("unslug", $row->asal) . "</td>
    					</tr>
    				";
    				$jumlah++;
    			}
    		}
    		if ($html == "")
    			$html = "<tr><td colspan='5'><center>Tidak Ada Data</center></td></tr>";
    		$data['html'] = $html;
    		$data['jumlah'] = $jumlah;
    		echo json_encode($data);
    	} else if ($_POST['command'] == "view_pasien_dipindah") {
    		$rows = $db->get_result("
    			SELECT *
    			FROM smis_rwt_antrian_" . $_POST['ruangan'] . "
    			WHERE prop = '' AND DATE(waktu_keluar) = '" . $_POST['tanggal'] . "' AND (
    				ruang_tujuan NOT LIKE '' AND cara_keluar LIKE 'Pindah Kamar'
    			)
    		");
    		$html = "";
    		$jumlah = 0;
    		if ($rows != null) {
    			$nomor = 1;
    			foreach ($rows as $row) {
                    $kelas = getSettings($db, "smis-rs-kelas-" . ArrayAdapter::format("slug", $row->ruang_tujuan), "");
    				$html .= "
    					<tr>
    						<td>" . $nomor++ . "</td>
    						<td>" . $row->nama_pasien . "</td>
    						<td>" . $row->nrm_pasien . "</td>
    						<td>" . ArrayAdapter::format("unslug", $kelas) . "</td>
                            <td>" . ArrayAdapter::format("unslug", $row->ruang_tujuan) . "</td>
    					</tr>
    				";
    				$jumlah++;
    			}
    		}
    		if ($html == "")
    			$html = "<tr><td colspan='5'><center>Tidak Ada Data</center></td></tr>";
    		$data['html'] = $html;
    		$data['jumlah'] = $jumlah;
    		echo json_encode($data);
    	} else if ($_POST['command'] == "view_pasien_pulang") {
    		$kelas = getSettings($db, "smis-rs-kelas-" . $_POST['ruangan'], "");
    		$rows = $db->get_result("
    			SELECT *
    			FROM smis_rwt_antrian_" . $_POST['ruangan'] . "
    			WHERE prop = '' AND DATE(waktu_keluar) = '" . $_POST['tanggal'] . "' AND cara_keluar NOT LIKE 'Tidak Datang' AND cara_keluar NOT LIKE 'Pindah Kamar'
    		");
    		$html = "";
            $jumlah_diizinkan_pulang = 0;
            $jumlah_dirujuk = 0;
            $jumlah_pindah_ke_rs_lain = 0;
            $jumlah_pulang_paksa = 0;
            $jumlah_lari = 0;
            $jumlah_mati_k48 = 0;
            $jumlah_mati_l48 = 0;
    		if ($rows != null) {
    			$nomor = 1;
    			foreach ($rows as $row) {
    				$diizinkan_pulang = "";
    				$dirujuk = "";
    				$pindah_ke_rs_lain = "";
    				$pulang_paksa = "";
    				$lari = "";
    				$mati_k48 = "";
    				$mari_l48 = "";
    				$los = "";

    				if ($row->waktu_keluar != "0000-00-00 00:00:00") {
    					if ($row->cara_keluar == "Dipulangkan Hidup") {
    						$diizinkan_pulang = ArrayAdapter::format("date d-m-Y", $row->waktu_keluar);
    						$jumlah_diizinkan_pulang++;
    					} else if ($row->cara_keluar == "Dirujuk") {
    						$dirujuk = ArrayAdapter::format("date d-m-Y", $row->waktu_keluar);
    						$jumlah_dirujuk++;
    					} else if ($row->cara_keluar == "Pindah ke RS Lain") {
    						$pindah_ke_rs_lain = ArrayAdapter::format("date d-m-Y", $row->waktu_keluar);
    						$jumlah_pindah_ke_rs_lain++;
    					} else if ($row->cara_keluar == "Pulang Paksa") {
    						$pulang_paksa = ArrayAdapter::format("date d-m-Y", $row->waktu_keluar);
    						$jumlah_pulang_paksa++;
    					} else if ($row->cara_keluar == "Kabur") {
    						$lari = ArrayAdapter::format("date d-m-Y", $row->waktu_keluar);
    						$jumlah_lari++;
    					} else if ($row->cara_keluar == "Dipulangkan Mati < 1 Jam Post Operasi" 
    						  || $row->cara_keluar == "Dipulangkan Mati <= 48 Jam" 
    						  || $row->cara_keluar == "Dipulangkan Mati <= 24 Jam") {
    						$mati_k48 = ArrayAdapter::format("date d-m-Y", $row->waktu_keluar);
    						$jumlah_mati_k48++;
    					} else if ($row->cara_keluar == "Dipulangkan Mati > 48 Jam") {
    						$mati_l48 = ArrayAdapter::format("date d-m-Y", $row->waktu_keluar);
    						$jumlah_mati_l48++;
    					}
    					$tanggal_mulai = strtotime(ArrayAdapter::format("date Y-m-d", $row->waktu));
			            $tanggal_selesai = strtotime(ArrayAdapter::format("date Y-m-d", $row->waktu_keluar));
			            $datediff = $tanggal_selesai - $tanggal_mulai;
    					$los = ceil($datediff / (60 * 60 * 24));
			            if ($los < 1)
			            	$los = 1;
    				}

    				$dpjp = "";
    				$reg_row = $db->get_row("
    					SELECT id_dokter, nama_dokter
    					FROM smis_rg_layananpasien
    					WHERE id = '" . $row->no_register . "'
    				");
    				if ($reg_row != null)
    					$dpjp = $reg_row->nama_dokter;

    				$html .= "
    					<tr>
    						<td>" . $nomor++ . "</td>
    						<td>" . $row->nama_pasien . "</td>
    						<td>" . $row->nrm_pasien . "</td>
    						<td>" . ArrayAdapter::format("unslug", $kelas) . "</td>
    						<td>" . ArrayAdapter::format("date d-m-Y", $row->waktu) . "</td>
    						<td>" . $diizinkan_pulang . "</td>
    						<td>" . $dirujuk . "</td>
    						<td>" . $pindah_ke_rs_lain . "</td>
    						<td>" . $pulang_paksa . "</td>
    						<td>" . $lari . "</td>
    						<td>" . $mati_k48 . "</td>
    						<td>" . $mati_l48 . "</td>
    						<td>" . $los . "</td>
    						<td>" . $dpjp . "</td>
    						<td>" . ArrayAdapter::format("unslug", $row->carabayar) . "</td>
    					</tr>
    				";
    			}
    		}
    		if ($html == "")
    			$html = "<tr><td colspan='15'><center>Tidak Ada Data</center></td></tr>";
    		$data['html'] = $html;
    		$data['jumlah_diizinkan_pulang'] = $jumlah_diizinkan_pulang;
    		$data['jumlah_dirujuk'] = $jumlah_dirujuk;
    		$data['jumlah_pindah_ke_rs_lain'] = $jumlah_pindah_ke_rs_lain;
    		$data['jumlah_pulang_paksa'] = $jumlah_pulang_paksa;
    		$data['jumlah_lari'] = $jumlah_lari;
    		$data['jumlah_mati_k48'] = $jumlah_mati_k48;
    		$data['jumlah_mati_l48'] = $jumlah_mati_l48;
    		echo json_encode($data);
    	} else if ($_POST['command'] == "view_pasien_awal") {
    		$kelas = getSettings($db, "smis-rs-kelas-" . $_POST['ruangan'], "");
    		$rows = $db->get_result("
    			SELECT *
    			FROM smis_rwt_antrian_" . $_POST['ruangan'] . "
    			WHERE prop = '' AND (
    				(DATE(waktu) < '" . $_POST['tanggal'] . "' AND waktu_keluar = '0000-00-00 00:00:00') OR 
    				(DATE(waktu_keluar) = '" . $_POST['tanggal'] . "')
    			)
    		");
    		$html = "";
    		if ($rows != null) {
    			$nomor = 1;
    			$jumlah = 0;
    			foreach ($rows as $row) {
    				$diagnosa_row = $db->get_row("
    					SELECT diagnosa, nama_dokter
    					FROM smis_mr_diagnosa
    					WHERE prop = '' AND noreg_pasien = '" . $row->no_register . "'
    					ORDER BY id DESC
    					LIMIT 0, 1
    				");

    				$diagnosa = "";
    				$dokter = "";
    				if ($diagnosa_row != null) {
    					$diagnosa = $diagnosa_row->diagnosa;
    					$dokter = $diagnosa_row->nama_dokter;
    				}

    				$nama_pasien = $row->nama_pasien;
                    $pasien_pulang = "n";
    				if ($row->waktu_keluar != "0000-00-00 00:00:00") {
    					$nama_pasien = "<i>" . $row->nama_pasien . "</i>";
                        $pasien_pulang = "y";
                    }

    				$html .= "
    					<tr>
    						<td>" . $nomor++ . "</td>
    						<td>" . $nama_pasien . "</td>
    						<td>" . $row->nrm_pasien . "</td>
    						<td>" . ArrayAdapter::format("unslug", $kelas) . "</td>
    						<td>" . $diagnosa . "</td>
    						<td>" . $dokter . "</td>
                            <td style='display: none;'>" . $pasien_pulang . "</td>
    					</tr>
    				";
    				$jumlah++;
    			}
    		}
    		if ($html == "")
    			$html = "<tr><td colspan='6'><center>Tidak Ada Data</center></td></tr>";
    		$data['html'] = $html;
    		$data['jumlah'] = $jumlah;
    		echo json_encode($data);
    	} else if ($_POST['command'] == "view_jumlah_tempat_tidur") {
            $tahun = explode("-", $_POST['tanggal'])[0];
            $ruangan_rows = $db->get_result("
                SELECT *
                FROM smis_mr_kamar_tidur 
                WHERE prop = '' AND tahun = '" . $tahun . "'
            ");
            $info_bed_ruangan = array();
            $total = 0;
            if ($ruangan_rows != null) {
                foreach ($ruangan_rows as $rr) {
                    $ruangan = $rr->nama_ruangan;
                    $kelas = "VVIP";
                    $jumlah_bed = $rr->jumlah_vvip;
                    if ($jumlah_bed > 0) {
                        $info_bed_ruangan[] = array(
                            'ruangan'       => $ruangan,
                            'kelas'         => $kelas,
                            'jumlah_bed'    => $jumlah_bed
                        );
                        $total += $jumlah_bed;
                    }
                    $ruangan = $rr->nama_ruangan;
                    $kelas = "VIP";
                    $jumlah_bed = $rr->jumlah_vip;
                    if ($jumlah_bed > 0) {
                        $info_bed_ruangan[] = array(
                            'ruangan'       => $ruangan,
                            'kelas'         => $kelas,
                            'jumlah_bed'    => $jumlah_bed
                        );
                        $total += $jumlah_bed;
                    }
                    $ruangan = $rr->nama_ruangan;
                    $kelas = "Kelas I";
                    $jumlah_bed = $rr->jumlah_kelas_i;
                    if ($jumlah_bed > 0) {
                        $info_bed_ruangan[] = array(
                            'ruangan'       => $ruangan,
                            'kelas'         => $kelas,
                            'jumlah_bed'    => $jumlah_bed
                        );
                        $total += $jumlah_bed;
                    }
                    $ruangan = $rr->nama_ruangan;
                    $kelas = "Kelas II";
                    $jumlah_bed = $rr->jumlah_kelas_ii;
                    if ($jumlah_bed > 0) {
                        $info_bed_ruangan[] = array(
                            'ruangan'       => $ruangan,
                            'kelas'         => $kelas,
                            'jumlah_bed'    => $jumlah_bed
                        );
                        $total += $jumlah_bed;
                    }
                    $ruangan = $rr->nama_ruangan;
                    $kelas = "Kelas III";
                    $jumlah_bed = $rr->jumlah_kelas_iii;
                    if ($jumlah_bed > 0) {
                        $info_bed_ruangan[] = array(
                            'ruangan'       => $ruangan,
                            'kelas'         => $kelas,
                            'jumlah_bed'    => $jumlah_bed
                        );
                        $total += $jumlah_bed;
                    }
                    $ruangan = $rr->nama_ruangan;
                    $kelas = "Kelas Khusus";
                    $jumlah_bed = $rr->jumlah_kelas_khusus;
                    if ($jumlah_bed > 0) {
                        $info_bed_ruangan[] = array(
                            'ruangan'       => $ruangan,
                            'kelas'         => $kelas,
                            'jumlah_bed'    => $jumlah_bed
                        );
                        $total += $jumlah_bed;
                    }
                }
            }

            $html = "";
            if (count($info_bed_ruangan) > 0) {
                $temp = array_column($info_bed_ruangan, 'kelas');
                array_multisort($temp, SORT_ASC, $info_bed_ruangan);

                $nomor = 1;
                foreach ($info_bed_ruangan as $ibr) {
                    $html .= "
                        <tr>
                            <td>" . $nomor++ . "</td>
                            <td>" . $ibr['kelas'] . "</td>
                            <td>" . $ibr['ruangan'] . "</td>
                            <td>" . ArrayAdapter::format("number", $ibr['jumlah_bed']) . "</td>
                        </tr>
                    ";
                }
                $html .= "
                    <tr>
                        <td colspan='3'><strong>Total Jumlah Tempat Tidur</strong></td>
                        <td><strong>" . ArrayAdapter::format("number", $total) . "</strong></td>
                    </tr>
                ";
            }
    		$data['html'] = $html;
    		echo json_encode($data);
    	} else if ($_POST['command'] == "export_xls") {
            $tanggal = $_POST['tanggal'];
            $hari = $_POST['hari'];
            $ruangan = $_POST['ruangan'];
            $ruangan_label = $_POST['ruangan_label'];

            $data_pasien_masuk = json_decode($_POST['data_pasien_masuk'], true);
            $data_pasien_pindahan = json_decode($_POST['data_pasien_pindahan'], true);
            $data_pasien_dipindahkan = json_decode($_POST['data_pasien_dipindahkan'], true);
            $data_pasien_keluar = json_decode($_POST['data_pasien_keluar'], true);
            $data_pasien_dirawat = json_decode($_POST['data_pasien_dirawat'], true);
            $data_bed = json_decode($_POST['data_bed'], true);

            require_once ("smis-libs-out/php-excel/PHPExcel.php");

            $file = new PHPExcel();
            $file->getProperties()->setCreator("PT. Inovasi Ide Utama");
            $file->getProperties()->setTitle("Laporan Sensus Harian Rawat Inap");
            $file->getProperties()->setSubject("Laporan Sensus Harian Rawat Inap");
            $file->getProperties()->setDescription("Laporan Sensus Harian Rawat Inap Generated by System");
            $file->getProperties()->setKeywords("Laporan Sensus Harian Rawat Inap");
            $file->getProperties()->setCategory("Laporan Sensus Harian Rawat Inap");

            $sheet = $file->getActiveSheet();
            $sheet->setTitle("Sensus Harian Rawat Inap");
            $i = 1;

            $sheet->mergeCells("A" . $i . ":S" . $i)->setCellValue("A" . $i, "Laporan Sensus Harian Rawat Inap");
            $sheet->getStyle("A" . $i . ":S" . $i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $sheet->getStyle("A" . $i)->getFont()->setBold(true);
            $i = $i + 1;
            $sheet->mergeCells("A" . $i . ":S" . $i)->setCellValue("A" . $i, "");
            $i = $i + 1;
            $sheet->setCellValue("A" . $i, "Hari :");
            $sheet->setCellValue("B" . $i, $hari);
            $sheet->setCellValue("D" . $i, "Tanggal :");
            $sheet->mergeCells("E" . $i . ":F" . $i)->setCellValue("E" . $i, ArrayAdapter::format("date d-m-Y", $tanggal));
            $sheet->setCellValue("H" . $i, "Ruang :");
            $sheet->setCellValue("I" . $i, $ruangan_label);
            $i = $i + 1;

            $border_start = $i;

            $sheet->mergeCells("A" . $i . ":I" . $i)->setCellValue("A" . $i, "PASIEN MASUK RUANG RAWAT INAP");
            $sheet->getStyle("A" . $i . ":I" . $i)->getFill()->applyFromArray(array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'startcolor' => array(
                    'rgb' => 'E0E0E0'
                )
            ));
            $sheet->getStyle("A" . $i . ":S" . $i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $sheet->getStyle("A" . $i)->getFont()->setBold(true);
            $i = $i + 1;
            $sheet->mergeCells("A" . $i . ":E" . $i)->setCellValue("A" . $i, "PASIEN MASUK");
            $sheet->getStyle("A" . $i . ":E" . $i)->getFill()->applyFromArray(array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'startcolor' => array(
                    'rgb' => 'E0E0E0'
                )
            ));
            $sheet->getStyle("A" . $i . ":E" . $i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $sheet->getStyle("A" . $i)->getFont()->setBold(true);
            $sheet->mergeCells("F" . $i . ":I" . $i)->setCellValue("F" . $i, "PASIEN PINDAHAN DARI RUANG LAIN");
            $sheet->getStyle("F" . $i . ":I" . $i)->getFill()->applyFromArray(array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'startcolor' => array(
                    'rgb' => 'E0E0E0'
                )
            ));
            $sheet->getStyle("F" . $i . ":I" . $i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $sheet->getStyle("F" . $i)->getFont()->setBold(true);
            $i = $i + 1;
            $sheet->setCellValue("A" . $i, "No.");
            $sheet->setCellValue("B" . $i, "Nama Pasien");
            $sheet->setCellValue("C" . $i, "Nomor RM");
            $sheet->mergeCells("D" . $i . ":E" . $i)->setCellValue("D" . $i, "Kelas");
            $sheet->setCellValue("F" . $i, "Nama Pasien");
            $sheet->setCellValue("G" . $i, "Nomor RM");
            $sheet->setCellValue("H" . $i, "Kelas");
            $sheet->setCellValue("I" . $i, "Dari Ruang/Kelas");
            $i = $i + 1;
            $sheet->setCellValue("A" . $i, "1");
            $sheet->setCellValue("B" . $i, "2");
            $sheet->setCellValue("C" . $i, "3");
            $sheet->mergeCells("D" . $i . ":E" . $i)->setCellValue("D" . $i, "4");
            $sheet->setCellValue("F" . $i, "5");
            $sheet->setCellValue("G" . $i, "6");
            $sheet->setCellValue("H" . $i, "7");
            $sheet->setCellValue("I" . $i, "8");
            $sheet->getStyle("A" . ($i - 1) . ":I" . $i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $sheet->getStyle("A" . ($i - 1) . ":I" . $i)->getFont()->setBold(true);
            $i_masuk = $i + 1;
            $i_pindahan = $i + 1;
            if (count($data_pasien_masuk) > 0) {
                foreach ($data_pasien_masuk as $dpm) {
                    $sheet->setCellValue("A" . $i_masuk, $dpm['nomor']);
                    $sheet->setCellValue("B" . $i_masuk, $dpm['nama_pasien']);
                    $sheet->setCellValue("C" . $i_masuk, $dpm['nrm_pasien']);
                    $sheet->mergeCells("D" . $i_masuk . ":E" . $i_masuk)->setCellValue("D" . $i_masuk, $dpm['kelas']);
                    $i_masuk = $i_masuk + 1;
                }
                if ($i_masuk > 0)
                    $i_masuk = $i_masuk - 1;
            }
            if (count($data_pasien_pindahan) > 0) {
                foreach ($data_pasien_pindahan as $dpp) {
                    if ($dpp['nomor'] > 0)
                        $sheet->setCellValue("A" . $i_pindahan, $dpp['nomor']);
                    $sheet->setCellValue("F" . $i_pindahan, $dpp['nama_pasien']);
                    $sheet->setCellValue("G" . $i_pindahan, $dpp['nrm_pasien']);
                    $sheet->setCellValue("H" . $i_pindahan, $dpp['kelas']);
                    $sheet->setCellValue("I" . $i_pindahan, $dpp['ruang']);
                    $i_pindahan = $i_pindahan + 1;
                }
                if ($i_pindahan > 0)
                    $i_pindahan = $i_pindahan - 1;
            }
            if ($i_masuk > $i_pindahan)
                $i = $i_masuk;
            else
                $i = $i_pindahan;
            $thin = array();
            $thin['borders'] = array();
            $thin['borders']['allborders'] = array();
            $thin['borders']['allborders']['style'] = PHPExcel_Style_Border::BORDER_THIN;
            $sheet->getStyle("A" . $border_start . ":I" . $i)->applyFromArray($thin);

            $i = $i + 2;
            $border_start = $i;
            $sheet->mergeCells("A" . $i . ":S" . $i)->setCellValue("A" . $i, "PASIEN KELUAR RUANGAN");
            $sheet->getStyle("A" . $i . ":S" . $i)->getFill()->applyFromArray(array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'startcolor' => array(
                    'rgb' => 'E0E0E0'
                )
            ));
            $sheet->getStyle("A" . $i . ":S" . $i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $sheet->getStyle("A" . $i . ":S" . $i)->getFont()->setBold(true);
            $i = $i + 1;
            $sheet->mergeCells("A" . $i . ":E" . $i)->setCellValue("A" . $i, "PASIEN DIPINDAHKAN KE RUANG LAIN");
            $sheet->getStyle("A" . $i . ":E" . $i)->getFill()->applyFromArray(array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'startcolor' => array(
                    'rgb' => 'E0E0E0'
                )
            ));
            $sheet->mergeCells("F" . $i . ":S" . $i)->setCellValue("F" . $i, "PASIEN KELUAR RUMAH SAKIT");
            $sheet->getStyle("F" . $i . ":I" . $i)->getFill()->applyFromArray(array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'startcolor' => array(
                    'rgb' => 'E0E0E0'
                )
            ));
            $sheet->getStyle("A" . $i . ":S" . $i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $sheet->getStyle("A" . $i . ":S" . $i)->getFont()->setBold(true);
            $i = $i + 1;
            $sheet->mergeCells("A" . $i . ":A" . ($i + 2))->setCellValue("A" . $i, "No.");
            $sheet->mergeCells("B" . $i . ":B" . ($i + 2))->setCellValue("B" . $i, "Nama Pasien");
            $sheet->mergeCells("C" . $i . ":C" . ($i + 2))->setCellValue("C" . $i, "Nomor RM");
            $sheet->mergeCells("D" . $i . ":E" . $i)->setCellValue("D" . $i, "Tujuan");
            $sheet->mergeCells("D" . ($i + 1) . ":D" . ($i + 2))->setCellValue("D" . ($i + 1), "Kelas");
            $sheet->mergeCells("E" . ($i + 1) . ":E" . ($i + 2))->setCellValue("E" . ($i + 1), "Ruang");
            $sheet->mergeCells("F" . $i . ":F" . ($i + 2))->setCellValue("F" . $i, "Nama Pasien");
            $sheet->mergeCells("G" . $i . ":G" . ($i + 2))->setCellValue("G" . $i, "Nomor RM");
            $sheet->mergeCells("H" . $i . ":H" . ($i + 2))->setCellValue("H" . $i, "Kelas");
            $sheet->mergeCells("I" . $i . ":I" . ($i + 2))->setCellValue("I" . $i, "Tanggal Masuk RS");
            $sheet->mergeCells("J" . $i . ":P" . $i)->setCellValue("J" . $i, "Cara Keluar Pasien");
            $sheet->mergeCells("J" . ($i + 1) . ":J" . ($i + 2))->setCellValue("J" . ($i + 1), "Diizinkan Pulang");
            $sheet->mergeCells("K" . ($i + 1) . ":K" . ($i + 2))->setCellValue("K" . ($i + 1), "Dirujuk");
            $sheet->mergeCells("L" . ($i + 1) . ":L" . ($i + 2))->setCellValue("L" . ($i + 1), "Pindah ke RS Lain");
            $sheet->mergeCells("M" . ($i + 1) . ":M" . ($i + 2))->setCellValue("M" . ($i + 1), "Pulang Paksa");
            $sheet->mergeCells("N" . ($i + 1) . ":N" . ($i + 2))->setCellValue("N" . ($i + 1), "Lari");
            $sheet->mergeCells("O" . ($i + 1) . ":P" . ($i + 1))->setCellValue("O" . ($i + 1), "Mati");
            $sheet->setCellValue("O" . ($i + 2), "< 48 Jam");
            $sheet->setCellValue("P" . ($i + 2), "≤ 48 Jam");
            $sheet->mergeCells("Q" . $i . ":Q" . ($i + 2))->setCellValue("Q" . $i, "Lama Dirawat");
            $sheet->mergeCells("R" . $i . ":R" . ($i + 2))->setCellValue("R" . $i, "DPJP");
            $sheet->mergeCells("S" . $i . ":S" . ($i + 2))->setCellValue("S" . $i, "Cara Pembayaran");
            $sheet->setCellValue("A" . ($i + 3), "1");
            $sheet->setCellValue("B" . ($i + 3), "2");
            $sheet->setCellValue("C" . ($i + 3), "3");
            $sheet->setCellValue("D" . ($i + 3), "4");
            $sheet->setCellValue("E" . ($i + 3), "5");
            $sheet->setCellValue("F" . ($i + 3), "6");
            $sheet->setCellValue("G" . ($i + 3), "7");
            $sheet->setCellValue("H" . ($i + 3), "8");
            $sheet->setCellValue("I" . ($i + 3), "9");
            $sheet->setCellValue("J" . ($i + 3), "10");
            $sheet->setCellValue("K" . ($i + 3), "11");
            $sheet->setCellValue("L" . ($i + 3), "12");
            $sheet->setCellValue("M" . ($i + 3), "13");
            $sheet->setCellValue("N" . ($i + 3), "14");
            $sheet->setCellValue("O" . ($i + 3), "15");
            $sheet->setCellValue("P" . ($i + 3), "16");
            $sheet->setCellValue("Q" . ($i + 3), "17");
            $sheet->setCellValue("R" . ($i + 3), "18");
            $sheet->setCellValue("S" . ($i + 3), "19");
            $sheet->getStyle("A" . $i . ":S" . ($i + 3))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $sheet->getStyle("A" . $i . ":S" . ($i + 3))->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $sheet->getStyle("A" . $i . ":S" . ($i + 3))->getFont()->setBold(true);
            $i = $i + 3;
            $i_dipindahkan = $i;
            $i_keluar = $i;
            if (count($data_pasien_dipindahkan) > 0) {
                foreach ($data_pasien_dipindahkan as $dpd) {
                    $i_dipindahkan = $i_dipindahkan + 1;
                    $sheet->setCellValue("A" . $i_dipindahkan, $dpd['nomor']);
                    $sheet->setCellValue("B" . $i_keluar, $dpd['nama_pasien']);
                    $sheet->setCellValue("C" . $i_keluar, $dpd['nrm_pasien']);
                    $sheet->setCellValue("D" . $i_keluar, $dpd['kelas']);
                    $sheet->setCellValue("E" . $i_keluar, $dpd['ruang']);
                }
            }
            if (count($data_pasien_keluar) > 0) {
                foreach ($data_pasien_keluar as $dpk) {
                    $i_keluar = $i_keluar + 1;
                    if ($dpk['nomor'] > 0)
                        $sheet->setCellValue("A" . $i_keluar, $dpk['nomor']);
                    $sheet->setCellValue("F" . $i_keluar, $dpk['nama_pasien']);
                    $sheet->setCellValue("G" . $i_keluar, $dpk['nrm_pasien']);
                    $sheet->setCellValue("H" . $i_keluar, $dpk['kelas']);
                    $sheet->setCellValue("I" . $i_keluar, $dpk['tanggal_mrs']);
                    $sheet->setCellValue("J" . $i_keluar, $dpk['diizinkan_pulang']);
                    $sheet->setCellValue("K" . $i_keluar, $dpk['dirujuk']);
                    $sheet->setCellValue("L" . $i_keluar, $dpk['pindah_ke_rs_lain']);
                    $sheet->setCellValue("M" . $i_keluar, $dpk['pulang_paksa']);
                    $sheet->setCellValue("N" . $i_keluar, $dpk['lari']);
                    $sheet->setCellValue("O" . $i_keluar, $dpk['mati_k48']);
                    $sheet->setCellValue("P" . $i_keluar, $dpk['mati_l48']);
                    $sheet->setCellValue("Q" . $i_keluar, $dpk['lama_dirawat']);
                    $sheet->setCellValue("R" . $i_keluar, $dpk['dpjp']);
                    $sheet->setCellValue("S" . $i_keluar, $dpk['cara_pembayaran']);
                }
            }
            if ($i_dipindahkan > $i_keluar)
                $i = $i_dipindahkan;
            else
                $i = $i_keluar;
            $thin = array();
            $thin['borders'] = array();
            $thin['borders']['allborders'] = array();
            $thin['borders']['allborders']['style'] = PHPExcel_Style_Border::BORDER_THIN;
            $sheet->getStyle("A" . $border_start . ":S" . $i)->applyFromArray($thin);

            $i = $i + 2;
            $border_start = $i;
            $sheet->mergeCells("A" . $i . ":G" . $i)->setCellValue("A" . $i, "PASIEN YANG MASIH DIRAWAT (PASIEN AWAL)");
            $sheet->getStyle("A" . $i . ":G" . $i)->getFill()->applyFromArray(array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'startcolor' => array(
                    'rgb' => 'E0E0E0'
                )
            ));
            $sheet->getStyle("A" . $i . ":G" . $i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $sheet->getStyle("A" . $i . ":G" . $i)->getFont()->setBold(true);
            $i = $i + 1;
            $sheet->setCellValue("A" . $i, "No.");
            $sheet->setCellValue("B" . $i, "Nama Pasien");
            $sheet->setCellValue("C" . $i, "Nomor RM");
            $sheet->mergeCells("D" . $i . ":E" . $i)->setCellValue("D" . $i, "Kelas");
            $sheet->setCellValue("F" . $i, "Diagnosa");
            $sheet->setCellValue("G" . $i, "Dokter yang Merawat");
            $sheet->getStyle("A" . $i . ":G" . $i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $sheet->getStyle("A" . $i . ":G" . $i)->getFont()->setBold(true);
            $i = $i + 1;
            $sheet->setCellValue("A" . $i, "1");
            $sheet->setCellValue("B" . $i, "2");
            $sheet->setCellValue("C" . $i, "3");
            $sheet->mergeCells("D" . $i . ":E" . $i)->setCellValue("D" . $i, "4");
            $sheet->setCellValue("F" . $i, "5");
            $sheet->setCellValue("G" . $i, "6");
            $sheet->getStyle("A" . $i . ":G" . $i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $sheet->getStyle("A" . $i . ":G" . $i)->getFont()->setBold(true);
            if (count($data_pasien_dirawat) > 0) {
                foreach ($data_pasien_dirawat as $dpd) {
                    $i = $i + 1;
                    $sheet->setCellValue("A" . $i, $dpd['nomor']);
                    $sheet->setCellValue("B" . $i, $dpd['nama_pasien']);
                    $sheet->setCellValue("C" . $i, $dpd['nrm_pasien']);
                    $sheet->mergeCells("D" . $i . ":E" . $i)->setCellValue("D" . $i, $dpd['kelas']);
                    $sheet->setCellValue("F" . $i, $dpd['diagnosa']);
                    $sheet->setCellValue("G" . $i, $dpd['dokter']);
                    if ($dpd['pasien_pulang'] == "y") {
                        $sheet->getStyle("A" . $i . ":G" . $i)->getFill()->applyFromArray(array(
                            'type' => PHPExcel_Style_Fill::FILL_SOLID,
                            'startcolor' => array(
                                'rgb' => 'FFFF99'
                            )
                        ));
                    }
                }
            }
            $thin = array();
            $thin['borders'] = array();
            $thin['borders']['allborders'] = array();
            $thin['borders']['allborders']['style'] = PHPExcel_Style_Border::BORDER_THIN;
            $sheet->getStyle("A" . $border_start . ":G" . $i)->applyFromArray($thin);

            $i_footer = $i;
            $i_footer = $i_footer + 2;
            $border_start = $i_footer;
            $sheet->mergeCells("B" . $i_footer . ":C" . $i_footer)->setCellValue("B" . $i_footer, "Pasien Masuk");
            $sheet->setCellValue("D" . $i_footer, $_POST['total_pasien_masuk']);
            $i_footer = $i_footer + 1;
            $sheet->mergeCells("B" . $i_footer . ":C" . $i_footer)->setCellValue("B" . $i_footer, "Pasien Pindahan");
            $sheet->setCellValue("D" . $i_footer, $_POST['total_pasien_pindahan']);
            $i_footer = $i_footer + 1;
            $sheet->mergeCells("B" . $i_footer . ":C" . $i_footer)->setCellValue("B" . $i_footer, "Total Pasien Masuk");
            $sheet->setCellValue("D" . $i_footer, $_POST['subtotal_pasien_masuk']);
            $sheet->getStyle("B" . $i_footer . ":D" . $i_footer)->getFont()->setBold(true);
            $sheet->getStyle("B" . $i_footer . ":D" . $i_footer)->getFill()->applyFromArray(array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'startcolor' => array(
                    'rgb' => '99FFFF'
                )
            ));
            $thin = array();
            $thin['borders'] = array();
            $thin['borders']['allborders'] = array();
            $thin['borders']['allborders']['style'] = PHPExcel_Style_Border::BORDER_THIN;
            $sheet->getStyle("B" . $border_start . ":D" . $i_footer)->applyFromArray($thin);
            $i_footer = $i_footer + 2;
            $border_start = $i_footer;
            $sheet->mergeCells("B" . $i_footer . ":C" . $i_footer)->setCellValue("B" . $i_footer, "Pasien Diizinkan Pulang");
            $sheet->setCellValue("D" . $i_footer, $_POST['total_pasien_diizinkan_pulang']);
            $i_footer = $i_footer + 1;
            $sheet->mergeCells("B" . $i_footer . ":C" . $i_footer)->setCellValue("B" . $i_footer, "Pasien Dirujuk");
            $sheet->setCellValue("D" . $i_footer, $_POST['total_pasien_dirujuk']);
            $i_footer = $i_footer + 1;
            $sheet->mergeCells("B" . $i_footer . ":C" . $i_footer)->setCellValue("B" . $i_footer, "Pasien Pindah ke RS Lain");
            $sheet->setCellValue("D" . $i_footer, $_POST['total_pasien_pindah_ke_rs_lain']);
            $i_footer = $i_footer + 1;
            $sheet->mergeCells("B" . $i_footer . ":C" . $i_footer)->setCellValue("B" . $i_footer, "Pasien Pulang Paksa");
            $sheet->setCellValue("D" . $i_footer, $_POST['total_pasien_pulang_paksa']);
            $i_footer = $i_footer + 1;
            $sheet->mergeCells("B" . $i_footer . ":C" . $i_footer)->setCellValue("B" . $i_footer, "Pasien Melarikan Diri");
            $sheet->setCellValue("D" . $i_footer, $_POST['total_pasien_melarikan_diri']);
            $i_footer = $i_footer + 1;
            $sheet->mergeCells("B" . $i_footer . ":C" . $i_footer)->setCellValue("B" . $i_footer, "Total Pasien Keluar Hidup");
            $sheet->setCellValue("D" . $i_footer, $_POST['subtotal_pasien_keluar_hidup']);
            $sheet->getStyle("B" . $i_footer . ":D" . $i_footer)->getFont()->setBold(true);
            $sheet->getStyle("B" . $i_footer . ":D" . $i_footer)->getFill()->applyFromArray(array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'startcolor' => array(
                    'rgb' => '99FFFF'
                )
            ));
            $thin = array();
            $thin['borders'] = array();
            $thin['borders']['allborders'] = array();
            $thin['borders']['allborders']['style'] = PHPExcel_Style_Border::BORDER_THIN;
            $sheet->getStyle("B" . $border_start . ":D" . $i_footer)->applyFromArray($thin);
            $i_footer = $i_footer + 2;
            $border_start = $i_footer;
            $sheet->mergeCells("B" . $i_footer . ":C" . $i_footer)->setCellValue("B" . $i_footer, "Pasien Dipindahkan");
            $sheet->setCellValue("D" . $i_footer, $_POST['total_pasien_dipindahkan']);
            $i_footer = $i_footer + 1;
            $sheet->mergeCells("B" . $i_footer . ":C" . $i_footer)->setCellValue("B" . $i_footer, "Mati Kurang dari 48 Jam");
            $sheet->setCellValue("D" . $i_footer, $_POST['total_pasien_mati_k48']);
            $i_footer = $i_footer + 1;
            $sheet->mergeCells("B" . $i_footer . ":C" . $i_footer)->setCellValue("B" . $i_footer, "Mati Lebih dari atau Selama 48 Jam");
            $sheet->setCellValue("D" . $i_footer, $_POST['total_pasien_mati_l48']);
            $i_footer = $i_footer + 1;
            $sheet->mergeCells("B" . $i_footer . ":C" . $i_footer)->setCellValue("B" . $i_footer, "Total Pasien Keluar Ruangan");
            $sheet->setCellValue("D" . $i_footer, $_POST['subtotal_pasien_keluar_ruangan']);
            $sheet->getStyle("B" . $i_footer . ":D" . $i_footer)->getFont()->setBold(true);
            $sheet->getStyle("B" . $i_footer . ":D" . $i_footer)->getFill()->applyFromArray(array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'startcolor' => array(
                    'rgb' => '99FFFF'
                )
            ));
            $thin = array();
            $thin['borders'] = array();
            $thin['borders']['allborders'] = array();
            $thin['borders']['allborders']['style'] = PHPExcel_Style_Border::BORDER_THIN;
            $sheet->getStyle("B" . $border_start . ":D" . $i_footer)->applyFromArray($thin);
            $i_footer = $i_footer + 2;
            $border_start = $i_footer;
            $sheet->mergeCells("B" . $i_footer . ":C" . $i_footer)->setCellValue("B" . $i_footer, "Pasien yang Dirawat");
            $sheet->setCellValue("D" . $i_footer, $_POST['total_pasien_dirawat']);
            $i_footer = $i_footer + 1;
            $sheet->mergeCells("B" . $i_footer . ":C" . $i_footer)->setCellValue("B" . $i_footer, "Total Pasien yang Dirawat");
            $sheet->setCellValue("D" . $i_footer, $_POST['total_pasien_dirawat']);
            $sheet->getStyle("B" . $i_footer . ":D" . $i_footer)->getFont()->setBold(true);
            $sheet->getStyle("B" . $i_footer . ":D" . $i_footer)->getFill()->applyFromArray(array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'startcolor' => array(
                    'rgb' => '99FFFF'
                )
            ));
            $thin = array();
            $thin['borders'] = array();
            $thin['borders']['allborders'] = array();
            $thin['borders']['allborders']['style'] = PHPExcel_Style_Border::BORDER_THIN;
            $sheet->getStyle("B" . $border_start . ":D" . $i_footer)->applyFromArray($thin);

            $i_bed = $i;
            $i_bed = $i_bed + 2;
            $border_start = $i_bed;
            $sheet->mergeCells("H" . $i_bed . ":K" . $i_bed)->setCellValue("H" . $i_bed, "DAFTAR RUANGAN");
            $sheet->getStyle("H" . $i_bed . ":K" . $i_bed)->getFill()->applyFromArray(array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'startcolor' => array(
                    'rgb' => 'E0E0E0'
                )
            ));
            $sheet->getStyle("H" . $i_bed . ":K" . $i_bed)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $sheet->getStyle("H" . $i_bed . ":K" . $i_bed)->getFont()->setBold(true);
            $i_bed = $i_bed + 1;
            $sheet->setCellValue("H" . $i_bed, "No.");
            $sheet->setCellValue("I" . $i_bed, "Kelas");
            $sheet->setCellValue("J" . $i_bed, "Ruang");
            $sheet->setCellValue("K" . $i_bed, "Jumlah Tempat Tidur");
            $sheet->getStyle("H" . $i_bed . ":K" . $i_bed)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $sheet->getStyle("H" . $i_bed . ":K" . $i_bed)->getFont()->setBold(true);
            $i_bed = $i_bed + 1;
            $sheet->setCellValue("H" . $i_bed, "1");
            $sheet->setCellValue("I" . $i_bed, "2");
            $sheet->setCellValue("J" . $i_bed, "3");
            $sheet->setCellValue("K" . $i_bed, "4");
            $sheet->getStyle("H" . $i_bed . ":K" . $i_bed)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $sheet->getStyle("H" . $i_bed . ":K" . $i_bed)->getFont()->setBold(true);
            if (count($data_bed) > 0) {
                $total_bed = 0;
                foreach ($data_bed as $dtt) {
                    $i_bed = $i_bed + 1;
                    $sheet->setCellValue("H" . $i_bed, $dtt['nomor']);
                    $sheet->setCellValue("I" . $i_bed, $dtt['kelas']);
                    $sheet->setCellValue("J" . $i_bed, $dtt['ruang']);
                    $sheet->setCellValue("K" . $i_bed, $dtt['jumlah_bed']);
                    $total_bed += $dtt['jumlah_bed'];
                }
                $i_bed = $i_bed + 1;
                $sheet->mergeCells("H" . $i_bed . ":J" . $i_bed)->setCellValue("H" . $i_bed, "Jumlah Tempat Tidur");
                $sheet->setCellValue("K" . $i_bed, $total_bed);
                $sheet->getStyle("H" . $i_bed . ":J" . $i_bed)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                $sheet->getStyle("H" . $i_bed . ":K" . $i_bed)->getFont()->setBold(true);
                $sheet->getStyle("H" . $i_bed . ":K" . $i_bed)->getFill()->applyFromArray(array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'startcolor' => array(
                        'rgb' => '99FFFF'
                    )
                ));
            }
            $thin = array();
            $thin['borders'] = array();
            $thin['borders']['allborders'] = array();
            $thin['borders']['allborders']['style'] = PHPExcel_Style_Border::BORDER_THIN;
            $sheet->getStyle("H" . $border_start . ":K" . $i_bed)->applyFromArray($thin);

            $sheet->getColumnDimension('A')->setWidth(6.29);
            $sheet->getColumnDimension('B')->setWidth(33);
            $sheet->getColumnDimension('C')->setWidth(14);
            $sheet->getColumnDimension('D')->setWidth(16);
            $sheet->getColumnDimension('E')->setWidth(20);
            $sheet->getColumnDimension('F')->setWidth(33);
            $sheet->getColumnDimension('G')->setWidth(40);
            $sheet->getColumnDimension('H')->setWidth(16);
            $sheet->getColumnDimension('I')->setWidth(22);
            $sheet->getColumnDimension('J')->setWidth(19);
            $sheet->getColumnDimension('K')->setWidth(19);
            $sheet->getColumnDimension('L')->setWidth(19);
            $sheet->getColumnDimension('M')->setWidth(19);
            $sheet->getColumnDimension('N')->setWidth(19);
            $sheet->getColumnDimension('O')->setWidth(19);
            $sheet->getColumnDimension('P')->setWidth(19);
            $sheet->getColumnDimension('Q')->setWidth(15);
            $sheet->getColumnDimension('R')->setWidth(30);
            $sheet->getColumnDimension('S')->setWidth(20);

            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename="Laporan Sensus Harian Rawat Inap - ' . $ruangan_label . ' - ' . $tanggal . '.xls"');
            header('Cache-Control: max-age=0');
            $writer = PHPExcel_IOFactory::createWriter($file, 'Excel5');
            $writer->save('php://output');
        }
    	return;
    }

	$form = new Form("", "", "Laporan Sensus Harian Rawat Inap");
	$tanggal_text = new Text("lsri_tanggal", "lsri_tanggal", date("Y-m-d"));
	$tanggal_text->setClass("mydate");
	$tanggal_text->setAtribute("data-date-format='yyyy-mm-dd'");
	$form->addElement("Tanggal", $tanggal_text);
	$hari_text = new Text("lsri_hari", "lsri_hari", "");
	$hari_text->setAtribute("disabled='disabled'");
	$form->addElement("Hari", $hari_text);
	$urjip = new ServiceConsumer($db, "get_urjip", array());
    $urjip->setMode(ServiceConsumer::$MULTIPLE_MODE);
    $urjip->execute();
    $content = $urjip->getContent();
    $ruangan = array();
    foreach ($content as $autonomous => $ruang) {
        foreach ($ruang as $nama_ruang => $jip) {
            if ($jip[$nama_ruang] == "URI") {
                $option = array();
                $option['value'] = $nama_ruang;
                if ($jip['name'] != null)
                    $option['name'] = $jip['name'];
                else
                    $option['name'] = ArrayAdapter::format("unslug", $nama_ruang);
                $ruangan[] = $option;
            }
        }
    }
    $ruangan_select = new Select("lsri_ruangan", "lsri_ruangan", $ruangan);
    $form->addElement("Ruangan", $ruangan_select);
    $show_button = new Button("", "", "Tampilkan");
	$show_button->setClass("btn-primary");
	$show_button->setIcon("icon-white icon-repeat");
	$show_button->setIsButton(Button::$ICONIC);
	$show_button->setAction("lsri.view()");
	$download_button = new Button("", "", "Download");
	$download_button->setClass("btn-inverse");
	$download_button->setIcon("fa fa-download");
	$download_button->setIsButton(Button::$ICONIC);
	$download_button->setAction("lsri.export_xls()");
	$btn_group = new ButtonGroup("noprint");
	$btn_group->addButton($show_button);
	$btn_group->addButton($download_button);
	$form->addElement("", $btn_group);

    $pasien_masuk_table = new Table(
    	array("No.", "Nama Pasien", "No. RM", "Kelas"),
    	"",
    	null,
    	true
    );
    $pasien_masuk_table->setName("lsri_pasien_masuk");
    $pasien_masuk_table->setAction(false);
    $pasien_masuk_table->setFooterVisible(false);
    $pasien_masuk_table->setHeaderVisible(false);
    $pasien_masuk_table->addHeader("after", "
		<tr class='inverse'>
			<th colspan='4' style='vertical-align: middle !important;'>
				<center>PASIEN MASUK</center>
			</th>
		</tr>
		<tr class='inverse'>
			<th colspan='1' style='vertical-align: middle !important;'>
				<center>No.</center>
			</th>
			<th colspan='1' style='vertical-align: middle !important;'>
				<center>Nama Pasien</center>
			</th>
			<th colspan='1' style='vertical-align: middle !important;'>
				<center>No. RM</center>
			</th>
			<th colspan='1' style='vertical-align: middle !important;'>
				<center>Kelas</center>
			</th>
		</tr>
	");

    $pasien_pindahan_dari_ruang_lain_table = new Table(
    	array("No.", "Nama Pasien", "No. RM", "Kelas", "Ruang"),
    	"",
    	null,
    	true
    );
    $pasien_pindahan_dari_ruang_lain_table->setName("lsri_pasien_pindahan_dari_ruang_lain");
    $pasien_pindahan_dari_ruang_lain_table->setAction(false);
    $pasien_pindahan_dari_ruang_lain_table->setFooterVisible(false);
    $pasien_pindahan_dari_ruang_lain_table->setHeaderVisible(false);
    $pasien_pindahan_dari_ruang_lain_table->addHeader("after", "
		<tr class='inverse'>
			<th colspan='5' style='vertical-align: middle !important;'>
				<center>PASIEN PINDAHAN DARI RUANG LAIN</center>
			</th>
		</tr>
		<tr class='inverse'>
			<th colspan='1' style='vertical-align: middle !important;'>
				<center>No.</center>
			</th>
			<th colspan='1' style='vertical-align: middle !important;'>
				<center>Nama Pasien</center>
			</th>
			<th colspan='1' style='vertical-align: middle !important;'>
				<center>No. RM</center>
			</th>
			<th colspan='1' style='vertical-align: middle !important;'>
				<center>Kelas</center>
			</th>
			<th colspan='1' style='vertical-align: middle !important;'>
				<center>Ruang</center>
			</th>
		</tr>
	");

    $pasien_dipindahan_ke_ruang_lain_table = new Table(
    	array("No.", "Nama Pasien", "No. RM", "Kelas", "Ruang"),
    	"",
    	null,
    	true
    );
    $pasien_dipindahan_ke_ruang_lain_table->setName("lsri_pasien_dipindahkan_ke_ruang_lain");
    $pasien_dipindahan_ke_ruang_lain_table->setAction(false);
    $pasien_dipindahan_ke_ruang_lain_table->setFooterVisible(false);
    $pasien_dipindahan_ke_ruang_lain_table->setHeaderVisible(false);
    $pasien_dipindahan_ke_ruang_lain_table->addHeader("after", "
		<tr class='inverse'>
			<th colspan='5' style='vertical-align: middle !important;'>
				<center>PASIEN DIPINDAHKAN KE RUANG LAIN</center>
			</th>
		</tr>
		<tr class='inverse'>
			<th colspan='1' style='vertical-align: middle !important;'>
				<center>No.</center>
			</th>
			<th colspan='1' style='vertical-align: middle !important;'>
				<center>Nama Pasien</center>
			</th>
			<th colspan='1' style='vertical-align: middle !important;'>
				<center>No. RM</center>
			</th>
			<th colspan='1' style='vertical-align: middle !important;'>
				<center>Kelas</center>
			</th>
			<th colspan='1' style='vertical-align: middle !important;'>
				<center>Ruang</center>
			</th>
		</tr>
	");

     $pasien_keluar_rs_table = new Table(
    	array("No.", "Nama Pasien", "No. RM", "Kelas", "Tgl. Masuk", "Diizinkan Pulang", "Dirujuk", "Pindah ke RS Lain", "Pulang Paksa", "Lari", "Mati < 48 Jam", "Mati >= 48 Jam", "Lama Dirawat", "DPJP", "Cara Pembayaran"),
    	"",
    	null,
    	true
    );
    $pasien_keluar_rs_table->setName("lsri_pasien_keluar_rs");
    $pasien_keluar_rs_table->setAction(false);
    $pasien_keluar_rs_table->setFooterVisible(false);
    $pasien_keluar_rs_table->setHeaderVisible(false);
    $pasien_keluar_rs_table->addHeader("after", "
		<tr class='inverse'>
			<th colspan='15' style='vertical-align: middle !important;'>
				<center>PASIEN KELUAR RUMAH SAKIT</center>
			</th>
		</tr>
		<tr class='inverse'>
			<th rowspan='2' style='vertical-align: middle !important;'>
				<center>No.</center>
			</th>
			<th rowspan='2' style='vertical-align: middle !important;'>
				<center>Nama Pasien</center>
			</th>
			<th rowspan='2' style='vertical-align: middle !important;'>
				<center>No. RM</center>
			</th>
			<th rowspan='2' style='vertical-align: middle !important;'>
				<center>Kelas</center>
			</th>
			<th rowspan='2' style='vertical-align: middle !important;'>
				<center>Tanggal Masuk RS</center>
			</th>
			<th colspan='7' style='vertical-align: middle !important;'>
				<center>Cara Pasien Keluar</center>
			</th>
			<th rowspan='2' style='vertical-align: middle !important;'>
				<center>Lama Dirawat</center>
			</th>
			<th rowspan='2' style='vertical-align: middle !important;'>
				<center>DPJP</center>
			</th>
			<th rowspan='2' style='vertical-align: middle !important;'>
				<center>Cara Pembayaran</center>
			</th>
		</tr>
		<tr class='inverse'>
			<th style='vertical-align: middle !important;'>
				<center>Diizinkan Pulang</center>
			</th>
			<th style='vertical-align: middle !important;'>
				<center>Dirujuk</center>
			</th>
			<th style='vertical-align: middle !important;'>
				<center>Pindah ke RS Lain</center>
			</th>
			<th style='vertical-align: middle !important;'>
				<center>Pulang Paksa</center>
			</th>
			<th style='vertical-align: middle !important;'>
				<center>Lari</center>
			</th>
			<th style='vertical-align: middle !important;'>
				<center>Mati &lt; 48 Jam</center>
			</th>
			<th style='vertical-align: middle !important;'>
				<center>Mati &#8805; 48 Jam</center>
			</th>
		</tr>
	");

    $pasien_masih_dirawat_table = new Table(
    	array("No.", "Nama Pasien", "No. RM", "Kelas", "Diagnosa", "Dokter"),
    	"",
    	null,
    	true
    );
    $pasien_masih_dirawat_table->setName("lsri_pasien_masih_dirawat");
    $pasien_masih_dirawat_table->setAction(false);
    $pasien_masih_dirawat_table->setFooterVisible(false);
    $pasien_masih_dirawat_table->setHeaderVisible(false);
    $pasien_masih_dirawat_table->addHeader("after", "
		<tr class='inverse'>
			<th colspan='6' style='vertical-align: middle !important;'>
				<center>PASIEN YANG MASIH DIRAWAT (PASIEN AWAL)</center>
			</th>
		</tr>
		<tr class='inverse'>
			<th colspan='1' style='vertical-align: middle !important;'>
				<center>No.</center>
			</th>
			<th colspan='1' style='vertical-align: middle !important;'>
				<center>Nama Pasien</center>
			</th>
			<th colspan='1' style='vertical-align: middle !important;'>
				<center>No. RM</center>
			</th>
			<th colspan='1' style='vertical-align: middle !important;'>
				<center>Kelas</center>
			</th>
			<th colspan='1' style='vertical-align: middle !important;'>
				<center>Diagnosa</center>
			</th>
			<th colspan='1' style='vertical-align: middle !important;'>
				<center>Dokter</center>
			</th>
		</tr>
	");

    $jumlah_bed_table = new Table(
    	array("No.", "Kelas", "Ruangan", "Jumlah Tempat Tidur"),
    	"",
    	null,
    	true
    );
    $jumlah_bed_table->setName("lsri_jumlah_bed");
    $jumlah_bed_table->setAction(false);
    $jumlah_bed_table->setFooterVisible(false);

    $summary_table = new Table(
    	array("Info", "Nilai"),
    	"",
    	null,
    	true
    );
    $summary_table->setName("lsri_summary");
    $summary_table->setAction(false);
    $summary_table->setFooterVisible(false);

	echo $form->getHtml();
	echo "<div class='row-fluid'>";
		echo "<div class='span6'>";
			echo $pasien_masuk_table->getHtml();
		echo "</div>";
		echo "<div class='span6'>";
			echo $pasien_pindahan_dari_ruang_lain_table->getHtml();
		echo "</div>";
	echo "</div>";
	echo "<div class='row-fluid'>";
		echo "<div class='span12'>";
			echo $pasien_dipindahan_ke_ruang_lain_table->getHtml();
		echo "</div>";
	echo "</div>";
	echo "<div class='row-fluid'>";
		echo "<div class='span12'>";
			echo $pasien_keluar_rs_table->getHtml();
		echo "</div>";
	echo "</div>";
	echo "<div class='row-fluid'>";
		echo "<div class='span7'>";
			echo $pasien_masih_dirawat_table->getHtml();
		echo "</div>";
		echo "<div class='span5'>";
			echo $summary_table->getHtml();
		echo "</div>";
	echo "</div>";

	echo addJS("framework/smis/js/table_action.js");
	echo addJS("framework/bootstrap/js/bootstrap-datepicker.js");
	echo addCSS("framework/bootstrap/css/datepicker.css");
?>
<script type="text/javascript">
	var lsri;
    var sudah_diproses;
	$(document).ready(function() {
        sudah_diproses = false;
		$(".mydate").datepicker();
		lsri = new TableAction(
			"lsri",
			"medical_record",
			"laporan_sensus_rawat_inap",
			new Array()
		);
		lsri.view = function() {
			if ($("#lsri_tanggal").val() == "")
				return;
			showLoading();
			var d = new Date($("#lsri_tanggal").val());
			var weekday = new Array(7);
			weekday[0] = "Minggu";
			weekday[1] = "Senin";
			weekday[2] = "Selasa";
			weekday[3] = "Rabu";
			weekday[4] = "Kamis";
			weekday[5] = "Jumat";
			weekday[6] = "Sabtu";

			var n = weekday[d.getDay()];
			$("#lsri_hari").val(n);
			var self = this;
			var data = this.getRegulerData();
			data['command'] = "view_pasien_masuk";
			$.post(
				"",
				data,
				function(response_pasien_masuk) {
					var json_pasien_masuk = JSON.parse(response_pasien_masuk);
					if (json_pasien_masuk == null) {
						dismissLoading();
						return;
					}
					$("#lsri_pasien_masuk_list").html(json_pasien_masuk.html);
					$("#lrsi_total_pasien_masuk").html("<div align='right'>" + json_pasien_masuk.jumlah + "</div>");

					data['command'] = "view_pasien_pindahan";
					$.post(
						'',
						data,
						function(response_pasien_pindahan) {
							var json_pasien_pindahan = JSON.parse(response_pasien_pindahan);
							if (json_pasien_pindahan == null) {
								dismissLoading();
								return;
							}
							$("#lsri_pasien_pindahan_dari_ruang_lain_list").html(json_pasien_pindahan.html);
							$("#lrsi_total_pasien_pindahan").html("<div align='right'>" + json_pasien_pindahan.jumlah + "</div>");
							var total_pasien_masuk = parseFloat(json_pasien_masuk.jumlah) + parseFloat(json_pasien_pindahan.jumlah);
							$("#lrsi_subtotal_pasien_masuk").html("<div align='right'><strong>" + total_pasien_masuk + "</strong></div>");;

							data['command'] = "view_pasien_dipindah";
							$.post(
								'',
								data,
								function(response_pasien_dipindah) {
									var json_pasien_dipindah = JSON.parse(response_pasien_dipindah);
									if (json_pasien_dipindah == null) {
										dismissLoading();
										return;
									}

									$("#lsri_pasien_dipindahkan_ke_ruang_lain_list").html(json_pasien_dipindah.html);
									$("#lrsi_total_pasien_dipindahkan").html("<div align='right'>" + json_pasien_dipindah.jumlah + "</div>");
									
									data['command'] = "view_pasien_pulang";
									$.post(
										"",
										data,
										function(response_pasien_pulang) {
											var json_pasien_pulang = JSON.parse(response_pasien_pulang);
											if (json_pasien_pulang == null) {
												dismissLoading();
												return;
											}
											$("#lsri_pasien_keluar_rs_list").html(json_pasien_pulang.html);
											$("#lrsi_total_pasien_diizinkan_pulang").html("<div align='right'>" + json_pasien_pulang.jumlah_diizinkan_pulang + "</div>");
											$("#lrsi_total_pasien_dirujuk").html("<div align='right'>" + json_pasien_pulang.jumlah_dirujuk + "</div>");
											$("#lrsi_total_pasien_pindah_ke_rs_lain").html("<div align='right'>" + json_pasien_pulang.jumlah_pindah_ke_rs_lain + "</div>");
											$("#lrsi_total_pasien_pulang_paksa").html("<div align='right'>" + json_pasien_pulang.jumlah_pulang_paksa + "</div>");
											$("#lrsi_total_pasien_melarikan_diri").html("<div align='right'>" + json_pasien_pulang.jumlah_lari + "</div>");
											$("#lrsi_total_pasien_mati_k48").html("<div align='right'>" + json_pasien_pulang.jumlah_mati_k48 + "</div>");
											$("#lrsi_total_pasien_mati_l48").html("<div align='right'>" + json_pasien_pulang.jumlah_mati_l48 + "</div>");

											var total_pasien_keluar_hidup = parseFloat(json_pasien_pulang.jumlah_diizinkan_pulang) + parseFloat(json_pasien_pulang.jumlah_dirujuk) + parseFloat(json_pasien_pulang.jumlah_pindah_ke_rs_lain) + parseFloat(json_pasien_pulang.jumlah_pulang_paksa) + parseFloat(json_pasien_pulang.jumlah_lari);
											$("#lrsi_subtotal_pasien_keluar_hidup").html("<div align='right'><strong>" + total_pasien_keluar_hidup + "</strong></div>");
											var total_pasien_keluar_ruangan = parseFloat(json_pasien_dipindah.jumlah) + parseFloat(json_pasien_pulang.jumlah_mati_k48) + parseFloat(json_pasien_pulang.jumlah_mati_l48);
											$("#lrsi_subtotal_pasien_keluar_ruangan").html("<div align='right'><strong>" + total_pasien_keluar_ruangan + "</strong></div>");


											data['command'] = "view_pasien_awal";
											$.post(
												'',
												data,
												function(response_pasien_awal) {
													var json_pasien_awal = JSON.parse(response_pasien_awal);
													if (json_pasien_awal == null) {
														dismissLoading();
														return;
													}
													$("#lsri_pasien_masih_dirawat_list").html(json_pasien_awal.html);
													$("#lrsi_total_pasien_dirawat").html("<div align='right'>" + json_pasien_awal.jumlah + "</div>");
													$("#lrsi_subtotal_pasien_dirawat").html("<div align='right'><strong>" + json_pasien_awal.jumlah + "</strong></div>");
													
													data['command'] = "view_jumlah_tempat_tidur";
													$.post(
														'',
														data,
														function(response_jumlah_tt) {
															var json_jumlah_tt = JSON.parse(response_jumlah_tt);
															if (json_jumlah_tt == null) {
																dismissLoading();
																return;
															}
															$("#lsri_jumlah_bed_list").html(json_jumlah_tt.html);
                                                            sudah_diproses = true;
															dismissLoading();
														}
													);
												} //. End of Pasien Aktif
											);
										} //. End of Pasien Pulang
									);
								} //. End of Pasien Dipindah
							);
						} //. Emd of Pasien Pindahan
					);
				} //. End of Pasien Masuk
			);
		};
		lsri.addRegulerData = function(data) {
			data['tanggal'] = $("#lsri_tanggal").val();
            data['hari'] = $("#lsri_hari").val();
			data['ruangan'] = $("#lsri_ruangan").val();
			data['ruangan_label'] = $("#lsri_ruangan option:selected").text();
			return data;
		};
        lsri.export_xls = function() {
            var data = this.getRegulerData();
            data['command'] = "export_xls";
            /// Pasien Masuk :
            var jumlah_data_pasien_masuk = $("#lsri_pasien_masuk_list tr").length;
            var data_pasien_masuk = {};
            for (var i = 0; i < jumlah_data_pasien_masuk; i++) {
                var nomor = $("#lsri_pasien_masuk_list tr:eq(" + i + ") td:eq(0)").text();
                var nama_pasien = $("#lsri_pasien_masuk_list tr:eq(" + i + ") td:eq(1)").text();
                var nrm_pasien = $("#lsri_pasien_masuk_list tr:eq(" + i + ") td:eq(2)").text();
                var kelas = $("#lsri_pasien_masuk_list tr:eq(" + i + ") td:eq(3)").text();
                var arr = {};
                arr['nomor'] = nomor;
                arr['nama_pasien'] = nama_pasien;
                arr['nrm_pasien'] = nrm_pasien;
                arr['kelas'] = kelas;
                data_pasien_masuk[i] = arr;
            }
            data['data_pasien_masuk'] = JSON.stringify(data_pasien_masuk);
            /// Pasien Pindahan dari Ruang Lain :
            var jumlah_data_pasien_pindahan = $("#lsri_pasien_pindahan_dari_ruang_lain_list tr").length;
            var data_pasien_pindahan = {};
            for (var i = 0; i < jumlah_data_pasien_pindahan; i++) {
                var nomor = $("#lsri_pasien_pindahan_dari_ruang_lain_list tr:eq(" + i + ") td:eq(0)").text();
                var nama_pasien = $("#lsri_pasien_pindahan_dari_ruang_lain_list tr:eq(" + i + ") td:eq(1)").text();
                var nrm_pasien = $("#lsri_pasien_pindahan_dari_ruang_lain_list tr:eq(" + i + ") td:eq(2)").text();
                var kelas = $("#lsri_pasien_pindahan_dari_ruang_lain_list tr:eq(" + i + ") td:eq(3)").text();
                var ruang = $("#lsri_pasien_pindahan_dari_ruang_lain_list tr:eq(" + i + ") td:eq(4)").text();
                var arr = {};
                arr['nomor']        = nomor;
                arr['nama_pasien']  = nama_pasien;
                arr['nrm_pasien']   = nrm_pasien;
                arr['kelas']        = kelas;
                arr['ruang']        = ruang;
                data_pasien_pindahan[i] = arr;
            }
            data['data_pasien_pindahan'] = JSON.stringify(data_pasien_pindahan);
            /// Pasien Dipindahkan ke Ruang Lain :
            var jumlah_data_pasien_dipindahkan = $("#lsri_pasien_dipindahkan_ke_ruang_lain_list tr").length;
            var data_pasien_dipindahkan = {};
            for (var i = 0; i < jumlah_data_pasien_dipindahkan; i++) {
                var nomor = $("#lsri_pasien_dipindahkan_ke_ruang_lain_list tr:eq(" + i + ") td:eq(0)").text();
                var nama_pasien = $("#lsri_pasien_dipindahkan_ke_ruang_lain_list tr:eq(" + i + ") td:eq(1)").text();
                var nrm_pasien = $("#lsri_pasien_dipindahkan_ke_ruang_lain_list tr:eq(" + i + ") td:eq(2)").text();
                var kelas = $("#lsri_pasien_dipindahkan_ke_ruang_lain_list tr:eq(" + i + ") td:eq(3)").text();
                var ruang = $("#lsri_pasien_dipindahkan_ke_ruang_lain_list tr:eq(" + i + ") td:eq(4)").text();
                var arr = {};
                arr['nomor']        = nomor;
                arr['nama_pasien']  = nama_pasien;
                arr['nrm_pasien']   = nrm_pasien;
                arr['kelas']        = kelas;
                arr['ruang']        = ruang;
                data_pasien_pindahan[i] = arr;
            }
            data['data_pasien_dipindahkan'] = JSON.stringify(data_pasien_dipindahkan);
            /// Pasien Keluar Rumah Sakit :
            var jumlah_data_pasien_keluar = $("#lsri_pasien_keluar_rs_list tr").length;
            var data_pasien_keluar = {};
            for (var i = 0; i < jumlah_data_pasien_keluar; i++) {
                var nomor = $("#lsri_pasien_keluar_rs_list tr:eq(" + i + ") td:eq(0)").text();
                var nama_pasien = $("#lsri_pasien_keluar_rs_list tr:eq(" + i + ") td:eq(1)").text();
                var nrm_pasien = $("#lsri_pasien_keluar_rs_list tr:eq(" + i + ") td:eq(2)").text();
                var kelas = $("#lsri_pasien_keluar_rs_list tr:eq(" + i + ") td:eq(3)").text();
                var tanggal_mrs = $("#lsri_pasien_keluar_rs_list tr:eq(" + i + ") td:eq(4)").text();
                var diizinkan_pulang = $("#lsri_pasien_keluar_rs_list tr:eq(" + i + ") td:eq(5)").text();
                var dirujuk = $("#lsri_pasien_keluar_rs_list tr:eq(" + i + ") td:eq(6)").text();
                var pindah_ke_rs_lain = $("#lsri_pasien_keluar_rs_list tr:eq(" + i + ") td:eq(7)").text();
                var pulang_paksa = $("#lsri_pasien_keluar_rs_list tr:eq(" + i + ") td:eq(8)").text();
                var lari = $("#lsri_pasien_keluar_rs_list tr:eq(" + i + ") td:eq(9)").text();
                var mati_k48 = $("#lsri_pasien_keluar_rs_list tr:eq(" + i + ") td:eq(10)").text();
                var mati_l48 = $("#lsri_pasien_keluar_rs_list tr:eq(" + i + ") td:eq(11)").text();
                var lama_dirawat = $("#lsri_pasien_keluar_rs_list tr:eq(" + i + ") td:eq(12)").text();
                var dpjp = $("#lsri_pasien_keluar_rs_list tr:eq(" + i + ") td:eq(13)").text();
                var cara_pembayaran = $("#lsri_pasien_keluar_rs_list tr:eq(" + i + ") td:eq(14)").text();
                var arr = {};
                arr['nomor']             = nomor;
                arr['nama_pasien']       = nama_pasien;
                arr['nrm_pasien']        = nrm_pasien;
                arr['kelas']             = kelas;
                arr['tanggal_mrs']       = tanggal_mrs;
                arr['diizinkan_pulang']  = diizinkan_pulang;
                arr['dirujuk']           = dirujuk;
                arr['pindah_ke_rs_lain'] = pindah_ke_rs_lain;
                arr['pulang_paksa']      = pulang_paksa;
                arr['lari']              = lari;
                arr['mati_k48']          = mati_k48;
                arr['mati_l48']          = mati_l48;
                arr['lama_dirawat']      = lama_dirawat;
                arr['dpjp']              = dpjp;
                arr['cara_pembayaran']   = cara_pembayaran;
                data_pasien_keluar[i] = arr;
            }
            data['data_pasien_keluar'] = JSON.stringify(data_pasien_keluar);
            /// Pasien Dirawat :
            var jumlah_data_pasien_dirawat = $("#lsri_pasien_masih_dirawat_list tr").length;
            var data_pasien_dirawat = {};
            for (var i = 0; i < jumlah_data_pasien_dirawat; i++) {
                var nomor = $("#lsri_pasien_masih_dirawat_list tr:eq(" + i + ") td:eq(0)").text();
                var nama_pasien = $("#lsri_pasien_masih_dirawat_list tr:eq(" + i + ") td:eq(1)").text();
                var nrm_pasien = $("#lsri_pasien_masih_dirawat_list tr:eq(" + i + ") td:eq(2)").text();
                var kelas = $("#lsri_pasien_masih_dirawat_list tr:eq(" + i + ") td:eq(3)").text();
                var diagnosa = $("#lsri_pasien_masih_dirawat_list tr:eq(" + i + ") td:eq(4)").text();
                var dokter = $("#lsri_pasien_masih_dirawat_list tr:eq(" + i + ") td:eq(5)").text();
                var pasien_pulang = $("#lsri_pasien_masih_dirawat_list tr:eq(" + i + ") td:eq(6)").text();
                var arr = {};
                arr['nomor']         = nomor;
                arr['nama_pasien']   = nama_pasien;
                arr['nrm_pasien']    = nrm_pasien;
                arr['kelas']         = kelas;
                arr['diagnosa']      = diagnosa;
                arr['dokter']        = dokter;
                arr['pasien_pulang'] = pasien_pulang;
                data_pasien_dirawat[i] = arr;
            }
            data['data_pasien_dirawat'] = JSON.stringify(data_pasien_dirawat);
            /// Info Footer :
            data['total_pasien_masuk'] = $("#lrsi_total_pasien_masuk").text();
            data['total_pasien_pindahan'] = $("#lrsi_total_pasien_pindahan").text();
            data['subtotal_pasien_masuk'] = $("#lrsi_subtotal_pasien_masuk").text();
            data['total_pasien_diizinkan_pulang'] = $("#lrsi_total_pasien_diizinkan_pulang").text();
            data['total_pasien_dirujuk'] = $("#lrsi_total_pasien_dirujuk").text();
            data['total_pasien_pindah_ke_rs_lain'] = $("#lrsi_total_pasien_pindah_ke_rs_lain").text();
            data['total_pasien_pulang_paksa'] = $("#lrsi_total_pasien_pulang_paksa").text();
            data['total_pasien_melarikan_diri'] = $("#lrsi_total_pasien_melarikan_diri").text();
            data['subtotal_pasien_keluar_hidup'] = $("#lrsi_subtotal_pasien_keluar_hidup").text();
            data['total_pasien_dipindahkan'] = $("#lrsi_total_pasien_dipindahkan").text();
            data['total_pasien_mati_k48'] = $("#lrsi_total_pasien_mati_k48").text();
            data['total_pasien_mati_l48'] = $("#lrsi_total_pasien_mati_l48").text();
            data['subtotal_pasien_keluar_ruangan'] = $("#lrsi_subtotal_pasien_keluar_ruangan").text();
            data['total_pasien_dirawat'] = $("#lrsi_total_pasien_dirawat").text();
            data['subtotal_pasien_dirawat'] = $("#lrsi_subtotal_pasien_dirawat").text();
            /// Info Bed :
            var jumlah_data_bed = $("#lsri_jumlah_bed_list tr").length;
            var data_bed = {};
            for (var i = 0; i < jumlah_data_bed - 1; i++) {
                var nomor = $("#lsri_jumlah_bed_list tr:eq(" + i + ") td:eq(0)").text();
                var kelas = $("#lsri_jumlah_bed_list tr:eq(" + i + ") td:eq(1)").text();
                var ruang = $("#lsri_jumlah_bed_list tr:eq(" + i + ") td:eq(2)").text();
                var jumlah_bed = $("#lsri_jumlah_bed_list tr:eq(" + i + ") td:eq(3)").text();
                var arr = {};
                arr['nomor']  = nomor;
                arr['kelas']  = kelas;
                arr['ruang']  = ruang;
                arr['jumlah_bed'] = jumlah_bed;
                data_bed[i] = arr;
            }
            data['data_bed'] = JSON.stringify(data_bed);

            postForm(data);
        };
		$("#lsri_pasien_masuk_list").html("<tr><td colspan='4'><center>Data Belum Diproses</center></td></tr>");
		$("#lsri_pasien_pindahan_dari_ruang_lain_list").html("<tr><td colspan='5'><center>Data Belum Diproses</center></td></tr>");
		$("#lsri_pasien_dipindahkan_ke_ruang_lain_list").html("<tr><td colspan='5'><center>Data Belum Diproses</center></td></tr>");
		$("#lsri_pasien_keluar_rs_list").html("<tr><td colspan='15'><center>Data Belum Diproses</center></td></tr>");
		$("#lsri_pasien_masih_dirawat_list").html("<tr><td colspan='6'><center>Data Belum Diproses</center></td></tr>");
		$("#lsri_jumlah_bed_list").html("<tr><td colspan='4'><center>Data Belum Diproses</center></td></tr>");
		$("#lsri_summary_list").html(
			"<tr>" + 
				"<td>Pasien Masuk</td>" +
				"<td id='lrsi_total_pasien_masuk'><div align='right'>0</div></td>" +
			"</tr>" +
			"<tr>" + 
				"<td>Pasien Pindahan</td>" +
				"<td id='lrsi_total_pasien_pindahan'><div align='right'>0</div></td>" +
			"</tr>" +
			"<tr>" + 
				"<td><strong>Total Pasien Masuk</strong></td>" +
				"<td id='lrsi_subtotal_pasien_masuk'><div align='right'><strong>0</strong></div></td>" +
			"</tr>" +
			"<tr>" + 
				"<td colspan='2'></td>" +
			"</tr>" +
			"<tr>" + 
				"<td>Pasien Diizinkan Pulang</td>" +
				"<td id='lrsi_total_pasien_diizinkan_pulang'><div align='right'>0</div></td>" +
			"</tr>" +
			"<tr>" + 
				"<td>Pasien Dirujuk</td>" +
				"<td id='lrsi_total_pasien_dirujuk'><div align='right'>0</div></td>" +
			"</tr>" +
			"<tr>" + 
				"<td>Pasien Pindah ke RS Lain</td>" +
				"<td id='lrsi_total_pasien_pindah_ke_rs_lain'><div align='right'>0</div></td>" +
			"</tr>" +
			"<tr>" + 
				"<td>Pasien Pulang Paksa</td>" +
				"<td id='lrsi_total_pasien_pulang_paksa'><div align='right'>0</div></td>" +
			"</tr>" +
			"<tr>" + 
				"<td>Pasien Melarikan Diri</td>" +
				"<td id='lrsi_total_pasien_melarikan_diri'><div align='right'>0</div></td>" +
			"</tr>" +
			"<tr>" + 
				"<td><strong>Total Pasien Keluar Hidup</strong></td>" +
				"<td id='lrsi_subtotal_pasien_keluar_hidup'><div align='right'><strong>0</strong></div></td>" +
			"</tr>" +
			"<tr>" + 
				"<td colspan='2'></td>" +
			"</tr>" +
			"<tr>" + 
				"<td>Pasien Dipindahkan</td>" +
				"<td id='lrsi_total_pasien_dipindahkan'><div align='right'>0</div></td>" +
			"</tr>" +
			"<tr>" + 
				"<td>Pasien Mati Kurang dari 48 Jam</td>" +
				"<td id='lrsi_total_pasien_mati_k48'><div align='right'>0</div></td>" +
			"</tr>" +
			"<tr>" + 
				"<td>Pasien Mati Lebih dari atau Selama 48 Jam</td>" +
				"<td id='lrsi_total_pasien_mati_l48'><div align='right'>0</div></td>" +
			"</tr>" +
			"<tr>" + 
				"<td><strong>Total Pasien Keluar Ruangan</strong></td>" +
				"<td id='lrsi_subtotal_pasien_keluar_ruangan'><div align='right'><strong>0</strong></div></td>" +
			"</tr>" +
			"<tr>" + 
				"<td colspan='2'></td>" +
			"</tr>" +
			"<tr>" + 
				"<td>Pasien Dirawat</td>" +
				"<td id='lrsi_total_pasien_dirawat'><div align='right'>0</div></td>" +
			"</tr>" +
			"<tr>" + 
				"<td><strong>Total Pasien Dirawat</strong></td>" +
				"<td id='lrsi_subtotal_pasien_dirawat'><div align='right'><strong>0</strong></div></td>" +
			"</tr>" +
			"<tr>" + 
				"<td colspan='2'></td>" +
			"</tr>" +
			"<tr>" + 
				"<td colspan='2'>" + 
					"<table id='table_lsri_jumlah_bed' data-fix-header='n' class='table table-bordered table-hover table-striped table-condensed'>" +
						"<thead>" +
							"<tr class='inverse lsri_pasien_masih_dirawat_header_normal'>" +
								"<th>No.</th>" + 
								"<th>Kelas</th>" +
								"<th>Ruangan</th>" +
								"<th>Jumlah Tempat Tidur</th>" +
							"</tr>" +
						"</thead>" +
						"<tbody id='lsri_jumlah_bed_list'>" +
							"<tr>" +
								"<td colspan='4'>" +
									"<center>Data Belum Diproses</center>" +
								"</td>" +
							"</tr>" + 
						"</tbody>" +
					"</table>" +
				"</td>" +
			"</tr>"
		);
	});
</script>