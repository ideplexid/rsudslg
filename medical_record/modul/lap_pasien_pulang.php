<?php

$tab = new Tabulator ( "lap_pasien_pulang", "lap_pasien_pulang", Tabulator::$LANDSCAPE_RIGHT);
$tab->add ( "rekap_pulang_rwt_jalan_non_igd", "Rekap Pulang Rawat Jalan Non IGD", "", Tabulator::$TYPE_HTML,"fa fa-sign-out" );
$tab->add ( "rincian_pulang_rwt_jalan_non_igd", "Rincian Pulang Rawat Jalan Non IGD", "", Tabulator::$TYPE_HTML,"fa fa-sign-out" );
$tab->add ( "rekap_pulang_rwt_inap", "Rekap Pulang Rawat Inap", "", Tabulator::$TYPE_HTML,"fa fa-sign-out" );
$tab->add ( "rincian_pulang_rwt_inap", "Rincian Pulang Rawat Inap", "", Tabulator::$TYPE_HTML,"fa fa-sign-out" );
$tab->add ( "rekap_pulang_igd", "Rekap Pulang IGD", "", Tabulator::$TYPE_HTML,"fa fa-sign-out" );
$tab->add ( "rincian_pulang_igd", "Rincian Pulang IGD", "", Tabulator::$TYPE_HTML,"fa fa-sign-out" );
$tab->add ( "lap_pasien_pulang_uncrawler", "Laporan Pasien Pulang", "", Tabulator::$TYPE_HTML,"fa fa-sign-out" );

$tab->setPartialLoad(true, "medical_record", "laporan_kunjungan", "laporan_kunjungan",true);
echo $tab->getHtml ();

?>