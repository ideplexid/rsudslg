<?php

$tab = new Tabulator ( "efisiensi_mutu", "Efisiensi Mutu", Tabulator::$LANDSCAPE_RIGHT);
$tab->add ( "efisiensi_mutu_layanan_rs", "Efisiensi Mutu Layanan Rumah Sakit", "", Tabulator::$TYPE_HTML,"fa fa-file" );
$tab->add ( "efisiensi_mutu_layanan_ruangan", "Efisiensi Mutu Layanan Ruangan", "", Tabulator::$TYPE_HTML,"fa fa-file" );

$tab->setPartialLoad(true, "medical_record", "efisiensi_mutu", "efisiensi_mutu",true);
echo $tab->getHtml ();

?>