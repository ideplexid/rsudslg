<?php

$tab = new Tabulator ( "laporan_kunjungan", "laporan_kunjungan", Tabulator::$LANDSCAPE_RIGHT);
$tab->add ( "rekap_kunj_rwt_jalan_non_igd", "Rekap Kunjungan Rawat Jalan Non IGD", "", Tabulator::$TYPE_HTML,"fa fa-sign-in" );
//$tab->add ( "rincian_kunj_rwt_jalan_non_igd", "Rincian Kunjungan Rawat Jalan Non IGD", "", Tabulator::$TYPE_HTML,"fa fa-sign-in" );
$tab->add ( "rekap_kunj_rwt_inap", "Rekap Kunjungan Rawat Inap", "", Tabulator::$TYPE_HTML,"fa fa-sign-in" );
//$tab->add ( "rincian_kunj_rwt_inap", "Rincian Kunjungan Rawat Inap", "", Tabulator::$TYPE_HTML,"fa fa-sign-in" );
$tab->add ( "rekap_kunj_igd", "Rekap Kunjungan IGD", "", Tabulator::$TYPE_HTML,"fa fa-sign-in" );
$tab->add ( "rincian_kunj_igd", "Rincian Kunjungan IGD", "", Tabulator::$TYPE_HTML,"fa fa-sign-in" );

$tab->setPartialLoad(true, "medical_record", "laporan_kunjungan", "laporan_kunjungan",true);
echo $tab->getHtml ();

?>