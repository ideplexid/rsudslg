<?php
setChangeCookie ( false );
changeCookie ();
$tab = new Tabulator ( "lap_kb", "lap_kb", Tabulator::$LANDSCAPE_RIGHT );
$tab->add ( "lap_kb_baru", "Laporan KB Baru", "", Tabulator::$TYPE_HTML," fa fa-female" );
$tab->add ( "lap_kb_kasus", "Laporan KB Pencabutan", "", Tabulator::$TYPE_HTML,"fa fa-trash" );
$tab->add ( "lap_kb_ulang", "Laporan KB Ulang", "", Tabulator::$TYPE_HTML ,"fa fa-refresh");
$tab->add ( "lap_kb_pasca", "Laporan KB Pasca", "", Tabulator::$TYPE_HTML ,"fa fa-heart");
$tab->add ( "lap_persalinan", "Laporan Persalinan", "", Tabulator::$TYPE_HTML,"fa fa-child" );
$tab->add ( "lap_imunisasi", "Laporan Imunisasi", "", Tabulator::$TYPE_HTML,"fa fa-eyedropper" );
$tab->setPartialLoad(true, "medical_record", "lap_kb", "lap_kb",true);	
echo $tab->getHtml ();

?>