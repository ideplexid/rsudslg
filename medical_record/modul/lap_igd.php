<?php
	$tab = new Tabulator ( "lap_igd", "lap_igd" );
	$tab->add ( "lap_igd_keterangan", "Keterangan", "", Tabulator::$TYPE_HTML,"fa fa-align-justify" );
	$tab->add ( "lap_igd_koreksi", "Rekap Data", "", Tabulator::$TYPE_HTML,"fa fa-support" );
	$tab->add ( "lap_igd_triage", "Laporan Triage", "", Tabulator::$TYPE_HTML,"fa fa-warning" );
	$tab->add ( "lap_igd_response", "Laporan Response Time", "", Tabulator::$TYPE_HTML,"fa fa-clock-o" );
	$tab->add ( "lap_igd_kunjungan", "Laporan Kunjungan", "", Tabulator::$TYPE_HTML,"fa fa-calendar" );
	$tab->add ( "lap_igd_data", "Laporan Shift", "", Tabulator::$TYPE_HTML,"fa fa-refresh" );
	$tab->setPartialLoad(true, "medical_record", "lap_igd", "lap_igd",true);	
	echo $tab->getHtml ();
?>