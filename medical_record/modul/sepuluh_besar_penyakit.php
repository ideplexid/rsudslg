<?php

$tab = new Tabulator ( "sepuluh_besar_penyakit", "sepuluh_besar_penyakit", Tabulator::$LANDSCAPE_RIGHT);
$tab->add ( "sepuluh_besar_penyakit_rwt_jalan_non_igd", "10 Besar Penyakit Rawat Jalan Non IGD", "", Tabulator::$TYPE_HTML,"fa fa-medkit" );
$tab->add ( "sepuluh_besar_penyakit_rwt_inap", "10 Besar Penyakit Rawat Inap", "", Tabulator::$TYPE_HTML,"fa fa-medkit" );
$tab->add ( "sepuluh_besar_penyakit_igd", "10 Besar Penyakit IGD", "", Tabulator::$TYPE_HTML,"fa fa-medkit" );

$tab->setPartialLoad(true, "medical_record", "sepuluh_besar_penyakit", "sepuluh_besar_penyakit",true);
echo $tab->getHtml ();

?>