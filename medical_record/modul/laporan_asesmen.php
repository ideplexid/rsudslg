<?php 


$tabs=new Tabulator("laporan_asesmen","laporan_asesmen",Tabulator::$LANDSCAPE_RIGHT);
$tabs->add("asesmen_anak","Assesmen Anak","",Tabulator::$TYPE_HTML,"fa fa-file-text-o");
$tabs->add("asesmen_perawat","Assesmen Keperawatan","",Tabulator::$TYPE_HTML,"fa fa-file-text-o");
$tabs->add("asesmen_jantung","Assesmen Jantung","",Tabulator::$TYPE_HTML,"fa fa-file-text-o");
$tabs->add("asesmen_psikologi","Assesmen Psikologi","",Tabulator::$TYPE_HTML,"fa fa-file-text-o");
$tabs->add("asesmen_psikiatri","Assesmen Psikiatri","",Tabulator::$TYPE_HTML,"fa fa-file-text-o");
$tabs->add("asesmen_syaraf","Assesmen Syaraf","",Tabulator::$TYPE_HTML,"fa fa-file-text-o");

$tabs->setPartialLoad(true, "medical_record", "laporan_asesmen", "laporan_asesmen",true);
echo $tabs->getHtml();
?>