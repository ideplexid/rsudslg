<?php 

$tabs=new Tabulator("kuisioner", "kuisioner",Tabulator::$POTRAIT);
$tabs->add("kuisioner_rj", "Input K. R.Jalan", "",Tabulator::$TYPE_HTML,"fa fa-user-md");
$tabs->add("analisa_kuisioner_rj", "Analisa R. Jalan ", "",Tabulator::$TYPE_HTML," fa fa-bar-chart");
$tabs->add("kuisioner_ri", "Input K. R. Inap", "",Tabulator::$TYPE_HTML,"fa fa-bed");
$tabs->add("analisa_kuisioner_ri", "Analisa K. R. Inap", "",Tabulator::$TYPE_HTML," fa fa-area-chart");
$tabs->setPartialLoad(true, "medical_record", "kuisioner","kuisioner",true);
echo $tabs->getHtml();
?>