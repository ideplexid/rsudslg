<?php 
$tab = new Tabulator ( "lap_index", "",Tabulator::$POTRAIT );
$tab->add ( "lap_index_kematian", "Kematian", "", Tabulator::$TYPE_HTML,"fa fa-ambulance" );
$tab->add ( "lap_index_dokter", "Dokter", "", Tabulator::$TYPE_HTML,"fa fa-user-md" );
$tab->add ( "lap_index_penyakit", "Penyakit", "", Tabulator::$TYPE_HTML,"fa fa-medkit" );
$tab->add ( "lap_index_operasi", "Operasi", "", Tabulator::$TYPE_HTML,"fa fa-heartbeat" );
$tab->add ( "lap_index_ranap", "Rawat Inap", "", Tabulator::$TYPE_HTML,"fa fa-bed" );
$tab->add ( "lap_index_rajal", "Rawat Jalan", "", Tabulator::$TYPE_HTML,"fa fa-blind" );
$tab->setPartialLoad(true,"medical_record","lap_index","lap_index",true);
echo $tab->getHtml ();
?>