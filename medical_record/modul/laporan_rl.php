<?php
	$tab = new Tabulator ( "laporan_rl", "laporan_rl", Tabulator::$LANDSCAPE_RIGHT);
	$tab->add ( "rl11", "RL 1.1 - Identitas RS", "", Tabulator::$TYPE_HTML,"fa fa-file-o" );
	$tab->add ( "rl12", "RL 1.2 - Indikator Pelayanan", "", Tabulator::$TYPE_HTML,"fa fa-file-o" );
	$tab->add ( "rl13", "RL 1.3 - Tempat Tidur", "", Tabulator::$TYPE_HTML,"fa fa-file-o" );
	$tab->add ( "rl2", "RL 2 - Ketenagaan", "", Tabulator::$TYPE_HTML,"fa fa-file-o" );
	$tab->add ( "rl31", "RL 3.1 - Kegiatan Pelayanan Rawat Inap", "", Tabulator::$TYPE_HTML,"fa fa-file-o" );
	$tab->add ( "rl32", "RL 3.2 - Kunjungan Rawat Darurat", "", Tabulator::$TYPE_HTML,"fa fa-file-o" );
	$tab->add ( "rl33", "RL 3.3 - Gigi & Mulut", "", Tabulator::$TYPE_HTML,"fa fa-file-o" );
	$tab->add ( "rl34", "RL 3.4 - Kebidanan", "", Tabulator::$TYPE_HTML,"fa fa-file-o" );
	$tab->add ( "rl35", "RL 3.5 - Perinatologi", "", Tabulator::$TYPE_HTML,"fa fa-file-o" );
	$tab->add ( "rl36", "RL 3.6 - Pembedahan", "", Tabulator::$TYPE_HTML,"fa fa-file-o" );
	$tab->add ( "rl37", "RL 3.7 - Radiology", "", Tabulator::$TYPE_HTML,"fa fa-file-o" );
	$tab->add ( "rl38", "RL 3.8 - Laboratorium", "", Tabulator::$TYPE_HTML,"fa fa-file-o" );
	$tab->add ( "rl39", "RL 3.9 - Rehabilitasi Medik", "", Tabulator::$TYPE_HTML,"fa fa-file-o" );
	$tab->add ( "rl310", "RL 3.10 - Pelayanan Khusus", "", Tabulator::$TYPE_HTML,"fa fa-file-o" );
	$tab->add ( "rl311", "RL 3.11 - Kesehatan Jiwa", "", Tabulator::$TYPE_HTML,"fa fa-file-o" );
	$tab->add ( "rl312", "RL 3.12 - Keluarga Berencana", "", Tabulator::$TYPE_HTML,"fa fa-file-o" );
	$tab->add ( "rl313", "RL 3.13 - Farmasi Rumah Sakit", "", Tabulator::$TYPE_HTML,"fa fa-file-o" );
	$tab->add ( "rl314", "RL 3.14 - Rujukan", "", Tabulator::$TYPE_HTML,"fa fa-file-o" );
	$tab->add ( "rl315", "RL 3.15 - Cara Bayar", "", Tabulator::$TYPE_HTML,"fa fa-file-o" );
	$tab->add ( "rl4a_1", "RL 4A - Penyakit Rawat Inap", "", Tabulator::$TYPE_HTML,"fa fa-file-o" );
	$tab->add ( "rl4a_2", "RL 4A - Penyakit Rawat Inap (Sebab Luar)", "", Tabulator::$TYPE_HTML,"fa fa-file-o" );
	$tab->add ( "rl4b_1", "RL 4B - Penyakit Rawat Jalan", "", Tabulator::$TYPE_HTML,"fa fa-file-o" );
	$tab->add ( "rl4b_2", "RL 4B - Penyakit Rawat Jalan (Sebab Luar)", "", Tabulator::$TYPE_HTML,"fa fa-file-o" );
	$tab->add ( "rl51", "RL 5.1 - Pengunjung", "", Tabulator::$TYPE_HTML,"fa fa-file-o" );
	$tab->add ( "rl52", "RL 5.2 - Kunjungan Rawat Jalan", "", Tabulator::$TYPE_HTML,"fa fa-file-o" );
	$tab->add ( "rl521", "RL 5.2.1 - Kunjungan Rawat Inap", "", Tabulator::$TYPE_HTML,"fa fa-file-o" );
	$tab->add ( "rl53", "RL 5.3 - 10 Besar Penyakit Rawat Inap", "", Tabulator::$TYPE_HTML,"fa fa-file-o" );
    $tab->add ( "rl5_4", "RL 5.4 - 10 Besar Penyakit Rawat Jalan", "", Tabulator::$TYPE_HTML,"fa fa-file-o" );
	
	$tab->setPartialLoad(true, "medical_record", "laporan_rl", "laporan_rl",true);
	echo $tab->getHtml ();
	
	
?>