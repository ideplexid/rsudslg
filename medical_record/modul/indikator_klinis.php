<?php
$tab = new Tabulator ( "inklin", "inklin", Tabulator::$LANDSCAPE_RIGHT);
$tab->add ( "decubitus", "Decubitus", "", Tabulator::$TYPE_HTML," fa fa-bed" );
$tab->add ( "infus", "Infeksi Infus", "", Tabulator::$TYPE_HTML ," fa fa-tint");
$tab->add ( "operasi", "Infeksi Luka Operasi", "", Tabulator::$TYPE_HTML ," fa fa-scissors");
$tab->add ( "isk", "Pasien ISK", "", Tabulator::$TYPE_HTML ,"fa fa-wheelchair");
$tab->add ( "elektif", "Masa Tunggu Operasi", "", Tabulator::$TYPE_HTML ,"fa fa-clock-o");
$tab->add ( "bayi", "Bayi Baru Lahir", "", Tabulator::$TYPE_HTML ,"fa fa-pie-chart");
$tab->add ( "sentinel", "Sentinel", "", Tabulator::$TYPE_HTML ,"fa fa-thumb-tack");
$tab->add ( "nyaris", "Nyaris Cedera", "", Tabulator::$TYPE_HTML ," fa fa-warning");
$tab->add ( "cedera", "Cedera", "", Tabulator::$TYPE_HTML ," fa fa-heart");
$tab->add ( "transfusi", "Transfusi Darah", "", Tabulator::$TYPE_HTML ," fa fa-stethoscope");
$tab->add ( "lap_transfusi", "Laporan Transfusi", "", Tabulator::$TYPE_HTML ," fa fa-stethoscope");
$tab->setPartialLoad(true, "medical_record", "inklin", "inklin",true);
echo $tab->getHtml ();

?>