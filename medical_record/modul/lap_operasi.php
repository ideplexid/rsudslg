<?php
$tab = new Tabulator ( "lap_operasi", "" );
$tab->add ( "lap_operasi_kamar", "Laporan Kamar Operasi", "", Tabulator::$TYPE_HTML," fa fa-bed" );
$tab->add ( "lap_operasi_anastesi", "Laporan Anastesi", "", Tabulator::$TYPE_HTML," fa fa-stethoscope" );
$tab->add ( "lap_operasi_bedah", "Laporan Bedah", "", Tabulator::$TYPE_HTML," fa fa-scissors" );
$tab->setPartialLoad(true, "medical_record", "lap_operasi", "lap_operasi",true);	
echo $tab->getHtml ();
?>