<?php
	require_once 'smis-framework/smis/api/SettingsBuilder.php';
	global $db;	
	$smis = new SettingsBuilder ( $db, "mr_settings", "medical_record", "settings" );
	$smis->setShowDescription ( true );
	$smis->setTabulatorMode ( Tabulator::$POTRAIT );
	
	$smis->addTabs ( "pasien", "Pasien"," fa fa-user" );
    if($smis->isGroup("pasien")){
        $smis->addItem ( "pasien", new SettingsItem ( $db, "mr-label-nama", "Max Karakter Nama", "20", "text", "Nama" ) );
        $smis->addItem ( "pasien", new SettingsItem ( $db, "mr-label-alamat", "Max Karakter Alamat", "20", "text", "Alamat" ) );
        $smis->addItem ( "pasien", new SettingsItem ( $db, "mr-label-css", "CSS Label", "", "textarea", "CSS Label" ) );
        $smis->addItem ( "pasien", new SettingsItem ( $db, "mr-label-js", "JS Labe", "", "textarea", "JS Label" ) );
        $smis->addItem ( "pasien", new SettingsItem ( $db, "mr-label-repeat", "Jumlah Label di Cetak", "1", "text", "Berapa Kali Jumlah Label di Cetak , default 1 " ) );
    }
	
	$smis->addTabs ( "input_data", "Input Data","fa fa-list" );
    if($smis->isGroup("input_data")){
        $smis->addItem ( "input_data", new SettingsItem ( $db, "smis-rs-igd-all-area", "Bisa Menghapus Di semua Area", "", "checkbox", "jika ini diaktifkan maka semua area di semua ruangan bisa menghapus diagnosa walaupun berbeda ruangan dan berbeda nomor registrasi" ) );
        $smis->addItem ( "input_data", new SettingsItem ( $db, "smis-rs-igd-diagnosa-icd", "Aktifkan Diagnosa Mengikuti ICD", "", "checkbox", "jika di centang diagnosa langsung mengikuti ICD" ) );
        $smis->addItem ( "input_data", new SettingsItem ( $db, "smis-rs-igd-diagnosa-edit", "Aktifkan Edit Diagnosa di Rekam Medis", "", "checkbox", "jika aktif, maka rekam medis dapat mengubah diagnosa pasien yang manual" ) );
        $smis->addItem ( "input_data", new SettingsItem ( $db, "smis-rs-igd-mr", "Slug dari IGD", "", "text", "Slug IGD untuk kepentingan Pasien Rawat Inap IGD" ) );
        $smis->addItem ( "input_data", new SettingsItem ( $db, "smis-rs-igd-icu", "Slug dari ICU", "", "text", "Slug ICU untuk kepentingan Pasien Rawat Inap ICU" ) );
        $smis->addItem ( "input_data", new SettingsItem ( $db, "smis-rs-igd-ok", "Slug dari OK", "", "text", "Slug OK untuk kepentingan Pasien Rawat Inap OK" ) );
        $smis->addItem ( "input_data", new SettingsItem ( $db, "smis-rs-igd-vk", "Slug dari VK", "", "text", "Slug VK untuk kepentingan Pasien Rawat Inap VK" ) );
        $smis->addItem ( "input_data", new SettingsItem ( $db, "smis-mr-tambah-laporan-igd", "Menambahkan Data untuk Laporan IGD Melalui Rekam Medis", "0", "checkbox", "Jika dicentang maka Rekam Medis diperbolehkan untuk menambah data laporan IGD pada menu Rekam Medis > Input Data > IGD." ) );    
        $smis->addItem ( "input_data", new SettingsItem ( $db, "smis-mr-auto-diagnosa-bpjs", "Aktifkan Auto Update Diagnosa BPJS", "0", "checkbox", "Jika Menu Ini Aktif maka diagnosa bpjs akan secara otomatis di update dari yang terakhir" ) );    
        $smis->addItem ( "input_data", new SettingsItem ( $db, "smis-mr-crud-diagnosa-pasien", "Mengelola Data Diagnosa Pasien Melalui Rekam Medis", "0", "checkbox", "Jika dicentang maka Rekam Medis diperbolehkan untuk menambah, menghapus, memperbaharui, dan melihat data diagnosa pasien pada menu Rekam Medis > Input Data > Diagnosa. Jika tidak dicentang maka Rekam Medis hanya diperbolehkan memperbaharui dan melihat data diagnosa pasien." ) );    
        $smis->addItem ( "input_data", new SettingsItem ( $db, "smis-mr-tambah-tindakan-dokter", "Menambahkan Tindakan Dokter Melalui Rekam Medis", "0", "checkbox", "Jika dicentang maka Rekam Medis diperbolehkan untuk menambah tindakan dokter pada menu Rekam Medis > Input Data > Tindakan." ) );
        $smis->addItem ( "input_data", new SettingsItem ( $db, "smis-mr-jumlah-bed", "Input Data Total Bed", "", "text", "Input Data Total Bed" ) );
    }
    
    $smis->addTabs ( "assessment_dokter", "Assesment Dokter","fa fa-file" );
    if($smis->isGroup("assessment_dokter")){
        $smis->addItem ( "assessment_dokter", new SettingsItem ( $db, "smis-assesment-dokter-poligigi-odontogram", "File Gambar Odontogram", "", "file-single-image", "File Gambar Default Untuk Odontogram di Assesment Dokter Poli Gigi & Mulut" ) );
        $smis->addItem ( "assessment_dokter", new SettingsItem ( $db, "smis-assesment-dokter-polibedah-fisik", "File Gambar Pemeriksaan Fisik", "", "file-single-image", "File Gambar Default Untuk Pemerksaan Fisik di Assesment Dokter Poli Bedah" ) );       
        $smis->addItem ( "assessment_dokter", new SettingsItem ( $db, "smis-assesment-dokter-poliurologi-fisik", "File Gambar Pemeriksaan Fisik", "", "file-single-image", "File Gambar Default Untuk Pemerksaan Fisik di Assesment Dokter Poli Bedah Urologi" ) );
        $smis->addItem ( "assessment_dokter", new SettingsItem ( $db, "smis-assesment-dokter-polidalam-paru", "File Gambar Paru", "", "file-single-image", "File Gambar Paru di Assesment Dokter Poli Dalam" ) );
        $smis->addItem ( "assessment_dokter", new SettingsItem ( $db, "smis-assesment-dokter-polidalam-perut", "File Gambar Perut", "", "file-single-image", "File Gambar Perut di Assesment Dokter Poli Dalam" ) );
        $smis->addItem ( "assessment_dokter", new SettingsItem ( $db, "smis-assesment-dokter-polikulit-dermatologi", "File Gambar Dermatologi", "", "file-single-image", "File Gambar Dermatologi di Assesment Dokter Poli Kulit dan Kelamin" ) );
        $smis->addItem ( "assessment_dokter", new SettingsItem ( $db, "smis-assesment-dokter-poliorthopedi-neurologis", "File Gambar Neurologis", "", "file-single-image", "File Gambar neurologis di Assesment Dokter Poli Orthopedi" ) );
        $smis->addItem ( "assessment_dokter", new SettingsItem ( $db, "smis-assesment-dokter-poliparu-paru", "File Gambar Paru", "", "file-single-image", "File Gambar Paru di Assesment Dokter Poli Paru" ) );
        $smis->addItem ( "assessment_dokter", new SettingsItem ( $db, "smis-assesment-dokter-poliparu-perut", "File Gambar Perut", "", "file-single-image", "File Gambar Perut di Assesment Dokter Poli Paru" ) );
        $smis->addItem ( "assessment_dokter", new SettingsItem ( $db, "smis-assesment-dokter-polibedahanak-fisik", "File Gambar Pemeriksaan Fisik", "", "file-single-image", "File Gambar Default Untuk Pemerksaan Fisik di Assesment Dokter Poli Bedah Anak" ) );
        $smis->addItem ( "assessment_dokter", new SettingsItem ( $db, "smis-assesment-dokter-polimata-pergerakan-bola-mata", "File Gambar Pergerakan Bola Mata", "", "file-single-image", "File Gambar Default Untuk Pergerakan Bola Mata di Assesment Dokter Poli Mata" ) );
        $smis->addItem ( "assessment_dokter", new SettingsItem ( $db, "smis-assesment-dokter-polimata-segmen-anterior-ods", "File Gambar Segmen Anterior ODS", "", "file-single-image", "File Gambar Default Untuk Segmen Anterior ODS di Assesment Dokter Poli Mata" ) );
        $smis->addItem ( "assessment_dokter", new SettingsItem ( $db, "smis-assesment-dokter-polimata-funduskopi-ods", "File Gambar Funduskopi ODS", "", "file-single-image", "File Gambar Default Untuk Funduskopi ODS di Assesment Dokter Poli Mata" ) );
    }
    
	
	$smis->addTabs ( "operasi", "Operasi"," fa fa-scissors" );
    if($smis->isGroup("operasi")){
        $option=new OptionBuilder();
        $smis->addItem ( "operasi", new SettingsItem ( $db, "mr-operasi-dokter-jaga", "Tampilkan Dokter Jaga", "", "checkbox", "Tampilkan Dokter Jaga" ) );
        $smis->addItem ( "operasi", new SettingsItem ( $db, "mr-slug-ruang-operasi", "Nama Slug Ruang Operasi", "", "text", "Prototype Slug dari Ruang Operasi" ) );
    }
	
	$smis->addTabs ( "laporan", "Laporan"," fa fa-book" );
    if($smis->isGroup("laporan")){
        $smis->addItem ( "laporan", new SettingsItem ( $db, "smis-rs-autosave-lap-kb", "Auto Save Laporan KB", "0", "checkbox", "Auto Save Laporan KB" ) );
        $smis->addItem ( "laporan", new SettingsItem ( $db, "smis-rs-autosave-lap-persalinan", "Auto Save Laporan Persalinan", "0", "checkbox", "Auto Save Laporan Persalinan" ) );
        $smis->addItem ( "laporan", new SettingsItem ( $db, "smis-rs-autosave-lap-igd", "Auto Save Laporan IGD", "1", "checkbox", "Auto Save Laporan Persalinan" ) );	
        $smis->addItem ( "laporan", new SettingsItem ( $db, "smis-rs-autosave-lap-operasi", "Auto Save Laporan Operasi", "1", "checkbox", "Auto Save Laporan Operasi" ) );
    }
    
    $smis->addTabs ( "laporan_rl", "Laporan RL"," fa fa-book" );
    if($smis->isGroup("laporan_rl")){
        $smis->addItem ( "laporan_rl", new SettingsItem ( $db, "smis-mr-lap-rl-tampil-filter-bor", "Menampilkan Filter BOS pada Laporan RL 1.2 Indikator Pelayanan", "0", "checkbox", "Menampilkan Filter BOS pada Laporan RL 1.2 Indikator Pelayanan" ) );
        $smis->addItem ( "laporan_rl", new SettingsItem ( $db, "smis-mr-lap-rl-tampil-filter-alos", "Menampilkan Filter ALOS pada Laporan RL 1.2 Indikator Pelayanan", "0", "checkbox", "Menampilkan Filter ALOS pada Laporan RL 1.2 Indikator Pelayanan" ) );
        $smis->addItem ( "laporan_rl", new SettingsItem ( $db, "smis-mr-lap-rl-tampil-filter-toi", "Menampilkan Filter TOI pada Laporan RL 1.2 Indikator Pelayanan", "0", "checkbox", "Menampilkan Filter TOI pada Laporan RL 1.2 Indikator Pelayanan" ) );
        $smis->addItem ( "laporan_rl", new SettingsItem ( $db, "smis-mr-lap-rl-tampil-filter-bto", "Menampilkan Filter BTO pada Laporan RL 1.2 Indikator Pelayanan", "0", "checkbox", "Menampilkan Filter BTO pada Laporan RL 1.2 Indikator Pelayanan" ) );
        $smis->addItem ( "laporan_rl", new SettingsItem ( $db, "smis-mr-lap-rl-tampil-filter-ndr", "Menampilkan Filter NDR pada Laporan RL 1.2 Indikator Pelayanan", "0", "checkbox", "Menampilkan Filter NDR pada Laporan RL 1.2 Indikator Pelayanan" ) );
        $smis->addItem ( "laporan_rl", new SettingsItem ( $db, "smis-mr-lap-rl-tampil-filter-gdr", "Menampilkan Filter GDR pada Laporan RL 1.2 Indikator Pelayanan", "0", "checkbox", "Menampilkan Filter GDR pada Laporan RL 1.2 Indikator Pelayanan" ) );
        $smis->addItem ( "laporan_rl", new SettingsItem ( $db, "smis-mr-lap-rl-tampil-laporan-rl5_4", "Menampilkan Laporan RL 5.4 Penyakit Baru Rawat Jalan", "0", "checkbox", "Menampilkan Laporan RL 5.4 Penyakit Baru Rawat Jalan" ) );
    }
    
    $smis->addTabs ( "laporan_pasien", "Laporan Pasien"," fa fa-book" );
    if($smis->isGroup("laporan_pasien")){
        $smis->addItem ( "laporan_pasien", new SettingsItem ( $db, "smis-mr-slug-igd", "Slug IGD", "", "text", "Slug Prototype IGD untuk keperluan Laporan pada Rekam Medis" ) );
        $smis->addItem ( "laporan_pasien", new SettingsItem ( $db, "smis-mr-lap-pasien-tampil-sensus-range", "Menampilkan Laporan Sensus dengan Filter Range Tanggal Dari - Tanggal Sampai", "0", "checkbox", "Menampilkan Laporan Sensus dengan Filter Range Tanggal Dari - Tanggal Sampai" ) );
        $smis->addItem ( "laporan_pasien", new SettingsItem ( $db, "smis-mr-lap-pasien-tampil-sensus-tgl", "Menampilkan Laporan Sensus per Tanggal", "1", "checkbox", "Menampilkan Laporan Sensus per Tanggal" ) );
        $smis->addItem ( "laporan_pasien", new SettingsItem ( $db, "smis-mr-lap-pasien-tampil-morbiditas-grup", "Menampilkan Laporan Morbiditas yang Dikelompokkan Berdasarkan No.DTD", "0", "checkbox", "Menampilkan Laporan Morbiditas yang Dikelompokkan Berdasarkan No.DTD" ) );
    }
    
    $smis->addTabs ( "laporan_assesmen", "Laporan Assesmen"," fa fa-book" );
    if($smis->isGroup("laporan_assesmen")){
        $smis->addItem ( "laporan_assesmen", new SettingsItem ( $db, "smis-mr-header-lap-assesmen", "Header Laporan Assesmen", "", "file-single-image", "Header Laporan Assesmen" ) );
    }
    
    $smis->setPartialLoad(true);
	$smis->init ();
?>