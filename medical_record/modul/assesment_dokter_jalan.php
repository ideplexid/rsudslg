<?php 
    global $db;
    global $user;
    
    if(isset($_POST['super_command'])){
        require_once "smis-base/smis-include-service-consumer.php";
        $uitable = new Table(array("No.","Nama","NRM","No Reg","Alamat","Ruangan"));
        $uitable ->setName("pasien_dokter_jalan");
        $uitable ->setModel(Table::$SELECT);
        $adapter = new SimpleAdapter();
        $adapter ->setUseNumber(true,"No.","back.")
                 ->add("Nama","nama_pasien")
                 ->add("NRM","nrm")
                 ->add("No Reg","id")
                 ->add("Alamat","alamat_pasien")
                 ->add("Ruangan","jenislayanan","unslug");
        $service = new ServiceResponder($db,$uitable,$adapter,"get_registered","registration");
        $super   = new SuperCommand();
        $super   ->addResponder("pasien_dokter_jalan",$service);
        $data    = $super->initialize();
        echo $data;
        return;
    }

    $status = new OptionBuilder();
    $status ->add("","",1)
            ->add("Posted","1")
            ->add("Un Posted","0");
    
    $uitable = new Table(array());
    $uitable ->setName("assesment_dokter_jalan")
             ->addModal("nama_pasien","chooser-assesment_dokter_jalan-pasien_dokter_jalan-Pilih Pasien","Nama Pasien","")
             ->addModal("nrm_pasien","text","NRM","","n",null,true)
             ->addModal("noreg_pasien","text","No. Reg","","n",null,true)
             ->addModal("carabayar","text","Carabayar","","n",null,true);
    
    $form    = $uitable->getModal()->getForm();
    echo addJS("medical_record/resource/js/assesment_dokter_jalan.js",false);
    echo addJS("medical_record/resource/js/asessment_dokter_jalan_action.js",false);
    
    $tab = new Tabulator("assesment_dokter_jalan", "assesment_dokter_jalan",Tabulator::$LANDSCAPE_RIGHT);
    require_once "medical_record/function/get_detail_assessment_rajal.php";
    $list = get_detail_assessment_rajal();
    foreach($list as $name=>$value){
        $settings = "mr-asm-rajal-".$name."-".$user->getUsername();
        if(getSettings($db,$settings,"edit")!="hide"){
            $file   = "assessment_".$name;
            $tab ->add($file,$value,"",Tabulator::$TYPE_HTML,"fa fa-file");    
        }
    }   
    $tab ->setPartialLoad(true,"medical_record","assesment_dokter_jalan","assesment_dokter_jalan",false);
     
    echo $form->getHtml();
    echo "<div class='clear'></div>";
    echo $tab->getHtml();
?>