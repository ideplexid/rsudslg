<?php
global $db;

$pasien_table = new Table(
    array("NRM", "Nama", "Jenis Kelamin", "Alamat"),
    "",
    null,
    true
);
$pasien_table->setName("pasien");
$pasien_table->setModel(Table::$SELECT);
$pasien_dbtable = new DBTable($db, "smis_rg_patient", array("id", "nama", "alamat"));
$pasien_dbtable->setOrder("id * 1 DESC");
$pasien_adapter = new SimpleAdapter();
$pasien_adapter->add("NRM", "id", "digit6");
$pasien_adapter->add("Nama", "nama");
$pasien_adapter->add("Jenis Kelamin", "kelamin", "kelamin_0_Laki-Laki_Perempuan");
$pasien_adapter->add("Alamat", "alamat");
$pasien_dbresponder = new DBResponder(
    $pasien_dbtable,
    $pasien_table,
    $pasien_adapter
);
$super_command = new SuperCommand();
$super_command->addResponder("pasien", $pasien_dbresponder);
$init = $super_command->initialize();
if ($init != null) {
    echo $init;
    return;
}

$table = new Table(
    array("Kunjungan Ke", "Tanggal Registrasi", "Tanggal Input", "No. Register", "Umur", "Kode Diagnosa Primer", "Diagnosa Primer", "Kode Diagnosa Sekunder", "Diagnosa Sekunder", "Kode ICD Tindakan", "ICD Tindakan", "Ruangan", "Tanggal Keluar"),
    "",
    null,
    true
);
$table->setName("resume_diagnosa_pasien");
$table->setAction(false);

if (isset($_POST['command'])) {
    $adapter = new SimpleAdapter();
    $adapter->add("Kunjungan Ke", "no_kunjungan");
    $adapter->add("Tanggal Registrasi", "tanggal_registrasi", "date d M Y");
    $adapter->add("Tanggal Input", "tanggal_input", "date d M Y");
    $adapter->add("No. Register", "noreg_pasien");
    $adapter->add("Umur", "umur");
    $adapter->add("Kode Diagnosa Primer", "kode_icd");
    $adapter->add("Diagnosa Primer", "nama_icd");
    $adapter->add("Kode Diagnosa Sekunder", "kode_icd_diagnosa_sekunder1");
    $adapter->add("Diagnosa Sekunder", "diagnosa_sekunder1");
    $adapter->add("Kode ICD Tindakan", "kode_icd_tindakan");
    $adapter->add("ICD Tindakan", "diagnosa_tindakan");
    $adapter->add("Ruangan", "ruangan");
    $adapter->add("Tanggal Keluar", "tanggal_pulang", "date d M Y");

    $dbtable = new DBTable($db, "smis_mr_diagnosa");
    $filter = "";
    if (isset($_POST['kriteria']))
        $filter .= " AND (
                a.kode_icd LIKE '%" . $_POST['kriteria'] . "%' OR
                a.nama_icd LIKE '%" . $_POST['kriteria'] . "%' OR
                a.kode_icd_diagnosa_sekunder1 LIKE '%" . $_POST['kriteria'] . "%' OR
                a.diagnosa_sekunder1 LIKE '%" . $_POST['kriteria'] . "%' OR
                a.kode_icd_tindakan LIKE '%" . $_POST['kriteria'] . "%' OR
                a.diagnosa_tindakan LIKE '%" . $_POST['kriteria'] . "%' OR
                c.nama LIKE '%" . $_POST['kriteria'] . "%'
            ) ";
    $query_value = "
            SELECT
                b.no_kunjungan,
                b.tanggal tanggal_registrasi,
                a.tanggal tanggal_input,
                a.noreg_pasien,
                b.umur,
                a.kode_icd,
                a.nama_icd,
                a.kode_icd_diagnosa_sekunder1,
                a.diagnosa_sekunder1,
                a.kode_icd_tindakan,
                a.diagnosa_tindakan,
                c.nama ruangan,
                b.tanggal_pulang
            FROM
                smis_mr_diagnosa a 
                    LEFT JOIN smis_rg_layananpasien b ON a.noreg_pasien = b.id 
                    LEFT JOIN smis_adm_prototype c ON a.ruangan = c.slug
            WHERE
                a.prop = '' AND c.parent = 'rawat' AND a.nrm_pasien = '" . $_POST['nrm_pasien'] . "' " . $filter . "
            ORDER BY
                b.no_kunjungan DESC, b.tanggal DESC, a.tanggal DESC, a.noreg_pasien DESC
        ";
    $query_count = "
            SELECT
                COUNT(*)
            FROM (   
                " . $query_value . "
            ) v
        ";
    $dbtable->setPreferredQuery(true, $query_value, $query_count);

    $dbresponder = new DBResponder(
        $dbtable,
        $table,
        $adapter
    );
    $data = $dbresponder->command($_POST['command']);
    echo json_encode($data);

    return;
}

$form = new Form("", "", "Resume Diagnosa Pasien");
$pasien_button = new Button("", "", "Pilih");
$pasien_button->setClass("btn-info");
$pasien_button->setIsButton(Button::$ICONIC);
$pasien_button->setIcon("icon-white " . Button::$icon_list_alt);
$pasien_button->setAction("pasien.chooser('pasien', 'pasien_button', 'pasien', pasien)");
$pasien_button->setAtribute("id='pasien_browse'");
$pasien_text = new Text("rdp_nama_pasien", "rdp_nama_pasien", "");
$pasien_text->setAtribute("disabled='disabled'");
$pasien_text->setClass("smis-one-option-input");
$pasien_input_group = new InputGroup("");
$pasien_input_group->addComponent($pasien_text);
$pasien_input_group->addComponent($pasien_button);
$form->addElement("Nama", $pasien_input_group);
$nrm_pasien_text = new Text("rdp_nrm_pasien", "rdp_nrm_pasien", "");
$nrm_pasien_text->setAtribute("disabled='disabled'");
$form->addElement("NRM", $nrm_pasien_text);
$alamat_textarea = new TextArea("rdp_alamat", "rdp_alamat", "");
$alamat_textarea->setAtribute("disabled");
$alamat_textarea->setLine(2);
$form->addElement("Alamat", $alamat_textarea);

echo $form->getHtml();
echo $table->getHtml();
echo addJS("framework/smis/js/table_action.js");
?>
<script type="text/javascript">
    var resume_diagnosa_pasien;
    var pasien;

    $(document).ready(function() {
        resume_diagnosa_pasien = new TableAction(
            "resume_diagnosa_pasien",
            "medical_record",
            "resume_diagnosa_pasien",
            new Array()
        );
        resume_diagnosa_pasien.addViewData = function(data) {
            data['nrm_pasien'] = $("#rdp_nrm_pasien").val();
            return data;
        };

        pasien = new TableAction(
            "pasien",
            "medical_record",
            "resume_diagnosa_pasien",
            new Array("id", "nama", "alamat")
        );
        pasien.setSuperCommand("pasien");
        pasien.selected = function(json) {
            $("#rdp_nrm_pasien").val(json.id);
            $("#rdp_nama_pasien").val(json.nama);
            $("#rdp_jenis_kelamin").val(json.kelamin);
            $("#rdp_alamat").val(json.alamat);
            resume_diagnosa_pasien.view();
        };

        $("#resume_diagnosa_pasien_pagination").html("");
    });
</script>
<style text="text/css">
    textarea {
        resize: none;
    }
</style>