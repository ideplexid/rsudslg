<?php 


$tabs=new Tabulator("data_induk","data_induk",Tabulator::$LANDSCAPE_RIGHT);
$tabs->add("jenis_rs", "Jenis RS", "", Tabulator::$TYPE_HTML, "fa fa-building-o");
$tabs->add("kepemilikan_rs", "Kepemilikan RS", "", Tabulator::$TYPE_HTML, "fa fa-building-o");
$tabs->add("penyelenggara_rs", "Penyelenggara RS", "", Tabulator::$TYPE_HTML, "fa fa-building-o");
$tabs->add("icdtindakan","ICD Tindakan","",Tabulator::$TYPE_HTML,"fa fa-heart");
$tabs->add("icd","ICD Penyakit","",Tabulator::$TYPE_HTML,"fa fa-heartbeat");
$tabs->add("icd_rehab_medis", "ICD Rehab Medik", "", Tabulator::$TYPE_HTML, "fa fa-heartbeat");
$tabs->add("jenis_kegiatan_rl52","Jenis Kegiatan - RL 5.2 Kunjungan Rawat Jalan","",Tabulator::$TYPE_HTML,"fa fa-file-o");
$tabs->add("pasien","Pasien","",Tabulator::$TYPE_HTML,"fa fa-user");
//$tabs->add("gambar","Gambar","",Tabulator::$TYPE_HTML,"fa fa-user");
$tabs->add("management_bed","Management Bed","",Tabulator::$TYPE_HTML,"fa fa-bed");

$tabs->setPartialLoad(true, "medical_record", "data_induk", "data_induk",true);
echo $tabs->getHtml();
?>