<?php

$tab = new Tabulator ( "laporan_rm", "laporan_rm", Tabulator::$LANDSCAPE_RIGHT);
$tab->add ( "lap_index_kematian", "Laporan Index Kematian", "", Tabulator::$TYPE_HTML,"fa fa-ambulance" );
$tab->add ( "lap_index_dokter", "Laporan Index Dokter", "", Tabulator::$TYPE_HTML,"fa fa-user-md" );
$tab->add ( "laporan_index_penyakit", "Laporan Index Penyakit", "", Tabulator::$TYPE_HTML,"fa fa-medkit" );
$tab->add ( "lap_index_operasi", "Laporan Index Operasi", "", Tabulator::$TYPE_HTML,"fa fa-heartbeat" );
$tab->add ( "lap_index_ranap", "Laporan Index Rawat Inap", "", Tabulator::$TYPE_HTML,"fa fa-bed" );
$tab->add ( "lap_index_rajal", "Laporan Index Rawat Jalan", "", Tabulator::$TYPE_HTML,"fa fa-blind" );

$tab->setPartialLoad(true, "medical_record", "laporan_rm", "laporan_rm",true);
echo $tab->getHtml ();

?>