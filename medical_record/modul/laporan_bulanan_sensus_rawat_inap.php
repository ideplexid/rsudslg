<?php
    require_once "smis-base/smis-include-service-consumer.php";
	global $db;

    if (isset($_POST['command'])) {
        if ($_POST['command'] == "list") {
            $bulan = $_POST['bulan'];
            $tahun = $_POST['tahun'];
            $ruangan = $_POST['ruangan'];
            $days = cal_days_in_month( 0, $bulan, $tahun);
            if ($bulan == date('m') && $tahun == date('Y'))
                $days = date('d');

            $html = "";
            $nomor = 1;
            for ($i = 1; $i <= $days; $i++) {
                $tanggal = $tahun . "-" . $bulan . "-" . ArrayAdapter::format("only-digit2", $i);

                $pasien_awal_row = $db->get_row("
                    SELECT COUNT(*) jumlah
                    FROM smis_rwt_antrian_" . $ruangan . "
                    WHERE prop = '' AND (
                        (DATE(waktu) < '" . $tanggal . "' AND waktu_keluar = '0000-00-00 00:00:00') OR 
                        (DATE(waktu_keluar) = '" . $tanggal . "')
                    )
                ");
                $jumlah_pasien_awal = 0;
                if ($pasien_awal_row != null)
                    $jumlah_pasien_awal = $pasien_awal_row->jumlah;

                $pasien_masuk_row = $db->get_row("
                    SELECT COUNT(*) jumlah
                    FROM smis_rwt_antrian_" . $ruangan . "
                    WHERE prop = '' AND DATE(waktu) = '" . $tanggal . "' AND (
                        asal LIKE 'POLI%' OR asal LIKE 'IGD' OR asal LIKE 'Pendaftaran'
                    )
                ");
                $jumlah_pasien_masuk = 0;
                if ($pasien_masuk_row != null)
                    $jumlah_pasien_masuk = $pasien_masuk_row->jumlah;

                $pasien_pindahan_row = $db->get_row("
                    SELECT COUNT(*) jumlah
                    FROM smis_rwt_antrian_" . $ruangan . "
                    WHERE prop = '' AND DATE(waktu) = '" . $tanggal . "' AND (
                        asal LIKE 'RUANG%'
                    )
                ");
                $jumlah_pasien_pindahan = 0;
                if ($pasien_pindahan_row != null)
                    $jumlah_pasien_pindahan = $pasien_pindahan_row->jumlah;

                $total_pasien_masuk = $jumlah_pasien_masuk + $jumlah_pasien_pindahan;

                $pasien_dipindahkan_row = $db->get_row("
                    SELECT COUNT(*) jumlah
                    FROM smis_rwt_antrian_" . $ruangan . "
                    WHERE prop = '' AND DATE(waktu_keluar) = '" . $tanggal . "' AND (
                        ruang_tujuan NOT LIKE '' AND cara_keluar LIKE 'Pindah Kamar'
                    )
                ");
                $jumlah_pasien_dipindahkan = 0;
                if ($pasien_dipindahkan_row != null)
                    $jumlah_pasien_dipindahkan = $pasien_dipindahkan_row->jumlah;

                $pasien_pulang_hidup = $db->get_row("
                    SELECT COUNT(*) jumlah
                    FROM smis_rwt_antrian_" . $ruangan . "
                    WHERE prop = '' AND DATE(waktu_keluar) = '" . $tanggal . "' AND cara_keluar LIKE 'Dipulangkan Hidup'
                ");
                $jumlah_pasien_pulang_hidup = 0;
                if ($pasien_pulang_hidup != null)
                    $jumlah_pasien_pulang_hidup = $pasien_pulang_hidup->jumlah;

                $pasien_pulang_dirujuk = $db->get_row("
                    SELECT COUNT(*) jumlah
                    FROM smis_rwt_antrian_" . $ruangan . "
                    WHERE prop = '' AND DATE(waktu_keluar) = '" . $tanggal . "' AND cara_keluar LIKE 'Dirujuk'
                ");
                $jumlah_pasien_pulang_dirujuk = 0;
                if ($pasien_pulang_dirujuk != null)
                    $jumlah_pasien_pulang_dirujuk = $pasien_pulang_dirujuk->jumlah;

                $pasien_pulang_pindah_ke_rs_lain = $db->get_row("
                    SELECT COUNT(*) jumlah
                    FROM smis_rwt_antrian_" . $ruangan . "
                    WHERE prop = '' AND DATE(waktu_keluar) = '" . $tanggal . "' AND cara_keluar LIKE 'Pindah ke RS Lain'
                ");
                $jumlah_pasien_pulang_pindah_ke_rs_lain = 0;
                if ($pasien_pulang_pindah_ke_rs_lain != null)
                    $jumlah_pasien_pulang_pindah_ke_rs_lain = $pasien_pulang_pindah_ke_rs_lain->jumlah;

                $pasien_pulang_paksa = $db->get_row("
                    SELECT COUNT(*) jumlah
                    FROM smis_rwt_antrian_" . $ruangan . "
                    WHERE prop = '' AND DATE(waktu_keluar) = '" . $tanggal . "' AND cara_keluar LIKE 'Pulang Paksa'
                ");
                $jumlah_pasien_pulang_paksa = 0;
                if ($pasien_pulang_paksa != null)
                    $jumlah_pasien_pulang_paksa = $pasien_pulang_paksa->jumlah;

                $pasien_pulang_kabur = $db->get_row("
                    SELECT COUNT(*) jumlah
                    FROM smis_rwt_antrian_" . $ruangan . "
                    WHERE prop = '' AND DATE(waktu_keluar) = '" . $tanggal . "' AND cara_keluar LIKE 'Kabur'
                ");
                $jumlah_pasien_pulang_kabur = 0;
                if ($pasien_pulang_kabur != null)
                    $jumlah_pasien_pulang_kabur = $pasien_pulang_kabur->jumlah;

                $total_pasien_keluar_hidup = $jumlah_pasien_pulang_hidup + $jumlah_pasien_pulang_dirujuk + $jumlah_pasien_pulang_pindah_ke_rs_lain + $jumlah_pasien_pulang_paksa + $jumlah_pasien_pulang_kabur + $jumlah_pasien_dipindahkan;

                $pasien_mati_k48 = $db->get_row("
                    SELECT COUNT(*) jumlah
                    FROM smis_rwt_antrian_" . $ruangan . "
                    WHERE prop = '' AND DATE(waktu_keluar) = '" . $tanggal . "' AND (cara_keluar LIKE 'Dipulangkan Mati <= 48 Jam' OR cara_keluar LIKE 'Dipulangkan Mati <= 24 Jam')
                ");
                $jumlah_pasien_mati_k48 = 0;
                if ($pasien_mati_k48 != null)
                    $jumlah_pasien_mati_k48 = $pasien_mati_k48->jumlah;

                $pasien_mati_l48 = $db->get_row("
                    SELECT COUNT(*) jumlah
                    FROM smis_rwt_antrian_" . $ruangan . "
                    WHERE prop = '' AND DATE(waktu_keluar) = '" . $tanggal . "' AND cara_keluar LIKE 'Dipulangkan Mati > 48 Jam'
                ");
                $jumlah_pasien_mati_l48 = 0;
                if ($pasien_mati_l48 != null)
                    $jumlah_pasien_mati_l48 = $pasien_mati_l48->jumlah;

                $total_pasien_keluar = $total_pasien_keluar_hidup + $jumlah_pasien_mati_k48 + $jumlah_pasien_mati_l48;

                $pasien_masih_dirawat_row = $db->get_row("
                    SELECT COUNT(*) jumlah
                    FROM smis_rwt_antrian_" . $ruangan . "
                    WHERE prop = '' AND DATE(waktu) <= '" . $tanggal . "' AND waktu_keluar = '0000-00-00 00:00:00'
                ");
                $jumlah_pasien_masih_dirawat = 0;
                if ($pasien_masih_dirawat_row != null)
                    $jumlah_pasien_masih_dirawat = $pasien_masih_dirawat_row->jumlah;

                $pasien_masuk_keluar_hari_sama_row = $db->get_row("
                    SELECT COUNT(*) jumlah
                    FROM smis_rwt_antrian_" . $ruangan . "
                    WHERE prop = '' AND DATE(waktu) = '" . $tanggal . "' AND DATE(waktu_keluar) = DATE(waktu)
                ");
                $jumlah_pasien_masuk_keluar_hari_sama = 0;
                if ($pasien_masuk_keluar_hari_sama_row != null)
                    $jumlah_pasien_masuk_keluar_hari_sama = $pasien_masuk_keluar_hari_sama_row->jumlah;

                $lama_dirawat_rows = $db->get_result("
                    SELECT DATE(waktu) tanggal_mulai, DATE(waktu_keluar) tanggal_selesai
                    FROM smis_rwt_antrian_" . $ruangan . "
                    WHERE prop = '' AND cara_keluar NOT LIKE 'Tidak Datang' AND DATE(waktu_keluar) = '" . $tanggal . "';
                ");
                $total_los = 0;
                if ($lama_dirawat_rows != null) {
                    foreach ($lama_dirawat_rows as $ldr) {
                        $tanggal_mulai = strtotime(ArrayAdapter::format("date Y-m-d", $ldr->tanggal_mulai));
                        $tanggal_selesai = strtotime(ArrayAdapter::format("date Y-m-d", $ldr->tanggal_selesai));
                        $datediff = $tanggal_selesai - $tanggal_mulai;
                        $los = ceil($datediff / (60 * 60 * 24));
                        if ($los < 1)
                            $los = 1;
                        $total_los += $los;
                    }
                }

                $jumlah_pasien_aktif_row = $db->get_row("
                    SELECT 
                        COUNT(id) jumlah_hari
                    FROM 
                        smis_rwt_antrian_" . $ruangan . "
                    WHERE 
                        prop = '' AND 
                        cara_keluar NOT LIKE 'Tidak Datang' AND  
                        DATE(waktu) <= '" . $tanggal . "' AND 
                        waktu_keluar = '0000-00-00 00:00:00'
                ");
                $jumlah_pasien_pulang_setelah_tanggal_row = $db->get_row("
                    SELECT 
                        COUNT(id) jumlah_hari
                    FROM 
                        smis_rwt_antrian_" . $ruangan . "
                    WHERE 
                        prop = '' AND 
                        cara_keluar NOT LIKE 'Tidak Datang' AND 
                        DATE(waktu) <= '" . $tanggal . "' AND 
                        DATE(waktu_keluar) > '" . $tanggal . "'
                ");
                $jumlah_hp = 0;
                if ($jumlah_pasien_aktif_row != null)
                    $jumlah_hp += $jumlah_pasien_aktif_row->jumlah_hari;
                if ($jumlah_pasien_pulang_setelah_tanggal_row != null)
                    $jumlah_hp += $jumlah_pasien_pulang_setelah_tanggal_row->jumlah_hari;

                $html .= "
                    <tr>
                        <td>" . $nomor++ . "</td>
                        <td>" . ArrayAdapter::format("date d-m-Y", $tanggal) . "</td>
                        <td>" . ArrayAdapter::format("number", $jumlah_pasien_awal) . "</td>
                        <td>" . ArrayAdapter::format("number", $jumlah_pasien_masuk) . "</td>
                        <td>" . ArrayAdapter::format("number", $jumlah_pasien_pindahan) . "</td>
                        <td>" . ArrayAdapter::format("number", $total_pasien_masuk) . "</td>
                        <td>" . ArrayAdapter::format("number", $jumlah_pasien_dipindahkan) . "</td>
                        <td>" . ArrayAdapter::format("number", $jumlah_pasien_pulang_hidup) . "</td>
                        <td>" . ArrayAdapter::format("number", $jumlah_pasien_pulang_dirujuk) . "</td>
                        <td>" . ArrayAdapter::format("number", $jumlah_pasien_pulang_pindah_ke_rs_lain) . "</td>
                        <td>" . ArrayAdapter::format("number", $jumlah_pasien_pulang_paksa) . "</td>
                        <td>" . ArrayAdapter::format("number", $jumlah_pasien_pulang_kabur) . "</td>
                        <td>" . ArrayAdapter::format("number", $total_pasien_keluar_hidup) . "</td>
                        <td>" . ArrayAdapter::format("number", $jumlah_pasien_mati_k48) . "</td>
                        <td>" . ArrayAdapter::format("number", $jumlah_pasien_mati_l48) . "</td>
                        <td>" . ArrayAdapter::format("number", $total_pasien_keluar) . "</td>
                        <td>" . ArrayAdapter::format("number", $jumlah_pasien_masih_dirawat) . "</td>
                        <td>" . ArrayAdapter::format("number", $total_los) . "</td>
                        <td>" . ArrayAdapter::format("number", $jumlah_pasien_masuk_keluar_hari_sama) . "</td>
                        <td>" . ArrayAdapter::format("number", $jumlah_hp) . "</td>
                    </tr>
                ";
            }
            $data['html'] = $html;
            echo json_encode($data);
        } else if ($_POST['command'] == "export_xls") {
            $bulan = $_POST['bulan'];
            $bulan_label = $_POST['bulan_label'];
            $tahun = $_POST['tahun'];
            $ruangan = $_POST['ruangan'];
            $ruangan_label = $_POST['ruangan_label'];

            $data_sensus = json_decode($_POST['data_sensus'], true);

            require_once ("smis-libs-out/php-excel/PHPExcel.php");

            $file = new PHPExcel();
            $file->getProperties()->setCreator("PT. Inovasi Ide Utama");
            $file->getProperties()->setTitle("Rekap Sensus Harian Rawat Inap");
            $file->getProperties()->setSubject("Rekap Sensus Harian Rawat Inap");
            $file->getProperties()->setDescription("Rekap Sensus Harian Rawat Inap Generated by System");
            $file->getProperties()->setKeywords("Rekap Sensus Harian Rawat Inap");
            $file->getProperties()->setCategory("Rekap Sensus Harian Rawat Inap");

            $sheet = $file->getActiveSheet();
            $sheet->setTitle("Sensus Harian Rawat Inap");
            $i = 1;

            $sheet->mergeCells("A" . $i . ":T" . $i)->setCellValue("A" . $i, "Rekap Sensus Harian Rawat Inap");
            $sheet->getStyle("A" . $i . ":T" . $i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $sheet->getStyle("A" . $i)->getFont()->setBold(true);
            $i = $i + 1;
            $sheet->mergeCells("A" . $i . ":T" . $i)->setCellValue("A" . $i, "Bulan : " . $bulan_label . " " . $tahun);
            $sheet->getStyle("A" . $i . ":T" . $i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $sheet->getStyle("A" . $i)->getFont()->setBold(true);
            $i = $i + 1;
            $sheet->mergeCells("A" . $i . ":T" . $i)->setCellValue("A" . $i, "Ruangan : " . $ruangan_label);
            $sheet->getStyle("A" . $i . ":T" . $i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $sheet->getStyle("A" . $i)->getFont()->setBold(true);
            $i = $i + 1;
            $sheet->mergeCells("A" . $i . ":T" . $i)->setCellValue("A" . $i, "");
            $i = $i + 1;

            $border_start = $i;
            $sheet->mergeCells("A" . $i . ":A" . ($i + 2))->setCellValue("A" . $i, "No.");
            $sheet->mergeCells("B" . $i . ":B" . ($i + 2))->setCellValue("B" . $i, "Tanggal");
            $sheet->mergeCells("C" . $i . ":C" . ($i + 2))->setCellValue("C" . $i, "Pasien Awal");
            $sheet->mergeCells("D" . $i . ":F" . $i)->setCellValue("D" . $i, "Pasien Masuk Ruangan");
            $sheet->mergeCells("D" . ($i + 1) . ":D" . ($i + 2))->setCellValue("D" . ($i + 1), "Pasien Masuk Ruangan");
            $sheet->mergeCells("E" . ($i + 1) . ":E" . ($i + 2))->setCellValue("E" . ($i + 1), "Pasien Pindahan");
            $sheet->mergeCells("F" . ($i + 1) . ":F" . ($i + 2))->setCellValue("F" . ($i + 1), "Jumlah Pasien Masuk");
            $sheet->mergeCells("G" . $i . ":P" . $i)->setCellValue("G" . $i, "Pasien Keluar Ruangan");
            $sheet->mergeCells("G" . ($i + 1) . ":G" . ($i + 2))->setCellValue("G" . ($i + 1), "Dipindahkan");
            $sheet->mergeCells("H" . ($i + 1) . ":M" . ($i + 1))->setCellValue("H" . ($i + 1), "Pasien Keluar");
            $sheet->setCellValue("H" . ($i + 2), "Hidup");
            $sheet->setCellValue("I" . ($i + 2), "Dirujuk");
            $sheet->setCellValue("J" . ($i + 2), "Pindah ke RS Lain");
            $sheet->setCellValue("K" . ($i + 2), "Pulang Paksa");
            $sheet->setCellValue("L" . ($i + 2), "Kabur");
            $sheet->setCellValue("M" . ($i + 2), "Jumlah Pasien Keluar Hidup");
            $sheet->mergeCells("N" . ($i + 1) . ":O" . ($i + 1))->setCellValue("N" . ($i + 1), "Pasien Mati");
            $sheet->setCellValue("N" . ($i + 2), "< 48 Jam");
            $sheet->setCellValue("O" . ($i + 2), "> 48 Jam");
            $sheet->mergeCells("P" . ($i + 1) . ":P" . ($i + 2))->setCellValue("P" . ($i + 1), "Jumlah Pasien Keluar");
            $sheet->mergeCells("Q" . $i . ":Q" . ($i + 2))->setCellValue("Q" . $i, "Jumlah Pasien Masih Dirawat");
            $sheet->mergeCells("R" . $i . ":R" . ($i + 2))->setCellValue("R" . $i, "Lama Dirawat");
            $sheet->mergeCells("S" . $i . ":S" . ($i + 2))->setCellValue("S" . $i, "Pasien Masuk - Keluar Hari Sama");
            $sheet->mergeCells("T" . $i . ":T" . ($i + 2))->setCellValue("T" . $i, "Jumlah HP");
            $sheet->getStyle("A" . $i . ":T" . ($i + 2))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $sheet->getStyle("A" . $i . ":T" . ($i + 2))->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $sheet->getStyle("A" . $i . ":T" . ($i + 2))->getFont()->setBold(true);
            $sheet->getStyle("A" . $i . ":T" . ($i + 2))->getFill()->applyFromArray(array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'startcolor' => array(
                    'rgb' => 'E0E0E0'
                )
            ));
            $i = $i + 2;

            if (count($data_sensus) > 0) {
                foreach ($data_sensus as $ds) {
                    $i = $i + 1;
                    $sheet->setCellValue("A" . $i, $ds['nomor']);
                    $sheet->setCellValue("B" . $i, $ds['tanggal']);
                    $sheet->setCellValue("C" . $i, $ds['pasien_awal']);
                    $sheet->setCellValue("D" . $i, $ds['pasien_masuk_ruangan']);
                    $sheet->setCellValue("E" . $i, $ds['pasien_pindahan']);
                    $sheet->setCellValue("F" . $i, $ds['jumlah_pasien_masuk']);
                    $sheet->setCellValue("G" . $i, $ds['dipindahkan']);
                    $sheet->setCellValue("H" . $i, $ds['hidup']);
                    $sheet->setCellValue("I" . $i, $ds['dirujuk']);
                    $sheet->setCellValue("J" . $i, $ds['pindah_ke_rs_lain']);
                    $sheet->setCellValue("K" . $i, $ds['pulang_paksa']);
                    $sheet->setCellValue("L" . $i, $ds['kabur']);
                    $sheet->setCellValue("M" . $i, $ds['jumlah_pasien_keluar_hidup']);
                    $sheet->setCellValue("N" . $i, $ds['mati_k48']);
                    $sheet->setCellValue("O" . $i, $ds['mati_l48']);
                    $sheet->setCellValue("P" . $i, $ds['jumlah_pasien_keluar']);
                    $sheet->setCellValue("Q" . $i, $ds['jumlah_pasien_dirawat']);
                    $sheet->setCellValue("R" . $i, $ds['lama_dirawat']);
                    $sheet->setCellValue("S" . $i, $ds['pasien_masuk_keluar_hr_sama']);
                    $sheet->setCellValue("T" . $i, $ds['jumlah_hp']);
                }
            }
            $thin = array();
            $thin['borders'] = array();
            $thin['borders']['allborders'] = array();
            $thin['borders']['allborders']['style'] = PHPExcel_Style_Border::BORDER_THIN;
            $sheet->getStyle("A" . $border_start . ":T" . $i)->applyFromArray($thin);

            $sheet->getColumnDimension('A')->setWidth(10);
            $sheet->getColumnDimension('B')->setWidth(15);
            $sheet->getColumnDimension('C')->setWidth(17);
            $sheet->getColumnDimension('D')->setWidth(22);
            $sheet->getColumnDimension('E')->setWidth(22);
            $sheet->getColumnDimension('F')->setWidth(22);
            $sheet->getColumnDimension('G')->setWidth(22);
            $sheet->getColumnDimension('H')->setWidth(22);
            $sheet->getColumnDimension('I')->setWidth(22);
            $sheet->getColumnDimension('J')->setWidth(22);
            $sheet->getColumnDimension('K')->setWidth(22);
            $sheet->getColumnDimension('L')->setWidth(22);
            $sheet->getColumnDimension('M')->setWidth(32);
            $sheet->getColumnDimension('N')->setWidth(22);
            $sheet->getColumnDimension('O')->setWidth(22);
            $sheet->getColumnDimension('P')->setWidth(22);
            $sheet->getColumnDimension('Q')->setWidth(32);
            $sheet->getColumnDimension('R')->setWidth(22);
            $sheet->getColumnDimension('S')->setWidth(32);
            $sheet->getColumnDimension('T')->setWidth(22);

            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename="Rekap. Sensus Harian Rawat Inap - ' . $ruangan_label . ' - ' . $bulan_label . ' - ' . $tahun . '.xls"');
            header('Cache-Control: max-age=0');
            $writer = PHPExcel_IOFactory::createWriter($file, 'Excel5');
            $writer->save('php://output');
        }
        return;
    }

    $form = new Form("", "", "Rekap. Sensus Harian Rawat Inap");
    $bulan_option = new OptionBuilder();
    if (date("m") == 1)
        $bulan_option->add("Januari", 1, "1");
    else
        $bulan_option->add("Januari", 1);
    if (date("m") == 2)
        $bulan_option->add("Februari", 2, "1");
    else
        $bulan_option->add("Februari", 2);
    if (date("m") == 3)
        $bulan_option->add("Maret", 3, "1");
    else
        $bulan_option->add("Maret", 3);
    if (date("m") == 4)
        $bulan_option->add("April", 4, "1");
    else
        $bulan_option->add("April", 4);
    if (date("m") == 5)
        $bulan_option->add("Mei", 5, "1");
    else
        $bulan_option->add("Mei", 5);
    if (date("m") == 6)
        $bulan_option->add("Juni", 6, "1");
    else
        $bulan_option->add("Juni", 6);
    if (date("m") == 7)
        $bulan_option->add("Juli", 7, "1");
    else
        $bulan_option->add("Juli", 7);
    if (date("m") == 8)
        $bulan_option->add("Agustus", 8, "1");
    else
        $bulan_option->add("Agustus", 8);
    if (date("m") == 9)
        $bulan_option->add("September", 9, "1");
    else
        $bulan_option->add("September", 9);
    if (date("m") == 10)
        $bulan_option->add("Oktober", 10, "1");
    else
        $bulan_option->add("Oktober", 10);
    if (date("m") == 11)
        $bulan_option->add("Nopember", 11, "1");
    else
        $bulan_option->add("Nopember", 11);
    if (date("m") == 12)
        $bulan_option->add("Desember", 12, "1");
    else
        $bulan_option->add("Desember", 12);
    $bulan_select = new Select("lbsri_bulan", "lbsri_bulan", $bulan_option->getContent());
    $form->addElement("Bulan", $bulan_select);
    $tahun_option = new OptionBuilder();
    for ($i = date('Y') - 5; $i <= date('Y'); $i++) {
        if ($i != date('Y'))
            $tahun_option->add($i, $i);
        else
            $tahun_option->add($i, $i, "1");
    }
    $tahun_select = new Select("lbsri_tahun", "lbsri_tahun", $tahun_option->getContent());
    $form->addElement("Tahun", $tahun_select);
    $urjip = new ServiceConsumer($db, "get_urjip", array());
    $urjip->setMode(ServiceConsumer::$MULTIPLE_MODE);
    $urjip->execute();
    $content = $urjip->getContent();
    $ruangan = array();
    foreach ($content as $autonomous => $ruang) {
        foreach ($ruang as $nama_ruang => $jip) {
            if ($jip[$nama_ruang] == "URI") {
                $option = array();
                $option['value'] = $nama_ruang;
                if ($jip['name'] != null)
                    $option['name'] = $jip['name'];
                else
                    $option['name'] = ArrayAdapter::format("unslug", $nama_ruang);
                $ruangan[] = $option;
            }
        }
    }
    $ruangan_select = new Select("lbsri_ruangan", "lbsri_ruangan", $ruangan);
    $form->addElement("Ruangan", $ruangan_select);
    $show_button = new Button("", "", "Tampilkan");
    $show_button->setClass("btn-primary");
    $show_button->setIcon("icon-white icon-repeat");
    $show_button->setIsButton(Button::$ICONIC);
    $show_button->setAction("lbsri.view()");
    $download_button = new Button("", "", "Download");
    $download_button->setClass("btn-inverse");
    $download_button->setIcon("fa fa-download");
    $download_button->setIsButton(Button::$ICONIC);
    $download_button->setAction("lbsri.export_xls()");
    $btn_group = new ButtonGroup("noprint");
    $btn_group->addButton($show_button);
    $btn_group->addButton($download_button);
    $form->addElement("", $btn_group);

    $table = new Table(
        array("No.", "Tanggal", "Pasien Awal", 
              "Pasien Masuk Ruangan", "Pasien Pindahan", "Jumlah Pasien Masuk", 
              "Pasien Dipindahkan",
              "Pasien Hidup", "Pasien Dirujuk", "Pasien Pindah ke RS Lain", "Pasien Pulang Paksa", "Pasien Kabur", "Jumlah Pasien Keluar Hidup",
              "Pasien Mati K48", "Pasien Mati L48", "Jumlah Pasien Keluar",
              "Pasien Masih Dirawat",
              "Lama Dirawat", "Pasien Masuk - Keluar di Hari Sama", "Jumlah HP"),
        "",
        null,
        true
    );
    $table->setName("lbsri");
    $table->setAction(false);
    $table->setFooterVisible(false);
    $table->setHeaderVisible(false);
    $table->addHeader("after", "
        <tr class='inverse'>
            <th rowspan='3' style='vertical-align: middle !important;'>
                <center>No.</center>
            </th>
            <th rowspan='3' style='vertical-align: middle !important;'>
                <center>Tanggal</center>
            </th>
            <th rowspan='3' style='vertical-align: middle !important;'>
                <center>Pasien Awal</center>
            </th>
            <th colspan='3' style='vertical-align: middle !important;'>
                <center>Pasien Masuk Ruangan</center>
            </th>
            <th colspan='10' style='vertical-align: middle !important;'>
                <center>Pasien Keluar Ruangan</center>
            </th>
            <th rowspan='3' style='vertical-align: middle !important;'>
                <center>Jumlah Pasien Masih Dirawat</center>
            </th>
            <th rowspan='3' style='vertical-align: middle !important;'>
                <center>Lama Dirawat</center>
            </th>
            <th rowspan='3' style='vertical-align: middle !important;'>
                <center>Pasien Masuk - Keluar Hari Sama</center>
            </th>
            <th rowspan='3' style='vertical-align: middle !important;'>
                <center>Jumlah HP</center>
            </th>
        </tr>
        <tr class='inverse'>
            <th rowspan='2' style='vertical-align: middle !important;'>
                <center>Pasien Masuk Ruangan</center>
            </th>
            <th rowspan='2' style='vertical-align: middle !important;'>
                <center>Pasien Pindahan</center>
            </th>
            <th rowspan='2' style='vertical-align: middle !important;'>
                <center>Jumlah Pasien Masuk</center>
            </th>
            <th rowspan='2' style='vertical-align: middle !important;'>
                <center>Dipindahkan</center>
            </th>
            <th colspan='6' style='vertical-align: middle !important;'>
                <center>Pasien Keluar</center>
            </th>
            <th colspan='2' style='vertical-align: middle !important;'>
                <center>Pasien Mati</center>
            </th>
            <th rowspan='2' style='vertical-align: middle !important;'>
                <center>Jumlah Pasien Keluar</center>
            </th>
        </tr>
        <tr class='inverse'>
            <th colspan='1' style='vertical-align: middle !important;'>
                <center>Hidup</center>
            </th>
            <th colspan='1' style='vertical-align: middle !important;'>
                <center>Dirujuk</center>
            </th>
            <th colspan='1' style='vertical-align: middle !important;'>
                <center>Pindah ke RS Lain</center>
            </th>
            <th colspan='1' style='vertical-align: middle !important;'>
                <center>Pulang Paksa</center>
            </th>
            <th colspan='1' style='vertical-align: middle !important;'>
                <center>Kabur</center>
            </th>
            <th colspan='1' style='vertical-align: middle !important;'>
                <center>Jumlah Pasien Keluar Hidup</center>
            </th>
            <th colspan='1' style='vertical-align: middle !important;'>
                <center>< 48 Jam</center>
            </th>
            <th colspan='1' style='vertical-align: middle !important;'>
                <center>> 48 Jam</center>
            </th>
        </tr>
    ");

    echo $form->getHtml();
    echo $table->getHtml();
	echo addJS("framework/smis/js/table_action.js");
?>
<script type="text/javascript">
	var lbsri;
    $(document).ready(function() {
        lbsri = new TableAction(
            "lbsri",
            "medical_record",
            "laporan_bulanan_sensus_rawat_inap",
            new Array()
        );
        lbsri.addRegulerData = function(data) {
            data['bulan'] = $("#lbsri_bulan").val();
            data['bulan_label'] = $("#lbsri_bulan option:selected").text();
            data['tahun'] = $("#lbsri_tahun").val();
            data['ruangan'] = $("#lbsri_ruangan").val();
            data['ruangan_label'] = $("#lbsri_ruangan option:selected").text();
            return data;
        };
        lbsri.view = function() {
            showLoading();
            var self = this;
            var data = this.getRegulerData();
            data['command'] = "list";
            $.post(
                "",
                data,
                function(response) {
                    var json = JSON.parse(response);
                    if (json == null) {
                        dismissLoading();
                        return;
                    }
                    $("#lbsri_list").html(json.html);
                    dismissLoading();
                }
            );
        };
        lbsri.export_xls = function() {
            var data = this.getRegulerData();
            data['command'] = "export_xls";
            var jumlah_data = $("#lbsri_list tr").length;
            var data_sensus = {};
            for (var i = 0; i < jumlah_data; i++) {
                var nomor                       = $("#lbsri_list tr:eq(" + i + ") td:eq(0)").text();
                var tanggal                     = $("#lbsri_list tr:eq(" + i + ") td:eq(1)").text();
                var pasien_awal                 = $("#lbsri_list tr:eq(" + i + ") td:eq(2)").text();
                var pasien_masuk_ruangan        = $("#lbsri_list tr:eq(" + i + ") td:eq(3)").text();
                var pasien_pindahan             = $("#lbsri_list tr:eq(" + i + ") td:eq(4)").text();
                var jumlah_pasien_masuk         = $("#lbsri_list tr:eq(" + i + ") td:eq(5)").text();
                var dipindahkan                 = $("#lbsri_list tr:eq(" + i + ") td:eq(6)").text();
                var hidup                       = $("#lbsri_list tr:eq(" + i + ") td:eq(7)").text();
                var dirujuk                     = $("#lbsri_list tr:eq(" + i + ") td:eq(8)").text();
                var pindah_ke_rs_lain           = $("#lbsri_list tr:eq(" + i + ") td:eq(9)").text();
                var pulang_paksa                = $("#lbsri_list tr:eq(" + i + ") td:eq(10)").text();
                var kabur                       = $("#lbsri_list tr:eq(" + i + ") td:eq(11)").text();
                var jumlah_pasien_keluar_hidup  = $("#lbsri_list tr:eq(" + i + ") td:eq(12)").text();
                var mati_k48                    = $("#lbsri_list tr:eq(" + i + ") td:eq(13)").text();
                var mati_l48                    = $("#lbsri_list tr:eq(" + i + ") td:eq(14)").text();
                var jumlah_pasien_keluar        = $("#lbsri_list tr:eq(" + i + ") td:eq(15)").text();
                var jumlah_pasien_dirawat       = $("#lbsri_list tr:eq(" + i + ") td:eq(16)").text();
                var lama_dirawat                = $("#lbsri_list tr:eq(" + i + ") td:eq(17)").text();
                var pasien_masuk_keluar_hr_sama = $("#lbsri_list tr:eq(" + i + ") td:eq(18)").text();
                var jumlah_hp                   = $("#lbsri_list tr:eq(" + i + ") td:eq(19)").text();
                var arr = {};
                arr['nomor'] = nomor;
                arr['tanggal'] = tanggal;                     
                arr['pasien_awal'] = pasien_awal;
                arr['pasien_masuk_ruangan'] = pasien_masuk_ruangan;  
                arr['pasien_pindahan'] = pasien_pindahan;
                arr['jumlah_pasien_masuk'] = jumlah_pasien_masuk;
                arr['dipindahkan'] = dipindahkan;
                arr['hidup'] = hidup;
                arr['dirujuk'] = dirujuk;
                arr['pindah_ke_rs_lain'] = pindah_ke_rs_lain;
                arr['pulang_paksa'] = pulang_paksa;
                arr['kabur'] = kabur;
                arr['jumlah_pasien_keluar_hidup'] = jumlah_pasien_keluar_hidup;
                arr['mati_k48'] = mati_k48;
                arr['mati_l48'] = mati_l48;
                arr['jumlah_pasien_keluar'] = jumlah_pasien_keluar;
                arr['jumlah_pasien_dirawat'] = jumlah_pasien_dirawat;
                arr['lama_dirawat'] = lama_dirawat;
                arr['pasien_masuk_keluar_hr_sama'] = pasien_masuk_keluar_hr_sama;
                arr['jumlah_hp'] = jumlah_hp;
                data_sensus[i] = arr;
            }
            data['data_sensus'] = JSON.stringify(data_sensus);
            postForm(data);
        };
        $("#lbsri_list").html("<tr><td colspan='20'><center>Data Belum Diproses</center></td></tr>");
    });
</script>