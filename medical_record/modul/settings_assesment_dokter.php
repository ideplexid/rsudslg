<?php
	require_once 'smis-framework/smis/api/SettingsBuilder.php';
	global $db;	
	global $user;
    $username = $user->getUsername();
    
    $smis = new SettingsBuilder ( $db, "settings_assesment_dokter", "medical_record", "settings_assesment_dokter","Settings Assesment Dokter" );
	$smis->setShowDescription ( true );
	$smis->setTabulatorMode ( Tabulator::$POTRAIT );
	
	$smis->addTabs ( "dokter", "Dokter"," fa fa-user-md" );
    if($smis->isGroup("dokter")){
        $smis->addItem ( "dokter", new SettingsItem ( $db, "mr-id-dokter-".$username, "ID Dokter", "", "text", "ID Dokter di HRD" ) );
        $smis->addItem ( "dokter", new SettingsItem ( $db, "mr-nama-dokter-".$username, "Nama Dokter", "", "text", "Nama Dokter di HRD" ) );
        $smis->addItem ( "dokter", new SettingsItem ( $db, "mr-ttd-".$username, "Tanda Tangan", "", "draw-file-", "Tanda Tangan Dokter" ) );
    }
    
    $smis->addTabs ( "assesment_rajal", "Assessment Rawat Jalan"," fa fa-wheelchair" );
    if($smis->isGroup("assesment_rajal")){
        require_once "medical_record/function/get_detail_assessment_rajal.php";
        $list = get_detail_assessment_rajal();
        $opsi = new OptionBuilder();
        $opsi ->add("Edit","edit","1");
        $opsi ->add("View","view","0");
        $opsi ->add("Hide","hide","0");
        foreach($list as $name=>$value){
            $smis->addItem ( "assesment_rajal", new SettingsItem ( $db, "mr-asm-rajal-".$name."-".$username, "Assesment Rawat Jalan ".$value, $opsi->getContent(), "select", "Enabled Assesment Dokter Rawat Jalan ".$value ) );
        }
    }
    $smis->setPartialLoad(true);
	$smis->init ();
?>