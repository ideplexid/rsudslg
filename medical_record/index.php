<?php
	global $db;
	global $user;
	require_once 'smis-libs-class/Policy.php';
	require_once 'smis-libs-inventory/policy.php';
	$inventory_policy = new InventoryPolicy("medical_record", $user,"modul/");
	
	$policy = new Policy("medical_record", $user);
	$policy ->setDefaultPolicy(Policy::$DEFAULT_POLICY_RESTRICT)
            ->addPolicy("data_induk", "data_induk", Policy::$DEFAULT_COOKIE_CHANGE,"modul/data_induk")
            ->addPolicy("jenis_rs", "data_induk", Policy::$DEFAULT_COOKIE_KEEP, "resource/php/data_induk/jenis_rs")
            ->addPolicy("kepemilikan_rs", "data_induk", Policy::$DEFAULT_COOKIE_KEEP, "resource/php/data_induk/kepemilikan_rs")
            ->addPolicy("penyelenggara_rs", "data_induk", Policy::$DEFAULT_COOKIE_KEEP, "resource/php/data_induk/penyelenggara_rs")
            ->addPolicy("icdtindakan", "data_induk", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/data_induk/icdtindakan")
            ->addPolicy("icd", "data_induk", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/data_induk/icd")
            ->addPolicy("icd_rehab_medis", "data_induk", Policy::$DEFAULT_COOKIE_KEEP, "resource/php/data_induk/icd_rehab_medis")
            ->addPolicy("pasien", "data_induk", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/data_induk/pasien")
            ->addPolicy("label", "data_induk", Policy::$DEFAULT_COOKIE_KEEP,"snippet/label")
            ->addPolicy("gambar", "data_induk", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/data_induk/gambar")
            ->addPolicy("management_bed", "data_induk", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/data_induk/management_bed")
            ->addPolicy("jenis_kegiatan_rl52", "data_induk", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/data_induk/jenis_kegiatan_rl52");
	
	$policy ->addPolicy("input_data", "input_data", Policy::$DEFAULT_COOKIE_CHANGE,"modul/input_data")
            ->addPolicy("input_data_dasar_rs", "input_data", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/input_data/input_data_dasar_rs")
            ->addPolicy("data_kunjungan", "input_data", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/input_data/data_kunjungan")
            ->addPolicy("create_excel_data_kunjungan", "input_data", Policy::$DEFAULT_COOKIE_KEEP,"snippet/create_excel_data_kunjungan")
            ->addPolicy("input_operasi", "input_data", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/input_data/input_operasi")
            ->addPolicy("tindakan", "input_data", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/input_data/tindakan")
            ->addPolicy("input_igd", "input_data", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/input_data/input_igd")
            ->addPolicy("diagnosa", "input_data", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/input_data/diagnosa")
            ->addPolicy("input_infeksi", "input_data", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/input_data/input_infeksi")
            ->addPolicy("input_jenis_perawatan", "input_data", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/input_data/input_jenis_perawatan")
            ->addPolicy("input_kamar_tidur", "input_data", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/input_data/input_kamar_tidur");
    
      $policy ->addPolicy("laporan_asesmen", "laporan_asesmen", Policy::$DEFAULT_COOKIE_CHANGE,"modul/laporan_asesmen")
            ->addPolicy("asesmen_anak", "laporan_asesmen", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/laporan_asesmen/asesmen_anak")
            ->addPolicy("asesmen_perawat", "laporan_asesmen", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/laporan_asesmen/asesmen_perawat")
            ->addPolicy("asesmen_jantung", "laporan_asesmen", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/laporan_asesmen/asesmen_jantung")
            ->addPolicy("asesmen_psikologi", "laporan_asesmen", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/laporan_asesmen/asesmen_psikologi")
            ->addPolicy("asesmen_psikiatri", "laporan_asesmen", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/laporan_asesmen/asesmen_psikiatri")
            ->addPolicy("asesmen_syaraf", "laporan_asesmen", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/laporan_asesmen/asesmen_syaraf");
	
	$policy ->addPolicy("indikator_klinis", "indikator_klinis", Policy::$DEFAULT_COOKIE_CHANGE,"modul/indikator_klinis")
            ->addPolicy("sentinel", "indikator_klinis", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/indikator_klinis/sentinel")
            ->addPolicy("nyaris", "indikator_klinis", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/indikator_klinis/nyaris")
            ->addPolicy("cedera", "indikator_klinis", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/indikator_klinis/cedera")
            ->addPolicy("decubitus", "indikator_klinis", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/indikator_klinis/decubitus")
            ->addPolicy("infus", "indikator_klinis", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/indikator_klinis/infus")
            ->addPolicy("transfusi", "indikator_klinis", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/indikator_klinis/transfusi")
            ->addPolicy("lap_transfusi", "indikator_klinis", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/indikator_klinis/lap_transfusi");
	
	$policy ->addPolicy("operasi", "indikator_klinis", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/indikator_klinis/operasi")
            ->addPolicy("isk", "indikator_klinis", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/indikator_klinis/isk")
            ->addPolicy("bayi", "indikator_klinis", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/indikator_klinis/bayi")
            ->addPolicy("elektif", "indikator_klinis", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/indikator_klinis/elektif");

	$policy ->addPolicy("laporan_rl", "laporan_rl", Policy::$DEFAULT_COOKIE_CHANGE,"modul/laporan_rl")
            ->addPolicy("rl11", "laporan_rl", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/laporan_rl/rl11")
            ->addPolicy("create_excel_rl11", "laporan_rl", Policy::$DEFAULT_COOKIE_KEEP,"snippet/create_excel_rl11")
            ->addPolicy("rl12", "laporan_rl", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/laporan_rl/rl12")
            ->addPolicy("rl2", "laporan_rl", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/laporan_rl/rl2")
            ->addPolicy("create_excel_rl2", "laporan_rl", Policy::$DEFAULT_COOKIE_KEEP,"snippet/create_excel_rl2")
            ->addPolicy("rl13", "laporan_rl", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/laporan_rl/rl13")
            ->addPolicy("rl31", "laporan_rl", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/laporan_rl/rl31")
            ->addPolicy("rl32", "laporan_rl", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/laporan_rl/rl32")
            ->addPolicy("rl33", "laporan_rl", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/laporan_rl/rl33")
            ->addPolicy("rl31", "laporan_rl", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/laporan_rl/rl31")
            ->addPolicy("rl34", "laporan_rl", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/laporan_rl/rl34")
            ->addPolicy("rl35", "laporan_rl", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/laporan_rl/rl35")
            ->addPolicy("rl36", "laporan_rl", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/laporan_rl/rl36")
            ->addPolicy("rl37", "laporan_rl", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/laporan_rl/rl37")
            ->addPolicy("rl38", "laporan_rl", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/laporan_rl/rl38")
            ->addPolicy("rl39", "laporan_rl", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/laporan_rl/rl39")
            ->addPolicy("rl310", "laporan_rl", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/laporan_rl/rl310")
            ->addPolicy("rl311", "laporan_rl", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/laporan_rl/rl311")
            ->addPolicy("rl312", "laporan_rl", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/laporan_rl/rl312")
            ->addPolicy("rl313", "laporan_rl", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/laporan_rl/rl313")
            ->addPolicy("rl314", "laporan_rl", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/laporan_rl/rl314")
            ->addPolicy("rl315", "laporan_rl", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/laporan_rl/rl315")
            ->addPolicy("rl4a_1", "laporan_rl", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/laporan_rl/rl4a_1")
            ->addPolicy("rl4a_2", "laporan_rl", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/laporan_rl/rl4a_2")
            ->addPolicy("rl4b_1", "laporan_rl", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/laporan_rl/rl4b_1")
            ->addPolicy("rl4b_2", "laporan_rl", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/laporan_rl/rl4b_2")
            ->addPolicy("rl51", "laporan_rl", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/laporan_rl/rl51")
            ->addPolicy("create_excel_rl51", "laporan_rl", Policy::$DEFAULT_COOKIE_KEEP,"snippet/create_excel_rl51")
            ->addPolicy("rl52", "laporan_rl", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/laporan_rl/rl52")
            ->addPolicy("create_excel_rl52", "laporan_rl", Policy::$DEFAULT_COOKIE_KEEP,"snippet/create_excel_rl52")
            ->addPolicy("rl521", "laporan_rl", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/laporan_rl/rl521")
            ->addPolicy("create_excel_rl521", "laporan_rl", Policy::$DEFAULT_COOKIE_KEEP,"snippet/create_excel_rl521")
            ->addPolicy("rl52b", "laporan_rl", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/laporan_rl/rl52b")
            ->addPolicy("rl53", "laporan_rl", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/laporan_rl/rl53")
            ->addPolicy("create_excel_rl53", "laporan_rl", Policy::$DEFAULT_COOKIE_KEEP,"snippet/create_excel_rl53")
            ->addPolicy("rl54", "laporan_rl", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/laporan_rl/rl54")
            ->addPolicy("rl53_lama", "laporan_rl", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/laporan_rl/rl53_lama")
            ->addPolicy("rl54_lama", "laporan_rl", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/laporan_rl/rl54_lama")
            ->addPolicy("rl5_4", "laporan_rl", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/laporan_rl/rl5_4")
            ->addPolicy("create_excel_rl5_4", "laporan_rl", Policy::$DEFAULT_COOKIE_KEEP,"snippet/create_excel_rl5_4");
	
	$policy ->addPolicy("lap_index", "lap_index", Policy::$DEFAULT_COOKIE_CHANGE,"modul/lap_index")
            ->addPolicy("lap_index_dokter", "lap_index", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/lap_index/lap_index_dokter")
            ->addPolicy("lap_index_penyakit", "lap_index", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/lap_index/lap_index_penyakit")
            ->addPolicy("lap_index_operasi", "lap_index", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/lap_index/lap_index_operasi")
            ->addPolicy("lap_index_pasien", "lap_index", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/lap_index/lap_index_pasien")
            ->addPolicy("lap_index_kematian", "lap_index", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/lap_index/lap_index_kematian")
            ->addPolicy("lap_index_ranap", "lap_index", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/lap_index/lap_index_ranap")
            ->addPolicy("lap_index_rajal", "lap_index", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/lap_index/lap_index_rajal");
	
	$policy ->addPolicy("lap_kb", "lap_kb", Policy::$DEFAULT_COOKIE_CHANGE,"modul/lap_kb")
            ->addPolicy("lap_kb_baru", "lap_kb", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/lap_kb/lap_kb_baru")
            ->addPolicy("lap_kb_kasus", "lap_kb", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/lap_kb/lap_kb_kasus")
            ->addPolicy("lap_kb_ulang", "lap_kb", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/lap_kb/lap_kb_ulang")
            ->addPolicy("lap_kb_pasca", "lap_kb", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/lap_kb/lap_kb_pasca")
            ->addPolicy("lap_persalinan", "lap_kb", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/lap_kb/lap_persalinan")
            ->addPolicy("lap_imunisasi", "lap_kb", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/lap_kb/lap_imunisasi");
    
	$policy ->addPolicy("lap_igd", "lap_igd", Policy::$DEFAULT_COOKIE_CHANGE,"modul/lap_igd")
            ->addPolicy("lap_igd_triage", "lap_igd", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/lap_igd/lap_igd_triage")
            ->addPolicy("lap_igd_data", "lap_igd", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/lap_igd/lap_igd_data")
            ->addPolicy("lap_igd_response", "lap_igd", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/lap_igd/lap_igd_response")
            ->addPolicy("lap_igd_kunjungan", "lap_igd", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/lap_igd/lap_igd_kunjungan")
            ->addPolicy("lap_igd_keterangan", "lap_igd", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/lap_igd/lap_igd_keterangan")
            ->addPolicy("lap_igd_koreksi", "lap_igd", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/lap_igd/lap_igd_koreksi");
	
	$policy ->addPolicy("lap_operasi", "lap_operasi", Policy::$DEFAULT_COOKIE_CHANGE,"modul/lap_operasi")
            ->addPolicy("lap_operasi_kamar", "lap_operasi", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/lap_operasi/lap_operasi_kamar")
            ->addPolicy("lap_operasi_anastesi", "lap_operasi", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/lap_operasi/lap_operasi_anastesi")
            ->addPolicy("lap_operasi_bedah", "lap_operasi", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/lap_operasi/lap_operasi_bedah");
	
	$policy ->addPolicy("kuisioner", "kuisioner", Policy::$DEFAULT_COOKIE_CHANGE,"modul/kuisioner")
            ->addPolicy("kuisioner_ri", "kuisioner", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/kuisioner/kuisioner_ri")
            ->addPolicy("kuisioner_rj", "kuisioner", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/kuisioner/kuisioner_rj")
            ->addPolicy("analisa_kuisioner_rj", "kuisioner", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/kuisioner/analisa_kuisioner_rj")
            ->addPolicy("analisa_kuisioner_ri", "kuisioner", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/kuisioner/analisa_kuisioner_ri");
	
	$policy ->addPolicy("laporan_pasien", "laporan_pasien", Policy::$DEFAULT_COOKIE_CHANGE,"modul/laporan_pasien")
            ->addPolicy("pasien_aktif", "laporan_pasien", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/laporan_pasien/pasien_aktif")
            ->addPolicy("lap_ri", "laporan_pasien", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/laporan_pasien/lap_ri")
            ->addPolicy("lap_rj", "laporan_pasien", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/laporan_pasien/lap_rj")
            ->addPolicy("lap_pulang", "laporan_pasien", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/laporan_pasien/lap_pulang")
            ->addPolicy("lap_diagnosa", "laporan_pasien", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/laporan_pasien/lap_diagnosa")
            ->addPolicy("lap_diagnosa_per_bulan", "laporan_pasien", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/laporan_pasien/lap_diagnosa_per_bulan")
            ->addPolicy("lap_diagnosa_detail_pertanggal", "laporan_pasien", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/laporan_pasien/lap_diagnosa_detail_pertanggal")
            ->addPolicy("lap_kun_pertanggal", "laporan_pasien", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/laporan_pasien/lap_kun_pertanggal")
            ->addPolicy("lap_rawat_inap", "laporan_pasien", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/laporan_pasien/lap_rawat_inap")
            ->addPolicy("lap_rawat_jalan", "laporan_pasien", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/laporan_pasien/lap_rawat_jalan")
            ->addPolicy("lap_resume_pasien_pulang_ri", "laporan_pasien", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/laporan_pasien/lap_resume_pasien_pulang_ri")
            ->addPolicy("lap_resume_ruangan", "laporan_pasien", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/laporan_pasien/lap_resume_ruangan")
            ->addPolicy("lap_lama_rawat", "laporan_pasien", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/laporan_pasien/lap_lama_rawat")
            ->addPolicy("lap_plebitis", "laporan_pasien", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/laporan_pasien/lap_plebitis")
            ->addPolicy("lap_cauti", "laporan_pasien", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/laporan_pasien/lap_cauti")
            ->addPolicy("lap_kasus", "laporan_pasien", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/laporan_pasien/lap_kasus")
            ->addPolicy("lap_kegiatan_gigi_mulut", "laporan_pasien", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/laporan_pasien/lap_kegiatan_gigi_mulut")
            ->addPolicy("lap_prolanis", "laporan_pasien", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/laporan_pasien/lap_prolanis")
            ->addPolicy("lap_kunjungan_rj", "laporan_pasien", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/laporan_pasien/lap_kunjungan_rj")
            ->addPolicy("lap_kunjungan_rawat", "laporan_pasien", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/laporan_pasien/lap_kunjungan_rawat")
            ->addPolicy("lap_kunjungan_rawat_jalan", "laporan_pasien", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/laporan_pasien/lap_kunjungan_rawat_jalan")
            ->addPolicy("lap_kunj_rawat_jalan_lk_pr", "laporan_pasien", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/laporan_pasien/lap_kunj_rawat_jalan_lk_pr")
            ->addPolicy("lap_persen_diagnosa", "laporan_pasien", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/laporan_pasien/lap_persen_diagnosa");
           
      $policy ->addPolicy("laporan_sensus_rawat_inap", "laporan_sensus_rawat_inap", Policy::$DEFAULT_COOKIE_CHANGE, "modul/laporan_sensus_rawat_inap");
      $policy ->addPolicy("laporan_bulanan_sensus_rawat_inap", "laporan_bulanan_sensus_rawat_inap", Policy::$DEFAULT_COOKIE_CHANGE, "modul/laporan_bulanan_sensus_rawat_inap");

      $policy ->addPolicy("laporan_rm", "laporan_rm", Policy::$DEFAULT_COOKIE_CHANGE,"modul/laporan_rm")
            ->addPolicy("laporan_index_penyakit", "laporan_rm", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/laporan_rm/laporan_index_penyakit");
            
      $policy ->addPolicy("laporan_ppi", "laporan_ppi", Policy::$DEFAULT_COOKIE_CHANGE,"modul/laporan_ppi")
            ->addPolicy("laporan_ppi_harian_per_ruang", "laporan_ppi", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/laporan_ppi/laporan_ppi_harian_per_ruang")
            ->addPolicy("laporan_ppi_total", "laporan_ppi", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/laporan_ppi/laporan_ppi_total");
    
      $policy ->addPolicy("laporan_kunjungan", "laporan_kunjungan", Policy::$DEFAULT_COOKIE_CHANGE,"modul/laporan_kunjungan")
            ->addPolicy("rekap_kunj_rwt_jalan_non_igd", "laporan_kunjungan", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/laporan_kunjungan/rekap_kunj_rwt_jalan_non_igd")
            ->addPolicy("rincian_kunj_rwt_jalan_non_igd", "laporan_kunjungan", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/laporan_kunjungan/rincian_kunj_rwt_jalan_non_igd")
            ->addPolicy("rekap_kunj_rwt_inap", "laporan_kunjungan", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/laporan_kunjungan/rekap_kunj_rwt_inap")
            ->addPolicy("rincian_kunj_rwt_inap", "laporan_kunjungan", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/laporan_kunjungan/rincian_kunj_rwt_inap")
            ->addPolicy("rekap_kunj_igd", "laporan_kunjungan", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/laporan_kunjungan/rekap_kunj_igd")
            ->addPolicy("rincian_kunj_igd", "laporan_kunjungan", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/laporan_kunjungan/rincian_kunj_igd");
            
      $policy ->addPolicy("lap_pasien_pulang", "lap_pasien_pulang", Policy::$DEFAULT_COOKIE_CHANGE,"modul/lap_pasien_pulang")
            ->addPolicy("rekap_pulang_rwt_jalan_non_igd", "lap_pasien_pulang", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/lap_pasien_pulang/rekap_pulang_rwt_jalan_non_igd")
            ->addPolicy("rincian_pulang_rwt_jalan_non_igd", "lap_pasien_pulang", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/lap_pasien_pulang/rincian_pulang_rwt_jalan_non_igd")
            ->addPolicy("rekap_pulang_rwt_inap", "lap_pasien_pulang", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/lap_pasien_pulang/rekap_pulang_rwt_inap")
            ->addPolicy("rincian_pulang_rwt_inap", "lap_pasien_pulang", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/lap_pasien_pulang/rincian_pulang_rwt_inap")
            ->addPolicy("rekap_pulang_igd", "lap_pasien_pulang", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/lap_pasien_pulang/rekap_pulang_igd")
            ->addPolicy("rincian_pulang_igd", "lap_pasien_pulang", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/lap_pasien_pulang/rincian_pulang_igd")
            ->addPolicy("lap_pasien_pulang_uncrawler", "lap_pasien_pulang", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/lap_pasien_pulang/lap_pasien_pulang_uncrawler");
            
      $policy ->addPolicy("efisiensi_mutu", "efisiensi_mutu", Policy::$DEFAULT_COOKIE_CHANGE,"modul/efisiensi_mutu")
            ->addPolicy("efisiensi_mutu_layanan_rs", "efisiensi_mutu", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/efisiensi_mutu/efisiensi_mutu_layanan_rs")
            ->addPolicy("efisiensi_mutu_layanan_ruangan", "efisiensi_mutu", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/efisiensi_mutu/efisiensi_mutu_layanan_ruangan");
            
      $policy ->addPolicy("sepuluh_besar_penyakit", "sepuluh_besar_penyakit", Policy::$DEFAULT_COOKIE_CHANGE,"modul/sepuluh_besar_penyakit")
            ->addPolicy("sepuluh_besar_penyakit_rwt_jalan_non_igd", "sepuluh_besar_penyakit", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/sepuluh_besar_penyakit/sepuluh_besar_penyakit_rwt_jalan_non_igd")
            ->addPolicy("sepuluh_besar_penyakit_rwt_inap", "sepuluh_besar_penyakit", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/sepuluh_besar_penyakit/sepuluh_besar_penyakit_rwt_inap")
            ->addPolicy("sepuluh_besar_penyakit_igd", "sepuluh_besar_penyakit", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/sepuluh_besar_penyakit/sepuluh_besar_penyakit_igd");
    
	$policy ->addPolicy("morbiditas", "laporan_pasien", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/laporan_pasien/morbiditas")
            ->addPolicy("morbiditas_grup", "laporan_pasien", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/laporan_pasien/morbiditas_grup")
            ->addPolicy("odontogram", "laporan_pasien", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/laporan_pasien/odontogram")
            ->addPolicy("resume_medis", "resume_medis", Policy::$DEFAULT_COOKIE_CHANGE,"modul/resume_medis")
            ->addPolicy("resume_diagnosa_pasien", "resume_diagnosa_pasien", Policy::$DEFAULT_COOKIE_CHANGE, "modul/resume_diagnosa_pasien")
            ->addPolicy("settings","settings", Policy::$DEFAULT_COOKIE_CHANGE,"modul/settings");
	
    $policy ->addPolicy("assesment_dokter_jalan", "assesment_dokter_jalan", Policy::$DEFAULT_COOKIE_CHANGE,"modul/assesment_dokter_jalan");
    require_once "medical_record/function/get_detail_assessment_rajal.php";
    $list = get_detail_assessment_rajal();
    foreach($list as $name=>$value){
        $file   = "assessment_".$name;
        $policy ->addPolicy($file, "assesment_dokter_jalan", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/assesment_dokter_jalan/".$file);
    }    
            
    
    $policy ->addPolicy("settings_assesment_dokter", "settings_assesment_dokter", Policy::$DEFAULT_COOKIE_CHANGE,"modul/settings_assesment_dokter");
	
	$policy ->combinePolicy($inventory_policy);
	$policy ->initialize();
?>
