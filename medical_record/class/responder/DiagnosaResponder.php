<?php

class DiagnosaResponder extends DuplicateResponder{

    public function save(){
        $result = parent::save();
        $this->updateDiagnosaBPJS();
        return $result;
    }

    private function updateDiagnosaBPJS(){
        $db = $this->getDBTable()->get_db();
        if(getSettings($db,"smis-mr-auto-diagnosa-bpjs","0")=="1"){
            $query="SELECT diagnosa FROM smis_mr_diagnosa 
                    WHERE prop!='del' 
                    AND diagnosa!='' 
                    AND noreg_pasien='".$_POST['noreg_pasien']."' 
                    ORDER BY id DESC LIMIT 0,1";
            $result=$db->get_var($query);
            require_once("smis-base/smis-include-service-consumer.php");
            $serv = new ServiceConsumer($db,"update_diagnosa_bpjs",NULL,"registration");
            $serv ->addData("noreg_pasien",$_POST['noreg_pasien'])
                  ->addData("diagnosa",$result);
            $serv ->execute();
        }
        
    }

}

?>