<?php 

class LapAsesmenPerawatResponder extends DBResponder {
    public function excel() {
        $this->dbtable->addCustomKriteria("id", " = '".$_POST['id']."'");
        $data=$this->dbtable->view("",0);
        $fix=$this->adapter->getContent($data['data']);
                
        require_once ("smis-libs-out/php-excel/PHPExcel.php");
        $file = new PHPExcel ();
        $file->getProperties ()->setCreator ( "Rumah Sakit Syamrabu" );
        $file->getProperties ()->setLastModifiedBy ( "Rumah Sakit Bagian Administrator" );
        $file->getProperties ()->setTitle ( "Laporan Asesmen Keperawatan" );
        $file->getProperties ()->setSubject ( "Laporan Asesmen Keperawatan" );
        $file->getProperties ()->setDescription ( "Laporan Asesmen Keperawatan Generated From system" );
        $file->getProperties ()->setKeywords ( "Laporan Asesmen Keperawatan" );
        $file->getProperties ()->setCategory ( "Laporan Asesmen Keperawatan" );
        $sheet = $file->getActiveSheet ();
        
        $i = 1;
        $headerImgPath = 'smis-upload/'.getSettings($db, "smis-mr-header-lap-assesmen", "");
        if(file_exists($headerImgPath)) {
            $objDrawing = new PHPExcel_Worksheet_Drawing();
            $objDrawing->setPath($headerImgPath);
            $objDrawing->setResizeProportional(false);
            $objDrawing->setWidthAndHeight(63,77);
            $objDrawing->setCoordinates("A".$i);
            $objDrawing->setWorksheet($sheet);
        }
        $sheet->setCellValue("B".$i,"NAMA (L/P) : ".$fix[0]["nama_pasien"]);
        $i++;
        $sheet->setCellValue("B".$i,"NO RM : ".$fix[0]["NRM"]);
        $i++;
        $sheet->setCellValue("B".$i,"TANGGAL LAHIR (UMUR) : ".$fix[0]["tgl_lahir_pasien"]);
        $i++;
        $sheet->setCellValue("B".$i,"ALAMAT : ".$fix[0]["alamat_pasien"]);
        $i++;
        $sheet->setCellValue("A".$i,"ASESMEN AWAL KEPERAWATAN");
        $sheet->getStyle("A".$i.":B".$i)->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
        $i += 2;
        
        $sheet->setCellValue("A".$i,"I. DATA AWAL");
        $sheet->getStyle("A".$i.":B".$i)->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
        $i++;
        $sheet->setCellValue("A".$i,"Tanggal Masuk");
        $sheet->setCellValue("B".$i, $fix[0]["tanggal_masuk"]);
        $i++;
        $sheet->setCellValue("A".$i,"Tanggal Asesmen");
        $sheet->setCellValue("B".$i, $fix[0]["tgl_asesmen"]);
        $i++;
        $sheet->setCellValue("A".$i,"Jam Asesmen");
        $sheet->setCellValue("B".$i, $fix[0]["jam_asesmen"]);
        $i++;
        $sheet->setCellValue("A".$i,"Berat Badan");
        $sheet->setCellValue("B".$i, $fix[0]["bb"]);
        $i++;
        $sheet->setCellValue("A".$i,"Tinggi Badan");
        $sheet->setCellValue("B".$i, $fix[0]["tb"]);
        $i++;
        $sheet->setCellValue("A".$i,"Rujukan");
        $sheet->setCellValue("B".$i, $fix[0]["rujukan"]);
        $i++;
        $sheet->setCellValue("A".$i,"Kedatangan");
        $sheet->setCellValue("B".$i, $fix[0]["kedatangan"]);
        $i++;
        $sheet->setCellValue("A".$i,"Tanda Vital");
        $sheet->setCellValue("B".$i, $fix[0]["tanda_vital"]);
        $i++;
        $sheet->setCellValue("A".$i,"Tekanan Darah");
        $sheet->setCellValue("B".$i, $fix[0]["tekanan_darah"]);
        $i++;
        $sheet->setCellValue("A".$i,"Frek Nafas");
        $sheet->setCellValue("B".$i, $fix[0]["frek_nafas"]);
        $i++;
        $sheet->setCellValue("A".$i,"Nadi");
        $sheet->setCellValue("B".$i, $fix[0]["nadi"]);
        $i++;
        $sheet->setCellValue("A".$i,"Suhu");
        $sheet->setCellValue("B".$i, $fix[0]["suhu"]);
        $i += 2;
        
        $sheet->setCellValue("A".$i,"II. RIWAYAT KEPERAWATAN");
        $sheet->getStyle("A".$i.":B".$i)->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
        $i++;
        $sheet->setCellValue("A".$i,"Sumber Data");
        $sheet->setCellValue("B".$i, $fix[0]["rkpr_sumber_data"]);
        $i++;
        $sheet->setCellValue("A".$i,"Keluhan Utama");
        $sheet->setCellValue("B".$i, $fix[0]["rkpr_keluhan_utama"]);
        $i++;
        $sheet->setCellValue("A".$i,"Keadaan Umum");
        $sheet->setCellValue("B".$i, $fix[0]["rkpr_keadaan_umum"]);
        $i++;
        $sheet->setCellValue("A".$i,"Gizi");
        $sheet->setCellValue("B".$i, $fix[0]["rkpr_gizi"]);
        $i++;
        $sheet->setCellValue("A".$i,"Tindakan Resusitasi");
        $sheet->setCellValue("B".$i, $fix[0]["rkpr_tindakan_resusitasi"]);
        $i++;
        $sheet->setCellValue("A".$i,"Lain-Lain");
        $sheet->setCellValue("B".$i, $fix[0]["rkpr_tindakan_resusitasi_lain"]);
        $i++;
        $sheet->setCellValue("A".$i,"Saturasi O2");
        $sheet->setCellValue("B".$i, $fix[0]["rkpr_saturasi_o2"]);
        $i++;
        $sheet->setCellValue("A".$i,"Pada");
        $sheet->setCellValue("B".$i, $fix[0]["rkpr_pada"]);
        $i += 2;
        
        $sheet->setCellValue("A".$i,"PSIKOSOSIAL");
        $sheet->getStyle("A".$i.":B".$i)->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
        $i++;
        $sheet->setCellValue("A".$i,"Status Pernikahan");
        $sheet->setCellValue("B".$i, $fix[0]["psks_status_nikah"]);
        $i++;
        $sheet->setCellValue("A".$i,"Anak");
        $sheet->setCellValue("B".$i, $fix[0]["psks_anak"]);
        $i++;
        $sheet->setCellValue("A".$i,"Jumlah Anak");
        $sheet->setCellValue("B".$i, $fix[0]["psks_jumlah_anak"]);
        $i++;
        $sheet->setCellValue("A".$i,"Pendidikan Terakhir");
        $sheet->setCellValue("B".$i, $fix[0]["psks_pend_akhir"]);
        $i++;
        $sheet->setCellValue("A".$i,"Warganegara");
        $sheet->setCellValue("B".$i, $fix[0]["psks_warganegara"]);
        $i++;
        $sheet->setCellValue("A".$i,"Pekerjaan");
        $sheet->setCellValue("B".$i, $fix[0]["psks_pekerjaan"]);
        $i++;
        $sheet->setCellValue("A".$i,"Agama");
        $sheet->setCellValue("B".$i, $fix[0]["psks_agama"]);
        $i++;
        $sheet->setCellValue("A".$i,"Tinggal Bersama");
        $sheet->setCellValue("B".$i, $fix[0]["psks_tinggal_bersama"]);
        $i++;
        $sheet->setCellValue("A".$i,"Nama");
        $sheet->setCellValue("B".$i, $fix[0]["psks_tinggal_bersama_nama"]);
        $i++;
        $sheet->setCellValue("A".$i,"No. Telepon");
        $sheet->setCellValue("B".$i, $fix[0]["psks_tinggal_bersama_telp"]);
        $i += 2;
        
        $sheet->setCellValue("A".$i,"RIWAYAT KESEHATAN");
        $sheet->getStyle("A".$i.":B".$i)->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
        $i++;
        $sheet->setCellValue("A".$i,"Riwayat Penyakit");
        $temp = 0;
        if($fix[0]["rks_riwayat_penyakit_hipertensi"] == 1)
        {
            $sheet->setCellValue("B".$i, "Hipertensi");
            $i++;
            $temp++;
        }
        if($fix[0]["rks_riwayat_penyakit_diabetes"] == 1)
        {
            $sheet->setCellValue("B".$i, "Diabetes");
            $i++;
            $temp++;
        }
        if($fix[0]["rks_riwayat_penyakit_jantung"] == 1)
        {
            $sheet->setCellValue("B".$i, "Jantung");
            $i++;
            $temp++;
        }
        if($fix[0]["rks_riwayat_penyakit_stroke"] == 1)
        {
            $sheet->setCellValue("B".$i, "Stroke");
            $i++;
            $temp++;
        }
        if($fix[0]["rks_riwayat_penyakit_dialisis"] == 1)
        {
            $sheet->setCellValue("B".$i, "Dialisis");
            $i++;
            $temp++;
        }
        if($fix[0]["rks_riwayat_penyakit_asthma"] == 1)
        {
            $sheet->setCellValue("B".$i, "Asthma");
            $i++;
            $temp++;
        }
        if($fix[0]["rks_riwayat_penyakit_kejang"] == 1)
        {
            $sheet->setCellValue("B".$i, "Kejang");
            $i++;
            $temp++;
        }
        if($fix[0]["rks_riwayat_penyakit_hati"] == 1)
        {
            $sheet->setCellValue("B".$i, "Hati");
            $i++;
            $temp++;
        }
        if($fix[0]["rks_riwayat_penyakit_kanker"] == 1)
        {
            $sheet->setCellValue("B".$i, "Kanker");
            $i++;
            $temp++;
        }
        if($fix[0]["rks_riwayat_penyakit_tbc"] == 1)
        {
            $sheet->setCellValue("B".$i, "TBC");
            $i++;
            $temp++;
        }
        if($fix[0]["rks_riwayat_penyakit_glaukoma"] == 1)
        {
            $sheet->setCellValue("B".$i, "Glaukoma");
            $i++;
            $temp++;
        }
        if($fix[0]["rks_riwayat_penyakit_std"] == 1)
        {
            $sheet->setCellValue("B".$i, "STD");
            $i++;
            $temp++;
        }
        if($fix[0]["rks_riwayat_penyakit_pendarahan"] == 1)
        {
            $sheet->setCellValue("B".$i, "Pendarahan");
            $i++;
            $temp++;
        }
        if($fix[0]["rks_riwayat_penyakit_lain"] == 1)
        {
            $sheet->setCellValue("B".$i, $fix[0]["rks_riwayat_penyakit_lain_lain"]);
            $i++;
            $temp++;
        }
        if($temp == 0)
        {
            $i++;
        }
        $sheet->setCellValue("A".$i,"Pengobatan Saat Ini");
        $sheet->setCellValue("B".$i, $fix[0]["rks_pengobatan_saat_ini"]);
        $i++;
        $sheet->setCellValue("A".$i,"Riwayat Operasi");
        $sheet->setCellValue("B".$i, $fix[0]["rks_riwayat_operasi"]);
        $i++;
        $sheet->setCellValue("A".$i,"Riwayat Transfusi");
        $sheet->setCellValue("B".$i, $fix[0]["rks_riwayat_transfusi"]);
        $i++;
        $sheet->setCellValue("A".$i,"Reaksi Transfusi");
        $sheet->setCellValue("B".$i, $fix[0]["rks_reaksi_transfusi"]);
        $i++;
        $sheet->setCellValue("A".$i,"Riwayat Penyakit dalam Keluarga");
        $sheet->setCellValue("B".$i, $fix[0]["rks_riwayat_penyakit_keluarga"]);
        $i++;
        $sheet->setCellValue("A".$i,"Ketergantungan");
        $sheet->setCellValue("B".$i, $fix[0]["rks_ketergantungan"]);
        $i++;
        $sheet->setCellValue("A".$i,"Obat / Zat Lainnya");
        $sheet->setCellValue("B".$i, $fix[0]["rks_obat"]);
        $i++;
        $sheet->setCellValue("A".$i,"Jenis dan Jumlah per Hari");
        $sheet->setCellValue("B".$i, $fix[0]["rks_jenis_jmlh_perhari"]);
        $i += 2;
        
        $sheet->setCellValue("A".$i,"NUTRISI");
        $sheet->getStyle("A".$i.":B".$i)->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
        $i++;
        $sheet->setCellValue("A".$i,"Makan x/hari");
        $sheet->setCellValue("B".$i, $fix[0]["nutrisi_makan_perhari"]);
        $i++;
        $sheet->setCellValue("A".$i,"Minum ml/hari");
        $sheet->setCellValue("B".$i, $fix[0]["nutrisi_minum_perhari"]);
        $i++;
        $sheet->setCellValue("A".$i,"Diet Khusus");
        $sheet->setCellValue("B".$i, $fix[0]["nutrisi_diet_khusus"]);
        $i++;
        $sheet->setCellValue("A".$i,"Keluhan");
        $sheet->setCellValue("B".$i, $fix[0]["nutrisi_keluhan"]);
        $i++;
        $sheet->setCellValue("A".$i,"Perubahan Berat Badan 6 Bulan Terakhir");
        $sheet->setCellValue("B".$i, $fix[0]["nutrisi_perubahan_bb"]);
        $i += 2;
        
        $sheet->setCellValue("A".$i,"RIWAYAT ALERGI");
        $sheet->getStyle("A".$i.":B".$i)->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
        $i++;
        $sheet->setCellValue("A".$i,"Alergi");
        $sheet->setCellValue("B".$i, $fix[0]["alergi"]);
        $i++;
        $sheet->setCellValue("A".$i,"Obat, Tipe Reaksi");
        $sheet->setCellValue("B".$i, $fix[0]["alergi_obat_tipereaksi"]);
        $i++;
        $sheet->setCellValue("A".$i,"Makanan, Tipe Reaksi");
        $sheet->setCellValue("B".$i, $fix[0]["alergi_makanan_tipereaksi"]);
        $i++;
        $sheet->setCellValue("A".$i,"Lain-Lain, Tipe Reaksi");
        $sheet->setCellValue("B".$i, $fix[0]["alergi_lain_tipereaksi"]);
        $i += 2;
        
        $sheet->setCellValue("A".$i,"PENILAIAN NYERI");
        $sheet->getStyle("A".$i.":B".$i)->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
        $i++;
        $nyeriImgPath = 'medical_record/resource/img/skala_nyeri.jpg';
        if(file_exists($nyeriImgPath)) {
            $objDrawing = new PHPExcel_Worksheet_Drawing();
            $objDrawing->setPath($nyeriImgPath);
            $objDrawing->setResizeProportional(false);
            $objDrawing->setWidthAndHeight(61,75);
            $objDrawing->setOffsetX(1);
            $objDrawing->setOffsetY(1);
            $objDrawing->setCoordinates("A".$i);
            $objDrawing->setWorksheet($sheet);
        }
        $i += 4;
        $sheet->setCellValue("A".$i,"Nyeri");
        $sheet->setCellValue("B".$i, $fix[0]["nyeri"]);
        $i++;
        $sheet->setCellValue("A".$i,"Sifat");
        $sheet->setCellValue("B".$i, $fix[0]["sifat_nyeri"]);
        $i++;
        $sheet->setCellValue("A".$i,"Skala Nyeri");
        $sheet->setCellValue("B".$i, $fix[0]["skala_nyeri"]);
        $i += 2;
        
        $sheet->setCellValue("A".$i,"SKRINING RESIKO JATUH/CEDERA");
        $sheet->getStyle("A".$i.":B".$i)->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
        $i++;
        $sheet->setCellValue("A".$i,"Get Up and Go Test");
        $sheet->setCellValue("B".$i, $fix[0]["get_up_go_test"]);
        $i += 2;
        
        $sheet->setCellValue("A".$i,"KEBUTUHAN KOMUNIKASI / PENDIDIKAN DAN PENGAJARAN");
        $sheet->getStyle("A".$i.":B".$i)->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
        $i++;
        $sheet->setCellValue("A".$i,"Bicara");
        $sheet->setCellValue("B".$i, $fix[0]["bicara"]);
        $i++;
        $sheet->setCellValue("A".$i,"Perlu Penerjemah");
        $sheet->setCellValue("B".$i, $fix[0]["penerjemah"]);
        $i++;
        $sheet->setCellValue("A".$i,"Bahasa Isyarat");
        $sheet->setCellValue("B".$i, $fix[0]["bhs_isyarat"]);
        $i++;
        $sheet->setCellValue("A".$i,"Hambatan Belajar");
        $sheet->setCellValue("B".$i, $fix[0]["hambatan_belajar"]);
        $i++;
        $sheet->setCellValue("A".$i,"Catatan Lain - Lain");
        $sheet->setCellValue("B".$i, $fix[0]["catatan_lain"]);
        $i += 2;
        $sheet->setCellValue("A".$i,"DIAGNOSA PERAWAT");
        $sheet->setCellValue("B".$i, $fix[0]["diagnosa_perawat"]);
        $i += 3;
        $sheet->setCellValue("A".$i,"VERIVIKASI DPJP");
        $sheet->setCellValue("B".$i,"TANDA TANGAN PERAWAT");
        $sheet->getStyle("B".$i)->getFont()->setBold(true); 
        $i += 4;
        $sheet->setCellValue("A".$i,$fix[0]["nama_dokter"]);
        $sheet->setCellValue("B".$i,$fix[0]["nama_perawat"]);
        $sheet->getStyle("B".$i)->getFont()->setBold(true);
        
        foreach(range('A','B') as $columnID) {
            $sheet->getColumnDimension($columnID)->setAutoSize(true);
        }
        $sheet->getStyle('A1:A1000')->getFont()->setBold(true);
        $sheet->getStyle('B1:B1000')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT); 
        
        header ( 'Content-Type: application/vnd.ms-excel' );
        header ( 'Content-Disposition: attachment;filename="LaporanAsesmenKeperawatan.xls"' );
        header ( 'Cache-Control: max-age=0' );
        $writer = PHPExcel_IOFactory::createWriter ( $file, 'Excel5' );
        $writer->save ( 'php://output' );
		return;
    }
}

?>