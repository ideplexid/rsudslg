<?php

class LapKunRJResponder extends ServiceResponder {
    public function excel(){
        $this->addData("command","list");
        $this->getService()->setData($this->getData());
		$this->getService()->execute();
		$d=$this->getService()->getContent();
		$data=$d['data'];
        $fix = $this->getAdapter()->getContent($data);
        
        require_once ("smis-libs-out/php-excel/PHPExcel.php");
        $file = new PHPExcel ();
        $file->getProperties ()->setCreator ( "SIM RS" );
        $file->getProperties ()->setLastModifiedBy ( "SIM RS" );
        $file->getProperties ()->setTitle ( "Laporan Kunjungan Rawat Jalan" );
        $file->getProperties ()->setSubject ( "Laporan Kunjungan Rawat Jalan" );
        $file->getProperties ()->setDescription ( "Laporan Kunjungan Rawat Jalan Generated From system" );
        $file->getProperties ()->setKeywords ( "Laporan Kunjungan Rawat Jalan" );
        $file->getProperties ()->setCategory ( "Laporan Kunjungan Rawat Jalan" );
        $sheet = $file->getActiveSheet ();
        
        $no = 1;
        $sheet->setCellValue("A".$no,"LAPORAN KUNJUNGAN RAWAT JALAN");
        $sheet->mergeCells('A'.$no.':'.'O'.$no);
        $sheet->getStyle('A'.$no.':'.'O'.$no)->getFont()->setBold(true);
        $sheet->getStyle('A'.$no.':'.'O'.$no)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $no++;
        $sheet->setCellValue("A".$no, $_POST['tahun']);
        $sheet->mergeCells('A'.$no.':'.'O'.$no);
        $sheet->getStyle('A'.$no.':'.'O'.$no)->getFont()->setBold(true);
        $sheet->getStyle('A'.$no.':'.'O'.$no)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $no = $no + 2;
        
        $sheet->setCellValue("A".$no,"NO");
        $sheet->setCellValue("B".$no,"POLI/RUANG");
        $sheet->setCellValue("C".$no,"JUMLAH/BULAN");
        $sheet->setCellValue("O".$no,"JUMLAH");
        $i = $no+1;
        $sheet->mergeCells('A'.$no.':'.'A'.$i);
        $sheet->mergeCells('B'.$no.':'.'B'.$i);
        $sheet->mergeCells('C'.$no.':'.'N'.$no);
        $sheet->mergeCells('O'.$no.':'.'O'.$i);
        $sheet->getStyle('A'.$no.':'.'O'.$no)->getFont()->setBold(true);
        $sheet->getStyle('A'.$no.':'.'O'.$no)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $no++;
        $sheet->setCellValue("C".$no,"JAN");
        $sheet->setCellValue("D".$no,"FEB");
        $sheet->setCellValue("E".$no,"MAR");
        $sheet->setCellValue("F".$no,"APR");
        $sheet->setCellValue("G".$no,"MEI");
        $sheet->setCellValue("H".$no,"JUN");
        $sheet->setCellValue("I".$no,"JUL");
        $sheet->setCellValue("J".$no,"AGS");
        $sheet->setCellValue("K".$no,"SEP");
        $sheet->setCellValue("L".$no,"OKT");
        $sheet->setCellValue("M".$no,"NOV");
        $sheet->setCellValue("N".$no,"DES");
        $sheet->getStyle('A'.$no.':'.'O'.$no)->getFont()->setBold(true);
        $sheet->getStyle('A'.$no.':'.'O'.$no)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        
        $length = sizeof($fix);
        $nomor = 1;
        foreach($fix as $x){
            $no++;
            $sheet->setCellValue("A".$no, $nomor);
            $sheet->setCellValue("B".$no, $x['Poli/Ruang']);
            $sheet->setCellValue("C".$no, $x['Jan']);
            $sheet->setCellValue("D".$no, $x['Feb']);
            $sheet->setCellValue("E".$no, $x['Mar']);
            $sheet->setCellValue("F".$no, $x['Apr']);
            $sheet->setCellValue("G".$no, $x['Mei']);
            $sheet->setCellValue("H".$no, $x['Jun']);
            $sheet->setCellValue("I".$no, $x['Jul']);
            $sheet->setCellValue("J".$no, $x['Ags']);
            $sheet->setCellValue("K".$no, $x['Sep']);
            $sheet->setCellValue("L".$no, $x['Okt']);
            $sheet->setCellValue("M".$no, $x['Nov']);
            $sheet->setCellValue("N".$no, $x['Des']);
            $sheet->setCellValue("O".$no, $x['Total']);
            $nomor++;
        }
        
        foreach(range('A','B') as $columnID) {
            $sheet->getColumnDimension($columnID)->setAutoSize(true);
        }
        
        header ( 'Content-Type: application/vnd.ms-excel' );
        header ( 'Content-Disposition: attachment;filename="Laporan Kunjungan Rawat Jalan Tahun.xls"' );
        header ( 'Cache-Control: max-age=0' );
        $writer = PHPExcel_IOFactory::createWriter ( $file, 'Excel5' );
        $writer->save ( 'php://output' );
		return;
    }
}

?>