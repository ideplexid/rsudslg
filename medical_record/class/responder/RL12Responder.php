<?php

class RL12Responder extends DBResponder {
    public function excel() {
        $data=$this->dbtable->view("",0);
        $fix=$this->adapter->getContent($data['data']);
        
        require_once ("smis-libs-out/php-excel/PHPExcel.php");
        $file = new PHPExcel ();
        $file->getProperties ()->setCreator ( "Rumah Sakit Syamrabu" );
        $file->getProperties ()->setLastModifiedBy ( "Rumah Sakit Bagian Administrator" );
        $file->getProperties ()->setTitle ( "Laporan RL 1.02 Indikator Pelayanan" );
        $file->getProperties ()->setSubject ( "Laporan RL 1.02" );
        $file->getProperties ()->setDescription ( "Laporan RL 1.02 Indikator Pelayanan Generated From system" );
        $file->getProperties ()->setKeywords ( "Laporan RL 1.02" );
        $file->getProperties ()->setCategory ( "Laporan RL 1.02" );
        $sheet = $file->getActiveSheet ();
        
        $no = 1;
        $sheet->setCellValue("A".$no,"No.");
        $sheet->setCellValue("B".$no,"Ruangan");
        $sheet->setCellValue("C".$no,"BOR");    
        $sheet->setCellValue("D".$no,"LOS");    
        $sheet->setCellValue("E".$no,"BTO");
        $sheet->setCellValue("F".$no,"TOI");
        $sheet->setCellValue("G".$no,"NDR");
        $sheet->setCellValue("H".$no,"GDR");
        $sheet->setCellValue("I".$no,"Rata/Hari");
        $sheet->setCellValue("J".$no,"Rata/Bulan");
        $sheet->getStyle('A'.$no.':'.'J'.$no)->getFont()->setBold(true);
        $sheet->getStyle('A'.$no.':'.'J'.$no)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        
        foreach($fix as $x) {
            $no++;
            $sheet->setCellValue("A".$no,$x["No."]);
            $sheet->setCellValue("B".$no,$x["Ruangan"]);
            $sheet->setCellValue("C".$no,$x["BOR"]);    
            $sheet->setCellValue("D".$no,$x["LOS"]);    
            $sheet->setCellValue("E".$no,$x["BTO"]);
            $sheet->setCellValue("F".$no,$x["TOI"]);
            $sheet->setCellValue("G".$no,$x["NDR"]);
            $sheet->setCellValue("H".$no,$x["GDR"]);
            $sheet->setCellValue("I".$no,$x["Rata/Hari"]);
            $sheet->setCellValue("J".$no,$x["Rata/Bulan"]);
            $sheet->getStyle('A'.$no.':'.'J'.$no)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        }
        foreach(range('A','J') as $columnID) {
            $sheet->getColumnDimension($columnID)->setAutoSize(true);
        }
        
        header ( 'Content-Type: application/vnd.ms-excel' );
        header ( 'Content-Disposition: attachment;filename="Laporan RL 1.02.xls"' );
        header ( 'Cache-Control: max-age=0' );
        $writer = PHPExcel_IOFactory::createWriter ( $file, 'Excel5' );
        $writer->save ( 'php://output' );
		return;
    }
}

?>