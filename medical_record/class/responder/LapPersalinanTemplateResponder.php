<?php
show_error();
require_once "smis-base/smis-include-duplicate.php";
class LapPersalinanTemplateResponder extends DuplicateResponder {
    public function save() {
        $data					= $this->postToArray();
		$id['id']				= $this->getPost("id", 0);
		$temp['noreg_pasien']	= $data['noreg_pasien'];
		$temp['ruangan']		= $data['ruangan'];
		if(($id['id']==0 || $id['id']=="") && (!$this->dbtable->is_exist($temp,$this->is_exist_include_del))){
			$result				= $this->dbtable->insert($data,$this->is_use_prop,$this->upsave_condition);
			$id['id']			= $this->dbtable->get_inserted_id();
			$success['type']	= 'insert';
		}else {
            if($data['id'] == 0) {
                $select     = $this->dbtable->select($temp);
                $data['id'] = $select->id;
            }
			$result				= $this->dbtable->update($data,$temp,$this->upsave_condition);
			$success['type']	= 'update';
		}
		$success['id']			= $id['id'];
		$success['success']		= 1;
		if($result===false){
			$success['success'] = 0;
		}
		return $success;
    }
}

?>