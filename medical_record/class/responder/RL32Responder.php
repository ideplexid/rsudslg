<?php

class RL32Responder extends DBResponder {
    public function excel() {
        $data=$this->dbtable->view("",0);
        $fix=$this->adapter->getContent($data['data']);
        
        require_once ("smis-libs-out/php-excel/PHPExcel.php");
        $file = new PHPExcel ();
        $file->getProperties ()->setLastModifiedBy ( "Rumah Sakit Bagian Administrator" );
        $file->getProperties ()->setTitle ( "Laporan RL 3.2 Rawat Darurat" );
        $file->getProperties ()->setSubject ( "Laporan RL 3.2" );
        $file->getProperties ()->setDescription ( "Laporan RL 3.2 Rawat Darurat Generated From system" );
        $file->getProperties ()->setKeywords ( "Laporan RL 3.2" );
        $file->getProperties ()->setCategory ( "Laporan RL 3.2" );
        $sheet = $file->getActiveSheet ();
        
        
        $row = $this->getDBTable()->get_db()->get_row("
            SELECT *
            FROM smis_mr_data_dasar_rs
            WHERE prop = ''
            LIMIT 0, 1
        ");
        //echo json_encode($row);
        $no=1;
        $sheet->setCellValue("A".$no,"Kode Propinsi");
        $sheet->setCellValue("B".$no,"Kabupaten/Kota");
        $sheet->setCellValue("C".$no,"Kode RS");
        $sheet->setCellValue("D".$no,"Nama RS");
        $sheet->setCellValue("E".$no,"Tahun");
        
        $sheet->setCellValue("F".$no,"No.");
        $sheet->setCellValue("G".$no,"Jenis");
        $sheet->setCellValue("H".$no,"Total Pasien Rujukan");    
        $sheet->setCellValue("I".$no,"Total Pasien Non Rujukan");    
        $sheet->setCellValue("J".$no,"Tindak Lanjut Pelayanan Dirawat");
        $sheet->setCellValue("K".$no,"Tindak Lanjut Pelayanan Dirujuk");
        $sheet->setCellValue("L".$no,"Tindak Lanjut Pelayanan Pulang");
        $sheet->setCellValue("M".$no,"Meninggal di UGD");
        $sheet->setCellValue("N".$no,"DOA");

        $sheet->getStyle('A'.$no.':'.'N'.$no)->getFont()->setBold(true);
        $sheet->getStyle('A'.$no.':'.'N'.$no)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        
        foreach($fix as $x) {
            $no++;
            $sheet->setCellValue("A".$no,$row->kode_provinsi);
            $sheet->setCellValue("B".$no,$row->kota_kabupaten);
            $sheet->setCellValue("C".$no,$row->koders);
            $sheet->setCellValue("D".$no,$row->nama_rs);
            $sheet->setCellValue("E".$no,$_POST['tahun']);

            $sheet->setCellValue("F".$no,$x["No."]);
            $sheet->setCellValue("G".$no,$x["Jenis"]);
            $sheet->setCellValue("H".$no,$x["Rujukan"]);    
            $sheet->setCellValue("I".$no,$x["Non-Rujukan"]);    
            $sheet->setCellValue("J".$no,$x["Tindak Lanjut Dirawat"]);
            $sheet->setCellValue("K".$no,$x["Tindak Lanjut Dirujuk"]);
            $sheet->setCellValue("L".$no,$x["Pulang Rawat Jalan"]);
            $sheet->setCellValue("M".$no,$x["Meninggal di UGD"]);
            $sheet->setCellValue("N".$no,$x["DOA"]);

            $sheet->getStyle('A'.$no)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $sheet->getStyle('B'.$no.":G".$no)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            $sheet->getStyle('H'.$no.':'.'N'.$no)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        }
        
        foreach(range('A','N') as $columnID) {
            $sheet->getColumnDimension($columnID)->setAutoSize(true);
        }

        $thin = array ();
        $thin['borders']=array();
        $thin['borders']['allborders']=array();
        $thin['borders']['allborders']['style']=PHPExcel_Style_Border::BORDER_THIN ;
        $sheet->getStyle ( 'A1:N'.$no )->applyFromArray ($thin);

        header ( 'Content-Type: application/vnd.ms-excel' );
        header ( 'Content-Disposition: attachment;filename="Laporan RL 3.2 - Rawat Darurat.xls"' );
        header ( 'Cache-Control: max-age=0' );
        $writer = PHPExcel_IOFactory::createWriter ( $file, 'Excel5' );
        $writer->save ( 'php://output' );
		return;
    }
}

?>