<?php

class LapKegiatanGigiMulutResponder extends DBResponder{
    public function excel(){
        loadLibrary("smis-libs-function-excel");
        
        $query_view="SELECT tanggal , 
                    sum(tumpatan_gigi_tetap) as tumpatan_gigi_tetap,
                    sum(tumpatan_gigi_sulung) as tumpatan_gigi_sulung,
                    sum(pengobatan_pulpa) as pengobatan_pulpa,
                    sum(if(pencabutan_gigi_tetap='',0,1)) as pencabutan_gigi_tetap,
                    sum(if(pencabutan_gigi_sulung='',0,1)) as pencabutan_gigi_sulung,
                    sum(if(pengobatan_periodontal='',0,1)) as pengobatan_periodontal,
                    sum(if(pengobatan_abses='',0,1)) as pengobatan_abses,
                    sum(pembersihan_karang_gigi) as pembersihan_karang_gigi,
                    sum(prothese_lengkap) as prothese_lengkap,
                    sum(prothese_sebagian) as prothese_sebagian,
                    sum(prothese_cekat) as prothese_cekat,
                    sum(if(orthodonti='',0,1)) as orthodonti,
                    sum(jacket_bridge) as jacket_bridge,
                    sum(bedah_mulut) as bedah_mulut,
                    sum(pasca_bedah) as pasca_bedah,
                    sum(if(preventif='',0,1)) as preventif,
                    sum(if(konservasi='',0,1)) as konservasi,
                    sum(if(periodonsia='',0,1)) as periodonsia,
                    sum(ro_photo) as ro_photo,
                    sum(cetak_crovo) as cetak_crovo,
                    sum(cetak_pasak) as cetak_pasak,
                    sum(splinting) as splinting,
                    sum(insersi_pasak) as insersi_pasak,
                    sum(if(jk=0,1,0)) as laki_laki,
                    sum(if(jk=1,1,0)) as perempuan
                    FROM smis_mr_gigimulut
                    ";
        $query_count="SELECT count(*) FROM smis_mr_gigimulut";
        $this->dbtable->setPreferredQuery(true,$query_view,$query_count);
        $this->dbtable->setFetchMethode(DBTable::$ARRAY_FETCH);
        $this->dbtable->setGroupBy(true," tanggal ");
        $this->dbtable->setOrder(" tanggal ASC ",true);
        $this->dbtable->setShowAll(true);
        $this->dbtable->addCustomKriteria(null," tanggal >='".$_POST['dari']."'");
        $this->dbtable->addCustomKriteria(null," tanggal <'".$_POST['sampai']."' ");
        $data=$this->dbtable->view("",0);
        $list=$data['data'];
        
        require_once ("smis-libs-out/php-excel/PHPExcel.php");
        $file = new PHPExcel ();
        $file->getProperties ()->setCreator ( "System" );
        $file->getProperties ()->setLastModifiedBy ( "Administrator" );
        $file->getProperties ()->setTitle ( "Laporan Kegiatan Gigi dan Mulut" );
        $file->getProperties ()->setSubject ( "Laporan Kegiatan Gigi dan Mulut" );
        $file->getProperties ()->setDescription ( "Laporan Kegiatan Gigi dan Mulut Generated From system" );
        $file->getProperties ()->setKeywords ( "Laporan Kegiatan Gigi dan Mulut" );
        $file->getProperties ()->setCategory ( "Laporan Kegiatan Gigi dan Mulut" );
        $sheet = $file->getActiveSheet ();
        
        $no = 1;
        $sheet->setCellValue("A".$no,"No.");
        $sheet->setCellValue("B".$no,"Tanggal");
        $sheet->setCellValue("C".$no,"Tumpatan Gigi Tetap");    
        $sheet->setCellValue("D".$no,"Tumpatan Gigi Sulung");    
        $sheet->setCellValue("E".$no,"Pengobatan Pulpa");
        $sheet->setCellValue("F".$no,"Pencabutan Gigi Tetap");
        $sheet->setCellValue("G".$no,"Pencabutan Gigi Sulung");
        $sheet->setCellValue("H".$no,"Pengobatan Periodontal");
        $sheet->setCellValue("I".$no,"Pengobatan Abses");
        $sheet->setCellValue("J".$no,"Pembersihan Karang Gigi");
        $sheet->setCellValue("K".$no,"Prothese Lengkap");
        $sheet->setCellValue("L".$no,"Prothese Sebagian");
        $sheet->setCellValue("M".$no,"Prothese Cekat");
        $sheet->setCellValue("N".$no,"Orthodonti");
        $sheet->setCellValue("O".$no,"Jacket Bridge");
        $sheet->setCellValue("P".$no,"Bedah Mulut");
        $sheet->setCellValue("Q".$no,"Pasca Bedah");
        $sheet->setCellValue("R".$no,"Preventif");
        $sheet->setCellValue("S".$no,"Konservasi");
        $sheet->setCellValue("T".$no,"Periodonsia");
        $sheet->setCellValue("U".$no,"RO Photo");
        $sheet->setCellValue("V".$no,"Cetak Crovo");
        $sheet->setCellValue("W".$no,"Cetak Pasak");
        $sheet->setCellValue("X".$no,"Splinting");
        $sheet->setCellValue("Y".$no,"Insersi Pasak");
        $sheet->setCellValue("Z".$no,"Total Tindakan");
        $sheet->setCellValue("AA".$no,"Laki-Laki");
        $sheet->setCellValue("AB".$no,"Perempuan");
        $sheet->setCellValue("AC".$no,"Total Pasien");
        
        $sheet  ->getStyle('A'.$no.':AC'.$no)
                ->getFont()
                ->setBold(true);
        
        foreach($list as $x){
            $no++;
            $sheet->setCellValue("A".$no,($no-1).".");
            date_excel_format($sheet,"B".$no,$x["tanggal"]); 
            $sheet->setCellValue("C".$no,$x["tumpatan_gigi_tetap"]);
            $sheet->setCellValue("D".$no,$x["tumpatan_gigi_sulung"]);
            $sheet->setCellValue("E".$no,$x["pengobatan_pulpa"]);
            $sheet->setCellValue("F".$no,$x["pencabutan_gigi_tetap"]);
            $sheet->setCellValue("G".$no,$x["pencabutan_gigi_sulung"]);
            $sheet->setCellValue("H".$no,$x["pengobatan_periodontal"]);
            $sheet->setCellValue("I".$no,$x["pengobatan_abses"]);
            $sheet->setCellValue("J".$no,$x["pembersihan_karang_gigi"]);
            $sheet->setCellValue("K".$no,$x["prothese_lengkap"]);
            $sheet->setCellValue("L".$no,$x["prothese_sebagian"]);
            $sheet->setCellValue("M".$no,$x["prothese_cekat"]);
            $sheet->setCellValue("N".$no,$x["orthodonti"]);
            $sheet->setCellValue("O".$no,$x["jacket_bridge"]);
            $sheet->setCellValue("P".$no,$x["bedah_mulut"]);
            $sheet->setCellValue("Q".$no,$x["pasca_bedah"]);
            $sheet->setCellValue("R".$no,$x["preventif"]);
            $sheet->setCellValue("S".$no,$x["konservasi"]);
            $sheet->setCellValue("T".$no,$x["periodonsia"]);
            $sheet->setCellValue("U".$no,$x["ro_photo"]);
            $sheet->setCellValue("V".$no,$x["cetak_crovo"]);
            $sheet->setCellValue("W".$no,$x["cetak_pasak"]);
            $sheet->setCellValue("X".$no,$x["splinting"]);
            $sheet->setCellValue("Y".$no,$x["insersi_pasak"]);
            $sheet->setCellValue("Z".$no,"=sum(C".$no.":Y".$no.")");
            $sheet->setCellValue("AA".$no,$x["laki_laki"]);
            $sheet->setCellValue("AB".$no,$x["perempuan"]);
            $sheet->setCellValue("AC".$no,"=sum(AA".$no.":AB".$no.")");
        }
        $row_total=$no;
        $no++;
        $sheet->setCellValue("B".$no,"TOTAL");
        $sheet->setCellValue("C".$no,"=sum(C2:C".($row_total).")");
        $sheet->setCellValue("D".$no,"=sum(D2:D".($row_total).")");
        $sheet->setCellValue("E".$no,"=sum(E2:E".($row_total).")");
        $sheet->setCellValue("F".$no,"=sum(F2:F".($row_total).")");
        $sheet->setCellValue("G".$no,"=sum(G2:G".($row_total).")");
        $sheet->setCellValue("H".$no,"=sum(H2:H".($row_total).")");
        $sheet->setCellValue("I".$no,"=sum(I2:I".($row_total).")");
        $sheet->setCellValue("J".$no,"=sum(J2:J".($row_total).")");
        $sheet->setCellValue("K".$no,"=sum(K2:K".($row_total).")");
        $sheet->setCellValue("L".$no,"=sum(L2:L".($row_total).")");
        $sheet->setCellValue("M".$no,"=sum(M2:M".($row_total).")");
        $sheet->setCellValue("N".$no,"=sum(N2:N".($row_total).")");
        $sheet->setCellValue("O".$no,"=sum(O2:O".($row_total).")");
        $sheet->setCellValue("P".$no,"=sum(P2:P".($row_total).")");
        $sheet->setCellValue("Q".$no,"=sum(Q2:Q".($row_total).")");
        $sheet->setCellValue("R".$no,"=sum(R2:R".($row_total).")");
        $sheet->setCellValue("S".$no,"=sum(S2:S".($row_total).")");
        $sheet->setCellValue("T".$no,"=sum(T2:T".($row_total).")");
        $sheet->setCellValue("U".$no,"=sum(U2:U".($row_total).")");
        $sheet->setCellValue("V".$no,"=sum(V2:V".($row_total).")");
        $sheet->setCellValue("W".$no,"=sum(W2:W".($row_total).")");
        $sheet->setCellValue("X".$no,"=sum(X2:X".($row_total).")");
        $sheet->setCellValue("Y".$no,"=sum(Y2:Y".($row_total).")");
        $sheet->setCellValue("Z".$no,"=sum(Z2:Z".($row_total).")");
        $sheet->setCellValue("AA".$no,"=sum(AA2:AA".($row_total).")");
        $sheet->setCellValue("AB".$no,"=sum(AB2:AB".($row_total).")");
        $sheet->setCellValue("AC".$no,"=sum(AC2:AC".($row_total).")");
        
        $sheet->getColumnDimension("A")->setAutoSize(true);
        $sheet->getColumnDimension("B")->setAutoSize(true);
        $sheet->getColumnDimension("C")->setAutoSize(true);
        $sheet->getColumnDimension("D")->setAutoSize(true);
        $sheet->getColumnDimension("E")->setAutoSize(true);
        $sheet->getColumnDimension("F")->setAutoSize(true);
        $sheet->getColumnDimension("G")->setAutoSize(true);
        $sheet->getColumnDimension("H")->setAutoSize(true);
        $sheet->getColumnDimension("I")->setAutoSize(true);
        $sheet->getColumnDimension("J")->setAutoSize(true);
        $sheet->getColumnDimension("K")->setAutoSize(true);
        $sheet->getColumnDimension("L")->setAutoSize(true);
        $sheet->getColumnDimension("M")->setAutoSize(true);
        $sheet->getColumnDimension("N")->setAutoSize(true);
        $sheet->getColumnDimension("O")->setAutoSize(true);
        $sheet->getColumnDimension("P")->setAutoSize(true);
        $sheet->getColumnDimension("Q")->setAutoSize(true);
        $sheet->getColumnDimension("R")->setAutoSize(true);
        $sheet->getColumnDimension("S")->setAutoSize(true);
        $sheet->getColumnDimension("T")->setAutoSize(true);
        $sheet->getColumnDimension("U")->setAutoSize(true);
        $sheet->getColumnDimension("V")->setAutoSize(true);
        $sheet->getColumnDimension("W")->setAutoSize(true);
        $sheet->getColumnDimension("X")->setAutoSize(true);
        $sheet->getColumnDimension("Y")->setAutoSize(true);
        $sheet->getColumnDimension("Z")->setAutoSize(true);
        $sheet->getColumnDimension("AA")->setAutoSize(true);
        $sheet->getColumnDimension("AB")->setAutoSize(true);
        $sheet->getColumnDimension("AC")->setAutoSize(true);
        $sheet->getColumnDimension("AD")->setAutoSize(true);
        
        $sheet  ->getStyle('A'.$no.':AC'.$no)
                ->getFont()
                ->setBold(true);
                
        $thin = array ();
        $thin['borders']=array();
        $thin['borders']['allborders']=array();
        $thin['borders']['allborders']['style']=PHPExcel_Style_Border::BORDER_DASHDOT ;
        $sheet  ->getStyle ( 'A1:AC'.$no )->applyFromArray ($thin);
        
        header ( 'Content-Type: application/vnd.ms-excel' );
        header ( 'Content-Disposition: attachment;filename="Laporan Kegiatan Gigi dan Mulut.xls"' );
        header ( 'Cache-Control: max-age=0' );
        $writer = PHPExcel_IOFactory::createWriter ( $file, 'Excel5' );
        $writer->save ( 'php://output' );
		return;
    }
}

?>