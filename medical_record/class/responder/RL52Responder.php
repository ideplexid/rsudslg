<?php

class RL52Responder extends ServiceResponder {
    public function excel() {
        $this->addData("command","list");
        $this->getService()->setData($this->getData());
		$this->getService()->execute();
		$d=$this->getService()->getContent();
		$data=$d['data'];
        $fix = $this->getAdapter()->getContent($data);
        
        require_once ("smis-libs-out/php-excel/PHPExcel.php");
        $file = new PHPExcel ();
        $file->getProperties ()->setCreator ( "Rumah Sakit Syamrabu" );
        $file->getProperties ()->setLastModifiedBy ( "Rumah Sakit Bagian Administrator" );
        $file->getProperties ()->setTitle ( "Laporan RL 5.02 Pengunjung RJ" );
        $file->getProperties ()->setSubject ( "Laporan RL 5.02" );
        $file->getProperties ()->setDescription ( "Laporan RL 5.02 Pengunjung RJ Generated From system" );
        $file->getProperties ()->setKeywords ( "Laporan RL 5.02" );
        $file->getProperties ()->setCategory ( "Laporan RL 5.02" );
        $sheet = $file->getActiveSheet ();
        
        $no = 1;
        $sheet->setCellValue("A".$no,"No.");
        $sheet->setCellValue("B".$no,"Tahun");
        $sheet->setCellValue("C".$no,"Bulan");
        $sheet->setCellValue("D".$no,"Baru");
        $sheet->setCellValue("E".$no,"Lama");
        $sheet->setCellValue("F".$no,"Total");
        
        $sheet->getStyle('A'.$no.':'.'F'.$no)->getFont()->setBold(true);
        $sheet->getStyle('A'.$no.':'.'F'.$no)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        
        foreach($fix as $x)
        {
            $no++;
            $sheet->setCellValue("A".$no,$x["No."]);
            $sheet->setCellValue("B".$no,$x['Tahun']);
            $sheet->setCellValue("C".$no,$x['Bulan']);
            $sheet->setCellValue("D".$no,$x['Baru']);
            $sheet->setCellValue("E".$no,$x['Lama']);
            $sheet->setCellValue("F".$no,$x['Total']);
        
            $sheet->getStyle('A'.$no.':'.'F'.$no)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        }
        
        foreach(range('A','F') as $columnID) {
            $sheet->getColumnDimension($columnID)->setAutoSize(true);
        }
        
        header ( 'Content-Type: application/vnd.ms-excel' );
        header ( 'Content-Disposition: attachment;filename="Laporan RL 5.02.xls"' );
        header ( 'Cache-Control: max-age=0' );
        $writer = PHPExcel_IOFactory::createWriter ( $file, 'Excel5' );
        $writer->save ( 'php://output' );
		return;
    }
}

?>