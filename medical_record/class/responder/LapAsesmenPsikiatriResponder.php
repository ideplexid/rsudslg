<?php 

class LapAsesmenPsikiatriResponder extends DBResponder {
    public function excel() {
        $this->dbtable->addCustomKriteria("id", " = '".$_POST['id']."'");
        $data=$this->dbtable->view("",0);
        $fix=$this->adapter->getContent($data['data']);
        
        require_once ("smis-libs-out/php-excel/PHPExcel.php");
        $file = new PHPExcel ();
        $file->getProperties ()->setCreator ( "Rumah Sakit Syamrabu" );
        $file->getProperties ()->setLastModifiedBy ( "Rumah Sakit Bagian Administrator" );
        $file->getProperties ()->setTitle ( "Laporan Asesmen Psikiatri" );
        $file->getProperties ()->setSubject ( "Laporan Asesmen Psikiatri" );
        $file->getProperties ()->setDescription ( "Laporan Asesmen Psikiatri Generated From system" );
        $file->getProperties ()->setKeywords ( "Laporan Asesmen Psikiatri" );
        $file->getProperties ()->setCategory ( "Laporan Asesmen Psikiatri" );
        $sheet = $file->getActiveSheet ();
        
        $i = 1;
        $sheet->setCellValue("A".$i,"ASESMEN AWAL PSIKIATRI");
        $sheet->mergeCells("A".$i.":C".$i);
        $i += 2;
        
        $sheet->setCellValue("A".$i,"NAMA");
        $sheet->setCellValue("B".$i, $fix[0]["nama_pasien"]);
        $i++;
        $sheet->setCellValue("A".$i,"JENIS KELAMIN");
        $sheet->setCellValue("B".$i, $fix[0]["jk"]);
        $i++;
        $sheet->setCellValue("A".$i,"NO. RM");
        $sheet->setCellValue("B".$i, $fix[0]["NRM"]);
        $i++;
        $sheet->setCellValue("A".$i,"TANGGAL LAHIR");
        $sheet->setCellValue("B".$i, $fix[0]["tgl_lahir_pasien"]);
        $i++;
        $sheet->setCellValue("A".$i,"ALAMAT");
        $sheet->setCellValue("B".$i, $fix[0]["alamat_pasien"]);
        $i += 2;
        
        $sheet->setCellValue("A".$i,"TANGGAL MASUK");
        $sheet->setCellValue("B".$i, $fix[0]["tanggal_masuk"]);
        $i++;
        $sheet->setCellValue("A".$i,"TANGGAL ASESMEN");
        $sheet->setCellValue("B".$i, $fix[0]["tgl_asesmen"]);
        $i++;
        $sheet->setCellValue("A".$i,"JAM ASESMEN");
        $sheet->setCellValue("B".$i, $fix[0]["jam_asesmen"]);
        $i += 2;
        
        $sheet->setCellValue("A".$i,"ANAMNESES");
        $sheet->getStyle("A".$i)->getFont()->setUnderline(true);
        $i++;
        $sheet->setCellValue("A".$i,"Wawancara");
        $i++;
        $sheet->setCellValue("A".$i,"1. Keluhan Utama");
        $sheet->setCellValue("B".$i, $fix[0]["keluhan_utama"]);
        $i++;
        $sheet->setCellValue("A".$i,"2. Riwayat Penyakit Sekarang");
        $sheet->setCellValue("B".$i, $fix[0]["riwayat_penyakit_skrg"]);
        $i++;
        $sheet->setCellValue("A".$i,"- Auto Anamnesis");
        $sheet->setCellValue("B".$i, $fix[0]["auto_anamnesis"]);
        $i++;
        $sheet->setCellValue("A".$i,"- Hetero Namnesis");
        $sheet->setCellValue("B".$i, $fix[0]["hetero_namnesis"]);
        $i++;
        $sheet->setCellValue("A".$i,"- Faktor Pencetus/Penyebab");
        $sheet->setCellValue("B".$i, $fix[0]["faktor_penyebab"]);
        $i++;
        $sheet->setCellValue("A".$i,"- Faktor Keluarga");
        $sheet->setCellValue("B".$i, $fix[0]["faktor_keluarga"]);
        $i++;
        $sheet->setCellValue("A".$i,"- Faktor Kerja/Sosial");
        $sheet->setCellValue("B".$i, $fix[0]["fungsi_kerja"]);
        $i++;
        $sheet->setCellValue("A".$i,"- Riwayat NAPZA");
        $sheet->setCellValue("B".$i, $fix[0]["riwayat_napza"]);
        $i++;
        $sheet->setCellValue("A".$i,"Lama Pemakaian");
        $sheet->setCellValue("B".$i, $fix[0]["napza_lama_pemakaian"]);
        $i++;
        $sheet->setCellValue("A".$i,"Jenis Zat");
        $sheet->setCellValue("B".$i, $fix[0]["napza_jenis_zat"]);
        $i++;
        $sheet->setCellValue("A".$i,"Cara Pemakaian");
        $sheet->setCellValue("B".$i, $fix[0]["napza_cara_pemakaian"]);
        $i++;
        $sheet->setCellValue("A".$i,"Latar Belakang Pemakaian");
        $sheet->setCellValue("B".$i, $fix[0]["napza_latarbelakang_pemakaian"]);
        $i++;
        $sheet->setCellValue("A".$i,"- Faktor Premorbid");
        $sheet->setCellValue("B".$i, $fix[0]["faktor_premorbid"]);
        $i++;
        $sheet->setCellValue("A".$i,"- Faktor Organik");
        $sheet->setCellValue("B".$i, $fix[0]["faktor_organik"]);
        $i += 2;
        
        $sheet->setCellValue("A".$i,"STATUS PSIKIATRI");
        $sheet->getStyle("A".$i)->getFont()->setUnderline(true);
        $i++;
        $sheet->setCellValue("A".$i,"- Kesan Umum");
        $sheet->setCellValue("B".$i, $fix[0]["kesan_umum"]);
        $i++;
        $sheet->setCellValue("A".$i,"- Kesadaran");
        $sheet->setCellValue("B".$i, $fix[0]["kesadaran"]);
        $i++;
        $sheet->setCellValue("A".$i,"- Mood/Afek");
        $sheet->setCellValue("B".$i, $fix[0]["mood"]);
        $i++;
        $sheet->setCellValue("A".$i,"- Proses Pikir");
        $sheet->setCellValue("B".$i, $fix[0]["proses_pikir"]);
        $i++;
        $sheet->setCellValue("A".$i,"- Pencerapan");
        $sheet->setCellValue("B".$i, $fix[0]["pencerapan"]);
        $i++;
        $sheet->setCellValue("A".$i,"- Dorongan Insting");
        $sheet->setCellValue("B".$i, $fix[0]["dorongan_insting"]);
        $i++;
        $sheet->setCellValue("A".$i,"- Psikomotor");
        $sheet->setCellValue("B".$i, $fix[0]["psikomotor"]);
        $i += 2;
        
        $sheet->setCellValue("A".$i,"STATUS PENYAKIT DALAM");
        $sheet->getStyle("A".$i)->getFont()->setUnderline(true);
        $sheet->setCellValue("B".$i, $fix[0]["penyakit_dalam"]);
        $i += 2;
        
        $sheet->setCellValue("A".$i,"STATUS NEUROLOGI");
        $sheet->getStyle("A".$i)->getFont()->setUnderline(true);
        $sheet->setCellValue("B".$i, $fix[0]["neurologi"]);
        $i += 2;
        
        $sheet->setCellValue("A".$i,"DIAGNOSA KERJA/DIAGNOSA BANDING");
        $sheet->getStyle("A".$i)->getFont()->setUnderline(true);
        $sheet->setCellValue("B".$i, $fix[0]["diagnosis_kerja"]);
        $i += 2;
        
        $sheet->setCellValue("A".$i,"TERAPI/TINDAKAN");
        $sheet->getStyle("A".$i)->getFont()->setUnderline(true);
        $sheet->setCellValue("B".$i, $fix[0]["terapi"]);
        $i += 2;
        
        $sheet->setCellValue("A".$i,"RENCANA KERJA");
        $sheet->getStyle("A".$i)->getFont()->setUnderline(true);
        $sheet->setCellValue("B".$i, $fix[0]["rencana_kerja"]);
        $i += 2;
        
        $sheet->setCellValue("A".$i,"DISPOSISI");
        $sheet->getStyle("A".$i)->getFont()->setUnderline(true);
        $i++;
        $sheet->setCellValue("A".$i,"Tanggal Pulang");
        $sheet->setCellValue("B".$i, $fix[0]["tgl_pulang"]);
        $i++;
        $sheet->setCellValue("A".$i,"Jam Pulang");
        $sheet->setCellValue("B".$i, $fix[0]["jam_pulang"]);
        $i++;
        $sheet->setCellValue("A".$i,"Tanggal Kontrol Klinik");
        $sheet->setCellValue("B".$i, $fix[0]["tgl_kontrol_klinik"]);
        $i++;
        $sheet->setCellValue("A".$i,"Dirawat Di Ruang");
        $sheet->setCellValue("B".$i, $fix[0]["dirawat_diruang"]);
        
        foreach(range('A','B') as $columnID) {
            $sheet->getColumnDimension($columnID)->setAutoSize(true);
        }
        $sheet->getStyle('A1:A100')->getFont()->setBold(true);
        $sheet->getStyle('B1:B100')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); 
        
        header ( 'Content-Type: application/vnd.ms-excel' );
        header ( 'Content-Disposition: attachment;filename="LaporanAsesmenPsikiatri.xls"' );
        header ( 'Cache-Control: max-age=0' );
        $writer = PHPExcel_IOFactory::createWriter ( $file, 'Excel5' );
        $writer->save ( 'php://output' );
		return;
    }
}

?>