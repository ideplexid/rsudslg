<?php 

class LapAsesmenPsikologiResponder extends DBResponder {
    public function excel() {
        $this->dbtable->addCustomKriteria("id", " = '".$_POST['id']."'");
        $data=$this->dbtable->view("",0);
        $fix=$this->adapter->getContent($data['data']);
        
        require_once ("smis-libs-out/php-excel/PHPExcel.php");
        $file = new PHPExcel ();
        $file->getProperties ()->setCreator ( "Rumah Sakit Syamrabu" );
        $file->getProperties ()->setLastModifiedBy ( "Rumah Sakit Bagian Administrator" );
        $file->getProperties ()->setTitle ( "Laporan Asesmen Psikologi" );
        $file->getProperties ()->setSubject ( "Laporan Asesmen Psikologi" );
        $file->getProperties ()->setDescription ( "Laporan Asesmen Psikologi Generated From system" );
        $file->getProperties ()->setKeywords ( "Laporan Asesmen Psikologi" );
        $file->getProperties ()->setCategory ( "Laporan Asesmen Psikologi" );
        $sheet = $file->getActiveSheet ();
        
        $i = 1;
        $sheet->setCellValue("A".$i,"ASESMEN AWAL PSIKOLOGI");
        $sheet->mergeCells("A".$i.":C".$i);
        $i += 2;
        
        $sheet->setCellValue("A".$i,"NAMA");
        $sheet->setCellValue("B".$i, $fix[0]["nama_pasien"]);
        $i++;
        $sheet->setCellValue("A".$i,"JENIS KELAMIN");
        $sheet->setCellValue("B".$i, $fix[0]["jk"]);
        $i++;
        $sheet->setCellValue("A".$i,"NO. RM");
        $sheet->setCellValue("B".$i, $fix[0]["NRM"]);
        $i++;
        $sheet->setCellValue("A".$i,"TANGGAL LAHIR");
        $sheet->setCellValue("B".$i, $fix[0]["tgl_lahir_pasien"]);
        $i++;
        $sheet->setCellValue("A".$i,"ALAMAT");
        $sheet->setCellValue("B".$i, $fix[0]["alamat_pasien"]);
        $i += 2;
        
        $sheet->setCellValue("A".$i,"TANGGAL MASUK");
        $sheet->setCellValue("B".$i, $fix[0]["tanggal_masuk"]);
        $i++;
        $sheet->setCellValue("A".$i,"TANGGAL ASESMEN");
        $sheet->setCellValue("B".$i, $fix[0]["tgl_asesmen"]);
        $i++;
        $sheet->setCellValue("A".$i,"JAM ASESMEN");
        $sheet->setCellValue("B".$i, $fix[0]["jam_asesmen"]);
        $i += 2;
        
        $sheet->setCellValue("A".$i,"ANAK KE");
        $sheet->setCellValue("B".$i, $fix[0]["anak_ke"]);
        $i++;
        $sheet->setCellValue("A".$i,"DARI");
        $sheet->setCellValue("B".$i, $fix[0]["dari_saudara"]);
        $i++;
        $sheet->setCellValue("A".$i,"STATUS PERNIKAHAN");
        $sheet->setCellValue("B".$i, $fix[0]["status_nikah"]);
        $i++;
        $sheet->setCellValue("A".$i,"PENDIDIKAN TERAKHIR");
        $sheet->setCellValue("B".$i, $fix[0]["pend_terakhir"]);
        $i += 2;
        
        $sheet->setCellValue("A".$i,"1. OBSERVASI");
        $sheet->getStyle("A".$i)->getFont()->setUnderline(true);
        $sheet->setCellValue("B".$i, $fix[0]["obeservasi"]);
        $i++;
        $sheet->setCellValue("A".$i,"2. WAWANCARA");
        $sheet->getStyle("A".$i)->getFont()->setUnderline(true);
        $sheet->setCellValue("B".$i, $fix[0]["wawancara"]);
        $i++;
        $sheet->setCellValue("A".$i,"Keterangan");
        $sheet->setCellValue("B".$i, $fix[0]["wawancara_ket"]);
        $i++;
        $sheet->setCellValue("A".$i,"- Keluhan Utama");
        $sheet->setCellValue("B".$i, $fix[0]["keluhan_utama"]);
        $i++;
        $sheet->setCellValue("A".$i,"- Kapan Mulai Terjadi");
        $sheet->setCellValue("B".$i, $fix[0]["mulai_terjadi"]);
        $i++;
        $sheet->setCellValue("A".$i,"- Riwayat Penyakit");
        $sheet->setCellValue("B".$i, $fix[0]["riwayat_penyakit"]);
        $i++;
        $sheet->setCellValue("A".$i,"- Keluhan Psikosomatis");
        $sheet->setCellValue("B".$i, $fix[0]["keluhan_psikosomatis"]);
        $i += 2;
        
        $sheet->setCellValue("A".$i,"3. PREDISPOSISI FAKTOR");
        $sheet->getStyle("A".$i)->getFont()->setUnderline(true);
        $i++;
        $sheet->setCellValue("A".$i,"- Penyebab/Pencetus");
        $sheet->setCellValue("B".$i, $fix[0]["penyebab"]);
        $i++;
        $sheet->setCellValue("A".$i,"- Faktor Keluarga");
        $sheet->setCellValue("B".$i, $fix[0]["faktor_keluarga"]);
        $i++;
        $sheet->setCellValue("A".$i,"- Faktor Pekerjaan/Sosial");
        $sheet->setCellValue("B".$i, $fix[0]["faktor_pekerjaan"]);
        $i++;
        $sheet->setCellValue("A".$i,"- Faktor Premorbid");
        $sheet->setCellValue("B".$i, $fix[0]["faktor_premorboid"]);
        $i++;
        $sheet->setCellValue("A".$i,"- Riwayat NAPZA");
        $sheet->setCellValue("B".$i, $fix[0]["riwayat_napza"]);
        $i++;
        $sheet->setCellValue("A".$i,"Lama pemakaian ");
        $sheet->setCellValue("B".$i, $fix[0]["napza_lama_pemakaian"]);
        $i++;
        $sheet->setCellValue("A".$i,"Jenis Zat ");
        $sheet->setCellValue("B".$i, $fix[0]["napza_jenis_zat"]);
        $i++;
        $sheet->setCellValue("A".$i,"Cara Pemakaian ");
        $sheet->setCellValue("B".$i, $fix[0]["napza_cara_pakai"]);
        $i++;
        $sheet->setCellValue("A".$i,"Latar belakang Pemakaian ");
        $sheet->setCellValue("B".$i, $fix[0]["napza_latarbelakang_pakai"]);
        $i += 2;
        
        $sheet->setCellValue("A".$i,"STATUS PSIKOLOGI");
        $sheet->getStyle("A".$i)->getFont()->setUnderline(true);
        $i++;
        $sheet->setCellValue("A".$i,"- Kesan Umum");
        $sheet->setCellValue("B".$i, $fix[0]["kesan_umum"]);
        $i++;
        $sheet->setCellValue("A".$i,"- Kesadaran");
        $sheet->setCellValue("B".$i, $fix[0]["kesadaran"]);
        $i++;
        $sheet->setCellValue("A".$i,"- Mood/Afek/Ekspresi");
        $sheet->setCellValue("B".$i, $fix[0]["mood"]);
        $i++;
        $sheet->setCellValue("A".$i,"- Proses dan Isi Pikiran");
        $sheet->setCellValue("B".$i, $fix[0]["proses_dan_isi_pikiran"]);
        $i++;
        $sheet->setCellValue("A".$i,"- Kecerdasan");
        $sheet->setCellValue("B".$i, $fix[0]["kecerdasan"]);
        $i++;
        $sheet->setCellValue("A".$i,"- Dorongan (Insting, Drive, Motivasi, Semangat)");
        $sheet->setCellValue("B".$i, $fix[0]["dorongan"]);
        $i++;
        $sheet->setCellValue("A".$i,"- Judgment (Pemahaman)");
        $sheet->setCellValue("B".$i, $fix[0]["judgment"]);
        $i++;
        $sheet->setCellValue("A".$i,"- Attention (Perhatian)");
        $sheet->setCellValue("B".$i, $fix[0]["attention"]);
        $i++;
        $sheet->setCellValue("A".$i,"- Psikomotorik");
        $sheet->setCellValue("B".$i, $fix[0]["psikomotorik"]);
        $i++;
        $sheet->setCellValue("A".$i,"- Orientasi Diri");
        $sheet->setCellValue("B".$i, $fix[0]["orientasi_diri"]);
        $i++;
        $sheet->setCellValue("A".$i,"- Orientasi Waktu");
        $sheet->setCellValue("B".$i, $fix[0]["orientasi_wkt"]);
        $i++;
        $sheet->setCellValue("A".$i,"- Orientasi Tempat");
        $sheet->setCellValue("B".$i, $fix[0]["orientasi_tempat"]);
        $i++;
        $sheet->setCellValue("A".$i,"- Orientasi Ruang");
        $sheet->setCellValue("B".$i, $fix[0]["orientasi_ruang"]);
        $i += 2;
        
        $sheet->setCellValue("A".$i,"STATUS ANTROPOMETRIS");
        $sheet->getStyle("A".$i)->getFont()->setUnderline(true);
        $i++;
        $sheet->setCellValue("A".$i,"- Berat Badan");
        $sheet->setCellValue("B".$i, $fix[0]["bb"]." kg");
        $i++;
        $sheet->setCellValue("A".$i,"- Tinggi Badan");
        $sheet->setCellValue("B".$i, $fix[0]["tb"]." cm");
        $i++;
        $sheet->setCellValue("A".$i,"- LILA");
        $sheet->setCellValue("B".$i, $fix[0]["lila"]);
        $i++;
        $sheet->setCellValue("A".$i,"- LK");
        $sheet->setCellValue("B".$i, $fix[0]["lk"]);
        $i++;
        $sheet->setCellValue("A".$i,"- Status Gizi");
        $sheet->setCellValue("B".$i, $fix[0]["status_gizi"]);
        $i += 2;
        
        $sheet->setCellValue("A".$i,"PEMERIKSAAN PSIKOLOGI");
        $sheet->getStyle("A".$i)->getFont()->setUnderline(true);
        $i++;
        $sheet->setCellValue("A".$i,"- Tes IQ");
        $sheet->setCellValue("B".$i, $fix[0]["tes_iq"]);
        $i++;
        $sheet->setCellValue("A".$i,"- Tes Bakat/Minat");
        $sheet->setCellValue("B".$i, $fix[0]["tes_bakat"]);
        $i++;
        $sheet->setCellValue("A".$i,"- Tes Kepribadian");
        $sheet->setCellValue("B".$i, $fix[0]["tes_kepribadian"]);
        $i++;
        $sheet->setCellValue("A".$i,"- Tes tes_okupasi");
        $sheet->setCellValue("B".$i, $fix[0]["kesan_umum"]);
        $i++;
        $sheet->setCellValue("A".$i,"- Tes Perkembangan");
        $sheet->setCellValue("B".$i, $fix[0]["tes_perkembangan"]);
        $i += 2;
        
        $sheet->setCellValue("A".$i,"OBSERVASI/WAWANCARA");
        $sheet->getStyle("A".$i)->getFont()->setUnderline(true);
        $i++;
        $sheet->setCellValue("A".$i,"1. Perkembangan Fisik");
        $sheet->setCellValue("B".$i, $fix[0]["perkembangan_fisik"]);
        $i++;
        $sheet->setCellValue("A".$i,"2. Perkembangan Kognitif");
        $sheet->setCellValue("B".$i, $fix[0]["perkembangan_kognitif"]);
        $i++;
        $sheet->setCellValue("A".$i,"3. Perkembangan Emosi");
        $sheet->setCellValue("B".$i, $fix[0]["perkembangan_emosi"]);
        $i++;
        $sheet->setCellValue("A".$i,"4. Perkembangan Sosial");
        $sheet->setCellValue("B".$i, $fix[0]["perkembangan_sosial"]);
        $i++;
        $sheet->setCellValue("A".$i,"5. Perkembangan Motorik");
        $sheet->setCellValue("B".$i, $fix[0]["perkembangan_motorik"]);
        $i += 2;
        
        $sheet->setCellValue("A".$i,"OBSERVASI/WAWANCARA");
        $sheet->getStyle("A".$i)->getFont()->setUnderline(true);
        $i++;
        $sheet->setCellValue("A".$i,"1. Raport");
        $sheet->setCellValue("B".$i, $fix[0]["report"]);
        $i++;
        $sheet->setCellValue("A".$i,"2. Ekspresi");
        $sheet->setCellValue("B".$i, $fix[0]["ekspresi"]);
        $i++;
        $sheet->setCellValue("A".$i,"3. Reaksi");
        $sheet->setCellValue("B".$i, $fix[0]["reaksi"]);
        $i++;
        $sheet->setCellValue("A".$i,"4. Komunikasi");
        $sheet->setCellValue("B".$i, $fix[0]["komunikasi"]);
        $i++;
        $sheet->setCellValue("A".$i,"5. Lain-Lain");
        $sheet->setCellValue("B".$i, $fix[0]["lain_lain"]);
        $i += 2;
        
        $sheet->setCellValue("A".$i,"DISPOSISI");
        $sheet->getStyle("A".$i)->getFont()->setUnderline(true);
        $i++;
        $sheet->setCellValue("A".$i,"Tanggal Pulang");
        $sheet->setCellValue("B".$i, $fix[0]["tgl_pulang"]);
        $i++;
        $sheet->setCellValue("A".$i,"Jam Pulang");
        $sheet->setCellValue("B".$i, $fix[0]["jam_pulang"]);
        $i++;
        $sheet->setCellValue("A".$i,"Tanggal Kontrol Klinik");
        $sheet->setCellValue("B".$i, $fix[0]["tgl_kontrol_klinik"]);
        $i++;
        $sheet->setCellValue("A".$i,"Dirawat Di Ruang");
        $sheet->setCellValue("B".$i, $fix[0]["dirawat_diruang"]);
        
        foreach(range('A','B') as $columnID) {
            $sheet->getColumnDimension($columnID)->setAutoSize(true);
        }
        $sheet->getStyle('A1:A100')->getFont()->setBold(true);
        $sheet->getStyle('B1:B100')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); 
        
        
        
        header ( 'Content-Type: application/vnd.ms-excel' );
        header ( 'Content-Disposition: attachment;filename="LaporanAsesmenPsikologi.xls"' );
        header ( 'Cache-Control: max-age=0' );
        $writer = PHPExcel_IOFactory::createWriter ( $file, 'Excel5' );
        $writer->save ( 'php://output' );
		return;
    }
}

?>