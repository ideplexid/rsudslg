<?php

class LapDiagnosaPerBulanResponder extends DBResponder {
    public function excel() {
        $this->dbtable->setFetchMethode(DBTable::$ARRAY_FETCH);
        $data=$this->dbtable->view("",0);
        $list = $data['data'];
        /*echo "<pre>";
        var_dump($list);
        echo "</pre>";*/
        require_once ("smis-libs-out/php-excel/PHPExcel.php");
        $file = new PHPExcel ();
        $file->getProperties ()->setCreator ( "SIMRS" );
        $file->getProperties ()->setLastModifiedBy ( "SIMRS Administrator" );
        $file->getProperties ()->setTitle ( "Laporan Diagnosa Per Bulan" );
        $file->getProperties ()->setSubject ( "Laporan Diagnosa Per Bulan" );
        $file->getProperties ()->setDescription ( "Laporan Diagnosa Per Bulan Generated From system" );
        $file->getProperties ()->setKeywords ( "Laporan Diagnosa Per Bulan" );
        $file->getProperties ()->setCategory ( "Laporan Diagnosa Per Bulan" );
        $sheet = $file->getActiveSheet ();
        
        $no = 1;
        $sheet->setCellValue("A".$no,"No.");
        $sheet->setCellValue("B".$no,"Tahun");
        $sheet->setCellValue("C".$no,"Bulan");    
        $sheet->setCellValue("D".$no,"Kode ICD");    
        $sheet->setCellValue("E".$no,"Nama ICD");
        $sheet->setCellValue("F".$no,"Jumlah");
        $sheet->getStyle('A'.$no.':'.'F'.$no)->getFont()->setBold(true);
        $sheet->getStyle('A'.$no.':'.'F'.$no)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        
        $i=0;
        foreach($list as $x) {
            $no++;
            $i++;
            $sheet->setCellValue("A".$no,$i);
            $sheet->setCellValue("B".$no,$x["tahun"]);
            $sheet->setCellValue("C".$no,$x["bulan"]);    
            $sheet->setCellValue("D".$no,$x["kode_icd"]);    
            $sheet->setCellValue("E".$no,$x["nama_icd"]);
            $sheet->setCellValue("F".$no,$x["jumlah"]);
            $sheet->getStyle('A'.$no.':'.'D'.$no)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $sheet->getStyle('F'.$no)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        }
        foreach(range('A','F') as $columnID) {
            $sheet->getColumnDimension($columnID)->setAutoSize(true);
        }
        
        header ( 'Content-Type: application/vnd.ms-excel' );
        header ( 'Content-Disposition: attachment;filename="Laporan Diagnosa Per Bulan.xls"' );
        header ( 'Cache-Control: max-age=0' );
        $writer = PHPExcel_IOFactory::createWriter ( $file, 'Excel5' );
        $writer->save ( 'php://output' );
		return;
    }
}

?>