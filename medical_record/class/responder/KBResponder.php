<?php 

class KBResponder extends DBResponder{
    
    public function excel(){
        /*@var User*/
		global $user;
        require_once "smis-libs-out/php-excel/PHPExcel.php";
        loadLibrary("smis-libs-function-excel");
        $this->dbtable->setFetchMethode(DBTable::$OBJECT_FETCH);        
        $this->dbtable->setShowAll(true);
        $data=$this->dbtable->view("",0);
        $list=$data['data'];
        
        $file = new PHPExcel ();
        $file->getProperties ()->setCreator ( $user->getNameOnly() );
        $file->getProperties ()->setLastModifiedBy ( $user->getNameOnly() );
        
        $sheet = $file->getActiveSheet ();
        $kolom=PHPExcel_Cell::stringFromColumnIndex();
        $sheet->mergeCells ( 'A1:I1' )->setCellValue ( "A1", "LAPORAN KB" );
        $sheet->mergeCells ( 'A2:C2' )->setCellValue ( "A2", "DARI" );
        $sheet->setCellValue ( "D2", ArrayAdapter::format("date d M Y",$_POST['dari']) );
        $sheet->mergeCells ( 'E2:H2' )->setCellValue ( "D2", "SAMPAI" );
        $sheet->setCellValue ( "I2", ArrayAdapter::format("date d M Y",$_POST['sampai']) );
        
        $sheet->setCellValue ( "A3","No."  );
        $sheet->setCellValue ( "B3","Nama"  );
        $sheet->setCellValue ( "C3","No BPJS"  );
        $sheet->setCellValue ( "D3","Alamat"  );
        $sheet->setCellValue ( "E3","No Telp"  );
        $sheet->setCellValue ( "F3","Tanggal" );
        $sheet->setCellValue ( "G3","Dokter"  );
        $sheet->setCellValue ( "H3","Perawat"  );
        $sheet->setCellValue ( "I3","Jenis"  );
        $sheet->getStyle ( 'A1:I3')->getFont ()->setBold ( true );
        
        $no=0;
        $start=4;
        foreach($list as $x){
            $no++;
            $sheet->setCellValue ( "A".$start,$no."."  );
            $sheet->setCellValue ( "B".$start,$x->nama_pasien  );
            $sheet->setCellValue ( "C".$start,$x->no_bpjs  );
            $sheet->setCellValue ( "D".$start,$x->alamat );
            $sheet->setCellValue ( "E".$start,$x->no_telp );
            $sheet->setCellValue ( "F".$start,ArrayAdapter::format("date d M Y",$x->tanggal) );
            $sheet->setCellValue ( "G".$start,$x->nama_dokter );
            $sheet->setCellValue ( "H".$start,$x->nama_perawat );
            $sheet->setCellValue ( "I".$start,$x->metode );
            $start++;
        }
        
        $fillcabang['borders']=array();
        $fillcabang['borders']['allborders']=array();
        $fillcabang['borders']['allborders']['style']=PHPExcel_Style_Border::BORDER_THIN ;
        $sheet->getStyle ( "A1:I".$start )->applyFromArray ( $fillcabang );
        
        header ( 'Content-Type: application/vnd.ms-excel' );
        header ( 'Content-Disposition: attachment;filename="Lap KB.xls"' );
        header ( 'Cache-Control: max-age=0' );
        $writer = PHPExcel_IOFactory::createWriter ( $file, 'Excel5' );
        $writer->save ( 'php://output' );
        return null;
    }
    
}

?>