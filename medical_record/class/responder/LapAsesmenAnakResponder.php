<?php 

class LapAsesmenAnakResponder extends DBResponder {
    public function excel() {
        $this->dbtable->addCustomKriteria("id", " = '".$_POST['id']."'");
        $data=$this->dbtable->view("",0);
        $fix=$this->adapter->getContent($data['data']);
                
        require_once ("smis-libs-out/php-excel/PHPExcel.php");
        $file = new PHPExcel ();
        $file->getProperties ()->setCreator ( "Rumah Sakit Syamrabu" );
        $file->getProperties ()->setLastModifiedBy ( "Rumah Sakit Bagian Administrator" );
        $file->getProperties ()->setTitle ( "Laporan Asesmen Anak" );
        $file->getProperties ()->setSubject ( "Laporan Asesmen Anak" );
        $file->getProperties ()->setDescription ( "Laporan Asesmen Anak Generated From system" );
        $file->getProperties ()->setKeywords ( "Laporan Asesmen Anak" );
        $file->getProperties ()->setCategory ( "Laporan Asesmen Anak" );
        $sheet = $file->getActiveSheet ();
        
        $i = 1;
        $sheet->setCellValue("A".$i,"ASESMEN AWAL MEDIS - POLI ANAK");
        $sheet->mergeCells("A".$i.":C".$i);
        $i += 2;
        
        $sheet->setCellValue("A".$i,"NAMA");
        $sheet->setCellValue("B".$i, $fix[0]["nama_pasien"]);
        $i++;
        $sheet->setCellValue("A".$i,"JENIS KELAMIN");
        $sheet->setCellValue("B".$i, $fix[0]["jk"]);
        $i++;
        $sheet->setCellValue("A".$i,"NO. RM");
        $sheet->setCellValue("B".$i, $fix[0]["NRM"]);
        $i++;
        $sheet->setCellValue("A".$i,"TANGGAL LAHIR");
        $sheet->setCellValue("B".$i, $fix[0]["tgl_lahir_pasien"]);
        $i++;
        $sheet->setCellValue("A".$i,"ALAMAT");
        $sheet->setCellValue("B".$i, $fix[0]["alamat_pasien"]);
        $i += 2;
        
        $sheet->setCellValue("A".$i,"TANGGAL MASUK");
        $sheet->setCellValue("B".$i, $fix[0]["tanggal_masuk"]);
        $i++;
        $sheet->setCellValue("A".$i,"TANGGAL ASESMEN");
        $sheet->setCellValue("B".$i, $fix[0]["tgl_asesmen"]);
        $i++;
        $sheet->setCellValue("A".$i,"JAM ASESMEN");
        $sheet->setCellValue("B".$i, $fix[0]["jam_asesmen"]);
        $i += 2;
        
        $sheet->setCellValue("A".$i,"Berat Badan");
        $sheet->setCellValue("B".$i, $fix[0]["bb"]." kg");
        $i++;
        $sheet->setCellValue("A".$i,"Tinggi Badan");
        $sheet->setCellValue("B".$i, $fix[0]["tb"]." cm");
        $i++;
        $sheet->setCellValue("A".$i,"LILA");
        $sheet->setCellValue("B".$i, $fix[0]["lila"]);
        $i++;
        $sheet->setCellValue("A".$i,"LK");
        $sheet->setCellValue("B".$i, $fix[0]["lk"]);
        $i += 2;
        
        $sheet->setCellValue("A".$i,"ANAMNESES");
        $sheet->getStyle("A".$i)->getFont()->setUnderline(true);
        $i++;
        $sheet->setCellValue("A".$i,"1. Keluhan Utama");
        $sheet->setCellValue("B".$i, $fix[0]["anamneses_utama"]);
        $i++;
        $sheet->setCellValue("A".$i,"2. Riwayat Penyakit Sekarang");
        $sheet->setCellValue("B".$i, $fix[0]["anamneses_penyakit_skrg"]);
        $i++;
        $sheet->setCellValue("A".$i,"3. Riwayat Pengobatan");
        $sheet->setCellValue("B".$i, $fix[0]["anamneses_pengobatan"]);
        $i += 2;
        
        $sheet->setCellValue("A".$i,"PEMERIKSAAN FISIK");
        $sheet->getStyle("A".$i)->getFont()->setUnderline(true);
        $i++;
        $sheet->setCellValue("A".$i,"- Mata");
        $i++;
        $sheet->setCellValue("A".$i,"Anemia");
        $sheet->setCellValue("B".$i, $fix[0]["mata_anemia"]);
        $i++;
        $sheet->setCellValue("A".$i,"Ikterus");
        $sheet->setCellValue("B".$i, $fix[0]["mata_ikterus"]);
        $i++;
        $sheet->setCellValue("A".$i,"Reflek Pupil");
        $sheet->setCellValue("B".$i, $fix[0]["mata_reflek_pupil"]);
        $i++;
        $sheet->setCellValue("A".$i,"Reflek Pupil");
        $sheet->setCellValue("B".$i, $fix[0]["mata_reflek_pupil"]);
        $i++;
        $sheet->setCellValue("A".$i,"Edema Palbebrae");
        $sheet->setCellValue("B".$i, $fix[0]["mata_edema_palbebrae"]);
        $i++;
        $sheet->setCellValue("A".$i,"- THT");
        $i++;
        $sheet->setCellValue("A".$i,"Tonsil");
        $sheet->setCellValue("B".$i, $fix[0]["tht_tonsil"]);
        $i++;
        $sheet->setCellValue("A".$i,"Faring");
        $sheet->setCellValue("B".$i, $fix[0]["tht_faring"]);
        $i++;
        $sheet->setCellValue("A".$i,"Lidah");
        $sheet->setCellValue("B".$i, $fix[0]["tht_lidah"]);
        $i++;
        $sheet->setCellValue("A".$i,"Bibir");
        $sheet->setCellValue("B".$i, $fix[0]["tht_bibir"]);
        $i++;
        $sheet->setCellValue("A".$i,"- Leher");
        $i++;
        $sheet->setCellValue("A".$i,"JVP");
        $sheet->setCellValue("B".$i, $fix[0]["leher_jvp"]);
        $i++;
        $sheet->setCellValue("A".$i,"Pembesaran Kelenjar");
        $sheet->setCellValue("B".$i, $fix[0]["leher_pembesaran_kelenjar"]);
        $i++;
        $sheet->setCellValue("A".$i,"Kaku Kuduk");
        $sheet->setCellValue("B".$i, $fix[0]["kaku_kuduk"]);
        $i++;
        $sheet->setCellValue("A".$i,"- Torak");
        $i++;
        $sheet->setCellValue("A".$i,"Simetris");
        $sheet->setCellValue("B".$i, $fix[0]["torak_simetris"]);
        $i++;
        $sheet->setCellValue("A".$i,"Asimetris");
        $sheet->setCellValue("B".$i, $fix[0]["torak_asimetris"]);
        $i++;
        $sheet->setCellValue("A".$i,"- Jantung");
        $i++;
        $sheet->setCellValue("A".$i,"S1/S2");
        $sheet->setCellValue("B".$i, $fix[0]["jantung_s1_s2"]);
        $i++;
        $sheet->setCellValue("A".$i,"Reguler");
        $sheet->setCellValue("B".$i, $fix[0]["jantung_reguler"]);
        $i++;
        $sheet->setCellValue("A".$i,"Ireguler");
        $sheet->setCellValue("B".$i, $fix[0]["jantung_ireguler"]);
        $i++;
        $sheet->setCellValue("A".$i,"Murmur");
        $sheet->setCellValue("B".$i, $fix[0]["jantung_murmur"]);
        $i++;
        $sheet->setCellValue("A".$i,"Lain-Lain");
        $sheet->setCellValue("B".$i, $fix[0]["jantung_lain"]);
        $i++;
        $sheet->setCellValue("A".$i,"- Paru-Paru");
        $i++;
        $sheet->setCellValue("A".$i,"Suara Nafas");
        $sheet->setCellValue("B".$i, $fix[0]["paru_suara_nafas"]);
        $i++;
        $sheet->setCellValue("A".$i,"Ronki");
        $sheet->setCellValue("B".$i, $fix[0]["paru_ronki"]);
        $i++;
        $sheet->setCellValue("A".$i,"Wheezing");
        $sheet->setCellValue("B".$i, $fix[0]["paru_wheezing"]);
        $i++;
        $sheet->setCellValue("A".$i,"Lain-Lain");
        $sheet->setCellValue("B".$i, $fix[0]["paru_lain"]);
        $i++;
        $sheet->setCellValue("A".$i,"- Abdomen");
        $i++;
        $sheet->setCellValue("A".$i,"Distensi");
        $sheet->setCellValue("B".$i, $fix[0]["abdomen_distensi"]);
        $i++;
        $sheet->setCellValue("A".$i,"Meteorismus");
        $sheet->setCellValue("B".$i, $fix[0]["abdomen_meteorismus"]);
        $i++;
        $sheet->setCellValue("A".$i,"Peristaltik");
        $sheet->setCellValue("B".$i, $fix[0]["abdomen_peristaltik"]);
        $i++;
        $sheet->setCellValue("A".$i,"Arcites");
        $sheet->setCellValue("B".$i, $fix[0]["abdomen_arcites"]);
        $i++;
        $sheet->setCellValue("A".$i,"Nyeri Tekan");
        $sheet->setCellValue("B".$i, $fix[0]["abdomen_nyeri"]);
        $i++;
        $sheet->setCellValue("A".$i,"Lokasi");
        $sheet->setCellValue("B".$i, $fix[0]["abdomen_nyeri_lokasi"]);
        $i++;
        $sheet->setCellValue("A".$i,"- Hepar");
        $sheet->setCellValue("B".$i, $fix[0]["hepar"]);
        $i++;
        $sheet->setCellValue("A".$i,"- Lien");
        $sheet->setCellValue("B".$i, $fix[0]["lien"]);
        $i++;
        $sheet->setCellValue("A".$i,"- Status Gizi");
        $sheet->setCellValue("B".$i, $fix[0]["status_gizi"]);
        $i++;
        $sheet->setCellValue("A".$i,"- Extremitas");
        $i++;
        $sheet->setCellValue("A".$i,"Hangat");
        $sheet->setCellValue("B".$i, $fix[0]["extremitas_hangat"]);
        $i++;
        $sheet->setCellValue("A".$i,"Dingin");
        $sheet->setCellValue("B".$i, $fix[0]["extremitas_dingin"]);
        $i++;
        $sheet->setCellValue("A".$i,"Edema");
        $sheet->setCellValue("B".$i, $fix[0]["extremitas_edema"]);
        $i++;
        $sheet->setCellValue("A".$i,"Lain-Lain");
        $sheet->setCellValue("B".$i, $fix[0]["extremitas_lain"]);
        $i += 2;
        
        $sheet->setCellValue("A".$i,"RENCANA KERJA");
        $sheet->getStyle("A".$i)->getFont()->setUnderline(true);
        $sheet->setCellValue("B".$i, $fix[0]["rencana_kerja"]);
        $i += 2;
        
        $sheet->setCellValue("A".$i,"HASIL PEMERIKSAAN PENUNJANG");
        $sheet->getStyle("A".$i)->getFont()->setUnderline(true);
        $sheet->setCellValue("B".$i, $fix[0]["hasil_periksa_penunjang"]);
        $i += 2;
        
        $sheet->setCellValue("A".$i,"DIAGNOSIS");
        $sheet->getStyle("A".$i)->getFont()->setUnderline(true);
        $sheet->setCellValue("B".$i, $fix[0]["diagnosis"]);
        $i += 2;
        
        $sheet->setCellValue("A".$i,"TERAPI");
        $sheet->getStyle("A".$i)->getFont()->setUnderline(true);
        $sheet->setCellValue("B".$i, $fix[0]["terapi"]);
        $i += 2;
        
        $sheet->setCellValue("A".$i,"HASIL PEMBEDAHAN");
        $sheet->getStyle("A".$i)->getFont()->setUnderline(true);
        $sheet->setCellValue("B".$i, $fix[0]["hasil_pembedahan"]);
        $i += 2;
        
        $sheet->setCellValue("A".$i,"REKOMENDASI (SARAN)");
        $sheet->getStyle("A".$i)->getFont()->setUnderline(true);
        $sheet->setCellValue("B".$i, $fix[0]["rekomendasi"]);
        $i += 2;
        
        $sheet->setCellValue("A".$i,"CATATAN PENTING");
        $sheet->getStyle("A".$i)->getFont()->setUnderline(true);
        $sheet->setCellValue("B".$i, $fix[0]["catatan"]);
        $i += 2;
        
        $sheet->setCellValue("A".$i,"DISPOSISI");
        $sheet->getStyle("A".$i)->getFont()->setUnderline(true);
        $i++;
        $sheet->setCellValue("A".$i,"Tanggal Pulang");
        $sheet->setCellValue("B".$i, $fix[0]["tgl_pulang"]);
        $i++;
        $sheet->setCellValue("A".$i,"Jam Pulang");
        $sheet->setCellValue("B".$i, $fix[0]["jam_pulang"]);
        $i++;
        $sheet->setCellValue("A".$i,"Tanggal Kontrol Klinik");
        $sheet->setCellValue("B".$i, $fix[0]["tgl_kontrol_klinik"]);
        $i++;
        $sheet->setCellValue("A".$i,"Dirawat Di Ruang");
        $sheet->setCellValue("B".$i, $fix[0]["dirawat_diruang"]);
        
        foreach(range('A','B') as $columnID) {
            $sheet->getColumnDimension($columnID)->setAutoSize(true);
        }
        $sheet->getStyle('A1:A100')->getFont()->setBold(true);
        $sheet->getStyle('B1:B100')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); 
        
        header ( 'Content-Type: application/vnd.ms-excel' );
        header ( 'Content-Disposition: attachment;filename="LaporanAsesmenAnak.xls"' );
        header ( 'Cache-Control: max-age=0' );
        $writer = PHPExcel_IOFactory::createWriter ( $file, 'Excel5' );
        $writer->save ( 'php://output' );
		return;
        
    }
}

?>