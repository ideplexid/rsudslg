<?php

//require_once 'smis-framework/smis/database/DBReport.php';

class RL54Responder extends DBReport {
    public function excel() {
        $this->dbtable->setShowAll(true);
        $data = $this->dbtable->view("",0);
        $fix = $this->adapter->getContent($data['data']);
        
        /*echo "<pre>";
        var_dump($data);
        echo "<pre>";*/
        
        require_once ("smis-libs-out/php-excel/PHPExcel.php");
        $file = new PHPExcel ();
        $file->getProperties ()->setCreator ( "PT. Inovasi Ide Utama" );
        $file->getProperties ()->setLastModifiedBy ( "Operator" );
        $file->getProperties ()->setTitle ( "Laporan RL 5.4 Penyakit Baru Rawat Jalan" );
        $file->getProperties ()->setSubject ( "Laporan RL 5.4 Penyakit Baru Rawat Jalan" );
        $file->getProperties ()->setDescription ( "Laporan RL 5.4 Penyakit Baru Rawat Jalan Generated From system" );
        $file->getProperties ()->setKeywords ( "Laporan RL 5.4 Penyakit Baru Rawat Jalan" );
        $file->getProperties ()->setCategory ( "Laporan RL 5.4 Penyakit Baru Rawat Jalan" );
        $sheet = $file->getActiveSheet ();
        
        $no = 1;
        $sheet->setCellValue("A".$no,"No.");
        $a = $no + 2;
        $sheet->mergeCells("A".$no.":A".$a);
        $sheet->setCellValue("B".$no,"Golongan Sebab-Sebab Sakit");
        $sheet->mergeCells("B".$no.":B".$a);
        $sheet->setCellValue("C".$no,"Kode");
        $sheet->mergeCells("C".$no.":C".$a);    
        $sheet->setCellValue("D".$no,"Ruangan");
        $sheet->mergeCells("D".$no.":D".$a);
        $sheet->setCellValue("E".$no,"Kasus Baru Menurut Golongan Umur");
        $sheet->mergeCells("E".$no.":T".$no);    
        $sheet->setCellValue("U".$no,"Jumlah Kasus");
        $sheet->mergeCells("U".$no.":AA".$no);
        $b = $no + 1;
        $sheet->setCellValue("AB".$no,"Pasien Baru");
        $sheet->mergeCells("AB".$no.":AD".$no);
        $sheet->mergeCells("AB".$no.":AD".$b);
        $sheet->setCellValue("AE".$no,"Pasien Lama");
        $sheet->mergeCells("AE".$no.":AG".$no);
        $sheet->mergeCells("AE".$no.":AE".$b);
        $sheet->setCellValue("AH".$no,"Jumlah Kunjungan");
        $sheet->mergeCells("AH".$no.":AH".$a);  
        $sheet->getStyle('A'.$no.':'.'AH'.$no)->getFont()->setBold(true);
        $sheet->getStyle('A'.$no.':'.'AH'.$no)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('A'.$no.':'.'AH'.$no)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $no++;
        
        $sheet->setCellValue("E".$no,"0 - 28 HR");
        $sheet->mergeCells("E".$no.":F".$no);
        $sheet->setCellValue("G".$no,"28 HR - 1 TH");
        $sheet->mergeCells("G".$no.":H".$no);
        $sheet->setCellValue("I".$no,"1 - 4 TH");
        $sheet->mergeCells("I".$no.":J".$no);
        $sheet->setCellValue("K".$no,"5 - 14 TH");
        $sheet->mergeCells("K".$no.":L".$no);
        $sheet->setCellValue("M".$no,"15 - 24 TH");
        $sheet->mergeCells("M".$no.":N".$no);
        $sheet->setCellValue("O".$no,"25 - 44 TH");
        $sheet->mergeCells("O".$no.":P".$no);
        $sheet->setCellValue("Q".$no,"45 - 64 TH");
        $sheet->mergeCells("Q".$no.":R".$no);
        $sheet->setCellValue("S".$no,"45 - 64 TH");
        $sheet->mergeCells("S".$no.":T".$no);
        $sheet->setCellValue("U".$no,"Baru");
        $sheet->mergeCells("U".$no.":W".$no);
        $sheet->setCellValue("X".$no,"Lama");
        $sheet->mergeCells("X".$no.":Z".$no);
        $a = $no + 1;
        $sheet->setCellValue("AA".$no,"Jumlah");
        $sheet->mergeCells("AA".$no.":AA".$a);
        $sheet->getStyle('A'.$no.':'.'AH'.$no)->getFont()->setBold(true);
        $sheet->getStyle('A'.$no.':'.'AH'.$no)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('A'.$no.':'.'AH'.$no)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $no++;
        
        $sheet->setCellValue("E".$no,"Lk");
        $sheet->setCellValue("F".$no,"Pr");
        $sheet->setCellValue("G".$no,"Lk");
        $sheet->setCellValue("H".$no,"Pr");
        $sheet->setCellValue("I".$no,"Lk");
        $sheet->setCellValue("J".$no,"Pr");
        $sheet->setCellValue("K".$no,"Lk");
        $sheet->setCellValue("L".$no,"Pr");
        $sheet->setCellValue("M".$no,"Lk");
        $sheet->setCellValue("N".$no,"Pr");
        $sheet->setCellValue("O".$no,"Lk");
        $sheet->setCellValue("P".$no,"Pr");
        $sheet->setCellValue("Q".$no,"Lk");
        $sheet->setCellValue("R".$no,"Pr");
        $sheet->setCellValue("S".$no,"Lk");
        $sheet->setCellValue("T".$no,"Pr");
        $sheet->setCellValue("U".$no,"Lk");
        $sheet->setCellValue("V".$no,"Pr");
        $sheet->setCellValue("W".$no,"Jumlah");
        $sheet->setCellValue("X".$no,"Lk");
        $sheet->setCellValue("Y".$no,"Pr");
        $sheet->setCellValue("Z".$no,"Jumlah");
        $sheet->setCellValue("AA".$no,"Jumlah");
        $sheet->setCellValue("AB".$no,"Lk");
        $sheet->setCellValue("AC".$no,"Pr");
        $sheet->setCellValue("AD".$no,"Jumlah");
        $sheet->setCellValue("AE".$no,"Lk");
        $sheet->setCellValue("AF".$no,"Pr");
        $sheet->setCellValue("AG".$no,"Jumlah");
        $sheet->setCellValue("AH".$no,"Jumlah Kunjungan");
        $sheet->getStyle('A'.$no.':'.'AH'.$no)->getFont()->setBold(true);
        $sheet->getStyle('A'.$no.':'.'AH'.$no)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('A'.$no.':'.'AH'.$no)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $no++;
        
        $nomor = 1;
        $first_row = $no;
        foreach($fix as $x) {
            $sheet->setCellValue("A".$no, $nomor);
            $sheet->setCellValue("B".$no, $x["Sebab Sakit"]);
            $sheet->setCellValue("C".$no, $x["Kode"]);
            $sheet->setCellValue("D".$no, $x["Ruangan"]);
            $sheet->setCellValue("E".$no, $x["28hr Lk"]);
            $sheet->setCellValue("F".$no, $x["28hr Pr"]);
            $sheet->setCellValue("G".$no, $x["1th Lk"]);
            $sheet->setCellValue("H".$no, $x["1th Pr"]);
            $sheet->setCellValue("I".$no, $x["4th Lk"]);
            $sheet->setCellValue("J".$no, $x["4th Pr"]);
            $sheet->setCellValue("K".$no, $x["14th Lk"]);
            $sheet->setCellValue("L".$no, $x["14th Pr"]);
            $sheet->setCellValue("M".$no, $x["24th Lk"]);
            $sheet->setCellValue("N".$no, $x["24th Pr"]);
            $sheet->setCellValue("O".$no, $x["44th Lk"]);
            $sheet->setCellValue("P".$no, $x["44th Pr"]);
            $sheet->setCellValue("Q".$no, $x["65th Lk"]);
            $sheet->setCellValue("R".$no, $x["65th Pr"]);
            $sheet->setCellValue("S".$no, $x["l65th Lk"]);
            $sheet->setCellValue("T".$no, $x["l65th Pr"]);
            $sheet->setCellValue("U".$no, $x["Kasus Baru Lk"]);
            $sheet->setCellValue("V".$no, $x["Kasus Baru Pr"]);
            $sheet->setCellValue("W".$no, $x["Kasus Baru Jmlh"]);
            $sheet->setCellValue("X".$no, $x["Kasus Lama Lk"]);
            $sheet->setCellValue("Y".$no, $x["Kasus Lama Pr"]);
            $sheet->setCellValue("Z".$no, $x["Kasus Lama Jmlh"]);
            $sheet->setCellValue("AA".$no, $x["Kasus Jmlh"]);
            $sheet->setCellValue("AB".$no, $x["Pasien Baru Lk"]);
            $sheet->setCellValue("AC".$no, $x["Pasien Baru Pr"]);
            $sheet->setCellValue("AD".$no, $x["Jumlah Pasien Baru"]);
            $sheet->setCellValue("AE".$no, $x["Pasien Lama Lk"]);
            $sheet->setCellValue("AF".$no, $x["Pasien Lama Pr"]);
            $sheet->setCellValue("AG".$no, $x["Jumlah Pasien Lama"]);
            $sheet->setCellValue("AH".$no, $x["Jumlah Kunjungan"]);
            $sheet->getStyle('A'.$no)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $sheet->getStyle('B'.$no)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $sheet->getStyle('C'.$no.':'.'AH'.$no)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $sheet->getStyle('A'.$no)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $sheet->getStyle('B'.$no)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $sheet->getStyle('C'.$no.':'.'AH'.$no)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $nomor++;
            $no++;
        }
        $last_row = $no - 1;
        
        $sheet->setCellValue("A".$no,"JUMLAH");
        $sheet->mergeCells("A".$no.":D".$no);
        $sheet->setCellValue("E".$no,"=SUM(E".$first_row.":E".$last_row.")");
        $sheet->setCellValue("F".$no,"=SUM(F".$first_row.":F".$last_row.")");
        $sheet->setCellValue("G".$no,"=SUM(G".$first_row.":G".$last_row.")");
        $sheet->setCellValue("H".$no,"=SUM(H".$first_row.":H".$last_row.")");
        $sheet->setCellValue("I".$no,"=SUM(I".$first_row.":I".$last_row.")");
        $sheet->setCellValue("J".$no,"=SUM(J".$first_row.":J".$last_row.")");
        $sheet->setCellValue("K".$no,"=SUM(K".$first_row.":K".$last_row.")");
        $sheet->setCellValue("L".$no,"=SUM(L".$first_row.":L".$last_row.")");
        $sheet->setCellValue("M".$no,"=SUM(M".$first_row.":M".$last_row.")");
        $sheet->setCellValue("N".$no,"=SUM(N".$first_row.":N".$last_row.")");
        $sheet->setCellValue("O".$no,"=SUM(O".$first_row.":O".$last_row.")");
        $sheet->setCellValue("P".$no,"=SUM(P".$first_row.":P".$last_row.")");
        $sheet->setCellValue("Q".$no,"=SUM(Q".$first_row.":Q".$last_row.")");
        $sheet->setCellValue("R".$no,"=SUM(R".$first_row.":R".$last_row.")");
        $sheet->setCellValue("S".$no,"=SUM(S".$first_row.":S".$last_row.")");
        $sheet->setCellValue("T".$no,"=SUM(T".$first_row.":T".$last_row.")");
        $sheet->setCellValue("U".$no,"=SUM(U".$first_row.":U".$last_row.")");
        $sheet->setCellValue("V".$no,"=SUM(V".$first_row.":V".$last_row.")");
        $sheet->setCellValue("W".$no,"=SUM(W".$first_row.":W".$last_row.")");
        $sheet->setCellValue("X".$no,"=SUM(X".$first_row.":X".$last_row.")");
        $sheet->setCellValue("Y".$no,"=SUM(Y".$first_row.":Y".$last_row.")");
        $sheet->setCellValue("Z".$no,"=SUM(Z".$first_row.":Z".$last_row.")");
        $sheet->setCellValue("AA".$no,"=SUM(AA".$first_row.":AA".$last_row.")");
        $sheet->setCellValue("AB".$no,"=SUM(AB".$first_row.":AB".$last_row.")");
        $sheet->setCellValue("AC".$no,"=SUM(AC".$first_row.":AC".$last_row.")");
        $sheet->setCellValue("AD".$no,"=SUM(AD".$first_row.":AD".$last_row.")");
        $sheet->setCellValue("AE".$no,"=SUM(AE".$first_row.":AE".$last_row.")");
        $sheet->setCellValue("AF".$no,"=SUM(AF".$first_row.":AF".$last_row.")");
        $sheet->setCellValue("AG".$no,"=SUM(AG".$first_row.":AG".$last_row.")");
        $sheet->setCellValue("AH".$no,"=SUM(AH".$first_row.":AH".$last_row.")");
        $sheet->getStyle('A'.$no.':'.'AH'.$no)->getFont()->setBold(true);
        $sheet->getStyle('A'.$no.':'.'AH'.$no)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('A'.$no.':'.'AH'.$no)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        
        foreach(range('A','AH') as $columnID) {
            $sheet->getColumnDimension($columnID)->setAutoSize(true);
        }
        
        header ( 'Content-Type: application/vnd.ms-excel' );
        header ( 'Content-Disposition: attachment;filename="Laporan RL 5.4 Penyakit Baru Rawat Jalan.xls"' );
        header ( 'Cache-Control: max-age=0' );
        $writer = PHPExcel_IOFactory::createWriter ( $file, 'Excel5' );
        $writer->save ( 'php://output' );
		return;
    }
}

?>