<?php

class ProlanisResponder extends DBResponder{
    public function excel(){
        //$this->dbtable->setFetchMethode(DBTable::$ARRAY_FETCH);
        $data=$this->dbtable->view("",0);
        $fix=$this->adapter->getContent($data['data']);
        
        require_once ("smis-libs-out/php-excel/PHPExcel.php");
        $file = new PHPExcel ();
        $file->getProperties ()->setCreator ( "System" );
        $file->getProperties ()->setLastModifiedBy ( "Administrator" );
        $file->getProperties ()->setTitle ( "Laporan Prolanis" );
        $file->getProperties ()->setSubject ( "Laporan Prolanis" );
        $file->getProperties ()->setDescription ( "Laporan Prolanis Generated From system" );
        $file->getProperties ()->setKeywords ( "Laporan Prolanis" );
        $file->getProperties ()->setCategory ( "Laporan Prolanis" );
        $sheet = $file->getActiveSheet ();
        
        //Write Header
        $no = 1;
        $sheet->setCellValue("A".$no,"No.");
        $sheet->setCellValue("B".$no,"Tanggal");
        $sheet->setCellValue("C".$no,"Nama");
        $sheet->setCellValue("D".$no,"No. BPJS");
        $sheet->setCellValue("E".$no,"Alamat");
        $sheet->setCellValue("F".$no,"Telp");
        $sheet->setCellValue("G".$no,"Hasil Pemeriksaan");
        $sheet->mergeCells('G1:J1');
        $sheet->setCellValue("K".$no,"Diagnosa");
        
        for($col = 'A'; $col <= 'F'; $col++){
            $sheet->mergeCells($col.'1:'.$col.'2');
        }
        $sheet->mergeCells('K1:K2');
        $sheet->getStyle('A'.$no.':'.'K'.$no)->getFont()->setBold(true);
        $sheet->getStyle('A'.$no.':'.'K'.$no)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        
        $no++;
        $sheet->setCellValue("G".$no,"GDS");
        $sheet->setCellValue("H".$no,"GDP");
        $sheet->setCellValue("I".$no,"GDPP");
        $sheet->setCellValue("J".$no,"Tensi");
        
        $sheet->getStyle('A'.$no.':'.'K'.$no)->getFont()->setBold(true);
        $sheet->getStyle('A'.$no.':'.'K'.$no)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        
        //Write Content
        foreach($fix as $x){
            $no++;
            $sheet->setCellValue("A".$no,$x["No."]);
            $sheet->setCellValue("B".$no,$x["Tanggal"]);
            $sheet->setCellValue("C".$no,$x["Nama"]);    
            $sheet->setCellValue("D".$no,$x["No. BPJS"]);    
            $sheet->setCellValue("E".$no,$x["Alamat"]);
            $sheet->setCellValue("F".$no,$x["Telp"]);
            $sheet->setCellValue("G".$no,$x["GDS"]);
            $sheet->setCellValue("H".$no,$x["GDP"]);
            $sheet->setCellValue("I".$no,$x["GDPP"]);
            $sheet->setCellValue("J".$no,$x["Tensi"]);
            $sheet->setCellValue("K".$no,$x["Diagnosa"]);
            
            $sheet->getStyle('A'.$no.':'.'B'.$no)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $sheet->getStyle('C'.$no)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            $sheet->getStyle('D'.$no.':'.'K'.$no)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        }
        
        foreach(range('A','K') as $columnID) {
            $sheet->getColumnDimension($columnID)->setAutoSize(true);
        }
        
        header ( 'Content-Type: application/vnd.ms-excel' );
        header ( 'Content-Disposition: attachment;filename="Laporan Prolanis.xls"' );
        header ( 'Cache-Control: max-age=0' );
        $writer = PHPExcel_IOFactory::createWriter ( $file, 'Excel5' );
        $writer->save ( 'php://output' );
		return;
    }
}

?>