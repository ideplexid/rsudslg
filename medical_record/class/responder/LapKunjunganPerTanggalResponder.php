<?php 

class LapKunjunganPerTanggalResponder extends DBResponder{
    
    public function excel(){
        /*@var User*/
		global $user;
        require_once "smis-libs-out/php-excel/PHPExcel.php";
        loadLibrary("smis-libs-function-excel");
        $this->dbtable->setShowAll(true);
        $data=$this->dbtable->view("",0);
        $list=$data['data'];
        $result=array();
        $ruangan=array();
        foreach($list as $x){
            if(!isset($ruangan[$x->ruangan])){
                $ruangan[$x->ruangan]=ArrayAdapter::format("unslug",$x->ruangan);
            }
            if(!isset($result[$x->tanggal])){
                $result[$x->tanggal]=array();
            }
            $result[$x->tanggal][$x->ruangan]=array("masuk"=>$x->masuk,"rujukan"=>$x->rujukan);
        }
        
        $file = new PHPExcel ();
        $file->getProperties ()->setCreator ( $user->getNameOnly() );
        $file->getProperties ()->setLastModifiedBy ( $user->getNameOnly() );
        $file->getProperties ()->setTitle ( "Data Kunjungan Pasien" );
        $file->getProperties ()->setSubject ( "Data Kunjungan Pasien" );
        $file->getProperties ()->setDescription ( "Data Kunjungan Pasien" );
        $file->getProperties ()->setKeywords ( "Data Kunjungan Pasien" );
        $file->getProperties ()->setCategory ( "Data Kunjungan Pasien" );
        
        $total=count($ruangan)*2;
        $sheet = $file->getActiveSheet ();
        $kolom=PHPExcel_Cell::stringFromColumnIndex($total);
        $sheet->mergeCells ( 'A1:'.$kolom.'1' )->setCellValue ( "A1", "DATA KUNJUNGAN PASIEN" );
        $sheet->getStyle ( 'A1')->getFont ()->setBold ( true );
        $sheet->mergeCells ( 'A2:A3' )->setCellValue ( "A2", "Tanggal" );
        $sheet->getStyle ( 'A2:A3')->getFont ()->setBold ( true );
        $sheet->getColumnDimension ( "A" )->setAutoSize ( true );
        $index=0;
        foreach($ruangan as $x=>$y){
            $kolom1=PHPExcel_Cell::stringFromColumnIndex($index+1);
            $kolom2=PHPExcel_Cell::stringFromColumnIndex($index+2);
            $sheet->mergeCells ( $kolom1.'2:'.$kolom2.'2' )->setCellValue ( $kolom1 . '2', $y );
            $sheet->getStyle (  $kolom1 . '2' )->getFont ()->setBold ( true );
            
            $sheet->setCellValue ( $kolom1 . '3', "Masuk" );
            $sheet->getStyle (  $kolom1 . '3' )->getFont ()->setBold ( true );
            $sheet->setCellValue ( $kolom2 . '3', "Rujukan" );
            $sheet->getStyle (  $kolom2 . '3' )->getFont ()->setBold ( true );
            $index+=2;
        }
        $baris=3;
        foreach($result as $tanggal=>$isi){
            $baris++;
            date_excel_format($sheet,"A".$baris,$tanggal);            
            $index=0;
            foreach($ruangan as $x=>$y){
                if(isset($isi[$x])){
                    if($isi[$x]['masuk']>0){
                        $kolom1=PHPExcel_Cell::stringFromColumnIndex($index+1);
                        $sheet->setCellValue ( $kolom1 . $baris, $isi[$x]['masuk'] );
                    }
                    
                    if($isi[$x]['rujukan']>0){
                        $kolom2=PHPExcel_Cell::stringFromColumnIndex($index+2);
                        $sheet->setCellValue ( $kolom2 . $baris, $isi[$x]['rujukan'] );
                    }                        
                }
                $index+=2;
            }
        }
        $baris++;
        $sheet->setCellValue ( 'A'.$baris, "TOTAL" );
        $sheet->getStyle (  'A'.$baris )->getFont ()->setBold ( true );
        for($i=1;$i<=$total;$i++){
            $kolom=PHPExcel_Cell::stringFromColumnIndex($i);
            $sheet->setCellValue ( $kolom.($baris), "=sum(".$kolom."4:".$kolom.($baris-1).")" );
            $sheet->getStyle (  $kolom.($baris) )->getFont ()->setBold ( true );
        }
        
        $fillcabang['borders']=array();
        $fillcabang['borders']['allborders']=array();
        $fillcabang['borders']['allborders']['style']=PHPExcel_Style_Border::BORDER_THIN ;
        $sheet->getStyle ( "A1:".$kolom.$baris )->applyFromArray ( $fillcabang );
        
        header ( 'Content-Type: application/vnd.ms-excel' );
        header ( 'Content-Disposition: attachment;filename="Data Kunjungan Pasien.xls"' );
        header ( 'Cache-Control: max-age=0' );
        $writer = PHPExcel_IOFactory::createWriter ( $file, 'Excel5' );
        $writer->save ( 'php://output' );
        return null;
	}
}


?>