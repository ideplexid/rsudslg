<?php 
require_once 'smis-base/smis-include-service-consumer.php';
class RencanaBPJSResponder extends DBResponder{

	public function command($command){
		if($command=="lock_bpjs"){
			$pack=new ResponsePackage();
			$pack->setStatus(ResponsePackage::$STATUS_OK);
			$data['noreg_pasien']=$_POST['noreg_pasien'];
			$ser=new ServiceConsumer($this->dbtable->get_db(), "lock_bpjs",$data,"registration");
			$ser->execute();
			$hasil=$ser->getContent();
			$pack->setContent($hasil);
			return $pack->getPackage();
		}else if(command=="save"){
			$pack=new ResponsePackage();
			$content=$this->save();
			if($content['successfull']=="-1"){
				$pack->setContent($content);
				$pack->setStatus(ResponsePackage::$STATUS_OK);
				$pack->setAlertVisible(true);
				$pack->setAlertContent("Gagal Menyimpan", "Plafon Tidak Mencukupi", ResponsePackage::$TIPE_INFO);
			}else{
				$pack->setContent($content);
				$pack->setStatus(ResponsePackage::$STATUS_OK);
				$pack->setAlertVisible(true);
				$pack->setAlertContent("Data Saved", "Your Data Had Been Saved", ResponsePackage::$TIPE_INFO);
			}
		}else{
			return parent::command($command);
		}
	}
	
	public function save(){
		if($this->sisa_plafon()>=$_POST['biaya']){
			$json=parent::save();
			$json['total_rencana']=$this->total_rencana();
			$json['successfull']="1";
			return $json;
		}else{
			$json['successfull']="-1";
			$json['total_rencana']=$this->total_rencana();
			return $json;
		}
	}
	
	private function total_rencana(){
		$query="SELECT SUM(biaya) as total FROM smis_mr_rencana WHERE prop!='del' AND noreg_pasien='".$_POST['noreg_pasien']."';";
		$total=$this->dbtable->get_db()->get_var($query); 
		return $total;
	}
	
	private function sisa_plafon(){
		$nilai_plafon=$_POST['plafon_bpjs'];
		$total=$this->total_rencana();
		return $nilai_plafon-$total;
	}
	
}

?>