<?php 

class MorbiditasResponder extends DBReport{
    
    private $carabayar;
    public function setCarabayar($carabayar){
        $this->carabayar=$carabayar;
        return $this;
    }
    
    public function excel(){
        $this->dbtable->addCustomKriteria(null," prop != 'del' ");
        $this->dbtable->addCustomKriteria(null," DATE(tanggal)>='".$_POST['from_date']."' ");
        $this->dbtable->addCustomKriteria(null," DATE(tanggal)<='".$_POST['to_date']."' ");
        if (isset ( $_POST ['filter_ruangan'] ) && $_POST ['filter_ruangan'] != '') {
            $this->dbtable->addCustomKriteria ( "ruangan", " = '" . $_POST ['filter_ruangan'] . "'" );
        }
		$this->dbtable->setShowAll(true);
        $this->dbtable->setMaximum($_POST['max']);
        $data=$this->dbtable->view("",0);
        
        $fix=$this->adapter->getContent($data['data']);
        
        /*echo "<pre>";
        var_dump($fix);
        echo "<pre>";*/
        
        require_once ("smis-libs-out/php-excel/PHPExcel.php");
        $file = new PHPExcel ();
        $file->getProperties ()->setCreator ( "SMIS" );
        $file->getProperties ()->setLastModifiedBy ( "SMIS" );
        $file->getProperties ()->setTitle ( "Laporan Morbiditas" );
        $file->getProperties ()->setSubject ( "Laporan Morbiditas" );
        $file->getProperties ()->setDescription ( "Laporan Morbiditas" );
        $file->getProperties ()->setKeywords ( "Laporan Morbiditas" );
        $file->getProperties ()->setCategory ( "Laporan Morbiditas" );
        $sheet = $file->getActiveSheet ();
        $no=1;
        
        
         
        $sheet->getColumnDimension("A")->setAutoSize(true);
        $sheet->mergeCells("A1:A2")->setCellValue("A1","No.");
        $sheet->mergeCells("B1:B2")->setCellValue("B1","DTD");
        $sheet->mergeCells("C1:C2")->setCellValue("C1","Diagnosa");
        $sheet->mergeCells("D1:E1")->setCellValue("D1","Kunjungan Baru");
        $sheet->mergeCells("F1:G1")->setCellValue("F1","Kunjungan Lama");
        $sheet->mergeCells("H1:I1")->setCellValue("H1","Jenis Kasus");
        $end=PHPExcel_Cell::stringFromColumnIndex(9+count($this->carabayar) - 2);
        $sheet->mergeCells("J1:".$end."1")->setCellValue("J1","Jenis Pasien");
        
        $sheet->setCellValue("D2","Laki");
        $sheet->setCellValue("E2","Perempuan");
        $sheet->setCellValue("F2","Laki");
        $sheet->setCellValue("G2","Perempuan");
        $sheet->setCellValue("H2","Baru");
        $sheet->setCellValue("I2","Lama");
        
        $no=1;
        foreach($this->carabayar as $x=>$y ){
            $huruf=PHPExcel_Cell::stringFromColumnIndex(9+$no - 2);
            $no++;
            $sheet->setCellValue($huruf."2",$y);
        }
        
        $sheet  ->getStyle('A1:'.$huruf.'2')
                ->getAlignment()
                ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $sheet  ->getStyle('A1:'.$huruf.'2')
                ->getFont()
                ->setBold(true);
        
        $no=2;
        foreach($fix as $o){
            $no++;
            $sheet->setCellValue("A".$no,$o['No']);
            $sheet->setCellValue("B".$no,$o['DTD']);
            $sheet->setCellValue("C".$no,$o['Diagnosa']);
            $sheet->setCellValue("D".$no,$o['L']);
            $sheet->setCellValue("E".$no,$o['P']);
            $sheet->setCellValue("F".$no,$o['L1']);
            $sheet->setCellValue("G".$no,$o['P1']);
            $sheet->setCellValue("H".$no,$o['Baru']);
            $sheet->setCellValue("I".$no,$o['Lama']);
            
            $index=1;
            foreach($this->carabayar as $x=>$y ){
                $huruf=PHPExcel_Cell::stringFromColumnIndex(9+$index - 2);
                $index++;
                $sheet->setCellValue($huruf.$no,$o[$y]);
            }
            $sheet  ->getStyle('A'.$no.':B'.$no)
                    ->getAlignment()
                    ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $sheet  ->getStyle('D'.$no.':'.$huruf.$no)
                    ->getAlignment()
                    ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        }
        $sheet  ->getStyle('A'.$no.':'.$huruf.$no)
                ->getFont()
                ->setBold(true);
                
        $sheet->getColumnDimension("C")->setAutoSize(true);
        $sheet->getColumnDimension("E")->setAutoSize(true);
        $sheet->getColumnDimension("G")->setAutoSize(true);
        
        $thin = array ();
        $thin['borders']=array();
        $thin['borders']['allborders']=array();
        $thin['borders']['allborders']['style']=PHPExcel_Style_Border::BORDER_DASHDOT ;
        $sheet  ->getStyle ( 'A1:'.$huruf.$no )->applyFromArray ($thin);
        $sheet  ->getStyle('A'.$no.':'.$huruf.(9+$index-2))
                ->getFont()
                ->setBold(true);
        
        header ( 'Content-Type: application/vnd.ms-excel' );
        header ( 'Content-Disposition: attachment;filename="Data Morbiditas.xls"' );
        header ( 'Cache-Control: max-age=0' );
        $writer = PHPExcel_IOFactory::createWriter ( $file, 'Excel5' );
        $writer->save ( 'php://output' );
		return;
    }
    
}

?>