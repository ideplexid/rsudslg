<?php 

class RL314Responder extends ServiceResponder {
    public function excel() {
        $this->addData("command","list");
        $hasil=$this->getData();
        $this->getService()->setData($hasil);
		$this->getService()->execute();
		$d=$this->getService()->getContent();
		$data=$d['data'];
        
        $fix = $this->getAdapter()->getContent($data);
        
        require_once ("smis-libs-out/php-excel/PHPExcel.php");
        $file = new PHPExcel ();
        $file->getProperties ()->setCreator ( "Rumah Sakit Syamrabu" );
        $file->getProperties ()->setLastModifiedBy ( "Rumah Sakit Bagian Administrator" );
        $file->getProperties ()->setTitle ( "Laporan RL 3.14 Rujukan" );
        $file->getProperties ()->setSubject ( "Laporan RL 3.14" );
        $file->getProperties ()->setDescription ( "Laporan RL 3.14 Rujukan Generated From system" );
        $file->getProperties ()->setKeywords ( "Laporan RL 3.14" );
        $file->getProperties ()->setCategory ( "Laporan RL 3.14" );
        $sheet = $file->getActiveSheet ();
        
        $no = 1;
        $sheet->setCellValue("A".$no,"No.");
        $sheet->setCellValue("B".$no,"Layanan");
        $sheet->setCellValue("C".$no,"Rujukan PKM");
        $sheet->setCellValue("D".$no,"Rujukan RS Lain");
        $sheet->setCellValue("E".$no,"Rujukan Faskes Lain");
        $sheet->setCellValue("F".$no,"Kembali PKM");
        $sheet->setCellValue("G".$no,"Kembali RS Lain");
        $sheet->setCellValue("H".$no,"Kembali Faskes Lain");
        $sheet->setCellValue("I".$no,"Rujukan Dirujuk");
        $sheet->setCellValue("J".$no,"DS Dirujuk");
        $sheet->setCellValue("K".$no,"Kembali");
        $sheet->getStyle('A'.$no.':'.'K'.$no)->getFont()->setBold(true);
        $sheet->getStyle('A'.$no.':'.'K'.$no)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        
        $length = sizeof($fix);
        foreach($fix as $x)
        {
            $no++;
            if($no == $length + 1)
            {
                $layanan = explode("<strong>",$x["Layanan"]);
                $layanan1 = explode("</strong>",$layanan[1]);
                
                $sheet->setCellValue("B".$no,$layanan1[0]);
                $sheet->setCellValue("C".$no,$x['Rujukan PKM']);
                $sheet->setCellValue("D".$no,$x['Rujukan RS Lain']);
                $sheet->setCellValue("E".$no,$x['Rujukan Faskes Lain']);
                $sheet->setCellValue("F".$no,$x['Kembali PKM']);
                $sheet->setCellValue("G".$no,$x['Kembali RS Lain']);
                $sheet->setCellValue("H".$no,$x['Kembali Faskes Lain']);
                $sheet->setCellValue("I".$no,$x['Rujukan Dirujuk']);
                $sheet->setCellValue("J".$no,$x['DS Dirujuk']);
                $sheet->setCellValue("K".$no,$x['Kembali']);
                $sheet->getStyle('A'.$no.':'.'K'.$no)->getFont()->setBold(true);
                $sheet->getStyle('A'.$no)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                $sheet->getStyle('B'.$no)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
                $sheet->getStyle('C'.$no.':'.'K'.$no)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            }
            else
            {
                $sheet->setCellValue("A".$no,$x["No."]);
                $sheet->setCellValue("B".$no,$x["Layanan"]);
                $sheet->setCellValue("C".$no,$x['Rujukan PKM']);
                $sheet->setCellValue("D".$no,$x['Rujukan RS Lain']);
                $sheet->setCellValue("E".$no,$x['Rujukan Faskes Lain']);
                $sheet->setCellValue("F".$no,$x['Kembali PKM']);
                $sheet->setCellValue("G".$no,$x['Kembali RS Lain']);
                $sheet->setCellValue("H".$no,$x['Kembali Faskes Lain']);
                $sheet->setCellValue("I".$no,$x['Rujukan Dirujuk']);
                $sheet->setCellValue("J".$no,$x['DS Dirujuk']);
                $sheet->setCellValue("K".$no,$x['Kembali']);
                //$sheet->getStyle('A'.$no.':'.'C'.$no)->getFont()->setBold(true);
                $sheet->getStyle('A'.$no)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                $sheet->getStyle('B'.$no)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
                $sheet->getStyle('C'.$no.':'.'K'.$no)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            }
        }
        
        foreach(range('A','K') as $columnID) {
            $sheet->getColumnDimension($columnID)->setAutoSize(true);
        }
        
        header ( 'Content-Type: application/vnd.ms-excel' );
        header ( 'Content-Disposition: attachment;filename="Laporan RL 3.14.xls"' );
        header ( 'Cache-Control: max-age=0' );
        $writer = PHPExcel_IOFactory::createWriter ( $file, 'Excel5' );
        $writer->save ( 'php://output' );
		return;
        
    }
}

 ?>