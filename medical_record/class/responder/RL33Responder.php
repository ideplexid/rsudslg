<?php

class RL33Responder extends DBResponder {
    public function excel() {
        $this->dbtable->setFetchMethode(DBTable::$ARRAY_FETCH);
        $data=$this->dbtable->view("",0);
        $fix=$this->adapter->getContent($data['data']);
        
        require_once ("smis-libs-out/php-excel/PHPExcel.php");
        $file = new PHPExcel ();
        $file->getProperties ()->setLastModifiedBy ( "Rumah Sakit Bagian Administrator" );
        $file->getProperties ()->setTitle ( "Laporan RL 3.3 Gigi dan Mulut" );
        $file->getProperties ()->setSubject ( "Laporan RL 3.3" );
        $file->getProperties ()->setDescription ( "Laporan RL 3.3 Gigi dan Mulut Generated From system" );
        $file->getProperties ()->setKeywords ( "Laporan RL 3.3" );
        $file->getProperties ()->setCategory ( "Laporan RL 3.3" );
        $sheet = $file->getActiveSheet ();
        
        $no = 1;

        $sheet->setCellValue("A".$no,"KODE PROPINSI");
        $sheet->setCellValue("B".$no,"KAB/KOTA");
        $sheet->setCellValue("C".$no,"KODE RS"); 
        $sheet->setCellValue("D".$no,"NAMA RS");
        $sheet->setCellValue("E".$no,"TAHUN"); 
        $sheet->setCellValue("F".$no,"NO.");
        $sheet->setCellValue("G".$no,"JENIS KEGIATAN");
        $sheet->setCellValue("H".$no,"JUMLAH");
        
        $sheet->getStyle('A'.$no.':'.'H'.$no)->getFont()->setBold(true);
        $sheet->getStyle('A'.$no.':'.'H'.$no)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        
        $row = $this->getDBTable()->get_db()->get_row(" SELECT * FROM smis_mr_data_dasar_rs WHERE prop = '' LIMIT 0, 1 ");
        $length = sizeof($fix);
        foreach($fix as $x) {
            $no++;
            
            if($no == $length + 1){
                $layanan = explode("<strong>",$x["Layanan"]);
                $layanan1 = explode("</strong>",$layanan[1]);
                
                $total = explode("<strong>",$x["Total"]);
                $total1 = explode("</strong>",$total[1]);
                
                $sheet->setCellValue("G".$no,$layanan1[0]);
                $sheet->setCellValue("H".$no,$total1[0]);
                $sheet->getStyle('F'.$no.':'.'C'.$no)->getFont()->setBold(true);
                $sheet->getStyle('F'.$no)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                $sheet->getStyle('G'.$no)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
                $sheet->getStyle('H'.$no)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            }else{
                $sheet->setCellValue("A".$no,$row->kode_provinsi);
                $sheet->setCellValue("B".$no,$row->kota_kabupaten);
                $sheet->setCellValue("C".$no,$row->koders);
                $sheet->setCellValue("D".$no,$row->nama_rs);
                $sheet->setCellValue("E".$no,$_POST['tahun']);

                $sheet->setCellValue("F".$no,$x["No."]);
                $sheet->setCellValue("G".$no,$x["Layanan"]);
                $sheet->setCellValue("H".$no,$x["Total"]);    
                $sheet->getStyle('F'.$no)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                $sheet->getStyle('G'.$no)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
                $sheet->getStyle('H'.$no)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            }
        }
        
        foreach(range('A','H') as $columnID) {
            $sheet->getColumnDimension($columnID)->setAutoSize(true);
        }

        $thin = array ();
        $thin['borders']=array();
        $thin['borders']['allborders']=array();
        $thin['borders']['allborders']['style']=PHPExcel_Style_Border::BORDER_THIN ;
        $sheet->getStyle ( 'A1:H'.$no )->applyFromArray ($thin);
        
        header ( 'Content-Type: application/vnd.ms-excel' );
        header ( 'Content-Disposition: attachment;filename="Laporan RL 3.3 - Gigi dan Mulut.xls"' );
        header ( 'Cache-Control: max-age=0' );
        $writer = PHPExcel_IOFactory::createWriter ( $file, 'Excel5' );
        $writer->save ( 'php://output' );
		return;
    }
}