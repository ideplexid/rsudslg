<?php 

class PersalinanResponder extends DBReport{
    
    public function excel(){
        /*@var User*/
		global $user;
        require_once "smis-libs-out/php-excel/PHPExcel.php";
        loadLibrary("smis-libs-function-excel");
        $this->dbtable->setFetchMethode(DBTable::$OBJECT_FETCH);
        $this->dbtable->setPreferredQuery(false, "", "");
        $this->dbtable->setShowAll(true);
        $this->dbtable->setOrder ( " tanggal ASC" );
        $this->dbtable->setGroupBy(false, " metode ");
        $this->dbtable->addCustomKriteria(" tanggal>= ","'".$_POST['from_date']."'");
        $this->dbtable->addCustomKriteria(" tanggal< ","'".$_POST['to_date']."'");
        $data=$this->dbtable->view("",0);
        $list=$data['data'];
        
        $file = new PHPExcel ();
        $file->getProperties ()->setCreator ( $user->getNameOnly() );
        $file->getProperties ()->setLastModifiedBy ( $user->getNameOnly() );
        
        $sheet = $file->getActiveSheet ();
        $kolom=PHPExcel_Cell::stringFromColumnIndex();
        $sheet->mergeCells ( 'A1:G1' )->setCellValue ( "A1", "LAPORAN NEONATAL & MATERNAL" );
        $sheet->mergeCells ( 'A2:B2' )->setCellValue ( "A2", "DARI" );
        $sheet->setCellValue ( "C2", ArrayAdapter::format("date d M Y",$_POST['from_date']) );
        $sheet->mergeCells ( 'D2:F2' )->setCellValue ( "D2", "SAMPAI" );
        $sheet->setCellValue ( "G2", ArrayAdapter::format("date d M Y",$_POST['to_date']) );
        
        $sheet->setCellValue ( "A3","No."  );
        $sheet->setCellValue ( "B3","Nama"  );
        $sheet->setCellValue ( "C3","No BPJS"  );
        $sheet->setCellValue ( "D3","Alamat"  );
        $sheet->setCellValue ( "E3","No Telp"  );
        $sheet->setCellValue ( "F3","Tanggal" );
        $sheet->setCellValue ( "G3","Keterangan"  );
        
        $sheet->getStyle ( 'A1:G3')->getFont ()->setBold ( true );
        
        $no=0;
        $start=4;
        foreach($list as $x){
            $no++;
            $sheet->setCellValue ( "A".$start,$no."."  );
            $sheet->setCellValue ( "B".$start,$x->nama_pasien  );
            $sheet->setCellValue ( "C".$start,$x->no_bpjs  );
            $sheet->setCellValue ( "D".$start,$x->alamat );
            $sheet->setCellValue ( "E".$start,$x->no_telp );
            $sheet->setCellValue ( "F".$start,ArrayAdapter::format("date d M Y",$x->tanggal) );
            $sheet->setCellValue ( "G".$start,$x->keterangan );
            $start++;
        }
        
        $fillcabang['borders']=array();
        $fillcabang['borders']['allborders']=array();
        $fillcabang['borders']['allborders']['style']=PHPExcel_Style_Border::BORDER_THIN ;
        $sheet->getStyle ( "A1:G".$start )->applyFromArray ( $fillcabang );
        
        header ( 'Content-Type: application/vnd.ms-excel' );
        header ( 'Content-Disposition: attachment;filename="Lap Neonatal dan Maternal.xls"' );
        header ( 'Cache-Control: max-age=0' );
        $writer = PHPExcel_IOFactory::createWriter ( $file, 'Excel5' );
        $writer->save ( 'php://output' );
        return null;
    }
    
}

?>