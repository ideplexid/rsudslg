<?php 

class LapDiagnosaDetailPerTanggal extends DBResponder{
    
    public function excel(){
        $data=$this->dbtable->view("",0);
        $fix=$this->adapter->getContent($data['data']);
        
        
        $dari=$_POST['dari'];
		$sampai=$_POST['sampai'];
        loadLibrary("smis-libs-function-time");
		$date_range=createDateRangeArray($dari,$sampai);
		$month_range=createMonthDateRange($date_range);
		$year_range=createYearMonthRange($month_range);		
		
        require_once ("smis-libs-out/php-excel/PHPExcel.php");
        $file = new PHPExcel ();
        $file->getProperties ()->setCreator ( "SMIS" );
        $file->getProperties ()->setLastModifiedBy ( "SMIS" );
        $file->getProperties ()->setTitle ( "Laporan Diagnosa Detail Per Tanggal" );
        $file->getProperties ()->setSubject ( "Data Diagnosa" );
        $file->getProperties ()->setDescription ( "Data Pegawai" );
        $file->getProperties ()->setKeywords ( "Data Pegawai " );
        $file->getProperties ()->setCategory ( "Data Pegawai " );
        $sheet = $file->getActiveSheet ();
        $no=1;
        
        
        $year_start=2;
        $sheet->mergeCells("A1:A3")->setCellValue("A1","Nama");
        foreach($year_range as $year=>$span){
            $first=PHPExcel_Cell::stringFromColumnIndex($year_start - 1);
            $second=PHPExcel_Cell::stringFromColumnIndex($year_start+$span - 2);
            $sheet->mergeCells($first."1:".$second."1")->setCellValue($first."1",$year);
            $year_start=$year_start+$span;
        }
        
        $month_start=2;
        foreach($month_range as $month=>$span){
            $month=enid_month(substr($month, 5,7)*1);
            $first=PHPExcel_Cell::stringFromColumnIndex($month_start - 1);
            $second=PHPExcel_Cell::stringFromColumnIndex($month_start+$span - 2);
            $sheet->mergeCells($first."2:".$second."2")->setCellValue($first."2",$month);
            $month_start=$month_start+$span;
        }
        
        $day=2;
        foreach($date_range as $x){
            $x=substr($x, 8,10);
            $first=PHPExcel_Cell::stringFromColumnIndex($day - 1);
            $sheet->setCellValue($first."3",$x);
            $day++;
        }
        
        $last_column=PHPExcel_Cell::stringFromColumnIndex($day - 1);
        $sheet  ->mergeCells($last_column."1:".$last_column."3")
                ->setCellValue($last_column."1","Total");
        $sheet  ->getStyle('A1:'.$last_column.'3')
                ->getAlignment()
                ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $sheet  ->getStyle('A1:'.$last_column.'3')
                ->getFont()
                ->setBold(true);
        $no=3;
        foreach($fix as $x){
            $no++;
            $sheet->setCellValue("A".$no,$x["Nama"]);
            $day=2;
            foreach($date_range as $date){
                $head_name=str_replace("-", "_", $date);
                $first=PHPExcel_Cell::stringFromColumnIndex($day - 1);
                $sheet->setCellValue($first.$no,$x[$head_name]);
                $day++;
            }
            $sheet->setCellValue($last_column.$no,"=SUM(B".$no.":".$first.$no.")");
        }
        
        
        $thin = array ();
        $thin['borders']=array();
        $thin['borders']['allborders']=array();
        $thin['borders']['allborders']['style']=PHPExcel_Style_Border::BORDER_THIN ;
        $sheet->getStyle ( 'A1:'.$last_column.$no )->applyFromArray ($thin);
        
        $sheet->getColumnDimension("A")->setAutoSize(true);
        
        header ( 'Content-Type: application/vnd.ms-excel' );
        header ( 'Content-Disposition: attachment;filename="Laporan Persentase Diagnosa.xls"' );
        header ( 'Cache-Control: max-age=0' );
        $writer = PHPExcel_IOFactory::createWriter ( $file, 'Excel5' );
        $writer->save ( 'php://output' );
		return;
        
    }
    
}

?>