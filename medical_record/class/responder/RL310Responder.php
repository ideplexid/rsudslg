<?php

class RL310Responder extends DBResponder {
    public function excel() {
        $this->dbtable->setFetchMethode(DBTable::$ARRAY_FETCH);
        $data=$this->dbtable->view("",0);
        $fix=$this->adapter->getContent($data['data']);
        
        require_once ("smis-libs-out/php-excel/PHPExcel.php");
        $file = new PHPExcel ();
        $file->getProperties ()->setCreator ( "Rumah Sakit Syamrabu" );
        $file->getProperties ()->setLastModifiedBy ( "Rumah Sakit Bagian Administrator" );
        $file->getProperties ()->setTitle ( "Laporan RL 3.10 Pelayanan Khusus" );
        $file->getProperties ()->setSubject ( "Laporan RL 3.10" );
        $file->getProperties ()->setDescription ( "Laporan RL 3.10 Pelayanan Khusus Generated From system" );
        $file->getProperties ()->setKeywords ( "Laporan RL 3.10" );
        $file->getProperties ()->setCategory ( "Laporan RL 3.10" );
        $sheet = $file->getActiveSheet ();
        
        $no = 1;
        $sheet->setCellValue("A".$no,"No.");
        $sheet->setCellValue("B".$no,"Layanan");
        $sheet->setCellValue("C".$no,"Total");    
        $sheet->getStyle('A'.$no.':'.'C'.$no)->getFont()->setBold(true);
        $sheet->getStyle('A'.$no.':'.'C'.$no)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        
        $length = sizeof($fix);
        foreach($fix as $x) 
        {
            $no++;
            if($no == $length + 1)
            {
                $layanan = explode("<strong>",$x["Layanan"]);
                $layanan1 = explode("</strong>",$layanan[1]);
                
                $total = explode("<strong>",$x["Total"]);
                $total1 = explode("</strong>",$total[1]);
                
                $sheet->setCellValue("B".$no,$layanan1[0]);
                $sheet->setCellValue("C".$no,$total1[0]);
                $sheet->getStyle('A'.$no.':'.'C'.$no)->getFont()->setBold(true);
                $sheet->getStyle('A'.$no)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                $sheet->getStyle('B'.$no)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
                $sheet->getStyle('C'.$no)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            }
            else
            {
                $sheet->setCellValue("A".$no,$x["No."]);
                $sheet->setCellValue("B".$no,$x["Layanan"]);
                $sheet->setCellValue("C".$no,$x["Total"]);    
                $sheet->getStyle('A'.$no)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                $sheet->getStyle('B'.$no)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
                $sheet->getStyle('C'.$no)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            }
        }
        
        foreach(range('A','C') as $columnID) {
            $sheet->getColumnDimension($columnID)->setAutoSize(true);
        }
        
        header ( 'Content-Type: application/vnd.ms-excel' );
        header ( 'Content-Disposition: attachment;filename="Laporan RL 3.10.xls"' );
        header ( 'Cache-Control: max-age=0' );
        $writer = PHPExcel_IOFactory::createWriter ( $file, 'Excel5' );
        $writer->save ( 'php://output' );
		return;
    }
}

?>