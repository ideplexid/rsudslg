<?php

class LAPRJResponder extends DBResponder {
	public function excel() {
		$this->dbtable->setShowAll(true);
        $data=$this->dbtable->view("",0);
        $fix=$this->adapter->getContent($data['data']);
        
        require_once ("smis-libs-out/php-excel/PHPExcel.php");
        $file = new PHPExcel ();
        $file->getProperties ()->setCreator ( "PT. Inovasi Ide Utama" );
        $file->getProperties ()->setLastModifiedBy ( "Administrator" );
        $file->getProperties ()->setTitle ( "Laporan Rawat Jalan" );
        $file->getProperties ()->setSubject ( "Laporan Rawat Jalan" );
        $file->getProperties ()->setDescription ( "Laporan Rawat Jalan Generated by System" );
        $file->getProperties ()->setKeywords ( "Laporan Rawat Jalan" );
        $file->getProperties ()->setCategory ( "Laporan Rawat Jalan" );
        
        $sheet = $file->getActiveSheet ();
        $i = 1;

        $objDrawing = new PHPExcel_Worksheet_Drawing();
        $objDrawing->setName('logo_img');
        $objDrawing->setDescription('logo_img');
        $objDrawing->setPath(getLogoNonInterlaced());
        $objDrawing->setCoordinates('A'.$i);
        $objDrawing->setOffsetX(20); 
        $objDrawing->setOffsetY(5);
        $objDrawing->setWidth(100); 
        $objDrawing->setHeight(100);
        $objDrawing->setWorksheet($sheet);
        $i++;
        
        $sheet->setCellValue("C".$i, getSettings($db,"smis_autonomous_title"));
        $sheet->getStyle("C".$i)->getFont()->setBold(true)->setSize(14);
        $i++;
        $sheet->setCellValue("C".$i, getSettings($db,"smis_autonomous_address"));
        $sheet->getStyle("C".$i)->getFont()->setBold(true)->setSize(14);
        $i++;
        $sheet->setCellValue("C".$i, getSettings($db,"smis_autonomous_contact"));
        $sheet->getStyle("C".$i)->getFont()->setBold(true)->setSize(14);
        $i += 3;
        $sheet->mergeCells("A".$i.":M".$i)->setCellValue("A".$i,"LAPORAN RAWAT JALAN");
        $sheet->getStyle("A".$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle("A".$i)->getFont()->setBold(true)->setSize(14);
        $i++;
        $sheet->mergeCells("A".$i.":M".$i)->setCellValue("A".$i, strtoupper(ArrayAdapter::dateFormat("date d M Y", $_POST['dari']))." - ".strtoupper(ArrayAdapter::dateFormat("date d M Y", $_POST['sampai'])));
        $sheet->getStyle("A".$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle("A".$i)->getFont()->setBold(true);
        $i += 3;

        $border_start = $i;
        $sheet->setCellValue("A".$i, "No.");
        $sheet->setCellValue("B".$i, "Tgl Kunjungan");
        $sheet->setCellValue("C".$i, "NRM");
        $sheet->setCellValue("D".$i, "Nama");
        $sheet->setCellValue("E".$i, "Ruangan");
        $sheet->setCellValue("F".$i, "Diagnosa");
        $sheet->setCellValue("G".$i, "Dokter");
        $sheet->setCellValue("H".$i, "L/B");
        $sheet->setCellValue("I".$i, "JK");
        $sheet->setCellValue("J".$i, "Kelurahan");
        $sheet->setCellValue("K".$i, "Kecamatan");
        $sheet->setCellValue("L".$i, "Kabupaten");
        $sheet->setCellValue("M".$i, "Propinsi");
        $sheet->getStyle("A".$i.":M".$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle("A".$i.":M".$i)->getFont()->setBold(true);
        $i++;

        for($a = 0; $a < sizeof($fix); $a++) {
        	$sheet->setCellValue("A".$i, $fix[$a]['No.']);
            $sheet->setCellValue("B".$i, $fix[$a]['Tgl Kunj']);
            $sheet->setCellValue("C".$i, $fix[$a]['NRM']);
            $sheet->setCellValue("D".$i, $fix[$a]['Nama']);
            $sheet->setCellValue("E".$i, $fix[$a]['Ruangan']);
            $sheet->setCellValue("F".$i, $fix[$a]['Diagnosa']);
            $sheet->setCellValue("G".$i, $fix[$a]['Dokter']);
            $sheet->setCellValue("H".$i, $fix[$a]['L/B']);
            $sheet->setCellValue("I".$i, $fix[$a]['JK']);
            $sheet->setCellValue("J".$i, $fix[$a]['Kelurahan']);
            $sheet->setCellValue("K".$i, $fix[$a]['Kecamatan']);
            $sheet->setCellValue("L".$i, $fix[$a]['Kabupaten']);
            $sheet->setCellValue("M".$i, $fix[$a]['Kabupaten']);
            $sheet->getStyle("A".$i.":M".$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $i++;
        }

        $border_end = $i - 1;

        foreach(range('A','M') as $columnID) {
            $sheet->getColumnDimension($columnID)->setAutoSize(true);
        }

        $thin = array ();
        $thin['borders']=array();
        $thin['borders']['allborders']=array();
        $thin['borders']['allborders']['style']=PHPExcel_Style_Border::BORDER_THIN ;
        $sheet  ->getStyle ( "A".$border_start.":M".$border_end )->applyFromArray ($thin);
        
        $filename = "Laporan Rawat Jalan ".strtoupper(ArrayAdapter::dateFormat("date d M Y", $_POST['dari']))." - ".strtoupper(ArrayAdapter::dateFormat("date d M Y", $_POST['sampai'])).".xls";
        header ( 'Content-Type: application/vnd.ms-excel' );
        header ( 'Content-Disposition: attachment;filename="'.$filename.'"' );
        header ( 'Cache-Control: max-age=0' );
        $writer = PHPExcel_IOFactory::createWriter ( $file, 'Excel5' );
        $writer->save ( 'php://output' );
        return;
	}
}

?>