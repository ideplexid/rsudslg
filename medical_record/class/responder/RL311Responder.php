<?php 

class RL311Responder extends DBResponder {
    public function excel() {
        $this->dbtable->setFetchMethode(DBTable::$ARRAY_FETCH);
        $data=$this->dbtable->view("",0);
        $fix=$this->adapter->getContent($data['data']);

        $kode_rs = "";
        $nama_rs = "";
        $kode_propinsi = "";
        $kabupaten_kota = "";
        $info_rs_row = $this->dbtable->get_row("
            SELECT 
                *
            FROM 
                smis_mr_data_dasar_rs
            WHERE 
                prop = ''
            LIMIT 
                0, 1
        ");
        if ($info_rs_row != null) {
            $kode_rs = $info_rs_row['koders'];
            $nama_rs = $info_rs_row['nama_rs'];
            $kode_propinsi = $info_rs_row['kode_provinsi'];
            $kabupaten_kota = $info_rs_row['kota_kabupaten'];
        }
        
        require_once ("smis-libs-out/php-excel/PHPExcel.php");
        $file = new PHPExcel ();
        $file->getProperties ()->setCreator ( "Rumah Sakit Syamrabu" );
        $file->getProperties ()->setLastModifiedBy ( "Rumah Sakit Bagian Administrator" );
        $file->getProperties ()->setTitle ( "Laporan RL 3.11 Kesehatan Jiwa" );
        $file->getProperties ()->setSubject ( "Laporan RL 3.11" );
        $file->getProperties ()->setDescription ( "Laporan RL 3.11 Kesehatan Jiwa Generated From system" );
        $file->getProperties ()->setKeywords ( "Laporan RL 3.11" );
        $file->getProperties ()->setCategory ( "Laporan RL 3.11" );
        $sheet = $file->getActiveSheet ();

        $no = 1;
        $sheet->mergeCells("A".$no.":H".$no)->setCellValue("A".$no,"RL 3.11 - Kesehatan Jiwa");
        $sheet->getStyle("A".$no.":H".$no)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle("A".$no)->getFont()->setBold(true);
        $no = $no + 2;
        
        $border_start = $no;
        $sheet->setCellValue("A".$no,"Kode Propinsi");
        $sheet->setCellValue("B".$no,"Kab/Kota");
        $sheet->setCellValue("C".$no,"Kode RS");
        $sheet->setCellValue("D".$no,"Nama RS");
        $sheet->setCellValue("E".$no,"Tahun");
        $sheet->setCellValue("F".$no,"No.");
        $sheet->setCellValue("G".$no,"Layanan");
        $sheet->setCellValue("H".$no,"Total");
        $sheet->getStyle('A'.$no.':'.'G'.$no)->getFont()->setBold(true);
        $sheet->getStyle('A'.$no.':'.'G'.$no)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        
        $length = sizeof($fix);
        foreach($fix as $x) 
        {
            $no++;
            if($no == $length + 3)
            {
                $layanan = explode("<strong>",$x["Layanan"]);
                $layanan1 = explode("</strong>",$layanan[1]);
                
                $total = explode("<strong>",$x["Total"]);
                $total1 = explode("</strong>",$total[1]);
                
                $sheet->mergeCells("A".$no.":G".$no)->setCellValue("A".$no,$layanan1[0]);
                $sheet->setCellValue("H".$no,$total1[0]);
                $sheet->getStyle('A'.$no.':'.'H'.$no)->getFont()->setBold(true);
                $sheet->getStyle('F'.$no)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                $sheet->getStyle('H'.$no)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
            }
            else
            {
                $sheet->setCellValue("A".$no,$kode_propinsi);
                $sheet->setCellValue("B".$no,$kabupaten_kota);
                $sheet->setCellValue("C".$no,$kode_rs);
                $sheet->setCellValue("D".$no,$nama_rs);
                $sheet->setCellValue("E".$no,$x["Tahun"]);
                $sheet->setCellValue("F".$no,$x["No."]);
                $sheet->setCellValue("G".$no,$x["Layanan"]);
                $sheet->setCellValue("H".$no,$x["Total"]);    
                $sheet->getStyle('F'.$no)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                $sheet->getStyle('A'.$no.':'.'E'.$no)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
                $sheet->getStyle('G'.$no)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
                $sheet->getStyle('H'.$no)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
            }
        }
        
        $thin = array ();
        $thin['borders']=array();
        $thin['borders']['allborders']=array();
        $thin['borders']['allborders']['style']=PHPExcel_Style_Border::BORDER_THIN ;
        $sheet->getStyle ( "A".$border_start.":H".$no )->applyFromArray ($thin);

        foreach(range('A','H') as $columnID) {
            $sheet->getColumnDimension($columnID)->setAutoSize(true);
        }
        
        header ( 'Content-Type: application/vnd.ms-excel' );
        header ( 'Content-Disposition: attachment;filename="Laporan RL 3.11 Kesehatan Jiwa.xls"' );
        header ( 'Cache-Control: max-age=0' );
        $writer = PHPExcel_IOFactory::createWriter ( $file, 'Excel5' );
        $writer->save ( 'php://output' );
		return;
        
    }
}

 ?>