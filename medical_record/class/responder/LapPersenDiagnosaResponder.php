<?php 


class LapPersenDiagnosaResponder extends DBResponder{
    
    public function excel(){
        $data=$this->dbtable->view("",0);
        $fix=$this->adapter->getContent($data['data']);
        
        require_once ("smis-libs-out/php-excel/PHPExcel.php");
        $file = new PHPExcel ();
        $file->getProperties ()->setCreator ( "PT Mahakam Intan Padi" );
        $file->getProperties ()->setLastModifiedBy ( "Rumah Sakit Bagian Administrator" );
        $file->getProperties ()->setTitle ( "Data Pegawai" );
        $file->getProperties ()->setSubject ( "Data Pegawai" );
        $file->getProperties ()->setDescription ( "Data Pegawai Generated From system" );
        $file->getProperties ()->setKeywords ( "Data Pegawai " );
        $file->getProperties ()->setCategory ( "Data Pegawai " );
        $sheet = $file->getActiveSheet ();
        $no=1;
        $sheet->setCellValue("A".$no,"No.");
        $sheet->setCellValue("B".$no,"Diagnosa");
        $sheet->setCellValue("C".$no,"Nama");    
        $sheet->setCellValue("D".$no,"ICD");    
        $sheet->setCellValue("E".$no,"Lali");    
        $sheet->setCellValue("F".$no,"Perempuan");    
        $sheet->setCellValue("G".$no,"Jumlah");
        $sheet->setCellValue("H".$no,"Persentase");    
            
        foreach($fix as $x){
            $no++;
            $sheet->setCellValue("A".$no,$x["No."]);
            $sheet->setCellValue("B".$no,$x["Diagnosa"]);
            $sheet->setCellValue("C".$no,$x["Nama"]);    
            $sheet->setCellValue("D".$no,$x["ICD"]);    
            $sheet->setCellValue("E".$no,$x["Laki"]);    
            $sheet->setCellValue("F".$no,$x["Perempuan"]);    
            $sheet->setCellValue("G".$no,$x["Jumlah"]);
            $sheet->setCellValue("H".$no,$x["Persentase"]);              
        }
        header ( 'Content-Type: application/vnd.ms-excel' );
        header ( 'Content-Disposition: attachment;filename="Laporan Persentase Diagnosa.xls"' );
        header ( 'Cache-Control: max-age=0' );
        $writer = PHPExcel_IOFactory::createWriter ( $file, 'Excel5' );
        $writer->save ( 'php://output' );
		return;
        
    }
    
}

?>