<?php

class RincianKunjIGDResponder extends DBResponder {
    public function excel() {
        $this->dbtable->setShowAll(true);
        $data=$this->dbtable->view("",0);
        $fix=$this->adapter->getContent($data['data']);
        
        require_once ("smis-libs-out/php-excel/PHPExcel.php");
        $file = new PHPExcel ();
        $file->getProperties ()->setCreator ( "PT. Inovasi Ide Utama" );
        $file->getProperties ()->setLastModifiedBy ( "Administrator" );
        $file->getProperties ()->setTitle ( "Rincian Kunjungan IGD" );
        $file->getProperties ()->setSubject ( "Rincian Kunjungan IGD" );
        $file->getProperties ()->setDescription ( "Rincian Kunjungan IGD Generated by System" );
        $file->getProperties ()->setKeywords ( "Rincian Kunjungan IGD" );
        $file->getProperties ()->setCategory ( "Rincian Kunjungan IGD" );
        
        $sheet = $file->getActiveSheet ();
        $i = 1;
        
        $objDrawing = new PHPExcel_Worksheet_Drawing();
        $objDrawing->setName('logo_img');
        $objDrawing->setDescription('logo_img');
        $objDrawing->setPath(getLogoNonInterlaced());
        $objDrawing->setCoordinates('A'.$i);
        $objDrawing->setOffsetX(20); 
        $objDrawing->setOffsetY(5);
        $objDrawing->setWidth(100); 
        $objDrawing->setHeight(100);
        $objDrawing->setWorksheet($sheet);
        $i++;
        
        $sheet->setCellValue("C".$i, getSettings($db,"smis_autonomous_title"));
        $sheet->getStyle("C".$i)->getFont()->setBold(true)->setSize(14);
        $i++;
        $sheet->setCellValue("C".$i, getSettings($db,"smis_autonomous_address"));
        $sheet->getStyle("C".$i)->getFont()->setBold(true)->setSize(14);
        $i++;
        $sheet->setCellValue("C".$i, getSettings($db,"smis_autonomous_contact"));
        $sheet->getStyle("C".$i)->getFont()->setBold(true)->setSize(14);
        $i += 3;
        $sheet->mergeCells("A".$i.":I".$i)->setCellValue("A".$i,"RINCIAN KUNJUNGAN IGD");
        $sheet->getStyle("A".$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle("A".$i)->getFont()->setBold(true)->setSize(14);
        $i++;
        $sheet->mergeCells("A".$i.":AD".$i)->setCellValue("A".$i, strtoupper(ArrayAdapter::dateFormat("date d M Y", $_POST['dari']))." - ".strtoupper(ArrayAdapter::dateFormat("date d M Y", $_POST['sampai'])));
        $sheet->getStyle("A".$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle("A".$i)->getFont()->setBold(true);
        $i += 3;
        
        $border_start = $i;
        $ipd = $i + 2;
        $sheet->mergeCells("A".$i.":A".$ipd)->setCellValue("A".$i, "NO.");
        $sheet->mergeCells("B".$i.":B".$ipd)->setCellValue("B".$i, "TANGGAL");
        $sheet->mergeCells("C".$i.":C".$ipd)->setCellValue("C".$i, "NRM");
        $sheet->mergeCells("D".$i.":D".$ipd)->setCellValue("D".$i, "NO. REG");
        $sheet->mergeCells("E".$i.":E".$ipd)->setCellValue("E".$i, "NO. PROFILE");
        $sheet->mergeCells("F".$i.":F".$ipd)->setCellValue("F".$i, "NAMA");
        $sheet->mergeCells("G".$i.":J".$i)->setCellValue("G".$i, "BEDAH");
        $sheet->mergeCells("K".$i.":N".$i)->setCellValue("K".$i, "MENINGGAL");
        $sheet->mergeCells("O".$i.":R".$i)->setCellValue("O".$i, "NON BEDAH");
        $sheet->mergeCells("S".$i.":V".$i)->setCellValue("S".$i, "MENINGGAL");
        $sheet->mergeCells("W".$i.":Z".$i)->setCellValue("X".$i, "KEBIDANAN");
        $sheet->mergeCells("AA".$i.":AD".$i)->setCellValue("AB".$i, "MENINGGAL");
        $sheet->getStyle("A".$i.":AE".$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle("A".$i.":AE".$i)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $sheet->getStyle("A".$i.":AE".$i)->getFont()->setBold(true);
        $i++;
        
        $sheet->mergeCells("G".$i.":H".$i)->setCellValue("A".$i, "RUJUKAN");
        $sheet->mergeCells("I".$i.":J".$i)->setCellValue("C".$i, "NON RUJUKAN");
        $sheet->mergeCells("K".$i.":L".$i)->setCellValue("E".$i, "DOA");
        $sheet->mergeCells("M".$i.":N".$i)->setCellValue("G".$i, "DOR");
        $sheet->mergeCells("O".$i.":P".$i)->setCellValue("I".$i, "RUJUKAN");
        $sheet->mergeCells("Q".$i.":R".$i)->setCellValue("K".$i, "NON RUJUKAN");
        $sheet->mergeCells("S".$i.":T".$i)->setCellValue("M".$i, "DOA");
        $sheet->mergeCells("U".$i.":V".$i)->setCellValue("O".$i, "DOR");
        $sheet->mergeCells("W".$i.":X".$i)->setCellValue("Q".$i, "RUJUKAN");
        $sheet->mergeCells("Y".$i.":Z".$i)->setCellValue("S".$i, "NON RUJUKAN");
        $sheet->mergeCells("AA".$i.":AB".$i)->setCellValue("U".$i, "DOA");
        $sheet->mergeCells("AC".$i.":AD".$i)->setCellValue("W".$i, "DOR");
        $sheet->getStyle("G".$i.":AD".$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle("G".$i.":AD".$i)->getFont()->setBold(true);
        $i++;
        
        $sheet->setCellValue("G".$i, "L");
        $sheet->setCellValue("H".$i, "P");
        $sheet->setCellValue("I".$i, "L");
        $sheet->setCellValue("J".$i, "P");
        $sheet->setCellValue("K".$i, "L");
        $sheet->setCellValue("L".$i, "P");
        $sheet->setCellValue("M".$i, "L");
        $sheet->setCellValue("N".$i, "P");
        $sheet->setCellValue("O".$i, "L");
        $sheet->setCellValue("P".$i, "P");
        $sheet->setCellValue("Q".$i, "L");
        $sheet->setCellValue("R".$i, "P");
        $sheet->setCellValue("S".$i, "L");
        $sheet->setCellValue("T".$i, "P");
        $sheet->setCellValue("U".$i, "L");
        $sheet->setCellValue("V".$i, "P");
        $sheet->setCellValue("W".$i, "L");
        $sheet->setCellValue("X".$i, "P");
        $sheet->setCellValue("Y".$i, "L");
        $sheet->setCellValue("Z".$i, "P");
        $sheet->setCellValue("AA".$i, "L");
        $sheet->setCellValue("AB".$i, "P");
        $sheet->setCellValue("AC".$i, "L");
        $sheet->setCellValue("AD".$i, "P");
        $sheet->getStyle("A".$i.":AD".$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle("A".$i.":AD".$i)->getFont()->setBold(true);
        $i++;
        
        for($a = 0; $a < sizeof($fix); $a++) {
            $sheet->setCellValue("A".$i, $fix[$a]['no']);
            $sheet->setCellValue("B".$i, $fix[$a]['tanggal']);
            $sheet->setCellValue("C".$i, $fix[$a]['nrm']);
            $sheet->setCellValue("D".$i, $fix[$a]['noreg']);
            $sheet->setCellValue("E".$i, $fix[$a]['profile_number']);
            $sheet->setCellValue("F".$i, $fix[$a]['nama_pasien']);
            $sheet->setCellValue("G".$i, $fix[$a]['bedah_rujukan_l']);
            $sheet->setCellValue("H".$i, $fix[$a]['bedah_rujukan_p']);
            $sheet->setCellValue("I".$i, $fix[$a]['bedah_nonrujukan_l']);
            $sheet->setCellValue("J".$i, $fix[$a]['bedah_nonrujukan_p']);
            $sheet->setCellValue("K".$i, $fix[$a]['bedah_doa_l']);
            $sheet->setCellValue("L".$i, $fix[$a]['bedah_doa_p']);
            $sheet->setCellValue("M".$i, $fix[$a]['bedah_dor_l']);
            $sheet->setCellValue("N".$i, $fix[$a]['bedah_dor_p']);
            $sheet->setCellValue("O".$i, $fix[$a]['nonbedah_rujukan_l']);
            $sheet->setCellValue("P".$i, $fix[$a]['nonbedah_rujukan_p']);
            $sheet->setCellValue("Q".$i, $fix[$a]['nonbedah_nonrujukan_l']);
            $sheet->setCellValue("R".$i, $fix[$a]['nonbedah_nonrujukan_p']);
            $sheet->setCellValue("S".$i, $fix[$a]['nonbedah_doa_l']);
            $sheet->setCellValue("T".$i, $fix[$a]['nonbedah_doa_p']);
            $sheet->setCellValue("U".$i, $fix[$a]['nonbedah_dor_l']);
            $sheet->setCellValue("V".$i, $fix[$a]['nonbedah_dor_p']);
            $sheet->setCellValue("W".$i, $fix[$a]['kebidanan_rujukan_l']);
            $sheet->setCellValue("X".$i, $fix[$a]['kebidanan_rujukan_p']);
            $sheet->setCellValue("Y".$i, $fix[$a]['kebidanan_nonrujukan_l']);
            $sheet->setCellValue("Z".$i, $fix[$a]['kebidanan_nonrujukan_p']);
            $sheet->setCellValue("AA".$i, $fix[$a]['kebidanan_doa_l']);
            $sheet->setCellValue("AB".$i, $fix[$a]['kebidanan_doa_p']);
            $sheet->setCellValue("AC".$i, $fix[$a]['kebidanan_dor_l']);
            $sheet->setCellValue("AD".$i, $fix[$a]['kebidanan_dor_p']);
            $sheet->getStyle("A".$i.":E".$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $sheet->getStyle("G".$i.":AD".$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $i++;
        }
        $border_end = $i - 1;
        $sheet->getStyle("A".$border_end.":AD".$border_end)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle("A".$border_end.":AD".$border_end)->getFont()->setBold(true);
        
        $thin = array ();
        $thin['borders']=array();
        $thin['borders']['allborders']=array();
        $thin['borders']['allborders']['style']=PHPExcel_Style_Border::BORDER_THIN ;
        $sheet  ->getStyle ( "A".$border_start.":AD".$border_end )->applyFromArray ($thin);
        
        $filename = "Rincian Kunjungan IGD ".strtoupper(ArrayAdapter::dateFormat("date d M Y", $_POST['dari']))." - ".strtoupper(ArrayAdapter::dateFormat("date d M Y", $_POST['sampai'])).".xls";
        
        header ( 'Content-Type: application/vnd.ms-excel' );
        header ( 'Content-Disposition: attachment;filename="'.$filename.'"' );
        header ( 'Cache-Control: max-age=0' );
        $writer = PHPExcel_IOFactory::createWriter ( $file, 'Excel5' );
        $writer->save ( 'php://output' );
		return;
    }
}

?>