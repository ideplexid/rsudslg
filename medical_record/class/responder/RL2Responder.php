<?php

class RL2Responder extends ServiceResponder {
    public function excel() {
        $this->addData("command","list");
        $this->getService()->setData($this->getData());
		$this->getService()->execute();
		$d=$this->getService()->getContent();
		$data=$d['data'];
        $fix = $this->getAdapter()->getContent($data);
        
        require_once ("smis-libs-out/php-excel/PHPExcel.php");
        $file = new PHPExcel ();
        $file->getProperties ()->setCreator ( "Rumah Sakit Syamrabu" );
        $file->getProperties ()->setLastModifiedBy ( "Rumah Sakit Bagian Administrator" );
        $file->getProperties ()->setTitle ( "Laporan RL 2 Kepegawaian" );
        $file->getProperties ()->setSubject ( "Laporan RL 2" );
        $file->getProperties ()->setDescription ( "Laporan RL 2 Kepegawaian Generated From system" );
        $file->getProperties ()->setKeywords ( "Laporan RL 2" );
        $file->getProperties ()->setCategory ( "Laporan RL 2" );
        $sheet = $file->getActiveSheet ();
        
        $no = 1;
        $sheet->setCellValue("A".$no,"No.");
        $sheet->setCellValue("B".$no,"PENDIDIKAN");
        $sheet->setCellValue("C".$no,"PRIA");
        $sheet->setCellValue("D".$no,"WANITA");
        $sheet->setCellValue("E".$no,"TOTAL");
        $sheet->getStyle('A'.$no.':'.'E'.$no)->getFont()->setBold(true);
        $sheet->getStyle('A'.$no.':'.'E'.$no)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        
        $length = sizeof($fix);
        foreach($fix as $x)
        {
            $no++;
            if($no == $length + 1)
            {
                $pend = explode("<strong>",$x["PENDIDIKAN"]);
                $pend1 = explode("</strong>",$pend[1]);
                
                $sheet->setCellValue("B".$no,$pend1[0]);
                $sheet->setCellValue("C".$no,$x['PRIA']);
                $sheet->setCellValue("D".$no,$x['WANITA']);
                $sheet->setCellValue("E".$no,$x['TOTAL']);
                
                $sheet->getStyle('A'.$no.':'.'E'.$no)->getFont()->setBold(true);
                $sheet->getStyle('A'.$no)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                $sheet->getStyle('B'.$no)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
                $sheet->getStyle('C'.$no.':'.'K'.$no)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            }
            else
            {
                $sheet->setCellValue("A".$no,$x["No."]);
                $sheet->setCellValue("B".$no,$x['PENDIDIKAN']);
                $sheet->setCellValue("C".$no,$x['PRIA']);
                $sheet->setCellValue("D".$no,$x['WANITA']);
                $sheet->setCellValue("E".$no,$x['TOTAL']);
                
                $sheet->getStyle('A'.$no)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                $sheet->getStyle('B'.$no)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
                $sheet->getStyle('C'.$no.':'.'E'.$no)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            }
        }
        
        foreach(range('A','E') as $columnID) {
            $sheet->getColumnDimension($columnID)->setAutoSize(true);
        }
        
        header ( 'Content-Type: application/vnd.ms-excel' );
        header ( 'Content-Disposition: attachment;filename="Laporan RL 2.xls"' );
        header ( 'Cache-Control: max-age=0' );
        $writer = PHPExcel_IOFactory::createWriter ( $file, 'Excel5' );
        $writer->save ( 'php://output' );
		return;
    }
}

?>