<?php

class RincianPulangRwtJalanNonIGDAdapter extends ArrayAdapter {
    private $no = 0;
    private $jumlah_baru_l = 0;
    private $jumlah_baru_p = 0;
    private $jumlah_lama_l = 0;
    private $jumlah_lama_p = 0;
    
    private $list = array();
    
    private $slug_igd;
    
    public function setSlugIGD($igd) {
        $this->slug_igd = $igd;
    }
    
    public function adapt($d) {
        if($d['jenislayanan'] != $this->slug_igd && $d['uri'] == "0" && $d['jenislayanan'] != 'hd_hemodialisa' && $d['jenislayanan'] != 'laboratory'  && $d['jenislayanan'] != 'radiology' && $d['jenislayanan'] != 'fisiotherapy' && $d['jenislayanan'] != 'elektromedis'  && $d['jenislayanan'] != 'bank_darah') {
            if($d['barulama'] == "0" && $d['kelamin'] == "0") {
                $this->no++;
                $array = array();
                $array['no']                = $this->no.".";
                $array['tanggal']           = ArrayAdapter::dateFormat("date d M Y", $d['tanggal']);
                $array['nrm']               = $d['nrm'];
                $array['noreg']             = $d['id'];
                $array['profile_number']    = $d['profile_number'];
                $array['nama_pasien']       = $d['sebutan']." ".$d['nama_pasien'];
                $array['ruangan']           = ArrayAdapter::slugFormat("unslug", $d['jenislayanan']);
                $array['baru_l'] = "1";
                $array['baru_p'] = "";
                $array['lama_l'] = "";
                $array['lama_p'] = "";
                $array['keterangan'] = "";
                $this->jumlah_baru_l++;
                $this->list[] = $array;
            } 
            if($d['barulama'] == "0" && $d['kelamin'] == "1") {
                $this->no++;
                $array = array();
                $array['no']                = $this->no.".";
                $array['tanggal']           = ArrayAdapter::dateFormat("date d M Y", $d['tanggal']);
                $array['nrm']               = $d['nrm'];
                $array['noreg']             = $d['id'];
                $array['profile_number']    = $d['profile_number'];
                $array['nama_pasien']       = $d['sebutan']." ".$d['nama_pasien'];
                $array['ruangan']           = ArrayAdapter::slugFormat("unslug", $d['jenislayanan']);
                $array['baru_l'] = "";
                $array['baru_p'] = "1";
                $array['lama_l'] = "";
                $array['lama_p'] = "";
                $array['keterangan'] = "";
                $this->jumlah_baru_p++;
                $this->list[] = $array;
            }
            if($d['barulama'] == "1" && $d['kelamin'] == "0") {
                $this->no++;
                $array = array();
                $array['no']                = $this->no.".";
                $array['tanggal']           = ArrayAdapter::dateFormat("date d M Y", $d['tanggal']);
                $array['nrm']               = $d['nrm'];
                $array['noreg']             = $d['id'];
                $array['profile_number']    = $d['profile_number'];
                $array['nama_pasien']       = $d['sebutan']." ".$d['nama_pasien'];
                $array['ruangan']           = ArrayAdapter::slugFormat("unslug", $d['jenislayanan']);
                $array['baru_l'] = "";
                $array['baru_p'] = "";
                $array['lama_l'] = "1";
                $array['lama_p'] = "";
                $array['keterangan'] = "";
                $this->jumlah_lama_l++;
                $this->list[] = $array;
            }
            if($d['barulama'] == "1" && $d['kelamin'] == "1") {
                $this->no++;
                $array = array();
                $array['no']                = $this->no.".";
                $array['tanggal']           = ArrayAdapter::dateFormat("date d M Y", $d['tanggal']);
                $array['nrm']               = $d['nrm'];
                $array['noreg']             = $d['id'];
                $array['profile_number']    = $d['profile_number'];
                $array['nama_pasien']       = $d['sebutan']." ".$d['nama_pasien'];
                $array['ruangan']           = ArrayAdapter::slugFormat("unslug", $d['jenislayanan']);
                $array['baru_l'] = "";
                $array['baru_p'] = "";
                $array['lama_l'] = "";
                $array['lama_p'] = "1";
                $array['keterangan'] = "";
                $this->jumlah_lama_p++;
                $this->list[] = $array;
            }
        }
    }
    
    public function getContent($data) {
        for($i=0; $i < sizeof($data); $i++) {
            $this->adapt($data[$i]);
        }
        
        $array                      = array();
        $array['no']                = "";
        $array['tanggal']           = "";
        $array['nrm']               = "";
        $array['noreg']             = "";
        $array['profile_number']    = "";
        $array['nama_pasien']       = "";
        $array['ruangan']           = "TOTAL";
        $array['baru_l']            = (string)$this->jumlah_baru_l;
        $array['baru_p']            = (string)$this->jumlah_baru_p;
        $array['lama_l']            = (string)$this->jumlah_lama_l;
        $array['lama_p']            = (string)$this->jumlah_lama_p;
        $array['keterangan']        = "";
        
        $this->list[]               = $array;
        return $this->list;
    }
}

?>