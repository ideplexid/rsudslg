<?php 

/**
 * @author goblooge
 * digunakan untuk mem-parsing perubahan data dari
 * service get_rl13, adapun rumusnya sebegai berikut
 * Service mengenai tempat tidur
 * 
 */

class Syncrl13Adapter extends ArrayAdapter {
	public function adapt($d) {
		$a = array ();
		$a ['jtt'] = $d ['JTT'];
		$a ['ruangan'] = $d ['RUANGAN'];
		$a ['layanan'] = $d ['LAYANAN'];
		$a ['kelas'] = $d ['KELAS'];
		return $a;
	}
}

?>