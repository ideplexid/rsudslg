<?php 

/**
 * @author goblooge
 * digunakan untuk mem-parsing perubahan data dari
 * service get_rl52b, untuk menghitung jenis kegiatan 
 * Rekam Medis
 * 
 */

class SyncRL52BAdapter extends ArrayAdapter {
	public function adapt($d) {
		$a = array ();
		$a ['layanan'] = $d ['rl52'];
		$a ['jumlah'] = $d ['total'];
		return $a;
	}
}



?>