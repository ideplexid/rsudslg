<?php 

function percentage_diagnosa_sort($a, $b){
    if ($a['total'] == $b['total']) {
        return 0;
    }
    return ($a['total'] > $b['total']) ? -1 : 1;
}

class LaporanPersentaseDiagnosaAdapter extends ArrayAdapter{
    
    private $content=array();
    private $total=0;
    
    public function adapt($x){
        if(!isset($this->content[$x->kode_icd])){
            $this->content[$x->kode_icd]=array();
            $this->content[$x->kode_icd]['name']=$x->nama_icd;
            $this->content[$x->kode_icd]['kode']=$x->kode_icd;
            $this->content[$x->kode_icd]['nama']=$x->sebab_sakit;
            $this->content[$x->kode_icd]['total']=$x->total;
            if($x->jk == '0') {
                $this->content[$x->kode_icd]['laki'] = $x->total;
            } else {
                $this->content[$x->kode_icd]['perempuan'] = $x->total;
            }
        }else{
            $this->content[$x->kode_icd]['total'] += $x->total;
            if($x->jk == '0') {
                $this->content[$x->kode_icd]['laki'] += $x->total;
            } else {
                $this->content[$x->kode_icd]['perempuan'] += $x->total;
            }
        }
        $this->total += $x->total;
    }
    
    public function refactoring($content){
        $result=array();
        $no=0;
        usort($content,"percentage_diagnosa_sort");
        foreach($content as $x=>$y){
            $one=array();
            $one['No.']=++$no.".";
            $one['Diagnosa']=$y['name'];
            $one['Nama']=$y['nama'];
            $one['Jumlah']=$y['total'];
            $one['Laki']=$y['laki'];
            $one['Perempuan']=$y['perempuan'];
            $one['Persentase']=round($y['total']*100/$this->total,2)." %";
            $one['ICD']=$y['kode'];
            $result[]=$one;
        }
        
        return $result;
    }
    
    public function getContent($data){
        parent::getContent($data);
        return $this->refactoring($this->content);
    }
    
}

?>