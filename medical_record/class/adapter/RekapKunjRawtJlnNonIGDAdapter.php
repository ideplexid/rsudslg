<?php

class RekapKunjRawtJlnNonIGDAdapter extends ArrayAdapter {
    private $no = 0;
    private $baru_l_total = 0;
    private $baru_p_total = 0;
    private $lama_l_total = 0;
    private $lama_p_total = 0;
    private $jumlah_l_total = 0;
    private $jumlah_p_total = 0;
    
    public function adapt($d) {
        $this->no++;
        $res = array();
        $res['no']          = $this->no;
        $res['ruangan']     = ArrayAdapter::slugFormat("unslug", $d['ruangan']);
        $res['baru_l']      = $d['baru_l'];
        $res['baru_p']      = $d['baru_p'];
        $res['lama_l']      = $d['lama_l'];
        $res['lama_p']      = $d['lama_p'];
        $res['jumlah_l']    = $d['jumlah_l'];
        $res['jumlah_p']    = $d['jumlah_p'];
        $res['keterangan']  = $d['keterangan'];
        
        $this->baru_l_total     = $this->baru_l_total + $d['baru_l'];
        $this->baru_p_total     = $this->baru_p_total + $d['baru_p'];
        $this->lama_l_total     = $this->lama_l_total + $d['lama_l'];
        $this->lama_p_total     = $this->lama_p_total + $d['lama_p'];
        $this->jumlah_l_total   = $this->jumlah_l_total + $d['jumlah_l'];
        $this->jumlah_p_total   = $this->jumlah_p_total + $d['jumlah_p'];
        
        return $res;
    }
    
    public function getContent($data) {
        $result             = array();
        foreach($data as $d => $key) {
            $result[]         = $this->adapt($key);
        }
        
        $res                = array();
        $res['no.']         = "";
        $res['ruangan']     = "TOTAL";
        $res['baru_l']      = $this->baru_l_total;
        $res['baru_p']      = $this->baru_p_total;
        $res['lama_l']      = $this->lama_l_total;
        $res['lama_p']      = $this->lama_p_total;
        $res['jumlah_l']    = $this->jumlah_l_total;
        $res['jumlah_p']    = $this->jumlah_p_total;
        $res['keterangan']  = "";
        
        $result[] = $res;
        return $result;
    }
}

?>