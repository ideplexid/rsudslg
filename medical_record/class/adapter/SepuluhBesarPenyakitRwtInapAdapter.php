<?php

class SepuluhBesarPenyakitRwtInapAdapter extends ArrayAdapter {
    public function adapt($d) {
        parent::adapt($d);
    }
    
    public function getContent($data) {
        $res = array();
        $no = 0;
        for($i = 0; $i < 20; $i++) {
            $no++;
            $res[$i]['No.']          = (string)$no;
            $res[$i]['Diagnosa']     = $data[$i]->nama_icd;
            $res[$i]['Kode ICD']     = $data[$i]->kode_icd;
            $res[$i]['Laki-Laki']    = $data[$i]->laki;
            $res[$i]['Perempuan']    = $data[$i]->perempuan;
            $res[$i]['Jumlah']       = $data[$i]->jumlah;
        }
        return $res;
    }
}

?>