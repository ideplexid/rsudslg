<?php

class LaporanAsesmenJantungAdapter extends SimpleAdapter {
    public function adapt($d) {
        $array=parent::adapt($d);
        
        $show=array();
        $show['Pasien']                                     = $d->nama_pasien;
        $show['Jenis Kelamin']                              = $d->jk==0?"Laki-Laki":"Perempuan";
        $show['Alamat']                                     = $d->alamat_pasien;
        $show['NRM']                                        = $d->nrm_pasien;
        $show['No. Reg']                                    = $d->noreg_pasien;
        $show['Tanggal Asesmen']                            = self::format("date d M Y", $d->waktu);
        $show['Tanggal Masuk']                              = self::format("date d M Y", $d->waktu_masuk);
        $show['Ruangan']                                    = self::format("unslug", $d->ruangan);
        $show['Cara Bayar']                                 = $d->carabayar;
        $show['Dokter']                                     = $d->nama_dokter;
        $show['Keluhan Utama']                              = $d->keluhan_utama;
        $show['Riwayat Penyakit Sekarang']                  = $d->riwayat_penyakit_skrg;
        $show['Riwayat Pengobatan']                         = $d->riwayat_pengobatan;
        $show['Torak']                                      = $d->torak;
        $show['Keterangan']                                 = $d->torak_ket;
        $show['Inspeksi (Iktus Kordis)']                    = $d->iktus_kordis;
        $show['Keterangan']                                 = $d->iktus_kordis_ket;
        $show['Lokasi']                                     = $d->iktus_kordis_lokasi;
        $show['Pulpasi']                                    = $d->pulpasi;
        $show['Keterangan']                                 = $d->pulpasi_ket;
        $show['Palpasi (Iktus Kordis)']                     = $d->palpasi_iktus_kordis;
        $show['Keterangan']                                 = $d->palpasi_iktus_kordis_ket;
        $show['Lokasi']                                     = $d->palpasi_iktus_kordis_lokasi;
        $show['Thrill']                                     = $d->palpasi_thrill;
        $show['Keterangan']                                 = $d->palpasi_thrill_ket;
        $show['Lokasi']                                     = $d->palpasi_thrill_lokasi;
        $show['Perkusi (Batas Atas)']                       = $d->perkusi_batas_atas;
        $show['Perkusi (Batas Bawah)']                      = $d->perkusi_batas_bawah;
        $show['Perkusi (Batas Kanan)']                      = $d->perkusi_batas_kanan;
        $show['Perkusi (Batas Kiri)']                       = $d->perkusi_batas_kiri;
        $show['Suara Jantung Utama']                        = $d->sju;
        $show['S1']                                         = $d->sju_s1;
        $show['S2']                                         = $d->sju_s2;
        $show['Extra Systole']                              = $d->extra_systole;
        $show['Gallop']                                     = $d->gallop;
        $show['Murmur (Fase)']                              = $d->murmur_fase;
        $show['Keterangan']                                 = $d->murmur_fase_ket;
        $show['Murmur (Lokasi)']                            = $d->murmur_lokasi;
        $show['Keterangan']                                 = $d->murmur_lokasi_ket;
        $show['Murmur (Kualitas)']                          = $d->murmur_kualitas;
        $show['Keterangan']                                 = $d->murmur_kualitas_ket;
        $show['Murmur (Grade)']                             = $d->murmur_grade;
        $show['Murmur (Penjalaran)']                        = $d->murmur_penjalaran;
        $show['Keterangan']                                 = $d->murmur_penjalaran_ket;
        $show['Murmur (Opening Snap)']                      = $d->murmur_opening_snap;
        $show['Murmur (Friction Rub)']                      = $d->murmur_friction_rub;
        $show['Paru']                                       = $d->paru;
        $show['Keterangan']                                 = $d->paru_ket;
        $show['ECG']                                        = $d->ecg;
        $show['Thorax Foto']                                = $d->thorax_foto;
        $show['Echocardiografi']                            = $d->echocardiografi;
        $show['Diagnosis']                                  = $d->diagnosis;
        $show['Terapi']                                     = $d->terapi;
        $show['Rencana Kerja']                              = $d->rencana_kerja;
        $show['Waktu Pulang']                               = self::format("date d M Y: H:i:s", $d->waktu_pulang);
        $show['Tanggal Kontrol Klinik']                     = self::format("date d M Y", $d->waktu_kontrol_klinik);
        $show['Dirawat Di Ruang']                           = $d->dirawat_diruang;
        
        $result="<div class='hide' id='laporan_asesmen_jantung_".$d->id."'>".self::format("array-bold-trim", $show)."</div>";
        $array['Pasien']=$array['Pasien'].$result;
        
        return $array;
    }
}

?>