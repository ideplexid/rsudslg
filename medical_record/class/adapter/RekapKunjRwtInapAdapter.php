<?php

require_once "medical_record/function/date_range.php";

class RekapKunjRwtInapAdapter extends ArrayAdapter {
    private $dari;
    private $sampai;
    private $jumlah_tt                  = 0;
    private $jumlah_masuk_l             = 0;
    private $jumlah_masuk_p             = 0;
    private $jumlah_keluar_hidup_l      = 0;
    private $jumlah_keluar_hidup_p      = 0;
    private $jumlah_keluar_mati_l       = 0;
    private $jumlah_keluar_mati_p       = 0;
    private $jumlah_mati_k48_l          = 0;
    private $jumlah_mati_k48_p          = 0;
    private $jumlah_mati_l48_l          = 0;
    private $jumlah_mati_l48_p          = 0;
    private $jumlah_mati_k1jop_l        = 0;
    private $jumlah_mati_k1jop_p        = 0;
    private $jumlah_lama_dirawat        = 0;
    private $jumlah_hari_perawatan      = 0;
    
    public function setDariSampai($d, $s) {
        $this->dari = $d;
        $this->sampai = $s;
    }
    
    public function setJumlahTT($tt) {
        $this->jumlah_tt = $tt;
    }
    
    public function setJumlahHariPerawatan($jumlah) {
        $this->jumlah_hari_perawatan = $jumlah;
    }
    
    public function adapt($d) {
        parent::adapt($d);
    }
    
    public function getContent($data) {
        /************ Get range tanggal berdasarkan filter dari, sampai ************/
        $date_range = dateRange($this->dari, $this->sampai);
        $rincian_tgl = array();
        for($i=0; $i<sizeof($date_range); $i++) {
            $rincian_tgl[$i] = $date_range[$i];
        }
        /************  ************/
        
        $jumlah_hari_perawatan      = 0;
        $jumlah_masuk_l             = 0;
        $jumlah_masuk_p             = 0;
        $jumlah_keluar_hidup_l      = 0;
        $jumlah_keluar_hidup_p      = 0;
        $jumlah_keluar_mati_l       = 0;
        $jumlah_keluar_mati_p       = 0;
        $jumlah_mati_k48_l          = 0;
        $jumlah_mati_k48_p          = 0;
        $jumlah_mati_l48_l          = 0;
        $jumlah_mati_l48_p          = 0;
        $jumlah_mati_k1jop_l        = 0;
        $jumlah_mati_k1jop_p        = 0;
        $jumlah_lama_dirawat        = 0;
        
        for($i=0; $i<sizeof($rincian_tgl); $i++) {
            for($j=0; $j<sizeof($data); $j++) {
                $tgl_inap = explode(" ", $data[$j]['tanggal_inap']);
                $tanggal_inap = $tgl_inap[0];
                $tgl_pulang = explode(" ", $data[$j]['tanggal_pulang']);
                $tanggal_pulang = $tgl_pulang[0];
                $tgl_masuk = explode(" ", $data[$j]['tanggal']);
                $tanggal_masuk = $tgl_masuk[0];

                if($rincian_tgl[$i] >= $tanggal_inap && $rincian_tgl[$i] <= $tanggal_pulang) {
                    $jumlah_hari_perawatan ++;
                }
                if($tanggal_inap == $rincian_tgl[$i] && $data[$j]['kelamin'] == "0") {
                    $jumlah_masuk_l ++;
                }
                if($tanggal_inap == $rincian_tgl[$i] && $data[$j]['kelamin'] == "1") {
                    $jumlah_masuk_p ++;
                }
                if($tanggal_pulang == $rincian_tgl[$i] && $data[$j]['carapulang'] == "Dipulangkan Hidup" && $data[$j]['kelamin'] == "0") {
                    $jumlah_keluar_hidup_l ++;
                }
                if($tanggal_pulang == $rincian_tgl[$i] && $data[$j]['carapulang'] == "Dipulangkan Hidup" && $data[$j]['kelamin'] == "1") {
                    $jumlah_keluar_hidup_p ++;
                }
                if($tanggal_pulang == $rincian_tgl[$i] && $data[$j]['carapulang'] == "Dipulangkan Mati <=48 Jam" && $data[$j]['kelamin'] == "0") {
                    $jumlah_mati_k48_l ++;
                }
                if($tanggal_pulang == $rincian_tgl[$i] && $data[$j]['carapulang'] == "Dipulangkan Mati <=48 Jam" && $data[$j]['kelamin'] == "1") {
                    $jumlah_mati_k48_p ++;
                }
                if($tanggal_pulang == $rincian_tgl[$i] && $data[$j]['carapulang'] == "Dipulangkan Mati >48 Jam" && $data[$j]['kelamin'] == "0") {
                    $jumlah_mati_l48_l ++;
                }
                if($tanggal_pulang == $rincian_tgl[$i] && $data[$j]['carapulang'] == "Dipulangkan Mati >48 Jam" && $data[$j]['kelamin'] == "1") {
                    $jumlah_mati_l48_p ++;
                }
                if($tanggal_pulang == $rincian_tgl[$i] && $data[$j]['carapulang'] == "Dipulangkan Mati < 1 Jam Post Operasi" && $data[$j]['kelamin'] == "0") {
                    $jumlah_mati_k1jop_l ++;
                }
                if($tanggal_pulang == $rincian_tgl[$i] && $data[$j]['carapulang'] == "Dipulangkan Mati < 1 Jam Post Operasi" && $data[$j]['kelamin'] == "1") {
                    $jumlah_mati_k1jop_p ++;
                }
            }
        }
        
        $sampai = str_replace('-', '/', $this->sampai);
        $tgl_sampai = date('Y-m-d',strtotime($sampai . "+1 days"));
        for($j=0; $j<sizeof($data); $j++) {
            if($data[$j]['tanggal_inap'] != "0000-00-00 00:00:00" && $data[$j]['tanggal_pulang'] != "0000-00-00 00:00:00" && $data[$j]['tanggal_pulang'] <= $tgl_sampai) {
                $tgl_msk  = new DateTime(date('Y-m-d', strtotime($data[$j]['tanggal_inap'])));
                $tgl_plg = new DateTime(date('Y-m-d', strtotime($data[$j]['tanggal_pulang'])));
                $lama_rawat = $tgl_msk->diff($tgl_plg)->days;
                if($lama_rawat == 0 || $lama_rawat == '0') {
                    $jumlah_lama_dirawat += 1;
                } else {
                    $jumlah_lama_dirawat += $lama_rawat;
                }
            }
        }
        
        $res                                = array();
        $res[0]['jumlah_tt']                = (string)$this->jumlah_tt;
        $res[0]['masuk_l']                  = (string)$jumlah_masuk_l;
        $res[0]['masuk_p']                  = (string)$jumlah_masuk_p;
        $res[0]['keluar_hidup_l']           = (string)$jumlah_keluar_hidup_l;
        $res[0]['keluar_hidup_p']           = (string)$jumlah_keluar_hidup_p;
        $jumlah_keluar_mati_l               = $jumlah_mati_k48_l + $jumlah_mati_l48_l + $jumlah_mati_k1jop_l;
        $res[0]['keluar_mati_l']            = (string)$jumlah_keluar_mati_l;
        $jumlah_keluar_mati_p               = $jumlah_mati_k48_p + $jumlah_mati_l48_p + $jumlah_mati_k1jop_p;
        $res[0]['keluar_mati_p']            = (string)$jumlah_keluar_mati_p;
        $res[0]['mati_k48_l']               = (string)$jumlah_mati_k48_l;
        $res[0]['mati_k48_p']               = (string)$jumlah_mati_k48_p;
        $res[0]['mati_l48_l']               = (string)$jumlah_mati_l48_l;
        $res[0]['mati_l48_p']               = (string)$jumlah_mati_l48_p;
        $res[0]['jumlah_lama_dirawat']      = (string)$jumlah_lama_dirawat;
        $res[0]['jumlah_hari_perawatan']    = (string)$jumlah_hari_perawatan;
        
        return $res;
    }
}

?>