<?php
class MorbiditasAdapter extends ArrayAdapter {
	private $baru_laki=0;
	private $baru_pr=0;
	private $lama_laki=0;
	private $lama_pr=0;
	private $baru_kasus=0;
	private $lama_kasus=0;
	private $total_carabayar=array();
	private $carabayar;
    
    public function setCarabayar($carabayar){
        $this->carabayar=$carabayar;
        return $this;
    }
	
	public function adapt($onerow) {
		$this->number ++;
		$array = array ();
		$array ['id'] = $onerow->id;
		$array ['No'] = $this->number.".";
		$array ['DTD'] = $onerow->dtd;
		$array ['Diagnosa'] = $onerow->diagnosa;
		
		if($onerow->kunjungan == "Baru"){
			if($onerow->jk=="0") $this->baru_laki++;
			else $this->baru_pr++;
		}else{
			if($onerow->jk=="0") $this->lama_laki++;
			else $this->lama_pr++;
		}
		
		if($onerow->kasus=="Baru"){
			$this->baru_kasus++;
		}else{
			$this->lama_kasus++;
		}
		
		$array ['L'] = $onerow->kunjungan == "Baru" ? ($onerow->jk == "0" ? "x" : "") : "";
		$array ['P'] = $onerow->kunjungan == "Baru" ? ($onerow->jk == "1" ? "x" : "") : "";
		$array ['L1'] = $onerow->kunjungan == "Lama" ? ($onerow->jk == "0" ? "x" : "") : "";
		$array ['P1'] = $onerow->kunjungan == "Lama" ? ($onerow->jk == "1" ? "x" : "") : "";
		$array ['Baru'] = $onerow->kasus == "Baru" ? "x" : "";
		$array ['Lama'] = $onerow->kasus == "Lama" ? "x" : "";
        foreach($this->carabayar as $x=>$y){
            $array [$y] =  "";
            if(!isset($this->total_carabayar[$y])){
                $this->total_carabayar[$y]=0;
            }
            if($onerow->carabayar == $x){
                $array [$y] =  "x";
                $this->total_carabayar[$y]++;    
            } 
        }
		return $array;
	}
	
	public function getContent($data){
		$result=parent::getContent($data);
		$one=array();
		$one['Diagnosa']="Total";
		$one['L']=$this->baru_laki;
		$one['P']=$this->baru_pr;
		$one['L1']=$this->lama_laki;
		$one['P1']=$this->lama_pr;
		$one['Baru']=$this->baru_kasus;
		$one['Lama']=$this->lama_kasus;
        foreach($this->carabayar as $x=>$y){
            $one[$y]=$this->total_carabayar[$y];
        }
		$result[]=$one;
		return $result;
	}
}
?>