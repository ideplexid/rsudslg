<?php

class LaporanPPITotalAdapter extends ArrayAdapter {
    private $data_jenis_perawatan;
    private $data_infeksi;
    private $data_ruangan;
    
    public function setDataJenisPerawatan($data) {
        $this->data_jenis_perawatan = $data;
    }
    
    public function setDataInfeksi($data) {
        $this->data_infeksi = $data;
    }
    
    public function setDataRuangan($ruangan) {
        $this->data_ruangan = $ruangan;
    }
    
    public function adapt($d) {
        parent::adapt($d);
    }
    
    public function getContent() {
        $arr = array();
        for($i = 0; $i < sizeof($this->data_ruangan); $i++) {
            for($j = 0; $j < sizeof($this->data_jenis_perawatan); $j++) {
                if($this->data_ruangan[$i]['value'] == $this->data_jenis_perawatan[$j]->ruangan) {
                    $arr[$this->data_ruangan[$i]['name']]['ivl']                 = $this->data_jenis_perawatan[$j]->ivl;
                    $arr[$this->data_ruangan[$i]['name']]['cvl']                 = $this->data_jenis_perawatan[$j]->cvl;
                    $arr[$this->data_ruangan[$i]['name']]['uc']                  = $this->data_jenis_perawatan[$j]->uc;
                    $arr[$this->data_ruangan[$i]['name']]['ett']                 = $this->data_jenis_perawatan[$j]->ett;
                    $arr[$this->data_ruangan[$i]['name']]['pasien_tirah_baring'] = $this->data_jenis_perawatan[$j]->pasien_tirah_baring;
                    $arr[$this->data_ruangan[$i]['name']]['luka_bersih']         = $this->data_jenis_perawatan[$j]->luka_bersih;
                    $arr[$this->data_ruangan[$i]['name']]['luka_kotor']          = $this->data_jenis_perawatan[$j]->luka_kotor;
                }
            }
            for($j = 0; $j < sizeof($this->data_infeksi); $j++) {
                if($this->data_ruangan[$i]['value'] == $this->data_infeksi[$j]->ruangan) {
                    $arr[$this->data_ruangan[$i]['name']]['iadp']       = $this->data_infeksi[$j]->iadp;
                    $arr[$this->data_ruangan[$i]['name']]['vap']        = $this->data_infeksi[$j]->vap;
                    $arr[$this->data_ruangan[$i]['name']]['isk']        = $this->data_infeksi[$j]->isk;
                    $arr[$this->data_ruangan[$i]['name']]['plebitis']   = $this->data_infeksi[$j]->plebitis;
                    $arr[$this->data_ruangan[$i]['name']]['decubitus']  = $this->data_infeksi[$j]->decubitus;
                }
            }
        }
        
        $res = array();
        $i = 0;
        $no = 0;
        foreach($arr as $a => $b) {
            $no++;
            
            $res[$i]['no']                  = (string)$no;
            $res[$i]['ruangan']             = (string)$a;
            $res[$i]['ivl']                 = (string)$b['ivl'];
            $res[$i]['cvl']                 = (string)$b['cvl'];
            $res[$i]['uc']                  = (string)$b['uc'];
            $res[$i]['ett']                 = (string)$b['ett'];
            $res[$i]['pasien_tirah_baring'] = (string)$b['pasien_tirah_baring'];
            
            $res[$i]['iadp']             = (string)$b['iadp'];
            $res[$i]['vap']              = (string)$b['vap'];
            $res[$i]['isk']              = (string)$b['isk'];
            $res[$i]['plebitis']         = (string)$b['plebitis'];
            $res[$i]['decubitus']        = (string)$b['decubitus'];
            
            $iadp                           = ($b['iadp'] / $b['cvl']) * 100;
            $res[$i]['ir_iadp']             = (string)$iadp;
            $vap                            = ($b['vap'] / $b['ett']) * 100;
            $res[$i]['ir_vap']              = (string)$vap;
            $isk                            = ($b['isk'] / $b['uc']) * 100;
            $res[$i]['ir_isk']              = (string)$isk;
            $plebitis                       = ($b['plebitis'] / $b['ivl']) * 100;
            $res[$i]['ir_plebitis']         = (string)$plebitis;
            $decubitus                      = ($b['decubitus'] / $b['pasien_tirah_baring']) * 100;
            $res[$i]['ir_decubitus']        = (string)$decubitus;
            
            $i++;
        }
        
        return $res;
    }
}

?>