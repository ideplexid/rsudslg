<?php

class RadAdapter extends SimpleAdapter{
	
	public function adapt($d){
		$array=parent::adapt($d);
		$periksa=json_decode($d['periksa'],true);
		$hperiksa=json_decode($d['hasil'],true);
		
		//global $querylog;
		//$querylog->
		
		$hasil="<div class='hide' id='hasil_rad_".$d['id']."'>";
			foreach($periksa as $key=>$val){
				if($val=="1"){
					$hasil.="<div class='clear d_indent'>".$hperiksa[$key]."</div>";
					$hasil.="<strong> - - - - - -  - - - - -  - - - - - - - - - - - - - - - - - - -  - - - - -  - - - - - - </strong>";
				}
			}		
		$hasil.="</div>";
		$btn=new Button("", "", "Hasil");
		$btn->setClass("btn-primary");
		$btn->setAction("rad.hasil('".$d['id']."')");
		$btn->setIsButton(Button::$ICONIC);
		$btn->setIcon("fa fa-expand");
		$array['Hasil']=$btn->getHtml().$hasil;
		return $array;
	}
	
}

?>