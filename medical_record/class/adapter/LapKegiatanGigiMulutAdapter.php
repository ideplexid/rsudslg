<?php 

class LapKegiatanGigiMulutAdapter extends ArrayAdapter{
    
    private $tumpatan_gigi_tetap        = array("Layanan"=>"Tumpatan Gigi Tetap","No."=>1);
    private $tumpatan_gigi_sulung       = array("Layanan"=>"Tumpatan Gigi Sulung","No."=>2);
    private $pengobatan_pulpa           = array("Layanan"=>"Pengobatan Pulpa","No."=>3);
    private $pencabutan_gigi_tetap      = array("Layanan"=>"Pencabutan Gigi Tetap","No."=>4);
    private $pencabutan_gigi_sulung     = array("Layanan"=>"Pencabutan Gigi Sulung","No."=>5);
    private $pengobatan_periodontal     = array("Layanan"=>"Pengobatan Periodontal","No."=>6);
    private $pengobatan_abses           = array("Layanan"=>"Pengobatan Abses","No."=>7);
    private $karang_gigi                = array("Layanan"=>"Karang Gigi","No."=>8);
    private $prothese_lengkap           = array("Layanan"=>"Karang Prothese Lengkap","No."=>9);
    private $prothese_sebagian          = array("Layanan"=>"Karang Prothese Sebagian","No."=>10);
    private $prothese_cekat             = array("Layanan"=>"Karang Prothese Cekat","No."=>11);
    private $orthodonti                 = array("Layanan"=>"Orthodonti","No."=>12);
    private $bedah_mulut                = array("Layanan"=>"Bedah Mulut","No."=>13);
    
    public function adapt($x){
        $baru_lama=$x->baru_lama=="1";
        $this->count($this->tumpatan_gigi_tetap,$x->tumpatan_gigi_tetap=="1",$baru_lama,$x->carabayar);
        $this->count($this->tumpatan_gigi_sulung,$x->tumpatan_gigi_sulung=="1",$baru_lama,$x->carabayar);
        $this->count($this->pengobatan_pulpa,$x->pengobatan_pulpa=="1",$baru_lama,$x->carabayar);
        $this->count($this->pencabutan_gigi_tetap,$x->pencabutan_gigi_tetap!="0" &&  $x->pencabutan_gigi_tetap!="",$baru_lama,$x->carabayar);
        $this->count($this->pencabutan_gigi_sulung,$x->pencabutan_gigi_sulung!="0" &&  $x->pencabutan_gigi_tetap!="",$baru_lama,$x->carabayar);
        $this->count($this->pengobatan_periodontal,$x->pengobatan_periodontal!="0" &&  $x->pengobatan_periodontal!="",$baru_lama,$x->carabayar);
        $this->count($this->pengobatan_abses,$x->pengobatan_abses!="0" &&  $x->pengobatan_abses!="",$baru_lama,$x->carabayar);
        $this->count($this->karang_gigi,$x->karang_gigi=="1",$baru_lama,$x->carabayar);
        $this->count($this->prothese_sebagian,$x->prothese_sebagian=="1",$baru_lama,$x->carabayar);
        $this->count($this->prothese_lengkap,$x->prothese_lengkap=="1",$baru_lama,$x->carabayar);
        $this->count($this->prothese_cekat,$x->prothese_cekat=="1",$baru_lama,$x->carabayar);
        $this->count($this->orthodonti,$x->orthodonti!="0" &&  $x->orthodonti!="",$baru_lama,$x->carabayar);
        $this->count($this->bedah_mulut,$x->bedah_mulut!="0" &&  $x->bedah_mulut!="",$baru_lama,$x->carabayar);
        return null;
    }
    
    private function summary(&$content){
        $hasil=array("No."=>"","Layanan"=>"<strong>Total</strong>");
        foreach($content as $x){
            foreach($x as $name=>$value){
                if($name!="Layanan" && $name!="No."){
                    if(!isset($hasil[$name])){
                        $hasil[$name]=0;
                    }
                    $hasil[$name]+=$value;
                }
            }
        }
        $content[]=$hasil;
    }
    
    public function getContent($data){
        parent::getContent($data);
        $content=array();
        $content[]=$this->tumpatan_gigi_tetap;
        $content[]=$this->tumpatan_gigi_sulung;
        $content[]=$this->pengobatan_pulpa;
        $content[]=$this->pencabutan_gigi_tetap;
        $content[]=$this->pencabutan_gigi_sulung;
        $content[]=$this->pengobatan_periodontal;
        $content[]=$this->pengobatan_abses;
        $content[]=$this->karang_gigi;
        $content[]=$this->prothese_lengkap;
        $content[]=$this->prothese_sebagian;
        $content[]=$this->prothese_cekat;
        $content[]=$this->orthodonti;
        $content[]=$this->bedah_mulut;
        $this->summary($content);
        return $content;
    } 
    
    private function count(Array &$coloni,$value,$is_baru,$carabayar){
        if(!$value){
            return;
        }
        if($is_baru){
            if(!isset($coloni['Baru '.$carabayar])){
                $coloni['Baru '.$carabayar]=0;
            }
            $coloni['Baru '.$carabayar]++;
        }else{
            if(!isset($coloni['Lama '.$carabayar])){
                $coloni['Lama '.$carabayar]=0;
            }
            $coloni['Lama '.$carabayar]++;
        }
        if(!isset($coloni['Jumlah '.$carabayar])){
            $coloni['Jumlah '.$carabayar]=0;
        }
        $coloni['Jumlah '.$carabayar]++;
        
        if(!isset($coloni['Jumlah'])){
            $coloni['Jumlah']=0;
        }
        $coloni['Jumlah']++;
    }
    
    
    
}

?>