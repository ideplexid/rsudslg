<?php

class LapKegiatanGigiMulutExcelAdapter extends ArrayAdapter{
    private $content = array();
    private $total = 0;
    
    public function adapt($x){
        if(!isset($this->content[$x->tanggal])){
            $this->content[$x->tanggal] = array();
            $this->content[$x->tanggal]['tanggal'] = $x->tanggal;
            $this->content[$x->tanggal]['tumpatan_gigi_tetap'] = $x->tumpatan_gigi_tetap;
            $this->content[$x->tanggal]['tumpatan_gigi_sulung'] = $x->tumpatan_gigi_sulung;
            $this->content[$x->tanggal]['pengobatan_pulpa'] = $x->pengobatan_pulpa;
            $this->content[$x->tanggal]['pencabutan_gigi_tetap'] = (int)$x->pencabutan_gigi_tetap;
            $this->content[$x->tanggal]['pencabutan_gigi_sulung'] = (int)$x->pencabutan_gigi_sulung;
            $this->content[$x->tanggal]['pengobatan_periodontal'] = (int)$x->pengobatan_periodontal;
            $this->content[$x->tanggal]['pengobatan_abses'] = (int)$x->pengobatan_abses;
            $this->content[$x->tanggal]['pembersihan_karang_gigi'] = $x->pembersihan_karang_gigi;
            $this->content[$x->tanggal]['prothese_lengkap'] = $x->prothese_lengkap;
            $this->content[$x->tanggal]['prothese_sebagian'] = $x->prothese_sebagian;
            $this->content[$x->tanggal]['prothese_cekat'] = $x->prothese_cekat;
            $this->content[$x->tanggal]['orthodonti'] = (int)$x->orthodonti;
            $this->content[$x->tanggal]['jacket_bridge'] = $x->jacket_bridge;
            $this->content[$x->tanggal]['bedah_mulut'] = (int)$x->bedah_mulut;
        }
        else{
            $this->content[$x->tanggal]['total']+=$x->total;
        }
        $this->total+=$x->total;
    }
    
    public function getContent($data){
        parent::getContent($data);
        return $this->content;
    }
}

?>