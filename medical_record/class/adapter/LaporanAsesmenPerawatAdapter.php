<?php

class LaporanAsesmenPerawatAdapter extends SimpleAdapter {
    public function adapt($d) {
        $array=parent::adapt($d);
        
        $show=array();
        $show['Pasien']                                     = $d->nama_pasien;
        $show['Jenis Kelamin']                              = $d->jk==0?"Laki-Laki":"Perempuan";
        $show['Alamat']                                     = $d->alamat_pasien;
        $show['NRM']                                        = $d->nrm_pasien;
        $show['No. Reg']                                    = $d->noreg_pasien;
        $show['Tanggal Asesmen']                            = self::format("date d M Y", $d->waktu);
        $show['Tanggal Masuk']                              = self::format("date d M Y", $d->waktu_masuk);
        $show['Ruangan']                                    = self::format("unslug", $d->ruangan);
        $show['Cara Bayar']                                 = $d->carabayar;
        $show['Dokter']                                     = $d->nama_dokter;
        //$show['Dokter DPJP']                                = $d->nama_dokter_dpjp;
        $show['Perawat']                                    = $d->nama_perawat;
        $show['Diagnosa Perawat']                           = $d->diagnosa_perawat;
        $show['Berat Badan']                                = $d->bb. " kg";
        $show['Tinggi Badan']                               = $d->tb. " cm";
        $show['Rujukan']                                    = $d->rujukan;
        $show['Kedatangan']                                 = $d->kedatangan;
        $show['Tekanan Darah']                              = $d->tekanan_darah;
        $show['Frek Nafas']                                 = $d->frek_nafas;
        $show['Nadi']                                       = $d->nadi;
        $show['Suhu']                                       = $d->suhu;
        $show['Sumber Data']                                = $d->rkpr_sumber_data;
        $show['Sumber Data Lain']                           = $d->rkpr_sumber_data_lain;
        $show['Keluhan Utama']                              = $d->rkpr_keluhan_utama;
        $show['Keadaan Umum']                               = $d->rkpr_keadaan_umum;
        $show['Tindakan Resusitasi']                        = $d->rkpr_tindakan_resusitasi;
        $show['Tindakan Resusitasi Lain']                   = $d->rkpr_tindakan_resusitasi_lain;
        $show['Saturasi O2']                                = $d->rkpr_saturasi_o2;
        $show['Saturasi O2 pada']                           = $d->rkpr_pada;
        $show['Status Pernikahan']                          = $d->psks_status_nikah;
        $show['Anak']                                       = $d->psks_anak;
        $show['Jumlah Anak']                                = $d->psks_jumlah_anak;
        $show['Pendidikan Terakhir']                        = $d->psks_pend_akhir;
        $show['Warga Negara']                               = $d->psks_warganegara;
        $show['Pekerjaan']                                  = $d->psks_pekerjaan;
        $show['Agama']                                      = $d->psks_agama;
        $show['Tinggal Bersama']                            = $d->psks_tinggal_bersama;
        $show['Nama']                                       = $d->psks_tinggal_bersama_nama;
        $show['Telepon']                                    = $d->psks_tinggal_bersama_telp;
        $show['Hipertensi']                                 = $d->rks_riwayat_penyakit_hipertensi==0?"Tidak Ada":"Ada";
        $show['Diabetes']                                   = $d->rks_riwayat_penyakit_diabetes==0?"Tidak Ada":"Ada";
        $show['Jantung']                                    = $d->rks_riwayat_penyakit_jantung==0?"Tidak Ada":"Ada";
        $show['Stroke']                                     = $d->rks_riwayat_penyakit_stroke==0?"Tidak Ada":"Ada";
        $show['Dialisis']                                   = $d->rks_riwayat_penyakit_dialisis==0?"Tidak Ada":"Ada";
        $show['Ashtma']                                     = $d->rks_riwayat_penyakit_asthma==0?"Tidak Ada":"Ada";
        $show['Kejang']                                     = $d->rks_riwayat_penyakit_kejang==0?"Tidak Ada":"Ada";
        $show['Hati']                                       = $d->rks_riwayat_penyakit_hati==0?"Tidak Ada":"Ada";
        $show['Kanker']                                     = $d->rks_riwayat_penyakit_kanker==0?"Tidak Ada":"Ada";
        $show['TBC']                                        = $d->rks_riwayat_penyakit_tbc==0?"Tidak Ada":"Ada";
        $show['Glaukoma']                                   = $d->rks_riwayat_penyakit_glaukoma==0?"Tidak Ada":"Ada";
        $show['STD']                                        = $d->rks_riwayat_penyakit_std==0?"Tidak Ada":"Ada";
        $show['Pendarahan']                                 = $d->rks_riwayat_penyakit_pendarahan==0?"Tidak Ada":"Ada";
        $show['Riwayat penyakit Lain']                      = $d->rks_riwayat_penyakit_lain_lain;
        $show['Pengobatan Saat Ini']                        = $d->rks_pengobatan_saat_ini;
        $show['Keterangan']                                 = $d->rks_pengobatan_saat_ini_ket;
        $show['Riwayat Operasi']                            = $d->rks_riwayat_operasi;
        $show['Keterangan']                                 = $d->rks_riwayat_operasi_ket;
        $show['Riwayat Transfusi']                          = $d->rks_riwayat_transfusi;
        $show['Reaksi Transfusi']                           = $d->rks_reaksi_transfusi;
        $show['Keterangan']                                 = $d->rks_reaksi_transfusi_ket;
        $show['Riwayat Penyakit Keluarga']                  = $d->rks_riwayat_penyakit_keluarga;
        $show['Ketergantungan']                             = $d->rks_ketergantungan;
        $show['Ketergantungan Obat']                        = $d->rks_obat;
        $show['Jenis dan Jumlah Perhari']                   = $d->rks_jenis_jmlh_perhari;
        $show['Makan x/hari']                               = $d->nutrisi_makan_perhari;
        $show['Minum ml/hari']                              = $d->nutrisi_minum_perhari;
        $show['Diet Khusus']                                = $d->nutrisi_diet_khusus;
        $show['Keluhan']                                    = $d->nutrisi_keluhan;
        $show['Keterangan']                                 = $d->nutrisi_keluhan_ket;
        $show['Perubahan BB 6 Bln Akhir']                   = $d->nutrisi_perubahan_bb;
        $show['Penurunan']                                  = $d->nutrisi_perubahan_bb_penurunan;
        $show['Kenaikan']                                   = $d->nutrisi_perubahan_bb_kenaikan;
        $show['Alergi']                                     = $d->alergi;
        $show['Obat, Tipe Reaksi']                          = $d->alergi_obat_tipereaksi;
        $show['Makanan, Tipe Reaksi']                       = $d->alergi_makanan_tipereaksi;
        $show['Lain-lain, Tipe Reaksi']                     = $d->alergi_lain_tipereaksi;
        $show['Nyeri']                                      = $d->nyeri;
        $show['Sifat Nyeri']                                = $d->sifat_nyeri;
        $show['Lokasi Nyeri']                               = $d->nyeri_lokasi;
        $show['Sejak']                                      = $d->nyeri_sejak;
        $show['Skala Nyeri']                                = $d->skala_nyeri;
        $show['Cara Berjalan Membungkuk']                   = $d->get_up_go_test_jalan_bungkuk==0?"Tidak":"Ya";
        $show['Menopang Saat Akan Duduk']                   = $d->get_up_go_test_topang_duduk==0?"Tidak":"Ya";
        $show['Get Up and Go Test']                         = $d->get_up_go_test;
        $show['Bicara']                                     = $d->bicara;
        $show['Perlu Penerjemah']                           = $d->penerjemah;
        $show['Bahasa']                                     = $d->bahasa;
        $show['Bahasa Isyarat']                             = $d->bhs_isyarat;
        $show['Hambatan Belajar']                           = $d->hambatan_belajar;
        $show['Keterangan']                                 = $d->hambatan_belajar_ket;
        $show['Catatan Lain']                               = $d->catatan_lain;
        
        $result="<div class='hide' id='laporan_asesmen_perawat_".$d->id."'>".self::format("array-bold-trim", $show)."</div>";
        $array['Pasien']=$array['Pasien'].$result;
        
        return $array;
    }
}

?>