<?php 

class KunjunganIGDAdapter extends ArrayAdapter {
	private $rujukan = 0;
	private $non_rujukan = 0;
	private $lk = 0;
	private $pr = 0;
	private $mrs = 0;
	private $dirujuk = 0;
	private $doa = 0;
	private $meninggal = 0;
	private $rawat_jalan= 0;
	private $total = 0;
	private $result=array();
	
	public function adapt($d) {
		$grup_name=$d->pola_kasus;		
		if(!isset($this->result[$d->pola_kasus])){
			$a=array();
			$a['Jenis Layanan'] = $d->pola_kasus;
			$a['Rujukan']       = 0;
			$a['Non Rujukan']   = 0;
			$a['Lk']            = 0;
			$a['Pr']            = 0;
			$a['MRS']           = 0;
			$a['Dirujuk']       = 0;
			$a['Rawat Jalan']   = 0;
			$a['DOA']           = 0;
			$a['Meninggal']     = 0;
			$a['Total']         = 0;
			$this->result[$d->pola_kasus]=$a;
		}		
		$x=&$this->result[$d->pola_kasus];
		$x['Total']++;
		if($d->triage=="Hitam DOA"){
			$x['DOA']++;
			$this->doa++;
		}else if($d->triage=="Hitam Mati"){
			$x['Meninggal']++;
			$this->meninggal++;
		}else if($d->kelanjutan=="RJK"){
			$x['Dirujuk']++;
			$this->dirujuk++;
		}else if($d->kelanjutan=="RJ"){
			$x['Rawat Jalan']++;
			$this->rawat_jalan++;
		}else{
			$x['MRS']++;
			$this->mrs++;
		}
        
        if($d->ds == '1') {
            $x['Non Rujukan']++;
            $this->non_rujukan++;
        } else {
            $x['Rujukan']++;
            $this->rujukan++;
        }
        
        if($d->jk == '0') {
            $x['Lk']++;
            $this->lk++;
        } else if($d->jk == '1') {
            $x['Pr']++;
            $this->pr++;
        }
        
		$this->total++;
		return NULL;
	}
	public function getContent($data) {
		parent::getContent ( $data );
		$a=array();
		$a['Jenis Layanan'] = "Jumlah";
        $a['Rujukan']       = $this->rujukan;
        $a['Non Rujukan']   = $this->non_rujukan;
        $a['Lk']            = $this->lk;
        $a['Pr']            = $this->pr;
		$a['MRS']           = $this->mrs;
		$a['Dirujuk']       = $this->dirujuk;
		$a['Rawat Jalan']   = $this->rawat_jalan;
		$a['DOA']           = $this->doa;
		$a['Meninggal']     = $this->meninggal;
		$a['Total']         = $this->total;
		$this->result[]     = $a;
		//$this->result       = $this->removeZero($this->result);
		return $this->result;
	}
}

?>