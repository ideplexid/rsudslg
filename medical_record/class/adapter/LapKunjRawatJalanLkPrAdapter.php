<?php

class LapKunjRawatJalanLkPrAdapter extends ArrayAdapter {
    private $jan_lk_total = 0;
    private $jan_pr_total = 0;
    private $feb_lk_total = 0;
    private $feb_pr_total = 0;
    private $mar_lk_total = 0;
    private $mar_pr_total = 0;
    private $apr_lk_total = 0;
    private $apr_pr_total = 0;
    private $mei_lk_total = 0;
    private $mei_pr_total = 0;
    private $jun_lk_total = 0;
    private $jun_pr_total = 0;
    private $jul_lk_total = 0;
    private $jul_pr_total = 0;
    private $ags_lk_total = 0;
    private $ags_pr_total = 0;
    private $sep_lk_total = 0;
    private $sep_pr_total = 0;
    private $okt_lk_total = 0;
    private $okt_pr_total = 0;
    private $nov_lk_total = 0;
    private $nov_pr_total = 0;
    private $des_lk_total = 0;
    private $des_pr_total = 0;
    private $total_lk = 0;
    private $total_pr = 0;
    private $result = array();
    
    public function adapt($d) {
        $a = array();
        $a['id'] = '';
        $a['ruangan'] = ArrayAdapter::slugFormat("unslug", $d['ruangan']);
        $a['jan_lk'] = $d['jan_lk'];
        $a['jan_pr'] = $d['jan_pr'];
        $a['feb_lk'] = $d['feb_lk'];
        $a['feb_pr'] = $d['feb_pr'];
        $a['mar_lk'] = $d['mar_lk'];
        $a['mar_pr'] = $d['mar_pr'];
        $a['apr_lk'] = $d['apr_lk'];
        $a['apr_pr'] = $d['apr_pr'];
        $a['mei_lk'] = $d['mei_lk'];
        $a['mei_pr'] = $d['mei_pr'];
        $a['jun_lk'] = $d['jun_lk'];
        $a['jun_pr'] = $d['jun_pr'];
        $a['jul_lk'] = $d['jul_lk'];
        $a['jul_pr'] = $d['jul_pr'];
        $a['ags_lk'] = $d['ags_lk'];
        $a['ags_pr'] = $d['ags_pr'];
        $a['sep_lk'] = $d['sep_lk'];
        $a['sep_pr'] = $d['sep_pr'];
        $a['okt_lk'] = $d['okt_lk'];
        $a['okt_pr'] = $d['okt_pr'];
        $a['nov_lk'] = $d['nov_lk'];
        $a['nov_pr'] = $d['nov_pr'];
        $a['des_lk'] = $d['des_lk'];
        $a['des_pr'] = $d['des_pr'];
        $a['total_lk'] = $d['total_lk'];
        $a['total_pr'] = $d['total_pr'];
        $this->result[] = $a;
        
        $this->jan_lk_total = $this->jan_lk_total + (int)$d['jan_lk'];
        $this->jan_pr_total = $this->jan_pr_total + (int)$d['jan_pr'];
        $this->feb_lk_total = $this->feb_lk_total + (int)$d['feb_lk'];
        $this->feb_pr_total = $this->feb_pr_total + (int)$d['feb_pr'];
        $this->mar_lk_total = $this->mar_lk_total + (int)$d['mar_lk'];
        $this->mar_pr_total = $this->mar_pr_total + (int)$d['mar_pr'];
        $this->apr_lk_total = $this->apr_lk_total + (int)$d['apr_lk'];
        $this->apr_pr_total = $this->apr_pr_total + (int)$d['apr_pr'];
        $this->mei_lk_total = $this->mei_lk_total + (int)$d['mei_lk'];
        $this->mei_pr_total = $this->mei_pr_total + (int)$d['mei_pr'];
        $this->jun_lk_total = $this->jun_lk_total + (int)$d['jun_lk'];
        $this->jun_pr_total = $this->jun_pr_total + (int)$d['jun_pr'];
        $this->jul_lk_total = $this->jul_lk_total + (int)$d['jul_lk'];
        $this->jul_pr_total = $this->jul_pr_total + (int)$d['jul_pr'];
        $this->ags_lk_total = $this->ags_lk_total + (int)$d['ags_lk'];
        $this->ags_pr_total = $this->ags_pr_total + (int)$d['ags_pr'];
        $this->sep_lk_total = $this->sep_lk_total + (int)$d['sep_lk'];
        $this->sep_pr_total = $this->sep_pr_total + (int)$d['sep_pr'];
        $this->okt_lk_total = $this->okt_lk_total + (int)$d['okt_lk'];
        $this->okt_pr_total = $this->okt_pr_total + (int)$d['okt_pr'];
        $this->nov_lk_total = $this->nov_lk_total + (int)$d['nov_lk'];
        $this->nov_pr_total = $this->nov_pr_total + (int)$d['nov_pr'];
        $this->des_lk_total = $this->des_lk_total + (int)$d['des_lk'];
        $this->des_pr_total = $this->des_pr_total + (int)$d['des_pr'];
        $this->total_lk = $this->total_lk + (int)$d['total_lk'];
        $this->total_pr = $this->total_pr + (int)$d['total_pr'];
    }
    
    public function getContent($data) {
		foreach($data as $d){
			$this->adapt($d);		
		}
		$a = array();
        $a['id'] = '';
        $a['ruangan'] = "<strong>TOTAL</strong>";
        $a['jan_lk'] = (string)$this->jan_lk_total;
        $a['jan_pr'] = (string)$this->jan_pr_total;
        $a['feb_lk'] = (string)$this->feb_lk_total;
        $a['feb_pr'] = (string)$this->feb_pr_total;
        $a['mar_lk'] = (string)$this->mar_lk_total;
        $a['mar_pr'] = (string)$this->mar_pr_total;
        $a['apr_lk'] = (string)$this->apr_lk_total;
        $a['apr_pr'] = (string)$this->apr_pr_total;
        $a['mei_lk'] = (string)$this->mei_lk_total;
        $a['mei_pr'] = (string)$this->mei_pr_total;
        $a['jun_lk'] = (string)$this->jun_lk_total;
        $a['jun_pr'] = (string)$this->jun_pr_total;
        $a['jul_lk'] = (string)$this->jul_lk_total;
        $a['jul_pr'] = (string)$this->jul_pr_total;
        $a['ags_lk'] = (string)$this->ags_lk_total;
        $a['ags_pr'] = (string)$this->ags_pr_total;
        $a['sep_lk'] = (string)$this->sep_lk_total;
        $a['sep_pr'] = (string)$this->sep_pr_total;
        $a['okt_lk'] = (string)$this->okt_lk_total;
        $a['okt_pr'] = (string)$this->okt_pr_total;
        $a['nov_lk'] = (string)$this->nov_lk_total;
        $a['nov_pr'] = (string)$this->nov_pr_total;
        $a['des_lk'] = (string)$this->des_lk_total;
        $a['des_pr'] = (string)$this->des_pr_total;
        $a['total_lk'] = (string)$this->total_lk;
        $a['total_pr'] = (string)$this->total_pr;
        $this->result[] = $a;
		return $this->result;
	}
}

?>