<?php

class LapDiagnosaAdapter extends ArrayAdapter {
    private $total = 0;
    public function adapt($d) {
        $this->total++;
        $res = array();
        $res['Nomor Profile']   = $d->profile_number;
        $res['Tanggal']         = self::format("date d M Y", $d->waktu);
        $res['No. Reg']         = $d->noreg_pasien;
        $res['NRM']             = $d->nrm_pasien;
        $res['Pasien']          = $d->sebutan." ".$d->nama_pasien;
        $res['Gol Umur']        = $d->gol_umur;
        $res['Jenis Kelamin']   = $d->jk;
        $res['Dokter']          = $d->nama_dokter;
        $res['Diagnosa']        = $d->diagnosa;
        $res['ICD']             = $d->kode_icd;
        $res['Penyakit']        = $d->nama_icd;
        $res['Ruangan']         = self::slugFormat("unslug", $d->ruangan);
        $res['Carabayar']         = self::slugFormat("unslug", $d->carabayar);
        $res['Alamat']          = $d->alamat;
        $res['Propinsi']        = $d->propinsi;
        $res['Kabupaten']       = $d->kabupaten;
        $res['Kecamatan']       = $d->kecamatan;
        $res['Kelurahan']       = $d->kelurahan;
        
        return $res;
    }
    
    public function getContent($data) {
        $result = parent::getContent($data);
		$one    = array();
        $one['Nomor Profile']   = "TOTAL";
        $one['Tanggal']         = $this->total;
        $result[]   = $one;
		return $result;
    }
} 

?>