<?php

class RL34Adapter extends ArrayAdapter {
    private $tahun;
    private $kode_propinsi;
    private $kode_rs;
    private $nama_rs;
    private $kab_kota;
    private $jenis_kegiatan;
    private $rm_rs;
    private $rm_bidan;
    private $rm_puskesmas;
    private $rm_faskes_lainnya;
    private $rm_hidup;
    private $rm_mati;
    private $rm_total;
    private $rnm_hidup;
    private $rnm_mati;
    private $rnm_total;
    private $nr_hidup;
    private $nr_mati;
    private $nr_total;
    private $dirujuk;
    
    public function setTahun($tahun) {
        $this->tahun = $tahun;
    }
    
    public function setKodePropinsi($kode_propinsi) {
        $this->kode_propinsi = $kode_propinsi;
    }
    
    public function setKodeRS($kode_rs) {
        $this->kode_rs = $kode_rs;
    }
    
    public function setNamaRS($nama_rs) {
        $this->nama_rs = $nama_rs;
    } 
    
    public function setKabKota($kab_kota) {
        $this->kab_kota = $kab_kota;
    }
    
    public function setJenisKegiatan($jenis_kegiatan) {
        $this->jenis_kegiatan = $jenis_kegiatan;
    }
    
    public function adapt($d) {
        for($i = 0; $i < sizeof($this->jenis_kegiatan); $i++) {
            if ($this->jenis_kegiatan[$i] == "3 - Persalinan dengan Komplikasi") {
                if (
                    $d->jenis_komplikasi == "3.1 - Pendarahan Sebelum Persalinan" ||
                    $d->jenis_komplikasi == "3.2 - Pendarahan Sesudah Persalinan" ||
                    $d->jenis_komplikasi == "3.3 - Pre Eclampsi" ||
                    $d->jenis_komplikasi == "3.4 - Eclampsi" ||
                    $d->jenis_komplikasi == "3.5 - Infeksi" ||
                    $d->jenis_komplikasi == "3.6 - Lain-Lain"
                ) {
                    if($d->kedatangan == 'Rumah Sakit Lain') {
                        $this->rm_rs['3 - Persalinan dengan Komplikasi']++;
                        if($d->kondisi_bayi == 'Lahir Hidup') {
                            $this->rm_hidup['3 - Persalinan dengan Komplikasi']++;
                        } else {
                            $this->rm_mati['3 - Persalinan dengan Komplikasi']++;
                        }
                        $this->rm_total['3 - Persalinan dengan Komplikasi']++;
                    } else if($d->kedatangan == 'Bidan') {
                        $this->rm_bidan['3 - Persalinan dengan Komplikasi']++;
                        if($d->kondisi_bayi == 'Lahir Hidup') {
                            $this->rm_hidup['3 - Persalinan dengan Komplikasi']++;
                        } else {
                            $this->rm_mati['3 - Persalinan dengan Komplikasi']++;
                        }
                        $this->rm_total['3 - Persalinan dengan Komplikasi']++;
                    } else if($d->kedatangan == 'Puskesmas') {
                        $this->rm_puskesmas['3 - Persalinan dengan Komplikasi']++;
                        if($d->kondisi_bayi == 'Lahir Hidup') {
                            $this->rm_hidup['3 - Persalinan dengan Komplikasi']++;
                        } else {
                            $this->rm_mati['3 - Persalinan dengan Komplikasi']++;
                        }
                        $this->rm_total[$this->jenis_kegiatan[$i]]++;
                    } else if($d->kedatangan == 'Faskes Lainnya') {
                        $this->rm_faskes_lainnya['3 - Persalinan dengan Komplikasi']++;
                        if($d->kondisi_bayi == 'Lahir Hidup') {
                            $this->rm_hidup['3 - Persalinan dengan Komplikasi']++;
                        } else {
                            $this->rm_mati['3 - Persalinan dengan Komplikasi']++;
                        }
                        $this->rm_total['3 - Persalinan dengan Komplikasi']++;
                    } else if($d->kedatangan == 'Non Medis') {
                        if($d->kondisi_bayi == 'Lahir Hidup') {
                            $this->rnm_hidup['3 - Persalinan dengan Komplikasi']++;
                        } else {
                            $this->rnm_mati['3 - Persalinan dengan Komplikasi']++;
                        }
                        $this->rnm_total['3 - Persalinan dengan Komplikasi']++;
                    } else if($d->kedatangan == 'Non Rujukan') {
                        if($d->kondisi_bayi == 'Lahir Hidup') {
                            $this->nr_hidup['3 - Persalinan dengan Komplikasi']++;
                        } else {
                            $this->nr_mati['3 - Persalinan dengan Komplikasi']++;
                        }
                        $this->nr_total['3 - Persalinan dengan Komplikasi']++;
                    }
                    if($d->dirujuk == '1' || $d->dirujuk == 1) {
                        $this->dirujuk['3 - Persalinan dengan Komplikasi']++;
                    }
                }                    
            }
            if($d->jenis_kegiatan == $this->jenis_kegiatan[$i] || $d->jenis_komplikasi == $this->jenis_kegiatan[$i]) {
                if($d->kedatangan == 'Rumah Sakit Lain') {
                    $this->rm_rs[$this->jenis_kegiatan[$i]]++;
                    if($d->kondisi_bayi == 'Lahir Hidup') {
                        $this->rm_hidup[$this->jenis_kegiatan[$i]]++;
                    } else {
                        $this->rm_mati[$this->jenis_kegiatan[$i]]++;
                    }
                    $this->rm_total[$this->jenis_kegiatan[$i]]++;
                } else if($d->kedatangan == 'Bidan') {
                    $this->rm_bidan[$this->jenis_kegiatan[$i]]++;
                    if($d->kondisi_bayi == 'Lahir Hidup') {
                        $this->rm_hidup[$this->jenis_kegiatan[$i]]++;
                    } else {
                        $this->rm_mati[$this->jenis_kegiatan[$i]]++;
                    }
                    $this->rm_total[$this->jenis_kegiatan[$i]]++;
                } else if($d->kedatangan == 'Puskesmas') {
                    $this->rm_puskesmas[$this->jenis_kegiatan[$i]]++;
                    if($d->kondisi_bayi == 'Lahir Hidup') {
                        $this->rm_hidup[$this->jenis_kegiatan[$i]]++;
                    } else {
                        $this->rm_mati[$this->jenis_kegiatan[$i]]++;
                    }
                    $this->rm_total[$this->jenis_kegiatan[$i]]++;
                } else if($d->kedatangan == 'Faskes Lainnya') {
                    $this->rm_faskes_lainnya[$this->jenis_kegiatan[$i]]++;
                    if($d->kondisi_bayi == 'Lahir Hidup') {
                        $this->rm_hidup[$this->jenis_kegiatan[$i]]++;
                    } else {
                        $this->rm_mati[$this->jenis_kegiatan[$i]]++;
                    }
                    $this->rm_total[$this->jenis_kegiatan[$i]]++;
                } else if($d->kedatangan == 'Non Medis') {
                    if($d->kondisi_bayi == 'Lahir Hidup') {
                        $this->rnm_hidup[$this->jenis_kegiatan[$i]]++;
                    } else {
                        $this->rnm_mati[$this->jenis_kegiatan[$i]]++;
                    }
                    $this->rnm_total[$this->jenis_kegiatan[$i]]++;
                } else if($d->kedatangan == 'Non Rujukan') {
                    if($d->kondisi_bayi == 'Lahir Hidup') {
                        $this->nr_hidup[$this->jenis_kegiatan[$i]]++;
                    } else {
                        $this->nr_mati[$this->jenis_kegiatan[$i]]++;
                    }
                    $this->nr_total[$this->jenis_kegiatan[$i]]++;
                }
                if($d->dirujuk == '1' || $d->dirujuk == 1) {
                    $this->dirujuk[$this->jenis_kegiatan[$i]]++;
                }
            }
        }
    }
    
    public function getContent($data) {
        for($i = 0; $i < sizeof($this->jenis_kegiatan); $i++) {
            $this->rm_rs[$this->jenis_kegiatan[$i]]             = 0;
            $this->rm_bidan[$this->jenis_kegiatan[$i]]          = 0;
            $this->rm_puskesmas[$this->jenis_kegiatan[$i]]      = 0;
            $this->rm_faskes_lainnya[$this->jenis_kegiatan[$i]] = 0;
            $this->rm_hidup[$this->jenis_kegiatan[$i]]          = 0;
            $this->rm_mati[$this->jenis_kegiatan[$i]]           = 0;
            $this->rm_total[$this->jenis_kegiatan[$i]]          = 0;
            $this->rnm_hidup[$this->jenis_kegiatan[$i]]         = 0;
            $this->rnm_mati[$this->jenis_kegiatan[$i]]          = 0;
            $this->rnm_total[$this->jenis_kegiatan[$i]]         = 0;
            $this->nr_hidup[$this->jenis_kegiatan[$i]]          = 0;
            $this->nr_mati[$this->jenis_kegiatan[$i]]           = 0;
            $this->nr_total[$this->jenis_kegiatan[$i]]          = 0;
            $this->dirujuk[$this->jenis_kegiatan[$i]]           = 0;
        }
        
        foreach($data as $d) {
            $this->adapt($d);
        }
        
        $res = array();
        for($i = 0; $i < sizeof($this->jenis_kegiatan); $i++) {
            $res[$i]['TAHUN']               = $this->tahun;
            $res[$i]['KODE PROPINSI']       = $this->kode_propinsi;
            $res[$i]['KODE RS']             = $this->kode_rs;
            $res[$i]['NAMA RS']             = $this->nama_rs;
            $res[$i]['KAB/KOTA']            = $this->kab_kota;
            $jenis_kegiatan                 = explode(" - ", $this->jenis_kegiatan[$i]);
            $res[$i]['NO']                  = $jenis_kegiatan[0];
            $res[$i]['JENIS KEGIATAN']      = $jenis_kegiatan[1];
            $res[$i]['RM RUMAH SAKIT']      = $this->rm_rs[$this->jenis_kegiatan[$i]];
            $res[$i]['RM BIDAN']            = $this->rm_bidan[$this->jenis_kegiatan[$i]];
            $res[$i]['RM PUSKESMAS']        = $this->rm_puskesmas[$this->jenis_kegiatan[$i]];
            $res[$i]['RM FASKES LAINNYA']   = $this->rm_faskes_lainnya[$this->jenis_kegiatan[$i]];
            $res[$i]['RM HIDUP']            = $this->rm_hidup[$this->jenis_kegiatan[$i]];
            $res[$i]['RM MATI']             = $this->rm_mati[$this->jenis_kegiatan[$i]];
            $res[$i]['RM TOTAL']            = $this->rm_total[$this->jenis_kegiatan[$i]];
            $res[$i]['RNM HIDUP']           = $this->rnm_hidup[$this->jenis_kegiatan[$i]];
            $res[$i]['RNM MATI']            = $this->rnm_mati[$this->jenis_kegiatan[$i]];
            $res[$i]['RNM TOTAL']           = $this->rnm_total[$this->jenis_kegiatan[$i]];
            $res[$i]['NR HIDUP']            = $this->nr_hidup[$this->jenis_kegiatan[$i]];
            $res[$i]['NR MATI']             = $this->nr_mati[$this->jenis_kegiatan[$i]];
            $res[$i]['NR TOTAL']            = $this->nr_total[$this->jenis_kegiatan[$i]];
            $res[$i]['DIRUJUK']             = $this->dirujuk[$this->jenis_kegiatan[$i]];
        }
        
        return $res;
    }
}

?>