<?php 

class TriageIGDAdapter extends ArrayAdapter {
	private $penganiayaan = 0;
	private $keckerja = 0;
	private $kll = 0;
	private $anak = 0;
	private $psikiatrik = 0;
	private $trauma = 0;
	private $medis = 0;
	private $vk = 0;
	private $biru = 0;
	private $merah = 0;
	private $kuning = 0;
	private $hijau = 0;
	private $doa = 0;
	private $mati = 0;
	private $rujuk = 0;
	private $menolak = 0;
	private $ver = 0;
	private $abh= 0;
	
	public function adapt($d) {
		$a = array ();
		$a ['id'] 					= $d->id;
		$a ['Tanggal'] 				= self::format ( "date d M Y", $d->tanggal );
		$a ['Penganiayaan'] 		= self::format ( "trivial_Bedah - Penganiayaan_x_ ", $d->pola_kasus );
		$a ['Kecelakaan Kerja'] 	= self::format ( "trivial_Bedah - Kecelakaan Kerja_x_ ", $d->pola_kasus );
		$a ['KLL'] 					= self::format ( "trivial_Bedah - KLL_x_ ", $d->pola_kasus );
		$a ['Non Trauma'] 			= self::format ( "trivial_Bedah - Non Trauma_x_ ", $d->pola_kasus );
		$a ['Medis'] 				= self::format ( "trivial_Medis_x_ ", $d->pola_kasus );
		$a ['Anak'] 				= self::format ( "trivial_Anak_x_ ", $d->pola_kasus );
		$a ['Psikiatrik'] 			= self::format ( "trivial_Psikiatrik_x_ ", $d->pola_kasus );
		$a ['VK'] 					= self::format ( "trivial_VK_x_ ", $d->pola_kasus );
		$a ['Biru'] 				= self::format ( "trivial_Biru_x_ ", $d->triage );
		$a ['Merah'] 				= self::format ( "trivial_Merah_x_ ", $d->triage );
		$a ['Kuning'] 				= self::format ( "trivial_Kuning_x_ ", $d->triage );
		$a ['Hijau'] 				= self::format ( "trivial_Hijau_x_ ", $d->triage );
		$a ['DOA'] 					= self::format ( "trivial_Hitam DOA_x_ ", $d->triage );
		$a ['Mati'] 				= self::format ( "trivial_Hitam Mati_x_ ", $d->triage );
		$a ['Rujuk'] 				= self::format ( "trivial_RJK_x_ ",$d->kelanjutan );
		$a ['Menolak MRS'] 			= self::format ( "trivial_1_x_ ", $d->menolak_mrs );
		$a ['VER'] 					= self::format ( "trivial_1_x_ ", $d->ver );
		$a ['Alat Bantu Hidup'] 	= self::format ( "trivial_1_x_ ", $d->alat_bantu_hidup);
		
		
		
		if ($a ['Penganiayaan'] == "x") 	$this->penganiayaan ++;
		if ($a ['Kecelakaan Kerja'] == "x")	$this->keckerja ++;
		if ($a ['KLL'] == "x")				$this->kll ++;
		if ($a ['Non Trauma'] == "x")		$this->trauma ++;
		if ($a ['Medis'] == "x")			$this->medis ++;
		if ($a ['Anak'] == "x")				$this->anak ++;
		if ($a ['Psikiatrik'] == "x")		$this->psikiatrik ++;
		if ($a ['VK'] == "x")				$this->vk ++;
		if ($a ['Biru'] == "x")				$this->biru ++;
		if ($a ['Merah'] == "x") 			$this->merah ++;
		if ($a ['Kuning'] == "x")			$this->kuning ++;
		if ($a ['Hijau'] == "x") 			$this->hijau ++;
		if ($a ['DOA'] == "x") 				$this->doa ++;
		if ($a ['Mati'] == "x") 			$this->mati ++;
		if ($a ['Rujuk'] == "x") 			$this->rujuk ++;
		if ($a ['Menolak MRS'] == "x") 		$this->menolak ++;
		if ($a ['VER'] == "x") 				$this->ver ++;
		if ($a ['Alat Bantu Hidup'] == "x") $this->abh ++;
		return $a;
	}
	public function getContent($data) {
		$result = parent::getContent ( $data );
		$last_data = array ();
		$last_data ['Tanggal'] 			= "Jumlah";
		$last_data ["Penganiayaan"] 	= $this->penganiayaan;
		$last_data ["Kecelakaan Kerja"] = $this->keckerja;
		$last_data ["KLL"] 				= $this->kll;
		$last_data ["Non Trauma"] 		= $this->trauma;
		$last_data ["Medis"] 			= $this->medis;
		$last_data ["Anak"] 			= $this->anak;
		$last_data ["Psikiatrik"] 		= $this->psikiatrik;
		$last_data ["VK"] 				= $this->vk;
		$last_data ["Biru"] 			= $this->biru;
		$last_data ["Merah"] 			= $this->merah;
		$last_data ["Kuning"] 			= $this->kuning;
		$last_data ["Hijau"] 			= $this->hijau;
		$last_data ["DOA"] 				= $this->doa;
		$last_data ["Mati"]				= $this->mati;
		$last_data ["Rujuk"] 			= $this->rujuk;
		$last_data ["Menolak MRS"] 		= $this->menolak;
		$last_data ["VER"] 				= $this->ver;
		$last_data ["Alat Bantu Hidup"] = $this->abh;
		
		$grand = $this->refactor ( $result );
		$grand [] = $last_data;
		$final = $this->removeZero ( $grand );
		return $final;
	}
	public function removeZero($gdata) {
		$hasil = array ();
		foreach ( $gdata as $onedata ) {
			$one_hasil = array ();
			foreach ( $onedata as $name => $value ) {
				if ($name == "Tanggal" || $value > 0) {
					$one_hasil [$name] = $value;
				} else {
					$one_hasil [$name] = "";
				}
			}
			$hasil [] = $one_hasil;
		}
		return $hasil;
	}
	public function refactor($data) {
		$res = array ();
		foreach ( $data as $d ) {
			$tgl = $d ["Tanggal"];
			$one = null;
			if (! isset ( $res [$tgl] )) {
				$one = array ();
				$one ['Tanggal'] = $tgl;
				$one ['Penganiayaan'] = 0;
				$one ['Kecelakaan Kerja'] = 0;
				$one ['KLL'] = 0;
				$one ['Non Trauma'] = 0;
				$one ['Medis'] = 0;
				$one ['Anak'] = 0;
				$one ['Psikiatrik'] = 0;
				$one ['VK'] = 0;
				$one ['Biru'] = 0;
				$one ['Merah'] = 0;
				$one ['Kuning'] = 0;
				$one ['Hijau'] = 0;
				$one ['DOA'] = 0;
				$one ['Mati'] = 0;
				$one ['Rujuk'] = 0;
				$one ['Menolak MRS'] = 0;
				$one ['VER'] = 0;
				$one ['Alat Alat Bantu Hidup'] = 0;
			} else {
				$one = $res [$tgl];
			}
			
			foreach ( $d as $name => $val ) {
				if ($name != "Tanggal" && $name != "id" && $val != " ") {
					$one [$name] = $one [$name] + 1;
					// echo $name." = ".$one[$name]."\n";
				}
			}
			$res [$tgl] = $one;
		}
		
		return $res;
	}
}


?>