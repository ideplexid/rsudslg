<?php


class LapTransfusiAdapter extends ArrayAdapter {
	private $wb = 0;
	private $prc = 0;
	private $plasma = 0;
	private $lain = 0;
	private $jumlah = 0;
	private $_result=array();
	
	public function adapt($d) {
		$tgl= self::format ( "date d M Y", $d->transfusi_kapan );
		if(!isset($this->_result[$tgl])){
				$a=array();
				$a['Tanggal']=$tgl;
				$a['Whole Blood']=0;
				$a['PRC']=0;
				$a['Plasma']=0;
				$a['Komp. Lain']=0;	
				$a['Jumlah']=0;		
				$this->_result[$tgl]=$a;		
		}		
		$this->_result[$tgl]['Whole Blood']+=$d->transfusi_wb;
		$this->_result[$tgl]['PRC']+=$d->transfusi_prc;
		$this->_result[$tgl]['Plasma']+=$d->transfusi_plasma;
		$this->_result[$tgl]['Komp. Lain']+=$d->transfusi_lain;
		$this->_result[$tgl]['Jumlah']+=($d->transfusi_wb+$d->transfusi_lain+$d->transfusi_prc+$d->transfusi_plasma);
		
		$this->wb+=$d->transfusi_wb;
		$this->prc+=$d->transfusi_prc;
		$this->plasma+=$d->transfusi_plasma;
		$this->lain+=$d->transfusi_lain;
		$this->jumlah+=($d->transfusi_wb+$d->transfusi_plasma+$d->transfusi_prc+$d->transfusi_lain);
		return NULL;
	}
	public function getContent($data) {
		parent::getContent ( $data );
		$last_data = array ();
		$last_data ['Tanggal'] = "<strong>Jumlah</strong>";
		$last_data ["Whole Blood"] = $this->wb;
		$last_data ["PRC"] = $this->prc;
		$last_data ["Plasma"] = $this->plasma;
		$last_data ["Komp. Lain"] = $this->lain;
		$last_data ["Jumlah"] = $this->jumlah;
		$grand=$this->_result;
		$grand [] = $last_data;		
		$final = $this->removeZero ( $grand );
		return $final;
	}
	
	public function removeZero($gdata) {
		$hasil = array ();
		foreach ( $gdata as $onedata ) {
			$one_hasil = array ();
			foreach ( $onedata as $name => $value ) {
				if ($name == "Tanggal" || $value > 0) {
					$one_hasil [$name] = $value;
				} else {
					$one_hasil [$name] = "";
				}
			}
			$hasil [] = $one_hasil;
		}
		return $hasil;
	}
	
}

?>