<?php

class LaporanAsesmenSyarafAdapter extends SimpleAdapter {
    public function adapt($d) {
        $array=parent::adapt($d);
        
        $show=array();
        $show['Pasien']                                     = $d->nama_pasien;
        $show['Jenis Kelamin']                              = $d->jk==0?"Laki-Laki":"Perempuan";
        $show['Alamat']                                     = $d->alamat_pasien;
        $show['NRM']                                        = $d->nrm_pasien;
        $show['No. Reg']                                    = $d->noreg_pasien;
        $show['Tanggal Asesmen']                            = self::format("date d M Y", $d->waktu);
        $show['Tanggal Masuk']                              = self::format("date d M Y", $d->waktu_masuk);
        $show['Ruangan']                                    = self::format("unslug", $d->ruangan);
        $show['Cara Bayar']                                 = $d->carabayar;
        $show['Dokter']                                     = $d->nama_dokter;
        $show['Mata (Anamnesis)']                           = $d->mata_anamnesis;
        $show['Mata (Iketerus)']                            = $d->mata_ikaterus;
        $show['Mata (Reflek Pupil)']                        = $d->mata_reflek_pupil;
        $show['Mata (Ukuran Pupil)']                        = $d->mata_ukuran_pupil;
        $show['Mata (Iso Unisocore)']                       = $d->mata_iso_unisocore;
        $show['Mata (Lain-Lain)']                           = $d->mata_lain;
        $show['THT (Tonsil)']                               = $d->tht_tonsil;
        $show['THT (Faring)']                               = $d->tht_faring;
        $show['THT (Lidah)']                                = $d->tht_lidah;
        $show['THT (Bibir)']                                = $d->tht_bibir;
        $show['Leher (JVP)']                                = $d->leher_jvp;
        $show['Leher (Pembesaran Kelenjar)']                = $d->leher_pembesaran_kelenjar;
        $show['Torak (Simetris/Asimetris)']                 = $d->torak_simetris_asimetris;
        $show['Cor (S1, S2)']                               = $d->cor_s1_s2;
        $show['Cor (Reguler)']                              = $d->cor_reguler;
        $show['Cor (Ireguler)']                             = $d->cor_ireguler;
        $show['Cor (Mur-Mur)']                              = $d->cor_mur;
        $show['Cor (Besar)']                                = $d->cor_besar;
        $show['Pulmo (Suara Nafas)']                        = $d->pulmo_suara_nafas;
        $show['Pulmo (Ronchi)']                             = $d->pulmo_ronchi;
        $show['Pulmo (Wheezing)']                           = $d->pulmo_wheezing;
        $show['Abdomen (Hepar)']                            = $d->abdomen_hepar;
        $show['Abdomen (Lien)']                             = $d->abdomen_lien;
        $show['Extremitas (Hangat)']                        = $d->extremitas_hangat;
        $show['Extremitas (Dingin)']                        = $d->extremitas_dingin;
        $show['Extremitas (Edema)']                         = $d->extremitas_edema;
        $show['Kranium']                                    = $d->kranium;
        $show['Kranium (Lain-Lain)']                        = $d->kranium_lain;
        $show['Kranium (Keterangan)']                       = $d->kranium_keterangan;
        $show['Korpus Vertebra']                            = $d->korpus_vertebra;
        $show['Korpus Vertebra (Lain-Lain)']                = $d->korpus_vertebra_lain;
        $show['Korpus Vertebra (Keterangan)']               = $d->korpus_vertebra_keterangan;
        $show['Perangsangan Selaput Otak']                  = $d->tanda_selaput_otak;
        $show['Lain-Lain']                                  = $d->tanda_selaput_otak_lain;
        $show['Keterangan']                                 = $d->tanda_selaput_otak_keterangan;
        $show['Motorik']                                    = $d->motorik;
        $show['Motorik (Kaki Kanan)']                       = $d->motorik_kaki_kanan;
        $show['Motorik (Kaki Kiri)']                        = $d->motorik_kaki_kiri;
        $show['Refleks']                                    = $d->refleks;
        $show['Refleks (Kaki Kanan)']                       = $d->refleks_kaki_kanan;
        $show['Refleks (Kaki Kiri)']                        = $d->refleks_kaki_kiri;
        $show['Sensorik']                                   = $d->sensorik;
        $show['Sensorik (Kaki Kanan)']                      = $d->sensorik_kaki_kanan;
        $show['Sensorik (Kaki Kiri)']                       = $d->sensorik_kaki_kiri;
        $show['Vegentatif']                                 = $d->vegentatif;
        $show['Kesadaran']                                  = $d->luhur_kesadaran;
        $show['Reaksi Emosi']                               = $d->luhur_reaksi_emosi;
        $show['Fungsi Intelek']                             = $d->luhur_intelek;
        $show['Proses Berpikir']                            = $d->luhur_proses_berpikir;
        $show['Fungsi Psikomotorik']                        = $d->luhur_psikomotorik;
        $show['Fungsi Psikosensorik']                       = $d->luhur_psikosensorik;
        $show['Fungsi Bicara dan Bahasa']                   = $d->luhur_bicara_bahasa;
        $show['Kemunduran Mental (Refleks)']                = $d->mental_refleks;
        $show['Kemunduran Mental (Memegang)']               = $d->mental_memegang;
        $show['Kemunduran Mental (Menetek)']                = $d->mental_menetek;
        $show['Snout Refleks']                              = $d->mental_snout_refleks;
        $show['Refleks Glabola']                            = $d->mental_glabola;
        $show['Refleks Palmomental']                        = $d->mental_palmomental;
        $show['Refleks Korneomandibular']                   = $d->mental_korneomandibular;
        $show['Refleks Kaki Tolik']                         = $d->mental_kaki_tolik;
        $show['Refleks Lain-Lain']                          = $d->mental_lain;
        $show['Nyeri Tekan Syaraf']                         = $d->nyeri_tekan_syaraf;
        $show['Tanda Laseque']                              = $d->tanda_laseque;
        $show['Lain-Lain']                                  = $d->lain_lain;
        $show['Diagnosa Kerja']                             = $d->diagnosa_kerja;
        $show['Kode ICD Diagnosa Kerja']                    = $d->diagnosa_kerja_icd;
        $show['Diagnosa Kerja Ket']                         = $d->diagnosa_kerja_ket;
        $show['Terapi Tindakan']                            = $d->terapi_tindakan;
        $show['Kode ICD Terapi Tindakan']                   = $d->terapi_tindakan_icd;
        $show['Terapi Tindakan Ket']                        = $d->terapi_tindakan_ket;
        $show['Neurovaskuler']                              = $d->penunjang_neurovaskuler;
        $show['Neuroimaging']                               = $d->penunjang_neuroimaging;
        $show['Elektrodiagnostik']                          = $d->penunjang_elektrodiagnostik;
        $show['Laboratorium']                               = $d->penunjang_lab;
        $show['Penunjang Lain-Lain']                        = $d->penunjang_lain;
        $show['Waktu Pulang']                               = self::format("date d M Y: H:i:s", $d->waktu_pulang);
        $show['Tanggal Kontrol Klinik']                     = self::format("date d M Y", $d->tgl_kontrol_klinik);
        $show['Dirawat Di Ruang']                           = $d->dirawat_diruang;
        
        $result="<div class='hide' id='laporan_asesmen_syaraf_".$d->id."'>".self::format("array-bold-trim", $show)."</div>";
        $array['Pasien']=$array['Pasien'].$result;
        
        return $array;
    }
}

?>