<?php

class LapRL311Adapter extends ArrayAdapter{
	
	public function adapt($onerow){
		return null;
	}
	
	public function getContent($data){
		$content=array();
		$x=$data[0];
		$array=array();
		$array[1]="psikotes";
		$array[2]="konsultasi";
		$array[3]="medikamentosa";
		$array[4]="elektro_medik";
		$array[5]="psikoterapi";
		$array[6]="play_therapy";
		$array[7]="rehab_medik_psikiatrik";
		$total = 0;
		foreach($array as $no=>$name){
			$one=array();
			$one['Kode RS'] = $x['koders'];
			$one['Kode Propinsi'] = $x['kode_provinsi'];
			$one['Kab/Kota'] = $x['kota_kabupaten'];
			$one['Nama RS'] = $x['nama_rs'];
			$one['Tahun'] = $x['tahun'];
			$one["No."]=$no.".";
			$one["Layanan"]=self::format("unslug", $name);
			$one["Total"]=$x[$name];
			$total+=$x[$name];
			$content[]=$one;
		}
		$one=array();
		$one["No."]="";
		$one["Layanan"]="<strong>TOTAL</strong>";
		$one["Total"]="<strong>".$total."</strong>";
		$content[]=$one;
		return $content;
	}
	
}

?>