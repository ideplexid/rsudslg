<?php 

class TriageIGDData extends ArrayAdapter {
	private $p_rj = 0;
	private $p_mrs = 0;
	private $p_ds = 0;
	private $p_dokter = 0;
	private $p_lain= 0;
	private $p_pkm = 0;
	private $p_prbd = 0;
	private $p_jml = 0;
	private $s_rj = 0;
	private $s_mrs = 0;
	private $s_ds = 0;
	private $s_lain = 0;
	private $s_dokter = 0;
	private $s_pkm = 0;
	private $s_prbd = 0;
	private $s_jml = 0;
	private $m_rj = 0;
	private $m_mrs = 0;
	private $m_ds = 0;
	private $m_lain = 0;
	private $m_dokter = 0;
	private $m_pkm = 0;
	private $m_prbd = 0;
	private $m_jml = 0;
	private $total = 0;
	public function adapt($d) {
		$a = array ();
		$a ['id'] = $d->id;
		$a ['Tanggal'] = self::format ( "date d M Y", $d->tgl_lap );
		$this->total ++;
		$a ["Total"] = "x";
		
		$a ["P-RJ"] = " ";
		$a ["P-MRS"] = " ";
		$a ["P-Lain"] = " ";
		$a ["P-DS"] = " ";
		$a ["P-Dokter"] = " ";
		$a ["P-PKM"] = " ";
		$a ["P-PRBD"] = " ";
		$a ["P-JML"] = " ";
		
		$a ["S-RJ"] = " ";
		$a ["S-MRS"] = " ";
		$a ["S-DS"] = " ";
		$a ["S-Lain"] = " ";
		$a ["S-Dokter"] = " ";
		$a ["S-PKM"] = " ";
		$a ["S-PRBD"] = " ";
		$a ["S-JML"] = " ";
		
		$a ["M-RJ"] = " ";
		$a ["M-MRS"] = " ";
		$a ["M-DS"] = " ";
		$a ["M-Dokter"] = " ";
		$a ["M-PKM"] = " ";
		$a ["M-PRBD"] = " ";
		$a ["M-Lain"] = " ";
		$a ["M-JML"] = " ";
		
		if ($d->sift == "Pagi") {
			$a ["P-RJ"] = $d->kelanjutan=="MRS"?"":"x";//self::format ( "trivial_RJ_x_ ", $d->kelanjutan );
			$a ["P-MRS"] = self::format ( "trivial_MRS_x_ ", $d->kelanjutan );
			$a ["P-DS"] = self::format ( "trivial_1_x_ ", $d->ds );
			$a ["P-Dokter"] = self::format ( "trivial_Dokter_x_ ", $d->kiriman );
			$a ["P-Lain"] = self::format ( "trivial_Lain_x_ ", $d->kiriman );
			$a ["P-PKM"] = self::format ( "trivial_PKM_x_ ", $d->kiriman );
			$a ["P-PRBD"] = self::format ( "trivial_PR/BD_x_ ", $d->kiriman );
			$a ["P-JML"] = "x";
			if ($a ['P-RJ'] == "x")
				$this->p_rj ++;
			if ($a ['P-MRS'] == "x")
				$this->p_mrs ++;
			if ($a ['P-DS'] == "x")
				$this->p_ds ++;
			if ($a ['P-Dokter'] == "x")
				$this->p_dokter ++;
			if ($a ['P-PKM'] == "x")
				$this->p_pkm ++;
			if ($a ['P-PRBD'] == "x")
				$this->p_prbd ++;
			if ($a ["P-JML"] == "x")
				$this->p_jml ++;
			if ($a ['P-Lain'] == "x")
				$this->p_lain ++;
		} else if ($d->sift == "Sore") {
			$a ["S-RJ"] = $d->kelanjutan=="MRS"?"":"x";//self::format ( "trivial_RJ_x_ ", $d->kelanjutan );
			$a ["S-MRS"] = self::format ( "trivial_MRS_x_ ", $d->kelanjutan );
			$a ["S-DS"] = 	self::format ( "trivial_1_x_ ", $d->ds );
			$a ["S-Dokter"] = self::format ( "trivial_Dokter_x_ ", $d->kiriman );
			$a ["S-Lain"] = self::format ( "trivial_Lain_x_ ", $d->kiriman );
			$a ["S-PKM"] = self::format ( "trivial_PKM_x_ ", $d->kiriman );
			$a ["S-PRBD"] = self::format ( "trivial_PR/BD_x_ ", $d->kiriman );
			$a ["S-JML"] = "x";
			if ($a ['S-RJ'] == "x")
				$this->s_rj ++;
			if ($a ['S-MRS'] == "x")
				$this->s_mrs ++;
			if ($a ['S-DS'] == "x")
				$this->s_ds ++;
			if ($a ['S-Dokter'] == "x")
				$this->s_dokter ++;
			if ($a ['S-PKM'] == "x")
				$this->s_pkm ++;
			if ($a ['S-PRBD'] == "x")
				$this->s_prbd ++;
			if ($a ['S-JML'] == "x")
				$this->s_jml ++;
			if ($a ['S-Lain'] == "x")
				$this->s_lain ++;
		} else if ($d->sift == "Malam"){
			$a ["M-RJ"] = $d->kelanjutan=="MRS"?"":"x";//self::format ( "trivial_RJ_x_ ", $d->kelanjutan );
			$a ["M-MRS"] = self::format ( "trivial_MRS_x_ ", $d->kelanjutan );
			$a ["M-DS"] = self::format ( "trivial_1_x_ ", $d->ds );
			$a ["M-Dokter"] = self::format ( "trivial_Dokter_x_ ", $d->kiriman );
			$a ["M-PKM"] = self::format ( "trivial_PKM_x_ ", $d->kiriman );
			$a ["M-PRBD"] = self::format ( "trivial_PR/BD_x_ ", $d->kiriman );
			$a ["M-Lain"] = self::format ( "trivial_Lain_x_ ", $d->kiriman );
			$a ["M-JML"] = "x";
			if ($a ['M-RJ'] == "x")
				$this->m_rj ++;
			if ($a ['M-MRS'] == "x")
				$this->m_mrs ++;
			if ($a ['M-DS'] == "x")
				$this->m_ds ++;
			if ($a ['M-Dokter'] == "x")
				$this->m_dokter ++;
			if ($a ['M-PKM'] == "x")
				$this->m_pkm ++;
			if ($a ['M-PRBD'] == "x")
				$this->m_prbd ++;
			if ($a ['M-JML'] == "x")
				$this->m_jml ++;
			if ($a ['M-Lain'] == "x")
				$this->m_lain ++;
		}
		
		return $a;
	}
	public function getContent($data) {
		$result = parent::getContent ( $data );
		$last_data = array ();
		$last_data ['Tanggal'] = "Jumlah";
		$last_data ['P-RJ'] = $this->p_rj;
		$last_data ['P-MRS'] = $this->p_mrs;
		$last_data ['P-DS'] = $this->p_ds;
		$last_data ['P-Dokter'] = $this->p_dokter;
		$last_data ['P-PKM'] = $this->p_pkm;
		$last_data ['P-PRBD'] = $this->p_prbd;
		$last_data ['P-Lain'] = $this->p_lain;
		$last_data ['P-JML'] = $this->p_jml;
		
		$last_data ['S-RJ'] = $this->s_rj;
		$last_data ['S-MRS'] = $this->s_mrs;
		$last_data ['S-DS'] = $this->s_ds;
		$last_data ['S-Dokter'] = $this->s_dokter;
		$last_data ['S-Lain'] = $this->s_lain;
		$last_data ['S-PKM'] = $this->s_pkm;
		$last_data ['S-PRBD'] = $this->s_prbd;
		$last_data ['S-JML'] = $this->s_jml;
		
		$last_data ['M-RJ'] = $this->m_rj;
		$last_data ['M-MRS'] = $this->m_mrs;
		$last_data ['M-DS'] = $this->m_ds;
		$last_data ['M-Lain'] = $this->m_lain;
		$last_data ['M-Dokter'] = $this->m_dokter;
		$last_data ['M-PKM'] = $this->m_pkm;
		$last_data ['M-PRBD'] = $this->m_prbd;
		$last_data ['M-JML'] = $this->m_jml;
		
		$last_data ['Total'] = $this->total;
		
		$grand = $this->refactor ( $result );
		$grand [] = $last_data;
		$final = $this->removeZero ( $grand );
		return $final;
	}
	public function removeZero($gdata) {
		$hasil = array ();
		foreach ( $gdata as $onedata ) {
			$one_hasil = array ();
			foreach ( $onedata as $name => $value ) {
				if ($name == "Tanggal" || $value > 0) {
					$one_hasil [$name] = $value;
				} else {
					$one_hasil [$name] = "";
				}
			}
			$hasil [] = $one_hasil;
		}
		return $hasil;
	}
	public function refactor($data) {
		$res = array ();
		foreach ( $data as $d ) {
			$tgl = $d ["Tanggal"];
			$one = null;
			if (! isset ( $res [$tgl] )) {
				$one = array ();
				$one ['Tanggal'] = $tgl;
				$one ["Total"] = 0;
				$one ["P-RJ"] = 0;
				$one ["P-MRS"] = 0;
				$one ["P-DS"] = 0;
				$one ["P-Lain"] = 0;
				$one ["P-Dokter"] = 0;
				$one ["P-PKM"] = 0;
				$one ["P-PRBD"] = 0;
				$one ["P-JML"] = 0;
				
				$one ["S-RJ"] = 0;
				$one ["S-Lain"] = 0;
				$one ["S-MRS"] = 0;
				$one ["S-DS"] = 0;
				$one ["S-Dokter"] = 0;
				$one ["S-PKM"] = 0;
				$one ["S-PRBD"] = 0;
				$one ["S-JML"] = 0;
				
				$one ["M-RJ"] = 0;
				$one ["M-MRS"] = 0;
				$one ["M-DS"] = 0;
				$one ["M-Lain"] = 0;
				$one ["M-Dokter"] = 0;
				$one ["M-PKM"] = 0;
				$one ["M-PRBD"] = 0;
				$one ["M-JML"] = 0;
			} else {
				$one = $res [$tgl];
			}
			
			foreach ( $d as $name => $val ) {
				if ($name != "Tanggal" && $name != "id" && $val == "x") {
					$one [$name] = $one [$name] + 1;
					// echo $name." = ($val) ".$one[$name]."\n";
				}
			}
			$res [$tgl] = $one;
		}
		
		return $res;
	}
}


?>