<?php

class RL12Adapter extends ArrayAdapter{
    private $bor = 0;
    private $alos = 0;
    private $toi = 0;
    private $bto = 0;
    private $ndr = 0;
    private $gdr = 0;
    private $grup_ruangan;
    private $result = array();
    
    public function setGrupRuangan($grup_ruangan) {
        for($i = 0; $i < sizeof($grup_ruangan); $i++) {
            $this->grup_ruangan[$i]['Ruangan'] = $grup_ruangan[$i]->grup_ruangan;
        }
    }
    
    public function setBor($bor) {
        $this->bor = $bor;
    }
    
    public function setAlos($alos) {
        $this->alos = $alos;
    }
    
    public function setToi($toi) {
        $this->toi = $toi;
    }
    
    public function setBto($bto) {
        $this->bto = $bto;
    }
    
    public function setNdr($ndr) {
        $this->ndr = $ndr;
    }
    
    public function setGdr($gdr) {
        $this->gdr = $gdr;
    }
    
    public function adapt($d) {
        $a = array ();
        $this->number++;
        $a ['No.'] = $this->number;
        $a ['Ruangan'] = ArrayAdapter::slugFormat("unslug", $d ['ruangan']);
        if(isset($_POST['bor']) && $_POST['bor'] != "" && $_POST['bor'] != "undefined") {
            $a ['BOR'] = round(($d ['bor']) * 100 / ($this->bor), 2);
        } else {
            $a ['BOR'] = round($d ['bor'], 2);
        }
        
        if(isset($_POST['alos']) && $_POST['alos'] != "" && $_POST['alos'] != "undefined") {
            $a ['ALOS'] = round(($d ['los']) / ($this->alos), 2);
        } else {
            $a ['ALOS'] = round($d ['los'], 2);
        }
        
        if(isset($_POST['toi']) && $_POST['toi'] != "" && $_POST['toi'] != "undefined") {
            $a ['TOI'] = round(((($d ['jtt'] * $this->toi) - $d['jhp']) / $d ['jp']), 2);
        } else {
            $a ['TOI'] = round($d ['toi'], 2);
        }
        
        if(isset($_POST['bto']) && $_POST['bto'] != "" && $_POST['bto'] != "undefined") {
            $a ['BTO'] = round(($d['bto'] * 1000 * 1000) /  ($this->bto), 2);
        } else {
            $a ['BTO'] = round($d['bto'], 2);
        }
        
        if(isset($_POST['ndr']) && $_POST['ndr'] != "" && $_POST['ndr'] != "undefined") {
            $a ['NDR'] = round(($d ['ndr']) * 1000 / $this->ndr, 2);
        } else {
            $a ['NDR'] = round($d ['ndr'], 2);
        }
        
        if(isset($_POST['gdr']) && $_POST['gdr'] != "" && $_POST['gdr'] != "undefined") {
            $a ['GDR'] = round(($d ['gdr']) * 1000 / $this->gdr, 2);
        } else {
            $a ['GDR'] = round($d ['gdr'], 2);
        }
		
		$a ['Rata/Hari'] = round($d['rata_hari'], 2);
		$a ['Rata/Bulan'] = round($d['rata_bulan'], 2);
		return $a;
    }
    
    public function getContent($data) {
        foreach($data as $d) {
            $this->result[] = $this->adapt($d);
        }
        
        $total_rs['Ruangan'] = '** RUMAH SAKIT';
        $total_rs['BOR'] = 0;
        $total_rs['ALOS'] = 0;
        $total_rs['TOI'] = 0;
        $total_rs['BTO'] = 0;
        $total_rs['NDR'] = 0;
        $total_rs['GDR'] = 0;
        $total_rs['Rata/Hari'] = 0;
        $total_rs['Rata/Bulan'] = 0;
        
        for($i = 0; $i < sizeof($this->grup_ruangan); $i++) {
            $this->grup_ruangan[$i]['jhp']          = 0;
            $this->grup_ruangan[$i]['jtt']          = 0;
            $this->grup_ruangan[$i]['jh']           = 0;
            $this->grup_ruangan[$i]['jp']           = 0;
            $this->grup_ruangan[$i]['jph']          = 0;
            $this->grup_ruangan[$i]['jpm48']        = 0;
            $this->grup_ruangan[$i]['jpm']          = 0;
            
            for($j = 0; $j < sizeof($data); $j++) {
                if($this->grup_ruangan[$i]['Ruangan'] == $data[$j]['ruangan']) {
                    $this->grup_ruangan[$i]['jhp']      += $data[$j]['jhp'];
                    $this->grup_ruangan[$i]['jtt']      += $data[$j]['jtt'];
                    $this->grup_ruangan[$i]['jh']       = $data[$j]['jh'];
                    $this->grup_ruangan[$i]['jp']       += $data[$j]['jp'];
                    $this->grup_ruangan[$i]['jph']      += $data[$j]['jph'];
                    $this->grup_ruangan[$i]['jpm48']    += $data[$j]['jpm48'];
                    $this->grup_ruangan[$i]['jpm']      += $data[$j]['jpm'];
                }
            }
            
            $grup_ruangan = '* '.$this->grup_ruangan[$i]['Ruangan'];
            if($this->grup_ruangan[$i]['jhp'] != 0) {
                $bor = ($this->grup_ruangan[$i]['jhp'] * 100) / ($this->grup_ruangan[$i]['jtt'] * $this->grup_ruangan[$i]['jh']);
                $this->grup_ruangan[$i]['BOR']          = round($bor, 2);
            } else {
                $this->grup_ruangan[$i]['BOR']          = 0;
            }
            
            if($this->grup_ruangan[$i]['jhp'] != 0) {
                $alos = $this->grup_ruangan[$i]['jhp'] / $this->grup_ruangan[$i]['jp'];
                $this->grup_ruangan[$i]['ALOS']         = round($alos, 2);
            } else {
                $this->grup_ruangan[$i]['ALOS']         = 0;
            }
            
            if(($this->grup_ruangan[$i]['jtt'] * $this->grup_ruangan[$i]['jh']) - $this->grup_ruangan[$i]['jhp'] != 0) {
                $toi = (($this->grup_ruangan[$i]['jtt'] * $this->grup_ruangan[$i]['jh']) - $this->grup_ruangan[$i]['jhp']) / $this->grup_ruangan[$i]['jp'];
                $this->grup_ruangan[$i]['TOI']          = round($toi, 2);
            } else {
                $this->grup_ruangan[$i]['TOI']          = 0;
            }
            
            if($this->grup_ruangan[$i]['jp'] != 0) {
                $bto = $this->grup_ruangan[$i]['jp'] / $this->grup_ruangan[$i]['jtt'];
                $this->grup_ruangan[$i]['BTO']          = round($bto, 2);
            } else {
                $this->grup_ruangan[$i]['BTO']          = 0;
            }
            
            if($this->grup_ruangan[$i]['jpm48'] != 0) {
                $ndr = ($this->grup_ruangan[$i]['jpm48'] * 1000) / $this->grup_ruangan[$i]['jp'];
                $this->grup_ruangan[$i]['NDR']          = round($ndr, 2);
            } else {
                $this->grup_ruangan[$i]['NDR']          = 0;
            }
            
            if($this->grup_ruangan[$i]['jpm'] != 0) {
                $gdr = ($this->grup_ruangan[$i]['jpm'] * 1000) / $this->grup_ruangan[$i]['jp'];
                $this->grup_ruangan[$i]['GDR']          = round($gdr, 2);
            } else {
                $this->grup_ruangan[$i]['GDR']          = 0;
            }
            
            if($this->grup_ruangan[$i]['jp'] != 0) {
                $ratahari = $this->grup_ruangan[$i]['jp'] / $this->grup_ruangan[$i]['jh'];
                $this->grup_ruangan[$i]['Rata/Hari']    = round($ratahari, 2);
            } else {
                $this->grup_ruangan[$i]['Rata/Hari']    = 0;
            }
            
            if($this->grup_ruangan[$i]['jp'] != 0) {
                $ratabulan = $this->grup_ruangan[$i]['jp'] * 30 / $this->grup_ruangan[$i]['jh'];
                $this->grup_ruangan[$i]['Rata/Bulan']   = round($ratabulan, 2);
            } else {
                $this->grup_ruangan[$i]['Rata/Bulan']   = 0;
            }
            
            $this->grup_ruangan[$i]['Ruangan'] = $grup_ruangan;
            $this->number++;
            $this->grup_ruangan[$i]['No.']          = $this->number;
            
            $this->result[] = $this->grup_ruangan[$i];
            
            $total_rs['BOR']        += $this->grup_ruangan[$i]['BOR'];
            $total_rs['ALOS']       += $this->grup_ruangan[$i]['ALOS'];
            $total_rs['TOI']        += $this->grup_ruangan[$i]['TOI'];
            $total_rs['BTO']        += $this->grup_ruangan[$i]['BTO'];
            $total_rs['NDR']        += $this->grup_ruangan[$i]['NDR'];
            $total_rs['GDR']        += $this->grup_ruangan[$i]['GDR'];
            $total_rs['Rata/Hari']  += $this->grup_ruangan[$i]['Rata/Hari'];
            $total_rs['Rata/Bulan'] += $this->grup_ruangan[$i]['Rata/Bulan'];
        }
        
        $total_rs['BOR']        = round($total_rs['BOR'] / sizeof($this->grup_ruangan), 2);
        $total_rs['ALOS']       = round($total_rs['ALOS'] / sizeof($this->grup_ruangan), 2);
        $total_rs['TOI']        = round($total_rs['TOI'] / sizeof($this->grup_ruangan), 2);
        $total_rs['BTO']        = round($total_rs['BTO'] / sizeof($this->grup_ruangan), 2);
        $total_rs['NDR']        = round($total_rs['NDR'] / sizeof($this->grup_ruangan), 2);
        $total_rs['GDR']        = round($total_rs['GDR'] / sizeof($this->grup_ruangan), 2);
        $total_rs['Rata/Hari']  = round($total_rs['Rata/Hari'] / sizeof($this->grup_ruangan), 2);
        $total_rs['Rata/Bulan'] = round($total_rs['Rata/Bulan'] / sizeof($this->grup_ruangan), 2);
        $this->number++;
        $total_rs['No.'] = $this->number;
            
        $this->result[] = $total_rs;
        
        return $this->result;
    }
}

?>