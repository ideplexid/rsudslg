<?php 

class KamarOperasiAdapter extends ArrayAdapter {
	
	private $kh=0;
	private $bs=0;
	private $kc=0;
	private $sd=0;
	private $poli=0;
	private $ruang=0;
	private $rslain=0;
	
	
	public function adapt($d) {
		$a = array ();
		$this->number ++;
		$a ['Nomor'] = $this->number;
		$a ['id'] = $d->id;
		$a ['Tanggal'] = self::format ( "date d M Y", $d->tanggal_mulai );
		$a ['Spesialisasi'] = $d->spesialisasi;
		$a ['Anastesi'] = $d->anastesi;
		$a ['Dokter'] = $d->dokter_operator;
		
		if($d->asal_ruang == "Pendaftaran" || $d->asal_ruang == "IGD"){
			$a ['RS Lain'] =  "x";
			$this->rslain++;
		}
		if(startsWith ( $d->asal_ruang, "poli" )){
			$a ['Poli'] =   $d->asal_ruang ;
			$this->poli++;
		}
		if($a ['RS Lain'] == "" && $a ['Poli'] == ""){
			$a ['Ruang'] =  $d->asal_ruang ;
			$this->ruang++;
		}
		
		
		$a ['KH'] = "";
		$a ['BS'] = "";
		$a ['SD'] = "";
		$a ['KC'] = "";
		if(strpos($d->jenis_operasi  , "Khusus")!==false){
			$a ['KH'] = "x";
			$this->kh++;
		}
		
		if(strpos($d->jenis_operasi  , "Besar")!==false){
			$a ['BS'] = "x";
			$this->bs++;
		}
		if(strpos($d->jenis_operasi  , "Sedang")!==false){
			$a ['SD'] = "x";
			$this->sd++;
		}
		if(strpos($d->jenis_operasi  , "Kecil")!==false){
			$a ['KC'] = "x";
			$this->kc++;
		}
		
		return $a;
	}
	
	public function getContent($data){
		$content=parent::getContent($data);
		$a=array();
		$a ['KH'] = $this->kh;
		$a ['BS'] = $this->bs;
		$a ['SD'] = $this->sd;
		$a ['KC'] = $this->kc;
		
		$a ['RS Lain'] = $this->rslain;
		$a ['Poli'] = $this->poli;
		$a ['Ruang'] = $this->ruang;
		$a ['Spesialisasi']="<strong>Total</strong>";
		$content[]=$a;
		return $content;
	}
}

?>