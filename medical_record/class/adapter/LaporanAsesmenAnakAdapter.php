<?php

class LaporanAsesmenAnakAdapter extends SimpleAdapter {
    public function adapt($d) {
        $array=parent::adapt($d);
        
        $show=array();
        $show['Pasien']                                     = $d->nama_pasien;
        $show['Jenis Kelamin']                              = $d->jk==0?"Laki-Laki":"Perempuan";
        $show['Alamat']                                     = $d->alamat_pasien;
        $show['NRM']                                        = $d->nrm_pasien;
        $show['No. Reg']                                    = $d->noreg_pasien;
        $show['Tanggal Asesmen']                            = self::format("date d M Y", $d->waktu);
        $show['Tanggal Masuk']                              = self::format("date d M Y", $d->waktu_masuk);
        $show['Ruangan']                                    = self::format("unslug", $d->ruangan);
        $show['Cara Bayar']                                 = $d->carabayar;
        $show['Dokter']                                     = $d->nama_dokter;
        $show['Berat Badan']                                = $d->bb. " kg";
        $show['Tinggi Badan']                               = $d->tb. " cm";
        $show['LILA']                                       = $d->lila;
        $show['LK']                                         = $d->lk;
        $show['Keluhan Utama']                              = $d->anamneses_utama;
        $show['Riwayat Penyakit Sekarang']                  = $d->anamneses_penyakit_skrg;
        $show['Riwayat Pengobatan']                         = $d->anamneses_pengobatan;
        $show['Anemia']                                     = $d->mata_anemia;
        $show['Ikterus']                                    = $d->mata_ikterus;
        $show['Reflek Pupil']                               = $d->mata_reflek_pupil;
        $show['Edema Palbebrae']                            = $d->mata_edema_palbebrae;
        $show['Tonsil']                                     = $d->tht_tonsil;
        $show['Faring']                                     = $d->tht_faring;
        $show['Lidah']                                      = $d->tht_lidah;
        $show['Bibir']                                      = $d->tht_bibir;
        $show['JVP']                                        = $d->leher_jvp;
        $show['Pembesaran Kelenjar (Leher)']                = $d->leher_pembesaran_kelenjar;
        $show['Kaku Kuduk']                                 = $d->kaku_kuduk;
        $show['Torak Simetris']                             = $d->torak_simetris;
        $show['Torak Asimetris']                            = $d->torak_asimetris;
        $show['Jantung (S1/S2)']                            = $d->jantung_s1_s2;
        $show['Jantung (Reguler)']                          = $d->jantung_reguler;
        $show['Jantung (Ireguler)']                         = $d->jantung_ireguler;
        $show['Jantung (Murmur)']                           = $d->jantung_murmur;
        $show['Jantung (Lain-lain)']                        = $d->jantung_lain;
        $show['Suara Nafas']                                = $d->paru_suara_nafas;
        $show['Ronki']                                      = $d->paru_ronki;
        $show['Wheezing']                                   = $d->paru_wheezing;
        $show['Paru-paru (Lain-lain)']                      = $d->paru_lain;
        $show['Distensi']                                   = $d->abdomen_distensi;
        $show['Meteorismus']                                = $d->abdomen_meteorismus;
        $show['Peristaltik']                                = $d->abdomen_peristaltik;
        $show['Arcites']                                    = $d->abdomen_arcites;
        $show['Nyeri Tekan']                                = $d->abdomen_nyeri;
        $show['Lokasi Nyeri Tekan']                         = $d->abdomen_nyeri_lokasi;
        $show['Hepar']                                      = $d->hepar;
        $show['Lien']                                       = $d->lien;
        $show['Status Gizi']                                = $d->status_gizi;
        $show['Extremitas (Hangat)']                        = $d->extremitas_hangat;
        $show['Extremitas (Dingin)']                        = $d->extremitas_dingin;
        $show['Extremitas (Edema)']                         = $d->extremitas_edema;
        $show['Extremitas (Lain)']                          = $d->extremitas_lain;
        $show['Rencana Kerja']                              = $d->rencana_kerja;
        $show['Hasil Pemeriksaan Penunjang']                = $d->hasil_periksa_penunjang;
        $show['Diagnosis']                                  = $d->diagnosis;
        $show['Terapi']                                     = $d->terapi;
        $show['Hasil Pembedahan']                           = $d->hasil_pembedahan;
        $show['Rekomendasi (Saran)']                        = $d->rekomendasi;
        $show['Catatan']                                    = $d->catatan;
        $show['Waktu Pulang']                               = self::format("date d M Y: H:i:s", $d->waktu_pulang);
        $show['Tanggal Kontrol Klinik']                     = self::format("date d M Y", $d->tgl_kontrol_klinik);
        $show['Dirawat Di Ruang']                           = $d->dirawat_diruang;
        
        $result="<div class='hide' id='laporan_asesmen_anak_".$d->id."'>".self::format("array-bold-trim", $show)."</div>";
        $array['Pasien']=$array['Pasien'].$result;
        
        return $array;
    }
}

?>