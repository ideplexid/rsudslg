<?php

class LaporanAsesmenPsikologiAdapter extends SimpleAdapter {
    public function adapt($d) {
        $array=parent::adapt($d);
        
        $show=array();
        $show['Pasien']                                     = $d->nama_pasien;
        $show['Jenis Kelamin']                              = $d->jk==0?"Laki-Laki":"Perempuan";
        $show['Alamat']                                     = $d->alamat_pasien;
        $show['NRM']                                        = $d->nrm_pasien;
        $show['No. Reg']                                    = $d->noreg_pasien;
        $show['Tanggal Asesmen']                            = self::format("date d M Y", $d->waktu);
        $show['Tanggal Masuk']                              = self::format("date d M Y", $d->waktu_masuk);
        $show['Ruangan']                                    = self::format("unslug", $d->ruangan);
        $show['Cara Bayar']                                 = $d->carabayar;
        $show['Dokter']                                     = $d->nama_dokter;
        $show['Anak ke-']                                   = $d->anak_ke;
        $show['Dari (Bersaudara)']                          = $d->dari_saudara;
        $show['Status Pernikahan']                          = $d->status_nikah;
        $show['Pendidikan Terakhir']                        = $d->pend_terakhir;
        $show['Observasi']                                  = $d->obeservasi;
        $show['Wawancara']                                  = $d->wawancara;
        $show['Keterangan']                                 = $d->wawancara_ket;
        $show['Keluhan Utama']                              = $d->keluhan_utama;
        $show['Kapan Mulai Terjadi']                        = $d->mulai_terjadi;
        $show['Riwayat Penyakit']                           = $d->riwayat_penyakit;
        $show['Keluhan Psikosomatis']                       = $d->keluhan_psikosomatis;
        $show['Penyebab/Pencetus']                          = $d->penyebab;
        $show['Faktor Keluarga']                            = $d->faktor_keluarga;
        $show['Faktor Pekerjaan']                           = $d->faktor_pekerjaan;
        $show['Faktor Premorbid']                           = $d->faktor_premorboid;
        $show['Riwayat NAPZA']                              = $d->riwayat_napza;
        $show['Lama Pemakaian']                             = $d->napza_lama_pemakaian;
        $show['Jenis Zat']                                  = $d->napza_jenis_zat;
        $show['Cara Pakai']                                 = $d->napza_cara_pakai;
        $show['Latar Belakang Pemakaian']                   = $d->napza_latarbelakang_pakai;
        $show['Kesan Umum']                                 = $d->kesan_umum;
        $show['Kesadaran']                                  = $d->kesadaran;
        $show['Mood/Afek/Ekspresi']                         = $d->mood;
        $show['Proses dan Isi Pikiran']                     = $d->proses_dan_isi_pikiran;
        $show['Kecerdasan']                                 = $d->kecerdasan;
        $show['Dorongan']                                   = $d->dorongan;
        $show['Judgement/Pemahaman']                        = $d->judgment;
        $show['Attention (Perhatian)']                      = $d->attention;
        $show['Psikomotorik']                               = $d->psikomotorik;
        $show['Orientasi Diri']                             = $d->orientasi_diri;
        $show['Orientasi Waktu']                            = $d->orientasi_wkt;
        $show['Orientasi Tempat']                           = $d->orientasi_tempat;
        $show['Orientasi Ruang']                            = $d->orientasi_ruang;
        $show['Berat Badan']                                = $d->bb." kg";
        $show['Tinggi Badan']                               = $d->tb." cm";
        $show['LILA']                                       = $d->lila;
        $show['LK']                                         = $d->lk;
        $show['Status Gizi']                                = $d->status_gizi;
        $show['Tes IQ']                                     = $d->tes_iq;
        $show['Tes Bakat/Minat']                            = $d->tes_bakat;
        $show['Tes Kepribadian']                            = $d->tes_kepribadian;
        $show['Tes Okupasi']                                = $d->tes_okupasi;
        $show['Tes Perkembangan']                           = $d->tes_perkembangan;
        $show['Perkembangan Fisik']                         = $d->perkembangan_fisik;
        $show['Perkembangan Kognitif']                      = $d->perkembangan_kognitif;
        $show['Perkembangan Emosi']                         = $d->perkembangan_emosi;
        $show['Perkembangan Sosial']                        = $d->perkembangan_sosial;
        $show['Perkembangan Motorik']                       = $d->perkembangan_motorik;
        $show['Report']                                     = $d->report;
        $show['Ekspresi']                                   = $d->ekspresi;
        $show['Reaksi']                                     = $d->reaksi;
        $show['Komunikasi']                                 = $d->komunikasi;
        $show['Lain-Lain']                                  = $d->lain_lain;
        $show['Waktu Pulang']                               = self::format("date d M Y: H:i:s", $d->waktu_pulang);
        $show['Tanggal Kontrol Klinik']                     = self::format("date d M Y", $d->waktu_kontrol_klinik);
        $show['Dirawat Di Ruang']                           = $d->dirawat_diruang;
        
        $result="<div class='hide' id='laporan_asesmen_psikologi_".$d->id."'>".self::format("array-bold-trim", $show)."</div>";
        $array['Pasien']=$array['Pasien'].$result;
        
        return $array;
    }
}

?>