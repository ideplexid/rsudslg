<?php

class RekapPulangIGDAdapter extends ArrayAdapter {
    private $jumlah_bedah_rujukan_l         = 0;
    private $jumlah_bedah_rujukan_p         = 0;
    private $jumlah_bedah_nonrujukan_l      = 0;
    private $jumlah_bedah_nonrujukan_p      = 0;
    private $jumlah_bedah_doa_l             = 0;
    private $jumlah_bedah_doa_p             = 0;
    private $jumlah_bedah_dor_l             = 0;
    private $jumlah_bedah_dor_p             = 0;
    
    private $jumlah_nonbedah_rujukan_l      = 0;
    private $jumlah_nonbedah_rujukan_p      = 0;
    private $jumlah_nonbedah_nonrujukan_l   = 0;
    private $jumlah_nonbedah_nonrujukan_p   = 0;
    private $jumlah_nonbedah_doa_l          = 0;
    private $jumlah_nonbedah_doa_p          = 0;
    private $jumlah_nonbedah_dor_l          = 0;
    private $jumlah_nonbedah_dor_p          = 0;
    
    private $jumlah_kebidanan_rujukan_l     = 0;
    private $jumlah_kebidanan_rujukan_p     = 0;
    private $jumlah_kebidanan_nonrujukan_l  = 0;
    private $jumlah_kebidanan_nonrujukan_p  = 0;
    private $jumlah_kebidanan_doa_l         = 0;
    private $jumlah_kebidanan_doa_p         = 0;
    private $jumlah_kebidanan_dor_l         = 0;
    private $jumlah_kebidanan_dor_p         = 0;
    
    public function adapt($d) {
        $pola_kasus = explode(" - ", $d->pola_kasus);
        if($pola_kasus[0] == "Bedah" && $d->rujuk != "" && $d->jk == "0") {
            $this->jumlah_bedah_rujukan_l++;
        }
        if($pola_kasus[0] == "Bedah" && $d->rujuk != "" && $d->jk == "1") {
            $this->jumlah_bedah_rujukan_p++;
        }
        if($pola_kasus[0] == "Bedah" && $d->rujuk == "" && $d->jk == "0") {
            $this->jumlah_bedah_nonrujukan_l++;
        }
        if($pola_kasus[0] == "Bedah" && $d->rujuk == "" && $d->jk == "1") {
            $this->jumlah_bedah_nonrujukan_p++;
        }
        if($pola_kasus[0] == "Bedah" && $d->triage == "Hitam DOA" && $d->jk == "0") {
            $this->jumlah_bedah_doa_l++;
        }
        if($pola_kasus[0] == "Bedah" && $d->triage == "Hitam DOA" && $d->jk == "1") {
            $this->jumlah_bedah_doa_p++;
        }
        if($pola_kasus[0] == "Bedah" && $d->triage == "Hitam Mati" && $d->jk == "0") {
            $this->jumlah_bedah_dor_l++;
        }
        if($pola_kasus[0] == "Bedah" && $d->triage == "Hitam Mati" && $d->jk == "1") {
            $this->jumlah_bedah_dor_p++;
        }
        
        if($pola_kasus[0] == "Medis" && $d->rujuk != "" && $d->jk == "0") {
            $this->jumlah_nonbedah_rujukan_l++;
        }
        if($pola_kasus[0] == "Medis" && $d->rujuk != "" && $d->jk == "1") {
            $this->jumlah_nonbedah_rujukan_p++;
        }
        if($pola_kasus[0] == "Medis" && $d->rujuk == "" && $d->jk == "0") {
            $this->jumlah_nonbedah_nonrujukan_l++;
        }
        if($pola_kasus[0] == "Medis" && $d->rujuk == "" && $d->jk == "1") {
            $this->jumlah_nonbedah_nonrujukan_p++;
        }
        if($pola_kasus[0] == "Medis" && $d->triage == "Hitam DOA" && $d->jk == "0") {
            $this->jumlah_nonbedah_doa_l++;
        }
        if($pola_kasus[0] == "Medis" && $d->triage == "Hitam DOA" && $d->jk == "1") {
            $this->jumlah_nonbedah_doa_p++;
        }
        if($pola_kasus[0] == "Medis" && $d->triage == "Hitam Mati" && $d->jk == "0") {
            $this->jumlah_nonbedah_dor_l++;
        }
        if($pola_kasus[0] == "Medis" && $d->triage == "Hitam Mati" && $d->jk == "1") {
            $this->jumlah_nonbedah_dor_p++;
        }
        
        if($pola_kasus[0] == "VK" && $d->rujuk != "" && $d->jk == "0") {
            $this->jumlah_kebidanan_rujukan_l++;
        }
        if($pola_kasus[0] == "VK" && $d->rujuk != "" && $d->jk == "1") {
            $this->jumlah_kebidanan_rujukan_p++;
        }
        if($pola_kasus[0] == "VK" && $d->rujuk == "" && $d->jk == "0") {
            $this->jumlah_kebidanan_nonrujukan_l++;
        }
        if($pola_kasus[0] == "VK" && $d->rujuk == "" && $d->jk == "1") {
            $this->jumlah_kebidanan_nonrujukan_p++;
        }
        if($pola_kasus[0] == "VK" && $d->triage == "Hitam DOA" && $d->jk == "0") {
            $this->jumlah_kebidanan_doa_l++;
        }
        if($pola_kasus[0] == "VK" && $d->triage == "Hitam DOA" && $d->jk == "1") {
            $this->jumlah_kebidanan_doa_p++;
        }
        if($pola_kasus[0] == "VK" && $d->triage == "Hitam Mati" && $d->jk == "0") {
            $this->jumlah_kebidanan_dor_l++;
        }
        if($pola_kasus[0] == "VK" && $d->triage == "Hitam Mati" && $d->jk == "1") {
            $this->jumlah_kebidanan_dor_p++;
        }
    }
    
    public function getContent($data) {
        foreach($data as $d) {
            $this->adapt($d);
        }
        
        $result = array();
        $result['bedah_rujukan_l']          = $this->jumlah_bedah_rujukan_l;
        $result['bedah_rujukan_p']          = $this->jumlah_bedah_rujukan_p;
        $result['bedah_nonrujukan_l']       = $this->jumlah_bedah_nonrujukan_l;
        $result['bedah_nonrujukan_p']       = $this->jumlah_bedah_nonrujukan_p;
        $result['bedah_doa_l']              = $this->jumlah_bedah_doa_l;
        $result['bedah_doa_p']              = $this->jumlah_bedah_doa_p;
        $result['bedah_dor_l']              = $this->jumlah_bedah_dor_l;
        $result['bedah_dor_p']              = $this->jumlah_bedah_dor_p;
        
        $result['nonbedah_rujukan_l']       = $this->jumlah_nonbedah_rujukan_l;
        $result['nonbedah_rujukan_p']       = $this->jumlah_nonbedah_rujukan_p;
        $result['nonbedah_nonrujukan_l']    = $this->jumlah_nonbedah_nonrujukan_l;
        $result['nonbedah_nonrujukan_p']    = $this->jumlah_nonbedah_nonrujukan_p;
        $result['nonbedah_doa_l']           = $this->jumlah_nonbedah_doa_l;
        $result['nonbedah_doa_p']           = $this->jumlah_nonbedah_doa_p;
        $result['nonbedah_dor_l']           = $this->jumlah_nonbedah_dor_l;
        $result['nonbedah_dor_p']           = $this->jumlah_nonbedah_dor_p;
        
        $result['kebidanan_rujukan_l']      = $this->jumlah_kebidanan_rujukan_l;
        $result['kebidanan_rujukan_p']      = $this->jumlah_kebidanan_rujukan_p;
        $result['kebidanan_nonrujukan_l']   = $this->jumlah_kebidanan_nonrujukan_l;
        $result['kebidanan_nonrujukan_p']   = $this->jumlah_kebidanan_nonrujukan_p;
        $result['kebidanan_doa_l']          = $this->jumlah_kebidanan_doa_l;
        $result['kebidanan_doa_p']          = $this->jumlah_kebidanan_doa_p;
        $result['kebidanan_dor_l']          = $this->jumlah_kebidanan_dor_l;
        $result['kebidanan_dor_p']          = $this->jumlah_kebidanan_dor_p;
        
        $arr = array();
        $arr[0] = $result;
        return $arr;
    }
}

?>