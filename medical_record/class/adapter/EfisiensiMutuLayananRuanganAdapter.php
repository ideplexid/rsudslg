<?php

require_once "medical_record/function/date_range.php";

class EfisiensiMutuLayananRuanganAdapter extends ArrayAdapter {
    private $ruangan = array();
    private $dari;
    private $sampai;
    private $jumlah_tt = 0;
    
    public function setDariSampai($d, $s) {
        $this->dari = $d;
        $this->sampai = $s;
    }
    
    public function setJumlahTT($tt) {
        $this->jumlah_tt = (int)$tt;
    }
    
    public function setRuangan($ruang) {
        $this->ruangan = $ruang;
    }
    
    public function adapt($d) {
        parent::adapt($d);
    }
    
    public function getContent($data) {
        /************ Get range tanggal berdasarkan filter dari, sampai ************/
        $date_range = dateRange($this->dari, $this->sampai);
        $rincian_tgl = array();
        for($i=0; $i<sizeof($date_range); $i++) {
            $rincian_tgl[$i] = $date_range[$i];
        }
        /************  ************/
        
        /************ Get Jumlah Hari ************/
        $dari = new DateTime($this->dari);
        $sampai = new DateTime($this->sampai);
        $jumlah_hari = $dari->diff($sampai)->days;
        /************  ************/
        
        $result = array();
        
        for($i=0; $i<sizeof($this->ruangan); $i++) {
            $result[$i]['ruangan']                  = $this->ruangan[$i]['name'];
            $result[$i]['jumlah_bed']               = $this->ruangan[$i]['jumlah_bed'];
            $result[$i]['masuk_rs_l']               = 0;
            $result[$i]['masuk_rs_p']               = 0;
            $result[$i]['masuk_rs_total']           = 0;
            $result[$i]['keluar_hidup_l']           = 0;
            $result[$i]['keluar_hidup_p']           = 0;
            $result[$i]['keluar_hidup_total']       = 0;
            $result[$i]['mati_k48_l']               = 0;
            $result[$i]['mati_k48_p']               = 0;
            $result[$i]['mati_k48_total']           = 0;
            $result[$i]['mati_l48_l']               = 0;
            $result[$i]['mati_l48_p']               = 0;
            $result[$i]['mati_l48_total']           = 0;
            $result[$i]['hari_perawatan']           = 0;
            $result[$i]['lama_rawat']               = 0;
            $result[$i]['bor']                      = 0;
            
            for($j=0; $j<sizeof($rincian_tgl); $j++) {
                for($k=0; $k<sizeof($data); $k++) {
                    if($this->ruangan[$i]['value'] == $data[$k]['kamar_inap']) {
                        $tgl_inap       = explode(" ", $data[$k]['tanggal_inap']);
                        $tanggal_inap   = $tgl_inap[0];
                        $tgl_pulang     = explode(" ", $data[$k]['tanggal_pulang']);
                        $tanggal_pulang = $tgl_pulang[0];
                        $tgl_masuk      = explode(" ", $data[$k]['tanggal']);
                        $tanggal_masuk  = $tgl_masuk[0];
                        
                        if(($rincian_tgl[$j] >= $tanggal_inap && $rincian_tgl[$j] <= $tanggal_pulang) || ($rincian_tgl[$j] >= $tanggal_inap && $rincian_tgl[$j] <= $tanggal_pulang)) {
                            $result[$i]['hari_perawatan'] ++;
                        }
                        if($tanggal_masuk == $rincian_tgl[$j] && $data[$k]['kelamin'] = "0") {
                            $result[$i]['masuk_rs_l'] ++;
                        }
                        if($tanggal_masuk == $rincian_tgl[$j] && $data[$k]['kelamin'] = "1") {
                            $result[$i]['masuk_rs_p'] ++;
                        }
                        if($tanggal_pulang == $rincian_tgl[$j] && $data[$k]['carapulang'] == "Dipulangkan Hidup" && $data[$k]['kelamin'] == "0") {
                            $result[$i]['keluar_hidup_l'] ++;
                        }
                        if($tanggal_pulang == $rincian_tgl[$j] && $data[$k]['carapulang'] == "Dipulangkan Hidup" && $data[$k]['kelamin'] == "1") {
                            $result[$i]['keluar_hidup_p'] ++;
                        }
                        if($tanggal_pulang == $rincian_tgl[$j] && $data[$k]['carapulang'] == "Dipulangkan Mati <=48 Jam" && $data[$k]['kelamin'] == "0") {
                            $result[$i]['mati_k48_l'] ++;
                        }
                        if($tanggal_pulang == $rincian_tgl[$j] && $data[$k]['carapulang'] == "Dipulangkan Mati <=48 Jam" && $data[$k]['kelamin'] == "1") {
                            $result[$i]['mati_k48_p'] ++;
                        }
                        if($tanggal_pulang == $rincian_tgl[$j] && $data[$k]['carapulang'] == "Dipulangkan Mati >48 Jam" && $data[$k]['kelamin'] == "0") {
                            $result[$i]['mati_l48_l'] ++;
                        }
                        if($tanggal_pulang == $rincian_tgl[$j] && $data[$k]['carapulang'] == "Dipulangkan Mati >48 Jam" && $data[$k]['kelamin'] == "1") {
                            $result[$i]['mati_l48_p'] ++;
                        }
                        if($tanggal_pulang == $rincian_tgl[$j] && $data[$k]['carapulang'] == "Dipulangkan Mati < 1 Jam Post Operasi" && $data[$k]['kelamin'] == "0") {
                            $result[$i]['mati_k1jop_l'] ++;
                        }
                        if($tanggal_pulang == $rincian_tgl[$j] && $data[$k]['carapulang'] == "Dipulangkan Mati < 1 Jam Post Operasi" && $data[$k]['kelamin'] == "1") {
                            $result[$i]['mati_k1jop_p'] ++;
                        }
                    }
                    
                }
            }
            
            $sampai = str_replace('-', '/', $this->sampai);
            $tgl_sampai = date('Y-m-d',strtotime($sampai . "+1 days"));
            for($j=0; $j<sizeof($data); $j++) {
                if($this->ruangan[$i]['value'] == $data[$j]['kamar_inap'] && $data[$j]['tanggal_inap'] != "0000-00-00 00:00:00" && $data[$j]['tanggal_pulang'] != "0000-00-00 00:00:00" && $data[$j]['tanggal_pulang'] <= $tgl_sampai) {
                    $tgl_msk  = new DateTime(date('Y-m-d', strtotime($data[$j]['tanggal_inap'])));
                    $tgl_plg = new DateTime(date('Y-m-d', strtotime($data[$j]['tanggal_pulang'])));
                    $lama_rawat = $tgl_msk->diff($tgl_plg)->days;
                    if($lama_rawat == 0 || $lama_rawat == '0') {
                        $result[$i]['lama_rawat'] += 1;
                    } else {
                        $result[$i]['lama_rawat'] += $lama_rawat;
                    }
                }
            }
            
            $result[$i]['masuk_rs_total']       = $result[$i]['masuk_rs_l'] + $result[$i]['masuk_rs_p'];
            $result[$i]['keluar_hidup_total']   = $result[$i]['keluar_hidup_l'] + $result[$i]['keluar_hidup_p'];
            $result[$i]['mati_k48_total']       = $result[$i]['mati_k48_l'] + $result[$i]['mati_k48_p'];
            $result[$i]['mati_l48_total']       = $result[$i]['mati_l48_l'] + $result[$i]['mati_l48_p'];
            
            /************ Get BOR ************/
            if($this->ruangan[$i]['jumlah_bed'] == 0 || $jumlah_hari == 0) {
                $result[$i]['bor'] = "undefined";
            } else {
                $result[$i]['bor'] = (100 * $result[$i]['hari_perawatan']) / ($this->ruangan[$i]['jumlah_bed'] * $jumlah_hari);
            }
            /************  ************/
        }
        $res = array();
        for($i = 0; $i < sizeof($result); $i++) {
            $res[$i]['ruangan']             = (string)$result[$i]['ruangan'];
            $res[$i]['masuk_rs_l']          = (string)$result[$i]['masuk_rs_l'];
            $res[$i]['masuk_rs_p']          = (string)$result[$i]['masuk_rs_p'];
            $res[$i]['masuk_rs_total']      = (string)$result[$i]['masuk_rs_total'];
            $res[$i]['keluar_hidup_l']      = (string)$result[$i]['keluar_hidup_l'];
            $res[$i]['keluar_hidup_p']      = (string)$result[$i]['keluar_hidup_p'];
            $res[$i]['keluar_hidup_total']  = (string)$result[$i]['keluar_hidup_total'];
            $res[$i]['mati_k48_l']          = (string)$result[$i]['mati_k48_l'];
            $res[$i]['mati_k48_p']          = (string)$result[$i]['mati_k48_p'];
            $res[$i]['mati_k48_total']      = (string)$result[$i]['mati_k48_total'];
            $res[$i]['mati_l48_l']          = (string)$result[$i]['mati_l48_l'];
            $res[$i]['mati_l48_p']          = (string)$result[$i]['mati_l48_p'];
            $res[$i]['mati_l48_total']      = (string)$result[$i]['mati_l48_total'];
            $res[$i]['hari_perawatan']      = (string)$result[$i]['hari_perawatan'];
            $res[$i]['lama_rawat']          = (string)$result[$i]['lama_rawat'];
            $res[$i]['bor']                 = (string)$result[$i]['bor'];
        }
        return $res;
    }
}

?>