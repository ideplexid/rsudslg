<?php 

class DiagnosaAdapter extends SimpleAdapter{
	
	public function adapt($d){
		$array=parent::adapt($d);
		
		$show=array();
		$show['Tanggal']=self::format("date d M Y", $d->tanggal);
		$show['Dokter']=$d->nama_dokter;
		
		$show['Tensi']=$d->tensi." mm/Hg";
		$show['Nadi']=$d->nadi." /Menit";
		$show['Suhu']=$d->suhu." &#176;C";
		$show['Respiration Rate']=$d->rr." /Menit";
		$show['Nyeri']=$d->nyeri;
		$show['Keadaan Umum']=$d->keadaan_umum;
		$show['Keadaan Luka']=$d->keadaan_luka;
		$show['Gula Darah']=$d->gula_darah;
		
		$show['Diagnosa Primer']=$d->diagnosa;
		$show['Diagnosa Sekunder']=$d->keterangan;
		
		$result="<div class='hide' id='diagnosa_data_".$d->id."'>".self::format("array-bold-trim", $show)."</div>";
		$array['Tanggal']=$array['Tanggal'].$result;
        $array['Pasien']=$d->sebutan." ".$d->nama_pasien;

		return $array;
		
	}
	
	
}


?>