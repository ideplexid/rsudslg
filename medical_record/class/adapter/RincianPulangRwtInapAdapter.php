<?php

class RincianPulangRwtInapAdapter extends ArrayAdapter {
    private $result = array();
    private $no = 0;
    private $dari;
    private $sampai;
    private $jumlah_lama_dirawat = 0;
    
    public function setDariSampai($d, $s) {
        $this->dari = $d;
        $this->sampai = $s;
    }
    
    public function adapt($d) {
        $this->no++;
        $res = array();
        $res['no']              = $this->no;
        $res['tanggal']         = ArrayAdapter::dateFormat("date d M Y", $d['tanggal']);
        $res['nrm']             = $d['nrm'];
        $res['noreg']           = $d['id'];
        $res['profile_number']  = $d['profile_number'];
        $res['nama_pasien']     = $d['nama_pasien'];
        if($d['kelamin'] == "0") {
            $res['jk'] = "Laki-laki";
        } else if($d['kelamin'] == "1") {
            $res['jk'] = "Perempuan";
        } else {
            $res['jk'] = "";
        }
        $res['tanggal_inap']    = ArrayAdapter::dateFormat("date d M Y", $d['tanggal_inap']);
        $res['tanggal_pulang']  = ArrayAdapter::dateFormat("date d M Y", $d['tanggal_pulang']);
        $res['carapulang']      = $d['carapulang'];
        
        $sampai = str_replace('-', '/', $this->sampai);
        $tgl_sampai = date('Y-m-d',strtotime($sampai . "+1 days"));
        if($d['tanggal_inap'] != "0000-00-00 00:00:00" && $d['tanggal_pulang'] != "0000-00-00 00:00:00" && $d['tanggal_pulang'] <= $tgl_sampai) {
            $tgl_msk  = new DateTime(date('Y-m-d', strtotime($d['tanggal_inap'])));
            $tgl_plg = new DateTime(date('Y-m-d', strtotime($d['tanggal_pulang'])));
            $lama_rawat = $tgl_msk->diff($tgl_plg)->days;
            if($lama_rawat == 0 || $lama_rawat == '0') {
                $res['jumlah_lama_dirawat'] = 1;
                $this->jumlah_lama_dirawat += 1;
            } else {
                $res['jumlah_lama_dirawat'] = $lama_rawat;
                $this->jumlah_lama_dirawat += $lama_rawat;
            }
        } else {
            $res['jumlah_lama_dirawat'] = "";
        }
        
        return $res;
    }
    
    public function getContent($data) {
        for($i = 0; $i < sizeof($data); $i++) {
            $this->result[] = $this->adapt($data[$i]);
        }
        $arr = array();
        $arr['no']                  = "";
        $arr['tanggal']             = "";
        $arr['nrm']                 = "";
        $arr['noreg']               = "";
        $arr['profile_number']      = "";
        $arr['nama_pasien']         = "TOTAL";
        $arr['jk']                  = "";
        $arr['tanggal_inap']        = "";
        $arr['tanggal_pulang']      = "";
        $arr['carapulang']          = "";
        $arr['jumlah_lama_dirawat'] = $this->jumlah_lama_dirawat;
        
        $this->result[] = $arr;
        
        return $this->result;
    }
}

?>