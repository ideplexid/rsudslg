<?php

class LaporanAsesmenPsikiatriAdapter extends SimpleAdapter {
    public function adapt($d) {
        $array=parent::adapt($d);
        
        $show=array();
        $show['Pasien']                                     = $d->nama_pasien;
        $show['Jenis Kelamin']                              = $d->jk==0?"Laki-Laki":"Perempuan";
        $show['Alamat']                                     = $d->alamat_pasien;
        $show['NRM']                                        = $d->nrm_pasien;
        $show['No. Reg']                                    = $d->noreg_pasien;
        $show['Tanggal Asesmen']                            = self::format("date d M Y", $d->waktu);
        $show['Tanggal Masuk']                              = self::format("date d M Y", $d->waktu_masuk);
        $show['Ruangan']                                    = self::format("unslug", $d->ruangan);
        $show['Cara Bayar']                                 = $d->carabayar;
        $show['Dokter']                                     = $d->nama_dokter;
        $show['Keluhan Utama']                              = $d->keluhan_utama;
        $show['Riwayat Penyakit Sekarang']                  = $d->riwayat_penyakit_skrg;
        $show['Auto Anamnesis']                             = $d->auto_anamnesis;
        $show['Hetero Namnesis']                            = $d->hetero_namnesis;
        $show['Faktor Penyetus/Penyebab']                   = $d->faktor_penyebab;
        $show['Faktor Keluarga']                            = $d->faktor_keluarga;
        $show['Fungsi Kerja/Sosial']                        = $d->fungsi_kerja;
        $show['Riwayat NAPZA']                              = $d->riwayat_napza;
        $show['Lama Pemakaian']                             = $d->napza_lama_pemakaian;
        $show['Janis Zat']                                  = $d->napza_jenis_zat;
        $show['Cara Pemakaian']                             = $d->napza_cara_pemakaian;
        $show['Latar Belakang Pemakaian']                   = $d->napza_latarbelakang_pemakaian;
        $show['Faktor Premorbid']                           = $d->faktor_premorbid;
        $show['Faktor Organik']                             = $d->faktor_organik;
        $show['Kesan Umum']                                 = $d->kesan_umum;
        $show['Kesadaran']                                  = $d->kesadaran;
        $show['Mood/Afek']                                  = $d->mood;
        $show['Proses Pikir']                               = $d->proses_pikir;
        $show['Pencerapan']                                 = $d->pencerapan;
        $show['Dorongan Insting']                           = $d->dorongan_insting;
        $show['Psikomotor']                                 = $d->psikomotor;
        $show['Penyakit Dalam']                             = $d->penyakit_dalam;
        $show['Neurologi']                                  = $d->neurologi;
        $show['Diagnosis Kerja']                            = $d->diagnosis_kerja;
        $show['Terapi']                                     = $d->terapi;
        $show['Rencana Kerja']                              = $d->rencana_kerja;
        $show['Waktu Pulang']                               = self::format("date d M Y: H:i:s", $d->waktu_pulang);
        $show['Tanggal Kontrol Klinik']                     = self::format("date d M Y", $d->waktu_kontrol_klinik);
        $show['Dirawat Di Ruang']                           = $d->dirawat_diruang;
        
        $result="<div class='hide' id='laporan_asesmen_psikiatri_".$d->id."'>".self::format("array-bold-trim", $show)."</div>";
        $array['Pasien']=$array['Pasien'].$result;
        
        return $array;
    }
}

?>