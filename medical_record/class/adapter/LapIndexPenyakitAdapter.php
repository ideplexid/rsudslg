<?php

class LapIndexPenyakitAdapter extends ArrayAdapter {
    private $no     = 0;
    private $total  = 0;
    
    public function adapt($d) {
        $this->no++;
        $this->total++;
        $array = array();
        $array['No.']           = $this->no;
        $array['Tanggal']       = ArrayAdapter::dateFormat("date d M Y", $d->tanggal);
        $array['NRM']           = $d->nrm_pasien;
        $array['Nama Pasien']   = $d->nama_pasien;
        $array['Alamat']        = $d->alamat;
        $array['kode_icd']      = $d->kode_icd;
        $array['Tgl Lahir']     = ArrayAdapter::dateFormat("date d M Y", $d->tgl_lahir);
        $array['Ruangan']       = ArrayAdapter::slugFormat("unslug", $d->ruangan);
        $array['Masuk']         = ArrayAdapter::dateFormat("date d M Y", $d->tgl_masuk);
        $array['Keluar']        = ArrayAdapter::dateFormat("date d M Y", $d->tgl_keluar);
        if($array['Masuk'] != "" || $array['Keluar'] != ""){
            $lama_rawat             = date_diff($d->tgl_masuk, $d->tgl_keluar);
            $array['Lama Dirawat']  = $lama_rawat. " Hari";
        } else {
            $array['Lama Dirawat']  = "0 Hari";
        }
        
        if($d->jk == "0") {
            $array['L']         = "<i class='fa fa-check'></>";
            $array['P']         = "";
        } else {
            $array['L']         = "";
            $array['P']         = "<i class='fa fa-check'></>";
        }
        
        $array['0 - 28 HR']     = "";
        $array['28 HR - 1 TH']  = "";
        $array['1-4 TH']        = "";
        $array['5-14 TH']       = "";
        $array['15-24 TH']      = "";
        $array['25-44 TH']      = "";
        $array['45-65 TH']      = "";
        $array['65 TH+']        = "";
        if($d->gol_umur == "0 - 28 HR") {
            $array['0 - 28 HR']     = "<i class='fa fa-check'></>";
        } else if($d->gol_umur == "28 HR - 1 TH") {
            $array['28 HR - 1 TH']  = "<i class='fa fa-check'></>";
        } else if($d->gol_umur == "1-4 TH") {
            $array['1-4 TH']        = "<i class='fa fa-check'></>";
        } else if($d->gol_umur == "5-14 TH") {
            $array['5-14 TH']       = "<i class='fa fa-check'></>";
        } else if($d->gol_umur == "15-24 TH") {
            $array['15-24 TH']      = "<i class='fa fa-check'></>";
        } else if($d->gol_umur == "25-44 TH") {
            $array['25-44 TH']      = "<i class='fa fa-check'></>";
        } else if($d->gol_umur == "45-65 TH") {
            $array['45-65 TH']      = "<i class='fa fa-check'></>";
        } else if($d->gol_umur == ">65 TH") {
            $array['65 TH+']        = "<i class='fa fa-check'></>";
        }
        
        $array['Diagnosa']          = $d->nama_icd;
        $array['Kelas']             = ArrayAdapter::slugFormat("unslug", $d->kelas);
        
        $array['Dipulangkan Mati <=48 Jam'] = "";
        $array['Dipulangkan Mati >48 Jam']  = "";
        $array['Pulang Paksa']              = "";
        $array['Kabur']                     = "";
        $array['Rujuk RS Lain']             = "";
        $array['Dikembalikan ke Perujuk']   = "";
        $array['Dipulangkan Hidup']         = "";
        if($d->carapulang == "Dipulangkan Mati <=48 Jam" || $d->carapulang == "Dipulangkan Mati < 1 Jam Post Operasi") {
            $array['Dipulangkan Mati <=48 Jam'] = "<i class='fa fa-check'></>";
        } else if($d->carapulang == "Dipulangkan Mati >48 Jam") {
            $array['Dipulangkan Mati >48 Jam']  = "<i class='fa fa-check'></>";
        } else if($d->carapulang == "Pulang Paksa") {
            $array['Pulang Paksa']              = "<i class='fa fa-check'></>";
        } else if($d->carapulang == "Kabur") {
            $array['Kabur']                     = "<i class='fa fa-check'></>";
        } else if($d->carapulang == "Rujuk RS Lain") {
            $array['Rujuk RS Lain']             = "<i class='fa fa-check'></>";
        } else if($d->carapulang == "Dikembalikan ke Perujuk") {
            $array['Dikembalikan ke Perujuk']   = "<i class='fa fa-check'></>";
        } else if($d->carapulang == "Dipulangkan Hidup") {
            $array['Dipulangkan Hidup']         = "<i class='fa fa-check'></>";
        }
        
        $array['Rujukan Dokter']        = "";
        $array['Rujukan Bidan']         = "";
        $array['Rujukan Rumah Sakit']   = "";
        $array['Datang Sendiri']        = "";
        if($d->rujukan == 'Dokter') {
            $array['Rujukan Dokter']        = "<i class='fa fa-check'></>";
        } else if($d->rujukan == 'Bidan') {
            $array['Rujukan Bidan']         = "<i class='fa fa-check'></>";
        } else if($d->rujukan == 'RS Lain') {
            $array['Rujukan Rumah Sakit']   = "<i class='fa fa-check'></>";
        } else if($d->rujukan == '') {
            $array['Datang Sendiri']        = "<i class='fa fa-check'></>";
        }
        
        return $array;
    }
    
    public function getContent($data) {
        $result = parent::getContent ( $data );
        
        $array = array();
        $array['No.']                       = "";
        $array['Tanggal']                   = "TOTAL";
        $array['NRM']                       = $this->total;
        $array['Nama Pasien']               = "";
        $array['Alamat']                    = "";
        $array['kode_icd']                  = "";
        $array['Tgl Lahir']                 = "";
        $array['Ruangan']                   = "";
        $array['Masuk']                     = "";
        $array['Keluar']                    = "";
        $array['Lama Dirawat']              = "";
        $array['L']                         = "";
        $array['P']                         = "";
        $array['0 - 28 HR']                 = "";
        $array['28 HR - 1 TH']              = "";
        $array['1-4 TH']                    = "";
        $array['5-14 TH']                   = "";
        $array['15-24 TH']                  = "";
        $array['25-44 TH']                  = "";
        $array['45-65 TH']                  = "";
        $array['65 TH+']                    = "";
        $array['Diagnosa']                  = "";
        $array['Kelas']                     = "";
        $array['Dipulangkan Mati <=48 Jam'] = "";
        $array['Dipulangkan Mati >48 Jam']  = "";
        $array['Pulang Paksa']              = "";
        $array['Kabur']                     = "";
        $array['Rujuk RS Lain']             = "";
        $array['Dikembalikan ke Perujuk']   = "";
        $array['Dipulangkan Hidup']         = "";
        $array['Rujukan Dokter']            = "";
        $array['Rujukan Bidan']             = "";
        $array['Rujukan Rumah Sakit']       = "";
        $array['Datang Sendiri']            = "";
        
        $result[] = $array;
        return $result;
    }
}

?>