<?php

class LapGigiMulutAdapter extends ArrayAdapter{
	
	public function adapt($onerow){
		return NULL;
	}
	
	public function getContent($data){
		$content=array();
		$x=$data[0];
		$array=array();
		$array[1]="tumpatan_gigi_tetap";
		$array[2]="tumpatan_gigi_sulung";
		$array[3]="pengobatan_pulpa";
		$array[4]="pencabutan_gigi_tetap";
		$array[5]="pencabutan_gigi_sulung";
		$array[6]="pengobatan_periodontal";
		$array[7]="pengobatan_abses";
		$array[8]="pembersihan_karang_gigi";
		$array[9]="prothese_lengkap";
		$array[10]="prothese_sebagian";
		$array[11]="prothese_cekat";
		$array[12]="orthodonti";
		$array[13]="jacket_bridge";
		$array[14]="bedah_mulut";
		$total;
		foreach($array as $no=>$name){
			$one=array();
			$one["No."]=$no.".";
			$one["Layanan"]=self::format("unslug", $name);
			$one["Total"]=$x[$name];
			$total+=$x[$name];
			$content[]=$one;
		}
		$one=array();
		$one["No."]="<strong>99.</strong>";
		$one["Layanan"]="<strong>TOTAL</strong>";
		$one["Total"]="<strong>".$total."</strong>";
		$content[]=$one;
		return $content;
	}
	
}