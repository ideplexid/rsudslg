<?php

class LapPKhususAdapter extends ArrayAdapter{
	
	public function adapt($onerow){
		return NULL;
	}
	
	public function getContent($data){
		$content=array();
		$x=$data[0];
		$array=array();
		$array[1]="eeg";
		$array[2]="ekg";
		$array[3]="emg";
		$array[4]="ecg";
		$array[5]="endoskopi";
		$array[6]="hemodialisa";
		$array[7]="densometri_tulang";
		$array[8]="koreksi_fraktur";
		$array[9]="pungsi";
		$array[10]="spirometri";
		$array[11]="tes_kulit";
		$array[12]="topometri";
		$array[13]="tredmil";
		$array[14]="akupuntur";
		$array[15]="hiperbarik";
		$array[16]="herbal";
		$array[17]="lain";
		$total;
		foreach($array as $no=>$name){
			$one=array();
			$one["No."]=$no.".";
			$one["Layanan"]=self::format("unslug", $name);
			$one["Total"]=$x[$name];
			$total+=$x[$name];
			$content[]=$one;
		}
		$one=array();
		$one["No."]="<strong>99.</strong>";
		$one["Layanan"]="<strong>TOTAL</strong>";
		$one["Total"]="<strong>".$total."</strong>";
		$content[]=$one;
		return $content;
	}
	
}

?>