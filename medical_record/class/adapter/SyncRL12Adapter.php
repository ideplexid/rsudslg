<?php 

/**
 * @author goblooge
 * digunakan untuk mem-parsing perubahan data dari
 * service, adapun rumusnya sebegai berikut
 * 
 * 
 * bor = jhp * 100% / (jtt*jh)
 * los = jhp / jp
 * toi = (jtt * jh - jhp)/jp
 * bto = jp/jtt
 * ndr = jpm48 * 100% / jp
 * gdr = jpm *100%/ jp
 */

class SyncRL12Adapter extends ArrayAdapter {
	public function adapt($d) {
		$a = array ();
		$a ['jhp'] = $d ['JHP'];
		$a ['jtt'] = $d ['JTT'];
		$a ['jh'] = $d ['JH'];
		$a ['jp'] = $d ['JP'];
		$a ['jph'] = $d ['JPH'];
		$a ['jpm48'] = $d ['JPM48'];
		$a ['jpm'] = $d ['JPM'];
		$a ['jph_intern'] = $d ['JPH_INTERN'];
		$a ['jph_extern'] = $d ['JPH_EXTERN'];
		$a ['jph_start_inside'] = $d ['JPH_START_INSIDE'];
		$a ['jph_end_inside'] = $d ['JPH_END_INSIDE'];
		$a ['ruangan'] = $d ['RUANGAN'];
		$a ['grup_ruangan'] = $d ['GRUP_RUANGAN'];
		$a ['kelas'] = $d ['KELAS'];
		$a ['bor']=($d ['JHP']*100)/($d ['JTT']*$d ['JH']);
		$a ['los'] = $d ['JHP']/$d ['JP'];
		$a ['toi'] = ($d ['JTT']*$d ['JH']-$d['JHP'])/$d ['JP'];
		$a ['bto'] = $d['JP']/$d['JTT'];
		$a ['ndr'] = $d['JPM48']*1000/$d['JP'];
		$a ['gdr'] = $d['JPM']*1000/$d['JP'];
		$a ['rata_hari'] = $d['JP']/$d['JH'];
		$a ['rata_bulan'] = $d['JP']*30/$d['JH'];
		return $a;
	}
}

?>