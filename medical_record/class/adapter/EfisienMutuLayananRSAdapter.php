<?php

require_once "medical_record/function/date_range.php";

class EfisienMutuLayananRSAdapter extends ArrayAdapter {
    private $dari;
    private $sampai;
    private $jumlah_tt = 0;
    
    public function setDariSampai($d, $s) {
        $this->dari = $d;
        $this->sampai = $s;
    }
    
    public function setJumlahTT($tt) {
        $this->jumlah_tt = (int)$tt;
    }
    
    public function adapt($d) {
        parent::adapt($d);
    }
    
    public function getContent($data) {
        /************ Get range tanggal berdasarkan filter dari, sampai ************/
        $date_range = dateRange($this->dari, $this->sampai);
        $rincian_tgl = array();
        for($i=0; $i<sizeof($date_range); $i++) {
            $rincian_tgl[$i] = $date_range[$i];
        }
        /************  ************/
        
        /************ Get Jumlah Hari Perawatan ************/
        $jumlah_hari_perawatan      = 0;
        $jumlah_pasien_keluar       = 0;
        $jumlah_keluar_hidup_l      = 0;
        $jumlah_keluar_hidup_p      = 0;
        $jumlah_keluar_hidup        = 0;
        $jumlah_mati_k48_l          = 0;
        $jumlah_mati_k48_p          = 0;
        $jumlah_mati_l48_l          = 0;
        $jumlah_mati_l48_p          = 0;
        $jumlah_mati_k1jop_l        = 0;
        $jumlah_mati_k1jop_p        = 0;
        $jumlah_keluar_mati_l       = 0;
        $jumlah_keluar_mati_p       = 0;
        $jumlah_keluar_mati         = 0;
        
        for($i=0; $i<sizeof($rincian_tgl); $i++) {
            for($j=0; $j<sizeof($data); $j++) {
                $tgl_inap = explode(" ", $data[$j]['tanggal_inap']);
                $tanggal_inap = $tgl_inap[0];
                $tgl_pulang = explode(" ", $data[$j]['tanggal_pulang']);
                $tanggal_pulang = $tgl_pulang[0];
                $tgl_masuk = explode(" ", $data[$j]['tanggal']);
                $tanggal_masuk = $tgl_masuk[0];
                
                if(($rincian_tgl[$i] >= $tanggal_inap && $rincian_tgl[$i] <= $tanggal_pulang) || ($rincian_tgl[$i] >= $tanggal_inap && $rincian_tgl[$i] <= $tanggal_pulang)) {
                    $jumlah_hari_perawatan ++;
                }
                if($tanggal_pulang == $rincian_tgl[$i] && $data[$j]['carapulang'] != "") {
                    $jumlah_pasien_keluar ++;
                }
                if($tanggal_pulang == $rincian_tgl[$i] && $data[$j]['carapulang'] == "Dipulangkan Hidup" && $data[$j]['kelamin'] == "0") {
                    $jumlah_keluar_hidup_l ++;
                }
                if($tanggal_pulang == $rincian_tgl[$i] && $data[$j]['carapulang'] == "Dipulangkan Hidup" && $data[$j]['kelamin'] == "1") {
                    $jumlah_keluar_hidup_p ++;
                }
                if($tanggal_pulang == $rincian_tgl[$i] && $data[$j]['carapulang'] == "Dipulangkan Mati <=48 Jam" && $data[$j]['kelamin'] == "0") {
                    $jumlah_mati_k48_l ++;
                }
                if($tanggal_pulang == $rincian_tgl[$i] && $data[$j]['carapulang'] == "Dipulangkan Mati <=48 Jam" && $data[$j]['kelamin'] == "1") {
                    $jumlah_mati_k48_p ++;
                }
                if($tanggal_pulang == $rincian_tgl[$i] && $data[$j]['carapulang'] == "Dipulangkan Mati >48 Jam" && $data[$j]['kelamin'] == "0") {
                    $jumlah_mati_l48_l ++;
                }
                if($tanggal_pulang == $rincian_tgl[$i] && $data[$j]['carapulang'] == "Dipulangkan Mati >48 Jam" && $data[$j]['kelamin'] == "1") {
                    $jumlah_mati_l48_p ++;
                }
                if($tanggal_pulang == $rincian_tgl[$i] && $data[$j]['carapulang'] == "Dipulangkan Mati < 1 Jam Post Operasi" && $data[$j]['kelamin'] == "0") {
                    $jumlah_mati_k1jop_l ++;
                }
                if($tanggal_pulang == $rincian_tgl[$i] && $data[$j]['carapulang'] == "Dipulangkan Mati < 1 Jam Post Operasi" && $data[$j]['kelamin'] == "1") {
                    $jumlah_mati_k1jop_p ++;
                }
            }
        }
        $jumlah_keluar_hidup    = $jumlah_keluar_hidup_l + $jumlah_keluar_hidup_p;
        $jumlah_keluar_mati_l   = $jumlah_mati_k48_l + $jumlah_mati_l48_l + $jumlah_mati_k1jop_l;
        $jumlah_keluar_mati_p   = $jumlah_mati_k48_p + $jumlah_mati_l48_p + $jumlah_mati_k1jop_p;
        $jumlah_keluar_mati     = $jumlah_keluar_mati_l + $jumlah_keluar_mati_p;
        /************  ************/
        
        /************ Get Jumlah Lama Dirawat ************/
        $sampai = str_replace('-', '/', $this->sampai);
        $tgl_sampai = date('Y-m-d',strtotime($sampai . "+1 days"));
        for($j=0; $j<sizeof($data); $j++) {
            if($data[$j]['tanggal_inap'] != "0000-00-00 00:00:00" && $data[$j]['tanggal_pulang'] != "0000-00-00 00:00:00" && $data[$j]['tanggal_pulang'] <= $tgl_sampai) {
                $tgl_msk  = new DateTime(date('Y-m-d', strtotime($data[$j]['tanggal_inap'])));
                $tgl_plg = new DateTime(date('Y-m-d', strtotime($data[$j]['tanggal_pulang'])));
                $lama_rawat = $tgl_msk->diff($tgl_plg)->days;
                if($lama_rawat == 0 || $lama_rawat == '0') {
                    $jumlah_lama_dirawat += 1;
                } else {
                    $jumlah_lama_dirawat += $lama_rawat;
                }
            }
        }
        /************  ************/
        
        /************ Get Jumlah Hari ************/
        $dari = new DateTime($this->dari);
        $sampai = new DateTime($this->sampai);
        $jumlah_hari = $dari->diff($sampai)->days;
        /************  ************/
        
        /************ Get BOR ************/
        $bor = (100 * $jumlah_hari_perawatan) / ($this->jumlah_tt * $jumlah_hari);
        /************  ************/
        
        /************ Get ALOS ************/
        $alos = $jumlah_lama_dirawat / $jumlah_pasien_keluar;
        /************  ************/
        
        /************ Get TOI ************/
        $toi = (($this->jumlah_tt * $jumlah_hari) - $jumlah_hari_perawatan) / $jumlah_pasien_keluar;
        /************  ************/
        
        /************ Get TOI ************/
        $bto = ($jumlah_keluar_hidup + $jumlah_keluar_mati) / $this->jumlah_tt;
        /************  ************/
        
        /************ Get GDR Laki-Laki ************/
        $gdr_l = (1000 * $jumlah_keluar_mati_l) / ($jumlah_keluar_hidup_l + $jumlah_keluar_mati_l);
        /************  ************/
        
        /************ Get GDR Perempuan ************/
        $gdr_p = (1000 * $jumlah_keluar_mati_p) / ($jumlah_keluar_hidup_p + $jumlah_keluar_mati_p);
        /************  ************/
        
        /************ Get GDR TOTAL ************/
        $gdr_total = (1000 * $jumlah_keluar_mati) / ($jumlah_keluar_hidup + $jumlah_keluar_mati);
        /************  ************/
        
        /************ Get NDR Laki-Laki ************/
        $ndr_l = (1000 * $jumlah_mati_l48_l) / ($jumlah_keluar_hidup_l + $jumlah_keluar_mati_l);
        /************  ************/
        
        /************ Get NDR Perempuan ************/
        $ndr_p = (1000 * $jumlah_mati_l48_p) / ($jumlah_keluar_hidup_p + $jumlah_keluar_mati_p);
        /************  ************/
        
        /************ Get NDR Total ************/
        $ndr_total = (1000 * ($jumlah_mati_l48_l + $jumlah_mati_l48_p)) / ($jumlah_keluar_hidup + $jumlah_keluar_mati);
        /************  ************/
        
        $result = array();
        $i = 0;
        $result[$i]['NO.'] = "1.";
        $result[$i]['URAIAN'] = "BOR RS (%)";
        $result[$i]['PENCAPAIAN'] = (string)$bor;
        $result[$i]['STANDAR'] = "60 - 85";
        
        $i++;
        $result[$i]['NO.'] = "2.";
        $result[$i]['URAIAN'] = "TOI (Hari)";
        $result[$i]['PENCAPAIAN'] = (string)$toi;
        $result[$i]['STANDAR'] = "1 - 3";
        
        $i++;
        $result[$i]['NO.'] = "3.";
        $result[$i]['URAIAN'] = "BTO (Hari)";
        $result[$i]['PENCAPAIAN'] = (string)$bto;
        $result[$i]['STANDAR'] = "40 - 50";
        
        $i++;
        $result[$i]['NO.'] = "4.";
        $result[$i]['URAIAN'] = "ALOS (Hari)";
        $result[$i]['PENCAPAIAN'] = (string)$alos;
        $result[$i]['STANDAR'] = "6 - 9";
        
        $i++;
        $result[$i]['NO.'] = "5.";
        $result[$i]['URAIAN'] = "GDR";
        $result[$i]['PENCAPAIAN'] = (string)$gdr_total;
        $result[$i]['STANDAR'] = "< 45";
        
        $i++;
        $result[$i]['NO.'] = "";
        $result[$i]['URAIAN'] = "a. Laki-Laki";
        $result[$i]['PENCAPAIAN'] = (string)$gdr_l;
        $result[$i]['STANDAR'] = "< 45";
        
        $i++;
        $result[$i]['NO.'] = "";
        $result[$i]['URAIAN'] = "b. Perempuan";
        $result[$i]['PENCAPAIAN'] = (string)$gdr_p;
        $result[$i]['STANDAR'] = "< 45";
        
        $i++;
        $result[$i]['NO.'] = "6.";
        $result[$i]['URAIAN'] = "NDR";
        $result[$i]['PENCAPAIAN'] = (string)$ndr_total;
        $result[$i]['STANDAR'] = "< 25";
        
        $i++;
        $result[$i]['NO.'] = "";
        $result[$i]['URAIAN'] = "a. Laki-Laki";
        $result[$i]['PENCAPAIAN'] = (string)$ndr_l;
        $result[$i]['STANDAR'] = "< 25";
        
        $i++;
        $result[$i]['NO.'] = "";
        $result[$i]['URAIAN'] = "b. Perempuan";
        $result[$i]['PENCAPAIAN'] = (string)$ndr_p;
        $result[$i]['STANDAR'] = "< 25";
        
        return $result;
    }
}

?>