<?php

class LapPersalinanAdapter extends ArrayAdapter {
    public function adapt($d) {
        $this->number++;
        $a = array();
        
        $a['no']                    = $this->number;
        $a['tanggal']               = ArrayAdapter::dateFormat("date d M Y", $d->tanggal);
        $a['nama_pasien']           = $d->nama_pasien;
        $a['nrm_pasien']            = $d->nrm_pasien;
        $a['noreg_pasien']          = $d->noreg_pasien;
        $a['alamat']                = $d->alamat;
        $a['umur_pasien']           = $d->umur_pasien;
        $a['hamil_ke']              = $d->hamil_ke;
        $a['umur_kehamilan']        = $d->umur_kehamilan;
        $a['letak_janin_kep']       = $d->letak_janin == "Kep" ? "1" : "";
        $a['letak_janin_su']        = $d->letak_janin == "SU" ? "1" : "";
        $a['letak_janin_lin']       = $d->letak_janin == "Lin" ? "1" : "";
        $a['metode_spt']            = $d->metode == "Partus" ? "1" : "";
        $a['metode_ve']             = $d->metode == "Vacuum Extraction" ? "1" : "";
        $a['metode_sc']             = $d->metode == "Sectio Caesaria" ? "1" : "";
        $a['metode_forcep']         = $d->metode == "Forcep" ? "1" : "";
        $a['metode_oksitosin']      = $d->metode == "Oksitosin" ? "1" : "";
        $a['metode_amniotomi']      = $d->metode == "Amniotomi" ? "1" : "";
        $a['bayi_lahir_hidup']      = $d->kondisi_bayi == "Lahir Hidup" ? "1" : "";
        $a['bayi_lk']               = $d->jk_bayi == "0" ? "1" : "";
        $a['bayi_pr']               = $d->jk_bayi == "1" ? "1" : "";
        $a['bayi_kembar']           = $d->bayi_kembar;
        $a['bayi_mati_krg_7_hr']    = $d->kondisi_bayi == "Mati < 7 hari" ? "1" : "";
        $a['bayi_mati_lbh_7_hr']    = $d->kondisi_bayi == "Mati > 7 hari" ? "1" : "";
        $a['ibu_mati']              = $d->ibu_meninggal;
        $a['rujukan']               = $d->kedatangan;
        
        return $a;
    }
}

?>