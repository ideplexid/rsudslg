<?php

require_once "medical_record/function/date_range.php";

class LaporanPPIHarianPerRuangAdapter extends ArrayAdapter {
    private $dari;
    private $sampai;
    private $data_jenis_perawatan;
    private $data_infeksi;
    
    public function setDariSampai($d, $s) {
        $this->dari = $d;
        $this->sampai = $s;
    }
    
    public function setDataJenisPerawatan($data) {
        $this->data_jenis_perawatan = $data;
    }
    
    public function setDataInfeksi($data) {
        $this->data_infeksi = $data;
    }
    
    public function adapt($d) {
        parent::adapt($d);
    }
    
    public function getContent() {
        /************ Get range tanggal berdasarkan filter dari, sampai ************/
        $date_range = dateRange($this->dari, $this->sampai);
        /************  ************/
        
        $arr = array();
        for($i=0; $i<sizeof($date_range); $i++) {
            $tanggal = explode("-", $date_range[$i]);
            $arr[$tanggal[2]]['ivl'] = 0;
            $arr[$tanggal[2]]['cvl'] = 0;
            $arr[$tanggal[2]]['uc'] = 0;
            $arr[$tanggal[2]]['ett'] = 0;
            $arr[$tanggal[2]]['pasien_tirah_baring'] = 0;
            $arr[$tanggal[2]]['luka_bersih'] = 0;
            $arr[$tanggal[2]]['luka_kotor'] = 0;
            $arr[$tanggal[2]]['iadp'] = 0;
            $arr[$tanggal[2]]['vap'] = 0;
            $arr[$tanggal[2]]['isk'] = 0;
            $arr[$tanggal[2]]['plebitis'] = 0;
            $arr[$tanggal[2]]['decubitus'] = 0;
            $arr[$tanggal[2]]['ido'] = 0;
            //$arr[$tanggal[2]]['index_rate'] = 0;
            
            for($j=0; $j<sizeof($this->data_jenis_perawatan); $j++) {
                $tanggal_jp = explode(" ", $this->data_jenis_perawatan[$j]->tanggal);
                $tgl_jp = $tanggal_jp[0];
                if($date_range[$i] == $tgl_jp && $this->data_jenis_perawatan[$j]->jenis_perawatan == "IVL") {
                    $arr[$tanggal[2]]['ivl'] ++;
                }
                if($date_range[$i] == $tgl_jp && $this->data_jenis_perawatan[$j]->jenis_perawatan == "CVL") {
                    $arr[$tanggal[2]]['cvl'] ++;
                }
                if($date_range[$i] == $tgl_jp && $this->data_jenis_perawatan[$j]->jenis_perawatan == "UC") {
                    $arr[$tanggal[2]]['uc'] ++;
                }
                if($date_range[$i] == $tgl_jp && $this->data_jenis_perawatan[$j]->jenis_perawatan == "ETT") {
                    $arr[$tanggal[2]]['ett'] ++;
                }
                if($date_range[$i] == $tgl_jp && $this->data_jenis_perawatan[$j]->jenis_perawatan == "Pasien Tirah Baring") {
                    $arr[$tanggal[2]]['pasien_tirah_baring'] ++;
                }
                if($date_range[$i] == $tgl_jp && $this->data_jenis_perawatan[$j]->jenis_luka == "Luka Bersih") {
                    $arr[$tanggal[2]]['luka_bersih'] ++;
                }
                if($date_range[$i] == $tgl_jp && $this->data_jenis_perawatan[$j]->jenis_luka == "Luka Kotor") {
                    $arr[$tanggal[2]]['luka_kotor'] ++;
                }
            }
            
            for($j=0; $j<sizeof($this->data_infeksi); $j++) {
                $tanggal_i = explode(" ", $this->data_infeksi[$j]->tanggal);
                $tgl_i = $tanggal_i[0];
                if($date_range[$i] == $tgl_i && $this->data_infeksi[$j]->jenis_infeksi == "IADP") {
                    $arr[$tanggal[2]]['iadp'] ++;
                }
                if($date_range[$i] == $tgl_i && $this->data_infeksi[$j]->jenis_infeksi == "VAP") {
                    $arr[$tanggal[2]]['vap'] ++;
                }
                if($date_range[$i] == $tgl_i && $this->data_infeksi[$j]->jenis_infeksi == "ISK") {
                    $arr[$tanggal[2]]['isk'] ++;
                }
                if($date_range[$i] == $tgl_i && $this->data_infeksi[$j]->jenis_infeksi == "Plebitis") {
                    $arr[$tanggal[2]]['plebitis'] ++;
                }
                if($date_range[$i] == $tgl_i && $this->data_infeksi[$j]->jenis_infeksi == "Decubitus") {
                    $arr[$tanggal[2]]['decubitus'] ++;
                }
                if($date_range[$i] == $tgl_i && $this->data_infeksi[$j]->jenis_infeksi == "IDO") {
                    $arr[$tanggal[2]]['ido'] ++;
                }
            }
        }
        
        $res = array();
        $index                  = 0;
        $ivl                    = 0;
        $cvl                    = 0;
        $uc                     = 0;
        $ett                    = 0;
        $pasien_tirah_baring    = 0;
        $luka_bersih            = 0;
        $iadp                   = 0;
        $vap                    = 0;
        $isk                    = 0;
        $plebitis               = 0;
        $decubitus              = 0;
        $ido                    = 0;
        foreach($arr as $a => $b) {
            $res[$index]['tanggal']             = $a;
            $res[$index]['ivl']                 = (string)$arr[$a]['ivl'];
            $ivl                                += $arr[$a]['ivl'];
            $res[$index]['cvl']                 = (string)$arr[$a]['cvl'];
            $cvl                                += $arr[$a]['cvl'];
            $res[$index]['uc']                  = (string)$arr[$a]['uc'];
            $uc                                 += $arr[$a]['uc'];
            $res[$index]['ett']                 = (string)$arr[$a]['ett'];
            $ett                                += $arr[$a]['ett'];
            $res[$index]['pasien_tirah_baring'] = (string)$arr[$a]['pasien_tirah_baring'];
            $pasien_tirah_baring                += $arr[$a]['pasien_tirah_baring'];
            $res[$index]['luka_bersih']         = (string)$arr[$a]['luka_bersih'];
            $luka_bersih                        += $arr[$a]['luka_bersih'];
            $res[$index]['luka_kotor']          = (string)$arr[$a]['luka_kotor'];
            $res[$index]['iadp']                = (string)$arr[$a]['iadp'];
            $iadp                               += $arr[$a]['iadp'];
            $res[$index]['vap']                 = (string)$arr[$a]['vap'];
            $vap                                += $arr[$a]['vap'];
            $res[$index]['isk']                 = (string)$arr[$a]['isk'];
            $isk                                += $arr[$a]['isk'];
            $res[$index]['plebitis']            = (string)$arr[$a]['plebitis'];
            $plebitis                           += $arr[$a]['plebitis'];
            $res[$index]['decubitus']           = (string)$arr[$a]['decubitus'];
            $decubitus                          += $arr[$a]['decubitus'];
            $res[$index]['ido']                 = (string)$arr[$a]['ido'];
            $ido                                += $arr[$a]['ido'];
            $index++;
        }
        
        $res[$index]['tanggal']                 = "";
        $res[$index]['ivl']                     = "";
        $res[$index]['cvl']                     = "";
        $res[$index]['uc']                      = "";
        $res[$index]['ett']                     = "";
        $res[$index]['pasien_tirah_baring']     = "";
        $res[$index]['luka_bersih']             = "";
        $res[$index]['luka_kotor']              = "INDEX RATE";
        $total_iadp                             = ($iadp / $cvl) * 100;
        $res[$index]['iadp']                    = (string)$total_iadp;
        $total_vap                              = ($vap / $ett) * 100;
        $res[$index]['vap']                     = (string)$total_vap;
        $total_isk                              = ($isk / $uc) * 100;
        $res[$index]['isk']                     = (string)$total_isk;
        $total_plebitis                         = ($plebitis / $ivl) * 100;
        $res[$index]['plebitis']                = (string)$total_plebitis;
        $total_decubitus                        = ($decubitus / $pasien_tirah_baring) * 100;
        $res[$index]['decubitus']               = (string)$total_decubitus;
        $total_ido                              = ($ido / $luka_bersih) * 100;
        $res[$index]['ido']                     = (string)$total_ido;
        
        return $res;
    }
}

?>