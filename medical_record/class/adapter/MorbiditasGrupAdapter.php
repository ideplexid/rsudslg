<?php

class MorbiditasGrupAdapter extends ArrayAdapter {
    private $index = 0;
    private $list = array();
    private $datasize;
    private $no = 0;
    private $dtd = '';
    private $grup = '-';
    private $no_dftr_terperinci = '';
    private $nama_icd = '';
    private $u28hr_lk = 0;
    private $u1th_lk = 0;
    private $u4th_lk = 0;
    private $u14th_lk = 0;
    private $u24th_lk = 0;
    private $u44th_lk = 0;
    private $u65th_lk = 0;
    private $ul65th_lk = 0;
    private $u28hr_pr = 0;
    private $u1th_pr = 0;
    private $u4th_pr = 0;
    private $u14th_pr = 0;
    private $u24th_pr = 0;
    private $u44th_pr = 0;
    private $u65th_pr = 0;
    private $ul65th_pr = 0;
    private $kasus_baru_lk = 0;
    private $kasus_baru_pr = 0;
    private $kasus_baru_jmlh = 0;
    private $jmlh_kunjungan = 0;
    
    public function setDatasize($size) {
        $this->datasize = $size;
        return $this->datasize;
    }
    
    public function adapt($d) {
        $this->index ++;
        if($this->index == 1 && $this->datasize != 1) {
            $this->grup                 = $d->grup;
            $this->dtd                  = $d->dtd;
            $this->no_dftr_terperinci   = $d->kode_icd;
            $this->nama_icd             = $d->nama_icd;
            $this->u28hr_lk             = $this->u28hr_lk + $d->u28hr_lk;
            $this->u1th_lk              = $this->u1th_lk + $d->u1th_lk;
            $this->u4th_lk              = $this->u4th_lk + $d->u4th_lk;
            $this->u14th_lk             = $this->u14th_lk + $d->u14th_lk;
            $this->u24th_lk             = $this->u24th_lk + $d->u24th_lk;
            $this->u44th_lk             = $this->u44th_lk + $d->u44th_lk;
            $this->u65th_lk             = $this->u65th_lk + $d->u65th_lk;
            $this->ul65th_lk            = $this->ul65th_lk + $d->ul65th_lk;
            $this->u28hr_pr             = $this->u28hr_pr + $d->u28hr_pr;
            $this->u1th_pr              = $this->u1th_pr + $d->u1th_pr;
            $this->u4th_pr              = $this->u4th_pr + $d->u4th_pr;
            $this->u14th_pr             = $this->u14th_pr + $d->u14th_pr;
            $this->u24th_pr             = $this->u24th_pr + $d->u24th_pr;
            $this->u44th_pr             = $this->u44th_pr + $d->u44th_pr;
            $this->u65th_pr             = $this->u65th_pr + $d->u65th_pr;
            $this->ul65th_pr            = $this->ul65th_pr + $d->ul65th_pr;
            $this->kasus_baru_lk        = $this->kasus_baru_lk + $d->kasus_baru_lk;
            $this->kasus_baru_pr        = $this->kasus_baru_pr + $d->kasus_baru_pr;
            $this->kasus_baru_jmlh      = $this->kasus_baru_jmlh + $d->kasus_baru_jmlh;
            $this->jmlh_kunjungan       = $this->jmlh_kunjungan + $d->jmlh_kunjungan;
        } else if($this->index == $this->datasize && $d->grup != $this->grup) {
            $this->no ++;
            $array = array();
            $array['No.']                           = $this->no;
            $array['No. DTD']                       = $this->dtd;
            $array['Golongan Sebab-Sebab Sakit']    = $this->nama_icd;
            $array['No. Daftar Terperinci']         = $this->no_dftr_terperinci;
            $array['28hr Lk']                       = $this->u28hr_lk;
            $array['28hr Pr']                       = $this->u28hr_pr;
            $array['1th Lk']                        = $this->u1th_lk;
            $array['1th Pr']                        = $this->u1th_pr;
            $array['4th Lk']                        = $this->u4th_lk;
            $array['4th Pr']                        = $this->u4th_pr;
            $array['14th Lk']                       = $this->u14th_lk;
            $array['14th Pr']                       = $this->u14th_pr;
            $array['24th Lk']                       = $this->u24th_lk;
            $array['24th Pr']                       = $this->u24th_pr;
            $array['44th Lk']                       = $this->u44th_lk;
            $array['44th Pr']                       = $this->u44th_pr;
            $array['65th Lk']                       = $this->u65th_lk;
            $array['65th Pr']                       = $this->u65th_pr;
            $array['l65th Lk']                      = $this->ul65th_lk;
            $array['l65th Pr']                      = $this->ul65th_pr;
            $array['Kasus Baru Lk']                 = $this->kasus_baru_lk;
            $array['Kasus Baru Pr']                 = $this->kasus_baru_pr;
            $array['Total']                         = $this->kasus_baru_jmlh;
            $array['Jumlah Kunjungan']              = $this->jmlh_kunjungan;
            $this->list[]                           = $array;
        } else if($this->index == $this->datasize && $d->grup == $this->grup) {
            $this->no ++;
            $array = array();
            $array['No.']                           = $this->no;
            $array['No. DTD']                       = $this->dtd;
            $array['Golongan Sebab-Sebab Sakit']    = $this->nama_icd;
            $array['No. Daftar Terperinci']         = $this->no_dftr_terperinci;
            $array['28hr Lk']                       = $this->u28hr_lk;
            $array['28hr Pr']                       = $this->u28hr_pr;
            $array['1th Lk']                        = $this->u1th_lk;
            $array['1th Pr']                        = $this->u1th_pr;
            $array['4th Lk']                        = $this->u4th_lk;
            $array['4th Pr']                        = $this->u4th_pr;
            $array['14th Lk']                       = $this->u14th_lk;
            $array['14th Pr']                       = $this->u14th_pr;
            $array['24th Lk']                       = $this->u24th_lk;
            $array['24th Pr']                       = $this->u24th_pr;
            $array['44th Lk']                       = $this->u44th_lk;
            $array['44th Pr']                       = $this->u44th_pr;
            $array['65th Lk']                       = $this->u65th_lk;
            $array['65th Pr']                       = $this->u65th_pr;
            $array['l65th Lk']                      = $this->ul65th_lk;
            $array['l65th Pr']                      = $this->ul65th_pr;
            $array['Kasus Baru Lk']                 = $this->kasus_baru_lk;
            $array['Kasus Baru Pr']                 = $this->kasus_baru_pr;
            $array['Total']                         = $this->kasus_baru_jmlh;
            $array['Jumlah Kunjungan']              = $this->jmlh_kunjungan;
            $this->list[]                           = $array;
        } else {
            if($d->grup != $this->grup) {
                $this->no ++;
                $array = array();
                $array['No.']                           = $this->no;
                $array['No. DTD']                       = $this->dtd;
                $array['Golongan Sebab-Sebab Sakit']    = $this->nama_icd;
                $array['No. Daftar Terperinci']         = $this->no_dftr_terperinci;
                $array['28hr Lk']                       = $this->u28hr_lk;
                $array['28hr Pr']                       = $this->u28hr_pr;
                $array['1th Lk']                        = $this->u1th_lk;
                $array['1th Pr']                        = $this->u1th_pr;
                $array['4th Lk']                        = $this->u4th_lk;
                $array['4th Pr']                        = $this->u4th_pr;
                $array['14th Lk']                       = $this->u14th_lk;
                $array['14th Pr']                       = $this->u14th_pr;
                $array['24th Lk']                       = $this->u24th_lk;
                $array['24th Pr']                       = $this->u24th_pr;
                $array['44th Lk']                       = $this->u44th_lk;
                $array['44th Pr']                       = $this->u44th_pr;
                $array['65th Lk']                       = $this->u65th_lk;
                $array['65th Pr']                       = $this->u65th_pr;
                $array['l65th Lk']                      = $this->ul65th_lk;
                $array['l65th Pr']                      = $this->ul65th_pr;
                $array['Kasus Baru Lk']                 = $this->kasus_baru_lk;
                $array['Kasus Baru Pr']                 = $this->kasus_baru_pr;
                $array['Total']                         = $this->kasus_baru_jmlh;
                $array['Jumlah Kunjungan']              = $this->jmlh_kunjungan;
                $this->list[]                           = $array;
                
                $this->no_dftr_terperinci   = '';
                $this->dtd                  = '';
                $this->nama_icd             = '';
                $this->u28hr_lk             = 0;
                $this->u1th_lk              = 0;
                $this->u4th_lk              = 0;
                $this->u14th_lk             = 0;
                $this->u24th_lk             = 0;
                $this->u44th_lk             = 0;
                $this->u65th_lk             = 0;
                $this->ul65th_lk            = 0;
                $this->u28hr_pr             = 0;
                $this->u1th_pr              = 0;
                $this->u4th_pr              = 0;
                $this->u14th_pr             = 0;
                $this->u24th_pr             = 0;
                $this->u44th_pr             = 0;
                $this->u65th_pr             = 0;
                $this->ul65th_pr            = 0;
                $this->kasus_baru_lk        = 0;
                $this->kasus_baru_pr        = 0;
                $this->kasus_baru_jmlh      = 0;
                $this->jmlh_kunjungan       = 0;
                
                $this->grup                 = $d->grup;
                $this->dtd                  = $d->dtd;
                $this->no_dftr_terperinci   = $d->kode_icd;
                $this->nama_icd             = $d->nama_icd;
                $this->u28hr_lk             = $this->u28hr_lk + $d->u28hr_lk;
                $this->u1th_lk              = $this->u1th_lk + $d->u1th_lk;
                $this->u4th_lk              = $this->u4th_lk + $d->u4th_lk;
                $this->u14th_lk             = $this->u14th_lk + $d->u14th_lk;
                $this->u24th_lk             = $this->u24th_lk + $d->u24th_lk;
                $this->u44th_lk             = $this->u44th_lk + $d->u44th_lk;
                $this->u65th_lk             = $this->u65th_lk + $d->u65th_lk;
                $this->ul65th_lk            = $this->ul65th_lk + $d->ul65th_lk;
                $this->u28hr_pr             = $this->u28hr_pr + $d->u28hr_pr;
                $this->u1th_pr              = $this->u1th_pr + $d->u1th_pr;
                $this->u4th_pr              = $this->u4th_pr + $d->u4th_pr;
                $this->u14th_pr             = $this->u14th_pr + $d->u14th_pr;
                $this->u24th_pr             = $this->u24th_pr + $d->u24th_pr;
                $this->u44th_pr             = $this->u44th_pr + $d->u44th_pr;
                $this->u65th_pr             = $this->u65th_pr + $d->u65th_pr;
                $this->ul65th_pr            = $this->ul65th_pr + $d->ul65th_pr;
                $this->kasus_baru_lk        = $this->kasus_baru_lk + $d->kasus_baru_lk;
                $this->kasus_baru_pr        = $this->kasus_baru_pr + $d->kasus_baru_pr;
                $this->kasus_baru_jmlh      = $this->kasus_baru_jmlh + $d->kasus_baru_jmlh;
                $this->jmlh_kunjungan       = $this->jmlh_kunjungan + $d->jmlh_kunjungan;
            } else {
                if($d->kode_icd == NULL || $d->kode_icd == "") {
                    $this->no_dftr_terperinci   = $this->no_dftr_terperinci;
                } else {
                    $this->no_dftr_terperinci   = $this->no_dftr_terperinci.', '.$d->kode_icd;
                }
                $this->u28hr_lk             = $this->u28hr_lk + $d->u28hr_lk;
                $this->u1th_lk              = $this->u1th_lk + $d->u1th_lk;
                $this->u4th_lk              = $this->u4th_lk + $d->u4th_lk;
                $this->u14th_lk             = $this->u14th_lk + $d->u14th_lk;
                $this->u24th_lk             = $this->u24th_lk + $d->u24th_lk;
                $this->u44th_lk             = $this->u44th_lk + $d->u44th_lk;
                $this->u65th_lk             = $this->u65th_lk + $d->u65th_lk;
                $this->ul65th_lk            = $this->ul65th_lk + $d->ul65th_lk;
                $this->u28hr_pr             = $this->u28hr_pr + $d->u28hr_pr;
                $this->u1th_pr              = $this->u1th_pr + $d->u1th_pr;
                $this->u4th_pr              = $this->u4th_pr + $d->u4th_pr;
                $this->u14th_pr             = $this->u14th_pr + $d->u14th_pr;
                $this->u24th_pr             = $this->u24th_pr + $d->u24th_pr;
                $this->u44th_pr             = $this->u44th_pr + $d->u44th_pr;
                $this->u65th_pr             = $this->u65th_pr + $d->u65th_pr;
                $this->ul65th_pr            = $this->ul65th_pr + $d->ul65th_pr;
                $this->kasus_baru_lk        = $this->kasus_baru_lk + $d->kasus_baru_lk;
                $this->kasus_baru_pr        = $this->kasus_baru_pr + $d->kasus_baru_pr;
                $this->kasus_baru_jmlh      = $this->kasus_baru_jmlh + $d->kasus_baru_jmlh;
                $this->jmlh_kunjungan       = $this->jmlh_kunjungan + $d->jmlh_kunjungan;
            }
        }
    }
    
    public function getContent($data) {
		foreach($data as $d){
			$this->adapt($d);		
		}
        return $this->list;
    }
}

?>