<?php

require_once 'smis-framework/smis/template/ModulTemplate.php';
require_once ("smis-base/smis-include-service-consumer.php");
require_once "medical_record/class/service/RuanganService.php";
require_once 'smis-libs-hrd/EmployeeResponder.php';

class AsesmenPsikologi extends ModulTemplate {
    protected $db;
	protected $polislug;
	protected $page;
	protected $protoslug;
	protected $protoname;
	protected $protoimplement;
	protected $action;
	protected $dbtable;
	protected $uitable;
	protected $noreg_pasien;
	protected $nama_pasien;
	protected $nrm_pasien;
	protected $alamat;
	protected $umur;
    protected $tgl_lahir;
    protected $jk;
	protected $ruang_asal;
	protected $id_antrian;
    protected $carabayar;
    protected $waktu_masuk;
    
    public function __construct($db, $polislug = "all", $noreg = "", $nrm = "", $nama = "", $alamat = "", $umur = "", $jk = "0", $tgl_lahir = "", $waktu_masuk = "", $id_antrian = "0", $page = "medical_record", $action = "asesmen_psikologi",$carabayar, $protoslug = "", $protoname = "", $protoimplement = "") {
        $this->db               = $db;
		$this->noreg_pasien     = $noreg;
		$this->id_antrian       = $id_antrian;
		$this->nama_pasien      = $nama;
		$this->nrm_pasien       = $nrm;
		$this->alamat           = $alamat;
		$this->umur             = $umur;
        $this->jk               = $jk;
        $this->tgl_lahir        = $tgl_lahir;
        $this->waktu_masuk      = $waktu_masuk;
		$this->ruang_asal       = $ruang_asal;
		$this->polislug         = $polislug;
		$this->dbtable          = new DBTable ( $this->db, "smis_mr_assesmen_psikologi" );
		$this->page             = $page;
		$this->protoslug        = $protoslug;
		$this->protoimplement   = $protoimplement;
		$this->protoname        = $protoname;
		$this->action           = $action;
        $this->carabayar        = $carabayar;
        
        if ($noreg != ""){
            $this->dbtable->addCustomKriteria ( "noreg_pasien", "='" . $noreg . "'" );
        }
        $head=array ('NRM',"Pasien","No Register" );
		$this->uitable = new Table ($head , " Asesmen Anak - " . ($this->polislug == "all" ? "" : ucfirst ( $this->protoname )), NULL, true );
		$this->uitable->setName ( $action );
    }
    
    public function command($command) {
        require_once "smis-base/smis-include-duplicate.php";
		$adapter = new SimpleAdapter ();
		$adapter->add ( "Pasien", "nama_pasien" );
		$adapter->add ( "NRM", "nrm_pasien", "digit8" );
		$adapter->add ( "No Register", "noreg_pasien", "digit8" );
		$dbres = new DuplicateResponder ( $this->dbtable, $this->uitable, $adapter );
		$dbres->setDuplicate(false,"");
        $dbres->setAutonomous(getSettings($this->db,"smis_autonomous_id",""));
        $data = $dbres->command ( $_POST ['command'] );
		echo json_encode ( $data );
		return;
	}
    
    public function phpPreLoad() {
        $service = new RuanganService ( $this->db );
		$service->execute ();
		$ruangan = $service->getContent ();
        
        /*block data header*/
		if ($this->noreg_pasien != "") {
			$this->uitable->addModal ( "nama_pasien", "hidden", "", $this->nama_pasien, "n", null, true );
			$this->uitable->addModal ( "nrm_pasien", "hidden", "", $this->nrm_pasien, "n", null, true );
			$this->uitable->addModal ( "noreg_pasien", "hidden", "", $this->noreg_pasien, "n", null, true );
			$this->uitable->addModal ( "alamat_pasien", "hidden", "", $this->alamat, "n", null, true );
			$this->uitable->addModal ( "jk", "hidden", "", $this->jk, "n", null, true );
            $this->uitable->addModal ( "waktu_masuk", "hidden", "", $this->waktu_masuk, "n", null, true );
			$this->uitable->addModal ( "umur", "hidden", "", $this->umur, "n", null, true );
            $this->uitable->addModal ( "tgl_lahir_pasien", "hidden", "", $this->tgl_lahir, "n", null, true );
		} else {
			$this->uitable->addModal ( "nama_pasien", "chooser-" . $this->action . "-apg_pasien-Pasien", "Pasien", $this->nama_pasien, "n", null, true );
			$this->uitable->addModal ( "nrm_pasien", "text", "NRM", $this->nrm_pasien, "n", null, true );
			$this->uitable->addModal ( "noreg_pasien", "text", "No Reg", $this->noreg_pasien, "n", null, true );
		}
		if ($this->polislug == "all")
			$this->uitable->addModal ( "ruangan", "select", "Asal Ruangan", $ruangan );
		else
			$this->uitable->addModal ( "ruangan", "hidden", "", $this->polislug, "n", null, true );
        /*end - block data header*/
        
        $the_row = array ();
		$the_row ["id"] 						    = "";
		$the_row ["nama_dokter"] 				    = "";
		$the_row ["id_dokter"] 					    = "";
		$the_row ['waktu'] 					        = date ( "Y-m-d H:i:s" );
        $the_row ["carabayar"] 				        = $this->carabayar;
        $the_row ["waktu_masuk"] 				    = $this->waktu_masuk;		
        $the_row ["anak_ke"] 					    = "";
        $the_row ["dari_saudara"] 					= "";
        $the_row ["status_nikah"] 				    = "";
        $the_row ["pend_terakhir"] 				    = "";
        $the_row ["obeservasi"] 				    = "";
        $the_row ["wawancara"] 					    = "";
        $the_row ["wawancara_ket"] 				    = "";
        $the_row ["keluhan_utama"] 				    = "";
        $the_row ["mulai_terjadi"] 				    = "";
        $the_row ["riwayat_penyakit"] 			    = "";
        $the_row ["keluhan_psikosomatis"] 		    = "";
        $the_row ["penyebab"] 					    = "";
        $the_row ["faktor_keluarga"] 			    = "";
        $the_row ["faktor_pekerjaan"] 			    = "";
        $the_row ["faktor_premorboid"] 			    = "";
        $the_row ["riwayat_napza"] 				    = "";
        $the_row ["napza_lama_pemakaian"] 		    = "";
        $the_row ["napza_jenis_zat"] 			    = "";
        $the_row ["napza_cara_pakai"] 			    = "";
        $the_row ["napza_latarbelakang_pakai"] 	    = "";
        $the_row ["kesan_umum"] 				    = "";
        $the_row ["kesadaran"] 					    = "";
        $the_row ["mood"] 					        = "";
        $the_row ["proses_dan_isi_pikiran"] 	    = "";
        $the_row ["kecerdasan"] 					 = "";
        $the_row ["dorongan"] 					    = "";
        $the_row ["judgment"] 					    = "";
        $the_row ["attention"] 					    = "";
        $the_row ["psikomotorik"] 					= "";
        $the_row ["orientasi_diri"] 				= "";
        $the_row ["orientasi_wkt"] 					= "";
        $the_row ["orientasi_tempat"] 				= "";
        $the_row ["orientasi_ruang"] 				= "";
        $the_row ["bb"] 					        = "";
        $the_row ["tb"] 					        = "";
        $the_row ["lila"] 					        = "";
        $the_row ["lk"] 					        = "";
        $the_row ["status_gizi"] 					= "";
        $the_row ["tes_iq"] 					    = "";
        $the_row ["tes_bakat"] 					    = "";
        $the_row ["tes_kepribadian"] 				= "";
        $the_row ["tes_okupasi"] 					= "";
        $the_row ["tes_perkembangan"] 				= "";
        $the_row ["perkembangan_fisik"] 			= "";
        $the_row ["perkembangan_kognitif"] 			= "";
        $the_row ["perkembangan_emosi"] 			= "";
        $the_row ["perkembangan_sosial"] 			= "";
        $the_row ["perkembangan_motorik"] 			= "";
        $the_row ["report"] 					    = "";
        $the_row ["ekspresi"] 					    = "";
        $the_row ["reaksi"] 					    = "";
        $the_row ["komunikasi"] 					= "";
        $the_row ["lain_lain"] 					    = "";
        $the_row ["waktu_pulang"] 					= "";
        $the_row ["waktu_kontrol_klinik"] 			= "";
        $the_row ["dirawat_diruang"] 				= "";
        
        $exist ['noreg_pasien'] = $this->noreg_pasien;
		$exist ['nama_pasien'] = $this->nama_pasien;
		$exist ['nrm_pasien'] = $this->nrm_pasien;
        
        if ($this->dbtable->is_exist ( $exist )) {
            $row = $this->dbtable->select ( $exist );
            $the_row ["id"] 					        = $row->id;
            $the_row ["nama_dokter"] 			        = $row->nama_dokter;
            $the_row ["id_dokter"] 				        = $row->id_dokter;
            $the_row ["waktu"] 				            = $row->waktu;
            $the_row ["waktu_masuk"] 				    = $row->waktu_masuk;
            $the_row ["anak_ke"] 				        = $row->anak_ke;
            $the_row ["dari_saudara"] 				    = $row->dari_saudara;
            $the_row ["status_nikah"] 				    = $row->status_nikah;
            $the_row ["pend_terakhir"] 				    = $row->pend_terakhir;
            $the_row ["obeservasi"] 				    = $row->obeservasi;
            $the_row ["wawancara"] 				        = $row->wawancara;
            $the_row ["wawancara_ket"] 				    = $row->wawancara_ket;
            $the_row ["keluhan_utama"] 				    = $row->keluhan_utama;
            $the_row ["mulai_terjadi"] 				    = $row->mulai_terjadi;
            $the_row ["riwayat_penyakit"] 				= $row->riwayat_penyakit;
            $the_row ["keluhan_psikosomatis"] 			= $row->keluhan_psikosomatis;
            $the_row ["penyebab"] 				        = $row->penyebab;
            $the_row ["faktor_keluarga"] 				= $row->faktor_keluarga;
            $the_row ["faktor_pekerjaan"] 				= $row->faktor_pekerjaan;
            $the_row ["faktor_premorboid"] 				= $row->faktor_premorboid;
            $the_row ["riwayat_napza"] 				    = $row->riwayat_napza;
            $the_row ["napza_lama_pemakaian"] 			= $row->napza_lama_pemakaian;
            $the_row ["napza_jenis_zat"] 				= $row->napza_jenis_zat;
            $the_row ["napza_cara_pakai"] 				= $row->napza_cara_pakai;
            $the_row ["napza_latarbelakang_pakai"] 		= $row->napza_latarbelakang_pakai;
            $the_row ["kesan_umum"] 				    = $row->kesan_umum;
            $the_row ["kesadaran"] 				        = $row->kesadaran;
            $the_row ["mood"] 				            = $row->mood;
            $the_row ["proses_dan_isi_pikiran"] 		= $row->proses_dan_isi_pikiran;
            $the_row ["kecerdasan"] 				    = $row->kecerdasan;
            $the_row ["dorongan"] 				        = $row->dorongan;
            $the_row ["judgment"] 				        = $row->judgment;
            $the_row ["attention"] 				        = $row->attention;
            $the_row ["psikomotorik"] 				    = $row->psikomotorik;
            $the_row ["orientasi_diri"] 				= $row->orientasi_diri;
            $the_row ["orientasi_wkt"] 				    = $row->orientasi_wkt;
            $the_row ["orientasi_tempat"] 				= $row->orientasi_tempat;
            $the_row ["orientasi_ruang"] 				= $row->orientasi_ruang;
            $the_row ["bb"] 				            = $row->bb;
            $the_row ["tb"] 				            = $row->tb;
            $the_row ["lila"] 				            = $row->lila;
            $the_row ["status_gizi"] 				    = $row->status_gizi;
            $the_row ["tes_iq"] 				        = $row->tes_iq;
            $the_row ["tes_bakat"] 				        = $row->tes_bakat;
            $the_row ["tes_kepribadian"] 				= $row->tes_kepribadian;
            $the_row ["tes_okupasi"] 				    = $row->tes_okupasi;
            $the_row ["tes_perkembangan"] 				= $row->tes_perkembangan;
            $the_row ["perkembangan_fisik"] 			= $row->perkembangan_fisik;
            $the_row ["perkembangan_kognitif"] 			= $row->perkembangan_kognitif;
            $the_row ["perkembangan_emosi"] 			= $row->perkembangan_emosi;
            $the_row ["perkembangan_sosial"] 			= $row->perkembangan_sosial;
            $the_row ["perkembangan_motorik"] 			= $row->perkembangan_motorik;
            $the_row ["report"] 				        = $row->report;
            $the_row ["ekspresi"] 				        = $row->ekspresi;
            $the_row ["reaksi"] 				        = $row->reaksi;
            $the_row ["komunikasi"] 				    = $row->komunikasi;
            $the_row ["lain_lain"] 				        = $row->lain_lain;
            $the_row ["waktu_pulang"] 				    = $row->waktu_pulang;
            $the_row ["waktu_kontrol_klinik"] 			= $row->waktu_kontrol_klinik;
            $the_row ["dirawat_diruang"] 				= $row->dirawat_diruang;
        }else{
            $exist ['umur'] = $this->umur;
			$exist ['jk'] = $this->jk;
			$exist ['alamat_pasien'] = $this->alamat;
			$exist ['noreg_pasien'] = $this->noreg_pasien;
			$exist ['nrm_pasien'] = $this->nrm_pasien;
			$exist ['nama_pasien'] = $this->nama_pasien;
            $exist ['ruangan'] = $this->polislug;
            $exist ['tgl_lahir_pasien'] = $this->tgl_lahir;
			$exist ['waktu'] = date ( "Y-m-d H:i:s" );
            $exist ['waktu_masuk'] = $this->waktu_masuk;
            $this->dbtable->insert ( $exist );
			
            $the_row ["id"] = $this->dbtable->get_inserted_id ();
            
            $update['origin_id']=$the_row ["id"];
            $update['autonomous']="[".getSettings($this->db,"smis_autonomous_id","")."]";
            $this->dbtable->update($update,array("id"=>$the_row ["id"]));
        }
        
        $this->uitable->addModal ( "id", "hidden", "", $the_row ['id'] );
		$this->uitable->addModal ( "nama_dokter", "chooser-apg_pasien-dokter_apg-Dokter Pemeriksa", "Dokter Pemeriksa", $the_row ['nama_dokter'],"n",null,false,null,true);
		$this->uitable->addModal ( "id_dokter", "hidden", "", $the_row ['id_dokter'],"y",null,false,null,false);
		$this->uitable->addModal ( "waktu", "datetime", "Waktu", $the_row ['waktu'] ,"y",null,false,null,false);
		$this->uitable->addModal ( "anak_ke", "text", "Anak ke-", $the_row ['anak_ke'] ,"y",null,false,null,false);
		$this->uitable->addModal ( "dari_saudara", "text", "Dari (Bersaudara)", $the_row ['dari_saudara'] ,"y",null,false,null,false);
        $status_nikah=array(
            array("name"=>"","value"=>"","default"=> $the_row ["status_nikah"]==""?1:0),
            array("name"=>"Sudah","value"=>"Sudah","default"=> $the_row ["status_nikah"]=="Sudah"?1:0),
            array("name"=>"Belum","value"=>"Belum","default"=> $the_row ["status_nikah"]=="Belum"?1:0),
		);
        $this->uitable->addModal("status_nikah", "select", "Status Pernikahan", $status_nikah,"y",NULL,false,NULL,false);
        $this->uitable->addModal ( "pend_terakhir", "text", "Pendidikan Terakhir", $the_row ['pend_terakhir'] ,"y",null,false,null,false);
        $this->uitable->addModal ( "obeservasi", "text", "<strong>Observasi</strong>", $the_row ['obeservasi'] ,"y",null,false,null,false);
        $wawancara=array(
            array("name"=>"","value"=>"","default"=> $the_row ["wawancara"]==""?1:0),
            array("name"=>"Autoanamnese","value"=>"Autoanamnese","default"=> $the_row ["wawancara"]=="Autoanamnese"?1:0),
            array("name"=>"Allo/Hetero Anamnese","value"=>"Allo/Hetero Anamnese","default"=> $the_row ["wawancara"]=="Allo/Hetero Anamnese"?1:0),
		);
        $this->uitable->addModal("wawancara", "select", "<strong>Wawancara</strong>", $wawancara,"y",NULL,false,NULL,false);
        $this->uitable->addModal ( "wawancara_ket", "text", "Keterangan", $the_row ['wawancara_ket'] ,"y",null,false,null,false);
        $this->uitable->addModal ( "keluhan_utama", "text", "Keluhan Utama", $the_row ['keluhan_utama'] ,"y",null,false,null,false);
        $this->uitable->addModal ( "mulai_terjadi", "text", "Kapan Mulai Terjadi", $the_row ['mulai_terjadi'] ,"y",null,false,null,false);
        $this->uitable->addModal ( "riwayat_penyakit", "text", "Riwayat Penyakit", $the_row ['riwayat_penyakit'] ,"y",null,false,null,false);
        $this->uitable->addModal ( "keluhan_psikosomatis", "text", "Keluhan Psikosomatis", $the_row ['keluhan_psikosomatis'] ,"y",null,false,null,false);
        $this->uitable->addModal("", "label", "<strong>PREDISPOSISI FAKTOR</strong>", "");
        $this->uitable->addModal ( "penyebab", "text", "Penyebab/Pencetus", $the_row ['penyebab'] ,"y",null,false,null,false);
        $this->uitable->addModal ( "faktor_keluarga", "text", "Faktor Keluarga", $the_row ['faktor_keluarga'] ,"y",null,false,null,false);
        $this->uitable->addModal ( "faktor_pekerjaan", "text", "Faktor Pekerjaan/Sosial", $the_row ['faktor_pekerjaan'] ,"y",null,false,null,false);
        $this->uitable->addModal ( "faktor_premorboid", "text", "Faktor Premorbid", $the_row ['faktor_premorboid'] ,"y",null,false,null,false);
        $napza=array(
            array("name"=>"","value"=>"","default"=> $the_row ["riwayat_napza"]==""?1:0),
            array("name"=>"Ada","value"=>"Ada","default"=> $the_row ["riwayat_napza"]=="Ada"?1:0),
            array("name"=>"Tidak","value"=>"Tidak","default"=> $the_row ["riwayat_napza"]=="Tidak"?1:0),
		);
        $this->uitable->addModal("riwayat_napza", "select", "Riwayat NAPZA", $napza,"y",NULL,false,NULL,false);
        $this->uitable->addModal ( "napza_lama_pemakaian", "text", "Lama Pemakaian", $the_row ['napza_lama_pemakaian'] ,"y",null,false,null,false);
        $this->uitable->addModal ( "napza_jenis_zat", "text", "Janis Zat", $the_row ['napza_jenis_zat'] ,"y",null,false,null,false);
        $this->uitable->addModal ( "napza_cara_pakai", "text", "Cara Pakai", $the_row ['napza_cara_pakai'] ,"y",null,false,null,false);
        $this->uitable->addModal ( "napza_latarbelakang_pakai", "text", "Latar Belakang Pemakaian", $the_row ['napza_latarbelakang_pakai'] ,"y",null,false,null,false);
        $this->uitable->addModal("", "label", "<strong>STATUS PSIKOLOGI</strong>", "");
        $this->uitable->addModal ( "kesan_umum", "text", "Kesan Umum", $the_row ['kesan_umum'] ,"y",null,false,null,false);
        $this->uitable->addModal ( "kesadaran", "text", "Kesadaran", $the_row ['kesadaran'] ,"y",null,false,null,false);
        $this->uitable->addModal ( "mood", "text", "Mood/Afek/Ekspresi", $the_row ['mood'] ,"y",null,false,null,false);
        $this->uitable->addModal ( "proses_dan_isi_pikiran", "text", "Proses dan Isi Pikiran", $the_row ['proses_dan_isi_pikiran'] ,"y",null,false,null,false);
        $this->uitable->addModal ( "kecerdasan", "text", "Kecerdasan", $the_row ['kecerdasan'] ,"y",null,false,null,false);
        $this->uitable->addModal ( "dorongan", "text", "Dorongan (Insting, Drive, Motivasi, Semangat)", $the_row ['dorongan'] ,"y",null,false,null,false);
        $this->uitable->addModal ( "judgment", "text", "Judgement (Pemahaman)", $the_row ['judgment'] ,"y",null,false,null,false);
        $this->uitable->addModal ( "attention", "text", "Attention (Perhatian)", $the_row ['attention'] ,"y",null,false,null,false);
        $this->uitable->addModal ( "psikomotorik", "text", "Psikomotorik", $the_row ['psikomotorik'] ,"y",null,false,null,false);
        $this->uitable->addModal("", "label", "Orientasi", "");
        $this->uitable->addModal ( "orientasi_diri", "text", "Diri", $the_row ['orientasi_diri'] ,"y",null,false,null,false);
        $this->uitable->addModal ( "orientasi_wkt", "text", "Waktu", $the_row ['orientasi_wkt'] ,"y",null,false,null,false);
        $this->uitable->addModal ( "orientasi_tempat", "text", "Tempat", $the_row ['orientasi_tempat'] ,"y",null,false,null,false);
        $this->uitable->addModal ( "orientasi_ruang", "text", "Ruang", $the_row ['orientasi_ruang'] ,"y",null,false,null,false);
        $this->uitable->addModal("", "label", "<strong>STATUS ANTROPOMETRIS</strong>", "");
        $this->uitable->addModal ( "bb", "text", "Berat Badan (kg)", $the_row ['bb'] ,"y",null,false,null,false);
        $this->uitable->addModal ( "tb", "text", "Tinggi Badan (cm)", $the_row ['tb'] ,"y",null,false,null,false);
        $this->uitable->addModal ( "lila", "text", "LILA (cm)", $the_row ['lila'] ,"y",null,false,null,false);
        $this->uitable->addModal ( "lk", "text", "LK (cm)", $the_row ['lk'] ,"y",null,false,null,false);
        $status_gizi=array(
            array("name"=>"","value"=>"","default"=> $the_row ["status_gizi"]==""?1:0),
            array("name"=>"Baik","value"=>"Baik","default"=> $the_row ["status_gizi"]=="Baik"?1:0),
            array("name"=>"Sedang","value"=>"Sedang","default"=> $the_row ["status_gizi"]=="Sedang"?1:0),
            array("name"=>"Kurang","value"=>"Kurang","default"=> $the_row ["status_gizi"]=="Kurang"?1:0),
            array("name"=>"Jelek/Gizi Buruk","value"=>"Jelek/Gizi Buruk","default"=> $the_row ["status_gizi"]=="Jelek/Gizi Buruk"?1:0),
		);
        $this->uitable->addModal("status_gizi", "select", "Status Gizi", $status_gizi,"y",NULL,false,NULL,false);
        $this->uitable->addModal("", "label", "<strong>PEMERIKSAAN PSIKOLOGI</strong>", "");
        $this->uitable->addModal ( "tes_iq", "text", "Tes IQ", $the_row ['tes_iq'] ,"y",null,false,null,false);
        $this->uitable->addModal ( "tes_bakat", "text", "Tes Bakat/Minat", $the_row ['tes_bakat'] ,"y",null,false,null,false);
        $this->uitable->addModal ( "tes_kepribadian", "text", "Tes Kepribadian", $the_row ['tes_kepribadian'] ,"y",null,false,null,false);
        $this->uitable->addModal ( "tes_okupasi", "text", "Tes Okupasi", $the_row ['tes_okupasi'] ,"y",null,false,null,false);
        $this->uitable->addModal ( "tes_perkembangan", "text", "Tes Perkembangan", $the_row ['tes_perkembangan'] ,"y",null,false,null,false);
        $this->uitable->addModal("", "label", "<strong>OBSERVASI/WAWANCARA</strong>", "");
        $this->uitable->addModal ( "perkembangan_fisik", "text", "Perkembangan Fisik", $the_row ['perkembangan_fisik'] ,"y",null,false,null,false);
        $this->uitable->addModal ( "perkembangan_kognitif", "text", "Perkembangan Kognitif", $the_row ['perkembangan_kognitif'] ,"y",null,false,null,false);
        $this->uitable->addModal ( "perkembangan_emosi", "text", "Perkembangan Emosi", $the_row ['perkembangan_emosi'] ,"y",null,false,null,false);
        $this->uitable->addModal ( "perkembangan_sosial", "text", "Perkembangan Sosial", $the_row ['perkembangan_sosial'] ,"y",null,false,null,false);
        $this->uitable->addModal ( "perkembangan_motorik", "text", "Perkembangan Motorik", $the_row ['perkembangan_motorik'] ,"y",null,false,null,false);
        $this->uitable->addModal("", "label", "<strong>OBSERVASI/WAWANCARA</strong>", "");
        $this->uitable->addModal ( "report", "text", "Report", $the_row ['report'] ,"y",null,false,null,false);
        $this->uitable->addModal ( "ekspresi", "text", "Ekspresi", $the_row ['ekspresi'] ,"y",null,false,null,false);
        $this->uitable->addModal ( "reaksi", "text", "Reaksi", $the_row ['reaksi'] ,"y",null,false,null,false);
        $this->uitable->addModal ( "komunikasi", "text", "Komunikasi", $the_row ['komunikasi'] ,"y",null,false,null,false);
        $this->uitable->addModal ( "lain_lain", "text", "Lain-Lain", $the_row ['lain_lain'] ,"y",null,false,null,false);
        $this->uitable->addModal("", "label", "<strong>DISPOSISI</strong>", "");
        $this->uitable->addModal ( "waktu_pulang", "datetime", "Waktu Pulang", $the_row ['waktu_pulang'] ,"y",null,false,null,false);
        $this->uitable->addModal ( "waktu_kontrol_klinik", "datetime", "Tanggal Kontrol Klinik", $the_row ['waktu_kontrol_klinik'] ,"y",null,false,null,false);
        $dirawat=array(
				array("name"=>"","value"=>"","default"=> $the_row ["dirawat_diruang"]==""?1:0),
				array("name"=>"Intensif","value"=>"Intensif","default"=> $the_row ["dirawat_diruang"]=="Intensif"?1:0),
				array("name"=>"Ruang Lain","value"=>"Ruang Lain","default"=> $the_row ["dirawat_diruang"]=="Ruang Lain"?1:0),
				array("name"=>"Tidak Dirawat Diruang","value"=>"Tidak Dirawat Diruang","default"=> $the_row ["dirawat_diruang"]=="Tidak Dirawat Diruang"?1:0)
		);
        $this->uitable->addModal("dirawat_diruang", "select", "Dirawat Diruang", $dirawat,"y",NULL,false,NULL,false);
        
        
        $modal = $this->uitable->getModal ();
		$modal->setTitle ( "Assessment Psikologi" );
		if ($this->page == "medical_record") {
			echo $this->uitable->getHtml ();
		} else {
			echo "<div class='hide'>" . $this->uitable->getHtml () . "</div>";
			$modal->setAlwaysShow ( true );
		}
		echo $modal->joinFooterAndForm ()->getHtml ();
        
        $hidden_noreg=new Hidden("apg_noreg_pasien","",$this->noreg_pasien);
        $hidden_nama=new Hidden("apg_nama_pasien","",$this->nama_pasien);
        $hidden_nrm=new Hidden("apg_nrm_pasien","",$this->nrm_pasien);
        $hidden_polislug=new Hidden("apg_polislug","",$this->polislug);
        $hidden_the_page=new Hidden("apg_the_page","",$this->page);
        $hidden_the_protoslug=new Hidden("apg_the_protoslug","",$this->protoslug);
        $hidden_the_protoname=new Hidden("apg_the_protoname","",$this->protoname);
        $hidden_the_protoimpl=new Hidden("apg_the_protoimplement","",$this->protoimplement);
        $hidden_antrian=new Hidden("apg_id_antrian","",$this->id_antrian);
        $hidden_action=new Hidden("apg_action","",$this->action);
        
        echo $hidden_noreg->getHtml();
        echo $hidden_nama->getHtml();
        echo $hidden_nrm->getHtml();
        echo $hidden_polislug->getHtml();
        echo $hidden_the_page->getHtml();
        echo $hidden_the_protoslug->getHtml();
        echo $hidden_the_protoname->getHtml();
        echo $hidden_the_protoimpl->getHtml();
        echo $hidden_antrian->getHtml();
        echo $hidden_action->getHtml();
        
        echo addJS("medical_record/resource/js/asesmen_psikologi.js",false);
        echo addCSS ( "framework/bootstrap/css/datepicker.css" );
        echo addJS ( "framework/bootstrap/js/bootstrap-datetimepicker.js" );
        echo addCSS ( "framework/bootstrap/css/bootstrap-datetimepicker.css" );
    }
    
    public function cssPreLoad() {
		?>
		<style type="text/css">
				#<?php echo $this->action;?>_add_form_form>div {clear: both; width: 100%;}
				#<?php echo $this->action; ?>_add_form_form>div label { width: 250px; }
		</style>
		<?php
	}
    
    public function superCommand($super_command) {
        $super = new SuperCommand ();
        
        /* PASIEN */
		$phead=array ('Nama','NRM',"No Reg" );
		$ptable = new Table ( $phead, "", NULL, true );
		$ptable->setName ( "apg_pasien" );
		$ptable->setModel ( Table::$SELECT );
		$padapter = new SimpleAdapter ();
		$padapter->add ( "Nama", "nama_pasien" );
		$padapter->add ( "NRM", "nrm", "digit8" );
		$padapter->add ( "No Reg", "id" );
		$presponder = new ServiceResponder ( $this->db, $ptable, $padapter, "get_registered" );		
		$super->addResponder ( "apg_pasien", $presponder );
        
        /* DOKTER */
		$eadapt = new SimpleAdapter ();
		$eadapt->add ( "Jabatan", "nama_jabatan" );
		$eadapt->add ( "Nama", "nama" );
		$eadapt->add ( "NIP", "nip" );
		$head_karyawan=array ('Nama','Jabatan',"NIP");
		$dktable = new Table ( $head_karyawan, "", NULL, true );
		$dktable->setName ( "dokter_apg" );
		$dktable->setModel ( Table::$SELECT );
		$employee = new EmployeeResponder ( $this->db, $dktable, $eadapt, "dokter" );
		$super->addResponder ( "dokter_apg", $employee );
        
        $init = $super->initialize ();
		if ($init != null) {
			echo $init;
			return;
		}
    }
} 

?>