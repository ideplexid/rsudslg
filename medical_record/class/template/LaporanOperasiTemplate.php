<?php
require_once 'smis-framework/smis/template/ModulTemplate.php';
require_once ("smis-base/smis-include-service-consumer.php");
require_once "medical_record/class/service/RuanganService.php";
require_once 'smis-libs-hrd/EmployeeResponder.php';
class LaporanOperasiTemplate extends ModulTemplate {
	protected $db;
	protected $polislug;
	protected $page;
	protected $protoslug;
	protected $protoname;
	protected $protoimplement;
	protected $action;
	protected $dbtable;
	protected $uitable;
	protected $noreg_pasien;
	protected $nama_pasien;
	protected $nrm_pasien;
	protected $alamat;
	protected $umur;
	protected $jk;
	protected $ruang_asal;
	protected $id_antrian;
	public function __construct($db, $polislug = "all", $noreg = "", $nrm = "", $nama = "", $alamat = "", $umur = "", $jk = "0", $id_antrian = "0", $ruang_asal = "", $page = "medical_record", $action = "lap_operasi", $protoslug = "", $protoname = "", $protoimplement = "") {
		$this->db = $db;
		$this->noreg_pasien = $noreg;
		$this->id_antrian = $id_antrian;
		
		$this->nama_pasien = $nama;
		$this->nrm_pasien = $nrm;
		$this->alamat = $alamat;
		$this->umur = $umur;
		$this->jk = $jk;
		$this->ruang_asal = $ruang_asal;
		$this->polislug = $polislug;
		$this->dbtable = new DBTable ( $this->db, "smis_mr_operasi" );
		$this->page = $page;
		$this->protoslug = $protoslug;
		$this->protoimplement = $protoimplement;
		$this->protoname = $protoname;
		$this->action = $action;
		if ($polislug != "all")
			$this->dbtable->addCustomKriteria ( "ruangan", "='" . $polislug . "'" );
		if ($noreg != "")
			$this->dbtable->addCustomKriteria ( "noreg_pasien", "='" . $noreg . "'" );
		$head=array ('NRM',"Pasien","No Register" );
		$this->uitable = new Table ($head , " Indikator " . ($this->polislug == "all" ? "" : ucfirst ( $this->protoname )), NULL, true );
		$this->uitable->setName ( $action );
		
		if ($this->page == "medical_record" && $this->action == "lap_operasi") {
			$this->uitable->setAddButtonEnable ( false );
			$this->uitable->setDelButtonEnable ( false );
		}
	}
	
	
	
	public function command($command) {
		$adapter = new SimpleAdapter ();
		$adapter->add ( "Pasien", "nama_pasien" );
		$adapter->add ( "NRM", "nrm_pasien", "digit8" );
		$adapter->add ( "No Register", "noreg_pasien", "digit8" );
		$dbres = new DBResponder ( $this->dbtable, $this->uitable, $adapter );
		$data = $dbres->command ( $_POST ['command'] );
		echo json_encode ( $data );
		return;
	}
	public function phpPreLoad() {
		if ($this->noreg_pasien != "") {
			$this->uitable->addModal ( "nama_pasien", "hidden", "", $this->nama_pasien, "n", null, true );
			$this->uitable->addModal ( "nrm_pasien", "hidden", "", $this->nrm_pasien, "n", null, true );
			$this->uitable->addModal ( "noreg_pasien", "hidden", "", $this->noreg_pasien, "n", null, true );
			$this->uitable->addModal ( "alamat", "hidden", "", $this->alamat, "n", null, true );
			$this->uitable->addModal ( "jk", "hidden", "", $this->jk, "n", null, true );
			$this->uitable->addModal ( "umur", "hidden", "", $this->umur, "n", null, true );
			$this->uitable->addModal ( "ruang_asal", "hidden", "", $this->ruang_asal, "n", null, true );
		} else {
			$this->uitable->addModal ( "nama_pasien", "chooser-" . $this->action . "-lo_pasien", "Pasien", $this->nama_pasien, "n", null, true );
			$this->uitable->addModal ( "nrm_pasien", "text", "NRM", $this->nrm_pasien, "n", null, true );
			$this->uitable->addModal ( "noreg_pasien", "text", "No Reg", $this->noreg_pasien, "n", null, true );
		}
        
        if ($this->polislug == "all"){
            $service = new RuanganService ( $this->db );
            $service->execute ();
            $ruangan = $service->getContent ();
            $this->uitable->addModal ( "ruangan", "select", "Asal Ruangan", $ruangan );
        }else{
            $this->uitable->addModal ( "ruangan", "hidden", "", $this->polislug, "n", null, true );
        }
		
		$the_row = array ();
		$the_row ['spesialisasi'] = "";
		$the_row ['anastesi'] = "";
		$the_row ['gol_bedah'] = "";
		$the_row ['dokter_operator'] = "";
		$the_row ['dokter_jaga'] = "";
		$the_row ['asisten_operator'] = "";
		$the_row ['dokter_anastesi'] = "";
		$the_row ['perawat_rr'] = "";
		$the_row ['asisten_anastesi'] = "";
		$the_row ['jenis_operasi'] = "";
		$the_row ['sifat_operasi'] = "";
		$the_row ['asal_ruang'] = "";
		$the_row ['diagnosa'] = "";
		$the_row ['tindakan'] = "";
		$the_row ['instrument'] = "";
		$the_row ['alamat'] = "";
		$the_row ["id"] = "";
		$the_row ["status"] = "";
		$the_row ['tanggal_mulai'] = date ( "Y-m-d H:i" );
		$the_row ['tanggal_selesai'] = date ( "Y-m-d H:i" );
		
		$exist ['noreg_pasien'] = $this->noreg_pasien;
		$exist ['nama_pasien'] = $this->nama_pasien;
		$exist ['nrm_pasien'] = $this->nrm_pasien;
		$exist ['ruangan'] = $this->polislug;
		if ($this->dbtable->is_exist ( $exist )) {
			$row = $this->dbtable->select ( $exist );
			$the_row ['spesialisasi'] = $row->spesialisasi;
			$the_row ['anastesi'] = $row->anastesi;
			$the_row ['gol_bedah'] = $row->gol_bedah;
			$the_row ['dokter_operator'] = $row->dokter_operator;
			$the_row ['asisten_operator'] = $row->asisten_operator;
			$the_row ['perawat_rr'] = $row->perawat_rr;
			$the_row ['dokter_anastesi'] = $row->dokter_anastesi;
			$the_row ['dokter_jaga'] = $row->dokter_jaga;
			$the_row ['asisten_anastesi'] = $row->asisten_anastesi;
			$the_row ['jenis_operasi'] = $row->jenis_operasi;
			$the_row ['sifat_operasi'] = $row->sifat_operasi;
			$the_row ['asal_ruang'] = $row->asal_ruang;
			$the_row ['diagnosa'] = $row->diagnosa;
			$the_row ['tindakan'] = $row->tindakan;
			$the_row ['alamat'] = $row->alamat;
			$the_row ['instrument'] = $row->instrument;
			$the_row ['tanggal_mulai'] = $row->tanggal_mulai;
			$the_row ['tanggal_selesai'] = $row->tanggal_selesai;
			$the_row ["id"] = $row->id;
			$the_row ['umur'] = $row->umur==""?$this->umur:$row->umur;
			$the_row ['jk'] = $row->jk==""?$this->jk:$row->jk;
			$the_row ['asal_ruang'] = $row->asal_ruang==""?$this->ruang_asal:$row->asal_ruang;
			$the_row ['alamat'] = $row->alamat==""?$this->alamat:$row->alamat;
			$the_row ['noreg_pasien'] = $row->noreg_pasien==""?$this->noreg_pasien:$row->noreg_pasien;
			$the_row ['nrm_pasien'] = $row->nrm_pasien==""?$this->nrm_pasien:$row->nrm_pasien;
			$the_row ['nama_pasien'] = $row->nama_pasien==""?$this->nama_pasien:$row->nama_pasien;			
		} else if(getSettings($this->db,"smis-rs-autosave-lap-operasi","1")=="1"){
			$exist ['umur'] = $this->umur;
			$exist ['jk'] = $this->jk;
			$exist ['asal_ruang'] = $this->ruang_asal;
			$exist ['alamat'] = $this->alamat;
			$exist ['noreg_pasien'] = $this->noreg_pasien;
			$exist ['nrm_pasien'] = $this->nrm_pasien;
			$exist ['nama_pasien'] = $this->nama_pasien;
			$exist ['tanggal_mulai'] = date ( "Y-m-d H:i" );
			$exist ['tanggal_selesai'] = date ( "Y-m-d H:i" );
			$this->dbtable->insert ( $exist );
			$the_row ["id"] = $this->dbtable->get_inserted_id ();
		}
		$spesialisasi = new OptionBuilder ();
		$spesialisasi->add ( "Bedah", "Bedah", $the_row ['spesialisasi'] == "Bedah" ? "1" : "0" );
		$spesialisasi->add ( "Obstetrik &amp; Ginekologi", "Obstetrik & Ginekologi", $the_row ['spesialisasi'] == "Obstetrik & Ginekologi" ? "1" : "0" );
		$spesialisasi->add ( "Bedah Saraf", "Bedah Saraf", $the_row ['spesialisasi'] == "Bedah Saraf" ? "1" : "0" );
		$spesialisasi->add ( "T H T", "T H T", $the_row ['spesialisasi'] == "T H T" ? "1" : "0" );
		$spesialisasi->add ( "Mata", "Mata", $the_row ['spesialisasi'] == "Mata" ? "1" : "0" );
		$spesialisasi->add ( "Kulit &amp; Kelamin", "Kulit & Kelamin", $the_row ['spesialisasi'] == "Kulit & Kelamin" ? "1" : "0" );
		$spesialisasi->add ( "Gigi &amp; Mulut", "Gigi & Mulut", $the_row ['spesialisasi'] == "Gigi & Mulut" ? "1" : "0" );
		$spesialisasi->add ( "Bedah Anak", "Bedah Anak", $the_row ['spesialisasi'] == "Bedah Anak" ? "1" : "0" );
		$spesialisasi->add ( "Kardiovaskuler", "Kardiovaskuler", $the_row ['spesialisasi'] == "Kardiovaskuler" ? "1" : "0" );
		$spesialisasi->add ( "Bedah Orthopedi", "Bedah Orthopedi", $the_row ['spesialisasi'] == "Bedah Orthopedi" ? "1" : "0" );
		$spesialisasi->add ( "Thorak", "Thorak", $the_row ['spesialisasi'] == "Thorak" ? "1" : "0" );
		$spesialisasi->add ( "Digestive", "Digestive", $the_row ['spesialisasi'] == "Digestive" ? "1" : "0" );
		$spesialisasi->add ( "Urologi", "Urologi", $the_row ['spesialisasi'] == "Urologi" ? "1" : "0" );
		$spesialisasi->add ( "Lain-Lain", "Lain-Lain", $the_row ['spesialisasi'] == "Lain-Lain" ? "1" : "0" );
		
		$anastesi = new OptionBuilder ();
		$anastesi->add ( "General Anastesi (TIVA)", "General Anastesi (TIVA)", $the_row ['anastesi'] == "General Anastesi (TIVA)" ? "1" : "0" );
		$anastesi->add ( "General Anastesi (Inhalasi)", "General Anastesi (Inhalasi)", $the_row ['anastesi'] == "General Anastesi (Inhalasi)" ? "1" : "0" );
		$anastesi->add ( "General Anastesi (Intub)", "General Anastesi (Intub)", $the_row ['anastesi'] == "General Anastesi (Intub)" ? "1" : "0" );
		$anastesi->add ( "Local Anastesi", "Local Anastesi", $the_row ['anastesi'] == "Local Anastesi" ? "1" : "0" );
		$anastesi->add ( "Regional Anastesi", "Regional Anastesi", $the_row ['anastesi'] == "Regional Anastesi" ? "1" : "0" );
		$anastesi->add ( "Regional Anastesi (SAB)", "Regional Anastesi (SAB)", $the_row ['anastesi'] == "Regional Anastesi (SAB)" ? "1" : "0" );
		$anastesi->add ( "Regional Anastesi (Peridual)", "Regional Anastesi (Peridual)", $the_row ['anastesi'] == "Regional Anastesi (Peridual)" ? "1" : "0" );
		
		
		$sifat = new OptionBuilder ();
		$sifat->add ( "Operasi Elektif", "Operasi Elektif", $the_row ['sifat_operasi'] == "Operasi Elektif" || $the_row ['sifat_operasi'] == "" ? "1" : "0" );
		$sifat->add ( "Operasi Emergency", "Operasi Emergency", $the_row ['sifat_operasi'] == "Operasi Emergency" ? "1" : "0" );
		
		$kasus = new OptionBuilder ();
		$kasus->add ( "Operasi Baru", "0", $the_row ['kasus'] == "0" || $the_row ['kasus'] == "" ? "1" : "0" );
		$kasus->add ( "Operasi Ulang", "1", $the_row ['kasus'] == "1" ? "1" : "0" );
		
		$jenis_operasi = new OptionBuilder ();
		$jenis_operasi->add ( "Kecil", "Kecil", $the_row ['jenis_operasi'] == "Kecil" ? "1" : "0" );
		$jenis_operasi->add ( "Besar", "Besar", $the_row ['jenis_operasi'] == "Besar" ? "1" : "0" );
		$jenis_operasi->add ( "Sedang", "Sedang", $the_row ['jenis_operasi'] == "Sedang" ? "1" : "0" );
		$jenis_operasi->add ( "Khusus", "Khusus", $the_row ['jenis_operasi'] == "Khusus" ? "1" : "0" );
		
		$this->uitable->addModal ( "id", "hidden", "", $the_row ['id'] );
		$this->uitable->addModal ( "tanggal_mulai", "datetime", "Mulai", $the_row ['tanggal_mulai'] ,"y",null,false,null,false,"tanggal_selesai");
		$this->uitable->addModal ( "tanggal_selesai", "datetime", "Selesai", $the_row ['tanggal_selesai'] ,"y",null,false,null,true,"kasus");
		$this->uitable->addModal ( "kasus", "select", "Kasus", $kasus->getContent () ,"y",null,false,null,false,"anastesi");		
		$this->uitable->addModal ( "spesialisasi", "select", "Spesialisasi", $spesialisasi->getContent () ,"y",null,false,null,false,"anastesi");
		$this->uitable->addModal ( "anastesi", "select", "Anastesi", $anastesi->getContent () ,"y",null,false,null,false,"jenis_operasi");
		$this->uitable->addModal ( "jenis_operasi", "select", "Jenis Operasi", $jenis_operasi->getContent () ,"y",null,false,null,false,"gol_bedah");
		$this->uitable->addModal ( "sifat_operasi", "select", "Sifat Operasi", $sifat->getContent () ,"y",null,false,null,false,"gol_bedah");
		$this->uitable->addModal ( "gol_bedah", "hidden", "",  "","y",null,false,null,false,"dokter_operator");
		$this->uitable->addModal ( "dokter_operator", "chooser-lo_pasien-dokter_operator", "Dokter Operator", $the_row ['dokter_operator'],"y",null,false,null,false );
		$this->uitable->addModal ( "asisten_operator", "chooser-lo_pasien-asisten_operator", "Asisten Operator", $the_row ['asisten_operator'],null,false,null,false );
		$this->uitable->addModal ( "instrument", "chooser-lo_pasien-instrument", "Instrument", $the_row ['instrument'],null,false,null,false );
		$this->uitable->addModal ( "dokter_anastesi", "chooser-lo_pasien-dokter_anastesi", "Dokter Anastesi", $the_row ['dokter_anastesi'],null,false,null,false );
		$this->uitable->addModal ( "asisten_anastesi", "chooser-lo_pasien-asisten_anastesi", "Pranata Anastesi", $the_row ['asisten_anastesi'],null,false,null,false );
		if(getSettings($this->db, "mr-operasi-dokter-jaga", "0")=="1"){
			$this->uitable->addModal ( "dokter_jaga", "chooser-lo_pasien-dokter_jaga", "Dokter Jaga", $the_row ['dokter_jaga'],null,false,null,false );
		}else{
			$this->uitable->addModal ( "dokter_jaga", "hidden", "", $the_row ['dokter_jaga'],null,false,null,false );
		}
		$this->uitable->addModal ( "perawat_rr", "chooser-lo_pasien-perawat_rr", "Perawat RR", $the_row ['perawat_rr'],null,false,null,false );
		$this->uitable->addModal ( "diagnosa", "text", "Diagnosa", $the_row ['diagnosa'] ,"y",null,false,null,false,"tindakan");
		$this->uitable->addModal ( "tindakan", "text", "Tindakan", $the_row ['tindakan'] ,"y",null,false,null,false,"save");
		
		
		$modal = $this->uitable->getModal ();
		$modal->setTitle ( "Laporan Operasi" );
		if ($this->page == "medical_record" && $this->action == "iklin") {
			echo $this->uitable->getHtml ();
		} else {
			echo "<div class='hide'>" . $this->uitable->getHtml () . "</div>";
			$modal->setAlwaysShow ( true );
		}
        $form = $modal->joinFooterAndForm ();
        $button=new Button("","","Reload");
        $button->addClass("btn-primary");
        $button->setAction("layanan.ReLoadData('lap_operasi')");
        $button->setIcon("fa fa-refresh");
        $button->setIsButton(Button::$ICONIC_TEXT);
        
        echo $button->getHtml();
		echo $form->getHtml ();
	}
	public function jsLoader() {
		echo addJS ( "framework/bootstrap/js/bootstrap-datetimepicker.js" );
		echo addJS ( "framework/smis/js/table_action.js" );
		loadLibrary ( "smis-libs-function-javascript" );
	}
	
	public function cssLoader() {
		echo addCSS ( "framework/bootstrap/css/bootstrap-datetimepicker.css" );
	}
	
	public function cssPreLoad() {
		?>
		<style type="text/css">
				#<?php echo $this->action;?>_add_form_form>div {clear: both; width: 100%;}
				#<?php echo $this->action; ?>_add_form_form>div label { width: 250px; }
		</style>
		<?php
	}
	
	/* when it's star build */
	public function jsPreLoad() {
		?>
		<script type="text/javascript">
		var <?php echo $this->action; ?>;
		var lo_pasien;
		var lo_noreg="<?php echo $this->noreg_pasien; ?>";
		var lo_nama_pasien="<?php echo $this->nama_pasien; ?>";
		var lo_nrm_pasien="<?php echo $this->nrm_pasien; ?>";
		var lo_polislug="<?php echo $this->polislug; ?>";
		var lo_the_page="<?php echo $this->page; ?>";
		var lo_the_protoslug="<?php echo $this->protoslug; ?>";
		var lo_the_protoname="<?php echo $this->protoname; ?>";
		var lo_the_protoimplement="<?php echo $this->protoimplement; ?>";
		var lo_id_antrian="<?php echo $this->id_antrian; ?>";
		var lo_action="<?php echo $this->action; ?>";
		$(document).ready(function() {
			$('.mydatetime').datetimepicker({ minuteStep: 1});
			lo_pasien=new TableAction("lo_pasien",lo_the_page,lo_action,new Array());
			lo_pasien.setSuperCommand("lo_pasien");
			lo_pasien.setPrototipe(lo_the_protoname,lo_the_protoslug,lo_the_protoimplement);
			lo_pasien.selected=function(json){
				var nama=json.nama_pasien;
				var nrm=json.nrm;
				var noreg=json.id;		
				$("#"+lo_action+"_nama_pasien").val(nama);
				$("#"+lo_action+"_nrm_pasien").val(nrm);
				$("#"+lo_action+"_noreg_pasien").val(noreg);
			};

			dokter_operator=new TableAction("dokter_operator",lo_the_page,lo_action,new Array());
			dokter_operator.setSuperCommand("dokter_operator");
			dokter_operator.setPrototipe(lo_the_protoname,lo_the_protoslug,lo_the_protoimplement);
			dokter_operator.selected=function(json){
				$("#"+lo_action+"_dokter_operator").val(json.nama);
			};

			dokter_jaga=new TableAction("dokter_jaga",lo_the_page,lo_action,new Array());
			dokter_jaga.setSuperCommand("dokter_jaga");
			dokter_jaga.setPrototipe(lo_the_protoname,lo_the_protoslug,lo_the_protoimplement);
			dokter_jaga.selected=function(json){
				$("#"+lo_action+"_dokter_jaga").val(json.nama);
			};

			dokter_anastesi=new TableAction("dokter_anastesi",lo_the_page,lo_action,new Array());
			dokter_anastesi.setSuperCommand("dokter_anastesi");
			dokter_anastesi.setPrototipe(lo_the_protoname,lo_the_protoslug,lo_the_protoimplement);
			dokter_anastesi.selected=function(json){
				$("#"+lo_action+"_dokter_anastesi").val(json.nama);
			};

			asisten_operator=new TableAction("asisten_operator",lo_the_page,lo_action,new Array());
			asisten_operator.setSuperCommand("asisten_operator");
			asisten_operator.setPrototipe(lo_the_protoname,lo_the_protoslug,lo_the_protoimplement);
			asisten_operator.selected=function(json){
				$("#"+lo_action+"_asisten_operator").val(json.nama);
			};

			asisten_anastesi=new TableAction("asisten_anastesi",lo_the_page,lo_action,new Array());
			asisten_anastesi.setSuperCommand("asisten_anastesi");
			asisten_anastesi.setPrototipe(lo_the_protoname,lo_the_protoslug,lo_the_protoimplement);
			asisten_anastesi.selected=function(json){
				$("#"+lo_action+"_asisten_anastesi").val(json.nama);
			};

			instrument=new TableAction("instrument",lo_the_page,lo_action,new Array());
			instrument.setSuperCommand("instrument");
			instrument.setPrototipe(lo_the_protoname,lo_the_protoslug,lo_the_protoimplement);
			instrument.selected=function(json){
				$("#"+lo_action+"_instrument").val(json.nama);
			};

			perawat_rr=new TableAction("perawat_rr",lo_the_page,lo_action,new Array());
			perawat_rr.setSuperCommand("perawat_rr");
			perawat_rr.setPrototipe(lo_the_protoname,lo_the_protoslug,lo_the_protoimplement);
			perawat_rr.selected=function(json){
				$("#"+lo_action+"_perawat_rr").val(json.nama);
			};

			stypeahead("#"+lo_action+"_dokter_operator",3,dokter_operator,"nama",function(item){
				$("#"+lo_action+"_dokter_operator").val(item.nama);
				$("#"+lo_action+"_asisten_operator").focus();					
			});

			stypeahead("#"+lo_action+"_dokter_jaga",3,dokter_jaga,"nama",function(item){
				$("#"+lo_action+"_dokter_jaga").val(item.nama);
				$("#"+lo_action+"_perawat_rr").focus();					
			});

			stypeahead("#"+lo_action+"_dokter_anastesi",3,dokter_anastesi,"nama",function(item){
				$("#"+lo_action+"_dokter_anastesi").val(item.nama);
				$("#"+lo_action+"_asisten_anastesi").focus();					
			});

			stypeahead("#"+lo_action+"_asisten_operator",3,asisten_operator,"nama",function(item){
				$("#"+lo_action+"_asisten_operator").val(item.nama);
				$("#"+lo_action+"_instrument").focus();					
			});

			stypeahead("#"+lo_action+"_instrument",3,instrument,"nama",function(item){
				$("#"+lo_action+"_instrument").val(item.nama);
				$("#"+lo_action+"_dokter_anastesi").focus();					
			});
			
			stypeahead("#"+lo_action+"_perawat_rr",3,perawat_rr,"nama",function(item){
				$("#"+lo_action+"_perawat_rr").val(item.nama);
				$("#"+lo_action+"_diagnosa").focus();					
			});
			

			stypeahead("#"+lo_action+"_asisten_anastesi",3,asisten_anastesi,"nama",function(item){
				$("#"+lo_action+"_asisten_anastesi").val(item.nama);
				$("#"+lo_action+"_perawat_rr").focus();					
			});

			
			
			var column=new Array(
					"id","tanggal_mulai","tanggal_selesai","ruangan",
					"nama_pasien","noreg_pasien","nrm_pasien","dokter_jaga",
					"alamat","umur","jk","spesialisasi","anastesi",
					"dokter_operator","asisten_operator","dokter_anastesi","asisten_anastesi",
					"jenis_operasi","sifat_operasi",
					"asal_ruang","tindakan","instrument","diagnosa","perawat_rr",
					"gol_bedah","kasus"
					);
			<?php echo $this->action; ?>=new TableAction("<?php echo $this->action; ?>",lo_the_page,"<?php echo $this->action; ?>",column);
			<?php echo $this->action; ?>.setPrototipe(lo_the_protoname,lo_the_protoslug,lo_the_protoimplement);
			<?php echo $this->action; ?>.getRegulerData=function(){
				var reg_data={	
						page:this.page,
						action:this.action,
						super_command:this.super_command,
						prototype_name:this.prototype_name,
						prototype_slug:this.prototype_slug,
						prototype_implement:this.prototype_implement,
						polislug:lo_polislug,
						noreg_pasien:lo_noreg,
						nama_pasien:lo_nama_pasien,
						nrm_pasien:lo_nrm_pasien,
						id_antrian:lo_id_antrian
						};
				return reg_data;
			};

			<?php echo $this->action; ?>.setNextEnter();
			<?php echo $this->action; ?>.setEnableAutofocus(true);
			<?php
		
			if ($this->page == "medical_record" && $this->action == "iklin") {
				echo $this->action;
				?>.view();<?php
			} else {
				echo $this->action . ".clear=function(){return;};";
			}
			?>
		});
		</script>
<?php
	}
	public function superCommand($super_command) {
		/* PASIEN */
		$super = new SuperCommand ();
		
		$phead=array ('Nama','NRM',"No Reg" );
		$ptable = new Table ( $phead, "", NULL, true );
		$ptable->setName ( "lo_pasien" );
		$ptable->setModel ( Table::$SELECT );
		$padapter = new SimpleAdapter ();
		$padapter->add ( "Nama", "nama_pasien" );
		$padapter->add ( "NRM", "nrm", "digit8" );
		$padapter->add ( "No Reg", "id" );
		$presponder = new ServiceResponder ( $this->db, $ptable, $padapter, "get_registered" );		
		$super->addResponder ( "lo_pasien", $presponder );
		
		$eadapt = new SimpleAdapter ();
		$eadapt->add ( "Jabatan", "nama_jabatan" );
		$eadapt->add ( "Nama", "nama" );
		$eadapt->add ( "NIP", "nip" );
		
		$head_karyawan=array ('Nama','Jabatan',"NIP");
		$dktable = new Table ( $head_karyawan, "", NULL, true );
		$dktable->setName ( "dokter_operator" );
		$dktable->setModel ( Table::$SELECT );
		$employee = new EmployeeResponder ( $this->db, $dktable, $eadapt, "dokter" );
		$super->addResponder ( "dokter_operator", $employee );
		
		$dktable = new Table ($head_karyawan, "", NULL, true );
		$dktable->setName ( "dokter_anastesi" );
		$dktable->setModel ( Table::$SELECT );
		$employee = new EmployeeResponder ( $this->db, $dktable, $eadapt, "dokter" );
		$super->addResponder ( "dokter_anastesi", $employee );
		
		$dktable = new Table ($head_karyawan, "", NULL, true );
		$dktable->setName ( "dokter_jaga" );
		$dktable->setModel ( Table::$SELECT );
		$employee = new EmployeeResponder ( $this->db, $dktable, $eadapt, "dokter" );
		$super->addResponder ( "dokter_jaga", $employee );
		
		
		$dktable = new Table ( $head_karyawan, "", NULL, true );
		$dktable->setName ( "asisten_operator" );
		$dktable->setModel ( Table::$SELECT );
		$employee = new EmployeeResponder ( $this->db, $dktable, $eadapt, "" );
		$super->addResponder ( "asisten_operator", $employee );
		
		$dktable = new Table ( $head_karyawan, "", NULL, true );
		$dktable->setName ( "asisten_anastesi" );
		$dktable->setModel ( Table::$SELECT );
		$employee = new EmployeeResponder ( $this->db, $dktable, $eadapt, "" );
		$super->addResponder ( "asisten_anastesi", $employee );
		
		$dktable = new Table ( $head_karyawan, "", NULL, true );
		$dktable->setName ( "perawat_rr" );
		$dktable->setModel ( Table::$SELECT );
		$employee = new EmployeeResponder ( $this->db, $dktable, $eadapt, "" );
		$super->addResponder ( "perawat_rr", $employee );
		
		$dktable = new Table ( $head_karyawan, "", NULL, true );
		$dktable->setName ( "instrument" );
		$dktable->setModel ( Table::$SELECT );
		$employee = new EmployeeResponder ( $this->db, $dktable, $eadapt, "" );
		$super->addResponder ( "instrument", $employee );	
		
		
		$init = $super->initialize ();
		if ($init != null) {
			echo $init;
			return;
		}
	}
}

?>