<?php

require_once 'smis-framework/smis/template/ModulTemplate.php';
require_once ("smis-base/smis-include-service-consumer.php");
require_once "medical_record/class/service/RuanganService.php";
require_once 'medical_record/library/EmployeeResponder.php';

class JenisPerawatanTemplate extends ModulTemplate {
    protected $db;
	protected $polislug;
	protected $page;
    protected $action;
	protected $protoslug;
	protected $protoname;
	protected $protoimplement;
	protected $dbtable;
	protected $uitable;
	protected $noreg_pasien;
	protected $nama_pasien;
	protected $nrm_pasien;
    protected $sebutan;
    protected $jk;
    protected $tanggal;
    public static $MODE_DAFTAR = "pendaftaran";
	public static $MODE_PERIKSA = "pemeriksaan";
    
    public function __construct($db, $mode, $polislug = "all", $page = "medical_record", $action = "input_jenis_perawatan", $protoslug = "", $protoname = "", $protoimplement = "", $noreg = "", $nrm = "", $nama = "") {
        $this->db               = $db;
        $this->mode             = $mode;
        $this->polislug         = $polislug;
        $this->page             = $page;
        $this->action           = $action;
		$this->protoslug        = $protoslug;
		$this->protoimplement   = $protoimplement;
        $this->protoname        = $protoname;
		$this->tanggal          = date("Y-m-d H:i");
		$this->noreg_pasien     = $noreg;
		$this->nama_pasien      = $nama;
		$this->nrm_pasien       = $nrm;
		$this->dbtable          = new DBTable ( $this->db, "smis_mr_jenis_perawatan" );
        
        if ($polislug != "all")
			$this->dbtable->addCustomKriteria ( "ruangan", "='" . $polislug . "'" );
		if ($noreg != "")
			$this->dbtable->addCustomKriteria ( "noreg_pasien", "='" . $noreg . "'" );
            
        if ($noreg != "") {
			$data_post = array ("command" => "edit","id" => $noreg );
			$service = new ServiceConsumer($this->db, "get_registered", $data_post);
			$service->execute ();
			$data = $service->getContent ();
			if($data!=NULL){
                $this->sebutan          = $data ['sebutan'];
                $this->jk               = $data ['kelamin'];
			}
		}
        
        $array=array ("No.","Tanggal","Jenis Perawatan", "Perawat", "Keterangan", "Jenis Luka" );
		$this->uitable = new Table ( $array, " Jenis Perawatan " . ($this->polislug == "all" ? "" : ucfirst ( $this->protoname )), NULL, true );
		$this->uitable->setName ( $action );
    }
    
    public function command($command) {
        $adapter = new SimpleAdapter ();
        $adapter->setUseNumber(true, "No.","back.");
		$adapter->add ( "Tanggal", "tanggal", "date d M Y H:i" );
		$adapter->add ( "Jenis Perawatan", "jenis_perawatan" );
		$adapter->add ( "Perawat", "nama_perawat" );
		$adapter->add ( "Keterangan", "keterangan" );
		$adapter->add ( "Jenis Luka", "jenis_luka" );
		$dbres = new DBResponder ( $this->dbtable, $this->uitable, $adapter );
		$data = $dbres->command ( $_POST ['command'] );
		echo json_encode ( $data );
		return;
    }
    
    public function phpPreLoad() {
        if ($this->noreg_pasien != "") {
            $this->uitable->addModal ( "id", "hidden", "", "", "y", null, false, null, false, null );
			$this->uitable->addModal ( "nama_pasien", "hidden", "", $this->nama_pasien, "n", null, true );
			$this->uitable->addModal ( "nrm_pasien", "hidden", "", $this->nrm_pasien, "n", null, true, null, false, null );
			$this->uitable->addModal ( "noreg_pasien", "hidden", "", $this->noreg_pasien, "n", null, true, null, false, null );
			$this->uitable->addModal ( "sebutan", "hidden", "", $this->sebutan, "y", null, true, null, false, null );
			$this->uitable->addModal ( "jk", "hidden", "", $this->jk, "y", null, true, null, false, null );
		} else {
            $this->uitable->addModal ( "id", "hidden", "", "", "y", null, true, null, false, null );
			$this->uitable->addModal ( "nama_pasien", "chooser-" . $this->action . "-jenis_perawatan_pasien", "Pasien", $this->nama_pasien, "n", null, true, null, false, null );
			$this->uitable->addModal ( "nrm_pasien", "text", "NRM", $this->nrm_pasien, "n", null, true, null, false, null );
			$this->uitable->addModal ( "noreg_pasien", "text", "No Reg", $this->noreg_pasien, "n", null, true, null, false, null );
			$this->uitable->addModal ( "sebutan", "hidden", "", "", "y", null, true, null, false, null );
			$this->uitable->addModal ( "jk", "hidden", "", "", "y", null, true, null, false, null );
		}
        
        if ($this->polislug == "all"){
			$service = new RuanganService ( $this->db );
			$service->execute ();
			$ruangan = $service->getContent ();
			$this->uitable->addModal ( "ruangan", "select", "Ruangan", $ruangan, "y", null, true, null, false, null );
		}else{
			$this->uitable->addModal ( "ruangan", "hidden", "", $this->polislug, "n", null, true, null, false, null );
		}
        
        $jenis_perawatan = new OptionBuilder ();
		$jenis_perawatan->add ( "", "", "1" );
		$jenis_perawatan->add ( "IVL", "IVL" );
		$jenis_perawatan->add ( "CVL", "CVL" );
		$jenis_perawatan->add ( "UC", "UC" );
		$jenis_perawatan->add ( "ETT", "ETT" );
		$jenis_perawatan->add ( "Pasien Tirah Baring", "Pasien Tirah Baring" );
		$jenis_perawatan->add ( "Pasca Operasi", "Pasca Operasi" );
        
        $jenis_luka = new OptionBuilder ();
		$jenis_luka->add ( "", "", "1" );
		$jenis_luka->add ( "Luka Bersih", "Luka Bersih" );
		$jenis_luka->add ( "Luka Kotor", "Luka Kotor" );
        
        $this->uitable->addModal ( "tanggal", "datetime", "Tanggal Pemakaian", $this->tanggal, "n", true, false, null, false, null );
        $this->uitable->addModal ( "jenis_perawatan", "select", "Jenis Perawatan", $jenis_perawatan->getContent (), "n", null, false, null, false, null );
        $this->uitable->addModal ( "nama_perawat", "chooser-" . $this->action . "-jenis_perawatan_perawat-Pilih Perawat", "Perawat", "", "n", null, true, null, false, null );
        $this->uitable->addModal ( "id_perawat", "hidden", "", "", "y", null, false, null, false, null );
        $this->uitable->addModal ( "keterangan", "textarea", "Keterangan", "", "y", null, false, null, false, null );
        $this->uitable->addModal ( "jenis_luka", "select", "Jenis Luka", $jenis_luka->getContent (), "y", null, false, null, false, null );
        
        $modal = $this->uitable->getModal ();
		$modal->setComponentSize(Modal::$MEDIUM);
		$modal->setTitle ( "Jenis Perawatan" );
        $modal->addHTML("
            <div class='alert alert-block alert-inverse' id='help_desk'>
                <h4>Keterangan Pilihan Jenis Perawatan</h4>
                <ul>
                    <li>IVL = Pemasangan Infus</li>
                    <li>CVL = Pemasangan CVC</li>
                    <li>UC = Kateter</li>
                    <li>ETT = Tindakan Intubasi</li>
                <ul>
            </div>
        ", "before");
		echo $this->uitable->getHtml ();
		echo $modal->getHtml ();
    }
    
    public function jsLoader() {
		echo addJS ( "framework/bootstrap/js/bootstrap-datetimepicker.js" );
		echo addJS ( "framework/smis/js/table_action.js" );
		loadLibrary ( "smis-libs-function-javascript" );
	}
    
	public function cssLoader() {
		echo addCSS ( "framework/bootstrap/css/bootstrap-datetimepicker.css" );
	}
    
    public function jsPreLoad() {
?>
    <script type="text/javascript">
        var <?php echo $this->action; ?>;
        var jenis_perawatan_noreg="<?php echo $this->noreg_pasien; ?>";
        var jenis_perawatan_pasien;
        var jenis_perawatan_perawat;
        var jenis_perawatan_nama_pasien="<?php echo $this->nama_pasien; ?>";
        var jenis_perawatan_nrm_pasien="<?php echo $this->nrm_pasien; ?>";
        var jenis_perawatan_polislug="<?php echo $this->polislug; ?>";
        var jenis_perawatan_the_page="<?php echo $this->page; ?>";
        var jenis_perawatan_the_protoslug="<?php echo $this->protoslug; ?>";
        var jenis_perawatan_the_protoname="<?php echo $this->protoname; ?>";
        var jenis_perawatan_the_protoimplement="<?php echo $this->protoimplement; ?>";
    
        $(document).ready(function() {
            $(".mydatetime").datetimepicker({ minuteStep: 1});
            
            var column=new Array(
                "id", "tanggal", "noreg_pasien", "nrm_pasien", "nama_pasien", "sebutan", "ruangan",
                "jk", "jenis_perawatan", "id_perawat", "nama_perawat", "keterangan", "jenis_luka"
            );
            <?php echo $this->action; ?>=new TableAction("<?php echo $this->action; ?>", jenis_perawatan_the_page,"<?php echo $this->action; ?>", column);
            <?php echo $this->action; ?>.setPrototipe(jenis_perawatan_the_protoname, jenis_perawatan_the_protoslug, jenis_perawatan_the_protoimplement);
            <?php echo $this->action; ?>.getRegulerData=function(){
                var reg_data={	
                        page:this.page,
                        action:this.action,
                        super_command:this.super_command,
                        prototype_name:this.prototype_name,
                        prototype_slug:this.prototype_slug,
                        prototype_implement:this.prototype_implement,
                        polislug:jenis_perawatan_polislug,
                        noreg_pasien:jenis_perawatan_noreg,
                        nama_pasien:jenis_perawatan_nama_pasien,
                        nrm_pasien:jenis_perawatan_nrm_pasien
                        };
                return reg_data;
            };
            <?php echo $this->action; ?>.view();
            
            jenis_perawatan_pasien=new TableAction("jenis_perawatan_pasien", jenis_perawatan_the_page, "<?php echo $this->action; ?>",new Array());
            jenis_perawatan_pasien.setSuperCommand("jenis_perawatan_pasien");
            jenis_perawatan_pasien.setPrototipe(jenis_perawatan_the_protoname, jenis_perawatan_the_protoslug, jenis_perawatan_the_protoimplement);
            jenis_perawatan_pasien.selected=function(json){
                $("#<?php echo $this->action; ?>_nama_pasien").val(json.nama_pasien);
                $("#<?php echo $this->action; ?>_nrm_pasien").val(json.nrm);
                $("#<?php echo $this->action; ?>_noreg_pasien").val(json.id);
                $("#<?php echo $this->action; ?>_jk").val(json.kelamin);
                $("#<?php echo $this->action; ?>_sebutan").val(json.sebutan);
                if(json.kamar_inap == "" || json.kamar_inap == null) {
                    $("#<?php echo $this->action; ?>_ruangan").val(json.jenislayanan);
                } else {
                    $("#<?php echo $this->action; ?>_ruangan").val(json.kamar_inap);
                }
            };
            
            jenis_perawatan_perawat=new TableAction("jenis_perawatan_perawat", jenis_perawatan_the_page, "<?php echo $this->action; ?>",new Array());
            jenis_perawatan_perawat.setSuperCommand("jenis_perawatan_perawat");
            jenis_perawatan_perawat.setPrototipe(jenis_perawatan_the_protoname, jenis_perawatan_the_protoslug, jenis_perawatan_the_protoimplement);
            jenis_perawatan_perawat.selected=function(json){
                $("#<?php echo $this->action; ?>_nama_perawat").val(json.nama);
                $("#<?php echo $this->action; ?>_id_perawat").val(json.id);
            };
        });
    </script>
<?php
    }
    
    public function superCommand($super_command) {
        $super = new SuperCommand ();
        if($super_command == 'jenis_perawatan_perawat') {
            $array=array ('Nama','Jabatan',"NIP" );
            $dktable = new Table ($array);
            $dktable->setName ( "jenis_perawatan_perawat" );
            $dktable->setModel ( Table::$SELECT );
            $dkadapter = new SimpleAdapter ();
            $dkadapter->add ( "Jabatan", "nama_jabatan" );
            $dkadapter->add ( "Nama", "nama" );
            $dkadapter->add ( "NIP", "nip" );
            $dkresponder = new EmployeeResponder($this->db, $dktable, $dkadapter) ;
            $super->addResponder ( "jenis_perawatan_perawat", $dkresponder );
        }
        if($super_command == 'jenis_perawatan_pasien') {
            /* PASIEN */
            $aname=array ('Sebutan','Nama','NRM',"No Reg" );
            $ptable = new Table ( $aname);
            $ptable->setName ( "jenis_perawatan_pasien" );
            $ptable->setModel ( Table::$SELECT );
            $padapter = new SimpleAdapter ();
            $padapter->add ( "Sebutan", "sebutan" );
            $padapter->add ( "Nama", "nama_pasien" );
            $padapter->add ( "NRM", "nrm", "digit8" );
            $padapter->add ( "No Reg", "id" );
            $presponder = new ServiceResponder ( $this->db, $ptable, $padapter, "get_registered" );
            $super->addResponder ( "jenis_perawatan_pasien", $presponder );
        }
		$init = $super->initialize ();
		if ($init != null) {
			echo $init;
			return;
		}
	}
}

?>