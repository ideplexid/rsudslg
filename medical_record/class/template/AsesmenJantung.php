<?php

require_once 'smis-framework/smis/template/ModulTemplate.php';
require_once ("smis-base/smis-include-service-consumer.php");
require_once "medical_record/class/service/RuanganService.php";
require_once 'smis-libs-hrd/EmployeeResponder.php';

class AsesmenJantung extends ModulTemplate {
    protected $db;
	protected $polislug;
	protected $page;
	protected $protoslug;
	protected $protoname;
	protected $protoimplement;
	protected $action;
	protected $dbtable;
	protected $uitable;
	protected $noreg_pasien;
	protected $nama_pasien;
	protected $nrm_pasien;
	protected $alamat;
	protected $umur;
    protected $tgl_lahir;
    protected $jk;
	protected $ruang_asal;
	protected $id_antrian;
    protected $carabayar;
    protected $waktu_masuk;
    
    public function __construct($db, $polislug = "all", $noreg = "", $nrm = "", $nama = "", $alamat = "", $umur = "", $jk = "0", $tgl_lahir = "", $waktu_masuk = "", $id_antrian = "0", $page = "medical_record", $action = "asesmen_jantung",$carabayar, $protoslug = "", $protoname = "", $protoimplement = "") {
        $this->db               = $db;
		$this->noreg_pasien     = $noreg;
		$this->id_antrian       = $id_antrian;
		$this->nama_pasien      = $nama;
		$this->nrm_pasien       = $nrm;
		$this->alamat           = $alamat;
		$this->umur             = $umur;
        $this->jk               = $jk;
        $this->tgl_lahir        = $tgl_lahir;
        $this->waktu_masuk      = $waktu_masuk;
		$this->ruang_apgl       = $ruang_asal;
		$this->polislug         = $polislug;
		$this->dbtable          = new DBTable ( $this->db, "smis_mr_assesmen_jantung" );
		$this->page             = $page;
		$this->protoslug        = $protoslug;
		$this->protoimplement   = $protoimplement;
		$this->protoname        = $protoname;
		$this->action           = $action;
        $this->carabayar        = $carabayar;
        
        if ($noreg != ""){
            $this->dbtable->addCustomKriteria ( "noreg_pasien", "='" . $noreg . "'" );
        }
        $head=array ('NRM',"Pasien","No Register" );
		$this->uitable = new Table ($head , " Asesmen Jantung - " . ($this->polislug == "all" ? "" : ucfirst ( $this->protoname )), NULL, true );
		$this->uitable->setName ( $action );
    }
    
    public function command($command){
        require_once "smis-base/smis-include-duplicate.php";
		$adapter = new SimpleAdapter ();
		$adapter->add ( "Pasien", "nama_pasien" );
		$adapter->add ( "NRM", "nrm_pasien", "digit8" );
		$adapter->add ( "No Register", "noreg_pasien", "digit8" );
		$dbres = new DuplicateResponder ( $this->dbtable, $this->uitable, $adapter );
		$dbres->setDuplicate(false,"");
        $dbres->setAutonomous(getSettings($this->db,"smis_autonomous_id",""));
        $data = $dbres->command ( $_POST ['command'] );
		echo json_encode ( $data );
		return;
    }
    
    public function phpPreLoad(){
        $service = new RuanganService ( $this->db );
		$service->execute ();
		$ruangan = $service->getContent ();
        
        /*block data header*/
		if ($this->noreg_pasien != "") {
			$this->uitable->addModal ( "nama_pasien", "hidden", "", $this->nama_pasien, "n", null, true );
			$this->uitable->addModal ( "nrm_pasien", "hidden", "", $this->nrm_pasien, "n", null, true );
			$this->uitable->addModal ( "noreg_pasien", "hidden", "", $this->noreg_pasien, "n", null, true );
			$this->uitable->addModal ( "alamat_pasien", "hidden", "", $this->alamat, "n", null, true );
			$this->uitable->addModal ( "jk", "hidden", "", $this->jk, "n", null, true );
			$this->uitable->addModal ( "umur", "hidden", "", $this->umur, "n", null, true );
            $this->uitable->addModal ( "tgl_lahir_pasien", "hidden", "", $this->tgl_lahir, "n", null, true );
            $this->uitable->addModal ( "waktu_masuk", "hidden", "", $this->waktu_masuk, "n", null, true );
		} else {
			$this->uitable->addModal ( "nama_pasien", "chooser-" . $this->action . "-apj_pasien-Pasien", "Pasien", $this->nama_pasien, "n", null, true );
			$this->uitable->addModal ( "nrm_pasien", "text", "NRM", $this->nrm_pasien, "n", null, true );
			$this->uitable->addModal ( "noreg_pasien", "text", "No Reg", $this->noreg_pasien, "n", null, true );
		}
		if ($this->polislug == "all")
			$this->uitable->addModal ( "ruangan", "select", "Asal Ruangan", $ruangan );
		else
			$this->uitable->addModal ( "ruangan", "hidden", "", $this->polislug, "n", null, true );
        /*end - block data header*/
        
        $the_row = array ();
		$the_row ["id"] 						        = "";
		$the_row ["nama_dokter"] 				        = "";
		$the_row ["id_dokter"] 					        = "";
		$the_row ['waktu'] 					            = date ( "Y-m-d H:i:s" );
        $the_row ["carabayar"] 				            = $this->carabayar;
        $the_row ["waktu_masuk"] 				        = $this->waktu_masuk;
        $the_row ["keluhan_utama"] 					    = "";
        $the_row ["riwayat_penyakit_skrg"] 				= "";
        $the_row ["riwayat_pengobatan"] 				= "";
        $the_row ["torak"] 					            = "";
        $the_row ["torak_ket"] 					        = "";
        $the_row ["iktus_kordis"] 					    = "";
        $the_row ["iktus_kordis_ket"] 					= "";
        $the_row ["iktus_kordis_lokasi"] 				= "";
        $the_row ["pulpasi"] 					        = "";
        $the_row ["pulpasi_ket"] 					    = "";
        $the_row ["palpasi_iktus_kordis"] 				= "";
        $the_row ["palpasi_iktus_kordis_ket"] 			= "";
        $the_row ["palpasi_iktus_kordis_lokasi"] 		 = "";
        $the_row ["palpasi_thrill"] 					= "";
        $the_row ["palpasi_thrill_ket"] 				= "";
        $the_row ["palpasi_thrill_lokasi"] 				= "";
        $the_row ["perkusi_batas_atas"] 				= "";
        $the_row ["perkusi_batas_bawah"] 				= "";
        $the_row ["perkusi_batas_kanan"] 				= "";
        $the_row ["perkusi_batas_kiri"] 				= "";
        $the_row ["sju"] 					            = "";
        $the_row ["sju_s1"] 					        = "";
        $the_row ["sju_s2"] 					        = "";
        $the_row ["extra_systole"] 					    = "";
        $the_row ["gallop"] 					        = "";
        $the_row ["murmur_fase"] 					    = "";
        $the_row ["murmur_fase_ket"] 					= "";
        $the_row ["murmur_lokasi"] 					    = "";
        $the_row ["murmur_lokasi_ket"] 					= "";
        $the_row ["murmur_kualitas"] 					= "";
        $the_row ["murmur_kualitas_ket"] 				= "";
        $the_row ["murmur_grade"] 					    = "";
        $the_row ["murmur_penjalaran"] 					= "";
        $the_row ["murmur_penjalaran_ket"] 				= "";
        $the_row ["murmur_opening_snap"] 				= "";
        $the_row ["murmur_friction_rub"] 				= "";
        $the_row ["paru"] 					            = "";
        $the_row ["paru_ket"] 					        = "";
        $the_row ["ecg"] 					            = "";
        $the_row ["thorax_foto"] 					    = "";
        $the_row ["echocardiografi"] 					= "";
        $the_row ["diagnosis"] 					        = "";
        $the_row ["terapi"] 					        = "";
        $the_row ["rencana_kerja"] 					    = "";
        $the_row ["waktu_pulang"] 					    = "";
        $the_row ["waktu_kontrol_klinik"] 				= "";
        $the_row ["dirawat_diruang"] 					= "";
        
        $exist ['noreg_pasien'] = $this->noreg_pasien;
		$exist ['nama_pasien'] = $this->nama_pasien;
		$exist ['nrm_pasien'] = $this->nrm_pasien;
        
        if ($this->dbtable->is_exist ( $exist )){
            $row = $this->dbtable->select ( $exist );
            $the_row ["id"] 					            = $row->id;
            $the_row ["nama_dokter"] 			            = $row->nama_dokter;
            $the_row ["id_dokter"] 				            = $row->id_dokter;
            $the_row ["waktu"] 				                = $row->waktu;
            $the_row ["waktu_masuk"] 				        = $row->waktu_masuk;
            $the_row ["keluhan_utama"] 				        = $row->keluhan_utama;
            $the_row ["riwayat_penyakit_skrg"] 				= $row->riwayat_penyakit_skrg;
            $the_row ["riwayat_pengobatan"] 				= $row->riwayat_pengobatan;
            $the_row ["torak"] 				                = $row->torak;
            $the_row ["torak_ket"] 				            = $row->torak_ket;
            $the_row ["iktus_kordis"] 				        = $row->iktus_kordis;
            $the_row ["iktus_kordis_ket"] 				    = $row->iktus_kordis_ket;
            $the_row ["iktus_kordis_lokasi"] 				= $row->iktus_kordis_lokasi;
            $the_row ["pulpasi"] 				            = $row->pulpasi;
            $the_row ["pulpasi_ket"] 				        = $row->pulpasi_ket;
            $the_row ["palpasi_iktus_kordis"] 				= $row->palpasi_iktus_kordis;
            $the_row ["palpasi_iktus_kordis_ket"] 			= $row->palpasi_iktus_kordis_ket;
            $the_row ["palpasi_iktus_kordis_lokasi"] 		= $row->palpasi_iktus_kordis_lokasi;
            $the_row ["palpasi_thrill"] 				    = $row->palpasi_thrill;
            $the_row ["palpasi_thrill_ket"] 				= $row->palpasi_thrill_ket;
            $the_row ["palpasi_thrill_lokasi"] 				= $row->palpasi_thrill_lokasi;
            $the_row ["perkusi_batas_atas"] 				= $row->perkusi_batas_atas;
            $the_row ["perkusi_batas_bawah"] 				= $row->perkusi_batas_bawah;
            $the_row ["perkusi_batas_kanan"] 				= $row->perkusi_batas_kanan;
            $the_row ["perkusi_batas_kiri"] 				= $row->perkusi_batas_kiri;
            $the_row ["sju"] 				                = $row->sju;
            $the_row ["sju_s1"] 				            = $row->sju_s1;
            $the_row ["sju_s2"] 				            = $row->sju_s2;
            $the_row ["extra_systole"] 				        = $row->extra_systole;
            $the_row ["gallop"] 				            = $row->gallop;
            $the_row ["murmur_fase"] 				        = $row->murmur_fase;
            $the_row ["murmur_fase_ket"] 				    = $row->murmur_fase_ket;
            $the_row ["murmur_lokasi"] 				        = $row->murmur_lokasi;
            $the_row ["murmur_lokasi_ket"] 				    = $row->murmur_lokasi_ket;
            $the_row ["murmur_kualitas"] 				    = $row->murmur_kualitas;
            $the_row ["murmur_kualitas_ket"] 				= $row->murmur_kualitas_ket;
            $the_row ["murmur_grade"] 				        = $row->murmur_grade;
            $the_row ["murmur_penjalaran"] 				    = $row->murmur_penjalaran;
            $the_row ["murmur_penjalaran_ket"] 				= $row->murmur_penjalaran_ket;
            $the_row ["murmur_opening_snap"] 				= $row->murmur_opening_snap;
            $the_row ["murmur_friction_rub"] 				= $row->murmur_friction_rub;
            $the_row ["paru"] 				                = $row->paru;
            $the_row ["paru_ket"] 				            = $row->paru_ket;
            $the_row ["ecg"] 				                = $row->ecg;
            $the_row ["thorax_foto"] 				        = $row->thorax_foto;
            $the_row ["echocardiografi"] 				    = $row->echocardiografi;
            $the_row ["diagnosis"] 				            = $row->diagnosis;
            $the_row ["terapi"] 				            = $row->terapi;
            $the_row ["rencana_kerja"] 				        = $row->rencana_kerja;
            $the_row ["waktu_pulang"] 				        = $row->waktu_pulang;
            $the_row ["waktu_kontrol_klinik"] 				= $row->waktu_kontrol_klinik;
            $the_row ["dirawat_diruang"] 				    = $row->dirawat_diruang;
        }else{
            $exist ['umur'] = $this->umur;
			$exist ['jk'] = $this->jk;
			$exist ['alamat_pasien'] = $this->alamat;
			$exist ['noreg_pasien'] = $this->noreg_pasien;
			$exist ['nrm_pasien'] = $this->nrm_pasien;
			$exist ['nama_pasien'] = $this->nama_pasien;
            $exist ['ruangan'] = $this->polislug;
            $exist ['tgl_lahir_pasien'] = $this->tgl_lahir;
			$exist ['waktu'] = date ( "Y-m-d H:i:s" );
            $exist ['waktu_masuk'] = $this->waktu_masuk;
            $this->dbtable->insert ( $exist );
			
            $the_row ["id"] = $this->dbtable->get_inserted_id ();
            
            $update['origin_id']=$the_row ["id"];
            $update['autonomous']="[".getSettings($this->db,"smis_autonomous_id","")."]";
            $this->dbtable->update($update,array("id"=>$the_row ["id"]));
        }
        
        $this->uitable->addModal ( "id", "hidden", "", $the_row ['id'] );
		$this->uitable->addModal ( "nama_dokter", "chooser-apj_pasien-dokter_apj-Dokter Pemeriksa", "Dokter Pemeriksa", $the_row ['nama_dokter'],"n",null,false,null,true);
		$this->uitable->addModal ( "id_dokter", "hidden", "", $the_row ['id_dokter'],"y",null,false,null,false);
		$this->uitable->addModal ( "waktu", "datetime", "Waktu", $the_row ['waktu'] ,"y",null,false,null,false);
        $this->uitable->addModal("", "label", "<strong>ANAMNESES</strong>", "");
		$this->uitable->addModal ( "keluhan_utama", "text", "Keluhan Utama", $the_row ['keluhan_utama'] ,"y",null,false,null,false);
		$this->uitable->addModal ( "riwayat_penyakit_skrg", "text", "Riwayat Penyakit Sekarang", $the_row ['riwayat_penyakit_skrg'] ,"y",null,false,null,false);
		$this->uitable->addModal ( "riwayat_pengobatan", "text", "Riwayat Pengobatan", $the_row ['riwayat_pengobatan'] ,"y",null,false,null,false);
        $this->uitable->addModal("", "label", "<strong>PEMERIKSAAN FISIK</strong>", "");
        $this->uitable->addModal("", "label", "<strong>Jantung</strong>", "");
        $torak=array(
            array("name"=>"","value"=>"","default"=> $the_row ["torak"]==""?1:0),
            array("name"=>"Simetris","value"=>"Simetris","default"=> $the_row ["torak"]=="Simetris"?1:0),
            array("name"=>"Asimetris","value"=>"Asimetris","default"=> $the_row ["torak"]=="Asimetris"?1:0),
		);
        $this->uitable->addModal("torak", "select", "Torak", $torak,"y",NULL,false,NULL,false);
        $this->uitable->addModal ( "torak_ket", "text", "Keterangan", $the_row ['torak_ket'] ,"y",null,false,null,false);
        $this->uitable->addModal("", "label", "Inspeksi", "");
        $iktus_kordis=array(
            array("name"=>"","value"=>"","default"=> $the_row ["iktus_kordis"]==""?1:0),
            array("name"=>"Normal","value"=>"Normal","default"=> $the_row ["iktus_kordis"]=="Normal"?1:0),
            array("name"=>"Melebar ke","value"=>"Melebar","default"=> $the_row ["iktus_kordis"]=="Melebar"?1:0),
		);
        $this->uitable->addModal("iktus_kordis", "select", "Iktus Kordis", $iktus_kordis,"y",NULL,false,NULL,false);
        $this->uitable->addModal ( "iktus_kordis_ket", "text", "Keterangan", $the_row ['iktus_kordis_ket'] ,"y",null,false,null,false);
        $this->uitable->addModal ( "iktus_kordis_lokasi", "text", "Lokasi", $the_row ['iktus_kordis_lokasi'] ,"y",null,false,null,false);
        $pulpasi=array(
            array("name"=>"","value"=>"","default"=> $the_row ["pulpasi"]==""?1:0),
            array("name"=>"Apex","value"=>"Apex","default"=> $the_row ["pulpasi"]=="Apex"?1:0),
            array("name"=>"Prekordium","value"=>"Prekordium","default"=> $the_row ["pulpasi"]=="Prekordium"?1:0),
            array("name"=>"Epigastrum","value"=>"Epigastrum","default"=> $the_row ["pulpasi"]=="Epigastrum"?1:0),
            array("name"=>"Lain-Lain","value"=>"Lain-Lain","default"=> $the_row ["pulpasi"]=="Lain-Lain"?1:0),
		);
        $this->uitable->addModal("pulpasi", "select", "Pulpasi", $pulpasi,"y",NULL,false,NULL,false);
        $this->uitable->addModal ( "pulpasi_ket", "text", "Keterangan", $the_row ['pulpasi_ket'] ,"y",null,false,null,false);
        $this->uitable->addModal("", "label", "Palpasi", "");
        $iktus_kordisp=array(
            array("name"=>"","value"=>"","default"=> $the_row ["palpasi_iktus_kordis"]==""?1:0),
            array("name"=>"Normal","value"=>"Normal","default"=> $the_row ["palpasi_iktus_kordis"]=="Normal"?1:0),
            array("name"=>"Kuat Angkat","value"=>"Kuat Angkat","default"=> $the_row ["palpasi_iktus_kordis"]=="Kuat Angkat"?1:0),
            array("name"=>"Meluas","value"=>"Meluas","default"=> $the_row ["palpasi_iktus_kordis"]=="Meluas"?1:0),
		);
        $this->uitable->addModal("palpasi_iktus_kordis", "select", "Iktus Kordis", $iktus_kordisp,"y",NULL,false,NULL,false);
        $this->uitable->addModal ( "palpasi_iktus_kordis_lokasi", "text", "Lokasi", $the_row ['palpasi_iktus_kordis_lokasi'] ,"y",null,false,null,false);
        $palpasi_thrill=array(
            array("name"=>"","value"=>"","default"=> $the_row ["palpasi_thrill"]==""?1:0),
            array("name"=>"Sistolik","value"=>"Sistolik","default"=> $the_row ["palpasi_thrill"]=="Sistolik"?1:0),
            array("name"=>"Diastolik","value"=>"Diastolik","default"=> $the_row ["palpasi_thrill"]=="Diastolik"?1:0),
		);
        $this->uitable->addModal("palpasi_thrill", "select", "Thrill", $palpasi_thrill,"y",NULL,false,NULL,false);
        $this->uitable->addModal ( "palpasi_thrill_lokasi", "text", "Lokasi", $the_row ['palpasi_thrill_lokasi'] ,"y",null,false,null,false);
        $this->uitable->addModal("", "label", "Perkusi", "");
        $this->uitable->addModal ( "perkusi_batas_atas", "text", "Batas Atas", $the_row ['perkusi_batas_atas'] ,"y",null,false,null,false);
        $this->uitable->addModal ( "perkusi_batas_bawah", "text", "Batas Bawah", $the_row ['perkusi_batas_bawah'] ,"y",null,false,null,false);
        $this->uitable->addModal ( "perkusi_batas_kanan", "text", "Batas Kanan", $the_row ['perkusi_batas_kanan'] ,"y",null,false,null,false);
        $this->uitable->addModal ( "perkusi_batas_kiri", "text", "Batas Kiri", $the_row ['perkusi_batas_kiri'] ,"y",null,false,null,false);
        $this->uitable->addModal("", "label", "Auskultasi", "");
        $sju=array(
            array("name"=>"","value"=>"","default"=> $the_row ["sju"]==""?1:0),
            array("name"=>"Reguler","value"=>"Reguler","default"=> $the_row ["sju"]=="Reguler"?1:0),
            array("name"=>"Ireguler","value"=>"Ireguler","default"=> $the_row ["sju"]=="Ireguler"?1:0),
		);
        $this->uitable->addModal("sju", "select", "Suara Jantung Utama", $sju,"y",NULL,false,NULL,false);
        $this->uitable->addModal ( "sju_s1", "text", "S1", $the_row ['sju_s1'] ,"y",null,false,null,false);
        $this->uitable->addModal ( "sju_s2", "text", "S1", $the_row ['sju_s2'] ,"y",null,false,null,false);
        $extra_systole=array(
            array("name"=>"","value"=>"","default"=> $the_row ["extra_systole"]==""?1:0),
            array("name"=>"Positif (+)","value"=>"Positif","default"=> $the_row ["extra_systole"]=="Positif"?1:0),
            array("name"=>"Negatif (-)","value"=>"Negatif","default"=> $the_row ["extra_systole"]=="Negatif"?1:0),
		);
        $this->uitable->addModal("extra_systole", "select", "Extra Systole", $extra_systole,"y",NULL,false,NULL,false);
        $gallop=array(
            array("name"=>"","value"=>"","default"=> $the_row ["gallop"]==""?1:0),
            array("name"=>"Positif (+)","value"=>"Positif","default"=> $the_row ["gallop"]=="Positif"?1:0),
            array("name"=>"Negatif (-)","value"=>"Negatif","default"=> $the_row ["gallop"]=="Negatif"?1:0),
		);
        $this->uitable->addModal("gallop", "select", "Gallop", $gallop,"y",NULL,false,NULL,false);
        $this->uitable->addModal("", "label", "Suara Jantung Tambahan", "");
        $this->uitable->addModal("", "label", "Murmur", "");
        $murmur_fase=array(
            array("name"=>"","value"=>"","default"=> $the_row ["murmur_fase"]==""?1:0),
            array("name"=>"Sistolik","value"=>"Sistolik","default"=> $the_row ["murmur_fase"]=="Sistolik"?1:0),
            array("name"=>"Diastolik","value"=>"Diastolik","default"=> $the_row ["murmur_fase"]=="Diastolik"?1:0),
            array("name"=>"Lain-Lain","value"=>"Lain-Lain","default"=> $the_row ["murmur_fase"]=="Lain-Lain"?1:0),
		);
        $this->uitable->addModal("murmur_fase", "select", "Fase", $murmur_fase,"y",NULL,false,NULL,false);
        $this->uitable->addModal ( "murmur_fase_ket", "text", "Keterangan", $the_row ['murmur_fase_ket'] ,"y",null,false,null,false);
        $murmur_lokasi=array(
            array("name"=>"","value"=>"","default"=> $the_row ["murmur_lokasi"]==""?1:0),
            array("name"=>"Apex","value"=>"Apex","default"=> $the_row ["murmur_lokasi"]=="Apex"?1:0),
            array("name"=>"ICS II Kiri PASL Kiri","value"=>"ICS II Kiri PASL Kiri","default"=> $the_row ["murmur_lokasi"]=="ICS II Kiri PASL Kiri"?1:0),
            array("name"=>"ICS II Kanan PSL Kanan","value"=>"ICS II Kanan PSL Kanan","default"=> $the_row ["murmur_lokasi"]=="ICS II Kanan PSL Kanan"?1:0),
            array("name"=>"ICS IV PSL Kiri","value"=>"ICS IV PSL Kiri","default"=> $the_row ["murmur_lokasi"]=="ICS IV PSL Kiri"?1:0),
            array("name"=>"Lain-Lain","value"=>"Lain-Lain","default"=> $the_row ["murmur_lokasi"]=="Lain-Lain"?1:0),
		);
        $this->uitable->addModal("murmur_lokasi", "select", "Lokasi", $murmur_fase,"y",NULL,false,NULL,false);
        $this->uitable->addModal ( "murmur_lokasi_ket", "text", "Keterangan", $the_row ['murmur_lokasi_ket'] ,"y",null,false,null,false);
        $murmur_kualitas=array(
            array("name"=>"","value"=>"","default"=> $the_row ["murmur_kualitas"]==""?1:0),
            array("name"=>"Rumbling","value"=>"Rumbling","default"=> $the_row ["murmur_kualitas"]=="Rumbling"?1:0),
            array("name"=>"Blowing","value"=>"Blowing","default"=> $the_row ["murmur_kualitas"]=="Blowing"?1:0),
            array("name"=>"Ejection","value"=>"Ejection","default"=> $the_row ["murmur_kualitas"]=="Ejection"?1:0),
            array("name"=>"Lain-Lain","value"=>"Lain-Lain","default"=> $the_row ["murmur_kualitas"]=="Lain-Lain"?1:0),
		);
        $this->uitable->addModal("murmur_kualitas", "select", "Kualitas", $murmur_kualitas,"y",NULL,false,NULL,false);
        $this->uitable->addModal ( "murmur_kualitas_ket", "text", "Keterangan", $the_row ['murmur_kualitas_ket'] ,"y",null,false,null,false);
        $murmur_grade=array(
            array("name"=>"","value"=>"","default"=> $the_row ["murmur_grade"]==""?1:0),
            array("name"=>"I","value"=>"I","default"=> $the_row ["murmur_grade"]=="I"?1:0),
            array("name"=>"II","value"=>"II","default"=> $the_row ["murmur_grade"]=="II"?1:0),
            array("name"=>"III","value"=>"III","default"=> $the_row ["murmur_grade"]=="III"?1:0),
            array("name"=>"IV","value"=>"IV","default"=> $the_row ["murmur_grade"]=="IV"?1:0),
            array("name"=>"IV","value"=>"IV","default"=> $the_row ["murmur_grade"]=="IV"?1:0),
            array("name"=>"IV","value"=>"IV","default"=> $the_row ["murmur_grade"]=="IV"?1:0),
		);
        $this->uitable->addModal("murmur_grade", "select", "Grade", $murmur_grade,"y",NULL,false,NULL,false);
        $murmur_penjalaran=array(
            array("name"=>"","value"=>"","default"=> $the_row ["murmur_penjalaran"]==""?1:0),
            array("name"=>"Axilla","value"=>"Axilla","default"=> $the_row ["murmur_penjalaran"]=="Axilla"?1:0),
            array("name"=>"Punggung","value"=>"Punggung","default"=> $the_row ["murmur_penjalaran"]=="Punggung"?1:0),
            array("name"=>"Lain-Lain","value"=>"Lain-Lain","default"=> $the_row ["murmur_penjalaran"]=="Lain-Lain"?1:0),
		);
        $this->uitable->addModal("murmur_penjalaran", "select", "Penjalaran", $murmur_penjalaran,"y",NULL,false,NULL,false);
        $this->uitable->addModal ( "murmur_penjalaran_ket", "text", "Keterangan", $the_row ['murmur_penjalaran_ket'] ,"y",null,false,null,false);
        $murmur_opening_snap=array(
            array("name"=>"","value"=>"","default"=> $the_row ["murmur_opening_snap"]==""?1:0),
            array("name"=>"Positif (+)","value"=>"Positif","default"=> $the_row ["murmur_opening_snap"]=="Positif"?1:0),
            array("name"=>"Negatif (-)","value"=>"Negatif","default"=> $the_row ["murmur_opening_snap"]=="Negatif"?1:0),
		);
        $this->uitable->addModal("murmur_opening_snap", "select", "Opening Snap", $murmur_opening_snap,"y",NULL,false,NULL,false);
        $murmur_friction_rub=array(
            array("name"=>"","value"=>"","default"=> $the_row ["murmur_friction_rub"]==""?1:0),
            array("name"=>"Positif (+)","value"=>"Positif","default"=> $the_row ["murmur_friction_rub"]=="Positif"?1:0),
            array("name"=>"Negatif (-)","value"=>"Negatif","default"=> $the_row ["murmur_friction_rub"]=="Negatif"?1:0),
		);
        $this->uitable->addModal("murmur_friction_rub", "select", "Friction Rub", $murmur_friction_rub,"y",NULL,false,NULL,false);
        $paru=array(
            array("name"=>"","value"=>"","default"=> $the_row ["paru"]==""?1:0),
            array("name"=>"Vesikuler","value"=>"Vesikuler","default"=> $the_row ["paru"]=="Vesikuler"?1:0),
            array("name"=>"Ronki","value"=>"Ronki","default"=> $the_row ["paru"]=="Ronki"?1:0),
            array("name"=>"Wheezing","value"=>"Wheezing","default"=> $the_row ["paru"]=="Wheezing"?1:0),
            array("name"=>"Lain-Lain","value"=>"Lain-Lain","default"=> $the_row ["paru"]=="Lain-Lain"?1:0),
		);
        $this->uitable->addModal("paru", "select", "<strong>Paru</strong>", $paru,"y",NULL,false,NULL,false);
        $this->uitable->addModal ( "paru_ket", "text", "Keterangan", $the_row ['paru_ket'] ,"y",null,false,null,false);
        $this->uitable->addModal("", "label", "<strong>HASIL PEMERIKSAAN PENUNJANG</strong>", "");
        $this->uitable->addModal ( "ecg", "text", "ECG", $the_row ['ecg'] ,"y",null,false,null,false);
        $this->uitable->addModal ( "thorax_foto", "text", "Thorax Foto", $the_row ['thorax_foto'] ,"y",null,false,null,false);
        $this->uitable->addModal ( "echocardiografi", "text", "ECHOCARDIOGRAFI", $the_row ['echocardiografi'] ,"y",null,false,null,false);
        $this->uitable->addModal ( "diagnosis", "textarea", "DIAGNOSIS", $the_row ['diagnosis'] ,"y",null,false,null,false);
        $this->uitable->addModal ( "terapi", "textarea", "TERAPI/TINDAKAN", $the_row ['terapi'] ,"y",null,false,null,false);
        $this->uitable->addModal ( "rencana_kerja", "textarea", "RENCANA KERJA", $the_row ['rencana_kerja'] ,"y",null,false,null,false);
        $this->uitable->addModal("", "label", "<strong>DISPOSISI</strong>", "");
        $this->uitable->addModal ( "waktu_pulang", "datetime", "Waktu Pulang", $the_row ['waktu_pulang'] ,"y",null,false,null,false);
        $this->uitable->addModal ( "waktu_kontrol_klinik", "datetime", "Tanggal Kontrol Klinik", $the_row ['waktu_kontrol_klinik'] ,"y",null,false,null,false);
        $dirawat=array(
				array("name"=>"","value"=>"","default"=> $the_row ["dirawat_diruang"]==""?1:0),
				array("name"=>"Intensif","value"=>"Intensif","default"=> $the_row ["dirawat_diruang"]=="Intensif"?1:0),
				array("name"=>"Ruang Lain","value"=>"Ruang Lain","default"=> $the_row ["dirawat_diruang"]=="Ruang Lain"?1:0),
				array("name"=>"Tidak Dirawat Diruang","value"=>"Tidak Dirawat Diruang","default"=> $the_row ["dirawat_diruang"]=="Tidak Dirawat Diruang"?1:0)
		);
        $this->uitable->addModal("dirawat_diruang", "select", "Dirawat Diruang", $dirawat,"y",NULL,false,NULL,false);
        
        $modal = $this->uitable->getModal ();
		$modal->setTitle ( "Assessment Jantung" );
		if ($this->page == "medical_record") {
			echo $this->uitable->getHtml ();
		} else {
			echo "<div class='hide'>" . $this->uitable->getHtml () . "</div>";
			$modal->setAlwaysShow ( true );
		}
		echo $modal->joinFooterAndForm ()->getHtml ();
        
        $hidden_noreg=new Hidden("apj_noreg_pasien","",$this->noreg_pasien);
        $hidden_nama=new Hidden("apj_nama_pasien","",$this->nama_pasien);
        $hidden_nrm=new Hidden("apj_nrm_pasien","",$this->nrm_pasien);
        $hidden_polislug=new Hidden("apj_polislug","",$this->polislug);
        $hidden_the_page=new Hidden("apj_the_page","",$this->page);
        $hidden_the_protoslug=new Hidden("apj_the_protoslug","",$this->protoslug);
        $hidden_the_protoname=new Hidden("apj_the_protoname","",$this->protoname);
        $hidden_the_protoimpl=new Hidden("apj_the_protoimplement","",$this->protoimplement);
        $hidden_antrian=new Hidden("apj_id_antrian","",$this->id_antrian);
        $hidden_action=new Hidden("apj_action","",$this->action);
        
        echo $hidden_noreg->getHtml();
        echo $hidden_nama->getHtml();
        echo $hidden_nrm->getHtml();
        echo $hidden_polislug->getHtml();
        echo $hidden_the_page->getHtml();
        echo $hidden_the_protoslug->getHtml();
        echo $hidden_the_protoname->getHtml();
        echo $hidden_the_protoimpl->getHtml();
        echo $hidden_antrian->getHtml();
        echo $hidden_action->getHtml();
        
        echo addJS("medical_record/resource/js/asesmen_jantung.js",false);
        echo addCSS ( "framework/bootstrap/css/datepicker.css" );
        echo addJS ( "framework/bootstrap/js/bootstrap-datetimepicker.js" );
        echo addCSS ( "framework/bootstrap/css/bootstrap-datetimepicker.css" );
    }
    
    public function cssPreLoad() {
		?>
		<style type="text/css">
				#<?php echo $this->action;?>_add_form_form>div {clear: both; width: 100%;}
				#<?php echo $this->action; ?>_add_form_form>div label { width: 250px; }
		</style>
		<?php
	}
    
    public function superCommand($super_command){
        $super = new SuperCommand ();
        
        /* PASIEN */
		$phead=array ('Nama','NRM',"No Reg" );
		$ptable = new Table ( $phead, "", NULL, true );
		$ptable->setName ( "apj_pasien" );
		$ptable->setModel ( Table::$SELECT );
		$padapter = new SimpleAdapter ();
		$padapter->add ( "Nama", "nama_pasien" );
		$padapter->add ( "NRM", "nrm", "digit8" );
		$padapter->add ( "No Reg", "id" );
		$presponder = new ServiceResponder ( $this->db, $ptable, $padapter, "get_registered" );		
		$super->addResponder ( "apj_pasien", $presponder );
        
        /* DOKTER */
		$eadapt = new SimpleAdapter ();
		$eadapt->add ( "Jabatan", "nama_jabatan" );
		$eadapt->add ( "Nama", "nama" );
		$eadapt->add ( "NIP", "nip" );
		$head_karyawan=array ('Nama','Jabatan',"NIP");
		$dktable = new Table ( $head_karyawan, "", NULL, true );
		$dktable->setName ( "dokter_apj" );
		$dktable->setModel ( Table::$SELECT );
		$employee = new EmployeeResponder ( $this->db, $dktable, $eadapt, "dokter" );
		$super->addResponder ( "dokter_apj", $employee );
        
        $init = $super->initialize ();
		if ($init != null) {
			echo $init;
			return;
		}
    }
}

?>