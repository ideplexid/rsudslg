<?php
require_once 'smis-framework/smis/template/ModulTemplate.php';
require_once ("smis-base/smis-include-service-consumer.php");
require_once "medical_record/class/service/RuanganService.php";
class LaporanImunisasiTemplate extends ModulTemplate {
	protected $db;
	protected $polislug;
	protected $page;
	protected $protoslug;
	protected $protoname;
	protected $protoimplement;
	protected $action;
	protected $dbtable;
	protected $uitable;
	protected $noreg_pasien;
	protected $nama_pasien;
	protected $nrm_pasien;
    protected $jk;
    protected $tgl_lahir;
    protected $alamat;
    protected $nama_ibu;
    protected $urji;
    protected $carabayar;
    
	public function __construct($db, $polislug = "all", $noreg = "", $nrm = "", $nama = "",$jk=1,$ibu="",$alamat="",$tgl_lahir="",$urji=0,$carabayar="", $page = "medical_record", $action = "imunisasi", $protoslug = "", $protoname = "", $protoimplement = "") {
		$this->db = $db;
		$this->noreg_pasien = $noreg;
		$this->nama_pasien = $nama;
		$this->nrm_pasien = $nrm;
		$this->polislug = $polislug;
		$this->dbtable = new DBTable ( $this->db, "smis_mr_imunisasi" );
		$this->page = $page;
		$this->protoslug = $protoslug;
		$this->protoimplement = $protoimplement;
		$this->protoname = $protoname;
		$this->action = $action;
        $this->jk=$jk;
        $this->nama_ibu=$ibu;
        $this->alamat=$alamat;
        $this->tgl_lahir=$tgl_lahir;
        $this->urji=$urji;
        $this->carabayar=$carabayar;
        
		if ($polislug != "all")
			$this->dbtable->addCustomKriteria ( "ruangan", "='" . $polislug . "'" );
		if ($noreg != "")
			$this->dbtable->addCustomKriteria ( "noreg_pasien", "='" . $noreg . "'" );
		$this->uitable = new Table ( array (
				'NRM',
				"Pasien",
				"No Register" 
		), " Indikator " . ($this->polislug == "all" ? "" : ucfirst ( $this->protoname )), NULL, true );
		$this->uitable->setName ( $action );
		if ($this->page == "medical_record" && $this->action == "lap_imunisasi") {
			$this->uitable->setAddButtonEnable ( false );
			$this->uitable->setDelButtonEnable ( false );
		}
	}
    
	public function command($command) {
        require_once "smis-base/smis-include-duplicate.php";
		$adapter = new SimpleAdapter ();
		$adapter->add ( "Pasien", "nama_pasien" );
		$adapter->add ( "NRM", "nrm_pasien", "digit8" );
		$adapter->add ( "No Register", "noreg_pasien", "digit8" );
		$dbres = new DuplicateResponder ( $this->dbtable, $this->uitable, $adapter );
		$dbres->setDuplicate(false,"");
        $dbres->setAutonomous(getSettings($this->db,"smis_autonomous_id",""));
        $data = $dbres->command ( $_POST ['command'] );
		echo json_encode ( $data );
		return;
	}
    
	public function phpPreLoad() {
		
		if ($this->noreg_pasien != "") {
			$this->uitable->addModal ( "nama_pasien", "hidden", "", $this->nama_pasien, "n", null, true );
			$this->uitable->addModal ( "nrm_pasien", "hidden", "", $this->nrm_pasien, "n", null, true );
			$this->uitable->addModal ( "noreg_pasien", "hidden", "", $this->noreg_pasien, "n", null, true );
		} else {
			$this->uitable->addModal ( "nama_pasien", "chooser-" . $this->action . "-lim_pasien", "Pasien", $this->nama_pasien, "n", null, true );
			$this->uitable->addModal ( "nrm_pasien", "text", "NRM", $this->nrm_pasien, "n", null, true );
			$this->uitable->addModal ( "noreg_pasien", "text", "No Reg", $this->noreg_pasien, "n", null, true );
		}
        
		if ($this->polislug == "all"){
            $service = new RuanganService ( $this->db );
            $service->execute ();
            $ruangan = $service->getContent ();
            $this->uitable->addModal ( "ruangan", "select", "Asal Ruangan", $ruangan );
        }else{
            $this->uitable->addModal ( "ruangan", "hidden", "", $this->polislug, "n", null, true );
        }
			
		$the_row = array ();
		$the_row ['tanggal'] = date ( "Y-m-d H:i:s" );
        $the_row ['jk'] = $this->jk;
		$the_row ['ruangan'] = $this->polislug;
		$the_row ['tgl_lahir'] = $this->tgl_lahir;
		$the_row ['nama_ibu'] = $this->nama_ibu;
		$the_row ['urji'] = $this->urji;
		$the_row ['alamat'] = $this->alamat;
        $the_row ['carabayar'] = $this->carabayar;
		
        $the_row ['hbo'] = "0";
        $the_row ['bcg'] = "0";
        $the_row ['polio1'] = "0";
        $the_row ['polio2'] = "0";
        $the_row ['polio3'] = "0";
        $the_row ['polio4'] = "0";
        $the_row ['dpthbhib1'] = "0";
        $the_row ['dpthbhib2'] = "0";
        $the_row ['dpthbhib3'] = "0";
        $the_row ['campak'] = "0";
		
		$exist ['noreg_pasien'] = $this->noreg_pasien;
		$exist ['nama_pasien'] = $this->nama_pasien;
		$exist ['nrm_pasien'] = $this->nrm_pasien;
		$exist ['ruangan'] = $this->polislug;
		if ($this->dbtable->is_exist ( $exist )) {
			$row = $this->dbtable->select ( $exist );
			$the_row ['tanggal'] = date ( "Y-m-d H:i:s" );
            $the_row ['hbo'] = $row->hbo;
            $the_row ['bcg'] = $row->bcg;
            $the_row ['polio1'] = $row->polio1;
            $the_row ['polio2'] = $row->polio2;
            $the_row ['polio3'] = $row->polio3;
            $the_row ['polio4'] = $row->polio4;
            $the_row ['dpthbhib1'] = $row->dpthbhib1;
            $the_row ['dpthbhib2'] = $row->dpthbhib2;
            $the_row ['dpthbhib3'] = $row->dpthbhib3;
            $the_row ['campak'] = $row->campak;            
			$the_row ["id"] = $row->id;
		} else if(getSettings($this->db,"smis-rs-autosave-lap-imunisasi","1")=="1"){
			$exist ['noreg_pasien'] = $this->noreg_pasien;
			$exist ['nrm_pasien'] = $this->nrm_pasien;
			$exist ['nama_pasien'] = $this->nama_pasien;
			$exist ['tanggal'] = date ( "Y-m-d H:i:s" );
			$this->dbtable->insert ( $exist );
			$the_row ["id"] = $this->dbtable->get_inserted_id ();
            
            $update['origin_id']=$the_row ["id"];
            $update['autonomous']="[".getSettings($this->db,"smis_autonomous_id","")."]";
            $this->dbtable->update($update,array("id"=>$the_row ["id"]));
		}
				
		$this->uitable->addModal ( "id", "hidden", "", $the_row ['id'] );
		$this->uitable->addModal ( "tanggal", "datetime", "Tanggal", $the_row ['tanggal'] );
		$this->uitable->addModal ( "hbo", "checkbox", "HBO", $the_row ['hbo'] );
        $this->uitable->addModal ( "bcg", "checkbox", "BCG", $the_row ['bcg'] );
        $this->uitable->addModal ( "polio1", "checkbox", "Polio 1", $the_row ['polio1'] );
        $this->uitable->addModal ( "polio2", "checkbox", "Polio 2", $the_row ['polio2'] );
        $this->uitable->addModal ( "polio3", "checkbox", "Polio 3", $the_row ['polio3'] );
        $this->uitable->addModal ( "polio4", "checkbox", "Polio 4", $the_row ['polio4'] );
        $this->uitable->addModal ( "dpthbhib1", "checkbox", "DPT HB Hib 1", $the_row ['dpthbhib1'] );
        $this->uitable->addModal ( "dpthbhib2", "checkbox", "DPT HB Hib 2", $the_row ['dpthbhib2'] );
        $this->uitable->addModal ( "dpthbhib3", "checkbox", "DPT HB Hib 3", $the_row ['dpthbhib3'] );
        $this->uitable->addModal ( "campak", "checkbox", "Campak", $the_row ['campak'] );
			
		$modal = $this->uitable->getModal ();
		$modal->setTitle ( "Laporan Imunisasi" );
		if ($this->page == "medical_record" && $this->action == "lap_imunisasi") {
			echo $this->uitable->getHtml ();
		} else {
			echo "<div class='hide'>" . $this->uitable->getHtml () . "</div>";
			$modal->setAlwaysShow ( true );
		}
		echo $modal->joinFooterAndForm ()->getHtml ();
	}
    
	public function jsLoader() {
		echo addJS ( "framework/bootstrap/js/bootstrap-datetimepicker.js" );
		echo addJS ( "framework/smis/js/table_action.js" );
		
		loadLibrary ( "smis-libs-function-javascript" );
	}
    
	public function cssLoader() {
		echo addCSS ( "framework/bootstrap/css/bootstrap-datetimepicker.css" );
	}
    
	public function cssPreLoad() {
		?>
		<style type="text/css">
		#<?php echo $this->action;?>_add_form_form>div { clear: both; width: 100%;}
		#<?php echo $this->action;?>_add_form_form>div label { width: 250px;}
		</style>
		<?php
	}
	
	/* when it's star build */
	public function jsPreLoad() {
		?>
		<script type="text/javascript">
		var <?php echo $this->action; ?>;
		var lim_pasien;
		var lim_noreg="<?php echo $this->noreg_pasien; ?>";
		var lim_nama_pasien="<?php echo $this->nama_pasien; ?>";
		var lim_nrm_pasien="<?php echo $this->nrm_pasien; ?>";
		var lim_polislug="<?php echo $this->polislug; ?>";
		var lim_the_page="<?php echo $this->page; ?>";
		var lim_the_protoslug="<?php echo $this->protoslug; ?>";
		var lim_the_protoname="<?php echo $this->protoname; ?>";
		var lim_the_protoimplement="<?php echo $this->protoimplement; ?>";
		var lim_prefix="<?php echo $this->action; ?>";

		
		
		$(document).ready(function() {

			$("#"+lim_prefix+"_pelayanan").change(function(){
				checktru();
				
			});
			
			$(".mydatetime").datetimepicker({minuteStep:1});
			lim_pasien=new TableAction("lim_pasien",lim_the_page,"<?php echo $this->action; ?>",new Array());
			lim_pasien.setSuperCommand("lim_pasien");
			lim_pasien.setPrototipe(lim_the_protoname,lim_the_protoslug,lim_the_protoimplement);
			lim_pasien.selected=function(json){
				var nama=json.nama_pasien;
				var nrm=json.nrm;
				var noreg=json.id;		
				$("#<?php echo $this->action; ?>_nama_pasien").val(nama);
				$("#<?php echo $this->action; ?>_nrm_pasien").val(nrm);
				$("#<?php echo $this->action; ?>_noreg_pasien").val(noreg);
			};
			
			var column=new Array(
					"id","tanggal","ruangan",
					"nama_pasien","noreg_pasien","nrm_pasien","carabayar",
					"jk","nama_ibu","ruangan","tgl_lahir","urji","alamat",
                    "hbo","bcg","polio1","polio2","polio3","polio4",
                    "dpthbhib1","dpthbhib2","dpthbhib3","campak"
					);
			<?php echo $this->action; ?>=new TableAction("<?php echo $this->action; ?>",lim_the_page,"<?php echo $this->action; ?>",column);
			<?php echo $this->action; ?>.setPrototipe(lim_the_protoname,lim_the_protoslug,lim_the_protoimplement);
			<?php echo $this->action; ?>.getRegulerData=function(){
				var reg_data={	
						page:this.page,
						action:this.action,
						super_command:this.super_command,
						prototype_name:this.prototype_name,
						prototype_slug:this.prototype_slug,
						prototype_implement:this.prototype_implement,
						polislug:lim_polislug,
						noreg_pasien:lim_noreg,
						nama_pasien:lim_nama_pasien,
						nrm_pasien:lim_nrm_pasien
						};
				return reg_data;
			};
			<?php
		
			if ($this->page == "medical_record" && $this->action == "iklin") {
				echo $this->action;?>.view();
				<?php
			} else {
				echo $this->action . ".clear=function(){return;};";
			}
				?>
		});
		</script>
<?php
	}
    
	public function superCommand($super_command) {
		/* PASIEN */
		$ptable = new Table ( array ('Nama','NRM',"No Reg" ), "", NULL, true );
		$ptable->setName ( "lim_pasien" );
		$ptable->setModel ( Table::$SELECT );
		$padapter = new SimpleAdapter ();
		$padapter->add ( "Nama", "nama_pasien" );
		$padapter->add ( "NRM", "nrm", "digit8" );
		$padapter->add ( "No Reg", "id" );
		$presponder = new ServiceResponder ( $this->db, $ptable, $padapter, "get_registered" );
		$super = new SuperCommand ();
		$super->addResponder ( "lim_pasien", $presponder );
		$init = $super->initialize ();
		if ($init != null) {
			echo $init;
			return;
		}
	}
}

?>