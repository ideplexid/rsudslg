<?php
require_once 'smis-framework/smis/template/ModulTemplate.php';
require_once("smis-base/smis-include-service-consumer.php");
require_once 'smis-libs-hrd/EmployeeResponder.php';

class LaporanDiagnosaRehabMedisTemplate extends ModulTemplate
{
	protected $db;
	protected $mode;
	protected $polislug;
	protected $page;
	protected $protoslug;
	protected $protoname;
	protected $protoimplement;
	protected $action;
	protected $dbtable;
	protected $uitable;

	public function __construct(
		$db,
		$polislug,
		$page,
		$action,
		$protoslug,
		$protoname,
		$protoimplement
	) {
		$this->db = $db;
		$this->polislug = $polislug;
		$this->dbtable = new DBTable($this->db, "smis_mr_diagnosa_rehab_medis");
		$this->page = $page;
		$this->protoslug = $protoslug;
		$this->protoimplement = $protoimplement;
		$this->protoname = $protoname;
		$this->action = $action;
		$columns = array("No.", "Diagnosa", "Kelompok", "Jumlah");
		$this->uitable = new Table(
			$columns,
			"",
			null,
			true
		);
		$this->uitable->setName($action);
		$this->uitable->setAction(false);
		$this->uitable->setFooterVisible(false);
	}
	public function command($command)
	{
		$adapter = new SimpleAdapter(true, "No.");
		$adapter->add("Diagnosa", "diagnosa_rehab_medis");
		$adapter->add("Kelompok", "grup_diagnosa");
		$adapter->add("Jumlah", "jumlah", "number");
		$qview = "
			SELECT *
			FROM (
				SELECT diagnosa_rehab_medis, kode_diagnosa, grup_diagnosa, COUNT(id) jumlah
				FROM smis_mr_diagnosa_rehab_medis
				WHERE prop = '' AND tanggal >= '" . $_POST['tanggal_dari'] . "' AND tanggal <= '" . $_POST['tanggal_sampai'] . "'
				GROUP BY kode_diagnosa
			) v
			ORDER BY jumlah DESC
		";
		$qcount = "
			SELECT COUNT(*)
			FROM (
				" . $qview . "
			) v
		";
		$this->dbtable->setPreferredQuery(true, $qview, $qcount);
		$this->dbtable->setShowAll(true);
		$responder = new DBResponder(
			$this->dbtable,
			$this->uitable,
			$adapter
		);
		$data = $responder->command($_POST['command']);
		echo json_encode($data);
		return;
	}
	public function phpPreLoad()
	{
		$laporan_form = new Form("", "", $this->protoname . " : Laporan Diagnosa Rehab Medik");
		$tanggal_from_text = new Text("ldrm_tanggal_dari", "ldrm_tanggal_dari", date("Y-m-") . "01");
		$tanggal_from_text->setClass("mydate");
		$tanggal_from_text->setAtribute("data-date-format='yyyy-mm-dd'");
		$laporan_form->addElement("Dari", $tanggal_from_text);
		$tanggal_to_text = new Text("ldrm_tanggal_sampai", "ldrm_tanggal_sampai", date("Y-m-d"));
		$tanggal_to_text->setClass("mydate");
		$tanggal_to_text->setAtribute("data-date-format='yyyy-mm-dd'");
		$laporan_form->addElement("Sampai", $tanggal_to_text);
		$show_button = new Button("", "", "Tampilkan");
		$show_button->setClass("btn-primary");
		$show_button->setIcon("icon-white icon-repeat");
		$show_button->setIsButton(Button::$ICONIC);
		$show_button->setAction($this->action . ".view()");
		$download_button = new Button("", "", "Unduh");
		$download_button->setClass("btn-inverse");
		$download_button->setIcon("fa fa-download");
		$download_button->setIsButton(Button::$ICONIC);
		$download_button->setAtribute("id='ldrm_export_button'");
		$btn_group = new ButtonGroup("noprint");
		$btn_group->addButton($show_button);
		$btn_group->addButton($download_button);
		$laporan_form->addElement("", $btn_group);

		echo $laporan_form->getHtml();
		echo $this->uitable->getHtml();
	}
	public function jsLoader()
	{
		echo addJS("framework/bootstrap/js/bootstrap-datepicker.js");
		echo addJS("framework/smis/js/table_action.js");
		loadLibrary("smis-libs-function-javascript");
	}
	public function cssLoader()
	{
		echo addCSS("framework/bootstrap/css/datepicker.css");
	}

	public function jsPreLoad()
	{
?>
		<script type="text/javascript">
			var <?php echo $this->action; ?>;
			var ldrm_polislug = "<?php echo $this->polislug; ?>";
			var ldrm_the_page = "<?php echo $this->page; ?>";
			var ldrm_the_protoslug = "<?php echo $this->protoslug; ?>";
			var ldrm_the_protoname = "<?php echo $this->protoname; ?>";
			var ldrm_the_protoimplement = "<?php echo $this->protoimplement; ?>";
			$(document).ready(function() {
				$(".mydate").datepicker();
				<?php echo $this->action; ?> = new TableAction("<?php echo $this->action; ?>", ldrm_the_page, "<?php echo $this->action; ?>", new Array());
				<?php echo $this->action; ?>.setPrototipe(ldrm_the_protoname, ldrm_the_protoslug, ldrm_the_protoimplement);
				<?php echo $this->action; ?>.getRegulerData = function() {
					var reg_data = {
						page: this.page,
						action: this.action,
						super_command: this.super_command,
						prototype_name: this.prototype_name,
						prototype_slug: this.prototype_slug,
						prototype_implement: this.prototype_implement,
						polislug: ldrm_polislug,
						tanggal_dari: $("#ldrm_tanggal_dari").val(),
						tanggal_sampai: $("#ldrm_tanggal_sampai").val()
					};
					return reg_data;
				};
				<?php echo $this->action; ?>.view = function() {
					if ($("#ldrm_tanggal_dari").val() == "" || $("#ldrm_tanggal_sampai").val() == "")
						return;
					TableAction.prototype.view.call(this);
				};
				<?php echo $this->action; ?>.afterview = function(json) {
					var nor = $("#<?php echo $this->action; ?>_list tr").length;
					if (nor > 0) {
						$("#ldrm_export_button").removeAttr("onclick");
						$("#ldrm_export_button").attr("onclick", "<?php echo $this->action; ?>.excel()");
					} else
						$("#ldrm_export_button").removeAttr("onclick");
				};
				<?php echo $this->action; ?>.view();
			});
		</script>
<?php
	}

	public function superCommand($super_command)
	{
	}
}
?>