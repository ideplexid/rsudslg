<?php
require_once 'smis-framework/smis/template/ModulTemplate.php';
require_once ("smis-base/smis-include-service-consumer.php");
require_once "medical_record/class/service/RuanganService.php";
class LaporanIGDTemplate extends ModulTemplate {
	protected $db;
	protected $polislug;
	protected $page;
	protected $protoslug;
	protected $protoname;
	protected $protoimplement;
	protected $action;
	protected $dbtable;
	protected $uitable;
	protected $noreg_pasien;
	protected $nama_pasien;
	protected $nrm_pasien;
    protected $sebutan;
    protected $jk;
    protected $profile_number;
	protected $ds;
	protected $kr;
	protected $rujukan;
	protected $kelanjutan;
	protected $tanggal;
	protected $carabayar;
	
	public function __construct($db, $polislug = "all", $noreg = "", $nrm = "", $nama = "", $page = "medical_record", $action = "lap_igd", $protoslug = "", $protoname = "", $protoimplement = "",$tanggal=NULL) {
		$this->db = $db;
		$this->tanggal=$tanggal==NULL?date("Y-m-d H:i:s"):$tanggal;
		$this->noreg_pasien = $noreg;
		$this->nama_pasien = $nama;
		$this->nrm_pasien = $nrm;
		$this->polislug = $polislug;
		$this->dbtable = new DBTable ( $this->db, "smis_mr_igd" );
		$this->page = $page;
		$this->protoslug = $protoslug;
		$this->protoimplement = $protoimplement;
		$this->protoname = $protoname;
		$this->action = $action;
		if ($polislug != "all")
			$this->dbtable->addCustomKriteria ( "ruangan", "='" . $polislug . "'" );
		if ($noreg != "")
			$this->dbtable->addCustomKriteria ( "noreg_pasien", "='" . $noreg . "'" );
		
		if ($noreg != "") {
			$data_post = array ("command" => "edit","id" => $noreg );
			$service = new ServiceConsumer($this->db, "get_registered", $data_post);
			$service->execute ();
			$data = $service->getContent ();
			if($data!=NULL){
				$this->ds = $data ['caradatang']=="Rujukan"?"0":"1";
				$this->kelanjutan       = $data ['uri']=="0"?"RJ":"MRS";
				$this->rujukan          = $data ['nama_rujukan'];
                $this->sebutan          = $data ['sebutan'];
                $this->profile_number   = $data ['profile_number'];
                $this->jk               = $data ['kelamin'];
				if($data ['rujukan']=="Dokter"){
					$this->kr="Dokter";
				}else if(in_array($data ['rujukan'],array("Mantri","Bidan"))){
					$this->kr="PR/BD";
				}else if(in_array($data ['rujukan'], array("Puskesmas","RS Lain","Balai Pengobatan Lain"))){
					$this->kr="PKM";
				}else if(in_array($data ['rujukan'], array("Karyawan","Lainya"))){
					$this->kr="Lain";
				}else{
					$this->kr="";
				}
				$this->carabayar=$data['carabayar'];
			}
		}
		
		$array=array ('NRM',"Pasien","No Register" );
		$this->uitable = new Table ( $array, " Indikator " . ($this->polislug == "all" ? "" : ucfirst ( $this->protoname )), NULL, true );
		$this->uitable->setName ( $action );
		
		if ($this->page == "medical_record" && $this->action == "lap_igd") {
			$this->uitable->setAddButtonEnable ( false );
			$this->uitable->setDelButtonEnable ( false );
		}
	}
	public function command($command) {
		$adapter = new SimpleAdapter ();
		$adapter->add ( "Pasien", "nama_pasien" );
		$adapter->add ( "NRM", "nrm_pasien", "digit8" );
		$adapter->add ( "No Register", "noreg_pasien", "digit8" );
		$dbres = new DBResponder ( $this->dbtable, $this->uitable, $adapter );
		$data = $dbres->command ( $_POST ['command'] );
		echo json_encode ( $data );
		return;
	}
	public function phpPreLoad() {
		if ($this->noreg_pasien != "") {
			$this->uitable->addModal ( "nama_pasien", "hidden", "", $this->nama_pasien, "n", null, true );
			$this->uitable->addModal ( "nrm_pasien", "hidden", "", $this->nrm_pasien, "n", null, true );
			$this->uitable->addModal ( "noreg_pasien", "hidden", "", $this->noreg_pasien, "n", null, true );
            $this->uitable->addModal ( "profile_number", "hidden", "", $this->profile_number, "n", null, true );
			$this->uitable->addModal ( "sebutan", "hidden", "", $this->sebutan, "n", null, true );
			$this->uitable->addModal ( "jk", "hidden", "", $this->jk, "n", null, true );
		} else {
			$this->uitable->addModal ( "nama_pasien", "chooser-" . $this->action . "-ligd_pasien", "Pasien", $this->nama_pasien, "n", null, true );
			$this->uitable->addModal ( "nrm_pasien", "text", "NRM", $this->nrm_pasien, "n", null, true );
			$this->uitable->addModal ( "noreg_pasien", "text", "No Reg", $this->noreg_pasien, "n", null, true );
            $this->uitable->addModal ( "profile_number", "hidden", "", "" );
			$this->uitable->addModal ( "sebutan", "hidden", "", "" );
			$this->uitable->addModal ( "jk", "hidden", "", "" );
		}
		
		if ($this->polislug == "all"){
			$service = new RuanganService ( $this->db );
			$service->execute ();
			$ruangan = $service->getContent ();
			$this->uitable->addModal ( "ruangan", "select", "Asal Ruangan", $ruangan );
		}else{
			$this->uitable->addModal ( "ruangan", "hidden", "", $this->polislug, "n", null, true );
		}
		
		$the_row = array ();
		$the_row ['pola_kasus'] = "";
		$the_row ['triage'] = "";
		$the_row ['rujuk'] = $this->rujukan;
		$the_row ['menolak_mrs'] = false;
		$the_row ['ver'] = false;
		$the_row ['sift'] = "";
		$the_row ['kelanjutan'] = $this->kelanjutan;
		$the_row ['alat_bantu_hidup'] = false;
		$the_row ['kiriman'] = $this->kr;
		$the_row ['tanggal'] = $this->tanggal;
		$the_row ['ditangani'] = "";
        $the_row ['dikeluarkan'] = "";
		$the_row ['response'] = 0;
		$the_row ['carabayar'] = $this->carabayar;
		$the_row ['tunanetra'] = 0;
		$the_row ['tunadaksa'] = 0;
		$the_row ['tunawicara'] = 0;
		$the_row ['tunarungu'] = 0;
		$the_row ['tunagrahita'] = 0;
		$the_row ['tunalaras'] = 0;
		
		
		
		$exist ['noreg_pasien'] = $this->noreg_pasien;
		$exist ['nama_pasien'] = $this->nama_pasien;
		$exist ['nrm_pasien'] = $this->nrm_pasien;
		$exist ['ruangan'] = $this->polislug;
		if ($this->dbtable->is_exist ( $exist )) {
			$row = $this->dbtable->select ( $exist );
			$the_row = array ();
			$the_row ['pola_kasus'] = $row->pola_kasus;
			$the_row ['alat_bantu_hidup'] = $row->alat_bantu_hidup=="1";
			$the_row ['triage'] = $row->triage;
			$the_row ['rujuk'] = $row->rujuk;
			$the_row ['menolak_mrs'] = $row->menolak_mrs == "1";
			$the_row ['ver'] = $row->ver == "1";
			$the_row ['sift'] = $row->sift;
			$the_row ['kelanjutan'] = $row->kelanjutan;
			$the_row ['kiriman'] = $row->kiriman;
			$the_row ['tanggal'] = $row->tanggal;
			$the_row ['ditangani'] = $row->ditangani;
            $the_row ['dikeluarkan'] = $row->dikeluarkan;
			$the_row ['response'] = $row->response;
			$the_row ['carabayar'] = $row->carabayar==""?$this->carabayar:$row->carabayar;
			$the_row ['tunanetra'] = $row->tunanetra;
			$the_row ['tunadaksa'] = $row->tunadaksa;
			$the_row ['tunawicara'] = $row->tunawicara;
			$the_row ['tunarungu'] = $row->tunarungu;
			$the_row ['tunagrahita'] = $row->tunagrahita;
			$the_row ['tunalaras'] = $row->tunalaras;
		
			
			$the_row ["id"] = $row->id;
		} else if(getSettings($this->db,"smis-rs-autosave-lap-igd","1")=="1"){
			$exist ['noreg_pasien'] = $this->noreg_pasien;
			$exist ['nrm_pasien'] = $this->nrm_pasien;
			$exist ['nama_pasien'] = $this->nama_pasien;
			$exist ['tanggal'] = $the_row ['tanggal'] ;
			$exist ['ditangani'] = $the_row ['ditangani'] ;
            $exist ['dikeluarkan'] = $the_row ['dikeluarkan'] ;
			$exist ['response'] = $the_row ['response'] ;
			$exist ['pola_kasus'] = $the_row ['pola_kasus'];
			$exist ['triage'] = $the_row ['triage'] ;
			$exist ['rujuk'] = $the_row ['rujuk'] ;
			$exist ['menolak_mrs'] = $the_row ['menolak_mrs'] ;
			$exist ['ver'] = $the_row ['ver'] ;
			$exist ['sift'] = $the_row ['sift'] ;
			$exist ['kelanjutan'] = $the_row ['kelanjutan'] ;
			$exist ['alat_bantu_hidup'] = $the_row ['alat_bantu_hidup'] ;
			$exist ['kiriman'] = $the_row ['kiriman'] ;	
			$exist ['carabayar'] = $the_row ['carabayar'] ;
			
			$this->dbtable->insert ( $exist );
			$the_row ["id"] = $this->dbtable->get_inserted_id ();
		}
		$kelanjutan = new OptionBuilder ();
		$kelanjutan->add ( "", "", $the_row ['kelanjutan'] == "" ? "1" : "0" );
		$kelanjutan->add ( "Rawat Jalan", "RJ", $the_row ['kelanjutan'] == "RJ" ? "1" : "0" );
		$kelanjutan->add ( "Rujuk ke RS Lain", "RJK", $the_row ['kelanjutan'] == "RJK" ? "1" : "0" );
		$kelanjutan->add ( "Masuk Rumah Sakit (Rawat Inap)", "MRS", $the_row ['kelanjutan'] == "MRS" ? "1" : "0" );
		
		$kiriman = new OptionBuilder ();
		$kiriman->add ( "", "", $the_row ['kiriman'] == "" ? "1" : "0" );
		$kiriman->add ( "Dokter", "Dokter", $the_row ['kiriman'] == "Dokter" ? "1" : "0" );
		$kiriman->add ( "PKM/RS", "PKM", $the_row ['kiriman'] == "PKM" ? "1" : "0" );
		$kiriman->add ( "PR/BD", "PR/BD", $the_row ['kiriman'] == "PR/BD" ? "1" : "0" );
		$kiriman->add ( "Lainya", "Lain", $the_row ['kiriman'] == "Lain" ? "1" : "0" );
		
		$pola_kasus = new OptionBuilder ();
		$pola_kasus->add ( "", "", $the_row ['pola_kasus'] == "" ? "1" : "0" );
		//$pola_kasus->add ( "Bedah - Penganiayaan", "Bedah - Penganiayaan", $the_row ['pola_kasus'] == "Bedah - Penganiayaan" ? "1" : "0" );
		//$pola_kasus->add ( "Bedah - Kecelakaan Kerja", "Bedah - Kecelakaan Kerja", $the_row ['pola_kasus'] == "Bedah - Kecelakaan Kerja" ? "1" : "0" );
		//$pola_kasus->add ( "Bedah - KLL", "Bedah - KLL", $the_row ['pola_kasus'] == "Bedah - KLL" ? "1" : "0" );
		$pola_kasus->add ( "Bedah", "Bedah", $the_row ['pola_kasus'] == "Bedah" ? "1" : "0" );
		$pola_kasus->add ( "Non Bedah", "Non Bedah", $the_row ['pola_kasus'] == "Non Bedah" ? "1" : "0" );
		$pola_kasus->add ( "Kebidanan", "Kebidanan", $the_row ['pola_kasus'] == "Kebidanan" ? "1" : "0" );
		$pola_kasus->add ( "Psikiatrik", "Psikiatrik", $the_row ['pola_kasus'] == "Psikiatrik" ? "1" : "0" );
		$pola_kasus->add ( "Anak", "Anak", $the_row ['pola_kasus'] == "Anak" ? "1" : "0" );
		
		$triage = new OptionBuilder ();
		$triage->add ( "", "", $the_row ['triage'] == "" ? "1" : "0" );
		//$triage->add ( "Biru", "Biru", $the_row ['triage'] == "Biru" ? "1" : "0" );
		$triage->add ( "P1 - Pasien Gawat - Merah", "Merah", $the_row ['triage'] == "Merah" ? "1" : "0" );
		$triage->add ( "P2 - Pasien Darurat - Kuning", "Kuning", $the_row ['triage'] == "Kuning" ? "1" : "0" );
		$triage->add ( "P3 - Pasien Tidak Gawat & Tidak Darurat - Hijau", "Hijau", $the_row ['triage'] == "Hijau" ? "1" : "0" );
		$triage->add ( "P0 - Hitam DOA", "Hitam DOA", $the_row ['triage'] == "Hitam DOA" ? "1" : "0" );
		$triage->add ( "P0 - Hitam Mati", "Hitam Mati", $the_row ['triage'] == "Hitam Mati" ? "1" : "0" );
		
		$sift = new OptionBuilder ();
		$sift->add ( "", "", $the_row ['sift'] == "" ? "1" : "0" );
		$sift->add ( "Pagi", "Pagi", $the_row ['sift'] == "Pagi" ? "1" : "0" );
		$sift->add ( "Sore", "Sore", $the_row ['sift'] == "Sore" ? "1" : "0" );
		$sift->add ( "Malam", "Malam", $the_row ['sift'] == "Malam" ? "1" : "0" );
		
		$this->uitable->addModal ( "id", "hidden", "", $the_row ['id'] );
		$this->uitable->addModal ( "tanggal", "datetime", "Tanggal", $the_row ['tanggal'],NULL,true  );
		$this->uitable->addModal ( "ditangani", "datetime", "Tangani", $the_row ['ditangani'] );
        $this->uitable->addModal ( "dikeluarkan", "datetime", "Dikeluarkan", $the_row ['dikeluarkan'] );
		$this->uitable->addModal ( "response", "text", "Response Time (Menit)", $the_row ['response'],"y",NULL,true );
		$this->uitable->addModal ( "sift", "select", "Waktu", $sift->getContent () );
		$this->uitable->addModal ( "pola_kasus", "select", "Pola Kasus", $pola_kasus->getContent () );
		$this->uitable->addModal ( "triage", "select", "Triage", $triage->getContent () );
		$this->uitable->addModal ( "kelanjutan", "select", "Kelanjutan", $kelanjutan->getContent (),"n" );
		$this->uitable->addModal ( "ds", "checkbox", "DS", $this->ds );
		$this->uitable->addModal ( "kiriman", "select", "Perujuk", $kiriman->getContent () );
		$this->uitable->addModal ( "rujuk", "text", "Nama Perujuk", $the_row ['rujuk'] );
		$this->uitable->addModal ( "carabayar", "text", "Jenis Pasien", $the_row ['carabayar'],"y",NULL,true  );
		$this->uitable->addModal ( "menolak_mrs", "checkbox", "Menolak MRS", $the_row ['menolak_mrs']);
		$this->uitable->addModal ( "alat_bantu_hidup", "checkbox", "Menggunakan Alat Bantu Hidup", $the_row ['alat_bantu_hidup'] );
		$this->uitable->addModal ( "tunanetra", "checkbox", "Tunanetra", $the_row ['tunanetra'] );
		$this->uitable->addModal ( "tunadaksa", "checkbox", "Tunadaksa", $the_row ['tunadaksa'] );
		$this->uitable->addModal ( "tunawicara", "checkbox", "Tunawicara", $the_row ['tunawicara'] );
		$this->uitable->addModal ( "tunarungu", "checkbox", "Tunarungu", $the_row ['tunarungu'] );
		$this->uitable->addModal ( "tunagrahita", "checkbox", "Tunagrahita", $the_row ['tunagrahita'] );
		$this->uitable->addModal ( "tunalaras", "checkbox", "Tunalaras", $the_row ['tunalaras'] );		
		$this->uitable->addModal ( "ver", "checkbox", "Ver", $the_row ['ver'] );
		
		$modal = $this->uitable->getModal ();
		$modal->setTitle ( "Laporan IGD" );
		if ($this->page == "medical_record" && $this->action == "lap_igd") {
			echo $this->uitable->getHtml ();
		} else {
			echo "<div class='hide'>" . $this->uitable->getHtml () . "</div>";
			$modal->setAlwaysShow ( true );
		}
		echo $modal->joinFooterAndForm ()->getHtml ();
	}
	public function jsLoader() {
		echo addJS ( "framework/bootstrap/js/bootstrap-datetimepicker.js" );
		echo addJS ( "framework/smis/js/table_action.js" );
		
		loadLibrary ( "smis-libs-function-javascript" );
	}
	public function cssLoader() {
		echo addCSS ( "framework/bootstrap/css/bootstrap-datetimepicker.css" );
	}
	public function cssPreLoad() {
		?>
			<style type="text/css">
			#<?php echo $this->action;?>_add_form_form>div {clear: both; width: 100%; }
			#<?php echo $this->action;?>_add_form_form>div label { 	width: 250px; }
			#<?php echo $this->action;?>_triage>option:nth-child(2){background:#c0392b !important; color:white;}
            #<?php echo $this->action;?>_triage>option:nth-child(3){background:#f39c12 !important;color:white;}
            #<?php echo $this->action;?>_triage>option:nth-child(4){background:#2ecc71 !important;color:white;}
            #<?php echo $this->action;?>_triage>option:nth-child(5){background:#34495e !important;color:white;}
            #<?php echo $this->action;?>_triage>option:nth-child(6){background:#34495e !important;color:white;}
            </style>
		<?php
	}
	
	/* when it's star build */
	public function jsPreLoad() {
		?>
<script type="text/javascript">
		var <?php echo $this->action; ?>;
		var ligd_pasien;
		var ligd_noreg="<?php echo $this->noreg_pasien; ?>";
		var ligd_nama_pasien="<?php echo $this->nama_pasien; ?>";
		var ligd_nrm_pasien="<?php echo $this->nrm_pasien; ?>";
		var ligd_polislug="<?php echo $this->polislug; ?>";
		var ligd_the_page="<?php echo $this->page; ?>";
		var ligd_the_protoslug="<?php echo $this->protoslug; ?>";
		var ligd_the_protoname="<?php echo $this->protoname; ?>";
		var ligd_the_protoimplement="<?php echo $this->protoimplement; ?>";
		$(document).ready(function() {
			$(".mydatetime").datetimepicker({ minuteStep: 1});
			ligd_pasien=new TableAction("ligd_pasien",ligd_the_page,"<?php echo $this->action; ?>",new Array());
			ligd_pasien.setSuperCommand("ligd_pasien");
			ligd_pasien.setPrototipe(ligd_the_protoname,ligd_the_protoslug,ligd_the_protoimplement);
			ligd_pasien.selected=function(json){
				var nama=json.nama_pasien;
				var nrm=json.nrm;
				var noreg=json.id;		
				$("#<?php echo $this->action; ?>_nama_pasien").val(nama);
				$("#<?php echo $this->action; ?>_nrm_pasien").val(nrm);
				$("#<?php echo $this->action; ?>_noreg_pasien").val(noreg);
                $("#<?php echo $this->action; ?>_jk").val(json.kelamin);
				$("#<?php echo $this->action; ?>_profile_number").val(json.profile_number);
				$("#<?php echo $this->action; ?>_sebutan").val(json.sebutan);
			};
			
			var column=new Array(
					"id","tanggal","dikeluarkan", "ditangani","response","ruangan","ds",
					"nama_pasien","noreg_pasien","nrm_pasien", "profile_number", "sebutan", "jk",
					"sift","pola_kasus","triage","kelanjutan","kiriman","rujuk",
					"menolak_mrs","ver","alat_bantu_hidup","tunanetra","tunarungu",
					"tunagrahita","tunalaras","tunawicara","tunadaksa","carabayar"
					);
			<?php echo $this->action; ?>=new TableAction("<?php echo $this->action; ?>",ligd_the_page,"<?php echo $this->action; ?>",column);
			<?php echo $this->action; ?>.setPrototipe(ligd_the_protoname,ligd_the_protoslug,ligd_the_protoimplement);
			<?php echo $this->action; ?>.getRegulerData=function(){
				var reg_data={	
						page:this.page,
						action:this.action,
						super_command:this.super_command,
						prototype_name:this.prototype_name,
						prototype_slug:this.prototype_slug,
						prototype_implement:this.prototype_implement,
						polislug:ligd_polislug,
						noreg_pasien:ligd_noreg,
						nama_pasien:ligd_nama_pasien,
						nrm_pasien:ligd_nrm_pasien
						};
				return reg_data;
			};

			$("#<?php echo $this->action; ?>_ditangani, #<?php echo $this->action; ?>_tanggal").on("change",function(){
				var s_date = $("#<?php echo $this->action; ?>_tanggal").val().replace(/-/g, "/");
				var e_date = $("#<?php echo $this->action; ?>_ditangani").val().replace(/-/g, "/");
				var timeStart = new Date(s_date).getTime();
				var timeEnd = new Date(e_date).getTime();
				var hourDiff = timeEnd - timeStart; //in ms
				var minDiff = Math.floor(hourDiff / 60 / 1000); //in minutes
				$("#<?php echo $this->action; ?>_response").val(minDiff);
			});
			
			<?php
		
		if ($this->page == "medical_record" && $this->action == "iklin") { 
			echo $this->action; ?>.view();<?php
		} else {
			echo $this->action . ".clear=function(){return;};";
		}
		
		
		?>
		});
		</script>
<?php
	}
	public function superCommand($super_command) {
		/* PASIEN */
		$ptable = new Table ( array (
				'Nama',
				'NRM',
				"No Reg" 
		), "", NULL, true );
		$ptable->setName ( "ligd_pasien" );
		$ptable->setModel ( Table::$SELECT );
		$padapter = new SimpleAdapter ();
		$padapter->add ( "Nama", "nama_pasien" );
		$padapter->add ( "NRM", "nrm", "digit8" );
		$padapter->add ( "No Reg", "id" );
		$presponder = new ServiceResponder ( $this->db, $ptable, $padapter, "get_registered" );
		$super = new SuperCommand ();
		$super->addResponder ( "ligd_pasien", $presponder );
		$init = $super->initialize ();
		if ($init != null) {
			echo $init;
			return;
		}
	}
}

?>