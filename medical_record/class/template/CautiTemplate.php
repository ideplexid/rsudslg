<?php
require_once 'smis-framework/smis/template/ModulTemplate.php';
require_once ("smis-base/smis-include-service-consumer.php");
require_once 'smis-libs-hrd/EmployeeResponder.php';

class CautiTemplate extends ModulTemplate {
	protected $db;
	protected $mode;
	protected $polislug;
	protected $page;
	protected $protoslug;
	protected $protoname;
	protected $protoimplement;
	protected $action;
	protected $dbtable;
	protected $uitable;
	protected $noreg_pasien;
	protected $nama_pasien;
	protected $nrm_pasien;
	protected $gol_umur;
	protected $jk;	
	protected $carabayar;
	protected $urji;
	public static $MODE_DAFTAR 	= "pendaftaran";
	public static $MODE_PERIKSA = "pemeriksaan";
	public function __construct($db, $mode, $polislug = "all", $noreg = "", $nrm = "", $nama = "", $page = "medical_record", $action = "diagnosa", $protoslug = "", $protoname = "", $protoimplement = "", $jenis_umur = "TGL LAHIR TDK VALID", $jk = "0", $carabayar = "Umum",$urji="0") {
		$this->db = $db;
		$this->mode = $mode;
		$this->urji=$urji;
		$this->noreg_pasien = $noreg;
		$this->nama_pasien = $nama;
		$this->gol_umur = $jenis_umur;
		$this->nrm_pasien = $nrm;
		$this->polislug = $polislug;
		$this->dbtable = new DBTable ( $this->db, "smis_mr_cauti" );
		$this->page = $page;
		$this->protoslug = $protoslug;
		$this->protoimplement = $protoimplement;
		$this->protoname = $protoname;
		$this->action = $action;
		$this->jk = $jk;
		$this->carabayar = $carabayar;
		if ($noreg != "") $this->dbtable->addCustomKriteria ( "noreg_pasien", "='" . $noreg. "'" );
        if($polislug!="") $this->dbtable->addCustomKriteria ( "ruangan", "='" . $polislug. "'" );
		
		$array=array ("No.",'Tanggal',"Jenis Cath","No. Cath","Gejala ISK","Kegiatan","Keluar" );
		$this->uitable = new Table ( $array , " Cauti RM-36 " . ($this->polislug == "all" ? "" : ucfirst ( $this->protoname )), NULL, true );
		$this->uitable->setName ( $action );
		if ($this->mode == self::$MODE_PERIKSA) {
			$this->uitable->setAddButtonEnable ( false );
			$this->uitable->setDelButtonEnable ( false );
		}
	}
	public function command($command) {
		$adapter = new SimpleAdapter();
		$adapter->setUseNumber(true,"No.","back.");
		$adapter->add ( "Tanggal", "tanggal", "date d M Y" );
		$adapter->add ( "Lokasi", "lokasi" );
		$adapter->add ( "No. Cath", "nocath" );
		$adapter->add ( "Jenis Cath", "jenis_cath" );
		$adapter->add ( "Kegiatan", "kegiatan" );
        $adapter->add ( "Gejala ISK", "isk" );
        $adapter->add ( "Keluar", "keluar" );
		$dbres = new DBResponder( $this->dbtable, $this->uitable, $adapter );
		$data = $dbres->command ( $_POST ['command'] );
		
		echo json_encode ( $data );
		return;
	}
	public function phpPreLoad() {
        
        $kegiatan=new OptionBuilder();
        $kegiatan->addSingle("Catheter terpasang dari ruang lain");
        $kegiatan->addSingle("Pasang Catheter");
        $kegiatan->addSingle("Cek Catheter");
        $kegiatan->addSingle("Ganti Catheter");
        $kegiatan->addSingle("Lepas Catheter");
        
        $nocath=new OptionBuilder();
        $nocath->addSingle("6");
        $nocath->addSingle("8");
        $nocath->addSingle("10");
        $nocath->addSingle("12");
        $nocath->addSingle("14");
        $nocath->addSingle("16");
        $nocath->addSingle("18");
        $nocath->addSingle("20");
        $nocath->addSingle("22");
        
        $urin=new OptionBuilder();
        $urin->addSingle("Urin Bag Dibawah Bladder");
        $urin->addSingle("Urin Bag Menyentuh Lantai");
        
        $isk=new OptionBuilder();
        $isk->addSingle("","1");
        $isk->addSingle("Demam >38 C");
        $isk->addSingle("Nyeri Supra-Publik");
        $isk->addSingle("Urgency");
        $isk->addSingle("Frequency");
        $isk->addSingle("Dysuria");
        $isk->addSingle("Nyeri Costovertebral Angle");
        
        $keluar=new OptionBuilder();
        $keluar->addSingle("","1");
        $keluar->addSingle("Pulang dilepas");
        $keluar->addSingle("Pindah Kamar terpasang");        
        $keluar->addSingle("Pindah Kamar dilepas");        
        $keluar->addSingle("Rujuk terpasang");    
        $keluar->addSingle("Rujuk dilepas");    
        
		$this->uitable->addModal ( "id", "hidden", "", "" );
		$this->uitable->addModal ( "nama_pasien", "hidden", "", $this->nama_pasien, "n", null, true );
		$this->uitable->addModal ( "noreg_pasien", "hidden", "", $this->noreg_pasien, "n", null, true );
		$this->uitable->addModal ( "nrm_pasien", "hidden", "", $this->nrm_pasien, "n", null, true );	
		$this->uitable->addModal ( "ruangan", "hidden", "", $this->polislug, "n", null, true );
		$this->uitable->addModal ( "tanggal", "date", "Tanggal", date ( "Y-m-d" ) );
		$this->uitable->addModal ( "nama_perawat", "chooser-" . $this->action . "-cauti_perawat", "Perawat", "", "y", null, false,null,true );
		$this->uitable->addModal ( "kegiatan", "select", "Kegiatan", $kegiatan->getContent(), "y", null, false ,null,false,"kasus");
		$this->uitable->addModal ( "jenis_cath", "text", "Jenis Cath","", "y", null, false );
		$this->uitable->addModal ( "nocath", "select", "No. Cath", $nocath->getContent(), "y", null, false );
		$this->uitable->addModal ( "aseptik", "checkbox", "Pemasangan Dengan Teknik Aseptik", "0", "y", null, false );
		$this->uitable->addModal ( "kondisi", "select", "Kondisi Urin Bag", $urin->getContent(), "y", null, false );
		$this->uitable->addModal ( "bladder", "checkbox", "Bladder Training dengan klem kateter", "0", "y", null, false );
		$this->uitable->addModal ( "sambungan", "checkbox", "Membuka sambungan antara cath dan selang urin bag", "0", "y", null, false );
		$this->uitable->addModal ( "perineal", "checkbox", "Perineal hygiene dengan air dan sabun", "0", "y", null, false );
		$this->uitable->addModal ( "gelas_ukur", "checkbox", "Gelas ukur terpisah antar pasien", "0", "y", null, false );
		$this->uitable->addModal ( "indikasi", "checkbox", "masih ada indikasi pemakaian kateter urin", "0", "y", null, false );
		$this->uitable->addModal ( "isk", "select", "Gejala ISK", $isk->getContent(), "y", null, false );
		$this->uitable->addModal ( "kuman", "checkbox", "Kuman Biakan Urine > 10s/ml", "0", "y", null, false );
		$this->uitable->addModal ( "pyuria", "checkbox", "Pyuria > 10 Leukosit Urine", "0", "y", null, false );
		$this->uitable->addModal ( "keluar", "select", "Keluar", $keluar->getContent(), "y", null, false );
		
		$modal = $this->uitable->getModal ();
		$modal->setComponentSize(Modal::$MEDIUM);
		$modal->setTitle ( "cauti" );
		
		$poliname=new Hidden("cauti_poliname","",$this->protoname);
		$polislug=new Hidden("cauti_polislug","",$this->polislug);
		$protoslug=new Hidden("cauti_protoslug","",$this->protoslug);
		$implement=new Hidden("cauti_implement","",$this->protoimplement);
		$page=new Hidden("cauti_page","",$this->page);		
		$noreg=new Hidden("cauti_noreg","",$this->noreg_pasien);
		$nama=new Hidden("cauti_nama","",$this->nama_pasien);
		$nrm=new Hidden("cauti_nrm","",$this->nrm_pasien);		
	
		echo $poliname->getHtml();
		echo $polislug->getHtml();	
		echo $protoslug->getHtml();	
		echo $implement->getHtml();
		echo $page->getHtml();
		echo $noreg->getHtml();		
		echo $nrm->getHtml();
		echo $nama->getHtml();
		
		echo $this->uitable->getHtml ();
		echo $modal->getHtml ();
		loadLibrary ( "smis-libs-function-javascript" );
		echo addJS ( "framework/bootstrap/js/bootstrap-datepicker.js" );
		echo addJS ( "framework/smis/js/table_action.js" );
		echo addCSS ( "framework/bootstrap/css/datepicker.css" );
		echo addJS ( "medical_record/resource/js/cauti.js",false );
		
	}
	
	public function superCommand($super_command) {
		$array=array ('Nama','Jabatan',"NIP" );
		$dktable = new Table ($array);
		$dktable->setName ( "cauti_perawat" );
		$dktable->setModel ( Table::$SELECT );
		$dkadapter = new SimpleAdapter ();
		$dkadapter->add ( "Jabatan", "nama_jabatan" );
		$dkadapter->add ( "Nama", "nama" );
		$dkadapter->add ( "NIP", "nip" );
		$dkresponder_perawat = new EmployeeResponder($this->db, $dktable, $dkadapter, "perawat") ;
		
		$super = new SuperCommand ();
		$super->addResponder ( "cauti_perawat", $dkresponder_perawat );
		$init = $super->initialize ();
		if ($init != null) {
			echo $init;
			return;
		}
	}
}

?>