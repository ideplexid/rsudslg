<?php
require_once 'smis-framework/smis/template/ModulTemplate.php';
require_once ("smis-base/smis-include-service-consumer.php");
require_once 'medical_record/class/table/AsuhanObatTable.php';
require_once 'medical_record/class/adapter/DiagnosaAdapter.php';
require_once 'smis-libs-hrd/EmployeeResponder.php';

class AsuhanObatTemplate extends ModulTemplate {
	protected $db;
	protected $mode;
	protected $polislug;
	protected $page;
	protected $protoslug;
	protected $protoname;
	protected $protoimplement;
	protected $action;
	protected $dbtable;
	protected $uitable;
	protected $noreg_pasien;
	protected $nama_pasien;
	protected $nrm_pasien;
	protected $gol_umur;
	protected $jk;	
	protected $carabayar;
	protected $urji;
	public static $MODE_DAFTAR 	= "pendaftaran";
	public static $MODE_PERIKSA = "pemeriksaan";
	public function __construct($db, $mode, $polislug = "all", $noreg = "", $nrm = "", $nama = "", $page = "medical_record", $action = "diagnosa", $protoslug = "", $protoname = "", $protoimplement = "", $jenis_umur = "TGL LAHIR TDK VALID", $jk = "0", $carabayar = "Umum",$urji="0") {
		$this->db = $db;
		$this->mode = $mode;
		$this->urji=$urji;
		$this->noreg_pasien = $noreg;
		$this->nama_pasien = $nama;
		$this->gol_umur = $jenis_umur;
		$this->nrm_pasien = $nrm;
		$this->polislug = $polislug;
		$this->dbtable = new DBTable ( $this->db, "smis_mr_asuhan_obat" );
		$this->page = $page;
		$this->protoslug = $protoslug;
		$this->protoimplement = $protoimplement;
		$this->protoname = $protoname;
		$this->action = $action;
		$this->jk = $jk;
		$this->carabayar = $carabayar;
		if ($noreg != "")
			$this->dbtable->addCustomKriteria ( "noreg_pasien", "='" . $noreg. "'" );
		
		$array=array ("No.",'Waktu',"Ruangan","Perawat","Dokter","Obat","Jumlah","Metode","Keterangan" );
		$this->uitable = new AsuhanObatTable ($polislug,$noreg, $array , " Asuhan Obat " . ($this->polislug == "all" ? "" : ucfirst ( $this->protoname )), NULL, true );
		$this->uitable->setName ( $action );
		if ($this->mode == self::$MODE_PERIKSA) {
			$this->uitable->setAddButtonEnable ( false );
			$this->uitable->setDelButtonEnable ( false );
		}
	}
	public function command($command) {
		$adapter = new SimpleAdapter();
		$adapter->setUseNumber(true,"No.","back.");
		$adapter->add ( "Waktu", "waktu", "date d M Y H:i" );
		$adapter->add ( "Dokter", "nama_dokter" );
		$adapter->add ( "Perawat", "nama_perawat" );
		$adapter->add ( "Obat", "nama_obat" );
		$adapter->add ( "Jumlah", "jumlah" );
		$adapter->add ( "Metode", "metode" );
		$adapter->add ( "Keterangan", "keterangan" );
		$adapter->add ( "Ruangan", "ruangan" );
		$adapter->add ( "Noreg", "noreg_pasien" );
		$dbres = new DBResponder( $this->dbtable, $this->uitable, $adapter );
		$data = $dbres->command ( $_POST ['command'] );
		
		echo json_encode ( $data );
		return;
	}
	public function phpPreLoad() {
		loadClass ( "ServiceProviderList" );
		$service = new ServiceProviderList ( $this->db, "push_antrian" );
		$service->execute ();
		$ruangan = $service->getContent ();
		
		loadLibrary ( "smis-libs-function-medical" );
		$gol_umur = get_medical_gol_umur ( $this->gol_umur );
		$metode=medical_metode_obat();
		
		$this->uitable->addModal ( "id", "hidden", "", "" );
		$this->uitable->addModal ( "jk", "hidden", "", $this->jk );
		$this->uitable->addModal ( "carabayar", "hidden", "", $this->carabayar );
		$this->uitable->addModal ( "urji", "hidden", "", $this->urji);
		$this->uitable->addModal ( "waktu", "datetime", "Waktu", date ( "Y-m-d H:i:s" ) );
		$this->uitable->addModal ( "nama_dokter", "chooser-" . $this->action . "-aso_dokter", "Dokter", "", "y", null, false,null,true );
		$this->uitable->addModal ( "id_dokter", "hidden", "", "" );
		$this->uitable->addModal ( "nama_perawat", "chooser-" . $this->action . "-aso_perawat", "Perawat", "", "y", null, false,null,true );
		$this->uitable->addModal ( "id_perawat", "hidden", "", "" );
		$this->uitable->addModal ( "nama_obat", "chooser-" . $this->action . "-aso_obat", "Obat", "", "y", null, false,null,true );
		$this->uitable->addModal ( "id_obat", "hidden", "", "" );
		$this->uitable->addModal ( "jumlah", "text", "Jumlah", "", "y", null, false );
		$this->uitable->addModal ( "metode", "select", "Metode", $metode, "y", null, false ,null,false,"kasus");
		$this->uitable->addModal ( "keterangan", "textarea", "Keterangan", "", "y", null, false ,null,false,"kasus");
		$this->uitable->addModal ( "nama_pasien", "text", "Pasien", $this->nama_pasien, "n", null, true );
		$this->uitable->addModal ( "noreg_pasien", "text", "No Reg", $this->noreg_pasien, "n", null, true );
		$this->uitable->addModal ( "nrm_pasien", "text", "NRM", $this->nrm_pasien, "n", null, true );	
		$this->uitable->addModal ( "gol_umur", "select", "Golongan", $gol_umur, "y", null, false );
		$this->uitable->addModal ( "ruangan", "text", "Asal Ruangan", $this->polislug, "n", null, true );
		
		$modal = $this->uitable->getModal ();
		$modal->setComponentSize(Modal::$MEDIUM);
		$modal->setTitle ( "Asuhan Obat" );

		
		$carabayar=new Hidden("aso_carabayar","",$this->carabayar);
		$poliname=new Hidden("aso_poliname","",$this->protoname);
		$polislug=new Hidden("aso_polislug","",$this->polislug);
		$protoslug=new Hidden("aso_protoslug","",$this->protoslug);
		$implement=new Hidden("aso_implement","",$this->protoimplement);
		$page=new Hidden("aso_page","",$this->page);		
		$noreg=new Hidden("aso_noreg","",$this->noreg_pasien);
		$nama=new Hidden("aso_nama","",$this->nama_pasien);
		$nrm=new Hidden("aso_nrm","",$this->nrm_pasien);		
		
		echo $carabayar->getHtml();
		echo $poliname->getHtml();
		echo $polislug->getHtml();	
		echo $protoslug->getHtml();	
		echo $implement->getHtml();
		echo $page->getHtml();
		echo $noreg->getHtml();		
		echo $nrm->getHtml();
		echo $nama->getHtml();
		
		echo $this->uitable->getHtml ();
		echo $modal->getHtml ();
		loadLibrary ( "smis-libs-function-javascript" );
		echo addJS ( "framework/bootstrap/js/bootstrap-datetimepicker.js" );
		echo addJS ( "framework/smis/js/table_action.js" );
		echo addCSS ( "framework/bootstrap/css/bootstrap-datetimepicker.css" );
		echo addJS ( "medical_record/resource/js/asuhan_obat.js",false );
		
	}
	
	public function superCommand($super_command) {
		$array=array ('Nama','Jabatan',"NIP" );
		$dktable = new Table ($array);
		$dktable->setName ( "aso_dokter" );
		$dktable->setModel ( Table::$SELECT );
		$dkadapter = new SimpleAdapter ();
		$dkadapter->add ( "Jabatan", "nama_jabatan" );
		$dkadapter->add ( "Nama", "nama" );
		$dkadapter->add ( "NIP", "nip" );
		$dkresponder = new EmployeeResponder($this->db, $dktable, $dkadapter, "dokter") ;
		
		$array=array ('Nama','Jabatan',"NIP" );
		$dktable = new Table ($array);
		$dktable->setName ( "aso_perawat" );
		$dktable->setModel ( Table::$SELECT );
		$dkadapter = new SimpleAdapter ();
		$dkadapter->add ( "Jabatan", "nama_jabatan" );
		$dkadapter->add ( "Nama", "nama" );
		$dkadapter->add ( "NIP", "nip" );
		$dkresponder_perawat = new EmployeeResponder($this->db, $dktable, $dkadapter, "perawat") ;
		
		/* PASIEN */
		$aname=array ('Nama','NRM',"No Reg" );
		$ptable = new Table ( $aname);
		$ptable->setName ( "aso_pasien" );
		$ptable->setModel ( Table::$SELECT );
		$padapter = new SimpleAdapter ();
		$padapter->add ( "Nama", "nama_pasien" );
		$padapter->add ( "NRM", "nrm", "digit8" );
		$padapter->add ( "No Reg", "id" );
		$presponder = new ServiceResponder ( $this->db, $ptable, $padapter, "get_registered" );
		
		$array=array ("Kode",'DTD','Nama',"Sebab" );
		$muitable = new Table ( $array);
		$muitable->setModel ( Table::$SELECT );
		$muitable->setName ( "aso_icd" );
		$madapter = new SimpleAdapter ();
		$madapter->add ( "Nama", "nama" );
		$madapter->add ( "Kode", "icd" );
		$madapter->add ( "DTD", "dtd" );
		$madapter->add ( "Sebab", "sebab" );
		$dbtable = new DBTable ( $this->db, "smis_aso_icd" );
		$mresponder = new DBResponder ( $dbtable, $muitable, $madapter );
		
		$super = new SuperCommand ();
		$super->addResponder ( "aso_dokter", $dkresponder );
		$super->addResponder ( "aso_perawat", $dkresponder_perawat );
		$init = $super->initialize ();
		if ($init != null) {
			echo $init;
			return;
		}
	}
}

?>