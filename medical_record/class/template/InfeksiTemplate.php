<?php

require_once 'smis-framework/smis/template/ModulTemplate.php';
require_once ("smis-base/smis-include-service-consumer.php");
require_once "medical_record/class/service/RuanganService.php";
require_once 'medical_record/library/EmployeeResponder.php';

class InfeksiTemplate extends ModulTemplate {
    protected $db;
	protected $polislug;
	protected $page;
    protected $action;
	protected $protoslug;
	protected $protoname;
	protected $protoimplement;
	protected $dbtable;
	protected $uitable;
	protected $noreg_pasien;
	protected $nama_pasien;
	protected $nrm_pasien;
    protected $sebutan;
    protected $jk;
    protected $tanggal;
    public static $MODE_DAFTAR = "pendaftaran";
	public static $MODE_PERIKSA = "pemeriksaan";
    
    public function __construct($db, $mode, $polislug = "all", $page = "medical_record", $action = "input_infeksi", $protoslug = "", $protoname = "", $protoimplement = "", $noreg = "", $nrm = "", $nama = "") {
        $this->db               = $db;
        $this->mode             = $mode;
        $this->polislug         = $polislug;
        $this->page             = $page;
        $this->action           = $action;
		$this->protoslug        = $protoslug;
		$this->protoimplement   = $protoimplement;
        $this->protoname        = $protoname;
		$this->tanggal          = date("Y-m-d H:i");
		$this->noreg_pasien     = $noreg;
		$this->nama_pasien      = $nama;
		$this->nrm_pasien       = $nrm;
		$this->dbtable          = new DBTable ( $this->db, "smis_mr_infeksi" );
		
		if ($polislug != "all")
			$this->dbtable->addCustomKriteria ( "ruangan", "='" . $polislug . "'" );
		if ($noreg != "")
			$this->dbtable->addCustomKriteria ( "noreg_pasien", "='" . $noreg . "'" );
            
        if ($noreg != "") {
			$data_post = array ("command" => "edit","id" => $noreg );
			$service = new ServiceConsumer($this->db, "get_registered", $data_post);
			$service->execute ();
			$data = $service->getContent ();
			if($data!=NULL){
                $this->sebutan          = $data ['sebutan'];
                $this->jk               = $data ['kelamin'];
			}
		}
        
        $array=array ("No.","Tanggal","Jenis Infeksi", "Perawat", "Keterangan" );
		$this->uitable = new Table ( $array, " Infeksi " . ($this->polislug == "all" ? "" : ucfirst ( $this->protoname )), NULL, true );
		$this->uitable->setName ( $action );
    }
    
    public function command($command) {
        $adapter = new SimpleAdapter ();
        $adapter->setUseNumber(true, "No.","back.");
		$adapter->add ( "Tanggal", "tanggal", "date d M Y" );
		$adapter->add ( "Jenis Infeksi", "jenis_infeksi" );
		$adapter->add ( "Perawat", "nama_perawat" );
		$adapter->add ( "Keterangan", "keterangan" );
		$dbres = new DBResponder ( $this->dbtable, $this->uitable, $adapter );
		$data = $dbres->command ( $_POST ['command'] );
		echo json_encode ( $data );
		return;
    }
    
    public function phpPreLoad() {
        if ($this->noreg_pasien != "") {
            $this->uitable->addModal ( "id", "hidden", "", "", "y", null, false, null, false, null );
			$this->uitable->addModal ( "nama_pasien", "hidden", "", $this->nama_pasien, "n", null, true, null, false, null );
			$this->uitable->addModal ( "nrm_pasien", "hidden", "", $this->nrm_pasien, "n", null, true, null, false, null );
			$this->uitable->addModal ( "noreg_pasien", "hidden", "", $this->noreg_pasien, "n", null, true, null, false, null );
			$this->uitable->addModal ( "sebutan", "hidden", "", $this->sebutan, "y", null, true, null, false, null );
			$this->uitable->addModal ( "jk", "hidden", "", $this->jk, "y", null, true, null, false, null );
		} else {
            $this->uitable->addModal ( "id", "hidden", "", "", "y", null, false, null, false, null );
			$this->uitable->addModal ( "nama_pasien", "chooser-" . $this->action . "-infeksi_pasien", "Pasien", $this->nama_pasien, "n", null, true, null, false, null );
			$this->uitable->addModal ( "nrm_pasien", "text", "NRM", $this->nrm_pasien, "n", null, true, null, false, null );
			$this->uitable->addModal ( "noreg_pasien", "text", "No Reg", $this->noreg_pasien, "n", null, true, null, false, null );
			$this->uitable->addModal ( "sebutan", "hidden", "", "", "y", null, false, null, false, null );
			$this->uitable->addModal ( "jk", "hidden", "", "", "y", null, false, null, false, null );
		}
        
        if ($this->polislug == "all"){
			$service = new RuanganService ( $this->db );
			$service->execute ();
			$ruangan = $service->getContent ();
			$this->uitable->addModal ( "ruangan", "select", "Ruangan", $ruangan, "n", null, false, null, false, null );
		}else{
			$this->uitable->addModal ( "ruangan", "hidden", "", $this->polislug, "n", null, true, null, false, null );
		}
        
        $jenis_infeksi = new OptionBuilder ();
		$jenis_infeksi->add ( "", "", "1" );
		$jenis_infeksi->add ( "IADP", "IADP" );
		$jenis_infeksi->add ( "VAP", "VAP" );
		$jenis_infeksi->add ( "ISK", "ISK" );
		$jenis_infeksi->add ( "Plebitis", "Plebitis" );
		$jenis_infeksi->add ( "Decubitus", "Decubitus" );
		$jenis_infeksi->add ( "IDO", "IDO" );
        
        $this->uitable->addModal ( "jenis_infeksi", "select", "Jenis Infeksi", $jenis_infeksi->getContent (), "n", null, false, null, false, null );
        $this->uitable->addModal ( "nama_perawat", "chooser-" . $this->action . "-infeksi_perawat-Pilih Perawat", "Perawat", "", "n", null, true, null, false, null );
        $this->uitable->addModal ( "id_perawat", "hidden", "", "y", null, false, null, false, null );
        $this->uitable->addModal ( "tanggal", "datetime", "Tanggal", $this->tanggal, "n", true, false, null, false, null  );
        $this->uitable->addModal ( "keterangan", "textarea", "Keterangan", "", "y", null, false, null, false, null );
        
        $modal = $this->uitable->getModal ();
		$modal->setComponentSize(Modal::$MEDIUM);
		$modal->setTitle ( "Infeksi" );
        $modal->addHTML("
            <div class='alert alert-block alert-inverse' id='help_desk'>
                <h4>Keterangan Pilihan Jenis Infeksi</h4>
                <ul>
                    <li>IADP = Infeksi Aliran Darah Primer</li>
                    <li>VAP = Infeksi Paru-Paru (Pneumonia)</li>
                    <li>ISK =Infeksi Saluran Kemih</li>
                    <li>Plebitis = Infeksi Infus</li>
                    <li>Decubitus = Cedera yang Diakibatkan oleh Kerusakan Kulit</li>
                    <li>IDO = Infeksi Daerah Operasi</li>
                <ul>
            </div>
        ", "before");
		echo $this->uitable->getHtml ();
		echo $modal->getHtml ();
    }
    
    public function jsLoader() {
		echo addJS ( "framework/bootstrap/js/bootstrap-datetimepicker.js" );
		echo addJS ( "framework/smis/js/table_action.js" );
		loadLibrary ( "smis-libs-function-javascript" );
	}
    
	public function cssLoader() {
		echo addCSS ( "framework/bootstrap/css/bootstrap-datetimepicker.css" );
	}
    
    public function jsPreLoad() {
?>
    <script type="text/javascript">
        var <?php echo $this->action; ?>;
        var infeksi_noreg="<?php echo $this->noreg_pasien; ?>";
        var infeksi_pasien;
        var infeksi_perawat;
        var infeksi_nama_pasien="<?php echo $this->nama_pasien; ?>";
        var infeksi_nrm_pasien="<?php echo $this->nrm_pasien; ?>";
        var infeksi_polislug="<?php echo $this->polislug; ?>";
        var infeksi_the_page="<?php echo $this->page; ?>";
        var infeksi_the_protoslug="<?php echo $this->protoslug; ?>";
        var infeksi_the_protoname="<?php echo $this->protoname; ?>";
        var infeksi_the_protoimplement="<?php echo $this->protoimplement; ?>";
    
        $(document).ready(function() {
            $(".mydatetime").datetimepicker({ minuteStep: 1});
            
            var column=new Array(
                "id", "tanggal", "noreg_pasien", "nrm_pasien", "nama_pasien", "sebutan", "ruangan",
                "jk", "jenis_infeksi", "id_perawat", "nama_perawat", "keterangan"
            );
            <?php echo $this->action; ?>=new TableAction("<?php echo $this->action; ?>", infeksi_the_page,"<?php echo $this->action; ?>", column);
            <?php echo $this->action; ?>.setPrototipe(infeksi_the_protoname, infeksi_the_protoslug, infeksi_the_protoimplement);
            <?php echo $this->action; ?>.getRegulerData=function(){
                var reg_data={	
                        page:this.page,
                        action:this.action,
                        super_command:this.super_command,
                        prototype_name:this.prototype_name,
                        prototype_slug:this.prototype_slug,
                        prototype_implement:this.prototype_implement,
                        polislug:infeksi_polislug,
                        noreg_pasien:infeksi_noreg,
                        nama_pasien:infeksi_nama_pasien,
                        nrm_pasien:infeksi_nrm_pasien
                        };
                return reg_data;
            };
            <?php echo $this->action; ?>.view();
            
            infeksi_pasien=new TableAction("infeksi_pasien", infeksi_the_page, "<?php echo $this->action; ?>",new Array());
            infeksi_pasien.setSuperCommand("infeksi_pasien");
            infeksi_pasien.setPrototipe(infeksi_the_protoname, infeksi_the_protoslug, infeksi_the_protoimplement);
            infeksi_pasien.selected=function(json){
                $("#<?php echo $this->action; ?>_nama_pasien").val(json.nama_pasien);
                $("#<?php echo $this->action; ?>_nrm_pasien").val(json.nrm);
                $("#<?php echo $this->action; ?>_noreg_pasien").val(json.id);
                $("#<?php echo $this->action; ?>_jk").val(json.kelamin);
                $("#<?php echo $this->action; ?>_sebutan").val(json.sebutan);
                if(json.kamar_inap == "" || json.kamar_inap == null) {
                    $("#<?php echo $this->action; ?>_ruangan").val(json.jenislayanan);
                } else {
                    $("#<?php echo $this->action; ?>_ruangan").val(json.kamar_inap);
                }
            };
            
            infeksi_perawat=new TableAction("infeksi_perawat", infeksi_the_page, "<?php echo $this->action; ?>",new Array());
            infeksi_perawat.setSuperCommand("infeksi_perawat");
            infeksi_perawat.setPrototipe(infeksi_the_protoname, infeksi_the_protoslug, infeksi_the_protoimplement);
            infeksi_perawat.selected=function(json){
                $("#<?php echo $this->action; ?>_nama_perawat").val(json.nama);
                $("#<?php echo $this->action; ?>_id_perawat").val(json.id);
            };
        });
    </script>
<?php
    }
    
    public function superCommand($super_command) {
        $super = new SuperCommand ();
        if($super_command == 'infeksi_perawat') {
            $array=array ('Nama','Jabatan',"NIP" );
            $dktable = new Table ($array);
            $dktable->setName ( "infeksi_perawat" );
            $dktable->setModel ( Table::$SELECT );
            $dkadapter = new SimpleAdapter ();
            $dkadapter->add ( "Jabatan", "nama_jabatan" );
            $dkadapter->add ( "Nama", "nama" );
            $dkadapter->add ( "NIP", "nip" );
            $dkresponder = new EmployeeResponder($this->db, $dktable, $dkadapter) ;
            $super->addResponder ( "infeksi_perawat", $dkresponder );
        }
        if($super_command == 'infeksi_pasien') {
            /* PASIEN */
            $aname=array ('Sebutan','Nama','NRM',"No Reg" );
            $ptable = new Table ( $aname);
            $ptable->setName ( "infeksi_pasien" );
            $ptable->setModel ( Table::$SELECT );
            $padapter = new SimpleAdapter ();
            $padapter->add ( "Sebutan", "sebutan" );
            $padapter->add ( "Nama", "nama_pasien" );
            $padapter->add ( "NRM", "nrm", "digit8" );
            $padapter->add ( "No Reg", "id" );
            $presponder = new ServiceResponder ( $this->db, $ptable, $padapter, "get_registered" );
            $super->addResponder ( "infeksi_pasien", $presponder );
        }
		$init = $super->initialize ();
		if ($init != null) {
			echo $init;
			return;
		}
	}
}

?>