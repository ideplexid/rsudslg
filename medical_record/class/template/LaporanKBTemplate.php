<?php
require_once 'smis-framework/smis/template/ModulTemplate.php';
require_once ("smis-base/smis-include-service-consumer.php");
require_once "medical_record/class/service/RuanganService.php";
require_once 'smis-libs-hrd/EmployeeResponder.php';

class LaporanKBTemplate extends ModulTemplate {
	protected $db;
	protected $polislug;
	protected $page;
	protected $protoslug;
	protected $protoname;
	protected $protoimplement;
	protected $action;
	protected $dbtable;
	protected $uitable;
	protected $noreg_pasien;
	protected $nama_pasien;
	protected $nrm_pasien;
    protected $alamat;
    protected $no_bpjs;
    protected $no_telp;
	public function __construct($db, $polislug = "all", $noreg = "", $nrm = "", $nama = "", $page = "medical_record", $action = "lap_kb", $protoslug = "", $protoname = "", $protoimplement = "") {
		$this->db = $db;
		$this->noreg_pasien = $noreg;
		$this->nama_pasien = $nama;
		$this->nrm_pasien = $nrm;
		$this->polislug = $polislug;
		$this->dbtable = new DBTable ( $this->db, "smis_mr_kb" );
		$this->page = $page;
		$this->protoslug = $protoslug;
		$this->protoimplement = $protoimplement;
		$this->protoname = $protoname;
		$this->action = $action;
		if ($polislug != "all")
			$this->dbtable->addCustomKriteria ( "ruangan", "='" . $polislug . "'" );
		if ($noreg != "")
			$this->dbtable->addCustomKriteria ( "noreg_pasien", "='" . $noreg . "'" );
		$this->uitable = new Table ( array ('NRM',"Pasien","No Register" ), " Indikator " . ($this->polislug == "all" ? "" : ucfirst ( $this->protoname )), NULL, true );
		$this->uitable->setName ( $action );
		if ($this->page == "medical_record" && $this->action == "lap_kb") {
			$this->uitable->setAddButtonEnable ( false );
			$this->uitable->setDelButtonEnable ( false );
		}
        
        if ($noreg != "") {
			$data_post = array ("command" => "edit","id" => $noreg );
			$service = new ServiceConsumer($this->db, "get_registered", $data_post);
			$service->execute ();
			$data = $service->getContent ();
			if($data!=NULL){
				$this->alamat       = $data ['alamat_pasien']." ".$data ['nama_kelurahan']." ".$data ['nama_kecamatan']." ".$data ['nama_kabupaten']." ".$data ['nama_provinsi'];
                $this->no_bpjs      = $data ['nobpjs'];
                $this->no_telp      = $data ['telpon'];
			}
		}
	}
    
	public function command($command) {
        require_once "smis-base/smis-include-duplicate.php";
		$adapter = new SimpleAdapter ();
		$adapter->add ( "Pasien", "nama_pasien" );
		$adapter->add ( "NRM", "nrm_pasien", "digit8" );
		$adapter->add ( "No Register", "noreg_pasien", "digit8" );
		$dbres = new DuplicateResponder ( $this->dbtable, $this->uitable, $adapter );
		$dbres->setDuplicate(false,"");
        $dbres->setAutonomous(getSettings($this->db,"smis_autonomous_id",""));
        $data = $dbres->command ( $_POST ['command'] );
		echo json_encode ( $data );
		return;
	}
    
	public function phpPreLoad() {
		if ($this->noreg_pasien != "") {
			$this->uitable->addModal ( "nama_pasien", "hidden", "", $this->nama_pasien, "n", null, true );
			$this->uitable->addModal ( "nrm_pasien", "hidden", "", $this->nrm_pasien, "n", null, true );
			$this->uitable->addModal ( "noreg_pasien", "hidden", "", $this->noreg_pasien, "n", null, true );
		} else {
			$this->uitable->addModal ( "nama_pasien", "chooser-" . $this->action . "-lkb_pasien", "Pasien", $this->nama_pasien, "n", null, true );
			$this->uitable->addModal ( "nrm_pasien", "text", "NRM", $this->nrm_pasien, "n", null, true );
			$this->uitable->addModal ( "noreg_pasien", "text", "No Reg", $this->noreg_pasien, "n", null, true );
		}
		
        if ($this->polislug == "all"){
            $service = new RuanganService ( $this->db );
            $service->execute ();
            $ruangan = $service->getContent ();
            $this->uitable->addModal ( "ruangan", "select", "Asal Ruangan", $ruangan );
        }else{
            $this->uitable->addModal ( "ruangan", "hidden", "", $this->polislug, "n", null, true );
        }
        
		$the_row = array ();
		$the_row ['metode']             = "";
		$the_row ['pelayanan']          = "";
		$the_row ['cabut_apa']          = "";
		$the_row ['cabut_siap']         = "";
		$the_row ['pasca']              = "";
		$the_row ['informed']           = false;
		$the_row ['petugas']            = "Klinik";
        $the_row ['id_dokter']          = "";
        $the_row ['nama_dokter']        = "";
        $the_row ['id_perawat']         = "";
        $the_row ['nama_perawat']       = "";
		$the_row ['praski']             = false;
		$the_row ['komplikasi_berat']   = false;
		$the_row ['gagal']              = false;
		$the_row ['ganti_cara']         = false;
		$the_row ['tanggal']            = date ( "Y-m-d" );
        $the_row ['alamat']             = $this->alamat;
		$the_row ['no_bpjs']            = $this->no_bpjs;
		$the_row ['no_telp']            = $this->no_telp;	
		
		$exist ['noreg_pasien'] = $this->noreg_pasien;
		$exist ['nama_pasien'] = $this->nama_pasien;
		$exist ['nrm_pasien'] = $this->nrm_pasien;
		$exist ['ruangan'] = $this->polislug;
		if ($this->dbtable->is_exist ( $exist )) {
			$row = $this->dbtable->select ( $exist );
			$the_row ['metode']             = $row->metode;
			$the_row ['pelayanan']          = $row->pelayanan;
			$the_row ['cabut_apa']          = $row->cabut_apa;
			$the_row ['cabut_siapa']        = $row->cabut_siapa;
			$the_row ['pasca']              = $row->pasca;
			$the_row ['informed']           = $row->informed == "1";
			$the_row ['petugas']            = $row->petugas;
            $the_row ['id_dokter']          = $row->id_dokter;
            $the_row ['nama_dokter']        = $row->nama_dokter;
            $the_row ['id_perawat']         = $row->id_perawat;
            $the_row ['nama_perawat']       = $row->nama_perawat;
			$the_row ['praski']             = $row->praski == "1";
			$the_row ['komplikasi_berat']   = $row->komplikasi_berat == "1";
			$the_row ['gagal']              = $row->gagal == "1";
			$the_row ['ganti_cara']         = $row->ganti_cara == "1";
			$the_row ['tanggal']            = date ( "Y-m-d" );
			$the_row ['tanggal']            = $row->tanggal;
			$the_row ["id"]                 = $row->id;
		} else if(getSettings($this->db,"smis-rs-autosave-lap-kb","1")=="1"){
			$exist ['noreg_pasien']         = $this->noreg_pasien;
			$exist ['nrm_pasien']           = $this->nrm_pasien;
			$exist ['nama_pasien']          = $this->nama_pasien;
			$exist ['tanggal']              = date ( "Y-m-d" );
			$this->dbtable->insert ( $exist );
			$the_row ["id"]                 = $this->dbtable->get_inserted_id ();
            
            $update['origin_id']            = $the_row ["id"];
            $update['autonomous']           = "[".getSettings($this->db,"smis_autonomous_id","")."]";
            $this->dbtable->update($update,array("id"=>$the_row ["id"]));
		}
		$metode = new OptionBuilder ();
		$metode->add ( "IUD", "IUD", $the_row ['metode'] == "IUD" ? "1" : "0" );
		$metode->add ( "MOW", "MOW", $the_row ['metode'] == "MOW" ? "1" : "0" );
		$metode->add ( "MOP", "MOP", $the_row ['metode'] == "MOP" ? "1" : "0" );
		$metode->add ( "Kondom", "Kondom", $the_row ['metode'] == "Kondom" ? "1" : "0" );
		$metode->add ( "Implant", "Implant", $the_row ['metode'] == "Implant" ? "1" : "0" );
		$metode->add ( "Suntikan", "Suntikan", $the_row ['metode'] == "Suntikan" ? "1" : "0" );
		$metode->add ( "PIL", "PIL", $the_row ['metode'] == "PIL" ? "1" : "0" );
		
		$cabut = new OptionBuilder ();
		$cabut->add ( "", "", $the_row ['cabut_apa'] == "" ? "1" : "0" );
		$cabut->add ( "IUD", "IUD", $the_row ['cabut_apa'] == "IUD" ? "1" : "0" );
		$cabut->add ( "Implant", "Implant", $the_row ['cabut_apa'] == "Implant" ? "1" : "0" );
		
		$pelayanan = new OptionBuilder ();
		$pelayanan->add ( "Tidak KB", "Non", $the_row ['pelayanan'] == "Non" ? "1" : "0" );
		$pelayanan->add ( "KB Baru", "KB Baru", $the_row ['pelayanan'] == "KB Baru" ? "1" : "0" );
		$pelayanan->add ( "KB Ulang", "KB Ulang", $the_row ['pelayanan'] == "KB Ulang" ? "1" : "0" );
		
		$petugas = new OptionBuilder ();
		$petugas->add ( "Klinik KB", "Klinik", $the_row ['petugas'] == "Klinik" ? "1" : "0" );
		$petugas->add ( "Dokter Swasta", "Dokter", $the_row ['petugas'] == "Dokter" ? "1" : "0" );
		$petugas->add ( "Bidan Praktek", "Bidan", $the_row ['petugas'] == "Bidan" ? "1" : "0" );
		
		$petugas_cabut = new OptionBuilder ();
		$petugas_cabut->add ( "Klinik KB", "Klinik", $the_row ['cabut_siapa'] == "Klinik" ? "1" : "0" );
		$petugas_cabut->add ( "Dokter Swasta", "Dokter", $the_row ['cabut_siapa'] == "Dokter" ? "1" : "0" );
		$petugas_cabut->add ( "Bidan Praktek", "Bidan", $the_row ['cabut_siapa'] == "Bidan" ? "1" : "0" );
		
		$pasca = new OptionBuilder ();
		$pasca->add ( "Normal", "Normal", $the_row ['pasca'] == "Normal" ? "1" : "0" );
		$pasca->add ( "Persalinan", "Persalinan", $the_row ['pasca'] == "Persalinan" ? "1" : "0" );
		$pasca->add ( "Keguguran", "Keguguran", $the_row ['pasca'] == "Keguguran" ? "1" : "0" );
		
		$this->uitable->addModal ( "id", "hidden", "", $the_row ['id'] );
		$this->uitable->addModal ( "tanggal", "date", "Tanggal", $the_row ['tanggal'] );
		$this->uitable->addModal ( "alamat", "text", "Alamat", $the_row ['alamat'] );
		$this->uitable->addModal ( "no_telp", "text", "No. Telp", $the_row ['no_telp'] );
		$this->uitable->addModal ( "no_bpjs", "text", "No. BPJS", $the_row ['no_bpjs'] );
		$this->uitable->addModal ( "pasca", "select", "Pasien ", $pasca->getContent () );
		$this->uitable->addModal ( "pelayanan", "select", "Pelayanan", $pelayanan->getContent () );
		$this->uitable->addModal ( "petugas", "select", "Petugas", $petugas->getContent () );
        $this->uitable->addModal ( "nama_dokter", "chooser-".$this->action."-lkb_dokter-Dokter", "Dokter", $the_row ['nama_dokter'] );
        $this->uitable->addModal ( "id_dokter", "hidden", "", $the_row ['id_dokter'] );
        $this->uitable->addModal ( "nama_perawat", "chooser-".$this->action."-lkb_perawat-Perawat", "Perawat", $the_row ['nama_perawat'] );
        $this->uitable->addModal ( "id_perawat", "hidden", "", $the_row ['id_perawat'] );
        $this->uitable->addModal ( "metode", "select", "Metode", $metode->getContent () );
		$this->uitable->addModal ( "informed", "checkbox", "Informed Consent", $the_row ['informed'] );
		$this->uitable->addModal ( "cabut_apa", "select", "Metode yang dicabut", $cabut->getContent () );
		$this->uitable->addModal ( "cabut_siapa", "select", "Dicabut Oleh ", $petugas_cabut->getContent () );
		$this->uitable->addModal ( "praski", "checkbox", "Pra S / KS I", $the_row ['praski'] );
		$this->uitable->addModal ( "komplikasi_berat", "checkbox", "Komplikasi Berat", $the_row ['komplikasi_berat'] );
		$this->uitable->addModal ( "gagal", "checkbox", "Gagal", $the_row ['gagal'] );
		$this->uitable->addModal ( "ganti_cara", "checkbox", "Ganti Cara", $the_row ['ganti_cara'] );
		
		$modal = $this->uitable->getModal ();
		$modal->setTitle ( "Laporan KB" );
		if ($this->page == "medical_record" && $this->action == "lap_kb") {
			echo $this->uitable->getHtml ();
		} else {
			echo "<div class='hide'>" . $this->uitable->getHtml () . "</div>";
			$modal->setAlwaysShow ( true );
		}
		echo $modal->joinFooterAndForm ()->getHtml ();
	}
	public function jsLoader() {
		echo addJS ( "framework/bootstrap/js/bootstrap-datepicker.js" );
		echo addJS ( "framework/smis/js/table_action.js" );
		
		loadLibrary ( "smis-libs-function-javascript" );
	}
	public function cssLoader() {
		echo addCSS ( "framework/bootstrap/css/datepicker.css" );
	}
	public function cssPreLoad() {
		?>
		<style type="text/css">
		#<?php echo $this->action;?>_add_form_form>div { clear: both; width: 100%;}
		#<?php echo $this->action;?>_add_form_form>div label { width: 250px;}
		</style>
		<?php
	}
	
	/* when it's star build */
	public function jsPreLoad() {
		?>
		<script type="text/javascript">
		var <?php echo $this->action; ?>;
		var lkb_pasien;
		var lkb_noreg="<?php echo $this->noreg_pasien; ?>";
		var lkb_nama_pasien="<?php echo $this->nama_pasien; ?>";
		var lkb_nrm_pasien="<?php echo $this->nrm_pasien; ?>";
		var lkb_polislug="<?php echo $this->polislug; ?>";
		var lkb_the_page="<?php echo $this->page; ?>";
		var lkb_the_protoslug="<?php echo $this->protoslug; ?>";
		var lkb_the_protoname="<?php echo $this->protoname; ?>";
		var lkb_the_protoimplement="<?php echo $this->protoimplement; ?>";
		var lkb_prefix="<?php echo $this->action; ?>";

		function checktru(){
			var value=$("#"+lkb_prefix+"_pelayanan").val();
			if(value=="Non"){
				$("#"+lkb_prefix+"_metode").prop("disabled",true);
				$("#"+lkb_prefix+"_petugas").prop("disabled",true);	
				$("#"+lkb_prefix+"_cabut_apa").prop("disabled",true);	
				$("#"+lkb_prefix+"_cabut_siapa").prop("disabled",true);										
				$("#"+lkb_prefix+"_informed").prop("disabled",true);
				$("#"+lkb_prefix+"_praski").prop("disabled",true);
				$("#"+lkb_prefix+"_komplikasi_berat").prop("disabled",true);
				$("#"+lkb_prefix+"_gagal").prop("disabled",true);
				$("#"+lkb_prefix+"_ganti_cara").prop("disabled",true);					
			}else if(value=="KB Baru"){
				$("#"+lkb_prefix+"_metode").prop("disabled",false);
				$("#"+lkb_prefix+"_petugas").prop("disabled",false);	
				$("#"+lkb_prefix+"_cabut_apa").prop("disabled",true);	
				$("#"+lkb_prefix+"_cabut_siapa").prop("disabled",true);										
				$("#"+lkb_prefix+"_informed").prop("disabled",false);
				$("#"+lkb_prefix+"_praski").prop("disabled",false);
				$("#"+lkb_prefix+"_komplikasi_berat").prop("disabled",true);
				$("#"+lkb_prefix+"_gagal").prop("disabled",true);
				$("#"+lkb_prefix+"_ganti_cara").prop("disabled",true);
			}else if(value=="KB Ulang"){
				$("#"+lkb_prefix+"_metode").prop("disabled",false);
				$("#"+lkb_prefix+"_petugas").prop("disabled",false);	
				$("#"+lkb_prefix+"_cabut_apa").prop("disabled",false);	
				$("#"+lkb_prefix+"_cabut_siapa").prop("disabled",false);										
				$("#"+lkb_prefix+"_informed").prop("disabled",false);
				$("#"+lkb_prefix+"_praski").prop("disabled",false);
				$("#"+lkb_prefix+"_komplikasi_berat").prop("disabled",false);
				$("#"+lkb_prefix+"_gagal").prop("disabled",false);
				$("#"+lkb_prefix+"_ganti_cara").prop("disabled",false);
			}
		}
		
		$(document).ready(function() {

			$("#"+lkb_prefix+"_pelayanan").change(function(){
				checktru();
				
			});
			checktru();
			
			$(".mydate").datepicker();
			lkb_pasien=new TableAction("lkb_pasien",lkb_the_page,lkb_prefix,new Array());
			lkb_pasien.setSuperCommand("lkb_pasien");
			lkb_pasien.setPrototipe(lkb_the_protoname,lkb_the_protoslug,lkb_the_protoimplement);
			lkb_pasien.selected=function(json){
				var nama=json.nama_pasien;
				var nrm=json.nrm;
				var noreg=json.id;		
				$("#"+lkb_prefix+"_nama_pasien").val(nama);
				$("#"+lkb_prefix+"_nrm_pasien").val(nrm);
				$("#"+lkb_prefix+"_noreg_pasien").val(noreg);
			};
            
            lkb_dokter=new TableAction("lkb_dokter",lkb_the_page,lkb_prefix,new Array());
			lkb_dokter.setSuperCommand("lkb_dokter");
			lkb_dokter.setPrototipe(lkb_the_protoname,lkb_the_protoslug,lkb_the_protoimplement);
			lkb_dokter.selected=function(json){
				$("#"+lkb_prefix+"_nama_dokter").val(json.nama);
                $("#"+lkb_prefix+"_id_dokter").val(json.id);
			};
            
            lkb_perawat=new TableAction("lkb_perawat",lkb_the_page,lkb_prefix,new Array());
			lkb_perawat.setSuperCommand("lkb_perawat");
			lkb_perawat.setPrototipe(lkb_the_protoname,lkb_the_protoslug,lkb_the_protoimplement);
			lkb_perawat.selected=function(json){
				$("#"+lkb_prefix+"_nama_perawat").val(json.nama);
                $("#"+lkb_prefix+"_id_perawat").val(json.id);
			};
			
			var column=new Array(
					"id","tanggal","ruangan",
					"nama_pasien","noreg_pasien","nrm_pasien",
					"metode","pelayanan","informed",
					"petugas","praski","komplikasi_berat",
					"gagal","ganti_cara","cabut_apa","cabut_siapa","pasca",
                    "alamat","no_telp","no_bpjs","id_dokter","nama_dokter",
                    "id_perawat","nama_perawat"
					);
			<?php echo $this->action; ?>=new TableAction(lkb_prefix,lkb_the_page,lkb_prefix,column);
			<?php echo $this->action; ?>.setPrototipe(lkb_the_protoname,lkb_the_protoslug,lkb_the_protoimplement);
			<?php echo $this->action; ?>.getRegulerData=function(){
				var reg_data={	
						page:this.page,
						action:this.action,
						super_command:this.super_command,
						prototype_name:this.prototype_name,
						prototype_slug:this.prototype_slug,
						prototype_implement:this.prototype_implement,
						polislug:lkb_polislug,
						noreg_pasien:lkb_noreg,
						nama_pasien:lkb_nama_pasien,
						nrm_pasien:lkb_nrm_pasien
						};
				return reg_data;
			};
			<?php
		
			if ($this->page == "medical_record" && $this->action == "iklin") {
				echo $this->action;?>.view();
				<?php
			} else {
				echo $this->action . ".clear=function(){return;};";
			}
				?>
		});
		</script>
<?php
	}
	public function superCommand($super_command) {
		/* PASIEN */
        $super = new SuperCommand ();
		if($super_command=="lkb_pasien"){
            $ptable = new Table ( array ('Nama','NRM',"No Reg" ), "", NULL, true );
            $ptable->setName ( "lkb_pasien" );
            $ptable->setModel ( Table::$SELECT );
            $padapter = new SimpleAdapter ();
            $padapter->add ( "Nama", "nama_pasien" );
            $padapter->add ( "NRM", "nrm", "digit8" );
            $padapter->add ( "No Reg", "id" );
            $presponder = new ServiceResponder ( $this->db, $ptable, $padapter, "get_registered" );		
            $super->addResponder ( "lkb_pasien", $presponder );
        }else if($super_command=="lkb_dokter" || $super_command=="lkb_perawat"){
            $eadapt = new SimpleAdapter ();
            $eadapt->add ( "Jabatan", "nama_jabatan" );
            $eadapt->add ( "Nama", "nama" );
            $eadapt->add ( "NIP", "nip" );
            $head_karyawan=array ('Nama','Jabatan',"NIP");
            $dktable = new Table ( $head_karyawan, "", NULL, true );
            $dktable->setName ( $super_command );
            $dktable->setModel ( Table::$SELECT );
            $employee = new EmployeeResponder ( $this->db, $dktable, $eadapt, substr($super_command,4) );
            $super->addResponder ( $super_command, $employee );
        }
        
		$init = $super->initialize ();
		if ($init != null) {
			echo $init;
			return;
		}
	}
}

?>