<?php
require_once 'smis-framework/smis/template/ModulTemplate.php';
require_once ("smis-base/smis-include-service-consumer.php");
require_once 'medical_record/class/table/DiagnosaTable.php';
require_once 'medical_record/class/adapter/DiagnosaAdapter.php';
require_once 'smis-libs-hrd/EmployeeResponder.php';

class MedicalRecordTemplate extends ModulTemplate {
	protected $db;
	protected $mode;
	protected $polislug;
	protected $page;
	protected $protoslug;
	protected $protoname;
	protected $protoimplement;
	protected $action;
	protected $dbtable;
	protected $uitable;
	protected $noreg_pasien;
	protected $nama_pasien;
	protected $nrm_pasien;
	protected $list_pesan;
	protected $list_hasil;
	protected $gol_umur;
	protected $jk;
	protected $kunjungan;
	protected $carabayar;
	protected $urji;
    protected $sebutan;
    protected $alamat;
    protected $propinsi;
    protected $kabupaten;
    protected $kecamatan;
    protected $kelurahan;
    protected $profile_number;
    protected $tgl_masuk;
    protected $tgl_lahir;
    protected $rujukan;
	public static $MODE_DAFTAR = "pendaftaran";
	public static $MODE_PERIKSA = "pemeriksaan";
	public function __construct($db, $mode, $polislug = "all", $noreg = "", $nrm = "", $nama = "", $page = "medical_record", $action = "diagnosa", $protoslug = "", $protoname = "", $protoimplement = "", $jenis_umur = "TGL LAHIR TDK VALID", $jk = "0", $kunjungan = "Baru", $carabayar = "Umum",$urji="0") {
		$this->db = $db;
		$this->mode = $mode;
		$this->urji=$urji;
		$this->noreg_pasien = $noreg;
		$this->nama_pasien = $nama;
		$this->gol_umur = $jenis_umur;
		$this->nrm_pasien = $nrm;
		$this->polislug = $polislug;
		$this->dbtable = new DBTable ( $this->db, "smis_mr_diagnosa" );
		$this->page = $page;
		$this->protoslug = $protoslug;
		$this->protoimplement = $protoimplement;
		$this->protoname = $protoname;
		$this->action = $action;
		$this->jk = $jk;
		$this->kunjungan = $kunjungan;
		$this->carabayar = $carabayar;
        if($this->noreg_pasien != "" && $this->nrm_pasien != "") {
            $data = array();
            $data['command'] = 'list';
            $data['nrm'] = $this->nrm_pasien;
            $data['noreg_pasien'] = $this->noreg_pasien;

            $service = new ServiceConsumer ( $db, "get_registered_all", $data);
            $service->execute ();
            $d = $service->getContent ();
            $this->profile_number   = $d['data'][0]['profile_number'];
            $this->sebutan          = $d['data'][0]['sebutan'];
            $this->alamat           = $d['data'][0]['alamat_pasien'];
            $this->tgl_lahir        = $d['data'][0]['tgl_lahir'];
            $this->propinsi         = $d['data'][0]['nama_provinsi'];
            $this->kabupaten        = $d['data'][0]['nama_kabupaten'];
            $this->kecamatan        = $d['data'][0]['nama_kecamatan'];
            $this->kelurahan        = $d['data'][0]['nama_kelurahan'];
            $this->rujukan          = $d['data'][0]['rujukan'];
            if($d['data'][0]['kamar_inap'] == "" || $d['data'][0]['kamar_inap'] == NULL) {
                $tgl_masuk              = explode(" ", $d['data'][0]['tanggal']);
                $this->tgl_masuk        = $tgl_masuk[0];
            } else {
                $tgl_masuk              = explode(" ", $d['data'][0]['tanggal_inap']);
                $this->tgl_masuk        = $tgl_masuk[0];
            }
            
        } 
		if ($nrm != ""){
            $this->dbtable->addCustomKriteria ( "nrm_pasien", "='" . $nrm. "'" );
        }
		
		$all_area=getSettings($db,"smis-rs-igd-all-area","0")=="1";
        $array=array ('Tanggal','No. Profile','NRM',"No.Reg","Pasien","Jenis Kelamin","Dokter",'Diagnosa',"ICD","Penyakit","Ruangan" );
		$this->uitable = new DiagnosaTable ($polislug,$noreg, $array , " Diagnosa " . ($this->polislug == "all" ? "" : ucfirst ( $this->protoname )), NULL, true );
		$this->uitable->setEnableDellInAllArea($all_area);
        $this->uitable->setName ( $action );
		if ($this->mode == self::$MODE_PERIKSA) {
			$this->uitable->setAddButtonEnable ( false );
			$this->uitable->setDelButtonEnable ( false );
		}
	}
	public function command($command) {
        require_once "smis-base/smis-include-duplicate.php";
		$adapter = new DiagnosaAdapter ();
		$adapter->add ( "Tanggal", "tanggal", "date d M Y" );
		$adapter->add ( "Pasien", "nama_pasien" );
		$adapter->add ( "NRM", "nrm_pasien", "digit8" );
		$adapter->add ( "No. Profile", "profile_number", "digit8" );
		$adapter->add ( "No.Reg", "noreg_pasien" );
		$adapter->add ( "Dokter", "nama_dokter" );
		$adapter->add ( "Diagnosa", "diagnosa" );
		$adapter->add ( "ICD", "kode_icd" );
		$adapter->add ( "Penyakit", "nama_icd" );
		$adapter->add ( "Kelas", "kelas", "unslug" );
		$adapter->add ( "Ruangan", "ruangan" );
		$adapter->add ( "Jenis Kelamin", "jk", "trivial_0_Laki-Laki_Perempuan" );
		require_once("medical_record/class/responder/DiagnosaResponder.php");
		$dbres = new DiagnosaResponder( $this->dbtable, $this->uitable, $adapter );
		$dbres->setDuplicate(false,"");
		$dbres->setAutonomous(getSettings($this->db,"smis_autonomous_id",""));
		if ($dbres->isSave()) {
			$tgl_masuk = "";
			$tgl_lahir = "";
			if (isset($_POST['ruangan']) && isset($_POST['noreg_pasien'])) {
				$ruangan = $_POST['ruangan'];
				$noreg_pasien = $_POST['noreg_pasien'];
				
				$row = $this->db->get_row("
					SELECT DATE(waktu) tgl_masuk
					FROM smis_rwt_antrian_" . $ruangan . "
					WHERE no_register = '" . $noreg_pasien . "'
				");
				if ($row != null)
					$tgl_masuk = $row->tgl_masuk;

				$row = $this->db->get_row("
					SELECT tgl_lahir
					FROM smis_rgv_layananpasien 
					WHERE id = '" . $noreg_pasien . "'
				");
				if ($row != null)
					$tgl_lahir = $row->tgl_lahir;
			}
			$_POST['tgl_masuk'] = $tgl_masuk;
			$_POST['tgl_lahir'] = $tgl_lahir;
		}
        $data = $dbres->command ( $_POST ['command'] );
		echo json_encode ( $data );
		return;
	}
	public function phpPreLoad() {
		$kelas = new OptionBuilder ();
		$kl = json_decode ( getSettings ( $this->db, "smis-hospital-class", null ), true );
		foreach ( $kl as $kl_key => $kl_v ) {
			$kelas->add ( $kl_key, $kl_v );
		}
		
		loadLibrary ( "smis-libs-function-medical" );
		$gol_umur = get_medical_gol_umur ( $this->gol_umur );
        
        if($this->noreg_pasien != "") {
            $data_post = array ();
            $data_post['command']="edit";
            $data_post['id'] = $this->noreg_pasien;
            $service = new ServiceConsumer ( $db, "get_registered", $data_post );
            $service->execute ();
            $data = $service->getContent ();
            $this->sebutan = $data['sebutan'];
            $this->profile_number = $data['profile_number'];
        }
			
		$kasus = new OptionBuilder();
		$kasus->addSingle("Baru","1");
		$kasus->addSingle("Lama","0");
		
		$luka=new OptionBuilder();
		$luka->addSingle("");
		$luka->addSingle("Mengering");
		$luka->addSingle("Basah");
		$luka->addSingle("Nekrosis");
		
		$nyeri=new OptionBuilder();
		$nyeri->addSingle("");
		$nyeri->addSingle("Numeric Rating Scale 0 : tidak nyeri");
		$nyeri->addSingle("Numeric Rating Scale 1 : nyeri ringan");
		$nyeri->addSingle("Numeric Rating Scale 2 : nyeri ringan");
		$nyeri->addSingle("Numeric Rating Scale 3 : nyeri ringan");
		$nyeri->addSingle("Numeric Rating Scale 4 : nyeri sedang");
		$nyeri->addSingle("Numeric Rating Scale 5 : nyeri sedang");
		$nyeri->addSingle("Numeric Rating Scale 6 : nyeri sedang");
		$nyeri->addSingle("Numeric Rating Scale 7 : nyeri berat");
		$nyeri->addSingle("Numeric Rating Scale 8 : nyeri berat");
		$nyeri->addSingle("Numeric Rating Scale 9 : nyeri berat");
		$nyeri->addSingle("Numeric Rating Scale 10: nyeri sangat berat");
		$nyeri->addSingle("Wong Baker Faces 0 : tidak merasakan sakit");
		$nyeri->addSingle("Wong Baker Faces 2 : sedikit rasa sakit");
		$nyeri->addSingle("Wong Baker Faces 4 : nyeri ringan");
		$nyeri->addSingle("Wong Baker Faces 6 : nyeri sedang");
		$nyeri->addSingle("Wong Baker Faces 8 : nyeri berat");
		$nyeri->addSingle("Wong Baker Faces 10 : nyeri sangat berat");
		$nyeri->addSingle("FLACCS 0 : tidak nyeri");
		$nyeri->addSingle("FLACCS 1 : nyeri ringan");
		$nyeri->addSingle("FLACCS 2 : nyeri ringan");
		$nyeri->addSingle("FLACCS 3 : nyeri ringan");
		$nyeri->addSingle("FLACCS 4 : nyeri sedang");
		$nyeri->addSingle("FLACCS 5 : nyeri sedang");
		$nyeri->addSingle("FLACCS 6 : nyeri sedang");
		$nyeri->addSingle("FLACCS 7 : nyeri hebat");
		$nyeri->addSingle("FLACCS 8 : nyeri hebat");
		$nyeri->addSingle("FLACCS 9 : nyeri hebat");
		$nyeri->addSingle("FLACCS 10: nyeri hebat");
		$nyeri->addSingle("NIPS 0 : Tidak ada nyeri");
		$nyeri->addSingle("NIPS 1 : Nyeri Ringan");
		$nyeri->addSingle("NIPS 2 : Nyeri Ringan");
		$nyeri->addSingle("NIPS 3 : Nyeri Sedang");
		$nyeri->addSingle("NIPS 4 : Nyeri Sedang");
		$nyeri->addSingle("NIPS 5 : Nyeri Berat");
		$nyeri->addSingle("NIPS 6 : Nyeri Berat");
		$nyeri->addSingle("Comfort Scale 09");
		$nyeri->addSingle("Comfort Scale 10");
		$nyeri->addSingle("Comfort Scale 11");
		$nyeri->addSingle("Comfort Scale 12");
		$nyeri->addSingle("Comfort Scale 13");
		$nyeri->addSingle("Comfort Scale 14");
		$nyeri->addSingle("Comfort Scale 15");
		$nyeri->addSingle("Comfort Scale 16");
		$nyeri->addSingle("Comfort Scale 17");
		$nyeri->addSingle("Comfort Scale 18");
		$nyeri->addSingle("Comfort Scale 19");
		$nyeri->addSingle("Comfort Scale 20");
		$nyeri->addSingle("Comfort Scale 21");
		$nyeri->addSingle("Comfort Scale 22");
		$nyeri->addSingle("Comfort Scale 23");
		$nyeri->addSingle("Comfort Scale 24");
		$nyeri->addSingle("Comfort Scale 25");
		$nyeri->addSingle("Comfort Scale 26");
		$nyeri->addSingle("Comfort Scale 27");
		$nyeri->addSingle("Comfort Scale 28");
		$nyeri->addSingle("Comfort Scale 29");
		$nyeri->addSingle("Comfort Scale 30");
		$nyeri->addSingle("Comfort Scale 31");
		$nyeri->addSingle("Comfort Scale 32");
		$nyeri->addSingle("Comfort Scale 33");
		$nyeri->addSingle("Comfort Scale 34");
		$nyeri->addSingle("Comfort Scale 35");
		$nyeri->addSingle("Comfort Scale 36");
		$nyeri->addSingle("Comfort Scale 37");
		$nyeri->addSingle("Comfort Scale 38");
		$nyeri->addSingle("Comfort Scale 39");
		$nyeri->addSingle("Comfort Scale 40");
		$nyeri->addSingle("Comfort Scale 41");
		$nyeri->addSingle("Comfort Scale 42");
		$nyeri->addSingle("Comfort Scale 43");
		$nyeri->addSingle("Comfort Scale 44");
		$nyeri->addSingle("Comfort Scale 45");
		
		$this->uitable->addModal ( "id", "hidden", "", "" );
		$this->uitable->addModal ( "jk", "hidden", "", $this->jk );
		$this->uitable->addModal ( "kunjungan", "hidden", "", $this->kunjungan );
		$this->uitable->addModal ( "carabayar", "hidden", "", $this->carabayar );
		$this->uitable->addModal ( "urji", "hidden", "", $this->urji);
		$this->uitable->addModal ( "tanggal", "date", "Tanggal", date ( "Y-m-d" ) );
        $this->uitable->addModal ( "tgl_lahir", "hidden", "", $this->tgl_lahir);
		
		
		if($this->mode == self::$MODE_DAFTAR){
			$this->uitable->addModal("", "label", "<strong>DATA DOKTER / PSIKOLOG</strong>", "");
			$this->uitable->addModal ( "nama_dokter", "chooser-" . $this->action . "-mr_dokter-Pilih Dokter / Psikolog", "Dokter / Psikolog", "", "n", null, false,null,true,"tensi" );
			$this->uitable->addModal ( "id_dokter", "text", "NIP Dokter / Psikolog", "", "n", null, true );
			$this->uitable->addModal("", "label", "<strong>TANDA VITAL</strong>", "");
			$this->uitable->addModal("tensi", "text", "Tensi (mmHg) ", "","y",null,false,null,false,"nadi");
			$this->uitable->addModal("nadi", "text", "Nadi (x/menit)", "","y",null,false,null,false,"suhu");
			$this->uitable->addModal("suhu", "text", "Suhu (&#176;C)", "","y",null,false,null,false,"rr");
			$this->uitable->addModal("rr", "text", "Respiration Rate (x/menit)", "","y",null,false,null,false,"nyeri");
			$this->uitable->addModal("nyeri", "select", "Skala Nyeri", $nyeri->getContent(),"y",null,false,null,false,"berat_badan");
			$this->uitable->addModal("berat_badan", "text", "Berat Badan", "","y",null,false,null,false,"gula_darah");
			$this->uitable->addModal("gula_darah", "text", "Gula Darah (mg/dL)", "","y",null,false,null,false,"keadaan_luka");
			$this->uitable->addModal("keadaan_luka", "select", "Keadaan Luka", $luka->getContent(),"y",null,false,null,false,"keadaan_umum");
			$this->uitable->addModal("keadaan_umum", "textarea", "Keadaan Umum", "","y",null,false,null,false,"diagnosa");
			$this->uitable->addModal("", "label", "<strong>DIAGNOSA</strong>", "");
			
			//$this->uitable->addModal ( "kelas", "select", "Kelas", $kelas->getContent (), "y", null, true);smis-rs-igd-diagnosa-icd
			if(getSettings($this->db,"smis-rs-igd-diagnosa-icd","0")=="1"){
				$this->uitable->addModal ( "diagnosa", "chooser-".$this->action."-mr_diagnosa_icd-Pilih ICD", "Diagnosa Primer", "","y",null,false,null,false,"keterangan");
			}else{
				$this->uitable->addModal ( "diagnosa", "text", "Diagnosa Primer", "","y",null,false,null,false,"keterangan");
			}

			$this->uitable->addModal ( "nama_icd", "chooser-" . $this->action . "-mr_icd", "Nama ICD", "", "y", null, true );
			$this->uitable->addModal ( "kode_icd", "text", "Kode ICD", "", "y", null, true );
			$this->uitable->addModal ( "sebab_sakit", "text", "Penyebab", "", "y", null, true );

			$this->uitable->addModal ( "keterangan", "textarea", "Diagnosa Lain", "", "y", null, false,null,false,"kasus" );
            $this->uitable->addModal ( "diagnosa_sekunder1", "chooser-" . $this->action . "-mr_diagnosa_sekunder1", "Diagnosa Sekunder 1", "", "y", null, false,null,true );
            $this->uitable->addModal ( "kode_icd_diagnosa_sekunder1", "text", "ICD X", "", "y", null, true );
            $this->uitable->addModal ( "diagnosa_sekunder2", "chooser-" . $this->action . "-mr_diagnosa_sekunder2", "Diagnosa Sekunder 2", "", "y", null, false,null,true );
            $this->uitable->addModal ( "kode_icd_diagnosa_sekunder2", "text", "ICD X", "", "y", null, true );
            $this->uitable->addModal ( "diagnosa_sekunder3", "chooser-" . $this->action . "-mr_diagnosa_sekunder3", "Diagnosa Sekunder 3", "", "y", null, false,null,true );
            $this->uitable->addModal ( "kode_icd_diagnosa_sekunder3", "text", "ICD X", "", "y", null, true );
            $this->uitable->addModal ( "diagnosa_sekunder4", "chooser-" . $this->action . "-mr_diagnosa_sekunder4", "Diagnosa Sekunder 4", "", "y", null, false,null,true );
            $this->uitable->addModal ( "kode_icd_diagnosa_sekunder4", "text", "ICD X", "", "y", null, true );
            $this->uitable->addModal ( "diagnosa_sekunder5", "chooser-" . $this->action . "-mr_diagnosa_sekunder5", "Diagnosa Sekunder 5", "", "y", null, false,null,true );
            $this->uitable->addModal ( "kode_icd_diagnosa_sekunder5", "text", "ICD X", "", "y", null, true );
            $this->uitable->addModal ( "diagnosa_external_cause", "chooser-" . $this->action . "-mr_diagnosa_external_cause", "Diagnosa Ext. Cuase", "", "y", null, false,null,true );
			$this->uitable->addModal ( "kode_icd_diagnosa_external_cause", "text", "ICD X", "", "y", null, true );
			$this->uitable->addModal ( "sebab_diagnosa_external_cause", "text", "Sebab", "", "y", null, true );
            $this->uitable->addModal ( "diagnosa_tindakan", "chooser-" . $this->action . "-mr_diagnosa_tindakan", "Diagnosa Tindakan / ICD IX 1", "", "y", null, false,null,true );
            $this->uitable->addModal ( "diagnosa_tindakan2", "chooser-" . $this->action . "-mr_diagnosa_tindakan2", "Diagnosa Tindakan / ICD IX 2", "", "y", null, false,null,true );
			$this->uitable->addModal ( "diagnosa_tindakan3", "chooser-" . $this->action . "-mr_diagnosa_tindakan3", "Diagnosa Tindakan / ICD IX 3", "", "y", null, false,null,true );
			$this->uitable->addModal ( "diagnosa_kematian_manual", "text", "Diagnosa Kematian", "" );
            $this->uitable->addModal ( "diagnosa_kematian", "chooser-" . $this->action . "-mr_diagnosa_kematian", "Diagnosa Kematian", "", "y", null, false,null,true );
            $this->uitable->addModal ( "kode_icd_diagnosa_kematian", "text", "ICD X", "", "y", null, true );
			
			if ($this->gol_umur == "TGL LAHIR TDK VALID")
				$this->uitable->addModal ( "gol_umur", "select", "Golongan", $gol_umur, "y", null, false );
			else
				$this->uitable->addModal ( "gol_umur", "hidden", "", $this->gol_umur );
				
			if ($this->noreg_pasien != "") {
				$this->uitable->addModal ( "nama_pasien", "hidden", "", $this->nama_pasien, "n", null, true );
                $this->uitable->addModal ( "sebutan", "hidden", "", $this->sebutan, "y", null, true );
				$this->uitable->addModal ( "nrm_pasien", "hidden", "", $this->nrm_pasien, "n", null, true );
				$this->uitable->addModal ( "noreg_pasien", "hidden", "", $this->noreg_pasien, "n", null, true );
				$this->uitable->addModal ( "profile_number", "hidden", "", $this->profile_number, "y", null, true );
				$this->uitable->addModal ( "alamat", "hidden", "", $this->alamat, "y", null, true );
				$this->uitable->addModal ( "propinsi", "hidden", "", $this->propinsi, "y", null, true );
				$this->uitable->addModal ( "kabupaten", "hidden", "", $this->kabupaten, "y", null, true );
				$this->uitable->addModal ( "kecamatan", "hidden", "", $this->kecamatan, "y", null, true );
				$this->uitable->addModal ( "kelurahan", "hidden", "", $this->kelurahan, "y", null, true );
				$this->uitable->addModal ( "tgl_masuk", "hidden", "", $this->tgl_masuk, "y", null, true );
				$this->uitable->addModal ( "rujukan", "hidden", "", $this->rujukan, "y", null, true );
			} else {
				$this->uitable->addModal ( "nama_pasien", "chooser-" . $this->action . "-mr_pasien", "Pasien", $this->nama_pasien, "n", null, true );
                $this->uitable->addModal ( "sebutan", "text", "Sebutan", $this->sebutan, "y", null, true );
				$this->uitable->addModal ( "nrm_pasien", "text", "NRM", $this->nrm_pasien, "n", null, true );
				$this->uitable->addModal ( "noreg_pasien", "text", "No Reg", $this->noreg_pasien, "n", null, true );
				$this->uitable->addModal ( "profile_number", "text", "No. Profile", $this->profile_number, "n", null, true );
				$this->uitable->addModal ( "alamat", "text", "Alamat", $this->alamat, "n", null, true );
				$this->uitable->addModal ( "propinsi", "text", "Propinsi", $this->propinsi, "n", null, true );
				$this->uitable->addModal ( "kabupaten", "text", "Kabupaten", $this->kabupaten, "n", null, true );
				$this->uitable->addModal ( "kecamatan", "text", "Kecamatan", $this->kecamatan, "n", null, true );
				$this->uitable->addModal ( "kelurahan", "text", "Kelurahan", $this->kelurahan, "n", null, true );
				$this->uitable->addModal ( "tgl_masuk", "date", "Tgl Masuk", $this->tgl_masuk, "n", null, true );
                $this->uitable->addModal ( "rujukan", "hidden", "", $this->rujukan, "y", null, true );
			}
				
			if ($this->polislug == "all"){
                loadClass ( "ServiceProviderList" );
                $service = new ServiceProviderList ( $this->db, "push_antrian" );
                $service->execute ();
                $ruangan = $service->getContent ();
				$this->uitable->addModal ( "ruangan", "select", "Asal Ruangan", $ruangan );
            }else{
                $this->uitable->addModal ( "ruangan", "text", "Asal Ruangan", $this->polislug, "n", null, true );
            }
				
			$this->uitable->addModal ( "kasus", "select", "Kasus", $kasus->getContent(),"y", null, false,null,false,"save" );
		}else{
			$_editing=!getSettings($this->db,"smis-rs-igd-diagnosa-edit","0")=="1";			
			$this->uitable->addModal("", "label", "<strong>DIAGNOSA</strong>", "");
			$this->uitable->addModal ( "diagnosa", "text", "Diagnosa Primer", "", "y", null, $_editing ,null,false,"keterangan");
			$this->uitable->addModal ( "nama_icd", "chooser-" . $this->action . "-mr_icd", "Nama ICD", "", "y", null, false,null,true );
			$this->uitable->addModal ( "kode_icd", "text", "Kode ICD", "", "y", null, true );
			$this->uitable->addModal ( "sebab_sakit", "text", "Penyebab", "", "y", null, true );
			$this->uitable->addModal ( "keterangan", "textarea", "Diagnosa Lain", "", "y", null, false,null,false,"kasus" );
            $this->uitable->addModal ( "diagnosa_sekunder1", "chooser-" . $this->action . "-mr_diagnosa_sekunder1", "Diagnosa Sekunder 1", "", "y", null, false,null,true );
            $this->uitable->addModal ( "kode_icd_diagnosa_sekunder1", "text", "ICD X", "", "y", null, true );
            $this->uitable->addModal ( "diagnosa_sekunder2", "chooser-" . $this->action . "-mr_diagnosa_sekunder2", "Diagnosa Sekunder 2", "", "y", null, false,null,true );
            $this->uitable->addModal ( "kode_icd_diagnosa_sekunder2", "text", "ICD X", "", "y", null, true );
            $this->uitable->addModal ( "diagnosa_sekunder3", "chooser-" . $this->action . "-mr_diagnosa_sekunder3", "Diagnosa Sekunder 3", "", "y", null, false,null,true );
            $this->uitable->addModal ( "kode_icd_diagnosa_sekunder3", "text", "ICD X", "", "y", null, true );
            $this->uitable->addModal ( "diagnosa_sekunder4", "chooser-" . $this->action . "-mr_diagnosa_sekunder4", "Diagnosa Sekunder 4", "", "y", null, false,null,true );
            $this->uitable->addModal ( "kode_icd_diagnosa_sekunder4", "text", "ICD X", "", "y", null, true );
            $this->uitable->addModal ( "diagnosa_sekunder5", "chooser-" . $this->action . "-mr_diagnosa_sekunder5", "Diagnosa Sekunder 5", "", "y", null, false,null,true );
            $this->uitable->addModal ( "kode_icd_diagnosa_sekunder5", "text", "ICD X", "", "y", null, true );
            $this->uitable->addModal ( "diagnosa_external_cause", "chooser-" . $this->action . "-mr_diagnosa_external_cause", "Diagnosa Ext. Cuase", "", "y", null, false,null,true );
			$this->uitable->addModal ( "kode_icd_diagnosa_external_cause", "text", "ICD X", "", "y", null, true );
			$this->uitable->addModal ( "sebab_diagnosa_external_cause", "text", "Sebab", "", "y", null, true );
            $this->uitable->addModal ( "diagnosa_tindakan", "chooser-" . $this->action . "-mr_diagnosa_tindakan", "Diagnosa Tindakan / ICD IX 1", "", "y", null, false,null,true );
            $this->uitable->addModal ( "diagnosa_tindakan2", "chooser-" . $this->action . "-mr_diagnosa_tindakan2", "Diagnosa Tindakan / ICD IX 2", "", "y", null, false,null,true );
			$this->uitable->addModal ( "diagnosa_tindakan3", "chooser-" . $this->action . "-mr_diagnosa_tindakan3", "Diagnosa Tindakan / ICD IX 3", "", "y", null, false,null,true );
			$this->uitable->addModal ( "diagnosa_kematian_manual", "text", "Diagnosa Kematian", "" );
            $this->uitable->addModal ( "diagnosa_kematian", "chooser-" . $this->action . "-mr_diagnosa_kematian", "Diagnosa Kematian", "", "y", null, false,null,true );
            $this->uitable->addModal ( "kode_icd_diagnosa_kematian", "text", "ICD X", "", "y", null, true );
			$this->uitable->addModal("", "label", "<strong>DATA DOKTER</strong>", "");
			$this->uitable->addModal ( "nama_dokter", "text", "Dokter", "", "n", null, false,null,false,"tensi" );
			$this->uitable->addModal ( "id_dokter", "text", "NIP Dokter", "", "n", null, true );
			$this->uitable->addModal ( "ruangan", "text", "Asal Ruangan", "", "n", null, true );
			$this->uitable->addModal("", "label", "<strong>TANDA VITAL</strong>", "");
			$this->uitable->addModal("tensi", "text", "Tensi (mmHg) ", "","y",null,false,null,false,"nadi");
			$this->uitable->addModal("nadi", "text", "Nadi (x/menit)", "","y",null,false,null,false,"suhu");
			$this->uitable->addModal("suhu", "text", "Suhu (&#176;C)", "","y",null,false,null,false,"rr");
			$this->uitable->addModal("rr", "text", "Respiration Rate (x/menit)", "","y",null,false,null,false,"nyeri");
			$this->uitable->addModal("nyeri", "select", "Skala Nyeri", $nyeri->getContent(),"y",null,false,null,false,"berat_badan");
			$this->uitable->addModal("berat_badan", "text", "Berat Badan", "","y",null,false,null,false,"gula_darah");
			$this->uitable->addModal("gula_darah", "text", "Gula Darah (mg/dL)", "","y",null,false,null,false,"keadaan_luka");
			$this->uitable->addModal("keadaan_luka", "select", "Keadaan Luka", $luka->getContent(),"y",null,false,null,false,"keadaan_umum");
			$this->uitable->addModal("keadaan_umum", "textarea", "Keadaan Umum", "","y",null,false,null,false,"diagnosa");
			//$this->uitable->addModal ( "kelas", "hidden", "Kelas", $kelas->getContent (), "y", null, true );						
			$this->uitable->addModal("", "label", "<strong>DATA PASIEN</strong>", "");
			$this->uitable->addModal ( "nama_pasien", "text", "Pasien", $this->nama_pasien, "n", null, true );
            $this->uitable->addModal ( "sebutan", "text", "Sebutan", $this->sebutan, "y", null, true );
			$this->uitable->addModal ( "noreg_pasien", "text", "No Reg", $this->noreg_pasien, "n", null, true );
			$this->uitable->addModal ( "nrm_pasien", "text", "NRM", $this->nrm_pasien, "n", null, true );	
			$this->uitable->addModal ( "profile_number", "text", "No. Profile", $this->profile_number, "n", null, true );	
			$this->uitable->addModal ( "gol_umur", "select", "Golongan", $gol_umur, "y", null, false );
			$this->uitable->addModal ( "kasus", "hidden", "", "Baru","y", null, false,null,false,"save" );
            $this->uitable->addModal ( "alamat", "hidden", "", $this->alamat, "y", null, true );
            $this->uitable->addModal ( "propinsi", "hidden", "", $this->propinsi, "y", null, true );
            $this->uitable->addModal ( "kabupaten", "hidden", "", $this->kabupaten, "y", null, true );
            $this->uitable->addModal ( "kecamatan", "hidden", "", $this->kecamatan, "y", null, true );
            $this->uitable->addModal ( "kelurahan", "hidden", "", $this->kelurahan, "y", null, true );
            $this->uitable->addModal ( "tgl_masuk", "hidden", "", $this->tgl_masuk, "y", null, true );
            $this->uitable->addModal ( "rujukan", "hidden", "", $this->rujukan, "y", null, true );
		}
		
		$modal = $this->uitable->getModal ();
		$modal->setComponentSize(Modal::$MEDIUM);
		$modal->setTitle ( "Diagnosa" );
		echo $this->uitable->getHtml ();
		echo $modal->getHtml ();
	}
	public function jsLoader() {
		echo addJS ( "framework/bootstrap/js/bootstrap-datepicker.js" );
		echo addJS ( "framework/smis/js/table_action.js" );
		loadLibrary ( "smis-libs-function-javascript" );
	}
	public function cssLoader() {
		echo addCSS ( "framework/bootstrap/css/datepicker.css" );
	}
	
	
	/* when it's star build */
	public function jsPreLoad() {
		?>
<script type="text/javascript">

		var <?php echo $this->action; ?>;
		var mr_dokter;
		var mr_icd;
		var mr_pasien;
		var mr_diagnosa_icd;
		var mr_noreg="<?php echo $this->noreg_pasien; ?>";
		var mr_nama_pasien="<?php echo $this->nama_pasien; ?>";
		var mr_nrm_pasien="<?php echo $this->nrm_pasien; ?>";
		var mr_polislug="<?php echo $this->polislug; ?>";
		var mr_the_page="<?php echo $this->page; ?>";
		var mr_the_protoslug="<?php echo $this->protoslug; ?>";
		var mr_the_protoname="<?php echo $this->protoname; ?>";
		var mr_the_protoimplement="<?php echo $this->protoimplement; ?>";
		var mr_diagnosa_sekunder1;
		var mr_diagnosa_sekunder2;
		var mr_diagnosa_sekunder3;
		var mr_diagnosa_sekunder4;
		var mr_diagnosa_sekunder5;
		var mr_diagnosa_external_cause;
		var mr_diagnosa_tindakan;
		var mr_diagnosa_tindakan2;
		var mr_diagnosa_tindakan3;
		var mr_diagnosa_kematian;
		$(document).ready(function() {
			$(".mydate").datepicker();


			$('#<?php echo $this->action; ?>_nama_dokter').typeahead({
				minLength:3,
		        source: function (query, process) {
			     var data_dokter=mr_dokter.getViewData();
			     data_dokter["kriteria"]=$('#<?php echo $this->action; ?>_nama_dokter').val();
		         var $items = new Array;
		           $items = [""];				                
		          $.ajax({
		            url: '',
		            type: 'POST',
		            data: data_dokter,
		            success: function(res) {
		              var json=getContent(res);
		              var the_data_proses=json.d.data;
		               $items = [""];	
		              $.map(the_data_proses, function(data){
		                  var group;
		                  group = {
		                      id: data.id,
		                      name: data.nama,                            
		                      toString: function () {
		                    	  return JSON.stringify(this);
		                      },
		                      toLowerCase: function () {
		                          return this.name.toLowerCase();
		                      },
		                      indexOf: function (string) {
		                          return String.prototype.indexOf.apply(this.name, arguments);
		                      },
		                      replace: function (string) {
		                          var value = '';
		                          value +=  this.name;
		                          if(typeof(this.level) != 'undefined') {
		                              value += ' <span class="pull-right muted">';
		                              value += this.level;
		                              value += '</span>';
		                          }
		                          return String.prototype.replace.apply('<div class="typeaheadiv">' + value + '</div>', arguments);
		                      }
		                  };
		                  $items.push(group);
		              });
		              
		              process($items);
		            }
		          });
		        },
		        updater: function (item) {
		            var item = JSON.parse(item);
					$("#<?php echo $this->action; ?>_id_dokter").val(item.id);
					$("#<?php echo $this->action; ?>_kasus").focus();
		            return item.name;
		        }
		      });


			$('#<?php echo $this->action; ?>_nama_icd').typeahead({
				minLength:3,
		        source: function (query, process) {
			     var data_dokter=mr_icd.getViewData();
			     data_dokter["kriteria"]=$('#<?php echo $this->action; ?>_nama_icd').val();
		         var $items = new Array;
		           $items = [""];				                
		          $.ajax({
		            url: '',
		            type: 'POST',
		            data: data_dokter,
		            success: function(res) {
		              var json=getContent(res);
		              var the_data_proses=json.dbtable.data;
		               $items = [""];	
		              $.map(the_data_proses, function(data){
		                  var group;
		                  group = {
		                      id: data.id,
		                      name: data.nama,
		                      kode: data.icd,    
		                      sebab: data.sebab,                                
		                      toString: function () {
		                    	  return JSON.stringify(this);
		                      },
		                      toLowerCase: function () {
		                          return this.name.toLowerCase();
		                      },
		                      indexOf: function (string) {
		                          return String.prototype.indexOf.apply(this.name, arguments);
		                      },
		                      replace: function (string) {
		                          var value = '';
		                          value +=  this.name;
		                          if(typeof(this.level) != 'undefined') {
		                              value += ' <span class="pull-right muted">';
		                              value += this.level;
		                              value += '</span>';
		                          }
		                          return String.prototype.replace.apply('<div class="typeaheadiv">' + value + '</div>', arguments);
		                      }
		                  };
		                  $items.push(group);
		              });
		              
		              process($items);
		            }
		          });
		        },
		        updater: function (item) {
		            var item = JSON.parse(item);
					$("#<?php echo $this->action; ?>_kode_icd").val(item.kode);
					$("#<?php echo $this->action; ?>_sebab_sakit").val(item.sebab);
					$("#<?php echo $this->action; ?>_save").focus();
		            return item.name;
		        }
		      });
              
			$('#<?php echo $this->action; ?>_diagnosa_sekunder1').typeahead({
				minLength:3,
		        source: function (query, process) {
			     var data_dokter=mr_icd.getViewData();
			     data_dokter["kriteria"]=$('#<?php echo $this->action; ?>_diagnosa_sekunder1').val();
		         var $items = new Array;
		           $items = [""];				                
		          $.ajax({
		            url: '',
		            type: 'POST',
		            data: data_dokter,
		            success: function(res) {
		              var json=getContent(res);
		              var the_data_proses=json.dbtable.data;
		               $items = [""];	
		              $.map(the_data_proses, function(data){
		                  var group;
		                  group = {
		                      id: data.id,
		                      name: data.nama,
		                      kode: data.icd,    
		                      sebab: data.sebab,                                
		                      toString: function () {
		                    	  return JSON.stringify(this);
		                      },
		                      toLowerCase: function () {
		                          return this.name.toLowerCase();
		                      },
		                      indexOf: function (string) {
		                          return String.prototype.indexOf.apply(this.name, arguments);
		                      },
		                      replace: function (string) {
		                          var value = '';
		                          value +=  this.name;
		                          if(typeof(this.level) != 'undefined') {
		                              value += ' <span class="pull-right muted">';
		                              value += this.level;
		                              value += '</span>';
		                          }
		                          return String.prototype.replace.apply('<div class="typeaheadiv">' + value + '</div>', arguments);
		                      }
		                  };
		                  $items.push(group);
		              });
		              
		              process($items);
		            }
		          });
		        },
		        updater: function (item) {
		            var item = JSON.parse(item);
					$("#<?php echo $this->action; ?>_diagnosa_sekunder1").val(item.name);
		            return item.name;
		        }
		      });
              
            $('#<?php echo $this->action; ?>_diagnosa_sekunder2').typeahead({
				minLength:3,
		        source: function (query, process) {
			     var data_dokter=mr_icd.getViewData();
			     data_dokter["kriteria"]=$('#<?php echo $this->action; ?>_diagnosa_sekunder2').val();
		         var $items = new Array;
		           $items = [""];				                
		          $.ajax({
		            url: '',
		            type: 'POST',
		            data: data_dokter,
		            success: function(res) {
		              var json=getContent(res);
		              var the_data_proses=json.dbtable.data;
		               $items = [""];	
		              $.map(the_data_proses, function(data){
		                  var group;
		                  group = {
		                      id: data.id,
		                      name: data.nama,
		                      kode: data.icd,    
		                      sebab: data.sebab,                                
		                      toString: function () {
		                    	  return JSON.stringify(this);
		                      },
		                      toLowerCase: function () {
		                          return this.name.toLowerCase();
		                      },
		                      indexOf: function (string) {
		                          return String.prototype.indexOf.apply(this.name, arguments);
		                      },
		                      replace: function (string) {
		                          var value = '';
		                          value +=  this.name;
		                          if(typeof(this.level) != 'undefined') {
		                              value += ' <span class="pull-right muted">';
		                              value += this.level;
		                              value += '</span>';
		                          }
		                          return String.prototype.replace.apply('<div class="typeaheadiv">' + value + '</div>', arguments);
		                      }
		                  };
		                  $items.push(group);
		              });
		              
		              process($items);
		            }
		          });
		        },
		        updater: function (item) {
		            var item = JSON.parse(item);
					$("#<?php echo $this->action; ?>_diagnosa_sekunder2").val(item.name);
		            return item.name;
		        }
		      });
			
            $('#<?php echo $this->action; ?>_diagnosa_sekunder3').typeahead({
				minLength:3,
		        source: function (query, process) {
			     var data_dokter=mr_icd.getViewData();
			     data_dokter["kriteria"]=$('#<?php echo $this->action; ?>_diagnosa_sekunder3').val();
		         var $items = new Array;
		           $items = [""];				                
		          $.ajax({
		            url: '',
		            type: 'POST',
		            data: data_dokter,
		            success: function(res) {
		              var json=getContent(res);
		              var the_data_proses=json.dbtable.data;
		               $items = [""];	
		              $.map(the_data_proses, function(data){
		                  var group;
		                  group = {
		                      id: data.id,
		                      name: data.nama,
		                      kode: data.icd,    
		                      sebab: data.sebab,                                
		                      toString: function () {
		                    	  return JSON.stringify(this);
		                      },
		                      toLowerCase: function () {
		                          return this.name.toLowerCase();
		                      },
		                      indexOf: function (string) {
		                          return String.prototype.indexOf.apply(this.name, arguments);
		                      },
		                      replace: function (string) {
		                          var value = '';
		                          value +=  this.name;
		                          if(typeof(this.level) != 'undefined') {
		                              value += ' <span class="pull-right muted">';
		                              value += this.level;
		                              value += '</span>';
		                          }
		                          return String.prototype.replace.apply('<div class="typeaheadiv">' + value + '</div>', arguments);
		                      }
		                  };
		                  $items.push(group);
		              });
		              
		              process($items);
		            }
		          });
		        },
		        updater: function (item) {
		            var item = JSON.parse(item);
					$("#<?php echo $this->action; ?>_diagnosa_sekunder3").val(item.name);
		            return item.name;
		        }
		      });

            $('#<?php echo $this->action; ?>_diagnosa_sekunder4').typeahead({
				minLength:3,
		        source: function (query, process) {
			     var data_dokter=mr_icd.getViewData();
			     data_dokter["kriteria"]=$('#<?php echo $this->action; ?>_diagnosa_sekunder4').val();
		         var $items = new Array;
		           $items = [""];				                
		          $.ajax({
		            url: '',
		            type: 'POST',
		            data: data_dokter,
		            success: function(res) {
		              var json=getContent(res);
		              var the_data_proses=json.dbtable.data;
		               $items = [""];	
		              $.map(the_data_proses, function(data){
		                  var group;
		                  group = {
		                      id: data.id,
		                      name: data.nama,
		                      kode: data.icd,    
		                      sebab: data.sebab,                                
		                      toString: function () {
		                    	  return JSON.stringify(this);
		                      },
		                      toLowerCase: function () {
		                          return this.name.toLowerCase();
		                      },
		                      indexOf: function (string) {
		                          return String.prototype.indexOf.apply(this.name, arguments);
		                      },
		                      replace: function (string) {
		                          var value = '';
		                          value +=  this.name;
		                          if(typeof(this.level) != 'undefined') {
		                              value += ' <span class="pull-right muted">';
		                              value += this.level;
		                              value += '</span>';
		                          }
		                          return String.prototype.replace.apply('<div class="typeaheadiv">' + value + '</div>', arguments);
		                      }
		                  };
		                  $items.push(group);
		              });
		              
		              process($items);
		            }
		          });
		        },
		        updater: function (item) {
		            var item = JSON.parse(item);
					$("#<?php echo $this->action; ?>_diagnosa_sekunder4").val(item.name);
		            return item.name;
		        }
		      });

            $('#<?php echo $this->action; ?>_diagnosa_sekunder5').typeahead({
				minLength:3,
		        source: function (query, process) {
			     var data_dokter=mr_icd.getViewData();
			     data_dokter["kriteria"]=$('#<?php echo $this->action; ?>_diagnosa_sekunder5').val();
		         var $items = new Array;
		           $items = [""];				                
		          $.ajax({
		            url: '',
		            type: 'POST',
		            data: data_dokter,
		            success: function(res) {
		              var json=getContent(res);
		              var the_data_proses=json.dbtable.data;
		               $items = [""];	
		              $.map(the_data_proses, function(data){
		                  var group;
		                  group = {
		                      id: data.id,
		                      name: data.nama,
		                      kode: data.icd,    
		                      sebab: data.sebab,                                
		                      toString: function () {
		                    	  return JSON.stringify(this);
		                      },
		                      toLowerCase: function () {
		                          return this.name.toLowerCase();
		                      },
		                      indexOf: function (string) {
		                          return String.prototype.indexOf.apply(this.name, arguments);
		                      },
		                      replace: function (string) {
		                          var value = '';
		                          value +=  this.name;
		                          if(typeof(this.level) != 'undefined') {
		                              value += ' <span class="pull-right muted">';
		                              value += this.level;
		                              value += '</span>';
		                          }
		                          return String.prototype.replace.apply('<div class="typeaheadiv">' + value + '</div>', arguments);
		                      }
		                  };
		                  $items.push(group);
		              });
		              
		              process($items);
		            }
		          });
		        },
		        updater: function (item) {
		            var item = JSON.parse(item);
					$("#<?php echo $this->action; ?>_diagnosa_sekunder5").val(item.name);
		            return item.name;
		        }
		      });

            $('#<?php echo $this->action; ?>_diagnosa_external_cause').typeahead({
				minLength:3,
		        source: function (query, process) {
			     var data_dokter=mr_icd.getViewData();
			     data_dokter["kriteria"]=$('#<?php echo $this->action; ?>_diagnosa_external_cause').val();
		         var $items = new Array;
		           $items = [""];				                
		          $.ajax({
		            url: '',
		            type: 'POST',
		            data: data_dokter,
		            success: function(res) {
		              var json=getContent(res);
		              var the_data_proses=json.dbtable.data;
		               $items = [""];	
		              $.map(the_data_proses, function(data){
		                  var group;
		                  group = {
		                      id: data.id,
		                      name: data.nama,
		                      kode: data.icd,    
		                      sebab: data.sebab,                                
		                      toString: function () {
		                    	  return JSON.stringify(this);
		                      },
		                      toLowerCase: function () {
		                          return this.name.toLowerCase();
		                      },
		                      indexOf: function (string) {
		                          return String.prototype.indexOf.apply(this.name, arguments);
		                      },
		                      replace: function (string) {
		                          var value = '';
		                          value +=  this.name;
		                          if(typeof(this.level) != 'undefined') {
		                              value += ' <span class="pull-right muted">';
		                              value += this.level;
		                              value += '</span>';
		                          }
		                          return String.prototype.replace.apply('<div class="typeaheadiv">' + value + '</div>', arguments);
		                      }
		                  };
		                  $items.push(group);
		              });
		              
		              process($items);
		            }
		          });
		        },
		        updater: function (item) {
		            var item = JSON.parse(item);
					$("#<?php echo $this->action; ?>_diagnosa_external_cause").val(item.name);
		            return item.name;
		        }
		      });
              
            $('#<?php echo $this->action; ?>_diagnosa_tindakan').typeahead({
				minLength:3,
		        source: function (query, process) {
			     var data_dokter=mr_icd.getViewData();
			     data_dokter["kriteria"]=$('#<?php echo $this->action; ?>_diagnosa_tindakan').val();
		         var $items = new Array;
		           $items = [""];				                
		          $.ajax({
		            url: '',
		            type: 'POST',
		            data: data_dokter,
		            success: function(res) {
		              var json=getContent(res);
		              var the_data_proses=json.dbtable.data;
		               $items = [""];	
		              $.map(the_data_proses, function(data){
		                  var group;
		                  group = {
		                      id: data.id,
		                      name: data.nama,
		                      kode: data.icd,    
		                      sebab: data.sebab,                                
		                      toString: function () {
		                    	  return JSON.stringify(this);
		                      },
		                      toLowerCase: function () {
		                          return this.name.toLowerCase();
		                      },
		                      indexOf: function (string) {
		                          return String.prototype.indexOf.apply(this.name, arguments);
		                      },
		                      replace: function (string) {
		                          var value = '';
		                          value +=  this.name;
		                          if(typeof(this.level) != 'undefined') {
		                              value += ' <span class="pull-right muted">';
		                              value += this.level;
		                              value += '</span>';
		                          }
		                          return String.prototype.replace.apply('<div class="typeaheadiv">' + value + '</div>', arguments);
		                      }
		                  };
		                  $items.push(group);
		              });
		              
		              process($items);
		            }
		          });
		        },
		        updater: function (item) {
		            var item = JSON.parse(item);
					$("#<?php echo $this->action; ?>_diagnosa_tindakan").val(item.name);
		            return item.name;
		        }
		      });

            $('#<?php echo $this->action; ?>_diagnosa_tindakan2').typeahead({
				minLength:3,
		        source: function (query, process) {
			     var data_dokter=mr_icd.getViewData();
			     data_dokter["kriteria"]=$('#<?php echo $this->action; ?>_diagnosa_tindakan2').val();
		         var $items = new Array;
		           $items = [""];				                
		          $.ajax({
		            url: '',
		            type: 'POST',
		            data: data_dokter,
		            success: function(res) {
		              var json=getContent(res);
		              var the_data_proses=json.dbtable.data;
		               $items = [""];	
		              $.map(the_data_proses, function(data){
		                  var group;
		                  group = {
		                      id: data.id,
		                      name: data.nama,
		                      kode: data.icd,    
		                      sebab: data.sebab,                                
		                      toString: function () {
		                    	  return JSON.stringify(this);
		                      },
		                      toLowerCase: function () {
		                          return this.name.toLowerCase();
		                      },
		                      indexOf: function (string) {
		                          return String.prototype.indexOf.apply(this.name, arguments);
		                      },
		                      replace: function (string) {
		                          var value = '';
		                          value +=  this.name;
		                          if(typeof(this.level) != 'undefined') {
		                              value += ' <span class="pull-right muted">';
		                              value += this.level;
		                              value += '</span>';
		                          }
		                          return String.prototype.replace.apply('<div class="typeaheadiv">' + value + '</div>', arguments);
		                      }
		                  };
		                  $items.push(group);
		              });
		              
		              process($items);
		            }
		          });
		        },
		        updater: function (item) {
		            var item = JSON.parse(item);
					$("#<?php echo $this->action; ?>_diagnosa_tindakan2").val(item.name);
		            return item.name;
		        }
		      });

            $('#<?php echo $this->action; ?>_diagnosa_tindakan3').typeahead({
				minLength:3,
		        source: function (query, process) {
			     var data_dokter=mr_icd.getViewData();
			     data_dokter["kriteria"]=$('#<?php echo $this->action; ?>_diagnosa_tindakan3').val();
		         var $items = new Array;
		           $items = [""];				                
		          $.ajax({
		            url: '',
		            type: 'POST',
		            data: data_dokter,
		            success: function(res) {
		              var json=getContent(res);
		              var the_data_proses=json.dbtable.data;
		               $items = [""];	
		              $.map(the_data_proses, function(data){
		                  var group;
		                  group = {
		                      id: data.id,
		                      name: data.nama,
		                      kode: data.icd,    
		                      sebab: data.sebab,                                
		                      toString: function () {
		                    	  return JSON.stringify(this);
		                      },
		                      toLowerCase: function () {
		                          return this.name.toLowerCase();
		                      },
		                      indexOf: function (string) {
		                          return String.prototype.indexOf.apply(this.name, arguments);
		                      },
		                      replace: function (string) {
		                          var value = '';
		                          value +=  this.name;
		                          if(typeof(this.level) != 'undefined') {
		                              value += ' <span class="pull-right muted">';
		                              value += this.level;
		                              value += '</span>';
		                          }
		                          return String.prototype.replace.apply('<div class="typeaheadiv">' + value + '</div>', arguments);
		                      }
		                  };
		                  $items.push(group);
		              });
		              
		              process($items);
		            }
		          });
		        },
		        updater: function (item) {
		            var item = JSON.parse(item);
					$("#<?php echo $this->action; ?>_diagnosa_tindakan3").val(item.name);
		            return item.name;
		        }
		      });
              
            $('#<?php echo $this->action; ?>_diagnosa_kematian').typeahead({
				minLength:3,
		        source: function (query, process) {
			     var data_dokter=mr_icd.getViewData();
			     data_dokter["kriteria"]=$('#<?php echo $this->action; ?>_diagnosa_kematian').val();
		         var $items = new Array;
		           $items = [""];				                
		          $.ajax({
		            url: '',
		            type: 'POST',
		            data: data_dokter,
		            success: function(res) {
		              var json=getContent(res);
		              var the_data_proses=json.dbtable.data;
		               $items = [""];	
		              $.map(the_data_proses, function(data){
		                  var group;
		                  group = {
		                      id: data.id,
		                      name: data.nama,
		                      kode: data.icd,    
		                      sebab: data.sebab,                                
		                      toString: function () {
		                    	  return JSON.stringify(this);
		                      },
		                      toLowerCase: function () {
		                          return this.name.toLowerCase();
		                      },
		                      indexOf: function (string) {
		                          return String.prototype.indexOf.apply(this.name, arguments);
		                      },
		                      replace: function (string) {
		                          var value = '';
		                          value +=  this.name;
		                          if(typeof(this.level) != 'undefined') {
		                              value += ' <span class="pull-right muted">';
		                              value += this.level;
		                              value += '</span>';
		                          }
		                          return String.prototype.replace.apply('<div class="typeaheadiv">' + value + '</div>', arguments);
		                      }
		                  };
		                  $items.push(group);
		              });
		              
		              process($items);
		            }
		          });
		        },
		        updater: function (item) {
		            var item = JSON.parse(item);
					$("#<?php echo $this->action; ?>_diagnosa_kematian").val(item.name);
		            return item.name;
		        }
		      });
			
			mr_pasien=new TableAction("mr_pasien",mr_the_page,"<?php echo $this->action; ?>",new Array());
			mr_pasien.setSuperCommand("mr_pasien");
			mr_pasien.setPrototipe(mr_the_protoname,mr_the_protoslug,mr_the_protoimplement);
			mr_pasien.selected=function(json){
				var nama=json.nama_pasien;
				var nrm=json.nrm;
				var noreg=json.id;		
				var jk=json.kelamin;
                var tanggal = json.tanggal;
                var tgl = tanggal.split(" ");
                var tgl_masuk = tgl[0];
				$("#<?php echo $this->action; ?>_nama_pasien").val(nama);
				$("#<?php echo $this->action; ?>_profile_number").val(json.profile_number);
				$("#<?php echo $this->action; ?>_nrm_pasien").val(nrm);
				$("#<?php echo $this->action; ?>_noreg_pasien").val(noreg);
				$("#<?php echo $this->action; ?>_jk").val(jk);
				$("#<?php echo $this->action; ?>_alamat").val(json.alamat_pasien);
				$("#<?php echo $this->action; ?>_provinsi").val(json.nama_provinsi);
				$("#<?php echo $this->action; ?>_kabupaten").val(json.nama_kabupaten);
				$("#<?php echo $this->action; ?>_kecamatan").val(json.nama_kecamatan);
				$("#<?php echo $this->action; ?>_kelurahan").val(json.nama_kelurahan);
				$("#<?php echo $this->action; ?>_tgl_masuk").val(tgl_masuk);
                $("#<?php echo $this->action; ?>_tgl_lahir").val(json.tgl_lahir);
                $("#<?php echo $this->action; ?>_rujukan").val(json.rujukan);
			};
			
			mr_dokter=new TableAction("mr_dokter",mr_the_page,"<?php echo $this->action; ?>",new Array());
			mr_dokter.setSuperCommand("mr_dokter");
			mr_dokter.setPrototipe(mr_the_protoname,mr_the_protoslug,mr_the_protoimplement);
			mr_dokter.selected=function(json){
				var nama=json.nama;
				var nip=json.id;		
				$("#<?php echo $this->action; ?>_nama_dokter").val(nama);
				$("#<?php echo $this->action; ?>_id_dokter").val(nip);
			};

			mr_icd=new TableAction("mr_icd",mr_the_page,"<?php echo $this->action; ?>",new Array());
			mr_icd.setSuperCommand("mr_icd");
			mr_icd.setPrototipe(mr_the_protoname,mr_the_protoslug,mr_the_protoimplement);
			mr_icd.selected=function(json){
				var nama=json.nama;
				var kode=json.icd;		
				var sebab=json.sebab;
				$("#<?php echo $this->action; ?>_nama_icd").val(nama);
				$("#<?php echo $this->action; ?>_kode_icd").val(kode);
				$("#<?php echo $this->action; ?>_sebab_sakit").val(sebab);
			};
			
			mr_diagnosa_sekunder1=new TableAction("mr_diagnosa_sekunder1",mr_the_page,"<?php echo $this->action; ?>",new Array());
			mr_diagnosa_sekunder1.setSuperCommand("mr_diagnosa_sekunder1");
			mr_diagnosa_sekunder1.setPrototipe(mr_the_protoname,mr_the_protoslug,mr_the_protoimplement);
			mr_diagnosa_sekunder1.selected=function(json){
				var nama=json.nama;
				var kode=json.icd;
                $("#<?php echo $this->action; ?>_diagnosa_sekunder1").val(nama);
                $("#<?php echo $this->action; ?>_kode_icd_diagnosa_sekunder1").val(kode);
			};
            
            mr_diagnosa_sekunder2=new TableAction("mr_diagnosa_sekunder2",mr_the_page,"<?php echo $this->action; ?>",new Array());
			mr_diagnosa_sekunder2.setSuperCommand("mr_diagnosa_sekunder2");
			mr_diagnosa_sekunder2.setPrototipe(mr_the_protoname,mr_the_protoslug,mr_the_protoimplement);
			mr_diagnosa_sekunder2.selected=function(json){
				var nama=json.nama;
				var kode=json.icd;
                $("#<?php echo $this->action; ?>_diagnosa_sekunder2").val(nama);
                $("#<?php echo $this->action; ?>_kode_icd_diagnosa_sekunder2").val(kode);
			};
			
            mr_diagnosa_sekunder3=new TableAction("mr_diagnosa_sekunder3",mr_the_page,"<?php echo $this->action; ?>",new Array());
			mr_diagnosa_sekunder3.setSuperCommand("mr_diagnosa_sekunder3");
			mr_diagnosa_sekunder3.setPrototipe(mr_the_protoname,mr_the_protoslug,mr_the_protoimplement);
			mr_diagnosa_sekunder3.selected=function(json){
				var nama=json.nama;
				var kode=json.icd;
                $("#<?php echo $this->action; ?>_diagnosa_sekunder3").val(nama);
                $("#<?php echo $this->action; ?>_kode_icd_diagnosa_sekunder3").val(kode);
			};

			mr_diagnosa_sekunder4=new TableAction("mr_diagnosa_sekunder4",mr_the_page,"<?php echo $this->action; ?>",new Array());
			mr_diagnosa_sekunder4.setSuperCommand("mr_diagnosa_sekunder4");
			mr_diagnosa_sekunder4.setPrototipe(mr_the_protoname,mr_the_protoslug,mr_the_protoimplement);
			mr_diagnosa_sekunder4.selected=function(json){
				var nama=json.nama;
				var kode=json.icd;
                $("#<?php echo $this->action; ?>_diagnosa_sekunder4").val(nama);
                $("#<?php echo $this->action; ?>_kode_icd_diagnosa_sekunder4").val(kode);
			};

			mr_diagnosa_sekunder5=new TableAction("mr_diagnosa_sekunder5",mr_the_page,"<?php echo $this->action; ?>",new Array());
			mr_diagnosa_sekunder5.setSuperCommand("mr_diagnosa_sekunder5");
			mr_diagnosa_sekunder5.setPrototipe(mr_the_protoname,mr_the_protoslug,mr_the_protoimplement);
			mr_diagnosa_sekunder5.selected=function(json){
				var nama=json.nama;
				var kode=json.icd;
                $("#<?php echo $this->action; ?>_diagnosa_sekunder5").val(nama);
                $("#<?php echo $this->action; ?>_kode_icd_diagnosa_sekunder5").val(kode);
			};

			mr_diagnosa_external_cause=new TableAction("mr_diagnosa_external_cause",mr_the_page,"<?php echo $this->action; ?>",new Array());
			mr_diagnosa_external_cause.setSuperCommand("mr_diagnosa_external_cause");
			mr_diagnosa_external_cause.setPrototipe(mr_the_protoname,mr_the_protoslug,mr_the_protoimplement);
			mr_diagnosa_external_cause.selected=function(json){
				var nama=json.nama;
				var kode=json.icd;
				var sebab=json.sebab;
                $("#<?php echo $this->action; ?>_diagnosa_external_cause").val(nama);
                $("#<?php echo $this->action; ?>_kode_icd_diagnosa_external_cause").val(kode);
				$("#<?php echo $this->action; ?>_sebab_diagnosa_external_cause").val(sebab);
			};

			mr_diagnosa_tindakan=new TableAction("mr_diagnosa_tindakan",mr_the_page,"<?php echo $this->action; ?>",new Array());
			mr_diagnosa_tindakan.setSuperCommand("mr_diagnosa_tindakan");
			mr_diagnosa_tindakan.setPrototipe(mr_the_protoname,mr_the_protoslug,mr_the_protoimplement);
			mr_diagnosa_tindakan.selected=function(json){
				var nama=json.nama;
                $("#<?php echo $this->action; ?>_diagnosa_tindakan").val(nama);
			};

			mr_diagnosa_tindakan2=new TableAction("mr_diagnosa_tindakan2",mr_the_page,"<?php echo $this->action; ?>",new Array());
			mr_diagnosa_tindakan2.setSuperCommand("mr_diagnosa_tindakan2");
			mr_diagnosa_tindakan2.setPrototipe(mr_the_protoname,mr_the_protoslug,mr_the_protoimplement);
			mr_diagnosa_tindakan2.selected=function(json){
				var nama=json.nama;
                $("#<?php echo $this->action; ?>_diagnosa_tindakan2").val(nama);
			};

			mr_diagnosa_tindakan3=new TableAction("mr_diagnosa_tindakan3",mr_the_page,"<?php echo $this->action; ?>",new Array());
			mr_diagnosa_tindakan3.setSuperCommand("mr_diagnosa_tindakan3");
			mr_diagnosa_tindakan3.setPrototipe(mr_the_protoname,mr_the_protoslug,mr_the_protoimplement);
			mr_diagnosa_tindakan3.selected=function(json){
				var nama=json.nama;
                $("#<?php echo $this->action; ?>_diagnosa_tindakan3").val(nama);
			};

			mr_diagnosa_kematian=new TableAction("mr_diagnosa_kematian",mr_the_page,"<?php echo $this->action; ?>",new Array());
			mr_diagnosa_kematian.setSuperCommand("mr_diagnosa_kematian");
			mr_diagnosa_kematian.setPrototipe(mr_the_protoname,mr_the_protoslug,mr_the_protoimplement);
			mr_diagnosa_kematian.selected=function(json){
				var nama=json.nama;
				var kode=json.icd;
                $("#<?php echo $this->action; ?>_diagnosa_kematian").val(nama);
                $("#<?php echo $this->action; ?>_kode_icd_diagnosa_kematian").val(kode);
			};

			mr_diagnosa_icd=new TableAction("mr_diagnosa_icd",mr_the_page,"<?php echo $this->action; ?>",new Array());
			mr_diagnosa_icd.setSuperCommand("mr_diagnosa_icd");
			mr_diagnosa_icd.setPrototipe(mr_the_protoname,mr_the_protoslug,mr_the_protoimplement);
			mr_diagnosa_icd.selected=function(json){
				var nama=json.nama;
				var kode=json.icd;		
				var sebab=json.sebab;
				$("#<?php echo $this->action; ?>_diagnosa").val(nama);
				$("#<?php echo $this->action; ?>_nama_icd").val(nama);
				$("#<?php echo $this->action; ?>_kode_icd").val(kode);
				$("#<?php echo $this->action; ?>_sebab_sakit").val(sebab);
			};
			
			<?php if (getSettings($this->db,"smis-rs-igd-diagnosa-icd","0") == "1") { ?>
			stypeahead("#<?php echo $this->action; ?>_diagnosa",3,mr_diagnosa_icd,"nama",function(json){
				var nama=json.nama;
				var kode=json.icd;		
				var sebab=json.sebab;
				$("#<?php echo $this->action; ?>_diagnosa").val(nama);
				$("#<?php echo $this->action; ?>_nama_icd").val(nama);
				$("#<?php echo $this->action; ?>_kode_icd").val(kode);
				$("#<?php echo $this->action; ?>_sebab_sakit").val(sebab);
			});
			<?php } ?>
					
			var column=new Array("id",
                                "tanggal",
                                "jk",
                                "kunjungan",
                                "carabayar",
                                "kasus",
                                "ruangan",
                                "nama_pasien",
                                "sebutan",
                                "noreg_pasien",
                                "tgl_lahir",
                                "nrm_pasien",
                                "profile_number",
                                "diagnosa",
                                'keterangan',
                                "diagnosa_sekunder1",
                                "kode_icd_diagnosa_sekunder1",
                                "diagnosa_sekunder2",
                                "kode_icd_diagnosa_sekunder2",
                                "diagnosa_sekunder3",
                                "kode_icd_diagnosa_sekunder3",
                                "diagnosa_sekunder4",
                                "kode_icd_diagnosa_sekunder4",
                                "diagnosa_sekunder5",
                                "kode_icd_diagnosa_sekunder5",
                                "diagnosa_external_cause",
                                "kode_icd_diagnosa_external_cause",
								"sebab_diagnosa_external_cause",
                                "diagnosa_tindakan",
                                "diagnosa_tindakan2",
                                "diagnosa_tindakan3",
								"diagnosa_kematian_manual",
                                "diagnosa_kematian",
                                "kode_icd_diagnosa_kematian",
                                "id_dokter",
                                "nama_dokter",
                                "kode_icd",
                                "nama_icd",
                                "gol_umur",
                                "sebab_sakit",
                                "tensi",
                                "nadi",
                                "suhu",
                                "rr",
                                "nyeri",
                                "keadaan_umum",
                                "keadaan_luka",
                                "gula_darah",
                                "berat_badan",
                                "urji",
                                "tgl_masuk",
                                "propinsi",
                                "kabupaten",
                                "kecamatan",
                                "kelurahan",
                                "alamat",
                                "rujukan"
                                );
			<?php echo $this->action; ?>=new TableAction("<?php echo $this->action; ?>",mr_the_page,"<?php echo $this->action; ?>",column);
			<?php echo $this->action; ?>.setPrototipe(mr_the_protoname,mr_the_protoslug,mr_the_protoimplement);
			<?php echo $this->action; ?>.setEnableAutofocus(true);
			<?php echo $this->action; ?>.setNextEnter();
			<?php echo $this->action; ?>.getRegulerData=function(){
				var reg_data={	
						page:this.page,
						action:this.action,
						super_command:this.super_command,
						prototype_name:this.prototype_name,
						prototype_slug:this.prototype_slug,
						prototype_implement:this.prototype_implement,
						polislug:mr_polislug,
						noreg_pasien:mr_noreg,
						nama_pasien:mr_nama_pasien,
						nrm_pasien:mr_nrm_pasien,
						jk:$("#<?php echo $this->action; ?>_jk").val()
						};
				return reg_data;
			};
			<?php echo $this->action; ?>.view();
			<?php echo $this->action; ?>.show=function(id){
				var data=$("#diagnosa_data_"+id).html();
				showWarning("Diagnosa ", data);
			};
            
            <?php echo $this->action; ?>.setEditClearForNoClear(true);
            <?php echo $this->action; ?>.addNoClear("id_dokter");
            <?php echo $this->action; ?>.addNoClear("nama_dokter");
            <?php echo $this->action; ?>.setEnableAutofocus(true);
            <?php echo $this->action; ?>.setNextEnter();
            <?php echo $this->action; ?>.setMultipleInput(true);
			
		});
		</script>
<?php
	}
	
	public function superCommand($super_command) {
        $super = new SuperCommand ();
        if($super_command == 'mr_dokter') {
            $array=array ('Nama','Jabatan',"NIP" );
            $dktable = new Table ($array);
            $dktable->setName ( "mr_dokter" );
            $dktable->setModel ( Table::$SELECT );
            $dkadapter = new SimpleAdapter ();
            $dkadapter->add ( "Jabatan", "nama_jabatan" );
            $dkadapter->add ( "Nama", "nama" );
            $dkadapter->add ( "NIP", "nip" );
            $dkresponder = new EmployeeResponder($this->db, $dktable, $dkadapter, "dokter") ;
            $super->addResponder ( "mr_dokter", $dkresponder );
        }
        if($super_command == 'mr_pasien') {
            /* PASIEN */
            $aname=array ('Sebutan','Nama','NRM','No. Profile',"No Reg", "Tgl Masuk" );
            $ptable = new Table ( $aname);
            $ptable->setName ( "mr_pasien" );
            $ptable->setModel ( Table::$SELECT );
            $padapter = new SimpleAdapter ();
            $padapter->add ( "Sebutan", "sebutan" );
            $padapter->add ( "Nama", "nama_pasien" );
            $padapter->add ( "NRM", "nrm", "digit8" );
            $padapter->add ( "No. Profile", "profile_number", "digit8" );
            $padapter->add ( "No Reg", "id" );
            $padapter->add ( "Tgl Masuk", "tanggal", "date d M Y" );
            $presponder = new ServiceResponder ( $this->db, $ptable, $padapter, "get_registered_all" );
            $super->addResponder ( "mr_pasien", $presponder );
        }
        if($super_command == 'mr_icd') {
            $array=array ("Kode",'DTD','Nama',"Sebab" );
            $muitable = new Table ( $array);
            $muitable->setModel ( Table::$SELECT );
            $muitable->setName ( "mr_icd" );
            $madapter = new SimpleAdapter ();
            $madapter->add ( "Nama", "nama" );
            $madapter->add ( "Kode", "icd" );
            $madapter->add ( "DTD", "dtd" );
            $madapter->add ( "Sebab", "sebab" );
            $dbtable = new DBTable ( $this->db, "smis_mr_icd" );
            $mresponder = new DBResponder ( $dbtable, $muitable, $madapter );
            $super->addResponder ( "mr_icd", $mresponder );
        }
        if($super_command == 'mr_diagnosa_sekunder1') {
            $array=array ("Kode",'DTD','Nama',"Sebab" );
            $muitable = new Table ( $array);
            $muitable->setModel ( Table::$SELECT );
            $muitable->setName ( "mr_diagnosa_sekunder1" );
            $madapter = new SimpleAdapter ();
            $madapter->add ( "Nama", "nama" );
            $madapter->add ( "Kode", "icd" );
            $madapter->add ( "DTD", "dtd" );
            $madapter->add ( "Sebab", "sebab" );
            $dbtable = new DBTable ( $this->db, "smis_mr_icd" );
            $mresponder = new DBResponder ( $dbtable, $muitable, $madapter );
            $super->addResponder ( "mr_diagnosa_sekunder1", $mresponder );
        }
        if($super_command == 'mr_diagnosa_sekunder2') {
            $array=array ("Kode",'DTD','Nama',"Sebab" );
            $muitable = new Table ( $array);
            $muitable->setModel ( Table::$SELECT );
            $muitable->setName ( "mr_diagnosa_sekunder2" );
            $madapter = new SimpleAdapter ();
            $madapter->add ( "Nama", "nama" );
            $madapter->add ( "Kode", "icd" );
            $madapter->add ( "DTD", "dtd" );
            $madapter->add ( "Sebab", "sebab" );
            $dbtable = new DBTable ( $this->db, "smis_mr_icd" );
            $mresponder = new DBResponder ( $dbtable, $muitable, $madapter );
            $super->addResponder ( "mr_diagnosa_sekunder2", $mresponder );
        }
        if($super_command == 'mr_diagnosa_sekunder3') {
            $array=array ("Kode",'DTD','Nama',"Sebab" );
            $muitable = new Table ( $array);
            $muitable->setModel ( Table::$SELECT );
            $muitable->setName ( "mr_diagnosa_sekunder3" );
            $madapter = new SimpleAdapter ();
            $madapter->add ( "Nama", "nama" );
            $madapter->add ( "Kode", "icd" );
            $madapter->add ( "DTD", "dtd" );
            $madapter->add ( "Sebab", "sebab" );
            $dbtable = new DBTable ( $this->db, "smis_mr_icd" );
            $mresponder = new DBResponder ( $dbtable, $muitable, $madapter );
            $super->addResponder ( "mr_diagnosa_sekunder3", $mresponder );
        }
        if($super_command == 'mr_diagnosa_sekunder4') {
            $array=array ("Kode",'DTD','Nama',"Sebab" );
            $muitable = new Table ( $array);
            $muitable->setModel ( Table::$SELECT );
            $muitable->setName ( "mr_diagnosa_sekunder4" );
            $madapter = new SimpleAdapter ();
            $madapter->add ( "Nama", "nama" );
            $madapter->add ( "Kode", "icd" );
            $madapter->add ( "DTD", "dtd" );
            $madapter->add ( "Sebab", "sebab" );
            $dbtable = new DBTable ( $this->db, "smis_mr_icd" );
            $mresponder = new DBResponder ( $dbtable, $muitable, $madapter );
            $super->addResponder ( "mr_diagnosa_sekunder4", $mresponder );
        }
        if($super_command == 'mr_diagnosa_sekunder5') {
            $array=array ("Kode",'DTD','Nama',"Sebab" );
            $muitable = new Table ( $array);
            $muitable->setModel ( Table::$SELECT );
            $muitable->setName ( "mr_diagnosa_sekunder5" );
            $madapter = new SimpleAdapter ();
            $madapter->add ( "Nama", "nama" );
            $madapter->add ( "Kode", "icd" );
            $madapter->add ( "DTD", "dtd" );
            $madapter->add ( "Sebab", "sebab" );
            $dbtable = new DBTable ( $this->db, "smis_mr_icd" );
            $mresponder = new DBResponder ( $dbtable, $muitable, $madapter );
            $super->addResponder ( "mr_diagnosa_sekunder5", $mresponder );
        }
        if($super_command == 'mr_diagnosa_external_cause') {
            $array=array ("Kode",'DTD','Nama',"Sebab" );
            $muitable = new Table ( $array);
            $muitable->setModel ( Table::$SELECT );
            $muitable->setName ( "mr_diagnosa_external_cause" );
            $madapter = new SimpleAdapter ();
            $madapter->add ( "Nama", "nama" );
            $madapter->add ( "Kode", "icd" );
            $madapter->add ( "DTD", "dtd" );
            $madapter->add ( "Sebab", "sebab" );
            $dbtable = new DBTable ( $this->db, "smis_mr_icd" );
            $mresponder = new DBResponder ( $dbtable, $muitable, $madapter );
            $super->addResponder ( "mr_diagnosa_external_cause", $mresponder );
        }
        if($super_command == 'mr_diagnosa_tindakan') {
            $array=array ("Kode",'DTD','Nama',"Sebab" );
            $muitable = new Table ( $array);
            $muitable->setModel ( Table::$SELECT );
            $muitable->setName ( "mr_diagnosa_tindakan" );
            $madapter = new SimpleAdapter ();
            $madapter->add ( "Nama", "nama" );
            $madapter->add ( "Kode", "icd" );
            $madapter->add ( "DTD", "dtd" );
            $madapter->add ( "Sebab", "sebab" );
            $dbtable = new DBTable ( $this->db, "smis_mr_icdtindakan" );
            $mresponder = new DBResponder ( $dbtable, $muitable, $madapter );
            $super->addResponder ( "mr_diagnosa_tindakan", $mresponder );
        }
        if($super_command == 'mr_diagnosa_tindakan2') {
            $array=array ("Kode",'DTD','Nama',"Sebab" );
            $muitable = new Table ( $array);
            $muitable->setModel ( Table::$SELECT );
            $muitable->setName ( "mr_diagnosa_tindakan2" );
            $madapter = new SimpleAdapter ();
            $madapter->add ( "Nama", "nama" );
            $madapter->add ( "Kode", "icd" );
            $madapter->add ( "DTD", "dtd" );
            $madapter->add ( "Sebab", "sebab" );
            $dbtable = new DBTable ( $this->db, "smis_mr_icdtindakan" );
            $mresponder = new DBResponder ( $dbtable, $muitable, $madapter );
            $super->addResponder ( "mr_diagnosa_tindakan2", $mresponder );
        }
        if($super_command == 'mr_diagnosa_tindakan3') {
            $array=array ("Kode",'DTD','Nama',"Sebab" );
            $muitable = new Table ( $array);
            $muitable->setModel ( Table::$SELECT );
            $muitable->setName ( "mr_diagnosa_tindakan3" );
            $madapter = new SimpleAdapter ();
            $madapter->add ( "Nama", "nama" );
            $madapter->add ( "Kode", "icd" );
            $madapter->add ( "DTD", "dtd" );
            $madapter->add ( "Sebab", "sebab" );
            $dbtable = new DBTable ( $this->db, "smis_mr_icdtindakan" );
            $mresponder = new DBResponder ( $dbtable, $muitable, $madapter );
            $super->addResponder ( "mr_diagnosa_tindakan3", $mresponder );
        }
        if($super_command == 'mr_diagnosa_kematian') {
            $array=array ("Kode",'DTD','Nama',"Sebab" );
            $muitable = new Table ( $array);
            $muitable->setModel ( Table::$SELECT );
            $muitable->setName ( "mr_diagnosa_kematian" );
            $madapter = new SimpleAdapter ();
            $madapter->add ( "Nama", "nama" );
            $madapter->add ( "Kode", "icd" );
            $madapter->add ( "DTD", "dtd" );
            $madapter->add ( "Sebab", "sebab" );
            $dbtable = new DBTable ( $this->db, "smis_mr_icd" );
            $mresponder = new DBResponder ( $dbtable, $muitable, $madapter );
            $super->addResponder ( "mr_diagnosa_kematian", $mresponder );
        }
		$init = $super->initialize ();
		if ($init != null) {
			echo $init;
			return;
		}
	}
    
}

?>