<?php
require_once 'smis-framework/smis/template/ModulTemplate.php';
require_once ("smis-base/smis-include-service-consumer.php");
require_once "medical_record/class/service/RuanganService.php";
class IndikatorTemplate extends ModulTemplate {
	protected $db;
	protected $polislug;
	protected $page;
	protected $protoslug;
	protected $protoname;
	protected $protoimplement;
	protected $action;
	protected $dbtable;
	protected $uitable;
	protected $noreg_pasien;
	protected $nama_pasien;
	protected $nrm_pasien;
	public function __construct($db, $polislug = "all", $noreg = "", $nrm = "", $nama = "", $page = "medical_record", $action = "iklin", $protoslug = "", $protoname = "", $protoimplement = "") {
		$this->db = $db;
		$this->noreg_pasien = $noreg;
		$this->nama_pasien = $nama;
		$this->nrm_pasien = $nrm;
		$this->polislug = $polislug;
		$this->dbtable = new DBTable ( $this->db, "smis_mr_iklin" );
		$this->page = $page;
		$this->protoslug = $protoslug;
		$this->protoimplement = $protoimplement;
		$this->protoname = $protoname;
		$this->action = $action;
		if ($polislug != "all")
			$this->dbtable->addCustomKriteria ( "ruangan", "='" . $polislug . "'" );
		if ($noreg != "")
			$this->dbtable->addCustomKriteria ( "noreg_pasien", "='" . $noreg . "'" );
		$this->uitable = new Table ( array (
				'NRM',
				"Pasien",
				"No Register" 
		), " Indikator " . ($this->polislug == "all" ? "" : ucfirst ( $this->protoname )), NULL, true );
		$this->uitable->setName ( $action );
		
		if ($this->page == "medical_record" && $this->action == "iklin") {
			$this->uitable->setAddButtonEnable ( false );
			$this->uitable->setDelButtonEnable ( false );
		}
	}
	public function command($command) {
		$adapter = new SimpleAdapter ();
		$adapter->add ( "Pasien", "nama_pasien" );
		$adapter->add ( "NRM", "nrm_pasien", "digit8" );
		$adapter->add ( "No Register", "noreg_pasien", "digit8" );
		$dbres = new DBResponder ( $this->dbtable, $this->uitable, $adapter );
		/*
		 * if($dbres->is("save")){ $e['noreg_pasien']=$_POST['noreg_pasien']; $e['ruangan']=$_POST['ruangan']; if($this->dbtable->is_exist($e)){ $row=$this->dbtable->select($e); $id=$row->id; $data=$dbres->command($_POST['command']); } }
		 */
		$data = $dbres->command ( $_POST ['command'] );
		echo json_encode ( $data );
		return;
	}
	public function phpPreLoad() {
		$service = new RuanganService ( $this->db );
		$service->execute ();
		$ruangan = $service->getContent ();
		
		$transfusi_kasus = array (
				array (
						"name" => "" 
				),
				array () 
		);
		
		if ($this->noreg_pasien != "") {
			$this->uitable->addModal ( "nama_pasien", "hidden", "", $this->nama_pasien, "n", null, true );
			$this->uitable->addModal ( "nrm_pasien", "hidden", "", $this->nrm_pasien, "n", null, true );
			$this->uitable->addModal ( "noreg_pasien", "hidden", "", $this->noreg_pasien, "n", null, true );
		} else {
			$this->uitable->addModal ( "nama_pasien", "chooser-" . $this->action . "-ik_pasien", "Pasien", $this->nama_pasien, "n", null, true );
			$this->uitable->addModal ( "nrm_pasien", "text", "NRM", $this->nrm_pasien, "n", null, true );
			$this->uitable->addModal ( "noreg_pasien", "text", "No Reg", $this->noreg_pasien, "n", null, true );
		}
		
		if ($this->polislug == "all")
			$this->uitable->addModal ( "ruangan", "select", "Asal Ruangan", $ruangan );
		else
			$this->uitable->addModal ( "ruangan", "hidden", "", $this->polislug, "n", null, true );
		
		$the_row = array ();
		$the_row ['decubitus_baring'] = false;
		$the_row ['decubitus_kapan'] = "";
		$the_row ['decubitus'] = false;
		$the_row ['decubitus_ket'] = "";
		$the_row ['infus_pasang'] = false;
		$the_row ['infus_kapan'] = "";
		$the_row ['infus_infeksi'] = false;
		$the_row ['infus_ket'] = "";
		$the_row ['transfusi_darah'] = false;
		$the_row ['transfusi_kapan'] = "";
		$the_row ['transfusi_kasus'] = "";
		$the_row ['transfusi_wb'] = false;
		$the_row ['transfusi_prc'] = false;
		$the_row ['transfusi_plasma'] = false;
		$the_row ['transfusi_lain'] = false;
		$the_row ['transfusi_guna'] = "";
		$the_row ['transfusi_infeksi'] = false;
		$the_row ['transfusi_ket'] = "";
		$the_row ['isk_pasang'] = false;
		$the_row ['isk_kapan'] = "";
		$the_row ['isk_infeksi'] = false;
		$the_row ['isk_ket'] = "";
		$the_row ['operasi_infeksi'] = false;
		$the_row ['operasi_kapan'] = "";
		$the_row ['operasi_jenis'] = false;
		$the_row ['operasi_ket'] = "";
		$the_row ['elektif_operasi'] = false;
		$the_row ['elektif_infeksi'] = false;
		$the_row ['elektif_kapan'] = "";
		$the_row ['elektif_tunggu'] = "0";
		$the_row ['elektif_ket'] = "";
		$the_row ["bayi_kurang"] = false;
		$the_row ["bayi_kapan"] = "";
		$the_row ["bayi_hidup"] = true;
		$the_row ["bayi_sebab"] = "";
		$the_row ["bayi_rujukan"] = false;
		$the_row ['sentinel'] = false;
		$the_row ['sentinel_kapan'] = "";
		$the_row ['sentinel_keterangan'] = "";
		$the_row ['cedera'] = false;
		$the_row ['cedera_kapan'] = "";
		$the_row ['cedera_keterangan'] = "";
		$the_row ['nyaris'] = false;
		$the_row ['nyaris_kapan'] = "";
		$the_row ['nyaris_keterangan'] = "";		
		$the_row ["id"] = "";
		
		$exist ['noreg_pasien'] = $this->noreg_pasien;
		$exist ['nama_pasien'] = $this->nama_pasien;
		$exist ['nrm_pasien'] = $this->nrm_pasien;
		$exist ['ruangan'] = $this->polislug;
		if ($this->dbtable->is_exist ( $exist )) {
			$row = $this->dbtable->select ( $exist );
			$the_row ['decubitus_baring'] = $row->decubitus_baring == "1";
			$the_row ['decubitus_kapan'] = $row->decubitus_kapan;
			$the_row ['decubitus'] = $row->decubitus == "1";
			$the_row ['decubitus_ket'] = $row->decubitus_ket;
			$the_row ['infus_pasang'] = $row->infus_pasang == "1";
			$the_row ['infus_kapan'] = $row->infus_kapan;
			$the_row ['infus_infeksi'] = $row->infus_infeksi == "1";
			$the_row ['infus_ket'] = $row->infus_ket;
			$the_row ['transfusi_darah'] = $row->transfusi_darah == "1";
			$the_row ['transfusi_kapan'] = $row->transfusi_kapan;
			$the_row ['transfusi_kasus'] = $row->transfusi_kasus;
			$the_row ['transfusi_wb'] = $row->transfusi_wb;
			$the_row ['transfusi_prc'] = $row->transfusi_prc;
			$the_row ['transfusi_plasma'] = $row->transfusi_plasma;
			$the_row ['transfusi_lain'] = $row->transfusi_lain;
			$the_row ['transfusi_guna'] = $row->transfusi_guna;
			$the_row ['transfusi_infeksi'] = $row->transfusi_infeksi == "1";
			$the_row ['transfusi_ket'] = $row->transfusi_ket;
			$the_row ['isk_pasang'] = $row->isk_pasang =="1";
			$the_row ['isk_kapan'] = $row->isk_kapan;
			$the_row ['isk_infeksi'] = $row->isk_infeksi=="1";
			$the_row ['isk_ket'] = $row->isk_ket;
			
			$the_row ['operasi_infeksi'] = $row->operasi_infeksi == "1";
			$the_row ['operasi_kapan'] = $row->operasi_kapan;
			$the_row ['operasi_jenis'] = $row->operasi_jenis;
			$the_row ['operasi_ket'] = $row->operasi_ket;
			$the_row ['elektif_operasi'] = $row->elektif_operasi == "1";
			$the_row ['elektif_kapan'] = $row->elektif_kapan;
			$the_row ['elektif_tunggu'] = $row->elektif_tunggu;
			$the_row ['elektif_ket'] = $row->elektif_ket;
			$the_row ["bayi_kurang"] = $row->bayi_kurang == "1";
			$the_row ["bayi_kapan"] = $row->bayi_kapan;
			$the_row ["bayi_hidup"] = $row->bayi_hidup == "1";
			$the_row ["bayi_sebab"] = $row->bayi_sebab;
			$the_row ["bayi_rujukan"] = $row->bayi_rujukan == "1";
			
			$the_row ['sentinel'] = $row->sentinel== "1";
			$the_row ['sentinel_kapan'] = $row->sentinel_kapan;
			$the_row ['sentinel_keterangan'] = $row->sentinel_keterangan;
			$the_row ['nyaris'] = $row->nyaris== "1";
			$the_row ['nyaris_kapan'] = $row->nyaris_kapan;
			$the_row ['nyaris_keterangan'] = $row->nyaris_keterangan;
			$the_row ['cedera'] = $row->cedera== "1";
			$the_row ['cedera_kapan'] = $row->cedera_kapan;
			$the_row ['cedera_keterangan'] = $row->cedera_keterangan;
				
			$the_row ["id"] = $row->id;
		} else {
			$this->dbtable->insert ( $exist );
			$the_row ["id"] = $this->dbtable->get_inserted_id ();
		}
		
		$guna_darah = array (
				array (
						"name" => "Kasus Obgyn",
						"value" => "obgyn",
						"default" => $the_row ['trasnfusi_guna'] == "obgyn" ? "1" : "0" 
				),
				array (
						"name" => "Kasus Neonatal",
						"value" => "neonatal",
						"default" => $the_row ['trasnfusi_guna'] == "neonatal" ? "1" : "0" 
				),
				array (
						"name" => "Kasus Bedah",
						"value" => "bedah",
						"default" => $the_row ['trasnfusi_guna'] == "bedah" ? "1" : "0" 
				),
				array (
						"name" => "Kasus Dalam",
						"value" => "dalam",
						"default" => $the_row ['trasnfusi_guna'] == "dalam" ? "1" : "0" 
				),
				array (
						"name" => "Kasus Lain-Lain",
						"value" => "lain",
						"default" => $the_row ['trasnfusi_guna'] == "lain" ? "1" : "0" 
				) 
		);
		
		$this->uitable->addModal ( "id", "hidden", "", $the_row ["id"] );
		$this->uitable->addModal ( "bayi_hidup", "checkbox-ya", "Apakah Pasien Hidup ?", $the_row ["bayi_hidup"] );
		$this->uitable->addModal ( "", "label", "<strong>DECUBITUS</strong>", "" );
		$this->uitable->addModal ( "decubitus_baring", "checkbox-ya", "Apakah Pasien Baring Total ? ", $the_row ["decubitus_baring"] );
		$this->uitable->addModal ( "decubitus_kapan", "date", "Kapan Baring Total ?", $the_row ["decubitus_kapan"] );
		$this->uitable->addModal ( "decubitus", "checkbox-ya", "Apakah Terkena Decubitus ?", $the_row ["decubitus"] );
		$this->uitable->addModal ( "decubitus_ket", "textarea", "Keterangan !", $the_row ["decubitus_ket"] );
		
		$this->uitable->addModal ( "", "label", "<strong>INFEKSI JARUM INFUS</strong>", "" );
		$this->uitable->addModal ( "infus_pasang", "checkbox-ya", "Apakah dipasang Infus ? ", $the_row ["infus_pasang"] );
		$this->uitable->addModal ( "infus_kapan", "date", "Kapan Dipasang Infus ?", $the_row ["infus_kapan"] );
		$this->uitable->addModal ( "infus_infeksi", "checkbox-ya", "Apakah Mengalami Infeksi ?", $the_row ["infus_infeksi"] );
		$this->uitable->addModal ( "infus_ket", "textarea", "Keterangan !", $the_row ["infus_ket"] );
		
		$this->uitable->addModal ( "", "label", "<strong>INFEKSI TRANSFUSI DARAH</strong>", "" );
		$this->uitable->addModal ( "transfusi_darah", "checkbox-ya", "Apakah Tranfusi Darah? ", $the_row ["transfusi_darah"] );
		$this->uitable->addModal ( "transfusi_kapan", "date", "Kapan Transfusi Darah ?", $the_row ["transfusi_kapan"] );
		$this->uitable->addModal ( "transfusi_guna", "select", "Digunakan Untuk ?", $guna_darah );
		$this->uitable->addModal ( "transfusi_wb", "text", " 'Whole Blood' Berapa Kantong ?", $the_row ["transfusi_wb"],"y","numeric" );
		$this->uitable->addModal ( "transfusi_prc", "text", " 'PRC' Berapa Kantong ?", $the_row ["transfusi_prc"] ,"y","numeric");
		$this->uitable->addModal ( "transfusi_plasma", "text", " 'Plasma' Berapa Kantong ?", $the_row ["transfusi_plasma"] ,"y","numeric");
		$this->uitable->addModal ( "transfusi_lain", "text", " Komp. Darah Lain Berapa Kantong ?", $the_row ["transfusi_lain"] ,"y","numeric");
		$this->uitable->addModal ( "transfusi_infeksi", "checkbox-ya", "Apakah Mengalami Infeksi ?", $the_row ["transfusi_infeksi"] );
		$this->uitable->addModal ( "transfusi_ket", "textarea", "Keterangan !", $the_row ["transfusi_ket"] );
		
		$this->uitable->addModal ( "", "label", "<strong>PASIEN OPERASI ELEKTIF</strong>", "" );
		$this->uitable->addModal ( "elektif_operasi", "checkbox-ya", "Apakah Operasi Elektif ? ", $the_row ["elektif_operasi"] );
		$this->uitable->addModal ( "elektif_kapan", "date", "Kapan Operasi Elektif ?", $the_row ["elektif_kapan"] );
		$this->uitable->addModal ( "elektif_tunggu", "text", "Berapa lama Waktu Tunggu ?", $the_row ["elektif_tunggu"] );
		$this->uitable->addModal ( "elektif_ket", "textarea", "Keterangan !", $the_row ["elektif_ket"] );
		
		
		$this->uitable->addModal ( "", "label", "<strong>INFEKSI LUKA OPERASI</strong>", "" );
		$this->uitable->addModal ( "operasi_infeksi", "checkbox-ya", "Apakah Mengalami Luka Operasi ? ", $the_row ["operasi_infeksi"] );
		$this->uitable->addModal ( "operasi_kapan", "date", "Kapan Mengalami Luka Operasi ?", $the_row ["operasi_kapan"] );
		$this->uitable->addModal ( "operasi_jenis", "checkbox-ya", "Apakah Mengalami Infeksi ?", $the_row ["operasi_jenis"] );
		$this->uitable->addModal ( "operasi_ket", "textarea", "Keterangan !", $the_row ["operasi_ket"] );
		
		$this->uitable->addModal ( "", "label", "<strong>PASIEN ISK</strong>", "" );
		$this->uitable->addModal ( "isk_pasang", "checkbox-ya", "Apakah Pasien ISK (Cateter, dll) ? ", $the_row ["isk_pasang"] );
		$this->uitable->addModal ( "isk_kapan", "date", "Kapan Operasi Elektif ?", $the_row ["isk_kapan"] );
		$this->uitable->addModal ( "isk_infeksi", "checkbox-ya", "Apakah Mengalami Infeksi ?", $the_row ["isk_infeksi"] );
		$this->uitable->addModal ( "isk_ket", "textarea", "Keterangan !", $the_row ["isk_ket"] );
		
		$this->uitable->addModal ( "", "label", "<strong>Sentinel</strong>", "" );
		$this->uitable->addModal ( "sentinel", "checkbox-ya", "Apakah Pasien Sentinel ? ", $the_row ["sentinel"] );
		$this->uitable->addModal ( "sentinel_kapan", "date", "Kapan Sentinel ?", $the_row ["sentinel_kapan"] );
		$this->uitable->addModal ( "sentinel_keterangan", "textarea", "Keterangan !", $the_row ["sentinel_keterangan"] );
		
		$this->uitable->addModal ( "", "label", "<strong>Nyaris Cedera</strong>", "" );
		$this->uitable->addModal ( "nyaris", "checkbox-ya", "Apakah Pasien Nyaris Cedera ? ", $the_row ["nyaris"] );
		$this->uitable->addModal ( "nyaris_kapan", "date", "Kapan Nyaris Cedera?", $the_row ["nyaris_kapan"] );
		$this->uitable->addModal ( "nyaris_keterangan", "textarea", "Keterangan !", $the_row ["nyaris_keterangan"] );
		
		$this->uitable->addModal ( "", "label", "<strong>Cedera</strong>", "" );
		$this->uitable->addModal ( "cedera", "checkbox-ya", "Apakah Pasien Cedera ? ", $the_row ["cedera"] );
		$this->uitable->addModal ( "cedera_kapan", "date", "Kapan Cedera ?", $the_row ["cedera_kapan"] );
		$this->uitable->addModal ( "cedera_keterangan", "textarea", "Keterangan !", $the_row ["cedera_keterangan"] );		
		
		$this->uitable->addModal ( "", "label", "<strong>BAYI < 2000 Gram</strong>", "" );
		$this->uitable->addModal ( "bayi_kurang", "checkbox-ya", "Apakah Bayi < 2000 gr ? ", $the_row ["bayi_kurang"] );
		$this->uitable->addModal ( "bayi_kapan", "date", "Kapan Lahir ?", $the_row ["bayi_kapan"] );
		$this->uitable->addModal ( "bayi_rujukan", "checkbox-ya", "Rujukan !", $the_row ["bayi_rujukan"] );
		$this->uitable->addModal ( "bayi_sebab", "textarea", "Jika Meninggal, Mengapa ?", $the_row ["bayi_sebab"] );
		
		$modal = $this->uitable->getModal ();
		//$modal->setComponentSize(Modal::$BIG);
		//$modal->setModalSize(Modal::$FULL_MODEL);
		$modal->setTitle ( "Indikator Klinis" );
		if ($this->page == "medical_record" && $this->action == "iklin") {
			echo $this->uitable->getHtml ();
		} else {
			echo "<div class='hide'>" . $this->uitable->getHtml () . "</div>";
			$modal->setAlwaysShow ( true );
		}
		echo $modal->joinFooterAndForm ()->getHtml ();
	}
	public function jsLoader() {
		echo addJS ( "framework/bootstrap/js/bootstrap-datepicker.js" );
		echo addJS ( "framework/smis/js/table_action.js" );
		
		loadLibrary ( "smis-libs-function-javascript" );
	}
	public function cssLoader() {
		echo addCSS ( "framework/bootstrap/css/datepicker.css" );
	}
	public function cssPreLoad() {
		?>
			<style type="text/css">
			#<?php echo $this->action;?>_add_form_form>div { clear: both; width: 100%;}
			#<?php echo $this->action; ?>_add_form_form>div label { width: 250px;}
			</style>
		<?php
	}
	
	/* when it's star build */
	public function jsPreLoad() {
		?>
		<script type="text/javascript">
		var <?php echo $this->action; ?>;
		var ik_pasien;
		var ik_noreg="<?php echo $this->noreg_pasien; ?>";
		var ik_nama_pasien="<?php echo $this->nama_pasien; ?>";
		var ik_nrm_pasien="<?php echo $this->nrm_pasien; ?>";
		var ik_polislug="<?php echo $this->polislug; ?>";
		var ik_the_page="<?php echo $this->page; ?>";
		var ik_the_protoslug="<?php echo $this->protoslug; ?>";
		var ik_the_protoname="<?php echo $this->protoname; ?>";
		var ik_the_protoimplement="<?php echo $this->protoimplement; ?>";
		$(document).ready(function() {
			$(".mydate").datepicker();
			ik_pasien=new TableAction("ik_pasien",ik_the_page,"<?php echo $this->action; ?>",new Array());
			ik_pasien.setSuperCommand("ik_pasien");
			ik_pasien.setPrototipe(ik_the_protoname,ik_the_protoslug,ik_the_protoimplement);
			ik_pasien.selected=function(json){
				var nama=json.nama_pasien;
				var nrm=json.nrm;
				var noreg=json.id;		
				$("#<?php echo $this->action; ?>_nama_pasien").val(nama);
				$("#<?php echo $this->action; ?>_nrm_pasien").val(nrm);
				$("#<?php echo $this->action; ?>_noreg_pasien").val(noreg);
			};
			
			var column=new Array(
					"id","tanggal","ruangan","kelas",
					"nama_pasien","noreg_pasien","nrm_pasien",
					"decubitus_baring",'decubitus_kapan',"decubitus","decubitus_ket",
					"infus_pasang",'infus_kapan',"infus_infeksi","infus_ket",
					"transfusi_darah","transfusi_kapan","transfusi_guna","transfusi_wb","transfusi_prc",
					"transfusi_lain","transfusi_plasma","transfusi_infeksi","transfusi_ket","transfusi_kasus",
					"isk_pasang","isk_kapan","isk_infeksi","isk_ket",
					"operasi_infeksi","operasi_kapan","operasi_jenis","operasi_ket",
					"elektif_operasi","elektif_kapan","elektif_tunggu","elektif_ket",
					"bayi_kurang","bayi_kapan","bayi_hidup","bayi_sebab","bayi_rujukan",
					"sentinel","sentinel_kapan","sentinel_keterangan",
					"cedera","cedera_kapan","cedera_keterangan",
					"nyaris","nyaris_kapan","nyaris_keterangan"					
					);
			<?php echo $this->action; ?>=new TableAction("<?php echo $this->action; ?>",ik_the_page,"<?php echo $this->action; ?>",column);
			<?php echo $this->action; ?>.setPrototipe(ik_the_protoname,ik_the_protoslug,ik_the_protoimplement);
			<?php echo $this->action; ?>.getRegulerData=function(){
				var reg_data={	
						page:this.page,
						action:this.action,
						super_command:this.super_command,
						prototype_name:this.prototype_name,
						prototype_slug:this.prototype_slug,
						prototype_implement:this.prototype_implement,
						polislug:ik_polislug,
						noreg_pasien:ik_noreg,
						nama_pasien:ik_nama_pasien,
						nrm_pasien:ik_nrm_pasien
						};
				return reg_data;
			};
			<?php
			if ($this->page == "medical_record" && $this->action == "iklin") {
				echo $this->action;?>.view();<?php
			} else {
				echo $this->action . ".clear=function(){return;};";
			}
			?>
		});
		</script>
<?php
	}
	public function superCommand($super_command) {
		/* PASIEN */
		$ptable = new Table ( array (
				'Nama',
				'NRM',
				"No Reg" 
		), "", NULL, true );
		$ptable->setName ( "ik_pasien" );
		$ptable->setModel ( Table::$SELECT );
		$padapter = new SimpleAdapter ();
		$padapter->add ( "Nama", "nama_pasien" );
		$padapter->add ( "NRM", "nrm", "digit8" );
		$padapter->add ( "No Reg", "id" );
		$presponder = new ServiceResponder ( $this->db, $ptable, $padapter, "get_registered" );
		$super = new SuperCommand ();
		$super->addResponder ( "ik_pasien", $presponder );
		$init = $super->initialize ();
		if ($init != null) {
			echo $init;
			return;
		}
	}
}

?>