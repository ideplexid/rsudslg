<?php
require_once 'smis-framework/smis/template/ModulTemplate.php';
require_once ("smis-base/smis-include-service-consumer.php");
require_once "medical_record/class/service/RuanganService.php";
require_once 'smis-libs-hrd/EmployeeResponder.php';

class LaporanGigiMulutTemplate extends ModulTemplate {
	protected $db;
	protected $polislug;
	protected $page;
	protected $protoslug;
	protected $protoname;
	protected $protoimplement;
	protected $action;
	protected $dbtable;
	protected $uitable;
	protected $noreg_pasien;
	protected $nama_pasien;
	protected $nrm_pasien;
	protected $alamat;
	protected $umur;
	protected $jk;
	protected $ruang_asal;
	protected $id_antrian;
    protected $carabayar;
	public function __construct($db, $polislug = "all", $noreg = "", $nrm = "", $nama = "", $alamat = "", $umur = "", $jk = "0", $id_antrian = "0", $ruang_asal = "", $page = "medical_record", $action = "lap_gigimulut",$carabayar, $protoslug = "", $protoname = "", $protoimplement = "") {
		$this->db = $db;
		$this->noreg_pasien = $noreg;
		$this->id_antrian = $id_antrian;
		
		$this->nama_pasien = $nama;
		$this->nrm_pasien = $nrm;
		$this->alamat = $alamat;
		$this->umur = $umur;
		$this->jk = $jk;
		$this->ruang_asal = $ruang_asal;
		$this->polislug = $polislug;
		$this->dbtable = new DBTable ( $this->db, "smis_mr_gigimulut" );
		$this->page = $page;
		$this->protoslug = $protoslug;
		$this->protoimplement = $protoimplement;
		$this->protoname = $protoname;
		$this->action = $action;
        $this->carabayar=$carabayar;
		if ($polislug != "all")
			$this->dbtable->addCustomKriteria ( "ruangan", "='" . $polislug . "'" );
		if ($noreg != "")
			$this->dbtable->addCustomKriteria ( "noreg_pasien", "='" . $noreg . "'" );
		$head=array ('NRM',"Pasien","No Register" );
		$this->uitable = new Table ($head , " Indikator " . ($this->polislug == "all" ? "" : ucfirst ( $this->protoname )), NULL, true );
		$this->uitable->setName ( $action );
	}
	
	
	
	public function command($command) {
        require_once "smis-base/smis-include-duplicate.php";
		$adapter = new SimpleAdapter ();
		$adapter->add ( "Pasien", "nama_pasien" );
		$adapter->add ( "NRM", "nrm_pasien", "digit8" );
		$adapter->add ( "No Register", "noreg_pasien", "digit8" );
		$dbres = new DuplicateResponder ( $this->dbtable, $this->uitable, $adapter );
		$dbres->setDuplicate(false,"");
        $dbres->setAutonomous(getSettings($this->db,"smis_autonomous_id",""));
        $data = $dbres->command ( $_POST ['command'] );
		echo json_encode ( $data );
		return;
	}
	public function phpPreLoad() {
		$service = new RuanganService ( $this->db );
		$service->execute ();
		$ruangan = $service->getContent ();
		
		if ($this->noreg_pasien != "") {
			$this->uitable->addModal ( "nama_pasien", "hidden", "", $this->nama_pasien, "n", null, true );
			$this->uitable->addModal ( "nrm_pasien", "hidden", "", $this->nrm_pasien, "n", null, true );
			$this->uitable->addModal ( "noreg_pasien", "hidden", "", $this->noreg_pasien, "n", null, true );
			$this->uitable->addModal ( "alamat", "hidden", "", $this->alamat, "n", null, true );
			$this->uitable->addModal ( "jk", "hidden", "", $this->jk, "n", null, true );
			$this->uitable->addModal ( "umur", "hidden", "", $this->umur, "n", null, true );
			$this->uitable->addModal ( "ruang_asal", "hidden", "", $this->ruang_asal, "n", null, true );
		} else {
			$this->uitable->addModal ( "nama_pasien", "chooser-" . $this->action . "-lgm_pasien-Pasien", "Pasien", $this->nama_pasien, "n", null, true );
			$this->uitable->addModal ( "nrm_pasien", "text", "NRM", $this->nrm_pasien, "n", null, true );
			$this->uitable->addModal ( "noreg_pasien", "text", "No Reg", $this->noreg_pasien, "n", null, true );
		}
		if ($this->polislug == "all")
			$this->uitable->addModal ( "ruangan", "select", "Asal Ruangan", $ruangan );
		else
			$this->uitable->addModal ( "ruangan", "hidden", "", $this->polislug, "n", null, true );
		
		$the_row = array ();
		$the_row ['tumpatan_gigi_tetap'] 		= "";
		$the_row ['tumpatan_gigi_sulung']		= "";
		$the_row ['pengobatan_pulpa'] 			= "";
		$the_row ['pencabutan_gigi_tetap'] 		= "";
		$the_row ['pencabutan_gigi_sulung'] 	= "";
		$the_row ['pengobatan_periodontal'] 	= "";
		$the_row ['pengobatan_abses'] 			= "";
		$the_row ['pembersihan_karang_gigi'] 	= "";
		$the_row ['prothese_lengkap'] 			= "";
		$the_row ['prothese_sebagian'] 			= "";
		$the_row ['prothese_cekat'] 			= "";
		$the_row ['orthodonti'] 				= "";
		$the_row ['jacket_bridge'] 				= "";
		$the_row ['bedah_mulut'] 				= "";
		$the_row ["id"] 						= "";
		$the_row ["dokter"] 					= "";
		$the_row ["id_dokter"] 					= "";
		$the_row ['tanggal'] 					= date ( "Y-m-d" );
		
        $the_row ["pasca_bedah"] 				= "";
		$the_row ["preventif"] 					= "";
		$the_row ["konservasi"] 				= "";
		$the_row ["periodonsia"] 				= "";
		$the_row ["ro_photo"] 					= "";
		$the_row ["splinting"] 					= "";
		$the_row ["cetak_crovo"] 				= "";
		$the_row ["cetak_pasak"] 				= "";
		$the_row ["insersi_pasak"] 				= "";
        
        $the_row ["carabayar"] 				    = $this->carabayar;
		$the_row ["baru_lama"] 				    = "";
		
        
		$exist ['noreg_pasien'] = $this->noreg_pasien;
		$exist ['nama_pasien'] = $this->nama_pasien;
		$exist ['nrm_pasien'] = $this->nrm_pasien;
		$exist ['ruangan'] = $this->polislug;
		if ($this->dbtable->is_exist ( $exist )) {
			$row = $this->dbtable->select ( $exist );
			$the_row ['tumpatan_gigi_tetap'] 		= $row->tumpatan_gigi_tetap;
			$the_row ['tumpatan_gigi_sulung']		= $row->tumpatan_gigi_sulung;
			$the_row ['pengobatan_pulpa'] 			= $row->pengobatan_pulpa;
			$the_row ['pencabutan_gigi_tetap'] 		= $row->pencabutan_gigi_tetap;
			$the_row ['pencabutan_gigi_sulung'] 	= $row->pencabutan_gigi_sulung;
			$the_row ['pengobatan_periodontal'] 	= $row->pengobatan_periodontal;
			$the_row ['pengobatan_abses'] 			= $row->pengobatan_abses;
			$the_row ['pembersihan_karang_gigi'] 	= $row->pembersihan_karang_gigi;
			$the_row ['prothese_lengkap'] 			= $row->prothese_lengkap;
			$the_row ['prothese_sebagian'] 			= $row->prothese_sebagian;
			$the_row ['prothese_cekat'] 			= $row->prothese_cekat;
			$the_row ['orthodonti'] 				= $row->orthodonti;
			$the_row ['jacket_bridge'] 				= $row->jacket_bridge;
			$the_row ['bedah_mulut'] 				= $row->bedah_mulut;
			$the_row ["id"] 						= $row->id;
			$the_row ['tanggal'] 					= $row->tanggal;
			$the_row ["dokter"] 					= $row->dokter;
			$the_row ["id_dokter"] 					= $row->id_dokter;            
            $the_row ["pasca_bedah"] 				= $row->pasca_bedah;
            $the_row ["preventif"] 					= $row->preventif;
            $the_row ["konservasi"] 				= $row->konservasi;
            $the_row ["periodonsia"] 				= $row->periodonsia;
            $the_row ["ro_photo"] 					= $row->ro_photo;
            $the_row ["splinting"] 					= $row->splinting;
            $the_row ["cetak_crovo"] 				= $row->cetak_crovo;
            $the_row ["cetak_pasak"] 				= $row->cetak_pasak;
            $the_row ["insersi_pasak"] 				= $row->insersi_pasak;
            $the_row ["baru_lama"] 				    = $row->baru_lama;
		} else {
			$exist ['umur'] = $this->umur;
			$exist ['jk'] = $this->jk;
			$exist ['asal_ruang'] = $this->ruang_asal;
			$exist ['alamat'] = $this->alamat;
			$exist ['noreg_pasien'] = $this->noreg_pasien;
			$exist ['nrm_pasien'] = $this->nrm_pasien;
			$exist ['nama_pasien'] = $this->nama_pasien;
			$exist ['tanggal'] = date ( "Y-m-d" );
            $this->dbtable->insert ( $exist );
			
            $the_row ["id"] = $this->dbtable->get_inserted_id ();
            
            $update['origin_id']=$the_row ["id"];
            $update['autonomous']="[".getSettings($this->db,"smis_autonomous_id","")."]";
            $this->dbtable->update($update,array("id"=>$the_row ["id"]));
		}
        
		$this->uitable->addModal ( "id", "hidden", "", $the_row ['id'] );
		$this->uitable->addModal ( "dokter", "chooser-lgm_pasien-dokter_lgm-Dokter Pemeriksa", "Dokter Pemeriksa", $the_row ['dokter_lgm'],"y",null,false,null,true,"tanggal" );
		$this->uitable->addModal ( "id_dokter", "hidden", "", $the_row ['id_dokter'],"y",null,false,null,false);
		$this->uitable->addModal ( "tanggal", "date", "Tanggal", $the_row ['tanggal'] ,"y",null,false,null,false,"tumpatan_gigi_tetap");
		$this->uitable->addModal ( "carabayar", "text", "Carabayar", $the_row ['carabayar'] ,"n",null,true,null,false,"tumpatan_gigi_tetap");
		$this->uitable->addModal ( "tumpatan_gigi_tetap", "checkbox", "Tumpatan Gigi Tetap", $the_row ['tumpatan_gigi_tetap'] ,"y",null,false,null,false,"tumpatan_gigi_sulung");
		$this->uitable->addModal ( "tumpatan_gigi_sulung", "checkbox", "Tumpatan Gigi Sulung", $the_row ['tumpatan_gigi_sulung'] ,"y",null,false,null,false,"pengobatan_pulpa");
		$this->uitable->addModal ( "pengobatan_pulpa", "checkbox", "Pengobatan Pulpa", $the_row ['pengobatan_pulpa'] ,"y",null,false,null,false,"pencabutan_gigi_tetap");
		$this->uitable->addModal ( "pencabutan_gigi_tetap", "select", "Pencabutan Gigi Tetap", $this->getPencabutanGigiTetap($the_row ['pencabutan_gigi_tetap']) ,"y",null,false,null,false,"pencabutan_gigi_sulung");
		$this->uitable->addModal ( "pencabutan_gigi_sulung", "select", "Pencabutan Gigi Sulung", $this->getPencabutanGigiSulung($the_row ['pencabutan_gigi_sulung']) ,"y",null,false,null,false,"pengobatan_periodontal");
        $this->uitable->addModal ( "pengobatan_periodontal", "select", "Pengobatan Periodontal", $this->getPengobatanPeriodental($the_row ['pengobatan_periodontal']) ,"y",null,false,null,false,"pengobatan_abses");
		$this->uitable->addModal ( "pengobatan_abses", "select", "Pengobatan Abses", $this->getPengobatanAbses($the_row ['pengobatan_abses']) ,"y",null,false,null,false,"pembersihan_karang_gigi");
		$this->uitable->addModal ( "pembersihan_karang_gigi", "checkbox", "Pembersihan Karang Gigi (Scalling)", $the_row ['pembersihan_karang_gigi'] ,"y",null,false,null,false,"prothese_lengkap");
		$this->uitable->addModal ( "prothese_lengkap", "checkbox", "Prothesa Lepas Lengkap", $the_row ['prothese_lengkap'] ,"y",null,false,null,false,"prothese_sebagian");
		$this->uitable->addModal ( "prothese_sebagian", "checkbox", "Prothesa Lepas Sebagian", $the_row ['prothese_sebagian'] ,"y",null,false,null,false,"prothese_cekat");
		$this->uitable->addModal ( "prothese_cekat", "checkbox", "Prothesa Cekat", $the_row ['prothese_cekat'] ,"y",null,false,null,false,"orthodonti");
		$this->uitable->addModal ( "orthodonti", "select", "Orthodonsia", $this->getOrthodontia($the_row ['orthodonti']) ,"y",null,false,null,false,"jacket_bridge");
		$this->uitable->addModal ( "jacket_bridge", "checkbox", "Jacket Bridge", $the_row ['jacket_bridge'] ,"y",null,false,null,false,"bedah_mulut");
		$this->uitable->addModal ( "bedah_mulut", "select", "Bedah Mulut", $this->getBedahMulut($the_row ['bedah_mulut']) ,"y",null,false,null,false,"pasca_bedah");
		
        $this->uitable->addModal ( "pasca_bedah", "checkbox", "Tindakan Pasca Bedah", $the_row ['pasca_bedah'] ,"y",null,false,null,false,"preventif");
		$this->uitable->addModal ( "preventif", "select", "Tindakan Preventif (Premedical)", $this->getPreventif($the_row ['preventif']) ,"y",null,false,null,false,"konservasi");
		$this->uitable->addModal ( "konservasi", "select", "Konservasi Gigi", $this->getKonservasiGigi($the_row ['konservasi']) ,"y",null,false,null,false,"periodonsia");
		$this->uitable->addModal ( "periodonsia", "select", "Periodonsia", $this->getPriodensia($the_row ['periodonsia']) ,"y",null,false,null,false,"ro_photo");
		$this->uitable->addModal ( "ro_photo", "checkbox", "RO Photo", $the_row ['ro_photo'] ,"y",null,false,null,false,"splinting");
		$this->uitable->addModal ( "splinting", "checkbox", "Splinting", $the_row ['splinting'] ,"y",null,false,null,false,"cetak_crovo");
		$this->uitable->addModal ( "cetak_crovo", "checkbox", "Cetak Crovo", $the_row ['cetak_crovo'] ,"y",null,false,null,false,"cetak_pasak");
		$this->uitable->addModal ( "cetak_pasak", "checkbox", "Cetak Pasak", $the_row ['cetak_pasak'] ,"y",null,false,null,false,"insersi_pasak");
		$this->uitable->addModal ( "insersi_pasak", "checkbox", "Insersi Pasak", $the_row ['insersi_pasak'] ,"y",null,false,null,false,"save");
		$this->uitable->addModal ( "baru_lama", "checkbox", "Baru", $the_row ['baru_lama'] ,"n",null,false,null,false,"save");
		
		
		$modal = $this->uitable->getModal ();
		$modal->setTitle ( "Laporan Gigi & Mulut" );
		if ($this->page == "medical_record" && $this->action == "iklin") {
			echo $this->uitable->getHtml ();
		} else {
			echo "<div class='hide'>" . $this->uitable->getHtml () . "</div>";
			$modal->setAlwaysShow ( true );
		}
		echo $modal->joinFooterAndForm ()->getHtml ();
	}
    
    private function getPriodensia($df){
        $opt=new OptionBuilder();
        $opt->add("","",$df==""?"1":"0");
        $opt->add("Gingivectomy","Gingivectomy",$df=="Gingivectomy"?"1":"0");
        $opt->add("Gingivoplasty","Gingivoplasty",$df=="Gingivoplasty"?"1":"0");
        return $opt->getContent();
    }
    
    private function getKonservasiGigi($df){
        $opt=new OptionBuilder();
        $opt->add("","",$df==""?"1":"0");
        $opt->add("Amalgam tanpa Pin Retainer","Amalgam",$df=="Amalgam"?"1":"0");
        $opt->add("Amalgam dengan Pin Retainer","Amalgam Pin",$df=="Amalgam Pin"?"1":"0");
        $opt->add("Komposit Resin","Resin",$df=="Resin"?"1":"0");
        $opt->add("Komposit Resin dengan Pin Retainer","Resin Pin",$df=="Resin Pin"?"1":"0");
        $opt->add("Logam Tuang (inly-only)","Inly",$df=="Inly"?"1":"0");
        $opt->add("Logam Tuang (overly)","Overly",$df=="Overly"?"1":"0");
        $opt->add("Pupektomi","Pupektomi",$df=="Pupektomi"?"1":"0");
        $opt->add("Pemutihan Gigi","Pemutihan",$df=="Pemutihan"?"1":"0");
        return $opt->getContent();
    }
    
    public function getPreventif($df){
        $opt=new OptionBuilder();
        $opt->add("","",$df==""?"1":"0");
        $opt->add("Scaling","Scaling",$df=="Scaling"?"1":"0");
        $opt->add("Topical Aplikasi","Topical",$df=="Topical"?"1":"0");
        return $opt->getContent();
    }
    
    public function getBedahMulut($df){
        $opt=new OptionBuilder();
        $opt->add("","",$df==""?"1":"0");
        $opt->add("Bedah pada Squestrum kecil (Lokal Esteomyelitis)","Lokal Esteomyelitis",$df=="Lokal Esteomyelitis"?"1":"0");
        $opt->add("Odontectomi (M3/G/P bawah dll.)","Odontectomi",$df=="Odontectomi"?"1":"0");
        $opt->add("Frenektomi","Frenektomi",$df=="Frenektomi"?"1":"0");
        $opt->add("Excisi Dentura Hyperplasi","Excisi Dentura Hyperplasi",$df=="Excisi Dentura Hyperplasi"?"1":"0");
        $opt->add("Alveolaktomi / Alveoloplasti","Alveolaktomi",$df=="Alveolaktomi"?"1":"0");
        $opt->add("Excisi Varus Palatinus","Excisi Varus Palatinus",$df=="Excisi Varus Palatinus"?"1":"0");
        $opt->add("Apek Reseksi","Apek",$df=="Apek"?"1":"0");
        $opt->add("Diopsi","Diopsi",$df=="Diopsi"?"1":"0");
        $opt->add("Operasi Kusta","Operasi Kusta",$df=="Operasi Kusta"?"1":"0");
        $opt->add("Bedah Sialokitniasis","Bedah Sialokitniasis",$df=="Bedah Sialokitniasis"?"1":"0");
        $opt->add("Penjahitan Luka Sobek Jaringan Lunak","Penjahitan",$df=="Penjahitan"?"1":"0");
        $opt->add("Ekstraksi Dento Alveolar","Ekstraksi Dento Alveolar",$df=="Ekstraksi Dento Alveolar"?"1":"0");
        $opt->add("Pengelolaan Simple Fraktur Mandibula & Maxila","Pengelolaan Simple Fraktur",$df=="Pengelolaan Simple Fraktur"?"1":"0");
        return $opt->getContent();
    }
    
    public function getOrthodontia($df){
        $opt=new OptionBuilder();
        $opt->add("","",$df==""?"1":"0");
        $opt->add("Kasus Orthodonsia yang dirawat","Kasus",$df=="Kasus"?"1":"0");
        $opt->add("Perawatan orthodonsia","Rawat",$df=="Rawat"?"1":"0");
        return $opt->getContent();
    }
    
	public function jsLoader() {
		echo addJS ( "framework/bootstrap/js/bootstrap-datepicker.js" );
		echo addJS ( "framework/smis/js/table_action.js" );
		loadLibrary ( "smis-libs-function-javascript" );
	}
    
    private function getPencabutanGigiTetap($df){
        $opt=new OptionBuilder();
        $opt->add("","",$df==""?"1":"0");
        $opt->add("Karena kelainan pulpa","Pulpa",$df=="Pulpa"?"1":"0");
        $opt->add("Karena kelainan lain","Lain",$df=="Lain"?"1":"0");
        return $opt->getContent();
    }
    
    private function getPencabutanGigiSulung($df){
        $opt=new OptionBuilder();
        $opt->add("","",$df==""?"1":"0");
        $opt->add("Karena kelainan persitensi","Persitensi",$df=="Persitensi"?"1":"0");
        $opt->add("Karena kelainan lain","Lain",$df=="Lain"?"1":"0");
        return $opt->getContent();
    }
    
    private function getPengobatanPeriodental($df){
        $opt=new OptionBuilder();
        $opt->add("","",$df==""?"1":"0");
        $opt->add("Ginggival Curetage","Curetage",$df=="Curetage"?"1":"0");
        $opt->add("Tindakan Lainya","Lain",$df=="Lain"?"1":"0");
        return $opt->getContent();
    }
    
    private function getPengobatanAbses($df){
        $opt=new OptionBuilder();
        $opt->add("","",$df==""?"1":"0");
        $opt->add("Pengobatan Abses berupa pemberian obat-obatan per oral","Obat Oral",$df=="Obat"?"1":"0");
        $opt->add("Pengobatan Abses berupa incisi intra oral","Incisi Intra Oral",$df=="Incisi Intra"?"1":"0");
        $opt->add("Pengobatan Abses berupa incisi extra oral","Incisi Extra Oral",$df=="Incisi Extra"?"1":"0");
        return $opt->getContent();
    }
	
	public function cssLoader() {
		echo addCSS ( "framework/bootstrap/css/datepicker.css" );
	}
	
	public function cssPreLoad() {
		?>
		<style type="text/css">
				#<?php echo $this->action;?>_add_form_form>div {clear: both; width: 100%;}
				#<?php echo $this->action; ?>_add_form_form>div label { width: 250px; }
		</style>
		<?php
	}
	
	/* when it's star build */
	public function jsPreLoad() {
		?>
		<script type="text/javascript">
		var <?php echo $this->action; ?>;
		var lgm_pasien;
		var lgm_noreg="<?php echo $this->noreg_pasien; ?>";
		var lgm_nama_pasien="<?php echo $this->nama_pasien; ?>";
		var lgm_nrm_pasien="<?php echo $this->nrm_pasien; ?>";
		var lgm_polislug="<?php echo $this->polislug; ?>";
		var lgm_the_page="<?php echo $this->page; ?>";
		var lgm_the_protoslug="<?php echo $this->protoslug; ?>";
		var lgm_the_protoname="<?php echo $this->protoname; ?>";
		var lgm_the_protoimplement="<?php echo $this->protoimplement; ?>";
		var lgm_id_antrian="<?php echo $this->id_antrian; ?>";
		var lgm_action="<?php echo $this->action; ?>";
		$(document).ready(function() {
			$('.mydate').datepicker();
			lgm_pasien=new TableAction("lgm_pasien",lgm_the_page,lgm_action,new Array());
			lgm_pasien.setSuperCommand("lgm_pasien");
			lgm_pasien.setPrototipe(lgm_the_protoname,lgm_the_protoslug,lgm_the_protoimplement);
			lgm_pasien.selected=function(json){
				var nama=json.nama_pasien;
				var nrm=json.nrm;
				var noreg=json.id;		
				$("#"+lgm_action+"_nama_pasien").val(nama);
				$("#"+lgm_action+"_nrm_pasien").val(nrm);
				$("#"+lgm_action+"_noreg_pasien").val(noreg);
			};

			dokter_lgm=new TableAction("dokter_lgm",lgm_the_page,lgm_action,new Array());
			dokter_lgm.setSuperCommand("dokter_lgm");
			dokter_lgm.setPrototipe(lgm_the_protoname,lgm_the_protoslug,lgm_the_protoimplement);
			dokter_lgm.selected=function(json){
				$("#"+lgm_action+"_dokter").val(json.nama);
				$("#"+lgm_action+"_id_dokter").val(json.id);
			};

			stypeahead("#"+lgm_action+"_dokter_lgm",3,dokter_lgm,"nama",function(item){
				$("#"+lgm_action+"_dokter").val(json.nama);
				$("#"+lgm_action+"_id_dokter").val(json.id);			
			});

			var column=new Array(
					"id","tanggal","ruangan",'tumpatan_gigi_tetap',
					'tumpatan_gigi_sulung','pengobatan_pulpa',
					'pencabutan_gigi_tetap','pencabutan_gigi_sulung',
					'pengobatan_periodontal','pengobatan_abses',
					'pembersihan_karang_gigi','prothese_lengkap',
					'prothese_sebagian','prothese_cekat',
					'orthodonti','jacket_bridge','bedah_mulut',
					"nama_pasien","noreg_pasien","nrm_pasien",
					"alamat","umur","jk",					
					"spesialisasi","anastesi","baru_lama","carabayar",
					"dokter","id_dokter","asal_ruang",
                    "pasca_bedah","preventif","konservasi","periodonsia",
                    "ro_photo","splinting","cetak_crovo","cetak_pasak","insersi_pasak"
					);
			<?php echo $this->action; ?>=new TableAction("<?php echo $this->action; ?>",lgm_the_page,"<?php echo $this->action; ?>",column);
			<?php echo $this->action; ?>.setPrototipe(lgm_the_protoname,lgm_the_protoslug,lgm_the_protoimplement);
			<?php echo $this->action; ?>.getRegulerData=function(){
				var reg_data={	
						page:this.page,
						action:this.action,
						super_command:this.super_command,
						prototype_name:this.prototype_name,
						prototype_slug:this.prototype_slug,
						prototype_implement:this.prototype_implement,
						polislug:lgm_polislug,
						noreg_pasien:lgm_noreg,
						nama_pasien:lgm_nama_pasien,
						nrm_pasien:lgm_nrm_pasien,
						id_antrian:lgm_id_antrian
						};
				return reg_data;
			};

			<?php echo $this->action; ?>.setNextEnter();
			<?php echo $this->action; ?>.setEnableAutofocus(true);
			<?php
		
			if ($this->page == "medical_record" && $this->action == "iklin") {
				echo $this->action;
				?>.view();<?php
			} else {
				echo $this->action . ".clear=function(){return;};";
			}
			?>
		});
		</script>
<?php
	}
	public function superCommand($super_command) {
		/* PASIEN */
		$super = new SuperCommand ();
		
		$phead=array ('Nama','NRM',"No Reg" );
		$ptable = new Table ( $phead, "", NULL, true );
		$ptable->setName ( "lgm_pasien" );
		$ptable->setModel ( Table::$SELECT );
		$padapter = new SimpleAdapter ();
		$padapter->add ( "Nama", "nama_pasien" );
		$padapter->add ( "NRM", "nrm", "digit8" );
		$padapter->add ( "No Reg", "id" );
		$presponder = new ServiceResponder ( $this->db, $ptable, $padapter, "get_registered" );		
		$super->addResponder ( "lgm_pasien", $presponder );
		
		$eadapt = new SimpleAdapter ();
		$eadapt->add ( "Jabatan", "nama_jabatan" );
		$eadapt->add ( "Nama", "nama" );
		$eadapt->add ( "NIP", "nip" );
		
		$head_karyawan=array ('Nama','Jabatan',"NIP");
		$dktable = new Table ( $head_karyawan, "", NULL, true );
		$dktable->setName ( "dokter_lgm" );
		$dktable->setModel ( Table::$SELECT );
		$employee = new EmployeeResponder ( $this->db, $dktable, $eadapt, "dokter" );
		$super->addResponder ( "dokter_lgm", $employee );
		
		$init = $super->initialize ();
		if ($init != null) {
			echo $init;
			return;
		}
	}
}

?>