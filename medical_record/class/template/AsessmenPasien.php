<?php 

require_once 'smis-framework/smis/template/ModulTemplate.php';
require_once ("smis-base/smis-include-service-consumer.php");

class AsessmenPasien extends ModulTemplate{
    
    /**
     * @brief mengambil data detail pasien yang mana akan ditampilkan
     *          pada bagian pelayanan
     * @param string $noreg 
     * @return  array() detail pasien
     */
	public function getDetailPatient($noreg){
        $serv=new ServiceConsumer($this->db,"get_registered",null,"registration");
        $serv->addData("command","edit");
        $serv->addData("id",$noreg);
        $serv->execute();
        $data=$serv->getContent();
        return $data;
    }
    
}

?>