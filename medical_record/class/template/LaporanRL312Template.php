<?php
require_once 'smis-framework/smis/template/ModulTemplate.php';
require_once ("smis-base/smis-include-service-consumer.php");
require_once "medical_record/class/service/RuanganService.php";
require_once 'smis-libs-hrd/EmployeeResponder.php';
class LaporanRL312Template extends ModulTemplate {
	protected $db;
	protected $polislug;
	protected $page;
	protected $protoslug;
	protected $protoname;
	protected $protoimplement;
	protected $action;
	protected $dbtable;
	protected $uitable;
	protected $noreg_pasien;
	protected $nama_pasien;
	protected $nrm_pasien;
	protected $alamat;
	protected $umur;
	protected $jk;
	protected $ruang_asal;
	protected $id_antrian;
	public function __construct($db, $polislug = "all", $noreg = "", $nrm = "", $nama = "", $alamat = "", $umur = "", $jk = "0", $id_antrian = "0", $ruang_asal = "", $page = "medical_record", $action = "lap_keluarga_berencana", $protoslug = "", $protoname = "", $protoimplement = "") {
		$this->db = $db;
		$this->noreg_pasien = $noreg;
		$this->id_antrian = $id_antrian;
		
		$this->nama_pasien = $nama;
		$this->nrm_pasien = $nrm;
		$this->alamat = $alamat;
		$this->umur = $umur;
		$this->jk = $jk;
		$this->ruang_asal = $ruang_asal;
		$this->polislug = $polislug;
		$this->dbtable = new DBTable ( $this->db, "smis_mr_keluarga_berencana" );
		$this->page = $page;
		$this->protoslug = $protoslug;
		$this->protoimplement = $protoimplement;
		$this->protoname = $protoname;
		$this->action = $action;
		if ($polislug != "all")
			$this->dbtable->addCustomKriteria ( "ruangan", "='" . $polislug . "'" );
		if ($noreg != "")
			$this->dbtable->addCustomKriteria ( "noreg_pasien", "='" . $noreg . "'" );
		$head=array ('NRM',"Pasien","No Register" );
		$this->uitable = new Table ($head , " Rehabilitasi Medik " . ($this->polislug == "all" ? "" : ucfirst ( $this->protoname )), NULL, true );
		$this->uitable->setName ( $action );
		
		if ($this->page == "medical_record" && $this->action == "lap_keluarga_berencana") {
			$this->uitable->setAddButtonEnable ( false );
			$this->uitable->setDelButtonEnable ( false );
		}
	}
	
	
	
	public function command($command) {
		$adapter = new SimpleAdapter ();
		$adapter->add ( "Pasien", "nama_pasien" );
		$adapter->add ( "NRM", "nrm_pasien", "digit8" );
		$adapter->add ( "No Register", "noreg_pasien", "digit8" );
		$dbres = new DBResponder ( $this->dbtable, $this->uitable, $adapter );
		$data = $dbres->command ( $_POST ['command'] );
		echo json_encode ( $data );
		return;
	}
	public function phpPreLoad() {
		
		if ($this->noreg_pasien != "") {
			$this->uitable->addModal ( "nama_pasien", "hidden", "", $this->nama_pasien, "n", null, true );
			$this->uitable->addModal ( "nrm_pasien", "hidden", "", $this->nrm_pasien, "n", null, true );
			$this->uitable->addModal ( "noreg_pasien", "hidden", "", $this->noreg_pasien, "n", null, true );
		} else {
			$this->uitable->addModal ( "nama_pasien", "chooser-" . $this->action . "-rl312_pasien", "Pasien", $this->nama_pasien, "n", null, true );
			$this->uitable->addModal ( "nrm_pasien", "text", "NRM", $this->nrm_pasien, "n", null, true );
			$this->uitable->addModal ( "noreg_pasien", "text", "No Reg", $this->noreg_pasien, "n", null, true );
		}
		if ($this->polislug == "all"){
			$service = new RuanganService ( $this->db );
			$service->execute ();
			$ruangan = $service->getContent ();
			$this->uitable->addModal ( "ruangan", "select", "Asal Ruangan", $ruangan );
		}else{
			$this->uitable->addModal ( "ruangan", "hidden", "", $this->polislug, "n", null, true );
		}
		$the_row = array ();
		$the_row ["id"] 	 = "";
		$the_row ['tanggal'] = date ( "Y-m-d" );
		
		$exist ['noreg_pasien'] = $this->noreg_pasien;
		$exist ['nama_pasien'] = $this->nama_pasien;
		$exist ['nrm_pasien'] = $this->nrm_pasien;
		$exist ['ruangan'] = $this->polislug;
		if ($this->dbtable->is_exist ( $exist )) {
			$row = $this->dbtable->select ( $exist );
			$the_row[ "id" ] = $row->id;
			$the_row[ 'tanggal' ] = $row->tanggal;
			$the_row[ "baru" ] = $row->baru;
			$the_row[ "metode" ] = $row->metode;
			$the_row[ "konseling" ] = $row->konseling;
			$the_row[ "kb_baru_cara_masuk" ] = $row->kb_baru_cara_masuk;
			$the_row[ "kb_baru_kondisi" ] = $row->kb_baru_kondisi;
			$the_row[ "kunjungan_ulang" ] = $row->kunjungan_ulang;
			$the_row[ "keluhan_efek_samping" ] = $row->keluhan_efek_samping;
			$the_row[ "keluhan_efek_samping_dirujuk" ] = $row->keluhan_efek_samping_dirujuk;
		} else {
			$exist ['noreg_pasien'] 		= $this->noreg_pasien;
			$exist ['nrm_pasien'] 			= $this->nrm_pasien;
			$exist ['nama_pasien'] 			= $this->nama_pasien;
			$exist ['tanggal'] 				= date("Y-m-d");
			$this->dbtable->insert($exist);
			$the_row ["id"] 				= $this->dbtable->get_inserted_id();
		}
		
		$baru_option = new OptionBuilder();
		if (array_key_exists("baru", $the_row)) {
			if ($the_row['baru'] == 1)
				$baru_option->add("Baru", "1", "1");
			else
				$baru_option->add("Baru", "1");
			if ($the_row['baru'] == 0)
				$baru_option->add("Lama", "0", "1");
			else
				$baru_option->add("Lama", "0");
		} else {
			$baru_option->add("Baru", "1", "1");
			$baru_option->add("Lama", "0");
		}

		$metode_option = new OptionBuilder();
		if (array_key_exists("metode", $the_row)) {
			if ($the_row['metode'] == "")
				$metode_option->add("", "", "1");
			else
				$metode_option->addSingle("");
			if ($the_row['metode'] == "I U D")
				$metode_option->add("I U D", "I U D", "1");
			else
				$metode_option->addSingle("I U D");
			if ($the_row['metode'] == "PIL KB")
				$metode_option->add("PIL KB", "PIL KB", "1");
			else
				$metode_option->addSingle("PIL KB");
			if ($the_row['metode'] == "Kondom")
				$metode_option->add("Kondom", "Kondom", "1");
			else
				$metode_option->addSingle("Kondom");
			if ($the_row['metode'] == "Obat Vaginal")
				$metode_option->add("Obat Vaginal", "Obat Vaginal", "1");
			else
				$metode_option->addSingle("Obat Vaginal");
			if ($the_row['metode'] == "MO Pria")
				$metode_option->add("MO Pria", "MO Pria", "1");
			else
				$metode_option->addSingle("MO Pria");
			if ($the_row['metode'] == "MO Wanita")
				$metode_option->add("MO Wanita", "MO Wanita", "1");
			else
				$metode_option->addSingle("MO Wanita");
			if ($the_row['metode'] == "Suntikan")
				$metode_option->add("Suntikan", "Suntikan", "1");
			else
				$metode_option->addSingle("Suntikan");
			if ($the_row['metode'] == "Implant")
				$metode_option->add("Implant", "Implant", "1");
			else
				$metode_option->addSingle("Implant");
		} else {
			$metode_option->add("", "", "1");
			$metode_option->addSingle("I U D");
			$metode_option->addSingle("PIL KB");
			$metode_option->addSingle("Kondom");
			$metode_option->addSingle("Obat Vaginal");
			$metode_option->addSingle("MO Pria");
			$metode_option->addSingle("MO Wanita");
			$metode_option->addSingle("Suntikan");
			$metode_option->addSingle("Implant");
		}

		$konseling_option = new OptionBuilder();
		if (array_key_exists("konseling", $the_row)) {
			if ($the_row['konseling'] == "")
				$konseling_option->add("", "", "1");
			else
				$konseling_option->addSingle("");
			if ($the_row['konseling'] == "ANC")
				$konseling_option->add("ANC", "ANC", "1");
			else
				$konseling_option->addSingle("ANC");
			if ($the_row['konseling'] == "Pasca Persalinan")
				$konseling_option->add("Pasca Persalinan", "Pasca Persalinan", "1");
			else
				$konseling_option->addSingle("Pasca Persalinan");
		} else {
			$konseling_option->add("", "", "1");
			$konseling_option->addSingle("ANC");
			$konseling_option->addSingle("Pasca Persalinan");
		}

		$cara_masuk_option = new OptionBuilder();
		if (array_key_exists("kb_baru_cara_masuk", $the_row)) {
			if ($the_row['kb_baru_cara_masuk'] == "")
				$cara_masuk_option->add("", "", "1");
			else
				$cara_masuk_option->addSingle("");
			if ($the_row['kb_baru_cara_masuk'] == "Bukan Rujukan")
				$cara_masuk_option->add("Bukan Rujukan", "Bukan Rujukan", "1");
			else
				$cara_masuk_option->addSingle("Bukan Rujukan");
			if ($the_row['kb_baru_cara_masuk'] == "Rujukan Rawat Inap")
				$cara_masuk_option->add("Rujukan Rawat Inap", "Rujukan Rawat Inap", "1");
			else
				$cara_masuk_option->addSingle("Rujukan Rawat Inap");
			if ($the_row['kb_baru_cara_masuk'] == "Rujukan Rawat Jalan")
				$cara_masuk_option->add("Rujukan Rawat Jalan", "Rujukan Rawat Jalan", "1");
			else
				$cara_masuk_option->addSingle("Rujukan Rawat Jalan");
		} else {
			$cara_masuk_option->add("", "", "1");
			$cara_masuk_option->addSingle("Bukan Rujukan");
			$cara_masuk_option->addSingle("Rujukan Rawat Inap");
			$cara_masuk_option->addSingle("Rujukan Rawat Jalan");
		}

		$kondisi_option = new OptionBuilder();
		if (array_key_exists("kb_baru_kondisi", $the_row)) {
			if ($the_row['kb_baru_kondisi'] == "")
				$kondisi_option->add("", "", "1");
			else
				$kondisi_option->addSingle("");
			if ($the_row['kb_baru_kondisi'] == "Pasca Persalinan/Nifas")
				$kondisi_option->add("Pasca Persalinan/Nifas", "Pasca Persalinan/Nifas", "1");
			else
				$kondisi_option->addSingle("Pasca Persalinan/Nifas");
			if ($the_row['kb_baru_kondisi'] == "Abortus")
				$kondisi_option->add("Abortus", "Abortus", "1");
			else
				$kondisi_option->addSingle("Abortus");
			if ($the_row['kb_baru_kondisi'] == "Lainnya")
				$kondisi_option->add("Lainnya", "Lainnya", "1");
			else
				$kondisi_option->addSingle("Lainnya");
		} else {
			$kondisi_option->add("", "", "1");
			$kondisi_option->addSingle("Pasca Persalinan/Nifas");
			$kondisi_option->addSingle("Abortus");
			$kondisi_option->addSingle("Lainnya");
		}

		$this->uitable->addModal("id", "hidden", "", $the_row ['id']);
		$this->uitable->addModal("tanggal", "date", "Tanggal", $the_row ['tanggal'] ,"y",null,false,null,false,"baru");
		$this->uitable->addModal("baru", "select", "Baru/Lama", $baru_option->getContent());		
		$this->uitable->addModal("metode", "select", "Metode", $metode_option->getContent());
		$this->uitable->addModal("konseling", "select", "Konseling", $konseling_option->getContent());
		$this->uitable->addModal("kb_baru_cara_masuk", "select", "KB Baru dg. Cara Masuk", $cara_masuk_option->getContent());
		$this->uitable->addModal("kb_baru_kondisi", "select", "KB Baru dg. Kondisi", $kondisi_option->getContent());
		$this->uitable->addModal("kunjungan_ulang", "checkbox", "Kunjungan Ulang", $the_row['kunjungan_ulang']);
		$this->uitable->addModal("keluhan_efek_samping", "checkbox", "Keluhan Efek Samping", $the_row['keluhan_efek_samping']);
		$this->uitable->addModal("keluhan_efek_samping_dirujuk", "checkbox", "Kel. Efek Samping Dirujuk", $the_row['keluhan_efek_samping_dirujuk']);

		$modal = $this->uitable->getModal ();
		$modal->setTitle ( "Laporan RL 3.12 - Keluarga Berencana" );
		if ($this->page == "medical_record" && $this->action == "iklin") {
			echo $this->uitable->getHtml ();
		} else {
			echo "<div class='hide'>" . $this->uitable->getHtml () . "</div>";
			$modal->setAlwaysShow ( true );
		}
		echo $modal->joinFooterAndForm ()->getHtml ();
	}
	public function jsLoader() {
		echo addJS ( "framework/bootstrap/js/bootstrap-datepicker.js" );
		echo addJS ( "framework/smis/js/table_action.js" );
		loadLibrary ( "smis-libs-function-javascript" );
	}
	
	public function cssLoader() {
		echo addCSS ( "framework/bootstrap/css/datepicker.css" );
	}
	
	public function cssPreLoad() {
		?>
		<style type="text/css">
				#<?php echo $this->action;?>_add_form_form>div {clear: both; width: 100%;}
				#<?php echo $this->action; ?>_add_form_form>div label { width: 250px; }
		</style>
		<?php
	}
	
	/* when it's star build */
	public function jsPreLoad() {
		?>
		<script type="text/javascript">
		var <?php echo $this->action; ?>;
		var rl312_pasien;
		var rl312_noreg="<?php echo $this->noreg_pasien; ?>";
		var rl312_nama_pasien="<?php echo $this->nama_pasien; ?>";
		var rl312_nrm_pasien="<?php echo $this->nrm_pasien; ?>";
		var rl312_polislug="<?php echo $this->polislug; ?>";
		var rl312_the_page="<?php echo $this->page; ?>";
		var rl312_the_protoslug="<?php echo $this->protoslug; ?>";
		var rl312_the_protoname="<?php echo $this->protoname; ?>";
		var rl312_the_protoimplement="<?php echo $this->protoimplement; ?>";
		var rl312_id_antrian="<?php echo $this->id_antrian; ?>";
		var rl312_action="<?php echo $this->action; ?>";
		$(document).ready(function() {
			if ($("#lap_keluarga_berencana_baru").val() == 1) {
				$(".lap_keluarga_berencana_kb_baru_cara_masuk").show();
				$(".lap_keluarga_berencana_kb_baru_kondisi").show();
				$(".lap_keluarga_berencana_kunjungan_ulang").hide();
				$(".lap_keluarga_berencana_keluhan_efek_samping").hide();
				$(".lap_keluarga_berencana_keluhan_efek_samping_dirujuk").hide();
			} else {
				$(".lap_keluarga_berencana_kb_baru_cara_masuk").hide();
				$(".lap_keluarga_berencana_kb_baru_kondisi").hide();
				$(".lap_keluarga_berencana_kunjungan_ulang").show();
				$(".lap_keluarga_berencana_keluhan_efek_samping").show();
				$(".lap_keluarga_berencana_keluhan_efek_samping_dirujuk").show();
			}

			$("#lap_keluarga_berencana_baru").on("change", function() {
				var val = $(this).val();
				if (val == 1) {
					$("#lap_keluarga_berencana_kunjungan_ulang").prop("checked", false);
					$("#lap_keluarga_berencana_keluhan_efek_samping").prop("checked", false);
					$("#lap_keluarga_berencana_keluhan_efek_samping_dirujuk").prop("checked", false);
					$(".lap_keluarga_berencana_kb_baru_cara_masuk").show();
					$(".lap_keluarga_berencana_kb_baru_kondisi").show();
					$(".lap_keluarga_berencana_kunjungan_ulang").hide();
					$(".lap_keluarga_berencana_keluhan_efek_samping").hide();
					$(".lap_keluarga_berencana_keluhan_efek_samping_dirujuk").hide();
				} else {
					$("#lap_keluarga_berencana_kb_baru_cara_masuk").val("");
					$("#lap_keluarga_berencana_kb_baru_kondisi").val("");
					$(".lap_keluarga_berencana_kb_baru_cara_masuk").hide();
					$(".lap_keluarga_berencana_kb_baru_kondisi").hide();
					$(".lap_keluarga_berencana_kunjungan_ulang").show();
					$(".lap_keluarga_berencana_keluhan_efek_samping").show();
					$(".lap_keluarga_berencana_keluhan_efek_samping_dirujuk").show();
				}
			});

			$('.mydate').datepicker();
			rl312_pasien=new TableAction("rl312_pasien",rl312_the_page,rl312_action,new Array());
			rl312_pasien.setSuperCommand("rl312_pasien");
			rl312_pasien.setPrototipe(rl312_the_protoname,rl312_the_protoslug,rl312_the_protoimplement);
			rl312_pasien.selected=function(json){
				var nama=json.nama_pasien;
				var nrm=json.nrm;
				var noreg=json.id;		
				$("#"+rl312_action+"_nama_pasien").val(nama);
				$("#"+rl312_action+"_nrm_pasien").val(nrm);
				$("#"+rl312_action+"_noreg_pasien").val(noreg);
			};
			
			var column=new Array(
				"id","tanggal","ruangan",
				"noreg_pasien", "nrm_pasien", "nama_pasien",
				"baru", "metode", "konseling", "kb_baru_cara_masuk", "kb_baru_kondisi", "kunjungan_ulang", "keluhan_efek_samping", "keluhan_efek_samping_dirujuk"			
			);
			<?php echo $this->action; ?>=new TableAction("<?php echo $this->action; ?>",rl312_the_page,"<?php echo $this->action; ?>",column);
			<?php echo $this->action; ?>.setPrototipe(rl312_the_protoname,rl312_the_protoslug,rl312_the_protoimplement);
			<?php echo $this->action; ?>.getRegulerData=function(){
				var reg_data={	
						page:this.page,
						action:this.action,
						super_command:this.super_command,
						prototype_name:this.prototype_name,
						prototype_slug:this.prototype_slug,
						prototype_implement:this.prototype_implement,
						polislug:rl312_polislug,
						noreg_pasien:rl312_noreg,
						nama_pasien:rl312_nama_pasien,
						nrm_pasien:rl312_nrm_pasien,
						id_antrian:rl312_id_antrian
						};
				return reg_data;
			};

			<?php echo $this->action; ?>.setNextEnter();
			<?php echo $this->action; ?>.setEnableAutofocus(true);
			<?php
			if ($this->page == "medical_record" && $this->action == "iklin") {
				echo $this->action;
				?>.view();<?php
			} else {
				echo $this->action . ".clear=function(){return;};";
			}
			?>
		});
		</script>
<?php
	}
	public function superCommand($super_command) {
		/* PASIEN */
		$super = new SuperCommand ();
		
		$phead=array ('Nama','NRM',"No Reg" );
		$ptable = new Table ( $phead, "", NULL, true );
		$ptable->setName ( "rl312_pasien" );
		$ptable->setModel ( Table::$SELECT );
		$padapter = new SimpleAdapter ();
		$padapter->add ( "Nama", "nama_pasien" );
		$padapter->add ( "NRM", "nrm", "digit8" );
		$padapter->add ( "No Reg", "id" );
		$presponder = new ServiceResponder ( $this->db, $ptable, $padapter, "get_registered" );		
		$super->addResponder ( "rl312_pasien", $presponder );
		$init = $super->initialize ();
		if ($init != null) {
			echo $init;
			return;
		}
	}
}

?>