<?php
require_once 'smis-framework/smis/template/ModulTemplate.php';
require_once ("smis-base/smis-include-service-consumer.php");
require_once 'smis-libs-hrd/EmployeeResponder.php';

class PlebitisTemplate extends ModulTemplate {
	protected $db;
	protected $mode;
	protected $polislug;
	protected $page;
	protected $protoslug;
	protected $protoname;
	protected $protoimplement;
	protected $action;
	protected $dbtable;
	protected $uitable;
	protected $noreg_pasien;
	protected $nama_pasien;
	protected $nrm_pasien;
	protected $gol_umur;
	protected $jk;	
	protected $carabayar;
	protected $urji;
	public static $MODE_DAFTAR 	= "pendaftaran";
	public static $MODE_PERIKSA = "pemeriksaan";
	public function __construct($db, $mode, $polislug = "all", $noreg = "", $nrm = "", $nama = "", $page = "medical_record", $action = "diagnosa", $protoslug = "", $protoname = "", $protoimplement = "", $jenis_umur = "TGL LAHIR TDK VALID", $jk = "0", $carabayar = "Umum",$urji="0") {
		$this->db = $db;
		$this->mode = $mode;
		$this->urji=$urji;
		$this->noreg_pasien = $noreg;
		$this->nama_pasien = $nama;
		$this->gol_umur = $jenis_umur;
		$this->nrm_pasien = $nrm;
		$this->polislug = $polislug;
		$this->dbtable = new DBTable ( $this->db, "smis_mr_plebitis" );
		$this->page = $page;
		$this->protoslug = $protoslug;
		$this->protoimplement = $protoimplement;
		$this->protoname = $protoname;
		$this->action = $action;
		$this->jk = $jk;
		$this->carabayar = $carabayar;
		if ($noreg != "") $this->dbtable->addCustomKriteria ( "noreg_pasien", "='" . $noreg. "'" );
        if($polislug!="") $this->dbtable->addCustomKriteria ( "ruangan", "='" . $polislug. "'" );
		
		$array=array ("No.",'Tanggal',"Lokasi","No. IV","Jenis Cairan","Gol. Obat","Plebitis","Kegiatan","Keluar" );
		$this->uitable = new Table ( $array , " Plebitis RM-35 " . ($this->polislug == "all" ? "" : ucfirst ( $this->protoname )), NULL, true );
		$this->uitable->setName ( $action );
		if ($this->mode == self::$MODE_PERIKSA) {
			$this->uitable->setAddButtonEnable ( false );
			$this->uitable->setDelButtonEnable ( false );
		}
	}
	public function command($command) {
		$adapter = new SimpleAdapter();
		$adapter->setUseNumber(true,"No.","back.");
		$adapter->add ( "Tanggal", "tanggal", "date d M Y" );
		$adapter->add ( "Lokasi", "lokasi" );
		$adapter->add ( "No. IV", "noiv" );
		$adapter->add ( "Jenis Cairan", "jenis_cairan" );
		$adapter->add ( "Gol. Obat", "gol_obat" );
		$adapter->add ( "Plebitis", "plebitis" );
		$adapter->add ( "Kegiatan", "kegiatan" );
        $adapter->add ( "Keluar", "keluar" );
		
		$dbres = new DBResponder( $this->dbtable, $this->uitable, $adapter );
		$data = $dbres->command ( $_POST ['command'] );
		
		echo json_encode ( $data );
		return;
	}
	public function phpPreLoad() {
        
        $kegiatan=new OptionBuilder();
        $kegiatan->addSingle("Infus terpasang dari ruang lain");
        $kegiatan->addSingle("Pasang Infus");
        $kegiatan->addSingle("Cek Infus");
        $kegiatan->addSingle("Ganti Infus");
        $kegiatan->addSingle("Lepas Infus");
        
        $noiv=new OptionBuilder();
        $noiv->addSingle("18");
        $noiv->addSingle("20");
        $noiv->addSingle("22");
        $noiv->addSingle("24");
        $noiv->addSingle("26");
        
        $cairan=new OptionBuilder();
        $cairan->addSingle("Hipotonis < 250 mOs m/l");
        $cairan->addSingle("Isotonis 250-335 mOs m/l");
        $cairan->addSingle("Hipertonis > 375 mOs m/l");
        
        $plebitis=new OptionBuilder();
        $plebitis->add("","","1");
        $plebitis->addSingle("0. Tidak ada nyeri, kemerahan atau bengkak");
        $plebitis->addSingle("1. Tidak ada nyeri, tampak sedikit kemerahan <2.5 cm tidak ada bengkak tidak ada pengerasan");
        $plebitis->addSingle("2. Nyeri, Kemerahan tidak ada pengerasan 2.5 - 4 cm");
        $plebitis->addSingle("3. Nyeri, kemerahan, bengkak tidak ada pengerasan 4 - 75 cm");
        $plebitis->addSingle("4. Nyeri, Kemerahan, bengkak tidak ada pengerasan - 75 cm, keluar cairan purulen");
        
        $keluar=new OptionBuilder();
        $keluar->addSingle("","1");
        $keluar->addSingle("Pulang dilepas");
        $keluar->addSingle("Pindah Kamar terpasang");        
        $keluar->addSingle("Pindah Kamar dilepas");        
        $keluar->addSingle("Rujuk terpasang");    
        $keluar->addSingle("Rujuk dilepas");    
        
		$this->uitable->addModal ( "id", "hidden", "", "" );
		$this->uitable->addModal ( "nama_pasien", "hidden", "", $this->nama_pasien, "n", null, true );
		$this->uitable->addModal ( "noreg_pasien", "hidden", "", $this->noreg_pasien, "n", null, true );
		$this->uitable->addModal ( "nrm_pasien", "hidden", "", $this->nrm_pasien, "n", null, true );	
		$this->uitable->addModal ( "ruangan", "hidden", "", $this->polislug, "n", null, true );
		$this->uitable->addModal ( "tanggal", "date", "Tanggal", date ( "Y-m-d" ) );
		$this->uitable->addModal ( "nama_perawat", "chooser-" . $this->action . "-plebitis_perawat", "Perawat", "", "y", null, false,null,true );
		$this->uitable->addModal ( "kegiatan", "select", "Kegiatan", $kegiatan->getContent(), "y", null, false ,null,false,"kasus");
		$this->uitable->addModal ( "gol_obat", "text", "Gol. Obat", "", "y", null, false );
		$this->uitable->addModal ( "lokasi", "text", "Lokasi", "", "y", null, false );
		$this->uitable->addModal ( "noiv", "select", "No. IV", $noiv->getContent(), "y", null, false );
		$this->uitable->addModal ( "jenis_cairan", "select", "Jenis Cairan", $cairan->getContent(), "y", null, false );
		$this->uitable->addModal ( "plebitis", "select", "Plebitis", $plebitis->getContent(), "y", null, false );
		$this->uitable->addModal ( "keluar", "select", "Keluar", $keluar->getContent(), "y", null, false );
		
		$modal = $this->uitable->getModal ();
		$modal->setComponentSize(Modal::$MEDIUM);
		$modal->setTitle ( "Plebitis" );
		
		$poliname=new Hidden("plebitis_poliname","",$this->protoname);
		$polislug=new Hidden("plebitis_polislug","",$this->polislug);
		$protoslug=new Hidden("plebitis_protoslug","",$this->protoslug);
		$implement=new Hidden("plebitis_implement","",$this->protoimplement);
		$page=new Hidden("plebitis_page","",$this->page);		
		$noreg=new Hidden("plebitis_noreg","",$this->noreg_pasien);
		$nama=new Hidden("plebitis_nama","",$this->nama_pasien);
		$nrm=new Hidden("plebitis_nrm","",$this->nrm_pasien);		
	
		echo $poliname->getHtml();
		echo $polislug->getHtml();	
		echo $protoslug->getHtml();	
		echo $implement->getHtml();
		echo $page->getHtml();
		echo $noreg->getHtml();		
		echo $nrm->getHtml();
		echo $nama->getHtml();
		
		echo $this->uitable->getHtml ();
		echo $modal->getHtml ();
		loadLibrary ( "smis-libs-function-javascript" );
		echo addJS ( "framework/bootstrap/js/bootstrap-datepicker.js" );
		echo addJS ( "framework/smis/js/table_action.js" );
		echo addCSS ( "framework/bootstrap/css/datepicker.css" );
		echo addJS ( "medical_record/resource/js/plebitis.js",false );
		
	}
	
	public function superCommand($super_command) {
		$array=array ('Nama','Jabatan',"NIP" );
		$dktable = new Table ($array);
		$dktable->setName ( "plebitis_perawat" );
		$dktable->setModel ( Table::$SELECT );
		$dkadapter = new SimpleAdapter ();
		$dkadapter->add ( "Jabatan", "nama_jabatan" );
		$dkadapter->add ( "Nama", "nama" );
		$dkadapter->add ( "NIP", "nip" );
		$dkresponder_perawat = new EmployeeResponder($this->db, $dktable, $dkadapter, "perawat") ;
		
		$super = new SuperCommand ();
		$super->addResponder ( "plebitis_perawat", $dkresponder_perawat );
		$init = $super->initialize ();
		if ($init != null) {
			echo $init;
			return;
		}
	}
}

?>