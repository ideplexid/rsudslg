<?php
require_once 'smis-framework/smis/template/ModulTemplate.php';
require_once ("smis-base/smis-include-service-consumer.php");
require_once 'medical_record/class/responder/RencanaBPJSResponder.php';

class RencanaKeperawatanTemplate extends ModulTemplate {
	protected $db;
	protected $polislug;
	protected $page;
	protected $protoslug;
	protected $protoname;
	protected $protoimplement;
	protected $action;
	protected $dbtable;
	protected $uitable;
	protected $noreg_pasien;
	protected $nama_pasien;
	protected $nrm_pasien;
	protected $gol_umur;
	protected $jk;
	protected $kunjungan;
	protected $carabayar;
	protected $status_bpjs;
	protected $plafon_bpjs;
	protected $diagnosa_bpjs;
	protected $tindakan_bpjs;
	
	
	public function __construct($db, $polislug = "all", $noreg = "", $nrm = "", $nama = "", $page = "medical_record", $action = "rencana_bpjs", $protoslug = "", $protoname = "", $protoimplement = "",$carabayar = "Umum") {
		$this->db = $db;
		$this->noreg_pasien = $noreg;
		$this->nama_pasien = $nama;
		$this->nrm_pasien = $nrm;
		$this->polislug = $polislug;
		$this->dbtable = new DBTable ( $this->db, "smis_mr_rencana" );
		$this->page = $page;
		$this->protoslug = $protoslug;
		$this->protoimplement = $protoimplement;
		$this->protoname = $protoname;
		$this->action = $action;
		$this->carabayar = $carabayar;
		$this->dbtable->addCustomKriteria ( "noreg_pasien", "='" . $noreg. "'" );
		
		$array=array ('Tanggal',"Rencana","Biaya","Keterangan");
		$this->uitable = new Table ($array , "", NULL, true );
		$this->uitable->setName ( $action );
		$this->setStatus($noreg);
		$print=new Button();
		$print->setClass("btn-primary");
		$print->setIcon("fa fa-print");
		$print->setIsButton(Button::$ICONIC);
		$print->setAction("rencana_bpjs.print()");
		$this->uitable->addFooterButton($print);
		$this->uitable->setPrintButtonEnable(false);
		$this->uitable->setEditButtonEnable(false);
		$this->uitable->setReloadButtonEnable(false);
		
		if($this->status_bpjs=="1"){
			$this->uitable->setAction(false);			
		}else{
			$lock=new Button();
			$lock->setClass("btn-primary");
			$lock->setIcon("fa fa-lock");
			$lock->setIsButton(Button::$ICONIC);
			$lock->setAction("rencana_bpjs.kunci()");
			$this->uitable->addFooterButton($lock);
		}
			
	}
	
	
	private function setStatus($noreg){
		if ($noreg != "") {
			$data_post = array ();
			$data_post['command']="edit";
			$data_post['id']=$noreg;
			$service = new ServiceConsumer ( $this->db, "get_registered", $data_post,"registration" );
			$service->execute ();
			$data = $service->getContent ();
			if($data!=null){
				$this->umur = $data ['umur'];
				$this->status_bpjs=$data ['kunci_bpjs'];
				$this->plafon_bpjs=$data ['plafon_bpjs'];
				$this->tindakan_bpjs=$data ['tindakan_bpjs'];
				$this->diagnosa_bpjs=$data ['diagnosa_bpjs'];
				$this->keterangan_bpjs=$data ['keterangan_bpjs'];
			}
		}
	}
	
	public function command($command) {
		$adapter = new SimpleAdapter();
		$adapter->setUseNumber(true, "No.","back.");
		$adapter->add ( "Tanggal", "tanggal", "date d M Y" );
		$adapter->add ( "Pasien", "nama_pasien" );
		$adapter->add ( "NRM", "nrm_pasien", "digit8" );
		$adapter->add ( "Noreg", "noreg_pasien" );
		$adapter->add ( "Rencana", "rencana" );
		$adapter->add ( "Keterangan", "keterangan" );
		$adapter->add ( "Biaya", "biaya","money Rp." );
		
		$dbres = new RencanaBPJSResponder( $this->dbtable, $this->uitable, $adapter );
		$data = $dbres->command ( $_POST ['command'] );
		echo json_encode ( $data );
		return;
	}
	public function phpPreLoad() {
		
		$query="SELECT SUM(biaya) as total FROM smis_mr_rencana WHERE prop!='del' AND noreg_pasien='".$this->noreg_pasien."';";
		$total=$this->db->get_var($query); 
		
		$this->uitable->addModal("plafon_bpjs", "money", "Plafon BPJS", $this->plafon_bpjs,"y",NULL,true);
		$this->uitable->addModal("total_rencana", "money", "Total Rencana", $total,"y",NULL,true);
		$this->uitable->addModal("sisa_plafon", "money", "Sisa Plafon", $this->plafon_bpjs - $total,"y",NULL,true);
		$this->uitable->addModal("diagnosa_bpjs", "text", "Diagnosa", $this->diagnosa_bpjs);
		$this->uitable->addModal("tindakan_bpjs", "text", "Tindakan", $this->tindakan_bpjs);
		$this->uitable->addModal("keterangan_bpjs", "text", "Keterangan", $this->keterangan_bpjs);
		$form=$this->uitable->getModal()->getForm();
		$form->setTitle(" Rencana Keperawatan BPJS - ".$this->nama_pasien." [ ".ArrayAdapter::format("only-digit8", $this->nrm_pasien)." ] ");
		$this->uitable->clearContent();
		$this->uitable->addModal ( "id", "hidden", "", "" );
		$this->uitable->addModal ( "nama_pasien", "hidden", "", $this->nama_pasien, "n", null, true );
		$this->uitable->addModal ( "nrm_pasien", "hidden", "", $this->nrm_pasien, "n", null, true );
		$this->uitable->addModal ( "noreg_pasien", "hidden", "", $this->noreg_pasien, "n", null, true );
		$this->uitable->addModal ( "tanggal", "date", "Tanggal", date ( "Y-m-d" ),"n",NULL,false,NULL,false,"rencana" );
		$this->uitable->addModal ( "rencana", "text", "Rencana", "","n",NULL,false,NULL,true,"biaya" );
		$this->uitable->addModal ( "biaya", "money", "Biaya", "" ,"n",NULL,false,NULL,false,"keterangan");
		$this->uitable->addModal ( "keterangan", "text", "Keterangan", "","n",NULL,false,NULL,false,"save" );
		
		$modal = $this->uitable->getModal ();
		$modal->setComponentSize(Modal::$MEDIUM);
		$modal->setTitle ( "Rencana" );
		if($this->status_bpjs=="0"){
			echo addCSS("medical_record/resource/css/lock_layanan.css",false);
		}
		echo $form->getHtml();
		
		echo $this->uitable->getHtml ();
		echo $modal->getHtml ();
	}
	public function jsLoader() {
		echo addJS ( "framework/bootstrap/js/bootstrap-datepicker.js" );
		echo addJS ( "framework/smis/js/table_action.js" );
		loadLibrary ( "smis-libs-function-javascript" );
	}
	public function cssLoader() {
		echo addCSS ( "framework/bootstrap/css/datepicker.css" );
	}
	
	
	/* when it's star build */
	public function jsPreLoad() {
		?>
<script type="text/javascript">

		var <?php echo $this->action; ?>;
		var rencana_bpjs_noreg="<?php echo $this->noreg_pasien; ?>";
		var rencana_bpjs_nama_pasien="<?php echo $this->nama_pasien; ?>";
		var rencana_bpjs_nrm_pasien="<?php echo $this->nrm_pasien; ?>";
		var rencana_bpjs_polislug="<?php echo $this->polislug; ?>";
		var rencana_bpjs_the_page="<?php echo $this->page; ?>";
		var rencana_bpjs_the_protoslug="<?php echo $this->protoslug; ?>";
		var rencana_bpjs_the_protoname="<?php echo $this->protoname; ?>";
		var rencana_bpjs_the_protoimplement="<?php echo $this->protoimplement; ?>";
		$(document).ready(function() {
			$(".mydate").datepicker();			
			var column=new Array("id","tanggal","rencana","biaya","keterangan",
								"nama_pasien","noreg_pasien",
								"nrm_pasien","diagnosa",'keterangan',
								"plafon_bpjs","total_rencana","sisa_plafon");
			<?php echo $this->action; ?>=new TableAction("<?php echo $this->action; ?>",rencana_bpjs_the_page,"<?php echo $this->action; ?>",column);
			<?php echo $this->action; ?>.setPrototipe(rencana_bpjs_the_protoname,rencana_bpjs_the_protoslug,rencana_bpjs_the_protoimplement);
			<?php echo $this->action; ?>.setEnableAutofocus(true);
			<?php echo $this->action; ?>.setNextEnter();
			<?php echo $this->action; ?>.setEditClearForNoClear(true);
			<?php echo $this->action; ?>.addNoClear("tanggal");
			<?php echo $this->action; ?>.addNoClear("plafon_bpjs");
			<?php echo $this->action; ?>.addNoClear("total_rencana");
			<?php echo $this->action; ?>.addNoClear("sisa_plafon");
			<?php echo $this->action; ?>.setFocusOnMultiple("rencana");
			
			<?php echo $this->action; ?>.getRegulerData=function(){
				var reg_data={	
						page:this.page,
						action:this.action,
						super_command:this.super_command,
						prototype_name:this.prototype_name,
						prototype_slug:this.prototype_slug,
						prototype_implement:this.prototype_implement,
						polislug:rencana_bpjs_polislug,
						noreg_pasien:rencana_bpjs_noreg,
						nama_pasien:rencana_bpjs_nama_pasien,
						nrm_pasien:rencana_bpjs_nrm_pasien
						};
				return reg_data;
			};

			<?php echo $this->action; ?>.kunci=function(){
				var a=this.getRegulerData();
				a['command']="lock_bpjs";
				$.post("",a,function(res){
					layanan.select($("#id_antrian").val());
				});
			};
			
			<?php echo $this->action; ?>.setEnableAutofocus(true);
			<?php echo $this->action; ?>.setNextEnter();
			<?php echo $this->action; ?>.setMultipleInput(true);
			<?php echo $this->action; ?>.view();

			<?php echo $this->action; ?>.del=function(id){
				var self=this;
				var del_data=this.getDelData(id);	
				bootbox.confirm("Anda yakin Ingin Menghapus ?", function(result) {
				   if(result){
					   showLoading();
					    $.post('',del_data,function(res){
							var json=getContent(res);
							if(json==null) return;
							self.view();
							dismissLoading();
							var plafon=Number(getMoney("#rencana_bpjs_plafon_bpjs"));
							var sisa=plafon-Number(json.total_rencana);
							setMoney("#rencana_bpjs_total_rencana",Number(json.total_rencana));
							setMoney("#rencana_bpjs_sisa_plafon",sisa);
							self.postAction(id);
						});
				   }
				}); 
			};
						
			
			<?php echo $this->action; ?>.save=function (){
				if(!this.cekSave()){
					return;
				}
				var self=this;
				var a=this.getSaveData();				
				var sisa=Number(getMoney("#rencana_bpjs_sisa_plafon"));
				var input=Number(a['biaya']);
				if(input>sisa){
					var msg="Plafon Tidak Mencukupi";
					var id_alt="#modal_alert_"+this.prefix+"_add_form";		
					var warn='<div class="alert alert-block alert-info "><h4>Pemberitahuan</h4>'+msg+'</div>';
					$(id_alt).html(warn);
					$(id_alt).focus();
					return;
				}			
				showLoading();
				$.ajax({url:"",data:a,type:'post',success:function(res){
					var json=getContent(res);
					if(json==null) {
						$("#"+self.prefix+"_add_form").smodal('hide');
						return;		
					}else if(json.success=="1"){
						if(self.multiple_input) {
							if(self.focus_on_multi!=""){
								$('#'+self.action+'_'+self.focus_on_multi).focus();
							}else{
								$('#'+self.action+'_add_form').find('[autofocus]').focus();					
							}
						}else {
							$("#"+self.prefix+"_add_form").smodal('hide');
						}

						var plafon=Number(getMoney("#rencana_bpjs_plafon_bpjs"));
						var sisa=plafon-Number(json.total_rencana);
						setMoney("#rencana_bpjs_total_rencana",Number(json.total_rencana));
						setMoney("#rencana_bpjs_sisa_plafon",sisa);
						self.view();
						self.clear();
					}
					dismissLoading();
					self.postAction(a['id']);
				}});
				this.aftersave();
			};
			
		});
		</script>
<?php
	}
	
	public function superCommand($super_command) {
		$array=array ("Kode",'DTD','Nama',"Sebab" );
		$muitable = new Table ( $array);
		$muitable->setModel ( Table::$SELECT );
		$muitable->setName ( "mr_icd" );
		$madapter = new SimpleAdapter ();
		$madapter->add ( "Nama", "nama" );
		$madapter->add ( "Kode", "icd" );
		$madapter->add ( "DTD", "dtd" );
		$madapter->add ( "Sebab", "sebab" );
		$dbtable = new DBTable ( $this->db, "smis_mr_icd" );
		$mresponder = new DBResponder ( $dbtable, $muitable, $madapter );
		
		$super = new SuperCommand ();
		$super->addResponder ( "mr_dokter", $dkresponder );
		$super->addResponder ( "mr_pasien", $presponder );
		$super->addResponder ( "mr_icd", $mresponder );
		$init = $super->initialize ();
		if ($init != null) {
			echo $init;
			return;
		}
	}
}

?>