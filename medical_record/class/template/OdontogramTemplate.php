<?php
require_once 'smis-framework/smis/template/ModulTemplate.php';
require_once ("smis-base/smis-include-service-consumer.php");
require_once 'smis-libs-hrd/EmployeeResponder.php';

class OdontogramTemplate extends ModulTemplate {
	protected $db;
	protected $mode;
	protected $polislug;
	protected $page;
	protected $protoslug;
	protected $protoname;
	protected $protoimplement;
	protected $action;
	protected $dbtable;
	protected $uitable;
	protected $noreg_pasien;
	protected $nama_pasien;
	protected $nrm_pasien;
	protected $list_pesan;
	protected $list_hasil;
	protected $umur;
	protected $jk;
	protected $kunjungan;
	protected $carabayar;
	protected $id_antrian;
	public static $MODE_DAFTAR = "pendaftaran";
	public static $MODE_PERIKSA = "pemeriksaan";
	
	public function __construct($db, $mode, $polislug = "all", $noreg = "", $nrm = "", $nama = "", $page = "medical_record", $action = "odontogram", $protoslug = "", $protoname = "", $protoimplement = "", $jenis_umur = "TGL LAHIR TDK VALID", $jk = "0", $id_antrian="0", $kunjungan = "Baru", $carabayar = "Umum") {
		$this->db = $db;
		$this->mode = $mode;
		$this->noreg_pasien = $noreg;
		$this->nama_pasien = $nama;
		$this->umur = $jenis_umur;
		$this->nrm_pasien = $nrm;
		$this->polislug = $polislug;
		$this->id_antrian= $id_antrian;
		$this->dbtable = new DBTable ( $this->db, "smis_mr_odontogram" );
		$this->page = $page;
		$this->protoslug = $protoslug;
		$this->protoimplement = $protoimplement;
		$this->protoname = $protoname;
		$this->action = $action;
		$this->jk = $jk;
		$this->kunjungan = $kunjungan;
		$this->carabayar = $carabayar;
		//if ($polislug != "all")
			//$this->dbtable->addCustomKriteria ( "ruangan", "='" . $polislug . "'" );
		if ($nrm != "")
			$this->dbtable->addCustomKriteria ( "nrm_pasien", "='" . $nrm. "'" );
		
		$array=array ('Tanggal','NRM',"Pasien","L/P","Dokter","Ruangan" );
		$this->uitable = new Table ($array , " Odontogram " . ($this->polislug == "all" ? "" : ucfirst ( $this->protoname )), NULL, true );
		$this->uitable->setName ( $action );
		if ($this->mode == self::$MODE_PERIKSA) {
			$this->uitable->setAddButtonEnable ( false );
			$this->uitable->setDelButtonEnable ( false );
		}
	}
	public function command($command) {
		$adapter = new SimpleAdapter ();
		$adapter->add ( "Tanggal", "tanggal", "date d M Y" );
		$adapter->add ( "Pasien", "nama_pasien" );
		$adapter->add ( "NRM", "nrm_pasien", "digit8" );
		$adapter->add ( "Ruangan", "ruangan", "unslug" );
		$adapter->add ( "Noreg", "noreg_pasien" );
		$adapter->add ( "Dokter", "nama_dokter" );
		$adapter->add ( "L/P", "jk", "trivial_0_L_P" );
		$dbres = new DBResponder( $this->dbtable, $this->uitable, $adapter );
		$dbres->setDebuggable(true);
		
		$data = $dbres->command ( $_POST ['command'] );
		echo json_encode ( $data );
		return;
	}
	public function phpPreLoad() {
		
		$kelas = new OptionBuilder ();
		$kl = json_decode ( getSettings ( $this->db, "smis-hospital-class", null ), true );
		foreach ( $kl as $kl_key => $kl_v ) {
			$kelas->add ( $kl_key, $kl_v );
		}
		
		
		$jtensi=new OptionBuilder();
		$jtensi	->addSingle("Normal")
				->addSingle("Hipertensi")
				->addSingle("Hipotensi");
		
		$this->uitable->addModal ( "id", "hidden", "", "" );
		$this->uitable->addModal ( "id_antrian", "text", "Andtrian", $this->id_antrian );
		$this->uitable->addModal ( "jk", "hidden", "", $this->jk );
		$this->uitable->addModal ( "tanggal", "date", "Tanggal", date ( "Y-m-d" ) );
		
		$this->uitable->addModal("umur", "text", "Umur ", $this->umur);
		$this->uitable->addModal("tensi", "text", "Tensi (mmHg) ", "");
		$this->uitable->addModal("jtensi", "select", "Jenis", $jtensi->getContent());
		$this->uitable->addModal("jantung", "checkbox", "Jantung", "");
		$this->uitable->addModal("diabetes", "checkbox", "Diabetes", "");
		$this->uitable->addModal("gdp", "text", "GDP", "");
		$this->uitable->addModal("2jam", "text", "2 Jam", "");
		$this->uitable->addModal("gda", "text", "GDA", "");
		$this->uitable->addModal("hemofilia", "checkbox", "Hemofilia", "");
		$this->uitable->addModal("hepatitis", "checkbox", "Hepatitis", "");
		$this->uitable->addModal("penyakit_lain", "text", "Penyakit Lain", "");
		$this->uitable->addModal("alergi_makanan", "text", "Alergi Makanan", "");
		$this->uitable->addModal("alergi_obat", "text", "Alergi Obat", "");
		
		
		if ($this->mode == self::$MODE_DAFTAR) {
			
			if ($this->noreg_pasien != "") {
				$this->uitable->addModal ( "nama_pasien", "hidden", "", $this->nama_pasien, "n", null, true );
				$this->uitable->addModal ( "nrm_pasien", "hidden", "", $this->nrm_pasien, "n", null, true );
				$this->uitable->addModal ( "noreg_pasien", "hidden", "", $this->noreg_pasien, "n", null, true );
			} else {
				$this->uitable->addModal ( "nama_pasien", "chooser-" . $this->action . "-odontogram_pasien", "Pasien", $this->nama_pasien, "n", null, true );
				$this->uitable->addModal ( "nrm_pasien", "text", "NRM", $this->nrm_pasien, "n", null, true );
				$this->uitable->addModal ( "noreg_pasien", "text", "No Reg", $this->noreg_pasien, "n", null, true );
			}
			$this->uitable->addModal("", "label", "<strong>DATA DOKTER</strong>", "");
			$this->uitable->addModal ( "nama_dokter", "chooser-" . $this->action . "-odontogram_dokter", "Dokter", "", "n", null, true );
			$this->uitable->addModal ( "id_dokter", "text", "NIP Dokter", "", "n", null, true );
			if ($this->polislug == "all"){
                loadClass ( "ServiceProviderList" );
                $service = new ServiceProviderList ( $this->db, "push_antrian" );
                $service->execute ();
                $ruangan = $service->getContent ();
				$this->uitable->addModal ( "ruangan", "select", "Asal Ruangan", $ruangan );
            }else{
                $this->uitable->addModal ( "ruangan", "text", "Asal Ruangan", $this->polislug, "n", null, true );
            }
			
		} else {
			$this->uitable->addModal("", "label", "<strong>DATA PASIEN</strong>", "");
			$this->uitable->addModal ( "nama_pasien", "text", "Pasien", $this->nama_pasien, "n", null, true );
			$this->uitable->addModal ( "noreg_pasien", "text", "No Reg", $this->noreg_pasien, "n", null, true );
			$this->uitable->addModal ( "nrm_pasien", "text", "NRM", $this->nrm_pasien, "n", null, true );
			
			$this->uitable->addModal("", "label", "<strong>DATA DOKTER</strong>", "");
			$this->uitable->addModal ( "nama_dokter", "text", "Dokter", "", "n", null, true );
			$this->uitable->addModal ( "id_dokter", "text", "NIP Dokter", "", "n", null, true );
			$this->uitable->addModal ( "ruangan", "text", "Asal Ruangan", "", "n", null, true );
		}
		
		$modal = $this->uitable->getModal ();
		$modal->setComponentSize(Modal::$MEDIUM);
		$modal->setTitle ( "Odontogram" );
		echo $this->uitable->getHtml ();
		echo $modal->getHtml ();
	}
	public function jsLoader() {
		echo addJS ( "framework/bootstrap/js/bootstrap-datepicker.js" );
		echo addJS ( "framework/smis/js/table_action.js" );
		loadLibrary ( "smis-libs-function-javascript" );
	}
	public function cssLoader() {
		echo addCSS ( "framework/bootstrap/css/datepicker.css" );
	}
	
	
	/* when it's star build */
	public function jsPreLoad() {
		?>
<script type="text/javascript">

		var <?php echo $this->action; ?>;
		var odontogram_dokter;
		var odontogram_pasien;
		var odontogram_noreg="<?php echo $this->noreg_pasien; ?>";
		var odontogram_nama_pasien="<?php echo $this->nama_pasien; ?>";
		var odontogram_nrm_pasien="<?php echo $this->nrm_pasien; ?>";
		var odontogram_polislug="<?php echo $this->polislug; ?>";
		var odontogram_the_page="<?php echo $this->page; ?>";
		var odontogram_the_protoslug="<?php echo $this->protoslug; ?>";
		var odontogram_the_protoname="<?php echo $this->protoname; ?>";
		var odontogram_the_protoimplement="<?php echo $this->protoimplement; ?>";
		var odontogram_id_antrian="<?php echo $this->id_antrian; ?>";
		$(document).ready(function() {
			$(".mydate").datepicker();
			
			odontogram_pasien=new TableAction("odontogram_pasien",odontogram_the_page,"<?php echo $this->action; ?>",new Array());
			odontogram_pasien.setSuperCommand("odontogram_pasien");
			odontogram_pasien.setPrototipe(odontogram_the_protoname,odontogram_the_protoslug,odontogram_the_protoimplement);
			odontogram_pasien.selected=function(json){
				var nama=json.nama_pasien;
				var nrm=json.nrm;
				var noreg=json.id;		
				$("#<?php echo $this->action; ?>_nama_pasien").val(nama);
				$("#<?php echo $this->action; ?>_nrm_pasien").val(nrm);
				$("#<?php echo $this->action; ?>_noreg_pasien").val(noreg);
			};
			
			odontogram_dokter=new TableAction("odontogram_dokter",odontogram_the_page,"<?php echo $this->action; ?>",new Array());
			odontogram_dokter.setSuperCommand("odontogram_dokter");
			odontogram_dokter.setPrototipe(odontogram_the_protoname,odontogram_the_protoslug,odontogram_the_protoimplement);
			odontogram_dokter.selected=function(json){
				var nama=json.nama;
				var nip=json.id;		
				$("#<?php echo $this->action; ?>_nama_dokter").val(nama);
				$("#<?php echo $this->action; ?>_id_dokter").val(nip);
			};

			

			var column=new Array("id","tanggal","jk","umur","tensi","jtensi","jantung","diabetes","gdp","2jam","gda","hemofilia","hepatitis","penyakit_lain","alergi_makanan","alergi_obat","odontogram","d_odontogram","ruangan","nama_pasien","noreg_pasien","nrm_pasien","id_dokter","nama_dokter");
			<?php echo $this->action; ?>=new TableAction("<?php echo $this->action; ?>",odontogram_the_page,"<?php echo $this->action; ?>",column);
			<?php echo $this->action; ?>.setPrototipe(odontogram_the_protoname,odontogram_the_protoslug,odontogram_the_protoimplement);
			<?php echo $this->action; ?>.getRegulerData=function(){
				var reg_data={	
						page:this.page,
						action:this.action,
						super_command:this.super_command,
						prototype_name:this.prototype_name,
						prototype_slug:this.prototype_slug,
						prototype_implement:this.prototype_implement,
						polislug:odontogram_polislug,
						noreg_pasien:odontogram_noreg,
						nama_pasien:odontogram_nama_pasien,
						nrm_pasien:odontogram_nrm_pasien,
						id_antrian:odontogram_id_antrian
						};
				return reg_data;
			};
			<?php echo $this->action; ?>.view();
			
		});
		</script>
<?php
	}
	
	public function superCommand($super_command) {
		$array=array ('Nama','Jabatan',"NIP" );
		$dktable = new Table ($array);
		$dktable->setName ( "odontogram_dokter" );
		$dktable->setModel ( Table::$SELECT );
		$dkadapter = new SimpleAdapter ();
		$dkadapter->add ( "Jabatan", "nama_jabatan" );
		$dkadapter->add ( "Nama", "nama" );
		$dkadapter->add ( "NIP", "nip" );
		$dkresponder = new EmployeeResponder($this->db, $dktable, $dkadapter, "dokter") ;
		
		/* PASIEN */
		$aname=array ('Nama','NRM',"No Reg" );
		$ptable = new Table ( $aname);
		$ptable->setName ( "odontogram_pasien" );
		$ptable->setModel ( Table::$SELECT );
		$padapter = new SimpleAdapter ();
		$padapter->add ( "Nama", "nama_pasien" );
		$padapter->add ( "NRM", "nrm", "digit8" );
		$padapter->add ( "No Reg", "id" );
		$presponder = new ServiceResponder ( $this->db, $ptable, $padapter, "get_registered" );
		
		$super = new SuperCommand ();
		$super->addResponder ( "odontogram_dokter", $dkresponder );
		$super->addResponder ( "odontogram_pasien", $presponder );
		$init = $super->initialize ();
		if ($init != null) {
			echo $init;
			return;
		}
	}
}

?>