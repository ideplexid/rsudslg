<?php
require_once 'smis-framework/smis/template/ModulTemplate.php';
require_once ("smis-base/smis-include-service-consumer.php");
require_once 'smis-libs-hrd/EmployeeResponder.php';

class InputDataDiagnosaRehabMedisTemplate extends ModulTemplate {
	protected $db;
	protected $mode;
	protected $polislug;
	protected $page;
	protected $protoslug;
	protected $protoname;
	protected $protoimplement;
	protected $action;
	protected $dbtable;
	protected $uitable;
	protected $noreg_pasien;
	protected $nama_pasien;
	protected $nrm_pasien;
	protected $jk;

	public function __construct(
		$db, 
		$polislug, 
		$noreg, 
		$nrm, 
		$nama,
		$jk, 
		$page, 
		$action, 
		$protoslug, 
		$protoname, 
		$protoimplement
	) {
		$this->db = $db;
		$this->noreg_pasien = $noreg;
		$this->nama_pasien = $nama;
		$this->nrm_pasien = $nrm;
		$this->jk = $jk;
		$this->polislug = $polislug;
		$this->dbtable = new DBTable($this->db, "smis_mr_diagnosa_rehab_medis");
		$this->page = $page;
		$this->protoslug = $protoslug;
		$this->protoimplement = $protoimplement;
		$this->protoname = $protoname;
		$this->action = $action;
		if ($noreg != ""){
			$this->dbtable->addCustomKriteria("noreg_pasien", "='" . $noreg . "'");
			$this->dbtable->addCustomKriteria("ruangan", "='" . $polislug . "'");
        }
        $columns = array("No.", "Tanggal", "NRM", "No. Reg.", "Pasien", "Jenis Kelamin", "Dokter", "Diagnosa", "Ruangan");
		$this->uitable = new Table(
			$columns, 
			"Diagnosa Rehab Medik " . ($this->polislug == "all" ? "" : ucfirst ( $this->protoname )), 
			null, 
			true 
		);
        $this->uitable->setName($action);
	}
	public function command($command) {
		$adapter = new SimpleAdapter(true, "No.");
		$adapter->add ( "Tanggal", "tanggal", "date d M Y" );
		$adapter->add ( "Pasien", "nama_pasien" );
		$adapter->add ( "NRM", "nrm_pasien", "digit8" );
		$adapter->add ( "No. Reg.", "noreg_pasien" );
		$adapter->add ( "Dokter", "nama_dokter" );
		$adapter->add ( "Diagnosa", "diagnosa_rehab_medis" );
		$adapter->add ( "Ruangan", "ruangan", "unslug" );
		$adapter->add ( "Jenis Kelamin", "jk", "trivial_0_Laki-Laki_Perempuan" );
		require_once ("smis-base/smis-include-duplicate.php");
		$responder = new DuplicateResponder( 
			$this->dbtable, 
			$this->uitable, 
			$adapter 
		);
		$responder->setDuplicate(false, "");
		$responder->setAutonomous(getSettings($this->db,"smis_autonomous_id",""));
        $data = $responder->command ( $_POST ['command'] );
		echo json_encode ( $data );
		return;
	}
	public function phpPreLoad() {		
		$this->uitable->addModal ( "id", "hidden", "", "" );
		$this->uitable->addModal ( "jk", "hidden", "", $this->jk );
		$this->uitable->addModal ( "nama_pasien", "hidden", "", $this->nama_pasien, "n", null, true );
		$this->uitable->addModal ( "nrm_pasien", "hidden", "", $this->nrm_pasien, "n", null, true );
		$this->uitable->addModal ( "noreg_pasien", "hidden", "", $this->noreg_pasien, "n", null, true );
		$this->uitable->addModal ( "tanggal", "date", "Tanggal", date ( "Y-m-d" ) );
		$this->uitable->addModal ( "", "label", "<strong>DATA DOKTER / PSIKOLOG</strong>", "");
		$this->uitable->addModal ( "nama_dokter", "chooser-" . $this->action . "-drm_dokter-Pilih Dokter / Psikolog", "Dokter / Psikolog", "", "n", null, false, null, true );
		$this->uitable->addModal ( "id_dokter", "text", "NIP Dokter / Psikolog", "", "n", null, true );
		$this->uitable->addModal ( "", "label", "<strong>DIAGNOSA</strong>", "" );
		$this->uitable->addModal ( "diagnosa_rehab_medis", "chooser-" . $this->action . "-drm_icd-ICD Rehab Medik", "Diagnosa Rehab Medik", "", "y", null, true );
		$this->uitable->addModal ( "kode_diagnosa", "hidden", "", "" );
		$this->uitable->addModal ( "grup_diagnosa", "hidden", "", "" );
		$this->uitable->addModal ( "ruangan", "text", "Ruangan", $this->polislug, "n", null, true );		
		$modal = $this->uitable->getModal ();
		$modal->setComponentSize(Modal::$MEDIUM);
		$modal->setTitle ( "Diagnosa Rehab Medik" );
		echo $this->uitable->getHtml ();
		echo $modal->getHtml ();
	}
	public function jsLoader() {
		echo addJS ( "framework/bootstrap/js/bootstrap-datepicker.js" );
		echo addJS ( "framework/smis/js/table_action.js" );
		loadLibrary ( "smis-libs-function-javascript" );
	}
	public function cssLoader() {
		echo addCSS ( "framework/bootstrap/css/datepicker.css" );
	}

	public function jsPreLoad() {
		?>
<script type="text/javascript">
		var <?php echo $this->action; ?>;
		var drm_dokter;
		var drm_icd;
		var drm_noreg = "<?php echo $this->noreg_pasien; ?>";
		var drm_nama_pasien = "<?php echo $this->nama_pasien; ?>";
		var drm_nrm_pasien = "<?php echo $this->nrm_pasien; ?>";
		var drm_polislug = "<?php echo $this->polislug; ?>";
		var drm_the_page = "<?php echo $this->page; ?>";
		var drm_the_protoslug = "<?php echo $this->protoslug; ?>";
		var drm_the_protoname = "<?php echo $this->protoname; ?>";
		var drm_the_protoimplement = "<?php echo $this->protoimplement; ?>";
		$(document).ready(function() {
			$(".mydate").datepicker();
			$('#<?php echo $this->action; ?>_nama_dokter').typeahead({
				minLength:3,
		        source: function (query, process) {
			    	var data_dokter = drm_dokter.getViewData();
			    	data_dokter["kriteria"] = $('#<?php echo $this->action; ?>_nama_dokter').val();
		        	var $items = new Array;
		        	items = [""];				                
		          	$.ajax({
		            	url: '',
		            	type: 'POST',
		            	data: data_dokter,
		            	success: function(res) {
		              		var json=getContent(res);
		              		var the_data_proses=json.d.data;
		               		$items = [""];	
		              		$.map(the_data_proses, function(data){
		                  		var group;
		                  		group = {
		                      		id: data.id,
		                      		name: data.nama,                            
		                      		toString: function () {
		                    	  		return JSON.stringify(this);
		                      		},
									toLowerCase: function () {
										return this.name.toLowerCase();
									},
									indexOf: function (string) {
										return String.prototype.indexOf.apply(this.name, arguments);
									},
									replace: function (string) {
										var value = '';
										value +=  this.name;
										if(typeof(this.level) != 'undefined') {
											value += ' <span class="pull-right muted">';
											value += this.level;
											value += '</span>';
										}
										return String.prototype.replace.apply('<div class="typeaheadiv">' + value + '</div>', arguments);
									}
		                  		};
		                  		$items.push(group);
		              		});
		              		process($items);
		           		}
					});
		    	},
				updater: function (item) {
					var item = JSON.parse(item);
					$("#<?php echo $this->action; ?>_id_dokter").val(item.id);
					$("#<?php echo $this->action; ?>_kasus").focus();
					return item.name;
				}
			});

			$('#<?php echo $this->action; ?>_nama_icd').typeahead({
				minLength:3,
		        source: function (query, process) {
			    	var data_dokter=drm_icd.getViewData();
			     	data_dokter["kriteria"]=$('#<?php echo $this->action; ?>_nama_icd').val();
		         	var $items = new Array;
		           	$items = [""];				                
		          	$.ajax({
		            	url: '',
		            	type: 'POST',
		            	data: data_dokter,
		            	success: function(res) {
		              		var json=getContent(res);
		              		var the_data_proses=json.dbtable.data;
		               		$items = [""];	
		              		$.map(the_data_proses, function(data){
		                  		var group;
		                  		group = {
		                      		id: data.id,
		                      		name: data.nama,
		                      		kode: data.icd,    
		                      		sebab: data.sebab,                                
		                      		toString: function () {
		                    	  		return JSON.stringify(this);
		                      		},
		                      		toLowerCase: function () {
		                          		return this.name.toLowerCase();
		                      		},
		                      		indexOf: function (string) {
		                          		return String.prototype.indexOf.apply(this.name, arguments);
		                      		},
		                      		replace: function (string) {
		                          		var value = '';
		                          		value +=  this.name;
		                          		if(typeof(this.level) != 'undefined') {
		                              		value += ' <span class="pull-right muted">';
		                              		value += this.level;
		                              		value += '</span>';
		                          		}
		                          		return String.prototype.replace.apply('<div class="typeaheadiv">' + value + '</div>', arguments);
		                      		}
		                  		};
		                  		$items.push(group);
		              		});
		              		process($items);
		            	}
		          	});
		        },
		        updater: function (item) {
		            var item = JSON.parse(item);
					$("#<?php echo $this->action; ?>_kode_icd").val(item.kode);
					$("#<?php echo $this->action; ?>_sebab_sakit").val(item.sebab);
					$("#<?php echo $this->action; ?>_save").focus();
		            return item.name;
		        }
		    });
			
			drm_dokter = new TableAction("drm_dokter", mr_the_page, "<?php echo $this->action; ?>", new Array());
			drm_dokter.setSuperCommand("drm_dokter");
			drm_dokter.setPrototipe(drm_the_protoname, drm_the_protoslug, drm_the_protoimplement);
			drm_dokter.selected = function(json) {
				var nama=json.nama;
				var nip=json.id;		
				$("#<?php echo $this->action; ?>_nama_dokter").val(nama);
				$("#<?php echo $this->action; ?>_id_dokter").val(nip);
			};

			drm_icd=new TableAction("drm_icd", drm_the_page, "<?php echo $this->action; ?>", new Array());
			drm_icd.setSuperCommand("drm_icd");
			drm_icd.setPrototipe(drm_the_protoname, drm_the_protoslug, drm_the_protoimplement);
			drm_icd.selected = function(json){
				var nama=json.nama;
				var kode=json.icd;		
				var grup=json.grup;
				$("#<?php echo $this->action; ?>_diagnosa_rehab_medis").val(nama);
				$("#<?php echo $this->action; ?>_kode_diagnosa").val(kode);
				$("#<?php echo $this->action; ?>_grup_diagnosa").val(grup);
			};
					
			var column = new Array(
				"id",
				"tanggal",
				"jk",
				"ruangan",
				"nama_pasien",
				"noreg_pasien",
				"nrm_pasien",
				"diagnosa_rehab_medis",
				"kode_diagnosa",
				"grup_diagnosa",
				"id_dokter",
				"nama_dokter"
			);
			<?php echo $this->action; ?> = new TableAction("<?php echo $this->action; ?>", drm_the_page, "<?php echo $this->action; ?>", column);
			<?php echo $this->action; ?>.setPrototipe(drm_the_protoname, drm_the_protoslug, drm_the_protoimplement);
			<?php echo $this->action; ?>.getRegulerData = function() {
				var reg_data = {	
					page:this.page,
					action:this.action,
					super_command:this.super_command,
					prototype_name:this.prototype_name,
					prototype_slug:this.prototype_slug,
					prototype_implement:this.prototype_implement,
					polislug:drm_polislug,
					noreg_pasien:drm_noreg,
					nama_pasien:drm_nama_pasien,
					nrm_pasien:drm_nrm_pasien,
					jk:$("#<?php echo $this->action; ?>_jk").val()
				};
				return reg_data;
			};
			<?php echo $this->action; ?>.view();			
		});
		</script>
<?php
	}
	
	public function superCommand($super_command) {
        $super = new SuperCommand ();
        if ($super_command == 'drm_dokter') {
            $array=array ('Nama','Jabatan',"NIP" );
            $dktable = new Table ($array);
            $dktable->setName ( "drm_dokter" );
            $dktable->setModel ( Table::$SELECT );
            $dkadapter = new SimpleAdapter ();
            $dkadapter->add ( "Jabatan", "nama_jabatan" );
            $dkadapter->add ( "Nama", "nama" );
            $dkadapter->add ( "NIP", "nip" );
            $dkresponder = new EmployeeResponder($this->db, $dktable, $dkadapter, "dokter") ;
            $super->addResponder ( "drm_dokter", $dkresponder );
        }
        if ($super_command == 'drm_icd') {
            $array=array ("Kode",'DTD', 'Grup', 'Nama',"Sebab" );
            $muitable = new Table ( $array);
            $muitable->setModel ( Table::$SELECT );
            $muitable->setName ( "drm_icd" );
            $madapter = new SimpleAdapter ();
            $madapter->add ( "Nama", "nama" );
            $madapter->add ( "Kode", "icd" );
			$madapter->add ( "DTD", "dtd" );
			$madapter->add ( "Grup", "grup" );
            $madapter->add ( "Sebab", "sebab" );
            $dbtable = new DBTable ( $this->db, "smis_mr_icd_rehab_medis" );
            $mresponder = new DBResponder ( $dbtable, $muitable, $madapter );
            $super->addResponder ( "drm_icd", $mresponder );
        }
		$init = $super->initialize ();
		if ($init != null) {
			echo $init;
			return;
		}
	}
}
?>