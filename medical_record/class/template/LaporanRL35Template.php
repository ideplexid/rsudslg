<?php
require_once 'smis-framework/smis/template/ModulTemplate.php';
require_once ("smis-base/smis-include-service-consumer.php");
require_once "medical_record/class/service/RuanganService.php";
require_once 'smis-libs-hrd/EmployeeResponder.php';
class LaporanRL35Template extends ModulTemplate {
	protected $db;
	protected $polislug;
	protected $page;
	protected $protoslug;
	protected $protoname;
	protected $protoimplement;
	protected $action;
	protected $dbtable;
	protected $uitable;
	protected $noreg_pasien;
	protected $nama_pasien;
	protected $nrm_pasien;
	protected $alamat;
	protected $umur;
	protected $jk;
	protected $ruang_asal;
	protected $id_antrian;
	public function __construct($db, $polislug = "all", $noreg = "", $nrm = "", $nama = "", $alamat = "", $umur = "", $jk = "0", $id_antrian = "0", $ruang_asal = "", $page = "medical_record", $action = "lap_perinatologi", $protoslug = "", $protoname = "", $protoimplement = "") {
		$this->db = $db;
		$this->noreg_pasien = $noreg;
		$this->id_antrian = $id_antrian;
		
		$this->nama_pasien = $nama;
		$this->nrm_pasien = $nrm;
		$this->alamat = $alamat;
		$this->umur = $umur;
		$this->jk = $jk;
		$this->ruang_asal = $ruang_asal;
		$this->polislug = $polislug;
		$this->dbtable = new DBTable ( $this->db, "smis_mr_perinatologi" );
		$this->page = $page;
		$this->protoslug = $protoslug;
		$this->protoimplement = $protoimplement;
		$this->protoname = $protoname;
		$this->action = $action;
		if ($polislug != "all")
			$this->dbtable->addCustomKriteria ( "ruangan", "='" . $polislug . "'" );
		if ($noreg != "")
			$this->dbtable->addCustomKriteria ( "noreg_pasien", "='" . $noreg . "'" );
		$head=array ('NRM',"Pasien","No Register" );
		$this->uitable = new Table ($head , " Perinatologi " . ($this->polislug == "all" ? "" : ucfirst ( $this->protoname )), NULL, true );
		$this->uitable->setName ( $action );
		
		if ($this->page == "medical_record" && $this->action == "lap_perinatologi") {
			$this->uitable->setAddButtonEnable ( false );
			$this->uitable->setDelButtonEnable ( false );
		}
	}
	
	
	
	public function command($command) {
		$adapter = new SimpleAdapter ();
		$adapter->add ( "Pasien", "nama_pasien" );
		$adapter->add ( "NRM", "nrm_pasien", "digit8" );
		$adapter->add ( "No Register", "noreg_pasien", "digit8" );
		$dbres = new DBResponder ( $this->dbtable, $this->uitable, $adapter );
		$data = $dbres->command ( $_POST ['command'] );
		echo json_encode ( $data );
		return;
	}
	public function phpPreLoad() {
		
		if ($this->noreg_pasien != "") {
			$this->uitable->addModal ( "nama_pasien", "hidden", "", $this->nama_pasien, "n", null, true );
			$this->uitable->addModal ( "nrm_pasien", "hidden", "", $this->nrm_pasien, "n", null, true );
			$this->uitable->addModal ( "noreg_pasien", "hidden", "", $this->noreg_pasien, "n", null, true );
		} else {
			$this->uitable->addModal ( "nama_pasien", "chooser-" . $this->action . "-rl35_pasien", "Pasien", $this->nama_pasien, "n", null, true );
			$this->uitable->addModal ( "nrm_pasien", "text", "NRM", $this->nrm_pasien, "n", null, true );
			$this->uitable->addModal ( "noreg_pasien", "text", "No Reg", $this->noreg_pasien, "n", null, true );
		}
		if ($this->polislug == "all"){
			$service = new RuanganService ( $this->db );
			$service->execute ();
			$ruangan = $service->getContent ();
			$this->uitable->addModal ( "ruangan", "select", "Asal Ruangan", $ruangan );
		}else{
			$this->uitable->addModal ( "ruangan", "hidden", "", $this->polislug, "n", null, true );
		}
		$the_row = array ();
		$the_row ["id"] 	 = "";
		$the_row ['tanggal'] = date ( "Y-m-d" );
		
		$exist ['noreg_pasien'] = $this->noreg_pasien;
		$exist ['nama_pasien'] = $this->nama_pasien;
		$exist ['nrm_pasien'] = $this->nrm_pasien;
		$exist ['ruangan'] = $this->polislug;
		if ($this->dbtable->is_exist ( $exist )) {
			$row = $this->dbtable->select ( $exist );
			$the_row ["id"] 				= $row->id;
			$the_row ['tanggal'] 			= $row->tanggal;
			$the_row ['cara_datang']		= $row->cara_datang;
			$the_row ['asal_rujukan']		= $row->asal_rujukan;
			$the_row ['bayi_lahir_hidup']	= $row->bayi_lahir_hidup;
			$the_row ['kematian_perinatal']	= $row->kematian_perinatal;
			$the_row ['sebab_kematian']		= $row->sebab_kematian;
		} else {
			$exist ['noreg_pasien'] 		= $this->noreg_pasien;
			$exist ['nrm_pasien'] 			= $this->nrm_pasien;
			$exist ['nama_pasien'] 			= $this->nama_pasien;
			$exist ['tanggal'] 				= date("Y-m-d");
			$this->dbtable->insert($exist);
			$the_row ["id"] 				= $this->dbtable->get_inserted_id();
		}
				
		$this->uitable->addModal("id", "hidden", "", $the_row ['id']);
		$this->uitable->addModal("tanggal", "date", "Tanggal", $the_row ['tanggal'] ,"y",null,false,null,false,"cara_datang");
		$cara_datang_option = new OptionBuilder();
		if (array_key_exists('cara_datang', $the_row)) {
			if ($the_row['cara_datang'] == "Rujukan")
				$cara_datang_option->add("Rujukan", "Rujukan", "1");
			else
				$cara_datang_option->addSingle("Rujukan");
			if ($the_row['cara_datang'] == "Non-Rujukan")
				$cara_datang_option->add("Non-Rujukan", "Non-Rujukan", "1");
			else
				$cara_datang_option->addSingle("Non-Rujukan");
		} else {
			$cara_datang_option->addSingle("Rujukan");
			$cara_datang_option->add("Non-Rujukan", "Non-Rujukan", "1");
		}
		$this->uitable->addModal("cara_datang", "select", "Cara Datang", $cara_datang_option->getContent(), "y", null, false, null, false);
		$asal_rujukan_option = new OptionBuilder();
		$asal_rujukan_arr = array("", "RS", "Bidan", "Puskesmas", "Faskes Lainnya", "Non Medis");
		if (array_key_exists('asal_rujukan', $the_row)) {
			foreach ($asal_rujukan_arr as $ar) {
				if ($the_row['kematian_perinatal'] == $ar)
					$asal_rujukan_option->add($ar, $ar, "1");
				else
					$asal_rujukan_option->addSingle($ar);
			}
		} else {
			foreach ($asal_rujukan_arr as $ar)
				$asal_rujukan_option->addSingle($ar);
		}
		$this->uitable->addModal("asal_rujukan", "select", "Asal Rujukan", $asal_rujukan_option->getContent(), "y", null, false, null, false);
		$bayi_lahir_hidup_option = new OptionBuilder();
		$bayi_lahir_hidup_arr = array("", ">= 2500 gram", "< 2500 gram");
		if (array_key_exists('bayi_lahir_hidup', $the_row)) {
			foreach ($bayi_lahir_hidup_arr as $blh) {
				if ($the_row['bayi_lahir_hidup'] == $blh)
					$bayi_lahir_hidup_option->add($blh, $blh, "1");
				else
					$bayi_lahir_hidup_option->addSingle($blh);
			}
		} else {
			foreach ($bayi_lahir_hidup_arr as $blh)
				if ($blh == "")
					$bayi_lahir_hidup_option->add($blh, $blh, "1");	
				else
					$bayi_lahir_hidup_option->addSingle($blh);
		}
		$this->uitable->addModal("bayi_lahir_hidup", "select", "Bayi Lahir Hidup", $bayi_lahir_hidup_option->getContent(), "y", null, false, null, false);
		$kematian_perinatal_option = new OptionBuilder();
		$kematian_perinatal_arr = array("", "Kelahiran Mati", "Mati Neonatal < 7 hari");
		if (array_key_exists('kematian_perinatal', $the_row)) {
			foreach ($kematian_perinatal_arr as $kp) {
				if ($the_row['kematian_perinatal'] == $kp)
					$kematian_perinatal_option->add($kp, $kp, "1");
				else
					$kematian_perinatal_option->addSingle($kp);
			}
		} else {
			foreach ($kematian_perinatal_arr as $kp)
				$kematian_perinatal_option->addSingle($kp);
		}
		$this->uitable->addModal("kematian_perinatal", "select", "Kematian Perinatal", $kematian_perinatal_option->getContent(), "y", null, false, null, false);
		$sebab_kematian_perinatal_option = new OptionBuilder();
		$sebab_kematian_perinatal_arr = array("", "Asphyxia", "Trauma Kelahiran", "BBLR", "Tetanus Neonatorum", "Kelainan Congenital", "ISPA", "Diare", "Lain-lain");
		if (array_key_exists('sebab_kematian', $the_row)) {
			foreach ($sebab_kematian_perinatal_arr as $skp) {
				if ($the_row['sebab_kematian'] == $skp)
					$sebab_kematian_perinatal_option->add($skp, $skp, "1");
				else
					$sebab_kematian_perinatal_option->addSingle($skp);
			}
		} else {
			foreach ($sebab_kematian_perinatal_arr as $skp)
				$sebab_kematian_perinatal_option->addSingle($skp);
		}
		$this->uitable->addModal("sebab_kematian", "select", "Sebab Kematian", $sebab_kematian_perinatal_option->getContent(), "y", null, false, null, false);
		$this->uitable->addModal("dirujuk", "checkbox", "Dirujuk", "");

		$modal = $this->uitable->getModal ();
		$modal->setTitle ( "Laporan RL 3.5 - Perinatologi" );
		if ($this->page == "medical_record" && $this->action == "iklin") {
			echo $this->uitable->getHtml ();
		} else {
			echo "<div class='hide'>" . $this->uitable->getHtml () . "</div>";
			$modal->setAlwaysShow ( true );
		}
		echo $modal->joinFooterAndForm ()->getHtml ();
	}
	public function jsLoader() {
		echo addJS ( "framework/bootstrap/js/bootstrap-datepicker.js" );
		echo addJS ( "framework/smis/js/table_action.js" );
		loadLibrary ( "smis-libs-function-javascript" );
	}
	
	public function cssLoader() {
		echo addCSS ( "framework/bootstrap/css/datepicker.css" );
	}
	
	public function cssPreLoad() {
		?>
		<style type="text/css">
				#<?php echo $this->action;?>_add_form_form>div {clear: both; width: 100%;}
				#<?php echo $this->action; ?>_add_form_form>div label { width: 250px; }
		</style>
		<?php
	}
	
	/* when it's star build */
	public function jsPreLoad() {
		?>
		<script type="text/javascript">
		var <?php echo $this->action; ?>;
		var rl35_pasien;
		var rl35_noreg="<?php echo $this->noreg_pasien; ?>";
		var rl35_nama_pasien="<?php echo $this->nama_pasien; ?>";
		var rl35_nrm_pasien="<?php echo $this->nrm_pasien; ?>";
		var rl35_polislug="<?php echo $this->polislug; ?>";
		var rl35_the_page="<?php echo $this->page; ?>";
		var rl35_the_protoslug="<?php echo $this->protoslug; ?>";
		var rl35_the_protoname="<?php echo $this->protoname; ?>";
		var rl35_the_protoimplement="<?php echo $this->protoimplement; ?>";
		var rl35_id_antrian="<?php echo $this->id_antrian; ?>";
		var rl35_action="<?php echo $this->action; ?>";
		$(document).ready(function() {
			$('.mydate').datepicker();
			rl35_pasien=new TableAction("rl35_pasien",rl35_the_page,rl35_action,new Array());
			rl35_pasien.setSuperCommand("rl35_pasien");
			rl35_pasien.setPrototipe(rl35_the_protoname,rl35_the_protoslug,rl35_the_protoimplement);
			rl35_pasien.selected=function(json){
				var nama=json.nama_pasien;
				var nrm=json.nrm;
				var noreg=json.id;		
				$("#"+rl35_action+"_nama_pasien").val(nama);
				$("#"+rl35_action+"_nrm_pasien").val(nrm);
				$("#"+rl35_action+"_noreg_pasien").val(noreg);
			};

			if ($("#lap_perinatologi_cara_datang").val() == "Rujukan")
				$(".lap_perinatologi_asal_rujukan").show();
			else
				$(".lap_perinatologi_asal_rujukan").hide();
			$("#lap_perinatologi_cara_datang").on("change", function() {
				var value = $(this).val();
				if (value == "Rujukan")
					$(".lap_perinatologi_asal_rujukan").show();
				else {
					$(".lap_perinatologi_asal_rujukan").hide();
					$("#lap_perinatologi_asal_rujukan").val("");
				}
			});
			
			var column=new Array(
				"id","tanggal","ruangan",
				"noreg_pasien", "nrm_pasien", "nama_pasien",
				"cara_datang", "asal_rujukan", "bayi_lahir_hidup", "kematian_perinatal", "sebab_kematian", "dirujuk"			
			);
			<?php echo $this->action; ?>=new TableAction("<?php echo $this->action; ?>",rl35_the_page,"<?php echo $this->action; ?>",column);
			<?php echo $this->action; ?>.setPrototipe(rl35_the_protoname,rl35_the_protoslug,rl35_the_protoimplement);
			<?php echo $this->action; ?>.getRegulerData=function(){
				var reg_data={	
						page:this.page,
						action:this.action,
						super_command:this.super_command,
						prototype_name:this.prototype_name,
						prototype_slug:this.prototype_slug,
						prototype_implement:this.prototype_implement,
						polislug:rl35_polislug,
						noreg_pasien:rl35_noreg,
						nama_pasien:rl35_nama_pasien,
						nrm_pasien:rl35_nrm_pasien,
						id_antrian:rl35_id_antrian
						};
				return reg_data;
			};

			<?php echo $this->action; ?>.setNextEnter();
			<?php echo $this->action; ?>.setEnableAutofocus(true);
			<?php
			if ($this->page == "medical_record" && $this->action == "iklin") {
				echo $this->action;
				?>.view();<?php
			} else {
				echo $this->action . ".clear=function(){return;};";
			}
			?>
		});
		</script>
<?php
	}
	public function superCommand($super_command) {
		/* PASIEN */
		$super = new SuperCommand ();
		
		$phead=array ('Nama','NRM',"No Reg" );
		$ptable = new Table ( $phead, "", NULL, true );
		$ptable->setName ( "rl35_pasien" );
		$ptable->setModel ( Table::$SELECT );
		$padapter = new SimpleAdapter ();
		$padapter->add ( "Nama", "nama_pasien" );
		$padapter->add ( "NRM", "nrm", "digit8" );
		$padapter->add ( "No Reg", "id" );
		$presponder = new ServiceResponder ( $this->db, $ptable, $padapter, "get_registered" );		
		$super->addResponder ( "rl35_pasien", $presponder );
		$init = $super->initialize ();
		if ($init != null) {
			echo $init;
			return;
		}
	}
}

?>