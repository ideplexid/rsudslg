<?php

require_once 'smis-framework/smis/template/ModulTemplate.php';
require_once 'medical_record/class/template/AsessmenPasien.php';
require_once ("smis-base/smis-include-service-consumer.php");
require_once "medical_record/class/service/RuanganService.php";
require_once 'smis-libs-hrd/EmployeeResponder.php';

class AsesmenPerawat extends AsessmenPasien {
    protected $db;
	protected $polislug;
	protected $page;
	protected $protoslug;
	protected $protoname;
	protected $protoimplement;
	protected $action;
	protected $dbtable;
	protected $uitable;
	protected $noreg_pasien;
	protected $nama_pasien;
	protected $nrm_pasien;
	protected $alamat;
	protected $umur;
    protected $tgl_lahir;
	protected $jk;
	protected $ruang_asal;
	protected $id_antrian;
    protected $carabayar;
    protected $waktu_masuk;
    
    public function __construct($db, $polislug = "all", $noreg = "", $nrm = "", $nama = "", $alamat = "", $umur = "", $jk = "0", $tgl_lahir="", $waktu_masuk = "", $id_antrian = "0", $page = "medical_record", $action = "asesmen_keperawatan",$carabayar, $protoslug = "", $protoname = "", $protoimplement = "") {
        $this->db               = $db;
		$this->noreg_pasien     = $noreg;
		$this->id_antrian       = $id_antrian;
		$this->nama_pasien      = $nama;
		$this->nrm_pasien       = $nrm;
		$this->alamat           = $alamat;
		$this->umur             = $umur;
		$this->jk               = $jk;
        $this->tgl_lahir        = $tgl_lahir;
        $this->waktu_masuk      = $waktu_masuk;
		$this->ruang_asal       = $ruang_asal;
		$this->polislug         = $polislug;
		$this->dbtable          = new DBTable ( $this->db, "smis_mr_assesmen_awal_perawat" );
		$this->page             = $page;
		$this->protoslug        = $protoslug;
		$this->protoimplement   = $protoimplement;
		$this->protoname        = $protoname;
		$this->action           = $action;
        $this->carabayar        = $carabayar;
		//if ($polislug != "all")
			//$this->dbtable->addCustomKriteria ( "ruangan", "='" . $polislug . "'" );
            
		if ($noreg != "")
			$this->dbtable->addCustomKriteria ( "noreg_pasien", "='" . $noreg . "'" );
		$head=array ('NRM',"Pasien","No Register" );
		$this->uitable = new Table ($head , " Asesmen Keperawatan - " . ($this->polislug == "all" ? "" : ucfirst ( $this->protoname )), NULL, true );
		$this->uitable->setName ( $action );
    }
    
    public function command($command) {
        require_once "smis-base/smis-include-duplicate.php";
		$adapter = new SimpleAdapter ();
		$adapter->add ( "Pasien", "nama_pasien" );
		$adapter->add ( "NRM", "nrm_pasien", "digit8" );
		$adapter->add ( "No Register", "noreg_pasien", "digit8" );
		$dbres = new DuplicateResponder ( $this->dbtable, $this->uitable, $adapter );
		$dbres->setDuplicate(false,"");
        $dbres->setAutonomous(getSettings($this->db,"smis_autonomous_id",""));
        $data = $dbres->command ( $_POST ['command'] );
		echo json_encode ( $data );
		return;
	}
    
    public function phpPreLoad() {
        $service = new RuanganService ( $this->db );
		$service->execute ();
		$ruangan = $service->getContent ();
		$pasien=$this->getDetailPatient($this->noreg_pasien);
        
        /*block data header*/
		if ($this->noreg_pasien != "") {
			$this->uitable->addModal ( "nama_pasien", "hidden", "", $this->nama_pasien, "n", null, true );
			$this->uitable->addModal ( "nrm_pasien", "hidden", "", $this->nrm_pasien, "n", null, true );
			$this->uitable->addModal ( "noreg_pasien", "hidden", "", $this->noreg_pasien, "n", null, true );
			$this->uitable->addModal ( "alamat_pasien", "hidden", "", $this->alamat, "n", null, true );
			$this->uitable->addModal ( "jk", "hidden", "", $this->jk, "n", null, true );
            $this->uitable->addModal ( "waktu_masuk", "hidden", "", $this->waktu_masuk, "n", null, true );
			$this->uitable->addModal ( "umur_pasien", "hidden", "", $this->umur, "n", null, true );
            $this->uitable->addModal ( "tgl_lahir_pasien", "hidden", "", $this->tgl_lahir, "n", null, true );
		} else {
			$this->uitable->addModal ( "nama_pasien", "chooser-" . $this->action . "-asp_pasien-Pasien", "Pasien", $this->nama_pasien, "n", null, true );
			$this->uitable->addModal ( "nrm_pasien", "text", "NRM", $this->nrm_pasien, "n", null, true );
			$this->uitable->addModal ( "noreg_pasien", "text", "No Reg", $this->noreg_pasien, "n", null, true );
		}
		if ($this->polislug == "all")
			$this->uitable->addModal ( "ruangan", "select", "Asal Ruangan", $ruangan );
		else
			$this->uitable->addModal ( "ruangan", "hidden", "", $this->polislug, "n", null, true );
        /*end - block data header*/
        
        $the_row = array ();
		$the_row ["id"] 						    = "";
		$the_row ["nama_dokter"] 				    = "";
		$the_row ["id_dokter"] 					    = "";
        //$the_row ["nama_dokter_dpjp"] 				= "";
		//$the_row ["id_dokter_dpjp"] 				= "";
        $the_row ["nama_perawat"] 				    = "";
		$the_row ["id_perawat"] 				    = "";
		$the_row ["diagnosa_perawat"] 				= "";
		$the_row ['waktu'] 					        = date ( "Y-m-d H:i:s" );
		$the_row ["carabayar"] 				        = $this->carabayar;		
		$the_row ["waktu_masuk"] 				    = $this->waktu_masuk;		
		$the_row ["bb"] 				            = "";		
		$the_row ["tb"] 				            = "";		
		$the_row ["rujukan"] 				        = $pasien['nama_rujukan'];		
		$the_row ["kedatangan"] 				    = $pasien['caradatang'];		
		$the_row ["caradatang"] 				    = $pasien['caradatang'];		
		$the_row ["tanda_vital"] 				    = "";		
		$the_row ["tekanan_darah"] 				    = "";		
		$the_row ["frek_nafas"] 				    = "";		
		$the_row ["nadi"] 				            = "";		
		$the_row ["suhu"] 				            = "";		
		$the_row ["rkpr_sumber_data"] 				= "";		
		$the_row ["rkpr_sumber_data_lain"] 			= "";		
		$the_row ["rkpr_keluhan_utama"] 			= "";		
		$the_row ["rkpr_keadaan_umum"] 				= "";		
		$the_row ["rkpr_gizi"] 				        = "";		
		$the_row ["rkpr_tindakan_resusitasi"] 		= "";		
		$the_row ["rkpr_tindakan_resusitasi_lain"] 	= "";		
		$the_row ["rkpr_saturasi_o2"] 				= "";		
		$the_row ["rkpr_pada"] 				        = "";		
		$the_row ["psks_status_nikah"] 				= "";		
		$the_row ["psks_anak"] 				        = "";		
		$the_row ["psks_jumlah_anak"] 				= "";		
		$the_row ["psks_pend_akhir"] 				= $pasien['pendidikan'];		
		$the_row ["psks_warganegara"] 				= "";		
		$the_row ["psks_pekerjaan"] 				= $pasien['pekerjaan'];		
		$the_row ["psks_agama"] 				    = $pasien['agama'];		
		$the_row ["psks_tinggal_bersama"] 			= "";		
		$the_row ["psks_tinggal_bersama_nama"] 		= "";		
		$the_row ["psks_tinggal_bersama_telp"] 		= "";		
		$the_row ["rks_riwayat_penyakit"] 			= "";		
		$the_row ["rks_riwayat_penyakit_hipertensi"]= "";		
		$the_row ["rks_riwayat_penyakit_diabetes"] 	= "";		
		$the_row ["rks_riwayat_penyakit_jantung"] 	= "";		
		$the_row ["rks_riwayat_penyakit_stroke"] 	= "";		
		$the_row ["rks_riwayat_penyakit_dialisis"] 	= "";		
		$the_row ["rks_riwayat_penyakit_asthma"] 	= "";		
		$the_row ["rks_riwayat_penyakit_kejang"] 	= "";		
		$the_row ["rks_riwayat_penyakit_hati"] 		= "";		
		$the_row ["rks_riwayat_penyakit_kanker"] 	= "";		
		$the_row ["rks_riwayat_penyakit_tbc"] 		= "";		
		$the_row ["rks_riwayat_penyakit_glaukoma"] 	= "";		
		$the_row ["rks_riwayat_penyakit_std"] 		= "";		
		$the_row ["rks_riwayat_penyakit_pendarahan"]= "";		
		$the_row ["rks_riwayat_penyakit_lain"] 		= "";		
		$the_row ["rks_riwayat_penyakit_lain_lain"] = "";		
		$the_row ["rks_pengobatan_saat_ini"] 		= "";		
		$the_row ["rks_pengobatan_saat_ini_ket"] 	= "";		
		$the_row ["rks_riwayat_operasi"] 			= "";		
		$the_row ["rks_riwayat_operasi_ket"] 		= "";		
		$the_row ["rks_riwayat_transfusi"] 			= "";		
		$the_row ["rks_reaksi_transfusi"] 			= "";		
		$the_row ["rks_reaksi_transfusi_ket"] 		= "";		
		$the_row ["rks_riwayat_penyakit_keluarga"] 	= "";		
		$the_row ["rks_ketergantungan"] 			= "";		
		$the_row ["rks_obat"] 				        = "";		
		$the_row ["rks_jenis_jmlh_perhari"] 		= "";		
		$the_row ["nutrisi_makan_perhari"] 			= "";		
		$the_row ["nutrisi_minum_perhari"] 			= "";		
		$the_row ["nutrisi_diet_khusus"] 			= "";		
		$the_row ["nutrisi_keluhan"] 				= "";		
		$the_row ["nutrisi_keluhan_ket"] 			= "";		
		$the_row ["nutrisi_perubahan_bb"] 			= "";		
		$the_row ["nutrisi_perubahan_bb_penurunan"] = "";		
		$the_row ["nutrisi_perubahan_bb_kenaikan"]  = "";		
		$the_row ["alergi"] 				        = "";		
		$the_row ["alergi_obat_tipereaksi"] 		= "";		
		$the_row ["alergi_makanan_tipereaksi"] 		= "";		
		$the_row ["alergi_lain_tipereaksi"] 		= "";		
		$the_row ["nyeri"] 				            = "";		
		$the_row ["sifat_nyeri"] 				    = "";		
		$the_row ["skala_nyeri"] 				    = "";		
		$the_row ["nyeri_lokasi"] 				    = "";		
		$the_row ["nyeri_sejak"] 				    = "";		
		$the_row ["get_up_go_test"] 				= "";		
		$the_row ["get_up_go_test_jalan_bungkuk"] 	= "";		
		$the_row ["get_up_go_test_topang_duduk"] 	= "";		
		$the_row ["bicara"] 				        = "";		
		$the_row ["bicara_ket"] 				    = "";		
		$the_row ["penerjemah"] 				    = "";		
		$the_row ["bahasa"] 				        = $pasien['bahasa'];		
		$the_row ["bhs_isyarat"] 				    = "";	
		$the_row ["hambatan_belajar"] 				= "";		
		$the_row ["hambatan_belajar_ket"] 			= "";		
		$the_row ["catatan_lain"] 				    = "";				
        
        $exist ['noreg_pasien'] = $this->noreg_pasien;
		$exist ['nama_pasien'] = $this->nama_pasien;
		$exist ['nrm_pasien'] = $this->nrm_pasien;
        //$exist ['ruangan'] = $this->polislug;
        if ($this->dbtable->is_exist ( $exist )) {
            $row = $this->dbtable->select ( $exist );
            $the_row ["id"] 					        = $row->id;
            $the_row ["nama_dokter"] 			        = $row->nama_dokter;
            $the_row ["id_dokter"] 				        = $row->id_dokter;
            //$the_row ["nama_dokter_dpjp"] 				= $row->nama_dokter_dpjp;
            //$the_row ["id_dokter_dpjp"] 				= $row->id_dokter_dpjp;
            $the_row ["nama_perawat"] 				    = $row->nama_perawat;
            $the_row ["id_perawat"] 				    = $row->id_perawat;
            $the_row ["diagnosa_perawat"] 				= $row->diagnosa_perawat;
            $the_row ["waktu"] 				            = $row->waktu;
            $the_row ["waktu_masuk"] 				    = $row->waktu_masuk;
            $the_row ["bb"] 				            = $row->bb;
            $the_row ["tb"] 				            = $row->tb;
            $the_row ["rujukan"] 				        = $row->rujukan;
            $the_row ["kedatangan"] 				    = $row->kedatangan;
            $the_row ["caradatang"] 				    = $row->caradatang;
            $the_row ["tanda_vital"] 				    = $row->tanda_vital;
            $the_row ["tekanan_darah"] 				    = $row->tekanan_darah;
            $the_row ["frek_nafas"] 				    = $row->frek_nafas;
            $the_row ["nadi"] 				            = $row->nadi;
            $the_row ["suhu"] 				            = $row->suhu;
            $the_row ["rkpr_sumber_data"] 				= $row->rkpr_sumber_data;
            $the_row ["rkpr_sumber_data_lain"] 			= $row->rkpr_sumber_data_lain;
            $the_row ["rkpr_keluhan_utama"] 			= $row->rkpr_keluhan_utama;
            $the_row ["rkpr_keadaan_umum"] 				= $row->rkpr_keadaan_umum;
            $the_row ["rkpr_gizi"] 				        = $row->rkpr_gizi;
            $the_row ["rkpr_tindakan_resusitasi"] 		= $row->rkpr_tindakan_resusitasi;
            $the_row ["rkpr_tindakan_resusitasi_lain"] 	= $row->rkpr_tindakan_resusitasi_lain;
            $the_row ["rkpr_saturasi_o2"] 				= $row->rkpr_saturasi_o2;
            $the_row ["rkpr_pada"] 				        = $row->rkpr_pada;
            $the_row ["psks_status_nikah"] 				= $row->psks_status_nikah;
            $the_row ["psks_anak"] 				        = $row->psks_anak;
            $the_row ["psks_jumlah_anak"] 				= $row->psks_jumlah_anak;
            $the_row ["psks_pend_akhir"] 				= $row->psks_pend_akhir;
            $the_row ["psks_warganegara"] 				= $row->psks_warganegara;
            $the_row ["psks_pekerjaan"] 				= $row->psks_pekerjaan;
            $the_row ["psks_agama"] 				    = $row->psks_agama;
            $the_row ["psks_tinggal_bersama"] 			= $row->psks_tinggal_bersama;
            $the_row ["psks_tinggal_bersama_nama"] 		= $row->psks_tinggal_bersama_nama;
            $the_row ["psks_tinggal_bersama_telp"] 		= $row->psks_tinggal_bersama_telp;
            $the_row ["rks_riwayat_penyakit"] 			= $row->rks_riwayat_penyakit;
            $the_row ["rks_riwayat_penyakit_hipertensi"]= $row->rks_riwayat_penyakit_hipertensi;
            $the_row ["rks_riwayat_penyakit_diabetes"] 	= $row->rks_riwayat_penyakit_diabetes;
            $the_row ["rks_riwayat_penyakit_jantung"] 	= $row->rks_riwayat_penyakit_jantung;
            $the_row ["rks_riwayat_penyakit_stroke"] 	= $row->rks_riwayat_penyakit_stroke;
            $the_row ["rks_riwayat_penyakit_dialisis"] 	= $row->rks_riwayat_penyakit_dialisis;
            $the_row ["rks_riwayat_penyakit_asthma"] 	= $row->rks_riwayat_penyakit_asthma;
            $the_row ["rks_riwayat_penyakit_kejang"] 	= $row->rks_riwayat_penyakit_kejang;
            $the_row ["rks_riwayat_penyakit_hati"] 		= $row->rks_riwayat_penyakit_hati;
            $the_row ["rks_riwayat_penyakit_kanker"] 	= $row->rks_riwayat_penyakit_kanker;
            $the_row ["rks_riwayat_penyakit_tbc"] 		= $row->rks_riwayat_penyakit_tbc;
            $the_row ["rks_riwayat_penyakit_glaukoma"] 	= $row->rks_riwayat_penyakit_glaukoma;
            $the_row ["rks_riwayat_penyakit_std"] 		= $row->rks_riwayat_penyakit_std;
            $the_row ["rks_riwayat_penyakit_pendarahan"]= $row->rks_riwayat_penyakit_pendarahan;
            $the_row ["rks_riwayat_penyakit_lain"] 		= $row->rks_riwayat_penyakit_lain;
            $the_row ["rks_riwayat_penyakit_lain_lain"] = $row->rks_riwayat_penyakit_lain_lain;
            $the_row ["rks_pengobatan_saat_ini"] 		= $row->rks_pengobatan_saat_ini;
            $the_row ["rks_pengobatan_saat_ini_ket"] 	= $row->rks_pengobatan_saat_ini_ket;
            $the_row ["rks_riwayat_operasi"] 			= $row->rks_riwayat_operasi;
            $the_row ["rks_riwayat_operasi_ket"] 		= $row->rks_riwayat_operasi_ket;
            $the_row ["rks_riwayat_transfusi"] 			= $row->rks_riwayat_transfusi;
            $the_row ["rks_reaksi_transfusi"] 			= $row->rks_reaksi_transfusi;
            $the_row ["rks_reaksi_transfusi_ket"] 		= $row->rks_reaksi_transfusi_ket;
            $the_row ["rks_riwayat_penyakit_keluarga"] 	= $row->rks_riwayat_penyakit_keluarga;
            $the_row ["rks_ketergantungan"] 			= $row->rks_ketergantungan;
            $the_row ["rks_obat"] 				        = $row->rks_obat;
            $the_row ["rks_jenis_jmlh_perhari"] 		= $row->rks_jenis_jmlh_perhari;
            $the_row ["nutrisi_makan_perhari"] 			= $row->nutrisi_makan_perhari;
            $the_row ["nutrisi_minum_perhari"] 			= $row->nutrisi_minum_perhari;
            $the_row ["nutrisi_diet_khusus"] 			= $row->nutrisi_diet_khusus;
            $the_row ["nutrisi_keluhan"] 				= $row->nutrisi_keluhan;
            $the_row ["nutrisi_keluhan_ket"] 			= $row->nutrisi_keluhan_ket;
            $the_row ["nutrisi_perubahan_bb"] 			= $row->nutrisi_perubahan_bb;
            $the_row ["nutrisi_perubahan_bb_penurunan"] = $row->nutrisi_perubahan_bb_penurunan;
            $the_row ["nutrisi_perubahan_bb_kenaikan"]  = $row->nutrisi_perubahan_bb_kenaikan;
            $the_row ["alergi"] 				        = $row->alergi;
            $the_row ["alergi_obat_tipereaksi"] 		= $row->alergi_obat_tipereaksi;
            $the_row ["alergi_makanan_tipereaksi"] 		= $row->alergi_makanan_tipereaksi;
            $the_row ["alergi_lain_tipereaksi"] 		= $row->alergi_lain_tipereaksi;
            $the_row ["nyeri"] 				            = $row->nyeri;
            $the_row ["sifat_nyeri"] 				    = $row->sifat_nyeri;
            $the_row ["skala_nyeri"] 				    = $row->skala_nyeri;
            $the_row ["nyeri_lokasi"] 				    = $row->nyeri_lokasi;
            $the_row ["nyeri_sejak"] 				    = $row->nyeri_sejak;
            $the_row ["get_up_go_test"] 				= $row->get_up_go_test;
            $the_row ["get_up_go_test_jalan_bungkuk"] 	= $row->get_up_go_test_jalan_bungkuk;
            $the_row ["get_up_go_test_topang_duduk"] 	= $row->get_up_go_test_topang_duduk;
            $the_row ["bicara"] 				        = $row->bicara;
            $the_row ["bicara_ket"] 				    = $row->bicara_ket;
            $the_row ["penerjemah"] 				    = $row->penerjemah;
            $the_row ["bahasa"] 				        = $row->bahasa;
            $the_row ["bhs_isyarat"] 				    = $row->bhs_isyarat;
            $the_row ["hambatan_belajar"] 				= $row->hambatan_belajar;
            $the_row ["hambatan_belajar_ket"] 			= $row->hambatan_belajar_ket;
            $the_row ["catatan_lain"] 				    = $row->catatan_lain;
        } else {
            $exist ['umur_pasien'] = $this->umur;
			$exist ['jk'] = $this->jk;
			$exist ['alamat_pasien'] = $this->alamat;
			$exist ['noreg_pasien'] = $this->noreg_pasien;
			$exist ['nrm_pasien'] = $this->nrm_pasien;
			$exist ['nama_pasien'] = $this->nama_pasien;
            $exist ['ruangan'] = $this->polislug;
            $exist ['tgl_lahir_pasien'] = $this->tgl_lahir;
			$exist ['waktu'] = date ( "Y-m-d H:i:s" );
            $exist ['waktu_masuk'] = $this->waktu_masuk;
            $this->dbtable->insert ( $exist );
			
            $the_row ["id"] = $this->dbtable->get_inserted_id ();
            
            $update['origin_id']=$the_row ["id"];
            $update['autonomous']="[".getSettings($this->db,"smis_autonomous_id","")."]";
            $this->dbtable->update($update,array("id"=>$the_row ["id"]));
        }
        
        $this->uitable->addModal ( "id", "hidden", "", $the_row ['id'] );
		$this->uitable->addModal ( "nama_dokter", "chooser-".$this->action."-dokter_asp-Dokter Pemeriksa", "Dokter Pemeriksa", $the_row ['nama_dokter'],"n",null,false,null,true);
        $this->uitable->addModal ( "id_dokter", "hidden", "", $the_row ['id_dokter'],"y",null,false,null,false);
        //$this->uitable->addModal ( "nama_dokter_dpjp", "chooser-".$this->action."-dokter_dpjp_asp-Dokter DPJP", "Dokter DPJP", $the_row ['nama_dokter_dpjp'],"n",null,false,null,true);
        //$this->uitable->addModal ( "id_dokter_dpjp", "hidden", "", $the_row ['id_dokter_dpjp'],"y",null,false,null,false);
        $this->uitable->addModal ( "nama_perawat", "chooser-".$this->action."-perawat_asp-Perawat", "Perawat", $the_row ['nama_perawat'],"n",null,false,null,true);
        $this->uitable->addModal ( "id_perawat", "hidden", "", $the_row ['id_perawat'],"y",null,false,null,false);
        $this->uitable->addModal ( "diagnosa_perawat", "textarea", "Diagnosa Perawat", $the_row ['diagnosa_perawat'] ,"y",null,false,null,false);
		$this->uitable->addModal ( "waktu", "datetime", "Waktu", $the_row ['waktu'] ,"y",null,false,null,false);
		$this->uitable->addModal ( "carabayar", "text", "Carabayar", $the_row ['carabayar'] ,"n",null,true,null,false);
        $this->uitable->addModal ( "rujukan", "text", "Rujukan", $the_row ['rujukan'] ,"y",null,false,null,false);
		$this->uitable->addModal ( "kedatangan", "text", "Kedatangan", $the_row ['kedatangan'] ,"y",null,false,null,false);
		$this->uitable->addModal ( "bb", "text", "Berat Badan", $the_row ['bb'] ,"y",null,false,null,false);
		$this->uitable->addModal ( "tb", "text", "Tinggi Badan", $the_row ['tb'] ,"y",null,false,null,false);
        
        $this->uitable->addModal("", "label", "<strong>Tanda Vital</strong>", "");
		$this->uitable->addModal ( "tanda_vital", "hidden", "Tanda Vital", $the_row ['tanda_vital'] ,"y",null,false,null,false);
		$this->uitable->addModal ( "tekanan_darah", "text", "Tekanan Darah", $the_row ['tekanan_darah'] ,"y",null,false,null,false);
		$this->uitable->addModal ( "frek_nafas", "text", "Frek Nafas", $the_row ['frek_nafas'] ,"y",null,false,null,false);
		$this->uitable->addModal ( "nadi", "text", "Nadi", $the_row ['nadi'] ,"y",null,false,null,false);
		$this->uitable->addModal ( "suhu", "text", "Suhu", $the_row ['suhu'] ,"y",null,false,null,false);
        
        $this->uitable->addModal("", "label", "<strong>RIWAYAT KEPERAWATAN</strong>", "");
        $sumber_data=array(
				array("name"=>"","value"=>"","default"=> $the_row ["rkpr_sumber_data"]==""?1:0),
				array("name"=>"Pasien","value"=>"Pasien","default"=> $the_row ["rkpr_sumber_data"]=="Pasien"?1:0),
				array("name"=>"Keluarga","value"=>"Keluarga","default"=> $the_row ["rkpr_sumber_data"]=="Keluarga"?1:0),
				array("name"=>"Teman","value"=>"Teman","default"=> $the_row ["rkpr_sumber_data"]=="Teman"?1:0),
				array("name"=>"Lain-Lain","value"=>"Lain-Lain","default"=> $the_row ["rkpr_sumber_data"]=="Lain-Lain"?1:0)
		);
        $this->uitable->addModal("rkpr_sumber_data", "select", "<strong>Sumber Data</strong>", $sumber_data,"y",NULL,false,NULL,false);
        $this->uitable->addModal ( "rkpr_sumber_data_lain", "text", "Lain-Lain", $the_row ['rkpr_sumber_data_lain'] ,"y",null,false,null,false);
        $this->uitable->addModal ( "rkpr_keluhan_utama", "textarea", "<strong>Keluhat Utama</strong>", $the_row ['rkpr_keluhan_utama'] ,"y",null,false,null,false);
        $keadaan_umum=array(
				array("name"=>"","value"=>"","default"=> $the_row ["rkpr_keadaan_umum"]==""?1:0),
				array("name"=>"Baik","value"=>"Baik","default"=> $the_row ["rkpr_keadaan_umum"]=="Baik"?1:0),
				array("name"=>"Sedang","value"=>"Sedang","default"=> $the_row ["rkpr_keadaan_umum"]=="Sedang"?1:0),
				array("name"=>"Lemah","value"=>"Lemah","default"=> $the_row ["rkpr_keadaan_umum"]=="Lemah"?1:0),
				array("name"=>"Jelek","value"=>"Jelek","default"=> $the_row ["rkpr_keadaan_umum"]=="Jelek"?1:0)
		);
        $this->uitable->addModal("rkpr_keadaan_umum", "select", "<strong>Keadaan Umum</strong>", $keadaan_umum,"y",NULL,false,NULL,false);
        $gizi=array(
				array("name"=>"","value"=>"","default"=> $the_row ["rkpr_gizi"]==""?1:0),
				array("name"=>"Baik","value"=>"Baik","default"=> $the_row ["rkpr_gizi"]=="Baik"?1:0),
				array("name"=>"Sedang","value"=>"Sedang","default"=> $the_row ["rkpr_gizi"]=="Sedang"?1:0),
				array("name"=>"Lemah","value"=>"Lemah","default"=> $the_row ["rkpr_gizi"]=="Lemah"?1:0),
				array("name"=>"Jelek","value"=>"Jelek","default"=> $the_row ["rkpr_gizi"]=="Jelek"?1:0)
		);
        $this->uitable->addModal("rkpr_gizi", "select", "<strong>Gizi</strong>", $gizi,"y",NULL,false,NULL,false);
        $resusitasi=array(
				array("name"=>"","value"=>"","default"=> $the_row ["rkpr_tindakan_resusitasi"]==""?1:0),
				array("name"=>"Ya","value"=>"Baik","default"=> $the_row ["rkpr_tindakan_resusitasi"]=="Ya"?1:0),
				array("name"=>"Tidak","value"=>"Sedang","default"=> $the_row ["rkpr_tindakan_resusitasi"]=="Tidak"?1:0)
		);
        $this->uitable->addModal("rkpr_tindakan_resusitasi", "select", "<strong>Tindakan Resusitasi</strong>", $resusitasi,"y",NULL,false,NULL,false);
        $this->uitable->addModal ( "rkpr_tindakan_resusitasi_lain", "text", "Lain-Lain", $the_row ['rkpr_tindakan_resusitasi_lain'] ,"y",null,false,null,false);
        $this->uitable->addModal ( "rkpr_saturasi_o2", "textarea", "<strong>Saturasi O<sub>2</sub></strong>", $the_row ['rkpr_saturasi_o2'] ,"y",null,false,null,false);
        $pada=array(
				array("name"=>"","value"=>"","default"=> $the_row ["rkpr_pada"]==""?1:0),
				array("name"=>"Suhu Ruangan","value"=>"Suhu Ruangan","default"=> $the_row ["rkpr_pada"]=="Suhu Ruangan"?1:0),
				array("name"=>"Nasal Canule","value"=>"Nasal Canule","default"=> $the_row ["rkpr_pada"]=="Nasal Canule"?1:0),
				array("name"=>"NRM","value"=>"NRM","default"=> $the_row ["rkpr_pada"]=="NRM"?1:0)
		);
        $this->uitable->addModal("rkpr_pada", "select", "Pada", $pada,"y",NULL,false,NULL,false);
        
        $this->uitable->addModal("", "label", "<strong>PSIKOSOSIAL</strong>", "");
        $nikah=array(
				array("name"=>"","value"=>"","default"=> $the_row ["psks_status_nikah"]==""?1:0),
				array("name"=>"Single","value"=>"Single","default"=> $the_row ["psks_status_nikah"]=="Single"?1:0),
				array("name"=>"Menikah","value"=>"Menikah","default"=> $the_row ["psks_status_nikah"]=="Menikah"?1:0),
				array("name"=>"Bercerai","value"=>"Bercerai","default"=> $the_row ["psks_status_nikah"]=="Bercerai"?1:0),
				array("name"=>"Janda/Duda","value"=>"Janda/Duda","default"=> $the_row ["psks_status_nikah"]=="Janda/Duda"?1:0)
		);
        $this->uitable->addModal("psks_status_nikah", "select", "<strong>Status Pernikahan</strong>", $nikah,"y",NULL,false,NULL,false);
        $anak=array(
				array("name"=>"","value"=>"","default"=> $the_row ["psks_anak"]==""?1:0),
				array("name"=>"Tidak Ada","value"=>"Tidak Ada","default"=> $the_row ["psks_anak"]=="Tidak Ada"?1:0),
				array("name"=>"Ada","value"=>"Ada","default"=> $the_row ["psks_anak"]=="Ada"?1:0)
		);
        $this->uitable->addModal("psks_anak", "select", "<strong>Anak</strong>", $anak,"y",NULL,false,NULL,false);
        $this->uitable->addModal ( "psks_jumlah_anak", "text", "Jumlah Anak", $the_row ['psks_jumlah_anak'] ,"y",null,false,null,false);
        $this->uitable->addModal ( "psks_pend_akhir", "text", "<strong>Pendidikan terakhir</strong>", $the_row ['psks_pend_akhir'] ,"y",null,false,null,false);
        $this->uitable->addModal ( "psks_warganegara", "text", "<strong>Warganegara</strong>", $the_row ['psks_warganegara'] ,"y",null,false,null,false);
        $this->uitable->addModal ( "psks_pekerjaan", "text", "<strong>Pekerjaan</strong>", $the_row ['psks_pekerjaan'] ,"y",null,false,null,false);
        $this->uitable->addModal ( "psks_agama", "text", "<strong>Agama</strong>", $the_row ['psks_agama'] ,"y",null,false,null,false);
        $this->uitable->addModal ( "psks_tinggal_bersama", "text", "<strong>Tinggal Bersama</strong>", $the_row ['psks_tinggal_bersama'] ,"y",null,false,null,false);
        $this->uitable->addModal ( "psks_tinggal_bersama_nama", "text", "Nama", $the_row ['psks_tinggal_bersama_nama'] ,"y",null,false,null,false);
        $this->uitable->addModal ( "psks_tinggal_bersama_telp", "text", "No. Telepon", $the_row ['psks_tinggal_bersama_telp'] ,"y",null,false,null,false);
        
        $this->uitable->addModal("", "label", "<strong>RIWAYAT KESEHATAN</strong>", "");
        $this->uitable->addModal("", "label", "<strong>Riwayat Penyakit</strong>", "");
        $this->uitable->addModal("rks_riwayat_penyakit_hipertensi", "checkbox", "Hipertensi", $the_row ['rks_riwayat_penyakit_hipertensi'],"y",NULL,false,NULL,false);
        $this->uitable->addModal("rks_riwayat_penyakit_diabetes", "checkbox", "Diabetes", $the_row ['rks_riwayat_penyakit_diabetes'],"y",NULL,false,NULL,false);
        $this->uitable->addModal("rks_riwayat_penyakit_jantung", "checkbox", "Jantung", $the_row ['rks_riwayat_penyakit_jantung'],"y",NULL,false,NULL,false);
        $this->uitable->addModal("rks_riwayat_penyakit_stroke", "checkbox", "Stroke", $the_row ['rks_riwayat_penyakit_stroke'],"y",NULL,false,NULL,false);
        $this->uitable->addModal("rks_riwayat_penyakit_dialisis", "checkbox", "Dialisis", $the_row ['rks_riwayat_penyakit_dialisis'],"y",NULL,false,NULL,false);
        $this->uitable->addModal("rks_riwayat_penyakit_asthma", "checkbox", "Asthma", $the_row ['rks_riwayat_penyakit_asthma'],"y",NULL,false,NULL,false);
        $this->uitable->addModal("rks_riwayat_penyakit_kejang", "checkbox", "Kejang", $the_row ['rks_riwayat_penyakit_kejang'],"y",NULL,false,NULL,false);
        $this->uitable->addModal("rks_riwayat_penyakit_hati", "checkbox", "Hati", $the_row ['rks_riwayat_penyakit_hati'],"y",NULL,false,NULL,false);
        $this->uitable->addModal("rks_riwayat_penyakit_kanker", "checkbox", "Kanker", $the_row ['rks_riwayat_penyakit_kanker'],"y",NULL,false,NULL,false);
        $this->uitable->addModal("rks_riwayat_penyakit_tbc", "checkbox", "TBC", $the_row ['rks_riwayat_penyakit_tbc'],"y",NULL,false,NULL,false);
        $this->uitable->addModal("rks_riwayat_penyakit_glaukoma", "checkbox", "Glaukoma", $the_row ['rks_riwayat_penyakit_glaukoma'],"y",NULL,false,NULL,false);
        $this->uitable->addModal("rks_riwayat_penyakit_std", "checkbox", "STD", $the_row ['rks_riwayat_penyakit_std'],"y",NULL,false,NULL,false);
        $this->uitable->addModal("rks_riwayat_penyakit_pendarahan", "checkbox", "Pendarahan", $the_row ['rks_riwayat_penyakit_pendarahan'],"y",NULL,false,NULL,false);
        $this->uitable->addModal("rks_riwayat_penyakit_lain", "checkbox", "Lain-Lain", $the_row ['rks_riwayat_penyakit_lain'],"y",NULL,false,NULL,false);
        $this->uitable->addModal ( "rks_riwayat_penyakit_lain_lain", "text", "Lain-Lain (Keterangan)", $the_row ['rks_riwayat_penyakit_lain_lain'] ,"y",null,false,null,false);
        $obat_saat_ini=array(
				array("name"=>"","value"=>"","default"=> $the_row ["rks_pengobatan_saat_ini"]==""?1:0),
				array("name"=>"Tidak","value"=>"Tidak","default"=> $the_row ["rks_pengobatan_saat_ini"]=="Tidak"?1:0),
				array("name"=>"Ya","value"=>"Ya","default"=> $the_row ["rks_pengobatan_saat_ini"]=="Ya"?1:0)
		);
        $this->uitable->addModal("rks_pengobatan_saat_ini", "select", "<strong>Pengobatan Saat Ini</strong>", $obat_saat_ini,"y",NULL,false,NULL,false);
        $this->uitable->addModal ( "rks_pengobatan_saat_ini_ket", "text", "Jika Ya, Jelaskan", $the_row ['rks_pengobatan_saat_ini_ket'] ,"y",null,false,null,false);
        $operasi=array(
				array("name"=>"","value"=>"","default"=> $the_row ["rks_riwayat_operasi"]==""?1:0),
				array("name"=>"Tidak","value"=>"Tidak","default"=> $the_row ["rks_riwayat_operasi"]=="Tidak"?1:0),
				array("name"=>"Ya","value"=>"Ya","default"=> $the_row ["rks_riwayat_operasi"]=="Ya"?1:0)
		);
        $this->uitable->addModal("rks_riwayat_operasi", "select", "<strong>Riwayat Operasi</strong>", $operasi,"y",NULL,false,NULL,false);
        $this->uitable->addModal ( "rks_riwayat_operasi_ket", "text", "Jika Ya, Jenis dan Kapan", $the_row ['rks_riwayat_operasi_ket'] ,"y",null,false,null,false);
        $transfusi=array(
				array("name"=>"","value"=>"","default"=> $the_row ["rks_riwayat_transfusi"]==""?1:0),
				array("name"=>"Tidak","value"=>"Tidak","default"=> $the_row ["rks_riwayat_transfusi"]=="Tidak"?1:0),
				array("name"=>"Ya","value"=>"Ya","default"=> $the_row ["rks_riwayat_transfusi"]=="Ya"?1:0)
		);
        $this->uitable->addModal("rks_riwayat_transfusi", "select", "<strong>Riwayat Transfusi</strong>", $transfusi,"y",NULL,false,NULL,false);
        $reaksi_transfusi=array(
				array("name"=>"","value"=>"","default"=> $the_row ["rks_reaksi_transfusi"]==""?1:0),
				array("name"=>"Tidak","value"=>"Tidak","default"=> $the_row ["rks_reaksi_transfusi"]=="Tidak"?1:0),
				array("name"=>"Ya","value"=>"Ya","default"=> $the_row ["rks_reaksi_transfusi"]=="Ya"?1:0)
		);
        $this->uitable->addModal("rks_reaksi_transfusi", "select", "Reaksi Riwayat Transfusi", $reaksi_transfusi,"y",NULL,false,NULL,false);
        $this->uitable->addModal ( "rks_reaksi_transfusi_ket", "text", "Jika Ya, Reaksi Yang Timbul", $the_row ['rks_reaksi_transfusi_ket'] ,"y",null,false,null,false);
        $this->uitable->addModal ( "rks_riwayat_penyakit_keluarga", "textarea", "Riwayat Penyakit Dalam Keluarga", $the_row ['rks_riwayat_penyakit_keluarga'] ,"y",null,false,null,false);
        $ketergantungan=array(
				array("name"=>"","value"=>"","default"=> $the_row ["rks_ketergantungan"]==""?1:0),
				array("name"=>"Merokok","value"=>"Merokok","default"=> $the_row ["rks_ketergantungan"]=="Merokok"?1:0),
				array("name"=>"Alkohol","value"=>"Alkohol","default"=> $the_row ["rks_ketergantungan"]=="Alkohol"?1:0)
		);
        $this->uitable->addModal("rks_ketergantungan", "select", "<strong>Reaksi Riwayat Transfusi</strong>", $ketergantungan,"y",NULL,false,NULL,false);
        $this->uitable->addModal ( "rks_obat", "textarea", "Obat / Zat Lainnya", $the_row ['rks_obat'] ,"y",null,false,null,false);
        $this->uitable->addModal ( "rks_jenis_jmlh_perhari", "textarea", "Jenis dan Jumlah per Hari", $the_row ['rks_jenis_jmlh_perhari'] ,"y",null,false,null,false);
        
        $this->uitable->addModal("", "label", "<strong>NUTRISI</strong>", "");
        $this->uitable->addModal ( "nutrisi_makan_perhari", "text", "<strong>Makan x/hari</strong>", $the_row ['nutrisi_makan_perhari'] ,"y",null,false,null,false);
        $this->uitable->addModal ( "nutrisi_minum_perhari", "text", "<strong>Minum ml/hari</strong>", $the_row ['nutrisi_minum_perhari'] ,"y",null,false,null,false);
        $diet=array(
				array("name"=>"","value"=>"","default"=> $the_row ["nutrisi_diet_khusus"]==""?1:0),
				array("name"=>"Tidak Ada","value"=>"Tidak Ada","default"=> $the_row ["nutrisi_diet_khusus"]=="Tidak Ada"?1:0),
				array("name"=>"Ada","value"=>"Ada","default"=> $the_row ["nutrisi_diet_khusus"]=="Ada"?1:0)
		);
        $this->uitable->addModal("nutrisi_diet_khusus", "select", "<strong>Diet Khusus</strong>", $diet,"y",NULL,false,NULL,false);
        $keluhan=array(
				array("name"=>"","value"=>"","default"=> $the_row ["nutrisi_keluhan"]==""?1:0),
				array("name"=>"Tidak Ada","value"=>"Tidak Ada","default"=> $the_row ["nutrisi_keluhan"]=="Tidak Ada"?1:0),
				array("name"=>"Ada","value"=>"Ada","default"=> $the_row ["nutrisi_keluhan"]=="Ada"?1:0)
		);
        $this->uitable->addModal("nutrisi_keluhan", "select", "<strong>Keluhan</strong>", $keluhan,"y",NULL,false,NULL,false);
        $this->uitable->addModal ( "nutrisi_keluhan_ket", "text", "Jika Ada, Jelaskan", $the_row ['nutrisi_keluhan_ket'] ,"y",null,false,null,false);
        $bb=array(
				array("name"=>"","value"=>"","default"=> $the_row ["nutrisi_perubahan_bb"]==""?1:0),
				array("name"=>"Tidak Ada","value"=>"Tidak Ada","default"=> $the_row ["nutrisi_perubahan_bb"]=="Tidak Ada"?1:0),
				array("name"=>"Ada","value"=>"Ada","default"=> $the_row ["nutrisi_perubahan_bb"]=="Ada"?1:0)
		);
        $this->uitable->addModal("nutrisi_perubahan_bb", "select", "<strong>Perubahan Berat badan 6 Bulan Terakhir</strong>", $bb,"y",NULL,false,NULL,false);
        $this->uitable->addModal ( "nutrisi_perubahan_bb_penurunan", "text", "Penurunan", $the_row ['nutrisi_perubahan_bb_penurunan'] ,"y",null,false,null,false);
        $this->uitable->addModal ( "nutrisi_perubahan_bb_kenaikan", "text", "Kenaikan", $the_row ['nutrisi_perubahan_bb_kenaikan'] ,"y",null,false,null,false);
        
        $this->uitable->addModal("", "label", "<strong>RIWAYAT ALERGI</strong>", "");
        $alergi=array(
				array("name"=>"","value"=>"","default"=> $the_row ["alergi"]==""?1:0),
				array("name"=>"Tidak","value"=>"Tidak","default"=> $the_row ["alergi"]=="Tidak"?1:0),
				array("name"=>"Ya","value"=>"Ya","default"=> $the_row ["alergi"]=="Ya"?1:0)
		);
        $this->uitable->addModal("alergi", "select", "Alergi", $alergi,"y",NULL,false,NULL,false);
        $this->uitable->addModal ( "alergi_obat_tipereaksi", "text", "Obat, Tipe Reaksi", $the_row ['alergi_obat_tipereaksi'] ,"y",null,false,null,false);
        $this->uitable->addModal ( "alergi_makanan_tipereaksi", "text", "Makanan, Tipe Reaksi", $the_row ['alergi_makanan_tipereaksi'] ,"y",null,false,null,false);
        $this->uitable->addModal ( "alergi_lain_tipereaksi", "text", "Lain-Lain, Tipe Reaksi", $the_row ['alergi_lain_tipereaksi'] ,"y",null,false,null,false);
        
        $this->uitable->addModal("", "label", "<strong>PENILAIAN NYERI</strong>", "");
        $nyeri=array(
				array("name"=>"","value"=>"","default"=> $the_row ["nyeri"]==""?1:0),
				array("name"=>"Tidak","value"=>"Tidak","default"=> $the_row ["nyeri"]=="Tidak"?1:0),
				array("name"=>"Ya","value"=>"Ya","default"=> $the_row ["nyeri"]=="Ya"?1:0)
		);
        $this->uitable->addModal("nyeri", "select", "<strong>Nyeri</strong>", $nyeri,"y",NULL,false,NULL,false);
        $sifat_nyeri=array(
				array("name"=>"","value"=>"","default"=> $the_row ["sifat_nyeri"]==""?1:0),
				array("name"=>"Akut","value"=>"Akut","default"=> $the_row ["sifat_nyeri"]=="Akut"?1:0),
				array("name"=>"Kronis","value"=>"Kronis","default"=> $the_row ["sifat_nyeri"]=="Kronis"?1:0)
		);
        $this->uitable->addModal("sifat_nyeri", "select", "<strong>Sifat</strong>", $sifat_nyeri,"y",NULL,false,NULL,false);
        $this->uitable->addModal ( "nyeri_lokasi", "text", "Lokasi", $the_row ['nyeri_lokasi'] ,"y",null,false,null,false);
        $this->uitable->addModal ( "nyeri_sejak", "text", "Sejak", $the_row ['nyeri_sejak'] ,"y",null,false,null,false);
        $skala_nyeri=array(
				array("name"=>"","value"=>"","default"=> $the_row ["sifat_nyeri"]==""?1:0),
				array("name"=>"0-3 Ringan","value"=>"0-3 Ringan","default"=> $the_row ["sifat_nyeri"]=="0-3 Ringan"?1:0),
				array("name"=>"4-6 Sedang","value"=>"4-6 Sedang","default"=> $the_row ["sifat_nyeri"]=="4-6 Sedang"?1:0),
				array("name"=>"7-10 Berat","value"=>"7-10 Berat","default"=> $the_row ["sifat_nyeri"]=="7-10 Berat"?1:0),
				array("name"=>">10 Konsul Dokter","value"=>">10 Konsul Dokter","default"=> $the_row ["sifat_nyeri"]==">10 Konsul Dokter"?1:0)
		);
        $this->uitable->addModal("skala_nyeri", "select", "<strong>Skala Nyeri</strong>", $skala_nyeri,"y",NULL,false,NULL,false);
        
        $this->uitable->addModal("", "label", "<strong>SKRINNING RESIKO JATUH/CEDERA</strong>", "");
        $this->uitable->addModal("", "label", "<strong>get Up and Go Test</strong>", "");
        $this->uitable->addModal("get_up_go_test_jalan_bungkuk", "checkbox", "a. Cara Berjalan Membungkuk", "0","y",NULL,false,NULL,false);
        $this->uitable->addModal("get_up_go_test_topang_duduk", "checkbox", "b. Menopang Saat Akan Duduk", "0","y",NULL,false,NULL,false);
        $guagt=array(
				array("name"=>"","value"=>"","default"=> $the_row ["get_up_go_test"]==""?1:0),
				array("name"=>"Tidak Berisiko (Tidak a & b)","value"=>"Tidak Berisiko (Tidak a & b)","default"=> $the_row ["get_up_go_test"]=="Tidak Berisiko (Tidak a & b)"?1:0),
				array("name"=>"Resiko Rendah (Beri Edukasi)","value"=>"Resiko Rendah (Beri Edukasi)","default"=> $the_row ["get_up_go_test"]=="Resiko Rendah (Beri Edukasi)"?1:0),
				array("name"=>"Resiko Tinggi (Pasang Pita Kuning)","value"=>"Resiko Tinggi (Pasang Pita Kuning)","default"=> $the_row ["get_up_go_test"]=="Resiko Tinggi (Pasang Pita Kuning)"?1:0)
		);
        $this->uitable->addModal("get_up_go_test", "select", "<strong>Get Up and Go Test</strong>", $guagt,"y",NULL,false,NULL,false);
        
        $this->uitable->addModal("", "label", "<strong>KEBUTUHAN KOMUNIKASI/PENDIDIKAN DAN PENGAJARAN</strong>", "");
        $this->uitable->addModal("", "label", "", "");
        $bicara=array(
				array("name"=>"","value"=>"","default"=> $the_row ["bicara"]==""?1:0),
				array("name"=>"Tidak","value"=>"Tidak","default"=> $the_row ["bicara"]=="Tidak"?1:0),
				array("name"=>"Ya","value"=>"Ya","default"=> $the_row ["bicara"]=="Ya"?1:0)
		);
        $this->uitable->addModal("bicara", "select", "<strong>Bicara</strong>", $bicara,"y",NULL,false,NULL,false);
        $penerjemah=array(
				array("name"=>"","value"=>"","default"=> $the_row ["penerjemah"]==""?1:0),
				array("name"=>"Tidak","value"=>"Tidak","default"=> $the_row ["penerjemah"]=="Tidak"?1:0),
				array("name"=>"Ya","value"=>"Ya","default"=> $the_row ["penerjemah"]=="Ya"?1:0)
		);
        $this->uitable->addModal("penerjemah", "select", "<strong>Perlu Penerjemah</strong>", $penerjemah,"y",NULL,false,NULL,false);
        $this->uitable->addModal ( "bahasa", "text", "Bahasa", $the_row ['bahasa'] ,"y",null,false,null,false);
        $isyarat=array(
				array("name"=>"","value"=>"","default"=> $the_row ["bhs_isyarat"]==""?1:0),
				array("name"=>"Tidak","value"=>"Tidak","default"=> $the_row ["bhs_isyarat"]=="Tidak"?1:0),
				array("name"=>"Ya","value"=>"Ya","default"=> $the_row ["bhs_isyarat"]=="Ya"?1:0)
		);
        $this->uitable->addModal("bhs_isyarat", "select", "<strong>Bahasa Isyarat</strong>", $isyarat,"y",NULL,false,NULL,false);
        $hambatan_belajar=array(
				array("name"=>"","value"=>"","default"=> $the_row ["hambatan_belajar"]==""?1:0),
				array("name"=>"Tidak","value"=>"Tidak","default"=> $the_row ["hambatan_belajar"]=="Tidak"?1:0),
				array("name"=>"Ya","value"=>"Ya","default"=> $the_row ["hambatan_belajar"]=="Ya"?1:0)
		);
        $this->uitable->addModal("hambatan_belajar", "select", "<strong>Hambatan Belajar</strong>", $hambatan_belajar,"y",NULL,false,NULL,false);
        $this->uitable->addModal ( "hambatan_belajar_ket", "text", "Jika Ya, Jelaskan", $the_row ['hambatan_belajar_ket'] ,"y",null,false,null,false);
        $this->uitable->addModal ( "catatan_lain", "textarea", "<strong>Jika Ya, Jelaskan</strong>", $the_row ['catatan_lain'] ,"y",null,false,null,false);
        
        
        $modal = $this->uitable->getModal ();
		$modal->setTitle ( "Assessment Keperawatan" );
		if ($this->page == "medical_record") {
			echo $this->uitable->getHtml ();
		} else {
			echo "<div class='hide'>" . $this->uitable->getHtml () . "</div>";
			$modal->setAlwaysShow ( true );
		}
		echo $modal->joinFooterAndForm ()->getHtml ();
        
        $hidden_noreg=new Hidden("asp_noreg_pasien","",$this->noreg_pasien);
        $hidden_nama=new Hidden("asp_nama_pasien","",$this->nama_pasien);
        $hidden_nrm=new Hidden("asp_nrm_pasien","",$this->nrm_pasien);
        $hidden_polislug=new Hidden("asp_polislug","",$this->polislug);
        $hidden_the_page=new Hidden("asp_the_page","",$this->page);
        $hidden_the_protoslug=new Hidden("asp_the_protoslug","",$this->protoslug);
        $hidden_the_protoname=new Hidden("asp_the_protoname","",$this->protoname);
        $hidden_the_protoimpl=new Hidden("asp_the_protoimplement","",$this->protoimplement);
        $hidden_antrian=new Hidden("asp_id_antrian","",$this->id_antrian);
        $hidden_action=new Hidden("asp_action","",$this->action);
        
        echo $hidden_noreg->getHtml();
        echo $hidden_nama->getHtml();
        echo $hidden_nrm->getHtml();
        echo $hidden_polislug->getHtml();
        echo $hidden_the_page->getHtml();
        echo $hidden_the_protoslug->getHtml();
        echo $hidden_the_protoname->getHtml();
        echo $hidden_the_protoimpl->getHtml();
        echo $hidden_antrian->getHtml();
        echo $hidden_action->getHtml();

        echo addJS("medical_record/resource/js/asesmen_perawat.js",false);
        echo addCSS ( "framework/bootstrap/css/datepicker.css" );
        echo addJS ( "framework/bootstrap/js/bootstrap-datetimepicker.js" );
        echo addCSS ( "framework/bootstrap/css/bootstrap-datetimepicker.css" );
    }
    
    public function cssPreLoad() {
        ?>
		<style type="text/css">
				#<?php echo $this->action;?>_add_form_form>div {clear: both; width: 100%;}
				#<?php echo $this->action; ?>_add_form_form>div label { width: 250px; }
		</style>
		<?php
    }
    
    public function superCommand($super_command) {
        $super = new SuperCommand ();
        
        /* PASIEN */
        if($super_command=="asp_pasien"){
            $phead=array ('Nama','NRM',"No Reg" );
            $ptable = new Table ( $phead, "", NULL, true );
            $ptable->setName ( "asp_pasien" );
            $ptable->setModel ( Table::$SELECT );
            $padapter = new SimpleAdapter ();
            $padapter->add ( "Nama", "nama_pasien" );
            $padapter->add ( "NRM", "nrm", "digit8" );
            $padapter->add ( "No Reg", "id" );
            $presponder = new ServiceResponder ( $this->db, $ptable, $padapter, "get_registered" );		
            $super->addResponder ( "asp_pasien", $presponder );            
        }
		
        /* DOKTER */
        if($super_command=="dokter_asp"){
            $eadapt = new SimpleAdapter ();
            $eadapt->add ( "Jabatan", "nama_jabatan" );
            $eadapt->add ( "Nama", "nama" );
            $eadapt->add ( "NIP", "nip" );
            $head_karyawan=array ('Nama','Jabatan',"NIP");
            $dktable = new Table ( $head_karyawan, "", NULL, true );
            $dktable->setName ( "dokter_asp" );
            $dktable->setModel ( Table::$SELECT );
            $employee = new EmployeeResponder ( $this->db, $dktable, $eadapt, "dokter" );
            $super->addResponder ( "dokter_asp", $employee );
		}
        
        /* DOKTER DPJP 
        if($super_command=="dokter_dpjp_asp"){
            $eadapt = new SimpleAdapter ();
            $eadapt->add ( "Jabatan", "nama_jabatan" );
            $eadapt->add ( "Nama", "nama" );
            $eadapt->add ( "NIP", "nip" );
            $head_karyawan=array ('Nama','Jabatan',"NIP");
            $dktable = new Table ( $head_karyawan, "", NULL, true );
            $dktable->setName ( "dokter_dpjp_asp" );
            $dktable->setModel ( Table::$SELECT );
            $employee = new EmployeeResponder ( $this->db, $dktable, $eadapt, "dokter" );
            $super->addResponder ( "dokter_dpjp_asp", $employee );
		}*/
        
        /* PERAWAT */
        if($super_command=="perawat_asp"){
            $eadapt = new SimpleAdapter ();
            $eadapt->add ( "Jabatan", "nama_jabatan" );
            $eadapt->add ( "Nama", "nama" );
            $eadapt->add ( "NIP", "nip" );
            $head_karyawan=array ('Nama','Jabatan',"NIP");
            $dktable = new Table ( $head_karyawan, "", NULL, true );
            $dktable->setName ( "perawat_asp" );
            $dktable->setModel ( Table::$SELECT );
            $employee = new EmployeeResponder ( $this->db, $dktable, $eadapt, "perawat" );
            $super->addResponder ( "perawat_asp", $employee );
		}
        
        $init = $super->initialize ();
		if ($init != null) {
			echo $init;
			return;
		}
    }
}

?>