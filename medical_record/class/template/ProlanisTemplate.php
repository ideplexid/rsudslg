<?php
require_once 'smis-framework/smis/template/ModulTemplate.php';
require_once ("smis-base/smis-include-service-consumer.php");
require_once "medical_record/class/service/RuanganService.php";
require_once 'smis-libs-hrd/EmployeeResponder.php';

class ProlanisTemplate extends ModulTemplate {
	protected $db;
	protected $polislug;
	protected $page;
	protected $protoslug;
	protected $protoname;
	protected $protoimplement;
	protected $action;
	protected $dbtable;
	protected $uitable;
	protected $noreg_pasien;
	protected $nama_pasien;
	protected $nrm_pasien;
	protected $alamat;
	protected $umur;
	protected $jk;
	protected $ruang_asal;
	protected $id_antrian;
    protected $carabayar;
    protected $no_telp;
    protected $no_bpjs;
    
	public function __construct($db, $polislug = "all", $noreg = "", $nrm = "", $nama = "", $alamat = "", $umur = "", $jk = "0", $id_antrian = "0", $ruang_asal = "", $page = "medical_record", $action = "lap_gigimulut",$carabayar, $protoslug = "", $protoname = "", $protoimplement = "") {
		$this->db = $db;
		$this->noreg_pasien = $noreg;
		$this->id_antrian = $id_antrian;
		
		$this->nama_pasien = $nama;
		$this->nrm_pasien = $nrm;
		$this->alamat = $alamat;
		$this->umur = $umur;
		$this->jk = $jk;
		$this->ruang_asal = $ruang_asal;
		$this->polislug = $polislug;
		$this->dbtable = new DBTable ( $this->db, "smis_mr_prolanis" );
		$this->page = $page;
		$this->protoslug = $protoslug;
		$this->protoimplement = $protoimplement;
		$this->protoname = $protoname;
		$this->action = $action;
        $this->carabayar=$carabayar;
		if ($polislug != "all")
			$this->dbtable->addCustomKriteria ( "ruangan", "='" . $polislug . "'" );
		if ($noreg != "")
			$this->dbtable->addCustomKriteria ( "noreg_pasien", "='" . $noreg . "'" );
		$head=array ('NRM',"Pasien","No Register" );
		$this->uitable = new Table ($head , " Indikator " . ($this->polislug == "all" ? "" : ucfirst ( $this->protoname )), NULL, true );
		$this->uitable->setName ( $action );
        
        /*getting additional data from registration*/
        if ($noreg != "") {
			$data_post = array ("command" => "edit","id" => $noreg );
			$service = new ServiceConsumer($this->db, "get_registered", $data_post);
			$service->execute ();
			$data = $service->getContent ();
			if($data !=NULL){
				$this->no_telp = $data ['telpon']!=""?$data ['telpon']:$data ['telponpenanggungjawab'];
				$this->no_bpjs = $data ['nobpjs'];
			}
		}
	}
	
	public function command($command) {
        require_once "smis-base/smis-include-duplicate.php";
		$adapter = new SimpleAdapter ();
		$adapter->add ( "Pasien", "nama_pasien" );
		$adapter->add ( "NRM", "nrm_pasien", "digit8" );
		$adapter->add ( "No Register", "noreg_pasien", "digit8" );
		$dbres = new DuplicateResponder ( $this->dbtable, $this->uitable, $adapter );
		$dbres->setDuplicate(false,"");
        $dbres->setAutonomous(getSettings($this->db,"smis_autonomous_id",""));
        $data = $dbres->command ( $_POST ['command'] );
		echo json_encode ( $data );
		return;
	}
	public function phpPreLoad() {
		$service = new RuanganService ( $this->db );
		$service->execute ();
		$ruangan = $service->getContent ();
		
		if ($this->noreg_pasien != "") {
			$this->uitable->addModal ( "nama_pasien", "hidden", "", $this->nama_pasien, "n", null, true );
			$this->uitable->addModal ( "nrm_pasien", "hidden", "", $this->nrm_pasien, "n", null, true );
			$this->uitable->addModal ( "noreg_pasien", "hidden", "", $this->noreg_pasien, "n", null, true );
			$this->uitable->addModal ( "alamat", "hidden", "", $this->alamat, "n", null, true );
			$this->uitable->addModal ( "jk", "hidden", "", $this->jk, "n", null, true );
			$this->uitable->addModal ( "umur", "hidden", "", $this->umur, "n", null, true );
			$this->uitable->addModal ( "ruang_asal", "hidden", "", $this->ruang_asal, "n", null, true );
            $this->uitable->addModal ( "no_telp", "text", "No Telp", $this->no_telp, "n", null, false );
            $this->uitable->addModal ( "no_bpjs", "text", "No BPJS", $this->no_bpjs, "n", null, false );            
		} else {
			$this->uitable->addModal ( "nama_pasien", "chooser-" . $this->action . "-prolanis_pasien-Pasien", "Pasien", $this->nama_pasien, "n", null, true );
			$this->uitable->addModal ( "nrm_pasien", "text", "NRM", $this->nrm_pasien, "n", null, true );
			$this->uitable->addModal ( "noreg_pasien", "text", "No Reg", $this->noreg_pasien, "n", null, true );
		}
		if ($this->polislug == "all")
			$this->uitable->addModal ( "ruangan", "select", "Asal Ruangan", $ruangan );
		else
			$this->uitable->addModal ( "ruangan", "hidden", "", $this->polislug, "n", null, true );
		
		$the_row = array ();
		$the_row ['gds'] 		    = "";
		$the_row ['gdp']		    = "";
		$the_row ['gdpp'] 			= "";
		$the_row ['tensi'] 		    = "";
		$the_row ['diagnosa'] 		= "";
		$the_row ["id"] 			= "";
		$the_row ["dokter"] 		= "";
		$the_row ["id_dokter"] 		= "";
		$the_row ['tanggal'] 		= date ( "Y-m-d" );
		$the_row ["carabayar"] 		= $this->carabayar;
        
		$exist ['noreg_pasien'] = $this->noreg_pasien;
		$exist ['nama_pasien'] = $this->nama_pasien;
		$exist ['nrm_pasien'] = $this->nrm_pasien;
		$exist ['ruangan'] = $this->polislug;
		if ($this->dbtable->is_exist ( $exist )) {
			$row = $this->dbtable->select ( $exist );
			$the_row ['gds'] 		= $row->gds;
            $the_row ['gdp'] 		= $row->gdp;
            $the_row ['gdpp'] 		= $row->gdpp;
            $the_row ['tensi'] 		= $row->tensi;
            $the_row ['diagnosa']   = $row->diagnosa;
            
			$the_row ["id"] 		= $row->id;
			$the_row ['tanggal'] 	= $row->tanggal;
			$the_row ["dokter"] 	= $row->dokter;
			$the_row ["id_dokter"] 	= $row->id_dokter;  
            
            /*auto insert diagnosa dan tensi*/
            if($row->diagnosa=="" || $row->tensi==""){
                $dbtable=new DBTable($this->db,"smis_mr_diagnosa");
                $diagnosa=$dbtable->select(array("noreg_pasien"=>$this->noreg_pasien));
                if($diagnosa!=null){
                    if($row->diagnosa=="") {
                        $the_row ['diagnosa']   = $diagnosa->diagnosa;
                    }
                    if($row->tensi=="") {
                        $the_row ['tensi']      = $diagnosa->tensi;
                    }
                }
            }
          
		} else {
			$exist ['umur'] = $this->umur;
			$exist ['jk'] = $this->jk;
			$exist ['asal_ruang'] = $this->ruang_asal;
			$exist ['alamat'] = $this->alamat;
			$exist ['noreg_pasien'] = $this->noreg_pasien;
			$exist ['nrm_pasien'] = $this->nrm_pasien;
			$exist ['nama_pasien'] = $this->nama_pasien;
			$exist ['tanggal'] = date ( "Y-m-d" );
            
            /*auto insert diagnosa dan tensi*/
            $dbtable=new DBTable($this->db,"smis_mr_diagnosa");
            $diagnosa=$dbtable->select(array("noreg_pasien"=>$this->noreg_pasien));
            if($diagnosa!=null){
                $the_row ['diagnosa']   = $diagnosa->diagnosa;
                $the_row ['tensi']      = $diagnosa->tensi;
            }
            
            $this->dbtable->insert ( $exist );
			$the_row ["id"] = $this->dbtable->get_inserted_id ();
            $update['origin_id']=$the_row ["id"];
            $update['autonomous']="[".getSettings($this->db,"smis_autonomous_id","")."]";
            $this->dbtable->update($update,array("id"=>$the_row ["id"]));
		}
        
		$this->uitable->addModal ( "id", "hidden", "", $the_row ['id'] );
		$this->uitable->addModal ( "dokter", "chooser-prolanis_pasien-dokter_lgm-Dokter Pemeriksa", "Dokter Pemeriksa", $the_row ['dokter_lgm'],"y",null,false,null,true,"tanggal" );
		$this->uitable->addModal ( "id_dokter", "hidden", "", $the_row ['id_dokter'],"y",null,false,null,false);
		$this->uitable->addModal ( "tanggal", "date", "Tanggal", $the_row ['tanggal'] ,"y",null,false,null,false,"");
		$this->uitable->addModal ( "carabayar", "text", "Carabayar", $the_row ['carabayar'] ,"n",null,true,null,false,"");
        $this->uitable->addModal ( "gds", "text", "GDS (Gula Darah Sesaat)", $the_row ['gds'] ,"n",null,false,null,false,"");
		$this->uitable->addModal ( "gdp", "text", "GDP (Gula Darah Puasa)", $the_row ['gdp'] ,"n",null,false,null,false,"");
		$this->uitable->addModal ( "gdpp", "text", "GDPP (Gula Darah Pemeriksaan Pasca 2 Jam Makan)", $the_row ['gdpp'] ,"n",null,false,null,false,"");
		$this->uitable->addModal ( "tensi", "text", "Tensi", $the_row ['tensi'] ,"n",null,false,null,false,"");
		$this->uitable->addModal ( "diagnosa", "text", "Diagnosa", $the_row ['diagnosa'] ,"n",null,false,null,false,"");
		
		
		$modal = $this->uitable->getModal ();
		$modal->setTitle ( "Laporan Gigi & Mulut" );
		if ($this->page == "medical_record" && $this->action == "iklin") {
			echo $this->uitable->getHtml ();
		} else {
			echo "<div class='hide'>" . $this->uitable->getHtml () . "</div>";
			$modal->setAlwaysShow ( true );
		}
		echo $modal->joinFooterAndForm ()->getHtml ();
	}
    
	public function jsLoader() {
		echo addJS ( "framework/bootstrap/js/bootstrap-datepicker.js" );
		echo addJS ( "framework/smis/js/table_action.js" );
		loadLibrary ( "smis-libs-function-javascript" );
	}
    
	public function cssLoader() {
		echo addCSS ( "framework/bootstrap/css/datepicker.css" );
	}
	
	public function cssPreLoad() {
		?>
		<style type="text/css">
				#<?php echo $this->action;?>_add_form_form>div {clear: both; width: 100%;}
				#<?php echo $this->action; ?>_add_form_form>div label { width: 350px; }
		</style>
		<?php
	}
	
	/* when it's star build */
	public function jsPreLoad() {
		?>
		<script type="text/javascript">
		var <?php echo $this->action; ?>;
		var prolanis_pasien;
		var prolanis_noreg="<?php echo $this->noreg_pasien; ?>";
		var prolanis_nama_pasien="<?php echo $this->nama_pasien; ?>";
		var prolanis_nrm_pasien="<?php echo $this->nrm_pasien; ?>";
		var prolanis_polislug="<?php echo $this->polislug; ?>";
		var prolanis_the_page="<?php echo $this->page; ?>";
		var prolanis_the_protoslug="<?php echo $this->protoslug; ?>";
		var prolanis_the_protoname="<?php echo $this->protoname; ?>";
		var prolanis_the_protoimplement="<?php echo $this->protoimplement; ?>";
		var prolanis_id_antrian="<?php echo $this->id_antrian; ?>";
		var prolanis_action="<?php echo $this->action; ?>";
		$(document).ready(function() {
			$('.mydate').datepicker();
			prolanis_pasien=new TableAction("prolanis_pasien",prolanis_the_page,prolanis_action,new Array());
			prolanis_pasien.setSuperCommand("prolanis_pasien");
			prolanis_pasien.setPrototipe(prolanis_the_protoname,prolanis_the_protoslug,prolanis_the_protoimplement);
			prolanis_pasien.selected=function(json){
				var nama=json.nama_pasien;
				var nrm=json.nrm;
				var noreg=json.id;		
				$("#"+prolanis_action+"_nama_pasien").val(nama);
				$("#"+prolanis_action+"_nrm_pasien").val(nrm);
				$("#"+prolanis_action+"_noreg_pasien").val(noreg);
			};

			dokter_lgm=new TableAction("dokter_lgm",prolanis_the_page,prolanis_action,new Array());
			dokter_lgm.setSuperCommand("dokter_lgm");
			dokter_lgm.setPrototipe(prolanis_the_protoname,prolanis_the_protoslug,prolanis_the_protoimplement);
			dokter_lgm.selected=function(json){
				$("#"+prolanis_action+"_dokter").val(json.nama);
				$("#"+prolanis_action+"_id_dokter").val(json.id);
			};

			stypeahead("#"+prolanis_action+"_dokter_lgm",3,dokter_lgm,"nama",function(item){
				$("#"+prolanis_action+"_dokter").val(json.nama);
				$("#"+prolanis_action+"_id_dokter").val(json.id);			
			});

			var column=new Array(
					"id","tanggal","ruangan",
                    "no_telp","no_bpjs","gdp","gdpp","gds","tensi","diagnosa",
					"nama_pasien","noreg_pasien","nrm_pasien",
					"alamat","umur","jk","carabayar",
					"dokter","id_dokter",
					);
			<?php echo $this->action; ?>=new TableAction("<?php echo $this->action; ?>",prolanis_the_page,"<?php echo $this->action; ?>",column);
			<?php echo $this->action; ?>.setPrototipe(prolanis_the_protoname,prolanis_the_protoslug,prolanis_the_protoimplement);
			<?php echo $this->action; ?>.getRegulerData=function(){
				var reg_data={	
						page:this.page,
						action:this.action,
						super_command:this.super_command,
						prototype_name:this.prototype_name,
						prototype_slug:this.prototype_slug,
						prototype_implement:this.prototype_implement,
						polislug:prolanis_polislug,
						noreg_pasien:prolanis_noreg,
						nama_pasien:prolanis_nama_pasien,
						nrm_pasien:prolanis_nrm_pasien,
						id_antrian:prolanis_id_antrian
						};
				return reg_data;
			};

			<?php echo $this->action; ?>.setNextEnter();
			<?php echo $this->action; ?>.setEnableAutofocus(true);
			<?php
		
			if ($this->page == "medical_record" && $this->action == "iklin") {
				echo $this->action;
				?>.view();<?php
			} else {
				echo $this->action . ".clear=function(){return;};";
			}
			?>
		});
		</script>
<?php
	}
	public function superCommand($super_command) {
		/* PASIEN */
		$super = new SuperCommand ();
		
		$phead=array ('Nama','NRM',"No Reg" );
		$ptable = new Table ( $phead, "", NULL, true );
		$ptable->setName ( "prolanis_pasien" );
		$ptable->setModel ( Table::$SELECT );
		$padapter = new SimpleAdapter ();
		$padapter->add ( "Nama", "nama_pasien" );
		$padapter->add ( "NRM", "nrm", "digit8" );
		$padapter->add ( "No Reg", "id" );
		$presponder = new ServiceResponder ( $this->db, $ptable, $padapter, "get_registered" );		
		$super->addResponder ( "prolanis_pasien", $presponder );
		
		$eadapt = new SimpleAdapter ();
		$eadapt->add ( "Jabatan", "nama_jabatan" );
		$eadapt->add ( "Nama", "nama" );
		$eadapt->add ( "NIP", "nip" );
		
		$head_karyawan=array ('Nama','Jabatan',"NIP");
		$dktable = new Table ( $head_karyawan, "", NULL, true );
		$dktable->setName ( "dokter_lgm" );
		$dktable->setModel ( Table::$SELECT );
		$employee = new EmployeeResponder ( $this->db, $dktable, $eadapt, "dokter" );
		$super->addResponder ( "dokter_lgm", $employee );
		
		$init = $super->initialize ();
		if ($init != null) {
			echo $init;
			return;
		}
	}
}

?>