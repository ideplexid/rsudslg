<?php
require_once 'smis-framework/smis/template/ModulTemplate.php';
require_once ("smis-base/smis-include-service-consumer.php");
require_once "medical_record/class/service/RuanganService.php";
require_once 'smis-libs-hrd/EmployeeResponder.php';
class LaporanRL39Template extends ModulTemplate {
	protected $db;
	protected $polislug;
	protected $page;
	protected $protoslug;
	protected $protoname;
	protected $protoimplement;
	protected $action;
	protected $dbtable;
	protected $uitable;
	protected $noreg_pasien;
	protected $nama_pasien;
	protected $nrm_pasien;
	protected $alamat;
	protected $umur;
	protected $jk;
	protected $ruang_asal;
	protected $id_antrian;
	public function __construct($db, $polislug = "all", $noreg = "", $nrm = "", $nama = "", $alamat = "", $umur = "", $jk = "0", $id_antrian = "0", $ruang_asal = "", $page = "medical_record", $action = "lap_rehabilitasi_medik", $protoslug = "", $protoname = "", $protoimplement = "") {
		$this->db = $db;
		$this->noreg_pasien = $noreg;
		$this->id_antrian = $id_antrian;
		
		$this->nama_pasien = $nama;
		$this->nrm_pasien = $nrm;
		$this->alamat = $alamat;
		$this->umur = $umur;
		$this->jk = $jk;
		$this->ruang_asal = $ruang_asal;
		$this->polislug = $polislug;
		$this->dbtable = new DBTable ( $this->db, "smis_mr_rehabilitasi_medik" );
		$this->page = $page;
		$this->protoslug = $protoslug;
		$this->protoimplement = $protoimplement;
		$this->protoname = $protoname;
		$this->action = $action;
		if ($polislug != "all")
			$this->dbtable->addCustomKriteria ( "ruangan", "='" . $polislug . "'" );
		if ($noreg != "")
			$this->dbtable->addCustomKriteria ( "noreg_pasien", "='" . $noreg . "'" );
		$head=array ('NRM',"Pasien","No Register" );
		$this->uitable = new Table ($head , " Rehabilitasi Medik " . ($this->polislug == "all" ? "" : ucfirst ( $this->protoname )), NULL, true );
		$this->uitable->setName ( $action );
		
		if ($this->page == "medical_record" && $this->action == "lap_rehabilitasi_medik") {
			$this->uitable->setAddButtonEnable ( false );
			$this->uitable->setDelButtonEnable ( false );
		}
	}
	
	
	
	public function command($command) {
		$adapter = new SimpleAdapter ();
		$adapter->add ( "Pasien", "nama_pasien" );
		$adapter->add ( "NRM", "nrm_pasien", "digit8" );
		$adapter->add ( "No Register", "noreg_pasien", "digit8" );
		$dbres = new DBResponder ( $this->dbtable, $this->uitable, $adapter );
		$data = $dbres->command ( $_POST ['command'] );
		echo json_encode ( $data );
		return;
	}
	public function phpPreLoad() {
		
		if ($this->noreg_pasien != "") {
			$this->uitable->addModal ( "nama_pasien", "hidden", "", $this->nama_pasien, "n", null, true );
			$this->uitable->addModal ( "nrm_pasien", "hidden", "", $this->nrm_pasien, "n", null, true );
			$this->uitable->addModal ( "noreg_pasien", "hidden", "", $this->noreg_pasien, "n", null, true );
		} else {
			$this->uitable->addModal ( "nama_pasien", "chooser-" . $this->action . "-rl39_pasien", "Pasien", $this->nama_pasien, "n", null, true );
			$this->uitable->addModal ( "nrm_pasien", "text", "NRM", $this->nrm_pasien, "n", null, true );
			$this->uitable->addModal ( "noreg_pasien", "text", "No Reg", $this->noreg_pasien, "n", null, true );
		}
		if ($this->polislug == "all"){
			$service = new RuanganService ( $this->db );
			$service->execute ();
			$ruangan = $service->getContent ();
			$this->uitable->addModal ( "ruangan", "select", "Asal Ruangan", $ruangan );
		}else{
			$this->uitable->addModal ( "ruangan", "hidden", "", $this->polislug, "n", null, true );
		}
		$the_row = array ();
		$the_row ["id"] 	 = "";
		$the_row ['tanggal'] = date ( "Y-m-d" );
		
		$exist ['noreg_pasien'] = $this->noreg_pasien;
		$exist ['nama_pasien'] = $this->nama_pasien;
		$exist ['nrm_pasien'] = $this->nrm_pasien;
		$exist ['ruangan'] = $this->polislug;
		if ($this->dbtable->is_exist ( $exist )) {
			$row = $this->dbtable->select ( $exist );
			$the_row[ "id" ] = $row->id;
			$the_row[ 'tanggal' ] = $row->tanggal;
			$the_row[ 'medis_gait_analyzer' ] = $row->medis_gait_analyzer;
			$the_row[ 'medis_e_m_g' ] = $row->medis_e_m_g;
			$the_row[ 'medis_uro_dinamic' ] = $row->medis_uro_dinamic;
			$the_row[ 'medis_side_back' ] = $row->medis_side_back;
			$the_row[ 'medis_e_n_tree' ] = $row->medis_e_n_tree;
			$the_row[ 'medis_spyrometer' ] = $row->medis_spyrometer;
			$the_row[ 'medis_static_bicycle' ] = $row->medis_static_bicycle;
			$the_row[ 'medis_tread_mill' ] = $row->medis_tread_mill;
			$the_row[ 'medis_body_platysmograf' ] = $row->medis_body_platysmograf;
			$the_row[ 'medis_lain_lain' ] = $row->medis_lain_lain;
			$the_row[ 'fisioterapi_latihan_fisik' ] = $row->fisioterapi_latihan_fisik;
			$the_row[ 'fisioterapi_aktinoterapi' ] = $row->fisioterapi_aktinoterapi;
			$the_row[ 'fisioterapi_elektroterapi' ] = $row->fisioterapi_elektroterapi;
			$the_row[ 'fisioterapi_hidroterapi' ] = $row->fisioterapi_hidroterapi;
			$the_row[ 'fisioterapi_traksi_lumbal_cervical' ] = $row->fisioterapi_traksi_lumbal_cervical;
			$the_row[ 'fisioterapi_lain_lain' ] = $row->fisioterapi_lain_lain;
			$the_row[ 'okupasiterapi_snoosien_room' ] = $row->okupasiterapi_snoosien_room;
			$the_row[ 'okupasiterapi_sensori_integrasi' ] = $row->okupasiterapi_sensori_integrasi;
			$the_row[ 'okupasiterapi_latihan_aktivitas_kehidupan_sehari_hari' ] = $row->okupasiterapi_latihan_aktivitas_kehidupan_sehari_hari;
			$the_row[ 'okupasiterapi_proper_body_mekanik' ] = $row->okupasiterapi_proper_body_mekanik;
			$the_row[ 'okupasiterapi_pembuatan_alat_lontar_adaptasi_alat' ] = $row->okupasiterapi_pembuatan_alat_lontar_adaptasi_alat;
			$the_row[ 'okupasiterapi_analisa_persiapan_kerja' ] = $row->okupasiterapi_analisa_persiapan_kerja;
			$the_row[ 'okupasiterapi_latihan_relaksasi' ] = $row->okupasiterapi_latihan_relaksasi;
			$the_row[ 'okupasiterapi_analisa_intervensi_persepsi_kognitif_psikomot' ] = $row->okupasiterapi_analisa_intervensi_persepsi_kognitif_psikomot;
			$the_row[ 'okupasiterapi_lain_lain' ] = $row->okupasiterapi_lain_lain;
			$the_row[ 'terapi_wicara_fungsi_bicara' ] = $row->terapi_wicara_fungsi_bicara;
			$the_row[ 'terapi_wicara_fungsi_bahasa_laku' ] = $row->terapi_wicara_fungsi_bahasa_laku;
			$the_row[ 'terapi_wicara_fungsi_menelan' ] = $row->terapi_wicara_fungsi_menelan;
			$the_row[ 'terapi_wicara_lain_lain' ] = $row->terapi_wicara_lain_lain;
			$the_row[ 'psikologi_psikolog_anak' ] = $row->psikologi_psikolog_anak;
			$the_row[ 'psikologi_psikolog_dewasa' ] = $row->psikologi_psikolog_dewasa;
			$the_row[ 'psikologi_lain_lain' ] = $row->psikologi_lain_lain;
			$the_row[ 'sosial_medis_evaluasi_lingkungan_rumah' ] = $row->sosial_medis_evaluasi_lingkungan_rumah;
			$the_row[ 'sosial_medis_evaluasi_ekonomi' ] = $row->sosial_medis_evaluasi_ekonomi;
			$the_row[ 'sosial_medis_evaluasi_pekerjaan' ] = $row->sosial_medis_evaluasi_pekerjaan;
			$the_row[ 'sosial_medis_lain_lain' ] = $row->sosial_medis_lain_lain;
			$the_row[ 'ortotik_prostetik_pembuatan_alat_bantu' ] = $row->ortotik_prostetik_pembuatan_alat_bantu;
			$the_row[ 'ortotik_prostetik_pembuatan_alat_anggota_tiruan' ] = $row->ortotik_prostetik_pembuatan_alat_anggota_tiruan;
			$the_row[ 'ortotik_prostetik_lain_lain' ] = $row->ortotik_prostetik_lain_lain;
			$the_row[ 'kunjungan_rumah' ] = $row->kunjungan_rumah;
		} else {
			$exist ['noreg_pasien'] 		= $this->noreg_pasien;
			$exist ['nrm_pasien'] 			= $this->nrm_pasien;
			$exist ['nama_pasien'] 			= $this->nama_pasien;
			$exist ['tanggal'] 				= date("Y-m-d");
			$this->dbtable->insert($exist);
			$the_row ["id"] 				= $this->dbtable->get_inserted_id();
		}
				
		$this->uitable->addModal("id", "hidden", "", $the_row ['id']);
		$this->uitable->addModal("tanggal", "date", "Tanggal", $the_row ['tanggal'] ,"y",null,false,null,false,"cara_datang");
		$this->uitable->addModal("medis_gait_analyzer", "checkbox", "Medis - Gait Analyzer", "");
		$this->uitable->addModal("medis_e_m_g", "checkbox", "Medis - E M G", "");
		$this->uitable->addModal("medis_uro_dinamic", "checkbox", "Medis - Uro Dinamic", "");
		$this->uitable->addModal("medis_side_back", "checkbox", "Medis - Side Back", "");
		$this->uitable->addModal("medis_e_n_tree", "checkbox", "Medis - E N Tree", ""); 
		$this->uitable->addModal("medis_spyrometer", "checkbox", "Medis Spyrometer", "");
		$this->uitable->addModal("medis_static_bicycle", "checkbox", "Medis - Static Bicycle", "");
		$this->uitable->addModal("medis_tread_mill", "checkbox", "Medis - Tread Mill", "");
		$this->uitable->addModal("medis_body_platysmograf", "checkbox", "Medis - Body Platysmograf", "");
		$this->uitable->addModal("medis_lain_lain", "checkbox", "Medis - Lain-Lain", "");
		$this->uitable->addModal("fisioterapi_latihan_fisik", "checkbox", "Fisioterapi - Latihan Fisik", "");
		$this->uitable->addModal("fisioterapi_aktinoterapi", "checkbox", "Fisioterapi - Aktinoterapi", "");
		$this->uitable->addModal("fisioterapi_elektroterapi", "checkbox", "Fisioterapi - Elektroterapi", "");
		$this->uitable->addModal("fisioterapi_hidroterapi", "checkbox", "Fisioterapi - Hidroterapi", "");
		$this->uitable->addModal("fisioterapi_traksi_lumbal_cervical", "checkbox", "Fisioterapi - Traksi Lumbal & Cervical", "");
		$this->uitable->addModal("fisioterapi_lain_lain", "checkbox", "Fisioterapi - Lain-Lain", "");
		$this->uitable->addModal("okupasiterapi_snoosien_room", "checkbox", "Okupasiterapi - Snoosien Room", "");
		$this->uitable->addModal("okupasiterapi_sensori_integrasi", "checkbox", "Okupasiterapi - Sensori Integrasi", "");
		$this->uitable->addModal("okupasiterapi_latihan_aktivitas_kehidupan_sehari_hari", "checkbox", "Okupasiterapi - Lat. Akt. Kehidupan Sehari-hari", "");
		$this->uitable->addModal("okupasiterapi_proper_body_mekanik", "checkbox", "Okupasiterapi - Proper Body Mekanik", "");
		$this->uitable->addModal("okupasiterapi_pembuatan_alat_lontar_adaptasi_alat", "checkbox", "Okupasiterapi - Pembuatan Alat Lontar & Adaptasi Alat", "");
		$this->uitable->addModal("okupasiterapi_analisa_persiapan_kerja", "checkbox", "Okupasiterapi - Analisa Persiapan Kerja", "");
		$this->uitable->addModal("okupasiterapi_latihan_relaksasi", "checkbox", "Okupasiterapi - Latihan Relaksasi", "");
		$this->uitable->addModal("okupasiterapi_analisa_intervensi_persepsi_kognitif_psikomot", "checkbox", "Okupasiterapi - Analisa & Intervensi, Persepsi, Kognitif, Psikomot", "");
		$this->uitable->addModal("okupasiterapi_lain_lain", "checkbox", "Okupasiterapi - Lain-Lain", "");
		$this->uitable->addModal("terapi_wicara_fungsi_bicara", "checkbox", "Terapi Wicara - Fungsi Bicara", "");
		$this->uitable->addModal("terapi_wicara_fungsi_bahasa_laku", "checkbox", "Terapi Wicara - Fungsi Bahasa Laku", "");
		$this->uitable->addModal("terapi_wicara_fungsi_menelan", "checkbox", "Terapi Wicara - Fungsi Menelan", "");
		$this->uitable->addModal("terapi_wicara_lain_lain", "checkbox", "Terapi Wicara - Lain-Lain", "");
		$this->uitable->addModal("psikologi_psikolog_anak", "checkbox", "Psikologi - Psikolog Anak", "");
		$this->uitable->addModal("psikologi_psikolog_dewasa", "checkbox", "Psikologi - Psikolog Dewasa", "");
		$this->uitable->addModal("psikologi_lain_lain", "checkbox", "Psikologi - Lain-Lain", "");
		$this->uitable->addModal("sosial_medis_evaluasi_lingkungan_rumah", "checkbox", "Sosial Medis - Evaluasi Lingkungan Rumah", "");
		$this->uitable->addModal("sosial_medis_evaluasi_ekonomi", "checkbox", "Sosial Medis - Evaluasi Ekonomi", ""); 
		$this->uitable->addModal("sosial_medis_evaluasi_pekerjaan", "checkbox", "Sosial Medis - Pekerjaan", "");
		$this->uitable->addModal("sosial_medis_lain_lain", "checkbox", "Sosial Medis - Lain-Lain", "");
		$this->uitable->addModal("ortotik_prostetik_pembuatan_alat_bantu", "checkbox", "Ortotik Prostetik - Pembuatan Alat Bantu", "");
		$this->uitable->addModal("ortotik_prostetik_pembuatan_alat_anggota_tiruan", "checkbox", "Ortotik Prostetik - Pembuatan Alat Anggota Tiruan", "");
		$this->uitable->addModal("ortotik_prostetik_lain_lain", "checkbox", "Ortotik Prostetik - Lain-Lain", "");
		$this->uitable->addModal("kunjungan_rumah", "checkbox", "Kunjungan Rumah", "");

		$modal = $this->uitable->getModal ();
		$modal->setTitle ( "Laporan RL 3.9 - Rehabilitasi Medik" );
		if ($this->page == "medical_record" && $this->action == "iklin") {
			echo $this->uitable->getHtml ();
		} else {
			echo "<div class='hide'>" . $this->uitable->getHtml () . "</div>";
			$modal->setAlwaysShow ( true );
		}
		echo $modal->joinFooterAndForm ()->getHtml ();
	}
	public function jsLoader() {
		echo addJS ( "framework/bootstrap/js/bootstrap-datepicker.js" );
		echo addJS ( "framework/smis/js/table_action.js" );
		loadLibrary ( "smis-libs-function-javascript" );
	}
	
	public function cssLoader() {
		echo addCSS ( "framework/bootstrap/css/datepicker.css" );
	}
	
	public function cssPreLoad() {
		?>
		<style type="text/css">
				#<?php echo $this->action;?>_add_form_form>div {clear: both; width: 100%;}
				#<?php echo $this->action; ?>_add_form_form>div label { width: 250px; }
		</style>
		<?php
	}
	
	/* when it's star build */
	public function jsPreLoad() {
		?>
		<script type="text/javascript">
		var <?php echo $this->action; ?>;
		var rl39_pasien;
		var rl39_noreg="<?php echo $this->noreg_pasien; ?>";
		var rl39_nama_pasien="<?php echo $this->nama_pasien; ?>";
		var rl39_nrm_pasien="<?php echo $this->nrm_pasien; ?>";
		var rl39_polislug="<?php echo $this->polislug; ?>";
		var rl39_the_page="<?php echo $this->page; ?>";
		var rl39_the_protoslug="<?php echo $this->protoslug; ?>";
		var rl39_the_protoname="<?php echo $this->protoname; ?>";
		var rl39_the_protoimplement="<?php echo $this->protoimplement; ?>";
		var rl39_id_antrian="<?php echo $this->id_antrian; ?>";
		var rl39_action="<?php echo $this->action; ?>";
		$(document).ready(function() {
			$('.mydate').datepicker();
			rl39_pasien=new TableAction("rl39_pasien",rl39_the_page,rl39_action,new Array());
			rl39_pasien.setSuperCommand("rl39_pasien");
			rl39_pasien.setPrototipe(rl39_the_protoname,rl39_the_protoslug,rl39_the_protoimplement);
			rl39_pasien.selected=function(json){
				var nama=json.nama_pasien;
				var nrm=json.nrm;
				var noreg=json.id;		
				$("#"+rl39_action+"_nama_pasien").val(nama);
				$("#"+rl39_action+"_nrm_pasien").val(nrm);
				$("#"+rl39_action+"_noreg_pasien").val(noreg);
			};
			
			var column=new Array(
				"id","tanggal","ruangan",
				"noreg_pasien", "nrm_pasien", "nama_pasien",
				"medis_gait_analyzer","medis_e_m_g","medis_uro_dinamic","medis_side_back","medis_e_n_tree","medis_spyrometer","medis_static_bicycle","medis_tread_mill","medis_body_platysmograf","medis_lain_lain",
				"fisioterapi_latihan_fisik","fisioterapi_aktinoterapi","fisioterapi_elektroterapi","fisioterapi_hidroterapi","fisioterapi_traksi_lumbal_cervical","fisioterapi_lain_lain",
				"okupasiterapi_snoosien_room","okupasiterapi_sensori_integrasi","okupasiterapi_latihan_aktivitas_kehidupan_sehari_hari","okupasiterapi_proper_body_mekanik",
				"okupasiterapi_pembuatan_alat_lontar_adaptasi_alat","okupasiterapi_analisa_persiapan_kerja","okupasiterapi_latihan_relaksasi","okupasiterapi_analisa_intervensi_persepsi_kognitif_psikomot",
				"okupasiterapi_lain_lain",
				"terapi_wicara_fungsi_bicara","terapi_wicara_fungsi_bahasa_laku","terapi_wicara_fungsi_menelan","terapi_wicara_lain_lain",
				"psikologi_psikolog_anak","psikologi_psikolog_dewasa","psikologi_lain_lain",
				"sosial_medis_evaluasi_lingkungan_rumah","sosial_medis_evaluasi_ekonomi","sosial_medis_evaluasi_pekerjaan","sosial_medis_lain_lain",
				"ortotik_prostetik_pembuatan_alat_bantu","ortotik_prostetik_pembuatan_alat_anggota_tiruan","ortotik_prostetik_lain_lain",
				"kunjungan_rumah"			
			);
			<?php echo $this->action; ?>=new TableAction("<?php echo $this->action; ?>",rl39_the_page,"<?php echo $this->action; ?>",column);
			<?php echo $this->action; ?>.setPrototipe(rl39_the_protoname,rl39_the_protoslug,rl39_the_protoimplement);
			<?php echo $this->action; ?>.getRegulerData=function(){
				var reg_data={	
						page:this.page,
						action:this.action,
						super_command:this.super_command,
						prototype_name:this.prototype_name,
						prototype_slug:this.prototype_slug,
						prototype_implement:this.prototype_implement,
						polislug:rl39_polislug,
						noreg_pasien:rl39_noreg,
						nama_pasien:rl39_nama_pasien,
						nrm_pasien:rl39_nrm_pasien,
						id_antrian:rl39_id_antrian
						};
				return reg_data;
			};

			<?php echo $this->action; ?>.setNextEnter();
			<?php echo $this->action; ?>.setEnableAutofocus(true);
			<?php
			if ($this->page == "medical_record" && $this->action == "iklin") {
				echo $this->action;
				?>.view();<?php
			} else {
				echo $this->action . ".clear=function(){return;};";
			}
			?>
		});
		</script>
<?php
	}
	public function superCommand($super_command) {
		/* PASIEN */
		$super = new SuperCommand ();
		
		$phead=array ('Nama','NRM',"No Reg" );
		$ptable = new Table ( $phead, "", NULL, true );
		$ptable->setName ( "rl39_pasien" );
		$ptable->setModel ( Table::$SELECT );
		$padapter = new SimpleAdapter ();
		$padapter->add ( "Nama", "nama_pasien" );
		$padapter->add ( "NRM", "nrm", "digit8" );
		$padapter->add ( "No Reg", "id" );
		$presponder = new ServiceResponder ( $this->db, $ptable, $padapter, "get_registered" );		
		$super->addResponder ( "rl39_pasien", $presponder );
		$init = $super->initialize ();
		if ($init != null) {
			echo $init;
			return;
		}
	}
}

?>