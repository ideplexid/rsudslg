<?php
require_once 'smis-framework/smis/template/ModulTemplate.php';
require_once ("smis-base/smis-include-service-consumer.php");
require_once "medical_record/class/service/RuanganService.php";
require_once 'smis-libs-hrd/EmployeeResponder.php';
class LaporanRL311Template extends ModulTemplate {
	protected $db;
	protected $polislug;
	protected $page;
	protected $protoslug;
	protected $protoname;
	protected $protoimplement;
	protected $action;
	protected $dbtable;
	protected $uitable;
	protected $noreg_pasien;
	protected $nama_pasien;
	protected $nrm_pasien;
	protected $alamat;
	protected $umur;
	protected $jk;
	protected $ruang_asal;
	protected $id_antrian;
	public function __construct($db, $polislug = "all", $noreg = "", $nrm = "", $nama = "", $alamat = "", $umur = "", $jk = "0", $id_antrian = "0", $ruang_asal = "", $page = "medical_record", $action = "lap_gigimulut", $protoslug = "", $protoname = "", $protoimplement = "") {
		$this->db = $db;
		$this->noreg_pasien = $noreg;
		$this->id_antrian = $id_antrian;
		
		$this->nama_pasien = $nama;
		$this->nrm_pasien = $nrm;
		$this->alamat = $alamat;
		$this->umur = $umur;
		$this->jk = $jk;
		$this->ruang_asal = $ruang_asal;
		$this->polislug = $polislug;
		$this->dbtable = new DBTable ( $this->db, "smis_mr_jiwa" );
		$this->page = $page;
		$this->protoslug = $protoslug;
		$this->protoimplement = $protoimplement;
		$this->protoname = $protoname;
		$this->action = $action;
		if ($polislug != "all")
			$this->dbtable->addCustomKriteria ( "ruangan", "='" . $polislug . "'" );
		if ($noreg != "")
			$this->dbtable->addCustomKriteria ( "noreg_pasien", "='" . $noreg . "'" );
		$head=array ('NRM',"Pasien","No Register" );
		$this->uitable = new Table ($head , " Indikator " . ($this->polislug == "all" ? "" : ucfirst ( $this->protoname )), NULL, true );
		$this->uitable->setName ( $action );
		
		if ($this->page == "medical_record" && $this->action == "lap_operasi") {
			$this->uitable->setAddButtonEnable ( false );
			$this->uitable->setDelButtonEnable ( false );
		}
	}
	
	
	
	public function command($command) {
		$adapter = new SimpleAdapter ();
		$adapter->add ( "Pasien", "nama_pasien" );
		$adapter->add ( "NRM", "nrm_pasien", "digit8" );
		$adapter->add ( "No Register", "noreg_pasien", "digit8" );
		$dbres = new DBResponder ( $this->dbtable, $this->uitable, $adapter );
		$data = $dbres->command ( $_POST ['command'] );
		echo json_encode ( $data );
		return;
	}
	public function phpPreLoad() {
		if ($this->noreg_pasien != "") {
			$this->uitable->addModal ( "nama_pasien", "hidden", "", $this->nama_pasien, "n", null, true );
			$this->uitable->addModal ( "nrm_pasien", "hidden", "", $this->nrm_pasien, "n", null, true );
			$this->uitable->addModal ( "noreg_pasien", "hidden", "", $this->noreg_pasien, "n", null, true );
			$this->uitable->addModal ( "alamat", "hidden", "", $this->alamat, "n", null, true );
			$this->uitable->addModal ( "jk", "hidden", "", $this->jk, "n", null, true );
			$this->uitable->addModal ( "umur", "hidden", "", $this->umur, "n", null, true );
			$this->uitable->addModal ( "ruang_asal", "hidden", "", $this->ruang_asal, "n", null, true );
		} else {
			$this->uitable->addModal ( "nama_pasien", "chooser-" . $this->action . "-rl311_pasien", "Pasien", $this->nama_pasien, "n", null, true );
			$this->uitable->addModal ( "nrm_pasien", "text", "NRM", $this->nrm_pasien, "n", null, true );
			$this->uitable->addModal ( "noreg_pasien", "text", "No Reg", $this->noreg_pasien, "n", null, true );
		}
		if ($this->polislug == "all"){
			$service = new RuanganService ( $this->db );
			$service->execute ();
			$ruangan = $service->getContent ();
			$this->uitable->addModal ( "ruangan", "select", "Asal Ruangan", $ruangan );
		}else{
			$this->uitable->addModal ( "ruangan", "hidden", "", $this->polislug, "n", null, true );
		}
		$the_row = array ();
		$the_row ['psikotes'] 				= "";
		$the_row ['konsultasi']				= "";
		$the_row ['medikamentosa'] 			= "";
		$the_row ['elektro_medik'] 			= "";
		$the_row ['psikoterapi'] 			= "";
		$the_row ['play_therapy'] 			= "";
		$the_row ['rehab_medik_psikiatrik'] = "";
		$the_row ["id"] 					= "";
		$the_row ['tanggal'] 				= date ( "Y-m-d" );
		
		$exist ['noreg_pasien'] = $this->noreg_pasien;
		$exist ['nama_pasien'] = $this->nama_pasien;
		$exist ['nrm_pasien'] = $this->nrm_pasien;
		$exist ['ruangan'] = $this->polislug;
		if ($this->dbtable->is_exist ( $exist )) {
			$row = $this->dbtable->select ( $exist );
			$the_row ['psikotes'] 				= $row->psikotes;
			$the_row ['konsultasi']				= $row->konsultasi;
			$the_row ['medikamentosa'] 			= $row->medikamentosa;
			$the_row ['elektro_medik'] 			= $row->elektro_medik;
			$the_row ['psikoterapi'] 			= $row->psikoterapi;
			$the_row ['play_therapy'] 			= $row->play_therapy;
			$the_row ['rehab_medik_psikiatrik'] = $row->rehab_medik_psikiatrik;
			$the_row ["id"] 					= $row->id;
			$the_row ['tanggal'] 				= $row->tanggal;
		} else {
			$exist ['umur'] = $this->umur;
			$exist ['jk'] = $this->jk;
			$exist ['alamat'] = $this->alamat;
			$exist ['noreg_pasien'] = $this->noreg_pasien;
			$exist ['nrm_pasien'] = $this->nrm_pasien;
			$exist ['nama_pasien'] = $this->nama_pasien;
			$exist ['tanggal'] = date ( "Y-m-d" );
			$this->dbtable->insert ( $exist );
			$the_row ["id"] = $this->dbtable->get_inserted_id ();
		}
				
		$this->uitable->addModal ( "id", "hidden", "", $the_row ['id'] );
		$this->uitable->addModal ( "tanggal", "date", "Tanggal", $the_row ['tanggal'] ,"y",null,false,null,false,"psikotes");
		$this->uitable->addModal ( "psikotes", "checkbox", "Psikotes", $the_row ['psikotes'] ,"y",null,false,null,false,"konsultasi");
		$this->uitable->addModal ( "konsultasi", "checkbox", "Konsultasi", $the_row ['konsultasi'] ,"y",null,false,null,false,"medikamentosa");
		$this->uitable->addModal ( "medikamentosa", "checkbox", "Terapi Medikamentosa", $the_row ['medikamentosa'] ,"y",null,false,null,false,"elektro_medik");
		$this->uitable->addModal ( "elektro_medik", "checkbox", "Elektro Medik", $the_row ['elektro_medik'] ,"y",null,false,null,false,"psikoterapi");
		$this->uitable->addModal ( "psikoterapi", "checkbox", "Psikoterapi", $the_row ['psikoterapi'] ,"y",null,false,null,false,"play_therapy");
		$this->uitable->addModal ( "play_therapy", "checkbox", "Play Therapy", $the_row ['play_therapy'] ,"y",null,false,null,false,"rehab_medik_psikiatrik");
		$this->uitable->addModal ( "rehab_medik_psikiatrik", "checkbox", "Rehabilitasi Medik Psikiatrik", $the_row ['rehab_medik_psikiatrik'] ,"y",null,false,null,false,"save");
		
		$modal = $this->uitable->getModal ();
		$modal->setTitle ( "Laporan RL 3.11 - Kesehatan Jiwa" );
		if ($this->page == "medical_record" && $this->action == "iklin") {
			echo $this->uitable->getHtml ();
		} else {
			echo "<div class='hide'>" . $this->uitable->getHtml () . "</div>";
			$modal->setAlwaysShow ( true );
		}
		echo $modal->joinFooterAndForm ()->getHtml ();
	}
	public function jsLoader() {
		echo addJS ( "framework/bootstrap/js/bootstrap-datepicker.js" );
		echo addJS ( "framework/smis/js/table_action.js" );
		loadLibrary ( "smis-libs-function-javascript" );
	}
	
	public function cssLoader() {
		echo addCSS ( "framework/bootstrap/css/datepicker.css" );
	}
	
	public function cssPreLoad() {
		?>
		<style type="text/css">
				#<?php echo $this->action;?>_add_form_form>div {clear: both; width: 100%;}
				#<?php echo $this->action; ?>_add_form_form>div label { width: 250px; }
		</style>
		<?php
	}
	
	/* when it's star build */
	public function jsPreLoad() {
		?>
		<script type="text/javascript">
		var <?php echo $this->action; ?>;
		var rl311_pasien;
		var rl311_noreg="<?php echo $this->noreg_pasien; ?>";
		var rl311_nama_pasien="<?php echo $this->nama_pasien; ?>";
		var rl311_nrm_pasien="<?php echo $this->nrm_pasien; ?>";
		var rl311_polislug="<?php echo $this->polislug; ?>";
		var rl311_the_page="<?php echo $this->page; ?>";
		var rl311_the_protoslug="<?php echo $this->protoslug; ?>";
		var rl311_the_protoname="<?php echo $this->protoname; ?>";
		var rl311_the_protoimplement="<?php echo $this->protoimplement; ?>";
		var rl311_id_antrian="<?php echo $this->id_antrian; ?>";
		var rl311_action="<?php echo $this->action; ?>";
		$(document).ready(function() {
			$('.mydate').datepicker();
			rl311_pasien=new TableAction("rl311_pasien",rl311_the_page,rl311_action,new Array());
			rl311_pasien.setSuperCommand("rl311_pasien");
			rl311_pasien.setPrototipe(rl311_the_protoname,rl311_the_protoslug,rl311_the_protoimplement);
			rl311_pasien.selected=function(json){
				var nama=json.nama_pasien;
				var nrm=json.nrm;
				var noreg=json.id;		
				$("#"+rl311_action+"_nama_pasien").val(nama);
				$("#"+rl311_action+"_nrm_pasien").val(nrm);
				$("#"+rl311_action+"_noreg_pasien").val(noreg);
			};
			
			var column=new Array(
					"id","tanggal","ruangan",
					'psikotes','konsultasi','medikamentosa','elektro_medik',
					'psikoterapi','play_therapy','rehab_medik_psikiatrik'
					);
			<?php echo $this->action; ?>=new TableAction("<?php echo $this->action; ?>",rl311_the_page,"<?php echo $this->action; ?>",column);
			<?php echo $this->action; ?>.setPrototipe(rl311_the_protoname,rl311_the_protoslug,rl311_the_protoimplement);
			<?php echo $this->action; ?>.getRegulerData=function(){
				var reg_data={	
						page:this.page,
						action:this.action,
						super_command:this.super_command,
						prototype_name:this.prototype_name,
						prototype_slug:this.prototype_slug,
						prototype_implement:this.prototype_implement,
						polislug:rl311_polislug,
						noreg_pasien:rl311_noreg,
						nama_pasien:rl311_nama_pasien,
						nrm_pasien:rl311_nrm_pasien,
						id_antrian:rl311_id_antrian
						};
				return reg_data;
			};

			<?php echo $this->action; ?>.setNextEnter();
			<?php echo $this->action; ?>.setEnableAutofocus(true);
			<?php
		
			if ($this->page == "medical_record" && $this->action == "iklin") {
				echo $this->action;
				?>.view();<?php
			} else {
				echo $this->action . ".clear=function(){return;};";
			}
			?>
		});
		</script>
<?php
	}
	public function superCommand($super_command) {
		/* PASIEN */
		$super = new SuperCommand ();
		
		$phead=array ('Nama','NRM',"No Reg" );
		$ptable = new Table ( $phead, "", NULL, true );
		$ptable->setName ( "rl311_pasien" );
		$ptable->setModel ( Table::$SELECT );
		$padapter = new SimpleAdapter ();
		$padapter->add ( "Nama", "nama_pasien" );
		$padapter->add ( "NRM", "nrm", "digit8" );
		$padapter->add ( "No Reg", "id" );
		$presponder = new ServiceResponder ( $this->db, $ptable, $padapter, "get_registered" );		
		$super->addResponder ( "rl311_pasien", $presponder );
		$init = $super->initialize ();
		if ($init != null) {
			echo $init;
			return;
		}
	}
}

?>