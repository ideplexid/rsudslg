<?php
require_once 'smis-framework/smis/template/ModulTemplate.php';
require_once ("smis-base/smis-include-service-consumer.php");
require_once "medical_record/class/service/RuanganService.php";
class LaporanPersalinanTemplate extends ModulTemplate {
	protected $db;
	protected $polislug;
	protected $page;
	protected $protoslug;
	protected $protoname;
	protected $protoimplement;
	protected $action;
	protected $dbtable;
	protected $uitable;
	protected $noreg_pasien;
	protected $nama_pasien;
	protected $nrm_pasien;
	protected $profile;
	protected $sebutan;
	protected $ds;
	protected $kr;
	protected $rujukan;
	protected $kelanjutan;
	protected $tanggal;
	protected $carabayar;    
    protected $alamat;
    protected $no_bpjs;
    protected $no_telp;
    protected $umur;
	public function __construct($db, $polislug = "all", $noreg = "", $nrm = "", $nama = "", $page = "medical_record", $action = "lap_Persalinan", $protoslug = "", $protoname = "", $protoimplement = "",$tanggal=NULL) {
		$this->db = $db;
		$this->tanggal=$tanggal==NULL?date("Y-m-d H:i:s"):$tanggal;
		$this->noreg_pasien = $noreg;
		$this->nama_pasien = $nama;
		$this->nrm_pasien = $nrm;
		$this->polislug = $polislug;
		$this->dbtable = new DBTable ( $this->db, "smis_mr_persalinan" );
		$this->page = $page;
		$this->protoslug = $protoslug;
		$this->protoimplement = $protoimplement;
		$this->protoname = $protoname;
		$this->action = $action;
		if ($polislug != "all")
			$this->dbtable->addCustomKriteria ( "ruangan", "='" . $polislug . "'" );
		if ($noreg != "")
			$this->dbtable->addCustomKriteria ( "noreg_pasien", "='" . $noreg . "'" );
		
		if ($noreg != "") {
			$data_post = array ("command" => "edit","id" => $noreg );
			$service = new ServiceConsumer($this->db, "get_registered", $data_post);
			$service->execute ();
			$data = $service->getContent ();
			if($data!=NULL){
				$this->ds               = $data ['caradatang']=="Rujukan"?"0":"1";
				$this->kelanjutan       = $data ['uri']=="0"?"RJ":"MRS";
				$this->rujukan          = $data ['rujukan'];
                $this->alamat           = $data ['alamat_pasien']." ".$data ['nama_kelurahan']." ".$data ['nama_kecamatan']." ".$data ['nama_kabupaten']." ".$data ['nama_provinsi'];
                $this->no_bpjs          = $data ['nobpjs'];
                $this->no_telp          = $data ['telpon'];
                $this->umur             = $data ['umur'];
                $this->sebutan          = $data ['sebutan'];
                $this->profile_number   = $data ['profile_number'];
			}
			
		}
		
		$array=array ('NRM',"Pasien","No Register" );
		$this->uitable = new Table ( $array, " Indikator " . ($this->polislug == "all" ? "" : ucfirst ( $this->protoname )), NULL, true );
		$this->uitable->setName ( $action );
		
		if ($this->page == "medical_record" && $this->action == "lap_Persalinan") {
			$this->uitable->setAddButtonEnable ( false );
			$this->uitable->setDelButtonEnable ( false );
		}
	}
	public function command($command) {
        require_once "smis-base/smis-include-duplicate.php";
        require_once "medical_record/class/responder/LapPersalinanTemplateResponder.php";
		$adapter = new SimpleAdapter ();
		$adapter->add ( "Pasien", "nama_pasien" );
		$adapter->add ( "NRM", "nrm_pasien", "digit8" );
		$adapter->add ( "No Register", "noreg_pasien", "digit8" );
		$dbres = new LapPersalinanTemplateResponder ( $this->dbtable, $this->uitable, $adapter );
		$dbres->setDuplicate(false,"");
        $dbres->setAutonomous(getSettings($this->db,"smis_autonomous_id",""));
        $data = $dbres->command ( $_POST ['command'] );
		echo json_encode ( $data );
		return;
	}
	public function phpPreLoad() {
		
		if ($this->noreg_pasien != "") {
			$this->uitable->addModal ( "nama_pasien", "hidden", "", $this->nama_pasien, "n", null, true );
			$this->uitable->addModal ( "nrm_pasien", "hidden", "", $this->nrm_pasien, "n", null, true );
			$this->uitable->addModal ( "noreg_pasien", "hidden", "", $this->noreg_pasien, "n", null, true );
            $this->uitable->addModal ( "umur_pasien", "hidden", "", $this->umur, "n", null, true );
            $this->uitable->addModal ( "alamat", "hidden", "", $this->alamat, "n", null, true );
            $this->uitable->addModal ( "sebutan", "hidden", "", $this->sebutan, "n", null, true );
            $this->uitable->addModal ( "profile_number", "hidden", "", $this->profile_number, "n", null, true );
		} else {
			$this->uitable->addModal ( "nama_pasien", "chooser-" . $this->action . "-lPersalinan_pasien", "Pasien", $this->nama_pasien, "n", null, true );
			$this->uitable->addModal ( "nrm_pasien", "text", "NRM", $this->nrm_pasien, "n", null, true );
			$this->uitable->addModal ( "noreg_pasien", "text", "No Reg", $this->noreg_pasien, "n", null, true );
            $this->uitable->addModal ( "umur_pasien", "text", "Umur", $this->umur, "n", null, true );
            $this->uitable->addModal ( "alamat", "text", "Alamat", $this->alamat, "n", null, true );
            $this->uitable->addModal ( "sebutan", "hidden", "", $this->sebutan, "n", null, true );
            $this->uitable->addModal ( "profile_number", "hidden", "", $this->profile_number, "n", null, true );
		}
		if ($this->polislug == "all"){
			$service = new RuanganService ( $this->db );
			$service->execute ();
			$ruangan = $service->getContent ();
			$this->uitable->addModal ( "ruangan", "select", "Asal Ruangan", $ruangan );
		}else{
			$this->uitable->addModal ( "ruangan", "hidden", "", $this->polislug, "n", null, true );
		}
			
		$the_row = array ();
		$the_row ['metode']     = "";
		$the_row ['keterangan'] = "";
		$the_row ['tanggal']    = "";
		$the_row ['kedatangan'] = $this->rujukan; 
		$the_row ['no_bpjs']    = $this->no_bpjs;
		$the_row ['no_telp']    = $this->no_telp;
		
		$exist ['noreg_pasien'] = $this->noreg_pasien;
		$exist ['nama_pasien']  = $this->nama_pasien;
		$exist ['nrm_pasien']   = $this->nrm_pasien;
		$exist ['ruangan']      = $this->polislug;
		if ($this->dbtable->is_exist ( $exist )) {
			$row = $this->dbtable->select ( $exist );
			$the_row = array ();
			$the_row ['metode']             = $row->metode;
            $the_row ['no_bpjs']            = $row->no_bpjs;
            $the_row ['no_telp']            = $row->no_telp;
			$the_row ['kedatangan']         = $row->kedatangan;
			$the_row ['tanggal']            = $row->tanggal;
			$the_row ["id"]                 = $row->id;
			$the_row ["hamil_ke"]           = $row->hamil_ke;
			$the_row ["umur_kehamilan"]     = $row->umur_kehamilan;
			$the_row ["letak_janin"]        = $row->letak_janin;
			$the_row ["kondisi_bayi"]       = $row->kondisi_bayi;
			$the_row ["jk_bayi"]            = $row->jk_bayi;
			$the_row ["bayi_kembar"]        = $row->bayi_kembar;
			$the_row ["ibu_meninggal"]      = $row->ibu_meninggal;
            $the_row ['keterangan']         = $row->keterangan;
            $the_row ['profile_number']     = $row->profile_number;
            $the_row ['sebutan']            = $row->sebutan;
            $the_row ['jenis_kegiatan']     = $row->jenis_kegiatan;
            $the_row ['dirujuk']            = $row->dirujuk;
		} else if(getSettings($this->db,"smis-rs-autosave-lap-persalinan","1")=="1"){
			$exist ['noreg_pasien']     = $this->noreg_pasien;
			$exist ['nrm_pasien']       = $this->nrm_pasien;
			$exist ['nama_pasien']      = $this->nama_pasien;
			$exist ['tanggal']          = $the_row ['tanggal'] ;
			$exist ['metode']           = $the_row ['metode'] ;
			$exist ['kedatangan']       = $the_row ['kedatangan'];
			$exist ['umur']             = $the_row ['umur'];
			$exist ['hamil_ke']         = $the_row ['hamil_ke'];
			$exist ['umur_kehamilan']   = $the_row ['umur_kehamilan'];
			$exist ['letak_janin']      = $the_row ['letak_janin'];
			$exist ['kondisi_bayi']     = $the_row ['kondisi_bayi'];
			$exist ['jk_bayi']          = $the_row ['jk_bayi'];
			$exist ['bayi_kembar']      = $the_row ['bayi_kembar'];
			$exist ['ibu_meninggal']    = $the_row ['ibu_meninggal'];
            $exist ['keterangan']       = $the_row ['keterangan'] ;
            $exist ['no_bpjs']          = $the_row ['no_bpjs'] ;
            $exist ['no_telp']          = $the_row ['no_telp'] ;
            $exist ['jenis_kegiatan']   = $the_row ['jenis_kegiatan'] ;
            $exist ['dirujuk']          = $the_row ['dirujuk'] ;
            $exist ['sebutan']          = $this->sebutan;
			$exist ['profile_number']   = $this->profile_number;
			$this->dbtable->insert ( $exist );
			$the_row ["id"]         = $this->dbtable->get_inserted_id ();
            $update['origin_id']    = $the_row ["id"];
            $update['autonomous']   = "[".getSettings($this->db,"smis_autonomous_id","")."]";
            $this->dbtable->update($update,array("id"=>$the_row ["id"]));
		}
		$kiriman = new OptionBuilder ();
		$kiriman->add ( "Non Rujukan", "Non Rujukan", $the_row ['kedatangan'] == "" ? "1" : "0" );
		$kiriman->add ( "Rumah Sakit Lain", "Rumah Sakit Lain", $the_row ['kedatangan'] == "Rumah Sakit Lain" ? "1" : "0" );
		$kiriman->add ( "Bidan", "Bidan", $the_row ['kedatangan'] == "Bidan" ? "1" : "0" );
		$kiriman->add ( "Puskesmas", "Puskesmas", $the_row ['kedatangan'] == "Puskesmas" ? "1" : "0" );		
		$kiriman->add ( "Faskes Lainnya", "Faskes Lainnya", $the_row ['kedatangan'] == "Faskes Lainnya" ? "1" : "0" );
		$kiriman->add ( "Non Medis", "Non Medis", $the_row ['kedatangan'] == "Non Medis" ? "1" : "0" );
		
		$metode = new OptionBuilder ();
		$metode->add ( "", "", $the_row ['metode'] == "" ? "1" : "0" );
		$metode->add ( "Spontan ( Partus )", "Partus", $the_row ['metode'] == "Spontan / Partus" ? "1" : "0" );
		$metode->add ( "Vacuum Extraction (VE)", "Vacuum Extraction", $the_row ['metode'] == "Vacuum Extraction" ? "1" : "0" );
		$metode->add ( "Sectio Caesaria (SC)", "Sectio Caesaria", $the_row ['metode'] == "Sectio Caesaria" ? "1" : "0" );
		$metode->add ( "Forcep", "Forcep", $the_row ['metode'] == "Forcep" ? "1" : "0" );
		$metode->add ( "Oksitosin", "Oksitosin", $the_row ['metode'] == "Oksitosin" ? "1" : "0" );
		$metode->add ( "Amniotomi (Pemecahan Kulit Ketuban)", "Amniotomi", $the_row ['metode'] == "Amniotomi" ? "1" : "0" );
        
        $letak_janin = new OptionBuilder();
        $letak_janin->add("", "", $the_row['letak_janin'] == "" ? "1" : "0");
        $letak_janin->add("Kep", "Kep", $the_row['letak_janin'] == "Kep" ? "1" : "0");
        $letak_janin->add("SU", "SU", $the_row['letak_janin'] == "SU" ? "1" : "0");
        $letak_janin->add("Lin", "Lin", $the_row['letak_janin'] == "Lin" ? "1" : "0");
        
        $kondisi_ibu = new OptionBuilder();
        $kondisi_ibu->add("Hidup", "Hidup", $the_row['kondisi_ibu'] == "Hidup" ? "1" : "0");
        $kondisi_ibu->add("Mati", "Mati", $the_row['kondisi_ibu'] == "Mati" ? "1" : "0");
        
        $jk_bayi = new OptionBuilder();
        $jk_bayi->add("", "", $the_row['jk_bayi'] == "" ? "1" : "0");
        $jk_bayi->add("Laki-laki", "0", $the_row['jk_bayi'] == "0" ? "1" : "0");
        $jk_bayi->add("Perempuan", "1", $the_row['jk_bayi'] == "1" ? "1" : "0");
        
        $jenis_kegiatan = new OptionBuilder();
        $jenis_kegiatan->add("", "", $the_row['jenis_kegiatan'] == "" ? "1" : "0");
        $jenis_kegiatan->add("Persalinan Normal", "1 - Persalinan Normal", $the_row['jenis_kegiatan'] == "1 - Persalinan Normal" ? "1" : "0");
        $jenis_kegiatan->add("Sectio Caesaria", "2 - Sectio Caesaria", $the_row['jenis_kegiatan'] == "2 - Sectio Caesaria" ? "1" : "0");
        $jenis_kegiatan->add("Abortus", "4 - Abortus", $the_row['jenis_kegiatan'] == "4 - Abortus" ? "1" : "0");
        $jenis_kegiatan->add("Imunisasi-TT1", "5.1 - Imunisasi-TT1", $the_row['jenis_kegiatan'] == "5.1 - Imunisasi-TT1" ? "1" : "0");
        $jenis_kegiatan->add("Imunisasi-TT2", "5.2 - Imunisasi-TT2", $the_row['jenis_kegiatan'] == "5.2 - Imunisasi-TT2" ? "1" : "0");

        $jenis_komplikasi = new OptionBuilder();
        $jenis_komplikasi->add("", "", $the_row['jenis_komplikasi'] == "" ? "1" : "0");
        $jenis_komplikasi->add("Pendarahan Sebelum Persalinan", "3.1 - Pendarahan Sebelum Persalinan", $the_row['jenis_komplikasi'] == "3.1 - Pendarahan Sebelum Persalinan" ? "1" : "0");
        $jenis_komplikasi->add("Pendarahan Sesudah Persalinan", "3.2 - Pendarahan Sesudah Persalinan", $the_row['jenis_komplikasi'] == "3.2 - Perdarahan Sesudah Persalinan" ? "1" : "0");
        $jenis_komplikasi->add("Pre Eclampsi", "3.3 - Pre Eclampsi", $the_row['jenis_komplikasi'] == "3.3 - Pre Eclampsi" ? "1" : "0");
        $jenis_komplikasi->add("Eclampsi", "3.4 - Eclampsi", $the_row['jenis_komplikasi'] == "3.4 - Eclampsi" ? "1" : "0");
        $jenis_komplikasi->add("Infeksi", "3.5 - Infeksi", $the_row['jenis_komplikasi'] == "3.5 - Infeksi" ? "1" : "0");
        $jenis_komplikasi->add("Lain-Lain", "3.6 - Lain-Lain", $the_row['jenis_komplikasi'] == "3.6 - Lain-Lain" ? "1" : "0");
		
		$this->uitable->addModal ( "id", "hidden", "", $the_row ['id'] );
		$this->uitable->addModal ( "tanggal", "date", "Tanggal", $the_row ['tanggal'] );
		$this->uitable->addModal ( "no_telp", "text", "No. Telp", $the_row ['no_telp'] );
		$this->uitable->addModal ( "no_bpjs", "text", "No. BPJS", $the_row ['no_bpjs'] );
		$this->uitable->addModal ( "hamil_ke", "text", "Hamil ke-", $the_row ['hamil_ke'] );
		$this->uitable->addModal ( "umur_kehamilan", "text", "Umur Kehamilan", $the_row ['umur_kehamilan'] );
		$this->uitable->addModal ( "kedatangan", "select", "Rujukan", $kiriman->getContent () );
		$this->uitable->addModal ( "metode", "select", "Metode", $metode->getContent () );
        $this->uitable->addModal ( "letak_janin", "select", "Letak Janin", $letak_janin->getContent () );
        $this->uitable->addModal ( "kondisi_ibu", "select", "Kondisi Ibu", $kondisi_ibu->getContent () );
        $this->uitable->addModal ( "jk_bayi", "select", "Jenis Kelamin Bayi", $jk_bayi->getContent () );
        $this->uitable->addModal ( "jenis_kegiatan", "select", "Jenis Kegiatan", $jenis_kegiatan->getContent () );
        $this->uitable->addModal ( "jenis_komplikasi", "select", "Jenis Komplikasi", $jenis_komplikasi->getContent () );
        $this->uitable->addModal ( "bayi_kembar", "checkbox", "Bayi Kembar", $the_row ['bayi_kembar'] );
        $this->uitable->addModal ( "dirujuk", "checkbox", "Dirujuk", $the_row ['dirujuk'] );
        $this->uitable->addModal ( "keterangan", "textarea", "Keterangan", $the_row ['keterangan'] );
		
		$modal = $this->uitable->getModal ();
		$modal->setTitle ( "Laporan Persalinan" );
		if ($this->page == "medical_record" && $this->action == "lap_Persalinan") {
			echo $this->uitable->getHtml ();
		} else {
			echo "<div class='hide'>" . $this->uitable->getHtml () . "</div>";
			$modal->setAlwaysShow ( true );
		}
		echo $modal->joinFooterAndForm ()->getHtml ();
	}
	public function jsLoader() {
		echo addJS ( "framework/bootstrap/js/bootstrap-datepicker.js" );
		echo addJS ( "framework/smis/js/table_action.js" );
		
		loadLibrary ( "smis-libs-function-javascript" );
	}
	public function cssLoader() {
		echo addCSS ( "framework/bootstrap/css/datepicker.css" );
	}
	public function cssPreLoad() {
		?>
			<style type="text/css">
			#<?php echo $this->action;?>_add_form_form>div {clear: both; width: 100%; }
			#<?php echo $this->action;?>_add_form_form>div label { 	width: 250px; }
			</style>
		<?php
	}
	
	/* when it's star build */
	public function jsPreLoad() {
		?>
<script type="text/javascript">
		var <?php echo $this->action; ?>;
		var lPersalinan_pasien;
		var lPersalinan_noreg="<?php echo $this->noreg_pasien; ?>";
		var lPersalinan_nama_pasien="<?php echo $this->nama_pasien; ?>";
		var lPersalinan_nrm_pasien="<?php echo $this->nrm_pasien; ?>";
		var lPersalinan_polislug="<?php echo $this->polislug; ?>";
		var lPersalinan_the_page="<?php echo $this->page; ?>";
		var lPersalinan_the_protoslug="<?php echo $this->protoslug; ?>";
		var lPersalinan_the_protoname="<?php echo $this->protoname; ?>";
		var lPersalinan_the_protoimplement="<?php echo $this->protoimplement; ?>";
		$(document).ready(function() {
			$(".mydate").datepicker();
			lPersalinan_pasien=new TableAction("lPersalinan_pasien",lPersalinan_the_page,"<?php echo $this->action; ?>",new Array());
			lPersalinan_pasien.setSuperCommand("lPersalinan_pasien");
			lPersalinan_pasien.setPrototipe(lPersalinan_the_protoname,lPersalinan_the_protoslug,lPersalinan_the_protoimplement);
			lPersalinan_pasien.selected=function(json){
				var nama=json.nama_pasien;
				var nrm=json.nrm;
				var noreg=json.id;		
				$("#<?php echo $this->action; ?>_nama_pasien").val(nama);
				$("#<?php echo $this->action; ?>_nrm_pasien").val(nrm);
				$("#<?php echo $this->action; ?>_noreg_pasien").val(noreg);
			};
			
			var column=new Array(
					"id","tanggal",
					"nama_pasien","noreg_pasien","nrm_pasien", "sebutan", "profile_number", "ruangan", "umur_pasien", 
					'kedatangan','keterangan','metode','kedatangan',
                    "alamat","no_telp","no_bpjs", "hamil_ke", "letak_janin", "kondisi_ibu", "umur_kehamilan",
                    "jk_bayi", "bayi_kembar", "jenis_kegiatan", "jenis_komplikasi", "dirujuk"
					);
			<?php echo $this->action; ?>=new TableAction("<?php echo $this->action; ?>",lPersalinan_the_page,"<?php echo $this->action; ?>",column);
			<?php echo $this->action; ?>.setPrototipe(lPersalinan_the_protoname,lPersalinan_the_protoslug,lPersalinan_the_protoimplement);
			<?php echo $this->action; ?>.getRegulerData=function(){
				var reg_data={	
						page:this.page,
						action:this.action,
						super_command:this.super_command,
						prototype_name:this.prototype_name,
						prototype_slug:this.prototype_slug,
						prototype_implement:this.prototype_implement,
						polislug:lPersalinan_polislug,
						noreg_pasien:lPersalinan_noreg,
						nama_pasien:lPersalinan_nama_pasien,
						nrm_pasien:lPersalinan_nrm_pasien
						};
				return reg_data;
			};

			$("#<?php echo $this->action; ?>_ditangani, #<?php echo $this->action; ?>_tanggal").on("change",function(){
				var s_date = $("#<?php echo $this->action; ?>_tanggal").val().replace(/-/g, "/");
				var e_date = $("#<?php echo $this->action; ?>_ditangani").val().replace(/-/g, "/");
				var timeStart = new Date(s_date).getTime();
				var timeEnd = new Date(e_date).getTime();
				var hourDiff = timeEnd - timeStart; //in ms
				var minDiff = Math.floor(hourDiff / 60 / 1000); //in minutes
				$("#<?php echo $this->action; ?>_response").val(minDiff);
			});
			
			<?php
		
		if ($this->page == "medical_record" && $this->action == "iklin") { 
			echo $this->action; ?>.view();<?php
		} else {
			echo $this->action . ".clear=function(){return;};";
		}
		
		
		?>
		});
		</script>
<?php
	}
	public function superCommand($super_command) {
		/* PASIEN */
        $array=array ('Nama','NRM',"No Reg");
		$ptable = new Table ( $array, "", NULL, true );
		$ptable->setName ( "lPersalinan_pasien" );
		$ptable->setModel ( Table::$SELECT );
		$padapter = new SimpleAdapter ();
		$padapter->add ( "Nama", "nama_pasien" );
		$padapter->add ( "NRM", "nrm", "digit8" );
		$padapter->add ( "No Reg", "id" );
		$presponder = new ServiceResponder ( $this->db, $ptable, $padapter, "get_registered" );
		$super = new SuperCommand ();
		$super->addResponder ( "lPersalinan_pasien", $presponder );
		$init = $super->initialize ();
		if ($init != null) {
			echo $init;
			return;
		}
	}
}

?>