<?php
require_once 'smis-framework/smis/template/ModulTemplate.php';
require_once 'medical_record/class/template/AsessmenPasien.php';
require_once ("smis-base/smis-include-service-consumer.php");
require_once "medical_record/class/service/RuanganService.php";
require_once 'smis-libs-hrd/EmployeeResponder.php';

class AsesmenSyaraf extends AsessmenPasien {
	protected $db;
	protected $polislug;
	protected $page;
	protected $protoslug;
	protected $protoname;
	protected $protoimplement;
	protected $action;
	protected $dbtable;
	protected $uitable;
	protected $noreg_pasien;
	protected $nama_pasien;
	protected $nrm_pasien;
	protected $alamat;
	protected $umur;
    protected $tgl_lahir;
	protected $jk;
	protected $ruang_asal;
	protected $id_antrian;
    protected $carabayar;
    protected $waktu_masuk;
    
	public function __construct($db, $polislug = "all", $noreg = "", $nrm = "", $nama = "", $alamat = "", $jk = "0", $umur = "", $tgl_lahir="", $waktu_masuk = "", $id_antrian = "0", $page = "medical_record", $action = "asesmen_syaraf",$carabayar, $protoslug = "", $protoname = "", $protoimplement = "") {
		$this->db               = $db;
		$this->noreg_pasien     = $noreg;
		$this->id_antrian       = $id_antrian;
		$this->nama_pasien      = $nama;
		$this->nrm_pasien       = $nrm;
		$this->alamat           = $alamat;
		$this->umur             = $umur;
		$this->jk               = $jk;
        $this->tgl_lahir        = $tgl_lahir;
        $this->waktu_masuk      = $waktu_masuk;
		$this->ruang_asal       = $ruang_asal;
		$this->polislug         = $polislug;
		$this->dbtable          = new DBTable ( $this->db, "smis_mr_assesmen_syaraf" );
		$this->page             = $page;
		$this->protoslug        = $protoslug;
		$this->protoimplement   = $protoimplement;
		$this->protoname        = $protoname;
		$this->action           = $action;
        $this->carabayar        = $carabayar;
		//if ($polislug != "all")
			//$this->dbtable->addCustomKriteria ( "ruangan", "='" . $polislug . "'" );
            
		if ($noreg != "")
			$this->dbtable->addCustomKriteria ( "noreg_pasien", "='" . $noreg . "'" );
		$head=array ('NRM',"Pasien","No Register" );
		$this->uitable = new Table ($head , " Asesmen Syaraf - " . ($this->polislug == "all" ? "" : ucfirst ( $this->protoname )), NULL, true );
		$this->uitable->setName ( $action );
	}
	
	public function command($command) {
        require_once "smis-base/smis-include-duplicate.php";
        $adapter = new SimpleAdapter ();
		$adapter->add ( "Pasien", "nama_pasien" );
		$adapter->add ( "NRM", "nrm_pasien", "digit8" );
		$adapter->add ( "No Register", "noreg_pasien", "digit8" );
		$dbres = new DuplicateResponder ( $this->dbtable, $this->uitable, $adapter );
		$dbres->setDuplicate(false,"");
        $dbres->setAutonomous(getSettings($this->db,"smis_autonomous_id",""));
        $data = $dbres->command ( $_POST ['command'] );
		echo json_encode ( $data );
		return;
	}
	public function phpPreLoad() {
		$service = new RuanganService ( $this->db );
		$service->execute ();
		$ruangan = $service->getContent ();
        
		/*data detail disimpan pada variable pasien*/
        $pasien=$this->getDetailPatient($this->noreg_pasien);
        $pasien['no_sep_ri'];
        /*block data header*/
		if ($this->noreg_pasien != "") {
			$this->uitable->addModal ( "nama_pasien", "hidden", "", $this->nama_pasien, "n", null, true );
			$this->uitable->addModal ( "nrm_pasien", "hidden", "", $this->nrm_pasien, "n", null, true );
			$this->uitable->addModal ( "noreg_pasien", "hidden", "", $this->noreg_pasien, "n", null, true );
			$this->uitable->addModal ( "alamat_pasien", "hidden", "", $this->alamat, "n", null, true );
			$this->uitable->addModal ( "jk", "hidden", "", $this->jk, "n", null, true );
			$this->uitable->addModal ( "umur", "hidden", "", $this->umur, "n", null, true );
            $this->uitable->addModal ( "waktu_masuk", "hidden", "", $this->waktu_masuk, "n", null, true );
            $this->uitable->addModal ( "tgl_lahir_pasien", "hidden", "", $this->tgl_lahir, "n", null, true );
		} else {
			$this->uitable->addModal ( "nama_pasien", "chooser-" . $this->action . "-asy_pasien-Pasien", "Pasien", $this->nama_pasien, "n", null, true );
			$this->uitable->addModal ( "nrm_pasien", "text", "NRM", $this->nrm_pasien, "n", null, true );
			$this->uitable->addModal ( "noreg_pasien", "text", "No Reg", $this->noreg_pasien, "n", null, true );
		}
		if ($this->polislug == "all")
			$this->uitable->addModal ( "ruangan", "select", "Asal Ruangan", $ruangan );
		else
			$this->uitable->addModal ( "ruangan", "hidden", "", $this->polislug, "n", null, true );
        /*end - block data header*/
		
		
		$the_row = array ();
		$the_row ["id"] 						    = "";
		$the_row ["nama_dokter"] 				    = "";
		$the_row ["id_dokter"] 					    = "";
		$the_row ['waktu'] 					        = date ( "Y-m-d H:i:s" );
		$the_row ["carabayar"] 				        = $this->carabayar;
        $the_row ["waktu_masuk"] 				    = $this->waktu_masuk;			
        $the_row ["mata_anamnesis"] 			    = "";
        $the_row ["mata_ikaterus"] 			        = "";
        $the_row ["mata_reflek_pupil"] 			    = "";
        $the_row ["mata_ukuran_pupil"] 			    = "";
        $the_row ["mata_iso_unisocore"] 		    = "";
        $the_row ["mata_lain"]           		    = "";
        $the_row ["tht_tonsil"]           		    = "";
        $the_row ["tht_faring"]           		    = "";
        $the_row ["tht_lidah"]           		    = "";
        $the_row ["tht_bibir"]           		    = "";
        $the_row ["leher_jvp"]           		    = "";
        $the_row ["leher_pembesaran_kelenjar"]	    = "";
        $the_row ["torak_simetris_asimetris"]	    = "";
        $the_row ["cor_s1_s2"]	                    = "";
        $the_row ["cor_reguler"]	                = "";
        $the_row ["cor_ireguler"]	                = "";
        $the_row ["cor_mur"]	                    = "";
        $the_row ["cor_besar"]	                    = "";
        $the_row ["pulmo_suara_nafas"]	            = "";
        $the_row ["pulmo_ronchi"]   	            = "";
        $the_row ["pulmo_wheezing"]   	            = "";
        $the_row ["abdomen_hepar"]   	            = "";
        $the_row ["abdomen_lien"]   	            = "";
        $the_row ["extremitas_hangat"]   	        = "";
        $the_row ["extremitas_dingin"]   	        = "";
        $the_row ["extremitas_edema"]   	        = "";
        $the_row ["kranium"]               	        = "";
        $the_row ["kranium_lain"]          	        = "";
        $the_row ["kranium_keterangan"]    	        = "";
        $the_row ["korpus_vertebra"]       	        = "";
        $the_row ["korpus_vertebra_lain"]           = "";
        $the_row ["korpus_vertebra_keterangan"]     = "";
        $the_row ["tanda_selaput_otak"]    	        = "";
        $the_row ["tanda_selaput_otak_lain"]    	= "";
        $the_row ["tanda_selaput_otak_keterangan"]  = "";
        $the_row ["syaraf_otak_kanan"]              = "";
        $the_row ["syaraf_otak_kiri"]               = "";
        $the_row ["motorik"]            	        = "";
        $the_row ["motorik_kaki_kanan"]            	= "";
        $the_row ["motorik_kaki_kiri"]            	= "";
        $the_row ["refleks"]            	        = "";
        $the_row ["refleks_kaki_kanan"]   	        = "";
        $the_row ["refleks_kaki_kiri"]     	        = "";
        $the_row ["sensorik"]            	        = "";
        $the_row ["sensorik_kaki_kanan"]            = "";
        $the_row ["sensorik_kaki_kiri"]            	= "";
        $the_row ["vegentatif"]                 	= "";
        $the_row ["luhur_kesadaran"]              	= "";
        $the_row ["luhur_reaksi_emosi"]           	= "";
        $the_row ["luhur_intelek"]                 	= "";
        $the_row ["luhur_proses_berpikir"]         	= "";
        $the_row ["luhur_psikomotorik"]            	= "";
        $the_row ["luhur_psikosensorik"]           	= "";
        $the_row ["luhur_bicara_bahasa"]           	= "";
        $the_row ["mental_refleks"]                	= "";
        $the_row ["mental_memegang"]               	= "";
        $the_row ["mental_menetek"]                	= "";
        $the_row ["mental_snout_refleks"]          	= "";
        $the_row ["mental_glabola"]                	= "";
        $the_row ["mental_palmomental"]            	= "";
        $the_row ["mental_korneomandibular"]       	= "";
        $the_row ["mental_kaki_tolik"]            	= "";
        $the_row ["mental_lain"]                	= "";
        $the_row ["nyeri_tekan_syaraf"]            	= "";
        $the_row ["tanda_laseque"]              	= "";
        $the_row ["lain_lain"]                    	= "";
        $the_row ["diagnosa_kerja_icd"]             = "";
        $the_row ["diagnosa_kerja"]                 = "";
        $the_row ["diagnosa_kerja_sebab"]           = "";
        $the_row ["diagnosa_kerja_ket"]             = "";
        $the_row ["terapi_tindakan_icd"]            = "";
        $the_row ["terapi_tindakan"]                = "";
        $the_row ["terapi_tindakan_sebab"]          = "";
        $the_row ["terapi_tindakan_ket"]            = "";
        $the_row ["penunjang_neurovaskuler"]        = "";
        $the_row ["penunjang_neuroimaging"]         = "";
        $the_row ["penunjang_elektrodiagnostik"]    = "";
        $the_row ["penunjang_lab"]                  = "";
        $the_row ["penunjang_lain"]                 = "";
        $the_row ["waktu_pulang"]                   = "";
        $the_row ["tgl_kontrol_klinik"]             = "";
        $the_row ["dirawat_diruang"]                = "";
        
		$exist ['noreg_pasien'] = $this->noreg_pasien;
		$exist ['nama_pasien'] = $this->nama_pasien;
		$exist ['nrm_pasien'] = $this->nrm_pasien;
		//$exist ['ruangan'] = $this->polislug;
		if ($this->dbtable->is_exist ( $exist )) {
			$row = $this->dbtable->select ( $exist );
            $the_row ["id"] 					        = $row->id;
            $the_row ["nama_dokter"] 			        = $row->nama_dokter;
            $the_row ["id_dokter"] 				        = $row->id_dokter;
            $the_row ["waktu"] 				            = $row->waktu;
            $the_row ["waktu_masuk"] 				    = $row->waktu_masuk;
			$the_row ['mata_anamnesis'] 		        = $row->mata_anamnesis;
            $the_row ["mata_ikaterus"] 			        = $row->mata_ikaterus;
            $the_row ["mata_reflek_pupil"] 		        = $row->mata_reflek_pupil;
            $the_row ["mata_ukuran_pupil"] 		        = $row->mata_ukuran_pupil;
            $the_row ["mata_iso_unisocore"] 	        = $row->mata_iso_unisocore;
            $the_row ["mata_lain"] 	                    = $row->mata_lain;
            $the_row ["tht_tonsil"] 	                = $row->tht_tonsil;
            $the_row ["tht_faring"] 	                = $row->tht_faring;
            $the_row ["tht_lidah"] 	                    = $row->tht_faring;
            $the_row ["tht_bibir"] 	                    = $row->tht_bibir;
            $the_row ["leher_jvp"] 	                    = $row->leher_jvp;
            $the_row ["leher_pembesaran_kelenjar"]      = $row->leher_pembesaran_kelenjar;
            $the_row ["torak_simetris_asimetris"]       = $row->torak_simetris_asimetris;
            $the_row ["cor_s1_s2"]                      = $row->cor_s1_s2;
            $the_row ["cor_reguler"]                    = $row->cor_reguler;
            $the_row ["cor_ireguler"]                   = $row->cor_ireguler;
            $the_row ["cor_mur"]                        = $row->cor_mur;
            $the_row ["cor_besar"]                      = $row->cor_besar;
            $the_row ["pulmo_suara_nafas"]              = $row->pulmo_suara_nafas;
            $the_row ["pulmo_ronchi"]                   = $row->pulmo_ronchi;
            $the_row ["pulmo_wheezing"]                 = $row->pulmo_wheezing;
            $the_row ["abdomen_hepar"]                  = $row->abdomen_hepar;
            $the_row ["abdomen_lien"]                   = $row->abdomen_lien;
            $the_row ["extremitas_hangat"]              = $row->extremitas_hangat;
            $the_row ["extremitas_dingin"]              = $row->extremitas_dingin;
            $the_row ["extremitas_edema"]               = $row->extremitas_edema;
            $the_row ["kranium"]                        = $row->kranium;
            $the_row ["kranium_lain"]                   = $row->kranium_lain;
            $the_row ["kranium_keterangan"]             = $row->kranium_keterangan;
            $the_row ["korpus_vertebra"]                = $row->korpus_vertebra;
            $the_row ["korpus_vertebra_lain"]           = $row->korpus_vertebra_lain;
            $the_row ["korpus_vertebra_keterangan"]     = $row->korpus_vertebra_keterangan;
            $the_row ["tanda_selaput_otak"]             = $row->tanda_selaput_otak;
            $the_row ["tanda_selaput_otak_lain"]        = $row->tanda_selaput_otak_lain;
            $the_row ["tanda_selaput_otak_keterangan"]  = $row->tanda_selaput_otak_keterangan;
            $the_row ["syaraf_otak_kanan"]              = $row->syaraf_otak_kanan;
            $the_row ["syaraf_otak_kiri"]               = $row->syaraf_otak_kiri;
            $the_row ["motorik"]                        = $row->motorik;
            $the_row ["motorik_kaki_kanan"]             = $row->motorik_kaki_kanan;
            $the_row ["motorik_kaki_kiri"]              = $row->motorik_kaki_kiri;
            $the_row ["refleks"]                        = $row->refleks;
            $the_row ["refleks_kaki_kanan"]             = $row->refleks_kaki_kanan;
            $the_row ["refleks_kaki_kiri"]              = $row->refleks_kaki_kiri;
            $the_row ["sensorik"]                       = $row->sensorik;
            $the_row ["sensorik_kaki_kanan"]            = $row->sensorik_kaki_kanan;
            $the_row ["sensorik_kaki_kiri"]             = $row->sensorik_kaki_kiri;
            $the_row ["vegentatif"]                     = $row->vegentatif;
            $the_row ["luhur_kesadaran"]                = $row->luhur_kesadaran;
            $the_row ["luhur_reaksi_emosi"]             = $row->luhur_reaksi_emosi;
            $the_row ["luhur_intelek"]                  = $row->luhur_intelek;
            $the_row ["luhur_proses_berpikir"]          = $row->luhur_proses_berpikir;
            $the_row ["luhur_psikomotorik"]             = $row->luhur_psikomotorik;
            $the_row ["luhur_psikosensorik"]            = $row->luhur_psikosensorik;
            $the_row ["luhur_bicara_bahasa"]            = $row->luhur_bicara_bahasa;
            $the_row ["mental_refleks"]                 = $row->mental_refleks;
            $the_row ["mental_memegang"]                = $row->mental_memegang;
            $the_row ["mental_menetek"]                 = $row->mental_menetek;
            $the_row ["mental_snout_refleks"]           = $row->mental_snout_refleks;
            $the_row ["mental_glabola"]                 = $row->mental_glabola;
            $the_row ["mental_palmomental"]             = $row->mental_palmomental;
            $the_row ["mental_korneomandibular"]        = $row->mental_korneomandibular;
            $the_row ["mental_kaki_tolik"]              = $row->mental_kaki_tolik;
            $the_row ["mental_lain"]                    = $row->mental_lain;
            $the_row ["nyeri_tekan_syaraf"]             = $row->nyeri_tekan_syaraf;
            $the_row ["tanda_laseque"]                  = $row->tanda_laseque;
            $the_row ["lain_lain"]                      = $row->lain_lain;
            $the_row ["diagnosa_kerja_icd"]             = $row->diagnosa_kerja_icd;
            $the_row ["diagnosa_kerja"]                 = $row->diagnosa_kerja;
            $the_row ["diagnosa_kerja_sebab"]           = $row->diagnosa_kerja_sebab;
            $the_row ["diagnosa_kerja_ket"]             = $row->diagnosa_kerja_ket;
            $the_row ["terapi_tindakan_icd"]            = $row->terapi_tindakan_icd;
            $the_row ["terapi_tindakan"]                = $row->terapi_tindakan;
            $the_row ["terapi_tindakan_sebab"]          = $row->terapi_tindakan_sebab;
            $the_row ["terapi_tindakan_ket"]            = $row->terapi_tindakan_ket;
            $the_row ["penunjang_neurovaskuler"]        = $row->penunjang_neurovaskuler;
            $the_row ["penunjang_neuroimaging"]         = $row->penunjang_neuroimaging;
            $the_row ["penunjang_elektrodiagnostik"]    = $row->penunjang_elektrodiagnostik;
            $the_row ["penunjang_lab"]                  = $row->penunjang_lab;
            $the_row ["penunjang_lain"]                 = $row->penunjang_lain;
            $the_row ["waktu_pulang"]                   = $row->waktu_pulang;
            $the_row ["tgl_kontrol_klinik"]             = $row->tgl_kontrol_klinik;
            $the_row ["dirawat_diruang"]                = $row->dirawat_diruang;
		} else {
			$exist ['umur'] = $this->umur;
			$exist ['jk'] = $this->jk;
			$exist ['alamat_pasien'] = $this->alamat;
			$exist ['noreg_pasien'] = $this->noreg_pasien;
			$exist ['nrm_pasien'] = $this->nrm_pasien;
			$exist ['nama_pasien'] = $this->nama_pasien;
            $exist ['ruangan'] = $this->polislug;
            $exist ['tgl_lahir_pasien'] = $this->tgl_lahir;
			$exist ['waktu'] = date ( "Y-m-d H:i:s" );
            $exist ['waktu_masuk'] = $this->waktu_masuk;
            $this->dbtable->insert ( $exist );
			
            $the_row ["id"] = $this->dbtable->get_inserted_id ();
            
            $update['origin_id']=$the_row ["id"];
            $update['autonomous']="[".getSettings($this->db,"smis_autonomous_id","")."]";
            $this->dbtable->update($update,array("id"=>$the_row ["id"]));
		}
        
        $this->uitable->addModal ( "id", "hidden", "", $the_row ['id'] );
		$this->uitable->addModal ( "nama_dokter", "chooser-asy_pasien-dokter_asy-Dokter Pemeriksa", "Dokter Pemeriksa", $the_row ['nama_dokter'],"y",null,false,null,true);
		$this->uitable->addModal ( "id_dokter", "hidden", "", $the_row ['id_dokter'],"y",null,false,null,false);
		$this->uitable->addModal ( "waktu", "datetime", "Waktu", $the_row ['waktu'] ,"y",null,false,null,false);
		$this->uitable->addModal ( "carabayar", "text", "Carabayar", $the_row ['carabayar'] ,"n",null,true,null,false);
		
        $this->uitable->addModal("", "label", "<strong>Mata</strong>", "");
        $this->uitable->addModal ( "mata_anamnesis", "text", "Anamnesis", $the_row ['mata_anamnesis'] ,"y",null,false,null,false);
        $this->uitable->addModal ( "mata_ikaterus", "text", "Iketerus", $the_row ['mata_ikaterus'] ,"y",null,false,null,false);
		$this->uitable->addModal ( "mata_reflek_pupil", "text", "Reflek Pupil", $the_row ['mata_reflek_pupil'] ,"y",null,false,null,false);
		$this->uitable->addModal ( "mata_ukuran_pupil", "text", "Ukuran Pupil", $the_row ['mata_ukuran_pupil'] ,"y",null,false,null,false);
		$this->uitable->addModal ( "mata_iso_unisocore", "text", "Iso Unisocore", $the_row ['mata_iso_unisocore'] ,"y",null,false,null,false);
		$this->uitable->addModal ( "mata_lain", "text", "Lain-Lain", $the_row ['mata_lain'] ,"y",null,false,null,false);
        
        $this->uitable->addModal("", "label", "<strong>THT</strong>", "");
		$this->uitable->addModal ( "tht_tonsil", "text", "Tonsil", $the_row ['tht_tonsil'] ,"y",null,false,null,false);
		$this->uitable->addModal ( "tht_faring", "text", "Faring", $the_row ['tht_faring'] ,"y",null,false,null,false);
		$this->uitable->addModal ( "tht_lidah", "text", "Lidah", $the_row ['tht_lidah'] ,"y",null,false,null,false);
		$this->uitable->addModal ( "tht_bibir", "text", "Bibir", $the_row ['tht_bibir'] ,"y",null,false,null,false);
        
        $this->uitable->addModal("", "label", "<strong>Leher</strong>", "");
		$this->uitable->addModal ( "leher_jvp", "text", "JVP", $the_row ['leher_jvp'] ,"y",null,false,null,false);
		$this->uitable->addModal ( "leher_pembesaran_kelenjar", "text", "Pembesaran Kelenjar", $the_row ['leher_pembesaran_kelenjar'] ,"y",null,false,null,false);
        
        $this->uitable->addModal("", "label", "<strong>Torak</strong>", "");
        $this->uitable->addModal ( "torak_simetris_asimetris", "text", "Simetris / Asimetris", $the_row ['torak_simetris_asimetris'] ,"y",null,false,null,false);
		
        $this->uitable->addModal("", "label", "<strong>Cor</strong>", "");
        $this->uitable->addModal ( "cor_s1_s2", "text", "S1, S2", $the_row ['cor_s1_s2'] ,"y",null,false,null,false);
        $this->uitable->addModal ( "cor_reguler", "text", "Reguler", $the_row ['cor_reguler'] ,"y",null,false,null,false);
        $this->uitable->addModal ( "cor_ireguler", "text", "Ireguler", $the_row ['cor_ireguler'] ,"y",null,false,null,false);
        $this->uitable->addModal ( "cor_mur", "text", "Mur-Mur", $the_row ['cor_mur'] ,"y",null,false,null,false);
        $this->uitable->addModal ( "cor_besar", "text", "Besar", $the_row ['cor_besar'] ,"y",null,false,null,false);
        
        $this->uitable->addModal("", "label", "<strong>Pulmo</strong>", "");
        $this->uitable->addModal ( "pulmo_suara_nafas", "text", "Suara Nafas", $the_row ['pulmo_suara_nafas'] ,"y",null,false,null,false);
        $this->uitable->addModal ( "pulmo_ronchi", "text", "Ronchi", $the_row ['pulmo_ronchi'] ,"y",null,false,null,false);
        $this->uitable->addModal ( "pulmo_wheezing", "text", "Wheezing", $the_row ['pulmo_wheezing'] ,"y",null,false,null,false);
        
        $this->uitable->addModal("", "label", "<strong>Abdomen</strong>", "");
        $this->uitable->addModal ( "abdomen_hepar", "text", "Hepar", $the_row ['abdomen_hepar'] ,"y",null,false,null,false);
        $this->uitable->addModal ( "abdomen_lien", "text", "Lien", $the_row ['abdomen_lien'] ,"y",null,false,null,false);
        
        $this->uitable->addModal("", "label", "<strong>Extremitas</strong>", "");
        $this->uitable->addModal ( "extremitas_hangat", "text", "Hangat", $the_row ['extremitas_hangat'] ,"y",null,false,null,false);
        $this->uitable->addModal ( "extremitas_dingin", "text", "Dingin", $the_row ['extremitas_dingin'] ,"y",null,false,null,false);
        $this->uitable->addModal ( "extremitas_edema", "text", "Edema", $the_row ['extremitas_edema'] ,"y",null,false,null,false);
        
        $kranium=array(
				array("name"=>"","value"=>"","default"=> $the_row ["kranium"]==""?1:0),
				array("name"=>"Inspeksi","value"=>"Inspeksi","default"=> $the_row ["kranium"]=="Inspeksi"?1:0),
				array("name"=>"Palpasi","value"=>"Palpasi","default"=> $the_row ["kranium"]=="Palpasi"?1:0),
				array("name"=>"Perkusi","value"=>"Perkusi","default"=> $the_row ["kranium"]=="Perkusi"?1:0),
				array("name"=>"Auskultasi","value"=>"Auskultasi","default"=> $the_row ["kranium"]=="Auskultasi"?1:0),
				array("name"=>"Transibuminasi","value"=>"Transibuminasi","default"=> $the_row ["kranium"]=="Transibuminasi"?1:0),
				array("name"=>"Lain-Lain","value"=>"Lain-Lain","default"=> $the_row ["kranium"]=="Lain-Lain"?1:0)
		);
        $this->uitable->addModal("kranium", "select", "<strong>Kranium</strong>", $kranium,"y",NULL,false,NULL,false);
        $this->uitable->addModal ( "kranium_lain", "text", "Lain-Lain", $the_row ['kranium_lain'] ,"y",null,false,null,false);
        $this->uitable->addModal ( "kranium_keterangan", "text", "Keterangan", $the_row ['kranium_keterangan'] ,"y",null,false,null,false);
        
        $korpus_vertebra=array(
				array("name"=>"","value"=>"","default"=> $the_row ["korpus_vertebra"]==""?1:0),
				array("name"=>"Inspeksi","value"=>"Inspeksi","default"=> $the_row ["korpus_vertebra"]=="Inspeksi"?1:0),
				array("name"=>"Palpasi","value"=>"Palpasi","default"=> $the_row ["korpus_vertebra"]=="Palpasi"?1:0),
				array("name"=>"Perkusi","value"=>"Perkusi","default"=> $the_row ["korpus_vertebra"]=="Perkusi"?1:0),
				array("name"=>"Auskultasi","value"=>"Auskultasi","default"=> $the_row ["korpus_vertebra"]=="Auskultasi"?1:0),
				array("name"=>"Transibuminasi","value"=>"Transibuminasi","default"=> $the_row ["korpus_vertebra"]=="Transibuminasi"?1:0),
				array("name"=>"Lain-Lain","value"=>"Lain-Lain","default"=> $the_row ["korpus_vertebra"]=="Lain-Lain"?1:0)
		);
        $this->uitable->addModal("korpus_vertebra", "select", "<strong>Korpus Vertebra</strong>", $korpus_vertebra,"y",NULL,false,NULL,false);
        $this->uitable->addModal ( "korpus_vertebra_lain", "text", "Lain-Lain", $the_row ['korpus_vertebra_lain'] ,"y",null,false,null,false);
        $this->uitable->addModal ( "korpus_vertebra_keterangan", "text", "Keterangan", $the_row ['korpus_vertebra_keterangan'] ,"y",null,false,null,false);
        
        $selaput_otak=array(
            array("name"=>"","value"=>"","default"=> $the_row ["tanda_selaput_otak"]==""?1:0),
            array("name"=>"Kaku Kuduk","value"=>"Kaku Kuduk","default"=> $the_row ["tanda_selaput_otak"]=="Kaku Kuduk"?1:0),
            array("name"=>"Kernig's Sign","value"=>"Kernig's Sign","default"=> $the_row ["tanda_selaput_otak"]=="Kernig's Sign"?1:0),
            array("name"=>"Brudzinski's Neck Sign","value"=>"Brudzinski's Neck Sign","default"=> $the_row ["tanda_selaput_otak"]=="Brudzinski's Neck Sign"?1:0),
            array("name"=>"Brudzinski's Leg Sign","value"=>"Brudzinski's Leg Sign","default"=> $the_row ["tanda_selaput_otak"]=="Brudzinski's Leg Sign"?1:0),
            array("name"=>"Lain-Lain","value"=>"Lain-Lain","default"=> $the_row ["tanda_selaput_otak"]=="Lain-Lain"?1:0)
        );
        $this->uitable->addModal("tanda_selaput_otak", "select", "<strong>Tanda Perangsangan Selaput Otak</strong>", $selaput_otak,"y",NULL,false,NULL,false);
        $this->uitable->addModal ( "tanda_selaput_otak_lain", "text", "Lain-Lain", $the_row ['tanda_selaput_otak_lain'] ,"y",null,false,null,false);
        $this->uitable->addModal ( "tanda_selaput_otak_keterangan", "text", "Keterangan", $the_row ['tanda_selaput_otak_keterangan'] ,"y",null,false,null,false);
        
        $this->uitable->addModal("", "label", "<strong>Syaraf Otak</strong>", "");
        $this->uitable->addModal ( "syaraf_otak_kanan", "text", "Kanan", $the_row ['syaraf_otak_kanan'] ,"y",null,false,null,false);
        $this->uitable->addModal ( "syaraf_otak_kiri", "text", "Kiri", $the_row ['syaraf_otak_kiri'] ,"y",null,false,null,false);
        
        $motorik=array(
            array("name"=>"","value"=>"","default"=> $the_row ["motorik"]==""?1:0),
            array("name"=>"Tenaga","value"=>"Tenaga","default"=> $the_row ["motorik"]=="Tenaga"?1:0),
            array("name"=>"Tonus","value"=>"Tonus","default"=> $the_row ["motorik"]=="Tonus"?1:0),
            array("name"=>"Kordinasi","value"=>"Kordinasi","default"=> $the_row ["motorik"]=="Kordinasi"?1:0),
            array("name"=>"Gerakan Involunter","value"=>"Gerakan Involunter","default"=> $the_row ["motorik"]=="Gerakan Involunter"?1:0),
            array("name"=>"Langkah dan Gaya Jalan","value"=>"Langkah dan Gaya Jalan","default"=> $the_row ["motorik"]=="Langkah dan Gaya Jalan"?1:0)
        );
        $this->uitable->addModal("motorik", "select", "<strong>Motorik</strong>", $motorik,"y",NULL,false,NULL,false);
        $this->uitable->addModal ( "motorik_kaki_kanan", "text", "Kaki Kanan", $the_row ['motorik_kaki_kanan'] ,"y",null,false,null,false);
        $this->uitable->addModal ( "motorik_kaki_kiri", "text", "Kaki Kiri", $the_row ['motorik_kaki_kiri'] ,"y",null,false,null,false);
        
        $refleks=array(
            array("name"=>"","value"=>"","default"=> $the_row ["refleks"]==""?1:0),
            array("name"=>"Fisiologik","value"=>"Fisiologik","default"=> $the_row ["refleks"]=="Fisiologik"?1:0),
            array("name"=>"Patologik","value"=>"Patologik","default"=> $the_row ["refleks"]=="Patologik"?1:0)
        );
        $this->uitable->addModal("refleks", "select", "<strong>Refleks</strong>", $refleks,"y",NULL,false,NULL,false);
        $this->uitable->addModal ( "refleks_kaki_kanan", "text", "Kaki Kanan", $the_row ['refleks_kaki_kanan'] ,"y",null,false,null,false);
        $this->uitable->addModal ( "refleks_kaki_kiri", "text", "Kaki Kiri", $the_row ['refleks_kaki_kiri'] ,"y",null,false,null,false);
        
        $sensorik=array(
            array("name"=>"","value"=>"","default"=> $the_row ["sensorik"]==""?1:0),
            array("name"=>"Permukaan","value"=>"Permukaan","default"=> $the_row ["sensorik"]=="Permukaan"?1:0),
            array("name"=>"Dalam","value"=>"Dalam","default"=> $the_row ["sensorik"]=="Dalam"?1:0)
        );
        $this->uitable->addModal("sensorik", "select", "<strong>Sensorik</strong>", $sensorik,"y",NULL,false,NULL,false);
        $this->uitable->addModal ( "sensorik_kaki_kanan", "text", "Kaki Kanan", $the_row ['sensorik_kaki_kanan'] ,"y",null,false,null,false);
        $this->uitable->addModal ( "sensorik_kaki_kiri", "text", "Kaki Kiri", $the_row ['sensorik_kaki_kiri'] ,"y",null,false,null,false);
        
        $this->uitable->addModal ( "vegentatif", "textarea", "<strong>Vegentatif</strong>", $the_row ['vegentatif'] ,"y",null,false,null,false);
        
        $this->uitable->addModal("", "label", "<strong>Fungsi Luhur</strong>", "");
        $this->uitable->addModal ( "luhur_kesadaran", "text", "Kesadaran", $the_row ['luhur_kesadaran'] ,"y",null,false,null,false);
        $this->uitable->addModal ( "luhur_reaksi_emosi", "text", "Reaksi Emosi", $the_row ['luhur_reaksi_emosi'] ,"y",null,false,null,false);
        $this->uitable->addModal ( "luhur_intelek", "text", "Fungsi Intelek", $the_row ['luhur_intelek'] ,"y",null,false,null,false);
        $this->uitable->addModal ( "luhur_proses_berpikir", "text", "Proses Berpikir", $the_row ['luhur_proses_berpikir'] ,"y",null,false,null,false);
        $this->uitable->addModal ( "luhur_psikomotorik", "text", "Fungsi Psikomotorik", $the_row ['luhur_psikomotorik'] ,"y",null,false,null,false);
        $this->uitable->addModal ( "luhur_psikosensorik", "text", "Fungsi Psikosensorik", $the_row ['luhur_psikosensorik'] ,"y",null,false,null,false);
        $this->uitable->addModal ( "luhur_bicara_bahasa", "text", "Fungsi Bicara dan Bahasa", $the_row ['luhur_bicara_bahasa'] ,"y",null,false,null,false);
        
        $this->uitable->addModal("", "label", "<strong>Tanda Kemunduran Mental</strong>", "");
        $this->uitable->addModal ( "mental_refleks", "text", "Refleks", $the_row ['mental_refleks'] ,"y",null,false,null,false);
        $this->uitable->addModal ( "mental_memegang", "text", "Memegang", $the_row ['mental_memegang'] ,"y",null,false,null,false);
        $this->uitable->addModal ( "mental_menetek", "text", "Menetek", $the_row ['mental_menetek'] ,"y",null,false,null,false);
        $this->uitable->addModal ( "mental_snout_refleks", "text", "Snout Refleks", $the_row ['mental_snout_refleks'] ,"y",null,false,null,false);
        $this->uitable->addModal ( "mental_glabola", "text", "Refleks Glabola", $the_row ['mental_glabola'] ,"y",null,false,null,false);
        $this->uitable->addModal ( "mental_palmomental", "text", "Refleks Palmomental", $the_row ['mental_palmomental'] ,"y",null,false,null,false);
        $this->uitable->addModal ( "mental_korneomandibular", "text", "Refleks Korneomandibular", $the_row ['mental_korneomandibular'] ,"y",null,false,null,false);
        $this->uitable->addModal ( "mental_kaki_tolik", "text", "Refleks Kaki Tolik", $the_row ['mental_kaki_tolik'] ,"y",null,false,null,false);
        $this->uitable->addModal ( "mental_lain", "text", "Lain-Lain", $the_row ['mental_lain'] ,"y",null,false,null,false);
        
        $this->uitable->addModal ( "nyeri_tekan_syaraf", "textarea", "<strong>Nyeri Tekan Syaraf</strong>", $the_row ['nyeri_tekan_syaraf'] ,"y",null,false,null,false);
        
        $this->uitable->addModal ( "tanda_laseque", "textarea", "<strong>Tanda Laseque</strong>", $the_row ['tanda_laseque'] ,"y",null,false,null,false);
        
        $this->uitable->addModal ( "lain_lain", "textarea", "<strong>Lain-Lain</strong>", $the_row ['lain_lain'] ,"y",null,false,null,false);
        
        $this->uitable->addModal ( "diagnosa_kerja", "chooser-".$this->action."-diagnosa_asy-ICD Diagnosa", "<strong>Diagnosa Kerja</strong>", $the_row ['diagnosa_kerja'] ,"y",null,false,null,false);
        $this->uitable->addModal ( "diagnosa_kerja_icd", "text", "Kode ICD Diagnosa", $the_row ['diagnosa_kerja_icd'] ,"y",null,false,null,false);
        $this->uitable->addModal ( "diagnosa_kerja_sebab", "hidden", "", $the_row ['diagnosa_kerja_sebab'] ,"y",null,false,null,false);
        $this->uitable->addModal ( "diagnosa_kerja_ket", "text", "Keterangan", $the_row ['diagnosa_kerja_ket'] ,"y",null,false,null,false);
        
        $this->uitable->addModal ( "terapi_tindakan", "chooser-".$this->action."-terapi_asy-ICD Terapi", "<strong>Terapi Tindakan</strong>", $the_row ['terapi_tindakan'] ,"y",null,false,null,false);
        $this->uitable->addModal ( "terapi_tindakan_icd", "text", "Kode ICD Terapi Tindakan", $the_row ['terapi_tindakan_icd'] ,"y",null,false,null,false);
        $this->uitable->addModal ( "terapi_tindakan_sebab", "hidden", "", $the_row ['terapi_tindakan_sebab'] ,"y",null,false,null,false);
        $this->uitable->addModal ( "terapi_tindakan_ket", "text", "Keterangan", $the_row ['terapi_tindakan_ket'] ,"y",null,false,null,false);
        
        $this->uitable->addModal("", "label", "<strong>Pemeriksaan Penunjang</strong>", "");
        $this->uitable->addModal ( "penunjang_neurovaskuler", "text", "Neurovaskuler", $the_row ['penunjang_neurovaskuler'] ,"y",null,false,null,false);
        $this->uitable->addModal ( "penunjang_neuroimaging", "text", "Neuroimaging", $the_row ['penunjang_neuroimaging'] ,"y",null,false,null,false);
        $this->uitable->addModal ( "penunjang_elektrodiagnostik", "text", "Elektrodiagnostik", $the_row ['penunjang_elektrodiagnostik'] ,"y",null,false,null,false);
        $this->uitable->addModal ( "penunjang_lab", "text", "Laboratorium", $the_row ['penunjang_lab'] ,"y",null,false,null,false);
        $this->uitable->addModal ( "penunjang_lain", "text", "Lain-Lain", $the_row ['penunjang_lain'] ,"y",null,false,null,false);
        
        $this->uitable->addModal("", "label", "<strong>DISPOSISI</strong>", "");
        $this->uitable->addModal ( "waktu_pulang", "datetime", "Waktu Pulang", $the_row ['waktu_pulang'] ,"y",null,false,null,false);
        $this->uitable->addModal ( "tgl_kontrol_klinik", "date", "Tanggal Kontrol Klinik", $the_row ['tgl_kontrol_klinik'] ,"y",null,false,null,false);
        $dirawat=array(
				array("name"=>"","value"=>"","default"=> $the_row ["dirawat_diruang"]==""?1:0),
				array("name"=>"Intensif","value"=>"Intensif","default"=> $the_row ["dirawat_diruang"]=="Intensif"?1:0),
				array("name"=>"Ruang Lain","value"=>"Ruang Lain","default"=> $the_row ["dirawat_diruang"]=="Ruang Lain"?1:0),
				array("name"=>"Tidak Dirawat Diruang","value"=>"Tidak Dirawat Diruang","default"=> $the_row ["dirawat_diruang"]=="Tidak Dirawat Diruang"?1:0)
		);
        $this->uitable->addModal("dirawat_diruang", "select", "<strong>Dirawat Diruang</strong>", $dirawat,"y",NULL,false,NULL,false);
        
        
		$modal = $this->uitable->getModal ();
		$modal->setTitle ( "Assessment Syaraf" );
		if ($this->page == "medical_record") {
			echo $this->uitable->getHtml ();
		} else {
			echo "<div class='hide'>" . $this->uitable->getHtml () . "</div>";
			$modal->setAlwaysShow ( true );
		}
		echo $modal->joinFooterAndForm ()->getHtml ();
        
        $hidden_noreg=new Hidden("asy_noreg_pasien","",$this->noreg_pasien);
        $hidden_nama=new Hidden("asy_nama_pasien","",$this->nama_pasien);
        $hidden_nrm=new Hidden("asy_nrm_pasien","",$this->nrm_pasien);
        $hidden_polislug=new Hidden("asy_polislug","",$this->polislug);
        $hidden_the_page=new Hidden("asy_the_page","",$this->page);
        $hidden_the_protoslug=new Hidden("asy_the_protoslug","",$this->protoslug);
        $hidden_the_protoname=new Hidden("asy_the_protoname","",$this->protoname);
        $hidden_the_protoimpl=new Hidden("asy_the_protoimplement","",$this->protoimplement);
        $hidden_antrian=new Hidden("asy_id_antrian","",$this->id_antrian);
        $hidden_action=new Hidden("asy_action","",$this->action);
        
       echo $hidden_noreg->getHtml();
       echo $hidden_nama->getHtml();
       echo $hidden_nrm->getHtml();
       echo $hidden_polislug->getHtml();
       echo $hidden_the_page->getHtml();
       echo $hidden_the_protoslug->getHtml();
       echo $hidden_the_protoname->getHtml();
       echo $hidden_the_protoimpl->getHtml();
       echo $hidden_antrian->getHtml();
       echo $hidden_action->getHtml();
       
       echo addJS("medical_record/resource/js/asesmen_syaraf.js",false);
       echo addCSS ( "framework/bootstrap/css/datepicker.css" );
       echo addJS ( "framework/bootstrap/js/bootstrap-datetimepicker.js" );
       echo addCSS ( "framework/bootstrap/css/bootstrap-datetimepicker.css" );
	}
   
	public function cssPreLoad() {
		?>
		<style type="text/css">
				#<?php echo $this->action;?>_add_form_form>div {clear: both; width: 100%;}
				#<?php echo $this->action; ?>_add_form_form>div label { width: 250px; }
		</style>
		<?php
	}
	
	public function superCommand($super_command) {
		/* PASIEN */
		$super = new SuperCommand ();
		
        if($super_command=="asy_pasien"){
            $phead=array ('Nama','NRM',"No Reg" );
            $ptable = new Table ( $phead, "", NULL, true );
            $ptable->setName ( "asy_pasien" );
            $ptable->setModel ( Table::$SELECT );
            $padapter = new SimpleAdapter ();
            $padapter->add ( "Nama", "nama_pasien" );
            $padapter->add ( "NRM", "nrm", "digit8" );
            $padapter->add ( "No Reg", "id" );
            $presponder = new ServiceResponder ( $this->db, $ptable, $padapter, "get_registered" );		
            $super->addResponder ( "asy_pasien", $presponder );            
        }
		
        /* DOKTER */
        if($super_command=="dokter_asy"){
            $eadapt = new SimpleAdapter ();
            $eadapt->add ( "Jabatan", "nama_jabatan" );
            $eadapt->add ( "Nama", "nama" );
            $eadapt->add ( "NIP", "nip" );
            $head_karyawan=array ('Nama','Jabatan',"NIP");
            $dktable = new Table ( $head_karyawan, "", NULL, true );
            $dktable->setName ( "dokter_asy" );
            $dktable->setModel ( Table::$SELECT );
            $employee = new EmployeeResponder ( $this->db, $dktable, $eadapt, "dokter" );
            $super->addResponder ( "dokter_asy", $employee );
		}
        
        /* DIAGNOSA */
        if($super_command=="diagnosa_asy"){
            $array=array ("Kode",'DTD','Nama',"Sebab" );
            $muitable = new Table ( $array);
            $muitable->setModel ( Table::$SELECT );
            $muitable->setName ( "diagnosa_asy" );
            $madapter = new SimpleAdapter ();
            $madapter->add ( "Nama", "nama" );
            $madapter->add ( "Kode", "icd" );
            $madapter->add ( "DTD", "dtd" );
            $madapter->add ( "Sebab", "sebab" );
            $dbtable = new DBTable ( $this->db, "smis_mr_icd" );
            $mr_mresponder = new DBResponder ( $dbtable, $muitable, $madapter );
            $super->addResponder ( "diagnosa_asy", $mr_mresponder );
		}
        
        /* TERAPI TINDAKAN */
        if($super_command=="terapi_asy"){
            $array=array ("Kode",'DTD','Nama',"Sebab" );
            $muitable = new Table ( $array);
            $muitable->setModel ( Table::$SELECT );
            $muitable->setName ( "terapi_asy" );
            $madapter = new SimpleAdapter ();
            $madapter->add ( "Nama", "nama" );
            $madapter->add ( "Kode", "icd" );
            $madapter->add ( "DTD", "dtd" );
            $madapter->add ( "Sebab", "sebab" );
            $dbtable = new DBTable ( $this->db, "smis_mr_icd" );
            $mr_mresponder = new DBResponder ( $dbtable, $muitable, $madapter );
            $super->addResponder ( "terapi_asy", $mr_mresponder );
		}
        
		$init = $super->initialize ();
		if ($init != null) {
			echo $init;
			return;
		}
	}
}

?>