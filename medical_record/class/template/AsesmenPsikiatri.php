<?php

require_once 'smis-framework/smis/template/ModulTemplate.php';
require_once ("smis-base/smis-include-service-consumer.php");
require_once "medical_record/class/service/RuanganService.php";
require_once 'smis-libs-hrd/EmployeeResponder.php';

class AsesmenPsikiatri extends ModulTemplate {
    protected $db;
	protected $polislug;
	protected $page;
	protected $protoslug;
	protected $protoname;
	protected $protoimplement;
	protected $action;
	protected $dbtable;
	protected $uitable;
	protected $noreg_pasien;
	protected $nama_pasien;
	protected $nrm_pasien;
	protected $alamat;
	protected $umur;
    protected $tgl_lahir;
    protected $jk;
	protected $ruang_asal;
	protected $id_antrian;
    protected $carabayar;
    protected $waktu_masuk;
    
    public function __construct($db, $polislug = "all", $noreg = "", $nrm = "", $nama = "", $alamat = "", $umur = "", $jk = "0", $tgl_lahir = "", $waktu_masuk = "", $id_antrian = "0", $page = "medical_record", $action = "asesmen_psikiatri",$carabayar, $protoslug = "", $protoname = "", $protoimplement = "") {
        $this->db               = $db;
		$this->noreg_pasien     = $noreg;
		$this->id_antrian       = $id_antrian;
		$this->nama_pasien      = $nama;
		$this->nrm_pasien       = $nrm;
		$this->alamat           = $alamat;
		$this->umur             = $umur;
        $this->jk               = $jk;
        $this->tgl_lahir        = $tgl_lahir;
        $this->waktu_masuk      = $waktu_masuk;
		$this->ruang_apgl       = $ruang_asal;
		$this->polislug         = $polislug;
		$this->dbtable          = new DBTable ( $this->db, "smis_mr_assesmen_psikiatri" );
		$this->page             = $page;
		$this->protoslug        = $protoslug;
		$this->protoimplement   = $protoimplement;
		$this->protoname        = $protoname;
		$this->action           = $action;
        $this->carabayar        = $carabayar;
        
        if ($noreg != ""){
            $this->dbtable->addCustomKriteria ( "noreg_pasien", "='" . $noreg . "'" );
        }
        $head=array ('NRM',"Pasien","No Register" );
		$this->uitable = new Table ($head , " Asesmen Psikiatri - " . ($this->polislug == "all" ? "" : ucfirst ( $this->protoname )), NULL, true );
		$this->uitable->setName ( $action );
    }
    
    public function command($command){
        require_once "smis-base/smis-include-duplicate.php";
		$adapter = new SimpleAdapter ();
		$adapter->add ( "Pasien", "nama_pasien" );
		$adapter->add ( "NRM", "nrm_pasien", "digit8" );
		$adapter->add ( "No Register", "noreg_pasien", "digit8" );
		$dbres = new DuplicateResponder ( $this->dbtable, $this->uitable, $adapter );
		$dbres->setDuplicate(false,"");
        $dbres->setAutonomous(getSettings($this->db,"smis_autonomous_id",""));
        $data = $dbres->command ( $_POST ['command'] );
		echo json_encode ( $data );
		return;
    }
    
    public function phpPreLoad(){
        $service = new RuanganService ( $this->db );
		$service->execute ();
		$ruangan = $service->getContent ();
        
        /*block data header*/
		if ($this->noreg_pasien != "") {
			$this->uitable->addModal ( "nama_pasien", "hidden", "", $this->nama_pasien, "n", null, true );
			$this->uitable->addModal ( "nrm_pasien", "hidden", "", $this->nrm_pasien, "n", null, true );
			$this->uitable->addModal ( "noreg_pasien", "hidden", "", $this->noreg_pasien, "n", null, true );
			$this->uitable->addModal ( "alamat_pasien", "hidden", "", $this->alamat, "n", null, true );
			$this->uitable->addModal ( "jk", "hidden", "", $this->jk, "n", null, true );
            $this->uitable->addModal ( "waktu_masuk", "hidden", "", $this->waktu_masuk, "n", null, true );
			$this->uitable->addModal ( "umur", "hidden", "", $this->umur, "n", null, true );
            $this->uitable->addModal ( "tgl_lahir_pasien", "hidden", "", $this->tgl_lahir, "n", null, true );
		} else {
			$this->uitable->addModal ( "nama_pasien", "chooser-" . $this->action . "-apk_pasien-Pasien", "Pasien", $this->nama_pasien, "n", null, true );
			$this->uitable->addModal ( "nrm_pasien", "text", "NRM", $this->nrm_pasien, "n", null, true );
			$this->uitable->addModal ( "noreg_pasien", "text", "No Reg", $this->noreg_pasien, "n", null, true );
		}
		if ($this->polislug == "all")
			$this->uitable->addModal ( "ruangan", "select", "Asal Ruangan", $ruangan );
		else
			$this->uitable->addModal ( "ruangan", "hidden", "", $this->polislug, "n", null, true );
        /*end - block data header*/
        
        $the_row = array ();
		$the_row ["id"] 						        = "";
		$the_row ["nama_dokter"] 				        = "";
		$the_row ["id_dokter"] 					        = "";
		$the_row ['waktu'] 					            = date ( "Y-m-d H:i:s" );
        $the_row ["carabayar"] 				            = $this->carabayar;
        $the_row ["waktu_masuk"] 				        = $this->waktu_masuk;		
        $the_row ["wawancara"] 					        = "";
        $the_row ["keluhan_utama"] 					    = "";
        $the_row ["riwayat_penyakit_skrg"] 				= "";
        $the_row ["auto_anamnesis"] 					= "";
        $the_row ["hetero_namnesis"] 					= "";
        $the_row ["faktor_penyebab"] 					= "";
        $the_row ["faktor_keluarga"] 					= "";
        $the_row ["fungsi_kerja"] 					    = "";
        $the_row ["riwayat_napza"] 					    = "";
        $the_row ["napza_lama_pemakaian"] 				= "";
        $the_row ["napza_jenis_zat"] 					= "";
        $the_row ["napza_cara_pemakaian"] 				= "";
        $the_row ["napza_latarbelakang_pemakaian"] 		= "";
        $the_row ["faktor_premorbid"] 					= "";
        $the_row ["faktor_organik"] 					= "";
        $the_row ["kesan_umum"] 					    = "";
        $the_row ["kesadaran"] 					        = "";
        $the_row ["mood"] 					            = "";
        $the_row ["proses_pikir"] 					    = "";
        $the_row ["pencerapan"] 					    = "";
        $the_row ["dorongan_insting"] 					= "";
        $the_row ["psikomotor"] 					    = "";
        $the_row ["penyakit_dalam"] 					= "";
        $the_row ["neurologi"] 					        = "";
        $the_row ["diagnosis_kerja"] 					= "";
        $the_row ["terapi"] 					        = "";
        $the_row ["rencana_kerja"] 					    = "";
        $the_row ["waktu_pulang"] 					    = "";
        $the_row ["waktu_kontrol_klinik"] 				= "";
        $the_row ["dirawat_diruang"] 					= "";
        
        $exist ['noreg_pasien'] = $this->noreg_pasien;
		$exist ['nama_pasien'] = $this->nama_pasien;
		$exist ['nrm_pasien'] = $this->nrm_pasien;
        
        if ($this->dbtable->is_exist ( $exist )){
            $row = $this->dbtable->select ( $exist );
            $the_row ["id"] 					            = $row->id;
            $the_row ["nama_dokter"] 			            = $row->nama_dokter;
            $the_row ["id_dokter"] 				            = $row->id_dokter;
            $the_row ["waktu"] 				                = $row->waktu;
            $the_row ["waktu_masuk"] 				        = $row->waktu_masuk;
            $the_row ["wawancara"] 				            = $row->wawancara;
            $the_row ["keluhan_utama"] 				        = $row->keluhan_utama;
            $the_row ["riwayat_penyakit_skrg"] 				= $row->riwayat_penyakit_skrg;
            $the_row ["auto_anamnesis"] 				    = $row->auto_anamnesis;
            $the_row ["hetero_namnesis"] 				    = $row->hetero_namnesis;
            $the_row ["faktor_penyebab"] 				    = $row->faktor_penyebab;
            $the_row ["fungsi_kerja"] 				        = $row->fungsi_kerja;
            $the_row ["riwayat_napza"] 				        = $row->riwayat_napza;
            $the_row ["napza_lama_pemakaian"] 				= $row->napza_lama_pemakaian;
            $the_row ["napza_jenis_zat"] 				    = $row->napza_jenis_zat;
            $the_row ["napza_cara_pemakaian"] 				= $row->napza_cara_pemakaian;
            $the_row ["napza_latarbelakang_pemakaian"] 		= $row->napza_latarbelakang_pemakaian;
            $the_row ["faktor_premorbid"] 				    = $row->faktor_premorbid;
            $the_row ["faktor_organik"] 				    = $row->faktor_organik;
            $the_row ["kesan_umum"] 				        = $row->kesan_umum;
            $the_row ["kesadaran"] 				            = $row->kesadaran;
            $the_row ["mood"] 				                = $row->mood;
            $the_row ["proses_pikir"] 				        = $row->proses_pikir;
            $the_row ["pencerapan"] 				        = $row->pencerapan;
            $the_row ["dorongan_insting"] 				    = $row->dorongan_insting;
            $the_row ["psikomotor"] 				        = $row->psikomotor;
            $the_row ["penyakit_dalam"] 				    = $row->penyakit_dalam;
            $the_row ["neurologi"] 				            = $row->neurologi;
            $the_row ["diagnosis_kerja"] 				    = $row->diagnosis_kerja;
            $the_row ["terapi"] 				            = $row->terapi;
            $the_row ["rencana_kerja"] 				        = $row->rencana_kerja;
            $the_row ["waktu_pulang"] 				        = $row->waktu_pulang;
            $the_row ["waktu_kontrol_klinik"] 				= $row->waktu_kontrol_klinik;
            $the_row ["dirawat_diruang"] 				    = $row->dirawat_diruang;
        }else{
            $exist ['umur'] = $this->umur;
			$exist ['jk'] = $this->jk;
			$exist ['alamat_pasien'] = $this->alamat;
			$exist ['noreg_pasien'] = $this->noreg_pasien;
			$exist ['nrm_pasien'] = $this->nrm_pasien;
			$exist ['nama_pasien'] = $this->nama_pasien;
            $exist ['ruangan'] = $this->polislug;
            $exist ['tgl_lahir_pasien'] = $this->tgl_lahir;
			$exist ['waktu'] = date ( "Y-m-d H:i:s" );
            $exist ['waktu_masuk'] = $this->waktu_masuk;
            $this->dbtable->insert ( $exist );
			
            $the_row ["id"] = $this->dbtable->get_inserted_id ();
            
            $update['origin_id']=$the_row ["id"];
            $update['autonomous']="[".getSettings($this->db,"smis_autonomous_id","")."]";
            $this->dbtable->update($update,array("id"=>$the_row ["id"]));
        }
        
        $this->uitable->addModal ( "id", "hidden", "", $the_row ['id'] );
		$this->uitable->addModal ( "nama_dokter", "chooser-apk_pasien-dokter_apk-Dokter Pemeriksa", "Dokter Pemeriksa", $the_row ['nama_dokter'],"n",null,false,null,true);
		$this->uitable->addModal ( "id_dokter", "hidden", "", $the_row ['id_dokter'],"y",null,false,null,false);
		$this->uitable->addModal ( "waktu", "datetime", "Waktu", $the_row ['waktu'] ,"y",null,false,null,false);
        $this->uitable->addModal("", "label", "<strong>ANAMNESES</strong>", "");
        $this->uitable->addModal("", "label", "Wawancara", "");
        $this->uitable->addModal ( "keluhan_utama", "text", "Keluhan Utama", $the_row ['keluhan_utama'] ,"y",null,false,null,false);
        $this->uitable->addModal ( "riwayat_penyakit_skrg", "text", "Riwayat Penyakit Sekarang", $the_row ['riwayat_penyakit_skrg'] ,"y",null,false,null,false);
        $this->uitable->addModal ( "auto_anamnesis", "text", "Auto Anamnesis", $the_row ['auto_anamnesis'] ,"y",null,false,null,false);
        $this->uitable->addModal ( "hetero_namnesis", "text", "Hetero Namnesis", $the_row ['hetero_namnesis'] ,"y",null,false,null,false);
        $this->uitable->addModal ( "faktor_penyebab", "text", "Faktor Pencetus/Penyebab", $the_row ['faktor_penyebab'] ,"y",null,false,null,false);
        $this->uitable->addModal ( "faktor_keluarga", "text", "Faktor Keluarga", $the_row ['faktor_keluarga'] ,"y",null,false,null,false);
        $this->uitable->addModal ( "fungsi_kerja", "text", "Fungsi Kerja/Sosial", $the_row ['fungsi_kerja'] ,"y",null,false,null,false);
        $napza=array(
            array("name"=>"","value"=>"","default"=> $the_row ["riwayat_napza"]==""?1:0),
            array("name"=>"Ada","value"=>"Ada","default"=> $the_row ["riwayat_napza"]=="Ada"?1:0),
            array("name"=>"Tidak","value"=>"Tidak","default"=> $the_row ["riwayat_napza"]=="Tidak"?1:0),
		);
        $this->uitable->addModal("riwayat_napza", "select", "Riwayat NAPZA", $napza,"y",NULL,false,NULL,false);
        $this->uitable->addModal ( "napza_lama_pemakaian", "text", "Lama Pemakaian", $the_row ['napza_lama_pemakaian'] ,"y",null,false,null,false);
        $this->uitable->addModal ( "napza_jenis_zat", "text", "Janis Zat", $the_row ['napza_jenis_zat'] ,"y",null,false,null,false);
        $this->uitable->addModal ( "napza_cara_pemakaian", "text", "Cara Pakai", $the_row ['napza_cara_pemakaian'] ,"y",null,false,null,false);
        $this->uitable->addModal ( "napza_latarbelakang_pemakaian", "text", "Latar Belakang Pemakaian", $the_row ['napza_latarbelakang_pemakaian'] ,"y",null,false,null,false);
        $this->uitable->addModal ( "faktor_premorbid", "text", "Faktor Premorbid", $the_row ['faktor_premorbid'] ,"y",null,false,null,false);
        $this->uitable->addModal ( "faktor_organik", "text", "Faktor Organik", $the_row ['faktor_organik'] ,"y",null,false,null,false);
        $this->uitable->addModal("", "label", "<strong>STATUS PSIKIATRI</strong>", "");
        $this->uitable->addModal ( "kesan_umum", "text", "Kesan Umum", $the_row ['kesan_umum'] ,"y",null,false,null,false);
        $this->uitable->addModal ( "kesadaran", "text", "Kesadaran", $the_row ['kesadaran'] ,"y",null,false,null,false);
        $this->uitable->addModal ( "mood", "text", "Mood/Afek", $the_row ['mood'] ,"y",null,false,null,false);
        $this->uitable->addModal ( "proses_pikir", "text", "Proses Pikir", $the_row ['proses_pikir'] ,"y",null,false,null,false);
        $this->uitable->addModal ( "pencerapan", "text", "Pencerapan", $the_row ['pencerapan'] ,"y",null,false,null,false);
        $this->uitable->addModal ( "dorongan_insting", "text", "Dorongan Insting", $the_row ['dorongan_insting'] ,"y",null,false,null,false);
        $this->uitable->addModal ( "psikomotor", "text", "Psikomotor", $the_row ['psikomotor'] ,"y",null,false,null,false);
        $this->uitable->addModal ( "penyakit_dalam", "text", "<strong>STATUS PENYAKIT DALAM</strong>", $the_row ['penyakit_dalam'] ,"y",null,false,null,false);
        $this->uitable->addModal ( "neurologi", "text", "<strong>STATUS NEUROLOGI</strong>", $the_row ['neurologi'] ,"y",null,false,null,false);
        $this->uitable->addModal ( "diagnosis_kerja", "text", "<strong>DIAGNOSIS KERJA/DIAGNOSIS BANDING</strong>", $the_row ['diagnosis_kerja'] ,"y",null,false,null,false);
        $this->uitable->addModal ( "terapi", "text", "<strong>TERAPI/TINDAKAN</strong>", $the_row ['terapi'] ,"y",null,false,null,false);
        $this->uitable->addModal ( "rencana_kerja", "text", "<strong>RENCANA KERJA</strong>", $the_row ['rencana_kerja'] ,"y",null,false,null,false);
        $this->uitable->addModal("", "label", "<strong>DISPOSISI</strong>", "");
        $this->uitable->addModal ( "waktu_pulang", "datetime", "Waktu Pulang", $the_row ['waktu_pulang'] ,"y",null,false,null,false);
        $this->uitable->addModal ( "waktu_kontrol_klinik", "datetime", "Tanggal Kontrol Klinik", $the_row ['waktu_kontrol_klinik'] ,"y",null,false,null,false);
        $dirawat=array(
				array("name"=>"","value"=>"","default"=> $the_row ["dirawat_diruang"]==""?1:0),
				array("name"=>"Intensif","value"=>"Intensif","default"=> $the_row ["dirawat_diruang"]=="Intensif"?1:0),
				array("name"=>"Ruang Lain","value"=>"Ruang Lain","default"=> $the_row ["dirawat_diruang"]=="Ruang Lain"?1:0),
				array("name"=>"Tidak Dirawat Diruang","value"=>"Tidak Dirawat Diruang","default"=> $the_row ["dirawat_diruang"]=="Tidak Dirawat Diruang"?1:0)
		);
        $this->uitable->addModal("dirawat_diruang", "select", "Dirawat Diruang", $dirawat,"y",NULL,false,NULL,false);
        
        $modal = $this->uitable->getModal ();
		$modal->setTitle ( "Assessment Psikiatri" );
		if ($this->page == "medical_record") {
			echo $this->uitable->getHtml ();
		} else {
			echo "<div class='hide'>" . $this->uitable->getHtml () . "</div>";
			$modal->setAlwaysShow ( true );
		}
		echo $modal->joinFooterAndForm ()->getHtml ();
        
        $hidden_noreg=new Hidden("apk_noreg_pasien","",$this->noreg_pasien);
        $hidden_nama=new Hidden("apk_nama_pasien","",$this->nama_pasien);
        $hidden_nrm=new Hidden("apk_nrm_pasien","",$this->nrm_pasien);
        $hidden_polislug=new Hidden("apk_polislug","",$this->polislug);
        $hidden_the_page=new Hidden("apk_the_page","",$this->page);
        $hidden_the_protoslug=new Hidden("apk_the_protoslug","",$this->protoslug);
        $hidden_the_protoname=new Hidden("apk_the_protoname","",$this->protoname);
        $hidden_the_protoimpl=new Hidden("apk_the_protoimplement","",$this->protoimplement);
        $hidden_antrian=new Hidden("apk_id_antrian","",$this->id_antrian);
        $hidden_action=new Hidden("apk_action","",$this->action);
        
        echo $hidden_noreg->getHtml();
        echo $hidden_nama->getHtml();
        echo $hidden_nrm->getHtml();
        echo $hidden_polislug->getHtml();
        echo $hidden_the_page->getHtml();
        echo $hidden_the_protoslug->getHtml();
        echo $hidden_the_protoname->getHtml();
        echo $hidden_the_protoimpl->getHtml();
        echo $hidden_antrian->getHtml();
        echo $hidden_action->getHtml();
        
        echo addJS("medical_record/resource/js/asesmen_psikiatri.js",false);
        echo addCSS ( "framework/bootstrap/css/datepicker.css" );
        echo addJS ( "framework/bootstrap/js/bootstrap-datetimepicker.js" );
        echo addCSS ( "framework/bootstrap/css/bootstrap-datetimepicker.css" );
    }
    
    public function cssPreLoad() {
		?>
		<style type="text/css">
				#<?php echo $this->action;?>_add_form_form>div {clear: both; width: 100%;}
				#<?php echo $this->action; ?>_add_form_form>div label { width: 250px; }
		</style>
		<?php
	}
    
    public function superCommand($super_command){
        $super = new SuperCommand ();
        
        /* PASIEN */
		$phead=array ('Nama','NRM',"No Reg" );
		$ptable = new Table ( $phead, "", NULL, true );
		$ptable->setName ( "apk_pasien" );
		$ptable->setModel ( Table::$SELECT );
		$padapter = new SimpleAdapter ();
		$padapter->add ( "Nama", "nama_pasien" );
		$padapter->add ( "NRM", "nrm", "digit8" );
		$padapter->add ( "No Reg", "id" );
		$presponder = new ServiceResponder ( $this->db, $ptable, $padapter, "get_registered" );		
		$super->addResponder ( "apk_pasien", $presponder );
        
        /* DOKTER */
		$eadapt = new SimpleAdapter ();
		$eadapt->add ( "Jabatan", "nama_jabatan" );
		$eadapt->add ( "Nama", "nama" );
		$eadapt->add ( "NIP", "nip" );
		$head_karyawan=array ('Nama','Jabatan',"NIP");
		$dktable = new Table ( $head_karyawan, "", NULL, true );
		$dktable->setName ( "dokter_apk" );
		$dktable->setModel ( Table::$SELECT );
		$employee = new EmployeeResponder ( $this->db, $dktable, $eadapt, "dokter" );
		$super->addResponder ( "dokter_apk", $employee );
        
        $init = $super->initialize ();
		if ($init != null) {
			echo $init;
			return;
		}
    }
}

?>