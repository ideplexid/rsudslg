<?php

require_once 'smis-framework/smis/template/ModulTemplate.php';
require_once ("smis-base/smis-include-service-consumer.php");
require_once "medical_record/class/service/RuanganService.php";
require_once 'smis-libs-hrd/EmployeeResponder.php';

class AsesmenAnak extends ModulTemplate {
    protected $db;
	protected $polislug;
	protected $page;
	protected $protoslug;
	protected $protoname;
	protected $protoimplement;
	protected $action;
	protected $dbtable;
	protected $uitable;
	protected $noreg_pasien;
	protected $nama_pasien;
	protected $nrm_pasien;
	protected $alamat;
	protected $umur;
    protected $tgl_lahir;
    protected $jk;
	protected $ruang_asal;
	protected $id_antrian;
    protected $carabayar;
    protected $waktu_masuk;
    
    public function __construct($db, $polislug = "all", $noreg = "", $nrm = "", $nama = "", $alamat = "", $umur = "", $jk = "0", $tgl_lahir = "", $waktu_masuk = "", $id_antrian = "0", $page = "medical_record", $action = "asesmen_anak",$carabayar, $protoslug = "", $protoname = "", $protoimplement = "") {
        $this->db               = $db;
		$this->noreg_pasien     = $noreg;
		$this->id_antrian       = $id_antrian;
		$this->nama_pasien      = $nama;
		$this->nrm_pasien       = $nrm;
		$this->alamat           = $alamat;
		$this->umur             = $umur;
        $this->jk               = $jk;
        $this->waktu_masuk      = $waktu_masuk;
        $this->tgl_lahir        = $tgl_lahir;
		$this->ruang_asal       = $ruang_asal;
		$this->polislug         = $polislug;
		$this->dbtable          = new DBTable ( $this->db, "smis_mr_assesmen_anak" );
		$this->page             = $page;
		$this->protoslug        = $protoslug;
		$this->protoimplement   = $protoimplement;
		$this->protoname        = $protoname;
		$this->action           = $action;
        $this->carabayar        = $carabayar;
        
        if ($noreg != ""){
            $this->dbtable->addCustomKriteria ( "noreg_pasien", "='" . $noreg . "'" );
        }
        $head=array ('NRM',"Pasien","No Register" );
		$this->uitable = new Table ($head , " Asesmen Anak - " . ($this->polislug == "all" ? "" : ucfirst ( $this->protoname )), NULL, true );
		$this->uitable->setName ( $action );
    }
    
    public function command($command) {
        require_once "smis-base/smis-include-duplicate.php";
		$adapter = new SimpleAdapter ();
		$adapter->add ( "Pasien", "nama_pasien" );
		$adapter->add ( "NRM", "nrm_pasien", "digit8" );
		$adapter->add ( "No Register", "noreg_pasien", "digit8" );
		$dbres = new DuplicateResponder ( $this->dbtable, $this->uitable, $adapter );
		$dbres->setDuplicate(false,"");
        $dbres->setAutonomous(getSettings($this->db,"smis_autonomous_id",""));
        $data = $dbres->command ( $_POST ['command'] );
		echo json_encode ( $data );
		return;
	}
    
    public function phpPreLoad() {
        $service = new RuanganService ( $this->db );
		$service->execute ();
		$ruangan = $service->getContent ();
        
        /*block data header*/
		if ($this->noreg_pasien != "") {
			$this->uitable->addModal ( "nama_pasien", "hidden", "", $this->nama_pasien, "n", null, true );
			$this->uitable->addModal ( "nrm_pasien", "hidden", "", $this->nrm_pasien, "n", null, true );
			$this->uitable->addModal ( "noreg_pasien", "hidden", "", $this->noreg_pasien, "n", null, true );
			$this->uitable->addModal ( "alamat_pasien", "hidden", "", $this->alamat, "n", null, true );
			$this->uitable->addModal ( "jk", "hidden", "", $this->jk, "n", null, true );
			$this->uitable->addModal ( "umur", "hidden", "", $this->umur, "n", null, true );
            $this->uitable->addModal ( "tgl_lahir_pasien", "hidden", "", $this->tgl_lahir, "n", null, true );
            $this->uitable->addModal ( "waktu_masuk", "hidden", "", $this->waktu_masuk, "n", null, true );
		} else {
			$this->uitable->addModal ( "nama_pasien", "chooser-" . $this->action . "-asa_pasien-Pasien", "Pasien", $this->nama_pasien, "n", null, true );
			$this->uitable->addModal ( "nrm_pasien", "text", "NRM", $this->nrm_pasien, "n", null, true );
			$this->uitable->addModal ( "noreg_pasien", "text", "No Reg", $this->noreg_pasien, "n", null, true );
		}
		if ($this->polislug == "all")
			$this->uitable->addModal ( "ruangan", "select", "Asal Ruangan", $ruangan );
		else
			$this->uitable->addModal ( "ruangan", "hidden", "", $this->polislug, "n", null, true );
        /*end - block data header*/
        
        $the_row = array ();
		$the_row ["id"] 						    = "";
		$the_row ["nama_dokter"] 				    = "";
		$the_row ["id_dokter"] 					    = "";
		$the_row ['waktu'] 					        = date ( "Y-m-d H:i:s" );
        $the_row ["carabayar"] 				        = $this->carabayar;
        $the_row ["waktu_masuk"] 				    = $this->waktu_masuk;
        $the_row ["bb"] 				            = "";
        $the_row ["tb"] 				            = "";
        $the_row ["lila"] 				            = "";
        $the_row ["lk"] 				            = "";
        $the_row ["anamneses_utama"] 				= "";
        $the_row ["anamneses_penyakit_skrg"] 	    = "";
        $the_row ["anamneses_pengobatan"] 			= "";
        $the_row ["mata_anemia"] 				    = "";
        $the_row ["mata_ikterus"] 				    = "";
        $the_row ["mata_reflek_pupil"] 				= "";
        $the_row ["mata_edema_palbebrae"] 			= "";
        $the_row ["tht_tonsil"] 				    = "";
        $the_row ["tht_faring"] 				    = "";
        $the_row ["tht_lidah"] 				        = "";
        $the_row ["tht_bibir"] 				        = "";
        $the_row ["leher_jvp"] 				        = "";
        $the_row ["leher_pembesaran_kelenjar"] 		= "";
        $the_row ["kaku_kuduk"] 				    = "";
        $the_row ["torak_simetris"] 				= "";
        $the_row ["torak_asimetris"] 				= "";
        $the_row ["jantung_s1_s2"] 				    = "";
        $the_row ["jantung_reguler"] 				= "";
        $the_row ["jantung_ireguler"] 				= "";
        $the_row ["jantung_murmur"] 				= "";
        $the_row ["jantung_lain"] 				    = "";
        $the_row ["paru_suara_nafas"] 				= "";
        $the_row ["paru_ronki"] 				    = "";
        $the_row ["paru_wheezing"] 				    = "";
        $the_row ["paru_lain"] 				        = "";
        $the_row ["abdomen_distensi"] 				= "";
        $the_row ["abdomen_meteorismus"] 			= "";
        $the_row ["abdomen_peristaltik"] 			= "";
        $the_row ["abdomen_arcites"] 				= "";
        $the_row ["abdomen_nyeri"] 				    = "";
        $the_row ["abdomen_nyeri_lokasi"] 			= "";
        $the_row ["hepar"] 				            = "";
        $the_row ["lien"] 				            = "";
        $the_row ["status_gizi"] 				    = "";
        $the_row ["extremitas_hangat"] 				= "";
        $the_row ["extremitas_dingin"] 				= "";
        $the_row ["extremitas_edema"] 				= "";
        $the_row ["extremitas_lain"] 				= "";
        $the_row ["rencana_kerja"] 				    = "";
        $the_row ["hasil_periksa_penunjang"] 		= "";
        $the_row ["diagnosis"] 				        = "";
        $the_row ["terapi"] 				        = "";
        $the_row ["hasil_pembedahan"] 				= "";
        $the_row ["rekomendasi"] 				    = "";
        $the_row ["catatan"] 				        = "";
        $the_row ["waktu_pulang"] 				    = "";
        $the_row ["tgl_kontrol_klinik"] 			= "";
        $the_row ["dirawat_diruang"] 				= "";
        
        $exist ['noreg_pasien'] = $this->noreg_pasien;
		$exist ['nama_pasien'] = $this->nama_pasien;
		$exist ['nrm_pasien'] = $this->nrm_pasien;
        if ($this->dbtable->is_exist ( $exist )){
            $row = $this->dbtable->select ( $exist );
            $the_row ["id"] 					        = $row->id;
            $the_row ["nama_dokter"] 			        = $row->nama_dokter;
            $the_row ["id_dokter"] 				        = $row->id_dokter;
            $the_row ["waktu"] 				            = $row->waktu;
            $the_row ["waktu_masuk"] 				    = $row->waktu_masuk;
            $the_row ["bb"] 				            = $row->bb;
            $the_row ["tb"] 				            = $row->tb;
            $the_row ["lila"] 				            = $row->lila;
            $the_row ["lk"] 				            = $row->lk;
            $the_row ["anamneses_utama"] 				= $row->anamneses_utama;
            $the_row ["anamneses_penyakit_skrg"] 		= $row->anamneses_penyakit_skrg;
            $the_row ["anamneses_pengobatan"] 			= $row->anamneses_pengobatan;
            $the_row ["mata_anemia"] 				    = $row->mata_anemia;
            $the_row ["mata_ikterus"] 				    = $row->mata_ikterus;
            $the_row ["mata_reflek_pupil"] 				= $row->mata_reflek_pupil;
            $the_row ["mata_edema_palbebrae"] 			= $row->mata_edema_palbebrae;
            $the_row ["tht_tonsil"] 				    = $row->tht_tonsil;
            $the_row ["tht_faring"] 				    = $row->tht_faring;
            $the_row ["tht_lidah"] 				        = $row->tht_lidah;
            $the_row ["tht_bibir"] 				        = $row->tht_bibir;
            $the_row ["leher_jvp"] 				        = $row->leher_jvp;
            $the_row ["leher_pembesaran_kelenjar"] 		= $row->leher_pembesaran_kelenjar;
            $the_row ["kaku_kuduk"] 				    = $row->kaku_kuduk;
            $the_row ["torak_simetris"] 				= $row->torak_simetris;
            $the_row ["torak_asimetris"] 				= $row->torak_asimetris;
            $the_row ["jantung_s1_s2"] 				    = $row->jantung_s1_s2;
            $the_row ["jantung_reguler"] 				= $row->jantung_reguler;
            $the_row ["jantung_ireguler"] 				= $row->jantung_ireguler;
            $the_row ["jantung_murmur"] 				= $row->jantung_murmur;
            $the_row ["jantung_lain"] 				    = $row->jantung_lain;
            $the_row ["paru_suara_nafas"] 				= $row->paru_suara_nafas;
            $the_row ["paru_ronki"] 				    = $row->paru_ronki;
            $the_row ["paru_wheezing"] 				    = $row->paru_wheezing;
            $the_row ["paru_lain"] 				        = $row->paru_lain;
            $the_row ["abdomen_distensi"] 				= $row->abdomen_distensi;
            $the_row ["abdomen_meteorismus"] 			= $row->abdomen_meteorismus;
            $the_row ["abdomen_peristaltik"] 			= $row->abdomen_peristaltik;
            $the_row ["abdomen_arcites"] 				= $row->abdomen_arcites;
            $the_row ["abdomen_nyeri"] 				    = $row->abdomen_nyeri;
            $the_row ["abdomen_nyeri_lokasi"] 			= $row->abdomen_nyeri_lokasi;
            $the_row ["hepar"] 				            = $row->hepar;
            $the_row ["lien"] 				            = $row->lien;
            $the_row ["status_gizi"] 				    = $row->status_gizi;
            $the_row ["extremitas_hangat"] 				= $row->extremitas_hangat;
            $the_row ["extremitas_dingin"] 				= $row->extremitas_dingin;
            $the_row ["extremitas_edema"] 				= $row->extremitas_edema;
            $the_row ["extremitas_lain"] 				= $row->extremitas_lain;
            $the_row ["rencana_kerja"] 				    = $row->rencana_kerja;
            $the_row ["hasil_periksa_penunjang"] 		= $row->hasil_periksa_penunjang;
            $the_row ["diagnosis"] 				        = $row->diagnosis;
            $the_row ["terapi"] 				        = $row->terapi;
            $the_row ["hasil_pembedahan"] 				= $row->hasil_pembedahan;
            $the_row ["rekomendasi"] 				    = $row->rekomendasi;
            $the_row ["catatan"] 				        = $row->catatan;
            $the_row ["waktu_pulang"] 				    = $row->waktu_pulang;
            $the_row ["tgl_kontrol_klinik"] 			= $row->tgl_kontrol_klinik;
            $the_row ["dirawat_diruang"] 				= $row->dirawat_diruang;
        }else{
            $exist ['umur'] = $this->umur;
			$exist ['jk'] = $this->jk;
			$exist ['alamat_pasien'] = $this->alamat;
			$exist ['noreg_pasien'] = $this->noreg_pasien;
			$exist ['nrm_pasien'] = $this->nrm_pasien;
			$exist ['nama_pasien'] = $this->nama_pasien;
            $exist ['ruangan'] = $this->polislug;
            $exist ['tgl_lahir_pasien'] = $this->tgl_lahir;
			$exist ['waktu'] = date ( "Y-m-d H:i:s" );
			$exist ['waktu_masuk'] = $this->waktu_masuk;
            $this->dbtable->insert ( $exist );
			
            $the_row ["id"] = $this->dbtable->get_inserted_id ();
            
            $update['origin_id']=$the_row ["id"];
            $update['autonomous']="[".getSettings($this->db,"smis_autonomous_id","")."]";
            $this->dbtable->update($update,array("id"=>$the_row ["id"]));
        }
        
        $this->uitable->addModal ( "id", "hidden", "", $the_row ['id'] );
		$this->uitable->addModal ( "nama_dokter", "chooser-asa_pasien-dokter_asa-Dokter Pemeriksa", "Dokter Pemeriksa", $the_row ['nama_dokter'],"n",null,false,null,true);
		$this->uitable->addModal ( "id_dokter", "hidden", "", $the_row ['id_dokter'],"y",null,false,null,false);
		$this->uitable->addModal ( "waktu", "datetime", "Waktu", $the_row ['waktu'] ,"y",null,false,null,false);
		$this->uitable->addModal ( "carabayar", "text", "Carabayar", $the_row ['carabayar'] ,"n",null,true,null,false);
        $this->uitable->addModal ( "bb", "text", "Berat Badan", $the_row ['bb'] ,"y",null,false,null,false);
        $this->uitable->addModal ( "tb", "text", "Tinggi Badan", $the_row ['tb'] ,"y",null,false,null,false);
        $this->uitable->addModal ( "lila", "text", "LILA", $the_row ['lila'] ,"y",null,false,null,false);
        $this->uitable->addModal ( "lk", "text", "LK", $the_row ['lk'] ,"y",null,false,null,false);
        $this->uitable->addModal("", "label", "<strong>ANAMNESES</strong>", "");
        $this->uitable->addModal ( "anamneses_utama", "text", "1. Keluhan Utama", $the_row ['anamneses_utama'] ,"y",null,false,null,false);
        $this->uitable->addModal ( "anamneses_penyakit_skrg", "text", "2. Riwayat Penyakit Sekarang", $the_row ['anamneses_penyakit_skrg'] ,"y",null,false,null,false);
        $this->uitable->addModal ( "anamneses_pengobatan", "text", "3. Riwayat Pengobatan", $the_row ['anamneses_pengobatan'] ,"y",null,false,null,false);
        $this->uitable->addModal("", "label", "<strong>PEMERIKSAAN FISIK</strong>", "");
        $this->uitable->addModal("", "label", "<strong>Mata</strong>", "");
        $this->uitable->addModal ( "mata_anemia", "text", "Anemia", $the_row ['mata_anemia'] ,"y",null,false,null,false);
        $this->uitable->addModal ( "mata_ikterus", "text", "Ikterus", $the_row ['mata_ikterus'] ,"y",null,false,null,false);
        $this->uitable->addModal ( "mata_reflek_pupil", "text", "Reflek Pupil", $the_row ['mata_reflek_pupil'] ,"y",null,false,null,false);
        $this->uitable->addModal ( "mata_edema_palbebrae", "text", "Edema Palbebrae", $the_row ['mata_edema_palbebrae'] ,"y",null,false,null,false);
        $this->uitable->addModal("", "label", "<strong>THT</strong>", "");
        $this->uitable->addModal ( "tht_tonsil", "text", "Tonsil", $the_row ['tht_tonsil'] ,"y",null,false,null,false);
        $this->uitable->addModal ( "tht_faring", "text", "Faring", $the_row ['tht_faring'] ,"y",null,false,null,false);
        $this->uitable->addModal ( "tht_lidah", "text", "Lidah", $the_row ['tht_lidah'] ,"y",null,false,null,false);
        $this->uitable->addModal ( "tht_bibir", "text", "Bibir", $the_row ['tht_bibir'] ,"y",null,false,null,false);
        $this->uitable->addModal("", "label", "<strong>Leher</strong>", "");
        $this->uitable->addModal ( "leher_jvp", "text", "JVP", $the_row ['leher_jvp'] ,"y",null,false,null,false);
        $this->uitable->addModal ( "leher_pembesaran_kelenjar", "text", "Pembesaran Kelenjar", $the_row ['leher_pembesaran_kelenjar'] ,"y",null,false,null,false);
        $kaku_kuduk=array(
            array("name"=>"","value"=>"","default"=> $the_row ["kaku_kuduk"]==""?1:0),
            array("name"=>"positif (+)","value"=>"positif","default"=> $the_row ["kaku_kuduk"]=="positif"?1:0),
            array("name"=>"negatif (-)","value"=>"negatif","default"=> $the_row ["kaku_kuduk"]=="negatif"?1:0),
		);
        $this->uitable->addModal("kaku_kuduk", "select", "<strong>Kaku Kuduk</strong>", $kaku_kuduk,"y",NULL,false,NULL,false);
        $this->uitable->addModal("", "label", "<strong>Torak</strong>", "");
        $this->uitable->addModal ( "torak_simetris", "text", "Simetris", $the_row ['torak_simetris'] ,"y",null,false,null,false);
        $this->uitable->addModal ( "torak_asimetris", "text", "Asimetris", $the_row ['torak_asimetris'] ,"y",null,false,null,false);
        $this->uitable->addModal("", "label", "<strong>Jantung</strong>", "");
        $this->uitable->addModal ( "jantung_s1_s2", "text", "S1 / S2", $the_row ['jantung_s1_s2'] ,"y",null,false,null,false);
        $this->uitable->addModal ( "jantung_reguler", "text", "Reguler", $the_row ['jantung_reguler'] ,"y",null,false,null,false);
        $this->uitable->addModal ( "jantung_ireguler", "text", "Ireguler", $the_row ['jantung_ireguler'] ,"y",null,false,null,false);
        $this->uitable->addModal ( "jantung_murmur", "text", "Murmur", $the_row ['jantung_murmur'] ,"y",null,false,null,false);
        $this->uitable->addModal ( "jantung_lain", "text", "Lain-Lain", $the_row ['jantung_lain'] ,"y",null,false,null,false);
        $this->uitable->addModal("", "label", "<strong>Paru-Paru</strong>", "");
        $this->uitable->addModal ( "paru_suara_nafas", "text", "Suara Nafas", $the_row ['paru_suara_nafas'] ,"y",null,false,null,false);
        $this->uitable->addModal ( "paru_ronki", "text", "Ronki", $the_row ['paru_ronki'] ,"y",null,false,null,false);
        $this->uitable->addModal ( "paru_wheezing", "text", "Wheezing", $the_row ['paru_wheezing'] ,"y",null,false,null,false);
        $this->uitable->addModal ( "paru_lain", "text", "Lain-Lain", $the_row ['paru_lain'] ,"y",null,false,null,false);
        $this->uitable->addModal("", "label", "<strong>Abdomen</strong>", "");
        $abdomen_distensi=array(
            array("name"=>"","value"=>"","default"=> $the_row ["abdomen_distensi"]==""?1:0),
            array("name"=>"positif (+)","value"=>"positif","default"=> $the_row ["abdomen_distensi"]=="positif"?1:0),
            array("name"=>"negatif (-)","value"=>"negatif","default"=> $the_row ["abdomen_distensi"]=="negatif"?1:0),
		);
        $this->uitable->addModal("abdomen_distensi", "select", "Distensi", $abdomen_distensi,"y",NULL,false,NULL,false);
        $abdomen_meteorismus=array(
            array("name"=>"","value"=>"","default"=> $the_row ["abdomen_meteorismus"]==""?1:0),
            array("name"=>"positif (+)","value"=>"positif","default"=> $the_row ["abdomen_meteorismus"]=="positif"?1:0),
            array("name"=>"negatif (-)","value"=>"negatif","default"=> $the_row ["abdomen_meteorismus"]=="negatif"?1:0),
		);
        $this->uitable->addModal("abdomen_meteorismus", "select", "Meteorismus", $abdomen_meteorismus,"y",NULL,false,NULL,false);
        $peristaltik=array(
            array("name"=>"","value"=>"","default"=> $the_row ["abdomen_peristaltik"]==""?1:0),
            array("name"=>"Normal","value"=>"Normal","default"=> $the_row ["abdomen_peristaltik"]=="Normal"?1:0),
            array("name"=>"Meningkat","value"=>"Meningkat","default"=> $the_row ["abdomen_peristaltik"]=="Meningkat"?1:0),
            array("name"=>"Menurun","value"=>"Menurun","default"=> $the_row ["abdomen_peristaltik"]=="Menurun"?1:0),
		);
        $this->uitable->addModal("abdomen_peristaltik", "select", "Peristaltik", $peristaltik,"y",NULL,false,NULL,false);
        $abdomen_arcites=array(
            array("name"=>"","value"=>"","default"=> $the_row ["abdomen_arcites"]==""?1:0),
            array("name"=>"positif (+)","value"=>"positif","default"=> $the_row ["abdomen_arcites"]=="positif"?1:0),
            array("name"=>"negatif (-)","value"=>"negatif","default"=> $the_row ["abdomen_arcites"]=="negatif"?1:0),
		);
        $this->uitable->addModal("abdomen_arcites", "select", "Arcites", $abdomen_arcites,"y",NULL,false,NULL,false);
        $abdomen_nyeri=array(
            array("name"=>"","value"=>"","default"=> $the_row ["abdomen_nyeri"]==""?1:0),
            array("name"=>"positif (+)","value"=>"positif","default"=> $the_row ["abdomen_nyeri"]=="positif"?1:0),
            array("name"=>"negatif (-)","value"=>"negatif","default"=> $the_row ["abdomen_nyeri"]=="negatif"?1:0),
		);
        $this->uitable->addModal("abdomen_nyeri", "select", "Nyeri Tekan", $abdomen_nyeri,"y",NULL,false,NULL,false);
        $this->uitable->addModal ( "abdomen_nyeri_lokasi", "text", "Nyeri Tekan Lokasi", $the_row ['abdomen_nyeri_lokasi'] ,"y",null,false,null,false);
        $this->uitable->addModal ( "hepar", "text", "<strong>Hepar</strong>", $the_row ['hepar'] ,"y",null,false,null,false);
        $this->uitable->addModal ( "lien", "text", "<strong>Lien</strong>", $the_row ['lien'] ,"y",null,false,null,false);
        $this->uitable->addModal ( "status_gizi", "text", "<strong>Status Gizi</strong>", $the_row ['status_gizi'] ,"y",null,false,null,false);
        $this->uitable->addModal("", "label", "<strong>Extremitas</strong>", "");
        $this->uitable->addModal ( "extremitas_hangat", "text", "Hangat", $the_row ['extremitas_hangat'] ,"y",null,false,null,false);
        $this->uitable->addModal ( "extremitas_dingin", "text", "Dingin", $the_row ['extremitas_dingin'] ,"y",null,false,null,false);
        $this->uitable->addModal ( "extremitas_edema", "text", "Edema", $the_row ['extremitas_edema'] ,"y",null,false,null,false);
        $this->uitable->addModal ( "extremitas_lain", "text", "Lain-Lain", $the_row ['extremitas_lain'] ,"y",null,false,null,false);
        $this->uitable->addModal ( "rencana_kerja", "textarea", "<strong>RENCANA KERJA</strong>", $the_row ['rencana_kerja'] ,"y",null,false,null,false);
        $this->uitable->addModal ( "hasil_periksa_penunjang", "textarea", "<strong>HASIL PEMERIKSAAN PENUNJANG</strong>", $the_row ['hasil_periksa_penunjang'] ,"y",null,false,null,false);
        $this->uitable->addModal ( "diagnosis", "textarea", "<strong>DIAGNOSIS</strong>", $the_row ['diagnosis'] ,"y",null,false,null,false);
        $this->uitable->addModal ( "terapi", "textarea", "<strong>TERAPI</strong>", $the_row ['terapi'] ,"y",null,false,null,false);
        $this->uitable->addModal ( "hasil_pembedahan", "textarea", "<strong>HASIL PEMBEDAHAN</strong>", $the_row ['hasil_pembedahan'] ,"y",null,false,null,false);
        $this->uitable->addModal ( "rekomendasi", "textarea", "<strong>REKOMENDASI (SARAN)</strong>", $the_row ['rekomendasi'] ,"y",null,false,null,false);
        $this->uitable->addModal ( "catatan", "textarea", "<strong>CATATAN</strong>", $the_row ['catatan'] ,"y",null,false,null,false);
        $this->uitable->addModal("", "label", "<strong>DISPOSISI</strong>", "");
        $this->uitable->addModal ( "waktu_pulang", "datetime", "Waktu Pulang", $the_row ['waktu_pulang'] ,"y",null,false,null,false);
        $this->uitable->addModal ( "tgl_kontrol_klinik", "date", "Tanggal Kontrol Klinik", $the_row ['tgl_kontrol_klinik'] ,"y",null,false,null,false);
        $dirawat=array(
				array("name"=>"","value"=>"","default"=> $the_row ["dirawat_diruang"]==""?1:0),
				array("name"=>"Intensif","value"=>"Intensif","default"=> $the_row ["dirawat_diruang"]=="Intensif"?1:0),
				array("name"=>"Ruang Lain","value"=>"Ruang Lain","default"=> $the_row ["dirawat_diruang"]=="Ruang Lain"?1:0),
				array("name"=>"Tidak Dirawat Diruang","value"=>"Tidak Dirawat Diruang","default"=> $the_row ["dirawat_diruang"]=="Tidak Dirawat Diruang"?1:0)
		);
        $this->uitable->addModal("dirawat_diruang", "select", "Dirawat Diruang", $dirawat,"y",NULL,false,NULL,false);
        
        
        $modal = $this->uitable->getModal ();
		$modal->setTitle ( "Assessment Anak" );
		if ($this->page == "medical_record") {
			echo $this->uitable->getHtml ();
		} else {
			echo "<div class='hide'>" . $this->uitable->getHtml () . "</div>";
			$modal->setAlwaysShow ( true );
		}
		echo $modal->joinFooterAndForm ()->getHtml ();
        
        $hidden_noreg=new Hidden("asa_noreg_pasien","",$this->noreg_pasien);
        $hidden_nama=new Hidden("asa_nama_pasien","",$this->nama_pasien);
        $hidden_nrm=new Hidden("asa_nrm_pasien","",$this->nrm_pasien);
        $hidden_polislug=new Hidden("asa_polislug","",$this->polislug);
        $hidden_the_page=new Hidden("asa_the_page","",$this->page);
        $hidden_the_protoslug=new Hidden("asa_the_protoslug","",$this->protoslug);
        $hidden_the_protoname=new Hidden("asa_the_protoname","",$this->protoname);
        $hidden_the_protoimpl=new Hidden("asa_the_protoimplement","",$this->protoimplement);
        $hidden_antrian=new Hidden("asa_id_antrian","",$this->id_antrian);
        $hidden_action=new Hidden("asa_action","",$this->action);
        
        echo $hidden_noreg->getHtml();
        echo $hidden_nama->getHtml();
        echo $hidden_nrm->getHtml();
        echo $hidden_polislug->getHtml();
        echo $hidden_the_page->getHtml();
        echo $hidden_the_protoslug->getHtml();
        echo $hidden_the_protoname->getHtml();
        echo $hidden_the_protoimpl->getHtml();
        echo $hidden_antrian->getHtml();
        echo $hidden_action->getHtml();
        
        echo addJS("medical_record/resource/js/asesmen_anak.js",false);
        echo addCSS ( "framework/bootstrap/css/datepicker.css" );
        echo addJS ( "framework/bootstrap/js/bootstrap-datetimepicker.js" );
        echo addCSS ( "framework/bootstrap/css/bootstrap-datetimepicker.css" );
    }
    
    public function cssPreLoad() {
		?>
		<style type="text/css">
				#<?php echo $this->action;?>_add_form_form>div {clear: both; width: 100%;}
				#<?php echo $this->action; ?>_add_form_form>div label { width: 250px; }
		</style>
		<?php
	}
    
    public function superCommand($super_command){
		$super = new SuperCommand ();
        
		/* PASIEN */
		$phead=array ('Nama','NRM',"No Reg" );
		$ptable = new Table ( $phead, "", NULL, true );
		$ptable->setName ( "asa_pasien" );
		$ptable->setModel ( Table::$SELECT );
		$padapter = new SimpleAdapter ();
		$padapter->add ( "Nama", "nama_pasien" );
		$padapter->add ( "NRM", "nrm", "digit8" );
		$padapter->add ( "No Reg", "id" );
		$presponder = new ServiceResponder ( $this->db, $ptable, $padapter, "get_registered" );		
		$super->addResponder ( "asa_pasien", $presponder );
        
        /* DOKTER */
		$eadapt = new SimpleAdapter ();
		$eadapt->add ( "Jabatan", "nama_jabatan" );
		$eadapt->add ( "Nama", "nama" );
		$eadapt->add ( "NIP", "nip" );
		$head_karyawan=array ('Nama','Jabatan',"NIP");
		$dktable = new Table ( $head_karyawan, "", NULL, true );
		$dktable->setName ( "dokter_asa" );
		$dktable->setModel ( Table::$SELECT );
		$employee = new EmployeeResponder ( $this->db, $dktable, $eadapt, "dokter" );
		$super->addResponder ( "dokter_asa", $employee );
        
        $init = $super->initialize ();
		if ($init != null) {
			echo $init;
			return;
		}
    }
}

?>