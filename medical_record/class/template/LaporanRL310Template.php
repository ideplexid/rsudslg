<?php
require_once 'smis-framework/smis/template/ModulTemplate.php';
require_once ("smis-base/smis-include-service-consumer.php");
require_once "medical_record/class/service/RuanganService.php";
require_once 'smis-libs-hrd/EmployeeResponder.php';
class LaporanRL310Template extends ModulTemplate {
	protected $db;
	protected $polislug;
	protected $page;
	protected $protoslug;
	protected $protoname;
	protected $protoimplement;
	protected $action;
	protected $dbtable;
	protected $uitable;
	protected $noreg_pasien;
	protected $nama_pasien;
	protected $nrm_pasien;
	protected $alamat;
	protected $umur;
	protected $jk;
	protected $ruang_asal;
	protected $id_antrian;
	public function __construct($db, $polislug = "all", $noreg = "", $nrm = "", $nama = "", $alamat = "", $umur = "", $jk = "0", $id_antrian = "0", $ruang_asal = "", $page = "medical_record", $action = "lap_gigimulut", $protoslug = "", $protoname = "", $protoimplement = "") {
		$this->db = $db;
		$this->noreg_pasien = $noreg;
		$this->id_antrian = $id_antrian;
		
		$this->nama_pasien = $nama;
		$this->nrm_pasien = $nrm;
		$this->alamat = $alamat;
		$this->umur = $umur;
		$this->jk = $jk;
		$this->ruang_asal = $ruang_asal;
		$this->polislug = $polislug;
		$this->dbtable = new DBTable ( $this->db, "smis_mr_pkhusus" );
		$this->page = $page;
		$this->protoslug = $protoslug;
		$this->protoimplement = $protoimplement;
		$this->protoname = $protoname;
		$this->action = $action;
		if ($polislug != "all")
			$this->dbtable->addCustomKriteria ( "ruangan", "='" . $polislug . "'" );
		if ($noreg != "")
			$this->dbtable->addCustomKriteria ( "noreg_pasien", "='" . $noreg . "'" );
		$head=array ('No.', 'Tanggal', 'NRM', "No. Reg.", "Pasien", "Jenis Layanan" );
		$this->uitable = new Table ($head , " Indikator " . ($this->polislug == "all" ? "" : ucfirst ( $this->protoname )), NULL, true );
		$this->uitable->setName ( $action );
		$this->uitable->setPrintButtonEnable(false);
		$this->uitable->setReloadButtonEnable(false);
	}	
	
	public function command($command) {
		$adapter = new SimpleAdapter (true, "No.");
		$adapter->add ( "Tanggal", "tanggal", "date d-m-Y" );
		$adapter->add ( "No. Reg.", "noreg_pasien" );
		$adapter->add ( "NRM", "nrm_pasien", "digit6" );
		$adapter->add ( "Pasien", "nama_pasien" );
		$adapter->add ( "Jenis Layanan", "layanan_khusus" );
		$dbres = new DBResponder ( $this->dbtable, $this->uitable, $adapter );
		$data = $dbres->command ( $_POST ['command'] );
		echo json_encode ( $data );
		return;
	}
	public function phpPreLoad() {
		$this->uitable->addModal ( "nama_pasien", "hidden", "", $this->nama_pasien, "n", null, true );
		$this->uitable->addModal ( "nrm_pasien", "hidden", "", $this->nrm_pasien, "n", null, true );
		$this->uitable->addModal ( "noreg_pasien", "hidden", "", $this->noreg_pasien, "n", null, true );
		$this->uitable->addModal ( "alamat", "hidden", "", $this->alamat, "n", null, true );
		$this->uitable->addModal ( "jk", "hidden", "", $this->jk, "n", null, true );
		$this->uitable->addModal ( "umur", "hidden", "", $this->umur, "n", null, true );
		$this->uitable->addModal ( "ruang_asal", "hidden", "", $this->ruang_asal, "n", null, true );
		$this->uitable->addModal ( "ruangan", "hidden", "", $this->polislug, "n", null, true );
		$this->uitable->addModal ( "id", "hidden", "", "" );
		$this->uitable->addModal ( "tanggal", "date", "Tanggal",date("Y-m-d"),"y",null,false,null,false,"layanan_khusus");
		$option = new OptionBuilder();
		$option->addSingle("Elektro Kardiographi (EKG)");
		$option->addSingle("Elektro Myographi (EMG)");
		$option->addSingle("Echo Cardiographi (ECG)");
		$option->addSingle("Endoskopi");
		$option->addSingle("Hemodialisa");
		$option->addSingle("Densometri Tulang");
		$option->addSingle("Pungsi");
		$option->addSingle("Spirometri");
		$option->addSingle("Tes Kulit/Alergi/Histamin");
		$option->addSingle("Topometri");
		$option->addSingle("Akupuntur");
		$option->addSingle("Hiperbarik");
		$option->addSingle("Herbal / jamu");
		$option->addSingle("Lain-lain");
		$this->uitable->addModal ( "layanan_khusus", "select", "Jenis Layanan", $option->getContent(),"y",null,false,null,false,"save");
		
		$modal = $this->uitable->getModal();
		$modal->setTitle ( "Laporan RL 3.10 - Pelayanan Khusus" );

		echo $modal->getHtml ();
		echo $this->uitable->getHtml ();
	}
	public function jsLoader() {
		echo addJS ( "framework/bootstrap/js/bootstrap-datepicker.js" );
		echo addJS ( "framework/smis/js/table_action.js" );
		loadLibrary ( "smis-libs-function-javascript" );
	}
	
	public function cssLoader() {
		echo addCSS ( "framework/bootstrap/css/datepicker.css" );
	}
	
	public function cssPreLoad() {
		?>
		<style type="text/css">
				#<?php echo $this->action;?>_add_form_form>div {clear: both; width: 100%;}
				#<?php echo $this->action; ?>_add_form_form>div label { width: 250px; }
		</style>
		<?php
	}
	
	/* when it's star build */
	public function jsPreLoad() {
		?>
		<script type="text/javascript">
		var <?php echo $this->action; ?>;
		var rl310_pasien;
		var rl310_noreg="<?php echo $this->noreg_pasien; ?>";
		var rl310_nama_pasien="<?php echo $this->nama_pasien; ?>";
		var rl310_nrm_pasien="<?php echo $this->nrm_pasien; ?>";
		var rl310_polislug="<?php echo $this->polislug; ?>";
		var rl310_the_page="<?php echo $this->page; ?>";
		var rl310_the_protoslug="<?php echo $this->protoslug; ?>";
		var rl310_the_protoname="<?php echo $this->protoname; ?>";
		var rl310_the_protoimplement="<?php echo $this->protoimplement; ?>";
		var rl310_id_antrian="<?php echo $this->id_antrian; ?>";
		var rl310_action="<?php echo $this->action; ?>";
		$(document).ready(function() {
			$('.mydate').datepicker();
			rl310_pasien=new TableAction("rl310_pasien",rl310_the_page,rl310_action,new Array());
			rl310_pasien.setSuperCommand("rl310_pasien");
			rl310_pasien.setPrototipe(rl310_the_protoname,rl310_the_protoslug,rl310_the_protoimplement);
			rl310_pasien.selected=function(json){
				var nama=json.nama_pasien;
				var nrm=json.nrm;
				var noreg=json.id;		
				$("#"+rl310_action+"_nama_pasien").val(nama);
				$("#"+rl310_action+"_nrm_pasien").val(nrm);
				$("#"+rl310_action+"_noreg_pasien").val(noreg);
			};
			
			var column=new Array("id","tanggal","ruangan","layanan_khusus");
			<?php echo $this->action; ?>=new TableAction("<?php echo $this->action; ?>",rl310_the_page,"<?php echo $this->action; ?>",column);
			<?php echo $this->action; ?>.setPrototipe(rl310_the_protoname,rl310_the_protoslug,rl310_the_protoimplement);
			<?php echo $this->action; ?>.getRegulerData=function(){
				var reg_data={	
						page:this.page,
						action:this.action,
						super_command:this.super_command,
						prototype_name:this.prototype_name,
						prototype_slug:this.prototype_slug,
						prototype_implement:this.prototype_implement,
						polislug:rl310_polislug,
						noreg_pasien:rl310_noreg,
						nama_pasien:rl310_nama_pasien,
						nrm_pasien:rl310_nrm_pasien,
						id_antrian:rl310_id_antrian
						};
				return reg_data;
			};

			<?php echo $this->action; ?>.setNextEnter();
			<?php echo $this->action; ?>.setEnableAutofocus(true);
			<?php echo $this->action; ?>.view();
		});
		</script>
<?php
	}
	public function superCommand($super_command) {
		/* PASIEN */
		$super = new SuperCommand ();
		
		$phead=array ('Nama','NRM',"No Reg" );
		$ptable = new Table ( $phead, "", NULL, true );
		$ptable->setName ( "rl310_pasien" );
		$ptable->setModel ( Table::$SELECT );
		$padapter = new SimpleAdapter ();
		$padapter->add ( "Nama", "nama_pasien" );
		$padapter->add ( "NRM", "nrm", "digit8" );
		$padapter->add ( "No Reg", "id" );
		$presponder = new ServiceResponder ( $this->db, $ptable, $padapter, "get_registered" );		
		$super->addResponder ( "rl310_pasien", $presponder );
		$init = $super->initialize ();
		if ($init != null) {
			echo $init;
			return;
		}
	}
}

?>