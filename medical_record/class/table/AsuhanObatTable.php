<?php 

class AsuhanObatTable extends Table{
	private $polislug;
	private $noreg;
	public function __construct($polislug,$noreg,$header,$title="",$content=NULL,$action=true){
		parent::__construct($header,$title,$content,$action);
		$this->polislug=$polislug;
		$this->noreg=$noreg;
	}
	

	public function getContentButton($id){
		$btngrup=null;
		if($this->current_data['Ruangan']==$this->polislug && $this->current_data['Noreg']*1==$this->noreg){
			$this->setDelButtonEnable(true);
			$btngrup=parent::getContentButton($id);
		}else{
			$this->setDelButtonEnable(false);
			$btngrup=parent::getContentButton($id);
		}
		return $btngrup;
	}
	
}

?>