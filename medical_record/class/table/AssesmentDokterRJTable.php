<?php 

class AssesmentDokterRJTable extends Table{
    private $id_dokter;
    private $nama_dokter;
    private $noreg_pasien;
    public function setDokter($id_dokter,$nama_dokter){
        $this->id_dokter=$id_dokter;
        $this->nama_dokter=$id_dokter;
        return $this;
    }
    
    public function setNoReg($noreg_pasien){
        $this->noreg_pasien=$noreg_pasien;
        return $this;
    }
    
    public function getContentButton($id){
        $df_del = $this->delete_button_enable;
        $df_edi = $this->edit_button_enable;        
        if( $this->current_data['id_dokter']==$this->id_dokter &&  $this->current_data['No. Reg']==$this->noreg_pasien ){
            $this->setDelButtonEnable(true);
            $this->setEditButtonEnable(true);
        }else{
            $this->setDelButtonEnable($df_del);
            $this->setEditButtonEnable($df_edi);
        }
        $result = parent::getContentButton($id);
        $this->delete_button_enable = $df_del;
        $this->edit_button_enable   = $df_edi;        
        return $result;
    }
}

?>