<?php 

class DiagnosaTable extends Table{
	private $polislug;
	private $noreg;
    private $enabled_dell;
	public function __construct($polislug,$noreg,$header,$title="",$content=NULL,$action=true){
		parent::__construct($header,$title,$content,$action);
		$this->polislug=$polislug;
		$this->noreg=$noreg;
        $this->enabled_dell=false;
	}
	
    public function setEnableDellInAllArea($dell){
        $this->enabled_dell=$dell;
        return $this;
    }

	public function getContentButton($id){
		$btngrup=null;
		if($this->enabled_dell || $this->current_data['Ruangan']==$this->polislug && $this->current_data['No.Reg']*1==$this->noreg){
			$this->setDelButtonEnable(true);
			$btngrup=parent::getContentButton($id);
		}else{
			$this->setDelButtonEnable(false);
			$btngrup=parent::getContentButton($id);
		}
		$btngrup->add("show", "Show", "", $this->name.".show('".$id."')", "btn-primary", "","fa fa-eye");
		return $btngrup;
	}
	
}

?>