<?php 

/**
 * 
 * this provided manual for get_riwayat_obat service
 * 
 * @version 1.0
 * @since 16 Nov 2014
 * @author goblooge
 * @license LGPL v3
 * @copyright Nurul Huda <goblooge@gmail.com>
 * 
 */

require_once 'smis-libs-class/ManualService.php';

$response=array();
$response['page']="0"; 									// always 0
$response['max_page']="0"; 								// always 0.
$response['data']=array(); 								// array of string
$response['data']["0"]=array();							// array of string number 0
$response['data']["0"]["Racikan"]="Racikan 1";			// put racikan name
$response['data']["0"]["Obat"]="Bodrexin";				// Drugs Name
$response['data']["0"]["Jumlah"]="1 Butir";				// Drugs Name
$response['data']["1"]=array();							// array of string number 1 
$response['data']["1"]["Racikan"]="Racikan 1";			// Racikan Name 
$response['data']["1"]["Obat"]="Betadine";				// Drugs name
$response['data']["1"]["Jumlah"]="2 Butir";				// Drugs Name
$response['data']["2"]=array();							// array of string number 2
$response['data']["2"]["Racikan"]="-";					// Racikan Name if not racikan then left with -
$response['data']["2"]["Obat"]="Oskadon";				// Drugs Name
$response['data']["2"]["Jumlah"]="4 Butir";				// Drugs Name

$provided=array();
$provided['noreg_pasien']="1";

$descrption="
This Service Consumer have several goal :
1. Provide Data Recipe of Drug of patient 
2. Data that provided including 'Racikan' Drug
3. No Count Needed
";

$author="
* @version 1.0
* @since 6 Jan 2015
* @author goblooge
* @license LGPL v3
* @copyright Nurul Huda &#60; goblooge@gmail.com &#62;
";

$man=new ManualService("get_riwayat_obat");
$man->setAuthor($author);
$man->setDataNeeded($response);
$man->setDataProvided($provided);
$man->setDescription($descrption);

echo $man->getHtml();



?>