<?php
	global $NAVIGATOR;
 
	// Administrator Top Menu
	$mr = new Menu ("fa fa-video-camera");
	$mr->addProperty ( 'title', 'Rekam Medis' );
	$mr->addProperty ( 'name', 'Rekam Medis' );
	$mr->addSubMenu ( "Data Induk", "medical_record", "data_induk", "Database","fa fa-database" );
	$mr->addSubMenu ( "Input Data", "medical_record", "input_data", "Input","fa fa-inbox" );
    $mr->addSubMenu ( "Laporan Assesmen", "medical_record", "laporan_asesmen", "Laporan Assesmen" ,"fa fa-file");
	$mr->addSubMenu ( "Laporan Indikator Klinis", "medical_record", "indikator_klinis", "Indikator Klinis","fa fa-plus-square-o" );
	$mr->addSubMenu ( "Rekapitulasi Laporan (RL)", "medical_record", "laporan_rl", "Rekapitulasi Laporan" ,"fa fa-file-o");
	$mr->addSubMenu ( "Laporan Index", "medical_record", "lap_index", "Rangkuman Index" ,"fa fa-exclamation");
	$mr->addSubMenu ( "Laporan KB, Persalinan & Imunisasi", "medical_record", "lap_kb", "Laporan KB" ,"fa fa-female");
	$mr->addSubMenu ( "Laporan IGD", "medical_record", "lap_igd", "Laporan Pasien IGD" ,"fa fa-warning");
	$mr->addSubMenu ( "Laporan Operasi", "medical_record", "lap_operasi", "Laporan Kamar Operasi" ,"fa fa-scissors");
	$mr->addSubMenu ( "Laporan Pasien", "medical_record", "laporan_pasien", "Laporan Lain untuk Pasien" ,"fa fa-book");
	$mr->addSubMenu ( "Laporan Sensus Harian Rawat Inap", "medical_record", "laporan_sensus_rawat_inap", "Laporan Sensus Harian Rawat Inap" ,"fa fa-file");
	$mr->addSubMenu ( "Rekap. Sensus Harian Rawat Inap", "medical_record", "laporan_bulanan_sensus_rawat_inap", "Rekap. Sensus Harian Rawat Inap" ,"fa fa-file");
    $mr->addSubMenu ( "Laporan Index Pasien", "medical_record", "laporan_rm", "Laporan Index Pasien" ,"fa fa-file");
    $mr->addSubMenu ( "Laporan Kunjungan", "medical_record", "laporan_kunjungan", "Laporan Kunjungan" ,"fa fa-users");
	$mr->addSubMenu ( "Laporan Pasien Pulang", "medical_record", "lap_pasien_pulang", "Laporan Pasien Pulang" ,"fa fa-sign-out");
	$mr->addSubMenu ( "10 Besar Penyakit", "medical_record", "sepuluh_besar_penyakit", "10 Besar Penyakit" ,"fa fa-medkit");
    $mr->addSubMenu ( "Laporan PPI", "medical_record", "laporan_ppi", "Laporan PPI" ,"fa fa-file");
	$mr->addSubMenu ( "Efisiensi Mutu", "medical_record", "efisiensi_mutu", "Efisiensi Mutu" ,"fa fa-file");
	$mr->addSubMenu ( "Kuisioner", "medical_record", "kuisioner", "Kuisioner" ,"fa fa-question");	
	$mr->addSubMenu ( "Resume Medis", "medical_record", "resume_medis", "Sejarah Pasien" ,"fa fa-file");
	$mr->addSubMenu ( "Resume Diagnosa Pasien", "medical_record", "resume_diagnosa_pasien", "Riwayat Diagnosa Pasien", "fa fa-file");
	$mr->addSubMenu ( "Pengaturan", "medical_record", "settings", "Pengaturan" ,"fa fa-cog");
	$mr->addSubMenu ( "Asesment Dokter Rawat Jalan", "medical_record", "assesment_dokter_jalan", "Asesmen Dokter Rawat Jalan" ,"fa fa-user-md");
	$mr->addSubMenu ( "Asesment Dokter Rawat Inap", "medical_record", "assesment_dokter_inap", "Asesmen Dokter Rawat Inap" ,"fa fa-user-md");
	$mr->addSubMenu ( "Asesment Keperawatan", "medical_record", "assesment_keperawatan", "Asesmen Keperawatan" ,"fa fa-medkit");
	$mr->addSubMenu ( "Setting Assesment Dokter", "medical_record", "settings_assesment_dokter", "Settings Asesmen Rawat Jalan" ,"fa fa-cogs");
	
	require_once 'smis-libs-inventory/navigation.php';
	$inventory_navigator = new InventoryNavigator($NAVIGATOR, "", "Rekam Medis", "medical_record");
	$mr = $inventory_navigator->extendMenu($mr);
	$NAVIGATOR->addMenu ( $mr, 'medical_record' );
?>
