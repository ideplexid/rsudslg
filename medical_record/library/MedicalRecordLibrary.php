<?php
	class MedicalRecordLibrary {
		public static function getDataDiagnosaSebabLuar() {
			$data_diagnosa = array();
			$data_diagnosa[] = array(
				"dtd" => "299.0",
				"kode_icd" => "V 01 - V 8",
				"sebab" => "Kecelakaan angkutan darat"
			);
			$data_diagnosa[] = array(
				"dtd" => "299.1",
				"kode_icd" => "V 90 - V 9",
				"sebab" => "Kecelakaan angkutan air"
			);
			$data_diagnosa[] = array(
				"dtd" => "299.2",
				"kode_icd" => "V 95 - V 9",
				"sebab" => "Kecelakaan angkutan udara dan ruang angkasa"
			);
			$data_diagnosa[] = array(
				"dtd" => "299.9",
				"kode_icd" => "V 98 -V 99",
				"sebab" => "Kecelakaan angkutan lain"
			);
			$data_diagnosa[] = array(
				"dtd" => "300",
				"kode_icd" => "W 00 - W 1",
				"sebab" => "Jatuh"
			);
			$data_diagnosa[] = array(
				"dtd" => "301",
				"kode_icd" => "W 65 - W 7",
				"sebab" => "Kecelakaan tenggelam dan terbenam"
			);
			$data_diagnosa[] = array(
				"dtd" => "302",
				"kode_icd" => "X 00 - X 0",
				"sebab" => "Terdedah asap, api dan uap"
			);
			$data_diagnosa[] = array(
				"dtd" => "303 .0",
				"kode_icd" => "X 45",
				"sebab" => "Keracunan akibat pemaparan alkohol"
			);
			$data_diagnosa[] = array(
				"dtd" => "303 .1",
				"kode_icd" => "X 46",
				"sebab" => "Keracunan akibat pemaparan pelarut organik & hidro"
			);
			$data_diagnosa[] = array(
				"dtd" => "303 .2",
				"kode_icd" => "X 47",
				"sebab" => "Keracunan akibat pemaparan gas-gas & uap-uap lainn"
			);
			$data_diagnosa[] = array(
				"dtd" => "303 .3",
				"kode_icd" => "X 48",
				"sebab" => "Keracunan akibat pemaparan pestisida"
			);
			$data_diagnosa[] = array(
				"dtd" => "303. 4",
				"kode_icd" => "X 49",
				"sebab" => "Keracunan akibat pemaparan bahan beracun berbahaya"
			);
			$data_diagnosa[] = array(
				"dtd" => "303 .9",
				"kode_icd" => "X 40 - X 4",
				"sebab" => "Kecelakaan keracunan dan terdedah oleh bahan berac"
			);
			$data_diagnosa[] = array(
				"dtd" => "304.0",
				"kode_icd" => "X 60 - X 6",
				"sebab" => "Sengaja mencederai diri dengan bahan beracun"
			);
			$data_diagnosa[] = array(
				"dtd" => "304.9",
				"kode_icd" => "X 70 - X 8",
				"sebab" => "Sengaja mencederai diri lainnya"
			);
			$data_diagnosa[] = array(
				"dtd" => "305",
				"kode_icd" => "X 85 - Y 0",
				"sebab" => "Dicederai"
			);
			$data_diagnosa[] = array(
				"dtd" => "306.0",
				"kode_icd" => "X 10 - X 1",
				"sebab" => "Kontak dengan bahan panas"
			);
			$data_diagnosa[] = array(
				"dtd" => "306.1",
				"kode_icd" => "X 20 - X 2",
				"sebab" => "Kontak dengan binatang & tumbuhan beracun"
			);
			$data_diagnosa[] = array(
				"dtd" => "306.2",
				"kode_icd" => "X 30 - X 3",
				"sebab" => "Terdedah faktor alam"
			);
			$data_diagnosa[] = array(
				"dtd" => "306 .3",
				"kode_icd" => "Y 40 - Y 5",
				"sebab" => "Efeksamping pengguna obat, bahan obat dan bahan bi"
			);
			$data_diagnosa[] = array(
				"dtd" => "306 .4",
				"kode_icd" => "Y 60 - Y 8",
				"sebab" => "Kesalahan pada pasien selama perawatan medis non b"
			);
			$data_diagnosa[] = array(
				"dtd" => "306.5",
				"kode_icd" => "W 42",
				"sebab" => "Pemaparan bising"
			);
			$data_diagnosa[] = array(
				"dtd" => "306 .6",
				"kode_icd" => "W 43",
				"sebab" => "Pemaparan getaran"
			);
			$data_diagnosa[] = array(
				"dtd" => "306 .7",
				"kode_icd" => "W 88",
				"sebab" => "Pemaparan radiasi pengion"
			);
			$data_diagnosa[] = array(
				"dtd" => "306 .8",
				"kode_icd" => "W 89",
				"sebab" => "Pemaparan sinar ultra violet dan man-mide visible"
			);
			$data_diagnosa[] = array(
				"dtd" => "306 .9",
				"kode_icd" => "W 90",
				"sebab" => "Pemaparan radiasi pengion lain"
			);
			$data_diagnosa[] = array(
				"dtd" => "306 .10",
				"kode_icd" => "W 91",
				"sebab" => "Pemaparan radiasi YTT"
			);
			$data_diagnosa[] = array(
				"dtd" => "306 .11",
				"kode_icd" => "X 50",
				"sebab" => "Gangguan gerakan berulang-ulang dengan kekuatan b"
			);
			$data_diagnosa[] = array(
				"dtd" => "306 .12",
				"kode_icd" => "X 96",
				"sebab" => "Gangguan kesehatan yang berhubungan dengan kesehat"
			);
			$data_diagnosa[] = array(
				"dtd" => "306 .13",
				"kode_icd" => "W20-W41, W",
				"sebab" => "Sebab luar lainnya"
			);
			return $data_diagnosa;
		}
		
		public static function getDataDiagnosaNonSebabLuar() {
			$data_diagnosa = array();
			$data_diagnosa[] = array(
			    "dtd" => "001",
			    "kode_icd" => "A00-A00.9",
			    "sebab" => "Kolera"
			);
			$data_diagnosa[] = array(
			    "dtd" => "002",
			    "kode_icd" => "A01",
			    "sebab" => "Demam tifoid dan paratifoid"
			);
			$data_diagnosa[] = array(
			    "dtd" => "003",
			    "kode_icd" => "A03",
			    "sebab" => "Sigelosis"
			);
			$data_diagnosa[] = array(
			    "dtd" => "004.0",
			    "kode_icd" => "A06.4",
			    "sebab" => "Abses hati amuba"
			);
			$data_diagnosa[] = array(
			    "dtd" => "004.9",
			    "kode_icd" => "A06.0–3.5-9",
			    "sebab" => "Amebiasis lainnya"
			);
			$data_diagnosa[] = array(
			    "dtd" => "005",
			    "kode_icd" => "A09",
			    "sebab" => "Diare & gastroenteritis oleh penyebab Infeksi tertentu (kolitis infeksi)"
			);
			$data_diagnosa[] = array(
			    "dtd" => "006",
			    "kode_icd" => "A02. A04-A05. A07-A08",
			    "sebab" => "Penyakit infeksi usus lainnya"
			);
			$data_diagnosa[] = array(
			    "dtd" => "007.0",
			    "kode_icd" => "A15.0",
			    "sebab" => "Tuberkulosis (TB) paru BTA (+) dengan/tanpa tindakan kuman TB"
			);
			$data_diagnosa[] = array(
			    "dtd" => "007.1",
			    "kode_icd" => "A15.1-A16.2",
			    "sebab" => "Tuberkulosis paru lainnya"
			);
			$data_diagnosa[] = array(
			    "dtd" => "007.9",
			    "kode_icd" => "A16.3-9",
			    "sebab" => "Tuberkulosis alat napas lainnya"
			);
			$data_diagnosa[] = array(
			    "dtd" => "008.0",
			    "kode_icd" => "A17.0",
			    "sebab" => "Meningitis tuberkulosa"
			);
			$data_diagnosa[] = array(
			    "dtd" => "008.1",
			    "kode_icd" => "A17.1-7",
			    "sebab" => "Tuberkulosis susunan saraf pusat lainnya"
			);
			$data_diagnosa[] = array(
			    "dtd" => "008.2",
			    "kode_icd" => "A18.0",
			    "sebab" => "Tuberkulosis tulang dan sensi"
			);
			$data_diagnosa[] = array(
			    "dtd" => "008.3",
			    "kode_icd" => "A18.2",
			    "sebab" => "Limfadenitis tuberkulosa"
			);
			$data_diagnosa[] = array(
			    "dtd" => "008.4",
			    "kode_icd" => "A19",
			    "sebab" => "Tuberkulosis milier"
			);
			$data_diagnosa[] = array(
			    "dtd" => "008.9",
			    "kode_icd" => "A18.1.3-8",
			    "sebab" => "Tuberkulosis lainnya"
			);
			$data_diagnosa[] = array(
			    "dtd" => "009",
			    "kode_icd" => "A20",
			    "sebab" => "Sampar/Pes"
			);
			$data_diagnosa[] = array(
			    "dtd" => "010",
			    "kode_icd" => "A23",
			    "sebab" => "Bruselosis"
			);
			$data_diagnosa[] = array(
			    "dtd" => "011",
			    "kode_icd" => "A30",
			    "sebab" => "Lepra/Kusta"
			);
			$data_diagnosa[] = array(
			    "dtd" => "012",
			    "kode_icd" => "A33",
			    "sebab" => "Tetanus neonatorum"
			);
			$data_diagnosa[] = array(
			    "dtd" => "013",
			    "kode_icd" => "A34-A35",
			    "sebab" => "Tetanus lainnya"
			);
			$data_diagnosa[] = array(
			    "dtd" => "014",
			    "kode_icd" => "A36",
			    "sebab" => "Difteria"
			);
			$data_diagnosa[] = array(
			    "dtd" => "015",
			    "kode_icd" => "A37",
			    "sebab" => "Pertusis/Batuk rejan"
			);
			$data_diagnosa[] = array(
			    "dtd" => "016",
			    "kode_icd" => "A39",
			    "sebab" => "Infeksi meningokok"
			);
			$data_diagnosa[] = array(
			    "dtd" => "017",
			    "kode_icd" => "A40-A41",
			    "sebab" => "Septisemia"
			);
			$data_diagnosa[] = array(
			    "dtd" => "018.0",
			    "kode_icd" => "A22",
			    "sebab" => "Antrak"
			);
			$data_diagnosa[] = array(
			    "dtd" => "018.9",
			    "kode_icd" => "A21.24-28. A31-32, 38-42-49",
			    "sebab" => "Penyakit bakteri lainnya"
			);
			$data_diagnosa[] = array(
			    "dtd" => "019",
			    "kode_icd" => "A50",
			    "sebab" => "Sifilis bawaan"
			);
			$data_diagnosa[] = array(
			    "dtd" => "020",
			    "kode_icd" => "A51",
			    "sebab" => "Sifilis dini"
			);
			$data_diagnosa[] = array(
			    "dtd" => "021",
			    "kode_icd" => "A52-A53",
			    "sebab" => "Sifilis lainnya"
			);
			$data_diagnosa[] = array(
			    "dtd" => "022",
			    "kode_icd" => "A54",
			    "sebab" => "Infeksi gonokok"
			);
			$data_diagnosa[] = array(
			    "dtd" => "023",
			    "kode_icd" => "A55-A56",
			    "sebab" => "Penyakit klamidia yg ditularkan melalui Hubungan seksual"
			);
			$data_diagnosa[] = array(
			    "dtd" => "024",
			    "kode_icd" => "A57-A64",
			    "sebab" => "Infeksi lainnya yang terutama ditularkan Melalui hubungan seksual"
			);
			$data_diagnosa[] = array(
			    "dtd" => "025",
			    "kode_icd" => "A68",
			    "sebab" => "Demam bolak balik"
			);
			$data_diagnosa[] = array(
			    "dtd" => "026",
			    "kode_icd" => "A71",
			    "sebab" => "Trakoma"
			);
			$data_diagnosa[] = array(
			    "dtd" => "027",
			    "kode_icd" => "A75",
			    "sebab" => "Demam tifus"
			);
			$data_diagnosa[] = array(
			    "dtd" => "028",
			    "kode_icd" => "A80",
			    "sebab" => "Poliomielitis akut"
			);
			$data_diagnosa[] = array(
			    "dtd" => "029",
			    "kode_icd" => "A82",
			    "sebab" => "Rabies"
			);
			$data_diagnosa[] = array(
			    "dtd" => "030",
			    "kode_icd" => "A83-A86",
			    "sebab" => "Ensefalitis virus"
			);
			$data_diagnosa[] = array(
			    "dtd" => "031",
			    "kode_icd" => "A95",
			    "sebab" => "Demam kuning"
			);
			$data_diagnosa[] = array(
			    "dtd" => "032.0",
			    "kode_icd" => "A90",
			    "sebab" => "Demam dengue"
			);
			$data_diagnosa[] = array(
			    "dtd" => "032.1",
			    "kode_icd" => "A91",
			    "sebab" => "Demam berdarah dengue"
			);
			$data_diagnosa[] = array(
			    "dtd" => "032.2",
			    "kode_icd" => "A92.0",
			    "sebab" => "Chikungunya"
			);
			$data_diagnosa[] = array(
			    "dtd" => "032.3",
			    "kode_icd" => "A92.1-A92",
			    "sebab" => "Demam virus tular nyamuk"
			);
			$data_diagnosa[] = array(
			    "dtd" => "032.9",
			    "kode_icd" => "A93-A94. A96-A99",
			    "sebab" => "Demam virus dan demam berdarah virus tular Serangga lainnya"
			);
			$data_diagnosa[] = array(
			    "dtd" => "033",
			    "kode_icd" => "B00",
			    "sebab" => "Infeksi herpesvirus (Herpes simpleks)"
			);
			$data_diagnosa[] = array(
			    "dtd" => "034",
			    "kode_icd" => "B01-B02",
			    "sebab" => "Varisela (cacar air) dan zoster (herpes zoster)"
			);
			$data_diagnosa[] = array(
			    "dtd" => "035",
			    "kode_icd" => "B05",
			    "sebab" => "Campak"
			);
			$data_diagnosa[] = array(
			    "dtd" => "036",
			    "kode_icd" => "B06",
			    "sebab" => "Rubela"
			);
			$data_diagnosa[] = array(
			    "dtd" => "037",
			    "kode_icd" => "B16",
			    "sebab" => "Hepatitis B akut"
			);
			$data_diagnosa[] = array(
			    "dtd" => "038.0",
			    "kode_icd" => "B15",
			    "sebab" => "Hepatitis A akut"
			);
			$data_diagnosa[] = array(
			    "dtd" => "038.1",
			    "kode_icd" => "B17.1",
			    "sebab" => "Hepatitis C akut"
			);
			$data_diagnosa[] = array(
			    "dtd" => "038.2",
			    "kode_icd" => "B17.2",
			    "sebab" => "Hepatitis E akut"
			);
			$data_diagnosa[] = array(
			    "dtd" => "038.9",
			    "kode_icd" => "B17.0.8 B18-B19",
			    "sebab" => "Hetitis virus lainnya"
			);
			$data_diagnosa[] = array(
			    "dtd" => "039",
			    "kode_icd" => "B20-B24",
			    "sebab" => "Penyakit virus gangguan defisiensi imun Pada manusia (HIV)"
			);
			$data_diagnosa[] = array(
			    "dtd" => "040",
			    "kode_icd" => "B26",
			    "sebab" => "Gondong"
			);
			$data_diagnosa[] = array(
			    "dtd" => "041",
			    "kode_icd" => "A81. A87-A89. B03-B04. B07-B09. B25. B27-B34",
			    "sebab" => "Penyakit virus lainnya"
			);
			$data_diagnosa[] = array(
			    "dtd" => "042",
			    "kode_icd" => "B35-B49",
			    "sebab" => "Mikosis"
			);
			$data_diagnosa[] = array(
			    "dtd" => "043",
			    "kode_icd" => "B50.0",
			    "sebab" => "Malaria cerebral NOS"
			);
			$data_diagnosa[] = array(
			    "dtd" => "043.0",
			    "kode_icd" => "B50.8-9",
			    "sebab" => "Malaria falciparum"
			);
			$data_diagnosa[] = array(
			    "dtd" => "043.1",
			    "kode_icd" => "B51.0",
			    "sebab" => "Malaria vivax"
			);
			$data_diagnosa[] = array(
			    "dtd" => "043.2",
			    "kode_icd" => "B51.8-9",
			    "sebab" => "Malaria vivax lainnya"
			);
			$data_diagnosa[] = array(
			    "dtd" => "043.3",
			    "kode_icd" => "B52.0",
			    "sebab" => "Malaria malariae"
			);
			$data_diagnosa[] = array(
			    "dtd" => "043.4",
			    "kode_icd" => "B52.8-9",
			    "sebab" => "Malaria malariae lainnya"
			);
			$data_diagnosa[] = array(
			    "dtd" => "043.5",
			    "kode_icd" => "B53",
			    "sebab" => "Malaria ovale"
			);
			$data_diagnosa[] = array(
			    "dtd" => "043.9",
			    "kode_icd" => "B54",
			    "sebab" => "Malaria YTT"
			);
			$data_diagnosa[] = array(
			    "dtd" => "044",
			    "kode_icd" => "B55",
			    "sebab" => "Lesmaniasis"
			);
			$data_diagnosa[] = array(
			    "dtd" => "045",
			    "kode_icd" => "B56-B57",
			    "sebab" => "Tripanosomiasis"
			);
			$data_diagnosa[] = array(
			    "dtd" => "046",
			    "kode_icd" => "B65",
			    "sebab" => "Skistosomiasis (Bilharziasis)"
			);
			$data_diagnosa[] = array(
			    "dtd" => "047",
			    "kode_icd" => "B66",
			    "sebab" => "Infeksi trematoda lainnya"
			);
			$data_diagnosa[] = array(
			    "dtd" => "048",
			    "kode_icd" => "B67",
			    "sebab" => "Ekinokokosis"
			);
			$data_diagnosa[] = array(
			    "dtd" => "049",
			    "kode_icd" => "B72",
			    "sebab" => "Drakunkuliasis"
			);
			$data_diagnosa[] = array(
			    "dtd" => "050",
			    "kode_icd" => "B73",
			    "sebab" => "Onkosersiasis"
			);
			$data_diagnosa[] = array(
			    "dtd" => "051",
			    "kode_icd" => "B74",
			    "sebab" => "Filariasis"
			);
			$data_diagnosa[] = array(
			    "dtd" => "052",
			    "kode_icd" => "B76",
			    "sebab" => "Penyakit cacing tambang"
			);
			$data_diagnosa[] = array(
			    "dtd" => "053",
			    "kode_icd" => "B68-B71, B75, B77-B83",
			    "sebab" => "Helmitiansis lain"
			);
			$data_diagnosa[] = array(
			    "dtd" => "054.0",
			    "kode_icd" => "B90.9.1",
			    "sebab" => "Patu/lobus luluh akibat TB"
			);
			$data_diagnosa[] = array(
			    "dtd" => "054.1",
			    "kode_icd" => "B90.9.2",
			    "sebab" => "Sindrom obstruksi pasca TB"
			);
			$data_diagnosa[] = array(
			    "dtd" => "054.9",
			    "kode_icd" => "B90.0-8",
			    "sebab" => "Sekuele (gejala sisa) TB lainnya"
			);
			$data_diagnosa[] = array(
			    "dtd" => "055",
			    "kode_icd" => "B91",
			    "sebab" => "Sekuele (gejala sisa) poliomielitis"
			);
			$data_diagnosa[] = array(
			    "dtd" => "056",
			    "kode_icd" => "B92",
			    "sebab" => "Sekuele (gejala sisa) lepra"
			);
			$data_diagnosa[] = array(
			    "dtd" => "057.0",
			    "kode_icd" => "A66",
			    "sebab" => "Patek (Frambusia)"
			);
			$data_diagnosa[] = array(
			    "dtd" => "057.1",
			    "kode_icd" => "A70",
			    "sebab" => "Infeksi Klamedia"
			);
			$data_diagnosa[] = array(
			    "dtd" => "057.2",
			    "kode_icd" => "B58 ",
			    "sebab" => "Toksoplasmosis"
			);
			$data_diagnosa[] = array(
			    "dtd" => "057.9",
			    "kode_icd" => "A65. A67. A69-A70. A74. A77-A79. B59-64.85-89.94-99",
			    "sebab" => "Penyakit infeksi dan parasit lainnya"
			);
			$data_diagnosa[] = array(
			    "dtd" => "058.0",
			    "kode_icd" => "C00-C10",
			    "sebab" => "Neoplasma ganas bibir, rongga mulut,Kelenjar liur, faring, tonsil"
			);
			$data_diagnosa[] = array(
			    "dtd" => "058.1",
			    "kode_icd" => "C11",
			    "sebab" => "Neoplasma ganas nosofaring"
			);
			$data_diagnosa[] = array(
			    "dtd" => "058.9",
			    "kode_icd" => "C12-C14",
			    "sebab" => "Neoplasma ganas bibir, rongga mulut,Faring, lainnya & YTT"
			);
			$data_diagnosa[] = array(
			    "dtd" => "059",
			    "kode_icd" => "C15",
			    "sebab" => "Neoplasma ganas esofagus"
			);
			$data_diagnosa[] = array(
			    "dtd" => "060",
			    "kode_icd" => "C16",
			    "sebab" => "Neoplasma ganas lambung"
			);
			$data_diagnosa[] = array(
			    "dtd" => "061",
			    "kode_icd" => "C18",
			    "sebab" => "Neoplasma ganas kolon"
			);
			$data_diagnosa[] = array(
			    "dtd" => "062",
			    "kode_icd" => "C19-C21",
			    "sebab" => "Neoplasma ganas daerah rektosigmoid, Rektum dan anus"
			);
			$data_diagnosa[] = array(
			    "dtd" => "063",
			    "kode_icd" => "C22",
			    "sebab" => "Neoplasma ganas hati dan saluran empedu Intrahepatik"
			);
			$data_diagnosa[] = array(
			    "dtd" => "064",
			    "kode_icd" => "C25",
			    "sebab" => "Neoplasma ganas pankreas"
			);
			$data_diagnosa[] = array(
			    "dtd" => "065",
			    "kode_icd" => "C17.C23-C24.C26",
			    "sebab" => "Neoplasma ganas usus halus dan alat cerna lainnya"
			);
			$data_diagnosa[] = array(
			    "dtd" => "066",
			    "kode_icd" => "C32",
			    "sebab" => "Neoplasma laring"
			);
			$data_diagnosa[] = array(
			    "dtd" => "067.0",
			    "kode_icd" => "C33",
			    "sebab" => "Neoplasma ganas trakea"
			);
			$data_diagnosa[] = array(
			    "dtd" => "067.9",
			    "kode_icd" => "C34",
			    "sebab" => "Neoplasma ganas bronkus dan paru"
			);
			$data_diagnosa[] = array(
			    "dtd" => "068.0",
			    "kode_icd" => "C38.1-8",
			    "sebab" => "Neoplasma ganas mediastinum"
			);
			$data_diagnosa[] = array(
			    "dtd" => "068.9",
			    "kode_icd" => "C30.C3. C37-C38.0 C39",
			    "sebab" => "Neoplasma ganas sistem napas dan alat Rongga dada lainnya"
			);
			$data_diagnosa[] = array(
			    "dtd" => "069",
			    "kode_icd" => "C40-C41",
			    "sebab" => "Neoplasma ganas tulang dan tulang rawan sendi"
			);
			$data_diagnosa[] = array(
			    "dtd" => "070",
			    "kode_icd" => "C43",
			    "sebab" => "Melanoma ganas kulit"
			);
			$data_diagnosa[] = array(
			    "dtd" => "071",
			    "kode_icd" => "C44",
			    "sebab" => "Neoplasma ganas kulit lainnya"
			);
			$data_diagnosa[] = array(
			    "dtd" => "072.0",
			    "kode_icd" => "C45",
			    "sebab" => "Mesotelioma"
			);
			$data_diagnosa[] = array(
			    "dtd" => "072.9",
			    "kode_icd" => "C46-C49",
			    "sebab" => "Neoplasma ganas jaringan ikat & jaringan Lunak"
			);
			$data_diagnosa[] = array(
			    "dtd" => "073",
			    "kode_icd" => "C50",
			    "sebab" => "Neoplasma ganas payudara"
			);
			$data_diagnosa[] = array(
			    "dtd" => "074",
			    "kode_icd" => "C53",
			    "sebab" => "Neoplasma ganas serviks uterus"
			);
			$data_diagnosa[] = array(
			    "dtd" => "075.0",
			    "kode_icd" => "C54",
			    "sebab" => "Neoplasma ganas korpus uteri"
			);
			$data_diagnosa[] = array(
			    "dtd" => "075.9",
			    "kode_icd" => "C55",
			    "sebab" => "Neoplasma ganas bagian uterus lainnya Dan YTT"
			);
			$data_diagnosa[] = array(
			    "dtd" => "076.0",
			    "kode_icd" => "C56",
			    "sebab" => "Neoplasma ganas ovarium (indung telur)"
			);
			$data_diagnosa[] = array(
			    "dtd" => "076.1",
			    "kode_icd" => "C58",
			    "sebab" => "Neopalsma ganas plasenta (uri)"
			);
			$data_diagnosa[] = array(
			    "dtd" => "076.9",
			    "kode_icd" => "C51-C52.C57",
			    "sebab" => "Neoplasma ganas alat kelamin perempuan Lainnya"
			);
			$data_diagnosa[] = array(
			    "dtd" => "077",
			    "kode_icd" => "C61",
			    "sebab" => "Neopalsma ganas prostat"
			);
			$data_diagnosa[] = array(
			    "dtd" => "078.0",
			    "kode_icd" => "C60",
			    "sebab" => "Neopalsma ganas penis"
			);
			$data_diagnosa[] = array(
			    "dtd" => "078.1",
			    "kode_icd" => "C62",
			    "sebab" => "Neoplasma ganas testis"
			);
			$data_diagnosa[] = array(
			    "dtd" => "078.9",
			    "kode_icd" => "C63",
			    "sebab" => "Neoplasma ganas alat kelamin pria lainnya"
			);
			$data_diagnosa[] = array(
			    "dtd" => "079",
			    "kode_icd" => "C67",
			    "sebab" => "Neoplasma ganas kandung kemih (buli - buli)"
			);
			$data_diagnosa[] = array(
			    "dtd" => "080",
			    "kode_icd" => "C64-C65",
			    "sebab" => "Neoplasma ganas ginjal, pelvis ginjal"
			);
			$data_diagnosa[] = array(
			    "dtd" => "080.9",
			    "kode_icd" => "C66. C68",
			    "sebab" => "Neoplasma ganas alat kemih lainnya"
			);
			$data_diagnosa[] = array(
			    "dtd" => "081",
			    "kode_icd" => "C69",
			    "sebab" => "Neoplasma ganas mata dan adneksa"
			);
			$data_diagnosa[] = array(
			    "dtd" => "082",
			    "kode_icd" => "C71",
			    "sebab" => "Neoplasma ganas otak"
			);
			$data_diagnosa[] = array(
			    "dtd" => "083",
			    "kode_icd" => "C70, C72",
			    "sebab" => "Neoplasma ganas bagian susunan saraf pusat"
			);
			$data_diagnosa[] = array(
			    "dtd" => "084.0",
			    "kode_icd" => "C73",
			    "sebab" => "Neopalsma ganas kelenjar tiroid"
			);
			$data_diagnosa[] = array(
			    "dtd" => "084.1",
			    "kode_icd" => "C74-C75",
			    "sebab" => "Neoplasma ganas kelenjar endokrin lain dan struktur terkait"
			);
			$data_diagnosa[] = array(
			    "dtd" => "084.2",
			    "kode_icd" => "C76",
			    "sebab" => "Neoplasma ganas tempat lain dan yang tidak Jelas batasannya"
			);
			$data_diagnosa[] = array(
			    "dtd" => "084.3",
			    "kode_icd" => "C77-C80",
			    "sebab" => "Neoplasma ganas sekunder dan neoplasma Ganas kelenjar getah bening YTT"
			);
			$data_diagnosa[] = array(
			    "dtd" => "084.9",
			    "kode_icd" => "C97",
			    "sebab" => "Neoplasma ganas primer tempat multipel"
			);
			$data_diagnosa[] = array(
			    "dtd" => "085",
			    "kode_icd" => "C81",
			    "sebab" => "Penyakit hodgkin"
			);
			$data_diagnosa[] = array(
			    "dtd" => "086",
			    "kode_icd" => "C82-C85",
			    "sebab" => "Limfoma non hodgkin"
			);
			$data_diagnosa[] = array(
			    "dtd" => "087",
			    "kode_icd" => "C91-C95",
			    "sebab" => "Leukimia"
			);
			$data_diagnosa[] = array(
			    "dtd" => "088",
			    "kode_icd" => "C88-C90. C96",
			    "sebab" => "Neoplasma ganas lain dari limfoid Hematopoetik dan jaringan terkait lainnya"
			);
			$data_diagnosa[] = array(
			    "dtd" => "089",
			    "kode_icd" => "D06",
			    "sebab" => "Karsinoma in situ serviks uterus"
			);
			$data_diagnosa[] = array(
			    "dtd" => "090",
			    "kode_icd" => "D22-D23",
			    "sebab" => "Neoplasma jinak kulit"
			);
			$data_diagnosa[] = array(
			    "dtd" => "091",
			    "kode_icd" => "D24",
			    "sebab" => "Neoplasma jinak payudara"
			);
			$data_diagnosa[] = array(
			    "dtd" => "092",
			    "kode_icd" => "D25",
			    "sebab" => "Leiomioma uterus"
			);
			$data_diagnosa[] = array(
			    "dtd" => "093",
			    "kode_icd" => "D27",
			    "sebab" => "Neoplasma jinak ovarium (indung telur)"
			);
			$data_diagnosa[] = array(
			    "dtd" => "094",
			    "kode_icd" => "D30",
			    "sebab" => "Neoplasma jinak alat kemih"
			);
			$data_diagnosa[] = array(
			    "dtd" => "095",
			    "kode_icd" => "D33",
			    "sebab" => "Neoplasma jinak otak dan susunan saraf Pusat lainnya"
			);
			$data_diagnosa[] = array(
			    "dtd" => "096.0",
			    "kode_icd" => "D04",
			    "sebab" => "Karsinoma in situ kulit"
			);
			$data_diagnosa[] = array(
			    "dtd" => "096.1",
			    "kode_icd" => "D05",
			    "sebab" => "Karsinoma in situ payudara"
			);
			$data_diagnosa[] = array(
			    "dtd" => "096.2",
			    "kode_icd" => "D00-D03, D07-D09",
			    "sebab" => "Karsinoma"
			);
			$data_diagnosa[] = array(
			    "dtd" => "096.3",
			    "kode_icd" => "D12.6",
			    "sebab" => "Polip gastrointestinal"
			);
			$data_diagnosa[] = array(
			    "dtd" => "096.4",
			    "kode_icd" => "D14.1-4",
			    "sebab" => "Neoplasma jinak sistem napas lainnya"
			);
			$data_diagnosa[] = array(
			    "dtd" => "096.5",
			    "kode_icd" => "D15.2",
			    "sebab" => "Neoplasma jinak mediastinum"
			);
			$data_diagnosa[] = array(
			    "dtd" => "096.6",
			    "kode_icd" => "D10-D12.0-5.7-9. D13-D14.0. D15.0.1 D79-D12, D21. D26. D28-29, D31-32. D34-D36",
			    "sebab" => "Neoplasma jinak lainnya"
			);
			$data_diagnosa[] = array(
			    "dtd" => "096.9",
			    "kode_icd" => "D37-D48",
			    "sebab" => "Neoplasma yang tak menentu peragainya dan yang tak diketahui sifatnya"
			);
			$data_diagnosa[] = array(
			    "dtd" => "097",
			    "kode_icd" => "D50",
			    "sebab" => "Anemia defisiensi zat besi"
			);
			$data_diagnosa[] = array(
			    "dtd" => "098.0",
			    "kode_icd" => "D59",
			    "sebab" => "Anemia Hemolitik"
			);
			$data_diagnosa[] = array(
			    "dtd" => "098.1",
			    "kode_icd" => "D61",
			    "sebab" => "Anemia aplastik lainnya"
			);
			$data_diagnosa[] = array(
			    "dtd" => "098.9",
			    "kode_icd" => "D51-D58, D60. D62-D64",
			    "sebab" => "Anemia lainnya"
			);
			$data_diagnosa[] = array(
			    "dtd" => "099.0",
			    "kode_icd" => "D70",
			    "sebab" => "Agranulositosus"
			);
			$data_diagnosa[] = array(
			    "dtd" => "099.1",
			    "kode_icd" => "D74",
			    "sebab" => "Metahaemoglobinema"
			);
			$data_diagnosa[] = array(
			    "dtd" => "099.9",
			    "kode_icd" => "D65-D69, D71-D73, D75-D77",
			    "sebab" => "Kondisi hemoragik dan penyakit darah dan organ Pembuat darah lainnya"
			);
			$data_diagnosa[] = array(
			    "dtd" => "100",
			    "kode_icd" => "D80-D89",
			    "sebab" => "Penyakit tertentu yang menyangkut mekanisme"
			);
			$data_diagnosa[] = array(
			    "dtd" => "101",
			    "kode_icd" => "E00-E02",
			    "sebab" => "Gangguan tiroid berhubungan dengan Defisiensi iodium"
			);
			$data_diagnosa[] = array(
			    "dtd" => "102",
			    "kode_icd" => "E05",
			    "sebab" => "Tirotoksikosis (hipertiroidisme)"
			);
			$data_diagnosa[] = array(
			    "dtd" => "103.0",
			    "kode_icd" => "E03",
			    "sebab" => "Hipotiroidisme lain"
			);
			$data_diagnosa[] = array(
			    "dtd" => "103.1",
			    "kode_icd" => "E04",
			    "sebab" => "Penyakit gondok nontoksik lain"
			);
			$data_diagnosa[] = array(
			    "dtd" => "103.2",
			    "kode_icd" => "E06",
			    "sebab" => "Tiroiditis"
			);
			$data_diagnosa[] = array(
			    "dtd" => "103.9",
			    "kode_icd" => "E07",
			    "sebab" => "Gangguan kelenjar tiroid lainnya"
			);
			$data_diagnosa[] = array(
			    "dtd" => "104.0",
			    "kode_icd" => "E10",
			    "sebab" => "Diabetes melitus bergantung insulin"
			);
			$data_diagnosa[] = array(
			    "dtd" => "104.1",
			    "kode_icd" => "E11",
			    "sebab" => "Diabetes melitus tidaj bergantung insulin"
			);
			$data_diagnosa[] = array(
			    "dtd" => "104.2",
			    "kode_icd" => "E12",
			    "sebab" => "Diabetel militus berhubungan malnutrisi"
			);
			$data_diagnosa[] = array(
			    "dtd" => "104.3",
			    "kode_icd" => "E13",
			    "sebab" => "Diabetes melitus YDT lainnya"
			);
			$data_diagnosa[] = array(
			    "dtd" => "104.9",
			    "kode_icd" => "E14",
			    "sebab" => "Diabetes melitus YTT"
			);
			$data_diagnosa[] = array(
			    "dtd" => "105",
			    "kode_icd" => "E40-E46",
			    "sebab" => "Malnutrisi"
			);
			$data_diagnosa[] = array(
			    "dtd" => "106",
			    "kode_icd" => "E50",
			    "sebab" => "Defisiensi vitamin A"
			);
			$data_diagnosa[] = array(
			    "dtd" => "107",
			    "kode_icd" => "E51-E56",
			    "sebab" => "Defisiensi vitamin lainnya"
			);
			$data_diagnosa[] = array(
			    "dtd" => "108",
			    "kode_icd" => "E64",
			    "sebab" => "Gejala sisa malnutrisi dan defisiensi gizi lainnya"
			);
			$data_diagnosa[] = array(
			    "dtd" => "109",
			    "kode_icd" => "E66",
			    "sebab" => "Obesitas"
			);
			$data_diagnosa[] = array(
			    "dtd" => "110",
			    "kode_icd" => "E86",
			    "sebab" => "Deplesi volume (dehidrasi)"
			);
			$data_diagnosa[] = array(
			    "dtd" => "111",
			    "kode_icd" => "E15-35.58.63.65.67, E85.87-90",
			    "sebab" => "Gangguan endokrin, nutrisi dan metbolik Lainnya"
			);
			$data_diagnosa[] = array(
			    "dtd" => "112",
			    "kode_icd" => "F00-F03",
			    "sebab" => "Demensia"
			);
			$data_diagnosa[] = array(
			    "dtd" => "113",
			    "kode_icd" => "F10",
			    "sebab" => "Gangguan mental dan perilaku akibat Penggunaan alkohol"
			);
			$data_diagnosa[] = array(
			    "dtd" => "114.0",
			    "kode_icd" => "F11",
			    "sebab" => "Gangguan mental dan perlaku akibat Penggunaan opioida"
			);
			$data_diagnosa[] = array(
			    "dtd" => "114.1",
			    "kode_icd" => "F12",
			    "sebab" => "Gangguan mental dan perilaku akibat Pengguanaan kanabinoida"
			);
			$data_diagnosa[] = array(
			    "dtd" => "114.2",
			    "kode_icd" => "F13",
			    "sebab" => "Gangguan mental dan perilaku akibat Penggunaan Sedativa atau Hipnotika"
			);
			$data_diagnosa[] = array(
			    "dtd" => "114.3",
			    "kode_icd" => "F14",
			    "sebab" => "Gangguan mental dan perilaku akibat Penggunaan Kokain"
			);
			$data_diagnosa[] = array(
			    "dtd" => "114.4",
			    "kode_icd" => "F15",
			    "sebab" => "Gangguan mental dan perilaku akibat Penggunaan stimeunlansia"
			);
			$data_diagnosa[] = array(
			    "dtd" => "114.5",
			    "kode_icd" => "F16",
			    "sebab" => "Gangguan mental dan perilaku akibat Pengunaan halosinogenika"
			);
			$data_diagnosa[] = array(
			    "dtd" => "114.6",
			    "kode_icd" => "F17",
			    "sebab" => "Gangguan mental dan perilaku akibat Penggunaan tembakau"
			);
			$data_diagnosa[] = array(
			    "dtd" => "114.9",
			    "kode_icd" => "F18.F19",
			    "sebab" => "Gangguan mental dan perilaku akibat Zat pelarut yang mudah menguap, atau zat Multipel dan zat psikoaktif lainnya"
			);
			$data_diagnosa[] = array(
			    "dtd" => "115.0",
			    "kode_icd" => "F20.F21.F23",
			    "sebab" => "Skizofrenia, gangguan skizotipal, psikotik Akut dan sementara"
			);
			$data_diagnosa[] = array(
			    "dtd" => "115.1",
			    "kode_icd" => "F22,F24",
			    "sebab" => "Gangguan waham menetap dan induksi"
			);
			$data_diagnosa[] = array(
			    "dtd" => "115.2",
			    "kode_icd" => "F25",
			    "sebab" => "Gangguan skizoafektif"
			);
			$data_diagnosa[] = array(
			    "dtd" => "115.9",
			    "kode_icd" => "F28,F29",
			    "sebab" => "Gangguan psikotik nonorganik lainnya atau YTT"
			);
			$data_diagnosa[] = array(
			    "dtd" => "116.0",
			    "kode_icd" => "F30,F31",
			    "sebab" => "Episode manik dan gangguan efektif bipolar"
			);
			$data_diagnosa[] = array(
			    "dtd" => "116.9",
			    "kode_icd" => "F32,F39",
			    "sebab" => "Episode defresif, gangguan depresif Berulang, gangguan suasana perasaan (mood Efektif) menetap, lainnya atau YTT"
			);
			$data_diagnosa[] = array(
			    "dtd" => "117.0",
			    "kode_icd" => "F40,F41.1,3-9",
			    "sebab" => "Gangguan anxietas fobik, gangguan anxietas Lainnya"
			);
			$data_diagnosa[] = array(
			    "dtd" => "117.1",
			    "kode_icd" => "F42",
			    "sebab" => "Gangguan obsesif - kompulsif"
			);
			$data_diagnosa[] = array(
			    "dtd" => "117.2",
			    "kode_icd" => "F43.1",
			    "sebab" => "Gangguian stres pasca trauma"
			);
			$data_diagnosa[] = array(
			    "dtd" => "117.3",
			    "kode_icd" => "F43.0,F43.2,F45,F48",
			    "sebab" => "Reaksi terhadap stres berat dan gangguan Penyesuaian, gangguan somatoform, gangguan Neurotik lainnya"
			);
			$data_diagnosa[] = array(
			    "dtd" => "117.9",
			    "kode_icd" => "F44",
			    "sebab" => "Gangguan dososiatif (konversi)"
			);
			$data_diagnosa[] = array(
			    "dtd" => "118",
			    "kode_icd" => "F70-F79",
			    "sebab" => "Retardasi mental"
			);
			$data_diagnosa[] = array(
			    "dtd" => "119.0",
			    "kode_icd" => "F04, F07, F09",
			    "sebab" => "Sindrom amnestik dan gangguan mental organik"
			);
			$data_diagnosa[] = array(
			    "dtd" => "119.1",
			    "kode_icd" => "F50-F52,F53.1-9,F54,F59",
			    "sebab" => "Sindrom makan, gangguan tidur, disfungsi seksual,gangguan indentitas, gangguan Perilaku lainnya"
			);
			$data_diagnosa[] = array(
			    "dtd" => "119.2",
			    "kode_icd" => "F60-F69",
			    "sebab" => "Gangguan kepribadian, gangguan kebiasaan Dan impuls, gangguan identitas, gangguan Prevensi seksual"
			);
			$data_diagnosa[] = array(
			    "dtd" => "119.3",
			    "kode_icd" => "F80-F89",
			    "sebab" => "Gangguan perkembangan psikologis"
			);
			$data_diagnosa[] = array(
			    "dtd" => "119.4",
			    "kode_icd" => "F05-F06.0-6,8-9, F90-F98",
			    "sebab" => 'Gangguan hiperkinetik, gangguan tingkah laku, gangguan emosional atau fungsi sosial khas, gangguan "tic", gangguan perilaku dan emosional lainnya'
			);
			$data_diagnosa[] = array(
			    "dtd" => "119.5",
			    "kode_icd" => "F53.0",
			    "sebab" => "Depresif post partum"
			);
			$data_diagnosa[] = array(
			    "dtd" => "119.6",
			    "kode_icd" => "F41.2",
			    "sebab" => "Depresif gangguan cemas "
			);
			$data_diagnosa[] = array(
			    "dtd" => "119.9",
			    "kode_icd" => "F99",
			    "sebab" => "Gangguan jiwa YTT"
			);
			$data_diagnosa[] = array(
			    "dtd" => "120",
			    "kode_icd" => "G00-G09",
			    "sebab" => "Penyakit radang susunan saraf pusat"
			);
			$data_diagnosa[] = array(
			    "dtd" => "121",
			    "kode_icd" => "G20",
			    "sebab" => "Penyakit parkinson"
			);
			$data_diagnosa[] = array(
			    "dtd" => "122",
			    "kode_icd" => "G30",
			    "sebab" => "Penyakit Alzheimer"
			);
			$data_diagnosa[] = array(
			    "dtd" => "123",
			    "kode_icd" => "G35",
			    "sebab" => "Sklerosis multipel"
			);
			$data_diagnosa[] = array(
			    "dtd" => "124",
			    "kode_icd" => "G40-G41",
			    "sebab" => "Epilepsi"
			);
			$data_diagnosa[] = array(
			    "dtd" => "125",
			    "kode_icd" => "G43-G44",
			    "sebab" => "Migren dan sindrom nyeri kepala lainnya"
			);
			$data_diagnosa[] = array(
			    "dtd" => "126",
			    "kode_icd" => "G45",
			    "sebab" => "Gangguan serangan peredaran otak sepintas Dan sindroma yang terkait"
			);
			$data_diagnosa[] = array(
			    "dtd" => "127.0",
			    "kode_icd" => "G56.0",
			    "sebab" => "Sindroma carpal tunnel"
			);
			$data_diagnosa[] = array(
			    "dtd" => "127.1",
			    "kode_icd" => "G56.2",
			    "sebab" => "Lesi saraf ulnaris"
			);
			$data_diagnosa[] = array(
			    "dtd" => "127.2",
			    "kode_icd" => "G56.3",
			    "sebab" => "Lesi saraf radialis"
			);
			$data_diagnosa[] = array(
			    "dtd" => "127.3",
			    "kode_icd" => "G56.8",
			    "sebab" => "Mononeuropati anggota tunuh bagian atas lainnya"
			);
			$data_diagnosa[] = array(
			    "dtd" => "127.9",
			    "kode_icd" => "G50-G55, G57-G59",
			    "sebab" => "Gangguan saraf , radiks dan pleksus saraf"
			);
			$data_diagnosa[] = array(
			    "dtd" => "128.0",
			    "kode_icd" => "G80",
			    "sebab" => "Infantil cerebral palsy"
			);
			$data_diagnosa[] = array(
			    "dtd" => "128.9",
			    "kode_icd" => "G81-G83",
			    "sebab" => "Sindroma paralitik lainnya"
			);
			$data_diagnosa[] = array(
			    "dtd" => "129.0",
			    "kode_icd" => "G21",
			    "sebab" => "Parkinson sekunder"
			);
			$data_diagnosa[] = array(
			    "dtd" => "129.1",
			    "kode_icd" => "G92",
			    "sebab" => "Toksik insefallopati"
			);
			$data_diagnosa[] = array(
			    "dtd" => "129.9",
			    "kode_icd" => "G10-13, G26, G31-32. G36-G37, G46-47, G60-73, G90-G91, G93, G99",
			    "sebab" => "Penyakit susunan saraf lainnya"
			);
			$data_diagnosa[] = array(
			    "dtd" => "130",
			    "kode_icd" => "H00-H01",
			    "sebab" => "Radang kelopak mata"
			);
			$data_diagnosa[] = array(
			    "dtd" => "131",
			    "kode_icd" => "H10-H13",
			    "sebab" => "Konjungtivitis dan gangguan lain konjungtiva"
			);
			$data_diagnosa[] = array(
			    "dtd" => "132",
			    "kode_icd" => "H15-H19",
			    "sebab" => "Keratitis dan dan gangguan lain sklera dan kornea"
			);
			$data_diagnosa[] = array(
			    "dtd" => "133",
			    "kode_icd" => "H25-H28",
			    "sebab" => "Katarak dan gangguan lain lensa"
			);
			$data_diagnosa[] = array(
			    "dtd" => "134",
			    "kode_icd" => "H33",
			    "sebab" => "Ablasi dan kerusakan retina"
			);
			$data_diagnosa[] = array(
			    "dtd" => "135",
			    "kode_icd" => "H40-H42",
			    "sebab" => "Glaukoma"
			);
			$data_diagnosa[] = array(
			    "dtd" => "136",
			    "kode_icd" => "H49-H50",
			    "sebab" => "Strabismus"
			);
			$data_diagnosa[] = array(
			    "dtd" => "137",
			    "kode_icd" => "H52",
			    "sebab" => "Gangguan refraksi dan oakomodasi"
			);
			$data_diagnosa[] = array(
			    "dtd" => "138",
			    "kode_icd" => "H54",
			    "sebab" => "Buta dan rabun"
			);
			$data_diagnosa[] = array(
			    "dtd" => "139.0",
			    "kode_icd" => "H02-H03",
			    "sebab" => "Gangguan lain kelopak mata"
			);
			$data_diagnosa[] = array(
			    "dtd" => "139.1",
			    "kode_icd" => "H04-H06",
			    "sebab" => "Gangguan sistem lakrimal dan orbita"
			);
			$data_diagnosa[] = array(
			    "dtd" => "139.2",
			    "kode_icd" => "H20-H22",
			    "sebab" => "Iridosiklitis dan gangguan lain iris dan Badan silier"
			);
			$data_diagnosa[] = array(
			    "dtd" => "139.3",
			    "kode_icd" => "H30-H32",
			    "sebab" => "Gangguan koroid dan korieretina"
			);
			$data_diagnosa[] = array(
			    "dtd" => "139.4",
			    "kode_icd" => "H34",
			    "sebab" => "Sumbatan vaskular retina"
			);
			$data_diagnosa[] = array(
			    "dtd" => "139.5",
			    "kode_icd" => "H35-H36",
			    "sebab" => "Gangguan lain retina"
			);
			$data_diagnosa[] = array(
			    "dtd" => "139.6",
			    "kode_icd" => "H43-H45",
			    "sebab" => "Gangguan badan kaca dan bola mata"
			);
			$data_diagnosa[] = array(
			    "dtd" => "139.7",
			    "kode_icd" => "H46-H48",
			    "sebab" => "Gangguan saraf mata optik dan saraf penglihatan"
			);
			$data_diagnosa[] = array(
			    "dtd" => "139.8",
			    "kode_icd" => "H51",
			    "sebab" => "Gangguan lain gerakan mata binokular"
			);
			$data_diagnosa[] = array(
			    "dtd" => "139.9",
			    "kode_icd" => "H53",
			    "sebab" => "Gangguan daya liat"
			);
			$data_diagnosa[] = array(
			    "dtd" => "139.10",
			    "kode_icd" => "H55",
			    "sebab" => "Nistagmus & pergerakan mata yang tidak teratur lainnya"
			);
			$data_diagnosa[] = array(
			    "dtd" => "139.11",
			    "kode_icd" => "H55-H59",
			    "sebab" => "Penyakit lain mata dan edneksia"
			);
			$data_diagnosa[] = array(
			    "dtd" => "140",
			    "kode_icd" => "H65-H75",
			    "sebab" => "Otitis media dan gangguan mastoid dan Telinga tengah"
			);
			$data_diagnosa[] = array(
			    "dtd" => "141",
			    "kode_icd" => "H90-H91",
			    "sebab" => "Gangguan daya dengar"
			);
			$data_diagnosa[] = array(
			    "dtd" => "142.0",
			    "kode_icd" => "H61.8",
			    "sebab" => "Fistula/Kista preurikel"
			);
			$data_diagnosa[] = array(
			    "dtd" => "142.1",
			    "kode_icd" => "H83.3",
			    "sebab" => "Efek kebisingan telinga bagian dalam"
			);
			$data_diagnosa[] = array(
			    "dtd" => "142.9",
			    "kode_icd" => "H60-61.3.9 H62, H80-H83.0. H83.2, H83.8-H83.9 H92, H95",
			    "sebab" => "Penyakit telinga dan proseus mastoid"
			);
			$data_diagnosa[] = array(
			    "dtd" => "143",
			    "kode_icd" => "I00-I02",
			    "sebab" => "Demam reumatik akut"
			);
			$data_diagnosa[] = array(
			    "dtd" => "144",
			    "kode_icd" => "I05-I09",
			    "sebab" => "Penyakit jantung reumatik kronik"
			);
			$data_diagnosa[] = array(
			    "dtd" => "145",
			    "kode_icd" => "I10",
			    "sebab" => "Hipertensi esensial (primer)"
			);
			$data_diagnosa[] = array(
			    "dtd" => "146",
			    "kode_icd" => "I11-I15",
			    "sebab" => "Penyakit hipertensi lainnya"
			);
			$data_diagnosa[] = array(
			    "dtd" => "147",
			    "kode_icd" => "I21-I22",
			    "sebab" => "Infark miokard akut"
			);
			$data_diagnosa[] = array(
			    "dtd" => "148",
			    "kode_icd" => "I20, I23-I25",
			    "sebab" => "Penyakit jantung iskemik lainnya"
			);
			$data_diagnosa[] = array(
			    "dtd" => "149",
			    "kode_icd" => "I26",
			    "sebab" => "Emboli paru"
			);
			$data_diagnosa[] = array(
			    "dtd" => "150",
			    "kode_icd" => "I44-I49",
			    "sebab" => "Gangguan hantaran dan aritmia jantung"
			);
			$data_diagnosa[] = array(
			    "dtd" => "151",
			    "kode_icd" => "I50",
			    "sebab" => "Gagal jantung"
			);
			$data_diagnosa[] = array(
			    "dtd" => "152.0",
			    "kode_icd" => "I42-I43",
			    "sebab" => "Kardiomiopati"
			);
			$data_diagnosa[] = array(
			    "dtd" => "152.9",
			    "kode_icd" => "I27-I41,51,52",
			    "sebab" => "Penyakit jantung lainnya"
			);
			$data_diagnosa[] = array(
			    "dtd" => "153",
			    "kode_icd" => "I60-I62",
			    "sebab" => "Perdarahan intrakranial"
			);
			$data_diagnosa[] = array(
			    "dtd" => "154",
			    "kode_icd" => "I63",
			    "sebab" => "Infark serebral"
			);
			$data_diagnosa[] = array(
			    "dtd" => "155",
			    "kode_icd" => "I64",
			    "sebab" => "Strok tak menyebut perdarahan atau infark"
			);
			$data_diagnosa[] = array(
			    "dtd" => "156",
			    "kode_icd" => "I65-I69",
			    "sebab" => "Penyakit serebrovaskular lainnya"
			);
			$data_diagnosa[] = array(
			    "dtd" => "157",
			    "kode_icd" => "I70",
			    "sebab" => "Aterosklerosis"
			);
			$data_diagnosa[] = array(
			    "dtd" => "158.0",
			    "kode_icd" => "I73.0",
			    "sebab" => "Sindroma raynaud's"
			);
			$data_diagnosa[] = array(
			    "dtd" => "158.9",
			    "kode_icd" => "I73, I73.8-9",
			    "sebab" => "Penyakit pembuluh darah perifer lainnya"
			);
			$data_diagnosa[] = array(
			    "dtd" => "159",
			    "kode_icd" => "I74",
			    "sebab" => "Emboli dan trombosis arteri"
			);
			$data_diagnosa[] = array(
			    "dtd" => "160",
			    "kode_icd" => "I71-I72,77-79",
			    "sebab" => "Penyakit arteri, arteriol dan kapiler lainnya"
			);
			$data_diagnosa[] = array(
			    "dtd" => "161",
			    "kode_icd" => "I80-I82",
			    "sebab" => "Flebitis, tromboflebitis,emboli dan trombosis vena"
			);
			$data_diagnosa[] = array(
			    "dtd" => "162",
			    "kode_icd" => "I83",
			    "sebab" => "Varises vena ekstremitas bawah"
			);
			$data_diagnosa[] = array(
			    "dtd" => "163",
			    "kode_icd" => "I84",
			    "sebab" => "Hemoroid/Wasir"
			);
			$data_diagnosa[] = array(
			    "dtd" => "164.0",
			    "kode_icd" => "I85",
			    "sebab" => "Varises esofagus"
			);
			$data_diagnosa[] = array(
			    "dtd" => "164.9",
			    "kode_icd" => "I86-I99",
			    "sebab" => "Penyakit sistem sirkulasi lainnya"
			);
			$data_diagnosa[] = array(
			    "dtd" => "165.0",
			    "kode_icd" => "J02",
			    "sebab" => "Faringitis akut"
			);
			$data_diagnosa[] = array(
			    "dtd" => "165.9",
			    "kode_icd" => "J03",
			    "sebab" => "Tonsilitis akut"
			);
			$data_diagnosa[] = array(
			    "dtd" => "166",
			    "kode_icd" => "J04",
			    "sebab" => "Laringitis dan trakeitis akut"
			);
			$data_diagnosa[] = array(
			    "dtd" => "167",
			    "kode_icd" => "J00-J01, J05-J06",
			    "sebab" => "Infeksi saluran napas bagian atas akut Lainnya"
			);
			$data_diagnosa[] = array(
			    "dtd" => "168",
			    "kode_icd" => "J10",
			    "sebab" => "Influensa virus teridentifikasi"
			);
			$data_diagnosa[] = array(
			    "dtd" => "168.0",
			    "kode_icd" => "J11",
			    "sebab" => "Influensa virus tidak teridentifikasi"
			);
			$data_diagnosa[] = array(
			    "dtd" => "169",
			    "kode_icd" => "J12-J18",
			    "sebab" => "Pneumonia"
			);
			$data_diagnosa[] = array(
			    "dtd" => "170",
			    "kode_icd" => "J20-J21",
			    "sebab" => "Bronkitis akut dan bronkiolitis akut"
			);
			$data_diagnosa[] = array(
			    "dtd" => "171",
			    "kode_icd" => "J32",
			    "sebab" => "Sinusitis kronik"
			);
			$data_diagnosa[] = array(
			    "dtd" => "172.0",
			    "kode_icd" => "J30.3",
			    "sebab" => "Alergi rhinitis akibat kerja"
			);
			$data_diagnosa[] = array(
			    "dtd" => "172.1",
			    "kode_icd" => "J34.8",
			    "sebab" => "Ulcus mucosa hidung & performasi septum nasi"
			);
			$data_diagnosa[] = array(
			    "dtd" => "172.9",
			    "kode_icd" => "J30.0-J30.2, J30.4-J31, J33-J34.0, J34.3",
			    "sebab" => "Penyakit hidung dan sinus hidung lainnya"
			);
			$data_diagnosa[] = array(
			    "dtd" => "173",
			    "kode_icd" => "J35",
			    "sebab" => "Penyakit tonsil dan adenoid kronik"
			);
			$data_diagnosa[] = array(
			    "dtd" => "174",
			    "kode_icd" => "J36-J39",
			    "sebab" => "Penyakit saluran napas bagian atas lainnya"
			);
			$data_diagnosa[] = array(
			    "dtd" => "175",
			    "kode_icd" => "J40-J44",
			    "sebab" => "Bronkitis, emfisema dan penyakit paru Obstruktif kronik lainnya"
			);
			$data_diagnosa[] = array(
			    "dtd" => "176.0",
			    "kode_icd" => "J45",
			    "sebab" => "Asma"
			);
			$data_diagnosa[] = array(
			    "dtd" => "176.9",
			    "kode_icd" => "J46",
			    "sebab" => "Atatus asmatika"
			);
			$data_diagnosa[] = array(
			    "dtd" => "177",
			    "kode_icd" => "J47",
			    "sebab" => "Bronkiektasis"
			);
			$data_diagnosa[] = array(
			    "dtd" => "178",
			    "kode_icd" => "J60-J65",
			    "sebab" => "Pneumokoniasis"
			);
			$data_diagnosa[] = array(
			    "dtd" => "179.0",
			    "kode_icd" => "J85.1.2",
			    "sebab" => "Abses paru"
			);
			$data_diagnosa[] = array(
			    "dtd" => "179.1",
			    "kode_icd" => "J93",
			    "sebab" => "Pneumonotoraks"
			);
			$data_diagnosa[] = array(
			    "dtd" => "179.2",
			    "kode_icd" => "J86",
			    "sebab" => "Piotoraks (empisema)"
			);
			$data_diagnosa[] = array(
			    "dtd" => "179.3",
			    "kode_icd" => "J90-J91",
			    "sebab" => "Efusi pleural (empisema)"
			);
			$data_diagnosa[] = array(
			    "dtd" => "179.4",
			    "kode_icd" => "J66.0",
			    "sebab" => "Bisinosis"
			);
			$data_diagnosa[] = array(
			    "dtd" => "179.5",
			    "kode_icd" => "J67",
			    "sebab" => "Pneumonisis hipersensitivity akibat abu organik"
			);
			$data_diagnosa[] = array(
			    "dtd" => "179.6",
			    "kode_icd" => "J68",
			    "sebab" => "Gangguan penafasan akibat menghirup zat kimia, Gas asap dan uap"
			);
			$data_diagnosa[] = array(
			    "dtd" => "179.7",
			    "kode_icd" => "J92",
			    "sebab" => "Plak pleural"
			);
			$data_diagnosa[] = array(
			    "dtd" => "179.9",
			    "kode_icd" => "J22. J66.1.2, J66.8. J69-J85.0.3-J89, J94-J99",
			    "sebab" => "Penyakit sistem napas lainnya"
			);
			$data_diagnosa[] = array(
			    "dtd" => "180",
			    "kode_icd" => "K02",
			    "sebab" => "Karies gigi"
			);
			$data_diagnosa[] = array(
			    "dtd" => "181.0",
			    "kode_icd" => "K00-K01",
			    "sebab" => "Gangguan perkembangan dan erupsi gigi Termasuk impaksi"
			);
			$data_diagnosa[] = array(
			    "dtd" => "181.1",
			    "kode_icd" => "K03",
			    "sebab" => "Penyakit jaringan keras gigi lainnya"
			);
			$data_diagnosa[] = array(
			    "dtd" => "181.2",
			    "kode_icd" => "K04",
			    "sebab" => "Penyakit pulpa dan periapikal"
			);
			$data_diagnosa[] = array(
			    "dtd" => "181.3",
			    "kode_icd" => "K05-K06",
			    "sebab" => "Penyakit gusi, jaringan periodontal dan tulang Alveolar"
			);
			$data_diagnosa[] = array(
			    "dtd" => "181.9",
			    "kode_icd" => "K07-K08",
			    "sebab" => "Kelainan dentofasial termasuk maloklusi"
			);
			$data_diagnosa[] = array(
			    "dtd" => "182.0",
			    "kode_icd" => "K09-K10",
			    "sebab" => "Kista rongga mulut dan penyakit pada rahang"
			);
			$data_diagnosa[] = array(
			    "dtd" => "182.1",
			    "kode_icd" => "K11",
			    "sebab" => "Penyakit kelenjar liur"
			);
			$data_diagnosa[] = array(
			    "dtd" => "182.2",
			    "kode_icd" => "K12",
			    "sebab" => "Penyakit jaringan lunak mulut (stomatitis) dan Lesi yang berkaitan"
			);
			$data_diagnosa[] = array(
			    "dtd" => "182.9",
			    "kode_icd" => "K13-K14",
			    "sebab" => "Penyakit bibir, mukosa mulut lainnya dan lidah"
			);
			$data_diagnosa[] = array(
			    "dtd" => "183",
			    "kode_icd" => "K25-K27",
			    "sebab" => "Tukak lambung dan duodenum"
			);
			$data_diagnosa[] = array(
			    "dtd" => "184",
			    "kode_icd" => "K29",
			    "sebab" => "Gastritis dan duodenitis"
			);
			$data_diagnosa[] = array(
			    "dtd" => "185.0",
			    "kode_icd" => "K30",
			    "sebab" => "Dispepsia"
			);
			$data_diagnosa[] = array(
			    "dtd" => "185.9",
			    "kode_icd" => "K20-K23, K28-K31",
			    "sebab" => "Penyakit esopagus, lambung dan duodenum Lainnya "
			);
			$data_diagnosa[] = array(
			    "dtd" => "186",
			    "kode_icd" => "K35-K38",
			    "sebab" => "Penyakit apendiks"
			);
			$data_diagnosa[] = array(
			    "dtd" => "187",
			    "kode_icd" => "K40",
			    "sebab" => "Hernia inguinal"
			);
			$data_diagnosa[] = array(
			    "dtd" => "188",
			    "kode_icd" => "K41-K46",
			    "sebab" => "Hernia lainnya"
			);
			$data_diagnosa[] = array(
			    "dtd" => "189",
			    "kode_icd" => "K50-K51",
			    "sebab" => "Penyakit crohn dan duodenum lainnya"
			);
			$data_diagnosa[] = array(
			    "dtd" => "190",
			    "kode_icd" => "K56",
			    "sebab" => "Ileus paralitik dan obstruksi usus tanpa hernia"
			);
			$data_diagnosa[] = array(
			    "dtd" => "191",
			    "kode_icd" => "K57",
			    "sebab" => "Penyakit divertikel usus"
			);
			$data_diagnosa[] = array(
			    "dtd" => "192.0",
			    "kode_icd" => "K58",
			    "sebab" => "Sindrom usus ringkih (irritable bowel syndrome)"
			);
			$data_diagnosa[] = array(
			    "dtd" => "192.9",
			    "kode_icd" => "K52-K55,59-67",
			    "sebab" => "Penyakit usus dan peritoneum lainnya"
			);
			$data_diagnosa[] = array(
			    "dtd" => "193",
			    "kode_icd" => "K70",
			    "sebab" => "Penyakit hati alkohol"
			);
			$data_diagnosa[] = array(
			    "dtd" => "194.0",
			    "kode_icd" => "K72",
			    "sebab" => "Koma hepatikum dan hepatitis fulminan"
			);
			$data_diagnosa[] = array(
			    "dtd" => "194.1",
			    "kode_icd" => "K73",
			    "sebab" => "Hepatitis kronik"
			);
			$data_diagnosa[] = array(
			    "dtd" => "194.2",
			    "kode_icd" => "K74.6",
			    "sebab" => "Sirosis hati"
			);
			$data_diagnosa[] = array(
			    "dtd" => "194.3",
			    "kode_icd" => "K76.0",
			    "sebab" => "Perlemakan hati"
			);
			$data_diagnosa[] = array(
			    "dtd" => "194.4",
			    "kode_icd" => "K76.6",
			    "sebab" => "Hipertensi portal"
			);
			$data_diagnosa[] = array(
			    "dtd" => "194.5",
			    "kode_icd" => "K76.7",
			    "sebab" => "Sindrom hepatorenal"
			);
			$data_diagnosa[] = array(
			    "dtd" => "194.6",
			    "kode_icd" => "K71",
			    "sebab" => "Penyakit hati akibat bahan beracun di tempat kerja"
			);
			$data_diagnosa[] = array(
			    "dtd" => "194.9",
			    "kode_icd" => "K74.0-5, K75, K76.1-5,8,9 K77",
			    "sebab" => "Penyakit hati lainnya"
			);
			$data_diagnosa[] = array(
			    "dtd" => "195.0",
			    "kode_icd" => "K80",
			    "sebab" => "Kolelitiasis"
			);
			$data_diagnosa[] = array(
			    "dtd" => "195.9",
			    "kode_icd" => "K81",
			    "sebab" => "Kolesistitis"
			);
			$data_diagnosa[] = array(
			    "dtd" => "196",
			    "kode_icd" => "K85-K86",
			    "sebab" => "Pankreatitis akut dan penyakit pankreas lainnya"
			);
			$data_diagnosa[] = array(
			    "dtd" => "197",
			    "kode_icd" => "K82-K83, K87-K93",
			    "sebab" => "Penyakit sistem cerna lainnya"
			);
			$data_diagnosa[] = array(
			    "dtd" => "198",
			    "kode_icd" => "L00-L08",
			    "sebab" => "Infeksi kulit dan jaringan subkutan"
			);
			$data_diagnosa[] = array(
			    "dtd" => "199.0",
			    "kode_icd" => "L23-L24",
			    "sebab" => "Dermatosis akibat kerja"
			);
			$data_diagnosa[] = array(
			    "dtd" => "199.9",
			    "kode_icd" => "L10-L22, L25-L99",
			    "sebab" => "Penyakit kulit dan jaringan subkutan lainnya"
			);
			$data_diagnosa[] = array(
			    "dtd" => "200.0",
			    "kode_icd" => "M05-M06",
			    "sebab" => "Artritis reumatoid"
			);
			$data_diagnosa[] = array(
			    "dtd" => "200.1",
			    "kode_icd" => "M07",
			    "sebab" => "Psoriasis dan artropati enteropati"
			);
			$data_diagnosa[] = array(
			    "dtd" => "200.2",
			    "kode_icd" => "M08-M09",
			    "sebab" => "Artritis belia"
			);
			$data_diagnosa[] = array(
			    "dtd" => "200.3",
			    "kode_icd" => "M10-M11",
			    "sebab" => "Psoriasis dan artripati lainnya"
			);
			$data_diagnosa[] = array(
			    "dtd" => "200.9",
			    "kode_icd" => "M12-M14",
			    "sebab" => "Artripati dan artritis"
			);
			$data_diagnosa[] = array(
			    "dtd" => "201",
			    "kode_icd" => "M15-M19",
			    "sebab" => "Artrisis"
			);
			$data_diagnosa[] = array(
			    "dtd" => "202",
			    "kode_icd" => "M20-M21",
			    "sebab" => "Deformitas tungkai didapat"
			);
			$data_diagnosa[] = array(
			    "dtd" => "203.0",
			    "kode_icd" => "M00-M01",
			    "sebab" => "Artritis piogenik dan artritis pada penyakit infeksi Dan parasit YDK di tempat lain"
			);
			$data_diagnosa[] = array(
			    "dtd" => "203.1",
			    "kode_icd" => "M02-M03",
			    "sebab" => "Artripati reaktif"
			);
			$data_diagnosa[] = array(
			    "dtd" => "203.9",
			    "kode_icd" => "M22-M25",
			    "sebab" => "Kelainan sendi lainnya"
			);
			$data_diagnosa[] = array(
			    "dtd" => "204.0",
			    "kode_icd" => "M32",
			    "sebab" => "Lupus eritemateus sistemik"
			);
			$data_diagnosa[] = array(
			    "dtd" => "204.9",
			    "kode_icd" => "M30-M31, M33-M36",
			    "sebab" => "Gangguan jaringan ikat sistemik lainnya"
			);
			$data_diagnosa[] = array(
			    "dtd" => "205",
			    "kode_icd" => "M50-M51",
			    "sebab" => "Gangguan diskus servikal dan intervertebral lainnya"
			);
			$data_diagnosa[] = array(
			    "dtd" => "206.0",
			    "kode_icd" => "M45-M49",
			    "sebab" => "Spondiloartropati seronegatif"
			);
			$data_diagnosa[] = array(
			    "dtd" => "206.1",
			    "kode_icd" => "M54.5",
			    "sebab" => "Nyeri punggung bawah"
			);
			$data_diagnosa[] = array(
			    "dtd" => "206.9",
			    "kode_icd" => "M40-M44, M53-M54.0, M54.4, M54.6, M54.8,9",
			    "sebab" => "Dorsopati lainnya"
			);
			$data_diagnosa[] = array(
			    "dtd" => "207.0",
			    "kode_icd" => "M60-M64, M65.0-M65.3.8.9, M68",
			    "sebab" => "Miopati dan reumatisme"
			);
			$data_diagnosa[] = array(
			    "dtd" => "207.1",
			    "kode_icd" => "M65.4",
			    "sebab" => "Penyakit de queervain"
			);
			$data_diagnosa[] = array(
			    "dtd" => "207.2",
			    "kode_icd" => "M70",
			    "sebab" => "Gangguan jaringan lunak akibat yang berhubungan Dengan penggunaan tekanan berlebihan"
			);
			$data_diagnosa[] = array(
			    "dtd" => "207.9",
			    "kode_icd" => "M71-M79",
			    "sebab" => "Gangguan jaringan ikat lainnya"
			);
			$data_diagnosa[] = array(
			    "dtd" => "208",
			    "kode_icd" => "M80-M85",
			    "sebab" => "Gangguan struktur dan densitas tulang"
			);
			$data_diagnosa[] = array(
			    "dtd" => "209",
			    "kode_icd" => "M86",
			    "sebab" => "Osteomielitis"
			);
			$data_diagnosa[] = array(
			    "dtd" => "210",
			    "kode_icd" => "M87-M99",
			    "sebab" => "Penyakit sistem muskuloskeletal dan jaringan ikat Lainnya"
			);
			$data_diagnosa[] = array(
			    "dtd" => "211",
			    "kode_icd" => "N00-N01",
			    "sebab" => "Sindrom nefritik progresif cepat dan akut"
			);
			$data_diagnosa[] = array(
			    "dtd" => "212.0",
			    "kode_icd" => "N04",
			    "sebab" => "Ssindrom nefrotik"
			);
			$data_diagnosa[] = array(
			    "dtd" => "212.2",
			    "kode_icd" => "N02.8",
			    "sebab" => "Nefropati imunoglobulin A (lg A)"
			);
			$data_diagnosa[] = array(
			    "dtd" => "212.9",
			    "kode_icd" => "N02.0-7.9. N03, N05-N08",
			    "sebab" => "Penyakit glomerulus lainnya"
			);
			$data_diagnosa[] = array(
			    "dtd" => "213.0",
			    "kode_icd" => "N12",
			    "sebab" => "Nefritis tubulo-intersitial, tidak Ditemukan akut atau kronik/pielonefritis"
			);
			$data_diagnosa[] = array(
			    "dtd" => "213.1",
			    "kode_icd" => "N14.3",
			    "sebab" => "Nefropati disebabkan oleh logam-logam berat"
			);
			$data_diagnosa[] = array(
			    "dtd" => "213.9",
			    "kode_icd" => "N10-N11, N13,14.0-2.4 16",
			    "sebab" => "Penyakit tubulo -intersitial ginjal lainnya"
			);
			$data_diagnosa[] = array(
			    "dtd" => "214.0",
			    "kode_icd" => "N17.8",
			    "sebab" => "Gagal ginjal akut akibat asam jengkol"
			);
			$data_diagnosa[] = array(
			    "dtd" => "214.9",
			    "kode_icd" => "N17.0-2,9-N19",
			    "sebab" => "Gagal ginjal lainnya"
			);
			$data_diagnosa[] = array(
			    "dtd" => "215",
			    "kode_icd" => "N20-N23",
			    "sebab" => "Urolitisiasis"
			);
			$data_diagnosa[] = array(
			    "dtd" => "216",
			    "kode_icd" => "N30",
			    "sebab" => "Sistitis"
			);
			$data_diagnosa[] = array(
			    "dtd" => "217",
			    "kode_icd" => "N25-N29, N31-N39",
			    "sebab" => "Penyakit sistem kemih lainnya"
			);
			$data_diagnosa[] = array(
			    "dtd" => "218",
			    "kode_icd" => "N40",
			    "sebab" => "Hiperplasia prostat"
			);
			$data_diagnosa[] = array(
			    "dtd" => "219",
			    "kode_icd" => "N41-N42",
			    "sebab" => "Gangguan prostat lainnya"
			);
			$data_diagnosa[] = array(
			    "dtd" => "220",
			    "kode_icd" => "N43",
			    "sebab" => "Hidrokel dan spermatokel"
			);
			$data_diagnosa[] = array(
			    "dtd" => "221",
			    "kode_icd" => "N47",
			    "sebab" => "Prepusium berlebih, fimosis dan parafimosis"
			);
			$data_diagnosa[] = array(
			    "dtd" => "222",
			    "kode_icd" => "N44-N46, N48-N51",
			    "sebab" => "Penyakit alat kelamin laki lainnya"
			);
			$data_diagnosa[] = array(
			    "dtd" => "223",
			    "kode_icd" => "N60-N64",
			    "sebab" => "Gangguan pada payudarah"
			);
			$data_diagnosa[] = array(
			    "dtd" => "224",
			    "kode_icd" => "N70",
			    "sebab" => "Salpingitis dan ooforitis"
			);
			$data_diagnosa[] = array(
			    "dtd" => "225",
			    "kode_icd" => "N72",
			    "sebab" => "Radang serviks"
			);
			$data_diagnosa[] = array(
			    "dtd" => "226.0",
			    "kode_icd" => "N73",
			    "sebab" => "Radang panggul perempuan lainnya"
			);
			$data_diagnosa[] = array(
			    "dtd" => "226.1",
			    "kode_icd" => "N75.0.1",
			    "sebab" => "Kista dan abses kelenjar Bartholin"
			);
			$data_diagnosa[] = array(
			    "dtd" => "226.9",
			    "kode_icd" => "N71, N74, N75.8-N77",
			    "sebab" => "Radang alat dalam panggul perempuan lainnya (adneksitis)"
			);
			$data_diagnosa[] = array(
			    "dtd" => "227",
			    "kode_icd" => "N80",
			    "sebab" => "Endometriosis"
			);
			$data_diagnosa[] = array(
			    "dtd" => "228",
			    "kode_icd" => "N81",
			    "sebab" => "Prolaps alat kelamin perempuan"
			);
			$data_diagnosa[] = array(
			    "dtd" => "229",
			    "kode_icd" => "N83",
			    "sebab" => "Gangguan bukan radang pada indung telur, Saluran telur dan ligamentum latum"
			);
			$data_diagnosa[] = array(
			    "dtd" => "230.0",
			    "kode_icd" => "N91.0.1.2",
			    "sebab" => "Amenare"
			);
			$data_diagnosa[] = array(
			    "dtd" => "230.1",
			    "kode_icd" => "N92.0.1",
			    "sebab" => "Menoragi atau metroragi"
			);
			$data_diagnosa[] = array(
			    "dtd" => "230.9",
			    "kode_icd" => "N91.3-5,92 2-6",
			    "sebab" => "Gangguan haid lainnya"
			);
			$data_diagnosa[] = array(
			    "dtd" => "231",
			    "kode_icd" => "N95",
			    "sebab" => "Gangguan dalam masa menapause dan perime nopause Lainnya"
			);
			$data_diagnosa[] = array(
			    "dtd" => "232",
			    "kode_icd" => "N97",
			    "sebab" => "Infertilitas perempuan"
			);
			$data_diagnosa[] = array(
			    "dtd" => "233",
			    "kode_icd" => "N82. N84-N90. N93-N94. N96. N98-N99",
			    "sebab" => "Gangguan sistem kemih kelamin lainnya"
			);
			$data_diagnosa[] = array(
			    "dtd" => "234",
			    "kode_icd" => "O03",
			    "sebab" => "Abortus spontan"
			);
			$data_diagnosa[] = array(
			    "dtd" => "235",
			    "kode_icd" => "O04",
			    "sebab" => "Abortus medik"
			);
			$data_diagnosa[] = array(
			    "dtd" => "236.0",
			    "kode_icd" => "O00",
			    "sebab" => "Kehamilan ektopik"
			);
			$data_diagnosa[] = array(
			    "dtd" => "236.1",
			    "kode_icd" => "O01",
			    "sebab" => "Mola hidatidosa"
			);
			$data_diagnosa[] = array(
			    "dtd" => "236.2",
			    "kode_icd" => "O05",
			    "sebab" => "Abortus lainnya"
			);
			$data_diagnosa[] = array(
			    "dtd" => "236.9",
			    "kode_icd" => "O02.O06-O08",
			    "sebab" => "Kehamilan lain yang berakhir dengan abortus"
			);
			$data_diagnosa[] = array(
			    "dtd" => "237.0",
			    "kode_icd" => "O14",
			    "sebab" => "Hipertensi gestasional (akibat kehamilan) Dengan proteinuria yang nyata/preeklamsia"
			);
			$data_diagnosa[] = array(
			    "dtd" => "237.1",
			    "kode_icd" => "O15",
			    "sebab" => "Eklampsia"
			);
			$data_diagnosa[] = array(
			    "dtd" => "237.9",
			    "kode_icd" => "O10-O13.O16",
			    "sebab" => "Edema,proteinuria dan gangguan hipertensi Dalam kehamilan,persalinan dan masa nifas"
			);
			$data_diagnosa[] = array(
			    "dtd" => "238.0",
			    "kode_icd" => "O44",
			    "sebab" => "Plasenta previa"
			);
			$data_diagnosa[] = array(
			    "dtd" => "238.1",
			    "kode_icd" => "O45",
			    "sebab" => "Solusio plasenta"
			);
			$data_diagnosa[] = array(
			    "dtd" => "238.9",
			    "kode_icd" => "O46",
			    "sebab" => "Perdarahan antepartum"
			);
			$data_diagnosa[] = array(
			    "dtd" => "239.0",
			    "kode_icd" => "O30",
			    "sebab" => "Kehamilan multipel"
			);
			$data_diagnosa[] = array(
			    "dtd" => "239.1",
			    "kode_icd" => "O40",
			    "sebab" => "Hidramnion"
			);
			$data_diagnosa[] = array(
			    "dtd" => "239.2",
			    "kode_icd" => "O42",
			    "sebab" => "Ketuban pecah dini"
			);
			$data_diagnosa[] = array(
			    "dtd" => "239.3",
			    "kode_icd" => "O48",
			    "sebab" => "Kehamilan lewat waktu"
			);
			$data_diagnosa[] = array(
			    "dtd" => "239.9",
			    "kode_icd" => "O31-O39, O41, 043, 047",
			    "sebab" => "Perawatan ibu yang berkaitan dengan janin Dan ketuban dan masalah persalinan"
			);
			$data_diagnosa[] = array(
			    "dtd" => "240",
			    "kode_icd" => "O64-O66",
			    "sebab" => "Persalinan macet"
			);
			$data_diagnosa[] = array(
			    "dtd" => "241",
			    "kode_icd" => "O72",
			    "sebab" => "Pendarahan pasca persalinan"
			);
			$data_diagnosa[] = array(
			    "dtd" => "242.0",
			    "kode_icd" => "O24",
			    "sebab" => "Diabetes militus dalam kehamilan"
			);
			$data_diagnosa[] = array(
			    "dtd" => "242.1",
			    "kode_icd" => "O60",
			    "sebab" => "Persalinan prematur"
			);
			$data_diagnosa[] = array(
			    "dtd" => "242.2",
			    "kode_icd" => "O68",
			    "sebab" => "Persalinan dengan penyulit gawat janin"
			);
			$data_diagnosa[] = array(
			    "dtd" => "242.3",
			    "kode_icd" => "O84",
			    "sebab" => "Persalinan multipel"
			);
			$data_diagnosa[] = array(
			    "dtd" => "242.9",
			    "kode_icd" => "O20-O23. 025-O29, O61-O63. O67, O69-71, O73-O75. 081-O83",
			    "sebab" => "Penyulit kehamilan dan persalinan lainnya"
			);
			$data_diagnosa[] = array(
			    "dtd" => "243",
			    "kode_icd" => "O80",
			    "sebab" => "Persalinan tunggal spontan"
			);
			$data_diagnosa[] = array(
			    "dtd" => "244",
			    "kode_icd" => "O85-O99",
			    "sebab" => "Penyulit yang lebih banyak berhubungan Dengan masa nifas dan kondisi obsterik Lainnya, YTK ditempat lain"
			);
			$data_diagnosa[] = array(
			    "dtd" => "245",
			    "kode_icd" => "P00-P04",
			    "sebab" => "Janin dan bayi baru lahir yang dipengaruhi Oleh faktor dan penyulit kehamilan persalinan Dan kelahiran"
			);
			$data_diagnosa[] = array(
			    "dtd" => "246",
			    "kode_icd" => "P05-P07",
			    "sebab" => "Pertumbuhan janin lamban, malnutrisi janin Dan gangguan yang berhubungan dengan kehamilan pendek dan berat badan lahir rendah"
			);
			$data_diagnosa[] = array(
			    "dtd" => "247",
			    "kode_icd" => "P10-P15",
			    "sebab" => "Cedera lahir"
			);
			$data_diagnosa[] = array(
			    "dtd" => "248",
			    "kode_icd" => "P20-P21",
			    "sebab" => "Hipoksia intrauterus dan asfiksia lahir"
			);
			$data_diagnosa[] = array(
			    "dtd" => "249",
			    "kode_icd" => "P22-P28",
			    "sebab" => "Gangguan saluran napas lainnya yang Berhubungan dengan masa perinatal"
			);
			$data_diagnosa[] = array(
			    "dtd" => "250",
			    "kode_icd" => "P35-P37",
			    "sebab" => "Penyakit infeksi dan parasit kongeniotal"
			);
			$data_diagnosa[] = array(
			    "dtd" => "251",
			    "kode_icd" => "P38-P39",
			    "sebab" => "Infeksi khusus lainnya pada masa perinatal"
			);
			$data_diagnosa[] = array(
			    "dtd" => "252",
			    "kode_icd" => "P55",
			    "sebab" => "Penyakit hemolitik pd jamin & bayi baru lahir"
			);
			$data_diagnosa[] = array(
			    "dtd" => "253.0",
			    "kode_icd" => "P95",
			    "sebab" => "Lahir mati"
			);
			$data_diagnosa[] = array(
			    "dtd" => "253.9",
			    "kode_icd" => "P08, P29, P50-54, P56-P94, P96",
			    "sebab" => "Kondisi lain yang bermula pada masa Perinatal "
			);
			$data_diagnosa[] = array(
			    "dtd" => "254",
			    "kode_icd" => "Q05",
			    "sebab" => "Spina bifida"
			);
			$data_diagnosa[] = array(
			    "dtd" => "255.0",
			    "kode_icd" => "Q03",
			    "sebab" => "Hidrosefalus kongenital"
			);
			$data_diagnosa[] = array(
			    "dtd" => "255.9",
			    "kode_icd" => "Q00-Q02, Q04, Q06, Q07",
			    "sebab" => "Malformasi kongenital susunan saraf lain"
			);
			$data_diagnosa[] = array(
			    "dtd" => "256",
			    "kode_icd" => "Q20-Q28",
			    "sebab" => "Malformasi kongenital sistem peredaran darah"
			);
			$data_diagnosa[] = array(
			    "dtd" => "257",
			    "kode_icd" => "Q35-Q37",
			    "sebab" => "Bibir selah dan langit langit celah"
			);
			$data_diagnosa[] = array(
			    "dtd" => "258",
			    "kode_icd" => "Q41",
			    "sebab" => "Tidak ada, atresia dan stenosis usus halus"
			);
			$data_diagnosa[] = array(
			    "dtd" => "259",
			    "kode_icd" => "Q38, Q40, Q42-Q45",
			    "sebab" => "Malformasi kongenital sistem cerna lainnya"
			);
			$data_diagnosa[] = array(
			    "dtd" => "260",
			    "kode_icd" => "Q53",
			    "sebab" => "Testis tidak turun"
			);
			$data_diagnosa[] = array(
			    "dtd" => "261.0",
			    "kode_icd" => "Q50-Q52",
			    "sebab" => "Malformasi kongenital alat kelamin wanita"
			);
			$data_diagnosa[] = array(
			    "dtd" => "261.1",
			    "kode_icd" => "Q54-Q56",
			    "sebab" => "Malformasi kongenital alat kelamin laki"
			);
			$data_diagnosa[] = array(
			    "dtd" => "261.9",
			    "kode_icd" => "Q60-Q64",
			    "sebab" => "Malformasi kongenital sistem kemih lainnya"
			);
			$data_diagnosa[] = array(
			    "dtd" => "262",
			    "kode_icd" => "Q65",
			    "sebab" => "Deformasi kongenital sendi panggul"
			);
			$data_diagnosa[] = array(
			    "dtd" => "263",
			    "kode_icd" => "Q66",
			    "sebab" => "Deformasi kongenital kaki"
			);
			$data_diagnosa[] = array(
			    "dtd" => "264",
			    "kode_icd" => "Q67-Q79",
			    "sebab" => "Malformasi dan deformasi kongenital sistem Muskuloskeletal lain"
			);
			$data_diagnosa[] = array(
			    "dtd" => "265",
			    "kode_icd" => "Q10-Q18, Q30-Q34, Q80-Q89",
			    "sebab" => "Malformasi kongenital lainnya"
			);
			$data_diagnosa[] = array(
			    "dtd" => "266.0",
			    "kode_icd" => "Q90",
			    "sebab" => "Sindrom down"
			);
			$data_diagnosa[] = array(
			    "dtd" => "266.9",
			    "kode_icd" => "Q91-Q99",
			    "sebab" => "Kelainan kromosom YTK ditempat lain"
			);
			$data_diagnosa[] = array(
			    "dtd" => "267",
			    "kode_icd" => "R10",
			    "sebab" => "Nyeri perut dan panggul"
			);
			$data_diagnosa[] = array(
			    "dtd" => "268",
			    "kode_icd" => "R50",
			    "sebab" => "Demam yang sebabnya tidak diketahui"
			);
			$data_diagnosa[] = array(
			    "dtd" => "269",
			    "kode_icd" => "R54",
			    "sebab" => "Senilitas"
			);
			$data_diagnosa[] = array(
			    "dtd" => "270.0",
			    "kode_icd" => "R00 – R01",
			    "sebab" => "Gejala pada jantung"
			);
			$data_diagnosa[] = array(
			    "dtd" => "270.1",
			    "kode_icd" => "R09.2",
			    "sebab" => "Gagal napas"
			);
			$data_diagnosa[] = array(
			    "dtd" => "270.2",
			    "kode_icd" => "R33",
			    "sebab" => "Retensi urin"
			);
			$data_diagnosa[] = array(
			    "dtd" => "270.3",
			    "kode_icd" => "R56",
			    "sebab" => "Kejang YTT"
			);
			$data_diagnosa[] = array(
			    "dtd" => "270.4",
			    "kode_icd" => "R75",
			    "sebab" => "Hasil laboratorium positif HIV"
			);
			$data_diagnosa[] = array(
			    "dtd" => "270.5",
			    "kode_icd" => "R95",
			    "sebab" => "Sindrom mati mendadak pada bayi"
			);
			$data_diagnosa[] = array(
			    "dtd" => "270.9",
			    "kode_icd" => "R02-R09.0.1.3.8, R11-R32, R34-R49, R51-R53. R55. R57-R74. R76-R94.96-99",
			    "sebab" => "gejala,tanda dan penemuan klinik dan Laboratorium tidak normal lainnya, YDT di Tempat lain"
			);
			$data_diagnosa[] = array(
			    "dtd" => "271",
			    "kode_icd" => "S02",
			    "sebab" => "Fraktur tengkorak dan tulang muka"
			);
			$data_diagnosa[] = array(
			    "dtd" => "272",
			    "kode_icd" => "S12,22,32,T08",
			    "sebab" => "Fraktur leher,toraks atau panggul"
			);
			$data_diagnosa[] = array(
			    "dtd" => "273",
			    "kode_icd" => "S72",
			    "sebab" => "Fraktur paha"
			);
			$data_diagnosa[] = array(
			    "dtd" => "274",
			    "kode_icd" => "S42, S52, S62, S82, S92, T10, T12",
			    "sebab" => "Fraktur tulang anggota gerak lainnya"
			);
			$data_diagnosa[] = array(
			    "dtd" => "275",
			    "kode_icd" => "T02",
			    "sebab" => "Fraktur meliputi daerah badan multipel"
			);
			$data_diagnosa[] = array(
			    "dtd" => "276",
			    "kode_icd" => "S03,13,23,33,43,53, S63,73,83,93.T03",
			    "sebab" => "Dislokasi,terkilir,teregang YDT dan daerah Badab multipel"
			);
			$data_diagnosa[] = array(
			    "dtd" => "277",
			    "kode_icd" => "S05",
			    "sebab" => "Cedera mata orbita"
			);
			$data_diagnosa[] = array(
			    "dtd" => "278",
			    "kode_icd" => "S06",
			    "sebab" => "Cedera intrakranial"
			);
			$data_diagnosa[] = array(
			    "dtd" => "279",
			    "kode_icd" => "S26 – S27,S36 – S37",
			    "sebab" => "Cedera alat dalam lainnya"
			);
			$data_diagnosa[] = array(
			    "dtd" => "280",
			    "kode_icd" => "S07-08.17-18.28.38, S47-48. S57-58, S67-68.77-78.87-88, S97-98. T04-05",
			    "sebab" => "Cedera remuk dan trauma amputasi YDT dan Daerah badan mulpel"
			);
			$data_diagnosa[] = array(
			    "dtd" => "281",
			    "kode_icd" => "S00-01.04.09-11, S14-16.19-21.21.24-25, S29-31.34-35.39-41, S44-46.49-51.54-56, S59-61.64-66.69-71, S74-S76. S79-S81, S84-S86, S89-91.94-96.99, T00-01.06-07.09.11, T13-T14",
			    "sebab" => "Cedera YDT lainnya.YTT dan daerah badan mutipel"
			);
			$data_diagnosa[] = array(
			    "dtd" => "282.0",
			    "kode_icd" => "T16",
			    "sebab" => "Benda asing pada telingah"
			);
			$data_diagnosa[] = array(
			    "dtd" => "282.9",
			    "kode_icd" => "T15.T17-T19",
			    "sebab" => "Akibat dari kemasukan benda asing melalui Lubang tubuh"
			);
			$data_diagnosa[] = array(
			    "dtd" => "283",
			    "kode_icd" => "T20-T32",
			    "sebab" => "Luka bakar dan korosi"
			);
			$data_diagnosa[] = array(
			    "dtd" => "284",
			    "kode_icd" => "T36-T50",
			    "sebab" => "Keracunan obat dan preparat biologok"
			);
			$data_diagnosa[] = array(
			    "dtd" => "285.0",
			    "kode_icd" => "T52",
			    "sebab" => "Keracunan pelarut organik"
			);
			$data_diagnosa[] = array(
			    "dtd" => "285.1",
			    "kode_icd" => "T56",
			    "sebab" => "Keracunan logam"
			);
			$data_diagnosa[] = array(
			    "dtd" => "285.2",
			    "kode_icd" => "T59",
			    "sebab" => "Keracunan gas, asap dan uap lain"
			);
			$data_diagnosa[] = array(
			    "dtd" => "285.3",
			    "kode_icd" => "T60",
			    "sebab" => "Keracunan pestisida"
			);
			$data_diagnosa[] = array(
			    "dtd" => "285.9",
			    "kode_icd" => "T51. T53-T55, T57. T58. T61-T65",
			    "sebab" => "Efek toksik bahan non medisinal lainnya"
			);
			$data_diagnosa[] = array(
			    "dtd" => "286",
			    "kode_icd" => "T74",
			    "sebab" => "Sindrom salah perlakuan"
			);
			$data_diagnosa[] = array(
			    "dtd" => "287.0",
			    "kode_icd" => "T66",
			    "sebab" => "Efek radiasi YTT"
			);
			$data_diagnosa[] = array(
			    "dtd" => "287.1",
			    "kode_icd" => "T67",
			    "sebab" => "Efek panas dan pencahayaan"
			);
			$data_diagnosa[] = array(
			    "dtd" => "287.2",
			    "kode_icd" => "T70",
			    "sebab" => "Efek tekanan udara dan tekanan air"
			);
			$data_diagnosa[] = array(
			    "dtd" => "287.9",
			    "kode_icd" => "T33-T35, T68, T69, T71-T73, T75-T78",
			    "sebab" => "Efek sebab luar lainnya dan YTT Pembedahan dan perawatan YTK di tempat Lain "
			);
			$data_diagnosa[] = array(
			    "dtd" => "288",
			    "kode_icd" => "T79-T88",
			    "sebab" => "Penyulit awal trauma tertentu dan penyulit Pembedahan dan perawatan YTK di tempat lain"
			);
			$data_diagnosa[] = array(
			    "dtd" => "289",
			    "kode_icd" => "T90-T98",
			    "sebab" => "Gejala sisa cedera, keracunan dan akibat Lanjut sebab luar"
			);
			$data_diagnosa[] = array(
			    "dtd" => "307",
			    "kode_icd" => "U04",
			    "sebab" => "Sindrome akut respiratory berat (SARS)"
			);
			$data_diagnosa[] = array(
			    "dtd" => "290.0",
			    "kode_icd" => "Z00.0",
			    "sebab" => "Pemeriksaan kesehatan umum"
			);
			$data_diagnosa[] = array(
			    "dtd" => "290.1",
			    "kode_icd" => "Z00.1",
			    "sebab" => "Pemeriksaan kesehatan bayi dan anak secara Rutin "
			);
			$data_diagnosa[] = array(
			    "dtd" => "290.9",
			    "kode_icd" => "Z00.2-Z13",
			    "sebab" => "Orang yang mendapatkan pelayanan kesehatan Untuk pemeriksaan khusus dan investigasi lainnya "
			);
			$data_diagnosa[] = array(
			    "dtd" => "291",
			    "kode_icd" => "Z21",
			    "sebab" => "Keadaan infeksi HIV asimtomatik"
			);
			$data_diagnosa[] = array(
			    "dtd" => "292.0",
			    "kode_icd" => "Z23.2",
			    "sebab" => "Imunisasi BCG"
			);
			$data_diagnosa[] = array(
			    "dtd" => "292.1",
			    "kode_icd" => "Z23.5",
			    "sebab" => "Imunisasi tetanus"
			);
			$data_diagnosa[] = array(
			    "dtd" => "292.2",
			    "kode_icd" => "Z24.0",
			    "sebab" => "Imunaisasi poliomielitis"
			);
			$data_diagnosa[] = array(
			    "dtd" => "292.3",
			    "kode_icd" => "Z24.2",
			    "sebab" => "Imunisasi rabies"
			);
			$data_diagnosa[] = array(
			    "dtd" => "292.4",
			    "kode_icd" => "Z24.4",
			    "sebab" => "Imunisasi campak"
			);
			$data_diagnosa[] = array(
			    "dtd" => "292.6",
			    "kode_icd" => "Z24.6",
			    "sebab" => "Imunisasi hepatitis virus"
			);
			$data_diagnosa[] = array(
			    "dtd" => "292.7",
			    "kode_icd" => "Z27.1",
			    "sebab" => "Imunisasi gabungan DPT (Difteri,Pertusis,tetanus)"
			);
			$data_diagnosa[] = array(
			    "dtd" => "292.8",
			    "kode_icd" => "Z23.0.1.3.4.6-8, Z24.1.3.5. Z25-Z27.0.2-Z29",
			    "sebab" => "Imunisasi dan kemoterapi pencegahan lainnya"
			);
			$data_diagnosa[] = array(
			    "dtd" => "292.9",
			    "kode_icd" => "Z20, Z22",
			    "sebab" => "Orang lain dengan risiko gangguan kesehatan Yang berkaitan dengan penyakit menular"
			);
			$data_diagnosa[] = array(
			    "dtd" => "293",
			    "kode_icd" => "Z30",
			    "sebab" => "Pengelolaan kontrasepsi"
			);
			$data_diagnosa[] = array(
			    "dtd" => "294.0",
			    "kode_icd" => "Z34",
			    "sebab" => "Pengawasan kehamilan normal"
			);
			$data_diagnosa[] = array(
			    "dtd" => "294.1",
			    "kode_icd" => "Z35",
			    "sebab" => "Pengawasan kehamilan dengan risiko tinggi"
			);
			$data_diagnosa[] = array(
			    "dtd" => "294.9",
			    "kode_icd" => "Z36",
			    "sebab" => "Seleksi antenatal"
			);
			$data_diagnosa[] = array(
			    "dtd" => "295",
			    "kode_icd" => "Z38",
			    "sebab" => "Bayi lahir hidup sesuai tempat lahir"
			);
			$data_diagnosa[] = array(
			    "dtd" => "296",
			    "kode_icd" => "Z39",
			    "sebab" => "Perawatan dan pemeriksaan pasca persalinan"
			);
			$data_diagnosa[] = array(
			    "dtd" => "297.0",
			    "kode_icd" => "Z46.0",
			    "sebab" => "Pemasangan dan penyesuaian kacamata dan Lensa kontak"
			);
			$data_diagnosa[] = array(
			    "dtd" => "297.1",
			    "kode_icd" => "Z41.2",
			    "sebab" => "Khitanan menurut agama dan adat kebiasaan"
			);
			$data_diagnosa[] = array(
			    "dtd" => "297.2",
			    "kode_icd" => "Z46.3",
			    "sebab" => "Pemasangan dan penyesuaian gigi palsu"
			);
			$data_diagnosa[] = array(
			    "dtd" => "297.3",
			    "kode_icd" => "Z50",
			    "sebab" => "Pelayanan yang melibatkan gangguan prosedur Rehabilitasi"
			);
			$data_diagnosa[] = array(
			    "dtd" => "297.9",
			    "kode_icd" => "Z40-Z41.0.1.3, Z45, Z46.1.2.4.9-Z49, Z51-Z54",
			    "sebab" => "Orang yang mengunjungi pelayanan kesehatan Untuk tindakan perawatan khusus lainnya"
			);
			$data_diagnosa[] = array(
			    "dtd" => "298",
			    "kode_icd" => "Z31-Z33. Z37, Z55-Z99",
			    "sebab" => "Penunjang sarana kesehatan untuk alasan Lainnya"
			);
			return $data_diagnosa;
		}
	}
?>