<?php 
/**
 * this file used for get all patient 
 * that go out from the hospital in
 * given date and export it as excel
 * special case for hospitalized patient
 * 
 * @author 		: Nurul Huda
 * @license 	: Apache v3
 * @copyright 	: goblooge@gmail.com
 * @used 		: /medical_record/resource/php/lap_index/lap_index_penyakit.php
 * @since		: 17 Feb 2017
 * @version		: 1.0.0
 * @database	: smist_mr_lap_ranap
 * */

require_once "smis-libs-out/php-excel/PHPExcel.php"; 
require_once "smis-base/smis-include-service-consumer.php";
loadLibrary("smis-libs-function-excel");
global $db;
$query_view="
			SELECT nama_icd, tanggal,nrm_pasien, nama_pasien,tgl_masuk,tgl_keluar,lama_dirawat, propinsi, kabupaten, kecamatan, kelurahan, tgl_masuk,tgl_keluar,lama_dirawat,
			if(gol_umur='0 - 28 HR','x','') as '28hr',
			if(gol_umur='28 HR - 1 TH','x','') as '1th',
			if(gol_umur='1-4 TH','x','') as '4th',
			if(gol_umur='5-14 TH','x','') as '14th',
			if(gol_umur='15-24 TH','x','') as '24th',
			if(gol_umur='25-44 TH','x','') as '44th',
			if(gol_umur='45-59 TH','x','') as '59th',
			if(gol_umur='60-64 TH','x','') as '64th',
			if(gol_umur='65 TH+','x','') as '65th',
			if(jk=0,'x','') as laki,
			if(jk=1,'x','') as pr,
			keterangan,
			ruangan as ruangan,
			ifnull(kelas,ruangan) as kelas,
			if(carapulang='Dipulangkan Mati <=48 Jam' OR carapulang='Dipulangkan Mati < 1 Jam Post Operasi','x','') as mk48,
			if(carapulang='Dipulangkan Mati >48 Jam','x','') as ml48,
			if(carapulang='Pulang Paksa','x','') as paksa,
			if(carapulang='Kabur','x','') as kabur,
			if(carapulang='Rujuk RS Lain','x','') as rslain,
			if(carapulang='Dikembalikan ke Perujuk','x','') as perujuk,
			if(carapulang='Dipulangkan Hidup','x','') as hidup
			FROM smis_mr_diagnosa			
	";
	$query_count="SELECT COUNT(*) FROM smis_mr_diagnosa";
	
	$dbtable=new DBTable($db, "smis_mr_diagnosa");
	$dbtable->setPreferredQuery(true,$query_view,$query_count);
	$dbtable->setUseWhereforView(true);
	$dbtable->setOrder(" tanggal ASC");	
	$dbtable->addCustomKriteria(" nama_icd "," like '".$_POST['nama_penyakit']."%' ");
    $dbtable->addCustomKriteria(NULL," tanggal >='".$_POST['dari']."' ");
	$dbtable->addCustomKriteria(NULL," tanggal <'".$_POST['sampai']."' ");
	if($_POST['urji']!="-1"){
		$dbtable->addCustomKriteria(" urji "," = '".$_POST['urji']."' ");
	}
    $dbtable->setFetchMethode(DBTable::$ARRAY_FETCH);
    $dbtable->setShowAll(true);
    
$data=$dbtable->view("","0");
$list=$data['data'];

show_error();
/*start - BLOK RESOURCE*/
	$_thin = array ();
	$_thin['borders']=array();
	$_thin['borders']['allborders']=array();
	$_thin['borders']['allborders']['style']=PHPExcel_Style_Border::BORDER_THIN ;

	$_center = array();
	$_center ['alignment']=array();
	$_center ['alignment']['horizontal']=PHPExcel_Style_Alignment::HORIZONTAL_CENTER;
/*end - BLOCK RESOURCE*/

/*start - BLOCK PROPERTIES FILE EXCEL*/
	$file = new PHPExcel ();
	$file->getProperties ()->setCreator ( $user->getUsername() );
	$file->getProperties ()->setLastModifiedBy ( $user->getNameOnly() );
	$file->getProperties ()->setTitle ( "Laporan Pasien Index Penyakit" );
	$file->getProperties ()->setSubject ( "Laporan Pasien Index Penyakit" );
	$file->getProperties ()->setDescription ( "Data Laporan Pasien Index Penyakit " );
	$file->getProperties ()->setKeywords ( "index, penyakit, rekam, medis" );
	$file->getProperties ()->setCategory ( "Rekam Medis" );
/*end - BLOCK PROPERTIES FILE EXCEL*/

/*start - BLOCK PASIEN MASUK*/
	$file->setActiveSheetIndex ( 0 );
	$sheet = $file->getActiveSheet ( 0 );
    
    $index = 1;
    $sheet  ->setTitle ( "LAPORAN" );	
    $sheet	->mergeCells('A'.$index.':AE'.$index);
	$sheet	->setCellValue ( "A".$index, "LAPORAN INDEX PENYAKIT" );
    $sheet  ->getStyle("A".$index.":AA".$index)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $sheet  ->getStyle("A".$index.":AA".$index)->getFont()->setBold(true);
    $index++;
	$sheet	->setCellValue ( "A".$index, "No." )
			->setCellValue ( "B".$index, "Tanggal" )
			->setCellValue ( "C".$index, "Pasien" )
			->setCellValue ( "D".$index, "NRM" )
			->setCellValue ( "E".$index, "Propinsi" )
			->setCellValue ( "F".$index, "Kabupaten" )
			->setCellValue ( "G".$index, "Kecamatan" )
			->setCellValue ( "H".$index, "Kelurahan" )
			->setCellValue ( "I".$index, "Ruangan" )
			->setCellValue ( "J".$index, "Masuk" )
			->setCellValue ( "K".$index, "Keluar" )
            ->setCellValue ( "L".$index, "Lama Dirawat" )
            ->setCellValue ( "M".$index, "L" )
            ->setCellValue ( "N".$index, "P" )
            ->setCellValue ( "O".$index, "0 - 28 HR" )
            ->setCellValue ( "P".$index, "28 HR - 1 TH" )
            ->setCellValue ( "Q".$index, "1-4 TH" )
            ->setCellValue ( "R".$index, "5-14 TH" )
            ->setCellValue ( "S".$index, "15-24 TH" )
            ->setCellValue ( "T".$index, "25-44 TH" )
            ->setCellValue ( "U".$index, "45-59 TH" )
            ->setCellValue ( "V".$index, "60-64 TH" )
            ->setCellValue ( "W".$index, "65 TH+" )
            ->setCellValue ( "X".$index, "Diagnosa Sekunder" )
            ->setCellValue ( "Y".$index, "Kelas" )
            ->setCellValue ( "Z".$index, "Dipulangkan Mati <=48 Jam" )
            ->setCellValue ( "AA".$index, "Dipulangkan Mati >48 Jam" )
            ->setCellValue ( "AB".$index, "Pulang Paksa" )
            ->setCellValue ( "AC".$index, "Kabur" )
            ->setCellValue ( "AD".$index, "Rujuk RS Lain" )
            ->setCellValue ( "AE".$index, "Dikembalikan ke Perujuk" )
            ->setCellValue ( "AF".$index, "Dipulangkan Hidup" );
	$sheet  ->getStyle ( 'A'.$index.':AF'.$index )
            ->applyFromArray ($_center);
	
	$sheet ->getStyle("A".$index.":AA".$index)->getFont()->setBold(true);
    foreach(range("A","AF") as $columnID) {
        $sheet->getColumnDimension($columnID)->setAutoSize(true);
    }
	$_ruang=array();
	$no=2;
	foreach($list as $x){
		$no++;
        if($x['tanggal'] != '' && $x['tanggal'] != '0000-00-00') {
            date_excel_format($sheet,"B".$no,$x['tanggal']);
        }
        if($x['tgl_masuk'] != '' && $x['tgl_masuk'] != '0000-00-00 00:00:00') {
            date_excel_format($sheet,"J".$no,$x['tgl_masuk']);
        }
        if($x['tgl_keluar'] != '' && $x['tgl_keluar'] != '0000-00-00 00:00:00') {
            date_excel_format($sheet,"K".$no,$x['tgl_keluar']);
        }
        
        $ruangan=ArrayAdapter::format("unslug",$x->ruang);
        if($ruangan=="") $ruangan="-";
		$sheet	->setCellValue ( "A".$no, ($no-2)."." )
                ->setCellValue ( "C".$no, $x['nama_pasien'])
				->setCellValue ( "D".$no, "'".ArrayAdapter::format("only-digit6",$x['nrm_pasien']) )
                ->setCellValue ( "E".$no, $x['propinsi'])
                ->setCellValue ( "F".$no, $x['kabupaten'])
                ->setCellValue ( "G".$no, $x['kecamatan'])
                ->setCellValue ( "H".$no, $x['kelurahan'])
				->setCellValue ( "I".$no, ArrayAdapter::format("unslug",$x['ruangan']) )
				->setCellValue ( "L".$no, $x['lama_dirawat'])
				->setCellValue ( "M".$no, $x['laki'])
                ->setCellValue ( "N".$no, $x['pr'])
                ->setCellValue ( "O".$no, $x['28hr'] )
                ->setCellValue ( "P".$no, $x['1th'] )
                ->setCellValue ( "Q".$no, $x['4th'] )
                ->setCellValue ( "R".$no, $x['14th'] )
                ->setCellValue ( "S".$no, $x['24th'] )
                ->setCellValue ( "T".$no, $x['44th'] )
                ->setCellValue ( "U".$no, $x['59th'] )
                ->setCellValue ( "V".$no, $x['64th'] )
                ->setCellValue ( "W".$no, $x['65th'] )
                ->setCellValue ( "X".$no, $x['keterangan'] )
                ->setCellValue ( "Y".$no, $x['kelas'] )
                ->setCellValue ( "Z".$no, $x['mk48'] )
                ->setCellValue ( "AA".$no, $x['ml48'] )
                ->setCellValue ( "AB".$no, $x['paksa'] )
                ->setCellValue ( "AC".$no, $x['kabur'] )
                ->setCellValue ( "AD".$no, $x['rslain'] )
                ->setCellValue ( "AE".$no, $x['perujuk'] )
                ->setCellValue ( "AF".$no,$x['hidup']  );
        if(!in_array($ruangan,$_ruang)){
            $_ruang[]=$ruangan;
        }
	}
    $sheet	->setCellValue ( "M".$no, "=counta(L3:L".($no-1).")" );
    $sheet	->setCellValue ( "N".$no, "=counta(M3:M".($no-1).")" );
    $sheet	->setCellValue ( "O".$no, "=counta(N3:N".($no-1).")" );
    $sheet	->setCellValue ( "P".$no, "=counta(O3:O".($no-1).")" );
    $sheet	->setCellValue ( "Q".$no, "=counta(P3:P".($no-1).")" );
    $sheet	->setCellValue ( "R".$no, "=counta(Q3:Q".($no-1).")" );
    $sheet	->setCellValue ( "S".$no, "=counta(R3:R".($no-1).")" );
    $sheet	->setCellValue ( "T".$no, "=counta(S3:S".($no-1).")" );
    $sheet	->setCellValue ( "U".$no, "=counta(T3:T".($no-1).")" );
    $sheet	->setCellValue ( "V".$no, "=counta(U3:U".($no-1).")" );
    $sheet	->setCellValue ( "W".$no, "=counta(V3:V".($no-1).")" );
    
    $sheet	->setCellValue ( "Z".$no, "=counta(Y3:Y".($no-1).")" );
	$sheet	->setCellValue ( "AA".$no, "=counta(Z3:Z".($no-1).")" );
	$sheet	->setCellValue ( "AB".$no, "=counta(AA3:AA".($no-1).")" );
	$sheet	->setCellValue ( "AC".$no, "=counta(AB3:AB".($no-1).")" );
	$sheet	->setCellValue ( "AD".$no, "=counta(AC3:AC".($no-1).")" );
	$sheet	->setCellValue ( "AE".$no, "=counta(AD3:AD".($no-1).")" );
	$sheet	->setCellValue ( "AF".$no, "=counta(AE3:AE".($no-1).")" );
	$sheet->getStyle ( 'A2:AF'.$no )->applyFromArray ($_thin);
/*end - BLOCK PASIEN MASUK*/
        
$filename= "DATA LAPORAN INDEX PENYAKIT - ".
           " - ".ArrayAdapter::format("date d M Y",$_POST['dari']).
           " - ".ArrayAdapter::format("date d M Y",$_POST['sampai']).".xls";

header ( 'Content-Type: application/vnd.ms-excel' );
header ( 'Content-Disposition: attachment;filename="'.$filename.'"' );
header ( 'Cache-Control: max-age=0' );
$writer = PHPExcel_IOFactory::createWriter ( $file, 'Excel5' );
$writer->save ( 'php://output' );

?>