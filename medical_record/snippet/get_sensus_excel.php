<?php 
/**
 * this file used for get all patient 
 * on certain room. the data will be 
 * parse into excel.
 * 
 * @author 		: Nurul Huda
 * @license 	: Apache v3
 * @copyright 	: goblooge@gmail.com
 * @used 		: /medical_record/resource/php/laporan_pasien/lap_sensus.php
 * @service 	: get_sensus
 * @since		: 29 Oct 2016
 * @version		: 1.0.0
 * @database	: smis_mr_sensus
 * */



require_once "smis-libs-out/php-excel/PHPExcel.php"; 
global $db;
global $user;
$_dbtable=new DBTable($db,"smis_mr_sensus");
$_dari=$_POST['dari'];
$_sampai=$_POST['sampai'];
$_slug_igd=getSettings($db,"smis-rs-igd-mr","igd");

/*start - BLOK RESOURCE*/
	$_thin = array ();
	$_thin['borders']=array();
	$_thin['borders']['allborders']=array();
	$_thin['borders']['allborders']['style']=PHPExcel_Style_Border::BORDER_DASHED ;

	$_center = array();
	$_center ['alignment']=array();
	$_center ['alignment']['horizontal']=PHPExcel_Style_Alignment::HORIZONTAL_CENTER;
/*end - BLOCK RESOURCE*/

/*start - BLOCK PROPERTIES FILE EXCEL*/
	$file = new PHPExcel ();
	$file->getProperties ()->setCreator ( $user->getUsername() );
	$file->getProperties ()->setLastModifiedBy ( $user->getNameOnly() );
	$file->getProperties ()->setTitle ( "Sensus Harian Pasien" );
	$file->getProperties ()->setSubject ( "Sensus Rekam Medis" );
	$file->getProperties ()->setDescription ( "Data Rekam Medis Sensus Harian Pasien ".$_POST['ruangan'] );
	$file->getProperties ()->setKeywords ( "sensus, harian, rekam, medis" );
	$file->getProperties ()->setCategory ( "Rekam Medis" );
/*end - BLOCK PROPERTIES FILE EXCEL*/

/*start - BLOCK PASIEN MASUK*/
	$file->setActiveSheetIndex ( 0 );
	$sheet = $file->getActiveSheet ( 0 );
	$sheet->setTitle ( "PASIEN MASUK" );
	$_dbtable->addCustomKriteria(NULL," tgl_masuk<'".$_sampai."' " );
	$_dbtable->addCustomKriteria(NULL," tgl_masuk>='".$_dari."' " );
	$_dbtable->addCustomKriteria(NULL," ( 	asal_ruangan='Pendaftaran' 
											OR asal_ruangan='' 
											OR asal_ruangan LIKE '%".$_slug_igd."%' 
										)" );
	$_dbtable->setShowAll(true);
	$package=$_dbtable->view("","0");
	$list=$package["data"];
    
    $i = 1;
    $objDrawing = new PHPExcel_Worksheet_Drawing();
    $objDrawing->setName('logo_img');
    $objDrawing->setDescription('logo_img');
    $objDrawing->setPath(getLogoNonInterlaced());
    $objDrawing->setCoordinates('A'.$i);
    $objDrawing->setOffsetX(20); 
    $objDrawing->setOffsetY(5);
    $objDrawing->setWidth(100); 
    $objDrawing->setHeight(100);
    $objDrawing->setWorksheet($sheet);
    $i++;
    
    $sheet->setCellValue("C".$i, getSettings($db,"smis_autonomous_title"));
    $sheet->getStyle("C".$i)->getFont()->setBold(true)->setSize(14);
    $i++;
    $sheet->setCellValue("C".$i, getSettings($db,"smis_autonomous_address"));
    $sheet->getStyle("C".$i)->getFont()->setBold(true)->setSize(14);
    $i++;
    $sheet->setCellValue("C".$i, getSettings($db,"smis_autonomous_contact"));
    $sheet->getStyle("C".$i)->getFont()->setBold(true)->setSize(14);
    $i += 3;
        
	$sheet	->mergeCells('A'.$i.':N'.$i);
	$sheet	->setCellValue ( "A".$i, "PASIEN MASUK" );
    $sheet->getStyle ( 'A'.$i.':K'.$i )->applyFromArray ($_center);
    $sheet->getStyle ( 'A'.$i.':K'.$i )->getFont()->setBold(true);
    $i++;
    $border_start = $i;
	$sheet	->setCellValue ( "A".$i, "No." )
			->setCellValue ( "B".$i, "Tanggal Masuk" )
			->setCellValue ( "C".$i, "NRM" )
			->setCellValue ( "D".$i, "No Profile" )
			->setCellValue ( "E".$i, "No Reg" )
			->setCellValue ( "F".$i, "Nama" )
			->setCellValue ( "G".$i, "L/P" )
			->setCellValue ( "H".$i, "Alamat" )
			->setCellValue ( "I".$i, "Umur" )
			->setCellValue ( "J".$i, "Diagnosa" )
            ->setCellValue ( "K".$i, "ICD" )
            ->setCellValue ( "L".$i, "DPJP" )
            ->setCellValue ( "M".$i, "Cara Bayar" )
            ->setCellValue ( "N".$i, "Kunjungan RS" );
	
	$sheet ->getColumnDimension ( "A" )->setAutoSize ( true );
	$sheet ->getColumnDimension ( "B" )->setAutoSize ( true );
	$sheet ->getColumnDimension ( "C" )->setAutoSize ( true );
	$sheet ->getColumnDimension ( "D" )->setAutoSize ( true );
	$sheet ->getColumnDimension ( "E" )->setAutoSize ( true );
	$sheet ->getColumnDimension ( "F" )->setAutoSize ( true );
	$sheet ->getColumnDimension ( "G" )->setAutoSize ( true );
	$sheet ->getColumnDimension ( "H" )->setAutoSize ( true );
	$sheet ->getColumnDimension ( "I" )->setAutoSize ( true );
	$sheet ->getColumnDimension ( "J" )->setAutoSize ( true );
	$sheet ->getColumnDimension ( "K" )->setAutoSize ( true );
	$sheet ->getColumnDimension ( "L" )->setAutoSize ( true );
	$sheet ->getColumnDimension ( "M" )->setAutoSize ( true );
	$sheet ->getColumnDimension ( "N" )->setAutoSize ( true );
    $sheet->getStyle ( 'A'.$i.':N'.$i )->applyFromArray ($_center);
    $sheet->getStyle ( 'A'.$i.':N'.$i )->getFont()->setBold(true);
	
	$no = $i;
	foreach($list as $x){
		$no++;
		$sheet	->setCellValue ( "A".$no, ($no-$i)."." )
				->setCellValue ( "B".$no, $x->tgl_masuk )
				->setCellValue ( "C".$no, "'".ArrayAdapter::format("only-digit6",$x->nrm_pasien) )
				->setCellValue ( "D".$no, "'".ArrayAdapter::format("only-digit6",$x->profile_number) )
				->setCellValue ( "E".$no, "'".ArrayAdapter::format("only-digit6",$x->noreg_pasien) )
				->setCellValue ( "F".$no, $x->nama_pasien )
				->setCellValue ( "G".$no, $x->nama_pasien )
				->setCellValue ( "H".$no, $x->jk==0?"L":"P" )
				->setCellValue ( "I".$no, $x->alamat )
				->setCellValue ( "J".$no, $x->diagnosa_masuk )
                ->setCellValue ( "K".$no, $x->icd_masuk )
                ->setCellValue ( "L".$no, $x->dpjp )
                ->setCellValue ( "M".$no, $x->carabayar )
                ->setCellValue ( "N".$no, $x->kunjungan_rs );
	}
	$sheet->getStyle ( 'A'.$border_start.':N'.$no )->applyFromArray ($_thin);
	
/*end - BLOCK PASIEN MASUK*/

/*start - BLOCK PASIEN PINDAHAN*/
	$file->createSheet ( NULL,1);
	$file->setActiveSheetIndex ( 1 );
	$sheet = $file->getActiveSheet ( 1 );
	$sheet->setTitle ( "PASIEN PINDAHAN" );
	$_dbtable->setCustomKriteria(array());
	$_dbtable->addCustomKriteria(NULL," tgl_masuk<'".$_sampai."' " );
	$_dbtable->addCustomKriteria(NULL," tgl_masuk>='".$_dari."' " );
	$_dbtable->addCustomKriteria(NULL," ( 	asal_ruangan!='Pendaftaran' 
											AND asal_ruangan!='' 
										)" );
	$_dbtable->setShowAll(true);
	$package=$_dbtable->view("","0");
	$list=$package["data"];
    
    $i = 1; 
    $objDrawing = new PHPExcel_Worksheet_Drawing();
    $objDrawing->setName('logo_img');
    $objDrawing->setDescription('logo_img');
    $objDrawing->setPath(getLogoNonInterlaced());
    $objDrawing->setCoordinates('A'.$i);
    $objDrawing->setOffsetX(20); 
    $objDrawing->setOffsetY(5);
    $objDrawing->setWidth(100); 
    $objDrawing->setHeight(100);
    $objDrawing->setWorksheet($sheet);
    $i++;
    
    $sheet->setCellValue("C".$i, getSettings($db,"smis_autonomous_title"));
    $sheet->getStyle("C".$i)->getFont()->setBold(true)->setSize(14);
    $i++;
    $sheet->setCellValue("C".$i, getSettings($db,"smis_autonomous_address"));
    $sheet->getStyle("C".$i)->getFont()->setBold(true)->setSize(14);
    $i++;
    $sheet->setCellValue("C".$i, getSettings($db,"smis_autonomous_contact"));
    $sheet->getStyle("C".$i)->getFont()->setBold(true)->setSize(14);
    $i += 3;
    
	$sheet	->mergeCells('A'.$i.':O'.$i);
	$sheet	->setCellValue ( "A".$i, "PASIEN PINDAHAN" );
    $sheet->getStyle ( 'A'.$i.':O'.$i )->applyFromArray ($_center);
    $sheet->getStyle ( 'A'.$i.':O'.$i )->getFont()->setBold(true);
    $i++;
    $border_start = $i;
	$sheet	->setCellValue ( "A".$i, "No." )
            ->setCellValue ( "B".$i, "Tanggal Masuk" )
			->setCellValue ( "C".$i, "NRM" )
			->setCellValue ( "D".$i, "No Profile" )
			->setCellValue ( "E".$i, "No Reg" )
			->setCellValue ( "F".$i, "Nama" )
			->setCellValue ( "G".$i, "L/P" )
			->setCellValue ( "H".$i, "Alamat" )
			->setCellValue ( "I".$i, "Umur" )
			->setCellValue ( "J".$i, "Pindahan Dari" )
			->setCellValue ( "K".$i, "Diagnosa" )
            ->setCellValue ( "L".$i, "ICD" )
            ->setCellValue ( "M".$i, "DPJP" )
            ->setCellValue ( "N".$i, "Cara Bayar" )
            ->setCellValue ( "O".$i, "Kunjungan RS" );
	
		
	$sheet ->getColumnDimension ( "A" )->setAutoSize ( true );
	$sheet ->getColumnDimension ( "B" )->setAutoSize ( true );
	$sheet ->getColumnDimension ( "C" )->setAutoSize ( true );
	$sheet ->getColumnDimension ( "D" )->setAutoSize ( true );
	$sheet ->getColumnDimension ( "E" )->setAutoSize ( true );
	$sheet ->getColumnDimension ( "F" )->setAutoSize ( true );
	$sheet ->getColumnDimension ( "G" )->setAutoSize ( true );
	$sheet ->getColumnDimension ( "H" )->setAutoSize ( true );
	$sheet ->getColumnDimension ( "I" )->setAutoSize ( true );
	$sheet ->getColumnDimension ( "J" )->setAutoSize ( true );
	$sheet ->getColumnDimension ( "K" )->setAutoSize ( true );
    $sheet ->getColumnDimension ( "L" )->setAutoSize ( true );
    $sheet ->getColumnDimension ( "M" )->setAutoSize ( true );
    $sheet ->getColumnDimension ( "N" )->setAutoSize ( true );
    $sheet ->getColumnDimension ( "O" )->setAutoSize ( true );
	$sheet->getStyle ( 'A'.$i.':O'.$i )->applyFromArray ($_center);
    $sheet->getStyle ( 'A'.$i.':O'.$i )->getFont()->setBold(true);
    $i++;
	
	$no = $i;
	foreach($list as $x){
		$no++;
		$sheet	->setCellValue ( "A".$no, ($no-$i)."." )
				->setCellValue ( "B".$no, $x->tgl_masuk )
				->setCellValue ( "C".$no, "'".ArrayAdapter::format("only-digit6",$x->nrm_pasien) )
				->setCellValue ( "D".$no, "'".ArrayAdapter::format("only-digit6",$x->profile_number) )
				->setCellValue ( "E".$no, "'".ArrayAdapter::format("only-digit6",$x->noreg_pasien) )
				->setCellValue ( "F".$no, $x->nama_pasien )
				->setCellValue ( "G".$no, $x->jk==0?"L":"P" )
				->setCellValue ( "H".$no, $x->alamat )
				->setCellValue ( "I".$no, $x->umur )
				->setCellValue ( "J".$no, ArrayAdapter::format("unslug",$x->asal_ruangan) )
				->setCellValue ( "K".$no, $x->diagnosa_ruang )
                ->setCellValue ( "L".$no, $x->icd_ruang )
                ->setCellValue ( "M".$no, $x->dpjp )
                ->setCellValue ( "N".$no, $x->carabayar )
                ->setCellValue ( "O".$no, $x->kunjungan_rs );
	}
	$sheet->getStyle ( 'A'.$border_start.':O'.$no )->applyFromArray ($_thin);
/*end - BLOCK PASIEN PINDAHAN*/


/*start - BLOCK PASIEN MENINGGAL*/
	$file->createSheet ( NULL,2);
	$file->setActiveSheetIndex ( 2 );
	$sheet = $file->getActiveSheet ( 2 );
	$sheet->setTitle ( "PASIEN MENINGGAL" );
	$_dbtable->setCustomKriteria(array());
	$_dbtable->addCustomKriteria(" tgl_pulang "," !='0000-00-00 00:00:00' " );
    $_dbtable->addCustomKriteria(NULL," tgl_pulang<'".$_sampai."' " );
	$_dbtable->addCustomKriteria(NULL," tgl_pulang>='".$_dari."' " );
	$_dbtable->addCustomKriteria(" carapulang "," LIKE '%Mati%' " );
	$_dbtable->setShowAll(true);
	$package=$_dbtable->view("","0");
	$list=$package["data"];
    
    $i = 1;
    $objDrawing = new PHPExcel_Worksheet_Drawing();
    $objDrawing->setName('logo_img');
    $objDrawing->setDescription('logo_img');
    $objDrawing->setPath(getLogoNonInterlaced());
    $objDrawing->setCoordinates('A'.$i);
    $objDrawing->setOffsetX(20); 
    $objDrawing->setOffsetY(5);
    $objDrawing->setWidth(100); 
    $objDrawing->setHeight(100);
    $objDrawing->setWorksheet($sheet);
    $i++;
    
    $sheet->setCellValue("C".$i, getSettings($db,"smis_autonomous_title"));
    $sheet->getStyle("C".$i)->getFont()->setBold(true)->setSize(14);
    $i++;
    $sheet->setCellValue("C".$i, getSettings($db,"smis_autonomous_address"));
    $sheet->getStyle("C".$i)->getFont()->setBold(true)->setSize(14);
    $i++;
    $sheet->setCellValue("C".$i, getSettings($db,"smis_autonomous_contact"));
    $sheet->getStyle("C".$i)->getFont()->setBold(true)->setSize(14);
    $i += 3;
    
	$sheet	->mergeCells('A'.$i.':Q'.$i);
    $sheet	->setCellValue ( "A".$i, "PASIEN MENINGGAL" );
    $sheet->getStyle ( 'A'.$i.':Q'.$i )->applyFromArray ($_center);
    $sheet->getStyle ( 'A'.$i.':Q'.$i )->getFont()->setBold(true);
    $i++;
    $ips = $i + 1;
    
	$sheet	->mergeCells('A'.$i.':A'.$ips);
	$sheet	->mergeCells('B'.$i.':B'.$ips);
	$sheet	->mergeCells('C'.$i.':C'.$ips);
	$sheet	->mergeCells('D'.$i.':D'.$ips);
	$sheet	->mergeCells('E'.$i.':E'.$ips);
	$sheet	->mergeCells('F'.$i.':F'.$ips);
	$sheet	->mergeCells('G'.$i.':G'.$ips);
	$sheet	->mergeCells('H'.$i.':H'.$ips);
	$sheet	->mergeCells('I'.$i.':I'.$ips);
	$sheet	->mergeCells('J'.$i.':K'.$i);
	$sheet	->mergeCells('L'.$i.':L'.$ips);
	$sheet	->mergeCells('M'.$i.':M'.$ips);
	$sheet	->mergeCells('N'.$i.':N'.$ips);
	$sheet	->mergeCells('O'.$i.':O'.$ips);
	$sheet	->mergeCells('P'.$i.':P'.$ips);
	$sheet	->mergeCells('Q'.$i.':Q'.$ips);
    
	$sheet	->setCellValue ( "A".$i, "No." )
			->setCellValue ( "B".$i, "Tanggal Keluar" )
			->setCellValue ( "C".$i, "NRM" )
			->setCellValue ( "D".$i, "No Profile" )
			->setCellValue ( "E".$i, "No Reg" )
			->setCellValue ( "F".$i, "Nama" )
			->setCellValue ( "G".$i, "L/P" )
			->setCellValue ( "H".$i, "Alamat" )
			->setCellValue ( "I".$i, "Umur" )
			->setCellValue ( "J".$i, "Meninggal" )
			->setCellValue ( "J".$ips, "<= 48 Jam" )
			->setCellValue ( "K".$ips, "> 48 Jam" )
			->setCellValue ( "L".$i, "Lama Dirawat" )
			->setCellValue ( "M".$i, "Diagnosa" )
            ->setCellValue ( "N".$i, "ICD" )
            ->setCellValue ( "O".$i, "DPJP" )
            ->setCellValue ( "P".$i, "Cara Bayar" )
            ->setCellValue ( "Q".$i, "Kunjungan RS" );
	$sheet->getStyle ( 'A'.$i.':Q'.$i )->applyFromArray ($_center);
	$sheet->getStyle ( 'A'.$i.':Q'.$i )->getFont()->setBold(true);
    $i++;
    
	$no = $i;
    $sum_start = $i;
	foreach($list as $x){
		$no++;
		$sheet	->setCellValue ( "A".$no, ($no-$i)."." )
				->setCellValue ( "B".$no, $x->tgl_pulang )
                ->setCellValue ( "C".$no, "'".ArrayAdapter::format("only-digit6",$x->nrm_pasien) )
                ->setCellValue ( "D".$no, "'".ArrayAdapter::format("only-digit6",$x->profile_number) )
				->setCellValue ( "E".$no, "'".ArrayAdapter::format("only-digit6",$x->noreg_pasien) )
				->setCellValue ( "F".$no, $x->nama_pasien )
				->setCellValue ( "G".$no, $x->jk==0?"L":"P" )
				->setCellValue ( "H".$no, $x->alamat )
				->setCellValue ( "I".$no, $x->umur )
				->setCellValue ( "J".$no, strpos($x->carapulang,">48 Jam")===false?1:"" )
				->setCellValue ( "K".$no, strpos($x->carapulang,">48 Jam")===false?"":1 )
				->setCellValue ( "L".$no, $x->durasi )
				->setCellValue ( "M".$no, $x->diagnosa_ruang )
                ->setCellValue ( "N".$no, $x->icd_ruang )
                ->setCellValue ( "O".$no, $x->dpjp )
                ->setCellValue ( "P".$no, $x->carabayar )
                ->setCellValue ( "Q".$no, $x->kunjungan_rs );
	}
	$sheet	->setCellValue ( "I".($no+1), "JUMLAH" );
	$sheet	->setCellValue ( "J".($no+1), "=SUM(J".$sum_start.":J".$no.")" );
	$sheet	->setCellValue ( "K".($no+1), "=SUM(K".$sum_start.":K".$no.")" );
    
	$sheet ->getStyle("F".($no+1).":H".($no+1))->getFont()->setBold(true);
	$sheet->getStyle ( 'A'.$border_start.':Q'.$no )->applyFromArray ($_thin);
    
	$sheet ->getColumnDimension ( "A" )->setAutoSize ( true );
	$sheet ->getColumnDimension ( "B" )->setAutoSize ( true );
	$sheet ->getColumnDimension ( "C" )->setAutoSize ( true );
	$sheet ->getColumnDimension ( "D" )->setAutoSize ( true );
	$sheet ->getColumnDimension ( "E" )->setAutoSize ( true );
	$sheet ->getColumnDimension ( "F" )->setAutoSize ( true );
	$sheet ->getColumnDimension ( "G" )->setAutoSize ( true );
	$sheet ->getColumnDimension ( "H" )->setAutoSize ( true );
	$sheet ->getColumnDimension ( "I" )->setAutoSize ( true );
    $sheet ->getColumnDimension ( "J" )->setAutoSize ( true );
    $sheet ->getColumnDimension ( "K" )->setAutoSize ( true );
    $sheet ->getColumnDimension ( "L" )->setAutoSize ( true );
    $sheet ->getColumnDimension ( "M" )->setAutoSize ( true );
    $sheet ->getColumnDimension ( "N" )->setAutoSize ( true );
    $sheet ->getColumnDimension ( "O" )->setAutoSize ( true );
    $sheet ->getColumnDimension ( "P" )->setAutoSize ( true );
    $sheet ->getColumnDimension ( "Q" )->setAutoSize ( true );
	
/*end - BLOCK PASIEN MENINGGAL*/


/*start - BLOCK PASIEN DIPINDAHKAN*/
	$file->createSheet ( NULL,3);
	$file->setActiveSheetIndex ( 3 );
	$sheet = $file->getActiveSheet ( 3 );
	$sheet->setTitle ( "PASIEN DIPINDAHKAN" );
	$_dbtable->setCustomKriteria(array());
	$_dbtable->addCustomKriteria(NULL," tgl_pulang!='0000-00-00 00:00:00' " );
	$_dbtable->addCustomKriteria(NULL," tgl_pulang<'".$_sampai."' " );
	$_dbtable->addCustomKriteria(NULL," tgl_pulang>='".$_dari."' " );
	$_dbtable->addCustomKriteria(" carapulang "," = 'Pindah Kamar' " );
	$_dbtable->setShowAll(true);
	$package=$_dbtable->view("","0");
	$list=$package["data"];
    
    $i = 1;
    $objDrawing = new PHPExcel_Worksheet_Drawing();
    $objDrawing->setName('logo_img');
    $objDrawing->setDescription('logo_img');
    $objDrawing->setPath(getLogoNonInterlaced());
    $objDrawing->setCoordinates('A'.$i);
    $objDrawing->setOffsetX(20); 
    $objDrawing->setOffsetY(5);
    $objDrawing->setWidth(100); 
    $objDrawing->setHeight(100);
    $objDrawing->setWorksheet($sheet);
    $i++;
    
    $sheet->setCellValue("C".$i, getSettings($db,"smis_autonomous_title"));
    $sheet->getStyle("C".$i)->getFont()->setBold(true)->setSize(14);
    $i++;
    $sheet->setCellValue("C".$i, getSettings($db,"smis_autonomous_address"));
    $sheet->getStyle("C".$i)->getFont()->setBold(true)->setSize(14);
    $i++;
    $sheet->setCellValue("C".$i, getSettings($db,"smis_autonomous_contact"));
    $sheet->getStyle("C".$i)->getFont()->setBold(true)->setSize(14);
    $i += 3;
    
	$sheet	->mergeCells('A'.$i.':P'.$i);
	$sheet	->setCellValue ( "A".$i, "PASIEN DIPINDAHKAN" );
    $sheet->getStyle ( 'A'.$i.':P'.$i )->applyFromArray ($_center);
    $sheet->getStyle ( 'A'.$i.':P'.$i )->getFont()->setBold(true);
    $i++;
    
    $border_start = $i;
	$sheet	->setCellValue ( "A".$i, "No." )
            ->setCellValue ( "B".$i, "Tanggal Keluar" )    
			->setCellValue ( "C".$i, "NRM" )
			->setCellValue ( "D".$i, "No Profile" )
			->setCellValue ( "E".$i, "No Reg" )
			->setCellValue ( "F".$i, "Nama" )
			->setCellValue ( "G".$i, "L/P" )
			->setCellValue ( "H".$i, "Alamat" )
			->setCellValue ( "I".$i, "Umur" )
			->setCellValue ( "J".$i, "Dipindahkan Ke" )
			->setCellValue ( "K".$i, "Lama Dirawat" )
			->setCellValue ( "L".$i, "Diagnosa" )
            ->setCellValue ( "M".$i, "ICD" )
            ->setCellValue ( "N".$i, "DPJP" )
            ->setCellValue ( "O".$i, "Cara Bayar" )
            ->setCellValue ( "P".$i, "Kunjungan RS" );
		
	$sheet ->getColumnDimension ( "A" )->setAutoSize ( true );
	$sheet ->getColumnDimension ( "B" )->setAutoSize ( true );
	$sheet ->getColumnDimension ( "C" )->setAutoSize ( true );
	$sheet ->getColumnDimension ( "D" )->setAutoSize ( true );
	$sheet ->getColumnDimension ( "E" )->setAutoSize ( true );
	$sheet ->getColumnDimension ( "F" )->setAutoSize ( true );
	$sheet ->getColumnDimension ( "G" )->setAutoSize ( true );
	$sheet ->getColumnDimension ( "H" )->setAutoSize ( true );
    $sheet ->getColumnDimension ( "I" )->setAutoSize ( true );
    $sheet ->getColumnDimension ( "J" )->setAutoSize ( true );
    $sheet ->getColumnDimension ( "K" )->setAutoSize ( true );
    $sheet ->getColumnDimension ( "L" )->setAutoSize ( true );
    $sheet ->getColumnDimension ( "M" )->setAutoSize ( true );
    $sheet ->getColumnDimension ( "N" )->setAutoSize ( true );
    $sheet ->getColumnDimension ( "O" )->setAutoSize ( true );
    $sheet ->getColumnDimension ( "P" )->setAutoSize ( true );
    $sheet->getStyle ( 'A'.$i.':P'.$i )->applyFromArray ($_center);
    $sheet->getStyle ( 'A'.$i.':P'.$i )->getFont()->setBold(true);
    $i++;
	
	$no = $i;
	foreach($list as $x){
		$no++;
		$sheet	->setCellValue ( "A".$no, ($no-$i)."." )
                ->setCellValue ( "B".$no, $x->tgl_pulang )
				->setCellValue ( "C".$no, "'".ArrayAdapter::format("only-digit6",$x->nrm_pasien) )
				->setCellValue ( "D".$no, "'".ArrayAdapter::format("only-digit6",$x->profile_number) )
				->setCellValue ( "E".$no, "'".ArrayAdapter::format("only-digit6",$x->noreg_pasien) )
				->setCellValue ( "F".$no, $x->nama_pasien )
				->setCellValue ( "G".$no, $x->jk==0?"L":"P" )
				->setCellValue ( "H".$no, $x->alamat )
				->setCellValue ( "I".$no, $x->umur )
				->setCellValue ( "J".$no, ArrayAdapter::format("unslug",$x->pindah_ke) )
				->setCellValue ( "K".$no, $x->durasi )
				->setCellValue ( "L".$no, $x->diagnosa_ruang )
                ->setCellValue ( "M".$no, $x->icd_ruang )
                ->setCellValue ( "N".$no, $x->dpjp )
                ->setCellValue ( "O".$no, $x->carabayar )
                ->setCellValue ( "P".$no, $x->kunjungan_rs );
	}
	$sheet->getStyle ( 'A'.$border_start.':P'.$no )->applyFromArray ($_thin);
/*end - BLOCK PASIEN DIPINDAHKAN*/

/*start - BLOCK PASIEN KELUAR RS*/
	$file->createSheet ( NULL,4);
	$file->setActiveSheetIndex ( 4 );
	$sheet = $file->getActiveSheet ( 4 );
	$sheet->setTitle ( "PASIEN KELUAR RS" );
	$_dbtable->setCustomKriteria(array());
	$_dbtable->addCustomKriteria(NULL," tgl_pulang<'".$_sampai."' " );
    $_dbtable->addCustomKriteria(NULL," tgl_pulang>='".$_dari."' " );
	$_dbtable->addCustomKriteria(NULL," tgl_pulang!='0000-00-00 00:00:00' " );
	$_dbtable->setShowAll(true);
	$package=$_dbtable->view("","0");
	$list=$package["data"];
    
    $i = 1;
    $objDrawing = new PHPExcel_Worksheet_Drawing();
    $objDrawing->setName('logo_img');
    $objDrawing->setDescription('logo_img');
    $objDrawing->setPath(getLogoNonInterlaced());
    $objDrawing->setCoordinates('A'.$i);
    $objDrawing->setOffsetX(20); 
    $objDrawing->setOffsetY(5);
    $objDrawing->setWidth(100); 
    $objDrawing->setHeight(100);
    $objDrawing->setWorksheet($sheet);
    $i++;
    
    $sheet->setCellValue("C".$i, getSettings($db,"smis_autonomous_title"));
    $sheet->getStyle("C".$i)->getFont()->setBold(true)->setSize(14);
    $i++;
    $sheet->setCellValue("C".$i, getSettings($db,"smis_autonomous_address"));
    $sheet->getStyle("C".$i)->getFont()->setBold(true)->setSize(14);
    $i++;
    $sheet->setCellValue("C".$i, getSettings($db,"smis_autonomous_contact"));
    $sheet->getStyle("C".$i)->getFont()->setBold(true)->setSize(14);
    $i += 3;
    
	$sheet	->mergeCells('A'.$i.':M'.$i);
	$sheet	->setCellValue ( "A".$i, "PASIEN KELUAR RS" );
    $sheet->getStyle ( 'A'.$i.':P'.$i )->applyFromArray ($_center);
    $sheet->getStyle ( 'A'.$i.':P'.$i )->getFont()->setBold(true);
    $i++;
    
	$sheet	->setCellValue ( "A".$i, "No." )
			->setCellValue ( "B".$i, "Tanggal Keluar" )
			->setCellValue ( "C".$i, "NRM" )
			->setCellValue ( "D".$i, "No Profile" )
			->setCellValue ( "E".$i, "No Reg" )
			->setCellValue ( "F".$i, "Nama" )
			->setCellValue ( "G".$i, "L/P" )
			->setCellValue ( "H".$i, "Alamat" )
			->setCellValue ( "I".$i, "Umur" )
			->setCellValue ( "J".$i, "Cara Keluar" )
			->setCellValue ( "K".$i, "Lama Dirawat" )
			->setCellValue ( "L".$i, "Diagnosa" )
            ->setCellValue ( "M".$i, "ICD" )
            ->setCellValue ( "N".$i, "DPJP" )
            ->setCellValue ( "O".$i, "Cara Bayar" )
            ->setCellValue ( "P".$i, "Kunjungan RS" );
	$sheet->getStyle ( 'A'.$i.':P'.$i )->applyFromArray ($_center);
    $sheet->getStyle ( 'A'.$i.':P'.$i )->getFont()->setBold(true);
		
	$sheet ->getColumnDimension ( "A" )->setAutoSize ( true );
	$sheet ->getColumnDimension ( "B" )->setAutoSize ( true );
	$sheet ->getColumnDimension ( "C" )->setAutoSize ( true );
	$sheet ->getColumnDimension ( "D" )->setAutoSize ( true );
	$sheet ->getColumnDimension ( "E" )->setAutoSize ( true );
	$sheet ->getColumnDimension ( "F" )->setAutoSize ( true );
	$sheet ->getColumnDimension ( "G" )->setAutoSize ( true );
	$sheet ->getColumnDimension ( "H" )->setAutoSize ( true );
    $sheet ->getColumnDimension ( "I" )->setAutoSize ( true );
    $sheet ->getColumnDimension ( "J" )->setAutoSize ( true );
    $sheet ->getColumnDimension ( "K" )->setAutoSize ( true );
    $sheet ->getColumnDimension ( "L" )->setAutoSize ( true );
    $sheet ->getColumnDimension ( "M" )->setAutoSize ( true );
	
	$no = $i;
	foreach($list as $x){
		$no++;
		$sheet	->setCellValue ( "A".$no, ($no-$i)."." )
				->setCellValue ( "B".$no, $x->tgl_pulang )
				->setCellValue ( "C".$no, "'".ArrayAdapter::format("only-digit6",$x->nrm_pasien) )
				->setCellValue ( "D".$no, "'".ArrayAdapter::format("only-digit6",$x->profile_number) )
				->setCellValue ( "E".$no, "'".ArrayAdapter::format("only-digit6",$x->noreg_pasien) )
				->setCellValue ( "F".$no, $x->nama_pasien )
				->setCellValue ( "G".$no, $x->jk==0?"L":"P" )
				->setCellValue ( "H".$no, $x->alamat )
				->setCellValue ( "I".$no, $x->umur )
				->setCellValue ( "J".$no, $x->carapulang )
				->setCellValue ( "K".$no, $x->durasi )
				->setCellValue ( "L".$no, $x->diagnosa_ruang )
                ->setCellValue ( "M".$no, $x->icd_ruang )
                ->setCellValue ( "N".$no, $x->dpjp )
                ->setCellValue ( "O".$no, $x->carabayar )
                ->setCellValue ( "P".$no, $x->kunjungan_rs );
	}
	$sheet->getStyle ( 'A'.$border_start.':P'.$no )->applyFromArray ($_thin);
/*end - BLOCK PASIEN KELUAR RS*/

/*start - BLOCK RANGKUMAN*/
	$file->createSheet ( NULL,5);
	$file->setActiveSheetIndex ( 5 );
	$sheet = $file->getActiveSheet ( 5 );
	$sheet->setTitle ( "RANGKUMAN" );
	$_dbtable1=new DBTable($db,"smis_mr_sensus");
	$_dbtable1->setShowAll(true);
	$package=$_dbtable1->view("","0");
	$list=$package["data"];
    
    $i = 1;
    $objDrawing = new PHPExcel_Worksheet_Drawing();
    $objDrawing->setName('logo_img');
    $objDrawing->setDescription('logo_img');
    $objDrawing->setPath(getLogoNonInterlaced());
    $objDrawing->setCoordinates('A'.$i);
    $objDrawing->setOffsetX(20); 
    $objDrawing->setOffsetY(5);
    $objDrawing->setWidth(100); 
    $objDrawing->setHeight(100);
    $objDrawing->setWorksheet($sheet);
    $i++;
    
    $sheet->setCellValue("C".$i, getSettings($db,"smis_autonomous_title"));
    $sheet->getStyle("C".$i)->getFont()->setBold(true)->setSize(14);
    $i++;
    $sheet->setCellValue("C".$i, getSettings($db,"smis_autonomous_address"));
    $sheet->getStyle("C".$i)->getFont()->setBold(true)->setSize(14);
    $i++;
    $sheet->setCellValue("C".$i, getSettings($db,"smis_autonomous_contact"));
    $sheet->getStyle("C".$i)->getFont()->setBold(true)->setSize(14);
    $i += 3;
    
	$sheet	->mergeCells('A'.$i.':W'.$i);
	$sheet	->setCellValue ( "A".$i, "RANGKUMAN" );
    $sheet->getStyle ( 'A'.$i.':W'.$i )->applyFromArray ($_center);
    $sheet->getStyle ( 'A'.$i.':W'.$i )->getFont()->setBold(true);
    $i++;
    
	$sheet	->setCellValue ( "A".$i, "No." )
			->setCellValue ( "B".$i, "No Reg" )
			->setCellValue ( "C".$i, "NRM" )
			->setCellValue ( "D".$i, "No Profile" )
			->setCellValue ( "E".$i, "Nama" )
			->setCellValue ( "F".$i, "L/P" )
			->setCellValue ( "G".$i, "Alamat" )
			->setCellValue ( "H".$i, "Diagnosa Masuk" )
			->setCellValue ( "I".$i, "Diagnosa Ruang" )
			->setCellValue ( "J".$i, "Cara Pulang" )
			->setCellValue ( "K".$i, "Cara Bayar" )
			->setCellValue ( "L".$i, "Asal" )
            ->setCellValue ( "M".$i, "Ruangan" )
            ->setCellValue ( "N".$i, "Pindah ke" )
            ->setCellValue ( "O".$i, "Umur" )
            ->setCellValue ( "P".$i, "Kelas" )
            ->setCellValue ( "Q".$i, "Masuk" )
            ->setCellValue ( "R".$i, "Keluar" )
            ->setCellValue ( "S".$i, "Durasi" )
            ->setCellValue ( "T".$i, "DPJP" )
            ->setCellValue ( "U".$i, "ICD Masuk" )
            ->setCellValue ( "V".$i, "ICD Keluar" )
            ->setCellValue ( "W".$i, "Kunjungan RS" );
	$sheet->getStyle ( 'A'.$i.':W'.$i )->applyFromArray ($_center);
    $sheet->getStyle ( 'A'.$i.':W'.$i )->getFont()->setBold(true);
		
	$sheet ->getColumnDimension ( "A" )->setAutoSize ( true );
	$sheet ->getColumnDimension ( "B" )->setAutoSize ( true );
	$sheet ->getColumnDimension ( "C" )->setAutoSize ( true );
	$sheet ->getColumnDimension ( "D" )->setAutoSize ( true );
	$sheet ->getColumnDimension ( "E" )->setAutoSize ( true );
	$sheet ->getColumnDimension ( "F" )->setAutoSize ( true );
	$sheet ->getColumnDimension ( "G" )->setAutoSize ( true );
	$sheet ->getColumnDimension ( "H" )->setAutoSize ( true );
    $sheet ->getColumnDimension ( "I" )->setAutoSize ( true );
    $sheet ->getColumnDimension ( "J" )->setAutoSize ( true );
    $sheet ->getColumnDimension ( "K" )->setAutoSize ( true );
    $sheet ->getColumnDimension ( "L" )->setAutoSize ( true );
    $sheet ->getColumnDimension ( "M" )->setAutoSize ( true );
    $sheet ->getColumnDimension ( "N" )->setAutoSize ( true );
    $sheet ->getColumnDimension ( "O" )->setAutoSize ( true );
    $sheet ->getColumnDimension ( "P" )->setAutoSize ( true );
    $sheet ->getColumnDimension ( "Q" )->setAutoSize ( true );
    $sheet ->getColumnDimension ( "R" )->setAutoSize ( true );
    $sheet ->getColumnDimension ( "S" )->setAutoSize ( true );
    $sheet ->getColumnDimension ( "T" )->setAutoSize ( true );
    $sheet ->getColumnDimension ( "U" )->setAutoSize ( true );
    $sheet ->getColumnDimension ( "V" )->setAutoSize ( true );
    $sheet ->getColumnDimension ( "W" )->setAutoSize ( true );
	
	$no = $i;
	foreach($list as $x){
		$no++;
		$sheet	->setCellValue ( "A".$no, ($no-$i)."." )
				->setCellValue ( "B".$no, "'".ArrayAdapter::format("only-digit6",$x->noreg_pasien) )
				->setCellValue ( "C".$no, "'".ArrayAdapter::format("only-digit6",$x->nrm_pasien) )
				->setCellValue ( "D".$no, "'".ArrayAdapter::format("only-digit6",$x->profile_number) )
				->setCellValue ( "E".$no, $x->nama_pasien )
				->setCellValue ( "F".$no, $x->jk==0?"L":"P" )
				->setCellValue ( "G".$no, $x->alamat )
				->setCellValue ( "H".$no, $x->diagnosa_masuk )
				->setCellValue ( "I".$no, $x->diagnosa_ruang )
				->setCellValue ( "J".$no, $x->carapulang )
				->setCellValue ( "K".$no, $x->carabayar )
				->setCellValue ( "L".$no, ArrayAdapter::format("unslug",$x->asal_ruangan) )
                ->setCellValue ( "M".$no, ArrayAdapter::format("unslug",$x->ruangan) )
                ->setCellValue ( "N".$no, ArrayAdapter::format("unslug",$x->pindah_ke) )
                ->setCellValue ( "O".$no, $x->umur )
                ->setCellValue ( "P".$no, $x->kelas )
                ->setCellValue ( "Q".$no, $x->tgl_masuk )
                ->setCellValue ( "R".$no, $x->tgl_pulang )
                ->setCellValue ( "S".$no, $x->durasi )
                ->setCellValue ( "T".$no, $x->dpjp )
                ->setCellValue ( "U".$no, $x->icd_masuk )
                ->setCellValue ( "V".$no, $x->icd_ruang )
                ->setCellValue ( "W".$no, $x->kunjungan_rs );
	}
	$sheet->getStyle ( 'A'.$border_start.':W'.$no )->applyFromArray ($_thin);
/*end - BLOCK RANGKUMAN*/

$filename="DATA SENSUS - ".ArrayAdapter::format("unslug",$_POST['ruangan']).
					 " - ".ArrayAdapter::format("date d M Y H:i",$_dari).
					 " - ".ArrayAdapter::format("date d M Y H:i",$_sampai).".xls";

header ( 'Content-Type: application/vnd.ms-excel' );
header ( 'Content-Disposition: attachment;filename="'.$filename.'"' );
header ( 'Cache-Control: max-age=0' );
$writer = PHPExcel_IOFactory::createWriter ( $file, 'Excel5' );
$writer->save ( 'php://output' );
?>