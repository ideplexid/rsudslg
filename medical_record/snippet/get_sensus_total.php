<?php 
/**
 * this file used for get all patient 
 * in certain room. only count how many, not the detail.
 * 
 * @author 		: Nurul Huda
 * @license 	: Apache v3
 * @copyright 	: goblooge@gmail.com
 * @used 		: /medical_record/resource/php/laporan_pasien/lap_sensus.php
 * @service 	: get_sensus
 * @since		: 29 Oct 2016
 * @version		: 1.0.0
 * */

global $db;
require_once 'smis-base/smis-include-service-consumer.php';
require_once 'smis-libs-class/ServiceProviderList.php';
$dbtable=new DBTable($db, "smis_mr_sensus");
$dbtable->truncate();
$serv=new ServiceConsumer($db, "get_sensus");
$serv->setEntity($_POST['ruangan']);
$serv->addData("command", "count");
$serv->addData("dari", $_POST['dari']);
$serv->addData("sampai", $_POST['sampai']);	
$serv->execute();
$data=$serv->getContent();
$res=new ResponsePackage();
$res->setStatus(ResponsePackage::$STATUS_OK);
$res->setContent($data);
echo json_encode($res->getPackage());
?>