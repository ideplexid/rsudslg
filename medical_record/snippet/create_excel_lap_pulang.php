<?php 
/**
 * this file used for get all patient 
 * that go out from the hospital in
 * given date and export it as excel
 * 
 * @author 		: Nurul Huda
 * @license 	: Apache v3
 * @copyright 	: goblooge@gmail.com
 * @used 		: /medical_record/resource/php/laporan_pasien/lap_pulang.php
 * @service 	: get_sensus
 * @since		: 01 Feb 2017
 * @version		: 1.0.0
 * @database	: smis_mr_pasien_pulang
 * */

require_once "smis-libs-out/php-excel/PHPExcel.php"; 
require_once "smis-base/smis-include-service-consumer.php";
global $db;
$dbtable=new DBTable($db, "smis_mr_pasien_pulang");
$dbtable->setOrder(" tgl_pulang ASC");
$dbtable->setShowAll(true);
$data=$dbtable->view("","0");
$list=$data['data'];

show_error();
/*start - BLOK RESOURCE*/
	$_thin = array ();
	$_thin['borders']=array();
	$_thin['borders']['allborders']=array();
	$_thin['borders']['allborders']['style']=PHPExcel_Style_Border::BORDER_THIN ;

	$_center = array();
	$_center ['alignment']=array();
	$_center ['alignment']['horizontal']=PHPExcel_Style_Alignment::HORIZONTAL_CENTER;
/*end - BLOCK RESOURCE*/

/*start - BLOCK PROPERTIES FILE EXCEL*/
	$file = new PHPExcel ();
	$file->getProperties ()->setCreator ( $user->getUsername() );
	$file->getProperties ()->setLastModifiedBy ( $user->getNameOnly() );
	$file->getProperties ()->setTitle ( "Laporan Pasien Pulang" );
	$file->getProperties ()->setSubject ( "Laporan Pasien Pulang" );
	$file->getProperties ()->setDescription ( "Data Laporan Pasien Pulang " );
	$file->getProperties ()->setKeywords ( "pasien, pulang, rekam, medis" );
	$file->getProperties ()->setCategory ( "Rekam Medis" );
/*end - BLOCK PROPERTIES FILE EXCEL*/

/*start - BLOCK PASIEN MASUK*/
	$file->setActiveSheetIndex ( 0 );
	$sheet = $file->getActiveSheet ( 0 );
	$sheet  ->setTitle ( "PASIEN" );	
    $sheet	->mergeCells('A1:T1');
	$sheet	->setCellValue ( "A1", "PASIEN PULANG RAWAT INAP DARI RUMAH SAKIT" );
	$sheet	->setCellValue ( "A2", "No." )
			->setCellValue ( "B2", "No. Reg" )
			->setCellValue ( "C2", "NRM" )
			->setCellValue ( "D2", "Nama" )
			->setCellValue ( "E2", "Ruangan" )
			->setCellValue ( "F2", "Masuk" )
            ->setCellValue ( "G2", "Keluar" )
            ->setCellValue ( "H2", "ICD 10" )
            ->setCellValue ( "I2", "Diagnosa" )
            ->setCellValue ( "J2", "DPJP" )
            ->setCellValue ( "K2", "Kasus Bedah" )
            ->setCellValue ( "L2", "ICD 9" )
            ->setCellValue ( "M2", "Tindakan" )
            ->setCellValue ( "N2", "Tanggal Tindakan" )
            ->setCellValue ( "O2", "Dokter Bedah" )
            ->setCellValue ( "P2", "Dokter Anastesi" )
            ->setCellValue ( "Q2", "Cara Bayar" )
            ->setCellValue ( "R2", "Cara Pulang" )
            ->setCellValue ( "S2", "Perusahaan" )
            ->setCellValue ( "T2", "Asuransi" );
	$sheet  ->getStyle ( 'A1:T1' )
            ->applyFromArray ($_center);
		
	$sheet ->getColumnDimension ( "A" )->setAutoSize ( true );
	$sheet ->getColumnDimension ( "B" )->setAutoSize ( true );
	$sheet ->getColumnDimension ( "C" )->setAutoSize ( true );
	$sheet ->getColumnDimension ( "D" )->setAutoSize ( true );
	$sheet ->getColumnDimension ( "E" )->setAutoSize ( true );
	$sheet ->getColumnDimension ( "F" )->setAutoSize ( true );
	$sheet ->getColumnDimension ( "G" )->setAutoSize ( true );
	$sheet ->getColumnDimension ( "H" )->setAutoSize ( true );
	$sheet ->getColumnDimension ( "I" )->setAutoSize ( true );
	$sheet ->getColumnDimension ( "J" )->setAutoSize ( true );
	$sheet ->getColumnDimension ( "K" )->setAutoSize ( true );
    $sheet ->getColumnDimension ( "L" )->setAutoSize ( true );
    $sheet ->getColumnDimension ( "M" )->setAutoSize ( true );
    $sheet ->getColumnDimension ( "N" )->setAutoSize ( true );
    $sheet ->getColumnDimension ( "O" )->setAutoSize ( true );
    $sheet ->getColumnDimension ( "P" )->setAutoSize ( true );
    $sheet ->getColumnDimension ( "Q" )->setAutoSize ( true );
    $sheet ->getColumnDimension ( "R" )->setAutoSize ( true );
    $sheet ->getColumnDimension ( "S" )->setAutoSize ( true );
    $sheet ->getColumnDimension ( "T" )->setAutoSize ( true );
	$sheet ->getStyle("A1:T2")->getFont()->setBold(true);
	$_ruang=array();
	$no=2;
	foreach($list as $x){
		$no++;
        if($x->tgl_masuk!="" && $x->tgl_masuk!="0000-00-00 00:00:00"){
            $pulang = new DateTime($x->tgl_masuk);
            $dateVal = PHPExcel_Shared_Date::PHPToExcel($pulang);
            $sheet  ->getStyle("F".$no)
                    ->getNumberFormat()
                    ->setFormatCode("dd-mm-yyyy HH:mm");
            $sheet  ->setCellValue("F".$no,$dateVal);            
        }
                
        if($x->tgl_pulang!="" && $x->tgl_pulang!="0000-00-00 00:00:00"){
            $masuk = new DateTime($x->tgl_pulang);
            $dateVal = PHPExcel_Shared_Date::PHPToExcel($masuk);
            $sheet  ->getStyle("G".$no)
                    ->getNumberFormat()
                    ->setFormatCode("dd-mm-yyyy HH:mm");
            $sheet  ->setCellValue("G".$no,$dateVal);
        }
        
        if($x->tgl_tindakan!="" && $x->tgl_tindakan!="0000-00-00"){
            $tindakan = new DateTime($x->tgl_tindakan);
            $dateVal = PHPExcel_Shared_Date::PHPToExcel($tindakan);
            $sheet  ->getStyle("N".$no)
                    ->getNumberFormat()
                    ->setFormatCode("dd-mm-yyyy");
            $sheet  ->setCellValue("N".$no,$dateVal);                
        }
        $ruangan=ArrayAdapter::format("unslug",$x->ruang);
        if($ruangan=="") $ruangan="-";
		$sheet	->setCellValue ( "A".$no, ($no-2)."." )
				->setCellValue ( "B".$no, "'".ArrayAdapter::format("only-digit6",$x->noreg))
				->setCellValue ( "C".$no, "'".ArrayAdapter::format("only-digit6",$x->nrm) )
				->setCellValue ( "D".$no, $x->nama_pasien )
				->setCellValue ( "E".$no,  $ruangan)
				->setCellValue ( "H".$no, $x->kode_diagnosa )
                ->setCellValue ( "I".$no, $x->diagnosa )
                ->setCellValue ( "J".$no, $x->dokter_pj )
                ->setCellValue ( "K".$no, $x->kasus )
                ->setCellValue ( "L".$no, $x->kode_tindakan )
                ->setCellValue ( "M".$no, $x->tindakan )
                ->setCellValue ( "O".$no, $x->dokter_bedah )
                ->setCellValue ( "P".$no, $x->dokter_anastesi )
                ->setCellValue ( "Q".$no, ArrayAdapter::format("unslug",$x->carabayar) )
                ->setCellValue ( "R".$no, ArrayAdapter::format("unslug",$x->carapulang) )
                ->setCellValue ( "S".$no, $x->n_perusahaan )
                ->setCellValue ( "T".$no, $x->n_asuransi );
        if(!in_array($ruangan,$_ruang)){
            $_ruang[]=$ruangan;
        }
	}
    
	$sheet->getStyle ( 'A2:T'.$no )->applyFromArray ($_thin);
/*end - BLOCK PASIEN MASUK*/

/*start - BLOCK SETTINGS*/
$END_NUM=$no;
$file   ->createSheet (1);
$file   ->setActiveSheetIndex ( 1 );
$sheet  = $file->getActiveSheet (1);
$sheet  ->setTitle("SETTINGS");
$sheet  ->setCellValue("A1","RUANGAN");
$sheet  ->setCellValue("B1","PASIEN!\$E\$3:\$E\$".$END_NUM);
$sheet  ->setCellValue("A2","CARA PULANG");
$sheet  ->setCellValue("B2","PASIEN!\$R\$3:\$R\$".$END_NUM);
$sheet  ->setCellValue("A3","CARA BAYAR");
$sheet  ->setCellValue("B3","PASIEN!\$Q\$3:\$Q\$".$END_NUM);
$sheet  ->setCellValue("A4","ASURANSI");
$sheet  ->setCellValue("B4","PASIEN!\$T\$3:\$T\$".$END_NUM);
/*end - BLOCK SETTINGS*/


/* start - BLOCK RESUME PASIEN */
sort($_ruang);
$file   ->createSheet (2);
$file   ->setActiveSheetIndex ( 2 );
$sheet  = $file->getActiveSheet (2);
$sheet  ->setTitle("RESUME PASIEN");
$sheet	->mergeCells('A1:A2');
$sheet	->setCellValue("A1","RUANGAN");
$sheet	->mergeCells('B1:E1');
$sheet	->setCellValue("B1","CARA PULANG");

$sheet	->setCellValue("B2","HIDUP");
$sheet	->setCellValue("C2","RS LAIN");
$sheet	->setCellValue("D2","MATI <=48");
$sheet	->setCellValue("E2","MATI >48");

/*START - PENCARIAN PASIEN BERDASARKAN CARABAYAR*/
$serv=new ServiceConsumer($db,"get_carabayar",NULL,"registration");
$serv->execute();
$database=$serv->getContent();
$total=count($database);

/*END - PENCARIAN PASIEN BERDASARKAN CARABAYAR*/
$HURUF=array();
$HURUF[1]="F";
$HURUF[2]="G";
$HURUF[3]="H";
$HURUF[4]="I";
$HURUF[5]="J";
$HURUF[6]="K";
$HURUF[7]="L";
$HURUF[8]="M";
$HURUF[9]="N";
$HURUF[10]="O";
$HURUF[11]="P";
$HURUF[12]="Q";
$HURUF[13]="R";
$HURUF[14]="S";
$HURUF[15]="T";
$HURUF[16]="U";
$HURUF[17]="V";
$HURUF[18]="W";
$HURUF[19]="X";
$HURUF[20]="Y";
$HURUF[21]="Z";
$HURUF[22]="AA";
$HURUF[23]="AB";
$HURUF[24]="AC";
$HURUF[25]="AD";
$HURUF[26]="AE";
$HURUF[27]="AF";
$HURUF[28]="AG";
$HURUF[29]="AH";
$HURUF[30]="AI";
$HURUF[31]="AJ";
$LAST_ALPHABET=$HURUF[$total];
$sheet	->mergeCells('F1:'.$LAST_ALPHABET.'1');
$sheet	->setCellValue("F1","CARA BAYAR");
$total=0;
foreach($database as $d){
    $total++;
    $LAST_ALPHABET=$HURUF[$total];
    $sheet	->setCellValue($LAST_ALPHABET."2",ArrayAdapter::format("unslug",$d['slug']));
}

$_LAST_CARABAYAR_ALPHABET=$LAST_ALPHABET;
if($_POST['detail']=="1"){
    $total++;
    $_FIRST_ASURANSI_ALPHABET=$HURUF[$total];
    $LAST_ALPHABET=$HURUF[$total];

    $sheet	->setCellValue($LAST_ALPHABET."2","SKM");
    $total++;
    $LAST_ALPHABET=$HURUF[$total];
    $sheet	->setCellValue($LAST_ALPHABET."2","SEHATI");
    $total++;
    $LAST_ALPHABET=$HURUF[$total];
    $sheet	->setCellValue($LAST_ALPHABET."2","SPM");
    $total++;
    $LAST_ALPHABET=$HURUF[$total];
    $sheet	->setCellValue($LAST_ALPHABET."2","SKTM");
    $total++;
    $LAST_ALPHABET=$HURUF[$total];
    $sheet	->setCellValue($LAST_ALPHABET."2","JAMKESDA");
    $total++;
    $LAST_ALPHABET=$HURUF[$total];
    $sheet	->setCellValue($LAST_ALPHABET."2","JASA RAHARJA");
    $total++;
    $LAST_ALPHABET=$HURUF[$total];

    $sheet	->mergeCells($LAST_ALPHABET.'1:'.$LAST_ALPHABET.'2');
    $sheet	->setCellValue($LAST_ALPHABET."1","GRATIS");
    $total++;
    $LAST_ALPHABET=$HURUF[$total];
    $sheet	->mergeCells($LAST_ALPHABET.'1:'.$LAST_ALPHABET.'2');
    $sheet	->setCellValue($LAST_ALPHABET."1","CHECKUP");
    $total++;
    $LAST_ALPHABET=$HURUF[$total];
    $sheet	->mergeCells($LAST_ALPHABET.'1:'.$LAST_ALPHABET.'2');
    $sheet	->setCellValue($LAST_ALPHABET."1","SURAT KETERANGAN SEHAT");
    $total++;
    $LAST_ALPHABET=$HURUF[$total];
    $sheet	->mergeCells($LAST_ALPHABET.'1:'.$LAST_ALPHABET.'2');
    $sheet	->setCellValue($LAST_ALPHABET."1","LAIN-LAIN");
}

$sheet  ->getStyle( 'A1:'.$LAST_ALPHABET.'1' )->applyFromArray ($_center);
$sheet  ->getStyle("A1:".$LAST_ALPHABET."2")->getFont()->setBold(true);

$sheet ->getColumnDimension ( "A" )->setAutoSize ( true );
$sheet ->getColumnDimension ( "C" )->setAutoSize ( true );
$sheet ->getColumnDimension ( "D" )->setAutoSize ( true );
$sheet ->getColumnDimension ( "E" )->setAutoSize ( true );
$sheet ->getColumnDimension ( "G" )->setAutoSize ( true );
$sheet ->getColumnDimension ( "I" )->setAutoSize ( true );
$sheet ->getColumnDimension ( "J" )->setAutoSize ( true );
$sheet ->getColumnDimension ( "K" )->setAutoSize ( true );
$sheet ->getColumnDimension ( "L" )->setAutoSize ( true );
$sheet ->getColumnDimension ( "M" )->setAutoSize ( true );
$sheet ->getColumnDimension ( "N" )->setAutoSize ( true );
$sheet ->getColumnDimension ( "O" )->setAutoSize ( true );
$sheet ->getColumnDimension ( "P" )->setAutoSize ( true );
$sheet ->getColumnDimension ( "Q" )->setAutoSize ( true );
$no=2;
foreach($_ruang as $r){
    $no++;
    $sheet	->setCellValue("A".$no,$r);    
    //$sheet  ->setCellValue("B".$no,'=COUNTIFS(INDIRECT(SETTINGS!$B$2),CONCATENATE("*",B$2,"*"),INDIRECT(SETTINGS!$B$1),$A1)');
    //$sheet  ->setCellValueExplicit("F".$no,"=COUNTIFS(INDIRECT(SETTINGS!\$B\$3);CONCATENATE(\"*\";F\$2;\"*\");INDIRECT(SETTINGS!\$B\$1);\$A1)");
    //$sheet  ->setCellValueExplicit("H".$no,"=COUNTIFS(INDIRECT(SETTINGS!\$B\$4);CONCATENATE(\"*\";H\$2;\"*\");INDIRECT(SETTINGS!\$B\$1);\$A1)");
}
$SEPARATOR=$_POST['separator'];
$sheet  ->setCellValueExplicit("B3","=COUNTIFS(INDIRECT(SETTINGS!\$B\$2)".$SEPARATOR."CONCATENATE(\"*\"".$SEPARATOR."B\$2".$SEPARATOR."\"*\")".$SEPARATOR."INDIRECT(SETTINGS!\$B\$1)".$SEPARATOR."\$A3)");
$sheet  ->setCellValueExplicit("F3","=COUNTIFS(INDIRECT(SETTINGS!\$B\$3)".$SEPARATOR."F\$2".$SEPARATOR."INDIRECT(SETTINGS!\$B\$1)".$SEPARATOR."\$A3)");
if($_POST['detail']=="1") 
    $sheet  ->setCellValueExplicit($_FIRST_ASURANSI_ALPHABET."3","=COUNTIFS(INDIRECT(SETTINGS!\$B\$4)".$SEPARATOR."CONCATENATE(\"*\"".$SEPARATOR."H\$2".$SEPARATOR."\"*\")".$SEPARATOR."INDIRECT(SETTINGS!\$B\$1)".$SEPARATOR."\$A3)");
$_total_ruang=count($_ruang)+2;

$backgroound['fill']=array();
$backgroound['fill']['type']=PHPExcel_Style_Fill::FILL_SOLID;
$backgroound['fill']['color']=array();
$backgroound['fill']['color']['rgb']='99FFFF';
$sheet->getStyle ( 'B3:E'.$_total_ruang )->applyFromArray ($backgroound);
$backgroound['fill']['color']['rgb']='FF99FF';
$sheet->getStyle ( 'F3:'.$_LAST_CARABAYAR_ALPHABET.$_total_ruang )->applyFromArray ($backgroound);
if($_POST['detail']=="1"){
    $backgroound['fill']['color']['rgb']='FFFF99';
    $sheet->getStyle ( $_FIRST_ASURANSI_ALPHABET.'3:'.$LAST_ALPHABET.$_total_ruang )->applyFromArray ($backgroound);
}
$sheet	->setCellValue("A".($_total_ruang+1),"TOTAL");
$sheet	->setCellValue("B".($_total_ruang+1),"=SUM(B3:B".$_total_ruang.")");
$sheet	->setCellValue("C".($_total_ruang+1),"=SUM(C3:C".$_total_ruang.")");
$sheet	->setCellValue("D".($_total_ruang+1),"=SUM(D3:D".$_total_ruang.")");
$sheet	->setCellValue("E".($_total_ruang+1),"=SUM(E3:E".$_total_ruang.")");

foreach($HURUF as $no=>$h){
    $sheet	->setCellValue($h.($_total_ruang+1),"=SUM(".$h."3:".$h.$_total_ruang.")");
    if($h==$LAST_ALPHABET) 
        break;
}



$sheet ->getStyle("A".($_total_ruang+1).":".$LAST_ALPHABET.($_total_ruang+1))->getFont()->setBold(true);
$sheet->getStyle ( 'A1:'.$LAST_ALPHABET.($_total_ruang+1) )->applyFromArray ($_thin);
/* end - BLOCK RESUME PASIEN */
        
$filename= "DATA PASIEN PULANG - ".
           " - ".ArrayAdapter::format("date d M Y",$_POST['dari']).
           " - ".ArrayAdapter::format("date d M Y",$_POST['sampai']).".xls";

header ( 'Content-Type: application/vnd.ms-excel' );
header ( 'Content-Disposition: attachment;filename="'.$filename.'"' );
header ( 'Cache-Control: max-age=0' );
$writer = PHPExcel_IOFactory::createWriter ( $file, 'Excel5' );
$writer->save ( 'php://output' );

?>