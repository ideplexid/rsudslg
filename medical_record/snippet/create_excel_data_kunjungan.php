<?php
    require_once ("smis-libs-out/php-excel/PHPExcel.php");
    global $db;

    $file = new PHPExcel ();
    $file->getProperties ()->setCreator ( "PT. Inovasi Ide Utama" );
    $file->getProperties ()->setTitle ( "Data Kunjungan Pasien" );
    $file->getProperties ()->setSubject ( "Data Kunjungan Pasien" );
    $file->getProperties ()->setDescription ( "Data Kunjungan Pasien Generated by System" );
    $file->getProperties ()->setKeywords ( "Data Kunjungan Pasien" );
    $file->getProperties ()->setCategory ( "Data Kunjungan Pasien" );

    $sheet = $file->getActiveSheet ();
    $i = 1;

    $sheet->mergeCells("A".$i.":P".$i)->setCellValue("A".$i,"DATA KUNJUNGAN PASIEN");
    $sheet->getStyle("A".$i.":P".$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $sheet->getStyle("A".$i)->getFont()->setBold(true);
    $i = $i + 1;
    $tanggal_awal = ArrayAdapter::format("date d-m-Y", $_POST['tanggal_from']);
    $tanggal_akhir = ArrayAdapter::format("date d-m-Y", $_POST['tanggal_to']);
    $uri_label = $_POST['uri_label'];
    $ruangan_label = $_POST['ruangan_label'];
    $carapulang_label = $_POST['carapulang_label'];
    $sheet->mergeCells("A".$i.":P".$i)->setCellValue(
        "A".$i,
        "TANGGAL : " . $tanggal_awal . " s/d " . $tanggal_akhir . " | " .
        "RJ/RI : " . $uri_label . " | " .
        "RUANGAN : " . $ruangan_label . " | " .
        "CARA KELUAR : " . $carapulang_label
    );
    $sheet->getStyle("A".$i.":P".$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $sheet->getStyle("A".$i)->getFont()->setBold(true);
    $i = $i + 2;

    $border_start = $i;
    $sheet->setCellValue("A".$i,"NO.");
    $sheet->setCellValue("B".$i,"NO. REGISTRASI");
    $sheet->setCellValue("C".$i,"NO. RM");
    $sheet->setCellValue("D".$i,"NAMA PASIEN");
    $sheet->setCellValue("E".$i,"RJ/RI");
    $sheet->setCellValue("F".$i,"TANGGAL MASUK");
    $sheet->setCellValue("G".$i,"CARA MASUK");
    $sheet->setCellValue("H".$i,"RUANGAN RAWAT JALAN");
    $sheet->setCellValue("I".$i,"JENIS KEGIATAN");
    $sheet->setCellValue("J".$i,"TANGGAL INAP (MRS)");
    $sheet->setCellValue("K".$i,"RUANGAN INAP");
    $sheet->setCellValue("L".$i,"TANGGAL KELUAR (KRS)");
    $sheet->setCellValue("M".$i,"CARA KELUAR");
    $sheet->setCellValue("N".$i,"LAMA DIRAWAT");
    $sheet->setCellValue("O".$i,"CARA BAYAR");
    $sheet->setCellValue("P".$i,"STATUS");
    $sheet->getStyle("A".$i.":P".$i)->getFont()->setBold(true);
    $sheet->getStyle("A".$i.":P".$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $sheet->getStyle("A".$i.":P".$i)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
    $i = $i + 1;

    $filter = " AND DATE(tanggal) >= '" . date("Y-m-d", strtotime($_POST['tanggal_from'])) . "' 
                AND DATE(tanggal) <= '" . date("Y-m-d", strtotime($_POST['tanggal_to'])) . "' 
                AND (
                    jenislayanan LIKE '" . $_POST['ruangan'] . "' OR
                    kamar_inap LIKE '" . $_POST['ruangan'] . "' OR
                    last_ruangan LIKE '" . $_POST['ruangan'] . "'
              ) AND carapulang LIKE '" . $_POST['carapulang'] . "'
                AND uri LIKE '" . $_POST['uri'] . "' ";
    $data = $db->get_result("
        SELECT *
        FROM smis_rg_layananpasien
        WHERE prop = '' " . $filter . "
    ");
    if ($data != null) {
        $nomor = 1;
        foreach ($data as $d) {
            $sheet->setCellValue("A".$i,$nomor++);
            $sheet->setCellValue("B".$i,$d->id);
            $sheet->setCellValue("C".$i,$d->nrm);
            $sheet->setCellValue("D".$i,$d->nama_pasien);
            if ($d->uri == 0)
                $sheet->setCellValue("E".$i,"Rawat Jalan");
            else
                $sheet->setCellValue("E".$i,"Rawat Inap");
            $sheet->setCellValue("F".$i,ArrayAdapter::format("date d-m-Y, H:i", $d->tanggal));
            $sheet->setCellValue("G".$i,$d->caradatang);
            $sheet->setCellValue("H".$i,ArrayAdapter::format("unslug", $d->jenislayanan));
            $sheet->setCellValue("I".$i,$d->jenis_kegiatan);
            $sheet->setCellValue("J".$i,ArrayAdapter::format("date d-m-Y, H:i", $d->tanggal_inap));
            $sheet->setCellValue("K".$i,ArrayAdapter::format("unslug", $d->kamar_inap));
            $sheet->setCellValue("L".$i,ArrayAdapter::format("date d-m-Y, H:i", $d->tanggal_pulang));
            $sheet->setCellValue("M".$i,$d->carapulang);
            $sheet->setCellValue("N".$i,round($d->lama_dirawat));
            $sheet->setCellValue("O".$i,ArrayAdapter::format("unslug", $d->carabayar));
            if ($d->selesai == 0)
                $sheet->setCellValue("P".$i,"Aktif");
            else
                $sheet->setCellValue("P".$i,"Tidak Aktif");
            $i = $i + 1;
        }
    }

    $thin = array ();
    $thin['borders']=array();
    $thin['borders']['allborders']=array();
    $thin['borders']['allborders']['style']=PHPExcel_Style_Border::BORDER_THIN ;
    $sheet->getStyle ( "A".$border_start.":P".($i - 1) )->applyFromArray ($thin);

    foreach (range('A', 'P') as $columnID) {
        $sheet->getColumnDimension($columnID)->setAutoSize(true);
    }

    header ( 'Content-Type: application/vnd.ms-excel' );
    header ( 'Content-Disposition: attachment;filename="Rekam Medis - Data Kunjungan Pasien.xls"' );
    header ( 'Cache-Control: max-age=0' );
    $writer = PHPExcel_IOFactory::createWriter ( $file, 'Excel5' );
    $writer->save ( 'php://output' );
    return;

?>