<?php 
/**
 * this file used for get all patient 
 * that go out from the hospital in
 * given date and export it as excel
 * special case for hospitalized patient
 * 
 * @author 		: Nurul Huda
 * @license 	: Apache v3
 * @copyright 	: goblooge@gmail.com
 * @used 		: /medical_record/resource/php/lap_index/lap_index_dokter.php
 * @since		: 17 Feb 2017
 * @version		: 1.0.0
 * @database	: smist_mr_lap_ranap
 * */

require_once "smis-libs-out/php-excel/PHPExcel.php"; 
require_once "smis-base/smis-include-service-consumer.php";
loadLibrary("smis-libs-function-excel");
global $db;
$dbtable=new DBTable($db, "smis_mr_index_dokter");
$dbtable->setOrder(" tgl_kunjungan ASC");
$dbtable->setShowAll(true);
if(isset($_POST['nama_dokter']) && $_POST['nama_dokter']!=""){
    $dbtable->addCustomKriteria("nama_dokter","='".$_POST['nama_dokter']."'");
}
$data=$dbtable->view("","0");
$list=$data['data'];

show_error();
/*start - BLOK RESOURCE*/
	$_thin = array ();
	$_thin['borders']=array();
	$_thin['borders']['allborders']=array();
	$_thin['borders']['allborders']['style']=PHPExcel_Style_Border::BORDER_THIN ;

	$_center = array();
	$_center ['alignment']=array();
	$_center ['alignment']['horizontal']=PHPExcel_Style_Alignment::HORIZONTAL_CENTER;
/*end - BLOCK RESOURCE*/

/*start - BLOCK PROPERTIES FILE EXCEL*/
	$file = new PHPExcel ();
	$file->getProperties ()->setCreator ( $user->getUsername() );
	$file->getProperties ()->setLastModifiedBy ( $user->getNameOnly() );
	$file->getProperties ()->setTitle ( "Laporan Pasien Index Rawat Inap" );
	$file->getProperties ()->setSubject ( "Laporan Pasien Index Rawat Inap" );
	$file->getProperties ()->setDescription ( "Data Laporan Pasien Index Rawat Inap " );
	$file->getProperties ()->setKeywords ( "pasien, pulang, rekam, medis" );
	$file->getProperties ()->setCategory ( "Rekam Medis" );
/*end - BLOCK PROPERTIES FILE EXCEL*/

/*start - BLOCK PASIEN MASUK*/
	$file->setActiveSheetIndex ( 0 );
	$sheet = $file->getActiveSheet ( 0 );
	$sheet  ->setTitle ( "PASIEN" );	
    $sheet	->mergeCells('A1:N1');
	$sheet	->setCellValue ( "A1", "LAPORAN INDEX DOKTER" );
	$sheet	->setCellValue ( "A2", "No." )
			->setCellValue ( "B2", "No. Reg" )
			->setCellValue ( "C2", "NRM" )
			->setCellValue ( "D2", "Nama" )
			->setCellValue ( "E2", "Tgl Lahir" )
			->setCellValue ( "F2", "Tgl Kunjugan" )
            ->setCellValue ( "G2", "Ruang Kelas" )
            ->setCellValue ( "H2", "Jenis Kelamin" )
            ->setCellValue ( "I2", "Diagnosa" )
            ->setCellValue ( "J2", "Dokter" );
	$sheet  ->getStyle ( 'A1:J1' )
            ->applyFromArray ($_center);
    
	$sheet ->getColumnDimension ( "A" )->setAutoSize ( true );
	$sheet ->getColumnDimension ( "B" )->setAutoSize ( true );
	$sheet ->getColumnDimension ( "C" )->setAutoSize ( true );
	$sheet ->getColumnDimension ( "D" )->setAutoSize ( true );
	$sheet ->getColumnDimension ( "E" )->setAutoSize ( true );
	$sheet ->getColumnDimension ( "F" )->setAutoSize ( true );
	$sheet ->getColumnDimension ( "G" )->setAutoSize ( true );
	$sheet ->getColumnDimension ( "H" )->setAutoSize ( true );
	$sheet ->getColumnDimension ( "I" )->setAutoSize ( true );
	$sheet ->getColumnDimension ( "J" )->setAutoSize ( true );
	$sheet ->getStyle("A1:J2")->getFont()->setBold(true);
	$_ruang=array();
	$no=2;
	foreach($list as $x){
		$no++;
        date_excel_format($sheet,"E".$no,$x->tgl_lahir);
        date_excel_format($sheet,"F".$no,$x->tgl_kunjungan);
        
        $ruangan=ArrayAdapter::format("unslug",$x->ruang);
        if($ruangan=="") $ruangan="-";
		$sheet	->setCellValue ( "A".$no, ($no-2)."." )
				->setCellValue ( "B".$no, "'".ArrayAdapter::format("only-digit6",$x->noreg_pasien))
				->setCellValue ( "C".$no, "'".ArrayAdapter::format("only-digit6",$x->nrm_pasien) )
				->setCellValue ( "D".$no, $x->nama_pasien )
				->setCellValue ( "G".$no, "'".ArrayAdapter::format("unslug",$x->kelas))
				->setCellValue ( "H".$no, $x->jk=="0"?"L":"P")
                ->setCellValue ( "I".$no, $x->diagnosa_utama)
                ->setCellValue ( "J".$no, $x->nama_dokter );
        if(!in_array($ruangan,$_ruang)){
            $_ruang[]=$ruangan;
        }
	}
    
	$sheet->getStyle ( 'A2:J'.$no )->applyFromArray ($_thin);
/*end - BLOCK PASIEN MASUK*/
        
$filename= "DATA LAPORAN INDEX DOKTER - ".
           " - ".ArrayAdapter::format("date d M Y",$_POST['dari']).
           " - ".ArrayAdapter::format("date d M Y",$_POST['sampai']).".xls";

header ( 'Content-Type: application/vnd.ms-excel' );
header ( 'Content-Disposition: attachment;filename="'.$filename.'"' );
header ( 'Cache-Control: max-age=0' );
$writer = PHPExcel_IOFactory::createWriter ( $file, 'Excel5' );
$writer->save ( 'php://output' );

?>