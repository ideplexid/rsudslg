<?php 
/**
 * this file used for get all patient 
 * that go out from the hospital in
 * given date and export it as excel
 * special case for hospitalized patient
 * 
 * @author 		: Nurul Huda
 * @license 	: Apache v3
 * @copyright 	: goblooge@gmail.com
 * @used 		: /medical_record/resource/php/lap_index/lap_index_ranap.php
 * @since		: 17 Feb 2017
 * @version		: 1.0.0
 * @database	: smist_mr_lap_ranap
 * */

require_once "smis-libs-out/php-excel/PHPExcel.php"; 
require_once "smis-base/smis-include-service-consumer.php";
global $db;
$dbtable=new DBTable($db, "smist_mr_lap_rajal");
$dbtable->setOrder(" tgl_pulang ASC");
$dbtable->setShowAll(true);
$data=$dbtable->view("","0");
$list=$data['data'];

show_error();
/*start - BLOK RESOURCE*/
	$_thin = array ();
	$_thin['borders']=array();
	$_thin['borders']['allborders']=array();
	$_thin['borders']['allborders']['style']=PHPExcel_Style_Border::BORDER_THIN ;

	$_center = array();
	$_center ['alignment']=array();
	$_center ['alignment']['horizontal']=PHPExcel_Style_Alignment::HORIZONTAL_CENTER;
/*end - BLOCK RESOURCE*/

/*start - BLOCK PROPERTIES FILE EXCEL*/
	$file = new PHPExcel ();
	$file->getProperties ()->setCreator ( $user->getUsername() );
	$file->getProperties ()->setLastModifiedBy ( $user->getNameOnly() );
	$file->getProperties ()->setTitle ( "Laporan Pasien Index Rawat Inap" );
	$file->getProperties ()->setSubject ( "Laporan Pasien Index Rawat Inap" );
	$file->getProperties ()->setDescription ( "Data Laporan Pasien Index Rawat Inap " );
	$file->getProperties ()->setKeywords ( "pasien, pulang, rekam, medis" );
	$file->getProperties ()->setCategory ( "Rekam Medis" );
/*end - BLOCK PROPERTIES FILE EXCEL*/

/*start - BLOCK PASIEN MASUK*/
	$file->setActiveSheetIndex ( 0 );
	$sheet = $file->getActiveSheet ( 0 );
	$sheet  ->setTitle ( "PASIEN" );	
    $sheet	->mergeCells('A1:N1');
	$sheet	->setCellValue ( "A1", "PASIEN PULANG RAWAT INAP DARI RUMAH SAKIT" );
	$sheet	->setCellValue ( "A2", "No." )
			->setCellValue ( "B2", "No. Reg" )
			->setCellValue ( "C2", "NRM" )
			->setCellValue ( "D2", "Nama" )
			->setCellValue ( "E2", "Umur" )
			->setCellValue ( "F2", "Jenis Kelamin" )
            ->setCellValue ( "G2", "Alamat" )
            ->setCellValue ( "H2", "Masuk" )
            ->setCellValue ( "I2", "Ruangan" )
            ->setCellValue ( "J2", "Diagnosa" )
            ->setCellValue ( "K2", "Tindakan" )
            ->setCellValue ( "L2", "DPJP" )
            ->setCellValue ( "M2", "Cara Bayar" )
            ->setCellValue ( "N2", "Cara Pulang" );
	$sheet  ->getStyle ( 'A1:N1' )
            ->applyFromArray ($_center);
		
	$sheet ->getColumnDimension ( "A" )->setAutoSize ( true );
	$sheet ->getColumnDimension ( "B" )->setAutoSize ( true );
	$sheet ->getColumnDimension ( "C" )->setAutoSize ( true );
	$sheet ->getColumnDimension ( "D" )->setAutoSize ( true );
	$sheet ->getColumnDimension ( "E" )->setAutoSize ( true );
	$sheet ->getColumnDimension ( "F" )->setAutoSize ( true );
	$sheet ->getColumnDimension ( "G" )->setAutoSize ( true );
	$sheet ->getColumnDimension ( "H" )->setAutoSize ( true );
	$sheet ->getColumnDimension ( "I" )->setAutoSize ( true );
	$sheet ->getColumnDimension ( "J" )->setAutoSize ( true );
	$sheet ->getColumnDimension ( "K" )->setAutoSize ( true );
    $sheet ->getColumnDimension ( "L" )->setAutoSize ( true );
    $sheet ->getColumnDimension ( "M" )->setAutoSize ( true );
    $sheet ->getColumnDimension ( "N" )->setAutoSize ( true );
	$sheet ->getStyle("A1:N2")->getFont()->setBold(true);
	$_ruang=array();
	$no=2;
	foreach($list as $x){
		$no++;
        if($x->tgl_masuk!="" && $x->tgl_masuk!="0000-00-00 00:00:00"){
            $pulang = new DateTime($x->tgl_masuk);
            $dateVal = PHPExcel_Shared_Date::PHPToExcel($pulang);
            $sheet  ->getStyle("H".$no)
                    ->getNumberFormat()
                    ->setFormatCode("dd-mm-yyyy HH:mm");
            $sheet  ->setCellValue("H".$no,$dateVal);            
        }
        
       
        
        $ruangan=ArrayAdapter::format("unslug",$x->ruang);
        if($ruangan=="") $ruangan="-";
		$sheet	->setCellValue ( "A".$no, ($no-2)."." )
				->setCellValue ( "B".$no, "'".ArrayAdapter::format("only-digit6",$x->noreg))
				->setCellValue ( "C".$no, "'".ArrayAdapter::format("only-digit6",$x->nrm) )
				->setCellValue ( "D".$no, $x->nama_pasien )
				->setCellValue ( "E".$no, $x->umur)
				->setCellValue ( "F".$no, $x->jk=="0"?"L":"P")
                ->setCellValue ( "G".$no, $x->kelurahan." - ".$x->kecamatan." - ".$x->kabupaten) //$x->kode_diagnosa_akhir." - ".$x->diagnosa_akhir
                ->setCellValue ( "I".$no, $ruangan )
                ->setCellValue ( "J".$no, $x->kode_diagnosa." - ".$x->diagnosa )
                ->setCellValue ( "K".$no, $x->kode_tindakan." - ".$x->tindakan )
                ->setCellValue ( "L".$no, $x->dokter_pj )
                ->setCellValue ( "M".$no, ArrayAdapter::format("unslug",$x->carabayar) )
                ->setCellValue ( "N".$no, ArrayAdapter::format("unslug",$x->carapulang) );
        if(!in_array($ruangan,$_ruang)){
            $_ruang[]=$ruangan;
        }
	}
    
	$sheet->getStyle ( 'A2:P'.$no )->applyFromArray ($_thin);
/*end - BLOCK PASIEN MASUK*/
        
$filename= "DATA LAPORAN INDEX RAWAT JALAN - ".
           " - ".ArrayAdapter::format("date d M Y",$_POST['dari']).
           " - ".ArrayAdapter::format("date d M Y",$_POST['sampai']).".xls";

header ( 'Content-Type: application/vnd.ms-excel' );
header ( 'Content-Disposition: attachment;filename="'.$filename.'"' );
header ( 'Cache-Control: max-age=0' );
$writer = PHPExcel_IOFactory::createWriter ( $file, 'Excel5' );
$writer->save ( 'php://output' );

?>