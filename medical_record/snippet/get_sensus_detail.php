<?php 
/**
 * this file used for get all patient 
 * in certain room. 
 * show detail each patient.
 * 
 * @author 		: Nurul Huda
 * @license 	: Apache v3
 * @copyright 	: goblooge@gmail.com
 * @used 		: /medical_record/resource/php/laporan_pasien/lap_sensus.php
 * @service 	: get_sensus
 * @since		: 29 Oct 2016
 * @version		: 1.0.0
 * */
global $db;
require_once 'smis-base/smis-include-service-consumer.php';
require_once 'smis-libs-class/ServiceProviderList.php';

$data=array();
$response=new ServiceConsumer($db, "get_sensus");
$response->addData("command", "list");
$response->setEntity($_POST['ruangan']);
$response->addData("dari", $_POST['dari']);
$response->addData("sampai", $_POST['sampai']);
//$response->addData("tanggal", $_POST['tanggal']);
$response->addData("halaman", $_POST['halaman']);	
$response->setMode(ServiceConsumer::$SINGLE_MODE);
$response->execute();
$list=$response->getContent();
$dbtable=new DBTable($db, "smis_mr_sensus");
$diagnosa=new DBTable($db,"smis_mr_diagnosa");
$diagnosa->setViewForSelect(true);
loadLibrary("smis-libs-function-time");
foreach($list as $x){
    $dari = strtotime($_POST['dari']);
    $dari = date('Y-m-d',$dari);
    $sampai = strtotime($_POST['sampai']);
    $sampai = date('Y-m-d',$sampai);
    $waktu = strtotime($x['waktu']);
    $waktu = date('Y-m-d',$waktu);
    $waktu_pulang = strtotime($x['waktu_keluar']);
    $waktu_pulang = date('Y-m-d',$waktu_pulang);
    if($x['selesai'] == "0" && $waktu < $dari && $waktu >= $sampai && $waktu_pulang < $dari && $waktu_pulang >= $sampai) {
        continue;
    }
    /*if($x['selesai'] == "0" && substr($x['waktu'], 0,10) !=  substr($_POST['dari'],0,10) ) {
        continue;
    }*/
	$d['id_asal']       = $x['id'];
	$d['ruangan']       = $_POST['ruangan'];
	$d['asal_ruangan']  = $x['asal'];
	$d['nrm_pasien']    = $x['nrm_pasien'];
	$d['nama_pasien']   = $x['nama_pasien'];
	$d['noreg_pasien']  = $x['no_register'];
    
    /* Get Profile Number and Status Kunjungan RS */
    $data = array();
    $data['command']    = "edit";
    $data['id']         = $d['noreg_pasien'];
    $serv = new ServiceConsumer($db, "get_registered", $data, "registration");
    $serv->execute();
    $content = $serv->getContent();
	$d['profile_number'] = $content['profile_number'];
    $d['kunjungan_rs']  = $content['barulama']=="0"?"Baru":"Lama";
    /* End of Get Profile Number */
    
	$d['alamat']        = $x['alamat'];
	$d['jk']            = $x['jk'];
	$d['carapulang']    = $x['cara_keluar'];
	$d['carabayar']     = $x['carabayar'];
	$d['umur']          = $x['umur'];
	$d['kelas']         = "";
	$d['tgl_masuk']     = $x['waktu'];
	$d['tgl_pulang']    = $x['waktu_keluar'];
	$d['durasi']        = day_diff_only($x['waktu'],$x['waktu_keluar']);		
	$d['pindah_ke']     = $x['pindahan'];
    //$d['kunjungan']=$x['kunjungan'];
    //$waktu = substr($x['waktu'], 0, 10);
	$d['kunjungan']     = ($x['waktu']>=$_POST['dari'] && $x['waktu'] < $_POST['sampai'])?"Baru":"Lama";
    
    /* mengambil data dokter dpjp di pendaftaran */
    $data=array("noreg_pasien"=>$d['noreg_pasien']);
    $serv=new ServiceConsumer($db,"get_dokter_dpjp",$data,"registration");
    $serv->execute();    
    $d['dpjp']=$serv->getContent();;
	
    
    
	/**mengambil data diagnosa di local rekam medis
	 * - pertama ambil diagnosa igd terlebih dahulu, atau diagnosa yang pertama kali masuk
	 * 	 --- jika kosong berarti tidak ada diagnosa lagi, dan langsung keluar
	 *   --- jika tidak kosong maka masukan sebagai diagnosa_masuk dan sekaligus diagnosa_ruang , 
	 * 		 jaga-jaga kalau diagnosa ruang ternyata kosong
	 * 		 --- cek diagnosa ruangan yang terakhir, jika kosong sudah terisi seperti diagnosa awal dari igd.
	 *       --- jika tidak kosong untuk diagnosa ruangan pakai seperti diagnosa ruangan.
	 * */
	$diagnosa->addCustomKriteria(" ruangan "," LIKE '%' ");
	$diagnosa->setOrder(" id ASC ",true);
	$diag=$diagnosa->select(array("noreg_pasien"=>"=".$x['no_register'] ));
	if($diag!=NULL){
		$d['diagnosa_masuk']=($diag->nama_icd==""?$diag->diagnosa:$diag->nama_icd);
        $d['icd_masuk']=($diag->kode_icd==""?$diag->diagnosa:$diag->kode_icd);        
		$d['diagnosa_ruang']=$d['diagnosa_masuk'];
		$d['icd_ruang']=$d['icd_masuk'];
		$diagnosa->addCustomKriteria(" ruangan "," = '".$_POST['ruangan']."' ");
		$diagnosa->setOrder(" id DESC ",true);
		$diag=$diagnosa->select(array("noreg_pasien"=>"=".$x['no_register']));
		if($diag!=NULL){
			$d['diagnosa_ruang']=($diag->nama_icd==""?$diag->diagnosa:$diag->nama_icd);
            $d['icd_ruang']=($diag->kode_icd==""?$diag->diagnosa:$diag->kode_icd);
		}
	}
	/* end of rekam medis */
	
	$dbtable->insert($d);		
}
$res=new ResponsePackage();
$res->setStatus(ResponsePackage::$STATUS_OK);
$res->setContent($list[0]);
echo json_encode($res->getPackage());

?>