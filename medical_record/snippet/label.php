<?php 
/**
 * 
 * 
 * DEFAULT RECOMMENDED JS
 * <script type='text/javascript'>
 * jsPrintSetup.clearSilentPrint();
 * jsPrintSetup.setOption('printSilent', 1);
 * jsPrintSetup.setOption('orientation', jsPrintSetup.kLandscapeOrientation);
 * jsPrintSetup.definePaperSize(102, 102, "DOGTAG_SMIS_PRINT", "CUSTOM DOGTAG", "DOGTAG PAPER", 19, 291,jsPrintSetup.kPaperSizeMillimeters);
 * jsPrintSetup.setPaperSizeData(102);
 * jsPrintSetup.setPrinter('102');
 * bootbox_choose_print();
 * </script>
 * 
 * DEFAULT RECOMMENDED CSS
 * .label_container{width:155mm; height:500; font-size:13px;}
 * div.label-element {float:left; margin-left:1mm; margin-top:1mm; border:solid 1px #000; width:75mm; height:23mm;}
 * div.element-row {clear:both; padding-left:4px;}
 * div.element-title {float:left; width:50px;}
 * div.element-colon {float:left;margin-left:3px;margin-right:2px; }
 * div.element-content {float:left; width:auto;}
 * 
 * */

require_once "smis-base/smis-include-service-consumer.php";
global $db;
$_id=$_POST['id'];
$service=new ServiceConsumer($db,"get_patient",NULL,"registration");
$service->addData("command","edit");
$service->addData("id",$_id);
$service->execute();
$px=$service->getContent();
$json=json_encode($px);
$_person=json_decode($json);

$_total=getSettings($db,"mr-label-repeat","1")*1;
$_max_nama=getSettings($db,"mr-label-nama","20")*1;
$_max_alamat=getSettings($db,"mr-label-alamat","20")*1;
$_css=getSettings($db,"mr-label-css","");
$_js=getSettings($db,"mr-label-js","");


$kelamin=$_person->kelamin==0?"(L)":"(P)";
$nama=substr($_person->nama,0,$_max_nama)." ".$kelamin;
$umur=ArrayAdapter::format("date d M Y",$_person->tgl_lahir);
$nrm=ArrayAdapter::format("only-digit8",$_person->id);
$alamat=substr($_person->alamat_pasien,0,$_max_alamat)." </br> ".$_person->nama_kelurahan." - ".$_person->nama_kecamatan;

$element="<div class='label-element'>";
	$element.="<div class='element-row'>";
		$element.="<div class='element-title'>Nama</div>";
		$element.="<div class='element-colon'>:</div>";
		$element.="<div class='element-content'>".$nama."</div>";
	$element.="</div>";
	$element.="<div class='element-row'>";
		$element.="<div class='element-title'>TL</div>";
		$element.="<div class='element-colon'>:</div>";
		$element.="<div class='element-content'>".$umur."</div>";
	$element.="</div>";
	$element.="<div class='element-row'>";
		$element.="<div class='element-title'>No. RM</div>";
		$element.="<div class='element-colon'>:</div>";
		$element.="<div class='element-content'>".$nrm."</div>";
	$element.="</div>";
	$element.="<div class='element-row'>";
		$element.="<div class='element-title'>Alamat</div>";
		$element.="<div class='element-colon'>:</div>";
		$element.="<div class='element-content'>".$alamat."</div>";
	$element.="</div>";
$element.="</div>";

$result.="<div class='label_container'>";
for($i=0;$i<$_total;$i++){
	$result.=$element;
}
$result.="</div>";
echo $result.$_css.$_js;
?>