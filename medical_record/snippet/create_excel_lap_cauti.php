<?php 
/**
 * this file used for get all patient 
 * that go out from the hospital in
 * given date and export it as excel
 * 
 * @author 		: Nurul Huda
 * @license 	: Apache v3
 * @copyright 	: goblooge@gmail.com
 * @used 		: /medical_record/resource/php/laporan_pasien/lap_cauti.php
 * @service 	: get_pasien_pulang
 * @since		: 01 Feb 2017
 * @version		: 1.0.0
 * @database	: smist_mr_lab_cauti
 * */

require_once "smis-libs-out/php-excel/PHPExcel.php"; 
require_once "smis-base/smis-include-service-consumer.php";
loadLibrary("smis-libs-function-excel");

global $db;
$dbtable=new DBTable($db, "smist_mr_lap_cauti");
$dbtable->setOrder(" masuk ASC");
if(isset($_POST['ruangan']) && $_POST['ruangan']!="" ){
    $dbtable->addCustomKriteria("ruangan","='". $_POST['ruangan']."'");
}
$dbtable->setShowAll(true);
$data=$dbtable->view("","0");
$list=$data['data'];

show_error();
/*start - BLOK RESOURCE*/
	$_thin = array ();
	$_thin['borders']=array();
	$_thin['borders']['allborders']=array();
	$_thin['borders']['allborders']['style']=PHPExcel_Style_Border::BORDER_THIN ;

	$_center = array();
	$_center ['alignment']=array();
	$_center ['alignment']['horizontal']=PHPExcel_Style_Alignment::HORIZONTAL_CENTER;
/*end - BLOCK RESOURCE*/

/*start - BLOCK PROPERTIES FILE EXCEL*/
	$file = new PHPExcel ();
	$file->getProperties ()->setCreator ( $user->getUsername() );
	$file->getProperties ()->setLastModifiedBy ( $user->getNameOnly() );
	$file->getProperties ()->setTitle ( "Laporan cauti" );
	$file->getProperties ()->setSubject ( "Laporan cauti" );
	$file->getProperties ()->setDescription ( "Data Laporan Cauti " );
	$file->getProperties ()->setKeywords ( "pasien, cauti, rekam, medis" );
	$file->getProperties ()->setCategory ( "Rekam Medis" );
/*end - BLOCK PROPERTIES FILE EXCEL*/

/*start - BLOCK PASIEN MASUK*/
	$file->setActiveSheetIndex ( 0 );
	$sheet = $file->getActiveSheet ( 0 );
	$sheet  ->setTitle ( "PASIEN" );	
    $sheet	->mergeCells('A1:K1');
	$sheet	->setCellValue ( "A1", "LAPORAN INFEKSI SALURAN KENCING" );
	$sheet	->mergeCells ( "A2:A3") ->setCellValue ( "A2", "No." )
			->mergeCells ( "B2:B3") ->setCellValue ( "B2", "No. Reg" )
			->mergeCells ( "C2:C3")->setCellValue ( "C2", "NRM" )
			->mergeCells ( "D2:D3")->setCellValue ( "D2", "Nama" )
			->mergeCells ( "E2:E3")->setCellValue ( "E2", "Masuk" )
            ->mergeCells ( "F2:F3")->setCellValue ( "F2", "Keluar" )
            ->mergeCells ( "G2:G3")->setCellValue ( "G2", "Ruangan" )
            ->mergeCells ( "H2:J2")->setCellValue ( "H2", "Pemakaian Alat" )
            ->setCellValue ( "H3", "Pasang Alat" )
            ->setCellValue ( "I3", "Lepas Alat" )
            ->setCellValue ( "J3", "Lama" )
            ->mergeCells ( "K2:K3")->setCellValue ( "K2", "Infeksi" );
	$sheet  ->getStyle ( 'A1:K1' )
            ->applyFromArray ($_center);
		
	$sheet ->getColumnDimension ( "A" )->setAutoSize ( true );
	$sheet ->getColumnDimension ( "B" )->setAutoSize ( true );
	$sheet ->getColumnDimension ( "C" )->setAutoSize ( true );
	$sheet ->getColumnDimension ( "D" )->setAutoSize ( true );
	$sheet ->getColumnDimension ( "E" )->setAutoSize ( true );
	$sheet ->getColumnDimension ( "F" )->setAutoSize ( true );
	$sheet ->getColumnDimension ( "G" )->setAutoSize ( true );
	$sheet ->getColumnDimension ( "H" )->setAutoSize ( true );
	$sheet ->getColumnDimension ( "I" )->setAutoSize ( true );
	$sheet ->getColumnDimension ( "J" )->setAutoSize ( true );
	$sheet ->getColumnDimension ( "K" )->setAutoSize ( true );
	$sheet ->getStyle("A1:K2")->getFont()->setBold(true);
	$_ruang=array();
	$no=3;
	foreach($list as $x){
		$no++;
        date_excel_format($sheet,"E".$no,$x->masuk);
        date_excel_format($sheet,"F".$no,$x->pulang);
        date_excel_format($sheet,"H".$no,$x->pasang);
        date_excel_format($sheet,"I".$no,$x->lepas);
       
        $ruangan=ArrayAdapter::format("unslug",$x->ruangan);
        if($ruangan=="") $ruangan="-";
		$sheet	->setCellValue ( "A".$no, ($no-3)."." )
				->setCellValue ( "B".$no, "'".ArrayAdapter::format("only-digit6",$x->noreg_pasien))
				->setCellValue ( "C".$no, "'".ArrayAdapter::format("only-digit6",$x->nrm_pasien) )
				->setCellValue ( "D".$no, $x->nama_pasien )
                ->setCellValue ( "G".$no, $ruangan )
                ->setCellValue ( "J".$no, $x->lama )
                ->setCellValue ( "K".$no, $x->infeksi );
	}
    $no++;
    $sheet->mergeCells ( "A".$no.":J".$no)->setCellValue ( "A".$no, "TOTAL" );
    $sheet->setCellValue ( "K".$no, "=counta(K4:K".($no-1).")" );    
	$sheet->getStyle ( 'A2:K'.$no )->applyFromArray ($_thin);
    $sheet->getStyle("A".$no,":K".$no)->getFont()->setBold(true);
    $sheet->getStyle("A1:K3")->getFont()->setBold(true);
/*end - BLOCK PASIEN MASUK*/

$filename= "LAPORAN ISK CAUTI - ".
           " - ".ArrayAdapter::format("date d M Y",$_POST['dari']).
           " - ".ArrayAdapter::format("date d M Y",$_POST['sampai']).".xls";

header ( 'Content-Type: application/vnd.ms-excel' );
header ( 'Content-Disposition: attachment;filename="'.$filename.'"' );
header ( 'Cache-Control: max-age=0' );
$writer = PHPExcel_IOFactory::createWriter ( $file, 'Excel5' );
$writer->save ( 'php://output' );

?>