<?php 
/**
 * this file used for get all patient 
 * that go out from the hospital in
 * given date and export it as excel
 * 
 * @author 		: Nurul Huda
 * @license 	: Apache v3
 * @copyright 	: goblooge@gmail.com
 * @used 		: /medical_record/resource/php/laporan_pasien/lap_kunjungan.php
 * @service 	: get_sensus
 * @since		: 01 Feb 2017
 * @version		: 1.0.0
 * @database	: smis_mr_pasien_pulang
 * */

require_once "smis-libs-out/php-excel/PHPExcel.php"; 
require_once "smis-base/smis-include-service-consumer.php";

global $db;
$dbtable=new DBTable($db, "smis_mr_pasien_pulang");
$dbtable->setOrder(" tgl_masuk ASC");
$dbtable->setShowAll(true);
$data=$dbtable->view("","0");
$list=$data['data'];

show_error();
/*start - BLOK RESOURCE*/
	$_thin = array ();
	$_thin['borders']=array();
	$_thin['borders']['allborders']=array();
	$_thin['borders']['allborders']['style']=PHPExcel_Style_Border::BORDER_THIN ;

	$_center = array();
	$_center ['alignment']=array();
	$_center ['alignment']['horizontal']=PHPExcel_Style_Alignment::HORIZONTAL_CENTER;
/*end - BLOCK RESOURCE*/

/*start - BLOCK PROPERTIES FILE EXCEL*/
	$file = new PHPExcel ();
	$file->getProperties ()->setCreator ( $user->getUsername() );
	$file->getProperties ()->setLastModifiedBy ( $user->getNameOnly() );
	$file->getProperties ()->setTitle ( "Laporan Pasien Pulang" );
	$file->getProperties ()->setSubject ( "Laporan Pasien Pulang" );
	$file->getProperties ()->setDescription ( "Data Laporan Pasien Pulang " );
	$file->getProperties ()->setKeywords ( "pasien, pulang, rekam, medis" );
	$file->getProperties ()->setCategory ( "Rekam Medis" );
/*end - BLOCK PROPERTIES FILE EXCEL*/

/*start - BLOCK PASIEN MASUK*/
	$file->setActiveSheetIndex ( 0 );
	$sheet = $file->getActiveSheet ( 0 );
    
    $i = 1;
    $objDrawing = new PHPExcel_Worksheet_Drawing();
    $objDrawing->setName('logo_img');
    $objDrawing->setDescription('logo_img');
    $objDrawing->setPath(getLogoNonInterlaced());
    $objDrawing->setCoordinates('A'.$i);
    $objDrawing->setOffsetX(20); 
    $objDrawing->setOffsetY(5);
    $objDrawing->setWidth(100); 
    $objDrawing->setHeight(100);
    $objDrawing->setWorksheet($sheet);
    $i++;
    
    $sheet->setCellValue("C".$i, getSettings($db,"smis_autonomous_title"));
    $sheet->getStyle("C".$i)->getFont()->setBold(true)->setSize(14);
    $i++;
    $sheet->setCellValue("C".$i, getSettings($db,"smis_autonomous_address"));
    $sheet->getStyle("C".$i)->getFont()->setBold(true)->setSize(14);
    $i++;
    $sheet->setCellValue("C".$i, getSettings($db,"smis_autonomous_contact"));
    $sheet->getStyle("C".$i)->getFont()->setBold(true)->setSize(14);
    $i += 3;
    
	$sheet  ->setTitle ( "PASIEN" );	
    $sheet	->mergeCells('A'.$i.':V'.$i);
	$sheet	->setCellValue ( "A".$i, "PASIEN MASUK RUMAH SAKIT" );
    $sheet  ->getStyle ( 'A'.$i.':V'.$i )
            ->applyFromArray ($_center);
    $sheet ->getStyle('A'.$i.':V'.$i)->getFont()->setBold(true);
    $i++;
	$sheet	->setCellValue ( "A".$i, "No." )
			->setCellValue ( "B".$i, "No. Reg" )
			->setCellValue ( "C".$i, "NRM" )
			->setCellValue ( "D".$i, "No. Profile" )
			->setCellValue ( "E".$i, "Nama" )
			->setCellValue ( "F".$i, "Masuk" )
            ->setCellValue ( "G".$i, "Kelamin" )
            ->setCellValue ( "H".$i, "Kunjungan" )
            ->setCellValue ( "I".$i, "Kelurahan" )
            ->setCellValue ( "J".$i, "Kecamatan" )
            ->setCellValue ( "K".$i, "Kabupaten" )
            ->setCellValue ( "L".$i, "Pendidikan" )
            ->setCellValue ( "M".$i, "Pekerjaan" )
            ->setCellValue ( "N".$i, "Agama" )
            ->setCellValue ( "O".$i, "Kasus" )
            ->setCellValue ( "P".$i, "Ruangan" )
			->setCellValue ( "Q".$i, "ICD 10" )
            ->setCellValue ( "R".$i, "Diagnosa" )
            ->setCellValue ( "S".$i, "DPJP" )
            ->setCellValue ( "T".$i, "Carabayar" )
            ->setCellValue ( "U".$i, "Perusahaan" )
            ->setCellValue ( "V".$i, "Asuransi" )
            ->setCellValue ( "W".$i, "Kedatangan" )
            ->setCellValue ( "X".$i, "Golongan Umur" );
	$sheet  ->getStyle ( 'A'.$i.':X'.$i )
            ->applyFromArray ($_center);
		
	$sheet ->getColumnDimension ( "A" )->setAutoSize ( true );
	$sheet ->getColumnDimension ( "B" )->setAutoSize ( true );
	$sheet ->getColumnDimension ( "C" )->setAutoSize ( true );
	$sheet ->getColumnDimension ( "D" )->setAutoSize ( true );
	$sheet ->getColumnDimension ( "E" )->setAutoSize ( true );
	$sheet ->getColumnDimension ( "F" )->setAutoSize ( true );
	$sheet ->getColumnDimension ( "G" )->setAutoSize ( true );
	$sheet ->getColumnDimension ( "H" )->setAutoSize ( true );
	$sheet ->getColumnDimension ( "I" )->setAutoSize ( true );
	$sheet ->getColumnDimension ( "J" )->setAutoSize ( true );
	$sheet ->getColumnDimension ( "K" )->setAutoSize ( true );
    $sheet ->getColumnDimension ( "L" )->setAutoSize ( true );
    $sheet ->getColumnDimension ( "M" )->setAutoSize ( true );
    $sheet ->getColumnDimension ( "N" )->setAutoSize ( true );
    $sheet ->getColumnDimension ( "O" )->setAutoSize ( true );
    $sheet ->getColumnDimension ( "P" )->setAutoSize ( true );
    $sheet ->getColumnDimension ( "Q" )->setAutoSize ( true );
    $sheet ->getColumnDimension ( "R" )->setAutoSize ( true );
    $sheet ->getColumnDimension ( "S" )->setAutoSize ( true );
	$sheet ->getColumnDimension ( "T" )->setAutoSize ( true );
    $sheet ->getColumnDimension ( "U" )->setAutoSize ( true );
    $sheet ->getColumnDimension ( "V" )->setAutoSize ( true );
    $sheet ->getColumnDimension ( "W" )->setAutoSize ( true );
    $sheet ->getColumnDimension ( "X" )->setAutoSize ( true );
	$sheet ->getStyle('A'.$i.':V'.$i)->getFont()->setBold(true);
	$_ruang=array();
	$no = $i + 1;
    $data_pasien_start = $no;
	foreach($list as $x){
		$no++;
        if($x->tgl_masuk!="" && $x->tgl_masuk!="0000-00-00 00:00:00"){
            $pulang = new DateTime($x->tgl_masuk);
            $dateVal = PHPExcel_Shared_Date::PHPToExcel($pulang);
            $sheet  ->getStyle("F".$no)
                    ->getNumberFormat()
                    ->setFormatCode("dd-mm-yyyy HH:mm");
            $sheet  ->setCellValue("F".$no,$dateVal);            
        }
                
       
        $ruangan=ArrayAdapter::format("unslug",$x->ruang);
        if($ruangan=="") $ruangan="-";
		$sheet	->setCellValue ( "A".$no, ($no-2)."." )
				->setCellValue ( "B".$no, "'".ArrayAdapter::format("only-digit6",$x->noreg))
				->setCellValue ( "C".$no, "'".ArrayAdapter::format("only-digit6",$x->nrm) )
				->setCellValue ( "D".$no, $x->profile_number )
				->setCellValue ( "E".$no, $x->nama_pasien )
				->setCellValue ( "G".$no,  $x->jk==1?"P":"L")
                ->setCellValue ( "H".$no, strtoupper($x->kunjungan) )
				->setCellValue ( "I".$no, $x->kelurahan )
                ->setCellValue ( "J".$no, $x->kecamatan )
                ->setCellValue ( "K".$no, $x->kabupaten )
                ->setCellValue ( "L".$no, $x->pendidikan )
                ->setCellValue ( "M".$no, $x->pekerjaan )
                ->setCellValue ( "N".$no, $x->agama )
                ->setCellValue ( "O".$no, strtoupper($x->kasus) )
                ->setCellValue ( "P".$no, $ruangan )
                ->setCellValue ( "Q".$no, $x->kode_diagnosa )
                ->setCellValue ( "R".$no, $x->diagnosa )
                ->setCellValue ( "S".$no, $x->dokter_pj )
                ->setCellValue ( "T".$no, ArrayAdapter::format("unslug",$x->carabayar) )
                ->setCellValue ( "U".$no, $x->n_perusahaan )
                ->setCellValue ( "V".$no, $x->n_asuransi )
                ->setCellValue ( "W".$no, $x->caradatang )
                ->setCellValue ( "X".$no, $x->gol_umur );
        if(!in_array($ruangan,$_ruang)){
            $_ruang[]=$ruangan;
        }
	}
    
	$sheet->getStyle ( 'A'.$i.':X'.$no )->applyFromArray ($_thin);
/*end - BLOCK PASIEN MASUK*/



/*start - BLOCK SETTINGS*/
$END_NUM=$no;
$file   ->createSheet (1);
$file   ->setActiveSheetIndex ( 1 );
$sheet  = $file->getActiveSheet (1);

$i = 1;
$objDrawing = new PHPExcel_Worksheet_Drawing();
$objDrawing->setName('logo_img');
$objDrawing->setDescription('logo_img');
$objDrawing->setPath(getLogoNonInterlaced());
$objDrawing->setCoordinates('A'.$i);
$objDrawing->setOffsetX(20); 
$objDrawing->setOffsetY(5);
$objDrawing->setWidth(100); 
$objDrawing->setHeight(100);
$objDrawing->setWorksheet($sheet);
$i++;

$sheet->setCellValue("C".$i, getSettings($db,"smis_autonomous_title"));
$sheet->getStyle("C".$i)->getFont()->setBold(true)->setSize(14);
$i++;
$sheet->setCellValue("C".$i, getSettings($db,"smis_autonomous_address"));
$sheet->getStyle("C".$i)->getFont()->setBold(true)->setSize(14);
$i++;
$sheet->setCellValue("C".$i, getSettings($db,"smis_autonomous_contact"));
$sheet->getStyle("C".$i)->getFont()->setBold(true)->setSize(14);
$i += 3;

$index_ruangan_settings = $i;
$sheet  ->setTitle("SETTINGS");
$sheet  ->setCellValue("A".$i,"RUANGAN");
$sheet  ->setCellValue("B".$i,"PASIEN!\$O\$".$data_pasien_start.":\$O\$".$END_NUM);
$i++;
$index_kasus_settings = $i;
$sheet  ->setCellValue("A".$i,"KASUS");
$sheet  ->setCellValue("B".$i,"PASIEN!\$N\$".$data_pasien_start.":\$N\$".$END_NUM);
$i++;
$index_kunjungan_settings = $i;
$sheet  ->setCellValue("A".$i,"KUNJUNGAN");
$sheet  ->setCellValue("B".$i,"PASIEN!\$G\$".$data_pasien_start.":\$G\$".$END_NUM);
$i++;
$index_kedatangan_settings = $i;
$sheet  ->setCellValue("A".$i,"KEDATANGAN");
$sheet  ->setCellValue("B".$i,"PASIEN!\$V\$".$data_pasien_start.":\$V\$".$END_NUM);
$i++;
$index_carabayar_settings = $i;
$sheet  ->setCellValue("A".$i,"CARABAYAR");
$sheet  ->setCellValue("B".$i,"PASIEN!\$S\$".$data_pasien_start.":\$S\$".$END_NUM);
$i++;
$index_asuransi_settings = $i;
$sheet  ->setCellValue("A".$i,"ASURANSI");
$sheet  ->setCellValue("B".$i,"PASIEN!\$U\$".$data_pasien_start.":\$U\$".$END_NUM);
/*end - BLOCK SETTINGS*/


/* start - BLOCK RESUME PASIEN */
sort($_ruang);
$file   ->createSheet (2);
$file   ->setActiveSheetIndex ( 2 );
$sheet  = $file->getActiveSheet (2);

$i = 1;
$objDrawing = new PHPExcel_Worksheet_Drawing();
$objDrawing->setName('logo_img');
$objDrawing->setDescription('logo_img');
$objDrawing->setPath(getLogoNonInterlaced());
$objDrawing->setCoordinates('A'.$i);
$objDrawing->setOffsetX(20); 
$objDrawing->setOffsetY(5);
$objDrawing->setWidth(100); 
$objDrawing->setHeight(100);
$objDrawing->setWorksheet($sheet);
$i++;

$sheet->setCellValue("C".$i, getSettings($db,"smis_autonomous_title"));
$sheet->getStyle("C".$i)->getFont()->setBold(true)->setSize(14);
$i++;
$sheet->setCellValue("C".$i, getSettings($db,"smis_autonomous_address"));
$sheet->getStyle("C".$i)->getFont()->setBold(true)->setSize(14);
$i++;
$sheet->setCellValue("C".$i, getSettings($db,"smis_autonomous_contact"));
$sheet->getStyle("C".$i)->getFont()->setBold(true)->setSize(14);
$i += 3;
$start = $i;
$ips = $i + 1;
$ipd = $i + 2;

$sheet  ->setTitle("RESUME PASIEN");
$sheet	->mergeCells('A'.$i.':A'.$ipd);
$sheet	->setCellValue("A".$i,"RUANGAN");
$sheet	->mergeCells('B'.$i.':C'.$ips);
$sheet	->setCellValue("B".$i,"KASUS");
$sheet	->setCellValue("B".$ips,"BARU");
$sheet	->setCellValue("C".$ips,"LAMA");

$sheet	->mergeCells('D'.$i.':E'.$ips);
$sheet	->setCellValue("D".$i,"KUNJUNGAN");
$sheet	->setCellValue("D".$ipd,"BARU");
$sheet	->setCellValue("E".$ipd,"LAMA");

$sheet	->mergeCells('F'.$i.':N'.$i);
$sheet	->setCellValue("F".$i,"JENIS KUNJUNGAN");
$sheet	->mergeCells('F'.$ips.':F'.$ipd);
$sheet	->setCellValue("F".$ips,"DATANG SENDIRI");
$sheet	->mergeCells('G'.$ips.':N'.$ips);
$sheet	->setCellValue("G".$ips,"RUJUKAN");
$sheet	->setCellValue("G".$ipd,"DOKTER");
$sheet	->setCellValue("H".$ipd,"MANTRI");
$sheet	->setCellValue("I".$ipd,"BIDAN");
$sheet	->setCellValue("J".$ipd,"PUSKESMAS");
$sheet	->setCellValue("K".$ipd,"RS LAIN");
$sheet	->setCellValue("L".$ipd,"BALAI PENGOBATAN LAIN");
$sheet	->setCellValue("M".$ipd,"KARYAWAN");
$sheet	->setCellValue("N".$ipd,"LAINYA");


/*START - PENCARIAN PASIEN BERDASARKAN CARABAYAR*/
$serv=new ServiceConsumer($db,"get_carabayar",NULL,"registration");
$serv->execute();
$database=$serv->getContent();
$total=count($database);

/*END - PENCARIAN PASIEN BERDASARKAN CARABAYAR*/
$HURUF=array();
$HURUF[1]="O";
$HURUF[2]="P";
$HURUF[3]="Q";
$HURUF[4]="R";
$HURUF[5]="S";
$HURUF[6]="T";
$HURUF[7]="U";
$HURUF[8]="V";
$HURUF[9]="W";
$HURUF[10]="X";
$HURUF[11]="Y";
$HURUF[12]="Z";
$HURUF[13]="AA";
$HURUF[14]="AB";
$HURUF[15]="AC";
$HURUF[16]="AD";
$HURUF[17]="AE";
$HURUF[18]="AF";
$HURUF[19]="AG";
$HURUF[20]="AH";
$HURUF[21]="AI";
$HURUF[22]="AJ";
$LAST_ALPHABET=$HURUF[$total];
$sheet	->mergeCells('O'.$i.':'.$LAST_ALPHABET.$ips);
$sheet	->setCellValue("O".$i,"CARA BAYAR");
$total=0;
foreach($database as $d){
    $total++;
    $LAST_ALPHABET=$HURUF[$total];
    $sheet	->setCellValue($LAST_ALPHABET.$ipd,ArrayAdapter::format("unslug",$d['slug']));
}
 $_LAST_CARABAYAR_ALPHABET=$LAST_ALPHABET;
if($_POST['detail']=="1"){
   
    $total++;
    $_FIRST_ASURANSI_ALPHABET=$HURUF[$total];
    $LAST_ALPHABET=$HURUF[$total];

    $sheet	->setCellValue($LAST_ALPHABET.$ipd,"SKM");
    $total++;
    $LAST_ALPHABET=$HURUF[$total];
    $sheet	->setCellValue($LAST_ALPHABET.$ipd,"SEHATI");
    $total++;
    $LAST_ALPHABET=$HURUF[$total];
    $sheet	->setCellValue($LAST_ALPHABET.$ipd,"SPM");
    $total++;
    $LAST_ALPHABET=$HURUF[$total];
    $sheet	->setCellValue($LAST_ALPHABET.$ipd,"SKTM");
    $total++;
    $LAST_ALPHABET=$HURUF[$total];
    $sheet	->setCellValue($LAST_ALPHABET.$ipd,"JAMKESDA");
    $total++;
    $LAST_ALPHABET=$HURUF[$total];
    $sheet	->setCellValue($LAST_ALPHABET.$ipd,"JASA RAHARJA");
    $total++;
    $LAST_ALPHABET=$HURUF[$total];
}




$sheet  ->getStyle( 'A'.$i.':'.$LAST_ALPHABET.$ipd )->applyFromArray ($_center);
$sheet  ->getStyle("A".$i.":".$LAST_ALPHABET.$ipd)->getFont()->setBold(true);

$sheet ->getColumnDimension ( "A" )->setAutoSize ( true );
$sheet ->getColumnDimension ( "C" )->setAutoSize ( true );
$sheet ->getColumnDimension ( "D" )->setAutoSize ( true );
$sheet ->getColumnDimension ( "E" )->setAutoSize ( true );
$sheet ->getColumnDimension ( "G" )->setAutoSize ( true );
$sheet ->getColumnDimension ( "I" )->setAutoSize ( true );
$sheet ->getColumnDimension ( "J" )->setAutoSize ( true );
$sheet ->getColumnDimension ( "K" )->setAutoSize ( true );
$sheet ->getColumnDimension ( "L" )->setAutoSize ( true );
$sheet ->getColumnDimension ( "M" )->setAutoSize ( true );
$sheet ->getColumnDimension ( "N" )->setAutoSize ( true );



$no = $ipd;
foreach($_ruang as $r){
    $no++;
    $sheet	->setCellValue("A".$no,$r);  
   
    //$sheet  ->setCellValue("B".$no,'=COUNTIFS(INDIRECT(SETTINGS!$B$2),CONCATENATE("*",B$2,"*"),INDIRECT(SETTINGS!$B$1),$A1)');
    //$sheet  ->setCellValueExplicit("F".$no,"=COUNTIFS(INDIRECT(SETTINGS!\$B\$3);CONCATENATE(\"*\";F\$2;\"*\");INDIRECT(SETTINGS!\$B\$1);\$A1)");
    //$sheet  ->setCellValueExplicit("H".$no,"=COUNTIFS(INDIRECT(SETTINGS!\$B\$4);CONCATENATE(\"*\";H\$2;\"*\");INDIRECT(SETTINGS!\$B\$1);\$A1)");

}

$i = $ipd + 1;
$SEPARATOR=$_POST['separator'];
$sheet  ->setCellValueExplicit("B".$i,'=COUNTIFS(INDIRECT(SETTINGS!$B$'.$index_kasus_settings.')'.$SEPARATOR.'CONCATENATE("*"'.$SEPARATOR.'B$'.$ipd.$SEPARATOR.'"*")'.$SEPARATOR.'INDIRECT(SETTINGS!$B$'.$index_ruangan_settings.')'.$SEPARATOR.'$A'.$i.')');
$sheet  ->setCellValueExplicit("D".$i,'=COUNTIFS(INDIRECT(SETTINGS!$B$'.$index_kunjungan_settings.')'.$SEPARATOR.'CONCATENATE("*"'.$SEPARATOR.'D$'.$ipd.$SEPARATOR.'"*")'.$SEPARATOR.'INDIRECT(SETTINGS!$B$'.$index_ruangan_settings.')'.$SEPARATOR.'$A'.$i.')');
$sheet  ->setCellValueExplicit("F".$i,'=COUNTIFS(INDIRECT(SETTINGS!$B$'.$index_kedatangan_settings.')'.$SEPARATOR.'CONCATENATE("*"'.$SEPARATOR.'F$'.$ips.$SEPARATOR.'"*")'.$SEPARATOR.'INDIRECT(SETTINGS!$B$'.$index_ruangan_settings.')'.$SEPARATOR.'$A'.$i.')');
$sheet  ->setCellValueExplicit("G".$i,'=COUNTIFS(INDIRECT(SETTINGS!$B$'.$index_kedatangan_settings.')'.$SEPARATOR.'CONCATENATE("*"'.$SEPARATOR.'G$'.$ipd.$SEPARATOR.'"*")'.$SEPARATOR.'INDIRECT(SETTINGS!$B$'.$index_ruangan_settings.')'.$SEPARATOR.'$A'.$i.')');
$sheet  ->setCellValueExplicit("O".$i,'=COUNTIFS(INDIRECT(SETTINGS!$B$'.$index_carabayar_settings.')'.$SEPARATOR.'O$'.$ipd.$SEPARATOR.'INDIRECT(SETTINGS!$B$'.$index_ruangan_settings.')'.$SEPARATOR.'$A'.$i.')');
if($_POST['detail']=="1") 
    $sheet  ->setCellValueExplicit($_FIRST_ASURANSI_ALPHABET.$i,'=COUNTIFS(INDIRECT(SETTINGS!$B$'.$index_asuransi_settings.')'.$SEPARATOR.'CONCATENATE("*"'.$SEPARATOR.'Q$'.$ipd.$SEPARATOR.'"*")'.$SEPARATOR.'INDIRECT(SETTINGS!$B$'.$index_ruangan_settings.')'.$SEPARATOR.'$A'.$i.')');
$_total_ruang=count($_ruang)+$ipd;

$backgroound['fill']=array();
$backgroound['fill']['type']=PHPExcel_Style_Fill::FILL_SOLID;
$backgroound['fill']['color']=array();

$backgroound['fill']['color']['rgb']='99FFFF';
$sheet->getStyle ( 'B'.$i.':C'.($_total_ruang+1) )->applyFromArray ($backgroound);
$backgroound['fill']['color']['rgb']='FF99FF';
$sheet->getStyle ( 'D'.$i.':E'.($_total_ruang+1) )->applyFromArray ($backgroound);
$backgroound['fill']['color']['rgb']='FFFF99';
$sheet->getStyle ( 'F'.$i.':F'.($_total_ruang+1) )->applyFromArray ($backgroound);
$backgroound['fill']['color']['rgb']='FF9999';
$sheet->getStyle ( 'G'.$i.':N'.($_total_ruang+1) )->applyFromArray ($backgroound);
$backgroound['fill']['color']['rgb']='99FF99';
$sheet->getStyle ( 'O'.$i.':'.$_LAST_CARABAYAR_ALPHABET.($_total_ruang+1) )->applyFromArray ($backgroound);
if($_POST['detail']=="1"){
    $backgroound['fill']['color']['rgb']='9999FF';
    $sheet->getStyle ( $_FIRST_ASURANSI_ALPHABET.$i.':'.$LAST_ALPHABET.($_total_ruang+1) )->applyFromArray ($backgroound);
}


$sheet	->setCellValue("A".($_total_ruang+1),"TOTAL");
$sheet	->setCellValue("B".($_total_ruang+1),"=SUM(B".$ipd.":B".$_total_ruang.")");
$sheet	->setCellValue("C".($_total_ruang+1),"=SUM(C".$ipd.":C".$_total_ruang.")");
$sheet	->setCellValue("D".($_total_ruang+1),"=SUM(D".$ipd.":D".$_total_ruang.")");
$sheet	->setCellValue("E".($_total_ruang+1),"=SUM(E".$ipd.":E".$_total_ruang.")");
$sheet	->setCellValue("F".($_total_ruang+1),"=SUM(F".$ipd.":F".$_total_ruang.")");
$sheet	->setCellValue("G".($_total_ruang+1),"=SUM(G".$ipd.":G".$_total_ruang.")");
$sheet	->setCellValue("H".($_total_ruang+1),"=SUM(H".$ipd.":H".$_total_ruang.")");
$sheet	->setCellValue("I".($_total_ruang+1),"=SUM(I".$ipd.":I".$_total_ruang.")");
$sheet	->setCellValue("J".($_total_ruang+1),"=SUM(J".$ipd.":J".$_total_ruang.")");
$sheet	->setCellValue("K".($_total_ruang+1),"=SUM(K".$ipd.":K".$_total_ruang.")");
$sheet	->setCellValue("L".($_total_ruang+1),"=SUM(L".$ipd.":L".$_total_ruang.")");
$sheet	->setCellValue("M".($_total_ruang+1),"=SUM(M".$ipd.":M".$_total_ruang.")");
$sheet	->setCellValue("N".($_total_ruang+1),"=SUM(N".$ipd.":N".$_total_ruang.")");

foreach($HURUF as $nomor=>$h){
    if($_total_ruang!=0)
        $sheet	->setCellValue($h.($_total_ruang+1),"=SUM(".$h.$ipd.":".$h.$_total_ruang.")");
    if($h==$LAST_ALPHABET) 
        break;
}

$sheet ->getStyle("A".($_total_ruang+1).":".$LAST_ALPHABET.($_total_ruang+1))->getFont()->setBold(true);
$sheet->getStyle ( 'A'.$start.':'.$LAST_ALPHABET.($_total_ruang+1) )->applyFromArray ($_thin);
/* end - BLOCK RESUME PASIEN */

$filename= "DATA PASIEN MASUK ".ArrayAdapter::format("date d M Y",$_POST['dari'])." - ".ArrayAdapter::format("date d M Y",$_POST['sampai']).".xls";

header ( 'Content-Type: application/vnd.ms-excel' );
header ( 'Content-Disposition: attachment;filename="'.$filename.'"' );
header ( 'Cache-Control: max-age=0' );
$writer = PHPExcel_IOFactory::createWriter ( $file, 'Excel5' );
$writer->save ( 'php://output' );

?>