<?php 
	global $db;
	$id=NULL;
	if(isset($_POST["noreg_pasien"]) && $_POST["noreg_pasien"]!="" ){
		$id=array("noreg_pasien"=>$_POST["noreg_pasien"]);
	}else if(isset($_POST["nrm_pasien"]) && $_POST["nrm_pasien"]!=""){
		$id=array("nrm_pasien"=>$_POST["nrm_pasien"]);
	}else{
		return;
	}
	$dbtable=new DBTable($db,"");
	$up=array("nrm_pasien"=>$_POST["nrm_pasien_baru"],"nama_pasien"=>$_POST["nama_pasien_baru"]);
	$listname=array();
	$listname[]="smis_mr_diagnosa";
	$listname[]="smis_mr_gigimulut";
	$listname[]="smis_mr_igd";
	$listname[]="smis_mr_iklin";
	$listname[]="smis_mr_jiwa";
	$listname[]="smis_mr_kb";
	$listname[]="smis_mr_odontogram";
	$listname[]="smis_mr_operasi";
	$listname[]="smis_mr_persalinan";
	$listname[]="smis_mr_rencana";
	
	foreach($listname as $table){
		$dbtable->setName($table);
		$dbtable->update($up, $id);
	}
?>