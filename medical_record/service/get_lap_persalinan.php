<?php
require_once 'medical_record/class/template/LaporanPersalinanTemplate.php';
global $db;

$noreg = $_POST ['noreg_pasien'];
$nama = $_POST ['nama_pasien'];
$nrm = $_POST ['nrm_pasien'];

$page = $_POST ['page'];
$action = $_POST ['action'];
$polislug = $_POST ['polislug'];
$pslug = $_POST ['prototype_slug'];
$pname = $_POST ['prototype_name'];
$pimplement = $_POST ['prototype_implement'];

ob_start ();
$template = new LaporanPersalinanTemplate ( $db, $polislug, $noreg, $nrm, $nama, $page, $action, $pslug, $pname, $pimplement );
$template->initialize ();
$result = ob_get_clean ();
echo json_encode ( $result );

?>