<?php
require_once 'medical_record/class/template/LaporanRL311Template.php';
global $db;

$noreg = $_POST ['noreg_pasien'];
$id_antrian = $_POST ['id_antrian'];

$nama = $_POST ['nama_pasien'];
$nrm = $_POST ['nrm_pasien'];
$alamat = $_POST ['alamat'];
$jk = $_POST ['jk'];
$umur = $_POST ['umur'];
$asal_ruang = $_POST ['asal_ruang'];
$page = $_POST ['page'];
$action = $_POST ['action'];
$polislug = $_POST ['polislug'];
$pslug = $_POST ['prototype_slug'];
$pname = $_POST ['prototype_name'];
$pimplement = $_POST ['prototype_implement'];

ob_start ();
$template = new LaporanRL311Template( $db, $polislug, $noreg, $nrm, $nama, $alamat, $umur, $jk, $id_antrian, $asal_ruang, $page, $action, $pslug, $pname, $pimplement );
$template->initialize ();
$result = ob_get_clean ();
echo json_encode ( $result );

?>