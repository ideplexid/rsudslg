<?php
    global $db;
    require_once 'medical_record/class/template/LaporanDiagnosaRehabMedisTemplate.php';

    $page = $_POST ['page'];
    $action = $_POST ['action'];
    $polislug = $_POST ['polislug'];
    $pslug = $_POST ['prototype_slug'];
    $pname = $_POST ['prototype_name'];
    $pimplement = $_POST ['prototype_implement'];

    ob_start ();
    $template = new LaporanDiagnosaRehabMedisTemplate (
        $db,
        $polislug,
        $page,
        $action,
        $pslug,
        $pname,
        $pimplement
    );
    $template->initialize ();
    $result = ob_get_clean ();
    echo json_encode ( $result );
?>