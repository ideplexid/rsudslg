<?php 
	require_once("smis-base/smis-include-service-consumer.php");
	global $db;

	$noreg_pasien 	= $_POST['noreg_pasien'];
	$service 		= new ServiceConsumer($db, "check_last_position");
	$service->addData("noreg_pasien", $noreg_pasien);
	$service->setMode(ServiceConsumer::$MULTIPLE_MODE);
	$service->execute();
	$contents 		= $service->getContent();
	
	$data = array();
	$waktu_last 	= "";
	$selesai_last 	= -1;
	foreach($contents as $content) {
		foreach ($content as $c) {
			if ($c != null) {
				if ($data['ruangan'] == "" || ($c['selesai'] == "0" && $selesai_last == "1") || $waktu_last < $c['waktu']) {
					$data['ruangan'] 	= $c['entity'];
					$data['bed'] 		= $c['bed'];
					$waktu_last 		= $c['waktu'];
					$selesai_last 		= $c['selesai'];
				}
			}
		}
	}
	echo json_encode($data);
?>