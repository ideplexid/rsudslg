<?php
global $db;
require_once 'medical_record/class/template/MedicalRecordTemplate.php';

$noreg_pasien = $_POST ['noreg_pasien'];
$urji = $_POST ['urji'];
$nama_pasien = $_POST ['nama_pasien'];
$nrm_pasien = $_POST ['nrm_pasien'];
$gol_umur = $_POST ['gol_umur'];
$jk = $_POST ['jk'];
$kunjungan = $_POST ['kunjungan'];
$carabayar = $_POST ['carabayar'];
$page = $_POST ['page'];
$action = $_POST ['action'];
$polislug = $_POST ['polislug'];
$pslug = $_POST ['prototype_slug'];
$pname = $_POST ['prototype_name'];
$pimplement = $_POST ['prototype_implement'];

ob_start ();
$template = new MedicalRecordTemplate ( $db, MedicalRecordTemplate::$MODE_DAFTAR, $polislug, $noreg_pasien, $nrm_pasien, $nama_pasien, $page, $action, $pslug, $pname, $pimplement, $gol_umur, $jk, $kunjungan, $carabayar,$urji );
$template->initialize ();
$result = ob_get_clean ();
echo json_encode ( $result );

?>