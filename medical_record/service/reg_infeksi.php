<?php
global $db;
require_once 'medical_record/class/template/InfeksiTemplate.php';
show_error();
$page           = $_POST ['page'];
$action         = $_POST ['action'];
$polislug       = $_POST ['polislug'];
$pslug          = $_POST ['prototype_slug'];
$pname          = $_POST ['prototype_name'];
$pimplement     = $_POST ['prototype_implement'];
$noreg_pasien   = $_POST ['noreg_pasien'];
$nama_pasien    = $_POST ['nama_pasien'];
$nrm_pasien     = $_POST ['nrm_pasien'];

ob_start ();
$template = new InfeksiTemplate ( $db, InfeksiTemplate::$MODE_DAFTAR, $polislug, $page, $action, $pslug, $pname, $pimplement, $noreg_pasien, $nrm_pasien, $nama_pasien );
$template->initialize ();
$result = ob_get_clean ();
echo json_encode ( $result );

?>