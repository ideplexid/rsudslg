<?php
    global $db;

    $data = $db->get_result("
        SELECT *
        FROM (
            SELECT diagnosa_rehab_medis, kode_diagnosa, grup_diagnosa, COUNT(id) jumlah
            FROM smis_mr_diagnosa_rehab_medis
            WHERE prop = '' AND tanggal >= '" . $_POST['tanggal_dari'] . "' AND tanggal <= '" . $_POST['tanggal_sampai'] . "'
            GROUP BY kode_diagnosa
        ) v
        ORDER BY jumlah DESC
    ");

    $response_data = array(
        'data' => $data
    );
    echo json_encode($response_data);
?>