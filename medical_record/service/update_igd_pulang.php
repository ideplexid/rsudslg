<?php 

/**
 * this service used to update the out datetime of patient
 * in IGD room.
 * 
 * @author      : Nurul Huda
 * @database    : -  smis_mr_igd
 * @vesion      : 1.0.0
 * @since       : 18 Apr 2017
 * @license     : LGPLv3
 * @copyright   : goblooge@gmail.com
 * */

/*global $db;
$id=array();
$id['noreg_pasien']=$_POST['noreg_pasien'];
$id['ruangan']=$_POST['ruangan'];
$id['dikeluarkan']="0000-00-00 00:00:00";
$up=array();
$up['dikeluarkan']=date("Y-m-d H:i:s");
$dbtable=new DBTable($db,"smis_mr_igd");
$dbtable->update($up,$id);*/

global $db;
$id=array();
$id['noreg_pasien']=$_POST['noreg_pasien'];
$id['ruangan']=$_POST['ruangan'];
$id['dikeluarkan']="0000-00-00 00:00:00";
$up=array();
$up['dikeluarkan'] = $_POST['waktu_keluar'];
$dbtable=new DBTable($db,"smis_mr_igd");
$dbtable->update($up,$id);

$data = array();
$data['noreg_pasien'] = $_POST['noreg_pasien'];
$data['ruangan'] = $_POST['ruangan'];
$update = array();
$update['carapulang'] = $_POST['cara_keluar'];
$update['tgl_keluar'] = $_POST['waktu_keluar'];
$dbtable_update = new DBTable($db,"smis_mr_diagnosa");
$dbtable_update->update($update, $data);

?>