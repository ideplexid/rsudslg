<?php
require_once 'medical_record/class/template/LaporanIGDTemplate.php';
global $db;

$noreg = $_POST ['noreg_pasien'];
$nama = $_POST ['nama_pasien'];
$nrm = $_POST ['nrm_pasien'];
$waktu = $_POST ['waktu'];

$page = $_POST ['page'];
$action = $_POST ['action'];
$polislug = $_POST ['polislug'];
$pslug = $_POST ['prototype_slug'];
$pname = $_POST ['prototype_name'];
$pimplement = $_POST ['prototype_implement'];

ob_start ();
$template = new LaporanIGDTemplate ( $db, $polislug, $noreg, $nrm, $nama, $page, $action, $pslug, $pname, $pimplement,$waktu );
$template->initialize ();
$result = ob_get_clean ();
echo json_encode ( $result );

?>