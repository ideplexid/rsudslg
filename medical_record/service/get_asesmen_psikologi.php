<?php
require_once 'medical_record/class/template/AsesmenPsikologi.php';
global $db;

$noreg = $_POST ['noreg_pasien'];
$id_antrian = $_POST ['id_antrian'];

$nama           = $_POST ['nama_pasien'];
$nrm            = $_POST ['nrm_pasien'];
$alamat         = $_POST ['alamat'];
$jk             = $_POST ['jk'];
$umur           = $_POST ['umur'];
$tgl_lahir      = $_POST ['tgl_lahir'];
$page           = $_POST ['page'];
$action         = $_POST ['action'];
$polislug       = $_POST ['polislug'];
$pslug          = $_POST ['prototype_slug'];
$pname          = $_POST ['prototype_name'];
$pimplement     = $_POST ['prototype_implement'];
$carabayar      = $_POST ['carabayar'];
$waktu_masuk    = $_POST ['waktu_masuk'];

ob_start ();
$template = new AsesmenPsikologi( $db, $polislug, $noreg, $nrm, $nama, $alamat, $umur, $jk, $tgl_lahir, $waktu_masuk, $id_antrian, $page, $action,$carabayar, $pslug, $pname, $pimplement );
$template->initialize ();
$result = ob_get_clean ();
echo json_encode ( $result );

?>