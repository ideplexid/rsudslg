<?php 
/* this function used for getting the 
 * whole icd tindakan of patient 
 * in 1 periodic of treatment
 * used for inacbg's report in kasir 
 * 
 * @author   : Nurul Huda
 * @since    : 6 oct 2016
 * @version  : 1.0.0
 * @license  : LGPLv2
 * @copyright: goblooge@gmail.com
 * @database : smis_mr_operasi
 * */
 
global $db;
$noreg=$_POST['noreg_pasien'];
$hasil=array();
$q="SELECT DISTINCT nama_tindakan as nama_icd, icd_tindakan as kode_icd FROM smis_mr_operasi WHERE noreg_pasien='$noreg' AND prop!='del' ";
$result=$db->get_result($q);
foreach($result as $x){
	$hasil[$x->kode_icd]=$x;
}
echo json_encode($hasil);

?>