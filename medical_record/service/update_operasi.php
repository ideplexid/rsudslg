<?php 

global $db;
$id['noreg_pasien']=$_POST['noreg_pasien'];
$id['ruangan']=$_POST['ruangan'];
$up=array();
$up['noreg_pasien']=$_POST['noreg_pasien'];
$up['ruangan']=$_POST['ruangan'];
if($_POST['operator']!=""){
	$up['dokter_operator']=$_POST['operator'];
}

if($_POST['asisten_operator']!=""){
	$up['asisten_operator']=$_POST['asisten_operator'];
}

if($_POST['anastesi']!=""){
	$up['dokter_anastesi']=$_POST['anastesi'];
}

if($_POST['asisten_anastesi']!=""){
	$up['asisten_anastesi']=$_POST['asisten_anastesi'];
}

if($_POST['instrument']!=""){
	$up['instrument']=$_POST['instrument'];
}

if($_POST['nama_icd']!=""){
	$up['nama_tindakan']=$_POST['nama_icd'];
}

if($_POST['ket_icd']!=""){
	$up['ket_tindakan']=$_POST['ket_icd'];
}

if($_POST['kode_icd']!=""){
	$up['icd_tindakan']=$_POST['kode_icd'];
}

$query="SELECT * FROM smis_mr_diagnosa WHERE prop!='del' 
        AND diagnosa!='' 
        AND kode_icd!=''
        AND noreg_pasien='".$_POST['noreg_pasien']."' 
        ORDER BY id DESC LIMIT 0,1";
$result=$db->get_row($query);
if($result!=null){
    $diagnosa=(array) $result;
    if($diagnosa['sebab_sakit']!=""){
        $up['ket_diagnosa']=$diagnosa['sebab_sakit'];
    }
    
    if($diagnosa['kode_icd']!=""){
        $up['icd_diagnosa']=$diagnosa['kode_icd'];
    }
    
    if($diagnosa['nama_icd']!=""){
        $up['nama_diagnosa']=$diagnosa['nama_icd'];
    }
}

$dbtable=new DBTable($db, "smis_mr_operasi");
$result=$dbtable->insertOrUpdate($up, $id);
echo json_encode($up);
?>