<?php 

/**
 * using for get the list of Doctor Treatment
 * used for Resume Medic of each patient on each Number of Registration
 * 
 * @database : smis_mr_diagnosa
 * @used	 : - medical_record/modul/resume_medis.php
 * 			   - rawat/class/template/ResumeMedis.php
 * @author 	 : Nurul Huda
 * @copyright: goblooge@gmail.com
 * @license  : LGPLv3
 * @since	 : 14 may 2015
 * @version	 : 1.0.1
 * */
 
global $db;
$dbtable=new DBTable($db, "smis_mr_diagnosa");
$dbtable->setShowAll(true);

if(isset($_POST['noreg_pasien']))
	$dbtable->addCustomKriteria("noreg_pasien", "='".$_POST['noreg_pasien']."'");

 $service_provider = new ServiceProvider($dbtable);
 $data = $service_provider->command($_POST['command']);
 echo json_encode($data);

?>