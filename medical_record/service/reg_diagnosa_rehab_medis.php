<?php
    global $db;
    require_once 'medical_record/class/template/InputDataDiagnosaRehabMedisTemplate.php';

    $noreg_pasien = $_POST ['noreg_pasien'];
    $nama_pasien = $_POST ['nama_pasien'];
    $nrm_pasien = $_POST ['nrm_pasien'];
    $jk = $_POST ['jk'];
    $page = $_POST ['page'];
    $action = $_POST ['action'];
    $polislug = $_POST ['polislug'];
    $pslug = $_POST ['prototype_slug'];
    $pname = $_POST ['prototype_name'];
    $pimplement = $_POST ['prototype_implement'];

    ob_start ();
    $template = new InputDataDiagnosaRehabMedisTemplate (
        $db,
        $polislug,
        $noreg_pasien,
        $nrm_pasien,
        $nama_pasien,
        $jk,
        $page,
        $action,
        $pslug,
        $pname,
        $pimplement
    );
    $template->initialize ();
    $result = ob_get_clean ();
    echo json_encode ( $result );
?>