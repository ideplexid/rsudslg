<?php

/**
 * this source used to describe how lock work
 * the system wuould check if data cauti and plebitis and diagnose
 * is complete. so that the patinet could be out from system
 * @database    smis_mr_diagnosa
 *              smis_mr_cauti
 *              smis_mr_plebitis
 * @author      Nurul Huda
 * @copyright   goblooge@gmail.com
 * @license     Propietary
 * @since       12 Jan 2016
 * @version     1.0.0
 * */
 
global $db;
$rm_lock['diagnosa']="";
$rm_lock['plebitis']="";
$rm_lock['cauti']="";
$noreg=$_POST['noreg_pasien'];
$ruangan=$_POST['ruangan'];

/*start - checking diagnosa*/
    $query="SELECT count(*) as total FROM smis_mr_diagnosa WHERE prop!='del' and noreg_pasien='".$noreg."'; ";
    $total=$db->get_var($query);
    if($total*1==0){
        $rm_lock['diagnosa']="Diagnosa untuk No. Reg <strong>".$noreg."</strong> Belum di isi ";
    }
/*end - checking diagnosa*/

/*start - checking plebitis*/
    $dbtable=new DBTable($db,"smis_mr_plebitis");
    $dbtable->setShowAll(true);
    $dbtable->addCustomKriteria(" noreg_pasien "," = '".$noreg."' ");
    $dbtable->addCustomKriteria(" ruangan "," = '".$ruangan."' ");
    
    $dbtable->setOrder(" tanggal ASC ");
    $list=$dbtable->view("","0");
    $data=$list['data'];

    $status_plebitis=0;
    $total=count($data);
    $plebitis=array();
    $tgl_mulai="";
    $tgl_selesai="";
    $date_range=array();
    $kondisi=0;
    if($total*1==0){
        $rm_lock['plebitis']="Data Plebitis Belum di isi Sama Sekali";
    }else{
        foreach($data as $x){
            if($x->kegiatan=="Pasang Infus" || $x->kegiatan=="Infus terpasang dari ruang lain"){
                $tgl_mulai=($tgl_mulai=="" || $tgl_mulai>$x->tanggal) ?$x->tanggal :$tgl_mulai;
                $status_plebitis=$status_plebitis|1;//data pasang infus ada / sudah terpasang dari tempat lain;
            }
            
            if($x->kegiatan=="Lepas Infus" || $x->keluar!=""){
                $tgl_selesai=($tgl_selesai=="" || $tgl_selesai<$x->tanggal)?$x->tanggal:$tgl_selesai;
                $status_plebitis=$status_plebitis|2;//tanggal selesai ada;
            }
            $date_range[$x->tanggal]="v";
            $pulang_plebitis=$x->keluar;
        }
        
        /*checking jumlah data plebitis*/
        if( ($status_plebitis & 1) == 0){
             $rm_lock['plebitis'].="<li>Data Pertama Kali Pasang Infus Tidak di isi</li>";
        }
        
        if($pulang_plebitis==""){
            $rm_lock['plebitis'].="<li>Status keluar data Terakhir tidak diisi</li>";
        }
        
        if(strpos($pulang_plebitis,"lepas")!==false && ($status_plebitis & 2) == 0 ){
           $rm_lock['plebitis'].="<li>Data Lepas Infus Tidak di isi padahal kondisi keluar '<strong>".$pulang_plebitis."</strong>'</li>";
        }
        
        
        if($tgl_mulai!=""){
            loadLibrary("smis-libs-function-time");
            $rentang=day_diff($tgl_mulai,$tgl_selesai);
            if($total<$rentang){
                $rm_lock['plebitis'].="<li>Pengisian Data Tidak Lengkap Jumlah data hanya '".$total."' padahal rentang ".$rentang." Hari (".ArrayAdapter::format("date d M Y",$tgl_mulai)." - ".ArrayAdapter::format("date d M Y",$tgl_selesai)." )</li>" ;
            }
            $range=createDateRangeArray($tgl_mulai,$tgl_selesai);
            foreach($range as $dt){
                if(!isset($date_range[$dt])){
                    $rm_lock['plebitis'].="<li>Tanggal (".ArrayAdapter::format("date d M Y",$dt)." tidak diisi )</li>" ;
                }
            }
        }
        
    }
    if($rm_lock['plebitis']!=""){
        $rm_lock['plebitis']="<ol>".$rm_lock['plebitis']."<ol>";
    }
/*end - checking plebitis*/


/*start - checking plebitis*/
    $dbtable=new DBTable($db,"smis_mr_cauti");
    $dbtable->setShowAll(true);
    $dbtable->addCustomKriteria(" noreg_pasien "," = '".$noreg."' ");
    $dbtable->addCustomKriteria(" ruangan "," = '".$ruangan."' ");
    $dbtable->setOrder(" tanggal ASC ");
    $list=$dbtable->view("","0");
    $data=$list['data'];

    $status_cauti=0;
    $total=count($data);
    $cauti=array();
    $tgl_mulai="";
    $tgl_selesai="";
    $date_range=array();
    $kondisi=0;
    if($total*1==0){
        $rm_lock['cauti']="Data Catheter Belum di isi Sama Sekali";
    }else{
        foreach($data as $x){
            if($x->kegiatan=="Pasang Catheter" || $x->kegiatan=="Catheter terpasang dari ruang lain"){
                $tgl_mulai=$tgl_selesai=="" || $tgl_mulai>$x->tanggal ?$x->tanggal :$tgl_mulai;
                $status_cauti=$status_cauti|1;//data pasang infus ada;
            }
            
            if($x->kegiatan=="Lepas Catheter" || $x->keluar!=""){
                $tgl_selesai=$tgl_selesai="" || $tgl_selesai<$x->tanggal?$x->tanggal:$tgl_selesai;
                $status_cauti=$status_cauti|2;//tanggal selesai ada;
            }
            $date_range[$x->tanggal]="v";
            $pulang_cauti=$x->keluar;
        }
        
        /*checking jumlah data cauti*/
        if( ($status_cauti & 1) == 0){
             $rm_lock['cauti'].="<li>Data Pertama Kali Pasang Catheter Tidak di isi</li>";
        }
        
        if($pulang_cauti==""){
            $rm_lock['cauti'].="<li>Status keluar data Terakhir tidak diisi</li>";
        }
        
        if(strpos($pulang_cauti,"lepas")!==false && ($status_cauti & 2) == 0 ){
           $rm_lock['cauti'].="<li>Data Lepas Catheter Tidak di isi padahal kondisi keluar '<strong>".$pulang_plebitis."</strong>'</li>";
        }
        
        
        
        if($tgl_mulai!=""){    
            loadLibrary("smis-libs-function-time");
            $rentang=day_diff($tgl_mulai,$tgl_selesai);
            if($total<$rentang){
                $rm_lock['cauti'].="<li>Pengisian Data Tidak Lengkap Jumlah data hanya '".$total."' padahal rentang ".$rentang." Hari (".ArrayAdapter::format("date d M Y",$tgl_mulai)." - ".ArrayAdapter::format("date d M Y",$tgl_selesai)." )</li>" ;
            }
            $range=createDateRangeArray($tgl_mulai,$tgl_selesai);
            foreach($range as $dt){
                if(!isset($date_range[$dt])){
                    $rm_lock['cauti'].="<li>Tanggal (".ArrayAdapter::format("date d M Y",$dt)." tidak diisi )</li>" ;
                }
            }
        }
        
    }
    if($rm_lock['cauti']!=""){
        $rm_lock['cauti']="<ol>".$rm_lock['cauti']."<ol>";
    }
/*end - checking cauti*/

echo json_encode($rm_lock);
?>