<?php 
/**
 * providing service for viewing 
 * data icd 10 for
 * 
 * @database : smis_mr_icd
 * @author 	 : Nurul Huda
 * @copyright: goblooge@gmail.com
 * @license  : LGPLv3
 * @since	 : 5 Aug 2017
 * @version	 : 1.0.1
 * */
 
global $db;
if(isset($_POST['command']) && $_POST['command']!=""){
    $dbtable=new DBTable($db,"smis_mr_icd");
    $dbres=new ServiceProvider($dbtable);
    $data=$dbres->command($_POST['command']);
    echo json_encode($data);
}

?>