<?php 
	global $db;
	require_once("smis-base/smis-include-service-consumer.php");
	$noreg=$_POST['noreg_pasien'];
	$ser=new ServiceConsumer($db,"check_last_position");
	$ser->addData("noreg_pasien", $noreg);
	$ser->setMode(ServiceConsumer::$MULTIPLE_MODE);
	$ser->execute();
	$con=$ser->getContent();
	
	$ruangan="";
	$waktu_last="";
	$selesai_last=-1;
	foreach($con as $x){
		foreach ($x as $one){
			if($one!=null){
				if($ruangan=="" || ($one['selesai']=="0" && $selesai_last=="1") || $waktu_last<$one['waktu'] ){
					$ruangan=$one['entity'];
					$waktu_last=$one['waktu'];
					$selesai_last=$one['selesai'];
				}
			}
		}
	}
	echo json_encode($ruangan);
?>