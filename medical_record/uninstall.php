<?php
	global $wpdb;
	
	require_once 'smis-libs-inventory/uninstall.php';
	$uninstall = new InventoryUninstallator($wpdb, "", "");
	$uninstall->setUsing(false, true, true);
	$uninstall->extendInstall("mr");
	$uninstall->install();
?>