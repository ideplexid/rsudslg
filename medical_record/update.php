<?php
	global $wpdb;

	require_once 'smis-libs-inventory/install.php';
	$install = new InventoryInstallator($wpdb, "", "");
	$install->setUsing(false, true, true);
	$install->extendInstall("mr");
	$install->install();
	
    require_once 'medical_record/resource/install/table/smis_mr_alergi_obat.php';
	require_once 'medical_record/resource/install/table/smis_mr_asuhan_obat.php';
    require_once 'medical_record/resource/install/table/smis_mr_cauti.php';
    require_once 'medical_record/resource/install/table/smis_mr_diagnosa.php';
    require_once 'medical_record/resource/install/table/smis_mr_gigimulut.php';
    require_once 'medical_record/resource/install/table/smis_mr_icd.php';
    require_once 'medical_record/resource/install/table/smis_mr_icdtindakan.php';
    require_once 'medical_record/resource/install/table/smis_mr_igd.php';
    require_once 'medical_record/resource/install/table/smis_mr_iklin.php';
    require_once 'medical_record/resource/install/table/smis_mr_imunisasi.php';
    require_once 'medical_record/resource/install/table/smis_mr_index_dokter.php';
    require_once 'medical_record/resource/install/table/smis_mr_jiwa.php';
    require_once 'medical_record/resource/install/table/smis_mr_kb.php';
    require_once 'medical_record/resource/install/table/smis_mr_kematian.php';
    require_once 'medical_record/resource/install/table/smis_mr_lap_ri.php';
    require_once 'medical_record/resource/install/table/smis_mr_lap_rj.php';
    require_once 'medical_record/resource/install/table/smis_mr_odontogram.php';
    require_once 'medical_record/resource/install/table/smis_mr_operasi.php';
    require_once 'medical_record/resource/install/table/smis_mr_pasien_pulang.php';
    require_once 'medical_record/resource/install/table/smis_mr_persalinan.php';
    require_once 'medical_record/resource/install/table/smis_mr_pkhusus.php';
    require_once 'medical_record/resource/install/table/smis_mr_plebitis.php';
    require_once 'medical_record/resource/install/table/smis_mr_prolanis.php';
    require_once 'medical_record/resource/install/table/smis_mr_quisioner_ri.php';
    require_once 'medical_record/resource/install/table/smis_mr_quisioner_rj.php';
    require_once 'medical_record/resource/install/table/smis_mr_rawat_inap.php';
    require_once 'medical_record/resource/install/table/smis_mr_rawat_jalan.php';
    require_once 'medical_record/resource/install/table/smis_mr_rencana.php';
    require_once 'medical_record/resource/install/table/smis_mr_resume_ri.php';
    require_once 'medical_record/resource/install/table/smis_mr_resume_ruangan.php';
    require_once 'medical_record/resource/install/table/smis_mr_rl12.php';
    require_once 'medical_record/resource/install/table/smis_mr_rl13.php';
    require_once 'medical_record/resource/install/table/smis_mr_rl52b.php';
    require_once 'medical_record/resource/install/table/smis_mr_sensus_harian.php';
    require_once 'medical_record/resource/install/table/smis_mr_lap_diag_pertanggal.php';
    require_once 'medical_record/resource/install/table/smist_mr_lap_cauti.php';
    require_once 'medical_record/resource/install/table/smist_mr_lap_plebitis.php';
    require_once 'medical_record/resource/install/table/smist_mr_lap_rajal.php';
    require_once 'medical_record/resource/install/table/smist_mr_lap_ranap.php';
	require_once 'medical_record/resource/install/table/smist_mr_lap_kunjptgl.php';
    require_once 'medical_record/resource/install/table/smis_mr_management_bed.php';
    
    require_once 'medical_record/resource/install/table/smis_mr_poligigi.php';
    require_once 'medical_record/resource/install/table/smis_mr_politht.php';
    require_once 'medical_record/resource/install/table/smis_mr_poliorthopedi.php';
    require_once 'medical_record/resource/install/table/smis_mr_polikulit.php';
    require_once 'medical_record/resource/install/table/smis_mr_poliparu.php';
    require_once 'medical_record/resource/install/table/smis_mr_polibedah.php';
    require_once 'medical_record/resource/install/table/smis_mr_poliurologi.php';
    require_once 'medical_record/resource/install/table/smis_mr_polidalam.php';
    require_once 'medical_record/resource/install/table/smis_mr_polibedahanak.php';
    require_once 'medical_record/resource/install/table/smis_mr_polianestesi.php';
    require_once 'medical_record/resource/install/table/smis_mr_polirehab.php';
    require_once 'medical_record/resource/install/table/smis_mr_polimata.php';
    require_once 'medical_record/resource/install/table/smis_mr_poliobstetri.php';
    require_once 'medical_record/resource/install/table/smis_mr_poliginekologi.php';
    
    require_once 'medical_record/resource/install/table/smis_mr_assesmen_syaraf.php';
    require_once 'medical_record/resource/install/table/smis_mr_assesmen_anak.php';
    require_once 'medical_record/resource/install/table/smis_mr_assesmen_awal_perawat.php';
    require_once 'medical_record/resource/install/table/smis_mr_assesmen_jantung.php';
    require_once 'medical_record/resource/install/table/smis_mr_assesmen_psikologi.php';
    require_once 'medical_record/resource/install/table/smis_mr_assesmen_psikiatri.php';
	
    require_once "medical_record/resource/install/view/smis_mr_rl12g.php";
	require_once "medical_record/resource/install/view/smis_mr_rl12rs.php";
	require_once "medical_record/resource/install/view/smis_mr_rl12f.php";
	require_once "medical_record/resource/install/view/smis_mr_rl534.php";
	require_once "medical_record/resource/install/view/smis_mr_stp.php";
	require_once "medical_record/resource/install/view/smis_mr_stp_lama.php";
	require_once "medical_record/resource/install/view/smis_vmr_igd.php";
	require_once "medical_record/resource/install/view/smis_vmr_rl54.php";
	require_once "medical_record/resource/install/view/smis_vmr_morbiditas.php";
	require_once "medical_record/resource/install/view/smis_vmr_morbiditas_grup.php";
    
    require_once 'medical_record/resource/install/table/smis_mr_sensus.php';
    
    require_once 'medical_record/resource/install/table/smis_mr_infeksi.php';
    require_once 'medical_record/resource/install/table/smis_mr_jenis_perawatan.php';

    require_once 'medical_record/resource/install/table/smis_mr_jenis_rs.php';
    require_once 'medical_record/resource/install/table/smis_mr_kepemilikan_rs.php';
    require_once 'medical_record/resource/install/table/smis_mr_penyelenggara_rs.php';
    require_once 'medical_record/resource/install/table/smis_mr_data_dasar_rs.php';
    require_once 'medical_record/resource/install/table/smis_mr_kamar_tidur.php';	
    require_once 'medical_record/resource/install/table/smis_mr_perinatologi.php';
    require_once 'medical_record/resource/install/table/smis_mr_rehabilitasi_medik.php';
    require_once 'medical_record/resource/install/table/smis_mr_keluarga_berencana.php';
    require_once 'medical_record/resource/install/table/smis_mr_jenis_kegiatan_rl52.php';

    require_once 'medical_record/resource/install/table/smis_mr_icd_rehab_medis.php';
    require_once 'medical_record/resource/install/table/smis_mr_diagnosa_rehab_medis.php';
?>
