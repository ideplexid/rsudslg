## Description ##
Safethree Operating System is a framework based on PHP, Bootstrap  and JQuery. 
Safethree is Trademark own by Nurul Huda <goblooge@gmail.com> , All Right Reserved. 


# Create New Project #
* mkdir /path/to/your/project
* cd /path/to/your/project
* git init
* git remote add origin https://goblooge@bitbucket.org/goblooge/safethree-os.git

# remove deleted file #
git ls-files --deleted -z | xargs -0 git rm

# Create your first file, commit, and push #
* echo "Nurul  Huda" >> contributors.txt
* git add contributors.txt
* git commit -m 'Initial commit with contributors'
* git push -u origin master

# If Have Existing File #
* cd /path/to/my/repo
* git remote add origin https://goblooge@bitbucket.org/goblooge/safethree-os.git
* git push -u origin --all # pushes up the repo and its refs for the first time
* git push -u origin --tags # pushes up any tags

# Config File #
[core]
        repositoryformatversion = 0
        filemode = true
        bare = false
        logallrefupdates = true
[remote "origin"]
        url = https://goblooge@bitbucket.org/goblooge/safethree-os.git
        fetch = +refs/heads/*:refs/remotes/origin/*
[user]
        name=Nurul Huda
        email=goblooge@gmail.com
        
