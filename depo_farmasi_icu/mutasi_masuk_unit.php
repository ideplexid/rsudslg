<?php
	require_once("depo_farmasi_icu/library/InventoryLibrary.php");
	require_once("smis-libs-inventory/mutasi_masuk_unit.php");
	global $db;
	$mutasi_masuk_unit = new MutasiMasukUnit($db, "Depo Farmasi ICU", InventoryLibrary::$_TBL_OBAT_MASUK, InventoryLibrary::$_TBL_STOK_OBAT, InventoryLibrary::$_TBL_KARTU_STOK_OBAT, InventoryLibrary::$_TBL_RIWAYAT_STOK_OBAT, "depo_farmasi_icu");
	$mutasi_masuk_unit->initialize();
?>