<?php
class PenjualanResepTable extends Table {
	public function getBodyContent() {
		$content = "";
		if ($this->content!=NULL) {
			foreach ($this->content as $d) {
				$content .= "<tr>";
				foreach ($this->header as $h) {
					$content .= "<td>" . $d[$h] . "</td>";
				}
				if ($this->is_action) {
					$content .= "<td>" . $this->getFilteredContentButton($d['id'], $d['edukasi'], $d['dibatalkan'], $d['tipe'], $d['uri'], $d['tanggal'], $d['etiket_obat'])->getHtml() . "</td>";
				}
				$content .= "</tr>";
			}
		}
		return $content;
	}
	public function getFilteredContentButton($id, $edukasi, $dibatalkan, $tipe, $uri, $tanggal, $etiket_obat) {
		$btn_group = new ButtonGroup("noprint");
		if ($dibatalkan) {
			$btn = new Button("", "", "Lihat Resep");
			$btn->setAction($this->action . ".detail('" . $id . "')");
			$btn->setClass("btn-danger");
			$btn->setAtribute("data-content='Lihat' data-toggle='popover'");
			$btn->setIcon("icon-eye-open icon-white");
			$btn->setIsButton(Button::$ICONIC);
			$btn_group->addElement($btn);
			$btn = new Button("", "", "Lihat Alasan Batal");
			$btn->setAction($this->action . ".view_cancel_info('" . $id . "')");
			$btn->setClass("btn-inverse");
			$btn->setAtribute("data-content='Lihat Alasan Batal' data-toggle='popover'");
			$btn->setIcon("fa fa-comment-o");
			$btn->setIsButton(Button::$ICONIC);
			$btn_group->addElement($btn);
		} else {
			$btn = new Button("", "", "Cetak Resep");
			$btn->setAction($this->action . ".print_prescription('" . $id . "')");
			$btn->setClass("btn-inverse");
			$btn->setAtribute("data-content='Cetak Resep' data-toggle='popover'");
			$btn->setIcon("icon-print icon-white");
			$btn->setIsButton(Button::$ICONIC);
			$btn_group->addElement($btn);
			if ($etiket_obat == 1) {
				$btn = new Button("", "", "Cetak Semua Etiket Obat");
				$btn->setAction($this->action . ".print_all_etiket('" . $id . "')");
				$btn->setClass("btn-inverse");
				$btn->setAtribute("data-content='Cetak Semua Etiket Obat' data-toggle='popover'");
				$btn->setIcon("fa fa-ticket");
				$btn->setIsButton(Button::$ICONIC);
				$btn_group->addElement($btn);
			}
			$btn = new Button("", "", "Lihat");
			$btn->setAction($this->action . ".detail('" . $id . "')");
			$btn->setClass("btn-success");
			$btn->setAtribute("data-content='Lihat' data-toggle='popover'");
			$btn->setIcon("icon-eye-open icon-white");
			$btn->setIsButton(Button::$ICONIC);
			$btn_group->addElement($btn);
			if (!$edukasi) {
				$btn = new Button("", "", "Ubah ke Sudah Diedukasi");
				$btn->setAction($this->action . ".educate('" . $id . "')");
				$btn->setClass("btn-success");
				$btn->setAtribute("data-content='Ubah ke Sudah Diberikan/Diedukasi' data-toggle='popover'");
				$btn->setIcon("fa fa-check-square-o");
				$btn->setIsButton(Button::$ICONIC);
				$btn_group->addElement($btn);
			} else {
				$btn = new Button("", "", "Ubah ke Belum Diedukasi");
				$btn->setAction($this->action . ".undo_educate('" . $id . "')");
				$btn->setClass("btn-danger");
				$btn->setAtribute("data-content='Ubah ke Belum Diberikan/Diedukasi' data-toggle='popover'");
				$btn->setIcon("fa fa-square-o");
				$btn->setIsButton(Button::$ICONIC);
				$btn_group->addElement($btn);
			}
			if ($tipe == "resep") {
				$btn = new Button("", "", "Ubah Status IRNA / IRJA");
				if ($uri == 0)
					$btn->setAction($this->action . ".change_uri_irna('" . $id . "')");
				else
					$btn->setAction($this->action . ".change_uri_irja('" . $id . "')");
				$btn->setClass("btn-inverse");
				$btn->setAtribute("data-content='Ubah Status IRNA/IRJA' data-toggle='popover'");
				$btn->setIcon("fa fa-refresh");
				$btn->setIsButton(Button::$ICONIC);
				$btn_group->addElement($btn);
			}
			global $db;
			$lock_transact_date = getSettings($db, "depo_farmasi4-penjualan-lock_transact_date", "") == "" ? date("Y-m-d H:i") : getSettings($db, "depo_farmasi4-penjualan-lock_transact_date", "") . " 23:59";
			if (strtotime($tanggal) > strtotime($lock_transact_date)) {
				$btn = new Button("", "", "Resep Susulan");
				$btn->setAction($this->action . ".copy_header('" . $id . "')");
				$btn->setClass("btn-inverse");
				$btn->setAtribute("data-content='Resep Susulan' data-toggle='popover'");
				$btn->setIcon("fa fa-files-o");
				$btn->setIsButton(Button::$ICONIC);
				$btn_group->addElement($btn);
			}
			
			$btn = new Button("", "", "Cetak Rekap Resep - No. Resep");
			$btn->setAction($this->action . ".print_all_prescription('" . $id . "')");
			$btn->setClass("btn-inverse");
			$btn->setAtribute("data-content='Cetak Rekap Resep - No. Resep' data-toggle='popover'");
			$btn->setIcon("fa fa-print");
			$btn->setIsButton(Button::$ICONIC);
			$btn_group->addElement($btn);
			$btn = new Button("", "", "Cetak Rekap Resep - No. Reg. Pasien");
			$btn->setAction($this->action . ".print_all_prescription_by_noreg('" . $id . "')");
			$btn->setClass("btn-inverse");
			$btn->setAtribute("data-content='Cetak Rekap Resep - No. Reg. Pasien' data-toggle='popover'");
			$btn->setIcon("fa fa-print");
			$btn->setIsButton(Button::$ICONIC);
			$btn_group->addElement($btn);

			if (strtotime($tanggal) > strtotime($lock_transact_date)) {
				$btn = new Button("", "", "Batal");
				$btn->setAction($this->action . ".cancel('" . $id . "')");
				$btn->setClass("btn-danger");
				$btn->setAtribute("data-content='Batal' data-toggle='popover'");
				$btn->setIcon("icon-remove icon-white");
				$btn->setIsButton(Button::$ICONIC);
				$btn_group->addElement($btn);
			}
		}
		return $btn_group;
	}
}
?>