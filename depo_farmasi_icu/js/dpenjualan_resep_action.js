function DResepAction(name, page, action, column) {
	this.initialize(name, page, action, column);
}
DResepAction.prototype.constructor = DResepAction;
DResepAction.prototype = new TableAction();
DResepAction.prototype.show_add_obat_jadi_form = function() {
	if ($(".btn").attr("disabled") == "disabled")
		return;
	$(".btn").removeAttr("disabled");
	$(".btn").attr("disabled", "disabled");
	$("#obat_jadi_id").val("");
	$("#obat_jadi_id_obat").val("");
	$("#obat_jadi_kode_obat").val("");
	$("#obat_jadi_name_obat").val("");
	$("#obat_jadi_nama_obat").val("");
	$("#obat_jadi_nama_jenis_obat").val("");
	$("#obat_jadi_satuan").removeAttr("onchange");
	$("#obat_jadi_satuan").attr("onchange", "obat.setDetailInfo()");
	$("#obat_jadi_satuan").html("");
	$("#obat_jadi_stok").val("");
	$("#obat_jadi_f_stok").val("");
	$("#obat_jadi_jumlah_lama").val(0);
	$("#obat_jadi_hna").val("");
	$("#obat_jadi_embalase").val("Rp. " + parseFloat(embalase_obat_jadi).formatMoney("2", ".", ","));
	$("#obat_jadi_tuslah").val("Rp. " + parseFloat(tuslah_obat_jadi).formatMoney("2", ".", ","));
	$("#obat_jadi_markup").val(0);
	$("#obat_jadi_jumlah").val("");
	$("#obat_jadi_aturan_pakai").val("-");
	$("#obat_jadi_tanggal_exp").val("");
	$("#obat_jadi_obat_luar").val(0);
	$("#obat_jadi_obat_luar").trigger("change");
	$("#obat_jadi_satuan_pakai").val("");
	$("#obat_jadi_takaran_pakai").val("");
	$("#obat_jadi_pemakaian").val("");
	$("#obat_jadi_pemakaian_obat_luar").val("");
	$("#obat_jadi_penggunaan").val("");
	$("#obat_jadi_detail_penggunaan").val("");
	$(".obat_jadi_detail_penggunaan").hide();
	$("#obat_jadi_save").removeAttr("onclick");
	$("#obat_jadi_save").attr("onclick", "dresep.save_obat_jadi()");
	$("#modal_alert_obat_jadi_add_form").html("");
	$(".error_field").removeClass("error_field");
	$("#obat_jadi_add_form").smodal("show");
	$(".btn").removeAttr("disabled");
};
DResepAction.prototype.validate_obat_jadi = function() {
	var valid = true;
	var invalid_msg = "";
	var nama_obat = $("#obat_jadi_name_obat").val();
	var satuan = $("#obat_jadi_satuan").val();
	var stok = $("#obat_jadi_stok").val();
	var jumlah_lama = $("#obat_jadi_jumlah_lama").val();
	var jumlah = $("#obat_jadi_jumlah").val();
	var harga = $("#obat_jadi_hna").val();
	$(".error_field").removeClass("error_field");
	if (nama_obat == "") {
		valid = false;
		invalid_msg += "</br><strong>Nama Obat</strong> tidak boleh kosong";
		$("#obat_jadi_nama_obat").addClass("error_field");
		$("#obat_jadi_nama_obat").focus();
	}
	if (satuan == "") {
		valid = false;
		invalid_msg += "</br><strong>Satuan</strong> tidak boleh kosong";
		$("#obat_jadi_satuan").addClass("error_field");
		$("#obat_jadi_satuan").focus();
	}
	if (jumlah == "") {
		valid = false;
		invalid_msg += "</br><strong>Jumlah</strong> tidak boleh kosong";
		$("#obat_jadi_jumlah").addClass("error_field");
		$("#obat_jadi_jumlah").focus();
	} else if (!is_numeric(jumlah)) {
		valid = false;
		invalid_msg += "</br><strong>Jumlah</strong> seharusnya numerik (0-9)";
		$("#obat_jadi_jumlah").addClass("error_field");
		$("#obat_jadi_jumlah").focus();
	} else if (stok != "" && is_numeric(stok) && (parseFloat(jumlah) - parseFloat(jumlah_lama)) > parseFloat(stok)) {
		valid = false;
		invalid_msg += "</br><strong>Jumlah</strong> tidak boleh melebihi stok";
		$("#obat_jadi_jumlah").addClass("error_field");
		$("#obat_jadi_jumlah").focus();
	}
	if (harga == 0 || harga == "") {
		valid = false;
		invalid_msg += "</br><strong>Harga Netto</strong> tidak boleh kosong";
		$("#obat_jadi_hna").addClass("error_field");
		$("#obat_jadi_hna").focus();
	}
	if (!valid) {
		$("#modal_alert_obat_jadi_add_form").html(
			"<div class='alert alert-block alert-danger'>" +
				"<h4>Peringatan</h4>" +
				invalid_msg +
			"</div>"
		);
	}
	return valid;
};
DResepAction.prototype.save_obat_jadi = function() {
	if ($(".btn").attr("disabled") == "disabled")
		return;
	$(".btn").removeAttr("disabled");
	$(".btn").attr("disabled", "disabled");
	if (!this.validate_obat_jadi()) {
		$(".btn").removeAttr("disabled");
		return;
	}
	var dresep_id = $("#obat_jadi_id").val();
	var dresep_id_obat = $("#obat_jadi_id_obat").val();
	var dresep_kode_obat = $("#obat_jadi_kode_obat").val();
	var dresep_nama_obat = $("#obat_jadi_name_obat").val();
	var dresep_nama_jenis_obat = $("#obat_jadi_nama_jenis_obat").val();
	var dresep_jumlah_lama = $("#obat_jadi_jumlah_lama").val();
	var dresep_jumlah = $("#obat_jadi_jumlah").val();
	var dresep_satuan = $("#obat_jadi_satuan").find(":selected").text();
	var dresep_konversi = $("#obat_jadi_konversi").val();
	var dresep_satuan_konversi = $("#obat_jadi_satuan_konversi").val();
	var dresep_hna = $("#obat_jadi_hna").val();
	var dresep_embalase = $("#obat_jadi_embalase").val();
	var dresep_tuslah = $("#obat_jadi_tuslah").val();
	var dresep_markup = $("#obat_jadi_markup").val();
	var dresep_aturan_pakai = $("#obat_jadi_aturan_pakai").val();
	var dresep_obat_luar = $("#obat_jadi_obat_luar").val();
	var dresep_tanggal_exp = $("#obat_jadi_tanggal_exp").val();
	var dresep_jumlah_pakai_sehari = $("#obat_jadi_jumlah_pakai_sehari").val();
	var dresep_satuan_pakai = $("#obat_jadi_satuan_pakai").val();
	var dresep_takaran_pakai = $("#obat_jadi_takaran_pakai").val();
	var dresep_pemakaian = $("#obat_jadi_pemakaian").val();
	var dresep_pemakaian_obat_luar = $("#obat_jadi_pemakaian_obat_luar").val();
	var dresep_penggunaan = $("#obat_jadi_penggunaan").val();
	var dresep_detail_penggunaan = $("#obat_jadi_detail_penggunaan").val();
	$("tbody#dresep_list").append(
		"<tr id='dresep_" + dresep_num + "'>" +
			"<td id='dresep_" + dresep_num + "_id' style='display: none;'>" + dresep_id + "</td>" +
			"<td id='dresep_" + dresep_num + "_tipe' style='display: none;'>obat_jadi</td>" +
			"<td id='dresep_" + dresep_num + "_id_obat' style='display: none;'>" + dresep_id_obat + "</td>" +
			"<td id='dresep_" + dresep_num + "_kode_obat' style='display: none;'>" + dresep_kode_obat + "</td>" +
			"<td id='dresep_" + dresep_num + "_nama_jenis_obat' style='display: none;'>" + dresep_nama_jenis_obat + "</td>" +
			"<td id='dresep_" + dresep_num + "_jumlah' style='display: none;'>" + dresep_jumlah + "</td>" +
			"<td id='dresep_" + dresep_num + "_jumlah_lama' style='display: none;'>" + dresep_jumlah_lama + "</td>" +
			"<td id='dresep_" + dresep_num + "_satuan' style='display: none;'>" + dresep_satuan + "</td>" +
			"<td id='dresep_" + dresep_num + "_konversi' style='display: none;'>" + dresep_konversi + "</td>" +
			"<td id='dresep_" + dresep_num + "_satuan_konversi' style='display: none;'>" + dresep_satuan_konversi + "</td>" +
			"<td id='dresep_" + dresep_num + "_hna' style='display: none;'>" + dresep_hna + "</td>" +
			"<td id='dresep_" + dresep_num + "_markup' style='display: none;'>" + dresep_markup + "</td>" +
			"<td id='dresep_" + dresep_num + "_tanggal_exp' style='display: none;'>" + dresep_tanggal_exp + "</td>" +
			"<td id='dresep_" + dresep_num + "_obat_luar' style='display: none;'>" + dresep_obat_luar + "</td>" +
			"<td id='dresep_" + dresep_num + "_jumlah_pakai_sehari' style='display: none;'>" + dresep_jumlah_pakai_sehari + "</td>" +
			"<td id='dresep_" + dresep_num + "_satuan_pakai' style='display: none;'>" + dresep_satuan_pakai + "</td>" +
			"<td id='dresep_" + dresep_num + "_takaran_pakai' style='display: none;'>" + dresep_takaran_pakai + "</td>" +
			"<td id='dresep_" + dresep_num + "_pemakaian' style='display: none;'>" + dresep_pemakaian + "</td>" +
			"<td id='dresep_" + dresep_num + "_pemakaian_obat_luar' style='display: none;'>" + dresep_pemakaian_obat_luar + "</td>" +
			"<td id='dresep_" + dresep_num + "_penggunaan' style='display: none;'>" + dresep_penggunaan + "</td>" +
			"<td id='dresep_" + dresep_num + "_detail_penggunaan' style='display: none;'>" + dresep_detail_penggunaan + "</td>" +
			"<td id='dresep_" + dresep_num + "_nomor'></td>" +
			"<td id='dresep_" + dresep_num + "_nama_obat'>" + dresep_nama_obat + "</td>" +
			"<td id='dresep_" + dresep_num + "_f_jumlah'>" + dresep_jumlah + " " + dresep_satuan + "</td>" +
			"<td id='dresep_" + dresep_num + "_harga'></td>" +
			"<td id='dresep_" + dresep_num + "_embalase'>" + dresep_embalase + "</td>" +
			"<td id='dresep_" + dresep_num + "_tuslah'>" + dresep_tuslah + "</td>" +
			"<td id='dresep_" + dresep_num + "_biaya_racik'>Rp. 0,00</td>" +
			"<td id='dresep_" + dresep_num + "_subtotal'></td>" +
			"<td id='dresep_" + dresep_num + "_aturan_pakai'>" + dresep_aturan_pakai + "</td>" +
			"<td id='dresep_" + dresep_num + "_apoteker'>-</td>" +
			"<td>" +
				"<div class='btn-group noprint'>" +
					"<a href='#' onclick='dresep.edit_obat_jadi(" + dresep_num + ")' data-content='Ubah' data-toggle='popover' class='input btn btn-warning'>" + 
						"<i class='icon-edit icon-white'></i>" +
					"</a>" +
					"<a href='#' onclick='dresep.delete(" + dresep_num + ")' data-content='Hapus' data-toggle='popover' class='input btn btn-danger'>" + 
						"<i class='icon-remove icon-white'></i>" + 
					"</a>" +
				"</div>" +
			"</td>" +
		"</tr>"
	);
	dresep_num++;
	$("#obat_jadi_id").val("");
	$("#obat_jadi_id_obat").val("");
	$("#obat_jadi_kode_obat").val("");
	$("#obat_jadi_name_obat").val("");
	$("#obat_jadi_nama_obat").val("");
	$("#obat_jadi_nama_jenis_obat").val("");
	$("#obat_jadi_satuan").removeAttr("onchange");
	$("#obat_jadi_satuan").attr("onchange", "obat.setDetailInfo()");
	$("#obat_jadi_satuan").html("");
	$("#obat_jadi_stok").val("");
	$("#obat_jadi_f_stok").val("");
	$("#obat_jadi_jumlah_lama").val(0);
	$("#obat_jadi_hna").val("");
	$("#obat_jadi_markup").val(0);
	$("#obat_jadi_jumlah").val("");
	$("#obat_jadi_aturan_pakai").val("-");
	$("#obat_jadi_tanggal_exp").val("");
	$("#obat_jadi_obat_luar").val(0);
	$("#obat_jadi_obat_luar").trigger("change");
	$("#obat_jadi_satuan_pakai").val("");
	$("#obat_jadi_takaran_pakai").val("");
	$("#obat_jadi_pemakaian").val("");
	$("#obat_jadi_pemakaian_obat_luar").val("");
	$("#obat_jadi_penggunaan").val("");
	$("#obat_jadi_detail_penggunaan").val("");
	$(".obat_jadi_detail_penggunaan").hide();
	$("#obat_jadi_save").removeAttr("onclick");
	$("#obat_jadi_save").attr("onclick", "dresep.save_obat_jadi()");
	$("#modal_alert_obat_jadi_add_form").html("");
	$(".error_field").removeClass("error_field");
	$("#obat_jadi_nama_obat").focus();
	resep.refreshHargaAndSubtotal();
	resep.refresh_no_dresep();
	$(".btn").removeAttr("disabled");
};
DResepAction.prototype.edit_obat_jadi = function(r_num) {
	if ($(".btn").attr("disabled") == "disabled")
		return;
	$(".btn").removeAttr("disabled");
	$(".btn").attr("disabled", "disabled");
	var dresep_id = $("#dresep_" + r_num + "_id").text();
	var dresep_id_obat = $("#dresep_" + r_num + "_id_obat").text();
	var dresep_kode_obat = $("#dresep_" + r_num + "_kode_obat").text();
	var dresep_nama_obat = $("#dresep_" + r_num + "_nama_obat").text();
	var dresep_nama_jenis_obat = $("#dresep_" + r_num + "_nama_jenis_obat").text();
	var dresep_jumlah_lama = $("#dresep_" + r_num + "_jumlah_lama").text();
	var dresep_jumlah = $("#dresep_" + r_num + "_jumlah").text();
	var dresep_satuan = $("#dresep_" + r_num + "_satuan").text();
	var dresep_konversi = $("#dresep_" + r_num + "_konversi").text();
	var dresep_satuan_konversi = $("#dresep_" + r_num + "_satuan_konversi").text();
	var dresep_hna = $("#dresep_" + r_num + "_hna").text();
	var dresep_embalase = $("#dresep_" + r_num + "_embalase").text();
	var dresep_tuslah = $("#dresep_" + r_num + "_tuslah").text();
	var dresep_markup = $("#dresep_" + r_num + "_markup").text();
	var dresep_aturan_pakai = $("#dresep_" + r_num + "_aturan_pakai").text();
	var dresep_tanggal_exp = $("#dresep_" + r_num + "_tanggal_exp").text();
	var dresep_obat_luar = $("#dresep_" + r_num + "_obat_luar").text();
	var dresep_jumlah_pakai_sehari = $("#dresep_" + r_num + "_jumlah_pakai_sehari").text();
	var dresep_satuan_pakai = $("#dresep_" + r_num + "_satuan_pakai").text();
	var dresep_takaran_pakai = $("#dresep_" + r_num + "_takaran_pakai").text();
	var dresep_pemakaian = $("#dresep_" + r_num + "_pemakaian").text();
	var dresep_pemakaian_obat_luar = $("#dresep_" + r_num + "_pemakaian_obat_luar").text();
	var dresep_penggunaan = $("#dresep_" + r_num + "_penggunaan").text();
	var dresep_detail_penggunaan = $("#dresep_" + r_num + "_detail_penggunaan").text();
	$("#obat_jadi_id").val(dresep_id);
	$("#obat_jadi_jumlah_lama").val(dresep_jumlah_lama);
	$("#obat_jadi_jumlah").val(dresep_jumlah);
	$("#obat_jadi_markup").val(dresep_markup);
	$("#obat_jadi_aturan_pakai").val(dresep_aturan_pakai);
	$("#obat_jadi_tanggal_exp").val(dresep_tanggal_exp);
	$("#obat_jadi_obat_luar").val(dresep_obat_luar);
	$("#obat_jadi_jumlah_pakai_sehari").val(dresep_jumlah_pakai_sehari);
	$("#obat_jadi_satuan_pakai").val(dresep_satuan_pakai);
	$("#obat_jadi_takaran_pakai").val(dresep_takaran_pakai);
	$("#obat_jadi_pemakaian").val(dresep_pemakaian);
	$("#obat_jadi_pemakaian_obat_luar").val(dresep_pemakaian_obat_luar);
	$("#obat_jadi_obat_luar").trigger("change");
	$("#obat_jadi_penggunaan").val(dresep_penggunaan);
	$("#obat_jadi_penggunaan").trigger("change");
	$("#obat_jadi_detail_penggunaan").val(dresep_detail_penggunaan);
	var self = this;
	var data = this.getRegulerData();
	data['super_command'] = "obat";
	data['command'] = "edit";
	data['id'] = dresep_id_obat;
	$.post(
		"",
		data,
		function(response) {
			var json = getContent(response);
			if (json == null) return;
			$("#obat_jadi_id_obat").val(json.header.id_obat);
			$("#obat_jadi_kode_obat").val(json.header.kode_obat);
			$("#obat_jadi_name_obat").val(json.header.nama_obat);
			$("#obat_jadi_nama_obat").val(json.header.nama_obat);
			$("#obat_jadi_nama_jenis_obat").val(json.header.nama_jenis_obat);
			$("#obat_jadi_satuan").html(json.satuan_option);
			$("#obat_jadi_satuan").val(dresep_konversi + "_" + dresep_satuan_konversi);
			$("#obat_jadi_embalase").val(dresep_embalase);
			$("#obat_jadi_tuslah").val(dresep_tuslah);
			var part = $("#obat_jadi_satuan").val().split("_");
			$("#obat_jadi_konversi").val(part[0]);
			$("#obat_jadi_satuan_konversi").val(part[1]);
			data = self.getRegulerData();
			data['super_command'] = "sisa";
			data['command'] = "edit";
			data['id_obat'] = $("#obat_jadi_id_obat").val();
			data['satuan'] = $("#obat_jadi_satuan").find(":selected").text();
			data['konversi'] = $("#obat_jadi_konversi").val();
			data['satuan_konversi'] = $("#obat_jadi_satuan_konversi").val();
			if ($("#resep_jenis").length)
				data['jenis_pasien'] = $("#resep_jenis").val();
			if ($("#resep_asuransi").length)
				data['asuransi'] = $("#resep_asuransi").val();
			if ($("#resep_perusahaan").length)
				data['perusahaan'] = $("#resep_perusahaan").val();
			if ($("#resep_uri").length)
				data['uri'] = $("#resep_uri").val();
			$.post(
				"",
				data,
				function(response) {
					var json = getContent(response);
					if (json == null) return;
					$("#obat_jadi_stok").val(json.sisa);
					$("#obat_jadi_f_stok").val(json.sisa + " " + json.satuan);						
					var hna = parseFloat(json.hna) * (1 + parseFloat(json.markup) / 100);
					hna = "Rp. " + (parseFloat(hna)).formatMoney("2", ".", ",");
					$("#obat_jadi_hna").val(hna);
					$("#obat_jadi_markup").val(json.markup);
					$("#obat_jadi_satuan").removeAttr("onchange");
					$("#obat_jadi_satuan").attr("onchange", "obat.setDetailInfo()");
					$("#modal_alert_obat_jadi_add_form").html("");
					$(".error_field").removeClass("error_field");
					$("#obat_jadi_save").removeAttr("onclick");
					$("#obat_jadi_save").attr("onclick", "dresep.update_obat_jadi(" + r_num + ")");
					$("#obat_jadi_add_form").smodal("show");
					$(".btn").removeAttr("disabled");
				}
			);
		}
	);
};
DResepAction.prototype.update_obat_jadi = function(r_num) {
	if ($(".btn").attr("disabled") == "disabled")
		return;
	$(".btn").removeAttr("disabled");
	$(".btn").attr("disabled", "disabled");
	if (!this.validate_obat_jadi()) {
		$(".btn").removeAttr("disabled");		
		return;
	}
	var dresep_id_obat = $("#obat_jadi_id_obat").val();
	var dresep_kode_obat = $("#obat_jadi_kode_obat").val();
	var dresep_nama_obat = $("#obat_jadi_name_obat").val();
	var dresep_nama_jenis_obat = $("#obat_jadi_nama_jenis_obat").val();
	var dresep_jumlah = $("#obat_jadi_jumlah").val();
	var dresep_satuan = $("#obat_jadi_satuan").find(":selected").text();
	var dresep_konversi = $("#obat_jadi_konversi").val();
	var dresep_satuan_konversi = $("#obat_jadi_satuan_konversi").val();
	var dresep_hna = $("#obat_jadi_hna").val();
	var dresep_embalase = $("#obat_jadi_embalase").val();
	var dresep_tuslah = $("#obat_jadi_tuslah").val();
	var dresep_markup = $("#obat_jadi_markup").val();
	var dresep_aturan_pakai = $("#obat_jadi_aturan_pakai").val();
	var dresep_tanggal_exp = $("#obat_jadi_tanggal_exp").val();
	var dresep_obat_luar = $("#obat_jadi_obat_luar").val();
	var dresep_jumlah_pakai_sehari = $("#obat_jadi_jumlah_pakai_sehari").val();
	var dresep_satuan_pakai = $("#obat_jadi_satuan_pakai").val();
	var dresep_takaran_pakai = $("#obat_jadi_takaran_pakai").val();
	var dresep_pemakaian = $("#obat_jadi_pemakaian").val();
	var dresep_pemakaian_obat_luar = $("#obat_jadi_pemakaian_obat_luar").val();
	var dresep_penggunaan = $("#obat_jadi_penggunaan").val();
	var dresep_detail_penggunaan = $("#obat_jadi_detail_penggunaan").val();
	$("#dresep_" + r_num + "_id_obat").text(dresep_id_obat);
	$("#dresep_" + r_num + "_kode_obat").text(dresep_kode_obat);
	$("#dresep_" + r_num + "_nama_obat").text(dresep_nama_obat);
	$("#dresep_" + r_num + "_nama_jenis_obat").text(dresep_nama_jenis_obat);
	$("#dresep_" + r_num + "_jumlah").text(dresep_jumlah);
	$("#dresep_" + r_num + "_satuan").text(dresep_satuan);
	$("#dresep_" + r_num + "_konversi").text(dresep_konversi);
	$("#dresep_" + r_num + "_satuan_konversi").text(dresep_satuan_konversi);
	$("#dresep_" + r_num + "_hna").text(dresep_hna);
	$("#dresep_" + r_num + "_embalase").text(dresep_embalase);
	$("#dresep_" + r_num + "_tuslah").text(dresep_tuslah);
	$("#dresep_" + r_num + "_markup").text(dresep_markup);
	$("#dresep_" + r_num + "_f_jumlah").text(dresep_jumlah + " " + dresep_satuan);
	$("#dresep_" + r_num + "_aturan_pakai").text(dresep_aturan_pakai);
	$("#dresep_" + r_num + "_tanggal_exp").text(dresep_tanggal_exp);
	$("#dresep_" + r_num + "_obat_luar").text(dresep_obat_luar);
	$("#dresep_" + r_num + "_jumlah_pakai_sehari").text(dresep_jumlah_pakai_sehari);
	$("#dresep_" + r_num + "_satuan_pakai").text(dresep_satuan_pakai);
	$("#dresep_" + r_num + "_takaran_pakai").text(dresep_takaran_pakai);
	$("#dresep_" + r_num + "_pemakaian").text(dresep_pemakaian);
	$("#dresep_" + r_num + "_pemakaian_obat_luar").text(dresep_pemakaian_obat_luar);
	$("#dresep_" + r_num + "_penggunaan").text(dresep_penggunaan);
	$("#dresep_" + r_num + "_detail_penggunaan").text(dresep_detail_penggunaan);
	resep.refreshHargaAndSubtotal();
	$("#obat_jadi_add_form").smodal("hide");
	$(".btn").removeAttr("disabled");
};
DResepAction.prototype.show_add_obat_racikan_form = function() {
	if ($(".btn").attr("disabled") == "disabled")
		return;
	$(".btn").removeAttr("disabled");
	$(".btn").attr("disabled", "disabled");
	$("#obat_racikan_nama").val("");
	$("#obat_racikan_nama").removeAttr("disabled");
	$("#obat_racikan_id_apoteker").val("");
	$("#obat_racikan_name_apoteker").val("");
	$("#obat_racikan_nama_apoteker").val("");
	$("#obat_racikan_tanggal_exp").val("");
	$("#obat_racikan_tanggal_exp").removeAttr("disabled");
	$("#obat_racikan_obat_luar").val(0);
	$("#obat_racikan_obat_luar").removeAttr("disabled");
	$("#obat_racikan_obat_luar").trigger("change");
	$("#obat_racikan_jumlah_pakai_sehari").val("");
	$("#obat_racikan_jumlah_pakai_sehari").removeAttr("disabled");
	$("#obat_racikan_satuan_pakai").val("");
	$("#obat_racikan_satuan_pakai").removeAttr("disabled");
	$("#obat_racikan_takaran_pakai").val("");
	$("#obat_racikan_takaran_pakai").removeAttr("disabled");
	$("#obat_racikan_pemakaian").val("");
	$("#obat_racikan_pemakaian").removeAttr("disabled");
	$("#obat_racikan_pemakaian_obat_luar").val("");
	$("#obat_racikan_pemakaian_obat_luar").removeAttr("disabled");
	$("#obat_racikan_penggunaan").val("");
	$("#obat_racikan_penggunaan").removeAttr("disabled");
	$("#obat_racikan_detail_penggunaan").val("");
	$(".obat_racikan_detail_penggunaan").hide();
	$("#obat_racikan_detail_penggunaan").removeAttr("disabled");
	$("#apoteker_browse").removeAttr("onclick");
	$("#apoteker_browse").attr("onclick", "apoteker.chooser('apoteker', 'apoteker_btn', 'apoteker', apoteker)");
	$("#apoteker_browse").removeClass("btn-info");
	$("#apoteker_browse").removeClass("btn-inverse");
	$("#apoteker_browse").addClass("btn-info");
	$("#obat_racikan_aturan_pakai").val("");
	$("#obat_racikan_aturan_pakai").removeAttr("disabled");
	$("#obat_racikan_biaya_racik").val("Rp. " + parseFloat(jasa_racik).formatMoney("2", ".", ","));
	$("#bahan_add").show();
	dracikan_num = 0;
	$("tbody#dracikan_list").children("tr").remove();
	$("#modal_alert_obat_racikan_add_form").html("");
	$(".error_field").removeClass("error_field");
	$("#racikan_save").removeAttr("onclick");
	$("#racikan_save").attr("onclick", "dresep.save_obat_racikan()");
	$("#racikan_save").show();
	$("#racikan_ok").hide();
	$("#modal_alert_obat_racikan_add_form").html("");
	$(".error_field").removeClass("error_field");
	$("#obat_racikan_add_form").smodal("show");
	$(".btn").removeAttr("disabled");
};
DResepAction.prototype.validate_obat_racikan = function() {
	var valid = true;
	var invalid_msg = "";
	var nama_racikan = $("#obat_racikan_nama").val();
	var nama_apoteker = $("#obat_racikan_name_apoteker").val();
	var nord = $("tbody#dracikan_list").children("tr").length;
	$(".error_field").removeClass("error_field");
	if (nama_racikan == "") {
		valid = false;
		invalid_msg += "</br><strong>Nama Racikan</strong> tidak boleh kosong";
		$("#obat_racikan_nama").addClass("error_field");
	}
	if (nama_apoteker == "") {
		valid = false;
		invalid_msg += "</br><strong>Nama Apoteker</strong> tidak boleh kosong";
		$("#obat_racikan_nama_apoteker").addClass("error_field");
	}
	if (nord == 0) {
		valid = false;
		invalid_msg += "</br><strong>Detail Racikan</strong> tidak boleh kosong";
	}
	if (!valid) {
		$("#modal_alert_obat_racikan_add_form").html(
			"<div class='alert alert-block alert-danger'>" +
				"<h4>Peringatan</h4>" +
				invalid_msg +
			"</div>"
		);
	}
	return valid;
};
DResepAction.prototype.save_obat_racikan = function() {
	if ($(".btn").attr("disabled") == "disabled")
		return;
	$(".btn").removeAttr("disabled");
	$(".btn").attr("disabled", "disabled");
	if (!this.validate_obat_racikan()) {
		$(".btn").removeAttr("disabled");
		return;
	}
	var racikan_id = $("#obat_racikan_id").val();
	var racikan_nama = $("#obat_racikan_nama").val();
	var racikan_aturan_pakai = $("#obat_racikan_aturan_pakai").val();
	var racikan_id_apoteker = $("#obat_racikan_id_apoteker").val();
	var racikan_nama_apoteker = $("#obat_racikan_name_apoteker").val();
	var racikan_tanggal_exp = $("#obat_racikan_tanggal_exp").val();
	var racikan_obat_luar = $("#obat_racikan_obat_luar").val();
	var racikan_jumlah_pakai_sehari = $("#obat_racikan_jumlah_pakai_sehari").val();
	var racikan_satuan_pakai = $("#obat_racikan_satuan_pakai").val();
	var racikan_takaran_pakai = $("#obat_racikan_takaran_pakai").val();
	var racikan_pemakaian = $("#obat_racikan_pemakaian").val();
	var racikan_pemakaian_obat_luar = $("#obat_racikan_pemakaian_obat_luar").val();
	var racikan_penggunaan = $("#obat_racikan_penggunaan").val();
	var racikan_detail_penggunaan = $("#obat_racikan_detail_penggunaan").val();
	var racikan_biaya_racik = parseFloat($("#obat_racikan_biaya_racik").val().replace(/[^0-9-,]/g, '').replace(",", "."));
	var racikan_harga = 0;
	var racikan_embalase = 0;
	var racikan_tuslah = 0;
	for(var i = 0; i < dracikan_num; i++) {
		var nama_bahan = $("#racikan_" + racikan_num + "_" + i + "_nama_bahan").text();
		var was_deleted = false;
		if ($("#dracikan_list tr:eq(" + i + ")").hasClass("deleted")) {
			was_deleted = true;
		}
		if (nama_bahan.length != 0 && was_deleted == false) {
			$("tbody#bahan_racikan_list").append(
				"<tr id=bahan_" + bahan_num + ">" +
					"<td style='display: none;' id='bahan_" + bahan_num + "_id'>" + $("#racikan_" + racikan_num + "_" + i + "_id").text() + "</td>" +
					"<td style='display: none;' id='bahan_" + bahan_num + "_id_bahan'>" + $("#racikan_" + racikan_num + "_" + i + "_id_bahan").text() + "</td>" +
					"<td style='display: none;' id='bahan_" + bahan_num + "_kode'>" + $("#racikan_" + racikan_num + "_" + i + "_kode_bahan").text() + "</td>" +
					"<td style='display: none;' id='bahan_" + bahan_num + "_nama'>" + $("#racikan_" + racikan_num + "_" + i + "_nama_bahan").text() + "</td>" +
					"<td style='display: none;' id='bahan_" + bahan_num + "_nama_jenis_bahan'>" + $("#racikan_" + racikan_num + "_" + i + "_nama_jenis_bahan").text() + "</td>" +
					"<td style='display: none;' id='bahan_" + bahan_num + "_jumlah'>" + $("#racikan_" + racikan_num + "_" + i + "_jumlah").text() + "</td>" +
					"<td style='display: none;' id='bahan_" + bahan_num + "_jumlah_lama'>0</td>" +
					"<td style='display: none;' id='bahan_" + bahan_num + "_satuan'>" + $("#racikan_" + racikan_num + "_" + i + "_satuan").text() + "</td>" +
					"<td style='display: none;' id='bahan_" + bahan_num + "_konversi'>" + $("#racikan_" + racikan_num + "_" + i + "_konversi").text() + "</td>" +
					"<td style='display: none;' id='bahan_" + bahan_num + "_satuan_konversi'>" + $("#racikan_" + racikan_num + "_" + i + "_satuan_konversi").text() + "</td>" +
					"<td style='display: none;' id='bahan_" + bahan_num + "_harga'>" + $("#racikan_" + racikan_num + "_" + i + "_harga").text() + "</td>" +
					"<td style='display: none;' id='bahan_" + bahan_num + "_embalase'>" + $("#racikan_" + racikan_num + "_" + i + "_embalase").text() + "</td>" +
					"<td style='display: none;' id='bahan_" + bahan_num + "_tuslah'>" + $("#racikan_" + racikan_num + "_" + i + "_tuslah").text() + "</td>" +
					"<td style='display: none;' id='bahan_" + bahan_num + "_markup'>" + $("#racikan_" + racikan_num + "_" + i + "_markup").text() + "</td>" +
					"<td style='display: none;' id='bahan_" + bahan_num + "_label'>racikan_" + racikan_num + "</td>" +
				"</tr>"
			);
			var dracikan_harga = $("#racikan_" + racikan_num + "_" + i + "_harga").text().replace(/[^0-9-,]/g, '').replace(",", ".");
			var dracikan_jumlah = $("#racikan_" + racikan_num + "_" + i + "_jumlah").text();
			var dracikan_embalase = $("#racikan_" + racikan_num + "_" + i + "_embalase").text().replace(/[^0-9-,]/g, '').replace(",", ".");
			racikan_embalase += parseFloat(dracikan_embalase);
			var dracikan_tuslah = $("#racikan_" + racikan_num + "_" + i + "_tuslah").text().replace(/[^0-9-,]/g, '').replace(",", ".");
			racikan_tuslah += parseFloat(dracikan_tuslah);
			racikan_harga += parseFloat(dracikan_harga) * parseFloat(dracikan_jumlah);
			bahan_num++;
		}
	}
	var subtotal = racikan_harga + racikan_embalase + racikan_tuslah + racikan_biaya_racik;
	var f_harga = "Rp. " + (parseFloat(racikan_harga)).formatMoney("2", ".", ",");
	var f_embalase = "Rp. " + (parseFloat(racikan_embalase).formatMoney("2", ".", ","));
	var f_tuslah = "Rp. " + (parseFloat(racikan_tuslah).formatMoney("2", ".", ","));
	var f_biaya_racik = $("#obat_racikan_biaya_racik").val();
	var f_subtotal = "Rp. " + (parseFloat(subtotal)).formatMoney("2", ".", ",");
	$("tbody#dresep_list").append(
		"<tr id='dresep_" + dresep_num + "'>" +
			"<td id='dresep_" + dresep_num + "_id' style='display: none;'>" + racikan_id + "</td>" +
			"<td id='dresep_" + dresep_num + "_tipe' style='display: none;'>obat_racikan</td>" +
			"<td id='dresep_" + dresep_num + "_label' style='display: none;'>racikan_" + racikan_num + "</td>" +
			"<td id='dresep_" + dresep_num + "_id_apoteker' style='display: none;'>" + racikan_id_apoteker + "</td>" +
			"<td id='dresep_" + dresep_num + "_jumlah' style='display: none;'>1</td>" +
			"<td id='dresep_" + dresep_num + "_jumlah' style='display: none;'>1</td>" +
			"<td id='dresep_" + dresep_num + "_hna' style='display: none;'>" + f_harga + "</td>" +
			"<td id='dresep_" + dresep_num + "_tanggal_exp' style='display: none;'>" + racikan_tanggal_exp + "</td>" +
			"<td id='dresep_" + dresep_num + "_obat_luar' style='display: none;'>" + racikan_obat_luar + "</td>" +
			"<td id='dresep_" + dresep_num + "_jumlah_pakai_sehari' style='display: none;'>" + racikan_jumlah_pakai_sehari + "</td>" +
			"<td id='dresep_" + dresep_num + "_satuan_pakai' style='display: none;'>" + racikan_satuan_pakai + "</td>" +
			"<td id='dresep_" + dresep_num + "_takaran_pakai' style='display: none;'>" + racikan_takaran_pakai + "</td>" +
			"<td id='dresep_" + dresep_num + "_pemakaian' style='display: none;'>" + racikan_pemakaian + "</td>" +
			"<td id='dresep_" + dresep_num + "_pemakaian_obat_luar' style='display: none;'>" + racikan_pemakaian_obat_luar + "</td>" +
			"<td id='dresep_" + dresep_num + "_penggunaan' style='display: none;'>" + racikan_penggunaan + "</td>" +
			"<td id='dresep_" + dresep_num + "_detail_penggunaan' style='display: none;'>" + racikan_detail_penggunaan + "</td>" +
			"<td id='dresep_" + dresep_num + "_nomor'></td>" +
			"<td id='dresep_" + dresep_num + "_nama_racikan'>" + racikan_nama + "</td>" +
			"<td id='dresep_" + dresep_num + "_f_jumlah'>1 Racikan</td>" +
			"<td id='dresep_" + dresep_num + "_harga'></td>" +
			"<td id='dresep_" + dresep_num + "_embalase'>" + f_embalase + "</td>" +
			"<td id='dresep_" + dresep_num + "_tuslah'>" + f_tuslah + "</td>" +
			"<td id='dresep_" + dresep_num + "_biaya_racik'>" + f_biaya_racik + "</td>" +
			"<td id='dresep_" + dresep_num + "_subtotal'></td>" +
			"<td id='dresep_" + dresep_num + "_aturan_pakai'>" + racikan_aturan_pakai + "</td>" +
			"<td id='dresep_" + dresep_num + "_apoteker'>" + racikan_nama_apoteker + "</td>" +
			"<td>" +
				"<div class='btn-group noprint'>" +
					"<a href='#' onclick='dresep.edit_obat_racikan(" + dresep_num + "," + racikan_num + ")' data-content='Ubah' data-toggle='popover' class='input btn btn-warning'>" + 
						"<i class='icon-edit icon-white'></i>" +
					"</a>" +
					"<a href='#' onclick='dresep.delete(" + dresep_num + ")' data-content='Hapus' data-toggle='popover' class='input btn btn-danger'>" + 
						"<i class='icon-remove icon-white'></i>" + 
					"</a>" +
				"</div>" +
			"</td>" +
		"</tr>"
	);
	dresep_num++;
	racikan_num++;
	resep.refreshHargaAndSubtotal();
	$("#obat_racikan_add_form").smodal("hide");
	resep.refresh_no_dresep();
	$(".btn").removeAttr("disabled");
};
DResepAction.prototype.edit_obat_racikan = function(dr_num, r_num) {
	if ($(".btn").attr("disabled") == "disabled")
		return;
	$(".btn").removeAttr("disabled");
	$(".btn").attr("disabled", "disabled");
	var racikan_id = $("#dresep_" + dr_num + "_id").text();
	var racikan_nama = $("#dresep_" + dr_num + "_nama_racikan").text();
	var racikan_id_apoteker = $("#dresep_" + dr_num + "_id_apoteker").text();
	var racikan_nama_apoteker = $("#dresep_" + dr_num + "_apoteker").text();
	var racikan_aturan_pakai = $("#dresep_" + dr_num + "_aturan_pakai").text();
	var racikan_biaya_racik = $("#dresep_" + dr_num + "_biaya_racik").text();
	var racikan_tanggal_exp = $("#dresep_" + dr_num + "_tanggal_exp").text();
	var racikan_obat_luar = $("#dresep_" + dr_num + "_obat_luar").text();
	var racikan_jumlah_pakai_sehari = $("#dresep_" + dr_num + "_jumlah_pakai_sehari").text();
	var racikan_satuan_pakai = $("#dresep_" + dr_num + "_satuan_pakai").text();
	var racikan_takaran_pakai = $("#dresep_" + dr_num + "_takaran_pakai").text();
	var racikan_pemakaian = $("#dresep_" + dr_num + "_pemakaian").text();
	var racikan_pemakaian_obat_luar = $("#dresep_" + dr_num + "_pemakaian_obat_luar").text();
	var racikan_penggunaan = $("#dresep_" + dr_num + "_penggunaan").text();
	var racikan_detail_penggunaan = $("#dresep_" + dr_num + "_detail_penggunaan").text();
	$("#obat_racikan_id").val(racikan_id);
	$("#obat_racikan_nama").val(racikan_nama);
	$("#obat_racikan_nama").removeAttr("disabled");
	$("#obat_racikan_id_apoteker").val(racikan_id_apoteker);
	$("#obat_racikan_name_apoteker").val(racikan_nama_apoteker);
	$("#obat_racikan_nama_apoteker").val(racikan_nama_apoteker);
	$("#apoteker_browse").removeAttr("onclick");
	$("#apoteker_browse").attr("onclick", "apoteker.chooser('apoteker', 'apoteker_btn', 'apoteker', apoteker)");
	$("#apoteker_browse").removeClass("btn-info");
	$("#apoteker_browse").removeClass("btn-inverse");
	$("#apoteker_browse").addClass("btn-info");
	$("#obat_racikan_aturan_pakai").val(racikan_aturan_pakai);
	$("#obat_racikan_aturan_pakai").removeAttr("disabled");
	$("#obat_racikan_biaya_racik").val(racikan_biaya_racik);
	$("#obat_racikan_tanggal_exp").val(racikan_tanggal_exp);
	$("#obat_racikan_tanggal_exp").removeAttr("disabled");
	$("#obat_racikan_obat_luar").val(racikan_obat_luar);
	$("#obat_racikan_obat_luar").removeAttr("disabled");
	$("#obat_racikan_jumlah_pakai_sehari").val(racikan_jumlah_pakai_sehari);
	$("#obat_racikan_jumlah_pakai_sehari").removeAttr("disabled");
	$("#obat_racikan_satuan_pakai").val(racikan_satuan_pakai);
	$("#obat_racikan_satuan_pakai").removeAttr("disabled");
	$("#obat_racikan_takaran_pakai").val(racikan_takaran_pakai);
	$("#obat_racikan_takaran_pakai").removeAttr("disabled");
	$("#obat_racikan_pemakaian").val(racikan_pemakaian);
	$("#obat_racikan_pemakaian").removeAttr("disabled");
	$("#obat_racikan_pemakaian_obat_luar").val(racikan_pemakaian_obat_luar);
	$("#obat_racikan_pemakaian_obat_luar").removeAttr("disabled");
	$("#obat_racikan_penggunaan").val(racikan_penggunaan);
	$("#obat_racikan_penggunaan").removeAttr("disabled");
	$("#obat_racikan_detail_penggunaan").val(racikan_detail_penggunaan);
	$("#obat_racikan_detail_penggunaan").removeAttr("disabled");
	$("#obat_racikan_obat_luar").trigger("change");
	$("#obat_racikan_penggunaan").trigger("change");
	$("#bahan_add").show();
	dracikan_num = 0;
	$("#dracikan_list").children("tr").remove();
	$("#modal_alert_obat_racikan_add_form").html("");
	$(".error_field").removeClass("error_field");
	for(i = 0; i < bahan_num; i++) {
		var was_deleted = false;
		if ($("#bahan_" + i).hasClass("deleted")) {
			was_deleted = true;
		}
		var dracikan_id = $("#bahan_" + i + "_id").text();
		var dracikan_id_bahan = $("#bahan_" + i + "_id_bahan").text();
		var dracikan_kode_bahan = $("#bahan_" + i + "_kode").text();
		var dracikan_nama_bahan = $("#bahan_" + i + "_nama").text();
		var dracikan_nama_jenis_bahan = $("#bahan_" + i + "_nama_jenis_bahan").text();
		var dracikan_jumlah = $("#bahan_" + i + "_jumlah").text();
		var dracikan_satuan = $("#bahan_" + i + "_satuan").text();
		var dracikan_konversi = $("#bahan_" + i + "_konversi").text();
		var dracikan_satuan_konversi = $("#bahan_" + i + "_satuan_konversi").text();
		var dracikan_harga = $("#bahan_" + i + "_harga").text();
		var dracikan_embalase = $("#bahan_" + i + "_embalase").text();
		var dracikan_tuslah = $("#bahan_" + i + "_tuslah").text();
		var dracikan_markup = $("#bahan_" + i + "_markup").text();
		var v_harga = $("#bahan_" + i + "_harga").text().replace(/[^0-9-,]/g, '').replace(",", ".");
		var v_embalase = $("#bahan_" + i + "_embalase").text().replace(/[^0-9-,]/g, '').replace(",", ".");
		var v_tuslah = $("#bahan_" + i + "_tuslah").text().replace(/[^0-9-,]/g, '').replace(",", ".");
		var dracikan_subtotal = "Rp. " + (parseFloat(dracikan_jumlah) * parseFloat(v_harga) + parseFloat(v_embalase) + parseFloat(v_tuslah)).formatMoney("2", ".", ",");
		if ($("#bahan_" + i + "_label").text() == "racikan_" + r_num && was_deleted == false) {
			$("tbody#dracikan_list").append(
				"<tr id='racikan_" + r_num + "_" + dracikan_num + "'>" +
					"<td id='racikan_" + r_num + "_" + dracikan_num + "_id' style='display: none;'>" + dracikan_id + "</td>" +
					"<td id='racikan_" + r_num + "_" + dracikan_num + "_row_num' style='display: none;'>" + i + "</td>" +
					"<td id='racikan_" + r_num + "_" + dracikan_num + "_id_bahan' style='display: none;'>" + dracikan_id_bahan + "</td>" +
					"<td id='racikan_" + r_num + "_" + dracikan_num + "_kode_bahan' style='display: none;'>" + dracikan_kode_bahan + "</td>" +
					"<td id='racikan_" + r_num + "_" + dracikan_num + "_nama_jenis_bahan' style='display: none;'>" + dracikan_nama_jenis_bahan + "</td>" +
					"<td id='racikan_" + r_num + "_" + dracikan_num + "_jumlah' style='display: none;'>" + dracikan_jumlah + "</td>" +
					"<td id='racikan_" + r_num + "_" + dracikan_num + "_jumlah_lama' style='display: none;'>" + dracikan_jumlah + "</td>" +
					"<td id='racikan_" + r_num + "_" + dracikan_num + "_satuan' style='display: none;'>" + dracikan_satuan + "</td>" +
					"<td id='racikan_" + r_num + "_" + dracikan_num + "_konversi' style='display: none;'>" + dracikan_konversi + "</td>" +
					"<td id='racikan_" + r_num + "_" + dracikan_num + "_satuan_konversi' style='display: none;'>" + dracikan_satuan_konversi + "</td>" +
					"<td id='racikan_" + r_num + "_" + dracikan_num + "_markup' style='display: none;'>" + dracikan_markup + "</td>" +
					"<td id='racikan_" + r_num + "_" + dracikan_num + "_nama_bahan'>" + dracikan_nama_bahan + "</td>" +
					"<td id='racikan_" + r_num + "_" + dracikan_num + "_harga'>" + dracikan_harga + "</td>" +
					"<td id='racikan_" + r_num + "_" + dracikan_num + "_f_jumlah'>" + dracikan_jumlah + " " + dracikan_satuan + "</td>" +
					"<td id='racikan_" + r_num + "_" + dracikan_num + "_embalase'>" + dracikan_embalase + "</td>" +
					"<td id='racikan_" + r_num + "_" + dracikan_num + "_tuslah'>" + dracikan_tuslah + "</td>" +
					"<td id='racikan_" + r_num + "_" + dracikan_num + "_subtotal'>" + dracikan_subtotal + "</td>" +
					"<td>" +
						"<div class='btn-group noprint'>" +
							"<a href='#' onclick='dresep.edit_bahan(" + r_num + "," + dracikan_num + ")' data-content='Ubah' data-toggle='popover' class='input btn btn-warning'>" + 
								"<i class='icon-edit icon-white'></i>" +
							"</a>" +
							"<a href='#' onclick='dresep.delete_bahan(" + r_num + "," + dracikan_num + ")' data-content='Hapus' data-toggle='popover' class='input btn btn-danger'>" + 
								"<i class='icon-remove icon-white'></i>" + 
							"</a>" +
						"</div>" +
					"</td>" +
				"</tr>"
			);
			dracikan_num++;
		}
	}
	$("#racikan_save").removeAttr("onclick");
	$("#racikan_save").attr("onclick", "dresep.update_obat_racikan(" + dr_num + ", " + r_num + ")");
	$("#obat_racikan_add_form").smodal("show");
	$(".btn").removeAttr("disabled");
};
DResepAction.prototype.update_obat_racikan = function(dr_num, r_num) {
	if ($(".btn").attr("disabled") == "disabled")
		return;
	$(".btn").removeAttr("disabled");
	$(".btn").attr("disabled", "disabled");
	if (!this.validate_obat_racikan()) {
		$(".btn").removeAttr("disabled");
		return;
	}
	var racikan_harga = 0;
	var racikan_embalase = 0;
	var racikan_tuslah = 0;
	var nor = $("tbody#dracikan_list").children("tr").length;
	for(var i = 0; i < nor; i++) {
		var prefix = $("tbody#dracikan_list").children("tr").eq(i).prop("id");
		var br_num = $("#" + prefix + "_row_num").text();
		var id_bahan = $("#" + prefix + "_id_bahan").text();
		var kode_bahan = $("#" + prefix + "_kode_bahan").text();
		var nama_bahan = $("#" + prefix + "_nama_bahan").text();
		var nama_jenis_bahan = $("#" + prefix + "_nama_jenis_bahan").text();
		var jumlah = $("#" + prefix + "_jumlah").text();
		var satuan = $("#" + prefix + "_satuan").text();
		var konversi = $("#" + prefix + "_konversi").text();
		var satuan_konversi = $("#" + prefix + "_satuan_konversi").text();
		var harga = $("#" + prefix + "_harga").text();
		var embalase = $("#" + prefix + "_embalase").text();
		var tuslah = $("#" + prefix + "_tuslah").text();
		var markup = $("#" + prefix + "_markup").text();
		var was_deleted = false;
		if ($("#" + prefix).hasClass("deleted")) {
			var bahan_id = $("#bahan_" + br_num + "_id").text();
			if (bahan_id.length == 0) {
				$("#bahan_" + br_num).remove();
			} else {
				$("#bahan_" + br_num).addClass("deleted");
			}
			was_deleted = true;
		}
		if (!was_deleted) {
			if (br_num.length == 0) {
				$("tbody#bahan_racikan_list").append(
					"<tr id=bahan_" + bahan_num + ">" +
						"<td style='display: none;' id='bahan_" + bahan_num + "_id'></td>" +
						"<td style='display: none;' id='bahan_" + bahan_num + "_id_bahan'>" + id_bahan + "</td>" +
						"<td style='display: none;' id='bahan_" + bahan_num + "_kode'>" + kode_bahan + "</td>" +
						"<td style='display: none;' id='bahan_" + bahan_num + "_nama'>" + nama_bahan + "</td>" +
						"<td style='display: none;' id='bahan_" + bahan_num + "_nama_jenis_bahan'>" + nama_jenis_bahan + "</td>" +
						"<td style='display: none;' id='bahan_" + bahan_num + "_jumlah'>" + jumlah + "</td>" +
						"<td style='display: none;' id='bahan_" + bahan_num + "_jumlah_lama'>0</td>" +
						"<td style='display: none;' id='bahan_" + bahan_num + "_satuan'>" + satuan + "</td>" +
						"<td style='display: none;' id='bahan_" + bahan_num + "_konversi'>" + konversi + "</td>" +
						"<td style='display: none;' id='bahan_" + bahan_num + "_satuan_konversi'>" + satuan_konversi + "</td>" +
						"<td style='display: none;' id='bahan_" + bahan_num + "_harga'>" + harga + "</td>" +
						"<td style='display: none;' id='bahan_" + bahan_num + "_embalase'>" + embalase + "</td>" +
						"<td style='display: none;' id='bahan_" + bahan_num + "_tuslah'>" + tuslah + "</td>" +
						"<td style='display: none;' id='bahan_" + bahan_num + "_markup'>" + markup + "</td>" +
						"<td style='display: none;' id='bahan_" + bahan_num + "_label'>racikan_" + r_num + "</td>" +
					"</tr>"
				);
				bahan_num++;
			} else {
				$("#bahan_" + br_num + "_id_bahan").text(id_bahan);
				$("#bahan_" + br_num + "_kode").text(kode_bahan);
				$("#bahan_" + br_num + "_nama").text(nama_bahan);
				$("#bahan_" + br_num + "_nama_jenis_bahan").text(nama_jenis_bahan);
				$("#bahan_" + br_num + "_jumlah").text(jumlah);
				$("#bahan_" + br_num + "_satuan").text(satuan);
				$("#bahan_" + br_num + "_konversi").text(konversi);
				$("#bahan_" + br_num + "_satuan_konversi").text(satuan_konversi);
				$("#bahan_" + br_num + "_harga").text(harga);
				$("#bahan_" + br_num + "_markup").text(markup);
			}
			var v_bahan_harga = parseFloat(harga.replace(/[^0-9-,]/g, '').replace(",", ".")) * parseFloat(jumlah);
			var v_embalase = parseFloat(embalase.replace(/[^0-9-,]/g, '').replace(",", "."));
			var v_tuslah = parseFloat(tuslah.replace(/[^0-9-,]/g, '').replace(",", "."));
			racikan_harga += v_bahan_harga;
			racikan_embalase += v_embalase;
			racikan_tuslah += v_tuslah;
		}
	}
	var racikan_id = $("#obat_racikan_id").val();
	var racikan_nama = $("#obat_racikan_nama").val();
	var racikan_id_apoteker = $("#obat_racikan_id_apoteker").val();
	var racikan_nama_apoteker = $("#obat_racikan_name_apoteker").val();
	var racikan_tanggal_exp = $("#obat_racikan_tanggal_exp").val();
	var racikan_obat_luar = $("#obat_racikan_obat_luar").val();
	var racikan_jumlah_pakai_sehari = $("#obat_racikan_jumlah_pakai_sehari").val();
	var racikan_satuan_pakai = $("#obat_racikan_satuan_pakai").val();
	var racikan_takaran_pakai = $("#obat_racikan_takaran_pakai").val();
	var racikan_pemakaian = $("#obat_racikan_pemakaian").val();
	var racikan_pemakaian_obat_luar = $("#obat_racikan_pemakaian_obat_luar").val();
	var racikan_penggunaan = $("#obat_racikan_penggunaan").val();
	var racikan_detail_penggunaan = $("#obat_racikan_detail_penggunaan").val();
	var subtotal = racikan_harga;
	var f_harga = "Rp. " + (parseFloat(racikan_harga)).formatMoney("2", ".", ",");
	var f_tuslah = "Rp. " + (parseFloat(racikan_tuslah)).formatMoney("2", ".", ",");
	var f_embalase = "Rp. " + (parseFloat(racikan_embalase)).formatMoney("2", ".", ",");
	var f_subtotal = "Rp. " + (parseFloat(subtotal)).formatMoney("2", ".", ",");
	$("#dresep_" + dr_num + "_nama_racikan").text(racikan_nama);
	$("#dresep_" + dr_num + "_id_apoteker").text(racikan_id_apoteker);
	$("#dresep_" + dr_num + "_nama_apoteker").text(racikan_nama_apoteker);
	$("#dresep_" + dr_num + "_tanggal_exp").text(racikan_tanggal_exp);
	$("#dresep_" + dr_num + "_obat_luar").text(racikan_obat_luar);
	$("#dresep_" + dr_num + "_jumlah_pakai_sehari").text(racikan_jumlah_pakai_sehari);
	$("#dresep_" + dr_num + "_satuan_pakai").text(racikan_satuan_pakai);
	$("#dresep_" + dr_num + "_takaran_pakai").text(racikan_takaran_pakai);
	$("#dresep_" + dr_num + "_pemakaian").text(racikan_pemakaian);
	$("#dresep_" + dr_num + "_pemakaian_obat_luar").text(racikan_pemakaian_obat_luar);
	$("#dresep_" + dr_num + "_penggunaan").text(racikan_penggunaan);
	$("#dresep_" + dr_num + "_detail_penggunaan").text(racikan_detail_penggunaan);
	$("#dresep_" + dr_num + "_hna").text(f_harga);
	$("#dresep_" + dr_num + "_tuslah").text(f_tuslah);
	$("#dresep_" + dr_num + "_embalase").text(f_embalase);
	$("#dresep_" + dr_num + "_hna").text(f_harga);
	resep.refreshHargaAndSubtotal();
	$("#obat_racikan_add_form").smodal("hide");
	$(".btn").removeAttr("disabled");
};
DResepAction.prototype.view_obat_racikan = function(dr_num, r_num) {
	if ($(".btn").attr("disabled") == "disabled")
		return;
	$(".btn").removeAttr("disabled");
	$(".btn").attr("disabled", "disabled");
	var racikan_id = $("#dresep_" + dr_num + "_id").text();
	var racikan_nama = $("#dresep_" + dr_num + "_nama_racikan").text();
	var racikan_id_apoteker = $("#dresep_" + dr_num + "_id_apoteker").text();
	var racikan_nama_apoteker = $("#dresep_" + dr_num + "_apoteker").text();
	var racikan_aturan_pakai = $("#dresep_" + dr_num + "_aturan_pakai").text();
	var racikan_harga = $("#dresep_" + dr_num + "_harga").text();
	var racikan_tanggal_exp = $("#dresep_" + dr_num + "_tanggal_exp").text();
	var racikan_obat_luar = $("#dresep_" + dr_num + "_obat_luar").text();
	var racikan_jumlah_pakai_sehari = $("#dresep_" + dr_num + "_jumlah_pakai_sehari").text();
	var racikan_satuan_pakai = $("#dresep_" + dr_num + "_satuan_pakai").text();
	var racikan_takaran_pakai = $("#dresep_" + dr_num + "_takaran_pakai").text();
	var racikan_pemakaian = $("#dresep_" + dr_num + "_pemakaian").text();
	var racikan_pemakaian_obat_luar = $("#dresep_" + dr_num + "_pemakaian_obat_luar").text();
	var racikan_penggunaan = $("#dresep_" + dr_num + "_penggunaan").text();
	var racikan_detail_penggunaan = $("#dresep_" + dr_num + "_detail_penggunaan").text();
	$("#obat_racikan_id").val(racikan_id);
	$("#obat_racikan_nama").val(racikan_nama);
	$("#obat_racikan_nama").removeAttr("disabled");
	$("#obat_racikan_nama").attr("disabled", "disabled");
	$("#obat_racikan_id_apoteker").val(racikan_id_apoteker);
	$("#obat_racikan_nama_apoteker").val(racikan_nama_apoteker);
	$("#obat_racikan_name_apoteker").val(racikan_nama_apoteker);
	$("#obat_racikan_tanggal_exp").val(racikan_tanggal_exp);
	$("#obat_racikan_tanggal_exp").removeAttr("disabled");
	$("#obat_racikan_tanggal_exp").attr("disabled", "disabled");
	$("#obat_racikan_obat_luar").val(racikan_obat_luar);
	$("#obat_racikan_obat_luar").removeAttr("disabled");
	$("#obat_racikan_obat_luar").attr("disabled", "disabled");
	$("#obat_racikan_jumlah_pakai_sehari").val(racikan_jumlah_pakai_sehari);
	$("#obat_racikan_jumlah_pakai_sehari").removeAttr("disabled");
	$("#obat_racikan_jumlah_pakai_sehari").attr("disabled", "disabled");
	$("#obat_racikan_satuan_pakai").val(racikan_satuan_pakai);
	$("#obat_racikan_satuan_pakai").removeAttr("disabled");
	$("#obat_racikan_satuan_pakai").attr("disabled", "disabled");
	$("#obat_racikan_takaran_pakai").val(racikan_takaran_pakai);
	$("#obat_racikan_takaran_pakai").removeAttr("disabled");
	$("#obat_racikan_takaran_pakai").attr("disabled", "disabled");
	$("#obat_racikan_pemakaian").val(racikan_pemakaian);
	$("#obat_racikan_pemakaian").removeAttr("disabled");
	$("#obat_racikan_pemakaian").attr("disabled", "disabled");
	$("#obat_racikan_pemakaian_obat_luar").val(racikan_pemakaian_obat_luar);
	$("#obat_racikan_pemakaian_obat_luar").removeAttr("disabled");
	$("#obat_racikan_pemakaian_obat_luar").attr("disabled", "disabled");
	$("#obat_racikan_penggunaan").val(racikan_penggunaan);
	$("#obat_racikan_penggunaan").removeAttr("disabled");
	$("#obat_racikan_penggunaan").attr("disabled", "disabled");
	$("#obat_racikan_penggunaan").trigger("change");
	$("#obat_racikan_detail_penggunaan").val(racikan_detail_penggunaan);
	$("#obat_racikan_detail_penggunaan").removeAttr("disabled");
	$("#obat_racikan_detail_penggunaan").attr("disabled", "disabled");
	$("#apoteker_browse").removeAttr("onclick");
	$("#apoteker_browse").removeClass("btn-info");
	$("#apoteker_browse").removeClass("btn-inverse");
	$("#apoteker_browse").addClass("btn-inverse");
	$("#obat_racikan_aturan_pakai").val(racikan_aturan_pakai);
	$("#obat_racikan_aturan_pakai").removeAttr("disabled");
	$("#obat_racikan_aturan_pakai").attr("disabled", "disabled");
	$("#bahan_add").hide();
	dracikan_num = 0;
	$("#dracikan_list").children("tr").remove();
	$("#modal_alert_obat_racikan_add_form").html("");
	$(".error_field").removeClass("error_field");
	for(i = 0; i < bahan_num; i++) {
		var was_deleted = false;
		if ($("#bahan_" + i).hasClass("deleted")) {
			was_deleted = true;
		}
		var dracikan_id = $("#bahan_" + i + "_id").text();
		var dracikan_id_bahan = $("#bahan_" + i + "_id_bahan").text();
		var dracikan_kode_bahan = $("#bahan_" + i + "_kode").text();
		var dracikan_nama_bahan = $("#bahan_" + i + "_nama").text();
		var dracikan_nama_jenis_bahan = $("#bahan_" + i + "_nama_jenis_bahan").text();
		var dracikan_jumlah = $("#bahan_" + i + "_jumlah").text();
		var dracikan_satuan = $("#bahan_" + i + "_satuan").text();
		var dracikan_konversi = $("#bahan_" + i + "_konversi").text();
		var dracikan_satuan_konversi = $("#bahan_" + i + "_satuan_konversi").text();
		var dracikan_harga = $("#bahan_" + i + "_harga").text();
		var dracikan_embalase = $("#bahan_" + i + "_embalase").text();
		var dracikan_tuslah = $("#bahan_" + i + "_tuslah").text();
		var v_harga = $("#bahan_" + i + "_harga").text().replace(/[^0-9-,]/g, '').replace(",", ".");
		var v_embalase = $("#bahan_" + i + "_embalase").text().replace(/[^0-9-,]/g, '').replace(",", ".");
		var v_tuslah = $("#bahan_" + i + "_tuslah").text().replace(/[^0-9-,]/g, '').replace(",", ".");
		var dracikan_subtotal = "Rp. " + (parseFloat(dracikan_jumlah) * parseFloat(v_harga) + parseFloat(v_embalase) + parseFloat(v_tuslah)).formatMoney("2", ".", ",");
		if ($("#bahan_" + i + "_label").text() == "racikan_" + r_num && was_deleted == false) {
			$("tbody#dracikan_list").append(
				"<tr id='racikan_" + r_num + "_" + dracikan_num + "'>" +
					"<td id='racikan_" + r_num + "_" + dracikan_num + "_id' style='display: none;'>" + dracikan_id + "</td>" +
					"<td id='racikan_" + r_num + "_" + dracikan_num + "_row_num' style='display: none;'>" + i + "</td>" +
					"<td id='racikan_" + r_num + "_" + dracikan_num + "_id_bahan' style='display: none;'>" + dracikan_id_bahan + "</td>" +
					"<td id='racikan_" + r_num + "_" + dracikan_num + "_kode_bahan' style='display: none;'>" + dracikan_kode_bahan + "</td>" +
					"<td id='racikan_" + r_num + "_" + dracikan_num + "_nama_jenis_bahan' style='display: none;'>" + dracikan_nama_jenis_bahan + "</td>" +
					"<td id='racikan_" + r_num + "_" + dracikan_num + "_jumlah' style='display: none;'>" + dracikan_jumlah + "</td>" +
					"<td id='racikan_" + r_num + "_" + dracikan_num + "_jumlah_lama' style='display: none;'>" + dracikan_jumlah + "</td>" +
					"<td id='racikan_" + r_num + "_" + dracikan_num + "_satuan' style='display: none;'>" + dracikan_satuan + "</td>" +
					"<td id='racikan_" + r_num + "_" + dracikan_num + "_konversi' style='display: none;'>" + dracikan_konversi + "</td>" +
					"<td id='racikan_" + r_num + "_" + dracikan_num + "_satuan_konversi' style='display: none;'>" + dracikan_satuan_konversi + "</td>" +
					"<td id='racikan_" + r_num + "_" + dracikan_num + "_nama_bahan'>" + dracikan_nama_bahan + "</td>" +
					"<td id='racikan_" + r_num + "_" + dracikan_num + "_harga'>" + dracikan_harga + "</td>" +
					"<td id='racikan_" + r_num + "_" + dracikan_num + "_f_jumlah'>" + dracikan_jumlah + " " + dracikan_satuan + "</td>" +
					"<td id='racikan_" + r_num + "_" + dracikan_num + "_embalase'>" + dracikan_embalase + "</td>" +
					"<td id='racikan_" + r_num + "_" + dracikan_num + "_tuslah'>" + dracikan_tuslah + "</td>" +
					"<td id='racikan_" + r_num + "_" + dracikan_num + "_subtotal'>" + dracikan_subtotal + "</td>" +
					"<td></td>" +
				"</tr>"
			);
			dracikan_num++;
		}
	}
	$("#racikan_save").removeAttr("onclick");
	$("#racikan_save").hide();
	$("#racikan_ok").show();
	$("#obat_racikan_add_form").smodal("show");
	$(".btn").removeAttr("disabled");
};
DResepAction.prototype.delete = function(r_num) {
	if ($(".btn").attr("disabled") == "disabled")
		return;
	$(".btn").removeAttr("disabled");
	$(".btn").attr("disabled", "disabled");
	var id = $("#dresep_" + r_num + "_id").text();
	if (id.length == 0) {
		$("#dresep_" + r_num).remove();
	} else {
		$("#dresep_" + r_num).attr("style", "display: none;");
		$("#dresep_" + r_num).attr("class", "deleted");
	}
	resep.refreshBiayaTotal();
	resep.refresh_no_dresep();
	$(".btn").removeAttr("disabled");
};
DResepAction.prototype.show_add_bahan_form = function() {
	if ($(".btn").attr("disabled") == "disabled")
		return;
	$(".btn").removeAttr("disabled");
	$(".btn").attr("disabled", "disabled");
	$("#bahan_id").val("");
	$("#bahan_id_bahan").val("");
	$("#bahan_kode_bahan").val("");
	$("#bahan_name_bahan").val("");
	$("#bahan_nama_bahan").val("");
	$("#bahan_nama_jenis_bahan").val("");
	$("#bahan_satuan").removeAttr("onchange");
	$("#bahan_satuan").attr("onchange", "bahan.setDetailInfo()");
	$("#bahan_satuan").html("");
	$("#bahan_stok").val("");
	$("#bahan_f_stok").val("");
	$("#bahan_jumlah_lama").val(0);
	$("#bahan_hna").val("");
	$("#bahan_embalase").val("Rp. " + parseFloat(embalase_bahan_racikan).formatMoney("2", ".", ","));
	$("#bahan_tuslah").val("Rp. " + parseFloat(tuslah_bahan_racikan).formatMoney("2", ".", ","));
	$("#bahan_markup").val(0);
	$("#bahan_jumlah").val("");
	$("#bahan_save").removeAttr("onclick");
	$("#bahan_save").attr("onclick", "dresep.save_bahan()");
	$("#modal_alert_bahan_add_form").html("");
	$(".error_field").removeClass("error_field");
	$("#bahan_add_form").smodal("show");
	$(".btn").removeAttr("disabled");
};
DResepAction.prototype.validate_bahan = function() {
	var valid = true;
	var invalid_msg = "";
	var nama_bahan = $("#bahan_name_bahan").val();
	var satuan = $("#bahan_satuan").val();
	var stok = $("#bahan_stok").val();
	var jumlah_lama = $("#bahan_jumlah_lama").val();
	var jumlah = $("#bahan_jumlah").val();
	var harga = $("#bahan_hna").val();
	$(".error_field").removeClass("error_field");
	if (nama_bahan == "") {
		valid = false;
		invalid_msg += "</br><strong>Nama Bahan</strong> tidak boleh kosong";
		$("#bahan_nama_bahan").addClass("error_field");
		$("#bahan_nama_bahan").focus();
	}
	if (satuan == "") {
		valid = false;
		invalid_msg += "</br><strong>Satuan</strong> tidak boleh kosong";
		$("#bahan_satuan").addClass("error_field");
		$("#bahan_satuan").focus();
	}
	if (jumlah == "") {
		valid = false;
		invalid_msg += "</br><strong>Jumlah</strong> tidak boleh kosong";
		$("#bahan_jumlah").addClass("error_field");
		$("#bahan_jumlah").focus();
	} else if (!is_numeric(jumlah)) {
		valid = false;
		invalid_msg += "</br><strong>Jumlah</strong> seharusnya numerik (0-9)";
		$("#bahan_jumlah").addClass("error_field");
		$("#bahan_jumlah").focus();
	} else if (stok != "" && is_numeric(stok) && (parseFloat(jumlah) - parseFloat(jumlah_lama)) > parseFloat(stok)) {
		valid = false;
		invalid_msg += "</br><strong>Jumlah</strong> tidak boleh melebihi stok";
		$("#bahan_jumlah").addClass("error_field");
		$("#bahan_jumlah").focus();
	}
	if (harga == "" || harga == 0) {
		valid = false;
		invalid_msg += "</br><strong>Harga Netto</strong> tidak boleh kosong";
		$("#bahan_hna").addClass("error_field");
		$("#bahan_jumlah").focus();
	}
	if (!valid) {
		$("#modal_alert_bahan_add_form").html(
			"<div class='alert alert-block alert-danger'>" +
				"<h4>Peringatan</h4>" +
				invalid_msg +
			"</div>"
		);
	}
	return valid;
};
DResepAction.prototype.save_bahan = function() {
	if ($(".btn").attr("disabled") == "disabled")
		return;
	$(".btn").removeAttr("disabled");
	$(".btn").attr("disabled", "disabled");
	if (!this.validate_bahan()) {
		$(".btn").removeAttr("disabled");
		return;
	}
	var dracikan_id = $("#bahan_id").val();
	var dracikan_id_bahan = $("#bahan_id_bahan").val();
	var dracikan_kode_bahan = $("#bahan_kode_bahan").val();
	var dracikan_nama_bahan = $("#bahan_name_bahan").val();
	var dracikan_nama_jenis_bahan = $("#bahan_nama_jenis_bahan").val();
	var dracikan_jumlah_lama = $("#bahan_jumlah_lama").val();
	var dracikan_jumlah = $("#bahan_jumlah").val();
	var dracikan_satuan = $("#bahan_satuan").find(":selected").text();
	var dracikan_konversi = $("#bahan_konversi").val();
	var dracikan_satuan_konversi = $("#bahan_satuan_konversi").val();
	var dracikan_hna = $("#bahan_hna").val();
	var dracikan_embalase = $("#bahan_embalase").val();
	var dracikan_tuslah = $("#bahan_tuslah").val();
	var dracikan_markup = $("#bahan_markup").val();
	var v_hna = parseFloat(dracikan_hna.replace(/[^0-9-,]/g, '').replace(",", "."));
	var v_embalase = parseFloat(dracikan_embalase.replace(/[^0-9-,]/g, '').replace(",", "."));
	var v_tuslah = parseFloat(dracikan_tuslah.replace(/[^0-9-,]/g, '').replace(",", "."));
	var v_subtotal = parseFloat(dracikan_jumlah) * parseFloat(v_hna) + parseFloat(v_embalase) + parseFloat(v_tuslah);
	dracikan_subtotal = "Rp. " + (parseFloat(v_subtotal)).formatMoney("2", ".", ",");
	$("tbody#dracikan_list").append(
		"<tr id='racikan_" + racikan_num + "_" + dracikan_num + "'>" +
			"<td id='racikan_" + racikan_num + "_" + dracikan_num + "_id' style='display: none;'>" + dracikan_id + "</td>" +
			"<td id='racikan_" + racikan_num + "_" + dracikan_num + "_row_num' style='display: none;'></td>" +
			"<td id='racikan_" + racikan_num + "_" + dracikan_num + "_id_bahan' style='display: none;'>" + dracikan_id_bahan + "</td>" +
			"<td id='racikan_" + racikan_num + "_" + dracikan_num + "_kode_bahan' style='display: none;'>" + dracikan_kode_bahan + "</td>" +
			"<td id='racikan_" + racikan_num + "_" + dracikan_num + "_nama_jenis_bahan' style='display: none;'>" + dracikan_nama_jenis_bahan + "</td>" +
			"<td id='racikan_" + racikan_num + "_" + dracikan_num + "_jumlah' style='display: none;'>" + dracikan_jumlah + "</td>" +
			"<td id='racikan_" + racikan_num + "_" + dracikan_num + "_jumlah_lama' style='display: none;'>" + dracikan_jumlah_lama + "</td>" +
			"<td id='racikan_" + racikan_num + "_" + dracikan_num + "_satuan' style='display: none;'>" + dracikan_satuan + "</td>" +
			"<td id='racikan_" + racikan_num + "_" + dracikan_num + "_konversi' style='display: none;'>" + dracikan_konversi + "</td>" +
			"<td id='racikan_" + racikan_num + "_" + dracikan_num + "_satuan_konversi' style='display: none;'>" + dracikan_satuan_konversi + "</td>" +
			"<td id='racikan_" + racikan_num + "_" + dracikan_num + "_markup' style='display: none;'>" + dracikan_markup + "</td>" +
			"<td id='racikan_" + racikan_num + "_" + dracikan_num + "_nama_bahan'>" + dracikan_nama_bahan + "</td>" +
			"<td id='racikan_" + racikan_num + "_" + dracikan_num + "_harga'>" + dracikan_hna + "</td>" +
			"<td id='racikan_" + racikan_num + "_" + dracikan_num + "_f_jumlah'>" + dracikan_jumlah + " " + dracikan_satuan + "</td>" +
			"<td id='racikan_" + racikan_num + "_" + dracikan_num + "_embalase'>" + dracikan_embalase + "</td>" +
			"<td id='racikan_" + racikan_num + "_" + dracikan_num + "_tuslah'>" + dracikan_tuslah + "</td>" +
			"<td id='racikan_" + racikan_num + "_" + dracikan_num + "_subtotal'>" + dracikan_subtotal + "</td>" +
			"<td>" +
				"<div class='btn-group noprint'>" +
					"<a href='#' onclick='dresep.edit_bahan(" + racikan_num + "," + dracikan_num + ")' data-content='Ubah' data-toggle='popover' class='input btn btn-warning'>" + 
						"<i class='icon-edit icon-white'></i>" +
					"</a>" +
					"<a href='#' onclick='dresep.delete_bahan(" + racikan_num + "," + dracikan_num + ")' data-content='Hapus' data-toggle='popover' class='input btn btn-danger'>" + 
						"<i class='icon-remove icon-white'></i>" + 
					"</a>" +
				"</div>" +
			"</td>" +
		"</tr>"
	);
	$("#bahan_id").val("");
	$("#bahan_id_bahan").val("");
	$("#bahan_kode_bahan").val("");
	$("#bahan_name_bahan").val("");
	$("#bahan_nama_bahan").val("");
	$("#bahan_nama_jenis_bahan").val("");
	$("#bahan_satuan").removeAttr("onchange");
	$("#bahan_satuan").attr("onchange", "bahan.setDetailInfo()");
	$("#bahan_satuan").html("");
	$("#bahan_stok").val("");
	$("#bahan_f_stok").val("");
	$("#bahan_jumlah_lama").val(0);
	$("#bahan_hna").val("");
	$("#bahan_markup").val(0);
	$("#bahan_jumlah").val("");
	$("#bahan_save").removeAttr("onclick");
	$("#bahan_save").attr("onclick", "dresep.save_bahan()");
	$("#modal_alert_bahan_add_form").html("");
	$(".error_field").removeClass("error_field");
	$("#bahan_nama_bahan").focus();
	dracikan_num++;
	$(".btn").removeAttr("disabled");
};
DResepAction.prototype.edit_bahan = function(r_num, b_num) {
	if ($(".btn").attr("disabled") == "disabled")
		return;
	$(".btn").removeAttr("disabled");
	$(".btn").attr("disabled", "disabled");
	var dracikan_id = $("#racikan_" + r_num + "_" + b_num + "_id").text();
	var dracikan_id_bahan = $("#racikan_" + r_num + "_" + b_num + "_id_bahan").text();
	var dracikan_kode_bahan = $("#racikan_" + r_num + "_" + b_num + "_kode_bahan").text();
	var dracikan_nama_bahan = $("#racikan_" + r_num + "_" + b_num + "_nama_bahan").text();
	var dracikan_nama_jenis_bahan = $("#racikan_" + r_num + "_" + b_num + "_nama_jenis_bahan").text();
	var dracikan_jumlah_lama = $("#racikan_" + r_num + "_" + b_num + "_jumlah_lama").text();
	var dracikan_jumlah = $("#racikan_" + r_num + "_" + b_num + "_jumlah").text();
	var dracikan_satuan = $("#racikan_" + r_num + "_" + b_num + "_satuan").text();
	var dracikan_konversi = $("#racikan_" + r_num + "_" + b_num + "_konversi").text();
	var dracikan_satuan_konversi = $("#racikan_" + r_num + "_" + b_num + "_satuan_konversi").text();
	var dracikan_hna = $("#racikan_" + r_num + "_" + b_num + "_harga").text();
	var dracikan_embalase = $("#racikan_" + r_num + "_" + b_num + "_embalase").text();
	var dracikan_tuslah = $("#racikan_" + r_num + "_" + b_num + "_tuslah").text();
	var dracikan_markup = $("#racikan_" + r_num + "_" + b_num + "_markup").text();
	$("#bahan_id").val(dracikan_id);
	$("#bahan_jumlah_lama").val(dracikan_jumlah_lama);
	$("#bahan_jumlah").val(dracikan_jumlah);
	$("#bahan_embalase").val(dracikan_embalase);
	$("#bahan_tuslah").val(dracikan_tuslah);
	$("#bahan_markup").val(dracikan_markup);
	var self = this;
	var data = this.getRegulerData();
	data['super_command'] = "bahan";
	data['command'] = "edit";
	data['id'] = dracikan_id_bahan;
	$.post(
		"",
		data,
		function(response) {
			var json = getContent(response);
			if (json == null) return;
			$("#bahan_id_bahan").val(json.header.id_obat);
			$("#bahan_kode_bahan").val(json.header.kode_obat);
			$("#bahan_name_bahan").val(json.header.nama_obat);
			$("#bahan_nama_bahan").val(json.header.nama_obat);
			$("#bahan_nama_jenis_bahan").val(json.header.nama_jenis_obat);
			$("#bahan_satuan").html(json.satuan_option);
			$("#bahan_satuan").val(dracikan_konversi + "_" + dracikan_satuan_konversi);
			var part = $("#bahan_satuan").val().split("_");
			$("#bahan_konversi").val(part[0]);
			$("#bahan_satuan_konversi").val(part[1]);
			data = self.getRegulerData();
			data['super_command'] = "sisa";
			data['command'] = "edit";
			data['id_obat'] = $("#bahan_id_bahan").val();
			data['satuan'] = $("#bahan_satuan").find(":selected").text();
			data['konversi'] = $("#bahan_konversi").val();
			data['satuan_konversi'] = $("#bahan_satuan_konversi").val();
			if ($("#resep_jenis").length)
				data['jenis_pasien'] = $("#resep_jenis").val();
			if ($("#resep_asuransi").length)
				data['asuransi'] = $("#resep_asuransi").val();
			if ($("#resep_perusahaan").length)
				data['perusahaan'] = $("#resep_perusahaan").val();
			if ($("#resep_uri").length)
				data['uri'] = $("#resep_uri").val();
			$.post(
				"",
				data,
				function(response) {
					var json = getContent(response);
					if (json == null) return;
					$("#bahan_stok").val(json.sisa);
					$("#bahan_f_stok").val(json.sisa + " " + json.satuan);						
					var hna = parseFloat(json.hna) * (1 + parseFloat(json.markup) / 100);
					hna = "Rp. " + (parseFloat(hna)).formatMoney("2", ".", ",");
					$("#bahan_hna").val(hna);
					$("#bahan_markup").val(json.markup);
					$("#bahan_satuan").removeAttr("onchange");
					$("#bahan_satuan").attr("onchange", "obat.setDetailInfo()");
					$("#modal_alert_bahan_add_form").html("");
					$(".error_field").removeClass("error_field");
					$("#bahan_save").removeAttr("onclick");
					$("#bahan_save").attr("onclick", "dresep.update_bahan(" + r_num + "," + b_num + ")");
					$("#bahan_add_form").smodal("show");
					$(".btn").removeAttr("disabled");
				}
			);
		}
	);
};
DResepAction.prototype.update_bahan = function(r_num, b_num) {
	if ($(".btn").attr("disabled") == "disabled")
		return;
	$(".btn").removeAttr("disabled");
	$(".btn").attr("disabled", "disabled");
	if (!this.validate_bahan()) {
		$(".btn").removeAttr("disabled");
		return;
	}
	var dracikan_id_bahan = $("#bahan_id_bahan").val();
	var dracikan_kode_bahan = $("#bahan_kode_bahan").val();
	var dracikan_nama_bahan = $("#bahan_name_bahan").val();
	var dracikan_nama_jenis_bahan = $("#bahan_nama_jenis_bahan").val();
	var dracikan_jumlah = $("#bahan_jumlah").val();
	var dracikan_satuan = $("#bahan_satuan").find(":selected").text();
	var dracikan_konversi = $("#bahan_konversi").val();
	var dracikan_satuan_konversi = $("#bahan_satuan_konversi").val();
	var dracikan_hna = $("#bahan_hna").val();
	var dracikan_embalase = $("#bahan_embalase").val();
	var dracikan_tuslah = $("#bahan_tuslah").val();
	var dracikan_markup = $("#bahan_markup").val();
	var v_hna = parseFloat(dracikan_hna.replace(/[^0-9-,]/g, '').replace(",", "."));
	var v_embalase = parseFloat(dracikan_embalase.replace(/[^0-9-,]/g, '').replace(",", "."));
	var v_tuslah = parseFloat(dracikan_tuslah.replace(/[^0-9-,]/g, '').replace(",", "."));
	var v_subtotal = parseFloat(dracikan_jumlah) * parseFloat(v_hna) + parseFloat(v_embalase) + parseFloat(v_tuslah);
	dracikan_subtotal = "Rp. " + (parseFloat(v_subtotal)).formatMoney("2", ".", ",");
	$("#racikan_" + r_num + "_" + b_num + "_id_bahan").text(dracikan_id_bahan);
	$("#racikan_" + r_num + "_" + b_num + "_kode_bahan").text(dracikan_kode_bahan);
	$("#racikan_" + r_num + "_" + b_num + "_nama_bahan").text(dracikan_nama_bahan);
	$("#racikan_" + r_num + "_" + b_num + "_nama_jenis_bahan").text(dracikan_nama_jenis_bahan);
	$("#racikan_" + r_num + "_" + b_num + "_jumlah").text(dracikan_jumlah);
	$("#racikan_" + r_num + "_" + b_num + "_satuan").text(dracikan_satuan);
	$("#racikan_" + r_num + "_" + b_num + "_konversi").text(dracikan_konversi);
	$("#racikan_" + r_num + "_" + b_num + "_satuan_konversi").text(dracikan_satuan_konversi);
	$("#racikan_" + r_num + "_" + b_num + "_markup").text(dracikan_markup);
	$("#racikan_" + r_num + "_" + b_num + "_harga").text(dracikan_hna);
	$("#racikan_" + r_num + "_" + b_num + "_f_jumlah").text(dracikan_jumlah + " " + dracikan_satuan);
	$("#racikan_" + r_num + "_" + b_num + "_subtotal").text(dracikan_subtotal);
	$("#bahan_add_form").smodal("hide");
	$(".btn").removeAttr("disabled");
};
DResepAction.prototype.delete_bahan = function(r_num, b_num) {
	if ($(".btn").attr("disabled") == "disabled")
		return;
	$(".btn").removeAttr("disabled");
	$(".btn").attr("disabled", "disabled");
	var id = $("tbody#dracikan_list tr#racikan_" + r_num + "_" + b_num + "_id").text();
	var row_num = $("tbody#dracikan_list tr#racikan_" + r_num + "_" + b_num + "_row_num").text();
	$("tbody#dracikan_list tr#racikan_" + r_num + "_" + b_num).attr("style", "display: none;");
	$("tbody#dracikan_list tr#racikan_" + r_num + "_" + b_num).attr("class", "deleted");
	$(".btn").removeAttr("disabled");
};