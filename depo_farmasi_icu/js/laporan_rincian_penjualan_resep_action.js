function DPRAction(name, page, action, column) {
	this.initialize(name, page, action, column);
}
DPRAction.prototype.constructor = DPRAction;
DPRAction.prototype = new TableAction();
DPRAction.prototype.getRegulerData = function() {
	var data = TableAction.prototype.getRegulerData.call(this);
	data['tanggal_from'] = $("#dpr_tanggal_from").val();
	data['tanggal_to'] = $("#dpr_tanggal_to").val();
	data['jenis_pasien'] = $("#dpr_jenis_pasien").val();
	data['uri'] = $("#dpr_uri").val();
	return data;
};
DPRAction.prototype.view = function() {
	if ($("#dpr_tanggal_from").val() == "" || $("#dpr_tanggal_to").val() == "")
		return;
	var self = this;
	FINISHED = false;
	$("#dpr_info").empty();
	$("#table_dpr tfoot").remove();
	$("#dpr_list").empty();
	$("#dpr_loading_bar").sload("true", "Harap ditunggu...", 0);
	$("#dpr_loading_modal").smodal("show");
	var data = this.getRegulerData();
	data['command'] = "get_jumlah";
	$.post(
		"",
		data,
		function(response) {
			var json = JSON.parse(response);
			if (json == null) return;
			self.fillHtml(0, json.jumlah);
		}
	);
};
DPRAction.prototype.fillHtml = function(num, limit) {
	if (FINISHED || num == limit) {
		if (FINISHED == false && num == limit) {
			this.finalize();
		} else {
			$("#dpr_loading_modal").smodal("hide");
			$("#dpr_info").html(
				"<div class='alert alert-block alert-inverse'>" +
					 "<center><strong>PROSES DIBATALKAN</strong></center>" +
				 "</div>"
			);
			$("#dpr_export_button").removeAttr("onclick");
		}
		return;
	}
	var self = this;
	var data = this.getRegulerData();
	data['command'] = "get_info";
	data['num'] = num;
	$.post(
		"",
		data,
		function(response) {
			var json = JSON.parse(response);
			if (json == null) return;
			console.log(json);
			$("tbody#dpr_list").append(
				json.html
			);
			$("#dpr_loading_bar").sload("true", json.nomor_transaksi + " - " + json.nomor_resep + " - " + json.nama_dokter + " - " + json.nama_pasien + " (" + (num + 1) + " / " + limit + ")", (num + 1) * 100 / limit - 1);
			self.fillHtml(num + 1, limit);
		}
	);
};
DPRAction.prototype.finalize = function() {
	$("#dpr_loading_modal").smodal("hide");
	$("#dpr_info").html(
		"<div class='alert alert-block alert-info'>" +
			 "<center><strong>PROSES SELESAI</strong></center>" +
		 "</div>"
	);
	$("#dpr_export_button").removeAttr("onclick");
	$("#dpr_export_button").attr("onclick", "dpr.export_xls()");
};
DPRAction.prototype.cancel = function() {
	FINISHED = true;
};
DPRAction.prototype.export_xls = function() {
	showLoading();
	var num_rows = $("#dpr_list").children("tr").length;
	var d_data = {};
	for (var i = 0; i < num_rows; i++) {
		var label = $("tbody#dpr_list tr:eq(" + i + ") td#dpr_label").text();
		if (label == "data") {
			var nomor = $("tbody#dpr_list tr:eq(" + i + ") td#dpr_nomor").text();
			var tanggal = $("tbody#dpr_list tr:eq(" + i + ") td#dpr_tanggal").text();
			var id = $("tbody#dpr_list tr:eq(" + i + ") td#dpr_id").text();
			var lokasi = $("tbody#dpr_list tr:eq(" + i + ") td#dpr_lokasi").text();
			var jenis = $("tbody#dpr_list tr:eq(" + i + ") td#dpr_jenis").text();
			var perusahaan = $("tbody#dpr_list tr:eq(" + i + ") td#dpr_perusahaan").text();
			var asuransi = $("tbody#dpr_list tr:eq(" + i + ") td#dpr_asuransi").text();
			var noreg_pasien = $("tbody#dpr_list tr:eq(" + i + ") td#dpr_noreg_pasien").text();
			var nrm_pasien = $("tbody#dpr_list tr:eq(" + i + ") td#dpr_nrm_pasien").text();
			var nama_pasien = $("tbody#dpr_list tr:eq(" + i + ") td#dpr_nama_pasien").text();
			var alamat_pasien = $("tbody#dpr_list tr:eq(" + i + ") td#dpr_alamat_pasien").text();
			var no_resep = $("tbody#dpr_list tr:eq(" + i + ") td#dpr_no_resep").text();
			var nama_dokter = $("tbody#dpr_list tr:eq(" + i + ") td#dpr_nama_dokter").text();
			var unit = $("tbody#dpr_list tr:eq(" + i + ") td#dpr_unit").text();
			var nama_obat = $("tbody#dpr_list tr:eq(" + i + ") td#dpr_nama_obat").text();
			var harga = $("tbody#dpr_list tr:eq(" + i + ") td#dpr_harga").text();
			var jumlah = $("tbody#dpr_list tr:eq(" + i + ") td#dpr_jumlah").text();
			var subtotal = $("tbody#dpr_list tr:eq(" + i + ") td#dpr_subtotal").text();
			d_data[i] = {
				"label" 		: label,
				"nomor" 		: nomor,
				"tanggal" 		: tanggal,
				"id" 			: id,
				"lokasi" 		: lokasi,
				"jenis" 		: jenis,
				"perusahaan"	: perusahaan,
				"asuransi"		: asuransi,
				"noreg_pasien"	: noreg_pasien,
				"nrm_pasien"	: nrm_pasien,
				"nama_pasien"	: nama_pasien,
				"alamat_pasien"	: alamat_pasien,
				"no_resep"		: no_resep,
				"nama_dokter"	: nama_dokter,
				"unit"			: unit,
				"nama_obat"		: nama_obat,
				"harga"			: harga,
				"jumlah"		: jumlah,
				"subtotal"		: subtotal
			};
		} else if (label == "diskon") {
			var diskon = $("tbody#dpr_list tr:eq(" + i + ") td#dpr_diskon").text();
			d_data[i] = {
				"label"			: "diskon",
				"diskon"		: diskon
			};
		} else if (label == "total") {
			var total = $("tbody#dpr_list tr:eq(" + i + ") td#dpr_total").text();
			d_data[i] = {
				"label"			: "total",
				"total"			: total
			};
		}
	}
	var data = this.getRegulerData();
	data['command'] = "export_xls";
	data['tanggal_from'] = $("#dpr_tanggal_from").val();
	data['tanggal_to'] = $("#dpr_tanggal_to").val();
	data['d_data'] = JSON.stringify(d_data);
	data['num_rows'] = num_rows;
	postForm(data);
	dismissLoading();
};