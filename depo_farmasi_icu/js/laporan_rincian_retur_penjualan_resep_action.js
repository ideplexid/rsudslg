function DRPRAction(name, page, action, column) {
	this.initialize(name, page, action, column);
}
DRPRAction.prototype.constructor = DRPRAction;
DRPRAction.prototype = new TableAction();
DRPRAction.prototype.getRegulerData = function() {
	var data = TableAction.prototype.getRegulerData.call(this);
	data['tanggal_from'] = $("#drpr_tanggal_from").val();
	data['tanggal_to'] = $("#drpr_tanggal_to").val();
	return data;
};
DRPRAction.prototype.view = function() {
	if ($("#drpr_tanggal_from").val() == "" || $("#drpr_tanggal_to").val() == "")
		return;
	var self = this;
	FINISHED = false;
	$("#drpr_info").empty();
	$("#table_drpr tfoot").remove();
	$("#drpr_list").empty();
	$("#drpr_loading_bar").sload("true", "Harap ditunggu...", 0);
	$("#drpr_loading_modal").smodal("show");
	var data = this.getRegulerData();
	data['command'] = "get_jumlah";
	$.post(
		"",
		data,
		function(response) {
			var json = JSON.parse(response);
			if (json == null) return;
			self.fillHtml(0, json.jumlah);
		}
	);
};
DRPRAction.prototype.fillHtml = function(num, limit) {
	if (FINISHED || num == limit) {
		if (FINISHED == false && num == limit) {
			this.finalize();
		} else {
			$("#drpr_loading_modal").smodal("hide");
			$("#drpr_info").html(
				"<div class='alert alert-block alert-inverse'>" +
					 "<center><strong>PROSES DIBATALKAN</strong></center>" +
				 "</div>"
			);
			$("#drpr_export_button").removeAttr("onclick");
		}
		return;
	}
	var self = this;
	var data = this.getRegulerData();
	data['command'] = "get_info";
	data['num'] = num;
	$.post(
		"",
		data,
		function(response) {
			var json = JSON.parse(response);
			if (json == null) return;
			console.log(json);
			$("tbody#drpr_list").append(
				json.html
			);
			$("#drpr_loading_bar").sload("true", json.nomor_transaksi + " - " + json.nomor_resep + " - " + json.nama_dokter + " - " + json.nama_pasien + " (" + (num + 1) + " / " + limit + ")", (num + 1) * 100 / limit - 1);
			self.fillHtml(num + 1, limit);
		}
	);
};
DRPRAction.prototype.finalize = function() {
	$("#drpr_loading_modal").smodal("hide");
	$("#drpr_info").html(
		"<div class='alert alert-block alert-info'>" +
			 "<center><strong>PROSES SELESAI</strong></center>" +
		 "</div>"
	);
	$("#drpr_export_button").removeAttr("onclick");
	$("#drpr_export_button").attr("onclick", "drpr.export_xls()");
};
DRPRAction.prototype.cancel = function() {
	FINISHED = true;
};
DRPRAction.prototype.export_xls = function() {
	showLoading();
	var num_rows = $("#drpr_list").children("tr").length;
	var d_data = {};
	for (var i = 0; i < num_rows; i++) {
		var label = $("tbody#drpr_list tr:eq(" + i + ") td#drpr_label").text();
		if (label == "data") {
			var nomor = $("tbody#drpr_list tr:eq(" + i + ") td#drpr_nomor").text();
			var tanggal = $("tbody#drpr_list tr:eq(" + i + ") td#drpr_tanggal").text();
			var id = $("tbody#drpr_list tr:eq(" + i + ") td#drpr_id").text();
			var lokasi = $("tbody#drpr_list tr:eq(" + i + ") td#drpr_lokasi").text();
			var jenis = $("tbody#drpr_list tr:eq(" + i + ") td#drpr_jenis").text();
			var perusahaan = $("tbody#drpr_list tr:eq(" + i + ") td#drpr_perusahaan").text();
			var asuransi = $("tbody#drpr_list tr:eq(" + i + ") td#drpr_asuransi").text();
			var noreg_pasien = $("tbody#drpr_list tr:eq(" + i + ") td#drpr_noreg_pasien").text();
			var nrm_pasien = $("tbody#drpr_list tr:eq(" + i + ") td#drpr_nrm_pasien").text();
			var nama_pasien = $("tbody#drpr_list tr:eq(" + i + ") td#drpr_nama_pasien").text();
			var alamat_pasien = $("tbody#drpr_list tr:eq(" + i + ") td#drpr_alamat_pasien").text();
			var no_resep = $("tbody#drpr_list tr:eq(" + i + ") td#drpr_no_resep").text();
			var nama_dokter = $("tbody#drpr_list tr:eq(" + i + ") td#drpr_nama_dokter").text();
			var unit = $("tbody#drpr_list tr:eq(" + i + ") td#drpr_unit").text();
			var nama_obat = $("tbody#drpr_list tr:eq(" + i + ") td#drpr_nama_obat").text();
			var harga = $("tbody#drpr_list tr:eq(" + i + ") td#drpr_harga").text();
			var jumlah = $("tbody#drpr_list tr:eq(" + i + ") td#drpr_jumlah").text();
			var subtotal = $("tbody#drpr_list tr:eq(" + i + ") td#drpr_subtotal").text();
			d_data[i] = {
				"label" 		: label,
				"nomor" 		: nomor,
				"tanggal" 		: tanggal,
				"id" 			: id,
				"lokasi" 		: lokasi,
				"jenis" 		: jenis,
				"perusahaan"	: perusahaan,
				"asuransi"		: asuransi,
				"noreg_pasien"	: noreg_pasien,
				"nrm_pasien"	: nrm_pasien,
				"nama_pasien"	: nama_pasien,
				"alamat_pasien"	: alamat_pasien,
				"no_resep"		: no_resep,
				"nama_dokter"	: nama_dokter,
				"unit"			: unit,
				"nama_obat"		: nama_obat,
				"harga"			: harga,
				"jumlah"		: jumlah,
				"subtotal"		: subtotal
			};
		} else {
			var total = $("tbody#drpr_list tr:eq(" + i + ") td#drpr_total").text();
			d_data[i] = {
				"label"			: "total",
				"total"			: total
			};
		}
	}
	var data = this.getRegulerData();
	data['command'] = "export_xls";
	data['tanggal_from'] = $("#drpr_tanggal_from").val();
	data['tanggal_to'] = $("#drpr_tanggal_to").val();
	data['d_data'] = JSON.stringify(d_data);
	data['num_rows'] = num_rows;
	postForm(data);
	dismissLoading();
};