var drpb;
var FINISHED;
$(document).ready(function() {
	$('.mydatetime').datetimepicker({ 
		minuteStep: 1
	});
	drpb = new DRPBAction(
		"drpb",
		"depo_farmasi_icu",
		"laporan_rincian_retur_penjualan_bebas",
		new Array()
	);
	$("#drpb_list").append(
		"<tr id='temp'>" +
			"<td colspan='18'><strong><small><center>DATA RETUR PENJUALAN BEBAS BELUM DIPROSES</center></small></strong></td>" +
		"</tr>"
	);
	$(document).keyup(function(e) {
		if (e.which == 27) {
			FINISHED = true;
		}
	});
});