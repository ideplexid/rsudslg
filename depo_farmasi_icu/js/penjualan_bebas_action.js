function PenjualanBebasAction(name, page, action, column) {
	this.initialize(name, page, action, column);
}
PenjualanBebasAction.prototype.constructor = PenjualanBebasAction;
PenjualanBebasAction.prototype = new TableAction();
PenjualanBebasAction.prototype.refresh_no_dpenjualan_bebas = function() {
	var no = 1;
	var nor_dpenjualan_bebas = $("tbody#dpenjualan_bebas_list").children("tr").length;
	for(var i = 0; i < nor_dpenjualan_bebas; i++) {
		var dr_prefix = $("tbody#dpenjualan_bebas_list").children("tr").eq(i).prop("id");
		$("#" + dr_prefix + "_nomor").html(no++);
	}
};
PenjualanBebasAction.prototype.show_add_form = function() {
	if ($(".btn").attr("disabled") == "disabled")
		return;
	$(".btn").removeAttr("disabled");
	$(".btn").attr("disabled", "disabled");
	$("#help_bebas").show();
	$("#penjualan_bebas_id").val("");
	$("#penjualan_bebas_nomor").val("");
	$("#penjualan_bebas_nomor").removeAttr("disabled");
	$("#penjualan_bebas_nama").val("");
	$("#penjualan_bebas_nama").removeAttr("disabled");
	$("#penjualan_bebas_markup").val("0");
	$("#obat_jadi_add").show();
	$("tbody#dpenjualan_bebas_list").children().remove();
	dpenjualan_bebas_num = 0;
	$("#modal_alert_penjualan_bebas_add_form").html("");
	$(".error_field").removeClass("error_field");
	$("#penjualan_bebas_diskon").val("0,00");
	$("#penjualan_bebas_diskon").removeAttr("disabled");
	$("#penjualan_bebas_t_diskon").val("persen");
	$("#penjualan_bebas_t_diskon").removeAttr("disabled");
	this.refreshBiayaTotal();
	$("#penjualan_bebas_save").removeAttr("onclick");
	$("#penjualan_bebas_save").attr("onclick", "penjualan_bebas.save()");
	$("#penjualan_bebas_save").show();
	$("#penjualan_bebas_save_cetak").removeAttr("onclick");
	$("#penjualan_bebas_save_cetak").attr("onclick", "penjualan_bebas.save_cetak()");
	$("#penjualan_bebas_save_cetak").show();
	$("#penjualan_bebas_ok").hide();
	$("#penjualan_bebas_add_form").smodal("show");
	$(".btn").removeAttr("disabled");
};
PenjualanBebasAction.prototype.validate = function() {
	var valid = true;
	var invalid_msg = "";
	var nomor = $("#penjualan_bebas_nomor").val();
	var diskon = $("#penjualan_bebas_diskon").val().replace(/[^0-9-,]/g, '').replace(",", ".");
	var t_diskon = $("#penjualan_bebas_t_diskon").val();
	var nord = $("tbody#dpenjualan_bebas_list").children("tr").length;
	$(".error_field").removeClass("error_field");
	if (nomor == "") {
		valid = false;
		invalid_msg += "</br><strong>Nomor</strong> tidak boleh kosong";
		$("#penjualan_bebas_nomor").addClass("error_field");
	}
	if (diskon == "") {
		valid = false;
		invalid_msg += "</br><strong>Diskon</strong> tidak boleh kosong";
		$("#penjualan_bebas_diskon").addClass("error_field");
	}
	if (t_diskon == "") {
		valid = false;
		invalid_msg += "</br><strong>Tipe Diskon</strong> tidak boleh kosong";
		$("#penjualan_bebas_t_diskon").addClass("error_field");
	} else if (t_diskon == "persen") {
		if (diskon > 100) {
			valid = false;
			invalid_msg += "</br><strong>Diskon</strong> tidak boleh lebih dari 100%";
			$("#penjualan_bebas_diskon").addClass("error_field");
		}
	}
	if (nord == 0) {
		valid = false;
		invalid_msg += "</br><strong>Detail PenjualanBebas</strong> tidak boleh kosong";
	}
	if (!valid) {
		$("#modal_alert_penjualan_bebas_add_form").html(
			"<div class='alert alert-block alert-danger'>" +
				"<h4>Peringatan</h4>" +
				invalid_msg +
			"</div>"
		);
	}
	return valid;
};
PenjualanBebasAction.prototype.getSaveData = function() {
	var data = this.getRegulerData();
	data['super_command'] == "penjualan_bebas";
	data['command'] = "save";
	data['id'] = $("#penjualan_bebas_id").val();
	data['nomor_resep'] = $("#penjualan_bebas_nomor").val();
	data['nama_pasien'] = $("#penjualan_bebas_nama").val();
	data['markup'] = $("#penjualan_bebas_markup").val();
	var v_total = parseFloat($("#penjualan_bebas_total").val().replace(/[^0-9-,]/g, '').replace(",", "."));
	data['total'] = v_total;
	data['diskon'] = parseFloat($("#penjualan_bebas_diskon").val().replace(/[^0-9-,]/g, '').replace(",", "."));
	data['t_diskon'] = $("#penjualan_bebas_t_diskon").val();
	data['ppn'] = $("#penjualan_bebas_ppn").val();
	var detail_penjualan_bebas = {};
	var nor_dpenjualan_bebas =  $("tbody#dpenjualan_bebas_list").children("tr").length;
	var pos = 0;
	for(var i = 0; i < nor_dpenjualan_bebas; i++) {
		var dr_prefix = $("tbody#dpenjualan_bebas_list").children("tr").eq(i).prop("id");
		var tipe = $("#" + dr_prefix + "_tipe").text();
		if (tipe == "obat_jadi") {
			var id_obat_jadi = $("#" + dr_prefix + "_id").text();
			var id_obat = $("#" + dr_prefix + "_id_obat").text();
			var kode_obat = $("#" + dr_prefix + "_kode_obat").text();
			var nama_obat = $("#" + dr_prefix + "_nama_obat").text();
			var nama_jenis_obat = $("#" + dr_prefix + "_nama_jenis_obat").text();
			var jumlah = $("#" + dr_prefix + "_jumlah").text();
			var jumlah_lama = $("#" + dr_prefix + "_jumlah_lama").text();
			var satuan = $("#" + dr_prefix + "_satuan").text();
			var konversi = $("#" + dr_prefix + "_konversi").text();
			var satuan_konversi = $("#" + dr_prefix + "_satuan_konversi").text();
			var markup = $("#" + dr_prefix + "_markup").text();
			var v_harga = parseFloat($("#" + dr_prefix + "_harga").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var v_embalase = parseFloat($("#" + dr_prefix + "_embalase").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var v_tusla = parseFloat($("#" + dr_prefix + "_tuslah").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var v_subtotal = parseFloat($("#" + dr_prefix + "_subtotal").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var d_data = {};
			d_data['tipe'] = tipe;
			d_data['id_obat_jadi'] = id_obat_jadi;
			d_data['id_obat'] = id_obat;
			d_data['kode_obat'] = kode_obat;
			d_data['nama_obat'] = nama_obat;
			d_data['nama_jenis_obat'] = nama_jenis_obat;
			d_data['jumlah'] = jumlah;
			d_data['jumlah_lama'] = jumlah_lama;
			d_data['satuan'] = satuan;
			d_data['konversi'] = konversi;
			d_data['satuan_konversi'] = satuan_konversi;
			d_data['harga'] = v_harga;
			d_data['embalase'] = v_embalase;
			d_data['tusla'] = v_tusla;
			d_data['subtotal'] = v_subtotal;
			d_data['pos'] = pos++;
			d_data['markup'] = markup;
			detail_penjualan_bebas[i] = d_data;
		}
	}
	data['detail_penjualan_bebas'] = detail_penjualan_bebas;
	return data;
};
PenjualanBebasAction.prototype.save = function() {
	if ($(".btn").attr("disabled") == "disabled")
		return;
	$(".btn").removeAttr("disabled");
	$(".btn").attr("disabled", "disabled");
	if (!this.validate()) {
		$(".btn").removeAttr("disabled");
		return;
	}
	showLoading();
	var self = this;
	var data = this.getSaveData();
	$.post(
		"",
		data,
		function(response) {
			var json = getContent(response);				
			if (json == null || json.success==0) {
				$("#modal_alert_penjualan_bebas_add_form").html(
					"<div class='alert alert-block alert-info'>" +
						"<h4>Peringatan</h4>" +
						json.message +
					"</div>"
				);
			} else {
				$("#penjualan_bebas_add_form").smodal("hide");
				self.print_prescription(json.id);
				self.view();
			}
			dismissLoading();
			$(".btn").removeAttr("disabled");
		}
	);
};
PenjualanBebasAction.prototype.detail = function(id) {
	if ($(".btn").attr("disabled") == "disabled")
		return;
	$(".btn").removeAttr("disabled");
	$(".btn").attr("disabled", "disabled");
	var self = this;
	var data = this.getRegulerData();
	data['super_command'] = "penjualan_bebas";
	data['command'] = "edit";
	data['id'] = id;
	data['readonly'] = true;
	$.post(
		"",
		data,
		function(response) {
			var json = getContent(response);
			if (json == null) return;
			$("#help_bebas").hide();
			$("#penjualan_bebas_ppn").val(json.header.ppn);
			$("#penjualan_bebas_nomor").val(json.header.nomor_resep);
			$("#penjualan_bebas_nomor").removeAttr("disabled");
			$("#penjualan_bebas_nomor").attr("disabled", "disabled");
			$("#penjualan_bebas_nama").val(json.header.nama_pasien);
			$("#penjualan_bebas_nama").removeAttr("disabled");
			$("#penjualan_bebas_nama").attr("disabled", "disabled");
			$("#penjualan_bebas_markup").val(json.header.markup);
			$("#penjualan_bebas_diskon").val((parseFloat(json.header.diskon)).formatMoney("2", ".", ","));
			$("#penjualan_bebas_diskon").removeAttr("disabled");
			$("#penjualan_bebas_diskon").attr("disabled", "disabled");
			$("#penjualan_bebas_t_diskon").val(json.header.t_diskon);
			$("#penjualan_bebas_t_diskon").removeAttr("disabled");
			$("#penjualan_bebas_t_diskon").attr("disabled", "disabled");
			$("#obat_jadi_add").hide();
			$("tbody#dpenjualan_bebas_list").html(json.dpenjualan_bebas_list);
			dpenjualan_bebas_num = json.dpenjualan_bebas_num;
			$("#modal_alert_penjualan_bebas_add_form").html("");
			$(".error_field").removeClass("error_field");
			self.refreshBiayaTotal();
			$("#penjualan_bebas_save").removeAttr("onclick");
			$("#penjualan_bebas_save").hide();
			$("#penjualan_bebas_ok").show();
			$("#penjualan_bebas_add_form").smodal("show");
			$(".btn").removeAttr("disabled");
		}
	);
};
PenjualanBebasAction.prototype.refreshBiayaTotal = function() {
	var biaya_total = 0;
	var nord = $("tbody#dpenjualan_bebas_list").children("tr").length;
	for(var i = 0; i < nord; i++) {
		var prefix = $("tbody#dpenjualan_bebas_list").children("tr").eq(i).prop("id");
		var v_subtotal = parseFloat($("#" + prefix + "_subtotal").text().replace(/[^0-9-,]/g, '').replace(",", "."));
		biaya_total += v_subtotal;
	}
	var diskon = $("#penjualan_bebas_diskon").val();
	diskon = parseFloat(diskon.replace(/[^0-9-,]/g, '').replace(",", "."));
	var t_diskon = $("#penjualan_bebas_t_diskon").val();
	if (t_diskon == "persen" || t_diskon == "gratis") {
		diskon = (diskon * biaya_total) / 100;
	}
	biaya_total = biaya_total - diskon;
	biaya_total = "Rp. " + parseFloat(biaya_total).formatMoney("2", ".", ",");
	$("#penjualan_bebas_total").val(biaya_total);
};
PenjualanBebasAction.prototype.refreshHargaAndSubtotal = function() {
	var self = this;
	if (need_margin_penjualan_request == 1 && mode_margin == 0) {
		need_margin_penjualan_request = 0;
		var data = this.getRegulerData();
		data['super_command'] = "penjualan_bebas";
		data['command'] = "get_margin_penjualan";
		data['mode_margin'] = mode_margin;
		$.post(
			"",
			data,
			function(response) {
				var json = JSON.parse(response);
				self.refreshDetailInfo(json.margin_penjualan);
			}
		);
	} else {
		var markup = $("#penjualan_bebas_markup").val();
		this.refreshDetailInfo(markup);
	}
};
PenjualanBebasAction.prototype.refreshDetailInfo = function(markup) {
	$("#penjualan_bebas_markup").val(markup);
	var nord = $("tbody#dpenjualan_bebas_list").children("tr").length;
	for(var i = 0; i < nord; i++) {
		var prefix = $("tbody#dpenjualan_bebas_list").children("tr").eq(i).prop("id");
		var hna = $("#" + prefix + "_hna").text();
		var embalase = $("#" + prefix + "_embalase").text();
		var tuslah = $("#" + prefix + "_tuslah").text();
		var biaya_racik = $("#" + prefix + "_biaya_racik").text();
		var v_hna = parseFloat(hna.replace(/[^0-9-,]/g, '').replace(",", "."));
		var v_embalase = parseFloat(embalase.replace(/[^0-9-,]/g, '').replace(",", "."));
		var v_tuslah = parseFloat(tuslah.replace(/[^0-9-,]/g, '').replace(",", "."));
		var v_hja = v_hna + (markup * v_hna);
		var hja = "Rp. " + (parseFloat(v_hja)).formatMoney("2", ".", ",");
		$("#" + prefix + "_harga").text(hja);
		var v_jumlah = $("#" + prefix + "_jumlah").text();
		var v_subtotal = parseFloat(v_jumlah) * parseFloat(v_hja) + parseFloat(v_embalase) + parseFloat(v_tuslah);
		var subtotal = "Rp. " + (parseFloat(v_subtotal)).formatMoney("2", ".", ",");
		$("#" + prefix + "_subtotal").text(subtotal);
	}
	penjualan_bebas.refreshBiayaTotal();
};
PenjualanBebasAction.prototype.cancel = function(id) {
	var self = this;
	bootbox.prompt(
		"Alasan Pembatalan Penjualan Umum Bebas",
		function(result) {
			if (result !== null && result !== "") {
				showLoading();
				var data = self.getRegulerData();
				data['super_command'] = "penjualan_bebas";
				data['command'] = "save";
				data['id'] = id;
				data['dibatalkan'] = 1;
				data['keterangan_batal'] = result;
				$.post(
					"",
					data,
					function(response) {
						var json = getContent(response);
						if (json == null) return;
						self.view();
						dismissLoading();
					}
				);
			} else if (result !== null && result == ""){
				self.cancel(id);
			}
		}
	);
};
PenjualanBebasAction.prototype.view_cancel_info = function(id) {
	var data = this.getRegulerData();
	data['id'] = id;
	data['super_command'] = "penjualan_bebas";
	data['command'] = "edit";
	showLoading();
	$.post(
		"",
		data,
		function(response) {
			var json = getContent(response);
			if (json == null) return;
			dismissLoading();
			if (json.header.keterangan_batal != "null")
				bootbox.alert("<b>Keterangan Pembatalan Penjualan Umum Bebas</b><br/>" + json.header.keterangan_batal);
			else
				bootbox.alert("<b>Keterangan Pembatalan Penjualan Umum Bebas</b><br/>&nbsp;");
		}
	);
};
PenjualanBebasAction.prototype.print_prescription = function(id) {
	if ($(".btn").attr("disabled") == "disabled")
		return;
	$(".btn").removeAttr("disabled");
	$(".btn").attr("disabled", "disabled");
	var self = this;
	var data = this.getRegulerData();
	data['super_command'] = "penjualan_bebas";
	data['command'] = "print_prescription";
	data['id'] = id;
	$.post(
		"",
		data,
		function(response) {
			var json = getContent(response);
			if (json == null) return;
			smis_print(json);
			// webprint(json);
			$(".btn").removeAttr("disabled");
		}
	);
};
PenjualanBebasAction.prototype.copy_header = function(id) {
	if ($(".btn").attr("disabled") == "disabled")
		return;
	$(".btn").removeAttr("disabled");
	$(".btn").attr("disabled", "disabled");
	var self = this;
	var data = this.getRegulerData();
	data['super_command'] = "penjualan_bebas";
	data['command'] = "edit";
	data['id'] = id;
	$.post(
		"",
		data,
		function(response) {
			var json = getContent(response);
			if (json == null) return;
			$("#help_bebas").hide();
			$("#penjualan_bebas_id").val("");
			$("#penjualan_bebas_ppn").val(json.header.ppn);
			$("#penjualan_bebas_nomor").val(json.header.nomor_resep);
			$("#penjualan_bebas_nomor").removeAttr("disabled");
			$("#penjualan_bebas_nomor").attr("disabled", "disabled");
			$("#penjualan_bebas_nama").val(json.header.nama_pasien);
			$("#penjualan_bebas_nama").removeAttr("disabled");
			$("#penjualan_bebas_nama").attr("disabled", "disabled");
			$("#penjualan_bebas_markup").val(json.header.markup);
			$("#penjualan_bebas_diskon").val((parseFloat(json.header.diskon)).formatMoney("2", ".", ","));
			$("#penjualan_bebas_diskon").removeAttr("disabled");
			$("#penjualan_bebas_diskon").attr("disabled", "disabled");
			$("#penjualan_bebas_t_diskon").val(json.header.t_diskon);
			$("#penjualan_bebas_t_diskon").removeAttr("disabled");
			$("#penjualan_bebas_t_diskon").attr("disabled", "disabled");
			$("#obat_jadi_add").show();
			$("tbody#dpenjualan_bebas_list").html("");
			dpenjualan_bebas_num = 0;
			$("#modal_alert_penjualan_bebas_add_form").html("");
			$(".error_field").removeClass("error_field");
			self.refreshBiayaTotal();
			$("#penjualan_bebas_save").removeAttr("onclick");
			$("#penjualan_bebas_save").attr("onclick", "penjualan_bebas.save()");
			$("#penjualan_bebas_save").show();
			$("#penjualan_bebas_ok").hide();
			$("#penjualan_bebas_add_form").smodal("show");
			$(".btn").removeAttr("disabled");
		}
	);
};
PenjualanBebasAction.prototype.print_all_prescription = function(id) {
	if ($(".btn").attr("disabled") == "disabled")
		return;
	$(".btn").removeAttr("disabled");
	$(".btn").attr("disabled", "disabled");
	var data = this.getRegulerData();
	data['super_command'] = "penjualan_bebas";
	data['command'] = "print_claim_prescription";
	data['id'] = id;
	$.post(
		"",
		data,
		function(response) {
			var json = getContent(response);
			if (json == null) return;
			smis_print(json);
			// webprint(json);
			$(".btn").removeAttr("disabled");
		}
	);
};