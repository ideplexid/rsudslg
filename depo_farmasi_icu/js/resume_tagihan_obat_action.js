function RTOPAction(name, page, action, column) {
	this.initialize(name, page, action, column);
}
RTOPAction.prototype.constructor = RTOPAction;
RTOPAction.prototype = new TableAction();
RTOPAction.prototype.view = function() {
	if ($("#rtop_noreg_pasien").val() == "")
		return;
	var self = this;
	var data = this.getRegulerData();
	data['command'] = "get_resume";
	data['noreg_pasien'] = $("#rtop_noreg_pasien").val();
	showLoading();
	$.post(
		"",
		data,
		function(response) {
			var json = JSON.parse(response);
			if (json == null) {
				dismissLoading();
				return;
			}
			$("#resume_list").html(json.body_html);
			$("#table_resume tfoot").remove();
			$("#table_resume").append("<tfoot>" + json.footer_html + "</tfoot>");
			dismissLoading();
		}
	);
};