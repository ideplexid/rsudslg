function DReturAction(name, page, action, column) {
	this.initialize(name, page, action, column);
}
DReturAction.prototype.constructor = DReturAction;
DReturAction.prototype = new TableAction();
DReturAction.prototype.edit = function(r_num) {
	var id = $("#obat_" + r_num + "_id").text();
	var nama_obat = $("#obat_" + r_num + "_nama_obat").text();
	var tanggal_exp = $("#obat_" + r_num + "_tanggal_exp").text();
	var jumlah = $("#obat_" + r_num + "_jumlah").text();
	var jumlah_retur = $("#obat_" + r_num + "_jumlah_retur").text();
	var satuan = $("#obat_" + r_num + "_satuan").text();
	$("#dretur_id").val(id);
	$("#dretur_nama_obat").val(nama_obat);
	$("#dretur_tanggal_exp").val(tanggal_exp);
	$("#dretur_jumlah").val(jumlah);
	$("#dretur_f_jumlah").val(jumlah + " " + satuan);
	$("#dretur_jumlah_retur").val(jumlah_retur);
	$("#dretur_satuan").val(satuan);
	$("#dretur_save").removeAttr("onclick");
	$("#dretur_save").attr("onclick", "dretur.update(" + r_num + ")");
	$("#dretur_add_form").smodal("show");
};
DReturAction.prototype.validate = function() {
	var valid = true;
	var invalid_msg = "";
	var jumlah = $("#dretur_jumlah").val();
	var jumlah_retur = $("#dretur_jumlah_retur").val();
	$(".error_field").removeClass("error_field");
	if (jumlah_retur == "") {
		valid = false;
		invalid_msg += "</br><strong>Jml. Retur</strong> tidak boleh kosong";
		$("#dretur_jumlah_retur").addClass("error_field");
	} else if (!is_numeric(jumlah_retur)) {
		valid = false;
		invalid_msg += "</br><strong>Jml. Retur</strong> seharusnya numerik (0-9)";
		$("#dretur_jumlah_retur").addClass("error_field");
	} else if (parseFloat(jumlah_retur) > parseFloat(jumlah)) {
		valid = false;
		invalid_msg += "</br><strong>Jml. Retur</strong> melebihi <strong>Jml. Awal</strong>";
		$("#dretur_jumlah_retur").addClass("error_field");
	}
	if (!valid) {
		$("#modal_alert_dretur_add_form").html(
			"<div class='alert alert-block alert-danger'>" +
				"<h4>Peringatan</h4>" +
				invalid_msg +
			"</div>"
		);
	}
	return valid;
};
DReturAction.prototype.update = function(r_num) {
	if (!this.validate()) {
		return;
	}
	var jumlah_retur = $("#dretur_jumlah_retur").val();
	var satuan = $("#dretur_satuan").val();
	$("#obat_" + r_num + "_jumlah_retur").text(jumlah_retur);
	$("#obat_" + r_num + "_f_retur").text(jumlah_retur + " " + satuan);
	$("#dretur_add_form").smodal("hide");
};