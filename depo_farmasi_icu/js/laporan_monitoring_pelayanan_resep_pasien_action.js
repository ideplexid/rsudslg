function LMPRPAction(name, page, action, column) {
	this.initialize(name, page, action, column);
}
LMPRPAction.prototype.constructor = LMPRPAction;
LMPRPAction.prototype = new TableAction();
LMPRPAction.prototype.getRegulerData = function() {
	var data = TableAction.prototype.getRegulerData.call(this);
	data['tanggal_from'] = $("#lmprp_tanggal_from").val();
	data['tanggal_to'] = $("#lmprp_tanggal_to").val();
	data['jenis_pasien'] = $("#lmprp_jenis_pasien").val();
	data['uri'] = $("#lmprp_uri").val();
	return data;
};
LMPRPAction.prototype.view = function() {
	if ($("#lmprp_tanggal_from").val() == "" || $("#lmprp_tanggal_to").val() == "")
		return;
	var self = this;
	FINISHED = false;
	$("#lmprp_info").empty();
	$("#table_lmprp tfoot").remove();
	$("#lmprp_list").empty();
	$("#lmprp_loading_bar").sload("true", "Harap ditunggu...", 0);
	$("#lmprp_loading_modal").smodal("show");
	var data = this.getRegulerData();
	data['command'] = "get_jumlah";
	$.post(
		"",
		data,
		function(response) {
			var json = JSON.parse(response);
			if (json == null) return;
			self.fillHtml(0, json.jumlah);
		}
	);
};
LMPRPAction.prototype.fillHtml = function(num, limit) {
	if (FINISHED || num == limit) {
		if (FINISHED == false && num == limit) {
			this.finalize();
		} else {
			$("#lmprp_loading_modal").smodal("hide");
			$("#lmprp_info").html(
				"<div class='alert alert-block alert-inverse'>" +
					 "<center><strong>PROSES DIBATALKAN</strong></center>" +
				 "</div>"
			);
			$("#lmprp_export_button").removeAttr("onclick");
		}
		return;
	}
	var self = this;
	var data = this.getRegulerData();
	data['command'] = "get_info";
	data['num'] = num;
	$.post(
		"",
		data,
		function(response) {
			var json = JSON.parse(response);
			if (json == null) return;
			$("tbody#lmprp_list").append(
				json.html
			);
			$("#lmprp_loading_bar").sload("true", json.nomor_transaksi + " - " + json.nomor_resep + " - " + json.nama_dokter + " - " + json.nama_pasien + " (" + (num + 1) + " / " + limit + ")", (num + 1) * 100 / limit - 1);
			self.fillHtml(num + 1, limit);
		}
	);
};
LMPRPAction.prototype.finalize = function() {
	var num_rows = $("tbody#lmprp_list tr").length;
	var total_duration = 0;
	var total_edukasi = 0;
	var total_response_time = 0;
	for (var i = 0; i < num_rows; i++) {
		$("tbody#lmprp_list tr:eq(" + i + ") td#lmprp_nomor").html("<small>" + (i + 1) + "</small>");
		var duration = parseFloat($("tbody#lmprp_list tr:eq(" + i + ") td#lmprp_duration").text());
		total_duration += duration;
		var edukasi = parseFloat($("tbody#lmprp_list tr:eq(" + i + ") td#lmprp_edukasi").text());
		total_edukasi += edukasi;
		var response_time = parseFloat($("tbody#lmprp_list tr:eq(" + i + ") td#lmprp_response_time").text());
		total_response_time += response_time;
	}
	var average_duration = total_duration / num_rows;
	var average_edukasi = total_edukasi / num_rows;
	var average_response_time = total_response_time / num_rows;
	$("#table_lmprp").append(
		"<tfoot>" +
			"<tr>" +
				"<td colspan='14'><small><strong><center>R A T A  -  R A T A</center></strong></small></td>" +
				"<td><small><div align='right'><strong>" + average_duration.formatMoney("0", ".", ",") + "</strong></div></small></td>" +
				"<td><small><div align='right'><strong>" + average_edukasi.formatMoney("0", ".", ",") + "</strong></div></small></td>" +
				"<td><small><div align='right'><strong>" + average_response_time.formatMoney("0", ".", ",") + "</strong></div></small></td>" +
			"</tr>" +
		"</tfoot>"
	);
	$("#lmprp_loading_modal").smodal("hide");
	$("#lmprp_info").html(
		"<div class='alert alert-block alert-info'>" +
			 "<center><strong>PROSES SELESAI</strong></center>" +
		 "</div>"
	);
	$("#lmprp_export_button").removeAttr("onclick");
	$("#lmprp_export_button").attr("onclick", "lmprp.export_xls()");
};
LMPRPAction.prototype.cancel = function() {
	FINISHED = true;
};
LMPRPAction.prototype.export_xls = function() {
	showLoading();
	var num_rows = $("#lmprp_list").children("tr").length;
	var d_data = {};
	for (var i = 0; i < num_rows; i++) {
		var nomor = $("tbody#lmprp_list tr:eq(" + i + ") td#lmprp_nomor").text();
		var nomor_transaksi = $("tbody#lmprp_list tr:eq(" + i + ") td#lmprp_id").text();
		var nomor_resep = $("tbody#lmprp_list tr:eq(" + i + ") td#lmprp_no_resep").text();
		var tanggal = $("tbody#lmprp_list tr:eq(" + i + ") td#lmprp_tanggal").text();
		var nama_dokter = $("tbody#lmprp_list tr:eq(" + i + ") td#lmprp_nama_dokter").text();
		var noreg_pasien = $("tbody#lmprp_list tr:eq(" + i + ") td#lmprp_noreg_pasien").text();
		var nrm_pasien = $("tbody#lmprp_list tr:eq(" + i + ") td#lmprp_nrm_pasien").text();
		var nama_pasien = $("tbody#lmprp_list tr:eq(" + i + ") td#lmprp_nama_pasien").text();
		var alamat_pasien = $("tbody#lmprp_list tr:eq(" + i + ") td#lmprp_alamat_pasien").text();
		var jenis = $("tbody#lmprp_list tr:eq(" + i + ") td#lmprp_jenis").text();
		var uri = $("tbody#lmprp_list tr:eq(" + i + ") td#lmprp_uri").text();
		var unit = $("tbody#lmprp_list tr:eq(" + i + ") td#lmprp_unit").text();
		var start_time = $("tbody#lmprp_list tr:eq(" + i + ") td#lmprp_start_time").text();
		var finish_time = $("tbody#lmprp_list tr:eq(" + i + ") td#lmprp_finish_time").text();
		var duration = $("tbody#lmprp_list tr:eq(" + i + ") td#lmprp_duration").text();
		var edukasi = $("tbody#lmprp_list tr:eq(" + i + ") td#lmprp_edukasi").text();
		var response_time = $("tbody#lmprp_list tr:eq(" + i + ") td#lmprp_response_time").text();
		d_data[i] = {
			"nomor" 			: nomor,
			"nomor_transaksi"	: nomor_transaksi,
			"nomor_resep"		: nomor_resep,
			"tanggal"			: tanggal,
			"nama_dokter"		: nama_dokter,
			"noreg_pasien"		: noreg_pasien,
			"nrm_pasien"		: nrm_pasien,
			"nama_pasien"		: nama_pasien,
			"alamat_pasien"		: alamat_pasien,
			"jenis"				: jenis,
			"uri"				: uri,
			"unit"				: unit,
			"start_time"		: start_time,
			"finish_time"		: finish_time,
			"duration"			: duration,
			"edukasi"			: edukasi,
			"response_time"		: response_time
		};
	}
	var data = this.getRegulerData();
	data['command'] = "export_xls";
	data['tanggal_from'] = $("#lmprp_tanggal_from").val();
	data['tanggal_to'] = $("#lmprp_tanggal_to").val();
	data['d_data'] = JSON.stringify(d_data);
	data['num_rows'] = num_rows;
	postForm(data);
	dismissLoading();
};