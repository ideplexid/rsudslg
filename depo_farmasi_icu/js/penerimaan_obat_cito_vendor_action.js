function VendorAction(name, page, action, column) {
	this.initialize(name, page, action, column);
}
VendorAction.prototype.constructor = VendorAction;
VendorAction.prototype = new TableAction();
VendorAction.prototype.selected = function(json) {
	$("#penerimaan_obat_cito_id_vendor").val(json.id);
	$("#penerimaan_obat_cito_nama_vendor").val(json.nama);
};