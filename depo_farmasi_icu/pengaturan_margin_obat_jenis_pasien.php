<?php
	require_once("depo_farmasi_icu/library/InventoryLibrary.php");
	require_once("smis-base/smis-include-service-consumer.php");
	global $db;

	$table = new Table(
		array("ID", "ID Obat", "Kode Obat", "Nama Obat", "Jenis Obat", "Slug J. Pasien", "Nama J. Pasien", "Perusahaan", "Asuransi", "IRJA/IRNA", "Margin Penjualan"),
		"Depo Farmasi ICU : Margin Jual Per Obat - Jenis Pasien"
	);
	$table->setName("pengaturan_margin_obat_jenis_pasien");

	$obat_table = new Table(
		array("No.", "ID Obat", "Kode Obat", "Nama Obat", "Jenis Obat"),
		"",
		null,
		true
	);
	$obat_table->setName("obat");
	$obat_table->setModel(Table::$SELECT);
	$obat_adapter = new SimpleAdapter(true, "No.");
	$obat_adapter->add("ID Obat", "id", "digit6");
	$obat_adapter->add("Kode Obat", "kode");
	$obat_adapter->add("Nama Obat", "nama");
	$obat_adapter->add("Jenis Obat", "nama_jenis_barang");
	$obat_service_responder = new ServiceResponder(
		$db,
		$obat_table,
		$obat_adapter,
		"get_daftar_barang_m"
	);
	$super_command = new SuperCommand();
	$super_command->addResponder("obat", $obat_service_responder);
	$init = $super_command->initialize();
	if ($init != null) {
		echo $init;
		return;
	}

	if (isset($_POST['command'])) {
		$adapter = new SimpleAdapter();
		$adapter->add("ID", "id", "digit6");
		$adapter->add("ID Obat", "id_obat", "digit6");
		$adapter->add("Kode Obat", "kode_obat");
		$adapter->add("Nama Obat", "nama_obat");
		$adapter->add("Jenis Obat", "nama_jenis_obat");
		$adapter->add("Slug J. Pasien", "slug");
		if (getSettings($db, "depo_farmasi4-resep-jenis_pasien_registration", 0) == 1) {
			$adapter->add("Nama J. Pasien", "slug", "unslug");
		} else {
			$adapter->add("Nama J. Pasien", "jenis_pasien");
		}
		$adapter->add("Asuransi", "asuransi");
		$adapter->add("Perusahaan", "perusahaan");
		$adapter->add("IRJA/IRNA", "uri", "trivial_1_IRNA_IRJA");
		$adapter->add("Margin Penjualan", "margin_jual", "money");
		$dbtable = new DBTable($db, InventoryLibrary::$_TBL_MARGIN_JUAL_OBAT_JENIS_PASIEN);
		$dbresponder = new DBResponder(
			$dbtable,
			$table,
			$adapter
		);
		$data = $dbresponder->command($_POST['command']);
		echo json_encode($data);
		return;
	}

	$modal = new Modal("pengaturan_margin_obat_jenis_pasien_add_form", "smis_form_container", "pengaturan_margin_obat_jenis_pasien");
	$modal->setTitle("Margin Jual Per Obat - Jenis Pasien");
	$id_hidden = new Hidden("pengaturan_margin_obat_jenis_pasien_id", "pengaturan_margin_obat_jenis_pasien_id", "");
	$modal->addElement("", $id_hidden);
	$id_obat_text = new Text("pengaturan_margin_obat_jenis_pasien_id_obat", "pengaturan_margin_obat_jenis_pasien_id_obat", "");
	$id_obat_text->setAtribute("disabled='disabled'");
	$modal->addElement("ID Obat", $id_obat_text);
	$kode_obat_text = new Text("pengaturan_margin_obat_jenis_pasien_kode_obat", "pengaturan_margin_obat_jenis_pasien_kode_obat", "");
	$kode_obat_text->setAtribute("disabled='disabled'");
	$modal->addElement("Kode Obat", $kode_obat_text);
	$nama_obat_text = new Text("pengaturan_margin_obat_jenis_pasien_nama_obat", "pengaturan_margin_obat_jenis_pasien_nama_obat", "");
	$nama_obat_text->setAtribute("disabled='disabled'");
	$nama_obat_text->setClass("smis-one-option-input");
	$button = new Button("", "", "Pilih");
	$button->setClass("btn-info");
	$button->setAction("obat.chooser('obat', 'obat_button', 'obat', obat)");
	$button->setIcon("fa fa-list");
	$button->setIsButton(Button::$ICONIC);
	$input_group = new InputGroup("");
	$input_group->addComponent($nama_obat_text);
	$input_group->addComponent($button);
	$modal->addElement("Nama Obat", $input_group);
	$nama_jenis_obat_text = new Text("pengaturan_margin_obat_jenis_pasien_nama_jenis_obat", "pengaturan_margin_obat_jenis_pasien_nama_jenis_obat", "");
	$nama_jenis_obat_text->setAtribute("disabled='disabled'");
	$modal->addElement("Jenis Obat", $nama_jenis_obat_text);
	if (getSettings($db, "depo_farmasi4-resep-jenis_pasien_registration", 0) == 1) {
		require_once("smis-base/smis-include-service-consumer.php");

		$jenis_pasien_service_consumer = new ServiceConsumer(
			$db,
			"get_jenis_patient",
			null,
			"registration"
		);
		$jenis_pasien_service_consumer->execute();
		$jenis_pasien_option = $jenis_pasien_service_consumer->getContent();

		$slug_select = new Select("pengaturan_margin_obat_jenis_pasien_slug", "pengaturan_margin_obat_jenis_pasien_slug", $jenis_pasien_option);
		$modal->addElement("Jenis Pasien", $slug_select);
		$jenis_pasien_hidden = new Hidden("pengaturan_margin_obat_jenis_pasien_jenis_pasien", "pengaturan_margin_obat_jenis_pasien_jenis_pasien", "");
		$modal->addElement("", $jenis_pasien_hidden);
	} else {
		$slug_text = new Text("pengaturan_margin_obat_jenis_pasien_slug", "pengaturan_margin_obat_jenis_pasien_slug", "");
		$modal->addElement("Slug J. Pasien", $slug_text);
		$jenis_pasien_text = new Text("pengaturan_margin_obat_jenis_pasien_jenis_pasien", "pengaturan_margin_obat_jenis_pasien_jenis_pasien", "");
		$modal->addElement("Jenis Pasien", $jenis_pasien_text);
	}
	$asuransi_option = new OptionBuilder();
	$asuransi_option->add("", "", 1);
	$service = new ServiceConsumer(
		$db, 
		"get_all_asuransi", 
		null, 
		"registration"
	);
    $content = $service->execute()->getContent();
    if ($content != null)
    	foreach ($content as $c)
    		$asuransi_option->add($c['nama'], $c['nama']);
    $asuransi_select = new Select("pengaturan_margin_obat_jenis_pasien_asuransi", "pengaturan_margin_obat_jenis_pasien_asuransi", $asuransi_option->getContent());
    $modal->addElement("Asuransi", $asuransi_select);
    $perusahaan_option = new OptionBuilder();
	$perusahaan_option->add("", "", 1);
	$service = new ServiceConsumer(
		$db, 
		"get_all_perusahaan", 
		null, 
		"registration"
	);
    $content = $service->execute()->getContent();
    if ($content != null)
    	foreach ($content as $c)
    		$perusahaan_option->add($c['nama'], $c['nama']);
    $perusahaan_select = new Select("pengaturan_margin_obat_jenis_pasien_perusahaan", "pengaturan_margin_obat_jenis_pasien_perusahaan", $perusahaan_option->getContent());
    $modal->addElement("Perusahaan", $perusahaan_select);
    $uri_option = new OptionBuilder();
    $uri_option->add("", "", "1");
    $uri_option->add("Rawat Jalan (IRJA)", "0");
    $uri_option->add("Rawat Inap (IRNA)", "1");
    $uri_select = new Select("pengaturan_margin_obat_jenis_pasien_uri", "pengaturan_margin_obat_jenis_pasien_uri", $uri_option->getContent());
    $modal->addElement("IRJA/IRNA", $uri_select);
	$margin_jual_text = new Text("pengaturan_margin_obat_jenis_pasien_margin_jual", "pengaturan_margin_obat_jenis_pasien_margin_jual", "");
	$modal->addElement("Margin Jual", $margin_jual_text);
	$button = new Button("", "", "Simpan");
	$button->setClass("btn-success");
	$button->setAction("pengaturan_margin_obat_jenis_pasien.save()");
	$button->setIcon("fa fa-floppy-o");
	$button->setIsButton(Button::$ICONIC);
	$modal->addFooter($button);

	echo $modal->getHtml();
	echo $table->getHtml();
	echo addJS("framework/smis/js/table_action.js");
	echo addJS("depo_farmasi_icu/js/pengaturan_margin_obat_jenis_pasien_obat_action.js", false);
	echo addJS("depo_farmasi_icu/js/pengaturan_margin_obat_jenis_pasien.js", false);
?>