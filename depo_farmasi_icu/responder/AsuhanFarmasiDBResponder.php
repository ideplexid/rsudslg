<?php
	require_once("smis-base/smis-include-service-consumer.php");
	
	class AsuhanFarmasiDBResponder extends DBResponder {
		protected $message_failure;
		
		public function command($command) {
			if ($command != "check_closing_bill") {
				return parent::command($command);
			}	
			$pack = null;
			if ($command == "check_closing_bill") {
				$pack = new ResponsePackage();
				$result = $this->checkClosingBill();
	            if ($result == "1") {
	                $pack->setAlertVisible(true);
				    $pack->setAlertContent("Peringatan", "<strong>Tagihan Pasien Sudah Ditutup</strong>, </br>Tidak dapat melakukan perubahan tagihan pada pasien. Silakan menghubungi pihak Kasir.",Alert::$DANGER);
	            }
				$pack->setContent($result);
				$pack->setStatus(ResponsePackage::$STATUS_OK);
			}
			return $pack->getPackage();
		}

		public function checkClosingBill() {
	        if (getSettings($this->dbtable->get_db(), "depo_farmasi4-lock_tagihan", "0") == "0") {
	            return "0";
	        }
	        $service = new ServiceConsumer($this->getDBTable()->get_db(), "cek_tutup_tagihan", null, "registration");
	        $service->addData("noreg_pasien", $_POST['noreg_pasien']);
	        $service->execute();
	        $result = $service->getContent();
	        return $result;
	    }
	}
?>