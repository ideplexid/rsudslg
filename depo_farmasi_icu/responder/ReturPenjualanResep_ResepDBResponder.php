<?php
	require_once("depo_farmasi_icu/library/InventoryLibrary.php");
	
	class ResepDBResponder extends DBResponder {
		public function edit() {
			$id = $_POST['id'];
			$data['header'] = $this->dbtable->get_row("
				SELECT *
				FROM " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . "
				WHERE id = '" . $id . "'
			");
			$uri = 0;
			if ($data['header'] != null)
				$uri = $data['header']->uri;
			$detail_list = "";
			$stok_pakai_obat_jadi_rows = $this->dbtable->get_result("
				SELECT " . InventoryLibrary::$_TBL_STOK_PAKAI_OBAT_JADI . ".id_stok_obat, " . InventoryLibrary::$_TBL_STOK_OBAT . ".id_obat, " . InventoryLibrary::$_TBL_STOK_OBAT . ".nama_obat, " . InventoryLibrary::$_TBL_STOK_OBAT . ".nama_jenis_obat, " . InventoryLibrary::$_TBL_STOK_PAKAI_OBAT_JADI . ".jumlah, " . InventoryLibrary::$_TBL_STOK_OBAT . ".satuan, " . InventoryLibrary::$_TBL_STOK_OBAT . ".konversi, " . InventoryLibrary::$_TBL_STOK_OBAT . ".satuan_konversi, " . InventoryLibrary::$_TBL_PENJUALAN_OBAT_JADI . ".harga, " . InventoryLibrary::$_TBL_PENJUALAN_OBAT_JADI . ".markup, " . InventoryLibrary::$_TBL_STOK_OBAT . ".tanggal_exp
				FROM (" . InventoryLibrary::$_TBL_STOK_PAKAI_OBAT_JADI . " LEFT JOIN " . InventoryLibrary::$_TBL_PENJUALAN_OBAT_JADI . " ON " . InventoryLibrary::$_TBL_STOK_PAKAI_OBAT_JADI . ".id_penjualan_obat_jadi = " . InventoryLibrary::$_TBL_PENJUALAN_OBAT_JADI . ".id) LEFT JOIN " . InventoryLibrary::$_TBL_STOK_OBAT . " ON " . InventoryLibrary::$_TBL_STOK_PAKAI_OBAT_JADI . ".id_stok_obat = " . InventoryLibrary::$_TBL_STOK_OBAT . ".id
				WHERE " . InventoryLibrary::$_TBL_PENJUALAN_OBAT_JADI . ".id_penjualan_resep = '" . $id . "' AND " . InventoryLibrary::$_TBL_STOK_PAKAI_OBAT_JADI . ".prop NOT LIKE 'del' AND " . InventoryLibrary::$_TBL_STOK_PAKAI_OBAT_JADI . ".jumlah > 0
			");
			$row_id = 0;
			foreach($stok_pakai_obat_jadi_rows as $spoj) {
				$diskon = 0;
				$t_diskon = $data['header']->t_diskon;
				if ($t_diskon == "persen" || $t_diskon == "gratis") {
					$diskon = $data['header']->diskon;
					$diskon = ($diskon * $spoj->harga) / 100;
				} else {
					$diskon = $data['header']->diskon;
					$diskon = ($diskon * $spoj->harga * $spoj->jumlah) / ($data['header']->total + $diskon);
					$diskon = $diskon / $spoj->jumlah;
				}
				$harga = $spoj->harga - $diskon;
				if ($uri == 0) {
					if (getSettings($this->dbtable->get_db(), "depo_farmasi4-retur_penjualan-irja_harga_dasar", "hja") == "hpp")
						$harga = $harga / (1 + $spoj->markup / 100);
				} else if ($uri == 1) {
					if (getSettings($this->dbtable->get_db(), "depo_farmasi4-retur_penjualan-irna_harga_dasar", "hja") == "hpp")
						$harga = $harga / (1 + $spoj->markup / 100);
				}				
				$detail_list .= "<tr id='obat_" . $row_id . "'>" .
									"<td id='obat_" . $row_id . "_id' style='display: none;'></td>" .
									"<td id='obat_" . $row_id . "_id_stok_obat' style='display: none;'>" . $spoj->id_stok_obat . "</td>" .
									"<td id='obat_" . $row_id . "_id_obat' style='display: none;'>" . $spoj->id_obat . "</td>" .
									"<td id='obat_" . $row_id . "_nama_jenis_obat' style='display: none;'>" . $spoj->nama_jenis_obat . "</td>" .
									"<td id='obat_" . $row_id . "_harga' style='display: none;'>" . $harga . "</td>" .
									"<td id='obat_" . $row_id . "_markup' style='display: none;'>" . $spoj->markup . "</td>" .
									"<td id='obat_" . $row_id . "_jumlah' style='display: none;'>" . $spoj->jumlah . "</td>" .
									"<td id='obat_" . $row_id . "_jumlah_retur' style='display: none;'>0</td>" .
									"<td id='obat_" . $row_id . "_satuan' style='display: none;'>" . $spoj->satuan . "</td>" .
									"<td id='obat_" . $row_id . "_konversi' style='display: none;'>" . $spoj->konversi . "</td>" .
									"<td id='obat_" . $row_id . "_satuan_konversi' style='display: none;'>" . $spoj->satuan_konversi . "</td>" .
									"<td id='obat_" . $row_id . "_nama_obat'>" . $spoj->nama_obat . "</td>" .
									"<td id='obat_" . $row_id . "_tanggal_exp'>" . ArrayAdapter::format("date d-m-Y", $spoj->tanggal_exp) . "</td>" .
									"<td id='obat_" . $row_id . "_f_subtotal'>" . $spoj->jumlah . " x " . "Rp. " . number_format($harga, 2, ",", ".") . " = " . "Rp. " . number_format($spoj->jumlah * $harga, 2, ",", ".") . "</td>" .
									"<td id='obat_" . $row_id . "_f_jumlah'>" . $spoj->jumlah . " " . $spoj->satuan . "</td>" .
									"<td id='obat_" . $row_id . "_f_retur'>0 " . $spoj->satuan . "</td>" .
									"<td>" .
										"<div class='btn-group noprint'>" .
											"<a href='#' onclick='dretur.edit(" . $row_id . ")' data-content='Ubah' data-toggle='popover' class='input btn btn-warning'>" . 
												"<i class='icon-edit icon-white'></i>" .
											"</a>" .
										"</div>" .
									"</td>" .
								"</tr>";
				$row_id++;
			}
			$data['detail_list'] = $detail_list;
			return $data;
		}
	}
?>