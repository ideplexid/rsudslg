<?php
	require_once("smis-libs-inventory/service/isInventory.php");
	global $db;

	$is_inventory_service = new IsInventory($db, "Depo Farmasi ICU", "depo_farmasi_icu");
	$is_inventory_service->setAlways(true);
	$is_inventory_service->setData("1", "1");
	$is_inventory_service->initialize();
?>