<?php
	require_once("depo_farmasi_icu/library/InventoryLibrary.php");
	global $db;
	if (isset($_POST['noreg_pasien'])) {
		$noreg_pasien = $_POST['noreg_pasien'];
		$total = 0;
		$dbtable = new DBTable($db, InventoryLibrary::$_TBL_PENJUALAN_RESEP);
		$row = $dbtable->get_row("
			SELECT SUM(total) AS 'total'
			FROM " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . "
			WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop NOT LIKE 'del' AND dibatalkan = '0'
		");
		if ($row != null)
			$total = $row->total;

		$retur_rows = $dbtable->get_result("
			SELECT a.*
			FROM " . InventoryLibrary::$_TBL_RETUR_PENJUALAN_RESEP . " a LEFT JOIN " . InventoryLibrary::$_TBL_DRETUR_PENJUALAN_RESEP . " b ON a.id_penjualan_resep = b.id
			WHERE a.prop NOT LIKE 'del' AND b.prop NOT LIKE 'del' AND b.dibatalkan = '0' AND b.noreg_pasien = '" . $noreg_pasien . "'
		");
		if ($retur_rows != null) {
			foreach ($retur_rows as $rr) {
				$dretur_rows = $dbtable->get_result("
					SELECT *
					FROM smis_frm_dretur_penjualan_resep
					WHERE id_retur_penjualan_resep = '" . $rr->id . "' AND prop NOT LIKE 'del'
				");
				if ($dretur_rows != null) {
					foreach ($dretur_rows as $dr)
						$total -= $dr->jumlah * $dr->harga * $rr->persentase_retur / 100;
				}
			}
		}
		$row = $dbtable->get_row("
			SELECT SUM(biaya) AS 'total'
			FROM " . InventoryLibrary::$_TBL_ASUHAN_FARMASI . "
			WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop = ''
		");
		if ($row != null)
			$total += $row->total;
		echo json_encode($total);
	}
?>