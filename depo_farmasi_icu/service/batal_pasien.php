<?php 
	require_once("depo_farmasi_icu/library/InventoryLibrary.php");
	global $db;

	$noreg_pasien = $_POST['noreg_pasien'];
	$dbtable = new DBTable($db, InventoryLibrary::$_TBL_PENJUALAN_RESEP);
	$dbtable->update(
		array(
			"batal_berobat" 	=> 1
		),
		array(
			"noreg_pasien"		=> $noreg_pasien
		)
    );

    $dbtable = new DBTable($db, InventoryLibrary::$_TBL_ASUHAN_FARMASI);
    $dbtable->update(
        array(
            "prop"     => 'del'
        ),
        array(
            "noreg_pasien"        => $noreg_pasien
        )
    );
	echo json_encode(1);
?>