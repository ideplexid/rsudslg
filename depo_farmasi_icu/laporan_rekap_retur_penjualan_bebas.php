<?php
	require_once("depo_farmasi_icu/library/InventoryLibrary.php");
	require_once("smis-base/smis-include-service-consumer.php");
	global $db;

	$form = new Form("", "", "Depo Farmasi ICU : Laporan Rekap Retur Penjualan Bebas");
	$tanggal_from_text = new Text("rrpb_tanggal_from", "rrpb_tanggal_from", date("Y-m-d 00:00"));
	$tanggal_from_text->setClass("mydatetime");
	$tanggal_from_text->setAtribute("data-date-format='yyyy-mm-dd hh:ii'");
	$form->addElement("Waktu Awal", $tanggal_from_text);
	$tanggal_to_text = new Text("rrpb_tanggal_to", "rrpb_tanggal_to", date("Y-m-d H:i"));
	$tanggal_to_text->setClass("mydatetime");
	$tanggal_to_text->setAtribute("data-date-format='yyyy-mm-dd hh:ii'");
	$form->addElement("Waktu Akhir", $tanggal_to_text);
	$show_button = new Button("", "", "Tampilkan");
	$show_button->setClass("btn-primary");
	$show_button->setIcon("icon-white icon-repeat");
	$show_button->setIsButton(Button::$ICONIC);
	$show_button->setAction("rrpb.view()");
	$download_button = new Button("", "", "Eksport XLS");
	$download_button->setClass("btn-inverse");
	$download_button->setIcon("fa fa-download");
	$download_button->setIsButton(Button::$ICONIC);
	$download_button->setAtribute("id='rrpb_export_button'");
	$btn_group = new ButtonGroup("noprint");
	$btn_group->addButton($show_button);
	$btn_group->addButton($download_button);
	$form->addElement("", $btn_group);

	$table = new Table(
		array("No.", "No. Trans.", "No. Resep", "Tanggal/Jam", "Nama", "Total (Rp.)"),
		"",
		null,
		true
	);
	$table->setName("rrpb");
	$table->setAction(false);
	$table->setFooterVisible(false);

	if (isset($_POST['command'])) {
		$dbtable = new DBTable($db, InventoryLibrary::$_TBL_RETUR_PENJUALAN_RESEP);
		if ($_POST['command'] == "get_jumlah") {
			$tanggal_from = $_POST['tanggal_from'];
			$tanggal_to = $_POST['tanggal_to'];
			$row = $dbtable->get_row("
				SELECT COUNT(a.id) AS 'jumlah'
				FROM " . InventoryLibrary::$_TBL_RETUR_PENJUALAN_RESEP . " a INNER JOIN " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . " b ON a.id_penjualan_resep = b.id
				WHERE a.tanggal >= '" . $tanggal_from . "' AND a.tanggal <= '" . $tanggal_to . "' AND a.prop NOT LIKE 'del' AND (b.tipe LIKE 'bebas')
			");
			$data = array(
				"jumlah"	=> $row->jumlah
			);
			echo json_encode($data);
		} else if ($_POST['command'] == "get_info") {
			$tanggal_from = $_POST['tanggal_from'];
			$tanggal_to = $_POST['tanggal_to'];
			$num = $_POST['num'];
			$row = $dbtable->get_row("
				SELECT a.*, b.nomor_resep, b.noreg_pasien, b.nrm_pasien, b.nama_pasien, b.alamat_pasien, b.jenis, b.id_dokter, b.nama_dokter, b.uri, b.ruangan, SUM(c.jumlah * c.harga) AS 'total'
				FROM (" . InventoryLibrary::$_TBL_RETUR_PENJUALAN_RESEP . " a INNER JOIN " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . " b ON a.id_penjualan_resep = b.id) INNER JOIN " . InventoryLibrary::$_TBL_DRETUR_PENJUALAN_RESEP . " c ON a.id = c.id_retur_penjualan_resep
				WHERE a.tanggal >= '" . $tanggal_from . "' AND a.tanggal <= '" . $tanggal_to . "' AND a.prop NOT LIKE 'del' AND (b.tipe LIKE 'bebas')
				GROUP BY a.id
				LIMIT " . $num . ", 1
			");
			$html = "";
			if ($row != null) {
				$uri = $row->uri == 1 ? "RAWAT INAP" : "RAWAT JALAN";
				$total = $row->total * $row->persentase_retur / 100;
				$html = "
					<tr>
						<td id='rrpb_nomor'></td>
						<td id='rrpb_id'><small>" . ArrayAdapter::format("only-digit8", $row->id) . "</small></td>
						<td id='rrpb_no_resep'><small>" . $row->nomor_resep . "</small></td>
						<td id='rrpb_tanggal'><small>" . ArrayAdapter::format("date d-m-Y, H:i:s", $row->tanggal) . "</small></td>
						<td id='rrpb_nama_pasien'><small>" . $row->nama_pasien . "</small></td>
						<td id='rrpb_total' style='display: none;'>" . $total . "</td>
						<td id='rrpb_f_total'><small><div align='right'>" . ArrayAdapter::format("only-money", $total) . "</div></small></td>
					</tr>
				";
			}
			$data = array(
				"nomor_transaksi"	=> ArrayAdapter::format("only-digit8", $row->id),
				"nomor_resep"		=> $row->nomor_resep,
				"nama_pasien"		=> $row->nama_pasien,
				"html"				=> $html
			);
			echo json_encode($data);
		} else if ($_POST['command'] == "export_xls") {
			ini_set("memory_limit", "256M");
			set_time_limit(0);
			$tanggal_from = $_POST['tanggal_from'];
			$tanggal_to = $_POST['tanggal_to'];
			require_once("smis-libs-out/php-excel/PHPExcel.php");
			$objPHPExcel = PHPExcel_IOFactory::load("depo_farmasi_icu/templates/template_rekap_retur_penjualan_bebas.xlsx");
			$objPHPExcel->setActiveSheetIndexByName("REKAP RETUR PENJUALAN BEBAS");
			$objWorksheet = $objPHPExcel->getActiveSheet();
			$objWorksheet->setCellValue("B3", "PERIODE : " . ArrayAdapter::format("date d-m-Y, H:i", $tanggal_from) . " s/d " . ArrayAdapter::format("date d-m-Y, H:i", $tanggal_to));
			$data = json_decode($_POST['d_data']);
			$objWorksheet->getStyle("G9")->getNumberFormat()->setFormatCode("#,##0.00");
			if ($_POST['num_rows'] - 2 > 0)
				$objWorksheet->insertNewRowBefore(8, $_POST['num_rows'] - 2);
			$start_row_num = 7;
			$end_row_num = 7;
			$row_num = $start_row_num;
			foreach ($data as $d) {
				$col_num = 1;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->nomor);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->nomor_transaksi);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->nomor_resep);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->tanggal);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->nama_pasien);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->total);
				$objWorksheet->getStyle("G" . $row_num)->getNumberFormat()->setFormatCode("#,##0.00");
				$row_num++;
				$end_row_num++;
				$no++;
			}
			header("Content-type: application/vnd.ms-excel");	
			header("Content-Disposition: attachment; filename=R2_REKAP_RETUR_PENJUALAN_BEBAS_" . ArrayAdapter::format("date Ymd_Hi", $tanggal_from) . "_" . ArrayAdapter::format("date Ymd_Hi", $tanggal_to) . ".xlsx");
			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");
			$objWriter->save("php://output");
		}
		return;
	}
	$loading_bar = new LoadingBar("rrpb_loading_bar", "");
	$button = new Button("", "", "Batal");
	$button->addClass("btn-primary");
	$button->setIsButton(Button::$ICONIC_TEXT);
	$button->setIcon("fa fa-close");
	$button->setAction("rrpb.cancel()");
	$loading_modal = new Modal("rrpb_loading_modal", "", "Proses..");
	$loading_modal->addHtml($loading_bar->getHtml(), "after");
	$loading_modal->addFooter($button);

	echo $loading_modal->getHtml();
	echo $form->getHtml();
	echo "<div id='table_content'>";
	echo $table->getHtml();
	echo "</div>";
	echo "<div id='rrpb_info'></div>";
	echo addJS("framework/smis/js/table_action.js");
	echo addJS("framework/bootstrap/js/bootstrap-datetimepicker.js");
	echo addCSS("framework/bootstrap/css/bootstrap-datetimepicker.css");
	echo addJS("depo_farmasi_icu/js/laporan_rekap_retur_penjualan_bebas_action.js", false);
	echo addJS("depo_farmasi_icu/js/laporan_rekap_retur_penjualan_bebas.js", false);
?>