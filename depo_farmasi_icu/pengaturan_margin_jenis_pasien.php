<?php 
	require_once("depo_farmasi_icu/library/InventoryLibrary.php");
	global $db;

	$table = new Table(
		array("ID", "Slug", "Jenis Pasien", "Margin Penjualan"),
		"Depo Farmasi ICU : Margin Jual Per Jenis Pasien",
		null,
		true
	);
	$table->setName("margin_jual");

	if (isset($_POST['command'])) {
		$adapter = new SimpleAdapter();
		$adapter->add("ID", "id");
		$adapter->add("Slug", "slug");
		if (getSettings($db, "depo_farmasi4-resep-jenis_pasien_registration", 0) == 0)
			$adapter->add("Jenis Pasien", "jenis_pasien");
		else
			$adapter->add("Jenis Pasien", "slug", "unslug");
		$adapter->add("Margin Penjualan", "margin_jual");
		$columns = array("id", "slug", "jenis_pasien", "margin_jual");
		if (getSettings($db, "depo_farmasi4-resep-jenis_pasien_registration", 0) == 1)
			$columns = array("id", "slug", "margin_jual");
		$dbtable = new DBTable(
			$db,
			InventoryLibrary::$_TBL_MARGIN_JUAL_JENIS_PASIEN,
			$columns
		);
		$dbtable->setOrder(" id DESC ");
		$dbresponder = new DBResponder(
			$dbtable,
			$table,
			$adapter
		);
		$data = $dbresponder->command($_POST['command']);
		echo json_encode($data);
		return;
	}
	$columns = array("id", "slug", "jenis_pasien", "margin_jual");
	$names = array("", "Slug", "Jenis Pasien", "Margin Jual");
	$values = array("", "", "", "0");
	$types = array("hidden", "text", "text", "text");
	$emptiness = array("y", "n", "n", "n");
	if (getSettings($db, "depo_farmasi4-resep-jenis_pasien_registration", 0) == 1) {
		require_once("smis-base/smis-include-service-consumer.php");

		$jenis_pasien_service_consumer = new ServiceConsumer(
			$db,
			"get_jenis_patient",
			null,
			"registration"
		);
		$jenis_pasien_service_consumer->execute();
		$jenis_pasien_option = $jenis_pasien_service_consumer->getContent();

		$columns = array("id", "slug", "margin_jual");
		$names = array("", "Jenis Pasien", "Margin Jual");
		$values = array("", $jenis_pasien_option, "0");
		$types = array("hidden", "select", "text");
		$emptiness = array("y", "n", "n");
	}
	$table->setModal(
		$columns,
		$types,
		$names,
		$values,
		$emptiness
	);
	$modal = $table->getModal();
	$modal->setTitle("Data Margin Per Jenis Pasien");
	
	echo $modal->getHtml();
	echo $table->getHtml();
	echo addJS("framework/smis/js/table_action.js");
	echo addJS("depo_farmasi_icu/js/pengaturan_margin_jenis_pasien.js", false);
?>