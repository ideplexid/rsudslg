<?php
	global $db;

	$laporan_tabulator = new Tabulator("laporan_rincian_retur_penjualan", "", Tabulator::$POTRAIT);
	$laporan_tabulator->add("laporan_rincian_retur_penjualan_resep", "Lap. Detail Retur Penjualan Resep", "depo_farmasi_icu/laporan_rincian_retur_penjualan_resep.php", Tabulator::$TYPE_INCLUDE);
	$laporan_tabulator->add("laporan_rincian_retur_penjualan_bebas", "Lap. Detail Retur Penjualan Bebas", "depo_farmasi_icu/laporan_rincian_retur_penjualan_bebas.php", Tabulator::$TYPE_INCLUDE);
	echo $laporan_tabulator->getHtml();
?>