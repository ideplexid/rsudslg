<?php
	global $db;

	$laporan_tabulator = new Tabulator("laporan_rekap_penjualan", "", Tabulator::$POTRAIT);
	$laporan_tabulator->add("laporan_rekap_penjualan_resep", "Lap. Rekap Penjualan Resep", "depo_farmasi_icu/laporan_rekap_penjualan_resep.php", Tabulator::$TYPE_INCLUDE);
	$laporan_tabulator->add("laporan_rekap_penjualan_bebas", "Lap. Rekap Penjualan Bebas", "depo_farmasi_icu/laporan_rekap_penjualan_bebas.php", Tabulator::$TYPE_INCLUDE);
	$laporan_tabulator->add("laporan_rekap_penjualan_up", "Lap. Rekap Penjualan UP", "depo_farmasi_icu/laporan_rekap_penjualan_up.php", Tabulator::$TYPE_INCLUDE);
	echo $laporan_tabulator->getHtml();
?>