<?php
    require_once("depo_farmasi_icu/table/ReturObatPerPasienTable.php");
    require_once("depo_farmasi_icu/responder/ROPPPasienDBResponder.php");
    require_once("depo_farmasi_icu/responder/ReturObatPerPasienDBResponder.php");
    require_once("depo_farmasi_icu/library/InventoryLibrary.php");
    global $db;

    $pasien_table = new Table(
        array("No.", "NRM", "No. Reg.", "Pasien", "Alamat")
    );
    $pasien_table->setName("pasien");
    $pasien_table->setModel(Table::$SELECT);
    $pasien_adapter = new SimpleAdapter(true, "No.");
    $pasien_adapter->add("NRM", "nrm_pasien", "digit6");
    $pasien_adapter->add("No. Reg.", "id", "digit8");
    $pasien_adapter->add("Pasien", "nama_pasien");
    $pasien_adapter->add("Alamat", "alamat_pasien");
    $pasien_dbtable = new DBTable($db, InventoryLibrary::$_TBL_PENJUALAN_RESEP);
    $pasien_dbtable->setViewForSelect(true);
    $filter = "a.dibatalkan = 0 AND a.prop = '' AND a.tipe = 'resep' AND a.diretur = '0' AND b.selesai <> 1";
    if (isset($_POST['kriteria'])) {
        $filter .= " AND (a.nrm_pasien LIKE '%" . $_POST['kriteria'] . "%' OR a.noreg_pasien LIKE '%" . $_POST['kriteria'] . "%' OR a.nama_pasien LIKE '%" . $_POST['kriteria'] . "%' OR a.alamat_pasien LIKE '%" . $_POST['kriteria'] . "%') ";
    }
    $query_value = "
        SELECT
            DISTINCT a.noreg_pasien id, a.nrm_pasien, a.nama_pasien, a.alamat_pasien
        FROM
            " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . " a LEFT JOIN smis_rg_layananpasien b ON a.noreg_pasien = b.id
        WHERE
            " . $filter . "
        ORDER BY 
            a.noreg_pasien DESC
	";
    $query_count = "
        SELECT 
            COUNT(*)
        FROM 
            (" . $query_value . ") v_obat
	";
    $pasien_dbtable->setPreferredQuery(true, $query_value, $query_count);
    $pasien_dbresponder = new ROPPPasienDBResponder(
        $pasien_dbtable,
        $pasien_table,
        $pasien_adapter
    );

    $super_command = new SuperCommand();
    $super_command->addResponder("pasien", $pasien_dbresponder);
    $init = $super_command->initialize();
    if ($init != null) {
        echo $init;
        return;
    }

    $table = new ReturObatPerPasienTable(
        array("No.", "Tanggal/Jam", "NRM", "No. Reg.", "Pasien", "Dokter", "Status"),
        "Depo Farmasi ICU : Retur Obat Per Pasien",
        null,
        true
    );
    $table->setName("retur_obat_per_pasien");
    $table->setPrintButtonEnable(false);
    $table->setReloadButtonEnable(false);
    $table->setEditButtonEnable(false);
    $table->setDelButtonEnable(false);

    if (isset($_POST['command'])) {
        $adapter = new SimpleAdapter(true, "No.");
        $adapter->add("id", "id");
        $adapter->add("dibatalkan", "dibatalkan");
        $adapter->add("Tanggal/Jam", "tanggal", "date d-m-Y, H:i");
        $adapter->add("NRM", "nrm_pasien", "digit6");
        $adapter->add("No. Reg.", "noreg_pasien", "digit6");
        $adapter->add("Pasien", "nama_pasien");
        $adapter->add("Dokter", "daftar_dokter");
        $adapter->add("Status", "dibatalkan", "trivial_0_-_Dibatalkan");

        $dbtable = new DBTable($db, InventoryLibrary::$_TBL_RETUR_PENJUALAN_RESEP);
        $filter = "";
        if (isset($_POST['kriteria'])) {
            $filter .= " AND (a.nrm_pasien LIKE '%" . $_POST['kriteria'] . "%' OR a.noreg_pasien LIKE '%" . $_POST['kriteria'] . "%' OR a.nama_pasien LIKE '%" . $_POST['kriteria'] . "%' OR c.nama_dokter LIKE '%" . $_POST['kriteria'] . "%') ";
        }
        $query_value = "
            SELECT
                a.id, a.tanggal, a.nrm_pasien, a.noreg_pasien, a.nama_pasien, a.dibatalkan, GROUP_CONCAT(DISTINCT c.nama_dokter SEPARATOR '<br/>') daftar_dokter
            FROM
                " . InventoryLibrary::$_TBL_RETUR_PENJUALAN_RESEP . " a 
                    LEFT JOIN " . InventoryLibrary::$_TBL_DRETUR_PENJUALAN_RESEP . " b ON a.id = b.id_retur_penjualan_resep
                    LEFT JOIN " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . " c ON b.id_penjualan_resep = c.id
            WHERE
                a.prop = ''
                    AND b.prop = ''
                    AND c.tipe = 'resep'
                    " . $filter . "
            GROUP BY
                a.id
            ORDER BY
                a.tanggal DESC
        ";
        $query_count = "
            SELECT
                COUNT(*)
            FROM
                (" . $query_value . ") v
        ";
        $dbtable->setPreferredQuery(true, $query_value, $query_count);

        $dbresponder = new ReturObatPerPasienDBResponder(
            $dbtable,
            $table,
            $adapter
        );
        $data = $dbresponder->command($_POST['command']);

        /// Push Data Tagihan ke Kasir Ketika Penambahan Data Baru atau Pembatalan Data Lama :
        if (isset($data['content']['success']) && $data['content']['success'] == 1 && getSettings($db, "cashier-real-time-tagihan", "0") != 0) {
            $mode_data_tagihan = getSettings($db, "depo_farmasi4-service-get_tagihan", "get_simple_tagihan.php") == "get_simple_tagihan.php" ? "simple" : "detail";
            if ($_POST['command'] == "save" && $data['content']['type'] == "insert") {
                $id_retur_penjualan_resep = $data['content']['id'];

                $resep_retur_rows = $db->get_result("
                    SELECT 
                        DISTINCT
                            a.id id_retur_penjualan_resep, 
                            a.tanggal,
                            b.id_penjualan_resep,
                            c.nomor_resep,
                            c.nama_dokter,
                            c.id_dokter, 
                            c.uri,
                            a.noreg_pasien, 
                            a.nrm_pasien, 
                            a.nama_pasien
                    FROM
                        " . InventoryLibrary::$_TBL_RETUR_PENJUALAN_RESEP ." a 
                            INNER JOIN " . InventoryLibrary::$_TBL_DRETUR_PENJUALAN_RESEP . " b ON a.id = b.id_retur_penjualan_resep
                            INNER JOIN " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . " c ON b.id_penjualan_resep = c.id
                    WHERE
                        a.id = '" . $id_retur_penjualan_resep . "' 
                            AND b.prop = '';
                ");
                if ($resep_retur_rows != null) {
                    foreach ($resep_retur_rows as $resep_retur) {
                        $tanggal = ArrayAdapter::format("date d M Y", $resep_retur->tanggal);
                        $timestamp = $resep_retur->tanggal;
                        $nama_dokter = $resep_retur->nama_dokter;
                        $prop = "";

                        $data_tagihan = array(
                            'grup_name'		=> "return_resep",
                            'entity'		=> "depo_farmasi_icu",
                            'nama_pasien'	=> $resep_retur->nama_pasien,
                            'nrm_pasien'	=> $resep_retur->nrm_pasien,
                            'noreg_pasien'	=> $resep_retur->noreg_pasien
                        );
                        $list = array();
                        $detail_rows = $db->get_result("
                            SELECT 
                                b.id id_dretur_penjualan_resep,
                                d.nama_obat,
                                d.satuan,
                                a.persentase_retur,
                                b.jumlah, 
                                b.harga, 
                                b.subtotal
                            FROM 
                                " . InventoryLibrary::$_TBL_RETUR_PENJUALAN_RESEP . " a 
                                    INNER JOIN " . InventoryLibrary::$_TBL_DRETUR_PENJUALAN_RESEP . " b ON a.id = b.id_retur_penjualan_resep
                                    INNER JOIN " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . " c ON b.id_penjualan_resep = c.id
                                    INNER JOIN " . InventoryLibrary::$_TBL_STOK_OBAT . " d ON b.id_stok_obat = d.id
                            WHERE 
                                b.prop = '' 
                                    AND a.id = '" . $resep_retur->id_retur_penjualan_resep . "' 
                                    AND b.id_penjualan_resep = '" . $resep_retur->id_penjualan_resep . "'
                        ");
                        if ($detail_rows != null) {
                            if ($mode_data_tagihan == "simple") {
                                $nilai_retur = 0;
                                $keterangan = "Return Obat Per Pasien ID " . $resep_retur->id_retur_penjualan_resep . " - Resep No. " . $resep_retur->nomor_resep . " : ";
                                foreach ($detail_rows as $detail) {
                                    $nilai_retur -= $detail->jumlah * $detail->harga * $detail->persentase_retur / 100;
                                    $keterangan .= $detail->nama_obat . " " . $detail->satuan . ", ";
                                }
                                $keterangan = rtrim($keterangan, ", ");
                                $info = array(
                                    'id'				=> "retur_obat_per_pasien_" . $resep_retur->id_retur_penjualan_resep . "_resep_" . $resep_retur->id_penjualan_resep,
                                    'waktu'				=> $tanggal,
                                    'nama'				=> "Return Obat Per Pasien ID " . $resep_retur->id_retur_penjualan_resep . " - Resep No. " . $resep_retur->nomor_resep,
                                    'jumlah'			=> 1,
                                    'biaya'				=> $nilai_retur,
                                    'keterangan'		=> $keterangan,
                                    'start'				=> $timestamp,
                                    'end'				=> $timestamp,
                                    'dokter'			=> $nama_dokter,
                                    'prop'				=> $prop,
                                    'grup_name'			=> "return_resep",
                                    'nama_dokter' 		=> $resep_retur->nama_dokter,
                                    'id_dokter'			=> $resep_retur->id_dokter,
                                    'jaspel_dokter'		=> 0,
                                    'urjigd'			=> $resep_retur->uri == 1 ? "URI" : "URJ",
                                    'tanggal_tagihan'	=> ArrayAdapter::format("date Y-m-d", $resep_retur->tanggal)
                                );					
                                $list[] = $info;
                            } else if ($mode_data_tagihan == "detail") {
                                foreach ($detail_rows as $detail) {
                                    $nilai_retur = -1 * $detail->jumlah * $detail->harga * $detail->persentase_retur / 100;
                                    $info = array(
                                        'id'				=> "detail_retur_obat_per_pasien_" . $detail->id_dretur_penjualan_resep,
                                        'waktu'				=> $tanggal,
                                        'nama'				=> "Return Obat Per Pasien ID " . $resep_retur->id_retur_penjualan_resep . " - Resep No. " . $resep_retur->nomor_resep . " - " . $detail->nama_obat,
                                        'jumlah'			=> $detail->jumlah,
                                        'biaya'				=> $nilai_retur,
                                        'keterangan'		=> "Retur Obat " . $detail->nama_obat . " : " . $detail->jumlah . " x " . ArrayAdapter::format("only-money", $nilai_retur / $detail->jumlah),
                                        'start'				=> $timestamp,
                                        'end'				=> $timestamp,
                                        'dokter'			=> $nama_dokter,
                                        'prop'				=> $prop,
                                        'grup_name'			=> "return_resep",
                                        'nama_dokter' 		=> $resep_retur->nama_dokter,
                                        'id_dokter'			=> $resep_retur->id_dokter,
                                        'jaspel_dokter'		=> 0,
                                        'urjigd'			=> $resep_retur->uri == 1 ? "URI" : "URJ",
                                        'tanggal_tagihan'	=> ArrayAdapter::format("date Y-m-d", $resep_retur->tanggal)
                                    );					
                                    $list[] = $info;
                                }
                            }
                        }
                        $data_tagihan['list'] = $list;
                        $consumer_service = new ServiceConsumer(
                            $db,
                            "proceed_receivable",
                            $data_tagihan,
                            "kasir"
                        );
                        $consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
                        $consumer_service->execute();
                    }
                }
            } else if ($_POST['command'] == "save" && $_POST['dibatalkan'] == 1 && isset($_POST['id'])) {
                $id_retur_penjualan_resep = $_POST['id'];
                
                $resep_retur_rows = $db->get_result("
                    SELECT 
                        DISTINCT
                            a.id id_retur_penjualan_resep, 
                            a.tanggal,
                            b.id_penjualan_resep,
                            c.nomor_resep,
                            c.nama_dokter,
                            c.id_dokter, 
                            c.uri,
                            a.noreg_pasien, 
                            a.nrm_pasien, 
                            a.nama_pasien
                    FROM
                        " . InventoryLibrary::$_TBL_RETUR_PENJUALAN_RESEP ." a 
                            INNER JOIN " . InventoryLibrary::$_TBL_DRETUR_PENJUALAN_RESEP . " b ON a.id = b.id_retur_penjualan_resep
                            INNER JOIN " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . " c ON b.id_penjualan_resep = c.id
                    WHERE
                        a.id = '" . $id_retur_penjualan_resep . "' 
                            AND b.prop = '';
                ");
                if ($resep_retur_rows != null) {
                    foreach ($resep_retur_rows as $resep_retur) {
                        $tanggal = ArrayAdapter::format("date d M Y", $resep_retur->tanggal);
                        $timestamp = $resep_retur->tanggal;
                        $nama_dokter = $resep_retur->nama_dokter;
                        $prop = "del";

                        $data_tagihan = array(
                            'grup_name'		=> "return_resep",
                            'entity'		=> "depo_farmasi_icu",
                            'nama_pasien'	=> $resep_retur->nama_pasien,
                            'nrm_pasien'	=> $resep_retur->nrm_pasien,
                            'noreg_pasien'	=> $resep_retur->noreg_pasien
                        );
                        $list = array();
                        $detail_rows = $db->get_result("
                            SELECT 
                                b.id id_dretur_penjualan_resep,
                                d.nama_obat,
                                d.satuan,
                                a.persentase_retur,
                                b.jumlah, 
                                b.harga, 
                                b.subtotal
                            FROM 
                                " . InventoryLibrary::$_TBL_RETUR_PENJUALAN_RESEP . " a 
                                    INNER JOIN " . InventoryLibrary::$_TBL_DRETUR_PENJUALAN_RESEP . " b ON a.id = b.id_retur_penjualan_resep
                                    INNER JOIN " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . " c ON b.id_penjualan_resep = c.id
                                    INNER JOIN " . InventoryLibrary::$_TBL_STOK_OBAT . " d ON b.id_stok_obat = d.id
                            WHERE 
                                b.prop = '' 
                                    AND a.id = '" . $resep_retur->id_retur_penjualan_resep . "' 
                                    AND b.id_penjualan_resep = '" . $resep_retur->id_penjualan_resep . "'
                        ");
                        if ($detail_rows != null) {
                            if ($mode_data_tagihan == "simple") {
                                $nilai_retur = 0;
                                $keterangan = "Return Obat Per Pasien ID " . $resep_retur->id_retur_penjualan_resep . " - Resep No. " . $resep_retur->nomor_resep . " : ";
                                foreach ($detail_rows as $detail) {
                                    $nilai_retur -= $detail->jumlah * $detail->harga * $detail->persentase_retur / 100;
                                    $keterangan .= $detail->nama_obat . " " . $detail->satuan . ", ";
                                }
                                $keterangan = rtrim($keterangan, ", ");
                                $info = array(
                                    'id'				=> "retur_obat_per_pasien_" . $resep_retur->id_retur_penjualan_resep . "_resep_" . $resep_retur->id_penjualan_resep,
                                    'waktu'				=> $tanggal,
                                    'nama'				=> "Return Obat Per Pasien ID " . $resep_retur->id_retur_penjualan_resep . " - Resep No. " . $resep_retur->nomor_resep,
                                    'jumlah'			=> 1,
                                    'biaya'				=> $nilai_retur,
                                    'keterangan'		=> $keterangan,
                                    'start'				=> $timestamp,
                                    'end'				=> $timestamp,
                                    'dokter'			=> $nama_dokter,
                                    'prop'				=> $prop,
                                    'grup_name'			=> "return_resep",
                                    'nama_dokter' 		=> $resep_retur->nama_dokter,
                                    'id_dokter'			=> $resep_retur->id_dokter,
                                    'jaspel_dokter'		=> 0,
                                    'urjigd'			=> $resep_retur->uri == 1 ? "URI" : "URJ",
                                    'tanggal_tagihan'	=> ArrayAdapter::format("date Y-m-d", $resep_retur->tanggal)
                                );					
                                $list[] = $info;
                            } else if ($mode_data_tagihan == "detail") {
                                foreach ($detail_rows as $detail) {
                                    $nilai_retur = -1 * $detail->jumlah * $detail->harga * $detail->persentase_retur / 100;
                                    $info = array(
                                        'id'				=> "detail_retur_obat_per_pasien_" . $detail->id_dretur_penjualan_resep,
                                        'waktu'				=> $tanggal,
                                        'nama'				=> "Return Obat Per Pasien ID " . $resep_retur->id_retur_penjualan_resep . " - Resep No. " . $resep_retur->nomor_resep . " - " . $detail->nama_obat,
                                        'jumlah'			=> $detail->jumlah,
                                        'biaya'				=> $nilai_retur,
                                        'keterangan'		=> "Retur Obat " . $detail->nama_obat . " : " . $detail->jumlah . " x " . ArrayAdapter::format("only-money", $nilai_retur / $detail->jumlah),
                                        'start'				=> $timestamp,
                                        'end'				=> $timestamp,
                                        'dokter'			=> $nama_dokter,
                                        'prop'				=> $prop,
                                        'grup_name'			=> "return_resep",
                                        'nama_dokter' 		=> $resep_retur->nama_dokter,
                                        'id_dokter'			=> $resep_retur->id_dokter,
                                        'jaspel_dokter'		=> 0,
                                        'urjigd'			=> $resep_retur->uri == 1 ? "URI" : "URJ",
                                        'tanggal_tagihan'	=> ArrayAdapter::format("date Y-m-d", $resep_retur->tanggal)
                                    );					
                                    $list[] = $info;
                                }
                            }
                        }
                        $data_tagihan['list'] = $list;
                        $consumer_service = new ServiceConsumer(
                            $db,
                            "proceed_receivable",
                            $data_tagihan,
                            "kasir"
                        );
                        $consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
                        $consumer_service->execute();
                    }
                }
            }
        }

        echo json_encode($data);
        return;
    }

    /// Main Form :
    $retur_obat_per_pasien_modal = new Modal("retur_obat_per_pasien_modal", "retur_obat_per_pasien_modal", "Data Retur Obat Per Pasien");
    $retur_obat_per_pasien_modal->setClass(Modal::$FULL_MODEL);
    $id_hidden = new Hidden("retur_obat_per_pasien_id", "retur_obat_per_pasien_id", "");
    $retur_obat_per_pasien_modal->addElement("", $id_hidden);
    $tanggal_text = new Text("retur_obat_per_pasien_tanggal", "retur_obat_per_pasien_tanggal", "");
    $tanggal_text->setAtribute("disabled='disabled'");
    $retur_obat_per_pasien_modal->addElement("Tanggal / Jam", $tanggal_text);
    $pasien_button = new Button("", "", "Pilih");
    $pasien_button->setClass("btn-info");
    $pasien_button->setIsButton(Button::$ICONIC);
    $pasien_button->setIcon("icon-white " . Button::$icon_list_alt);
    $pasien_button->setAction("pasien.chooser('pasien', 'pasien_button', 'pasien', pasien, 'Pilih Pasien')");
    $pasien_button->setAtribute("id='pasien_browse'");
    $pasien_text = new Text("retur_obat_per_pasien_nama_pasien", "retur_obat_per_pasien_nama_pasien", "");
    $pasien_text->setAtribute("disabled='disabled'");
    $pasien_text->setClass("smis-one-option-input");
    $pasien_input_group = new InputGroup("");
    $pasien_input_group->addComponent($pasien_text);
    $pasien_input_group->addComponent($pasien_button);
    $retur_obat_per_pasien_modal->addElement("Pasien", $pasien_input_group);
    $noreg_text = new Text("retur_obat_per_pasien_noreg_pasien", "retur_obat_per_pasien_noreg_pasien", "");
    $noreg_text->setAtribute("disabled='disabled'");
    $retur_obat_per_pasien_modal->addElement("No. Registrasi", $noreg_text);
    $nrm_text = new Text("retur_obat_per_pasien_nrm_pasien", "retur_obat_per_pasien_nrm_pasien", "");
    $nrm_text->setAtribute("disabled='disabled'");
    $retur_obat_per_pasien_modal->addElement("NRM Pasien", $nrm_text);
    $alamat_text = new Text("retur_obat_per_pasien_alamat_pasien", "retur_obat_per_pasien_alamat_pasien", "");
    $alamat_text->setAtribute("disabled='disabled'");
    $retur_obat_per_pasien_modal->addElement("Alamat Pasien", $alamat_text);
    $dokter_textarea = new TextArea("retur_obat_per_pasien_dokter", "retur_obat_per_pasien_dokter", "");
    $dokter_textarea->setLine(3);
    $dokter_textarea->setAtribute("disabled='disabled'");
    $retur_obat_per_pasien_modal->addElement("Dokter", $dokter_textarea);

    $detail_table = new Table(
        array("No. Penjualan", "No.Resep", "Nama Obat", "Tgl. Exp.", "Sub Total", "Jml. Beli", "Jml. Retur"),
        "",
        null,
        true
    );
    $detail_table->setName("detail_retur_obat_per_pasien");
    $detail_table->setAddButtonEnable(false);
    $detail_table->setReloadButtonEnable(false);
    $detail_table->setPrintButtonEnable(false);
    $detail_table->setDelButtonEnable(false);
    $detail_table->setFooterVisible(false);
    
    $search_nama_obat_input_group = new InputGroup("");
    $search_nama_obat_text = new Text("search_nama_obat", "search_nama_obat", "");
    $search_nama_obat_text->setClass("smis-one-option-input");
    $search_nama_obat_button = new Button("", "", "Cari");
    $search_nama_obat_button->setClass("btn-info");
    $search_nama_obat_button->setAction("retur_obat_per_pasien.search_obat()");
    $search_nama_obat_button->setIcon("icon-white icon-search");
    $search_nama_obat_button->setIsButton(Button::$ICONIC);
    $search_nama_obat_input_group->addComponent($search_nama_obat_text);
    $search_nama_obat_input_group->addComponent($search_nama_obat_button);

    $html = "<div class='clear'></div>" .
            "<div>" .
                "<div align='right' id='search_panel'>" . $search_nama_obat_input_group->getHtml() . "</div>" .
                "<div>" . $detail_table->getHtml() . "</div>" .
            "</div>";

    $retur_obat_per_pasien_modal->addHtml($html);

    $save_button = new Button("", "", "Simpan");
    $save_button->setClass("btn-success");
    $save_button->setIcon("fa fa-floppy-o");
    $save_button->setIsButton(Button::$ICONIC);
    $save_button->setAction("retur_obat_per_pasien.save()");
    
    $retur_obat_per_pasien_modal->addFooter($save_button);
    //. End of Main Form

    /// Detail Form :
    $detail_retur_obat_per_pasien_modal = new Modal("dretur_obat_per_pasien_modal", "dretur_obat_per_pasien_modal", "Detail Retur Obat Per Pasien");
    $nama_obat_text = new Text("detail_retur_obat_per_pasien_nama_obat", "detail_retur_obat_per_pasien_nama_obat", "");
    $nama_obat_text->setAtribute("disabled='disabled'");
    $detail_retur_obat_per_pasien_modal->addElement("Nama Obat", $nama_obat_text);
    $jumlah_jual_hidden = new Hidden("detail_retur_obat_per_pasien_jumlah_jual", "detail_retur_obat_per_pasien_jumlah_jual", "");
    $detail_retur_obat_per_pasien_modal->addElement("", $jumlah_jual_hidden);
    $f_jumlah_jual_text = new Text("detail_retur_obat_per_pasien_f_jumlah_jual", "detail_retur_obat_per_pasien_f_jumlah", "");
    $f_jumlah_jual_text->setAtribute("disabled='disabled'");
    $detail_retur_obat_per_pasien_modal->addElement("Jml. Beli", $f_jumlah_jual_text);
    $jumlah_retur_text = new Text("detail_retur_obat_per_pasien_jumlah_retur", "detail_retur_obat_per_pasien_jumlah_retur", "");
    $detail_retur_obat_per_pasien_modal->addElement("Jml. Retur", $jumlah_retur_text);
    $satuan_text = new Text("detail_retur_obat_per_pasien_satuan", "detail_retur_obat_per_pasien_satuan", "");
    $satuan_text->setAtribute("disabled='disabled'");
    $detail_retur_obat_per_pasien_modal->addElement("Satuan", $satuan_text);
    $save_button = new Button("", "", "Simpan");
    $save_button->setClass("btn-success");
    $save_button->setIcon("fa fa-floppy-o");
    $save_button->setIsButton(Button::$ICONIC);
    $save_button->setAtribute("id='save_detail_button'");
    $detail_retur_obat_per_pasien_modal->addFooter($save_button);
    //. End of Detail Form

    /// Read-Only Main Form :
    $v_retur_obat_per_pasien_modal = new Modal("v_retur_obat_per_pasien_modal", "v_retur_obat_per_pasien_modal", "Data Retur Obat Per Pasien");
    $v_retur_obat_per_pasien_modal->setClass(Modal::$FULL_MODEL);
    $id_hidden = new Hidden("v_retur_obat_per_pasien_id", "v_retur_obat_per_pasien_id", "");
    $v_retur_obat_per_pasien_modal->addElement("", $id_hidden);
    $tanggal_text = new Text("v_retur_obat_per_pasien_tanggal", "v_retur_obat_per_pasien_tanggal", "");
    $tanggal_text->setAtribute("disabled='disabled'");
    $v_retur_obat_per_pasien_modal->addElement("Tanggal / Jam", $tanggal_text);
    $pasien_text = new Text("v_retur_obat_per_pasien_nama_pasien", "v_retur_obat_per_pasien_nama_pasien", "");
    $pasien_text->setAtribute("disabled='disabled'");
    $v_retur_obat_per_pasien_modal->addElement("Pasien", $pasien_text);
    $noreg_text = new Text("v_retur_obat_per_pasien_noreg_pasien", "v_retur_obat_per_pasien_noreg_pasien", "");
    $noreg_text->setAtribute("disabled='disabled'");
    $v_retur_obat_per_pasien_modal->addElement("No. Registrasi", $noreg_text);
    $nrm_text = new Text("v_retur_obat_per_pasien_nrm_pasien", "v_retur_obat_per_pasien_nrm_pasien", "");
    $nrm_text->setAtribute("disabled='disabled'");
    $v_retur_obat_per_pasien_modal->addElement("NRM Pasien", $nrm_text);
    $alamat_text = new Text("v_retur_obat_per_pasien_alamat_pasien", "v_retur_obat_per_pasien_alamat_pasien", "");
    $alamat_text->setAtribute("disabled='disabled'");
    $v_retur_obat_per_pasien_modal->addElement("Alamat Pasien", $alamat_text);
    $dokter_textarea = new TextArea("v_retur_obat_per_pasien_dokter", "v_retur_obat_per_pasien_dokter", "");
    $dokter_textarea->setLine(3);
    $dokter_textarea->setAtribute("disabled='disabled'");
    $v_retur_obat_per_pasien_modal->addElement("Dokter", $dokter_textarea);

    $v_detail_table = new Table(
        array("No. Penjualan", "No.Resep", "Nama Obat", "Tgl. Exp.", "Sub Total", "Jml. Retur"),
        "",
        null,
        true
    );
    $v_detail_table->setName("v_detail_retur_obat_per_pasien");
    $v_detail_table->setAction(false);
    $v_detail_table->setFooterVisible(false);
    $v_retur_obat_per_pasien_modal->addBody("v_detail_table", $v_detail_table);

    $ok_button = new Button("", "", "OK");
    $ok_button->setClass("btn-success");
    $ok_button->setAtribute("id='ok_button'");
    $ok_button->setAction("$($(this).data('target')).smodal('hide')");
    $v_retur_obat_per_pasien_modal->addFooter($ok_button);
    //. End of Read-Only Main Form

    echo $v_retur_obat_per_pasien_modal->getHtml();
    echo $detail_retur_obat_per_pasien_modal->getHtml();
    echo $retur_obat_per_pasien_modal->getHtml();
    echo $table->getHtml();
    echo addJS("framework/smis/js/table_action.js");
    echo addJS("depo_farmasi_icu/js/retur_obat_per_pasien_action.js", false);
    echo addJS("depo_farmasi_icu/js/detail_retur_obat_per_pasien_action.js", false);
    echo addJS("depo_farmasi_icu/js/retur_obat_per_pasien.js", false);
?>
<style type="text/css">
    textarea {
        resize: none;
    }
</style>