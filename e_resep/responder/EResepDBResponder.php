<?php
	require_once("smis-base/smis-include-duplicate.php");

	class EResepDBResponder extends DuplicateResponder {
		public function command($command) {
			if ($command != "get_preview_html")
				return parent::command($command);
			$package = null;
			if ($command == "get_preview_html") {
				$package = new ResponsePackage();
				$content = $this->get_preview_html();
				$package->setContent($content);
				$package->setStatus(ResponsePackage::$STATUS_OK);
			}
			return $package != null ? $package->getPackage() : null;
		}
		public function get_preview_html() {
			$id = $_POST['id'];
			$header_data = $this->dbtable->get_row("
				SELECT *
				FROM smis_er_resep
				WHERE id = '" . $id . "'
			");
			$html = "<table border='1' width='100%' cellpadding='5'>" .
						"<tr>" .
							"<td>" .
								"<table border='0' width='100%'>" .
									"<tr><td align='center' colspan='5'><strong>RSUD SYARIFAH AMBAMI RATO EBU</strong></td></tr>" .
									"<tr><td align='center' colspan='5'>Jl. Pemuda Kaffa No. 9, Bangkalan</td></tr>" .
									"<tr><td align='center' colspan='5'>&nbsp;</td></tr>" .
									"<tr>
										<td colspan='1'>Dokter</td>
										<td colspan='1'>:</td>
										<td colspan='3'>" . $header_data->nama_dokter . "</td>
									</tr>" .
									"<tr>
										<td colspan='1'>SIP</td>
										<td colspan='1'>:</td>
										<td colspan='3'>" . $header_data->sip_dokter . "</td>
									</tr>" .
									"<tr>
										<td colspan='1'>Unit Pelayanan</td>
										<td colspan='1'>:</td>
										<td colspan='3'>" . ArrayAdapter::format("unslug", $header_data->ruangan) . "</td>
									</tr>" .
								"</table>" .
							"</td>" .
						"</tr>" .
						"<tr>" .
							"<td>" .
								"<table border='0' width='100%'>" .
									"<tr>
										<td colspan='5'><div align='right'>Bangkalan, " . ArrayAdapter::format("date d-m-Y", $header_data->tanggal) . "</div></td>
									</tr>";
			// racikan:
			$racikan_data = $this->dbtable->get_result("
				SELECT label_racikan, GROUP_CONCAT(DISTINCT aturan_pakai) AS 'aturan_pakai'
				FROM smis_er_dresep
				WHERE prop NOT LIKE 'del' AND id_resep = '" . $id . "' AND is_racikan = 1
				GROUP BY label_racikan
				ORDER BY label_racikan ASC
			");
			foreach ($racikan_data as $rd) {
				$detail_racikan_data = $this->dbtable->get_result("
					SELECT *
					FROM smis_er_dresep
					WHERE label_racikan = '" . $rd->label_racikan . "' AND id_resep = '" . $id . "' AND prop NOT LIKE 'del'
				");
				$html .= "
					<tr>
						<td>R/</td>
						<td>&nbsp;</td>
						<td colspan='2'>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>";
				foreach ($detail_racikan_data as $drd) {
					$html .= "<tr>
								 <td>&nbsp;</td>
								 <td colspan='2'>" . $drd->nama_obat . "</td>
								 <td><div align='right'>" . $drd->jumlah . "</div></td>
								 <td>" . $drd->satuan . "</td>
							  </tr>";
				}
				$html .= "<tr>
							  <td>&nbsp;</td>
							  <td colspan='4'>" . $rd->aturan_pakai . "</td>
						  </tr>";
			}

			// non:racikan:
			$detail_data = $this->dbtable->get_result("
				SELECT *
				FROM smis_er_dresep
				WHERE prop NOT LIKE 'del' AND id_resep = '" . $id . "' AND is_racikan = 0
			");
			foreach ($detail_data as $dd) {
				$html .= "
					<tr>
						<td>R/</td>
						<td>&nbsp;</td>
						<td colspan='2'>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
						<td colspan='2'>" . $dd->nama_obat . "</td>
						<td><div align='right'>" . $dd->jumlah . "</div></td>
						<td>" . $dd->satuan . "</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
						<td colspan='4'>" . $dd->aturan_pakai . "</td>
					</tr>
				";
			}
			$html .= 			"</table>" .
							"</td>" .
						"</tr>" .
						"<tr>" .
							"<td>" .
								"<table border='0' width='100%'>" .
									"<tr>
										<td colspan='1'>Nama Pasien</td>
										<td colspan='1'>:</td>
										<td colspan='3'>" . $header_data->nama_pasien . "</td>
									</tr>" .
									"<tr>
										<td colspan='1'>No. Reg.</td>
										<td colspan='1'>:</td>
										<td colspan='3'>" . ArrayAdapter::format("digit6", $header_data->noreg_pasien) . "</td>
									</tr>" .
									"<tr>
										<td colspan='1'>No. RM</td>
										<td colspan='1'>:</td>
										<td colspan='3'>" . ArrayAdapter::format("digit6", $header_data->nrm_pasien) . "</td>
									</tr>" .
									"<tr>
										<td colspan='1'>Alamat</td>
										<td colspan='1'>:</td>
										<td colspan='3'>" . $header_data->alamat_pasien . "</td>
									</tr>" .
									"<tr>
										<td colspan='1'>Jenis Pasien</td>
										<td colspan='1'>:</td>
										<td colspan='3'>" . ArrayAdapter::format("unslug", $header_data->jenis) . "</td>
									</tr>" .
									"<tr>
										<td colspan='1'>Perusahaan</td>
										<td colspan='1'>:</td>
										<td colspan='3'>" . $header_data->perusahaan . "</td>
									</tr>" .
									"<tr>
										<td colspan='1'>Asurasi</td>
										<td colspan='1'>:</td>
										<td colspan='3'>" . $header_data->asuransi . "</td>
									</tr>" .
								"</table>" .
							"</td>" .
						"</tr>" .
					"</table>";

			$data['html'] = $html;
			return $data;
		}
		public function save() {
			$header_data = $this->postToArray();
			$id['id'] = $_POST['id'];
			if ($id['id'] == 0 || $id['id'] == "") {
				$result = $this->dbtable->insert($header_data);
				$id['id'] = $this->dbtable->get_inserted_id();
				$success['type'] = "insert";
				if (isset($_POST['detail'])) {
					$detail_dbtable = new DBTable($this->dbtable->get_db(), "smis_er_dresep");
					$detail = json_decode($_POST['detail']);
					foreach ($detail as $d) {
						$detail_data = array();
						$detail_data['id_resep'] = $id['id'];
						if ($d->is_racikan != null) 
							$detail_data['is_racikan'] = $d->is_racikan;
						if ($d->label_racikan != null)
							$detail_data['label_racikan'] = $d->label_racikan;
						if ($d->id_obat != null)
							$detail_data['id_obat'] = $d->id_obat;
						if ($d->kode_obat != null)
							$detail_data['kode_obat'] = $d->kode_obat;
						if ($d->nama_obat != null)
							$detail_data['nama_obat'] = $d->nama_obat;
						if ($d->nama_jenis_obat != null)
							$detail_data['nama_jenis_obat'] = $d->nama_jenis_obat;
						if ($d->jumlah != null)
							$detail_data['jumlah'] = $d->jumlah;
						if ($d->satuan != null)
							$detail_data['satuan'] = $d->satuan;
						if ($d->aturan_pakai != null)
							$detail_data['aturan_pakai'] = $d->aturan_pakai;
						$detail_data['autonomous'] = "[".$this->getAutonomous()."]";
				        $detail_data['duplicate'] = 0;
				        $detail_data['time_updated'] = date("Y-m-d H:i:s");
				        $detail_data['origin_updated'] = $this->getAutonomous();
				        $detail_data['origin'] = $this->getAutonomous();
						$detail_dbtable->insert($detail_data);
					}
				}
			} else {
				$result = $this->dbtable->update($header_data, $id);
				$success['type'] = "update";
				if (isset($_POST['detail'])) {
					$detail_dbtable = new DBTable($this->dbtable->get_db(), "smis_er_dresep");
					$detail = json_decode($_POST['detail']);
					foreach ($detail as $d) {
						$detail_id['id'] = $d->id;
						$detail_data = array();
						if ($d->is_racikan != null) 
							$detail_data['is_racikan'] = $d->is_racikan;
						if ($d->label_racikan != null)
							$detail_data['label_racikan'] = $d->label_racikan;
						if ($d->id_obat != null)
							$detail_data['id_obat'] = $d->id_obat;
						if ($d->kode_obat != null)
							$detail_data['kode_obat'] = $d->kode_obat;
						if ($d->nama_obat != null)
							$detail_data['nama_obat'] = $d->nama_obat;
						if ($d->nama_jenis_obat != null)
							$detail_data['nama_jenis_obat'] = $d->nama_jenis_obat;
						if ($d->jumlah != null)
							$detail_data['jumlah'] = $d->jumlah;
						if ($d->satuan != null)
							$detail_data['satuan'] = $d->satuan;
						if ($d->aturan_pakai != null)
							$detail_data['aturan_pakai'] = $d->aturan_pakai;
						if ($d->id == 0 || $d->id == "") {
							$detail_data['id_resep'] = $id['id'];
							$detail_data['autonomous'] = "[".$this->getAutonomous()."]";
					        $detail_data['duplicate'] = 0;
					        $detail_data['time_updated'] = date("Y-m-d H:i:s");
					        $detail_data['origin_updated'] = $this->getAutonomous();
					        $detail_data['origin'] = $this->getAutonomous();
							$detail_dbtable->insert($detail_data);
						} else if ($d->deleted) {
							$detail_data['prop'] = "del";
							$detail_data['autonomous'] = "[".$this->getAutonomous()."]";
					        $detail_data['duplicate'] = 0;
					        $detail_data['time_updated'] = date("Y-m-d H:i:s");
					        $detail_data['origin_updated'] = $this->getAutonomous();
							$detail_dbtable->update($detail_data, $detail_id);
						}
					}
				}
			}
		}
		public function edit() {
			$id = $_POST['id'];
			$read_only = $_POST['readonly'] == 1 ? true : false;
			$header_row = $this->dbtable->select($id);
			$detail_rows = $this->dbtable->get_result("
				SELECT *
				FROM smis_er_dresep
				WHERE id_resep = '" . $id . "' AND prop NOT LIKE 'del'
			");
			$row_num = 0;
			$detail_html = "";
			foreach ($detail_rows as $detail_row) {
				$f_is_racikan = $detail_row->is_racikan == 1 ? "&#10003;" : "&#10005;";
				$f_jumlah = number_format($detail_row->jumlah, 0, ",", ".");
				$action_button = "<center><a href='#' onclick='de_resep.delete(" . $row_num . ")' class='input btn btn-danger'><i class='fa fa-trash-o'></i></a></center>";
				if ($read_only)
					$action_button = "";
				$detail_html .= "
					<tr id='data_" . $row_num . "'>
						<td id='number'><small>" . ($row_num + 1) . "</small></td>
						<td id='id' style='display: none;'>" . $detail_row->id . "</td>
						<td id='id_obat' style='display: none;'>" . $detail_row->id_obat . "</td>
						<td id='is_racikan' style='display: none;'>" . $detail_row->is_racikan . "</td>
						<td id='f_is_racikan'><center><small>" . $f_is_racikan . "</small></center></td>
						<td id='racikan_num'><small>" . $detail_row->label_racikan . "</small></td>
						<td id='kode_obat'><small>" . $detail_row->kode_obat . "</small></td>
						<td id='nama_obat'><small>" . $detail_row->nama_obat . "</small></td>
						<td id='nama_jenis_obat'><small>" . $detail_row->nama_jenis_obat . "</small></td>
						<td id='jumlah' style='display: none;'>" . $detail_row->jumlah . "</td>
						<td id='f_jumlah'><small><div align='right'>" . $f_jumlah . "</div></small></td>
						<td id='satuan'><small>" . $detail_row->satuan . "</small></td>
						<td id='aturan_pakai'><small>" . $detail_row->aturan_pakai . "</small></td>
						<td>" . $action_button . "</td>
					</tr>
				";
				$row_num++;
			}
			$data = array(
				"header" 		=> $header_row,
				"detail_html" 	=> $detail_html,
				"row_num"		=> $row_num
			);
			return $data;
		}
	}
?>