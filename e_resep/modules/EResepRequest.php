<?php
	require_once("smis-framework/smis/template/ModulTemplate.php");
	require_once("smis-base/smis-include-service-consumer.php");
	require_once("e_resep/table/EResepTable.php");
	require_once("e_resep/responder/DokterServiceResponder.php");
	require_once("e_resep/responder/EResepDBResponder.php");
	require_once("e_resep/service_consumer/EResepReceiverServiceConsumer.php");

	class EResepRequest extends ModulTemplate {
		private $db;
		private $table;
		private $adapter;
		private $dbtable;
		private $dbresponder;

		private $page;
		private $proto_name;
		private $proto_slug;
		private $proto_implement;

		private $noreg_pasien;
		private $nrm_pasien;
		private $nama_pasien;
		private $alamat_pasien;
		private $no_telp_pasien;
		private $jenis_pasien;
		private $perusahaan_pasien;
		private $asuransi_pasien;
		private $usia;
		private $berat_badan;

		private $username_operator;
		private $nama_operator;

		public function __construct($db, $page, $proto_name = "", $proto_slug = "", $proto_implement = "", $noreg_pasien = "", $nama_operator = "", $username_operator = "") {
			parent::__construct();
			$this->db = $db;
			$this->page = $page;
			$this->proto_name = $proto_name;
			$this->proto_slug = $proto_slug;
			$this->proto_implement = $proto_implement;
			$this->noreg_pasien = $noreg_pasien;
			$this->username_operator = $username_operator;
			$this->nama_operator = $nama_operator;

			$params = array(
				"id" 		=> $this->noreg_pasien,
				"command"	=> "edit"
			);
			$service_consumer = new ServiceConsumer(
				$this->db,
				"get_registered",
				$params,
				"registration"
			);
			$content = $service_consumer->execute()->getContent();
			$this->nrm_pasien = $content['nrm'];
			$this->nama_pasien = $content['nama_pasien'];
			$this->alamat_pasien = $content['alamat_pasien'];
			$this->no_telp_pasien = "";
			$this->jenis_pasien = $content['carabayar'];
			$this->perusahaan_pasien = $content['n_perusahaan'];
			$this->asuransi_pasien = $content['nama_asuransi'];
			$this->usia = $content['umur'];

			$params = array(
				"nrm" 		=> $this->nrm_pasien
			);
			$service_consumer = new ServiceConsumer(
				$this->db,
				"get_last_patient_weight",
				$params,
				"medical_record"
			);
			$content = $service_consumer->execute()->getContent();
			$this->berat_badan = $content;

			$this->table = new EResepTable(
				array("No.", "No. E-Resep", "Waktu", "Dokter", "Depo Tujuan", "Operator"),
				$this->proto_name . " : E-Resep",
				null,
				true
			);
			$this->table->setName("e_resep");
		}

		public function superCommand($super_command) {
			$obat_table = new Table(
				array("Kode", "Obat", "Jenis", "Stok", "Satuan"),
				"",
				null,
				true
			);
			$obat_table->setName("e_resep_obat");
			$obat_table->setModel(Table::$SELECT);
			$obat_adapter = new SimpleAdapter();
			$obat_adapter->add("Kode", "kode_obat");
			$obat_adapter->add("Obat", "nama_obat");
			$obat_adapter->add("Jenis", "nama_jenis_obat");
			$obat_adapter->add("Stok", "sisa", "number");
			$obat_adapter->add("Satuan", "satuan");
			$obat_service_responder = new ServiceResponder(
				$this->db,
				$obat_table,
				$obat_adapter,
				"get_list_obat",
				$_POST['depo_tujuan']
			);

			$dokter_table = new Table(
				array("Nama", "Jabatan"),
				"",
				null,
				true
			);
			$dokter_table->setName("e_resep_dokter");
			$dokter_table->setModel(Table::$SELECT);
			$dokter_adapter = new SimpleAdapter();
			$dokter_adapter->add("Nama", "nama");
			$dokter_adapter->add("Jabatan", "nama_jabatan");
			$dokter_service_responder = new DokterServiceResponder(
				$this->db, 
				$dokter_table, 
				$dokter_adapter,
				"employee"
			);
			$sc = new SuperCommand();
			$sc->addResponder("e_resep_obat", $obat_service_responder);
			$sc->addResponder("e_resep_dokter", $dokter_service_responder);
			$init = $sc->initialize();
			if ($init != null) {
				echo $init;
				return;
			}
		}

		public function command($command) {
			$this->adapter = new SimpleAdapter(true, "No.");
			$this->adapter->add("id", "id");
			$this->adapter->add("locked", "locked");
			$this->adapter->add("No. E-Resep", "id", "digit8");
			$this->adapter->add("Waktu", "tanggal", "date d-m-Y, H:i");
			$this->adapter->add("Dokter", "nama_dokter");
			$this->adapter->add("Depo Tujuan", "depo", "unslug");
			$this->adapter->add("Operator", "nama_operator");
			$this->dbtable = new DBTable($this->db, "smis_er_resep");
			if (isset($_POST['noreg_pasien']) && isset($_POST['proto_slug'])) {
				$this->dbtable->addCustomKriteria(" noreg_pasien ", " = '" . $_POST['noreg_pasien'] . "' ");
				$this->dbtable->addCustomKriteria(" ruangan ", " = '" . $_POST['proto_slug'] . "' ");
			}
			$this->dbresponder = new EResepDBResponder(
				$this->dbtable,
				$this->table,
				$this->adapter
			);
			if ($this->dbresponder->isSave()) {
				if (isset($_POST['locked']) && isset($_POST['id']) && $_POST['locked'] == 1) {
					global $notification;
					$e_resep_info = $this->dbtable->select($_POST['id']);
					$key = md5($this->proto_slug . " ". $_POST['id']);
					$message = "E-Resep (No. E-Resep: " . ArrayAdapter::format("only-digit6", $_POST['id']) . "; No. Reg.: " . ArrayAdapter::format("only-digit6", $e_resep_info->noreg_pasien) . "; No. RM: " . ArrayAdapter::format("only-digit6", $e_resep_info->nrm_pasien) . "; Pasien: " . strtoupper($e_resep_info->nama_pasien) . "; Dokter: " . strtoupper($e_resep_info->nama_dokter) . ") dari <strong>".ArrayAdapter::format("unslug", $this->proto_slug)."</strong> masuk ke <strong>" . ArrayAdapter::format("unslug", $e_resep_info->depo) . "</strong>";
					$notification->addNotification("E-Resep Masuk", $key, $message, $e_resep_info->depo,"e_resep");
					$notification->commit();
				}
				$this->dbresponder->addColumnFixValue("tanggal", date("Y-m-d H:i"));
				$this->dbresponder->addColumnFixValue("ruangan", $this->proto_slug);
				$this->dbresponder->addColumnFixValue("username_operator", $this->username_operator);
				$this->dbresponder->addColumnFixValue("nama_operator", $this->nama_operator);
			}
			$data = $this->dbresponder->command($_POST['command']);
			echo json_encode($data);
			return;
		}

		public function phpPreLoad() {
			$header_form = new Form("", "", $this->proto_name . " : Formulir E-Resep");
			$id_hidden = new Hidden("e_resep_id", "e_resep_id", "");
			$header_form->addElement("", $id_hidden);
			$tanggal_text = new Text("e_resep_tanggal", "e_resep_tanggal", "");
			$tanggal_text->setAtribute("disabled='disabled'");
			$header_form->addElement("Waktu", $tanggal_text);
			$id_dokter_hidden = new Hidden("e_resep_id_dokter", "e_resep_id_dokter", "");
			$header_form->addElement("", $id_dokter_hidden);
			$name_dokter_hidden = new Hidden("e_resep_name_dokter", "e_resep_name_dokter", "");
			$header_form->addElement("", $name_dokter_hidden);
			$nama_dokter_text = new Text("e_resep_nama_dokter", "e_resep_nama_dokter", "");
			$nama_dokter_text->setClass("smis-one-option-input");
			$dokter_browse_button = new Button("", "", "Browse Dokter");
			$dokter_browse_button->setClass("btn-info");
			$dokter_browse_button->setIsButton(Button::$ICONIC);
			$dokter_browse_button->setIcon("fa fa-list");
			$dokter_browse_button->setAction("e_resep.chooser('e_resep', 'e_resep_nama_dokter', 'e_resep_dokter', e_resep_dokter, 'Dokter')");
			$dokter_browse_button->setAtribute("id='e_resep_dokter_browse'");
			$dokter_input_group = new InputGroup("");
			$dokter_input_group->addComponent($nama_dokter_text);
			$dokter_input_group->addComponent($dokter_browse_button);
			$header_form->addElement("Dokter", $dokter_input_group);
			$sip_dokter_text = new Text("e_resep_sip_dokter", "e_resep_sip_dokter", "");
			$sip_dokter_text->setAtribute("disabled='disabled'");
			$header_form->addElement("SIP", $sip_dokter_text);
			$noreg_pasien_text = new Text("e_resep_noreg_pasien", "e_resep_noreg_pasien", $this->noreg_pasien);
			$noreg_pasien_text->setAtribute("disabled='disabled'");
			$header_form->addElement("No. Reg.", $noreg_pasien_text);
			$nrm_pasien_text = new Text("e_resep_nrm_pasien", "e_resep_nrm_pasien", $this->nrm_pasien);
			$nrm_pasien_text->setAtribute("disabled='disabled'");
			$header_form->addElement("NRM", $nrm_pasien_text);
			$nama_pasien_text = new Text("e_resep_nama_pasien", "e_resep_nama_pasien", $this->nama_pasien);
			$nama_pasien_text->setAtribute("disabled='disabled'");
			$header_form->addElement("Nama", $nama_pasien_text);
			$alamat_pasien_text = new Text("e_resep_alamat_pasien", "e_resep_alamat_pasien", $this->alamat_pasien);
			$alamat_pasien_text->setAtribute("disabled='disabled'");
			$header_form->addElement("Alamat", $alamat_pasien_text);
			$no_telpon_text = new Text("e_resep_no_telpon", "e_resep_no_telpon", $this->no_telp_pasien);
			$no_telpon_text->setAtribute("disabled='disabled'");
			$header_form->addElement("No. Telp.", $no_telpon_text);
			$usia_text = new Text("e_resep_usia", "e_resep_usia", $this->usia);
			$usia_text->setAtribute("disabled='disabled'");
			$header_form->addElement("Usia", $usia_text);
			$berat_badan_text = new Text("e_resep_berat_badan", "e_resep_berat_badan", $this->berat_badan);
			$berat_badan_text->setAtribute("disabled='disabled'");
			$header_form->addElement("Berat Badan", $berat_badan_text);
			$jenis_pasien_text = new Text("e_resep_jenis_pasien", "e_resep_jenis_pasien", $this->jenis_pasien);
			$jenis_pasien_text->setAtribute("disabled='disabled'");
			$header_form->addElement("Jenis Pasien", $jenis_pasien_text);
			$perusahaan_pasien_text = new Text("e_resep_perusahaan_pasien", "e_resep_perusahaan_pasien", $this->perusahaan_pasien);
			$perusahaan_pasien_text->setAtribute("disabled='disabled'");
			$header_form->addElement("Perusahaan", $perusahaan_pasien_text);
			$asuransi_pasien_text = new Text("e_resep_asuransi_pasien", "e_resep_asuransi_pasien", $this->asuransi_pasien);
			$asuransi_pasien_text->setAtribute("disabled='disabled'");
			$header_form->addElement("Asuransi", $asuransi_pasien_text);
			$depo_option = new OptionBuilder();
			$jenis_ruangan = getSettings($this->db, "smis-rs-urjip-" . $this->proto_slug, "URJ");
			$depo_eresep_service_consumer = new EResepReceiverServiceConsumer($this->db, $jenis_ruangan, $this->proto_slug);
			$depo_eresep_service_consumer->execute();
			$depo_option = $depo_eresep_service_consumer->getContent();
			$depo_select = new Select("e_resep_depo", "e_resep_depo", $depo_option);
			$header_form->addElement("Depo Tujuan", $depo_select);
			$ruangan_hidden = new Hidden("e_resep_ruangan", "e_resep_ruangan", $this->proto_slug);
			$header_form->addElement("", $ruangan_hidden);
			
			$detail_form = new Form("", "", "");
			$row_num_hidden = new Hidden("de_resep_row_num", "de_resep_row_num", "");
			$detail_form->addElement("", $row_num_hidden);
			$rj_option = new OptionBuilder();
			$rj_option->add("Non-Racikan", "0", "1");
			$rj_option->add("Racikan", "1");
			$rj_select = new Select("de_resep_is_racikan", "de_resep_is_racikan", $rj_option->getContent());
			$detail_form->addElement("Jns. Resep", $rj_select);
			$racikan_num_option = new OptionBuilder();
			$racikan_num_option->add("1", "1", "1");
			$racikan_num_option->add("2", "2");
			$racikan_num_option->add("3", "3");
			$racikan_num_option->add("4", "4");
			$racikan_num_option->add("5", "5");
			$racikan_num_option->add("6", "6");
			$racikan_num_option->add("7", "7");
			$racikan_num_option->add("8", "8");
			$racikan_num_option->add("9", "9");
			$racikan_num_option->add("10", "10");
			$racikan_num_select = new Select("de_resep_racikan_num", "dresep_racikan_num", $racikan_num_option->getContent());
			$detail_form->addElement("Racikan ke-", $racikan_num_select);
			$id_obat_hidden = new Hidden("de_resep_id_obat", "de_resep_id_obat", "");
			$detail_form->addElement("", $id_obat_hidden);
			$name_obat_hidden = new Hidden("de_resep_name_obat", "de_resep_name_obat", "");
			$detail_form->addElement("", $name_obat_hidden);
			$kode_obat_text = new Text("de_resep_kode_obat", "de_resep_kode_obat", "");
			$kode_obat_text->setAtribute("disabled='disabled'");
			$detail_form->addElement("Kode", $kode_obat_text);
			$nama_obat_text = new Text("de_resep_nama_obat", "de_resep_nama_obat", "");
			$nama_obat_text->setClass("smis-one-option-input");
			$obat_browse_button = new Button("", "", "Browse Obat");
			$obat_browse_button->setClass("btn-info");
			$obat_browse_button->setIsButton(Button::$ICONIC);
			$obat_browse_button->setIcon("fa fa-list");
			$obat_browse_button->setAction("e_resep.chooser('e_resep', 'de_resep_nama_obat', 'e_resep_obat', e_resep_obat, 'Obat')");
			$obat_browse_button->setAtribute("id='e_resep_obat_browse'");
			$obat_input_group = new InputGroup("");
			$obat_input_group->addComponent($nama_obat_text);
			$obat_input_group->addComponent($obat_browse_button);
			$detail_form->addElement("Obat", $obat_input_group);
			$nama_jenis_obat_text = new Text("de_resep_nama_jenis_obat", "de_resep_nama_jenis_obat", "");
			$nama_jenis_obat_text->setAtribute("disabled='disabled'");
			$detail_form->addElement("Jenis", $nama_jenis_obat_text);
			$sisa_text = new Text("de_resep_sisa", "de_resep_sisa", "");
			$sisa_text->setAtribute("disabled='disabled'");
			$detail_form->addElement("Sisa", $sisa_text);
			$satuan_text = new Text("de_resep_satuan", "de_resep_satuan", "");
			$satuan_text->setAtribute("disabled='disabled'");
			$detail_form->addElement("Satuan", $satuan_text);
			$jumlah_text = new Text("de_resep_jumlah", "de_resep_jumlah", "");
			$detail_form->addElement("Jumlah", $jumlah_text);
			$aturan_pakai_text = new Text("de_resep_aturan_pakai", "de_resep_aturan_pakai", "");
			$detail_form->addElement("Aturan Pakai", $aturan_pakai_text);

			$save_button = new Button("", "", "Tambahkan");
			$save_button->setClass("btn-info");
			$save_button->setIsButton(Button::$ICONIC_TEXT);
			$save_button->setIcon("fa fa-chevron-right");
			$save_button->setAction("de_resep.save()");
			$save_button->setAtribute("id='de_resep_form_save'");
			$update_button = new Button("", "", "Perbarui");
			$update_button->setClass("btn-info");
			$update_button->setIsButton(Button::$ICONIC_TEXT);
			$update_button->setIcon("fa fa-chevron-right");
			$update_button->setAction("de_resep.save()");
			$update_button->setAtribute("id='de_resep_form_update'");
			$cancel_button = new Button("", "", "Batal");
			$cancel_button->setClass("btn-danger");
			$cancel_button->setIsButton(Button::$ICONIC_TEXT);
			$cancel_button->setIcon("icon-white icon-remove");
			$cancel_button->setAction("de_resep.cancel()");
			$cancel_button->setAtribute("id='de_resep_form_cancel'");	
			$button_group = new ButtonGroup("");
			$button_group->addButton($update_button);
			$button_group->addButton($cancel_button);
			$detail_form->addElement("", $save_button);
			$detail_form->addElement("", $button_group);

			$de_resep_table = new Table(
				array("No.", "Racikan", "ke-", "Kode", "Obat", "Jenis", "Jumlah", "Satuan", "Aturan Pakai", "Hapus"),
				"",
				null, 
				true
			);
			$de_resep_table->setName("de_resep");
			$de_resep_table->setAction(false);
			$de_resep_table->setFooterVisible(false);

			$button_group = new ButtonGroup("");
			$cancel_button = new Button("", "", "Kembali");
			$cancel_button->setClass("btn-inverse");
			$cancel_button->setIsButton(Button::$ICONIC_TEXT);
			$cancel_button->setIcon("fa fa-close");
			$cancel_button->setAction("e_resep.cancel()");
			$cancel_button->setAtribute("id='e_resep_back'");
			$button_group->addButton($cancel_button);
			$save_button = new Button("", "", "Simpan");
			$save_button->setClass("btn-success");
			$save_button->setIsButton(Button::$ICONIC_TEXT);
			$save_button->setIcon("fa fa-floppy-o");
			$save_button->setAction("e_resep.save()");
			$save_button->setAtribute("id='e_resep_save'");
			$button_group->addButton($save_button);

			$modal = new Modal("e_resep_preview_modal", "smis_form_container", "e_resep_preview");
			$modal->setTitle("Pratinjau E-Resep");
			
			echo $modal->getHtml();
			echo "<div id='e_resep_form' style='display: none;'>";
			echo 	$header_form->getHtml();
			echo 	"<div id='table_content'>" .
			 	 		"<div class='row-fluid'>";
			echo			"<div class='span4'>" .
					 			$detail_form->getHtml() .
				 			"</div>" .
					 		"<div class='span8'>" .
						 		$de_resep_table->getHtml() .
								"<div align='right'>" .
									$button_group->getHtml() .
								"</div>" .
					 		"</div>" .
					 	"</div>";
			echo 		"<div class='row-fluid'>" .
						"</div>";
			echo	"</div>";
			echo "</div>";
			echo "<div id='e_resep_table_list'>";
			echo $this->table->getHtml();
			echo "</div>";
		}

		public function jsLoader() {
			echo addJS ("framework/smis/js/table_action.js");
			echo addJS ("framework/bootstrap/js/bootstrap-datepicker.js");
			echo addJS ("e_resep/js/e_resep_dokter_action.js", false);
			echo addJS ("e_resep/js/e_resep_obat_action.js", false);
			echo addJS ("e_resep/js/de_resep_action.js", false);
			echo addJS ("e_resep/js/e_resep_action.js", false);
		}

		public function jsPreLoad() {
			?>
			<script type="text/javascript">
				var e_resep;
				var de_resep;
				var e_resep_dokter;
				var e_resep_obat;
				var row_num;

				$(document).ready(function() {
					e_resep_dokter = new EResepDokterAction(
						"e_resep_dokter",
						"<?php echo $this->page; ?>",
						"e_resep",
						new Array()
					);
					e_resep_dokter.setPrototipe(
						"<?php echo $this->proto_name; ?>",
						"<?php echo $this->proto_slug; ?>",
						"<?php echo $this->proto_implement; ?>"
					);
					e_resep_dokter.setSuperCommand("e_resep_dokter");
					e_resep_obat = new EResepObatAction(
						"e_resep_obat",
						"<?php echo $this->page; ?>",
						"e_resep",
						new Array()	
					);
					e_resep_obat.setPrototipe(
						"<?php echo $this->proto_name; ?>",
						"<?php echo $this->proto_slug; ?>",
						"<?php echo $this->proto_implement; ?>"
					);
					e_resep_obat.setSuperCommand("e_resep_obat");
					var de_resep_columns = new Array("id", "id_resep", "is_racikan", "label_racikan", "id_obat", "kode_obat", "nama_obat", "nama_jenis_obat", "jumlah", "satuan");
					de_resep = new DEResepAction(
						"de_resep",
						"<?php echo $this->page; ?>",
						"e_resep",
						de_resep_columns
					);
					var e_resep_columns = new Array("id", "depo", "tanggal", "id_dokter", "nama_dokter", "noreg_pasien", "nrm_pasien", "nama_pasien", "alamat_pasien", "no_telpon", "jenis", "perusahaan", "asuransi", "ruangan", "username_operator", "nama_operator", "keterangan", "locked");
					e_resep = new EResepAction(
						"e_resep",
						"<?php echo $this->page; ?>",
						"e_resep",
						e_resep_columns
					);
					e_resep.setPrototipe(
						"<?php echo $this->proto_name; ?>",
						"<?php echo $this->proto_slug; ?>",
						"<?php echo $this->proto_implement; ?>"
					);
					e_resep.addRegulerData=function(data){
						data['noreg_pasien'] = $("#e_resep_noreg_pasien").val();
						return data;
					};
					e_resep.view();

					$("#de_resep_is_racikan").change(function() {
						var is_racikan = $("#de_resep_is_racikan").val();
						if (is_racikan === "1")
							$(".de_resep_racikan_num").show();
						else
							$(".de_resep_racikan_num").hide();
					});

					$("#e_resep_nama_dokter").typeahead({
						minLength	: 3,
						source 		: function (query, process) {
							var $items = new Array;
							$items = [""];
							var dokter_nama = e_resep_dokter.getViewData();               
							dokter_nama['kriteria'] = $('#e_resep_nama_dokter').val();
							$.ajax({
								url 	: '',
								type 	: 'POST',
								data 	: dokter_nama,
								success : function(res) {
									var json = getContent(res);
									var data_proses = json.d.data;
									$items = [""];
									$.map(data_proses, function(data) {
										var group;
										group = {
											id 			: data.id,
											name 	 	: data.nama,
											toString 	: function () {
												return JSON.stringify(this);
											},
											toLowerCase	: function () {
												return this.name.toLowerCase();
											},
											indexOf 	: function (string) {
												return String.prototype.indexOf.apply(this.name, arguments);
											},
											replace 	: function (string) {
												var value = '';
												value +=  this.name;
												if(typeof(this.level) != 'undefined') {
													value += ' <span class="pull-right muted">';
													value += this.level;
													value += '</span>';
												}
												return String.prototype.replace.apply('<div class="typeaheadiv">' + value + '</div>', arguments);
											}
										};
										$items.push(group);
									});
									process($items);
								}
							});
						},
						updater: function (item) {
							var item = JSON.parse(item);  
							e_resep_dokter.select(item.id);
							return item.name;
						}
					});

					$("#e_resep_nama_dokter").keypress(function(e) {
						$("ul.typeahead").html("");
						$("ul.typeahead").hide();
						if(e.which == 13) {
							$('#de_resep_nama_obat').focus();
						}
					});

					$("#de_resep_nama_obat").typeahead({
						minLength	: 3,
						source 		: function (query, process) {
							var $items = new Array;
							$items = [""];
							var obat_nama = e_resep_obat.getViewData();
							obat_nama['kriteria'] = $('#de_resep_nama_obat').val();
							$.ajax({
								url 	: '',
								type 	: 'POST',
								data 	: obat_nama,
								success : function(res) {
									var json = getContent(res);
									var data_proses = json.d.data;
									$items = [""];
									$.map(data_proses, function(data) {
										var group;
										group = {
											id 			: data.id,
											name 	 	: data.nama_obat,
											kode 	 	: data.kode_obat,
											toString 	: function () {
												return JSON.stringify(this);
											},
											toLowerCase	: function () {
												return this.name.toLowerCase();
											},
											indexOf 	: function (string) {
												return String.prototype.indexOf.apply(this.name, arguments);
											},
											replace 	: function (string) {
												var value = '';
												value +=  this.kode + " - " + this.name;
												if(typeof(this.level) != 'undefined') {
													value += ' <span class="pull-right muted">';
													value += this.level;
													value += '</span>';
												}
												return String.prototype.replace.apply('<div class="typeaheadiv">' + value + '</div>', arguments);
											}
										};
										$items.push(group);
									});
									process($items);
								}
							});
						},
						updater: function (item) {
							var item = JSON.parse(item);  
							e_resep_obat.select(item.id);
							return item.name;
						}
					});

					$("#de_resep_nama_obat").keypress(function(e) {
						$("ul.typeahead").html("");
						$("ul.typeahead").hide();
						if(e.which == 13) {
							$('#de_resep_jumlah').focus();
						}
					});

					$("#de_resep_jumlah").keypress(function(e) {
						$("ul.typeahead").html("");
						$("ul.typeahead").hide();
						if(e.which == 13) {
							$('#de_resep_aturan_pakai').focus();
						}
					});

					$("#de_resep_aturan_pakai").keypress(function(e) {
						$("ul.typeahead").html("");
						$("ul.typeahead").hide();
						if(e.which == 13) {
							$('#de_resep_form_save').trigger('click');
						}
					});					
				});
			</script>
			<?php
		}

		public function cssLoader() {
			echo addCSS("framework/bootstrap/css/datepicker.css");
		}
	}
?>