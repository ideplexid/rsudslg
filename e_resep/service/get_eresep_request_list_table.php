<?php
	global $db;
	require_once("e_resep/modules/EResepRequest.php");

	$page = $_POST['page'];
	$proto_name = $_POST['proto_name'];
	$proto_slug = $_POST['proto_slug'];
	$proto_implement = $_POST['proto_implement'];
	$noreg_pasien = $_POST['noreg_pasien'];
	$nama_operator = $_POST['nama_operator'];
	$username_operator = $_POST['username_operator'];

	ob_start();
	$e_resep_request = new EResepRequest(
		$db,
		$page,
		$proto_name,
		$proto_slug,
		$proto_implement,
		$noreg_pasien,
		$nama_operator,
		$username_operator
	);
	$e_resep_request->initialize();
	$result = ob_get_clean();
	echo json_encode($result);
?>