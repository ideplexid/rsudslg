<?php
	global $db;

	if (isset($_POST['command'])) {
		$command = $_POST['command'];
		$id = isset($_POST['id']) ? $_POST['id'] : "";
		$dbtable = new DBTable($db, "smis_er_resep");

		if ($command == "edit") {
			$header_row = $dbtable->get_row("
				SELECT *
				FROM smis_er_resep
				WHERE id = '" . $id . "'
			");
			$details = array();
			$detail_index = 0;
			$racikan_rows = $dbtable->get_result("
				SELECT label_racikan, GROUP_CONCAT(DISTINCT aturan_pakai) AS 'aturan_pakai'
				FROM smis_er_dresep
				WHERE id_resep = '" . $id . "' AND prop NOT LIKE 'del' AND is_racikan = '1'
				GROUP BY label_racikan
				ORDER BY label_racikan ASC
			");
			foreach ($racikan_rows as $racikan_row) {
				$bahan_rows = $dbtable->get_result("
					SELECT *
					FROM smis_er_dresep
					WHERE id_resep = '" . $id . "' AND prop NOT LIKE 'del' AND label_racikan = '" . $racikan_row->label_racikan . "'
				");
				$racikan_details = array();
				$racikan_index = 0;
				foreach ($bahan_rows as $bahan_row) {
					$racikan_details[$racikan_index++] = array(
						"id_obat"			=> $bahan_row->id_obat,
						"kode_obat"			=> $bahan_row->kode_obat,
						"nama_obat"			=> $bahan_row->nama_obat,
						"nama_jenis_obat"	=> $bahan_row->nama_jenis_obat,
						"jumlah"			=> $bahan_row->jumlah,
						"satuan"			=> $bahan_row->satuan
					);
				}
				$details[$detail_index++] = array(
					"is_racikan"	=> 1,
					"label_racikan"	=> $racikan_row->label_racikan,
					"aturan_pakai"	=> $racikan_row->aturan_pakai,
					"details"		=> $racikan_details
				);
			}
			$non_racikan_rows = $dbtable->get_result("
				SELECT *
				FROM smis_er_dresep
				WHERE id_resep = '" . $id . "' AND prop NOT LIKE 'del' AND is_racikan = '0'
			");
			foreach ($non_racikan_rows as $non_racikan_row) {
				$details[$detail_index++] = array(
					"is_racikan"		=> 0,
					"aturan_pakai"		=> $non_racikan_row->aturan_pakai,
					"id_obat"			=> $non_racikan_row->id_obat,
					"kode_obat"			=> $non_racikan_row->kode_obat,
					"nama_obat"			=> $non_racikan_row->nama_obat,
					"nama_jenis_obat"	=> $non_racikan_row->nama_jenis_obat,
					"jumlah"			=> $non_racikan_row->jumlah,
					"satuan"			=> $non_racikan_row->satuan
				);
			}

			$data = array(
				"header"	=> $header_row,
				"details"	=> json_encode($details)
			);
			echo json_encode($data);
		} else if ($command == "unlock") {
			$id_data = array(
				"id"	=> $id
			);
			$data = array(
				"locked"	=> 0
			);
			$dbtable->update($data, $id_data);
		} else if ($command == "preview") {
			$header_data = $dbtable->get_row("
				SELECT *
				FROM smis_er_resep
				WHERE id = '" . $id . "'
			");
			$html = "<table border='1' width='100%' cellpadding='5'>" .
						"<tr>" .
							"<td>" .
								"<table border='0' width='100%'>" .
									"<tr><td align='center' colspan='5'><strong>RSUD SYARIFAH AMBAMI RATO EBU</strong></td></tr>" .
									"<tr><td align='center' colspan='5'>Jl. Pemuda Kaffa No. 9, Bangkalan</td></tr>" .
									"<tr><td align='center' colspan='5'>&nbsp;</td></tr>" .
									"<tr>
										<td colspan='1'>Dokter</td>
										<td colspan='1'>:</td>
										<td colspan='3'>" . $header_data->nama_dokter . "</td>
									</tr>" .
									"<tr>
										<td colspan='1'>SIP</td>
										<td colspan='1'>:</td>
										<td colspan='3'>" . $header_data->sip_dokter . "</td>
									</tr>" .
									"<tr>
										<td colspan='1'>Unit Pelayanan</td>
										<td colspan='1'>:</td>
										<td colspan='3'>" . ArrayAdapter::format("unslug", $header_data->ruangan) . "</td>
									</tr>" .
								"</table>" .
							"</td>" .
						"</tr>" .
						"<tr>" .
							"<td>" .
								"<table border='0' width='100%'>" .
									"<tr>
										<td colspan='5'><div align='right'>Bangkalan, " . ArrayAdapter::format("date d-m-Y", $header_data->tanggal) . "</div></td>
									</tr>";
			// racikan:
			$racikan_data = $dbtable->get_result("
				SELECT label_racikan, GROUP_CONCAT(DISTINCT aturan_pakai) AS 'aturan_pakai'
				FROM smis_er_dresep
				WHERE prop NOT LIKE 'del' AND id_resep = '" . $id . "' AND is_racikan = 1
				GROUP BY label_racikan
				ORDER BY label_racikan ASC
			");
			foreach ($racikan_data as $rd) {
				$detail_racikan_data = $dbtable->get_result("
					SELECT *
					FROM smis_er_dresep
					WHERE label_racikan = '" . $rd->label_racikan . "' AND id_resep = '" . $id . "' AND prop NOT LIKE 'del'
				");
				$html .= "
					<tr>
						<td>R/</td>
						<td>&nbsp;</td>
						<td colspan='2'>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>";
				foreach ($detail_racikan_data as $drd) {
					$html .= "<tr>
								 <td>&nbsp;</td>
								 <td colspan='2'>" . $drd->nama_obat . "</td>
								 <td><div align='right'>" . $drd->jumlah . "</div></td>
								 <td>" . $drd->satuan . "</td>
							  </tr>";
				}
				$html .= "<tr>
							  <td>&nbsp;</td>
							  <td colspan='4'>" . $rd->aturan_pakai . "</td>
						  </tr>";
			}

			// non:racikan:
			$detail_data = $dbtable->get_result("
				SELECT *
				FROM smis_er_dresep
				WHERE prop NOT LIKE 'del' AND id_resep = '" . $id . "' AND is_racikan = 0
			");
			foreach ($detail_data as $dd) {
				$html .= "
					<tr>
						<td>R/</td>
						<td>&nbsp;</td>
						<td colspan='2'>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
						<td colspan='2'>" . $dd->nama_obat . "</td>
						<td><div align='right'>" . $dd->jumlah . "</div></td>
						<td>" . $dd->satuan . "</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
						<td colspan='4'>" . $dd->aturan_pakai . "</td>
					</tr>
				";
			}
			$html .= 			"</table>" .
							"</td>" .
						"</tr>" .
						"<tr>" .
							"<td>" .
								"<table border='0' width='100%'>" .
									"<tr>
										<td colspan='1'>Nama Pasien</td>
										<td colspan='1'>:</td>
										<td colspan='3'>" . $header_data->nama_pasien . "</td>
									</tr>" .
									"<tr>
										<td colspan='1'>No. Reg.</td>
										<td colspan='1'>:</td>
										<td colspan='3'>" . ArrayAdapter::format("digit6", $header_data->noreg_pasien) . "</td>
									</tr>" .
									"<tr>
										<td colspan='1'>No. RM</td>
										<td colspan='1'>:</td>
										<td colspan='3'>" . ArrayAdapter::format("digit6", $header_data->nrm_pasien) . "</td>
									</tr>" .
									"<tr>
										<td colspan='1'>Alamat</td>
										<td colspan='1'>:</td>
										<td colspan='3'>" . $header_data->alamat_pasien . "</td>
									</tr>" .
									"<tr>
										<td colspan='1'>Jenis Pasien</td>
										<td colspan='1'>:</td>
										<td colspan='3'>" . ArrayAdapter::format("unslug", $header_data->jenis) . "</td>
									</tr>" .
									"<tr>
										<td colspan='1'>Perusahaan</td>
										<td colspan='1'>:</td>
										<td colspan='3'>" . $header_data->perusahaan . "</td>
									</tr>" .
									"<tr>
										<td colspan='1'>Asurasi</td>
										<td colspan='1'>:</td>
										<td colspan='3'>" . $header_data->asuransi . "</td>
									</tr>" .
								"</table>" .
							"</td>" .
						"</tr>" .
					"</table>";

			$data['html'] = $html;
			echo json_encode($data);
		}
	}
?>