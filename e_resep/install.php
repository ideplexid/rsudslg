<?php 
	require_once("smis-libs-class/DBCreator.php");
	global $wpdb;

	$dbcreator = new DBCreator($wpdb, "smis_er_resep");
	$dbcreator->addColumn("tanggal", "DATETIME", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("depo", "VARCHAR(128)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("id_dokter", "VARCHAR(32)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("sip_dokter", "VARCHAR(128)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("nama_dokter", "VARCHAR(32)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("noreg_pasien", "VARCHAR(32)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("nrm_pasien", "VARCHAR(32)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("nama_pasien", "VARCHAR(32)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("usia", "VARCHAR(128)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("berat_badan", "VARCHAR(256)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("alamat_pasien", "VARCHAR(64)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("no_telpon", "VARCHAR(16)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("jenis", "VARCHAR(32)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("perusahaan", "VARCHAR(128)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("asuransi", "VARCHAR(128)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("ruangan", "VARCHAR(256)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("username_operator", "VARCHAR(128)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("nama_operator", "VARCHAR(256)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("keterangan", "VARCHAR(512)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("locked", "TINYINT(1)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_ZERO, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->initialize();

	$dbcreator = new DBCreator($wpdb, "smis_er_dresep");
	$dbcreator->addColumn("id_resep", "INT(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("is_racikan", "TINYINT(1)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_ZERO, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("label_racikan", "VARCHAR(128)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("id_obat", "INT(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("kode_obat", "VARCHAR(32)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("nama_obat", "VARCHAR(128)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("nama_jenis_obat", "VARCHAR(128)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("jumlah", "INT(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("satuan", "VARCHAR(32)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("aturan_pakai", "VARCHAR(256)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->setDuplicate(false);
	$dbcreator->initialize();
?>