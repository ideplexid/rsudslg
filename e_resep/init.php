<?php
	global $PLUGINS;

	$init ['name'] = "e_resep";
	$init ['path'] = SMIS_DIR . "e_resep/";
	$init ['description'] = "E-Resep";
	$init ['require'] = "administrator, rawat";
	$init ['service'] = "";
	$init ['version'] = "1.0.0";
	$init ['number'] = "0";
	$init ['type'] = "";

	$myplugins = new Plugin($init);
	$PLUGINS[$init['name']] = $myplugins;
?>