<?php
	require_once("smis-framework/smis/template/NavigatorTemplate.php");

	class EResepNavigator extends NavigatorTemplate {
		public function __construct($navigation, $tooltip, $name, $page) {
			parent::__construct($navigation, $tooltip, $name, $page, "fa fa-file-text");
		}
	}
?>