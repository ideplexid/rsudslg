<?php
class EResepTable extends Table {
	public function getBodyContent() {
		$content = "";
		if ($this->content!=NULL) {
			foreach ($this->content as $d) {
				$content .= "<tr>";
				foreach ($this->header as $h) {
					$content .= "<td>" . $d[$h] . "</td>";
				}
				if ($this->is_action) {
					$content .= "<td>" . $this->getFilteredContentButton($d['id'], $d['locked'])->getHtml() . "</td>";
				}
				$content .= "</tr>";
			}
		}
		return $content;
	}
	public function getFilteredContentButton($id, $locked) {
		$btn_group = new ButtonGroup("noprint");
		if ($locked) {
			$btn = new Button("", "", "Lihat E-Resep");
			$btn->setAction($this->action . ".detail('" . $id . "')");
			$btn->setClass("btn-danger");
			$btn->setAtribute("data-content='Lihat' data-toggle='popover'");
			$btn->setIcon("icon-eye-open icon-white");
			$btn->setIsButton(Button::$ICONIC);
			$btn_group->addElement($btn);
			$btn = new Button("", "", "Pratinjau E-Resep");
			$btn->setAction($this->action . ".preview('" . $id . "')");
			$btn->setClass("btn-inverse");
			$btn->setAtribute("data-content='Pratinjau' data-toggle='popover'");
			$btn->setIcon("fa fa-file");
			$btn->setIsButton(Button::$ICONIC);
			$btn_group->addElement($btn);
		} else {
			$btn = new Button("", "", "Ubah E-Resep");
			$btn->setAction($this->action . ".edit('" . $id . "')");
			$btn->setClass("btn-warning");
			$btn->setAtribute("data-content='Ubah' data-toggle='popover'");
			$btn->setIcon("fa fa-pencil");
			$btn->setIsButton(Button::$ICONIC);
			$btn_group->addElement($btn);
			$btn = new Button("", "", "Kunci E-Resep");
			$btn->setAction($this->action . ".lock('" . $id . "')");
			$btn->setClass("btn-success");
			$btn->setAtribute("data-content='Kunci' data-toggle='popover'");
			$btn->setIcon("fa fa-lock");
			$btn->setIsButton(Button::$ICONIC);
			$btn_group->addElement($btn);
			$btn = new Button("", "", "Hapus E-Resep");
			$btn->setAction($this->action . ".del('" . $id . "')");
			$btn->setClass("btn-danger");
			$btn->setAtribute("data-content='Hapus' data-toggle='popover'");
			$btn->setIcon("icon-remove icon-white");
			$btn->setIsButton(Button::$ICONIC);
			$btn_group->addElement($btn);
		}
		return $btn_group;
	}
}
?>