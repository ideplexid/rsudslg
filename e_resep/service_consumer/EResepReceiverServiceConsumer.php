<?php
class EResepReceiverServiceConsumer extends ServiceConsumer {
	private $uri;
	private $proto_slug;
	public function __construct($db, $uri, $proto_slug) {
		parent::__construct ( $db, "get_entity", "is_eresep_receiver" );
		$this->uri = $uri;
		$this->proto_slug = $proto_slug;
	}
	public function proceedResult() {
		$content = array ();
		$result = json_decode ( $this->result, true );
		$default_is_set = false;
		foreach ( $result as $autonomous ) {
			foreach ( $autonomous as $entity ) {
				$option = array ();
				$option ['value'] = $entity;
				$option ['name'] = ArrayAdapter::format("unslug", $entity ) ;
				if ($default_is_set == false) {
					if ($this->uri == "URI" && $entity == "depo_farmasi_irna") {
						$option ['default'] = "1";
						$default_is_set = true;
					} else if ($this->proto_slug == "igd" && $entity == "depo_farmasi_igd") {
						$option ['default'] = "1";
						$default_is_set = true;
					} else if (($this->uri == "URJ" || $this->uri == "URJI") && $this->proto_slug != "igd" && $entity == "depo_farmasi_irja") {
						$option ['default'] = "1";
						$default_is_set = true;
					}
				}
				$number = count ( $content );
				$content [$number] = $option;
			}
		}
		return $content;
	}
}
?>