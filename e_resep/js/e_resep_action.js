function EResepAction(name, page, action, column) {
	this.initialize(name, page, action, column);
}
EResepAction.prototype.constructor = EResepAction;
EResepAction.prototype = new TableAction();
EResepAction.prototype.show_add_form = function() {	
	$("#e_resep_table_list").hide();
	$("#e_resep_form").show();

	$("#e_resep_id_dokter").val("");
	$("#e_resep_name_dokter").val("");
	$("#e_resep_nama_dokter").val("");
	$("#e_resep_nama_dokter").removeAttr("disabled");
	$("#e_resep_dokter_browse").show();
	$("#e_resep_nama_dokter").addClass("smis-one-option-input");
	$("#e_resep_sip_dokter").val("");
	$("#e_resep_depo").removeAttr("disabled");

	row_num = 0;
	$("#de_resep_list").empty();
	$("#de_resep_list").append(
		"<tr id='temp'>" +
			"<td colspan='10'><small><center><strong>BELUM ADA DETAIL E-RESEP</strong></center></small></td>" +
		"</tr>"
	);
	
	$("#de_resep_is_racikan").val(0);
	$("#de_resep_is_racikan").removeAttr("disabled");
	$(".de_resep_racikan_num").hide();
	$("#de_resep_row_num").val("");
	$("#de_resep_id_obat").val("");
	$("#de_resep_name_obat").val("");
	$("#de_resep_nama_obat").val("");
	$("#de_resep_nama_obat").removeAttr("disabled");
	$("#e_resep_obat_browse").show();
	$("#de_resep_nama_jenis_obat").val("");
	$("#de_resep_sisa").val("");
	$("#de_resep_satuan").val("");
	$("#de_resep_jumlah").val("");
	$("#de_resep_aturan_pakai").val("");
	$("#de_resep_form_update").hide();
	$("#de_resep_form_cancel").hide();
	$("#de_resep_form_save").show();

	$("#e_resep_save").show();
};
EResepAction.prototype.validate = function() {
	var valid = true;
	var invalid_msg = "";
	$(".error_field").removeClass("error_field");
	var id_dokter = $("#e_resep_id_dokter").val();
	var noreg_pasien = $("#e_resep_noreg_pasien").val();
	var depo = $("#e_resep_depo").val();
	var num_rows = 0;
	for (var i= 0; i < $("tbody#de_resep_list").children("tr").length; i++) {
		if ($("tbody#de_resep_list tr:eq(" + i + ")").prop("id") != "temp")
			num_rows++;
	}
	if (id_dokter == "") {
		valid = false;
		invalid_msg += "<br/><strong>Dokter</strong> tidak boleh kosong.";
		$("#e_resep_nama_dokter").addClass("error_field");
	}
	if (noreg_pasien == "")	 {
		valid = false;
		invalid_msg += "<br/><strong>Pasien</strong> tidak boleh kosong.";
		$("#e_resep_nama_pasien").addClass("error_field");	
	}
	if (depo == "") {
		valid = false;
		invalid_msg += "<br/><strong>Depo</strong> tidak boleh kosong.";
		$("#e_resep_depo").addClass("error_field");
	}
	if (num_rows == 0) {
		valid = false;
		invalid_msg += "<br/><strong>Detail E-Resep</strong> tidak boleh kosong.";
	} 
	if (!valid)
		bootbox.alert(invalid_msg);
	return valid;
};
EResepAction.prototype.save = function() {
	if (!this.validate())
		return;
	showLoading();
	var self = this;
	var data = this.getRegulerData();
	data['command'] = "save";
	data['id'] = $("#e_resep_id").val();
	data['depo'] = $("#e_resep_depo").val();
	data['id_dokter'] = $("#e_resep_id_dokter").val();
	data['nama_dokter'] = $("#e_resep_name_dokter").val();
	data['sip_dokter'] = $("#e_resep_sip_dokter").val();
	data['noreg_pasien'] = $("#e_resep_noreg_pasien").val();
	data['nrm_pasien'] = $("#e_resep_nrm_pasien").val();
	data['nama_pasien'] = $("#e_resep_nama_pasien").val();
	data['alamat_pasien'] = $("#e_resep_alamat_pasien").val();
	data['no_telpon'] = $("#e_resep_no_telpon").val();
	data['jenis'] = $("#e_resep_jenis_pasien").val();
	data['perusahaan'] = $("#e_resep_perusahaan_pasien").val();
	data['asuransi'] = $("#e_resep_asuransi_pasien").val();
	data['usia'] = $("#e_resep_usia").val();
	data['berat_badan'] = $("#e_resep_berat_badan").val();
	var row_nums = $("tbody#de_resep_list").children("tr").length;
	var detail = {};
	for (var i = 0; i < row_nums; i++) {
		var d_data = {};
		d_data['id'] = $("tbody#de_resep_list tr:eq(" + i + ") td#id").text();
		d_data['is_racikan'] = $("tbody#de_resep_list tr:eq(" + i + ") td#is_racikan").text();
		d_data['label_racikan'] = $("tbody#de_resep_list tr:eq(" + i + ") td#racikan_num").text();
		d_data['id_obat'] = $("tbody#de_resep_list tr:eq(" + i + ") td#id_obat").text();
		d_data['kode_obat'] = $("tbody#de_resep_list tr:eq(" + i + ") td#kode_obat").text();
		d_data['nama_obat'] = $("tbody#de_resep_list tr:eq(" + i + ") td#nama_obat").text();
		d_data['nama_jenis_obat'] = $("tbody#de_resep_list tr:eq(" + i + ") td#nama_jenis_obat").text();
		d_data['jumlah'] = $("tbody#de_resep_list tr:eq(" + i + ") td#jumlah").text();
		d_data['satuan'] = $("tbody#de_resep_list tr:eq(" + i + ") td#satuan").text();
		d_data['aturan_pakai'] = $("tbody#de_resep_list tr:eq(" + i + ") td#aturan_pakai").text();
		if ($("tbody#de_resep_list tr:eq(" + i + ")").hasClass("deleted"))
			d_data['deleted'] = true;
		else
			d_data['deleted'] = false;
		detail[i] = d_data;
	}
	data['detail'] = JSON.stringify(detail);
	console.log(data);
	$.post(
		"",
		data,
		function() {
			$("#e_resep_table_list").show();
			$("#e_resep_form").hide();
			self.view();
			dismissLoading();
		}
	);
};
EResepAction.prototype.detail = function(id) {
	showLoading();
	var self = this;
	var data = this.getRegulerData();
	data['id'] = id;
	data['command'] = "edit";
	data['readonly'] = 1;
	$.post(
		"",
		data,
		function(response) {
			var json = getContent(response);
			if (json == null) {
				dismissLoading();
				return;
			}
			$("#e_resep_id").val(json.header.id);
			$("#e_resep_tanggal").val(json.header.tanggal);
			$("#e_resep_id_dokter").val(json.header.id_dokter);
			$("#e_resep_name_dokter").val(json.header.nama_dokter);
			$("#e_resep_nama_dokter").val(json.header.nama_dokter);
			$("#e_resep_sip_dokter").val(json.header.sip_dokter);
			$("#e_resep_noreg_pasien").val(json.header.noreg_pasien);
			$("#e_resep_nrm_pasien").val(json.header.nrm_pasien);
			$("#e_resep_nama_pasien").val(json.header.nama_pasien);
			$("#e_resep_alamat_pasien").val(json.header.alamat_pasien);
			$("#e_resep_no_telpon").val(json.header.no_telpon);
			$("#e_resep_jenis_pasien").val(json.header.jenis);
			$("#e_resep_perusahaan_pasien").val(json.header.perusahaan);
			$("#e_resep_asuransi_pasien").val(json.header.asuransi);
			$("#e_resep_usia").val(json.header.usia);
			$("#e_resep_berat_badan").val(json.header.berat_badan);
			$("#e_resep_depo").val(json.header.depo);
			$("tbody#de_resep_list").html(json.detail_html);
			row_num = json.row_num;

			$("#de_resep_is_racikan").val(0);
			$("#de_resep_is_racikan").removeAttr("disabled");
			$("#de_resep_is_racikan").attr("disabled", "disabled");
			$(".de_resep_racikan_num").hide();
			$("#de_resep_row_num").val("");
			$("#de_resep_id_obat").val("");
			$("#de_resep_name_obat").val("");
			$("#de_resep_nama_obat").val("");
			$("#de_resep_nama_obat").removeAttr("disabled");
			$("#de_resep_nama_obat").attr("disabled", "disabled");
			$("#e_resep_obat_browse").hide();
			$("#de_resep_nama_jenis_obat").val("");
			$("#de_resep_sisa").val("");
			$("#de_resep_satuan").val("");
			$("#de_resep_jumlah").val("");
			$("#de_resep_jumlah").removeAttr("disabled");
			$("#de_resep_jumlah").attr("disabled", "disabled");
			$("#de_resep_aturan_pakai").val("");
			$("#de_resep_aturan_pakai").removeAttr("disabled");
			$("#de_resep_aturan_pakai").attr("disabled", "disabled");
			$("#de_resep_form_update").hide();
			$("#de_resep_form_cancel").hide();
			$("#de_resep_form_save").hide();
			
			$("#e_resep_table_list").hide();
			$("#e_resep_form").show();
			$("#e_resep_dokter_browse").hide();
			$("#e_resep_nama_dokter").removeAttr("disabled");
			$("#e_resep_nama_dokter").attr("disabled", "disabled");
			$("#e_resep_nama_dokter").removeClass("smis-one-option-input");
			$("#e_resep_depo").removeAttr("disabled");
			$("#e_resep_depo").attr("disabled", "disabled");
			$("#e_resep_save").hide();
			dismissLoading();
		}
	);
};
EResepAction.prototype.edit = function(id) {
	showLoading();
	var self = this;
	var data = this.getRegulerData();
	data['id'] = id;
	data['command'] = "edit";
	data['readonly'] = 0;
	$.post(
		"",
		data,
		function(response) {
			var json = getContent(response);
			if (json == null) {
				dismissLoading();
				return;
			}
			$("#e_resep_id").val(json.header.id);
			$("#e_resep_tanggal").val(json.header.tanggal);
			$("#e_resep_id_dokter").val(json.header.id_dokter);
			$("#e_resep_name_dokter").val(json.header.nama_dokter);
			$("#e_resep_nama_dokter").val(json.header.nama_dokter);
			$("#e_resep_sip_dokter").val(json.header.sip_dokter);
			$("#e_resep_noreg_pasien").val(json.header.noreg_pasien);
			$("#e_resep_nrm_pasien").val(json.header.nrm_pasien);
			$("#e_resep_nama_pasien").val(json.header.nama_pasien);
			$("#e_resep_alamat_pasien").val(json.header.alamat_pasien);
			$("#e_resep_no_telpon").val(json.header.no_telpon);
			$("#e_resep_jenis_pasien").val(json.header.jenis);
			$("#e_resep_perusahaan_pasien").val(json.header.perusahaan);
			$("#e_resep_asuransi_pasien").val(json.header.asuransi);
			$("#e_resep_usia").val(json.header.usia);
			$("#e_resep_berat_badan").val(json.header.berat_badan);
			$("#e_resep_depo").val(json.header.depo);
			$("tbody#de_resep_list").html(json.detail_html);
			row_num = json.row_num;
			
			$("#de_resep_is_racikan").val(0);
			$("#de_resep_is_racikan").removeAttr("disabled");
			$(".de_resep_racikan_num").hide();
			$("#de_resep_row_num").val("");
			$("#de_resep_id_obat").val("");
			$("#de_resep_name_obat").val("");
			$("#de_resep_nama_obat").val("");
			$("#de_resep_nama_obat").removeAttr("disabled");
			$("#e_resep_obat_browse").show();
			$("#de_resep_nama_jenis_obat").val("");
			$("#de_resep_sisa").val("");
			$("#de_resep_satuan").val("");
			$("#de_resep_jumlah").val("");
			$("#de_resep_aturan_pakai").val("");
			$("#de_resep_form_update").hide();
			$("#de_resep_form_cancel").hide();
			$("#de_resep_form_save").show();
			
			$("#e_resep_table_list").hide();
			$("#e_resep_form").show();
			$("#e_resep_dokter_browse").show();
			$("#e_resep_nama_dokter").removeAttr("disabled");
			$("#e_resep_nama_dokter").addClass("smis-one-option-input");
			$("#e_resep_depo").removeAttr("disabled");
			$("#e_resep_save").show();
			dismissLoading();
		}
	);
};
EResepAction.prototype.lock = function(id) {
	var self = this;
	var data = this.getRegulerData();
	data['command'] = "save";
	data['id'] = id;
	data['locked'] = 1;
	bootbox.confirm(
		"Yakin mengunci E-Resep ini?",
		function(result) {
			if (result) {
				showLoading();
				$.post(
					"",
					data,
					function() {
						self.view();
						dismissLoading();
					}
				);
			}
		}
	);
};
EResepAction.prototype.preview = function(id) {
	showLoading();
	var data = this.getRegulerData();
	data['id'] = id;
	data['command'] = "get_preview_html";
	$.post(
		"",
		data,
		function(response) {
			var json = getContent(response);
			if (json == null) {
				dismissLoading();
				return;
			}
			$("#e_resep_preview_modal .modal-body").html(json.html);
			$("#e_resep_preview_modal").smodal("show");
			dismissLoading();
		}
	);
};
EResepAction.prototype.cancel = function() {
	$("#e_resep_table_list").show();
	$("#e_resep_form").hide();
	e_resep.view();
};