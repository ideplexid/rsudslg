function EResepDokterAction(name, page, action, column) {
	this.initialize(name, page, action, column);
}
EResepDokterAction.prototype.constructor = EResepDokterAction;
EResepDokterAction.prototype = new TableAction();
EResepDokterAction.prototype.selected = function(json) {
	$("#e_resep_id_dokter").val(json.id);
	$("#e_resep_name_dokter").val(json.nama);
	$("#e_resep_nama_dokter").val(json.nama);
	$("#e_resep_sip").val(json.no_ijin);
};