function DEResepAction(name, page, action, column) {
	this.initialize(name, page, action, column);
}
DEResepAction.prototype.constructor = DEResepAction;
DEResepAction.prototype = new TableAction();
DEResepAction.prototype.validate = function() {
	var valid = true;
	var invalid_msg = "";
	$(".error_field").removeClass("error_field");
	var id_obat = $("#de_resep_id_obat").val();
	var jumlah = $("#de_resep_jumlah").val();
	var aturan_pakai = $("#de_resep_aturan_pakai").val();
	if (id_obat == "") {
		valid = false;
		invalid_msg += "</br><strong>Obat</strong> tidak boleh kosong.";
		$("#de_resep_nama_obat").addClass("error_field");
	}
	if (jumlah == "") {
		valid = false;
		invalid_msg += "</br><strong>Jumlah</strong> tidak boleh kosong.";
		$("#de_resep_jumlah").addClass("error_field");
	} else if (!is_numeric(jumlah)) {
		valid = false;
		invalid_msg += "</br><strong>Jumlah</strong> seharusnya numerik (0-9).";
		$("#de_resep_jumlah").addClass("error_field");
	} else if (parseFloat(jumlah) <= 0) {
		valid = false;
		invalid_msg += "</br><strong>Jumlah</strong> seharusnya > 0.";
		$("#de_resep_jumlah").addClass("error_field");
	}
	if (aturan_pakai == "") {
		valid = false;
		invalid_msg += "</br><strong>Aturan Pakai</strong> tidak boleh kosong.";
		$("#de_resep_aturan_pakai").addClass("error_field");
	}
	if (!valid)
		bootbox.alert(invalid_msg);
	return valid;
};
DEResepAction.prototype.refresh_number = function() {
	var r_num = $("tbody#de_resep_list").children("tr").length;
	var num = 1;
	for (var i = 0; i < r_num; i++) {
		if (!$("tbody#de_resep_list tr:eq(" + i + ")").hasClass("deleted")) {
			$("tbody#de_resep_list tr:eq(" + i + ") td#number").html("<small>" + num + "</small>");
			num++;
		}
	}
	if (num == 1) {
		$("tbody#de_resep_list").empty();
		$("tbody#de_resep_list").append(
			"<tr id='temp'><td colspan='10'><center><small><strong>BELUM ADA DETAIL E-RESEP</strong></small></center></td></tr>"
		);
	}
};
DEResepAction.prototype.save = function() {
	if (!this.validate())
		return;
	var r_num = $("#de_resep_row_num").val();
	var is_racikan = $("#de_resep_is_racikan").val();
	var f_is_racikan = is_racikan == "1" ? "&#10003;" : "&#10005;";
	var racikan_num = is_racikan == "1" ? $("#de_resep_racikan_num").val() : "-";
	var id_obat = $("#de_resep_id_obat").val();
	var kode_obat = $("#de_resep_kode_obat").val();
	var nama_obat = $("#de_resep_nama_obat").val();
	var nama_jenis_obat = $("#de_resep_nama_jenis_obat").val();
	var jumlah = $("#de_resep_jumlah").val();
	var f_jumlah = parseFloat(jumlah).formatMoney("0", ".", ",");
	var satuan = $("#de_resep_satuan").val();
	var aturan_pakai = $("#de_resep_aturan_pakai").val();
	if (r_num == "") {
		if ($("#temp").length > 0)
			$("#temp").remove();
		$("tbody#de_resep_list").append(
			"<tr id='data_" + row_num + "'>" +
				"<td id='number'></td>" +
				"<td id='id' style='display: none;'></td>" +
				"<td id='id_obat' style='display: none;'>" + id_obat + "</td>" +
				"<td id='is_racikan' style='display: none;'>" + is_racikan + "</td>" +
				"<td id='f_is_racikan'><center><small>" + f_is_racikan + "</small></center></td>" +
				"<td id='racikan_num'><small>" + racikan_num + "</small></td>" +
				"<td id='kode_obat'><small>" + kode_obat + "</small></td>" +
				"<td id='nama_obat'><small>" + nama_obat + "</small></td>" +
				"<td id='nama_jenis_obat'><small>" + nama_jenis_obat + "</small></td>" +
				"<td id='jumlah' style='display: none;'>" + jumlah + "</td>" +
				"<td id='f_jumlah'><small><div align='right'>" + f_jumlah + "</div></small></td>" +
				"<td id='satuan'><small>" + satuan + "</small></td>" +
				"<td id='aturan_pakai'><small>" + aturan_pakai + "</small></td>" +
				"<td><center>" +
					"<a href='#' onclick='de_resep.delete(" + row_num + ")' class='input btn btn-danger'>" +
						"<i class='fa fa-trash-o'></i>" +
					"</a>" +
				"</center></td>" +
			"</tr>"
		);
		row_num++;
		this.refresh_number();
	}
	$("#de_resep_row_num").val("");
	$("#de_resep_id_obat").val("");
	$("#de_resep_kode_obat").val("");
	$("#de_resep_nama_obat").val("");
	$("#de_resep_nama_jenis_obat").val("");
	$("#de_resep_sisa").val("");
	$("#de_resep_satuan").val("");
	$("#de_resep_jumlah").val("");
	$("#de_resep_aturan_pakai").val("");
	$("#de_resep_nama_obat").focus();
};
DEResepAction.prototype.delete = function(r_num) {
	var id = $("tr#data_" + r_num + " td#id").text();
	if (id == 0 || id == "")
		$("#data_" + r_num).remove();
	else {
		$("#data_" + r_num).addClass("deleted");
		$("#data_" + r_num).hide();
	}
	this.refresh_number();
};