function EResepObatAction(name, page, action, column) {
	this.initialize(name, page, action, column);
}
EResepObatAction.prototype.constructor = EResepObatAction;
EResepObatAction.prototype = new TableAction();
EResepObatAction.prototype.getRegulerData = function() {
	var data = TableAction.prototype.getRegulerData.call(this);
	data['depo_tujuan'] = $("#e_resep_depo").val();
	return data;
};
EResepObatAction.prototype.selected = function(json) {
	$("#de_resep_id_obat").val(json.id);
	$("#de_resep_kode_obat").val(json.kode_obat);
	$("#de_resep_name_obat").val(json.nama_obat);
	$("#de_resep_nama_obat").val(json.nama_obat);
	$("#de_resep_nama_jenis_obat").val(json.nama_jenis_obat);
	$("#de_resep_sisa").val(json.sisa);
	$("#de_resep_satuan").val(json.satuan);
	$("#de_resep_jumlah").focus();
};