<?php 
	require_once ("smis-framework/smis/template/InstallatorTemplate.php");
	class InventoryUninstallator extends InstallatorTemplate {
		private $is_use_obat;
		private $is_use_inventory;
		private $is_use_barang;
		private $property_barang;
		private $property_obat;
		
		public function __construct($db, $slug, $code) {
			parent::__construct ( $db, $slug, $code );
			$this->is_use_barang=true;
			$this->is_use_inventory=true;
			$this->is_use_obat=true;
			$this->property_barang=array();
			$this->property_obat=array();
		}
		
		public function setPropertyObat($name,$val){
			$this->property_obat[$name]=$val;
		}
		
		public function setPropertyBarang($name,$val){
			$this->property_barang[$name]=$val;
		}
		
		public function setUsing($obat,$barang,$inventory){
			$this->is_use_barang=$barang;
			$this->is_use_inventory=$inventory;
			$this->is_use_obat=$obat;
		}
		
		public function extendInstall($dbslug){
						
		}
		

		
		public function getRKBUTable($dbslug){
			$query = "
				DROP TABLE IF EXISTS `smis_".$dbslug."_rkbu".$this->entity."`;
			";
			return $query;
		}
		
		public function getDRKBUTable($dbslug){
			$query = "
				DROP TABLE IF EXISTS `smis_".$dbslug."_drkbu".$this->entity."`;
			";
			return $query;
		}

		public function getBarangMasuk($dbslug){
			$query = "
				DROP TABLE IF EXISTS `smis_".$dbslug."_barang_masuk".$this->entity."`;
			";
			return $query;
		}
		
		public function getStokBarang($dbslug){
			$query = "
				DROP TABLE IF EXISTS `smis_".$dbslug."_stok_barang".$this->entity."`;
			";
			return $query;
		} 
		
		public function getRiwayatStokBarang($dbslug){
			$query = "
				DROP TABLE IF EXISTS `smis_".$dbslug."_riwayat_stok_barang".$this->entity."`;
			";
			return $query;
		} 

		public function getObatMasuk($dbslug){
			$query = "
				DROP TABLE IF EXISTS `smis_".$dbslug."_obat_masuk".$this->entity."`;
			";
			return $query;
		}
		
		public function getStokObat($dbslug){
			$query="
				DROP TABLE IF EXISTS `smis_".$dbslug."_stok_obat".$this->entity."`;
			";
			return $query;
		}
		
		public function getRiwayatStokObat($dbslug){
			$query = "
				DROP TABLE IF EXISTS `smis_".$dbslug."_riwayat_stok_obat".$this->entity."`;
			";
			return $query;
		}
		
		public function getInventaris($dbslug){
			$query = "
				DROP TABLE IF EXISTS `smis_".$dbslug."_inventaris".$this->entity."`;
			";
			return $query;
		}
			
		public function getPenggunaanBarang($dbslug){
			$query = "
				DROP TABLE IF EXISTS `smis_".$dbslug."_penggunaan_barang".$this->entity."`;
			";
			return $query;
		}
		
		public function getPenggunaanObat($dbslug){
			$query = "
				DROP TABLE IF EXISTS `smis_".$dbslug."_penggunaan_obat".$this->entity."`;
			";
			return $query;
		}
		
		public function getReturBarang($dbslug){
			$query = "
				DROP TABLE IF EXISTS `smis_".$dbslug."_retur_barang".$this->entity."`;
			";
			return $query;
		}
		
		public function getReturObat($dbslug){
			$query = "
				DROP TABLE IF EXISTS `smis_".$dbslug."_retur_obat".$this->entity."`;
			";
			return $query;
		}
		
		public function getPenyesuaianStokBarang($dbslug){
			$query = "
				DROP TABLE IF EXISTS `smis_".$dbslug."_penyesuaian_stok_barang".$this->entity."`;
			";
			return $query;
		}
		
		public function getPenyesuaianStokObat($dbslug){
			$query = "
				DROP TABLE IF EXISTS `smis_".$dbslug."_penyesuaian_stok_obat".$this->entity."`;
			";
			return $query;
		}
		
		public function getPermintaanBarang($dbslug){
			$query = "
				DROP TABLE IF EXISTS `smis_".$dbslug."_permintaan_barang".$this->entity."`;
			";
			return $query;
		}
		
		public function getDPermintaanBarang($dbslug){
			$query = "
				DROP TABLE IF EXISTS `smis_".$dbslug."_dpermintaan_barang".$this->entity."`;
			";
			return $query;
		}
		
		public function getPermintaanObat($dbslug){
			$query = "
				DROP TABLE IF EXISTS `smis_".$dbslug."_permintaan_obat".$this->entity."`;
			";
			return $query;
		}
		
		public function getDPermintaanObat($dbslug){
			$query = "
				DROP TABLE IF EXISTS `smis_".$dbslug."_dpermintaan_obat".$this->entity."`;
			";
			return $query;
		}
		
		public function getMinMaksSettings($dbslug){
			$query = "
				DROP TABLE IF EXISTS `smis_".$dbslug."_stok_barang_minimum_maksimum".$this->entity."`;
			";
			return $query;
		}
	}
?>