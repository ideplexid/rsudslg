<?php 
	require_once 'smis-framework/smis/template/ModulTemplate.php';

	class PenyesuaianObat extends ModulTemplate {
		private $db;
		private $name;
		private $entity;
		private $dbslug;
		private $proto_name;
		private $proto_implement;
		private $proto_slug;

		private $obat_masuk;
		private $stok_obat;
		private $penyesuaian_obat;
		private $riwayat_stok_obat;
		private $kartu_stok_obat;
		
		private $is_use_obat;
		
		private $obat_property;
		
		public function __construct($db, $name, $entity, $dbslug){
			$this->db=$db;
			$this->name=$name;
			$this->entity=$entity;
			$this->dbslug=$dbslug;
			$this->proto_name="";
			$this->proto_slug="";
			$this->proto_implement="";
			$this->is_use_obat=true;
			$this->obat_property=array();
			
			$this->obat_masuk="smis_".$dbslug."_obat_masuk";
			$this->stok_obat="smis_".$dbslug."_stok_obat";
			$this->penyesuaian_obat="smis_".$dbslug."_penyesuaian_stok_obat";
			$this->riwayat_stok_obat="smis_".$dbslug."_riwayat_stok_obat";
			$this->kartu_stok_obat="smis_".$dbslug."_kartu_stok_obat";	
		}
		
		public function setPrototype($pname,$pslug,$pimplement){
			$this->proto_name=$pname;
			$this->proto_implement=$pimplement;
			$this->proto_slug=$pslug;
			
			$this->obat_masuk="smis_".$this->dbslug."_obat_masuk_".$pslug;
			$this->stok_obat="smis_".$this->dbslug."_stok_obat_".$pslug;
			$this->penyesuaian_obat="smis_".$this->dbslug."_penyesuaian_stok_obat_".$pslug;
			$this->riwayat_stok_obat="smis_".$this->dbslug."_riwayat_stok_obat_".$pslug;
			$this->kartu_stok_obat="smis_".$this->dbslug."_kartu_stok_obat_".$pslug;
		}
		
		public function initialize(){
			$action=$_POST['action'];
			if($action=="penyesuaian_stok_obat"){
				$this->phpPreLoad();
				return;
			}
			
			switch ($action){
				case "detail_stok_obat" : $this->detailObat(); break;
				case "riwayat_penyesuaian_stok_obat" : $this->riwayatObat(); break;
			}
			
		}
		
		public function phpPreLoad(){		
			$penyesuaian_stok_obat=new Tabulator("penyesuaian_stok_obat", "");

			if($this->is_use_obat){
				$obat=new Tabulator("obat","",Tabulator::$LANDSCAPE);
				if(!isset($this->obat_property['detail_stok_obat']) || $this->obat_property['detail_stok_obat']){
					ob_start(); 
					$this->detailObat(); 
					$detail_stok_obat=ob_get_clean();
					$obat->add("detail_stok_obat", "Penyesuaian", $detail_stok_obat, Tabulator::$TYPE_HTML, "fa fa-adjust");
				}
				if(!isset($this->obat_property['riwayat_penyesuaian_stok_obat']) || $this->obat_property['riwayat_penyesuaian_stok_obat']){
					ob_start(); 
					$this->riwayatObat(); 
					$riwayat_penyesuaian_stok_obat = ob_get_clean();
					$obat->add("riwayat_penyesuaian_stok_obat", "Riwayat Penyesuaian", $riwayat_penyesuaian_stok_obat,Tabulator::$TYPE_HTML, "fa fa-history");
				}
				if($obat->getTotalElement()>0)
					$penyesuaian_stok_obat->add("obat_tabs", "Obat", $obat, Tabulator::$TYPE_COMPONENT,"fa fa-eyedropper");
			}
			echo $penyesuaian_stok_obat->getHtml();
		}

		public function detailObat(){
			require_once 'smis-libs-inventory/modul/PenyesuaianStokObat.php';
			$df=new PenyesuaianStokObat($this->db, $this->name, $this->penyesuaian_obat, $this->stok_obat, $this->riwayat_stok_obat, $this->obat_masuk, $this->entity);
			$df->setPrototype($this->proto_name, $this->proto_slug, $this->proto_implement);
			$df->initialize();
		}
		
		public function riwayatObat(){
			require_once 'smis-libs-inventory/modul/RiwayatPenyesuaianStokObat.php';
			$df=new RiwayatPenyesuaianStokObat($this->db, $this->name, $this->penyesuaian_obat, $this->stok_obat, $this->entity);
			$df->setPrototype($this->proto_name, $this->proto_slug, $this->proto_implement);
			$df->initialize();
		}
	}
?>