<?php 
	require_once 'smis-libs-class/Policy.php';

	class InventoryPolicy extends Policy {
		public function __construct($page, $user,$prefix=""){
			parent::__construct($page, $user);
			$this->addPolicy("boi", "boi", Policy::$DEFAULT_COOKIE_CHANGE,$prefix."boi");
			$this->addPolicy("mutasi_masuk_unit", "mutasi_masuk_unit", Policy::$DEFAULT_COOKIE_CHANGE,$prefix."mutasi_masuk_unit");
			$this->addPolicy("mutasi_keluar_unit", "mutasi_keluar_unit", Policy::$DEFAULT_COOKIE_CHANGE,$prefix."mutasi_keluar_unit");
			$this->addPolicy("rkbu", "rkbu", Policy::$DEFAULT_COOKIE_CHANGE,$prefix."rkbu");
			$this->addPolicy("pengaturan_stok_minmaks", "pengaturan_stok_minmaks", Policy::$DEFAULT_COOKIE_CHANGE,$prefix."pengaturan_stok_minmaks");
			
			$this->addPolicy("daftar_inventaris", "boi", Policy::$DEFAULT_COOKIE_KEEP, $prefix."boi");
			
			$this->addPolicy("daftar_barang", "boi", Policy::$DEFAULT_COOKIE_KEEP, $prefix."boi");
			$this->addPolicy("riwayat_stok_barang", "boi", Policy::$DEFAULT_COOKIE_KEEP, $prefix."boi");
			$this->addPolicy("kartu_stok_barang", "boi", Policy::$DEFAULT_COOKIE_KEEP, $prefix."boi");
			$this->addPolicy("permintaan_barang", "boi", Policy::$DEFAULT_COOKIE_KEEP, $prefix."boi");
			$this->addPolicy("barang_masuk", "boi", Policy::$DEFAULT_COOKIE_KEEP, $prefix."boi");
			$this->addPolicy("penggunaan_barang", "boi", Policy::$DEFAULT_COOKIE_KEEP, $prefix."boi");
			$this->addPolicy("retur_barang", "boi", Policy::$DEFAULT_COOKIE_KEEP, $prefix."boi");
			$this->addPolicy("pengecekan_stok_barang_ed", "boi", Policy::$DEFAULT_COOKIE_KEEP, $prefix."boi");
			$this->addPolicy("detail_stok_barang", "boi", Policy::$DEFAULT_COOKIE_KEEP, $prefix."boi");
			$this->addPolicy("riwayat_penyesuaian_stok_barang", "boi", Policy::$DEFAULT_COOKIE_KEEP, $prefix."boi");
			$this->addPolicy("laporan_so_barang", "boi", Policy::$DEFAULT_COOKIE_KEEP, $prefix."boi");
			
			$this->addPolicy("daftar_obat", "boi", Policy::$DEFAULT_COOKIE_KEEP, $prefix."boi");
			$this->addPolicy("riwayat_stok_obat", "boi", Policy::$DEFAULT_COOKIE_KEEP, $prefix."boi");
			$this->addPolicy("kartu_stok_obat", "boi", Policy::$DEFAULT_COOKIE_KEEP, $prefix."boi");
			$this->addPolicy("permintaan_obat", "boi", Policy::$DEFAULT_COOKIE_KEEP, $prefix."boi");
			$this->addPolicy("obat_masuk", "boi", Policy::$DEFAULT_COOKIE_KEEP, $prefix."boi");
			$this->addPolicy("penggunaan_obat", "boi", Policy::$DEFAULT_COOKIE_KEEP, $prefix."boi");
			$this->addPolicy("retur_obat", "boi", Policy::$DEFAULT_COOKIE_KEEP, $prefix."boi");
			$this->addPolicy("pengecekan_stok_obat_ed", "boi", Policy::$DEFAULT_COOKIE_KEEP, $prefix."boi");
			$this->addPolicy("laporan_so_obat", "boi", Policy::$DEFAULT_COOKIE_KEEP, $prefix."boi");

			if (strpos($this->page, "depo_farmasi") === false) {
				$this->addPolicy("detail_stok_obat", "boi", Policy::$DEFAULT_COOKIE_KEEP, $prefix."boi");
				$this->addPolicy("riwayat_penyesuaian_stok_obat", "boi", Policy::$DEFAULT_COOKIE_KEEP, $prefix."boi");
			}
			$this->addPolicy("penyesuaian_stok_obat", "penyesuaian_stok_obat", Policy::$DEFAULT_COOKIE_CHANGE, $prefix . "penyesuaian_stok_obat");
			if (strpos($this->page, "depo_farmasi") !== false) {
				$this->addPolicy("detail_stok_obat", "penyesuaian_stok_obat", Policy::$DEFAULT_COOKIE_KEEP, $prefix."penyesuaian_stok_obat");
				$this->addPolicy("riwayat_penyesuaian_stok_obat", "penyesuaian_stok_obat", Policy::$DEFAULT_COOKIE_KEEP, $prefix."penyesuaian_stok_obat");	
			}
		}	
	}
?>