<?php 
require_once 'smis-framework/smis/template/ModulTemplate.php';
class CronjobStockBarangED extends ModulTemplate{
	
	private $tbl_stok_barang;
	private $notification;
	private $db;
	private $entity;
	public function __construct($db,$notification,$tbl_stok_barang,$entity){
		$this->tbl_stok_barang=$tbl_stok_barang;
		$this->db=$db;
		$this->notification=$notification;
		$this->entity=$entity;
	}
	
	public function setEntity($entity,$tbl_stok_barang){
		$this->tbl_stok_barang=$tbl_stok_barang;
		$this->entity=$entity;
	}
	
	public function initialize(){
		$stok_barang_dbtable = new DBTable($this->db, $this->tbl_stok_barang);
		$stok_barang_dbtable->addCustomKriteria(" DATEDIFF(tanggal_exp, CURDATE()) ", " <= 30 ");
		$stok_barang_dbtable->setOrder(" tanggal_exp ");
		$stok_barang_dbtable->setMaximum(3);
		$stok_barang_pack = $stok_barang_dbtable->view("", 0);
		$stok_barang_data = $stok_barang_pack['data'];
		$message = "";
		$i = 0;
		foreach($stok_barang_data as $stok) {
			$message .= $stok->nama_barang;
			if ($i > 0)
				$message .= ",";
			$message .= " ";
			$i++;
		}
		$message .= "mendekati ED dalam waktu <= 30 hari.";
		$type = "Stok Barang ED";
		$code = $this->entity."-barang-ed-" . date("Y-m-d") . $message;
		if ($stok_barang_data != null && count($stok_barang_data) > 0) {
			global $notification;
			$notification->addNotification($type, md5($code), $message, $this->entity, "pengecekan_stok_ed");
		}
	}
}
	
?>