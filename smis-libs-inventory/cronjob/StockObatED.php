<?php 
require_once 'smis-framework/smis/template/ModulTemplate.php';

class CronjobStockObatED extends ModulTemplate{
	
	private $tbl_stok_obat;
	private $notification;
	private $db;
	private $entity;
	public function __construct($db,$notification,$tbl_stok_obat,$entity){
		$this->tbl_stok_obat=$tbl_stok_obat;
		$this->db=$db;
		$this->notification=$notification;
		$this->entity=$entity;
	}

	public function setEntity($entity,$tbl_stok_obat){
		$this->tbl_stok_obat=$tbl_stok_obat;
		$this->entity=$entity;
	}
	
	public function initialize(){
		$stok_obat_dbtable = new DBTable($this->db, $this->tbl_stok_obat);
		$stok_obat_dbtable->addCustomKriteria(" DATEDIFF(tanggal_exp, CURDATE()) ", " <= 30 ");
		$stok_obat_dbtable->setOrder(" tanggal_exp ");
		$stok_obat_dbtable->setMaximum(3);
		$stok_obat_pack = $stok_obat_dbtable->view("", 0);
		$stok_obat_data = $stok_obat_pack['data'];
		$message = "";
		$i = 0;
		foreach($stok_obat_data as $stok) {
			$message .= $stok->nama_obat;
			if ($i > 0)
				$message .= ",";
			$message .= " ";
			$i++;
		}
		$message .= "mendekati ED dalam waktu <= 30 hari.";
		$type = "Stok obat ED";
		$code = $this->entity."-obat-ed-" . date("Y-m-d") . $message;
		if ($stok_obat_data != null && count($stok_obat_data) > 0) {
			global $notification;
			$notification->addNotification($type, md5($code), $message, $this->entity, "pengecekan_stok_ed");
		}
	}
}
	
?>