<?php 
	class PenggunaanBarangAdapter extends ArrayAdapter {
		public function adapt($row) {
			$array = array();
			$array['id'] = $row->id;
			$array['Nomor'] = self::format("digit8", $row->id);
			$array['Tanggal'] = self::format("date d M Y", $row->tanggal);
			$array['Barang'] = $row->nama_barang;
			$array['jenis'] = $row->nama_jenis_barang;
			$array['Jumlah'] = $row->jumlah . " " . $row->satuan;
			return $array;
		}
	}
?>