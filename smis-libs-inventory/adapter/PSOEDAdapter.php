<?php 
	class PSOEDAdapter extends ArrayAdapter {
		public function adapt($row) {
			$array = array();
			$array['Nomor'] = self::format("digit8", $row->id);
			$array['Obat'] = $row->nama_obat;
			$array['Jenis Obat'] = $row->nama_jenis_obat;
			$array['Produsen'] = $row->produsen;
			$array['Vendor'] = $row->nama_vendor;
			$array['Jumlah'] = $row->jumlah . " " . $row->satuan;
			$array['Ket. Jumlah'] = "1 " . $row->satuan . " = " . $row->konversi . " " . $row->satuan_konversi;
			$array['Tgl. Exp.'] = self::format("date d M Y", $row->tanggal_exp);
			return $array;
		}
	}
?>