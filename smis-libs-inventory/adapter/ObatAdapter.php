<?php 
	class ObatAdapter extends ArrayAdapter {
		public function adapt($row) {
			$array = array();
			$array['id'] = $row->id;
			$array['Nomor'] = self::format("digit8", $row->id);
			$array['Obat'] = $row->nama_obat;
			$array['Jenis'] = $row->nama_jenis_obat;
			$array['Stok'] = $row->sisa." ".$row->satuan;
			if ($row->tanggal_exp == "0000-00-00")
				$array['Tanggal Exp.'] = "-";
			else
				$array['Tanggal Exp.'] = self::format("date d M Y", $row->tanggal_exp);
			return $array;
		}
	}
?>