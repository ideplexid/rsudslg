<?php 
	class RiwayatPenyesuaianStokBarangAdapter extends ArrayAdapter {
		public function adapt($row) {
			$array = array();
			$array['Tanggal'] = self::format("date d M Y", $row->tanggal);
			$array['Nama Barang'] = $row->nama_barang;
			$array['Jenis Barang'] = $row->nama_jenis_barang;
			$array['Jml. Awal'] = $row->jumlah_lama . " " . $row->satuan;
			$array['Jml. Aktual'] = $row->jumlah_baru . " " . $row->satuan;
			$sign = "";
			if ($row->jumlah_baru - $row->jumlah_lama > 0) {
				$sign = "+";
			}
			$array['Selisih'] = $sign . ($row->jumlah_baru - $row->jumlah_lama) . " " . $row->satuan;
			$array['Keterangan'] = $row->keterangan;
			$array['Petugas Entri'] = $row->nama_user;
			return $array;
		}
	}
?>