<?php 
	class StokBarangAdapter extends ArrayAdapter {
		public function adapt($row) {
			$array = array();
			$array['id'] = $row->id;
			$array['No. Stok'] = self::format("digit8", $row->id);
			$array['Nama Barang'] = $row->nama_barang;
			$array['Jenis Barang'] = $row->nama_jenis_barang;
			$array['Produsen'] = $row->produsen;
			$array['Vendor'] = $row->nama_vendor;
			$array['Satuan'] = $row->satuan;
			if ($row->tanggal_exp == "0000-00-00")
				$array['Tgl. ED'] = "-";
			else
				$array['Tgl. ED'] = self::format("date d M Y", $row->tanggal_exp);
			return $array;
		}
	}
?>