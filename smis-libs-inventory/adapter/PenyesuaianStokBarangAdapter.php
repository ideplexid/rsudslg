<?php 
	class PenyesuaianStokBarangAdapter extends ArrayAdapter {
		public function adapt($row) {
			$array = array();
			$array['id'] = $row->id;
			$array['Nomor'] = self::format("digit8", $row->id);
			$array['Barang'] = $row->nama_barang;
			$array['Jenis'] = $row->nama_jenis_barang;
			$array['Stok'] = $row->sisa . " " . $row->satuan;
			$array['Keterangan'] = "1 " . $row->satuan . " = " . $row->konversi . " " . $row->satuan_konversi;
			if ($row->tanggal_exp == "0000-00-00")
				$array['Tanggal Exp.'] = "-";
			else
				$array['Tanggal Exp.'] = self::format("date d M Y", $row->tanggal_exp);
			return $array;
		}
	}
?>