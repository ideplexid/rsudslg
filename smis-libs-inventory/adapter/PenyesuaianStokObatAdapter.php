<?php 
	class PenyesuaianStokObatAdapter extends ArrayAdapter {
		public function adapt($row) {
			$array = array();
			$array['id'] = $row->id;
			$array['No. Stok'] = self::format("digit8", $row->id);
			$array['No. Mutasi'] = self::format("digit8", $row->f_id);
			$array['Tanggal Masuk'] = self::format("date d-m-Y", $row->tanggal_masuk);
			$array['Obat'] = $row->nama_obat;
			$array['Jenis'] = $row->nama_jenis_obat;
			$array['Stok'] = $row->sisa . " " . $row->satuan;
			$array['Keterangan'] = "1 " . $row->satuan . " = " . $row->konversi . " " . $row->satuan_konversi;
			if ($row->tanggal_exp == "0000-00-00")
				$array['Tanggal Exp.'] = "-";
			else
				$array['Tanggal Exp.'] = self::format("date d M Y", $row->tanggal_exp);
			return $array;
		}
	}
?>