<?php 
	class PenggunaanObatAdapter extends ArrayAdapter {
		public function adapt($row) {
			$array = array();
			$array['id'] = $row->id;
			$array['Nomor'] = self::format("digit8", $row->id);
			$array['Tanggal'] = self::format("date d M Y", $row->tanggal);
			$array['Obat'] = $row->nama_obat;
			$array['jenis'] = $row->nama_jenis_obat;
			$array['Jumlah'] = $row->jumlah . " " . $row->satuan;
			return $array;
		}
	}
?>