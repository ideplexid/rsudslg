<?php 
	class BarangMasukAdapter extends ArrayAdapter {
		public function adapt($row) {
			$array = array();
			$array['id'] = $row->id;
			$array['status'] = $row->status;
			$array['No. Mutasi'] = self::format("digit8", $row->f_id);
			$array['Tanggal Masuk'] = self::format("date d M Y", $row->tanggal);
			$array['Gudang'] = self::format("unslug", $row->unit);
			if ($row->status == "sudah")
				$array['Status'] = "Sudah Diterima";
			else if ($row->status == "dikembalikan")
				$array['Status'] = "Dikembalikan";
			else
				$array['Status'] = "Belum Diterima";
			return $array;
		}
	}
?>