<?php 
	class ReturObatAdapter extends ArrayAdapter {
		public function adapt($row) {
			$array = array();
			$array['id'] = $row->id;
			$array['status'] = $row->status;
			$array['Nomor'] = self::format("digit8", $row->id);
			$array['Tanggal Retur'] = self::format("date d M Y", $row->tanggal);
			$array['Obat'] = $row->nama_obat;
			$array['Jenis'] = $row->nama_jenis_obat;
			$array['Jumlah'] = $row->jumlah . " " . $row->satuan;
			if ($row->status == "belum") {
				$array['Status'] = "Belum Diterima";
			} else if ($row->status == "sudah") {
				$array['Status'] = "Sudah Diterima";
			} else if ($row->status == "dikembalikan") {
				$array['Status'] = "Dikembalikan";
			}
			return $array;
		}
	}
?>