<?php 
	class PermintaanBarangAdapter extends ArrayAdapter {
		public function adapt($row) {
			$array = array();
			$array['id'] = $row->id;
			$array['status'] = $row->status;
			$array['Nomor'] = self::format("digit8", $row->id);
			$array['Tanggal'] = self::format("date d M Y", $row->tanggal);
			if ($row->status == "belum") {
				$array['Status'] = "Belum Diterima";
			} else if ($row->status == "sudah") {
				$array['Status'] = "Sudah Diterima";
			} else if ($row->status == "dikembalikan") {
				$array['Status'] = "Dikembalikan";
			}
			return $array;
		}
	}
?>