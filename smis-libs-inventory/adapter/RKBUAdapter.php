<?php 
	class RKBUAdapter extends ArrayAdapter {
		public function adapt($row) {
			$array = array();
			$array['id'] = $row->id;
			$array['disetujui'] = $row->disetujui;
			$array['locked'] = $row->locked;
			$array['Nomor'] = self::format("digit8", $row->id);
			$array['Tahun RKBU'] = $row->tahun;
			$array['Tanggal Diajukan'] = self::format("date d M Y ", $row->tanggal_diajukan);
			if ($row->tanggal_ditinjau != null)
				$array['Tanggal Ditinjau'] = self::format("date d M Y", $row->tanggal_ditinjau);
			else
				$array['Tanggal Ditinjau'] = "-";
			if ($row->locked == false && $row->disetujui == "belum")
				$array['Status'] = "Belum Diajukan";
			else if($row->locked == false && $row->disetujui == "revisi")
				$array['Status'] = "Revisi";
			else if ($row->locked == true && $row->disetujui == "sudah")
				$array['Status'] = "Disetujui";
			else if ($row->locked == true && $row->disetujui == "belum")
				$array['Status'] = "Belum Ditinjau";
			else if($row->locked == true && $row->disetujui == "revisi")
				$array['Status'] = "Revisi Belum Ditinjau";
			return $array;
		}
	}
?>