<?php 
require_once ("smis-framework/smis/template/NavigatorTemplate.php");
class InventoryNavigator extends NavigatorTemplate {
	public function __construct($navigation, $tooltip, $name, $page) {
		parent::__construct ( $navigation, $tooltip, $name, $page,"fa fa-hospital-o" );
	}
	public function extendMenu(Menu $mr,$pname="",$pslug="",$pimplement="") {
		$mr->addSeparator();
		if (strpos($this->page, "depo_farmasi") !== false)
			$mr->addSubMenu("Penyesuaian Stok Obat", $this->page, "penyesuaian_stok_obat", "Data Penyesuaian Stok Obat", "fa fa-pencil", $pname,$pslug, $pimplement);
		$mr->addSubMenu("Rencana Kebutuhan Barang", $this->page, "rkbu", "Data Rencana Kebutuhan Barang Unit", "fa fa-book", $pname, $pslug, $pimplement);
		$mr->addSubMenu("Mutasi Obat Antar-Unit - Masuk", $this->page, "mutasi_masuk_unit", "Data Mutasi Obat Antar-Unit Masuk", "fa fa-reply", $pname, $pslug, $pimplement);
		$mr->addSubMenu("Mutasi Obat Antar-Unit - Keluar", $this->page, "mutasi_keluar_unit", "Data Mutasi Obat Antar-Unit Keluar", "fa fa-mail-forward", $pname, $pslug, $pimplement);
		$mr->addSubMenu("Pengaturan Stok", $this->page, "pengaturan_stok_minmaks", "Pengaturan Stok Minimum dan Maksimum", "fa fa-unsorted",$pname,$pslug,$pimplement);
		$mr->addSubMenu("Barang, Obat, Inventaris", $this->page, "boi", "Barang Obat dan Inventaris", "fa fa-cubes",$pname,$pslug,$pimplement);
		return $mr;
	}
}
?>