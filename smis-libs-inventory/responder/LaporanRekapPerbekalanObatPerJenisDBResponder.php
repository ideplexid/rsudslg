<?php 
	class LaporanRekapPerbekalanObatPerJenisDBResponder extends DBResponder {
		private $unit;
		private $tbl_stok_obat;
		private $tbl_obat_masuk;
		
		public function __construct($dbtable, $uitable, $adapter, $tbl_obat_masuk, $tbl_stok_obat, $unit){
			parent::__construct($dbtable, $uitable, $adapter);
			$this->tbl_stok_obat = $tbl_stok_obat;
			$this->tbl_obat_masuk = $tbl_obat_masuk;
			$this->unit = $unit;
		}
		
		public function command($command) {
			if ($command != "print_laporan") {
				return parent::command($command);
			}
			$pack = null;
			if ($command == "print_laporan") {
				$pack = new ResponsePackage();
				$content = $this->print_laporan();
				$pack->setContent($content);
				$pack->setStatus(ResponsePackage::$STATUS_OK);
			}
			return $pack->getPackage();
		}
		
		public function print_laporan() {
			$nama_jenis_obat = $_POST['nama_jenis_obat'];
			$print_data = "<center><b>LAPORAN REKAP PERBEKALAN OBAT PER JENIS (" . ArrayAdapter::format("unslug", $this->unit) . ")</b></center><br/>";
			$print_data .= "<table border='0'>
								<tr>
									<td>Tanggal</td>
									<td>:</td>
									<td>" . ArrayAdapter::format("date d M Y", date("Y-m-d")) . "</td>
								</tr>
								<tr>
									<td>Jam</td>
									<td>:</td>
									<td>" . date("H:i:s") . "</td>
								</tr>
								<tr>
									<td>Jenis</td>
									<td>:</td>
									<td>" . $nama_jenis_obat . "</td>
								</tr>
							</table>";
			$data_perbekalan = $this->dbtable->get_result("
				SELECT *
				FROM (
					SELECT " . $this->tbl_stok_obat . ".nama_obat AS 'id', " . $this->tbl_stok_obat . ".nama_obat, " . $this->tbl_stok_obat . ".nama_jenis_obat, SUM(" . $this->tbl_stok_obat . ".sisa) AS 'sisa', " . $this->tbl_stok_obat . ".satuan, " . $this->tbl_stok_obat . ".konversi, " . $this->tbl_stok_obat . ".satuan_konversi, " . $this->tbl_stok_obat . ".produsen, " . $this->tbl_stok_obat . ".tanggal_exp, " . $this->tbl_stok_obat . ".hna
					FROM " . $this->tbl_stok_obat . " LEFT JOIN " . $this->tbl_obat_masuk . " ON " . $this->tbl_stok_obat . ".id_obat_masuk = " . $this->tbl_obat_masuk . ".id
					WHERE " . $this->tbl_stok_obat . ".prop NOT LIKE 'del' AND " . $this->tbl_obat_masuk . ".prop NOT LIKE 'del' AND " . $this->tbl_obat_masuk . ".status = 'sudah' AND " . $this->tbl_stok_obat . ".nama_jenis_obat = '" . $nama_jenis_obat . "'
					GROUP BY " . $this->tbl_stok_obat . ".id_obat, " . $this->tbl_stok_obat . ".satuan, " . $this->tbl_stok_obat . ".konversi, " . $this->tbl_stok_obat . ".satuan_konversi, " . $this->tbl_stok_obat . ".produsen, " . $this->tbl_stok_obat . ".tanggal_exp, " . $this->tbl_stok_obat . ".hna
					HAVING sisa > 0
					ORDER BY " . $this->tbl_stok_obat . ".nama_obat, " . $this->tbl_stok_obat . ".nama_jenis_obat, " . $this->tbl_stok_obat . ".satuan, " . $this->tbl_stok_obat . ".produsen, " . $this->tbl_stok_obat . ".tanggal_exp ASC
				) v_stok
			");
			$print_data .= "<table border='1'>
								<tr>
									<th>No.</th>
									<th>Nama Obat</th>
									<th>Jenis Obat</th>
									<th>Jumlah</th>
									<th>Satuan</th>
									<th>Produsen</th>
									<th>Tgl. Exp.</th>
									<th>H. Netto</th>
									<th>Total H. Netto</th>
								</tr>";
			$total_hna = 0;
			$no = 1;
			if (count($data_perbekalan) > 0) {
				foreach($data_perbekalan as $dp) {
					$total_hna += ($dp->sisa * $dp->hna);
					$print_data .= "<tr>
										<td>" . $no++ . "</td>
										<td>" . $dp->nama_obat . "</td>
										<td>" . $dp->nama_jenis_obat . "</td>
										<td>" . $dp->sisa . "</td>
										<td>" . $dp->satuan . "</td>
										<td>" . $dp->produsen . "</td>
										<td>" . ArrayAdapter::format("date d M Y", $dp->tanggal_exp) . "</td>
										<td>" . ArrayAdapter::format("money Rp.", $dp->hna) . "</td>
										<td>" . ArrayAdapter::format("money Rp.", $dp->sisa * $dp->hna) . "</td>
									</tr>";
				}
			} else {
				$print_data .= "<tr>
									<td colspan='9'>Tidak terdapat data perbekalan obat</td>
								</tr>";
			}
			$print_data .= "<tr>
								<td align='center' colspan='8'><strong>T O T A L</strong></td>
								<td><strong>" . ArrayAdapter::format("money Rp.", $total_hna) . "</strong></td>
							</tr>";
			$print_data .= "</table><br/>";
			global $user;
			$print_data .= "<table border='0' align='center'>
								<tr>
									<td colspan='3' align='center'>Mengetahui :</td>
								</tr>
								<tr>
									<td>&nbsp;</td>
								</tr>
								<tr>
									<td align='center'>Tuban, " . ArrayAdapter::format("date d M Y", date("Y-m-d")) . "</td>
									<td>&Tab;</td>
									<td>&nbsp;</td>
								</tr>
								<tr>
									<td align='center'>KEPALA RSUD SYARIFAH AMBAMI RATO EBU</td>
									<td>&Tab;</td>
									<td align='center'>KEPALA " . ArrayAdapter::format("unslug", $this->unit) . "</td>
								</tr>
								<tr>
									<td>&nbsp;</td>
									<td>&Tab;</td>
									<td>&nbsp;</td>
								</tr>
								<tr>
									<td>&nbsp;</td>
									<td>&Tab;</td>
									<td>&nbsp;</td>
								</tr>
								<tr>
									<td>&nbsp;</td>
									<td>&Tab;</td>
									<td>&nbsp;</td>
								</tr>
								<tr>
									<td>&nbsp;</td>
									<td>&Tab;</td>
									<td>&nbsp;</td>
								</tr>
								<tr>
									<td align='center'><b>drg. Yusro</b></td>
									<td>&Tab;</td>
									<td align='center'>(_____________________)</td>
								</tr>
							</table>";
			return $print_data;
		}
	}
?>