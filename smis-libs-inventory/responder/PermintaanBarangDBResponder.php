<?php 
	require_once("smis-base/smis-include-duplicate.php");

	class PermintaanBarangDBResponder extends DuplicateResponder {
		private $dpermintaan_barang;
		private $unit;
		
		public function __construct($dbtable, $uitable, $adapter, $dpermintaan_barang, $unit){
			parent::__construct($dbtable, $uitable, $adapter);
			$this->dpermintaan_barang = $dpermintaan_barang;
			$this->unit = $unit;
		}
		
		public function command($command) {
			if ($command != "print_spb") {
				return parent::command($command);
			}
			$pack = null;
			if ($command == "print_spb") {
				$pack = new ResponsePackage();
				$content = $this->print_spb();
				$pack->setContent($content);
				$pack->setStatus(ResponsePackage::$STATUS_OK);
			}
			return $pack->getPackage();
		}
		
		public function print_spb() {
			$id = $_POST['id'];
			$header_data = $this->dbtable->select($id);
			$detail_data = $this->dbtable->get_result("
				SELECT *
				FROM " . $this->dpermintaan_barang . "
				WHERE prop NOT LIKE 'del' AND id_permintaan_barang = '" . $id . "'
			");
			$print_data = "<center><b>SURAT PERMINTAAN BARANG</b></center><br/>";
			$print_data .= "<table border='0'>
								<tr>
									<td>Tanggal</td>
									<td>:</td>
									<td>" . ArrayAdapter::format("date d M Y", $header_data->tanggal) . "</td>
								</tr>
								<tr>
									<td>Kepada</td>
									<td>:</td>
									<td>GUDANG UMUM</td>
								</tr>
								<tr>
									<td>Unit/Bag. yang memohon</td>
									<td>:</td>
									<td>" . ArrayAdapter::format("unslug", $this->unit) . "</td>
								</tr>
							</table>";
			$print_data .= "Mohon diberikan sejumlah barang sebagai berikut:<br/>";
			$print_data .= "<table border='1'>";
			$print_data .= "<tr>
								<th>No.</th>
								<th>Nama Barang</th>
								<th>Jumlah Permintaan</th>
								<th>Jumlah Diberikan</th>
								<th>Keterangan</th>
							</tr>";
			$no = 1;
			foreach ($detail_data as $d) {
				$dipenuhi = $d->jumlah_dipenuhi . " " . $d->satuan_dipenuhi;
				if ($d->dipenuhi == 0 && $d->satuan_dipenuhi == "-")
					$dipenuhi = "-";
				$print_data .= "<tr>
									<td>" . $no++ . "</td>
									<td>" . $d->nama_barang . "</td>
									<td>" . $d->jumlah_permintaan . " " . $d->satuan_permintaan . "</td>
									<td>" . $dipenuhi . "</td>
									<td>" . $d->keterangan . "</td>
								</tr>";
			}
			$print_data .= "</table><br/>";
			$print_data .= "<table border 1>
								<tr>
									<th>Pemohon</th>
									<th>Diketahui<br/>Kepala Seksi</th>
									<th>Diketahui<br/>Kepala Bagian</th>
									<th>Bagian<br/>Gudang Umum</th>
								</tr>
								<tr>
									<td>&Tab;&Tab;&Tab;&Tab;<br/><br/><br/><br/><br/></td>
									<td>&Tab;&Tab;&Tab;&Tab;</td>
									<td>&Tab;&Tab;&Tab;&Tab;</td>
									<td>&Tab;&Tab;&Tab;&Tab;</td>
								</tr>
							</table>";
			return $print_data;
		}
		
		public function save() {
			$header_data = $this->postToArray();
			$id['id'] = $_POST['id'];
			if ($id['id'] == 0 || $id['id'] == "") {
				//do insert here:
				$result = $this->dbtable->insert($header_data);
				$id['id'] = $this->dbtable->get_inserted_id();
				$success['type'] = "insert";
				if (isset($_POST['detail'])) {
					//do insert detail here:
					$detail_dbtable = new DBTable($this->dbtable->get_db(), $this->dpermintaan_barang);
					$detail = $_POST['detail'];
					foreach($detail as $d) {
						$detail_data = array();
						$detail_data['id_permintaan_barang'] = $id['id'];
						$detail_data['nama_barang'] = $d['nama_barang'];
						$detail_data['jumlah_permintaan'] = $d['jumlah_permintaan'];
						$detail_data['satuan_permintaan'] = $d['satuan_permintaan'];
						$detail_data['jumlah_dipenuhi'] = $d['jumlah_dipenuhi'];
						$detail_data['satuan_dipenuhi'] = $d['satuan_dipenuhi'];
						$detail_data['keterangan'] = $d['keterangan'];
						$detail_data['autonomous'] = "[".$this->getAutonomous()."]";
				        $detail_data['duplicate'] = 0;
				        $detail_data['time_updated'] = date("Y-m-d H:i:s");
				        $detail_data['origin_updated'] = $this->getAutonomous();
				        $detail_data['origin'] = $this->getAutonomous();
						$detail_dbtable->insert($detail_data);
					}
				}
			} else {
				//do update here:
				$result = $this->dbtable->update($header_data, $id);
				$success['type'] = "update";
				if (isset($_POST['detail'])) {
					//do update detail here:
					$detail_dbtable = new DBTable($this->dbtable->get_db(), $this->dpermintaan_barang);
					$detail = $_POST['detail'];
					foreach($detail as $d) {
						$detail_data = array();
						$detail_data['id_permintaan_barang'] = $id['id'];
						$detail_data['nama_barang'] = $d['nama_barang'];
						$detail_data['jumlah_permintaan'] = $d['jumlah_permintaan'];
						$detail_data['satuan_permintaan'] = $d['satuan_permintaan'];
						$detail_data['jumlah_dipenuhi'] = $d['jumlah_dipenuhi'];
						$detail_data['satuan_dipenuhi'] = $d['satuan_dipenuhi'];
						$detail_data['keterangan'] = $d['keterangan'];
						if ($d['cmd'] == "insert") {
							$detail_data['autonomous'] = "[".$this->getAutonomous()."]";
					        $detail_data['duplicate'] = 0;
					        $detail_data['time_updated'] = date("Y-m-d H:i:s");
					        $detail_data['origin_updated'] = $this->getAutonomous();
					        $detail_data['origin'] = $this->getAutonomous();
							$detail_dbtable->insert($detail_data);
						} else if($d['cmd'] == "update") {
							$detail_data['autonomous'] = "[".$this->getAutonomous()."]";
					        $detail_data['duplicate'] = 0;
					        $detail_data['time_updated'] = date("Y-m-d H:i:s");
					        $detail_data['origin_updated'] = $this->getAutonomous();
							$detail_id['id'] = $d['id'];
							$detail_dbtable->update($detail_data, $detail_id);
						} else if ($d['cmd'] == "delete") {
							$detail_id['id'] = $d['id'];
							$detail_data = array();
							$detail_data['autonomous'] = "[".$this->getAutonomous()."]";
					        $detail_data['duplicate'] = 0;
					        $detail_data['time_updated'] = date("Y-m-d H:i:s");
					        $detail_data['origin_updated'] = $this->getAutonomous();
							$detail_data['prop'] = "del";
							$detail_dbtable->update($detail_data, $detail_id);
						}
					}
				}
			}
			$success['id'] = $id['id'];
			$success['success'] = 1;
			if ($result === false) $success['success'] = 0;
			return $success;
		}
		public function edit() {
			$id = $_POST['id'];
			$header_row = $this->dbtable->select($id);
			$data['header'] = $header_row;
			$detail_dbtable = new DBTable($this->dbtable->get_db(), $this->dpermintaan_barang);
			$detail_dbtable->addCustomKriteria("id_permintaan_barang", "='".$id."'");
			$detail_dbtable->setShowAll(true);
			$view=$detail_dbtable->getQueryView("", "0");
			$data['detail'] = $detail_dbtable->get_result($view['query']);
			return $data;
		}
		public function delete() {
			$id['id'] = $_POST['id'];
			if ($this->dbtable->isRealDelete()) {
				$result = $this->dbtable->delete(null,$id);
			} else {
				$data['autonomous'] = "[".$this->getAutonomous()."]";
		        $data['duplicate'] = 0;
		        $data['time_updated'] = date("Y-m-d H:i:s");
		        $data['origin_updated'] = $this->getAutonomous();
				$data['prop'] = "del";
				$result = $this->dbtable->update($data, $id);
			}
			$success['success'] = 1;
			$success['id'] = $_POST['id'];
			if ($result === 'false') $success['success'] = 0;
			return $success;
		}
	}
?>