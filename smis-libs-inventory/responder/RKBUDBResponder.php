<?php 
	require_once("smis-base/smis-include-duplicate.php");

	class RKBUDBResponder extends DuplicateResponder {
		private $drkbu_tbl;
		public function __construct($dbtable, $uitable, $adapter,$drkbu_tbl){
			parent::__construct($dbtable, $uitable, $adapter);
			$this->drkbu_tbl=$drkbu_tbl;
		}
		
		public function save() {
			$header_data = $this->postToArray();
			$id['id'] = $_POST['id'];
			if ($id['id'] == 0 || $id['id'] == "") {
				//do insert header here:
				$result = $this->dbtable->insert($header_data);
				$id['id'] = $this->dbtable->get_inserted_id();
				$success['type'] = "insert";
				if (isset($_POST['detail'])) {
					//do insert detail here:
					$detail_dbtable = new DBTable($this->dbtable->get_db(), $this->drkbu_tbl);
					$detail = $_POST['detail'];
					foreach($detail as $d) {
						$detail_data = array();
						$detail_data['id_rkbu'] = $id['id'];
						$detail_data['nama_barang'] = $d['nama_barang'];
						$detail_data['jenis_barang'] = $d['jenis_barang'];
						$detail_data['jumlah_diajukan'] = $d['jumlah_diajukan'];
						$detail_data['satuan_diajukan'] = $d['satuan_diajukan'];
						$detail_data['jumlah_disetujui'] = $d['jumlah_disetujui'];
						$detail_data['satuan_disetujui'] = $d['satuan_disetujui'];
						$detail_data['bulan'] = $d['bulan'];
						$detail_data['autonomous'] = "[".$this->getAutonomous()."]";
				        $detail_data['duplicate'] = 0;
				        $detail_data['time_updated'] = date("Y-m-d H:i:s");
				        $detail_data['origin_updated'] = $this->getAutonomous();
						$detail_dbtable->insert($detail_data);
					}
				}
			} else {
				//do update header here:
				$result = $this->dbtable->update($header_data, $id);
				$success['type'] = "update";
				if (isset($_POST['detail'])) {
					//do update detail here:
					$detail_dbtable = new DBTable($this->dbtable->get_db(), $this->drkbu_tbl);
					$detail = $_POST['detail'];
					foreach($detail as $d) {
						$detail_data = array();
						$detail_data['id_rkbu'] = $id['id'];
						$detail_data['nama_barang'] = $d['nama_barang'];
						$detail_data['jenis_barang'] = $d['jenis_barang'];
						$detail_data['jumlah_diajukan'] = $d['jumlah_diajukan'];
						$detail_data['satuan_diajukan'] = $d['satuan_diajukan'];
						$detail_data['jumlah_disetujui'] = $d['jumlah_disetujui'];
						$detail_data['satuan_disetujui'] = $d['satuan_disetujui'];
						$detail_data['bulan'] = $d['bulan'];
						if ($d['cmd'] == "insert") {
							$detail_data['autonomous'] = "[".$this->getAutonomous()."]";
					        $detail_data['duplicate'] = 0;
					        $detail_data['time_updated'] = date("Y-m-d H:i:s");
					        $detail_data['origin_updated'] = $this->getAutonomous();
					        $detail_data['origin'] = $this->getAutonomous();
							$detail_dbtable->insert($detail_data);
						} else if($d['cmd'] == "update") {
							$detail_data['autonomous'] = "[".$this->getAutonomous()."]";
					        $detail_data['duplicate'] = 0;
					        $detail_data['time_updated'] = date("Y-m-d H:i:s");
					        $detail_data['origin_updated'] = $this->getAutonomous();
							$detail_id['id'] = $d['id'];
							$detail_dbtable->update($detail_data, $detail_id);
						} else if ($d['cmd'] == "delete") {
							$detail_id['id'] = $d['id'];
							$detail_data = array();
							$detail_data['autonomous'] = "[".$this->getAutonomous()."]";
					        $detail_data['duplicate'] = 0;
					        $detail_data['time_updated'] = date("Y-m-d H:i:s");
					        $detail_data['origin_updated'] = $this->getAutonomous();
							$detail_data['prop'] = "del";
							$detail_dbtable->update($detail_data, $detail_id);
						}
					}
				}
			}
			$success['id'] = $id['id'];
			$success['success'] = 1;
			if ($result === false) $success['success'] = 0;
			return $success;
		}
		public function edit() {
			$id = $_POST['id'];
			$header_row = $this->dbtable->select($id);
			$data['header'] = $header_row;
			$detail_dbtable = new DBTable($this->dbtable->get_db(), $this->drkbu_tbl);
			$detail_dbtable->addCustomKriteria(" id_rkbu ", " = '".$id."' ");
			$detail_dbtable->setShowAll(true);
			$view=$detail_dbtable->getQueryView("", "0");
			
			$data['detail'] =  $detail_dbtable->get_result($view['query']);
			return $data;
		}
		public function delete() {
			$id['id'] = $_POST['id'];
			if ($this->dbtable->isRealDelete()) {
				$result = $this->dbtable->delete(null,$id);
			} else {
				$data['autonomous'] = "[".$this->getAutonomous()."]";
		        $data['duplicate'] = 0;
		        $data['time_updated'] = date("Y-m-d H:i:s");
		        $data['origin_updated'] = $this->getAutonomous();
		        $data['origin'] = $this->getAutonomous();
				$data['prop'] = "del";
				$result = $this->dbtable->update($data, $id);
			}
			$success['success'] = 1;
			$success['id'] = $_POST['id'];
			if ($result === 'false') $success['success'] = 0;
			return $success;
		}
	}
?>