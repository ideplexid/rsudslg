<?php 
	class BarangKSBDBResponder extends DBResponder {
		private $tbl_stok_barang;
		private $tbl_barang_masuk;
		
		public function __construct($dbtable, $uitable, $adapter, $tbl_stok_barang, $tbl_barang_masuk){
			parent::__construct($dbtable, $uitable, $adapter);
			$this->tbl_stok_barang=$tbl_stok_barang;
			$this->tbl_barang_masuk=$tbl_barang_masuk;
		}
		
		public function edit() {
			$id = $_POST['id'];
			$row = $this->dbtable->get_row("
				SELECT *
				FROM (
					SELECT DISTINCT id_barang AS 'id', kode_barang, nama_barang, nama_jenis_barang, satuan
					FROM " . $this->tbl_stok_barang . " a LEFT JOIN " . $this->tbl_barang_masuk . " b ON a.id_barang_masuk = b.id
					WHERE a.prop NOT LIKE 'del' AND b.prop NOT LIKE 'del' AND b.status = 'sudah' AND a.konversi = 1
					ORDER BY kode_barang, nama_jenis_barang, nama_barang ASC
				) v
				WHERE id = '" . $id . "'
				LIMIT 0, 1
			");
			return $row;
		}
	}
?>