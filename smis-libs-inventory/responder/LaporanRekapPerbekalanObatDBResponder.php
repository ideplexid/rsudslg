<?php 
	class LaporanRekapPerbekalanObatDBResponder extends DBResponder {
		private $unit;
		private $tbl_stok_obat;
		private $tbl_obat_masuk;
		
		public function __construct($dbtable, $uitable, $adapter, $tbl_obat_masuk, $tbl_stok_obat, $unit){
			parent::__construct($dbtable, $uitable, $adapter);
			$this->tbl_stok_obat = $tbl_stok_obat;
			$this->tbl_obat_masuk = $tbl_obat_masuk;
			$this->unit = $unit;
		}
		
		public function command($command) {
			if ($command != "print_laporan") {
				return parent::command($command);
			}
			$pack = null;
			if ($command == "print_laporan") {
				$pack = new ResponsePackage();
				$content = $this->print_laporan();
				$pack->setContent($content);
				$pack->setStatus(ResponsePackage::$STATUS_OK);
			}
			return $pack->getPackage();
		}
		
		public function print_laporan() {
			$print_data = "<center><b>LAPORAN REKAP PERBEKALAN OBAT (" . ArrayAdapter::format("unslug", $this->unit) . ")</b></center><br/>";
			$print_data .= "<table border='0'>
								<tr>
									<td>Tanggal</td>
									<td>:</td>
									<td>" . ArrayAdapter::format("date d M Y", date("Y-m-d")) . "</td>
								</tr>
								<tr>
									<td>Jam</td>
									<td>:</td>
									<td>" . date("H:i:s") . "</td>
								</tr>
							</table>";
			$data_perbekalan = $this->dbtable->get_result("
				SELECT *
				FROM (
					SELECT id_obat AS id, nama_obat, nama_jenis_obat, SUM(sisa) AS 'sisa', satuan, konversi, satuan_konversi, avg_hna AS 'hna_ma', SUM(sisa * avg_hna) AS 'total'
					FROM (
						SELECT " . $this->tbl_stok_obat . ".*, v_obat_hna.avg_hna
						FROM " . $this->tbl_stok_obat . " LEFT JOIN (
							SELECT id_obat, satuan, konversi, satuan_konversi, AVG(hna) AS 'avg_hna'
							FROM (
								SELECT DISTINCT " . $this->tbl_stok_obat . ".id_obat, " . $this->tbl_stok_obat . ".nama_obat, " . $this->tbl_stok_obat . ".satuan, " . $this->tbl_stok_obat . ".konversi, " . $this->tbl_stok_obat . ".satuan_konversi, (" . $this->tbl_stok_obat . ".hna * 100) / 110 AS 'hna'
								FROM " . $this->tbl_stok_obat . " LEFT JOIN " . $this->tbl_obat_masuk . " ON " . $this->tbl_stok_obat . ".id_obat_masuk = " . $this->tbl_obat_masuk . ".id
								WHERE " . $this->tbl_stok_obat . ".prop NOT LIKE 'del' AND " . $this->tbl_obat_masuk . ".prop NOT LIKE 'del' AND " . $this->tbl_obat_masuk . ".status = 'sudah'
							) v_obat_hna
							GROUP BY id_obat, satuan, konversi, satuan_konversi
						) v_obat_hna ON " . $this->tbl_stok_obat . ".id_obat = v_obat_hna.id_obat AND " . $this->tbl_stok_obat . ".satuan = v_obat_hna.satuan AND " . $this->tbl_stok_obat . ".konversi = v_obat_hna.konversi AND " . $this->tbl_stok_obat . ".satuan_konversi = v_obat_hna.satuan_konversi
					) v_obat_hna
					GROUP BY id_obat, satuan, konversi, satuan_konversi
				) v_stok
			");
			$print_data .= "<table border='1'>
								<tr>
									<th>No.</th>
									<th>Nama Obat</th>
									<th>Jenis Obat</th>
									<th>Jumlah</th>
									<th>Satuan</th>
									<th>Harga Satuan</th>
									<th>Total</th>
								</tr>";
			$total_hna = 0;
			$no = 1;
			if (count($data_perbekalan) > 0) {
				foreach($data_perbekalan as $dp) {
					$total_hna += ($dp->sisa * $dp->hna_ma);
					$print_data .= "<tr>
										<td>" . $no++ . "</td>
										<td>" . $dp->nama_obat . "</td>
										<td>" . $dp->nama_jenis_obat . "</td>
										<td>" . $dp->sisa . "</td>
										<td>" . $dp->satuan . "</td>
										<td>" . ArrayAdapter::format("money Rp.", $dp->hna_ma) . "</td>
										<td>" . ArrayAdapter::format("money Rp.", $dp->sisa * $dp->hna_ma) . "</td>
									</tr>";
				}
			} else {
				$print_data .= "<tr>
									<td colspan='7'>Tidak terdapat data perbekalan obat</td>
								</tr>";
			}
			$print_data .= "<tr>
								<td align='center' colspan='6'><strong>T O T A L</strong></td>
								<td><strong>" . ArrayAdapter::format("money Rp.", $total_hna) . "</strong></td>
							</tr>";
			$print_data .= "</table><br/>";
			global $user;
			$print_data .= "<table border='0' align='center'>
								<tr>
									<td colspan='3' align='center'>Mengetahui :</td>
								</tr>
								<tr>
									<td>&nbsp;</td>
								</tr>
								<tr>
									<td align='center'>Bangkalan, " . ArrayAdapter::format("date d M Y", date("Y-m-d")) . "</td>
									<td>&Tab;</td>
									<td>&nbsp;</td>
								</tr>
								<tr>
									<td align='center'>KEPALA RSUD SYARIFAH AMBAMI RATO EBU</td>
									<td>&Tab;</td>
									<td align='center'>KEPALA " . ArrayAdapter::format("unslug", $this->unit) . "</td>
								</tr>
								<tr>
									<td>&nbsp;</td>
									<td>&Tab;</td>
									<td>&nbsp;</td>
								</tr>
								<tr>
									<td>&nbsp;</td>
									<td>&Tab;</td>
									<td>&nbsp;</td>
								</tr>
								<tr>
									<td>&nbsp;</td>
									<td>&Tab;</td>
									<td>&nbsp;</td>
								</tr>
								<tr>
									<td>&nbsp;</td>
									<td>&Tab;</td>
									<td>&nbsp;</td>
								</tr>
								<tr>
									<td align='center'><b>drg. Yusro</b></td>
									<td>&Tab;</td>
									<td align='center'>(_____________________)</td>
								</tr>
							</table>";
			return $print_data;
		}
	}
?>