<?php
class ObatDBResponder extends DBResponder {
	private $tbl_stok_obat;
	private $tbl_obat_masuk;

	public function __construct($dbtable, $table, $adapter, $tbl_stok_obat, $tbl_obat_masuk) {
		parent::__construct($dbtable, $table, $adapter);
		$this->tbl_stok_obat = $tbl_stok_obat;
		$this->tbl_obat_masuk = $tbl_obat_masuk;
	}
	
	public function edit() {
		$id = $_POST['id'];
		$data['header'] = $this->dbtable->get_row("
				SELECT DISTINCT " . $this->tbl_stok_obat . ".id_obat, " . $this->tbl_stok_obat . ".kode_obat, " . $this->tbl_stok_obat . ".nama_obat, " . $this->tbl_stok_obat . ".nama_jenis_obat
				FROM " . $this->tbl_stok_obat . " LEFT JOIN " . $this->tbl_obat_masuk . " ON " . $this->tbl_stok_obat . ".id_obat_masuk = " . $this->tbl_obat_masuk . ".id
				WHERE " . $this->tbl_obat_masuk . ".status = 'sudah' AND " . $this->tbl_stok_obat . ".prop NOT LIKE 'del' AND " . $this->tbl_stok_obat . ".id_obat = '" . $id . "'
			");
		$satuan_rows = $this->dbtable->get_result("
			SELECT satuan, konversi, satuan_konversi 
			FROM (
				SELECT satuan, konversi, satuan_konversi, SUM(sisa) AS 't_jumlah'
				FROM " . $this->tbl_stok_obat . " LEFT JOIN " . $this->tbl_obat_masuk . " ON " . $this->tbl_stok_obat . ".id_obat_masuk = " . $this->tbl_obat_masuk . ".id
				WHERE " . $this->tbl_obat_masuk . ".status = 'sudah' AND " . $this->tbl_stok_obat . ".prop NOT LIKE 'del' AND " . $this->tbl_stok_obat . ".id_obat = '" . $id . "'
				GROUP BY id_obat, satuan, konversi, satuan_konversi
			) v
			ORDER BY t_jumlah DESC
		");
		$satuan_option = "";
		foreach($satuan_rows as $sr) {
			if ($sr->konversi == 1) {
				$satuan_option .= "
					<option value='" . $sr->konversi . "_" . $sr->satuan_konversi . "'>" . $sr->satuan . "</option>
				";
			}
		}
		$data['satuan_option'] = $satuan_option;
		return $data;
	}
}
?>