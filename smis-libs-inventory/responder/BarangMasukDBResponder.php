<?php 
	require_once("smis-base/smis-include-duplicate.php");
	
	class BarangMasukDBResponder extends DuplicateResponder {
		private $dbstok;
		private $dbriwayat_stok;
		
		public function __construct($dbtable, $uitable, $adapter, $dbstok, $dbriwayat_stok){
			parent::__construct($dbtable, $uitable, $adapter);
			$this->dbstok=$dbstok;
			$this->dbriwayat_stok=$dbriwayat_stok;
		}
		
		public function save() {
			$header_data = array();
			$header_data['status'] = $_POST['status'];
			$header_data['keterangan'] = $_POST['keterangan'];
			$header_data['autonomous'] = "[".$this->getAutonomous()."]";
	        $header_data['duplicate'] = 0;
	        $header_data['time_updated'] = date("Y-m-d H:i:s");
	        $header_data['origin'] = $this->getAutonomous();
			$id['id'] = $_POST['id'];
			$result = $this->dbtable->update($header_data, $id);
			//logging riwayat stok:
			$stok_rows = $this->dbtable->get_result("
				SELECT " . $this->dbstok . ".*, " . $this->dbtable->getName() . ".unit
				FROM " . $this->dbstok . " LEFT JOIN " . $this->dbtable->getName() . " ON " . $this->dbstok . ".id_barang_masuk = " . $this->dbtable->getName() . ".id
				WHERE " . $this->dbstok . ".id_barang_masuk = '" . $id['id'] . "'
			");
			$riwayat_dbtable = new DBTable($this->dbtable->get_db(), $this->dbriwayat_stok);
			foreach($stok_rows as $sr) {
				$data_riwayat = array();
				$data_riwayat['tanggal'] = date("Y-m-d");
				$data_riwayat['id_stok_barang'] = $sr->id;
				$data_riwayat['jumlah_masuk'] = $sr->jumlah;
				$data_riwayat['sisa'] = $sr->sisa;
				$data_riwayat['keterangan'] = "Stok Masuk dari " . ArrayAdapter::format("unslug", $sr->unit);
				global $user;
				$data_riwayat['nama_user'] = $user->getName();
				$data_riwayat['autonomous'] = "[".$this->getAutonomous()."]";
		        $data_riwayat['duplicate'] = 0;
		        $data_riwayat['time_updated'] = date("Y-m-d H:i:s");
		        $data_riwayat['origin_updated'] = $this->getAutonomous();
		        $data_riwayat['origin'] = $this->getAutonomous();
				$riwayat_dbtable->insert($data_riwayat);
			}
			$success['type'] = "update";
			$success['id'] = $id['id'];
			$success['success'] = 1;
			if ($result === false) $success['success'] = 0;
			return $success;
		}
		
		public function edit() {
			$id = $_POST['id'];
			$header_row = $this->dbtable->select($id);
			if ($header_row->tanggal == "0000-00-00" || $header_row->tanggal == null)
				$header_row->tanggal = "-";
			$data['header'] = $header_row;
			$qv="
				SELECT id_barang, nama_barang, nama_jenis_barang,
				SUM(jumlah) AS 'jumlah', satuan, konversi, satuan_konversi 
				FROM ". $this->dbstok;
			$detail_dbtable = new DBTable($this->dbtable->get_db(), $this->dbstok);
			$detail_dbtable->setPreferredQuery(true, $qv, "");
			$detail_dbtable->setUseWhereForView(true);
			$detail_dbtable->setShowAll(true);
			$detail_dbtable->addCustomKriteria("id_barang_masuk", "= '" . $id . "'");
			$detail_dbtable->setGroupBy(true, "id_barang, satuan, konversi, satuan_konversi");
			$view=$detail_dbtable->getQueryView("", "0");
			$data['detail'] = $detail_dbtable->get_result($view['query']);
			return $data;
		}
	}
?>