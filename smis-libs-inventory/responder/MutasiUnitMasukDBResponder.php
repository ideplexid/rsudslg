	<?php
	class MutasiUnitMasukDBResponder extends DBResponder {
		private $tbl_obat_masuk;
		private $tbl_stok_obat;
		private $tbl_riwayat_stok_obat;

		public function __construct($dbtable, $table, $adapter, $tbl_obat_masuk, $tbl_stok_obat, $tbl_riwayat_stok_obat) {
			parent::__construct($dbtable, $table, $adapter);
			$this->tbl_obat_masuk = $tbl_obat_masuk;
			$this->tbl_stok_obat = $tbl_stok_obat;
			$this->tbl_riwayat_stok_obat = $tbl_riwayat_stok_obat;
		}

		public function save() {
			$header_data = array();
			$header_data['status'] = $_POST['status'];
			$id['id'] = $_POST['id'];
			$result = $this->dbtable->update($header_data, $id);
			//logging riwayat stok:
			$stok_rows = $this->dbtable->get_result("
				SELECT a.*, b.unit
				FROM " . $this->tbl_stok_obat . " a LEFT JOIN " . $this->tbl_obat_masuk . " b ON a.id_obat_masuk = b.id
				WHERE a.id_obat_masuk = '" . $id['id'] . "'
			");
			$riwayat_dbtable = new DBTable($this->dbtable->get_db(), $this->tbl_riwayat_stok_obat);
			foreach($stok_rows as $sr) {
				$data_riwayat = array();
				$data_riwayat['tanggal'] = date("Y-m-d");
				$data_riwayat['id_stok_obat'] = $sr->id;
				$data_riwayat['jumlah_masuk'] = $sr->jumlah;
				$data_riwayat['sisa'] = $sr->sisa;
				$data_riwayat['keterangan'] = "Mutasi Antar Unit Masuk dari " . ArrayAdapter::format("unslug", $sr->unit);
				global $user;
				$data_riwayat['nama_user'] = $user->getName();
				$riwayat_dbtable->insert($data_riwayat);
			}
			$success['type'] = "update";
			$success['id'] = $id['id'];
			$success['success'] = 1;
			if ($result === false) $success['success'] = 0;
			return $success;
		}
	}
?>