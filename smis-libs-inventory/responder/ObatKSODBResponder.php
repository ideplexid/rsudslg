<?php 
	class ObatKSODBResponder extends DBResponder {
		private $tbl_stok_obat;
		private $tbl_obat_masuk;
		
		public function __construct($dbtable, $uitable, $adapter, $tbl_stok_obat, $tbl_obat_masuk){
			parent::__construct($dbtable, $uitable, $adapter);
			$this->tbl_stok_obat=$tbl_stok_obat;
			$this->tbl_obat_masuk=$tbl_obat_masuk;
		}
		
		public function edit() {
			$id = $_POST['id'];
			$row = $this->dbtable->get_row("
				SELECT *
				FROM (
					SELECT DISTINCT id_obat AS 'id', kode_obat, nama_obat, nama_jenis_obat, satuan
					FROM " . $this->tbl_stok_obat . " a LEFT JOIN " . $this->tbl_obat_masuk . " b ON a.id_obat_masuk = b.id
					WHERE a.prop NOT LIKE 'del' AND b.prop NOT LIKE 'del' AND b.status = 'sudah' AND a.konversi = 1
					ORDER BY kode_obat, nama_jenis_obat, nama_obat ASC
				) v
				WHERE id = '" . $id . "'
				LIMIT 0, 1
			");
			return $row;
		}
	}
?>