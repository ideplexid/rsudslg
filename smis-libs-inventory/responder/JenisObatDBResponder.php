<?php 
	class JenisObatDBResponder extends DBResponder {
		private $tbl_stok_obat;
		private $tbl_obat_masuk;
		
		public function __construct($dbtable, $uitable, $adapter, $tbl_stok_obat, $tbl_obat_masuk){
			parent::__construct($dbtable, $uitable, $adapter);
			$this->tbl_stok_obat=$tbl_stok_obat;
			$this->tbl_obat_masuk=$tbl_obat_masuk;
		}
		
		public function edit() {
			$id = $_POST['id'];
			$data['id'] = $id;
			$data['nama_jenis_obat'] = $id;
			return $data;
		}
	}
?>