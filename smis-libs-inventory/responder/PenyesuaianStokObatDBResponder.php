<?php 
	require_once("smis-base/smis-include-duplicate.php");

	class PenyesuaianStokObatDBResponder extends DuplicateResponder {
		private $tbl_stok;
		private $tbl_penyesuaian;
		private $tbl_riwayat_stok;
		
		public function __construct($dbtable, $uitable, $adapter,$tbl_stok,$tbl_riwayat_stok){
			parent::__construct($dbtable, $uitable, $adapter);
			$this->tbl_stok=$tbl_stok;
			$this->tbl_penyesuaian=$this->dbtable->getName();
			$this->tbl_riwayat_stok=$tbl_riwayat_stok;
		}
		
		public function save() {
			$data = $this->postToArray();
			$result = $this->dbtable->insert($data);
			$stok_obat_dbtable = new DBTable($this->dbtable->get_db(), $this->tbl_stok);
			$stok_data = array();
			$stok_data['sisa'] = $_POST['jumlah_baru'];
			$stok_data['autonomous'] = "[".$this->getAutonomous()."]";
	        $stok_data['duplicate'] = 0;
	        $stok_data['time_updated'] = date("Y-m-d H:i:s");
	        $stok_data['origin_updated'] = $this->getAutonomous();
			$stok_id['id'] = $_POST['id_stok_obat'];
			$stok_obat_dbtable->update($stok_data, $stok_id);
			//logging riwayat stok:
			$riwayat_dbtable = new DBTable($this->dbtable->get_db(), $this->tbl_riwayat_stok);
			$data_riwayat = array();
			$data_riwayat['tanggal'] = date("Y-m-d");
			$data_riwayat['id_stok_obat'] = $_POST['id_stok_obat'];
			if ($_POST['jumlah_baru'] < $_POST['jumlah_lama']) {
				$data_riwayat['jumlah_keluar'] = $_POST['jumlah_lama'] - $_POST['jumlah_baru'];
			} else {
				$data_riwayat['jumlah_masuk'] = $_POST['jumlah_baru'] - $_POST['jumlah_lama'];
			}
			$data_riwayat['sisa'] = $_POST['jumlah_baru'];
			$data_riwayat['keterangan'] = "Penyesuaian Stok: " . $_POST['keterangan'];
			global $user;
			$data_riwayat['nama_user'] = $user->getName();
			$data_riwayat['autonomous'] = "[".$this->getAutonomous()."]";
	        $data_riwayat['duplicate'] = 0;
	        $data_riwayat['time_updated'] = date("Y-m-d H:i:s");
	        $data_riwayat['origin_updated'] = $this->getAutonomous();
	        $data_riwayat['origin'] = $this->getAutonomous();
			$riwayat_dbtable->insert($data_riwayat);
			$success['type'] = "insert";
			$success['id'] = $this->dbtable->get_inserted_id();
			$success['success'] = 1;
			if ($result === false) $success['success'] = 0;
			return $success;
		}
		
		public function edit() {
			$id = $_POST['id'];
			$this->dbtable->setName($this->tbl_stok);
			$data = $this->dbtable->select($id);
			$this->dbtable->setName($this->tbl_penyesuaian);
			return $data;
		}
	}
?>