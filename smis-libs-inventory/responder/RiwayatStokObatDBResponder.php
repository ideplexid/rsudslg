<?php 
	class RiwayatStokObatDBResponder extends DBResponder {
		private $unit;
		public function __construct($dbtable, $uitable, $adapter,$unit){
			parent::__construct($dbtable, $uitable, $adapter);
			$this->unit = $unit;
		}
		public function command($command) {
			if ($command != "print_stock_card") {
				return parent::command($command);
			}
			$pack = null;
			if ($command == "print_stock_card") {
				$pack = new ResponsePackage();
				$content = $this->print_stock_card();
				$pack->setContent($content);
				$pack->setStatus(ResponsePackage::$STATUS_OK);
			}
			return $pack->getPackage();
		}
		public function print_stock_card() {
			$no_stok = $_POST['no_stok'];			
			$nama_obat = $_POST['nama_obat'];
			$jenis_obat = $_POST['jenis_obat'];
			$jenis_stok = $_POST['jenis_stok'];
			$produsen = $_POST['produsen'];
			$vendor = $_POST['vendor'];
			$satuan = $_POST['satuan'];
			$tanggal_exp = $_POST['tanggal_exp'];
			$print_data = "<center><b>RIWAYAT STOK OBAT (" . ArrayAdapter::format("unslug", $this->unit) . ")</b></center><br/>";
			$print_data .= "<table border='0'>
								<tr>
									<td>No. Stok</td>
									<td>:</td>
									<td>" . $no_stok . "</td>
								</tr>
								<tr>
									<td>Nama Obat</td>
									<td>:</td>
									<td>" . $nama_obat . "</td>
								</tr>
								<tr>
									<td>Jenis Obat</td>
									<td>:</td>
									<td>" . $jenis_obat . "</td>
								</tr>
								<tr>
									<td>Jenis Stok</td>
									<td>:</td>
									<td>" . $jenis_stok . "</td>
								</tr>
								<tr>
									<td>Produsen</td>
									<td>:</td>
									<td>" . $produsen . "</td>
								</tr>
								<tr>
									<td>Vendor</td>
									<td>:</td>
									<td>" . $vendor . "</td>
								</tr>
								<tr>
									<td>Satuan</td>
									<td>:</td>
									<td>" . $satuan . "</td>
								</tr>
								<tr>
									<td>Tgl. Exp.</td>
									<td>:</td>
									<td>" . ArrayAdapter::format("date d M Y", $tanggal_exp) . "</td>
								</tr>
							</table>";
				$data_riwayat = $this->dbtable->get_result("
					SELECT *
					FROM " . $this->dbtable->getName() . "
					WHERE id_stok_obat = '" . $no_stok . "'
				");
				$print_data .= "<table border='1'>
									<tr>
										<th>Tanggal</th>
										<th>Masuk</th>
										<th>Keluar</th>
										<th>Sisa</th>
										<th>Keterangan</th>
									</tr>";
				if (count($data_riwayat) > 0) {
					foreach($data_riwayat as $dr) {
						$print_data .= "<tr>
											<td>" . ArrayAdapter::format("date d M Y", $dr->tanggal) . "</td>
											<td>" . $dr->jumlah_masuk . "</td>
											<td>" . $dr->jumlah_keluar . "</td>
											<td>" . $dr->sisa . "</td>
											<td>" . $dr->keterangan . "</td>
										</tr>";
					}
				} else {
					$print_data .= "<tr>
										<td colspan='5'>Tidak terdapat data riwayat stok</td>
									</tr>";
				}
				$print_data .= "</table><br/>";
				$print_data .= "<table border='0'>
									<tr>
										<td align='center'>Tuban, " . date("d-m-Y") . "</td>
									</tr>
									<tr><td>&nbsp;</td></tr>
									<tr><td>&nbsp;</td></tr>
									<tr><td>&nbsp;</td></tr>
									<tr><td>&nbsp;</td></tr>
									<tr>
										<td align='center'>(_____________________)</td>
									</tr>
								</table>";
			return $print_data;
		}
	}
?>