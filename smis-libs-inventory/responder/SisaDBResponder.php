<?php
class SisaDBResponder extends DBResponder {
	private $tbl_stok_obat;
	private $tbl_obat_masuk;

	public function __construct($dbtable, $table, $adapter, $tbl_stok_obat, $tbl_obat_masuk) {
		parent::__construct($dbtable, $table, $adapter);
		$this->tbl_stok_obat = $tbl_stok_obat;
		$this->tbl_obat_masuk = $tbl_obat_masuk;
	}
	
	public function edit() {
		$id_obat = $_POST['id_obat'];
		$satuan = $_POST['satuan'];
		$konversi = $_POST['konversi'];
		$satuan_konversi = $_POST['satuan_konversi'];
		$data = $this->dbtable->get_row("
				SELECT " . $this->tbl_stok_obat . ".id_obat, " . $this->tbl_stok_obat . ".nama_obat, " . $this->tbl_stok_obat . ".nama_jenis_obat, SUM(" . $this->tbl_stok_obat . ".sisa) AS 'sisa', " . $this->tbl_stok_obat . ".satuan, " . $this->tbl_stok_obat . ".konversi, " . $this->tbl_stok_obat . ".satuan_konversi, " . $this->tbl_stok_obat . ".label
				FROM " . $this->tbl_stok_obat . " LEFT JOIN " . $this->tbl_obat_masuk . " ON " . $this->tbl_stok_obat . ".id_obat_masuk = " . $this->tbl_obat_masuk . ".id
				WHERE " . $this->tbl_stok_obat . ".prop NOT LIKE 'del' AND " . $this->tbl_obat_masuk . ".status = 'sudah' AND " . $this->tbl_stok_obat . ".id_obat = '" . $id_obat . "'
				GROUP BY " . $this->tbl_stok_obat . ".id_obat, " . $this->tbl_stok_obat . ".satuan, " . $this->tbl_stok_obat . ".konversi, " . $this->tbl_stok_obat . ".satuan_konversi
			");
		return $data;
	}
}
?>