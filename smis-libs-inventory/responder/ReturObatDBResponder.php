<?php 
	require_once("smis-base/smis-include-duplicate.php");

	class ReturObatDBResponder extends DuplicateResponder {
		private $tbl_stok;
		private $tbl_retur;
		private $tbl_riwayat_stok;
		
		public function __construct($dbtable, $uitable, $adapter,$tbl_retur, $tbl_stok, $tbl_riwayat_stok){
			parent::__construct($dbtable, $uitable, $adapter);
			$this->tbl_stok=$tbl_stok;
			$this->tbl_retur=$tbl_retur;
			$this->tbl_riwayat_stok=$tbl_riwayat_stok;
		}
		
		public function save() {
			$data = $this->postToArray();
			$id['id'] = $_POST['id'];
			if ($id['id'] == 0 || $id['id'] == "") {
				//do insert here:
				$result = $this->dbtable->insert($data);
				$id['id'] = $this->dbtable->get_inserted_id();
				$success['type'] = "insert";
				$stok_obat_dbtable = new DBTable($this->dbtable->get_db(), "".$this->tbl_stok."");
				$stok_obat_row = $stok_obat_dbtable->select($data['id_stok_obat']);
				$stok_data = array();
				$stok_data['sisa'] = $stok_obat_row->sisa - $data['jumlah'];
				$stok_data['autonomous'] = "[".$this->getAutonomous()."]";
		        $stok_data['duplicate'] = 0;
		        $stok_data['time_updated'] = date("Y-m-d H:i:s");
		        $stok_data['origin_updated'] = $this->getAutonomous();
				$stok_id['id'] = $data['id_stok_obat'];
				$stok_obat_dbtable->update($stok_data, $stok_id);
				//logging riwayat stok:
				$riwayat_dbtable = new DBTable($this->dbtable->get_db(), $this->tbl_riwayat_stok);
				$data_riwayat = array();
				$data_riwayat['tanggal'] = date("Y-m-d");
				$data_riwayat['id_stok_obat'] = $data['id_stok_obat'];
				$data_riwayat['jumlah_keluar'] = $data['jumlah'];
				$data_riwayat['sisa'] = $stok_obat_row->sisa - $data['jumlah'];
				$data_riwayat['keterangan'] = "Retur Stok ke " . ArrayAdapter::format("unslug", "gudang_farmasi");
				global $user;
				$data_riwayat['nama_user'] = $user->getName();
				$data_riwayat['autonomous'] = "[".$this->getAutonomous()."]";
		        $data_riwayat['duplicate'] = 0;
		        $data_riwayat['time_updated'] = date("Y-m-d H:i:s");
		        $data_riwayat['origin_updated'] = $this->getAutonomous();
		        $data_riwayat['origin'] = $this->getAutonomous();
				$riwayat_dbtable->insert($data_riwayat);
			} else {
				//do update here:
				$result = $this->dbtable->update($data, $id);
				$success['type'] = "update";
				$selisih = $_POST['jumlah'] - $_POST['jumlah_lama'];
				$stok_obat_dbtable = new DBTable($this->dbtable->get_db(), $this->tbl_stok);
				$stok_obat_row = $stok_obat_dbtable->select($data['id_stok_obat']);
				$stok_data = array();
				$stok_data['sisa'] = $stok_obat_row->sisa - $selisih;
				$stok_data['autonomous'] = "[".$this->getAutonomous()."]";
		        $stok_data['duplicate'] = 0;
		        $stok_data['time_updated'] = date("Y-m-d H:i:s");
				$stok_data['origin_updated'] = $this->getAutonomous();
				$stok_id['id'] = $data['id_stok_obat'];
				$stok_obat_dbtable->update($stok_data, $stok_id);
				//logging riwayat stok:
				$riwayat_dbtable = new DBTable($this->dbtable->get_db(), $this->tbl_riwayat_stok);
				$data_riwayat = array();
				$data_riwayat['tanggal'] = date("Y-m-d");
				$data_riwayat['id_stok_obat'] = $data['id_stok_obat'];
				if ($selisih > 0)
					$data_riwayat['jumlah_keluar'] = $selisih;
				else
					$data_riwayat['jumlah_masuk'] = $selisih;
				$data_riwayat['sisa'] = $stok_obat_row->sisa - $selisih;
				$data_riwayat['keterangan'] = "Perubahan Retur Stok ke " . ArrayAdapter::format("unslug", "gudang_umum");
				global $user;
				$data_riwayat['nama_user'] = $user->getName();
				$data_riwayat['autonomous'] = "[".$this->getAutonomous()."]";
		        $data_riwayat['duplicate'] = 0;
		        $data_riwayat['time_updated'] = date("Y-m-d H:i:s");
		        $data_riwayat['origin_updated'] = $this->getAutonomous();
		        $data_riwayat['origin'] = $this->getAutonomous();
				$riwayat_dbtable->insert($data_riwayat);
			}
			$success['id'] = $id['id'];
			$success['success'] = 1;
			if ($result === false) $success['success'] = 0;
			return $success;
		}
		public function edit() {
			$id = $_POST['id'];
			$data = $this->dbtable->get_row("
							SELECT ".$this->tbl_retur.".*, ".$this->tbl_stok.".nama_obat, ".$this->tbl_stok.".nama_jenis_obat, ".$this->tbl_stok.".satuan
							FROM ".$this->tbl_retur." LEFT JOIN ".$this->tbl_stok." ON ".$this->tbl_retur.".id_stok_obat = ".$this->tbl_stok.".id
							WHERE ".$this->tbl_retur.".id = '" . $id . "'
						");
			return $data;
		}
		public function delete() {
			$id['id'] = $_POST['id'];
			if ($this->dbtable->isRealDelete()) {
				$result = $this->dbtable->delete(null,$id);
			} else {
				$data['prop'] = "del";
				$data['autonomous'] = "[".$this->getAutonomous()."]";
		        $data['duplicate'] = 0;
		        $data['time_updated'] = date("Y-m-d H:i:s");
		        $data['origin_updated'] = $this->getAutonomous();
				$result = $this->dbtable->update($data, $id);
			}
			$retur_obat_row = $this->dbtable->select($id['id']);
			$stok_obat_dbtable = new DBTable($this->dbtable->get_db(), $this->tbl_stok);
			$stok_obat_row = $stok_obat_dbtable->select($retur_obat_row->id_stok_obat );
			$stok_data = array();
			$stok_data['sisa'] = $stok_obat_row->sisa + $retur_obat_row->jumlah;
			$stok_data['autonomous'] = "[".$this->getAutonomous()."]";
	        $stok_data['duplicate'] = 0;
	        $stok_data['time_updated'] = date("Y-m-d H:i:s");
	        $stok_data['origin_updated'] = $this->getAutonomous();
			$stok_id['id'] = $retur_obat_row->id_stok_obat;
			$stok_obat_dbtable->update($stok_data, $stok_id);
			//logging riwayat stok:
			$riwayat_dbtable = new DBTable($this->dbtable->get_db(), $this->tbl_riwayat_stok);
			$data_riwayat = array();
			$data_riwayat['tanggal'] = date("Y-m-d");
			$data_riwayat['id_stok_obat'] = $retur_obat_row->id_stok_obat;
			$data_riwayat['jumlah_masuk'] = $retur_obat_row->jumlah;
			$data_riwayat['sisa'] = $stok_obat_row->sisa - $retur_obat_row->jumlah;
			$data_riwayat['keterangan'] = "Pembatalan Retur Stok ke " . ArrayAdapter::format("unslug", "gudang_umum");
			global $user;
			$data_riwayat['nama_user'] = $user->getName();
			$data_riwayat['autonomous'] = "[".$this->getAutonomous()."]";
	        $data_riwayat['duplicate'] = 0;
	        $data_riwayat['time_updated'] = date("Y-m-d H:i:s");
	        $data_riwayat['origin_updated'] = $this->getAutonomous();
	        $data_riwayat['origin'] = $this->getAutonomous();
			$riwayat_dbtable->insert($data_riwayat);
			$success['success'] = 1;
			$success['id'] = $_POST['id'];
			if ($result === 'false') $success['success'] = 0;
			return $success;
		}
	}
?>