<?php
class PermintaanObatDBResponder extends DBResponder {
	
	private $dpermintaan_obat;
	private $unit;
	
	public function __construct($dbtable, $uitable, $adapter, $dpermintaan_obat, $unit){
		parent::__construct($dbtable, $uitable, $adapter);
		$this->dpermintaan_obat = $dpermintaan_obat;
		$this->unit = $unit;
	}
	
	public function command($command) {
		if ($command != "print_spo") {
			return parent::command($command);
		}
		$pack = null;
		if ($command == "print_spo") {
			$pack = new ResponsePackage();
			$content = $this->print_spo();
			$pack->setContent($content);
			$pack->setStatus(ResponsePackage::$STATUS_OK);
		}
		return $pack->getPackage();
	}
	
	public function print_spo() {
		$id = $_POST['id'];
		$header_data = $this->dbtable->select($id);
		$detail_data = $this->dbtable->get_result("
			SELECT *
			FROM " . $this->dpermintaan_obat . "
			WHERE prop NOT LIKE 'del' AND id_permintaan_obat = '" . $id . "'
		");
		$print_data = "<b>RSUD SYARIFAH AMBAMI RATO EBU</b><br/>
					   Jl. Pemuda Kaffa No. 9<br/>
					   BANGKALAN<br/><br/>
					   <center><b>SURAT PERMINTAAN OBAT</b></center><br/>";
		$print_data .= "<table border='0'>
							<tr>
								<td>Tanggal</td>
								<td>:</td>
								<td>" . ArrayAdapter::format("date d M Y", $header_data->tanggal) . "</td>
							</tr>
							<tr>
								<td>Kepada</td>
								<td>:</td>
								<td>GUDANG FARMASI</td>
							</tr>
							<tr>
								<td>Unit/Bag. yang memohon</td>
								<td>:</td>
								<td>" . ArrayAdapter::format("unslug", $this->unit) . "</td>
							</tr>
						</table>";
		$print_data .= "Mohon diberikan sejumlah obat sebagai berikut:<br/>";
		$print_data .= "<table border='1'>";
		$print_data .= "<tr>
							<th>No.</th>
							<th>Nama Obat</th>
							<th>Jumlah Permintaan</th>
							<th>Jumlah Diberikan</th>
							<th>Keterangan</th>
						</tr>";
		$no = 1;
		foreach ($detail_data as $d) {
			$dipenuhi = $d->jumlah_dipenuhi . " " . $d->satuan_dipenuhi;
			if ($d->dipenuhi == 0 && $d->satuan_dipenuhi == "-")
				$dipenuhi = "-";
			$print_data .= "<tr>
								<td>" . $no++ . "</td>
								<td>" . $d->nama_obat . "</td>
								<td>" . $d->jumlah_permintaan . " " . $d->satuan_permintaan . "</td>
								<td>" . $dipenuhi . "</td>
								<td>" . $d->keterangan . "</td>
							</tr>";
		}
		$print_data .= "</table><br/>";
		$print_data .= "<table border 1>
							<tr>
								<th>Pemohon</th>
								<th>Diketahui<br/>Kepala Seksi</th>
								<th>Diketahui<br/>Kepala Bagian</th>
								<th>Bagian<br/>Gudang Farmasi</th>
							</tr>
							<tr>
								<td>&Tab;&Tab;&Tab;&Tab;<br/><br/><br/><br/><br/></td>
								<td>&Tab;&Tab;&Tab;&Tab;</td>
								<td>&Tab;&Tab;&Tab;&Tab;</td>
								<td>&Tab;&Tab;&Tab;&Tab;</td>
							</tr>
						</table>";
		return $print_data;
	}
	
	public function save() {
		$header_data = $this->postToArray();
		$id['id'] = $_POST['id'];
		if ($id['id'] == 0 || $id['id'] == "") {
			//do insert here:
			$header_data['tanggal'] = date("Y-m-d H:i:s");
			$result = $this->dbtable->insert($header_data);
			$id['id'] = $this->dbtable->get_inserted_id();
			$success['type'] = "insert";
			if (isset($_POST['detail'])) {
				//do insert detail here:
				$detail_dbtable = new DBTable($this->dbtable->get_db(), $this->dpermintaan_obat);
				$detail = $_POST['detail'];
				foreach($detail as $d) {
					$detail_data = array();
					$detail_data['id_permintaan_obat'] = $id['id'];
					$detail_data['id_obat'] = $d['id_obat'];
					$detail_data['kode_obat'] = $d['kode_obat'];
					$detail_data['nama_obat'] = $d['nama_obat'];
					$detail_data['nama_jenis_obat'] = $d['nama_jenis_obat'];
					$detail_data['jumlah_permintaan'] = $d['jumlah_permintaan'];
					$detail_data['satuan_permintaan'] = $d['satuan_permintaan'];
					$detail_data['jumlah_dipenuhi'] = $d['jumlah_dipenuhi'];
					$detail_data['satuan_dipenuhi'] = $d['satuan_dipenuhi'];
					$detail_data['keterangan'] = $d['keterangan'];
					$detail_dbtable->insert($detail_data);
				}
			}
		} else {
			//do update here:
			$result = $this->dbtable->update($header_data, $id);
			$success['type'] = "update";
			if (isset($_POST['detail'])) {
				//do update detail here:
				$detail_dbtable = new DBTable($this->dbtable->get_db(),$this->dpermintaan_obat);
				$detail = $_POST['detail'];
				foreach($detail as $d) {
					$detail_data = array();
					$detail_data['id_permintaan_obat'] = $id['id'];
					$detail_data['id_obat'] = $d['id_obat'];
					$detail_data['kode_obat'] = $d['kode_obat'];
					$detail_data['nama_obat'] = $d['nama_obat'];
					$detail_data['nama_jenis_obat'] = $d['nama_jenis_obat'];
					$detail_data['jumlah_permintaan'] = $d['jumlah_permintaan'];
					$detail_data['satuan_permintaan'] = $d['satuan_permintaan'];
					$detail_data['jumlah_dipenuhi'] = $d['jumlah_dipenuhi'];
					$detail_data['satuan_dipenuhi'] = $d['satuan_dipenuhi'];
					$detail_data['keterangan'] = $d['keterangan'];
					if ($d['cmd'] == "insert") {
						$detail_dbtable->insert($detail_data);
					} else if($d['cmd'] == "update") {
						$detail_id['id'] = $d['id'];
						$detail_dbtable->update($detail_data, $detail_id);
					} else if ($d['cmd'] == "delete") {
						$detail_id['id'] = $d['id'];
						$detail_data = array();
						$detail_data['prop'] = "del";
						$detail_dbtable->update($detail_data, $detail_id);
					}
				}
			}
		}
		$success['id'] = $id['id'];
		$success['success'] = 1;
		if ($result === false) $success['success'] = 0;
		return $success;
	}
	public function edit() {
		$id = $_POST['id'];
		$header_row = $this->dbtable->select($id);
		$data['header'] = $header_row;
		$detail_dbtable = new DBTable($this->dbtable->get_db(), $this->dpermintaan_obat);
		$detail_dbtable->setShowAll(true);
		$detail_dbtable->addCustomKriteria("id_permintaan_obat", "='".$id."'");
		$view=$detail_dbtable->getQueryView("" ,"");
		$data['detail'] = $detail_dbtable->get_result($view['query']);
		return $data;
	}
	public function delete() {
		$id['id'] = $_POST['id'];
		if ($this->dbtable->isRealDelete()) {
			$result = $this->dbtable->delete(null,$id);
		} else {
			$data['prop'] = "del";
			$result = $this->dbtable->update($data, $id);
		}
		$success['success'] = 1;
		$success['id'] = $_POST['id'];
		if ($result === 'false') $success['success'] = 0;
		return $success;
	}
}
?>