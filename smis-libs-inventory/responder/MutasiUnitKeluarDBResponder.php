<?php
	class MutasiUnitKeluarDBResponder extends DBResponder {
		private $tbl_obat_masuk;
		private $tbl_stok_obat;
		private $tbl_riwayat_stok_obat;
		private $tbl_stok_mutasi_depo_keluar;

		public function __construct($dbtable, $table, $adapter, $tbl_obat_masuk, $tbl_stok_obat, $tbl_riwayat_stok_obat, $tbl_stok_mutasi_depo_keluar) {
			parent::__construct($dbtable, $table, $adapter);
			$this->tbl_obat_masuk = $tbl_obat_masuk;
			$this->tbl_stok_obat = $tbl_stok_obat;
			$this->tbl_riwayat_stok_obat = $tbl_riwayat_stok_obat;
			$this->tbl_stok_mutasi_depo_keluar = $tbl_stok_mutasi_depo_keluar;
		}

		public function save() {
			$header_data = $this->postToArray();
			$id['id'] = $_POST['id'];
			if ($id['id'] == 0 || $id['id'] == "") {
				//do insert header here:
				$result = $this->dbtable->insert($header_data);
				$id['id'] = $this->dbtable->get_inserted_id();
				$success['type'] = "insert";
				
				$stok_dbtable = new DBTable($this->dbtable->get_db(), $this->tbl_stok_obat);
				$stok_query = "
					SELECT " . $this->tbl_stok_obat . ".id, " . $this->tbl_stok_obat . ".sisa
					FROM " . $this->tbl_stok_obat . " LEFT JOIN " . $this->tbl_obat_masuk . " ON " . $this->tbl_stok_obat . ".id_obat_masuk = " . $this->tbl_obat_masuk . ".id
					WHERE " . $this->tbl_stok_obat . ".id_obat = '" . $header_data['id_obat'] . "' 
					  AND " . $this->tbl_stok_obat . ".satuan = '" . $header_data['satuan'] . "' 
					  AND " . $this->tbl_stok_obat . ".konversi = '" . $header_data['konversi'] . "' 
					  AND " . $this->tbl_stok_obat . ".satuan_konversi = '" . $header_data['satuan_konversi'] . "'
					  AND " . $this->tbl_stok_obat . ".sisa > 0
					  AND " . $this->tbl_obat_masuk . ".status = 'sudah'
					ORDER BY " . $this->tbl_stok_obat . ".tanggal_exp ASC
				";
				$stok_rows = $stok_dbtable->get_result($stok_query);
				$jumlah = $header_data['jumlah'];
				foreach($stok_rows as $sr) {
					$sisa_stok = 0;
					$jumlah_stok_keluar = 0;
					if ($sr->sisa >= $jumlah) {
						$sisa_stok = $sr->sisa - $jumlah;
						$jumlah_stok_keluar = $jumlah;
						$jumlah = 0;
					} else {
						$sisa_stok = 0;
						$jumlah_stok_keluar = $sr->sisa;
						$jumlah = $jumlah - $sr->sisa;
					}
					//do update stok obat here:
					$stok_data = array();
					$stok_data['sisa'] = $sisa_stok;
					$stok_id['id'] = $sr->id;
					$stok_dbtable->update($stok_data, $stok_id);
					//logging riwayat stok obat:
					$riwayat_dbtable = new DBTable($this->dbtable->get_db(), $this->tbl_riwayat_stok_obat);
					$data_riwayat = array();
					$data_riwayat['tanggal'] = date("Y-m-d");
					$data_riwayat['id_stok_obat'] = $sr->id;
					$data_riwayat['jumlah_keluar'] = $jumlah_stok_keluar;
					$data_riwayat['sisa'] = $sisa_stok;
					$data_riwayat['keterangan'] = "Mutasi Unit ke " . ArrayAdapter::format("unslug", $header_data['unit']);
					global $user;
					$data_riwayat['nama_user'] = $user->getName();
					$riwayat_dbtable->insert($data_riwayat);
					//do insert stok mutasi depo keluar here:
					$stok_mutasi_depo_keluar_dbtable = new DBTable($this->dbtable->get_db(), $this->tbl_stok_mutasi_depo_keluar);
					$stok_keluar_data = array();
					$stok_keluar_data['id_mutasi_keluar'] = $id['id'];
					$stok_keluar_data['id_stok_obat'] = $sr->id;
					$stok_keluar_data['jumlah'] = $jumlah_stok_keluar;
					$stok_mutasi_depo_keluar_dbtable->insert($stok_keluar_data);
					if ($jumlah == 0) break;
				}
			}
			$success['id'] = $id['id'];
			$success['success'] = 1;
			if ($result === false) $success['success'] = 0;
			return $success;
		}
	}
?>