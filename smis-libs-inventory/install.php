<?php 
	require_once("smis-framework/smis/template/InstallatorTemplate.php");
	require_once("smis-libs-class/DBCreator.php");

	class InventoryInstallator extends InstallatorTemplate {
		private $is_use_obat;
		private $is_use_inventory;
		private $is_use_barang;
		private $property_barang;
		private $property_obat;
		private $dbslug;
		
		public function __construct($db, $slug, $code) {
			parent::__construct($db, $slug, $code);
			$this->is_use_min_max = true;
			$this->is_use_barang = true;
			$this->is_use_inventory = true;
			$this->is_use_obat = true;
			$this->property_barang = array();
			$this->property_obat = array();
		}
		
		public function setPropertyObat($name, $val) {
			$this->property_obat[$name] = $val;
		}
		
		public function setPropertyBarang($name, $val) {
			$this->property_barang[$name] = $val;
		}
		
		public function setUsing($obat, $barang, $inventory) {
			$this->is_use_barang = $barang;
			$this->is_use_inventory = $inventory;
			$this->is_use_obat = $obat;
		}
		
		public function extendInstall($dbslug) {
			$this->dbslug = $dbslug;
			$this->setupRKBU();
			$this->setupDRKBU();
			$this->setupMinMaksSettings();
			$this->setupMutasiAntarRuang();
			$this->setupStokMutasiAntarRuang();
			
			if ($this->is_use_inventory) {
				$this->setupInventaris();
			}
					
			if ($this->is_use_obat) {
				if (!isset($this->property_obat['obat_masuk']) || $this->property_obat['obat_masuk']) 	
					$this->setupObatMasuk();
				if (!isset($this->property_obat['stok_obat']) || $this->property_obat['stok_obat']) 
					$this->setupStokObat();
				if (!isset($this->property_obat['riwayat_stok_obat']) || $this->property_obat['riwayat_stok_obat']) {
					$this->setupRiwayatStokObat();
					$this->setupKartuStokObat();
				}
				if (!isset($this->property_obat['penggunaan']) || $this->property_obat['penggunaan'])
					$this->setupPenggunaanObat();
				if (!isset($this->property_obat['penyesuaian']) || $this->property_obat['penyesuaian'])	
					$this->setupPenyesuaianStokObat();
				if (!isset($this->property_obat['retur']) || $this->property_obat['retur'])
					$this->setupReturObat();
				if (!isset($this->property_obat['permintaan']) || $this->property_obat['permintaan'] ) {
					$this->setupPermintaanObat();
					$this->setupDPermintaanObat();
				}
			}
			
			if ($this->is_use_barang) {
				if (!isset($this->property_barang['barang_masuk']) || $this->property_barang['barang_masuk']) 
					$this->setupBarangMasuk();
				if (!isset($this->property_barang['stok_barang']) || $this->property_barang['stok_barang']) 
					$this->setupStokBarang();
				if (!isset($this->property_barang['riwayat_stok_barang']) || $this->property_barang['riwayat_stok_barang']) {
					$this->setupRiwayatStokBarang();
					$this->setupKartuStokBarang();
				}
				if (!isset($this->property_barang['penggunaan']) || $this->property_barang['penggunaan']) 
					$this->setupPenggunaanBarang();
				if (!isset($this->property_barang['penyesuaian']) || $this->property_barang['penyesuaian'])
					$this->setupPenyesuaianStokBarang();
				if (!isset($this->property_barang['retur']) || $this->property_barang['retur']) 
					$this->setupReturBarang();
				if (!isset($this->property_barang['permintaan']) || $this->property_barang['permintaan']) {
					$this->setupPermintaanBarang();
					$this->setupDPermintaanBarang();
				}
			}
		}
		
		private function setupRKBU() {
			$dbcreator = new DBCreator($this->db, "smis_" . $this->dbslug . "_rkbu" . $this->entity);
			$dbcreator->addColumn("tahun", "INT(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->addColumn("tanggal_diajukan", "DATE", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->addColumn("tanggal_ditinjau", "DATE", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->addColumn("keterangan", "TEXT", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->addColumn("tinjauan", "TEXT", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->addColumn("locked", "TINYINT(1)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_ZERO, false, DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->addColumn("disetujui", "VARCHAR(10)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->initialize();
		}

		private function setupDRKBU() {
			$dbcreator = new DBCreator($this->db, "smis_" . $this->dbslug . "_drkbu" . $this->entity);
			$dbcreator->addColumn("id_rkbu", "INT(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->addColumn("nama_barang", "VARCHAR(64)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->addColumn("jenis_barang", "VARCHAR(32)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->addColumn("jumlah_diajukan", "INT(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->addColumn("satuan_diajukan", "VARCHAR(32)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->addColumn("jumlah_disetujui", "INT(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->addColumn("satuan_disetujui", "VARCHAR(32)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->addColumn("bulan", "VARCHAR(32)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->initialize();
		}

		private function setupBarangMasuk() {
			$dbcreator = new DBCreator($this->db, "smis_" . $this->dbslug . "_barang_masuk" . $this->entity);
			$dbcreator->addColumn("f_id", "INT(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->addColumn("tanggal", "DATE", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->addColumn("unit", "VARCHAR(64)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->addColumn("status", "ENUM('belum','sudah','dikembalikan')", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->addColumn("keterangan", "TEXT", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->initialize();
		}

		private function setupStokBarang() {
			$dbcreator = new DBCreator($this->db, "smis_" . $this->dbslug . "_stok_barang" . $this->entity);
			$dbcreator->addColumn("f_id", "INT(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->addColumn("kode_barang", "VARCHAR(32)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->addColumn("id_barang_masuk", "INT(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->addColumn("id_barang", "VARCHAR(64)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->addColumn("nama_barang", "VARCHAR(64)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->addColumn("nama_jenis_barang", "VARCHAR(64)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->addColumn("id_vendor", "INT(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->addColumn("nama_vendor", "VARCHAR(64)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->addColumn("jumlah", "INT(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->addColumn("sisa", "INT(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->addColumn("satuan", "VARCHAR(64)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->addColumn("konversi", "INT(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->addColumn("satuan_konversi", "VARCHAR(64)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->addColumn("hna", "DOUBLE", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->addColumn("produsen", "VARCHAR(64)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->addColumn("tanggal_exp", "DATE", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->addColumn("turunan", "TINYINT(1)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_ZERO, false,DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->initialize();
		}
		
		private function setupRiwayatStokBarang() {
			$dbcreator = new DBCreator($this->db, "smis_" . $this->dbslug . "_riwayat_stok_barang" . $this->entity);
			$dbcreator->addColumn("tanggal", "DATE", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->addColumn("id_stok_barang", "INT(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->addColumn("jumlah_masuk", "INT(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_ZERO, false,DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->addColumn("jumlah_keluar", "INT(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_ZERO, false,DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->addColumn("sisa", "INT(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->addColumn("keterangan", "TEXT", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->addColumn("nama_user", "VARCHAR(128)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->initialize();
		}

		private function setupObatMasuk() {
			$dbcreator = new DBCreator($this->db, "smis_" . $this->dbslug . "_obat_masuk" . $this->entity);
			$dbcreator->addColumn("f_id", "INT(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->addColumn("tanggal", "DATE", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->addColumn("unit", "VARCHAR(64)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->addColumn("status", "ENUM('belum','sudah','dikembalikan')", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->addColumn("keterangan", "TEXT", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->initialize();
		}

		private function setupStokObat() {
			$dbcreator = new DBCreator($this->db, "smis_" . $this->dbslug . "_stok_obat" . $this->entity);
			$dbcreator->addColumn("f_id", "INT(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->addColumn("id_obat_masuk", "INT(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->addColumn("kode_obat", "VARCHAR(32)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->addColumn("id_obat", "VARCHAR(64)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->addColumn("nama_obat", "VARCHAR(64)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->addColumn("nama_jenis_obat", "VARCHAR(64)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->addColumn("formularium", "TINYINT(1)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_ZERO, false,DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->addColumn("berlogo", "TINYINT(1)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_ZERO, false,DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->addColumn("generik", "TINYINT(1)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_ZERO, false,DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->addColumn("id_vendor", "INT(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->addColumn("nama_vendor", "VARCHAR(64)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->addColumn("label", "VARCHAR(64)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->addColumn("jumlah", "INT(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->addColumn("sisa", "INT(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->addColumn("satuan", "VARCHAR(64)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->addColumn("konversi", "INT(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->addColumn("satuan_konversi", "VARCHAR(64)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->addColumn("hna", "DOUBLE", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->addColumn("ppn", "DOUBLE", DBCreator::$SIGN_NONE, true, 10, false,DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->addColumn("produsen", "VARCHAR(64)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->addColumn("tanggal_exp", "date", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->addColumn("no_batch", "VARCHAR(32)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->addColumn("turunan", "TINYINT(1)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_ZERO, false,DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->initialize();
		}
		
		private function setupRiwayatStokObat() {
			$dbcreator = new DBCreator($this->db, "smis_" . $this->dbslug . "_riwayat_stok_obat" . $this->entity);
			$dbcreator->addColumn("tanggal", "DATE", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->addColumn("id_stok_obat", "INT(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->addColumn("jumlah_masuk", "INT(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_ZERO, false,DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->addColumn("jumlah_keluar", "INT(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_ZERO, false,DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->addColumn("sisa", "INT(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->addColumn("keterangan", "TEXT", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->addColumn("nama_user", "VARCHAR(128)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->initialize();
		}

		private function setupInventaris() {
			$dbcreator = new DBCreator($this->db, "smis_" . $this->dbslug . "_inventaris" . $this->entity);
			$dbcreator->addColumn("f_id", "INT(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->addColumn("nama_barang", "VARCHAR(128)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->addColumn("medis", "TINYINT(1)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_ZERO, false,DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->addColumn("kode", "VARCHAR(128)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->addColumn("merk", "VARCHAR(64)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->addColumn("hna", "DOUBLE", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->addColumn("tahun_perolehan", "DATE", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->addColumn("usia_penyusutan", "INT(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->addColumn("kondisi_baik", "TINYINT(1)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->initialize();
		}

		private function setupPenggunaanBarang() {
			$dbcreator = new DBCreator($this->db, "smis_" . $this->dbslug . "_penggunaan_barang" . $this->entity);
			$dbcreator->addColumn("id_stok_barang", "INT(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->addColumn("tanggal", "DATE", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->addColumn("jumlah", "INT(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->addColumn("keterangan", "TEXT", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->initialize();
		}
			
		private function setupPenggunaanObat() {
			$dbcreator = new DBCreator($this->db, "smis_" . $this->dbslug . "_penggunaan_obat" . $this->entity);
			$dbcreator->addColumn("id_stok_obat", "INT(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->addColumn("tanggal", "DATE", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->addColumn("jumlah", "INT(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->addColumn("keterangan", "TEXT", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->initialize();
		}
		
		private function setupReturBarang() {
			$dbcreator = new DBCreator($this->db, "smis_" . $this->dbslug . "_retur_barang" . $this->entity);
			$dbcreator->addColumn("id_stok_barang", "INT(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->addColumn("tanggal", "DATE", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->addColumn("jumlah", "INT(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->addColumn("keterangan", "TEXT", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->addColumn("status", "ENUM('belum','sudah','dikembalikan')", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->initialize();
		}
		
		private function setupReturObat() {
			$dbcreator = new DBCreator($this->db, "smis_" . $this->dbslug . "_retur_obat" . $this->entity);
			$dbcreator->addColumn("id_stok_obat", "INT(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->addColumn("tanggal", "DATE", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->addColumn("jumlah", "INT(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->addColumn("keterangan", "TEXT", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->addColumn("status", "ENUM('belum','sudah','dikembalikan')", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->initialize();
		}	
		
		private function setupPenyesuaianStokBarang() {
			$dbcreator = new DBCreator($this->db, "smis_" . $this->dbslug . "_penyesuaian_stok_barang" . $this->entity);
			$dbcreator->addColumn("id_stok_barang", "INT(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->addColumn("tanggal", "DATE", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->addColumn("jumlah_lama", "INT(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->addColumn("jumlah_baru", "INT(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->addColumn("keterangan", "TEXT", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->addColumn("nama_user", "VARCHAR(32)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->initialize();
		}
		
		private function setupPenyesuaianStokObat() {
			$dbcreator = new DBCreator($this->db, "smis_" . $this->dbslug . "_penyesuaian_stok_obat" . $this->entity);
			$dbcreator->addColumn("id_stok_obat", "INT(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->addColumn("tanggal", "DATE", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->addColumn("jumlah_lama", "INT(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->addColumn("jumlah_baru", "INT(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->addColumn("keterangan", "TEXT", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->addColumn("nama_user", "VARCHAR(32)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->initialize();
		}
		
		private function setupPermintaanBarang() {
			$dbcreator = new DBCreator($this->db, "smis_" . $this->dbslug . "_permintaan_barang" . $this->entity);
			$dbcreator->addColumn("tanggal", "DATE", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->addColumn("status", "ENUM('belum','sudah','dikembalikan')", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->initialize();	
		}
		
		private function setupDPermintaanBarang() {
			$dbcreator = new DBCreator($this->db, "smis_" . $this->dbslug . "_dpermintaan_barang" . $this->entity);
			$dbcreator->addColumn("id_permintaan_barang", "INT(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->addColumn("nama_barang", "varchar(128)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->addColumn("jumlah_permintaan", "INT(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->addColumn("satuan_permintaan", "VARCHAR(64)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->addColumn("jumlah_dipenuhi", "INT(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->addColumn("satuan_dipenuhi", "VARCHAR(64)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->addColumn("keterangan", "TEXT", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->initialize();		
		}
		
		private function setupPermintaanObat() {
			$dbcreator = new DBCreator($this->db, "smis_" . $this->dbslug . "_permintaan_obat" . $this->entity);
			$dbcreator->addColumn("tanggal", "DATE", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->addColumn("status", "ENUM('belum','sudah','dikembalikan')", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->initialize();	
		}
		
		private function setupDPermintaanObat() {
			$dbcreator = new DBCreator($this->db, "smis_" . $this->dbslug . "_dpermintaan_obat" . $this->entity);
			$dbcreator->addColumn("id_permintaan_obat", "INT(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->addColumn("id_obat", "int(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->addColumn("kode_obat", "varchar(128)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->addColumn("nama_obat", "varchar(256)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->addColumn("nama_jenis_obat", "varchar(128)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->addColumn("jumlah_permintaan", "INT(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->addColumn("satuan_permintaan", "VARCHAR(64)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->addColumn("jumlah_dipenuhi", "INT(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->addColumn("satuan_dipenuhi", "VARCHAR(64)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->addColumn("keterangan", "TEXT", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->initialize();
		}

		private function setupKartuStokObat() {
			$dbcreator = new DBCreator($this->db, "smis_" . $this->dbslug . "_kartu_stok_obat" . $this->entity);
			$dbcreator->addColumn("f_id", "INT(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->addColumn("no_bon", "varchar(32)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->addColumn("unit", "varchar(1024)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->addColumn("id_obat", "INT(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->addColumn("kode_obat", "VARCHAR(32)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->addColumn("nama_obat", "VARCHAR(64)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->addColumn("nama_jenis_obat", "VARCHAR(64)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->addColumn("tanggal", "DATE", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->addColumn("masuk", "INT(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->addColumn("keluar", "INT(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->addColumn("sisa", "INT(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->initialize();
		}

		private function setupKartuStokBarang() {
			$dbcreator = new DBCreator($this->db, "smis_" . $this->dbslug . "_kartu_stok_barang" . $this->entity);
			$dbcreator->addColumn("f_id", "INT(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->addColumn("no_bon", "varchar(32)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->addColumn("unit", "varchar(1024)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->addColumn("id_barang", "INT(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->addColumn("kode_barang", "VARCHAR(32)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->addColumn("nama_barang", "VARCHAR(64)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->addColumn("nama_jenis_barang", "VARCHAR(64)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->addColumn("tanggal", "DATE", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->addColumn("masuk", "INT(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->addColumn("keluar", "INT(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->addColumn("sisa", "INT(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->initialize();
		}

		private function setupMinMaksSettings() {
			$dbcreator = new DBCreator($this->db, "smis_" . $this->dbslug . "_stok_barang_minimum_maksimum" . $this->entity);
			$dbcreator->addColumn("kode_obat", "VARCHAR(32)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->addColumn("kode_barang", "VARCHAR(32)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->addColumn("id_barang", "INT(11)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->addColumn("nama_barang", "VARCHAR(64)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->addColumn("nama_jenis_barang", "VARCHAR(64)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->addColumn("medis", "TINYINT(1)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->addColumn("jumlah_min", "INT(11)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->addColumn("jumlah_max", "INT(11)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->addColumn("satuan", "VARCHAR(32)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->initialize();
		}

		public function setupMutasiAntarRuang() {
			$dbcreator = new DBCreator($this->db, "smis_" . $this->dbslug . "_mutasi_keluar" . $this->entity);
			$dbcreator->addColumn("waktu", "DATE", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->addColumn("unit", "VARCHAR(32)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->addColumn("id_obat", "INT", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->addColumn("kode_obat", "VARCHAR(256)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->addColumn("nama_obat", "VARCHAR(512)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->addColumn("jumlah", "INT", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->addColumn("satuan", "VARCHAR(128)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->addColumn("konversi", "INT", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->addColumn("satuan_konversi", "VARCHAR(128)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->addColumn("status", "VARCHAR(128)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->initialize();
		}

		public function setupStokMutasiAntarRuang() {
			$dbcreator = new DBCreator($this->db, "smis_" . $this->dbslug . "_stok_mutasi_keluar" . $this->entity);
			$dbcreator->addColumn("id_mutasi_keluar", "INT", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->addColumn("id_stok_obat", "INT", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->addColumn("jumlah", "INT", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
			$dbcreator->initialize();
			return $query;
		}
	}
?>