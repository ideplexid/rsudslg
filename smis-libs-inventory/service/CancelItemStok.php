<?php
    class CancelItemStok extends ModulTemplate{
        private $db;
		private $dbslug;
		private $entity;
		private $id_transaksi_arr;
		private $penggunaan_obat_dbtable;
		private $stok_obat_dbtable;
		private $riwayat_stok_obat_dbtable;
        private $jumlah;
		
		public function __construct($db, $dbslug, $entity=""){
			$this->db=$db;
			$this->dbslug=$dbslug;
			$this->entity=$entity;
			$entity=$this->entity==""?"":"_".$this->entity;
			
            $this->id_transaksi_arr = array();
            $this->penggunaan_obat_dbtable = new DBTable($db, "smis_".$dbslug."_penggunaan_obat_" . $entity);
            $this->stok_obat_dbtable = new DBTable($db, "smis_".$dbslug."_stok_obat_" . $entity);
            $this->riwayat_stok_obat_dbtable = new DBTable($db, "smis_".$dbslug."_riwayat_stok_obat_" . $entity);
		}
        
        public function cancelStok($id_transaksi,$keterangan,$operator){            
            $id_transaksi_arr = explode(",", $id_transaksi);
            foreach ($id_transaksi_arr as $id_transaksi_d) {
                // mendapatkan id_stok_obat dan stok digunakan pada transaksi penggunaan obat:
                $row = $this->penggunaan_obat_dbtable->select($id_transaksi_d);			
                $id_stok_obat = $row->id_stok_obat;
                $stok_digunakan = $row->jumlah;
                // membatalkan transaksi penggunaan:
                $penggunaan_obat_data = array(
                    "autonomous"    => "[" . getSettings($this->db, "smis_autonomous_id", "") . "]",
                    "duplicate"     => 0,
                    "time_updated"  => date("Y-m-d H:i:s"),
                    "prop"	        => "del"
                );
                $penggunaan_obat_id_data = array("id" => $id_transaksi_d);
                $this->penggunaan_obat_dbtable->update($penggunaan_obat_data, $penggunaan_obat_id_data);
                // get actual item stock :
                $row = $this->stok_obat_dbtable->select($id_stok_obat);
                $stok_sisa = $row->sisa + $stok_digunakan;
                // update stok obat:
                $stok_obat_data = array(
                    "autonomous"    => "[" . getSettings($this->db, "smis_autonomous_id", "") . "]",
                    "duplicate"     => 0,
                    "time_updated"  => date("Y-m-d H:i:s"),
                    "sisa"	=> $stok_sisa
                );
                $stok_obat_id = array("id"	=> $id_stok_obat);
                $this->stok_obat_dbtable->update($stok_obat_data, $stok_obat_id);
                // simpan riwayat stok:
                $riwayat_stok_obat_data = array(
                    "tanggal"		=> date("Y-m-d"),
                    "id_stok_obat"	    => $id_stok_obat,
                    "jumlah_masuk"	    => $stok_digunakan,
                    "jumlah_keluar"     => 0,
                    "sisa"			    => $stok_sisa,
                    "keterangan"	    => $keterangan,
                    "nama_user"		    => $operator,
                    "autonomous"        => "[" . getSettings($this->db, "smis_autonomous_id", "") . "]",
                    "duplicate"         => 0,
                    "time_updated"      => date("Y-m-d H:i:s"),
                    "origin_updated"    => getSettings($this->db, "smis_autonomous_id", "")
                );
                $this->riwayat_stok_obat_dbtable->insert($riwayat_stok_obat_data);
            }
            $data = array(
                "success"	=> 1,
                "reason"	=> ""
            );
            
            return $data;
        }
        
        public function initialize(){
            $data=null;
            if (isset($_POST['id_obat']) && isset($_POST['jumlah']) && isset($_POST['satuan']) &&
                isset($_POST['keterangan']) && isset($_POST['user']) && isset($_POST['id_transaksi']) ) {
                $this->cancelStok($_POST['id_transaksi'],$_POST['keterangan'],$_POST['user']);
                
            }
            echo json_encode($data);
        }
    }
?>