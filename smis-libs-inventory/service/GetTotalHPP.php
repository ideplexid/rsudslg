<?php    
    class GetTotalHPP extends ModulTemplate{
        private $db;
		private $dbslug;
		private $entity;
		private $tbl_stok_obat;
		private $tbl_obat_masuk;

		public function __construct($db, $dbslug, $entity = "") {
			$this->db = $db;
			$this->dbslug = $dbslug;
			$this->entity = $entity;
			$entity = $this->entity == "" ? "" : "_" . $this->entity;
			$this->tbl_stok_obat = "smis_" . $this->dbslug . "_stok_obat" . $entity;
			$this->tbl_obat_masuk = "smis_" . $this->dbslug . "_obat_masuk" . $entity;
		}
        
        public function getTotalHPP($id_obat) {
            $dbtable = new DBTable($db, $this->tbl_stok_obat);
            $query = "
                SELECT id_obat, SUM(sisa * hna) AS 'total_hpp', satuan
                FROM " . $this->tbl_stok_obat . " a LEFT JOIN  " . $this->tbl_obat_masuk . " b ON a.id_obat_masuk = b.id
                WHERE a.prop NOT LIKE 'del' AND b.status = 'sudah' AND id_obat = '" . $id_obat . "' AND satuan = satuan_konversi AND konversi = '1'
                GROUP BY id_obat, satuan, satuan_konversi
                LIMIT 0, 1";
            $row = $dbtable->get_row($query);
            $data = array(
                "total_hpp" => $row != null ? $row->total_hpp : 0,
                "satuan"	=> $row != null ? $row->satuan : "-"
            );		
            return $data;
        }
        
        public function initialize(){
            $data = null;
            if (isset($_POST['id_obat']) && isset($_POST['ruangan']) ) { 
               $data = $this->getTotalHPP($_POST['id_obat']); 
            }
            echo json_encode($data);
        }
    }	
?>