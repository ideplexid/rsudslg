<?php 
	require_once 'smis-framework/smis/template/ModulTemplate.php';

	class SetMutasiObatAntarUnitStatus extends ModulTemplate {
		private $db;
		private $dbslug;
		private $entity;
		private $unit;
		private $tbl_mutasi_depo_keluar;
		private $tbl_stok_mutasi_depo_keluar;
		private $tbl_stok_obat;
		private $tbl_riwayat_stok_obat;
		
		public function __construct($db, $dbslug, $entity = "", $unit = ""){
			$this->db = $db;
			$this->dbslug = $dbslug;
			$this->entity = $entity;
			$entity = $this->entity == "" ? "" : "_" . $this->entity;
			$this->tbl_mutasi_depo_keluar = "smis_" . $this->dbslug . "_mutasi_keluar" . $entity;
			$this->tbl_stok_mutasi_depo_keluar = "smis_" . $this->dbslug . "_stok_mutasi_keluar" . $entity;
			$this->tbl_stok_obat = "smis_" . $this->dbslug . "_stok_obat" . $entity;
			$this->tbl_riwayat_stok_obat = "smis_" . $this->dbslug . "_riwayat_stok_obat" . $entity;
			$this->unit = $unit;
		}
		
		public function initialize(){
			$response_package = new ResponsePackage();
			if (isset($_POST['command'])) {
				if ($_POST['command'] == "push_update") {
					$id['id'] = $_POST['id'];
					$data['status'] = $_POST['status'];
					$dbtable = new DBTable($this->db, $this->tbl_mutasi_depo_keluar);
					$dbtable->update($data, $id);
					$response_package->setStatus(ResponsePackage::$STATUS_OK);
					$row = $dbtable->select($id['id']);
					if ($data['status'] == "dikembalikan") {
						// restock:
						$stok_mutasi_depo_keluar_dbtable = new DBTable($this->db, $this->tbl_stok_mutasi_depo_keluar);
						$stok_mutasi_depo_keluar_rows = $stok_mutasi_depo_keluar_dbtable->get_result("
							SELECT *
							FROM " . $this->tbl_stok_mutasi_depo_keluar . "
							WHERE id_mutasi_keluar = '" . $id['id'] . "' AND prop NOT LIKE 'del'
						");
						foreach ($stok_mutasi_depo_keluar_rows as $dokr) {
							$stok_keluar_data = array();
							$stok_keluar_data['jumlah'] = 0;
							$stok_keluar_data['prop'] = "del";
							$stok_keluar_id['id'] = $dokr->id;
							$stok_mutasi_depo_keluar_dbtable->update($stok_keluar_data, $stok_keluar_id);
							$stok_obat_dbtable = new DBTable($this->db, $this->tbl_stok_obat);
							$stok_obat_row = $stok_obat_dbtable->select($dokr->id_stok_obat);
							$stok_data = array();
							$stok_data['sisa'] = $stok_obat_row->sisa + $dokr->jumlah;
							$stok_id['id'] = $dokr->id_stok_obat;
							$stok_obat_dbtable->update($stok_data, $stok_id);
							//logging riwayat stok obat:
							$riwayat_dbtable = new DBTable($this->db, $this->tbl_riwayat_stok_obat);
							$data_riwayat = array();
							$data_riwayat['tanggal'] = date("Y-m-d");
							$data_riwayat['id_stok_obat'] = $dokr->id_stok_obat;
							$data_riwayat['jumlah_masuk'] = $dokr->jumlah;
							$data_riwayat['sisa'] = $stok_obat_row->sisa + $dokr->jumlah;
							$data_riwayat['keterangan'] = "Pembatalan Mutasi Antar Unit ke " . ArrayAdapter::format("unslug", $row->unit);
							global $user;
							$data_riwayat['nama_user'] = $_POST['nama_user'];
							$riwayat_dbtable->insert($data_riwayat);
						}
					}
					//notify:
					global $notification;
					$key = md5($row->unit . " " . $row->id);
					$status = ArrayAdapter::format("unslug", $_POST['status']);
					if ($status == "SUDAH")
						$status .= " DITERIMA";
					$msg = "Mutasi Antar Unit ke <strong>".ArrayAdapter::format("unslug", $row->unit)."</strong> sudah dikonfirmasi (<strong>" . $status . "</strong>)";
					$notification->addNotification("Konfirmasi Mutasi Antar Unit", $key, $msg, $row->unit,"mutasi_keluar_unit");
				} else {
					$response_package->setStatus(ResponsePackage::$STATUS_FAIL);
				}
			} else {
				$response_package->setStatus(ResponsePackage::$STATUS_FAIL);
			}
			echo json_encode($response_package->getPackage());
		}	
	}
?>