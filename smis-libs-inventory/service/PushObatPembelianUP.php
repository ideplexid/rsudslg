<?php 
	require_once 'smis-framework/smis/template/ModulTemplate.php';

	class PushObatPembelianUP extends ModulTemplate {
		private $db;
		private $dbslug;
		private $entity;
		private $unit;
		private $tbl_stok_obat;
		private $tbl_obat_masuk;
		private $tbl_riwayat_stok_obat;
		private $tbl_kartu_stok_obat;
		
		public function __construct($db, $dbslug, $entity="", $unit=""){
			$this->db=$db;
			$this->dbslug=$dbslug;
			$this->entity=$entity;
			$entity=$this->entity==""?"":"_".$this->entity;
			$this->tbl_stok_obat="smis_".$this->dbslug."_stok_obat".$entity;
			$this->tbl_obat_masuk="smis_".$this->dbslug."_obat_masuk".$entity;
			$this->tbl_riwayat_stok_obat = "smis_" . $this->dbslug . "_riwayat_stok_obat" . $entity;
			$this->tbl_kartu_stok_obat = "smis_" . $this->dbslug . "_kartu_stok_obat" . $entity;
			$this->unit=$unit;
		}
		
		public function initialize(){
			$response_package = new ResponsePackage();
			if (isset($_POST['command'])) {
				if ($_POST['command'] == "push_save") {
					$obat_masuk_dbtable = new DBTable($this->db,$this->tbl_obat_masuk);
					$id=array();
					$id['f_id']=$_POST['id_obat_keluar'];
					$id['unit']=$_POST['unit'];
					$obat_masuk_row = $obat_masuk_dbtable->select($id);
					if ($obat_masuk_row == null) {
						//do insert:
						$obat_masuk_data = array();
						$obat_masuk_data['f_id'] = $_POST['id_obat_keluar'];
						$obat_masuk_data['tanggal'] = $_POST['tanggal'];
						$obat_masuk_data['unit'] = $_POST['unit'];
						$obat_masuk_data['status'] = $_POST['status'];
						$obat_masuk_data['keterangan'] = $_POST['keterangan'];
						$obat_masuk_data['autonomous'] = "[" . getSettings($this->db, "smis_autonomous_id", "") . "]";
						$obat_masuk_data['duplicate'] = 0;
						$obat_masuk_data['time_updated'] = date("Y-m-d H:i:s");
						$obat_masuk_data['origin_updated'] = getSettings($this->db, "smis_autonomous_id", "");
						$obat_masuk_dbtable->insert($obat_masuk_data);
						$obat_masuk_id = $obat_masuk_dbtable->get_inserted_id();
						$obat_masuk_detail = json_decode($_POST['detail'], true);
						$stok_obat_dbtable = new DBTable($this->db,$this->tbl_stok_obat);
						foreach($obat_masuk_detail as $stok_obat) {
							$stok_obat_data = array();
							$stok_obat_data['f_id'] = $stok_obat['id_stok_obat_keluar'];
							$stok_obat_data['id_obat_masuk'] = $obat_masuk_id;
							$stok_obat_data['kode_obat'] = $stok_obat['kode_obat'];
							$stok_obat_data['id_obat'] = $stok_obat['id_obat'];
							$stok_obat_data['nama_obat'] = $stok_obat['nama_obat'];
							$stok_obat_data['nama_jenis_obat'] = $stok_obat['nama_jenis_obat'];
							$stok_obat_data['formularium'] = $stok_obat['formularium'];
							$stok_obat_data['generik'] = $stok_obat['generik'];
							$stok_obat_data['berlogo'] = $stok_obat['berlogo'];
							$stok_obat_data['label'] = $stok_obat['label'];
							$stok_obat_data['id_vendor'] = $stok_obat['id_vendor'];
							$stok_obat_data['nama_vendor'] = $stok_obat['nama_vendor'];
							$stok_obat_data['jumlah'] = $stok_obat['jumlah'];
							$stok_obat_data['sisa'] = $stok_obat['jumlah'];
							$stok_obat_data['satuan'] = $stok_obat['satuan'];
							$stok_obat_data['konversi'] = $stok_obat['konversi'];
							$stok_obat_data['satuan_konversi'] = $stok_obat['satuan_konversi'];
							$stok_obat_data['hna'] = $stok_obat['hna'];
							$stok_obat_data['produsen'] = $stok_obat['produsen'];
							$stok_obat_data['tanggal_exp'] = $stok_obat['tanggal_exp'];
							$stok_obat_data['no_batch'] = $stok_obat['no_batch'];
							$stok_obat_data['autonomous'] = "[" . getSettings($this->db, "smis_autonomous_id", "") . "]";
							$stok_obat_data['duplicate'] = 0;
							$stok_obat_data['time_updated'] = date("Y-m-d H:i:s");
							$stok_obat_data['origin_updated'] = getSettings($this->db, "smis_autonomous_id", "");
							$stok_obat_dbtable->insert($stok_obat_data);
							$id_stok_obat = $stok_obat_dbtable->get_inserted_id();

							// logging riwayat stok :
							$riwayat_stok_obat_dbtable = new DBTable($this->db, $this->tbl_riwayat_stok_obat);
							$riwayat_stok_obat_data = array(
								"tanggal"		=> date("Y-m-d"),
								"id_stok_obat"	=> $id_stok_obat,
								"jumlah_masuk"	=> $stok_obat['jumlah'],
								"sisa"			=> $stok_obat['jumlah'],
								"keterangan"	=> "Mutasi Pembelian UP " . ArrayAdapter::format("unslug", $_POST['unit']),
								"nama_user"		=> $_POST['username']
							);
							$riwayat_stok_obat_dbtable->insert($riwayat_stok_obat_data);

							// logging kartu stok :
							$sisa_stok = 0;
							$sisa_stok_row = $stok_obat_dbtable->get_row("
								SELECT SUM(a.sisa) AS 'sisa'
								FROM " . $this->tbl_stok_obat . " a INNER JOIN " . $this->tbl_obat_masuk . " b ON a.id_obat_masuk = b.id
								WHERE a.id_obat = '" . $stok_obat['id_obat'] . "' AND b.status = 'sudah' AND a.sisa > 0
							");
							if ($sisa_stok_row != null)
								$sisa_stok = $sisa_stok_row->sisa;
							$kartu_stok_obat_dbtable = new DBTable($this->db, $this->tbl_kartu_stok_obat);
							$kartu_stok_obat_data = array(
								"f_id"				=> $obat_masuk_id,
								"no_bon"			=> $obat_masuk_id,
								"unit"				=> $_POST['unit'],
								"id_obat"			=> $stok_obat['id_obat'],
								"kode_obat"			=> $stok_obat['kode_obat'],
								"nama_obat"			=> $stok_obat['nama_obat'],
								"nama_jenis_obat"	=> $stok_obat['nama_jenis_obat'],
								"tanggal"			=> date("Y-m-d"),
								"masuk"				=> $stok_obat['jumlah'],
								"keluar"			=> 0,
								"sisa"				=> $sisa_stok
							);
							$kartu_stok_obat_dbtable->insert($kartu_stok_obat_data);
						}
					}
				} else {
					$response_package->setStatus(ResponsePackage::$STATUS_FAIL);
				}
			} else {
				$response_package->setStatus(ResponsePackage::$STATUS_FAIL);
			}
			echo json_encode($response_package->getPackage());
		}	
	}
?>