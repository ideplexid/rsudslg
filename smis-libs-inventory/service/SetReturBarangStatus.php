<?php 
	require_once 'smis-framework/smis/template/ModulTemplate.php';

	class SetReturBarangStatus extends ModulTemplate {
		private $db;
		private $dbslug;
		private $entity;
		private $unit;
		
		public function __construct($db, $dbslug, $entity="", $unit=""){
			$this->db=$db;
			$this->dbslug=$dbslug;
			$this->entity=$entity;
			$this->unit=$unit;
		}
		
		public function initialize(){
			$entity=$this->entity==""?"":"_".$this->entity;
			$response_package = new ResponsePackage();
			if (isset($_POST['command']) && $_POST['command'] == "push_save") {
				$id['id'] = $_POST['id'];
				$data['status'] = $_POST['status'];
				$data['autonomous'] = "[" . getSettings($this->db, "smis_autonomous_id", "") . "]";
				$data['duplicate'] = 0;
				$data['time_updated'] = date("Y-m-d H:i:s");
				$retur_barang_dbtable = new DBTable($this->db, "smis_".$this->dbslug."_retur_barang".$entity);
				$retur_barang_dbtable->update($data, $id);
				$response_package->setStatus(ResponsePackage::$STATUS_OK);
				//notify:
				global $notification;
				$key=md5($this->unit." ".$_POST['id']);
				$status = $_POST['status'];
				if ($status == "sudah") {
					$status .= " diterima";
				}
				$msg="Retur Barang dari <strong>" . ArrayAdapter::format("unslug", $this->unit) . "</strong> ke <strong>" . ArrayAdapter::format("unslug", "gudang_umum") . "</strong> sudah dikonfirmasi (<strong>" . ArrayAdapter::format("unslug", $status) . "</strong>)";
				$notification->addNotification("Konfirmasi Retur Barang", $key, $msg, $this->unit,"boi");
				$notification->commit();
			} else {
				$response_package->setStatus(ResponsePackage::$STATUS_FAIL);
			}
			echo json_encode($response_package->getPackage());
		}
	}
?>