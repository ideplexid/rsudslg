<?php 
	require_once 'smis-framework/smis/template/ModulTemplate.php';

	class PushMutasiObatAntarUnit extends ModulTemplate {
		private $db;
		private $dbslug;
		private $entity;
		private $unit;
		private $tbl_stok_obat;
		private $tbl_obat_masuk;
		
		public function __construct($db, $dbslug, $entity = "", $unit = ""){
			$this->db = $db;
			$this->dbslug = $dbslug;
			$this->entity = $entity;
			$entity = $this->entity == "" ? "" : "_" . $this->entity;
			$this->tbl_stok_obat = "smis_" . $this->dbslug . "_stok_obat" . $entity;
			$this->tbl_obat_masuk = "smis_" . $this->dbslug . "_obat_masuk" . $entity;
			$this->unit = $unit;
		}
		
		public function initialize(){
			$response_package = new ResponsePackage();
			if (isset($_POST['command'])) {
				if ($_POST['command'] == "push_save") {
					$obat_masuk_dbtable = new DBTable($this->db, $this->tbl_obat_masuk);
					$id=array();
					$id['f_id'] = $_POST['id_mutasi_obat'];
					$id['unit'] = $_POST['unit'];
					$obat_masuk_row = $obat_masuk_dbtable->select($id);
					if ($obat_masuk_row == null) {
						//do insert:
						$obat_masuk_data = array();
						$obat_masuk_data['f_id'] = $_POST['id_mutasi_obat'];
						$obat_masuk_data['tanggal'] = $_POST['tanggal'];
						$obat_masuk_data['unit'] = $_POST['unit'];
						$obat_masuk_data['status'] = $_POST['status'];
						$obat_masuk_dbtable->insert($obat_masuk_data);
						$obat_masuk_id = $obat_masuk_dbtable->get_inserted_id();
						$obat_masuk_detail = json_decode($_POST['detail'], true);
						$stok_obat_dbtable = new DBTable($this->db,$this->tbl_stok_obat);
						foreach($obat_masuk_detail as $stok_obat) {
							$stok_obat_data = array();
							$stok_obat_data['f_id'] = $stok_obat['id_stok_mutasi_keluar'];
							$stok_obat_data['id_obat_masuk'] = $obat_masuk_id;
							$stok_obat_data['kode_obat'] = $stok_obat['kode_obat'];
							$stok_obat_data['id_obat'] = $stok_obat['id_obat'];
							$stok_obat_data['nama_obat'] = $stok_obat['nama_obat'];
							$stok_obat_data['nama_jenis_obat'] = $stok_obat['nama_jenis_obat'];
							$stok_obat_data['formularium'] = $stok_obat['formularium'];
							$stok_obat_data['generik'] = $stok_obat['generik'];
							$stok_obat_data['berlogo'] = $stok_obat['berlogo'];
							$stok_obat_data['label'] = $stok_obat['label'];
							$stok_obat_data['id_vendor'] = $stok_obat['id_vendor'];
							$stok_obat_data['nama_vendor'] = $stok_obat['nama_vendor'];
							$stok_obat_data['jumlah'] = $stok_obat['jumlah'];
							$stok_obat_data['sisa'] = $stok_obat['jumlah'];
							$stok_obat_data['satuan'] = $stok_obat['satuan'];
							$stok_obat_data['konversi'] = $stok_obat['konversi'];
							$stok_obat_data['satuan_konversi'] = $stok_obat['satuan_konversi'];
							$stok_obat_data['hna'] = $stok_obat['hna'];
							$stok_obat_data['produsen'] = $stok_obat['produsen'];
							$stok_obat_data['tanggal_exp'] = $stok_obat['tanggal_exp'];
							$stok_obat_data['no_batch'] = $stok_obat['no_batch'];
							$stok_obat_dbtable->insert($stok_obat_data);
						}
						//notify:
						global $notification;
						$key=md5($this->unit . " " . $obat_masuk_id);
						$msg = "Mutasi Antar Unit di <strong>" . ArrayAdapter::format("unslug", $this->unit) . "</strong> dari <strong>" . ArrayAdapter::format("unslug", $_POST['unit']) . "</strong>";
						$notification->addNotification("Mutasi Antar Unit", $key, $msg, $this->unit, "boi");
					}
				} else {
					$response_package->setStatus(ResponsePackage::$STATUS_FAIL);
				}
			} else {
				$response_package->setStatus(ResponsePackage::$STATUS_FAIL);
			}
			echo json_encode($response_package->getPackage());
		}	
	}
?>