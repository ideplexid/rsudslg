<?php 
	require_once 'smis-framework/smis/template/ModulTemplate.php';

	class GetMinimumStokObat extends ModulTemplate {
		private $db;
		private $dbslug;
		private $entity;
		private $tbl_stok_obat;
		private $tbl_obat_masuk;
		private $tbl_minmaks;

		public function __construct($db, $dbslug, $entity=""){
			$this->db=$db;
			$this->dbslug=$dbslug;
			$this->entity=$entity;
			$entity=$this->entity==""?"":"_".$this->entity;
			$this->tbl_stok_obat="smis_".$this->dbslug."_stok_obat".$entity;
			$this->tbl_obat_masuk="smis_".$this->dbslug."_obat_masuk".$entity;
			$this->tbl_minmaks="smis_".$dbslug."_stok_barang_minimum_maksimum".$entity;
		}

		public function initialize(){
			if (isset($_POST['command']) && $_POST['command'] == "list") {
				$dbtable = new DBTable($this->db, $this->tbl_minmaks);
				$filter = "";
				if (isset($_POST['kriteria'])) {
					$filter = " AND (v_stok_min.nama_barang LIKE '%" . $_POST['kriteria'] . "%')";
				}
				$query_value = "
					SELECT *
					FROM (
						SELECT ".$this->tbl_minmaks.".id_barang AS 'id', ".$this->tbl_minmaks.".nama_barang, ".$this->tbl_minmaks.".nama_jenis_barang, CASE WHEN v_stok_obat.sisa  IS NULL THEN 0 ELSE v_stok_obat.sisa END AS 'sisa', ".$this->tbl_minmaks.".jumlah_min, ".$this->tbl_minmaks.".satuan
						FROM ".$this->tbl_minmaks." LEFT JOIN (
							SELECT id_obat, nama_obat, nama_jenis_obat, SUM(sisa * konversi) AS 'sisa', satuan_konversi AS 'satuan'
							FROM ".$this->tbl_stok_obat." LEFT JOIN ".$this->tbl_obat_masuk." ON ".$this->tbl_stok_obat.".id_obat_masuk = ".$this->tbl_obat_masuk.".id
							WHERE ".$this->tbl_stok_obat.".prop NOT LIKE 'del' AND ".$this->tbl_obat_masuk.".prop NOT LIKE 'del' AND ".$this->tbl_obat_masuk.".status = 'sudah'
							GROUP BY id_obat, satuan, konversi, satuan_konversi
						) v_stok_obat ON ".$this->tbl_minmaks.".id_barang = v_stok_obat.id_obat AND ".$this->tbl_minmaks.".satuan = v_stok_obat.satuan
						WHERE ".$this->tbl_minmaks.".prop NOT LIKE 'del' AND CASE WHEN v_stok_obat.sisa  IS NULL THEN 0 ELSE v_stok_obat.sisa END < ".$this->tbl_minmaks.".jumlah_min
					) v_stok_min
					WHERE 1 " . $filter . "
				";
				$query_count = "
					SELECT COUNT(*)
					FROM (
						SELECT ".$this->tbl_minmaks.".id_barang AS 'id', ".$this->tbl_minmaks.".nama_barang, ".$this->tbl_minmaks.".nama_jenis_barang, CASE WHEN v_stok_obat.sisa  IS NULL THEN 0 ELSE v_stok_obat.sisa END AS 'sisa', ".$this->tbl_minmaks.".jumlah_min, ".$this->tbl_minmaks.".satuan
						FROM ".$this->tbl_minmaks." LEFT JOIN (
							SELECT id_obat, nama_obat, nama_jenis_obat, SUM(sisa * konversi) AS 'sisa', satuan_konversi AS 'satuan'
							FROM ".$this->tbl_stok_obat." LEFT JOIN ".$this->tbl_obat_masuk." ON ".$this->tbl_stok_obat.".id_obat_masuk = ".$this->tbl_obat_masuk.".id
							WHERE ".$this->tbl_stok_obat.".prop NOT LIKE 'del' AND ".$this->tbl_obat_masuk.".prop NOT LIKE 'del' AND ".$this->tbl_obat_masuk.".status = 'sudah'
							GROUP BY id_obat, satuan, konversi, satuan_konversi
						) v_stok_obat ON ".$this->tbl_minmaks.".id_barang = v_stok_obat.id_obat AND ".$this->tbl_minmaks.".satuan = v_stok_obat.satuan
						WHERE ".$this->tbl_minmaks.".prop NOT LIKE 'del' AND CASE WHEN v_stok_obat.sisa  IS NULL THEN 0 ELSE v_stok_obat.sisa END < ".$this->tbl_minmaks.".jumlah_min
					) v_stok_min
					WHERE 1 " . $filter . "
				";
				$dbtable->setPreferredQuery(true, $query_value, $query_count);
				$service = new ServiceProvider($dbtable);
				$pack = $service->command($_POST['command']);
				echo json_encode($pack);
			}	
		}
	}
?>