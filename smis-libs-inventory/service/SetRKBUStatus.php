<?php 
	require_once 'smis-framework/smis/template/ModulTemplate.php';

	class SetRKBUStatus extends ModulTemplate {
		private $db;
		private $dbslug;
		private $entity;
		private $unit;
		
		public function __construct($db, $dbslug, $entity="", $unit=""){
			$this->db=$db;
			$this->dbslug=$dbslug;
			$this->entity=$entity;
			$this->unit=$unit;
		}
		
		public function initialize(){
			$entity=$this->entity==""?"":"_".$this->entity;
			$response_package = new ResponsePackage();
			if (isset($_POST['command'])) {
				if ($_POST['command'] == "push_return") {
					$id['id'] = $_POST['id'];
					$data['tinjauan'] = $_POST['tinjauan'];
					$data['disetujui'] = "revisi";
					$data['tanggal_ditinjau'] = $_POST['tanggal_ditinjau'];
					$data['locked'] = "0";
					$data['autonomous'] = "[" . getSettings($this->db, "smis_autonomous_id", "") . "]";
					$data['duplicate'] = 0;
					$data['time_updated'] = date("Y-m-d H:i:s");
					$rkbu_dbtable = new DBTable($this->db, "smis_".$this->dbslug."_rkbu".$entity);
					$rkbu_dbtable->update($data, $id);
					$response_package->setStatus(ResponsePackage::$STATUS_OK);
					//notify:
					global $notification;
					$key=md5($this->unit." ".$_POST['id']);
					$msg="RKBU dari <strong>" . ArrayAdapter::format("unslug", $this->unit) . "</strong> dikembalikan";
					$notification->addNotification("RKBU Dikembalikan", $key, $msg, $this->unit,"rkbu");
					$notification->commit();
				} else if ($_POST['command'] == "push_permanent") {
					$id['id'] = $_POST['id'];
					$data['tinjauan'] = $_POST['tinjauan'];
					$data['disetujui'] = $_POST['disetujui'];
					$data['tanggal_ditinjau'] = $_POST['tanggal_ditinjau'];
					$data['autonomous'] = "[" . getSettings($this->db, "smis_autonomous_id", "") . "]";
					$data['duplicate'] = 0;
					$data['time_updated'] = date("Y-m-d H:i:s");
					$rkbu_dbtable = new DBTable($this->db, "smis_".$this->dbslug."_rkbu".$entity);
					$rkbu_dbtable->update($data, $id);
					$rkbu_detail = json_decode($_POST['detail'], true);
					$drkbu_dbtable = new DBTable($this->db, "smis_".$this->dbslug."_drkbu".$entity);
					foreach($rkbu_detail as $drkbu) {
						$drkbu_id_data['id'] = $drkbu['f_id'];
						$drkbu_data['jumlah_disetujui'] = $drkbu['jumlah_disetujui'];
						$drkbu_data['satuan_disetujui'] = $drkbu['satuan_disetujui'];
						$drkbu_data['autonomous'] = "[" . getSettings($this->db, "smis_autonomous_id", "") . "]";
						$drkbu_data['duplicate'] = 0;
						$drkbu_data['time_updated'] = date("Y-m-d H:i:s");
						$drkbu_dbtable->update($drkbu_data, $drkbu_id_data);
					}
					$response_package->setStatus(ResponsePackage::$STATUS_OK);
					//notify:
					global $notification;
					$key=md5($this->unit." ".$_POST['id']);
					$msg="RKBU dari <strong>" . ArrayAdapter::format("unslug", $this->unit) . "</strong> sudah dievaluasi";
					$notification->addNotification("Evaluasi RKBU", $key, $msg, $this->unit,"rkbu");
					$notification->commit();
				}
			} else {
				$response_package->setStatus(ResponsePackage::$STATUS_FAIL);
			}
			echo json_encode($response_package->getPackage());	
		}	
	}
?>