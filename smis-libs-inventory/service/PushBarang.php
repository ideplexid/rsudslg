<?php 
	require_once 'smis-framework/smis/template/ModulTemplate.php';

	class PushBarang extends ModulTemplate {
		private $db;
		private $dbslug;
		private $entity;
		private $unit;
		private $tbl_stok_barang;
		private $tbl_barang_masuk;
		
		public function __construct($db, $dbslug, $entity="", $unit=""){
			$this->db=$db;
			$this->dbslug=$dbslug;
			$this->entity=$entity;
			$entity=$this->entity==""?"":"_".$this->entity;
			$this->tbl_stok_barang="smis_".$this->dbslug."_stok_barang".$entity;
			$this->tbl_barang_masuk="smis_".$this->dbslug."_barang_masuk".$entity;
			$this->unit=$unit;
		}
		
		public function initialize(){
			$response_package = new ResponsePackage();
			if (isset($_POST['command'])) {
				if ($_POST['command'] == "push_save") {
					$barang_masuk_dbtable = new DBTable($this->db, $this->tbl_barang_masuk);
					$id=array();
					$id['f_id']=$_POST['id_barang_keluar'];
					$id['unit']=$_POST['unit'];
					$barang_masuk_row=$barang_masuk_dbtable->select($id);
						
					if ($barang_masuk_row == null) {
						//do insert:
						$barang_masuk_data = array();
						$barang_masuk_data['f_id'] = $_POST['id_barang_keluar'];
						$barang_masuk_data['tanggal'] = $_POST['tanggal'];
						$barang_masuk_data['unit'] = $_POST['unit'];
						$barang_masuk_data['status'] = $_POST['status'];
						$barang_masuk_data['keterangan'] = $_POST['keterangan'];
						$barang_masuk_data['autonomous'] = "[" . getSettings($this->db, "smis_autonomous_id", "") . "]";
						$barang_masuk_data['duplicate'] = 0;
						$barang_masuk_data['time_updated'] = date("Y-m-d H:i:s");
						$barang_masuk_data['origin_updated'] = getSettings($this->db, "smis_autonomous_id", "");
						$barang_masuk_dbtable->insert($barang_masuk_data);
						$barang_masuk_id = $barang_masuk_dbtable->get_inserted_id();
						$barang_masuk_detail = json_decode($_POST['detail'], true);
						$stok_barang_dbtable = new DBTable($this->db, $this->tbl_stok_barang);
						foreach($barang_masuk_detail as $stok_barang) {
							$stok_barang_data = array();
							$stok_barang_data['f_id'] = $stok_barang['id_stok_barang_keluar'];
							$stok_barang_data['id_barang_masuk'] = $barang_masuk_id;
							$stok_barang_data['id_barang'] = $stok_barang['id_barang'];
							$stok_barang_data['nama_barang'] = $stok_barang['nama_barang'];
							$stok_barang_data['nama_jenis_barang'] = $stok_barang['nama_jenis_barang'];
							$stok_barang_data['id_vendor'] = $stok_barang['id_vendor'];
							$stok_barang_data['nama_vendor'] = $stok_barang['nama_vendor'];
							$stok_barang_data['jumlah'] = $stok_barang['jumlah'];
							$stok_barang_data['sisa'] = $stok_barang['jumlah'];
							$stok_barang_data['satuan'] = $stok_barang['satuan'];
							$stok_barang_data['konversi'] = $stok_barang['konversi'];
							$stok_barang_data['satuan_konversi'] = $stok_barang['satuan_konversi'];
							$stok_barang_data['hna'] = $stok_barang['hna'];
							$stok_barang_data['produsen'] = $stok_barang['produsen'];
							$stok_barang_data['tanggal_exp'] = $stok_barang['tanggal_exp'];
							$stok_barang_data['autonomous'] = "[" . getSettings($this->db, "smis_autonomous_id", "") . "]";
							$stok_barang_data['duplicate'] = 0;
							$stok_barang_data['time_updated'] = date("Y-m-d H:i:s");
							$stok_barang_data['origin_updated'] = getSettings($this->db, "smis_autonomous_id", "");
							$stok_barang_dbtable->insert($stok_barang_data);
						}
						//notify:
						global $notification;
						$key=md5($this->unit." ".$barang_masuk_id);
						$msg="Barang Masuk di <strong>".ArrayAdapter::format("unslug", $this->unit)."</strong> dari <strong>GUDANG UMUM</strong>";
						$notification->addNotification("Barang Masuk", $key, $msg, $this->unit,"boi");
					} else {
						//do update:
						$barang_masuk_data = array();
						$barang_masuk_id['id'] = $barang_masuk_row->id;
						$barang_masuk_data['status'] = $_POST['status'];
						$barang_masuk_data['keterangan'] = $_POST['keterangan'];
						$barang_masuk_data['autonomous'] = getSettings($this->db, "smis_autonomous_id", "");
						$barang_masuk_data['duplicate'] = 0;
						$barang_masuk_data['time_updated'] = date("Y-m-d H:i:s");
						$barang_masuk_dbtable->update($barang_masuk_data, $barang_masuk_id);
						$barang_masuk_detail = json_decode($_POST['detail'], true);
						$stok_barang_dbtable = new DBTable($this->db, $this->tbl_stok_barang);
						foreach($barang_masuk_detail as $stok_barang) {
							$ids=array();
							$ids['f_id']=$stok_barang['id_stok_barang_keluar'];
							$ids['id_stok_barang_masuk']=$barang_masuk_row->id;
							$stok_barang_row=$stok_barang_dbtable->select($ids);
								
							if ($stok_barang_row == null) {
								//do insert if f_id not exist:
								$stok_barang_data = array();
								$stok_barang_data['f_id'] = $stok_barang['id_stok_barang_keluar'];
								$stok_barang_data['id_barang_masuk'] = $barang_masuk_row->id;
								$stok_barang_data['id_barang'] = $stok_barang['id_barang'];
								$stok_barang_data['nama_barang'] = $stok_barang['nama_barang'];
								$stok_barang_data['nama_jenis_barang'] = $stok_barang['nama_jenis_barang'];
								$stok_barang_data['id_vendor'] = $stok_barang['id_vendor'];
								$stok_barang_data['nama_vendor'] = $stok_barang['nama_vendor'];
								$stok_barang_data['jumlah'] = $stok_barang['jumlah'];
								$stok_barang_data['sisa'] = $stok_barang['jumlah'];
								$stok_barang_data['satuan'] = $stok_barang['satuan'];
								$stok_barang_data['konversi'] = $stok_barang['konversi'];
								$stok_barang_data['satuan_konversi'] = $stok_barang['satuan_konversi'];
								$stok_barang_data['hna'] = $stok_barang['hna'];
								$stok_barang_data['produsen'] = $stok_barang['produsen'];
								$stok_barang_data['tanggal_exp'] = $stok_barang['tanggal_exp'];
								$stok_barang_data['autonomous'] = "[" . getSettings($this->db, "smis_autonomous_id", "") . "]";
								$stok_barang_data['duplicate'] = 0;
								$stok_barang_data['time_updated'] = date("Y-m-d H:i:s");
								$stok_barang_data['origin_updated'] = getSettings($this->db, "smis_autonomous_id", "");
								$stok_barang_dbtable->insert($stok_barang_data);
							} else {
								//do update or delete if f_id exist:
								$stok_barang_id['id'] = $stok_barang_row->id;
								$stok_barang_data = array();
								$stok_barang_data['jumlah'] = $stok_barang['jumlah'];
								$stok_barang_data['sisa'] = $stok_barang['jumlah'];
								$stok_barang_data['prop'] = $stok_barang['prop'];
								$stok_barang_data['autonomous'] = "[" . getSettings($this->db, "smis_autonomous_id", "") . "]";
								$stok_barang_data['duplicate'] = 0;
								$stok_barang_data['time_updated'] = date("Y-m-d H:i:s");
								$stok_barang_dbtable->update($stok_barang_data, $stok_barang_id);
							}
						}
					}
					$response_package->setStatus(ResponsePackage::$STATUS_OK);
				} else if ($_POST['command'] == "push_delete") {
					$barang_masuk_dbtable = new DBTable($this->db, $this->tbl_barang_masuk);
					$id=array();
					$id['f_id']=$_POST['id_barang_keluar'];
					$id['unit']=$_POST['unit'];
					$barang_masuk_row=$barang_masuk_dbtable->select($id);
						
					if ($barang_masuk_row != null) {
						$barang_masuk_id['id'] = $barang_masuk_row->id;
						$barang_masuk_data['prop'] = "del";
						$barang_masuk_data['autonomous'] = "[" . getSettings($this->db, "smis_autonomous_id", "") . "]";
						$barang_masuk_data['duplicate'] = 0;
						$barang_masuk_data['time_updated'] = date("Y-m-d H:i:s");
						$barang_masuk_dbtable->update($barang_masuk_data, $barang_masuk_id);
					}
					$response_package->setStatus(ResponsePackage::$STATUS_OK);
				} else {
					$response_package->setStatus(ResponsePackage::$STATUS_FAIL);
				}
			} else {
				$response_package->setStatus(ResponsePackage::$STATUS_FAIL);
			}
			echo json_encode($response_package->getPackage());
		}	
	}
?>