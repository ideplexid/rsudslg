<?php 
	require_once 'smis-framework/smis/template/ModulTemplate.php';

	class PushInventaris extends ModulTemplate {
		private $db;
		private $dbslug;
		private $entity;
		private $unit;
		private $tbl_inventaris;
		
		public function __construct($db, $dbslug, $entity="", $unit=""){
			$this->db=$db;
			$this->dbslug=$dbslug;
			$this->entity=$entity;
			$entity=$this->entity==""?"":"_".$this->entity;
			$this->tbl_inventaris="smis_".$this->dbslug."_inventaris".$entity;
			$this->unit=$unit;
		}
		
		public function initialize(){
			$response_package = new ResponsePackage();
			if (isset($_POST['command'])) {
				if ($_POST['command'] == "push_insert") {
					$inventaris_dbtable = new DBTable($this->db, $this->tbl_inventaris);
					$data['f_id'] = $_POST['id_inventaris'];
					$data['nama_barang'] = $_POST['nama_barang'];
					$data['medis'] = $_POST['medis'];
					$data['kode'] = $_POST['kode'];
					$data['merk'] = $_POST['merk'];
					$data['hna'] = $_POST['harga_perolehan'];
					$data['tahun_perolehan'] = $_POST['tahun_perolehan'];
					$data['usia_penyusutan'] = $_POST['usia_penyusutan'];
					$data['kondisi_baik'] = $_POST['kondisi_baik'];
					$data['autonomous'] = "[" . getSettings($this->db, "smis_autonomous_id", "") . "]";
					$data['duplicate'] = 0;
					$data['time_updated'] = date("Y-m-d H:i:s");
					$inventaris_dbtable->insert($data);
					$inventaris_id = $inventaris_dbtable->get_inserted_id();
					$response_package->setStatus(ResponsePackage::$STATUS_OK);
					//notify:
					global $notification;
					$key=md5($this->unit." ".$inventaris_id);
					$msg="Inventaris <strong>" . $data['nama_barang'] . "</strong> Masuk di <strong>".ArrayAdapter::format("unslug", $this->unit)."</strong> dari <strong>GUDANG UMUM</strong>";
					$notification->addNotification("Inventaris Masuk", $key, $msg, $this->unit,"boi");
				} else if ($_POST['command'] == "push_update") {
					$inventaris_dbtable = new DBTable($this->db, $this->tbl_inventaris);
					$idf=array();
					$idf['f_id']=$_POST['id_inventaris'];
					$inventaris_row = $inventaris_dbtable->select($idf);
					$id['id'] = $inventaris_row->id;
					$data['f_id'] = $_POST['id_inventaris'];
					$data['nama_barang'] = $_POST['nama_barang'];
					$data['medis'] = $_POST['medis'];
					$data['kode'] = $_POST['kode'];
					$data['merk'] = $_POST['merk'];
					$data['hna'] = $_POST['harga_perolehan'];
					$data['tahun_perolehan'] = $_POST['tahun_perolehan'];
					$data['usia_penyusutan'] = $_POST['usia_penyusutan'];
					$data['kondisi_baik'] = $_POST['kondisi_baik'];
					$data['autonomous'] = "[" . getSettings($this->db, "smis_autonomous_id", "") . "]";
					$data['duplicate'] = 0;
					$data['time_updated'] = date("Y-m-d H:i:s");
					$inventaris_dbtable->update($data, $id);
					$response_package->setStatus(ResponsePackage::$STATUS_OK);
				} else {
					$response_package->setStatus(ResponsePackage::$STATUS_FAIL);
				}
			} else {
				$response_package->setStatus(ResponsePackage::$STATUS_OK);
			}
			echo json_encode($response_package->getPackage());
		}	
	}
?>