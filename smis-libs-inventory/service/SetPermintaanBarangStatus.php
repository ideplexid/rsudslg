<?php 
	require_once 'smis-framework/smis/template/ModulTemplate.php';

	class SetPermintaanBarangStatus extends ModulTemplate {
		private $db;
		private $dbslug;
		private $entity;
		private $unit;
		
		public function __construct($db, $dbslug, $entity="", $unit=""){
			$this->db=$db;
			$this->dbslug=$dbslug;
			$this->entity=$entity;
			$this->unit=$unit;
		}
		
		public function initialize(){
			$entity=$this->entity==""?"":"_".$this->entity;
			$response_package = new ResponsePackage();
			if (isset($_POST['command']) && $_POST['command'] == "push_update") {
				$id['id'] = $_POST['id'];
				$data['status'] = $_POST['status'];
				$data['autonomous'] = "[" . getSettings($this->db, "smis_autonomous_id", "") . "]";
				$data['duplicate'] = 0;
				$data['time_updated'] = date("Y-m-d H:i:s");
				$permintaan_barang_dbtable = new DBTable($this->db, "smis_".$this->dbslug."_permintaan_barang".$entity);
				$permintaan_barang_dbtable->update($data, $id);
				$detail = json_decode($_POST['detail'], true);
				$dpermintaan_barang_dbtable = new DBTable($this->db, "smis_".$this->dbslug."_dpermintaan_barang".$entity);
				foreach($detail as $d) {
					$d_id['id'] = $d['f_id'];
					$d_data['jumlah_dipenuhi'] = $d['jumlah_dipenuhi'];
					$d_data['satuan_dipenuhi'] = $d['satuan_dipenuhi'];
					$d_data['keterangan'] = $d['keterangan'];
					$d_data['autonomous'] = "[" . getSettings($this->db, "smis_autonomous_id", "") . "]";
					$d_data['duplicate'] = 0;
					$d_data['time_updated'] = date("Y-m-d H:i:s");
					$dpermintaan_barang_dbtable->update($d_data, $d_id);
				}
				$response_package->setStatus(ResponsePackage::$STATUS_OK);
				//notify:
				global $notification;
				$key=md5($this->unit." ".$_POST['id']);
				$msg="Permintaan Barang dari <strong>" . ArrayAdapter::format("unslug", $this->unit) . "</strong> ke <strong>" . ArrayAdapter::format("unslug", "gudang_umum") . "</strong> sudah dievaluasi";
				$notification->addNotification("Evaluasi Permintaan Barang", $key, $msg, $this->unit,"boi");
				$notification->commit();
			} else {
				$response_package->setStatus(ResponsePackage::$STATUS_FAIL);
			}
			echo json_encode($response_package->getPackage());
		}	
	}
?>