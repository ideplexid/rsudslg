<?php 
require_once 'smis-framework/smis/template/ModulTemplate.php';

class IsInventory extends ModulTemplate{
	
	private $db;
	private $entity;
	private $name;
	private $always_true;
	private $barang;
	private $obat;
	
	public function __construct($db, $name, $entity){
		$this->db=$db;
		$this->entity=$entity;
		$this->name=$name;
		$this->barang="1";
		$this->obat="1";
	}
	
	public function setAlways($always){
		$this->always_true=$always;
	}
	
	public function setData($barang,$obat){
		$this->barang=$barang;
		$this->obat=$obat;
	}
	
	public function initialize(){
		$a=array();
		$a["name"]=$this->name;
		if($this->always_true){
			$a["value_barang"]=$this->barang;
			$a["value_obat"]=$this->obat;
		}else{
			$o=getSettings($this->db, "smis-is-inventory-obat-".$this->entity, "1");
			$b=getSettings($this->db, "smis-is-inventory-barang-".$this->entity, "1");
			$a["value_barang"]=$b;
			$a["value_obat"]=$o;
		}
		echo json_encode($a);
	}
	
	
	
}

?>