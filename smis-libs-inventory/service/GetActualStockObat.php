<?php 
	require_once 'smis-framework/smis/template/ModulTemplate.php';

	class GetActualStockObat extends ModulTemplate {
		private $db;
		private $dbslug;
		private $entity;
		private $tbl_stok_obat;
		private $tbl_obat_masuk;

		public function __construct($db, $dbslug, $entity=""){
			$this->db=$db;
			$this->dbslug=$dbslug;
			$this->entity=$entity;
			$entity=$this->entity==""?"":"_".$this->entity;
			$this->tbl_stok_obat="smis_".$this->dbslug."_stok_obat".$entity;
			$this->tbl_obat_masuk="smis_".$this->dbslug."_obat_masuk".$entity;
		}

		public function initialize(){
			if (isset($_POST['command']) && $_POST['command'] == "list") {
				$dbtable = new DBTable($this->db, $this->tbl_stok_obat);
				$filter = "";
				if (isset($_POST['kriteria'])) {
					$filter = " AND (nama_obat LIKE '%" . $_POST['kriteria'] . "%' OR nama_jenis_obat LIKE '" . $_POST['kriteria'] . "')";
				}
				$query_value = "
					SELECT *
					FROM (
						SELECT " . $this->tbl_stok_obat . ".id_obat AS 'id', " . $this->tbl_stok_obat . ".nama_obat, " . $this->tbl_stok_obat . ".nama_jenis_obat, SUM(" . $this->tbl_stok_obat . ".sisa) AS 'sisa', " . $this->tbl_stok_obat . ".satuan, " . $this->tbl_stok_obat . ".konversi, " . $this->tbl_stok_obat . ".satuan_konversi, MAX(" . $this->tbl_stok_obat . ".hna) AS 'maks_hna', MIN(" . $this->tbl_stok_obat . ".hna) AS 'min_hna', IF(" . $this->tbl_stok_obat . ".nama_jenis_obat = 'ALAT KESEHATAN' OR " . $this->tbl_stok_obat . ".nama_jenis_obat = 'ALKES', MAX(" . $this->tbl_stok_obat . ".hna) + MAX(" . $this->tbl_stok_obat . ".hna) * 0.225, MAX(" . $this->tbl_stok_obat . ".hna) + MAX(" . $this->tbl_stok_obat . ".hna) * 0.25) AS 'hja'
						FROM " . $this->tbl_stok_obat . " LEFT JOIN " . $this->tbl_obat_masuk . " ON " . $this->tbl_stok_obat . ".id_obat_masuk = " . $this->tbl_obat_masuk . ".id
						WHERE " . $this->tbl_stok_obat . ".prop NOT LIKE 'del' AND " . $this->tbl_obat_masuk . ".prop NOT LIKE 'del' AND " . $this->tbl_obat_masuk . ".status = 'sudah'
						GROUP BY id_obat, satuan, konversi, satuan_konversi
						ORDER BY nama_obat, nama_jenis_obat, satuan ASC
					) v_stok
					WHERE 1 " . $filter . "
				";
				$query_count = "
					SELECT COUNT(*)
					FROM (
						SELECT " . $this->tbl_stok_obat . ".id_obat AS 'id', " . $this->tbl_stok_obat . ".nama_obat, " . $this->tbl_stok_obat . ".nama_jenis_obat, SUM(" . $this->tbl_stok_obat . ".sisa) AS 'sisa', " . $this->tbl_stok_obat . ".satuan, " . $this->tbl_stok_obat . ".konversi, " . $this->tbl_stok_obat . ".satuan_konversi, MAX(" . $this->tbl_stok_obat . ".hna) AS 'maks_hna', MIN(" . $this->tbl_stok_obat . ".hna) AS 'min_hna', IF(" . $this->tbl_stok_obat . ".nama_jenis_obat = 'ALAT KESEHATAN' OR " . $this->tbl_stok_obat . ".nama_jenis_obat = 'ALKES', MAX(" . $this->tbl_stok_obat . ".hna) + MAX(" . $this->tbl_stok_obat . ".hna) * 0.225, MAX(" . $this->tbl_stok_obat . ".hna) + MAX(" . $this->tbl_stok_obat . ".hna) * 0.25) AS 'hja'
						FROM " . $this->tbl_stok_obat . " LEFT JOIN " . $this->tbl_obat_masuk . " ON " . $this->tbl_stok_obat . ".id_obat_masuk = " . $this->tbl_obat_masuk . ".id
						WHERE " . $this->tbl_stok_obat . ".prop NOT LIKE 'del' AND " . $this->tbl_obat_masuk . ".prop NOT LIKE 'del' AND " . $this->tbl_obat_masuk . ".status = 'sudah'
						GROUP BY id_obat, satuan, konversi, satuan_konversi
						ORDER BY nama_obat, nama_jenis_obat, satuan ASC
					) v_stok
					WHERE 1 " . $filter . "
				";
				$dbtable->setPreferredQuery(true, $query_value, $query_count);
				$service = new ServiceProvider($dbtable);
				$pack = $service->command($_POST['command']);
				echo json_encode($pack);
			}	
		}
	}
?>