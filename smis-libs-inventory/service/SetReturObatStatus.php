<?php 
	require_once 'smis-framework/smis/template/ModulTemplate.php';

	class SetReturObatStatus extends ModulTemplate {
		private $db;
		private $dbslug;
		private $entity;
		private $unit;
		
		public function __construct($db, $dbslug, $entity="", $unit=""){
			$this->db=$db;
			$this->dbslug=$dbslug;
			$this->entity=$entity;
			$this->unit=$unit;
		}
		
		public function initialize(){
			$entity=$this->entity==""?"":"_".$this->entity;
			$response_package = new ResponsePackage();
			if (isset($_POST['command']) && $_POST['command'] == "push_save") {
				$id['id'] = $_POST['id'];
				$data['status'] = $_POST['status'];
				$retur_barang_dbtable = new DBTable($this->db, "smis_".$this->dbslug."_retur_obat".$entity);
				$retur_barang_dbtable->update($data, $id);
				$response_package->setStatus(ResponsePackage::$STATUS_OK);
				//notify:
				global $notification;
				$key=md5($this->unit." ".$_POST['id']);
				$status = $_POST['status'];
				if ($status == "sudah") {
					$status .= " diterima";
				}
				$msg="Retur Obat dari <strong>" . ArrayAdapter::format("unslug", $this->unit) . "</strong> ke <strong>" . ArrayAdapter::format("unslug", "gudang_farmasi") . "</strong> sudah dikonfirmasi (<strong>" . ArrayAdapter::format("unslug", $status) . "</strong>)";
				$notification->addNotification("Konfirmasi Retur Obat", $key, $msg, $this->unit,"boi");
				$notification->commit();
			} else {
				$response_package->setStatus(ResponsePackage::$STATUS_FAIL);
			}
			echo json_encode($response_package->getPackage());
		}
	}
?>