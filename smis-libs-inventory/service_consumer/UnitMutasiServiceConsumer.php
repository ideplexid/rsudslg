<?php 
	class UnitMutasiServiceConsumer extends ServiceConsumer {
		private $entity;
		
		public function __construct($db, $entity) {
			parent::__construct($db, "get_entity", "push_mutasi_obat_antarunit");
			$this->entity = $entity;
		}
		public function proceedResult() {
			$content = array();
			$result = json_decode($this->result, true);
			foreach ($result as $autonomous) {
				foreach ($autonomous as $entity) {
					if (
						$this->entity != "depo_farmasi_icu" &&
						$this->entity != "depo_farmasi_igd" &&
						$this->entity != "depo_farmasi_irja" &&
						$this->entity != "depo_farmasi_irja_irna" &&
						$this->entity != "depo_farmasi_irja_lt1" &&
						$this->entity != "depo_farmasi_irna" &&
						$this->entity != "depo_farmasi_ok"
					) {
						if ($entity != $this->entity) {
							$option = array();
							$option['value'] = $entity;
							$option['name'] = ArrayAdapter::format("unslug", $entity);
							$number = count($content);
							$content[$number] = $option;
						}
					} else {
						if ($entity != $this->entity && strpos($entity, "depo_farmasi") !== false) {
							$option = array();
							$option['value'] = $entity;
							$option['name'] = ArrayAdapter::format("unslug", $entity);
							$number = count($content);
							$content[$number] = $option;
						}
					}
				}
			}
			return $content;
		}
	}
?>