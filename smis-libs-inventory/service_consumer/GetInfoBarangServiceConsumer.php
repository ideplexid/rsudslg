<?php
	class GetInfoBarangServiceConsumer extends ServiceConsumer {
		public function __construct($db, $id_barang, $nama_barang, $nama_jenis_barang) {
			$array['id_barang'] = $id_barang;
			$array['nama_barang'] = $nama_barang;
			$array['nama_jenis_barang'] = $nama_jenis_barang;
			$array['command'] = "edit";
			parent::__construct($db, "get_info_barang", $array, "gudang_umum");
		}
		public function proceedResult() {
			$content = array();
			$result = json_decode($this->result,true);
			foreach ($result as $autonomous) {
				foreach ($autonomous as $entity) {
					if ($entity != null || $entity != "")
						return $entity;
				}
			}
			return $content;
		}
	}
?>