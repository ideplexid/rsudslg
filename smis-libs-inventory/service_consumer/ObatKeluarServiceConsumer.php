<?php 
	class SetObatKeluarStatusServiceConsumer extends ServiceConsumer {
		public function __construct($db, $nama_user, $id, $status, $keterangan, $command) {
			$array['id'] = $id;
			$array['status'] = $status;
			$array['nama_user'] = $nama_user;
			$array['keterangan'] = $keterangan;
			$array['command'] = $command;
			parent::__construct($db, "set_obat_keluar_status", $array, "gudang_farmasi");
		}
		public function proceedResult() {
			$content = array();
			$result = json_decode($this->result,true);
			foreach ($result as $autonomous) {
				foreach ($autonomous as $entity) {
					if ($entity != null || $entity != "")
						return $entity;
				}
			}
			return $content;
		}
	}
?>