<?php 
	class SetMutasiObatAntarUnitStatusServiceConsumer extends ServiceConsumer {
		public function __construct($db, $nama_user, $id, $status, $unit, $command) {
			$array['id'] = $id;
			$array['status'] = $status;
			$array['nama_user'] = $nama_user;
			$array['command'] = $command;
			parent::__construct($db, "set_mutasi_obat_antarunit_status", $array, $unit);
		}
		public function proceedResult() {
			$content = array();
			$result = json_decode($this->result,true);
			foreach ($result as $autonomous) {
				foreach ($autonomous as $entity) {
					if ($entity != null || $entity != "")
						return $entity;
				}
			}
			return $content;
		}
	}
?>