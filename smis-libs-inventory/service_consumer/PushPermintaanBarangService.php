<?php 
	class PushPermintaanBarangService extends ServiceConsumer {
		public function __construct($db, $id_permintaan_barang, $tanggal, $status, $detail, $command, $unit) {
			$array['id_permintaan_barang'] = $id_permintaan_barang;
			$array['unit'] = $unit;
			$array['tanggal'] = $tanggal;
			$array['status'] = $status;
			$array['detail'] = json_encode($detail);
			$array['command'] = $command;
			parent::__construct($db, "push_permintaan_barang", $array, "gudang_umum");
		}
		public function proceedResult() {
			$content = array();
			$result = json_decode($this->result,true);
			foreach ($result as $autonomous) {
				foreach ($autonomous as $entity) {
					if ($entity != null || $entity != "")
						return $entity;
				}
			}
			return $content;
		}
	}
?>