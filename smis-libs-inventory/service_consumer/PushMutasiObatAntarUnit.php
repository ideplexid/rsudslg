<?php
class PushMutasiObatAntarUnit extends ServiceConsumer {
	public function __construct($db, $slug, $id_mutasi_obat, $tanggal, $depo_tujuan, $status, $detail, $command) {
		$array['id_mutasi_obat'] = $id_mutasi_obat;
		$array['tanggal'] = $tanggal;
		$array['unit'] = $slug;
		$array['status'] = $status;
		$array['detail'] = json_encode($detail);
		$array['command'] = $command;
		parent::__construct($db, "push_mutasi_obat_antarunit", $array, $depo_tujuan);
	}
	public function proceedResult() {
		$content = array();
		$result = json_decode($this->result,true);
		foreach ($result as $autonomous) {
			foreach ($autonomous as $entity) {
				if ($entity != null || $entity != "")
					return $entity;
			}
		}
		return $content;
	}
}
?>