<?php 
	class PushReturBarangServiceConsumer extends ServiceConsumer {
		public function __construct($db, $id_retur_barang, $tanggal, $id_barang, $f_id, $nama_barang, $nama_jenis_barang, $id_vendor, $nama_vendor, $jumlah, $satuan, $konversi, $satuan_konversi, $hna, $produsen, $tanggal_exp, $turunan, $keterangan, $status, $command, $unit) {
			$array = array();
			$array['id_retur_barang'] = $id_retur_barang;
			$array['unit'] = $unit;
			$array['tanggal'] = $tanggal;
			$array['id_barang'] = $id_barang;
			$array['f_id'] = $f_id;			
			$array['nama_barang'] = $nama_barang;
			$array['nama_jenis_barang'] = $nama_jenis_barang;
			$array['id_vendor'] = $id_vendor;
			$array['nama_vendor'] = $nama_vendor;
			$array['jumlah'] = $jumlah;
			$array['satuan'] = $satuan;
			$array['konversi'] = $konversi;
			$array['satuan_konversi'] = $satuan_konversi;
			$array['hna'] = $hna;
			$array['produsen'] = $produsen;
			$array['tanggal_exp'] = $tanggal_exp;
			$array['turunan'] = $turunan;
			$array['keterangan'] = $keterangan;
			$array['status'] = $status;
			$array['command'] = $command;
			parent::__construct($db, "push_retur_barang", $array, "gudang_umum");
		}
		public function proceedResult() {
			$content = array();
			$result = json_decode($this->result,true);
			foreach ($result as $autonomous) {
				foreach ($autonomous as $entity) {
					if ($entity != null || $entity != "")
						return $entity;
				}
			}
			return $content;
		}
	}
?>