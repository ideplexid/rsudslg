<?php 
	class PushReturObatServiceConsumer extends ServiceConsumer {
		public function __construct($db, $id_retur_obat, $tanggal, $id_obat, $kode_obat, $f_id, $nama_obat, $nama_jenis_obat, $label, $id_vendor, $nama_vendor, $jumlah, $satuan, $konversi, $satuan_konversi, $hna, $produsen, $tanggal_exp, $no_batch, $ppn, $turunan, $keterangan, $status, $command, $unit) {
			$array = array();
			$array['id_retur_obat'] = $id_retur_obat;
			$array['unit'] = $unit;
			$array['tanggal'] = $tanggal;
			$array['id_obat'] = $id_obat;
			$array['kode_obat'] = $kode_obat;
			$array['f_id'] = $f_id;
			$array['nama_obat'] = $nama_obat;
			$array['nama_jenis_obat'] = $nama_jenis_obat;
			$array['label'] = $label;
			$array['id_vendor'] = $id_vendor;
			$array['nama_vendor'] = $nama_vendor;
			$array['jumlah'] = $jumlah;
			$array['satuan'] = $satuan;
			$array['konversi'] = $konversi;
			$array['satuan_konversi'] = $satuan_konversi;
			$array['hna'] = $hna;
			$array['produsen'] = $produsen;
			$array['tanggal_exp'] = $tanggal_exp;
			$array['no_batch'] = $no_batch;
			$array['ppn'] = $ppn;
			$array['turunan'] = $turunan;
			$array['keterangan'] = $keterangan;
			$array['status'] = $status;
			$array['command'] = $command;
			parent::__construct($db, "push_retur_obat", $array, "gudang_farmasi");
		}
		public function proceedResult() {
			$content = array();
			$result = json_decode($this->result,true);
			foreach ($result as $autonomous) {
				foreach ($autonomous as $entity) {
					if ($entity != null || $entity != "")
						return $entity;
				}
			}
			return $content;
		}
	}
?>