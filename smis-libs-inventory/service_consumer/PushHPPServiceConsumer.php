<?php 
	class PushHPPServiceConsumer extends ServiceConsumer {	
		public function __construct($db, $id_obat, $tanggal_transaksi, $in_out, $keterangan) {
			$array['id_obat'] = $id_obat;
			$array['tanggal_transaksi'] = $tanggal_transaksi;
			$array['in_out'] = $in_out;
			$array['keterangan'] = $keterangan;
			parent::__construct($db, "push_hpp", $array, "farmasi");
		}
		public function proceedResult() {
			$content = array();
			$result = json_decode($this->result,true);
			foreach ($result as $autonomous) {
				foreach ($autonomous as $entity) {
					if ($entity != null || $entity != "")
						return $entity;
				}
			}
			return $content;
		}
	}
?>