<?php 
	class PushPermintaanObatService extends ServiceConsumer {
		public function __construct($db, $id_permintaan_obat, $tanggal, $status, $detail, $command,$entity) {
			$array['id_permintaan_obat'] = $id_permintaan_obat;
			$array['unit'] = $entity;
			$array['tanggal'] = $tanggal;
			$array['status'] = $status;
			$array['detail'] = json_encode($detail);
			$array['command'] = $command;
			parent::__construct($db, "push_permintaan_obat", $array, "gudang_farmasi");
		}
		public function proceedResult() {
			$content = array();
			$result = json_decode($this->result,true);
			foreach ($result as $autonomous) {
				foreach ($autonomous as $entity) {
					if ($entity != null || $entity != "")
						return $entity;
				}
			}
			return $content;
		}
	}
?>