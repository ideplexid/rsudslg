<?php 
	class PushRKBUServiceConsumer extends ServiceConsumer {	
		public function __construct($db, $id_rkbu, $tahun, $tanggal_diajukan, $keterangan, $disetujui, $locked, $detail, $command,$unit) {
			$array['id_rkbu'] = $id_rkbu;
			$array['tahun'] = $tahun;
			$array['tanggal_diajukan'] = $tanggal_diajukan;
			$array['keterangan'] = $keterangan;
			$array['disetujui'] = $disetujui;
			$array['locked'] = $locked;
			$array['unit'] = $unit;
			$array['detail'] = json_encode($detail);
			$array['command'] = $command;
			parent::__construct($db, "push_rkbu", $array, "perencanaan");
		}
		public function proceedResult() {
			$content = array();
			$result = json_decode($this->result,true);
			foreach ($result as $autonomous) {
				foreach ($autonomous as $entity) {
					if ($entity != null || $entity != "")
						return $entity;
				}
			}
			return $content;
		}
	}
?>