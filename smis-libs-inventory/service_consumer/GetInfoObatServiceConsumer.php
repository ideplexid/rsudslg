<?php
	class GetInfoObatServiceConsumer extends ServiceConsumer {
		public function __construct($db, $id_obat, $nama_obat, $nama_jenis_obat) {
			$array['id_obat'] = $id_obat;
			$array['nama_obat'] = $nama_obat;
			$array['nama_jenis_obat'] = $nama_jenis_obat;
			$array['command'] = "edit";
			parent::__construct($db, "get_info_obat", $array, "gudang_farmasi");
		}
		public function proceedResult() {
			$content = array();
			$result = json_decode($this->result,true);
			foreach ($result as $autonomous) {
				foreach ($autonomous as $entity) {
					if ($entity != null || $entity != "")
						return $entity;
				}
			}
			return $content;
		}
	}
?>