<?php
	require_once("smis-framework/smis/template/ModulTemplate.php");
	require_once("smis-base/smis-include-service-consumer.php");
	require_once("smis-libs-inventory/service_consumer/UnitMutasiServiceConsumer.php");
	require_once("smis-libs-inventory/responder/MutasiUnitKeluarDBResponder.php");
	require_once("smis-libs-inventory/responder/ObatDBResponder.php");
	require_once("smis-libs-inventory/responder/SisaDBResponder.php");
	require_once("smis-libs-inventory/service_consumer/PushMutasiObatAntarUnit.php");

	class MutasiKeluarUnit extends ModulTemplate {
		private $db;
		private $name;
		private $tbl_obat_masuk;
		private $tbl_stok_obat;
		private $tbl_stok_mutasi_obat_keluar;
		private $tbl_mutasi_obat_keluar;
		private $tbl_kartu_stok_obat;
		private $tbl_riwayat_stok_obat;
		private $mutasi_keluar_unit_table;
		private $mutasi_keluar_unit_modal;
		private $proto_name;
		private $proto_implement;
		private $proto_slug;

		private $is_manual_date;

		public function __construct($db, $name_entity, $tbl_obat_masuk, $tbl_stok_obat, $tbl_mutasi_obat_keluar, $tbl_stok_mutasi_obat_keluar, $tbl_kartu_stok_obat, $tbl_riwayat_stok_obat, $page) {
			$this->db = $db;
			$this->name = $name_entity;
			$this->tbl_obat_masuk = $tbl_obat_masuk;
			$this->tbl_stok_obat = $tbl_stok_obat;
			$this->tbl_mutasi_obat_keluar = $tbl_mutasi_obat_keluar;
			$this->tbl_stok_mutasi_obat_keluar = $tbl_stok_mutasi_obat_keluar;
			$this->tbl_kartu_stok_obat = $tbl_kartu_stok_obat;
			$this->tbl_riwayat_stok_obat = $tbl_riwayat_stok_obat;
			$this->page = $page;

			$this->is_manual_date = false;

			$header = array(
				"No. Mutasi", "Tanggal", "Unit Tujuan", "Kode Obat", "Nama Obat", "Jumlah", "Satuan", "Status"
			);
			$this->mutasi_keluar_unit_table = new Table(
				$header,
				$this->name . " : Mutasi Keluar Unit",
				null,
				true
			);
			$this->mutasi_keluar_unit_table->setName("mutasi_keluar_unit");
			$this->mutasi_keluar_unit_table->setEditButtonEnable(false);
			$this->mutasi_keluar_unit_table->setDelButtonEnable(false);
			$this->mutasi_keluar_unit_table->setPrintButtonEnable(false);
		}

		public function setPrototype($pname, $pslug, $pimplement) {
			$this->proto_name = $pname;
			$this->proto_slug = $pslug;
			$this->proto_implement = $pimplement;
		}

		public function setManualDate() {
			$this->is_manual_date = true;
		}

		public function unsetManualDate() {
			$this->is_manual_date = false;
		}

		public function phpPreLoad() {
			$this->mutasi_keluar_unit_table->addModal("id", "hidden", "", "");
			$unit_service_consumer = new UnitMutasiServiceConsumer($this->db, $this->page);
			$unit_service_consumer->execute();
			$unit_option = $unit_service_consumer->getContent();
			if ($this->is_manual_date)
				$this->mutasi_keluar_unit_table->addModal("waktu", "datetime", "Tanggal", date("Y-m-d H:i"), "n", null, false);
			else
				$this->mutasi_keluar_unit_table->addModal("waktu", "hidden", "", date("Y-m-d H:i"));
			$this->mutasi_keluar_unit_table->addModal("unit", "select", "Unit Tujuan", $unit_option, "n", null, false);
			$this->mutasi_keluar_unit_table->addModal("id_obat", "hidden", "", "");
			$this->mutasi_keluar_unit_table->addModal("kode_obat", "text", "Kode Obat", "", "y", null, true);
			$this->mutasi_keluar_unit_table->addModal("nama_obat", "chooser-mutasi_keluar_unit-obat-Obat", "Nama Obat", "", "n", null, true);
			$this->mutasi_keluar_unit_table->addModal("nama_jenis_obat", "text", "Jenis Obat", "", "n", null, true);
			$this->mutasi_keluar_unit_table->addModal("satuan", "select", "Satuan", "", "n", null);
			$this->mutasi_keluar_unit_table->addModal("sisa", "text", "Sisa", "", "n", null, true);
			$this->mutasi_keluar_unit_table->addModal("jumlah", "text", "Jumlah", "", "n", "numeric");
			$this->mutasi_keluar_unit_table->addModal("konversi", "hidden", "", "");
			$this->mutasi_keluar_unit_table->addModal("satuan_konversi", "hidden", "", "");
			$this->mutasi_keluar_unit_modal = $this->mutasi_keluar_unit_table->getModal();
			$this->mutasi_keluar_unit_modal->setTitle("Mutasi Keluar Unit");

			echo $this->mutasi_keluar_unit_modal->getHtml();
			echo $this->mutasi_keluar_unit_table->getHtml();
			echo addJS("framework/smis/js/table_action.js");
			echo addJS("framework/bootstrap/js/bootstrap-datepicker.js");
			echo addCSS("framework/bootstrap/css/datepicker.css");
		}

		public function superCommand($super_command) {
			$obat_table = new Table(
				array("Kode", "Obat", "Jenis", "Sisa", "Satuan"),
				"",
				null,
				true
			);
			$obat_table->setName("obat");
			$obat_table->setModel(Table::$SELECT);
			$obat_adapter = new SimpleAdapter();
			$obat_adapter->add("Kode", "kode_obat");
			$obat_adapter->add("Obat", "nama_obat");
			$obat_adapter->add("Jenis", "nama_jenis_obat");
			$obat_adapter->add("Sisa", "sisa", "number");
			$obat_adapter->add("Satuan", "satuan");
			$obat_dbtable = new DBTable($this->db, $this->tbl_stok_obat);
			$obat_dbtable->setViewForSelect(true);
			$filter = "";
			if (isset($_POST['kriteria']))
				$filter = " AND (b.kode_obat LIKE '%" . $_POST['kriteria'] . "%' OR b.nama_obat LIKE '%" . $_POST['kriteria'] . "%' OR b.nama_jenis_obat LIKE '%" . $_POST['kriteria'] . "%' OR b.satuan LIKE '%" . $_POST['kriteria'] . "%') ";
			$query_value = "
				SELECT *
				FROM (
					SELECT b.id_obat AS 'id', b.kode_obat, b.nama_obat, b.nama_jenis_obat, SUM(b.sisa) AS 'sisa', b.satuan, b.konversi, b.satuan_konversi
					FROM " . $this->tbl_obat_masuk . " a LEFT JOIN " . $this->tbl_stok_obat . " b ON a.id = b.id_obat_masuk
					WHERE a.prop NOT LIKE 'del' AND a.status = 'sudah' AND b.prop NOT LIKE 'del' AND b.konversi = 1 " . $filter . "
					GROUP BY b.id_obat, b.satuan, b.konversi, b.satuan_konversi
				) v
			";
			$query_count = "
				SELECT COUNT(*)
				FROm (
					" . $query_value . "
				) v
			";
			$obat_dbtable->setPreferredQuery(true, $query_value, $query_count);
			$obat_dbresponder = new ObatDBResponder(
				$obat_dbtable,
				$obat_table,
				$obat_adapter,
				$this->tbl_stok_obat,
				$this->tbl_obat_masuk
			);

			$sisa_table = new Table(
				array("id_obat", "sisa", "satuan", "konversi", "satuan_konversi")
			);
			$sisa_table->setName("sisa");
			$sisa_adapter = new SimpleAdapter();
			$sisa_adapter->add("id_obat", "id_obat");
			$sisa_adapter->add("sisa", "sisa");
			$sisa_adapter->add("satuan", "satuan");
			$sisa_adapter->add("konversi", "konversi");
			$sisa_adapter->add("satuan_konversi", "satuan_konversi");
			$columns = array("id_obat", "sisa", "satuan", "konversi", "satuan_konversi");
			$sisa_dbtable = new DBTable($this->db, $this->tbl_stok_obat, $columns);
			$sisa_dbresponder = new SisaDBResponder(
				$sisa_dbtable,
				$sisa_table,
				$sisa_adapter,
				$this->tbl_stok_obat,
				$this->tbl_obat_masuk
			);

			$super_command = new SuperCommand();
			$super_command->addResponder("obat", $obat_dbresponder);
			$super_command->addResponder("sisa", $sisa_dbresponder);
			$init = $super_command->initialize();
			if ($init != null) {
				echo $init;
				return;
			}
		}

		public function command($command) {
			$mutasi_keluar_unit_adapter = new SimpleAdapter(true, "No.");
			$mutasi_keluar_unit_adapter->add("No. Mutasi", "id", "digit8");
			$mutasi_keluar_unit_adapter->add("Tanggal", "waktu", "date d-m-Y");
			$mutasi_keluar_unit_adapter->add("Unit Tujuan", "unit", "unslug");
			$mutasi_keluar_unit_adapter->add("Kode Obat", "kode_obat");
			$mutasi_keluar_unit_adapter->add("Nama Obat", "nama_obat");
			$mutasi_keluar_unit_adapter->add("Jumlah", "jumlah", "number");
			$mutasi_keluar_unit_adapter->add("Satuan", "satuan");
			$mutasi_keluar_unit_adapter->add("Status", "status", "unslug");
			$mutasi_keluar_unit_dbtable = new DBTable($this->db, $this->tbl_mutasi_obat_keluar);
			$mutasi_keluar_unit_dbtable->setOrder(" id DESC ");
			$mutasi_keluar_unit_dbresponder = new MutasiUnitKeluarDBResponder(
				$mutasi_keluar_unit_dbtable,
				$this->mutasi_keluar_unit_table,
				$mutasi_keluar_unit_adapter,
				$this->tbl_obat_masuk,
				$this->tbl_stok_obat,
				$this->tbl_riwayat_stok_obat,
				$this->tbl_stok_mutasi_obat_keluar
			);
			if ($mutasi_keluar_unit_dbresponder->isSave()) {
				if ($_POST['id'] == "" || $_POST['id'] == 0) {
					$mutasi_keluar_unit_dbresponder->addColumnFixValue("status", "belum");
					if ($this->is_manual_date)
						$mutasi_keluar_unit_dbresponder->addColumnFixValue("waktu", $_POST['waktu']);
					else
						$mutasi_keluar_unit_dbresponder->addColumnFixValue("waktu", date("Y-m-d H:i"));
				}
			}
			$data = $mutasi_keluar_unit_dbresponder->command($_POST['command']);

			if (isset($_POST['push_command'])) {
				$mutasi_keluar_unit_row = $mutasi_keluar_unit_dbtable->get_row("
					SELECT *
					FROM " . $this->tbl_mutasi_obat_keluar . "
					WHERE id = '" . $data['content']['id'] . "'
				");
				$detail = $mutasi_keluar_unit_dbtable->get_result("
					SELECT a.id AS 'id_mutasi_keluar', b.id_obat, b.kode_obat, b.nama_obat, b.nama_jenis_obat, b.formularium, b.berlogo, b.generik, b.label, b.id_vendor, b.nama_vendor, a.jumlah, b.satuan, b.konversi, b.satuan_konversi, b.hna, b.produsen, b.tanggal_exp, b.no_batch, a.prop
					FROM " . $this->tbl_stok_mutasi_obat_keluar . " a LEFT JOIN " . $this->tbl_stok_obat . " b ON a.id_stok_obat = b.id
					WHERE a.id_mutasi_keluar = '" . $data['content']['id'] . "'
				");
				$command = "push_" . $_POST['push_command'];
				$push_mutasi_obat_unit_service_consumer = new PushMutasiObatAntarUnit(
					$this->db,
					$this->page,
					$mutasi_keluar_unit_row->id,
					$mutasi_keluar_unit_row->waktu,
					$mutasi_keluar_unit_row->unit,
					$mutasi_keluar_unit_row->status,
					$detail,
					$command
				);
				$push_mutasi_obat_unit_service_consumer->execute();
			}
			echo json_encode($data);
			return;
		}
		public function jsLoader() {
			echo addJS("framework/bootstrap/js/bootstrap-datetimepicker.js");
		}

		public function cssLoader() {
			echo addCSS("framework/bootstrap/css/bootstrap-datetimepicker.css");
		}

		public function jsPreLoad() {
			?>
			<script type="text/javascript">
				function MutasiAntarUnitKeluarObatAction(name, page, action, column) {
					this.initialize(name, page, action, column);
				}
				MutasiAntarUnitKeluarObatAction.prototype.constructor = MutasiAntarUnitKeluarObatAction;
				MutasiAntarUnitKeluarObatAction.prototype = new TableAction();
				MutasiAntarUnitKeluarObatAction.prototype.selected = function(json) {
					$("#mutasi_keluar_unit_id_obat").val(json.header.id_obat);
					$("#mutasi_keluar_unit_kode_obat").val(json.header.kode_obat);
					$("#mutasi_keluar_unit_nama_obat").val(json.header.nama_obat);
					$("#mutasi_keluar_unit_nama_jenis_obat").val(json.header.nama_jenis_obat);
					$("#mutasi_keluar_unit_satuan").html(json.satuan_option);
					this.setDetailInfo();
				};
				MutasiAntarUnitKeluarObatAction.prototype.setDetailInfo = function() {
					var part = $("#mutasi_keluar_unit_satuan").val().split("_");
					$("#mutasi_keluar_unit_konversi").val(part[0]);
					$("#mutasi_keluar_unit_satuan_konversi").val(part[1]);
					var data = this.getRegulerData();
					data['super_command'] = "sisa";
					data['command'] = "edit";
					data['id_obat'] = $("#mutasi_keluar_unit_id_obat").val();
					data['satuan'] = $("#mutasi_keluar_unit_satuan").find(":selected").text();
					data['konversi'] = $("#mutasi_keluar_unit_konversi").val();
					data['satuan_konversi'] = $("#mutasi_keluar_unit_satuan_konversi").val();
					console.log(data);
					$.post(
						"",
						data,
						function(response) {
							var json = getContent(response);
							if (json == null) return;
							$("#mutasi_keluar_unit_sisa").val(json.sisa);
							$("#mutasi_keluar_unit_jumlah").focus();
						}
					);
				};

				function MutasiAntarUnitKeluarAction(name, page, action, column) {
					this.initialize(name, page, action, column);
				}
				MutasiAntarUnitKeluarAction.prototype.constructor = MutasiAntarUnitKeluarAction;
				MutasiAntarUnitKeluarAction.prototype = new TableAction();
				MutasiAntarUnitKeluarAction.prototype.getSaveData = function() {
					var data = TableAction.prototype.getSaveData.call(this);
					data['satuan'] = $("#mutasi_keluar_unit_satuan option:selected").text();
					data['waktu'] = $("#mutasi_keluar_unit_waktu").val();
					data['push_command'] = "save";
					return data;
				};
				MutasiAntarUnitKeluarAction.prototype.cekSave = function() {
					var is_allow = TableAction.prototype.cekSave.call(this);
					if (is_allow == true) {
						var jumlah = $("#mutasi_keluar_unit_jumlah").val();
						var sisa = $("#mutasi_keluar_unit_sisa").val();
						if (parseFloat(jumlah) > parseFloat(sisa)) {
							$("#mutasi_keluar_unit_jumlah").addClass("error_field");
							$("#modal_alert_mutasi_keluar_unit_add_form").html(
								"<div class='alert alert-block alert-info'>" + 
									"<h4>Pemberitahuan</h4><strong>Jumlah (" + jumlah + ")</strong> tidak boleh melebihi <strong>Sisa (" + sisa + ")</strong>" + 
								"</div>"
							);
							$("#mutasi_keluar_unit_jumlah").focus();
							is_allow = false;
						}
					}
					return is_allow;
				};

				var MUTASI_UNIT_KELUAR_PNAME = "<?php echo $this->proto_name; ?>";
				var MUTASI_UNIT_KELUAR_PSLUG = "<?php echo $this->proto_slug; ?>";
				var MUTASI_UNIT_KELUAR_PIMPL = "<?php echo $this->proto_implement; ?>";
				var MUTASI_UNIT_KELUAR_ENTITY = "<?php echo $this->page; ?>";
				var mutasi_keluar_unit;
				var obat;
				$(document).ready(function() {
					$('.mydatetime').datetimepicker({ 
						minuteStep: 1
					});
					obat = new MutasiAntarUnitKeluarObatAction(
						"obat",
						MUTASI_UNIT_KELUAR_ENTITY,
						"mutasi_keluar_unit",
						new Array()
					);
					obat.setSuperCommand("obat");
					obat.setPrototipe(
						MUTASI_UNIT_KELUAR_PNAME,
						MUTASI_UNIT_KELUAR_PSLUG,
						MUTASI_UNIT_KELUAR_PIMPL
					);
					mutasi_keluar_unit = new MutasiAntarUnitKeluarAction(
						"mutasi_keluar_unit",
						MUTASI_UNIT_KELUAR_ENTITY,
						"mutasi_keluar_unit",
						new Array("id", "waktu", "unit", "id_obat", "kode_obat", "nama_obat", "nama_jenis_obat", "jumlah", "satuan", "konversi", "satuan_konversi", "status")
					);
					mutasi_keluar_unit.setPrototipe(
						MUTASI_UNIT_KELUAR_PNAME,
						MUTASI_UNIT_KELUAR_PSLUG,
						MUTASI_UNIT_KELUAR_PIMPL
					);
					mutasi_keluar_unit.view();
				});
			</script>
			<?php
		}
	}
?>