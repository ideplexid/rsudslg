<?php
	require_once("smis-framework/smis/template/ModulTemplate.php");
	require_once("smis-base/smis-include-service-consumer.php");
	require_once("smis-libs-inventory/table/MutasiUnitMasukTable.php");
	require_once("smis-libs-inventory/responder/MutasiUnitMasukDBResponder.php");
	require_once("smis-libs-inventory/service_consumer/SetMutasiObatAntarUnitStatusServiceConsumer.php");

	class MutasiMasukUnit extends ModulTemplate {
		private $db;
		private $name;
		private $tbl_obat_masuk;
		private $tbl_stok_obat;
		private $tbl_kartu_stok_obat;
		private $tbl_riwayat_stok_obat;
		private $mutasi_masuk_unit_table;
		private $proto_name;
		private $proto_implement;
		private $proto_slug;

		public function __construct($db, $name_entity, $tbl_obat_masuk, $tbl_stok_obat, $tbl_kartu_stok_obat, $tbl_riwayat_stok_obat, $page) {
			$this->db = $db;
			$this->name = $name_entity;
			$this->tbl_obat_masuk = $tbl_obat_masuk;
			$this->tbl_stok_obat = $tbl_stok_obat;
			$this->tbl_kartu_stok_obat = $tbl_kartu_stok_obat;
			$this->tbl_riwayat_stok_obat = $tbl_riwayat_stok_obat;
			$this->page = $page;

			$header = array(
				"No. Mutasi", "Tanggal", "Unit Asal", "Kode Obat", "Nama Obat", "Jumlah", "Satuan", "Status"
			);
			$this->mutasi_masuk_unit_table = new MutasiUnitMasukTable(
				$header,
				$this->name . " : Mutasi Masuk Unit",
				null,
				true
			);
			$this->mutasi_masuk_unit_table->setName("mutasi_masuk_unit");
			$this->mutasi_masuk_unit_table->setPrintButtonEnable(false);
			$this->mutasi_masuk_unit_table->setAddButtonEnable(false);
		}

		public function setPrototype($pname, $pslug, $pimplement) {
			$this->proto_name = $pname;
			$this->proto_slug = $pslug;
			$this->proto_implement = $pimplement;
		}

		public function phpPreLoad() {
			echo $this->mutasi_masuk_unit_table->getHtml();
			echo addJS("framework/smis/js/table_action.js");
		}

		public function superCommand($super_command) {
			
		}

		public function command($command) {
			$mutasi_masuk_unit_adapter = new SimpleAdapter(true, "No.");
			$mutasi_masuk_unit_adapter->add("No. Mutasi", "id", "digit8");
			$mutasi_masuk_unit_adapter->add("Tanggal", "tanggal", "date d-m-Y");
			$mutasi_masuk_unit_adapter->add("Unit Asal", "unit", "unslug");
			$mutasi_masuk_unit_adapter->add("Kode Obat", "kode_obat");
			$mutasi_masuk_unit_adapter->add("Nama Obat", "nama_obat");
			$mutasi_masuk_unit_adapter->add("Jumlah", "jumlah", "number");
			$mutasi_masuk_unit_adapter->add("Satuan", "satuan");
			$mutasi_masuk_unit_adapter->add("Status", "status", "unslug");
			$mutasi_masuk_unit_adapter->add("status", "status");
			$mutasi_masuk_unit_dbtable = new DBTable($this->db, $this->tbl_obat_masuk);
			$filter = "";
			if (isset($_POST['kriteria'])) {
				$filter = " AND (a.tanggal LIKE '%" . $_POST['kriteria'] . "%' OR a.unit LIKE '%" . $_POST['kriteria'] . "%' OR b.id_obat LIKE '%" . $_POST['kriteria'] . "%' OR b.kode_obat LIKE '%" . $_POST['kriteria'] . "%' OR b.nama_obat LIKE '%" . $_POST['kriteria'] . "%') ";
			}
			$query_value = "
				SELECT a.id, a.f_id, a.tanggal, a.unit, b.id_obat, b.kode_obat, b.nama_obat, SUM(b.jumlah) AS 'jumlah', b.satuan, a.status
				FROM " . $this->tbl_obat_masuk . " a LEFT JOIN " . $this->tbl_stok_obat . " b ON a.id = b.id_obat_masuk
				WHERE a.unit NOT LIKE 'gudang_farmasi' AND a.id > 0 AND a.prop NOT LIKE 'del' AND b.prop NOT LIKE 'del' " . $filter . "
				GROUP BY a.id, b.id_obat, b.satuan, b.konversi, b.satuan_konversi
				ORDER BY a.id DESC
			";
			$query_count = "
				SELECT COUNT(*)
				FROM (
					" . $query_value . "
				) v
			";
			$mutasi_masuk_unit_dbtable->setPreferredQuery(true, $query_value, $query_count);
			$mutasi_masuk_unit_dbresponder = new MutasiUnitMasukDBResponder(
				$mutasi_masuk_unit_dbtable,
				$this->mutasi_masuk_unit_table,
				$mutasi_masuk_unit_adapter,
				$this->tbl_obat_masuk,
				$this->tbl_stok_obat,
				$this->tbl_riwayat_stok_obat
			);
			$data = $mutasi_masuk_unit_dbresponder->command($_POST['command']);

			if (isset($_POST['push_command'])) {
				$id['id'] = $data['content']['id'];
				$mutasi_masuk_row = $mutasi_masuk_unit_dbresponder->select($id);
				$push_command = "push_" . $_POST['push_command'];
				global $user;
				$nama_user = $user->getName();
				$set_status_service_consumer = new SetMutasiObatAntarUnitStatusServiceConsumer($this->db, $nama_user, $mutasi_masuk_row->f_id, $mutasi_masuk_row->status, $mutasi_masuk_row->unit, $push_command);
				$set_status_service_consumer->execute();
			}
			echo json_encode($data);
			return;
		}

		public function jsPreLoad() {
			?>
			<script type="text/javascript">
				function MutasiAntarUnitMasukAction(name, page, action, column) {
					this.initialize(name, page, action, column);
				}
				MutasiAntarUnitMasukAction.prototype.constructor = MutasiAntarUnitMasukAction;
				MutasiAntarUnitMasukAction.prototype = new TableAction();
				MutasiAntarUnitMasukAction.prototype.accept = function(id) {
					showLoading();
					var self = this;
					var data = this.getSaveData();
					data['id'] = id;
					data['status'] = "sudah";
					data['push_command'] = "update";
					$.post(
						"",
						data,
						function(response) {
							dismissLoading();
							var json = getContent(response);
							if (json == null) return;
							self.view();
						}
					);
				};
				MutasiAntarUnitMasukAction.prototype.return = function(id) {
					showLoading();
					var self = this;
					var data = this.getSaveData();
					data['id'] = id;
					data['status'] = "dikembalikan";
					data['push_command'] = "update";
					$.post(
						"",
						data,
						function(response) {
							dismissLoading();
							var json = getContent(response);
							if (json == null) return;
							self.view();
						}
					);
				};

				var MUTASI_UNIT_MASUK_PNAME = "<?php echo $this->proto_name; ?>";
				var MUTASI_UNIT_MASUK_PSLUG = "<?php echo $this->proto_slug; ?>";
				var MUTASI_UNIT_MASUK_PIMPL = "<?php echo $this->proto_implement; ?>";
				var MUTASI_UNIT_MASUK_ENTITY = "<?php echo $this->page; ?>";
				var mutasi_masuk_unit;

				$(document).ready(function() {
					mutasi_masuk_unit = new MutasiAntarUnitMasukAction(
						"mutasi_masuk_unit",
						MUTASI_UNIT_MASUK_ENTITY,
						"mutasi_masuk_unit",
						new Array()
					);
					mutasi_masuk_unit.setPrototipe(MUTASI_UNIT_MASUK_PNAME, MUTASI_UNIT_MASUK_PSLUG,MUTASI_UNIT_MASUK_PIMPL);
					mutasi_masuk_unit.view();
				});
			</script>
			<?php
		}
	}
?>