<?php 
	require_once 'smis-framework/smis/template/ModulTemplate.php';

	class BOI extends ModulTemplate {
		private $db;
		private $name;
		private $entity;
		private $dbslug;
		private $proto_name;
		private $proto_implement;
		private $proto_slug;
		
		private $dinventaris;
		
		private $barang_masuk;
		private $dpermintaan_barang;
		private $penggunaan_barang;
		private $penyesuaian_barang;
		private $permintaan_barang;
		private $retur_barang;
		private $stok_barang;
		private $riwayat_stok_barang;
		private $kartu_stok_barang;
		
		private $obat_masuk;
		private $dpermintaan_obat;
		private $penggunaan_obat;
		private $penyesuaian_obat;
		private $permintaan_obat;
		private $retur_obat;
		private $stok_obat;
		private $riwayat_stok_obat;
		private $kartu_stok_obat;
		private $mutasi_keluar;
		private $stok_mutasi_keluar;
		
		private $is_use_obat;
		private $is_use_barang;
		private $is_use_inventaris;
		
		private $obat_property;
		private $barang_property;

		private $gudang_asal;
		
		public function __construct($db, $name, $entity, $dbslug){
			$this->db=$db;
			$this->name=$name;
			$this->entity=$entity;
			$this->dbslug=$dbslug;
			$this->proto_name="";
			$this->proto_slug="";
			$this->proto_implement="";
			$this->is_use_obat=true;
			$this->is_use_barang=true;
			$this->is_use_inventaris=true;
			$this->obat_property=array();
			$this->barang_property=array();
			
			$this->dinventaris="smis_".$dbslug."_inventaris";
			$this->barang_masuk="smis_".$dbslug."_barang_masuk";
			$this->dpermintaan_barang="smis_".$dbslug."_dpermintaan_barang";
			$this->penggunaan_barang="smis_".$dbslug."_penggunaan_barang";
			$this->penyesuaian_barang="smis_".$dbslug."_penyesuaian_stok_barang";
			$this->permintaan_barang="smis_".$dbslug."_permintaan_barang";
			$this->retur_barang="smis_".$dbslug."_retur_barang";
			$this->stok_barang="smis_".$dbslug."_stok_barang";
			$this->riwayat_stok_barang="smis_".$dbslug."_riwayat_stok_barang";
			$this->kartu_stok_barang="smis_".$dbslug."_kartu_stok_barang";
			
			$this->obat_masuk="smis_".$dbslug."_obat_masuk";
			$this->dpermintaan_obat="smis_".$dbslug."_dpermintaan_obat";
			$this->penggunaan_obat="smis_".$dbslug."_penggunaan_obat";
			$this->penyesuaian_obat="smis_".$dbslug."_penyesuaian_stok_obat";
			$this->permintaan_obat="smis_".$dbslug."_permintaan_obat";
			$this->retur_obat="smis_".$dbslug."_retur_obat";
			$this->stok_obat="smis_".$dbslug."_stok_obat";
			$this->riwayat_stok_obat="smis_".$dbslug."_riwayat_stok_obat";
			$this->kartu_stok_obat="smis_".$dbslug."_kartu_stok_obat";
			$this->mutasi_keluar="smis_".$dbslug."_mutasi_keluar";
			$this->stok_mutasi_keluar="smis_".$dbslug."_stok_mutasi_keluar";	

			$this->gudang_asal = "gudang_farmasi";
		}

		public function setGudangAsal($gudang_asal) {
			$this->gudang_asal = $gudang_asal;
 		}
		
		public function setPrototype($pname,$pslug,$pimplement){
			$this->proto_name=$pname;
			$this->proto_implement=$pimplement;
			$this->proto_slug=$pslug;
			
			$this->dinventaris="smis_".$this->dbslug."_inventaris_".$pslug;
			$this->barang_masuk="smis_".$this->dbslug."_barang_masuk_".$pslug;
			$this->dpermintaan_barang="smis_".$this->dbslug."_dpermintaan_barang_".$pslug;
			$this->penggunaan_barang="smis_".$this->dbslug."_penggunaan_barang_".$pslug;
			$this->penyesuaian_barang="smis_".$this->dbslug."_penyesuaian_stok_barang_".$pslug;
			$this->permintaan_barang="smis_".$this->dbslug."_permintaan_barang_".$pslug;
			$this->retur_barang="smis_".$this->dbslug."_retur_barang_".$pslug;
			$this->stok_barang="smis_".$this->dbslug."_stok_barang_".$pslug;
			$this->riwayat_stok_barang="smis_".$this->dbslug."_riwayat_stok_barang_".$pslug;
			$this->kartu_stok_barang="smis_".$this->dbslug."_kartu_stok_barang_".$pslug;
			
			$this->obat_masuk="smis_".$this->dbslug."_obat_masuk_".$pslug;
			$this->dpermintaan_obat="smis_".$this->dbslug."_dpermintaan_obat_".$pslug;
			$this->penggunaan_obat="smis_".$this->dbslug."_penggunaan_obat_".$pslug;
			$this->penyesuaian_obat="smis_".$this->dbslug."_penyesuaian_stok_obat_".$pslug;
			$this->permintaan_obat="smis_".$this->dbslug."_permintaan_obat_".$pslug;
			$this->retur_obat="smis_".$this->dbslug."_retur_obat_".$pslug;
			$this->stok_obat="smis_".$this->dbslug."_stok_obat_".$pslug;
			$this->riwayat_stok_obat="smis_".$this->dbslug."_riwayat_stok_obat_".$pslug;
			$this->kartu_stok_obat="smis_".$this->dbslug."_kartu_stok_obat_".$pslug;
			$this->mutasi_keluar="smis_".$this->dbslug."_mutasi_keluar".$pslug;
			$this->stok_mutasi_keluar="smis_".$this->dbslug."_stok_mutasi_keluar".$pslug;
		}
		
		public function setUseBOI($barang,$obat,$inventaris){
			$this->is_use_barang=$barang;
			$this->is_use_obat=$obat;
			$this->is_use_inventaris=$inventaris;
		}
		
		public function setObatProperty($slug,$val){
			$this->obat_property[$slug]=$val;
		}
		
		public function setBarangProperty($slug,$val){
			$this->barang_property[$slug]=$val;
		}
		
		public function initialize(){
			$action=$_POST['action'];
			if($action=="boi"){
				$this->phpPreLoad();
				return;
			}
			
			switch ($action){
				case "daftar_inventaris" : $this->daftarInventaris(); break;
				case "barang_masuk" : $this->barangMasuk(); break;
				case "daftar_barang" : $this->daftarBarang(); break;
				case "riwayat_stok_barang" : $this->riwayatStokBarang(); break;
				case "kartu_stok_barang" : $this->kartuStokBarang(); break;
				case "permintaan_barang" : $this->permintaanBarang(); break;
				case "penggunaan_barang" : $this->penggunaanBarang(); break;
				case "retur_barang" : $this->returBarang(); break;
				case "pengecekan_stok_barang_ed" : $this->cekEdBarang(); break;
				case "detail_stok_barang" : $this->detailBarang(); break;
				case "riwayat_penyesuaian_stok_barang" : $this->riwayatBarang(); break;
				case "laporan_so_barang" : $this->laporanSOBarang(); break;
				case "obat_masuk" : $this->obatMasuk(); break;
				case "daftar_obat" : $this->daftarObat(); break;
				case "riwayat_stok_obat" : $this->riwayatStokObat(); break;
				case "kartu_stok_obat" : $this->kartuStokObat(); break;
				case "permintaan_obat" : $this->permintaanObat(); break;
				case "penggunaan_obat" : $this->penggunaanObat(); break;
				case "retur_obat" : $this->returObat(); break;
				case "pengecekan_stok_obat_ed" : $this->cekEdObat(); break;
				case "detail_stok_obat" : $this->detailObat(); break;
				case "riwayat_penyesuaian_stok_obat" : $this->riwayatObat(); break;
				case "laporan_so_obat" : $this->laporanSOObat(); break;
			}
			
		}
		
		/**
		 * Visitor Pattern 
		 * 
		 */
		public function phpPreLoad(){		
			$boi=new Tabulator("boi", "");

			if($this->is_use_obat){
				$obat=new Tabulator("obat","",Tabulator::$LANDSCAPE);
				if(!isset($this->obat_property['daftar_obat']) || $this->obat_property['daftar_obat']){
					ob_start(); 
					$this->daftarObat(); 
					$daftar_obat=ob_get_clean();
					$obat->add("daftar_obat", "Rekapitulasi", $daftar_obat,Tabulator::$TYPE_HTML, "fa fa-book");
				}
				if(!isset($this->obat_property['riwayat_stok_obat']) || $this->barang_property['riwayat_stok_obat']){
					ob_start(); 
					$this->riwayatStokObat(); 
					$riwayat_stok_obat = ob_get_clean();
					$obat->add("riwayat_stok_obat", "Riwayat Stok", $riwayat_stok_obat,Tabulator::$TYPE_HTML, "fa fa-bookmark");
				}
				if(!isset($this->obat_property['kartu_stok_obat']) || $this->barang_property['kartu_stok_obat']){
					ob_start(); 
					$this->kartuStokObat(); 
					$kartu_stok_obat = ob_get_clean();
					$obat->add("kartu_stok_obat", "Kartu Stok", $kartu_stok_obat,Tabulator::$TYPE_HTML, "fa fa-bookmark");
				}
				if(!isset($this->obat_property['permintaan_obat']) || $this->obat_property['permintaan_obat']){
					ob_start(); 
					$this->permintaanObat(); 
					$permintaan_obat=ob_get_clean();
					$obat->add("permintaan_obat", "Permintaan", $permintaan_obat,Tabulator::$TYPE_HTML, "fa fa-mail-forward");
				}
				if(!isset($this->obat_property['obat_masuk']) || $this->obat_property['obat_masuk']){
					ob_start(); 
					$this->obatMasuk(); 
					$obat_masuk = ob_get_clean();
					$obat->add("obat_masuk", "Pemasukan", $obat_masuk,Tabulator::$TYPE_HTML,"fa fa-reply");
				}
				if(!isset($this->obat_property['penggunaan_obat']) || $this->obat_property['penggunaan_obat']){
					ob_start(); 
					$this->penggunaanObat(); 
					$penggunaan_obat = ob_get_clean();
					$obat->add("penggunaan_obat", "Penggunaan", $penggunaan_obat,Tabulator::$TYPE_HTML, "fa fa-spinner");
				}
				if(!isset($this->obat_property['pengecekan_stok_obat_ed']) || $this->obat_property['pengecekan_stok_obat_ed']){
					ob_start(); 
					$this->cekEdObat(); 
					$pengecekan_stok_obat_ed = ob_get_clean();
					$obat->add("pengecekan_stok_obat_ed", "Pengecekan ED", $pengecekan_stok_obat_ed, Tabulator::$TYPE_HTML, "fa fa-retweet");
				}
				if(!isset($this->obat_property['retur_obat']) || $this->obat_property['retur_obat']){
					ob_start(); 
					$this->returObat(); 
					$retur_obat = ob_get_clean();
					$obat->add("retur_obat", "Retur", $retur_obat,Tabulator::$TYPE_HTML, "fa fa-recycle");
				}
				if (strpos($this->entity, "depo_farmasi") === false) {
					if(!isset($this->obat_property['detail_stok_obat']) || $this->obat_property['detail_stok_obat']){
						ob_start(); 
						$this->detailObat(); 
						$detail_stok_obat=ob_get_clean();
						$obat->add("detail_stok_obat", "Penyesuaian", $detail_stok_obat, Tabulator::$TYPE_HTML, "fa fa-adjust");
					}
					if(!isset($this->obat_property['riwayat_penyesuaian_stok_obat']) || $this->obat_property['riwayat_penyesuaian_stok_obat']){
						ob_start(); 
						$this->riwayatObat(); 
						$riwayat_penyesuaian_stok_obat = ob_get_clean();
						$obat->add("riwayat_penyesuaian_stok_obat", "Riwayat Penyesuaian", $riwayat_penyesuaian_stok_obat,Tabulator::$TYPE_HTML, "fa fa-history");
					}
				}
				if(!isset($this->obat_property['laporan_so_obat']) || $this->obat_property['laporan_so_obat']){
					ob_start(); 
					$this->laporanSOObat();
					$laporan_so_obat = ob_get_clean();
					$obat->add("laporan_so_obat", "Laporan SO", $laporan_so_obat, Tabulator::$TYPE_HTML, "fa fa-file");	
				}
				if($obat->getTotalElement()>0)
					$boi->add("obat_tabs", "Obat", $obat, Tabulator::$TYPE_COMPONENT,"fa fa-eyedropper");
			}
			
			if($this->is_use_barang){
				$barang=new Tabulator("barang", "",Tabulator::$LANDSCAPE);			
				if(!isset($this->barang_property['daftar_barang']) || $this->barang_property['daftar_barang']){
					ob_start(); $this->daftarBarang(); $daftar_barang=ob_get_clean();
					$barang->add("daftar_barang", "Rekapitulasi", $daftar_barang,Tabulator::$TYPE_HTML,"fa fa-list");
				}
				if(!isset($this->barang_property['riwayat_stok_barang']) || $this->barang_property['riwayat_stok_barang']){
					ob_start(); $this->riwayatStokBarang(); $riwayat_stok_barang=ob_get_clean();
					$barang->add("riwayat_stok_barang", "Riwayat Stok", $riwayat_stok_barang,Tabulator::$TYPE_HTML,"fa fa-history");
				}
				if(!isset($this->barang_property['kartu_stok_barang']) || $this->barang_property['kartu_stok_barang']){
					ob_start(); $this->kartuStokBarang(); $kartu_stok_barang=ob_get_clean();
					$barang->add("kartu_stok_barang", "Kartu Stok", $kartu_stok_barang,Tabulator::$TYPE_HTML,"fa fa-history");
				}
				if(!isset($this->barang_property['permintaan_barang']) || $this->barang_property['permintaan_barang']){
					ob_start(); $this->permintaanBarang(); $permintaan_barang=ob_get_clean();
					$barang->add("permintaan_barang", "Permintaan", $permintaan_barang,Tabulator::$TYPE_HTML,"fa fa-mail-forward");				
				}
				if(!isset($this->barang_property['barang_masuk']) || $this->barang_property['barang_masuk']){
					ob_start(); $this->barangMasuk(); $barang_masuk=ob_get_clean();
					$barang->add("barang_masuk", "Pemasukan", $barang_masuk,Tabulator::$TYPE_HTML,"fa fa-reply");
				}
				if(!isset($this->barang_property['penggunaan_barang']) || $this->barang_property['penggunaan_barang']){
					ob_start(); $this->penggunaanBarang(); $penggunaan_barang=ob_get_clean();
					$barang->add("penggunaan_barang", "Penggunaan", $penggunaan_barang,Tabulator::$TYPE_HTML,"fa fa-spinner");
				}
				if(!isset($this->barang_property['pengecekan_stok_barang_ed']) || $this->barang_property['pengecekan_stok_barang_ed']){
					ob_start(); $this->cekEdBarang(); $pengecekan_stok_barang_ed=ob_get_clean();
					$barang->add("pengecekan_stok_barang_ed", "Pengecekan ED", $pengecekan_stok_barang_ed,Tabulator::$TYPE_HTML,"fa fa-retweet");
				}
				if(!isset($this->barang_property['retur_barang']) || $this->barang_property['retur_barang']){
					ob_start(); $this->returBarang(); $retur_barang=ob_get_clean();
					$barang->add("retur_barang", "Retur", $retur_barang,Tabulator::$TYPE_HTML,"fa fa-recycle");
				}
				if(!isset($this->barang_property['detail_stok_barang']) || $this->barang_property['detail_stok_barang']){
					ob_start(); $this->detailBarang(); $detail_stok_barang=ob_get_clean();
					$barang->add("detail_stok_barang", "Penyesuaian", $detail_stok_barang,Tabulator::$TYPE_HTML,"fa fa-adjust");
				}
				if(!isset($this->barang_property['riwayat_penyesuaian_stok_barang']) || $this->barang_property['riwayat_penyesuaian_stok_barang']){
					ob_start(); $this->riwayatBarang(); $riwayat_penyesuaian_stok_barang=ob_get_clean();
					$barang->add("riwayat_penyesuaian_stok_barang", "Riwayat Penyesuaian", $riwayat_penyesuaian_stok_barang,Tabulator::$TYPE_HTML,"fa fa-history");
				}
				if(!isset($this->barang_property['laporan_so_barang']) || $this->barang_property['laporan_so_barang']){
					ob_start(); 
					$this->laporanSOBarang(); 
					$laporan_so_barang = ob_get_clean();
					$barang->add("laporan_so_barang", "Laporan SO", $laporan_so_barang, Tabulator::$TYPE_HTML, "fa fa-file");
				}
				if($barang->getTotalElement()>0)
					$boi->add("barang_tabs", "Barang", $barang, Tabulator::$TYPE_COMPONENT,"fa fa-suitcase");
			}
			
			
			if($this->is_use_inventaris){
				ob_start(); $this->daftarInventaris(); $daftar_inventaris=ob_get_clean();
				$boi->add("inventaris_tabs", "Inventaris", $daftar_inventaris,Tabulator::$TYPE_HTML," fa fa-building");		
			}					
			echo $boi->getHtml();
		}
		
		
		public function daftarInventaris(){
			require_once 'smis-libs-inventory/modul/DaftarInventaris.php';
			$df=new DaftarInventaris($this->db, $this->name, $this->dinventaris, $this->entity);
			$df->setPrototype($this->proto_name, $this->proto_slug, $this->proto_implement);
			$df->initialize();
		}
		
		public function daftarBarang(){
			require_once 'smis-libs-inventory/modul/DaftarBarang.php';		
			$df=new DaftarBarang($this->db, $this->name, $this->barang_masuk,$this->stok_barang, $this->entity);
			$df->setPrototype($this->proto_name, $this->proto_slug, $this->proto_implement);
			$df->initialize();
		}
		
		public function riwayatStokBarang(){
			require_once 'smis-libs-inventory/modul/RiwayatStokBarang.php';		
			$df=new RiwayatStokBarang($this->db, $this->name, $this->riwayat_stok_barang, $this->stok_barang, $this->barang_masuk, $this->entity);
			$df->setPrototype($this->proto_name, $this->proto_slug, $this->proto_implement);
			$df->initialize();
		}

		public function kartuStokBarang(){
			require_once 'smis-libs-inventory/modul/KartuStokBarang.php';		
			$df=new KartuStokBarang($this->db, $this->name, $this->kartu_stok_barang, $this->stok_barang, $this->barang_masuk, $this->entity);
			$df->setPrototype($this->proto_name, $this->proto_slug, $this->proto_implement);
			$df->initialize();	
		}
		
		public function permintaanBarang(){
			require_once 'smis-libs-inventory/modul/PermintaanBarang.php';
			$df=new PermintaanBarang($this->db, $this->name, $this->permintaan_barang, $this->dpermintaan_barang,$this->entity);
			$df->setPrototype($this->proto_name, $this->proto_slug, $this->proto_implement);
			$df->initialize();
		}
		
		public function barangMasuk(){
			require_once 'smis-libs-inventory/modul/BarangMasuk.php';
			$df=new BarangMasuk($this->db, $this->name,  $this->barang_masuk, $this->stok_barang, $this->riwayat_stok_barang, $this->entity);
			$df->setPrototype($this->proto_name, $this->proto_slug, $this->proto_implement);
			$df->initialize();
		}
		
		public function penggunaanBarang(){
			require_once 'smis-libs-inventory/modul/PenggunaanBarang.php';
			$df=new PenggunaanBarang($this->db, $this->name, $this->penggunaan_barang, $this->stok_barang, $this->riwayat_stok_barang, $this->barang_masuk, $this->entity);
			$df->setPrototype($this->proto_name, $this->proto_slug, $this->proto_implement);
			$df->initialize();
		}
		
		public function returBarang(){
			require_once 'smis-libs-inventory/modul/ReturBarang.php';
			$df=new ReturBarang($this->db, $this->name, $this->retur_barang, $this->stok_barang, $this->riwayat_stok_barang, $this->barang_masuk, $this->entity);
			$df->setPrototype($this->proto_name, $this->proto_slug, $this->proto_implement);
			$df->initialize();
		}
		
		public function cekEdBarang(){
			require_once 'smis-libs-inventory/modul/StockBarangED.php';
			$df=new StockBarangED($this->db, $this->name, $this->stok_barang, $this->barang_masuk, $this->entity);
			$df->setPrototype($this->proto_name, $this->proto_slug, $this->proto_implement);
			$df->initialize();
		}
		
		public function detailBarang(){
			require_once 'smis-libs-inventory/modul/PenyesuaianStokBarang.php';
			$df=new PenyesuaianStokBarang($this->db, $this->name, $this->penyesuaian_barang, $this->stok_barang, $this->riwayat_stok_barang, $this->barang_masuk, $this->entity);
			$df->setPrototype($this->proto_name, $this->proto_slug, $this->proto_implement);
			$df->initialize();
		}
		
		public function riwayatBarang(){
			require_once 'smis-libs-inventory/modul/RiwayatPenyesuaianStokBarang.php';
			$df=new RiwayatPenyesuaianStokBarang($this->db, $this->name,$this->penyesuaian_barang, $this->stok_barang, $this->entity);
			$df->setPrototype($this->proto_name, $this->proto_slug, $this->proto_implement);
			$df->initialize();
		}
		
		public function laporanSOBarang() {
			require_once 'smis-libs-inventory/modul/LaporanSOBarang.php';
			$df = new LaporanSOBarang($this->db, $this->name, $this->barang_masuk, $this->stok_barang, $this->penggunaan_barang, $this->retur_barang, $this->penyesuaian_barang, $this->entity);
			$df->setPrototype($this->proto_name, $this->proto_slug, $this->proto_implement);
			$df->initialize();
		}

		//obat
		public function daftarObat(){
			require_once 'smis-libs-inventory/modul/DaftarObat.php';
			$df=new DaftarObat($this->db, $this->name, $this->obat_masuk,$this->stok_obat, $this->entity);
			$df->setPrototype($this->proto_name, $this->proto_slug, $this->proto_implement);
			$df->initialize();
		}
		
		public function riwayatStokObat(){
			require_once 'smis-libs-inventory/modul/RiwayatStokObat.php';		
			$df=new RiwayatStokObat($this->db, $this->name, $this->riwayat_stok_obat,$this->stok_obat, $this->obat_masuk, $this->entity);
			$df->setPrototype($this->proto_name, $this->proto_slug, $this->proto_implement);
			$df->initialize();
		}

		public function kartuStokObat(){
			require_once 'smis-libs-inventory/modul/KartuStokObat.php';		
			$df=new KartuStokObat($this->db, $this->name, $this->kartu_stok_obat, $this->stok_obat, $this->obat_masuk, $this->entity);
			$df->setPrototype($this->proto_name, $this->proto_slug, $this->proto_implement);
			$df->initialize();	
		}
		
		public function permintaanObat(){
			require_once 'smis-libs-inventory/modul/PermintaanObat.php';
			$df=new PermintaanObat($this->db, $this->name, $this->permintaan_obat, $this->dpermintaan_obat, $this->entity);
			$df->setPrototype($this->proto_name, $this->proto_slug, $this->proto_implement);
			$df->initialize();
		}
		
		public function obatMasuk(){
			require_once 'smis-libs-inventory/modul/ObatMasuk.php';
			$df=new ObatMasuk($this->db, $this->name, $this->obat_masuk, $this->stok_obat, $this->riwayat_stok_obat, $this->entity);
			$df->setPrototype($this->proto_name, $this->proto_slug, $this->proto_implement);
			$df->initialize();
		}
		
		public function penggunaanObat(){
			require_once 'smis-libs-inventory/modul/PenggunaanObat.php';
			$df=new PenggunaanObat($this->db, $this->name, $this->penggunaan_obat,$this->stok_obat, $this->riwayat_stok_obat, $this->obat_masuk, $this->entity);
			$df->setPrototype($this->proto_name, $this->proto_slug, $this->proto_implement);
			$df->initialize();
		}
		
		public function returObat(){
			require_once 'smis-libs-inventory/modul/ReturObat.php';
			$df=new ReturObat($this->db, $this->name,$this->retur_obat, $this->stok_obat, $this->riwayat_stok_obat, $this->obat_masuk, $this->entity, $this->gudang_asal);
			$df->setPrototype($this->proto_name, $this->proto_slug, $this->proto_implement);
			$df->initialize();
		}
		
		public function cekEdObat(){
			require_once 'smis-libs-inventory/modul/StockObatED.php';
			$df=new StockObatED($this->db, $this->name, $this->stok_obat, $this->obat_masuk, $this->entity);
			$df->setPrototype($this->proto_name, $this->proto_slug, $this->proto_implement);
			$df->initialize();
		}
		
		public function detailObat(){
			require_once 'smis-libs-inventory/modul/PenyesuaianStokObat.php';
			$df=new PenyesuaianStokObat($this->db, $this->name, $this->penyesuaian_obat, $this->stok_obat, $this->riwayat_stok_obat, $this->obat_masuk, $this->entity);
			$df->setPrototype($this->proto_name, $this->proto_slug, $this->proto_implement);
			$df->initialize();
		}
		
		public function riwayatObat(){
			require_once 'smis-libs-inventory/modul/RiwayatPenyesuaianStokObat.php';
			$df=new RiwayatPenyesuaianStokObat($this->db, $this->name, $this->penyesuaian_obat, $this->stok_obat, $this->entity);
			$df->setPrototype($this->proto_name, $this->proto_slug, $this->proto_implement);
			$df->initialize();
		}

		public function laporanSOObat() {
			require_once 'smis-libs-inventory/modul/LaporanSOObat.php';
			$df = new LaporanSOObat($this->db, $this->name, $this->obat_masuk, $this->stok_obat, $this->penggunaan_obat, $this->retur_obat, $this->penyesuaian_obat, $this->mutasi_keluar, $this->entity);
			$df->setPrototype($this->proto_name, $this->proto_slug, $this->proto_implement);
			$df->initialize();
		}
	}
?>