<?php 
	class DRKBUTable extends Table {
		public function getHeaderButton() {
			$btn_group = new ButtonGroup("noprint");
			$btn = new Button("", "", "Tambah");
			$btn->setAction($this->action . ".show_add_form()");
			$btn->setClass("btn-primary");
			$btn->setAtribute("id='rkbu_add'");
			$btn->setIcon("icon-plus icon-white");
			$btn->setIsButton(Button::$ICONIC);
			$btn_group->addElement($btn);
			return $btn_group->getHtml();
		}
		
		public function getJenisBarang(){
			$opsi_jenis_barang=new OptionBuilder();
			$opsi_jenis_barang->addSingle("Barang Umum");
			$opsi_jenis_barang->addSingle("Obat - Alat Kesehatan");
			$opsi_jenis_barang->addSingle("Inventaris");
			return $opsi_jenis_barang->getContent();
		}
		
		public function getBulan(){
			$bulan_option=new OptionBuilder();
			$bulan_option->add("01 - Januari","Januari");
			$bulan_option->add("02 - Februari","Februari");
			$bulan_option->add("03 - Maret","Maret");
			$bulan_option->add("04 - April","April");
			$bulan_option->add("05 - Mei","Mei");
			$bulan_option->add("06 - Juni","Juni");
			$bulan_option->add("07 - Juli","Juli");
			$bulan_option->add("08 - Agustus","Agustus");
			$bulan_option->add("09 - September","September");
			$bulan_option->add("10 - Oktober","Oktober");
			$bulan_option->add("11 - Nopember","Nopember");
			$bulan_option->add("12 - Desember","Desember");
			return $bulan_option->getContent();
		}
	}
?>