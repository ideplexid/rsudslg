<?php 
	class DPermintaanObatTable extends Table {
		public function getHeaderButton() {
			$btn_group = new ButtonGroup("noprint");
			$btn = new Button("", "", "Tambah");
			$btn->setAction($this->action . ".show_add_form()");
			$btn->setClass("btn-primary");
			$btn->setAtribute("id='dpermintaan_obat_add'");
			$btn->setIcon("icon-plus icon-white");
			$btn->setIsButton(Button::$ICONIC);
			$btn_group->addElement($btn);
			return $btn_group->getHtml();
		}
	}
?>