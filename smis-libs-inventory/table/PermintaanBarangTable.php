<?php
	class PermintaanBarangTable extends Table {
		public function getBodyContent(){
			$content = "";
			if ($this->content!=NULL) {
				foreach ($this->content as $d) {
					$content .= "<tr>";
					foreach ($this->header as $h) {
						$content .= "<td>" . $d[$h] . "</td>";
					}
					if ($this->is_action) {
						$content .= "<td>" . $this->getFilteredContentButton($d['id'], $d['status'])->getHtml() . "</td>";
					}
					$content .= "</tr>";
				}
			}
			return $content;
		}
		
		public function getFilteredContentButton($id, $status) {
			$btn_group = new ButtonGroup("noprint");
			if ($status != "sudah") {
				$btn = new Button("", "", "Ubah");
				$btn->setAction($this->action . ".edit('" . $id . "')");
				$btn->setClass("btn-warning");
				$btn->setAtribute("data-content='Ubah' data-toggle='popover'");
				$btn->setIcon("icon-edit icon-white");
				$btn->setIsButton(Button::$ICONIC);
				$btn_group->addElement($btn);
				$btn = new Button("", "", "Hapus");
				$btn->setAction($this->action . ".del('" . $id . "')");
				$btn->setClass("btn-danger");
				$btn->setAtribute("data-content='Hapus' data-toggle='popover'");
				$btn->setIcon("icon-remove icon-white");
				$btn->setIsButton(Button::$ICONIC);
				$btn_group->addElement($btn);
				$btn = new Button("", "", "Cetak");
				$btn->setAction($this->action . ".print_spb('" . $id . "')");
				$btn->setClass("btn-inverse");
				$btn->setAtribute("data-content='Cetak' data-toggle='popover'");
				$btn->setIcon("icon-print icon-white");
				$btn->setIsButton(Button::$ICONIC);
				$btn_group->addElement($btn);
			} else {
				$btn = new Button("", "", "View");
				$btn->setAction($this->action . ".detail('" . $id . "')");
				$btn->setClass("btn-success");
				$btn->setAtribute("data-content='View' data-toggle='popover'");
				$btn->setIcon("icon-eye-open icon-white");
				$btn->setIsButton(Button::$ICONIC);
				$btn_group->addElement($btn);
			}
			return $btn_group;
		}
	}
?>