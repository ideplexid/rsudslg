<?php
	class MutasiUnitMasukTable extends Table {
		public function getBodyContent() {
			$content = "";
			if ($this->content!=NULL) {
				foreach ($this->content as $d) {
					$content .= "<tr>";
					foreach ($this->header as $h) {
						$content .= "<td>" . $d[$h] . "</td>";
					}
					if ($this->is_action) {
						$content .= "<td>" . $this->getFilteredContentButton($d['id'], $d['status'])->getHtml() . "</td>";
					}
					$content .= "</tr>";
				}
			}
			return $content;
		}
		public function getFilteredContentButton($id, $status) {
			$btn_group = new ButtonGroup("noprint");
			if ($status == "belum") {
				$button = new Button("accept_button", "", "Terima");
				$button->setClass("btn-success");
				$button->setIsButton(Button::$ICONIC);
				$button->setIcon("fa fa-check");
				$button->setAction($this->action . ".accept('" . $id . "')");
				$btn_group->addElement($button);
				$button = new Button("return_button", "", "Kembalikan");
				$button->setClass("btn-danger");
				$button->setIsButton(Button::$ICONIC);
				$button->setIcon("fa fa-times");
				$button->setAction($this->action . ".return('" . $id . "')");
				$btn_group->addElement($button);
			} 
			return $btn_group;
		}
	}
?>