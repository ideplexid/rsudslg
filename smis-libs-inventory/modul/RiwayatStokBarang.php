<?php 
	require_once 'smis-base/smis-include-service-consumer.php';
	require_once 'smis-framework/smis/template/ModulTemplate.php';
	require_once 'smis-libs-inventory/adapter/StokBarangAdapter.php';
	require_once 'smis-libs-inventory/responder/RiwayatStokBarangDBResponder.php';
	
	
	class RiwayatStokBarang extends ModulTemplate {
		private $db;
		private $name;
		private $tbl_riwayat_stok_barang;
		private $tbl_stok_barang;
		private $tbl_barang_masuk;
		private $page;
		private $action;
		private $rsb_form;
		private $rsb_table;
		private $proto_name;
		private $proto_implement;
		private $proto_slug;
		
		public function __construct($db, $name, $tbl_riwayat_stok_barang, $tbl_stok_barang, $tbl_barang_masuk, $page) {
			$this->db = $db;
			$this->name = $name;
			$this->tbl_riwayat_stok_barang = $tbl_riwayat_stok_barang;
			$this->tbl_stok_barang = $tbl_stok_barang;
			$this->tbl_barang_masuk = $tbl_barang_masuk;
			$this->page = $page;
			$this->rsb_form = new Form("riwayat_stok_barang_form", "", $this->name . " : Riwayat Stok Barang");
			$id_stok_barang_text = new Text("riwayat_stok_barang_id_stok_barang", "riwayat_stok_barang_id_stok_barang", "");
			$id_stok_barang_text->setClass("smis-one-option-input");
			$id_stok_barang_text->setAtribute("disabled='disabled'");
			$browse_button = new Button("", "", "Pilih");
			$browse_button->setClass("btn-info");
			$browse_button->setIsButton(Button::$ICONIC);
			$browse_button->setIcon("icon-white ".Button::$icon_list_alt);
			$browse_button->setAction("stok_barang.chooser('stok_barang', 'stok_barang_button', 'stok_barang', stok_barang)");
			$input_group = new InputGroup("");
			$input_group->addComponent($id_stok_barang_text);
			$input_group->addComponent($browse_button);
			$this->rsb_form->addElement("No. Stok", $input_group);
			$nama_barang_text = new Text("riwayat_stok_barang_nama_barang", "riwayat_stok_barang_nama_barang", "");
			$nama_barang_text->setAtribute("disabled='disabled'");
			$this->rsb_form->addElement("Nama Barang", $nama_barang_text);
			$jenis_barang_text = new Text("riwayat_stok_barang_jenis_barang", "riwayat_stok_barang_jenis_barang", "");
			$jenis_barang_text->setAtribute("disabled='disabled'");
			$this->rsb_form->addElement("Jenis Barang", $jenis_barang_text);
			$produsen_text = new Text("riwayat_stok_barang_produsen", "riwayat_stok_barang_produsen", "");
			$produsen_text->setAtribute("disabled='disabled'");
			$this->rsb_form->addElement("Produsen", $produsen_text);
			$vendor_text = new Text("riwayat_stok_barang_vendor", "riwayat_stok_barang_vendor", "");
			$vendor_text->setAtribute("disabled='disabled'");
			$this->rsb_form->addElement("Vendor", $vendor_text);
			$satuan_text = new Text("riwayat_stok_barang_satuan", "riwayat_stok_barang_satuan", "");
			$satuan_text->setAtribute("disabled='disabled'");
			$this->rsb_form->addElement("Satuan", $satuan_text);
			$ed_text = new Text("riwayat_stok_barang_ed", "riwayat_stok_barang_ed", "");
			$ed_text->setAtribute("disabled='disabled'");
			$this->rsb_form->addElement("Tgl. ED", $ed_text);
			$show_button = new Button("", "", "Tampilkan");
			$show_button->setClass("btn-primary");
			$show_button->setIcon("icon-white icon-repeat");
			$show_button->setIsButton(Button::$ICONIC);
			$show_button->setAction("riwayat_stok_barang.view()");
			$print_button = new Button("", "", "Cetak");
			$print_button->setClass("btn-inverse");
			$print_button->setIcon("icon-white icon-print");
			$print_button->setIsButton(Button::$ICONIC);
			$print_button->setAction("riwayat_stok_barang.print()");
			$button_group = new ButtonGroup("noprint");
			$button_group->addElement($show_button);
			$button_group->addElement($print_button);
			$this->rsb_form->addElement("", $button_group);
			$header = array("Nomor", "Tanggal", "Masuk", "Keluar", "Sisa", "Keterangan", "Petugas Entri");
			$this->rsb_table = new Table($header);
			$this->rsb_table->setName("riwayat_stok_barang");
			$this->rsb_table->setAction(false);
			$this->proto_name = "";
			$this->proto_implement = "";
			$this->proto_slug = "";
		}
		
		public function setPrototype($proto_name, $proto_slug, $proto_implement) {
			$this->proto_name = $proto_name;
			$this->proto_slug = $proto_slug;
			$this->proto_implement = $proto_implement;
		}
		
		public function superCommand($super_command) {
			$rsb_adapter = new SimpleAdapter();
			$rsb_adapter->add("Nomor", "id", "digit8");
			$rsb_adapter->add("Tanggal", "tanggal", "date d M Y");
			$rsb_adapter->add("Masuk", "jumlah_masuk", "front +");
			$rsb_adapter->add("Keluar", "jumlah_keluar", "front -");
			$rsb_adapter->add("Sisa", "sisa");
			$rsb_adapter->add("Keterangan", "keterangan");
			$rsb_adapter->add("Petugas Entri", "nama_user");
			$rsb_dbtable = new DBTable($this->db, $this->tbl_riwayat_stok_barang);
			if (isset($_POST['super_command']) && $_POST['super_command'] == "riwayat_stok_barang") {
				if (isset($_POST['command'])) {
					$filter = "";
					if (isset($_POST['kriteria'])) {
						$filter = " AND  (keterangan LIKE '%" . $_POST['kriteria'] . "%' OR nama_user LIKE '%" . $_POST['kriteria'] . "%') ";
					}
					$query_value = "
						SELECT *
						FROM " . $this->tbl_riwayat_stok_barang . "
						WHERE id_stok_barang = '" . $_POST['id_stok_barang'] . "' " . $filter . "
					";
					$query_count = "
						SELECT COUNT(*)
						FROM " . $this->tbl_riwayat_stok_barang . "
						WHERE id_stok_barang = '" . $_POST['id_stok_barang'] . "' " . $filter . "
					";
					$rsb_dbtable->setPreferredQuery(true, $query_value, $query_count);
				}
			}
			$rsb_dbresponder = new RiwayatStokBarangDBResponder(
				$rsb_dbtable,
				$this->rsb_table,
				$rsb_adapter,
				$this->page
			);
			
			$stok_barang_table = new Table(
				array("No. Stok", "Nama Barang", "Jenis Barang", "Produsen", "Vendor", "Satuan", "Tgl. ED"),
				"",
				null,
				true
			);
			$stok_barang_table->setName("stok_barang");
			$stok_barang_table->setModel(Table::$SELECT);
			$stok_barang_adapter = new StokBarangAdapter();
			$columns = array("id", "nama_barang", "nama_jenis_barang", "produsen", "nama_vendor", "satuan", "tanggal_exp");
			$stok_barang_dbtable = new DBTable($this->db, $this->tbl_stok_barang, $columns);
			$filter = "";
			if (isset($_POST['kriteria'])) {
				$filter = " AND (" . $this->tbl_stok_barang . ".nama_barang LIKE '%" . $_POST['kriteria'] . "%' OR ".$this->tbl_stok_barang.".nama_jenis_barang LIKE '%" . $_POST['kriteria'] . "%' OR " . $this->tbl_stok_barang . ".produsen LIKE '%" . $_POST['kriteria'] . "%' OR " . $this->tbl_stok_barang . ".nama_vendor LIKE '%" . $_POST['kriteria'] . "%') ";
			}
			$query_value = "
				SELECT " . $this->tbl_stok_barang . ".*
				FROM " . $this->tbl_stok_barang . " LEFT JOIN " . $this->tbl_barang_masuk . " ON " . $this->tbl_stok_barang . ".id_barang_masuk = " . $this->tbl_barang_masuk . ".id
				WHERE " . $this->tbl_barang_masuk . ".status = 'sudah' " . $filter . "
			";
			$query_count = "
				SELECT COUNT(*)
				FROM " . $this->tbl_stok_barang . " LEFT JOIN " . $this->tbl_barang_masuk . " ON " . $this->tbl_stok_barang . ".id_barang_masuk = " . $this->tbl_barang_masuk . ".id
				WHERE " . $this->tbl_barang_masuk . ".status = 'sudah' " . $filter . "
			";
			$stok_barang_dbtable->setPreferredQuery(true, $query_value, $query_count);
			$stok_barang_dbresponder = new DBResponder(
				$stok_barang_dbtable,
				$stok_barang_table,
				$stok_barang_adapter
			);
			
			$super_command = new SuperCommand();
			$super_command->addResponder("riwayat_stok_barang", $rsb_dbresponder);
			$super_command->addResponder("stok_barang", $stok_barang_dbresponder);
			$init = $super_command->initialize();
			if ($init != null) {
				echo $init;
				return;
			}
		}
		
		public function phpPreLoad() {
			echo $this->rsb_form->getHtml();
			echo "<div id='table_content'>";
			echo $this->rsb_table->getHtml();
			echo "</div>";
			echo addJS("framework/smis/js/table_action.js");
		}
		
		public function jsPreLoad() {
			?>
			<script type="text/javascript">
				function RiwayatStokBarangAction(name, page, action, column) {
					this.initialize(name, page, action, column);
				}
				RiwayatStokBarangAction.prototype.constructor = RiwayatStokBarangAction;
				RiwayatStokBarangAction.prototype = new TableAction();
				RiwayatStokBarangAction.prototype.getViewData = function() {
					var data = TableAction.prototype.getViewData.call(this);
					data['id_stok_barang'] = $("#riwayat_stok_barang_id_stok_barang").val();
					return data;
				};
				RiwayatStokBarangAction.prototype.view = function() {
					showLoading();
					var self=this;

					var view_data=this.getViewData();
					$.post('',view_data,function(res){
						var json=getContent(res);
						if(json==null) {
							return;
						}
						$("#"+self.prefix+"_list").html(json.list);
						$("#"+self.prefix+"_pagination").html(json.pagination);		
						dismissLoading();
					});
				};
				RiwayatStokBarangAction.prototype.print = function() {
					if ($("#riwayat_stok_barang_id_stok_barang").val() == "")
						return;
					var data = this.getRegulerData();
					data['command'] = "print_stock_card";
					data['no_stok'] = $("#riwayat_stok_barang_id_stok_barang").val();
					data['nama_barang'] = $("#riwayat_stok_barang_nama_barang").val();
					data['jenis_barang'] = $("#riwayat_stok_barang_jenis_barang").val();
					data['produsen'] = $("#riwayat_stok_barang_produsen").val();
					data['vendor'] = $("#riwayat_stok_barang_vendor").val();
					data['satuan'] = $("#riwayat_stok_barang_satuan").val();
					data['tanggal_exp'] = $("#riwayat_stok_barang_ed").val();
					$.post(
						"",
						data,
						function(response) {
							var json = getContent(response);
							if (json == null) return;
							smis_print(json);
						}
					);
				};
				
				function StokBarangAction(name, page, action, column) {
					this.initialize(name, page, action, column);
				}
				StokBarangAction.prototype.constructor = StokBarangAction;
				StokBarangAction.prototype = new TableAction();
				StokBarangAction.prototype.selected = function(json) {
					$("#riwayat_stok_barang_id_stok_barang").val(json.id);
					$("#riwayat_stok_barang_nama_barang").val(json.nama_barang);
					$("#riwayat_stok_barang_jenis_barang").val(json.nama_jenis_barang);
					$("#riwayat_stok_barang_produsen").val(json.produsen);
					$("#riwayat_stok_barang_vendor").val(json.nama_vendor);
					$("#riwayat_stok_barang_satuan").val(json.satuan);
					if (json.tanggal_exp == "0000-00-00")
						$("#riwayat_stok_barang_ed").val("-");
					else
						$("#riwayat_stok_barang_ed").val(json.tanggal_exp);
						riwayat_stok_barang.view();
				};
				
				var riwayat_stok_barang;
				var stok_barang;
				var RIWAYAT_STOK_BARANG_PAGE="<?php echo $this->page; ?>";
				var RIWAYAT_STOK_BARANG_PNAME="<?php echo $this->proto_name; ?>";
				var RIWAYAT_STOK_BARANG_PSLUG="<?php echo $this->proto_slug; ?>";
				var RIWAYAT_STOK_BARANG_PIMPL="<?php echo $this->proto_implement; ?>";
				$(document).ready(function() {
					$('[data-toggle="popover"]').popover({
						trigger: 'hover',
						'placement': 'top'
					});
					$("#smis-chooser-modal").on("show", function() {
						$("#smis-chooser-modal").removeClass("full_model");
						$("#smis-chooser-modal").addClass("full_model");
					});
					$("#smis-chooser-modal").on("hidden", function() {
						$("#smis-chooser-modal").removeClass("full_model");
					});
					stok_barang = new StokBarangAction(
						"stok_barang",
						RIWAYAT_STOK_BARANG_PAGE,
						"riwayat_stok_barang",
						new Array()
					);
					stok_barang.setSuperCommand("stok_barang");
					stok_barang.setPrototipe(
						RIWAYAT_STOK_BARANG_PNAME,
						RIWAYAT_STOK_BARANG_PSLUG,
						RIWAYAT_STOK_BARANG_PIMPL
					);
					riwayat_stok_barang = new RiwayatStokBarangAction(
						"riwayat_stok_barang",
						RIWAYAT_STOK_BARANG_PAGE,
						"riwayat_stok_barang",
						new Array()
					);
					riwayat_stok_barang.setSuperCommand("riwayat_stok_barang");
					riwayat_stok_barang.setPrototipe(
						RIWAYAT_STOK_BARANG_PNAME,
						RIWAYAT_STOK_BARANG_PSLUG,
						RIWAYAT_STOK_BARANG_PIMPL
					);
					riwayat_stok_barang.view();
				});
			</script>
			<?php
		}
	}
?>