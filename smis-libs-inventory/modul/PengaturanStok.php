<?php 
	require_once 'smis-framework/smis/template/ModulTemplate.php';
	require_once("smis-base/smis-include-service-consumer.php");

	class PengaturanStok extends ModulTemplate{
		private $db;
		private $name;
		private $tbl_pengaturan;
		private $page;
		private $action;
		private $pengaturan_stok_minmaks_table;
		private $proto_name;
		private $proto_implement;
		private $proto_slug;
		
		public function __construct($db, $name_entity, $slug, $page,$entity=""){
			$this->db=$db;
			$this->name=$name_entity;
			if($entity!="") $entity="_".$entity;
			$this->tbl_pengaturan="smis_".$slug."_stok_barang_minimum_maksimum".$entity;
			$this->page=$page;
			$header=array("Nomor", "Barang", "Jenis", "Minimum", "Maksimum", "Satuan");
			$this->pengaturan_stok_minmaks_table = new Table($header, $this->name . " : Pengaturan Stok Minimum - Maksimum Barang");
			$this->pengaturan_stok_minmaks_table->setName("pengaturan_stok_minmaks");
			$this->proto_name="";
			$this->proto_implement="";
			$this->proto_slug="";
		}
		
		public function setPrototype($pname,$pslug,$pimplement){
			$this->proto_name=$pname;
			$this->proto_implement=$pimplement;
			$this->proto_slug=$pslug;
		}
		
		public function command($command){
			$pengaturan_stok_minmaks_adapter = new SimpleAdapter();
			$pengaturan_stok_minmaks_adapter->add("Nomor", "id_barang","digit8");
			$pengaturan_stok_minmaks_adapter->add("Barang", "nama_barang");
			$pengaturan_stok_minmaks_adapter->add("Jenis", "nama_jenis_barang");
			$pengaturan_stok_minmaks_adapter->add("Minimum", "jumlah_min");
			$pengaturan_stok_minmaks_adapter->add("Maksimum", "jumlah_max");
			$pengaturan_stok_minmaks_adapter->add("Satuan", "satuan");
			$pengaturan_stok_minmaks_dbtable = new DBTable($this->db,$this->tbl_pengaturan);
			$pengaturan_stok_minmaks_dbresponder = new DBResponder($pengaturan_stok_minmaks_dbtable,$this->pengaturan_stok_minmaks_table,$pengaturan_stok_minmaks_adapter);
			$data = $pengaturan_stok_minmaks_dbresponder->command($_POST['command']);
			echo json_encode($data);
			return;
		}
		
		public function superCommand($super_command){
			$head=array("Barang", "Jenis");
			$barang_table = new Table($head);
			$barang_table->setName("pengaturan_stok_minmaks_barang");
			$barang_table->setModel(Table::$SELECT);
			$barang_adapter = new SimpleAdapter();
			$barang_adapter->add("Barang", "nama");
			$barang_adapter->add("Jenis", "nama_jenis_barang");
			$barang_service_responder = new ServiceResponder($db,$barang_table,$barang_adapter,"get_daftar_barang_non_inventaris");
			$super_command = new SuperCommand();
			$super_command->addResponder("pengaturan_stok_minmaks_barang", $barang_service_responder);
			$init = $super_command->initialize();
			if ($init != null) {
				echo $init;
				return;
			}
		}
		
		public function phpPreLoad(){
			$this->pengaturan_stok_minmaks_table->addModal("id", "hidden", "", "");
			$this->pengaturan_stok_minmaks_table->addModal("id_barang", "hidden", "", "");
			$this->pengaturan_stok_minmaks_table->addModal("nama_barang", "chooser-pengaturan_stok_minmaks-pengaturan_stok_minmaks_barang", "Barang", "");
			$this->pengaturan_stok_minmaks_table->addModal("nama_jenis_barang", "text", "Jenis", "","y",null,true);
			$this->pengaturan_stok_minmaks_table->addModal("jumlah_min", "text", "Minimum", "","n","numeric");
			$this->pengaturan_stok_minmaks_table->addModal("jumlah_max", "text", "Maximum", "","n","numeric");
			$this->pengaturan_stok_minmaks_table->addModal("satuan", "text", "Satuan", "","n");
			$this->pengaturan_stok_minmaks_table->addModal("medis", "hidden", "", "");
			$pengaturan_stok_minmaks_modal=$this->pengaturan_stok_minmaks_table->getModal();
			
			echo $pengaturan_stok_minmaks_modal->getHtml();
			echo $this->pengaturan_stok_minmaks_table->getHtml();
			echo addJS("framework/smis/js/table_action.js");
		}
		
		public function jsPreLoad(){
			?>		
			<script type="text/javascript">
				function PengaturanStokMinMaksAction(name, page, action, column) {
					this.initialize(name, page, action, column);
				}
				PengaturanStokMinMaksAction.prototype.constructor = PengaturanStokMinMaksAction;
				PengaturanStokMinMaksAction.prototype = new TableAction();
				PengaturanStokMinMaksAction.prototype.show_add_form = function() {
					$("#pengaturan_stok_minmaks_id").val("");
					$("#pengaturan_stok_minmaks_id_barang").val("");
					$("#pengaturan_stok_minmaks_nama_barang").val("");
					$("#pengaturan_stok_minmaks_nama_jenis_barang").val("");
					$("#pengaturan_stok_minmaks_medis").val("");
					$("#pengaturan_stok_minmaks_jumlah_min").val("");
					$("#pengaturan_stok_minmaks_jumlah_max").val("");
					$("#pengaturan_stok_minmaks_satuan").val("");
					$("#modal_alert_pengaturan_stok_minmaks_add_form").html();
					$(".error_field").removeClass("error_field");
					$("#pengaturan_stok_minmaks_add_form").smodal("show");
				};
				PengaturanStokMinMaksAction.prototype.validate = function() {
					var valid = true;
					var invalid_msg = "";
					var nama_barang = $("#pengaturan_stok_minmaks_nama_barang").val();
					var jumlah_min = $("#pengaturan_stok_minmaks_jumlah_min").val();
					var jumlah_max = $("#pengaturan_stok_minmaks_jumlah_max").val();
					var satuan = $("#pengaturan_stok_minmaks_satuan").val();
					$(".error_field").removeClass("error_field");
					if (nama_barang == "") {
						valid = false;
						invalid_msg += "<br/><strong>Barang</strong> tidak boleh kosong";
						$("#pengaturan_stok_minmaks_nama_barang").addClass("error_field");
					}
					if (jumlah_min == "") {
						valid = false;
						invalid_msg += "<br/><strong>Jumlah Min.</strong> tidak boleh kosong";
						$("#pengaturan_stok_minmaks_jumlah_min").addClass("error_field");
					} else if (!is_numeric(jumlah_min)) {
						valid = false;
						invalid_msg += "<br/><strong>Jumlah Min.</strong> seharusnya numerik (0-9)";
						$("#pengaturan_stok_minmaks_jumlah_min").addClass("error_field");
					}
					if (jumlah_max == "") {
						valid = false;
						invalid_msg += "<br/><strong>Jumlah Max.</strong> tidak boleh kosong";
						$("#pengaturan_stok_minmaks_jumlah_max").addClass("error_field");
					} else if (!is_numeric(jumlah_max)) {
						valid = false;
						invalid_msg += "<br/><strong>Jumlah Max.</strong> seharusnya numerik (0-9)";
						$("#pengaturan_stok_minmaks_jumlah_max").addClass("error_field");
					}
					if (satuan == "") {
						valid = false;
						invalid_msg += "<br/><strong>Satuan</strong> tidak boleh kosong";
						$("#pengaturan_stok_minmaks_satuan").addClass("error_field");
					}
					if (!valid) {
						$("#modal_alert_pengaturan_stok_minmaks_add_form").html(
							"<div class='alert alert-block alert-danger'>" +
								"<h4>Peringatan</h4>" +
								invalid_msg +
							"</div>"
						);
					}
					return valid;
				};
				PengaturanStokMinMaksAction.prototype.save = function() {
					if (!this.validate()) {
						return;
					}
					showLoading();
					$("#pengaturan_stok_minmaks_add_form").smodal("hide");
					var self = this;
					var data = this.getRegulerData();
					data['command'] = "save";
					data['id'] = $("#pengaturan_stok_minmaks_id").val();
					data['id_barang'] = $("#pengaturan_stok_minmaks_id_barang").val();
					data['nama_barang'] = $("#pengaturan_stok_minmaks_nama_barang").val();
					data['nama_jenis_barang'] = $("#pengaturan_stok_minmaks_nama_jenis_barang").val();
					data['medis'] = $("#pengaturan_stok_minmaks_medis").val();
					data['jumlah_min'] = $("#pengaturan_stok_minmaks_jumlah_min").val();
					data['jumlah_max'] = $("#pengaturan_stok_minmaks_jumlah_max").val();
					data['satuan'] = $("#pengaturan_stok_minmaks_satuan").val();
					$.post(
						"",
						data,
						function(response) {
							var json = getContent(response);
							if (json == null) {
								$("#pengaturan_stok_minmaks_add_form").smodal("show");
							} else {
								self.view();
							}
							dismissLoading();
						}
					);
				};
				PengaturanStokMinMaksAction.prototype.edit = function(id) {
					var data = this.getRegulerData();
					data['command'] = "edit";
					data['id'] = id;
					$.post(
						"",
						data,
						function(response) {
							var json = getContent(response);
							if (json == null) return;
							$("#pengaturan_stok_minmaks_id").val(id);
							$("#pengaturan_stok_minmaks_id_barang").val(json.id_barang);
							$("#pengaturan_stok_minmaks_nama_barang").val(json.nama_barang);
							$("#pengaturan_stok_minmaks_nama_jenis_barang").val(json.nama_jenis_barang);
							$("#pengaturan_stok_minmaks_medis").val(json.medis);
							$("#pengaturan_stok_minmaks_jumlah_min").val(json.jumlah_min);
							$("#pengaturan_stok_minmaks_jumlah_max").val(json.jumlah_max);
							$("#pengaturan_stok_minmaks_satuan").val(json.satuan);
							$("#modal_alert_pengaturan_stok_minmaks_add_form").html();
							$(".error_field").removeClass("error_field");
							$("#pengaturan_stok_minmaks_add_form").smodal("show");
						}
					);
				};
				
				function BarangAction(name, page, action, column) {
					this.initialize(name, page, action, column);
				}
				BarangAction.prototype.constructor = BarangAction;
				BarangAction.prototype = new TableAction();
				BarangAction.prototype.selected = function(json) {
					$("#pengaturan_stok_minmaks_id_barang").val(json.id);
					$("#pengaturan_stok_minmaks_nama_barang").val(json.nama);
					$("#pengaturan_stok_minmaks_nama_jenis_barang").val(json.nama_jenis_barang);
					$("#pengaturan_stok_minmaks_medis").val(json.medis);
				};
				var PENGATURAN_STOK_MINMAX_ENTITY="<?php echo $this->page; ?>";
				var pengaturan_stok_minmaks;
				var pengaturan_stok_minmaks_barang;
				var PENGATURAN_STOK_MINMAX_PNAME="<?php echo $this->proto_name; ?>";
				var PENGATURAN_STOK_MINMAX_PSLUG="<?php echo $this->proto_slug; ?>";
				var PENGATURAN_STOK_MINMAX_PIMPL="<?php echo $this->proto_implement; ?>";
				$(document).ready(function() {
					$('[data-toggle="popover"]').popover({
						trigger: 'hover',
						'placement': 'top'
					});
					pengaturan_stok_minmaks_barang = new BarangAction("pengaturan_stok_minmaks_barang",PENGATURAN_STOK_MINMAX_ENTITY,"pengaturan_stok_minmaks",new Array());
					pengaturan_stok_minmaks_barang.setSuperCommand("pengaturan_stok_minmaks_barang");
					pengaturan_stok_minmaks_barang.setPrototipe(PENGATURAN_STOK_MINMAX_PNAME,PENGATURAN_STOK_MINMAX_PSLUG,PENGATURAN_STOK_MINMAX_PIMPL);
					var pengaturan_stok_minmaks_columns = new Array("id", "id_barang", "nama_barang", "nama_jenis_barang", "medis", "jumlah_min", "jumlah_max", "satuan");
					pengaturan_stok_minmaks = new PengaturanStokMinMaksAction("pengaturan_stok_minmaks",PENGATURAN_STOK_MINMAX_ENTITY,"pengaturan_stok_minmaks",pengaturan_stok_minmaks_columns);
					pengaturan_stok_minmaks.setPrototipe(PENGATURAN_STOK_MINMAX_PNAME,PENGATURAN_STOK_MINMAX_PSLUG,PENGATURAN_STOK_MINMAX_PIMPL);
					pengaturan_stok_minmaks.view();
					
				});
			</script>
			<?php 
		}	
	}
?>