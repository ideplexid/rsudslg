<?php 
	require_once 'smis-base/smis-include-service-consumer.php';
	require_once 'smis-framework/smis/template/ModulTemplate.php';
	require_once 'smis-libs-inventory/responder/ObatKSODBResponder.php';
	
	class KartuStokObat extends ModulTemplate {
		private $db;
		private $name;
		private $tbl_kartu_stok_obat;
		private $tbl_stok_obat;
		private $tbl_obat_masuk;
		private $page;
		private $action;
		private $kso_form;
		private $kso_table;
		private $kso_loading_bar;
		private $kso_loading_modal;
		private $proto_name;
		private $proto_implement;
		private $proto_slug;
		
		public function __construct($db, $name, $tbl_kartu_stok_obat, $tbl_stok_obat, $tbl_obat_masuk, $page) {
			$this->db = $db;
			$this->name = $name;
			$this->tbl_kartu_stok_obat = $tbl_kartu_stok_obat;
			$this->tbl_stok_obat = $tbl_stok_obat;
			$this->tbl_obat_masuk = $tbl_obat_masuk;
			$this->page = $page;
			$this->kso_form = new Form("kartu_stok_obat_form", "", $this->name . " : Kartu Stok Obat");
			$id_obat_text = new Text("kartu_stok_obat_id_obat", "kartu_stok_obat_id_obat", "");
			$id_obat_text->setClass("smis-one-option-input");
			$id_obat_text->setAtribute("disabled='disabled'");
			$browse_button = new Button("", "", "Pilih");
			$browse_button->setClass("btn-info");
			$browse_button->setIsButton(Button::$ICONIC);
			$browse_button->setIcon("icon-white ".Button::$icon_list_alt);
			$browse_button->setAction("obat_kso.chooser('obat_kso', 'obat_kso_button', 'obat_kso', obat_kso)");
			$input_group = new InputGroup("");
			$input_group->addComponent($id_obat_text);
			$input_group->addComponent($browse_button);
			$this->kso_form->addElement("ID Obat", $input_group);
			$kode_obat_text = new Text("kartu_stok_obat_kode_obat", "kartu_stok_obat_kode_obat", "");
			$kode_obat_text->setAtribute("disabled='disabled'");
			$this->kso_form->addElement("Kode Obat", $kode_obat_text);
			$nama_obat_text = new Text("kartu_stok_obat_nama_obat", "kartu_stok_obat_nama_obat", "");
			$nama_obat_text->setAtribute("disabled='disabled'");
			$this->kso_form->addElement("Nama Obat", $nama_obat_text);
			$jenis_obat_text = new Text("kartu_stok_obat_jenis_obat", "kartu_stok_obat_jenis_obat", "");
			$jenis_obat_text->setAtribute("disabled='disabled'");
			$this->kso_form->addElement("Jenis Obat", $jenis_obat_text);
			$satuan_text = new Text("kartu_stok_obat_satuan", "kartu_stok_obat_satuan", "");
			$satuan_text->setAtribute("disabled='disabled'");
			$this->kso_form->addElement("Satuan", $satuan_text);
			$tanggal_from_text = new Text("kartu_stok_obat_tanggal_from", "kartu_stok_obat_tanggal_from", date("Y-m-") . "01");
			$tanggal_from_text->setClass("mydate");
			$tanggal_from_text->setAtribute("data-date-format='yyyy-mm-dd'");
			$this->kso_form->addElement("Tanggal Awal", $tanggal_from_text);
			$tanggal_to_text = new Text("kartu_stok_obat_tanggal_to", "kartu_stok_obat_tanggal_to", date("Y-m-d"));
			$tanggal_to_text->setClass("mydate");
			$tanggal_to_text->setAtribute("data-date-format='yyyy-mm-dd'");
			$this->kso_form->addElement("Tanggal Akhir", $tanggal_to_text);
			$show_button = new Button("", "", "Tampilkan");
			$show_button->setClass("btn-primary");
			$show_button->setIcon("icon-white icon-repeat");
			$show_button->setIsButton(Button::$ICONIC);
			$show_button->setAction("kartu_stok_obat.view()");
			$download_button = new Button("", "", "Eksport XLS");
			$download_button->setClass("btn-inverse");
			$download_button->setIcon("fa fa-download");
			$download_button->setIsButton(Button::$ICONIC);
			$download_button->setAtribute("id='download_button'");
			$button_group = new ButtonGroup("noprint");
			$button_group->addElement($show_button);
			$button_group->addElement($download_button);
			$this->kso_form->addElement("", $button_group);
			$header = array("No.", "Tanggal", "No. Bon", "Dipakai untuk / Diterima dari", "Masuk", "Keluar", "Sisa");
			$this->kso_table = new Table($header);
			$this->kso_table->setName("kartu_stok_obat");
			$this->kso_table->setAction(false);
			$this->kso_table->setFooterVisible(false);
			$this->proto_name = "";
			$this->proto_implement = "";
			$this->proto_slug = "";

			$this->kso_loading_bar = new LoadingBar("kartu_stok_obat_loading_bar", "");
			$button = new Button("", "", "Batal");
			$button->addClass("btn-primary");
			$button->setIsButton(Button::$ICONIC_TEXT);
			$button->setIcon("fa fa-close");
			$button->setAction("kartu_stok_obat.cancel()");
			$this->kso_loading_modal = new Modal("kartu_stok_obat_loading_modal", "", "Proses..");
			$this->kso_loading_modal->addHtml($this->kso_loading_bar->getHtml(), "after");
			$this->kso_loading_modal->addFooter($button);
		}
		
		public function setPrototype($proto_name, $proto_slug, $proto_implement) {
			$this->proto_name = $proto_name;
			$this->proto_slug = $proto_slug;
			$this->proto_implement = $proto_implement;
		}
		
		public function superCommand($super_command) {			
			$obat_table = new Table(
				array("ID Obat", "Kode Obat", "Nama Obat", "Jenis Obat"),
				"",
				null,
				true
			);
			$obat_table->setName("obat_kso");
			$obat_table->setModel(Table::$SELECT);
			$obat_adapter = new SimpleAdapter();
			$obat_adapter->add("ID Obat", "id", "digit8");
			$obat_adapter->add("Kode Obat", "kode_obat");
			$obat_adapter->add("Nama Obat", "nama_obat");
			$obat_adapter->add("Jenis Obat", "nama_jenis_obat");
			$obat_dbtable = new DBTable($this->db, $this->tbl_stok_obat);
			$filter = "1";
			if (isset($_POST['kriteria'])) {
				$filter .= " AND (id LIKE '%" . $_POST['kriteria'] . "%' OR kode_obat LIKE '%" . $_POST['kriteria'] . "%' OR nama_obat LIKE '%" . $_POST['kriteria'] . "%' OR nama_jenis_obat LIKE '%" . $_POST['kriteria'] . "%') ";
			}
			$query_value = "
				SELECT *
				FROM (
					SELECT DISTINCT a.id_obat AS 'id', a.kode_obat, a.nama_obat, a.nama_jenis_obat
					FROM " . $this->tbl_stok_obat . " a LEFT JOIN " . $this->tbl_obat_masuk . " b ON a.id_obat_masuk = b.id
					WHERE a.prop NOT LIKE 'del' AND b.prop NOT LIKE 'del' AND b.status = 'sudah'
					ORDER BY kode_obat, nama_jenis_obat, nama_obat ASC
				) v
				WHERE " . $filter . "
			";
			$query_count = "
				SELECT COUNT(*)
				FROM (
					" . $query_value . "
				) v
			";
			$obat_dbtable->setPreferredQuery(true, $query_value, $query_count);
			$obat_dbresponder = new ObatKSODBResponder(
				$obat_dbtable,
				$obat_table,
				$obat_adapter,
				$this->tbl_stok_obat,
				$this->tbl_obat_masuk
			);
			
			$super_command = new SuperCommand();
			$super_command->addResponder("obat_kso", $obat_dbresponder);
			$init = $super_command->initialize();
			if ($init != null) {
				echo $init;
				return;
			}
		}

		public function command($command) {
			if (isset($_POST['command'])) {
				if ($_POST['command'] == "get_jumlah_sisa") {
					$tanggal_from = $_POST['tanggal_from'];
					$tanggal_to = $_POST['tanggal_to'];
					$id_obat = $_POST['id_obat'] = $_POST['id_obat'];
					$dbtable = new DBTable($this->db, $this->tbl_kartu_stok_obat);
					$row = $dbtable->get_row("
						SELECT COUNT(*) AS 'jumlah'
						FROM " . $this->tbl_kartu_stok_obat . "
						WHERE tanggal >= '" . $tanggal_from . "' AND tanggal <= '" . date('Y-m-d') . "' AND id_obat = '" . $id_obat . "' AND prop NOT LIKE 'del'
					");
					$jumlah = 0;
					if ($row != null)
						$jumlah = $row->jumlah;
					$row = $dbtable->get_row("
						SELECT SUM(a.sisa) AS 'sisa'
						FROM " . $this->tbl_stok_obat . " a LEFT JOIN " . $this->tbl_obat_masuk . " b ON a.id_obat_masuk = b.id
						WHERE b.status = 'sudah' AND a.id_obat = '" . $id_obat . "'
					");
					$sisa = 0;
					if ($row != null)
						$sisa = $row->sisa;
					$data = array(
						"jumlah"	=> $jumlah,
						"sisa"		=> $sisa
					);
					echo json_encode($data);
					return;
				} else if ($_POST['command'] == "get_info") {
					$tanggal_from = $_POST['tanggal_from'];
					$tanggal_to = $_POST['tanggal_to'];
					$id_obat = $_POST['id_obat'] = $_POST['id_obat'];
					$num = $_POST['num'];
					$sisa = $_POST['sisa'];
					$dbtable = new DBTable($this->db, $this->tbl_kartu_stok_obat);
					$row = $dbtable->get_row("
						SELECT *
						FROM " . $this->tbl_kartu_stok_obat . "
						WHERE tanggal >= '" . $tanggal_from . "' AND tanggal <= '" . date('Y-m-d') . "' AND id_obat = '" . $id_obat . "' AND prop NOT LIKE 'del'
						ORDER BY tanggal DESC, id DESC
						LIMIT " . $num . ", 1
					");
					$html = "";
					$nama_obat = "";
					$tanggal = "";
					$no_bon = "";
					if ($row != null) {
						if (strtotime($row->tanggal) >= strtotime($tanggal_from) && strtotime($row->tanggal) <= strtotime($tanggal_to)) {
							$html = "
								<tr>
									<td id='nomor'></td>
									<td id='tanggal'>" . ArrayAdapter::format("date d-m-Y", $row->tanggal) . "</td>
									<td id='no_bon'>" . $row->no_bon . "</td>
									<td id='unit'>" . $row->unit . "</td>
									<td id='f_masuk'>" . ArrayAdapter::format("number", $row->masuk) . "</td>
									<td id='masuk' style='display: none;'>" . $row->masuk . "</td>
									<td id='f_keluar'>" . ArrayAdapter::format("number", $row->keluar) . "</td>
									<td id='keluar' style='display: none;'>" . $row->keluar . "</td>
									<td id='f_sisa'>" . ArrayAdapter::format("number", $sisa) . "</td>
									<td id='sisa' style='display: none;'>" . $sisa . "</td>
								</tr>
							";
						}
						$masuk = $row->masuk;
						$keluar = $row->keluar;
						$no_bon = $row->no_bon;
						$tanggal = $row->tanggal;
						$nama_obat = $row->nama_obat;
					}
					$data = array(
						"html" 		=> $html,
						"id_obat"	=> ArrayAdapter::format("only-digit6", $id_obat),
						"nama_obat"	=> $nama_obat,
						"tanggal"	=> ArrayAdapter::format("date d-m-Y", $tanggal),
						"no_bon"	=> $no_bon,
						"sisa"		=> $sisa - $masuk + $keluar
					);
					echo json_encode($data);
					return;
				} else if ($_POST['command'] == "export_excel") {
					require_once("smis-libs-out/php-excel/PHPExcel.php");
					
					$objPHPExcel = PHPExcel_IOFactory::load("smis-libs-inventory/templates/template_kartu_stok_obat.xlsx");
					$objPHPExcel->setActiveSheetIndexByName("KARTU STOK OBAT");
					$objWorksheet = $objPHPExcel->getActiveSheet();
					
					$id_obat = $_POST['id_obat'];
					$kode_obat = $_POST['kode_obat'];
					$nama_obat = $_POST['nama_obat'];
					$nama_jenis_obat = $_POST['nama_jenis_obat'];
					$tanggal_from = $_POST['tanggal_from'];
					$tanggal_to = $_POST['tanggal_to'];
					$nama_instansi = getSettings($db, "smis_autonomous_title", "SIMRS");
					$alamat_instansi = getSettings($db, "smis_autonomous_address");
					if ($nama_instansi == "")
						$objWorksheet->setCellValue("B3", "SIMRS");
					else
						$objWorksheet->setCellValue("B3", $nama_instansi);
					if ($alamat_instansi == "")
						$objWorksheet->setCellValue("B4", "ALAMAT SIMRS");
					else
						$objWorksheet->setCellValue("B4", $alamat_instansi);
					$objWorksheet->setCellValue("B5", strtoupper($this->name));
					$objWorksheet->setCellValue("E5", ": " . $kode_obat);
					$objWorksheet->setCellValue("H3", ": " . $kode_obat);
					$objWorksheet->setCellValue("H4", ": " . $nama_obat);
					$objWorksheet->setCellValue("H5", ": 0");
					
					$data = json_decode($_POST['d_data']);
					if ($_POST['num_rows'] - 2 > 0)
						$objWorksheet->insertNewRowBefore(10, $_POST['num_rows'] - 2);
					$start_row_num = 10;
					$end_row_num = 10;
					$row_num = $start_row_num;
					foreach ($data as $d) {
						$col_num = 1;
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->nomor);
						$col_num++;
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->tanggal);
						$col_num++;
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->unit);
						$col_num+=2;
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->masuk);
						$col_num++;
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->keluar);
						$col_num++;
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->sisa);
						$objWorksheet->getStyle("F" . $row_num)->getNumberFormat()->setFormatCode("#,##0");
						$objWorksheet->getStyle("G" . $row_num)->getNumberFormat()->setFormatCode("#,##0");
						$objWorksheet->getStyle("H" . $row_num)->getNumberFormat()->setFormatCode("#,##0");
						$row_num++;
						$end_row_num++;
					}
					
					header("Content-type: application/vnd.ms-excel");	
					header("Content-Disposition: attachment; filename=KARTU_STOK_OBAT-" . strtoupper($this->name) . "-" . $kode_obat . "_" . ArrayAdapter::format("only-digit6", $id_obat) . "_" . date("Ymd_his") . ".xlsx");
					$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");
					$objWriter->save("php://output");			
					return;
				}
				return;
			}
		}
		
		public function phpPreLoad() {
			echo $this->kso_loading_modal->getHtml();
			echo $this->kso_form->getHtml();
			echo "<div id='table_content'>";
			echo $this->kso_table->getHtml();
			echo "</div>";
			echo "<div id='kartu_stok_obat_info'></div>";
			echo addJS("framework/smis/js/table_action.js");
			echo addJS("framework/bootstrap/js/bootstrap-datepicker.js");
			echo addJS("base-js/smis-base-loading.js");
			echo addCSS("framework/bootstrap/css/datepicker.css");
		}
		
		public function jsPreLoad() {
			?>
			<script type="text/javascript">
				function KartuStokObatAction(name, page, action, column) {
					this.initialize(name, page, action, column);
				}
				KartuStokObatAction.prototype.constructor = KartuStokObatAction;
				KartuStokObatAction.prototype = new TableAction();
				KartuStokObatAction.prototype.getRegulerData = function() {
					var data = TableAction.prototype.getRegulerData.call(this);
					data['id_obat'] = $("#kartu_stok_obat_id_obat").val();
					data['kode_obat'] = $("#kartu_stok_obat_kode_obat").val();
					data['nama_obat'] = $("#kartu_stok_obat_nama_obat").val();
					data['jenis_obat'] = $("#kartu_stok_obat_jenis_obat").val();
					data['satuan'] = $("#kartu_stok_obat_satuan").val();
					if ($("#kartu_stok_obat_tanggal_from").val() != "" && $("#kartu_stok_obat_tanggal_to").val() != "") {
						data['tanggal_from'] = $("#kartu_stok_obat_tanggal_from").val();
						data['tanggal_to'] = $("#kartu_stok_obat_tanggal_to").val();
					}
					return data;
				};
				KartuStokObatAction.prototype.view = function() {
					if ($("#kartu_stok_obat_id_obat").val() == "")
						return;
					var self = this;
					$("#info").empty();
					$("#kartu_stok_obat_loading_bar").sload("true", "Harap ditunggu...", 0);
					$("#kartu_stok_obat_loading_modal").smodal("show");
					FINISHED = false;
					var data = this.getRegulerData();
					data['command'] = "get_jumlah_sisa";
					$.post(
						"",
						data,
						function(response) {
							var json = JSON.parse(response);
							if (json == null) return;
							$("#kartu_stok_obat_list").empty();
							self.fill_html(0, json.jumlah, json.sisa);
						}
					);
				};
				KartuStokObatAction.prototype.fill_html = function(num, limit, sisa) {
					if (num == limit || FINISHED) {
						if (FINISHED == false && num == limit) {
							this.finalize();
						} else {
							$("#kartu_stok_obat_loading_modal").smodal("hide");
							$("#kartu_stok_obat_info").html(
								"<div class='alert alert-block alert-inverse'>" +
									 "<center><strong>PROSES DIBATALKAN</strong></center>" +
								 "</div>"
							);
							$("#download_button").removeAttr("onclick");
						}
						return;
					}
					var self = this;
					var data = this.getRegulerData();
					data['command'] = "get_info";
					data['num'] = num;
					data['sisa'] = sisa;
					$.post(
						"",
						data,
						function(response) {
							var json = JSON.parse(response);
							if (json == null) return;
							var html = $("#kartu_stok_obat_list").html();
							$("#kartu_stok_obat_list").html(json.html + html);
							$("#kartu_stok_obat_loading_bar").sload("true", json.id_obat + " - " + json.nama_obat + " - TANGGAL : " + json.tanggal + "- NO. BON : " + json.no_bon + " (" + (num + 1) + " / " + limit + ")", (num + 1) * 100 / limit - 1);
							self.fill_html(num + 1, limit, json.sisa);
						}
					);
				};
				KartuStokObatAction.prototype.finalize = function() {
					var num_rows = $("tbody#kartu_stok_obat_list tr").length;
					for (var i = 0; i < num_rows; i++)
						$("tbody#kartu_stok_obat_list tr:eq(" + i + ") td#nomor").html(i + 1);
					$("#kartu_stok_obat_loading_modal").smodal("hide");
					$("#info").html(
						"<div class='alert alert-block alert-info'>" +
							 "<center><strong>PROSES SELESAI</strong></center>" +
						 "</div>"
					);
					$("#download_button").removeAttr("onclick");
					$("#download_button").attr("onclick", "kartu_stok_obat.export_xls()");
				};
				KartuStokObatAction.prototype.export_xls = function() {
					showLoading();
					var num_rows = $("#kartu_stok_obat_list").children("tr").length;
					var d_data = {};
					for (var i = 0; i < num_rows; i++) {
						var nomor = $("tbody#kartu_stok_obat_list tr:eq(" + i + ") td#nomor").text();
						var tanggal = $("tbody#kartu_stok_obat_list tr:eq(" + i + ") td#tanggal").text();
						var no_bon = $("tbody#kartu_stok_obat_list tr:eq(" + i + ") td#no_bon").text();
						var unit = $("tbody#kartu_stok_obat_list tr:eq(" + i + ") td#unit").text();
						var masuk = $("tbody#kartu_stok_obat_list tr:eq(" + i + ") td#masuk").text();
						var keluar = $("tbody#kartu_stok_obat_list tr:eq(" + i + ") td#keluar").text();
						var sisa = $("tbody#kartu_stok_obat_list tr:eq(" + i + ") td#sisa").text();
						d_data[i] = {
							"nomor" 	: nomor,
							"tanggal" 	: tanggal,
							"no_bon" 	: no_bon,
							"unit" 		: unit,
							"masuk"		: masuk,
							"keluar"	: keluar,
							"sisa"		: sisa
						};
					}
					var data = this.getRegulerData();
					data['command'] = "export_excel";
					data['d_data'] = JSON.stringify(d_data);
					data['num_rows'] = num_rows;
					postForm(data);
					dismissLoading();
				};
				
				function ObatKSOAction(name, page, action, column) {
					this.initialize(name, page, action, column);
				}
				ObatKSOAction.prototype.constructor = ObatKSOAction;
				ObatKSOAction.prototype = new TableAction();
				ObatKSOAction.prototype.selected = function(json) {
					$("#kartu_stok_obat_id_obat").val(json.id);
					$("#kartu_stok_obat_kode_obat").val(json.kode_obat);
					$("#kartu_stok_obat_nama_obat").val(json.nama_obat);
					$("#kartu_stok_obat_jenis_obat").val(json.nama_jenis_obat);
					$("#kartu_stok_obat_satuan").val(json.satuan);
				};
				
				var kartu_stok_obat;
				var obat_kso;
				var KARTU_STOK_OBAT_PAGE = "<?php echo $this->page; ?>";
				var KARTU_STOK_OBAT_PNAME = "<?php echo $this->proto_name; ?>";
				var KARTU_STOK_OBAT_PSLUG = "<?php echo $this->proto_slug; ?>";
				var KARTU_STOK_OBAT_PIMPL = "<?php echo $this->proto_implement; ?>";
				$(document).ready(function() {
					$('[data-toggle="popover"]').popover({
						trigger: 'hover',
						'placement': 'top'
					});
					$("mydate").datepicker();
					$("#smis-chooser-modal").on("show", function() {
						$("#smis-chooser-modal").removeClass("full_model");
						$("#smis-chooser-modal").addClass("full_model");
					});
					$("#smis-chooser-modal").on("hidden", function() {
						$("#smis-chooser-modal").removeClass("full_model");
					});
					obat_kso = new ObatKSOAction(
						"obat_kso",
						KARTU_STOK_OBAT_PAGE,
						"kartu_stok_obat",
						new Array()
					);
					obat_kso.setSuperCommand("obat_kso");
					obat_kso.setPrototipe(
						KARTU_STOK_OBAT_PNAME,
						KARTU_STOK_OBAT_PSLUG,
						KARTU_STOK_OBAT_PIMPL
					);
					kartu_stok_obat = new KartuStokObatAction(
						"kartu_stok_obat",
						KARTU_STOK_OBAT_PAGE,
						"kartu_stok_obat",
						new Array()
					);
					kartu_stok_obat.setPrototipe(
						KARTU_STOK_OBAT_PNAME,
						KARTU_STOK_OBAT_PSLUG,
						KARTU_STOK_OBAT_PIMPL
					);
					$("#kartu_stok_obat_loading_modal").on("show", function() {
						$("a.close").hide();
					});
					$("#kartu_stok_obat_loading_modal").on("hide", function() {
						$("a.close").show();
					});
					$("tbody#kartu_stok_obat_list").append(
						"<tr>" +
							"<td colspan='7'><strong><center><small>DATA KARTU STOK BELUM DIPROSES</small></center></strong></td>" +
						"</tr>"
					);
					$(document).keyup(function(e) {
						if (e.which == 27) {
							FINISHED = true;
						}
					})
				});
			</script>
			<?php
		}
	}
?>