<?php 
	require_once 'smis-framework/smis/template/ModulTemplate.php';
	require_once 'smis-libs-inventory/responder/LaporanRekapPerbekalanObatDBResponder.php';
	
	class LaporanRekapPerbekalanObat extends ModulTemplate {
		private $db;
		private $name;
		private $tbl_stok_obat;
		private $tbl_obat_masuk;
		private $page;
		private $action;
		private $uitable;
		private $proto_name;
		private $proto_implement;
		private $proto_slug;
		
		public function __construct($db, $name_entity, $dbslug, $page, $entity="") {
			$this->db = $db;
			$this->name = $name_entity;
			if ($entity != "") $entity = "_" . $entity;
			$this->tbl_stok_obat = "smis_" . $dbslug . "_stok_obat" . $entity;
			$this->tbl_obat_masuk = "smis_" . $dbslug . "_obat_masuk" . $entity;
			$this->page = $page;
			$header = array("Nama Obat", "Jenis Obat", "Jumlah", "Satuan", "Harga Satuan", "Total");
			$this->uitable = new Table($header, "", null, true);
			$this->uitable->setAction(false);
			$this->uitable->setName("lrrso");
			$this->proto_name = "";
			$this->proto_implement = "";
			$this->proto_slug = "";
		}
		
		public function setPrototype($pname, $pslug, $pimplement){
			$this->proto_name = $pname;
			$this->proto_implement = $pimplement;
			$this->proto_slug = $pslug;
		}
		
		public function command($command){		
			$adapter = new SimpleAdapter();
			$adapter->add("Nama Obat", "nama_obat");
			$adapter->add("Jenis Obat", "nama_jenis_obat");
			$adapter->add("Jumlah", "sisa");
			$adapter->add("Satuan", "satuan");
			$adapter->add("Harga Satuan", "hna_ma", "money Rp.");
			$adapter->add("Total", "total", "money Rp.");
			$dbtable = new DBTable($this->db, $this->tbl_stok_obat);
			$filter = "1";
			if (isset($_POST['kriteria'])) {
				$filter .= " AND (nama_obat LIKE '%" . $_POST['kriteria'] . "%' OR nama_jenis_obat LIKE '%" . $_POST['kriteria'] . "%')";
			}
			$query_value = "
				SELECT *
				FROM (
					SELECT id_obat AS id, nama_obat, nama_jenis_obat, SUM(sisa) AS 'sisa', satuan, konversi, satuan_konversi, avg_hna AS 'hna_ma', SUM(sisa * avg_hna) AS 'total'
					FROM (
						SELECT " . $this->tbl_stok_obat . ".*, v_obat_hna.avg_hna
						FROM " . $this->tbl_stok_obat . " LEFT JOIN (
							SELECT id_obat, satuan, konversi, satuan_konversi, AVG(hna) AS 'avg_hna'
							FROM (
								SELECT DISTINCT " . $this->tbl_stok_obat . ".id_obat, " . $this->tbl_stok_obat . ".nama_obat, " . $this->tbl_stok_obat . ".satuan, " . $this->tbl_stok_obat . ".konversi, " . $this->tbl_stok_obat . ".satuan_konversi, " . $this->tbl_stok_obat . ".hna
								FROM " . $this->tbl_stok_obat . " LEFT JOIN " . $this->tbl_obat_masuk . " ON " . $this->tbl_stok_obat . ".id_obat_masuk = " . $this->tbl_obat_masuk . ".id
								WHERE " . $this->tbl_stok_obat . ".prop NOT LIKE 'del' AND " . $this->tbl_obat_masuk . ".prop NOT LIKE 'del' AND " . $this->tbl_obat_masuk . ".status = 'sudah'
							) v_obat_hna
							GROUP BY id_obat, satuan, konversi, satuan_konversi
						) v_obat_hna ON " . $this->tbl_stok_obat . ".id_obat = v_obat_hna.id_obat AND " . $this->tbl_stok_obat . ".satuan = v_obat_hna.satuan AND " . $this->tbl_stok_obat . ".konversi = v_obat_hna.konversi AND " . $this->tbl_stok_obat . ".satuan_konversi = v_obat_hna.satuan_konversi
						WHERE " . $this->tbl_stok_obat . ".prop NOT LIKE 'del'
					) v_obat_hna
					GROUP BY id_obat, satuan, konversi, satuan_konversi
				) v_stok
				WHERE " . $filter . "
			";
			$query_count = "
				SELECT COUNT(*)
				FROM (
					SELECT id_obat AS id, nama_obat, nama_jenis_obat, SUM(sisa) AS 'sisa', satuan, konversi, satuan_konversi, avg_hna AS 'hna_ma', SUM(sisa * avg_hna) AS 'total'
					FROM (
						SELECT " . $this->tbl_stok_obat . ".*, v_obat_hna.avg_hna
						FROM " . $this->tbl_stok_obat . " LEFT JOIN (
							SELECT id_obat, satuan, konversi, satuan_konversi, AVG(hna) AS 'avg_hna'
							FROM (
								SELECT DISTINCT " . $this->tbl_stok_obat . ".id_obat, " . $this->tbl_stok_obat . ".nama_obat, " . $this->tbl_stok_obat . ".satuan, " . $this->tbl_stok_obat . ".konversi, " . $this->tbl_stok_obat . ".satuan_konversi, " . $this->tbl_stok_obat . ".hna
								FROM " . $this->tbl_stok_obat . " LEFT JOIN " . $this->tbl_obat_masuk . " ON " . $this->tbl_stok_obat . ".id_obat_masuk = " . $this->tbl_obat_masuk . ".id
								WHERE " . $this->tbl_stok_obat . ".prop NOT LIKE 'del' AND " . $this->tbl_obat_masuk . ".prop NOT LIKE 'del' AND " . $this->tbl_obat_masuk . ".status = 'sudah'
							) v_obat_hna
							GROUP BY id_obat, satuan, konversi, satuan_konversi
						) v_obat_hna ON " . $this->tbl_stok_obat . ".id_obat = v_obat_hna.id_obat AND " . $this->tbl_stok_obat . ".satuan = v_obat_hna.satuan AND " . $this->tbl_stok_obat . ".konversi = v_obat_hna.konversi AND " . $this->tbl_stok_obat . ".satuan_konversi = v_obat_hna.satuan_konversi
						WHERE " . $this->tbl_stok_obat . ".prop NOT LIKE 'del'
					) v_obat_hna
					GROUP BY id_obat, satuan, konversi, satuan_konversi
				) v_stok
				WHERE " . $filter . "
			";
			$dbtable->setPreferredQuery(true, $query_value, $query_count);
			$dbresponder = new LaporanRekapPerbekalanObatDBResponder(
				$dbtable,
				$this->uitable,
				$adapter,
				$this->tbl_obat_masuk,
				$this->tbl_stok_obat,
				$this->page
			);
			$data = $dbresponder->command($_POST['command']);
			echo json_encode($data);
			return;
		}
		
		public function jsLoader(){
			echo addJS("framework/smis/js/table_action.js");
		}
		
		public function phpPreLoad(){
			$form = new Form("lrrso_form", "", "Laporan Rekap Perbekalan Obat");
			$tanggal_text = new Text("lrrso_tanggal", "lrrso_tanggal", date("d-m-Y"));
			$tanggal_text->setAtribute("disabled='disabled'");
			$form->addElement("Tanggal", $tanggal_text);
			$show_button = new Button("", "", "Tampilkan");
			$show_button->setClass("btn-primary");
			$show_button->setIcon("icon-white icon-repeat");
			$show_button->setIsButton(Button::$ICONIC);
			$show_button->setAction("lrrso.view()");
			$print_button = new Button("", "", "Cetak");
			$print_button->setClass("btn-inverse");
			$print_button->setIcon("icon-white icon-print");
			$print_button->setIsButton(Button::$ICONIC);
			$print_button->setAction("lrrso.print()");
			$btn_group = new ButtonGroup("noprint");
			$btn_group->addButton($show_button);
			$btn_group->addButton($print_button);
			$form->addElement("", $btn_group);
			
			echo $form->getHtml();
			echo $this->uitable->getHtml();
		}
		
		public function jsPreLoad(){
			?>
			<script type="text/javascript">
				function LRRSOAction(name, page, action, column)	{
					this.initialize(name, page, action, column);
				}
				LRRSOAction.prototype.constructor = LRRSOAction;
				LRRSOAction.prototype = new TableAction();
				LRRSOAction.prototype.print = function() {
					var data = this.getRegulerData();
					data['command'] = "print_laporan";
					$.post(
						"",
						data,
						function(response) {
							var json = getContent(response);
							if (json == null) return;
							smis_print(json);
						}
					);
				};
				
				var lrrso;
				var LRSSO_PAGE = "<?php echo $this->page; ?>";
				var LRSSO_PNAME = "<?php echo $this->proto_name; ?>";
				var LRSSO_PSLUG = "<?php echo $this->proto_slug; ?>";
				var LRSSO_PIMPL = "<?php echo $this->proto_implement; ?>";
				$(document).ready(function() {
					$('[data-toggle="popover"]').popover({
						trigger: 'hover',
						'placement': 'top'
					});
					lrrso = new LRRSOAction(
						"lrrso",
						LRSSO_PAGE,
						"laporan_rekap_perbekalan_obat",
						new Array()
					);
					lrrso.setPrototipe(LRSSO_PNAME, LRSSO_PSLUG, LRSSO_PIMPL);
				});
			</script>
			<?php 
		}
	}
?>