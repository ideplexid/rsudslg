<?php 
	require_once 'smis-base/smis-include-service-consumer.php';
	require_once 'smis-framework/smis/template/ModulTemplate.php';
	require_once 'smis-libs-inventory/responder/BarangKSBDBResponder.php';
	
	class KartuStokBarang extends ModulTemplate {
		private $db;
		private $name;
		private $tbl_kartu_stok_barang;
		private $tbl_stok_barang;
		private $tbl_barang_masuk;
		private $page;
		private $action;
		private $kso_form;
		private $kso_table;
		private $kso_loading_bar;
		private $kso_loading_modal;
		private $proto_name;
		private $proto_implement;
		private $proto_slug;
		
		public function __construct($db, $name, $tbl_kartu_stok_barang, $tbl_stok_barang, $tbl_barang_masuk, $page) {
			$this->db = $db;
			$this->name = $name;
			$this->tbl_kartu_stok_barang = $tbl_kartu_stok_barang;
			$this->tbl_stok_barang = $tbl_stok_barang;
			$this->tbl_barang_masuk = $tbl_barang_masuk;
			$this->page = $page;
			$this->kso_form = new Form("kartu_stok_barang_form", "", $this->name . " : Kartu Stok Barang");
			$id_barang_text = new Text("kartu_stok_barang_id_barang", "kartu_stok_barang_id_barang", "");
			$id_barang_text->setClass("smis-one-option-input");
			$id_barang_text->setAtribute("disabled='disabled'");
			$browse_button = new Button("", "", "Pilih");
			$browse_button->setClass("btn-info");
			$browse_button->setIsButton(Button::$ICONIC);
			$browse_button->setIcon("icon-white ".Button::$icon_list_alt);
			$browse_button->setAction("barang_kso.chooser('barang_kso', 'barang_kso_button', 'barang_kso', barang_kso)");
			$input_group = new InputGroup("");
			$input_group->addComponent($id_barang_text);
			$input_group->addComponent($browse_button);
			$this->kso_form->addElement("ID Barang", $input_group);
			$kode_barang_text = new Text("kartu_stok_barang_kode_barang", "kartu_stok_barang_kode_barang", "");
			$kode_barang_text->setAtribute("disabled='disabled'");
			$this->kso_form->addElement("Kode Barang", $kode_barang_text);
			$nama_barang_text = new Text("kartu_stok_barang_nama_barang", "kartu_stok_barang_nama_barang", "");
			$nama_barang_text->setAtribute("disabled='disabled'");
			$this->kso_form->addElement("Nama Barang", $nama_barang_text);
			$jenis_barang_text = new Text("kartu_stok_barang_jenis_barang", "kartu_stok_barang_jenis_barang", "");
			$jenis_barang_text->setAtribute("disabled='disabled'");
			$this->kso_form->addElement("Jenis Barang", $jenis_barang_text);
			$satuan_text = new Text("kartu_stok_barang_satuan", "kartu_stok_barang_satuan", "");
			$satuan_text->setAtribute("disabled='disabled'");
			$this->kso_form->addElement("Satuan", $satuan_text);
			$tanggal_from_text = new Text("kartu_stok_barang_tanggal_from", "kartu_stok_barang_tanggal_from", date("Y-m-") . "01");
			$tanggal_from_text->setClass("mydate");
			$tanggal_from_text->setAtribute("data-date-format='yyyy-mm-dd'");
			$this->kso_form->addElement("Tanggal Awal", $tanggal_from_text);
			$tanggal_to_text = new Text("kartu_stok_barang_tanggal_to", "kartu_stok_barang_tanggal_to", date("Y-m-d"));
			$tanggal_to_text->setClass("mydate");
			$tanggal_to_text->setAtribute("data-date-format='yyyy-mm-dd'");
			$this->kso_form->addElement("Tanggal Akhir", $tanggal_to_text);
			$show_button = new Button("", "", "Tampilkan");
			$show_button->setClass("btn-primary");
			$show_button->setIcon("icon-white icon-repeat");
			$show_button->setIsButton(Button::$ICONIC);
			$show_button->setAction("kartu_stok_barang.view()");
			$download_button = new Button("", "", "Eksport XLS");
			$download_button->setClass("btn-inverse");
			$download_button->setIcon("fa fa-download");
			$download_button->setIsButton(Button::$ICONIC);
			$download_button->setAtribute("id='download_button'");
			$button_group = new ButtonGroup("noprint");
			$button_group->addElement($show_button);
			$button_group->addElement($download_button);
			$this->kso_form->addElement("", $button_group);
			$header = array("No.", "Tanggal", "No. Bon", "Dipakai untuk / Diterima dari", "Masuk", "Keluar", "Sisa");
			$this->kso_table = new Table($header);
			$this->kso_table->setName("kartu_stok_barang");
			$this->kso_table->setAction(false);
			$this->kso_table->setFooterVisible(false);
			$this->proto_name = "";
			$this->proto_implement = "";
			$this->proto_slug = "";

			$this->kso_loading_bar = new LoadingBar("kartu_stok_barang_loading_bar", "");
			$button = new Button("", "", "Batal");
			$button->addClass("btn-primary");
			$button->setIsButton(Button::$ICONIC_TEXT);
			$button->setIcon("fa fa-close");
			$button->setAction("kartu_stok_barang.cancel()");
			$this->kso_loading_modal = new Modal("kartu_stok_barang_loading_modal", "", "Proses..");
			$this->kso_loading_modal->addHtml($this->kso_loading_bar->getHtml(), "after");
			$this->kso_loading_modal->addFooter($button);
		}
		
		public function setPrototype($proto_name, $proto_slug, $proto_implement) {
			$this->proto_name = $proto_name;
			$this->proto_slug = $proto_slug;
			$this->proto_implement = $proto_implement;
		}
		
		public function superCommand($super_command) {			
			$barang_table = new Table(
				array("ID Barang", "Kode Barang", "Nama Barang", "Jenis Barang"),
				"",
				null,
				true
			);
			$barang_table->setName("barang_kso");
			$barang_table->setModel(Table::$SELECT);
			$barang_adapter = new SimpleAdapter();
			$barang_adapter->add("ID Barang", "id", "digit8");
			$barang_adapter->add("Kode Barang", "kode_barang");
			$barang_adapter->add("Nama Barang", "nama_barang");
			$barang_adapter->add("Jenis Barang", "nama_jenis_barang");
			$barang_dbtable = new DBTable($this->db, $this->tbl_stok_barang);
			$filter = "1";
			if (isset($_POST['kriteria'])) {
				$filter .= " AND (id LIKE '%" . $_POST['kriteria'] . "%' OR kode_barang LIKE '%" . $_POST['kriteria'] . "%' OR nama_barang LIKE '%" . $_POST['kriteria'] . "%' OR nama_jenis_barang LIKE '%" . $_POST['kriteria'] . "%') ";
			}
			$query_value = "
				SELECT *
				FROM (
					SELECT DISTINCT a.id_barang AS 'id', a.kode_barang, a.nama_barang, a.nama_jenis_barang
					FROM " . $this->tbl_stok_barang . " a LEFT JOIN " . $this->tbl_barang_masuk . " b ON a.id_barang_masuk = b.id
					WHERE a.prop NOT LIKE 'del' AND b.prop NOT LIKE 'del' AND b.status = 'sudah'
					ORDER BY kode_barang, nama_jenis_barang, nama_barang ASC
				) v
				WHERE " . $filter . "
			";
			$query_count = "
				SELECT COUNT(*)
				FROM (
					" . $query_value . "
				) v
			";
			$barang_dbtable->setPreferredQuery(true, $query_value, $query_count);
			$barang_dbresponder = new BarangKSBDBResponder(
				$barang_dbtable,
				$barang_table,
				$barang_adapter,
				$this->tbl_stok_barang,
				$this->tbl_barang_masuk
			);
			
			$super_command = new SuperCommand();
			$super_command->addResponder("barang_kso", $barang_dbresponder);
			$init = $super_command->initialize();
			if ($init != null) {
				echo $init;
				return;
			}
		}

		public function command($command) {
			if (isset($_POST['command'])) {
				if ($_POST['command'] == "get_jumlah_sisa") {
					$tanggal_from = $_POST['tanggal_from'];
					$tanggal_to = $_POST['tanggal_to'];
					$id_barang = $_POST['id_barang'] = $_POST['id_barang'];
					$dbtable = new DBTable($this->db, $this->tbl_kartu_stok_barang);
					$row = $dbtable->get_row("
						SELECT COUNT(*) AS 'jumlah'
						FROM " . $this->tbl_kartu_stok_barang . "
						WHERE tanggal >= '" . $tanggal_from . "' AND tanggal <= '" . date('Y-m-d') . "' AND id_barang = '" . $id_barang . "' AND prop NOT LIKE 'del'
					");
					$jumlah = 0;
					if ($row != null)
						$jumlah = $row->jumlah;
					$row = $dbtable->get_row("
						SELECT SUM(a.sisa) AS 'sisa'
						FROM " . $this->tbl_stok_barang . " a LEFT JOIN " . $this->tbl_barang_masuk . " b ON a.id_barang_masuk = b.id
						WHERE b.status = 'sudah' AND a.id_barang = '" . $id_barang . "'
					");
					$sisa = 0;
					if ($row != null)
						$sisa = $row->sisa;
					$data = array(
						"jumlah"	=> $jumlah,
						"sisa"		=> $sisa
					);
					echo json_encode($data);
					return;
				} else if ($_POST['command'] == "get_info") {
					$tanggal_from = $_POST['tanggal_from'];
					$tanggal_to = $_POST['tanggal_to'];
					$id_barang = $_POST['id_barang'] = $_POST['id_barang'];
					$num = $_POST['num'];
					$sisa = $_POST['sisa'];
					$dbtable = new DBTable($this->db, $this->tbl_kartu_stok_barang);
					$row = $dbtable->get_row("
						SELECT *
						FROM " . $this->tbl_kartu_stok_barang . "
						WHERE tanggal >= '" . $tanggal_from . "' AND tanggal <= '" . date('Y-m-d') . "' AND id_barang = '" . $id_barang . "' AND prop NOT LIKE 'del'
						ORDER BY tanggal DESC, id DESC
						LIMIT " . $num . ", 1
					");
					$html = "";
					$nama_barang = "";
					$tanggal = "";
					$no_bon = "";
					if ($row != null) {
						if (strtotime($row->tanggal) >= strtotime($tanggal_from) && strtotime($row->tanggal) <= strtotime($tanggal_to)) {
							$html = "
								<tr>
									<td id='nomor'></td>
									<td id='tanggal'>" . ArrayAdapter::format("date d-m-Y", $row->tanggal) . "</td>
									<td id='no_bon'>" . $row->no_bon . "</td>
									<td id='unit'>" . $row->unit . "</td>
									<td id='f_masuk'>" . ArrayAdapter::format("number", $row->masuk) . "</td>
									<td id='masuk' style='display: none;'>" . $row->masuk . "</td>
									<td id='f_keluar'>" . ArrayAdapter::format("number", $row->keluar) . "</td>
									<td id='keluar' style='display: none;'>" . $row->keluar . "</td>
									<td id='f_sisa'>" . ArrayAdapter::format("number", $sisa) . "</td>
									<td id='sisa' style='display: none;'>" . $sisa . "</td>
								</tr>
							";
						}
						$masuk = $row->masuk;
						$keluar = $row->keluar;
						$no_bon = $row->no_bon;
						$tanggal = $row->tanggal;
						$nama_barang = $row->nama_barang;
					}
					$data = array(
						"html" 		=> $html,
						"id_barang"	=> ArrayAdapter::format("only-digit6", $id_barang),
						"nama_barang"	=> $nama_barang,
						"tanggal"	=> ArrayAdapter::format("date d-m-Y", $tanggal),
						"no_bon"	=> $no_bon,
						"sisa"		=> $sisa - $masuk + $keluar
					);
					echo json_encode($data);
					return;
				} else if ($_POST['command'] == "export_excel") {
					require_once("smis-libs-out/php-excel/PHPExcel.php");
					
					$objPHPExcel = PHPExcel_IOFactory::load("smis-libs-inventory/templates/template_kartu_stok_barang.xlsx");
					$objPHPExcel->setActiveSheetIndexByName("KARTU STOK BARANG");
					$objWorksheet = $objPHPExcel->getActiveSheet();
					
					$id_barang = $_POST['id_barang'];
					$kode_barang = $_POST['kode_barang'];
					if (substr($kode_barang, 0, 3) == "REG")
						$kode_barang = "309" . substr($kode_barang, 3);
					else if (substr($kode_barang, 0, 3) == "JKN")
						$kode_barang = "319" . substr($kode_barang, 3);
					$nama_barang = $_POST['nama_barang'];
					$nama_jenis_barang = $_POST['nama_jenis_barang'];
					$tanggal_from = $_POST['tanggal_from'];
					$tanggal_to = $_POST['tanggal_to'];
					$objWorksheet->setCellValue("B5", strtoupper($this->name));
					$objWorksheet->setCellValue("E5", ": " . $kode_barang);
					$objWorksheet->setCellValue("H3", ": " . $kode_barang);
					$objWorksheet->setCellValue("H4", ": " . $nama_barang);
					$objWorksheet->setCellValue("H5", ": 0");
					
					$data = json_decode($_POST['d_data']);
					if ($_POST['num_rows'] - 2 > 0)
						$objWorksheet->insertNewRowBefore(10, $_POST['num_rows'] - 2);
					$start_row_num = 10;
					$end_row_num = 10;
					$row_num = $start_row_num;
					foreach ($data as $d) {
						$col_num = 1;
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->nomor);
						$col_num++;
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->tanggal);
						$col_num++;
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->unit);
						$col_num+=2;
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->masuk);
						$col_num++;
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->keluar);
						$col_num++;
						$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->sisa);
						$objWorksheet->getStyle("F" . $row_num)->getNumberFormat()->setFormatCode("#,##0");
						$objWorksheet->getStyle("G" . $row_num)->getNumberFormat()->setFormatCode("#,##0");
						$objWorksheet->getStyle("H" . $row_num)->getNumberFormat()->setFormatCode("#,##0");
						$row_num++;
						$end_row_num++;
					}
					
					header("Content-type: application/vnd.ms-excel");	
					header("Content-Disposition: attachment; filename=KARTU_STOK_BARANG-" . strtoupper($this->name) . "-" . $kode_barang . "_" . ArrayAdapter::format("only-digit6", $id_barang) . "_" . date("Ymd_his") . ".xlsx");
					$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");
					$objWriter->save("php://output");			
					return;
				}
				return;
			}
		}
		
		public function phpPreLoad() {
			echo $this->kso_loading_modal->getHtml();
			echo $this->kso_form->getHtml();
			echo "<div id='table_content'>";
			echo $this->kso_table->getHtml();
			echo "</div>";
			echo "<div id='kartu_stok_barang_info'></div>";
			echo addJS("framework/smis/js/table_action.js");
			echo addJS("framework/bootstrap/js/bootstrap-datepicker.js");
			echo addJS("base-js/smis-base-loading.js");
			echo addCSS("framework/bootstrap/css/datepicker.css");
		}
		
		public function jsPreLoad() {
			?>
			<script type="text/javascript">
				function KartuStokBarangAction(name, page, action, column) {
					this.initialize(name, page, action, column);
				}
				KartuStokBarangAction.prototype.constructor = KartuStokBarangAction;
				KartuStokBarangAction.prototype = new TableAction();
				KartuStokBarangAction.prototype.getRegulerData = function() {
					var data = TableAction.prototype.getRegulerData.call(this);
					data['id_barang'] = $("#kartu_stok_barang_id_barang").val();
					data['kode_barang'] = $("#kartu_stok_barang_kode_barang").val();
					data['nama_barang'] = $("#kartu_stok_barang_nama_barang").val();
					data['jenis_barang'] = $("#kartu_stok_barang_jenis_barang").val();
					data['satuan'] = $("#kartu_stok_barang_satuan").val();
					if ($("#kartu_stok_barang_tanggal_from").val() != "" && $("#kartu_stok_barang_tanggal_to").val() != "") {
						data['tanggal_from'] = $("#kartu_stok_barang_tanggal_from").val();
						data['tanggal_to'] = $("#kartu_stok_barang_tanggal_to").val();
					}
					return data;
				};
				KartuStokBarangAction.prototype.view = function() {
					if ($("#kartu_stok_barang_id_barang").val() == "")
						return;
					var self = this;
					$("#info").empty();
					$("#kartu_stok_barang_loading_bar").sload("true", "Harap ditunggu...", 0);
					$("#kartu_stok_barang_loading_modal").smodal("show");
					FINISHED = false;
					var data = this.getRegulerData();
					data['command'] = "get_jumlah_sisa";
					$.post(
						"",
						data,
						function(response) {
							var json = JSON.parse(response);
							if (json == null) return;
							$("#kartu_stok_barang_list").empty();
							self.fill_html(0, json.jumlah, json.sisa);
						}
					);
				};
				KartuStokBarangAction.prototype.fill_html = function(num, limit, sisa) {
					if (num == limit || FINISHED) {
						if (FINISHED == false && num == limit) {
							this.finalize();
						} else {
							$("#kartu_stok_barang_loading_modal").smodal("hide");
							$("#kartu_stok_barang_info").html(
								"<div class='alert alert-block alert-inverse'>" +
									 "<center><strong>PROSES DIBATALKAN</strong></center>" +
								 "</div>"
							);
							$("#download_button").removeAttr("onclick");
						}
						return;
					}
					var self = this;
					var data = this.getRegulerData();
					data['command'] = "get_info";
					data['num'] = num;
					data['sisa'] = sisa;
					$.post(
						"",
						data,
						function(response) {
							var json = JSON.parse(response);
							if (json == null) return;
							var html = $("#kartu_stok_barang_list").html();
							$("#kartu_stok_barang_list").html(json.html + html);
							$("#kartu_stok_barang_loading_bar").sload("true", json.id_barang + " - " + json.nama_barang + " - TANGGAL : " + json.tanggal + "- NO. BON : " + json.no_bon + " (" + (num + 1) + " / " + limit + ")", (num + 1) * 100 / limit - 1);
							self.fill_html(num + 1, limit, json.sisa);
						}
					);
				};
				KartuStokBarangAction.prototype.finalize = function() {
					var num_rows = $("tbody#kartu_stok_barang_list tr").length;
					for (var i = 0; i < num_rows; i++)
						$("tbody#kartu_stok_barang_list tr:eq(" + i + ") td#nomor").html(i + 1);
					$("#kartu_stok_barang_loading_modal").smodal("hide");
					$("#info").html(
						"<div class='alert alert-block alert-info'>" +
							 "<center><strong>PROSES SELESAI</strong></center>" +
						 "</div>"
					);
					$("#download_button").removeAttr("onclick");
					$("#download_button").attr("onclick", "kartu_stok_barang.export_xls()");
				};
				KartuStokBarangAction.prototype.export_xls = function() {
					showLoading();
					var num_rows = $("#kartu_stok_barang_list").children("tr").length;
					var d_data = {};
					for (var i = 0; i < num_rows; i++) {
						var nomor = $("tbody#kartu_stok_barang_list tr:eq(" + i + ") td#nomor").text();
						var tanggal = $("tbody#kartu_stok_barang_list tr:eq(" + i + ") td#tanggal").text();
						var no_bon = $("tbody#kartu_stok_barang_list tr:eq(" + i + ") td#no_bon").text();
						var unit = $("tbody#kartu_stok_barang_list tr:eq(" + i + ") td#unit").text();
						var masuk = $("tbody#kartu_stok_barang_list tr:eq(" + i + ") td#masuk").text();
						var keluar = $("tbody#kartu_stok_barang_list tr:eq(" + i + ") td#keluar").text();
						var sisa = $("tbody#kartu_stok_barang_list tr:eq(" + i + ") td#sisa").text();
						d_data[i] = {
							"nomor" 	: nomor,
							"tanggal" 	: tanggal,
							"no_bon" 	: no_bon,
							"unit" 		: unit,
							"masuk"		: masuk,
							"keluar"	: keluar,
							"sisa"		: sisa
						};
					}
					var data = this.getRegulerData();
					data['command'] = "export_excel";
					data['d_data'] = JSON.stringify(d_data);
					data['num_rows'] = num_rows;
					postForm(data);
					dismissLoading();
				};
				
				function BarangKSBAction(name, page, action, column) {
					this.initialize(name, page, action, column);
				}
				BarangKSBAction.prototype.constructor = BarangKSBAction;
				BarangKSBAction.prototype = new TableAction();
				BarangKSBAction.prototype.selected = function(json) {
					$("#kartu_stok_barang_id_barang").val(json.id);
					$("#kartu_stok_barang_kode_barang").val(json.kode_barang);
					$("#kartu_stok_barang_nama_barang").val(json.nama_barang);
					$("#kartu_stok_barang_jenis_barang").val(json.nama_jenis_barang);
					$("#kartu_stok_barang_satuan").val(json.satuan);
				};
				
				var kartu_stok_barang;
				var barang_kso;
				var KARTU_STOK_BARANG_PAGE = "<?php echo $this->page; ?>";
				var KARTU_STOK_BARANG_PNAME = "<?php echo $this->proto_name; ?>";
				var KARTU_STOK_BARANG_PSLUG = "<?php echo $this->proto_slug; ?>";
				var KARTU_STOK_BARANG_PIMPL = "<?php echo $this->proto_implement; ?>";
				$(document).ready(function() {
					$('[data-toggle="popover"]').popover({
						trigger: 'hover',
						'placement': 'top'
					});
					$("mydate").datepicker();
					$("#smis-chooser-modal").on("show", function() {
						$("#smis-chooser-modal").removeClass("full_model");
						$("#smis-chooser-modal").addClass("full_model");
					});
					$("#smis-chooser-modal").on("hidden", function() {
						$("#smis-chooser-modal").removeClass("full_model");
					});
					barang_kso = new BarangKSBAction(
						"barang_kso",
						KARTU_STOK_BARANG_PAGE,
						"kartu_stok_barang",
						new Array()
					);
					barang_kso.setSuperCommand("barang_kso");
					barang_kso.setPrototipe(
						KARTU_STOK_BARANG_PNAME,
						KARTU_STOK_BARANG_PSLUG,
						KARTU_STOK_BARANG_PIMPL
					);
					kartu_stok_barang = new KartuStokBarangAction(
						"kartu_stok_barang",
						KARTU_STOK_BARANG_PAGE,
						"kartu_stok_barang",
						new Array()
					);
					kartu_stok_barang.setPrototipe(
						KARTU_STOK_BARANG_PNAME,
						KARTU_STOK_BARANG_PSLUG,
						KARTU_STOK_BARANG_PIMPL
					);
					$("#kartu_stok_barang_loading_modal").on("show", function() {
						$("a.close").hide();
					});
					$("#kartu_stok_barang_loading_modal").on("hide", function() {
						$("a.close").show();
					});
					$("tbody#kartu_stok_barang_list").append(
						"<tr>" +
							"<td colspan='7'><strong><center><small>DATA KARTU STOK BELUM DIPROSES</small></center></strong></td>" +
						"</tr>"
					);
					$(document).keyup(function(e) {
						if (e.which == 27) {
							FINISHED = true;
						}
					})
				});
			</script>
			<?php
		}
	}
?>