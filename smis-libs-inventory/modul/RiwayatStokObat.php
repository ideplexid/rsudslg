<?php 
	require_once 'smis-base/smis-include-service-consumer.php';
	require_once 'smis-framework/smis/template/ModulTemplate.php';
	require_once 'smis-libs-inventory/adapter/StokObatAdapter.php';
	require_once 'smis-libs-inventory/responder/RiwayatStokObatDBResponder.php';
	
	class RiwayatStokObat extends ModulTemplate {
		private $db;
		private $name;
		private $tbl_riwayat_stok_obat;
		private $tbl_stok_obat;
		private $tbl_obat_masuk;
		private $page;
		private $action;
		private $rso_form;
		private $rso_table;
		private $proto_name;
		private $proto_implement;
		private $proto_slug;
		
		public function __construct($db, $name, $tbl_riwayat_stok_obat, $tbl_stok_obat, $tbl_obat_masuk, $page) {
			$this->db = $db;
			$this->name = $name;
			$this->tbl_riwayat_stok_obat = $tbl_riwayat_stok_obat;
			$this->tbl_stok_obat = $tbl_stok_obat;
			$this->tbl_obat_masuk = $tbl_obat_masuk;
			$this->page = $page;
			$this->rso_form = new Form("riwayat_stok_obat_form", "", $this->name . " : Riwayat Stok Obat");
			$id_stok_obat_text = new Text("riwayat_stok_obat_id_stok_obat", "riwayat_stok_obat_id_stok_obat", "");
			$id_stok_obat_text->setClass("smis-one-option-input");
			$id_stok_obat_text->setAtribute("disabled='disabled'");
			$browse_button = new Button("", "", "Pilih");
			$browse_button->setClass("btn-info");
			$browse_button->setIsButton(Button::$ICONIC);
			$browse_button->setIcon("icon-white ".Button::$icon_list_alt);
			$browse_button->setAction("stok_obat.chooser('stok_obat', 'stok_obat_button', 'stok_obat', stok_obat)");
			$input_group = new InputGroup("");
			$input_group->addComponent($id_stok_obat_text);
			$input_group->addComponent($browse_button);
			$this->rso_form->addElement("No. Stok", $input_group);
			$nama_obat_text = new Text("riwayat_stok_obat_nama_obat", "riwayat_stok_obat_nama_obat", "");
			$nama_obat_text->setAtribute("disabled='disabled'");
			$this->rso_form->addElement("Nama Obat", $nama_obat_text);
			$jenis_obat_text = new Text("riwayat_stok_obat_jenis_obat", "riwayat_stok_obat_jenis_obat", "");
			$jenis_obat_text->setAtribute("disabled='disabled'");
			$this->rso_form->addElement("Jenis Obat", $jenis_obat_text);
			$jenis_stok = new Text("riwayat_stok_obat_jenis_stok", "riwayat_stok_obat_jenis_stok", "");
			$jenis_stok->setAtribute("disabled='disabled'");
			$this->rso_form->addElement("Jenis Stok", $jenis_stok);
			$produsen_text = new Text("riwayat_stok_obat_produsen", "riwayat_stok_obat_produsen", "");
			$produsen_text->setAtribute("disabled='disabled'");
			$this->rso_form->addElement("Produsen", $produsen_text);
			$vendor_text = new Text("riwayat_stok_obat_vendor", "riwayat_stok_obat_vendor", "");
			$vendor_text->setAtribute("disabled='disabled'");
			$this->rso_form->addElement("Vendor", $vendor_text);
			$satuan_text = new Text("riwayat_stok_obat_satuan", "riwayat_stok_obat_satuan", "");
			$satuan_text->setAtribute("disabled='disabled'");
			$this->rso_form->addElement("Satuan", $satuan_text);
			$ed_text = new Text("riwayat_stok_obat_ed", "riwayat_stok_obat_ed", "");
			$ed_text->setAtribute("disabled='disabled'");
			$this->rso_form->addElement("Tgl. ED", $ed_text);
			$show_button = new Button("", "", "Tampilkan");
			$show_button->setClass("btn-primary");
			$show_button->setIcon("icon-white icon-repeat");
			$show_button->setIsButton(Button::$ICONIC);
			$show_button->setAction("riwayat_stok_obat.view()");
			$print_button = new Button("", "", "Cetak");
			$print_button->setClass("btn-inverse");
			$print_button->setIcon("icon-white icon-print");
			$print_button->setIsButton(Button::$ICONIC);
			$print_button->setAction("riwayat_stok_obat.print()");
			$button_group = new ButtonGroup("noprint");
			$button_group->addElement($show_button);
			$button_group->addElement($print_button);
			$this->rso_form->addElement("", $button_group);
			$header = array("Nomor", "Tanggal", "Masuk", "Keluar", "Sisa", "Keterangan", "Petugas Entri");
			$this->rso_table = new Table($header);
			$this->rso_table->setName("riwayat_stok_obat");
			$this->rso_table->setAction(false);
			$this->proto_name = "";
			$this->proto_implement = "";
			$this->proto_slug = "";
		}
		
		public function setPrototype($proto_name, $proto_slug, $proto_implement) {
			$this->proto_name = $proto_name;
			$this->proto_slug = $proto_slug;
			$this->proto_implement = $proto_implement;
		}
		
		public function superCommand($super_command) {
			$rso_adapter = new SimpleAdapter();
			$rso_adapter->add("Nomor", "id", "digit8");
			$rso_adapter->add("Tanggal", "tanggal", "date d M Y");
			$rso_adapter->add("Masuk", "jumlah_masuk", "front +");
			$rso_adapter->add("Keluar", "jumlah_keluar", "front -");
			$rso_adapter->add("Sisa", "sisa");
			$rso_adapter->add("Keterangan", "keterangan");
			$rso_adapter->add("Petugas Entri", "nama_user");
			$rso_dbtable = new DBTable($this->db, $this->tbl_riwayat_stok_obat);
			if (isset($_POST['super_command']) && $_POST['super_command'] == "riwayat_stok_obat") {
				if (isset($_POST['command'])) {
					$filter = "";
					if (isset($_POST['kriteria'])) {
						$filter = " AND  (keterangan LIKE '%" . $_POST['kriteria'] . "%' OR nama_user LIKE '%" . $_POST['kriteria'] . "%') ";
					}
					$query_value = "
						SELECT *
						FROM " . $this->tbl_riwayat_stok_obat . "
						WHERE id_stok_obat = '" . $_POST['id_stok_obat'] . "' " . $filter . "
					";
					$query_count = "
						SELECT COUNT(*)
						FROM " . $this->tbl_riwayat_stok_obat . "
						WHERE id_stok_obat = '" . $_POST['id_stok_obat'] . "' " . $filter . "
					";
					$rso_dbtable->setPreferredQuery(true, $query_value, $query_count);
				}
			}
			$rso_dbresponder = new RiwayatStokObatDBResponder(
				$rso_dbtable,
				$this->rso_table,
				$rso_adapter,
				$this->page
			);
			
			$stok_obat_table = new Table(
				array("No. Stok", "No. Mutasi", "Tgl. Masuk", "Nama Obat", "Jenis Obat", "Jenis Stok", "Produsen", "Vendor", "Satuan", "Tgl. ED"),
				"",
				null,
				true
			);
			$stok_obat_table->setName("stok_obat");
			$stok_obat_table->setModel(Table::$SELECT);
			$stok_obat_adapter = new StokObatAdapter();
			$columns = array("id", "nama_obat", "nama_jenis_obat", "label", "produsen", "nama_vendor", "satuan", "tanggal_exp");
			$stok_obat_dbtable = new DBTable($this->db, $this->tbl_stok_obat, $columns);
			$filter = "";
			if (isset($_POST['kriteria'])) {
				$filter = " AND (" . $this->tbl_stok_obat . ".nama_obat LIKE '%" . $_POST['kriteria'] . "%' OR ".$this->tbl_stok_obat.".nama_jenis_obat LIKE '%" . $_POST['kriteria'] . "%' OR " . $this->tbl_stok_obat . ".produsen LIKE '%" . $_POST['kriteria'] . "%' OR " . $this->tbl_stok_obat . ".nama_vendor LIKE '%" . $_POST['kriteria'] . "%') ";
			}
			$query_value = "
				SELECT " . $this->tbl_stok_obat . ".*, " . $this->tbl_obat_masuk . ".tanggal AS 'tanggal_masuk', " . $this->tbl_obat_masuk . ".f_id AS 'no_mutasi'
				FROM " . $this->tbl_stok_obat . " LEFT JOIN " . $this->tbl_obat_masuk . " ON " . $this->tbl_stok_obat . ".id_obat_masuk = " . $this->tbl_obat_masuk . ".id
				WHERE " . $this->tbl_stok_obat . ".prop NOT LIKE 'del' AND " . $this->tbl_obat_masuk . ".prop NOT LIKE 'del' AND " . $this->tbl_obat_masuk . ".status = 'sudah' " . $filter . "
			";
			$query_count = "
				SELECT COUNT(*)
				FROM " . $this->tbl_stok_obat . " LEFT JOIN " . $this->tbl_obat_masuk . " ON " . $this->tbl_stok_obat . ".id_obat_masuk = " . $this->tbl_obat_masuk . ".id
				WHERE " . $this->tbl_stok_obat . ".prop NOT LIKE 'del' AND " . $this->tbl_obat_masuk . ".prop NOT LIKE 'del' AND " . $this->tbl_obat_masuk . ".status = 'sudah' " . $filter . "
			";
			$stok_obat_dbtable->setPreferredQuery(true, $query_value, $query_count);
			$stok_obat_dbresponder = new DBResponder(
				$stok_obat_dbtable,
				$stok_obat_table,
				$stok_obat_adapter
			);
			
			$super_command = new SuperCommand();
			$super_command->addResponder("riwayat_stok_obat", $rso_dbresponder);
			$super_command->addResponder("stok_obat", $stok_obat_dbresponder);
			$init = $super_command->initialize();
			if ($init != null) {
				echo $init;
				return;
			}
		}
		
		public function phpPreLoad() {
			echo $this->rso_form->getHtml();
			echo "<div id='table_content'>";
			echo $this->rso_table->getHtml();
			echo "</div>";
			echo addJS("framework/smis/js/table_action.js");
		}
		
		public function jsPreLoad() {
			?>
			<script type="text/javascript">
				function RiwayatStokObatAction(name, page, action, column) {
					this.initialize(name, page, action, column);
				}
				RiwayatStokObatAction.prototype.constructor = RiwayatStokObatAction;
				RiwayatStokObatAction.prototype = new TableAction();
				RiwayatStokObatAction.prototype.getViewData = function() {
					var data = TableAction.prototype.getViewData.call(this);
					data['id_stok_obat'] = $("#riwayat_stok_obat_id_stok_obat").val();
					return data;
				};
				RiwayatStokObatAction.prototype.view = function() {
					showLoading();
					var self=this;

					var view_data=this.getViewData();
					$.post('',view_data,function(res){
						var json=getContent(res);
						if(json==null) {
							return;
						}
						$("#"+self.prefix+"_list").html(json.list);
						$("#"+self.prefix+"_pagination").html(json.pagination);		
						dismissLoading();
					});
				};
				RiwayatStokObatAction.prototype.print = function() {
					if ($("#riwayat_stok_obat_id_stok_obat").val() == "")
						return;
					var data = this.getRegulerData();
					data['command'] = "print_stock_card";
					data['no_stok'] = $("#riwayat_stok_obat_id_stok_obat").val();
					data['nama_obat'] = $("#riwayat_stok_obat_nama_obat").val();
					data['jenis_obat'] = $("#riwayat_stok_obat_jenis_obat").val();
					data['jenis_stok'] = $("#riwayat_stok_obat_jenis_stok").val();
					data['produsen'] = $("#riwayat_stok_obat_produsen").val();
					data['vendor'] = $("#riwayat_stok_obat_vendor").val();
					data['satuan'] = $("#riwayat_stok_obat_satuan").val();
					data['tanggal_exp'] = $("#riwayat_stok_obat_ed").val();
					$.post(
						"",
						data,
						function(response) {
							var json = getContent(response);
							if (json == null) return;
							smis_print(json);
						}
					);
				};
				
				function StokObatAction(name, page, action, column) {
					this.initialize(name, page, action, column);
				}
				StokObatAction.prototype.constructor = StokObatAction;
				StokObatAction.prototype = new TableAction();
				StokObatAction.prototype.selected = function(json) {
					$("#riwayat_stok_obat_id_stok_obat").val(json.id);
					$("#riwayat_stok_obat_nama_obat").val(json.nama_obat);
					$("#riwayat_stok_obat_jenis_obat").val(json.nama_jenis_obat);
					var label = json.label.replace("_", " ").toUpperCase();
					$("#riwayat_stok_obat_jenis_stok").val(label);
					$("#riwayat_stok_obat_produsen").val(json.produsen);
					$("#riwayat_stok_obat_vendor").val(json.nama_vendor);
					$("#riwayat_stok_obat_satuan").val(json.satuan);
					if (json.tanggal_exp == "0000-00-00")
						$("#riwayat_stok_obat_ed").val("-");
					else
						$("#riwayat_stok_obat_ed").val(json.tanggal_exp);
					riwayat_stok_obat.view();
				};
				
				var riwayat_stok_obat;
				var stok_obat;
				var RIWAYAT_STOK_BARANG_PAGE="<?php echo $this->page; ?>";
				var RIWAYAT_STOK_BARANG_PNAME="<?php echo $this->proto_name; ?>";
				var RIWAYAT_STOK_BARANG_PSLUG="<?php echo $this->proto_slug; ?>";
				var RIWAYAT_STOK_BARANG_PIMPL="<?php echo $this->proto_implement; ?>";
				$(document).ready(function() {
					$('[data-toggle="popover"]').popover({
						trigger: 'hover',
						'placement': 'top'
					});
					$("#smis-chooser-modal").on("show", function() {
						$("#smis-chooser-modal").removeClass("full_model");
						$("#smis-chooser-modal").addClass("full_model");
					});
					$("#smis-chooser-modal").on("hidden", function() {
						$("#smis-chooser-modal").removeClass("full_model");
					});
					stok_obat = new StokObatAction(
						"stok_obat",
						RIWAYAT_STOK_BARANG_PAGE,
						"riwayat_stok_obat",
						new Array()
					);
					stok_obat.setSuperCommand("stok_obat");
					stok_obat.setPrototipe(
						RIWAYAT_STOK_BARANG_PNAME,
						RIWAYAT_STOK_BARANG_PSLUG,
						RIWAYAT_STOK_BARANG_PIMPL
					);
					riwayat_stok_obat = new RiwayatStokObatAction(
						"riwayat_stok_obat",
						RIWAYAT_STOK_BARANG_PAGE,
						"riwayat_stok_obat",
						new Array()
					);
					riwayat_stok_obat.setSuperCommand("riwayat_stok_obat");
					riwayat_stok_obat.setPrototipe(
						RIWAYAT_STOK_BARANG_PNAME,
						RIWAYAT_STOK_BARANG_PSLUG,
						RIWAYAT_STOK_BARANG_PIMPL
					);
					riwayat_stok_obat.view();
				});
			</script>
			<?php
		}
	}
?>