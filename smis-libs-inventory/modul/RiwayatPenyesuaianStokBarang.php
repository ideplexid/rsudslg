<?php 
	require_once 'smis-framework/smis/template/ModulTemplate.php';
	require_once 'smis-libs-inventory/adapter/RiwayatPenyesuaianStokBarangAdapter.php';

	class RiwayatPenyesuaianStokBarang extends ModulTemplate {
		private $db;
		private $name;
		private $tbl_penyesuaian;
		private $tbl_stok_barang;
		private $page;
		private $action;
		private $riwayat_penyesuaian_stok_table;
		private $proto_name;
		private $proto_implement;
		private $proto_slug;
		
		public function __construct($db, $name_entity, $tbl_penyesuaian, $tbl_stok, $page){
			$this->db=$db;
			$this->name=$name_entity;
			$this->tbl_penyesuaian=$tbl_penyesuaian;
			$this->tbl_stok_barang=$tbl_stok;
			$this->page=$page;
			$header=array("Tanggal", "Nama Barang", "Jenis Barang", "Jml. Awal", "Jml. Aktual", "Selisih", "Keterangan", "Petugas Entri");
			$this->riwayat_penyesuaian_stok_table = new Table($header);
			$this->riwayat_penyesuaian_stok_table->setName("riwayat_penyesuaian_stok_barang");
			$this->riwayat_penyesuaian_stok_table->setAddButtonEnable(false);
			$this->riwayat_penyesuaian_stok_table->setReloadButtonEnable(false);
			$this->riwayat_penyesuaian_stok_table->setPrintButtonEnable(false);
			$this->riwayat_penyesuaian_stok_table->setAction(false);
			$this->proto_name="";
			$this->proto_implement="";
			$this->proto_slug="";
		}
		
		public function setPrototype($pname,$pslug,$pimplement){
			$this->proto_name=$pname;
			$this->proto_implement=$pimplement;
			$this->proto_slug=$pslug;
		}
		
		public function command($command){
			$riwayat_penyesuaian_stok_adapter = new RiwayatPenyesuaianStokBarangAdapter();
			$riwayat_penyesuaian_stok_dbtable = new DBTable($this->db,$this->tbl_penyesuaian);
			$filter = "";
			if (isset($_POST['kriteria'])) {
				$filter = " AND (".$this->tbl_stok_barang.".nama_barang LIKE '%" . $_POST['kriteria'] . "%' OR ".$this->tbl_stok_barang.".nama_jenis_barang LIKE '%" . $_POST['kriteria'] . "%' OR ".$this->tbl_penyesuaian.".nama_user LIKE '%" . $_POST['kriteria'] . "%')";
			}
			$query_value = "
				SELECT ".$this->tbl_penyesuaian.".*, ".$this->tbl_stok_barang.".nama_barang, ".$this->tbl_stok_barang.".nama_jenis_barang, ".$this->tbl_stok_barang.".satuan
				FROM ".$this->tbl_penyesuaian." LEFT JOIN ".$this->tbl_stok_barang." ON ".$this->tbl_penyesuaian.".id_stok_barang = ".$this->tbl_stok_barang.".id
				WHERE ".$this->tbl_penyesuaian.".prop NOT LIKE 'del' AND ".$this->tbl_penyesuaian.".tanggal = '" . $_POST['tanggal'] . "' " . $filter . "
			";
			$query_count = "
				SELECT COUNT(*)
				FROM  (
					SELECT ".$this->tbl_penyesuaian.".*, ".$this->tbl_stok_barang.".nama_barang, ".$this->tbl_stok_barang.".nama_jenis_barang, ".$this->tbl_stok_barang.".satuan
					FROM ".$this->tbl_penyesuaian." LEFT JOIN ".$this->tbl_stok_barang." ON ".$this->tbl_penyesuaian.".id_stok_barang = ".$this->tbl_stok_barang.".id
					WHERE ".$this->tbl_penyesuaian.".prop NOT LIKE 'del' AND ".$this->tbl_penyesuaian.".tanggal = '" . $_POST['tanggal'] . "' " . $filter . "
				) v_riwayat_penyesuaian_stok
			";
			$riwayat_penyesuaian_stok_dbtable->setPreferredQuery(true, $query_value, $query_count);
			$riwayat_penyesuaian_stok_dbresponder = new DBResponder($riwayat_penyesuaian_stok_dbtable,$this->riwayat_penyesuaian_stok_table,$riwayat_penyesuaian_stok_adapter);
			$data = $riwayat_penyesuaian_stok_dbresponder->command($_POST['command']);
			echo json_encode($data);
			return;
		}
		
		public function phpPreLoad(){		
			$riwayat_penyesuaian_stok_form = new Form("form_riwayat_penyesuaian_stok", "", $this->name . " : Riwayat Penyesuaian Stok Barang");
			$tanggal_text = new Text("riwayat_penyesuaian_stok_barang_tanggal", "riwayat_penyesuaian_stok_barang_tanggal", date('Y-m-d'));
			$tanggal_text->setClass("mydate smis-one-option-input");
			$tanggal_text->setAtribute("data-date-format='yyyy-m-d'");
			$show_button = new Button("", "", "Tampilkan");
			$show_button->setClass("btn-primary");
			$show_button->setIcon("icon-white icon-repeat");
			$show_button->setisButton(Button::$ICONIC);
			$show_button->setAction("riwayat_penyesuaian_stok_barang.view()");
			$filter_input_group = new InputGroup("");
			$filter_input_group->addComponent($tanggal_text);
			$filter_input_group->addComponent($show_button);
			$riwayat_penyesuaian_stok_form->addElement("Tanggal", $filter_input_group);
			
			echo $riwayat_penyesuaian_stok_form->getHtml();
			echo "<div id'table_content'>";
			echo $this->riwayat_penyesuaian_stok_table->getHtml();
			echo "</div>";
			echo addJS("framework/smis/js/table_action.js");
		}
		
		public function jsPreLoad(){
			?>
			<script type="text/javascript">
				function RiwayatPenyesuaianStokAction(name, page, action, column) {
					this.initialize(name, page, action, column);
				}
				RiwayatPenyesuaianStokAction.prototype.constructor = RiwayatPenyesuaianStokAction;
				RiwayatPenyesuaianStokAction.prototype = new TableAction();
				RiwayatPenyesuaianStokAction.prototype.view = function() {
					var self = this;
					var data = this.getViewData();
					data['tanggal'] = $("#riwayat_penyesuaian_stok_barang_tanggal").val();
					data['kriteria'] = $("#riwayat_penyesuaian_stok_barang_kriteria").val();
					showLoading();
					$.post(
						"",
						data,
						function(response) {
							var json = getContent(response);
							if (json == null) return;
							$("#" + self.prefix + "_list").html(json.list);
							$("#" + self.prefix + "_pagination").html(json.pagination);
							dismissLoading();
						}
					);
				};

				var RIWAYAT_PENYESUAIAN_STOK_BARANG_PNAME="<?php echo $this->proto_name; ?>";
				var RIWAYAT_PENYESUAIAN_STOK_BARANG_PSLUG="<?php echo $this->proto_slug; ?>";
				var RIWAYAT_PENYESUAIAN_STOK_BARANG_PIMPL="<?php echo $this->proto_implement; ?>";
				var riwayat_penyesuaian_stok_barang;
				var RIWAYAT_PENYESUAIAN_STOK_BARANG_ENTITY="<?php echo $this->page; ?>";
				$(document).ready(function() {
					$('[data-toggle="popover"]').popover({
						trigger: 'hover',
						'placement': 'top'
					});
					riwayat_penyesuaian_stok_barang = new RiwayatPenyesuaianStokAction("riwayat_penyesuaian_stok_barang", RIWAYAT_PENYESUAIAN_STOK_BARANG_ENTITY, "riwayat_penyesuaian_stok_barang",new Array());
					riwayat_penyesuaian_stok_barang.setPrototipe(RIWAYAT_PENYESUAIAN_STOK_BARANG_PNAME,RIWAYAT_PENYESUAIAN_STOK_BARANG_PSLUG,RIWAYAT_PENYESUAIAN_STOK_BARANG_PIMPL);
					riwayat_penyesuaian_stok_barang.view();
				});
			</script>
			<?php 
		}	
	}
?>