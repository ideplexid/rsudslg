<?php 
	require_once 'smis-framework/smis/template/ModulTemplate.php';
	require_once 'smis-base/smis-include-service-consumer.php';

	class DaftarBarang extends ModulTemplate{
		private $db;
		private $name;
		private $tbl_barang_masuk;
		private $tbl_stok_barang;
		private $page;
		private $action;
		private $daftar_barang_table;
		private $proto_name;
		private $proto_implement;
		private $proto_slug;
		
		public function __construct($db, $name_entity, $slug_table_parent, $slug_table_child, $page){
			$this->db=$db;
			$this->name=$name_entity;
			$this->tbl_barang_masuk=$slug_table_parent;
			$this->tbl_stok_barang=$slug_table_child;
			$this->page=$page;
			$header=array("Nomor", "Nama Barang", "Jenis Barang", "Stok");
			$this->daftar_barang_table = new Table($header, $this->name . " : Rekapitulasi Stok Barang");
			$this->daftar_barang_table->setName("daftar_barang");
			$this->daftar_barang_table->setAddButtonEnable(false);
			$this->daftar_barang_table->setPrintButtonEnable(false);
			$this->daftar_barang_table->setReloadButtonEnable(false);
			$this->daftar_barang_table->setAction(false);
		}
		
		public function setPrototype($pname,$pslug,$pimplement){
			$this->proto_name=$pname;
			$this->proto_implement=$pimplement;
			$this->proto_slug=$pslug;
		}
		
		public function command($command){
			$daftar_barang_adapter = new SimpleAdapter();
			$daftar_barang_adapter->setUseID(false);
			$daftar_barang_adapter->add("id", "id_barang");
			$daftar_barang_adapter->add("Nomor", "id_barang","digit8");
			$daftar_barang_adapter->add("Nama Barang", "nama_barang");
			$daftar_barang_adapter->add("Jenis Barang", "nama_jenis_barang");
			$daftar_barang_adapter->add("Stok", "stok");
			
			$columns = array("id_barang", "nama_barang", "nama_jenis_barang", "sisa");
			$daftar_barang_dbtable = new DBTable($this->db,$this->tbl_barang_masuk);
			$filter = "";
			if (isset($_POST['kriteria'])) {
				$filter = " AND (nama_barang LIKE '%" . $_POST['kriteria'] . "%' OR nama_jenis_barang LIKE '%" . $_POST['kriteria'] . "%')";
			}
			$v_stok="SELECT id_barang, nama_barang, nama_jenis_barang, GROUP_CONCAT(CONCAT(sisa, ' ', satuan) ORDER BY satuan, sisa ASC SEPARATOR ', ') AS 'stok'
					FROM (
						SELECT id_barang, nama_barang, nama_jenis_barang, SUM(sisa) AS 'sisa', satuan, konversi, satuan_konversi
						FROM ".$this->tbl_stok_barang." LEFT JOIN ".$this->tbl_barang_masuk." ON ".$this->tbl_stok_barang.".id_barang_masuk = ".$this->tbl_barang_masuk.".id
						WHERE ".$this->tbl_stok_barang.".prop NOT LIKE 'del' AND ".$this->tbl_barang_masuk.".prop NOT LIKE 'del' AND ".$this->tbl_barang_masuk.".status = 'sudah' " . $filter . "
						GROUP BY id_barang, satuan, konversi, satuan_konversi
					) v_barang
					GROUP BY id_barang";
			$query_value = "SELECT * FROM ( $v_stok ) v_stok ";
			$query_count = "SELECT COUNT(*) FROM ( $v_stok ) v_stok ";
			$daftar_barang_dbtable->setPreferredQuery(true, $query_value, $query_count);
			$daftar_barang_dbresponder = new DBResponder($daftar_barang_dbtable,$this->daftar_barang_table,$daftar_barang_adapter);
			$data = $daftar_barang_dbresponder->command($_POST['command']);
			echo json_encode($data);
			return;
		}
		
		public function phpPreLoad(){
			echo $this->daftar_barang_table->getHtml();
			echo addJS("framework/smis/js/table_action.js");
		}
		
		public function jsPreLoad(){
			?>
			<script type="text/javascript">
				var DAFTAR_BARANG_PNAME="<?php echo $this->proto_name; ?>";
				var DAFTAR_BARANG_PSLUG="<?php echo $this->proto_slug; ?>";
				var DAFTAR_BARANG_PIMPL="<?php echo $this->proto_implement; ?>";
				var DAFTAR_BARANG_ENTITY="<?php echo $this->page; ?>";
				var daftar_barang;
				$(document).ready(function() {
					$('[data-toggle="popover"]').popover({
						trigger: 'hover',
						'placement': 'top'
					});
					var daftar_barang_columns = new Array("id_barang", "nama_barang", "nama_jenis_barang", "sisa");
					daftar_barang = new TableAction("daftar_barang",DAFTAR_BARANG_ENTITY,"daftar_barang",daftar_barang_columns);
					daftar_barang.setPrototipe(DAFTAR_BARANG_PNAME,DAFTAR_BARANG_PSLUG,DAFTAR_BARANG_PIMPL);
					daftar_barang.view();
				});
			</script>
			<?php 
		}	
	}
?>