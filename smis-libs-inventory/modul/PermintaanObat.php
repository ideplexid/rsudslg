<?php 
	require_once 'smis-framework/smis/template/ModulTemplate.php';
	require_once 'smis-base/smis-include-service-consumer.php';
	require_once 'smis-libs-inventory/table/PermintaanObatTable.php';
	require_once 'smis-libs-inventory/adapter/PermintaanObatAdapter.php';
	require_once 'smis-libs-inventory/service_consumer/PermintaanObatService.php';
	require_once 'smis-libs-inventory/responder/PermintaanObatDBResponder.php';
	require_once 'smis-libs-inventory/table/DPermintaanObatTable.php';

	class PermintaanObat extends ModulTemplate {
		private $db;
		private $name;
		private $tbl_permintaan_obat;
		private $tbl_dpermintaan_obat;
		private $page;
		private $action;
		private $permintaan_obat_table;
		private $proto_name;
		private $proto_implement;
		private $proto_slug;
		public function __construct($db, $name_entity, $slug_table_parent, $slug_table_child, $page){
			$this->db=$db;
			$this->name=$name_entity;
			$this->tbl_permintaan_obat=$slug_table_parent;
			$this->tbl_dpermintaan_obat=$slug_table_child;
			$this->page=$page;
			$this->permintaan_obat_table = new PermintaanObatTable(array("Nomor", "Tanggal", "Status"), $this->name . " : Permintaan Obat");
			$this->permintaan_obat_table->setName("permintaan_obat");
			$this->proto_name="";
			$this->proto_implement="";
			$this->proto_slug="";
		}
		
		public function setPrototype($pname,$pslug,$pimplement){
			$this->proto_name=$pname;
			$this->proto_implement=$pimplement;
			$this->proto_slug=$pslug;
		}
		
		public function superCommand($super_command){
			$obat_table = new Table(array("Obat", "Jenis"));
			$obat_table->setName("permintaan_obat_obat");
			$obat_table->setModel(Table::$SELECT);
			$obat_adapter = new SimpleAdapter();
			$obat_adapter->add("Obat", "nama");
			$obat_adapter->add("Jenis", "nama_jenis_barang");
			$obat_service_responder = new ServiceResponder(	$this->db,$obat_table,$obat_adapter,"get_daftar_barang_m");
			
			$super_command = new SuperCommand();
			$super_command->addResponder("permintaan_obat_obat", $obat_service_responder);
			$init = $super_command->initialize();
			if ($init != null) {
				echo $init;
				return;
			}
		}
		
		public function command($command){
			$permintaan_obat_adapter = new PermintaanObatAdapter();
			$columns = array("id", "tanggal", "status");
			$permintaan_obat_dbtable = new DBTable($this->db, $this->tbl_permintaan_obat,$columns);
			$permintaan_obat_dbtable->setOrder(" id DESC ");
			$permintaan_obat_dbresponder = new PermintaanObatDBResponder($permintaan_obat_dbtable,$this->permintaan_obat_table,$permintaan_obat_adapter,$this->tbl_dpermintaan_obat,$this->page);
			$data = $permintaan_obat_dbresponder->command($_POST['command']);
			if (isset($_POST['push_command'])) {
				$id=$data['content']['id'];
				$permintaan_obat_row = $permintaan_obat_dbtable->select($id);
				$dpermintaan_obat_dbtable = new DBTable($this->db, $this->tbl_dpermintaan_obat);
				$dpermintaan_obat_dbtable->addCustomKriteria("id_permintaan_obat", "='".$id."'");
				$dpermintaan_obat_dbtable->setShowAll(true);
				$view=$dpermintaan_obat_dbtable->getQueryView("", "0");
				$detail = $dpermintaan_obat_dbtable->get_result($view['query']);
				$command = "push_" . $_POST['push_command'];
				$push_permintaan_obat_service_consumer = new PushPermintaanObatService($this->db, $permintaan_obat_row->id, $permintaan_obat_row->tanggal, $permintaan_obat_row->status, $detail, $command,$this->page);
				$push_permintaan_obat_service_consumer->execute();
			}
			echo json_encode($data);
			return;
		}
		
		public function phpPreLoad(){
			$this->permintaan_obat_table->addModal("id", "hidden", "", "");
			$this->permintaan_obat_table->addModal("tanggal", "date", "Tanggal", "", "y", null, true);
			$permintaan_obat_modal = $this->permintaan_obat_table->getModal();
			
			$dpermintaan_obat_table = new DPermintaanObatTable(array("Kode", "Obat", "Jenis", "Jml. Permintaan", "Jml. Dipenuhi", "Keterangan"));
			$dpermintaan_obat_table->setName("dpermintaan_obat");
			$dpermintaan_obat_table->setFooterVisible(false);
			$permintaan_obat_modal->addBody("dpermintaan_obat_table", $dpermintaan_obat_table);
			$permintaan_obat_modal->setTitle("Data Permintaan Obat");
			$permintaan_obat_modal->setModalSize(Modal::$FULL_MODEL);
			
			$permintaan_obat_button = new Button("", "", "OK");
			$permintaan_obat_button->setClass("btn-success");
			$permintaan_obat_button->setAtribute("id='permintaan_obat_ok'");
			$permintaan_obat_button->setAction("$($(this).data('target')).smodal('hide')");
			$permintaan_obat_modal->addFooter($permintaan_obat_button);
			
			$dpermintaan_obat_table->addModal("id", "hidden", "", "");
			$dpermintaan_obat_table->addModal("id_obat", "hidden", "", "");
			$dpermintaan_obat_table->addModal("kode_obat", "text", "Kode", "", "text", "n", "y");
			$dpermintaan_obat_table->addModal("nama_obat", "chooser-permintaan_obat-permintaan_obat_obat", "Obat", "");
			$dpermintaan_obat_table->addModal("nama_jenis_obat", "text", "Jenis", "", "text", "n", "y");
			$dpermintaan_obat_table->addModal("jumlah_diminta", "text", "Jumlah", "","numeric","n");
			$dpermintaan_obat_table->addModal("satuan_diminta", "text", "Satuan", "", "text", "n", "y");
			$dpermintaan_obat_modal=$dpermintaan_obat_table->getModal();
			$dpermintaan_obat_modal->setTitle("Detail Permintaan Obat");
			
			echo $dpermintaan_obat_modal->getHtml();
			echo $permintaan_obat_modal->getHtml();
			echo $this->permintaan_obat_table->getHtml();
			echo addJS("framework/smis/js/table_action.js");
			echo addJS("framework/bootstrap/js/bootstrap-datepicker.js");
			echo addCSS("framework/bootstrap/css/datepicker.css");
		}
		
		
		public function jsPreLoad(){
			?>
			<script type="text/javascript">
				function PermintaanObatAction(name, page, action, column) {
					this.initialize(name, page, action, column);
				}
				PermintaanObatAction.prototype.constructor = PermintaanObatAction;
				PermintaanObatAction.prototype = new TableAction();
				PermintaanObatAction.prototype.show_add_form = function() {
					var today = new Date();
					$("#permintaan_obat_id").val("");
					$("#permintaan_obat_tanggal").val("");
					$("#dpermintaan_obat_add").show();
					PERMINTAAN_OBAT_ROW_ID = 0;
					$("tbody#dpermintaan_obat_list").children("tr").remove();
					$("#modal_alert_permintaan_obat_add_form").html("");
					$(".error_field").removeClass("error_field");
					$("#permintaan_obat_save").show();
					$("#permintaan_obat_save").removeAttr("onclick");
					$("#permintaan_obat_save").attr("onclick", "permintaan_obat.save()");
					$("#permintaan_obat_ok").hide();
					$("#permintaan_obat_add_form").smodal("show");
				};
				PermintaanObatAction.prototype.validate = function() {
					var valid = true;
					var invalid_msg = "";
					var nord = $("tbody#dpermintaan_obat_list").children("tr").length;
					$(".error_field").removeClass("error_field");
					if (nord == 0) {
						valid = false;
						invalid_msg += "<br/><strong>Detil Permintaan Obat</strong> tidak boleh kosong";
					}
					if (!valid) {
						$("#modal_alert_permintaan_obat_add_form").html(
							"<div class='alert alert-block alert-danger'>" +
								"<h4>Peringatan</h4>" +
								invalid_msg +
							"</div>"
						);
					}
					return valid;
				};
				PermintaanObatAction.prototype.save = function() {
					if (!this.validate()) {
						return;
					}
					$("#permintaan_obat_add_form").smodal("hide");
					showLoading();
					var self = this;
					var data = this.getRegulerData();
					
					data['command'] = "save";
					data['push_command'] = "save";
					data['id'] = $("#permintaan_obat_id").val();
					data['tanggal'] = $("#permintaan_obat_tanggal").val();
					data['status'] = "belum";
					var detail = {};
					var nor = $("tbody#dpermintaan_obat_list").children("tr").length;
					for(var i = 0; i < nor; i++) {
						var d_data = {};
						var prefix = $("tbody#dpermintaan_obat_list").children('tr').eq(i).prop("id");
						d_data['id_obat'] = $("#" + prefix + "_id_obat").text();
						d_data['kode_obat'] = $("#" + prefix + "_kode_obat").text();
						d_data['nama_obat'] = $("#" + prefix + "_nama_obat").text();
						d_data['nama_jenis_obat'] = $("#" + prefix + "_nama_jenis_obat").text();
						d_data['jumlah_permintaan'] = $("#" + prefix + "_jumlah_diminta").text();
						d_data['satuan_permintaan'] = $("#" + prefix + "_satuan_diminta").text();
						d_data['jumlah_dipenuhi'] = $("#" + prefix + "_jumlah_dipenuhi").text();
						d_data['satuan_dipenuhi'] = $("#" + prefix + "_satuan_dipenuhi").text();
						d_data['keterangan'] = $("#" + prefix + "_keterangan").text();
						detail[i] = d_data;
					}
					data['detail'] = detail;
					$.post(
						"",
						data,
						function(response) {
							var json = getContent(response);
							if (json == null) {
								$("#permintaan_obat_add_form").smodal("show");
							} else {
								self.view();
							}
							dismissLoading();
						}
					);
				};
				PermintaanObatAction.prototype.edit = function(id) {
					var data = this.getRegulerData();
					
					data['command'] = "edit";
					data['id'] = id;
					$.post(
						"",
						data,
						function(response) {
							var json = getContent(response);
							if (json == null) return;
							$("#permintaan_obat_id").val(json.header.id);
							$("#permintaan_obat_tanggal").val(json.header.tanggal);
							$("#dpermintaan_obat_add").show();
							PERMINTAAN_OBAT_ROW_ID = 0;
							$("tbody#dpermintaan_obat_list").children("tr").remove();
							$("#modal_alert_permintaan_obat_add_form").html("");
							$(".error_field").removeClass("error_field");
							for(var i = 0; i < json.detail.length; i++) {
								var d_id = json.detail[i].id;
								var d_id_permintaan_obat = json.detail[i].id_permintaan_obat;
								var d_id_obat = json.detail[i].id_obat;
								var d_kode_obat = json.detail[i].kode_obat;
								var d_nama_obat = json.detail[i].nama_obat;
								var d_nama_jenis_obat = json.detail[i].nama_jenis_obat;
								var d_jumlah_diminta = json.detail[i].jumlah_permintaan;
								var d_satuan_diminta = json.detail[i].satuan_permintaan;
								var d_jumlah_dipenuhi = json.detail[i].jumlah_dipenuhi;
								var d_satuan_dipenuhi = json.detail[i].satuan_dipenuhi;
								var d_keterangan = json.detail[i].keterangan;
								var f_jumlah_dipenuhi = "-";
								var edit_group = "";
								if (Number(d_jumlah_dipenuhi) != 0) {
									f_jumlah_dipenuhi = d_jumlah_dipenuhi + " " + d_satuan_dipenuhi;
								} else {
									edit_group = "<a href='#' onclick='dpermintaan_obat.edit(" + PERMINTAAN_OBAT_ROW_ID + ")' data-content='Ubah' data-toggle='popover' class='input btn btn-warning'>" + 
													"<i class='icon-edit icon-white'></i>" +
												 "</a>" +
												 "<a href='#' onclick='dpermintaan_obat.del(" + PERMINTAAN_OBAT_ROW_ID + ")' data-content='Hapus' data-toggle='popover' class='input btn btn-danger'>" + 
													"<i class='icon-remove icon-white'></i>" + 
												 "</a>";
								}
								$("tbody#dpermintaan_obat_list").append(
									"<tr id='data_" + PERMINTAAN_OBAT_ROW_ID + "'>" +
										"<td id='data_" + PERMINTAAN_OBAT_ROW_ID + "_id' style='display: none;'>" + d_id + "</td>" +
										"<td id='data_" + PERMINTAAN_OBAT_ROW_ID + "_jumlah_diminta' style='display: none;'>" + d_jumlah_diminta + "</td>" +
										"<td id='data_" + PERMINTAAN_OBAT_ROW_ID + "_satuan_diminta' style='display: none;'>" + d_satuan_diminta + "</td>" +
										"<td id='data_" + PERMINTAAN_OBAT_ROW_ID + "_jumlah_dipenuhi' style='display: none;'>" + d_jumlah_dipenuhi + "</td>" +
										"<td id='data_" + PERMINTAAN_OBAT_ROW_ID + "_satuan_dipenuhi' style='display: none;'>" + d_satuan_dipenuhi + "</td>" +
										"<td id='data_" + PERMINTAAN_OBAT_ROW_ID + "_id_obat' style='display: none;'>" + d_id_obat + "</td>" +
										"<td id='data_" + PERMINTAAN_OBAT_ROW_ID + "_kode_obat'>" + d_kode_obat + "</td>" +
										"<td id='data_" + PERMINTAAN_OBAT_ROW_ID + "_nama_obat'>" + d_nama_obat + "</td>" +
										"<td id='data_" + PERMINTAAN_OBAT_ROW_ID + "_nama_jenis_obat'>" + d_nama_jenis_obat + "</td>" +
										"<td id='data_" + PERMINTAAN_OBAT_ROW_ID + "_f_jumlah_diminta'>" + d_jumlah_diminta + " " + d_satuan_diminta + "</td>" +
										"<td id='data_" + PERMINTAAN_OBAT_ROW_ID + "_f_jumlah_dipenuhi'>" + f_jumlah_dipenuhi + "</td>" +
										"<td id='data_" + PERMINTAAN_OBAT_ROW_ID + "_keterangan'>" + d_keterangan + "</td>" +
										"<td>" +
											"<div class='btn-group noprint'>" +
												edit_group +
											"</div>" +
										"</td>" +
									"</tr>"
								);
								PERMINTAAN_OBAT_ROW_ID++;
							}
							$("#permintaan_obat_save").removeAttr("onclick");
							$("#permintaan_obat_save").attr("onclick", "permintaan_obat.update(" + id + ")");
							$("#permintaan_obat_save").show();
							$("#permintaan_obat_ok").hide();
							$("#modal_alert_permintaan_obat_add_form").html("");
							$(".error_field").removeClass("error_field");
							$("#permintaan_obat_add_form").smodal("show");
						}
					);
				};
				PermintaanObatAction.prototype.update = function(id) {
					if (!this.validate()) {
						return;
					}
					$("#permintaan_obat_add_form").smodal("hide");
					showLoading();
					var self = this;
					var data = this.getRegulerData();
					
					data['command'] = "save";
					data['push_command'] = "save";
					data['id'] = $("#permintaan_obat_id").val();
					data['tanggal'] = $("#permintaan_obat_tanggal").val();
					data['status'] = "belum";
					var detail = {};
					var nor = $("tbody#dpermintaan_obat_list").children("tr").length;
					for(var i = 0; i < nor; i++) {
						var d_data = {};
						var prefix = $("tbody#dpermintaan_obat_list").children('tr').eq(i).prop("id");
						d_data['id'] = $("#" + prefix + "_id").text();
						d_data['id_obat'] = $("#" + prefix + "_id_obat").text();
						d_data['kode_obat'] = $("#" + prefix + "_kode_obat").text();
						d_data['nama_obat'] = $("#" + prefix + "_nama_obat").text();
						d_data['nama_jenis_obat'] = $("#" + prefix + "_nama_jenis_obat").text();
						d_data['jumlah_permintaan'] = $("#" + prefix + "_jumlah_diminta").text();
						d_data['satuan_permintaan'] = $("#" + prefix + "_satuan_diminta").text();
						d_data['jumlah_dipenuhi'] = $("#" + prefix + "_jumlah_dipenuhi").text();
						d_data['satuan_dipenuhi'] = $("#" + prefix + "_satuan_dipenuhi").text();
						d_data['keterangan'] = $("#" + prefix + "_keterangan").text();
						if ($("#" + prefix).attr("class") == "deleted") {
							d_data['cmd'] = "delete";
						} else if ($("#" + prefix + "_id").text() == "") {
							d_data['cmd'] = "insert";
						} else {
							d_data['cmd'] = "update";
						}
						detail[i] = d_data;
					}
					data['detail'] = detail;
					$.post(
						"",
						data,
						function(response) {
							var json = getContent(response);
							if (json == null) {
								$("#permintaan_obat_add_form").smodal("show");
							} else {
								self.view();
							}
							dismissLoading();
						}
					);
				};
				PermintaanObatAction.prototype.del = function(id) {
					var self = this;
					var del_data = this.getRegulerData();
					
					del_data['command'] = "del";
					del_data['id'] = id;
					del_data['push_command'] = "delete";
					bootbox.confirm(
						"Anda yakin ingin menghapus data ini?", 
						function(result) {
						   if (result) {
							   showLoading();
								$.post(
									"",
									del_data,
									function(res){
										var json = getContent(res);
										if (json == null) return;
										self.view();
										dismissLoading();
									}
								);
							}
						}
					);
				};
				PermintaanObatAction.prototype.detail = function(id) {
					var data = this.getRegulerData();
					
					data['command'] = "edit";
					data['id'] = id;
					$.post(
						"",
						data,
						function(response) {
							var json = getContent(response);
							if (json == null) return;
							$("#permintaan_obat_id").val(json.header.id);
							$("#permintaan_obat_tanggal").val(json.header.tanggal);
							$("#dpermintaan_obat_add").hide();
							PERMINTAAN_OBAT_ROW_ID = 0;
							$("tbody#dpermintaan_obat_list").children("tr").remove();
							$("#modal_alert_permintaan_obat_add_form").html("");
							$(".error_field").removeClass("error_field");
							for(var i = 0; i < json.detail.length; i++) {
								var d_id = json.detail[i].id;
								var d_id_permintaan_obat = json.detail[i].id_permintaan_obat;
								var d_id_obat = json.detail[i].id_obat;
								var d_kode_obat = json.detail[i].kode_obat;
								var d_nama_obat = json.detail[i].nama_obat;
								var d_nama_jenis_obat = json.detail[i].nama_jenis_obat;
								var d_jumlah_diminta = json.detail[i].jumlah_permintaan;
								var d_satuan_diminta = json.detail[i].satuan_permintaan;
								var d_jumlah_dipenuhi = json.detail[i].jumlah_dipenuhi;
								var d_satuan_dipenuhi = json.detail[i].satuan_dipenuhi;
								var d_keterangan = json.detail[i].keterangan;
								var f_jumlah_dipenuhi = "-";
								var edit_group = "";
								if (Number(d_jumlah_dipenuhi) != 0) {
									f_jumlah_dipenuhi = d_jumlah_dipenuhi + " " + d_satuan_dipenuhi;
								}
								$("tbody#dpermintaan_obat_list").append(
									"<tr id='data_" + PERMINTAAN_OBAT_ROW_ID + "'>" +
										"<td id='data_" + PERMINTAAN_OBAT_ROW_ID + "_id' style='display: none;'>" + d_id + "</td>" +
										"<td id='data_" + PERMINTAAN_OBAT_ROW_ID + "_jumlah_diminta' style='display: none;'>" + d_jumlah_diminta + "</td>" +
										"<td id='data_" + PERMINTAAN_OBAT_ROW_ID + "_satuan_diminta' style='display: none;'>" + d_satuan_diminta + "</td>" +
										"<td id='data_" + PERMINTAAN_OBAT_ROW_ID + "_jumlah_dipenuhi' style='display: none;'>" + d_jumlah_dipenuhi + "</td>" +
										"<td id='data_" + PERMINTAAN_OBAT_ROW_ID + "_satuan_dipenuhi' style='display: none;'>" + d_satuan_dipenuhi + "</td>" +
										"<td id='data_" + PERMINTAAN_OBAT_ROW_ID + "_id_obat' style='display: none;'>" + d_id_obat + "</td>" +
										"<td id='data_" + PERMINTAAN_OBAT_ROW_ID + "_kode_obat'>" + d_kode_obat + "</td>" +
										"<td id='data_" + PERMINTAAN_OBAT_ROW_ID + "_nama_obat'>" + d_nama_obat + "</td>" +
										"<td id='data_" + PERMINTAAN_OBAT_ROW_ID + "_nama_jenis_obat'>" + d_nama_jenis_obat + "</td>" +
										"<td id='data_" + PERMINTAAN_OBAT_ROW_ID + "_f_jumlah_diminta'>" + d_jumlah_diminta + " " + d_satuan_diminta + "</td>" +
										"<td id='data_" + PERMINTAAN_OBAT_ROW_ID + "_f_jumlah_dipenuhi'>" + f_jumlah_dipenuhi + "</td>" +
										"<td id='data_" + PERMINTAAN_OBAT_ROW_ID + "_keterangan'>" + d_keterangan + "</td>" +
										"<td></td>" +
									"</tr>"
								);
								PERMINTAAN_OBAT_ROW_ID++;
							}
							$("#permintaan_obat_save").removeAttr("onclick");
							$("#permintaan_obat_save").hide();
							$("#permintaan_obat_ok").show();
							$("#modal_alert_permintaan_obat_add_form").html("");
							$(".error_field").removeClass("error_field");
							$("#permintaan_obat_add_form").smodal("show");
						}
					);
				};
				PermintaanObatAction.prototype.print_spo = function(id) {
					var data = this.getRegulerData();
					data['command'] = "print_spo";
					data['id'] = id;
					$.post(
						"",
						data,
						function(response) {
							var json = getContent(response);
							if (json == null) return;
							smis_print(json);
						}
					);
				};
				
				function DPermintaanObatAction(name, page, action, column) {
					this.initialize(name, page, action, column);
				}
				DPermintaanObatAction.prototype.constructor = DPermintaanObatAction;
				DPermintaanObatAction.prototype = new TableAction();
				DPermintaanObatAction.prototype.show_add_form = function() {
					$("#dpermintaan_obat_id").val("");
					$("#dpermintaan_obat_id_obat").val("");
					$("#dpermintaan_obat_kode_obat").val("");
					$("#dpermintaan_obat_nama_obat").val("");
					$("#dpermintaan_obat_nama_obat").removeAttr("disabled");
					$("#dpermintaan_obat_nama_obat").attr("disabled", "disabled");
					$("#dpermintaan_obat_nama_jenis_obat").val("");
					$("#dpermintaan_obat_jumlah_diminta").val("");
					$("#dpermintaan_obat_satuan_diminta").val("");
					$("#modal_alert_dpermintaan_obat_add_form").html("");
					$(".error_field").removeClass("error_field");
					$("#dpermintaan_obat_save").removeAttr("onclick");
					$("#dpermintaan_obat_save").attr("onclick", "dpermintaan_obat.save()");
					$("#dpermintaan_obat_add_form").smodal("show");
				};
				DPermintaanObatAction.prototype.validate = function() {
					var valid = true;
					var invalid_msg = "";
					var nama_obat = $("#dpermintaan_obat_nama_obat").val();
					var jumlah_diminta = $("#dpermintaan_obat_jumlah_diminta").val();
					var satuan_diminta = $("#dpermintaan_obat_satuan_diminta").val();
					$(".error_field").removeClass("error_field");
					if (nama_obat == "") {
						valid = false;
						invalid_msg += "<br/><strong>Obat</strong> tidak boleh kosong";
						$("#dpermintaan_obat_nama_obat").addClass("error_field");
					}
					if (jumlah_diminta == "") {
						valid = false;
						invalid_msg += "<br/><strong>Jumlah</strong> tidak boleh kosong";
						$("#dpermintaan_obat_jumlah_diminta").addClass("error_field");
					} else if (!is_numeric(jumlah_diminta)) {
						valid = false;
						invalid_msg += "<br/><strong>Jumlah</strong> seharusnya numerik (0-9)";
						$("#dpermintaan_obat_jumlah_diminta").addClass("error_field");
					}
					if (satuan_diminta == "") {
						valid = false;
						invalid_msg += "<br/><strong>Satuan</strong> tidak boleh kosong";
						$("#dpermintaan_obat_satuan_diminta").addClass("error_field");
					}
					if (!valid) {
						$("#modal_alert_dpermintaan_obat_add_form").html(
							"<div class='alert alert-block alert-danger'>" +
								"<h4>Peringatan</h4>" +
								invalid_msg +
							"</div>"
						);
					}
					return valid;
				};
				DPermintaanObatAction.prototype.save = function() {
					if (!this.validate()) {
						return;
					}
					var id = $("#dpermintaan_obat_id").val();
					var id_obat = $("#dpermintaan_obat_id_obat").val();
					var kode_obat = $("#dpermintaan_obat_kode_obat").val();
					var nama_obat = $("#dpermintaan_obat_nama_obat").val();
					var nama_jenis_obat = $("#dpermintaan_obat_nama_jenis_obat").val();
					var jumlah_diminta = $("#dpermintaan_obat_jumlah_diminta").val();
					var satuan_diminta = $("#dpermintaan_obat_satuan_diminta").val();
					$("tbody#dpermintaan_obat_list").append(
						"<tr id='data_" + PERMINTAAN_OBAT_ROW_ID + "'>" +
							"<td id='data_" + PERMINTAAN_OBAT_ROW_ID + "_id' style='display: none;'>" + id + "</td>" +
							"<td id='data_" + PERMINTAAN_OBAT_ROW_ID + "_jumlah_diminta' style='display: none;'>" + jumlah_diminta + "</td>" +
							"<td id='data_" + PERMINTAAN_OBAT_ROW_ID + "_satuan_diminta' style='display: none;'>" + satuan_diminta + "</td>" +
							"<td id='data_" + PERMINTAAN_OBAT_ROW_ID + "_jumlah_dipenuhi' style='display: none;'>0</td>" +
							"<td id='data_" + PERMINTAAN_OBAT_ROW_ID + "_satuan_dipenuhi' style='display: none;'>-</td>" +
							"<td id='data_" + PERMINTAAN_OBAT_ROW_ID + "_id_obat' style='display: none;'>" + id_obat + "</td>" +
							"<td id='data_" + PERMINTAAN_OBAT_ROW_ID + "_kode_obat'>" + kode_obat + "</td>" +
							"<td id='data_" + PERMINTAAN_OBAT_ROW_ID + "_nama_obat'>" + nama_obat + "</td>" +
							"<td id='data_" + PERMINTAAN_OBAT_ROW_ID + "_nama_jenis_obat'>" + nama_jenis_obat + "</td>" +
							"<td id='data_" + PERMINTAAN_OBAT_ROW_ID + "_f_jumlah_diminta'>" + jumlah_diminta + " " + satuan_diminta + "</td>" +
							"<td id='data_" + PERMINTAAN_OBAT_ROW_ID + "_f_jumlah_dipenuhi'>-</td>" +
							"<td id='data_" + PERMINTAAN_OBAT_ROW_ID + "_keterangan'>-</td>" +
							"<td>" +
								"<div class='btn-group noprint'>" +
									"<a href='#' onclick='dpermintaan_obat.edit(" + PERMINTAAN_OBAT_ROW_ID + ")' data-content='Ubah' data-toggle='popover' class='input btn btn-warning'>" + 
										"<i class='icon-edit icon-white'></i>" +
									"</a>" +
									"<a href='#' onclick='dpermintaan_obat.del(" + PERMINTAAN_OBAT_ROW_ID + ")' data-content='Hapus' data-toggle='popover' class='input btn btn-danger'>" + 
										"<i class='icon-remove icon-white'></i>" + 
									"</a>" +
								"</div>" +
							"</td>" +
						"</tr>"
					);
					PERMINTAAN_OBAT_ROW_ID++;
					$("#dpermintaan_obat_add_form").smodal("hide");
				};
				DPermintaanObatAction.prototype.edit = function(r_num) {
					var id = $("#data_" + r_num + "_id").text();
					var id_obat = $("#data_" + r_num + "_id_obat").text();
					var kode_obat = $("#data_" + r_num + "_kode_obat").text();
					var nama_obat = $("#data_" + r_num + "_nama_obat").text();
					var nama_jenis_obat = $("#data_" + r_num + "_nama_jenis_obat").text();
					var jumlah_diminta = $("#data_" + r_num + "_jumlah_diminta").text();
					var satuan_diminta = $("#data_" + r_num + "_satuan_diminta").text();
					$("#dpermintaan_obat_id").val(id);
					$("#dpermintaan_obat_id_obat").val(id_obat);
					$("#dpermintaan_obat_kode_obat").val(kode_obat);
					$("#dpermintaan_obat_nama_obat").val(nama_obat);
					$("#dpermintaan_obat_nama_obat").removeAttr("disabled");
					$("#dpermintaan_obat_nama_obat").attr("disabled", "disabled");
					$("#dpermintaan_obat_nama_jenis_obat").val(nama_jenis_obat);
					$("#dpermintaan_obat_jumlah_diminta").val(jumlah_diminta);
					$("#dpermintaan_obat_satuan_diminta").val(satuan_diminta);
					$("#dpermintaan_obat_save").removeAttr("onclick");
					$("#dpermintaan_obat_save").attr("onclick", "dpermintaan_obat.update(" + r_num + ")");
					$("#dpermintaan_obat_add_form").smodal("show");
				};
				DPermintaanObatAction.prototype.update = function(r_num) {
					if (!this.validate()) {
						return;
					}
					var id_obat = $("#dpermintaan_obat_id_obat").val();
					var kode_obat = $("#dpermintaan_obat_kode_obat").val();
					var nama_obat = $("#dpermintaan_obat_nama_obat").val();
					var nama_jenis_obat = $("#dpermintaan_obat_nama_jenis_obat").val();
					var jumlah_diminta = $("#dpermintaan_obat_jumlah_diminta").val();
					var satuan_diminta = $("#dpermintaan_obat_satuan_diminta").val();
					$("#data_" + r_num + "_id_obat").text(id_obat);
					$("#data_" + r_num + "_kode_obat").text(kode_obat);
					$("#data_" + r_num + "_nama_obat").text(nama_obat);
					$("#data_" + r_num + "_nama_jenis_obat").text(nama_jenis_obat);
					$("#data_" + r_num + "_jumlah_diminta").text(jumlah_diminta);
					$("#data_" + r_num + "_satuan_diminta").text(satuan_diminta);
					$("#data_" + r_num + "_f_jumlah_diminta").text(jumlah_diminta + " " + satuan_diminta);
					$("#dpermintaan_obat_add_form").smodal("hide");
				};
				DPermintaanObatAction.prototype.del = function(r_num) {
					var id = $("#data_" + r_num + "_id").text();
					if (id.length == 0) {
						$("#data_" + r_num).remove();
					} else {
						$("#data_" + r_num).attr("style", "display: none;");
						$("#data_" + r_num).attr("class", "deleted");
					}
				};
				
				function ObatAction(name, page, action, column) {
					this.initialize(name, page, action, column);
				}
				ObatAction.prototype.constructor = ObatAction;
				ObatAction.prototype = new TableAction();
				ObatAction.prototype.selected = function(json) {
					$("#dpermintaan_obat_id_obat").val(json.id);
					$("#dpermintaan_obat_kode_obat").val(json.kode);
					$("#dpermintaan_obat_nama_obat").val(json.nama);
					$("#dpermintaan_obat_nama_jenis_obat").val(json.nama_jenis_barang);
					$("#dpermintaan_obat_satuan_diminta").val(json.satuan);
				};

				var PERMINTAAN_OBAT_PNAME="<?php echo $this->proto_name; ?>";
				var PERMINTAAN_OBAT_PSLUG="<?php echo $this->proto_slug; ?>";
				var PERMINTAAN_OBAT_PIMPL="<?php echo $this->proto_implement; ?>";
				var permintaan_obat;
				var dpermintaan_obat;
				var PERMINTAAN_OBAT_ROW_ID;
				var permintaan_obat_obat;
				var PERMINTAAN_OBAT_PAGE='<?php echo $this->page; ?>';
				
				$(document).ready(function() {
					$('[data-toggle="popover"]').popover({
						trigger: 'hover',
						'placement': 'top'
					});
					$(".mydate").datepicker();					
					permintaan_obat_obat = new ObatAction("permintaan_obat_obat",PERMINTAAN_OBAT_PAGE,"permintaan_obat",new Array());
					permintaan_obat_obat.setSuperCommand("permintaan_obat_obat");
					permintaan_obat_obat.setPrototipe(PERMINTAAN_OBAT_PNAME,PERMINTAAN_OBAT_PSLUG,PERMINTAAN_OBAT_PIMPL);
					var dpermintaan_obat_columns = new Array("id", "nama_obat", "jumlah_permintaan", "satuan_permintaan", "jumlah_dipenuhi", "satuan_dipenuhi", "keterangan");
					dpermintaan_obat = new DPermintaanObatAction("dpermintaan_obat",PERMINTAAN_OBAT_PAGE,"permintaan_obat",dpermintaan_obat_columns);
					dpermintaan_obat.setPrototipe(PERMINTAAN_OBAT_PNAME,PERMINTAAN_OBAT_PSLUG,PERMINTAAN_OBAT_PIMPL);
					var permintaan_obat_columns = new Array("id", "tanggal", "status");
					permintaan_obat = new PermintaanObatAction("permintaan_obat",PERMINTAAN_OBAT_PAGE,"permintaan_obat",permintaan_obat_columns);
					permintaan_obat.setPrototipe(PERMINTAAN_OBAT_PNAME,PERMINTAAN_OBAT_PSLUG,PERMINTAAN_OBAT_PIMPL);
					permintaan_obat.view();
				});
			</script>
			<?php 
		}	
	}
?>