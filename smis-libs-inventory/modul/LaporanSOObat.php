<?php
	require_once("smis-base/smis-include-service-consumer.php");

	class LaporanSOObat extends ModulTemplate {		
		private $db;
		private $name;
		private $tbl_obat_masuk;
		private $tbl_stok_obat;
		private $tbl_penggunaan_obat;
		private $tbl_retur_obat;
		private $tbl_penyesuaian_stok_obat;
		private $tbl_mutasi_keluar;
		private $page;
		private $action;
		private $so_table;
		private $so_form;
		private $so_loading_modal;
		private $proto_name;
		private $proto_implement;
		private $proto_slug;
		
		public function __construct($db, $name_entity, $tbl_obat_masuk, $tbl_stok_obat, $tbl_penggunaan_obat, $tbl_retur_obat, $tbl_penyesuaian_stok_obat, $page){
			$this->db = $db;
			$this->name = $name_entity;
			$this->tbl_obat_masuk = $tbl_obat_masuk;
			$this->tbl_stok_obat = $tbl_stok_obat;
			$this->tbl_penggunaan_obat = $tbl_penggunaan_obat;
			$this->tbl_retur_obat = $tbl_retur_obat;
			$this->tbl_penyesuaian_stok_obat = $tbl_penyesuaian_stok_obat;
			$this->tbl_mutasi_keluar = $tbl_mutasi_keluar;
			$this->page = $page;

			$this->initializeForm();
			$this->initailizeTable();
			$this->initializeLoadingModal();
		}
		
		protected function initializeForm() {
			$this->so_form = new Form("", "", $this->name . " : Stok Opname");
			$tanggal_from_text = new Text("laporan_so_obat_tanggal_from", "laporan_so_obat_tanggal_from", date("Y-m-") . "01");
			$tanggal_from_text->setClass("mydate");
			$tanggal_from_text->setAtribute("data-date-format='yyyy-mm-dd'");
			$this->so_form->addElement("Waktu Awal", $tanggal_from_text);
			$tanggal_to_text = new Text("laporan_so_obat_tanggal_to", "laporan_so_obat_tanggal_to", date("Y-m-d"));
			$tanggal_to_text->setClass("mydate");
			$tanggal_to_text->setAtribute("data-date-format='yyyy-mm-dd'");
			$this->so_form->addElement("Waktu Akhir", $tanggal_to_text);
			$jenis_filter_option = new OptionBuilder();
			$jenis_filter_option->add("SEMUA", "semua", "1");
			$jenis_filter_option->add("PER OBAT", "per_obat");
			$jenis_filter_option->add("PER JENIS", "per_jenis");
			$jenis_filter_select = new Select("laporan_so_obat_jenis_filter", "laporan_so_obat_jenis_filter", $jenis_filter_option->getContent());
			$this->so_form->addElement("Jenis Filter", $jenis_filter_select);
			$kode_jenis_obat_hidden = new Hidden("laporan_so_obat_kode_jenis_obat", "laporan_so_obat_kode_jenis_obat", "");
			$this->so_form->addElement("", $kode_jenis_obat_hidden);
			$id_obat_hidden = new Hidden("laporan_so_obat_id_obat", "laporan_so_obat_id_obat", "");
			$this->so_form->addElement("", $id_obat_hidden);
			$nama_obat_text = new Text("laporan_so_obat_nama_obat", "laporan_so_obat_nama_obat", "");
			$nama_obat_text->setAtribute("disabled='disabled'");
			$nama_obat_text->setClass("smis-one-option-input");
			$browse_button = new Button("", "", "Pilih");
			$browse_button->setClass("btn-info");
			$browse_button->setIcon("fa fa-list");
			$browse_button->setIsButton(Button::$ICONIC);
			$browse_button->setAction("laporan_so_obat_obat.chooser('laporan_so_obat_obat', 'laporan_so_obat_obat_button', 'laporan_so_obat_obat', laporan_so_obat_obat)");
			$input_group = new InputGroup("");
			$input_group->addComponent($nama_obat_text);
			$input_group->addComponent($browse_button);
			$this->so_form->addElement("Obat", $input_group);
			$nama_jenis_obat_text = new Text("laporan_so_obat_nama_jenis_obat", "laporan_so_obat_nama_jenis_obat", "");
			$nama_jenis_obat_text->setAtribute("disabled='disabled'");
			$nama_jenis_obat_text->setClass("smis-one-option-input");
			$browse_button = new Button("", "", "Pilih");
			$browse_button->setClass("btn-info");
			$browse_button->setIcon("fa fa-list");
			$browse_button->setIsButton(Button::$ICONIC);
			$browse_button->setAction("laporan_so_obat_jenis_obat.chooser('laporan_so_obat_jenis_obat', 'laporan_so_obat_jenis_obat_button', 'laporan_so_obat_jenis_obat', laporan_so_obat_jenis_obat)");
			$input_group = new InputGroup("");
			$input_group->addComponent($nama_jenis_obat_text);
			$input_group->addComponent($browse_button);
			$this->so_form->addElement("Jenis Obat", $input_group);
			$urutan_option = new OptionBuilder();
			$urutan_option->addSingle("KODE OBAT", "1");
			$urutan_option->addSingle("NAMA OBAT");
			$urutan_select = new Select("laporan_so_obat_urutan", "laporan_so_obat_urutan", $urutan_option->getContent());
			$this->so_form->addElement("Urutan", $urutan_select);
			$show_button = new Button("", "", "Tampilkan");
			$show_button->setClass("btn-primary");
			$show_button->setIcon("icon-white icon-repeat");
			$show_button->setIsButton(Button::$ICONIC);
			$show_button->setAction("laporan_so_obat.view()");
			$print_button = new Button("", "", "Unduh");
			$print_button->setClass("btn-inverse");
			$print_button->setIcon("fa fa-download");
			$print_button->setIsButton(Button::$ICONIC);
			$print_button->setAtribute("id='laporan_so_obat_export_button'");
			$btn_group = new ButtonGroup("noprint");
			$btn_group->addButton($show_button);
			$btn_group->addButton($print_button);
			$this->so_form->addElement("", $btn_group);
		}

		protected function initailizeTable() {
			$this->so_table = new Table(
				array("No.", "ID Obat", "Kode Obat", "Nama Obat", "Jenis Obat", "Stok Awal", "Penyesuaian", "Penerimaan", "Pengeluaran", "Stok Akhir"), ""
			);
			$this->so_table->setName("laporan_so_obat");
			$this->so_table->setAction(false);
			$this->so_table->setFooterVisible(false);
		}

		protected function initializeLoadingModal() {
			$loading_bar = new LoadingBar("laporan_so_obat_loading_bar", "");
			$button = new Button("", "", "Batal");
			$button->addClass("btn-primary");
			$button->setIsButton(Button::$ICONIC_TEXT);
			$button->setIcon("fa fa-close");
			$button->setAction("laporan_so_obat.cancel()");
			$this->so_loading_modal = new Modal("laporan_so_obat_loading_modal", "", "Proses..");
			$this->so_loading_modal->addHtml($loading_bar->getHtml(), "after");
			$this->so_loading_modal->addFooter($button);
		}

		public function setPrototype($pname,$pslug,$pimplement){
			$this->proto_name = $pname;
			$this->proto_implement = $pimplement;
			$this->proto_slug = $pslug;
		}
		
		public function superCommand($super_command){
			//chooser nama obat:
			$obat_table = new Table(
				array("ID", "Kode", "Nama Obat", "Jenis Obat"),
				"",
				null,
				true
			);
			$obat_table->setName("laporan_so_obat_obat");
			$obat_table->setModel(Table::$SELECT);
			$obat_adapter = new SimpleAdapter();
			$obat_adapter->add("ID", "id");
			$obat_adapter->add("Kode", "kode");
			$obat_adapter->add("Nama Obat", "nama");
			$obat_adapter->add("Jenis Obat", "nama_jenis_barang");
			$obat_dbtable = new DBTable($this->db, "smis_pr_barang");
			$obat_dbtable->addCustomKriteria(" medis ", " = 1 ");
			$obat_dbresponder = new DBResponder(
				$obat_dbtable,
				$obat_table,
				$obat_adapter
			);

			//chooser jenis obat:
			$jenis_obat_table = new Table(
				array("No.", "Kode", "Jenis Obat"),
				"",
				null,
				true
			);
			$jenis_obat_table->setName("laporan_so_obat_jenis_obat");
			$jenis_obat_table->setModel(Table::$SELECT);
			$jenis_obat_adapter = new SimpleAdapter(true, "No.");
			$jenis_obat_adapter->add("Kode", "kode");
			$jenis_obat_adapter->add("Jenis Obat", "nama");
			$jenis_obat_dbtable = new DBTable($this->db, "smis_pr_jenis_barang");
			$jenis_obat_dbtable->addCustomKriteria(" medis ", " = 1 ");
			$jenis_obat_dbresponder = new DBResponder(
				$jenis_obat_dbtable,
				$jenis_obat_table,
				$jenis_obat_adapter
			);
			
			$super_command = new SuperCommand();
			$super_command->addResponder("laporan_so_obat_obat", $obat_dbresponder);
			$super_command->addResponder("laporan_so_obat_jenis_obat", $jenis_obat_dbresponder);
			$init = $super_command->initialize();
			if ($init != null) {
				echo $init;
				return;
			}
		}

		private function getCurrentStock($id_obat, $satuan, $konversi, $satuan_konversi) {
			$dbtable = new DBTable($this->db, $this->tbl_stok_obat);
			$jumlah = 0;
			$row = $dbtable->get_row("
				SELECT SUM(a.sisa) AS 'jumlah'
				FROM " . $this->tbl_stok_obat . " a LEFT JOIN " . $this->tbl_obat_masuk . " b ON a.id_obat_masuk = b.id
				WHERE a.prop NOT LIKE 'del' AND b.prop NOT LIKE 'del' AND b.status = 'sudah' AND a.id_obat = '" . $id_obat . "' AND a.satuan = '" . $satuan_konversi . "' AND a.konversi = '1' AND a.satuan_konversi = '" . $satuan_konversi . "'
			");
			$jumlah += $row->jumlah;
			$row = $dbtable->get_row("
				SELECT SUM(a.sisa * a.konversi) AS 'jumlah'
				FROM " . $this->tbl_stok_obat . " a LEFT JOIN " . $this->tbl_obat_masuk . " b ON a.id_obat_masuk = b.id
				WHERE a.prop NOT LIKE 'del' AND b.prop NOT LIKE 'del' AND b.status = 'sudah' AND a.id_obat = '" . $id_obat . "' AND a.konversi > 1 AND a.satuan_konversi = '" . $satuan_konversi . "'
			");
			$jumlah += $row->jumlah;
			return $jumlah;
		}

		private function getStockIn($id_obat, $satuan, $konversi, $satuan_konversi, $tanggal_from, $tanggal_to) {
			$dbtable = new DBTable($this->db, $this->tbl_obat_masuk);
			$jumlah = 0;
			// penerimaan mutasi:
			$row = $dbtable->get_row("
				SELECT SUM(a.jumlah) AS 'jumlah'
				FROM " . $this->tbl_stok_obat . " a LEFT JOIN " . $this->tbl_obat_masuk . " b ON a.id_obat_masuk = b.id
				WHERE a.prop NOT LIKE 'del' AND b.prop NOT LIKE 'del'  AND b.status = 'sudah' AND a.id_obat = '" . $id_obat . "' AND a.satuan = '" . $satuan . "' AND a.konversi = '" . $konversi . "' AND a.satuan_konversi = '" . $satuan_konversi . "'  AND b.tanggal >= '" . $tanggal_from . "' AND b.tanggal <= '" . $tanggal_to . "'
			");
			$jumlah += $row->jumlah;
			return $jumlah;
		}

		private function getStockOut($id_obat, $satuan, $konversi, $satuan_konversi, $tanggal_from, $tanggal_to) {
			$dbtable = new DBTable($this->db, $this->tbl_penggunaan_obat);
			$jumlah = 0;
			// penggunaan :
			$row = $dbtable->get_row("
				SELECT SUM(jumlah) AS 'jumlah'
				FROM " . $this->tbl_penggunaan_obat . " a LEFT JOIN " . $this->tbl_stok_obat . " b ON a.id_stok_obat = b.id
				WHERE b.id_obat = '" . $id_obat . "' AND a.prop NOT LIKE 'del' AND a.tanggal >= '" . $tanggal_from . "' AND a.tanggal <= '" . $tanggal_to . "'
			");
			$jumlah += $row->jumlah;
			// retur ke gudang :
			$row = $dbtable->get_row("
				SELECT SUM(jumlah) AS 'jumlah'
				FROM " . $this->tbl_retur_obat . " a LEFT JOIN " . $this->tbl_stok_obat . " b ON a.id_stok_obat = b.id
				WHERE b.id_obat = '" . $id_obat . "' AND a.prop NOT LIKE 'del' AND a.tanggal >= '" . $tanggal_from . "' AND a.tanggal <= '" . $tanggal_to . "'
			");
			$jumlah += $row->jumlah;
			// mutasi obat keluar:
			$row = $dbtable->get_row("
				SELECT SUM(jumlah) AS 'jumlah'
				FROM " . $this->tbl_mutasi_keluar . "
				WHERE prop NOT LIKE 'del' AND id_obat = '" . $id_obat . "' AND waktu >= '" . $tanggal_from . "' AND waktu <= '" . $tanggal_to . "'
			");
			if ($row != null)
				$jumlah += $row->jumlah;
			return $jumlah;
		}

		private function getPenyesuaianStokPositif($id_obat, $satuan, $konversi, $satuan_konversi, $tanggal_from, $tanggal_to) {
			$dbtable = new DBTable($this->db, $this->tbl_penyesuaian_stok_obat);
			$jumlah = 0;
			$row = $dbtable->get_row("
				SELECT SUM(a.jumlah_baru - a.jumlah_lama) AS 'jumlah'
				FROM " . $this->tbl_penyesuaian_stok_obat . " a LEFT JOIN " . $this->tbl_stok_obat . " b ON a.id_stok_obat = b.id
				WHERE a.prop NOT LIKE 'del' AND b.id_obat = '" . $id_obat . "' AND b.satuan = '" . $satuan . "' AND b.konversi = '" . $konversi . "' AND b.satuan_konversi = '" . $satuan_konversi . "' AND a.tanggal >= '" . $tanggal_from . "' AND a.tanggal <= '" . $tanggal_to . "' AND a.jumlah_baru >= a.jumlah_lama
			");
			$jumlah += $row->jumlah;
			return $jumlah;
		}

		private function getPenyesuaianStokNegatif($id_obat, $satuan, $konversi, $satuan_konversi, $tanggal_from, $tanggal_to) {
			$dbtable = new DBTable($this->db, $this->tbl_penyesuaian_stok_obat);
			$jumlah = 0;
			$row = $dbtable->get_row("
				SELECT SUM(a.jumlah_baru - a.jumlah_lama) AS 'jumlah'
				FROM " . $this->tbl_penyesuaian_stok_obat . " a LEFT JOIN " . $this->tbl_stok_obat . " b ON a.id_stok_obat = b.id
				WHERE a.prop NOT LIKE 'del' AND b.id_obat = '" . $id_obat . "' AND b.satuan = '" . $satuan . "' AND b.konversi = '" . $konversi . "' AND b.satuan_konversi = '" . $satuan_konversi . "' AND a.tanggal >= '" . $tanggal_from . "' AND a.tanggal <= '" . $tanggal_to . "' AND a.jumlah_baru < a.jumlah_lama
			");
			$jumlah += $row->jumlah;
			return $jumlah;
		}

		public function command($command){
			if ($_POST['command'] == "get_jumlah_obat") {
				$jenis_filter = $_POST['jenis_filter'];
				$params = array();
				$params['order_by'] = $_POST['urutan'];
				if ($jenis_filter == "per_obat")
					$params['filter_id_obat'] = $_POST['id_obat'];
				else
					$params['filter_id_obat'] = "%%";
				if ($jenis_filter == "per_jenis")
					$params['filter_kode_jenis_obat'] = $_POST['kode_jenis_obat'];
				else
					$params['filter_kode_jenis_obat'] = "%%";
				$consumer_service = new ServiceConsumer(
					$this->db,
					"get_jumlah_obat_msrs",
					$params,
					"perencanaan"
				);
				$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
				$content = $consumer_service->execute()->getContent();
				$jumlah = 0;
				if ($content != null)
					$jumlah = $content[0];
				$data = array();
				$data['jumlah'] = $jumlah;
				echo json_encode($data);
			} else if ($_POST['command'] == "get_info_obat") {
				$tanggal_from = $_POST['tanggal_from'];
				$tanggal_to = $_POST['tanggal_to'];
				$jenis_filter = $_POST['jenis_filter'];
				$num = $_POST['num'];
				$params = array();
				$params['num'] = $_POST['num'];
				$params['order_by'] = $_POST['urutan'];
				if ($jenis_filter == "per_obat")
					$params['filter_id_obat'] = $_POST['id_obat'];
				else
					$params['filter_id_obat'] = "%%";
				if ($jenis_filter == "per_jenis")
					$params['filter_kode_jenis_obat'] = $_POST['kode_jenis_obat'];
				else
					$params['filter_kode_jenis_obat'] = "%%";
				$consumer_service = new ServiceConsumer(
					$this->db,
					"get_obat_info_msrs",
					$params,
					"perencanaan"
				);
				$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
				$content = $consumer_service->execute()->getContent();
				$id_obat = $content[0];
				$kode_obat = $content[1];
				$nama_obat = $content[2];
				$nama_jenis_obat = $content[3];
				$satuan = $content[5];
				$satuan_konversi = $content[5];

				// Mendapatkan Saldo Akhir:
				$saldo_sekarang = $this->getCurrentStock($id_obat, $satuan, 1, $satuan_konversi);
				$jumlah_masuk = $this->getStockIn($id_obat, $satuan, 1, $satuan_konversi, date("Y-m-d", strtotime($tanggal_to . " +1 day")), date("Y-m-d")) + $this->getPenyesuaianStokPositif($id_obat, $satuan, 1, $satuan_konversi, date("Y-m-d", strtotime($tanggal_to . "+1 day")), date("Y-m-d"));
				$jumlah_keluar = $this->getStockOut($db, $id_obat, $satuan, 1, $satuan_konversi,date("Y-m-d", strtotime($tanggal_to . " +1 day")), date("Y-m-d")) + $this->getPenyesuaianStokNegatif($id_obat, $satuan, 1, $satuan_konversi, date("Y-m-d", strtotime($tanggal_to . "+1 day")), date("Y-m-d"));
				$saldo_akhir = $saldo_sekarang - $jumlah_masuk + $jumlah_keluar;
				// Mendapatkan Penyesuaian Stok:
				$penyesuaian_stok = $this->getPenyesuaianStokPositif($id_obat, $satuan, 1, $satuan_konversi, $tanggal_from, $tanggal_to) - $this->getPenyesuaianStokNegatif($id_obat, $satuan, 1, $satuan_konversi, $tanggal_from, $tanggal_to);
				// Mendapatkan Jumlah Masuk:
				$jumlah_masuk = $this->getStockIn($id_obat, $satuan, 1, $satuan_konversi, $tanggal_from, $tanggal_to);
				// Mendapatkan Jumlah Keluar:
				$jumlah_keluar = $this->getStockOut($id_obat, $satuan, 1, $satuan_konversi, $tanggal_from, $tanggal_to);
				// Mendapatkan Saldo Awal:
				$saldo_awal = $saldo_akhir - $jumlah_masuk + $jumlah_keluar - $penyesuaian_stok;

				$html = "
					<tr>
						<td id='nomor'></td>
						<td id='id_obat'><small>" .  $id_obat . "</small></td>
						<td id='kode_obat'><small>" .  $kode_obat . "</small></td>
						<td id='nama_obat'><small>" .  $nama_obat . "</small></td>
						<td id='nama_jenis_obat'><small>" .  $nama_jenis_obat . "</small></td>
						<td id='saldo_awal' style='display: none;'><small>" .  $saldo_awal . "</small></td>
						<td id='f_saldo_awal'><small>" .  ArrayAdapter::format("number", $saldo_awal) . "</small></td>
						<td id='penyesuaian_stok' style='display: none;'><small>" .  $penyesuaian_stok . "</small></td>
						<td id='f_penyesuaian_stok'><small>" .  ArrayAdapter::format("number", $penyesuaian_stok) . "</small></td>
						<td id='jumlah_masuk' style='display: none;'><small>" .  $jumlah_masuk . "</small></td>
						<td id='f_jumlah_masuk'><small>" .  ArrayAdapter::format("number", $jumlah_masuk) . "</small></td>
						<td id='jumlah_keluar' style='display: none;'><small>" .  $jumlah_keluar . "</small></td>
						<td id='f_jumlah_keluar'><small>" .  ArrayAdapter::format("number", $jumlah_keluar) . "</small></td>
						<td id='saldo_akhir' style='display: none;'><small>" .  $saldo_akhir . "</small></td>
						<td id='f_saldo_akhir'><small>" .  ArrayAdapter::format("number", $saldo_akhir) . "</small></td>
					</tr>
				";

				$data = array();
				$data['id_obat'] = ArrayAdapter::format("only-digit6", $id_obat);
				$data['kode_obat'] = $kode_obat;
				$data['nama_obat'] = $nama_obat;
				$data['nama_jenis_obat'] = $nama_jenis_obat;
				$data['html'] = $html;
				echo json_encode($data);
			} else if ($_POST['command'] == "export_xls") {
				$tanggal_from = $_POST['tanggal_from'];
				$tanggal_to = $_POST['tanggal_to'];
				require_once("smis-libs-out/php-excel/PHPExcel.php");
				$objPHPExcel = PHPExcel_IOFactory::load("smis-libs-inventory/templates/template_persediaan_obat.xlsx");
				$objPHPExcel->setActiveSheetIndexByName("STOK OPNAME");
				$objWorksheet = $objPHPExcel->getActiveSheet();
				$objWorksheet->setCellValue("B2", ArrayAdapter::format("unslug", $this->name));
				$objWorksheet->setCellValue("B3", "PERIODE : " . ArrayAdapter::format("date d-m-Y", $tanggal_from) . " s/d " . ArrayAdapter::format("date d-m-Y", $tanggal_to));
				$data = json_decode($_POST['d_data']);
				if ($_POST['num_rows'] - 2 > 0)
					$objWorksheet->insertNewRowBefore(8, $_POST['num_rows'] - 2);
				$start_row_num = 7;
				$end_row_num = 7;
				$row_num = $start_row_num;
				foreach ($data as $d) {
					$col_num = 1;
					$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->nomor);
					$col_num++;
					$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->id_obat);
					$col_num++;
					$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->kode_obat);
					$col_num++;
					$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->nama_obat);
					$col_num++;
					$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->saldo_awal);
					$col_num++;
					$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->penyesuaian_stok);
					$col_num++;
					$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->jumlah_masuk);
					$col_num++;
					$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->jumlah_keluar);
					$col_num++;
					$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->saldo_akhir);
					$objWorksheet->getStyle("F" . $row_num)->getNumberFormat()->setFormatCode("#,##0");
					$objWorksheet->getStyle("G" . $row_num)->getNumberFormat()->setFormatCode("#,##0");
					$objWorksheet->getStyle("H" . $row_num)->getNumberFormat()->setFormatCode("#,##0");
					$objWorksheet->getStyle("I" . $row_num)->getNumberFormat()->setFormatCode("#,##0");
					$objWorksheet->getStyle("J" . $row_num)->getNumberFormat()->setFormatCode("#,##0");
					$row_num++;
					$end_row_num++;
					$no++;
				}
				header("Content-type: application/vnd.ms-excel");	
				header("Content-Disposition: attachment; filename=STOK_OPNAME_OBAT_" . ArrayAdapter::format("unslug", $this->name) . "_" . ArrayAdapter::format("date Ymd", $tanggal_from) . "_" . ArrayAdapter::format("date Ymd", $tanggal_to) . ".xlsx");
				$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");
				$objWriter->save("php://output");
			}
			return;
		}
		
		public function phpPreLoad(){
			echo $this->so_loading_modal->getHtml();	
			echo $this->so_form->getHtml();
			echo $this->so_table->getHtml();
			echo "<div id='laporan_so_obat_info'></div>";
			echo addJS("framework/smis/js/table_action.js");
			echo addJS("framework/bootstrap/js/bootstrap-datepicker.js");
			echo addCSS("framework/bootstrap/css/datepicker.css");
			echo addJS("base-js/smis-base-loading.js");
		}
		
		public function jsPreLoad(){
			?>
			<script type="text/javascript">
				function LaporanSOObatAction(name, page, action, column) {
					this.initialize(name, page, action, column);
				}
				LaporanSOObatAction.prototype.constructor = LaporanSOObatAction;
				LaporanSOObatAction.prototype = new TableAction();
				LaporanSOObatAction.prototype.getRegulerData = function() {
					var data = TableAction.prototype.getRegulerData.call(this);
					data['tanggal_from'] = $("#laporan_so_obat_tanggal_from").val();
					data['tanggal_to'] = $("#laporan_so_obat_tanggal_to").val();
					data['jenis_filter'] = $("#laporan_so_obat_jenis_filter").val();
					data['id_obat'] = $("#laporan_so_obat_id_obat").val();
					data['nama_obat'] = $("#laporan_so_obat_nama_obat").val();
					data['nama_jenis_obat'] = $("#laporan_so_obat_nama_jenis_obat").val();
					data['kode_jenis_obat'] = $("#laporan_so_obat_kode_jenis_obat").val();
					data['urutan'] = $("#laporan_so_obat_urutan").val();
					return data;
				};
				LaporanSOObatAction.prototype.view = function() {
					var self = this;
					$("#laporan_so_obat_info").empty();
					$("#laporan_so_obat_loading_bar").sload("true", "Harap ditunggu...", 0);
					$("#laporan_so_obat_loading_modal").smodal("show");
					FINISHED = false;
					var data = this.getRegulerData();
					data['command'] = "get_jumlah_obat";
					$.post(
						"",
						data,
						function(response) {
							var json = JSON.parse(response);
							if (json == null) return;
							$("#laporan_so_obat_list").empty();
							self.fillHtml(0, json.jumlah);
						}
					);
				};
				LaporanSOObatAction.prototype.fillHtml = function(num, limit) {
					if (FINISHED || num == limit) {
						if (FINISHED == false && num == limit) {
							this.finalize();
						} else {
							$("#laporan_so_obat_loading_modal").smodal("hide");
							$("#laporan_so_obat_info").html(
								"<div class='alert alert-block alert-inverse'>" +
									 "<center><strong>PROSES DIBATALKAN</strong></center>" +
								 "</div>"
							);
							$("#laporan_so_obat_export_button").removeAttr("onclick");
						}
						return;
					}
					var self = this;
					var data = this.getRegulerData();
					data['command'] = "get_info_obat";
					data['num'] = num;
					$.post(
						"",
						data,
						function(response) {
							var json = JSON.parse(response);
							if (json == null) return;
							$("tbody#laporan_so_obat_list").append(
								json.html
							);
							$("#laporan_so_obat_loading_bar").sload("true", json.id_obat + " - " + json.kode_obat + " - " + json.nama_obat + " - " + json.nama_jenis_obat + " (" + (num + 1) + " / " + limit + ")", (num + 1) * 100 / limit - 1);
							self.fillHtml(num + 1, limit);
						}
					);
				};
				LaporanSOObatAction.prototype.finalize = function() {
					var num_rows = $("tbody#laporan_so_obat_list tr").length;
					for (var i = 0; i < num_rows; i++)
						$("tbody#laporan_so_obat_list tr:eq(" + i + ") td#nomor").html("<small>" + (i + 1) + "</small>");
					$("#laporan_so_obat_loading_modal").smodal("hide");
					$("#laporan_so_obat_info").html(
						"<div class='alert alert-block alert-info'>" +
							 "<center><strong>PROSES SELESAI</strong></center>" +
						 "</div>"
					);
					$("#laporan_so_obat_export_button").removeAttr("onclick");
					$("#laporan_so_obat_export_button").attr("onclick", "laporan_so_obat.export_xls()");
				};
				LaporanSOObatAction.prototype.cancel = function() {
					FINISHED = true;
				};
				LaporanSOObatAction.prototype.export_xls = function() {
					showLoading();
					var num_rows = $("#laporan_so_obat_list").children("tr").length;
					var d_data = {};
					for (var i = 0; i < num_rows; i++) {
						var nomor = $("tbody#laporan_so_obat_list tr:eq(" + i + ") td#nomor").text();
						var id_obat = $("tbody#laporan_so_obat_list tr:eq(" + i + ") td#id_obat").text();
						var kode_obat = $("tbody#laporan_so_obat_list tr:eq(" + i + ") td#kode_obat").text();
						var nama_obat = $("tbody#laporan_so_obat_list tr:eq(" + i + ") td#nama_obat").text();
						var nama_jenis_obat = $("tbody#laporan_so_obat_list tr:eq(" + i + ") td#nama_jenis_obat").text();
						var saldo_awal = $("tbody#laporan_so_obat_list tr:eq(" + i + ") td#saldo_awal").text();
						var penyesuaian_stok = $("tbody#laporan_so_obat_list tr:eq(" + i + ") td#penyesuaian_stok").text();
						var jumlah_masuk = $("tbody#laporan_so_obat_list tr:eq(" + i + ") td#jumlah_masuk").text();
						var jumlah_keluar = $("tbody#laporan_so_obat_list tr:eq(" + i + ") td#jumlah_keluar").text();
						var saldo_akhir = $("tbody#laporan_so_obat_list tr:eq(" + i + ") td#saldo_akhir").text();
						d_data[i] = {
							"nomor" 			: nomor,
							"id_obat" 			: id_obat,
							"kode_obat" 		: kode_obat,
							"nama_obat" 		: nama_obat,
							"nama_jenis_obat"	: nama_jenis_obat,
							"saldo_awal" 		: saldo_awal,
							"penyesuaian_stok"	: penyesuaian_stok,
							"jumlah_masuk" 		: jumlah_masuk,
							"jumlah_keluar" 	: jumlah_keluar,
							"saldo_akhir" 		: saldo_akhir
						};
					}
					var data = this.getRegulerData();
					data['command'] = "export_xls";
					data['tanggal_from'] = $("#laporan_so_obat_tanggal_from").val();
					data['tanggal_to'] = $("#laporan_so_obat_tanggal_to").val();
					data['d_data'] = JSON.stringify(d_data);
					data['num_rows'] = num_rows;
					postForm(data);
					dismissLoading();
				};

				function LaporanSOObatObatAction(name, page, action, column) {
					this.initialize(name, page, action, column);
				}
				LaporanSOObatObatAction.prototype.constructor = LaporanSOObatObatAction;
				LaporanSOObatObatAction.prototype = new TableAction();
				LaporanSOObatObatAction.prototype.selected = function(json) {
					$("#laporan_so_obat_id_obat").val(json.id);
					$("#laporan_so_obat_nama_obat").val(json.nama);
				};

				function LaporanSOObatJenisObatAction(name, page, action, column) {
					this.initialize(name, page, action, column);
				}
				LaporanSOObatJenisObatAction.prototype.constructor = LaporanSOObatJenisObatAction;
				LaporanSOObatJenisObatAction.prototype = new TableAction();
				LaporanSOObatJenisObatAction.prototype.selected = function(json) {
					$("#laporan_so_obat_kode_jenis_obat").val(json.kode);
					$("#laporan_so_obat_nama_jenis_obat").val(json.nama);
				};
				
				var laporan_so_obat;
				var laporan_so_obat_obat;
				var laporan_so_obat_jenis_obat;
				var FINISHED;
				var LAPORAN_SO_OBAT_PNAME="<?php echo $this->proto_name; ?>";
				var LAPORAN_SO_OBAT_PSLUG="<?php echo $this->proto_slug; ?>";
				var LAPORAN_SO_OBAT_PIMPL="<?php echo $this->proto_implement; ?>";
				var LAPORAN_SO_OBAT_ENTITY="<?php echo $this->page; ?>";
				$(document).ready(function() {
					$('[data-toggle="popover"]').popover({
						trigger: 'hover',
						'placement': 'top'
					});
					laporan_so_obat_obat = new LaporanSOObatObatAction(
						"laporan_so_obat_obat",
						LAPORAN_SO_OBAT_ENTITY,
						"laporan_so_obat",
						new Array()
					);
					laporan_so_obat_obat.setPrototipe(LAPORAN_SO_OBAT_PNAME, LAPORAN_SO_OBAT_PSLUG, LAPORAN_SO_OBAT_PIMPL);
					laporan_so_obat_obat.setSuperCommand("laporan_so_obat_obat");
					laporan_so_obat_jenis_obat = new LaporanSOObatJenisObatAction(
						"laporan_so_obat_jenis_obat",
						LAPORAN_SO_OBAT_ENTITY,
						"laporan_so_obat",
						new Array()
					);
					laporan_so_obat_jenis_obat.setPrototipe(LAPORAN_SO_OBAT_PNAME, LAPORAN_SO_OBAT_PSLUG, LAPORAN_SO_OBAT_PIMPL);
					laporan_so_obat_jenis_obat.setSuperCommand("laporan_so_obat_jenis_obat");
					laporan_so_obat = new LaporanSOObatAction(
						"laporan_so_obat",
						LAPORAN_SO_OBAT_ENTITY,
						"laporan_so_obat",	
						new Array()
					);
					laporan_so_obat.setPrototipe(LAPORAN_SO_OBAT_PNAME, LAPORAN_SO_OBAT_PSLUG, LAPORAN_SO_OBAT_PIMPL);
					$("#laporan_so_obat > div.form-container > form > div:nth-child(7)").hide();
					$("#laporan_so_obat > div.form-container > form > div:nth-child(8)").hide();
					$("#laporan_so_obat_jenis_filter").on("change", function() {
						var jenis_filter = $("#laporan_so_obat_jenis_filter").val();
						$("#laporan_so_obat_id_obat").val("");
						$("#laporan_so_obat_nama_obat").val("");
						$("#laporan_so_obat_kode_jenis_obat").val("");
						$("#laporan_so_obat_jenis_obat").val("");
						if (jenis_filter == "semua") {
							$("#laporan_so_obat > div.form-container > form > div:nth-child(7)").hide();
							$("#laporan_so_obat > div.form-container > form > div:nth-child(8)").hide();
						} else if (jenis_filter == "per_obat") {
							$("#laporan_so_obat > div.form-container > form > div:nth-child(7)").show();
							$("#laporan_so_obat > div.form-container > form > div:nth-child(8)").hide();
						} else if (jenis_filter == "per_jenis") {
							$("#laporan_so_obat > div.form-container > form > div:nth-child(7)").hide();
							$("#laporan_so_obat > div.form-container > form > div:nth-child(8)").show();
						}
					});
					$("tbody#laporan_so_obat_list").append(
						"<tr>" +
							"<td colspan='10'><strong><center><small>DATA PERSEDIAAN OBAT BELUM DIPROSES</small></center></strong></td>" +
						"</tr>"
					);
					$(document).keyup(function(e) {
						if (e.which == 27) {
							FINISHED = true;
						}
					})
				});
			</script>		
			<?php 
		}	
	}
?>