<?php 
	require_once 'smis-framework/smis/template/ModulTemplate.php';
	require_once 'smis-base/smis-include-service-consumer.php';

	class DaftarInventaris extends ModulTemplate{
		
		private $db;
		private $name;
		private $tbl_inventaris;
		private $page;
		private $action;
		private $inventaris_table;
		private $proto_name;
		private $proto_implement;
		private $proto_slug;
		
		public function __construct($db, $name_entity, $slug_table_parent, $page){
			$this->db=$db;
			$this->name=$name_entity;
			$this->tbl_inventaris=$slug_table_parent;
			$this->page=$page;
			$header=array("Nomor", "Kode", "Jenis", "Barang", "Merk", "Harga", "Tahun", "Penyusutan", "Kondisi");
			$this->inventaris_table = new Table($header, $this->name . " : Daftar Inventaris");
			$this->inventaris_table->setName("inventaris");
			$this->inventaris_table->setReloadButtonEnable(false);
			$this->inventaris_table->setPrintButtonEnable(false);
			$this->inventaris_table->setAddButtonEnable(false);
			$this->inventaris_table->setEditButtonEnable(false);
			$this->inventaris_table->setDelButtonEnable(false);
			$btn = new Button("", "", "Return");
			$btn->setClass("btn-danger");
			$btn->setIcon("fa fa-share");
			$btn->setIsButton(Button::$ICONIC);
			$this->inventaris_table->addContentButton("retur", $btn);
			$this->proto_name="";
			$this->proto_implement="";
			$this->proto_slug="";
		}
		
		public function setPrototype($pname,$pslug,$pimplement){
			$this->proto_name=$pname;
			$this->proto_implement=$pimplement;
			$this->proto_slug=$pslug;
		}
		
		public function command($command){
			$inventaris_adapter=new SimpleAdapter();
			$inventaris_adapter->add("Nomor", "f_id","digit8");
			$inventaris_adapter->add("Kode", "kode");
			$inventaris_adapter->add("Jenis", "medis","trivial_1_Medis_Non-Medis");
			$inventaris_adapter->add("Barang", "nama_barang");
			$inventaris_adapter->add("Merk", "merk");
			$inventaris_adapter->add("Harga", "hna","money Rp.");
			$inventaris_adapter->add("Tahun", "tahun_perolehan", "date d-m-Y");
			$inventaris_adapter->add("Penyusutan", "usia_penyusutan", "back Tahun");
			$inventaris_adapter->add("Kondisi", "kondisi_baik","trivial_1_Baik_Rusak");
			$inventaris_dbtable = new DBTable($this->db,$this->tbl_inventaris);
			$inventaris_dbresponder = new DBResponder($inventaris_dbtable,$this->inventaris_table,$inventaris_adapter);
			$data = $inventaris_dbresponder->command($_POST['command']);
			if (isset($_POST['push_command'])) {
				$id=$_POST['id'];
				$inventaris_row = $inventaris_dbtable->select($id);
				$command = "push_" . $_POST['push_command'];
				$array = array();
				$array['f_id'] = $inventaris_row->f_id;
				$array['command'] = $command;
				$return_inventaris_service_consumer = new ServiceConsumer($this->db, "return_inventaris",$array,"gudang_umum");
				$return_inventaris_service_consumer->execute();
			}
			echo json_encode($data);
			return;
		}
		
		public function phpPreLoad(){
			echo $this->inventaris_table->getHtml();
			echo addJS("framework/smis/js/table_action.js");
		}
		
		public function jsPreLoad(){
			?>				
			<script type="text/javascript">
				var DAFTAR_INVENTARIS_PNAME="<?php echo $this->proto_name; ?>";
				var DAFTAR_INVENTARIS_PSLUG="<?php echo $this->proto_slug; ?>";
				var DAFTAR_INVENTARIS_PIMPL="<?php echo $this->proto_implement; ?>";
				var DAFTAR_INVENTARIS_ENTITY="<?php echo $this->page; ?>";
				var inventaris;
				$(document).ready(function() {
					$('[data-toggle="popover"]').popover({
						trigger: 'hover',
						'placement': 'top'
					});
					var inventaris_columns = new Array("id", "f_id", "nama_barang", "medis", "kode", "merk", "hna", "tahun_perolehan", "usia_penyusutan", "kondisi_baik");
					inventaris = new TableAction("inventaris",DAFTAR_INVENTARIS_ENTITY,"daftar_inventaris",inventaris_columns);
					inventaris.setPrototipe(DAFTAR_INVENTARIS_PNAME,DAFTAR_INVENTARIS_PSLUG,DAFTAR_INVENTARIS_PIMPL);
					inventaris.retur = function(id) {
						var self = this;
						bootbox.confirm(
							"Yakin mengembalikan Inventaris ini?",
							function(result) {
								if (result) {
									showLoading();
									var data = self.getRegulerData();
									data['command'] = "del";
									data['push_command'] = "return";
									data['id'] = id;
									$.post(
										"",
										data,
										function() {
											self.view();
											dismissLoading();
										}
									);
								}
							}
						);
					};
					inventaris.view();
				});
			</script>
			<?php 
		}	
	}
?>