<?php 
	require_once 'smis-framework/smis/template/ModulTemplate.php';
	require_once 'smis-libs-inventory/responder/LaporanRekapPerbekalanObatPerJenisDBResponder.php';
	require_once 'smis-libs-inventory/responder/JenisObatDBResponder.php';
	
	class LaporanRekapPerbekalanObatPerJenis extends ModulTemplate {
		private $db;
		private $name;
		private $tbl_stok_obat;
		private $tbl_obat_masuk;
		private $page;
		private $action;
		private $uitable;
		private $proto_name;
		private $proto_implement;
		private $proto_slug;
		
		public function __construct($db, $name_entity, $dbslug, $page, $entity="") {
			$this->db = $db;
			$this->name = $name_entity;
			if ($entity != "") $entity = "_" . $entity;
			$this->tbl_stok_obat = "smis_" . $dbslug . "_stok_obat" . $entity;
			$this->tbl_obat_masuk = "smis_" . $dbslug . "_obat_masuk" . $entity;
			$this->page = $page;
			$header = array("Nama Obat", "Jenis Obat", "Jumlah", "Satuan", "Produsen", "Tgl. Exp.", "H. Netto");
			$this->uitable = new Table($header, "", null, true);
			$this->uitable->setAction(false);
			$this->uitable->setName("lrrsoj");
			$this->proto_name = "";
			$this->proto_implement = "";
			$this->proto_slug = "";
		}
		
		public function setPrototype($pname, $pslug, $pimplement){
			$this->proto_name = $pname;
			$this->proto_implement = $pimplement;
			$this->proto_slug = $pslug;
		}
		
		public function command($command){		
			$adapter = new SimpleAdapter();
			$adapter->add("Nama Obat", "nama_obat");
			$adapter->add("Jenis Obat", "nama_jenis_obat");
			$adapter->add("Jumlah", "sisa");
			$adapter->add("Satuan", "satuan");
			$adapter->add("Produsen", "produsen");
			$adapter->add("Tgl. Exp.", "tanggal_exp", "date d M Y");
			$adapter->add("H. Netto", "hna", "money Rp.");
			$dbtable = new DBTable($this->db, $this->tbl_stok_obat);
			$filter = "1";
			if (isset($_POST['kriteria'])) {
				$filter .= " AND (nama_obat LIKE '%" . $_POST['kriteria'] . "%' OR produsen LIKE '%" . $_POST['kriteria'] . "%')";
			}
			$query_value = "
				SELECT *
				FROM (
					SELECT " . $this->tbl_stok_obat . ".nama_obat AS 'id', " . $this->tbl_stok_obat . ".nama_obat, " . $this->tbl_stok_obat . ".nama_jenis_obat, SUM(" . $this->tbl_stok_obat . ".sisa) AS 'sisa', " . $this->tbl_stok_obat . ".satuan, " . $this->tbl_stok_obat . ".konversi, " . $this->tbl_stok_obat . ".satuan_konversi, " . $this->tbl_stok_obat . ".produsen, " . $this->tbl_stok_obat . ".tanggal_exp, " . $this->tbl_stok_obat . ".hna
					FROM " . $this->tbl_stok_obat . " LEFT JOIN " . $this->tbl_obat_masuk . " ON " . $this->tbl_stok_obat . ".id_obat_masuk = " . $this->tbl_obat_masuk . ".id
					WHERE " . $this->tbl_stok_obat . ".prop NOT LIKE 'del' AND " . $this->tbl_obat_masuk . ".prop NOT LIKE 'del' AND " . $this->tbl_obat_masuk . ".status = 'sudah' AND " . $this->tbl_stok_obat . ".nama_jenis_obat = '" . $_POST['nama_jenis_obat'] . "'
					GROUP BY " . $this->tbl_stok_obat . ".id_obat, " . $this->tbl_stok_obat . ".satuan, " . $this->tbl_stok_obat . ".konversi, " . $this->tbl_stok_obat . ".satuan_konversi, " . $this->tbl_stok_obat . ".produsen, " . $this->tbl_stok_obat . ".tanggal_exp, " . $this->tbl_stok_obat . ".hna
					HAVING sisa > 0
					ORDER BY " . $this->tbl_stok_obat . ".nama_obat, " . $this->tbl_stok_obat . ".nama_jenis_obat, " . $this->tbl_stok_obat . ".satuan, " . $this->tbl_stok_obat . ".produsen, " . $this->tbl_stok_obat . ".tanggal_exp ASC
				) v_stok
				WHERE " . $filter . "
			";
			$query_count = "
				SELECT COUNT(*)
				FROM (
					SELECT " . $this->tbl_stok_obat . ".nama_obat AS 'id', " . $this->tbl_stok_obat . ".nama_obat, " . $this->tbl_stok_obat . ".nama_jenis_obat, SUM(" . $this->tbl_stok_obat . ".sisa) AS 'sisa', " . $this->tbl_stok_obat . ".satuan, " . $this->tbl_stok_obat . ".konversi, " . $this->tbl_stok_obat . ".satuan_konversi, " . $this->tbl_stok_obat . ".produsen, " . $this->tbl_stok_obat . ".tanggal_exp, " . $this->tbl_stok_obat . ".hna
					FROM " . $this->tbl_stok_obat . " LEFT JOIN " . $this->tbl_obat_masuk . " ON " . $this->tbl_stok_obat . ".id_obat_masuk = " . $this->tbl_obat_masuk . ".id
					WHERE " . $this->tbl_stok_obat . ".prop NOT LIKE 'del' AND " . $this->tbl_obat_masuk . ".prop NOT LIKE 'del' AND " . $this->tbl_obat_masuk . ".status = 'sudah' AND " . $this->tbl_stok_obat . ".nama_jenis_obat = '" . $_POST['nama_jenis_obat'] . "'
					GROUP BY " . $this->tbl_stok_obat . ".id_obat, " . $this->tbl_stok_obat . ".satuan, " . $this->tbl_stok_obat . ".konversi, " . $this->tbl_stok_obat . ".satuan_konversi, " . $this->tbl_stok_obat . ".produsen, " . $this->tbl_stok_obat . ".tanggal_exp, " . $this->tbl_stok_obat . ".hna
					HAVING sisa > 0
					ORDER BY " . $this->tbl_stok_obat . ".nama_obat, " . $this->tbl_stok_obat . ".nama_jenis_obat, " . $this->tbl_stok_obat . ".satuan, " . $this->tbl_stok_obat . ".produsen, " . $this->tbl_stok_obat . ".tanggal_exp ASC
				) v_stok
				WHERE " . $filter . "
			";
			$dbtable->setPreferredQuery(true, $query_value, $query_count);
			$dbresponder = new LaporanRekapPerbekalanObatPerJenisDBResponder(
				$dbtable,
				$this->uitable,
				$adapter,
				$this->tbl_obat_masuk,
				$this->tbl_stok_obat,
				$this->page
			);
			$data = $dbresponder->command($_POST['command']);
			echo json_encode($data);
			return;
		}
		
		public function superCommand($super_command) {
			$jenis_lrrsoj_table = new Table(
				array("Jenis Obat"),
				"",
				null,
				true
			);
			$jenis_lrrsoj_table->setName("jenis_lrrsoj");
			$jenis_lrrsoj_table->setModel(Table::$SELECT);
			$jenis_lrrsoj_adapter = new SimpleAdapter();
			$jenis_lrrsoj_adapter->add("Jenis Obat", "nama_jenis_obat");
			$columns = array("id", "nama_obat", "nama_jenis_obat", "label", "produsen", "nama_vendor", "satuan", "tanggal_exp");
			$jenis_lrrsoj_dbtable = new DBTable($this->db, $this->tbl_stok_obat, $columns);
			$filter = "";
			if (isset($_POST['kriteria'])) {
				$filter = " AND (nama_jenis_obat LIKE '%" . $_POST['kriteria'] . "%') ";
			}
			$query_value = "
				SELECT nama_jenis_obat AS 'id', nama_jenis_obat
				FROM (
					SELECT DISTINCT " . $this->tbl_stok_obat . ".nama_jenis_obat
					FROM " . $this->tbl_stok_obat . " LEFT JOIN " . $this->tbl_obat_masuk . " ON " . $this->tbl_stok_obat . ".id_obat_masuk = " . $this->tbl_obat_masuk . ".id
					WHERE " . $this->tbl_obat_masuk . ".status = 'sudah' " . $filter . " AND " . $this->tbl_obat_masuk . ".prop NOT LIKE 'del' AND " . $this->tbl_stok_obat . ".prop NOT LIKE 'del'
				) v_jenis
				ORDER BY nama_jenis_obat ASC
			";
			$query_count = "
				SELECT COUNT(*)
				FROM (
					SELECT DISTINCT " . $this->tbl_stok_obat . ".nama_jenis_obat
					FROM " . $this->tbl_stok_obat . " LEFT JOIN " . $this->tbl_obat_masuk . " ON " . $this->tbl_stok_obat . ".id_obat_masuk = " . $this->tbl_obat_masuk . ".id
					WHERE " . $this->tbl_obat_masuk . ".status = 'sudah' " . $filter . " AND " . $this->tbl_obat_masuk . ".prop NOT LIKE 'del' AND " . $this->tbl_stok_obat . ".prop NOT LIKE 'del'
				) v_jenis
				ORDER BY nama_jenis_obat ASC
			";
			$jenis_lrrsoj_dbtable->setPreferredQuery(true, $query_value, $query_count);
			$jenis_lrrsoj_dbresponder = new JenisObatDBResponder(
				$jenis_lrrsoj_dbtable,
				$jenis_lrrsoj_table,
				$jenis_lrrsoj_adapter,
				$this->tbl_stok_obat,
				$this->tbl_obat_masuk
			);
			
			$super_command = new SuperCommand();
			$super_command->addResponder("jenis_lrrsoj", $jenis_lrrsoj_dbresponder);
			$init = $super_command->initialize();
			if ($init != null) {
				echo $init;
				return;
			}
		}
		
		public function jsLoader(){
			echo addJS("framework/smis/js/table_action.js");
		}
		
		public function phpPreLoad(){
			$form = new Form("lrrsoj_form", "", $this->name . " : Laporan Rekap Perbekalan Obat Per Jenis");
			$tanggal_text = new Text("lrrsoj_tanggal", "lrrsoj_tanggal", date("d-m-Y"));
			$tanggal_text->setAtribute("disabled='disabled'");
			$form->addElement("Tanggal", $tanggal_text);
			$jenis_button = new Button("", "", "Pilih");
			$jenis_button->setClass("btn-info");
			$jenis_button->setAction("jenis_lrrsoj.chooser('jenis_lrrsoj', 'jenis_lrrsoj_button', 'jenis_lrrsoj', jenis_lrrsoj)");
			$jenis_button->setIcon("icon-white icon-list-alt");
			$jenis_button->setIsButton(Button::$ICONIC);
			$jenis_button->setAtribute("id='jenis_lrrsoj_browse'");
			$jenis_text = new Text("lrrsoj_nama_jenis", "lrrsoj_nama_jenis", "");
			$jenis_text->setClass("smis-one-option-input");
			$jenis_text->setAtribute("disabled='disabled'");
			$jenis_input_group = new InputGroup("");
			$jenis_input_group->addComponent($jenis_text);
			$jenis_input_group->addComponent($jenis_button);
			$form->addElement("Jenis", $jenis_input_group);
			$show_button = new Button("", "", "Tampilkan");
			$show_button->setClass("btn-primary");
			$show_button->setIcon("icon-white icon-repeat");
			$show_button->setIsButton(Button::$ICONIC);
			$show_button->setAction("lrrsoj.view()");
			$print_button = new Button("", "", "Cetak");
			$print_button->setClass("btn-inverse");
			$print_button->setIcon("icon-white icon-print");
			$print_button->setIsButton(Button::$ICONIC);
			$print_button->setAction("lrrsoj.print()");
			$btn_group = new ButtonGroup("noprint");
			$btn_group->addButton($show_button);
			$btn_group->addButton($print_button);
			$form->addElement("", $btn_group);
			
			echo $form->getHtml();
			echo $this->uitable->getHtml();
		}
		
		public function jsPreLoad(){
			?>
			<script type="text/javascript">
				function JenisLRRSOJAction(name, page, action, column) {
					this.initialize(name, page, action, column);
				}
				JenisLRRSOJAction.prototype.constructor = JenisLRRSOJAction;
				JenisLRRSOJAction.prototype = new TableAction();
				JenisLRRSOJAction.prototype.selected = function(json) {
					$("#lrrsoj_nama_jenis").val(json.nama_jenis_obat);
				};
				
				function LRRSOJAction(name, page, action, column)	{
					this.initialize(name, page, action, column);
				}
				LRRSOJAction.prototype.constructor = LRRSOJAction;
				LRRSOJAction.prototype = new TableAction();
				LRRSOJAction.prototype.getViewData = function() {
					var data = TableAction.prototype.getViewData.call(this);
					data['nama_jenis_obat'] = $("#lrrsoj_nama_jenis").val();
					return data;
				};
				LRRSOJAction.prototype.print = function() {
					if ($("#lrrsoj_nama_jenis").val() == "")
						return;
					var data = this.getRegulerData();
					data['command'] = "print_laporan";
					data['nama_jenis_obat'] = $("#lrrsoj_nama_jenis").val();
					$.post(
						"",
						data,
						function(response) {
							var json = getContent(response);
							if (json == null) return;
							smis_print(json);
						}
					);
				};
				
				var lrrsoj;
				var jenis_lrrsoj;
				var LRSSO_PAGE = "<?php echo $this->page; ?>";
				var LRSSO_PNAME = "<?php echo $this->proto_name; ?>";
				var LRSSO_PSLUG = "<?php echo $this->proto_slug; ?>";
				var LRSSO_PIMPL = "<?php echo $this->proto_implement; ?>";
				$(document).ready(function() {
					$('[data-toggle="popover"]').popover({
						trigger: 'hover',
						'placement': 'top'
					});
					jenis_lrrsoj = new JenisLRRSOJAction(
						"jenis_lrrsoj",
						LRSSO_PAGE,
						"laporan_rekap_perbekalan_obat_per_jenis",
						new Array()
					);
					jenis_lrrsoj.setPrototipe(LRSSO_PNAME, LRSSO_PSLUG, LRSSO_PIMPL);
					jenis_lrrsoj.setSuperCommand("jenis_lrrsoj");
					lrrsoj = new LRRSOJAction(
						"lrrsoj",
						LRSSO_PAGE,
						"laporan_rekap_perbekalan_obat_per_jenis",
						new Array()
					);
					lrrsoj.setPrototipe(LRSSO_PNAME, LRSSO_PSLUG, LRSSO_PIMPL);
				});
			</script>
			<?php 
		}
	}
?>