<?php 
	require_once 'smis-framework/smis/template/ModulTemplate.php';
	require_once 'smis-libs-inventory/responder/LaporanPerbekalanObatDBResponder.php';
	
	class LaporanPerbekalanObat extends ModulTemplate {
		private $db;
		private $name;
		private $tbl_stok_obat;
		private $tbl_obat_masuk;
		private $page;
		private $action;
		private $uitable;
		private $proto_name;
		private $proto_implement;
		private $proto_slug;
		
		public function __construct($db, $name_entity, $dbslug, $page, $entity="") {
			$this->db = $db;
			$this->name = $name_entity;
			if ($entity != "") $entity = "_" . $entity;
			$this->tbl_stok_obat = "smis_" . $dbslug . "_stok_obat" . $entity;
			$this->tbl_obat_masuk = "smis_" . $dbslug . "_obat_masuk" . $entity;
			$this->page = $page;
			$header = array("No. Stok", "Nama Obat", "Jenis Obat", "Jumlah", "Satuan", "Produsen", "Tgl. Exp.", "H. Netto", "No. Mutasi", "Tgl. Masuk", "Unit Asal");
			$this->uitable = new Table($header, "", null, true);
			$this->uitable->setAction(false);
			$this->uitable->setName("lrso");
			$this->proto_name = "";
			$this->proto_implement = "";
			$this->proto_slug = "";
		}
		
		public function setPrototype($pname, $pslug, $pimplement){
			$this->proto_name = $pname;
			$this->proto_implement = $pimplement;
			$this->proto_slug = $pslug;
		}
		
		public function command($command){		
			$adapter = new SimpleAdapter();
			$adapter->add("No. Stok", "id", "digit8");
			$adapter->add("Nama Obat", "nama_obat");
			$adapter->add("Jenis Obat", "nama_jenis_obat");
			$adapter->add("Jumlah", "sisa");
			$adapter->add("Satuan", "satuan");
			$adapter->add("Produsen", "produsen");
			$adapter->add("Tgl. Exp.", "tanggal_exp", "date d M Y");
			$adapter->add("H. Netto", "hna", "money Rp.");
			$adapter->add("No. Mutasi", "id_obat_masuk", "digit8");
			$adapter->add("Tgl. Masuk", "tanggal_masuk", "date d M Y");
			$adapter->add("Unit Asal", "unit_pengirim", "unslug");
			$dbtable = new DBTable($this->db, $this->tbl_stok_obat);
			$filter = "1";
			if (isset($_POST['kriteria'])) {
				$filter .= " AND (nama_obat LIKE '%" . $_POST['kriteria'] . "%' OR nama_jenis_obat LIKE '%" . $_POST['kriteria'] . "%' OR unit_pengirim LIKE '%" . $_POST['kriteria'] . "%' OR produsen LIKE '%" . $_POST['kriteria'] . "%')";
			}
			$query_value = "
				SELECT *
				FROM (
					SELECT " . $this->tbl_stok_obat . ".*, " . $this->tbl_obat_masuk . ".tanggal AS 'tanggal_masuk',  " . $this->tbl_obat_masuk . ".unit AS 'unit_pengirim', " . $this->tbl_obat_masuk . ".keterangan
					FROM " . $this->tbl_stok_obat . " LEFT JOIN " . $this->tbl_obat_masuk . " ON " . $this->tbl_stok_obat . ".id_obat_masuk = " . $this->tbl_obat_masuk . ".id
					WHERE " . $this->tbl_stok_obat . ".prop NOT LIKE 'del' AND " . $this->tbl_obat_masuk . ".prop NOT LIKE 'del' AND " . $this->tbl_obat_masuk . ".status = 'sudah' AND " . $this->tbl_stok_obat . ".sisa > 0
				) v_stok
				WHERE " . $filter . "
				ORDER BY nama_obat, nama_jenis_obat ASC
			";
			$query_count = "
				SELECT COUNT(*)
				FROM (
					SELECT " . $this->tbl_stok_obat . ".*, " . $this->tbl_obat_masuk . ".tanggal AS 'tanggal_masuk',  " . $this->tbl_obat_masuk . ".unit AS 'unit_pengirim', " . $this->tbl_obat_masuk . ".keterangan
					FROM " . $this->tbl_stok_obat . " LEFT JOIN " . $this->tbl_obat_masuk . " ON " . $this->tbl_stok_obat . ".id_obat_masuk = " . $this->tbl_obat_masuk . ".id
					WHERE " . $this->tbl_stok_obat . ".prop NOT LIKE 'del' AND " . $this->tbl_obat_masuk . ".prop NOT LIKE 'del' AND " . $this->tbl_obat_masuk . ".status = 'sudah' AND " . $this->tbl_stok_obat . ".sisa > 0
				) v_stok
				WHERE " . $filter . "
				ORDER BY nama_obat, nama_jenis_obat ASC
			";
			$dbtable->setPreferredQuery(true, $query_value, $query_count);
			$dbresponder = new LaporanPerbekalanObatDBResponder(
				$dbtable,
				$this->uitable,
				$adapter,
				$this->tbl_obat_masuk,
				$this->tbl_stok_obat,
				$this->page
			);
			$data = $dbresponder->command($_POST['command']);
			echo json_encode($data);
			return;
		}
		
		public function jsLoader(){
			echo addJS("framework/smis/js/table_action.js");
		}
		
		public function phpPreLoad(){
			$form = new Form("lrso_form", "", $this->name . " : Laporan Perbekalan Obat");
			$tanggal_text = new Text("lrso_tanggal", "lrso_tanggal", date("d-m-Y"));
			$tanggal_text->setAtribute("disabled='disabled'");
			$form->addElement("Tanggal", $tanggal_text);
			$show_button = new Button("", "", "Tampilkan");
			$show_button->setClass("btn-primary");
			$show_button->setIcon("icon-white icon-repeat");
			$show_button->setIsButton(Button::$ICONIC);
			$show_button->setAction("lrso.view()");
			$print_button = new Button("", "", "Cetak");
			$print_button->setClass("btn-inverse");
			$print_button->setIcon("icon-white icon-print");
			$print_button->setIsButton(Button::$ICONIC);
			$print_button->setAction("lrso.print()");
			$btn_group = new ButtonGroup("noprint");
			$btn_group->addButton($show_button);
			$btn_group->addButton($print_button);
			$form->addElement("", $btn_group);
			
			echo $form->getHtml();
			echo $this->uitable->getHtml();
		}
		
		public function jsPreLoad(){
			?>
			<script type="text/javascript">
				function LRSOAction(name, page, action, column)	{
					this.initialize(name, page, action, column);
				}
				LRSOAction.prototype.constructor = LRSOAction;
				LRSOAction.prototype = new TableAction();
				LRSOAction.prototype.print = function() {
					var data = this.getRegulerData();
					data['command'] = "print_laporan";
					$.post(
						"",
						data,
						function(response) {
							var json = getContent(response);
							if (json == null) return;
							smis_print(json);
						}
					);
				};
				
				var lrso;
				var LRSO_PAGE = "<?php echo $this->page; ?>";
				var LRSO_PNAME = "<?php echo $this->proto_name; ?>";
				var LRSO_PSLUG = "<?php echo $this->proto_slug; ?>";
				var LRSO_PIMPL = "<?php echo $this->proto_implement; ?>";
				$(document).ready(function() {
					$('[data-toggle="popover"]').popover({
						trigger: 'hover',
						'placement': 'top'
					});
					lrso = new LRSOAction(
						"lrso",
						LRSO_PAGE,
						"laporan_perbekalan_obat",
						new Array()
					);
					lrso.setPrototipe(LRSO_PNAME, LRSO_PSLUG, LRSO_PIMPL);
				});
			</script>
			<?php 
		}
	}
?>