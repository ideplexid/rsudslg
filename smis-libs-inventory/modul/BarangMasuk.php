<?php 
	require_once 'smis-base/smis-include-service-consumer.php';
	require_once 'smis-framework/smis/template/ModulTemplate.php';
	require_once 'smis-libs-inventory/adapter/BarangMasukAdapter.php';
	require_once 'smis-libs-inventory/responder/BarangMasukDBResponder.php';
	require_once 'smis-libs-inventory/table/BarangMasukTable.php';
	require_once 'smis-libs-inventory/service_consumer/SetBarangStatusKeluarServiceConsumer.php';

	class BarangMasuk extends ModulTemplate {
		private $db;
		private $name;
		private $tbl_barang_masuk;
		private $tbl_stok_barang;
		private $tbl_riwayat_stok_barang;
		private $page;
		private $action;
		private $barang_masuk_table;
		private $proto_name;
		private $proto_implement;
		private $proto_slug;
		
		public function __construct($db, $name_entity, $slug_table_parent, $slug_table_child, $slug_table_log, $page){
			$this->db=$db;
			$this->name=$name_entity;
			$this->tbl_barang_masuk=$slug_table_parent;
			$this->tbl_stok_barang=$slug_table_child;
			$this->tbl_riwayat_stok_barang=$slug_table_log;
			$this->page=$page;
			$this->barang_masuk_table = new BarangMasukTable(array("No. Mutasi", "Tanggal Masuk", "Gudang", "Status"), $this->name . " : Barang Masuk");
			$this->barang_masuk_table->setName("barang_masuk");
			$this->barang_masuk_table->setAddButtonEnable(false);
		}
		
		public function setPrototype($pname,$pslug,$pimplement){
			$this->proto_name=$pname;
			$this->proto_implement=$pimplement;
			$this->proto_slug=$pslug;
		}
		
		public function command($command){
			$barang_masuk_adapter = new BarangMasukAdapter();
			$columns = array("id", "tanggal", "unit", "status", "keterangan");
			$barang_masuk_dbtable = new DBTable($this->db,$this->tbl_barang_masuk,$columns);
			$barang_masuk_dbtable->addCustomKriteria(" id ", " > 0 ");
			$barang_masuk_dbtable->addCustomKriteria(" status ", " != 'dikembalikan' ");
			$barang_masuk_dbtable->setOrder(" id DESC ");
			$barang_masuk_dbresponder = new BarangMasukDBResponder($barang_masuk_dbtable,$this->barang_masuk_table,$barang_masuk_adapter,$this->tbl_stok_barang, $this->tbl_riwayat_stok_barang);
			$data = $barang_masuk_dbresponder->command($_POST['command']);
			if (isset($_POST['push_command'])) {
				$id['id'] = $data['content']['id'];
				$barang_masuk_row = $barang_masuk_dbtable->select($id);
				$push_command = "push_" . $_POST['push_command'];
				global $user;
				$nama_user = $user->getName();
				$set_barang_keluar_status_service_consumer = new SetBarangKeluarStatusServiceConsumer($this->db, $nama_user, $barang_masuk_row->f_id, $barang_masuk_row->status, $barang_masuk_row->keterangan, $push_command);
				$set_barang_keluar_status_service_consumer->execute();
			}
			echo json_encode($data);
			return;
		}
		
		public function phpPreLoad(){
			$this->barang_masuk_table->addModal("id", "hidden", "", "");
			$this->barang_masuk_table->addModal("tanggal", "date", "Tanggal", date('Y-m-d'), "y", "date", true);
			$this->barang_masuk_table->addModal("unit", "text", "Unit", "","y","free",true);
			$barang_masuk_modal=$this->barang_masuk_table->getModal();
			$barang_masuk_modal->setModalSize(Modal::$FULL_MODEL);
			$barang_masuk_modal->setTitle("Data Barang Masuk");
			$this->dbarang_masuk_table = new Table(array("Barang", "Jumlah", "Keterangan"));
			$this->dbarang_masuk_table->setName("dbarang_masuk");
			$this->dbarang_masuk_table->setFooterVisible(false);
			$this->dbarang_masuk_table->setAddButtonEnable(false);
			$this->dbarang_masuk_table->setReloadButtonEnable(false);
			$this->dbarang_masuk_table->setPrintButtonEnable(false);
			$barang_masuk_modal->addBody("dbarang_masuk_table", $this->dbarang_masuk_table);
			$barang_masuk_modal->clearFooter();
			
			$barang_masuk_button = new Button("", "", "Terima");
			$barang_masuk_button->setClass("btn-success");
			$barang_masuk_button->setAction("barang_masuk.accept()");
			$barang_masuk_button->setAtribute("id='barang_masuk_accept'");
			$barang_masuk_button->setIcon("fa fa-check");
			$barang_masuk_button->setIsButton(Button::$ICONIC);
			$barang_masuk_modal->addFooter($barang_masuk_button);
			
			$barang_masuk_button = new Button("", "", "Kembalikan");
			$barang_masuk_button->setClass("btn-danger");
			$barang_masuk_button->setAction("barang_masuk.return_barang()");
			$barang_masuk_button->setAtribute("id='barang_masuk_return'");
			$barang_masuk_button->setIcon("fa fa-times");
			$barang_masuk_button->setIsButton(Button::$ICONIC);
			$barang_masuk_modal->addFooter($barang_masuk_button);
			
			$barang_masuk_button = new Button("", "", "OK");
			$barang_masuk_button->setClass("btn-success");
			$barang_masuk_button->setAction("$($(this).data('target')).smodal('hide')");
			$barang_masuk_button->setAtribute("id='barang_masuk_ok'");
			$barang_masuk_modal->addFooter($barang_masuk_button);
			
			echo $barang_masuk_modal->getHtml();
			echo $this->barang_masuk_table->getHtml();
			echo addJS("framework/smis/js/table_action.js");
		}
		
		public function jsPreLoad(){
			?>
			<script type="text/javascript">
				function BarangMasukAction(name, page, action, column) {
					this.initialize(name, page, action, column);
				}
				BarangMasukAction.prototype.constructor = BarangMasukAction;
				BarangMasukAction.prototype = new TableAction();
				BarangMasukAction.prototype.edit = function(id) {
					var self = this;
					var data = this.getRegulerData();
					data['command'] = "edit";
					data['id'] = id;
					$.post(
						"",
						data,
						function(response) {
							var json = getContent(response);
							if (json == null) return;
							$("#barang_masuk_id").val(json.header.id);
							$("#barang_masuk_tanggal").val(json.header.tanggal);
							var unit = json.header.unit.toUpperCase().replace("_", " ");
							$("#barang_masuk_unit").val(unit);
							$("#dbarang_masuk_list").children("tr").remove();
							BARANG_MASUK_ROW_ID = 0;
							for(var i = 0; i < json.detail.length; i++) {
								var dbarang_masuk_id = json.detail[i].id;
								var dbarang_masuk_id_barang_masuk = json.detail[i].id_barang_keluar;
								var dbarang_masuk_id_barang = json.detail[i].id_barang;
								var dbarang_masuk_nama_barang = json.detail[i].nama_barang;
								var dbarang_masuk_nama_jenis_barang = json.detail[i].nama_jenis_barang;
								var dbarang_masuk_jumlah = json.detail[i].jumlah;
								var dbarang_masuk_satuan = json.detail[i].satuan;
								var dbarang_masuk_konversi = json.detail[i].konversi;
								var dbarang_masuk_satuan_konversi = json.detail[i].satuan_konversi;
								$("tbody#dbarang_masuk_list").append(
									"<tr id='data_" + BARANG_MASUK_ROW_ID + "'>" +
										"<td style='display: none;' id='data_" + BARANG_MASUK_ROW_ID + "_id'>" + dbarang_masuk_id + "</td>" +
										"<td style='display: none;' id='data_" + BARANG_MASUK_ROW_ID + "_id_barang'>" + dbarang_masuk_id_barang + "</td>" +
										"<td style='display: none;' id='data_" + BARANG_MASUK_ROW_ID + "_jumlah'>" + dbarang_masuk_jumlah + "</td>" +
										"<td style='display: none;' id='data_" + BARANG_MASUK_ROW_ID + "_satuan'>" + dbarang_masuk_satuan + "</td>" +
										"<td style='display: none;' id='data_" + BARANG_MASUK_ROW_ID + "_konversi'>" + dbarang_masuk_konversi + "</td>" +
										"<td style='display: none;' id='data_" + BARANG_MASUK_ROW_ID + "_satuan_konversi'>" + dbarang_masuk_satuan_konversi + "</td>" +
										"<td id='data_" + BARANG_MASUK_ROW_ID + "_nama_barang'>" + dbarang_masuk_nama_barang + "</td>" +
										"<td id='data_" + BARANG_MASUK_ROW_ID + "_nama_jenis_barang'>" + dbarang_masuk_nama_jenis_barang + "</td>" +
										"<td id='data_" + BARANG_MASUK_ROW_ID + "_f_jumlah'>" + dbarang_masuk_jumlah + " " + dbarang_masuk_satuan + "</td>" +
										"<td id='data_" + BARANG_MASUK_ROW_ID + "_keterangan'>1 " + dbarang_masuk_satuan + " = " + dbarang_masuk_konversi + " " + dbarang_masuk_satuan_konversi + "</td>" +
										"<td></td>" +
									"</tr>"
								);
								BARANG_MASUK_ROW_ID++;
							}
							$("#barang_masuk_accept").show();
							$("#barang_masuk_return").show();
							$("#barang_masuk_ok").hide();
							$("#barang_masuk_add_form").smodal("show");
						}
					);
				};
				BarangMasukAction.prototype.accept = function() {
					$("#barang_masuk_add_form").smodal("hide");
					showLoading();
					var self = this;
					var data = this.getRegulerData();
					data['command'] = "save";
					data['push_command'] = "update";
					data['id'] = $("#barang_masuk_id").val();
					data['status'] = "sudah";
					data['keterangan'] = "";
					$.post(
						"",
						data,
						function(response) {
							var json = getContent(response);
							if (json == null) {
								$("#barang_masuk_add_form").smodal("show");
							} else {
								self.view();
							}
							dismissLoading();
						}
					);
				};
				BarangMasukAction.prototype.return_barang = function() {
					$("#barang_masuk_add_form").smodal("hide");
					showLoading();
					var self = this;
					var data = this.getRegulerData();
					data['command'] = "save";
					data['push_command'] = "update";
					data['id'] = $("#barang_masuk_id").val();
					data['status'] = "dikembalikan";
					data['keterangan'] = "";
					$.post(
						"",
						data,
						function(response) {
							var json = getContent(response);
							if (json == null) {
								$("#barang_masuk_add_form").smodal("show");
							} else {
								self.view();
							}
							dismissLoading();
						}
					);
				};
				BarangMasukAction.prototype.detail = function(id) {
					var self = this;
					var data = this.getRegulerData();
					data['command'] = "edit";
					data['id'] = id;
					$.post(
						"",
						data,
						function(response) {
							var json = getContent(response);
							if (json == null) return;
							$("#barang_masuk_id").val(json.header.id);
							$("#barang_masuk_tanggal").val(json.header.tanggal);
							var unit = json.header.unit.toUpperCase().replace("_", " ");
							$("#barang_masuk_unit").val(unit);
							$("#dbarang_masuk_list").children("tr").remove();
							BARANG_MASUK_ROW_ID = 0;
							for(var i = 0; i < json.detail.length; i++) {
								var dbarang_masuk_id = json.detail[i].id;
								var dbarang_masuk_id_barang_masuk = json.detail[i].id_barang_keluar;
								var dbarang_masuk_id_barang = json.detail[i].id_barang;
								var dbarang_masuk_nama_barang = json.detail[i].nama_barang;
								var dbarang_masuk_nama_jenis_barang = json.detail[i].nama_jenis_barang;
								var dbarang_masuk_jumlah = json.detail[i].jumlah;
								var dbarang_masuk_satuan = json.detail[i].satuan;
								var dbarang_masuk_konversi = json.detail[i].konversi;
								var dbarang_masuk_satuan_konversi = json.detail[i].satuan_konversi;
								$("tbody#dbarang_masuk_list").append(
									"<tr id='data_" + BARANG_MASUK_ROW_ID + "'>" +
										"<td style='display: none;' id='data_" + BARANG_MASUK_ROW_ID + "_id'>" + dbarang_masuk_id + "</td>" +
										"<td style='display: none;' id='data_" + BARANG_MASUK_ROW_ID + "_id_barang'>" + dbarang_masuk_id_barang + "</td>" +
										"<td style='display: none;' id='data_" + BARANG_MASUK_ROW_ID + "_nama_jenis_barang'>" + dbarang_masuk_nama_jenis_barang + "</td>" +
										"<td style='display: none;' id='data_" + BARANG_MASUK_ROW_ID + "_jumlah'>" + dbarang_masuk_jumlah + "</td>" +
										"<td style='display: none;' id='data_" + BARANG_MASUK_ROW_ID + "_satuan'>" + dbarang_masuk_satuan + "</td>" +
										"<td style='display: none;' id='data_" + BARANG_MASUK_ROW_ID + "_konversi'>" + dbarang_masuk_konversi + "</td>" +
										"<td style='display: none;' id='data_" + BARANG_MASUK_ROW_ID + "_satuan_konversi'>" + dbarang_masuk_satuan_konversi + "</td>" +
										"<td id='data_" + BARANG_MASUK_ROW_ID + "_nama_barang'>" + dbarang_masuk_nama_barang + "</td>" +
										"<td id='data_" + BARANG_MASUK_ROW_ID + "_f_jumlah'>" + dbarang_masuk_jumlah + " " + dbarang_masuk_satuan + "</td>" +
										"<td id='data_" + BARANG_MASUK_ROW_ID + "_keterangan'>1 " + dbarang_masuk_satuan + " = " + dbarang_masuk_konversi + " " + dbarang_masuk_satuan_konversi + "</td>" +
										"<td></td>" +
									"</tr>"
								);
								BARANG_MASUK_ROW_ID++;
							}
							$("#barang_masuk_accept").hide();
							$("#barang_masuk_return").hide();
							$("#barang_masuk_ok").show();
							$("#barang_masuk_add_form").smodal("show");
						}
					);
				};
				
				var barang_masuk;
				var BARANG_MASUK_ROW_ID;
				var BARANG_MASUK_PNAME="<?php echo $this->proto_name; ?>";
				var BARANG_MASUK_PSLUG="<?php echo $this->proto_slug; ?>";
				var BARANG_MASUK_PIMPL="<?php echo $this->proto_implement; ?>";
				var BARANG_MASUK_ENTITY="<?php echo $this->page; ?>";
				$(document).ready(function() {
					$('[data-toggle="popover"]').popover({
						trigger: 'hover',
						'placement': 'top'
					});
					var barang_masuk_columns = new Array("id", "tanggal", "unit", "status", "keterangan");
					barang_masuk = new BarangMasukAction("barang_masuk",BARANG_MASUK_ENTITY,"barang_masuk",barang_masuk_columns);
					barang_masuk.setPrototipe(BARANG_MASUK_PNAME,BARANG_MASUK_PSLUG,BARANG_MASUK_PIMPL);
					barang_masuk.view();
				});
			</script>
			<?php 	
		}	
	}
?>