<?php 
	require_once 'smis-framework/smis/template/ModulTemplate.php';
	require_once 'smis-base/smis-include-service-consumer.php';
	require_once 'smis-libs-inventory/table/RKBUTable.php';
	require_once 'smis-libs-inventory/adapter/RKBUAdapter.php';
	require_once 'smis-libs-inventory/responder/RKBUDBResponder.php';
	require_once 'smis-libs-inventory/service_consumer/PushRKBUServiceConsumer.php';
	require_once 'smis-libs-inventory/table/DRKBUtable.php';

	class RKBU extends ModulTemplate {
		private $db;
		private $name;
		private $tbl_rkbu;
		private $tbl_drkbu;
		private $page;
		private $action;
		private $rkbu_table;
		private $proto_name;
		private $proto_implement;
		private $proto_slug;
		
		public function __construct($db, $name_entity, $dbslug, $page,$entity=""){
			$this->db=$db;
			$this->name=$name_entity;
			if($entity!="") $entity="_".$entity;
			$this->tbl_rkbu= "smis_".$dbslug."_rkbu".$entity;
			$this->tbl_drkbu="smis_".$dbslug."_drkbu".$entity;
			$this->page=$page;				
			$header=array("Nomor", "Tahun RKBU", "Tanggal Diajukan", "Tanggal Ditinjau", "Status");
			$this->rkbu_table = new RKBUTable($header,$this->name." : Rencana Kebutuhan Barang Unit",null,true);
			$this->rkbu_table->setName("rkbu");
			$this->proto_name="";
			$this->proto_implement="";
			$this->proto_slug="";
		}
		
		public function setPrototype($pname,$pslug,$pimplement){
			$this->proto_name=$pname;
			$this->proto_implement=$pimplement;
			$this->proto_slug=$pslug;
		}
		
		public function command($command){		
			$rkbu_adapter = new RKBUAdapter();
			$columns = array("id", "tahun", "tanggal_diajukan", "tanggal_ditinjau", "keterangan", "tinjauan", "disetujui", "locked");
			$rkbu_dbtable = new DBTable($this->db,$this->tbl_rkbu,$columns);
			$rkbu_dbresponder = new RKBUDBResponder($rkbu_dbtable,$this->rkbu_table,$rkbu_adapter,$this->tbl_drkbu);
			$data = $rkbu_dbresponder->command($_POST['command']);
			if (isset($_POST['push_command'])) {
				$id_rkbu=$data['content']['id'];
				$rkbu_row = $rkbu_dbtable->select($id_rkbu);
				$drkbu_dbtable = new DBTable($this->db, $this->tbl_drkbu);
				$drkbu_dbtable->addCustomKriteria(" id_rkbu ", " = '".$id_rkbu."'");
				$q=$drkbu_dbtable->getQueryView("", "0");
				$drkbu_rows = $drkbu_dbtable->get_result($q['query']);
				$command = "";
				if ($_POST['push_command'] == "save") {
					$command = "push_save";
				} else if ($_POST['push_command'] == "del") {
					$command = "push_delete";
				}
				$push_rkbu_service_consumer = new PushRKBUServiceConsumer($this->db, $rkbu_row->id, $rkbu_row->tahun, $rkbu_row->tanggal_diajukan, $rkbu_row->keterangan, $rkbu_row->disetujui, $rkbu_row->locked, $drkbu_rows, $command,$this->page);
				$push_rkbu_service_consumer->execute();
			}
			echo json_encode($data);
			return;
		}
		
		public function superCommand($super_command){
			$barang_table = new Table(array("Nomor", "Nama", "Jenis"),"",null,true);
			$barang_table->setName("barang");
			$barang_table->setModel(Table::$SELECT);
			$barang_adapter = new SimpleAdapter();
			$barang_adapter->add("Nomor", "id", "digit8");
			$barang_adapter->add("Nama", "nama");
			$barang_adapter->add("Jenis", "nama_jenis_barang");
			$barang_service_responder = new ServiceResponder($this->db,$barang_table,	$barang_adapter,"get_daftar_barang_reguler");
			$barang_super_command = new SuperCommand();
			$barang_super_command->addResponder("barang", $barang_service_responder);
			$init = $barang_super_command->initialize();
			if ($init != null) {
				echo $init;
				return;
			}
		}
		
		public function jsLoader(){
			echo addJS("framework/smis/js/table_action.js");
			echo addJS("framework/bootstrap/js/bootstrap-datepicker.js");
		}
		
		public function cssLoader(){
			echo addCSS("framework/bootstrap/css/datepicker.css");
		}
		
		public function phpPreLoad(){
			$this->rkbu_table->addModal("id", "hidden", "", "");
			$this->rkbu_table->addModal("tahun", "text", "Tahun", "");
			$this->rkbu_table->addModal("tanggal_diajukan", "date", "Tgl Diajukan", "");
			$this->rkbu_table->addModal("keterangan", "summernote", "Keterangan", "");
			$this->rkbu_table->addModal("tinjauan", "summernote", "Tinjauan", "");
			$rkbu_modal=$this->rkbu_table->getModal();
			$rkbu_modal->setTitle("RKBU ".$this->name);
			$rkbu_modal->setClass(Modal::$FULL_MODEL);
			
			$drkbu_table = new DRKBUTable(array("Bulan", "Barang", "Jumlah", "Disetujui"),"",null,true);
			$drkbu_table->setName("drkbu");
			$drkbu_table->setFooterVisible(false);
			$drkbu_table->addModal("id", "hidden", "", "");
			$drkbu_table->addModal("bulan", "select", "Bulan", $drkbu_table->getBulan());
			$drkbu_table->addModal("nama_barang", "chooser-drkbu-barang", "Barang", "");
			$drkbu_table->addModal("jenis_barang", "select", "Jenis Barang", $drkbu_table->getJenisBarang());
			$drkbu_table->addModal("jumlah", "text", "Jumlah", "n","numeric");
			$drkbu_table->addModal("satuan", "text", "Satuan", "");
			$drkbu_modal=$drkbu_table->getModal();
			$rkbu_modal->addBody("drkbu_table", $drkbu_table);
			$rkbu_button = new Button("", "", "OK");
			$rkbu_button->setClass("btn-success");
			$rkbu_button->setAtribute("id='rkbu_ok' data-dismiss='modal'");
			$rkbu_modal->addFooter($rkbu_button);
			
			echo $drkbu_modal->getHtml();
			echo $rkbu_modal->getHtml();
			echo $this->rkbu_table->getHtml();
		}
		
		public function jsPreLoad(){
			?>
			<script type="text/javascript">
				function RKBUAction(name, page, action, column) {
					this.initialize(name, page, action, column);
				}
				RKBUAction.prototype.constructor = RKBUAction;
				RKBUAction.prototype = new TableAction();
				RKBUAction.prototype.show_add_form = function() {
					row_id = 0;
					var today = new Date();
					$("#rkbu_tahun").val(today.getFullYear());
					$("#rkbu_tahun").removeAttr("disabled");
					$("#rkbu_tanggal_diajukan").val(today.getFullYear() + "-" + (Number(today.getMonth())+1) + "-" + today.getDate());
					$("#rkbu_tanggal_diajukan").removeAttr("disabled");
					$("#rkbu_keterangan").val("");
					$("#rkbu_edit_keterangan").removeAttr("disabled");
					$("#rkbu_edit_keterangan").removeAttr("onclick");
					$("#rkbu_edit_keterangan").attr("onclick", "smis_summernote_write('rkbu', 'rkbu_keterangan', 'rkbu_add_form')");
					$("#rkbu_add").show();
					$("tbody#drkbu_list").children().remove();
					$("#rkbu_save").show();
					$("#rkbu_save").removeAttr("onclick");
					$("#rkbu_save").attr("onclick", "rkbu.save()");
					$("#rkbu_ok").hide();
					$("#modal_alert_rkbu_add_form").html("");
					$(".error_field").removeClass("error_field");
					$("#rkbu_add_form").smodal("show");
				};
				RKBUAction.prototype.validate = function() {
					var valid = true;
					var invalid_msg = "";
					var rkbu_tahun = $("#rkbu_tahun").val();
					var rkbu_tanggal_diajukan = $("#rkbu_tanggal_diajukan").val();
					var rkbu_nord = $("tbody#drkbu_list").children().length;
					$(".error_field").removeClass("error_field");
					if (rkbu_tahun == "") {
						valid = false;
						invalid_msg += "</br><strong>Tahun RKBU</strong> tidak boleh kosong";
						$("#rkbu_tahun").addClass("error_field");
					} else if (!is_numeric(rkbu_tahun)) {
						valid = false;
						invalid_msg += "</br><strong>Tahun RKBU</strong> seharusnya numerik (0-9)";
						$("#rkbu_tahun").addClass("error_field");
					}
					if (rkbu_tanggal_diajukan == "") {
						valid = false;
						invalid_msg += "</br><strong>Tanggal Pengajuan</strong> tidak boleh kosong";
						$("#rkbu_tanggal_diajukan").addClass("error_field");
					}
					if (rkbu_nord == 0) {
						valid = false;
						invalid_msg += "</br>Tidak ada <strong>detil RKBU</strong>";
					}
					if (!valid) {
						$("#modal_alert_rkbu_add_form").html(
							"<div class='alert alert-block alert-danger'>" +
								"<h4>Peringatan</h4>" +
								invalid_msg +
							"</div>"
						);
					}
					return valid;
				};
				RKBUAction.prototype.save = function() {
					if (!this.validate()) {
						return;
					}
					$("#rkbu_add_form").smodal("hide");
					showLoading();
					var self = this;
					var data = this.getRegulerData();
					//data['super_command'] = "rkbu";
					data['command'] = "save";
					data['id'] = "";
					data['tahun'] = $("#rkbu_tahun").val();
					data['tanggal_diajukan'] = $("#rkbu_tanggal_diajukan").val();
					data['keterangan'] = $("#rkbu_keterangan").val();
					data['tinjauan'] = $("#rkbu_tinjauan").val();
					var detail = {};
					var nor = $("tbody#drkbu_list").children("tr").length;
					for(var i = 0; i < nor; i++) {
						var d_data = {};
						var prefix = $("tbody#drkbu_list").children('tr').eq(i).prop("id");
						d_data['bulan'] = $("#" + prefix + "_bulan").text();
						d_data['nama_barang'] = $("#" + prefix + "_nama_barang").text();
						d_data['jenis_barang'] = $("#" + prefix + "_jenis_barang").text();
						d_data['jumlah_diajukan'] = $("#" + prefix + "_jumlah_diajukan").text();
						d_data['satuan_diajukan'] = $("#" + prefix + "_satuan_diajukan").text();
						d_data['jumlah_disetujui'] = $("#" + prefix + "_jumlah_disetujui").text();
						d_data['satuan_disetujui'] = $("#" + prefix + "_satuan_disetujui").text();
						detail[i] = d_data;
					}
					data['detail'] = detail;
					$.post(
						"",
						data,
						function(response) {
							var json = getContent(response);
							if (json == null) {
								$("#rkbu_add_form").smodal("show");
							} else {
								self.view();
							}
							dismissLoading();
						}
					);
				};
				RKBUAction.prototype.edit = function(id) {
					var self = this;
					var data = this.getRegulerData();
					//data['super_command'] = "rkbu";
					data['command'] = "edit";
					data['id'] = id;
					$.post(
						"",
						data,
						function(response) {
							var json = getContent(response);
							if (json == null) return;
							$("#rkbu_id").val(json.header.id);
							$("#rkbu_tahun").val(json.header.tahun);
							$("#rkbu_tahun").removeAttr("disabled");
							$("#rkbu_tanggal_diajukan").val(json.header.tanggal_diajukan);
							$("#rkbu_tanggal_diajukan").removeAttr("disabled");
							$("#rkbu_keterangan").val(json.header.keterangan);
							$("#rkbu_edit_keterangan").removeAttr("disabled");
							$("#rkbu_edit_keterangan").removeAttr("onclick");
							$("#rkbu_edit_keterangan").attr("onclick", "smis_summernote_write('rkbu', 'rkbu_keterangan', 'rkbu_add_form')");
							$("#rkbu_tinjauan").val(json.header.tinjauan);
							$("#rkbu_add").show();
							row_id = 0;
							$("tbody#drkbu_list").children().remove();
							for(var i = 0; i < json.detail.length; i++) {
								var drkbu_id = json.detail[i].id;
								var drkbu_id_rkbu = json.detail[i].id_rkbu;
								var drkbu_bulan = json.detail[i].bulan;
								var drkbu_nama_barang = json.detail[i].nama_barang;
								var drkbu_jenis_barang = json.detail[i].jenis_barang;
								var drkbu_jumlah_diajukan = json.detail[i].jumlah_diajukan;
								var drkbu_satuan_diajukan = json.detail[i].satuan_diajukan;
								var drkbu_jumlah_disetujui = json.detail[i].jumlah_disetujui;
								var drkbu_satuan_disetujui = json.detail[i].satuan_disetujui;
								var f_disetujui = "-";
								var edit_group = "";
								if (Number(drkbu_jumlah_disetujui) != 0) {
									f_disetujui = drkbu_jumlah_disetujui + " " + drkbu_satuan_disetujui;
									
								} else {
									edit_group = "<div class='btn-group noprint'>" +
												 "<a href='#' onclick='drkbu.edit(" + row_id + ")' data-content='Ubah' data-toggle='popover' class='input btn btn-warning'>" + 
													"<i class='icon-edit icon-white'></i>" +
												 "</a>" +
												 "<a href='#' onclick='drkbu.del(" + row_id + ")' data-content='Hapus' data-toggle='popover' class='input btn btn-danger'>" + 
													"<i class='icon-remove icon-white'></i>" + 
												 "</a>" +
												 "</div>";
								}
								$("tbody#drkbu_list").append(
									"<tr id='data_" + row_id + "'>" +
										"<td style='display: none;' id='data_" + row_id + "_id'>" + drkbu_id + "</td>" +
										"<td style='display: none;' id='data_" + row_id + "_jenis_barang'>" + drkbu_jenis_barang + "</td>" +
										"<td style='display: none;' id='data_" + row_id + "_jumlah_diajukan'>" + drkbu_jumlah_diajukan + "</td>" +
										"<td style='display: none;' id='data_" + row_id + "_satuan_diajukan'>" + drkbu_satuan_diajukan + "</td>" +
										"<td style='display: none;' id='data_" + row_id + "_jumlah_disetujui'>" + drkbu_jumlah_disetujui + "</td>" +
										"<td style='display: none;' id='data_" + row_id + "_satuan_disetujui'>" + drkbu_satuan_disetujui + "</td>" +
										"<td id='data_" + row_id + "_bulan'>" + drkbu_bulan + "</td>" +
										"<td id='data_" + row_id + "_nama_barang'>" + drkbu_nama_barang + "</td>" +
										"<td id='data_" + row_id + "_f_jumlah'>" + drkbu_jumlah_diajukan + " " + drkbu_satuan_diajukan + "</td>" +
										"<td id='data_" + row_id + "_f_disetujui'>" + f_disetujui + "</td>" +
										"<td>" + edit_group + "</td>" +
									"</tr>"
								);
								row_id++;
							}
							$("#rkbu_add_form").smodal("show");
							$("#modal_alert_rkbu_add_form").html("");
							$(".error_field").removeClass("error_field");
							$("#rkbu_save").show();
							$("#rkbu_save").removeAttr("onclick");
							$("#rkbu_save").attr("onclick", "rkbu.update(" + id + ")");
							$("#rkbu_ok").hide();
						}
					);
				};
				RKBUAction.prototype.update = function(id) {
					 if (!this.validate()) {
						return;
					 }
					 $("#rkbu_add_form").smodal("hide");
					 showLoading();
					 var self = this;
					 var data = this.getRegulerData();
					 data['command'] = "save";
					 data['id'] = id;
					 data['tahun'] = $("#rkbu_tahun").val();
					 data['tanggal_diajukan'] = $("#rkbu_tanggal_diajukan").val();
					 data['keterangan'] = $("#rkbu_keterangan").val();
					 data['tinjauan'] = $("#rkbu_tinjauan").val();
					 data['disetujui'] = "belum";
					 data['locked'] = false;
					 var detail = {};
					 var nor = $("tbody#drkbu_list").children("tr").length;
					 for(var i = 0; i < nor; i++) {
						var d_data = {};
						var prefix = $("tbody#drkbu_list").children('tr').eq(i).prop("id");
						d_data['id'] = $("#" + prefix + "_id").text();
						d_data['bulan'] = $("#" + prefix + "_bulan").text();
						d_data['nama_barang'] = $("#" + prefix + "_nama_barang").text();
						d_data['jenis_barang'] = $("#" + prefix + "_jenis_barang").text();
						d_data['jumlah_diajukan'] = $("#" + prefix + "_jumlah_diajukan").text();
						d_data['satuan_diajukan'] = $("#" + prefix + "_satuan_diajukan").text();
						d_data['jumlah_disetujui'] = $("#" + prefix + "_jumlah_disetujui").text();
						d_data['satuan_disetujui'] = $("#" + prefix + "_satuan_disetujui").text();
						if ($("#" + prefix).attr("class") == "deleted") {
							d_data['cmd'] = "delete";
						} else if ($("#" + prefix + "_id").text() == "") {
							d_data['cmd'] = "insert";
						} else {
							d_data['cmd'] = "update";
						}
						detail[i] = d_data;
					 }
					 data['detail'] = detail;
					 $.post(
						"",
						data,
						function(response) {
							var json = getContent(response);
							if (json == null) { 
								$("#rkbu_add_form").smodal("show");
							} else {
								self.view();
							}
							dismissLoading();
						}
					 );
				};
				RKBUAction.prototype.del = function(id) {
					var self = this;
					var del_data = this.getRegulerData();
					//del_data['super_command'] = "rkbu";
					del_data['command'] = "del";
					del_data['id'] = id;
					del_data['push_command'] = "del";
					bootbox.confirm(
						"Anda yakin ingin menghapus RKBU ini?", 
						function(result) {
						   if (result) {
							   showLoading();
								$.post(
									"",
									del_data,
									function(res){
										var json = getContent(res);
										if (json == null) return;
										self.view();
										dismissLoading();
									}
								);
							}
						}
					); 
				};
				RKBUAction.prototype.submit = function(id) {
					var self = this;
					var data = this.getRegulerData();
					data['command'] = "edit";
					data['id'] = id;
					bootbox.confirm(
						"Anda yakin mengajukan RKBU ini?",
						function(result) {
							if (result) {
								showLoading();
								$.post(
									"",
									data,
									function(response) {
										var json = getContent(response);
										if (json == null) return;
										data = self.getRegulerData();
										data['command'] = "save";
										data['push_command'] = "save";
										data['id'] = json.header.id;
										data['tahun'] = json.header.tahun;
										data['tanggal_diajukan'] = json.header.tanggal_diajukan;
										data['keterangan'] = json.header.keterangan;
										data['disetujui'] = json.header.disetujui;
										data['locked'] = "1";
										$.post(
											"",
											data,
											function() {
												self.view();
												dismissLoading();
											}
										);
									}
								);
							}
						}
					);
				};
				RKBUAction.prototype.detail = function(id) {
					var self = this;
					var data = this.getRegulerData();
					data['command'] = "edit";
					data['id'] = id;
					$.post(
						"",
						data,
						function(response) {
							var json = getContent(response);
							if (json == null) return;
							$("#rkbu_id").val(json.header.id);
							$("#rkbu_tahun").val(json.header.tahun);
							$("#rkbu_tahun").removeAttr("disabled");
							$("#rkbu_tahun").attr("disabled", "disabled");
							$("#rkbu_tanggal_diajukan").val(json.header.tanggal_diajukan);
							$("#rkbu_tanggal_diajukan").removeAttr("disabled");
							$("#rkbu_tanggal_diajukan").attr("disabled", "disabled");
							$("#rkbu_keterangan").val(json.header.keterangan);
							$("#rkbu_edit_keterangan").removeAttr("disabled");
							$("#rkbu_edit_keterangan").attr("disabled", "disabled");
							$("#rkbu_edit_keterangan").removeAttr("onclick");
							$("#rkbu_tinjauan").val(json.header.tinjauan);
							$("#rkbu_add").hide();
							row_id = 0;
							$("tbody#drkbu_list").children().remove();
							for(var i = 0; i < json.detail.length; i++) {
								var drkbu_id = json.detail[i].id;
								var drkbu_id_rkbu = json.detail[i].id_rkbu;
								var drkbu_bulan = json.detail[i].bulan;
								var drkbu_nama_barang = json.detail[i].nama_barang;
								var drkbu_jenis_barang = json.detail[i].jenis_barang;
								var drkbu_jumlah_diajukan = json.detail[i].jumlah_diajukan;
								var drkbu_satuan_diajukan = json.detail[i].satuan_diajukan;
								var drkbu_jumlah_disetujui = json.detail[i].jumlah_disetujui;
								var drkbu_satuan_disetujui = json.detail[i].satuan_disetujui;
								var f_disetujui = "-";
								if (Number(drkbu_jumlah_disetujui) != 0 && drkbu_satuan_disetujui != null)
									f_disetujui = drkbu_jumlah_disetujui + " " + drkbu_satuan_disetujui;
								$("tbody#drkbu_list").append(
									"<tr id='data_" + row_id + "'>" +
										"<td style='display: none;' id='data_" + row_id + "_id'>" + drkbu_id + "</td>" +
										"<td style='display: none;' id='data_" + row_id + "_jenis_barang'>" + drkbu_jenis_barang + "</td>" +
										"<td style='display: none;' id='data_" + row_id + "_jumlah_diajukan'>" + drkbu_jumlah_diajukan + "</td>" +
										"<td style='display: none;' id='data_" + row_id + "_satuan_diajukan'>" + drkbu_satuan_diajukan + "</td>" +
										"<td style='display: none;' id='data_" + row_id + "_jumlah_disetujui'>" + drkbu_jumlah_disetujui + "</td>" +
										"<td style='display: none;' id='data_" + row_id + "_satuan_disetujui'>" + drkbu_satuan_disetujui + "</td>" +
										"<td id='data_" + row_id + "_bulan'>" + drkbu_bulan + "</td>" +
										"<td id='data_" + row_id + "_nama_barang'>" + drkbu_nama_barang + "</td>" +
										"<td id='data_" + row_id + "_f_jumlah'>" + drkbu_jumlah_diajukan + " " + drkbu_satuan_diajukan + "</td>" +
										"<td id='data_" + row_id + "_f_disetujui'>" + f_disetujui + "</td>" +
										"<td></td>" +
									"</tr>"
								);
								row_id++;
							}
							$("#rkbu_add_form").smodal("show");
							$("#modal_alert_rkbu_add_form").html("");
							$(".error_field").removeClass("error_field");
							$("#rkbu_save").hide();
							$("#rkbu_save").removeAttr("onclick");
							$("#rkbu_ok").show();
						}
					);
				};
				
				function DRKBUAction(name, page, action, column) {
					this.initialize(name, page, action, column);
				}
				DRKBUAction.prototype.constructor = DRKBUAction;
				DRKBUAction.prototype = new TableAction();
				DRKBUAction.prototype.show_add_form = function() {
					$("#drkbu_id").val("");
					$("#drkbu_bulan").val("");
					$("#drkbu_jenis_barang").val("");
					$("#drkbu_nama_barang").val("");
					$("#drkbu_jumlah").val("");
					$("#drkbu_satuan").val("");
					$("#drkbu_save").removeAttr("onclick");
					$("#drkbu_save").attr("onclick", "drkbu.save()");
					$("#modal_alert_drkbu_add_form").html("");
					$(".error_field").removeClass("error_field");
					$("#drkbu_add_form").smodal("show");
				};
				DRKBUAction.prototype.validate = function() {
					var valid = true;
					var invalid_msg = "";
					var drkbu_nama_barang = $("#drkbu_nama_barang").val();
					var drkbu_jenis_barang = $("#drkbu_jenis_barang").val();
					var drkbu_jumlah = $("#drkbu_jumlah").val();
					var drkbu_satuan = $("#drkbu_satuan").val();
					var drkbu_bulan = $("#drkbu_bulan").val();
					$(".error_field").removeClass("error_field");
					if (drkbu_bulan == null) {
						valid = false;
						invalid_msg += "</br><strong>Bulan</strong> tidak boleh kosong";
						$("#drkbu_bulan").addClass("error_field");
					}
					if (drkbu_nama_barang == "") {
						valid = false;
						invalid_msg += "</br><strong>Nama Barang</strong> tidak boleh kosong";
						$("#drkbu_nama_barang").addClass("error_field");
					}
					if (drkbu_jenis_barang == "") {
						valid = false;
						invalid_msg += "</br><strong>Jenis Barang</strong> tidak boleh kosong";
						$("#drkbu_jenis_barang").addClass("error_field");
					}
					if (drkbu_jumlah == "") {
						valid = false;
						invalid_msg += "</br><strong>Jumlah</strong> tidak boleh kosong";
						$("#drkbu_jumlah").addClass("error_field");
					} else if (!is_numeric(drkbu_jumlah)) {
						valid = false;
						invalid_msg += "</br><strong>Jumlah</strong> seharusnya numerik (0-9)";
						$("#drkbu_jumlah").addClass("error_field");
					}
					if (drkbu_satuan == "") {
						valid = false;
						invalid_msg += "</br><strong>Satuan</strong> tidak boleh kosong";
						$("#drkbu_satuan").addClass("error_field");
					}
					if (!valid) {
						$("#modal_alert_drkbu_add_form").html(
							"<div class='alert alert-block alert-danger'>" +
								"<h4>Peringatan</h4>" +
								invalid_msg +
							"</div>"
						);
					}
					return valid;
				};
				DRKBUAction.prototype.save = function() {
					if (!this.validate()) {
						return;
					}
					var drkbu_id = $("#drkbu_id").val();
					var drkbu_nama_barang = $("#drkbu_nama_barang").val();
					var drkbu_jenis_barang = $("#drkbu_jenis_barang").val();
					var drkbu_jumlah = $("#drkbu_jumlah").val();
					var drkbu_satuan = $("#drkbu_satuan").val();
					var drkbu_bulan = $("#drkbu_bulan").val();
					$("tbody#drkbu_list").append(
						"<tr id='data_" + row_id + "'>" +
							"<td style='display: none;' id='data_" + row_id + "_id'>" + drkbu_id + "</td>" +
							"<td style='display: none;' id='data_" + row_id + "_jenis_barang'>" + drkbu_jenis_barang + "</td>" +
							"<td style='display: none;' id='data_" + row_id + "_jumlah_diajukan'>" + drkbu_jumlah + "</td>" +
							"<td style='display: none;' id='data_" + row_id + "_satuan_diajukan'>" + drkbu_satuan + "</td>" +
							"<td style='display: none;' id='data_" + row_id + "_jumlah_disetujui'>0</td>" +
							"<td style='display: none;' id='data_" + row_id + "_satuan_disetujui'></td>" +
							"<td id='data_" + row_id + "_bulan'>" + drkbu_bulan + "</td>" +
							"<td id='data_" + row_id + "_nama_barang'>" + drkbu_nama_barang + "</td>" +
							"<td id='data_" + row_id + "_f_jumlah'>" + drkbu_jumlah + " " + drkbu_satuan + "</td>" +
							"<td id='data_" + row_id + "_f_disetujui'>-</td>" +
							"<td>" + 
								"<div class='btn-group noprint'>" +
									"<a href='#' onclick='drkbu.edit(" + row_id + ")' data-content='Ubah' data-toggle='popover' class='input btn btn-warning'>" + 
										"<i class='icon-edit icon-white'></i>" +
									"</a>" +
									"<a href='#' onclick='drkbu.delete(" + row_id + ")' data-content='Hapus' data-toggle='popover' class='input btn btn-danger'>" + 
										"<i class='icon-remove icon-white'></i>" + 
									"</a>" +
								"</div>" +
							"</td>" +
						"</tr>"
					);
					row_id++;
					$("#drkbu_add_form").smodal("hide");
				};
				DRKBUAction.prototype.edit = function(r_num) {
					var drkbu_id = $("#data_" + r_num + "_id").text();
					var drkbu_bulan = $("#data_" + r_num + "_bulan").text();
					var drkbu_jenis_barang = $("#data_" + r_num + "_jenis_barang").text();
					var drkbu_nama_barang = $("#data_" + r_num + "_nama_barang").text();
					var drkbu_jumlah = $("#data_" + r_num + "_jumlah_diajukan").text();
					var drkbu_satuan = $("#data_" + r_num + "_satuan_diajukan").text();
					$("#drkbu_id").val(drkbu_id);
					$("#drkbu_bulan").val(drkbu_bulan);
					$("#drkbu_jenis_barang").val(drkbu_jenis_barang);
					$("#drkbu_nama_barang").val(drkbu_nama_barang);
					$("#drkbu_jumlah").val(drkbu_jumlah);
					$("#drkbu_satuan").val(drkbu_satuan);
					$("#modal_alert_drkbu_add_form").html("");
					$(".error_field").removeClass("error_field");
					$("#drkbu_save").removeAttr("onclick");
					$("#drkbu_save").attr("onclick", "drkbu.update(" + r_num + ")");
					$("#drkbu_add_form").smodal("show");
				};
				DRKBUAction.prototype.update = function(r_num) {
					if (!this.validate()) {
						return;
					}
					$("#data_" + r_num + "_id").text($("#drkbu_id").val());
					$("#data_" + r_num + "_bulan").text($("#drkbu_bulan").val());
					$("#data_" + r_num + "_jenis_barang").text($("#drkbu_jenis_barang").val());
					$("#data_" + r_num + "_nama_barang").text($("#drkbu_nama_barang").val());
					$("#data_" + r_num + "_jumlah_diajukan").text($("#drkbu_jumlah").val());
					$("#data_" + r_num + "_satuan_diajukan").text($("#drkbu_satuan").val());
					$("#data_" + r_num + "_f_jumlah").text($("#drkbu_jumlah").val() + " " + $("#drkbu_satuan").val());
					$("#drkbu_add_form").smodal("hide");
				};
				DRKBUAction.prototype.del = function(r_num) {
					var id = $("#data_" + r_num + "_id").text();
					if (id.length == 0) {
						$("#data_" + r_num).remove();
					} else {
						$("#data_" + r_num).attr("style", "display: none;");
						$("#data_" + r_num).attr("class", "deleted");
					}
				};
				
				function BarangAction(name, page, action, column) {
					this.initialize(name, page, action, column);
				}
				BarangAction.prototype.constructor = BarangAction;
				BarangAction.prototype = new TableAction();
				BarangAction.prototype.selected = function(json) {
					$("#drkbu_nama_barang").val(json.nama);
				};
				
				var rkbu;
				var drkbu;
				var barang;
				var row_id;
				var is_chooser;
				var RKBU_PAGE="<?php echo $this->page; ?>";
				var RKBU_PNAME="<?php echo $this->proto_name; ?>";
				var RKBU_PSLUG="<?php echo $this->proto_slug; ?>";
				var RKBU_PIMPL="<?php echo $this->proto_implement; ?>";
				$(document).ready(function() {
					$("#rkbu_edit_tinjauan").removeAttr("onclick");
					$("#rkbu_edit_tinjauan").attr("disabled","disabled");				
					$('[data-toggle="popover"]').popover({
						trigger: 'hover',
						'placement': 'top'
					});
					$(".mydate").datepicker();
					barang = new BarangAction("barang",RKBU_PAGE,"rkbu",new Array()	);
					barang.setSuperCommand("barang");
					barang.setPrototipe(RKBU_PNAME,RKBU_PSLUG,RKBU_PIMPL);
					var drkbu_columns = new Array("id", "id_rkbu", "bulan", "jumlah_diajukan", "satuan_diajukan", "jumlah_disetujui", "satuan_disetujui");
					drkbu = new DRKBUAction("drkbu",RKBU_PAGE,"rkbu",drkbu_columns);
					drkbu.setPrototipe(RKBU_PNAME,RKBU_PSLUG,RKBU_PIMPL);
					var rkbu_columns = new Array("id", "tahun", "tanggal_diajukan", "tanggal_ditinjau", "keterangan", "tinjauan", "disetujui", "locked");
					rkbu = new RKBUAction("rkbu",RKBU_PAGE,	"rkbu",rkbu_columns);
					rkbu.setPrototipe(RKBU_PNAME,RKBU_PSLUG,RKBU_PIMPL);
					rkbu.view();
				});
			</script>
			<?php 
		}
	}
?>