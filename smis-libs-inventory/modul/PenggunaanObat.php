<?php 
	require_once 'smis-framework/smis/template/ModulTemplate.php';
	require_once 'smis-libs-inventory/adapter/PenggunaanObatAdapter.php';
	require_once 'smis-libs-inventory/table/PenggunaanObatTable.php';
	require_once 'smis-libs-inventory/responder/PenggunaanObatDBResponder.php';
	require_once 'smis-libs-inventory/adapter/ObatAdapter.php';

	class PenggunaanObat extends ModulTemplate {
		private $db;
		private $name;
		private $tbl_penggunaan_obat;
		private $tbl_stok_obat;
		private $tbl_riwayat_stok;
		private $tbl_obat_masuk;
		private $page;
		private $action;
		private $penggunaan_obat_table;
		private $proto_name;
		private $proto_implement;
		private $proto_slug;
		
		public function __construct($db, $name_entity, $tbl_penggunaan, $tbl_stok, $tbl_riwayat_stok, $tbl_masuk, $page){
			$this->db=$db;
			$this->name=$name_entity;
			$this->tbl_penggunaan_obat=$tbl_penggunaan;
			$this->tbl_stok_obat=$tbl_stok;
			$this->tbl_riwayat_stok=$tbl_riwayat_stok;
			$this->tbl_obat_masuk=$tbl_masuk;
			$this->page=$page;
			$header=array("Nomor", "Tanggal", "Obat", "Jumlah");
			$this->penggunaan_obat_table = new PenggunaanObatTable($header, $this->name . " : Penggunaan Obat");
			$this->penggunaan_obat_table->setName("penggunaan_obat");
			$this->proto_name="";
			$this->proto_implement="";
			$this->proto_slug="";
		}
		
		public function setPrototype($pname,$pslug,$pimplement){
			$this->proto_name=$pname;
			$this->proto_implement=$pimplement;
			$this->proto_slug=$pslug;
		}

		public function phpPreLoad(){
			$this->penggunaan_obat_table->addModal("id", "hidden", "", "");
			$this->penggunaan_obat_table->addModal("id_stok_obat", "hidden", "", "");
			$this->penggunaan_obat_table->addModal("tanggal", "date", "Tanggal", "");
			$this->penggunaan_obat_table->addModal("nama_obat", "chooser-penggunaan_obat-penggunaan_obat_obat", "Obat", "");
			$this->penggunaan_obat_table->addModal("sisa", "hidden", "", "");
			$this->penggunaan_obat_table->addModal("f_sisa", "text", "Stok", "","y","free",true);
			$this->penggunaan_obat_table->addModal("jumlah", "text", "Jumlah", "","y","numeric",true);
			$this->penggunaan_obat_table->addModal("satuan", "text", "Satuan", "","y","free",true);
			$this->penggunaan_obat_table->addModal("keterangan", "textarea", "Keterangan", "");
			$penggunaan_obat_modal=$this->penggunaan_obat_table->getModal();
			$penggunaan_obat_button = new Button("", "", "OK");
			$penggunaan_obat_button->setClass("btn-success");
			$penggunaan_obat_button->setAtribute("id='penggunaan_obat_ok'");
			$penggunaan_obat_button->setAction("$($(this).data('target')).smodal('hide')");
			$penggunaan_obat_modal->addFooter($penggunaan_obat_button);
			$penggunaan_obat_modal->setTitle("Data Penggunaan Obat");
			echo $penggunaan_obat_modal->getHtml();
			echo $this->penggunaan_obat_table->getHtml();
			echo addJS("framework/smis/js/table_action.js");
			echo addJS("framework/bootstrap/js/bootstrap-datepicker.js");
			echo addCSS("framework/bootstrap/css/datepicker.css");	
		}
		
		public function superCommand($super_command){
			$header=array("Nomor", "Obat", "Jenis", "Stok", "Tanggal Exp.");
			$obat_table = new Table($header);
			$obat_table->setName("penggunaan_obat_obat");
			$obat_table->setModel(Table::$SELECT);
			
			$obat_adapter = new ObatAdapter();
			$obat_dbtable = new DBTable($this->db, $this->tbl_stok_obat);
			$filter = "";
			if (isset($_POST['kriteria'])) {
				$filter = " AND (" . $this->tbl_stok_obat . ".nama_obat LIKE '%" . $_POST['kriteria'] . "%' OR ".$this->tbl_stok_obat.".nama_jenis_obat LIKE '%" . $_POST['kriteria'] . "%') ";
			}
			$query_value = "
				SELECT " . $this->tbl_stok_obat . ".*
				FROM " . $this->tbl_stok_obat . " LEFT JOIN " . $this->tbl_obat_masuk . " ON " . $this->tbl_stok_obat . ".id_obat_masuk = " . $this->tbl_obat_masuk . ".id
				WHERE " . $this->tbl_stok_obat . ".prop NOT LIKE 'del' AND " . $this->tbl_obat_masuk . ".prop NOT LIKE 'del' AND " . $this->tbl_obat_masuk . ".status = 'sudah' " . $filter . "
			";
			$query_count = "
				SELECT COUNT(*)
				FROM " . $this->tbl_stok_obat . " LEFT JOIN " . $this->tbl_obat_masuk . " ON " . $this->tbl_stok_obat . ".id_obat_masuk = " . $this->tbl_obat_masuk . ".id
				WHERE " . $this->tbl_stok_obat . ".prop NOT LIKE 'del' AND " . $this->tbl_obat_masuk . ".prop NOT LIKE 'del' AND " . $this->tbl_obat_masuk . ".status = 'sudah' " . $filter . "
			";
			$obat_dbtable->setPreferredQuery(true, $query_value, $query_count);
			$obat_dbresponder = new DBResponder($obat_dbtable,$obat_table,$obat_adapter);
			$super_command = new SuperCommand();
			$super_command->addResponder("penggunaan_obat_obat", $obat_dbresponder);
			$init = $super_command->initialize();
			if ($init != null) {
				echo $init;
				return;
			}
					
		}
		
		
		public function command($command){
				$penggunaan_obat_adapter = new PenggunaanObatAdapter();
				$columns = array("id", "id_stok_obat", "tanggal", "jumlah", "keterangan");
				$penggunaan_obat_dbtable = new DBTable(	$this->db,$this->tbl_penggunaan_obat,$columns);
				$filter = "";
				if (isset($_POST['kriteria'])) {
					$filter = " AND (".$this->tbl_stok_obat.".nama_obat LIKE '%" . $_POST['kriteria'] . "%')";
				}
				$query_value = "
					SELECT ".$this->tbl_penggunaan_obat.".*, ".$this->tbl_stok_obat.".nama_obat, ".$this->tbl_stok_obat.".nama_jenis_obat, ".$this->tbl_stok_obat.".satuan
					FROM ".$this->tbl_penggunaan_obat." LEFT JOIN ".$this->tbl_stok_obat." ON ".$this->tbl_penggunaan_obat.".id_stok_obat = ".$this->tbl_stok_obat.".id
					WHERE ".$this->tbl_penggunaan_obat.".prop NOT LIKE 'del' " . $filter . "
				";
				$query_count = "
					SELECT COUNT(*)
					FROM ".$this->tbl_penggunaan_obat."
					WHERE prop NOT LIKE 'del'
				";
				$penggunaan_obat_dbtable->setPreferredQuery(true, $query_value, $query_count);			
				$penggunaan_obat_dbresponder = new PenggunaanObatDBResponder($penggunaan_obat_dbtable,$this->penggunaan_obat_table,$penggunaan_obat_adapter,$this->tbl_penggunaan_obat,$this->tbl_stok_obat,$this->tbl_riwayat_stok);
				$data = $penggunaan_obat_dbresponder->command($_POST['command']);
				echo json_encode($data);
				return;
		}
		
		public function jsPreLoad(){
			?>
			<script type="text/javascript">
				function PenggunaanObatAction(name, page, action, column) {
					this.initialize(name, page, action, column);
				}
				PenggunaanObatAction.prototype.constructor = PenggunaanObatAction;
				PenggunaanObatAction.prototype = new TableAction();
				PenggunaanObatAction.prototype.show_add_form = function() {
					var today = new Date();
					$("#penggunaan_obat_id").val("");
					$("#penggunaan_obat_tanggal").val(today.getFullYear() + "-" + (Number(today.getMonth())+1) + "-" + today.getDate());
					$("#penggunaan_obat_tanggal").removeAttr("disabled");
					$("#penggunaan_obat_id_stok_obat").val("");
					$("#penggunaan_obat_nama_obat").val("");
					$("#penggunaan_obat_nama_obat").removeAttr("disabled");
					$("#penggunaan_obat_nama_obat").attr("disabled", "disabled");
					$("#penggunaan_obat_chooser_nama_obat").removeAttr("onclick");
					$("#penggunaan_obat_chooser_nama_obat").attr("onclick", "penggunaan_obat.chooser('penggunaan_obat','penggunaan_obat_nama_obat','penggunaan_obat_obat',penggunaan_obat_obat)");
					$("#penggunaan_obat_chooser_nama_obat").removeClass("btn-info");
					$("#penggunaan_obat_chooser_nama_obat").removeClass("btn-inverse");
					$("#penggunaan_obat_chooser_nama_obat").addClass("btn-info");
					$("#penggunaan_obat_sisa").val(0);
					$("#penggunaan_obat_f_sisa").val("");
					$(".penggunaan_obat_f_sisa").show();
					$("#penggunaan_obat_jumlah").val("");
					$("#penggunaan_obat_jumlah").removeAttr("disabled");
					$("#penggunaan_obat_satuan").val("");
					$("#penggunaan_obat_keterangan").val("");
					$("#penggunaan_obat_keterangan").removeAttr("disabled");
					$("#penggunaan_obat_save").show();
					$("#penggunaan_obat_save").removeAttr("onclick");
					$("#penggunaan_obat_save").attr("onclick", "penggunaan_obat.save()");
					$("#modal_alert_penggunaan_obat_add_form").html();
					$(".error_field").removeClass("error_field");
					$("#penggunaan_obat_ok").hide();
					$("#penggunaan_obat_add_form").smodal("show");
				};
				PenggunaanObatAction.prototype.validate = function() {
					var valid = true;
					var invalid_msg = "";
					var tanggal = $("#penggunaan_obat_tanggal").val();
					var nama_obat = $("#penggunaan_obat_nama_obat").val();
					var jumlah = $("#penggunaan_obat_jumlah").val();
					var sisa = $("#penggunaan_obat_sisa").val();
					var keterangan = $("#penggunaan_obat_keterangan").val();
					$(".error_field").removeClass("error_field");
					if (tanggal == "") {
						valid = false;
						invalid_msg += "<br/><strong>Tanggal</strong> tidak boleh kosong";
						$("#penggunaan_obat_tanggal").addClass("error_field");
					}
					if (nama_obat == "") {
						valid = false;
						invalid_msg += "<br/><strong>Obat</strong> tidak boleh kosong";
						$("#penggunaan_obat_nama_obat").addClass("error_field");
					}
					if (jumlah == "") {
						valid = false;
						invalid_msg += "<br/><strong>Jumlah</strong> tidak boleh kosong";
						$("#penggunaan_obat_jumlah").addClass("error_field");
					} else if (!is_numeric(jumlah)) {
						valid = false;
						invalid_msg += "<br/><strong>Jumlah</strong> seharusnya numerik (0-9)";
						$("#penggunaan_obat_jumlah").addClass("error_field");
					} else if (parseFloat(jumlah) > parseFloat(sisa)) {
						valid = false;
						invalid_msg += "<br/><strong>Jumlah</strong> melebihi stok";
						$("#penggunaan_obat_jumlah").addClass("error_field");
					}
					if (keterangan == "") {
						valid = false;
						invalid_msg += "<br/><strong>Keterangan</strong> tidak boleh kosong";
						$("#penggunaan_obat_keterangan").addClass("error_field");
					}
					if (!valid) {
						$("#modal_alert_penggunaan_obat_add_form").html(
							"<div class='alert alert-block alert-danger'>" +
								"<h4>Peringatan</h4>" +
								invalid_msg +
							"</div>"
						);
					}
					return valid;
				};
				PenggunaanObatAction.prototype.save = function() {
					if (!this.validate()) {
						return;
					}
					$("#penggunaan_obat_add_form").smodal("hide");
					showLoading();
					var self = this;
					var data = this.getRegulerData();				
					data['command'] = "save";
					data['id'] = $("#penggunaan_obat_id").val();
					data['id_stok_obat'] = $("#penggunaan_obat_id_stok_obat").val();
					data['tanggal'] = $("#penggunaan_obat_tanggal").val();
					data['jumlah'] = $("#penggunaan_obat_jumlah").val();
					data['keterangan'] = $("#penggunaan_obat_keterangan").val();
					$.post(
						"",
						data,
						function(response) {
							var json = getContent(response);
							if (json == null) {
								$("#penggunaan_obat_add_form").smodal("show");
							} else {
								self.view();
							}
							dismissLoading();
						}
					);
				};
				PenggunaanObatAction.prototype.detail = function(id) {
					var data = this.getRegulerData();				
					data['command'] = "edit";
					data['id'] = id;
					$.post(
						"",
						data,
						function(response) {
							var json = getContent(response);
							if (json == null) return;
							$("#penggunaan_obat_id").val("");
							$("#penggunaan_obat_tanggal").val(json.tanggal);
							$("#penggunaan_obat_tanggal").removeAttr("disabled");
							$("#penggunaan_obat_tanggal").attr("disabled", "disabled");
							$("#penggunaan_obat_id_stok_obat").val(json.id_stok_obat);
							$("#penggunaan_obat_nama_obat").val(json.nama_obat);
							$("#penggunaan_obat_nama_obat").removeAttr("disabled");
							$("#penggunaan_obat_nama_obat").attr("disabled", "disabled");
							$("#penggunaan_obat_chooser_nama_obat").removeAttr("onclick");
							$("#penggunaan_obat_chooser_nama_obat").removeClass("btn-info");
							$("#penggunaan_obat_chooser_nama_obat").removeClass("btn-inverse");
							$("#penggunaan_obat_chooser_nama_obat").addClass("btn-inverse");
							$("#penggunaan_obat_sisa").val(json.sisa);
							$("#penggunaan_obat_f_sisa").val(json.sisa + " " + json.satuan);
							$(".penggunaan_obat_f_sisa").hide();
							$("#penggunaan_obat_jumlah").val(json.jumlah);
							$("#penggunaan_obat_jumlah").removeAttr("disabled");
							$("#penggunaan_obat_jumlah").attr("disabled", "disabled");
							$("#penggunaan_obat_satuan").val(json.satuan);
							$("#penggunaan_obat_keterangan").val(json.keterangan);
							$("#penggunaan_obat_keterangan").attr("disabled", "disabled");
							$("#modal_alert_penggunaan_obat_add_form").html();
							$(".error_field").removeClass("error_field");
							$("#penggunaan_obat_save").hide();
							$("#penggunaan_obat_save").removeAttr("onclick");
							$("#penggunaan_obat_ok").show();
							$("#penggunaan_obat_add_form").smodal("show");
						}
					);
				};
				
				function ObatAction(name, page, action, column) {
					this.initialize(name, page, action, column);
				}
				ObatAction.prototype.constructor = ObatAction;
				ObatAction.prototype = new TableAction();
				ObatAction.prototype.selected = function(json) {
					$("#penggunaan_obat_id_stok_obat").val(json.id);
					$("#penggunaan_obat_nama_obat").val(json.nama_obat);
					$("#penggunaan_obat_sisa").val(json.sisa);
					$("#penggunaan_obat_f_sisa").val(json.sisa + " " + json.satuan);
					$("#penggunaan_obat_satuan").val(json.satuan);
				};

				var PENGGUNAAN_OBAT_PNAME="<?php echo $this->proto_name; ?>";
				var PENGGUNAAN_OBAT_PSLUG="<?php echo $this->proto_slug; ?>";
				var PENGGUNAAN_OBAT_PIMPL="<?php echo $this->proto_implement; ?>";
				var PENGGUNAAN_OBAT_ENTITY="<?php echo $this->page; ?>";
				var penggunaan_obat;
				var penggunaan_obat_obat;
				$(document).ready(function() {
					$('[data-toggle="popover"]').popover({
						trigger: 'hover',
						'placement': 'top'
					});
					$(".mydate").datepicker();
					$("#smis-chooser-modal").on("show", function() {
						$("#smis-chooser-modal").removeClass("half_model");
						$("#smis-chooser-modal").addClass("half_model");
					});
					$("#smis-chooser-modal").on("hidden", function() {
						$("#smis-chooser-modal").removeClass("half_model");
					});				
					penggunaan_obat_obat = new ObatAction("penggunaan_obat_obat",PENGGUNAAN_OBAT_ENTITY,"penggunaan_obat",new Array());
					penggunaan_obat_obat.setSuperCommand("penggunaan_obat_obat");
					penggunaan_obat_obat.setPrototipe(PENGGUNAAN_OBAT_PNAME,PENGGUNAAN_OBAT_PSLUG,PENGGUNAAN_OBAT_PIMPL);
					var penggunaan_obat_columns = new Array("id", "id_stok_obat", "tanggal", "jumlah", "keterangan");
					penggunaan_obat = new PenggunaanObatAction("penggunaan_obat",PENGGUNAAN_OBAT_ENTITY,"penggunaan_obat",penggunaan_obat_columns);
					penggunaan_obat.setPrototipe(PENGGUNAAN_OBAT_PNAME,PENGGUNAAN_OBAT_PSLUG,PENGGUNAAN_OBAT_PIMPL);
					penggunaan_obat.view();
				});
			</script>
			<?php 
		}	
	}
?>