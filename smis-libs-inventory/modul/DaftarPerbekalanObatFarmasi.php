<?php
	require_once 'smis-framework/smis/template/ModulTemplate.php';
	require_once 'smis-base/smis-include-service-consumer.php';
	
	class DaftarPerbekalanObatFarmasi extends ModulTemplate {
		private $db;
		private $name;
		private $page;
		private $action;
		private $table;
		private $proto_name;
		private $proto_implement;
		private $proto_slug;
		
		public function __construct($db, $name_entity, $dbslug, $page, $entity = "") {
			$this->db = $db;
			$this->name = $name_entity;
			if ($entity!="") $entity="_".$entity;
			$this->page = $page;				
			$header = array("Nama Obat", "Jenis Obat", "Produsen", "Sisa", "Satuan", "Ket. Satuan");
			$this->table = new Table(
				$header, 
				$this->name." : Daftar Perbekalan Obat Gudang Farmasi",
				null,
				true
			);
			$this->table->setName("dpfr");
			$this->table->setAction(false);
			$this->proto_name = "";
			$this->proto_implement = "";
			$this->proto_slug = "";
		}
		
		public function setPrototype($pname, $pslug, $pimplement){
			$this->proto_name = $pname;
			$this->proto_implement = $pimplement;
			$this->proto_slug = $pslug;
		}
		
		public function command($command) {
			return;
		}
		
		public function superCommand($super_command) {
			$dpfr_adapter = new SimpleAdapter();
			$dpfr_adapter->add("Nama Obat", "nama_obat");
			$dpfr_adapter->add("Jenis Obat", "nama_jenis_obat");
			$dpfr_adapter->add("Produsen", "produsen");
			$dpfr_adapter->add("Sisa", "sisa");
			$dpfr_adapter->add("Satuan", "satuan");
			$dpfr_adapter->add("Ket. Satuan", "ket_satuan");
			$dpfr_service_responder = new ServiceResponder(
				$this->db,
				$this->table,
				$dpfr_adapter,
				"get_stock",
				"gudang_farmasi"
			);
			$super_command = new SuperCommand();
			$super_command->addResponder("dpfr", $dpfr_service_responder);
			$init = $super_command->initialize();
			if ($init != null) {
				echo $init;
				return;
			}
		}
		
		public function jsLoader() {
			echo addJS("framework/smis/js/table_action.js");
		}
		
		public function cssLoader() {
			
		}
		
		public function phpPreLoad() {
			echo $this->table->getHtml();
		}
		
		public function jsPreLoad() {
			?>
			<script type="text/javascript">
				function DPFRAction(name, page, action, column) {
					this.initialize(name, page, action, column);
				}
				DPFRAction.prototype.constructor = DPFRAction;
				DPFRAction.prototype = new TableAction();
				
				var dpfr;
				var DPFR_PAGE = "<?php echo $this->page; ?>";
				var DPFR_PNAME = "<?php echo $this->proto_name; ?>";
				var DPFR_PSLUG = "<?php echo $this->proto_slug; ?>";
				var DPFR_PIMPL = "<?php echo $this->proto_implement; ?>";
				$(document).ready(function() {
					$('[data-toggle="popover"]').popover({
						trigger: 'hover',
						'placement': 'top'
					});
					dpfr = new DPFRAction(
						"dpfr",
						DPFR_PAGE,
						"daftar_perbekalan_obat_fr",
						new Array()
					);
					dpfr.setSuperCommand("dpfr");
					dpfr.setPrototipe(DPFR_PNAME, DPFR_PSLUG, DPFR_PIMPL);
					dpfr.view();
				});
			</script>
			<?php
		}
	}
?>