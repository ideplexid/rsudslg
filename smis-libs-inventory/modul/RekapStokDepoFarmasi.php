<?php
	require_once ('smis-framework/smis/template/ModulTemplate.php');

    class RekapStokDepoFarmasi extends ModulTemplate {
        private $db;
        private $name;
        private $page;
		private $proto_name;
		private $proto_implement;
		private $proto_slug;

        private $form;
        private $table;

        public function __construct($db, $entity_name, $page) {
			$this->db = $db;
			$this->name = $entity_name;
			$this->page = $page;
                   
			$this->instantiateUI();
		}

        public function setPrototype($pname, $pslug, $pimplement) {
			$this->proto_name = $pname;
			$this->proto_implement = $pimplement;
			$this->proto_slug = $pslug;
		}

        private function instantiateUI() {
            $this->table = new Table(
                array("No.", "Nama Obat", "Jenis Obat", "Stok", "Nilai"),
                "",
                null,
                true
            );
            $this->table->setAction(false);
            $this->table->setName("rekap_stok_depo");
            
            $this->form = new Form("", "", $this->name . " : Rekap. Stok Depo Farmasi");
            $filter_stok_option = new OptionBuilder();
            $filter_stok_option->add("Semua", 0);
            $filter_stok_option->add("Semua, Kecuali Kosong", 1, 1);
            $filter_stok_option->add("Kosong", 2);
            $filter_stok_select = new Select("rekap_stok_depo_filter_stok", "rekap_stok_depo_filter_stok", $filter_stok_option->getContent());
            $this->form->addElement("Filter Stok", $filter_stok_select);
            $depo_farmasi_option = new OptionBuilder();
            $depo_farmasi_option->add("IRJA-IRNA", "depo_farmasi_irja_irna", "1");
            $depo_farmasi_option->add("ICU", "depo_farmasi_icu");
            $depo_farmasi_option->add("OK", "depo_farmasi_ok");
            $depo_farmasi_option->add("IGD", "depo_farmasi_igd");
            $depo_farmasi_option->add("AKHP", "depo_farmasi_akhp");
            $depo_farmasi_select = new Select("rekap_stok_depo_depo_farmasi", "rekap_stok_depo_depo_farmasi", $depo_farmasi_option->getContent());
            $this->form->addElement("Depo Farmasi", $depo_farmasi_select);
        }

        public function command($command) {
			$adapter =new SummaryAdapter();
			$adapter->addFixValue("Stok", "<strong>Total</strong>");
			$adapter->addSummary("Nilai", "uang","money Rp.");
			$adapter->setUseID(false);
			$adapter->add("Nomor", "id_obat","digit8");
			$adapter->add("Nama Obat", "nama_obat");
			$adapter->add("Jenis Obat", "nama_jenis_obat");
			$adapter->add("Stok", "stok");
			$adapter->add("Nilai", "uang","money Rp.");
			$columns = array("nama_obat", "nama_jenis_obat");

            require_once($_POST['depo_farmasi'] . "/library/InventoryLibrary.php");
			$dbtable = new DBTable($this->db, InventoryLibrary::$_TBL_STOK_OBAT, $columns);
			$filter = "";
			if (isset($_POST['kriteria'])) {
				$filter = " 
                    AND (		
                        nama_obat LIKE '%" . $_POST['kriteria'] . "%' 
						    OR 	nama_jenis_obat LIKE '%" . $_POST['kriteria'] . "%'
					)
                ";
			}
			
			if (isset($_POST['filter_stok']) && $_POST['filter_stok'] == 1) {
				$filter .= " AND sisa > 0 ";
			} else if(isset($_POST['filter_stok']) && $_POST['filter_stok'] == 2){
				$filter .= " AND sisa <= 0 ";
			}
			
			$v_stok = "
                SELECT 
                    id_obat, nama_obat, nama_jenis_obat, 
                    SUM(uang) uang, 
                    GROUP_CONCAT(CONCAT(sisa, ' ', satuan) ORDER BY satuan, sisa ASC SEPARATOR ', ') stok
                FROM 
                    (
                        SELECT 
                            id_obat, nama_obat, nama_jenis_obat, 
                            SUM(sisa) sisa, 
                            SUM(hna * sisa) uang, 
                            satuan, konversi, satuan_konversi
                        FROM 
                            " . InventoryLibrary::$_TBL_STOK_OBAT . " 
                                LEFT JOIN " . InventoryLibrary::$_TBL_OBAT_MASUK . " ON " . InventoryLibrary::$_TBL_STOK_OBAT . ".id_obat_masuk = " . InventoryLibrary::$_TBL_OBAT_MASUK . ".id
                        WHERE 
                            " . InventoryLibrary::$_TBL_STOK_OBAT . ".prop LIKE '' 
                                AND " . InventoryLibrary::$_TBL_OBAT_MASUK . ".prop LIKE '' 
                                AND " . InventoryLibrary::$_TBL_OBAT_MASUK . ".status = 'sudah' " . $filter . "
                        GROUP BY 
                            id_obat, satuan, konversi, satuan_konversi
                    ) v_obat
                GROUP BY 
                    id_obat
            ";
			
			$query_value = "SELECT * FROM ( $v_stok ) v_stok ORDER BY nama_obat, nama_jenis_obat ASC";
			$query_count = "SELECT COUNT(*) FROM ( $v_stok ) v_stok";

			$dbtable->setPreferredQuery(true, $query_value, $query_count);
			$dbresponder = new DBResponder($dbtable, $this->table, $adapter);
			$data = $dbresponder->command($_POST['command']);
			echo json_encode($data);
			return;
		}

        public function phpPreLoad() {
			echo $this->form->getHtml();
			echo $this->table->getHtml();
			echo addJS("framework/smis/js/table_action.js");
		}
		
		public function jsPreLoad() {
			?>		
			<script type="text/javascript">
				var REKAP_STOK_DEPO_PNAME = "<?php echo $this->proto_name; ?>";
				var REKAP_STOK_DEPO_PSLUG = "<?php echo $this->proto_slug; ?>";
				var REKAP_STOK_DEPO_PIMPL = "<?php echo $this->proto_implement; ?>";
				var REKAP_STOK_DEPO_ENTITY = "<?php echo $this->page; ?>";
				var rekap_stok_depo;

				$(document).ready(function() {
					$('[data-toggle="popover"]').popover({
						trigger: 'hover',
						'placement': 'top'
					});
					rekap_stok_depo = new TableAction("rekap_stok_depo", REKAP_STOK_DEPO_ENTITY, "rekap_stok_depo", new Array());
					rekap_stok_depo.setPrototipe(REKAP_STOK_DEPO_PNAME, REKAP_STOK_DEPO_PSLUG, REKAP_STOK_DEPO_PIMPL);
					rekap_stok_depo.addViewData = function(data){
						data['filter_stok'] = $("#rekap_stok_depo_filter_stok").val();
                        data['depo_farmasi'] = $("#rekap_stok_depo_depo_farmasi").val();
						return data;
					};
					rekap_stok_depo.view();
				});
			</script>
			<?php 
		}
    }