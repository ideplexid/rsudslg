<?php 
	require_once 'smis-framework/smis/template/ModulTemplate.php';
	require_once 'smis-libs-inventory/adapter/PSOEDAdapter.php';

	class StockObatED extends ModulTemplate{
		private $db;
		private $name;
		private $tbl_stok_obat;
		private $tbl_obat_masuk;
		private $page;
		private $action;
		private $psoed_table;
		private $proto_name;
		private $proto_implement;
		private $proto_slug;
		
		public function __construct($db, $name_entity, $tbl_stok, $tbl_masuk, $page){
			$this->db=$db;
			$this->name=$name_entity;
			$this->tbl_stok_obat=$tbl_stok;
			$this->tbl_obat_masuk=$tbl_masuk;
			$this->page=$page;
			$header=array("Nomor", "Obat", "Jenis Obat", "Produsen", "Vendor", "Jumlah", "Ket. Jumlah", "Tgl. Exp.");
			$this->psoed_table = new Table($header);
			$this->psoed_table->setName("psoed");
			$this->psoed_table->setAddButtonEnable(false);
			$this->psoed_table->setReloadButtonEnable(false);
			$this->psoed_table->setPrintButtonEnable(false);
			$this->psoed_table->setAction(false);
			$this->proto_name="";
			$this->proto_implement="";
			$this->proto_slug="";
		}
		
		public function setPrototype($pname,$pslug,$pimplement){
			$this->proto_name=$pname;
			$this->proto_implement=$pimplement;
			$this->proto_slug=$pslug;
		}	
		
		public function command($command){
			$psoed_adapter = new PSOEDAdapter();
			$psoed_dbtable = new DBTable($this->db,$this->tbl_stok_obat);
			$filter = "";
			if (isset($_POST['kriteria'])) {
				$filter = " AND (" . $this->tbl_stok_obat.".nama_obat LIKE '%" . $_POST['kriteria'] . "%' OR " . $this->tbl_stok_obat.".nama_jenis_obat LIKE '%" . $_POST['kriteria'] . "%' OR " . $this->tbl_stok_obat.".nama_vendor LIKE '%" . $_POST['kriteria'] . "%' OR " . $this->tbl_stok_obat.".produsen LIKE '%" . $_POST['kriteria'] . "%')";
			}
			$query_value = "
				SELECT " . $this->tbl_stok_obat . ".*
				FROM " . $this->tbl_stok_obat . " LEFT JOIN " . $this->tbl_obat_masuk . " ON " . $this->tbl_stok_obat . ".id_obat_masuk = " . $this->tbl_obat_masuk . ".id
				WHERE " . $this->tbl_stok_obat . ".tanggal_exp != '0000-00-00' AND DATEDIFF(tanggal_exp, '" . $_POST['tanggal'] . "') <= " . $_POST['rentang'] . " AND " . $this->tbl_obat_masuk . ".status = 'sudah' " . $filter . "
				ORDER BY " . $this->tbl_stok_obat . ".tanggal_exp, " . $this->tbl_stok_obat . ".nama_obat ASC
			";
			$query_count = "
				SELECT COUNT(*)
				FROM (
					" . $query_value . "
				) v
			";
			$psoed_dbtable->setPreferredQuery(true, $query_value, $query_count);
			$psoed_dbresponder = new DBResponder($psoed_dbtable,$this->psoed_table,$psoed_adapter);
			$data = $psoed_dbresponder->command($_POST['command']);
			echo json_encode($data);
			return;	
		}
		
		public function phpPreLoad(){
			$pengecekan_stok_obat_ed_form = new Form("psoed_form", "", $this->name . " : Stok Obat ED");
			$tanggal_text = new Text("psoed_tanggal", "psoed_tanggal", date('Y-m-d'));
			$tanggal_text->setModel(Text::$DATE);
			$pengecekan_stok_obat_ed_form->addElement("Tgl. Sekarang", $tanggal_text);
			$rentang_option = new OptionBuilder();
			$rentang_option->add("7 hari", "7");
			$rentang_option->add("30 hari", "30");
			$rentang_option->add("60 hari", "60");
			$rentang_option->add("90 hari", "90");
			$rentang_option->add("180 hari", "180", "1");
			$rentang_select = new Select("psoed_rentang", "psoed_rentang", $rentang_option->getContent());
			$pengecekan_stok_obat_ed_form->addElement("Rentang", $rentang_select);
			$show_button = new Button("", "", "Tampilkan");
			$show_button->setClass("btn-primary");
			$show_button->setIcon("icon-white icon-repeat");
			$show_button->setIsButton(Button::$ICONIC);
			$show_button->setAction("psoed.view()");
			$pengecekan_stok_obat_ed_form->addElement("", $show_button);
			
			echo $pengecekan_stok_obat_ed_form->getHtml();
			echo "<div id'table_content'>";
			echo $this->psoed_table->getHtml();
			echo "</div>";
			echo addJS("framework/smis/js/table_action.js");
			echo addJS("framework/bootstrap/js/bootstrap-datepicker.js");
			echo addCSS("framework/bootstrap/css/datepicker.css");
		}
		
		public function jsPreLoad(){
			?>
			<script type="text/javascript">
				var PSOED_PNAME="<?php echo $this->proto_name; ?>";
				var PSOED_PSLUG="<?php echo $this->proto_slug; ?>";
				var PSOED_PIMPL="<?php echo $this->proto_implement; ?>";
				var PSOED_ENTITY="<?php echo $this->page; ?>";
				var psoed;
				$(document).ready(function() {
					$('[data-toggle="popover"]').popover({
						trigger: 'hover',
						'placement': 'top'
					});
					$(".mydate").datepicker();
					psoed = new TableAction("psoed",PSOED_ENTITY,"pengecekan_stok_obat_ed",new Array());
					psoed.setPrototipe(PSOED_PNAME,PSOED_PSLUG,PSOED_PIMPL);
					psoed.addViewData = function(data) {
						data['tanggal'] = $("#psoed_tanggal").val();
						data['rentang'] = $("#psoed_rentang").val();
						data['kriteria'] = $("#psoed_kriteria").val();
						return data;
					};
					psoed.view();
				});
			</script>
			<?php 
		}
	}
?>