<?php 
	require_once 'smis-base/smis-include-service-consumer.php';
	require_once 'smis-framework/smis/template/ModulTemplate.php';
	require_once 'smis-libs-inventory/service_consumer/ObatKeluarServiceConsumer.php';
	require_once 'smis-libs-inventory/adapter/ObatMasukAdapter.php';
	require_once 'smis-libs-inventory/responder/ObatMasukResponder.php';
	require_once 'smis-libs-inventory/table/ObatMasukTable.php';

	class ObatMasuk extends ModulTemplate {		
		private $db;
		private $name;
		private $tbl_obat_masuk;
		private $tbl_stok_obat;
		private $tbl_riwayat_stok_obat;
		private $page;
		private $action;
		private $obat_masuk_table;
		private $proto_name;
		private $proto_implement;
		private $proto_slug;
		
		public function __construct($db, $name_entity, $slug_table_parent, $slug_table_child, $slug_table_log, $page){
			$this->db=$db;
			$this->name=$name_entity;
			$this->tbl_obat_masuk=$slug_table_parent;
			$this->tbl_stok_obat=$slug_table_child;
			$this->tbl_riwayat_stok_obat=$slug_table_log;
			$this->page=$page;
			$this->obat_masuk_table= new ObatMasukTable(array("No. Mutasi", "Tanggal Masuk", "Gudang", "Status"), $this->name . " : Obat Masuk");
			$this->obat_masuk_table->setName("obat_masuk");
			$this->obat_masuk_table->setAddButtonEnable(false);
		}
		
		public function setPrototype($pname,$pslug,$pimplement){
			$this->proto_name=$pname;
			$this->proto_implement=$pimplement;
			$this->proto_slug=$pslug;
		}
		
		public function command($command){
			$obat_masuk_adapter = new ObatMasukAdapter();
			$columns = array("f_id", "id", "tanggal", "unit", "status", "keterangan");
			$obat_masuk_dbtable = new DBTable($this->db,$this->tbl_obat_masuk,$columns);
			$obat_masuk_dbtable->addCustomKriteria(" id ", " > 0 ");
			$obat_masuk_dbtable->addCustomKriteria(" unit ", " = 'gudang_farmasi' ");
			$obat_masuk_dbtable->addCustomKriteria(" status ", " != 'dikembalikan' ");
			$obat_masuk_dbtable->setOrder(" FIELD(status, 'belum', 'sudah') ASC, tanggal DESC, id DESC ");
			$obat_masuk_dbresponder = new ObatMasukDBResponder($obat_masuk_dbtable,$this->obat_masuk_table,$obat_masuk_adapter,$this->tbl_stok_obat, $this->tbl_riwayat_stok_obat);
			$data = $obat_masuk_dbresponder->command($_POST['command']);
			if (isset($_POST['push_command'])) {
				$id['id'] = $data['content']['id'];
				$obat_masuk_row = $obat_masuk_dbtable->select($id);
				$push_command = "push_" . $_POST['push_command'];
				global $user;
				$nama_user = $user->getName();
				$set_obat_keluar_status_service_consumer = new SetObatKeluarStatusServiceConsumer($this->db, $nama_user, $obat_masuk_row->f_id, $obat_masuk_row->status, $obat_masuk_row->keterangan, $push_command);
				$set_obat_keluar_status_service_consumer->execute();
			}
			echo json_encode($data);
			return;
		}
		
		public function phpPreLoad(){
			$this->obat_masuk_table->addModal("id", "hidden", "", "");
			$this->obat_masuk_table->addModal("tanggal", "date", "Tanggal","", "n",null,true);
			$this->obat_masuk_table->addModal("unit", "text", "Unit", "","n",null,true);
			$obat_masuk_modal = $this->obat_masuk_table->getModal();
			$obat_masuk_modal->setTitle("Data Obat Masuk");
			$obat_masuk_modal->setModalSize(Modal::$FULL_MODEL);
			$dobat_masuk_table = new Table(	array("Obat", "Jumlah", "Keterangan"));
			$dobat_masuk_table->setName("dobat_masuk");
			$dobat_masuk_table->setFooterVisible(false);
			$dobat_masuk_table->setAddButtonEnable(false);
			$dobat_masuk_table->setReloadButtonEnable(false);
			$dobat_masuk_table->setPrintButtonEnable(false);
			$obat_masuk_modal->addBody("dobat_masuk_table", $dobat_masuk_table);
			$obat_masuk_modal->clearFooter();
			
			$obat_masuk_button = new Button("", "", "Terima");
			$obat_masuk_button->setClass("btn-success");
			$obat_masuk_button->setAction("obat_masuk.accept()");
			$obat_masuk_button->setAtribute("id='obat_masuk_accept'");
			$obat_masuk_button->setIcon("fa fa-check");
			$obat_masuk_button->setIsButton(Button::$ICONIC);
			$obat_masuk_modal->addFooter($obat_masuk_button);
			
			$obat_masuk_button = new Button("", "", "Kembalikan");
			$obat_masuk_button->setClass("btn-danger");
			$obat_masuk_button->setAction("obat_masuk.retur()");
			$obat_masuk_button->setAtribute("id='obat_masuk_return'");
			$obat_masuk_button->setIcon("fa fa-times");
			$obat_masuk_button->setIsButton(Button::$ICONIC);
			$obat_masuk_modal->addFooter($obat_masuk_button);
			
			$obat_masuk_button = new Button("", "", "OK");
			$obat_masuk_button->setClass("btn-success");
			$obat_masuk_button->setAction("$($(this).data('target')).smodal('hide')");
			$obat_masuk_button->setAtribute("id='obat_masuk_ok'");
			$obat_masuk_modal->addFooter($obat_masuk_button);
			
			echo $obat_masuk_modal->getHtml();
			echo $this->obat_masuk_table->getHtml();
			echo addJS("framework/smis/js/table_action.js");
		}
		
		public function jsPreLoad(){
			?>
			<script type="text/javascript">
				function ObatMasukAction(name, page, action, column) {
					this.initialize(name, page, action, column);
				}
				ObatMasukAction.prototype.constructor = ObatMasukAction;
				ObatMasukAction.prototype = new TableAction();
				ObatMasukAction.prototype.edit = function(id) {
					var self = this;
					var data = this.getRegulerData();
					data['command'] = "edit";
					data['id'] = id;
					$.post(
						"",
						data,
						function(response) {
							var json = getContent(response);
							if (json == null) return;
							$("#obat_masuk_id").val(json.header.id);
							$("#obat_masuk_tanggal").val(json.header.tanggal);
							var unit = json.header.unit.toUpperCase().replace("_", " ");
							$("#obat_masuk_unit").val(unit);
							$("#dobat_masuk_list").children("tr").remove();
							OBAT_MASUK_ROW_ID = 0;
							for(var i = 0; i < json.detail.length; i++) {
								var dobat_masuk_id = json.detail[i].id;
								var dobat_masuk_id_obat_masuk = json.detail[i].id_obat_keluar;
								var dobat_masuk_id_obat = json.detail[i].id_obat;
								var dobat_masuk_nama_obat = json.detail[i].nama_obat;
								var dobat_masuk_nama_jenis_obat = json.detail[i].nama_jenis_obat;
								var dobat_masuk_label = json.detail[i].label;
								var dobat_masuk_jumlah = json.detail[i].jumlah;
								var dobat_masuk_satuan = json.detail[i].satuan;
								var dobat_masuk_konversi = json.detail[i].konversi;
								var dobat_masuk_satuan_konversi = json.detail[i].satuan_konversi;
								$("tbody#dobat_masuk_list").append(
									"<tr id='data_" + OBAT_MASUK_ROW_ID + "'>" +
										"<td style='display: none;' id='data_" + OBAT_MASUK_ROW_ID + "_id'>" + dobat_masuk_id + "</td>" +
										"<td style='display: none;' id='data_" + OBAT_MASUK_ROW_ID + "_id_obat'>" + dobat_masuk_id_obat + "</td>" +
										"<td style='display: none;' id='data_" + OBAT_MASUK_ROW_ID + "_nama_jenis_obat'>" + dobat_masuk_nama_jenis_obat + "</td>" +
										"<td style='display: none;' id='data_" + OBAT_MASUK_ROW_ID + "_label'>" + dobat_masuk_label + "</td>" +
										"<td style='display: none;' id='data_" + OBAT_MASUK_ROW_ID + "_jumlah'>" + dobat_masuk_jumlah + "</td>" +
										"<td style='display: none;' id='data_" + OBAT_MASUK_ROW_ID + "_satuan'>" + dobat_masuk_satuan + "</td>" +
										"<td style='display: none;' id='data_" + OBAT_MASUK_ROW_ID + "_konversi'>" + dobat_masuk_konversi + "</td>" +
										"<td style='display: none;' id='data_" + OBAT_MASUK_ROW_ID + "_satuan_konversi'>" + dobat_masuk_satuan_konversi + "</td>" +
										"<td id='data_" + OBAT_MASUK_ROW_ID + "_nama_obat'>" + dobat_masuk_nama_obat + "</td>" +
										"<td id='data_" + OBAT_MASUK_ROW_ID + "_f_jumlah'>" + dobat_masuk_jumlah + " " + dobat_masuk_satuan + "</td>" +
										"<td id='data_" + OBAT_MASUK_ROW_ID + "_keterangan'>1 " + dobat_masuk_satuan + " = " + dobat_masuk_konversi + " " + dobat_masuk_satuan_konversi + "</td>" +
										"<td></td>" +
									"</tr>"
								);
								OBAT_MASUK_ROW_ID++;
							}
							$("#obat_masuk_accept").show();
							$("#obat_masuk_return").show();
							$("#obat_masuk_ok").hide();
							$("#obat_masuk_add_form").smodal("show");
						}
					);
				};
				ObatMasukAction.prototype.accept = function() {
					$("#obat_masuk_add_form").smodal("hide");
					showLoading();
					var self = this;
					var data = this.getRegulerData();
					data['command'] = "save";
					data['push_command'] = "update";
					data['id'] = $("#obat_masuk_id").val();
					data['status'] = "sudah";
					data['keterangan'] = "";
					$.post(
						"",
						data,
						function(response) {
							var json = getContent(response);
							if (json == null) {
								$("#obat_masuk_add_form").smodal("show");
							} else {
								self.view();
							}
							dismissLoading();
						}
					);
				};
				ObatMasukAction.prototype.retur = function() {
					$("#obat_masuk_add_form").smodal("hide");
					showLoading();
					var self = this;
					var data = this.getRegulerData();
					data['command'] = "save";
					data['push_command'] = "update";
					data['id'] = $("#obat_masuk_id").val();
					data['status'] = "dikembalikan";
					data['keterangan'] = "";
					$.post(
						"",
						data,
						function(response) {
							var json = getContent(response);
							if (json == null) {
								$("#obat_masuk_add_form").smodal("show");
							} else {
								self.view();
							}
							dismissLoading();
						}
					);
				};
				ObatMasukAction.prototype.detail = function(id) {
					var self = this;
					var data = this.getRegulerData();
					data['command'] = "edit";
					data['id'] = id;
					$.post(
						"",
						data,
						function(response) {
							var json = getContent(response);
							if (json == null) return;
							$("#obat_masuk_id").val(json.header.id);
							$("#obat_masuk_tanggal").val(json.header.tanggal);
							var unit = json.header.unit.toUpperCase().replace("_", " ");
							$("#obat_masuk_unit").val(unit);
							$("#dobat_masuk_list").children("tr").remove();
							OBAT_MASUK_ROW_ID = 0;
							for(var i = 0; i < json.detail.length; i++) {
								var dobat_masuk_id = json.detail[i].id;
								var dobat_masuk_id_obat_masuk = json.detail[i].id_obat_keluar;
								var dobat_masuk_id_obat = json.detail[i].id_obat;
								var dobat_masuk_nama_obat = json.detail[i].nama_obat;
								var dobat_masuk_nama_jenis_obat = json.detail[i].nama_jenis_obat;
								var dobat_masuk_label = json.detail[i].label;
								var dobat_masuk_jumlah = json.detail[i].jumlah;
								var dobat_masuk_satuan = json.detail[i].satuan;
								var dobat_masuk_konversi = json.detail[i].konversi;
								var dobat_masuk_satuan_konversi = json.detail[i].satuan_konversi;
								$("tbody#dobat_masuk_list").append(
									"<tr id='data_" + OBAT_MASUK_ROW_ID + "'>" +
										"<td style='display: none;' id='data_" + OBAT_MASUK_ROW_ID + "_id'>" + dobat_masuk_id + "</td>" +
										"<td style='display: none;' id='data_" + OBAT_MASUK_ROW_ID + "_id_obat'>" + dobat_masuk_id_obat + "</td>" +
										"<td style='display: none;' id='data_" + OBAT_MASUK_ROW_ID + "_nama_jenis_obat'>" + dobat_masuk_nama_jenis_obat + "</td>" +
										"<td style='display: none;' id='data_" + OBAT_MASUK_ROW_ID + "_label'>" + dobat_masuk_label + "</td>" +
										"<td style='display: none;' id='data_" + OBAT_MASUK_ROW_ID + "_jumlah'>" + dobat_masuk_jumlah + "</td>" +
										"<td style='display: none;' id='data_" + OBAT_MASUK_ROW_ID + "_satuan'>" + dobat_masuk_satuan + "</td>" +
										"<td style='display: none;' id='data_" + OBAT_MASUK_ROW_ID + "_konversi'>" + dobat_masuk_konversi + "</td>" +
										"<td style='display: none;' id='data_" + OBAT_MASUK_ROW_ID + "_satuan_konversi'>" + dobat_masuk_satuan_konversi + "</td>" +
										"<td id='data_" + OBAT_MASUK_ROW_ID + "_nama_obat'>" + dobat_masuk_nama_obat + "</td>" +
										"<td id='data_" + OBAT_MASUK_ROW_ID + "_f_jumlah'>" + dobat_masuk_jumlah + " " + dobat_masuk_satuan + "</td>" +
										"<td id='data_" + OBAT_MASUK_ROW_ID + "_keterangan'>1 " + dobat_masuk_satuan + " = " + dobat_masuk_konversi + " " + dobat_masuk_satuan_konversi + "</td>" +
										"<td></td>" +
									"</tr>"
								);
								OBAT_MASUK_ROW_ID++;
							}
							$("#obat_masuk_accept").hide();
							$("#obat_masuk_return").hide();
							$("#obat_masuk_ok").show();
							$("#obat_masuk_add_form").smodal("show");
						}
					);
				};
				
				var obat_masuk;
				var OBAT_MASUK_ROW_ID;
				var OBAT_MASUK_PNAME="<?php echo $this->proto_name; ?>";
				var OBAT_MASUK_PSLUG="<?php echo $this->proto_slug; ?>";
				var OBAT_MASUK_PIMPL="<?php echo $this->proto_implement; ?>";
				var OBAT_MASUK_ENTITY="<?php echo $this->page; ?>";
				$(document).ready(function() {
					$('[data-toggle="popover"]').popover({
						trigger: 'hover',
						'placement': 'top'
					});
					var obat_masuk_columns = new Array("id", "tanggal", "unit", "status", "keterangan");
					obat_masuk = new ObatMasukAction("obat_masuk",OBAT_MASUK_ENTITY,"obat_masuk",	obat_masuk_columns);
					obat_masuk.setPrototipe(OBAT_MASUK_PNAME,OBAT_MASUK_PSLUG,OBAT_MASUK_PIMPL);
					obat_masuk.view();
				});
			</script>		
			<?php 
		}	
	}
?>