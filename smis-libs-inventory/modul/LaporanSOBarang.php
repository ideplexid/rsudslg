<?php
	require_once("smis-base/smis-include-service-consumer.php");

	class LaporanSOBarang extends ModulTemplate {		
		private $db;
		private $name;
		private $tbl_barang_masuk;
		private $tbl_stok_barang;
		private $tbl_penggunaan_barang;
		private $tbl_retur_barang;
		private $tbl_penyesuaian_stok_barang;
		private $page;
		private $action;
		private $so_table;
		private $so_form;
		private $so_loading_modal;
		private $proto_name;
		private $proto_implement;
		private $proto_slug;
		
		public function __construct($db, $name_entity, $tbl_barang_masuk, $tbl_stok_barang, $tbl_penggunaan_barang, $tbl_retur_barang, $tbl_penyesuaian_stok_barang, $page){
			$this->db = $db;
			$this->name = $name_entity;
			$this->tbl_barang_masuk = $tbl_barang_masuk;
			$this->tbl_stok_barang = $tbl_stok_barang;
			$this->tbl_penggunaan_barang = $tbl_penggunaan_barang;
			$this->tbl_retur_barang = $tbl_retur_barang;
			$this->tbl_penyesuaian_stok_barang = $tbl_penyesuaian_stok_barang;
			$this->page = $page;

			$this->initializeForm();
			$this->initailizeTable();
			$this->initializeLoadingModal();
		}
		
		protected function initializeForm() {
			$this->so_form = new Form("", "", $this->name . " : Stok Opname");
			$tanggal_from_text = new Text("laporan_so_barang_tanggal_from", "laporan_so_barang_tanggal_from", date("Y-m-") . "01");
			$tanggal_from_text->setClass("mydate");
			$tanggal_from_text->setAtribute("data-date-format='yyyy-mm-dd'");
			$this->so_form->addElement("Waktu Awal", $tanggal_from_text);
			$tanggal_to_text = new Text("laporan_so_barang_tanggal_to", "laporan_so_barang_tanggal_to", date("Y-m-d"));
			$tanggal_to_text->setClass("mydate");
			$tanggal_to_text->setAtribute("data-date-format='yyyy-mm-dd'");
			$this->so_form->addElement("Waktu Akhir", $tanggal_to_text);
			$jenis_filter_option = new OptionBuilder();
			$jenis_filter_option->add("SEMUA", "semua", "1");
			$jenis_filter_option->add("PER BARANG", "per_barang");
			$jenis_filter_option->add("PER JENIS", "per_jenis");
			$jenis_filter_select = new Select("laporan_so_barang_jenis_filter", "laporan_so_barang_jenis_filter", $jenis_filter_option->getContent());
			$this->so_form->addElement("Jenis Filter", $jenis_filter_select);
			$kode_jenis_barang_hidden = new Hidden("laporan_so_barang_kode_jenis_barang", "laporan_so_barang_kode_jenis_barang", "");
			$this->so_form->addElement("", $kode_jenis_barang_hidden);
			$id_barang_hidden = new Hidden("laporan_so_barang_id_barang", "laporan_so_barang_id_barang", "");
			$this->so_form->addElement("", $id_barang_hidden);
			$nama_barang_text = new Text("laporan_so_barang_nama_barang", "laporan_so_barang_nama_barang", "");
			$nama_barang_text->setAtribute("disabled='disabled'");
			$nama_barang_text->setClass("smis-one-option-input");
			$browse_button = new Button("", "", "Pilih");
			$browse_button->setClass("btn-info");
			$browse_button->setIcon("fa fa-list");
			$browse_button->setIsButton(Button::$ICONIC);
			$browse_button->setAction("laporan_so_barang_barang.chooser('laporan_so_barang_barang', 'laporan_so_barang_barang_button', 'laporan_so_barang_barang', laporan_so_barang_barang)");
			$input_group = new InputGroup("");
			$input_group->addComponent($nama_barang_text);
			$input_group->addComponent($browse_button);
			$this->so_form->addElement("Barang", $input_group);
			$nama_jenis_barang_text = new Text("laporan_so_barang_nama_jenis_barang", "laporan_so_barang_nama_jenis_barang", "");
			$nama_jenis_barang_text->setAtribute("disabled='disabled'");
			$nama_jenis_barang_text->setClass("smis-one-option-input");
			$browse_button = new Button("", "", "Pilih");
			$browse_button->setClass("btn-info");
			$browse_button->setIcon("fa fa-list");
			$browse_button->setIsButton(Button::$ICONIC);
			$browse_button->setAction("laporan_so_barang_jenis_barang.chooser('laporan_so_barang_jenis_barang', 'laporan_so_barang_jenis_barang_button', 'laporan_so_barang_jenis_barang', laporan_so_barang_jenis_barang)");
			$input_group = new InputGroup("");
			$input_group->addComponent($nama_jenis_barang_text);
			$input_group->addComponent($browse_button);
			$this->so_form->addElement("Jenis Barang", $input_group);
			$urutan_option = new OptionBuilder();
			$urutan_option->addSingle("KODE BARANG", "1");
			$urutan_option->addSingle("NAMA BARANG");
			$urutan_select = new Select("laporan_so_barang_urutan", "laporan_so_barang_urutan", $urutan_option->getContent());
			$this->so_form->addElement("Urutan", $urutan_select);
			$show_button = new Button("", "", "Tampilkan");
			$show_button->setClass("btn-primary");
			$show_button->setIcon("icon-white icon-repeat");
			$show_button->setIsButton(Button::$ICONIC);
			$show_button->setAction("laporan_so_barang.view()");
			$print_button = new Button("", "", "Unduh");
			$print_button->setClass("btn-inverse");
			$print_button->setIcon("fa fa-download");
			$print_button->setIsButton(Button::$ICONIC);
			$print_button->setAtribute("id='laporan_so_barang_export_button'");
			$btn_group = new ButtonGroup("noprint");
			$btn_group->addButton($show_button);
			$btn_group->addButton($print_button);
			$this->so_form->addElement("", $btn_group);
		}

		protected function initailizeTable() {
			$this->so_table = new Table(
				array("No.", "ID Barang", "Kode Barang", "Nama Barang", "Jenis Barang", "Stok Awal", "Penyesuaian", "Penerimaan", "Pengeluaran", "Stok Akhir"), ""
			);
			$this->so_table->setName("laporan_so_barang");
			$this->so_table->setAction(false);
			$this->so_table->setFooterVisible(false);
		}

		protected function initializeLoadingModal() {
			$loading_bar = new LoadingBar("laporan_so_barang_loading_bar", "");
			$button = new Button("", "", "Batal");
			$button->addClass("btn-primary");
			$button->setIsButton(Button::$ICONIC_TEXT);
			$button->setIcon("fa fa-close");
			$button->setAction("laporan_so_barang.cancel()");
			$this->so_loading_modal = new Modal("laporan_so_barang_loading_modal", "", "Proses..");
			$this->so_loading_modal->addHtml($loading_bar->getHtml(), "after");
			$this->so_loading_modal->addFooter($button);
		}

		public function setPrototype($pname,$pslug,$pimplement){
			$this->proto_name = $pname;
			$this->proto_implement = $pimplement;
			$this->proto_slug = $pslug;
		}
		
		public function superCommand($super_command){
			//chooser nama barang:
			$barang_table = new Table(
				array("ID", "Kode", "Nama Barang", "Jenis Barang"),
				"",
				null,
				true
			);
			$barang_table->setName("laporan_so_barang_barang");
			$barang_table->setModel(Table::$SELECT);
			$barang_adapter = new SimpleAdapter();
			$barang_adapter->add("ID", "id");
			$barang_adapter->add("Kode", "kode");
			$barang_adapter->add("Nama Barang", "nama");
			$barang_adapter->add("Jenis Barang", "nama_jenis_barang");
			$barang_dbtable = new DBTable($this->db, "smis_pr_barang");
			$barang_dbtable->addCustomKriteria(" medis ", " = 1 ");
			$barang_dbresponder = new DBResponder(
				$barang_dbtable,
				$barang_table,
				$barang_adapter
			);

			//chooser jenis barang:
			$jenis_barang_table = new Table(
				array("No.", "Kode", "Jenis Barang"),
				"",
				null,
				true
			);
			$jenis_barang_table->setName("laporan_so_barang_jenis_barang");
			$jenis_barang_table->setModel(Table::$SELECT);
			$jenis_barang_adapter = new SimpleAdapter(true, "No.");
			$jenis_barang_adapter->add("Kode", "kode");
			$jenis_barang_adapter->add("Jenis Barang", "nama");
			$jenis_barang_dbtable = new DBTable($this->db, "smis_pr_jenis_barang");
			$jenis_barang_dbtable->addCustomKriteria(" medis ", " = 1 ");
			$jenis_barang_dbresponder = new DBResponder(
				$jenis_barang_dbtable,
				$jenis_barang_table,
				$jenis_barang_adapter
			);
			
			$super_command = new SuperCommand();
			$super_command->addResponder("laporan_so_barang_barang", $barang_dbresponder);
			$super_command->addResponder("laporan_so_barang_jenis_barang", $jenis_barang_dbresponder);
			$init = $super_command->initialize();
			if ($init != null) {
				echo $init;
				return;
			}
		}

		private function getCurrentStock($id_barang, $satuan, $konversi, $satuan_konversi) {
			$dbtable = new DBTable($this->db, $this->tbl_stok_barang);
			$row = $dbtable->get_row("
				SELECT SUM(a.sisa) AS 'jumlah'
				FROM " . $this->tbl_stok_barang . " a LEFT JOIN " . $this->tbl_barang_masuk . " b ON a.id_barang_masuk = b.id
				WHERE a.prop NOT LIKE 'del' AND b.prop NOT LIKE 'del' AND b.status = 'sudah' AND a.id_barang = '" . $id_barang . "' AND a.satuan = '" . $satuan . "' AND a.konversi = '" . $konversi . "' AND a.satuan_konversi = '" . $satuan_konversi . "'
			");
			return $row->jumlah;
		}

		private function getStockIn($id_barang, $satuan, $konversi, $satuan_konversi, $tanggal_from, $tanggal_to) {
			$dbtable = new DBTable($this->db, $this->tbl_barang_masuk);
			$jumlah = 0;
			// penerimaan mutasi:
			$row = $dbtable->get_row("
				SELECT SUM(a.jumlah) AS 'jumlah'
				FROM " . $this->tbl_stok_barang . " a LEFT JOIN " . $this->tbl_barang_masuk . " b ON a.id_barang_masuk = b.id
				WHERE a.prop NOT LIKE 'del' AND b.prop NOT LIKE 'del'  AND b.status = 'sudah' AND a.id_barang = '" . $id_barang . "' AND a.satuan = '" . $satuan . "' AND a.konversi = '" . $konversi . "' AND a.satuan_konversi = '" . $satuan_konversi . "'  AND b.tanggal >= '" . $tanggal_from . "' AND b.tanggal <= '" . $tanggal_to . "'
			");
			$jumlah += $row->jumlah;
			return $jumlah;
		}

		private function getStockOut($id_barang, $satuan, $konversi, $satuan_konversi, $tanggal_from, $tanggal_to) {
			$dbtable = new DBTable($this->db, $this->tbl_penggunaan_barang);
			$jumlah = 0;
			// penggunaan :
			$row = $dbtable->get_row("
				SELECT SUM(jumlah) AS 'jumlah'
				FROM " . $this->tbl_penggunaan_barang . " a LEFT JOIN " . $this->tbl_stok_barang . " b ON a.id_stok_barang = b.id
				WHERE b.id_barang = '" . $id_barang . "' AND a.prop NOT LIKE 'del' AND a.tanggal >= '" . $tanggal_from . "' AND a.tanggal <= '" . $tanggal_to . "'
			");
			$jumlah += $row->jumlah;
			// retur ke gudang :
			$row = $dbtable->get_row("
				SELECT SUM(jumlah) AS 'jumlah'
				FROM " . $this->tbl_retur_barang . " a LEFT JOIN " . $this->tbl_stok_barang . " b ON a.id_stok_barang = b.id
				WHERE b.id_barang = '" . $id_barang . "' AND a.prop NOT LIKE 'del' AND a.tanggal >= '" . $tanggal_from . "' AND a.tanggal <= '" . $tanggal_to . "'
			");
			$jumlah += $row->jumlah;
			return $jumlah;
		}

		private function getPenyesuaianStokPositif($id_barang, $satuan, $konversi, $satuan_konversi, $tanggal_from, $tanggal_to) {
			$dbtable = new DBTable($this->db, $this->tbl_penyesuaian_stok_barang);
			$jumlah = 0;
			$row = $dbtable->get_row("
				SELECT SUM(a.jumlah_baru - a.jumlah_lama) AS 'jumlah'
				FROM " . $this->tbl_penyesuaian_stok_barang . " a LEFT JOIN " . $this->tbl_stok_barang . " b ON a.id_stok_barang = b.id
				WHERE a.prop NOT LIKE 'del' AND b.id_barang = '" . $id_barang . "' AND b.satuan = '" . $satuan . "' AND b.konversi = '" . $konversi . "' AND b.satuan_konversi = '" . $satuan_konversi . "' AND a.tanggal >= '" . $tanggal_from . "' AND a.tanggal <= '" . $tanggal_to . "' AND a.jumlah_baru >= a.jumlah_lama
			");
			$jumlah += $row->jumlah;
			return $jumlah;
		}

		private function getPenyesuaianStokNegatif($id_barang, $satuan, $konversi, $satuan_konversi, $tanggal_from, $tanggal_to) {
			$dbtable = new DBTable($this->db, $this->tbl_penyesuaian_stok_barang);
			$jumlah = 0;
			$row = $dbtable->get_row("
				SELECT SUM(a.jumlah_baru - a.jumlah_lama) AS 'jumlah'
				FROM " . $this->tbl_penyesuaian_stok_barang . " a LEFT JOIN " . $this->tbl_stok_barang . " b ON a.id_stok_barang = b.id
				WHERE a.prop NOT LIKE 'del' AND b.id_barang = '" . $id_barang . "' AND b.satuan = '" . $satuan . "' AND b.konversi = '" . $konversi . "' AND b.satuan_konversi = '" . $satuan_konversi . "' AND a.tanggal >= '" . $tanggal_from . "' AND a.tanggal <= '" . $tanggal_to . "' AND a.jumlah_baru < a.jumlah_lama
			");
			$jumlah += $row->jumlah;
			return $jumlah;
		}

		public function command($command){
			if ($_POST['command'] == "get_jumlah_barang") {
				$jenis_filter = $_POST['jenis_filter'];
				$params = array();
				$params['order_by'] = $_POST['urutan'];
				if ($jenis_filter == "per_barang")
					$params['filter_id_barang'] = $_POST['id_barang'];
				else
					$params['filter_id_barang'] = "%%";
				if ($jenis_filter == "per_jenis")
					$params['filter_kode_jenis_barang'] = $_POST['kode_jenis_barang'];
				else
					$params['filter_kode_jenis_barang'] = "%%";
				$consumer_service = new ServiceConsumer(
					$this->db,
					"get_jumlah_barang_msrs",
					$params,
					"perencanaan"
				);
				$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
				$content = $consumer_service->execute()->getContent();
				$jumlah = 0;
				if ($content != null)
					$jumlah = $content[0];
				$data = array();
				$data['jumlah'] = $jumlah;
				echo json_encode($data);
			} else if ($_POST['command'] == "get_info_barang") {
				$tanggal_from = $_POST['tanggal_from'];
				$tanggal_to = $_POST['tanggal_to'];
				$jenis_filter = $_POST['jenis_filter'];
				$num = $_POST['num'];
				$params = array();
				$params['num'] = $_POST['num'];
				$params['order_by'] = $_POST['urutan'];
				if ($jenis_filter == "per_barang")
					$params['filter_id_barang'] = $_POST['id_barang'];
				else
					$params['filter_id_barang'] = "%%";
				if ($jenis_filter == "per_jenis")
					$params['filter_kode_jenis_barang'] = $_POST['kode_jenis_barang'];
				else
					$params['filter_kode_jenis_barang'] = "%%";
				$consumer_service = new ServiceConsumer(
					$this->db,
					"get_barang_info_msrs",
					$params,
					"perencanaan"
				);
				$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
				$content = $consumer_service->execute()->getContent();
				$id_barang = $content[0];
				$kode_barang = $content[1];
				$nama_barang = $content[2];
				$nama_jenis_barang = $content[3];
				$satuan = $content[4];
				$satuan_konversi = $content[5];

				// Mendapatkan Saldo Akhir:
				$saldo_sekarang = $this->getCurrentStock($id_barang, $satuan, 1, $satuan_konversi);
				$jumlah_masuk = $this->getStockIn($id_barang, $satuan, 1, $satuan_konversi, date("Y-m-d", strtotime($tanggal_to . " +1 day")), date("Y-m-d")) + $this->getPenyesuaianStokPositif($id_barang, $satuan, 1, $satuan_konversi, date("Y-m-d", strtotime($tanggal_to . "+1 day")), date("Y-m-d"));
				$jumlah_keluar = $this->getStockOut($db, $id_barang, $satuan, 1, $satuan_konversi,date("Y-m-d", strtotime($tanggal_to . " +1 day")), date("Y-m-d")) + $this->getPenyesuaianStokNegatif($id_barang, $satuan, 1, $satuan_konversi, date("Y-m-d", strtotime($tanggal_to . "+1 day")), date("Y-m-d"));
				$saldo_akhir = $saldo_sekarang - $jumlah_masuk + $jumlah_keluar;
				// Mendapatkan Penyesuaian Stok:
				$penyesuaian_stok = $this->getPenyesuaianStokPositif($id_barang, $satuan, 1, $satuan_konversi, $tanggal_from, $tanggal_to) - $this->getPenyesuaianStokNegatif($id_barang, $satuan, 1, $satuan_konversi, $tanggal_from, $tanggal_to);
				// Mendapatkan Jumlah Masuk:
				$jumlah_masuk = $this->getStockIn($id_barang, $satuan, 1, $satuan_konversi, $tanggal_from, $tanggal_to);
				// Mendapatkan Jumlah Keluar:
				$jumlah_keluar = $this->getStockOut($id_barang, $satuan, 1, $satuan_konversi, $tanggal_from, $tanggal_to);
				// Mendapatkan Saldo Awal:
				$saldo_awal = $saldo_akhir - $jumlah_masuk + $jumlah_keluar - $penyesuaian_stok;

				$html = "
					<tr>
						<td id='nomor'></td>
						<td id='id_barang'><small>" .  $id_barang . "</small></td>
						<td id='kode_barang'><small>" .  $kode_barang . "</small></td>
						<td id='nama_barang'><small>" .  $nama_barang . "</small></td>
						<td id='nama_jenis_barang'><small>" .  $nama_jenis_barang . "</small></td>
						<td id='saldo_awal' style='display: none;'><small>" .  $saldo_awal . "</small></td>
						<td id='f_saldo_awal'><small>" .  ArrayAdapter::format("number", $saldo_awal) . "</small></td>
						<td id='penyesuaian_stok' style='display: none;'><small>" .  $penyesuaian_stok . "</small></td>
						<td id='f_penyesuaian_stok'><small>" .  ArrayAdapter::format("number", $penyesuaian_stok) . "</small></td>
						<td id='jumlah_masuk' style='display: none;'><small>" .  $jumlah_masuk . "</small></td>
						<td id='f_jumlah_masuk'><small>" .  ArrayAdapter::format("number", $jumlah_masuk) . "</small></td>
						<td id='jumlah_keluar' style='display: none;'><small>" .  $jumlah_keluar . "</small></td>
						<td id='f_jumlah_keluar'><small>" .  ArrayAdapter::format("number", $jumlah_keluar) . "</small></td>
						<td id='saldo_akhir' style='display: none;'><small>" .  $saldo_akhir . "</small></td>
						<td id='f_saldo_akhir'><small>" .  ArrayAdapter::format("number", $saldo_akhir) . "</small></td>
					</tr>
				";

				$data = array();
				$data['id_barang'] = ArrayAdapter::format("only-digit6", $id_barang);
				$data['kode_barang'] = $kode_barang;
				$data['nama_barang'] = $nama_barang;
				$data['nama_jenis_barang'] = $nama_jenis_barang;
				$data['html'] = $html;
				echo json_encode($data);
			} else if ($_POST['command'] == "export_xls") {
				$tanggal_from = $_POST['tanggal_from'];
				$tanggal_to = $_POST['tanggal_to'];
				require_once("smis-libs-out/php-excel/PHPExcel.php");
				$objPHPExcel = PHPExcel_IOFactory::load("smis-libs-inventory/templates/template_persediaan_barang.xlsx");
				$objPHPExcel->setActiveSheetIndexByName("STOK OPNAME");
				$objWorksheet = $objPHPExcel->getActiveSheet();
				$objWorksheet->setCellValue("B2", ArrayAdapter::format("unslug", $this->name));
				$objWorksheet->setCellValue("B3", "PERIODE : " . ArrayAdapter::format("date d-m-Y", $tanggal_from) . " s/d " . ArrayAdapter::format("date d-m-Y", $tanggal_to));
				$data = json_decode($_POST['d_data']);
				if ($_POST['num_rows'] - 2 > 0)
					$objWorksheet->insertNewRowBefore(8, $_POST['num_rows'] - 2);
				$start_row_num = 7;
				$end_row_num = 7;
				$row_num = $start_row_num;
				foreach ($data as $d) {
					$col_num = 1;
					$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->nomor);
					$col_num++;
					$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->id_barang);
					$col_num++;
					$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->kode_barang);
					$col_num++;
					$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->nama_barang);
					$col_num++;
					$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->saldo_awal);
					$col_num++;
					$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->penyesuaian_stok);
					$col_num++;
					$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->jumlah_masuk);
					$col_num++;
					$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->jumlah_keluar);
					$col_num++;
					$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->saldo_akhir);
					$objWorksheet->getStyle("F" . $row_num)->getNumberFormat()->setFormatCode("#,##0");
					$objWorksheet->getStyle("G" . $row_num)->getNumberFormat()->setFormatCode("#,##0");
					$objWorksheet->getStyle("H" . $row_num)->getNumberFormat()->setFormatCode("#,##0");
					$objWorksheet->getStyle("I" . $row_num)->getNumberFormat()->setFormatCode("#,##0");
					$objWorksheet->getStyle("J" . $row_num)->getNumberFormat()->setFormatCode("#,##0");
					$row_num++;
					$end_row_num++;
					$no++;
				}
				header("Content-type: application/vnd.ms-excel");	
				header("Content-Disposition: attachment; filename=STOK_OPNAME_BARANG_" . ArrayAdapter::format("unslug", $this->name) . "_" . ArrayAdapter::format("date Ymd", $tanggal_from) . "_" . ArrayAdapter::format("date Ymd", $tanggal_to) . ".xlsx");
				$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");
				$objWriter->save("php://output");
			}
			return;
		}
		
		public function phpPreLoad(){
			echo $this->so_loading_modal->getHtml();	
			echo $this->so_form->getHtml();
			echo $this->so_table->getHtml();
			echo "<div id='laporan_so_barang_info'></div>";
			echo addJS("framework/smis/js/table_action.js");
			echo addJS("framework/bootstrap/js/bootstrap-datepicker.js");
			echo addCSS("framework/bootstrap/css/datepicker.css");
			echo addJS("base-js/smis-base-loading.js");
		}
		
		public function jsPreLoad(){
			?>
			<script type="text/javascript">
				function LaporanSOBarangAction(name, page, action, column) {
					this.initialize(name, page, action, column);
				}
				LaporanSOBarangAction.prototype.constructor = LaporanSOBarangAction;
				LaporanSOBarangAction.prototype = new TableAction();
				LaporanSOBarangAction.prototype.getRegulerData = function() {
					var data = TableAction.prototype.getRegulerData.call(this);
					data['tanggal_from'] = $("#laporan_so_barang_tanggal_from").val();
					data['tanggal_to'] = $("#laporan_so_barang_tanggal_to").val();
					data['jenis_filter'] = $("#laporan_so_barang_jenis_filter").val();
					data['id_barang'] = $("#laporan_so_barang_id_barang").val();
					data['nama_barang'] = $("#laporan_so_barang_nama_barang").val();
					data['nama_jenis_barang'] = $("#laporan_so_barang_nama_jenis_barang").val();
					data['kode_jenis_barang'] = $("#laporan_so_barang_kode_jenis_barang").val();
					data['urutan'] = $("#laporan_so_barang_urutan").val();
					return data;
				};
				LaporanSOBarangAction.prototype.view = function() {
					var self = this;
					$("#laporan_so_barang_info").empty();
					$("#laporan_so_barang_loading_bar").sload("true", "Harap ditunggu...", 0);
					$("#laporan_so_barang_loading_modal").smodal("show");
					FINISHED = false;
					var data = this.getRegulerData();
					data['command'] = "get_jumlah_barang";
					$.post(
						"",
						data,
						function(response) {
							var json = JSON.parse(response);
							if (json == null) return;
							$("#laporan_so_barang_list").empty();
							self.fillHtml(0, json.jumlah);
						}
					);
				};
				LaporanSOBarangAction.prototype.fillHtml = function(num, limit) {
					if (FINISHED || num == limit) {
						if (FINISHED == false && num == limit) {
							this.finalize();
						} else {
							$("#laporan_so_barang_loading_modal").smodal("hide");
							$("#laporan_so_barang_info").html(
								"<div class='alert alert-block alert-inverse'>" +
									 "<center><strong>PROSES DIBATALKAN</strong></center>" +
								 "</div>"
							);
							$("#laporan_so_barang_export_button").removeAttr("onclick");
						}
						return;
					}
					var self = this;
					var data = this.getRegulerData();
					data['command'] = "get_info_barang";
					data['num'] = num;
					$.post(
						"",
						data,
						function(response) {
							var json = JSON.parse(response);
							if (json == null) return;
							$("tbody#laporan_so_barang_list").append(
								json.html
							);
							$("#laporan_so_barang_loading_bar").sload("true", json.id_barang + " - " + json.kode_barang + " - " + json.nama_barang + " - " + json.nama_jenis_barang + " (" + (num + 1) + " / " + limit + ")", (num + 1) * 100 / limit - 1);
							self.fillHtml(num + 1, limit);
						}
					);
				};
				LaporanSOBarangAction.prototype.finalize = function() {
					var num_rows = $("tbody#laporan_so_barang_list tr").length;
					for (var i = 0; i < num_rows; i++)
						$("tbody#laporan_so_barang_list tr:eq(" + i + ") td#nomor").html("<small>" + (i + 1) + "</small>");
					$("#laporan_so_barang_loading_modal").smodal("hide");
					$("#laporan_so_barang_info").html(
						"<div class='alert alert-block alert-info'>" +
							 "<center><strong>PROSES SELESAI</strong></center>" +
						 "</div>"
					);
					$("#laporan_so_barang_export_button").removeAttr("onclick");
					$("#laporan_so_barang_export_button").attr("onclick", "laporan_so_barang.export_xls()");
				};
				LaporanSOBarangAction.prototype.cancel = function() {
					FINISHED = true;
				};
				LaporanSOBarangAction.prototype.export_xls = function() {
					showLoading();
					var num_rows = $("#laporan_so_barang_list").children("tr").length;
					var d_data = {};
					for (var i = 0; i < num_rows; i++) {
						var nomor = $("tbody#laporan_so_barang_list tr:eq(" + i + ") td#nomor").text();
						var id_barang = $("tbody#laporan_so_barang_list tr:eq(" + i + ") td#id_barang").text();
						var kode_barang = $("tbody#laporan_so_barang_list tr:eq(" + i + ") td#kode_barang").text();
						var nama_barang = $("tbody#laporan_so_barang_list tr:eq(" + i + ") td#nama_barang").text();
						var nama_jenis_barang = $("tbody#laporan_so_barang_list tr:eq(" + i + ") td#nama_jenis_barang").text();
						var saldo_awal = $("tbody#laporan_so_barang_list tr:eq(" + i + ") td#saldo_awal").text();
						var penyesuaian_stok = $("tbody#laporan_so_barang_list tr:eq(" + i + ") td#penyesuaian_stok").text();
						var jumlah_masuk = $("tbody#laporan_so_barang_list tr:eq(" + i + ") td#jumlah_masuk").text();
						var jumlah_keluar = $("tbody#laporan_so_barang_list tr:eq(" + i + ") td#jumlah_keluar").text();
						var saldo_akhir = $("tbody#laporan_so_barang_list tr:eq(" + i + ") td#saldo_akhir").text();
						d_data[i] = {
							"nomor" 			: nomor,
							"id_barang" 			: id_barang,
							"kode_barang" 		: kode_barang,
							"nama_barang" 		: nama_barang,
							"nama_jenis_barang"	: nama_jenis_barang,
							"saldo_awal" 		: saldo_awal,
							"penyesuaian_stok"	: penyesuaian_stok,
							"jumlah_masuk" 		: jumlah_masuk,
							"jumlah_keluar" 	: jumlah_keluar,
							"saldo_akhir" 		: saldo_akhir
						};
					}
					var data = this.getRegulerData();
					data['command'] = "export_xls";
					data['tanggal_from'] = $("#laporan_so_barang_tanggal_from").val();
					data['tanggal_to'] = $("#laporan_so_barang_tanggal_to").val();
					data['d_data'] = JSON.stringify(d_data);
					data['num_rows'] = num_rows;
					postForm(data);
					dismissLoading();
				};

				function LaporanSOBarangBarangAction(name, page, action, column) {
					this.initialize(name, page, action, column);
				}
				LaporanSOBarangBarangAction.prototype.constructor = LaporanSOBarangBarangAction;
				LaporanSOBarangBarangAction.prototype = new TableAction();
				LaporanSOBarangBarangAction.prototype.selected = function(json) {
					$("#laporan_so_barang_id_barang").val(json.id);
					$("#laporan_so_barang_nama_barang").val(json.nama);
				};

				function LaporanSOBarangJenisBarangAction(name, page, action, column) {
					this.initialize(name, page, action, column);
				}
				LaporanSOBarangJenisBarangAction.prototype.constructor = LaporanSOBarangJenisBarangAction;
				LaporanSOBarangJenisBarangAction.prototype = new TableAction();
				LaporanSOBarangJenisBarangAction.prototype.selected = function(json) {
					$("#laporan_so_barang_kode_jenis_barang").val(json.kode);
					$("#laporan_so_barang_nama_jenis_barang").val(json.nama);
				};
				
				var laporan_so_barang;
				var laporan_so_barang_barang;
				var laporan_so_barang_jenis_barang;
				var FINISHED;
				var LAPORAN_SO_BARANG_PNAME="<?php echo $this->proto_name; ?>";
				var LAPORAN_SO_BARANG_PSLUG="<?php echo $this->proto_slug; ?>";
				var LAPORAN_SO_BARANG_PIMPL="<?php echo $this->proto_implement; ?>";
				var LAPORAN_SO_BARANG_ENTITY="<?php echo $this->page; ?>";
				$(document).ready(function() {
					$('[data-toggle="popover"]').popover({
						trigger: 'hover',
						'placement': 'top'
					});
					laporan_so_barang_barang = new LaporanSOBarangBarangAction(
						"laporan_so_barang_barang",
						LAPORAN_SO_BARANG_ENTITY,
						"laporan_so_barang",
						new Array()
					);
					laporan_so_barang_barang.setPrototipe(LAPORAN_SO_BARANG_PNAME, LAPORAN_SO_BARANG_PSLUG, LAPORAN_SO_BARANG_PIMPL);
					laporan_so_barang_barang.setSuperCommand("laporan_so_barang_barang");
					laporan_so_barang_jenis_barang = new LaporanSOBarangJenisBarangAction(
						"laporan_so_barang_jenis_barang",
						LAPORAN_SO_BARANG_ENTITY,
						"laporan_so_barang",
						new Array()
					);
					laporan_so_barang_jenis_barang.setPrototipe(LAPORAN_SO_BARANG_PNAME, LAPORAN_SO_BARANG_PSLUG, LAPORAN_SO_BARANG_PIMPL);
					laporan_so_barang_jenis_barang.setSuperCommand("laporan_so_barang_jenis_barang");
					laporan_so_barang = new LaporanSOBarangAction(
						"laporan_so_barang",
						LAPORAN_SO_BARANG_ENTITY,
						"laporan_so_barang",	
						new Array()
					);
					laporan_so_barang.setPrototipe(LAPORAN_SO_BARANG_PNAME, LAPORAN_SO_BARANG_PSLUG, LAPORAN_SO_BARANG_PIMPL);
					$("#laporan_so_barang > div.form-container > form > div:nth-child(7)").hide();
					$("#laporan_so_barang > div.form-container > form > div:nth-child(8)").hide();
					$("#laporan_so_barang_jenis_filter").on("change", function() {
						var jenis_filter = $("#laporan_so_barang_jenis_filter").val();
						$("#laporan_so_barang_id_barang").val("");
						$("#laporan_so_barang_nama_barang").val("");
						$("#laporan_so_barang_kode_jenis_barang").val("");
						$("#laporan_so_barang_jenis_barang").val("");
						if (jenis_filter == "semua") {
							$("#laporan_so_barang > div.form-container > form > div:nth-child(7)").hide();
							$("#laporan_so_barang > div.form-container > form > div:nth-child(8)").hide();
						} else if (jenis_filter == "per_barang") {
							$("#laporan_so_barang > div.form-container > form > div:nth-child(7)").show();
							$("#laporan_so_barang > div.form-container > form > div:nth-child(8)").hide();
						} else if (jenis_filter == "per_jenis") {
							$("#laporan_so_barang > div.form-container > form > div:nth-child(7)").hide();
							$("#laporan_so_barang > div.form-container > form > div:nth-child(8)").show();
						}
					});
					$("tbody#laporan_so_barang_list").append(
						"<tr>" +
							"<td colspan='10'><strong><center><small>DATA PERSEDIAAN BARANG BELUM DIPROSES</small></center></strong></td>" +
						"</tr>"
					);
					$(document).keyup(function(e) {
						if (e.which == 27) {
							FINISHED = true;
						}
					})
				});
			</script>		
			<?php 
		}	
	}
?>