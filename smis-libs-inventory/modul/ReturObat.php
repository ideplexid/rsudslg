<?php 
	require_once("smis-base/smis-include-service-consumer.php");
	require_once 'smis-framework/smis/template/ModulTemplate.php';
	require_once 'smis-libs-inventory/adapter/ObatAdapter.php';
	require_once 'smis-libs-inventory/service_consumer/PushReturObatServiceConsumer.php';
	require_once 'smis-libs-inventory/adapter/ReturObatAdapter.php';
	require_once 'smis-libs-inventory/responder/ReturObatDBResponder.php';
	require_once 'smis-libs-inventory/table/ReturObatTable.php';

	class ReturObat extends ModulTemplate {
		private $db;
		private $name;
		private $tbl_retur_obat;
		private $tbl_stok_obat;
		private $tbl_riwayat_stok;
		private $tbl_obat_masuk;
		private $page;
		private $action;
		private $retur_obat_table;
		private $proto_name;
		private $proto_implement;
		private $proto_slug;
		private $gudang_asal;
		
		public function __construct($db, $name_entity, $tbl_retur, $tbl_stok, $tbl_riwayat_stok, $tbl_masuk, $page, $gudang_asal){
			$this->db=$db;
			$this->name=$name_entity;
			$this->tbl_retur_obat=$tbl_retur;
			$this->tbl_stok_obat=$tbl_stok;
			$this->tbl_riwayat_stok=$tbl_riwayat_stok;
			$this->tbl_obat_masuk=$tbl_masuk;
			$this->page=$page;
			$header=array("Nomor", "Tanggal Retur", "Obat", "Jenis", "Jumlah", "Status");
			$this->retur_obat_table = new ReturObatTable($header, $this->name . " : Retur Obat");
			$this->retur_obat_table->setName("retur_obat");
			$this->proto_name="";
			$this->proto_implement="";
			$this->proto_slug="";
			$this->gudang_asal = $gudang_asal;
		}
		
		public function setPrototype($pname,$pslug,$pimplement){
			$this->proto_name=$pname;
			$this->proto_implement=$pimplement;
			$this->proto_slug=$pslug;
		}
		
		public function command($command){
			$retur_obat_adapter = new ReturObatAdapter();
			$columns = array("id", "id_stok_obat", "tanggal", "jumlah", "keterangan", "status");
			$retur_obat_dbtable = new DBTable($this->db,"".$this->tbl_retur_obat."",$columns);
			$filter = "";
			if (isset($_POST['kriteria'])) {
				$filter = " AND (".$this->tbl_stok_obat.".nama_obat LIKE '%" . $_POST['kriteria'] . "%' OR ".$this->tbl_stok_obat.".nama_jenis_obat LIKE '%" . $_POST['kriteria'] . "%')";
			}
			$query_value = "
					SELECT ".$this->tbl_retur_obat.".*, ".$this->tbl_stok_obat.".nama_obat, ".$this->tbl_stok_obat.".nama_jenis_obat, ".$this->tbl_stok_obat.".satuan
					FROM ".$this->tbl_retur_obat." LEFT JOIN ".$this->tbl_stok_obat." ON ".$this->tbl_retur_obat.".id_stok_obat = ".$this->tbl_stok_obat.".id
					WHERE ".$this->tbl_retur_obat.".prop NOT LIKE 'del' " . $filter . "
				";
			$query_count = "
					SELECT COUNT(*)
					FROM (
						SELECT ".$this->tbl_retur_obat.".*, ".$this->tbl_stok_obat.".nama_obat, ".$this->tbl_stok_obat.".nama_jenis_obat, ".$this->tbl_stok_obat.".satuan
						FROM ".$this->tbl_retur_obat." LEFT JOIN ".$this->tbl_stok_obat." ON ".$this->tbl_retur_obat.".id_stok_obat = ".$this->tbl_stok_obat.".id
						WHERE ".$this->tbl_retur_obat.".prop NOT LIKE 'del' " . $filter . "
					) v_retur
				";
			$retur_obat_dbtable->setPreferredQuery(true, $query_value, $query_count);
			$retur_obat_dbresponder = new ReturObatDBResponder(	$retur_obat_dbtable,$this->retur_obat_table,$retur_obat_adapter,$this->tbl_retur_obat,$this->tbl_stok_obat, $this->tbl_riwayat_stok);
			$data = $retur_obat_dbresponder->command($_POST['command']);
			//push_retur_obat:
			if (isset($_POST['push_command'])) {
				$retur_obat_dbtable = new DBTable($this->db, "".$this->tbl_retur_obat."");
				$retur_obat_row = $retur_obat_dbtable->get_row("
					SELECT ".$this->tbl_retur_obat.".*, ".$this->tbl_stok_obat.".id_obat, ".$this->tbl_stok_obat.".kode_obat, ".$this->tbl_stok_obat.".f_id, ".$this->tbl_stok_obat.".nama_obat, ".$this->tbl_stok_obat.".nama_jenis_obat, ".$this->tbl_stok_obat.".label, ".$this->tbl_stok_obat.".id_vendor, ".$this->tbl_stok_obat.".nama_vendor, ".$this->tbl_stok_obat.".satuan, ".$this->tbl_stok_obat.".konversi, ".$this->tbl_stok_obat.".satuan_konversi, ".$this->tbl_stok_obat.".hna, ".$this->tbl_stok_obat.".produsen, ".$this->tbl_stok_obat.".tanggal_exp, ".$this->tbl_stok_obat.".no_batch, ".$this->tbl_stok_obat.".turunan
					FROM ".$this->tbl_retur_obat." LEFT JOIN ".$this->tbl_stok_obat." ON ".$this->tbl_retur_obat.".id_stok_obat = ".$this->tbl_stok_obat.".id
					WHERE ".$this->tbl_retur_obat.".id = '" . $data['content']['id'] . "'
				");
				$command = "push_" . $_POST['push_command'];
				$push_retur_obat_service_consumer = new PushReturObatServiceConsumer($this->db, $retur_obat_row->id, $retur_obat_row->tanggal, $retur_obat_row->id_obat, $retur_obat_row->kode_obat, $retur_obat_row->f_id, $retur_obat_row->nama_obat, $retur_obat_row->nama_jenis_obat, $retur_obat_row->label, $retur_obat_row->id_vendor, $retur_obat_row->nama_vendor, $retur_obat_row->jumlah, $retur_obat_row->satuan, $retur_obat_row->konversi, $retur_obat_row->satuan_konversi, $retur_obat_row->hna, $retur_obat_row->produsen, $retur_obat_row->tanggal_exp, $retur_obat_row->no_batch, $retur_obat_row->ppn, $retur_obat_row->turunan, $retur_obat_row->keterangan, $retur_obat_row->status, $command, $this->page);
				$push_retur_obat_service_consumer->execute();
			}
			echo json_encode($data);
			return;
		}
		
		public function superCommand($super_command){
			$header=array("Nomor", "Obat", "Jenis", "Stok", "Tanggal Exp.");
			$obat_table = new Table($header);
			$obat_table->setName("retur_obat_obat");
			$obat_table->setModel(Table::$SELECT);
			
			$obat_adapter	= new ObatAdapter();
			$obat_dbtable = new DBTable($this->db, $this->tbl_stok_obat);
			$filter = "";
			if (isset($_POST['kriteria'])) {
				$filter = " AND (" . $this->tbl_stok_obat . ".nama_obat LIKE '%" . $_POST['kriteria'] . "%' OR ".$this->tbl_stok_obat.".nama_jenis_obat LIKE '%" . $_POST['kriteria'] . "%') ";
			}
			$query_value = "
				SELECT " . $this->tbl_stok_obat . ".*
				FROM " . $this->tbl_stok_obat . " LEFT JOIN " . $this->tbl_obat_masuk . " ON " . $this->tbl_stok_obat . ".id_obat_masuk = " . $this->tbl_obat_masuk . ".id
				WHERE " . $this->tbl_obat_masuk . ".status = 'sudah' AND " . $this->tbl_obat_masuk . ".unit = '" . $this->gudang_asal . "' " . $filter . "
			";
			$query_count = "
				SELECT COUNT(*)
				FROM " . $this->tbl_stok_obat . " LEFT JOIN " . $this->tbl_obat_masuk . " ON " . $this->tbl_stok_obat . ".id_obat_masuk = " . $this->tbl_obat_masuk . ".id
				WHERE " . $this->tbl_obat_masuk . ".status = 'sudah' AND " . $this->tbl_obat_masuk . ".unit = '" . $this->gudang_asal . "' " . $filter . "
			";
			$obat_dbtable->setPreferredQuery(true, $query_value, $query_count);
			$obat_dbresponder = new DBResponder($obat_dbtable,$obat_table,$obat_adapter);
			$super_command = new SuperCommand();
			$super_command->addResponder("retur_obat_obat", $obat_dbresponder);
			$init = $super_command->initialize();
			if ($init != null) {
				echo $init;
				return;
			}
		}
		
		public function phpPreLoad(){
			$this->retur_obat_table->addModal("id", "hidden", "", "");
			$this->retur_obat_table->addModal("id_stok_obat", "hidden", "", "");
			$this->retur_obat_table->addModal("tanggal", "date", "Tanggal", "");
			$this->retur_obat_table->addModal("nama_obat", "chooser-retur_obat-retur_obat_obat", "Obat", "");
			$this->retur_obat_table->addModal("sisa", "hidden", "", "");
			$this->retur_obat_table->addModal("f_sisa", "text", "Stok", "","n","free",true);
			$this->retur_obat_table->addModal("jumlah", "text", "Jumlah", "");
			$this->retur_obat_table->addModal("jumlah_lama", "hidden", "", "");
			$this->retur_obat_table->addModal("satuan", "text", "Satuan", "","n","free",true);
			$this->retur_obat_table->addModal("keterangan", "textarea", "Keterangan", "");
			$retur_obat_modal=$this->retur_obat_table->getModal();
			$retur_obat_modal->setTitle("Data Retur Obat");
			
			$retur_obat_button = new Button("", "", "OK");
			$retur_obat_button->setClass("btn-success");
			$retur_obat_button->setAction("$($(this).data('target')).smodal('hide')");
			$retur_obat_button->setAtribute("id='retur_obat_ok'");
			$retur_obat_modal->addFooter($retur_obat_button);
				
			echo $retur_obat_modal->getHtml();
			echo $this->retur_obat_table->getHtml();
			echo addJS("framework/smis/js/table_action.js");
			echo addJS("framework/bootstrap/js/bootstrap-datepicker.js");
			echo addCSS("framework/bootstrap/css/datepicker.css");	
		}
		
		public function jsPreLoad(){
			?>				
			<script type="text/javascript">
				function ReturObatAction(name, page, action, column) {
					this.initialize(name, page, action, column);
				}
				ReturObatAction.prototype.constructor = ReturObatAction;
				ReturObatAction.prototype = new TableAction();
				ReturObatAction.prototype.show_add_form = function() {
					var today = new Date();
					$("#retur_obat_id").val("");
					$("#retur_obat_tanggal").val(today.getFullYear() + "-" + (Number(today.getMonth())+1) + "-" + today.getDate());
					$("#retur_obat_tanggal").removeAttr("disabled");
					$("#retur_obat_id_stok_obat").val("");
					$("#retur_obat_nama_obat").val("");
					$("#retur_obat_nama_obat").removeAttr("disabled");
					$("#retur_obat_nama_obat").attr("disabled", "disabled");
					$("#retur_obat_chooser_nama_obat").removeAttr("onclick");
					$("#retur_obat_chooser_nama_obat").attr("onclick", "retur_obat.chooser('retur_obat','retur_obat_nama_obat','retur_obat_obat',retur_obat_obat)");
					$("#retur_obat_chooser_nama_obat").removeClass("btn-primary");
					$("#retur_obat_chooser_nama_obat").removeClass("btn-inverse");
					$("#retur_obat_chooser_nama_obat").addClass("btn-primary");
					$("#retur_obat_sisa").val(0);
					$("#retur_obat_f_sisa").val("");
					$(".retur_obat_f_sisa").show();
					$("#retur_obat_jumlah").val("");
					$("#retur_obat_jumlah").removeAttr("disabled");
					$("#retur_obat_jumlah_lama").val(0);
					$("#retur_obat_satuan").val("");
					$("#retur_obat_keterangan").val("");
					$("#retur_obat_keterangan").removeAttr("disabled");
					$("#modal_alert_retur_obat_add_form").html("");
					$(".error_field").removeClass("error_field");
					$("#retur_obat_save").show();
					$("#retur_obat_save").removeAttr("onclick");
					$("#retur_obat_save").attr("onclick", "retur_obat.save()");
					$("#retur_obat_ok").hide();
					$("#retur_obat_add_form").smodal("show");
				};
				ReturObatAction.prototype.validate = function() {
					var valid = true;
					var invalid_msg = "";
					var tanggal = $("#retur_obat_tanggal").val();
					var nama_obat = $("#retur_obat_nama_obat").val();
					var jumlah = $("#retur_obat_jumlah").val();
					var jumlah_lama = $("#retur_obat_jumlah_lama").val();
					var sisa = $("#retur_obat_sisa").val();
					var keterangan = $("#retur_obat_keterangan").val();
					$(".error_field").removeClass("error_field");
					if (tanggal == "") {
						valid = false;
						invalid_msg += "<br/><strong>Tanggal</strong> tidak boleh kosong";
						$("#retur_obat_tanggal").addClass("error_field");
					}
					if (nama_obat == "") {
						valid = false;
						invalid_msg += "<br/><strong>Obat</strong> tidak boleh kosong";
						$("#retur_obat_nama_obat").addClass("error_field");
					}
					if (jumlah == "") {
						valid = false;
						invalid_msg += "<br/><strong>Jumlah</strong> tidak boleh kosong";
						$("#retur_obat_jumlah").addClass("error_field");
					} else if (!is_numeric(jumlah)) {
						valid = false;
						invalid_msg += "<br/><strong>Jumlah</strong> seharusnya numerik (0-9)";
					} else if ((parseFloat(jumlah) - parseFloat(jumlah_lama)) > parseFloat(sisa)) {
						valid = false;
						invalid_msg += "<br/><strong>Jumlah</strong> melebihi stok";
						$("#retur_obat_jumlah").addClass("error_field");
					}
					if (keterangan == "") {
						valid = false;
						invalid_msg += "<br/><strong>Keterangan</strong> tidak boleh kosong";
						$("#retur_obat_keterangan").addClass("error_field");
					}
					if (!valid) {
						$("#modal_alert_retur_obat_add_form").html(
							"<div class='alert alert-block alert-danger'>" +
								"<h4>Peringatan</h4>" +
								invalid_msg +
							"</div>"
						);
					}
					return valid;
				};
				ReturObatAction.prototype.save = function() {
					if (!this.validate()) {
						return;
					}
					$("#retur_obat_add_form").smodal("hide");
					showLoading();
					var self = this;
					var data = this.getRegulerData();		
					data['command'] = "save";
					data['push_command'] = "save";
					data['id'] = $("#retur_obat_id").val();
					data['id_stok_obat'] = $("#retur_obat_id_stok_obat").val();
					data['tanggal'] = $("#retur_obat_tanggal").val();
					data['jumlah'] = $("#retur_obat_jumlah").val();
					data['keterangan'] = $("#retur_obat_keterangan").val();
					$.post(
						"",
						data,
						function(response) {
							var json = getContent(response);
							if (json == null) {
								$("#retur_obat_add_form").smodal("show");
							} else {
								self.view();
							}
							dismissLoading();
						}
					);
				};
				ReturObatAction.prototype.edit = function(id) {
					var data = this.getRegulerData();		
					data['command'] = "edit";
					data['id'] = id;
					$.post(
						"",
						data,
						function(response) {
							var json = getContent(response);
							if (json == null) return;
							$("#retur_obat_id").val(json.id);
							$("#retur_obat_tanggal").val(json.tanggal);
							$("#retur_obat_tanggal").removeAttr("disabled");
							$("#retur_obat_tanggal").attr("disabled", "disabled");
							$("#retur_obat_id_stok_obat").val(json.id_stok_obat);
							retur_obat_obat.select(json.id_stok_obat);
							$("#retur_obat_nama_obat").removeAttr("disabled");
							$("#retur_obat_nama_obat").attr("disabled", "disabled");
							$("#retur_obat_chooser_nama_obat").removeAttr("onclick");
							$("#retur_obat_chooser_nama_obat").removeClass("btn-primary");
							$("#retur_obat_chooser_nama_obat").removeClass("btn-inverse");
							$("#retur_obat_chooser_nama_obat").addClass("btn-inverse");
							$(".retur_obat_f_sisa").show();
							$("#retur_obat_jumlah").val(json.jumlah);
							$("#retur_obat_jumlah").removeAttr("disabled");
							$("#retur_obat_jumlah_lama").val(json.jumlah);
							$("#retur_obat_keterangan").val(json.keterangan);
							$("#retur_obat_keterangan").removeAttr("disabled");
							$("#modal_alert_retur_obat_add_form").html("");
							$(".error_field").removeClass("error_field");
							$("#retur_obat_save").show();
							$("#retur_obat_save").removeAttr("onclick");
							$("#retur_obat_save").attr("onclick", "retur_obat.update(" +  id + ")");
							$("#retur_obat_ok").hide();
							$("#retur_obat_add_form").smodal("show");
						}
					);
				};
				ReturObatAction.prototype.update = function(id) {
					if (!this.validate()) {
						return;
					}
					if (!this.validate()) {
						return;
					}
					$("#retur_obat_add_form").smodal("hide");
					showLoading();
					var self = this;
					var data = this.getRegulerData();		
					data['command'] = "save";
					data['push_command'] = "save";
					data['id'] = $("#retur_obat_id").val();
					data['id_stok_obat'] = $("#retur_obat_id_stok_obat").val();
					data['tanggal'] = $("#retur_obat_tanggal").val();
					data['jumlah_lama'] = $("#retur_obat_jumlah_lama").val();
					data['jumlah'] = $("#retur_obat_jumlah").val();
					data['keterangan'] = $("#retur_obat_keterangan").val();
					data['status'] = "belum";
					$.post(
						"",
						data,
						function(response) {
							var json = getContent(response);
							if (json == null) {
								$("#retur_obat_add_form").smodal("show");
							} else {
								self.view();
							}
							dismissLoading();
						}
					);
				};
				ReturObatAction.prototype.del = function(id) {
					var self = this;
					var data = this.getRegulerData();		
					data['command'] = "del";
					data['id'] = id;
					data['push_command'] = "delete";
					bootbox.confirm(
						"Anda yakin menghapus data ini?", 
						function(result) {
							if(result) {
								showLoading();
								$.post(
									'',
									data,
									function(response) {
										var json = getContent(response);
										if (json == null) return;
										self.view();
										dismissLoading();
									}
								);
							}
						}
					);
				};
				ReturObatAction.prototype.detail = function(id) {
					var data = this.getRegulerData();		
					data['command'] = "edit";
					data['id'] = id;
					$.post(
						"",
						data,
						function(response) {
							var json = getContent(response);
							if (json == null) return;
							$("#retur_obat_id").val(json.id);
							$("#retur_obat_tanggal").val(json.tanggal);
							$("#retur_obat_tanggal").removeAttr("disabled");
							$("#retur_obat_tanggal").attr("disabled", "disabled");
							$("#retur_obat_id_stok_obat").val(json.id_stok_obat);
							retur_obat_obat.select(json.id_stok_obat);
							$("#retur_obat_nama_obat").removeAttr("disabled");
							$("#retur_obat_nama_obat").attr("disabled", "disabled");
							$("#retur_obat_chooser_nama_obat").removeAttr("onclick");
							$("#retur_obat_chooser_nama_obat").removeClass("btn-primary");
							$("#retur_obat_chooser_nama_obat").removeClass("btn-inverse");
							$("#retur_obat_chooser_nama_obat").addClass("btn-inverse");
							$(".retur_obat_f_sisa").hide();
							$("#retur_obat_jumlah").val(json.jumlah);
							$("#retur_obat_jumlah").removeAttr("disabled");
							$("#retur_obat_jumlah").attr("disabled", "disabled");
							$("#retur_obat_keterangan").val(json.keterangan);
							$("#retur_obat_keterangan").removeAttr("disabled");
							$("#retur_obat_keterangan").attr("disabled", "disabled");
							$("#modal_alert_retur_obat_add_form").html("");
							$(".error_field").removeClass("error_field");
							$("#retur_obat_save").hide();
							$("#retur_obat_save").removeAttr("onclick");
							$("#retur_obat_ok").show();
							$("#retur_obat_add_form").smodal("show");
						}
					);
				};
				

				var RETUR_OBAT_PNAME="<?php echo $this->proto_name; ?>";
				var RETUR_OBAT_PSLUG="<?php echo $this->proto_slug; ?>";
				var RETUR_OBAT_PIMPL="<?php echo $this->proto_implement; ?>";
				var RETUR_OBAT_ENTITY="<?php echo $this->page; ?>";
				var retur_obat;
				var retur_obat_obat;
				$(document).ready(function() {
					$('[data-toggle="popover"]').popover({
						trigger: 'hover',
						'placement': 'top'
					});
					$(".mydate").datepicker();
					$("#smis-chooser-modal").on("show", function() {
						$("#smis-chooser-modal").removeClass("half_model");
						$("#smis-chooser-modal").addClass("half_model");
					});
					$("#smis-chooser-modal").on("hidden", function() {
						$("#smis-chooser-modal").removeClass("half_model");
					});
					retur_obat_obat = new TableAction("retur_obat_obat",RETUR_OBAT_ENTITY,"retur_obat",new Array());
					retur_obat_obat.setSuperCommand("retur_obat_obat");
					retur_obat_obat.setPrototipe(RETUR_OBAT_PNAME,RETUR_OBAT_PSLUG,RETUR_OBAT_PIMPL);
					retur_obat_obat.selected = function(json) {		
						$("#retur_obat_id_stok_obat").val(json.id);
						$("#retur_obat_nama_obat").val(json.nama_obat);
						$("#retur_obat_sisa").val(json.sisa);
						$("#retur_obat_f_sisa").val(json.sisa + " " + json.satuan);
						$("#retur_obat_satuan").val(json.satuan);
					};
					var retur_obat_columns = new Array("id", "id_stok_obat", "tanggal", "jumlah", "keterangan", "status");
					retur_obat = new ReturObatAction("retur_obat",RETUR_OBAT_ENTITY,"retur_obat",retur_obat_columns);
					retur_obat.setPrototipe(RETUR_OBAT_PNAME,RETUR_OBAT_PSLUG,RETUR_OBAT_PIMPL);
					retur_obat.view();
				});
			</script>
			<?php 
		}	
	}
?>