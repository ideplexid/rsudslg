<?php 
	require_once 'smis-base/smis-include-service-consumer.php';
	require_once 'smis-framework/smis/template/ModulTemplate.php';
	require_once 'smis-libs-inventory/table/PermintaanBarangTable.php';
	require_once 'smis-libs-inventory/adapter/PermintaanBarangAdapter.php';
	require_once 'smis-libs-inventory/responder/PermintaanBarangDBResponder.php';
	require_once 'smis-libs-inventory/service_consumer/PushPermintaanBarangService.php';
	require_once 'smis-libs-inventory/table/DPermintaanBarangTable.php';

	class PermintaanBarang extends ModulTemplate{
		private $db;
		private $name;
		private $tbl_permintaan_barang;
		private $tbl_dpermintaan_barang;
		private $page;
		private $action;
		private $permintaan_barang_table;
		private $proto_name;
		private $proto_implement;
		private $proto_slug;
		
		public function __construct($db, $name_entity, $slug_table_parent, $slug_table_child, $page){
			$this->db=$db;
			$this->name=$name_entity;
			$this->tbl_permintaan_barang=$slug_table_parent;
			$this->tbl_dpermintaan_barang=$slug_table_child;
			$this->page=$page;
			$this->permintaan_barang_table = new PermintaanBarangTable(array("Nomor", "Tanggal", "Status"), $this->name . " : Permintaan Barang");
			$this->permintaan_barang_table->setName("permintaan_barang");	
			$this->proto_name="";
			$this->proto_implement="";
			$this->proto_slug="";
		}
		
		public function setPrototype($pname,$pslug,$pimplement){
			$this->proto_name=$pname;
			$this->proto_implement=$pimplement;
			$this->proto_slug=$pslug;
		}
		
		public function superCommand($super_command){
			$barang_table = new Table(array("Barang", "Jenis"));
			$barang_table->setName("permintaan_barang_barang");
			$barang_table->setModel(Table::$SELECT);
			$barang_adapter = new SimpleAdapter();
			$barang_adapter->add("Barang", "nama");
			$barang_adapter->add("Jenis", "nama_jenis_barang");
			$barang_service_responder = new ServiceResponder($this->db,$barang_table,$barang_adapter,"get_daftar_barang_nm");
			$super_command = new SuperCommand();
			$super_command->addResponder("permintaan_barang_barang", $barang_service_responder);
			$init = $super_command->initialize();
			if ($init != null) {
				echo $init;
				return;
			}
		}
		
		public function command($command){		
			$permintaan_barang_adapter = new PermintaanBarangAdapter();
			$columns = array("id", "tanggal", "status");
			$permintaan_barang_dbtable = new DBTable($this->db,$this->tbl_permintaan_barang,$columns);
			$permintaan_barang_dbresponder = new PermintaanBarangDBResponder($permintaan_barang_dbtable,$this->permintaan_barang_table,$permintaan_barang_adapter,$this->tbl_dpermintaan_barang,$this->page);
			$data = $permintaan_barang_dbresponder->command($_POST['command']);
			if (isset($_POST['push_command'])) {			
				$permintaan_barang_dbtable = new DBTable($this->db, $this->tbl_permintaan_barang);
				$permintaan_barang_row = $permintaan_barang_dbtable->select($data['content']['id']);
				$dpermintaan_barang_dbtable = new DBTable($this->db, $this->tbl_dpermintaan_barang);
				$dpermintaan_barang_dbtable->addCustomKriteria("id_permintaan_barang"," ='".$data['content']['id']."'");
				$view=$dpermintaan_barang_dbtable->getQueryView("", "0");
				$detail = $dpermintaan_barang_dbtable->get_result($view['query']);
				$command = "push_" . $_POST['push_command'];
				$push_permintaan_barang_service_consumer = new PushPermintaanBarangService($this->db, $permintaan_barang_row->id, $permintaan_barang_row->tanggal, $permintaan_barang_row->status, $detail, $command, $this->page);
				$push_permintaan_barang_service_consumer->execute();
			}
			echo json_encode($data);
			return;
		}
		
		public function phpPreLoad(){
			$this->permintaan_barang_table->addModal("id", "hidden", "", "");
			$this->permintaan_barang_table->addModal("tanggal", "date", "Tanggal", "");
			$permintaan_barang_modal=$this->permintaan_barang_table->getModal();
			$permintaan_barang_modal->setTitle("Permintaan Barang");
			$permintaan_barang_button = new Button("", "", "OK");
			$permintaan_barang_button->setClass("btn-success");
			$permintaan_barang_button->setAtribute("id='permintaan_barang_ok'");
			$permintaan_barang_button->setAction("$($(this).data('target')).smodal('hide')");
			$permintaan_barang_modal->addFooter($permintaan_barang_button);
			$permintaan_barang_modal->setModalSize(Modal::$FULL_MODEL);
			
			$head=array("Barang", "Jml. Permintaan", "Jml. Dipenuhi", "Keterangan");
			$dpermintaan_barang_table = new DPermintaanBarangTable($head);
			$dpermintaan_barang_table->setName("dpermintaan_barang");
			$dpermintaan_barang_table->setFooterVisible(false);
			$dpermintaan_barang_table->addModal("id", "hidden", "", "");
			$dpermintaan_barang_table->addModal("nama_barang", "chooser-permintaan_barang-permintaan_barang_barang", "Nama Barang", "");
			$dpermintaan_barang_table->addModal("jumlah_diminta", "text", "Jumlah", "","numeric","n");
			$dpermintaan_barang_table->addModal("satuan_diminta", "text", "Satuan", "","free","n");
			$dpermintaan_barang_modal=$dpermintaan_barang_table->getModal();
			$dpermintaan_barang_modal->setTitle("Data Detail Permintaan Barang");
			$permintaan_barang_modal->addBody("dpermintaan_barang_table", $dpermintaan_barang_table);

			echo $dpermintaan_barang_modal->getHtml();
			echo $permintaan_barang_modal->getHtml();
			echo $this->permintaan_barang_table->getHtml();
			echo addJS("framework/smis/js/table_action.js");
			echo addJS("framework/bootstrap/js/bootstrap-datepicker.js");
			echo addCSS("framework/bootstrap/css/datepicker.css");
		}
		
		public function jsPreLoad(){
			?>
			<script type="text/javascript">

				//PARENT SYSTEM
				function PermintaanBarangAction(name, page, action, column) {
					this.initialize(name, page, action, column);
				}
				PermintaanBarangAction.prototype.constructor = PermintaanBarangAction;
				PermintaanBarangAction.prototype = new TableAction();
				PermintaanBarangAction.prototype.show_add_form = function() {
					var today = new Date();
					$("#permintaan_barang_id").val("");
					$("#permintaan_barang_tanggal").val(today.getFullYear() + "-" + (Number(today.getMonth())+1) + "-" + today.getDate());
					$("#permintaan_barang_tanggal").removeAttr("disabled");
					$("#dpermintaan_barang_add").show();
					PERMINTAAN_BARANG_ROW_ID = 0;
					$("tbody#dpermintaan_barang_list").children("tr").remove();
					$("#modal_alert_permintaan_barang_add_form").html("");
					$(".error_field").removeClass("error_field");
					$("#permintaan_barang_save").show();
					$("#permintaan_barang_save").removeAttr("onclick");
					$("#permintaan_barang_save").attr("onclick", "permintaan_barang.save()");
					$("#permintaan_barang_ok").hide();
					$("#permintaan_barang_add_form").smodal("show");
				};
				PermintaanBarangAction.prototype.validate = function() {
					var valid = true;
					var invalid_msg = "";
					var tanggal = $("#permintaan_barang_tanggal").val();
					var nord = $("tbody#dpermintaan_barang_list").children("tr").length;
					$(".error_field").removeClass("error_field");
					if (tanggal == "") {
						valid = false;
						invalid_msg += "<br/><strong>Tanggal</strong> tidak boleh kosong";
						$("#permintaan_barang_tanggal").addClass("error_field");
					}
					if (nord == 0) {
						valid = false;
						invalid_msg += "<br/><strong>Detil Permintaan Barang</strong> tidak boleh kosong";
					}
					if (!valid) {
						$("#modal_alert_permintaan_barang_add_form").html(
							"<div class='alert alert-block alert-danger'>" +
								"<h4>Peringatan</h4>" +
								invalid_msg +
							"</div>"
						);
					}
					return valid;
				};
				PermintaanBarangAction.prototype.save = function() {
					if (!this.validate()) {
						return;
					}
					$("#permintaan_barang_add_form").smodal("hide");
					showLoading();
					var self = this;
					var data = this.getRegulerData();		
					data['command'] = "save";
					data['push_command'] = "save";
					data['id'] = $("#permintaan_barang_id").val();
					data['tanggal'] = $("#permintaan_barang_tanggal").val();
					data['status'] = "belum";
					var detail = {};
					var nor = $("tbody#dpermintaan_barang_list").children("tr").length;
					for(var i = 0; i < nor; i++) {
						var d_data = {};
						var prefix = $("tbody#dpermintaan_barang_list").children('tr').eq(i).prop("id");
						d_data['nama_barang'] = $("#" + prefix + "_nama_barang").text();
						d_data['jumlah_permintaan'] = $("#" + prefix + "_jumlah_diminta").text();
						d_data['satuan_permintaan'] = $("#" + prefix + "_satuan_diminta").text();
						d_data['jumlah_dipenuhi'] = $("#" + prefix + "_jumlah_dipenuhi").text();
						d_data['satuan_dipenuhi'] = $("#" + prefix + "_satuan_dipenuhi").text();
						d_data['keterangan'] = $("#" + prefix + "_keterangan").text();
						detail[i] = d_data;
					}
					data['detail'] = detail;
					$.post(
						"",
						data,
						function(response) {
							var json = getContent(response);
							if (json == null) {
								$("#permintaan_barang_add_form").smodal("show");
							} else {
								self.view();
							}
							dismissLoading();
						}
					);
				};
				PermintaanBarangAction.prototype.edit = function(id) {
					var data = this.getRegulerData();		
					data['command'] = "edit";
					data['id'] = id;
					$.post(
						"",
						data,
						function(response) {
							var json = getContent(response);
							if (json == null) return;
							$("#permintaan_barang_id").val(json.header.id);
							$("#permintaan_barang_tanggal").val(json.header.tanggal);
							$("#permintaan_barang_tanggal").removeAttr("disabled");
							$("#dpermintaan_barang_add").show();
							PERMINTAAN_BARANG_ROW_ID = 0;
							$("tbody#dpermintaan_barang_list").children("tr").remove();
							$("#modal_alert_permintaan_barang_add_form").html("");
							$(".error_field").removeClass("error_field");
							for(var i = 0; i < json.detail.length; i++) {
								var d_id = json.detail[i].id;
								var d_id_permintaan_barang = json.detail[i].id_permintaan_barang;
								var d_nama_barang = json.detail[i].nama_barang;
								var d_jumlah_diminta = json.detail[i].jumlah_permintaan;
								var d_satuan_diminta = json.detail[i].satuan_permintaan;
								var d_jumlah_dipenuhi = json.detail[i].jumlah_dipenuhi;
								var d_satuan_dipenuhi = json.detail[i].satuan_dipenuhi;
								var d_keterangan = json.detail[i].keterangan;
								var f_jumlah_dipenuhi = "-";
								var edit_group = "";
								if (Number(d_jumlah_dipenuhi) != 0) {
									f_jumlah_dipenuhi = d_jumlah_dipenuhi + " " + d_satuan_dipenuhi;
								} else {
									edit_group = "<a href='#' onclick='dpermintaan_barang.edit(" + PERMINTAAN_BARANG_ROW_ID + ")' data-content='Ubah' data-toggle='popover' class='input btn btn-warning'>" + 
													"<i class='icon-edit icon-white'></i>" +
												 "</a>" +
												 "<a href='#' onclick='dpermintaan_barang.del(" + PERMINTAAN_BARANG_ROW_ID + ")' data-content='Hapus' data-toggle='popover' class='input btn btn-danger'>" + 
													"<i class='icon-remove icon-white'></i>" + 
												 "</a>";
								}
								$("tbody#dpermintaan_barang_list").append(
									"<tr id='data_" + PERMINTAAN_BARANG_ROW_ID + "'>" +
										"<td id='data_" + PERMINTAAN_BARANG_ROW_ID + "_id' style='display: none;'>" + d_id + "</td>" +
										"<td id='data_" + PERMINTAAN_BARANG_ROW_ID + "_jumlah_diminta' style='display: none;'>" + d_jumlah_diminta + "</td>" +
										"<td id='data_" + PERMINTAAN_BARANG_ROW_ID + "_satuan_diminta' style='display: none;'>" + d_satuan_diminta + "</td>" +
										"<td id='data_" + PERMINTAAN_BARANG_ROW_ID + "_jumlah_dipenuhi' style='display: none;'>" + d_jumlah_dipenuhi + "</td>" +
										"<td id='data_" + PERMINTAAN_BARANG_ROW_ID + "_satuan_dipenuhi' style='display: none;'>" + d_satuan_dipenuhi + "</td>" +
										"<td id='data_" + PERMINTAAN_BARANG_ROW_ID + "_nama_barang'>" + d_nama_barang + "</td>" +
										"<td id='data_" + PERMINTAAN_BARANG_ROW_ID + "_f_jumlah_diminta'>" + d_jumlah_diminta + " " + d_satuan_diminta + "</td>" +
										"<td id='data_" + PERMINTAAN_BARANG_ROW_ID + "_f_jumlah_dipenuhi'>" + f_jumlah_dipenuhi + "</td>" +
										"<td id='data_" + PERMINTAAN_BARANG_ROW_ID + "_keterangan'>" + d_keterangan + "</td>" +
										"<td>" +
											"<div class='btn-group noprint'>" +
												edit_group +
											"</div>" +
										"</td>" +
									"</tr>"
								);
								PERMINTAAN_BARANG_ROW_ID++;
							}
							$("#permintaan_barang_save").removeAttr("onclick");
							$("#permintaan_barang_save").attr("onclick", "permintaan_barang.update(" + id + ")");
							$("#permintaan_barang_save").show();
							$("#permintaan_barang_ok").hide();
							$("#modal_alert_permintaan_barang_add_form").html("");
							$(".error_field").removeClass("error_field");
							$("#permintaan_barang_add_form").smodal("show");
						}
					);
				};
				PermintaanBarangAction.prototype.update = function(id) {
					if (!this.validate()) {
						return;
					}
					$("#permintaan_barang_add_form").smodal("hide");
					showLoading();
					var self = this;
					var data = this.getRegulerData();
					
					data['command'] = "save";
					data['push_command'] = "save";
					data['id'] = $("#permintaan_barang_id").val();
					data['tanggal'] = $("#permintaan_barang_tanggal").val();
					data['status'] = "belum";
					var detail = {};
					var nor = $("tbody#dpermintaan_barang_list").children("tr").length;
					for(var i = 0; i < nor; i++) {
						var d_data = {};
						var prefix = $("tbody#dpermintaan_barang_list").children('tr').eq(i).prop("id");
						d_data['id'] = $("#" + prefix + "_id").text();
						d_data['nama_barang'] = $("#" + prefix + "_nama_barang").text();
						d_data['jumlah_permintaan'] = $("#" + prefix + "_jumlah_diminta").text();
						d_data['satuan_permintaan'] = $("#" + prefix + "_satuan_diminta").text();
						d_data['jumlah_dipenuhi'] = $("#" + prefix + "_jumlah_dipenuhi").text();
						d_data['satuan_dipenuhi'] = $("#" + prefix + "_satuan_dipenuhi").text();
						d_data['keterangan'] = $("#" + prefix + "_keterangan").text();
						if ($("#" + prefix).attr("class") == "deleted") {
							d_data['cmd'] = "delete";
						} else if ($("#" + prefix + "_id").text() == "") {
							d_data['cmd'] = "insert";
						} else {
							d_data['cmd'] = "update";
						}
						detail[i] = d_data;
					}
					data['detail'] = detail;
					$.post(
						"",
						data,
						function(response) {
							var json = getContent(response);
							if (json == null) {
								$("#permintaan_barang_add_form").smodal("show");
							} else {
								self.view();
							}
							dismissLoading();
						}
					);
				};
				PermintaanBarangAction.prototype.del = function(id) {
					var self = this;
					var del_data = this.getRegulerData();
					del_data['command'] = "del";
					del_data['id'] = id;
					del_data['push_command'] = "delete";
					bootbox.confirm(
						"Anda yakin ingin menghapus data ini?", 
						function(result) {
						   if (result) {
							   showLoading();
								$.post(
									"",
									del_data,
									function(res){
										var json = getContent(res);
										if (json == null) return;
										self.view();
										dismissLoading();
									}
								);
							}
						}
					);
				};
				PermintaanBarangAction.prototype.detail = function(id) {
					var data = this.getRegulerData();		
					data['command'] = "edit";
					data['id'] = id;
					$.post(
						"",
						data,
						function(response) {
							var json = getContent(response);
							if (json == null) return;
							$("#permintaan_barang_id").val(json.header.id);
							$("#permintaan_barang_tanggal").val(json.header.tanggal);
							$("#permintaan_barang_tanggal").removeAttr("disabled");
							$("#permintaan_barang_tanggal").attr("disabled", "disabled");
							$("#dpermintaan_barang_add").hide();
							PERMINTAAN_BARANG_ROW_ID = 0;
							$("tbody#dpermintaan_barang_list").children("tr").remove();
							$("#modal_alert_permintaan_barang_add_form").html("");
							$(".error_field").removeClass("error_field");
							for(var i = 0; i < json.detail.length; i++) {
								var d_id = json.detail[i].id;
								var d_id_permintaan_barang = json.detail[i].id_permintaan_barang;
								var d_nama_barang = json.detail[i].nama_barang;
								var d_jumlah_diminta = json.detail[i].jumlah_permintaan;
								var d_satuan_diminta = json.detail[i].satuan_permintaan;
								var d_jumlah_dipenuhi = json.detail[i].jumlah_dipenuhi;
								var d_satuan_dipenuhi = json.detail[i].satuan_dipenuhi;
								var d_keterangan = json.detail[i].keterangan;
								var f_jumlah_dipenuhi = "-";
								var edit_group = "";
								if (Number(d_jumlah_dipenuhi) != 0) {
									f_jumlah_dipenuhi = d_jumlah_dipenuhi + " " + d_satuan_dipenuhi;
								}
								$("tbody#dpermintaan_barang_list").append(
									"<tr id='data_" + PERMINTAAN_BARANG_ROW_ID + "'>" +
										"<td id='data_" + PERMINTAAN_BARANG_ROW_ID + "_id' style='display: none;'>" + d_id + "</td>" +
										"<td id='data_" + PERMINTAAN_BARANG_ROW_ID + "_jumlah_diminta' style='display: none;'>" + d_jumlah_diminta + "</td>" +
										"<td id='data_" + PERMINTAAN_BARANG_ROW_ID + "_satuan_diminta' style='display: none;'>" + d_satuan_diminta + "</td>" +
										"<td id='data_" + PERMINTAAN_BARANG_ROW_ID + "_jumlah_dipenuhi' style='display: none;'>" + d_jumlah_dipenuhi + "</td>" +
										"<td id='data_" + PERMINTAAN_BARANG_ROW_ID + "_satuan_dipenuhi' style='display: none;'>" + d_satuan_dipenuhi + "</td>" +
										"<td id='data_" + PERMINTAAN_BARANG_ROW_ID + "_nama_barang'>" + d_nama_barang + "</td>" +
										"<td id='data_" + PERMINTAAN_BARANG_ROW_ID + "_f_jumlah_diminta'>" + d_jumlah_diminta + " " + d_satuan_diminta + "</td>" +
										"<td id='data_" + PERMINTAAN_BARANG_ROW_ID + "_f_jumlah_dipenuhi'>" + f_jumlah_dipenuhi + "</td>" +
										"<td id='data_" + PERMINTAAN_BARANG_ROW_ID + "_keterangan'>" + d_keterangan + "</td>" +
										"<td></td>" +
									"</tr>"
								);
								PERMINTAAN_BARANG_ROW_ID++;
							}
							$("#permintaan_barang_save").removeAttr("onclick");
							$("#permintaan_barang_save").hide();
							$("#permintaan_barang_ok").show();
							$("#modal_alert_permintaan_barang_add_form").html("");
							$(".error_field").removeClass("error_field");
							$("#permintaan_barang_add_form").smodal("show");
						}
					);
				};
				PermintaanBarangAction.prototype.print_spb = function(id) {
					var data = this.getRegulerData();
					data['command'] = "print_spb";
					data['id'] = id;
					$.post(
						"",
						data,
						function(response) {
							var json = getContent(response);
							if (json == null) return;
							smis_print(json);
						}
					);
				};


				//CHILD SYSTEM
				
				function DPermintaanBarangAction(name, page, action, column) {
					this.initialize(name, page, action, column);
				}
				DPermintaanBarangAction.prototype.constructor = DPermintaanBarangAction;
				DPermintaanBarangAction.prototype = new TableAction();
				DPermintaanBarangAction.prototype.show_add_form = function() {
					$("#dpermintaan_barang_id").val("");
					$("#dpermintaan_barang_nama_barang").val("");
					$("#dpermintaan_barang_nama_barang").removeAttr("disabled");
					$("#dpermintaan_barang_nama_barang").attr("disabled", "disabled");
					$("#dpermintaan_barang_jumlah_diminta").val("");
					$("#dpermintaan_barang_satuan_diminta").val("");
					$("#modal_alert_dpermintaan_barang_add_form").html("");
					$(".error_field").removeClass("error_field");
					$("#dpermintaan_barang_save").removeAttr("onclick");
					$("#dpermintaan_barang_save").attr("onclick", "dpermintaan_barang.save()");
					$("#dpermintaan_barang_add_form").smodal("show");
				};
				DPermintaanBarangAction.prototype.validate = function() {
					var valid = true;
					var invalid_msg = "";
					var nama_barang = $("#dpermintaan_barang_nama_barang").val();
					var jumlah_diminta = $("#dpermintaan_barang_jumlah_diminta").val();
					var satuan_diminta = $("#dpermintaan_barang_satuan_diminta").val();
					$(".error_field").removeClass("error_field");
					if (nama_barang == "") {
						valid = false;
						invalid_msg += "<br/><strong>Barang</strong> tidak boleh kosong";
						$("#dpermintaan_barang_nama_barang").addClass("error_field");
					}
					if (jumlah_diminta == "") {
						valid = false;
						invalid_msg += "<br/><strong>Jumlah</strong> tidak boleh kosong";
						$("#dpermintaan_barang_jumlah_diminta").addClass("error_field");
					} else if (!is_numeric(jumlah_diminta)) {
						valid = false;
						invalid_msg += "<br/><strong>Jumlah</strong> seharusnya numerik (0-9)";
						$("#dpermintaan_barang_jumlah_diminta").addClass("error_field");
					}
					if (satuan_diminta == "") {
						valid = false;
						invalid_msg += "<br/><strong>Satuan</strong> tidak boleh kosong";
						$("#dpermintaan_barang_satuan_diminta").addClass("error_field");
					}
					if (!valid) {
						$("#modal_alert_dpermintaan_barang_add_form").html(
							"<div class='alert alert-block alert-danger'>" +
								"<h4>Peringatan</h4>" +
								invalid_msg +
							"</div>"
						);
					}
					return valid;
				};
				DPermintaanBarangAction.prototype.save = function() {
					if (!this.validate()) {
						return;
					}
					var id = $("#dpermintaan_barang_id").val();
					var nama_barang = $("#dpermintaan_barang_nama_barang").val();
					var jumlah_diminta = $("#dpermintaan_barang_jumlah_diminta").val();
					var satuan_diminta = $("#dpermintaan_barang_satuan_diminta").val();
					$("tbody#dpermintaan_barang_list").append(
						"<tr id='data_" + PERMINTAAN_BARANG_ROW_ID + "'>" +
							"<td id='data_" + PERMINTAAN_BARANG_ROW_ID + "_id' style='display: none;'>" + id + "</td>" +
							"<td id='data_" + PERMINTAAN_BARANG_ROW_ID + "_jumlah_diminta' style='display: none;'>" + jumlah_diminta + "</td>" +
							"<td id='data_" + PERMINTAAN_BARANG_ROW_ID + "_satuan_diminta' style='display: none;'>" + satuan_diminta + "</td>" +
							"<td id='data_" + PERMINTAAN_BARANG_ROW_ID + "_jumlah_dipenuhi' style='display: none;'>0</td>" +
							"<td id='data_" + PERMINTAAN_BARANG_ROW_ID + "_satuan_dipenuhi' style='display: none;'>-</td>" +
							"<td id='data_" + PERMINTAAN_BARANG_ROW_ID + "_nama_barang'>" + nama_barang + "</td>" +
							"<td id='data_" + PERMINTAAN_BARANG_ROW_ID + "_f_jumlah_diminta'>" + jumlah_diminta + " " + satuan_diminta + "</td>" +
							"<td id='data_" + PERMINTAAN_BARANG_ROW_ID + "_f_jumlah_dipenuhi'>-</td>" +
							"<td id='data_" + PERMINTAAN_BARANG_ROW_ID + "_keterangan'>-</td>" +
							"<td>" +
								"<div class='btn-group noprint'>" +
									"<a href='#' onclick='dpermintaan_barang.edit(" + PERMINTAAN_BARANG_ROW_ID + ")' data-content='Ubah' data-toggle='popover' class='input btn btn-warning'>" + 
										"<i class='icon-edit icon-white'></i>" +
									"</a>" +
									"<a href='#' onclick='dpermintaan_barang.del(" + PERMINTAAN_BARANG_ROW_ID + ")' data-content='Hapus' data-toggle='popover' class='input btn btn-danger'>" + 
										"<i class='icon-remove icon-white'></i>" + 
									"</a>" +
								"</div>" +
							"</td>" +
						"</tr>"
					);
					PERMINTAAN_BARANG_ROW_ID++;
					$("#dpermintaan_barang_add_form").smodal("hide");
				};
				DPermintaanBarangAction.prototype.edit = function(r_num) {
					var id = $("#data_" + r_num + "_id").text();
					var nama_barang = $("#data_" + r_num + "_nama_barang").text();
					var jumlah_diminta = $("#data_" + r_num + "_jumlah_diminta").text();
					var satuan_diminta = $("#data_" + r_num + "_satuan_diminta").text();
					$("#dpermintaan_barang_id").val(id);
					$("#dpermintaan_barang_nama_barang").val(nama_barang);
					$("#dpermintaan_barang_nama_barang").removeAttr("disabled");
					$("#dpermintaan_barang_nama_barang").attr("disabled", "disabled");
					$("#dpermintaan_barang_jumlah_diminta").val(jumlah_diminta);
					$("#dpermintaan_barang_satuan_diminta").val(satuan_diminta);
					$("#dpermintaan_barang_save").removeAttr("onclick");
					$("#dpermintaan_barang_save").attr("onclick", "dpermintaan_barang.update(" + r_num + ")");
					$("#dpermintaan_barang_add_form").smodal("show");
				};
				DPermintaanBarangAction.prototype.update = function(r_num) {
					if (!this.validate()) {
						return;
					}
					var nama_barang = $("#dpermintaan_barang_nama_barang").val();
					var jumlah_diminta = $("#dpermintaan_barang_jumlah_diminta").val();
					var satuan_diminta = $("#dpermintaan_barang_satuan_diminta").val();
					$("#data_" + r_num + "_nama_barang").text(nama_barang);
					$("#data_" + r_num + "_jumlah_diminta").text(jumlah_diminta);
					$("#data_" + r_num + "_satuan_diminta").text(satuan_diminta);
					$("#data_" + r_num + "_f_jumlah_diminta").text(jumlah_diminta + " " + satuan_diminta);
					$("#dpermintaan_barang_add_form").smodal("hide");
				};
				DPermintaanBarangAction.prototype.del = function(r_num) {
					var id = $("#data_" + r_num + "_id").text();
					if (id.length == 0) {
						$("#data_" + r_num).remove();
					} else {
						$("#data_" + r_num).attr("style", "display: none;");
						$("#data_" + r_num).attr("class", "deleted");
					}
				};
				
				function BarangAction(name, page, action, column) {
					this.initialize(name, page, action, column);
				}
				BarangAction.prototype.constructor = BarangAction;
				BarangAction.prototype = new TableAction();
				BarangAction.prototype.selected = function(json) {
					$("#dpermintaan_barang_nama_barang").val(json.nama);
				};

				var PERMINTAAN_BARANG_PNAME="<?php echo $this->proto_name; ?>";
				var PERMINTAAN_BARANG_PSLUG="<?php echo $this->proto_slug; ?>";
				var PERMINTAAN_BARANG_PIMPL="<?php echo $this->proto_implement; ?>";
				var PERMINTAAN_BARANG_ENTITY="<?php echo $this->page; ?>";
				var permintaan_barang;
				var dpermintaan_barang;
				var PERMINTAAN_BARANG_ROW_ID;
				var permintaan_barang_barang;
				$(document).ready(function() {
					$('[data-toggle="popover"]').popover({
						trigger: 'hover',
						'placement': 'top'
					});
					$(".mydate").datepicker();		
					permintaan_barang_barang = new BarangAction("permintaan_barang_barang",PERMINTAAN_BARANG_ENTITY,"permintaan_barang",new Array());
					permintaan_barang_barang.setSuperCommand("permintaan_barang_barang");
					permintaan_barang_barang.setPrototipe(PERMINTAAN_BARANG_PNAME,PERMINTAAN_BARANG_PSLUG,PERMINTAAN_BARANG_PIMPL);
					var dpermintaan_barang_columns = new Array("id", "nama_barang", "jumlah_permintaan", "satuan_permintaan", "jumlah_dipenuhi", "satuan_dipenuhi", "keterangan");
					dpermintaan_barang = new DPermintaanBarangAction("dpermintaan_barang",PERMINTAAN_BARANG_ENTITY,"permintaan_barang",dpermintaan_barang_columns);
					dpermintaan_barang.setPrototipe(PERMINTAAN_BARANG_PNAME,PERMINTAAN_BARANG_PSLUG,PERMINTAAN_BARANG_PIMPL);
					var permintaan_barang_columns = new Array("id", "tanggal", "status");
					permintaan_barang = new PermintaanBarangAction("permintaan_barang",PERMINTAAN_BARANG_ENTITY,"permintaan_barang",permintaan_barang_columns);
					permintaan_barang.setPrototipe(PERMINTAAN_BARANG_PNAME,PERMINTAAN_BARANG_PSLUG,PERMINTAAN_BARANG_PIMPL);
					permintaan_barang.view();
				});
			</script>
			<?php 
		}	
	}
?>