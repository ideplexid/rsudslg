<?php 
	require_once 'smis-framework/smis/template/ModulTemplate.php';
	require_once 'smis-libs-inventory/adapter/PenyesuaianStokObatAdapter.php';
	require_once 'smis-libs-inventory/responder/PenyesuaianStokObatDBResponder.php';

	class PenyesuaianStokObat extends ModulTemplate {
		private $db;
		private $name;
		private $tbl_penyesuaian;
		private $tbl_stok_obat;
		private $tbl_riwayat_stok;
		private $tbl_obat_masuk;
		private $page;
		private $action;
		private $penyesuaian_stok_table;
		private $proto_name;
		private $proto_implement;
		private $proto_slug;
		
		public function __construct($db, $name_entity, $tbl_penyesuaian, $tbl_stok, $tbl_riwayat_stok, $tbl_masuk, $page){
			$this->db=$db;
			$this->name=$name_entity;
			$this->tbl_penyesuaian=$tbl_penyesuaian;
			$this->tbl_stok_obat=$tbl_stok;
			$this->tbl_riwayat_stok=$tbl_riwayat_stok;
			$this->tbl_obat_masuk=$tbl_masuk;
			$this->page=$page;
			$header=array("No. Stok", "No. Mutasi", "Tanggal Masuk", "Obat", "Jenis", "Stok", "Keterangan", "Tanggal Exp.");
			$this->penyesuaian_stok_table = new Table($header, $this->name . " : Penyesuaian Stok Obat");
			$this->penyesuaian_stok_table->setName("penyesuaian_stok_obat");
			$this->penyesuaian_stok_table->setAddButtonEnable(false);
			$this->penyesuaian_stok_table->setDelButtonEnable(false);
			$this->proto_name="";
			$this->proto_implement="";
			$this->proto_slug="";
		}
		
		public function setPrototype($pname,$pslug,$pimplement){
			$this->proto_name=$pname;
			$this->proto_implement=$pimplement;
			$this->proto_slug=$pslug;
		}
		
		
		public function command($command){
			$penyesuaian_stok_adapter = new PenyesuaianStokObatAdapter();
			$columns = array("id", "id_stok_obat", "tanggal", "jumlah_baru", "jumlah_lama", "keterangan", "nama_user");
			$penyesuaian_stok_dbtable = new DBTable($this->db,$this->tbl_penyesuaian,$columns);
			$filter = "";
			if (isset($_POST['kriteria'])) {
				$filter = " AND (".$this->tbl_stok_obat.".nama_obat LIKE '%" . $_POST['kriteria'] . "%' OR ".$this->tbl_stok_obat.".nama_jenis_obat LIKE '%" . $_POST['kriteria'] . "%')";
			}
			$query_value = "
				SELECT ".$this->tbl_stok_obat.".*, " . $this->tbl_obat_masuk . ".tanggal AS 'tanggal_masuk', " . $this->tbl_obat_masuk . ".id AS 'id_obat_masuk', " . $this->tbl_obat_masuk . ".f_id
				FROM ".$this->tbl_stok_obat." LEFT JOIN " . $this->tbl_obat_masuk . " ON " . $this->tbl_stok_obat . ".id_obat_masuk = " . $this->tbl_obat_masuk . ".id
				WHERE ".$this->tbl_stok_obat.".prop NOT LIKE 'del' AND " . $this->tbl_obat_masuk . ".status = 'sudah' " . $filter . "
				ORDER BY ".$this->tbl_stok_obat.".nama_obat, ".$this->tbl_stok_obat.".tanggal_exp ASC
			";
			$query_count = "
				SELECT COUNT(*)
				FROM ".$this->tbl_stok_obat." LEFT JOIN " . $this->tbl_obat_masuk . " ON " . $this->tbl_stok_obat . ".id_obat_masuk = " . $this->tbl_obat_masuk . ".id
				WHERE ".$this->tbl_stok_obat.".prop NOT LIKE 'del' AND " . $this->tbl_obat_masuk . ".status = 'sudah' " . $filter . "
				ORDER BY ".$this->tbl_stok_obat.".nama_obat, ".$this->tbl_stok_obat.".tanggal_exp ASC
			";
			$penyesuaian_stok_dbtable->setPreferredQuery(true, $query_value, $query_count);		
			$penyesuaian_stok_dbresponder = new PenyesuaianStokObatDBResponder(	$penyesuaian_stok_dbtable,$this->penyesuaian_stok_table,$penyesuaian_stok_adapter,$this->tbl_stok_obat,$this->tbl_riwayat_stok);
			if ($penyesuaian_stok_dbresponder->is("save")) {
				global $user;
				$penyesuaian_stok_dbresponder->addColumnFixValue("nama_user", $user->getName());
			}
			$data = $penyesuaian_stok_dbresponder->command($_POST['command']);
			echo json_encode($data);
			return;
		}
		
		
		public function phpPreLoad(){
			$this->penyesuaian_stok_table->addModal("id_stok_obat", "hidden", "", "");
			$this->penyesuaian_stok_table->addModal("tanggal", "date", "Tanggal", "");
			$this->penyesuaian_stok_table->addModal("nama_obat", "text", "Obat", "","n","free",true);
			$this->penyesuaian_stok_table->addModal("f_jumlah_lama", "text", "Jumlah Tercatat", "","n","free",true);
			$this->penyesuaian_stok_table->addModal("jumlah_lama", "hidden", "", "");
			$this->penyesuaian_stok_table->addModal("jumlah_baru", "text", "Jumlah Aktual", "","n","numeric");
			$this->penyesuaian_stok_table->addModal("satuan", "text", "Satuan", "","n","free",true);
			$this->penyesuaian_stok_table->addModal("keterangan", "textarea", "Keterangan", "","n","free");
			$penyesuaian_stok_modal=$this->penyesuaian_stok_table->getModal();
			$penyesuaian_stok_modal->setTitle("Data Penyesuaian Stok");
			$penyesuaian_stok_modal->setComponentSize(Modal::$MEDIUM);
			
			echo $penyesuaian_stok_modal->getHtml();
			echo $this->penyesuaian_stok_table->getHtml();
			echo addJS("framework/smis/js/table_action.js");
			echo addJS("framework/bootstrap/js/bootstrap-datepicker.js");
			echo addCSS("framework/bootstrap/css/datepicker.css");
		}
		
		public function jsPreLoad(){
			?>				
			<script type="text/javascript">
				function PenyesuaianStokAction(name, page, action, column) {
					this.initialize(name, page, action, column);
				}
				PenyesuaianStokAction.prototype.constructor = PenyesuaianStokAction;
				PenyesuaianStokAction.prototype = new TableAction();
				PenyesuaianStokAction.prototype.edit = function(id) {
					var data = this.getRegulerData();
					data['command'] = "edit";
					data['id'] = id;
					$.post(
						"",
						data,
						function(response) {
							var json = getContent(response);
							if (json == null) return;
							$("#penyesuaian_stok_obat_id_stok_obat").val(id);
							var today = new Date();
							$("#penyesuaian_stok_obat_tanggal").val(today.getFullYear() + "-" + (today.getMonth()+1) + "-" + today.getDate());
							$("#penyesuaian_stok_obat_nama_obat").val(json.nama_obat);
							$("#penyesuaian_stok_obat_f_jumlah_lama").val(json.sisa + " " + json.satuan);
							$("#penyesuaian_stok_obat_jumlah_lama").val(json.sisa);
							$("#penyesuaian_stok_obat_jumlah_baru").val(json.sisa);
							$("#penyesuaian_stok_obat_satuan").val(json.satuan);
							$("#penyesuaian_stok_obat_keterangan").val("");
							$("#modal_alert_penyesuaian_stok_obat_add_form").html("");
							$(".error_field").removeClass("error_field");
							$("#penyesuaian_stok_obat_add_form").smodal("show");
						}
					);
				};
				PenyesuaianStokAction.prototype.validate = function() {
					var valid = true;
					var invalid_msg = "";
					var tanggal = $("#penyesuaian_stok_obat_tanggal").val();
					var jumlah_aktual = $("#penyesuaian_stok_obat_jumlah_baru").val();
					var jumlah_tercatat = $("#penyesuaian_stok_obat_jumlah_tercatat").val();
					var keterangan = $("#penyesuaian_stok_obat_keterangan").val();
					$(".error_field").removeClass("error_field");
					if (jumlah_aktual == "") {
						valid = false;
						invalid_msg += "</br><strong>Jml. Aktual</strong> tidak boleh kosong";
						$("#penyesuaian_stok_obat_jumlah_baru").addClass("error_field");
					} else if (!is_numeric(jumlah_aktual)) {
						valid = false;
						invalid_msg += "</br><strong>Jml. Aktual</strong> seharusnya numerik (0-9)";
						$("#penyesuaian_stok_obat_jumlah_baru").addClass("error_field");
					} else if (parseFloat(jumlah_aktual) == parseFloat(jumlah_tercatat)) {
						valid = false;
						invalid_msg += "</br><strong>Jml. Aktual</strong> dan <strong>Jml. Tercatat</strong> tidak terdapat selisih";
						$("#penyesuaian_stok_obat_jumlah_baru").addClass("error_field");
					}
					if (tanggal == "") {
						valid = false;
						invalid_msg += "</br><strong>Tanggal</strong> tidak boleh kosong";
						$("#penyesuaian_stok_obat_tanggal").addClass("error_field");
					}
					if (keterangan == "") {
						valid = false;
						invalid_msg += "</br><strong>Keterangan</strong> tidak boleh kosong";
						$("#penyesuaian_stok_obat_keterangan").addClass("error_field");
					}
					if (!valid) {
						$("#modal_alert_penyesuaian_stok_obat_add_form").html(
							"<div class='alert alert-block alert-danger'>" +
								"<h4>Peringatan</h4>" +
								invalid_msg +
							"</div>"
						);
					}
					return valid;
				};
				PenyesuaianStokAction.prototype.save = function() {
					if (!this.validate()) {
						return;
					}
					$("#penyesuaian_stok_obat_add_form").smodal("hide");
					showLoading();
					var self = this;
					var data = this.getRegulerData();
					data['command'] = "save";
					data['id'] = "";
					data['id_stok_obat'] = $("#penyesuaian_stok_obat_id_stok_obat").val();
					data['tanggal'] = $("#penyesuaian_stok_obat_tanggal").val();
					data['jumlah_baru'] = $("#penyesuaian_stok_obat_jumlah_baru").val();
					data['jumlah_lama'] = $("#penyesuaian_stok_obat_jumlah_lama").val();
					data['keterangan'] = $("#penyesuaian_stok_obat_keterangan").val();
					$.post(
						"",
						data,
						function(response) {
							var json = getContent(response);
							if (json == null) {
								$("#penyesuaian_stok_obat_add_form").smodal("show");
							} else {
								self.view();
							}
							dismissLoading();
						}
					);
				};

				var PENYESUAIAN_STOK_PNAME="<?php echo $this->proto_name; ?>";
				var PENYESUAIAN_STOK_PSLUG="<?php echo $this->proto_slug; ?>";
				var PENYESUAIAN_STOK_PIMPL="<?php echo $this->proto_implement; ?>";
				var penyesuaian_stok_obat;
				var PENYESUAIAN_STOK_ENTITY="<?php echo $this->page; ?>";
				$(document).ready(function() {
					$('[data-toggle="popover"]').popover({
						trigger: 'hover',
						'placement': 'top'
					});
					$(".mydate").datepicker();
					var penyesuaian_stok_obat_columns = new Array("id", "id_stok_obat", "tanggal", "jumlah_baru", "jumlah_lama", "keterangan");
					penyesuaian_stok_obat = new PenyesuaianStokAction("penyesuaian_stok_obat",PENYESUAIAN_STOK_ENTITY,"detail_stok_obat",	penyesuaian_stok_obat_columns);
					penyesuaian_stok_obat.setPrototipe(PENYESUAIAN_STOK_PNAME,PENYESUAIAN_STOK_PSLUG,PENYESUAIAN_STOK_PIMPL);
					penyesuaian_stok_obat.view();
				});
			</script>
			<?php 
		}	
	}
?>