<?php 
	require_once 'smis-framework/smis/template/ModulTemplate.php';
	require_once 'smis-base/smis-include-service-consumer.php';

	class DaftarObat extends ModulTemplate{
		
		private $db;
		private $name;
		private $tbl_obat_masuk;
		private $tbl_stok_obat;
		private $page;
		private $action;
		private $daftar_obat_table;
		private $proto_name;
		private $proto_implement;
		private $proto_slug;
		private $header_button;
		
		public function __construct($db, $name_entity, $slug_table_parent, $slug_table_child, $page){
			$this->db=$db;
			$this->name=$name_entity;
			$this->tbl_obat_masuk=$slug_table_parent;
			$this->tbl_stok_obat=$slug_table_child;
			$this->page=$page;
			$this->daftar_obat_table = new Table(	array("Nomor", "Nama Obat", "Jenis Obat", "Stok","Nilai"), $this->name . " : Rekapitulasi Stok Obat");
			$this->daftar_obat_table->addHeader("before", $this->headerObat());
			$this->daftar_obat_table->setName("daftar_obat");
			$this->daftar_obat_table->setAddButtonEnable(false);
			$this->header_button=$this->daftar_obat_table->getHeaderButton();
			$this->daftar_obat_table->setPrintButtonEnable(false);
			$this->daftar_obat_table->setReloadButtonEnable(false);
			$this->daftar_obat_table->setAction(false);
				
		}
		
		public function headerObat(){
			$option=new OptionBuilder();
			$option->add("Semua","all")
				   ->add("Semua, Kecuali Kosong","all_not_empty", "1")
				   ->add("Kosong","empty");
			$select=new Select("filter", "Daftar Obat", $option->getContent());
			$tr="<tr><td colspan='10'>".$select->getHtml()."</td></tr>";
			return $tr;
		}
		
		public function setPrototype($pname,$pslug,$pimplement){
			$this->proto_name=$pname;
			$this->proto_implement=$pimplement;
			$this->proto_slug=$pslug;
		}
		
		
		public function command($command){
			$daftar_obat_adapter =new SummaryAdapter();
			$daftar_obat_adapter->addFixValue("Stok", "<strong>Total</strong>");
			$daftar_obat_adapter->addSummary("Nilai", "uang","money Rp.");
			$daftar_obat_adapter->setUseID(false);
			$daftar_obat_adapter->add("id", "id_obat");
			$daftar_obat_adapter->add("Nomor", "id_obat","digit8");
			$daftar_obat_adapter->add("Nama Obat", "nama_obat");
			$daftar_obat_adapter->add("Jenis Obat", "nama_jenis_obat");
			$daftar_obat_adapter->add("Stok", "stok");
			$daftar_obat_adapter->add("Nilai", "uang","money Rp.");
			$columns = array("id_obat", "nama_obat", "nama_jenis_obat", "sisa");
			$daftar_obat_dbtable = new DBTable(	$this->db,$this->tbl_stok_obat,$columns);
			$filter = "";
			if (isset($_POST['kriteria'])) {
				$filter = " AND (		nama_obat LIKE '%" . $_POST['kriteria'] . "%' 
									OR 	nama_jenis_obat LIKE '%" . $_POST['kriteria'] . "%'
								)";
			}
			
			if(isset($_POST['filter']) && $_POST['filter']=="all_not_empty"){
				$filter .= " AND sisa>0 ";
			}else if(isset($_POST['filter']) && $_POST['filter']=="empty"){
				$filter .= " AND sisa<=0 ";
			}
			
			$v_stok="
					SELECT id_obat, 
							nama_obat, 
							nama_jenis_obat, 
							sum(uang) as uang, 
							GROUP_CONCAT(CONCAT(sisa, ' ', satuan) 
							ORDER BY satuan, sisa ASC SEPARATOR ', ') AS 'stok'
					FROM (
						SELECT id_obat, nama_obat, 
								nama_jenis_obat, SUM(sisa) AS 'sisa', 
								sum(hna*sisa) as uang, satuan, 
								konversi, satuan_konversi
						FROM ".$this->tbl_stok_obat." 
						LEFT JOIN ".$this->tbl_obat_masuk." 
							ON ".$this->tbl_stok_obat.".id_obat_masuk = ".$this->tbl_obat_masuk.".id
						WHERE ".$this->tbl_stok_obat.".prop NOT LIKE 'del' 
								AND ".$this->tbl_obat_masuk.".prop NOT LIKE 'del' 
								AND ".$this->tbl_obat_masuk.".status = 'sudah' " . $filter . "
						GROUP BY id_obat, satuan, konversi, satuan_konversi
					) v_obat
					GROUP BY id_obat";
			
			
			$query_value = "SELECT * FROM ( $v_stok ) v_stok ORDER BY nama_obat, nama_jenis_obat ASC";
			$query_count = " SELECT COUNT(*) FROM ( $v_stok ) v_stok WHERE stok NOT LIKE '0 %' ";

			$daftar_obat_dbtable->setPreferredQuery(true, $query_value, $query_count);
			$daftar_obat_dbresponder = new DBResponder(	$daftar_obat_dbtable,$this->daftar_obat_table,$daftar_obat_adapter);
			$data = $daftar_obat_dbresponder->command($_POST['command']);
			echo json_encode($data);
			return;
		}
		
		public function phpPreLoad(){
			echo $this->header_button;
			echo "</br></br>";
			echo $this->daftar_obat_table->getHtml();
			echo addJS("framework/smis/js/table_action.js");
		}
		
		public function jsPreLoad(){
			?>		
			<script type="text/javascript">
				var DAFTAR_OBAT_PNAME="<?php echo $this->proto_name; ?>";
				var DAFTAR_OBAT_PSLUG="<?php echo $this->proto_slug; ?>";
				var DAFTAR_OBAT_PIMPL="<?php echo $this->proto_implement; ?>";
				var DAFTAR_OBAT_ENTITY="<?php echo $this->page; ?>";
				var daftar_obat;
				$(document).ready(function() {
					$('[data-toggle="popover"]').popover({
						trigger: 'hover',
						'placement': 'top'
					});
					var daftar_obat_columns = new Array("id_obat", "nama_obat", "nama_jenis_obat", "sisa");
					daftar_obat = new TableAction("daftar_obat",DAFTAR_OBAT_ENTITY,"daftar_obat",daftar_obat_columns);
					daftar_obat.setPrototipe(DAFTAR_OBAT_PNAME,DAFTAR_OBAT_PSLUG,DAFTAR_OBAT_PIMPL);
					daftar_obat.addViewData=function(view_data){
						view_data['filter']=$("#filter").val();
						return view_data;
					};
					daftar_obat.view();
				});
			</script>
			<?php 
		}	
	}
?>