<?php 
	require_once 'smis-base/smis-include-service-consumer.php';
	require_once 'smis-framework/smis/template/ModulTemplate.php';
	require_once 'smis-libs-inventory/adapter/BarangAdapter.php';
	require_once 'smis-libs-inventory/service_consumer/PushReturBarangServiceConsumer.php';
	require_once 'smis-libs-inventory/adapter/ReturBarangAdapter.php';
	require_once 'smis-libs-inventory/responder/ReturBarangDBResponder.php';
	require_once 'smis-libs-inventory/table/ReturBarangTable.php';

	class ReturBarang extends ModulTemplate {
		private $db;
		private $name;
		private $tbl_retur_barang;
		private $tbl_stok_barang;
		private $tbl_riwayat_stok;
		private $tbl_barang_masuk;
		private $page;
		private $action;
		private $retur_barang_table;
		private $proto_name;
		private $proto_implement;
		private $proto_slug;
		
		public function __construct($db, $name_entity, $tbl_retur, $tbl_stok, $tbl_riwayat_stok, $tbl_masuk, $page){
			$this->db=$db;
			$this->name=$name_entity;
			$this->tbl_retur_barang=$tbl_retur;
			$this->tbl_stok_barang=$tbl_stok;
			$this->tbl_barang_masuk=$tbl_masuk;
			$this->tbl_riwayat_stok=$tbl_riwayat_stok;
			$this->page=$page;
			$header=array("Nomor", "Tanggal Retur", "Barang", "Jenis", "Jumlah", "Status");
			$this->retur_barang_table = new ReturBarangTable($header, $this->name . " : Retur Barang");
			$this->retur_barang_table->setName("retur_barang");
			$this->proto_name="";
			$this->proto_implement="";
			$this->proto_slug="";
		}
		
		public function setPrototype($pname,$pslug,$pimplement){
			$this->proto_name=$pname;
			$this->proto_implement=$pimplement;
			$this->proto_slug=$pslug;
		}
		
		public function command($command){
			$retur_barang_adapter = new ReturBarangAdapter();
			$columns = array("id", "id_stok_barang", "tanggal", "jumlah", "keterangan", "status");
			$retur_barang_dbtable = new DBTable($this->db,"".$this->tbl_retur_barang."",$columns);
			$filter = "";
			if (isset($_POST['kriteria'])) {
				$filter = " AND (".$this->tbl_stok_barang.".nama_barang LIKE '%" . $_POST['kriteria'] . "%' OR ".$this->tbl_stok_barang.".nama_jenis_barang LIKE '%" . $_POST['kriteria'] . "%')";
			}
			$query_value = "
					SELECT ".$this->tbl_retur_barang.".*, ".$this->tbl_stok_barang.".nama_barang, ".$this->tbl_stok_barang.".nama_jenis_barang, ".$this->tbl_stok_barang.".satuan
					FROM ".$this->tbl_retur_barang." LEFT JOIN ".$this->tbl_stok_barang." ON ".$this->tbl_retur_barang.".id_stok_barang = ".$this->tbl_stok_barang.".id
					WHERE ".$this->tbl_retur_barang.".prop NOT LIKE 'del' " . $filter . "
				";
			$query_count = "
					SELECT COUNT(*)
					FROM (
						SELECT ".$this->tbl_retur_barang.".*, ".$this->tbl_stok_barang.".nama_barang, ".$this->tbl_stok_barang.".nama_jenis_barang, ".$this->tbl_stok_barang.".satuan
						FROM ".$this->tbl_retur_barang." LEFT JOIN ".$this->tbl_stok_barang." ON ".$this->tbl_retur_barang.".id_stok_barang = ".$this->tbl_stok_barang.".id
						WHERE ".$this->tbl_retur_barang.".prop NOT LIKE 'del' " . $filter . "
					) v_retur
				";
			$retur_barang_dbtable->setPreferredQuery(true, $query_value, $query_count);
				
			$retur_barang_dbresponder = new ReturBarangDBResponder(	$retur_barang_dbtable,$this->retur_barang_table,$retur_barang_adapter,$this->tbl_retur_barang,$this->tbl_stok_barang, $this->tbl_riwayat_stok);
			$data = $retur_barang_dbresponder->command($_POST['command']);
			//push_retur_barang:
			if (isset($_POST['push_command'])) {		
				$retur_barang_dbtable = new DBTable($this->db, "".$this->tbl_retur_barang."");
				$retur_barang_row = $retur_barang_dbtable->get_row("
						SELECT ".$this->tbl_retur_barang.".*, ".$this->tbl_stok_barang.".id_barang, ".$this->tbl_stok_barang.".f_id, ".$this->tbl_stok_barang.".nama_barang, ".$this->tbl_stok_barang.".nama_jenis_barang, ".$this->tbl_stok_barang.".id_vendor, ".$this->tbl_stok_barang.".nama_vendor , ".$this->tbl_stok_barang.".satuan, ".$this->tbl_stok_barang.".konversi, ".$this->tbl_stok_barang.".satuan_konversi, ".$this->tbl_stok_barang.".hna, ".$this->tbl_stok_barang.".produsen, ".$this->tbl_stok_barang.".tanggal_exp, ".$this->tbl_stok_barang.".turunan
						FROM ".$this->tbl_retur_barang." LEFT JOIN ".$this->tbl_stok_barang." ON ".$this->tbl_retur_barang.".id_stok_barang = ".$this->tbl_stok_barang.".id
						WHERE ".$this->tbl_retur_barang.".id = '" . $data['content']['id'] . "'
					");
				$command = "push_" . $_POST['push_command'];
				$push_retur_barang_service_consumer = new PushReturBarangServiceConsumer($this->db, $retur_barang_row->id, $retur_barang_row->tanggal, $retur_barang_row->id_barang, $retur_barang_row->f_id, $retur_barang_row->nama_barang, $retur_barang_row->nama_jenis_barang, $retur_barang_row->id_vendor, $retur_barang_row->nama_vendor, $retur_barang_row->jumlah, $retur_barang_row->satuan, $retur_barang_row->konversi, $retur_barang_row->satuan_konversi, $retur_barang_row->hna, $retur_barang_row->produsen, $retur_barang_row->tanggal_exp, $retur_barang_row->turunan, $retur_barang_row->keterangan, $retur_barang_row->status, $command, $this->page);
				$push_retur_barang_service_consumer->execute();
			}
			echo json_encode($data);
			return;
		}
		
		public function superCommand($super_command){
			$header=array("Nomor", "Barang", "Jenis", "Stok", "Tanggal Exp.");
			$barang_table = new Table($header);
			$barang_table->setName("retur_barang_barang");
			$barang_table->setModel(Table::$SELECT);
			
			$barang_adapter	= new BarangAdapter();
			$barang_dbtable = new DBTable($this->db, $this->tbl_stok_barang);
			$filter = "";
			if (isset($_POST['kriteria'])) {
				$filter = " AND (" . $this->tbl_stok_barang . ".nama_barang LIKE '%" . $_POST['kriteria'] . "%' OR ".$this->tbl_stok_barang.".nama_jenis_barang LIKE '%" . $_POST['kriteria'] . "%') ";
			}
			$query_value = "
				SELECT " . $this->tbl_stok_barang . ".*
				FROM " . $this->tbl_stok_barang . " LEFT JOIN " . $this->tbl_barang_masuk . " ON " . $this->tbl_stok_barang . ".id_barang_masuk = " . $this->tbl_barang_masuk . ".id
				WHERE " . $this->tbl_barang_masuk . ".status = 'sudah' " . $filter . "
			";
			$query_count = "
				SELECT COUNT(*)
				FROM " . $this->tbl_stok_barang . " LEFT JOIN " . $this->tbl_barang_masuk . " ON " . $this->tbl_stok_barang . ".id_barang_masuk = " . $this->tbl_barang_masuk . ".id
				WHERE " . $this->tbl_barang_masuk . ".status = 'sudah' " . $filter . "
			";
			$barang_dbtable->setPreferredQuery(true, $query_value, $query_count);
			$barang_dbresponder = new DBResponder($barang_dbtable,$barang_table,$barang_adapter);
			$super_command = new SuperCommand();
			$super_command->addResponder("retur_barang_barang", $barang_dbresponder);
			$init = $super_command->initialize();
			if ($init != null) {
				echo $init;
				return;
			}
		}
		
		public function phpPreLoad(){
			$this->retur_barang_table->addModal("id", "hidden", "", "");
			$this->retur_barang_table->addModal("id_stok_barang", "hidden", "", "");
			$this->retur_barang_table->addModal("nama_barang", "chooser-retur_barang-retur_barang_barang", "Barang", "");
			$this->retur_barang_table->addModal("tanggal", "date", "Tanggal", "");
			$this->retur_barang_table->addModal("sisa", "hidden", "", "");
			$this->retur_barang_table->addModal("f_sisa", "text", "Stok", "","n","free",true);
			$this->retur_barang_table->addModal("jumlah", "text", "Jumlah", "");
			$this->retur_barang_table->addModal("jumlah_lama", "hidden", "", "");
			$this->retur_barang_table->addModal("satuan", "text", "Satuan", "","n","free",true);
			$this->retur_barang_table->addModal("keterangan", "textarea", "Keterangan", "");
			$retur_barang_modal=$this->retur_barang_table->getModal();
			$retur_barang_button = new Button("", "", "OK");
			$retur_barang_button->setClass("btn-success");
			$retur_barang_button->setAtribute("id='retur_barang_ok'");
			$retur_barang_button->setAction("$($(this).data('target')).smodal('hide')");
			$retur_barang_modal->addFooter($retur_barang_button);
			$retur_barang_modal->setTitle("Data Retur Barang");
			
			echo $retur_barang_modal->getHtml();
			echo $this->retur_barang_table->getHtml();
			echo addJS("framework/smis/js/table_action.js");
			echo addJS("framework/bootstrap/js/bootstrap-datepicker.js");
			echo addCSS("framework/bootstrap/css/datepicker.css");
		}
		
		public function jsPreLoad(){
			?>
			<script type="text/javascript">
				function ReturBarangAction(name, page, action, column) {
					this.initialize(name, page, action, column);
				}
				ReturBarangAction.prototype.constructor = ReturBarangAction;
				ReturBarangAction.prototype = new TableAction();
				ReturBarangAction.prototype.show_add_form = function() {
					var today = new Date();
					$("#retur_barang_id").val("");
					$("#retur_barang_tanggal").val(today.getFullYear() + "-" + (Number(today.getMonth())+1) + "-" + today.getDate());
					$("#retur_barang_tanggal").removeAttr("disabled");
					$("#retur_barang_id_stok_barang").val("");
					$("#retur_barang_nama_barang").val("");
					$("#retur_barang_nama_barang").removeAttr("disabled");
					$("#retur_barang_nama_barang").attr("disabled", "disabled");
					$("#retur_barang_chooser_nama_barang").removeAttr("onclick");
					$("#retur_barang_chooser_nama_barang").attr("onclick", "retur_barang.chooser('retur_barang','retur_barang_nama_barang','retur_barang_barang',retur_barang_barang)");
					$("#retur_barang_chooser_nama_barang").removeClass("btn-primary");
					$("#retur_barang_chooser_nama_barang").removeClass("btn-inverse");
					$("#retur_barang_chooser_nama_barang").addClass("btn-primary");
					$("#retur_barang_sisa").val(0);
					$("#retur_barang_f_sisa").val("");
					$(".retur_barang_f_sisa").show();
					$("#retur_barang_jumlah").val("");
					$("#retur_barang_jumlah").removeAttr("disabled");
					$("#retur_barang_jumlah_lama").val(0);
					$("#retur_barang_satuan").val("");
					$("#retur_barang_keterangan").val("");
					$("#retur_barang_keterangan").removeAttr("disabled");
					$("#modal_alert_retur_barang_add_form").html("");
					$(".error_field").removeClass("error_field");
					$("#retur_barang_save").show();
					$("#retur_barang_save").removeAttr("onclick");
					$("#retur_barang_save").attr("onclick", "retur_barang.save()");
					$("#retur_barang_ok").hide();
					$("#retur_barang_add_form").smodal("show");
				};
				ReturBarangAction.prototype.validate = function() {
					var valid = true;
					var invalid_msg = "";
					var tanggal = $("#retur_barang_tanggal").val();
					var nama_barang = $("#retur_barang_nama_barang").val();
					var jumlah = $("#retur_barang_jumlah").val();
					var jumlah_lama = $("#retur_barang_jumlah_lama").val();
					var sisa = $("#retur_barang_sisa").val();
					var keterangan = $("#retur_barang_keterangan").val();
					$(".error_field").removeClass("error_field");
					if (tanggal == "") {
						valid = false;
						invalid_msg += "<br/><strong>Tanggal</strong> tidak boleh kosong";
						$("#retur_barang_tanggal").addClass("error_field");
					}
					if (nama_barang == "") {
						valid = false;
						invalid_msg += "<br/><strong>Barang</strong> tidak boleh kosong";
						$("#retur_barang_nama_barang").addClass("error_field");
					}
					if (jumlah == "") {
						valid = false;
						invalid_msg += "<br/><strong>Jumlah</strong> tidak boleh kosong";
						$("#retur_barang_jumlah").addClass("error_field");
					} else if (!is_numeric(jumlah)) {
						valid = false;
						invalid_msg += "<br/><strong>Jumlah</strong> seharusnya numerik (0-9)";
					} else if ((parseFloat(jumlah) - parseFloat(jumlah_lama)) > parseFloat(sisa)) {
						valid = false;
						invalid_msg += "<br/><strong>Jumlah</strong> melebihi stok";
						$("#retur_barang_jumlah").addClass("error_field");
					}
					if (keterangan == "") {
						valid = false;
						invalid_msg += "<br/><strong>Keterangan</strong> tidak boleh kosong";
						$("#retur_barang_keterangan").addClass("error_field");
					}
					if (!valid) {
						$("#modal_alert_retur_barang_add_form").html(
							"<div class='alert alert-block alert-danger'>" +
								"<h4>Peringatan</h4>" +
								invalid_msg +
							"</div>"
						);
					}
					return valid;
				};
				ReturBarangAction.prototype.save = function() {
					if (!this.validate()) {
						return;
					}
					$("#retur_barang_add_form").smodal("hide");
					showLoading();
					var self = this;
					var data = this.getRegulerData();
					
					data['command'] = "save";
					data['push_command'] = "save";
					data['id'] = $("#retur_barang_id").val();
					data['id_stok_barang'] = $("#retur_barang_id_stok_barang").val();
					data['tanggal'] = $("#retur_barang_tanggal").val();
					data['jumlah'] = $("#retur_barang_jumlah").val();
					data['keterangan'] = $("#retur_barang_keterangan").val();
					$.post(
						"",
						data,
						function(response) {
							var json = getContent(response);
							if (json == null) {
								$("#retur_barang_add_form").smodal("show");
							} else {
								self.view();
							}
							dismissLoading();
						}
					);
				};
				ReturBarangAction.prototype.edit = function(id) {
					var data = this.getRegulerData();
					data['command'] = "edit";
					data['id'] = id;
					$.post(
						"",
						data,
						function(response) {
							var json = getContent(response);
							if (json == null) return;
							$("#retur_barang_id").val(json.id);
							$("#retur_barang_tanggal").val(json.tanggal);
							$("#retur_barang_tanggal").removeAttr("disabled");
							$("#retur_barang_tanggal").attr("disabled", "disabled");
							$("#retur_barang_id_stok_barang").val(json.id_stok_barang);
							retur_barang_barang.select(json.id_stok_barang);
							$("#retur_barang_nama_barang").removeAttr("disabled");
							$("#retur_barang_nama_barang").attr("disabled", "disabled");
							$("#retur_barang_chooser_nama_barang").removeAttr("onclick");
							$("#retur_barang_chooser_nama_barang").removeClass("btn-primary");
							$("#retur_barang_chooser_nama_barang").removeClass("btn-inverse");
							$("#retur_barang_chooser_nama_barang").addClass("btn-inverse");
							$(".retur_barang_f_sisa").show();
							$("#retur_barang_jumlah").val(json.jumlah);
							$("#retur_barang_jumlah").removeAttr("disabled");
							$("#retur_barang_jumlah_lama").val(json.jumlah);
							$("#retur_barang_keterangan").val(json.keterangan);
							$("#retur_barang_keterangan").removeAttr("disabled");
							$("#modal_alert_retur_barang_add_form").html("");
							$(".error_field").removeClass("error_field");
							$("#retur_barang_save").show();
							$("#retur_barang_save").removeAttr("onclick");
							$("#retur_barang_save").attr("onclick", "retur_barang.update(" +  id + ")");
							$("#retur_barang_ok").hide();
							$("#retur_barang_add_form").smodal("show");
						}
					);
				};
				ReturBarangAction.prototype.update = function(id) {
					if (!this.validate()) {
						return;
					}
					if (!this.validate()) {
						return;
					}
					$("#retur_barang_add_form").smodal("hide");
					showLoading();
					var self = this;
					var data = this.getRegulerData();
					
					data['command'] = "save";
					data['push_command'] = "save";
					data['id'] = $("#retur_barang_id").val();
					data['id_stok_barang'] = $("#retur_barang_id_stok_barang").val();
					data['tanggal'] = $("#retur_barang_tanggal").val();
					data['jumlah_lama'] = $("#retur_barang_jumlah_lama").val();
					data['jumlah'] = $("#retur_barang_jumlah").val();
					data['keterangan'] = $("#retur_barang_keterangan").val();
					data['status'] = "belum";
					$.post(
						"",
						data,
						function(response) {
							var json = getContent(response);
							if (json == null) {
								$("#retur_barang_add_form").smodal("show");
							} else {
								self.view();
							}
							dismissLoading();
						}
					);
				};
				ReturBarangAction.prototype.del = function(id) {
					var self = this;
					var data = this.getRegulerData();
					data['command'] = "del";
					data['id'] = id;
					data['push_command'] = "delete";
					bootbox.confirm(
						"Anda yakin menghapus data ini?", 
						function(result) {
							if(result) {
								showLoading();
								$.post(
									'',
									data,
									function(response) {
										var json = getContent(response);
										if (json == null) return;
										self.view();
										dismissLoading();
									}
								);
							}
						}
					);
				};
				ReturBarangAction.prototype.detail = function(id) {
					var data = this.getRegulerData();
					data['command'] = "edit";
					data['id'] = id;
					$.post(
						"",
						data,
						function(response) {
							var json = getContent(response);
							if (json == null) return;
							$("#retur_barang_id").val(json.id);
							$("#retur_barang_tanggal").val(json.tanggal);
							$("#retur_barang_tanggal").removeAttr("disabled");
							$("#retur_barang_tanggal").attr("disabled", "disabled");
							$("#retur_barang_id_stok_barang").val(json.id_stok_barang);
							retur_barang_barang.select(json.id_stok_barang);
							$("#retur_barang_nama_barang").removeAttr("disabled");
							$("#retur_barang_nama_barang").attr("disabled", "disabled");
							$("#retur_barang_chooser_nama_barang").removeAttr("onclick");
							$("#retur_barang_chooser_nama_barang").removeClass("btn-primary");
							$("#retur_barang_chooser_nama_barang").removeClass("btn-inverse");
							$("#retur_barang_chooser_nama_barang").addClass("btn-inverse");
							$(".retur_barang_f_sisa").hide();
							$("#retur_barang_jumlah").val(json.jumlah);
							$("#retur_barang_jumlah").removeAttr("disabled");
							$("#retur_barang_jumlah").attr("disabled", "disabled");
							$("#retur_barang_keterangan").val(json.keterangan);
							$("#retur_barang_keterangan").removeAttr("disabled");
							$("#retur_barang_keterangan").attr("disabled", "disabled");
							$("#modal_alert_retur_barang_add_form").html("");
							$(".error_field").removeClass("error_field");
							$("#retur_barang_save").hide();
							$("#retur_barang_save").removeAttr("onclick");
							$("#retur_barang_ok").show();
							$("#retur_barang_add_form").smodal("show");
						}
					);
				};
				
				function BarangAction(name, page, action, column) {
					this.initialize(name, page, action, column);
				}
				BarangAction.prototype.constructor = BarangAction;
				BarangAction.prototype = new TableAction();
				BarangAction.prototype.selected = function(json) {
					$("#retur_barang_id_stok_barang").val(json.id);
					$("#retur_barang_nama_barang").val(json.nama_barang);
					$("#retur_barang_sisa").val(json.sisa);
					$("#retur_barang_f_sisa").val(json.sisa + " " + json.satuan);
					$("#retur_barang_satuan").val(json.satuan);
				};
				var RETUR_BARANG_PNAME="<?php echo $this->proto_name; ?>";
				var RETUR_BARANG_PSLUG="<?php echo $this->proto_slug; ?>";
				var RETUR_BARANG_PIMPL="<?php echo $this->proto_implement; ?>";
				var RETUR_BARANG_ENTITY="<?php echo $this->page; ?>";
				var retur_barang;
				var retur_barang_barang;
				$(document).ready(function() {
					$('[data-toggle="popover"]').popover({
						trigger: 'hover',
						'placement': 'top'
					});
					$(".mydate").datepicker();
					$("#smis-chooser-modal").on("show", function() {
						$("#smis-chooser-modal").removeClass("half_model");
						$("#smis-chooser-modal").addClass("half_model");
					});
					$("#smis-chooser-modal").on("hidden", function() {
						$("#smis-chooser-modal").removeClass("half_model");
					});
					retur_barang_barang = new BarangAction("retur_barang_barang",RETUR_BARANG_ENTITY,"retur_barang",new Array());
					retur_barang_barang.setSuperCommand("retur_barang_barang");
					retur_barang_barang.setPrototipe(RETUR_BARANG_PNAME,RETUR_BARANG_PSLUG,RETUR_BARANG_PIMPL);
					var retur_barang_columns = new Array("id", "id_stok_barang", "tanggal", "jumlah", "keterangan", "status");
					retur_barang = new ReturBarangAction("retur_barang",RETUR_BARANG_ENTITY,"retur_barang",retur_barang_columns);
					retur_barang.setPrototipe(RETUR_BARANG_PNAME,RETUR_BARANG_PSLUG,RETUR_BARANG_PIMPL);
					retur_barang.view();
				});
			</script>
			<?php 
		}	
	}
?>