<?php 
	require_once 'smis-framework/smis/template/ModulTemplate.php';
	require_once 'smis-libs-inventory/adapter/PSBEDAdapter.php';

	class StockBarangED extends ModulTemplate {
		private $db;
		private $name;
		private $tbl_stok_barang;
		private $tbl_barang_masuk;
		private $page;
		private $action;
		private $psbed_table;
		private $proto_name;
		private $proto_implement;
		private $proto_slug;
		
		public function __construct($db, $name_entity, $tbl_stok, $tbl_masuk, $page) {
			$this->db=$db;
			$this->name=$name_entity;
			$this->tbl_stok_barang=$tbl_stok;
			$this->tbl_barang_masuk = $tbl_masuk;
			$this->page=$page;
			$header=array("Nomor", "Barang", "Jenis Barang", "Produsen", "Vendor", "Jumlah", "Ket. Jumlah", "Tgl. Exp.");
			$this->psbed_table = new Table($header);
			$this->psbed_table->setName("psbed");
			$this->psbed_table->setAddButtonEnable(false);
			$this->psbed_table->setReloadButtonEnable(false);
			$this->psbed_table->setPrintButtonEnable(false);
			$this->psbed_table->setAction(false);
			$this->proto_name="";
			$this->proto_implement="";
			$this->proto_slug="";
		}
		
		public function setPrototype($pname,$pslug,$pimplement){
			$this->proto_name=$pname;
			$this->proto_implement=$pimplement;
			$this->proto_slug=$pslug;
		}	
		
		public function command($command){
			$psbed_adapter = new PSBEDAdapter();
			$psbed_dbtable = new DBTable($this->db,$this->tbl_stok_barang);
			$filter = "";
			if (isset($_POST['kriteria'])) {
				$filter = " AND (" . $this->tbl_stok_barang.".nama_barang LIKE '%" . $_POST['kriteria'] . "%' OR " . $this->tbl_stok_barang.".nama_jenis_barang LIKE '%" . $_POST['kriteria'] . "%' OR " . $this->tbl_stok_barang.".nama_vendor LIKE '%" . $_POST['kriteria'] . "%' OR " . $this->tbl_stok_barang.".produsen LIKE '%" . $_POST['kriteria'] . "%')";
			}
			$query_value = "
				SELECT *
				FROM (
					SELECT " . $this->tbl_stok_barang . ".*
					FROM " . $this->tbl_stok_barang . " LEFT JOIN " . $this->tbl_barang_masuk . " ON " . $this->tbl_stok_barang . ".id_barang_masuk = " . $this->tbl_barang_masuk . ".id
					WHERE " . $this->tbl_stok_barang . ".tanggal_exp != '0000-00-00' AND " . $this->tbl_stok_barang . ".sisa > 0 AND DATEDIFF(tanggal_exp, '" . $_POST['tanggal'] . "') <= " . $_POST['rentang'] . " AND " . $this->tbl_barang_masuk . ".status = 'sudah' " . $filter . "
					ORDER BY " . $this->tbl_stok_barang . ".tanggal_exp, " . $this->tbl_stok_barang . ".nama_barang ASC
				) v
			";
			$query_count = "
				SELECT COUNT(*)
				FROM (
					SELECT " . $this->tbl_stok_barang . ".*
					FROM " . $this->tbl_stok_barang . " LEFT JOIN " . $this->tbl_barang_masuk . " ON " . $this->tbl_stok_barang . ".id_barang_masuk = " . $this->tbl_barang_masuk . ".id
					WHERE " . $this->tbl_stok_barang . ".tanggal_exp != '0000-00-00' AND " . $this->tbl_stok_barang . ".sisa > 0 AND DATEDIFF(tanggal_exp, '" . $_POST['tanggal'] . "') <= " . $_POST['rentang'] . " AND " . $this->tbl_barang_masuk . ".status = 'sudah' " . $filter . "
					ORDER BY " . $this->tbl_stok_barang . ".tanggal_exp, " . $this->tbl_stok_barang . ".nama_barang ASC
				) v
			";
			$psbed_dbtable->setPreferredQuery(true, $query_value, $query_count);
			$psbed_dbresponder = new DBResponder($psbed_dbtable,$this->psbed_table,$psbed_adapter);
			$data = $psbed_dbresponder->command($_POST['command']);
			echo json_encode($data);
			return;
		}	
		
		public function phpPreLoad(){
			$pengecekan_stok_barang_ed_form = new Form("psbed_form", "", $this->name . " : Stok Barang ED");
			$tanggal_text = new Text("psbed_tanggal", "psbed_tanggal", date('Y-m-d'));
			$tanggal_text->setModel(Text::$DATE);
			$pengecekan_stok_barang_ed_form->addElement("Tgl. Sekarang", $tanggal_text);
			$rentang_option = new OptionBuilder();
			$rentang_option->add("7 hari", "7");
			$rentang_option->add("30 hari", "30");
			$rentang_option->add("60 hari", "60");
			$rentang_option->add("90 hari", "90");
			$rentang_option->add("180 hari", "180", "1");
			$rentang_select = new Select("psbed_rentang", "psbed_rentang", $rentang_option->getContent());
			$pengecekan_stok_barang_ed_form->addElement("Rentang", $rentang_select);
			$show_button = new Button("", "", "Tampilkan");
			$show_button->setClass("btn-primary");
			$show_button->setIcon("icon-white icon-repeat");
			$show_button->setIsButton(Button::$ICONIC);
			$show_button->setAction("psbed.view()");
			$pengecekan_stok_barang_ed_form->addElement("", $show_button);
			
			echo $pengecekan_stok_barang_ed_form->getHtml();
			echo "<div id'table_content'>";
			echo $this->psbed_table->getHtml();
			echo "</div>";
			echo addJS("framework/smis/js/table_action.js");
			echo addJS("framework/bootstrap/js/bootstrap-datepicker.js");
			echo addCSS("framework/bootstrap/css/datepicker.css");
		}
		
		public function jsPreLoad(){
			?>
			<script type="text/javascript">
				var PSBED_PNAME="<?php echo $this->proto_name; ?>";
				var PSBED_PSLUG="<?php echo $this->proto_slug; ?>";
				var PSBED_PIMPL="<?php echo $this->proto_implement; ?>";
				var PSBED_ENTITY="<?php echo $this->page; ?>";
				var psbed;
				$(document).ready(function() {
					$('[data-toggle="popover"]').popover({
						trigger: 'hover',
						'placement': 'top'
					});
					$(".mydate").datepicker();
					psbed = new TableAction("psbed",PSBED_ENTITY,"pengecekan_stok_barang_ed",new Array());
					psbed.setPrototipe(PSBED_PNAME,PSBED_PSLUG,PSBED_PIMPL);
					psbed.getViewData = function() {
						var data = TableAction.prototype.getViewData.call(this);
						data['tanggal'] = $("#psbed_tanggal").val();
						data['rentang'] = $("#psbed_rentang").val();
						return data;
					};
					psbed.view();
				});
			</script>
			<?php 
		}	
	}
?>