<?php 
	require_once 'smis-framework/smis/template/ModulTemplate.php';
	require_once 'smis-libs-inventory/adapter/PenggunaanBarangAdapter.php';
	require_once 'smis-libs-inventory/table/PenggunaanBarangTable.php';
	require_once 'smis-libs-inventory/responder/PenggunaanBarangDBResponder.php';
	require_once 'smis-libs-inventory/adapter/BarangAdapter.php';

	class PenggunaanBarang extends ModulTemplate {
		private $db;
		private $name;
		private $tbl_penggunaan_barang;
		private $tbl_stok_barang;
		private $tbl_riwayat_stok;
		private $tbl_barang_masuk;
		private $page;
		private $action;
		private $penggunaan_barang_table;
		private $proto_name;
		private $proto_implement;
		private $proto_slug;
		
		public function __construct($db, $name_entity, $tbl_penggunaan, $tbl_stok, $tbl_riwayat_stok, $tbl_masuk, $page){
			$this->db=$db;
			$this->name=$name_entity;
			$this->tbl_penggunaan_barang=$tbl_penggunaan;
			$this->tbl_stok_barang=$tbl_stok;
			$this->tbl_riwayat_stok=$tbl_riwayat_stok;
			$this->tbl_barang_masuk=$tbl_masuk;
			$this->page=$page;
			$header=array("Nomor", "Tanggal", "Barang", "Jumlah");
			$this->penggunaan_barang_table = new PenggunaanBarangTable($header, $this->name . " : Penggunaan Barang");
			$this->penggunaan_barang_table->setName("penggunaan_barang");
		}

		public function setPrototype($pname,$pslug,$pimplement){
			$this->proto_name=$pname;
			$this->proto_implement=$pimplement;
			$this->proto_slug=$pslug;
		}
		
		public function phpPreLoad(){
			$this->penggunaan_barang_table->addModal("id", "hidden", "", "");
			$this->penggunaan_barang_table->addModal("id_stok_barang", "hidden", "", "");
			$this->penggunaan_barang_table->addModal("tanggal", "date", "Tanggal", "");
			$this->penggunaan_barang_table->addModal("nama_barang", "chooser-penggunaan_barang-penggunaan_barang_barang", "Barang", "");
			$this->penggunaan_barang_table->addModal("sisa", "hidden", "", "");
			$this->penggunaan_barang_table->addModal("f_sisa", "text", "Stok", "","y","free",true);
			$this->penggunaan_barang_table->addModal("jumlah", "text", "Jumlah", "","y","numeric",true);
			$this->penggunaan_barang_table->addModal("satuan", "text", "Satuan", "","y","free",true);
			$this->penggunaan_barang_table->addModal("keterangan", "textarea", "Keterangan", "");
			$penggunaan_barang_modal=$this->penggunaan_barang_table->getModal();
			$penggunaan_barang_button = new Button("", "", "OK");
			$penggunaan_barang_button->setClass("btn-success");
			$penggunaan_barang_button->setAtribute("id='penggunaan_barang_ok'");
			$penggunaan_barang_button->setAction("$($(this).data('target')).smodal('hide')");
			$penggunaan_barang_modal->addFooter($penggunaan_barang_button);
			$penggunaan_barang_modal->setTitle("Data Penggunaan Barang");
			echo $penggunaan_barang_modal->getHtml();
			echo $this->penggunaan_barang_table->getHtml();
			echo addJS("framework/smis/js/table_action.js");
			echo addJS("framework/bootstrap/js/bootstrap-datepicker.js");
			echo addCSS("framework/bootstrap/css/datepicker.css");
		}
		
		public function superCommand($super_command){
			$header=array("Nomor", "Barang", "Jenis", "Stok","Tanggal Exp.");
			$barang_table = new Table($header);
			$barang_table->setName("penggunaan_barang_barang");
			$barang_table->setModel(Table::$SELECT);
			$barang_adapter	= new BarangAdapter();
			$barang_dbtable = new DBTable($this->db, $this->tbl_stok_barang);
			$filter = "";
			if (isset($_POST['kriteria'])) {
				$filter = " AND (" . $this->tbl_stok_barang . ".nama_barang LIKE '%" . $_POST['kriteria'] . "%' OR ".$this->tbl_stok_barang.".nama_jenis_barang LIKE '%" . $_POST['kriteria'] . "%') ";
			}
			$query_value = "
				SELECT " . $this->tbl_stok_barang . ".*
				FROM " . $this->tbl_stok_barang . " LEFT JOIN " . $this->tbl_barang_masuk . " ON " . $this->tbl_stok_barang . ".id_barang_masuk = " . $this->tbl_barang_masuk . ".id
				WHERE " . $this->tbl_barang_masuk . ".status = 'sudah' " . $filter . "
			";
			$query_count = "
				SELECT COUNT(*)
				FROM " . $this->tbl_stok_barang . " LEFT JOIN " . $this->tbl_barang_masuk . " ON " . $this->tbl_stok_barang . ".id_barang_masuk = " . $this->tbl_barang_masuk . ".id
				WHERE " . $this->tbl_barang_masuk . ".status = 'sudah' " . $filter . "
			";
			$barang_dbtable->setPreferredQuery(true, $query_value, $query_count);
			$barang_dbresponder = new DBResponder($barang_dbtable,$barang_table,$barang_adapter);
			$super_command = new SuperCommand();
			$super_command->addResponder("penggunaan_barang_barang", $barang_dbresponder);
			$init = $super_command->initialize();
			if ($init != null) {
				echo $init;
				return;
			}
		}
		
		
		public function command($command){
			$penggunaan_barang_adapter = new PenggunaanBarangAdapter();
			$columns = array("id", "id_stok_barang", "tanggal", "jumlah", "keterangan");
			$penggunaan_barang_dbtable = new DBTable($this->db,"".$this->tbl_penggunaan_barang."",$columns);
			$filter = "";
			if (isset($_POST['kriteria'])) {
				$filter = " AND (".$this->tbl_stok_barang.".nama_barang LIKE '%" . $_POST['kriteria'] . "%')";
			}
			$query_value = "
					SELECT ".$this->tbl_penggunaan_barang.".*, 
							".$this->tbl_stok_barang.".nama_barang, 
							".$this->tbl_stok_barang.".nama_jenis_barang, 
							".$this->tbl_stok_barang.".satuan
					FROM 
							".$this->tbl_penggunaan_barang." LEFT JOIN ".$this->tbl_stok_barang." 
							ON ".$this->tbl_penggunaan_barang.".id_stok_barang = ".$this->tbl_stok_barang.".id
					WHERE ".$this->tbl_penggunaan_barang.".prop NOT LIKE 'del' " . $filter . "
				";
			$query_count = "
					SELECT COUNT(*)
					FROM ".$this->tbl_penggunaan_barang."
					WHERE prop NOT LIKE 'del'
				";
			$penggunaan_barang_dbtable->setPreferredQuery(true, $query_value, $query_count);			
			$penggunaan_barang_dbresponder = new PenggunaanBarangDBResponder($penggunaan_barang_dbtable,$this->penggunaan_barang_table,$penggunaan_barang_adapter,$this->tbl_penggunaan_barang,$this->tbl_stok_barang, $this->tbl_riwayat_stok);
			$data = $penggunaan_barang_dbresponder->command($_POST['command']);
			echo json_encode($data);
			return;
		}
		
		public function jsPreLoad(){
			?>
			<script type="text/javascript">
				function PenggunaanBarangAction(name, page, action, column) {
					this.initialize(name, page, action, column);
				}
				PenggunaanBarangAction.prototype.constructor = PenggunaanBarangAction;
				PenggunaanBarangAction.prototype = new TableAction();
				PenggunaanBarangAction.prototype.show_add_form = function() {
					var today = new Date();
					$("#penggunaan_barang_id").val("");
					$("#penggunaan_barang_tanggal").val(today.getFullYear() + "-" + (Number(today.getMonth())+1) + "-" + today.getDate());
					$("#penggunaan_barang_tanggal").removeAttr("disabled");
					$("#penggunaan_barang_id_stok_barang").val("");
					$("#penggunaan_barang_nama_barang").val("");
					$("#penggunaan_barang_nama_barang").removeAttr("disabled");
					$("#penggunaan_barang_nama_barang").attr("disabled", "disabled");
					$("#penggunaan_barang_chooser_nama_barang").removeAttr("onclick");
					$("#penggunaan_barang_chooser_nama_barang").attr("onclick", "penggunaan_barang.chooser('penggunaan_barang','penggunaan_barang_nama_barang','penggunaan_barang_barang',penggunaan_barang_barang)");
					$("#penggunaan_barang_chooser_nama_barang").removeClass("btn-info");
					$("#penggunaan_barang_chooser_nama_barang").removeClass("btn-inverse");
					$("#penggunaan_barang_chooser_nama_barang").addClass("btn-info");
					$("#penggunaan_barang_sisa").val(0);
					$("#penggunaan_barang_f_sisa").val("");
					$(".penggunaan_barang_f_sisa").show();
					$("#penggunaan_barang_jumlah").val("");
					$("#penggunaan_barang_jumlah").removeAttr("disabled");
					$("#penggunaan_barang_satuan").val("");
					$("#penggunaan_barang_keterangan").val("");
					$("#penggunaan_barang_keterangan").removeAttr("disabled");
					$("#penggunaan_barang_save").show();
					$("#penggunaan_barang_save").removeAttr("onclick");
					$("#penggunaan_barang_save").attr("onclick", "penggunaan_barang.save()");
					$("#modal_alert_penggunaan_barang_add_form").html();
					$(".error_field").removeClass("error_field");
					$("#penggunaan_barang_ok").hide();
					$("#penggunaan_barang_add_form").smodal("show");
				};
				PenggunaanBarangAction.prototype.validate = function() {
					var valid = true;
					var invalid_msg = "";
					var tanggal = $("#penggunaan_barang_tanggal").val();
					var nama_barang = $("#penggunaan_barang_nama_barang").val();
					var jumlah = $("#penggunaan_barang_jumlah").val();
					var sisa = $("#penggunaan_barang_sisa").val();
					var keterangan = $("#penggunaan_barang_keterangan").val();
					$(".error_field").removeClass("error_field");
					if (tanggal == "") {
						valid = false;
						invalid_msg += "<br/><strong>Tanggal</strong> tidak boleh kosong";
						$("#penggunaan_barang_tanggal").addClass("error_field");
					}
					if (nama_barang == "") {
						valid = false;
						invalid_msg += "<br/><strong>Barang</strong> tidak boleh kosong";
						$("#penggunaan_barang_nama_barang").addClass("error_field");
					}
					if (jumlah == "") {
						valid = false;
						invalid_msg += "<br/><strong>Jumlah</strong> tidak boleh kosong";
						$("#penggunaan_barang_jumlah").addClass("error_field");
					} else if (!is_numeric(jumlah)) {
						valid = false;
						invalid_msg += "<br/><strong>Jumlah</strong> seharusnya numerik (0-9)";
						$("#penggunaan_barang_jumlah").addClass("error_field");
					} else if (parseFloat(jumlah) > parseFloat(sisa)) {
						valid = false;
						invalid_msg += "<br/><strong>Jumlah</strong> melebihi stok";
						$("#penggunaan_barang_jumlah").addClass("error_field");
					}
					if (keterangan == "") {
						valid = false;
						invalid_msg += "<br/><strong>Keterangan</strong> tidak boleh kosong";
						$("#penggunaan_barang_keterangan").addClass("error_field");
					}
					if (!valid) {
						$("#modal_alert_penggunaan_barang_add_form").html(
							"<div class='alert alert-block alert-danger'>" +
								"<h4>Peringatan</h4>" +
								invalid_msg +
							"</div>"
						);
					}
					return valid;
				};
				PenggunaanBarangAction.prototype.save = function() {
					if (!this.validate()) {
						return;
					}
					$("#penggunaan_barang_add_form").smodal("hide");
					showLoading();
					var self = this;
					var data = this.getRegulerData();		
					data['command'] = "save";
					data['id'] = $("#penggunaan_barang_id").val();
					data['id_stok_barang'] = $("#penggunaan_barang_id_stok_barang").val();
					data['tanggal'] = $("#penggunaan_barang_tanggal").val();
					data['jumlah'] = $("#penggunaan_barang_jumlah").val();
					data['keterangan'] = $("#penggunaan_barang_keterangan").val();
					$.post(
						"",
						data,
						function(response) {
							var json = getContent(response);
							if (json == null) {
								$("#penggunaan_barang_add_form").smodal("show");
							} else {
								self.view();
							}
							dismissLoading();
						}
					);
				};
				PenggunaanBarangAction.prototype.detail = function(id) {
					var data = this.getRegulerData();		
					data['command'] = "edit";
					data['id'] = id;
					$.post(
						"",
						data,
						function(response) {
							var json = getContent(response);
							if (json == null) return;
							$("#penggunaan_barang_id").val("");
							$("#penggunaan_barang_tanggal").val(json.tanggal);
							$("#penggunaan_barang_tanggal").removeAttr("disabled");
							$("#penggunaan_barang_tanggal").attr("disabled", "disabled");
							$("#penggunaan_barang_id_stok_barang").val(json.id_stok_barang);
							$("#penggunaan_barang_nama_barang").val(json.nama_barang);
							$("#penggunaan_barang_nama_barang").removeAttr("disabled");
							$("#penggunaan_barang_nama_barang").attr("disabled", "disabled");
							$("#penggunaan_barang_chooser_nama_barang").removeAttr("onclick");
							$("#penggunaan_barang_chooser_nama_barang").removeClass("btn-info");
							$("#penggunaan_barang_chooser_nama_barang").removeClass("btn-inverse");
							$("#penggunaan_barang_chooser_nama_barang").addClass("btn-inverse");
							$("#penggunaan_barang_sisa").val(json.sisa);
							$("#penggunaan_barang_f_sisa").val(json.sisa + " " + json.satuan);
							$(".penggunaan_barang_f_sisa").hide();
							$("#penggunaan_barang_jumlah").val(json.jumlah);
							$("#penggunaan_barang_jumlah").removeAttr("disabled");
							$("#penggunaan_barang_jumlah").attr("disabled", "disabled");
							$("#penggunaan_barang_satuan").val(json.satuan);
							$("#penggunaan_barang_keterangan").val(json.keterangan);
							$("#penggunaan_barang_keterangan").attr("disabled", "disabled");
							$("#modal_alert_penggunaan_barang_add_form").html();
							$(".error_field").removeClass("error_field");
							$("#penggunaan_barang_save").hide();
							$("#penggunaan_barang_save").removeAttr("onclick");
							$("#penggunaan_barang_ok").show();
							$("#penggunaan_barang_add_form").smodal("show");
						}
					);
				};
				
				function BarangAction(name, page, action, column) {
					this.initialize(name, page, action, column);
				}
				BarangAction.prototype.constructor = BarangAction;
				BarangAction.prototype = new TableAction();
				BarangAction.prototype.selected = function(json) {
					$("#penggunaan_barang_id_stok_barang").val(json.id);
					$("#penggunaan_barang_nama_barang").val(json.nama_barang);
					$("#penggunaan_barang_sisa").val(json.sisa);
					$("#penggunaan_barang_f_sisa").val(json.sisa + " " + json.satuan);
					$("#penggunaan_barang_satuan").val(json.satuan);
				};
				
				var penggunaan_barang;
				var PENGGUNAAN_BARANG_PNAME="<?php echo $this->proto_name; ?>";
				var PENGGUNAAN_BARANG_PSLUG="<?php echo $this->proto_slug; ?>";
				var PENGGUNAAN_BARANG_PIMPL="<?php echo $this->proto_implement; ?>";
				var PENGGUNAAN_BARANG_ENTITY="<?php echo $this->page; ?>";
				var penggunaan_barang_barang;
				$(document).ready(function() {
					$('[data-toggle="popover"]').popover({
						trigger: 'hover',
						'placement': 'top'
					});
					$(".mydate").datepicker();
					$("#smis-chooser-modal").on("show", function() {
						$("#smis-chooser-modal").removeClass("half_model");
						$("#smis-chooser-modal").addClass("half_model");
					});
					$("#smis-chooser-modal").on("hidden", function() {
						$("#smis-chooser-modal").removeClass("half_model");
					});
					penggunaan_barang_barang = new BarangAction("penggunaan_barang_barang",PENGGUNAAN_BARANG_ENTITY,"penggunaan_barang",new Array());
					penggunaan_barang_barang.setSuperCommand("penggunaan_barang_barang");
					penggunaan_barang_barang.setPrototipe(PENGGUNAAN_BARANG_PNAME,PENGGUNAAN_BARANG_PSLUG,PENGGUNAAN_BARANG_PIMPL);
					var penggunaan_barang_columns = new Array("id", "id_stok_barang", "tanggal", "jumlah", "keterangan");
					penggunaan_barang = new PenggunaanBarangAction("penggunaan_barang",PENGGUNAAN_BARANG_ENTITY,"penggunaan_barang",penggunaan_barang_columns);
					penggunaan_barang.setPrototipe(PENGGUNAAN_BARANG_PNAME,PENGGUNAAN_BARANG_PSLUG,PENGGUNAAN_BARANG_PIMPL);
					penggunaan_barang.view();
				});
			</script>
			<?php 
		}	
	}
?>