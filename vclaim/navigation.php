<?php 
	global $NAVIGATOR;
	$mr = new Menu("fa fa-address-book-o");
	$mr->addProperty('title','Bridgin VClaim');
	$mr->addProperty('name','Bridging VClaim');
	$mr->addSubMenu("Rencana Kontrol / Inap","vclaim","kontrol","","fa fa-wheelchair");
	$mr->addSubMenu("Data SEP","vclaim","dataseplist","","fa fa-wheelchair");
	$mr->addSubMenu("Data SEP Internal","vclaim","datasepinternallist","","fa fa-wheelchair");
	$mr->addSubMenu("Data Kunjungan","vclaim","datakunjungan","","fa fa-wheelchair");

	$mr->addSubMenu("Buat Rujukan","vclaim","rujukan","","fa fa-forward");
	$mr->addSubMenu("Update Rujukan","vclaim","list_rujukan","","fa fa-forward");
	$mr->addSubMenu("Rujukan Khusus","vclaim","internal","","fa fa-wheelchair");

	$mr->addSubMenu("Pengajuan Penjaminan","vclaim","penjaminan","","fa fa-wheelchair");
	$mr->addSubMenu("Approval Penjaminan","vclaim","aproval_penjaminan","","fa fa-wheelchair");
	$mr->addSubMenu("PRB","vclaim","prb","","fa fa-wheelchair");
	$NAVIGATOR->addMenu($mr,'vclaim');
?>
