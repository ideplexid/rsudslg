<?php
	global $db;
	global $user;

	if($_POST['action']=="kontrol"){
		require_once "vclaim/modul/kontrol.php";
		return;
	}

	require_once 'smis-libs-class/Policy.php';	
	$policy	= new Policy("vclaim", $user);
	$policy	->setDefaultPolicy(Policy::$DEFAULT_POLICY_ALLOW);
    $policy->addPolicy("kontrol","kontrol",Policy::$DEFAULT_COOKIE_CHANGE,"modul/kontrol");
	$policy->addPolicy("dataseplist","dataseplist",Policy::$DEFAULT_COOKIE_CHANGE,"modul/dataseplist");
	$policy->addPolicy("datasep","dataseplist",Policy::$DEFAULT_COOKIE_CHANGE,"resource/php/dataseplist/datasep");
	$policy->addPolicy("print_vklaim", "dataseplist", Policy::$DEFAULT_COOKIE_KEEP,"../registration/snippet/print_vklaim");
	$policy->addPolicy("datakunjungan", "datakunjungan", Policy::$DEFAULT_COOKIE_CHANGE,"modul/datakunjungan");
	$policy->addPolicy("datasepinternallist","datasepinternallist",Policy::$DEFAULT_COOKIE_CHANGE,"modul/datasepinternallist");
	
	$policy->addPolicy("internal","internal",Policy::$DEFAULT_COOKIE_CHANGE,"modul/internal");
	$policy->addPolicy("internal_insert","internal",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/internal/internal_insert");
	$policy->addPolicy("internal_list","internal",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/internal/internal_list");
	$policy->addPolicy("rujukan","rujukan",Policy::$DEFAULT_COOKIE_KEEP,"modul/rujukan");
	
	$policy->addPolicy("list_rujukan","list_rujukan",Policy::$DEFAULT_COOKIE_CHANGE,"modul/list_rujukan");
	$policy->addPolicy("update_rujukan","list_rujukan",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/list_rujukan/update_rujukan");
	$policy->addPolicy("datalist_rujukan","list_rujukan",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/list_rujukan/datalist_rujukan");

	$policy->addPolicy("prb","prb",Policy::$DEFAULT_COOKIE_CHANGE,"modul/prb");
	$policy->addPolicy("halaman_awal","prb",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/prb/halaman_awal");
	$policy->addPolicy("tambah_prb","prb",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/prb/tambah_prb");
	$policy->addPolicy("edit_prb","prb",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/prb/edit_prb");
	


	$policy->addPolicy("penjaminan","penjaminan",Policy::$DEFAULT_COOKIE_CHANGE,"modul/penjaminan");
	$policy->addPolicy("aproval_penjaminan","aproval_penjaminan",Policy::$DEFAULT_COOKIE_CHANGE,"modul/aproval_penjaminan");

	$policy	->initialize();
?>
