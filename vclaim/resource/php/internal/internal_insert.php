<?php 
/**
 * this page used to control and registration
 * of patient BPJS so usign VClaim system
 * 
 * @author      : Nurul Huda
 * @since       : 17 Feb 2018
 * @license     : LGPv2
 * @copyright   : goblooge@gmail.com
 * @version     : 1.2.1
 * @database    : - smis_rg_sep
 * 
 */
global $db;
$uitable            = new Table("");
$uitable->setName("internal_insert");

if(isset($_POST['super_command']) && $_POST['super_command']!=""){
    $super          = new SuperCommand ();
    require_once "vclaim/class/adapter/KodeAdapter.php";
    if($_POST['super_command']=="internal_insert_icd_primer" || $_POST['super_command']=="internal_insert_icd_sekunder"){
        $adapt = new KodeAdapter();
        $adapt->setUseNumber(true,"No.","back.");
        $adapt->add("Nama","nama");
        $adapt->add("Kode","kode");
        $table = new Table(array("No.","Nama","Kode"));
        $table->setName($_POST['super_command']);
        $table->setModel(Table::$SELECT);
        require_once "registration/class/responder/VclaimReffDiagnosa.php";
        $super->addResponder ( $_POST['super_command'], new VclaimReffDiagnosa(new DBTable($db,"smis_rg_patient",array("id")),$table,$adapt) );
    }else if($_POST['super_command']=="internal_insert_procedure" ){
        $adapt = new KodeAdapter();
        $adapt->setUseNumber(true,"No.","back.");
        $adapt->add("Nama","nama");
        $adapt->add("Kode","kode");
        $table = new Table(array("No.","Nama","Kode"));
        $table->setName($_POST['super_command']);
        $table->setModel(Table::$SELECT);
        require_once "vclaim/class/responder/VclaimReffProsedure.php";
        $super->addResponder ( $_POST['super_command'], new VclaimReffProsedure(new DBTable($db,"smis_rg_patient",array("id")),$table,$adapt) );
    }

    $init           = $super->initialize ();
    if ($init != null) {
        echo $init;
        return;
    }
}

if(isset($_POST['command']) &&  $_POST['command']!=""){
    require_once "vclaim/class/responder/VClaimInternalResponder.php";
    $dbtable    = new DBTable($db,"smis_rg_sep");
    $adapter    = new SimpleAdapter();
    $dbres      = new VClaimInternalResponder($dbtable,$uitable,$adapter);
    $data       = $dbres->command($_POST['command']);
    echo json_encode($data);
    return;
}
//020500011121P000002
$uitable->addModal("noRujukan","text","No. Rujukan","","y",null);
$form2 = $uitable->getModal()->getForm();

$button = new Button("","","Cari");
$button->setClass("btn btn-inverse");
$button->setIsButton(Button::$ICONIC_TEXT);
$button->setIcon(" fa fa-id-card");
$button->setAction("internal_insert.cari()");
$form2 ->addElement("",$button);

$uitable        ->clearContent();
$uitable ->addModal("noSep","hidden","","","n",null)
         ->addModal("diagnosaPrimer","hidden","","","n",null)
         ->addModal("diagnosaSekunder","hidden","","","n",null)
         ->addModal("procedure","hidden","","","n",null)         
         ->addModal("namadiagnosaPrimer","chooser-internal_insert-internal_insert_icd_primer-ICD 10 Diagnosa Primer","Diagnosa Primer","","n",null)
         ->addModal("namadiagnosaSekunder","chooser-internal_insert-internal_insert_icd_sekunder-ICD 10 Diagnosa Primer","Diagnosa Sekunder","","n",null)
         ->addModal("namaprocedure","chooser-internal_insert-internal_insert_procedure-ICD 9 Prosedur","Prosedur","","n",null)
         ->addModal("user","hidden","",$bpjs_user,"n",null,true);
$formsep        = $uitable->getModal()->setComponentSize(Modal::$MEDIUM)->getForm();
$uitable->clearContent();


$formx = new Form("","","");
$simpan     = new Button("update_sep","","Simpan");
$simpan     ->setAction("internal_insert.simpan()")
            ->setIsButton(Button::$ICONIC_TEXT)
            ->setIcon(" fa fa-save")
            ->setClass(" btn btn-primary");

$cetak     = new Button("","","Cetak");
$cetak     ->setAction("internal_insert.cetak()")
            ->setIsButton(Button::$ICONIC_TEXT)
            ->setIcon(" fa fa-print")
            ->setClass(" btn btn-warning");

$btngroup       = new ButtonGroup("");
$btngroup       ->addButton($simpan)
                ->addButton($cetak);

$formx      ->addElement("",$btngroup);

$rows           = new RowSpan();
$rows           ->addSpan("
                            <h5>BUAT RUJUKAN KHUSUS</h5>
                            ".$form2->getHtml()."
                            <div class='clear'></div>
                            <div>
                                ".$formsep->getHtml()." 
                                <div class='clear cline'></div>
                                ".$formx->getHtml()." 
                            </div>",7)
                ->addSpan("<h5>DATA PESERTA BPJS</h5><div class='clear'></div><div id='internal_insert_page'>Silakan Masukan No. BPJS atau No. KTP dibagian Pencarian dan Hasilnya akan di tampilkan !!!</div>",5);

echo "<div class='clear'></div>";
echo $rows->getHtml();
echo addJS  ("framework/bootstrap/js/bootstrap-datetimepicker.js");
echo addCSS ("framework/bootstrap/css/bootstrap-datetimepicker.css");
echo addJS  ("framework/bootstrap/js/bootstrap-datepicker.js");
echo addCSS ("framework/bootstrap/css/datepicker.css");

echo addJS  ("framework/smis/js/table_action.js");
echo addJS  ("vclaim/resource/js/internal_insert.js",false);
echo addCSS ("registration/resource/css/internal_insert.css",false);
?>