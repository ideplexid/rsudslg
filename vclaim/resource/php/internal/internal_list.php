<?php 

global $db;
require_once "smis-base/smis-include-duplicate.php";
$head=array ('No.','ID Rujukan','No. Rujukan',"No. Kartu","Nama Peserta","Diagnosa","Tgl. Rujukan Awal","Tgl. Rujukan Akhir");
$uitable = new Table ( $head, "", NULL, true );
$uitable->setName ( "internal_list" );
$uitable->setFooterVisible(false);
$uitable->setPrintButtonEnable(false);
$uitable->setReloadButtonEnable(false);
$uitable->setPrintElementButtonEnable(true);
$uitable->setEditButtonEnable(false);
$uitable->setAddButtonEnable(false);

if (isset ( $_POST ['command'] )) {
	$adapter = new SimpleAdapter ();
    $adapter->setUseNumber(true,"No.","back.");
	$adapter->add ( "ID Rujukan", "idrujukan" );
	$adapter->add ( "No. Rujukan", "norujukan" );
	$adapter->add ( "No. Kartu", "nokapst" );
	$adapter->add ( "Nama Peserta", "nmpst" );
	$adapter->add ( "Diagnosa", "diagppk" );
    $adapter->add ( "Tgl. Rujukan Awal", "tglrujukan_awal","date d M Y" );
    $adapter->add ( "Tgl. Rujukan Akhir", "tglrujukan_berakhir","date d M Y" );

	$dbtable = new DBTable ( $db, "smis_hrd_job" );
	$dbtable->setFetchMethode ( DBTable::$ARRAY_FETCH );
	require_once "vclaim/class/responder/VClaimInternalResponder.php";
    $dbres = new VClaimInternalResponder ( $dbtable, $uitable, $adapter );
	$data = $dbres->command ( $_POST ['command'] );
	echo json_encode ( $data );
	return;
}
$th = date("Y")*1;
$tahun = new OptionBuilder();
$tahun->add($th,$th,1);
$th--;
$tahun->add($th,$th,1);
$th--;
$tahun->add($th,$th,1);
$th--;
$tahun->add($th,$th,1);
$th--;
$tahun->add($th,$th,1);

$bulan = new OptionBuilder();
$bulan->add("Januari","1");
$bulan->add("Februari","2");
$bulan->add("Maret","3");
$bulan->add("April","4");
$bulan->add("Mei","5");
$bulan->add("Juni","6");
$bulan->add("Juli","7");
$bulan->add("Agustus","8");
$bulan->add("September","9");
$bulan->add("Oktober","10");
$bulan->add("November","11");
$bulan->add("Desember","12");

$uitable->addModal ( "s_tahun", "select", "Tahun", $tahun->getContent() );
$uitable->addModal ( "s_bulan", "select", "Bulan", $bulan->getContent() );
$form = $uitable->getModal()->getForm();



$button = new Button("","","Cari");
$button->setIsButton(Button::$ICONIC_TEXT);
$button->setIcon("fa fa-search");
$button->setAction("internal_list.view()");
$button->setClass("btn btn-primary");
$form->addElement("",$button);

$modal = $uitable->getModal ();
$modal->setTitle ( "Rujukan Khusus" );
$modal->setModalSize(Modal::$HALF_MODEL);
$modal->setComponentSize(Modal::$MEDIUM);

$form->setTitle("Rujukan Khusus");
echo $form->getHtml();
echo $uitable->getHtml ();
echo $modal->getHtml ();
echo addJS ( "base-js/smis-base-loading.js");
echo addJS ( "vclaim/resource/js/internal_list.js",false );