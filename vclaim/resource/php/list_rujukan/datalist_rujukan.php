<?php
    global $db;
    show_error();
	$header=array ('No. BPJS',"No. SEP","Tanggal Rujukan","No. Rujukan",'Nama',"Jenis Pelayanan","PPK Dirujuk","Nama PPK");
	$uitable = new Table ( $header , "", NULL, true );
	$uitable->setName ( 'datalist_rujukan' );
	$uitable->setAddButtonEnable(false);
    $uitable->setEditButtonEnable(false);
    $uitable->setReloadButtonEnable(false);
    $uitable->setPrintButtonEnable(false);
    
    

	if (isset ( $_POST ['command'] )) {
        require_once "vclaim/class/responder/VClaimDataListRujukanResponder.php";
		$adapter = new SimpleAdapter();
		$adapter ->add("id", "noRujukan")
                 ->add("Nama", "nama")
				 ->add("No. BPJS", "noKartu")
                 ->add("No. SEP", "noSep")
                 ->add("No. Rujukan", "noRujukan")
                 ->add("PPK Dirujuk", "ppkDirujuk")
                 ->add("Nama PPK", "namaPpkDirujuk")
				 ->add("Tanggal Rujukan", "tglRujukan","date d M Y")
				 ->add("Jenis Pelayanan", "jnsPelayanan","trivial_1_Rawat Inap_Rawat Jalan")
				 ->add("Jenis Pengajuan", "jnsPengajuan","trivial_1_Back Date_Fingerprint")
				 ->add("Keterangan", "keterangan")
                 ->add("Status", "status","trivial_0_Belum di Approve_Suda di Approve");
		$dbtable = new DBTable ( $db, 'smis_vclaim_penjaminan' );
		$dbresponder = new VClaimDataListRujukanResponder ( $dbtable, $uitable, $adapter );
		$datapasien = $dbresponder->command ( $_POST ['command'] );
		echo json_encode ( $datapasien );
		return;
	}
	
    $cetak     = new Button("","","View");
    $cetak     ->setAction("datalist_rujukan.view()")
                ->setIsButton(Button::$ICONIC_TEXT)
                ->setIcon(" fa fa-search")
                ->setClass(" btn btn-warning");

    $uitable->addModal("dari","date","Dari","");
    $uitable->addModal("sampai","date","Sampai","");
    $form = $uitable->getModal()->getForm();
    $form->setTitle("Data List Rujukan");
    $form->addElement("",$cetak);
	
    $uitable->setFooterVisible(false);
    echo $form->getHtml();
	echo $uitable->getHtml ();
	echo addJS ( "framework/smis/js/table_action.js" );
	echo addCSS ( "framework/bootstrap/css/datepicker.css" );
	echo addJS ( "framework/bootstrap/js/bootstrap-datepicker.js" );

?>
<script type="text/javascript">
var datalist_rujukan;
$(document).ready(function(){
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
	$('.mydate').datepicker();
	var column=new Array('id');
	datalist_rujukan=new TableAction("datalist_rujukan","vclaim","datalist_rujukan",column);
    datalist_rujukan.addViewData = function(x){
        x['dari'] = datalist_rujukan.get("dari");
        x['sampai'] = datalist_rujukan.get("sampai");
        console.log(x);
        return x;
    }
});
</script>
