<?php 

global $db;
require_once "smis-base/smis-include-duplicate.php";
$head=array ('No.','No. SRB','No. Kartu',"Nama Peserta","Program PRB","No. Sep");
$uitable = new Table ( $head, "", NULL, true );
$uitable->setName ( "halaman_awal" );
$uitable->setFooterVisible(false);
$uitable->setReloadButtonEnable(false);
$uitable->setPrintButtonEnable(false);

if (isset ( $_POST ['command'] )) {
	$adapter = new SimpleAdapter ();
    $adapter->setUseNumber(true,"No.","back.");
	$adapter->add ( "No. SRB", "noSRB" );
	$adapter->add ( "No. Kartu", "noKartu" );
	$adapter->add ( "Nama Peserta", "nama" );
	$adapter->add ( "Program PRB", "programPRB" );
	$adapter->add ( "No. Sep", "noSEP" );
	
	$dbtable = new DBTable ( $db, "smis_hrd_job" );
	$dbtable->setFetchMethode ( DBTable::$ARRAY_FETCH );
	require_once "vclaim/class/responder/PRBResponder.php";
    $dbres = new PRBResponder ( $dbtable, $uitable, $adapter );
	$data = $dbres->command ( $_POST ['command'] );
	echo json_encode ( $data );
	return;
}

$uitable->addModal ( "dari", "date", "Dari", "" );
$uitable->addModal ( "sampai", "date", "Sampai", "" );
$form = $uitable->getModal()->getForm();



$button = new Button("","","Cari");
$button->setIsButton(Button::$ICONIC_TEXT);
$button->setIcon("fa fa-search");
$button->setAction("halaman_awal.view()");
$button->setClass("btn btn-primary");
$form->addElement("",$button);

$modal = $uitable->getModal ();
$modal->setTitle ( "PRB" );
$modal->setModalSize(Modal::$HALF_MODEL);
$modal->setComponentSize(Modal::$MEDIUM);

$form->setTitle("PRB");
echo $form->getHtml();
echo $uitable->getHtml ();
echo $modal->getHtml ();
echo addJS ( "framework/bootstrap/js/bootstrap-datepicker.js" );
echo addCSS ( "framework/bootstrap/css/datepicker.css" );
echo addJS ( "vclaim/resource/js/halaman_awal.js",false );
echo addCSS ("vclaim/resource/css/halaman_awal.css", false);