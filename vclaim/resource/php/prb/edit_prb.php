<?php 
/**
 * VClaim PRB
 * 
 * @author      : Fulan
 * @since       : 09-02-2022
 * @license     : LGPv2
 * @copyright   : -
 * @version     : 1.0.0
 * @database    : -
 * 
 */
global $db;
$uitable = new Table("");
$uitable->setName("edit_prb");

if (isset($_POST['super_command']) && $_POST['super_command'] != "") {
    $super = new SuperCommand();
    if ($_POST['super_command'] == "edit_prb_sep") {
        $dbtable    = new DBTable($db, "smis_rg_sep");
        $adapter    = new SimpleAdapter();
        require_once "vclaim/class/responder/VClaimSEPResponder.php";
        $super->addResponder($_POST['super_command'], new VClaimSEPResponder($dbtable, $uitable, $adapter));
    } else if ($_POST['super_command'] == "edit_prb_dpjp") {
        $adapt = new SimpleAdapter();
        $adapt->setUseNumber(true, "No.", "back.");
        $adapt->add("Nama", "nama");
        $adapt->add("id", "kode");
        $adapt->add("Kode", "kode");
        $table = new Table(array("No.", "Nama", "Kode"));
        $table->setName($_POST['super_command']);
        $table->setModel(Table::$SELECT);
        require_once "registration/class/responder/VClaimReffDokter.php";
        $super->addResponder($_POST['super_command'], new VClaimReffDokter(new DBTable($db, "smis_rg_sep", array("id")), $table, $adapt));
    } else if ($_POST['super_command'] == "edit_prb_obat") {
        $adapt = new SimpleAdapter();
        $adapt->setUseNumber(true, "No.", "back.");
        $adapt->add("Nama", "nama");
        $adapt->add("id", "kode");
        $adapt->add("Kode", "kode");
        $table = new Table(array("No.", "Nama", "Kode"));
        $table->setName($_POST['super_command']);
        $table->setModel(Table::$SELECT);
        require_once "vclaim/class/responder/VClaimReffObatGenerik.php";
        $super->addResponder($_POST['super_command'], new VClaimReffObatGenerik(new DBTable($db, "smis_rg_sep", array("id")), $table, $adapt));
    }
    $init = $super->initialize();
    if ($init != null) {
        echo $init;
        return;
    }
}

if (isset($_POST['command'])) {
    $dbtable    = new DBTable($db, "smis_rg_sep");
    $adapter    = new SimpleAdapter();
    require_once "vclaim/class/responder/VClaimPRBResponder.php";
    $dbresponder = new VClaimPRBResponder($dbtable,$uitable,$adapter);
    $data = $dbresponder->command($_POST['command']);
    echo json_encode($data);
    return;
}

$ppkPelayanan   = getSettings($db,"reg-bpjs-vclaim-api-ppk-pelayanan","");
$bpjs_user      = getSettings($db,"reg-bpjs-vclaim-api-user","smis");

require_once "vclaim/class/responder/VClaimReffDiagnosaPRB.php";
$diagnosaReffPRB = new VClaimReffDiagnosaPRB(
    new DBTable($db, "smis_rg_sep"),
    new Table(array(), ""),
    new SimpleAdapter()
);
$program_prb_option = $diagnosaReffPRB->command("get_option");

$uitable->addModal("jenisSep","hidden","","sep");
$uitable->addModal("searchNoSep","text","No. SEP","","y",null);
$form1 = $uitable->getModal()->getForm();
$button = new Button("","","Cari");
$button->setClass("btn btn-inverse");
$button->setAction("edit_prb.cariSEP()");
$form1 ->addElement("",$button);
$uitable->clearContent();
$uitable    ->addModal("noSep","hidden","","","n",null,false)
            ->addModal("noKartu","hidden","","","n",null,false)
            ->addModal("jnsPelayanan", "hidden", "", "", "n", null, false)
            ->addModal("poliTujuan", "hidden", "", "", "n", null, false)
            ->addModal("noSRB", "text", "No. SRB", "", "y", null, true)
            ->addModal("tglSRB", "date", "Tgl. Rujuk Balik", "")
            ->addModal("alamat","text","Alamat Peserta","","n",null,false)
            ->addModal("email","text","Email Peserta","","n",null,false)
            ->addModal("programPRB","select","Program PRB",$program_prb_option->getContent(),"n",null,false)
            ->addModal("kodeDPJP","chooser-edit_prb-edit_prb_dpjp-DPJP","DPJP","","n",null)
            ->addModal("keterangan","text","Keterangan","","n",null)
            ->addModal("saran","text","Saran","","n",null);
$form2 = $uitable->getModal()->getForm();
$uitable->clearContent();

$uitable    ->addModal("kdObat","text","Kode Obat","","n",null,true)
            ->addModal("nmObat","chooser-edit_prb-edit_prb_obat-Obat","Nama Obat","","n",null,true)
            ->addModal("signa1", "text", "Signa 1", "", "n", null, false)
            ->addModal("signa2", "text", "Signa 2", "", "n", null, false)
            ->addModal("jmlObat", "text", "Jumlah", "", "n", null, false);
$form3 = $uitable->getModal()->getForm();
$button = new Button("", "", "Tambahkan");
$button->setClass("btn btn-inverse");
$button->setAction("edit_prb.addObat()");
$form3->addElement("", $button);
$uitable->clearContent();

$obat_table = new Table(
    array("Kode Obat", "Nama Obat", "S1", "S2", "Jumlah"),
    ""
);
$obat_table->setName("edit_prb_dobat");
$obat_table->setAddButtonEnable(false);
$obat_table->setReloadButtonEnable(false);
$obat_table->setPrintButtonEnable(false);
$obat_table->setFooterVisible(false);

$button = new Button("", "", "Simpan");
$button->setClass("btn btn-success");
$button->setAction("edit_prb.save()");

$rows   = new RowSpan();
$rows   ->addSpan("
            ".$form1->getHtml()."
            <div class='clear'></div>
            <div>
                ".$form2->getHtml()." 
                <div class='ranap clear cline'></div>
                <h4 class='ranap'>Obat</h4>
                ".$form3->getHtml()."
                ".$obat_table->getHtml()."
                <div class='clear cline'></div>
                <div align='right'>".$button->getHtml()."</div>
            </div>",7)
        ->addSpan("<h4>DATA PESERTA BPJS</h4><div class='clear'></div><div id='edit_prb_page'>Silakan Masukan No. BPJS atau No. KTP dibagian Pencarian dan Hasilnya akan di tampilkan !!!</div>",5);

echo $rows->getHtml();
echo addJS  ("framework/bootstrap/js/bootstrap-datepicker.js");
echo addCSS ("framework/bootstrap/css/datepicker.css");

echo addJS  ("framework/smis/js/table_action.js");
echo addJS("vclaim/resource/js/edit_prb.js", false);
?>