<?php 
/**
 * this page used to control and registration
 * of patient BPJS so usign VClaim system
 * 
 * @author      : Nurul Huda
 * @since       : 17 Feb 2018
 * @license     : LGPv2
 * @copyright   : goblooge@gmail.com
 * @version     : 1.2.1
 * @database    : - smis_rg_sep
 * 
 */
global $db;
$uitable            = new Table("");
$uitable->setName("datasep");

if(isset($_POST['super_command']) && $_POST['super_command']!=""){
    $super          = new SuperCommand ();

    if($_POST['super_command']=="datasep_icd"){
        $adapt = new SimpleAdapter();
        $adapt->setUseNumber(true,"No.","back.");
        $adapt->add("Nama","nama");
        $adapt->add("id","kode");
        $adapt->add("Kode","kode");
        $table = new Table(array("No.","Nama","Kode"));
        $table->setName($_POST['super_command']);
        $table->setModel(Table::$SELECT);
        require_once "registration/class/responder/VclaimReffDiagnosa.php";
        $super->addResponder ( $_POST['super_command'], new VclaimReffDiagnosa(new DBTable($db,"smis_rg_patient",array("id")),$table,$adapt) );
    }else  if($_POST['super_command']=="datasep_dokter" || $_POST['super_command']=="datasep_dpjp"){
        require_once "registration/class/adapter/PoliAdapter.php";
        $adapt = new PoliAdapter();
        $adapt->setUseNumber(true,"No.","back.");
        $adapt->add("Nama","nama");
        $adapt->add("id","kode");
        $adapt->add("Kode","kode");
        $table = new Table(array("No.","Nama","Kode"));
        $table->setName($_POST['super_command']);
        $table->setModel(Table::$SELECT);
        require_once "registration/class/responder/VClaimReffDokter.php";
        $super->addResponder ( $_POST['super_command'], new VClaimReffDokter(new DBTable($db,"smis_rg_patient",array("id")),$table,$adapt) );
    }else if($_POST['super_command']=="datasep_poli"){
        require_once "registration/class/adapter/PoliAdapter.php";
        $adapt1 = new PoliAdapter();
        $adapt1->setUseNumber(true,"No.","back.");
        $adapt1->add("Nama","nama");
        $adapt1->add("id","kode");
        $adapt1->add("Kode","kode");
        $table1 = new Table(array("No.","Nama","Kode"));
        $table1->setName("datasep_poli");
        $table1->setModel(Table::$SELECT);
        require_once "registration/class/responder/VClaimReffPoli.php";
        $super->addResponder ( "datasep_poli", new VClaimReffPoli(new DBTable($db,"smis_rg_patient",array("id")),$table1,$adapt1) );    
    }else if($_POST['super_command']=="datasep_provinsi"){
        $adapt1 = new SimpleAdapter();
        $adapt1->setUseNumber(true,"No.","back.");
        $adapt1->add("Nama","nama");
        $adapt1->add("id","kode");
        $adapt1->add("Kode","kode");
        $table1 = new Table(array("No.","Nama","Kode"));
        $table1->setFooterVisible(false);
        $table1->setName("datasep_provinsi");
        $table1->setModel(Table::$SELECT);
        require_once "registration/class/responder/VClaimReffProvinsi.php";
        $super->addResponder ( "datasep_provinsi", new VClaimReffProvinsi(new DBTable($db,"smis_rg_patient",array("id")),$table1,$adapt1) );    
    }else if($_POST['super_command']=="datasep_kabupaten"){
        $adapt1 = new SimpleAdapter();
        $adapt1->setUseNumber(true,"No.","back.");
        $adapt1->add("Nama","nama");
        $adapt1->add("id","kode");
        $adapt1->add("Kode","kode");
        $table1 = new Table(array("No.","Nama","Kode"));
        $table1->setFooterVisible(false);
        $table1->setName("datasep_kabupaten");
        $table1->setModel(Table::$SELECT);
        require_once "registration/class/responder/VClaimReffKabupaten.php";
        $super->addResponder ( "datasep_kabupaten", new VClaimReffKabupaten(new DBTable($db,"smis_rg_patient",array("id")),$table1,$adapt1) );    
    }else if($_POST['super_command']=="datasep_kecamatan"){
        $adapt1 = new SimpleAdapter();
        $adapt1->setUseNumber(true,"No.","back.");
        $adapt1->add("Nama","nama");
        $adapt1->add("id","kode");
        $adapt1->add("Kode","kode");
        $table1 = new Table(array("No.","Nama","Kode"));
        $table1->setFooterVisible(false);
        $table1->setName("datasep_kecamatan");
        $table1->setModel(Table::$SELECT);
        require_once "registration/class/responder/VClaimReffKecamatan.php";
        $super->addResponder ( "datasep_kecamatan", new VClaimReffKecamatan(new DBTable($db,"smis_rg_patient",array("id")),$table1,$adapt1) );    
    }else if($_POST['super_command']=="datasep_kecelakaan"){
        $adapt1 = new SimpleAdapter();
        $adapt1->setUseNumber(true,"No.","back.");
        $adapt1->add("No. Sep","noSEP");
        $adapt1->add("Tanggal Kejadian","tglKejadian","date d M Y");
        $adapt1->add("Provinsi","kdProp");
        $adapt1->add("Kabupaten","kdKab");
        $adapt1->add("Kecamatan","kdKec");
        $adapt1->add("Keterangan Kejadian","ketKejadian");
        
        $table1 = new Table(array("No. Sep","Tanggal Kejadian","Provinsi","Kabupaten","Kecamatan","Keterangan"));
        $table1->setFooterVisible(false);
        $table1->setName("datasep_kecelakaan");
        $table1->setModel(Table::$SELECT);
        require_once "registration/class/responder/VclaimReffKecelakaan.php";
        $super->addResponder ( "datasep_kecelakaan", new VclaimReffKecelakaan(new DBTable($db,"smis_rg_patient",array("id")),$table1,$adapt1) );    
    }
    
    $init           = $super->initialize ();
    if ($init != null) {
        echo $init;
        return;
    }
}

if(isset($_POST['command']) &&  $_POST['command']!=""){
    require_once "vclaim/class/responder/VClaimSEPResponder.php";
    $dbtable    = new DBTable($db,"smis_rg_sep");
    $adapter    = new SimpleAdapter();
    $dbres      = new VClaimSEPResponder($dbtable,$uitable,$adapter);
    $data       = $dbres->command($_POST['command']);
    echo json_encode($data);
    return;
}

$uitable        ->addModal("no_bpjs","text","No. BPJS","")
                ->addModal("no_ktp","text","No. KTP","");
$form           = $uitable->getModal()->getForm();

$cek            = new Button("datasep_button","datasep_button","Cek BPJS");
$cek            ->setAction("datasep.fetch_server()")
                ->setIsButton(Button::$ICONIC_TEXT)
                ->setIcon(" fa fa-id-card")
                ->setClass(" btn btn-primary");
$rujukan        = new Button("","","Cek Rujukan");
$rujukan        ->setAction("datasep.cek_rujukan()")
                ->setIsButton(Button::$ICONIC_TEXT)
                ->setIcon(" fa fa-arrows")
                ->setClass(" btn btn-success");
$reset          = new Button("","","Reset");
$reset          ->setAction("datasep.reset()")
                ->setIsButton(Button::$ICONIC_TEXT)
                ->setIcon(" fa fa-refresh")
                ->setClass(" btn btn-danger");
$grup           = new ButtonGroup("","","");
$grup           ->addButton($cek)
                ->addButton($rujukan)
                ->addButton($reset);
$form           ->addElement("",$grup);

$jenis          = new OptionBuilder();
$jenis          ->add("","",1)
                ->add("Rawat Inap","1",0)
                ->add("Rawat Jalan","2",0);

$polidb         = new DBTable($db,"smis_rg_refpolibpjs");
$polidb         ->addCustomKriteria("status","='1'")
                ->addCustomKriteria("grup","='poli'")
                ->setOrder(" nama ASC ",true)
                ->setShowAll(true);
$d              = $polidb->view("",0);
$list           = $d['data'];
$adapter        = new SelectAdapter("nama","kode");
$content        = $adapter->getContent($list);
array_push($content,array("name"=>"","value"=>"","default"=>1));

$kelas          = new OptionBuilder();
$kelas          ->add("","",1)
                ->add("Kelas I","1",0)
                ->add("Kelas II","2",0)
                ->add("Kelas III/Rawat Jalan","3",0);
$asal           = new OptionBuilder();
$asal           ->add("SEP","sep",1);

$cob            = new OptionBuilder();
$cob            ->add("Ya","1","0")
                ->add("Tidak","0","1");
$pjmn           = new OptionBuilder();
$pjmn           ->add("","","1")
                ->add("Jasa raharja ","1","0")
                ->add("BPJS Ketenagakerjaan","2","0")
                ->add("TASPEN PT","3","0")
                ->add("ASABRI PT","4","0");

$laka           = new OptionBuilder();
$laka           ->add("Bukan Kecelakaan lalu lintas [BKLL]","0",1)
                ->add("KLL dan bukan kecelakaan Kerja [BKK]","1",0)
                ->add("KLL dan KK","2",0)
                ->add("KK","3",0);

$ppkPelayanan   = getSettings($db,"reg-bpjs-vclaim-api-ppk-pelayanan","");
$bpjs_user      = getSettings($db,"reg-bpjs-vclaim-api-user","smis");
$uitable        ->clearContent();




$pencarian = new OptionBuilder();
$pencarian->add("Nomor Rujukan");
$pencarian->add("Nomor BPJS");
$pencarian->add("Nomor KTP");

$uitable->addModal("jenisSep","select","Jenis SEP",$asal->getContent(),"y",null);
$uitable->addModal("noSep","text","No. SEP",$_POST['nosep'],"y",null);
$form2 = $uitable->getModal()->getForm();

$button = new Button("df_pasien","","Daftar Pasien");
$button->setClass("btn btn-inverse hide");
$button->setIsButton(Button::$ICONIC_TEXT);
$button->setIcon(" fa fa-id-card");
$button->setAction("datasep.daftar_pasien()");
$form2 ->addElement("",$button);

$button = new Button("","","Cari");
$button->setClass("btn btn-inverse");
$button->setIsButton(Button::$ICONIC_TEXT);
$button->setIcon(" fa fa-id-card");
$button->setAction("datasep.cari()");
$form2 ->addElement("",$button);

$uitable        ->clearContent();

$naik = new OptionBuilder();
$naik->add("",0);
$naik->add("VVIP",1);
$naik->add("VIP",2);
$naik->add("Kelas 1",3);
$naik->add("Kelas 2",4);
$naik->add("Kelas 3",5);
$naik->add("ICCU",6);
$naik->add("ICU",7);


$tujuan = new OptionBuilder();
$tujuan->add("","",1);
$tujuan->add("Normal",0);
$tujuan->add("Prosedur",1);
$tujuan->add("Konsul Dokter",2);

$prosedur = new OptionBuilder();
$prosedur->add("","",1);
$prosedur->add("Prosedur Tidak Berkelanjutan",0);
$prosedur->add("Prosedur dan Terapi Berkelanjutan",1);


$penunjang = new OptionBuilder();
$penunjang->add("","",1);
$penunjang->add("Radioterapi",1);
$penunjang->add("Kemoterapi",2);
$penunjang->add("Rehabilitasi Medik",3);
$penunjang->add("Rehabilitasi Psikososial",4);
$penunjang->add("Transfusi Darah",5);
$penunjang->add("Pelayanan Gigi",6);
$penunjang->add("Laboratorium",7);
$penunjang->add("USG",8);
$penunjang->add("Farmasi",9);
$penunjang->add("Lain-Lain",10);
$penunjang->add("MRI",11);
$penunjang->add("HEMODIALISA",12);


$pembiayaan = new OptionBuilder();
$pembiayaan->add("","",1);
$pembiayaan->add("Pribadi",1);
$pembiayaan->add("Pemberi Kerja",2);
$pembiayaan->add("Asuransi Kesehatan Tambahan",3);

$katarak = new OptionBuilder();
$katarak->add("Tidak",0,1);
$katarak->add("Ya",1);

$suplesi = new OptionBuilder();
$suplesi->add("Tidak",0,1);
$suplesi->add("Ya",1);

$ass = new OptionBuilder();
$ass->add("Poli spesialis tidak tersedia pada hari sebelumnya,",1);
$ass->add("Jam Poli telah berakhir pada hari sebelumnya",2);
$ass->add("Dokter Spesialis yang dimaksud tidak praktek pada hari sebelumnya",3);
$ass->add("Atas Instruksi RS",4);
$ass->add("Tujuan Kontrol",5);

$uitable       
                ->addModal("id_pasien","hidden","","","n",null,true)
                ->addModal("noKartu","hidden","","","n",null,false)
                ->addModal("asalRujukan","hidden","","","n",null)
                ->addModal("jnsPelayanan","select","Jenis Layanan",$jenis->getContent(),"n",null)
                ->addModal("tglSep","datetime","Tgl SEP",date("Y-m-d H:i:s"),"n",null,false)
                ->addModal("eksekutif","checkbox","Eksekutif","","y",null)
                ->addModal("poliTujuan","hidden","","","n",null)
                ->addModal("namapoliTujuan","chooser-datasep-datasep_poli-Spesialis / Subspesialis","Spesialis / Subspesialis","","n",null)

                ->addModal("dpjpLayan","hidden","","","n",null)
                ->addModal("namadpjpLayan","chooser-datasep-datasep_dokter-DPJP","DPJP","","n",null)
                ->addModal("noMr","text","No. Mr","","n",null,false)
                ->addModal("tglRujukan","datetime","Tgl Rujukan",date("Y-m-d H:i:s"),"n",null,true)
                ->addModal("namaRujukan","text","Asal Rujukan","","y",null,true)
                ->addModal("ppkRujukan","text","PPK Rujukan","","y",null,true)
                ->addModal("noRujukan","text","No Rujukan","","y",null,true)
                ->addModal("diagAwal","chooser-datasep-datasep_icd-ICD 10 Diagnosa","Kode Diagnosa","","n",null)
                ->addModal("cob","checkbox","COB","","y",null)
                ->addModal("katarak","checkbox","Katarak","","n",null)

                ->addModal("catatan","text","Catatan","","y",null)
                ->addModal("noTelp","text","Telp Pasien","","n")
                
                ->addModal("tujuanKunj","select","Tujuan Kunj.",$tujuan->getContent(),"n",null)
                ->addModal("flagProcedure","select","Prosedur",$prosedur->getContent(),"n",null)
                ->addModal("kdPenunjang","select","Penunjang",$penunjang->getContent(),"n",null)
                ->addModal("assesmentPel","select","Asessment Pelayanan",$ass->getContent(),"n",null);

$formsep        = $uitable->getModal()->setComponentSize(Modal::$MEDIUM)->getForm();
$uitable->clearContent();

$carapulang = new OptionBuilder();
$carapulang->add("Atas Persetujuan Dokter",1);
$carapulang->add("Atas Permintaan Sendiri",3);
$carapulang->add("Meninggal",4);
$carapulang->add("Lain-lain",5);

$uitable    ->addModal("statusPulang","select","Cara Pulang",$carapulang->getContent(),"n",null)
            ->addModal("noSuratMeninggal","text","No. Surat Meninggal","","n",null)
            ->addModal("tglPulang","date","Tanggal Pulang","","n",null)
            ->addModal("noLPManual","text","No. LP Manual","");
$formpulang  = $uitable->getModal()->setComponentSize(Modal::$MEDIUM)->getForm();
$uitable->clearContent();

$uitable    ->addModal("naikKelas","select","Naik Kelas",$naik->getContent(),"n",null)
            ->addModal("pembiayaan","select","Pembiayaan",$pembiayaan->getContent(),"n",null)
            ->addModal("penanggunjwb","text","Pngg. Jawab","");
$formranap  = $uitable->getModal()->setComponentSize(Modal::$MEDIUM)->getForm();
$uitable->clearContent();

$uitable    ->addModal("noSurat","text","No Surat Kontrol","","n",null)
            ->addModal("kodeDPJP","hidden","","","n",null)
            ->addModal("namaDPJP","chooser-datasep-datasep_dpjp-DPJP","DPJP","","n",null);
$formskdp  = $uitable->getModal()->setComponentSize(Modal::$MEDIUM)->getForm();
$uitable->clearContent();


$uitable    ->addModal("lakaLantas","select","Status Kecelakaan",$laka->getContent(),"n");
$formlaka1 = $uitable->getModal()->setComponentSize(Modal::$MEDIUM)->getForm();
$uitable->clearContent();

$uitable    ->addModal("tgl_kejadian","date","Tgl Kejadian","","n",null)
           
            ->addModal("no_suplesi","text","No. Suplesi","","n",null)
            ->addModal("lokasiLaka","text","Lokasi Laka","","y")
            ->addModal("kdPropinsi","chooser-datasep-datasep_provinsi-Provinsi","Provinsi","","n",null)
            ->addModal("kdKabupaten","chooser-datasep-datasep_kabupaten-Kabupaten","Kabupaten","","n",null)
            ->addModal("kdKecamatan","chooser-datasep-datasep_kecamatan-Kecamatan","Kecamatan","","n",null)
            ->addModal("keterangan","textarea","Keterangan","","n",null)

            ->addModal("suplesi","select","Suplesi",$suplesi->getContent(),"y",null)
            ->addModal("klsRawat","text","Kelas","","n",null)
            ->addModal("katarak","select","Katarak",$katarak->getContent(),"n",null)
            ->addModal("ppkPelayanan","hidden","",$ppkPelayanan,"n",null,true)
            ->addModal("user","hidden","",$bpjs_user,"n",null,true);
$formlaka2 = $uitable->getModal()->setComponentSize(Modal::$MEDIUM)->getForm();


$fingerprint            = new Button("","","Cek Fingerprint");
$fingerprint            ->setAction("datasep.fingerprint()")
                ->setIsButton(Button::$ICONIC_TEXT)
                ->setIcon(" fa fa-search")
                ->setClass(" btn btn-inverse");


$carilaka            = new Button("","","Cari");
$carilaka            ->setAction("datasep.cari_laka()")
                ->setIsButton(Button::$ICONIC_TEXT)
                ->setIcon(" fa fa-search")
                ->setClass(" btn btn-primary");
$formlaka1        ->addElement("",$carilaka);


$formx = new Form("","","");

$pulang     = new Button("update_tgl_pulang","","Update Tanggal Pulang");
$pulang     ->setAction("datasep.update_tgl_pulang()")
            ->setIsButton(Button::$ICONIC_TEXT)
            ->setIcon(" fa fa-upload")
            ->setClass(" btn btn-success");
$formx      ->addElement("",$pulang);

$pulang     = new Button("update_sep","","Update SEP");
$pulang     ->setAction("datasep.update_sep()")
            ->setIsButton(Button::$ICONIC_TEXT)
            ->setIcon(" fa fa-upload")
            ->setClass(" btn btn-primary");
$formx      ->addElement("",$pulang);

$pulang     = new Button("","","Hapus SEP");
$pulang     ->setAction("datasep.del()")
            ->setIsButton(Button::$ICONIC_TEXT)
            ->setIcon(" fa fa-upload")
            ->setClass(" btn btn-warning");
$formx      ->addElement("",$pulang);

$uitable->clearContent();
//$uitable->addModal("noSEP","text","No SEP","","n",null,true);
$formnosep      = $uitable->getModal()->setComponentSize(Modal::$MEDIUM)->getForm();
$print          = new Button("","","Cetak SEP");
$print          ->setAction("datasep.print_sep()")
                ->setIsButton(Button::$ICONIC_TEXT)
                ->setIcon(" fa fa-print")
                ->setClass(" btn btn-primary");
$formnosep      ->addElement("",$print);

$uitable->clearContent();
$uitable->addModal("noREG","text","No REG","","n",null,true);
$formnorereg    = $uitable->getModal()->setComponentSize(Modal::$MEDIUM)->getForm();
$reg            = new Button("","","Register Pasien");
$reg            ->setAction("datasep.register_pasien_bpjs()")
                ->setIsButton(Button::$ICONIC_TEXT)
                ->setIcon(" fa fa-sign-in")
                ->setClass(" btn btn-primary");
$fix            = new Button("","","Fix Register");
$fix            ->setAction("datasep.fix_register()")
                ->setIsButton(Button::$ICONIC_TEXT)
                ->setIcon(" fa fa-eraser")
                ->setClass(" btn btn-success");
$sep            = new Button("","","SEP Sementara");
$sep            ->setAction("datasep.sep_sementara()")
                ->setIsButton(Button::$ICONIC_TEXT)
                ->setIcon(" fa fa-tag")
                ->setClass(" btn btn-danger");
$btngroup       = new ButtonGroup("");
$btngroup       ->addButton($reg)
                ->addButton($fix);
$formnorereg    ->addElement("",$btngroup)
                ->addElement("",$sep);
$rows           = new RowSpan();
$rows           ->addSpan("
                            <h4>PENCARIAN PASIEN</h4>
                            ".$form2->getHtml()."
                            <div class='clear'></div>
                            <h4>Pembuatan SEP</h4>
                            <div>
                                ".$formsep->getHtml()." 
                                <div class='ranap clear cline'></div>
                                <h4 class='ranap'>Data Rawat Inap</h4>
                                <div class='ranap'>".$formranap->getHtml()."</div> 
                                <div class='clear cline'></div>

                                <h4>PULANG</h4>
                                ".$formpulang->getHtml()."
                                <div class='clear cline'></div>

                                <h4>SKDP</h4>
                                ".$formskdp->getHtml()." 
                                <div class='clear cline'></div>
                                <h4>DATA KECELAKAAN</h4>
                                ".$formlaka1->getHtml()." 
                                <div class='clear cline hide kclk'></div>
                                <div class='kclk hide'>".$formlaka2->getHtml()."</div> 

                                <div class='clear cline'></div>
                                ".$formx->getHtml()." 

                                <div class='hide fnosep'>".$formnosep->getHtml()."</div> 
                                <div class='clear'></div>
                                <div class='hide'>".$formnorereg->getHtml()."</div> 

                                <div class='clear cline finger hide'></div>
                                <h4 class='finger  hide'>DATA FINGERPRINT</h4>
                                <div class='finger  hide'>".$fingerprint->getHtml()."</div>
                                <div class='clear cline finger  hide' id='hasil-finger'></div>
                            </div>",7)
                ->addSpan("<h4>DATA PESERTA BPJS</h4><div class='clear'></div><div id='datasep_page'>Silakan Masukan No. BPJS atau No. KTP dibagian Pencarian dan Hasilnya akan di tampilkan !!!</div>",5);

                /*
$rows           ->addSpan("
                            <h5>PENCARIAN PASIEN</h5>
                            ".$form2->getHtml()."
                            <div class='clear'></div>
                            <h5>Pembuatan SEP</h5>
                            <div>
                                ".$formsep->getHtml()." 
                                <div class='clear cline'></div>
                                <h5>Data Rawat Inap</h5>
                                ".$formranap->getHtml()." 
                                <div class='clear cline'></div>
                                <h5>PULANG</h5>
                                ".$formpulang->getHtml()."
                                <div class='clear cline'></div>
                                <h5>SKDP</h5>
                                ".$formskdp->getHtml()." 
                                <div class='clear cline'></div>
                                <h5>DATA KECELAKAAN</h5>
                                ".$formlaka1->getHtml()." 
                                <div class='clear cline'></div>
                                ".$formlaka2->getHtml()." 
                                <div class='clear cline'></div>
                                ".$formx->getHtml()." 
                                <div class='hide'>".$formnosep->getHtml()."</div> 
                                <div class='clear'>
                            </div><div class='hide'>".$formnorereg->getHtml()."</div> </div>",7)
                ->addSpan("<h5>DATA PESERTA BPJS</h5><div class='clear'></div><div id='datasep_page'>Silakan Masukan No. BPJS atau No. KTP dibagian Pencarian dan Hasilnya akan di tampilkan !!!</div>",5);


$rows           ->addSpan("<h5>REGISTRASI BPJS/SEP</h5><div class='clear'></div><div>".$formsep->getHtml()." <div class='clear'></div>".$formnosep->getHtml()." <div class='clear'></div>".$formnorereg->getHtml()." </div>",7)
                ->addSpan("<h5>DATA PESERTA BPJS</h5><div class='clear'></div><div id='datasep_page'>Silakan Masukan No. BPJS atau No. KTP dibagian Pencarian dan Hasilnya akan di tampilkan !!!</div>",5);
*/
echo "<div class='hide'>".$form->getHtml()."</div>";
echo "<div class='clear'></div>";
echo $rows->getHtml();
echo addJS  ("framework/bootstrap/js/bootstrap-datetimepicker.js");
echo addCSS ("framework/bootstrap/css/bootstrap-datetimepicker.css");
echo addJS  ("framework/bootstrap/js/bootstrap-datepicker.js");
echo addCSS ("framework/bootstrap/css/datepicker.css");

echo addJS  ("framework/smis/js/table_action.js");
echo addJS  ("vclaim/resource/js/datasep.js",false);
echo addCSS ("vclaim/resource/css/datasep.css",false);
?>