/**
 * this page used to control and vclaim
 * of patient BPJS so usign VClaim system
 * 
 * @author      : Nurul Huda
 * @since       : 17 Feb 2018
 * @license     : LGPv2
 * @copyright   : goblooge@gmail.com
 * @version     : 1.0.1
 * @used        : vclaim/resource/php/vclaim_patient/internal_insert.php
 * 
 */
 var internal_insert;
 var internal_insert_icd_primer;
 var internal_insert_icd_sekunder;
 var internal_insert_procedure;
 var current_pasien;
 $(document).ready(function(){
     $(".mydate").datepicker();
     $(".mydatetime").datetimepicker({minuteStep:1});
     
     internal_insert = new TableAction("internal_insert","vclaim","internal_insert", new Array());     
     internal_insert.cari = function(x){
        var x = this.getRegulerData();
        x['command'] = "cari_rujukan";
        x['noRujukan'] = internal_insert.get("noRujukan");
        showLoading();
        $.post("",x,function(res){
            var j = getContent(res);
            console.log(j);
            if(j!=null){
                internal_insert.cari_sep(j.noSep);
            }
            dismissLoading();
        });  
     }

     internal_insert.cari_sep = function(nosep){
        var x = this.getRegulerData();
        x['command'] = "edit";
        x['noSep'] = nosep;
        x['jenisSep'] = rujukan.get("jenisSep");
        showLoading();
        $.post("",x,function(res){
            var j = getContent(res);
            $("#internal_insert_page").html(j.tbl);
            dismissLoading();
        });  
     }


     internal_insert.register_pasien_bpjs=function(){
         var nosep   = $("#"+this.prefix+"_noSEP").val();
         var noreg   = $("#"+this.prefix+"_noREG").val();
         var nrm     = $("#"+this.prefix+"_noMr").val();
         var nobpjs  = $("#"+this.prefix+"_noKartu").val();
         var kelas   = $("#"+this.prefix+"_klsRawat").val();
         if(nosep==""){
             showWarning("Peringatan !!!","Silakan Masukan Nomor BPJS Terlebih dahulu, pastikan pasien aktif, kemudian create SEP baru registrasi.");
             return;
         }        
         if(noreg=="0"){
             daftar_pasien.show_add_form_nosep(nrm,nosep,nobpjs,kelas);
         }else{
             daftar_pasien.edit_nosep(noreg,nosep,nobpjs,kelas);    
         }
     };
     
     internal_insert.cek_nomor_rm=function(id_pasien,nrm,ktp,telpon){
         internal_insert.reset();
         $("#internal_insert_id_pasien").val(id_pasien);
         $("#internal_insert_noMr").val(nrm);
         $("#internal_insert_no_ktp").val(ktp);
         $("#internal_insert_noTelp").val(telpon);
         $("#internal_insert_no_bpjs").val("");
         if(ktp=="" || ktp=="-"){
             var data        = this.getRegulerData();
             data['nrm']     = nrm;
             data['action']  = "search_no_bpjs";
             showLoading();
             $.post("",data,function(res){
                 dismissLoading();
                 var json=getContent(res);
                 if(json==""){
                     showWarning("No. BPJS/KTP/NIK Kosong","dikarenakan No. BPJS/KTP/NIK kosong, silakan isikan No. BPJS/KTP/NIK pasien yang bersangkutan");
                 }else{
                     $("#internal_insert_no_bpjs").val(json);
                     $("#internal_insert_button").trigger("click");
                 }
             });
         }else{
             $("#internal_insert_button").trigger("click");
         }
     };
 

     internal_insert.simpan=function(){
        data            = this.getRegulerData();
        data['noRujukan'] = internal_insert.get("noRujukan");
        data['diagnosaPrimer'] = internal_insert.get("diagnosaPrimer");
        data['diagnosaSekunder'] = internal_insert.get("diagnosaSekunder");
        data['procedure'] = internal_insert.get("procedure");
        data['command'] = "save";
        showLoading();
        $.post("",data,function(res){
              var json  = getContent(res);
              dismissLoading();
        });
    };
   
     
     internal_insert_icd_primer=new TableAction("internal_insert_icd_primer","vclaim","internal_insert",new Array());
     internal_insert_icd_primer.setSuperCommand("internal_insert_icd_primer");
     internal_insert_icd_primer.select=function(json){	
        var s = json.split("||");
        $("#internal_insert_diagnosaPrimer").val(s[0]);
        $("#internal_insert_namadiagnosaPrimer").val(s[1]);
        $("#smis-chooser-modal").smodal("hide");
     };

     internal_insert_icd_sekunder=new TableAction("internal_insert_icd_sekunder","vclaim","internal_insert",new Array());
     internal_insert_icd_sekunder.setSuperCommand("internal_insert_icd_sekunder");
     internal_insert_icd_sekunder.select=function(json){	
        var s = json.split("||");
        $("#internal_insert_diagnosaSekunder").val(s[0]);
        $("#internal_insert_namadiagnosaSekunder").val(s[1]);
        $("#smis-chooser-modal").smodal("hide");
     };

     internal_insert_procedure=new TableAction("internal_insert_procedure","vclaim","internal_insert",new Array());
     internal_insert_procedure.setSuperCommand("internal_insert_procedure");
     internal_insert_procedure.select=function(json){	
        var s = json.split("||");
        $("#internal_insert_procedure").val(s[0]);
        $("#internal_insert_namaprocedure").val(s[1]);
        $("#smis-chooser-modal").smodal("hide");
     };

     

 
 });