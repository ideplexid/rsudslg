/**
 * this page used to control and vclaim
 * of patient BPJS so usign VClaim system
 * 
 * @author      : Nurul Huda
 * @since       : 17 Feb 2018
 * @license     : LGPv2
 * @copyright   : goblooge@gmail.com
 * @version     : 1.0.1
 * @used        : vclaim/resource/php/vclaim_patient/datasep.php
 * 
 */
 var datasep;
 var datasep_icd;
 var datasep_dokter;
 var datasep_dpjp;
 var datasep_poli;
 var datasep_provinsi;
 var datasep_kabupaten;
 var datasep_kecamatan;
 var datasep_kecelakaan;
 var current_pasien;
 $(document).ready(function(){
    
    $(".mydate").datepicker();
     $(".mydatetime").datetimepicker({minuteStep:1});    
     $("#datasep_asalRujukan").on("click",function(){
         var vl = $(this).val();
         if(vl=="1" || vl=="2"){
             $("#datasep_diambil_dari option[value='Nomor Rujukan']").show();
             $("#datasep_diambil_dari option[value='Nomor BPJS']").show();
             $("#datasep_diambil_dari option[value='Nomor KTP']").hide();
             $("#datasep_jnsPelayanan").val(2);
             $("#datasep_jnsPelayanan").prop("disabled",true);
             $(".fcontainer_datasep_namapoliTujuan").show();
         }else{
             $("#datasep_diambil_dari option[value='Nomor Rujukan']").hide();
             $("#datasep_diambil_dari option[value='Nomor BPJS']").show();
             $("#datasep_diambil_dari option[value='Nomor KTP']").show();
             $("#datasep_jnsPelayanan").val("");
             $("#datasep_jnsPelayanan").prop("disabled",false);
             $(".fcontainer_datasep_namapoliTujuan").show();
         }
         $("#datasep_diambil_dari").val("");
         $(".ranap").hide();
     });
 
     $("#datasep_tujuanKunj").on("change",function(){
         var vl = $(this).val();
         if(vl=="0"){
             $("#datasep_flagProcedure").val("");
             $("#datasep_kdPenunjang").val("");
             $("#datasep_assesmentPel").val(4);
         }else if(vl=="2"){
             $("#datasep_assesmentPel").val(4);
         }
     });
 
     $("#datasep_naikKelas").on("change",function(){
         var vl = $(this).val();
         if(vl=="0"){
             $("#datasep_pembiayaan").val("");
         }
     });
     $("#datasep_asalRujukan").trigger("click");
     $("#datasep_no_bpjs, #datasep_no_ktp ").keyup(function(e){ 
         var code = e.which;
         if(code==13)e.preventDefault();
         if(code==32||code==13||code==188||code==186){
             datasep.fetch_server();
         }
     });


     
     datasep = new TableAction("datasep","vclaim","datasep");     
     datasep.update_tgl_pulang = function(){
        var x = this.getRegulerData();
        x['command'] = "update_pulang";
        x['noSep'] = datasep.get("noSep");
        x['statusPulang'] = datasep.get("statusPulang");
        x['noSuratMeninggal'] = datasep.get("noSuratMeninggal");
        x['tglMeninggal'] = datasep.get("tglMeninggal");
        x['tglPulang'] = datasep.get("tglPulang");
        x['noLPManual'] = datasep.get("noLPManual");
        showLoading();
        $.post("",x,function(res){
            var json = getContent(res);
            dismissLoading();
        });
     };
     datasep.update_sep = function(){
        var x = this.getRegulerData();
        x['command'] = "save";
        x['noSep'] = datasep.get("noSep");
        x['jenisSep'] = datasep.get("jenisSep");
        x["asalRujukan"] = datasep.get("asalRujukan");
        x["jnsPelayanan"] = datasep.get("jnsPelayanan");
        x["tglSep"] = datasep.get("tglSep");
        x["eksekutif"] = datasep.get("eksekutif");
        x["poliTujuan"] = datasep.get("poliTujuan");
        x["namapoliTujuan"] = datasep.get("namapoliTujuan");
        x["dpjpLayan"] = datasep.get("dpjpLayan");
        x["noMr"] = datasep.get("noMr");
        x["tglRujukan"] = datasep.get("tglRujukan");
        x["namaRujukan"] = datasep.get("namaRujukan");
        x["ppkRujukan"] = datasep.get("ppkRujukan");
        x["noRujukan"] = datasep.get("noRujukan");
        x["diagAwal"] = datasep.get("diagAwal");
        x["cob"] = datasep.get("cob");
        x["catatan"] = datasep.get("catatan");
        x["noTelp"] = datasep.get("noTelp");
        x["tujuanKunj"] = datasep.get("tujuanKunj");
        x["flagProcedure"] = datasep.get("flagProcedure");
        x["kdPenunjang"] = datasep.get("kdPenunjang");
        x["assesmentPel"] = datasep.get("assesmentPel");
        x["naikKelas"] = datasep.get("naikKelas");
        x["pembiayaan"] = datasep.get("pembiayaan");
        x["penanggunjwb"] = datasep.get("penanggunjwb");
        x["noSurat"] = datasep.get("noSurat");
        x["kodeDPJP"] = datasep.get("kodeDPJP");
        x["lakaLantas"] = datasep.get("lakaLantas");
        x["tgl_kejadian"] = datasep.get("tgl_kejadian");
        x["no_suplesi"] = datasep.get("no_suplesi");
        x["lokasiLaka"] = datasep.get("lokasiLaka");
        x["kdPropinsi"] = datasep.get("kdPropinsi");
        x["kdKabupaten"] = datasep.get("kdKabupaten");
        x["kdKecamatan"] = datasep.get("kdKecamatan");
        x["keterangan"] = datasep.get("keterangan");
        x["suplesi"] = datasep.get("suplesi");
        x["klsRawat"] = datasep.get("klsRawat");
        x["katarak"] = datasep.get("katarak");
        showLoading();
        $.post("",x,function(res){
            getContent(res);
            $(".fnosep").show();
            dismissLoading();
        });
     }

     datasep.cari = function(x){
        if(datasep.get("noSep")==""){
            return;
        }
        datasep.set("jnsPelayanan","");
        datasep.set("tglSep","");
        datasep.set("eksekutif",0);
        datasep.set("namapoliTujuan","");
        datasep.set("dpjpLayanNama","");
        datasep.set("dpjpLayan","");
        datasep.set("noMr","");
        datasep.set("tglRujukan","");
        datasep.set("namaRujukan","");
        datasep.set("ppkRujukan","");
        datasep.set("noRujukan","");
        datasep.set("diagAwal","");
        datasep.set("cob",0);
        datasep.set("katarak",0);
        datasep.set("catatan","");
        datasep.set("noTelp","");
        datasep.set("tujuanKunj","");
        datasep.set("flagProcedure","");
        datasep.set("kdPenunjang","");
        datasep.set("assesmentPel","");
        datasep.set("noSurat","");
        datasep.set("namaDPJP","");
        datasep.set("lakaLantas","");
        $("#datasep_noSEP").val("");
        $(".fnosep").hide();
        $(".datasep_namapoliTujuan").show();
        

        var x = this.getRegulerData();
        x['command'] = "edit";
        x['noSep'] = datasep.get("noSep");
        x['jenisSep'] = datasep.get("jenisSep");
        showLoading();
        $.post("",x,function(res){
            var j = getContent(res);
            datasep.set("asalRujukan",j.asalRujukan);
            datasep.set("jnsPelayanan",j.jnsPelayanan);
            datasep.set("tglSep",j.tglSep);
            datasep.set("eksekutif",j.eksekutif);
            datasep.set("poliTujuan",j.poliTujuan);
            datasep.set("namapoliTujuan",j.namapoliTujuan);
            datasep.set("dpjpLayan",j.dpjpLayan);
            datasep.set("namadpjpLayan",j.dpjpLayanNama);
            datasep.set("noMr",j.noMr);
            datasep.set("tglRujukan",j.tglRujukan);
            datasep.set("namaRujukan",j.namaRujukan);
            datasep.set("ppkRujukan",j.ppkRujukan);
            datasep.set("noRujukan",j.noRujukan);
            datasep.set("diagAwal",j.diagAwal);
            datasep.set("cob",j.cob);
            datasep.set("catatan",j.catatan);
            datasep.set("noTelp",j.noTelp);
            datasep.set("tujuanKunj",j.tujuanKunj);
            datasep.set("flagProcedure",j.flagProcedure);
            datasep.set("kdPenunjang",j.kdPenunjang);
            datasep.set("assesmentPel",j.assesmentPel);
            datasep.set("naikKelas",j.naikKelas);
            datasep.set("pembiayaan",j.pembiayaan);
            datasep.set("penanggunjwb",j.penanggunjwb);
            datasep.set("noSurat",j.noSurat);
            datasep.set("kodeDPJP",j.kodeDPJP);
            datasep.set("lakaLantas",j.lakaLantas);
            datasep.set("tgl_kejadian",j.tgl_kejadian);
            datasep.set("no_suplesi",j.no_suplesi);
            datasep.set("lokasiLaka",j.lokasiLaka);
            datasep.set("kdPropinsi",j.kdPropinsi);
            datasep.set("kdKabupaten",j.kdKabupaten);
            datasep.set("kdKecamatan",j.kdKecamatan);
            datasep.set("keterangan",j.keterangan);
            datasep.set("suplesi",j.suplesi);
            datasep.set("klsRawat",j.klsRawat);
            datasep.set("katarak",j.katarak);

            $("#datasep_page").html(j.tbl);

            dismissLoading();
        });  
     }

     datasep.del = function(){
        var x = this.getRegulerData();
        x['command'] = "del";
        x['noSep'] = datasep.get("noSep");
        x['jenisSep'] = datasep.get("jenisSep");
        x['user'] = datasep.get("user");
        x['noSurat'] = datasep.get("noSurat");
        x['tglRujukanInternal'] = datasep.get("tglRujukan");
        x['poli'] = datasep.get("poliTujuan")
        showLoading();
        $.post("",x,function(res){
            var j = getContent(res);
            dismissLoading();
        });  
     }

     datasep.fetch_server=function(){
         $("#datasep_noSEP").val("");
         data            = this.getRegulerData();
         data['no_bpjs'] = $("#datasep_no_bpjs").val();
         data['no_ktp']  = $("#datasep_no_ktp").val();
         data['command'] = "datasep";
         
         if(data['no_bpjs']=="" && (data['no_ktp']=="" || data['no_ktp']=="-") ){
             showWarning("Terjadi Kesalahan !!!","Masukan No. BPJS atau No. KTP, jika No. BPJS terisi No. KTP tidak dihiraukan");
         }
         
         showLoading();
         $.post("",data,function(res){
               var json  = getContent(res);
               $("#datasep_page").html(json.table);
               datasep.reset_part();
               if(json.raw != null && json.raw.status=="AKTIF"){
                   $("#datasep_noKartu").val(json.raw.nobpjs);
                   $("#datasep_no_ktp").val(json.raw.nik);
                   $("#datasep_no_bpjs").val(json.raw.nobpjs);
                   $("#datasep_ppkRujukan").val(json.raw.kd_provider);
                   $("#datasep_namaRujukan").val(json.raw.nama_provider);
               }
               dismissLoading();
         });
     };
 
     datasep.daftar_pasien = function(){
         vclaim_patient.show_add_form();
         setTimeout(function(){
             $("#vclaim_patient_save_reg").addClass("hide");
             $("#vclaim_patient_reg").addClass("hide");
             var tglhr = current_pasien.tgl_lahir.split("-");
             vclaim_patient.set("tgl_lahir",tglhr[2]+"-"+tglhr[1]+"-"+tglhr[0]);
             vclaim_patient.set("kelamin",current_pasien.jk=="L"?0:1);
             vclaim_patient.set("nama",current_pasien.nama);
             vclaim_patient.set("ktp",current_pasien.nik);
             vclaim_patient.set("umur",current_pasien.umur);
             vclaim_patient.set("nobpjs",current_pasien.nobpjs);
             vclaim_patient.aftersave=function(json){
                 $("#datasep_id_pasien").val(json.id);
                 $("#datasep_noMr").val(json.nrm);
                 activeTab("#datasep");
             };
             activeTab("#vclaim_patient_tab");
         },1000);
     }
     
     datasep.cek_rujukan=function(){
         $("#datasep_noSEP").val("");
         data                = this.getRegulerData();
         data['noRujukan']   = $("#datasep_noRujukan").val();
         data['noBPJS']      = $("#datasep_no_bpjs").val();
         data['faskes']      = $("#datasep_asalRujukan").val();
         data['command']     = "cek_rujukan";
         
         if(data['noBPJS']=="" && data['noRujukan']=="" ){
             showWarning("Terjadi Kesalahan !!!","Silakan Isikan Nomor Rujukanya atau Nomor BPJS Terlebih Dahulu, kemudian tekan enter atau Klik Tombol Pencarian Rujukan");
         }
         
         showLoading();
         $.post("",data,function(res){
               var json      = getContent(res);
               $("#datasep_noRujukan").val(data['noRujukan']);
               current_pasien = null;
               if(json.message=="Data Ditemukan"){
                 current_pasien = json;
                   datasep.reset_part();
                   $("#datasep_no_bpjs").val(json.nobpjs);     
                   $("#datasep_status_nrm").val(json.status_nrm);
                   $("#datasep_noKartu").val(json.nobpjs);                  
                   $("#datasep_tglRujukan").val(json.tgl_kunjungan);
                   $("#datasep_ppkRujukan").val(json.kd_provider);
                   $("#datasep_namaRujukan").val(json.nama_provider);
                   $("#datasep_diagAwal").val(json.kode_diagnosa);
                   $("#datasep_poliTujuan").val(json.kd_poli);
                   $("#datasep_namapoliTujuan").val(json.nama_poli);
                   $("#datasep_catatan").val(json.catatan);
                   $("#datasep_noRujukan").val(json.no_kunjungan);
                   $("#datasep_catatan").val(json.catatan);
                   $("#datasep_no_ktp").val(json.nik);
                   $("#datasep_klsRawat").val(json.kelas);
                   $("#datasep_asalRujukan").val(json.asal_rujukan);    
                   //$("#datasep_noRujukan").val(json.no_rujukan);     
                   $("#datasep_namapoliTujuan").trigger("change");           
                   if(json.kd_poli!=null && json.kd_poli!=""){
                     $("#datasep_jnsPelayanan").val(2);
                   }else{
                     $("#datasep_jnsPelayanan").val(1); 
                   }
 
                   if(json.status_nrm=="Belum Ada"){
                     $("#datasep_id_pasien").val("");
                     $("#datasep_noMr").val("");
                     $(".df_pasien").show();
                   }else{
                      $("#datasep_id_pasien").val(json.id_pasien);
                      $("#datasep_noMr").val(json.nrm_pasien);
                      $(".df_pasien").hide();                    
                   }
                   $("#datasep_jnsPelayanan").trigger("change");
 
                   data            = datasep.getRegulerData();
                   //data['no_bpjs'] = $("#datasep_no_bpjs").val();
                   //data['no_ktp']  = $("#datasep_no_ktp").val();
                   data['command'] = "datasep";
                   //$("#datasep_noRujukan").val("");
                   data['no_ktp']="";
                   data['no_bpjs']=json.nobpjs;
                   //alert($("#datasep_noRujukan").val()+"  "+json.no_kunjungan);  
                   showLoading();
                   $.post("",data,function(res){
                       var json  = getContent(res);
                       $("#datasep_page").html(json.table);
                       //datasep.reset_part();
                       if(json.raw != null && json.raw.status=="AKTIF"){
                           $("#datasep_noKartu").val(json.raw.nobpjs);
                           $("#datasep_no_ktp").val(json.raw.nik);
                           $("#datasep_no_bpjs").val(json.raw.nobpjs);
                           $("#datasep_ppkRujukan").val(json.raw.kd_provider);
                           $("#datasep_namaRujukan").val(json.raw.nama_provider);
                       }
                       if(json.raw!=null){
                           current_pasien = json.raw;
       
                           $("#datasep_status_nrm").val(json.raw.status_nrm);
                           if(json.raw.status_nrm=="Sudah Ada"){
                               //alert("BELUM ADA");
                               $("#datasep_id_pasien").val(json.raw.id_pasien);
                               $("#datasep_noMr").val(json.raw.nrm_pasien);
                               $("#df_pasien").addClass("hide");
                           }else{
                             //alert("BELUM ADA");
                             $("#datasep_id_pasien").val("");
                             $("#datasep_noMr").val("");
                             $("#df_pasien").removeClass("hide");
                           }                         
                       }
                       dismissLoading();
                   });
 
               }else{
                   showWarning("Terjadi Kesalahan !!!",json.message);
               }
               dismissLoading();
         });
     };
    
     
     
     datasep.reset=function(){
         $("#datasep_noSEP").val("");
         $("#datasep_no_ktp").val(""); 
         $("#datasep_noMr").val("");
         $("#datasep_noTelp").val("");
         datasep.reset_part();
     };
     
     datasep.reset_part=function(){
         var currentdate = new Date(); 
         var month       = (currentdate.getMonth()+1);
         if(month<10){
                 month   = "0"+month;
         }        
         var dt          = currentdate.getDate();
         if(dt<10){
                 dt      = "0"+dt;
         }        
         var hour        = currentdate.getHours();
         if(hour<10){
                 hour    = "0"+hour;
         }        
         var minute      = currentdate.getMinutes();
         if(minute<10){
                 minute  = "0"+minute;
         }        
         var second      = currentdate.getSeconds();
         if(second<10){
                 second  = "0"+second;
         }        
         var datetime    = currentdate.getFullYear() + "-" + month + "-" + dt + " "+ hour + ":" + minute + ":"+ second;         
         $("#datasep_noKartu").val("");
         $("#datasep_no_bpjs").val("");
         $("#datasep_tglSep").val(datetime);
         $("#datasep_tglRujukan").val(datetime);
         $("#datasep_noRujukan").val("");
         $("#datasep_ppkRujukan").val("");
         $("#datasep_namaRujukan").val("");
         $("#datasep_jnsPelayanan").val("");
         $("#datasep_diagAwal").val("");
         $("#datasep_poliTujuan").val("");
         $("#datasep_klsRawat").val("");
         $("#datasep_lakaLantas").val("");
         $("#datasep_lokasiLaka").val("");
         $("#datasep_catatan").val("");
         $("#datasep_noREG").val("");
         $("#datasep_penjamin").val("");
         $("#datasep_cob").val("");
         $("#datasep_eksekutif").val("");
         $("#datasep_asalRujukan").val("");        
     };
     
      datasep.print_sep=function(){
          var nosep      = $("#datasep_noSep").val();
          if(nosep==""){
             showWarning("Peringatan !!!","Silakan Masukan Nomor BPJS Terlebih dahulu, pastikan pasien aktif, kemudian create SEP baru registrasi.");
             return;
         }
          data           = this.getRegulerData();
          data['nosep']  = nosep;
          data['action'] = "print_vklaim";
          showLoading();
          $.post("",data,function(res){
              var json=getContent(res);
              smis_print(json);
              dismissLoading();
          });
      };
      
      datasep.sep_sementara=function(){
          var self                       = this;
          bootbox.confirm("SEP Sementara tidak terhubung dengan SIM SEP BPJS, Hanya Dipakai ketika Jaringan BPJS Bermasalah dan harus diinput ulang oleh petugas maksimal 3 hari", function(result){ 
             if(result){
                 data                    = self.getRegulerData();
                 data['tanggal']         = $("#"+self.prefix+"_tglSep").val();
                 data['no_bpjs']         = $("#"+self.prefix+"_no_bpjs").val()==""?$("#"+self.prefix+"_no_bpjs").val():$("#"+self.prefix+"_noKartu").val();
                 data['no_rujukan']      = $("#"+self.prefix+"_noRujukan").val();
                 data['no_ktp']          = $("#"+self.prefix+"_no_ktp").val();
                 data['nrm']             = $("#"+self.prefix+"_noMr").val();
                 data['poli_tujuan']     = $("#"+self.prefix+"_poliTujuan option:selected").text();;
                 data['diagnosa_awal']   = $("#"+self.prefix+"_diagAwal").val();
                 data['catatan']         = $("#"+self.prefix+"_catatan").val();
                 data['jns_rawat']       = $("#"+self.prefix+"_jnsPelayanan option:selected").text();
                 data['kasus_laka']      = $("#"+self.prefix+"_lakaLantas option:selected").text();
                 data['lokasi_laka']     = $("#"+self.prefix+"_lokasiLaka").val();;
                 data['action']          = "print_sep_sementara";
                 showLoading();
                 $.post("",data,function(res){
                     var json            = getContent(res);
                     smis_print(json);
                     dismissLoading();
                 });
             }
         });
      };
      
     datasep.register_pasien_bpjs=function(){
         var nosep   = $("#"+this.prefix+"_noSEP").val();
         var noreg   = $("#"+this.prefix+"_noREG").val();
         var nrm     = $("#"+this.prefix+"_noMr").val();
         var nobpjs  = $("#"+this.prefix+"_noKartu").val();
         var kelas   = $("#"+this.prefix+"_klsRawat").val();
         if(nosep==""){
             showWarning("Peringatan !!!","Silakan Masukan Nomor BPJS Terlebih dahulu, pastikan pasien aktif, kemudian create SEP baru registrasi.");
             return;
         }        
         if(noreg=="0"){
             daftar_pasien.show_add_form_nosep(nrm,nosep,nobpjs,kelas);
         }else{
             daftar_pasien.edit_nosep(noreg,nosep,nobpjs,kelas);    
         }
     };
     
     datasep.cek_nomor_rm=function(id_pasien,nrm,ktp,telpon){
         datasep.reset();
         $("#datasep_id_pasien").val(id_pasien);
         $("#datasep_noMr").val(nrm);
         $("#datasep_no_ktp").val(ktp);
         $("#datasep_noTelp").val(telpon);
         $("#datasep_no_bpjs").val("");
         if(ktp=="" || ktp=="-"){
             var data        = this.getRegulerData();
             data['nrm']     = nrm;
             data['action']  = "search_no_bpjs";
             showLoading();
             $.post("",data,function(res){
                 dismissLoading();
                 var json=getContent(res);
                 if(json==""){
                     showWarning("No. BPJS/KTP/NIK Kosong","dikarenakan No. BPJS/KTP/NIK kosong, silakan isikan No. BPJS/KTP/NIK pasien yang bersangkutan");
                 }else{
                     $("#datasep_no_bpjs").val(json);
                     $("#datasep_button").trigger("click");
                 }
             });
         }else{
             $("#datasep_button").trigger("click");
         }
     };
 
     datasep.cari_laka = function(){
         datasep.chooser('datasep','datasep_no_suplesi','datasep_kecelakaan',datasep_kecelakaan,'Kecelakaan')
     }
     
 
     datasep.fix_register=function(){
         data            = this.getRegulerData();
         data['noSEP']   = $("#datasep_noSEP").val();
         data['noMr']    = $("#datasep_noMr").val();
         if(data['noSEP']=="" || data['noMr']==""){
             showWarning("Peringatan !!!","Silakan Bikin BPJS SEP terlebih dahulu, Jika No RM kosong, silakan buka Menu Registrasi Pasien, Pilih pasienya, kemudian Tekan Opsi SEP. Pastikan No. KTP/NIK terisi untuk kemudahan");
             return;
         }        
         data['command'] = "fix_register";
         showLoading();
         $.post("",data,function(res){
               var json  = getContent(res);
               $("#datasep_noREG").val(json.noreg);
               $(".fnosep").show();
               dismissLoading();
         });
     };
 
     
     datasep.reg_sep=function(){
         $("#datasep_noSEP").val("");
         data                 = this.getRegulerData();
         
         data['jnsPelayanan'] = datasep.get("jnsPelayanan");
         data['eksekutif']    = datasep.get("eksekutif");
         data['noKartu']      = datasep.get("noKartu");
         data['tglSep']       = datasep.get("tglSep");
         data['tglRujukan']   = datasep.get("tglRujukan");
         data['noRujukan']    = datasep.get("noRujukan");
         data['ppkRujukan']   = datasep.get("ppkRujukan");
         data['ppkPelayanan'] = datasep.get("ppkPelayanan");
         data['jnsPelayanan'] = datasep.get("jnsPelayanan");
         data['diagAwal']     = datasep.get("diagAwal");
         data['poliTujuan']   = datasep.get("poliTujuan");
         data['klsRawat']     = datasep.get("klsRawat");
 
         data['lakaLantas']   = datasep.get("lakaLantas");
         data['tgl_kejadian']   = datasep.get("tgl_kejadian");
         data['no_suplesi']   = datasep.get("no_suplesi");
         data['lokasiLaka']   = datasep.get("lokasiLaka");
         data['kdPropinsi']   = datasep.get("kdPropinsi");
         data['kdKabupaten']  = datasep.get("kdKabupaten");
         data['kdKecamatan']  = datasep.get("kdKecamatan");
         data['keterangan']   = datasep.get("keterangan");
 
         data['catatan']     = datasep.get("catatan");
         data['user']        = datasep.get("user");
         data['noMr']        = datasep.get("noMr");
         data['id_pasien']   = datasep.get("id_pasien");        
         data['penjamin']    = datasep.get("penjamin");
         data['cob']         = datasep.get("cob");
         data['tujuanKunj']  = datasep.get("tujuanKunj");
         data['flagProcedure']  = datasep.get("flagProcedure");
         data['kdPenunjang']  = datasep.get("kdPenunjang");
         data['assesmentPel']  = datasep.get("assesmentPel");
 
         data['naikKelas']  = datasep.get("naikKelas");
         data['pembiayaan']  = datasep.get("pembiayaan");
         data['penanggunjwb']  = datasep.get("penanggunjwb");
         
         data['noSurat']     = datasep.get("noSurat");
         data['kodeDPJP']    = datasep.get("kodeDPJP");
     
         data['asalRujukan'] = datasep.get("asalRujukan");
         data['noTelp']      = datasep.get("noTelp");
         data['dpjpLayan']      = datasep.get("dpjpLayan");
         data['command']     = "reg_sep";
         showLoading();
         $.post("",data,function(res){
               var json      = getContent(res);
               $("#datasep_noSEP").val(json.noSEP);
               $("#datasep_noREG").val(json.noreg);
               dismissLoading();
         });
     };
     
     $("#datasep_noRujukan").keyup(function(e){
         if (e.keyCode==13) {
             datasep.cek_rujukan();
         }
     });
     
     $("#datasep_lakaLantas").on("change",function(e){
        if($("#cek_vklaim_lakaLantas").val()=="0"){
            $(".kclk").hide();
        }else{
            $(".kclk").show();
        }

        if( $("#datasep_lakaLantas").val()=="1"){
            $("#datasep_lokasiLaka").prop("disabled",false);
        }else{
            $("#datasep_lokasiLaka").prop("disabled",true);
            $("#datasep_lokasiLaka").val("");
        }
     });
     
   
     
     datasep_icd=new TableAction("datasep_icd","vclaim","datasep",new Array());
     datasep_icd.setSuperCommand("datasep_icd");
     datasep_icd.select=function(json){	
         $("#datasep_diagAwal").val(json);
         $("#smis-chooser-modal").smodal("hide");
     };
 
     datasep_poli=new TableAction("datasep_poli","vclaim","datasep",new Array());
     datasep_poli.setSuperCommand("datasep_poli");
     datasep_poli.select=function(kode){
         var kd = kode.split("||");
         $("#datasep_poliTujuan").val(kd[0]);
         $("#datasep_namapoliTujuan").val(kd[1]);
         $("#datasep_namapoliTujuan").trigger("change");
         $("#smis-chooser-modal").smodal("hide");
     };

 
     datasep_provinsi=new TableAction("datasep_provinsi","vclaim","datasep",new Array());
     datasep_provinsi.setSuperCommand("datasep_provinsi");
     datasep_provinsi.select=function(kode){
         $("#datasep_kdPropinsi").val(kode);
         $("#smis-chooser-modal").smodal("hide");
     };
 
     datasep_kabupaten=new TableAction("datasep_kabupaten","vclaim","datasep",new Array());
     datasep_kabupaten.setSuperCommand("datasep_kabupaten");
     datasep_kabupaten.addViewData = function(x){
         x['provinsi'] = $("#datasep_kdPropinsi").val();
         return x;
     };
     datasep_kabupaten.select=function(kode){
         $("#datasep_kdKabupaten").val(kode);
         $("#smis-chooser-modal").smodal("hide");
     };
 
     datasep_kecamatan=new TableAction("datasep_kecamatan","vclaim","datasep",new Array());
     datasep_kecamatan.setSuperCommand("datasep_kecamatan");
     datasep_kecamatan.addViewData = function(x){
         x['kabupaten'] = $("#datasep_kdKabupaten").val();
         return x;
     };
     datasep_kecamatan.select=function(kode){
         $("#datasep_kdKecamatan").val(kode);
         $("#smis-chooser-modal").smodal("hide");
     };
 
     datasep_kecelakaan=new TableAction("datasep_kecelakaan","vclaim","datasep",new Array());
     datasep_kecelakaan.setSuperCommand("datasep_kecelakaan");
     datasep_kecelakaan.addViewData = function(x){
         x['nokartu'] = $("#datasep_noKartu").val();
         return x;
     };
     datasep_kecelakaan.select=function(kode){
         $("#datasep_kdKecamatan").val(kode);
         $("#smis-chooser-modal").smodal("hide");
     };
 
     
 
     datasep_dokter=new TableAction("datasep_dokter","vclaim","datasep",new Array());
     datasep_dokter.setSuperCommand("datasep_dokter");
     datasep_dokter.addViewData = function(x){
         x['jenis'] = $("#datasep_jnsPelayanan").val();
         x['poli'] = $("#datasep_poliTujuan").val();
         return x;
     }
     datasep_dokter.select = function(kode){

        var kd = kode.split("||");
//        $("#datasep_kodeDPJP").val(kd[0]);
 //       $("#datasep_namaDPJP").val(kd[1]);

         $("#datasep_dpjpLayan").val(kd[0]);
         $("#datasep_namadpjpLayan").val(kd[1]);
         $("#smis-chooser-modal").smodal("hide");
     };
 
     datasep_dpjp=new TableAction("datasep_dpjp","vclaim","datasep",new Array());
     datasep_dpjp.setSuperCommand("datasep_dpjp");
     datasep_dpjp.addViewData = function(x){
         x['jenis'] = $("#datasep_jnsPelayanan").val();
         x['poli'] = $("#datasep_poliTujuan").val();
         return x;
     }
     datasep_dpjp.select = function(kode){
         var kd = kode.split("||");
         $("#datasep_kodeDPJP").val(kd[0]);
         $("#datasep_namaDPJP").val(kd[1]);
         $("#smis-chooser-modal").smodal("hide");
     };

     datasep.fingerprint = function(){
        data                = this.getRegulerData();
        data['peserta']     = datasep.get("noKartu");
        data['tgl']         = datasep.get("tglSep");
        data['command']     = "fingerprint";
        showLoading();
        $.post("",data,function(res){
            var g = getContent(res);
            $("#hasil-finger").html(g);
            dismissLoading();
        });
    }

     $("#datasep_jnsPelayanan").on("change",function(e){
        if($(this).val()=="1"){
            $(".ranap").show();
        }else{
            $(".ranap").hide();
        }
    });
     
 
     $("#datasep_jenisSep").on("change",function(){
        var jns= $(this).val();
        if(jns=="internal"){
            $("#update_sep").hide();
        }else{
            $("#update_sep").show();
        }
     });

     $("#datasep_namapoliTujuan").on("change",function(){
        var k = $("#datasep_poliTujuan").val();
        if(k=="HDL" || k=="JAN" || k=="IRM" || k=="MAT"){
            $(".finger").show();
            $("#hasil-finger").html("");
        }else{
            $(".finger").hide();
            $("#hasil-finger").html("");
        }
    });

    datasep.cari();
 
 });