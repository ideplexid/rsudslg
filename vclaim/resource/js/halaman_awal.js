var halaman_awal;
$(document).ready(function(){
    $(".mydate").datepicker();
    
    halaman_awal = new TableAction("halaman_awal","vclaim","halaman_awal", new Array()); 
    halaman_awal.addViewData = function(x){
        x['dari'] = halaman_awal.get("dari");
        x['sampai'] = halaman_awal.get("sampai");
        return x;
    }    

    halaman_awal.show_add_form = function(){
        $("#tambah_prb_anchor").trigger("click");
    }

    halaman_awal.edit = function(idx){
        var xx = idx.split("||");
        var noSep = xx[0];
        var noSRB = xx[1];
        $("#edit_prb_searchNoSep").val(noSep);
        $("#edit_prb_noSRB").val(noSRB);
        $("#edit_prb_anchor").trigger("click");
        edit_prb.edit();
        
    } 
});