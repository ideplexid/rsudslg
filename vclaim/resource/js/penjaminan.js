/**
 * this page used to control and vclaim
 * of patient BPJS so usign VClaim system
 * 
 * @author      : Nurul Huda
 * @since       : 17 Feb 2018
 * @license     : LGPv2
 * @copyright   : goblooge@gmail.com
 * @version     : 1.0.1
 * @used        : vclaim/resource/php/vclaim_patient/penjaminan.php
 * 
 */
 var penjaminan;
 var penjaminan_icd;
 var penjaminan_dokter;
 var penjaminan_dpjp;
 var penjaminan_poli;
 var penjaminan_provinsi;
 var penjaminan_kabupaten;
 var penjaminan_kecamatan;
 var penjaminan_kecelakaan;
 var current_pasien;
 $(document).ready(function(){
    
    $(".mydate").datepicker();
     $(".mydatetime").datetimepicker({minuteStep:1});    
     var col = new Array("noKartu","nama_peserta","tglSep","jnsPelayanan","jnsPengajuan","keterangan");
     penjaminan = new TableAction("penjaminan","vclaim","penjaminan",col);     
     penjaminan.more_option = function (a){
        penjaminan.fetch_server();
     }

     penjaminan.fetch_server=function(){
         data            = this.getRegulerData();
         data['noKartu'] = $("#penjaminan_noKartu").val();
         data['tglSep'] = $("#penjaminan_tglSep").val();
         data['command'] = "cek_bpjs";
         
         if(data['noKartu']==""){
             showWarning("Terjadi Kesalahan !!!","Masukan No. BPJS atau No. KTP, jika No. BPJS terisi No. KTP tidak dihiraukan");
         }
         
         showLoading();
         $.post("",data,function(res){
               var json  = getContent(res);
               $("#penjaminan_page").html(json.table);
               $("#penjaminan_nama_peserta").val(json.nama);
               dismissLoading();
         });
     };
 
     penjaminan.daftar_pasien = function(){
         vclaim_patient.show_add_form();
         setTimeout(function(){
             $("#vclaim_patient_save_reg").addClass("hide");
             $("#vclaim_patient_reg").addClass("hide");
             var tglhr = current_pasien.tgl_lahir.split("-");
             vclaim_patient.set("tgl_lahir",tglhr[2]+"-"+tglhr[1]+"-"+tglhr[0]);
             vclaim_patient.set("kelamin",current_pasien.jk=="L"?0:1);
             vclaim_patient.set("nama",current_pasien.nama);
             vclaim_patient.set("ktp",current_pasien.nik);
             vclaim_patient.set("umur",current_pasien.umur);
             vclaim_patient.set("nobpjs",current_pasien.nobpjs);
             vclaim_patient.aftersave=function(json){
                 $("#penjaminan_id_pasien").val(json.id);
                 $("#penjaminan_noMr").val(json.nrm);
                 activeTab("#penjaminan");
             };
             activeTab("#vclaim_patient_tab");
         },1000);
     }
     
    
     
     
     penjaminan.reset_part=function(){
         $("#penjaminan_jnsPelayanan").val("");
         $("#penjaminan_jnsPengajuan").val("");
         $("#penjaminan_keterangan").val("");
     };
     
    
 
 });