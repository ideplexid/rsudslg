/**
 * this page used to control and vclaim
 * of patient BPJS so usign VClaim system
 * 
 * @author      : Nurul Huda
 * @since       : 17 Feb 2018
 * @license     : LGPv2
 * @copyright   : goblooge@gmail.com
 * @version     : 1.0.1
 * @used        : vclaim/resource/php/vclaim_patient/rujukan.php
 * 
 */
 var rujukan;
 var rujukan_icd;
 var rujukan_dokter;
 var rujukan_dpjp;
 var rujukan_ppk;
 var rujukan_poli;

 var current_pasien;
 $(document).ready(function(){
    
    $(".mydate").datepicker();
     $(".mydatetime").datetimepicker({minuteStep:1});    
     $("#rujukan_asalRujukan").on("click",function(){
         var vl = $(this).val();
         if(vl=="1" || vl=="2"){
             $("#rujukan_diambil_dari option[value='Nomor Rujukan']").show();
             $("#rujukan_diambil_dari option[value='Nomor BPJS']").show();
             $("#rujukan_diambil_dari option[value='Nomor KTP']").hide();
             $("#rujukan_jnsPelayanan").val(2);
             $("#rujukan_jnsPelayanan").prop("disabled",true);
         }else{
             $("#rujukan_diambil_dari option[value='Nomor Rujukan']").hide();
             $("#rujukan_diambil_dari option[value='Nomor BPJS']").show();
             $("#rujukan_diambil_dari option[value='Nomor KTP']").show();
             $("#rujukan_jnsPelayanan").val("");
             $("#rujukan_jnsPelayanan").prop("disabled",false);
         }
         $("#rujukan_diambil_dari").val("");
     });
 
     $("#rujukan_tujuanKunj").on("change",function(){
         var vl = $(this).val();
         if(vl=="0"){
             $("#rujukan_flagProcedure").val("");
             $("#rujukan_kdPenunjang").val("");
             $("#rujukan_assesmentPel").val(4);
         }else if(vl=="2"){
             $("#rujukan_assesmentPel").val(4);
         }
     });
 
     $("#rujukan_naikKelas").on("change",function(){
         var vl = $(this).val();
         if(vl=="0"){
             $("#rujukan_pembiayaan").val("");
         }
     });
     $("#rujukan_asalRujukan").trigger("click");
     $("#rujukan_no_bpjs, #rujukan_no_ktp ").keyup(function(e){ 
         var code = e.which;
         if(code==13)e.preventDefault();
         if(code==32||code==13||code==188||code==186){
             rujukan.fetch_server();
         }
     });


     
     rujukan = new TableAction("rujukan","vclaim","rujukan", new Array());     
    
     rujukan.cari = function(x){
        rujukan.current_cetak = "";
        var x = this.getRegulerData();
        x['command'] = "edit";
        x['noSep'] = rujukan.get("noSep");
        x['jenisSep'] = rujukan.get("jenisSep");
        showLoading();
        $.post("",x,function(res){
            var j = getContent(res);
            rujukan.set("asalRujukan",j.asalRujukan);
            rujukan.set("jnsPelayanan",j.jnsPelayanan);
            rujukan.set("tglSep",j.tglSep);
            rujukan.set("eksekutif",j.eksekutif);
            rujukan.set("poliRujukan",j.poliRujukan);
            rujukan.set("namapoliRujukan",j.namapoliRujukan);
            rujukan.set("dpjpLayan",j.dpjpLayan);
            rujukan.set("noMr",j.noMr);
            rujukan.set("tglRujukan",j.tglRujukan);
            rujukan.set("namaRujukan",j.namaRujukan);
            rujukan.set("ppkRujukan",j.ppkRujukan);
            rujukan.set("noRujukan",j.noRujukan);
            rujukan.set("diagRujukan",j.diagRujukan);
            rujukan.set("cob",j.cob);
            rujukan.set("catatan",j.catatan);
            rujukan.set("noTelp",j.noTelp);
            rujukan.set("tujuanKunj",j.tujuanKunj);
            rujukan.set("flagProcedure",j.flagProcedure);
            rujukan.set("kdPenunjang",j.kdPenunjang);
            rujukan.set("assesmentPel",j.assesmentPel);
            rujukan.set("naikKelas",j.naikKelas);
            rujukan.set("pembiayaan",j.pembiayaan);
            rujukan.set("penanggunjwb",j.penanggunjwb);
            rujukan.set("noSurat",j.noSurat);
            rujukan.set("kodeDPJP",j.kodeDPJP);
            rujukan.set("lakaLantas",j.lakaLantas);
            rujukan.set("tgl_kejadian",j.tgl_kejadian);
            rujukan.set("no_suplesi",j.no_suplesi);
            rujukan.set("lokasiLaka",j.lokasiLaka);
            rujukan.set("kdPropinsi",j.kdPropinsi);
            rujukan.set("kdKabupaten",j.kdKabupaten);
            rujukan.set("kdKecamatan",j.kdKecamatan);
            rujukan.set("keterangan",j.keterangan);
            rujukan.set("suplesi",j.suplesi);
            rujukan.set("klsRawat",j.klsRawat);
            rujukan.set("katarak",j.katarak);

            $("#rujukan_page").html(j.tbl);

            dismissLoading();
        });  
     }

     rujukan.cetak = function(){

     }

     rujukan.del = function(){
        var x = this.getRegulerData();
        x['command'] = "del";
        x['noSep'] = rujukan.get("noSep");
        x['jenisSep'] = rujukan.get("jenisSep");
        x['user'] = rujukan.get("user");
        x['noSurat'] = rujukan.get("noSurat");
        x['tglRujukanInternal'] = rujukan.get("tglRujukan");
        x['poli'] = rujukan.get("poliRujukan")
        showLoading();
        $.post("",x,function(res){
            var j = getContent(res);
            dismissLoading();
        });  
     }
     
     rujukan.cek_rujukan=function(){
         $("#rujukan_noSEP").val("");
         data                = this.getRegulerData();
         data['noRujukan']   = $("#rujukan_noRujukan").val();
         data['noBPJS']      = $("#rujukan_no_bpjs").val();
         data['faskes']      = $("#rujukan_asalRujukan").val();
         data['command']     = "cek_rujukan";
         
         if(data['noBPJS']=="" && data['noRujukan']=="" ){
             showWarning("Terjadi Kesalahan !!!","Silakan Isikan Nomor Rujukanya atau Nomor BPJS Terlebih Dahulu, kemudian tekan enter atau Klik Tombol Pencarian Rujukan");
         }
         
         showLoading();
         $.post("",data,function(res){
               var json      = getContent(res);
               $("#rujukan_noRujukan").val(data['noRujukan']);
               current_pasien = null;
               if(json.message=="Data Ditemukan"){
                 current_pasien = json;
                   rujukan.reset_part();
                   $("#rujukan_no_bpjs").val(json.nobpjs);     
                   $("#rujukan_status_nrm").val(json.status_nrm);
                   $("#rujukan_noKartu").val(json.nobpjs);                  
                   $("#rujukan_tglRujukan").val(json.tgl_kunjungan);
                   $("#rujukan_ppkRujukan").val(json.kd_provider);
                   $("#rujukan_namaRujukan").val(json.nama_provider);
                   $("#rujukan_diagRujukan").val(json.kode_diagnosa);
                   $("#rujukan_ppkRujukan").val(json.kd_poli);
                   $("#rujukan_namapoliRujukan").val(json.nama_poli);
                   $("#rujukan_catatan").val(json.catatan);
                   $("#rujukan_noRujukan").val(json.no_kunjungan);
                   $("#rujukan_catatan").val(json.catatan);
                   $("#rujukan_no_ktp").val(json.nik);
                   $("#rujukan_klsRawat").val(json.kelas);
                   $("#rujukan_asalRujukan").val(json.asal_rujukan);    
                   //$("#rujukan_noRujukan").val(json.no_rujukan);              
                   if(json.kd_poli!=null && json.kd_poli!=""){
                     $("#rujukan_jnsPelayanan").val(2);
                   }else{
                     $("#rujukan_jnsPelayanan").val(1); 
                   }
 
                   if(json.status_nrm=="Belum Ada"){
                     $("#rujukan_id_pasien").val("");
                     $("#rujukan_noMr").val("");
                     $(".df_pasien").show();
                   }else{
                      $("#rujukan_id_pasien").val(json.id_pasien);
                      $("#rujukan_noMr").val(json.nrm_pasien);
                      $(".df_pasien").hide();                    
                   }
                   //$("#rujukan_jnsPelayanan").trigger("change");
 
                   data            = rujukan.getRegulerData();
                   //data['no_bpjs'] = $("#rujukan_no_bpjs").val();
                   //data['no_ktp']  = $("#rujukan_no_ktp").val();
                   data['command'] = "rujukan";
                   //$("#rujukan_noRujukan").val("");
                   data['no_ktp']="";
                   data['no_bpjs']=json.nobpjs;
                   //alert($("#rujukan_noRujukan").val()+"  "+json.no_kunjungan);  
                   showLoading();
                   $.post("",data,function(res){
                       var json  = getContent(res);
                       $("#rujukan_page").html(json.table);
                       //rujukan.reset_part();
                       if(json.raw != null && json.raw.status=="AKTIF"){
                           $("#rujukan_noKartu").val(json.raw.nobpjs);
                           $("#rujukan_no_ktp").val(json.raw.nik);
                           $("#rujukan_no_bpjs").val(json.raw.nobpjs);
                           $("#rujukan_ppkRujukan").val(json.raw.kd_provider);
                           $("#rujukan_namaRujukan").val(json.raw.nama_provider);
                       }
                       if(json.raw!=null){
                           current_pasien = json.raw;
       
                           $("#rujukan_status_nrm").val(json.raw.status_nrm);
                           if(json.raw.status_nrm=="Sudah Ada"){
                               //alert("BELUM ADA");
                               $("#rujukan_id_pasien").val(json.raw.id_pasien);
                               $("#rujukan_noMr").val(json.raw.nrm_pasien);
                               $("#df_pasien").addClass("hide");
                           }else{
                             //alert("BELUM ADA");
                             $("#rujukan_id_pasien").val("");
                             $("#rujukan_noMr").val("");
                             $("#df_pasien").removeClass("hide");
                           }                         
                       }
                       dismissLoading();
                   });
 
               }else{
                   showWarning("Terjadi Kesalahan !!!",json.message);
               }
               dismissLoading();
         });
     };
     rujukan.cetak = function(){
         if(rujukan.current_cetak!=""){
            $("#printing_area").html(rujukan.current_cetak);
            setTimeout(function(){
                window.print();
            },2000);
         }
     }
     rujukan.current_cetak = "";
     rujukan.simpan=function(){
        rujukan.current_cetak = "";
        data            = this.getRegulerData();
        data['noSep' ] = rujukan.get("noSep");
        data['tglRujukan' ] = rujukan.get("tglRujukan");
        data['tglRencanaKunjungan' ] = rujukan.get("tglRencanaKunjungan");
        data['ppkDirujuk' ] = rujukan.get("ppkDirujuk");
        data['jnsPelayanan' ] = rujukan.get("jnsPelayanan");
        data['catatan' ] = rujukan.get("catatan");
        data['diagRujukan' ] = rujukan.get("diagRujukan");
        data['tipeRujukan' ] = rujukan.get("tipeRujukan");
        data['poliRujukan' ] = rujukan.get("poliRujukan");    
        data['namadiagRujukan' ] = rujukan.get("namadiagRujukan");
        data['namappkDirujuk' ] = rujukan.get("namaDirujuk");
        data['namapoliRujukan' ] = rujukan.get("namapoliRujukan");    
        data['command'] = "save";
        console.log(data);
        showLoading();
        $.post("",data,function(res){
              var json  = getContent(res);
              if(typeof json.print!=undefined && json.success==1){
                rujukan.current_cetak = json.print;
                    $("#printing_area").html(json.print);
                    setTimeout(function(){
                        window.print();
                    },2000);
              }
              dismissLoading();
        });
    };
 
     $("#rujukan_noRujukan").keyup(function(e){
         if (e.keyCode==13) {
             rujukan.cek_rujukan();
         }
     });
     
     
   
     
     rujukan_icd=new TableAction("rujukan_icd","vclaim","rujukan",new Array());
     rujukan_icd.setSuperCommand("rujukan_icd");
     rujukan_icd.select=function(json){	
         var s = json.split("||");
         console.log(json+"#"+s[0]+"#"+s[1]);
         $("#rujukan_diagRujukan").val(s[0]);
         $("#rujukan_namadiagRujukan").val(s[1]);
         $("#smis-chooser-modal").smodal("hide");
     };
 
     rujukan_ppk=new TableAction("rujukan_ppk","vclaim","rujukan",new Array());
     rujukan_ppk.setSuperCommand("rujukan_ppk");
     rujukan_ppk.select=function(kode){
        var s = kode.split("||");
        $("#rujukan_ppkDirujuk").val(s[0]);
        $("#rujukan_namaDirujuk").val(s[1]);
         $("#smis-chooser-modal").smodal("hide");
     };
 
     rujukan_poli=new TableAction("rujukan_poli","vclaim","rujukan",new Array());
     rujukan_poli.setSuperCommand("rujukan_poli");
     rujukan_poli.addRegulerData = function(x){
        x['tglRujukan'] = rujukan.get("tglRencanaKunjungan");
        x['ppkDirujuk'] = rujukan.get("ppkDirujuk");
        x['ss'] = rujukan_poli.get("ss");
        return x;
     }
     rujukan_poli.select=function(kode){
        var s = kode.split("||");
        $("#rujukan_poliRujukan").val(s[0]);
        $("#rujukan_namapoliRujukan").val(s[1]);
         $("#smis-chooser-modal").smodal("hide");
     };
     
 
     rujukan_kecelakaan=new TableAction("rujukan_kecelakaan","vclaim","rujukan",new Array());
     rujukan_kecelakaan.setSuperCommand("rujukan_kecelakaan");
     rujukan_kecelakaan.addViewData = function(x){
         x['nokartu'] = $("#rujukan_noKartu").val();
         return x;
     };
     rujukan_kecelakaan.select=function(kode){
         $("#rujukan_kdKecamatan").val(kode);
         $("#smis-chooser-modal").smodal("hide");
     };

     rujukan_dokter=new TableAction("rujukan_dokter","vclaim","rujukan",new Array());
     rujukan_dokter.setSuperCommand("rujukan_dokter");
     rujukan_dokter.addViewData = function(x){
         x['jenis'] = $("#rujukan_jnsPelayanan").val();
         x['poli'] = $("#rujukan_ppkRujukan").val();
         return x;
     }
     rujukan_dokter.select = function(kode){
         $("#rujukan_dpjpLayan").val(kode);
         $("#smis-chooser-modal").smodal("hide");
     };
 
     rujukan_dpjp=new TableAction("rujukan_dpjp","vclaim","rujukan",new Array());
     rujukan_dpjp.setSuperCommand("rujukan_dpjp");
     rujukan_dpjp.addViewData = function(x){
         x['jenis'] = $("#rujukan_jnsPelayanan").val();
         x['poli'] = $("#rujukan_ppkRujukan").val();
         return x;
     }
     rujukan_dpjp.select = function(kode){
         $("#rujukan_kodeDPJP").val(kode);
         $("#smis-chooser-modal").smodal("hide");
     };
     
 
     $("#rujukan_jenisSep").on("change",function(){
        var jns= $(this).val();
        if(jns=="internal"){
            $("#update_sep").hide();
        }else{
            $("#update_sep").show();
        }
     });
 
 });