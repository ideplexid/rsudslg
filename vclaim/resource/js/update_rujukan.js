/**
 * this page used to control and vclaim
 * of patient BPJS so usign VClaim system
 * 
 * @author      : Nurul Huda
 * @since       : 17 Feb 2018
 * @license     : LGPv2
 * @copyright   : goblooge@gmail.com
 * @version     : 1.0.1
 * @used        : vclaim/resource/php/vclaim_patient/update_rujukan.php
 * 
 */
 var update_rujukan;
 var update_rujukan_icd;
 var update_rujukan_dokter;
 var update_rujukan_dpjp;
 var update_rujukan_ppk;
 var update_rujukan_poli;

 var current_pasien;
 $(document).ready(function(){
    
    $(".mydate").datepicker();
     $(".mydatetime").datetimepicker({minuteStep:1});    
     $("#update_rujukan_asalRujukan").on("click",function(){
         var vl = $(this).val();
         if(vl=="1" || vl=="2"){
             $("#update_rujukan_diambil_dari option[value='Nomor Rujukan']").show();
             $("#update_rujukan_diambil_dari option[value='Nomor BPJS']").show();
             $("#update_rujukan_diambil_dari option[value='Nomor KTP']").hide();
             $("#update_rujukan_jnsPelayanan").val(2);
             $("#update_rujukan_jnsPelayanan").prop("disabled",true);
         }else{
             $("#update_rujukan_diambil_dari option[value='Nomor Rujukan']").hide();
             $("#update_rujukan_diambil_dari option[value='Nomor BPJS']").show();
             $("#update_rujukan_diambil_dari option[value='Nomor KTP']").show();
             $("#update_rujukan_jnsPelayanan").val("");
             $("#update_rujukan_jnsPelayanan").prop("disabled",false);
         }
         $("#update_rujukan_diambil_dari").val("");
     });
 
     $("#update_rujukan_tujuanKunj").on("change",function(){
         var vl = $(this).val();
         if(vl=="0"){
             $("#update_rujukan_flagProcedure").val("");
             $("#update_rujukan_kdPenunjang").val("");
             $("#update_rujukan_assesmentPel").val(4);
         }else if(vl=="2"){
             $("#update_rujukan_assesmentPel").val(4);
         }
     });
 
     $("#update_rujukan_naikKelas").on("change",function(){
         var vl = $(this).val();
         if(vl=="0"){
             $("#update_rujukan_pembiayaan").val("");
         }
     });
     $("#update_rujukan_asalupdate_rujukan").trigger("click");
     $("#update_rujukan_no_bpjs, #update_rujukan_no_ktp ").keyup(function(e){ 
         var code = e.which;
         if(code==13)e.preventDefault();
         if(code==32||code==13||code==188||code==186){
             update_rujukan.fetch_server();
         }
     });


     
     update_rujukan = new TableAction("update_rujukan","vclaim","update_rujukan", new Array());     
    
     update_rujukan.cari_local_rujukan = function(x){
        var x = this.getRegulerData();
        x['command'] = "cari_local_rujukan";
        x['noRujukan'] = update_rujukan.get("noRujukan");
        showLoading();
        $.post("",x,function(res){
            var j = getContent(res);
            if(j!=null){
                update_rujukan.set("noSep",j.noSep);
                update_rujukan.set("tglRujukan",j.tglRujukan);
                update_rujukan.set("tglRencanaKunjungan",j.tglRencanaKunjungan);
                update_rujukan.set("ppkDirujuk",j.ppkDirujuk);
                update_rujukan.set("jnsPelayanan",j.jnsPelayanan);
                update_rujukan.set("catatan",j.catatan);
                
                update_rujukan.set("diagRujukan",j.diagRujukan);
                update_rujukan.set("tipeRujukan",j.tipeRujukan);
                update_rujukan.set("poliRujukan",j.poliRujukan);
                update_rujukan.set("namadiagRujukan",j.namadiagRujukan);

                update_rujukan.set("namaDirujuk",j.namappkDirujuk);
                update_rujukan.set("namapoliRujukan",j.namapoliRujukan);
                update_rujukan.cari();
            }
            dismissLoading();
        });
        
     }

     update_rujukan.cari = function(x){
        var x = this.getRegulerData();
        x['command'] = "edit";
        x['noSep'] = update_rujukan.get("noSep");
        showLoading();
        $.post("",x,function(res){
            var j = getContent(res);
            /*
            update_rujukan.set("asalRujukan",j.asalRujukan);
            update_rujukan.set("jnsPelayanan",j.jnsPelayanan);
            update_rujukan.set("tglSep",j.tglSep);
            update_rujukan.set("eksekutif",j.eksekutif);
            update_rujukan.set("dpjpLayan",j.dpjpLayan);
            update_rujukan.set("noMr",j.noMr);
            update_rujukan.set("tglRujukan",j.tglRujukan);
            update_rujukan.set("namaRujukan",j.namaRujukan);
            update_rujukan.set("ppkRujukan",j.ppkRujukan);
            update_rujukan.set("noRujukan",j.noRujukan);
            update_rujukan.set("diagRujukan",j.diagRujukan);
            update_rujukan.set("cob",j.cob);
            update_rujukan.set("catatan",j.catatan);
            update_rujukan.set("noTelp",j.noTelp);
            update_rujukan.set("tujuanKunj",j.tujuanKunj);
            update_rujukan.set("flagProcedure",j.flagProcedure);
            update_rujukan.set("kdPenunjang",j.kdPenunjang);
            update_rujukan.set("assesmentPel",j.assesmentPel);
            update_rujukan.set("naikKelas",j.naikKelas);
            update_rujukan.set("pembiayaan",j.pembiayaan);
            update_rujukan.set("penanggunjwb",j.penanggunjwb);
            update_rujukan.set("noSurat",j.noSurat);
            update_rujukan.set("kodeDPJP",j.kodeDPJP);
            update_rujukan.set("lakaLantas",j.lakaLantas);
            update_rujukan.set("tgl_kejadian",j.tgl_kejadian);
            update_rujukan.set("no_suplesi",j.no_suplesi);
            update_rujukan.set("lokasiLaka",j.lokasiLaka);
            update_rujukan.set("kdPropinsi",j.kdPropinsi);
            update_rujukan.set("kdKabupaten",j.kdKabupaten);
            update_rujukan.set("kdKecamatan",j.kdKecamatan);
            update_rujukan.set("keterangan",j.keterangan);
            update_rujukan.set("suplesi",j.suplesi);
            update_rujukan.set("klsRawat",j.klsRawat);
            update_rujukan.set("katarak",j.katarak);*/

            $("#update_rujukan_page").html(j.tbl);

            dismissLoading();
        });  
     }

     update_rujukan.del = function(){
        var x = this.getRegulerData();
        x['command'] = "del";
        x['noSep'] = update_rujukan.get("noSep");
        x['jenisSep'] = update_rujukan.get("jenisSep");
        x['user'] = update_rujukan.get("user");
        x['noSurat'] = update_rujukan.get("noSurat");
        x['tglRujukanInternal'] = update_rujukan.get("tglRujukan");
        x['poli'] = update_rujukan.get("poliRujukan")
        showLoading();
        $.post("",x,function(res){
            var j = getContent(res);
            dismissLoading();
        });  
     }
     
     update_rujukan.simpan=function(){
        data            = this.getRegulerData();
        data['noRujukan' ] = update_rujukan.get("noRujukan");
        data['tglRujukan' ] = update_rujukan.get("tglRujukan");
        data['tglRencanaKunjungan' ] = update_rujukan.get("tglRencanaKunjungan");
        data['ppkDirujuk' ] = update_rujukan.get("ppkDirujuk");
        data['jnsPelayanan' ] = update_rujukan.get("jnsPelayanan");
        data['catatan' ] = update_rujukan.get("catatan");
        data['diagRujukan' ] = update_rujukan.get("diagRujukan");
        data['tipeRujukan' ] = update_rujukan.get("tipeRujukan");
        data['poliRujukan' ] = update_rujukan.get("poliRujukan");    
        data['namadiagRujukan' ] = update_rujukan.get("namadiagRujukan");
        data['namappkDirujuk' ] = update_rujukan.get("namaDirujuk");
        data['namapoliRujukan' ] = update_rujukan.get("namapoliRujukan");    
        data['command'] = "save";
        showLoading();
        $.post("",data,function(res){
              var json  = getContent(res);
              dismissLoading();
        });
    };
 
     $("#update_rujukan_noRujukan").keyup(function(e){
         if (e.keyCode==13) {
             update_rujukan.cek_rujukan();
         }
     });
     
     
   
     
     update_rujukan_icd=new TableAction("update_rujukan_icd","vclaim","update_rujukan",new Array());
     update_rujukan_icd.setSuperCommand("update_rujukan_icd");
     update_rujukan_icd.select=function(json){	
         var s = json.split("||");
         console.log(s[0]+" - ".s[1]);
         $("#update_rujukan_diagRujukan").val(s[0]);
         $("#update_rujukan_namadiagRujukan").val(s[1]);
         $("#smis-chooser-modal").smodal("hide");
     };
 
     update_rujukan_ppk=new TableAction("update_rujukan_ppk","vclaim","update_rujukan",new Array());
     update_rujukan_ppk.setSuperCommand("update_rujukan_ppk");
     update_rujukan_ppk.select=function(kode){
        var s = kode.split("||");
        $("#update_rujukan_ppkDirujuk").val(s[0]);
        $("#update_rujukan_namaDirujuk").val(s[1]);
         $("#smis-chooser-modal").smodal("hide");
     };
 
     update_rujukan_poli=new TableAction("update_rujukan_poli","vclaim","update_rujukan",new Array());
     update_rujukan_poli.setSuperCommand("update_rujukan_poli");
     update_rujukan_poli.addRegulerData = function(x){
        x['tglRujukan'] = update_rujukan.get("tglRujukan");
        x['ppkDirujuk'] = update_rujukan.get("ppkDirujuk");
        x['ss'] = update_rujukan_poli.get("ss");
        return x;
     }
     update_rujukan_poli.select=function(kode){
        var s = kode.split("||");
        $("#update_rujukan_poliRujukan").val(s[0]);
        $("#update_rujukan_namapoliRujukan").val(s[1]);
         $("#smis-chooser-modal").smodal("hide");
     };
     
 
     update_rujukan_kecelakaan=new TableAction("update_rujukan_kecelakaan","vclaim","update_rujukan",new Array());
     update_rujukan_kecelakaan.setSuperCommand("update_rujukan_kecelakaan");
     update_rujukan_kecelakaan.addViewData = function(x){
         x['nokartu'] = $("#update_rujukan_noKartu").val();
         return x;
     };
     update_rujukan_kecelakaan.select=function(kode){
         $("#update_rujukan_kdKecamatan").val(kode);
         $("#smis-chooser-modal").smodal("hide");
     };

     update_rujukan_dokter=new TableAction("update_rujukan_dokter","vclaim","update_rujukan",new Array());
     update_rujukan_dokter.setSuperCommand("update_rujukan_dokter");
     update_rujukan_dokter.addViewData = function(x){
         x['jenis'] = $("#update_rujukan_jnsPelayanan").val();
         x['poli'] = $("#update_rujukan_ppkRujukan").val();
         return x;
     }
     update_rujukan_dokter.select = function(kode){
         $("#update_rujukan_dpjpLayan").val(kode);
         $("#smis-chooser-modal").smodal("hide");
     };
 
     update_rujukan_dpjp=new TableAction("update_rujukan_dpjp","vclaim","update_rujukan",new Array());
     update_rujukan_dpjp.setSuperCommand("update_rujukan_dpjp");
     update_rujukan_dpjp.addViewData = function(x){
         x['jenis'] = $("#update_rujukan_jnsPelayanan").val();
         x['poli'] = $("#update_rujukan_ppkRujukan").val();
         return x;
     }
     update_rujukan_dpjp.select = function(kode){
         $("#update_rujukan_kodeDPJP").val(kode);
         $("#smis-chooser-modal").smodal("hide");
     };
     
 
     $("#update_rujukan_jenisSep").on("change",function(){
        var jns= $(this).val();
        if(jns=="internal"){
            $("#update_sep").hide();
        }else{
            $("#update_sep").show();
        }
     });
 
 });