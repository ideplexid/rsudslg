/**
 * VClaim PRB
 * 
 * @author      : Fulan
 * @since       : 09-02-2022
 * @license     : LGPv2
 * @copyright   : -
 * @version     : 1.0.0
 * @used        : vclaim/resource/php/prb/tambah_prb.php
 * 
 */
 var add_prb;
 var add_prb_dpjp;
 var add_prb_obat;
 var ROW_COUNT;
 $(document).ready(function() {
    $(".mydate").datepicker();
    ROW_COUNT = 0;
    add_prb = new TableAction("add_prb","vclaim","tambah_prb");
    add_prb.cariSEP = function(){
        add_prb.set("noKartu", "");
        add_prb.set("jnsPelayanan", "");
        add_prb.set("poliTujuan", "");
        add_prb.set("noSRB", "");
        add_prb.set("tglSRB", "");
        add_prb.set("alamat", "");
        add_prb.set("email", "");
        add_prb.set("programPRB", "");
        add_prb.set("kodeDPJP","");
        add_prb.set("keterangan","");
        add_prb.set("saran","");
        $("#add_prb_page").html("");
        var x = this.getRegulerData();
        x['super_command'] = "add_prb_sep";
        x['command'] = "edit";
        x['noSep'] = add_prb.get("searchNoSep");
        x['jenisSep'] = add_prb.get("jenisSep");
        showLoading();
        $.post("",x,function(res){
            var j = getContent(res);
            add_prb.set("noSep", add_prb.get("searchNoSep"));
            add_prb.set("noKartu", j.noKartu);
            add_prb.set("jnsPelayanan", j.jnsPelayanan);
            add_prb.set("poliTujuan", j.poliTujuan);
            add_prb.set("noSRB", "");
            add_prb.set("tglSRB", "");
            add_prb.set("alamat", "");
            add_prb.set("email", "");
            add_prb.set("programPRB", "");
            add_prb.set("kodeDPJP", j.dpjpLayan);
            add_prb.set("keterangan", "");
            add_prb.set("saran", "");
            $("#add_prb_page").html(j.tbl);
            dismissLoading();
        });  
    };
    add_prb.addObat = function() {
        var kdObat = $("#add_prb_kdObat").val();
        var nmObat = $("#add_prb_nmObat").val();
        var signa1 = $("#add_prb_signa1").val();
        var signa2 = $("#add_prb_signa2").val();
        var jmlObat = $("#add_prb_jmlObat").val();
        if (kdObat == "" || nmObat == "" || signa1 == "" || signa2 == "" || jmlObat == "") {
            return;
        }
        $("#add_prb_dobat_list").append(
            "<tr id='add_prb_dobat_" + ROW_COUNT + "'>" +
                "<td id='dobat_kdObat'>" + kdObat + "</td>" +
                "<td id='dobat_nmObat'>" + nmObat + "</td>" +
                "<td id='dobat_signa1'>" + signa1 + "</td>" +
                "<td id='dobat_signa2'>" + signa2 + "</td>" +
                "<td id='dobat_jmlObat'>" + jmlObat + "</td>" +
                "<td>" + 
                    "<a href='#' onclick='add_prb.delObat(\"add_prb_dobat_" + ROW_COUNT + "\")' data-content='Hapus' data-toggle='popover' class='input btn btn-danger'>" +
                        "<i class='fa fa-trash'></i>" +
                    "</a>" +
                "</td>" +
            "</tr>"
        );
        ROW_COUNT++;
        $("#add_prb_kdObat").val("");
        $("#add_prb_nmObat").val("");
        $("#add_prb_signa1").val("");
        $("#add_prb_signa2").val("");
        $("#add_prb_jmlObat").val("");
    };
    add_prb.delObat = function(rowID) {
        $("#" + rowID).remove();
    }
    add_prb.save = function() {
        var data = this.getRegulerData();
        data['command'] = "add";
        data['noSep'] = add_prb.get("noSep");
        data['noKartu'] = add_prb.get("noKartu");
        data['alamat'] = add_prb.get("alamat");
        data['email'] = add_prb.get("email");
        data['programPRB'] = add_prb.get("programPRB");
        data['kodeDPJP'] = add_prb.get("kodeDPJP");
        data['keterangan'] = add_prb.get("keterangan");
        data['saran'] = add_prb.get("saran");
        var obat = {};
        var r_num = $("#add_prb_dobat_list tr").length;
        if (r_num > 0) {
            for (var i = 0; i < r_num; i++) {
                var kdObat = $("#add_prb_dobat_list tr:eq(" + i + ") td#dobat_kdObat").text();
                var signa1 = $("#add_prb_dobat_list tr:eq(" + i + ") td#dobat_signa1").text();
                var signa2 = $("#add_prb_dobat_list tr:eq(" + i + ") td#dobat_signa2").text();
                var jmlObat = $("#add_prb_dobat_list tr:eq(" + i + ") td#dobat_jmlObat").text();
                obat[i] = {
                    'kdObat'    : kdObat,
                    'signa1'    : signa1,
                    'signa2'    : signa2,
                    'jmlObat'   : jmlObat
                };
            }
        }
        data['obat'] = JSON.stringify(obat);
        showLoading();
        $.post(
            "",
            data,
            function(response) {
                var json = getContent(response);
                if (json == null) {
                    dismissLoading();
                    return;
                }
                if(json.success==1 && typeof (json.print) != undefined ){
                    $("#printing_area").html(json.print);
                    setTimeout(function(){
                        window.print();
                    },2000);
                }
                dismissLoading();
            }
        );
    };
    add_prb.clear = function() {
        add_prb.set("noSep", "");
        add_prb.set("noKartu", "");
        add_prb.set("alamat", "");
        add_prb.set("email", "");
        add_prb.set("programPRB", "");
        add_prb.set("kodeDPJP", "");
        add_prb.set("keterangan", "");
        add_prb.set("saran", "");

        add_prb.set("kdObat", "");
        add_prb.set("nmObat", "");
        add_prb.set("signa1", "");
        add_prb.set("signa2", "");
        add_prb.set("jmlObat", "");

        $("#add_prb_dobat_list").html("");
        $("#add_prb_page").html("Silakan Masukan No. BPJS atau No. KTP dibagian Pencarian dan Hasilnya akan di tampilkan !!!");
    };

    add_prb_dpjp = new TableAction("add_prb_dpjp", "vclaim", "tambah_prb", new Array());
    add_prb_dpjp.setSuperCommand("add_prb_dpjp");
    add_prb_dpjp.addViewData = function (x) {
        x['jenis'] = $("#add_prb_jnsPelayanan").val();
        x['poli'] = $("#add_prb_poliTujuan").val();
        return x;
    }
    add_prb_dpjp.select = function (kode) {
        var kd = kode.split("||");
        $("#add_prb_kodeDPJP").val(kd[0]);
        $("#smis-chooser-modal").smodal("hide");
    };

    add_prb_obat=new TableAction("add_prb_obat","vclaim","tambah_prb",new Array());
    add_prb_obat.setSuperCommand("add_prb_obat");
    add_prb_obat.select = function(kode){
        $("#add_prb_kdObat").val(kode);
        $("#add_prb_nmObat").val(this.getNamaObat(kode));
        $("#smis-chooser-modal").smodal("hide");
    };
    add_prb_obat.getNamaObat = function(kode) {
        if (kode == "") {
            return "";
        }
        var r_num = $("#add_prb_obat_list tr").length;
        for (var i = 0; i < r_num; i++) {
            var kdObat = $("#add_prb_obat_list tr:eq(" + i + ") td:eq(2)").text();
            var nmObat = $("#add_prb_obat_list tr:eq(" + i + ") td:eq(1)").text();
            if (kdObat == kode) {
                return nmObat;
            }
        }
        return "";
    }
 });