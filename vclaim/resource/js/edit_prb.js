/**
 * VClaim PRB
 * 
 * @author      : Fulan
 * @since       : 09-02-2022
 * @license     : LGPv2
 * @copyright   : -
 * @version     : 1.0.0
 * @used        : vclaim/resource/php/prb/edit_prb.php
 * 
 */
 var edit_prb;
 var edit_prb_dpjp;
 var edit_prb_obat;
 var ROW_COUNT;
 $(document).ready(function() {
    $(".mydate").datepicker();
    ROW_COUNT = 0;
    edit_prb = new TableAction("edit_prb","vclaim","edit_prb");
    edit_prb.edit = function(){
        edit_prb.set("noKartu", "");
        edit_prb.set("jnsPelayanan", "");
        edit_prb.set("poliTujuan", "");
        edit_prb.set("tglSRB", "");
        edit_prb.set("alamat", "");
        edit_prb.set("email", "");
        edit_prb.set("programPRB", "");
        edit_prb.set("kodeDPJP","");
        edit_prb.set("keterangan","");
        edit_prb.set("saran","");

        $("#edit_prb_page").html("");
        $("#edit_prb_dobat_list").html("");
        var x = this.getRegulerData();
        x['command'] = "edit";
        x['noSep'] = edit_prb.get("searchNoSep");
        x['noSRB'] = edit_prb.get("noSRB");
        showLoading();
        $.post("",x,function(res){
            var j = getContent(res);
            edit_prb.set("noSep", edit_prb.get("searchNoSep"));
            edit_prb.set("noKartu", j.noKartu);            
            edit_prb.set("tglSRB", j.tglSRB);
            edit_prb.set("alamat", j.alamat);
            edit_prb.set("email", j.email);
            edit_prb.set("programPRB", j.programPRB.trim());
            edit_prb.set("programPRB", "01");
            edit_prb.set("kodeDPJP", j.kodeDPJP);
            edit_prb.set("keterangan", j.keterangan);
            edit_prb.set("saran", j.saran);

            $(j.obat).each(function(index,value){
                $("#edit_prb_dobat_list").append(
                    "<tr id='edit_prb_dobat_" + ROW_COUNT + "'>" +
                        "<td id='dobat_kdObat'>" + value.kdObat + "</td>" +
                        "<td id='dobat_nmObat'>" + value.nmObat + "</td>" +
                        "<td id='dobat_signa1'>" + value.signa1 + "</td>" +
                        "<td id='dobat_signa2'>" + value.signa2 + "</td>" +
                        "<td id='dobat_jmlObat'>" + value.jmlObat + "</td>" +
                        "<td>" + 
                            "<a href='#' onclick='edit_prb.delObat(\"edit_prb_dobat_" + ROW_COUNT + "\")' data-content='Hapus' data-toggle='popover' class='input btn btn-danger'>" +
                                "<i class='fa fa-trash'></i>" +
                            "</a>" +
                        "</td>" +
                    "</tr>"
                );
                ROW_COUNT++;
            });
            

            edit_prb.cariSEP();
            dismissLoading();
        });  
    };

    edit_prb.cariSEP = function(){
        $("#add_prb_page").html("");
        var x = this.getRegulerData();
        x['super_command'] = "edit_prb_sep";
        x['command'] = "edit";
        x['noSep'] = edit_prb.get("searchNoSep");
        x['jenisSep'] = edit_prb.get("jenisSep");
        showLoading();
        $.post("",x,function(res){
            var j = getContent(res);
            edit_prb.set("noKartu", j.noKartu);
            edit_prb.set("jnsPelayanan", j.jnsPelayanan);
            edit_prb.set("poliTujuan", j.poliTujuan);
            $("#add_prb_page").html(j.tbl);
            dismissLoading();
        });  
    };

    edit_prb.addObat = function() {
        var kdObat = $("#edit_prb_kdObat").val();
        var nmObat = $("#edit_prb_nmObat").val();
        var signa1 = $("#edit_prb_signa1").val();
        var signa2 = $("#edit_prb_signa2").val();
        var jmlObat = $("#edit_prb_jmlObat").val();
        if (kdObat == "" || nmObat == "" || signa1 == "" || signa2 == "" || jmlObat == "") {
            return;
        }
        $("#edit_prb_dobat_list").append(
            "<tr id='edit_prb_dobat_" + ROW_COUNT + "'>" +
                "<td id='dobat_kdObat'>" + kdObat + "</td>" +
                "<td id='dobat_nmObat'>" + nmObat + "</td>" +
                "<td id='dobat_signa1'>" + signa1 + "</td>" +
                "<td id='dobat_signa2'>" + signa2 + "</td>" +
                "<td id='dobat_jmlObat'>" + jmlObat + "</td>" +
                "<td>" + 
                    "<a href='#' onclick='edit_prb.delObat(\"edit_prb_dobat_" + ROW_COUNT + "\")' data-content='Hapus' data-toggle='popover' class='input btn btn-danger'>" +
                        "<i class='fa fa-trash'></i>" +
                    "</a>" +
                "</td>" +
            "</tr>"
        );
        ROW_COUNT++;
        $("#edit_prb_kdObat").val("");
        $("#edit_prb_nmObat").val("");
        $("#edit_prb_signa1").val("");
        $("#edit_prb_signa2").val("");
        $("#edit_prb_jmlObat").val("");
    };
    edit_prb.delObat = function(rowID) {
        $("#" + rowID).remove();
    }
    edit_prb.save = function() {
        var data = this.getRegulerData();
        data['command'] = "update";
        data['noSep'] = edit_prb.get("noSep");
        data['noSrb'] = edit_prb.get("noSRB");
        data['noKartu'] = edit_prb.get("noKartu");
        data['alamat'] = edit_prb.get("alamat");
        data['email'] = edit_prb.get("email");
        data['programPRB'] = edit_prb.get("programPRB");
        data['kodeDPJP'] = edit_prb.get("kodeDPJP");
        data['keterangan'] = edit_prb.get("keterangan");
        data['saran'] = edit_prb.get("saran");
        var obat = {};
        var r_num = $("#edit_prb_dobat_list tr").length;
        if (r_num > 0) {
            for (var i = 0; i < r_num; i++) {
                var kdObat = $("#edit_prb_dobat_list tr:eq(" + i + ") td#dobat_kdObat").text();
                var signa1 = $("#edit_prb_dobat_list tr:eq(" + i + ") td#dobat_signa1").text();
                var signa2 = $("#edit_prb_dobat_list tr:eq(" + i + ") td#dobat_signa2").text();
                var jmlObat = $("#edit_prb_dobat_list tr:eq(" + i + ") td#dobat_jmlObat").text();
                obat[i] = {
                    'kdObat'    : kdObat,
                    'signa1'    : signa1,
                    'signa2'    : signa2,
                    'jmlObat'   : jmlObat
                };
            }
        }
        data['obat'] = JSON.stringify(obat);
        showLoading();
        $.post(
            "",
            data,
            function(response) {
                var json = getContent(response);
                if (json == null) {
                    dismissLoading();
                    return;
                }
                if(json.success==1 && typeof (json.print) != undefined ){
                    $("#printing_area").html(json.print);
                    setTimeout(function(){
                        window.print();
                    },2000);
                }
                dismissLoading();
            }
        );
    };
    edit_prb.clear = function() {
        edit_prb.set("noSep", "");
        edit_prb.set("noKartu", "");
        edit_prb.set("alamat", "");
        edit_prb.set("email", "");
        edit_prb.set("programPRB", "");
        edit_prb.set("kodeDPJP", "");
        edit_prb.set("keterangan", "");
        edit_prb.set("saran", "");

        edit_prb.set("kdObat", "");
        edit_prb.set("nmObat", "");
        edit_prb.set("signa1", "");
        edit_prb.set("signa2", "");
        edit_prb.set("jmlObat", "");

        $("#edit_prb_dobat_list").html("");
        $("#edit_prb_page").html("Silakan Masukan No. BPJS atau No. KTP dibagian Pencarian dan Hasilnya akan di tampilkan !!!");
    };

    edit_prb_dpjp = new TableAction("edit_prb_dpjp", "vclaim", "edit_prb", new Array());
    edit_prb_dpjp.setSuperCommand("edit_prb_dpjp");
    edit_prb_dpjp.addViewData = function (x) {
        x['jenis'] = $("#edit_prb_jnsPelayanan").val();
        x['poli'] = $("#edit_prb_poliTujuan").val();
        return x;
    }
    edit_prb_dpjp.select = function (kode) {
        var kd = kode.split("||");
        $("#edit_prb_kodeDPJP").val(kd[0]);
        $("#smis-chooser-modal").smodal("hide");
    };

    edit_prb_obat=new TableAction("edit_prb_obat","vclaim","edit_prb",new Array());
    edit_prb_obat.setSuperCommand("edit_prb_obat");
    edit_prb_obat.select = function(kode){
        $("#edit_prb_kdObat").val(kode);
        $("#edit_prb_nmObat").val(this.getNamaObat(kode));
        $("#smis-chooser-modal").smodal("hide");
    };
    edit_prb_obat.getNamaObat = function(kode) {
        if (kode == "") {
            return "";
        }
        var r_num = $("#edit_prb_obat_list tr").length;
        for (var i = 0; i < r_num; i++) {
            var kdObat = $("#edit_prb_obat_list tr:eq(" + i + ") td:eq(2)").text();
            var nmObat = $("#edit_prb_obat_list tr:eq(" + i + ") td:eq(1)").text();
            if (kdObat == kode) {
                return nmObat;
            }
        }
        return "";
    }
 });