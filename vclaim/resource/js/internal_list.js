var internal_list;
var internal_list_dokter;
var internal_list_poli;
$(document).ready(function(){
    $(".mydate").datepicker();
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});

    internal_list_dokter=new TableAction("internal_list_dokter","vclaim","internal_list",new Array());
    internal_list_dokter.setSuperCommand("internal_list_dokter");
    internal_list_dokter.addViewData = function(x){
        x['pelayanan'] = internal_list.get("jenis");
        x['tgl_internal_list'] = internal_list.get("tgl_internal_list");
        x['kode_poli'] = internal_list.get("kode_poli");
        return x;
        
    };
    internal_list_dokter.select = function(kode){
        internal_list.set("kode_dokter",kode);
        $("#smis-chooser-modal").smodal("hide");
    };

    internal_list_poli=new TableAction("internal_list_poli","vclaim","internal_list",new Array());
    internal_list_poli.setSuperCommand("internal_list_poli");
    internal_list_poli.addViewData = function(x){
        x['pelayanan'] = internal_list.get("jenis");
        x['tgl_internal_list'] = internal_list.get("tgl_internal_list");
        x['nomor'] = x['pelayanan']=="1"?internal_list.get("kartu"):internal_list.get("nosep");
        return x;
        
    };
    internal_list_poli.select = function(kode){
        internal_list.set("kode_poli",kode);
        $("#smis-chooser-modal").smodal("hide");
    };

	var column=new Array('id','diagnosa',"jenis",'tgl_lahir','nosep','nama','dokter','kode_dokter','kode_poli','poli','tgl_sep',"tgl_internal_list","kartu");
	internal_list=new TableAction("internal_list","vclaim","internal_list",column);
    internal_list.addViewData=function(x){
        x['s_tahun'] = internal_list.get("s_tahun");
        x['s_bulan'] = internal_list.get("s_bulan");
        return x;
    }




});