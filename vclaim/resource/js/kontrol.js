var kontrol;
var kontrol_dokter;
var kontrol_poli;
$(document).ready(function(){
    $(".mydate").datepicker();
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
    $("#kontrol_jenis").on("change",function(){
        if($(this).val()=="1"){
            $("#kontrol_nosep").prop("disabled",true);
            $("#kontrol_kartu").prop("disabled",false);
        }else{
            $("#kontrol_kartu").prop("disabled",true);
            $("#kontrol_nosep").prop("disabled",false);
        }
    });
    kontrol_dokter=new TableAction("kontrol_dokter","vclaim","kontrol",new Array());
    kontrol_dokter.setSuperCommand("kontrol_dokter");
    kontrol_dokter.addViewData = function(x){
        x['pelayanan'] = kontrol.get("jenis");
        x['tgl_kontrol'] = kontrol.get("tgl_kontrol");
        x['kode_poli'] = kontrol.get("kode_poli");
        return x;
        
    };
    kontrol_dokter.select = function(kode){
        var s = kode.split("||");
        kontrol.set("kode_dokter",s[0]);
        kontrol.set("dokter",s[1]);
        $("#smis-chooser-modal").smodal("hide");
    };

    kontrol_poli=new TableAction("kontrol_poli","vclaim","kontrol",new Array());
    kontrol_poli.setSuperCommand("kontrol_poli");
    kontrol_poli.addViewData = function(x){
        x['pelayanan'] = kontrol.get("jenis");
        x['tgl_kontrol'] = kontrol.get("tgl_kontrol");
        x['nomor'] = x['pelayanan']=="1"?kontrol.get("kartu"):kontrol.get("nosep");
        return x;
        
    };
    kontrol_poli.select = function(kode){
        kontrol.set("kode_poli",kode);
        $("#smis-chooser-modal").smodal("hide");
    };

	var column=new Array('id','diagnosa',"jenis",'tgl_lahir','nosep','nama','dokter','kode_dokter','kode_poli','poli','tgl_sep',"tgl_kontrol","kartu");
	kontrol=new TableAction("kontrol","vclaim","kontrol",column);
    kontrol.addViewData=function(x){
        x['s_dari'] = kontrol.get("s_dari");
        x['s_sampai'] = kontrol.get("s_sampai");
        x['s_filter'] = kontrol.get("s_filter");
        return x;
    }

    kontrol.more_option = function(idx){
        if(idx=='kontrol_nosep'){
            kontrol.cari_sep($("#"+idx).val());
        }
    };

    kontrol.printelement = function(id){
        var data		= this.getRegulerData();
        data['command']	= 'print-element';
        data['slug']	= 'print-element';
        data['id']		= id;
        showLoading();
        $.post("",data,function(res){
            dismissLoading();
            var json = getContent(res);
            console.log(json);
            var getUrl = window.location['pathname']+json;
            window.open(getUrl, 'pdf');
        });
        return this;
    } 

    kontrol.cari_sep = function(nosep){
        var d = this.getRegulerData();
        d['nosep'] = nosep;
        d['command'] = "cari_sep";
        showLoading();
        $.post("",d,function(res){
            var json = getContent(res);
            if(json!=null){
                kontrol.set("tgl_sep",json.tgl_sep);
                kontrol.set("nama",json.nama);
                kontrol.set("kartu",json.kartu);
                kontrol.set("tgl_lahir",json.tgl_lahir);
                kontrol.set("diagnosa",json.diagnosa);                
                kontrol.set("kode_poli",json.kode_poli);
                kontrol.set("poli",json.poli);
                kontrol.set("dokter",json.dokter);
                kontrol.set("kode_dokter",json.kode_dokter);
            }
            dismissLoading();
        });
    }

    kontrol.view=function(){	
        var self		= this;
        var view_data	= this.getViewData();

        if(view_data["s_sampai"]=='undefined-undefined-' || view_data["s_dari"]=='undefined-undefined-' ){
            showWarning("Error",'silakan isikan range tanggal yang benar');
            return;
        }
        showLoading();
        $.post('',view_data,function(res){
            var json=getContent(res);
            if(json==null) {
                
            }else{
                $("#"+self.prefix+"_list").html(json.list);
                $("#"+self.prefix+"_pagination").html(json.pagination);	
                self.initPopover();
            }
            self.afterview(json);
            dismissLoading();
        });
        return this;
    };

    kontrol.save=function (){
        if(!this.cekSave()){
            return;
        }
        this.beforesave();
        var self	= this;
        var a		= this.getSaveData();
        showLoading();
        $.ajax({url:"",data:a,type:'post',success:function(res){
            var json = getContent(res);
            if(json==null) {
                $("#"+self.prefix+"_add_form").smodal('hide');
                return;		
            }else if(json.success=="1"){
                if(self.multiple_input) {
                    if(self.focus_on_multi!=""){
                        $('#'+self.action+'_'+self.focus_on_multi).focus();
                    }else{
                        $('#'+self.action+'_add_form').find('[autofocus]').focus();					
                    }
                }else {
                    $("#"+self.prefix+"_add_form").smodal('hide');
                }
                self.view();
                self.clear();

                if(typeof json.print !== 'undefined'){
                    console.log(json.print);
                    var getUrl = window.location['pathname']+json.print;
                    setTimeout(function(){
                        window.open(getUrl, 'pdf');
                    },2000);                   
                }

            }
            dismissLoading();

            if(json['nosurat']!=""){
                if($('#cek_vklaim_noSurat').length){
                    $('#cek_vklaim_noSurat').val(json['nosurat']);
                    $('#cek_vklaim_namaDPJP').val(json['dpjp']);
                    $('#cek_vklaim_kodeDPJP').val(json['kode']);
                }
            }

            self.is_post_action_del=false;
            if(a['id']!=""){
                self.postAction(a['id']);    
            }else{
                self.postAction(json['id']);
            }

            

        }});
        this.aftersave();
        return this;
    };


});