<?php
    global $db;
    show_error();
	$header=array ('No. BPJS','Nama','Tanggal SEP',"Jenis Pelayanan","Jenis Pengajuan", "Keterangan","Status");
	$uitable = new Table ( $header , "aproval_penjaminan", NULL, true );
	$uitable->setName ( 'aproval_penjaminan' );
	$uitable->setAddButtonEnable(false);
    $uitable->setDelButtonEnable(false);
    $uitable->setEditButtonEnable(false);
    $uitable->setReloadButtonEnable(false);
    $uitable->setPrintButtonEnable(false);
    
    $b = new Button("","","Approve");
    $b->setClass("btn btn-warning");
    $uitable->addContentButton("approve",$b);

	if (isset ( $_POST ['command'] )) {
        require_once "vclaim/class/responder/VClaimPenjaminanResponder.php";
		$adapter = new SimpleAdapter();
		$adapter ->add("Nama", "nama_peserta")
				 ->add("No. BPJS", "noKartu")
				 ->add("Tanggal SEP", "tglSep","date d M Y")
				 ->add("Jenis Pelayanan", "jnsPelayanan","trivial_1_Rawat Inap_Rawat Jalan")
				 ->add("Jenis Pengajuan", "jnsPengajuan","trivial_1_Back Date_Fingerprint")
				 ->add("Keterangan", "keterangan")
                 ->add("Status", "status","trivial_0_Belum di Approve_Suda di Approve");
		$dbtable = new DBTable ( $db, 'smis_vclaim_penjaminan' );
		$dbresponder = new VClaimPenjaminanResponder ( $dbtable, $uitable, $adapter );
		$datapasien = $dbresponder->command ( $_POST ['command'] );
		echo json_encode ( $datapasien );
		return;
	}
	

	
	echo $uitable->getHtml ();
	echo addJS ( "framework/smis/js/table_action.js" );
	echo addCSS ( "framework/bootstrap/css/datepicker.css" );
	echo addJS ( "framework/bootstrap/js/bootstrap-datepicker.js" );

?>
<script type="text/javascript">
var aproval_penjaminan;
$(document).ready(function(){
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
	$('.mydate').datepicker();
	var column=new Array('id');
	aproval_penjaminan=new TableAction("aproval_penjaminan","vclaim","aproval_penjaminan",column);
	aproval_penjaminan.view();
    aproval_penjaminan.approve = function(id){
        var  d = this.getRegulerData();
        d['command'] = "approve";
        d['id'] = id;
        showLoading();
        $.post("",d,function(res){
            getContent(res);
            dismissLoading();
        });
    }
});
</script>
