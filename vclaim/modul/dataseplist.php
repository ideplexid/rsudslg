<?php
    global $db;
    show_error();
	$header=array ('Nama','No. Kartu','No. SEP',"No. Rujukan","Jenis Pelayanan","Kelas Rawat", "Poli","Diagnosa","Tgl. Pulang Sep","Tgl. Sep");
	$uitable = new Table ( $header , "", NULL, true );
	$uitable->setName ( 'dataseplist' );
	$uitable->setAddButtonEnable(false);
    $uitable->setDelButtonEnable(false);
    $uitable->setEditButtonEnable(false);
    $uitable->setReloadButtonEnable(false);
    $uitable->setPrintButtonEnable(false);
    $uitable->setPrintElementButtonEnable(true);
    $uitable->setFooterControlVisible(false);
    
    $uitable->setDetailButtonEnable(true);

	if (isset ( $_POST ['command'] )) {
        require_once "vclaim/class/responder/VClaimDataSepList.php";
		$adapter = new SimpleAdapter();
		$adapter ->add("Nama", "nama");
        $adapter ->add("No. Kartu", "nokartu");
        $adapter ->add("No. SEP", "nosep");
        $adapter ->add("No. Rujukan", "norujukan");
        $adapter ->add("Jenis Pelayanan", "jenis_pelayanan");
        $adapter ->add("Kelas Rawat", "kelas_rawat");
        $adapter ->add("Poli", "poli");
        $adapter ->add("Diagnosa", "diagnosa");
        $adapter ->add("Tgl. Pulang Sep", "tanggal_pulang","date d M Y");
        $adapter ->add("Tgl. Sep", "tanggal_sep","date d M Y");
        
        
        $dbtable = new DBTable ( $db, 'smis_vclaim_penjaminan' );
		$dbresponder = new VClaimDataSepList ( $dbtable, $uitable, $adapter );
		$datapasien = $dbresponder->command ( $_POST ['command'] );
		echo json_encode ( $datapasien );
		return;
	}
	
    $uitable->addModal("dari","date","Dari","");
    $uitable->addModal("sampai","date","Sampai","");
    $uitable->addModal("noKartu","text","No Kartu","");

    $b = new Button("","","Cari");
    $b->setClass("btn btn-warning");
    $b->setAction("dataseplist.view()");

    echo $uitable->getModal()->getForm()->addElement("",$b)->setTitle("Data SEP")->getHtml();
	echo $uitable->getHtml ();
	echo addJS ( "framework/smis/js/table_action.js" );
	echo addCSS ( "framework/bootstrap/css/datepicker.css" );
	echo addJS ( "framework/bootstrap/js/bootstrap-datepicker.js" );
    echo addCSS("registration/resource/css/sep_table.css",false);

?>
<script type="text/javascript">
var dataseplist;
$(document).ready(function(){
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
	$('.mydate').datepicker();
	var column=new Array('id');
	dataseplist=new TableAction("dataseplist","vclaim","dataseplist",column);
    dataseplist.addViewData = function(x){
        x['dari'] = dataseplist.get("dari");
        x['sampai'] = dataseplist.get("sampai");
        x['noKartu'] = dataseplist.get("noKartu");
        return x;
    };
    dataseplist.detail = function(id){
        var  d = this.getRegulerData();
        d['action'] = "datasep";
        d['command'] = null;
        d['nosep'] = id;
        LoadSmisPage(d);
    }

    dataseplist.printelement=function(id){
        var data		= this.getRegulerData();
        data['command']	= 'print-element';
        data['slug']	= 'print-element';
        data['id']		= id;
        $.post("",data,function(res){
            var json	= getContent(res);
            if(json==null) return;
            

            var getUrl = window.location['pathname']+json;
		    window.open(getUrl, 'pdf');

        });
        return this;
    };
    
});
</script>
