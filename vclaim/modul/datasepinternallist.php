<?php
    global $db;
    show_error();
	$header=array ('No.','No. BPJS','No. Surat',"Poli Asal","Poli Tujuan","Diagnosa", "Nama Dokter");
	$uitable = new Table ( $header , "", NULL, true );
	$uitable->setName ( 'datasepinternallist' );
	$uitable->setAddButtonEnable(false);
    $uitable->setEditButtonEnable(false);
    $uitable->setReloadButtonEnable(false);
    $uitable->setPrintButtonEnable(false);
    $uitable->setFooterControlVisible(false);
    

	if (isset ( $_POST ['command'] )) {
        require_once "vclaim/class/responder/VClaimDataSepInternalListResponder.php";
		$adapter = new SimpleAdapter();
        $adapter->setUseNumber(true,"No.","back.");
		$adapter ->add("No. BPJS", "no_bpjs");
        $adapter ->add("No. Surat", "no_surat");
        $adapter ->add("Poli Asal", "poli_asal");
        $adapter ->add("Poli Tujuan", "poli_tujuan");
        $adapter ->add("Diagnosa", "diagnosa");
        $adapter ->add("Nama Dokter", "nama_dokter");
        
        $dbtable = new DBTable ( $db, 'smis_vclaim_penjaminan' );
		$dbresponder = new VClaimDataSepInternalListResponder ( $dbtable, $uitable, $adapter );
		$datapasien = $dbresponder->command ( $_POST ['command'] );
		echo json_encode ( $datapasien );
		return;
	}
	
    $uitable->addModal("nosep","text","No. Sep","");

    $b = new Button("","","Cari");
    $b->setClass("btn btn-warning");
    $b->setAction("datasepinternallist.view()");

    echo $uitable->getModal()->getForm()->addElement("",$b)->setTitle("Data SEP")->getHtml();
	echo $uitable->getHtml ();
	echo addJS ( "framework/smis/js/table_action.js" );
	echo addCSS ( "framework/bootstrap/css/datepicker.css" );
	echo addJS ( "framework/bootstrap/js/bootstrap-datepicker.js" );

?>
<script type="text/javascript">
var datasepinternallist;
$(document).ready(function(){
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
	$('.mydate').datepicker();
	var column=new Array('id');
	datasepinternallist=new TableAction("datasepinternallist","vclaim","datasepinternallist",column);
    datasepinternallist.addViewData = function(x){
        x['nosep'] = datasepinternallist.get("nosep");
        return x;
    };
});
</script>
