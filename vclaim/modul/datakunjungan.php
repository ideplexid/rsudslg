<?php
    global $db;
    show_error();
	$header=array ('Nama','No. Kartu','No. SEP',"No. Rujukan","Jenis Pelayanan","Kelas Rawat", "Poli","Diagnosa","Tgl. Pulang Sep","Tgl. Sep");
	$uitable = new Table ( $header , "", NULL, true );
	$uitable->setName ( 'datakunjungan' );
	$uitable->setAddButtonEnable(false);
    $uitable->setDelButtonEnable(false);
    $uitable->setEditButtonEnable(false);
    $uitable->setReloadButtonEnable(false);
    $uitable->setPrintButtonEnable(false);
    $uitable->setFooterControlVisible(false);
    
    //$uitable->setDetailButtonEnable(true);

	if (isset ( $_POST ['command'] )) {
        require_once "vclaim/class/responder/VClaimDataKunjunganList.php";
		$adapter = new SimpleAdapter();
		$adapter ->add("Nama", "nama");
        $adapter ->add("No. Kartu", "nokartu");
        $adapter ->add("No. SEP", "nosep");
        $adapter ->add("No. Rujukan", "norujukan");
        $adapter ->add("Jenis Pelayanan", "jenis_pelayanan");
        $adapter ->add("Kelas Rawat", "kelas_rawat");
        $adapter ->add("Poli", "poli");
        $adapter ->add("Diagnosa", "diagnosa");
        $adapter ->add("Tgl. Pulang Sep", "tanggal_pulang","date d M Y");
        $adapter ->add("Tgl. Sep", "tanggal_sep","date d M Y");
        
        
        $dbtable = new DBTable ( $db, 'smis_vclaim_penjaminan' );
		$dbresponder = new VClaimDataKunjunganList ( $dbtable, $uitable, $adapter );
		$datapasien = $dbresponder->command ( $_POST ['command'] );
		echo json_encode ( $datapasien );
		return;
	}
	
    $opt = new OptionBuilder();
    $opt->add("Rawat Inap","1");
    $opt->add("Rawat Jalan","2");

    $uitable->addModal("tanggal","date","Tanggal","");
    $uitable->addModal("jenis","select","Jenis Pelayanan",$opt->getContent());

    $b = new Button("","","Cari");
    $b->setClass("btn btn-warning");
    $b->setAction("datakunjungan.view()");

    echo $uitable->getModal()->getForm()->addElement("",$b)->setTitle("Data Kunjungan")->getHtml();
	echo $uitable->getHtml ();
	echo addJS ( "framework/smis/js/table_action.js" );
	echo addCSS ( "framework/bootstrap/css/datepicker.css" );
	echo addJS ( "framework/bootstrap/js/bootstrap-datepicker.js" );

?>
<script type="text/javascript">
var datakunjungan;
$(document).ready(function(){
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
	$('.mydate').datepicker();
	var column=new Array('id');
	datakunjungan=new TableAction("datakunjungan","vclaim","datakunjungan",column);
    datakunjungan.addViewData = function(x){
        x['tanggal'] = datakunjungan.get("tanggal");
        x['jenis'] = datakunjungan.get("jenis");
        return x;
    };
	datakunjungan.view();
    datakunjungan.approve = function(id){
        var  d = this.getRegulerData();
        d['command'] = "approve";
        d['id'] = id;
        showLoading();
        $.post("",d,function(res){
            getContent(res);
            dismissLoading();
        });
    }
});
</script>
