<?php 
/**
 * this page used to control and registration
 * of patient BPJS so usign VClaim system
 * 
 * @author      : Nurul Huda
 * @since       : 17 Feb 2018
 * @license     : LGPv2
 * @copyright   : goblooge@gmail.com
 * @version     : 1.2.1
 * @database    : - smis_rg_sep
 * 
 */
global $db;
$uitable            = new Table("");
$uitable->setName("rujukan");

if(isset($_POST['super_command']) && $_POST['super_command']!=""){
    $super          = new SuperCommand ();

    require_once "vclaim/class/adapter/KodeAdapter.php";
    if($_POST['super_command']=="rujukan_icd"){
        $adapt = new KodeAdapter();
        $adapt->setUseNumber(true,"No.","back.");
        $adapt->add("Nama","nama");
        //$adapt->add("id","kode");
        $adapt->add("Kode","kode");
        $table = new Table(array("No.","Nama","Kode"));
        $table->setName($_POST['super_command']);
        $table->setModel(Table::$SELECT);
        require_once "registration/class/responder/VclaimReffDiagnosa.php";
        $super->addResponder ( $_POST['super_command'], new VclaimReffDiagnosa(new DBTable($db,"smis_rg_patient",array("id")),$table,$adapt) );
    }else  if($_POST['super_command']=="rujukan_dokter" || $_POST['super_command']=="rujukan_dpjp"){
        $adapt = new KodeAdapter();
        $adapt->setUseNumber(true,"No.","back.");
        $adapt->add("Nama","nama");
        //$adapt->add("id","kode");
        $adapt->add("Kode","kode");
        $table = new Table(array("No.","Nama","Kode"));
        $table->setName($_POST['super_command']);
        $table->setModel(Table::$SELECT);
        require_once "registration/class/responder/VClaimReffDokter.php";
        $super->addResponder ( $_POST['super_command'], new VClaimReffDokter(new DBTable($db,"smis_rg_patient",array("id")),$table,$adapt) );
    }else if($_POST['super_command']=="rujukan_ppk"){
        $adapt1 = new KodeAdapter();
        $adapt1->setUseNumber(true,"No.","back.");
        $adapt1->add("Nama","nama");
        //$adapt1->add("id","kode");
        $adapt1->add("Kode","kode");
        $table1 = new Table(array("No.","Nama","Kode"));
        $table1->setName("rujukan_ppk");
        $table1->setModel(Table::$SELECT);
        require_once "vclaim/class/responder/VClaimReffPPK.php";
        $super->addResponder ( "rujukan_ppk", new VClaimReffPPK(new DBTable($db,"smis_rg_patient",array("id")),$table1,$adapt1) );    
    }else if($_POST['super_command']=="rujukan_poli"){
        $adapt1 = new KodeAdapter();
        $adapt1->setUseNumber(true,"No.","back.");
        $adapt1->add("Nama","namaSpesialis");
        //$adapt1->add("id","kodeSpesialis");
        $adapt1->add("Kode","kodeSpesialis");
        if(isset($_POST['ss']) && $_POST['ss']=="Sarana"){
            $adapt1->add("Nama","namaSarana");
            $adapt1->add("id","kodeSarana");
            $adapt1->add("Kode","kodeSarana");
        }
        
        $table1 = new Table(array("No.","Nama","Kode"));

        $tr = "<tr>
                    <th>Pilih</th>
                    <th>
                        <select id='rujukan_poli_ss'>
                            <option value='Spesialis'>Spesialis</option>
                            <option value='Sarana'>Sarana</option>
                        </select>
                    </th>
                    <th>
                        <a href='#' class='btn btn-primary' onclick='rujukan_poli.view()' ><i class='fa fa-refresh'> </i> Cari</a>
                    </th>
                </tr>";
        $table1->addHeader("before",$tr);
        $table1->setName("rujukan_poli");
        $table1->setModel(Table::$SELECT);
        $table1->setFooterVisible(false);
        require_once "vclaim/class/responder/VClaimReffSpesialistik.php";
        $super->addResponder ( "rujukan_poli", new VClaimReffSpesialistik(new DBTable($db,"smis_rg_patient",array("id")),$table1,$adapt1) );    
    }

    $init           = $super->initialize ();
    if ($init != null) {
        echo $init;
        return;
    }
}

if(isset($_POST['command']) &&  $_POST['command']!=""){
    require_once "vclaim/class/responder/VClaimRujukanResponder.php";
    $dbtable    = new DBTable($db,"smis_rg_sep");
    $adapter    = new SimpleAdapter();
    $dbres      = new VClaimRujukanResponder($dbtable,$uitable,$adapter);
    $data       = $dbres->command($_POST['command']);
    echo json_encode($data);
    return;
}

$uitable->addModal("noSep","text","No. SEP","","y",null);
$form2 = $uitable->getModal()->getForm();

$button = new Button("","","Cari");
$button->setClass("btn btn-inverse");
$button->setIsButton(Button::$ICONIC_TEXT);
$button->setIcon(" fa fa-id-card");
$button->setAction("rujukan.cari()");
$form2 ->addElement("",$button);


$uitable        ->clearContent();

$katarak = new OptionBuilder();
$katarak->add("Tidak",0,1);
$katarak->add("Ya",1);

$suplesi = new OptionBuilder();
$suplesi->add("Tidak",0,1);
$suplesi->add("Ya",1);

$jenis          = new OptionBuilder();
$jenis          ->add("","",1)
                ->add("Rawat Inap","1",0)
                ->add("Rawat Jalan","2",0);

$tipe           = new OptionBuilder();
$tipe           ->add("Penuh","0")
                ->add("Partial","1")
                ->add("Rujuk Balik","2");

$uitable ->addModal("id_pasien","hidden","","","n",null,true)
         ->addModal("noKartu","hidden","","","n",null,false)
         ->addModal("asalRujukan","hidden","","","n",null)
         ->addModal("tglRujukan","date","Tgl Rujukan",date("Y-m-d"),"n",null,false)
         ->addModal("tglRencanaKunjungan","date","tgl Rencana Kunjungan",date("Y-m-d"),"n",null,false)
         ->addModal("jnsPelayanan","select","Jenis Layanan",$jenis->getContent(),"n",null)
         ->addModal("tipeRujukan","select","Tipe Rujukan",$tipe->getContent(),"n",null)
         ->addModal("diagRujukan","hidden","","","n",null)
         ->addModal("namadiagRujukan","chooser-rujukan-rujukan_icd-ICD 10 Diagnosa","Diagnosa Rujukan","","n",null)
         ->addModal("ppkDirujuk","hidden","","","n",null,true)
         ->addModal("namaDirujuk","chooser-rujukan-rujukan_ppk-Di Rujuk Ke","Dirujuk Ke","","n",null,true)
         ->addModal("poliRujukan","hidden","","","n",null)
         ->addModal("namapoliRujukan","chooser-rujukan-rujukan_poli-Spesialis / Subspesialis","Spesialis / Subspesialis","","n",null)
         ->addModal("catatan","textarea","Catatan Rujukan","","y",null)
         ->addModal("user","hidden","",$bpjs_user,"n",null,true);
$formsep        = $uitable->getModal()->setComponentSize(Modal::$MEDIUM)->getForm();
$uitable->clearContent();


$formx = new Form("","","");
$simpan     = new Button("update_sep","","Simpan");
$simpan     ->setAction("rujukan.simpan()")
            ->setIsButton(Button::$ICONIC_TEXT)
            ->setIcon(" fa fa-save")
            ->setClass(" btn btn-primary");

$cetak     = new Button("","","Cetak");
$cetak     ->setAction("rujukan.cetak()")
            ->setIsButton(Button::$ICONIC_TEXT)
            ->setIcon(" fa fa-print")
            ->setClass(" btn btn-warning");

$btngroup       = new ButtonGroup("");
$btngroup       ->addButton($simpan);
$btngroup       ->addButton($cetak);

$formx      ->addElement("",$btngroup);

$rows           = new RowSpan();
$rows           ->addSpan("
                            <h5>BUAT RUJUKAN VCLAIM</h5>
                            ".$form2->getHtml()."
                            <div class='clear'></div>
                            <div>
                                ".$formsep->getHtml()." 
                                <div class='clear cline'></div>
                                ".$formx->getHtml()." 
                            </div>",7)
                ->addSpan("<h5>DATA PESERTA BPJS</h5><div class='clear'></div><div id='rujukan_page'>Silakan Masukan No. BPJS atau No. KTP dibagian Pencarian dan Hasilnya akan di tampilkan !!!</div>",5);

echo "<div class='clear'></div>";
echo $rows->getHtml();
echo addJS  ("framework/bootstrap/js/bootstrap-datetimepicker.js");
echo addCSS ("framework/bootstrap/css/bootstrap-datetimepicker.css");
echo addJS  ("framework/bootstrap/js/bootstrap-datepicker.js");
echo addCSS ("framework/bootstrap/css/datepicker.css");

echo addJS  ("framework/smis/js/table_action.js");
echo addJS  ("vclaim/resource/js/rujukan.js",false);
echo addCSS ("vclaim/resource/css/rujukan.css",false);
?>