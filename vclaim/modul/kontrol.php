<?php

/**
 * digunakan untuk melakukan manajemen 
 * database jenis-jenis pekerjaan atau Job Desk / bagian
 * dari masing-masing pegawai, dengan demikian
 * dapat dibedakan mana-mana pegawai dengan job desk tertentu
 * 
 * @author 		: Nurul Huda
 * @copyright 	: goblooge@gmail.com
 * @license 	: LGPLv2
 * @database 	: smis_hrd_job
 * @since		: 14 Mei 2015
 * @version		: 1.0.0
 * 
 * */

global $db;



if(isset($_POST['super_command']) && $_POST['super_command']!=""){
    $super          = new SuperCommand ();
    require_once "vclaim/class/adapter/KodeAdapter.php";
    $adapt =  new KodeAdapter();
    $adapt->setUseNumber(true,"No.","back.");
    $adapt->add("Nama","namaDokter");
    $adapt->add("Kode","kodeDokter");
    $table = new Table(array("No.","Nama","Kode"));
    $table->setName("kontrol_dokter");
    $table->setModel(Table::$SELECT);
    $table->setFooterVisible(false);
    require_once "vclaim/class/responder/VClaimReffDokterKontrol.php";
    $super->addResponder ( "kontrol_dokter", new VClaimReffDokterKontrol(new DBTable($db,"smis_hrd_job",array("id")),$table,$adapt) );
    
    $adapt = new SimpleAdapter();
    $adapt->setUseNumber(true,"No.","back.");
    $adapt->add("Nama","namaPoli");
    $adapt->add("id","kodePoli");
    $adapt->add("Kode","kodePoli");
    $adapt->add("Kapasitas","kapasitas");

    $table = new Table(array("No.","Nama","Kode","Kapasitas"));
    $table->setName("kontrol_poli");
    $table->setModel(Table::$SELECT);
    $table->setFooterVisible(false);
    require_once "vclaim/class/responder/VClaimReffKontrolPoli.php";
    $super->addResponder ( "kontrol_poli", new VClaimReffKontrolPoli(new DBTable($db,"smis_hrd_job",array("id")),$table,$adapt) );
    
    
    $init           = $super->initialize ();
    if ($init != null) {
        echo $init;
        return;
    }


    
}

require_once "smis-base/smis-include-duplicate.php";
$duplicate=getSettings($db,"hrd-job-duplicate","0");
$head=array ('No.','No. Surat','Kontrol/Inap',"Tgl. Rencana Kontrol","Tgl. Entri","No. SEP Asal","Poli  Asal","Poli Tujuan","Nama DPJP","No. Kartu","Nama Lengkap");
require_once "vclaim/class/table/KontrolTable.php";
$uitable = new KontrolTable ( $head, "", NULL, true );
$uitable->setName ( "kontrol" );
$uitable->setFooterVisible(false);
$uitable->setPrintButtonEnable(false);
$uitable->setReloadButtonEnable(false);
$uitable->setPrintElementButtonEnable(true);
if (isset ( $_POST ['command'] )) {
	$adapter = new SimpleAdapter ();
    $adapter->setUseNumber(true,"No.","back.");
	$adapter->add ( "No. Surat", "noSuratKontrol" );
	$adapter->add ( "Kontrol/Inap", "namaJnsKontrol" );
	$adapter->add ( "Tgl. Rencana Kontrol", "tglRencanaKontrol","date d M Y" );
	$adapter->add ( "Tgl. Entri", "tglTerbitKontrol" );
	$adapter->add ( "No. SEP Asal", "noSepAsalKontrol" );
    $adapter->add ( "Poli  Asal", "namaPoliAsal" );
    $adapter->add ( "Poli Tujuan", "namaPoliTujuan" );
    $adapter->add ( "Nama DPJP", "namaDokter" );
    $adapter->add ( "No. Kartu", "noKartu" );
    $adapter->add ( "Nama Lengkap", "nama" );    

	$dbtable = new DBTable ( $db, "smis_hrd_job" );
	$dbtable->setFetchMethode ( DBTable::$ARRAY_FETCH );
	require_once "vclaim/class/responder/VClaimKontrolResponder.php";
    $dbres = new VClaimKontrolResponder ( $dbtable, $uitable, $adapter );
	$data = $dbres->command ( $_POST ['command'] );
	echo json_encode ( $data );
	return;
}
$fill = new OptionBuilder();
$fill->add("Tanggal Entri","1");
$fill->add("Tanggal Rencana Kontrol","2");

$uitable->addModal ( "s_filter", "select", "Filter", $fill->getContent() );
$uitable->addModal ( "s_dari", "idate", "Dari", date("d-m-Y") );
$uitable->addModal ( "s_sampai", "idate", "Sampai", date("d-m-Y") );
$form = $uitable->getModal()->getForm();

$renanca = new OptionBuilder();
$renanca->add("Rencana Kontrol","2");
$renanca->add("Rencana Inap","1");

$uitable->clearContent();
$uitable->addModal ( "id", "hidden", "", "" );
$uitable->addModal ( "jenis", "select", "Rencana Kontrol/Inap", $renanca->getContent() );
$uitable->addModal ( "tgl_kontrol", "idate", "Tgl rencana Kontrol / Inap", "" );
$uitable->addModal ( "nosep", "text", "Nomor SEP", "","y",null,false,"kontrol" );
$uitable->addModal ( "kartu", "text", "No. Kartu", "" ,"y",null,true);
$uitable->addModal ( "poli", "hidden", "", "" );
$uitable->addModal ( "kode_poli", "chooser-kontrol-kontrol_poli-Poli", "Spesialis / Sub Spesialis", "" );
$uitable->addModal ( "tgl_sep", "text", "Tanggal SEP", "","y",null,true );
$uitable->addModal ( "dokter", "chooser-kontrol-kontrol_dokter-Dokter", "DPJP Tujuan Kontrol / Inap", "" );
$uitable->addModal ( "kode_dokter", "hidden", "", "" );
$uitable->addModal ( "nama", "text", "Nama Peserta", "","y",null,true );
$uitable->addModal ( "tgl_lahir", "text", "Tgl. Lahir", "" ,"y",null,true);
$uitable->addModal ( "diagnosa", "text", "Diagnosa", "" ,"y",null,true);


$button = new Button("","","Cari");
$button->setIsButton(Button::$ICONIC_TEXT);
$button->setIcon("fa fa-search");
$button->setAction("kontrol.view()");
$button->setClass("btn btn-primary");
$form->addElement("",$button);

$modal = $uitable->getModal ();
$modal->setTitle ( "Surat Kontrol / SPRI" );
$modal->setModalSize(Modal::$HALF_MODEL);
$modal->setComponentSize(Modal::$MEDIUM);

$form->setTitle("Surat Kontrol / SPRI");
echo $form->getHtml();
echo $uitable->getHtml ();
echo $modal->getHtml ();
echo addJS ( "framework/bootstrap/js/bootstrap-datepicker.js" );
echo addCSS ( "framework/bootstrap/css/datepicker.css" );
echo addJS ( "base-js/smis-base-loading.js");
echo addJS ( "framework/smis/js/table_action.js" );
echo addJS ( "vclaim/resource/js/kontrol.js",false );
echo addCSS ( "vclaim/resource/css/kontrol.css",false );