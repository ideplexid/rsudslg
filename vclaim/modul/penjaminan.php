<?php 
/**
 * this page used to control and registration
 * of patient BPJS so usign VClaim system
 * 
 * @author      : Nurul Huda
 * @since       : 17 Feb 2018
 * @license     : LGPv2
 * @copyright   : goblooge@gmail.com
 * @version     : 1.2.1
 * @database    : - smis_rg_sep
 * 
 */
global $db;
$uitable            = new Table("");
$uitable->setName("penjaminan");
if(isset($_POST['command']) &&  $_POST['command']!=""){
    require_once "vclaim/class/responder/VClaimPenjaminanResponder.php";
    $dbtable    = new DBTable($db,"smis_vclaim_penjaminan");
    $adapter    = new SimpleAdapter();
    $dbres      = new VClaimPenjaminanResponder($dbtable,$uitable,$adapter);
    $data       = $dbres->command($_POST['command']);
    echo json_encode($data);
    return;
}

$pengajuan      = new OptionBuilder();
$pengajuan      ->add("backdate","1",1)
                ->add("finger print","2",0);

$jenis          = new OptionBuilder();
$jenis          ->add("","",1)
                ->add("Rawat Inap","1",0)
                ->add("Rawat Jalan","2",0);

$uitable        ->addModal("noKartu","text","No. BPJS","","n",null,false,"penjaminan");
$uitable        ->addModal("nama_peserta","text","Nama Peserta","","y",null,true);
$uitable        ->addModal("tglSep","date","Tanggal SEP",date("Y-m-d"));
$uitable        ->addModal("jnsPelayanan","select","Jenis Pelayanan",$jenis->getContent());
$uitable        ->addModal("jnsPengajuan","select","Jenis Pengajuan",$pengajuan->getContent());
$uitable        ->addModal("keterangan","textarea","Keterangan","");
$uitable        ->addModal("user","hidden","",getSettings($db,"reg-bpjs-vclaim-api-user","smis"));
$form           = $uitable->getModal()->getForm();

$save       = new Button("","","Simpan");
$save       ->setAction("penjaminan.save()")
                ->setIsButton(Button::$ICONIC_TEXT)
                ->setIcon(" fa fa-save")
                ->setClass(" btn btn-primary");
               
$rows           = new RowSpan();
$rows           ->addSpan("
<h4>Penjaminan SEP</h4>
".$form->getHtml()." 
<div class='clear cline'></div>
<div >".$save->getHtml()."</div>
",4)
                ->addSpan("<h4>DATA PESERTA BPJS</h4><div class='clear'>
                                <div id='penjaminan_page'>Silakan Masukan No. BPJS atau No. KTP dibagian Pencarian dan Hasilnya akan di tampilkan !!!</div>
                            
                            ",3);

echo "<div class='clear'></div>";
echo $rows->getHtml();
echo addJS  ("framework/bootstrap/js/bootstrap-datetimepicker.js");
echo addCSS ("framework/bootstrap/css/bootstrap-datetimepicker.css");
echo addJS  ("framework/bootstrap/js/bootstrap-datepicker.js");
echo addCSS ("framework/bootstrap/css/datepicker.css");

echo addJS  ("framework/smis/js/table_action.js");
echo addJS  ("vclaim/resource/js/penjaminan.js",false);
echo addCSS ("vclaim/resource/css/penjaminan.css",false);
?>