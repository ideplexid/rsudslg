<?php 

class KodeAdapter extends SimpleAdapter{

    public function adapt($d){
        $a = parent::adapt($d);
        $a['id'] = $a['Kode']."||".$a['Nama'];
        return $a;
    }

}