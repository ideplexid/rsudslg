<?php 

class RujukanInternalTable extends Table{
	public function getBodyContent(){
		$content		= "";
		foreach($this->body_before as $h){
			$content   .= $h;
		}
		if($this->content!=NULL){
			foreach($this->content as $d){
				$content.="<tr>";
					foreach($this->header as $h){
						$content.="<td>".(isset($d[$h])?$d[$h]:"")."</td>";
					}
					if($this->is_action){
                        $ctx="";
                        if(isset($d['id'])){
                            $this->current_data=$d;
                            if($this->current_data['Kontrol/Inap']=="SPRI"){
                                $this->setDelButtonEnable(false);
								$this->setPrintElementButtonEnable(false);
                            }else{
								$this->setPrintElementButtonEnable(true);
                                $this->setDelButtonEnable(true);
                            }
                            $ctx=$this->getContentButton($d['id'])->getHtml();
                        }
						$content.="<td class='noprint'>".$ctx."</td>";
					}
				$content.="</tr>";
			}
		}
		foreach($this->body_after as $h){
			$content.=$h;
		}
		return $content;
	}

}