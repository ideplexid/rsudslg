<?php 
require_once "registration/class/responder/BPJSResponder.php";
class VClaimDataSepInternalListResponder extends BPJSResponder{

    public function delete(){
        global $querylog;
        $id = $_POST['id'];
        $idx = explode("||",$id);

        global $db;
        $request['request']['t_sep'] = array(
            "noSep" =>$idx[0],
            "noSurat" =>$idx[1],
            "tglRujukanInternal" => $idx[2],
            "kdPoliTuj"=>$idx[3],
            "user"=>getSettings($db,"reg-bpjs-vclaim-api-user",""),
        );
        $url  = $this->url_base."/SEP/Internal/delete";
        $this->do_curl($this->port,$url,"DELETE",json_encode($request));  
        if ($this->error) {
            return "cURL Error #:" . $this->error;
          } else {
              $querylog->addMessage($this->response);
              $querylog->addMessage("URL ".$url);
              $data=json_decode($this->response,true);
              if($data==null ){
                 $this->response_pack->setAlertVisible(true);
                 $this->response_pack->setAlertContent("Error","Error Komunikasi BPJS");
                  return array();
              }else if( $data['metaData']['code']!="200" ){
                 $this->response_pack->setAlertVisible(true);
                 $this->response_pack->setAlertContent($data['metaData']['code'],$data['metaData']['message']);
                  return array();
              }else{
                $final['success'] = "1";
                return $final;
            }
	}
}

    public function view(){
        $nosep = $_POST['nosep'];
        $methode    = "GET";
        $request    = "";        
        $url    = $this->url_base."/SEP/Internal/".$nosep;
        $this->do_curl($this->port,$url,$methode,$request);
        $data   = null;
        $hasil_akhir = array();
        if ($this->error) {
            $this->response_pack->setAlertVisible(true);
            $this->response_pack->setAlertContent("Error","Error Komunikasi BPJS");
        } else {
            $response   = json_decode($this->response,true);
            global $querylog;
            if($response==null || $response['metaData']['code']!="200" ){
                $this->response_pack->setAlertVisible(true);
                $this->response_pack->setAlertContent("Error",$response['metaData']['message']);
            }else{
                $querylog->addMessage($response);
                require_once "registration/function/vclaim_decrypt.php";
                $rep = array();
                if(is_string($response['response'])){
                    $hasilcompress = stringDecrypt($this->getKey(), $response['response']);
                    $hasil = decompress($hasilcompress);
                    $rep = json_decode($hasil,true);
                }else{
                    $rep = $data['response'];
                }
                $history = $rep['list'];
                $querylog->addMessage(json_encode($history));
                foreach($history as $h){
                    $one = array();
                    $one['id'] = $h["nosep"]."||".$h["nosurat"]."||".$h['tglrujukinternal']."||".$h['kdpolituj'];
                    $one['no_bpjs'] = $h["nosep"];
                    $one['no_bpjs'] = $h['nokapst'];
                    $one['no_surat'] = $h["nosurat"];
                    $one['poli_asal'] = $h["nmpoliasal"];
                    $one['poli_tujuan'] = $h["nmtujuanrujuk"];
                    $one['diagnosa'] = $h["nmdiag"];
                    $one['nama_dokter'] = $h["nmdokter"];
                    $hasil_akhir[] = $one;
                }
            }
        }

        $uidata				 = $this->adapter->getContent($hasil_akhir);
		$this->uitable->setContent($uidata);
		$list				 = $this->uitable->getBodyContent();
		$pagination			 = $this->uitable->getPagination($page,3,$max_page);		
		$json['list']		 = $list;
		if(!$this->debug) {
			unset($d['custom_kriteria']);
			unset($d['query']);
		}
		$json['debug']		 = $this->debug;
		$json['dbtable']	 = $d;
		if($this->include_adapter_data){
			$json['adapter'] = $uidata;
		}
		if($this->include_raw_data){
			$json['raw']	 = $data;
		}
		$json['pagination']	 = $pagination->getHtml();
		$json['number']		 = $page;
		$json['number_p']	 = $number;
		return $json;
    }
}