<?php 
require_once "registration/class/responder/BPJSResponder.php";
class VClaimKontrolResponder extends BPJSResponder{

    public function command($command){		
		$this->response_pack = new ResponsePackage();	
		if($command=="cari_sep"){
			$content		 = $this->cari_sep();
			$this->response_pack->setContent($content);
			$this->response_pack->setStatus(ResponsePackage::$STATUS_OK);
            return $this->response_pack->getPackage();
		}else if($command=="save"){
            $content		 = $this->save();
			$this->response_pack->setContent($content);
			$this->response_pack->setStatus(ResponsePackage::$STATUS_OK);
            return $this->response_pack->getPackage();
        }else{
            return parent::command($command);
        }
	}

    public function printing(){
		return $this-> printElement();
	}

    
    public function edit(){
        global $querylog;
        global $db;
        $url  = $this->url_base."/RencanaKontrol/noSuratKontrol/".$_POST['id'];
        $this->do_curl($this->port,$url,"GET",NULL);  
        if ($this->error) {
           return "cURL Error #:" . $this->error;
         } else {
             $querylog->addMessage($this->response);
             $querylog->addMessage("URL ".$url);
             $data=json_decode($this->response,true);
             if($data==null ){
                $this->response_pack->setAlertVisible(true);
                $this->response_pack->setAlertContent("Error","Error Komunikasi BPJS");
                 return array();
             }else if( $data['metaData']['code']!="200" ){
                $this->response_pack->setAlertVisible(true);
                $this->response_pack->setAlertContent($data['metaData']['code'],$data['metaData']['message']);
                 return array();
             }else{
               require_once "registration/function/vclaim_decrypt.php";
               $rep = array();
               if(is_string($data['response'])){
                   $hasilcompress = stringDecrypt($this->getKey(), $data['response']);
                   $hasil = decompress($hasilcompress);
                   $repx = json_decode($hasil,true);
                   $querylog->addMessage("Hasil Decrypt : ".$hasil);                   
                   $rep = $repx;
               }else{
                   $rep = $data['response'];
               }

                $final = array();
                $final['id'] =  $_POST['id'];
                $final['jenis'] =  $rep['jnsKontrol'];
                $final['tgl_kontrol'] =  $rep['tglRencanaKontrol'];
                $final['tgl_terbit'] =  $rep['tglTerbit'];
                $final['nosep'] =  $rep['sep']['noSep'];
                $final['poli'] =  $rep['namaPoliTujuan'];
                $final['kode_poli'] =  $rep['poliTujuan'];
                $final['tgl_sep'] =  $rep['sep']['tglSep'];
                $final['dokter'] =  $rep['namaDokter'];
                $final['kode_dokter'] =  $rep['kodeDokter'];
                $final['nama'] =  $rep['sep']['peserta']['nama'];
                $final['kartu'] =  $rep['sep']['peserta']['noKartu'];
                $final['tgl_lahir'] =  $rep['sep']['peserta']['tglLahir'];
                $final['diagnosa'] =  $rep['sep']['diagnosa'];
                $final['kelamin'] =  $rep['sep']['peserta']['kelamin'];
                return $final;
             }
         }
    }

    public function printSPRI($final){
        global $db;
        $tprin = new TablePrint("",true);
        $tprin->setMaxWidth(true);
        $tprin->addStyle("width","100%");
        $tprin->setDefaultBootrapClass(false);
        //vclaim/resource/image/bpjs.png
        $tprin->addColumn("<img style='width:200px; height:auto' src='smis-upload/".getSettings($db,"reg-bpjs-vclaim-logo","")."'>",2,1);
        $tprin->addColumn("<h4>SURAT PERINTAH RENCANA INAP</h4><h4>".getSettings($db,"smis_autonomous_title","")." (JST)</h4>",1,1);
        $tprin->addColumn("<h3> No. ".$final['noSPRI']."</h3>",1,1);
        $tprin->commit("title");


        $tprin->addColumn("&nbsp;",1,1);
        $tprin->addColumn("&nbsp;",1,1);
        $tprin->addColumn("&nbsp;",1,1);
        $tprin->addColumn("&nbsp;",1,1);
        $tprin->commit("title");

        $tprin->addColumn("Kepada Yth",1,1);
        $tprin->addColumn($final['namaDokter'],3,1);
        $tprin->commit("body");

        $tprin->addColumn("",1,1);
        $tprin->addColumn("Sp./Sub. ".$_POST['poliKontrol'],3,1);
        $tprin->commit("body");

        $tprin->addColumn("Mohon pemeriksaan dan penanganan lebih lanjut:",5,1);
        $tprin->commit("body");

        $tprin->addColumn("No. Kartu",1,1);
        $tprin->addColumn(":",1,1);
        $tprin->addColumn($final['noKartu'],1,1);
        $tprin->addColumn("",1,1);
        $tprin->commit("body");

        $tprin->addColumn("Nama Peserta",1,1);
        $tprin->addColumn(":",1,1);
        $tprin->addColumn($final['nama']."(".($final['kelamin']=="L"?"Laki-Laki":"Perempuan").")",1,1);
        $tprin->addColumn("",1,1);
        $tprin->commit("body");

        $tprin->addColumn("Tanggal Lahir",1,1);
        $tprin->addColumn(":",1,1);
        $tprin->addColumn(ArrayAdapter::format("date d M Y",$final['tglLahir']),1,1);
        $tprin->addColumn("",1,1);
        $tprin->commit("body");

        $tprin->addColumn("Diagnosa",1,1);
        $tprin->addColumn(":",1,1);
        $tprin->addColumn($_POST['diagnosa'],1,1);
        $tprin->addColumn("",1,1);
        $tprin->commit("body");

        $tprin->addColumn("Rencana Inap",1,1);
        $tprin->addColumn(":",1,1);
        $tprin->addColumn(ArrayAdapter::format("date d M Y",$final['tglRencanaKontrol']),1,1);
        $tprin->addColumn("",1,1);
        $tprin->commit("body");

        $tprin->addColumn("",1,1);
        $tprin->addColumn("",1,1);
        $tprin->addColumn("",1,1);
        $tprin->addColumn("Mengetahui DPJP,",1,1);
        $tprin->commit("body");

        $tprin->addColumn("&nbsp;",1,1);
        $tprin->addColumn("&nbsp;",1,1);
        $tprin->addColumn("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;",1,1);
        $tprin->addColumn("&nbsp;",1,1);
        $tprin->commit("body");

        $tgc = ArrayAdapter::format("date d M Y",$final['tglRencanaKontrol']);
        $tglct = ArrayAdapter::format("date d M Y H:i",date("Y-m-d H:i:s"));
        $tprin->addColumn("<small>Tgl Entri : ".$tgc." | Tgl Cetak : ".$tglct."</small>",3,1);
        //$tprin->addColumn(strtoupper($final['namaDokter']),1,1);
        $tprin->addColumn("",1,1);
        $tprin->commit("body");

        $css = "";//getSettings($db,"reg-bpjs-vclaim-print-css","");

        require_once 'smis-libs-out/mpdf-7.1.0/vendor/autoload.php';
        include 'smis-libs-out/mpdf-7.1.0/src/Mpdf.php';
        
        $pathfile = "smis-temp/SRK-".$_POST['id'].".pdf";     
        $mpdfConfig = array(
           'mode' => 'utf-8', 
           'format' => [215,140]  
        );
        //['mode' => 'utf-8', 'format' => 'A4-P']
        $mpdf = new \Mpdf\Mpdf($mpdfConfig);
        $print_data = "<html><body>".$tprin->getHtml().$css."</body></html>";
        $mpdf->AddPageByArray([
           'margin-left' => 10,
           'margin-right' => 0,
           'margin-top' => 10,
           'margin-bottom' => 0,
       ]);
        
        $mpdf->WriteHTML($print_data,0);
        $mpdf->SetDisplayMode('fullpage');
        $mpdf->Output($pathfile, "F");
        return $pathfile;
    }


    public function printElement(){
        global $db;
        $final = $this->edit();
        $tprin = new TablePrint("",true);
        $tprin->addStyle("width","100%");
        $tprin->setMaxWidth(true);
        $tprin->setDefaultBootrapClass(false);

        $tprin->addColumn("<img style='width:200px; height:auto' src='smis-upload/".getSettings($db,"reg-bpjs-vclaim-logo","")."'>",2,1);
        $tprin->addColumn("<h4>SURAT RENCANA KONTROL</h4><h4>".getSettings($db,"smis_autonomous_title","")." (JST)</h4>",1,1);
        $tprin->addColumn("<h3> No. ".$_POST['id']."</h3>",1,1);
        $tprin->commit("title");


        $tprin->addColumn("&nbsp;",1,1);
        $tprin->addColumn("&nbsp;",1,1);
        $tprin->addColumn("&nbsp;",1,1);
        $tprin->addColumn("&nbsp;",1,1);
        $tprin->commit("title");

        $tprin->addColumn("Kepada Yth",1,1);
        $tprin->addColumn($final['dokter'],3,1);
        $tprin->commit("body");

        $tprin->addColumn("",1,1);
        $tprin->addColumn("Sp./Sub. ".$final['poli'],3,1);
        $tprin->commit("body");

        $tprin->addColumn("Mohon pemeriksaan dan penanganan lebih lanjut:",5,1);
        $tprin->commit("body");

        $tprin->addColumn("No. Kartu",1,1);
        $tprin->addColumn(":",1,1);
        $tprin->addColumn($final['kartu'],1,1);
        $tprin->addColumn("",1,1);
        $tprin->commit("body");

        $tprin->addColumn("Nama Peserta",1,1);
        $tprin->addColumn(":",1,1);
        $tprin->addColumn($final['nama']."(".($final['kelamin']=="L"?"Laki-Laki":"Perempuan").")",1,1);
        $tprin->addColumn("",1,1);
        $tprin->commit("body");

        $tprin->addColumn("Tanggal Lahir",1,1);
        $tprin->addColumn(":",1,1);
        $tprin->addColumn(ArrayAdapter::format("date d M Y",$final['tgl_lahir']),1,1);
        $tprin->addColumn("",1,1);
        $tprin->commit("body");

        $tprin->addColumn("Diagnosa",1,1);
        $tprin->addColumn(":",1,1);
        $tprin->addColumn($final['diagnosa'],1,1);
        $tprin->addColumn("",1,1);
        $tprin->commit("body");

        $tprin->addColumn("Rencana Kontrol",1,1);
        $tprin->addColumn(":",1,1);
        $tprin->addColumn(ArrayAdapter::format("date d M Y",$final['tgl_kontrol']),1,1);
        $tprin->addColumn("",1,1);
        $tprin->commit("body");

        $tprin->addColumn("Demikian atas bantuannya, diucapkan banyak terima kasih",4,1);
        $tprin->commit("body");

        $tprin->addColumn("",1,1);
        $tprin->addColumn("",1,1);
        $tprin->addColumn("",1,1);
        $tprin->addColumn("Mengetahui DPJP,",1,1);
        $tprin->commit("body");

        $tprin->addColumn("&nbsp;",1,1);
        $tprin->addColumn("&nbsp;",1,1);
        $tprin->addColumn("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;",1,1);
        $tprin->addColumn("&nbsp;",1,1);
        $tprin->commit("body");

        //$tprin->addColumn("<small>Tgl Entri : ".ArrayAdapter::format("date d M Y",$final['tgl_terbit'])." | Tgl Cetak : ".ArrayAdapter::format("date d M Y h:i A",date("Y-m-d h:i:s")."</small>"),3,1);

        $tgc = ArrayAdapter::format("date d M Y",$final['tgl_terbit']);
        $tglct = ArrayAdapter::format("date d M Y H:i",date("Y-m-d H:i:s"));
        $tprin->addColumn("<small>Tgl Entri : ".$tgc." | Tgl Cetak : ".$tglct."</small>",3,1);

        //$tprin->addColumn(strtoupper($final['dokter']),1,1);
        $tprin->addColumn("",1,1);
        $tprin->commit("body");


        $css = "";//getSettings($db,"reg-bpjs-vclaim-print-css","");

        require_once 'smis-libs-out/mpdf-7.1.0/vendor/autoload.php';
        include 'smis-libs-out/mpdf-7.1.0/src/Mpdf.php';
        
        $pathfile = "smis-temp/SRK-".$_POST['id'].".pdf";     
        $mpdfConfig = array(
           'mode' => 'utf-8', 
           'format' => [215,140]  
        );
        //['mode' => 'utf-8', 'format' => 'A4-P']
        $mpdf = new \Mpdf\Mpdf($mpdfConfig);
        $print_data = "<html><body>".$tprin->getHtml().$css."</body></html>";
        $mpdf->AddPageByArray([
           'margin-left' => 10,
           'margin-right' => 0,
           'margin-top' => 10,
           'margin-bottom' => 0,
       ]);
        
        $mpdf->WriteHTML($print_data,0);
        $mpdf->SetDisplayMode('fullpage');
        $mpdf->Output($pathfile, "F");
        return $pathfile;

        //return $tprin ->getHtml();
	}


    public function delete(){
        global $querylog;
        $id = $_POST['id'];
        global $db;
        $request['request']['t_suratkontrol'] = array(
            "noSuratKontrol" =>$id, 
            "user" => getSettings($db,"reg-bpjs-vclaim-api-user",""),
        );
        $url  = $this->url_base."/RencanaKontrol/Delete";
        $this->do_curl($this->port,$url,"DELETE",json_encode($request));  
        if ($this->error) {
            return "cURL Error #:" . $this->error;
          } else {
              $querylog->addMessage($this->response);
              $querylog->addMessage("URL ".$url);
              $data=json_decode($this->response,true);
              if($data==null ){
                 $this->response_pack->setAlertVisible(true);
                 $this->response_pack->setAlertContent("Error","Error Komunikasi BPJS");
                  return array();
              }else if( $data['metaData']['code']!="200" ){
                 $this->response_pack->setAlertVisible(true);
                 $this->response_pack->setAlertContent($data['metaData']['code'],$data['metaData']['message']);
                  return array();
              }else{
                $final['success'] = "1";
                return $final;
            }
        }
    }


	

    public function view(){	
		$d					 = $this->getKontrol($_POST['s_dari'],$_POST['s_sampai'],$_POST['s_filter']);
		$page				 = 1;
		$data				 = $d;
		$max_page			 = 1;

		$this->adapter->setNumber($page*1000);
		$uidata				 = $this->adapter->getContent($data);
		$this->uitable->setContent($uidata);
		$list				 = $this->uitable->getBodyContent();
		$json['list']		 = $list;

		if(!$this->debug) {
			unset($d['custom_kriteria']);
			unset($d['query']);
		}
		$json['debug']		 = $this->debug;
		$json['dbtable']	 = $d;
		if($this->include_adapter_data){
			$json['adapter'] = $uidata;
		}
		if($this->include_raw_data){
			$json['raw']	 = $data;
		}
		$json['pagination']	 = "";
		$json['number']		 = 1;
		$json['number_p']	 = 1;
		return $json;
	}

    public function cari_sep(){
        global $querylog;
        global $db;
        $url  = $this->url_base."/RencanaKontrol/nosep/".$_POST['nosep'];
        $this->do_curl($this->port,$url,"GET",NULL);  
        if ($this->error) {
           return "cURL Error #:" . $this->error;
         } else {
             $querylog->addMessage($this->response);
             $querylog->addMessage("URL ".$url);
             $data=json_decode($this->response,true);
             if($data==null ){
                $this->response_pack->setAlertVisible(true);
                $this->response_pack->setAlertContent("Error","Error Komunikasi BPJS");
                 return array();
             }else if( $data['metaData']['code']!="200" ){
                $this->response_pack->setAlertVisible(true);
                $this->response_pack->setAlertContent($data['metaData']['code'],$data['metaData']['message']);
                 return array();
             }else{
               require_once "registration/function/vclaim_decrypt.php";
               $rep = array();
               if(is_string($data['response'])){
                   $hasilcompress = stringDecrypt($this->getKey(), $data['response']);
                   $hasil = decompress($hasilcompress);
                   $repx = json_decode($hasil,true);
                   $querylog->addMessage("Hasil Decrypt : ".$hasil);                   
                   $rep = $repx;
               }else{
                   $rep = $data['response'];
               }
               $final = array();
               $final['tgl_sep'] = $rep['tglSep'];
               $final['nama'] = $rep['peserta']['nama'];
               $final['kartu'] = $rep['peserta']['noKartu'];
               $final['tgl_lahir'] = $rep['peserta']['tglLahir'];
               $final['diagnosa'] = $rep['diagnosa'];
               $pl = explode(" - ",$rep['poli']);
               $final['kode_poli'] = $pl[0];
               $final['poli'] = $pl[1];
               $final['dokter'] = "";
               $final['kode_dokter'] = "";
               return $final;
             }
         }
    }

   
    public function save(){
        global $db;
        global $querylog;
        $request = array();
        $mthode = "POST";
        if($_POST['id']==""){
            $request['request'] = array(
                "noSEP" => $_POST['nosep'], 
                "kodeDokter" => $_POST['kode_dokter'], 
                "poliKontrol" => $_POST['kode_poli'], 
                "tglRencanaKontrol" => $_POST['tgl_kontrol'], 
                "user" => getSettings($db,"reg-bpjs-vclaim-api-user",""), 
                "noKartu" => $_POST['kartu'], 
            );
           
             /**rencana inap */
             $url  = $this->url_base."RencanaKontrol/InsertSPRI";
             if($_POST['jenis']=="2"){
                 /**rencana kontrol jenis = 2 */
                $url  = $this->url_base."RencanaKontrol/insert";
                unset($request['request']['noKartu']);
             }else{
                 /**SPRI jenis = 1 */
                unset($request['request']['noSEP']);
             }    
        }else{
            if($_POST['jenis']=="2"){
                /**rencana kontrol jenis = 2 */
                $mthode = "PUT";
               $url  = $this->url_base."RencanaKontrol/Update";
               $request['request'] = array(
                    "noSuratKontrol" => $_POST['id'], 
                    "noSEP" => $_POST['nosep'], 
                    "kodeDokter"=>$_POST['kode_dokter'],
                    "poliKontrol" => $_POST['kode_poli'], 
                    "tglRencanaKontrol" => $_POST['tgl_kontrol'], 
                    "user" => getSettings($db,"reg-bpjs-vclaim-api-user","")
               );
            }else{
                /**SPRI jenis = 1 */
                $url  = $this->url_base."RencanaKontrol/UpdateSPRI";
                $request['request'] = array(
                    "noSPRI" => $_POST['id'], 
                    "kodeDokter" => $_POST['kode_dokter'], 
                    "poliKontrol" => $_POST['kode_poli'], 
                    "tglRencanaKontrol" => $_POST['tgl_kontrol'], 
                    "user" => getSettings($db,"reg-bpjs-vclaim-api-user",""), 
                );
            }
        }


         $this->do_curl($this->port,$url,$mthode,json_encode($request));  
         if ($this->error) {
            return "cURL Error #:" . $this->error;
          } else {
              $querylog->addMessage($this->response);
              $querylog->addMessage("URL ".$url);
              $data=json_decode($this->response,true);
              if($data==null ){
                $this->response_pack->setAlertVisible(true);
                $this->response_pack->setAlertContent("Error","Error Komunikasi BPJS");
                  return array();
              }else if( $data['metaData']['code']!="200" ){
                  $this->response_pack->setAlertVisible(true);
                  $this->response_pack->setAlertContent($data['metaData']['code'],$data['metaData']['message']);
                  return array();
              }else{

                require_once "registration/function/vclaim_decrypt.php";
                $rep = array();
                if(is_string($data['response'])){
                    $hasilcompress = stringDecrypt($this->getKey(), $data['response']);
                    $hasil = decompress($hasilcompress);
                    $repx = json_decode($hasil,true);
                    $querylog->addMessage("Hasil Decrypt : ".$hasil);                   
                    $rep = $repx;
                }else{
                    $rep = $data['response'];
                }                
                if($_POST['jenis']=="2"){
                    $_POST['id'] = $rep['noSuratKontrol'];
                    $final['print'] = $this->printElement();
                    $final['nosurat'] = $rep['noSuratKontrol'];
                    $final['dpjp'] = $_POST['dokter'];
                    $final['kode'] = $_POST['kode_dokter'];
                }else{
                    $final['print'] = $this->printSPRI($rep);
                    $final['nosurat'] = "";
                    $final['dpjp'] = "";
                    $final['kode'] = "";
                }
                $querylog->addMessage($rep);
                $final['success'] = 1;
                $final['id'] = 1;
                return $final;
              }
          }
                     
    }

     public function getKontrol($tgl_awal="",$tgl_akhir="",$filter=""){
         global $querylog;
         global $db;
         $url  = $this->url_base."/RencanaKontrol/ListRencanaKontrol/tglAwal/".$tgl_awal."/tglAkhir/".$tgl_akhir."/filter/".$filter;
         $this->do_curl($this->port,$url,"GET",NULL);  
         if ($this->error) {
            return "cURL Error #:" . $this->error;
          } else {
              $querylog->addMessage($this->response);
              $querylog->addMessage("URL ".$url);
              $data=json_decode($this->response,true);
              if($data==null ){
                $this->response_pack->setAlertVisible(true);
                $this->response_pack->setAlertContent("Error","Error Komunikasi BPJS");
                  return array();
              }else if( $data['metaData']['code']!="200" ){
                $this->response_pack->setAlertVisible(true);
                $this->response_pack->setAlertContent($data['metaData']['code'],$data['metaData']['message']);
                  return array();
              }else{
                require_once "registration/function/vclaim_decrypt.php";
                $rep = array();
                if(is_string($data['response'])){
                    $hasilcompress = stringDecrypt($this->getKey(), $data['response']);
                    $hasil = decompress($hasilcompress);
                    $repx = json_decode($hasil,true);
                    $rep = $repx['list'];
                }else{
                    $rep = $data['response']['list'];
                }
                $final = array();
                foreach($rep as $x){
                    $x['id'] = $x['noSuratKontrol'];
                    $final[] = $x;    
                }
                return $final;
              }
          }

     }
}