<?php 
require_once "registration/class/responder/BPJSResponder.php";
class VClaimDataSepList extends BPJSResponder{

    public function printing(){
        $url        = $this->url_base."SEP/".$_POST['id'];
        $metode     = "GET";
        $this->do_curl($this->port,$url,$metode,NULL);
        //$one        = (array) $this->dbtable->select(array("noSEP"=>$nosep));

        if ($this->error) {
          return "cURL Error #:" . $err;
        } else {
            $data   = json_decode($this->response,true);
            if($data==null ){
                return "Terjadi Kesalahan Akibat Koneksi Server BPJS";
            }else if( $data['metaData']['code']!="200" ){
                return $data['metaData']['message'];
            }else{
                
                require_once "registration/function/vclaim_decrypt.php";
                $rep = array();
                if(is_string($data['response'])){
                    $hasilcompress = stringDecrypt($this->getKey(), $data['response']);
                    $hasil = decompress($hasilcompress);
                    $rep = json_decode($hasil,true);
                }else{
                    $rep = $data['response'];
                }
                //echo json_encode($rep);
                
                global $db;
                global $querylog;
                require_once "registration/function/bpjs_array_adapter.php";
                require_once "registration/function/bpjs_print_format.php";
                $querylog->addMessage(json_encode($rep));
                if(trim($rep['noRujukan'])==""){
                    $rep['asalRujukan'] = "-";
                }else{
                    //file_put_contents("smis-temp/ngegetch.json",json_encode($rep));
                    //$rujukan = $this->cari_rujukan($rep["noRujukan"]);
                    //$rep['asalRujukan'] = $rujukan["nama_provider"]." (".$rujukan['kd_provider'].") ";
                }
                $res    = bpjs_array_adapter_vclaim($rep,$one);
                return bpjs_print_format($db,$_POST['id'],$res);
            }
        }
	}

    public function view(){
        $dari = $_POST['dari'];
        $sampai = $_POST['sampai'];
        $noKartu = $_POST['noKartu'];
        $methode    = "GET";
        $request    = "";        
        $url    = $this->url_base."/monitoring/HistoriPelayanan/NoKartu/".$noKartu."/tglMulai/".$dari."/tglAkhir/".$sampai;
        $this->do_curl($this->port,$url,$methode,$request);
        $data   = null;
        $hasil_akhir = array();
        if ($this->error) {
            $this->response_pack->setAlertVisible(true);
            $this->response_pack->setAlertContent("Error","Error Komunikasi BPJS");
        } else {
            $response   = json_decode($this->response,true);
            global $querylog;
            if($response==null || $response['metaData']['code']!="200" ){
                $this->response_pack->setAlertVisible(true);
                $this->response_pack->setAlertContent("Error",$response['metaData']['message']);
            }else{
                $querylog->addMessage($response);
                require_once "registration/function/vclaim_decrypt.php";
                $rep = array();
                if(is_string($response['response'])){
                    $hasilcompress = stringDecrypt($this->getKey(), $response['response']);
                    $hasil = decompress($hasilcompress);
                    $rep = json_decode($hasil,true);
                }else{
                    $rep = $data['response'];
                }
                $history = $rep['histori'];
                foreach($history as $h){
                    $one = array();
                    $one['id'] = $h["noSep"];
                    $one['nama'] = $h['namaPeserta'];
                    $one['nokartu'] = $h["noKartu"];
                    $one['nosep'] = $h["noSep"];
                    $one['norujukan'] = $h["noRujukan"];
                    $one['jenis_pelayanan'] = $h["jnsPelayanan"];
                    $one['kelas_rawat'] = $h["kelasRawat"];
                    $one['poli'] = $h["poli"];
                    $one['diagnosa'] = $h["diagnosa"];
                    $one['tanggal_pulang'] = $h["tglPlgSep"];
                    $one['tanggal_sep'] = $h["tglSep"];
                    $hasil_akhir[] = $one;
                }
            }
        }

        $uidata				 = $this->adapter->getContent($hasil_akhir);
		$this->uitable->setContent($uidata);
		$list				 = $this->uitable->getBodyContent();
		$pagination			 = $this->uitable->getPagination($page,3,$max_page);		
		$json['list']		 = $list;
		if(!$this->debug) {
			unset($d['custom_kriteria']);
			unset($d['query']);
		}
		$json['debug']		 = $this->debug;
		$json['dbtable']	 = $d;
		if($this->include_adapter_data){
			$json['adapter'] = $uidata;
		}
		if($this->include_raw_data){
			$json['raw']	 = $data;
		}
		$json['pagination']	 = $pagination->getHtml();
		$json['number']		 = $page;
		$json['number_p']	 = $number;
		return $json;
    }
}