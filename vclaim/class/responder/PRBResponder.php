<?php 
require_once "registration/class/responder/BPJSResponder.php";
require_once "vclaim/class/responder/VClaimSEPResponder.php";
class PRBResponder extends VClaimSEPResponder{

    public function delete(){
        global $querylog;
        $id = $_POST['id'];
        $idx = explode("||",$id);

        global $db;
        $request['request']['t_prb'] = array(
            "noSrb" =>$idx[1],
            "noSep" =>$idx[0],
            "user" => "123456" //getSettings($db,"reg-bpjs-vclaim-api-user",""),
        );
        $url  = $this->url_base."/Rujukan/Khusus/delete";
        $this->do_curl($this->port,$url,"DELETE",json_encode($request));  
        if ($this->error) {
            return "cURL Error #:" . $this->error;
          } else {
              $querylog->addMessage($this->response);
              $querylog->addMessage("URL ".$url);
              $data=json_decode($this->response,true);
              if($data==null ){
                 $this->response_pack->setAlertVisible(true);
                 $this->response_pack->setAlertContent("Error","Error Komunikasi BPJS");
                  return array();
              }else if( $data['metaData']['code']!="200" ){
                 $this->response_pack->setAlertVisible(true);
                 $this->response_pack->setAlertContent($data['metaData']['code'],$data['metaData']['message']);
                  return array();
              }else{
                $final['success'] = "1";
                return $final;
            }
	}
}


	

    public function view(){	
		$d					 = $this->getDataPRB($_POST['dari'],$_POST['sampai']);
		$page				 = 1;
		$data				 = $d;
		$max_page			 = 1;

		$this->adapter->setNumber($page*1000);
		$uidata				 = $this->adapter->getContent($data);
		$this->uitable->setContent($uidata);
		$list				 = $this->uitable->getBodyContent();
		$json['list']		 = $list;

		if(!$this->debug) {
			unset($d['custom_kriteria']);
			unset($d['query']);
		}
		$json['debug']		 = $this->debug;
		$json['dbtable']	 = $d;
		if($this->include_adapter_data){
			$json['adapter'] = $uidata;
		}
		if($this->include_raw_data){
			$json['raw']	 = $data;
		}
		$json['pagination']	 = "";
		$json['number']		 = 1;
		$json['number_p']	 = 1;
		return $json;
	}


    public function getDataPRB($dari="",$sampai=""){
        global $querylog;
        global $db;
        $url  = $this->url_base."/prb/tglMulai/$dari/tglAkhir/$sampai";
        $this->do_curl($this->port,$url,"GET",NULL);  
        if ($this->error) {
           return "cURL Error #:" . $this->error;
         } else {
             $querylog->addMessage($this->response);
             $querylog->addMessage("URL ".$url);
             $data=json_decode($this->response,true);
             if($data==null ){
               $this->response_pack->setAlertVisible(true);
               $this->response_pack->setAlertContent("Error","Error Komunikasi BPJS");
                 return array();
             }else if( $data['metaData']['code']!="200" ){
               $this->response_pack->setAlertVisible(true);
               $this->response_pack->setAlertContent($data['metaData']['code'],$data['metaData']['message']);
                 return array();
             }else{
               require_once "registration/function/vclaim_decrypt.php";
               $rep = array();
               if(is_string($data['response'])){
                   $hasilcompress = stringDecrypt($this->getKey(), $data['response']);
                   $hasil = decompress($hasilcompress);
                   $repx = json_decode($hasil,true);
                   $rep = $repx['prb']['list'];
               }else{
                   $rep = $data['response']['prb']['list'];
               }
               $final = array();
               foreach($rep as $x){
                    $one = array();
                    $one['id'] = $x['noSEP']."||".$x['noSRB'];
                    $one['noSRB'] = $x['noSRB'];
                    $one['noKartu'] = $x['peserta']['noKartu'];
                    $one['nama'] = $x['peserta']['nama'];
                    $one['programPRB'] = $x['programPRB']['nama'];
                    $one['noSEP'] = $x['noSEP'];                    
                    $final[] = $one;    
               }
               return $final;
             }
         }

    }

    public function cari_sep(){
        global $querylog;
        global $db;
        $url  = $this->url_base."/RencanaKontrol/nosep/".$_POST['nosep'];
        $this->do_curl($this->port,$url,"GET",NULL);  
        if ($this->error) {
           return "cURL Error #:" . $this->error;
         } else {
             $querylog->addMessage($this->response);
             $querylog->addMessage("URL ".$url);
             $data=json_decode($this->response,true);
             if($data==null ){
                $this->response_pack->setAlertVisible(true);
                $this->response_pack->setAlertContent("Error","Error Komunikasi BPJS");
                 return array();
             }else if( $data['metaData']['code']!="200" ){
                $this->response_pack->setAlertVisible(true);
                $this->response_pack->setAlertContent($data['metaData']['code'],$data['metaData']['message']);
                 return array();
             }else{
               require_once "registration/function/vclaim_decrypt.php";
               $rep = array();
               if(is_string($data['response'])){
                   $hasilcompress = stringDecrypt($this->getKey(), $data['response']);
                   $hasil = decompress($hasilcompress);
                   $repx = json_decode($hasil,true);
                   $querylog->addMessage("Hasil Decrypt : ".$hasil);                   
                   $rep = $repx;
               }else{
                   $rep = $data['response'];
               }
               $final = array();
               $final['tgl_sep'] = $rep['tglSep'];
               $final['nama'] = $rep['peserta']['nama'];
               $final['kartu'] = $rep['peserta']['noKartu'];
               $final['tgl_lahir'] = $rep['peserta']['tglLahir'];
               $final['diagnosa'] = $rep['diagnosa'];
               $pl = explode(" - ",$rep['poli']);
               $final['kode_poli'] = $pl[0];
               $final['poli'] = $pl[1];
               $final['dokter'] = "";
               $final['kode_dokter'] = "";
               return $final;
             }
         }
    }

   
    public function cari_rujukan($no_rujukan,$asal=""){
        $methode    = "GET";
        $request    = "";
        $asal="";
        $url    = $this->url_base."Rujukan/".$asal.$no_rujukan;

        global $querylog;
        $querylog->addMessage($url);
        
        $this->do_curl($this->port,$url,$methode,$request);
        $data=null;
        if ($this->error) {
            require_once "registration/function/bpjs_rujukan_null.php";
            $data=bpjs_rujukan_null("Terjadi Masalah Koneksi dengan Server BPJS");
        } else {
            $response=json_decode($this->response,true);
            //echo $this->response;
            $querylog->addMessage($this->response);
            if($response==null || $response['metaData']['code']!="200" ){
                require_once "registration/function/bpjs_rujukan_null.php";
                if($response['metaData']['code']=="202"){
                    $data = bpjs_rujukan_null($response['metaData']['message']);
                }else if($asal==""){
                    /* try search from another RS */
                    return $this->cari_rujukan($no_rujukan,"RS/");
                }
                $data = bpjs_rujukan_null($response['metaData']['message']);
            }else{
                
                require_once "registration/function/bpjs_rujukan_found.php";
                $data = bpjs_rujukan_found_vclaim($response['response'],$this->getKey(),$asal);
                $data['asalRujukan'] = $asal==""?1:2;

                return $this->cek_bpjs($data['nobpjs'],$data['tgl_kunjungan']);
            }
        }
        return $data;
    }

    public function cek_bpjs($nobpjs,$tglsep){
        $methode    = "GET";
        $request    = "";        
        $url    = $this->url_base."Peserta/nokartu/".$nobpjs."/tglSEP/".$tglsep;
        
        $this->do_curl($this->port,$url,$methode,$request);
        $data   = null;
        if ($this->error) {
            require_once "registration/function/bpjs_null.php";
            $data   = bpjs_null();
        } else {
            $response   = json_decode($this->response,true);
            if($response==null || $response['metaData']['code']!="200" ){
                require_once "registration/function/bpjs_null.php";
                $data   = bpjs_null($response['metaData']['message']);
            }else{
                require_once "registration/function/bpjs_found.php";
                $data   = bpjs_found($response,$this->getKey());
            }
        }
        require_once "registration/function/bpjs_table_checker.php";
        $table  = bpjs_table_checker($data);
        return $table;
    }

    public function save(){
        global $db;
        global $querylog;
        $request = array(
            "noRujukan"=>$_POST['noRujukan'],
            "diagnosa"=>array(
                array("kode"=>"P;".$_POST['diagnosaPrimer']),
                array("kode"=>"S;".$_POST['diagnosaSekunder']),
            ),
            "procedure"=>array(
                array("kode"=>$_POST['procedure'])
            ),
            "user"=>getSettings($db,"reg-bpjs-vclaim-api-user","")
        );
        $url    = $this->url_base."/Rujukan/Khusus/insert";
         $this->do_curl($this->port,$url,"POST",json_encode($request));  
         if ($this->error) {
            return "cURL Error #:" . $this->error;
          } else {
                global $querylog;
                $querylog->addMessage($this->response);
                $querylog->addMessage("URL ".$url);
                $data=json_decode($this->response,true);
                if($data==null ){
                    $this->response_pack->setAlertVisible(true);
                    $this->response_pack->setAlertContent("Error","Error Komunikasi BPJS");
                    $success['success'] = 0;
                        return $success;
                }else if( $data['metaData']['code']!="200" ){
                    $this->response_pack->setAlertVisible(true);
                    $this->response_pack->setAlertContent($data['metaData']['code'],$data['metaData']['message']);
                    $success['success'] = 0;
                    return $success;
                }else{
                    if(is_string($data['response'])){
                        $hasilcompress = stringDecrypt($this->getKey(), $data['response']);
                        $hasil = decompress($hasilcompress);
                        $repx = json_decode($hasil,true);
                        $querylog->addMessage("Hasil Decrypt : ".$hasil);                   
                        $data = $repx;
                    }else{
                        $data = $data['response'];
                    }
                    $success['success'] = "1";
                    return $success;
                }
          }
                     
    }

}