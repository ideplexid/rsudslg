<?php 
require_once "registration/class/responder/BPJSResponder.php";
class VClaimDataKunjunganList extends BPJSResponder{
    public function view(){
        $tanggal = $_POST['tanggal'];
        $jenis = $_POST['jenis'];

        $methode    = "GET";
        $request    = "";        
        $url    = $this->url_base."/monitoring/Kunjungan/Tanggal/".$tanggal."/JnsPelayanan/".$jenis;
        $this->do_curl($this->port,$url,$methode,$request);
        $data   = null;
        $hasil_akhir = array();
        if ($this->error) {
            $this->response_pack->setAlertVisible(true);
            $this->response_pack->setAlertContent("Error","Error Komunikasi BPJS");
        } else {
            $response   = json_decode($this->response,true);
            global $querylog;
            if($response==null || $response['metaData']['code']!="200" ){
                $this->response_pack->setAlertVisible(true);
                $this->response_pack->setAlertContent("Error",$response['metaData']['message']);
            }else{
                $querylog->addMessage($response);
                require_once "registration/function/vclaim_decrypt.php";
                $rep = array();
                if(is_string($response['response'])){
                    $hasilcompress = stringDecrypt($this->getKey(), $response['response']);
                    $hasil = decompress($hasilcompress);
                    $rep = json_decode($hasil,true);
                }else{
                    $rep = $data['response'];
                }
                $history = $rep['sep'];
                
                foreach($history as $h){
                    $one = array();
                    $one['id'] = $h["noSep"];
                    $one['nama'] = $h['nama'];
                    $one['nokartu'] = $h["noKartu"];
                    $one['nosep'] = $h["noSep"];
                    $one['norujukan'] = $h["noRujukan"];
                    $one['jenis_pelayanan'] = $h["jnsPelayanan"];
                    $one['kelas_rawat'] = $h["kelasRawat"];
                    $one['poli'] = $h["poli"];
                    $one['diagnosa'] = $h["diagnosa"];
                    $one['tanggal_pulang'] = $h["tglPlgSep"];
                    $one['tanggal_sep'] = $h["tglSep"];
                    $hasil_akhir[] = $one;
                }
            }
        }

        $uidata				 = $this->adapter->getContent($hasil_akhir);
		$this->uitable->setContent($uidata);
		$list				 = $this->uitable->getBodyContent();
		$pagination			 = $this->uitable->getPagination($page,3,$max_page);		
		$json['list']		 = $list;
		if(!$this->debug) {
			unset($d['custom_kriteria']);
			unset($d['query']);
		}
		$json['debug']		 = $this->debug;
		$json['dbtable']	 = $d;
		if($this->include_adapter_data){
			$json['adapter'] = $uidata;
		}
		if($this->include_raw_data){
			$json['raw']	 = $data;
		}
		$json['pagination']	 = $pagination->getHtml();
		$json['number']		 = $page;
		$json['number_p']	 = $number;
		return $json;
    }
}