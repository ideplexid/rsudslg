<?php 
require_once "registration/class/responder/BPJSResponder.php";
class VClaimPRBResponder extends BPJSResponder{

    public function command($command){		
		$this->response_pack = new ResponsePackage();	
		if ($command=="add") {
            $content = $this->insertPRB();
			$this->response_pack->setContent($content);
			$this->response_pack->setStatus(ResponsePackage::$STATUS_OK);
            return $this->response_pack->getPackage();
        } else if ($command=="update") {
            $content = $this->updatePRB();
			$this->response_pack->setContent($content);
			$this->response_pack->setStatus(ResponsePackage::$STATUS_OK);
            return $this->response_pack->getPackage();
        } else {
            return parent::command($command);
        }
	}

    public function edit(){
        global $db;
        global $querylog;
      
        $noSep = $_POST['noSep'];
        $noSRB = $_POST['noSRB'];
        
        $url    = $this->url_base."/prb/$noSRB/nosep/$noSep";
        $this->do_curl($this->port,$url,"GET",NULL);  
        if ($this->error) {
            return "cURL Error #:" . $this->error;
        } else {
            $querylog->addMessage($this->response);
            $querylog->addMessage("URL ".$url);
            $data=json_decode($this->response,true);
            if($data==null ){
                $this->response_pack->setAlertVisible(true);
                $this->response_pack->setAlertContent("Error","Error Komunikasi BPJS");
                return array();
            }else if( $data['metaData']['code']!="200" ){
                $this->response_pack->setAlertVisible(true);
                $this->response_pack->setAlertContent($data['metaData']['code'],$data['metaData']['message']);
                return array();
            }else{
                require_once "registration/function/vclaim_decrypt.php";
                $rep = array();
                if(is_string($data['response'])){
                    $hasilcompress = stringDecrypt($this->getKey(), $data['response']);
                    $hasil = decompress($hasilcompress);
                    $repx = json_decode($hasil,true);
                    $rep = $repx['prb'];
                }else{
                    $rep = $data['response']['prb'];
                }
                $final = array();
                $final['noKartu'] = $rep['peserta']['noKartu'];
                $final['tglSRB'] = $rep['tglSRB'];
                $final['alamat'] = $rep['peserta']['alamat'];
                $final['email'] = $rep['peserta']['email'];
                $final['programPRB'] = $rep['programPRB']['kode'];
                $final['kodeDPJP'] = $rep['DPJP']['kode'];
                $final['keterangan'] = $rep['keterangan'];
                $final['saran'] = $rep['saran'];
                $final['obat'] = $rep['obat']['obat'];
                $querylog->addMessage(json_encode($rep));
                return $final;
            }
        }
    }

    public function updatePRB(){
        global $db;
        global $querylog;
        $_POST['obat'] = json_decode($_POST['obat'], true);
        $_POST['user'] = "123456";//getSettings($db,"reg-bpjs-vclaim-api-user"); 
        $request['request'] = array(
            "t_prb" => $_POST
        );
        $final = array();
        $url    = $this->url_base."/PRB/Update";
         $this->do_curl($this->port,$url,"PUT",json_encode($request));  
         if ($this->error) {
            return "cURL Error #:" . $this->error;
          } else {
              $querylog->addMessage($this->response);
              $querylog->addMessage("URL ".$url);
              $data=json_decode($this->response,true);
              if($data==null ){
                $this->response_pack->setAlertVisible(true);
                $this->response_pack->setAlertContent("Error","Error Komunikasi BPJS");
                $final['success'] = 0;
                $final['id'] = 0;
                return $final;
              }else if( $data['metaData']['code']!="200" ){
                  $this->response_pack->setAlertVisible(true);
                  $this->response_pack->setAlertContent($data['metaData']['code'],$data['metaData']['message']);
                  $final['success'] = 0;
                  $final['id'] = 0;
                  //$final['print'] = $this->generatePrintRujukan(array());
                  return $final;
              }else{
                if(is_string($data['response'])){
                    require_once "registration/function/vclaim_decrypt.php";
                    $hasilcompress = stringDecrypt($this->getKey(), $data['response']);
                    $hasil = decompress($hasilcompress);
                    $repx = json_decode($hasil,true);
                    $querylog->addMessage("Hasil Decrypt : ".$hasil);                   
                    $datax = $repx;
                    $final['print'] = $this->generatePrintRujukan($datax);
                    $querylog->addMessage(json_encode($datax));
                }else{
                    $datax = $data['response'];
                    $final['print'] = $this->generatePrintRujukan($datax);
                }
                $this->response_pack->setAlertVisible(true);
                $this->response_pack->setAlertContent('Berhasil',"Update PRB Berhasil");
                $final['success'] = 1;
                $final['id'] = 1;
                return $final;
              }
          }                
    }

    public function insertPRB(){
        global $db;
        global $querylog;
        $_POST['obat'] = json_decode($_POST['obat'], true);
        $_POST['user'] = "123456";//getSettings($db,"reg-bpjs-vclaim-api-user",""); 
        $request['request'] = array(
            "t_prb" => $_POST
        );
        $final = array();
        $url    = $this->url_base."/PRB/insert";
         $this->do_curl($this->port,$url,"POST",json_encode($request));  
         if ($this->error) {
            return "cURL Error #:" . $this->error;
          } else {
              $querylog->addMessage($this->response);
              $querylog->addMessage("URL ".$url);
              $data=json_decode($this->response,true);
              if($data==null ){
                $this->response_pack->setAlertVisible(true);
                $this->response_pack->setAlertContent("Error","Error Komunikasi BPJS");
                  $final['success'] = 0;
                    $final['id'] = 0;
                    return $final;
              }else if( $data['metaData']['code']!="200" ){
                  $this->response_pack->setAlertVisible(true);
                  $this->response_pack->setAlertContent($data['metaData']['code'],$data['metaData']['message']);
                  $final['success'] = 0;
                  $final['id'] = 0;
                  return $final;
              }else{
                if(is_string($data['response'])){
                    require_once "registration/function/vclaim_decrypt.php";
                    $hasilcompress = stringDecrypt($this->getKey(), $data['response']);
                    $hasil = decompress($hasilcompress);
                    $repx = json_decode($hasil,true);
                    $querylog->addMessage("Hasil Decrypt : ".$hasil);                   
                    $datax = $repx;
                    $final['print'] = $this->generatePrintRujukan($datax);
                    $querylog->addMessage(json_encode($datax));
                }else{
                    $datax = $data['response'];
                    $final['print'] = $this->generatePrintRujukan($datax);
                }
                $this->response_pack->setAlertVisible(true);
                $this->response_pack->setAlertContent('Berhasil',"Insert PRB Berhasil");
                $final['success'] = 1;
                $final['id'] = 1;
                
                return $final;
              }
          }                
    }


    public function generatePrintRujukan($d){
        //$d = json_decode(file_get_contents("vclaim/resource/json/test.json"),true);
        $olist = $d['obat']['list'];
        $counto = count($olist);
        global $db;
        $logo = getSettings($db,"reg-bpjs-vclaim-logo",'');
        if($logo==""){
            $logo="registration/resource/image/bpjs-logo.png";
        }else{
            $logo="smis-upload/".$logo;
        }

        $image = "<img style='width:200px;height:auto;' src='".$logo."' />";
        $table = new TablePrint("tbprint","tbprint");
        $table->setMaxWidth(true);
        $table->setDefaultBootrapClass(false);

        $table->addColumn($image,"3","2");
        $table->addColumn("SURAT RUJUK BALIK (PRB)","2","1",null,null,"bold");
        $table->addColumn("No. ".$d['noSRB'],"2","1",null,null,"bold");
        $table->commit("header");

        $table->addColumn(getSettings($db,"smis_autonomous_title",""),"2","1",null,null,"bold");
        $table->addColumn("Tgl. ".ArrayAdapter::format("date d M Y",$d['tglSRB']),"2","1",null,null,"bold");
        $table->commit("header");

        $table->addColumn("&nbsp;","1","1");
        $table->addColumn("&nbsp;","1","1");
        $table->addColumn("&nbsp;","1","1");
        $table->addColumn("&nbsp;","1","1");
        $table->addColumn("&nbsp;","1","1");
        $table->addColumn("&nbsp;","1","1");
        $table->addColumn("&nbsp;","1","1");
        $table->commit("header");

        $table->addColumn("Kepada Yth","1","1");
        $table->addColumn(":","1","1");
        $table->addColumn("&nbsp;","5","1");
        $table->commit("body");

        //: "{1-> rawat inap, 2-> rawat jalan}",
        //0->Penuh, 1->Partial, 2->balik PRB
        $table->addColumn("&nbsp;","1","1");
        $table->addColumn("&nbsp;","1","1");
        $table->addColumn("&nbsp;","2","1");
        $table->addColumn("R/.","3","1");
        $table->commit("body");

        $table->addColumn("Mohon Pemeriksaan dan Penanganan Lebih Lanjut : ","4","1");
        if($counto>=1){
            $table->addColumn("1. ".$olist[0]['signa']." ".$olist[0]['nmObat'],"2","1");
            $table->addColumn($olist[0]['jmlObat'],"1","1");    
        }else{
            $table->addColumn("&nbsp;","3","1");
        }
        $table->commit("body");

        $table->addColumn("No. Kartu","1","1");
        $table->addColumn(":","1","1");
        $table->addColumn($d['peserta']['noKartu'],"2","1");
        if($counto>=2){
            $table->addColumn("2. ".$olist[1]['signa']." ".$olist[1]['nmObat'],"2","1");
            $table->addColumn($olist[1]['jmlObat'],"1","1");
        }else{
            $table->addColumn("&nbsp;","3","1");
        }
        $table->commit("body");

        $table->addColumn("Nama Peserta","1","1");
        $table->addColumn(":","1","1");
        $table->addColumn($d['peserta']['nama']."(".$d['peserta']['kelamin'].")","2","1");
        if($counto>=3){
            $table->addColumn("3. ".$olist[2]['signa']." ".$olist[2]['nmObat'],"2","1");
            $table->addColumn($olist[2]['jmlObat'],"1","1");
        }else{
            $table->addColumn("&nbsp;","3","1");
        }
        $table->commit("body");

        $table->addColumn("Tgl. Lahir","1","1");
        $table->addColumn(":","1","1");
        $table->addColumn(ArrayAdapter::format("date d M Y",$d['peserta']['tglLahir']),"2","1");
        if($counto>=4){
            $table->addColumn("4. ".$olist[3]['signa']." ".$olist[3]['nmObat'],"2","1");
            $table->addColumn($olist[3]['jmlObat'],"1","1");
        }else{
            $table->addColumn("&nbsp;","3","1");
        }
        $table->commit("body");

        $table->addColumn("Diagnosa","1","1");
        $table->addColumn(":","1","1");
        $table->addColumn("","2","1");
        if($counto>=5){
            $table->addColumn("5. ".$olist[4]['signa']." ".$olist[4]['nmObat'],"2","1");
            $table->addColumn($olist[4]['jmlObat'],"1","1");
        }else{
            $table->addColumn("&nbsp;","3","1");
        }       
        $table->commit("body");

        $table->addColumn("Keterangan","1","1");
        $table->addColumn(":","1","1");
        $table->addColumn($_POST['catatan'],"2","1");
        if($counto>=6){
            $table->addColumn("6. ".$olist[5]['signa']." ".$olist[5]['nmObat'],"2","1");
            $table->addColumn($olist[5]['jmlObat'],"1","1");
        }else{
            $table->addColumn("&nbsp;","3","1");
        }  
        $table->commit("body");

        $table->addColumn("Saran Pengelolaan Lanjutan di FKTP :","4","1");
        if($counto>=7){
            $table->addColumn("7. ".$olist[6]['signa']." ".$olist[6]['nmObat'],"2","1");
            $table->addColumn($olist[6]['jmlObat'],"1","1");
        }else{
            $table->addColumn("&nbsp;","3","1");
        }
        $table->commit("body");

        $next = 0;
        foreach($olist as $o){
            $next++;
            if($next<8){
                continue;
            }
            $table->addColumn("&nbsp;","3","1");
            $table->addColumn($next.". ".$o['signa']." ".$o['nmObat'],"2","1");
            $table->addColumn($o['jmlObat'],"1","1");
            $table->commit("body");
        }

        $table->addColumn("&nbsp;","1","1");
        $table->addColumn("&nbsp;","1","1");
        $table->addColumn("&nbsp;","1","1");
        $table->addColumn("&nbsp;","1","1");
        $table->addColumn("&nbsp;","1","1");
        $table->addColumn("Mengetahui","2","1");
        $table->commit("body");

        $table->addColumn("Demikian atas bantuanya, diucapkan banyak terima kasih.","7","1");
        $table->commit("body");

        $table->addColumn("&nbsp;","1","1");
        $table->addColumn("&nbsp;","1","1");
        $table->addColumn("&nbsp;","1","1");
        $table->addColumn("&nbsp;","1","1");
        $table->addColumn("&nbsp;","1","1");
        $table->addColumn("&nbsp;","1","1");
        $table->addColumn("&nbsp;","1","1");
        $table->commit("body");

        $table->addColumn("Tgl. Cetak ".ArrayAdapter::format("date d M Y",date("Y-m-d h:i A")),"2","1");
        $table->addColumn("&nbsp;","1","1");
        $table->addColumn("&nbsp;","1","1");
        $table->addColumn("&nbsp;","1","1");
        $table->addColumn("_______________","2","1");
        $table->commit("body");

        return $table->getHtml();
    }
}