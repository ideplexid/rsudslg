<?php 
require_once "registration/class/responder/BPJSResponder.php";
class VClaimSEPResponder extends BPJSResponder{

    public function command($command){		
		$this->response_pack = new ResponsePackage();	
		if($command=="cari_sep"){
			$content		 = $this->cari_sep();
			$this->response_pack->setContent($content);
			$this->response_pack->setStatus(ResponsePackage::$STATUS_OK);
            return $this->response_pack->getPackage();
		}else if($command=="cari_rujukan"){
			$content		 = $this->cari_rujukan($_POST['noRujukan'],"RS");
			$this->response_pack->setContent($content);
			$this->response_pack->setStatus(ResponsePackage::$STATUS_OK);
            return $this->response_pack->getPackage();
		}else if($command=="save"){
            $content		 = $this->save();
			$this->response_pack->setContent($content);
			$this->response_pack->setStatus(ResponsePackage::$STATUS_OK);
            return $this->response_pack->getPackage();
        }else if($command=="update_pulang"){
            $content		 = $this->update_pulang();
			$this->response_pack->setContent($content);
			$this->response_pack->setStatus(ResponsePackage::$STATUS_OK);
            return $this->response_pack->getPackage();
        } else if($command=="fingerprint"){
            $pack = new ResponsePackage();
            $pack->setStatus(ResponsePackage::$STATUS_OK);
            $r=$this->cek_fingerprint();
            $pack->setContent($r);
            return $pack->getPackage();
        }else if($command=="print_vklaim"){
            $pack=new ResponsePackage();
            $pack->setStatus(ResponsePackage::$STATUS_OK);
            $r=$this->print_sep();
            $pack->setContent($r);
            return $pack->getPackage();
        }else{
            return parent::command($command);
        }
	}

    public function cek_fingerprint(){
        $nobpjs     = $_POST['peserta'];
        $tgl      = $_POST['tgl'];        
        $methode    = "GET";
        $request    = "";        
        $url    = $this->url_base."/SEP/FingerPrint/Peserta/$nobpjs/TglPelayanan/".date("Y-m-d");
       
        $this->do_curl($this->port,$url,$methode,$request);
        $data   = null;
        if ($this->error) {
            return "Eror Komunikasi dengan BPJS";
        } else {
            $response   = json_decode($this->response,true);
            global $querylog;
            if($response==null || $response['metaData']['code']!="200" ){
                return $response['metaData']['message'];
            }else{
                $querylog->addMessage($response);
                require_once "registration/function/vclaim_decrypt.php";
                $rep = array();
                if(is_string($response['response'])){
                    $hasilcompress = stringDecrypt($this->getKey(), $response['response']);
                    $hasil = decompress($hasilcompress);
                    $rep = json_decode($hasil,true);
                }else{
                    $rep = $data['response'];
                }
                return $rep['status'];
            }
        }
    }



    public function update_pulang(){
        global $db;
        global $querylog;
        $request['request'] = array(
            "t_sep"=>array(
                "noSep"=>$_POST['noSep'],                
                "statusPulang"=>$_POST['statusPulang'],
                "noSuratMeninggal"=>$_POST['noSuratMeninggal'],
                "tglMeninggal"=>$_POST['tglMeninggal'],
                "tglPulang"=>$_POST['tglPulang'],
                "user"=>getSettings($db,"reg-bpjs-vclaim-api-user","")
            )
        );
        $url    = $this->url_base."/SEP/2.0/updtglplg";
         $this->do_curl($this->port,$url,"PUT",json_encode($request));  
         if ($this->error) {
            return "cURL Error #:" . $this->error;
          } else {
              $querylog->addMessage($this->response);
              $querylog->addMessage("URL ".$url);
              $data=json_decode($this->response,true);
              if($data==null ){
                $this->response_pack->setAlertVisible(true);
                $this->response_pack->setAlertContent("Error","Error Komunikasi BPJS");
                  return array();
              }else if( $data['metaData']['code']!="200" ){
                  $this->response_pack->setAlertVisible(true);
                  $this->response_pack->setAlertContent($data['metaData']['code'],$data['metaData']['message']);
                  return array();
              }else{
                $this->response_pack->setAlertVisible(true);
                $this->response_pack->setAlertContent('Berhasil',"Berhasil di Update");
                $final['success'] = 1;
                $final['id'] = 1;
                return $final;
              }
          }
                     
    }

    


    public function save(){
        global $db;
        global $querylog;
        $request['request'] = array(
            "t_sep"=>array(
                "noSep"=>$_POST['noSep'],
                "klsRawat"=>array(
                    "klsRawatHak"=>$_POST['klsRawat'],
                    "klsRawatNaik"=>$_POST['naikKelas'],
                    "pembiayaan"=>$_POST['pembiayaan'],
                    "penanggungJawab"=>$_POST['penanggunjwb']
                ),
                "noMR"=>$_POST['noMr'],               
                "catatan"=>$_POST['catatan'],
                "diagAwal"=>$_POST['diagAwal'],
                "poli"=>array(
                    "tujuan"=>"poliTujuan",
                    "eksekutif"=>$_POST['eksekutif']
                ),
                "cob"=>array(
                    "cob"=>$_POST['cob']
                ),
                "katarak"=>array(
                    "katarak"=>$_POST['katarak']
                ),
                "skdp"=>array(
                    "noSurat"=>$_POST['noSurat'],
                    "kodeDPJP"=>$_POST['kodeDPJP']
                ),
                "jaminan"=>array(
                    "lakaLantas"=>$_POST['lakaLantas'],
                    "penjamin"=>array(
                        "tglKejadian"=>$_POST['tgl_kejadian'],
                        "keterangan"=>$_POST['keterangan'],
                        "suplesi"=>array(
                            "suplesi"=>$_POST['suplesi'],
                            "noSepSuplesi"=>$_POST['no_suplesi'],
                            "lokasiLaka"=>array(
                                "kdPropinsi"=>$_POST['kdPropinsi'],
                                "kdKabupaten"=>$_POST['kdKabupaten'],
                                "kdKecamatan"=>$_POST['kdKecamatan']
                            )
                        )
                    )
                ),
                "dpjpLayan"=>$_POST['dpjpLayan'],
                "noTelp"=>$_POST['noTelp'],
                "user"=>getSettings($db,"reg-bpjs-vclaim-api-user","")
            )
        );
        $url    = $this->url_base."/SEP/2.0/update";
         $this->do_curl($this->port,$url,"PUT",json_encode($request));  
         if ($this->error) {
            return "cURL Error #:" . $this->error;
          } else {
              $querylog->addMessage($this->response);
              $querylog->addMessage("URL ".$url);
              $data=json_decode($this->response,true);
              if($data==null ){
                $this->response_pack->setAlertVisible(true);
                $this->response_pack->setAlertContent("Error","Error Komunikasi BPJS");
                  return array();
              }else if( $data['metaData']['code']!="200" ){
                  $this->response_pack->setAlertVisible(true);
                  $this->response_pack->setAlertContent($data['metaData']['code'],$data['metaData']['message']);
                  return array();
              }else{
                $final['success'] = 1;
                $final['id'] = 1;
                return $final;
              }
          }
                     
    }




    public function delete(){
        global $db;
        $noSep = $_POST['noSep'];
        $jenisSep = $_POST['jenisSep'];
        $url  = $this->url_base."/SEP/2.0/Delete";
        global $querylog;
        $data = array(
            "request"=>array(
                "t_sep"=>array(
                    "noSep"=>  $_POST['noSep'],
                    "user"=>getSettings($db,"reg-bpjs-vclaim-api-user","")
                )
            )
        );
        if($jenisSep=="internal"){
            $url  = $this->url_base."/SEP/Internal/delete/";    
            $data = array(
                "request"=>array(
                    "t_sep"=>array(
                        "noSep"=>  $_POST['noSep'],
                        "noSurat"=> $_POST['noSurat'],
                        "tglRujukanInternal"=>$_POST['tglRujukanInternal'],
                        "kdPoliTuj"=>$_POST['poli'],
                        "user"=>getSettings($db,"reg-bpjs-vclaim-api-user","")
                    )
                )
            );
        }
            $this->do_curl($this->port,$url,"DELETE",json_encode($data) );
            if ($this->error) {
            return "cURL Error #:" . $this->error;
          } else {
              $querylog->addMessage($this->response);
              $querylog->addMessage("URL ".$url);
              $data=json_decode($this->response,true);
              if($data==null ){
                 $this->response_pack->setAlertVisible(true);
                 $this->response_pack->setAlertContent("Error","Error Komunikasi BPJS");
                 $success['success'] = 0;
                  return $success;
              }else if( $data['metaData']['code']!="200" ){
                 $this->response_pack->setAlertVisible(true);
                 $this->response_pack->setAlertContent($data['metaData']['code'],$data['metaData']['message']);
                 $success['success'] = 0;
                 return $success;
              }else{
                $success['success'] = "1";
                return $success;
            }
	    }
	}

    public function cari_rujukan($no_rujukan,$asal=""){
        $methode    = "GET";
        $request    = "";
        $url    = $this->url_base."Rujukan/".$asal.$no_rujukan;

        global $querylog;
        $querylog->addMessage($url);
        
        $this->do_curl($this->port,$url,$methode,$request);
        $data=null;
        if ($this->error) {
            require_once "registration/function/bpjs_rujukan_null.php";
            $data=bpjs_rujukan_null("Terjadi Masalah Koneksi dengan Server BPJS");
        } else {
            $response=json_decode($this->response,true);
            //echo $this->response;
            $querylog->addMessage($this->response);
            if($response==null || $response['metaData']['code']!="200" ){
                require_once "registration/function/bpjs_rujukan_null.php";
                if($response['metaData']['code']=="202"){
                    $data = bpjs_rujukan_null($response['metaData']['message']);
                }else if($asal==""){
                    /* try search from another RS */
                    return $this->cari_rujukan($no_rujukan,"RS/");
                }
                $data = bpjs_rujukan_null($response['metaData']['message']);
            }else{
                
                require_once "registration/function/bpjs_rujukan_found.php";
                $data = bpjs_rujukan_found_vclaim($response['response'],$this->getKey(),$asal);
                $data['asalRujukan'] = $asal==""?1:2;
            }
        }
        return $data;
    }

    
    public function edit(){
        global $querylog;
        global $db;

        $noSep = $_POST['noSep'];
        $jenisSep = $_POST['jenisSep'];
        $url  = $this->url_base."/SEP/".$noSep;
        if($jenisSep=="internal"){
            $url  = $this->url_base."/SEP/Internal/".$noSep;    
        }

        $this->do_curl($this->port,$url,"GET",NULL);          
        if ($this->error) {
           return "cURL Error #:" . $this->error;
         } else {
             $querylog->addMessage($this->response);
             $querylog->addMessage("URL ".$url);
             $data=json_decode($this->response,true);
             if($data==null ){
                $this->response_pack->setAlertVisible(true);
                $this->response_pack->setAlertContent("Error","Error Komunikasi BPJS");
                 return array();
             }else if( $data['metaData']['code']!="200" ){
                $this->response_pack->setAlertVisible(true);
                $this->response_pack->setAlertContent($data['metaData']['code'],$data['metaData']['message']);
                 return array();
             }else{
               require_once "registration/function/vclaim_decrypt.php";
               $rep = array();
               if(is_string($data['response'])){
                   $hasilcompress = stringDecrypt($this->getKey(), $data['response']);
                   $hasil = decompress($hasilcompress);
                   $repx = json_decode($hasil,true);
                   $querylog->addMessage("Hasil Decrypt : ".$hasil);                   
                   $rep = $repx;
               }else{
                   $rep = $data['response'];
               }
               $final = array();

              
               if($jenisSep=="internal"){
                    $rep = $rep['list'][0];
                    if($rep==null){
                        return array();
                    }
                    
                    $final['noKartu'] = $rep['nokapst'];
                    $final['asalRujukan'] = $rep["asalRujukan"];
                    $final['poliTujuan'] = $rep["tujuanrujuk"];
                    $final['noSurat'] = $rep["nosurat"];
                    $final['tglRujukan'] = $rep["tglrujukinternal"];
                    $final['jnsPelayanan'] = "0";

                    $final['tglSep'] = $rep['tglsep'];
                    $final['eksekutif'] = 0;
                    $final['diagAwal'] = $rep["diagppk"];
                    $final['namapoliTujuan'] = $rep['nmtujuanrujuk'];
                    $final['dpjpLayan'] = $rep['kddokter'];
                    $final['dpjpLayanNama'] = $rep['nmdokter'];

                    $final['noMr'] = "";
                    $final['namaRujukan'] = $rep["nmtujuanrujuk"];
                    $final['ppkRujukan'] = $rep["ppkpelsep"];
                    $final['noRujukan'] = $rep["noRujukan"];
                    $final['cob'] = 0;
                    $final['catatan'] = "";
                    $final['noTelp'] = "";//??
                    $final['tujuanKunj'] = "";//??
                    $final['flagProcedure'] = $rep["flagprosedur"];//??

                    $final['kdPenunjang'] = $rep["kdpenunjang"];//??
                    $final['assesmentPel'] = "";//??
                    $final['naikKelas'] = "";
                    $final['pembiayaan'] = "";
                    $final['penanggunjwb'] = "";
                    
                    $final['kodeDPJP'] = "";
                    $final['lakaLantas'] = "";
                    $final['tgl_kejadian'] = "";
                    $final['no_suplesi'] = "";//??
                    $final['lokasiLaka'] = "";
                    $final['kdPropinsi'] = "";
                    $final['kdKabupaten'] = "";
                    $final['kdKecamatan'] = "";
                    $final['keterangan'] = "";//??
                    $final['suplesi'] = "";//??
                    $final['klsRawat'] = "";
                    $final['katarak'] = 0;
                    
               }else{
                    require_once "registration/class/responder/VClaimReffPoli.php";
                    require_once "registration/class/responder/VclaimReffDiagnosa.php";
                    $v = new VClaimReffPoli($this->dbtable,$this->uitable,$this->adapter);
                    $kode = new VclaimReffDiagnosa($this->dbtable,$this->uitable,$this->adapter);
                    
                    $final['noKartu'] = $rep['peserta']["noKartu"];
                    $final['asalRujukan'] = $rep["asalRujukan"];
                    $final['jnsPelayanan'] = $rep['jnsPelayanan']=="Rawat Inap"?1:2;
                    $final['tglSep'] = $rep["tglSep"];
                    $final['eksekutif'] = $rep["poliEksekutif"];
                    $final['poliTujuan'] = $v->getPoliBaseReff($rep["poli"]);
                    $final['namapoliTujuan'] = $rep["poli"];
                    $final['dpjpLayan'] = $rep["dpjp"]["kdDPJP"];
                    $final['dpjpLayanNama'] = $rep["dpjp"]["nmDPJP"];
                    $final['noMr'] = $rep["peserta"]["noMr"];

                    if($rep["noRujukan"]!=""){
                        $rujukan = $this->cari_rujukan($rep["noRujukan"]);
                        $final['tglRujukan'] = $rujukan["tgl_kunjungan"];//??
                        $final['namaRujukan'] = $rujukan["nama_provider"];//??
                        $final['ppkRujukan'] = $rujukan["kd_provider"];//??
                        $final['asalRujukan'] = $rujukan["asalRujukan"];//??
                        $final['noRujukan'] = $rep["noRujukan"];    
                    }else{
                        $final['tglRujukan'] = "";//??
                        $final['namaRujukan'] = "";//??
                        $final['ppkRujukan'] = "";//??
                        $final['asalRujukan'] = "";//??
                        $final['noRujukan'] = "";   
                    }

                    $final['diagAwal'] = $kode->getSingleDiagnosa($rep["diagnosa"]);
                    $final['cob'] = $rep["cob"];
                    $final['catatan'] = $rep["catatan"];
                    $final['noTelp'] = "";//??
                    $final['tujuanKunj'] = "";//??
                    $final['flagProcedure'] = "";//??
                    $final['kdPenunjang'] = "";//??
                    $final['assesmentPel'] = "";//??
                    $final['naikKelas'] = $rep["klsRawat"]['klsRawatNaik'];
                    $final['pembiayaan'] = $rep["klsRawat"]["pembiayaan"];
                    $final['penanggunjwb'] = $rep["klsRawat"]["penanggungJawab"];
                    $final['noSurat'] = $rep["kontrol"]["noSurat"];
                    $final['kodeDPJP'] = $rep["kontrol"]["kdDokter"];
                    $final['lakaLantas'] = $rep["kdStatusKecelakaan"];
                    $final['tgl_kejadian'] = $rep["lokasiKejadian"]["tglKejadian"];
                    $final['no_suplesi'] = "";//??
                    $final['lokasiLaka'] = $rep["lokasiKejadian"]['lokasi'];
                    $final['kdPropinsi'] = $rep["lokasiKejadian"]['kdProp'];
                    $final['kdKabupaten'] = $rep["lokasiKejadian"]['kdKab'];
                    $final['kdKecamatan'] = $rep["lokasiKejadian"]['kdKec'];
                    $final['keterangan'] = "";//??
                    $final['suplesi'] = "";//??
                    $final['klsRawat'] = $rep["klsRawat"]["klsRawatHak"];
                    $final['katarak'] = $rep["katarak"];
                
               }
               $final['tbl'] = $this->cek_bpjs($final['noKartu'],$final['tglSep']);

               return $final;
              
             }
         }
    }



    public function cek_bpjs($nobpjs,$tglsep){
        $methode    = "GET";
        $request    = "";        
        $url    = $this->url_base."Peserta/nokartu/".$nobpjs."/tglSEP/".$tglsep;
        
        $this->do_curl($this->port,$url,$methode,$request);
        $data   = null;
        if ($this->error) {
            require_once "registration/function/bpjs_null.php";
            $data   = bpjs_null();
        } else {
            $response   = json_decode($this->response,true);
            if($response==null || $response['metaData']['code']!="200" ){
                require_once "registration/function/bpjs_null.php";
                $data   = bpjs_null($response['metaData']['message']);
            }else{
                require_once "registration/function/bpjs_found.php";
                $data   = bpjs_found($response,$this->getKey());
            }
        }
        require_once "registration/function/bpjs_table_checker.php";
        $table  = bpjs_table_checker($data);
        return $table;
    }

    protected function print_sep(){
        //$id     = $_POST['id'];
        //$row    = $this->getDBTable()->select(array("id"=>$id));
        //$nosep  = $row->noSEP;
        return $this->print_sep_nosep($_POST['id']);
    }
    
    public function print_sep_nosep($nosep){
        global $db;
        $url        = $this->url_base."SEP/".$nosep;
        $metode     = "GET";
        $request    = "";
        $this->do_curl($this->port,$url,$metode,$request);
        $dbtl = new DBTable($db,"smis_rg_sep");
        $one        = (array) $dbtl->select(array("noSEP"=>$nosep));
        if ($this->error) {
          return "cURL Error #:" . $err;
        } else {
            $data   = json_decode($this->response,true);
            if($data==null ){
                return "Terjadi Kesalahan Akibat Koneksi Server BPJS";
            }else if( $data['metaData']['code']!="200" ){
                return $data['metaData']['message'];
            }else{
                global $db;
                require_once "registration/function/bpjs_array_adapter.php";
                require_once "registration/function/bpjs_print_format.php";
                $res    = bpjs_array_adapter_vclaim($data,$one);
                return bpjs_print_format($db,$nosep,$res);
            }
        }
    }




	


    public function cari_sep(){
        global $querylog;
        global $db;
        $url  = $this->url_base."/RencanaKontrol/nosep/".$_POST['nosep'];
        $this->do_curl($this->port,$url,"GET",NULL);  
        if ($this->error) {
           return "cURL Error #:" . $this->error;
         } else {
             $querylog->addMessage($this->response);
             $querylog->addMessage("URL ".$url);
             $data=json_decode($this->response,true);
             if($data==null ){
                $this->response_pack->setAlertVisible(true);
                $this->response_pack->setAlertContent("Error","Error Komunikasi BPJS");
                 return array();
             }else if( $data['metaData']['code']!="200" ){
                $this->response_pack->setAlertVisible(true);
                $this->response_pack->setAlertContent($data['metaData']['code'],$data['metaData']['message']);
                 return array();
             }else{
               require_once "registration/function/vclaim_decrypt.php";
               $rep = array();
               if(is_string($data['response'])){
                   $hasilcompress = stringDecrypt($this->getKey(), $data['response']);
                   $hasil = decompress($hasilcompress);
                   $repx = json_decode($hasil,true);
                   $querylog->addMessage("Hasil Decrypt : ".$hasil);                   
                   $rep = $repx;
               }else{
                   $rep = $data['response'];
               }
               $final = array();
               $final['tgl_sep'] = $rep['tglSep'];
               $final['nama'] = $rep['peserta']['nama'];
               $final['kartu'] = $rep['peserta']['noKartu'];
               $final['tgl_lahir'] = $rep['peserta']['tglLahir'];
               $final['diagnosa'] = $rep['diagnosa'];
               $pl = explode(" - ",$rep['poli']);
               $final['kode_poli'] = $pl[0];
               $final['poli'] = $pl[1];
               $final['dokter'] = "";
               $final['kode_dokter'] = "";
               return $final;
             }
         }
    }

   

}