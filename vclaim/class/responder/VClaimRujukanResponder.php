<?php 
require_once "registration/class/responder/BPJSResponder.php";
require_once "vclaim/class/responder/VClaimSEPResponder.php";
class VClaimRujukanResponder extends VClaimSEPResponder{

    public function generatePrintRujukan($d){
        global $db;
        $logo = getSettings($db,"reg-bpjs-vclaim-logo",'');
        if($logo==""){
            $logo="registration/resource/image/bpjs-logo.png";
        }else{
            $logo="smis-upload/".$logo;
        }

        $image = "<img style='width:200px;height:auto;' src='".$logo."' />";
        $table = new TablePrint("tbprint","tbprint");
        $table->setMaxWidth(true);
        $table->setDefaultBootrapClass(false);

        $table->addColumn($image,"3","2");
        $table->addColumn("SURAT RUJUKAN","2","1",null,null,"bold");
        $table->addColumn("No. ".$d['rujukan']['noRujukan'],"2","1",null,null,"bold");
        $table->commit("header");

        $table->addColumn(getSettings($db,"smis_autonomous_title",""),"2","1",null,null,"bold");
        $table->addColumn("Tgl. ".ArrayAdapter::format("date d M Y",$d['rujukan']['tglRencanaKunjungan']),"2","1",null,null,"bold");
        $table->commit("header");

        $table->addColumn("&nbsp;","1","1");
        $table->addColumn("&nbsp;","1","1");
        $table->addColumn("&nbsp;","1","1");
        $table->addColumn("&nbsp;","1","1");
        $table->addColumn("&nbsp;","1","1");
        $table->addColumn("&nbsp;","1","1");
        $table->addColumn("&nbsp;","1","1");
        $table->commit("header");

        $table->addColumn("Kepada Yth","1","1");
        $table->addColumn(":","1","1");
        $table->addColumn($d['rujukan']['poliTujuan']['nama'],"5","1");
        $table->commit("body");

        //: "{1-> rawat inap, 2-> rawat jalan}",
        //0->Penuh, 1->Partial, 2->balik PRB
        $table->addColumn("&nbsp;","1","1");
        $table->addColumn("&nbsp;","1","1");
        $table->addColumn($d['rujukan']['tujuanRujukan']['nama'],"3","1");
        $table->addColumn("== ".($_POST['jnsPelayanan']=="1"?"Penuh":($_POST['tipeRujukan']=="2"?"Partial":"Balik PRB"))." ==","2","1");
        $table->commit("body");

        $table->addColumn("Mohon Pemeriksaan dan Penanganan Lebih Lanjut : ","5","1");
        $table->addColumn($_POST['jnsPelayanan']=="1"?"Rawat Inap":"Rawat Jalan","2","1");
        $table->commit("body");

        $table->addColumn("No. Kartu","1","1");
        $table->addColumn(":","1","1");
        $table->addColumn($d['rujukan']['peserta']['noKartu'],"5","1");
        $table->commit("body");

        $table->addColumn("Nama Peserta","1","1");
        $table->addColumn(":","1","1");
        $table->addColumn($d['rujukan']['peserta']['nama']."(".$d['rujukan']['peserta']['kelamin'].")","5","1");
        $table->commit("body");

        $table->addColumn("Tgl. Lahir","1","1");
        $table->addColumn(":","1","1");
        $table->addColumn(ArrayAdapter::format("date d M Y",$d['rujukan']['peserta']['tglLahir']),"5","1");
        $table->commit("body");

        $table->addColumn("Diagnosa","1","1");
        $table->addColumn(":","1","1");
        $table->addColumn($d['rujukan']['diagnosa']['nama'],"5","1");
        $table->commit("body");

        $table->addColumn("Ketarangan","1","1");
        $table->addColumn(":","1","1");
        $table->addColumn($_POST['catatan'],"5","1");
        $table->commit("body");

        $table->addColumn("&nbsp;","1","1");
        $table->addColumn("&nbsp;","1","1");
        $table->addColumn("&nbsp;","1","1");
        $table->addColumn("&nbsp;","1","1");
        $table->addColumn("&nbsp;","1","1");
        $table->addColumn("&nbsp;","1","1");
        $table->addColumn("&nbsp;","1","1");
        $table->commit("body");

        $table->addColumn("Demikian atas bantuanya, diucapkan banyak terima kasih.","7","1");
        $table->commit("body");

        $table->addColumn("* Rujukan Berlaku Sampai Dengan ".ArrayAdapter::format("date d M Y",$d['rujukan']['tglBerlakuKunjungan']),"7","1",null,null,"small");
        $table->commit("body");

        $table->addColumn("* Tgl. Rencana Berkunjung ".ArrayAdapter::format("date d M Y",$d['rujukan']['tglRencanaKunjungan']),"6","1",null,null,"small");
        $table->addColumn("Mengetahui","1","1");
        $table->commit("body");

        $table->addColumn("&nbsp;","1","1");
        $table->addColumn("&nbsp;","1","1");
        $table->addColumn("&nbsp;","1","1");
        $table->addColumn("&nbsp;","1","1");
        $table->addColumn("&nbsp;","1","1");
        $table->addColumn("&nbsp;","1","1");
        $table->addColumn("&nbsp;","1","1");
        $table->commit("body");

        $table->addColumn("Tgl. Cetak ".ArrayAdapter::format("date d M Y",date("Y-m-d h:i A")),"2","1");
        $table->addColumn("&nbsp;","1","1");
        $table->addColumn("&nbsp;","1","1");
        $table->addColumn("&nbsp;","1","1");
        $table->addColumn("&nbsp;","1","1");
        $table->addColumn("_______________","1","1");
        $table->commit("body");

        return $table->getHtml();
    }
   
    public function save(){
        global $db;
        global $querylog;
        $request['request'] = array(
            "t_rujukan"=> array(
                "noSep"=>$_POST['noSep'],
                "tglRujukan"=>$_POST['tglRujukan'],
                "tglRencanaKunjungan"=>$_POST['tglRencanaKunjungan'],
                "ppkDirujuk"=>$_POST['ppkDirujuk'],
                "jnsPelayanan"=>$_POST['jnsPelayanan'],
                "catatan"=>$_POST['catatan'],
                "diagRujukan"=>$_POST['diagRujukan'],
                "tipeRujukan"=>$_POST['tipeRujukan'],
                "poliRujukan"=>$_POST['poliRujukan'],
                "user"=>getSettings($db,"reg-bpjs-vclaim-api-user","")
             )    
        );


        $url    = $this->url_base."/Rujukan/2.0/insert";
         $this->do_curl($this->port,$url,"POST",json_encode($request));  
         if ($this->error) {
            return "cURL Error #:" . $this->error;
          } else {
                global $querylog;
                $querylog->addMessage($this->response);
                $querylog->addMessage("URL ".$url);
                $data=json_decode($this->response,true);
                if($data==null ){
                    $this->response_pack->setAlertVisible(true);
                    $this->response_pack->setAlertContent("Error","Error Komunikasi BPJS");
                    $success['success'] = 0;
                        return $success;
                }else if( $data['metaData']['code']!="200" ){
                    $this->response_pack->setAlertVisible(true);
                    $this->response_pack->setAlertContent($data['metaData']['code'],$data['metaData']['message']);
                    $success['success'] = 0;
                    return $success;
                }else{
                    if(is_string($data['response'])){
                        require_once "registration/function/vclaim_decrypt.php";
                        $hasilcompress = stringDecrypt($this->getKey(), $data['response']);
                        $hasil = decompress($hasilcompress);
                        $repx = json_decode($hasil,true);
                        $querylog->addMessage("Hasil Decrypt : ".$hasil);                   
                        $datax = $repx;
                    }else{
                        $datax = $data['response'];
                    }
                    $success['success'] = 1;
                    
                    $data = array(
                        "noRujukan"=>$data['rujukan']['noRujukan'],
                        "noSep"=>$_POST['noSep'],
                        "tglRujukan"=>$_POST['tglRujukan'],
                        "tglRencanaKunjungan"=>$_POST['tglRencanaKunjungan'],
                        "ppkDirujuk"=>$_POST['ppkDirujuk'],
                        "jnsPelayanan"=>$_POST['jnsPelayanan'],
                        "catatan"=>$_POST['catatan'],
                        "namadiagRujukan"=>$_POST['namadiagRujukan'],
                        "namappkDirujuk"=>$_POST['namappkDirujuk'],
                        "poliRujukan"=>$_POST['poliRujukan'],
                        "namapoliRujukan"=>$_POST['namapoliRujukan'],
                    );
                    $dtable = new DBTable($db,"smis_vclaim_rujukan");
                    $dtable->insert($data);
                    $success['print'] = $this->generatePrintRujukan($datax);
                    $this->response_pack->setAlertVisible(true);
                    $this->response_pack->setAlertContent("Berhasil","Data Rujukan Berhasil disimpan");
                    return $success;


                }
          }
                     
    }

}