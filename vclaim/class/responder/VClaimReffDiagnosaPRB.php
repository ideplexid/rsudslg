<?php 
require_once "registration/class/responder/BPJSResponder.php";
class VClaimReffDiagnosaPRB extends BPJSResponder{

    public function command($command) {
        if ($command != "get_option") {
            return parent::command($command);
        }
        if ($command == "get_option") {
            return $this->getDiagnosaPRBOption();
        }
    }

    private function getDiagnosaPRBOption() {
        $data = $this->getDiagnosaPRB();
        $option = new OptionBuilder();
        if (count($data) > 0) {
            foreach ($data as $d) {
                $option->add($d['nama'], $d['kode']);
            }
        }
        return $option;
    }

    private function getDiagnosaPRB(){
         global $querylog;
         $url  = $this->url_base."/referensi/diagnosaprb";
         
         $this->do_curl($this->port,$url,"GET",NULL);  
         if ($this->error) {
            return "cURL Error #:" . $this->error;
          } else {
              $data=json_decode($this->response,true);
              if($data==null || $data['metaData']['code'] != "200" ){
                  return array();
              }else{
                require_once "registration/function/vclaim_decrypt.php";
                $rep = array();
                if(is_string($data['response'])){
                    $hasilcompress = stringDecrypt($this->getKey(), $data['response']);
                    $hasil = decompress($hasilcompress);
                    $querylog->addMessage($hasil);
                    $repx = json_decode($hasil,true);
                    $rep = $repx['list'];
                }else{
                    $rep = $data['response']['list'];
                }
                return $rep;
              }
        }

     }
}