<?php 
require_once "registration/class/responder/BPJSResponder.php";
class VClaimDataListRujukanResponder extends BPJSResponder{


    
    public function delete(){
        global $db;
        $url  = $this->url_base."/Rujukan/delete";    
        $data = array(
            "request"=>array(
                "t_rujukan"=>array(
                    "noRujukan"=>  $_POST['id'],
                    "user"=>getSettings($db,"reg-bpjs-vclaim-api-user","")
                )
            )
        );
        global $querylog;
        $this->do_curl($this->port,$url,"DELETE",json_encode($data) );
            if ($this->error) {
            return "cURL Error #:" . $this->error;
          } else {
              $querylog->addMessage($this->response);
              $querylog->addMessage("URL ".$url);
              $data=json_decode($this->response,true);
              if($data==null ){
                 $this->response_pack->setAlertVisible(true);
                 $this->response_pack->setAlertContent("Error","Error Komunikasi BPJS");
                 $success['success'] = 0;
                  return $success;
              }else if( $data['metaData']['code']!="200" ){
                 $this->response_pack->setAlertVisible(true);
                 $this->response_pack->setAlertContent($data['metaData']['code'],$data['metaData']['message']);
                 $success['success'] = 0;
                 return $success;
              }else{
                $success['success'] = "1";
                return $success;
            }
	    }
	}

    public function view(){
        global $db;
        global $querylog;
        $dari = $_POST['dari'];
        $sampai = $_POST['sampai'];
        $data = array();
        $url    = $this->url_base."/Rujukan/Keluar/List/tglMulai/$dari/tglAkhir/".$sampai;
        $this->do_curl($this->port,$url,"GET",NULL);  
        if ($this->error) {
            $this->response_pack->setAlertVisible(true);
            $this->response_pack->setAlertContent("Error","cURL Error #:" . $this->error);
            
        } else {
            $querylog->addMessage($this->response);
            $querylog->addMessage("URL ".$url);
            $data=json_decode($this->response,true);
            if($data==null ){
                $this->response_pack->setAlertVisible(true);
                $this->response_pack->setAlertContent("Error","Error Komunikasi BPJS");
                $data= array();
            }else if( $data['metaData']['code']!="200" ){
                $this->response_pack->setAlertVisible(true);
                $this->response_pack->setAlertContent($data['metaData']['code'],$data['metaData']['message']);
                $data= array();
            }else{
                if(is_string($data['response'])){
                    require_once "registration/function/vclaim_decrypt.php";
                    $hasilcompress = stringDecrypt($this->getKey(), $data['response']);
                    $hasil = decompress($hasilcompress);
                    $repx = json_decode($hasil,true);
                    $querylog->addMessage("Hasil Decrypt : ".$hasil);                   
                    $data = $repx['list'];
                }else{
                    $data = $data['response']['list'];
                }
            }
        }

        $uidata				 = $this->adapter->getContent($data);
		$this->uitable->setContent($uidata);
		$list				 = $this->uitable->getBodyContent();
		$json['list']		 = $list;
		if(!$this->debug) {
			unset($d['custom_kriteria']);
			unset($d['query']);
		}
		$json['debug']		 = $this->debug;
		$json['dbtable']	 = array();
		$json['pagination']	 = "";
		$json['number']		 = 1;
		$json['number_p']	 = 0;
		return $json;
                     
    }
}