<?php 
require_once "vclaim/class/responder/VClaimRujukanResponder.php";
class VClaimRujukanUpdateResponder extends VClaimRujukanResponder{

    public function command($command){		
		$this->response_pack = new ResponsePackage();	
		if($command=="cari_local_rujukan"){
			$content		 = $this->cari_rujukan_local();
			$this->response_pack->setContent($content);
			$this->response_pack->setStatus(ResponsePackage::$STATUS_OK);
            return $this->response_pack->getPackage();
		}else{
            return parent::command($command);
        }
	}

    public function cari_rujukan_local(){
        global $db;
        //$dbtabel = new DBTable($db,"smis_vclaim_rujukan");
        //$row = $dbtabel->select(array("noRujukan"=>$_POST['noRujukan']));
       
        $url    = $this->url_base."/Rujukan/Keluar/".$_POST['noRujukan'];
        $this->do_curl($this->port,$url,"GET",NULL);  
        $row=null;
        if ($this->error) {
            $this->response_pack->setAlertVisible(true);
            $this->response_pack->setAlertContent("Error","cURL Error #:" . $this->error);
            return null;
         } else {
               global $querylog;
               $querylog->addMessage($this->response);
               $querylog->addMessage("URL ".$url);
               $data=json_decode($this->response,true);
               if($data==null ){
                   $this->response_pack->setAlertVisible(true);
                   $this->response_pack->setAlertContent("Error","Error Komunikasi BPJS");
                   $success['success'] = 0;
               }else if( $data['metaData']['code']!="200" ){
                   $this->response_pack->setAlertVisible(true);
                   $this->response_pack->setAlertContent($data['metaData']['code'],$data['metaData']['message']);
                   $success['success'] = 0;                   
               }else{
                   if(is_string($data['response'])){
                       require_once "registration/function/vclaim_decrypt.php";
                       $hasilcompress = stringDecrypt($this->getKey(), $data['response']);
                       $hasil = decompress($hasilcompress);
                       $repx = json_decode($hasil,true);
                       $querylog->addMessage("Hasil Decrypt : ".$hasil);                   
                       $data = $repx['rujukan'];
                   }else{
                       $data = $data['response']['rujukan'];
                   }
                   $success['success'] = "1";
                   $row = array(
                        "noSep"=>$data['noSep'],
                       "tglRujukan"=>$data['tglRujukan'],
                       "tglRencanaKunjungan"=>$data['tglRencanaKunjungan'],
                       "ppkDirujuk"=>$data['ppkDirujuk'],
                       "jnsPelayanan"=>$data['jnsPelayanan'],
                       "catatan"=>$data['catatan'],
                       "tipeRujukan"=>$data['tipeRujukan'],
                       "diagRujukan"=>$data['diagRujukan'],
                       "namadiagRujukan"=>$data['namaDiagRujukan'],
                       "namappkDirujuk"=>$data['namaPpkDirujuk'],
                       "poliRujukan"=>$data['poliRujukan'],
                       "namapoliRujukan"=>$data['namaPoliRujukan'],
                   );
                   $dtable = new DBTable($db,"smis_vclaim_rujukan");
                   $dtable->update($data,array("noRujukan"=>$_POST['noRujukan']));
                   $this->response_pack->setAlertVisible(true);
                   $this->response_pack->setAlertContent("Berhasil","Data Rujukan Berhasil ditemukan");
                   return $row;
               }
         }
         return $row;
    }

   
    public function save(){
        global $db;
        global $querylog;
        $request['request'] = array(
            "t_rujukan"=> array(
                "noRujukan"=>$_POST['noRujukan'],
                "tglRujukan"=>$_POST['tglRujukan'],
                "tglRencanaKunjungan"=>$_POST['tglRencanaKunjungan'],
                "ppkDirujuk"=>$_POST['ppkDirujuk'],
                "jnsPelayanan"=>$_POST['jnsPelayanan'],
                "catatan"=>$_POST['catatan'],
                "diagRujukan"=>$_POST['diagRujukan'],
                "tipeRujukan"=>$_POST['tipeRujukan'],
                "poliRujukan"=>$_POST['poliRujukan'],
                "user"=>getSettings($db,"reg-bpjs-vclaim-api-user","")
             )    
        );


        $url    = $this->url_base."/Rujukan/2.0/Update";
         $this->do_curl($this->port,$url,"PUT",json_encode($request));  
         if ($this->error) {
            return "cURL Error #:" . $this->error;
          } else {
                global $querylog;
                $querylog->addMessage($this->response);
                $querylog->addMessage("URL ".$url);
                $data=json_decode($this->response,true);
                if($data==null ){
                    $this->response_pack->setAlertVisible(true);
                    $this->response_pack->setAlertContent("Error","Error Komunikasi BPJS");
                    $success['success'] = 0;
                        return $success;
                }else if( $data['metaData']['code']!="200" ){
                    $this->response_pack->setAlertVisible(true);
                    $this->response_pack->setAlertContent($data['metaData']['code'],$data['metaData']['message']);
                    $success['success'] = 0;
                    return $success;
                }else{
                    if(is_string($data['response'])){
                        require_once "registration/function/vclaim_decrypt.php";
                        $hasilcompress = stringDecrypt($this->getKey(), $data['response']);
                        $hasil = decompress($hasilcompress);
                        $repx = json_decode($hasil,true);
                        $querylog->addMessage("Hasil Decrypt : ".$hasil);                   
                        $data = $repx;
                    }else{
                        $data = $data['response'];
                    }
                    $success['success'] = "1";
                    $datasave = array(
                        "tglRujukan"=>$_POST['tglRujukan'],
                        "tglRencanaKunjungan"=>$_POST['tglRencanaKunjungan'],
                        "ppkDirujuk"=>$_POST['ppkDirujuk'],
                        "jnsPelayanan"=>$_POST['jnsPelayanan'],
                        "catatan"=>$_POST['catatan'],
                        "namadiagRujukan"=>$_POST['namadiagRujukan'],
                        "namappkDirujuk"=>$_POST['namappkDirujuk'],
                        "poliRujukan"=>$_POST['poliRujukan'],
                        "namapoliRujukan"=>$_POST['namapoliRujukan'],
                    );
                    $querylog->addMessage(json_encode($data));
                    $success['print'] = $this->generatePrintRujukan($data);
                    $dtable = new DBTable($db,"smis_vclaim_rujukan");
                    $dtable->update($datasave,array("noRujukan"=>$_POST['noRujukan']));
                    $this->response_pack->setAlertVisible(true);
                    $this->response_pack->setAlertContent("Berhasil","Data Rujukan Berhasil disimpan");
                    return $success;
                }
          }
                     
    }

}