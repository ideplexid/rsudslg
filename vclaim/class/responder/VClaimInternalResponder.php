<?php 
require_once "registration/class/responder/BPJSResponder.php";
require_once "vclaim/class/responder/VClaimSEPResponder.php";
class VClaimInternalResponder extends VClaimSEPResponder{


    public function delete(){
        global $querylog;
        $id = $_POST['id'];
        $idx = explode("||",$id);

        global $db;
        $request['request']['t_rujukan'] = array(
            "idRujukan" =>$idx[0],
            "noRujukan" =>$idx[1],
            "user" => getSettings($db,"reg-bpjs-vclaim-api-user",""),
        );
        $url  = $this->url_base."/Rujukan/Khusus/delete";
        $this->do_curl($this->port,$url,"POST",json_encode($request));  
        if ($this->error) {
            return "cURL Error #:" . $this->error;
          } else {
              $querylog->addMessage($this->response);
              $querylog->addMessage("URL ".$url);
              $data=json_decode($this->response,true);
              if($data==null ){
                 $this->response_pack->setAlertVisible(true);
                 $this->response_pack->setAlertContent("Error","Error Komunikasi BPJS");
                  return array();
              }else if( $data['metaData']['code']!="200" ){
                 $this->response_pack->setAlertVisible(true);
                 $this->response_pack->setAlertContent($data['metaData']['code'],$data['metaData']['message']);
                  return array();
              }else{
                $final['success'] = "1";
                return $final;
            }
	}
}


	

    public function view(){	
		$d					 = $this->getRujukanKontrol($_POST['s_tahun'],$_POST['s_bulan']);
		$page				 = 1;
		$data				 = $d;
		$max_page			 = 1;

		$this->adapter->setNumber($page*1000);
		$uidata				 = $this->adapter->getContent($data);
		$this->uitable->setContent($uidata);
		$list				 = $this->uitable->getBodyContent();
		$json['list']		 = $list;

		if(!$this->debug) {
			unset($d['custom_kriteria']);
			unset($d['query']);
		}
		$json['debug']		 = $this->debug;
		$json['dbtable']	 = $d;
		if($this->include_adapter_data){
			$json['adapter'] = $uidata;
		}
		if($this->include_raw_data){
			$json['raw']	 = $data;
		}
		$json['pagination']	 = "";
		$json['number']		 = 1;
		$json['number_p']	 = 1;
		return $json;
	}


    public function getRujukanKontrol($tahun="",$bulan=""){
        global $querylog;
        global $db;
        $url  = $this->url_base."/Rujukan/Khusus/List/Bulan/$bulan/Tahun/".$tahun;
        $this->do_curl($this->port,$url,"GET",NULL);  
        if ($this->error) {
           return "cURL Error #:" . $this->error;
         } else {
             $querylog->addMessage($this->response);
             $querylog->addMessage("URL ".$url);
             $data=json_decode($this->response,true);
             if($data==null ){
               $this->response_pack->setAlertVisible(true);
               $this->response_pack->setAlertContent("Error","Error Komunikasi BPJS");
                 return array();
             }else if( $data['metaData']['code']!="200" ){
               $this->response_pack->setAlertVisible(true);
               $this->response_pack->setAlertContent($data['metaData']['code'],$data['metaData']['message']);
                 return array();
             }else{
               require_once "registration/function/vclaim_decrypt.php";
               $rep = array();
               if(is_string($data['response'])){
                   $hasilcompress = stringDecrypt($this->getKey(), $data['response']);
                   $hasil = decompress($hasilcompress);
                   $repx = json_decode($hasil,true);
                   $rep = $repx['rujukan'];
               }else{
                   $rep = $data['response']['rujukan'];
               }
               $final = array();
               foreach($rep as $x){
                   $x['id'] = $x['idrujukan']."||".$x['norujukan'];
                   $final[] = $x;    
               }
               return $final;
             }
         }

    }

    public function cari_sep(){
        global $querylog;
        global $db;
        $url  = $this->url_base."/RencanaKontrol/nosep/".$_POST['nosep'];
        $this->do_curl($this->port,$url,"GET",NULL);  
        if ($this->error) {
           return "cURL Error #:" . $this->error;
         } else {
             $querylog->addMessage($this->response);
             $querylog->addMessage("URL ".$url);
             $data=json_decode($this->response,true);
             if($data==null ){
                $this->response_pack->setAlertVisible(true);
                $this->response_pack->setAlertContent("Error","Error Komunikasi BPJS");
                 return array();
             }else if( $data['metaData']['code']!="200" ){
                $this->response_pack->setAlertVisible(true);
                $this->response_pack->setAlertContent($data['metaData']['code'],$data['metaData']['message']);
                 return array();
             }else{
               require_once "registration/function/vclaim_decrypt.php";
               $rep = array();
               if(is_string($data['response'])){
                   $hasilcompress = stringDecrypt($this->getKey(), $data['response']);
                   $hasil = decompress($hasilcompress);
                   $repx = json_decode($hasil,true);
                   $querylog->addMessage("Hasil Decrypt : ".$hasil);                   
                   $rep = $repx;
               }else{
                   $rep = $data['response'];
               }
               $final = array();
               $final['tgl_sep'] = $rep['tglSep'];
               $final['nama'] = $rep['peserta']['nama'];
               $final['kartu'] = $rep['peserta']['noKartu'];
               $final['tgl_lahir'] = $rep['peserta']['tglLahir'];
               $final['diagnosa'] = $rep['diagnosa'];
               $pl = explode(" - ",$rep['poli']);
               $final['kode_poli'] = $pl[0];
               $final['poli'] = $pl[1];
               $final['dokter'] = "";
               $final['kode_dokter'] = "";
               return $final;
             }
         }
    }


    public function cari_rujukan($no_rujukan,$asal=""){
        global $db;
        $url    = $this->url_base."/rujukan/".$_POST['noRujukan'];
        $this->do_curl($this->port,$url,"GET",NULL);  
        $row=null;
        if ($this->error) {
            $this->response_pack->setAlertVisible(true);
            $this->response_pack->setAlertContent("Error","cURL Error #:" . $this->error);
            return null;
         } else {
               global $querylog;
               $querylog->addMessage($this->response);
               $querylog->addMessage("URL ".$url);
               $data=json_decode($this->response,true);
               if($data==null ){
                   $this->response_pack->setAlertVisible(true);
                   $this->response_pack->setAlertContent("Error","Error Komunikasi BPJS");
                   $success['success'] = 0;
               }else if( $data['metaData']['code']!="200" ){
                   $this->response_pack->setAlertVisible(true);
                   $this->response_pack->setAlertContent($data['metaData']['code'],$data['metaData']['message']);
                   $success['success'] = 0;                   
               }else{
                   if(is_string($data['response'])){
                       require_once "registration/function/vclaim_decrypt.php";
                       $hasilcompress = stringDecrypt($this->getKey(), $data['response']);
                       $hasil = decompress($hasilcompress);
                       $repx = json_decode($hasil,true);
                       $querylog->addMessage("Hasil Decrypt : ".$hasil);                   
                       $data = $repx['rujukan'];
                   }else{
                       $data = $data['response']['rujukan'];
                   }
                   $success['success'] = "1";
                   
                   $row = array(
                    "noSep"=>$data['noSep'],
                    "tglRujukan"=>$data['tglRujukan'],
                    "tglRencanaKunjungan"=>$data['tglRencanaKunjungan'],
                    "ppkDirujuk"=>$data['ppkDirujuk'],
                    "jnsPelayanan"=>$data['jnsPelayanan'],
                    "catatan"=>$data['catatan'],
                    "tipeRujukan"=>$data['tipeRujukan'],
                    "namadiagRujukan"=>$data['namaDiagRujukan'],
                    "namappkDirujuk"=>$data['namaPpkDirujuk'],
                    "poliRujukan"=>$data['poliRujukan'],
                    "namapoliRujukan"=>$data['namaPoliRujukan'],
                   );
                   //$dtable = new DBTable($db,"smis_vclaim_rujukan");
                   //$dtable->update($data,array("noRujukan"=>$_POST['noRujukan']));
                   $this->response_pack->setAlertVisible(true);
                   $this->response_pack->setAlertContent("Berhasil","Data Rujukan Berhasil Ditemukan");
                   return $row;
               }
         }
         return $row;
    }

    public function cek_bpjs($nobpjs,$tglsep){
        $methode    = "GET";
        $request    = "";        
        $url    = $this->url_base."Peserta/nokartu/".$nobpjs."/tglSEP/".$tglsep;
        
        $this->do_curl($this->port,$url,$methode,$request);
        $data   = null;
        if ($this->error) {
            require_once "registration/function/bpjs_null.php";
            $data   = bpjs_null();
        } else {
            $response   = json_decode($this->response,true);
            if($response==null || $response['metaData']['code']!="200" ){
                require_once "registration/function/bpjs_null.php";
                $data   = bpjs_null($response['metaData']['message']);
            }else{
                require_once "registration/function/bpjs_found.php";
                $data   = bpjs_found($response,$this->getKey());
            }
        }
        require_once "registration/function/bpjs_table_checker.php";
        $table  = bpjs_table_checker($data);
        return $table;
    }

    public function save(){
        global $db;
        global $querylog;
        $request = array(
            "noRujukan"=>$_POST['noRujukan'],
            "diagnosa"=>array(
                array("kode"=>"P;".$_POST['diagnosaPrimer']),
                array("kode"=>"S;".$_POST['diagnosaSekunder']),
            ),
            "procedure"=>array(
                array("kode"=>$_POST['procedure'])
            ),
            "user"=>getSettings($db,"reg-bpjs-vclaim-api-user","")
        );
        $url    = $this->url_base."/Rujukan/Khusus/insert";
         $this->do_curl($this->port,$url,"POST",json_encode($request));  
         if ($this->error) {
            return "cURL Error #:" . $this->error;
          } else {
                global $querylog;
                $querylog->addMessage($this->response);
                $querylog->addMessage("URL ".$url);
                $data=json_decode($this->response,true);
                if($data==null ){
                    $this->response_pack->setAlertVisible(true);
                    $this->response_pack->setAlertContent("Error","Error Komunikasi BPJS");
                    $success['success'] = 0;
                        return $success;
                }else if( $data['metaData']['code']!="200" ){
                    $this->response_pack->setAlertVisible(true);
                    $this->response_pack->setAlertContent($data['metaData']['code'],$data['metaData']['message']);
                    $success['success'] = 0;
                    return $success;
                }else{
                    if(is_string($data['response'])){
                        $hasilcompress = stringDecrypt($this->getKey(), $data['response']);
                        $hasil = decompress($hasilcompress);
                        $repx = json_decode($hasil,true);
                        $querylog->addMessage("Hasil Decrypt : ".$hasil);                   
                        $data = $repx;
                    }else{
                        $data = $data['response'];
                    }
                    $success['success'] = "1";
                    return $success;
                }
          }
                     
    }

}