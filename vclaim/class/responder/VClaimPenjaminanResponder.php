<?php 
require_once "registration/class/responder/BPJSResponder.php";
class VClaimPenjaminanResponder extends BPJSResponder{

    public function command($command){		
		$this->response_pack = new ResponsePackage();	
		if($command=="save"){
            $content		 = $this->save();
			$this->response_pack->setContent($content);
			$this->response_pack->setStatus(ResponsePackage::$STATUS_OK);
            return $this->response_pack->getPackage();
        }else if($command=="approve"){
            $content		 = $this->approve();
			$this->response_pack->setContent($content);
			$this->response_pack->setStatus(ResponsePackage::$STATUS_OK);
            return $this->response_pack->getPackage();
        }else if($command=="cek_bpjs"){
            $content		 = $this->cek_bpjs($_POST['noKartu'],$_POST['tglSep']);
			$this->response_pack->setContent($content);
			$this->response_pack->setStatus(ResponsePackage::$STATUS_OK);
            return $this->response_pack->getPackage();
        }else{
            return parent::command($command);
        }
	}


    public function approve(){
        global $db;
        global $querylog;
        $id = $_POST['id'];
        $cur = $this->getDBTable()->select($id);
        $request['request'] = array(
            "t_sep"=>array(
                "noKartu"=>$cur->noKartu,
                "tglSep"=>$cur->tglSep,
                "jnsPelayanan"=>$cur->jnsPelayanan,
                "jnsPengajuan"=>$cur->jnsPengajuan,
                "keterangan"=>$cur->keterangan,               
                "user"=>getSettings($db,"reg-bpjs-vclaim-api-user","")
            )
        );

        if($cur->jnsPengajuan==1){
            unset($request['request']['t_sep']['jnsPengajuan']);
        }

        $url    = $this->url_base."/Sep/aprovalSEP";
         $this->do_curl($this->port,$url,"POST",json_encode($request));  
         if ($this->error) {
            return "cURL Error #:" . $this->error;
          } else {
              $querylog->addMessage($this->response);
              $querylog->addMessage("URL ".$url);
              $data=json_decode($this->response,true);
              if($data==null ){
                $this->response_pack->setAlertVisible(true);
                $this->response_pack->setAlertContent("Error","Error Komunikasi BPJS");
                  return array();
              }else if( $data['metaData']['code']!="200" ){
                  $this->response_pack->setAlertVisible(true);
                  $this->response_pack->setAlertContent($data['metaData']['code'],$data['metaData']['message']);
                  return array();
              }else{
                $this->response_pack->setAlertVisible(true);
                $this->response_pack->setAlertContent('Berhasil',"Berhasil di Approve");
                $final['success'] = 1;
                $final['id'] = 1;

                $ud = array(
                    "status"=>1,
                );
                $this->getDBTable()->update($ud,array("id"=>$_POST['id']));
                return $final;
              }
          }
                     
    }

    


    public function save(){
        global $db;
        global $querylog;
        $request['request'] = array(
            "t_sep"=>array(
                "noKartu"=>$_POST['noKartu'],
                "tglSep"=>$_POST['tglSep'],
                "jnsPelayanan"=>$_POST['jnsPelayanan'],
                "jnsPengajuan"=>$_POST['jnsPengajuan'],
                "keterangan"=>$_POST['keterangan'],               
                "user"=>getSettings($db,"reg-bpjs-vclaim-api-user","")
            )
        );
        $url    = $this->url_base."/Sep/pengajuanSEP";
         $this->do_curl($this->port,$url,"POST",json_encode($request));  
         if ($this->error) {
            return "cURL Error #:" . $this->error;
          } else {
              $querylog->addMessage($this->response);
              $querylog->addMessage("URL ".$url);
              $data=json_decode($this->response,true);
              if($data==null ){
                $this->response_pack->setAlertVisible(true);
                $this->response_pack->setAlertContent("Error","Error Komunikasi BPJS");
                $final['success'] = 0;
                $final['id'] = 0;
                return $final;
              }else if( $data['metaData']['code']!="200" ){
                  $this->response_pack->setAlertVisible(true);
                  $this->response_pack->setAlertContent($data['metaData']['code'],$data['metaData']['message']);
                  $final['success'] = 0;
                  $final['id'] = 0;
                  return $final;
              }else{
                $final['success'] = 1;
                $final['id'] = 1;
                $final = parent::save();
                $this->response_pack->setAlertVisible(true);
                $this->response_pack->setAlertContent("Behasil","Data Berhasil disimpan");
                return $final;
              }
          }                     
    }

    public function cek_bpjs($nobpjs,$tglsep){
        $methode    = "GET";
        $request    = "";        
        $url    = $this->url_base."Peserta/nokartu/".$nobpjs."/tglSEP/".$tglsep;
        
        $this->do_curl($this->port,$url,$methode,$request);
        $data   = null;
        if ($this->error) {
            require_once "registration/function/bpjs_null.php";
            $data   = bpjs_null();
        } else {
            $response   = json_decode($this->response,true);
            if($response==null || $response['metaData']['code']!="200" ){
                require_once "registration/function/bpjs_null.php";
                $data   = bpjs_null($response['metaData']['message']);
            }else{
                require_once "registration/function/bpjs_found.php";
                $data   = bpjs_found($response,$this->getKey());
            }
        }
        require_once "registration/function/bpjs_table_checker.php";
        $table  = bpjs_table_checker($data);
        $data['table'] = $table;
        return $data;
    }

   

}