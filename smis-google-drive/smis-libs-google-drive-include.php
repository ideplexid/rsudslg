<?php 

require_once 'smis-google-drive/Google/autoload.php';
define('APPLICATION_NAME', 'Safethree MIS');
define('SMIS_GOOGLE_CREDENTIALS', 'smis-google-drive-credential');
define('CLIENT_SECRET_PATH', 'smis-google-drive/client_secret.json');
define('SCOPES', implode(' ', array(Google_Service_Drive::DRIVE_FILE)));

?>