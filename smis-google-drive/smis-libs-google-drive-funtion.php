<?php 

/**
 * Returns an authorized API client.
 * @return Google_Client the authorized client object
 */
function getClient() {
	$client = new Google_Client();
	$client->setApplicationName(APPLICATION_NAME);
	$client->setScopes(SCOPES);
	$client->setAuthConfigFile(CLIENT_SECRET_PATH);
	$client->setAccessType('offline');

	// Load previously authorized credentials from a file.
	$credentialsPath = expandHomeDirectory(CREDENTIALS_PATH);
	if (file_exists($credentialsPath)) {
		$accessToken = file_get_contents($credentialsPath);
	} else {
		// Request authorization from the user.
		$authUrl = $client->createAuthUrl();
		printf("Open the following link in your browser:\n%s\n", $authUrl);
		
		$authCode="4/Ho7usR5YbNBXr2li9X6K-i5adt2ploUxP9hB8bcn8CY";
		// Exchange authorization code for an access token.
		$accessToken = $client->authenticate($authCode);

		// Store the credentials to disk.
		if(!file_exists(dirname($credentialsPath))) {
			mkdir(dirname($credentialsPath), 0700, true);
		}
		file_put_contents($credentialsPath, $accessToken);
		printf("Credentials saved to %s\n", $credentialsPath);
	}
	$client->setAccessToken($accessToken);

	// Refresh the token if it's expired.
	if ($client->isAccessTokenExpired()) {
		$client->refreshToken($client->getRefreshToken());
		file_put_contents($credentialsPath, $client->getAccessToken());
	}
	return $client;
}

function putGoogleFile($service,$filename,$content,$parent){
	$fileMetadata = new Google_Service_Drive_DriveFile(array(
	  'name' => $filename.'.json',
	  'parents' => array($parent),
	  'mimeType' => 'application/json'));
	$file = $service->files->create($fileMetadata, array(
	  'data' => $content,
	  'mimeType' => 'text/plain',
	  'uploadType' => 'multipart',
	  'fields' => 'id'));
	return $file->id;
}

function putGoogleFolder($service,$filename,$parent){
	$fileMetadata = new Google_Service_Drive_DriveFile(array(
			'name' => $filename,
			'parents' => array($parent),
			'mimeType' => 'application/vnd.google-apps.folder'));
	$file = $service->files->create($fileMetadata, array('fields' => 'id'));
	return $file->id;
}

function searchGoogleFolder($service,$filename,$parent){
	$parameter=array();
	$parameter['pageSize']=1;
	$parameter['q']="name='$filename' 
					and '$parent' in parents and mimeType = 'application/vnd.google-apps.folder' 
					and trashed = false";
	$parameter['fields']="nextPageToken, files(id, name)";	
	$results = $service->files->listFiles($parameter);
	
	if (count($results->getFiles()) != 0) {
		foreach ($results->getFiles() as $file) {
			if($file->getName()==$filename)
				return $file->getId();
		}
	}
	return null;
}

function updateGoogleFile($service,$fileid,$content){
	// First retrieve the file from the API.
    $file = $service->files->get($fileid);
    // File's new metadata.
    $additionalParams = array(
		'uploadType' => 'multipart',
        'data' => $content
    );
   $service->files->update($fileid, (new Google_Service_Drive_DriveFile()), $additionalParams);
}

function searchGoogleFile($service,$filename,$parent){
	$parameter=array();
	$filename=$filename.".json";
	$parameter['pageSize']=1;
	$parameter['q']="name='$filename' 
					and '$parent' in parents 
					and trashed = false";
	$parameter['fields']="nextPageToken, files(id, name)";	
	$results = $service->files->listFiles($parameter);
	
	if (count($results->getFiles()) != 0) {
		foreach ($results->getFiles() as $file) {
			if($file->getName()==$filename)
				return $file->getId();
		}
	}
	return null;
}

function getGoogleDriveClient($db){
	$client = new Google_Client();
	$client->setApplicationName(APPLICATION_NAME);
	$client->setScopes(SCOPES);
	$client->setAuthConfigFile(CLIENT_SECRET_PATH);
	$client->setAccessType('offline');
	$token=getGoogleDriveToken($client, $db);
	$client->setAccessToken($token);
	refreshGoogleDriveToken($client, $db);
	return $client;
}


function getGoogleDriveToken($client,$db){
	return getSettings($db, SMIS_GOOGLE_CREDENTIALS, "");
}

function refreshGoogleDriveToken($client,$db){
	if ($client->isAccessTokenExpired()) {
		$client->refreshToken($client->getRefreshToken());
		setSettings($db, SMIS_GOOGLE_CREDENTIALS, $client->getAccessToken());
		//file_put_contents($credentialsPath, $client->getAccessToken());
	}
	return $client;
}

/**
 * Expands the home directory alias '~' to the full path.
 * @param string $path the path to expand.
 * @return string the expanded path.
 */
function expandHomeDirectory($path) {
	$homeDirectory = getenv('HOME');
	if (empty($homeDirectory)) {
		$homeDirectory = getenv("HOMEDRIVE") . getenv("HOMEPATH");
	}
	return str_replace('~', realpath($homeDirectory), $path);
}

?>