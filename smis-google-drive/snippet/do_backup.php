<?php 
global $db;

$tbl_name=$_POST['tbl_name'];
$dbtable=new DBTable($db, $tbl_name);
$dbtable->setDebuggable(true);
$dbtable->setDelete(true);
$filename="";

require_once 'smis-google-drive/smis-libs-google-drive-include.php';
require_once 'smis-google-drive/smis-libs-google-drive-funtion.php';
$client=getGoogleDriveClient($db);
$service = new Google_Service_Drive($client);
$CURRENT_FOLDER_BASE_ID=getSettings($db,"smis-google-drive-base-folder-id","");
$CURRENT_FOLDER_BASE_NAME=getSettings($db,"smis-google-drive-base-folder-name","");

$ldata=NULL;
if(!file_exists("smis-backup/".$tbl_name)){
	mkdir("smis-backup/".$tbl_name);
}
$digit=$_POST['digit_file'];
$digitn=$_POST['digit_name'];
$numeric=ArrayAdapter::format("only-digit".$digitn, $_POST['file_step']);
$filename=$CURRENT_FOLDER_BASE_NAME."/".$tbl_name."/".$numeric.".json";
$data=$dbtable	->setMaximum($_POST['max_request'])
				->setFetchMethode(DBTable::$ARRAY_FETCH)
				->view("", $_POST['page_number']);
$ldata=$data['data'];
$first_data=$ldata[0];
$json=json_encode($first_data);

$parent_id=searchGoogleFolder($service,$tbl_name,$CURRENT_FOLDER_BASE_ID);
if($parent_id==null){
	$parent_id=putGoogleFolder($service,$tbl_name,$CURRENT_FOLDER_BASE_ID);
}

$google_id=searchGoogleFile($service,$tbl_name."_".$numeric,$parent_id);
if($google_id==null){
	$google_id=putGoogleFile($service,$tbl_name."_".$numeric,$json,$parent_id);
}else{
	updateGoogleFile($service,$google_id,$json);
}

$data['filename']=$filename;
$data['google_id']=$google_id;
$res=new ResponsePackage();
$dt=$res->setContent($data)
		->setStatus(ResponsePackage::$STATUS_OK)
		->getPackage();
echo json_encode($dt);
return;


	

?>