<?php 
	global $db;
	require_once 'smis-google-drive/smis-libs-google-drive-include.php';
	require_once 'smis-google-drive/smis-libs-google-drive-funtion.php';
	$client=getGoogleDriveClient($db);
	$service = new Google_Service_Drive($client);
	$FOLDER_BASE=$_POST['base_folder_name'];
	$CURRENT_FOLDER_BASE_ID=$_POST['base_folder_id'];
	$FOLDER_BASE_ID="";
	
	$parameter=array();
	$parameter['pageSize']=10;
	$parameter['q']="name='$FOLDER_BASE' and mimeType = 'application/vnd.google-apps.folder'";
	$parameter['fields']="nextPageToken, files(id, name)";	
	$results = $service->files->listFiles($parameter);
	
	if (count($results->getFiles()) != 0) {
		foreach ($results->getFiles() as $file) {
			if($file->getName()==$FOLDER_BASE)
				$FOLDER_BASE_ID=$file->getId();
		}
	} 
	$message="";
	if($FOLDER_BASE_ID==$CURRENT_FOLDER_BASE_ID && $CURRENT_FOLDER_BASE_ID!=""){
		$message="Folder Base sudah sama dengan yang di settings tidak perlu ada yang dibuat ulang </br>
					Nama Folder Base: <strong>".$FOLDER_BASE."</strong> </br>
					GoID Folder Base: <strong>".$CURRENT_FOLDER_BASE_ID."</strong>";
	}else if($FOLDER_BASE_ID!=$CURRENT_FOLDER_BASE_ID  && $FOLDER_BASE_ID!=""){
		$message="Folder Base ada tetapi memiliki ID yang berbeda dengan yang aslinya, dan system telah mengupdate yang baru </br>
				 Nama Folder Base: <strong>".$FOLDER_BASE."</strong> </br>
				 GoID Folder Lama : <strong>".$CURRENT_FOLDER_BASE_ID."</strong></br>
				 GoID Folder Baru : <strong>".$FOLDER_BASE_ID."</strong>";
	}else{
		/*create baru*/
		$fileMetadata = new Google_Service_Drive_DriveFile(array(
			'name' => $FOLDER_BASE,
			'mimeType' => 'application/vnd.google-apps.folder'));
		$file = $service->files->create($fileMetadata, array('fields' => 'id'));
		$FOLDER_BASE_ID=$file->id;
		
		
		$message="Folder Base tidak ada sehingga system membuat Baru </br>
					Nama Folder Base: <strong>".$FOLDER_BASE."</strong> </br>
					GoID Folder Base: <strong>".$FOLDER_BASE_ID."</strong>";
	}
	setSettings($db,"smis-google-drive-base-folder-name",$FOLDER_BASE);
	setSettings($db,"smis-google-drive-base-folder-id",$FOLDER_BASE_ID);
	
	
	$response=new ResponsePackage();
	$response->setStatus(ResponsePackage::$STATUS_OK);
	$response->setWarning(true,"Peringatan",$message);
	$response->setContent($FOLDER_BASE_ID);
	echo json_encode($response->getPackage());
?>