<?php 

	global $db;
	require_once 'smis-google-drive/smis-libs-google-drive-include.php';
	require_once 'smis-google-drive/smis-libs-google-drive-funtion.php';
	$client=getGoogleDriveClient($db);
	$service = new Google_Service_Drive($client);
	$FOLDER_BASE_ID=getSettings($db,"smis-google-drive-base-folder-id","");
	$DB_NAME=$_POST['dbname'];
	// Print the names and IDs for up to 10 files.
	//$optParams = array('pageSize' => 10,'fields' => "nextPageToken, files(id, name)");
	$parameter=array();
	$parameter['pageSize']=10;
	$parameter['fields']="nextPageToken, files(id, name)";
	$parameter['q']="'$FOLDER_BASE_ID' in parents";
	
	
	$file = new Google_Service_Drive_DriveFile();	
	//Setup the Folder to Create
	$fileMetadata = new Google_Service_Drive_DriveFile(array(
			'name' => $DB_NAME,
			'parents' => array($FOLDER_BASE_ID),
			'mimeType' => 'application/vnd.google-apps.folder'));
	$file = $service->files->create($fileMetadata, array('fields' => 'id'));
	printf("Folder ID: %s\n", $file->id);

?>