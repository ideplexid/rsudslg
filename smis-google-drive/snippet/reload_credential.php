<?php 
	require_once 'smis-google-drive/smis-libs-google-drive-include.php';
	require_once 'smis-google-drive/smis-libs-google-drive-funtion.php';
	global $db;
	$client = new Google_Client();
	$client->setApplicationName(APPLICATION_NAME);
	$client->setScopes(SCOPES);
	$client->setAuthConfigFile(CLIENT_SECRET_PATH);
	$client->setAccessType('offline');

	$credentialsPath = expandHomeDirectory(CREDENTIALS_PATH);
	$authCode=getSettings($db, "smis-google-drive-token", "");
	$accessToken = $client->authenticate($authCode);
	setSettings($db, SMIS_GOOGLE_CREDENTIALS, $accessToken);

	$pack=new ResponsePackage();
	$pack->setWarning(true, "Credential Created", "Credential File Have Been Created, From Now System Will Use This Credential");
	$pack->setStatus(ResponsePackage::$STATUS_OK);
	$pack->setAlertVisible(false);
	echo json_encode($pack->getPackage());
	return;
?>