<?php 
global $db;
$logtable=array();
$logtable[]="smis_adm_entity_response";
$logtable[]="smis_adm_error";
$logtable[]="smis_adm_log";
$logtable[]="smis_adm_service_request";
$logtable[]="smis_adm_service_responds";
$logtable[]="smis_sb_log";
$logtable[]="smis_adm_upload";
$hasil=NULL;
if($_POST['mode']=="log"){		
	$hasil=$logtable;																	//backup only log
}else{
	$dbtable = new DBTable ( $db, "smis_table" );
	$dbtable->setFetchMethode ( DBTable::$ARRAY_FETCH );
	$dbtable->setShowAll(true);
	if($_POST['mode']=="notlog"){														//backup all except log
		$data=$dbtable->view("", "0");
		$ldata=$data['data'];
		$hasil=array();
		foreach($ldata as $d){
			if(!in_array($d['id'], $logtable))
				$hasil[]=$d['id'];
		}
	}else{
		if($_POST['mode']=="kriteria"){
			$dbtable->addCustomKriteria(" id ", "LIKE '%".$_POST['kriteria']."%' ");		//backup all that accept by kriteria 
		}else if($_POST['mode']=="sql"){
			$dbtable->setCustomKriteria($_POST['kriteria']);
		}else {
			if(strpos($_POST['mode'], "serverbus")!==false){
				$dbtable->ConjuctionEachCustomKriteria(" OR ");
				$dbtable->addCustomKriteria(NULL, "id LIKE 'smis_sb_%' ");
			}
			if(strpos($_POST['mode'], "essential")!==false){
				$dbtable->ConjuctionEachCustomKriteria(" OR ");
				$dbtable->addCustomKriteria(NULL, "id LIKE 'smis_adm_%' ");
				$dbtable->addCustomKriteria(NULL, "id LIKE 'smis_base_%' ");
			}
			if(strpos($_POST['mode'], "tools")!==false){
				$dbtable->ConjuctionEachCustomKriteria(" OR ");
				$dbtable->addCustomKriteria(NULL, "id LIKE 'smis_tools_%' ");
			}
			if($_POST['mode']=="modul"){
				$dbtable->addCustomKriteria(NULL," id NOT LIKE 'smis_sb_%'  ");
				$dbtable->addCustomKriteria(NULL," id NOT LIKE 'smis_tools_%'  ");
				$dbtable->addCustomKriteria(NULL," id NOT LIKE 'smis_adm_%'  ");
				$dbtable->addCustomKriteria(NULL," id NOT LIKE 'smis_base_%'  ");
			}
		}			
		
		$data=$dbtable->view("", "0");
		$ldata=$data['data'];
		$hasil=array();
		foreach($ldata as $d){
			$hasil[]=$d['id'];
		}
	}
}

$res=new ResponsePackage();
$dt=$res->setContent($hasil)
		->setStatus(ResponsePackage::$STATUS_OK)
		->getPackage();
echo json_encode($dt);

?>