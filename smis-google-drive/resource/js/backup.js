var backup;
var all_table;
var total_backup=0;
var total_done=0;
var IS_BACKUP_RUNNING=false;


function backup_run(){
	if(IS_BACKUP_RUNNING){
		backupRunning();
		return;
	}
	var r_data=backup.getRegulerData();
	r_data['super_command']="list_item";
	r_data['mode']=$("#backup_load_mode").val();
	r_data['kriteria']=$("#backup_criteria").val();
	
	$.post('',r_data,function(res){
		var json=getContent(res);
		all_table=json;
		total_backup=all_table.length;
		total_done=0;
		var d=new Date();
		var year=d.getFullYear();
		var month=d.getMonth()+1;
		var day=d.getDate();
		var hour=d.getHours();
		var minute=d.getMinutes();
		var second=d.getSeconds();
		month=month<10?"0"+month:month;
		day=day<10?"0"+day:day;
		hour=hour<10?"0"+hour:hour;
		minute=minute<10?"0"+minute:minute;
		second=second<10?"0"+second:second;
		$("#logtbl_list").html("");
		var folder=year+"_"+month+"_"+day+"___"+hour+"__"+minute+"__"+second;
		fullBackup(folder);
	});
}

/**
 * inilah proses backup yang sebenarnya
 * setiap table dalam database 
 * memiliki beberapa data
 * yang nantinya akan di backup sedikit demi sedikit
 * */
function recursiveBackup(folder,nama_table,page,total_page,max_load,sisa_max_load,file_step,digit_file,digit_name){
	var r_data=backup.getRegulerData();
	r_data['super_command']="do_backup";
	r_data['tbl_name']=nama_table;
	r_data['page_number']=page;
	r_data['max_load']=max_load;
	r_data['folder']=folder;
	r_data['file_step']=file_step;
	r_data['digit_file']=digit_file;
	r_data['digit_name']=digit_name;
	r_data['max_request']=$("#backup_max_request").val();	
	$.post('',r_data,function(res){
		var json=getContent(res);
		var html="<tr>";
			html+="<td>"+nama_table+"</td>";
			html+="<td>"+json.filename+"</td>";
			html+="<td>"+json.page_post+" / "+json.max_page+" ( "+json.asal_maxpost+" ) </td>";
			html+="<td>"+json.limit+"</td>";
			html+="<td>"+json.google_id+"</td>";
		html+="</tr>";
		
		$("#logtbl_list").append(html);
		sisa_max_load=sisa_max_load-Number($("#backup_max_request").val());
		if(sisa_max_load<=0){
			sisa_max_load=max_load;
			file_step++;
		}
		var persen=Math.floor(page*100/total_page);
		$("#database_bar").sload("true",nama_table,persen);
		if(page<total_page-1) {
			recursiveBackup(folder,nama_table,++page,total_page,max_load,sisa_max_load,file_step,digit_file,digit_name);
		}else{
			$("#database_bar").sload("true","...",0);
			do_full_backup(folder);
		}
	});
}

/*
 * melakukan backup pada 
 * seluruh table tergantung table 
 * yang ada di all_table
 * */
function do_full_backup(folder){
	if(all_table.length>0){
		$("#sload_modal").modal("show");
		IS_BACKUP_RUNNING=true;
		var ps=total_done*100/total_backup;
		$("#unit_bar").sload("true","Database",ps);
		var d=all_table.splice(0, 1);
		var id=d[0];
		oneBackup(id,folder);
		total_done++;		
	}else{
		IS_BACKUP_RUNNING=false;
		$("#unit_bar").sload("true","",0);
		$("#sload_modal").modal("hide");
		 reload_history();
	}
}

/**
 * cuma pertanyaan untuk memastikan apakah user telah siap menggunakan 
 * sistem ini kalau-kalau dia melakukan backup pada satu file saja
 * */
function fullBackup(folder){
	
	if(Number($("#backup_max_request").val()) > Number($("#backup_max_load").val())  && Number($("#backup_max_load").val()>0) ){
		var html="<ul>";
		html+="<li>Nilai Data Request Harus Kurang Dari Sama dengan Nilai Data Load </li>";
		html+="<li>Semakin Besar nilai Data Request Semakin Besar Terjadi Kegagalan karena kehabisan Memory</li>";
		html+="<li>Semakin Kecil nilai Data Request Semakin Ringan Tetapi Semakin Lama Waktu Yang dibutuhkan</li>";
		html+="<li>Semakin Besar Nilai Data Load Semakin Sedikit Jumlah File yang dihasilkan, dan proses semakin cepat tetapi Semakin Besar Pula Kegagalan restore akibat kekurangan Memory</li>";
		html+="<li>Semakin Kecil Data Load Semakin Banyak File yang Dihasilkan dan proses semakin lambat tetapi Semakin Ringan dan Tingkat Keberhasilan Restore Semakin Besar</li>";
		html+="</ul>";
		showWarning("Error",html);
		return;
	}
	
	if(Number($("#backup_max_load").val()==-1) ){
		var html="<ul>";
		html+="<li>Opsi Ini Menggunakan Single File</li>";
		html+="<li>Sistem akan Menjadi semua backup ke dalam satu file saja</li>";
		html+="<li>Kekuranganya saat melakukan Import ke database lain harus menggunakan MySQL Workbench</li>";
		html+="</ul>";

		bootbox.confirm(html+" Anda Yakin ?? ", function(result) {
			if (result ) {                                             
				do_full_backup(folder);                         
			} 
		}); 
	}else{
		do_full_backup(folder);
	}
	
}

/*melakukan backup satu table tertentu tertentu*/
function oneBackup(id,folder){
	var e_data=backup.getEditData();
	e_data['command']="select";
	e_data['id']=id;
	
	$.post('',e_data,function(res){
		var json=getContent(res);
		var size=Number(json.tt);
		var nama_table=id;
		var page=0;
		var total_page=Math.floor(size/Number($("#backup_max_request").val()));
		var max_load=Number($("#backup_max_load").val());
		var sisa_max_load=max_load;
		var file_step=1;
		var digit_file=digit(Math.ceil(size/max_load));
		var digit_name=digit(size);
		
		$("#database_bar").sload("true",nama_table,0);
		recursiveBackup(folder,nama_table,page,total_page,max_load,sisa_max_load,file_step,digit_file,digit_name);
		
	});
}

function digit(digit_file){

	if(digit_file<10) digit_file=1;
	else if(digit_file<100) digit_file=2;
	else if(digit_file<1000) digit_file=3;
	else if(digit_file<10000) digit_file=4;
	else if(digit_file<100000) digit_file=5;
	else if(digit_file<1000000) digit_file=6;
	else if(digit_file<10000000) digit_file=7;
	else if(digit_file<100000000) digit_file=8;
	else if(digit_file<1000000000) digit_file=9;
	else if(digit_file<10000000000) digit_file=10;
	return digit_file;
}

function backupRunning(){
	var html="<ul>";
		html+="<li>Backup Sedang Berjalan Anda Tidak Dapat Membuat Backup Baru, Sampai Backup yang Sebelumnya Selesai</li>";
		html+="<li>Tekan F5 atau Refresh untuk Mengakhiri Backup yang Sedang Berjalan</li>";
		html+="<li>Atau Tunggu Sampai Selesai</li>";
	html+="</ul>";
	showWarning("Backup Sedang Berjalan",html);
}


function reload_history(){
	var history_data=backup.getRegulerData();
	history_data['action']="backup_history";
	showLoading();
	$.post("",history_data,function(res){
		$("#backup_history").html(res);
		dismissLoading();
	});
}

$(document).ready(function(){
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
	$("#backup_load_mode").on("change",function(){
		if($("#backup_load_mode").val()=="kriteria" || $("#backup_load_mode").val()=="sql") {
			$("#backup_criteria").prop('disabled', false);
			$("#backup_criteria").val("");
		} else { 
			$("#backup_criteria").prop('disabled', true);
			$("#backup_criteria").val("");
		}
	});
	var column=new Array('id');
	backup=new TableAction("backup","smis-google-drive","backup",column);
	backup.view();

	/*dipakai untuk melakukan backup pada satu table saja*/
	backup.do_backup=function(id){
		$("#logtbl_list").html("");
		if(IS_BACKUP_RUNNING){
			backupRunning();
			return;
		}
		all_table=new Array();
		all_table.push(id);
		total_backup=all_table.length;
		total_done=0;
		var d=new Date();
		var folder=d.getFullYear()+"_"+(d.getMonth()+1)+"_"+d.getDate()+"___"+d.getHours()+"__"+d.getMinutes()+"__"+d.getSeconds();
		fullBackup(folder);
	};
});
