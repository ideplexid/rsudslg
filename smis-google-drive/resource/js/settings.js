function reload_credential(){
	var a={};
	a['page']="smis-google-drive";
	a['action']="reload_credential";
	a['super_command']="";
	a['prototype_name']="";
	a['prototype_slug']="";
	a['prototype_implement']="";
	showLoading();
	$.post("",a,function(res){
		getContent(res);
		dismissLoading();
	});
};
	
function get_google_base_folder(){
	var a={};
	a['page']="smis-google-drive";
	a['action']="base_folder";
	a['base_folder_name']=$("#smis-google-drive-base-folder-name").val();
	a['base_folder_id']=$("#smis-google-drive-base-folder-id").val();
	a['super_command']="";
	a['prototype_name']="";
	a['prototype_slug']="";
	a['prototype_implement']="";
	showLoading();
	$.post("",a,function(res){
		var json=getContent(res);
		$("#smis-google-drive-base-folder-id").val(json);
		dismissLoading();
	});
}