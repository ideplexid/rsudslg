<?php
global $PLUGINS;
require_once ("smis-framework/smis/api/Plugin.php");
$init ['name'] = 'smis-google-drive';
$init ['path'] = SMIS_DIR . "smis-google-drive/";
$init ['description'] = "This Plugin using for Google Drive Access For Administration";
$init ['require'] = "smis-administrator";
$init ['service'] = "nothing";
$init ['version'] = "1.0.0";
$init ['number'] = "1";
$init ['type'] = "";

$myplugins = new Plugin ( $init );
$PLUGINS [$init ['name']] = $myplugins;
?>