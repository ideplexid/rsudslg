<?php
	global $db;
	global $user;
	require_once 'smis-libs-class/Policy.php';
	$policy=new Policy("smis-google-drive", $user);
	$policy->setDefaultPolicy(Policy::$DEFAULT_POLICY_ALLOW);
	$policy->addPolicy("backup","backup",Policy::$DEFAULT_COOKIE_CHANGE,"modul/backup");
	$policy->addPolicy("settings","settings");
	$policy->addPolicy("reload_credential","settings",Policy::$DEFAULT_COOKIE_KEEP,"snippet/reload_credential");
	$policy->addPolicy("base_folder","settings",Policy::$DEFAULT_COOKIE_KEEP,"snippet/base_folder");
	
	$policy->initialize();
?>