<?php
require_once 'smis-framework/smis/api/SettingsBuilder.php';
require_once 'smis-google-drive/smis-libs-google-drive-include.php';
require_once 'smis-google-drive/smis-libs-google-drive-funtion.php';
global $db;

$client = new Google_Client();
$client->setApplicationName(APPLICATION_NAME);
$client->setScopes(SCOPES);
$client->setAuthConfigFile(CLIENT_SECRET_PATH);
$client->setAccessType('offline');

$authUrl = $client->createAuthUrl();
$smis = new SettingsBuilder( $db, "google_drive_settings", "smis-google-drive", "settings" );
$smis->setShowDescription ( true );
$smis->addTabs("credential", "Credential Settings","fa fa-key");
$smis->addJS("smis-google-drive/resource/js/settings.js");
$url = new SettingsItem ( $db, "smis-google-drive-token", "Put Token Here", "", "text", "If Empty Go To This Link <a class='btn btn-primary' target='_blank' href='".$authUrl."'> <i class='fa fa-forward'></i> </a>, Save Then Click <a class='btn btn-primary' onclick='reload_credential();' href='javascript:;'  ><i class='fa fa-refresh'></i></a>" );
$smis->addItem ( "credential", $url );
$url = new SettingsItem ( $db, "smis-google-drive-base-folder-name", "Base Folder Name", "smis", "text", "This is the base folder name, give it the folder name and system will create the credential ID in google Drive <a class='btn btn-primary' onclick='get_google_base_folder();' href='javascript:;'  ><i class='fa fa-cloud-upload'></i></a>" );
$smis->addItem ( "credential", $url );
$url = new SettingsItem ( $db, "smis-google-drive-base-folder-id", "Base Folder ID", "", "text", "It Will Be Generated from system" );
$smis->addItem ( "credential", $url );
$response = $smis->init ();
?>