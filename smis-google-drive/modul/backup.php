<?php 
show_error();
global $db;
if(isset($_POST['super_command']) && $_POST['super_command']=="list_item"){
	require_once "smis-google-drive/snippet/list_database.php";
	return;
}

if(isset($_POST['super_command']) && $_POST['super_command']=="do_backup"){
	require_once "smis-google-drive/snippet/do_backup.php";
	return;
}

require_once 'smis-google-drive/class/adapter/BackupAdapter.php';
$button=new Button("", "", "");
$button	->setIsButton(Button::$ICONIC)
		->setIcon(" fa fa-database")
		->setClass("btn-primary");

$head=array("No","Nama","Total","Ukuran","Waktu");
$uitable = new Table ($head);
$uitable->addContentButton("do_backup", $button);



$uitable->setName ( "backup" )
		->setAddButtonEnable(false)
		->setReloadButtonEnable(false)
		->setEditButtonEnable(false)
		->setPrintButtonEnable(false)
		->setDelButtonEnable(false);

if (isset ( $_POST ['command'] )) {
	$adapter = new BackupAdapter ();
	$adapter->add("Nama", "table_name")
			->add("Total", "table_rows","number")
			->add("Ukuran", "size","number")
			->add("Waktu", "time","date d M Y, H:i:s")
			->setUseNumber(true,"No");
	$dbtable = new DBTable ( $db, "smis_table" );
	$dbtable->setFetchMethode ( DBTable::$ARRAY_FETCH );
	$dbres = new DBResponder ( $dbtable, $uitable, $adapter );
	$data = $dbres	->setUseAdapterForSelect(true)
		  		 	->command ( $_POST ['command'] );
	echo json_encode ( $data );
	return;
}
			
$mode=new OptionBuilder();
$mode 	->add("All Table","alltable","1")
		->add("All Table Without Log","notlog","0")
		->add("Log Only","log","0")
		->add("Modul Only","modul","0")
		->add("Safethree Essential","essential","0")
		->add("Safethree Tools","tools","0")
		->add("Safethree Serverbus","serverbus","0")
		->add("Safethree Essential + Tools","essential-tools","0")
		->add("Safethree Essential + Serverbus","essential-serverbus","0")
		->add("Safethree Serverbus + Tools","serverbus-tools","0")
		->add("Safethree Essential + Serverbus + Tools","essential-serverbus-tools","0")
		->add("Criteria","kriteria","0")
		->add("SQL","sql","0");

$uitable->addModal("max_request", "hidden", "", 1)
		->addModal("max_load", "hidden", "",1)
		->addModal("load_mode", "select", "Filter Table", $mode->getContent())
		->addModal("criteria", "text", "Kriteria", "","",NULL,true);
$backup_action=new Button("log", "Run", "Running Backup");
$backup_action->setIsButton(Button::$ICONIC_TEXT)
			  ->setIcon(" fa fa-dot-circle-o")
			  ->setAction("backup_run()")
			  ->setClass("btn-primary");
$help=new Button("bantuan", "bantuan", "Bantuan");
$help->setIsButton(Button::$ICONIC_TEXT)
	 ->setIcon(" fa fa-question-circle")
	 ->setAction("help('smis-google-drive','backup')")
	 ->setClass("btn-primary");


$form=$uitable->getModal()->getForm();
$form->addElement("", $backup_action);
$form->addElement("", $help);

$close=new Button("", "", "Batal");
$close	->addClass("btn-primary")
		->setIsButton(Button::$ICONIC_TEXT)
		->setIcon("fa fa-close")
		->setAction("location.reload()");

$load=new LoadingBar("database_bar", "");
$unit=new LoadingBar("unit_bar", "");
$modal=new Modal("sload_modal", "", "Backup in Progress");
$modal->addHTML($unit->getHtml(),"after")
	  ->addHTML($load->getHtml(),"after")
	  ->addFooter($close);

$logtbh=array("Table","Google Location","Page","Limit","Google File ID");
$uilogable=new Table($logtbh);
$uilogable->setName("logtbl");
$uilogable->setFooterVisible(false);
$uilogable->setAction(false);

echo $form->getHtml();
echo "<div class='clear'></div>";
echo $uitable->getHtml ();
echo "<div class='clear'></div>";
echo $modal->getHtml();
echo addJS ( "base-js/smis-base-loading.js");
echo addJS ( "framework/smis/js/table_action.js" );
echo addJS ( "smis-google-drive/resource/js/backup.js",false);
echo addCSS ( "smis-google-drive/resource/css/backup.css",false);
echo "<div class='clear'></div>";
echo $uilogable->getHtml()

?>