<?php 

class BackupAdapter extends SimpleAdapter{
	
	private $total=0;
	private $ukuran=0;
	
	public function adapt($d){
		$array=parent::adapt($d);
		global $db;
		$query="SELECT count(*) as total FROM  ".$d['id'];
		$total=$db->get_var($query);
		$array['Total']=self::format("number", $total);	
		$array['tt']=$total;
		$array['Ukuran']=self::format("text-right", $this->getUkuran($d['size']*1));
		$this->total+=$total;
		$this->ukuran+=$d['size']*1;
		$array['id']=$d['id'];
		return $array;
	}
	
	public function getUkuran($size){
		if($size<1024){
			return number_format($size,0,",",".")." B ";
		}
		if($size<1048576){
			return number_format(ceil($size/1024),0,",",".")." KB ";
		}
		if($size<1073741824){
			return number_format(ceil($size/1048576),0,",",".")." MB ";
		}
		return number_format(ceil($size/1073741824),0,",",".")." GB ";
	}
	
	public function getContent($data){
		$content=array();
		foreach($data as $d){
			$adapt=$this->adapt($d);
			$content[]=$adapt;
		}
		$array=array();
		$array["Nama"]="<strong>Total</strong>";
		$array["No."]="";
		$array["Total"]=self::format("number", $this->total);	
		$array["Ukuran"]=self::format("text-right", $this->getUkuran($this->ukuran));;
		$array["Waktu"]="";
		
		$content[]=$array;
		return $content;
	}
	
}


?>