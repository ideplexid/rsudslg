<?php
	global $NAVIGATOR;
	$mr = new Menu ("fa fa-google-plus-square");
	$mr->addProperty ( 'title', 'Google Drive' );
	$mr->addProperty ( 'name', 'Google Drive' );
	$mr->addSubMenu ( "Backup Google Drive", "smis-google-drive", "backup", "Backup Cloud to Google Drive" ,"fa fa-cloud-upload");
	
	$mr->addSubMenu ( "Drive Browser", "smis-google-drive", "drive_browser", "Google Drive Browser" ,"fa fa-google-plus");
	$mr->addSubMenu ( "Settings", "smis-google-drive", "settings", "Settings" ,"fa fa-cog");
	$NAVIGATOR->addMenu ( $mr, 'smis-google-drive' );
?>
