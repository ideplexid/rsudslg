<?php 
/**
 * this responder specially for 
 * detail paket, because each paket
 * must be top down for every treatment.
 * 
 * @version     : 1.0.0
 * @since       : 14 Aug 2017
 * @license     : LGPLv3
 * @author      : Nurul Huda
 * @copyright   : goblooge@gmail.com
 * */
class PaketResponder extends DBResponder{
    
    public function postToArray(){
		$result=array();
		foreach ($this->column as $c){
			if(isset($_POST[$c])){
				$value=$_POST[$c];
				if(is_array($value) || in_array($c,$this->column_json)){
					switch($c){
                        case "laboratory"   :   $this->getDetailTarif($result,"laboratory","smis_lab_harga",$_POST['kelas'],$value);    break;
                        case "radiology"    :   $this->getDetailTarif($result,"radiology","smis_rad_harga",$_POST['kelas'],$value);     break;
                        case "fisiotherapy" :   $this->getDetailTarif($result,"fisiotherapy","smis_fst_harga",$_POST['kelas'],$value);  break;
                        case "elektromedis" :   $this->getDetailTarif($result,"elektromedis","smis_emd_harga",$_POST['kelas'],$value);  break;
                    }
				}else{
					$result[$c]=$value;
				}				
			}
		}		
		foreach($this->column_fix_value as $c=>$v){
			$result[$c]=$v;
		}		
		return $result;
	}
    
    
    public function getDetailTarif(&$data,$ruangan,$settings_name,$kelas,$tindakan){
        $harga      = getSettings ( $this->getDBTable()->get_db(), $settings_name, "" );
        $list_harga = json_decode ( $harga,true );
        $total=0;
        $filtered_harga = array ();
        $clean_tindakan = array();
        
        foreach($tindakan as $key=>$value){
            if($value=="0")
                continue;
            
            $nilai = $list_harga[$kelas."_".$key];
            $filtered_harga[$kelas."_".$key] = $nilai;
            $clean_tindakan[$key] = 1;
            $total += $nilai;
        }
        
        $data[$ruangan]=json_encode($clean_tindakan);
        $data["tarif_".$ruangan]=json_encode($filtered_harga);
        $data["total_".$ruangan]=$total;
        if(!isset($data["total"])){
            $data["total"]=0;
        }
        $data["total"]+=$total;
    }
}

?>