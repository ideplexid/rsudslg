<?php 

class ManajemenTarifResponder extends DuplicateResponder{
    private $update_column;
    
    public function __construct($dbtable,$uitable,$adapter,$duplicate){
        parent::__construct($dbtable,$uitable,$adapter);
        $this->setDuplicate($duplicate=="1","duplicate_manajemen");
        $this->setAutonomous(getSettings($dbtable->get_db(),"smis_autonomous_id",""));
        $this->setEntity("manajemen");
        $this->addNotifyData("table",$dbtable->getName());
        $this->update_column=array();
    }
    
    public function addUpdateColumn($name){
        $this->update_column[]=$name;
        return $this;
    }
    
    public function command($command){
        if($command=="backup"){
            $this->backup();
            $res=new ResponsePackage();
            $res->setStatus(ResponsePackage::$STATUS_OK);
            $res->setAlertVisible(true);
            $res->setContent("");
            $res->setAlertContent("Berhasil", "Berhasil di-backup");
            return $res->getPackage();
        }else if($command=="restore"){
            $this->restore();
            $res=new ResponsePackage();
            $res->setStatus(ResponsePackage::$STATUS_OK);
            $res->setAlertVisible(true);
            $res->setContent("");
            $res->setAlertContent("Berhasil", "Berhasil di-restore");
            return $res->getPackage();
        }else if($command=="terapkan"){
            $this->terapkan();
            $res=new ResponsePackage();
            $res->setStatus(ResponsePackage::$STATUS_OK);
            $res->setAlertVisible(true);
            $res->setContent("");
            $res->setAlertContent("Berhasil", "Berhasil diterapkan");
            return $res->getPackage();
        }else{
            return parent::command($command);
        }
    }
    
    private function backup(){
        $up=array();
        foreach($this->update_column as $name){
           $up[$name."_lama"]=$name;
           $warp[$name."_lama"]=DBController::$NO_STRIP_WRAP;
        }        
        $up['origin_updated']=$this->getAutonomous();
        $up['time_updated']=date("Y-m-d H:i:s");
        $up['duplicate']="0";
        $id=array();
        return $this->dbtable->update($up,$id,$warp);
    }
    
    private function restore(){
        $up=array();
        foreach($this->update_column as $name){
           $up[$name]=$name."_lama";
           $warp[$name]=DBController::$NO_STRIP_WRAP;
        }        
        
        $up['origin_updated']=$this->getAutonomous();
        $up['time_updated']=date("Y-m-d H:i:s");
        $up['duplicate']="0";
        $id=array();
        return $this->dbtable->update($up,$id,$warp);
    }
    
    private function terapkan(){
        $up=array();
       foreach($this->update_column as $name){
           $up[$name]=$name."_baru";
           $warp[$name]=DBController::$NO_STRIP_WRAP;
        }
        $up['origin_updated']=$this->getAutonomous();
        $up['time_updated']=date("Y-m-d H:i:s");
        $up['duplicate']="0";
        $id=array();
        return $this->dbtable->update($up,$id,$warp);
    }
}

?>