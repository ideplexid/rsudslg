<?php 

class SettingsBuilderKarcis extends SettingsBuilder{


    public function getSuperCommandJavascript(){
        parent::getSuperCommandJavascript();
        $slug = $_POST['slug'];
        if(file_exists("manajemen/resource/js/karcis_".$slug.".js")){
            echo addJS("manajemen/resource/js/karcis_".$slug.".js",false);
        }
    }

}