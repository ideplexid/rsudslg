<?php
	global $db;
	global $user;
	require_once 'smis-libs-class/Policy.php';
	$policy=new Policy("manajemen", $user);
	$policy	->setDefaultPolicy(Policy::$DEFAULT_POLICY_RESTRICT)
			->addPolicy("provit_sharing","provit_sharing",Policy::$DEFAULT_COOKIE_CHANGE,"modul/provit_sharing")				
			->addPolicy("provit_sharing_default","provit_sharing",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/provit_sharing/provit_sharing_default")
			->addPolicy("provit_sharing_bpjs","provit_sharing",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/provit_sharing/provit_sharing_bpjs")
			->addPolicy("provit_sharing_karyawan","provit_sharing",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/provit_sharing/provit_sharing_karyawan")
			
            ->addPolicy("data_induk","data_induk",Policy::$DEFAULT_COOKIE_CHANGE,"modul/data_induk")
			->addPolicy("rumus_pv","data_induk",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/data_induk/rumus_pv")
            ->addPolicy("gizi","data_induk",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/data_induk/gizi")
            ->addPolicy("mobil_ambulan","data_induk",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/data_induk/mobil_ambulan")
            ->addPolicy("bbm","data_induk",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/data_induk/bbm")

            ->addPolicy("paket_rlfe","paket_rlfe",Policy::$DEFAULT_COOKIE_CHANGE,"modul/paket_rlfe")
			->addPolicy("tarif_oksigen","tarif_oksigen",Policy::$DEFAULT_COOKIE_CHANGE,"modul/tarif_oksigen")
			
            
			->addPolicy("tarif_penunjang","tarif_penunjang",Policy::$DEFAULT_COOKIE_CHANGE,"modul/tarif_penunjang")
			->addPolicy("tarif_bronchoscopy","tarif_penunjang",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/tarif_penunjang/tarif_bronchoscopy")
			->addPolicy("tarif_endoscopy","tarif_penunjang",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/tarif_penunjang/tarif_endoscopy")
			->addPolicy("tarif_spirometry","tarif_penunjang",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/tarif_penunjang/tarif_spirometry")
			->addPolicy("tarif_audiometry","tarif_penunjang",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/tarif_penunjang/tarif_audiometry")
			->addPolicy("tarif_faalparu","tarif_penunjang",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/tarif_penunjang/tarif_faalparu")
			->addPolicy("tarif_ekg","tarif_penunjang",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/tarif_penunjang/tarif_ekg")
			->addPolicy("tarif_ambulan","tarif_penunjang",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/tarif_penunjang/tarif_ambulan")
			->addPolicy("sewa_ambulan","tarif_penunjang",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/tarif_penunjang/sewa_ambulan")
			->addPolicy("tarif_gizi","tarif_penunjang",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/tarif_penunjang/tarif_gizi")
			->addPolicy("tindakan_farmasi", "tarif_penunjang", Policy::$DEFAULT_COOKIE_KEEP, "resource/php/tarif_penunjang/tindakan_farmasi")
			->addPolicy("tarif_ambulan_bbm", "tarif_penunjang", Policy::$DEFAULT_COOKIE_KEEP, "resource/php/tarif_penunjang/tarif_ambulan_bbm")
			

			->addPolicy("tarif_ccsd","tarif_ccsd",Policy::$DEFAULT_COOKIE_CHANGE,"modul/tarif_ccsd")
			->addPolicy("tarif_dokter","tarif_dokter",Policy::$DEFAULT_COOKIE_CHANGE,"modul/tarif_dokter")
			->addPolicy("tarif_visite","tarif_dokter",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/tarif_dokter/tarif_visite")
			->addPolicy("tarif_konsul","tarif_dokter",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/tarif_dokter/tarif_konsul")
			->addPolicy("tarif_tindakan_dokter_jalan","tarif_dokter",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/tarif_dokter/tarif_tindakan_dokter_jalan")
			->addPolicy("tarif_tindakan_dokter_inap","tarif_dokter",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/tarif_dokter/tarif_tindakan_dokter_inap")
			->addPolicy("tarif_tindakan_operasi","tarif_dokter",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/tarif_dokter/tarif_tindakan_operasi")
			->addPolicy("tarif_tindakan_vk","tarif_dokter",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/tarif_dokter/tarif_tindakan_vk")
			->addPolicy("tarif_anastesi","tarif_dokter",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/tarif_dokter/tarif_anastesi")
			
			->addPolicy("keperawatan","keperawatan",Policy::$DEFAULT_COOKIE_CHANGE,"modul/keperawatan")
			->addPolicy("tarif_keperawatan","keperawatan",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/keperawatan/tarif_keperawatan")
			->addPolicy("tarif_keperawatan_inap","keperawatan",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/keperawatan/tarif_keperawatan_inap")
			->addPolicy("derivatif_keperawatan","keperawatan",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/keperawatan/derivatif_keperawatan")
			
			->addPolicy("tarif_radiology","tarif_radiology",Policy::$DEFAULT_COOKIE_CHANGE,"modul/tarif_radiology")
			->addPolicy("harga_radiology","tarif_radiology",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/tarif_radiology/harga_radiology")
			->addPolicy("rencana_harga_radiology","tarif_radiology",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/tarif_radiology/rencana_harga_radiology")
			->addPolicy("bhp_radiology","tarif_radiology",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/tarif_radiology/bhp_radiology")
			->addPolicy("sewa_alat_radiology","tarif_radiology",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/tarif_radiology/sewa_alat_radiology")
			->addPolicy("jasa_pelayanan_radiology","tarif_radiology",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/tarif_radiology/jasa_pelayanan_radiology")
			->addPolicy("unit_cost_radiology","tarif_radiology",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/tarif_radiology/unit_cost_radiology")
			->addPolicy("perujuk_radiology","tarif_radiology",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/tarif_radiology/perujuk_radiology")
			->addPolicy("total_radiology","tarif_radiology",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/tarif_radiology/total_radiology")
			->addPolicy("sisa_radiology","tarif_radiology",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/tarif_radiology/sisa_radiology")
			->addPolicy("bacaan_radiology","tarif_radiology",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/tarif_radiology/bacaan_radiology")
			->addPolicy("rs_radiology","tarif_radiology",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/tarif_radiology/rs_radiology")
			
			->addPolicy("tarif_laboratory","tarif_laboratory",Policy::$DEFAULT_COOKIE_CHANGE,"modul/tarif_laboratory")
			->addPolicy("harga_laboratory","tarif_laboratory",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/tarif_laboratory/harga_laboratory")
			->addPolicy("rencana_harga_laboratory","tarif_laboratory",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/tarif_laboratory/rencana_harga_laboratory")
			->addPolicy("bhp_laboratory","tarif_laboratory",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/tarif_laboratory/bhp_laboratory")
			->addPolicy("sewa_alat_laboratory","tarif_laboratory",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/tarif_laboratory/sewa_alat_laboratory")
			->addPolicy("jasa_pelayanan_laboratory","tarif_laboratory",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/tarif_laboratory/jasa_pelayanan_laboratory")
			->addPolicy("unit_cost_laboratory","tarif_laboratory",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/tarif_laboratory/unit_cost_laboratory")
			->addPolicy("perujuk_laboratory","perujuk_laboratory",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/tarif_laboratory/perujuk_laboratory")
			->addPolicy("total_laboratory","total_laboratory",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/tarif_laboratory/total_laboratory")
			->addPolicy("rs_laboratory","tarif_laboratory",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/tarif_laboratory/rs_laboratory")
			
			->addPolicy("tarif_patology","tarif_patology",Policy::$DEFAULT_COOKIE_CHANGE,"modul/tarif_patology")
			->addPolicy("harga_patology","tarif_patology",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/tarif_patology/harga_patology")
			->addPolicy("rencana_harga_patology","tarif_patology",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/tarif_patology/rencana_harga_patology")
			->addPolicy("bhp_patology","tarif_patology",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/tarif_patology/bhp_patology")
			->addPolicy("sewa_alat_patology","tarif_patology",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/tarif_patology/sewa_alat_patology")
			->addPolicy("jasa_pelayanan_patology","tarif_patology",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/tarif_patology/jasa_pelayanan_patology")
			->addPolicy("unit_cost_patology","tarif_patology",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/tarif_patology/unit_cost_patology")
			->addPolicy("perujuk_patology","perujuk_patology",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/tarif_patology/perujuk_patology")
			->addPolicy("total_patology","total_patology",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/tarif_patology/total_patology")
			->addPolicy("rs_patology","tarif_patology",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/tarif_patology/rs_patology")
			

			->addPolicy("tarif_fisiotherapy","tarif_fisiotherapy",Policy::$DEFAULT_COOKIE_CHANGE,"modul/tarif_fisiotherapy")
			->addPolicy("harga_fisiotherapy","tarif_fisiotherapy",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/tarif_fisiotherapy/harga_fisiotherapy")
			->addPolicy("rencana_harga_fisiotherapy","tarif_laboratory",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/tarif_fisiotherapy/rencana_harga_fisiotherapy")
			->addPolicy("bhp_fisiotherapy","tarif_laboratory",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/tarif_fisiotherapy/bhp_fisiotherapy")
			->addPolicy("sewa_alat_fisiotherapy","tarif_laboratory",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/tarif_fisiotherapy/sewa_alat_fisiotherapy")
			->addPolicy("jasa_pelayanan_fisiotherapy","tarif_laboratory",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/tarif_fisiotherapy/jasa_pelayanan_fisiotherapy")
			->addPolicy("unit_cost_fisiotherapy","tarif_laboratory",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/tarif_fisiotherapy/unit_cost_fisiotherapy")			
			
			->addPolicy("tarif_elektromedis","tarif_elektromedis",Policy::$DEFAULT_COOKIE_CHANGE,"modul/tarif_elektromedis")
			->addPolicy("harga_elektromedis","tarif_elektromedis",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/tarif_elektromedis/harga_elektromedis")
			->addPolicy("rencana_harga_elektromedis","tarif_elektromedis",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/tarif_elektromedis/rencana_harga_elektromedis")
			->addPolicy("bhp_elektromedis","tarif_elektromedis",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/tarif_elektromedis/bhp_elektromedis")
			->addPolicy("sewa_alat_elektromedis","tarif_elektromedis",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/tarif_elektromedis/sewa_alat_elektromedis")
			->addPolicy("jasa_pelayanan_elektromedis","tarif_elektromedis",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/tarif_elektromedis/jasa_pelayanan_elektromedis")
			->addPolicy("unit_cost_elektromedis","tarif_elektromedis",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/tarif_elektromedis/unit_cost_elektromedis")

            
            ->addPolicy("kelas","kelas",Policy::$DEFAULT_COOKIE_CHANGE,"modul/kelas")
			->addPolicy("tarif_ok","tarif_ok",Policy::$DEFAULT_COOKIE_CHANGE,"modul/tarif_ok")
			->addPolicy("karcis","karcis",Policy::$DEFAULT_COOKIE_CHANGE,"modul/karcis")
			->addPolicy("tarif_ambulance","tarif_ambulance",Policy::$DEFAULT_COOKIE_CHANGE,"modul/tarif_ambulance")
			->addPolicy("terapkan","terapkan",Policy::$DEFAULT_COOKIE_CHANGE,"modul/terapkan")
			->addPolicy("settings","settings",Policy::$DEFAULT_COOKIE_CHANGE,"modul/settings")
			->initialize();
?>
