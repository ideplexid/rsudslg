<?php
require_once 'smis-framework/smis/api/SettingsBuilder.php';
require_once 'smis-base/smis-include-service-consumer.php';
global $db;
$service = new ServiceConsumer ( $db, "get_list_fisiotherapy" );
$service->execute ();
$array = $service->getContent ();
$slug = "smis_fst_harga";
$settings = new SettingsBuilder ( $db, "harga", "manajemen", "harga_fisiotherapy", "" );
$settings->setSingle ( true, $slug );
$settings->setPrefix("harga_laboratory");
$settings->setOneVariable ( true, "smis_fst_harga" );

$dbtable = new DBTable ( $db, "smis_mjm_kelas" );
$dbtable->setShowAll ( true );
$dbtable->setOrder(" nama ASC ");
$data = $dbtable->view ( "", "0" );
$select = new SelectAdapter ( "nama", "slug" );
$kelas = $select->getContent ( $data ['data'] );

foreach ( $kelas as $element => $value ) {
	$k = $value ['name'];
	$v = $value ['value'];
	$settings->addTabs ( "harga_laboratory".$v, $k,"fa fa-money" );
    if($settings->isGroup("harga_laboratory".$v)){
        foreach ( $array as $ak => $av ) {
            $setitem = new SettingsItem ( $db, $v . "_" . $ak, $av, "0", "money", "", true, $slug );
            $settings->addItem ( "harga_laboratory".$v, $setitem );
        }
    }
}
$settings->setPartialLoad(true);
$settings->init ();
?>