<?php

global $db;
require_once ("smis-base/smis-include-service-consumer.php");
require_once 'smis-libs-hrd/EmployeeResponder.php';
require_once "smis-base/smis-include-duplicate.php";
require_once 'manajemen/class/responder/ManajemenTarifResponder.php';

$dktable = new Table ( array ('Nama','Jabatan',"NIP"), "", NULL, true );
$dktable->setName ( "dokter_tarif_tindakan_operasi" );
$dktable->setModel ( Table::$SELECT );
$dkadapter = new SimpleAdapter ();
$dkadapter->add ( "Jabatan", "nama_jabatan" );
$dkadapter->add ( "Nama", "nama" );
$dkadapter->add ( "NIP", "nip" );
$dokter = new EmployeeResponder ( $db, $dktable, $dkadapter, "dokter" );

$super = new SuperCommand ();
$super->addResponder ( "dokter_tarif_tindakan_operasi", $dokter );
$init = $super->initialize ();
if ($init != null) {
	echo $init;
	return;
}
$duplicate=getSettings($db,"manajer-duplicate-tarif-tindakan-operasi","0");
$header=array ('No.','Nama','Dokter','Kelas','Pasien',"Jenis Operasi",'Tarif Dokter','Team OK',"Sewa Kamar" );
$uitable = new TableSynchronous ( $header, "Tarif Tindakan Dokter", NULL, true );
$uitable->setName ( "tarif_tindakan_operasi" );
$uitable->setReloadButtonEnable(false);
$uitable->setLoopDuplicateButtonEnable ($duplicate!="0");
$uitable->setDuplicateButton($duplicate!="0");
$uitable->setSynchronizeButton(false);
$uitable->setReloadButtonEnable(false);
$uitable->setPrintButtonEnable(false);


if (isset ( $_POST ['command'] )) {
	$adapter = new SynchronousViewAdapter ();
	$adapter->setUseNumber(true, "No.","back.");
	$adapter->add ( "Dokter", "nama_dokter" );
	$adapter->add ( "Nama", "nama" );
	$adapter->add ( "Jenis Operasi", "jenis_operasi" );
	$adapter->add ( "Kelas", "kelas", "unslug" );
	$adapter->add ( "Pasien", "jenis_pasien", "unslug" );
	$adapter->add ( "Tarif Dokter", "tarif", "money Rp." );
	$adapter->add ( "Team OK", "ok", "money Rp." );
	$adapter->add ( "Sewa Kamar", "tarif_sewa_ok", "money Rp." );
	$dbtable = new DBTable ( $db, "smis_mjm_tindakan_operasi" );
	$dbtable->setFetchMethode ( DBTable::$ARRAY_FETCH );
	$dbtable->setOrder(" nama_dokter ASC ");
	$dbres = new ManajemenTarifResponder ( $dbtable, $uitable, $adapter,$duplicate );
    $dbres->addUpdateColumn("tarif");   
    $dbres->addUpdateColumn("ok");
    $dbres->addUpdateColumn("tarif_sewa_ok"); 
	$data = $dbres->command ( $_POST ['command'] );
	echo json_encode ( $data );
	return;
}

$is_full=getSettings($db, "manajer-operasi-mode", "0")=="1";

$lama=new Button("", "", "Backup");
$lama->setIsButton(Button::$ICONIC_TEXT);
$lama->setIcon(" fa fa-backward");
$lama->setClass(" btn-primary ");
$lama->setAction("tarif_tindakan_operasi.backup()");
$restore=new Button("", "", "Restore");
$restore->setIsButton(Button::$ICONIC_TEXT);
$restore->setClass(" btn-primary ");
$restore->setIcon(" fa fa-forward");
$restore->setAction("tarif_tindakan_operasi.restore()");
$baru=new Button("", "", "Terapkan");
$baru->setIsButton(Button::$ICONIC_TEXT);
$baru->setClass(" btn-primary ");
$baru->setIcon(" fa fa-stop");
$baru->setAction("tarif_tindakan_operasi.terapkan()");
$uitable->addFooterButton($lama);
$uitable->addFooterButton($restore);
$uitable->addFooterButton($baru);

$ser=new ServiceConsumer($db, "get_jenis_patient");
$ser->execute();
$ctx=$ser->getContent();


$dbtable = new DBTable ( $db, "smis_mjm_kelas" );
$dbtable->setShowAll ( true );
$data = $dbtable->view ( "", "0" );
$select = new SelectAdapter ( "nama", "slug" );
$kelas = $select->getContent ( $data ['data'] );

$jenis=new OptionBuilder();
$jenis->addSingle("Operasi Canggih");
$jenis->addSingle("Operasi Besar");
$jenis->addSingle("Operasi Sedang");
$jenis->addSingle("Operasi Kecil");

$uitable->addModal ( "id", "hidden", "", "" );
$uitable->addModal ( "id_dokter", "hidden", "", "0" );
$uitable->addModal ( "jabatan", "hidden", "", "" );
$uitable->addModal ( "nama", "text", "Tindakan", "" );
$uitable->addModal ( "nama_dokter", "chooser-tarif_tindakan_operasi-dokter_tarif_tindakan_operasi-Dokter", "Dokter", "" );
$uitable->addModal ( "kelas", "select", "Kelas", $kelas );
$uitable->addModal ( "jenis_pasien", "select", "Jenis Pasien", $ctx );
$uitable->addModal ( "jenis_operasi", "select", "Jenis Operasi", $jenis->getContent());
$uitable->addModal ( "bagi_dokter", "money", "Bagi Dokter", "" );
$uitable->addModal ( "bagi_asisten_operasi", "money", "Bagi Asisten Operator", "" );
$uitable->addModal ( "bagi_oomloop", "money", "Bagi Oomloop", "" );

if($is_full){
	$uitable->addModal ("","label","<strong>TARIF SEKARANG</strong>","");
    $uitable->addModal ( "tarif_sewa_alat", "money", "Sewa Alat", "" );
	$uitable->addModal ( "tarif_sewa_ok", "money", "Sewa Kamar", "" );
	$uitable->addModal ( "tarif", "money", " Operator I ", "" );
	$uitable->addModal ( "tarif_operator_dua", "money", " Operator II ", "" );
	$uitable->addModal ( "tarif_asisten_operator_satu", "money", " Asisten Operator I ", "" );
	$uitable->addModal ( "tarif_asisten_operator_dua", "money", " Asisten Operator II ", "" );
	$uitable->addModal ( "tarif_anastesi", "money", " Anastesi ", "" );
	$uitable->addModal ( "tarif_asisten_anastesi", "money", " Asisten Anastesi I ", "" );
	$uitable->addModal ( "tarif_asisten_anastesi_dua", "money", " Asisten Anastesi II ", "" );
	$uitable->addModal ( "tarif_instrument_satu", "money", " Instrument I ", "" );
	$uitable->addModal ( "tarif_instrument_dua", "money", " Instrument II ", "" );
	$uitable->addModal ( "tarif_oomloop_satu", "money", " Oomloop I ", "" );
	$uitable->addModal ( "tarif_oomloop_dua", "money", " Oomloop II ", "" );
	$uitable->addModal ( "tarif_bidan", "money", " Bidan I", "" );
	$uitable->addModal ( "tarif_bidan_dua", "money", " Bidan II", "" );
	$uitable->addModal ( "tarif_perawat", "money", " Perawat ", "" );
	$uitable->addModal ( "ok", "money", "Team OK", "" );	
	$uitable->addModal ( "tarif_recovery_room", "money", " Recovery Room", "" );
	/*
	$uitable->addModal("","label","<strong>RENCANA TARIF BARU</strong>","");
	$uitable->addModal ( "tarif_sewa_alat_baru", "money", "Sewa Alat", "" );
	$uitable->addModal ( "tarif_sewa_ok_baru", "money", " Sewa Kamar", "" );
	$uitable->addModal ( "tarif_baru", "money", " Operator I ", "" );
	$uitable->addModal ( "rencana_tarif_operator_dua", "money", "  Operator II ", "" );
	$uitable->addModal ( "rencana_tarif_asisten_operator_satu", "money", "  Asisten Operator I ", "" );	
	$uitable->addModal ( "rencana_tarif_asisten_operator_dua", "money", "  Asisten Operator II ", "" );	
	$uitable->addModal ( "rencana_tarif_anastesi", "money", "  Anastesi ", "" );	
	$uitable->addModal ( "rencana_tarif_asisten_anastesi", "money", "  Asisten Anastesi I ", "" );	
	$uitable->addModal ( "rencana_tarif_asisten_anastesi_dua", "money", "  Asisten Anastesi II ", "" );	
	$uitable->addModal ( "rencana_tarif_instrument_satu", "money", "  Instrument I ", "" );
	$uitable->addModal ( "rencana_tarif_instrument_dua", "money", "  Instrument II ", "" );	
	$uitable->addModal ( "rencana_tarif_oomloop_satu", "money", "  Oomloop I ", "" );	
	$uitable->addModal ( "rencana_tarif_oomloop_dua", "money", "  Oomloop II ", "" );
	$uitable->addModal ( "rencana_tarif_bidan", "money", "  Bidan I ", "" );	
	$uitable->addModal ( "rencana_tarif_bidan_dua", "money", "  Bidan II ", "" );	
	$uitable->addModal ( "rencana_tarif_perawat", "money", "  Perawat ", "" );		
	$uitable->addModal ( "ok_baru", "money", " Team OK ", "" );
	$uitable->addModal ( "rencana_tarif_recovery_room", "money", "  Recovery Room", "" );	
	
	$uitable->addModal("","label","<strong>TARIF LAMA</strong>","");	
	$uitable->addModal ( "tarif_sewa_alat_lama", "money", "Sewa Alat", "" );
	$uitable->addModal ( "tarif_sewa_ok_lama", "money", "Sewa Kamar", "","y",NULL,true );
	$uitable->addModal ( "tarif_lama", "money", " Operator I", "","y",NULL,true );
	$uitable->addModal ( "tarif_operator_dua_lama", "money", " Operator II", "","y",NULL,true );
	$uitable->addModal ( "tarif_asisten_operator_satu_lama", "money", " Asisten Operator I", "","y",NULL,true );
	$uitable->addModal ( "tarif_asisten_operator_dua_lama", "money", " Asisten Operator II ", "","y",NULL,true );
	$uitable->addModal ( "tarif_anastesi_lama", "money", " Anastesi ", "","y",NULL,true );
	$uitable->addModal ( "tarif_asisten_anastesi_lama", "money", " Asisten Anastesi I ", "","y",NULL,true );
	$uitable->addModal ( "tarif_asisten_anastesi_dua_lama", "money", " Asisten Anastesi II ", "","y",NULL,true );
	$uitable->addModal ( "tarif_instrument_satu_lama", "money", " Instrument I ", "","y",NULL,true );
	$uitable->addModal ( "tarif_instrument_dua_lama", "money", " Instrument II ", "","y",NULL,true );
	$uitable->addModal ( "tarif_oomloop_satu_lama", "money", " Oomloop I ", "","y",NULL,true );
	$uitable->addModal ( "tarif_oomloop_dua_lama", "money", " Oomloop II ", "","y",NULL,true );
	$uitable->addModal ( "tarif_bidan_lama", "money", " Bidan I ", "","y",NULL,true );
	$uitable->addModal ( "tarif_bidan_dua_lama", "money", " Bidan II ", "","y",NULL,true );
	$uitable->addModal ( "tarif_perawat_lama", "money", " Perawat ", "","y",NULL,true );
	$uitable->addModal ( "ok_lama", "money", "Team OK ", "","y",NULL,true );	
	$uitable->addModal ( "tarif_recovery_room_lama", "money", " Recovery Room ", "","y",NULL,true );
	*/

	
}else{
	$uitable->addModal("","label","<strong>TARIF SEKARANG</strong>","");	
	$uitable->addModal ( "tarif_sewa_alat", "money", "Sewa Alat", "" );
	$uitable->addModal ( "tarif_sewa_ok", "money", "Sewa Kamar", "" );
	$uitable->addModal ( "tarif", "money", "Tarif Operator I ", "" );
	$uitable->addModal ( "tarif_operator_dua", "hidden", "", "" );
	$uitable->addModal ( "tarif_asisten_operator_satu", "hidden", "", "" );
	$uitable->addModal ( "tarif_asisten_operator_dua", "hidden", "", "" );
	$uitable->addModal ( "tarif_anastesi", "hidden", "", "" );
	$uitable->addModal ( "tarif_asisten_anastesi_satu", "hidden", "", "" );
	$uitable->addModal ( "tarif_asisten_anastesi_dua", "hidden", "", "" );
	$uitable->addModal ( "tarif_instrument_satu", "hidden", "", "" );
	$uitable->addModal ( "tarif_instrument_dua", "hidden", "", "" );
	$uitable->addModal ( "tarif_oomloop_satu", "hidden", "", "" );
	$uitable->addModal ( "tarif_oomloop_dua", "hidden", "", "" );
	$uitable->addModal ( "tarif_bidan", "hidden", "", "" );
	$uitable->addModal ( "tarif_bidan_dua", "hidden", "", "" );
	$uitable->addModal ( "tarif_perawat", "hidden", "", "" );
	$uitable->addModal ( "ok", "money", "Team OK", "" );
	$uitable->addModal ( "tarif_recovery_room", "hidden", "", "" );
	/*
	$uitable->addModal("","label","<strong>RENCANA TARIF BARU</strong>","");
	$uitable->addModal ( "tarif_sewa_alat_baru", "money", "Sewa Alat", "" );
	$uitable->addModal ( "tarif_sewa_ok_baru", "money", "Sewa Kamar", "" );
	$uitable->addModal ( "tarif_baru", "money", " Operator I", "" );
	$uitable->addModal ( "rencana_tarif_operator_dua", "hidden", "", "" );	
	$uitable->addModal ( "rencana_tarif_asisten_operator_satu", "hidden", "", "" );	
	$uitable->addModal ( "rencana_tarif_asisten_operator_dua", "hidden", "", "" );	
	$uitable->addModal ( "rencana_tarif_anastesi", "hidden", "", "" );	
	$uitable->addModal ( "rencana_tarif_asisten_anastesi_satu", "hidden", "", "" );	
	$uitable->addModal ( "rencana_tarif_asisten_anastesi_dua", "hidden", "", "" );	
	$uitable->addModal ( "rencana_tarif_instrument_satu", "hidden", "", "" );	
	$uitable->addModal ( "rencana_tarif_instrument_dua", "hidden", "", "" );
	$uitable->addModal ( "rencana_tarif_oomloop_satu", "hidden", "", "" );	
	$uitable->addModal ( "rencana_tarif_oomloop_dua", "hidden", "", "" );	
	$uitable->addModal ( "rencana_tarif_bidan", "hidden", "", "" );	
	$uitable->addModal ( "rencana_tarif_bidan_dua", "hidden", "", "" );	
	$uitable->addModal ( "rencana_tarif_perawat", "hidden", "", "" );
	$uitable->addModal ( "ok_baru", "money", "Team OK ", "" );
	$uitable->addModal ( "rencana_tarif_recovery_room", "hidden", "", "" );
	
	$uitable->addModal("","label","<strong>TARIF LAMA</strong>","");	
	$uitable->addModal ( "tarif_sewa_alat_lama", "money", "Sewa Alat", "" );
	$uitable->addModal ( "tarif_sewa_ok_lama", "money", "Sewa Kamar", "","y",NULL,true );	
	$uitable->addModal ( "tarif_lama", "money", "Operator I", "","y",NULL,true );
	$uitable->addModal ( "tarif_operator_dua_lama", "hidden", "", "","y",NULL,true );
	$uitable->addModal ( "tarif_asisten_operator_satu_lama", "hidden", "", "","y",NULL,true );
	$uitable->addModal ( "tarif_asisten_operator_dua_lama", "hidden", "", "","y",NULL,true );
	$uitable->addModal ( "tarif_anastesi_lama", "hidden", "", "","y",NULL,true );
	$uitable->addModal ( "tarif_asisten_anastesi_satu_lama", "hidden", "", "","y",NULL,true );
	$uitable->addModal ( "tarif_asisten_anastesi_dua_lama", "hidden", "", "","y",NULL,true );
	$uitable->addModal ( "tarif_instrument_satu_lama", "hidden", "", "","y",NULL,true );
	$uitable->addModal ( "tarif_instrument_dua_lama", "hidden", "", "","y",NULL,true );	
	$uitable->addModal ( "tarif_oomloop_satu_lama", "hidden", "", "","y",NULL,true );
	$uitable->addModal ( "tarif_oomloop_dua_lama", "hidden", "", "","y",NULL,true );
	$uitable->addModal ( "tarif_bidan_lama", "hidden", "", "","y",NULL,true );
	$uitable->addModal ( "tarif_bidan_dua_lama", "hidden", "", "","y",NULL,true );
	$uitable->addModal ( "tarif_perawat_lama", "hidden", "", "","y",NULL,true );
	$uitable->addModal ( "ok_lama", "money", "Team OK", "","y",NULL,true );
	$uitable->addModal ( "tarif_recovery_room_lama", "hidden", "", "","y",NULL,true );
	*/

}

$modal = $uitable->getModal ();
$modal->setTitle ( "Tarif Operasi" );
$modal->setComponentSize(Modal::$BIG);
$modal->setModalSize(Modal::$HALF_MODEL);
echo $uitable->getHtml ();
echo $modal->getHtml ();
echo addJS ( "base-js/smis-base-loading.js");
echo addJS ( "framework/smis/js/table_action.js" );
echo addJS ( "manajemen/resource/js/manajemen_tarif_action.js",false );

?>
<script type="text/javascript">
var tarif_tindakan_operasi;
var dokter_tarif_tindakan_operasi;
$(document).ready(function(){
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
	var column=new Array('id','nama_dokter','jabatan','nama','id_dokter','kelas',
			"tarif",'tarif_lama','tarif_baru','jenis_pasien',
			"ok",'ok_lama','ok_baru','bagi_dokter',
			"tarif_sewa_ok",'tarif_sewa_ok_lama','tarif_sewa_ok_baru',
            "tarif_sewa_alat",'tarif_sewa_alat_lama','tarif_sewa_alat_baru',
			"jenis_operasi","tarif_operator_dua_lama", "tarif_operator_dua", 
			"rencana_tarif_operator_dua", 	"tarif_asisten_operator_satu_lama", "tarif_asisten_operator_satu", 
			"rencana_tarif_asisten_operator_satu", 	"tarif_asisten_operator_dua_lama", "tarif_asisten_operator_dua", 
			"rencana_tarif_asisten_operator_dua", 	"tarif_anastesi_lama", 
			"tarif_anastesi", "rencana_tarif_anastesi", "tarif_asisten_anastesi_lama", 
			"tarif_asisten_anastesi", 	"rencana_tarif_asisten_anastesi", 	"tarif_asisten_anastesi_dua_lama", 
			"tarif_asisten_anastesi_dua", "rencana_tarif_asisten_anastesi_dua", "tarif_bidan_lama", 
			"tarif_bidan", "rencana_tarif_bidan", 	"tarif_bidan_dua_lama", 
			"tarif_bidan_dua", "rencana_tarif_bidan_dua","tarif_perawat_lama", 
			"tarif_perawat", "rencana_tarif_perawat", "tarif_oomloop_satu_lama", 
			"tarif_oomloop_satu", "rencana_tarif_oomloop_satu", "tarif_oomloop_dua_lama", 
			"tarif_oomloop_dua", "rencana_tarif_oomloop_dua", "tarif_instrument_satu_lama", 
			"tarif_instrument_satu", "rencana_tarif_instrument_satu", "tarif_instrument_dua_lama", 
			"tarif_instrument_dua", "rencana_tarif_instrument_dua", "tarif_recovery_room_lama", 
			"tarif_recovery_room", "rencana_tarif_recovery_room","bagi_asisten_operasi","bagi_oomloop"
			);
	tarif_tindakan_operasi=new ManajemenTarifAction("tarif_tindakan_operasi","manajemen","tarif_tindakan_operasi",column);
	tarif_tindakan_operasi.view();
	dokter_tarif_tindakan_operasi=new TableAction("dokter_tarif_tindakan_operasi","manajemen","tarif_tindakan_operasi",new Array());
	dokter_tarif_tindakan_operasi.setSuperCommand("dokter_tarif_tindakan_operasi");
	dokter_tarif_tindakan_operasi.selected=function(json){
		var nama=json.nama;
		var nip=json.id;		
		var jabatan=json.slug;
		$("#tarif_tindakan_operasi_nama_dokter").val(nama);
		$("#tarif_tindakan_operasi_id_dokter").val(nip);
		$("#tarif_tindakan_operasi_jabatan").val(jabatan);
	};

	tarif_tindakan_operasi.setDuplicateNameView("nama");
	
});
</script>