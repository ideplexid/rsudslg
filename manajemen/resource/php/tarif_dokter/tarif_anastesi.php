<?php
global $db;
require_once ("smis-base/smis-include-service-consumer.php");
require_once "smis-base/smis-include-duplicate.php";
require_once 'manajemen/class/responder/ManajemenTarifResponder.php';

$uitable = new TableSynchronous ( array ('Nama','Jenis','Kelas','Tarif Lama','Tarif Sekarang','Rencana Tarif Baru'), "Tarif Anastesi", NULL, true );
$uitable->setName ( "tarif_anastesi" );
$duplicate=getSettings($db,"manajer-duplicate-tarif-anastesi","0");
$uitable->setLoopDuplicateButtonEnable ($duplicate!="0");
$uitable->setDuplicateButton($duplicate!="0");
$uitable->setSynchronizeButton(false);
$uitable->setReloadButtonEnable(false);
$uitable->setPrintButtonEnable(false);
if (isset ( $_POST ['command'] )) {
	$adapter = new SynchronousViewAdapter ();
	$adapter->add ( "Kelas", "kelas", "unslug" );
	$adapter->add ( "Nama", "nama" );
	$adapter->add ( "Jenis", "jenis" );
	$adapter->add ( "Tarif Sekarang", "tarif", "money Rp." );
	$adapter->add ( "Tarif Lama", "tarif_lama", "money Rp." );
	$adapter->add ( "Rencana Tarif Baru", "tarif_baru", "money Rp." );
	$dbtable = new DBTable ( $db, "smis_mjm_anastesi" );
	$dbtable->setFetchMethode ( DBTable::$ARRAY_FETCH );
	$dbres = new ManajemenTarifResponder ( $dbtable, $uitable, $adapter,$duplicate );
    $dbres->addUpdateColumn("tarif");   
	$data = $dbres->command ( $_POST ['command'] );
	echo json_encode ( $data );
	return;
}

$lama=new Button("", "", "Backup");
$lama->setIsButton(Button::$ICONIC_TEXT);
$lama->setIcon(" fa fa-backward");
$lama->setClass(" btn-primary ");
$lama->setAction("tarif_anastesi.backup()");
$restore=new Button("", "", "Restore");
$restore->setIsButton(Button::$ICONIC_TEXT);
$restore->setClass(" btn-primary ");
$restore->setIcon(" fa fa-forward");
$restore->setAction("tarif_anastesi.restore()");
$baru=new Button("", "", "Terapkan");
$baru->setIsButton(Button::$ICONIC_TEXT);
$baru->setClass(" btn-primary ");
$baru->setIcon(" fa fa-stop");
$baru->setAction("tarif_anastesi.terapkan()");
$uitable->addFooterButton($lama);
$uitable->addFooterButton($restore);
$uitable->addFooterButton($baru);

$dbtable = new DBTable ( $db, "smis_mjm_kelas" );
$dbtable->setShowAll ( true );
$data = $dbtable->view ( "", "0" );
$select = new SelectAdapter ( "nama", "slug" );
$kelas = $select->getContent ( $data ['data'] );

$jenis=new OptionBuilder();
$jenis->addSingle("Regional Anastesi");
$jenis->add("Local Anastesi");
$jenis->add("General Anastesi (TIVA)");
$jenis->add("General Anastesi (Inhalasi)");
$jenis->add("General Anastesi (Intub)");


$uitable->addModal ( "id", "hidden", "", "" );
$uitable->addModal ( "kelas", "select", "Kelas", $kelas );
$uitable->addModal ( "nama", "text", "Nama", "" );
$uitable->addModal ( "jenis", "select", "Jenis", $jenis->getContent() );
$uitable->addModal ( "tarif_lama", "money", "Tarif Lama", "","y",NULL,true );
$uitable->addModal ( "tarif", "money", "Tarif Sekarang", "" );
$uitable->addModal ( "tarif_baru", "money", "Rencana Tarif Baru", "" );
$modal = $uitable->getModal ();
$modal->setTitle ( "Tarif Anastesi" );

echo $uitable->getHtml ();
echo $modal->getHtml ();
echo addJS ( "base-js/smis-base-loading.js");
echo addJS ( "framework/smis/js/table_action.js" );
echo addJS ( "manajemen/resource/js/manajemen_tarif_action.js",false );
?>
<script type="text/javascript">
var tarif_anastesi;
$(document).ready(function(){
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
	var column=new Array('id','kelas',"tarif",'nama','tarif_lama','tarif_baru','jenis');
	tarif_anastesi=new ManajemenTarifAction("tarif_anastesi","manajemen","tarif_anastesi",column);	
	tarif_anastesi.view();	
    tarif_anastesi.setDuplicateNameView("nama");
});
</script>
