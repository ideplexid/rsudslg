<?php
global $db;
require_once ("smis-base/smis-include-service-consumer.php");
require_once 'smis-libs-hrd/EmployeeResponder.php';
require_once "smis-base/smis-include-duplicate.php";
require_once 'manajemen/class/responder/ManajemenTarifResponder.php';

$header=array ('Nama','Jabatan',"NIP");
$dktable = new Table ( $header, "", NULL, true );
$dktable->setName ( "dokter_konsul" );
$dktable->setModel ( Table::$SELECT );
$dkadapter = new SimpleAdapter ();
$dkadapter->add ( "Jabatan", "nama_jabatan" );
$dkadapter->add ( "Nama", "nama" );
$dkadapter->add ( "NIP", "nip" );
$dokter = new EmployeeResponder ( $db, $dktable, $dkadapter, "dokter" );

$super = new SuperCommand ();
$super->addResponder ( "dokter_konsul", $dokter );
$init = $super->initialize ();
if ($init != null) {
	echo $init;
	return;
}
$duplicate=getSettings($db,"manajer-duplicate-tarif-konsul","0");
$header=array("Nama",'Dokter','Kelas','Tarif Sekarang');
$uitable = new TableSynchronous ( $header, "Tarif Konsul", NULL, true );
$uitable->setName ( "tarif_konsul" );
$uitable->setLoopDuplicateButtonEnable ($duplicate!="0");
$uitable->setDuplicateButton($duplicate!="0");
$uitable->setSynchronizeButton(false);
$uitable->setReloadButtonEnable(false);
$uitable->setPrintButtonEnable(false);

if (isset ( $_POST ['command'] )) {
	$adapter = new SynchronousViewAdapter ();
	$adapter->add ( "Dokter", "nama_dokter" );
	$adapter->add ( "Nama", "nama_konsul" );
	$adapter->add ( "Kelas", "kelas", "unslug" );
	$adapter->add ( "Tarif Sekarang", "tarif", "money Rp." );
	$dbtable = new DBTable ( $db, "smis_mjm_konsul" );
	$dbtable->setFetchMethode ( DBTable::$ARRAY_FETCH );
	$dbtable->setOrder(" nama_dokter ASC ");
	$dbres = new ManajemenTarifResponder ( $dbtable, $uitable, $adapter,$duplicate );
    $dbres->addUpdateColumn("tarif");   
	$data = $dbres->command ( $_POST ['command'] );
	echo json_encode ( $data );
	return;
}
/*
$lama=new Button("", "", "Backup");
$lama->setIsButton(Button::$ICONIC_TEXT);
$lama->setIcon(" fa fa-backward");
$lama->setClass(" btn-primary ");
$lama->setAction("tarif_konsul.backup()");
$restore=new Button("", "", "Restore");
$restore->setIsButton(Button::$ICONIC_TEXT);
$restore->setClass(" btn-primary ");
$restore->setIcon(" fa fa-forward");
$restore->setAction("tarif_konsul.restore()");
$baru=new Button("", "", "Terapkan");
$baru->setIsButton(Button::$ICONIC_TEXT);
$baru->setClass(" btn-primary ");
$baru->setIcon(" fa fa-stop");
$baru->setAction("tarif_konsul.terapkan()");
$uitable->addFooterButton($lama);
$uitable->addFooterButton($restore);
$uitable->addFooterButton($baru);*/

$dbtable = new DBTable ( $db, "smis_mjm_kelas" );
$dbtable->setShowAll ( true );
$data = $dbtable->view ( "", "0" );
$select = new SelectAdapter ( "nama", "slug" );
$kelas = $select->getContent ( $data ['data'] );


$nominal = getSettings($db,"manajer-konsul-model","1")=="1";
$persen  = !$nominal;


$uitable->addModal ( "id", "hidden", "", "" );
$uitable->addModal ( "id_dokter", "hidden", "", "" );
$uitable->addModal ( "jabatan", "hidden", "", "" );
$uitable->addModal ( "nama_konsul", "text", "Konsul", "" );
$uitable->addModal ( "nama_dokter", "chooser-tarif_konsul-dokter_konsul", "Dokter", "" );
$uitable->addModal ( "kelas", "select", "Kelas", $kelas );
//$uitable->addModal ( "tarif_lama", "money", "Tarif Lama", "","y",NULL,true );
$uitable->addModal ( "tarif", "money", "Tarif Sekarang", "" );
//$uitable->addModal ( "tarif_baru", "money", "Rencana Tarif Baru", "" );

$uitable->addModal ( "jaspel", "money", "Jasa Pelayanan", "","y","null",$persen );
$uitable->addModal ( "jaspel_persen", "text", "Jasa Pelayanan (%)", "","y","null",$nominal );
$uitable->addModal ( "rs", "money", "RS & BHP", "","y","null",$persen );
$uitable->addModal ( "rs_persen", "text", "RS & BHP (%)", "","y","null",$nominal );
    
$modal = $uitable->getModal ();
$modal->setTitle ( "Tarif Konsul" );
$modal->setComponentSize(Modal::$MEDIUM);

echo $uitable->getHtml ();
echo $modal->getHtml ();
echo addJS ( "base-js/smis-base-loading.js");
echo addJS ( "framework/smis/js/table_action.js" );
echo addJS ( "manajemen/resource/js/manajemen_tarif_action.js",false );
echo addJS ( "manajemen/resource/js/tarif_konsul.js",false );