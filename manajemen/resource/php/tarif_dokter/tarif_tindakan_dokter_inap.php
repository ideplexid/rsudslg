<?php
global $db;
require_once ("smis-base/smis-include-service-consumer.php");
require_once 'smis-libs-hrd/EmployeeResponder.php';
require_once "smis-base/smis-include-duplicate.php";
require_once 'manajemen/class/responder/ManajemenTarifResponder.php';

$dktable = new Table ( array ('Nama','Jabatan',"NIP" ), "", NULL, true );
$dktable->setName ( "dokter_tarif_tindakan_dokter_inap" );
$dktable->setModel ( Table::$SELECT );
$dkadapter = new SimpleAdapter ();
$dkadapter->add ( "Jabatan", "nama_jabatan" );
$dkadapter->add ( "Nama", "nama" );
$dkadapter->add ( "NIP", "nip" );
$dokter = new EmployeeResponder ( $db, $dktable, $dkadapter, "dokter" );

$super = new SuperCommand ();
$super->addResponder ( "dokter_tarif_tindakan_dokter_inap", $dokter );
$init = $super->initialize ();
if ($init != null) {
	echo $init;
	return;
}
$duplicate=getSettings($db,"manajer-duplicate-tarif-tindakan-dokter","0");
$uitable = new TableSynchronous ( array ('No.','Nama','Dokter','Kelas','Pasien','Tarif Sekarang' ), "Tarif Tindakan Dokter", NULL, true );
$uitable->setName ( "tarif_tindakan_dokter_inap" );
$uitable->setLoopDuplicateButtonEnable ($duplicate!="0");
$uitable->setDuplicateButton($duplicate!="0");
$uitable->setSynchronizeButton(false);
$uitable->setReloadButtonEnable(false);
$uitable->setPrintButtonEnable(false);

if (isset ( $_POST ['command'] )) {
	$adapter = new SynchronousViewAdapter ();
	$adapter->setUseNumber(true, "No.","back.");
	$adapter->add ( "Dokter", "nama_dokter" );
	$adapter->add ( "Nama", "nama" );
	$adapter->add ( "Kelas", "kelas", "unslug" );
	$adapter->add ( "Pasien", "jenis_pasien", "unslug" );
	$adapter->add ( "Tarif Sekarang", "tarif", "money Rp." );
//	$adapter->add ( "Tarif Lama", "tarif_lama", "money Rp." );
//	$adapter->add ( "Rencana Tarif Baru", "tarif_baru", "money Rp." );
	$dbtable = new DBTable ( $db, "smis_mjm_tindakan_dokter" );
	$dbtable->setFetchMethode ( DBTable::$ARRAY_FETCH );
    $dbtable->setOrder(" nama_dokter ASC ");
    $dbtable->addCustomKriteria(" uri "," = '1' ");

    $dbres = new ManajemenTarifResponder ( $dbtable, $uitable, $adapter,$duplicate );
    if($dbres->isSave()){
        $dbres->addColumnFixValue("uri",1);
    }

    $dbres->addUpdateColumn("tarif");   
    $dbres->addUpdateColumn("perawat");   
	$data = $dbres->command ( $_POST ['command'] );
	echo json_encode ( $data );
	return;
}

/*
$lama=new Button("", "", "Backup");
$lama->setIsButton(Button::$ICONIC_TEXT);
$lama->setIcon(" fa fa-backward");
$lama->setClass(" btn-primary ");
$lama->setAction("tarif_tindakan_dokter_inap.backup()");
$restore=new Button("", "", "Restore");
$restore->setIsButton(Button::$ICONIC_TEXT);
$restore->setClass(" btn-primary ");
$restore->setIcon(" fa fa-forward");
$restore->setAction("tarif_tindakan_dokter_inap.restore()");
$baru=new Button("", "", "Terapkan");
$baru->setIsButton(Button::$ICONIC_TEXT);
$baru->setClass(" btn-primary ");
$baru->setIcon(" fa fa-stop");
$baru->setAction("tarif_tindakan_dokter_inap.terapkan()");
$uitable->addFooterButton($lama);
$uitable->addFooterButton($restore);
$uitable->addFooterButton($baru);*/

$ser=new ServiceConsumer($db, "get_jenis_patient");
$ser->execute();
$ctx=$ser->getContent();


$dbtable = new DBTable ( $db, "smis_mjm_kelas" );
$dbtable->setShowAll ( true );
$data = $dbtable->view ( "", "0" );
$select = new SelectAdapter ( "nama", "slug" );
$kelas = $select->getContent ( $data ['data'] );

$uitable->addModal ( "id", "hidden", "", "" );
$uitable->addModal ( "id_dokter", "hidden", "", "" );
$uitable->addModal ( "jabatan", "hidden", "", "" );
$uitable->addModal ( "nama", "text", "Tindakan", "" );
$uitable->addModal ( "nama_dokter", "chooser-tarif_tindakan_dokter_inap-dokter_tarif_tindakan_dokter_inap", "Dokter", "" );
$uitable->addModal ( "kelas", "select", "Kelas", $kelas );
$uitable->addModal ( "jenis_pasien", "select", "Jenis Pasien", $ctx );
//$uitable->addModal ( "tarif_lama", "money", "Tarif Dokter Lama", "","y",NULL,true );
$uitable->addModal ( "tarif", "money", "Tarif Dokter Sekarang", "" );
//$uitable->addModal ( "tarif_baru", "money", "Rencana Tarif Dokter Baru", "" );
//$uitable->addModal ( "perawat_lama", "money", "Tarif Perawat Lama", "","y",NULL,true );
$uitable->addModal ( "perawat", "money", "Tarif Perawat Sekarang", "" );
//$uitable->addModal ( "perawat_baru", "money", "Rencana Tarif Perawat Baru", "" );


$nominal = getSettings($db,"manajer-tindakan-dokter-model","1")=="1";
$persen  = !$nominal;

if(getSettings($db, "manajer-tindakan-dokter-show-sewa-alat", "0")=="1"){
	$uitable->addModal ( "sewa_alat", "money", "Sewa Alat", "","y","null",$persen );
	$uitable->addModal ( "sewa_alat_persen", "text", "Sewa Alat (%)", "","y","null",$nominal );
}
if(getSettings($db, "manajer-tindakan-dokter-show-rs", "0")=="1"){
	$uitable->addModal ( "rs", "money", "RS & BHP", "","y","null",$persen );
	$uitable->addModal ( "rs_persen", "text", "RS & BHP (%)", "","y","null",$nominal );
}
if(getSettings($db, "manajer-tindakan-dokter-show-jasa-pelayanan", "0")=="1"){
    $uitable->addModal ( "jaspel", "money", "Jasa Pelayanan", "","y","null",$persen );
    $uitable->addModal ( "jaspel_persen", "text", "Jasa Pelayanan (%)", "","y","null",$nominal );
}
/*
if(getSettings($db, "manajer-tindakan-dokter-show-bhp", "0")=="1"){
	$uitable->addModal ( "bhp", "money", "Bahan Habis Pakai", "","y","null",$persen );
	$uitable->addModal ( "bhp_persen", "text", "Bahan Habis Pakai (%)", "","y","null",$nominal );
}*/


    
if(getSettings($db, "manajer-tindakan-dokter-show-jasa-lain-lain", "0")=="1"){
	$uitable->addModal ( "lain_lain", "money", "Lain-Lain", "","y","null",$persen );
	$uitable->addModal ( "lain_lain_persen", "text", "Lain-Lain (%)", "","y","null",$nominal );
}



if(getSettings($db, "manajer-tindakan-dokter-show-operator", "0")=="1"){
    $uitable->addModal ( "operator", "money", "Operator", "","y","null",$persen );
    $uitable->addModal ( "operator_persen", "text", "Operator (%)", "","y","null",$nominal );
}

if(getSettings($db, "manajer-tindakan-dokter-show-asisten", "0")=="1"){
	$uitable->addModal ( "asisten", "money", "Asisten", "","y","null",$persen );
	$uitable->addModal ( "asisten_persen", "text", "Asisten (%)", "","y","null",$nominal );
}

$modal = $uitable->getModal ();
$modal->setTitle ( "Tarif Tindakan Dokter" );
$modal->setComponentSize(Modal::$MEDIUM);
echo $uitable->getHtml ();
echo $modal->getHtml ();
echo addJS ( "base-js/smis-base-loading.js");
echo addJS ( "framework/smis/js/table_action.js" );
echo addJS ( "manajemen/resource/js/manajemen_tarif_action.js",false );
echo addJS ( "manajemen/resource/js/tarif_tindakan_dokter_inap.js",false );
