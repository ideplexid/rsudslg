<?php 
require_once "smis-base/smis-include-duplicate.php";
global $db;

if(isset($_POST['command'])){
	if($_POST['command']=="list"){
		$dbtable=new DBTable($db, "smis_mjm_keperawatan");
		$dbtable->addCustomKriteria("kelas", "='".$_POST['basis']."'");
		$dbtable->setShowAll(true);
		$total=$dbtable->count("");
		$response=new ResponsePackage();
		$response->setStatus(ResponsePackage::$STATUS_OK);
		$response->setContent($total);
		echo json_encode($response->getPackage());
	}else if($_POST['command']=="derivatif_keperawatan"){
		$dbtable=new DBTable($db, "smis_mjm_keperawatan");
		$dbtable->addCustomKriteria("kelas", "='".$_POST['basis']."'");
		$dbtable->setMaximum(1);
		$query=$dbtable->getQueryView("", $_POST['current']);
		$row=$db->get_row($query['query']);

		$harga_awal=0;
		$harga_akhir=-1;
		if(strpos($_POST['factor'], "\$")!==false){
			$factor=str_replace("\$",$row->tarif,$_POST['factor']);
			try{
				$formula=$factor;
				eval( '$harga= (' . $factor. ');' );
				$harga_akhir=$harga;
			}catch(Exception $e){
				$harga_akhir=-1;
				$formula="-1";
			}
		}else{
			$factor=floatval($_POST['factor']);
			if(is_float($factor) && $factor>0){
				$harga_akhir=$row->tarif*$factor;
				$formula=$row->tarif." * ".$factor;
			}else{
				$harga_akhir=-1;
				$formula="Error";
			}
				
			
		}

		$id['kelas']=$_POST['turunan'];
		$id['nama']=$row->nama;
		$insert['nama']=$row->nama;
		$insert['kelas']=$_POST['turunan'];
		$insert['tarif']=$harga_akhir;
        $insert['duplicate']=0;
        $insert['time_updated']=date("Y-m-d H:i:s");
        $insert['origin_updated']=getSettings($db,"smis_autonomous_id","");
        
        
		$insert['prop']="";

		$status="<span class='label label-inverse'>Fail</span>";
		if($harga_akhir==-1 || $harga_akhir==0){
			$status="<span class='label label-important'>Error Factor</span>";
		}else if($dbtable->is_exist($id)){
			$awal=$dbtable->select($id);
			$harga_awal=$awal->tarif;
			if($awal->tarif!=$insert['tarif']){
                $autonom=getSettings($db,"smis_autonomous_id","");
                $insert['origin_updated']=$autonom;
                $insert['time_updated']=date("Y-m-d H:i:s");
                $insert['autonomous']="[".$autonom."]";
                
                $result=$dbtable->update($insert, $id);
				if($result) {
					$status="<span class='label label-success'>Updated</span>";
				}else{
					$status="<span class='label label-inverse'>Fail Update</span>";
				}
			}else{
				$status="<span class='label'>No Need Update</span>";
			}
		}else{
			if($dbtable->is_exist($id,true)){
                $autonom=getSettings($db,"smis_autonomous_id","");
                $insert['origin_updated']=$autonom;
                $insert['time_updated']=date("Y-m-d H:i:s");
                $insert['autonomous']="[".$autonom."]";
                
				$result=$dbtable->update($insert,$id);
                if($result) {
					$status="<span class='label label-success'>Restored</span>";
				}else{
					$status="<span class='label label-warning'>Fail Restored</span>";
				}
			}else{
                $autonom=getSettings($db,"smis_autonomous_id","");
                $insert['origin']=$autonom;
                $insert['time_updated']=date("Y-m-d H:i:s");
                $insert['autonomous']="[".$autonom."]";
                
                $data['origin']=getSettings($db,"smis_autonomous_id","");
                $result=$dbtable->insert($insert,$id);
                $id['id']=$dbtable->get_inserted_id();
                
                /*update the origin id*/
                $update['origin_id']=$id['id'];
                $result=$dbtable->update($update,$id);
                
                $reup=array();
                $reup['origin_id']=$id['id'];
                $dbtable->update($reup,$id);
                if($result) {
					$status="<span class='label label-success'>Inserted</span>";
				}else{
					$status="<span class='label label-warning'>Fail Insert</span>";
				}
			}
		}




		$json="
		<tr>
		<td>".$row->nama."</td>
		<td>".ArrayAdapter::format("unslug", $_POST['turunan'])."</td>
		<td>".ArrayAdapter::format("money Rp.", $row->tarif)."</td>
		<td>".$formula."</td>
		<td>".ArrayAdapter::format("money Rp.",$harga_awal)."</td>
		<td>".ArrayAdapter::format("money Rp.", $harga_akhir)."</td>
		<td>".$status."</td>
		</tr>
		";

		$response=new ResponsePackage();
		$response->setStatus(ResponsePackage::$STATUS_OK);
		$response->setContent($json);
		echo json_encode($response->getPackage());
	}
	return;
}


$title="Keterangan";
$content="<ul>";
$content.="<li><strong>Basis</strong> adalah kelas harga awal yang akan dijadikan basis awal untuk perhitungan harga baru</li>";
$content.="<li><strong>Turunan</strong> adalah kelas harga yang akan di ubah berdasarkan basis dan faktor</li>";
$content.="<li><strong>Faktor</strong> adalah fungsi perubahan yang akan digunakan untuk mengubah harga</li>";
$content.="</ul>";
$content.="<h5>Penggunaan Faktor</h5>";
$content.="<ul>";
$content.="<li>1.5 berarti harga basis akan dikali dengan 1.5, (harga baru naik 50% dari harga basis)</li>";
$content.="<li>0.5 berarti harga basis akan dikali dengan 0.5 (harga baru turun 50% dari harga basis)</li>";
$content.="<li>100 berarti harga basis akan dikali dengan 100 (harga baru naik 100 kali lipat dari harga basis)</li>";
$content.="<li> $ digunakan sebagai penanda dalam formula factor </li>";
$content.="<li> $+10000 berarti harga baru adalah harga basis ditambah Rp.10.000,00</li>";
$content.="<li> $*1.23+5000 berarti harga baru adalah harga basis dikali 1.23 kemudian ditambah Rp.5.000,00</li>";
$content.="<li> input salah 123p akan dianggap sebagai 123, sehingga harga baru adalah harga basis dikali 123</li>";
$content.="<li> input salah g90 akan dianggap sebagai error, sehingga harga tidak di update</li>";
$content.="</ul>";

$alert=new Alert("help_derivatif_keperawatan", $title, $content);
$alert->setModel(Alert::$INVERSE);

$kelas=new DBTable($db, "smis_mjm_kelas");
$kelas->setShowAll(true);
$data=$kelas->view("","0");
$dlist=$data['data'];
$adapter=new SelectAdapter("nama", "slug");
$content=$adapter->getContent($dlist);

$basis=new Select("derivatif_keperawatan_basis", "basis", $content);
$turunan=new Select("derivatif_keperawatan_turunan", "turunan", $content);
$factor=new Text("derivatif_keperawatan_factor", "faktor", "1");
$button=new Button("aksi", "aksi", "Run");
$button->setIsButton(Button::$ICONIC);
$button->setAction("derivatif_keperawatan_running();");
$button->setIcon(" fa fa-circle-o-notch");
$button->setClass("btn-primary");

$form=new Form("", "", "derivatif_keperawatan");
$form->addElement("Basis", $basis);
$form->addElement("Turunan", $turunan);
$form->addElement("Faktor", $factor);
$form->addElement("", $button);

$close=new Button("", "", "Batal");
$close	->addClass("btn-primary")
->setIsButton(Button::$ICONIC_TEXT)
->setIcon("fa fa-close")
->setAction("derivatif_keperawatan_stop()");


$header=array("Name","Kelas","Basis","Formula","Sebelum","Setelah","Status");
$table=new Table($header,"",NULL,false);
$table->setName("derivatif_keperawatan");
$table->setFooterVisible(false);


$load=new LoadingBar("derivatif_keperawatan_bar", "");
$modal=new Modal("derivatif_keperawatan_modal", "", "Derivating in Progress");
$modal->addHTML($load->getHtml(),"after")
	  ->addFooter($close)
	  ->setTitle("Derivating in Progress");


echo addJS ( "base-js/smis-base-loading.js");
echo $form->getHtml();
echo $table->getHtml();
echo $modal->getHtml();
echo $alert->getHTML();


?>

<script type="text/javascript">
var data_derivatif_keperawatan={
		prototype_implement:"",
		prototype_name:"",
		prototype_slug:"",
		page:"manajemen",
		action:"derivatif_keperawatan"	
	};
var derivatif_keperawatan_is_running=false;

function derivatif_keperawatan_stop(){
	derivatif_keperawatan_is_running=false;	
	$("#derivatif_keperawatan_modal").modal("hide");
}

function derivatif_keperawatan_loop(total,current){
	var persen=current*100/total
	$("#derivatif_keperawatan_bar").sload("true"," Loading... [ "+ current+" / "+total+" ]",persen);
	if(current>=total || !derivatif_keperawatan_is_running){
		$("#derivatif_keperawatan_modal").modal("hide");
		derivatif_keperawatan_is_running=false;
	}else{
		data_derivatif_keperawatan['command']="derivatif_keperawatan";
		data_derivatif_keperawatan['current']=current;
		data_derivatif_keperawatan['basis']=$("#derivatif_keperawatan_basis").val();
		data_derivatif_keperawatan['turunan']=$("#derivatif_keperawatan_turunan").val();
		data_derivatif_keperawatan['factor']=$("#derivatif_keperawatan_factor").val();
		$.post("",data_derivatif_keperawatan,function(res){
			var json=getContent(res);
			setTimeout(derivatif_keperawatan_loop(total,++current),200);
			$("#derivatif_keperawatan_list").append(json);
		});
	}
}

function derivatif_keperawatan_running(){
	derivatif_keperawatan_is_running=true;
	data_derivatif_keperawatan['command']="list";
	data_derivatif_keperawatan['basis']=$("#derivatif_keperawatan_basis").val();
	showLoading();
	$.post("",data_derivatif_keperawatan,function(res){
		var total=getContent(res);
		var tt=Number(total);
		dismissLoading();
		if(tt>0){
			$("#derivatif_keperawatan_modal").modal("show");
			$("#derivatif_keperawatan_list").html("");
			derivatif_keperawatan_loop(tt,0);
		}
	});
}

</script>
