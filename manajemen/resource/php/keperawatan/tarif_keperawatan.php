<?php
global $db;
require_once ("smis-base/smis-include-service-consumer.php");
require_once "smis-base/smis-include-duplicate.php";
require_once 'smis-libs-hrd/EmployeeResponder.php';
require_once "smis-base/smis-include-duplicate.php";
require_once 'manajemen/class/responder/ManajemenTarifResponder.php';

$duplicate=getSettings($db,"manajer-duplicate-tarif-keperawatan","0");
$uitable = new TableSynchronous ( array ('Nama','Kelas','Tarif Sekarang'), "Tarif Keperawatan Rawat jalan", NULL, true );
$uitable->setName ( "tarif_keperawatan" );
$uitable->setLoopDuplicateButtonEnable ($duplicate!="0");
$uitable->setDuplicateButton($duplicate!="0");
$uitable->setSynchronizeButton(false);
$uitable->setReloadButtonEnable(false);
$uitable->setPrintButtonEnable(false);

if (isset ( $_POST ['command'] )) {
	$adapter = new SynchronousViewAdapter ();
	$adapter->add ( "Nama", "nama" );
	$adapter->add ( "Kelas", "kelas", "unslug" );
	$adapter->add ( "Tarif Sekarang", "tarif", "money Rp." );
//	$adapter->add ( "Tarif Lama", "tarif_lama", "money Rp." );
//	$adapter->add ( "Rencana Tarif Baru", "tarif_baru", "money Rp." );
	$dbtable = new DBTable ( $db, "smis_mjm_keperawatan" );
	$dbtable->setFetchMethode ( DBTable::$ARRAY_FETCH );
	$dbtable->addCustomKriteria(" uri "," = '0' ");
	$dbres = new ManajemenTarifResponder ( $dbtable, $uitable, $adapter,$duplicate );
    $dbres->addUpdateColumn("tarif");   
	$data = $dbres->command ( $_POST ['command'] );
	echo json_encode ( $data );
	return;
}

//$lama=new Button("", "", "Backup");
//$lama->setIsButton(Button::$ICONIC_TEXT);
//$lama->setIcon(" fa fa-backward");
//$lama->setClass(" btn-primary ");
//$lama->setAction("tarif_keperawatan.backup()");
//$restore=new Button("", "", "Restore");
//$restore->setIsButton(Button::$ICONIC_TEXT);
//$restore->setClass(" btn-primary ");
//$restore->setIcon(" fa fa-forward");
//$restore->setAction("tarif_keperawatan.restore()");
//$baru=new Button("", "", "Terapkan");
//$baru->setIsButton(Button::$ICONIC_TEXT);
//$baru->setClass(" btn-primary ");
//$baru->setIcon(" fa fa-stop");
//$baru->setAction("tarif_keperawatan.terapkan()");
//$uitable->addFooterButton($lama);
//$uitable->addFooterButton($restore);
//$uitable->addFooterButton($baru);

$dbtable = new DBTable ( $db, "smis_mjm_kelas" );
$dbtable->setShowAll ( true );
$data = $dbtable->view ( "", "0" );
$select = new SelectAdapter ( "nama", "slug" );
$kelas = $select->getContent ( $data ['data'] );

$uitable->addModal ( "id", "hidden", "", "" );
$uitable->addModal ( "nama", "text", "Nama", "" );
$uitable->addModal ( "kelas", "select", "Kelas", $kelas );
//$uitable->addModal ( "tarif_lama", "money", "Tarif Lama", "","y",NULL,true );
$uitable->addModal ( "tarif", "money", "Tarif Sekarang", "" );
//$uitable->addModal ( "tarif_baru", "money", "Rencana Tarif Baru", "" );

$nominal = getSettings($db,"manajer-tindakan-perawat-model","1")=="1";
$persen  = !$nominal;

if(getSettings($db, "manajer-tindakan-perawat-show-sewa-alat", "0")=="1"){
	$uitable->addModal ( "sewa_alat", "money", "Sewa Alat", "","y","null",$persen );
	$uitable->addModal ( "sewa_alat_persen", "text", "Sewa Alat %", "","y","null",$nominal );
}
/*
if(getSettings($db, "manajer-tindakan-perawat-show-bhp", "0")=="1"){
	$uitable->addModal ( "bhp", "money", "Bahan Habis Pakai", "","y","null",$persen );
	$uitable->addModal ( "bhp_persen", "text", "Bahan Habis Pakai %", "","y","null",$nominal );
}*/
if(getSettings($db, "manajer-tindakan-perawat-show-jasa-pelayanan", "0")=="1"){
	$uitable->addModal ( "jaspel", "money", "Jasa Pelayanan", "","y","null",$persen );
	$uitable->addModal ( "jaspel_persen", "text", "Jasa Pelayanan %", "","y","null",$nominal );
}
if(getSettings($db, "manajer-tindakan-perawat-show-jasa-lain-lain", "0")=="1"){
	$uitable->addModal ( "lain_lain", "money", "Lain-Lain", "" ,"y","null",$persen);
	$uitable->addModal ( "lain_lain_persen", "text", "Lain-Lain %", "","y","null",$nominal );
}
if(getSettings($db, "manajer-tindakan-perawat-show-jasa-penunjang", "0")=="1"){
    $uitable->addModal ( "penunjang", "money", "Penunjang", "" ,"y","null",$persen);
    $uitable->addModal ( "penunjang_persen", "text", "Penunjang %", "","y","null",$nominal );
}
if(getSettings($db, "manajer-tindakan-perawat-show-rs", "0")=="1"){
    $uitable->addModal ( "rs", "money", "Bagi RS & BHP", "" ,"y","null",$persen);
    $uitable->addModal ( "rs_persen", "text", "Bagi RS & BHP %", "","y","null",$nominal );
}
if(getSettings($db, "manajer-tindakan-perawat-show-operator", "0")=="1"){
    $uitable->addModal ( "operator", "money", "Operator", "" ,"y","null",$persen);
    $uitable->addModal ( "operator_persen", "text", "Operator %", "" ,"y","null",$nominal);
}
if(getSettings($db, "manajer-tindakan-perawat-show-asisten", "0")=="1"){
    $uitable->addModal ( "asisten", "money", "Asisten", "" ,"y","null",$persen);
    $uitable->addModal ( "asisten_persen", "text", "Asisten %", "" ,"y","null",$nominal);
}
    
$modal = $uitable->getModal ();
$modal->setTitle ( "Tarif Keperawatan" );
//$modal->setModalSize(Modal::$HALF_MODEL);
$modal->setComponentSize(Modal::$MEDIUM);
echo $uitable->getHtml ();
echo $modal->getHtml ();
echo addJS ( "base-js/smis-base-loading.js");
echo addJS ( "framework/smis/js/table_action.js" );
echo addJS ( "manajemen/resource/js/manajemen_tarif_action.js",false );
echo addJS ( "manajemen/resource/js/tarif_keperawatan.js",false );
