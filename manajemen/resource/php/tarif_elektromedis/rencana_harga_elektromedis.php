<?php

require_once 'smis-framework/smis/api/SettingsBuilder.php';
require_once 'smis-base/smis-include-service-consumer.php';

global $db;
$service = new ServiceConsumer ( $db, "get_list_elektromedis" );
$service->setCached(true,"get_list_elektromedis");
$service->execute ();
$array = $service->getContent ();

$slug = "rencana_smis_emd_harga";
$settings = new SettingsBuilder ( $db, "rencana", "manajemen", "rencana_harga_elektromedis", "" );
$settings->setPrefix("rencana_smis_emd_harga");
$settings->setSingle ( true, $slug );
$settings->setOneVariable(true, $slug);

$dbtable = new DBTable ( $db, "smis_mjm_kelas" );
$dbtable->setShowAll ( true );
$data = $dbtable->view ( "", "0" );
$select = new SelectAdapter ( "nama", "slug" );
$kelas = $select->getContent ( $data ['data'] );

foreach ( $kelas as $element => $value ) {
	$k = $value ['name'];
	$v = $value ['value'];
	$settings->addTabs ( "rencana_smis_emd_harga".$v, $k," fa fa-calendar-o" );
    if($settings->isGroup("rencana_smis_emd_harga".$v)){
        foreach ( $array as $ak => $av ) {
            $setitem = new SettingsItem ( $db, $v . "_" . $ak, $av, "0", "money", "", true, $slug );
            $settings->addItem ( "rencana_smis_emd_harga".$v, $setitem );
        }
    }
}
$settings->setPartialLoad(true);
$settings->init ();

?>