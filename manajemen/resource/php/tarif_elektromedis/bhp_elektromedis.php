<?php

require_once 'smis-framework/smis/api/SettingsBuilder.php';
require_once 'smis-base/smis-include-service-consumer.php';

global $db;

$service = new ServiceConsumer ( $db, "get_list_elektromedis" );
$service->setCached(true,"get_list_elektromedis");
$service->execute ();
$array = $service->getContent ();

$slug = "bhp_smis_emd_harga";
$settings = new SettingsBuilder ( $db, "bhp", "manajemen", "bhp_elektromedis", "" );
$settings->setPrefix("bhp_smis_emd_harga");
$settings->setSingle ( true, $slug );
$settings->setOneVariable(true, $slug);

?>