<?php
require_once 'smis-framework/smis/api/SettingsBuilder.php';
require_once 'smis-base/smis-include-service-consumer.php';
global $db;
$service = new ServiceConsumer ( $db, "get_list_patology" );
$service->execute ();
$array = $service->getContent ();
$slug = "rs_smis_pat_harga";
$settings = new SettingsBuilder ( $db, "harga", "manajemen", "rs_patology", "" );
$settings->setSingle ( true, $slug );
$settings->setPrefix("rs_harga_patology");
$settings->setOneVariable ( true, $slug);

$dbtable = new DBTable ( $db, "smis_mjm_kelas" );
$dbtable->setShowAll ( true );
$data = $dbtable->view ( "", "0" );
$select = new SelectAdapter ( "nama", "slug" );
$kelas = $select->getContent ( $data ['data'] );

foreach ( $kelas as $element => $value ) {
	$k = $value ['name'];
	$v = $value ['value'];
	$settings->addTabs ( "rs_harga_patology".$v, $k,"fa fa-tty" );
    if($settings->isGroup("rs_harga_patology".$v)){
        foreach ( $array as $ak => $av ) {
            $setitem = new SettingsItem ( $db, $v . "_" . $ak, $av, "0", "money", "", true, $slug );
            $settings->addItem ( "rs_harga_patology".$v, $setitem );
        }
    }
	
}
$settings->setPartialLoad(true);
$settings->init ();
?>