<?php 
    require_once "smis-base/smis-include-service-consumer.php";
    require_once "smis-libs-class/MasterTemplate.php";

    $master = new MasterTemplate($db,"smis_mjm_bbm","manajemen","bbm");
    $master ->addJSColumn("id",true)
            ->addJSColumn("nama",false);
    $master ->getDBtable()->setOrder("nama ASC",true);
    $master ->getUItable()
            ->setPrintButtonEnable(false)
            ->setReloadButtonEnable(false)
            ->setHeader(array("No.","Nama"));
    $master ->getUItable()
            ->addModal ( "id", "hidden", "", "" )
            ->addModal ( "nama", "text", "Nama", "" );
    $master ->getAdapter()
            ->setUseNumber(true,"No.","back.")
            ->add("Nama","nama");
    $master ->setModalTitle("BBM Mobil Ambulance");
    $master ->setMultipleInput(true);
    $master ->setAutofocusOnMultiple("nama");
    $master ->setClearOnEditForNoClear(true);
    $master ->initialize();
?>