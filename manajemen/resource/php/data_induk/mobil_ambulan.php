<?php 
    require_once "smis-base/smis-include-service-consumer.php";
    require_once "smis-libs-class/MasterTemplate.php";

    $master = new MasterTemplate($db,"smis_mjm_jenis_mobil","manajemen","mobil_ambulan");
    $master ->addJSColumn("id",true)
            ->addJSColumn("jenis_mobil",false)
            ->addJSColumn("keterangan",false);
    $master ->getDBtable()->setOrder("jenis_mobil ASC, keterangan ASC",true);
    $master ->getUItable()
            ->setPrintButtonEnable(false)
            ->setReloadButtonEnable(false)
            ->setHeader(array("No.","Mobil Ambulance"));
    $master ->getUItable()
            ->addModal ( "id", "hidden", "", "" )
            ->addModal ( "jenis_mobil", "text", "Jenis Mobil", "" )
            ->addModal ( "keterangan", "textarea", "Keterangan", "" );   
    $master ->getAdapter()
            ->setUseNumber(true,"No.","back.")
            ->add("Mobil Ambulance","jenis_mobil")
            ->add("Keterangan", "keterangan");
    $master ->setModalTitle("Mobil Ambulance");
    $master ->setMultipleInput(true);
    $master ->addNoClear("template");
    $master ->setAutofocusOnMultiple("template");
    $master ->setClearOnEditForNoClear(true);
    $master ->initialize();
?>