<?php 
    require_once "smis-base/smis-include-service-consumer.php";
    require_once "smis-libs-class/MasterTemplate.php";
    require_once "smis-libs-class/MasterServiceTemplate.php";
    
    $waktu  = new OptionBuilder();
    $waktu  ->add("Pagi","pagi")
            ->add("Siang","siang")
            ->add("Malam","malam");
    $master = new MasterServiceTemplate($db,"get_menu","manajemen","gizi");
    $master ->addJSColumn("id",true)
            ->addJSColumn("waktu",false)
            ->addJSColumn("nama",false)
            ->addJSColumn("keterangan",false)
            ->addJSColumn("tarif_lama",false)
            ->addJSColumn("tarif_sekarang",false)
            ->addJSColumn("tarif_baru",false);
    $master ->getDBtable()->setOrder("template ASC, urutan ASC",true);
    $master ->getUItable()
            ->setPrintButtonEnable(false)
            ->setReloadButtonEnable(false)
            ->setHeader(array("Waktu","Nama","Keterangan","Lama","Sekarang","Baru"));
    $master ->getUItable()
            ->addModal ( "id", "hidden", "", "" )
            ->addModal ( "waktu", "select", "Waktu", $waktu->getContent() )
            ->addModal ( "nama", "text", "Nama", "" )
            ->addModal ( "keterangan", "textarea", "Keterangan", "" )
            ->addModal ( "tarif_lama", "money", "Tarif Lama", "" )
            ->addModal ( "tarif_sekarang", "money", "Tarif Sekarang", "" )
            ->addModal ( "tarif_baru", "money", "Tarif Baru", "" );   
    $master ->getAdapter()
            ->add("Nama","nama")
            ->add("Keterangan", "keterangan")
            ->add("Waktu", "waktu")
            ->add("Lama", "tarif_lama","money Rp.")
            ->add("Sekarang", "tarif_sekarang","money Rp.")
            ->add("Baru", "tarif_baru","money Rp.");
    $master ->setModalTitle("Gizi");
    $master ->setMultipleInput(true);
    $master ->addNoClear("template");
    $master ->setAutofocusOnMultiple("template");
    $master ->setClearOnEditForNoClear(true);
    $master ->initialize();
?>