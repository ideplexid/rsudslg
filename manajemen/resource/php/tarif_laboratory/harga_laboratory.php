<?php
require_once 'smis-framework/smis/api/SettingsBuilder.php';
require_once 'smis-base/smis-include-service-consumer.php';
global $db;
$service = new ServiceConsumer ( $db, "get_list_laboratory" );
$service->execute ();
$array = $service->getContent ();
$slug = "smis_lab_harga";
$settings = new SettingsBuilder ( $db, "harga", "manajemen", "harga_laboratory", "" );
$settings->setSingle ( true, $slug );
$settings->setPrefix("harga_laboratory");
$settings->setOneVariable ( true, "smis_lab_harga" );

$dbtable = new DBTable ( $db, "smis_mjm_kelas" );
$dbtable->setShowAll ( true );
$data = $dbtable->view ( "", "0" );
$select = new SelectAdapter ( "nama", "slug" );
$kelas = $select->getContent ( $data ['data'] );

foreach ( $kelas as $element => $value ) {
	$k = $value ['name'];
	$v = $value ['value'];
	$settings->addTabs ( "harga_laboratory".$v, $k,"fa fa-money" );
    if($settings->isGroup("harga_laboratory".$v)){
        foreach ( $array as $ak => $av ) {
            $setitem = new SettingsItem ( $db, $v . "_" . $ak, $av, "0", "money", "", true, $slug );
            $settings->addItem ( "harga_laboratory".$v, $setitem );
            $setitem = new SettingsItem ( $db, $v . "_nominal_" . $ak, "Nominal ".$av, "0", "money", "", true, $slug );
            $settings->addItem ( "bhp_harga_laboratory".$v, $setitem );
            $setitem = new SettingsItem ( $db, $v . "_persen_" . $ak, "Persentase ".$av, "0", "text", "", true, $slug );
            $settings->addItem ( "bhp_harga_laboratory".$v, $setitem );
        }
    }
}
$settings->setPartialLoad(true);
$settings->init ();
?>