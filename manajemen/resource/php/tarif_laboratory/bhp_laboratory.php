<?php
require_once 'smis-framework/smis/api/SettingsBuilder.php';
require_once 'smis-base/smis-include-service-consumer.php';
global $db;
$service = new ServiceConsumer ( $db, "get_list_laboratory" );
$service->execute ();
$array = $service->getContent ();
$slug = "bhp_smis_lab_harga";
$settings = new SettingsBuilder ( $db, "harga", "manajemen", "bhp_laboratory", "" );
$settings->setSingle ( true, $slug );
$settings->setPrefix("bhp_harga_laboratory");
$settings->setOneVariable ( true, $slug);

$dbtable = new DBTable ( $db, "smis_mjm_kelas" );
$dbtable->setShowAll ( true );
$data = $dbtable->view ( "", "0" );
$select = new SelectAdapter ( "nama", "slug" );
$kelas = $select->getContent ( $data ['data'] );

foreach ( $kelas as $element => $value ) {
	$k = $value ['name'];
	$v = $value ['value'];
	$settings->addTabs ( "bhp_harga_laboratory".$v, $k,"fa fa-tty" );
    if($settings->isGroup("bhp_harga_laboratory".$v)){
        foreach ( $array as $ak => $av ) {
            $setitem = new SettingsItem ( $db, $v . "_nominal_" . $ak, "Nominal ".$av, "0", "money", "", true, $slug );
            $settings->addItem ( "bhp_harga_laboratory".$v, $setitem );
            $setitem2 = new SettingsItem ( $db, $v . "_persen_" . $ak, "Persentase ".$av, "0", "text", "", true, $slug );
            $settings->addItem ( "bhp_harga_laboratory".$v, $setitem2 );
        }
    }
	
}
$settings->setPartialLoad(true);
$settings->init ();
?>