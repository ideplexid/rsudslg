<?php
require_once 'smis-framework/smis/api/SettingsBuilder.php';
require_once 'smis-base/smis-include-service-consumer.php';
global $db;
$service = new ServiceConsumer ( $db, "get_list_laboratory" );
$service->execute ();
$array = $service->getContent ();
$slug = "sewa_alat_smis_lab_harga";
$settings = new SettingsBuilder ( $db, "harga", "manajemen", "sewa_alat_laboratory", "" );
$settings->setSingle ( true, $slug );
$settings->setPrefix("sewa_alat_harga_laboratory");
$settings->setOneVariable ( true, $slug);

$dbtable = new DBTable ( $db, "smis_mjm_kelas" );
$dbtable->setShowAll ( true );
$data = $dbtable->view ( "", "0" );
$select = new SelectAdapter ( "nama", "slug" );
$kelas = $select->getContent ( $data ['data'] );

foreach ( $kelas as $element => $value ) {
	$k = $value ['name'];
	$v = $value ['value'];
	$settings->addTabs ( "sewa_alat_harga_laboratory".$v, $k,"fa fa-wrench" );
    if($settings->isGroup("sewa_alat_harga_laboratory".$v)){
        foreach ( $array as $ak => $av ) {
            $setitem = new SettingsItem ( $db, $v . "_" . $ak, $av, "0", "money", "", true, $slug );
            $settings->addItem ( "sewa_alat_harga_laboratory".$v, $setitem );
        }
    }
}
$settings->setPartialLoad(true);
$settings->init ();
?>