<?php
require_once 'smis-framework/smis/api/SettingsBuilder.php';
global $db;
setChangeCookie ( false );
changeCookie ();
$option = new OptionBuilder ();
$option->add ( "Individu", "individual", "1" );
$option->add ( "Kelompok", "communal" );
$opsi = $option->getContent ();

$settings = new SettingsBuilder ( $db, "settings", "manajemen", "provit_sharing_default", "" );
$settings->setShowDescription ( true );
$settings->setTabulatorMode ( Tabulator::$LANDSCAPE_RIGHT);


$settings->addTabs ( "tindakan", "Layanan Dokter dan Perawat" );
$settings->addItem ( "tindakan", new SettingsItem ( $db, "smis-provit-dokter-dtamu", "Dokter Tamu", "90", "text", "Provit Sharing Bagian Dokter Tamu dengan Rumah Sakit" ) );
$settings->addItem ( "tindakan", new SettingsItem ( $db, "smis-provit-dokter-dtamu-sifat", "Bagi Hasil Dokter Tamu", $opsi, "select", "Sifat Bagi Hasil PV Dokter Tamu " ) );
$settings->addItem ( "tindakan", new SettingsItem ( $db, "smis-provit-dokter-dorganik", "Dokter Organik", "70", "text", "Provit Sharing Bagian Dokter Organik dengan Rumah Sakit" ) );
$settings->addItem ( "tindakan", new SettingsItem ( $db, "smis-provit-dokter-dorganik-sifat", "Bagi Hasil Dokter Organik", $opsi, "select", "Sifat Bagi Hasil PV Dokter Organik " ) );
$settings->addItem ( "tindakan", new SettingsItem ( $db, "smis-provit-dokter-asisten-tindakan-dokter", "Asisten Dokter", "85", "text", "Provit Sharing Bagian Dokter Organik dengan Rumah Sakit" ) );
$settings->addItem ( "tindakan", new SettingsItem ( $db, "smis-provit-dokter-asisten-tindakan-dokter-sifat", "Sifat Bagi Hasil Asisten Tindakan Dokter", $opsi, "select", "Sifat Bagi Hasil Asisten Tindakan DOkter" ) );
$settings->addItem ( "tindakan", new SettingsItem ( $db, "smis-provit-dokter-konsultasi", "Konsultasi Dokter", "100", "text", "Bagian Dokter ketika Konsultasi di Poli Rawat Jalan" ) );
$settings->addItem ( "tindakan", new SettingsItem ( $db, "smis-provit-dokter-konsultasi-sifat", "Bagi Hasil Konsultasi Dokter", $opsi, "select", "Sifat Bagi Hasil PV Konsultasi " ) );
$settings->addItem ( "tindakan", new SettingsItem ( $db, "smis-provit-dokter-konsul", "Konsul Dokter", "100", "text", "Bagian yang menjadi Hak Milik DOkter Ketika Konsul" ) );
$settings->addItem ( "tindakan", new SettingsItem ( $db, "smis-provit-dokter-konsul-sifat", "Bagi Hasil Konsul Dokter", $opsi, "select", "Sifat Bagi Hasil PV Konsul " ) );
$settings->addItem ( "tindakan", new SettingsItem ( $db, "smis-provit-dokter-visite", "Visite Dokter", "100", "text", "Bagian Dokter ketika dokter melakukan Visite " ) );
$settings->addItem ( "tindakan", new SettingsItem ( $db, "smis-provit-dokter-visite-sifat", "Bagi Hasil Visite DOkter", $opsi, "select", "Sifat Bagi Hasil PV Visite " ) );
$settings->addItem ( "tindakan", new SettingsItem ( $db, "smis-provit-perawat", "Tindakan Perawat", "51", "text", "Provit Sharing Bagian Perawat dengan Rumah Sakit" ) );
$settings->addItem ( "tindakan", new SettingsItem ( $db, "smis-provit-perawat-sifat", "Bagi Hasil Tindakan Perawat", $opsi, "select", "Sifat Bagi Hasil PV Tindakan Perawat" ) );

$settings->addTabs ( "rr", "Recovery Room" );
$settings->addItem ( "rr", new SettingsItem ( $db, "smis-provit-rr-asisten", "Bagian Asisten Recovery Room", "10", "text", "bagi hasil asisten Recovery Room" ) );
$settings->addItem ( "rr", new SettingsItem ( $db, "smis-provit-rr-asisten-sifat", "Bagi Hasil Asisten Recovery Room", $opsi, "select", "Sifat Bagi Hasil Recovery Room Asisten " ) );
$settings->addItem ( "rr", new SettingsItem ( $db, "smis-provit-rr-petugas", "Bagian Petugas Recovery Room", "90", "text", "bagi hasil petugas Recovery Room" ) );
$settings->addItem ( "rr", new SettingsItem ( $db, "smis-provit-rr-petugas-sifat", "Bagi Hasil Petugas Recovery Room", $opsi, "select", "Sifat Bagi Hasil PV Recovery Room Petugas" ) );

$settings->addTabs ( "ok", "Kamar Operasi (OK)" );
$settings->addItem ( "ok", new SettingsItem ( $db, "smis-provit-ok-dtamu", "Dokter Tamu", "90", "text", "Provit Sharing Bagian Dokter Tamu Saat Operasi di OK dengan Rumah Sakit" ) );
$settings->addItem ( "ok", new SettingsItem ( $db, "smis-provit-ok-dtamu-sifat", "Bagi Hasil Dokter Tamu", $opsi, "select", "Sifat Bagi Hasil Dokter Tamu di OK  " ) );
$settings->addItem ( "ok", new SettingsItem ( $db, "smis-provit-ok-dorganik", "Dokter Organik", "70", "text", "Provit Sharing Bagian Dokter Organik Saat Operasi di OK dengan Rumah Sakit" ) );
$settings->addItem ( "ok", new SettingsItem ( $db, "smis-provit-ok-dorganik-sifat", "Bagi Hasil Dokter Organik", $opsi, "select", "Sifat Bagi Hasil Dokter Organik di OK " ) );
$settings->addItem ( "ok", new SettingsItem ( $db, "smis-provit-ok-anastesi-hadir", "Anastesi Hadir", "70", "text", "Bagian Anastesi saat Hadir di Operasi" ) );
$settings->addItem ( "ok", new SettingsItem ( $db, "smis-provit-ok-anastesi-hadir-sifat", "Bagi Hasil Anastesi yg Hadir", $opsi, "select", "Sifat Bagi Hasil Anastesi yg Hadir di OK " ) );
$settings->addItem ( "ok", new SettingsItem ( $db, "smis-provit-ok-anastesi-thadir", "Anastesi Tidak Hadir", "70", "text", "persentase anastesi saat tidak hadir" ) );
$settings->addItem ( "ok", new SettingsItem ( $db, "smis-provit-ok-anastesi-thadir-sifat", "Bagi Hasil Anastesi yg Tidak Hadir", $opsi, "select", "Sifat Bagi Hasil Anastesi yg Tidak Hadir di OK " ) );
$settings->addItem ( "ok", new SettingsItem ( $db, "smis-provit-ok-as-anastesi-hadir", "Asisten Saat Anastesi Hadir", "70", "text", "Bagian Asisten Anastesi saat Anastesi hadir" ) );
$settings->addItem ( "ok", new SettingsItem ( $db, "smis-provit-ok-as-anastesi-hadir-sifat", "Bagi Hasil Asisten Anastesi saat Anastesi Hadir", $opsi, "select", "Sifat Bagi Hasil Asisten Anastesi saat Dokter Anastesi Hadir di OK " ) );
$settings->addItem ( "ok", new SettingsItem ( $db, "smis-provit-ok-as-anastesi-thadir", "Asisten Saat Anastesi Tidak Hadir", "70", "text", "Bagian Asisten Anastesi saat Anastesi Tidak Hadir" ) );
$settings->addItem ( "ok", new SettingsItem ( $db, "smis-provit-ok-as-anastesi-thadir-sifat", "Bagi Hasil Asisten Anastesi saat Anastesi Tidak Hadir", $opsi, "select", "Sifat Bagi Hasil Asisten Anastesi saat Dokter Anastesi Tidak Hadir di OK " ) );
$settings->addItem ( "ok", new SettingsItem ( $db, "smis-provit-ok-team-ok", "Bagian Team OK", "70", "text", "Bagian Team OK" ) );
$settings->addItem ( "ok", new SettingsItem ( $db, "smis-provit-ok-team-ok-sifat", "Bagi Hasil Team OK", $opsi, "select", "Sifat Bagi Hasil Team OK " ) );
$settings->addItem ( "ok", new SettingsItem ( $db, "smis-provit-ok-team-perawat", "Bagian Team Perawat di OK", "70", "text", "Team Perawat di OK" ) );
$settings->addItem ( "ok", new SettingsItem ( $db, "smis-provit-ok-team-perawat-sifat", "Bagi Hasil Team Perawat", $opsi, "select", "Sifat Bagi Hasil Team Perawat di OK" ) );
$settings->addItem ( "ok", new SettingsItem ( $db, "smis-provit-ok-team-bidan", "Bagian Team Bidan", "70", "text", "Team Perawat di OK" ) );
$settings->addItem ( "ok", new SettingsItem ( $db, "smis-provit-ok-team-bidan-sifat", "Bagi Hasil Team Bidan", $opsi, "select", "Sifat Bagi Hasil Team Bidan di OK" ) );

$settings->addTabs ( "vk", "Kamar Bersalin (VK)" );
$settings->addItem ( "vk", new SettingsItem ( $db, "smis-provit-vk-dtamu", "Dokter Tamu", "90", "text", "Provit Sharing Bagian Dokter Tamu Saat Operasi di VK dengan Rumah Sakit" ) );
$settings->addItem ( "vk", new SettingsItem ( $db, "smis-provit-vk-dtamu-sifat", "Bagi Hasil Dokter Tamu", $opsi, "select", "Sifat Bagi Hasil Dokter Tamu di VK  " ) );
$settings->addItem ( "vk", new SettingsItem ( $db, "smis-provit-vk-dorganik", "Dokter Organik", "70", "text", "Provit Sharing Bagian Dokter Organik Saat Operasi di VK dengan Rumah Sakit" ) );
$settings->addItem ( "vk", new SettingsItem ( $db, "smis-provit-vk-dorganik-sifat", "Bagi Hasil Dokter Organik", $opsi, "select", "Sifat Bagi Hasil Dokter Organik di VK " ) );
$settings->addItem ( "vk", new SettingsItem ( $db, "smis-provit-vk-anastesi-hadir", "Anastesi Hadir", "70", "text", "Bagian Anastesi saat Hadir di Operasi" ) );
$settings->addItem ( "vk", new SettingsItem ( $db, "smis-provit-vk-anastesi-hadir-sifat", "Bagi Hasil Anastesi yg Hadir", $opsi, "select", "Sifat Bagi Hasil Anastesi yg Hadir di VK " ) );
$settings->addItem ( "vk", new SettingsItem ( $db, "smis-provit-vk-anastesi-thadir", "Anastesi Tidak Hadir", "70", "text", "persentase anastesi saat tidak hadir" ) );
$settings->addItem ( "vk", new SettingsItem ( $db, "smis-provit-vk-anastesi-thadir-sifat", "Bagi Hasil Anastesi yg Tidak Hadir", $opsi, "select", "Sifat Bagi Hasil Anastesi yg Tidak Hadir di VK " ) );
$settings->addItem ( "vk", new SettingsItem ( $db, "smis-provit-vk-as-anastesi-hadir", "Asisten Saat Anastesi Hadir", "70", "text", "Bagian Asisten Anastesi saat Anastesi hadir" ) );
$settings->addItem ( "vk", new SettingsItem ( $db, "smis-provit-vk-as-anastesi-hadir-sifat", "Bagi Hasil Asisten Anastesi saat Anastesi Hadir", $opsi, "select", "Sifat Bagi Hasil Asisten Anastesi saat Dokter Anastesi Hadir di VK " ) );
$settings->addItem ( "vk", new SettingsItem ( $db, "smis-provit-vk-as-anastesi-thadir", "Asisten Saat Anastesi Tidak Hadir", "70", "text", "Bagian Asisten Anastesi saat Anastesi Tidak Hadir" ) );
$settings->addItem ( "vk", new SettingsItem ( $db, "smis-provit-vk-as-anastesi-thadir-sifat", "Bagi Hasil Asisten Anastesi saat Anastesi Tidak Hadir", $opsi, "select", "Sifat Bagi Hasil Asisten Anastesi saat Dokter Anastesi Tidak Hadir di VK " ) );
$settings->addItem ( "vk", new SettingsItem ( $db, "smis-provit-vk-team-vk", "Bagian Team VK", "70", "text", "Bagian Team VK" ) );
$settings->addItem ( "vk", new SettingsItem ( $db, "smis-provit-vk-team-vk-sifat", "Bagi Hasil Team VK", $opsi, "select", "Sifat Bagi Hasil Team VK " ) );
$settings->addItem ( "vk", new SettingsItem ( $db, "smis-provit-vk-team-perawat", "Bagian Team Perawat di VK", "70", "text", "Team Perawat di VK" ) );
$settings->addItem ( "vk", new SettingsItem ( $db, "smis-provit-vk-team-perawat-sifat", "Bagi Hasil Team Perawat", $opsi, "select", "Sifat Bagi Hasil Team Perawat di VK" ) );
$settings->addItem ( "vk", new SettingsItem ( $db, "smis-provit-vk-team-bidan", "Bagian Team Bidan", "70", "text", "Team Perawat di VK" ) );
$settings->addItem ( "vk", new SettingsItem ( $db, "smis-provit-vk-team-bidan-sifat", "Bagi Hasil Team Bidan", $opsi, "select", "Sifat Bagi Hasil Team Bidan di VK" ) );

$settings->addTabs ( "fst", "Fisiotherapy" );
$settings->addItem ( "fst", new SettingsItem ( $db, "smis-provit-fst-dokter", "Dokter Fisiotherapy", "10", "text", "Bagian Dokter Pengirim di Fisiotherapy" ) );
$settings->addItem ( "fst", new SettingsItem ( $db, "smis-provit-fst-dokter-sifat", "Bagi Hasil Dokter Pengirim", $opsi, "select", "Sifat Bagi Hasil Dokter Pengirim" ) );
$settings->addItem ( "fst", new SettingsItem ( $db, "smis-provit-fst-petugas", "Petugas Fisiotherapy", "8.5", "text", "Bagian Petugas Fisiotherapy" ) );
$settings->addItem ( "fst", new SettingsItem ( $db, "smis-provit-fst-petugas-sifat", "Bagi Hasil Petugas", $opsi, "select", "Sifat Bagi Hasil Dokter Petugas" ) );

$settings->addTabs ( "ekg", "EKG" );
$settings->addItem ( "ekg", new SettingsItem ( $db, "smis-provit-ekg-dokter-pengirim", "Dokter Pengirim EKG", "10", "text", "Bagian Dokter Pengirim EKG" ) );
$settings->addItem ( "ekg", new SettingsItem ( $db, "smis-provit-ekg-dokter-pengirim-sifat", "Bagi Hasil EKG Dokter Pengirim", $opsi, "select", "Bagi Hasil EKG Dokter Pengirim" ) );
$settings->addItem ( "ekg", new SettingsItem ( $db, "smis-provit-ekg-dokter-pembaca", "Dokter Pembaca EKG", "20", "text", "Bagian Dokter Pembaca EKG" ) );
$settings->addItem ( "ekg", new SettingsItem ( $db, "smis-provit-ekg-dokter-pembaca-sifat", "Bagi Hasil EKG Dokter Pembaca", $opsi, "select", "Bagi Hasil EKG Dokter Pembaca" ) );
$settings->addItem ( "ekg", new SettingsItem ( $db, "smis-provit-ekg-petugas", "Petugas EKG", "5", "text", "Bagian Petugas EKG" ) );
$settings->addItem ( "ekg", new SettingsItem ( $db, "smis-provit-ekg-petugas-sifat", "Bagi Hasil EKG Petugas EKG", $opsi, "select", "Bagi Hasil Petugas EKG" ) );

$settings->addTabs ( "faalparu", "Faal Paru" );
$settings->addItem ( "faalparu", new SettingsItem ( $db, "smis-provit-fparu-dokter", "Dokter Faal Paru", "10", "text", "Bagian Dokter Faal Paru" ) );
$settings->addItem ( "faalparu", new SettingsItem ( $db, "smis-provit-fparu-dokter-sifat", "Sifat Bagi Hasil Dokter Faal Paru", $opsi, "select", "Sifat Bagi Hasil Dokter Faal Paru" ) );
$settings->addItem ( "faalparu", new SettingsItem ( $db, "smis-provit-fparu-asisten", "Asisten Faal Paru", "20", "text", "Bagian Asisten Faal Paru" ) );
$settings->addItem ( "faalparu", new SettingsItem ( $db, "smis-provit-fparu-asisten-sifat", "Sifat Bagi Hasil Asisten", $opsi, "select", "Sifat Bagi Hasil Asisten Faal Paru" ) );

$settings->addTabs ( "endoscopy", "Endoscopy" );
$settings->addItem ( "endoscopy", new SettingsItem ( $db, "smis-provit-endoscopy-dokter", "Dokter Endoscopy", "10", "text", "Bagian Dokter Endoscopy" ) );
$settings->addItem ( "endoscopy", new SettingsItem ( $db, "smis-provit-endoscopy-dokter-sifat", "Sifat Bagi Hasil Dokter", $opsi, "select", "Sifat Bagi Hasil Dokter" ) );
$settings->addItem ( "endoscopy", new SettingsItem ( $db, "smis-provit-endoscopy-asisten", "Asisten Endoscopy", "20", "text", "Bagian Asisten Endoscopy" ) );
$settings->addItem ( "endoscopy", new SettingsItem ( $db, "smis-provit-endoscopy-asisten-sifat", "Sifat Bagi Hasil Asisten", $opsi, "select", "Sifat Bagi Hasil Asisten" ) );

$settings->addTabs ( "bronchoscopy", "Bronchoscopy" );
$settings->addItem ( "bronchoscopy", new SettingsItem ( $db, "smis-provit-bronchoscopy-dokter", "Dokter Bronchoscopy", "10", "text", "Bagian Dokter Bronchoscopy" ) );
$settings->addItem ( "bronchoscopy", new SettingsItem ( $db, "smis-provit-bronchoscopy-dokter-sifat", "Sifat Bagi Hasil Dokter", $opsi, "select", "Sifat Bagi Hasil Dokter" ) );
$settings->addItem ( "bronchoscopy", new SettingsItem ( $db, "smis-provit-bronchoscopy-perawat", "Perawat Bronchoscopy", "20", "text", "Bagian Perawat Bronchoscopy" ) );
$settings->addItem ( "bronchoscopy", new SettingsItem ( $db, "smis-provit-bronchoscopy-perawat-sifat", "Sifat Bagi Hasil Perawat", $opsi, "select", "Sifat Bagi Hasil Perawat" ) );

$settings->addTabs ( "audiometry", "Audiometry" );
$settings->addItem ( "audiometry", new SettingsItem ( $db, "smis-provit-audiometry-dokter", "Dokter Audiometry", "10", "text", "Bagian Dokter Audiometry" ) );
$settings->addItem ( "audiometry", new SettingsItem ( $db, "smis-provit-audiometry-dokter-sifat", "Sifat Bagi Hasil Dokter", $opsi, "select", "Sifat Bagi Hasil Dokter" ) );
$settings->addItem ( "audiometry", new SettingsItem ( $db, "smis-provit-audiometry-perawat", "Perawat Audiometry", "20", "text", "Bagian Perawat Audiometry" ) );
$settings->addItem ( "audiometry", new SettingsItem ( $db, "smis-provit-audiometry-perawat-sifat", "Sifat Bagi Hasil Perawat", $opsi, "select", "Sifat Bagi Hasil Perawat" ) );

$settings->addTabs ( "spirometry", "Spirometry" );
$settings->addItem ( "spirometry", new SettingsItem ( $db, "smis-provit-spirometry-dokter", "Dokter Spirometry", "10", "text", "Bagian Dokter Spirometry" ) );
$settings->addItem ( "spirometry", new SettingsItem ( $db, "smis-provit-spirometry-dokter-sifat", "Sifat Bagi Hasil Dokter", $opsi, "select", "Sifat Bagi Hasil Dokter" ) );
$settings->addItem ( "spirometry", new SettingsItem ( $db, "smis-provit-spirometry-perawat", "Perawat Spirometry", "20", "text", "Bagian Perawat Spirometry" ) );
$settings->addItem ( "spirometry", new SettingsItem ( $db, "smis-provit-spirometry-perawat-sifat", "Sifat Bagi Hasil Perawat", $opsi, "select", "Sifat Bagi Hasil Perawat" ) );

$settings->addTabs ( "laboratory", "Laboratory" );
$settings->addItem ( "laboratory", new SettingsItem ( $db, "smis-provit-laboratory-dokter-pengirim", "Dokter Pengirim Laboratory", "10", "text", "Bagian Dokter Pengirim di Laboratory" ) );
$settings->addItem ( "laboratory", new SettingsItem ( $db, "smis-provit-laboratory-dokter-pengirim-sifat", "Sifat Bagi Hasil Dokter Pengirim", $opsi, "select", "Sifat Bagi Hasil Dokter Pengirim" ) );
$settings->addItem ( "laboratory", new SettingsItem ( $db, "smis-provit-laboratory-dokter-konsultan", "Dokter Konsultan Laboratory", "10", "text", "Bagian Dokter Konsultan di Laboratory" ) );
$settings->addItem ( "laboratory", new SettingsItem ( $db, "smis-provit-laboratory-dokter-konsultan-sifat", "Sifat Bagi Hasil Dokter Konsultan", $opsi, "select", "Sifat Bagi Hasil Dokter Konsultan" ) );
$settings->addItem ( "laboratory", new SettingsItem ( $db, "smis-provit-laboratory-petugas", "Petugas Laboratory", "10", "text", "Bagian Petugas di Laboratory" ) );
$settings->addItem ( "laboratory", new SettingsItem ( $db, "smis-provit-laboratory-petugas-sifat", "Sifat Bagi Hasil Petugas Laboratory", $opsi, "select", "Sifat Bagi Petugas Pengirim" ) );
$settings->addItem ( "laboratory", new SettingsItem ( $db, "smis-provit-laboratory-grup", "Grup", "5", "text", "Bagian Grup di Laboratory" ) );
$settings->addItem ( "laboratory", new SettingsItem ( $db, "smis-provit-laboratory-grup-sifat", "Sifat Bagi Hasil Grup ", $opsi, "select", "Sifat Bagi Grup Pengirim" ) );

$settings->addTabs ( "radiology", "Radiology" );
$settings->addItem ( "radiology", new SettingsItem ( $db, "smis-provit-radiology-dokter-pengirim-plain", "Dokter Pengirim Plain Foto", "10", "text", "Bagian Dokter Pengirim ke Radiology untuk Plain Foto" ) );
$settings->addItem ( "radiology", new SettingsItem ( $db, "smis-provit-radiology-dokter-pengirim-plain-sifat", "Sifat Bagi Hasil Dokter Pengirim Plain Foto", $opsi, "select", "Sifat Bagi Hasil Dokter Pengirim PLain Foto" ) );
$settings->addItem ( "radiology", new SettingsItem ( $db, "smis-provit-radiology-dokter-konsultan-plain", "Dokter Konsultan Plain Foto", "10", "text", "Bagian Dokter KOnsultan untuk Plain Foto" ) );
$settings->addItem ( "radiology", new SettingsItem ( $db, "smis-provit-radiology-dokter-konsultan-plain-sifat", "Sifat Bagi Hasil Dokter Konsultan Plain Foto", $opsi, "select", "Sifat Bagi Hasil Dokter Konsultan PLain Foto" ) );
$settings->addItem ( "radiology", new SettingsItem ( $db, "smis-provit-radiology-petugas-plain", "Petugas Plain Foto", "10", "text", "Bagian Petugas untuk Plain Foto" ) );
$settings->addItem ( "radiology", new SettingsItem ( $db, "smis-provit-radiology-dokter-petugas-plain-sifat", "Sifat Bagi Hasil Petugas Plain Foto", $opsi, "select", "Sifat Bagi Hasil Petugas PLain Foto" ) );

$settings->addItem ( "radiology", new SettingsItem ( $db, "smis-provit-radiology-dokter-pengirim-contrast", "Dokter Pengirim Foto Kontras", "10", "text", "Bagian Dokter Pengirim ke Radiology untuk Foto Kontras" ) );
$settings->addItem ( "radiology", new SettingsItem ( $db, "smis-provit-radiology-dokter-pengirim-contrast-sifat", "Sifat Bagi Hasil Dokter Pengirim Foto Kontras", $opsi, "select", "Sifat Bagi Hasil Dokter Pengirim Foto Kontras" ) );
$settings->addItem ( "radiology", new SettingsItem ( $db, "smis-provit-radiology-dokter-konsultan-contrast", "Dokter Konsultan Foto Kontras", "10", "text", "Bagian Dokter KOnsultan untuk Foto Kontras" ) );
$settings->addItem ( "radiology", new SettingsItem ( $db, "smis-provit-radiology-dokter-konsultan-contrast-sifat", "Sifat Bagi Hasil Dokter Konsultan Foto Kontras", $opsi, "select", "Sifat Bagi Hasil Dokter Konsultan Foto Kontras" ) );
$settings->addItem ( "radiology", new SettingsItem ( $db, "smis-provit-radiology-petugas-contrast", "Petugas Foto Kontras", "10", "text", "Bagian Petugas untuk Foto Kontras" ) );
$settings->addItem ( "radiology", new SettingsItem ( $db, "smis-provit-radiology-petugas-contrast-sifat", "Sifat Bagi Hasil Petugas Foto Kontras", $opsi, "select", "Sifat Bagi Hasil Petugas Foto Kontras" ) );

$settings->addItem ( "radiology", new SettingsItem ( $db, "smis-provit-radiology-dokter-pengirim-usg", "Dokter Pengirim Ultrasonography", "10", "text", "Bagian Dokter Pengirim ke Radiology untuk USG" ) );
$settings->addItem ( "radiology", new SettingsItem ( $db, "smis-provit-radiology-dokter-pengirim-usg-sifat", "Sifat Bagi Hasil Dokter Pengirim USG", $opsi, "select", "Sifat Bagi Hasil Dokter Pengirim USG" ) );
$settings->addItem ( "radiology", new SettingsItem ( $db, "smis-provit-radiology-dokter-konsultan-usg", "Dokter Konsultan Ultrasonography", "10", "text", "Bagian Dokter KOnsultan untuk USG" ) );
$settings->addItem ( "radiology", new SettingsItem ( $db, "smis-provit-radiology-dokter-konsultan-usg-sifat", "Sifat Bagi Hasil Dokter Konsultan USG", $opsi, "select", "Sifat Bagi Hasil Dokter Konsultan USG" ) );
$settings->addItem ( "radiology", new SettingsItem ( $db, "smis-provit-radiology-petugas-usg", "Petugas Ultrasonography", "10", "text", "Bagian Petugas untuk USG" ) );
$settings->addItem ( "radiology", new SettingsItem ( $db, "smis-provit-radiology-petugas-usg-sifat", "Sifat Bagi Hasil Petugas USG", $opsi, "select", "Sifat Bagi Hasil Petugas USG" ) );

$settings->addItem ( "radiology", new SettingsItem ( $db, "smis-provit-radiology-dokter-pengirim-ctscan", "Dokter Pengirim CT Scan", "10", "text", "Bagian Dokter Pengirim ke Radiology untuk CT Scan" ) );
$settings->addItem ( "radiology", new SettingsItem ( $db, "smis-provit-radiology-dokter-pengirim-ctscan-sifat", "Sifat Bagi Hasil Dokter Pengirim CT Scan", $opsi, "select", "Sifat Bagi Hasil Dokter Pengirim CT Scan" ) );
$settings->addItem ( "radiology", new SettingsItem ( $db, "smis-provit-radiology-dokter-konsultan-ctscan", "Dokter Konsultan CT Scan", "10", "text", "Bagian Dokter KOnsultan untuk CT Scan" ) );
$settings->addItem ( "radiology", new SettingsItem ( $db, "smis-provit-radiology-dokter-konsultan-ctscan-sifat", "Sifat Bagi Hasil Dokter Konsultan CT Scan", $opsi, "select", "Sifat Bagi Hasil Dokter Konsultan CT Scan" ) );
$settings->addItem ( "radiology", new SettingsItem ( $db, "smis-provit-radiology-petugas-ctscan", "Petugas CT Scan", "10", "text", "Bagian Petugas untuk CT Scan" ) );
$settings->addItem ( "radiology", new SettingsItem ( $db, "smis-provit-radiology-petugas-ctscan-sifat", "Sifat Bagi Hasil Petugas CT Scan", $opsi, "select", "Sifat Bagi Hasil Petugas CT Scan" ) );

$settings->addTabs ( "igd", "IGD" );
$settings->addItem ( "igd", new SettingsItem ( $db, "smis-provit-igd-dokter", "Dokter Jaga IGD", "10", "text", "Bagian Doker Jaga IGD" ) );
$settings->addItem ( "igd", new SettingsItem ( $db, "smis-provit-igd-dokter-sifat", "Sifat Pembagian Dokter Jaga", $opsi, "select", "Sifat Pembagian Dokter Jaga" ) );
$settings->addItem ( "igd", new SettingsItem ( $db, "smis-provit-igd-perawat", "Perawat IGD", "0", "text", "Bagian Perawat IGD" ) );
$settings->addItem ( "igd", new SettingsItem ( $db, "smis-provit-igd-perawat-sifat", "Sifat Pembagian Perawat IGD", $opsi, "select", "Sifat Pembagian Perawat IGD" ) );


$settings->addTabs ( "apotek", "Apotek" );
$settings->addItem ( "apotek", new SettingsItem ( $db, "smis-provit-apotek-dokter-pengirim", "Dokter Pengirim dari Apotek", "10", "text", "Provit Sharing Bagian Dokter yang Mengirim Pasien ke Apotek untuik beli obat" ) );
$settings->addItem ( "apotek", new SettingsItem ( $db, "smis-provit-dokter-obat-return", "Return Obat", "90", "text", "Return Obat Apotek sekian % dari harga" ) );

$settings->addTabs ( "pajak", "Pajak" );
$settings->addItem ( "pajak", new SettingsItem ( $db, "smis-rs-persen-pajak-gaji", "Persentase Pajak", "2", "text", "Persentase Pemotongan Pajak" ) );

$response = $settings->init ();
?>