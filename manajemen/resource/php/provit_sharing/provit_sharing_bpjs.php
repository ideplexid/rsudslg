<?php
require_once 'smis-framework/smis/api/SettingsBuilder.php';
global $db;
setChangeCookie ( false );
changeCookie ();
$option = new OptionBuilder ();
$option->add ( "Individu", "individual", "1" );
$option->add ( "Kelompok", "communal" );
$opsi = $option->getContent ();

$settings = new SettingsBuilder ( $db, "settings", "manajemen", "provit_sharing_bpjs", "" );
$settings->setShowDescription ( true ); 
$settings->setTabulatorMode ( Tabulator::$LANDSCAPE_RIGHT);

$settings->addTabs ( "bpjs_tindakan", "Layanan Dokter dan Perawat" );
$settings->addItem ( "bpjs_tindakan", new SettingsItem ( $db, "smis-provit-bpjs-dokter-dtamu", "Dokter Tamu", "90", "text", "Provit Sharing Bagian Dokter Tamu dengan Rumah Sakit" ) );
$settings->addItem ( "bpjs_tindakan", new SettingsItem ( $db, "smis-provit-bpjs-dokter-dtamu-sifat", "Bagi Hasil Dokter Tamu", $opsi, "select", "Sifat Bagi Hasil PV Dokter Tamu " ) );
$settings->addItem ( "bpjs_tindakan", new SettingsItem ( $db, "smis-provit-bpjs-dokter-dorganik", "Dokter Organik", "70", "text", "Provit Sharing Bagian Dokter Organik dengan Rumah Sakit" ) );
$settings->addItem ( "bpjs_tindakan", new SettingsItem ( $db, "smis-provit-bpjs-dokter-dorganik-sifat", "Bagi Hasil Dokter Organik", $opsi, "select", "Sifat Bagi Hasil PV Dokter Organik " ) );
$settings->addItem ( "bpjs_tindakan", new SettingsItem ( $db, "smis-provit-bpjs-dokter-asisten-tindakan-dokter", "Asisten Dokter", "85", "text", "Provit Sharing Bagian Dokter Organik dengan Rumah Sakit" ) );
$settings->addItem ( "bpjs_tindakan", new SettingsItem ( $db, "smis-provit-bpjs-dokter-asisten-tindakan-dokter-sifat", "Sifat Bagi Hasil Asisten Tindakan Dokter", $opsi, "select", "Sifat Bagi Hasil Asisten Tindakan DOkter" ) );
$settings->addItem ( "bpjs_tindakan", new SettingsItem ( $db, "smis-provit-bpjs-dokter-konsultasi", "Konsultasi Dokter", "100", "text", "Bagian Dokter ketika Konsultasi di Poli Rawat Jalan" ) );
$settings->addItem ( "bpjs_tindakan", new SettingsItem ( $db, "smis-provit-bpjs-dokter-konsultasi-sifat", "Bagi Hasil Konsultasi Dokter", $opsi, "select", "Sifat Bagi Hasil PV Konsultasi " ) );
$settings->addItem ( "bpjs_tindakan", new SettingsItem ( $db, "smis-provit-bpjs-dokter-konsul", "Konsul Dokter", "100", "text", "Bagian yang menjadi Hak Milik DOkter Ketika Konsul" ) );
$settings->addItem ( "bpjs_tindakan", new SettingsItem ( $db, "smis-provit-bpjs-dokter-konsul-sifat", "Bagi Hasil Konsul Dokter", $opsi, "select", "Sifat Bagi Hasil PV Konsul " ) );
$settings->addItem ( "bpjs_tindakan", new SettingsItem ( $db, "smis-provit-bpjs-dokter-visite", "Visite Dokter", "100", "text", "Bagian Dokter ketika dokter melakukan Visite " ) );
$settings->addItem ( "bpjs_tindakan", new SettingsItem ( $db, "smis-provit-bpjs-dokter-visite-sifat", "Bagi Hasil Visite DOkter", $opsi, "select", "Sifat Bagi Hasil PV Visite " ) );
$settings->addItem ( "bpjs_tindakan", new SettingsItem ( $db, "smis-provit-bpjs-perawat", "Tindakan Perawat", "51", "text", "Provit Sharing Bagian Perawat dengan Rumah Sakit" ) );
$settings->addItem ( "bpjs_tindakan", new SettingsItem ( $db, "smis-provit-bpjs-perawat-sifat", "Bagi Hasil Tindakan Perawat", $opsi, "select", "Sifat Bagi Hasil PV Tindakan Perawat" ) );

$settings->addTabs ( "bpjs_rr", "Recovery Room" );
$settings->addItem ( "bpjs_rr", new SettingsItem ( $db, "smis-provit-bpjs-rr-asisten", "Bagian Asisten Recovery Room", "10", "text", "bagi hasil asisten Recovery Room" ) );
$settings->addItem ( "bpjs_rr", new SettingsItem ( $db, "smis-provit-bpjs-rr-asisten-sifat", "Bagi Hasil Asisten Recovery Room", $opsi, "select", "Sifat Bagi Hasil Recovery Room Asisten " ) );
$settings->addItem ( "bpjs_rr", new SettingsItem ( $db, "smis-provit-bpjs-rr-petugas", "Bagian Petugas Recovery Room", "90", "text", "bagi hasil petugas Recovery Room" ) );
$settings->addItem ( "bpjs_rr", new SettingsItem ( $db, "smis-provit-bpjs-rr-petugas-sifat", "Bagi Hasil Petugas Recovery Room", $opsi, "select", "Sifat Bagi Hasil PV Recovery Room Petugas" ) );

$settings->addTabs ( "bpjs_ok", "Kamar Operasi (OK)" );
$settings->addItem ( "bpjs_ok", new SettingsItem ( $db, "smis-provit-bpjs-ok-dtamu", "Dokter Tamu", "90", "text", "Provit Sharing Bagian Dokter Tamu Saat Operasi di OK dengan Rumah Sakit" ) );
$settings->addItem ( "bpjs_ok", new SettingsItem ( $db, "smis-provit-bpjs-ok-dtamu-sifat", "Bagi Hasil Dokter Tamu", $opsi, "select", "Sifat Bagi Hasil Dokter Tamu di OK  " ) );
$settings->addItem ( "bpjs_ok", new SettingsItem ( $db, "smis-provit-bpjs-ok-dorganik", "Dokter Organik", "70", "text", "Provit Sharing Bagian Dokter Organik Saat Operasi di OK dengan Rumah Sakit" ) );
$settings->addItem ( "bpjs_ok", new SettingsItem ( $db, "smis-provit-bpjs-ok-dorganik-sifat", "Bagi Hasil Dokter Organik", $opsi, "select", "Sifat Bagi Hasil Dokter Organik di OK " ) );
$settings->addItem ( "bpjs_ok", new SettingsItem ( $db, "smis-provit-bpjs-ok-anastesi-hadir", "Anastesi Hadir", "70", "text", "Bagian Anastesi saat Hadir di Operasi" ) );
$settings->addItem ( "bpjs_ok", new SettingsItem ( $db, "smis-provit-bpjs-ok-anastesi-hadir-sifat", "Bagi Hasil Anastesi yg Hadir", $opsi, "select", "Sifat Bagi Hasil Anastesi yg Hadir di OK " ) );
$settings->addItem ( "bpjs_ok", new SettingsItem ( $db, "smis-provit-bpjs-ok-anastesi-thadir", "Anastesi Tidak Hadir", "70", "text", "persentase anastesi saat tidak hadir" ) );
$settings->addItem ( "bpjs_ok", new SettingsItem ( $db, "smis-provit-bpjs-ok-anastesi-thadir-sifat", "Bagi Hasil Anastesi yg Tidak Hadir", $opsi, "select", "Sifat Bagi Hasil Anastesi yg Tidak Hadir di OK " ) );
$settings->addItem ( "bpjs_ok", new SettingsItem ( $db, "smis-provit-bpjs-ok-as-anastesi-hadir", "Asisten Saat Anastesi Hadir", "70", "text", "Bagian Asisten Anastesi saat Anastesi hadir" ) );
$settings->addItem ( "bpjs_ok", new SettingsItem ( $db, "smis-provit-bpjs-ok-as-anastesi-hadir-sifat", "Bagi Hasil Asisten Anastesi saat Anastesi Hadir", $opsi, "select", "Sifat Bagi Hasil Asisten Anastesi saat Dokter Anastesi Hadir di OK " ) );
$settings->addItem ( "bpjs_ok", new SettingsItem ( $db, "smis-provit-bpjs-ok-as-anastesi-thadir", "Asisten Saat Anastesi Tidak Hadir", "70", "text", "Bagian Asisten Anastesi saat Anastesi Tidak Hadir" ) );
$settings->addItem ( "bpjs_ok", new SettingsItem ( $db, "smis-provit-bpjs-ok-as-anastesi-thadir-sifat", "Bagi Hasil Asisten Anastesi saat Anastesi Tidak Hadir", $opsi, "select", "Sifat Bagi Hasil Asisten Anastesi saat Dokter Anastesi Tidak Hadir di OK " ) );
$settings->addItem ( "bpjs_ok", new SettingsItem ( $db, "smis-provit-bpjs-ok-team-ok", "Bagian Team OK", "70", "text", "Bagian Team OK" ) );
$settings->addItem ( "bpjs_ok", new SettingsItem ( $db, "smis-provit-bpjs-ok-team-ok-sifat", "Bagi Hasil Team OK", $opsi, "select", "Sifat Bagi Hasil Team OK " ) );
$settings->addItem ( "bpjs_ok", new SettingsItem ( $db, "smis-provit-bpjs-ok-team-perawat", "Bagian Team Perawat di OK", "70", "text", "Team Perawat di OK" ) );
$settings->addItem ( "bpjs_ok", new SettingsItem ( $db, "smis-provit-bpjs-ok-team-perawat-sifat", "Bagi Hasil Team Perawat", $opsi, "select", "Sifat Bagi Hasil Team Perawat di OK" ) );
$settings->addItem ( "bpjs_ok", new SettingsItem ( $db, "smis-provit-bpjs-ok-team-bidan", "Bagian Team Bidan", "70", "text", "Team Perawat di OK" ) );
$settings->addItem ( "bpjs_ok", new SettingsItem ( $db, "smis-provit-bpjs-ok-team-bidan-sifat", "Bagi Hasil Team Bidan", $opsi, "select", "Sifat Bagi Hasil Team Bidan di OK" ) );

$settings->addTabs ( "bpjs_vk", "Kamar Bersalin (VK)" );
$settings->addItem ( "bpjs_vk", new SettingsItem ( $db, "smis-provit-bpjs-vk-dtamu", "Dokter Tamu", "90", "text", "Provit Sharing Bagian Dokter Tamu Saat Operasi di VK dengan Rumah Sakit" ) );
$settings->addItem ( "bpjs_vk", new SettingsItem ( $db, "smis-provit-bpjs-vk-dtamu-sifat", "Bagi Hasil Dokter Tamu", $opsi, "select", "Sifat Bagi Hasil Dokter Tamu di VK  " ) );
$settings->addItem ( "bpjs_vk", new SettingsItem ( $db, "smis-provit-bpjs-vk-dorganik", "Dokter Organik", "70", "text", "Provit Sharing Bagian Dokter Organik Saat Operasi di VK dengan Rumah Sakit" ) );
$settings->addItem ( "bpjs_vk", new SettingsItem ( $db, "smis-provit-bpjs-vk-dorganik-sifat", "Bagi Hasil Dokter Organik", $opsi, "select", "Sifat Bagi Hasil Dokter Organik di VK " ) );
$settings->addItem ( "bpjs_vk", new SettingsItem ( $db, "smis-provit-bpjs-vk-anastesi-hadir", "Anastesi Hadir", "70", "text", "Bagian Anastesi saat Hadir di Operasi" ) );
$settings->addItem ( "bpjs_vk", new SettingsItem ( $db, "smis-provit-bpjs-vk-anastesi-hadir-sifat", "Bagi Hasil Anastesi yg Hadir", $opsi, "select", "Sifat Bagi Hasil Anastesi yg Hadir di VK " ) );
$settings->addItem ( "bpjs_vk", new SettingsItem ( $db, "smis-provit-bpjs-vk-anastesi-thadir", "Anastesi Tidak Hadir", "70", "text", "persentase anastesi saat tidak hadir" ) );
$settings->addItem ( "bpjs_vk", new SettingsItem ( $db, "smis-provit-bpjs-vk-anastesi-thadir-sifat", "Bagi Hasil Anastesi yg Tidak Hadir", $opsi, "select", "Sifat Bagi Hasil Anastesi yg Tidak Hadir di VK " ) );
$settings->addItem ( "bpjs_vk", new SettingsItem ( $db, "smis-provit-bpjs-vk-as-anastesi-hadir", "Asisten Saat Anastesi Hadir", "70", "text", "Bagian Asisten Anastesi saat Anastesi hadir" ) );
$settings->addItem ( "bpjs_vk", new SettingsItem ( $db, "smis-provit-bpjs-vk-as-anastesi-hadir-sifat", "Bagi Hasil Asisten Anastesi saat Anastesi Hadir", $opsi, "select", "Sifat Bagi Hasil Asisten Anastesi saat Dokter Anastesi Hadir di VK " ) );
$settings->addItem ( "bpjs_vk", new SettingsItem ( $db, "smis-provit-bpjs-vk-as-anastesi-thadir", "Asisten Saat Anastesi Tidak Hadir", "70", "text", "Bagian Asisten Anastesi saat Anastesi Tidak Hadir" ) );
$settings->addItem ( "bpjs_vk", new SettingsItem ( $db, "smis-provit-bpjs-vk-as-anastesi-thadir-sifat", "Bagi Hasil Asisten Anastesi saat Anastesi Tidak Hadir", $opsi, "select", "Sifat Bagi Hasil Asisten Anastesi saat Dokter Anastesi Tidak Hadir di VK " ) );
$settings->addItem ( "bpjs_vk", new SettingsItem ( $db, "smis-provit-bpjs-vk-team-vk", "Bagian Team VK", "70", "text", "Bagian Team VK" ) );
$settings->addItem ( "bpjs_vk", new SettingsItem ( $db, "smis-provit-bpjs-vk-team-vk-sifat", "Bagi Hasil Team VK", $opsi, "select", "Sifat Bagi Hasil Team VK " ) );
$settings->addItem ( "bpjs_vk", new SettingsItem ( $db, "smis-provit-bpjs-vk-team-perawat", "Bagian Team Perawat di VK", "70", "text", "Team Perawat di VK" ) );
$settings->addItem ( "bpjs_vk", new SettingsItem ( $db, "smis-provit-bpjs-vk-team-perawat-sifat", "Bagi Hasil Team Perawat", $opsi, "select", "Sifat Bagi Hasil Team Perawat di VK" ) );
$settings->addItem ( "bpjs_vk", new SettingsItem ( $db, "smis-provit-bpjs-vk-team-bidan", "Bagian Team Bidan", "70", "text", "Team Perawat di VK" ) );
$settings->addItem ( "bpjs_vk", new SettingsItem ( $db, "smis-provit-bpjs-vk-team-bidan-sifat", "Bagi Hasil Team Bidan", $opsi, "select", "Sifat Bagi Hasil Team Bidan di VK" ) );

$settings->addTabs ( "bpjs_fst", "Fisiotherapy" );
$settings->addItem ( "bpjs_fst", new SettingsItem ( $db, "smis-provit-bpjs-fst-dokter", "Dokter Fisiotherapy", "10", "text", "Bagian Dokter Pengirim di Fisiotherapy" ) );
$settings->addItem ( "bpjs_fst", new SettingsItem ( $db, "smis-provit-bpjs-fst-dokter-sifat", "Bagi Hasil Dokter Pengirim", $opsi, "select", "Sifat Bagi Hasil Dokter Pengirim" ) );
$settings->addItem ( "bpjs_fst", new SettingsItem ( $db, "smis-provit-bpjs-fst-petugas", "Petugas Fisiotherapy", "8.5", "text", "Bagian Petugas Fisiotherapy" ) );
$settings->addItem ( "bpjs_fst", new SettingsItem ( $db, "smis-provit-bpjs-fst-petugas-sifat", "Bagi Hasil Petugas", $opsi, "select", "Sifat Bagi Hasil Dokter Petugas" ) );

$settings->addTabs ( "bpjs_ekg", "EKG" );
$settings->addItem ( "bpjs_ekg", new SettingsItem ( $db, "smis-provit-bpjs-ekg-dokter-pengirim", "Dokter Pengirim EKG", "10", "text", "Bagian Dokter Pengirim EKG" ) );
$settings->addItem ( "bpjs_ekg", new SettingsItem ( $db, "smis-provit-bpjs-ekg-dokter-pengirim-sifat", "Bagi Hasil EKG Dokter Pengirim", $opsi, "select", "Bagi Hasil EKG Dokter Pengirim" ) );
$settings->addItem ( "bpjs_ekg", new SettingsItem ( $db, "smis-provit-bpjs-ekg-dokter-pembaca", "Dokter Pembaca EKG", "20", "text", "Bagian Dokter Pembaca EKG" ) );
$settings->addItem ( "bpjs_ekg", new SettingsItem ( $db, "smis-provit-bpjs-ekg-dokter-pembaca-sifat", "Bagi Hasil EKG Dokter Pembaca", $opsi, "select", "Bagi Hasil EKG Dokter Pembaca" ) );
$settings->addItem ( "bpjs_ekg", new SettingsItem ( $db, "smis-provit-bpjs-ekg-petugas", "Petugas EKG", "5", "text", "Bagian Petugas EKG" ) );
$settings->addItem ( "bpjs_ekg", new SettingsItem ( $db, "smis-provit-bpjs-ekg-petugas-sifat", "Bagi Hasil EKG Petugas EKG", $opsi, "select", "Bagi Hasil Petugas EKG" ) );

$settings->addTabs ( "bpjs_faalparu", "Faal Paru" );
$settings->addItem ( "bpjs_faalparu", new SettingsItem ( $db, "smis-provit-bpjs-fparu-dokter", "Dokter Faal Paru", "10", "text", "Bagian Dokter Faal Paru" ) );
$settings->addItem ( "bpjs_faalparu", new SettingsItem ( $db, "smis-provit-bpjs-fparu-dokter-sifat", "Sifat Bagi Hasil Dokter Faal Paru", $opsi, "select", "Sifat Bagi Hasil Dokter Faal Paru" ) );
$settings->addItem ( "bpjs_faalparu", new SettingsItem ( $db, "smis-provit-bpjs-fparu-asisten", "Asisten Faal Paru", "20", "text", "Bagian Asisten Faal Paru" ) );
$settings->addItem ( "bpjs_faalparu", new SettingsItem ( $db, "smis-provit-bpjs-fparu-asisten-sifat", "Sifat Bagi Hasil Asisten", $opsi, "select", "Sifat Bagi Hasil Asisten Faal Paru" ) );

$settings->addTabs ( "bpjs_endoscopy", "Endoscopy" );
$settings->addItem ( "bpjs_endoscopy", new SettingsItem ( $db, "smis-provit-bpjs-endoscopy-dokter", "Dokter Endoscopy", "10", "text", "Bagian Dokter Endoscopy" ) );
$settings->addItem ( "bpjs_endoscopy", new SettingsItem ( $db, "smis-provit-bpjs-endoscopy-dokter-sifat", "Sifat Bagi Hasil Dokter", $opsi, "select", "Sifat Bagi Hasil Dokter" ) );
$settings->addItem ( "bpjs_endoscopy", new SettingsItem ( $db, "smis-provit-bpjs-endoscopy-asisten", "Asisten Endoscopy", "20", "text", "Bagian Asisten Endoscopy" ) );
$settings->addItem ( "bpjs_endoscopy", new SettingsItem ( $db, "smis-provit-bpjs-endoscopy-asisten-sifat", "Sifat Bagi Hasil Asisten", $opsi, "select", "Sifat Bagi Hasil Asisten" ) );

$settings->addTabs ( "bpjs_bronchoscopy", "Bronchoscopy" );
$settings->addItem ( "bpjs_bronchoscopy", new SettingsItem ( $db, "smis-provit-bpjs-bronchoscopy-dokter", "Dokter Bronchoscopy", "10", "text", "Bagian Dokter Bronchoscopy" ) );
$settings->addItem ( "bpjs_bronchoscopy", new SettingsItem ( $db, "smis-provit-bpjs-bronchoscopy-dokter-sifat", "Sifat Bagi Hasil Dokter", $opsi, "select", "Sifat Bagi Hasil Dokter" ) );
$settings->addItem ( "bpjs_bronchoscopy", new SettingsItem ( $db, "smis-provit-bpjs-bronchoscopy-perawat", "Perawat Bronchoscopy", "20", "text", "Bagian Perawat Bronchoscopy" ) );
$settings->addItem ( "bpjs_bronchoscopy", new SettingsItem ( $db, "smis-provit-bpjs-bronchoscopy-perawat-sifat", "Sifat Bagi Hasil Perawat", $opsi, "select", "Sifat Bagi Hasil Perawat" ) );

$settings->addTabs ( "bpjs_audiometry", "Audiometry" );
$settings->addItem ( "bpjs_audiometry", new SettingsItem ( $db, "smis-provit-bpjs-audiometry-dokter", "Dokter Audiometry", "10", "text", "Bagian Dokter Audiometry" ) );
$settings->addItem ( "bpjs_audiometry", new SettingsItem ( $db, "smis-provit-bpjs-audiometry-dokter-sifat", "Sifat Bagi Hasil Dokter", $opsi, "select", "Sifat Bagi Hasil Dokter" ) );
$settings->addItem ( "bpjs_audiometry", new SettingsItem ( $db, "smis-provit-bpjs-audiometry-perawat", "Perawat Audiometry", "20", "text", "Bagian Perawat Audiometry" ) );
$settings->addItem ( "bpjs_audiometry", new SettingsItem ( $db, "smis-provit-bpjs-audiometry-perawat-sifat", "Sifat Bagi Hasil Perawat", $opsi, "select", "Sifat Bagi Hasil Perawat" ) );

$settings->addTabs ( "bpjs_spirometry", "Spirometry" );
$settings->addItem ( "bpjs_spirometry", new SettingsItem ( $db, "smis-provit-bpjs-spirometry-dokter", "Dokter Spirometry", "10", "text", "Bagian Dokter Spirometry" ) );
$settings->addItem ( "bpjs_spirometry", new SettingsItem ( $db, "smis-provit-bpjs-spirometry-dokter-sifat", "Sifat Bagi Hasil Dokter", $opsi, "select", "Sifat Bagi Hasil Dokter" ) );
$settings->addItem ( "bpjs_spirometry", new SettingsItem ( $db, "smis-provit-bpjs-spirometry-perawat", "Perawat Spirometry", "20", "text", "Bagian Perawat Spirometry" ) );
$settings->addItem ( "bpjs_spirometry", new SettingsItem ( $db, "smis-provit-bpjs-spirometry-perawat-sifat", "Sifat Bagi Hasil Perawat", $opsi, "select", "Sifat Bagi Hasil Perawat" ) );

$settings->addTabs ( "bpjs_laboratory", "Laboratory" );
$settings->addItem ( "bpjs_laboratory", new SettingsItem ( $db, "smis-provit-bpjs-laboratory-dokter-pengirim", "Dokter Pengirim Laboratory", "10", "text", "Bagian Dokter Pengirim di Laboratory" ) );
$settings->addItem ( "bpjs_laboratory", new SettingsItem ( $db, "smis-provit-bpjs-laboratory-dokter-pengirim-sifat", "Sifat Bagi Hasil Dokter Pengirim", $opsi, "select", "Sifat Bagi Hasil Dokter Pengirim" ) );
$settings->addItem ( "bpjs_laboratory", new SettingsItem ( $db, "smis-provit-bpjs-laboratory-dokter-konsultan", "Dokter Konsultan Laboratory", "10", "text", "Bagian Dokter Konsultan di Laboratory" ) );
$settings->addItem ( "bpjs_laboratory", new SettingsItem ( $db, "smis-provit-bpjs-laboratory-dokter-konsultan-sifat", "Sifat Bagi Hasil Dokter Konsultan", $opsi, "select", "Sifat Bagi Hasil Dokter Konsultan" ) );
$settings->addItem ( "bpjs_laboratory", new SettingsItem ( $db, "smis-provit-bpjs-laboratory-petugas", "Petugas Laboratory", "10", "text", "Bagian Petugas di Laboratory" ) );
$settings->addItem ( "bpjs_laboratory", new SettingsItem ( $db, "smis-provit-bpjs-laboratory-petugas-sifat", "Sifat Bagi Hasil Petugas Laboratory", $opsi, "select", "Sifat Bagi Petugas Pengirim" ) );
$settings->addItem ( "bpjs_laboratory", new SettingsItem ( $db, "smis-provit-bpjs-laboratory-grup", "Grup", "5", "text", "Bagian Grup di Laboratory" ) );
$settings->addItem ( "bpjs_laboratory", new SettingsItem ( $db, "smis-provit-bpjs-laboratory-grup-sifat", "Sifat Bagi Hasil Grup ", $opsi, "select", "Sifat Bagi Grup Pengirim" ) );

$settings->addTabs ( "bpjs_radiology", "Radiology" );
$settings->addItem ( "bpjs_radiology", new SettingsItem ( $db, "smis-provit-bpjs-radiology-dokter-pengirim-plain", "Dokter Pengirim Plain Foto", "10", "text", "Bagian Dokter Pengirim ke Radiology untuk Plain Foto" ) );
$settings->addItem ( "bpjs_radiology", new SettingsItem ( $db, "smis-provit-bpjs-radiology-dokter-pengirim-plain-sifat", "Sifat Bagi Hasil Dokter Pengirim Plain Foto", $opsi, "select", "Sifat Bagi Hasil Dokter Pengirim PLain Foto" ) );
$settings->addItem ( "bpjs_radiology", new SettingsItem ( $db, "smis-provit-bpjs-radiology-dokter-konsultan-plain", "Dokter Konsultan Plain Foto", "10", "text", "Bagian Dokter KOnsultan untuk Plain Foto" ) );
$settings->addItem ( "bpjs_radiology", new SettingsItem ( $db, "smis-provit-bpjs-radiology-dokter-konsultan-plain-sifat", "Sifat Bagi Hasil Dokter Konsultan Plain Foto", $opsi, "select", "Sifat Bagi Hasil Dokter Konsultan PLain Foto" ) );
$settings->addItem ( "bpjs_radiology", new SettingsItem ( $db, "smis-provit-bpjs-radiology-petugas-plain", "Petugas Plain Foto", "10", "text", "Bagian Petugas untuk Plain Foto" ) );
$settings->addItem ( "bpjs_radiology", new SettingsItem ( $db, "smis-provit-bpjs-radiology-dokter-petugas-plain-sifat", "Sifat Bagi Hasil Petugas Plain Foto", $opsi, "select", "Sifat Bagi Hasil Petugas PLain Foto" ) );

$settings->addItem ( "bpjs_radiology", new SettingsItem ( $db, "smis-provit-bpjs-radiology-dokter-pengirim-contrast", "Dokter Pengirim Foto Kontras", "10", "text", "Bagian Dokter Pengirim ke Radiology untuk Foto Kontras" ) );
$settings->addItem ( "bpjs_radiology", new SettingsItem ( $db, "smis-provit-bpjs-radiology-dokter-pengirim-contrast-sifat", "Sifat Bagi Hasil Dokter Pengirim Foto Kontras", $opsi, "select", "Sifat Bagi Hasil Dokter Pengirim Foto Kontras" ) );
$settings->addItem ( "bpjs_radiology", new SettingsItem ( $db, "smis-provit-bpjs-radiology-dokter-konsultan-contrast", "Dokter Konsultan Foto Kontras", "10", "text", "Bagian Dokter KOnsultan untuk Foto Kontras" ) );
$settings->addItem ( "bpjs_radiology", new SettingsItem ( $db, "smis-provit-bpjs-radiology-dokter-konsultan-contrast-sifat", "Sifat Bagi Hasil Dokter Konsultan Foto Kontras", $opsi, "select", "Sifat Bagi Hasil Dokter Konsultan Foto Kontras" ) );
$settings->addItem ( "bpjs_radiology", new SettingsItem ( $db, "smis-provit-bpjs-radiology-petugas-contrast", "Petugas Foto Kontras", "10", "text", "Bagian Petugas untuk Foto Kontras" ) );
$settings->addItem ( "bpjs_radiology", new SettingsItem ( $db, "smis-provit-bpjs-radiology-petugas-contrast-sifat", "Sifat Bagi Hasil Petugas Foto Kontras", $opsi, "select", "Sifat Bagi Hasil Petugas Foto Kontras" ) );

$settings->addItem ( "bpjs_radiology", new SettingsItem ( $db, "smis-provit-bpjs-radiology-dokter-pengirim-usg", "Dokter Pengirim Ultrasonography", "10", "text", "Bagian Dokter Pengirim ke Radiology untuk USG" ) );
$settings->addItem ( "bpjs_radiology", new SettingsItem ( $db, "smis-provit-bpjs-radiology-dokter-pengirim-usg-sifat", "Sifat Bagi Hasil Dokter Pengirim USG", $opsi, "select", "Sifat Bagi Hasil Dokter Pengirim USG" ) );
$settings->addItem ( "bpjs_radiology", new SettingsItem ( $db, "smis-provit-bpjs-radiology-dokter-konsultan-usg", "Dokter Konsultan Ultrasonography", "10", "text", "Bagian Dokter KOnsultan untuk USG" ) );
$settings->addItem ( "bpjs_radiology", new SettingsItem ( $db, "smis-provit-bpjs-radiology-dokter-konsultan-usg-sifat", "Sifat Bagi Hasil Dokter Konsultan USG", $opsi, "select", "Sifat Bagi Hasil Dokter Konsultan USG" ) );
$settings->addItem ( "bpjs_radiology", new SettingsItem ( $db, "smis-provit-bpjs-radiology-petugas-usg", "Petugas Ultrasonography", "10", "text", "Bagian Petugas untuk USG" ) );
$settings->addItem ( "bpjs_radiology", new SettingsItem ( $db, "smis-provit-bpjs-radiology-petugas-usg-sifat", "Sifat Bagi Hasil Petugas USG", $opsi, "select", "Sifat Bagi Hasil Petugas USG" ) );

$settings->addItem ( "bpjs_radiology", new SettingsItem ( $db, "smis-provit-bpjs-radiology-dokter-pengirim-ctscan", "Dokter Pengirim CT Scan", "10", "text", "Bagian Dokter Pengirim ke Radiology untuk CT Scan" ) );
$settings->addItem ( "bpjs_radiology", new SettingsItem ( $db, "smis-provit-bpjs-radiology-dokter-pengirim-ctscan-sifat", "Sifat Bagi Hasil Dokter Pengirim CT Scan", $opsi, "select", "Sifat Bagi Hasil Dokter Pengirim CT Scan" ) );
$settings->addItem ( "bpjs_radiology", new SettingsItem ( $db, "smis-provit-bpjs-radiology-dokter-konsultan-ctscan", "Dokter Konsultan CT Scan", "10", "text", "Bagian Dokter KOnsultan untuk CT Scan" ) );
$settings->addItem ( "bpjs_radiology", new SettingsItem ( $db, "smis-provit-bpjs-radiology-dokter-konsultan-ctscan-sifat", "Sifat Bagi Hasil Dokter Konsultan CT Scan", $opsi, "select", "Sifat Bagi Hasil Dokter Konsultan CT Scan" ) );
$settings->addItem ( "bpjs_radiology", new SettingsItem ( $db, "smis-provit-bpjs-radiology-petugas-ctscan", "Petugas CT Scan", "10", "text", "Bagian Petugas untuk CT Scan" ) );
$settings->addItem ( "bpjs_radiology", new SettingsItem ( $db, "smis-provit-bpjs-radiology-petugas-ctscan-sifat", "Sifat Bagi Hasil Petugas CT Scan", $opsi, "select", "Sifat Bagi Hasil Petugas CT Scan" ) );

$settings->addTabs ( "bpjs_igd", "IGD" );
$settings->addItem ( "bpjs_igd", new SettingsItem ( $db, "smis-provit-bpjs-igd-dokter", "Dokter Jaga IGD", "10", "text", "Bagian Doker Jaga IGD" ) );
$settings->addItem ( "bpjs_igd", new SettingsItem ( $db, "smis-provit-bpjs-igd-dokter-sifat", "Sifat Pembagian Dokter Jaga", $opsi, "select", "Sifat Pembagian Dokter Jaga" ) );
$settings->addItem ( "bpjs_igd", new SettingsItem ( $db, "smis-provit-bpjs-igd-perawat", "Perawat IGD", "0", "text", "Bagian Perawat IGD" ) );
$settings->addItem ( "bpjs_igd", new SettingsItem ( $db, "smis-provit-bpjs-igd-perawat-sifat", "Sifat Pembagian Perawat IGD", $opsi, "select", "Sifat Pembagian Perawat IGD" ) );


$settings->addTabs ( "bpjs_apotek", "Apotek" );
$settings->addItem ( "bpjs_apotek", new SettingsItem ( $db, "smis-provit-bpjs-apotek-dokter-pengirim", "Dokter Pengirim dari Apotek", "10", "text", "Provit Sharing Bagian Dokter yang Mengirim Pasien ke Apotek untuik beli obat" ) );
$settings->addItem ( "bpjs_apotek", new SettingsItem ( $db, "smis-provit-bpjs-dokter-obat-return", "Return Obat", "90", "text", "Return Obat Apotek sekian % dari harga" ) );

$settings->addTabs ( "bpjs_pajak", "Pajak" );
$settings->addItem ( "bpjs_pajak", new SettingsItem ( $db, "smis-rs-persen-pajak-gaji", "Persentase Pajak", "2", "text", "Persentase Pemotongan Pajak" ) );

$response = $settings->init ();
?>