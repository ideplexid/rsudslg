<?php
require_once 'smis-framework/smis/api/SettingsBuilder.php';
require_once 'smis-base/smis-include-service-consumer.php';

global $db;
$service = new ServiceConsumer ( $db, "get_list_radiology" );
$service->setCached(true,"get_list_radiology");
$service->execute ();
$array = $service->getContent ();

$slug = "smis_rad_harga";
$settings = new SettingsBuilder ( $db, "harga", "manajemen", "harga_radiology", "" );
$settings->setPrefix("harga_radiology");
$settings->setSingle ( true, $slug );
$settings->setOneVariable(true, $slug);


$dbtable = new DBTable ( $db, "smis_mjm_kelas" );
$dbtable->setShowAll ( true );
$data = $dbtable->view ( "", "0" );
$select = new SelectAdapter ( "nama", "slug" );
$kelas = $select->getContent ( $data ['data'] );

foreach ( $kelas as $element => $value ) {
	$k = $value ['name'];
	$v = $value ['value'];
	$settings->addTabs ( "harga_radiology".$v, $k,"fa fa-money" );
    if($settings->isGroup("harga_radiology".$v)){
        foreach ( $array as $ak => $av ) {
            $setitem = new SettingsItem ( $db, $v . "_" . $ak, $av, "0", "money", "", true, $slug );
            $settings->addItem ( "harga_radiology".$v, $setitem );
        }
    }
}
$settings->setPartialLoad(true);
$settings->init ();
?>