<?php
    global $db;
    require_once("smis-base/smis-include-duplicate.php");

    $header = array("Jenis Tindakan", "Biaya");
    $uitable = new Table($header, "Tarif Farmasi", null, true);
    $uitable->setName("tindakan_farmasi");
    $uitable->setReloadButtonEnable(false);
    $uitable->setPrintButtonEnable(false);

    if (isset ( $_POST ['command'] )) {
        $adapter = new SimpleAdapter ();
        $adapter->add("Jenis Tindakan", "nama_tindakan");
        $adapter->add("Biaya", "tarif","money Rp");
        $dbtable = new DBTable($db, "smis_mjm_tindakan_farmasi");
        $dbresponder = new DBResponder($dbtable, $uitable, $adapter);
        $data = $dbresponder->command($_POST['command']);
        echo json_encode($data);
        return;
    }

    $uitable->addModal("id", "hidden", "", "");
    $uitable->addModal("nama_tindakan", "text", "Jenis Tindakan", "");
    $uitable->addModal("tarif", "money", "Biaya", "");

    $modal = $uitable->getModal();
    $modal->setTitle("Tarif Farmasi");
    $modal->setComponentSize(Modal::$MEDIUM);

    echo $uitable->getHtml();
    echo $modal->getHtml();
    echo addJS ("framework/smis/js/table_action.js");
?>
<script type="text/javascript">
    var tindakan_farmasi;
    $(document).ready(function() {
        var columns = new Array("id", "nama_tindakan", "tarif");
        tindakan_farmasi = new TableAction(
            "tindakan_farmasi",
            "manajemen",
            "tindakan_farmasi",
            columns
        );
        tindakan_farmasi.view();
    });
</script>
