<?php
global $db;
require_once ("smis-base/smis-include-service-consumer.php");
require_once 'smis-libs-hrd/EmployeeResponder.php';
require_once "smis-base/smis-include-duplicate.php";
require_once 'manajemen/class/responder/ManajemenTarifResponder.php';
$duplicate=getSettings($db,"manajer-duplicate-tarif-endoscopy","0");
$uitable = new TableSynchronous ( array ('Kelas','Tarif Lama','Tarif Sekarang','Rencana Tarif Baru' ,'Keterangan' ), "Endoscopy", NULL, true );
$uitable->setName ( "tarif_endoscopy" );
$uitable->setLoopDuplicateButtonEnable ($duplicate!="0");
$uitable->setDuplicateButton($duplicate!="0");
$uitable->setSynchronizeButton(false);
$uitable->setReloadButtonEnable(false);
$uitable->setPrintButtonEnable(false);

if (isset ( $_POST ['command'] )) {
	$adapter = new SynchronousViewAdapter ();
	$adapter->add ( "Kelas", "kelas", "unslug" );
	$adapter->add ( "Tarif Sekarang", "tarif", "money Rp." );
	$adapter->add ( "Tarif Lama", "tarif_lama", "money Rp." );
	$adapter->add ( "Rencana Tarif Baru", "tarif_baru", "money Rp." );
	$adapter->add ( "Keterangan", "keterangan" );
	$dbtable = new DBTable ( $db, "smis_mjm_endoscopy" );
	$dbtable->setFetchMethode ( DBTable::$ARRAY_FETCH );
	$dbres = new ManajemenTarifResponder ( $dbtable, $uitable, $adapter,$duplicate );
    $dbres->addUpdateColumn("tarif");   
	$data = $dbres->command ( $_POST ['command'] );
	echo json_encode ( $data );
	return;
}
$lama=new Button("", "", "Backup");
$lama->setIsButton(Button::$ICONIC_TEXT);
$lama->setIcon(" fa fa-backward");
$lama->setClass(" btn-primary ");
$lama->setAction("tarif_endoscopy.backup()");
$restore=new Button("", "", "Restore");
$restore->setIsButton(Button::$ICONIC_TEXT);
$restore->setClass(" btn-primary ");
$restore->setIcon(" fa fa-forward");
$restore->setAction("tarif_endoscopy.restore()");
$baru=new Button("", "", "Terapkan");
$baru->setIsButton(Button::$ICONIC_TEXT);
$baru->setClass(" btn-primary ");
$baru->setIcon(" fa fa-stop");
$baru->setAction("tarif_endoscopy.terapkan()");
$uitable->addFooterButton($lama);
$uitable->addFooterButton($restore);
$uitable->addFooterButton($baru);

$dbtable = new DBTable ( $db, "smis_mjm_kelas" );
$dbtable->setShowAll ( true );
$data = $dbtable->view ( "", "0" );
$select = new SelectAdapter ( "nama", "slug" );
$kelas = $select->getContent ( $data ['data'] );

$uitable->addModal ( "id", "hidden", "", "" );
$uitable->addModal ( "kelas", "select", "Kelas", $kelas );
$uitable->addModal ( "tarif_lama", "money", "Tarif Lama", "","y",NULL,true );
$uitable->addModal ( "tarif", "money", "Tarif Sekarang", "" );
$uitable->addModal ( "tarif_baru", "money", "Rencana Tarif Baru", "" );
$uitable->addModal ( "keterangan", "textarea", "Keterangan", "" );
$modal = $uitable->getModal ();
$modal->setTitle ( "Tarif Endoscopy" );

echo $uitable->getHtml ();
echo $modal->getHtml ();
echo addJS ( "base-js/smis-base-loading.js");
echo addJS ( "framework/smis/js/table_action.js" );
echo addJS ( "manajemen/resource/js/manajemen_tarif_action.js",false );

?>
<script type="text/javascript">
var tarif_endoscopy;
$(document).ready(function(){
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
	var column=new Array('id','kelas',"tarif",'keterangan','tarif_lama','tarif_baru');
	tarif_endoscopy=new ManajemenTarifAction("tarif_endoscopy","manajemen","tarif_endoscopy",column);	
	tarif_endoscopy.view();	
	tarif_endoscopy.setDuplicateNameView("keterangan");
});
</script>
