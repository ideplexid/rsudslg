<?php
/**
 * this is handling about element where used for patient management
 * @author goblooge
 *
 */
global $db;
require_once "smis-base/smis-include-duplicate.php";
require_once 'manajemen/class/responder/ManajemenTarifResponder.php';
$duplicate=getSettings($db,"manajer-duplicate-tarif-ambulan","0");
$header=array ("Jenis Mobil","Durasi Jam",'Durasi Hari' );
$uitable = new TableSynchronous ( $header, "Tarif Ambulan", NULL, true );
$uitable->setName ( "sewa_ambulan" );
$uitable->setLoopDuplicateButtonEnable ($duplicate!="0");
$uitable->setDuplicateButton($duplicate!="0");
$uitable->setSynchronizeButton(false);
$uitable->setReloadButtonEnable(false);
$uitable->setPrintButtonEnable(false);

if (isset ( $_POST ['command'] )) {
    $adapter = new SynchronousViewAdapter ();
    $adapter->add ( "Jenis Mobil", "jenis_mobil" );
	$adapter->add ( "Durasi Jam", "durasi_jam","money Rp." );
    $adapter->add ( "Durasi Hari", "durasi_hari", "money Rp." ); 
	
    $dbtable=new DBTable($db,"smis_mjm_sewa_ambulan");
	$dbtable->setFetchMethode ( DBTable::$ARRAY_FETCH );
	$dbres = new ManajemenTarifResponder ( $dbtable, $uitable, $adapter,$duplicate );
	$data = $dbres->command ( $_POST ['command'] );
	echo json_encode ( $data );
	return;
}

$lama=new Button("", "", "Backup");
$lama->setIsButton(Button::$ICONIC_TEXT);
$lama->setIcon(" fa fa-backward");
$lama->setClass(" btn-primary ");
$lama->setAction("sewa_ambulan.backup()");
$restore=new Button("", "", "Restore");
$restore->setIsButton(Button::$ICONIC_TEXT);
$restore->setClass(" btn-primary ");
$restore->setIcon(" fa fa-forward");
$restore->setAction("sewa_ambulan.restore()");
$baru=new Button("", "", "Terapkan");
$baru->setIsButton(Button::$ICONIC_TEXT);
$baru->setClass(" btn-primary ");
$baru->setIcon(" fa fa-stop");
$baru->setAction("sewa_ambulan.terapkan()");
$uitable->addFooterButton($lama);
$uitable->addFooterButton($restore);
$uitable->addFooterButton($baru);

$dbtable = new DBTable ( $db, "smis_mjm_kelas" );
$dbtable->setShowAll ( true );
$data = $dbtable->view ( "", "0" );
$select = new SelectAdapter ( "nama", "slug" );
$kelas = $select->getContent ( $data ['data'] );


$dbtabel = new DBTable($db,"smis_mjm_jenis_mobil");
$dbtabel ->setShowAll(true);
$dtc = $dbtabel ->view("","0");
$list = $dtc['data'];

$selectadapter = new SelectAdapter("jenis_mobil","id");
$jenis_mobil = $selectadapter ->getContent($list);
$jenis_mobil[] = array("name"=>"","value"=>"","default"=>"");

$uitable->addModal ( "id", "hidden", "", "" );
$uitable->addModal ( "id_jenis_mobil", "select", "Jenis Mobil", $jenis_mobil );
$uitable->addModal ( "durasi_jam", "money", "Durasi Jam", "" );
$uitable->addModal ( "durasi_hari", "money", "Durasi Hari", "" );


$modal = $uitable->getModal ();
$modal->setTitle ( "Tarif Ambulan" );
$modal->setComponentSize(Modal::$MEDIUM);

echo $uitable->getHtml ();
echo $modal->getHtml ();
echo addJS ( "base-js/smis-base-loading.js");
echo addJS ( "framework/smis/js/table_action.js" );
echo addJS ( "manajemen/resource/js/manajemen_tarif_action.js",false );


?>
<script type="text/javascript">
var sewa_ambulan;
$(document).ready(function(){
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
	var column=new Array("id","id_jenis_mobil","durasi_jam","durasi_hari");
	sewa_ambulan=new ManajemenTarifAction("sewa_ambulan","manajemen","sewa_ambulan",column);
    sewa_ambulan.view();
    sewa_ambulan.addSaveData = function(x){
        x['jenis_mobil'] = $("#sewa_ambulan_id_jenis_mobil option:selected").text();
        return x;
    }

    
});
</script>
