<?php
global $db;
require_once ("smis-base/smis-include-service-consumer.php");
require_once 'smis-libs-hrd/EmployeeResponder.php';
require_once "smis-base/smis-include-duplicate.php";
require_once 'manajemen/class/responder/ManajemenTarifResponder.php';

$uitable = new TableSynchronous ( array ('Kelas','Jenis Tindakan','Biaya'),"Tarif Gizi", NULL, true );
$uitable->setName ( "tarif_gizi" );
$uitable->setLoopDuplicateButtonEnable (false);
$uitable->setDuplicateButton(false);
$uitable->setSynchronizeButton(false);
$uitable->setReloadButtonEnable(false);
$uitable->setPrintButtonEnable(false);


if (isset ( $_POST ['command'] )) {
	$adapter = new SynchronousViewAdapter ();
	$adapter->add ( "Kelas", "kelas", "unslug" );
	$adapter->add ( "Jenis Tindakan", "jenis_tindakan");
	$adapter->add ( "Biaya", "biaya", "money Rp." );
	$dbtable = new DBTable ( $db, "smis_mjm_gizi" );
	$dbtable->setFetchMethode ( DBTable::$ARRAY_FETCH );
	$dbres = new ManajemenTarifResponder ( $dbtable, $uitable, $adapter,$duplicate );
    $dbres->addUpdateColumn("tarif_gizi");   
	$data = $dbres->command ( $_POST ['command'] );
	echo json_encode ( $data );
	return;
}

$dbtable = new DBTable ( $db, "smis_mjm_kelas" );
$dbtable->setShowAll ( true );
$data = $dbtable->view ( "", "0" );
$select = new SelectAdapter ( "nama", "slug" );
$kelas = $select->getContent ( $data ['data'] );

$uitable->addModal ( "id", "hidden", "", "" );
$uitable->addModal ( "kelas", "select", "Kelas", $kelas );
$uitable->addModal ( "jenis_tindakan", "text", "Jenis Tindakan", "" );
$uitable->addModal ( "biaya", "money", "Biaya", "","y" );
$modal = $uitable->getModal ();
$modal->setTitle ( "Tarif Gizi" );

echo $uitable->getHtml ();
echo $modal->getHtml ();
echo addJS ( "base-js/smis-base-loading.js");
echo addJS ( "framework/smis/js/table_action.js" );
echo addJS ( "manajemen/resource/js/manajemen_tarif_action.js",false );

?>
<script type="text/javascript">
var tarif_gizi;
$(document).ready(function(){
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
	var column=new Array('id','kelas',"biaya",'jenis_tindakan');
	tarif_gizi=new ManajemenTarifAction("tarif_gizi","manajemen","tarif_gizi",column);	
	tarif_gizi.view();	
});
</script>
