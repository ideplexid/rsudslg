<?php
/**
 * this is handling about element where used for patient management
 * @author goblooge
 *
 */
global $db;
require_once "smis-base/smis-include-duplicate.php";
require_once 'manajemen/class/responder/ManajemenTarifResponder.php';
$duplicate=getSettings($db,"manajer-duplicate-tarif-ambulan","0");
$header=array ("Jenis Mobil","Jenis BBM","5 KM Pertama",'KM Berikutnya',"Harga/Liter","Kelas" );
$uitable = new TableSynchronous ( $header, "Tarif Ambulan", NULL, true );
$uitable->setName ( "tarif_ambulan_bbm" );
$uitable->setLoopDuplicateButtonEnable ($duplicate!="0");
$uitable->setDuplicateButton($duplicate!="0");
$uitable->setSynchronizeButton(false);
$uitable->setReloadButtonEnable(false);
$uitable->setPrintButtonEnable(false);

if (isset ( $_POST ['command'] )) {
    $adapter = new SynchronousViewAdapter ();
    $adapter->add ( "Jenis Mobil", "jenis_mobil" );
    $adapter->add ( "Jenis BBM", "jenis_bbm" );
	$adapter->add ( "5 KM Pertama", "km_pertama","back Ltr" );
    $adapter->add ( "KM Berikutnya", "km_berikutnya", "back Ltr" ); 
    $adapter->add ( "Harga/Liter", "harga_liter", "money Rp." ); 
    $adapter->add ( "Kelas", "kelas", "unslug" );
	
	
    $dbtable=new DBTable($db,"smis_mjm_ambulan_bbm");
	$dbtable->setFetchMethode ( DBTable::$ARRAY_FETCH );
	$dbres = new ManajemenTarifResponder ( $dbtable, $uitable, $adapter,$duplicate );
    //$dbres->addUpdateColumn("biaya"); 
    //$dbres->addUpdateColumn("jasa_supir"); 
    //$dbres->addUpdateColumn("jasa_perawat"); 
	$data = $dbres->command ( $_POST ['command'] );
	echo json_encode ( $data );
	return;
}

$lama=new Button("", "", "Backup");
$lama->setIsButton(Button::$ICONIC_TEXT);
$lama->setIcon(" fa fa-backward");
$lama->setClass(" btn-primary ");
$lama->setAction("tarif_ambulan_bbm.backup()");
$restore=new Button("", "", "Restore");
$restore->setIsButton(Button::$ICONIC_TEXT);
$restore->setClass(" btn-primary ");
$restore->setIcon(" fa fa-forward");
$restore->setAction("tarif_ambulan_bbm.restore()");
$baru=new Button("", "", "Terapkan");
$baru->setIsButton(Button::$ICONIC_TEXT);
$baru->setClass(" btn-primary ");
$baru->setIcon(" fa fa-stop");
$baru->setAction("tarif_ambulan_bbm.terapkan()");
$uitable->addFooterButton($lama);
$uitable->addFooterButton($restore);
$uitable->addFooterButton($baru);

$dbtable = new DBTable ( $db, "smis_mjm_kelas" );
$dbtable->setShowAll ( true );
$data = $dbtable->view ( "", "0" );
$select = new SelectAdapter ( "nama", "slug" );
$kelas = $select->getContent ( $data ['data'] );


$dbtabel = new DBTable($db,"smis_mjm_jenis_mobil");
$dbtabel ->setShowAll(true);
$dtc = $dbtabel ->view("","0");
$list = $dtc['data'];
$selectadapter = new SelectAdapter("jenis_mobil","id");
$jenis_mobil = $selectadapter ->getContent($list);
$jenis_mobil[] = array("name"=>"","value"=>"","default"=>"");

$dbtabel = new DBTable($db,"smis_mjm_bbm");
$dbtabel ->setShowAll(true);
$dtc = $dbtabel ->view("","0");
$list = $dtc['data'];
$selectadapter = new SelectAdapter("nama","nama");
$jenis_bbm = $selectadapter ->getContent($list);
$jenis_bbm[] = array("name"=>"","value"=>"","default"=>"");

$uitable->addModal ( "id", "hidden", "", "" );
$uitable->addModal ( "id_jenis_mobil", "select", "Jenis Mobil", $jenis_mobil );
$uitable->addModal ( "jenis_bbm", "select", "Jenis BBM", $jenis_bbm );
$uitable->addModal ( "km_pertama", "text", "Keb BBM , 5 KM Pertama ", "" );
$uitable->addModal ( "km_berikutnya", "text", "Keb BBM , KM Berikutnya ", "" );
$uitable->addModal ( "harga_liter", "money", "Harga/Liter", "" );
$uitable->addModal ( "kelas", "select", "Kelas", $kelas );


$modal = $uitable->getModal ();
$modal->setTitle ( "Tarif Ambulan" );
$modal->setComponentSize(Modal::$MEDIUM);

echo $uitable->getHtml ();
echo $modal->getHtml ();
echo addJS ( "base-js/smis-base-loading.js");
echo addJS ( "framework/smis/js/table_action.js" );
echo addJS ( "manajemen/resource/js/manajemen_tarif_action.js",false );
?>

<script type="text/javascript">
var tarif_ambulan_bbm;
//var employee;
$(document).ready(function(){
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
	var column=new Array("id","id_jenis_mobil","jenis_bbm","km_pertama","km_berikutnya","harga_liter","kelas");
	tarif_ambulan_bbm=new ManajemenTarifAction("tarif_ambulan_bbm","manajemen","tarif_ambulan_bbm",column);
    tarif_ambulan_bbm.view();
    tarif_ambulan_bbm.addSaveData = function(x){
        x['jenis_mobil'] = $("#tarif_ambulan_bbm_id_jenis_mobil option:selected").text();
        return x;
    };
});
</script>
