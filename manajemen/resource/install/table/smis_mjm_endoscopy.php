<?php  
$dbcreator=new DBCreator($wpdb,"smis_mjm_endoscopy");
$dbcreator->addColumn("kelas","varchar(16)",DBCreator::$SIGN_NONE,false,DBCreator::$DEFAULT_NONE,false,DBCreator::$ON_UPDATE_NONE,"");
$dbcreator->addColumn("tarif","int(11)",DBCreator::$SIGN_NONE,false,DBCreator::$DEFAULT_NONE,false,DBCreator::$ON_UPDATE_NONE,"");
$dbcreator->addColumn("tarif_lama","int(11)",DBCreator::$SIGN_NONE,false,DBCreator::$DEFAULT_NONE,false,DBCreator::$ON_UPDATE_NONE,"");
$dbcreator->addColumn("tarif_baru","int(11)",DBCreator::$SIGN_NONE,false,DBCreator::$DEFAULT_NONE,false,DBCreator::$ON_UPDATE_NONE,"");
$dbcreator->addColumn("keterangan","text",DBCreator::$SIGN_NONE,false,DBCreator::$DEFAULT_NONE,false,DBCreator::$ON_UPDATE_NONE,"");
$dbcreator->initialize();
?>