<?php
    $dbcreator = new DBCreator($wpdb, "smis_mjm_tindakan_farmasi");
    $dbcreator->addColumn("nama_tindakan", "varchar(128)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
    $dbcreator->addColumn("tarif","double", DBCreator::$SIGN_NONE, false,DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE,"");
    $dbcreator->initialize();
?>