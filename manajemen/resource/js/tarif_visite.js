var tarif_visite;
var dokter_visite;
//var employee;
$(document).ready(function(){
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
	var column=new Array('id','nama_dokter',
			'jabatan','id_dokter','kelas',"tarif",
            'tarif_lama','tarif_baru',"nama_visite",
            'jaspel','jaspel_persen',"rs","rs_persen"
            );
	tarif_visite=new ManajemenTarifAction("tarif_visite","manajemen","tarif_visite",column);
	
	tarif_visite.view();

	dokter_visite=new TableAction("dokter_visite","manajemen","tarif_visite",new Array());
	dokter_visite.setSuperCommand("dokter_visite");
	dokter_visite.selected=function(json){
		var nama=json.nama;
		var nip=json.id;		
		var jabatan=json.slug;
		$("#tarif_visite_nama_dokter").val(nama);
		$("#tarif_visite_id_dokter").val(nip);
		$("#tarif_visite_jabatan").val(jabatan);
	};
    tarif_visite.setDuplicateNameView("nama_visite");
    
    $("#tarif_visite_jaspel, #tarif_visite_rs").on("keyup",function(){
        var id = $(this).attr("id");
        var x = id.replace("tarif_visite_", "");
        var money = getMoney("#"+id);
        var total = getMoney("#tarif_visite_tarif");
        var persen = money*100/total;
        $("#"+id+"_persen").val(persen.toFixed(3));
    });

    $("#tarif_visite_jaspel_persen  ,#tarif_visite_rs_persen").on("keyup",function(){
        var id = $(this).attr("id");
        var x = id.replace("_persen", "");
        var total  = getMoney("#tarif_visite_tarif");
        var persen = $(this).val();
        var money = total*persen/100;
        setMoney("#"+x,money.toFixed(2));
    });
	
});