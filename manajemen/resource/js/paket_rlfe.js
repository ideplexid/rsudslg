var paket_rlfe;
var paket_array;
$(document).ready(function(){
    var paket_array=$.parseJSON($("#list_nama_paket").val());
    $('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
    var column = new Array(
                    'id','nama','keterangan',"kelas","tarif_manual",
                    "total_konsul","total_periksa","bagi_konsul","bagi_periksa",
                    'nominal_jaspel','nominal_rs','persen_jaspel','persen_rs'
                );
    $.each(paket_array,function(index,value) {
        if(value!=null){
            $.each(value,function(s_value,s_index){
                column.push(s_value);
            });
        } 
    });
    paket_rlfe=new TableAction("paket_rlfe","manajemen","paket_rlfe",column);
    $.each(paket_array,function(index,value) {
        if(value!=null){
            $.each(value,function(s_index,s_value){
                paket_rlfe.addJSONColumn(s_index,index);
            });
        }
        
    });
    paket_rlfe.view();

    $("#paket_rlfe_nominal_jaspel , #paket_rlfe_nominal_rs ").on("keyup",function(){
        var tarif = getMoney("#paket_rlfe_tarif_manual");
        var nominal_jaspel = getMoney("#paket_rlfe_nominal_jaspel");
        var nominal_rs = getMoney("#paket_rlfe_nominal_rs");

        var persen_rs = nominal_rs*100/tarif;
        var persen_jaspel = nominal_jaspel*100/tarif;
        
        $("#paket_rlfe_persen_jaspel").val(persen_jaspel.toFixed(3));
        $("#paket_rlfe_persen_rs").val(persen_rs.toFixed(3));
    });

    $("#paket_rlfe_tarif_manual, #paket_rlfe_persen_jaspel , #paket_rlfe_persen_rs").on("keyup",function(){
        var tarif = getMoney("#paket_rlfe_tarif_manual");

        var persen_rs = Number($("#paket_rlfe_persen_rs").val());
        var persen_jaspel = Number($("#paket_rlfe_persen_jaspel").val());
        
        var nominal_jaspel = persen_jaspel*tarif/100;
        var nominal_rs = persen_rs*tarif/100;
        
        setMoney("#paket_rlfe_nominal_jaspel",nominal_jaspel);
        setMoney("#paket_rlfe_nominal_rs",nominal_rs);
    });
	
});