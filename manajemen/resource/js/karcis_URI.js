$(document).ready(function(){
    setTimeout(function(){
        var x = karcis['partial_itemid']['URI'];
        $.each(x , function( index, value ) {
            var pos_persen = value.indexOf("persen");
            var pos_nominal = value.indexOf("nominal");
            if(pos_persen==-1 && pos_nominal==-1){
                var n = value.split("-")
                var poli = n[n.length - 1];
                var basename = value.replace(poli,'');

                var id_basic_tarif = value;
                var id_nominal_jaspel = basename+"jaspel-nominal-"+poli;
                var id_persen_jaspel = basename+"jaspel-persen-"+poli;
                var id_nominal_rs = basename+"rs-nominal-"+poli;
                var id_persen_rs = basename+"rs-persen-"+poli;

                $("#"+id_basic_tarif+",#"+id_persen_jaspel+",#"+id_persen_rs).on("keyup",function(){
                    var uang = getMoney("#"+id_basic_tarif);
                    var persen_jaspel = Number($("#"+id_persen_jaspel).val());
                    var persen_rs = Number($("#"+id_persen_rs).val());
                    var uang_jaspel = uang*persen_jaspel/100;
                    var uang_rs = uang*persen_rs/100;
                    setMoney("#"+id_nominal_jaspel,uang_jaspel);
                    setMoney("#"+id_nominal_rs,uang_rs);
                });

                $("#"+id_nominal_rs+",#"+id_nominal_jaspel).on("keyup",function(){
                    var uang = getMoney("#"+id_basic_tarif);
                    var uang_jaspel = getMoney("#"+id_nominal_jaspel);
                    var uang_rs = getMoney("#"+id_nominal_rs);

                    var persen_jaspel = uang_jaspel*100/uang;
                    var persen_rs = uang_rs*100/uang;
                    $("#"+id_persen_jaspel).val(persen_jaspel);
                    $("#"+id_persen_rs).val(persen_rs);
                });


            }
          });
    },3000);
    
});