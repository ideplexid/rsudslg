var tarif_keperawatan_inap;
$(document).ready(function(){
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
    var column = new Array('id','kelas',"tarif",'nama','tarif_lama','tarif_baru',
    'bhp','jaspel','sewa_alat',"lain_lain","penunjang","rs","operator","asisten",
    'bhp_persen','jaspel_persen','sewa_alat_persen',"lain_lain_persen","penunjang_persen","rs_persen","operator_persen","asisten_persen","uri");
	tarif_keperawatan_inap = new ManajemenTarifAction("tarif_keperawatan_inap","manajemen","tarif_keperawatan_inap",column);	
	tarif_keperawatan_inap.view();
    tarif_keperawatan_inap.setDuplicateNameView("nama");

    $("#tarif_keperawatan_inap_sewa_alat ,#tarif_keperawatan_inap_bhp ,#tarif_keperawatan_inap_jaspel ,#tarif_keperawatan_inap_lain_lain ,#tarif_keperawatan_inap_penunjang ,#tarif_keperawatan_inap_rs ,#tarif_keperawatan_inap_operator ,#tarif_keperawatan_inap_asisten").on("keyup",function(){
        var id = $(this).attr("id");
        var x = id.replace("tarif_keperawatan_inap_", "");
        var money = getMoney("#"+id);
        var total = getMoney("#tarif_keperawatan_inap_tarif");
        var persen = money*100/total;
        $("#"+id+"_persen").val(persen.toFixed(3));
    });

    $("#tarif_keperawatan_inap_sewa_alat_persen ,#tarif_keperawatan_inap_bhp_persen ,#tarif_keperawatan_inap_jaspel_persen ,#tarif_keperawatan_inap_lain_lain_persen ,#tarif_keperawatan_inap_penunjang_persen ,#tarif_keperawatan_inap_rs_persen ,#tarif_keperawatan_inap_operator_persen ,#tarif_keperawatan_inap_asisten_persen").on("keyup",function(){
        var id = $(this).attr("id");
        var x = id.replace("_persen", "");
        var total  = getMoney("#tarif_keperawatan_inap_tarif");
        var persen = $(this).val();
        var money = total*persen/100;
        setMoney("#"+x,money.toFixed(2));
    });
});
