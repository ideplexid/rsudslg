var tarif_tindakan_dokter;
var dokter_tarif_tindakan_dokter;
//var employee;
$(document).ready(function(){
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
	var column=new Array('id','nama_dokter','jabatan','nama',
						'id_dokter','kelas',"tarif",'tarif_lama','tarif_baru',
						'jenis_pasien','perawat','perawat_lama','perawat_baru',
                        'tarif_baru','bhp','jaspel','sewa_alat',
                        "lain_lain","rs","operator","asisten",
                        'bhp_persen','jaspel_persen','sewa_alat_persen',"lain_lain_persen",
                        "rs_persen","operator_persen","asisten_persen"
                        );
	tarif_tindakan_dokter=new ManajemenTarifAction("tarif_tindakan_dokter","manajemen","tarif_tindakan_dokter",column);
	tarif_tindakan_dokter.view();
	dokter_tarif_tindakan_dokter=new TableAction("dokter_tarif_tindakan_dokter","manajemen","tarif_tindakan_dokter",new Array());
	dokter_tarif_tindakan_dokter.setSuperCommand("dokter_tarif_tindakan_dokter");
	dokter_tarif_tindakan_dokter.selected=function(json){
		var nama=json.nama;
		var nip=json.id;		
		var jabatan=json.slug;
		$("#tarif_tindakan_dokter_nama_dokter").val(nama);
		$("#tarif_tindakan_dokter_id_dokter").val(nip);
		$("#tarif_tindakan_dokter_jabatan").val(jabatan);
	};
    tarif_tindakan_dokter.setDuplicateNameView("nama");

    $("#tarif_tindakan_dokter_sewa_alat ,#tarif_tindakan_dokter_bhp ,#tarif_tindakan_dokter_jaspel ,#tarif_tindakan_dokter_lain_lain ,#tarif_tindakan_dokter_penunjang ,#tarif_tindakan_dokter_rs ,#tarif_tindakan_dokter_operator ,#tarif_tindakan_dokter_asisten").on("keyup",function(){
        var id = $(this).attr("id");
        var x = id.replace("tarif_tindakan_dokter_", "");
        var money = getMoney("#"+id);
        var total = getMoney("#tarif_tindakan_dokter_tarif");
        var persen = money*100/total;
        $("#"+id+"_persen").val(persen.toFixed(3));
    });

    $("#tarif_tindakan_dokter_sewa_alat_persen ,#tarif_tindakan_dokter_bhp_persen ,#tarif_tindakan_dokter_jaspel_persen ,#tarif_tindakan_dokter_lain_lain_persen ,#tarif_tindakan_dokter_penunjang_persen ,#tarif_tindakan_dokter_rs_persen ,#tarif_tindakan_dokter_operator_persen ,#tarif_tindakan_dokter_asisten_persen").on("keyup",function(){
        var id = $(this).attr("id");
        var x = id.replace("_persen", "");
        var total  = getMoney("#tarif_tindakan_dokter_tarif");
        var persen = $(this).val();
        var money = total*persen/100;
        setMoney("#"+x,money.toFixed(2));
    });
	
});