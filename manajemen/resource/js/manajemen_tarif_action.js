function ManajemenTarifAction(name,page,action,column){
	var total=column.length;
	column[++total]='from_date';
	column[++total]='to_date';
	TableAction.call( this, name,page,action,column);
	this.diagram_holder=null;
	this.diagram_object=null;
	this.last_source=new Array();
	this.title="";
	this.source=null;
	this.ymin=0;
	this.ymax=10;
	this.xkey="";
	this.ykey=[];
	this.labels=[];
	
}
ManajemenTarifAction.prototype.constructor = ManajemenTarifAction;
ManajemenTarifAction.prototype = Object.create( TableAction.prototype );

ManajemenTarifAction.prototype.backup=function(){
    var self=this;
     var a=this.getRegulerData();			
    bootbox.confirm("Peringatan, Proses ini Tidak Dapat di Kembalikan ?", function(result) {
          if(result){
                a['command']="backup";
                showLoading();
                $.post("",a,function(res){
                    var json=getContent(res);
                    self.view();
                    dismissLoading();
                });
        }
    });
};

ManajemenTarifAction.prototype.terapkan=function(){
    var self=this;
     var a=this.getRegulerData();
    bootbox.confirm("Peringatan, Proses ini Tidak Dapat di Kembalikan ?", function(result) {
          if(result){
                a['command']="terapkan";
                showLoading();
                $.post("",a,function(res){
                    var json=getContent(res);
                    self.view();
                    dismissLoading();
                });
        }
    });
};

ManajemenTarifAction.prototype.restore=function(){
    var self=this;
     var a=this.getRegulerData();
     bootbox.confirm("Peringatan, Proses ini Tidak Dapat di Kembalikan ?", function(result) {
          if(result){
                a['command']="restore";
                showLoading();
                $.post("",a,function(res){
                    var json=getContent(res);
                    self.view();
                    dismissLoading();
                });
        }
    });
};