var tarif_konsul;
var dokter_konsul;
//var employee;
$(document).ready(function(){
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
	var column=new Array('id','nama_dokter','jabatan',
			'id_dokter','kelas',"tarif",
            'tarif_lama','tarif_baru',"nama_konsul",
            'jaspel','jaspel_persen',"rs","rs_persen"
            );
	tarif_konsul=new ManajemenTarifAction("tarif_konsul","manajemen","tarif_konsul",column);
	
	tarif_konsul.view();

	dokter_konsul=new TableAction("dokter_konsul","manajemen","tarif_konsul",new Array());
	dokter_konsul.setSuperCommand("dokter_konsul");
	dokter_konsul.selected=function(json){
		var nama=json.nama;
		var nip=json.id;		
		var jabatan=json.slug;
		$("#tarif_konsul_nama_dokter").val(nama);
		$("#tarif_konsul_id_dokter").val(nip);
		$("#tarif_konsul_jabatan").val(jabatan);
	};	
    tarif_konsul.setDuplicateNameView("nama_konsul");
    
    $("#tarif_konsul_jaspel, #tarif_konsul_rs").on("keyup",function(){
        var id = $(this).attr("id");
        var x = id.replace("tarif_konsul_", "");
        var money = getMoney("#"+id);
        var total = getMoney("#tarif_konsul_tarif");
        var persen = money*100/total;
        $("#"+id+"_persen").val(persen.toFixed(3));
    });

    $("#tarif_konsul_jaspel_persen  ,#tarif_konsul_rs_persen").on("keyup",function(){
        var id = $(this).attr("id");
        var x = id.replace("_persen", "");
        var total  = getMoney("#tarif_konsul_tarif");
        var persen = $(this).val();
        var money = total*persen/100;
        console.log(x);
        setMoney("#"+x,money.toFixed(2));
    });
});