<?php
$tabs=new Tabulator("tarif_elektromedis", "");
$tabs->add("harga_elektromedis", "Harga", "",Tabulator::$TYPE_HTML,"fa fa-money");
$tabs->add("rencana_harga_elektromedis", "Rencana", "",Tabulator::$TYPE_HTML,"fa fa-calendar-o");

if(getSettings($db, "manajer-elektromedis-show-bhp", "0")=="1")
	$tabs->add("bhp_elektromedis", "BHP", "",Tabulator::$TYPE_HTML,"fa fa-tty");
if(getSettings($db, "manajer-elektromedis-show-jasa-pelayanan", "0")=="1")
	$tabs->add("jasa_pelayanan_elektromedis", "Jaspel", "",Tabulator::$TYPE_HTML,"fa fa-trophy");
if(getSettings($db, "manajer-elektromedis-show-sewa-alat", "0")=="1")
	$tabs->add("sewa_alat_elektromedis", "Sewa Alat", "",Tabulator::$TYPE_HTML,"fa fa-wrench");
if(getSettings($db, "manajer-elektromedis-show-unit-cost", "0")=="1")
	$tabs->add("unit_cost_elektromedis", "Unit Cost", "",Tabulator::$TYPE_HTML,"fa fa-dollar");
$tabs->setPartialLoad(true, "manajemen", "tarif_elektromedis", "tarif_elektromedis",true);
echo $tabs->getHtml();

?>