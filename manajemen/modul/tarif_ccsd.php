<?php 
require_once "smis-libs-class/MasterTemplate.php";
$m = new MasterTemplate($db,"smis_mjm_cssd",$_POST['page'],$_POST['action']);
$m->getUItable()->setHeader(array("No.","Jenis Tindakan","Satuan","Tarif"));
$m->getUItable()->addModal("id","hidden","","");
$m->getUItable()->addModal("jenis_tindakan","text","Jenis Tindakan","");
$m->getUItable()->addModal("satuan","text","Satuan","");
$m->getUItable()->addModal("tarif","money","Tarif","");
$m->getAdapter()->setUseNumber(true,"No.","back.");
$m->getAdapter()->add("Jenis Tindakan","jenis_tindakan");
$m->getAdapter()->add("Satuan","satuan");
$m->getAdapter()->add("Tarif","tarif","money Rp.");
$m->setModalTitle("Tarif CCSD");
$m->getUItable()->setTitle("Tarif CCS");
$m->initialize();