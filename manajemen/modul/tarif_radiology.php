<?php 
$tabs=new Tabulator("tarif_radiology", "");
$tabs->add("harga_radiology", "Harga", "",Tabulator::$TYPE_HTML,"fa fa-money");
$tabs->add("rencana_harga_radiology", "Rencana", "",Tabulator::$TYPE_HTML,"fa fa-calendar-o");
if(getSettings($db, "manajer-radiology-show-bhp", "0")=="1")
	$tabs->add("bhp_radiology", "BHP", "",Tabulator::$TYPE_HTML,"fa fa-tty");
if(getSettings($db, "manajer-radiology-show-jasa-pelayanan", "0")=="1")
	$tabs->add("jasa_pelayanan_radiology", "Jaspel", "",Tabulator::$TYPE_HTML,"fa fa-trophy");
if(getSettings($db, "manajer-radiology-show-sewa-alat", "0")=="1")
	$tabs->add("sewa_alat_radiology", "Sewa Alat", "",Tabulator::$TYPE_HTML,"fa fa-wrench");
if(getSettings($db, "manajer-radiology-show-unit-cost", "0")=="1")
	$tabs->add("unit_cost_radiology", "Unit Cost", "",Tabulator::$TYPE_HTML,"fa fa-dollar");
if(getSettings($db, "manajer-radiology-show-perujuk", "0")=="1")
	$tabs->add("perujuk_radiology", "Perujuk", "",Tabulator::$TYPE_HTML,"fa fa-user");
if(getSettings($db, "manajer-radiology-show-total", "0")=="1")
	$tabs->add("total_radiology", "Total", "",Tabulator::$TYPE_HTML,"fa fa-dollar");
if(getSettings($db, "manajer-radiology-show-sisa", "0")=="1")
	$tabs->add("sisa_radiology", "Sisa", "",Tabulator::$TYPE_HTML,"fa fa-dollar");
if(getSettings($db, "manajer-radiology-show-bacaan", "0")=="1")
	$tabs->add("bacaan_radiology", "Bacaan", "",Tabulator::$TYPE_HTML,"fa fa-user");
if(getSettings($db, "manajer-radiology-show-rs", "0")=="1")
	$tabs->add("rs_radiology", "Rumah Sakit", "",Tabulator::$TYPE_HTML,"fa fa-hospital-o");
$tabs->setPartialLoad(true, "manajemen", "tarif_radiology", "tarif_radiology",true);
echo $tabs->getHtml();
?>