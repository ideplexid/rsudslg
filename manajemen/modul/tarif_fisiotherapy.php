<?php 
$tabs=new Tabulator("tarif_fisiotherapy", "");
$tabs->add("harga_fisiotherapy", "Harga", "",Tabulator::$TYPE_HTML,"fa fa-money");
$tabs->add("rencana_harga_fisiotherapy", "Rencana", "",Tabulator::$TYPE_HTML,"fa fa-calendar-o");
if(getSettings($db, "manajer-fisiotherapy-show-bhp", "0")=="1")
	$tabs->add("bhp_fisiotherapy", "BHP", "",Tabulator::$TYPE_HTML,"fa fa-tty");
if(getSettings($db, "manajer-fisiotherapy-show-jasa-pelayanan", "0")=="1")
	$tabs->add("jasa_pelayanan_fisiotherapy", "Jaspel", "",Tabulator::$TYPE_HTML,"fa fa-trophy");
if(getSettings($db, "manajer-fisiotherapy-show-sewa-alat", "0")=="1")
	$tabs->add("sewa_alat_fisiotherapy", "Sewa Alat", "",Tabulator::$TYPE_HTML,"fa fa-wrench");
if(getSettings($db, "manajer-fisiotherapy-show-unit-cost", "0")=="1")
	$tabs->add("unit_cost_fisiotherapy", "Unit Cost", "",Tabulator::$TYPE_HTML,"fa fa-dollar");
$tabs->setPartialLoad(true, "manajemen", "tarif_fisiotherapy", "tarif_fisiotherapy",true);
echo $tabs->getHtml();

?>