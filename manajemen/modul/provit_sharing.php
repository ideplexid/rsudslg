<?php
	$tab = new Tabulator ( "provit_sharing", "Provit Share", Tabulator::$POTRAIT );
	$tab->add ( "provit_sharing_default", "Standard", "", Tabulator::$TYPE_HTML,"fa fa-file" );
	$tab->add ( "provit_sharing_karyawan", "Karyawan", "", Tabulator::$TYPE_HTML,"fa fa-user" );
	$tab->add ( "provit_sharing_bpjs", "BPJS", "", Tabulator::$TYPE_HTML,"fa fa-money" );
	$tab->setPartialLoad(true,"manajemen","provit_sharing","provit_sharing",true);
	echo $tab->getHtml ();
?>