<?php
require_once 'smis-framework/smis/api/SettingsBuilder.php';
require_once 'smis-base/smis-include-service-consumer.php';
require_once "smis-libs-class/ServiceProviderList.php";
global $db;

$persen = new OptionBuilder();
$persen ->add("Nominal","1","1");
$persen ->add("Persen","0","0");


$smis = new SettingsBuilder( $db, "man_settings", "manajemen", "settings" );
$smis->setShowDescription ( true );

$smis->addTabs ( "laboratory", "Laboratory","fa fa-flask" );
if($smis->isGroup("laboratory")){
    $smis->addItem ( "laboratory", new SettingsItem ( $db, "manajer-laboratory-model", "Tampilan Tindakan Laboratory", $persen->getContent(), "select", "" ) );
    $smis->addItem ( "laboratory", new SettingsItem ( $db, "manajer-laboratory-show-sewa-alat", "Tampilkan Sewa Alat",0, "checkbox", "Menampilkan Komponen Tarif untuk Sewa Alat" ) );
    $smis->addItem ( "laboratory", new SettingsItem ( $db, "manajer-laboratory-show-jasa-pelayanan", "Tampilkan Jasa Pelayanan", 0, "checkbox", "Menampilkan Komponen Tarif untuk Jasa Pelayanan" ) );
    $smis->addItem ( "laboratory", new SettingsItem ( $db, "manajer-laboratory-show-bhp", "Tampilkan Bahan Habis Pakai", 0, "checkbox", "Menampilkan Komponen Tarif untuk Bahan Habis Pakai" ) );
    $smis->addItem ( "laboratory", new SettingsItem ( $db, "manajer-laboratory-show-unit-cost", "Tampilkan Bahan Unit Cost", 0, "checkbox", "Menampilkan Komponen Tarif untuk Unit Cost" ) );
    $smis->addItem ( "laboratory", new SettingsItem ( $db, "manajer-laboratory-show-perujuk", "Tampilkan Tarif Perujuk", 0, "checkbox", "Menampilkan Komponen Tarif untuk Perujuk" ) );
    $smis->addItem ( "laboratory", new SettingsItem ( $db, "manajer-laboratory-show-total", "Tampilkan Total Tarif", 0, "checkbox", "Menampilkan Komponen Total Tarif (BHP + Jaspel + Perujuk)" ) );
    $smis->addItem ( "laboratory", new SettingsItem ( $db, "manajer-laboratory-show-rs", "Tampilkan RS", 0, "checkbox", "Menampilkan Komponen Tarif untuk Rumah Sakit" ) );
    $smis->addSection("laboratory","Break Down");
    $smis->addItem ( "laboratory", new SettingsItem ( $db, "manajer-laboratory-persen-rs", "Persentase RS & BHP", "0", "text" ) );
    $smis->addItem ( "laboratory", new SettingsItem ( $db, "manajer-laboratory-persen-jaspel", "Pesentase Jasa Pelayanan", "0", "text" ) );

}

$smis->addTabs ( "patology", "patology","fa fa-flask" );
if($smis->isGroup("patology")){
    $smis->addItem ( "patology", new SettingsItem ( $db, "manajer-patology-model", "Tampilan Tindakan Patology", $persen->getContent(), "select", "" ) );
    $smis->addItem ( "patology", new SettingsItem ( $db, "manajer-patology-show-sewa-alat", "Tampilkan Sewa Alat",0, "checkbox", "Menampilkan Komponen Tarif untuk Sewa Alat" ) );
    $smis->addItem ( "patology", new SettingsItem ( $db, "manajer-patology-show-jasa-pelayanan", "Tampilkan Jasa Pelayanan", 0, "checkbox", "Menampilkan Komponen Tarif untuk Jasa Pelayanan" ) );
    $smis->addItem ( "patology", new SettingsItem ( $db, "manajer-patology-show-bhp", "Tampilkan Bahan Habis Pakai", 0, "checkbox", "Menampilkan Komponen Tarif untuk Bahan Habis Pakai" ) );
    $smis->addItem ( "patology", new SettingsItem ( $db, "manajer-patology-show-unit-cost", "Tampilkan Bahan Unit Cost", 0, "checkbox", "Menampilkan Komponen Tarif untuk Unit Cost" ) );
    $smis->addItem ( "patology", new SettingsItem ( $db, "manajer-patology-show-perujuk", "Tampilkan Tarif Perujuk", 0, "checkbox", "Menampilkan Komponen Tarif untuk Perujuk" ) );
    $smis->addItem ( "patology", new SettingsItem ( $db, "manajer-patology-show-total", "Tampilkan Total Tarif", 0, "checkbox", "Menampilkan Komponen Total Tarif (BHP + Jaspel + Perujuk)" ) );
    $smis->addItem ( "patology", new SettingsItem ( $db, "manajer-patology-show-rs", "Tampilkan RS", 0, "checkbox", "Menampilkan Komponen Tarif untuk Rumah Sakit" ) );
    $smis->addSection("patology","Break Down");
    $smis->addItem ( "patology", new SettingsItem ( $db, "manajer-patology-persen-rs", "Persentase RS & BHP", "0", "text" ) );
    $smis->addItem ( "patology", new SettingsItem ( $db, "manajer-patology-persen-jaspel", "Pesentase Jasa Pelayanan", "0", "text" ) );

}

$smis->addTabs ( "radiology", "Radiology","fa fa-warning" );
if($smis->isGroup("radiology")){
    $smis->addItem ( "radiology", new SettingsItem ( $db, "manajer-radiology-model", "Tampilan Tindakan Radiology", $persen->getContent(), "select", "" ) );
    $smis->addItem ( "radiology", new SettingsItem ( $db, "manajer-radiology-show-sewa-alat", "Tampilkan Sewa Alat", 0, "checkbox", "Menampilkan Komponen Tarif untuk Sewa Alat" ) );
    $smis->addItem ( "radiology", new SettingsItem ( $db, "manajer-radiology-show-jasa-pelayanan", "Tampilkan Jasa Pelayanan", 0, "checkbox", "Menampilkan Komponen Tarif untuk Jasa Pelayanan" ) );
    $smis->addItem ( "radiology", new SettingsItem ( $db, "manajer-radiology-show-bhp", "Tampilkan Bahan Habis Pakai", 0, "checkbox", "Menampilkan Komponen Tarif untuk Bahan Habis Pakai" ) );
    $smis->addItem ( "radiology", new SettingsItem ( $db, "manajer-radiology-show-unit-cost", "Tampilkan Bahan Unit Cost", 0, "checkbox", "Menampilkan Komponen Tarif untuk Unit Cost" ) );
    $smis->addItem ( "radiology", new SettingsItem ( $db, "manajer-radiology-show-perujuk", "Tampilkan Tarif Perujuk", 0, "checkbox", "Menampilkan Komponen Tarif untuk Perujuk" ) );
    $smis->addItem ( "radiology", new SettingsItem ( $db, "manajer-radiology-show-total", "Tampilkan Total Tarif", 0, "checkbox", "Menampilkan Komponen Total Tarif (BHP + Jaspel + Perujuk)" ) );
    $smis->addItem ( "radiology", new SettingsItem ( $db, "manajer-radiology-show-sisa", "Tampilkan Sisa Total Tarif", 0, "checkbox", "Menampilkan Komponen Sisa Total Tarif" ) );
    $smis->addItem ( "radiology", new SettingsItem ( $db, "manajer-radiology-show-bacaan", "Tampilkan Tarif Bacaan", 0, "checkbox", "Menampilkan Komponen Tarif untuk Bacaan" ) );
    $smis->addItem ( "radiology", new SettingsItem ( $db, "manajer-radiology-show-rs", "Tampilkan RS", 0, "checkbox", "Menampilkan Komponen Tarif untuk Rumah Sakit" ) );
    $smis->addSection("radiology","Break Down");
    $smis->addItem ( "radiology", new SettingsItem ( $db, "manajer-radiology-persen-rs", "Persentase RS & BHP", "0", "text" ) );
    $smis->addItem ( "radiology", new SettingsItem ( $db, "manajer-radiology-persen-jaspel", "Pesentase Jasa Pelayanan", "0", "text" ) );
}

$smis->addTabs ( "fisiotherapy", "Fisiotherapy","fa fa-slideshare" );
if($smis->isGroup("fisiotherapy")){
    $smis->addItem ( "fisiotherapy", new SettingsItem ( $db, "manajer-fisiotherapy-model", "Tampilan Tindakan Fisiotherapy", $persen->getContent(), "select", "" ) );
    $smis->addItem ( "fisiotherapy", new SettingsItem ( $db, "manajer-fisiotherapy-show-sewa-alat", "Tampilkan Sewa Alat", 0, "checkbox", "Menampilkan Sewa Alat dan Bahan Habis Pakai" ) );
    $smis->addItem ( "fisiotherapy", new SettingsItem ( $db, "manajer-fisiotherapy-show-jasa-pelayanan", "Tampilkan Jasa Pelayanan", 0, "checkbox", "Tampilan Jasa Pelayanan" ) );
    $smis->addItem ( "fisiotherapy", new SettingsItem ( $db, "manajer-fisiotherapy-show-bhp", "Tampilkan Bahan Habis Pakai", 0, "checkbox", "Tampilkan Bahan Habis Pakai" ) );
    $smis->addItem ( "fisiotherapy", new SettingsItem ( $db, "manajer-fisiotherapy-show-unit-cost", "Tampilkan Unit Cost", 0, "checkbox", "Tampilkan Unit Cost" ) );
    $smis->addSection("fisiotherapy","Break Down");
    $smis->addItem ( "fisiotherapy", new SettingsItem ( $db, "manajer-fisiotherapy-persen-rs", "Persentase RS & BHP", "0", "text" ) );
    $smis->addItem ( "fisiotherapy", new SettingsItem ( $db, "manajer-fisiotherapy-persen-jaspel", "Pesentase Jasa Pelayanan", "0", "text" ) );
}

$smis->addTabs ( "elektromedis", "Elektromedis","fa fa-slideshare" );
if($smis->isGroup("elektromedis")){
    $smis->addItem ( "elektromedis", new SettingsItem ( $db, "manajer-elektromedis-model", "Tampilan Tindakan Elektromedis", $persen->getContent(), "select", "" ) );
    $smis->addItem ( "elektromedis", new SettingsItem ( $db, "manajer-elektromedis-show-sewa-alat", "Tampilkan Sewa Alat", 0, "checkbox", "Menampilkan Sewa Alat dan Bahan Habis Pakai" ) );
    $smis->addItem ( "elektromedis", new SettingsItem ( $db, "manajer-elektromedis-show-jasa-pelayanan", "Tampilkan Jasa Pelayanan", 0, "checkbox", "Tampilan Jasa Pelayanan" ) );
    $smis->addItem ( "elektromedis", new SettingsItem ( $db, "manajer-elektromedis-show-bhp", "Tampilkan Bahan Habis Pakai", 0, "checkbox", "Tampilkan Bahan Habis Pakai" ) );
    $smis->addItem ( "elektromedis", new SettingsItem ( $db, "manajer-elektromedis-show-unit-cost", "Tampilkan Unit Cost", 0, "checkbox", "Tampilkan Unit Cost" ) );
    $smis->addSection("elektromedis","Break Down");
    $smis->addItem ( "elektromedis", new SettingsItem ( $db, "manajer-elektromedis-persen-rs", "Persentase RS & BHP", "0", "text" ) );
    $smis->addItem ( "elektromedis", new SettingsItem ( $db, "manajer-elektromedis-persen-jaspel", "Pesentase Jasa Pelayanan", "0", "text" ) );
}

$smis->addTabs ( "tindakan_dokter", "Tindakan Dokter","fa fa-user-md" );
if($smis->isGroup("tindakan_dokter")){
    $smis->addItem ( "tindakan_dokter", new SettingsItem ( $db, "manajer-tindakan-dokter-model", "Tampilan Tindakan Dokter", $persen->getContent(), "select", "" ) );
    $smis->addItem ( "tindakan_dokter", new SettingsItem ( $db, "manajer-tindakan-dokter-show-sewa-alat", "Tampilkan Sewa Alat", 0, "checkbox", "Menampilkan Sewa Alat dan Bahan Habis Pakai" ) );
    $smis->addItem ( "tindakan_dokter", new SettingsItem ( $db, "manajer-tindakan-dokter-show-jasa-pelayanan", "Tampilkan Jasa Pelayanan", 0, "checkbox", "Tampilan Jasa Pelayanan" ) );
    $smis->addItem ( "tindakan_dokter", new SettingsItem ( $db, "manajer-tindakan-dokter-show-bhp", "Tampilkan Bahan Habis Pakai", 0, "checkbox", "Tampilkan Bahan Habis Pakai" ) );
    $smis->addItem ( "tindakan_dokter", new SettingsItem ( $db, "manajer-tindakan-dokter-show-rs", "Tampilkan Nilai Bagi RS", 0, "checkbox", "Tampilkan Nilai RS" ) );
    $smis->addItem ( "tindakan_dokter", new SettingsItem ( $db, "manajer-tindakan-dokter-show-jasa-lain-lain", "Tampilkan Jasa Lain-Lain", 0, "checkbox", "Tampilan Jasa Lain-Lain" ) );
    $smis->addItem ( "tindakan_dokter", new SettingsItem ( $db, "manajer-tindakan-dokter-show-operator", "Tampilkan Operator", 0, "checkbox", "Tampilkan Operator" ) );
    $smis->addItem ( "tindakan_dokter", new SettingsItem ( $db, "manajer-tindakan-dokter-show-asisten", "Tampilkan Asisten", 0, "checkbox", "Tampilkan Asisten" ) );
}

$smis->addTabs ( "tindakan_perawat", "Tindakan Keperawatan","fa fa-stethoscope" );
if($smis->isGroup("tindakan_perawat")){    
    $smis->addItem ( "tindakan_perawat", new SettingsItem ( $db, "manajer-tindakan-perawat-model", "Tampilan Tindakan Perawat", $persen->getContent(), "select", "" ) );
    $smis->addItem ( "tindakan_perawat", new SettingsItem ( $db, "manajer-tindakan-perawat-show-sewa-alat", "Tampilkan Sewa Alat", 0, "checkbox", "Menampilkan Sewa Alat dan Bahan Habis Pakai" ) );
    $smis->addItem ( "tindakan_perawat", new SettingsItem ( $db, "manajer-tindakan-perawat-show-bhp", "Tampilkan Bahan Habis Pakai", 0, "checkbox", "Tampilkan Bahan Habis Pakai" ) );
    $smis->addItem ( "tindakan_perawat", new SettingsItem ( $db, "manajer-tindakan-perawat-show-jasa-pelayanan", "Tampilkan Jasa Pelayanan", 0, "checkbox", "Tampilan Jasa Pelayanan" ) );
    $smis->addItem ( "tindakan_perawat", new SettingsItem ( $db, "manajer-tindakan-perawat-show-jasa-lain-lain", "Tampilkan Jasa Lain-Lain", 0, "checkbox", "Tampilan Jasa Lain-Lain" ) );
    $smis->addItem ( "tindakan_perawat", new SettingsItem ( $db, "manajer-tindakan-perawat-show-jasa-penunjang", "Tampilkan Jasa Penunjang", 0, "checkbox", "Tampilan Jasa Penunjang" ) );
    $smis->addItem ( "tindakan_perawat", new SettingsItem ( $db, "manajer-tindakan-perawat-show-rs", "Tampilkan Nilai Bagi RS", 0, "checkbox", "Tampilkan Nilai RS" ) );
    $smis->addItem ( "tindakan_perawat", new SettingsItem ( $db, "manajer-tindakan-perawat-show-operator", "Tampilkan Operator", 0, "checkbox", "Tampilkan Operator" ) );
    $smis->addItem ( "tindakan_perawat", new SettingsItem ( $db, "manajer-tindakan-perawat-show-asisten", "Tampilkan Asisten", 0, "checkbox", "Tampilkan Asisten" ) );
}

$smis->addTabs ( "visite", "Visite","fa fa-stethoscope" );
if($smis->isGroup("visite")){    
    $smis->addItem ( "visite", new SettingsItem ( $db, "manajer-visite-model", "Tampilan Tindakan Visite", $persen->getContent(), "select", "" ) );
}

$smis->addTabs ( "konsul", "Konsul","fa fa-stethoscope" );
if($smis->isGroup("konsul")){    
    $smis->addItem ( "konsul", new SettingsItem ( $db, "manajer-konsul-model", "Tampilan Tindakan Konsul", $persen->getContent(), "select", "" ) );
}

$smis->addTabs ( "service", "Service","fa fa-cog" );
if($smis->isGroup("service")){
    $smis->addItem ( "service", new SettingsItem ( $db, "manajer-service-tarif-keperawatan-view", "Gunakan View untuk Tarif Keperawatan", 0, "checkbox", "Menggunakan View untuk Tarif Keperawatan pada service get_keperawatan" ) );
    $smis->addItem ( "service", new SettingsItem ( $db, "manajer-service-used-filter", "Aktifkan Filter pada Setting Formula", 0, "checkbox", "Menggunakan Filter pada Settings Formula" ) );
}

$smis->addTabs ( "operasi", "Operasi","fa fa-user-md" );
if($smis->isGroup("operasi")){
    $mode_operasi=new OptionBuilder();
    $mode_operasi->add("Sederhana","0","1");
    $mode_operasi->add("Lengkap","1","0");
    $smis->addItem ( "operasi", new SettingsItem ( $db, "manajer-operasi-mode", "Mode Operasi", $mode_operasi->getContent(), "select", "Tampilan Inputan Operasi" ) );
}


$smis->addTabs ( "duplicate", "Duplikasi","fa fa-upload" );
if($smis->isGroup("duplicate")){
    $smis->addItem ( "duplicate", new SettingsItem ( $db, "manajer-duplicate-tarif-keperawatan", "Melakukan Duplikasi Tarif Keperawatan", 0, "text", "0 - Tidak Aktif, 1. Auto Duplicate , 2. Manual Duplicate" ) );
    $smis->addItem ( "duplicate", new SettingsItem ( $db, "manajer-duplicate-tarif-ambulan", "Melakukan Duplikasi Tarif Ambulan", 0, "text", "0 - Tidak Aktif, 1. Auto Duplicate , 2. Manual Duplicate" ) );
    $smis->addItem ( "duplicate", new SettingsItem ( $db, "manajer-duplicate-tarif-anastesi", "Melakukan Duplikasi Tarif Anastesi", 0, "text", "0 - Tidak Aktif, 1. Auto Duplicate , 2. Manual Duplicate" ) );
    $smis->addItem ( "duplicate", new SettingsItem ( $db, "manajer-duplicate-tarif-audiometry", "Melakukan Duplikasi Tarif Audiometry", 0, "text", "0 - Tidak Aktif, 1. Auto Duplicate , 2. Manual Duplicate" ) );
    $smis->addItem ( "duplicate", new SettingsItem ( $db, "manajer-duplicate-tarif-bronchoscopy", "Melakukan Duplikasi Tarif Bronchoscopy", 0, "text", "0 - Tidak Aktif, 1. Auto Duplicate , 2. Manual Duplicate" ) );
    $smis->addItem ( "duplicate", new SettingsItem ( $db, "manajer-duplicate-tarif-faalparu", "Melakukan Duplikasi Tarif Faal Paru", 0, "text", "0 - Tidak Aktif, 1. Auto Duplicate , 2. Manual Duplicate" ) );
    $smis->addItem ( "duplicate", new SettingsItem ( $db, "manajer-duplicate-tarif-endoscopy", "Melakukan Duplikasi Tarif Endoscopy", 0, "text", "0 - Tidak Aktif, 1. Auto Duplicate , 2. Manual Duplicate" ) );
    $smis->addItem ( "duplicate", new SettingsItem ( $db, "manajer-duplicate-tarif-ekg", "Melakukan Duplikasi Tarif EKG", 0, "text", "0 - Tidak Aktif, 1. Auto Duplicate , 2. Manual Duplicate" ) );
    $smis->addItem ( "duplicate", new SettingsItem ( $db, "manajer-duplicate-tarif-konsul", "Melakukan Duplikasi Tarif Konsul", 0, "text", "0 - Tidak Aktif, 1. Auto Duplicate , 2. Manual Duplicate" ) );
    $smis->addItem ( "duplicate", new SettingsItem ( $db, "manajer-duplicate-tarif-ok", "Melakukan Duplikasi Tarif OK", 0, "text", "0 - Tidak Aktif, 1. Auto Duplicate , 2. Manual Duplicate" ) );
    $smis->addItem ( "duplicate", new SettingsItem ( $db, "manajer-duplicate-tarif-spirometry", "Melakukan Duplikasi Tarif Spirometry", 0, "text", "0 - Tidak Aktif, 1. Auto Duplicate , 2. Manual Duplicate" ) );
    $smis->addItem ( "duplicate", new SettingsItem ( $db, "manajer-duplicate-tarif-tindakan-dokter", "Melakukan Duplikasi Tarif Tindakan Dokter", 0, "text", "0 - Tidak Aktif, 1. Auto Duplicate , 2. Manual Duplicate" ) );
    $smis->addItem ( "duplicate", new SettingsItem ( $db, "manajer-duplicate-tarif-tindakan-operasi", "Melakukan Duplikasi Tarif Tindakan Operasi", 0, "text", "0 - Tidak Aktif, 1. Auto Duplicate , 2. Manual Duplicate" ) );
    $smis->addItem ( "duplicate", new SettingsItem ( $db, "manajer-duplicate-tarif-visite", "Melakukan Duplikasi Tarif Visite", 0, "text", "0 - Tidak Aktif, 1. Auto Duplicate , 2. Manual Duplicate" ) );
    $smis->addItem ( "duplicate", new SettingsItem ( $db, "manajer-duplicate-tarif-vk", "Melakukan Duplikasi Tarif VK", 0, "text", "0 - Tidak Aktif, 1. Auto Duplicate , 2. Manual Duplicate" ) );
}

$smis->addTabs ( "profile", "Profile","fa fa-user-secret" );
if($smis->isGroup("profile")){
    $smis->addItem ( "profile", new SettingsItem ( $db, "smis_autonomous_logo", "Logo", "", "file-single-image", "Logo Default" ) );
    $smis->addItem ( "profile", new SettingsItem ( $db, "smis_autonomous_bw_logo", "Logo Hitam Putih", "", "file-single-image", "Logo Hitam Putih" ) );
    $smis->addItem ( "profile", new SettingsItem ( $db, "smis_autonomous_loading_logo", "Logo Loading", "", "file-single-image", "Logo untuk Loading Bar" ) );
    $smis->addItem ( "profile", new SettingsItem ( $db, "smis_autonomous_title", "Nama", "", "text", "Nama dari Instansi" ) );
    $smis->addItem ( "profile", new SettingsItem ( $db, "smis_autonomous_sub_title", "Sub Nama", "", "text", "Sub Nama Instansi" ) );
    $smis->addItem ( "profile", new SettingsItem ( $db, "smis_autonomous_abbrevation", "Singkatan Instansi", "", "text", "nama singkatan dari instansi" ) );
    $smis->addItem ( "profile", new SettingsItem ( $db, "smis_autonomous_code", "No. Code", "", "text", "Code dari Instansi" ) );
    $smis->addItem ( "profile", new SettingsItem ( $db, "smis_autonomous_tanggal", "Tanggal Register", "", "date", "Tanggal Registrasi Instansi" ) );
    
    $smis->addItem ( "profile", new SettingsItem ( $db, "smis_autonomous_address", "Alamat Lengkap", "Alamat Lengkap Berserta Nomor Telponya", "textarea", "Alamat Lengkap Beserta Nomor Telponya" ) );
    $smis->addItem ( "profile", new SettingsItem ( $db, "smis_autonomous_address_mini", "Alamat Mini", "", "textarea", "Alamat Mini untuk Kebutuhan Header Kecil" ) );
    $smis->addItem ( "profile", new SettingsItem ( $db, "smis_autonomous_contact", "Telp 1", "(0322) ", "textarea", "Telp. 1" ) );
    $smis->addItem ( "profile", new SettingsItem ( $db, "smis_autonomous_contact2", "Telp 2", "(0322) ", "textarea", "Telp. 1" ) );
    $smis->addItem ( "profile", new SettingsItem ( $db, "smis_autonomous_fax", "Fax", "", "text", "Nomor Faximile" ) );
    $smis->addItem ( "profile", new SettingsItem ( $db, "smis_autonomous_email", "E-Mail", "", "text", "Alamat E-mail" ) );
    $smis->addItem ( "profile", new SettingsItem ( $db, "smis_autonomous_website", "Website", "", "text", "URL Web" ) );
    $smis->addItem ( "profile", new SettingsItem ( $db, "smis_autonomous_npwp", "NPWP", "", "text", "Nomor NPWP" ) );    
    $smis->addItem ( "profile", new SettingsItem ( $db, "smis_autonomous_province", "Provinsi", "", "text", "Nama Provinsi" ) );
    $smis->addItem ( "profile", new SettingsItem ( $db, "smis_autonomous_town", "Kota / Kabupaten", "", "text", "Nama Kabupaten atau Kota" ) );
    $smis->addItem ( "profile", new SettingsItem ( $db, "smis_autonomous_district", "Kecamatan", "", "text", "Nama Kecamatan" ) );
    $smis->addItem ( "profile", new SettingsItem ( $db, "smis_autonomous_village", "Kelurahan", "", "text", "Nama Kelurahan" ) );
    $smis->addItem ( "profile", new SettingsItem ( $db, "smis_autonomous_type", "Jenis", "", "text", "Jenis dari Instansi" ) );
    $smis->addItem ( "profile", new SettingsItem ( $db, "smis_autonomous_class", "Kelas", "", "text", "Kelas dari Instansi" ) );
    
}

$smis->setPartialLoad(true);
$smis->init();