<?php 
$tabs=new Tabulator("tarif_patology", "");
$tabs->add("harga_patology", "Harga", "",Tabulator::$TYPE_HTML,"fa fa-money");
$tabs->add("bhp_patology", "BHP & RS", "",Tabulator::$TYPE_HTML,"fa fa-tty");
$tabs->add("rs_patology", "Rumah Sakit", "",Tabulator::$TYPE_HTML,"fa fa-hospital-o");

$tabs->add("rencana_harga_patology", "Rencana", "",Tabulator::$TYPE_HTML,"fa fa-calendar-o");
if(getSettings($db, "manajer-patology-show-bhp", "0")=="1")
	$tabs->add("bhp_patology", "BHP", "",Tabulator::$TYPE_HTML,"fa fa-tty");
if(getSettings($db, "manajer-patology-show-jasa-pelayanan", "0")=="1")
	$tabs->add("jasa_pelayanan_patology", "Jaspel", "",Tabulator::$TYPE_HTML,"fa fa-trophy");
if(getSettings($db, "manajer-patology-show-sewa-alat", "0")=="1")
	$tabs->add("sewa_alat_patology", "Sewa Alat", "",Tabulator::$TYPE_HTML,"fa fa-wrench");
if(getSettings($db, "manajer-patology-show-unit-cost", "0")=="1")
	$tabs->add("unit_cost_patology", "Unit Cost", "",Tabulator::$TYPE_HTML,"fa fa-dollar");
if(getSettings($db, "manajer-patology-show-perujuk", "0")=="1")
	$tabs->add("perujuk_patology", "Perujuk", "",Tabulator::$TYPE_HTML,"fa fa-user");
if(getSettings($db, "manajer-patology-show-total", "0")=="1")
	$tabs->add("total_patology", "Total", "",Tabulator::$TYPE_HTML,"fa fa-dollar");
if(getSettings($db, "manajer-patology-show-rs", "0")=="1")
	$tabs->add("rs_patology", "Rumah Sakit", "",Tabulator::$TYPE_HTML,"fa fa-hospital-o");

$tabs->setPartialLoad(true, "manajemen", "tarif_patology", "tarif_patology",true);
echo $tabs->getHtml();