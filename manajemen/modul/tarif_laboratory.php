<?php 
$tabs=new Tabulator("tarif_laboratory", "");
$tabs->add("harga_laboratory", "Harga", "",Tabulator::$TYPE_HTML,"fa fa-money");
$tabs->add("bhp_laboratory", "BHP & RS", "",Tabulator::$TYPE_HTML,"fa fa-tty");
$tabs->add("rs_laboratory", "Rumah Sakit", "",Tabulator::$TYPE_HTML,"fa fa-hospital-o");

$tabs->add("rencana_harga_laboratory", "Rencana", "",Tabulator::$TYPE_HTML,"fa fa-calendar-o");
if(getSettings($db, "manajer-laboratory-show-bhp", "0")=="1")
	$tabs->add("bhp_laboratory", "BHP", "",Tabulator::$TYPE_HTML,"fa fa-tty");
if(getSettings($db, "manajer-laboratory-show-jasa-pelayanan", "0")=="1")
	$tabs->add("jasa_pelayanan_laboratory", "Jaspel", "",Tabulator::$TYPE_HTML,"fa fa-trophy");
if(getSettings($db, "manajer-laboratory-show-sewa-alat", "0")=="1")
	$tabs->add("sewa_alat_laboratory", "Sewa Alat", "",Tabulator::$TYPE_HTML,"fa fa-wrench");
if(getSettings($db, "manajer-laboratory-show-unit-cost", "0")=="1")
	$tabs->add("unit_cost_laboratory", "Unit Cost", "",Tabulator::$TYPE_HTML,"fa fa-dollar");
if(getSettings($db, "manajer-laboratory-show-perujuk", "0")=="1")
	$tabs->add("perujuk_laboratory", "Perujuk", "",Tabulator::$TYPE_HTML,"fa fa-user");
if(getSettings($db, "manajer-laboratory-show-total", "0")=="1")
	$tabs->add("total_laboratory", "Total", "",Tabulator::$TYPE_HTML,"fa fa-dollar");
if(getSettings($db, "manajer-laboratory-show-rs", "0")=="1")
	$tabs->add("rs_laboratory", "Rumah Sakit", "",Tabulator::$TYPE_HTML,"fa fa-hospital-o");

$tabs->setPartialLoad(true, "manajemen", "tarif_laboratory", "tarif_laboratory",true);
echo $tabs->getHtml();