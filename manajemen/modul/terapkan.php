<?php


global $db;
require_once "smis-base/smis-include-service-consumer.php";

$head=array ('ID','Name','Value');
$uitable = new Table ( $head, "", NULL, false );
$uitable->setName ("terapkan");
$uitable->setFooterVisible(false);

if(isset ( $_POST ['command'] ) && $_POST ['command']=="backup"){
	
	if($_POST['ruangan']=="Laboratory" || $_POST['ruangan']==" - - Semua - - "){
		$rlab=getSettings($db, "smis_lab_harga", NULL);
		if($rlab!=null)
			setSettings($db, "backup_smis_lab_harga", $rlab);
	}
	if($_POST['ruangan']=="Radiology" || $_POST['ruangan']==" - - Semua - - "){
		$rrad=getSettings($db, "smis_rad_harga", NULL);
		if($rrad!=null)
			setSettings($db, "backup_smis_rad_harga", $rrad);
	}
	if($_POST['ruangan']=="Fisiotherapy" || $_POST['ruangan']==" - - Semua - - "){
		$rfis=getSettings($db, "smis_fst_harga", NULL);
		if($rfis!=null)
			setSettings($db, "backup_smis_fst_harga", $rfis);
	}
	
	
	$res=new ResponsePackage();
	$res->setStatus(ResponsePackage::$STATUS_OK);
	$res->setAlertVisible(true);
	$res->setContent("");
	$res->setAlertContent("Berhasil", "Berhasil di Backup");
	echo json_encode($res->getPackage());
	return;
}

if(isset ( $_POST ['command'] ) && $_POST ['command']=="restore"){
	if($_POST['ruangan']=="Laboratory" || $_POST['ruangan']==" - - Semua - - "){
		$rlab=getSettings($db, "backup_smis_lab_harga", NULL);
		if($rlab!=null)
			setSettings($db, "smis_lab_harga", $rlab);
	}
	if($_POST['ruangan']=="Radiology" || $_POST['ruangan']==" - - Semua - - "){
		$rrad=getSettings($db, "backup_smis_rad_harga", NULL);
		if($rrad!=null)
			setSettings($db, "smis_rad_harga", $rrad);
	}
	if($_POST['ruangan']=="Fisiotherapy" || $_POST['ruangan']==" - - Semua - - "){
		$rfis=getSettings($db, "backup_smis_fst_harga", NULL);
		if($rfis!=null)
			setSettings($db, "smis_fst_harga", $rfis);
	}
	
	$res=new ResponsePackage();
	$res->setStatus(ResponsePackage::$STATUS_OK);
	$res->setAlertVisible(true);
	$res->setContent("");
	$res->setAlertContent("Berhasil", "Berhasil di Restore");
	echo json_encode($res->getPackage());
	return;
}

if(isset ( $_POST ['command'] ) && $_POST ['command']=="terapkan"){
	if($_POST['ruangan']=="Laboratory" || $_POST['ruangan']==" - - Semua - - "){
		$rlab=getSettings($db, "rencana_smis_lab_harga", NULL);
		if($rlab!=null)
			setSettings($db, "smis_lab_harga", $rlab);
	}	
	if($_POST['ruangan']=="Radiology" || $_POST['ruangan']==" - - Semua - - "){
		$rrad=getSettings($db, "rencana_smis_rad_harga", NULL);
		if($rrad!=null)
			setSettings($db, "smis_rad_harga", $rrad);
	}	
	if($_POST['ruangan']=="Fisiotherapy" || $_POST['ruangan']==" - - Semua - - "){
		$rfis=getSettings($db, "rencana_smis_fst_harga", NULL);
		if($rfis!=null)
			setSettings($db, "smis_fst_harga", $rfis);
	}	
	$res=new ResponsePackage();
	$res->setStatus(ResponsePackage::$STATUS_OK);
	$res->setAlertVisible(true);
	$res->setContent("");
	$res->setAlertContent("Berhasil", "Berhasil di Terapkan");
	echo json_encode($res->getPackage());
	return;
}

if(isset ( $_POST ['command'] ) && $_POST ['command']=="synch"){
    $data=array();    
	if($_POST['ruangan']=="Laboratory" || $_POST['ruangan']==" - - Semua - - "){
		$data['backup_smis_lab_harga']=getSettings($db, "backup_smis_lab_harga", "");
        $data['rencana_smis_lab_harga']=getSettings($db, "rencana_smis_lab_harga", "");
        $data['smis_lab_harga']=getSettings($db, "smis_lab_harga", "");
	}
	if($_POST['ruangan']=="Radiology" || $_POST['ruangan']==" - - Semua - - "){
		$data['backup_smis_rad_harga']=getSettings($db, "backup_smis_rad_harga", "");
        $data['rencana_smis_rad_harga']=getSettings($db, "rencana_smis_rad_harga", "");
        $data['smis_rad_harga']=getSettings($db, "smis_rad_harga", "");
	}	
	if($_POST['ruangan']=="Fisiotherapy" || $_POST['ruangan']==" - - Semua - - "){
        $data['backup_smis_fst_harga']=getSettings($db, "backup_smis_fst_harga", "");
        $data['rencana_smis_fst_harga']=getSettings($db, "rencana_smis_fst_harga", "");
        $data['smis_fst_harga']=getSettings($db, "smis_fst_harga", "");
	}
    
    $settings=array();
    $settings['settings']=$data;
    
    $serv=new ServiceConsumer($db,"duplicate_fst",$settings,"manajemen");
    $serv->execute();
    $serv->setMode(ServiceConsumer::$CLEAN_BOTH);
    $content=$serv->getContent();
	$success=true;
    foreach($content as $v){
        if($v!="1"){
            $success=false;
        }
    }
    
	$res=new ResponsePackage();
	$res->setStatus(ResponsePackage::$STATUS_OK);
	$res->setAlertVisible(true);
	$res->setContent("");
	if($success){
        $res->setAlertContent("Berhasil", "Data Berhasil di Synchronkan");    
    }else{
        $res->setAlertContent("Gagal", "Data Gagal di Synchronkan");
    }
    echo json_encode($res->getPackage());
	return;
}

$option=new OptionBuilder();
$option->addSingle(" - - Semua - - ");
$option->addSingle("Laboratory");
$option->addSingle("Radiology");
$option->addSingle("Fisiotherapy");
$select=new Select("ruangan", "", $option->getContent());

$lama=new Button("", "", "Backup");
$lama->setIsButton(Button::$ICONIC_TEXT);
$lama->setIcon(" fa fa-backward");
$lama->setClass(" btn-primary ");
$lama->setAction("terapkan.backup()");
$restore=new Button("", "", "Restore");
$restore->setIsButton(Button::$ICONIC_TEXT);
$restore->setClass(" btn-primary ");
$restore->setIcon(" fa fa-forward");
$restore->setAction("terapkan.restore()");
$baru=new Button("", "", "Terapkan");
$baru->setIsButton(Button::$ICONIC_TEXT);
$baru->setClass(" btn-primary ");
$baru->setIcon(" fa fa-stop");
$baru->setAction("terapkan.terapkan()");

$synch=new Button("", "", "Synch");
$synch->setIsButton(Button::$ICONIC_TEXT);
$synch->setClass(" btn-primary ");
$synch->setIcon(" fa fa-circle-o-notch");
$synch->setAction("terapkan.synch()");

$form=new Form("", "", "");
$form->addElement("Ruangan", $select);
$form->addElement("", $lama);
$form->addElement("", $restore);
$form->addElement("", $baru);
$form->addElement("", $synch);

if (isset ( $_POST ['command'] )) {
	$adapter = new SimpleAdapter ();
	$adapter->add ( "ID", "id" );
	$adapter->add ( "Name", "name" );
	$adapter->add ( "Value", "value" );
	$dbtable = new DBTable ( $db, "smis_mjm_terapkan" );
	$dbtable->setShowAll(true);
	$dbtable->setFetchMethode ( DBTable::$ARRAY_FETCH );
	$dbres = new DBResponder ( $dbtable, $uitable, $adapter );
	$data = $dbres->command ( $_POST ['command'] );
	echo json_encode ( $data );
	return;
}


echo $form->getHtml();
echo $uitable->getHtml ();
echo addJS ( "framework/smis/js/table_action.js" );

?>
<script type="text/javascript">
var terapkan;
$(document).ready(function(){
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
	var column=new Array('id','nama','keterangan','slug');
	terapkan=new TableAction("terapkan","manajemen","terapkan",column);
	terapkan.view();

	terapkan.backup=function(){
		 var a=this.getRegulerData();			
		bootbox.confirm("Peringatan, Proses ini Tidak Dapat di Kembalikan ?", function(result) {
			  if(result){
					a['command']="backup";
					a['ruangan']=$("#ruangan").val();
					showLoading();
					$.post("",a,function(res){
						var json=getContent(res);
						terapkan.view();
						dismissLoading();
					});
			}
		});
	};

	terapkan.terapkan=function(){
		 var a=this.getRegulerData();
		bootbox.confirm("Peringatan, Proses ini Tidak Dapat di Kembalikan ?", function(result) {
			  if(result){
				 	a['command']="terapkan";
				 	a['ruangan']=$("#ruangan").val();
					showLoading();
					$.post("",a,function(res){
						var json=getContent(res);
						terapkan.view();
						dismissLoading();
					});
			}
		});
	};

	terapkan.restore=function(){
		 var a=this.getRegulerData();
		bootbox.confirm("Peringatan, Proses ini Tidak Dapat di Kembalikan ?", function(result) {
			  if(result){
				 	a['command']="restore";
				 	a['ruangan']=$("#ruangan").val();
					showLoading();
					$.post("",a,function(res){
						var json=getContent(res);
						terapkan.view();
						dismissLoading();
					});
			}
		});
	};
    
    terapkan.synch=function(){
		var a=this.getRegulerData();
		a['command']="synch";
        a['ruangan']=$("#ruangan").val();
        showLoading();
        $.post("",a,function(res){
            var json=getContent(res);
            terapkan.view();
            dismissLoading();
        });
	};
});
</script>
