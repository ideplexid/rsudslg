<?php
global $db;
require_once ("smis-base/smis-include-service-consumer.php");
$uitable = new Table ( array (
		'Nama',
		'Kelas',
		'Tarif' 
), "Tarif Kamar Bersalin", NULL, true );
$uitable->setName ( "tarif_vk" );
if (isset ( $_POST ['command'] )) {
	$adapter = new SimpleAdapter ();
	$adapter->add ( "Nama", "nama" );
	$adapter->add ( "Kelas", "kelas", "unslug" );
	$adapter->add ( "Tarif", "tarif", "money Rp." );
	$dbtable = new DBTable ( $db, "smis_mjm_vk" );
	$dbtable->setFetchMethode ( DBTable::$ARRAY_FETCH );
	$dbres = new DBResponder ( $dbtable, $uitable, $adapter );
	$data = $dbres->command ( $_POST ['command'] );
	echo json_encode ( $data );
	return;
}

$dbtable = new DBTable ( $db, "smis_mjm_kelas" );
$dbtable->setShowAll ( true );
$data = $dbtable->view ( "", "0" );
$select = new SelectAdapter ( "nama", "slug" );
$kelas = $select->getContent ( $data ['data'] );

$uitable->addModal ( "id", "hidden", "", "" );
$uitable->addModal ( "nama", "text", "Nama", "" );
$uitable->addModal ( "kelas", "select", "Kelas", $kelas );
$uitable->addModal ( "tarif", "money", "Tarif", "" );
$modal = $uitable->getModal ();
$modal->setTitle ( "Tarif Kamar Bersalin" );

echo $uitable->getHtml ();
echo $modal->getHtml ();
echo addJS ( "framework/smis/js/table_action.js" );

?>
<script type="text/javascript">
var tarif_vk;
$(document).ready(function(){
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
	var column=new Array('id','kelas',"tarif",'nama');
	tarif_vk=new TableAction("tarif_vk","manajemen","tarif_vk",column);	
	tarif_vk.view();	
});
</script>
