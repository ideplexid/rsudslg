<?php
$uitable = new Table ( array ('Nama','Slug','Keterangan'), "Kelas", NULL, true );
$uitable->setName ( "kelas" );
if (isset ( $_POST ['command'] )) {
	$adapter = new SimpleAdapter ();
	$adapter->add ( "Nama", "nama" );
	$adapter->add ( "Slug", "slug" );
	$adapter->add ( "Keterangan", "keterangan" );
	$dbtable = new DBTable ( $db, "smis_mjm_kelas" );
	$dbtable->setFetchMethode ( DBTable::$ARRAY_FETCH );
	$dbres = new DBResponder ( $dbtable, $uitable, $adapter );
	$data = $dbres->command ( $_POST ['command'] );
	echo json_encode ( $data );
	return;
}

$uitable->addModal ( "id", "hidden", "", "" );
$uitable->addModal ( "nama", "text", "Nama", "" );
$uitable->addModal ( "slug", "text", "Slug", "" );
$uitable->addModal ( "keterangan", "textarea", "Keterangan", "" );
// $uitable->addModal("test", "chooser-kelas-employee", "Chooser", "");
$modal = $uitable->getModal ();
$modal->setTitle ( "kelas" );

echo $uitable->getHtml ();
echo $modal->getHtml ();
echo addJS ( "framework/smis/js/table_action.js" );

?>
<script type="text/javascript">
var kelas;
$(document).ready(function(){
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
	var column=new Array('id','nama','keterangan','slug');
	kelas=new TableAction("kelas","manajemen","kelas",column);
	kelas.view();
	
});
</script>
