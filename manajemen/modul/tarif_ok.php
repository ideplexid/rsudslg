<?php
global $db;
require_once ("smis-base/smis-include-service-consumer.php");
$uitable = new Table ( array (
		'Nama',
		'Kelas',
		'Tarif Lama',
		'Tarif Sekarang',		
		'Rencana Tarif Baru' 
), "Tarif Kamar Operasi", NULL, true );

if(isset ( $_POST ['command'] ) && $_POST ['command']=="backup"){
	$query="UPDATE smis_mjm_ok SET tarif_lama=tarif;";
	$db->query($query);
	$res=new ResponsePackage();
	$res->setStatus(ResponsePackage::$STATUS_OK);
	$res->setAlertVisible(true);
	$res->setContent("");
	$res->setAlertContent("Berhasil", "Berhasil di Backup");
	echo json_encode($res->getPackage());
	return;
}

if(isset ( $_POST ['command'] ) && $_POST ['command']=="restore"){
	$query="UPDATE smis_mjm_ok SET tarif=tarif_lama;";
	$db->query($query);
	$res=new ResponsePackage();
	$res->setStatus(ResponsePackage::$STATUS_OK);
	$res->setAlertVisible(true);
	$res->setContent("");
	$res->setAlertContent("Berhasil", "Berhasil di Backup");
	echo json_encode($res->getPackage());
	return;
}

if(isset ( $_POST ['command'] ) && $_POST ['command']=="terapkan"){
	$query="UPDATE smis_mjm_ok SET tarif=tarif_baru;";
	$db->query($query);
	$res=new ResponsePackage();
	$res->setStatus(ResponsePackage::$STATUS_OK);
	$res->setAlertVisible(true);
	$res->setContent("");
	$res->setAlertContent("Berhasil", "Berhasil di Terapkan");
	echo json_encode($res->getPackage());
	return;
}


$uitable->setName ( "tarif_ok" );
if (isset ( $_POST ['command'] )) {
	$adapter = new SimpleAdapter ();
	$adapter->add ( "Nama", "nama" );
	$adapter->add ( "Kelas", "kelas", "unslug" );
	$adapter->add ( "Tarif Sekarang", "tarif", "money Rp." );
	$adapter->add ( "Tarif Lama", "tarif_lama", "money Rp." );
	$adapter->add ( "Rencana Tarif Baru", "tarif_baru", "money Rp." );
	$dbtable = new DBTable ( $db, "smis_mjm_ok" );
	$dbtable->setFetchMethode ( DBTable::$ARRAY_FETCH );
	$dbres = new DBResponder ( $dbtable, $uitable, $adapter );
	$data = $dbres->command ( $_POST ['command'] );
	echo json_encode ( $data );
	return;
}

$lama=new Button("", "", "Backup");
$lama->setIsButton(Button::$ICONIC_TEXT);
$lama->setIcon(" fa fa-backward");
$lama->setClass(" btn-primary ");
$lama->setAction("tarif_ok.backup()");
$restore=new Button("", "", "Restore");
$restore->setIsButton(Button::$ICONIC_TEXT);
$restore->setClass(" btn-primary ");
$restore->setIcon(" fa fa-forward");
$restore->setAction("tarif_ok.restore()");
$baru=new Button("", "", "Terapkan");
$baru->setIsButton(Button::$ICONIC_TEXT);
$baru->setClass(" btn-primary ");
$baru->setIcon(" fa fa-stop");
$baru->setAction("tarif_ok.terapkan()");
$uitable->addFooterButton($lama);
$uitable->addFooterButton($restore);
$uitable->addFooterButton($baru);

$dbtable = new DBTable ( $db, "smis_mjm_kelas" );
$dbtable->setShowAll ( true );
$data = $dbtable->view ( "", "0" );
$select = new SelectAdapter ( "nama", "slug" );
$kelas = $select->getContent ( $data ['data'] );

$uitable->addModal ( "id", "hidden", "", "" );
$uitable->addModal ( "nama", "text", "Nama", "" );
$uitable->addModal ( "kelas", "select", "Kelas", $kelas );
$uitable->addModal ( "tarif", "money", "Tarif", "" );
$modal = $uitable->getModal ();
$modal->setTitle ( "Tarif Kamar Operasi" );

echo $uitable->getHtml ();
echo $modal->getHtml ();
echo addJS ( "framework/smis/js/table_action.js" );



?>
<script type="text/javascript">
var tarif_ok;
$(document).ready(function(){
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
	var column=new Array('id','kelas',"tarif",'nama','tarif_lama','tarif_baru');
	tarif_ok=new TableAction("tarif_ok","manajemen","tarif_ok",column);	
	tarif_ok.view();	
	tarif_ok.backup=function(){
		 var a=this.getRegulerData();			
		bootbox.confirm("Peringatan, Proses ini Tidak Dapat di Kembalikan ?", function(result) {
			  if(result){
					a['command']="backup";
					showLoading();
					$.post("",a,function(res){
						var json=getContent(res);
						tarif_ok.view();
						dismissLoading();
					});
			}
		});
	};

	tarif_ok.terapkan=function(){
		 var a=this.getRegulerData();
		bootbox.confirm("Peringatan, Proses ini Tidak Dapat di Kembalikan ?", function(result) {
			  if(result){
				 	a['command']="terapkan";
					showLoading();
					$.post("",a,function(res){
						var json=getContent(res);
						tarif_ok.view();
						dismissLoading();
					});
			}
		});
	};

	tarif_ok.restore=function(){
		 var a=this.getRegulerData();
		bootbox.confirm("Peringatan, Proses ini Tidak Dapat di Kembalikan ?", function(result) {
			  if(result){
				 	a['command']="restore";
					showLoading();
					$.post("",a,function(res){
						var json=getContent(res);
						tarif_ok.view();
						dismissLoading();
					});
			}
		});
	};
});
</script>
