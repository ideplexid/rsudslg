<?php 

global $db;
$uitable=new Table(array("No.","Nama","Kelas","Radiology","Laboratory","Fisiotherapy","Elektromedis","Total","Tarif Manual"),"Paket");
$uitable->setName("paket_rlfe");
if(isset($_POST['command']) && $_POST['command']!=""){
    require_once "manajemen/class/responder/PaketResponder.php";
    $simple=new SimpleAdapter();
    $simple->add("Nama","nama");
    $simple->add("Tarif Manual","tarif_manual","money Rp.");
    $simple->add("Total","total","money Rp.");
    $simple->add("Radiology","total_radiology","money Rp.");
    $simple->add("Laboratory","total_laboratory","money Rp.");
    $simple->add("Fisiotherapy","total_fisiotherapy","money Rp.");
    $simple->add("Elektromedis","total_elektromedis","money Rp.");
    $simple->add("Kelas","kelas","unslug");
    $simple->setUseNumber(true,"No.","back.");
    $dbtable=new DBTable($db,"smis_mjm_paket");
    $dbres=new PaketResponder($dbtable,$uitable,$simple);
    $data=$dbres->command($_POST['command']);
    echo json_encode($data);
    return;
}

$dbtable=new DBTable($db,"smis_mjm_kelas");
$dbtable->setShowAll(true);
$dt=$dbtable->view("",0);
$list=$dt['data'];

$secl=new SelectAdapter("nama","slug");
$cont=$secl->getContent($list);

require_once "smis-base/smis-include-service-consumer.php";
$entity=array("fisiotherapy","radiology","laboratory","elektromedis");
$serv=new ServiceConsumer($db,"get_list_paket",null,$entity);
$serv->setMode(ServiceConsumer::$KEY_ENTITY);
$serv->execute();
$content=$serv->getContent();
$uitable->addModal("id","hidden","","");
$uitable->addModal("nama","text","Nama Paket","");
$uitable->addModal("kelas","select","Kelas Paket",$cont);
$uitable->addModal("keterangan","textarea","Keterangan","");
$uitable->addModal("tarif_manual","money","Tarif Manual","");
$uitable->addModal ( "nominal_jaspel", "money", "Nominal Jaspel", "" );
$uitable->addModal ( "persen_jaspel", "text", "Persen Jaspel", "" );
$uitable->addModal ( "nominal_rs", "money", "Nominal BHP & RS", "" );
$uitable->addModal ( "persen_rs", "text", "Persen BHP & RS", "" );

$uitable->addModal("","label","<strong>Periksa Dokter</strong>","");
$uitable->addModal("total_periksa","money","Nilai Tarif Periksa","");
$uitable->addModal("bagi_periksa","money","Bagi Dokter Periksa","");
$uitable->addModal("","label","<strong>Konsul Dokter</strong>","");
$uitable->addModal("total_konsul","money","Nilai Tarif Konsul","");
$uitable->addModal("bagi_konsul","money","Bagi Dokter Konsul","");
    
foreach($content as $entity=>$list){
    if($list==null) 
        continue;
    $uitable->addModal("","label","<strong>".ArrayAdapter::slugFormat("unslug",$entity)."</strong>","");
    foreach($list as $key=>$name){
        $uitable->addModal($key,"checkbox",$name,"0");
    }
}

$modal=$uitable->getModal();
$modal->setTitle("Paket");
$modal->setModalSize(Modal::$HALF_MODEL);
$modal->setComponentSize(Modal::$BIG);
$paket_array=new Hidden("list_nama_paket","",json_encode($content));

echo $modal->getHtml();
echo $uitable->getHtml();
echo $paket_array->getHtml();

echo addJS ( "framework/smis/js/table_action.js" );
echo addJS ( "manajemen/resource/js/paket_rlfe.js",false );
?>