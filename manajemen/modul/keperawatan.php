<?php 
$tabs=new Tabulator("keperawatan", "Keperawatan");
$tabs->add("tarif_keperawatan", "Tarif Keperawatan Rawat Jalan", "",Tabulator::$TYPE_HTML,"fa fa-money");
$tabs->add("tarif_keperawatan_inap", "Tarif Keperawatan Rawat Inap", "",Tabulator::$TYPE_HTML,"fa fa-money");
$tabs->add("derivatif_keperawatan", "Derivatif", "",Tabulator::$TYPE_HTML,"fa fa-share-alt");
$tabs->setPartialLoad(true,"manajemen","keperawatan","keperawatan",true);
echo $tabs->getHtml();
?>