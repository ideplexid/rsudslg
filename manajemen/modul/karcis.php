<?php
require_once ("smis-base/smis-include-service-consumer.php");
require_once 'smis-framework/smis/api/SettingsBuilder.php';
require_once "manajemen/class/settings/SettingsBuilderKarcis.php";
global $db;

$setting = new SettingsBuilderKarcis ( $db, "karcis", "manajemen", "karcis", "Karcis dan Administrasi" );
$RUANGAN = array ();
$urjip = new ServiceConsumer ( $db, "get_urjip", array () );
$urjip->setMode ( ServiceConsumer::$MULTIPLE_MODE );
$urjip->setCached(true,"get_urjip");
$urjip->execute ();
$content = $urjip->getContent ();
foreach ( $content as $autonomous => $ruang ) {
    foreach ( $ruang as $nama_ruang => $jip ) {
        $RUANGAN [$nama_ruang] = $jip [$nama_ruang];
    }
}

$setting->setShowDescription ( false );
$setting->addTabs ( "URJ", "Karcis URJ"," fa fa-ticket" );
if($setting->isGroup("URJ")){
    foreach ( $RUANGAN as $r => $jip ) {
        if ($jip == "URJ" || $jip == "URJI") {
            $setting->addSection($jip,ArrayAdapter::format("unslug",  $r ));
            $setting->addItem ( $jip, new SettingsItem ( $db, "smis-rs-karcis-baru-" . $r, "<strong>KARCIS PENDAFTARAN RUANG ".ArrayAdapter::format("unslug",  $r ) ." PASIEN BARU </strong>", "0", "money" ) );
            $setting->addItem ( $jip, new SettingsItem ( $db, "smis-rs-karcis-baru-jaspel-nominal-" . $r, " * Breakdown Nominal Jaspel KARCIS PENDAFTARAN RUANG ".ArrayAdapter::format("unslug",  $r ) ." PASIEN BARU ", "0", "money" ) );
            $setting->addItem ( $jip, new SettingsItem ( $db, "smis-rs-karcis-baru-jaspel-persen-" . $r, " * Breakdown Persen Jaspel KARCIS PENDAFTARAN RUANG ".ArrayAdapter::format("unslug",  $r ) ." PASIEN BARU ", "0", "text" ) );
            $setting->addItem ( $jip, new SettingsItem ( $db, "smis-rs-karcis-baru-rs-nominal-" . $r, " * Breakdown Nominal BHP & RS KARCIS PENDAFTARAN RUANG ".ArrayAdapter::format("unslug",  $r ) ." PASIEN BARU ", "0", "money" ) );
            $setting->addItem ( $jip, new SettingsItem ( $db, "smis-rs-karcis-baru-rs-persen-" . $r, " * Breakdown Persen BHP & RS KARCIS PENDAFTARAN RUANG ".ArrayAdapter::format("unslug",  $r ) ." PASIEN BARU ", "0", "text" ) );
            
            $setting->addItem ( $jip, new SettingsItem ( $db, "smis-rs-karcis-lama-" . $r, "<strong>KARCIS PENDAFTARAN RUANG ".ArrayAdapter::format("unslug", $r ) ." PASIEN LAMA</strong>", "0", "money" ) );
            $setting->addItem ( $jip, new SettingsItem ( $db, "smis-rs-karcis-lama-jaspel-nominal-" . $r, " * Breakdown Nominal Jaspel KARCIS PENDAFTARAN RUANG ".ArrayAdapter::format("unslug",  $r ) ." PASIEN LAMA ", "0", "money" ) );
            $setting->addItem ( $jip, new SettingsItem ( $db, "smis-rs-karcis-lama-jaspel-persen-" . $r, " * Breakdown Persen Jaspel KARCIS PENDAFTARAN RUANG ".ArrayAdapter::format("unslug",  $r ) ." PASIEN LAMA ", "0", "text" ) );
            $setting->addItem ( $jip, new SettingsItem ( $db, "smis-rs-karcis-lama-rs-nominal-" . $r, " * Breakdown Nominal BHP & RS KARCIS PENDAFTARAN RUANG ".ArrayAdapter::format("unslug",  $r ) ." PASIEN LAMA ", "0", "money" ) );
            $setting->addItem ( $jip, new SettingsItem ( $db, "smis-rs-karcis-lama-rs-persen-" . $r, " * Breakdown Persen BHP & RS KARCIS PENDAFTARAN RUANG ".ArrayAdapter::format("unslug",  $r ) ." PASIEN LAMA ", "0", "text" ) );


        }
    }
}

$setting->addTabs ( "URI", "Administrasi URI"," fa fa-money" );
if($setting->isGroup("URI")){
    foreach ( $RUANGAN as $r => $jip ) {
        if ($jip == "URI" || $jip == "URJI") {
            $setting->addSection($jip,ArrayAdapter::format("unslug",  $r ));
            $setting->addItem ( $jip, new SettingsItem ( $db, "smis-rs-administrasi-baru-" . $r, "<strong>BIAYA ADMINISTRASI RUANG ".ArrayAdapter::format("unslug",  $r ) ." PASIEN BARU</strong>", "0", "money" ) );
            $setting->addItem ( $jip, new SettingsItem ( $db, "smis-rs-administrasi-baru-jaspel-nominal-" . $r, " * Breakdown Nominal Jaspel BIAYA ADMINISTRASI RUANG ".ArrayAdapter::format("unslug",  $r ) ." PASIEN BARU ", "0", "money" ) );
            $setting->addItem ( $jip, new SettingsItem ( $db, "smis-rs-administrasi-baru-jaspel-persen-" . $r, " * Breakdown Persen Jaspel BIAYA ADMINISTRASI RUANG ".ArrayAdapter::format("unslug",  $r ) ." PASIEN BARU ", "0", "text" ) );
            $setting->addItem ( $jip, new SettingsItem ( $db, "smis-rs-administrasi-baru-rs-nominal-" . $r, " * Breakdown Nominal BHP & RS BIAYA ADMINISTRASI RUANG ".ArrayAdapter::format("unslug",  $r ) ." PASIEN BARU ", "0", "money" ) );
            $setting->addItem ( $jip, new SettingsItem ( $db, "smis-rs-administrasi-baru-rs-persen-" . $r, " * Breakdown Persen BHP & RS BIAYA ADMINISTRASI RUANG ".ArrayAdapter::format("unslug",  $r ) ." PASIEN BARU ", "0", "text" ) );
            
            $setting->addItem ( $jip, new SettingsItem ( $db, "smis-rs-administrasi-lama-" . $r, "<strong>BIAYA ADMINISTRASI RUANG ".ArrayAdapter::format("unslug", $r ) ." PASIEN LAMA</strong>", "0", "money" ) );
            $setting->addItem ( $jip, new SettingsItem ( $db, "smis-rs-administrasi-lama-jaspel-nominal-" . $r, " * Breakdown Nominal Jaspel BIAYA ADMINISTRASI RUANG ".ArrayAdapter::format("unslug",  $r ) ." PASIEN LAMA ", "0", "money" ) );
            $setting->addItem ( $jip, new SettingsItem ( $db, "smis-rs-administrasi-lama-jaspel-persen-" . $r, " * Breakdown Persen Jaspel BIAYA ADMINISTRASI RUANG ".ArrayAdapter::format("unslug",  $r ) ." PASIEN LAMA ", "0", "text" ) );
            $setting->addItem ( $jip, new SettingsItem ( $db, "smis-rs-administrasi-lama-rs-nominal-" . $r, " * Breakdown Nominal BHP & RS BIAYA ADMINISTRASI RUANG ".ArrayAdapter::format("unslug",  $r ) ." PASIEN LAMA ", "0", "money" ) );
            $setting->addItem ( $jip, new SettingsItem ( $db, "smis-rs-administrasi-lama-rs-persen-" . $r, " * Breakdown Persen BHP & RS BIAYA ADMINISTRASI RUANG ".ArrayAdapter::format("unslug",  $r ) ." PASIEN LAMA ", "0", "text" ) );
        }
    }
}

$setting->addTabs ( "UP", "Penunjang"," fa fa-wheelchair-alt" );
if($setting->isGroup("UP")){
    foreach ( $RUANGAN as $r => $jip ) {
        if ($jip == "UP") {
            $setting->addItem ( $jip, new SettingsItem ( $db, "smis-rs-karcis-baru-" . $r, "<strong>KARCIS PENDAFTARAN RUANG ".ArrayAdapter::format("unslug",  $r ) ." PASIEN BARU </strong>", "0", "money" ) );
            $setting->addItem ( $jip, new SettingsItem ( $db, "smis-rs-karcis-baru-jaspel-nominal-" . $r, " * Breakdown Nominal Jaspel KARCIS PENDAFTARAN RUANG ".ArrayAdapter::format("unslug",  $r ) ." PASIEN BARU ", "0", "money" ) );
            $setting->addItem ( $jip, new SettingsItem ( $db, "smis-rs-karcis-baru-jaspel-persen-" . $r, " * Breakdown Persen Jaspel KARCIS PENDAFTARAN RUANG ".ArrayAdapter::format("unslug",  $r ) ." PASIEN BARU ", "0", "text" ) );
            $setting->addItem ( $jip, new SettingsItem ( $db, "smis-rs-karcis-baru-rs-nominal-" . $r, " * Breakdown Nominal BHP & RS KARCIS PENDAFTARAN RUANG ".ArrayAdapter::format("unslug",  $r ) ." PASIEN BARU ", "0", "money" ) );
            $setting->addItem ( $jip, new SettingsItem ( $db, "smis-rs-karcis-baru-rs-persen-" . $r, " * Breakdown Persen BHP & RS KARCIS PENDAFTARAN RUANG ".ArrayAdapter::format("unslug",  $r ) ." PASIEN BARU ", "0", "text" ) );
            
            $setting->addItem ( $jip, new SettingsItem ( $db, "smis-rs-karcis-lama-" . $r, "<strong>KARCIS PENDAFTARAN RUANG ".ArrayAdapter::format("unslug", $r ) ." PASIEN LAMA</strong>", "0", "money" ) );
            $setting->addItem ( $jip, new SettingsItem ( $db, "smis-rs-karcis-lama-jaspel-nominal-" . $r, " * Breakdown Nominal Jaspel KARCIS PENDAFTARAN RUANG ".ArrayAdapter::format("unslug", $r ) ." PASIEN LAMA", "0", "money" ) );
            $setting->addItem ( $jip, new SettingsItem ( $db, "smis-rs-karcis-lama-jaspel-persen-" . $r, " * Breakdown Persen Jaspel KARCIS PENDAFTARAN RUANG ".ArrayAdapter::format("unslug", $r ) ." PASIEN LAMA", "0", "text" ) );
            $setting->addItem ( $jip, new SettingsItem ( $db, "smis-rs-karcis-lama-rs-nominal-" . $r, " * Breakdown Nominal BHP & RS KARCIS PENDAFTARAN RUANG ".ArrayAdapter::format("unslug", $r ) ." PASIEN LAMA", "0", "money" ) );
            $setting->addItem ( $jip, new SettingsItem ( $db, "smis-rs-karcis-lama-rs-persen-" . $r, " * Breakdown Persen BHP & RS KARCIS PENDAFTARAN RUANG ".ArrayAdapter::format("unslug", $r ) ." PASIEN LAMA", "0", "text" ) );
        }
    }
}

$setting->addTabs ( "konsultasi", "Periksa"," fa fa-stethoscope" );
if($setting->isGroup("konsultasi")){
    foreach ( $RUANGAN as $r => $jip ) {
        if ($jip == "URJ" || $jip=="URJI") {
            $setting->addItem ( "konsultasi", new SettingsItem ( $db, "smis-rs-uri-konsultasi-" . $r, "<strong>TARIF PERIKSA ".ArrayAdapter::format("unslug", $r )."</strong>", "0", "money" ) );
            $setting->addItem ( "konsultasi", new SettingsItem ( $db, "smis-rs-uri-konsultasi-jaspel-nominal-" . $r, " * Breakdown Nominal Jaspel Dokter TARIF PERIKSA ".ArrayAdapter::format("unslug", $r ), "0", "money" ) );
            $setting->addItem ( "konsultasi", new SettingsItem ( $db, "smis-rs-uri-konsultasi-jaspel-persen-" . $r, " * Breakdown Persen Jaspel Dokter TARIF PERIKSA ".ArrayAdapter::format("unslug", $r ), "0", "text" ) );
            $setting->addItem ( "konsultasi", new SettingsItem ( $db, "smis-rs-uri-konsultasi-rs-nominal-" . $r, " * Breakdown Nominal BHP & RS TARIF PERIKSA ".ArrayAdapter::format("unslug", $r ), "0", "money" ) );
            $setting->addItem ( "konsultasi", new SettingsItem ( $db, "smis-rs-uri-konsultasi-rs-persen-" . $r, " * Breakdown Persen BHP & RS TARIF PERIKSA ".ArrayAdapter::format("unslug", $r ), "0", "text" ) );
            $setting->addItem ( "konsultasi", new SettingsItem ( $db, "smis-rs-uri-konsultasi-jaspel-nominal-perawat-" . $r, " * Breakdown Nominal Jaspel Perawat TARIF PERIKSA ".ArrayAdapter::format("unslug", $r ), "0", "money" ) );
            $setting->addItem ( "konsultasi", new SettingsItem ( $db, "smis-rs-uri-konsultasi-jaspel-persen-perawat-" . $r, " * Breakdown Persen Jaspel Perawat TARIF PERIKSA ".ArrayAdapter::format("unslug", $r ), "0", "text" ) );
            
        }
    }
}

$setting->addTabs ( "bed", "Kamar Inap"," fa fa-bed" );
if($setting->isGroup("bed")){
    foreach ( $RUANGAN as $r => $jip ) {
        if ($jip == "URI" || $jip=="URJI") {
            $setting->addItem ( "bed", new SettingsItem ( $db, "smis-rs-uri-bed-" . $r, "TARIF KAMAR ".ArrayAdapter::format("unslug", $r ), "0", "money" ) );
        }
    }
}

$setting->addTabs ( "oksigen", "Oksigen"," fa fa-fire-extinguisher" );
if($setting->isGroup("oksigen")){
    foreach ( $RUANGAN as $r => $jip ) {
        if ($jip == "URI" || $jip == "URJ" || $jip == "URJI") {
            $setting->addItem ( "oksigen", new SettingsItem ( $db, "smis-rs-oksigen-liter-".$r, "Oksigen / Liter ".ArrayAdapter::format("unslug", $r ), "0", "money" ) );
        } 
    }
    $setting->addItem ( "oksigen", new SettingsItem ( $db, "smis-rs-oksigen-jam", "Oksigen / Jam", "0", "money" ) );
    $setting->addItem ( "oksigen", new SettingsItem ( $db, "smis-rs-oksigen-menit", "Oksigen / Menit", "0", "money" ) );
}

$setting->addTabs ( "apotik", "Penjulaan Obat"," fa fa-medkit" );
if($setting->isGroup("apotik")){
    $setting->addSection("apotik","Mark Up");
    $setting->addItem ( "apotik", new SettingsItem ( $db, "smis-rs-apotek-return", "Pesentase Return", "0", "text" ) );
    $setting->addItem ( "apotik", new SettingsItem ( $db, "smis-rs-markup-tablet", "Pesentase Markup Tablet", "0", "text" ) );
    $setting->addItem ( "apotik", new SettingsItem ( $db, "smis-rs-markup-ntablet", "Pesentase Markup Non-Tablet", "0", "text" ) );
    $setting->addSection("apotik","Penjualan Resep");
    $setting->addItem ( "apotik", new SettingsItem ( $db, "smis-rs-resep-persen-rs", "Persentase RS & BHP", "0", "text" ) );
    $setting->addItem ( "apotik", new SettingsItem ( $db, "smis-rs-resep-persen-jaspel", "Pesentase Jasa Pelayanan", "0", "text" ) );
    $setting->addSection("apotik","Visite Farmasi");
    $setting->addItem ( "apotik", new SettingsItem ( $db, "smis-rs-visite-farmasi-persen-rs", "Persentase RS & BHP", "0", "text" ) );
    $setting->addItem ( "apotik", new SettingsItem ( $db, "smis-rs-visite-farmasi-persen-jaspel", "Pesentase Jasa Pelayanan", "0", "text" ) );
    $setting->addSection("apotik","Asuhan Farmasi");
    $setting->addItem ( "apotik", new SettingsItem ( $db, "smis-rs-asuhan-farmasi-persen-rs", "Persentase RS & BHP", "0", "text" ) );
    $setting->addItem ( "apotik", new SettingsItem ( $db, "smis-rs-asuhan-farmasi-persen-jaspel", "Pesentase Jasa Pelayanan", "0", "text" ) );
    
}
$setting->setPartialLoad(true);
$setting->init ();