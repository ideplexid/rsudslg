<?php
global $db;
require_once ("smis-base/smis-include-service-consumer.php");
$uitable = new Table ( array ('Durasi','Liter','Tarif'), "Tarif Oksigen", NULL, true );
$uitable->setName ( "tarif_oksigen" );
if (isset ( $_POST ['command'] )) {
	$adapter = new SimpleAdapter ();
	$adapter->add ( "Durasi", "durasi" );
	$adapter->add ( "Liter", "liter", "unslug" );
	$adapter->add ( "Tarif", "tarif", "money Rp." );
	$dbtable = new DBTable ( $db, "smis_mjm_oksigen" );
	$dbtable->setFetchMethode ( DBTable::$ARRAY_FETCH );
	$dbres = new DBResponder ( $dbtable, $uitable, $adapter );
	$data = $dbres->command ( $_POST ['command'] );
	echo json_encode ( $data );
	return;
}
$uitable->addModal ( "id", "hidden", "", "" );
$uitable->addModal ( "durasi", "text", "Durasi", "" );
$uitable->addModal ( "liter", "text", "Liter", "" );
$uitable->addModal ( "tarif", "money", "Tarif", "" );
$modal = $uitable->getModal ();
$modal->setTitle ( "Tarif Kamar Bersalin" );

echo $uitable->getHtml ();
echo $modal->getHtml ();
echo addJS ( "framework/smis/js/table_action.js" );

?>
<script type="text/javascript">
var tarif_oksigen;
$(document).ready(function(){
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
	var column=new Array('id','durasi',"liter",'tarif');
	tarif_oksigen=new TableAction("tarif_oksigen","manajemen","tarif_oksigen",column);	
	tarif_oksigen.view();	
});
</script>
