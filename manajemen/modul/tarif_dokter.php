<?php
$tab = new Tabulator ( "tarif", "Tarif Dokter", Tabulator::$POTRAIT );
$tab->add ( "tarif_konsul", "Konsul Dokter", "", Tabulator::$TYPE_HTML );
$tab->add ( "tarif_visite", "Visite Dokter", "", Tabulator::$TYPE_HTML );
$tab->add ( "tarif_tindakan_dokter_inap", "Tindakan Dokter Inap", "", Tabulator::$TYPE_HTML );
$tab->add ( "tarif_tindakan_dokter_jalan", "Tindakan Dokter Jalan", "", Tabulator::$TYPE_HTML );
$tab->add ( "tarif_tindakan_operasi", "Operatie Kamer (OK)", "", Tabulator::$TYPE_HTML );
$tab->add ( "tarif_tindakan_vk", "Verlos Kamer (VK)", "", Tabulator::$TYPE_HTML );
$tab->add ( "tarif_anastesi", "Anastesi", "", Tabulator::$TYPE_HTML );
$tab->setPartialLoad(true,"manajemen","tarif","tarif",true);
echo $tab->getHtml ();
?>