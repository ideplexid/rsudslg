<?php
global $PLUGINS;

$init ['name']              = 'manajemen';
$init ['path']              = SMIS_DIR . "manajemen/";
$init ['description']       = "Manajemen";
$init ['require']           = "administrator";
$init ['service']           = "";
$init ['version']           = "1.3.9";
$init ['number']            = "39";
$init ['type']              = "";
$myplugins                  = new Plugin ( $init );
$PLUGINS [$init ['name']]   = $myplugins;