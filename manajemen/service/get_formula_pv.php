<?php
if (isset ( $_POST ['command'] )) {
	$dbtable = new DBTable ( $db, "smis_mjm_formula" );
	$dbtable->setUseWhereforView ( true );
    
    if(isset($_POST['grup_slug']) && $_POST['grup_slug']!=""){
        $dbtable->addCustomKriteria("perawat_".$_POST['grup_slug'],"!='0'");
    }
    
    if(getSettings($db,"manajer-service-used-filter","0")=="1" && 
        isset($_POST['ruangan']) && $_POST['ruangan']!=""){
        $dbtable->addCustomKriteria("ruangan"," LIKE '%[".$_POST['ruangan']."]%'");
    }
    
	$service = new ServiceProvider ( $dbtable );
	$pack = $service->command ( $_POST ['command'] );
	echo json_encode ( $pack );
}
?>