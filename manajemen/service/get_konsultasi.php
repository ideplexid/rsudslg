<?php
if (isset ( $_POST ['ruangan'] )) {
	global $db;
	$ruangan = $_POST ['ruangan'];
    $harga = getSettings ( $db, "smis-rs-uri-konsultasi-" . $ruangan, "50000" );
    $jaspel_nominal = getSettings ( $db, "smis-rs-uri-konsultasi-jaspel-nominal-" . $ruangan, "10" );
    $jaspel_persen = getSettings ( $db, "smis-rs-uri-konsultasi-jaspel-persen-" . $ruangan, "5000" );

    $jaspel_nominal_perawat = getSettings ( $db, "smis-rs-uri-konsultasi-jaspel-nominal-perawat-" . $ruangan, "10" );
    $jaspel_persen_perawat = getSettings ( $db, "smis-rs-uri-konsultasi-jaspel-persen-perawat-" . $ruangan, "5000" );

    $result = array(
        "harga" => $harga,
        "jaspel_nominal"=>$jaspel_nominal,
        "jaspel_persen"=>$jaspel_persen,
        "jaspel_nominal_perawat"=>$jaspel_nominal_perawat,
        "jaspel_persen_perawat"=>$jaspel_persen_perawat
    );

	echo json_encode ( $result );
}
?>