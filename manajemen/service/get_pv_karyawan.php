<?php
/**
 * provit sharing yang tersedia, berikut nama-nama dari provit sharing
 * ruang diperlukan kalau-kalau nantinya provit
 * sharing sangat tergantung dengan ruangan
 *
 * dokter tamu 			-> smis-pv-tindakan-dokter
 * dokter perawat 		-> smis-pv-tindakan-perawat
 * dokter perawat igd	-> smis-pv-tindakan-perawat-igd
 * konsul dokter 		-> smis-pv-konsul
 * konsultasi dokter 	-> smis-pv-konsultasi
 * visite dokter 		-> smis-pv-visite
 * OK ok 				-> smis-pv-ok
 * recovery room 		-> smis-pv-rr
 * fisiotherapy 		-> smis-pv-fisiotherapy
 * ekg 					-> smis-pv-ekg
 * faal paru 			-> smis-pv-fparu
 * endoscopy 			-> smis-pv-endoscopy
 * bronchoscopy 		-> smis-pv-bronchoscopy
 * audiometry 			-> smis-pv-audiometry
 * spirometry 			-> smis-pv-spirometry
 * laboratory 			-> smis-pv-laboratory
 * radiology 			-> smis-pv-radiology
 * 
 * @version 1.0.0
 * @since 25 oct 2014
 * @author goblooge
 */

global $db;
if (isset ( $_POST ['provit'] )) {
	$provit = $_POST ['provit'];
	$ruang = $_POST ['ruang'];
	if ($provit == "smis-pv-tindakan-dokter") {
		$dtamu = getSettings ( $db, "smis-provit-karyawan-dokter-dtamu", "90" );
		$dorganik = getSettings ( $db, "smis-provit-karyawan-dokter-dorganik", "70" );
		$sdtamu = getSettings ( $db, "smis-provit-karyawan-dokter-dtamu-sifat", "individual" );
		$sdorganik = getSettings ( $db, "smis-provit-karyawan-dokter-dorganik-sifat", "individual" );
		$asisten=getSettings ( $db, "smis-provit-karyawan-dokter-asisten-tindakan-dokter", "85" );
		$sasisten=getSettings ( $db, "smis-provit-karyawan-dokter-asisten-tindakan-dokter-sifat", "communal" );
		
		$array = array (
				"dtamu" => $dtamu,
				"dorganik" => $dorganik,
				"s-dtamu" => $sdtamu,
				"s-dorganik" => $sdorganik ,
				"asisten"=>$asisten,
				"s-asisten"=>$sasisten				
		);
		echo json_encode ( $array );
	} else if ($provit == "smis-pv-tindakan-perawat") {
		$perawat = getSettings ( $db, "smis-provit-karyawan-perawat", "51" );
		$sperawat = getSettings ( $db, "smis-provit-karyawan-perawat-sifat", "communal" );
		$array = array (
				"perawat" => $perawat,
				"s-perawat" => $sperawat 
		);
		echo json_encode ( $array );
	} else if ($provit == "smis-pv-tindakan-perawat-igd") {
		$dokter = getSettings ( $db, "smis-provit-karyawan-igd-dokter", "17.5" );
		$sdokter = getSettings ( $db, "smis-provit-karyawan-igd-dokter-sifat", "individual" );
		$perawat = getSettings ( $db, "smis-provit-karyawan-igd-perawat", "28" );
		$sperawat = getSettings ( $db, "smis-provit-karyawan-igd-perawat-sifat", "communal" );
		$array = array (
				"perawat" => $perawat,
				"s-perawat" => $sperawat, 
				"dokter" => $dokter,
				"s-dokter" => $sdokter
		);
		echo json_encode ( $array );
	} else if ($provit == "smis-pv-konsul") {
		$nilai = getSettings ( $db, "smis-provit-karyawan-dokter-konsul", "100" );
		$snilai = getSettings ( $db, "smis-provit-karyawan-dokter-konsul-sifat", "individual" );
		$array = array (
				"dokter" => $nilai,
				"s-dokter" => $snilai 
		);
		echo json_encode ( $array );
	} else if ($provit == "smis-pv-konsultasi") {
		$nilai = getSettings ( $db, "smis-provit-karyawan-dokter-konsultasi", "100" );
		$snilai = getSettings ( $db, "smis-provit-karyawan-dokter-konsultasi-sifat", "individual" );
		$array = array (
				"dokter" => $nilai,
				"s-dokter" => $snilai 
		);
		echo json_encode ( $array );
	} else if ($provit == "smis-pv-visite") {
		$nilai = getSettings ( $db, "smis-provit-karyawan-dokter-visite", "100" );
		$snilai = getSettings ( $db, "smis-provit-karyawan-dokter-visite-sifat", "individual" );
		$array = array (
				"dokter" => $nilai,
				"s-dokter" => $snilai 
		);
		echo json_encode ( $array );
	} else if ($provit == "smis-pv-ok") {
		$array = array ();
		$array ['dtamu'] = getSettings ( $db, "smis-provit-karyawan-ok-dtamu", "90" );
		$array ['dorganik'] = getSettings ( $db, "smis-provit-karyawan-ok-dorganik", "70" );
		$array ['anastesi-hadir'] = getSettings ( $db, "smis-provit-karyawan-ok-anastesi-hadir", "50" );
		$array ['anatesi-thadir'] = getSettings ( $db, "smis-provit-karyawan-ok-anastesi-thadir", "20" );
		$array ['as-anastesi-hadir'] = getSettings ( $db, "smis-provit-karyawan-ok-as-anastesi-hadir", "10" );
		$array ['as-anastesi-thadir'] = getSettings ( $db, "smis-provit-karyawan-ok-as-anastesi-thadir", "20" );
		$array ['team-ok'] = getSettings ( $db, "smis-provit-karyawan-ok-team-ok", "51" );
		$array ['team-perawat'] = getSettings ( $db, "smis-provit-karyawan-ok-team-perawat", "51" );
		$array ['team-bidan'] = getSettings ( $db, "smis-provit-karyawan-ok-team-bidan", "51" );
		$array ['s-dtamu'] = getSettings ( $db, "smis-provit-karyawan-ok-dtamu-sifat", "individual" );
		$array ['s-dorganik'] = getSettings ( $db, "smis-provit-karyawan-ok-dorganik-sifat", "individual" );
		$array ['s-anastesi-hadir'] = getSettings ( $db, "smis-provit-karyawan-ok-anastesi-hadir-sifat", "individual" );
		$array ['s-anatesi-thadir'] = getSettings ( $db, "smis-provit-karyawan-ok-anastesi-thadir-sifat", "individual" );
		$array ['s-as-anastesi-hadir'] = getSettings ( $db, "smis-provit-karyawan-ok-as-anastesi-hadir-sifat", "communal" );
		$array ['s-as-anastesi-thadir'] = getSettings ( $db, "smis-provit-karyawan-ok-as-anastesi-thadir-sifat", "communal" );
		$array ['s-team-ok'] = getSettings ( $db, "smis-provit-karyawan-ok-team-ok-sifat", "communal" );
		$array ['s-team-perawat'] = getSettings ( $db, "smis-provit-karyawan-ok-team-perawat-sifat", "communal" );
		$array ['s-team-bidan'] = getSettings ( $db, "smis-provit-karyawan-ok-team-bidan-sifat", "communal" );
		echo json_encode ( $array );
	} else if ($provit == "smis-pv-vk") {
		$array = array ();
		$array ['dtamu'] = getSettings ( $db, "smis-provit-karyawan-vk-dtamu", "90" );
		$array ['dorganik'] = getSettings ( $db, "smis-provit-karyawan-vk-dorganik", "70" );
		$array ['anastesi-hadir'] = getSettings ( $db, "smis-provit-karyawan-vk-anastesi-hadir", "50" );
		$array ['anatesi-thadir'] = getSettings ( $db, "smis-provit-karyawan-vk-anastesi-thadir", "20" );
		$array ['as-anastesi-hadir'] = getSettings ( $db, "smis-provit-karyawan-vk-as-anastesi-hadir", "10" );
		$array ['as-anastesi-thadir'] = getSettings ( $db, "smis-provit-karyawan-vk-as-anastesi-thadir", "20" );
		$array ['team-vk'] = getSettings ( $db, "smis-provit-karyawan-vk-team-vk", "51" );
		$array ['team-perawat'] = getSettings ( $db, "smis-provit-karyawan-vk-team-perawat", "51" );
		$array ['team-bidan'] = getSettings ( $db, "smis-provit-karyawan-vk-team-bidan", "51" );
		$array ['s-dtamu'] = getSettings ( $db, "smis-provit-karyawan-vk-dtamu-sifat", "individual" );
		$array ['s-dorganik'] = getSettings ( $db, "smis-provit-karyawan-vk-dorganik-sifat", "individual" );
		$array ['s-anastesi-hadir'] = getSettings ( $db, "smis-provit-karyawan-vk-anastesi-hadir-sifat", "individual" );
		$array ['s-anatesi-thadir'] = getSettings ( $db, "smis-provit-karyawan-vk-anastesi-thadir-sifat", "individual" );
		$array ['s-as-anastesi-hadir'] = getSettings ( $db, "smis-provit-karyawan-vk-as-anastesi-hadir-sifat", "individual" );
		$array ['s-as-anastesi-thadir'] = getSettings ( $db, "smis-provit-karyawan-vk-as-anastesi-thadir-sifat", "individual" );
		$array ['s-team-vk'] = getSettings ( $db, "smis-provit-karyawan-vk-team-vk-sifat", "individual" );
		$array ['s-team-perawat'] = getSettings ( $db, "smis-provit-karyawan-vk-team-perawat-sifat", "individual" );
		$array ['s-team-bidan'] = getSettings ( $db, "smis-provit-karyawan-vk-team-bidan-sifat", "individual" );
		echo json_encode ( $array );
	} else if ($provit == "smis-pv-fparu") {
		$array = array ();
		$array ['dokter'] = getSettings ( $db, "smis-provit-karyawan-fparu-dokter", "10" );
		$array ['asisten'] = getSettings ( $db, "smis-provit-karyawan-fparu-asisten", "20" );
		$array ['s-dokter'] = getSettings ( $db, "smis-provit-karyawan-fparu-dokter-sifat", "individual" );
		$array ['s-asisten'] = getSettings ( $db, "smis-provit-karyawan-fparu-asisten-sifat", "communal" );
		echo json_encode ( $array );
	} else if ($provit == "smis-pv-endoscopy") {
		$array = array ();
		$array ['dokter'] = getSettings ( $db, "smis-provit-karyawan-endoscopy-dokter", "10" );
		$array ['asisten'] = getSettings ( $db, "smis-provit-karyawan-endoscopy-asisten", "20" );
		$array ['s-dokter'] = getSettings ( $db, "smis-provit-karyawan-endoscopy-dokter-sifat", "individual" );
		$array ['s-asisten'] = getSettings ( $db, "smis-provit-karyawan-endoscopy-asisten-sifat", "communal" );
		echo json_encode ( $array );
	} else if ($provit == "smis-pv-fisiotherapy") {
		$array = array ();
		$array ['dokter'] = getSettings ( $db, "smis-provit-karyawan-fst-dokter", "10" );
		$array ['petugas'] = getSettings ( $db, "smis-provit-karyawan-fst-petugas", "8.5" );
		$array ['s-dokter'] = getSettings ( $db, "smis-provit-karyawan-fst-dokter-sifat", "individual" );
		$array ['s-petugas'] = getSettings ( $db, "smis-provit-karyawan-fst-petugas-sifat", "communal" );
		echo json_encode ( $array );
	} else if ($provit == "smis-pv-ekg") {
		$array = array ();
		$array ['dokter-pengirim'] = getSettings ( $db, "smis-provit-karyawan-ekg-dokter-pengirim", "10" );
		$array ['dokter-pembaca'] = getSettings ( $db, "smis-provit-karyawan-ekg-dokter-pembaca", "20" );
		$array ['petugas'] = getSettings ( $db, "smis-provit-karyawan-ekg-petugas", "5" );
		$array ['s-dokter-pengirim'] = getSettings ( $db, "smis-provit-karyawan-ekg-dokter-pengirim-sifat", "individual" );
		$array ['s-dokter-pembaca'] = getSettings ( $db, "smis-provit-karyawan-ekg-dokter-pembaca-sifat", "individual" );
		$array ['s-petugas'] = getSettings ( $db, "smis-provit-karyawan-ekg-petugas-sifat", "communal" );
		echo json_encode ( $array );
	} else if ($provit == "smis-pv-bronchoscopy") {
		$array = array ();
		$array ['dokter'] = getSettings ( $db, "smis-provit-karyawan-bronchoscopy-dokter", "10" );
		$array ['perawat'] = getSettings ( $db, "smis-provit-karyawan-bronchoscopy-perawat", "20" );
		$array ['s-dokter'] = getSettings ( $db, "smis-provit-karyawan-bronchoscopy-dokter-sifat", "individual" );
		$array ['s-perawat'] = getSettings ( $db, "smis-provit-karyawan-bronchoscopy-perawat-sifat", "communal" );
		echo json_encode ( $array );
	} else if ($provit == "smis-pv-audiometry") {
		$array = array ();
		$array ['dokter'] = getSettings ( $db, "smis-provit-karyawan-audiometry-dokter", "10" );
		$array ['perawat'] = getSettings ( $db, "smis-provit-karyawan-audiometry-perawat", "20" );
		$array ['s-dokter'] = getSettings ( $db, "smis-provit-karyawan-audiometry-dokter-sifat", "individual" );
		$array ['s-perawat'] = getSettings ( $db, "smis-provit-karyawan-audiometry-perawat-sifat", "communal" );
		echo json_encode ( $array );
	} else if ($provit == "smis-pv-spirometry") {
		$array = array ();
		$array ['dokter'] = getSettings ( $db, "smis-provit-karyawan-spirometry-dokter", "10" );
		$array ['perawat'] = getSettings ( $db, "smis-provit-karyawan-spirometry-perawat", "20" );
		$array ['s-dokter'] = getSettings ( $db, "smis-provit-karyawan-spirometry-dokter-sifat", "individual" );
		$array ['s-perawat'] = getSettings ( $db, "smis-provit-karyawan-spirometry-perawat-sifat", "communal" );
		echo json_encode ( $array );
	} else if ($provit == "smis-pv-laboratory") {
		$array = array ();
		$array ['dokter-pengirim'] = getSettings ( $db, "smis-provit-karyawan-laboratory-dokter-pengirim", "10" );
		$array ['s-dokter-pengirim'] = getSettings ( $db, "smis-provit-karyawan-laboratory-dokter-pengirim-sifat", "individual" );
		$array ['dokter-konsultan'] = getSettings ( $db, "smis-provit-karyawan-laboratory-dokter-konsultan", "10" );
		$array ['s-dokter-konsultan'] = getSettings ( $db, "smis-provit-karyawan-laboratory-dokter-konsultan-sifat", "individual" );
		$array ['petugas'] = getSettings ( $db, "smis-provit-karyawan-laboratory-petugas", "10" );
		$array ['s-petugas'] = getSettings ( $db, "smis-provit-karyawan-laboratory-petugas-sifat", "communal" );
		$array ['grup'] = getSettings ( $db, "smis-provit-karyawan-laboratory-grup", "5" );
		$array ['s-grup'] = getSettings ( $db, "smis-provit-karyawan-laboratory-grup-sifat", "communal" );
		echo json_encode ( $array );
	} else if ($provit == "smis-pv-radiology") {
		$array = array ();
		$array ['dokter-pengirim-plain'] = getSettings ( $db, "smis-provit-karyawan-radiology-dokter-pengirim-plain", "10" );
		$array ['dokter-konsultan-plain'] = getSettings ( $db, "smis-provit-karyawan-radiology-dokter-konsultan-plain", "10" );
		$array ['petugas-plain'] = getSettings ( $db, "smis-provit-karyawan-radiology-dokter-petugas-plain", "10" );
		$array ['dokter-pengirim-contrast'] = getSettings ( $db, "smis-provit-karyawan-radiology-dokter-pengirim-contrast", "10" );
		$array ['dokter-konsultan-contrast'] = getSettings ( $db, "smis-provit-karyawan-radiology-dokter-konsultan-contrast", "10" );
		$array ['petugas-contrast'] = getSettings ( $db, "smis-provit-karyawan-radiology-dokter-petugas-contrast", "10" );
		$array ['dokter-pengirim-usg'] = getSettings ( $db, "smis-provit-karyawan-radiology-dokter-pengirim-usg", "10" );
		$array ['dokter-konsultan-usg'] = getSettings ( $db, "smis-provit-karyawan-radiology-dokter-konsultan-usg", "10" );
		$array ['petugas-usg'] = getSettings ( $db, "smis-provit-karyawan-radiology-dokter-petugas-usg", "10" );
		$array ['dokter-pengirim-ctscan'] = getSettings ( $db, "smis-provit-karyawan-radiology-dokter-pengirim-ctscan", "10" );
		$array ['dokter-konsultan-ctscan'] = getSettings ( $db, "smis-provit-karyawan-radiology-dokter-konsultan-ctscan", "10" );
		$array ['petugas-ctscan'] = getSettings ( $db, "smis-provit-karyawan-radiology-dokter-petugas-ctscan", "10" );
		$array ['s-dokter-pengirim-plain'] = getSettings ( $db, "smis-provit-karyawan-radiology-dokter-pengirim-plain-sifat", "individual" );
		$array ['s-dokter-konsultan-plain'] = getSettings ( $db, "smis-provit-karyawan-radiology-dokter-konsultan-plain-sifat", "individual" );
		$array ['s-petugas-plain'] = getSettings ( $db, "smis-provit-karyawan-radiology-dokter-petugas-plain-sifat", "communal" );
		$array ['s-dokter-pengirim-contrast'] = getSettings ( $db, "smis-provit-karyawan-radiology-dokter-pengirim-contrast-sifat", "individual" );
		$array ['s-dokter-konsultan-contrast'] = getSettings ( $db, "smis-provit-karyawan-radiology-dokter-konsultan-contrast-sifat", "individual" );
		$array ['s-petugas-contrast'] = getSettings ( $db, "smis-provit-karyawan-radiology-dokter-petugas-contrast-sifat", "communal" );
		$array ['s-dokter-pengirim-usg'] = getSettings ( $db, "smis-provit-karyawan-radiology-dokter-pengirim-usg-sifat", "individual" );
		$array ['s-dokter-konsultan-usg'] = getSettings ( $db, "smis-provit-karyawan-radiology-dokter-konsultan-usg-sifat", "individual" );
		$array ['s-petugas-usg'] = getSettings ( $db, "smis-provit-karyawan-radiology-dokter-petugas-usg-sifat", "communal" );
		$array ['s-dokter-pengirim-ctscan'] = getSettings ( $db, "smis-provit-karyawan-radiology-dokter-pengirim-ctscan-sifat", "individual" );
		$array ['s-dokter-konsultan-ctscan'] = getSettings ( $db, "smis-provit-karyawan-radiology-dokter-konsultan-ctscan-sifat", "individual" );
		$array ['s-petugas-ctscan'] = getSettings ( $db, "smis-provit-karyawan-radiology-dokter-petugas-ctscan-sifat", "communal" );
		echo json_encode ( $array );
	} else if ($provit == "smis-pv-rr") {
		$array = array ();
		$array ['petugas-rr'] = getSettings ( $db, "smis-provit-karyawan-rr-petugas", "10" );
		$array ['asisten-rr'] = getSettings ( $db, "smis-provit-karyawan-rr-asisten", "10" );
		$array ['s-petugas-rr'] = getSettings ( $db, "smis-provit-karyawan-rr-petugas-sifat", "communal" );
		$array ['s-asisten-rr'] = getSettings ( $db, "smis-provit-karyawan-rr-asisten-sifat", "individual" );
		echo json_encode ( $array );
	}
} else {
	echo json_encode ( array () );
}
?>