<?php 
global $db;
require_once "smis-base/smis-include-duplicate.php";
$dbtable=new DBTable($db,$_POST['table']);
$serv=new SynchronousServiceProvider($dbtable,true,getSettings($db,"smis_autonomous_name"));
$serv->setSynchMode(SynchronousServiceProvider::$SINGLE_SYNCH);
$data=$serv->command($_POST['command']);
echo json_encode($data);
?>