<?php
//$kelas = ArrayAdapter::format ( "slug", $_POST ['kelas'] );
$kelas = $_POST ['kelas'];
$default = '';
$harga = getSettings ( $db, "smis_fst_harga", $default );
$list_harga = json_decode ( $harga );
$filtered_harga = array ();
foreach ( $list_harga as $name => $value ) {
	if (startsWith ( $name, $kelas . "_" )) {
		$filtered_harga [$name] = $value;
	}
}
$filtered = json_encode ( $filtered_harga );
echo json_encode ( $filtered );
?>