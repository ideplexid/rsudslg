<?php

global $wpdb;
require_once "smis-libs-class/DBCreator.php";
require_once "manajemen/resource/install/table/smis_mjm_jenis_mobil.php";
require_once "manajemen/resource/install/table/smis_mjm_ambulan.php";
require_once "manajemen/resource/install/table/smis_mjm_sewa_ambulan.php";

require_once "manajemen/resource/install/table/smis_mjm_anastesi.php";
require_once "manajemen/resource/install/table/smis_mjm_audiometry.php";
require_once "manajemen/resource/install/table/smis_mjm_bronchoscopy.php";
require_once "manajemen/resource/install/table/smis_mjm_ekg.php";
require_once "manajemen/resource/install/table/smis_mjm_endoscopy.php";
require_once "manajemen/resource/install/table/smis_mjm_faalparu.php";
require_once "manajemen/resource/install/table/smis_mjm_formula.php";
require_once "manajemen/resource/install/table/smis_mjm_kelas.php";
require_once "manajemen/resource/install/table/smis_mjm_keperawatan.php";
require_once "manajemen/resource/install/table/smis_mjm_konsul.php";
require_once "manajemen/resource/install/table/smis_mjm_ok.php";
require_once "manajemen/resource/install/table/smis_mjm_paket.php";
require_once "manajemen/resource/install/table/smis_mjm_spirometry.php";
require_once "manajemen/resource/install/table/smis_mjm_tindakan_dokter.php";
require_once "manajemen/resource/install/table/smis_mjm_tindakan_operasi.php";
require_once "manajemen/resource/install/table/smis_mjm_tindakan_vk.php";
require_once "manajemen/resource/install/table/smis_mjm_visite.php";
require_once "manajemen/resource/install/table/smis_mjm_vk.php";
require_once "manajemen/resource/install/table/smis_mjm_oksigen.php";
require_once "manajemen/resource/install/table/smis_mjm_gizi.php";
require_once "manajemen/resource/install/table/smis_mjm_bbm.php";
require_once "manajemen/resource/install/table/smis_mjm_ambulan_bbm.php";
require_once "manajemen/resource/install/table/smis_mjm_cssd.php";


require_once "manajemen/resource/install/table/smis_mjm_tindakan_farmasi.php";

require_once "manajemen/resource/install/view/smis_mjm_terapkan.php";
require_once "manajemen/resource/install/view/smis_mjmv_keperawatan.php";
