	<?php
	global $NAVIGATOR;
	$mr = new Menu ("fa fa-user");
	$mr->addProperty ( 'title', 'Pengaturan Manajerial' );
	$mr->addProperty ( 'name', 'Manajer' );
	$mr->addSubMenu ( "Kelas", "manajemen", "kelas", "Data Kelas-Kelas Rumah Sakit" ,"fa fa-building");
	$mr->addSubMenu ( "Provit Sharing", "manajemen", "provit_sharing", "Provit Sharing" ,"fa fa-share-alt");
	$mr->addSubMenu ( "Data Induk", "manajemen", "data_induk", "Data Induk" ,"fa fa-database");
	$mr->addSeparator ();
	$mr->addSubMenu ( "Settings Ruangan", "manajemen", "karcis", "Karcis" ,"fa fa-credit-card");
	$mr->addSubMenu ( "Settings Manajemen", "manajemen", "settings", "Settings" ,"fa fa-cog");
	$mr->addSeparator ();
	$mr->addSubMenu ( "Tarif Penunjang", "manajemen", "tarif_penunjang", "Settings Tarif Rumah Sakit","fa fa-list" );
	$mr->addSubMenu ( "Tarif Dokter", "manajemen", "tarif_dokter", "Tarif Dokter","fa fa-user-md" );
	$mr->addSubMenu ( "Tarif Keperawatan", "manajemen", "keperawatan", "Tarif Keperawatan" ,"fa fa-female");
	$mr->addSubMenu ( "Tarif Kamar Operasi", "manajemen", "tarif_ok", "Tarif Kamar OK" ,"fa fa-building-o");
	$mr->addSubMenu ( "Tarif Radiology", "manajemen", "tarif_radiology", "Tarif Radiology" ,"fa fa-warning");
	$mr->addSubMenu ( "Tarif Laboratory", "manajemen", "tarif_laboratory", "Tarif Laboratory" ,"fa fa-flask");
	$mr->addSubMenu ( "Tarif Fisiotherapy", "manajemen", "tarif_fisiotherapy", "Tarif Fisiotherapy" ,"fa fa-slideshare");
    $mr->addSubMenu ( "Tarif Elektromedis", "manajemen", "tarif_elektromedis", "Tarif Elektromedis" ,"fa fa-slideshare");
	$mr->addSubMenu ( "Tarif Patology", "manajemen", "tarif_patology", "Tarif Patology" ,"fa fa-slideshare");
    $mr->addSubMenu ( "Tarif Oksigen", "manajemen", "tarif_oksigen", "Settings Tarif Rumah Sakit","fa fa-fire-extinguisher" );
	$mr->addSubMenu ( "Tarif CCSD", "manajemen", "tarif_ccsd", "","fa fa-list" );
	$mr->addSubMenu ( "Penerapan Tarif RLF", "manajemen", "terapkan", "Penerapan Tarif Laboratory, Radiology dan Fisiotherapy" ,"fa fa-share");
	$mr->addSubMenu ( "Tarif Paket Medical Checkup", "manajemen", "paket_rlfe", "Tarif Paket Medical Checkup" ,"fa fa-bolt");
	$NAVIGATOR->addMenu ( $mr, 'manajemen' );
	?>
