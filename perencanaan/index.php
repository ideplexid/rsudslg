<?php
	global $db;
	global $user;
	require_once 'smis-libs-class/Policy.php';
	require_once 'smis-libs-inventory/policy.php';

	$policy = new Policy("perencanaan", $user);
	$policy->addPolicy("jenis_obat_alkes", "jenis_obat_alkes", Policy::$DEFAULT_COOKIE_CHANGE);
	$policy->addPolicy("jenis_barang_non_medis", "jenis_barang_non_medis", Policy::$DEFAULT_COOKIE_CHANGE);
	$policy->addPolicy("obat_alkes", "obat_alkes", Policy::$DEFAULT_COOKIE_CHANGE);
	$policy->addPolicy("produsen", "produsen", Policy::$DEFAULT_COOKIE_CHANGE);
	$policy->addPolicy("mapping_acc_obat_alkes", "mapping_acc_obat_alkes", Policy::$DEFAULT_COOKIE_CHANGE);
	$policy->addPolicy("mapping_acc_barang_inventaris", "mapping_acc_barang_inventaris", Policy::$DEFAULT_COOKIE_CHANGE);
	$policy->addPolicy("usia_susut_inventaris", "usia_susut_inventaris", Policy::$DEFAULT_COOKIE_CHANGE);
	$policy->addPolicy("barang_non_medis", "barang_non_medis", Policy::$DEFAULT_COOKIE_CHANGE);
	$policy->addPolicy("rencana_pembelian_farmasi", "rencana_pembelian_farmasi", Policy::$DEFAULT_COOKIE_CHANGE);
	$policy->addPolicy("rencana_pembelian_farmasi_form", "rencana_pembelian_farmasi", Policy::$DEFAULT_COOKIE_KEEP);
	$policy->addPolicy("rencana_pembelian_farmasi_export_xls", "rencana_pembelian_farmasi", Policy::$DEFAULT_COOKIE_KEEP);
	$policy->addPolicy("acc_rencana_pembelian_farmasi", "acc_rencana_pembelian_farmasi", Policy::$DEFAULT_COOKIE_CHANGE);
	$policy->addPolicy("acc_rencana_pembelian_farmasi_form", "acc_rencana_pembelian_farmasi", Policy::$DEFAULT_COOKIE_KEEP);
	$policy->addPolicy("acc_rencana_pembelian_farmasi_export_xls", "acc_rencana_pembelian_farmasi", Policy::$DEFAULT_COOKIE_KEEP);
	$policy->addPolicy("rp_abc", "rp_abc", Policy::$DEFAULT_COOKIE_CHANGE);
	$policy->addPolicy("rp_mmsl", "rp_mmsl", Policy::$DEFAULT_COOKIE_CHANGE);
	$policy->setDefaultPolicy(Policy::$DEFAULT_POLICY_RESTRICT);
	$policy->initialize();
?>