<?php
	global $db;
	require_once("smis-libs-out/php-excel/PHPExcel.php");
	
	$id = $_POST['id'];
	$dbtable = new DBTable($db, "smis_pr_rencana_pembelian");
	$header_row = $dbtable->select($id);
	$detail_rows = $dbtable->get_result("
		SELECT *
		FROM smis_pr_drencana_pembelian
		WHERE id_rencana_pembelian = '" . $id . "' AND prop NOT LIKE 'del'
	");

	$objPHPExcel = PHPExcel_IOFactory::load("perencanaan/templates/rencana_pembelian_farmasi_template.xlsx");
	
	$objPHPExcel->setActiveSheetIndexByName("Rencana Pembelian");
	$objWorksheet = $objPHPExcel->getActiveSheet();
	if (count($detail_rows) - 1 > 0)
		$objWorksheet->insertNewRowBefore(9, count($detail_rows) - 1);
	
	$kode_dokumen = "RPF." . ArrayAdapter::format("only-digit6", $header_row->id) . "." . ArrayAdapter::format("date Ymd", $header_row->tanggal);
	$objWorksheet->setCellValue("I1", $kode_dokumen);
	setlocale(LC_ALL, 'IND');
	$objWorksheet->setCellValue("I3", "TGL. " . ArrayAdapter::format("date d-m-Y", $header_row->tanggal));
	$objWorksheet->setCellValue("I4", "PBF / DISTRIBUTOR : " . $header_row->kode_vendor . " - " . $header_row->nama_vendor);
	
	$no = 1;
	$start_row_num = 8;
	$end_row_num = 9;
	$row_num = $start_row_num;
	foreach ($detail_rows as $d) {
		$col_num = 0;
		$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $no);
		$col_num++;
		$kode_barang = substr($d->kode_barang, 0, 3) == "JKN" ? "319" . substr($d->kode_barang, 3) : "309" . substr($d->kode_barang, 3);
		$objWorksheet->getCellByColumnAndRow($col_num, $row_num)->setValueExplicit($kode_barang, PHPExcel_Cell_DataType::TYPE_STRING);
		$col_num++;
		$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->nama_barang);
		$col_num++;
		$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->nama_jenis_barang);
		$col_num++;
		$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->jumlah_diajukan);
		$col_num++;
		$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->jumlah_disetujui);
		$col_num++;
		$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->satuan);
		$col_num++;
		$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->hps);
		$col_num++;
		$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->jumlah_disetujui * $d->hps);
		$no++;
		$row_num++;
		$end_row_num++;
	}
	
	// $objDrawing = new PHPExcel_Worksheet_Drawing();
	// $objDrawing->setPath("kasir/jurnal_2016/resources/logo_1_5.png");
	// $objDrawing->setCoordinates("A1");
	// $objDrawing->setOffsetX(0);
	// $objDrawing->setWorksheet($objWorksheet);
		
	header("Content-type: application/vnd.ms-excel");	
	header("Content-Disposition: attachment; filename=RENCANA_PEMBELIAN_FARMASI_" . $kode_dokumen . "_" . ArrayAdapter::format("date Ymd_Hi", $header_row->tanggal) . "_" . date("Ymd_His") . ".xlsx");
	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");
	$objWriter->save("php://output");
?>