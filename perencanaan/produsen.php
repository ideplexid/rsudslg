<?php
	global $db;
	
	$table = new Table(
		array("No.", "ID-Data", "Kode", "Nama"),
		"Perencanaan : Produsen Obat dan Alkes",
		null,
		true
	);
	$table->setName("produsen");
	
	if (isset($_POST['command'])) {
		$adapter = new SimpleAdapter(true, "No.");
		$adapter->add("ID-Data", "id", "digit6");
		$adapter->add("Kode", "kode");
		$adapter->add("Nama", "nama");
		$dbtable = new DBTable($db, "smis_pr_produsen");
		$dbtable->addCustomKriteria(" medis ", " = '1' ");
		$dbtable->setOrder(" id DESC ");
		$dbresponder = new DBResponder(
			$dbtable,
			$table,
			$adapter
		);
		$data = $dbresponder->command($_POST['command']);
		echo json_encode($data);
		return;
	}
	
	$modal = new Modal("produsen_add_form", "smis_form_container", "produsen");
	$modal->setTitle("Data Produsen");
	$id_hidden = new Hidden("produsen_id", "produsen_id", "");
	$modal->addElement("", $id_hidden);
	$kode_text = new Text("produsen_kode", "produsen_kode", "");
	$modal->addElement("Kode", $kode_text);
	$nama_text = new Text("produsen_nama", "produsen_nama", "");
	$modal->addElement("Nama", $nama_text);
	$save_button = new Button("", "", "Simpan");
	$save_button->setClass("btn-success");
	$save_button->setAction("produsen.save()");
	$save_button->setIcon("fa fa-floppy-o");
	$save_button->setIsButton(Button::$ICONIC);
	$modal->addFooter($save_button);
	
	echo $modal->getHtml();
	echo $table->getHtml();
	echo addJS("framework/smis/js/table_action.js");
	echo addJS("perencanaan/js/produsen_action.js", false);
	echo addJS("perencanaan/js/produsen.js", false);
?>