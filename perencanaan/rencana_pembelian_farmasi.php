<?php
	require_once("perencanaan/table/RencanaPembelianFarmasiTable.php");
	require_once("perencanaan/responder/RencanaPembelianFarmasiDBResponder.php");
	global $db;
	
	$table = new RencanaPembelianFarmasiTable(
		array("No.", "No. RP", "Tanggal", "Kode Vendor", "Nama Vendor", "Status"),
		"Perencanaan : Draft Rencana Pembelian Farmasi",
		null,
		true
	);
	$table->setName("rencana_pembelian_farmasi");
	
	if (isset($_POST['command'])) {
		if ($_POST['command'] == "export_xls") {
			require_once("perencanaan/rencana_pembelian_farmasi_export_xls.php");
			return;
		}
		$adapter = new SimpleAdapter(true, "No.");
		$adapter->add("id", "id");
		$adapter->add("lock_plan", "lock_plan");
		$adapter->add("lock_acc", "lock_acc");
		$adapter->add("No. RP", "id", "digit8");
		$adapter->add("Tanggal", "tanggal", "date d-m-Y");
		$adapter->add("Kode Vendor", "kode_vendor");
		$adapter->add("Nama Vendor", "nama_vendor");
		$adapter->add("Status", "lock_acc", "trivial_0_BELUM DISETUJUI_SUDAH DISETUJUI");
		$dbtable = new DBTable($db, "smis_pr_rencana_pembelian");
		$dbtable->setOrder(" id DESC ");
		$dbresponder = new RencanaPembelianFarmasiDBResponder(
			$dbtable,
			$table,
			$adapter
		);
		$data = $dbresponder->command($_POST['command']);
		echo json_encode($data);
		return;
	}
	
	echo $table->getHtml();
	echo addJS("framework/bootstrap/js/bootstrap-datepicker.js");
	echo addCSS("framework/bootstrap/css/datepicker.css");
	echo addJS("framework/smis/js/table_action.js");
	echo addJS("perencanaan/js/rencana_pembelian_farmasi_action.js", false);
	echo addJS("perencanaan/js/rencana_pembelian_farmasi.js", false);
?>