<?php 
	global $wpdb;
	require_once("smis-libs-class/DBCreator.php");

	$dbcreator = new DBCreator($wpdb, "smis_pr_rkbu");
	$dbcreator->addColumn("f_id", "INT(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("unit", "VARCHAR(64)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("tahun", "INT(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("tanggal_diajukan", "DATE", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("tanggal_ditinjau", "DATE", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("keterangan", "TEXT", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("tinjauan", "TEXT", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("locked", "TINYINT(1)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_ZERO, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("disetujui", "VARCHAR(10)", DBCreator::$SIGN_NONE, true, "belum", false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->initialize();
	
	$dbcreator = new DBCreator($wpdb, "smis_pr_drkbu");
	$dbcreator->addColumn("f_id", "INT(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("id_rkbu", "INT(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("nama_barang", "VARCHAR(64)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("jenis_barang", "VARCHAR(32)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("jumlah_diajukan", "INT(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("satuan_diajukan", "VARCHAR(32)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("jumlah_disetujui", "INT(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("satuan_disetujui", "VARCHAR(32)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("bulan", "VARCHAR(32)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->initialize();
	
	$dbcreator = new DBCreator($wpdb, "smis_pr_jenis_barang");
	$dbcreator->addColumn("kode", "VARCHAR(64)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("nama", "VARCHAR(64)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("medis", "TINYINT(1)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->initialize();

	$dbcreator = new DBCreator($wpdb, "smis_pr_barang");
	$dbcreator->addColumn("kode", "VARCHAR(64)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("nama", "VARCHAR(64)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("medis", "TINYINT(1)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_ZERO, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("inventaris", "TINYINT(1)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_ZERO, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("id_jenis_barang", "INT(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("kode_jenis_barang", "VARCHAR(64)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("nama_jenis_barang", "VARCHAR(64)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("harga_pbf", "DOUBLE", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_ZERO, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("formularium", "TINYINT(1)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_ZERO, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("berlogo", "TINYINT(1)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_ZERO, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("generik", "TINYINT(1)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_ZERO, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("alkes", "TINYINT(1)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_ZERO, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("narkotika", "TINYINT(1)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_ZERO, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("okt", "TINYINT(1)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_ZERO, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("prekursor", "TINYINT(1)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_ZERO, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("satuan", "VARCHAR(64)", DBCreator::$SIGN_NONE, true, "BIJI", false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("satuan_konversi", "VARCHAR(64)", DBCreator::$SIGN_NONE, true, "BIJI", false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("konversi", "FLOAT", DBCreator::$SIGN_NONE, true, 1, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("produsen", "VARCHAR(256)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("acc_kode_nilai_persediaan", "VARCHAR(64)", DBCreator::$SIGN_NONE, true, "", false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("acc_kode_hpp", "VARCHAR(64)", DBCreator::$SIGN_NONE, true, "", false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("acc_kode_pendapatan", "VARCHAR(64)", DBCreator::$SIGN_NONE, true, "", false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("acc_kode_diskon_penjualan", "VARCHAR(64)", DBCreator::$SIGN_NONE, true, "", false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("acc_kode_piutang", "VARCHAR(64)", DBCreator::$SIGN_NONE, true, "", false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("acc_kode_ppn_keluar", "VARCHAR(64)", DBCreator::$SIGN_NONE, true, "", false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("usia_susut", "FLOAT", DBCreator::$SIGN_NONE, true, 0, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->initialize();

	$dbcreator = new DBCreator($wpdb, "smis_pr_produsen");
	$dbcreator->addColumn("kode", "VARCHAR(64)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("nama", "VARCHAR(64)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("medis", "TINYINT(1)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->initialize();
	
	$dbcreator = new DBCreator($wpdb, "smis_pr_rencana_pembelian");
	$dbcreator->addColumn("tanggal", "DATE", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("kode_vendor", "VARCHAR(64)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("id_vendor", "INT(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("nama_vendor", "VARCHAR(256)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("keterangan", "VARCHAR(512)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("lock_plan", "TINYINT(1)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_ZERO, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("lock_acc", "TINYINT(1)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_ZERO, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("medis", "TINYINT(1)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->initialize();
	
	$dbcreator = new DBCreator($wpdb, "smis_pr_drencana_pembelian");
	$dbcreator->addColumn("id_rencana_pembelian", "INT(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("id_barang", "INT(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("kode_barang", "VARCHAR(64)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("nama_barang", "VARCHAR(256)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("nama_jenis_barang", "VARCHAR(256)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("jumlah_diajukan", "INT(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_ZERO, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("jumlah_disetujui", "INT(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_ZERO, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("jumlah_dipesan", "INT(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_ZERO, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("satuan", "VARCHAR(64)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("konversi", "INT(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("satuan_konversi", "VARCHAR(64)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("hps", "DECIMAL(10,0)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_ZERO, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("medis", "TINYINT(1)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_ZERO, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("inventaris", "TINYINT(1)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_ZERO, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->initialize();
?>