<?php
	global $db;
	
	$table = new Table(
		array("No.", "ID-Data", "Kode", "Nama"),
		"Perencanaan : Jenis Barang Non-Medis",
		null,
		true
	);
	$table->setName("jenis_barang_non_medis");
	
	if (isset($_POST['command'])) {
		$adapter = new SimpleAdapter(true, "No.");
		$adapter->add("ID-Data", "id", "digit6");
		$adapter->add("Kode", "kode");
		$adapter->add("Nama", "nama");
		$dbtable = new DBTable($db, "smis_pr_jenis_barang");
		$dbtable->addCustomKriteria(" medis ", " = '0' ");
		$dbtable->setOrder(" id DESC ");
		$dbresponder = new DBResponder(
			$dbtable,
			$table,
			$adapter
		);
		$data = $dbresponder->command($_POST['command']);
		echo json_encode($data);
		return;
	}
	
	$modal = new Modal("jenis_barang_non_medis_add_form", "smis_form_container", "jenis_barang_non_medis");
	$modal->setTitle("Data Jenis Barang");
	$id_hidden = new Hidden("jenis_barang_non_medis_id", "jenis_barang_non_medis_id", "");
	$modal->addElement("", $id_hidden);
	$kode_text = new Text("jenis_barang_non_medis_kode", "jenis_barang_non_medis_kode", "");
	$modal->addElement("Kode", $kode_text);
	$nama_text = new Text("jenis_barang_non_medis_nama", "jenis_barang_non_medis_nama", "");
	$modal->addElement("Nama", $nama_text);
	$save_button = new Button("", "", "Simpan");
	$save_button->setClass("btn-success");
	$save_button->setAction("jenis_barang_non_medis.save()");
	$save_button->setIcon("fa fa-floppy-o");
	$save_button->setIsButton(Button::$ICONIC);
	$modal->addFooter($save_button);
	
	echo $modal->getHtml();
	echo $table->getHtml();
	echo addJS("framework/smis/js/table_action.js");
	echo addJS("perencanaan/js/jenis_barang_non_medis_action.js", false);
	echo addJS("perencanaan/js/jenis_barang_non_medis.js", false);
?>