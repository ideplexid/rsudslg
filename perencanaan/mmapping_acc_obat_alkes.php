<?php
	require_once ("smis-framework/smis/interface/LockerInterface.php");
	require_once ("smis-framework/smis/database/DuplicateResponder.php");
    require_once ("smis-base/smis-include-service-consumer.php");
	global $db;

	$table = new Table(
		array("No.", "ID Obat", "Kode Obat", "Nama Obat", "Jenis Obat", "Jenis Pasien", "Kode Acc. Nilai Persediaan", "Kode Acc. HPP", "Kode Acc. Pendapatan", "Kode Acc. Piutang", "Kode Acc. Diskon Penjualan", "Kode Acc. PPn Keluar", "Kode Acc. Hutang Konsinyasi", "Kode Acc. Penggunaan BHP"),
		"Perencanaan : Multi-Mapping Acc. Obat dan Alkes",
		null,
		true
	);
	$table->setName("mmapping_acc_obat_alkes");

	$obat_table = new Table(
		array("No.", "ID Obat", "Kode Obat", "Nama Obat", "Jenis Obat"),
		"",
		null,
		true
	);
	$obat_table->setName("obat");
	$obat_table->setModel(Table::$SELECT);
	$obat_adapter = new SimpleAdapter(true, "No.");
	$obat_adapter->add("ID Obat", "id");
	$obat_adapter->add("Kode Obat", "kode");
	$obat_adapter->add("Nama Obat", "nama");
	$obat_adapter->add("Jenis Obat", "nama_jenis_barang");
	$obat_dbtable = new DBTable($db, "smis_pr_barang");
	$obat_dbtable->addCustomKriteria(" medis ", " = '1' ");
	$obat_dbtable->addCustomKriteria(" inventaris ", " = '0' ");
	$obat_dbresponder = new DBResponder(
		$obat_dbtable,
		$obat_table,
		$obat_adapter
	);

	$kode_ppn_keluar_table = new Table(
		array("No.", "Kode Acc.", "Label"),
		"",
		null,
		true
	);
	$kode_ppn_keluar_table->setName("kode_ppn_keluar");
	$kode_ppn_keluar_table->setModel(Table::$SELECT);
	$kode_ppn_keluar_adapter = new SimpleAdapter(true, "No.");
	$kode_ppn_keluar_adapter->add("Kode Acc.", "nomor");
	$kode_ppn_keluar_adapter->add("Label", "nama");
	$kode_ppn_keluar_service_responder = new ServiceResponder(
		$db,
		$kode_ppn_keluar_table,
		$kode_ppn_keluar_adapter,
		"get_account",
		"accounting"
	);

	$kode_nilai_persediaan_table = new Table(
		array("No.", "Kode Acc.", "Label"),
		"",
		null,
		true
	);
	$kode_nilai_persediaan_table->setName("kode_nilai_persediaan");
	$kode_nilai_persediaan_table->setModel(Table::$SELECT);
	$kode_nilai_persediaan_adapter = new SimpleAdapter(true, "No.");
	$kode_nilai_persediaan_adapter->add("Kode Acc.", "nomor");
	$kode_nilai_persediaan_adapter->add("Label", "nama");
	$kode_nilai_persediaan_service_responder = new ServiceResponder(
		$db,
		$kode_nilai_persediaan_table,
		$kode_nilai_persediaan_adapter,
		"get_account",
		"accounting"
	);

	$kode_hpp_table = new Table(
		array("No.", "Kode Acc.", "Label"),
		"",
		null,
		true
	);
	$kode_hpp_table->setName("kode_hpp");
	$kode_hpp_table->setModel(Table::$SELECT);
	$kode_hpp_adapter = new SimpleAdapter(true, "No.");
	$kode_hpp_adapter->add("Kode Acc.", "nomor");
	$kode_hpp_adapter->add("Label", "nama");
	$kode_hpp_service_responder = new ServiceResponder(
		$db,
		$kode_hpp_table,
		$kode_hpp_adapter,
		"get_account",
		"accounting"
	);

	$kode_pendapatan_table = new Table(
		array("No.", "Kode Acc.", "Label"),
		"",
		null,
		true
	);
	$kode_pendapatan_table->setName("kode_pendapatan");
	$kode_pendapatan_table->setModel(Table::$SELECT);
	$kode_pendapatan_adapter = new SimpleAdapter(true, "No.");
	$kode_pendapatan_adapter->add("Kode Acc.", "nomor");
	$kode_pendapatan_adapter->add("Label", "nama");
	$kode_pendapatan_service_responder = new ServiceResponder(
		$db,
		$kode_pendapatan_table,
		$kode_pendapatan_adapter,
		"get_account",
		"accounting"
	);

	$kode_diskon_penjualan_table = new Table(
		array("No.", "Kode Acc.", "Label"),
		"",
		null,
		true
	);
	$kode_diskon_penjualan_table->setName("kode_diskon_penjualan");
	$kode_diskon_penjualan_table->setModel(Table::$SELECT);
	$kode_diskon_penjualan_adapter = new SimpleAdapter(true, "No.");
	$kode_diskon_penjualan_adapter->add("Kode Acc.", "nomor");
	$kode_diskon_penjualan_adapter->add("Label", "nama");
	$kode_diskon_penjualan_service_responder = new ServiceResponder(
		$db,
		$kode_diskon_penjualan_table,
		$kode_diskon_penjualan_adapter,
		"get_account",
		"accounting"
	);

	$kode_piutang_table = new Table(
		array("No.", "Kode Acc.", "Label"),
		"",
		null,
		true
	);
	$kode_piutang_table->setName("kode_piutang");
	$kode_piutang_table->setModel(Table::$SELECT);
	$kode_piutang_adapter = new SimpleAdapter(true, "No.");
	$kode_piutang_adapter->add("Kode Acc.", "nomor");
	$kode_piutang_adapter->add("Label", "nama");
	$kode_piutang_service_responder = new ServiceResponder(
		$db,
		$kode_piutang_table,
		$kode_piutang_adapter,
		"get_account",
		"accounting"
	);

	$kode_hutang_konsinyasi_table = new Table(
		array("No.", "Kode Acc.", "Label"),
		"",
		null,
		true
	);
	$kode_hutang_konsinyasi_table->setName("kode_hutang_konsinyasi");
	$kode_hutang_konsinyasi_table->setModel(Table::$SELECT);
	$kode_hutang_konsinyasi_adapter = new SimpleAdapter(true, "No.");
	$kode_hutang_konsinyasi_adapter->add("Kode Acc.", "nomor");
	$kode_hutang_konsinyasi_adapter->add("Label", "nama");
	$kode_hutang_konsinyasi_service_responder = new ServiceResponder(
		$db,
		$kode_hutang_konsinyasi_table,
		$kode_hutang_konsinyasi_adapter,
		"get_account",
		"accounting"
	);

	$kode_penggunaan_bhp_table = new Table(
		array("No.", "Kode Acc.", "Label"),
		"",
		null,
		true
	);
	$kode_penggunaan_bhp_table->setName("kode_penggunaan_bhp");
	$kode_penggunaan_bhp_table->setModel(Table::$SELECT);
	$kode_penggunaan_bhp_adapter = new SimpleAdapter(true, "No.");
	$kode_penggunaan_bhp_adapter->add("Kode Acc.", "nomor");
	$kode_penggunaan_bhp_adapter->add("Label", "nama");
	$kode_penggunaan_bhp_service_responder = new ServiceResponder(
		$db,
		$kode_penggunaan_bhp_table,
		$kode_penggunaan_bhp_adapter,
		"get_account",
		"accounting"
	);

	$super_command = new SuperCommand();
	$super_command->addResponder("obat", $obat_dbresponder);
	$super_command->addResponder("kode_nilai_persediaan", $kode_nilai_persediaan_service_responder);
	$super_command->addResponder("kode_hpp", $kode_hpp_service_responder);
	$super_command->addResponder("kode_pendapatan", $kode_pendapatan_service_responder);
	$super_command->addResponder("kode_diskon_penjualan", $kode_diskon_penjualan_service_responder);
	$super_command->addResponder("kode_piutang", $kode_piutang_service_responder);
	$super_command->addResponder("kode_hutang_konsinyasi", $kode_hutang_konsinyasi_service_responder);
	$super_command->addResponder("kode_penggunaan_bhp", $kode_penggunaan_bhp_service_responder);
	$super_command->addResponder("kode_ppn_keluar", $kode_ppn_keluar_service_responder);
	$init = $super_command->initialize();
	if ($init != null) {
		echo $init;
		return;
	}
	
	if (isset($_POST['command'])) {
		$adapter = new SimpleAdapter(true, "No.");
		$adapter->add("ID Obat", "id_barang");
		$adapter->add("Kode Obat", "kode_obat");
		$adapter->add("Nama Obat", "nama_obat");
		$adapter->add("Jenis Obat", "nama_jenis_obat");
		$adapter->add("Jenis Pasien", "jenis_pasien", "unslug");
		$adapter->add("Kode Acc. Nilai Persediaan", "acc_kode_nilai_persediaan");
		$adapter->add("Kode Acc. HPP", "acc_kode_hpp");
		$adapter->add("Kode Acc. Pendapatan", "acc_kode_pendapatan");
		$adapter->add("Kode Acc. Piutang", "acc_kode_piutang");
		$adapter->add("Kode Acc. Diskon Penjualan", "acc_kode_diskon_penjualan");
		$adapter->add("Kode Acc. PPn Keluar", "acc_kode_ppn_keluar");
		$adapter->add("Kode Acc. Hutang Konsinyasi", "acc_kode_hutang_konsinyasi");
		$adapter->add("Kode Acc. Penggunaan BHP", "acc_kode_penggunaan_bhp");
		$dbtable = new DBTable($db, "smis_pr_kode_acc_barang");
		$filter = "";
		if (isset($_POST['kriteria']))
			$filter = " AND b.nama LIKE '%" . $_POST['kriteria'] . "%' ";
		$query_value = "
			SELECT a.*, b.nama AS 'nama_obat', b.kode AS 'kode_obat', b.nama_jenis_barang AS 'nama_jenis_obat'
			FROM smis_pr_kode_acc_barang a LEFT JOIN smis_pr_barang b ON a.id_barang = b.id
			WHERE a.prop NOT LIKE 'del' " . $filter . "
			ORDER BY id_barang ASC, id DESC
		";
		$query_count = "
			SELECT COUNT(*)
			FROM (
				" . $query_value . "
			) v
		";
		$dbtable->setPreferredQuery(true, $query_value, $query_count);
		$dbresponder = new DuplicateResponder(
			$dbtable,
			$table,
			$adapter
		);
		$data = $dbresponder->command($_POST['command']);
		echo json_encode($data);
		return;
	}
	
	$modal = new Modal("mmapping_acc_obat_alkes_add_form", "smis_form_container", "mmapping_acc_obat_alkes");
	$modal->setTitle("Data Obat dan Alkes");
	$id_hidden = new Hidden("mmapping_acc_obat_alkes_id", "mmapping_acc_obat_alkes_id", "");
	$modal->addElement("", $id_hidden);
	$id_obat_hidden = new Hidden("mmapping_acc_obat_alkes_id_barang", "mmapping_acc_obat_alkes_id_barang", "");
	$modal->addElement("", $id_obat_hidden);
	$nama_text = new Text("mmapping_acc_obat_alkes_nama_obat", "mmapping_acc_obat_alkes_nama_obat", "");
	$nama_text->setClass("smis-one-option-input");
	$nama_text->setAtribute("disabled='disabled'");
	$nama_button = new Button("", "", "Pilih");
	$nama_button->setAtribute("id='nama_obat_btn'");
	$nama_button->setClass("btn-info");
	$nama_button->setIsButton(Button::$ICONIC);
	$nama_button->setIcon("icon-white ".Button::$icon_list_alt);
	$nama_button->setAction("obat.chooser('obat', 'obat_button', 'obat', obat)");
	$nama_input_group = new InputGroup("");
	$nama_input_group->addComponent($nama_text);
	$nama_input_group->addComponent($nama_button);
	$modal->addElement("Nama", $nama_input_group);
	$jenis_text = new Text("mmapping_acc_obat_alkes_nama_jenis_obat", "mmapping_acc_obat_alkes_nama_jenis_obat", "");
	$jenis_text->setAtribute("disabled='disabled'");
	$modal->addElement("Jenis Obat", $jenis_text);
	$kode_text = new Text("mmapping_acc_obat_alkes_kode", "mmapping_acc_obat_alkes_kode", "");
	$kode_text->setAtribute("disabled='disabled'");
	$modal->addElement("Kode", $kode_text);
	$jenis_pasien_service_consumer = new ServiceConsumer($db, "get_jenis_patient", null, "registration");
	$jenis_pasien_service_consumer->execute();
	$jenis_pasien_option = $jenis_pasien_service_consumer->getContent();
	$jenis_pasien_select = new Select("mmapping_acc_obat_alkes_jenis_pasien", "mmapping_acc_obat_alkes_jenis_pasien", $jenis_pasien_option);
	$modal->addElement("Jns. Pasien", $jenis_pasien_select);
	$acc_kode_nilai_persediaan_text = new Text("mmapping_acc_obat_alkes_acc_kode_nilai_persediaan", "mmapping_acc_obat_alkes_acc_kode_nilai_persediaan", "");
	$acc_kode_nilai_persediaan_text->setAtribute("disabled='disabled'");
	$acc_kode_nilai_persediaan_text->setClass("smis-two-option-input");
	$acc_kode_nilai_persediaan_button = new Button("", "", "Pilih");
	$acc_kode_nilai_persediaan_button->setAtribute("id='acc_kode_nilai_persediaan_btn'");
	$acc_kode_nilai_persediaan_button->setClass("btn-info");
	$acc_kode_nilai_persediaan_button->setIsButton(Button::$ICONIC);
	$acc_kode_nilai_persediaan_button->setIcon("icon-white ".Button::$icon_list_alt);
	$acc_kode_nilai_persediaan_button->setAction("kode_nilai_persediaan.chooser('kode_nilai_persediaan', 'kode_nilai_persediaan_button', 'kode_nilai_persediaan', kode_nilai_persediaan)");
	$acc_kode_nilai_persediaan_clear_button = new Button("", "", "Hapus");
	$acc_kode_nilai_persediaan_clear_button->setClass("btn-danger");
	$acc_kode_nilai_persediaan_clear_button->setIsButton(Button::$ICONIC);
	$acc_kode_nilai_persediaan_clear_button->setIcon("fa fa-trash-o");
	$acc_kode_nilai_persediaan_clear_button->setAction("kode_nilai_persediaan.clear()");
	$acc_kode_nilai_persediaan_input_group = new InputGroup("");
	$acc_kode_nilai_persediaan_input_group->addComponent($acc_kode_nilai_persediaan_text);
	$acc_kode_nilai_persediaan_input_group->addComponent($acc_kode_nilai_persediaan_button);
	$acc_kode_nilai_persediaan_input_group->addComponent($acc_kode_nilai_persediaan_clear_button);
	$modal->addElement("KA. Persediaan", $acc_kode_nilai_persediaan_input_group);
	$acc_kode_hpp_text = new Text("mmapping_acc_obat_alkes_acc_kode_hpp", "mmapping_acc_obat_alkes_acc_kode_hpp", "");
	$acc_kode_hpp_text->setAtribute("disabled='disabled'");
	$acc_kode_hpp_text->setClass("smis-two-option-input");
	$acc_kode_hpp_button = new Button("", "", "Pilih");
	$acc_kode_hpp_button->setAtribute("id='acc_kode_hpp_btn'");
	$acc_kode_hpp_button->setClass("btn-info");
	$acc_kode_hpp_button->setIsButton(Button::$ICONIC);
	$acc_kode_hpp_button->setIcon("icon-white ".Button::$icon_list_alt);
	$acc_kode_hpp_button->setAction("kode_hpp.chooser('kode_hpp', 'kode_hpp_button', 'kode_hpp', kode_hpp)");
	$acc_kode_hpp_clear_button = new Button("", "", "Hapus");
	$acc_kode_hpp_clear_button->setClass("btn-danger");
	$acc_kode_hpp_clear_button->setIsButton(Button::$ICONIC);
	$acc_kode_hpp_clear_button->setIcon("fa fa-trash-o");
	$acc_kode_hpp_clear_button->setAction("kode_hpp.clear()");
	$acc_kode_hpp_input_group = new InputGroup("");
	$acc_kode_hpp_input_group->addComponent($acc_kode_hpp_text);
	$acc_kode_hpp_input_group->addComponent($acc_kode_hpp_button);
	$acc_kode_hpp_input_group->addComponent($acc_kode_hpp_clear_button);
	$modal->addElement("KA. HPP", $acc_kode_hpp_input_group);
	$acc_kode_pendapatan_text = new Text("mmapping_acc_obat_alkes_acc_kode_pendapatan", "mmapping_acc_obat_alkes_acc_kode_pendapatan", "");
	$acc_kode_pendapatan_text->setAtribute("disabled='disabled'");
	$acc_kode_pendapatan_text->setClass("smis-two-option-input");
	$acc_kode_pendapatan_button = new Button("", "", "Pilih");
	$acc_kode_pendapatan_button->setAtribute("id='acc_kode_pendapatan_btn'");
	$acc_kode_pendapatan_button->setClass("btn-info");
	$acc_kode_pendapatan_button->setIsButton(Button::$ICONIC);
	$acc_kode_pendapatan_button->setIcon("icon-white ".Button::$icon_list_alt);
	$acc_kode_pendapatan_button->setAction("kode_pendapatan.chooser('kode_pendapatan', 'kode_pendapatan_button', 'kode_pendapatan', kode_pendapatan)");
	$acc_kode_pendapatan_clear_button = new Button("", "", "Hapus");
	$acc_kode_pendapatan_clear_button->setClass("btn-danger");
	$acc_kode_pendapatan_clear_button->setIsButton(Button::$ICONIC);
	$acc_kode_pendapatan_clear_button->setIcon("fa fa-trash-o");
	$acc_kode_pendapatan_clear_button->setAction("kode_pendapatan.clear()");
	$acc_kode_pendapatan_input_group = new InputGroup("");
	$acc_kode_pendapatan_input_group->addComponent($acc_kode_pendapatan_text);
	$acc_kode_pendapatan_input_group->addComponent($acc_kode_pendapatan_button);
	$acc_kode_pendapatan_input_group->addComponent($acc_kode_pendapatan_clear_button);
	$modal->addElement("KA. Pendapatan", $acc_kode_pendapatan_input_group);
	$acc_kode_piutang_text = new Text("mmapping_acc_obat_alkes_acc_kode_piutang", "mmapping_acc_obat_alkes_acc_kode_piutang", "");
	$acc_kode_piutang_text->setAtribute("disabled='disabled'");
	$acc_kode_piutang_text->setClass("smis-two-option-input");
	$acc_kode_piutang_button = new Button("", "", "Pilih");
	$acc_kode_piutang_button->setAtribute("id='acc_kode_piutang_btn'");
	$acc_kode_piutang_button->setClass("btn-info");
	$acc_kode_piutang_button->setIsButton(Button::$ICONIC);
	$acc_kode_piutang_button->setIcon("icon-white ".Button::$icon_list_alt);
	$acc_kode_piutang_button->setAction("kode_piutang.chooser('kode_piutang', 'kode_piutang_button', 'kode_piutang', kode_piutang)");
	$acc_kode_piutang_clear_button = new Button("", "", "Hapus");
	$acc_kode_piutang_clear_button->setClass("btn-danger");
	$acc_kode_piutang_clear_button->setIsButton(Button::$ICONIC);
	$acc_kode_piutang_clear_button->setIcon("fa fa-trash-o");
	$acc_kode_piutang_clear_button->setAction("kode_piutang.clear()");
	$acc_kode_piutang_input_group = new InputGroup("");
	$acc_kode_piutang_input_group->addComponent($acc_kode_piutang_text);
	$acc_kode_piutang_input_group->addComponent($acc_kode_piutang_button);
	$acc_kode_piutang_input_group->addComponent($acc_kode_piutang_clear_button);
	$modal->addElement("KA. Piutang", $acc_kode_piutang_input_group);
	$acc_kode_diskon_penjualan_text = new Text("mmapping_acc_obat_alkes_acc_kode_diskon_penjualan", "mmapping_acc_obat_alkes_acc_kode_diskon_penjualan", "");
	$acc_kode_diskon_penjualan_text->setAtribute("disabled='disabled'");
	$acc_kode_diskon_penjualan_text->setClass("smis-two-option-input");
	$acc_kode_diskon_penjualan_button = new Button("", "", "Pilih");
	$acc_kode_diskon_penjualan_button->setAtribute("id='acc_kode_diskon_penjualan_btn'");
	$acc_kode_diskon_penjualan_button->setClass("btn-info");
	$acc_kode_diskon_penjualan_button->setIsButton(Button::$ICONIC);
	$acc_kode_diskon_penjualan_button->setIcon("icon-white ".Button::$icon_list_alt);
	$acc_kode_diskon_penjualan_button->setAction("kode_diskon_penjualan.chooser('kode_diskon_penjualan', 'kode_diskon_penjualan_button', 'kode_diskon_penjualan', kode_diskon_penjualan)");
	$acc_kode_diskon_penjualan_clear_button = new Button("", "", "Hapus");
	$acc_kode_diskon_penjualan_clear_button->setClass("btn-danger");
	$acc_kode_diskon_penjualan_clear_button->setIsButton(Button::$ICONIC);
	$acc_kode_diskon_penjualan_clear_button->setIcon("fa fa-trash-o");
	$acc_kode_diskon_penjualan_clear_button->setAction("kode_diskon_penjualan.clear()");
	$acc_kode_diskon_penjualan_input_group = new InputGroup("");
	$acc_kode_diskon_penjualan_input_group->addComponent($acc_kode_diskon_penjualan_text);
	$acc_kode_diskon_penjualan_input_group->addComponent($acc_kode_diskon_penjualan_button);
	$acc_kode_diskon_penjualan_input_group->addComponent($acc_kode_diskon_penjualan_clear_button);
	$modal->addElement("KA. Disk. Jual", $acc_kode_diskon_penjualan_input_group);
	$acc_kode_ppn_keluar_text = new Text("mmapping_acc_obat_alkes_acc_kode_ppn_keluar", "mmapping_acc_obat_alkes_acc_kode_ppn_keluar", "");
	$acc_kode_ppn_keluar_text->setAtribute("disabled='disabled'");
	$acc_kode_ppn_keluar_text->setClass("smis-two-option-input");
	$acc_kode_ppn_keluar_button = new Button("", "", "Pilih");
	$acc_kode_ppn_keluar_button->setAtribute("id='acc_kode_ppn_keluar_btn'");
	$acc_kode_ppn_keluar_button->setClass("btn-info");
	$acc_kode_ppn_keluar_button->setIsButton(Button::$ICONIC);
	$acc_kode_ppn_keluar_button->setIcon("icon-white ".Button::$icon_list_alt);
	$acc_kode_ppn_keluar_button->setAction("kode_ppn_keluar.chooser('kode_ppn_keluar', 'kode_ppn_keluar_button', 'kode_ppn_keluar', kode_ppn_keluar)");
	$acc_kode_ppn_keluar_clear_button = new Button("", "", "Hapus");
	$acc_kode_ppn_keluar_clear_button->setClass("btn-danger");
	$acc_kode_ppn_keluar_clear_button->setIsButton(Button::$ICONIC);
	$acc_kode_ppn_keluar_clear_button->setIcon("fa fa-trash-o");
	$acc_kode_ppn_keluar_clear_button->setAction("kode_ppn_keluar.clear()");
	$acc_kode_ppn_keluar_input_group = new InputGroup("");
	$acc_kode_ppn_keluar_input_group->addComponent($acc_kode_ppn_keluar_text);
	$acc_kode_ppn_keluar_input_group->addComponent($acc_kode_ppn_keluar_button);
	$acc_kode_ppn_keluar_input_group->addComponent($acc_kode_ppn_keluar_clear_button);
	$modal->addElement("KA. PPn Jual", $acc_kode_ppn_keluar_input_group);
	$acc_kode_hutang_konsinyasi_text = new Text("mmapping_acc_obat_alkes_acc_kode_hutang_konsinyasi", "mmapping_acc_obat_alkes_acc_kode_hutang_konsinyasi", "");
	$acc_kode_hutang_konsinyasi_text->setAtribute("disabled='disabled'");
	$acc_kode_hutang_konsinyasi_text->setClass("smis-two-option-input");
	$acc_kode_hutang_konsinyasi_button = new Button("", "", "Pilih");
	$acc_kode_hutang_konsinyasi_button->setAtribute("id='acc_kode_hutang_konsinyasi_btn'");
	$acc_kode_hutang_konsinyasi_button->setClass("btn-info");
	$acc_kode_hutang_konsinyasi_button->setIsButton(Button::$ICONIC);
	$acc_kode_hutang_konsinyasi_button->setIcon("icon-white ".Button::$icon_list_alt);
	$acc_kode_hutang_konsinyasi_button->setAction("kode_hutang_konsinyasi.chooser('kode_hutang_konsinyasi', 'kode_hutang_konsinyasi_button', 'kode_hutang_konsinyasi', kode_hutang_konsinyasi)");
	$acc_kode_hutang_konsinyasi_clear_button = new Button("", "", "Hapus");
	$acc_kode_hutang_konsinyasi_clear_button->setClass("btn-danger");
	$acc_kode_hutang_konsinyasi_clear_button->setIsButton(Button::$ICONIC);
	$acc_kode_hutang_konsinyasi_clear_button->setIcon("fa fa-trash-o");
	$acc_kode_hutang_konsinyasi_clear_button->setAction("kode_hutang_konsinyasi.clear()");
	$acc_kode_hutang_konsinyasi_input_group = new InputGroup("");
	$acc_kode_hutang_konsinyasi_input_group->addComponent($acc_kode_hutang_konsinyasi_text);
	$acc_kode_hutang_konsinyasi_input_group->addComponent($acc_kode_hutang_konsinyasi_button);
	$acc_kode_hutang_konsinyasi_input_group->addComponent($acc_kode_hutang_konsinyasi_clear_button);
	$modal->addElement("KA. Hutang Kons.", $acc_kode_hutang_konsinyasi_input_group);
	$acc_kode_penggunaan_bhp_text = new Text("mmapping_acc_obat_alkes_acc_kode_penggunaan_bhp", "mmapping_acc_obat_alkes_acc_kode_penggunaan_bhp", "");
	$acc_kode_penggunaan_bhp_text->setAtribute("disabled='disabled'");
	$acc_kode_penggunaan_bhp_text->setClass("smis-two-option-input");
	$acc_kode_penggunaan_bhp_button = new Button("", "", "Pilih");
	$acc_kode_penggunaan_bhp_button->setAtribute("id='acc_kode_penggunaan_bhp_btn'");
	$acc_kode_penggunaan_bhp_button->setClass("btn-info");
	$acc_kode_penggunaan_bhp_button->setIsButton(Button::$ICONIC);
	$acc_kode_penggunaan_bhp_button->setIcon("icon-white ".Button::$icon_list_alt);
	$acc_kode_penggunaan_bhp_button->setAction("kode_penggunaan_bhp.chooser('kode_penggunaan_bhp', 'kode_penggunaan_bhp_button', 'kode_penggunaan_bhp', kode_penggunaan_bhp)");
	$acc_kode_penggunaan_bhp_clear_button = new Button("", "", "Hapus");
	$acc_kode_penggunaan_bhp_clear_button->setClass("btn-danger");
	$acc_kode_penggunaan_bhp_clear_button->setIsButton(Button::$ICONIC);
	$acc_kode_penggunaan_bhp_clear_button->setIcon("fa fa-trash-o");
	$acc_kode_penggunaan_bhp_clear_button->setAction("kode_penggunaan_bhp.clear()");
	$acc_kode_penggunaan_bhp_input_group = new InputGroup("");
	$acc_kode_penggunaan_bhp_input_group->addComponent($acc_kode_penggunaan_bhp_text);
	$acc_kode_penggunaan_bhp_input_group->addComponent($acc_kode_penggunaan_bhp_button);
	$acc_kode_penggunaan_bhp_input_group->addComponent($acc_kode_penggunaan_bhp_clear_button);
	$modal->addElement("KA. Peng. BHP", $acc_kode_penggunaan_bhp_input_group);
	
	$save_button = new Button("", "", "Simpan");
	$save_button->setClass("btn-success");
	$save_button->setAction("mmapping_acc_obat_alkes.save()");
	$save_button->setIcon("fa fa-floppy-o");
	$save_button->setIsButton(Button::$ICONIC);
	$modal->addFooter($save_button);
	
	echo $modal->getHtml();
	echo $table->getHtml();
	echo addJS("framework/smis/js/table_action.js");
	echo addJS("perencanaan/js/mmapping_acc_obat_alkes_obat_action.js", false);
	echo addJS("perencanaan/js/mmapping_acc_obat_alkes_kode_nilai_persediaan_action.js", false);
	echo addJS("perencanaan/js/mmapping_acc_obat_alkes_kode_hpp_action.js", false);
	echo addJS("perencanaan/js/mmapping_acc_obat_alkes_kode_pendapatan_action.js", false);
	echo addJS("perencanaan/js/mmapping_acc_obat_alkes_kode_diskon_penjualan_action.js", false);
	echo addJS("perencanaan/js/mmapping_acc_obat_alkes_kode_piutang_action.js", false);
	echo addJS("perencanaan/js/mmapping_acc_obat_alkes_kode_ppn_keluar_action.js", false);
	echo addJS("perencanaan/js/mmapping_acc_obat_alkes_kode_hutang_konsinyasi_action.js", false);
	echo addJS("perencanaan/js/mmapping_acc_obat_alkes_kode_penggunaan_bhp_action.js", false);
	echo addJS("perencanaan/js/mmapping_acc_obat_alkes_action.js", false);
	echo addJS("perencanaan/js/mmapping_acc_obat_alkes.js", false);
?>