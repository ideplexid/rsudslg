<?php
	require_once("smis-base/smis-include-service-consumer.php");
	require_once("perencanaan/responder/RencanaPembelianFarmasiDBResponder.php");
	global $db;
	
	$header_form = new Form("", "", "Perencanaan : Draft Rencana Pembelian Obat");

	$id = isset($_POST['id']) ? $_POST['id'] : "";
	$tanggal = !isset($_POST['tanggal']) || $_POST['tanggal'] == "" ? date("Y-m-d") : $_POST['tanggal'];
	$id_vendor = isset($_POST['id_vendor']) ? $_POST['id_vendor'] : "";
	$kode_vendor = isset($_POST['kode_vendor']) ? $_POST['kode_vendor'] : "";
	$nama_vendor = isset($_POST['nama_vendor']) ? $_POST['nama_vendor'] : "";
	$medis = isset($_POST['medis']) ? $_POST['medis'] : 1;
	$editable = isset($_POST['editable']) ? $_POST['editable'] : "false";
	
	$id_hidden = new Hidden("rencana_pembelian_farmasi_id", "rencana_pembelian_farmasi_id", $id);
	$header_form->addElement("", $id_hidden);
	$medis_hidden = new Hidden("rencana_pembelian_farmasi_medis", "rencana_pembelian_farmasi_medis", $medis);
	$header_form->addElement("", $medis_hidden);
	$tanggal_text = new Text("rencana_pembelian_farmasi_tanggal", "rencana_pembelian_farmasi_tanggal", $tanggal);
	if ($editable == "true") {
		$tanggal_text->setAtribute("data-date-format='yyyy-mm-dd'");
		$tanggal_text->setClass("mydate");
	}
	else
		$tanggal_text->setAtribute("disabled='disabled'");
	$header_form->addElement("Tanggal", $tanggal_text);
	$id_vendor_hidden = new Hidden("rencana_pembelian_farmasi_id_vendor", "rencana_pembelian_farmasi_id_vendor", $id_vendor);
	$header_form->addElement("", $id_vendor_hidden);
	$nama_vendor_text = new Text("rencana_pembelian_farmasi_nama_vendor", "rencana_pembelian_farmasi_nama_vendor", $nama_vendor);
	$nama_vendor_text->setAtribute("disabled='disabled'");
	if ($editable == "true")
		$nama_vendor_text->setClass("smis-one-option-input");
	$vendor_browse_button = new Button("", "", "Pilih");
	$vendor_browse_button->setClass("btn-info");
	$vendor_browse_button->setIsButton(Button::$ICONIC);
	$vendor_browse_button->setIcon("icon-white ".Button::$icon_list_alt);
	$vendor_browse_button->setAction("vendor.chooser('vendor', 'vendor', 'vendor', vendor)");
	$vendor_browse_button->setAtribute("id='vendor_browse'");
	$vendor_input_group = new InputGroup("");
	$vendor_input_group->addComponent($nama_vendor_text);
	$vendor_input_group->addComponent($vendor_browse_button);
	if ($editable == "true")
		$header_form->addElement("Nama Vendor", $vendor_input_group);
	else
		$header_form->addElement("Nama Vendor", $nama_vendor_text);
	$kode_vendor_text = new Text("rencana_pembelian_farmasi_kode_vendor", "rencana_pembelian_farmasi_kode_vendor", $kode_vendor);
	$kode_vendor_text->setAtribute("disabled='disabled'");
	$header_form->addElement("Kode Vendor", $kode_vendor_text);
	
	$detail_form = new Form("", "", "");
	$id_drencana_pembelian_farmasi_hidden = new Hidden("drencana_pembelian_farmasi_id", "drencana_pembelian_farmasi_id", "");
	$detail_form->addElement("", $id_drencana_pembelian_farmasi_hidden);
	$id_barang_hidden = new Hidden("drencana_pembelian_farmasi_id_barang", "drencana_pembelian_farmasi_id_barang", "");
	$detail_form->addElement("", $id_barang_hidden);
	$medis_hidden = new Hidden("drencana_pembelian_farmasi_medis", "drencana_pembelian_farmasi_medis", "");
	$detail_form->addElement("", $medis_hidden);
	$inventaris_hidden = new Hidden("drencana_pembelian_farmasi_inventaris", "drencana_pembelian_farmasi_inventaris", "");
	$detail_form->addElement("", $inventaris_hidden);
	$name_barang_hidden = new Hidden("drencana_pembelian_farmasi_name_barang", "drencana_pembelian_farmasi_name_barang", "");
	$detail_form->addElement("", $name_barang_hidden);
	$nama_barang_text = new Text("drencana_pembelian_farmasi_nama_barang", "drencana_pembelian_farmasi_nama_barang", "");
	$nama_barang_text->setClass("smis-one-option-input");
	$barang_browse_button = new Button("", "", "Pilih");
	$barang_browse_button->setClass("btn-info");
	$barang_browse_button->setIsButton(Button::$ICONIC);
	$barang_browse_button->setIcon("icon-white ".Button::$icon_list_alt);
	$barang_browse_button->setAction("barang.chooser('barang', 'barang_button', 'barang', barang)");
	$barang_browse_button->setAtribute("id='barang_browse'");
	$barang_input_group = new InputGroup("");
	$barang_input_group->addComponent($nama_barang_text);
	$barang_input_group->addComponent($barang_browse_button);
	$detail_form->addElement("Nama Barang", $barang_input_group);
	$kode_barang_text = new Text("drencana_pembelian_farmasi_kode_barang", "drencana_pembelian_farmasi_kode_barang", "");
	$kode_barang_text->setAtribute("disabled='disabled'");
	$detail_form->addElement("Kode Barang", $kode_barang_text);
	$nama_jenis_barang_text = new Text("drencana_pembelian_farmasi_nama_jenis_barang", "drencana_pembelian_farmasi_nama_jenis_barang", "");
	$nama_jenis_barang_text->setAtribute("disabled='disabled'");
	$detail_form->addElement("Jenis Barang", $nama_jenis_barang_text);
	$sisa_text = new Text("drencana_pembelian_farmasi_sisa", "drencana_pembelian_farmasi_sisa", "");
	$sisa_text->setAtribute("disabled='disabled'");
	$detail_form->addElement("Persediaan", $sisa_text);
	$jumlah_text = new Text("drencana_pembelian_farmasi_jumlah", "drencana_pembelian_farmasi_jumlah", "");
	$detail_form->addElement("Jml. Diajukan", $jumlah_text);
	$satuan_text = new Text("drencana_pembelian_farmasi_satuan", "drencana_pembelian_farmasi_satuan", "");
	$satuan_text->setAtribute("disabled='disabled'");
	$detail_form->addElement("Satuan", $satuan_text);
	$hps_text = new Text("drencana_pembelian_farmasi_hps", "drencana_pembelian_farmasi_hps", "");
	$hps_text->setTypical("money");
	$hps_text->setAtribute("data-thousands='.' data-decimal=',' data-prefix='Rp. '  data-precision='2'");
	$detail_form->addElement("HPS", $hps_text);
	$add_button = new Button("", "", "Tambahkan");
	$add_button->setClass("btn-info");
	$add_button->setIsButton(Button::$ICONIC_TEXT);
	$add_button->setIcon("fa fa-chevron-right");
	$add_button->setAction("drencana_pembelian_farmasi.save()");
	$add_button->setAtribute("id='rencana_pembelian_farmasi_form_save'");
	$detail_form->addElement("", $add_button);
	
	$table = new Table(
		array("No.", "Kode Barang", "Nama Barang", "Jenis Barang", "Jumlah Diajukan", "Jumlah Disetujui", "Satuan", "HPS", "Subtotal", "Hapus"),
		"",
		null,
		true
	);
	$table->setName("drencana_pembelian_farmasi");
	$table->setFooterVisible(false);
	$table->setAction(false);
	
	$button_group = new ButtonGroup("");
	$back_button = new Button("", "", "Kembali");
	$back_button->setClass("btn-inverse");
	$back_button->setIsButton(Button::$ICONIC_TEXT);
	$back_button->setIcon("fa fa-chevron-left");
	$back_button->setAction("rencana_pembelian_farmasi.back()");
	$back_button->setAtribute("id='rencana_pembelian_farmasi_back'");
	$button_group->addButton($back_button);
	if ($editable == "true") {
		$save_button = new Button("", "", "Simpan Draft");
		$save_button->setClass("btn-success");
		$save_button->setIsButton(Button::$ICONIC_TEXT);
		$save_button->setIcon("fa fa-floppy-o");
		$save_button->setAction("rencana_pembelian_farmasi.save()");
		$save_button->setAtribute("id='rencana_pembelian_farmasi_save'");
		$button_group->addButton($save_button);
	}
	
	$barang_table = new Table(
		array("No.", "Kode", "Barang", "Jenis Barang"),
		"",
		null,
		true
	);
	$barang_table->setName("barang");
	$barang_table->setModel(Table::$SELECT);
	$barang_adapter = new SimpleAdapter(true, "No.");
	$barang_adapter->add("Kode", "kode");
	$barang_adapter->add("Barang", "nama");
	$barang_adapter->add("Jenis Barang", "nama_jenis_barang");
	$barang_dbtable = new DBTable($db, "smis_pr_barang");
	$barang_dbtable->addCustomKriteria(" medis ", " = '1' ");
	$barang_dbtable->addCustomKriteria(" inventaris ", " = '0' ");
	$barang_dbresponder = new DBResponder(
		$barang_dbtable,
		$barang_table,
		$barang_adapter
	);
	$vendor_table = new Table(
		array("No.", "Kode", "Vendor"),
		"",
		null,
		true
	);
	$vendor_table->setName("vendor");
	$vendor_table->setModel(Table::$SELECT);
	$vendor_adapter = new SimpleAdapter(true, "No.");
	$vendor_adapter->add("Kode", "kode");
	$vendor_adapter->add("Vendor", "nama");
	$vendor_service_responder = new ServiceResponder(
		$db,
		$vendor_table,
		$vendor_adapter,
		"get_daftar_vendor"
	);
	$super_command = new SuperCommand();
	$super_command->addResponder("barang", $barang_dbresponder);
	$super_command->addResponder("vendor", $vendor_service_responder);
	$init = $super_command->initialize();
	if ($init != null) {
		echo $init;
		return;
	}
	
	if (isset($_POST['command'])) {
		if ($_POST['command'] == "show_detail") {
			$id = $_POST['id'];
			$dbtable = new DBTable($db, "smis_pr_rencana_pembelian");
			$header_row = $dbtable->get_row("
				SELECT lock_plan
				FROM smis_pr_rencana_pembelian
				WHERE id = '" . $id . "'
			");
			$detail_action = false;
			if ($header_row != null)
				$locked = $header_row->lock_plan;
			$detail_rows = $dbtable->get_result("
				SELECT *
				FROM smis_pr_drencana_pembelian
				WHERE id_rencana_pembelian = '" . $id . "' AND prop NOT LIKE 'del'
			");
			$html = "";
			$num = 0;
			if ($detail_rows != null) {
				foreach ($detail_rows as $row) {
					$html_action = "";
					if (!$locked && $row->jumlah_disetujui == 0) 
						$html_action = "<a href='#' onclick='drencana_pembelian_farmasi.del(" . $num . ")' class='input btn btn-danger'><i class='fa fa-trash-o'></i></a>";
					$subtotal = $row->hps * $row->jumlah_disetujui;
					$f_subtotal = ArrayAdapter::format("money", $subtotal);
					$html .= "
						<tr id='data_" . $num . "'>
							<td id='nomor'></td>
							<td id='id' style='display: none;'>" . $row->id . "</td>
							<td id='id_barang' style='display: none;'>" . $row->id_barang . "</td>
							<td id='kode_barang'><small>" . $row->kode_barang . "</small></td>
							<td id='nama_barang'><small>" . $row->nama_barang . "</small></td>
							<td id='nama_jenis_barang'><small>" . $row->nama_jenis_barang . "</small></td>
							<td id='jumlah_diajukan' style='display: none;'>" . $row->jumlah_diajukan . "</td>
							<td id='f_jumlah_diajukan'><small>" . ArrayAdapter::format("number", $row->jumlah_diajukan) . "</small></td>
							<td id='jumlah_disetujui' style='display: none;'>" . $row->jumlah_disetujui . "</td>
							<td id='f_jumlah_disetujui'><small>" . ArrayAdapter::format("number", $row->jumlah_disetujui) . "</small></td>
							<td id='satuan'><small>" . $row->satuan . "</small></td>
							<td id='hps' style='display: none;'>" . $row->hps . "</td>
							<td id='f_hps'><small>" . ArrayAdapter::format("money", $row->hps) . "</small></td>
							<td id='subtotal' style='display: none;'>" . $subtotal . "</td>
							<td id='f_subtotal'><small>" . $f_subtotal . "</small></td>
							<td id='medis' style='display: none;'>" . $row->medis . "</td>
							<td id='inventaris' style='display: none;'>" . $row->inventaris . "</td>
							<td>" . $html_action . "</td>
						</tr>
					";
					$num++;
				}
			}
			$data = array();
			$data['html'] = $html;
			$data['row_num'] = $num;
			$data['editable'] = $editable;
			echo json_encode($data);
		} else if ($_POST['command'] == "show_footer") {
			$html = "<tfoot>
						<tr>
							<td colspan='8'><center><small><strong>T O T A L</strong></small></center></td>
							<td id='total'></td>
							<td>&nbsp;</td>
						</tr>
					</tfoot>";
			$data = array();
			$data['html'] = $html;
			echo json_encode($data);
		} else if ($_POST['command'] == "get_current_stock") {
			$params = array();
			$params['id_obat'] = $_POST['id_obat'];
			$params['satuan'] = $_POST['satuan'];
			$params['konversi'] = 1;
			$params['satuan_konversi'] = $_POST['satuan'];
			$consumer_service = new ServiceConsumer(
				$db,
				"get_single_stock_info",
				$params,
				"gudang_farmasi"
			);
			$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
			$content = $consumer_service->execute()->getContent();
			$jumlah = 0;
			if ($content != null)
				$jumlah = number_format($content[0], 0, ",", ".");
			$data = array(
				"jumlah" => $jumlah
			);
			echo json_encode($data);
		} else if ($_POST['command'] == "get_last_hpp") {
			$params = array();
			$params['id_obat'] = $_POST['id_obat'];
			$params['satuan'] = $_POST['satuan'];
			$consumer_service = new ServiceConsumer(
				$db,
				"get_last_hpp",
				$params,
				"gudang_farmasi"
			);
			$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
			$content = $consumer_service->execute()->getContent();
			$hpp = "Rp. 0,00";
			if ($content != null)
				$hpp = ArrayAdapter::format("only-money Rp. ", $content[0]['hpp']);
			$data = array(
				"hpp" => $hpp
			);
			echo json_encode($data);
		} else {
			$adapter = new SimpleAdapter(true, "No.");
			$adapter->add("No. RP", "id", "digit8");
			$adapter->add("Tanggal", "tanggal", "date d-m-Y");
			$adapter->add("Kode Vendor", "kode_vendor");
			$adapter->add("Nama Vendor", "nama_vendor");
			$dbtable = new DBTable($db, "smis_pr_rencana_pembelian");
			$dbtable->addCustomKriteria(" medis ", " = '1' ");
			$dbresponder = new RencanaPembelianFarmasiDBResponder(
				$dbtable,
				$table,
				$adapter
			);
			$data = $dbresponder->command($_POST['command']);
			echo json_encode($data);
		}
		return;
	}
	
	echo $header_form->getHtml();
	echo "<div id='table_content'>" .
			 "<div class='row-fluid'>";
	if ($editable == "true") {
		echo	 "<div class='span4'>" .
					 $detail_form->getHtml() .
				 "</div>" .
				 "<div class='span8'>" .
					 $table->getHtml() .
				 "</div>";
	} else {
		echo	 "<div class='span12'>" .
					 $table->getHtml() .
				 "</div>";
	}
	echo 	 "</div>" .
			 "<div class='row-fluid'>" .
				 "<div class='span12' align='right'>" .
					$button_group->getHtml() .
				 "</div>" .
			 "</div>" .
		 "</div>";
	echo addJS("framework/bootstrap/js/bootstrap-datepicker.js");
	echo addCSS("framework/bootstrap/css/datepicker.css");
	echo addJS("perencanaan/js/rencana_pembelian_farmasi_vendor_action.js", false);
	echo addJS("perencanaan/js/rencana_pembelian_farmasi_barang_action.js", false);
	echo addJS("perencanaan/js/drencana_pembelian_farmasi_action.js", false);
	echo addJS("perencanaan/js/rencana_pembelian_farmasi_action.js", false);
	echo addJS("perencanaan/js/rencana_pembelian_farmasi_form.js", false);
?>