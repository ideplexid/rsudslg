function KodeInventarisAction(name, page, action, column) {
	this.initialize(name, page, action, column);
}
KodeInventarisAction.prototype.constructor = KodeInventarisAction;
KodeInventarisAction.prototype = new TableAction();
KodeInventarisAction.prototype.selected = function(json) {
	$("#mapping_acc_barang_inventaris_acc_kode_inventaris").val(json.nomor);
};
KodeInventarisAction.prototype.clear = function() {
	$("#mapping_acc_barang_inventaris_acc_kode_inventaris").val("");
};