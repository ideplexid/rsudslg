function JenisBarangNonMedisAction(name, page, action, column) {
	this.initialize(name, page, action, column);
}
JenisBarangNonMedisAction.prototype.constructor = JenisBarangNonMedisAction;
JenisBarangNonMedisAction.prototype = new TableAction();
JenisBarangNonMedisAction.prototype.getSaveData = function() {
	var data = this.getRegulerData();
	data['command'] = "save";
	data['id'] = $("#jenis_barang_non_medis_id").val();
	data['kode'] = $("#jenis_barang_non_medis_kode").val();
	data['nama'] = $("#jenis_barang_non_medis_nama").val();
	data['medis'] = "0";
	return data;
};
JenisBarangNonMedisAction.prototype.selected = function(json) {
	$("#barang_non_medis_id_jenis_barang").val(json.id);
	$("#barang_non_medis_kode_jenis_barang").val(json.kode);
	$("#barang_non_medis_nama_jenis_barang").val(json.nama);
	barang_non_medis.get_last_code();
};