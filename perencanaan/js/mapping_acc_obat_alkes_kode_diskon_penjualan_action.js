function KodeDiskonPenjualanAction(name, page, action, column) {
	this.initialize(name, page, action, column);
}
KodeDiskonPenjualanAction.prototype.constructor = KodeDiskonPenjualanAction;
KodeDiskonPenjualanAction.prototype = new TableAction();
KodeDiskonPenjualanAction.prototype.selected = function(json) {
	$("#mapping_acc_obat_alkes_acc_kode_diskon_penjualan").val(json.nomor);
};