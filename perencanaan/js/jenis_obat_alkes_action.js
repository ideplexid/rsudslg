function JenisObatAlkesAction(name, page, action, column) {
	this.initialize(name, page, action, column);
}
JenisObatAlkesAction.prototype.constructor = JenisObatAlkesAction;
JenisObatAlkesAction.prototype = new TableAction();
JenisObatAlkesAction.prototype.getSaveData = function() {
	var data = this.getRegulerData();
	data['command'] = "save";
	data['id'] = $("#jenis_obat_alkes_id").val();
	data['kode'] = $("#jenis_obat_alkes_kode").val();
	data['nama'] = $("#jenis_obat_alkes_nama").val();
	data['medis'] = "1";
	return data;
};
JenisObatAlkesAction.prototype.selected = function(json) {
	$("#obat_alkes_id_jenis_barang").val(json.id);
	$("#obat_alkes_kode_jenis_barang").val(json.kode);
	$("#obat_alkes_nama_jenis_barang").val(json.nama);
};