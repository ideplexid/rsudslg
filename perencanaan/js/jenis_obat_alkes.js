var jenis_obat_alkes;

$(document).ready(function() {
	$('[data-toggle="popover"]').popover({
		trigger: 'hover',
		'placement': 'top'
	});
	var jenis_obat_alkes_columns = new Array("id", "kode", "nama", "medis");
	jenis_obat_alkes = new JenisObatAlkesAction(
		"jenis_obat_alkes",
		"perencanaan",
		"jenis_obat_alkes",
		jenis_obat_alkes_columns
	);
	jenis_obat_alkes.view();
});