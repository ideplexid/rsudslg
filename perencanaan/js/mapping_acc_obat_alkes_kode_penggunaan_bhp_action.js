function KodePenggunaanBHPAction(name, page, action, column) {
	this.initialize(name, page, action, column);
}
KodePenggunaanBHPAction.prototype.constructor = KodePenggunaanBHPAction;
KodePenggunaanBHPAction.prototype = new TableAction();
KodePenggunaanBHPAction.prototype.selected = function(json) {
	$("#mapping_acc_obat_alkes_acc_kode_penggunaan_bhp").val(json.nomor);
};
KodePenggunaanBHPAction.prototype.clear = function() {
	$("#mapping_acc_obat_alkes_acc_kode_penggunaan_bhp").val("");
};