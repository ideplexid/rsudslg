var mapping_acc_obat_alkes;
var kode_nilai_persediaan;
var kode_hpp;
var kode_pendapatan;
var kode_diskon_penjualan;
var kode_piutang;
var kode_ppn_keluar;

$(document).ready(function() {
	$('[data-toggle="popover"]').popover({
		trigger: 'hover',
		'placement': 'top'
	});
	kode_ppn_keluar = new KodePPnKeluarAction(
		"kode_ppn_keluar",
		"perencanaan",
		"mapping_acc_obat_alkes",
		new Array()
	);
	kode_ppn_keluar.setSuperCommand("kode_ppn_keluar");
	kode_piutang = new KodePiutangAction(
		"kode_piutang",
		"perencanaan",
		"mapping_acc_obat_alkes",
		new Array()
	);
	kode_piutang.setSuperCommand("kode_piutang");
	kode_nilai_persediaan = new KodeNilaiPersediaanAction(
		"kode_nilai_persediaan",
		"perencanaan",
		"mapping_acc_obat_alkes",
		new Array()
	);
	kode_nilai_persediaan.setSuperCommand("kode_nilai_persediaan");
	kode_hpp = new KodeHPPAction(
		"kode_hpp",
		"perencanaan",
		"mapping_acc_obat_alkes",
		new Array()
	);
	kode_hpp.setSuperCommand("kode_hpp");
	kode_pendapatan = new KodePendapatanAction(
		"kode_pendapatan",
		"perencanaan",
		"mapping_acc_obat_alkes",
		new Array()
	);
	kode_pendapatan.setSuperCommand("kode_pendapatan");
	kode_diskon_penjualan = new KodeDiskonPenjualanAction(
		"kode_diskon_penjualan",
		"perencanaan",
		"mapping_acc_obat_alkes",
		new Array()
	);
	kode_diskon_penjualan.setSuperCommand("kode_diskon_penjualan");
	var mapping_acc_obat_alkes_columns = new Array("id", "kode", "nama", "id_jenis_barang", "kode_jenis_barang", "nama_jenis_barang", "acc_kode_nilai_persediaan", "acc_kode_hpp", "acc_kode_pendapatan", "acc_kode_piutang", "acc_kode_diskon_penjualan", "acc_kode_ppn_keluar");
	mapping_acc_obat_alkes = new TableAction(
		"mapping_acc_obat_alkes",
		"perencanaan",
		"mapping_acc_obat_alkes",
		mapping_acc_obat_alkes_columns
	);
	mapping_acc_obat_alkes.view();
});