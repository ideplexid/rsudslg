var barang_non_medis;
var jenis_barang_non_medis;

$(document).ready(function() {
	$('[data-toggle="popover"]').popover({
		trigger: 'hover',
		'placement': 'top'
	});
	var barang_non_medis_columns = new Array("id", "kode", "nama", "id_jenis_barang", "kode_jenis_barang", "nama_jenis_barang", "harga_pbf", "formularium", "berlogo", "generik", "narkotika", "okt", "satuan", "konversi", "satuan_konversi");
	barang_non_medis = new BarangNonMedisAction(
		"barang_non_medis",
		"perencanaan",
		"barang_non_medis",
		barang_non_medis_columns
	);
	jenis_barang_non_medis = new JenisBarangNonMedisAction(
		"jenis_barang_non_medis",
		"perencanaan",
		"barang_non_medis",
		new Array()
	);
	jenis_barang_non_medis.setSuperCommand("jenis_barang_non_medis");
	barang_non_medis.view();
	$("#barang_non_medis_prefix_kode").on("change", function() {
		barang_non_medis.get_last_code();
	});
});