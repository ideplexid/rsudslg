function KodeDepresiasiAction(name, page, action, column) {
	this.initialize(name, page, action, column);
}
KodeDepresiasiAction.prototype.constructor = KodeDepresiasiAction;
KodeDepresiasiAction.prototype = new TableAction();
KodeDepresiasiAction.prototype.selected = function(json) {
	$("#mapping_acc_barang_inventaris_acc_kode_depresiasi").val(json.nomor);
};
KodeDepresiasiAction.prototype.clear = function() {
	$("#mapping_acc_barang_inventaris_acc_kode_depresiasi").val("");
};