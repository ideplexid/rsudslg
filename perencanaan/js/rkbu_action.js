function RKBUAction(name, page, action, column) {
	this.initialize(name, page, action, column);
}
RKBUAction.prototype.constructor = RKBUAction;
RKBUAction.prototype = new TableAction();
RKBUAction.prototype.getViewData = function() {
	var data = TableAction.prototype.getViewData.call(this);
	data['search_unit'] = $("#search_unit").val();
	data['search_nomor'] = $("#search_nomor").val();
	data['search_tahun'] = $("#search_tahun").val();
	data['search_bulan'] = $("#search_bulan").val();
	data['search_tanggal_diajukan'] = $("#search_tanggal_diajukan").val();
	data['search_tanggal_ditinjau'] = $("#search_tanggal_ditinjau").val();
	data['search_status'] = $("#search_status").val();
	return data;
};
RKBUAction.prototype.edit = function(id) {
	var self = this;
	var data = this.getRegulerData();
	data['command'] = "edit";
	data['id'] = id;
	$.post(
		"",
		data,
		function(response) {
			var json = getContent(response);
			if (json == null) return;
			$("#rkbu_id").val(json.header.id);
			$("#rkbu_fid").val(json.header.f_id);
			$("#rkbu_unit").val(json.header.unit.replace("_", " ").toUpperCase());
			$("#rkbu_tahun").val(json.header.tahun);
			$("#rkbu_tanggal_diajukan").val(json.header.tanggal_diajukan);
			$("#rkbu_keterangan").val(json.header.keterangan);
			$("#rkbu_tinjauan").val(json.header.tinjauan);
			$("#rkbu_edit_keterangan").removeAttr("onclick");
			$("#rkbu_edit_keterangan").removeAttr("disabled");
			$("#rkbu_edit_keterangan").attr("disabled", "disabled");
			$("#rkbu_edit_tinjauan").removeAttr("onclick");
			$("#rkbu_edit_tinjauan").attr("onclick", "smis_summernote_write('rkbu', 'rkbu_tinjauan', 'rkbu_add_form')");
			$("#rkbu_edit_tinjauan").removeAttr("disabled");
			$("#rkbu_tanggal_ditinjau").val(json.header.tanggal_ditinjau);
			$("#rkbu_tanggal_ditinjau").removeAttr("disabled");
			$("#rkbu_status").val(json.header.disetujui);
			$("#rkbu_status").removeAttr("disabled");
			row_id = 0;
			$("tbody#drkbu_list").children().remove();
			for(var i = 0; i < json.detail.length; i++) {
				var drkbu_id = json.detail[i].id;
				var drkbu_f_id = json.detail[i].f_id;
				var drkbu_id_rkbu = json.detail[i].id_rkbu;
				var drkbu_bulan = json.detail[i].bulan;
				var drkbu_nama_barang = json.detail[i].nama_barang;
				var drkbu_jenis_barang = json.detail[i].jenis_barang;
				var drkbu_jumlah_diajukan = json.detail[i].jumlah_diajukan;
				var drkbu_satuan_diajukan = json.detail[i].satuan_diajukan;
				var drkbu_jumlah_disetujui = json.detail[i].jumlah_disetujui;
				var drkbu_satuan_disetujui = json.detail[i].satuan_disetujui;
				var f_disetujui = "-";
				if (Number(drkbu_jumlah_disetujui) != 0 && drkbu_satuan_disetujui != null)
					f_disetujui = drkbu_jumlah_disetujui + " " + drkbu_satuan_disetujui;
				$("tbody#drkbu_list").append(
					"<tr id='data_" + row_id + "'>" +
						"<td style='display: none;' id='data_" + row_id + "_id'>" + drkbu_id + "</td>" +
						"<td style='display: none;' id='data_" + row_id + "_f_id'>" + drkbu_f_id + "</td>" +
						"<td style='display: none;' id='data_" + row_id + "_jenis_barang'>" + drkbu_jenis_barang + "</td>" +
						"<td style='display: none;' id='data_" + row_id + "_jumlah_diajukan'>" + drkbu_jumlah_diajukan + "</td>" +
						"<td style='display: none;' id='data_" + row_id + "_satuan_diajukan'>" + drkbu_satuan_diajukan + "</td>" +
						"<td style='display: none;' id='data_" + row_id + "_jumlah_disetujui'>" + drkbu_jumlah_disetujui + "</td>" +
						"<td style='display: none;' id='data_" + row_id + "_satuan_disetujui'>" + drkbu_satuan_disetujui + "</td>" +
						"<td id='data_" + row_id + "_bulan'>" + drkbu_bulan + "</td>" +
						"<td id='data_" + row_id + "_nama_barang'>" + drkbu_nama_barang + "</td>" +
						"<td id='data_" + row_id + "_f_jumlah'>" + drkbu_jumlah_diajukan + " " + drkbu_satuan_diajukan + "</td>" +
						"<td id='data_" + row_id + "_f_disetujui'>" + f_disetujui + "</td>" +
						"<td>" + 
							"<div class='btn-group noprint'>" +
								"<a href='#' onclick='drkbu.edit(" + row_id + ")' data-content='Ubah' data-toggle='popover' class='input btn btn-warning'>" + 
									"<i class='icon-edit icon-white'></i>" +
								"</a>" +
							"</div>" +
						"</td>" +
					"</tr>"
				);
				row_id++;
			}
			$("#rkbu_add_form").smodal("show");
			$("#modal_alert_rkbu_add_form").html("");
			$(".error_field").removeClass("error_field");
			$(".error_field").removeClass("error_field");
			$("#rkbu_save").show();
			$("#rkbu_save").removeAttr("onclick");
			$("#rkbu_save").attr("onclick", "rkbu.update(" + id + ")");
			$("#rkbu_ok").hide();
		}
	);
};
RKBUAction.prototype.validate = function() {
	var valid = true;
	var invalid_msg = "";
	var rkbu_tanggal_ditinjau = $("#rkbu_tanggal_ditinjau").val();
	var rkbu_tinjauan = $("#rkbu_tinjauan").val();
	var rkbu_status = $("#rkbu_status").val();
	$(".error_field").removeClass("error_field");
	if (rkbu_status == "") {
		valid = false;
		invalid_msg += "</br><strong>Status</strong> tidak boleh kosong";
		$("#rkbu_status").addClass("error_field");
	}
	if (rkbu_tinjauan == "") {
		valid = false;
		invalid_msg += "</br><strong>Tinjauan</strong> tidak boleh kosong";
	}
	if (rkbu_tanggal_ditinjau == "") {
		valid = false;
		invalid_msg += "</br><strong>Tgl. Ditinjau</strong> tidak boleh kosong";
		$("#rkbu_tanggal_ditinjau").addClass("error_field");
	}
	if (!valid) {
		$("#modal_alert_rkbu_add_form").html(
			"<div class='alert alert-block alert-danger'>" +
				"<h4>Peringatan</h4>" +
				invalid_msg +
			"</div>"
		);
	}
	return valid;
};
RKBUAction.prototype.update = function(id) {
	if (!this.validate()) {
		return;
	}
	$("#rkbu_add_form").smodal("hide");
	showLoading();
	var self = this;
	var data = this.getRegulerData();
	data['command'] = "save";
	data['id'] = id;
	data['f_id'] = $("#rkbu_fid").val();
	data['unit'] = $("#rkbu_unit").val().replace(" ", "_").toLowerCase();
	data['tanggal_diajukan'] = $("#rkbu_tanggal_diajukan").val();
	data['tanggal_ditinjau'] = $("#rkbu_tanggal_ditinjau").val();
	data['keterangan'] = $("#rkbu_keterangan").val();
	data['tinjauan'] = $("#rkbu_tinjauan").val();
	data['locked'] = "1";
	data['disetujui'] = $("#rkbu_status").val();
	var detail = {};
	var nor = $("tbody#drkbu_list").children("tr").length;
	for(var i = 0; i < nor; i++) {
		var d_data = {};
		var prefix = $("tbody#drkbu_list").children('tr').eq(i).prop("id");
		d_data['id'] = $("#" + prefix + "_id").text();
		d_data['f_id'] = $("#" + prefix + "_f_id").text();
		d_data['bulan'] = $("#" + prefix + "_bulan").text();
		d_data['nama_barang'] = $("#" + prefix + "_nama_barang").text();
		d_data['jenis_barang'] = $("#" + prefix + "_jenis_barang").text();
		d_data['jumlah_diajukan'] = $("#" + prefix + "_jumlah_diajukan").text();
		d_data['satuan_diajukan'] = $("#" + prefix + "_satuan_diajukan").text();
		d_data['jumlah_disetujui'] = $("#" + prefix + "_jumlah_disetujui").text();
		d_data['satuan_disetujui'] = $("#" + prefix + "_satuan_disetujui").text();
		if ($("#" + prefix).attr("class") == "deleted") {
			d_data['cmd'] = "delete";
		} else if ($("#" + prefix + "_id").text() == "") {
			d_data['cmd'] = "insert";
		} else {
			d_data['cmd'] = "update";
		}
		detail[i] = d_data;
	}
	data['detail'] = detail;
	var confirm_msg = "";
	if (data['disetujui'] != "belum") {
		data['push_command'] = "permanent";
	}
	$.post(
		"",
		data,
		function(response) {
			var json = getContent(response);
			if (json == null) {
				$("#rkbu_add_form").smodal("show");
			} else {
				self.view();
				rincian_rkbu.view();
			}
			dismissLoading();
		}
	);
};
RKBUAction.prototype.return = function(id) {
	var self = this;
	var today = new Date();
	var data = this.getRegulerData();
	data['command'] = "save";
	data['push_command'] = "return";
	data['id'] = id;
	data['tanggal_ditinjau'] = today.getFullYear() + "-" + (Number(today.getMonth())+1) + "-" + today.getDate();
	data['locked'] = "0";
	bootbox.confirm(
		"Anda yakin mengembalikan RKBU ini?",
		function(result) {
			if (result) {
				showLoading();
				$.post(
					"",
					data,
					function() {
						self.view();
						rincian_rkbu.view();
						dismissLoading();
					}
				);
			}
		}
	);
};
RKBUAction.prototype.detail = function(id) {
	var self = this;
	var data = this.getRegulerData();
	data['command'] = "edit";
	data['id'] = id;
	$.post(
		"",
		data,
		function(response) {
			var json = getContent(response);
			if (json == null) return;
			$("#rkbu_id").val(json.header.id);
			$("#rkbu_fid").val(json.header.f_id);
			$("#rkbu_unit").val(json.header.unit.replace("_", " ").toUpperCase());
			$("#rkbu_tahun").val(json.header.tahun);
			$("#rkbu_tanggal_diajukan").val(json.header.tanggal_diajukan);
			$("#rkbu_keterangan").val(json.header.keterangan);
			$("#rkbu_tinjauan").val(json.header.tinjauan);
			$("#rkbu_edit_keterangan").removeAttr("onclick");
			$("#rkbu_edit_keterangan").attr("onclick", "smis_summernote_write('rkbu', 'rkbu_keterangan', 'rkbu_add_form')");
			$("#rkbu_edit_keterangan").removeAttr("disabled");
			$("#rkbu_edit_tinjauan").removeAttr("onclick");
			$("#rkbu_edit_tinjauan").removeAttr("disabled");
			$("#rkbu_edit_tinjauan").attr("disabled", "disabled");
			$("#rkbu_tanggal_ditinjau").val(json.header.tanggal_ditinjau);
			$("#rkbu_tanggal_ditinjau").removeAttr("disabled");
			$("#rkbu_tanggal_ditinjau").attr("disabled", "disabled");
			$("#rkbu_status").val(json.header.disetujui);
			$("#rkbu_status").removeAttr("disabled");
			$("#rkbu_status").attr("disabled", "disabled");
			row_id = 0;
			$("tbody#drkbu_list").children().remove();
			for(var i = 0; i < json.detail.length; i++) {
				var drkbu_id = json.detail[i].id;
				var drkbu_f_id = json.detail[i].f_id;
				var drkbu_id_rkbu = json.detail[i].id_rkbu;
				var drkbu_bulan = json.detail[i].bulan;
				var drkbu_nama_barang = json.detail[i].nama_barang;
				var drkbu_jenis_barang = json.detail[i].jenis_barang;
				var drkbu_jumlah_diajukan = json.detail[i].jumlah_diajukan;
				var drkbu_satuan_diajukan = json.detail[i].satuan_diajukan;
				var drkbu_jumlah_disetujui = json.detail[i].jumlah_disetujui;
				var drkbu_satuan_disetujui = json.detail[i].satuan_disetujui;
				var f_disetujui = "-";
				if (Number(drkbu_jumlah_disetujui) != 0 && drkbu_satuan_disetujui != null)
					f_disetujui = drkbu_jumlah_disetujui + " " + drkbu_satuan_disetujui;
				$("tbody#drkbu_list").append(
					"<tr id='data_" + row_id + "'>" +
						"<td style='display: none;' id='data_" + row_id + "_id'>" + drkbu_id + "</td>" +
						"<td style='display: none;' id='data_" + row_id + "_f_id'>" + drkbu_f_id + "</td>" +
						"<td style='display: none;' id='data_" + row_id + "_jenis_barang'>" + drkbu_jenis_barang + "</td>" +
						"<td style='display: none;' id='data_" + row_id + "_jumlah_diajukan'>" + drkbu_jumlah_diajukan + "</td>" +
						"<td style='display: none;' id='data_" + row_id + "_satuan_diajukan'>" + drkbu_satuan_diajukan + "</td>" +
						"<td style='display: none;' id='data_" + row_id + "_jumlah_disetujui'>" + drkbu_jumlah_disetujui + "</td>" +
						"<td style='display: none;' id='data_" + row_id + "_satuan_disetujui'>" + drkbu_satuan_disetujui + "</td>" +
						"<td id='data_" + row_id + "_bulan'>" + drkbu_bulan + "</td>" +
						"<td id='data_" + row_id + "_nama_barang'>" + drkbu_nama_barang + "</td>" +
						"<td id='data_" + row_id + "_f_jumlah'>" + drkbu_jumlah_diajukan + " " + drkbu_satuan_diajukan + "</td>" +
						"<td id='data_" + row_id + "_f_disetujui'>" + f_disetujui + "</td>" +
						"<td></td>" +
					"</tr>"
				);
				row_id++;
			}
			$("#rkbu_add_form").smodal("show");
			$("#modal_alert_rkbu_add_form").html("");
			$(".error_field").removeClass("error_field");
			$("#rkbu_save").hide();
			$("#rkbu_save").removeAttr("onclick");
			$("#rkbu_ok").show();
		}
	);
};
RKBUAction.prototype.cancel = function(id) {
	var self = this;
	var data = this.getRegulerData();
	data['command'] = "save";
	data['id'] = id;
	data['disetujui'] = "belum";
	bootbox.confirm(
		"Yakin membatalkan persetujuan RKBU ini?",
		function(result) {
			if (result) {
				showLoading();
				$.post(
					"",
					data,
					function() {
						self.view();
						rincian_rkbu.view();
						dismissLoading();
					}
				);
			}
		}
	);
};