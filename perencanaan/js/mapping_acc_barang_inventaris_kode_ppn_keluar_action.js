function KodePPnKeluarAction(name, page, action, column) {
	this.initialize(name, page, action, column);
}
KodePPnKeluarAction.prototype.constructor = KodePPnKeluarAction;
KodePPnKeluarAction.prototype = new TableAction();
KodePPnKeluarAction.prototype.selected = function(json) {
	$("#mapping_acc_barang_inventaris_acc_kode_ppn_keluar").val(json.nomor);
};