var jenis_barang_non_medis;

$(document).ready(function() {
	$('[data-toggle="popover"]').popover({
		trigger: 'hover',
		'placement': 'top'
	});
	var jenis_barang_non_medis_columns = new Array("id", "kode", "nama", "medis");
	jenis_barang_non_medis = new JenisBarangNonMedisAction(
		"jenis_barang_non_medis",
		"perencanaan",
		"jenis_barang_non_medis",
		jenis_barang_non_medis_columns
	);
	jenis_barang_non_medis.view();
});