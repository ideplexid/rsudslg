function BarangAction (name, page, action, column) {
	this.initialize(name, page, action, column);
}
BarangAction.prototype.constructor = BarangAction;
BarangAction.prototype = new TableAction();
BarangAction.prototype.selected = function(json) {
	showLoading();
	$("#drencana_pembelian_farmasi_id_barang").val(json.id);
	$("#drencana_pembelian_farmasi_kode_barang").val(json.kode);
	$("#drencana_pembelian_farmasi_name_barang").val(json.nama);
	$("#drencana_pembelian_farmasi_nama_barang").val(json.nama);
	$("#drencana_pembelian_farmasi_nama_jenis_barang").val(json.nama_jenis_barang);
	$("#drencana_pembelian_farmasi_satuan").val(json.satuan);
	$("#drencana_pembelian_farmasi_medis").val(json.medis);
	$("#drencana_pembelian_farmasi_inventaris").val(json.inventaris);
	$("ul.typeahead").hide();
	var self = this;
	var data = this.getRegulerData();
	data['command'] = "get_last_hpp";
	data['super_command'] = "";
	data['id_obat'] = json.id;
	data['satuan'] = json.satuan;
	$.post(
		"",
		data,
		function(hpp_response) {
			var json_hpp = JSON.parse(hpp_response);
			if (json_hpp == null)
				$("#drencana_pembelian_farmasi_hps").val("Rp. 0,00");
			else
				$("#drencana_pembelian_farmasi_hps").val(json_hpp.hpp);
			data['command'] = "get_current_stock";
			$.post(
				"",
				data,
				function(sisa_response) {
					var json_sisa = JSON.parse(sisa_response);
					if (json_sisa == null)
						$("#drencana_pembelian_farmasi_sisa").val(0);
					else
						$("#drencana_pembelian_farmasi_sisa").val(json_sisa.jumlah);
					dismissLoading();
				}
			);
		}
	);
};