function KodePendapatanAction(name, page, action, column) {
	this.initialize(name, page, action, column);
}
KodePendapatanAction.prototype.constructor = KodePendapatanAction;
KodePendapatanAction.prototype = new TableAction();
KodePendapatanAction.prototype.selected = function(json) {
	$("#mapping_acc_obat_alkes_acc_kode_pendapatan").val(json.nomor);
};