function VendorAction(name, page, action, column) {
	this.initialize(name, page, action, column);
}
VendorAction.prototype.constructor = VendorAction;
VendorAction.prototype = new TableAction();
VendorAction.prototype.selected = function(json) {
	$("#rencana_pembelian_farmasi_id_vendor").val(json.id);
	$("#rencana_pembelian_farmasi_kode_vendor").val(json.kode);
	$("#rencana_pembelian_farmasi_nama_vendor").val(json.nama);
	$("#drencana_pembelian_farmasi_nama_barang").focus();
};