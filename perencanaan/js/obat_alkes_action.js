function ObatAlkesAction(name, page, action, column) {
	this.initialize(name, page, action, column);
}
ObatAlkesAction.prototype.constructor = ObatAlkesAction;
ObatAlkesAction.prototype = new TableAction();
ObatAlkesAction.prototype.validate = function() {
	var valid = true;
	var invalid_msg = "";
	var nama_obat = $("#obat_alkes_nama").val();
	var nama_jenis_obat = $("#obat_alkes_nama_jenis_barang").val();
	var satuan = $("#obat_alkes_satuan").val();
	var konversi = $("#obat_alkes_konversi").val();
	var satuan_konversi = $("#obat_alkes_satuan_konversi").val();
	var produsen = $("#obat_alkes_produsen").val();
	$(".error_field").removeClass("error_field");
	if (nama_obat == "") {
		valid = false;
		invalid_msg += "</br><strong>Nama</strong> tidak boleh kosong";
		$("#obat_alkes_nama").addClass("error_field");
	}
	if (nama_jenis_obat == "") {
		valid = false;
		invalid_msg += "</br><strong>Jenis Obat</strong> tidak boleh kosong";
		$("#obat_alkes_nama_jenis_barang").addClass("error_field");
	}
	if (satuan == "") {
		valid = false;
		invalid_msg += "</br><strong>Satuan</strong> tidak boleh kosong";
		$("#obat_alkes_satuan").addClass("error_field");
	}
	if (konversi == "") {
		valid = false;
		invalid_msg += "</br><strong>Konversi</strong> tidak boleh kosong";
		$("#obat_alkes_konversi").addClass("error_field");
	} else if (konversi <= 0) {
		valid = false;
		invalid_msg += "</br><strong>Konversi</strong> > 0";
		$("#obat_alkes_konversi").addClass("error_field");
	}
	if (satuan_konversi == "") {
		valid = false;
		invalid_msg += "</br><strong>Satuan Konversi</strong> tidak boleh kosong";
		$("#obat_alkes_satuan").addClass("error_field");
	}
	if (produsen == "") {
		valid = false;
		invalid_msg += "</br><strong>Produsen</strong> tidak boleh kosong";
		$("#obat_alkes_produsen").addClass("error_field");
	}
	if (!valid) {
		$("#modal_alert_obat_alkes_add_form").html(
			"<div class='alert alert-block alert-danger'>" +
				"<h4>Peringatan</h4>" +
				invalid_msg +
			"</div>"
		);
	}
	return valid;
};
ObatAlkesAction.prototype.save = function() {
	if (!this.validate())
		return;
	TableAction.prototype.save.call(this);
};
ObatAlkesAction.prototype.edit = function(id) {
	var data = this.getEditData(id);
	$.post(
		"",
		data,
		function(response) {
			var json = getContent(response);
			if (json == null) return;
			$("#obat_alkes_id").val(json.id);
			$("#obat_alkes_kode").val(json.kode);
			$("#obat_alkes_nama").val(json.nama);
			$("#obat_alkes_id_jenis_barang").val(json.id_jenis_barang);
			$("#obat_alkes_kode_jenis_barang").val(json.kode_jenis_barang);
			$("#obat_alkes_nama_jenis_barang").val(json.nama_jenis_barang);
			if (json.formularium == 1)
				$("#obat_alkes_formularium").prop("checked", true);
			else
				$("#obat_alkes_formularium").prop("checked", false);
			if (json.alkes == 1)
				$("#obat_alkes_alkes").prop("checked", true);
			else
				$("#obat_alkes_alkes").prop("checked", false);
			if (json.generik == 1)
				$("#obat_alkes_generik").prop("checked", true);
			else
				$("#obat_alkes_generik").prop("checked", false);
			if (json.berlogo == 1)
				$("#obat_alkes_berlogo").prop("checked", true);
			else
				$("#obat_alkes_berlogo").prop("checked", false);
			if (json.narkotika == 1)
				$("#obat_alkes_narkotika").prop("checked", true);
			else
				$("#obat_alkes_narkotika").prop("checked", false);
			if (json.okt == 1)
				$("#obat_alkes_okt").prop("checked", true);
			else
				$("#obat_alkes_okt").prop("checked", false);
			if (json.prekursor == 1)
				$("#obat_alkes_prekursor").prop("checked", true);
			else
				$("#obat_alkes_prekursor").prop("checked", false);
			$("#obat_alkes_satuan").val(json.satuan);
			$("#obat_alkes_konversi").val(json.konversi);
			$("#obat_alkes_satuan_konversi").val(json.satuan_konversi);
			$("#obat_alkes_produsen").val(json.produsen);
			$("#obat_alkes_add_form").smodal("show");
		}
	);
};