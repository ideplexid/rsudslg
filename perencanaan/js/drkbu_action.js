function DRKBUAction(name, page, action, column) {
	this.initialize(name, page, action, column);
}
DRKBUAction.prototype.constructor = DRKBUAction;
DRKBUAction.prototype = new TableAction();
DRKBUAction.prototype.edit = function(r_num) {
	var drkbu_id = $("#data_" + r_num + "_id").text();
	var drkbu_bulan = $("#data_" + r_num + "_bulan").text();
	var drkbu_jenis_barang = $("#data_" + r_num + "_jenis_barang").text();
	var drkbu_nama_barang = $("#data_" + r_num + "_nama_barang").text();
	var drkbu_jumlah_diajukan = $("#data_" + r_num + "_jumlah_diajukan").text();
	var drkbu_satuan_diajukan = $("#data_" + r_num + "_satuan_diajukan").text();
	var drkbu_jumlah_disetujui = $("#data_" + r_num + "_jumlah_disetujui").text();
	var drkbu_satuan_disetujui = $("#data_" + r_num + "_satuan_disetujui").text();
	$("#drkbu_id").val(drkbu_id);
	$("#drkbu_bulan").val(drkbu_bulan);
	$("#drkbu_jenis_barang").val(drkbu_jenis_barang);
	$("#drkbu_nama_barang").val(drkbu_nama_barang);
	$("#drkbu_jumlah_diajukan").val(drkbu_jumlah_diajukan + " " + drkbu_satuan_diajukan);
	$("#drkbu_jumlah_disetujui").val(drkbu_jumlah_disetujui);
	$("#drkbu_satuan_disetujui").val(drkbu_satuan_disetujui);
	$("#modal_alert_drkbu_add_form").html("");
	$(".error_field").removeClass("error_field");
	$("#drkbu_save").removeAttr("onclick");
	$("#drkbu_save").attr("onclick", "drkbu.update(" + r_num + ")");
	$("#drkbu_add_form").smodal("show");
};
DRKBUAction.prototype.validate = function() {
	var valid = true;
	var invalid_msg = "";
	var drkbu_jumlah_disetujui = $("#drkbu_jumlah_disetujui").val();
	var drkbu_satuan_disetujui = $("#drkbu_satuan_disetujui").val();
	$(".error_field").removeClass("error_field");
	if (drkbu_jumlah_disetujui == "") {
		valid = false;
		invalid_msg = "</br><strong>Jml. Disetujui</strong> tidak boleh kosong";
		$("#drkbu_jumlah_disetujui").addClass("error_field");
	} else if (!is_numeric(drkbu_jumlah_disetujui)) {
		valid = false;
		invalid_msg = "</br><strong>Jml. Disetujui</strong> seharusnya numerik (0-9)";
		$("#drkbu_jumlah_disetujui").addClass("error_field");
	}
	if (drkbu_satuan_disetujui == "") {
		valid = false;
		invalid_msg = "</br><strong>Satuan</strong> tidak boleh kosong";
		$("#drkbu_satuan_disetujui").addClass("error_field");
	}
	if (!valid) {
		$("#modal_alert_drkbu_add_form").html(
			"<div class='alert alert-block alert-danger'>" +
				"<h4>Peringatan</h4>" +
				invalid_msg +
			"</div>"
		);
	}
	return valid;
};
DRKBUAction.prototype.update = function(r_num) {
	if (!this.validate()) {
		return;
	}
	$("#data_" + r_num + "_jumlah_disetujui").text($("#drkbu_jumlah_disetujui").val());
	$("#data_" + r_num + "_satuan_disetujui").text($("#drkbu_satuan_disetujui").val());
	$("#data_" + r_num + "_f_disetujui").text($("#drkbu_jumlah_disetujui").val() + " " + $("#drkbu_satuan_disetujui").val());
	$("#drkbu_add_form").smodal("hide");
};
DRKBUAction.prototype.delete = function(r_num) {
	var id = $("#data_" + r_num + "_id").text();
	if (id.length == 0) {
		$("#data_" + r_num).remove();
	} else {
		$("#data_" + r_num).attr("style", "display: none;");
		$("#data_" + r_num).attr("class", "deleted");
	}
};