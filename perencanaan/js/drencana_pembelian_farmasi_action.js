function DRencanaPembelianFarmasiAction(name, page, action, column) {
	this.initialize(name, page, action, column);
}
DRencanaPembelianFarmasiAction.prototype.constructor = DRencanaPembelianFarmasiAction;
DRencanaPembelianFarmasiAction.prototype = new TableAction();
DRencanaPembelianFarmasiAction.prototype.validate = function() {
	var valid = true;
	var invalid_message = "";
	var nama_barang = $("#drencana_pembelian_farmasi_name_barang").val();
	var jumlah_diajukan = $("#drencana_pembelian_farmasi_jumlah").val();
	var satuan = $("#drencana_pembelian_farmasi_satuan").val();
	var hps = parseFloat($("#drencana_pembelian_farmasi_hps").val().replace(/[^0-9-,]/g, '').replace(",", "."));
	$(".error_field").removeClass("error_field");
	if (nama_barang == "") {
		valid = false;
		invalid_message += "</br><strong>Nama Barang</strong> tidak boleh kosong.";
		$("#drencana_pembelian_farmasi_nama_barang").addClass("error_field");
		$("#drencana_pembelian_farmasi_nama_barang").focus();
	}
	if (jumlah_diajukan == "") {
		valid = false;
		invalid_message += "</br><strong>Jumlah</strong> tidak boleh kosong.";
		$("#drencana_pembelian_farmasi_jumlah").addClass("error_field");
		$("#drencana_pembelian_farmasi_jumlah").focus();
	} else if (!is_numeric(jumlah_diajukan)) {
		valid = false;
		invalid_message += "</br><strong>Jumlah</strong> seharusnya numerik (0-9).";
		$("#drencana_pembelian_farmasi_jumlah").addClass("error_field");
		$("#drencana_pembelian_farmasi_jumlah").focus();
	} else if (jumlah_diajukan <= 0) {
		valid = false;
		invalid_message += "</br><strong>Jumlah</strong> seharusnya lebih dari nol (jumlah > 0).";
		$("#drencana_pembelian_farmasi_jumlah").addClass("error_field");
		$("#drencana_pembelian_farmasi_jumlah").focus();
	}
	if (satuan == "") {
		valid = false;
		invalid_message += "</br><strong>Satuan</strong> tidak boleh kosong.";
		$("#drencana_pembelian_farmasi_satuan").addClass("error_field");
		$("#drencana_pembelian_farmasi_satuan").focus();
	}
	if (hps == "" || hps == 0) {
		valid = false;
		invalid_message += "</br><strong>HPS</strong> tidak boleh kosong.";
		$("#drencana_pembelian_farmasi_hps").addClass("error_field");
		$("#drencana_pembelian_farmasi_hps").focus();
	}
	if (!valid)
		bootbox.alert(invalid_message);
	return valid;
};
DRencanaPembelianFarmasiAction.prototype.clear = function() {
	$("#drencana_pembelian_farmasi_id").val("");
	$("#drencana_pembelian_farmasi_id_barang").val("");
	$("#drencana_pembelian_farmasi_kode_barang").val("");
	$("#drencana_pembelian_farmasi_name_barang").val("");
	$("#drencana_pembelian_farmasi_nama_barang").val("");
	$("#drencana_pembelian_farmasi_nama_jenis_barang").val("");
	$("#drencana_pembelian_farmasi_sisa").val("");
	$("#drencana_pembelian_farmasi_jumlah").val("");
	$("#drencana_pembelian_farmasi_satuan").val("");
	$("#drencana_pembelian_farmasi_hps").val("Rp. 0,00");
	$("#drencana_pembelian_farmasi_medis").val("");
	$("#drencana_pembelian_farmasi_inventaris").val("");
};
DRencanaPembelianFarmasiAction.prototype.save = function() {
	if (!this.validate())
		return;
	var id = $("#drencana_pembelian_farmasi_id").val();
	var id_barang = $("#drencana_pembelian_farmasi_id_barang").val();
	var kode_barang = $("#drencana_pembelian_farmasi_kode_barang").val();
	var nama_barang = $("#drencana_pembelian_farmasi_name_barang").val();
	var nama_jenis_barang = $("#drencana_pembelian_farmasi_nama_jenis_barang").val();
	var jumlah_diajukan = parseFloat($("#drencana_pembelian_farmasi_jumlah").val());
	var f_jumlah_diajukan = jumlah_diajukan.formatMoney("0", ".", ",");
	var jumlah_disetujui = 0;
	var f_jumlah_disetujui = jumlah_disetujui.formatMoney("0", ".", ",");
	var satuan = $("#drencana_pembelian_farmasi_satuan").val();
	var hps = parseFloat($("#drencana_pembelian_farmasi_hps").val().replace(/[^0-9-,]/g, '').replace(",", "."));
	var f_hps = hps.formatMoney("2", ".", ",");
	var subtotal = jumlah_disetujui * hps;
	var f_subtotal = subtotal.formatMoney("2", ".", ",");
	var medis = $("#drencana_pembelian_farmasi_medis").val();
	var inventaris = $("#drencana_pembelian_farmasi_inventaris").val();
	$("tbody#drencana_pembelian_farmasi_list").append(
		"<tr id='data_" + row_num + "'>" +
			"<td id='nomor'></td>" +
			"<td id='id' style='display: none;'>" + id + "</td>" +
			"<td id='id_barang' style='display: none;'>" + id_barang + "</td>" +
			"<td id='kode_barang'><small>" + kode_barang + "</small></td>" +
			"<td id='nama_barang'><small>" + nama_barang + "</small></td>" +
			"<td id='nama_jenis_barang'><small>" + nama_jenis_barang + "</small></td>" +
			"<td id='jumlah_diajukan' style='display: none;'>" + jumlah_diajukan + "</td>" +
			"<td id='f_jumlah_diajukan'><small><div align='right'>" + f_jumlah_diajukan + "</div></small></td>" +
			"<td id='jumlah_disetujui' style='display: none;'>" + jumlah_disetujui + "</td>" +
			"<td id='f_jumlah_disetujui'><small><div align='right'>" + f_jumlah_disetujui + "</div></small></td>" +
			"<td id='satuan'><small>" + satuan + "</small></td>" +
			"<td id='hps' style='display: none;'>" + hps + "</td>" +
			"<td id='f_hps'><small><div align='right'>" + f_hps + "</div></small></td>" +
			"<td id='subtotal' style='display: none;'>" + subtotal + "</td>" +
			"<td id='f_subtotal'><small><div align='right'>" + f_subtotal + "</div></small></td>" +
			"<td id='medis' style='display: none;'>" + medis + "</td>" +
			"<td id='inventaris' style='display: none;'>" + inventaris + "</td>" +
			"<td>" +
				"<a href='#' onclick='drencana_pembelian_farmasi.del(" + row_num + ")' class='input btn btn-danger'>" +
					"<i class='fa fa-trash-o'></i>" +
				"</a>" +
			"</td>" +
		"</tr>"
	);
	this.clear();
	rencana_pembelian_farmasi.update_total();
	$("#drencana_pembelian_farmasi_nama_barang").focus();
	row_num++;
};
DRencanaPembelianFarmasiAction.prototype.del = function(r_num) {
	var id = $("tr#data_" + r_num + " td#id").text();
	if (id.length == 0) {
		$("#data_" + r_num).remove();
	} else {
		$("#data_" + r_num).attr("style", "display: none;");
		$("#data_" + r_num).attr("class", "deleted");
	}
	rencana_pembelian_farmasi.update_total();
};