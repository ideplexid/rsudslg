var usia_susut_inventaris;

$(document).ready(function() {
	$('[data-toggle="popover"]').popover({
		trigger: 'hover',
		'placement': 'top'
	});
	var usia_susut_inventaris_columns = new Array("id", "kode", "nama", "id_jenis_barang", "kode_jenis_barang", "nama_jenis_barang", "usia_susut");
	usia_susut_inventaris = new TableAction(
		"usia_susut_inventaris",
		"perencanaan",
		"usia_susut_inventaris",
		usia_susut_inventaris_columns
	);
	usia_susut_inventaris.view();
});