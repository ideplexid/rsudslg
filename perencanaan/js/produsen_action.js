function ProdusenAction(name, page, action, column) {
	this.initialize(name, page, action, column);
}
ProdusenAction.prototype.constructor = ProdusenAction;
ProdusenAction.prototype = new TableAction();
ProdusenAction.prototype.getSaveData = function() {
	var data = this.getRegulerData();
	data['command'] = "save";
	data['id'] = $("#produsen_id").val();
	data['kode'] = $("#produsen_kode").val();
	data['nama'] = $("#produsen_nama").val();
	data['medis'] = "1";
	return data;
};
ProdusenAction.prototype.selected = function(json) {
	$("#obat_alkes_produsen").val(json.nama);
};