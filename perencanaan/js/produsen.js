var produsen;

$(document).ready(function() {
	$('[data-toggle="popover"]').popover({
		trigger: 'hover',
		'placement': 'top'
	});
	var produsen_columns = new Array("id", "kode", "nama", "medis");
	produsen = new ProdusenAction(
		"produsen",
		"perencanaan",
		"produsen",
		produsen_columns
	);
	produsen.view();
});