var rencana_pembelian_farmasi;
var drencana_pembelian_farmasi;
var barang;
var vendor;
var row_num;
$(document).ready(function() {
	$(".mydate").datepicker();
	vendor = new VendorAction(
		"vendor",
		"perencanaan",
		"acc_rencana_pembelian_farmasi_form",
		new Array()
	);
	vendor.setSuperCommand("vendor");
	barang = new BarangAction(
		"barang",
		"perencanaan",
		"acc_rencana_pembelian_farmasi_form",
		new Array()
	);
	barang.setSuperCommand("barang");
	drencana_pembelian_farmasi = new AccDRencanaPembelianFarmasiAction(
		"drencana_pembelian_farmasi",
		"perencanaan",
		"acc_rencana_pembelian_farmasi_form",
		new Array("hps")
	);
	rencana_pembelian_farmasi = new AccRencanaPembelianFarmasiAction(
		"rencana_pembelian_farmasi",
		"perencanaan",
		"acc_rencana_pembelian_farmasi_form",
		new Array()
	);

	var id = $("#rencana_pembelian_farmasi_id").val();
	row_num = 0;
	rencana_pembelian_farmasi.get_footer();
	rencana_pembelian_farmasi.show_detail_form(id);
	drencana_pembelian_farmasi.setEditMode("false");
	$("#rencana_pembelian_farmasi_tanggal").focus();

	var barang_data = barang.getViewData();
	$("#drencana_pembelian_farmasi_nama_barang").typeahead({
		minLength	: 3,
		source		: function (query, process) {
			var $items = new Array;
			$items = [""];                
			barang_data['kriteria'] = $('#drencana_pembelian_farmasi_nama_barang').val();
			$.ajax({
				url		: "",
				type	: "POST",
				data	: barang_data,
				success	: function(response) {
					var json = getContent(response);
					var t_data = json.dbtable.data;
					$items = [""];      				
					$.map(t_data, function(data) {
						var group;
						group = {
							id		: data.id,
							name	: data.nama, 
							kode	: data.kode, 
							toString: function () {
								return JSON.stringify(this);
							},
							toLowerCase: function () {
								return this.name.toLowerCase();
							},
							indexOf: function (string) {
								return String.prototype.indexOf.apply(this.name, arguments);
							},
							replace: function (string) {
								var value = "";
								value +=  this.kode + " - " + this.name;
								if(typeof(this.level) != "undefined") {
									value += " <span class='pull-right muted'>";
									value += this.level;
									value += "</span>";
								}
								return String.prototype.replace.apply("<div class='typeaheadiv'>" + value + "</div>", arguments);
							}
						};
						$items.push(group);
					});
					process($items);
				}
			});
		},
		updater		: function (item) {
			var item = JSON.parse(item);  
			barang.select(item.id);  
			$("#drencana_pembelian_farmasi_jumlah").focus();       
			return item.name;
		}
	});

	$("#rencana_pembelian_farmasi_tanggal").keypress(function(e) {
		if (e.which == 13) {
			$("#vendor_browse").focus();
			$("div.datepicker").hide();
		}
	});
	$("#vendor_browse").keypress(function(e) {
		if (e.which == 13) {
			$("#vendor_browse").trigger("click");
		}
	});

	$("#drencana_pembelian_farmasi_nama_barang").keypress(function(e) {
		if (e.which == 13) {
			$("#drencana_pembelian_farmasi_jumlah").focus();
		}
	});
	$("#drencana_pembelian_farmasi_jumlah").keypress(function(e) {
		if (e.which == 13) {
			$("#drencana_pembelian_farmasi_hps").focus();
		}
	});
	$("#drencana_pembelian_farmasi_hps").keypress(function(e) {
		if (e.which == 13) {
			$("#rencana_pembelian_farmasi_form_save").trigger("click");
		}
	});
});