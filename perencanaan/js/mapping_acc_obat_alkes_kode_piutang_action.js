function KodePiutangAction(name, page, action, column) {
	this.initialize(name, page, action, column);
}
KodePiutangAction.prototype.constructor = KodePiutangAction;
KodePiutangAction.prototype = new TableAction();
KodePiutangAction.prototype.selected = function(json) {
	$("#mapping_acc_obat_alkes_acc_kode_piutang").val(json.nomor);
};