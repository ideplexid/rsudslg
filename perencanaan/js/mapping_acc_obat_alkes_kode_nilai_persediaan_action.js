function KodeNilaiPersediaanAction(name, page, action, column) {
	this.initialize(name, page, action, column);
}
KodeNilaiPersediaanAction.prototype.constructor = KodeNilaiPersediaanAction;
KodeNilaiPersediaanAction.prototype = new TableAction();
KodeNilaiPersediaanAction.prototype.selected = function(json) {
	$("#mapping_acc_obat_alkes_acc_kode_nilai_persediaan").val(json.nomor);
};