function AccRencanaPembelianFarmasiAction(name, page, action, column) {
	this.initialize(name, page, action, column);
}
AccRencanaPembelianFarmasiAction.prototype.constructor = AccRencanaPembelianFarmasiAction;
AccRencanaPembelianFarmasiAction.prototype = new TableAction();
AccRencanaPembelianFarmasiAction.prototype.show_add_form = function() {
	var data = this.getRegulerData();
	data['action'] = "acc_rencana_pembelian_farmasi_form";
	data['super_command'] = "";
	data['id'] = "";
	data['tanggal'] = "";
	data['id_vendor'] = "";
	data['kode_vendor'] = "";
	data['nama_vendor'] = "";
	data['medis'] = "1";
	data['editable'] = "true";
	LoadSmisPage(data);
};
AccRencanaPembelianFarmasiAction.prototype.show_detail_form = function(id) {
	if (id == "" || id == 0)
		return;
	var self = this;
	var data = this.getRegulerData();
	data['command'] = "show_detail";
	data['id'] = id;
	$.post(
		"",
		data,
		function(response) {
			var json = JSON.parse(response);
			if (json == null) return;
			$("#drencana_pembelian_farmasi_list").html(json.html);
			self.update_total();
			row_num = json.row_num;
		}
	);
};
AccRencanaPembelianFarmasiAction.prototype.get_footer = function() {
	if ($("#total").length != 0)
		return;
	var self = this;
	var data = this.getRegulerData();
	data['command'] = "show_footer";
	$.post(
		"",
		data,
		function(response) {
			var json = JSON.parse(response);
			if (json == null) return;
			$("tbody#drencana_pembelian_farmasi_list").after(json.html);
		}
	);
};
AccRencanaPembelianFarmasiAction.prototype.update_total = function() {
	var num_rows = $("tbody#drencana_pembelian_farmasi_list").children("tr").length;
	var total = 0;
	var nomor = 1;
	for (var i = 0; i < num_rows; i++) {
		var subtotal = parseFloat($("tbody#drencana_pembelian_farmasi_list tr:eq(" + i + ") td#subtotal").text());
		var deleted = $("tbody#drencana_pembelian_farmasi_list tr:eq(" + i + ")").attr("class") == "deleted" ? true : false;
		if (!deleted) {
			total += subtotal;
			$("tbody#drencana_pembelian_farmasi_list tr:eq(" + i + ") td:eq(0)").html("<small>" + nomor + "</small>");
			nomor++;
		}
	}
	$("#total").html("<small><strong><div align='right'>" + total.formatMoney("2", ".", ",") + "</div></strong></small>");
};
AccRencanaPembelianFarmasiAction.prototype.getSaveData = function() {
	var data = this.getRegulerData();
	data['command'] = "save";
	data['id'] = $("#rencana_pembelian_farmasi_id").val();
	data['tanggal'] = $("#rencana_pembelian_farmasi_tanggal").val();
	data['id_vendor'] = $("#rencana_pembelian_farmasi_id_vendor").val();
	data['kode_vendor'] = $("#rencana_pembelian_farmasi_kode_vendor").val();
	data['nama_vendor'] = $("#rencana_pembelian_farmasi_nama_vendor").val();
	data['medis'] = $("#rencana_pembelian_farmasi_medis").val();
	data['keterangan'] = "-";
	var num_rows = $("tbody#drencana_pembelian_farmasi_list").children("tr").length;
	var detail = {};
	for (var i = 0; i < num_rows; i++) {
		var item = {
			id 					: $("tbody#drencana_pembelian_farmasi_list tr:eq(" + i + ") td:eq(1)").text(),
			id_barang			: $("tbody#drencana_pembelian_farmasi_list tr:eq(" + i + ") td:eq(2)").text(),
			kode_barang			: $("tbody#drencana_pembelian_farmasi_list tr:eq(" + i + ") td:eq(3)").text(),
			nama_barang			: $("tbody#drencana_pembelian_farmasi_list tr:eq(" + i + ") td:eq(4)").text(),
			nama_jenis_barang	: $("tbody#drencana_pembelian_farmasi_list tr:eq(" + i + ") td:eq(5)").text(),
			jumlah_diajukan		: $("tbody#drencana_pembelian_farmasi_list tr:eq(" + i + ") td:eq(6)").text(),
			jumlah_disetujui	: $("tbody#drencana_pembelian_farmasi_list tr:eq(" + i + ") td:eq(8)").text(),
			satuan				: $("tbody#drencana_pembelian_farmasi_list tr:eq(" + i + ") td:eq(10)").text(),
			konversi			: 1,
			satuan_konversi		: $("tbody#drencana_pembelian_farmasi_list tr:eq(" + i + ") td:eq(10)").text(),
			hps					: $("tbody#drencana_pembelian_farmasi_list tr:eq(" + i + ") td:eq(11)").text(),
			medis				: $("tbody#drencana_pembelian_farmasi_list tr:eq(" + i + ") td:eq(15)").text(),
			inventaris			: $("tbody#drencana_pembelian_farmasi_list tr:eq(" + i + ") td:eq(16)").text(),
			deleted				: $("tbody#drencana_pembelian_farmasi_list tr:eq(" + i + ")").hasClass("deleted")
		};
		detail[i] = item;
	}
	data['detail'] = JSON.stringify(detail);
	return data;
};
AccRencanaPembelianFarmasiAction.prototype.validate = function() {
	var valid = true;
	var invalid_message = "";
	var tanggal = $("#rencana_pembelian_farmasi_tanggal").val();
	var id_vendor = $("#rencana_pembelian_farmasi_id_vendor").val();
	$(".error_field").removeClass("error_field");
	if (tanggal == "") {
		valid = false;
		invalid_message += "</br><strong>Tanggal</strong> tidak boleh kosong.";
		$("#rencana_pembelian_farmasi_tanggal").addClass("error_field");
		$("#rencana_pembelian_farmasi_tanggal").focus();
	}
	if (id_vendor == "") {
		valid = false;
		invalid_message += "</br><strong>Vendor</strong> tidak boleh kosong.";
		$("#rencana_pembelian_farmasi_nama_vendor").addClass("error_field");
		$("#vendor_browse").focus();
	}
	if (!valid)
		bootbox.alert(invalid_message);
	return valid;
};
AccRencanaPembelianFarmasiAction.prototype.save = function() {
	if (!this.validate())
		return;
	var self = this;
	var data = this.getSaveData();
	bootbox.dialog({
		message : "Yakin melakukan penyimpanan draft rencana pembelian ini?",
		title	: "Konfirmasi",
		buttons	: {
			success : {
				label 		: "Lanjutkan",
				className 	: "btn-success",
				callback	: function() {
					showLoading();
					$.post(
						"",
						data,
						function() {
							dismissLoading();
							self.back();
						}
					);
				}
			},
			danger 	: {
				label 		: "Batal",
				className	: "btn-inverse"
			}
		}
	});
};
AccRencanaPembelianFarmasiAction.prototype.edit = function(id) {
	var self = this;
	var data = this.getRegulerData();
	data['command'] = "edit";
	data['id'] = id;
	$.post(
		"",
		data,
		function(response) {
			var json = getContent(response);
			if (json == null) 
				return;
			data = self.getRegulerData();
			data['action'] = "acc_rencana_pembelian_farmasi_form";
			data['super_command'] = "";
			data['id'] = json.id;
			data['tanggal'] = json.tanggal;
			data['id_vendor'] = json.id_vendor;
			data['kode_vendor'] = json.kode_vendor;
			data['nama_vendor'] = json.nama_vendor;
			data['medis'] = json.medis;
			data['editable'] = "true";
			LoadSmisPage(data);
		}
	);
};
AccRencanaPembelianFarmasiAction.prototype.lock_acc = function(id) {
	var self = this;
	var data = this.getRegulerData();
	data['command'] = "save";
	data['id'] = id;
	data['lock_acc'] = 1;
	bootbox.dialog({
		message : "Yakin melakukan persetujuan draft rencana pembelian ini?<br/><br/><strong>PERINGATAN : </strong>Persetujuan draft rencana pembelian sekaligus melakukan penguncian draft rencana pembelian.",
		title	: "Konfirmasi",
		buttons	: {
			success : {
				label 		: "Lanjutkan",
				className 	: "btn-success",
				callback	: function() {
					showLoading();
					$.post(
						"",
						data,
						function() {
							dismissLoading();
							self.view();
						}
					);
				}
			},
			danger 	: {
				label 		: "Batal",
				className	: "btn-inverse"
			}
		}
	});
};
AccRencanaPembelianFarmasiAction.prototype.unlock_plan = function(id) {
	var self = this;
	var data = this.getRegulerData();
	data['command'] = "save";
	data['id'] = id;
	data['lock_plan'] = 0;
	bootbox.dialog({
		message : "Yakin membuka kunci draft rencana pembelian ini?<br/><br/><strong>PERINGATAN : </strong>Data acc. draft rencana pembelian farmasi akan hilang dan bagian perencanaan farmasi dapat melakukan sunting draft rencana pembelian farmasi kembali.",
		title	: "Konfirmasi",
		buttons	: {
			success : {
				label 		: "Lanjutkan",
				className 	: "btn-success",
				callback	: function() {
					$.post(
						"",
						data,
						function() {
							self.view();
						}
					);
				}
			},
			danger 	: {
				label 		: "Batal",
				className	: "btn-inverse"
			}
		}
	});
};
AccRencanaPembelianFarmasiAction.prototype.detail = function(id) {
	var self = this;
	var data = this.getRegulerData();
	data['command'] = "edit";
	data['id'] = id;
	$.post(
		"",
		data,
		function(response) {
			var json = getContent(response);
			if (json == null) 
				return;
			data = self.getRegulerData();
			data['action'] = "acc_rencana_pembelian_farmasi_form";
			data['super_command'] = "";
			data['id'] = json.id;
			data['tanggal'] = json.tanggal;
			data['id_vendor'] = json.id_vendor;
			data['kode_vendor'] = json.kode_vendor;
			data['nama_vendor'] = json.nama_vendor;
			data['medis'] = json.medis;
			data['editable'] = "false";
			LoadSmisPage(data);
		}
	);
};
AccRencanaPembelianFarmasiAction.prototype.export_xls = function(id) {
	var self = this;
	var data = this.getRegulerData();
	data['command'] = "export_xls";
	data['id'] = id;
	postForm(data);
};
AccRencanaPembelianFarmasiAction.prototype.back = function() {
	var data = this.getRegulerData();
	data['action'] = "acc_rencana_pembelian_farmasi";
	data['super_command'] = "";
	data['kriteria'] = "";
	data['max'] = 5;
	data['number'] = 0;
	LoadSmisPage(data);
};