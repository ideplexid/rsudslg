function AccDRencanaPembelianFarmasiAction(name, page, action, column) {
	this.initialize(name, page, action, column);
}
AccDRencanaPembelianFarmasiAction.prototype.constructor = AccDRencanaPembelianFarmasiAction;
AccDRencanaPembelianFarmasiAction.prototype = new TableAction();
AccDRencanaPembelianFarmasiAction.prototype.validate = function() {
	var valid = true;
	var invalid_message = "";
	var nama_barang = $("#drencana_pembelian_farmasi_name_barang").val();
	var jumlah_diajukan = $("#drencana_pembelian_farmasi_jumlah").val();
	var satuan = $("#drencana_pembelian_farmasi_satuan").val();
	var hps = parseFloat($("#drencana_pembelian_farmasi_hps").val().replace(/[^0-9-,]/g, '').replace(",", "."));
	$(".error_field").removeClass("error_field");
	if (nama_barang == "") {
		valid = false;
		invalid_message += "</br><strong>Nama Barang</strong> tidak boleh kosong.";
		$("#drencana_pembelian_farmasi_nama_barang").addClass("error_field");
		$("#drencana_pembelian_farmasi_nama_barang").focus();
	}
	if (jumlah_diajukan == "") {
		valid = false;
		invalid_message += "</br><strong>Jumlah</strong> tidak boleh kosong.";
		$("#drencana_pembelian_farmasi_jumlah").addClass("error_field");
		$("#drencana_pembelian_farmasi_jumlah").focus();
	} else if (!is_numeric(jumlah_diajukan)) {
		valid = false;
		invalid_message += "</br><strong>Jumlah</strong> seharusnya numerik (0-9).";
		$("#drencana_pembelian_farmasi_jumlah").addClass("error_field");
		$("#drencana_pembelian_farmasi_jumlah").focus();
	}
	if (satuan == "") {
		valid = false;
		invalid_message += "</br><strong>Satuan</strong> tidak boleh kosong.";
		$("#drencana_pembelian_farmasi_satuan").addClass("error_field");
		$("#drencana_pembelian_farmasi_satuan").focus();
	}
	if (hps == "" || hps == 0) {
		valid = false;
		invalid_message += "</br><strong>HPS</strong> tidak boleh kosong.";
		$("#drencana_pembelian_farmasi_hps").addClass("error_field");
		$("#drencana_pembelian_farmasi_hps").focus();
	}
	if (!valid)
		bootbox.alert(invalid_message);
	return valid;
};
AccDRencanaPembelianFarmasiAction.prototype.clear = function() {
	$("#drencana_pembelian_farmasi_row_num").val("");
	$("#drencana_pembelian_farmasi_id").val("");
	$("#drencana_pembelian_farmasi_id_barang").val("");
	$("#drencana_pembelian_farmasi_kode_barang").val("");
	$("#drencana_pembelian_farmasi_name_barang").val("");
	$("#drencana_pembelian_farmasi_nama_barang").val("");
	$("#drencana_pembelian_farmasi_nama_jenis_barang").val("");
	$("#drencana_pembelian_farmasi_sisa").val("");
	$("#drencana_pembelian_farmasi_jumlah").val("");
	$("#drencana_pembelian_farmasi_satuan").val("");
	$("#drencana_pembelian_farmasi_hps").val("Rp. 0,00");
	$("#drencana_pembelian_farmasi_medis").val("");
	$("#drencana_pembelian_farmasi_inventaris").val("");
};
AccDRencanaPembelianFarmasiAction.prototype.save = function() {
	if (!this.validate())
		return;
	var r_num = $("#drencana_pembelian_farmasi_row_num").val();
	var id = $("#drencana_pembelian_farmasi_id").val();
	var id_barang = $("#drencana_pembelian_farmasi_id_barang").val();
	var kode_barang = $("#drencana_pembelian_farmasi_kode_barang").val();
	var nama_barang = $("#drencana_pembelian_farmasi_name_barang").val();
	var nama_jenis_barang = $("#drencana_pembelian_farmasi_nama_jenis_barang").val();
	var jumlah_diajukan = 0;
	var f_jumlah_diajukan = jumlah_diajukan.formatMoney("0", ".", ",");
	var jumlah_disetujui = parseFloat($("#drencana_pembelian_farmasi_jumlah").val());
	var f_jumlah_disetujui = jumlah_disetujui.formatMoney("0", ".", ",");
	var satuan = $("#drencana_pembelian_farmasi_satuan").val();
	var hps = parseFloat($("#drencana_pembelian_farmasi_hps").val().replace(/[^0-9-,]/g, '').replace(",", "."));
	var f_hps = hps.formatMoney("2", ".", ",");
	var subtotal = jumlah_disetujui * hps;
	var f_subtotal = subtotal.formatMoney("2", ".", ",");
	var medis = $("#drencana_pembelian_farmasi_medis").val();
	var inventaris = $("#drencana_pembelian_farmasi_inventaris").val();
	if (r_num.length == 0) {
		$("tbody#drencana_pembelian_farmasi_list").append(
			"<tr id='data_" + row_num + "'>" +
				"<td id='nomor'></td>" +
				"<td id='id' style='display: none;'>" + id + "</td>" +
				"<td id='id_barang' style='display: none;'>" + id_barang + "</td>" +
				"<td id='kode_barang'><small>" + kode_barang + "</small></td>" +
				"<td id='nama_barang'><small>" + nama_barang + "</small></td>" +
				"<td id='nama_jenis_barang'><small>" + nama_jenis_barang + "</small></td>" +
				"<td id='jumlah_diajukan' style='display: none;'>" + jumlah_diajukan + "</td>" +
				"<td id='f_jumlah_diajukan'><small><div align='right'>" + f_jumlah_diajukan + "</div></small></td>" +
				"<td id='jumlah_disetujui' style='display: none;'>" + jumlah_disetujui + "</td>" +
				"<td id='f_jumlah_disetujui'><small><div align='right'>" + f_jumlah_disetujui + "</div></small></td>" +
				"<td id='satuan'><small>" + satuan + "</small></td>" +
				"<td id='hps' style='display: none;'>" + hps + "</td>" +
				"<td id='f_hps'><small><div align='right'>" + f_hps + "</div></small></td>" +
				"<td id='subtotal' style='display: none;'>" + subtotal + "</td>" +
				"<td id='f_subtotal'><small><div align='right'>" + f_subtotal + "</div></small></td>" +
				"<td id='medis' style='display: none;'>" + medis + "</td>" +
				"<td id='inventaris' style='display: none;'>" + inventaris + "</td>" +
				"<td>" +
					"<a href='#' onclick='drencana_pembelian_farmasi.edit(" + row_num + ")' class='input btn btn-warning'>" +
						"<i class='icon-edit icon-white'></i>" +
					"</a>" +
				"</td>" +
				"<td>" +
					"<a href='#' onclick='drencana_pembelian_farmasi.del(" + row_num + ")' class='input btn btn-danger'>" +
						"<i class='fa fa-trash-o'></i>" +
					"</a>" +
				"</td>" +
			"</tr>"
		);
		row_num++;
	} else {
		$("tr#data_" + r_num + " td#jumlah_disetujui").html(jumlah_disetujui);
		$("tr#data_" + r_num + " td#f_jumlah_disetujui").html("<small><div align='right'>" + f_jumlah_disetujui + "</div></small>");
		$("tr#data_" + r_num + " td#subtotal").html(subtotal);
		$("tr#data_" + r_num + " td#f_subtotal").html("<small><div align='right'>" + f_subtotal + "</div></small>");
	}
	this.clear();
	rencana_pembelian_farmasi.update_total();
	$("#drencana_pembelian_farmasi_nama_barang").focus();
	this.setEditMode("false");
};
AccDRencanaPembelianFarmasiAction.prototype.setEditMode = function(enable) {
	if (enable == "true") {
		$("#drencana_pembelian_farmasi_nama_barang").removeClass("smis-one-option-input");
		$("#drencana_pembelian_farmasi_nama_barang").removeAttr("disabled");
		$("#drencana_pembelian_farmasi_nama_barang").attr("disabled", "disabled");
		$("#barang_browse").hide();
		$("#drencana_pembelian_farmasi_hps").removeAttr("disabled");
		$("#drencana_pembelian_farmasi_hps").attr("disabled", "disabled");
		$("#rencana_pembelian_farmasi_form_save").hide();
		$("#rencana_pembelian_farmasi_form_update").show();
		$("#rencana_pembelian_farmasi_form_cancel").show();
	} else {
		$("#drencana_pembelian_farmasi_nama_barang").addClass("smis-one-option-input");
		$("#drencana_pembelian_farmasi_nama_barang").removeAttr("disabled");
		$("#barang_browse").show();
		$("#drencana_pembelian_farmasi_hps").removeAttr("disabled");
		$("#rencana_pembelian_farmasi_form_save").show();
		$("#rencana_pembelian_farmasi_form_update").hide();
		$("#rencana_pembelian_farmasi_form_cancel").hide();
	}
};
AccDRencanaPembelianFarmasiAction.prototype.edit = function(r_num) {
	var id = $("tr#data_" + r_num + " td#id").text();
	var id_barang = $("tr#data_" + r_num + " td#id_barang").text();
	var kode_barang = $("tr#data_" + r_num + " td#kode_barang").text();
	var nama_barang = $("tr#data_" + r_num + " td#nama_barang").text();
	var nama_jenis_barang = $("tr#data_" + r_num + " td#nama_jenis_barang").text();
	var jumlah_disetujui = parseFloat($("tr#data_" + r_num + " td#jumlah_disetujui").text());
	var satuan = $("tr#data_" + r_num + " td#satuan").text();
	var hps = "Rp. " + parseFloat($("tr#data_" + r_num + " td#hps").text()).formatMoney("2", ".", ",");
	var medis = $("tr#data_" + r_num + " td#medis").text();
	var inventaris = $("tr#data_" + r_num + " td#inventaris").text();
	this.setEditMode("true");
	$("#drencana_pembelian_farmasi_row_num").val(r_num);
	$("#drencana_pembelian_farmasi_id").val(id);
	$("#drencana_pembelian_farmasi_id_barang").val(id_barang);
	$("#drencana_pembelian_farmasi_kode_barang").val(kode_barang);
	$("#drencana_pembelian_farmasi_nama_barang").val(nama_barang);
	$("#drencana_pembelian_farmasi_name_barang").val(nama_barang);
	$("#drencana_pembelian_farmasi_nama_jenis_barang").val(nama_jenis_barang);
	$("#drencana_pembelian_farmasi_jumlah").val(jumlah_disetujui);
	$("#drencana_pembelian_farmasi_satuan").val(satuan);
	$("#drencana_pembelian_farmasi_hps").val(hps);
	$("#drencana_pembelian_farmasi_medis").val(medis);
	$("#drencana_pembelian_farmasi_inventaris").val(inventaris);
	var data = this.getRegulerData();
	data['command'] = "get_current_stock";
	data['super_command'] = "";
	data['id_obat'] = id_barang;
	data['satuan'] = satuan;
	showLoading();
	$.post(
		"",
		data,
		function(response) {
			var json = JSON.parse(response);
			if (json == null)
				$("#drencana_pembelian_farmasi_sisa").val(0);
			else
				$("#drencana_pembelian_farmasi_sisa").val(json.jumlah);
			dismissLoading();
		}
	);
};
AccDRencanaPembelianFarmasiAction.prototype.del = function(r_num) {
	var id = $("tr#data_" + r_num + " td#id").text();
	if (id.length == 0) {
		$("#data_" + r_num).remove();
	} else {
		$("#data_" + r_num).attr("style", "display: none;");
		$("#data_" + r_num).attr("class", "deleted");
		if ($("#drencana_pembelian_farmasi_row_num").val() == r_num) {
			this.clear();
			this.setEditMode("false");
		}
	}
	rencana_pembelian_farmasi.update_total();
};
AccDRencanaPembelianFarmasiAction.prototype.cancel = function() {
	this.clear();
	this.setEditMode("false");
};