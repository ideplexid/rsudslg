var rkbu;
var drkbu;
var row_id;
$(document).ready(function() {
	$('[data-toggle="popover"]').popover({
		trigger: 'hover',
		'placement': 'top'
	});
	$(".mydate").datepicker();
	var drkbu_columns = new Array("id", "id_rkbu", "bulan", "jumlah_diajukan", "satuan_diajukan", "jumlah_disetujui", "satuan_disetujui");
	drkbu = new DRKBUAction(
		"drkbu",
		"perencanaan",
		"daftar_rkbu",
		drkbu_columns
	);
	var rkbu_columns = new Array("id", "f_id", "unit", "tahun", "tanggal_diajukan", "tanggal_ditinjau", "keterangan", "locked", "disetujui");
	rkbu = new RKBUAction(
		"rkbu",
		"perencanaan",
		"daftar_rkbu",
		rkbu_columns
	);
	rkbu.view();
	$("#table_rkbu tfoot tr .input-append .btn-group").hide();
	$(".search").on("keyup", function(e) {
		if (e.which == 13) {
			rkbu.view();
		}
	});
	$(".search_select").on("change", function(e) {
		rkbu.view();
	});
});