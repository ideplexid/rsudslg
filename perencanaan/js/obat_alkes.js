var obat_alkes;
var jenis_obat_alkes;
var produsen;

$(document).ready(function() {
	$('[data-toggle="popover"]').popover({
		trigger: 'hover',
		'placement': 'top'
	});
	var obat_alkes_columns = new Array("id", "kode", "nama", "id_jenis_barang", "kode_jenis_barang", "nama_jenis_barang", "harga_pbf", "formularium", "alkes", "berlogo", "generik", "narkotika", "okt", "prekursor", "satuan", "konversi", "satuan_konversi", "produsen");
	obat_alkes = new ObatAlkesAction(
		"obat_alkes",
		"perencanaan",
		"obat_alkes",
		obat_alkes_columns
	);
	jenis_obat_alkes = new JenisObatAlkesAction(
		"jenis_obat_alkes",
		"perencanaan",
		"obat_alkes",
		new Array()
	);
	jenis_obat_alkes.setSuperCommand("jenis_obat_alkes");
	produsen = new ProdusenAction(
		"produsen",
		"perencanaan",
		"obat_alkes",
		new Array()
	);
	produsen.setSuperCommand("produsen");
	obat_alkes.view();
});