function BarangNonMedisAction(name, page, action, column) {
	this.initialize(name, page, action, column);
}
BarangNonMedisAction.prototype.constructor = BarangNonMedisAction;
BarangNonMedisAction.prototype = new TableAction();
BarangNonMedisAction.prototype.getSaveData = function() {
	var data = this.getRegulerData();
	data['command'] = "save";
	data['id'] = $("#barang_non_medis_id").val();
	data['kode'] = $("#barang_non_medis_kode").val();
	data['nama'] = $("#barang_non_medis_nama").val();
	data['id_jenis_barang'] = $("#barang_non_medis_id_jenis_barang").val();
	data['kode_jenis_barang'] = $("#barang_non_medis_kode_jenis_barang").val();
	data['nama_jenis_barang'] = $("#barang_non_medis_nama_jenis_barang").val();
	if ($("#barang_non_medis_label").val() == "non_inventaris") {
		data['medis'] = 0;
		data['inventaris'] = 0;
	} else if ($("#barang_non_medis_label").val() == "inventaris_non_medis") {
		data['medis'] = 0;
		data['inventaris'] = 1;
	} else if ($("#barang_non_medis_label").val() == "inventaris_medis") {
		data['medis'] = 1;
		data['inventaris'] = 1;
	}
	data['satuan'] = $("#barang_non_medis_satuan").val();
	data['konversi'] = $("#barang_non_medis_konversi").val();
	data['satuan_konversi'] = $("#barang_non_medis_satuan_konversi").val();
	return data;
};
BarangNonMedisAction.prototype.edit = function(id) {
	var data = this.getEditData(id);
	$.post(
		"",
		data,
		function(response) {
			var json = getContent(response);
			if (json == null) return;
			$("#barang_non_medis_id").val(json.id);
			$("#barang_non_medis_kode").val(json.kode);
			$("#barang_non_medis_nama").val(json.nama);
			var part_kode = $("#barang_non_medis_kode").val().split(".");
			$("#barang_non_medis_prefix_kode").val(part_kode[0]);
			$("#barang_non_medis_id_jenis_barang").val(json.id_jenis_barang);
			$("#barang_non_medis_kode_jenis_barang").val(json.kode_jenis_barang);
			$("#barang_non_medis_nama_jenis_barang").val(json.nama_jenis_barang);
			if (json.medis == 0 && json.inventaris == 0)
				$("#barang_non_medis_label").val("non_inventaris");
			else if (json.medis == 0 && json.inventaris == 1)
				$("#barang_non_medis_label").val("inventaris_non_medis");
			else if (json.medis == 1 && json.inventaris == 1)
				$("#barang_non_medis_label").val("inventaris_medis");
			$("#barang_non_medis_satuan").val(json.satuan);
			$("#barang_non_medis_konversi").val(json.konversi);
			$("#barang_non_medis_satuan_konversi").val(json.satuan_konversi);
			$("#barang_non_medis_add_form").smodal("show");
		}
	);
};
BarangNonMedisAction.prototype.get_last_code = function() {
	var data = this.getRegulerData();
	data['command'] = "get_last_code";
	data['super_command'] = "";
	data['prefix_kode'] = "REG";
	data['kode_jenis_barang'] = $("#barang_non_medis_kode_jenis_barang").val();
	if ($("#barang_non_medis_id").val() != "") {
		var part_kode = $("#barang_non_medis_kode").val().split(".");
		$("#barang_non_medis_kode").val(data['prefix_kode'] + "." + data['kode_jenis_barang'] + "." + part_kode[2]);
		return;
	}
	if (data['prefix_kode'] == "" || data['kode_jenis_barang'] == "")
		return;
	$.post(
		"",
		data,
		function(response) {
			var json = JSON.parse(response);
			if (json == null) {
				$("#barang_non_medis_kode").val(data['prefix_kode'] + "." + data['kode_jenis_barang'] + ".1");
				return;
			}
			$("#barang_non_medis_kode").val(data['prefix_kode'] + "." + data['kode_jenis_barang'] + "." + (Number(json.postfix) + 1));
		}
	);
};