<?php
	global $db;
	
	$table = new Table(
		array("No.", "ID-Data", "Kode", "Nama", "Jenis", "Formularium", "Alkes", "Generik", "Berlogo", "Narkotika", "OKT", "Prekursor", "Satuan", "Konversi", "Sat. Konversi", "Produsen"),
		"Perencanaan : Daftar Obat dan Alkes",
		null,
		true
	);
	$table->setName("obat_alkes");
	
	$joa_table = new Table(
		array("No.", "Kode", "Nama Jenis Obat dan Alkes"),
		"",
		null,
		true
	);
	$joa_table->setName("jenis_obat_alkes");
	$joa_table->setModel(Table::$SELECT);
	$joa_adapter = new SimpleAdapter(true, "No.");
	$joa_adapter->add("Kode", "kode");
	$joa_adapter->add("Nama Jenis Obat dan Alkes", "nama");
	$joa_dbtable = new DBTable($db, "smis_pr_jenis_barang");
	$joa_dbtable->addCustomKriteria(" medis ", " = '1' ");
	$joa_dbtable->setOrder(" id DESC ");
	$joa_dbresponder = new DBResponder(
		$joa_dbtable,
		$joa_table,
		$joa_adapter
	);

	$produsen_table = new Table(
		array("No.", "Kode", "Nama Produsen"),
		"",
		null,
		true
	);
	$produsen_table->setName("produsen");
	$produsen_table->setModel(Table::$SELECT);
	$produsen_adapter = new SimpleAdapter(true, "No.");
	$produsen_adapter->add("Kode", "kode");
	$produsen_adapter->add("Nama Produsen", "nama");
	$produsen_dbtable = new DBTable($db, "smis_pr_produsen");
	$produsen_dbtable->addCustomKriteria(" medis ", " = '1' ");
	$produsen_dbtable->setOrder(" id DESC ");
	$produsen_dbresponder = new DBResponder(
		$produsen_dbtable,
		$produsen_table,
		$produsen_adapter
	);	

	$super_command = new SuperCommand();
	$super_command->addResponder("jenis_obat_alkes", $joa_dbresponder);
	$super_command->addResponder("produsen", $produsen_dbresponder);
	$init = $super_command->initialize();
	if ($init != null) {
		echo $init;
		return;
	}
	
	if (isset($_POST['command'])) {
		$_POST['medis'] = "1";
		$adapter = new SimpleAdapter(true, "No.");
		$adapter->add("ID-Data", "id", "digit6");
		$adapter->add("Kode", "kode");
		$adapter->add("Nama", "nama");
		$adapter->add("Jenis", "nama_jenis_barang");
		$adapter->add("Formularium", "formularium", "trivial_1_&#10003_&#10005");
		$adapter->add("Generik", "generik", "trivial_1_&#10003_&#10005");
		$adapter->add("Berlogo", "berlogo", "trivial_1_&#10003_&#10005");
		$adapter->add("Narkotika", "narkotika", "trivial_1_&#10003_&#10005");
		$adapter->add("OKT", "okt", "trivial_1_&#10003_&#10005");
		$adapter->add("Prekursor", "prekursor", "trivial_1_&#10003_&#10005");
		$adapter->add("Alkes", "alkes", "trivial_1_&#10003_&#10005");
		$adapter->add("Satuan", "satuan");
		$adapter->add("Konversi", "konversi", "number");
		$adapter->add("Sat. Konversi", "satuan_konversi");
		$adapter->add("Produsen", "produsen");
		$dbtable = new DBTable($db, "smis_pr_barang");
		$dbtable->addCustomKriteria(" medis ", " = '1' ");
		$dbtable->addCustomKriteria(" inventaris ", " = '0' ");
		$dbtable->setOrder(" id DESC ");
		$dbresponder = new DBResponder(
			$dbtable,
			$table,
			$adapter
		);
		$data = $dbresponder->command($_POST['command']);
		echo json_encode($data);
		return;
	}
	
	$modal = new Modal("obat_alkes_add_form", "smis_form_container", "obat_alkes");
	$modal->setTitle("Data Obat dan Alkes");
	$id_hidden = new Hidden("obat_alkes_id", "obat_alkes_id", "");
	$modal->addElement("", $id_hidden);
	$nama_text = new Text("obat_alkes_nama", "obat_alkes_nama", "");
	$modal->addElement("Nama", $nama_text);
	$id_jenis_hidden = new Hidden("obat_alkes_id_jenis_barang", "obat_alkes_id_jenis_barang", "");
	$modal->addElement("", $id_jenis_hidden);
	$jenis_text = new Text("obat_alkes_nama_jenis_barang", "obat_alkes_nama_jenis_barang", "");
	$jenis_text->setAtribute("disabled='disabled'");
	$jenis_text->setClass("smis-one-option-input");
	$jenis_button = new Button("", "", "Pilih");
	$jenis_button->setAtribute("id='jenis_obat_alkes_btn'");
	$jenis_button->setClass("btn-info");
	$jenis_button->setIsButton(Button::$ICONIC);
	$jenis_button->setIcon("icon-white ".Button::$icon_list_alt);
	$jenis_button->setAction("jenis_obat_alkes.chooser('jenis_obat_alkes', 'jenis_obat_alkes_button', 'jenis_obat_alkes', jenis_obat_alkes)");
	$jenis_input_group = new InputGroup("");
	$jenis_input_group->addComponent($jenis_text);
	$jenis_input_group->addComponent($jenis_button);
	$modal->addElement("Jenis Obat", $jenis_input_group);
	$kode_jenis_text = new Text("obat_alkes_kode_jenis_barang", "obat_alkes_kode_jenis_barang", "");
	$kode_jenis_text->setAtribute("disabled='disabled'");
	$modal->addElement(" ", $kode_jenis_text);
	$kode_text = new Text("obat_alkes_kode", "obat_kode_alkes", "");
	$modal->addElement("Kode", $kode_text);
	$formularium_checkbox = new CheckBox("obat_alkes_formularium", "", false);
	$modal->addElement("Formularium", $formularium_checkbox);
	$alkes_checkbox = new CheckBox("obat_alkes_alkes", "", false);
	$modal->addElement("Alkes", $alkes_checkbox);
	$generik_checkbox = new CheckBox("obat_alkes_generik", "", false);
	$modal->addElement("Generik", $generik_checkbox);
	$berlogo_checkbox = new CheckBox("obat_alkes_berlogo", "", false);
	$modal->addElement("Berlogo", $berlogo_checkbox);
	$narkotika_checkbox = new CheckBox("obat_alkes_narkotika", "", false);
	$modal->addElement("Narkotika", $narkotika_checkbox);
	$okt_checkbox = new CheckBox("obat_alkes_okt", "", false);
	$modal->addElement("OKT", $okt_checkbox);
	$prekursor_checkbox = new CheckBox("obat_alkes_prekursor", "", false);
	$modal->addElement("Prekursor", $prekursor_checkbox);
	$satuan_text = new Text("obat_alkes_satuan", "obat_alkes_satuan", "");
	$modal->addElement("Satuan", $satuan_text);
	$konversi_text = new Text("obat_alkes_konversi", "obat_alkes_konversi", "1");
	$modal->addElement("Konversi", $konversi_text);
	$satuan_konversi_text = new Text("obat_alkes_satuan_konversi", "obat_alkes_satuan_konversi", "");
	$modal->addElement("Sat. Konversi", $satuan_konversi_text);
	$produsen_text = new Text("obat_alkes_produsen", "obat_alkes_produsen", "");
	$produsen_text->setAtribute("disabled='disabled'");
	$produsen_text->setClass("smis-one-option-input'");
	$produsen_button = new Button("", "", "Pilih");
	$produsen_button->setAtribute("id='produsen_obat_alkes_btn'");
	$produsen_button->setClass("btn-info");
	$produsen_button->setIsButton(Button::$ICONIC);
	$produsen_button->setIcon("icon-white ".Button::$icon_list_alt);
	$produsen_button->setAction("produsen.chooser('produsen', 'produsen_button', 'produsen', produsen)");
	$produsen_input_group = new InputGroup("");
	$produsen_input_group->addComponent($produsen_text);
	$produsen_input_group->addComponent($produsen_button);
	$modal->addElement("Produsen", $produsen_input_group);

	$save_button = new Button("", "", "Simpan");
	$save_button->setClass("btn-success");
	$save_button->setAction("obat_alkes.save()");
	$save_button->setIcon("fa fa-floppy-o");
	$save_button->setIsButton(Button::$ICONIC);
	$modal->addFooter($save_button);
	
	echo $modal->getHtml();
	echo $table->getHtml();
	echo addJS("framework/smis/js/table_action.js");
	echo addJS("perencanaan/js/jenis_obat_alkes_action.js", false);
	echo addJS("perencanaan/js/produsen_action.js", false);
	echo addJS("perencanaan/js/obat_alkes_action.js", false);
	echo addJS("perencanaan/js/obat_alkes.js", false);
?>