<?php
	require_once ("smis-framework/smis/interface/LockerInterface.php");
	require_once ("smis-framework/smis/database/DuplicateResponder.php");
	global $db;

	$table = new Table(
		array("No.", "ID-Data", "Kode", "Nama", "Jenis", "Usia Susut (Tahun)"),
		"Perencanaan : Usia Susut Inventaris",
		null,
		true
	);
	$table->setName("usia_susut_inventaris");
	$table->setAddButtonEnable(false);
	$table->setDelButtonEnable(false);
	
	if (isset($_POST['command'])) {
		$adapter = new SimpleAdapter(true, "No.");
		$adapter->add("ID-Data", "id", "digit6");
		$adapter->add("Kode", "kode");
		$adapter->add("Nama", "nama");
		$adapter->add("Jenis", "nama_jenis_barang");
		$adapter->add("Usia Susut (Tahun)", "usia_susut", "number");
		$dbtable = new DBTable($db, "smis_pr_barang");
		$dbtable->setCustomKriteria(" inventaris ", " = 1 ");
		$dbtable->setOrder(" id DESC ");
		$dbresponder = new DuplicateResponder(
			$dbtable,
			$table,
			$adapter
		);
		$data = $dbresponder->command($_POST['command']);
		echo json_encode($data);
		return;
	}
	
	$modal = new Modal("usia_susut_inventaris_add_form", "smis_form_container", "usia_susut_inventaris");
	$modal->setTitle("Data Barang dan inventaris");
	$id_hidden = new Hidden("usia_susut_inventaris_id", "usia_susut_inventaris_id", "");
	$modal->addElement("", $id_hidden);
	$nama_text = new Text("usia_susut_inventaris_nama", "usia_susut_inventaris_nama", "");
	$nama_text->setAtribute("disabled='disabled'");
	$modal->addElement("Nama", $nama_text);
	$id_jenis_hidden = new Hidden("usia_susut_inventaris_id_jenis_barang", "usia_susut_inventaris_id_jenis_barang", "");
	$modal->addElement("", $id_jenis_hidden);
	$jenis_text = new Text("usia_susut_inventaris_nama_jenis_barang", "usia_susut_inventaris_nama_jenis_barang", "");
	$jenis_text->setAtribute("disabled='disabled'");
	$modal->addElement("Jenis Obat", $jenis_text);
	$usia_susut_text = new Text("usia_susut_inventaris_usia_susut", "usia_susut_inventaris_usia_susut", "");
	$modal->addElement("Usia Susut", $usia_susut_text);
	
	$save_button = new Button("", "", "Simpan");
	$save_button->setClass("btn-success");
	$save_button->setAction("usia_susut_inventaris.save()");
	$save_button->setIcon("fa fa-floppy-o");
	$save_button->setIsButton(Button::$ICONIC);
	$modal->addFooter($save_button);
	
	echo $modal->getHtml();
	echo $table->getHtml();
	echo addJS("framework/smis/js/table_action.js");
	echo addJS("perencanaan/js/usia_susut_inventaris.js", false);
?>