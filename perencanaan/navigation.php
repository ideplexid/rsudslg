<?php 
	global $NAVIGATOR;	
	$perencanaan_menu = new Menu("fa fa-cubes");
	$perencanaan_menu->addProperty('title', "Perencanaan");
	$perencanaan_menu->addProperty('name', "Perencanaan");
	$perencanaan_menu->addSubMenu("Jenis Obat dan Alkes", "perencanaan", "jenis_obat_alkes", "Data Jenis Obat dan Alat Kesehatan","fa fa-list");
	$perencanaan_menu->addSubMenu("Jenis Barang Non-Medis", "perencanaan", "jenis_barang_non_medis", "Data Jenis Barang Non-Medis","fa fa-list");
	$perencanaan_menu->addSubMenu("Daftar Obat dan Alkes", "perencanaan", "obat_alkes", "Data Obat dan Alkes","fa fa-list-ul");
	$perencanaan_menu->addSubMenu("Daftar Produsen Obat dan Alkes", "perencanaan", "produsen", "Data Produsen Obat dan Alkes","fa fa-group");
	$perencanaan_menu->addSubMenu("Mapping Kode Acc. Obat dan Alkes", "perencanaan", "mapping_acc_obat_alkes", "Data Mapping Kode Acc. Obat dan Alkes","fa fa-random");
	$perencanaan_menu->addSubMenu("Daftar Barang Non Medis", "perencanaan", "barang_non_medis", "Data Barang Non Medis","fa fa-list-ul");
	$perencanaan_menu->addSubMenu("Mapping Kode Acc. Barang dan Inventaris", "perencanaan", "mapping_acc_barang_inventaris", "Data Mapping Kode Acc. Barang dan Inventaris","fa fa-random");
	$perencanaan_menu->addSubMenu("Usia Susut Inventaris", "perencanaan", "usia_susut_inventaris", "Data Usia Susut Inventaris","fa fa-sort-amount-asc");
	$perencanaan_menu->addSeparator();
	$perencanaan_menu->addSubMenu("Draft Rencana Pembelian Farmasi", "perencanaan", "rencana_pembelian_farmasi", "Draft Rencana Pembelian Farmasi","fa fa-calendar-plus-o");
	$perencanaan_menu->addSubMenu("Acc. Rencana Pembelian Farmasi", "perencanaan", "acc_rencana_pembelian_farmasi", "Persetujuan Rencana Pembelian Farmasi","fa fa-calendar-check-o");
	$perencanaan_menu->addSubMenu("Analisis Pengendalian Stok Obat (ABC)", "perencanaan", "rp_abc", "Analisis Pengendalian Stok Obat (ABC)","fa fa-bar-chart");
	$perencanaan_menu->addSubMenu("Sim. Ren. Pemesanan Obat (MMSL)", "perencanaan", "rp_mmsl", "Simulasi Rencana Pemesanan Obat (MMSL)","fa fa-bar-chart");
	$NAVIGATOR->addMenu($perencanaan_menu, "perencanaan");
?>