<?php
	class RKBUAdapter extends ArrayAdapter {
		public function adapt($row) {
			$array = array();
			$array['id'] = $row->id;
			$array['disetujui'] = $row->disetujui;
			$array['Unit'] = self::format("unslug", $row->unit);
			$array['Nomor'] = self::format("digit8", $row->f_id);
			$array['Tahun RKBU'] = $row->tahun;
			$array['Bulan'] = $row->daftar_bulan;
			$array['Tanggal Diajukan'] = self::format("date d-m-Y", $row->tanggal_diajukan);
			if ($row->tanggal_ditinjau != null && self::format("date d-m-Y", $row->tanggal_ditinjau) != "00-00-0000")
				$array['Tanggal Ditinjau'] = self::format("date d-m-Y", $row->tanggal_ditinjau);
			else
				$array['Tanggal Ditinjau'] = "-";
			if ($row->disetujui == "sudah")
				$array['Status'] = "Disetujui";
			else if ($row->disetujui == "belum")
				$array['Status'] = "Belum Ditinjau";
			else if ($row->disetujui == "revisi")
				$array['Status'] = "Revisi Belum Ditinjau";
			return $array;
		}
	}
?>