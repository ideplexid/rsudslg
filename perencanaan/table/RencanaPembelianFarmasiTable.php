<?php
	class RencanaPembelianFarmasiTable extends Table {
		public function getBodyContent() {
			$content = "";
			if ($this->content!=NULL) {
				foreach ($this->content as $d) {
					$content .= "<tr>";
					foreach ($this->header as $h) {
						$content .= "<td>" . $d[$h] . "</td>";
					}
					if ($this->is_action) {
						$content .= "<td>" . $this->getFilteredContentButton($d['id'], $d['lock_plan'], $d['lock_acc'])->getHtml() . "</td>";
					}
					$content .= "</tr>";
				}
			}
			return $content;
		}
		public function getFilteredContentButton($id, $lock_plan, $lock_acc) {
			$btn_group = new ButtonGroup("noprint");
			$btn_group->setMax(4, "");
			if ($lock_plan == false && $lock_acc == false) {
				$btn = new Button("", "", "Ubah");
				$btn->setAction($this->action . ".edit('" . $id . "')");
				$btn->setClass("btn-warning");
				$btn->setAtribute("data-content='Ubah' data-toggle='popover'");
				$btn->setIcon("icon-edit icon-white");
				$btn->setIsButton(Button::$ICONIC);
				$btn_group->addElement($btn);
				$btn = new Button("", "", "Hapus");
				$btn->setAction($this->action . ".del('" . $id . "')");
				$btn->setClass("btn-danger");
				$btn->setAtribute("data-content='Hapus' data-toggle='popover'");
				$btn->setIcon("icon-remove icon-white");
				$btn->setIsButton(Button::$ICONIC);
				$btn_group->addElement($btn);
				$btn = new Button("", "", "Kunci");
				$btn->setAction($this->action . ".lock_plan('" . $id . "')");
				$btn->setClass("btn-success");
				$btn->setAtribute("data-content='Ajukan' data-toggle='popover'");
				$btn->setIcon("fa fa-lock");
				$btn->setIsButton(Button::$ICONIC);
				$btn_group->addElement($btn);
			} else {
				$btn = new Button("", "", "View");
				$btn->setAction($this->action . ".detail('" . $id . "')");
				$btn->setClass("btn-success");
				$btn->setAtribute("data-content='View' data-toggle='popover'");
				$btn->setIcon("icon-eye-open icon-white");
				$btn->setIsButton(Button::$ICONIC);
				$btn_group->addElement($btn);
			}
			$btn = new Button("", "", "Eksport XLS");
			$btn->setAction($this->action . ".export_xls('" . $id . "')");
			$btn->setClass("btn-inverse");
			$btn->setAtribute("data-content='Cetak Perencanaan' data-toggle='popover'");
			$btn->setIcon("fa fa-download");
			$btn->setIsButton(Button::$ICONIC);
			$btn_group->addElement($btn);
			return $btn_group;
		}
	}
?>