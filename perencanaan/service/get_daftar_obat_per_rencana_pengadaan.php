<?php 
	if (isset($_POST['command'])) {
		$drpengadaan_dbtable = new DBTable($db, "smis_pr_drencana_pengadaan");
		if ($_POST['command'] == "list") {
			$drpengadaan_dbtable->setViewForSelect(true);
			$filter = "";
			if (isset($_POST['kriteria'])) {
				$filter = " AND (smis_pr_drencana_pengadaan.nama_barang LIKE '%" . $_POST['kriteria'] . "%' OR smis_pr_drencana_pengadaan.bulan LIKE '%" . $_POST['kriteria'] . "%' OR smis_pr_drencana_pengadaan.nama_jenis_barang LIKE '%" . $_POST['kriteria'] . "%')";
			}
			$query_value = "
				SELECT smis_pr_drencana_pengadaan.*
				FROM smis_pr_drencana_pengadaan LEFT JOIN smis_pr_barang ON smis_pr_drencana_pengadaan.id_barang = smis_pr_barang.id
				WHERE smis_pr_drencana_pengadaan.prop NOT LIKE 'del' AND smis_pr_drencana_pengadaan.medis = '1' AND smis_pr_drencana_pengadaan.inventaris = '0' AND smis_pr_drencana_pengadaan.id_rencana_pengadaan = '" . $_POST['id_rencana_pengadaan'] . "' " . $filter . "
			";
			$query_count = "
				SELECT COUNT(*)
				FROM smis_pr_drencana_pengadaan LEFT JOIN smis_pr_barang ON smis_pr_drencana_pengadaan.id_barang = smis_pr_barang.id
				WHERE smis_pr_drencana_pengadaan.prop NOT LIKE 'del' AND smis_pr_drencana_pengadaan.medis = '1' AND smis_pr_drencana_pengadaan.inventaris = '0' AND smis_pr_drencana_pengadaan.id_rencana_pengadaan = '" . $_POST['id_rencana_pengadaan'] . "' " . $filter . "
			";
			$drpengadaan_dbtable->setPreferredQuery(true, $query_value, $query_count);
		}
		$drpengadaan_dbtable->setDebuggable(true);
		class GDOPRPServiceProvider extends ServiceProvider {
			public function edit() {
				$id=$_POST['id'];
				$row=$this->dbtable->get_row("
					SELECT smis_pr_drencana_pengadaan.*
					FROM smis_pr_drencana_pengadaan LEFT JOIN smis_pr_barang ON smis_pr_drencana_pengadaan.id_barang = smis_pr_barang.id
					WHERE smis_pr_drencana_pengadaan.prop NOT LIKE 'del' AND smis_pr_drencana_pengadaan.medis = '1' AND smis_pr_drencana_pengadaan.inventaris = '0' AND smis_pr_drencana_pengadaan.id = '" . $id . "'
				");
				return $row;
			}
		}
		$service_provider = new GDOPRPServiceProvider($drpengadaan_dbtable);
		$data = $service_provider->command($_POST['command']);
		echo json_encode($data);
	}
?>