<?php
	global $db;
	
	$dbtable = new DBTable($db, "smis_pr_barang");
	$filter = "";
	if (isset($_POST['filter_id_barang']))
		$filter .= " AND id LIKE '" . $_POST['filter_id_barang'] . "' ";
	if (isset($_POST['filter_kode_jenis_barang'])) {
		if ($_POST['filter_kode_jenis_barang'] != "%%")
			$filter .= " AND kode_jenis_barang LIKE '" . $_POST['filter_kode_jenis_barang'] . "' ";
		else
			$filter .= " AND (kode_jenis_barang LIKE '" . $_POST['filter_kode_jenis_barang'] . "' OR kode_jenis_barang IS NULL) ";
	}
	$row = $dbtable->get_row("
		SELECT COUNT(*) AS 'jumlah'
		FROM smis_pr_barang
		WHERE prop NOT LIKE 'del' AND medis = '0' AND inventaris = '0' " . $filter . "
	");
	$data['jumlah'] = $row->jumlah;
	echo json_encode($data);
?>