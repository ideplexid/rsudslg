<?php
	global $db;

	$dbtable = new DBTable($db, "smis_pr_barang");
	$alkes_formularium_csv = "";
	$alkes_non_formularium_csv = "";
	$paten_formularium_csv = "";
	$paten_non_formularium_csv = "";
	$generik_formularium_csv = "";
	$generik_non_formularium_csv = "";

	$row = $dbtable->get_row("
		SELECT GROUP_CONCAT(id) AS 'csv'
		FROM smis_pr_barang
		WHERE medis = 1 AND inventaris = 0 AND alkes = 1 AND berlogo = 0 AND generik = 0 AND formularium = 0
	");
	if ($row != null)
		$alkes_non_formularium_csv = $row->csv;

	$row = $dbtable->get_row("
		SELECT GROUP_CONCAT(id) AS 'csv'
		FROM smis_pr_barang
		WHERE medis = 1 AND inventaris = 0 AND alkes = 1 AND berlogo = 0 AND generik = 0 AND formularium = 1
	");
	if ($row != null)
		$alkes_formularium_csv = $row->csv;	

	$row = $dbtable->get_row("
		SELECT GROUP_CONCAT(id) AS 'csv'
		FROM smis_pr_barang
		WHERE medis = 1 AND inventaris = 0 AND alkes = 0 AND berlogo = 1 AND generik = 0 AND formularium = 0
	");
	if ($row != null)
		$paten_non_formularium_csv = $row->csv;

	$row = $dbtable->get_row("
		SELECT GROUP_CONCAT(id) AS 'csv'
		FROM smis_pr_barang
		WHERE medis = 1 AND inventaris = 0 AND alkes = 0 AND berlogo = 1 AND generik = 0 AND formularium = 1
	");
	if ($row != null)
		$paten_formularium_csv = $row->csv;

	$row = $dbtable->get_row("
		SELECT GROUP_CONCAT(id) AS 'csv'
		FROM smis_pr_barang
		WHERE medis = 1 AND inventaris = 0 AND alkes = 0 AND berlogo = 0 AND generik = 1 AND formularium = 0
	");
	if ($row != null)
		$generik_non_formularium_csv = $row->csv;

	$row = $dbtable->get_row("
		SELECT GROUP_CONCAT(id) AS 'csv'
		FROM smis_pr_barang
		WHERE medis = 1 AND inventaris = 0 AND alkes = 0 AND berlogo = 0 AND generik = 1 AND formularium = 1
	");
	if ($row != null)
		$generik_formularium_csv = $row->csv;

	$data = array(
		"0"	=> array(
			"alkes_formularium_csv"			=> $alkes_formularium_csv,
			"alkes_non_formularium_csv"		=> $alkes_non_formularium_csv,
			"paten_formularium_csv"			=> $paten_formularium_csv,
			"paten_non_formularium_csv"		=> $paten_non_formularium_csv,
			"generik_formularium_csv"		=> $generik_formularium_csv,
			"generik_non_formularium_csv"	=> $generik_non_formularium_csv
		)
	);
	echo json_encode($data);
?>