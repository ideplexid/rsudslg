<?php
	global $db;

	if (isset($_POST['id'])) {
		$id = $_POST['id'];
		$dbtable = new DBTable($db, "smis_pr_barang");
		$row = $dbtable->get_row("
			SELECT *
			FROM smis_pr_barang
			WHERE id = '" . $id . "'
		");
		$kode_pendapatan = "";
		$kode_hpp = "";
		$kode_nilai_persediaan = "";
		$kode_diskon_penjualan = "";
		$kode_piutang_pendapatan = "";
		$kode_ppn_keluar = "";
		if ($row != null) {
			$kode_pendapatan = $row->acc_kode_pendapatan;
			$kode_hpp = $row->acc_kode_hpp;
			$kode_nilai_persediaan = $row->acc_kode_nilai_persediaan;
			$kode_diskon_penjualan = $row->acc_kode_diskon_penjualan;
			$kode_piutang = $row->acc_kode_piutang;
			$kode_ppn_keluar = $row->acc_kode_ppn_keluar;
		}
		$data = array(
			"kode_pendapatan" 		=> $kode_pendapatan,
			"kode_hpp"				=> $kode_hpp,
			"kode_nilai_persediaan"	=> $kode_nilai_persediaan,
			"kode_diskon_penjualan"	=> $kode_diskon_penjualan,
			"kode_piutang"			=> $kode_piutang,
			"kode_ppn_keluar"		=> $kode_ppn_keluar
		);
		echo json_encode($data);
	}
?>