<?php 
	if (isset($_POST['command'])) {
		$jenis_barang_dbtable = new DBTable($db, "smis_pr_jenis_barang");
		if ($_POST['command'] == "list") {
			$jenis_barang_dbtable->setViewForSelect(true);
			$filter = "";
			if (isset($_POST['kriteria'])) {
				$filter = " AND (nama LIKE '%" . $_POST['kriteria'] . "%')";
			}
			$query_value = "
				SELECT *
				FROM smis_pr_jenis_barang
				WHERE prop NOT LIKE 'del' AND id IN (1, 2, 3, 4, 5, 6, 7) " . $filter . "
			";
			$query_count = "
				SELECT COUNT(*)
				FROM smis_pr_jenis_barang
				WHERE prop NOT LIKE 'del' AND id IN (1, 2, 3, 4, 5, 6, 7) " . $filter . "
			";
			$jenis_barang_dbtable->setPreferredQuery(true, $query_value, $query_count);
		}
		$get_jenis_barang_m_service_provider = new ServiceProvider($jenis_barang_dbtable);
		$data = $get_jenis_barang_m_service_provider->command($_POST['command']);
		echo json_encode($data);
	}
?>