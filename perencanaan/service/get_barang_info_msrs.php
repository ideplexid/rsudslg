<?php
	global $db;
	
	if (isset($_POST['num'])) {
		$num = $_POST['num'];
		$dbtable = new DBTable($db, "smis_pr_barang");
		$filter = "";
		if (isset($_POST['filter_id_obat']))
			$filter .= " AND id LIKE '" . $_POST['filter_id_barang'] . "' ";
		if (isset($_POST['filter_kode_jenis_barang'])) {
			if ($_POST['filter_kode_jenis_barang'] != "%%")
				$filter .= " AND kode_jenis_barang LIKE '" . $_POST['filter_kode_jenis_barang'] . "' ";
			else
				$filter .= " AND (kode_jenis_barang LIKE '" . $_POST['filter_kode_jenis_barang'] . "' OR kode_jenis_barang IS NULL) ";
		}
		$order = "";
		if (isset($_POST['order_by'])) {
			if ($_POST['order_by'] == "KODE BARANG")
				$order = "ORDER BY kode, nama ASC";
			else if ($_POST['order_by'] == "NAMA BARANG")
				$order = "ORDER BY nama, kode ASC";
		}
		$row = $dbtable->get_row("
			SELECT *
			FROM smis_pr_barang
			WHERE prop NOT LIKE 'del' AND medis = '0' AND inventaris = '0' " . $filter . "
			" . $order . "
			LIMIT " . $num . ", 1
		");
		$data = array();
		$data['id_obat'] = $row->id;
		$data['kode_obat'] = $row->kode;
		$data['nama_obat'] = $row->nama;
		$data['nama_jenis_obat'] = $row->nama_jenis_barang;
		$data['satuan'] = $row->satuan;
		$data['satuan_konversi'] = $row->satuan_konversi;
		echo json_encode($data);
	}
?>