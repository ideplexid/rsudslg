<?php 
	if (isset($_POST['command'])) {
		$barang_dbtable = new DBTable($db, "smis_pr_barang");
		if ($_POST['command'] == "list") {
			$barang_dbtable->setViewForSelect(true);
			$filter = "";
			if (isset($_POST['kriteria'])) {
				$filter = " AND (nama LIKE '%" . $_POST['kriteria'] . "%' OR nama_jenis_barang LIKE '%" . $_POST['kriteria'] . "%')";
			}
			$query_value = "
				SELECT *
				FROM smis_pr_barang
				WHERE prop NOT LIKE 'del' AND medis = '0' " . $filter . "
			";
			$query_count = "
				SELECT COUNT(*)
				FROM smis_pr_barang
				WHERE prop NOT LIKE 'del' AND medis = '0' " . $filter . "
			";
			$barang_dbtable->setPreferredQuery(true, $query_value, $query_count);
		}
		$get_barang_nm_service_provider = new ServiceProvider($barang_dbtable);
		$data = $get_barang_nm_service_provider->command($_POST['command']);
		echo json_encode($data);
	}
?>