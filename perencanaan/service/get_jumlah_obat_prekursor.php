<?php
	global $db;
	
	$dbtable = new DBTable($db, "smis_pr_barang");
	$row = $dbtable->get_row("
		SELECT COUNT(*) AS 'jumlah'
		FROM smis_pr_barang
		WHERE prop NOT LIKE 'del' AND medis = '1' AND inventaris = '0' AND prekursor = '1'
	");
	$data['jumlah'] = $row->jumlah;
	echo json_encode($data);
?>