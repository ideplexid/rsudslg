<?php 
	$response_package = new ResponsePackage();
	if (isset($_POST['command'])) {
		if ($_POST['command'] == "push_save") {
			$rkbu_dbtable = new DBTable($db, "smis_pr_rkbu");
			$rkbu_row = $rkbu_dbtable->get_row("
				SELECT id
				FROM smis_pr_rkbu
				WHERE f_id = '" . $_POST['id_rkbu'] . "' AND unit = '" . $_POST['unit'] . "'
			");
			$id['id'] = 0;
			if ($rkbu_row == null) {
				//do insert:
				$rkbu_data = array();
				$rkbu_data['f_id'] = $_POST['id_rkbu'];
				$rkbu_data['tahun'] = $_POST['tahun'];
				$rkbu_data['tanggal_diajukan'] = $_POST['tanggal_diajukan'];
				$rkbu_data['keterangan'] = $_POST['keterangan'];
				$rkbu_data['disetujui'] = $_POST['disetujui'];
				$rkbu_data['locked'] = $_POST['locked'];
				$rkbu_data['unit'] = $_POST['unit'];
				$rkbu_dbtable->insert($rkbu_data);
				$rkbu_id = $rkbu_dbtable->get_inserted_id();
				$rkbu_detail = json_decode($_POST['detail'], true);
				$drkbu_dbtable = new DBTable($db, "smis_pr_drkbu");
				foreach($rkbu_detail as $drkbu) {
					$drkbu_data = array();
					$drkbu_data['f_id'] = $drkbu['id'];
					$drkbu_data['id_rkbu'] = $rkbu_id;
					$drkbu_data['bulan'] = $drkbu['bulan'];
					$drkbu_data['nama_barang'] = $drkbu['nama_barang'];
					$drkbu_data['jenis_barang'] = $drkbu['jenis_barang'];
					$drkbu_data['jumlah_diajukan'] = $drkbu['jumlah_diajukan'];
					$drkbu_data['satuan_diajukan'] = $drkbu['satuan_diajukan'];
					$drkbu_data['jumlah_disetujui'] = $drkbu['jumlah_disetujui'];
					$drkbu_data['satuan_disetujui'] = $drkbu['satuan_disetujui'];
					$drkbu_dbtable->insert($drkbu_data);
				}
				//notify:
				global $notification;
				$key=md5($_POST['unit']." ".$rkbu_id);
				$msg="RKBU dari <strong>".ArrayAdapter::format("unslug", $_POST['unit'])."</strong> masuk";
				$notification->addNotification("RKBU Masuk", $key, $msg, "perencanaan","rkbu");
				$notification->commit();
			} else {
				//do update:
				$rkbu_id_data['id'] = $rkbu_row->id;
				$rkbu_data = array();
				$rkbu_data['tahun'] = $_POST['tahun'];
				$rkbu_data['tanggal_diajukan'] = $_POST['tanggal_diajukan'];
				$rkbu_data['keterangan'] = $_POST['keterangan'];
				$rkbu_data['disetujui'] = $_POST['disetujui'];
				$rkbu_data['locked'] = $_POST['locked'];
				$rkbu_data['unit'] = $_POST['unit'];
				$rkbu_dbtable->update($rkbu_data, $rkbu_id_data);
				$rkbu_detail = json_decode($_POST['detail'], true);
				$drkbu_dbtable = new DBTable($db, "smis_pr_drkbu");
				foreach($rkbu_detail as $drkbu) {
					$drkbu_row = $drkbu_dbtable->get_row("
						SELECT id
						FROM smis_pr_drkbu
						WHERE f_id = '" . $drkbu['id'] . "' AND id_rkbu = '" . $rkbu_row->id . "'
					");
					if ($drkbu_row == null) {
						//do insert if f_id not exist:
						$drkbu_data = array();
						$drkbu_data['f_id'] = $drkbu['id'];
						$drkbu_data['id_rkbu'] = $rkbu_row->id;
						$drkbu_data['bulan'] = $drkbu['bulan'];
						$drkbu_data['nama_barang'] = $drkbu['nama_barang'];
						$drkbu_data['jenis_barang'] = $drkbu['jenis_barang'];
						$drkbu_data['jumlah_diajukan'] = $drkbu['jumlah_diajukan'];
						$drkbu_data['satuan_diajukan'] = $drkbu['satuan_diajukan'];
						$drkbu_data['jumlah_disetujui'] = $drkbu['jumlah_disetujui'];
						$drkbu_data['satuan_disetujui'] = $drkbu['satuan_disetujui'];
						$drkbu_dbtable->insert($drkbu_data);
					} else {
						//do update or delete if f_id exist:
						$drkbu_id_data['id'] = $drkbu_row->id;
						$drkbu_data = array();
						$drkbu_data['bulan'] = $drkbu['bulan'];
						$drkbu_data['nama_barang'] = $drkbu['nama_barang'];
						$drkbu_data['jenis_barang'] = $drkbu['jenis_barang'];
						$drkbu_data['jumlah_diajukan'] = $drkbu['jumlah_diajukan'];
						$drkbu_data['satuan_diajukan'] = $drkbu['satuan_diajukan'];
						$drkbu_data['prop'] = $drkbu['prop'];
						$drkbu_dbtable->update($drkbu_data, $drkbu_id_data);
					}
				}
				//notify:
				global $notification;
				$key=md5($_POST['unit']." ".$rkbu_row->id);
				$msg="RKBU dari <strong>".ArrayAdapter::format("unslug", $_POST['unit'])."</strong> diperbarui";
				$notification->addNotification("RKBU Diperbarui", $key, $msg, "perencanaan","rkbu");
				$notification->commit();
			}
			$response_package->setStatus(ResponsePackage::$STATUS_OK);
		} else if ($_POST['command'] == "push_delete") {
			$rkbu_dbtable = new DBTable($db, "smis_pr_rkbu");
			$rkbu_row = $rkbu_dbtable->get_row("
				SELECT id
				FROM smis_pr_rkbu
				WHERE f_id = '" . $_POST['id_rkbu'] . "' AND unit = '" . $_POST['unit'] . "'
			");
			if ($rkbu_row != null) {
				$rkbu_id_data['id'] = $rkbu_row->id;
				$rkbu_data['prop'] = "del";
				$rkbu_dbtable->update($rkbu_data, $rkbu_id_data);
				//notify:
				global $notification;
				$key=md5($_POST['unit']." ".$rkbu_row->id);
				$msg="RKBU dari <strong>".ArrayAdapter::format("unslug", $_POST['unit'])."</strong> dibatalkan";
				$notification->addNotification("RKBU Batal", $key, $msg, "perencanaan","rkbu");
				$notification->commit();
			}
			$response_package->setStatus(ResponsePackage::$STATUS_OK);
		} else {
			$response_package->setStatus(ResponsePackage::$STATUS_FAIL);
		}
	} else {
		$response_package->setStatus(ResponsePackage::$STATUS_FAIL);
	}
	echo json_encode($response_package->getPackage());
?>
