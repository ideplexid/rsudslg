<?php
	global $db;
	
	if (isset($_POST['num'])) {
		$num = $_POST['num'];
		$dbtable = new DBTable($db, "smis_pr_barang");
		$order = "";
		if (isset($_POST['order_by'])) {
			if ($_POST['order_by'] == "KODE OBAT")
				$order = "ORDER BY kode, nama ASC";
			else if ($_POST['order_by'] == "NAMA OBAT")
				$order = "ORDER BY nama, kode ASC";
		}
		$row = $dbtable->get_row("
			SELECT *
			FROM smis_pr_barang
			WHERE prop NOT LIKE 'del' AND medis = '1' AND inventaris = '0' AND prekursor = '1'
			" . $order . "
			LIMIT " . $num . ", 1
		");
		$data = array();
		$data['id_obat'] = $row->id;
		$data['kode_obat'] = $row->kode;
		$data['nama_obat'] = $row->nama;
		$data['nama_jenis_obat'] = $row->nama_jenis_barang;
		$data['satuan'] = $row->satuan;
		$data['satuan_konversi'] = $row->satuan_konversi;
		echo json_encode($data);
	}
?>