<?php
	global $db;

	$dbtable = new DBTable($db, "smis_pr_barang");
	$okt_csv = "";
	$narkotika_csv = "";
	$prekursor_farmasi_csv = "";

	$row = $dbtable->get_row("
		SELECT GROUP_CONCAT(id) AS 'csv'
		FROM smis_pr_barang
		WHERE okt = 1
	");
	if ($row != null)
		$okt_csv = $row->csv;

	$row = $dbtable->get_row("
		SELECT GROUP_CONCAT(id) AS 'csv'
		FROM smis_pr_barang
		WHERE narkotika = 1
	");
	if ($row != null)
		$narkotika_csv = $row->csv;	

	$row = $dbtable->get_row("
		SELECT GROUP_CONCAT(id) AS 'csv'
		FROM smis_pr_barang
		WHERE prekursor = 1
	");
	if ($row != null)
		$prekursor_farmasi_csv = $row->csv;

	$data = array(
		"0"	=> array(
			"okt_csv"				=> $okt_csv,
			"narkotika_csv"			=> $narkotika_csv,
			"prekursor_farmasi_csv"	=> $prekursor_farmasi_csv
		)
	);
	echo json_encode($data);
?>