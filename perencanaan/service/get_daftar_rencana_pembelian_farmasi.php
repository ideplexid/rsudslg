<?php 
	if (isset($_POST['command'])) {
		$rencana_pembelian_dbtable = new DBTable($db, "smis_pr_rencana_pembelian");
		$rencana_pembelian_dbtable->addCustomKriteria(" medis ", " = '1' ");
		$rencana_pembelian_dbtable->addCustomKriteria(" lock_acc ", " = '1' ");
		$rencana_pembelian_dbtable->setOrder(" id DESC ");
		$service_provider = new ServiceProvider($rencana_pembelian_dbtable);
		$data = $service_provider->command($_POST['command']);
		$service_provider->setDebuggable(true);
		echo json_encode($data);
	}
?>