<?php
	global $db;
	
	$table = new Table(
		array("No.", "ID-Data", "Kode", "Nama"),
		"Perencanaan : Jenis Obat dan Alkes",
		null,
		true
	);
	$table->setName("jenis_obat_alkes");
	
	if (isset($_POST['command'])) {
		$adapter = new SimpleAdapter(true, "No.");
		$adapter->add("ID-Data", "id", "digit6");
		$adapter->add("Kode", "kode");
		$adapter->add("Nama", "nama");
		$dbtable = new DBTable($db, "smis_pr_jenis_barang");
		$dbtable->addCustomKriteria(" medis ", " = '1' ");
		$dbtable->setOrder(" id DESC ");
		$dbresponder = new DBResponder(
			$dbtable,
			$table,
			$adapter
		);
		$data = $dbresponder->command($_POST['command']);
		echo json_encode($data);
		return;
	}
	
	$modal = new Modal("jenis_obat_alkes_add_form", "smis_form_container", "jenis_obat_alkes");
	$modal->setTitle("Data Jenis Obat dan Alkes");
	$id_hidden = new Hidden("jenis_obat_alkes_id", "jenis_obat_alkes_id", "");
	$modal->addElement("", $id_hidden);
	$kode_text = new Text("jenis_obat_alkes_kode", "jenis_obat_alkes_kode", "");
	$modal->addElement("Kode", $kode_text);
	$nama_text = new Text("jenis_obat_alkes_nama", "jenis_obat_alkes_nama", "");
	$modal->addElement("Nama", $nama_text);
	$save_button = new Button("", "", "Simpan");
	$save_button->setClass("btn-success");
	$save_button->setAction("jenis_obat_alkes.save()");
	$save_button->setIcon("fa fa-floppy-o");
	$save_button->setIsButton(Button::$ICONIC);
	$modal->addFooter($save_button);
	
	echo $modal->getHtml();
	echo $table->getHtml();
	echo addJS("framework/smis/js/table_action.js");
	echo addJS("perencanaan/js/jenis_obat_alkes_action.js", false);
	echo addJS("perencanaan/js/jenis_obat_alkes.js", false);
?>