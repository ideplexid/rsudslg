<?php
	class SetRKBUStatusServiceConsumer extends ServiceConsumer {
		public function __construct($db, $f_id, $tanggal_ditinjau, $tinjauan, $disetujui, $detail, $command, $tujuan) {
			$array['id'] = $f_id;
			$array['tanggal_ditinjau'] = $tanggal_ditinjau;
			$array['tinjauan'] = $tinjauan;
			$array['disetujui'] = $disetujui;
			$array['detail'] = json_encode($detail);
			$array['command'] = $command;
			parent::__construct($db, "set_rkbu_status", $array, $tujuan);
		}
		public function proceedResult() {
			$content = array();
			$result = json_decode($this->result,true);
			foreach ($result as $autonomous) {
				foreach ($autonomous as $entity) {
					if ($entity != null || $entity != "")
						return $entity;
				}
			}
			return $content;
		}
	}
?>