<?php
	require_once 'smis-base/smis-include-service-consumer.php';
	global $db;
	
	$form = new Form("mmsl_form", "", "Perencanaan : Simulasi Rencana Pemesanan Obat (MMSL)");
	$bulan_option = new OptionBuilder();
	setlocale(LC_ALL, 'IND');
	for( $i = 1; $i <= 12; $i++ ) {
		if ($i == date('m'))
			$bulan_option->add(strftime('%B', mktime(0, 0, 0, $i, 1 )), $i, "1");
		else
			$bulan_option->add(strftime('%B', mktime(0, 0, 0, $i, 1 )), $i);
	}
	$bulan_select = new Select("mmsl_bulan", "mmsl_bulan", $bulan_option->getContent());
	$form->addElement("Bulan", $bulan_select);
	$tahun_option = new OptionBuilder();
	$tahun_option->add(date("Y") - 1, date("Y") - 1);
	$tahun_option->add(date("Y"), date("Y"), "1");
	$tahun_select = new Select("mmsl_tahun", "mmsl_tahun", $tahun_option->getContent());
	$form->addElement("Tahun", $tahun_select);
	$bobot_1_text = new Text("mmsl_bobot_1", "mmsl_bobot_1", "0.10");
	$bobot_1_text->setTypical("money");
	$form->addElement("Bobot I", $bobot_1_text);
	$bobot_2_text = new Text("mmsl_bobot_2", "mmsl_bobot_2", "0.20");
	$bobot_2_text->setTypical("money");
	$form->addElement("Bobot II", $bobot_2_text);
	$bobot_3_text = new Text("mmsl_bobot_3", "mmsl_bobot_3", "0.30");
	$bobot_3_text->setTypical("money");
	$form->addElement("Bobot III", $bobot_3_text);
	$bobot_4_text = new Text("mmsl_bobot_4", "mmsl_bobot_4", "0.40");
	$bobot_4_text->setTypical("money");
	$form->addElement("Bobot IV", $bobot_4_text);
	$smax_hari_text = new Text("mmsl_smax_hari", "mmsl_smax_hari", "14");
	$smax_hari_text->setTypical("money");
	$form->addElement("S-Max - Hari", $smax_hari_text);
	$smax_minggu_text = new Text("mmsl_smax_minggu", "mmsl_smax_minggu", "2");
	$smax_minggu_text->setAtribute("disabled='disabled'");
	$form->addElement("<small>S-Max - Minggu</small>", $smax_minggu_text);
	$smin_hari_text = new Text("mmsl_smin_hari", "mmsl_smin_hari", "7");
	$smin_hari_text->setTypical("money");
	$form->addElement("S-Min - Hari", $smin_hari_text);
	$smin_minggu_text = new Text("mmsl_smin_minggu", "mmsl_smin_minggu", "1");
	$smin_minggu_text->setAtribute("disabled='disabled'");
	$form->addElement("<small>S-Min - Minggu</small>", $smin_minggu_text);
	$rop_hari_text = new Text("mmsl_rop_hari", "mmsl_rop_hari", "7");
	$rop_hari_text->setTypical("money");
	$form->addElement("ROP - Hari", $rop_hari_text);
	$rop_minggu_text = new Text("mmsl_rop_minggu", "mmsl_rop_minggu", "1");
	$rop_minggu_text->setAtribute("disabled='disabled'");
	$form->addElement("<small>ROP - Minggu</small>", $rop_minggu_text);
	$lt_hari_text = new Text("mmsl_lt_hari", "mmsl_lt_hari", "2");
	$lt_hari_text->setTypical("money");
	$form->addElement("LT - Hari", $lt_hari_text);
	$lt_minggu_text = new Text("mmsl_lt_minggu", "mmsl_lt_minggu", "0.286");
	$lt_minggu_text->setAtribute("disabled='disabled'");
	$form->addElement("<small>LT - Minggu</small>", $lt_minggu_text);
	$show_button = new Button("", "", "Tampilkan");
	$show_button->setClass("btn-primary");
	$show_button->setIcon("icon-white icon-repeat");
	$show_button->setIsButton(Button::$ICONIC);
	$show_button->setAtribute("id='btn_show'");
	$show_button->setAction("mmsl.view()");
	$history_button = new Button("", "", "Riwayat");
	$history_button->setClass("btn-info");
	$history_button->setIcon("fa fa-history");
	$history_button->setIsButton(Button::$ICONIC);
	$history_button->setAtribute("id='btn_history'");
	$history_button->setAction("mmsl.history()");
	$print_button = new Button("", "", "Cetak");
	$print_button->setClass("btn-inverse");
	$print_button->setIcon("icon-white icon-print");
	$print_button->setIsButton(Button::$ICONIC);
	$print_button->setAtribute("id='btn_print'");
	$print_button->setAction("mmsl.print()");
	$btn_group = new ButtonGroup("noprint");
	$btn_group->addButton($show_button);
	$btn_group->addButton($history_button);
	$btn_group->addButton($print_button);
	$form->addElement("", $btn_group);
	
	$table = new Table(
		array("No.", "Nama Obat", "Jenis Obat", "PRDI I", "PRDI II", "PRDI III", "PRDI IV", "PRDII I", "PRDII II", "PRDII III", "PRDII IV", "PR Total", "Sisa Stok Depo I", "Sisa Stok Depo II", "Sisa Gudang Farmasi", "Sisa Total"),
		"",
		null,
		true
	);
	$table->setName("mmsl");
	$table->setAction(false);
	$table->setFooterVisible(false);
	$table->setHeaderVisible(false);
	$table->addHeader("after", "
		<tr class='inverse'>
			<th rowspan='3' style='vertical-align: middle !important;'>
				<center><small>No.</small></center>
			</th>
			<th rowspan='3' style='vertical-align: middle !important;'>
				<center><small>Nama Obat</small></center>
			</th>
			<th rowspan='3' style='vertical-align: middle !important;'>
				<center><small>Jenis Obat</small></center>
			</th>
			<th colspan='5' style='vertical-align: middle !important;'>
				<center><small>Pemakaian Riil <div id='bulan_tahun_dinamis'>" . strftime('%B %Y') . "</div></small></center>
			</th>
			<th colspan='3' style='vertical-align: middle !important;'>
				<center><small>Sisa Stok <div id='bulan_tahun_sekarang'>" . strftime('%d %B %Y') . "</div></small></center>
			</th>
			<th colspan='2' style='vertical-align: middle !important;'>
				<center><small>Forecasting Rerata Penggunaan Riil</small></center>
			</th>
			<th colspan='2' style='vertical-align: middle !important;'>
				<center>
					<small>S-Max<br/><div id='s_max_option'></div></small>
				</center>
			</th>
			<th colspan='2' style='vertical-align: middle !important;'>
				<center>
					<small>S-Min<br/><div id='s_min_option'></div></small>
				</center>
			</th>
			<th colspan='2' style='vertical-align: middle !important;'>
				<center>
					<small>ROP<br/><div id='rop_option'></div></small>
				</center>
			</th>
			<th colspan='2' style='vertical-align: middle !important;'>
				<center>
					<small>Lead Time<br/><div id='lt_option'></div></small>
				</center>
			</th>
			<th colspan='2' style='vertical-align: middle !important;'>
				<center><small>Warning MMSL</small></center>
			</th>
			<th colspan='2' style='vertical-align: middle !important;'>
				<center><small>Quantity Beli</small></center>
			</th>
		</tr>
		<tr class='inverse'>
			<th colspan='4' style='vertical-align: middle !important;'>
				<center><small>Depo Farmasi</small></center>
			</th>
			<th rowspan='2' style='vertical-align: middle !important;'>
				<center><small>Total</small></center>
			</th>
			<th rowspan='2' style='vertical-align: middle !important;'>
				<center><small>Gudang Farmasi</small></center>
			</th>
			<th rowspan='2' style='vertical-align: middle !important;'>
				<center><small>Depo Farmasi</small></center>
			</th>
			<th rowspan='2' style='vertical-align: middle !important;'>
				<center><small>Total</small></center>
			</th>
			<th rowspan='2' style='vertical-align: middle !important;'>
				<center><small>Normal</small></center>
			</th>
			<th rowspan='2' style='vertical-align: middle !important;'>
				<center><small>Bobot</small></center>
			</th>
			<th rowspan='2' style='vertical-align: middle !important;'>
				<center><small>Normal</small></center>
			</th>
			<th rowspan='2' style='vertical-align: middle !important;'>
				<center><small>Bobot</small></center>
			</th>
			<th rowspan='2' style='vertical-align: middle !important;'>
				<center><small>Normal</small></center>
			</th>
			<th rowspan='2' style='vertical-align: middle !important;'>
				<center><small>Bobot</small></center>
			</th>
			<th rowspan='2' style='vertical-align: middle !important;'>
				<center><small>Normal</small></center>
			</th>
			<th rowspan='2' style='vertical-align: middle !important;'>
				<center><small>Bobot</small></center>
			</th>
			<th rowspan='2' style='vertical-align: middle !important;'>
				<center><small>Normal</small></center>
			</th>
			<th rowspan='2' style='vertical-align: middle !important;'>
				<center><small>Bobot</small></center>
			</th>
			<th rowspan='2' style='vertical-align: middle !important;'>
				<center><small>Normal</small></center>
			</th>
			<th rowspan='2' style='vertical-align: middle !important;'>
				<center><small>Bobot</small></center>
			</th>
			<th rowspan='2' style='vertical-align: middle !important;'>
				<center><small>Normal</small></center>
			</th>
			<th rowspan='2' style='vertical-align: middle !important;'>
				<center><small>Bobot</small></center>
			</th>
		</tr>
		<tr class='inverse'>
			<th colspan='1' style='vertical-align: middle !important;'>
				<center>
					<small>I<br /><div id='prd1_bobot_1'></div></small>
				</center>
			</th>
			<th colspan='1' style='vertical-align: middle !important;'>
				<center>
					<small>II<br /><div id='prd1_bobot_2'></div></small>
				</center>
			</th>
			<th colspan='1' style='vertical-align: middle !important;'>
				<center>
					<small>III<br /><div id='prd1_bobot_3'></div></small>
				</center>
			</th>
			<th colspan='1' style='vertical-align: middle !important;'>
				<center>
					<small>IV<br /><div id='prd1_bobot_4'></div></small>
				</center>
			</th>
		</tr>
	");
	
	if (isset($_POST['command'])) {
		if ($_POST['command'] == "get_jumlah") {
			$service = new ServiceConsumer($db, "get_mmsl_jumlah_master_obat", array(), "gudang_farmasi");
			$service->setMode(ServiceConsumer::$CLEAN_BOTH);
			$content = $service->execute()->getContent();
			$data = array();
			$data['jumlah'] = $content[0];
			$data['timestamp'] = date("d-m-Y H:i:s");
			echo json_encode($data);
		} else if ($_POST['command'] == "get_mmsl") {
			$timestamp_start = date("d-m-Y, H:i:s");
			$row_num = $_POST['row_num'];
			$params = array();
			$params['row_num'] = $row_num;
			$service = new ServiceConsumer($db, "get_mmsl_master_obat", $params, "gudang_farmasi");
			$service->setMode(ServiceConsumer::$CLEAN_BOTH);
			$content = $service->execute()->getContent();
			$nama_obat = $content[0];
			$nama_jenis_obat = $content[1];
			
			$bulan = $_POST['bulan'];
			$tahun = $_POST['tahun'];
			$jumlah_hari = cal_days_in_month(0, $bulan, $tahun);
			
			//PRDI - I:
			$start_day = 1;
			$end_day = 7;
			$params = array();
			$params['nama_obat'] = $nama_obat;
			$params['nama_jenis_obat'] = $nama_jenis_obat;
			$params['tanggal_from'] = $tahun . "-" . $bulan . "-" . $start_day;
			$params['tanggal_to'] = $tahun . "-" . $bulan . "-" . $end_day;
			$service = new ServiceConsumer($db, "get_mmsl_penggunaan_riil", $params, "depo_farmasi");
			$service->setMode(ServiceConsumer::$CLEAN_BOTH);
			$content = $service->execute()->getContent();
			$prd1_1 = $content[0];
			
			//PRDI - II:
			$start_day += 7;
			$end_day += 7;
			$params = array();
			$params['nama_obat'] = $nama_obat;
			$params['nama_jenis_obat'] = $nama_jenis_obat;
			$params['tanggal_from'] = $tahun . "-" . $bulan . "-" . $start_day;
			$params['tanggal_to'] = $tahun . "-" . $bulan . "-" . $end_day;
			$service = new ServiceConsumer($db, "get_mmsl_penggunaan_riil", $params, "depo_farmasi");
			$service->setMode(ServiceConsumer::$CLEAN_BOTH);
			$content = $service->execute()->getContent();
			$prd1_2 = $content[0];
			
			//PRDI - III:
			$start_day += 7;
			$end_day += 7;
			$params = array();
			$params['nama_obat'] = $nama_obat;
			$params['nama_jenis_obat'] = $nama_jenis_obat;
			$params['tanggal_from'] = $tahun . "-" . $bulan . "-" . $start_day;
			$params['tanggal_to'] = $tahun . "-" . $bulan . "-" . $end_day;
			$service = new ServiceConsumer($db, "get_mmsl_penggunaan_riil", $params, "depo_farmasi");
			$service->setMode(ServiceConsumer::$CLEAN_BOTH);
			$content = $service->execute()->getContent();
			$prd1_3 = $content[0];
			
			//PRDI - IV:
			$start_day += 7;
			$end_day = $jumlah_hari;
			$params = array();
			$params['nama_obat'] = $nama_obat;
			$params['nama_jenis_obat'] = $nama_jenis_obat;
			$params['tanggal_from'] = $tahun . "-" . $bulan . "-" . $start_day;
			$params['tanggal_to'] = $tahun . "-" . $bulan . "-" . $end_day;
			$service = new ServiceConsumer($db, "get_mmsl_penggunaan_riil", $params, "depo_farmasi");
			$service->setMode(ServiceConsumer::$CLEAN_BOTH);
			$content = $service->execute()->getContent();
			$prd1_4 = $content[0];
			
			$prd_total = $prd1_1 + $prd1_2 + $prd1_3 + $prd1_4;
			$prd_rerata_normal = ceil($prd_total / 4);
			
			$bobot_1 = $_POST['bobot_1'];
			$bobot_2 = $_POST['bobot_2'];
			$bobot_3 = $_POST['bobot_3'];
			$bobot_4 = $_POST['bobot_4'];
			$bobot_total = $bobot_1 + $bobot_2 + $bobot_3 + $bobot_4;
			$prd_rerata_bobot = ceil(($bobot_1 * ($prd1_1 + $prd2_1) + $bobot_2 * ($prd1_2 + $prd2_2) + $bobot_3 * ($prd1_3 + $prd2_3) + $bobot_4 * ($prd1_4 + $prd2_4)) / $bobot_total);
			
			$smax_minggu = round($_POST['smax_minggu'], 1);
			$smin_minggu = round($_POST['smin_minggu'], 1);
			$rop_minggu = round($_POST['rop_minggu'], 1);
			$lt_minggu = round($_POST['lt_minggu'], 1);
			$smax_normal = round($smax_minggu * $prd_rerata_normal);
			$smax_bobot = round($smax_minggu * $prd_rerata_bobot);
			$smin_normal = round($smin_minggu * $prd_rerata_normal);
			$smin_bobot = round($smin_minggu * $prd_rerata_bobot);
			$rop_normal = round($rop_minggu * $prd_rerata_normal);
			$rop_bobot = round($rop_minggu * $prd_rerata_bobot);
			$lt_normal = round($lt_minggu * $prd_rerata_normal);
			$lt_bobot = round($lt_minggu * $prd_rerata_bobot);
			
			$service = new ServiceConsumer($db, "get_mmsl_stok", $params, "depo_farmasi");
			$service->setMode(ServiceConsumer::$CLEAN_BOTH);
			$content = $service->execute()->getContent();
			$stok_depo_1 = $content[0];
			$service = new ServiceConsumer($db, "get_mmsl_stok", $params, "gudang_farmasi");
			$service->setMode(ServiceConsumer::$CLEAN_BOTH);
			$content = $service->execute()->getContent();
			$stok_gudang_farmasi = $content[0];
			$stok_total = $stok_depo_1 + $stok_gudang_farmasi;
			
			$mmsl_status_normal = "TIDAK PESAN";
			$qty_beli_normal = 0;
			if ($stok_total <= $rop_normal) {
				$qty_beli_normal = $smax_normal - $stok_total + $lt_normal;
				$mmsl_status_normal = $qty_beli_normal == 0? "TIDAK PESAN" : "<strong>PESAN</strong>";
			}
			$mmsl_status_bobot = "TIDAK PESAN";
			$qty_beli_bobot = 0;
			if ($stok_total <= $rop_bobot) {
				$qty_beli_bobot = $smax_bobot - $stok_total + $lt_bobot;
				$mmsl_status_bobot = $qty_beli_bobot == 0? "TIDAK PESAN" : "<strong>PESAN</strong>";
			}
			
			$data = array();
			$data['nama_obat'] = $nama_obat;
			$data['jenis_obat'] = $nama_jenis_obat;
			$data['timestamp_start'] = $timestamp_start;
			$timestamp_end = date("d-m-Y, H:i:s");
			$data['timestamp_end'] = $timestamp_end;
			$durasi = strtotime($timestamp_end) - strtotime($timestamp_start);
			$data['durasi'] = $durasi . " detik";
			$data['element'] = "
				<tr id='obat_" . $row_num . "'>
					<td><small>" . ($row_num + 1) . "</small></td>
					<td><small>" . $nama_obat . "</small></td>
					<td><small>" . $nama_jenis_obat . "</small></td>
					<td style='text-align: right !important;'><small>" . $prd1_1 . "</small></td>
					<td style='text-align: right !important;'><small>" . $prd1_2 . "</small></td>
					<td style='text-align: right !important;'><small>" . $prd1_3 . "</small></td>
					<td style='text-align: right !important;'><small>" . $prd1_4 . "</small></td>
					<td style='text-align: right !important;'><small>" . $prd_total . "</small></td>
					<td style='text-align: right !important;'><small>" . $stok_gudang_farmasi . "</small></td>
					<td style='text-align: right !important;'><small>" . $stok_depo_1 . "</small></td>
					<td style='text-align: right !important;'><small>" . $stok_total . "</small></td>
					<td style='text-align: right !important;'><small>" . $prd_rerata_normal . "</small></td>
					<td style='text-align: right !important;'><small>" . $prd_rerata_bobot . "</small></td>
					<td style='text-align: right !important;'><small>" . $smax_normal . "</small></td>
					<td style='text-align: right !important;'><small>" . $smax_bobot . "</small></td>
					<td style='text-align: right !important;'><small>" . $smin_normal . "</small></td>
					<td style='text-align: right !important;'><small>" . $smin_bobot . "</small></td>
					<td style='text-align: right !important;'><small>" . $rop_normal . "</small></td>
					<td style='text-align: right !important;'><small>" . $rop_bobot . "</small></td>
					<td style='text-align: right !important;'><small>" . $lt_normal . "</small></td>
					<td style='text-align: right !important;'><small>" . $lt_bobot . "</small></td>
					<td style='text-align: right !important;'><small>" . $mmsl_status_normal . "</small></td>
					<td style='text-align: right !important;'><small>" . $mmsl_status_bobot . "</small></td>
					<td style='text-align: right !important;'><small>" . $qty_beli_normal . "</small></td>
					<td style='text-align: right !important;'><small>" . $qty_beli_bobot . "</small></td>
				</tr>
			";
			echo json_encode($data);
		}
		return;
	}
	
	$loading_bar = new LoadingBar("loading_bar", "");
	$button = new Button("", "", "Batal");
	$button->addClass("btn-primary");
	$button->setIsButton(Button::$ICONIC_TEXT);
	$button->setIcon("fa fa-close");
	$button->setAction("mmsl.cancel()");
	$loading_modal = new Modal("mmsl_modal", "", "Proses..");
	$loading_modal->addHtml($loading_bar->getHtml(), "after");
	$loading_modal->addFooter($button);
	
	$history_modal = new Modal("history_modal", "", "Riwayat Penarikan Data");
	$history_modal->setClass(Modal::$HALF_MODEL);
	$history_table = new Table(
		array("No.", "Obat", "Jenis", "Waktu Penarikan", "Durasi"),
		"",
		null,
		true
	);
	$history_table->setName("history");
	$history_table->setAction(false);
	$history_table->setFooterVisible(false);
	$history_modal->addHtml($history_table->getHtml(), "after");
	
	echo $history_modal->getHtml();
	echo $loading_modal->getHtml();
	echo $form->getHtml();
	echo "<div id='table_content'>";
	echo $table->getHtml();
	echo "</div>";
	echo "<div id='info'></div>";
	echo addJS("framework/smis/js/table_action.js");
	echo addJS("framework/bootstrap/js/bootstrap-datepicker.js");
	echo addCSS("framework/bootstrap/css/datepicker.css");
	echo addJS ("base-js/smis-base-loading.js");
?>
<script type="text/javascript">
	function MMSLAction(name, page, action, column) {
		this.initialize(name, page, action, column);
	}
	MMSLAction.prototype.constructor = MMSLAction;
	MMSLAction.prototype = new TableAction();
	MMSLAction.prototype.view = function() {
		if ($("#mmsl_bulan").val() == "" || $("#mmsl_tahun").val() == "" || $("#mmsl_bobot_1").val() == "" || $("#mmsl_bobot_2").val() == "" || $("#mmsl_bobot_3").val() == "" || $("#mmsl_bobot_4").val() == "")
			return;
		FINISHED = false;
		$("#mmsl_list").html("");
		$("#history_list").html("");
		$("#info").html("");
		var self = this;
		$("#loading_bar").sload("true", "Harap ditunggu...", 0);
		$("#mmsl_modal").smodal("show");
		var data = this.getRegulerData();
		data['command'] = "get_jumlah";
		$.post(
			"",
			data,
			function(response) {
				var json = JSON.parse(response);
				self.fill_html(0, json.jumlah);
			}
		);
	};
	MMSLAction.prototype.fill_html = function(row_num, limit) {
		if (row_num == limit || FINISHED) {
			$("#mmsl_modal").smodal("hide");
			if (FINISHED == false && row_num == limit) {
				$("#info").html(
					"<div class='alert alert-block alert-info'>" +
						 "<center><strong>PROSES SELESAI</strong></center>" +
					 "</div>"
				);
			} else {
				$("#info").html(
					"<div class='alert alert-block alert-inverse'>" +
						 "<center><strong>PROSES DIBATALKAN</strong></center>" +
					 "</div>"
				);
			}
			return;
		}
		var self = this;
		var data = this.getRegulerData();
		data['command'] = "get_mmsl";
		data['row_num'] = row_num;
		data['bulan'] = $("#mmsl_bulan").val();
		data['tahun'] = $("#mmsl_tahun").val();
		data['bobot_1'] = $("#mmsl_bobot_1").val();
		data['bobot_2'] = $("#mmsl_bobot_2").val();
		data['bobot_3'] = $("#mmsl_bobot_3").val();
		data['bobot_4'] = $("#mmsl_bobot_4").val();
		data['smax_minggu'] = $("#mmsl_smax_minggu").val();
		data['smin_minggu'] = $("#mmsl_smin_minggu").val();
		data['rop_minggu'] = $("#mmsl_rop_minggu").val();
		data['lt_minggu'] = $("#mmsl_lt_minggu").val();
		$.post(
			"",
			data,
			function(response) {
				var json = JSON.parse(response);
				$("#history_list").append(
					"<tr>" +
						"<td><small>" + (row_num+1) + "</small></td>" +
						"<td><small>" + json.nama_obat + "</small></td>" +
						"<td><small>" + json.jenis_obat + "</small></td>" +
						"<td><small>" + json.timestamp_start + " s/d " + json.timestamp_end + "</small></td>" +
						"<td><small>" + json.durasi + "</small></td>" +
					"</tr>"
				);
				$("#mmsl_list").append(json.element);
				$("#loading_bar").sload("true", json.nama_obat + " - " + json.jenis_obat + " (" + (row_num + 1) + " / " + limit + ")", (row_num+1) * 100 / limit);
				self.fill_html(row_num + 1, limit);
			}
		);
	};
	MMSLAction.prototype.history = function() {
		$("#history_modal").smodal("show");
	};
	MMSLAction.prototype.cancel = function() {
		FINISHED = true;
	};
	
	var mmsl;
	var FINISHED;
	$(document).ready(function() {
		mmsl = new MMSLAction(
			"mmsl",
			"perencanaan",
			"rp_mmsl",
			new Array("bobot_1", "bobot_2", "bobot_3", "bobot_4")
		);
		$("#prd1_bobot_1").html($("#mmsl_bobot_1").val());
		$("#prd1_bobot_2").html($("#mmsl_bobot_2").val());
		$("#prd1_bobot_3").html($("#mmsl_bobot_3").val());
		$("#prd1_bobot_4").html($("#mmsl_bobot_4").val());
		$("#prd2_bobot_1").html($("#mmsl_bobot_1").val());
		$("#prd2_bobot_2").html($("#mmsl_bobot_2").val());
		$("#prd2_bobot_3").html($("#mmsl_bobot_3").val());
		$("#prd2_bobot_4").html($("#mmsl_bobot_4").val());
		var s_max_option = $("#mmsl_smax_hari").val() + " hari<br/>" + $("#mmsl_smax_minggu").val() + " minggu";
		var s_min_option = $("#mmsl_smin_hari").val() + " hari<br/>" + $("#mmsl_smin_minggu").val() + " minggu";
		var rop_option = $("#mmsl_rop_hari").val() + " hari<br/>" + $("#mmsl_rop_minggu").val() + " minggu";
		var lt_option = $("#mmsl_lt_hari").val() + " hari<br/>" + $("#mmsl_lt_minggu").val() + " minggu";
		$("#s_max_option").html(s_max_option);
		$("#s_min_option").html(s_min_option);
		$("#rop_option").html(rop_option);
		$("#lt_option").html(lt_option);
		$("#mmsl_modal").on("show", function() {
			$("a.close").hide();
		});
		$("#history_modal").on("show", function() {
			$("a.close").show();
		});
		$("#mmsl_bobot_1").on("keyup", function() {
			$("#prd1_bobot_1").html($("#mmsl_bobot_1").val());
			$("#prd2_bobot_1").html($("#mmsl_bobot_1").val());
		});
		$("#mmsl_bobot_2").on("keyup", function() {
			$("#prd1_bobot_2").html($("#mmsl_bobot_2").val());
			$("#prd2_bobot_2").html($("#mmsl_bobot_2").val());
		});
		$("#mmsl_bobot_3").on("keyup", function() {
			$("#prd1_bobot_3").html($("#mmsl_bobot_3").val());
			$("#prd2_bobot_3").html($("#mmsl_bobot_3").val());
		});
		$("#mmsl_bobot_4").on("keyup", function() {
			$("#prd1_bobot_4").html($("#mmsl_bobot_4").val());
			$("#prd2_bobot_4").html($("#mmsl_bobot_4").val());
		});
		$("#mmsl_smax_hari").on("keyup", function() {
			var smax_hari = $("#mmsl_smax_hari").val();
			if (smax_hari == "") {
				smax_hari = 0;
			}
			var smax_minggu = Math.round(parseFloat(smax_hari / 7) * 1000) / 1000;
			$("#mmsl_smax_minggu").val(smax_minggu);
			s_max_option = smax_hari + " hari<br/>" + smax_minggu + " minggu";
			$("#s_max_option").html(s_max_option);
		});
		$("#mmsl_smax_hari").on("blur", function() {
			if ($("#mmsl_smax_hari").val() == "")
				$("#mmsl_smax_hari").val("0");
		});
		$("#mmsl_smin_hari").on("keyup", function() {
			var smin_hari = $("#mmsl_smin_hari").val();
			if (smin_hari == "") {
				smin_hari = 0;
			}
			var smin_minggu = Math.round(parseFloat(smin_hari / 7) * 1000) / 1000;
			$("#mmsl_smin_minggu").val(smin_minggu);
			s_min_option = smin_hari + " hari<br/>" + smin_minggu + " minggu";
			$("#s_min_option").html(s_min_option);
		});
		$("#mmsl_smin_hari").on("blur", function() {
			if ($("#mmsl_smin_hari").val() == "")
				$("#mmsl_smin_hari").val("0");
		});
		$("#mmsl_rop_hari").on("keyup", function() {
			var rop_hari = $("#mmsl_rop_hari").val();
			if (rop_hari == "") {
				rop_hari = 0;
			}
			var rop_minggu = Math.round(parseFloat(rop_hari / 7) * 1000) / 1000;
			$("#mmsl_rop_minggu").val(rop_minggu);
			rop_option = rop_hari + " hari<br/>" + rop_minggu + " minggu";
			$("#rop_option").html(rop_option);
		});
		$("#mmsl_rop_hari").on("blur", function() {
			if ($("#mmsl_rop_hari").val() == "")
				$("#mmsl_rop_hari").val("0");
		});
		$("#mmsl_lt_hari").on("keyup", function() {
			var lt_hari = $("#mmsl_lt_hari").val();
			if (lt_hari == "") {
				lt_hari = 0;
			}
			var lt_minggu = Math.round(parseFloat(lt_hari / 7) * 1000) / 1000;
			$("#mmsl_lt_minggu").val(lt_minggu);
			lt_option = lt_hari + " hari<br/>" + lt_minggu + " minggu";
			$("#lt_option").html(lt_option);
		});
		$("#mmsl_lt_hari").on("blur", function() {
			if ($("#mmsl_lt_hari").val() == "")
				$("#mmsl_lt_hari").val("0");
		});
		$("#mmsl_smax_hari, #mmsl_smin_hari, #mmsl_rop_hari, #mmsl_lt_hari").keydown(function (e) {
			if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 || (e.keyCode == 65 && ( e.ctrlKey === true || e.metaKey === true ) ) || (e.keyCode >= 35 && e.keyCode <= 40)) {
					 return;
			}
			if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
				e.preventDefault();
			}
		});
		$(document).keyup(function(e) {
			if (e.which == 27) {
				FINISHED = true;
			}
		});
		$("#mmsl_bulan, #mmsl_tahun").on("change", function() {
			var bulan = $("#mmsl_bulan :selected").text();
			var tahun = $("#mmsl_tahun").val();
			$("#bulan_tahun_dinamis").html(bulan + " " + tahun);
		});
	});
</script>