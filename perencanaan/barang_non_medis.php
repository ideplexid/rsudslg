<?php
	global $db;
	
	$table = new Table(
		array("No.", "ID-Data", "Kode", "Nama", "Jenis", "Inventaris", "Medis", "Satuan", "Konversi", "Sat. Konversi"),
		"Perencanaan : Daftar Barang Non Medis",
		null,
		true
	);
	$table->setName("barang_non_medis");
	
	$joa_table = new Table(
		array("No.", "Kode", "Nama Jenis Barang Non Medis"),
		"",
		null,
		true
	);
	$joa_table->setName("jenis_barang_non_medis");
	$joa_table->setModel(Table::$SELECT);
	$joa_adapter = new SimpleAdapter(true, "No.");
	$joa_adapter->add("Kode", "kode");
	$joa_adapter->add("Nama Jenis Barang Non Medis", "nama");
	$joa_dbtable = new DBTable($db, "smis_pr_jenis_barang");
	$joa_dbtable->addCustomKriteria(" medis ", " = '0' ");
	$joa_dbtable->setOrder(" id DESC ");
	$joa_dbresponder = new DBResponder(
		$joa_dbtable,
		$joa_table,
		$joa_adapter
	);
	$super_command = new SuperCommand();
	$super_command->addResponder("jenis_barang_non_medis", $joa_dbresponder);
	$init = $super_command->initialize();
	if ($init != null) {
		echo $init;
		return;
	}
	
	if (isset($_POST['command'])) {
		$adapter = new SimpleAdapter(true, "No.");
		$adapter->add("ID-Data", "id", "digit6");
		$adapter->add("Kode", "kode");
		$adapter->add("Nama", "nama");
		$adapter->add("Jenis", "nama_jenis_barang");
		$adapter->add("Inventaris", "inventaris", "trivial_1_&#10003_&#10005");
		$adapter->add("Medis", "medis", "trivial_1_&#10003_&#10005");
		$adapter->add("Satuan", "satuan");
		$adapter->add("Konversi", "konversi", "number");
		$adapter->add("Sat. Konversi", "satuan_konversi");
		$dbtable = new DBTable($db, "smis_pr_barang");
		if ($_POST['command'] == "get_last_code") {
			$row = $dbtable->get_row("
				SELECT CONVERT(TRIM(SUBSTRING(kode, LENGTH('" . $_POST['prefix_kode'] . "." . $_POST['kode_jenis_barang'] . ".') + 1)), UNSIGNED INTEGER) AS 'number' 
				FROM smis_pr_barang 
				WHERE kode LIKE '" . $_POST['prefix_kode'] . "." . $_POST['kode_jenis_barang'] . ".%' 
				ORDER BY CONVERT(TRIM(SUBSTRING(kode, LENGTH('" . $_POST['prefix_kode'] . "." . $_POST['kode_jenis_barang'] . ".') + 1)), UNSIGNED INTEGER) DESC 
				LIMIT 0, 1
			");
			$data['postfix'] = $row->number;
			echo json_encode($data);
			return;
		}
		$filter = "";
		if (isset($_POST['kriteria']))
			$filter = " AND (kode LIKE '%" . $_POST['kriteria'] . "%' OR nama LIKE '" . $_POST['kriteria'] . "' OR nama_jenis_barang LIKE '%" . $_POST['kriteria'] . "%') ";
		$query_value = "
			SELECT *
			FROM smis_pr_barang
			WHERE prop NOT LIKE 'del' AND NOT (medis = '1' AND inventaris = '0') " . $filter . " 
			ORDER BY id DESC
		";
		$query_count = "
			SELECT COUNT(*)
			FROM (
				" . $query_value . "
			) v
		";
		$dbtable->setPreferredQuery(true, $query_value, $query_count);
		$dbresponder = new DBResponder(
			$dbtable,
			$table,
			$adapter
		);
		$data = $dbresponder->command($_POST['command']);
		echo json_encode($data);
		return;
	}
	
	$modal = new Modal("barang_non_medis_add_form", "smis_form_container", "barang_non_medis");
	$modal->setTitle("Data Barang");
	$id_hidden = new Hidden("barang_non_medis_id", "barang_non_medis_id", "");
	$modal->addElement("", $id_hidden);
	$nama_text = new Text("barang_non_medis_nama", "barang_non_medis_nama", "");
	$modal->addElement("Nama", $nama_text);
	$prefix_hidden = new Hidden("barang_non_medis_prefix_kode", "barang_non_medis_prefix_kode", "REG");
	$modal->addElement("", $prefix_hidden);
	$id_jenis_hidden = new Hidden("barang_non_medis_id_jenis_barang", "barang_non_medis_id_jenis_barang", "");
	$modal->addElement("", $id_jenis_hidden);
	$jenis_text = new Text("barang_non_medis_nama_jenis_barang", "barang_non_medis_nama_jenis_barang", "");
	$jenis_text->setAtribute("disabled='disabled'");
	$jenis_text->setClass("smis-one-option-input");
	$jenis_button = new Button("", "", "Pilih");
	$jenis_button->setAtribute("id='jenis_barang_non_medis_btn'");
	$jenis_button->setClass("btn-info");
	$jenis_button->setIsButton(Button::$ICONIC);
	$jenis_button->setIcon("icon-white ".Button::$icon_list_alt);
	$jenis_button->setAction("jenis_barang_non_medis.chooser('jenis_barang_non_medis', 'jenis_barang_non_medis_button', 'jenis_barang_non_medis', jenis_barang_non_medis)");
	$jenis_input_group = new InputGroup("");
	$jenis_input_group->addComponent($jenis_text);
	$jenis_input_group->addComponent($jenis_button);
	$modal->addElement("Jenis Obat", $jenis_input_group);
	$kode_jenis_text = new Text("barang_non_medis_kode_jenis_barang", "barang_non_medis_kode_jenis_barang", "");
	$kode_jenis_text->setAtribute("disabled='disabled'");
	$modal->addElement(" ", $kode_jenis_text);
	$kode_text = new Text("barang_non_medis_kode", "obat_kode_alkes", "");
	$modal->addElement("Kode", $kode_text);
	$label_option = new OptionBuilder();
	$label_option->add("Non-Invetaris", "non_inventaris", "1");
	$label_option->add("Inventaris Medis", "inventaris_medis");
	$label_option->add("Inventaris Non-Medis", "inventaris_non_medis");
	$label_select = new Select("barang_non_medis_label", "barang_non_medis_label", $label_option->getContent());
	$modal->addElement("Label", $label_select);
	$satuan_text = new Text("barang_non_medis_satuan", "barang_non_medis_satuan", "");
	$modal->addElement("Satuan", $satuan_text);
	$konversi_text = new Text("barang_non_medis_konversi", "barang_non_medis_konversi", "");
	$modal->addElement("Konversi", $konversi_text);
	$satuan_konversi_text = new Text("barang_non_medis_satuan_konversi", "barang_non_medis_satuan_konversi", "");
	$modal->addElement("Stn. Konversi", $satuan_konversi_text);
	$save_button = new Button("", "", "Simpan");
	$save_button->setClass("btn-success");
	$save_button->setAction("barang_non_medis.save()");
	$save_button->setIcon("fa fa-floppy-o");
	$save_button->setIsButton(Button::$ICONIC);
	$modal->addFooter($save_button);
	
	echo $modal->getHtml();
	echo $table->getHtml();
	echo addJS("framework/smis/js/table_action.js");
	echo addJS("perencanaan/js/jenis_barang_non_medis_action.js", false);
	echo addJS("perencanaan/js/barang_non_medis_action.js", false);
	echo addJS("perencanaan/js/barang_non_medis.js", false);
?>