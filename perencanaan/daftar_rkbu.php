<?php 
	require_once("smis-base/smis-include-service-consumer.php");
	global $db;
	
	class RKBUTable extends Table {
		public function getBodyContent(){
			$content = "";
			if ($this->content!=NULL) {
				foreach ($this->content as $d) {
					$content .= "<tr>";
					foreach ($this->header as $h) {
						$content .= "<td>" . $d[$h] . "</td>";
					}
					if ($this->is_action) {
						$content .= "<td>" . $this->getFilteredContentButton($d['id'], $d['disetujui'])->getHtml() . "</td>";
					}
					$content .= "</tr>";
				}
			}
			return $content;
		}
		public function getFilteredContentButton($id, $disetujui) {
			$btn_group = new ButtonGroup("noprint");
			if ($disetujui == "belum" || $disetujui == "revisi") {
				$btn = new Button("", "", "Return");
				$btn->setAction($this->action . ".return('" . $id . "')");
				$btn->setClass("btn-danger");
				$btn->setAtribute("data-content='View' data-toggle='popover'");
				$btn->setIcon("fa fa-reply");
				$btn->setIsButton(Button::$ICONIC);
				$btn_group->addElement($btn);
				$btn = new Button("", "", "Edit");
				$btn->setAction($this->action . ".edit('" . $id . "')");
				$btn->setClass("btn-warning");
				$btn->setAtribute("data-content='Edit' data-toggle='popover'");
				$btn->setIcon("icon-edit icon-white");
				$btn->setIsButton(Button::$ICONIC);
				$btn_group->addElement($btn);				
			} else if ($disetujui == "sudah") {
				$btn = new Button("", "", "View");
				$btn->setAction($this->action . ".detail('" . $id . "')");
				$btn->setClass("btn-success");
				$btn->setAtribute("data-content='View' data-toggle='popover'");
				$btn->setIcon("icon-eye-open icon-white");
				$btn->setIsButton(Button::$ICONIC);
				$btn_group->addElement($btn);
				$btn = new Button("", "", "Cancel");
				$btn->setAction($this->action . ".cancel('" . $id . "')");
				$btn->setClass("btn-danger");
				$btn->setAtribute("data-content='Cancel' data-toggle='popover'");
				$btn->setIcon("icon-remove icon-white");
				$btn->setIsButton(Button::$ICONIC);
				$btn_group->addElement($btn);
			}
			return $btn_group;
		}
	}
	$rkbu_table = new RKBUTable(
		array("Unit", "Nomor", "Tahun RKBU", "Bulan", "Tanggal Diajukan", "Tanggal Ditinjau", "Status"),
		"",
		null,
		true
	);
	$rkbu_table->setName("rkbu");
	$rkbu_table->setAddButtonEnable(false);
	$unit_search_text = new Text("search_unit", "search_unit", "");
	$unit_search_text->setClass("search");
	$nomor_search_text = new Text("search_nomor", "search_nomor", "");
	$nomor_search_text->setClass("search");
	$tahun_search_text = new Text("search_tahun", "search_tahun", "");
	$tahun_search_text->setClass("search");
	$bulan_option = new OptionBuilder();
	$bulan_option->add("SEMUA", "%%", "1");
	$bulan_option->add("1 - Januari", "Januari");
	$bulan_option->add("2 - Februari", "Februari");
	$bulan_option->add("3 - Maret", "Maret");
	$bulan_option->add("4 - April", "April");
	$bulan_option->add("5 - Mei", "Mei");
	$bulan_option->add("6 - Juni", "Juni");
	$bulan_option->add("7 - Juli", "Juli");
	$bulan_option->add("8 - Agustus", "Agustus");
	$bulan_option->add("9 - September", "September");
	$bulan_option->add("10 - Oktober", "Oktober");
	$bulan_option->add("11 - Nopember", "Nopember");
	$bulan_option->add("12 - Desember", "Desember");
	$bulan_search_select = new Select("search_bulan", "search_bulan", $bulan_option->getContent());
	$bulan_search_select->setClass("search_select");
	$tanggal_diajukan_search_text = new Text("search_tanggal_diajukan", "search_tanggal_diajukan", "");
	$tanggal_diajukan_search_text->setModel(Text::$DATE);
	$tanggal_diajukan_search_text->setClass("search");
	$tanggal_ditinjau_search_text = new Text("search_tanggal_ditinjau", "search_tanggal_ditinjau", "");
	$tanggal_ditinjau_search_text->setModel(Text::$DATE);
	$tanggal_ditinjau_search_text->setClass("search");
	$status_option = new OptionBuilder();
	$status_option->add("SEMUA", "%%", "1");
	$status_option->add("Disetujui", "sudah");
	$status_option->add("Belum Disetujui", "belum");
	$status_search_select = new Select("search_status", "search_status", $status_option->getContent());
	$status_search_select->setClass("search_select");
	$header = 	"<tr class='rkbu_header'>
					<td>" . $unit_search_text->getHtml() . "</td>
					<td>" . $nomor_search_text->getHtml() . "</td>
					<td>" . $tahun_search_text->getHtml() . "</td>
					<td>" . $bulan_search_select->getHtml() . "</td>
					<td>" . $tanggal_diajukan_search_text->getHtml() . "</td>
					<td>" . $tanggal_ditinjau_search_text->getHtml() . "</td>
					<td>" . $status_search_select->getHtml() . "</td>
					<td></td>
				 </tr>";
	$rkbu_table->addHeader("after", $header);
	
	if (isset($_POST['command'])) {
		class RKBUAdapter extends ArrayAdapter {
			public function adapt($row) {
				$array = array();
				$array['id'] = $row->id;
				$array['disetujui'] = $row->disetujui;
				$array['Unit'] = self::format("unslug", $row->unit);
				$array['Nomor'] = self::format("digit8", $row->f_id);
				$array['Tahun RKBU'] = $row->tahun;
				$array['Bulan'] = $row->daftar_bulan;
				$array['Tanggal Diajukan'] = self::format("date d-m-Y", $row->tanggal_diajukan);
				if ($row->tanggal_ditinjau != null && self::format("date d-m-Y", $row->tanggal_ditinjau) != "00-00-0000")
					$array['Tanggal Ditinjau'] = self::format("date d-m-Y", $row->tanggal_ditinjau);
				else
					$array['Tanggal Ditinjau'] = "-";
				if ($row->disetujui == "sudah")
					$array['Status'] = "Disetujui";
				else if ($row->disetujui == "belum")
					$array['Status'] = "Belum Ditinjau";
				else if ($row->disetujui == "revisi")
					$array['Status'] = "Revisi Belum Ditinjau";
				return $array;
			}
		}
		$rkbu_adapter = new RKBUAdapter();
		$rkbu_dbtable = new DBTable(
			$db,
			"smis_pr_rkbu"
		);
		$filter = "";
		if (isset($_POST['search_unit']) || isset($_POST['search_nomor']) || isset($_POST['search_tahun']) || isset($_POST['search_bulan']) || isset($_POST['search_tanggal_diajukan']) || isset($_POST['search_tanggal_ditinjau']) || isset($_POST['search_status'])) {
			$filter .= "unit LIKE '%" . $_POST['search_unit'] . "%' AND f_id LIKE '%" . $_POST['search_nomor'] . "%' AND tahun LIKE '%" . $_POST['search_tahun'] . "%' AND daftar_bulan LIKE '%" . $_POST['search_bulan'] . "%' AND IF(tanggal_diajukan IS NULL, '0000-00-00', tanggal_diajukan) LIKE '%" . $_POST['search_tanggal_diajukan'] . "%' AND IF(tanggal_ditinjau IS NULL, '0000-00-00', tanggal_ditinjau) LIKE '%" . $_POST['search_tanggal_ditinjau'] . "%' AND disetujui LIKE '%" . $_POST['search_status'] . "%'";
		}
		$query_value = "
			SELECT *
			FROM (
				SELECT a.*, GROUP_CONCAT(DISTINCT CONCAT('<li>', b.bulan, '</li>') ORDER BY FIELD(b.bulan, 'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'Nopember', 'Desember') SEPARATOR '') AS 'daftar_bulan'
				FROM smis_pr_rkbu a LEFT JOIN smis_pr_drkbu b ON a.id = b.id_rkbu
				WHERE a.prop NOT LIKE 'del' AND a.locked = '1' AND b.prop NOT LIKE 'del'
				GROUP BY b.id_rkbu
			) v
			WHERE " . $filter . "
			ORDER BY id DESC
		";
		$query_count = "
			SELECT COUNT(*)
			FROM (
				SELECT a.*, GROUP_CONCAT(DISTINCT CONCAT('<li>', b.bulan, '</li>') ORDER BY FIELD(b.bulan, 'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'Nopember', 'Desember') SEPARATOR '') AS 'daftar_bulan'
				FROM smis_pr_rkbu a LEFT JOIN smis_pr_drkbu b ON a.id = b.id_rkbu
				WHERE a.prop NOT LIKE 'del' AND a.locked = '1' AND b.prop NOT LIKE 'del'
				GROUP BY b.id_rkbu
			) v
			WHERE " . $filter . "
			ORDER BY id DESC
		";
		$rkbu_dbtable->setPreferredQuery(true, $query_value, $query_count);
		class RKBUDBResponder extends DBResponder {
			public function save() {
				$header_data = $this->postToArray();
				$id['id'] = $_POST['id'];
				//do update header here:
				$result = $this->dbtable->update($header_data, $id);
				$success['type'] = "update";
				if (isset($_POST['detail'])) {
					//do update detail here:
					$detail_dbtable = new DBTable($this->dbtable->get_db(), "smis_pr_drkbu");
					$detail = $_POST['detail'];
					foreach($detail as $d) {
						$detail_data = array();
						$detail_data['f_id'] = $d['f_id'];
						$detail_data['id_rkbu'] = $id['id'];
						$detail_data['nama_barang'] = $d['nama_barang'];
						$detail_data['jenis_barang'] = $d['jenis_barang'];
						$detail_data['jumlah_diajukan'] = $d['jumlah_diajukan'];
						$detail_data['satuan_diajukan'] = $d['satuan_diajukan'];
						$detail_data['jumlah_disetujui'] = $d['jumlah_disetujui'];
						$detail_data['satuan_disetujui'] = $d['satuan_disetujui'];
						$detail_data['bulan'] = $d['bulan'];
						$detail_id['id'] = $d['id'];
						$detail_dbtable->update($detail_data, $detail_id);
					}
				}
				$success['id'] = $id['id'];
				$success['success'] = 1;
				if ($result === false) $success['success'] = 0;
				return $success;
			}
			public function edit() {
				$id = $_POST['id'];
				$header_row = $this->dbtable->get_row("
					SELECT *
					FROM smis_pr_rkbu
					WHERE id = '" . $id . "'
				");
				$data['header'] = $header_row;
				$detail_dbtable = new DBTable($this->dbtable->get_db(), "smis_pr_drkbu");
				$data['detail'] = $detail_dbtable->get_result("
					SELECT *
					FROM smis_pr_drkbu
					WHERE id_rkbu = '" . $id . "' AND prop NOT LIKE 'del'
					ORDER BY FIELD(smis_pr_drkbu.bulan, 'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'Nopember', 'Desember'), smis_pr_drkbu.nama_barang ASC
				");
				return $data;
			}
		}
		$rkbu_dbresponder = new RKBUDBResponder(
			$rkbu_dbtable,
			$rkbu_table,
			$rkbu_adapter
		);
		$data = $rkbu_dbresponder->command($_POST['command']);
		//set_rkbu_status_service:
		if (isset($_POST['push_command'])) {
			class SetRKBUStatusServiceConsumer extends ServiceConsumer {
				public function __construct($db, $f_id, $tanggal_ditinjau, $tinjauan, $disetujui, $detail, $command, $tujuan) {
					$array['id'] = $f_id;
					$array['tanggal_ditinjau'] = $tanggal_ditinjau;
					$array['tinjauan'] = $tinjauan;
					$array['disetujui'] = $disetujui;
					$array['detail'] = json_encode($detail);
					$array['command'] = $command;
					parent::__construct($db, "set_rkbu_status", $array, $tujuan);
				}
				public function proceedResult() {
					$content = array();
					$result = json_decode($this->result,true);
					foreach ($result as $autonomous) {
						foreach ($autonomous as $entity) {
							if ($entity != null || $entity != "")
								return $entity;
						}
					}
					return $content;
				}
			}
			$rkbu_dbtable = new DBTable($db, "smis_pr_rkbu");
			$rkbu_row = $rkbu_dbtable->get_row("
				SELECT *
				FROM smis_pr_rkbu
				WHERE id = '" . $data['content']['id'] . "'
			");
			$drkbu_dbtable = new DBTable($db, "smis_pr_drkbu");
			$drkbu_rows = $drkbu_dbtable->get_result("
				SELECT *
				FROM smis_pr_drkbu
				WHERE id_rkbu = '" . $data['content']['id'] . "'
			");
			$command = "push_" . $_POST['push_command'];
			$set_rkbu_status_service_consumer = new SetRKBUStatusServiceConsumer($db, $rkbu_row->f_id, $rkbu_row->tanggal_ditinjau, $rkbu_row->tinjauan, $rkbu_row->disetujui, $drkbu_rows, $command, $rkbu_row->unit);
			$set_rkbu_status_service_consumer->execute();
		}
		echo json_encode($data);
		return;
	}
	
	$rkbu_modal = new Modal("rkbu_add_form", "smis_form_container", "rkbu");
	$rkbu_modal->setTitle("RKBU");
	$rkbu_modal->setClass(Modal::$FULL_MODEL);
	$id_hidden = new Hidden("rkbu_id", "rkbu_id", "");
	$rkbu_modal->addElement("", $id_hidden);
	$fid_hidden = new Hidden("rkbu_fid", "rkbu_fid", "");
	$rkbu_modal->addElement("", $fid_hidden);
	$unit_text = new Text("rkbu_unit", "rkbu_unit", "");
	$unit_text->setAtribute("disabled", "disabled");
	$rkbu_modal->addElement("Unit", $unit_text);
	$tahun_text = new Text("rkbu_tahun", "rkbu_tahun", "");
	$tahun_text->setAtribute("disabled='disabled'");
	$rkbu_modal->addElement("Tahun", $tahun_text);
	$tanggal_diajukan_text = new Text("rkbu_tanggal_diajukan", "rkbu_tanggal_diajukan", "");
	$tanggal_diajukan_text->setAtribute("disabled='disabled'");
	$rkbu_modal->addElement("Tgl. Diajukan", $tanggal_diajukan_text);
	$tanggal_ditinjau_text = new Text("rkbu_tanggal_ditinjau", "tanggal_ditinjau", date('Y-m-d'));
	$tanggal_ditinjau_text->setClass("mydate");
	$tanggal_ditinjau_text->setAtribute("data-date-format='yyyy-m-d'");
	$rkbu_modal->addElement("Tgl. Ditinjau", $tanggal_ditinjau_text);
	$persetujuan_option = array(
		array(
			'name'	=> "Setuju",
			'value'	=> "sudah"
		),
		array(
			'name'	=> "Belum Ditinjau",
			'value'	=> "belum"
		),
		array(
			'name'	=> "Revisi Belum Ditinjau",
			'value'	=> "revisi"
		)
	);
	$status_select = new Select("rkbu_status", "rkbu_status", $persetujuan_option);
	$rkbu_modal->addElement("Status", $status_select);
	$keterangan_edit_button = new Button("", "", "Ubah");
	$keterangan_edit_button->setAction("smis_summernote_write('rkbu', 'rkbu_keterangan', 'rkbu_add_form')");
	$keterangan_edit_button->setIcon("icon-black " . Button::$icon_pencil);
	$keterangan_edit_button->setIsButton(Button::$ICONIC);
	$keterangan_edit_button->setAtribute("id='rkbu_edit_keterangan'");
	$keterangan_view_button = new Button("", "", "Lihat");
	$keterangan_view_button->setAction("smis_summernote_show('rkbu', 'rkbu_keterangan', 'rkbu_add_form')");
	$keterangan_view_button->setClass("btn-inverse");
	$keterangan_view_button->setIcon("icon-white " . Button::$icon_eye_open);
	$keterangan_view_button->setIsButton(Button::$ICONIC);
	$keterangan_textarea = new TextArea("rkbu_keterangan", "rkbu_keterangan", "");
	$keterangan_textarea->setClass("hide");
	$keterangan_button_group = new ButtonGroup("summernote-button-group noprint");
	$keterangan_button_group->addButton($keterangan_edit_button);
	$keterangan_button_group->addButton($keterangan_textarea);
	$keterangan_button_group->addButton($keterangan_view_button);
	$rkbu_modal->addElement("Keterangan", $keterangan_button_group);
	$tinjauan_edit_button = new Button("", "", "Ubah");
	$tinjauan_edit_button->setAction("smis_summernote_write('rkbu', 'rkbu_tinjauan', 'rkbu_add_form')");
	$tinjauan_edit_button->setIcon("icon-black " . Button::$icon_pencil);
	$tinjauan_edit_button->setIsButton(Button::$ICONIC);
	$tinjauan_edit_button->setAtribute("id='rkbu_edit_tinjauan");
	$tinjauan_view_button = new Button("", "", "Lihat");
	$tinjauan_view_button->setAction("smis_summernote_show('rkbu', 'rkbu_tinjauan', 'rkbu_add_form')");
	$tinjauan_view_button->setClass("btn-inverse");
	$tinjauan_view_button->setIcon("icon-white " . Button::$icon_eye_open);
	$tinjauan_view_button->setIsButton(Button::$ICONIC);
	$tinjauan_textarea = new TextArea("rkbu_tinjauan", "rkbu_tinjauan", "");
	$tinjauan_textarea->setClass("hide");
	$tinjauan_button_group = new ButtonGroup("summernote-button-group noprint");
	$tinjauan_button_group->addButton($tinjauan_edit_button);
	$tinjauan_button_group->addButton($tinjauan_textarea);
	$tinjauan_button_group->addButton($tinjauan_view_button);
	$rkbu_modal->addElement("Tinjauan", $tinjauan_button_group);
	$drkbu_table = new Table(
		array("Bulan", "Barang", "Jumlah", "Disetujui"),
		"",
		null,
		true
	);
	$drkbu_table->setName("drkbu");
	$drkbu_table->setAddButtonEnable(false);
	$drkbu_table->setPrintButtonEnable(false);
	$drkbu_table->setReloadButtonEnable(false);
	$drkbu_table->setFooterVisible(false);
	$rkbu_modal->addBody("drkbu_table", $drkbu_table);
	$rkbu_button = new Button("", "", "Simpan");
	$rkbu_button->setClass("btn-success");
	$rkbu_button->setAtribute("id='rkbu_save'");
	$rkbu_button->setIcon("fa fa-floppy-o");
	$rkbu_button->setIsButton(Button::$ICONIC);
	$rkbu_modal->addFooter($rkbu_button);
	$rkbu_button = new Button("", "", "OK");
	$rkbu_button->setClass("btn-success");
	$rkbu_button->setAtribute("id='rkbu_ok'");
	$rkbu_button->setAction("$($(this).data('target')).smodal('hide')");
	$rkbu_modal->addFooter($rkbu_button);
	
	$drkbu_modal = new Modal("drkbu_add_form", "smis_form_container", "drkbu");
	$drkbu_modal->setTitle("Detail RKBU");
	$id_hidden = new Hidden("drkbu_id", "drkbu_id", "");
	$drkbu_modal->addElement("", $id_hidden);
	$bulan_text = new Text("drkbu_bulan", "drkbu_bulan", "");
	$bulan_text->setAtribute("disabled='disabled'");
	$drkbu_modal->addElement("Bulan", $bulan_text);
	$nama_barang_text = new Text("drkbu_nama_barang", "drkbu_nama_barang", "");
	$nama_barang_text->setAtribute("disabled='disabled'");
	$drkbu_modal->addELement("Nama Barang", $nama_barang_text);
	$jenis_barang_text = new Text("drkbu_jenis_barang", "drkbu_jenis_barang", "");
	$jenis_barang_text->setAtribute("disabled='disabled'");
	$drkbu_modal->addElement("Jenis Barang", $jenis_barang_text);
	$jumlah_diajukan_text = new Text("drkbu_jumlah_diajukan", "drkbu_jumlah_diajukan", "");
	$jumlah_diajukan_text->setAtribute("disabled='disabled'");
	$drkbu_modal->addElement("Jml. Diajukan", $jumlah_diajukan_text);
	$jumlah_disetujui_text = new Text("drkbu_jumlah_disetujui", "drkbu_jumlah_disetujui", "");
	$drkbu_modal->addElement("Jml. Disetujui", $jumlah_disetujui_text);
	$satuan_disetujui_text = new Text("drkbu_satuan_disetujui", "drkbu_satuan_disetujui", "");
	$drkbu_modal->addElement("Satuan", $satuan_disetujui_text);
	$drkbu_button = new Button("", "", "Simpan");
	$drkbu_button->setClass("btn-success");
	$drkbu_button->setAtribute("id='drkbu_save'");
	$drkbu_button->setIcon("fa fa-floppy-o");
	$drkbu_button->setIsButton(Button::$ICONIC);
	$drkbu_modal->addFooter($drkbu_button);
	
	echo $drkbu_modal->getHtml();
	echo $rkbu_modal->getHtml();
	echo $rkbu_table->getHtml();
	echo addJS("framework/smis/js/table_action.js");
	echo addJS("framework/bootstrap/js/bootstrap-datepicker.js");
	echo addCSS("framework/bootstrap/css/datepicker.css");
?>
<script type="text/javascript">
	function RKBUAction(name, page, action, column) {
		this.initialize(name, page, action, column);
	}
	RKBUAction.prototype.constructor = RKBUAction;
	RKBUAction.prototype = new TableAction();
	RKBUAction.prototype.getViewData = function() {
		var data = TableAction.prototype.getViewData.call(this);
		data['search_unit'] = $("#search_unit").val();
		data['search_nomor'] = $("#search_nomor").val();
		data['search_tahun'] = $("#search_tahun").val();
		data['search_bulan'] = $("#search_bulan").val();
		data['search_tanggal_diajukan'] = $("#search_tanggal_diajukan").val();
		data['search_tanggal_ditinjau'] = $("#search_tanggal_ditinjau").val();
		data['search_status'] = $("#search_status").val();
		return data;
	};
	RKBUAction.prototype.edit = function(id) {
		var self = this;
		var data = this.getRegulerData();
		data['command'] = "edit";
		data['id'] = id;
		$.post(
			"",
			data,
			function(response) {
				var json = getContent(response);
				if (json == null) return;
				$("#rkbu_id").val(json.header.id);
				$("#rkbu_fid").val(json.header.f_id);
				$("#rkbu_unit").val(json.header.unit.replace("_", " ").toUpperCase());
				$("#rkbu_tahun").val(json.header.tahun);
				$("#rkbu_tanggal_diajukan").val(json.header.tanggal_diajukan);
				$("#rkbu_keterangan").val(json.header.keterangan);
				$("#rkbu_tinjauan").val(json.header.tinjauan);
				$("#rkbu_edit_keterangan").removeAttr("onclick");
				$("#rkbu_edit_keterangan").removeAttr("disabled");
				$("#rkbu_edit_keterangan").attr("disabled", "disabled");
				$("#rkbu_edit_tinjauan").removeAttr("onclick");
				$("#rkbu_edit_tinjauan").attr("onclick", "smis_summernote_write('rkbu', 'rkbu_tinjauan', 'rkbu_add_form')");
				$("#rkbu_edit_tinjauan").removeAttr("disabled");
				$("#rkbu_tanggal_ditinjau").val(json.header.tanggal_ditinjau);
				$("#rkbu_tanggal_ditinjau").removeAttr("disabled");
				$("#rkbu_status").val(json.header.disetujui);
				$("#rkbu_status").removeAttr("disabled");
				row_id = 0;
				$("tbody#drkbu_list").children().remove();
				for(var i = 0; i < json.detail.length; i++) {
					var drkbu_id = json.detail[i].id;
					var drkbu_f_id = json.detail[i].f_id;
					var drkbu_id_rkbu = json.detail[i].id_rkbu;
					var drkbu_bulan = json.detail[i].bulan;
					var drkbu_nama_barang = json.detail[i].nama_barang;
					var drkbu_jenis_barang = json.detail[i].jenis_barang;
					var drkbu_jumlah_diajukan = json.detail[i].jumlah_diajukan;
					var drkbu_satuan_diajukan = json.detail[i].satuan_diajukan;
					var drkbu_jumlah_disetujui = json.detail[i].jumlah_disetujui;
					var drkbu_satuan_disetujui = json.detail[i].satuan_disetujui;
					var f_disetujui = "-";
					if (Number(drkbu_jumlah_disetujui) != 0 && drkbu_satuan_disetujui != null)
						f_disetujui = drkbu_jumlah_disetujui + " " + drkbu_satuan_disetujui;
					$("tbody#drkbu_list").append(
						"<tr id='data_" + row_id + "'>" +
							"<td style='display: none;' id='data_" + row_id + "_id'>" + drkbu_id + "</td>" +
							"<td style='display: none;' id='data_" + row_id + "_f_id'>" + drkbu_f_id + "</td>" +
							"<td style='display: none;' id='data_" + row_id + "_jenis_barang'>" + drkbu_jenis_barang + "</td>" +
							"<td style='display: none;' id='data_" + row_id + "_jumlah_diajukan'>" + drkbu_jumlah_diajukan + "</td>" +
							"<td style='display: none;' id='data_" + row_id + "_satuan_diajukan'>" + drkbu_satuan_diajukan + "</td>" +
							"<td style='display: none;' id='data_" + row_id + "_jumlah_disetujui'>" + drkbu_jumlah_disetujui + "</td>" +
							"<td style='display: none;' id='data_" + row_id + "_satuan_disetujui'>" + drkbu_satuan_disetujui + "</td>" +
							"<td id='data_" + row_id + "_bulan'>" + drkbu_bulan + "</td>" +
							"<td id='data_" + row_id + "_nama_barang'>" + drkbu_nama_barang + "</td>" +
							"<td id='data_" + row_id + "_f_jumlah'>" + drkbu_jumlah_diajukan + " " + drkbu_satuan_diajukan + "</td>" +
							"<td id='data_" + row_id + "_f_disetujui'>" + f_disetujui + "</td>" +
							"<td>" + 
								"<div class='btn-group noprint'>" +
									"<a href='#' onclick='drkbu.edit(" + row_id + ")' data-content='Ubah' data-toggle='popover' class='input btn btn-warning'>" + 
										"<i class='icon-edit icon-white'></i>" +
									"</a>" +
								"</div>" +
							"</td>" +
						"</tr>"
					);
					row_id++;
				}
				$("#rkbu_add_form").smodal("show");
				$("#modal_alert_rkbu_add_form").html("");
				$(".error_field").removeClass("error_field");
				$(".error_field").removeClass("error_field");
				$("#rkbu_save").show();
				$("#rkbu_save").removeAttr("onclick");
				$("#rkbu_save").attr("onclick", "rkbu.update(" + id + ")");
				$("#rkbu_ok").hide();
			}
		);
	};
	RKBUAction.prototype.validate = function() {
		var valid = true;
		var invalid_msg = "";
		var rkbu_tanggal_ditinjau = $("#rkbu_tanggal_ditinjau").val();
		var rkbu_tinjauan = $("#rkbu_tinjauan").val();
		var rkbu_status = $("#rkbu_status").val();
		$(".error_field").removeClass("error_field");
		if (rkbu_status == "") {
			valid = false;
			invalid_msg += "</br><strong>Status</strong> tidak boleh kosong";
			$("#rkbu_status").addClass("error_field");
		}
		if (rkbu_tinjauan == "") {
			valid = false;
			invalid_msg += "</br><strong>Tinjauan</strong> tidak boleh kosong";
		}
		if (rkbu_tanggal_ditinjau == "") {
			valid = false;
			invalid_msg += "</br><strong>Tgl. Ditinjau</strong> tidak boleh kosong";
			$("#rkbu_tanggal_ditinjau").addClass("error_field");
		}
		if (!valid) {
			$("#modal_alert_rkbu_add_form").html(
				"<div class='alert alert-block alert-danger'>" +
					"<h4>Peringatan</h4>" +
					invalid_msg +
				"</div>"
			);
		}
		return valid;
	};
	RKBUAction.prototype.update = function(id) {
		if (!this.validate()) {
			return;
		}
		$("#rkbu_add_form").smodal("hide");
		showLoading();
		var self = this;
		var data = this.getRegulerData();
		data['command'] = "save";
		data['id'] = id;
		data['f_id'] = $("#rkbu_fid").val();
		data['unit'] = $("#rkbu_unit").val().replace(" ", "_").toLowerCase();
		data['tanggal_diajukan'] = $("#rkbu_tanggal_diajukan").val();
		data['tanggal_ditinjau'] = $("#rkbu_tanggal_ditinjau").val();
		data['keterangan'] = $("#rkbu_keterangan").val();
		data['tinjauan'] = $("#rkbu_tinjauan").val();
		data['locked'] = "1";
		data['disetujui'] = $("#rkbu_status").val();
		var detail = {};
		var nor = $("tbody#drkbu_list").children("tr").length;
		for(var i = 0; i < nor; i++) {
			var d_data = {};
			var prefix = $("tbody#drkbu_list").children('tr').eq(i).prop("id");
			d_data['id'] = $("#" + prefix + "_id").text();
			d_data['f_id'] = $("#" + prefix + "_f_id").text();
			d_data['bulan'] = $("#" + prefix + "_bulan").text();
			d_data['nama_barang'] = $("#" + prefix + "_nama_barang").text();
			d_data['jenis_barang'] = $("#" + prefix + "_jenis_barang").text();
			d_data['jumlah_diajukan'] = $("#" + prefix + "_jumlah_diajukan").text();
			d_data['satuan_diajukan'] = $("#" + prefix + "_satuan_diajukan").text();
			d_data['jumlah_disetujui'] = $("#" + prefix + "_jumlah_disetujui").text();
			d_data['satuan_disetujui'] = $("#" + prefix + "_satuan_disetujui").text();
			if ($("#" + prefix).attr("class") == "deleted") {
				d_data['cmd'] = "delete";
			} else if ($("#" + prefix + "_id").text() == "") {
				d_data['cmd'] = "insert";
			} else {
				d_data['cmd'] = "update";
			}
			detail[i] = d_data;
		}
		data['detail'] = detail;
		var confirm_msg = "";
		if (data['disetujui'] != "belum") {
			data['push_command'] = "permanent";
		}
		$.post(
			"",
			data,
			function(response) {
				var json = getContent(response);
				if (json == null) {
					$("#rkbu_add_form").smodal("show");
				} else {
					self.view();
					rincian_rkbu.view();
				}
				dismissLoading();
			}
		);
	};
	RKBUAction.prototype.return = function(id) {
		var self = this;
		var today = new Date();
		var data = this.getRegulerData();
		data['command'] = "save";
		data['push_command'] = "return";
		data['id'] = id;
		data['tanggal_ditinjau'] = today.getFullYear() + "-" + (Number(today.getMonth())+1) + "-" + today.getDate();
		data['locked'] = "0";
		bootbox.confirm(
			"Anda yakin mengembalikan RKBU ini?",
			function(result) {
				if (result) {
					showLoading();
					$.post(
						"",
						data,
						function() {
							self.view();
							rincian_rkbu.view();
							dismissLoading();
						}
					);
				}
			}
		);
	};
	RKBUAction.prototype.detail = function(id) {
		var self = this;
		var data = this.getRegulerData();
		data['command'] = "edit";
		data['id'] = id;
		$.post(
			"",
			data,
			function(response) {
				var json = getContent(response);
				if (json == null) return;
				$("#rkbu_id").val(json.header.id);
				$("#rkbu_fid").val(json.header.f_id);
				$("#rkbu_unit").val(json.header.unit.replace("_", " ").toUpperCase());
				$("#rkbu_tahun").val(json.header.tahun);
				$("#rkbu_tanggal_diajukan").val(json.header.tanggal_diajukan);
				$("#rkbu_keterangan").val(json.header.keterangan);
				$("#rkbu_tinjauan").val(json.header.tinjauan);
				$("#rkbu_edit_keterangan").removeAttr("onclick");
				$("#rkbu_edit_keterangan").attr("onclick", "smis_summernote_write('rkbu', 'rkbu_keterangan', 'rkbu_add_form')");
				$("#rkbu_edit_keterangan").removeAttr("disabled");
				$("#rkbu_edit_tinjauan").removeAttr("onclick");
				$("#rkbu_edit_tinjauan").removeAttr("disabled");
				$("#rkbu_edit_tinjauan").attr("disabled", "disabled");
				$("#rkbu_tanggal_ditinjau").val(json.header.tanggal_ditinjau);
				$("#rkbu_tanggal_ditinjau").removeAttr("disabled");
				$("#rkbu_tanggal_ditinjau").attr("disabled", "disabled");
				$("#rkbu_status").val(json.header.disetujui);
				$("#rkbu_status").removeAttr("disabled");
				$("#rkbu_status").attr("disabled", "disabled");
				row_id = 0;
				$("tbody#drkbu_list").children().remove();
				for(var i = 0; i < json.detail.length; i++) {
					var drkbu_id = json.detail[i].id;
					var drkbu_f_id = json.detail[i].f_id;
					var drkbu_id_rkbu = json.detail[i].id_rkbu;
					var drkbu_bulan = json.detail[i].bulan;
					var drkbu_nama_barang = json.detail[i].nama_barang;
					var drkbu_jenis_barang = json.detail[i].jenis_barang;
					var drkbu_jumlah_diajukan = json.detail[i].jumlah_diajukan;
					var drkbu_satuan_diajukan = json.detail[i].satuan_diajukan;
					var drkbu_jumlah_disetujui = json.detail[i].jumlah_disetujui;
					var drkbu_satuan_disetujui = json.detail[i].satuan_disetujui;
					var f_disetujui = "-";
					if (Number(drkbu_jumlah_disetujui) != 0 && drkbu_satuan_disetujui != null)
						f_disetujui = drkbu_jumlah_disetujui + " " + drkbu_satuan_disetujui;
					$("tbody#drkbu_list").append(
						"<tr id='data_" + row_id + "'>" +
							"<td style='display: none;' id='data_" + row_id + "_id'>" + drkbu_id + "</td>" +
							"<td style='display: none;' id='data_" + row_id + "_f_id'>" + drkbu_f_id + "</td>" +
							"<td style='display: none;' id='data_" + row_id + "_jenis_barang'>" + drkbu_jenis_barang + "</td>" +
							"<td style='display: none;' id='data_" + row_id + "_jumlah_diajukan'>" + drkbu_jumlah_diajukan + "</td>" +
							"<td style='display: none;' id='data_" + row_id + "_satuan_diajukan'>" + drkbu_satuan_diajukan + "</td>" +
							"<td style='display: none;' id='data_" + row_id + "_jumlah_disetujui'>" + drkbu_jumlah_disetujui + "</td>" +
							"<td style='display: none;' id='data_" + row_id + "_satuan_disetujui'>" + drkbu_satuan_disetujui + "</td>" +
							"<td id='data_" + row_id + "_bulan'>" + drkbu_bulan + "</td>" +
							"<td id='data_" + row_id + "_nama_barang'>" + drkbu_nama_barang + "</td>" +
							"<td id='data_" + row_id + "_f_jumlah'>" + drkbu_jumlah_diajukan + " " + drkbu_satuan_diajukan + "</td>" +
							"<td id='data_" + row_id + "_f_disetujui'>" + f_disetujui + "</td>" +
							"<td></td>" +
						"</tr>"
					);
					row_id++;
				}
				$("#rkbu_add_form").smodal("show");
				$("#modal_alert_rkbu_add_form").html("");
				$(".error_field").removeClass("error_field");
				$("#rkbu_save").hide();
				$("#rkbu_save").removeAttr("onclick");
				$("#rkbu_ok").show();
			}
		);
	};
	RKBUAction.prototype.cancel = function(id) {
		var self = this;
		var data = this.getRegulerData();
		data['command'] = "save";
		data['id'] = id;
		data['disetujui'] = "belum";
		bootbox.confirm(
			"Yakin membatalkan persetujuan RKBU ini?",
			function(result) {
				if (result) {
					showLoading();
					$.post(
						"",
						data,
						function() {
							self.view();
							rincian_rkbu.view();
							dismissLoading();
						}
					);
				}
			}
		);
	};
	
	function DRKBUAction(name, page, action, column) {
		this.initialize(name, page, action, column);
	}
	DRKBUAction.prototype.constructor = DRKBUAction;
	DRKBUAction.prototype = new TableAction();
	DRKBUAction.prototype.edit = function(r_num) {
		var drkbu_id = $("#data_" + r_num + "_id").text();
		var drkbu_bulan = $("#data_" + r_num + "_bulan").text();
		var drkbu_jenis_barang = $("#data_" + r_num + "_jenis_barang").text();
		var drkbu_nama_barang = $("#data_" + r_num + "_nama_barang").text();
		var drkbu_jumlah_diajukan = $("#data_" + r_num + "_jumlah_diajukan").text();
		var drkbu_satuan_diajukan = $("#data_" + r_num + "_satuan_diajukan").text();
		var drkbu_jumlah_disetujui = $("#data_" + r_num + "_jumlah_disetujui").text();
		var drkbu_satuan_disetujui = $("#data_" + r_num + "_satuan_disetujui").text();
		$("#drkbu_id").val(drkbu_id);
		$("#drkbu_bulan").val(drkbu_bulan);
		$("#drkbu_jenis_barang").val(drkbu_jenis_barang);
		$("#drkbu_nama_barang").val(drkbu_nama_barang);
		$("#drkbu_jumlah_diajukan").val(drkbu_jumlah_diajukan + " " + drkbu_satuan_diajukan);
		$("#drkbu_jumlah_disetujui").val(drkbu_jumlah_disetujui);
		$("#drkbu_satuan_disetujui").val(drkbu_satuan_disetujui);
		$("#modal_alert_drkbu_add_form").html("");
		$(".error_field").removeClass("error_field");
		$("#drkbu_save").removeAttr("onclick");
		$("#drkbu_save").attr("onclick", "drkbu.update(" + r_num + ")");
		$("#drkbu_add_form").smodal("show");
	};
	DRKBUAction.prototype.validate = function() {
		var valid = true;
		var invalid_msg = "";
		var drkbu_jumlah_disetujui = $("#drkbu_jumlah_disetujui").val();
		var drkbu_satuan_disetujui = $("#drkbu_satuan_disetujui").val();
		$(".error_field").removeClass("error_field");
		if (drkbu_jumlah_disetujui == "") {
			valid = false;
			invalid_msg = "</br><strong>Jml. Disetujui</strong> tidak boleh kosong";
			$("#drkbu_jumlah_disetujui").addClass("error_field");
		} else if (!is_numeric(drkbu_jumlah_disetujui)) {
			valid = false;
			invalid_msg = "</br><strong>Jml. Disetujui</strong> seharusnya numerik (0-9)";
			$("#drkbu_jumlah_disetujui").addClass("error_field");
		}
		if (drkbu_satuan_disetujui == "") {
			valid = false;
			invalid_msg = "</br><strong>Satuan</strong> tidak boleh kosong";
			$("#drkbu_satuan_disetujui").addClass("error_field");
		}
		if (!valid) {
			$("#modal_alert_drkbu_add_form").html(
				"<div class='alert alert-block alert-danger'>" +
					"<h4>Peringatan</h4>" +
					invalid_msg +
				"</div>"
			);
		}
		return valid;
	};
	DRKBUAction.prototype.update = function(r_num) {
		if (!this.validate()) {
			return;
		}
		$("#data_" + r_num + "_jumlah_disetujui").text($("#drkbu_jumlah_disetujui").val());
		$("#data_" + r_num + "_satuan_disetujui").text($("#drkbu_satuan_disetujui").val());
		$("#data_" + r_num + "_f_disetujui").text($("#drkbu_jumlah_disetujui").val() + " " + $("#drkbu_satuan_disetujui").val());
		$("#drkbu_add_form").smodal("hide");
	};
	DRKBUAction.prototype.delete = function(r_num) {
		var id = $("#data_" + r_num + "_id").text();
		if (id.length == 0) {
			$("#data_" + r_num).remove();
		} else {
			$("#data_" + r_num).attr("style", "display: none;");
			$("#data_" + r_num).attr("class", "deleted");
		}
	};
	
	var rkbu;
	var drkbu;
	var row_id;
	$(document).ready(function() {
		$('[data-toggle="popover"]').popover({
			trigger: 'hover',
			'placement': 'top'
		});
		$(".mydate").datepicker();
		var drkbu_columns = new Array("id", "id_rkbu", "bulan", "jumlah_diajukan", "satuan_diajukan", "jumlah_disetujui", "satuan_disetujui");
		drkbu = new DRKBUAction(
			"drkbu",
			"perencanaan",
			"daftar_rkbu",
			drkbu_columns
		);
		var rkbu_columns = new Array("id", "f_id", "unit", "tahun", "tanggal_diajukan", "tanggal_ditinjau", "keterangan", "locked", "disetujui");
		rkbu = new RKBUAction(
			"rkbu",
			"perencanaan",
			"daftar_rkbu",
			rkbu_columns
		);
		rkbu.view();
		$("#table_rkbu tfoot tr .input-append .btn-group").hide();
		$(".search").on("keyup", function(e) {
			if (e.which == 13) {
				rkbu.view();
			}
		});
		$(".search_select").on("change", function(e) {
			rkbu.view();
		});
	});
</script>
<style type="text/css">
	tr.rkbu_header td input, tr.reghead td select {
		width:100%;
		max-width:100% !important;
	}
</style>