<?php
	require_once 'smis-base/smis-include-service-consumer.php';
	global $db;
	
	$form = new Form("abc_form", "", "Perencanaan : Analisis Pengendalian Stok Obat (ABC)");
	$tanggal_awal_text = new Text("abc_tanggal_awal", "abc_tanggal_awal", date("Y-m-01"));
	$tanggal_awal_text->setClass("mydate");
	$tanggal_awal_text->setAtribute("data-date-format='yyyy-mm-dd'");
	$form->addElement("Tanggal Awal", $tanggal_awal_text);
	$tanggal_akhir_text = new Text("abc_tanggal_akhir", "abc_tanggal_akhir", date("Y-m-d"));
	$tanggal_akhir_text->setClass("mydate");
	$tanggal_akhir_text->setAtribute("data-date-format='yyyy-mm-dd'");
	$form->addElement("Tanggal Akhir", $tanggal_akhir_text);
	$show_button = new Button("", "", "Tampilkan");
	$show_button->setClass("btn-primary");
	$show_button->setIcon("icon-white icon-repeat");
	$show_button->setIsButton(Button::$ICONIC);
	$show_button->setAtribute("id='btn_show'");
	$show_button->setAction("abc.view()");
	$history_button = new Button("", "", "Riwayat");
	$history_button->setClass("btn-info");
	$history_button->setIcon("fa fa-history");
	$history_button->setIsButton(Button::$ICONIC);
	$history_button->setAtribute("id='btn_history'");
	$history_button->setAction("abc.history()");
	$print_button = new Button("", "", "Cetak");
	$print_button->setClass("btn-inverse");
	$print_button->setIcon("icon-white icon-print");
	$print_button->setIsButton(Button::$ICONIC);
	$print_button->setAtribute("id='btn_print'");
	$print_button->setAction("abc.print()");
	$btn_group = new ButtonGroup("noprint");
	$btn_group->addButton($show_button);
	$btn_group->addButton($history_button);
	$btn_group->addButton($print_button);
	$form->addElement("", $btn_group);
	
	$table = new Table(
		array("No.", "Nama Obat", "Jenis Obat", "Jml. Beli", "Nominal Investasi", "Persentase Investasi", "Kelas"),
		"",
		null,
		true
	);
	$table->setName("abc");
	$table->setAction(false);
	$table->setFooterVisible(false);
	$table->setHeaderVisible(false);
	$table->addHeader("after", "
		<tr class='inverse'>
			<th rowspan='2' style='vertical-align: middle !important;'>
				<center><small>No.</small></center>
			</th>
			<th rowspan='2' style='vertical-align: middle !important;'>
				<center><small>Nama Obat</small></center>
			</th>
			<th rowspan='2' style='vertical-align: middle !important;'>
				<center><small>Jenis Obat</small></center>
			</th>
			<th rowspan='2' style='vertical-align: middle !important;'>
				<center><small>Jumlah Pembelian <div id='rentang_dinamis'></div></small></center>
			</th>
			<th colspan='3' style='vertical-align: middle !important;'>
				<center><small>Nilai Investasi</small></center>
			</th>
			<th rowspan='2' style='vertical-align: middle !important;'>
				<center><small>Kelas</small></center>
			</th>
		</tr>
		<tr class='inverse'>
			<th style='vertical-align: middle !important;'>
				<center><small>Nominal</small></center>
			</th>
			<th style='vertical-align: middle !important;'>
				<center><small>Persentase</small></center>
			</th>
			<th style='vertical-align: middle !important;'>
				<center><small>Persentase Kumulatif</small></center>
			</th>
		</tr>
	");
	
	if (isset($_POST['command'])) {
		if ($_POST['command'] == "get_jumlah") {
			$params = array();
			$params['tanggal_awal'] = $_POST['tanggal_awal'];
			$params['tanggal_akhir'] = $_POST['tanggal_akhir'];
			$service = new ServiceConsumer($db, "get_abc_jumlah_master_obat", $params, "gudang_farmasi");
			$service->setMode(ServiceConsumer::$CLEAN_BOTH);
			$content = $service->execute()->getContent();
			$data = array();
			$data['jumlah'] = $content[0];
			$data['timestamp'] = date("d-m-Y H:i:s");
			echo json_encode($data);
		} else if ($_POST['command'] == "get_info") {
			$timestamp_start = date("d-m-Y, H:i:s");
			$row_num = $_POST['row_num'];
			$params = array();
			$params['row_num'] = $row_num;
			$params['tanggal_awal'] = $_POST['tanggal_awal'];
			$params['tanggal_akhir'] = $_POST['tanggal_akhir'];
			$service = new ServiceConsumer($db, "get_abc_master_obat", $params, "gudang_farmasi");
			$service->setMode(ServiceConsumer::$CLEAN_BOTH);
			$content = $service->execute()->getContent();
			$nama_obat = $content[0];
			$nama_jenis_obat = $content[1];
			
			$params = array();
			$params['nama_obat'] = $nama_obat;
			$params['nama_jenis_obat'] = $nama_jenis_obat;
			$params['tanggal_awal'] = $_POST['tanggal_awal'];
			$params['tanggal_akhir'] = $_POST['tanggal_akhir'];
			$service = new ServiceConsumer($db, "get_abc_info", $params, "gudang_farmasi");
			$service->setMode(ServiceConsumer::$CLEAN_BOTH);
			$content = $service->execute()->getContent();
			$nominal = $content[0];
			$jumlah = $content[1];
			
			$data = array();
			$data['nama_obat'] = $nama_obat;
			$data['jenis_obat'] = $nama_jenis_obat;
			$data['timestamp_start'] = $timestamp_start;
			$timestamp_end = date("d-m-Y, H:i:s");
			$data['timestamp_end'] = $timestamp_end;
			$durasi = strtotime($timestamp_end) - strtotime($timestamp_start);
			$data['durasi'] = $durasi . " detik";
			$data['element'] = "
				<tr id='obat_" . $row_num . "'>
					<td id='obat_" . $row_num . "_nomor'><small>" . ($row_num + 1) . "</small></td>
					<td><small>" . $nama_obat . "</small></td>
					<td><small>" . $nama_jenis_obat . "</small></td>
					<td style='text-align: right !important;'><small>" . $jumlah . "</small></td>
					<td style='display: none;' id='obat_" . $row_num . "_nominal_investasi'>" . $nominal . "</td>
					<td><small>" . ArrayAdapter::format("money Rp. ", $nominal) . "</small></td>
					<td id='obat_" . $row_num . "_persentase_investasi' style='text-align: right !important;'><small>-</small></td>
					<td id='obat_" . $row_num . "_persentase_kumulatif' style='text-align: right !important;'><small>-</small></td>
					<td id='obat_" . $row_num . "_kelas' style='text-align: right !important;'><small>-</small></td>
				</tr>
			";
			echo json_encode($data);
		}
		return;
	}
	
	$loading_bar = new LoadingBar("loading_bar", "");
	$button = new Button("", "", "Batal");
	$button->addClass("btn-primary");
	$button->setIsButton(Button::$ICONIC_TEXT);
	$button->setIcon("fa fa-close");
	$button->setAction("abc.cancel()");
	$loading_modal = new Modal("abc_modal", "", "Proses..");
	$loading_modal->addHtml($loading_bar->getHtml(), "after");
	$loading_modal->addFooter($button);
	
	$history_modal = new Modal("history_modal", "", "Riwayat Penarikan Data");
	$history_modal->setClass(Modal::$HALF_MODEL);
	$history_table = new Table(
		array("No.", "Obat", "Jenis", "Waktu Penarikan", "Durasi"),
		"",
		null,
		true
	);
	$history_table->setName("history");
	$history_table->setAction(false);
	$history_table->setFooterVisible(false);
	$history_modal->addHtml($history_table->getHtml(), "after");
	
	echo $history_modal->getHtml();
	echo $loading_modal->getHtml();
	echo $form->getHtml();
	echo "<div id='table_content'>";
	echo $table->getHtml();
	echo "</div>";
	echo "<div id='info'></div>";
	echo addJS("framework/smis/js/table_action.js");
	echo addJS("framework/bootstrap/js/bootstrap-datepicker.js");
	echo addCSS("framework/bootstrap/css/datepicker.css");
	echo addJS ("base-js/smis-base-loading.js");
?>
<script type="text/javascript">
	function ABCAction(name, page, action, column) {
		this.initialize(name, page, action, column);
	}
	ABCAction.prototype.constructor = ABCAction;
	ABCAction.prototype = new TableAction();
	ABCAction.prototype.view = function() {
		if ($("#abc_tanggal_awal").val() == "" || $("#abc_tanggal_akhir").val() == "") {
			return;
		}
		FINISHED = false;
		$("#abc_list").html("");
		$("#history_list").html("");
		$("#info").html("");
		var self = this;
		$("#loading_bar").sload("true", "Harap ditunggu...", 0);
		$("#abc_modal").smodal("show");
		var data = this.getRegulerData();
		data['command'] = "get_jumlah";
		data['tanggal_awal'] = $("#abc_tanggal_awal").val();
		data['tanggal_akhir'] = $("#abc_tanggal_akhir").val();
		$.post(
			"",
			data,
			function(response) {
				var json = JSON.parse(response);
				self.fill_html(0, json.jumlah);
			}
		);
	};
	ABCAction.prototype.fill_html = function(row_num, limit) {
		if (FINISHED || row_num == limit) {
			if (FINISHED == false && row_num == limit) {
				this.finalize_html(limit);
			} else {
				$("#abc_modal").smodal("hide");
				$("#info").html(
					"<div class='alert alert-block alert-inverse'>" +
						 "<center><strong>PROSES DIBATALKAN</strong></center>" +
					 "</div>"
				);
			}
			return;
		}
		var self = this;
		var data = this.getRegulerData();
		data['command'] = "get_info";
		data['row_num'] = row_num;
		data['tanggal_awal'] = $("#abc_tanggal_awal").val();
		data['tanggal_akhir'] = $("#abc_tanggal_akhir").val();
		$.post(
			"",
			data,
			function(response) {
				var json = JSON.parse(response);
				$("#history_list").append(
					"<tr>" +
						"<td><small>" + (row_num+1) + "</small></td>" +
						"<td><small>" + json.nama_obat + "</small></td>" +
						"<td><small>" + json.jenis_obat + "</small></td>" +
						"<td><small>" + json.timestamp_start + " s/d " + json.timestamp_end + "</small></td>" +
						"<td><small>" + json.durasi + "</small></td>" +
					"</tr>"
				);
				$("#abc_list").append(json.element);
				$("#loading_bar").sload("true", json.nama_obat + " - " + json.jenis_obat + " (" + (row_num + 1) + " / " + limit + ")", (row_num+1) * 100 / limit - 1);
				self.fill_html(row_num + 1, limit);
			}
		);
	};
	ABCAction.prototype.finalize_html = function(limit) {
		$("#loading_bar").sload("true", "Finalisasi...", 100);
		var total = 0;
		for (var i = 0; i < limit; i++) {
			var subtotal = parseFloat($("#obat_" + i + "_nominal_investasi").text());
			total += subtotal;
		}
		//persentase nilai investasi:
		for (var i = 0; i < limit; i++) {
			var subtotal = parseFloat($("#obat_" + i + "_nominal_investasi").text());
			var persentase = (subtotal * 100 / total).toFixed(2);
			$("#obat_" + i + "_persentase_investasi").html("<small>" + persentase + "</small>");
		}
		//sorting:
		var rows = $('#abc_list tr').get();
		rows.sort(function(a, b) {
			var persentase_investasi_a = parseFloat($(a).children("td").eq(6).children("small").html());
			var persentase_investasi_b = parseFloat($(b).children("td").eq(6).children("small").html());
			var nominal_investasi_a = parseFloat($(a).children("td").eq(4).html());
			var nominal_investasi_b = parseFloat($(b).children("td").eq(4).html());
			var jumlah_a = parseFloat($(a).children("td").eq(3).html());
			var jumlah_b = parseFloat($(b).children("td").eq(3).html());
			if(persentase_investasi_a < persentase_investasi_b) {
				return 1;
			}
			if(persentase_investasi_a > persentase_investasi_b) {
				return -1;
			}
			if(nominal_investasi_a < nominal_investasi_b) {
				return 1;
			}
			if(nominal_investasi_a > nominal_investasi_b) {
				return -1;
			}
			if(jumlah_a > jumlah_b) {
				return 1;
			}
			if(jumlah_a < jumlah_b) {
				return -1;
			}
			return 0;
		});

		$.each(rows, function(index, row) {
			$('#abc_list').append(row);
		});
		//update kumulatif + kelas:
		var kumulatif = 0;
		for (var i = 0; i < limit; i++) {
			var prefix = $("#abc_list tr").eq(i).attr("id");
			$("#" + prefix + "_nomor small").html(i + 1);
			if (kumulatif <= 70)
				$("#" + prefix + "_kelas small").html("A");
			else if (kumulatif <= 90)
				$("#" + prefix + "_kelas small").html("B");
			else
				$("#" + prefix + "_kelas small").html("C");
			kumulatif = (parseFloat(kumulatif) + parseFloat($("#" + prefix + "_persentase_investasi small").text())).toFixed(2);
			$("#" + prefix + "_persentase_kumulatif small").html(kumulatif);
		}
		$("#info").html(
			"<div class='alert alert-block alert-info'>" +
				 "<center><strong>PROSES BERHASIL</strong></center>" +
			 "</div>"
		);
		$("#abc_modal").smodal("hide");
	};
	ABCAction.prototype.history = function() {
		$("#history_modal").smodal("show");
	};
	ABCAction.prototype.cancel = function() {
		FINISHED = true;
	};
	
	var abc;
	var FINISHED;
	$(document).ready(function() {
		$('.mydate').datepicker();
		$("#abc_modal").on("show", function() {
			$("a.close").hide();
		});
		$("#history_modal").on("show", function() {
			$("a.close").show();
		});
		abc = new ABCAction(
			"abc",
			"perencanaan",
			"rp_abc",
			new Array()
		);
		$(document).keyup(function(e) {
			if (e.which == 27) {
				FINISHED = true;
			}
		});
	});
</script>