<?php
	require_once ("smis-framework/smis/interface/LockerInterface.php");
	require_once ("smis-framework/smis/database/DuplicateResponder.php");
    require_once ("smis-base/smis-include-service-consumer.php");
	global $db;

	$table = new Table(
		array("No.", "ID-Data", "Kode", "Nama", "Jenis", "Kode Acc. Nilai Persediaan", "Kode Acc. HPP", "Kode Acc. Pendapatan", "Kode Acc. Piutang", "Kode Acc. Diskon Penjualan", "Kode Acc. PPn Keluar"),
		"Perencanaan : Mapping Acc. Obat dan Alkes",
		null,
		true
	);
	$table->setName("mapping_acc_barang_inventaris");
	$table->setAddButtonEnable(false);
	$table->setDelButtonEnable(false);

	$kode_ppn_keluar_table = new Table(
		array("No.", "Kode Acc.", "Label"),
		"",
		null,
		true
	);
	$kode_ppn_keluar_table->setName("kode_ppn_keluar");
	$kode_ppn_keluar_table->setModel(Table::$SELECT);
	$kode_ppn_keluar_adapter = new SimpleAdapter(true, "No.");
	$kode_ppn_keluar_adapter->add("Kode Acc.", "nomor");
	$kode_ppn_keluar_adapter->add("Label", "nama");
	$kode_ppn_keluar_service_responder = new ServiceResponder(
		$db,
		$kode_ppn_keluar_table,
		$kode_ppn_keluar_adapter,
		"get_account",
		"accounting"
	);

	$kode_nilai_persediaan_table = new Table(
		array("No.", "Kode Acc.", "Label"),
		"",
		null,
		true
	);
	$kode_nilai_persediaan_table->setName("kode_nilai_persediaan");
	$kode_nilai_persediaan_table->setModel(Table::$SELECT);
	$kode_nilai_persediaan_adapter = new SimpleAdapter(true, "No.");
	$kode_nilai_persediaan_adapter->add("Kode Acc.", "nomor");
	$kode_nilai_persediaan_adapter->add("Label", "nama");
	$kode_nilai_persediaan_service_responder = new ServiceResponder(
		$db,
		$kode_nilai_persediaan_table,
		$kode_nilai_persediaan_adapter,
		"get_account",
		"accounting"
	);

	$kode_hpp_table = new Table(
		array("No.", "Kode Acc.", "Label"),
		"",
		null,
		true
	);
	$kode_hpp_table->setName("kode_hpp");
	$kode_hpp_table->setModel(Table::$SELECT);
	$kode_hpp_adapter = new SimpleAdapter(true, "No.");
	$kode_hpp_adapter->add("Kode Acc.", "nomor");
	$kode_hpp_adapter->add("Label", "nama");
	$kode_hpp_service_responder = new ServiceResponder(
		$db,
		$kode_hpp_table,
		$kode_hpp_adapter,
		"get_account",
		"accounting"
	);

	$kode_pendapatan_table = new Table(
		array("No.", "Kode Acc.", "Label"),
		"",
		null,
		true
	);
	$kode_pendapatan_table->setName("kode_pendapatan");
	$kode_pendapatan_table->setModel(Table::$SELECT);
	$kode_pendapatan_adapter = new SimpleAdapter(true, "No.");
	$kode_pendapatan_adapter->add("Kode Acc.", "nomor");
	$kode_pendapatan_adapter->add("Label", "nama");
	$kode_pendapatan_service_responder = new ServiceResponder(
		$db,
		$kode_pendapatan_table,
		$kode_pendapatan_adapter,
		"get_account",
		"accounting"
	);

	$kode_diskon_penjualan_table = new Table(
		array("No.", "Kode Acc.", "Label"),
		"",
		null,
		true
	);
	$kode_diskon_penjualan_table->setName("kode_diskon_penjualan");
	$kode_diskon_penjualan_table->setModel(Table::$SELECT);
	$kode_diskon_penjualan_adapter = new SimpleAdapter(true, "No.");
	$kode_diskon_penjualan_adapter->add("Kode Acc.", "nomor");
	$kode_diskon_penjualan_adapter->add("Label", "nama");
	$kode_diskon_penjualan_service_responder = new ServiceResponder(
		$db,
		$kode_diskon_penjualan_table,
		$kode_diskon_penjualan_adapter,
		"get_account",
		"accounting"
	);

	$kode_piutang_table = new Table(
		array("No.", "Kode Acc.", "Label"),
		"",
		null,
		true
	);
	$kode_piutang_table->setName("kode_piutang");
	$kode_piutang_table->setModel(Table::$SELECT);
	$kode_piutang_adapter = new SimpleAdapter(true, "No.");
	$kode_piutang_adapter->add("Kode Acc.", "nomor");
	$kode_piutang_adapter->add("Label", "nama");
	$kode_piutang_service_responder = new ServiceResponder(
		$db,
		$kode_piutang_table,
		$kode_piutang_adapter,
		"get_account",
		"accounting"
	);

	$super_command = new SuperCommand();
	$super_command->addResponder("kode_nilai_persediaan", $kode_nilai_persediaan_service_responder);
	$super_command->addResponder("kode_hpp", $kode_hpp_service_responder);
	$super_command->addResponder("kode_pendapatan", $kode_pendapatan_service_responder);
	$super_command->addResponder("kode_diskon_penjualan", $kode_diskon_penjualan_service_responder);
	$super_command->addResponder("kode_piutang", $kode_piutang_service_responder);
	$super_command->addResponder("kode_ppn_keluar", $kode_ppn_keluar_service_responder);
	$init = $super_command->initialize();
	if ($init != null) {
		echo $init;
		return;
	}
	
	if (isset($_POST['command'])) {
		$adapter = new SimpleAdapter(true, "No.");
		$adapter->add("ID-Data", "id", "digit6");
		$adapter->add("Kode", "kode");
		$adapter->add("Nama", "nama");
		$adapter->add("Jenis", "nama_jenis_barang");
		$adapter->add("Kode Acc. Nilai Persediaan", "acc_kode_nilai_persediaan");
		$adapter->add("Kode Acc. HPP", "acc_kode_hpp");
		$adapter->add("Kode Acc. Pendapatan", "acc_kode_pendapatan");
		$adapter->add("Kode Acc. Piutang", "acc_kode_piutang");
		$adapter->add("Kode Acc. Diskon Penjualan", "acc_kode_diskon_penjualan");
		$adapter->add("Kode Acc. PPn Keluar", "acc_kode_ppn_keluar");
		$dbtable = new DBTable($db, "smis_pr_barang");
		$dbtable->setCustomKriteria(" (medis = 1 AND inventaris = 1) OR (medis = 0) ");
		$dbtable->setOrder(" id DESC ");
		$dbresponder = new DuplicateResponder(
			$dbtable,
			$table,
			$adapter
		);
		$data = $dbresponder->command($_POST['command']);
		echo json_encode($data);
		return;
	}
	
	$modal = new Modal("mapping_acc_barang_inventaris_add_form", "smis_form_container", "mapping_acc_barang_inventaris");
	$modal->setTitle("Data Barang dan inventaris");
	$id_hidden = new Hidden("mapping_acc_barang_inventaris_id", "mapping_acc_barang_inventaris_id", "");
	$modal->addElement("", $id_hidden);
	$nama_text = new Text("mapping_acc_barang_inventaris_nama", "mapping_acc_barang_inventaris_nama", "");
	$nama_text->setAtribute("disabled='disabled'");
	$modal->addElement("Nama", $nama_text);
	$id_jenis_hidden = new Hidden("mapping_acc_barang_inventaris_id_jenis_barang", "mapping_acc_barang_inventaris_id_jenis_barang", "");
	$modal->addElement("", $id_jenis_hidden);
	$jenis_text = new Text("mapping_acc_barang_inventaris_nama_jenis_barang", "mapping_acc_barang_inventaris_nama_jenis_barang", "");
	$jenis_text->setAtribute("disabled='disabled'");
	$modal->addElement("Jenis Obat", $jenis_text);
	$kode_text = new Text("mapping_acc_barang_inventaris_kode", "mapping_acc_barang_inventaris_kode", "");
	$kode_text->setAtribute("disabled='disabled'");
	$modal->addElement("Kode", $kode_text);
	$acc_kode_nilai_persediaan_text = new Text("mapping_acc_barang_inventaris_acc_kode_nilai_persediaan", "mapping_acc_barang_inventaris_acc_kode_nilai_persediaan", "");
	$acc_kode_nilai_persediaan_text->setAtribute("disabled='disabled'");
	$acc_kode_nilai_persediaan_text->setClass("smis-one-option-input");
	$acc_kode_nilai_persediaan_button = new Button("", "", "Pilih");
	$acc_kode_nilai_persediaan_button->setAtribute("id='acc_kode_nilai_persediaan_btn'");
	$acc_kode_nilai_persediaan_button->setClass("btn-info");
	$acc_kode_nilai_persediaan_button->setIsButton(Button::$ICONIC);
	$acc_kode_nilai_persediaan_button->setIcon("icon-white ".Button::$icon_list_alt);
	$acc_kode_nilai_persediaan_button->setAction("kode_nilai_persediaan.chooser('kode_nilai_persediaan', 'kode_nilai_persediaan_button', 'kode_nilai_persediaan', kode_nilai_persediaan)");
	$acc_kode_nilai_persediaan_input_group = new InputGroup("");
	$acc_kode_nilai_persediaan_input_group->addComponent($acc_kode_nilai_persediaan_text);
	$acc_kode_nilai_persediaan_input_group->addComponent($acc_kode_nilai_persediaan_button);
	$modal->addElement("KA. Persediaan", $acc_kode_nilai_persediaan_input_group);
	$acc_kode_hpp_text = new Text("mapping_acc_barang_inventaris_acc_kode_hpp", "mapping_acc_barang_inventaris_acc_kode_hpp", "");
	$acc_kode_hpp_text->setAtribute("disabled='disabled'");
	$acc_kode_hpp_text->setClass("smis-one-option-input");
	$acc_kode_hpp_button = new Button("", "", "Pilih");
	$acc_kode_hpp_button->setAtribute("id='acc_kode_hpp_btn'");
	$acc_kode_hpp_button->setClass("btn-info");
	$acc_kode_hpp_button->setIsButton(Button::$ICONIC);
	$acc_kode_hpp_button->setIcon("icon-white ".Button::$icon_list_alt);
	$acc_kode_hpp_button->setAction("kode_hpp.chooser('kode_hpp', 'kode_hpp_button', 'kode_hpp', kode_hpp)");
	$acc_kode_hpp_input_group = new InputGroup("");
	$acc_kode_hpp_input_group->addComponent($acc_kode_hpp_text);
	$acc_kode_hpp_input_group->addComponent($acc_kode_hpp_button);
	$modal->addElement("KA. HPP", $acc_kode_hpp_input_group);
	$acc_kode_pendapatan_text = new Text("mapping_acc_barang_inventaris_acc_kode_pendapatan", "mapping_acc_barang_inventaris_acc_kode_pendapatan", "");
	$acc_kode_pendapatan_text->setAtribute("disabled='disabled'");
	$acc_kode_pendapatan_text->setClass("smis-one-option-input");
	$acc_kode_pendapatan_button = new Button("", "", "Pilih");
	$acc_kode_pendapatan_button->setAtribute("id='acc_kode_pendapatan_btn'");
	$acc_kode_pendapatan_button->setClass("btn-info");
	$acc_kode_pendapatan_button->setIsButton(Button::$ICONIC);
	$acc_kode_pendapatan_button->setIcon("icon-white ".Button::$icon_list_alt);
	$acc_kode_pendapatan_button->setAction("kode_pendapatan.chooser('kode_pendapatan', 'kode_pendapatan_button', 'kode_pendapatan', kode_pendapatan)");
	$acc_kode_pendapatan_input_group = new InputGroup("");
	$acc_kode_pendapatan_input_group->addComponent($acc_kode_pendapatan_text);
	$acc_kode_pendapatan_input_group->addComponent($acc_kode_pendapatan_button);
	$modal->addElement("KA. Pendapatan", $acc_kode_pendapatan_input_group);
	$acc_kode_piutang_text = new Text("mapping_acc_barang_inventaris_acc_kode_piutang", "mapping_acc_barang_inventaris_acc_kode_piutang", "");
	$acc_kode_piutang_text->setAtribute("disabled='disabled'");
	$acc_kode_piutang_text->setClass("smis-one-option-input");
	$acc_kode_piutang_button = new Button("", "", "Pilih");
	$acc_kode_piutang_button->setAtribute("id='acc_kode_piutang_btn'");
	$acc_kode_piutang_button->setClass("btn-info");
	$acc_kode_piutang_button->setIsButton(Button::$ICONIC);
	$acc_kode_piutang_button->setIcon("icon-white ".Button::$icon_list_alt);
	$acc_kode_piutang_button->setAction("kode_piutang.chooser('kode_piutang', 'kode_piutang_button', 'kode_piutang', kode_piutang)");
	$acc_kode_piutang_input_group = new InputGroup("");
	$acc_kode_piutang_input_group->addComponent($acc_kode_piutang_text);
	$acc_kode_piutang_input_group->addComponent($acc_kode_piutang_button);
	$modal->addElement("KA. Piutang", $acc_kode_piutang_input_group);
	$acc_kode_diskon_penjualan_text = new Text("mapping_acc_barang_inventaris_acc_kode_diskon_penjualan", "mapping_acc_barang_inventaris_acc_kode_diskon_penjualan", "");
	$acc_kode_diskon_penjualan_text->setAtribute("disabled='disabled'");
	$acc_kode_diskon_penjualan_text->setClass("smis-one-option-input");
	$acc_kode_diskon_penjualan_button = new Button("", "", "Pilih");
	$acc_kode_diskon_penjualan_button->setAtribute("id='acc_kode_diskon_penjualan_btn'");
	$acc_kode_diskon_penjualan_button->setClass("btn-info");
	$acc_kode_diskon_penjualan_button->setIsButton(Button::$ICONIC);
	$acc_kode_diskon_penjualan_button->setIcon("icon-white ".Button::$icon_list_alt);
	$acc_kode_diskon_penjualan_button->setAction("kode_diskon_penjualan.chooser('kode_diskon_penjualan', 'kode_diskon_penjualan_button', 'kode_diskon_penjualan', kode_diskon_penjualan)");
	$acc_kode_diskon_penjualan_input_group = new InputGroup("");
	$acc_kode_diskon_penjualan_input_group->addComponent($acc_kode_diskon_penjualan_text);
	$acc_kode_diskon_penjualan_input_group->addComponent($acc_kode_diskon_penjualan_button);
	$modal->addElement("KA. Disk. Jual", $acc_kode_diskon_penjualan_input_group);
	$acc_kode_ppn_keluar_text = new Text("mapping_acc_barang_inventaris_acc_kode_ppn_keluar", "mapping_acc_barang_inventaris_acc_kode_ppn_keluar", "");
	$acc_kode_ppn_keluar_text->setAtribute("disabled='disabled'");
	$acc_kode_ppn_keluar_text->setClass("smis-one-option-input");
	$acc_kode_ppn_keluar_button = new Button("", "", "Pilih");
	$acc_kode_ppn_keluar_button->setAtribute("id='acc_kode_ppn_keluar_btn'");
	$acc_kode_ppn_keluar_button->setClass("btn-info");
	$acc_kode_ppn_keluar_button->setIsButton(Button::$ICONIC);
	$acc_kode_ppn_keluar_button->setIcon("icon-white ".Button::$icon_list_alt);
	$acc_kode_ppn_keluar_button->setAction("kode_ppn_keluar.chooser('kode_ppn_keluar', 'kode_ppn_keluar_button', 'kode_ppn_keluar', kode_ppn_keluar)");
	$acc_kode_ppn_keluar_input_group = new InputGroup("");
	$acc_kode_ppn_keluar_input_group->addComponent($acc_kode_ppn_keluar_text);
	$acc_kode_ppn_keluar_input_group->addComponent($acc_kode_ppn_keluar_button);
	$modal->addElement("KA. PPn Jual", $acc_kode_ppn_keluar_input_group);
	
	$save_button = new Button("", "", "Simpan");
	$save_button->setClass("btn-success");
	$save_button->setAction("mapping_acc_barang_inventaris.save()");
	$save_button->setIcon("fa fa-floppy-o");
	$save_button->setIsButton(Button::$ICONIC);
	$modal->addFooter($save_button);
	
	echo $modal->getHtml();
	echo $table->getHtml();
	echo addJS("framework/smis/js/table_action.js");
	echo addJS("perencanaan/js/mapping_acc_barang_inventaris_kode_nilai_persediaan_action.js", false);
	echo addJS("perencanaan/js/mapping_acc_barang_inventaris_kode_hpp_action.js", false);
	echo addJS("perencanaan/js/mapping_acc_barang_inventaris_kode_pendapatan_action.js", false);
	echo addJS("perencanaan/js/mapping_acc_barang_inventaris_kode_diskon_penjualan_action.js", false);
	echo addJS("perencanaan/js/mapping_acc_barang_inventaris_kode_piutang_action.js", false);
	echo addJS("perencanaan/js/mapping_acc_barang_inventaris_kode_ppn_keluar_action.js", false);
	echo addJS("perencanaan/js/mapping_acc_barang_inventaris.js", false);
?>