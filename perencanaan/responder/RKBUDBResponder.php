<?php
	require_once("smis-base/smis-include-duplicate.php");
	
	class RKBUDBResponder extends DuplicateResponder {
		public function save() {
			$header_data = $this->postToArray();
			$id['id'] = $_POST['id'];
			//do update header here:
			$result = $this->dbtable->update($header_data, $id);
			$success['type'] = "update";
			if (isset($_POST['detail'])) {
				//do update detail here:
				$detail_dbtable = new DBTable($this->dbtable->get_db(), "smis_pr_drkbu");
				$detail = $_POST['detail'];
				foreach($detail as $d) {
					$detail_data = array();
					$detail_data['f_id'] = $d['f_id'];
					$detail_data['id_rkbu'] = $id['id'];
					$detail_data['nama_barang'] = $d['nama_barang'];
					$detail_data['jenis_barang'] = $d['jenis_barang'];
					$detail_data['jumlah_diajukan'] = $d['jumlah_diajukan'];
					$detail_data['satuan_diajukan'] = $d['satuan_diajukan'];
					$detail_data['jumlah_disetujui'] = $d['jumlah_disetujui'];
					$detail_data['satuan_disetujui'] = $d['satuan_disetujui'];
					$detail_data['bulan'] = $d['bulan'];
					$detail_data['autonomous'] = "[".$this->autonomous."]";
			        $detail_data['duplicate'] = 0;
			        $detail_data['time_updated'] = date("Y-m-d H:i:s");
					$detail_id['id'] = $d['id'];
					$detail_dbtable->update($detail_data, $detail_id);
				}
			}
			$success['id'] = $id['id'];
			$success['success'] = 1;
			if ($result === false) $success['success'] = 0;
			return $success;
		}
		public function edit() {
			$id = $_POST['id'];
			$header_row = $this->dbtable->get_row("
				SELECT *
				FROM smis_pr_rkbu
				WHERE id = '" . $id . "'
			");
			$data['header'] = $header_row;
			$detail_dbtable = new DBTable($this->dbtable->get_db(), "smis_pr_drkbu");
			$data['detail'] = $detail_dbtable->get_result("
				SELECT *
				FROM smis_pr_drkbu
				WHERE id_rkbu = '" . $id . "' AND prop NOT LIKE 'del'
				ORDER BY FIELD(smis_pr_drkbu.bulan, 'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'Nopember', 'Desember'), smis_pr_drkbu.nama_barang ASC
			");
			return $data;
		}
	}
?>