<?php
	global $wpdb;

	require_once 'smis-libs-inventory/install.php';
	$install = new InventoryInstallator($wpdb, "", "");
	$install->extendInstall("rad");
	$install->install();
	
    require_once "radiology/resource/install/table/smis_rad_dpesanan_lain.php";
	require_once "radiology/resource/install/table/smis_rad_layanan.php";
	require_once "radiology/resource/install/table/smis_rad_lrangkuman.php";
	require_once "radiology/resource/install/table/smis_rad_lrangkuman_pertanggal.php";
	require_once "radiology/resource/install/table/smis_rad_pembanding.php";
	require_once "radiology/resource/install/table/smis_rad_pendapatan.php";
	require_once "radiology/resource/install/table/smis_rad_pertindakan.php";
	require_once "radiology/resource/install/table/smis_rad_pesanan.php";
	require_once "radiology/resource/install/table/smis_rad_grup.php";
    require_once "radiology/resource/install/table/smis_rad_perlayanan_pasien.php";
    require_once "radiology/resource/install/table/smis_rad_rl52.php";
?>