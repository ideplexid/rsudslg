<?php
require_once ("smis-base/smis-include-service-consumer.php");
/**
 * service to get tarif of Radiology from Manajemen
 * @since 15 Mei 2014
 * @version 1.1
 * @copyright Nurul Huda <goblooge@gmail.com>
 * @author goblooge
 *
 */
class TarifRadiology extends ServiceConsumer {
	public function __construct($db, $kelas) {
		$data = array (
				"kelas" => $kelas 
		);
		parent::__construct ( $db, "get_radiology", $data, "manajemen" );
		$this->setMode ( ServiceConsumer::$SINGLE_MODE );
	}
}

?>