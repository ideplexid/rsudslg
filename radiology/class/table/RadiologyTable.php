<?php 
class RadiologyTable extends Table {
	private $code;
	private $is_stand_alone;
	private $e_resep;
	
	public function __construct($header,$title="",$content=NULL,$action=true){
		parent::__construct($header,$title,$content,$action);
		global $db;
		$this->e_resep=getSettings($db,"radiology-mode-e-resep","0")=="1";
	}
	
	public function setSystemModel($is_stand_alone){
		$this->is_stand_alone=$is_stand_alone;
		return $this;
	}
    
    public function getDetailPrintedElement($p, $f){
		$tbl=new TablePrint("");
		$tbl->setMaxWidth(false);
		$tbl->addStyle("width", "100%");
		$tbl->setTableClass("rad_kwitansi");
		global $db;
		
		$SHOW_JENIS_PASIEN=getSettings($db,"radiology-kwitansi-tampil-jenis-pasien","1")=="1";
		$TOTAL_RESULT=getSettings($db,"radiology-kwitansi-jumlah","1")*1;
		$SET_TAMPIL_TINDAKAN=getSettings($db,"radiology-kwitansi-tampil-jenis-pasien","1")=="1";
		$nama=getSettings($db,"smis_autonomous_title", "");
		$alamat=getSettings($db,"smis_autonomous_address", "");
		$tbl->addColumn("<strong> RADIOLOGY ".$nama."</strong>", 4, 1);
		$tbl->commit("title");
		$tbl->addColumn("<small>".$alamat."</small>", 4, 1);
		$tbl->commit("title");
		
        
        
		$tbl->addColumn("<small>&nbsp;</small>", 2, 1);
		$tbl->commit("title");
		
		$nrg=ArrayAdapter::format("only-digit10", $p->id)." - ".ArrayAdapter::format("only-digit6", $p->nrm_pasien);
		$tbl->addColumn("Nama", 1, 1,NULL,"","r_left");
		$tbl->addColumn(": ".$p->nama_pasien, 1, 1,NULL,"","r_left");
		$tbl->addColumn("NRM/NOREG", 1, 1,NULL,"","r_left");
		$tbl->addColumn(": ".$nrg, 1, 1,NULL,"","r_left");
		$tbl->commit("title");
		
        
        $nrg=ArrayAdapter::format("only-digit10", $p->id)." - ".ArrayAdapter::format("only-digit6", $p->nrm_pasien);
		$tbl->addColumn("Tanggal", 1, 1,NULL,"","r_left");
		$tbl->addColumn(": ".ArrayAdapter::format("date d M Y",$p->tanggal), 1, 1,NULL,"","r_left");
		$tbl->addColumn("Kelas", 1, 1,NULL,"","r_left");
		$tbl->addColumn(": ".ArrayAdapter::format("unslug",$p->kelas), 1, 1,NULL,"","r_left");
		$tbl->commit("title");
        
        $nrg=ArrayAdapter::format("only-digit10", $p->id)." - ".ArrayAdapter::format("only-digit6", $p->nrm_pasien);
		$tbl->addColumn("No. Pemeriksaan", 1, 1,NULL,"","r_left");
		$tbl->addColumn(": ".$p->no_lab, 1, 1,NULL,"","r_left");
		$tbl->addColumn("Jenis Pasien", 1, 1,NULL,"","r_left");
		$tbl->addColumn(": ".ArrayAdapter::format("unslug",$p->carabayar), 1, 1,NULL,"","r_left");
		$tbl->commit("title");
		
		
		$tbl->addColumn("<small>&nbsp;</small>", 2, 1);
		$tbl->commit("title");
		
		$tbl->addColumn("NAMA TEST", 3, 1,NULL,"","r_left b_bottom b_top bold");
		$tbl->addColumn("SUB TOTAL", 1, 1,NULL,"","r_left b_bottom b_top bold");
		$tbl->commit("title");
        
        $radres=new RadiologyResource();
		if($SET_TAMPIL_TINDAKAN){
			//tindakan centang
            $periksa=json_decode($p->periksa,true);
			$harga=json_decode($p->harga,true);
			foreach($periksa as $code=>$value){
				if($value=="1" || $value==1){
                    $name=$radres->getNameMap($code);
					$tbl->addColumn($name, 3, 1,NULL,"","r_left");
					$tbl->addColumn(ArrayAdapter::format("money Rp.", $harga[$p->kelas."_".$code] ), 1, 1,NULL,"","r_left");
					$tbl->commit("title");
				}
			}
            //tindakan lain
            $dbtable=new DBTable($db,"smis_rad_dpesanan_lain");
            $dbtable->addCustomKriteria(" id_pesanan ","='".$p->id."'");
            $dbtable->setShowAll(true);
            $data=$dbtable->view("","0");
            $list=$data['data'];
            foreach($list as $x){
                $tbl->addColumn($x->nama_layanan." x ".$x->jumlah, 3, 1,NULL,"","r_left");
                $tbl->addColumn(ArrayAdapter::format("money Rp.", $x->jumlah*$x->harga_layanan ), 1, 1,NULL,"","r_left");
				$tbl->commit("title");
            }
            //konsul dokter
            if($p->biaya_konsul>0){
                $tbl->addColumn(" Biaya Dokter ".$x->nama_konsultan, 3, 1,NULL,"","r_left");
                $tbl->addColumn(ArrayAdapter::format("money Rp.", $p->biaya_konsul ), 1, 1,NULL,"","r_left");
				$tbl->commit("title");
            }
		}
		
		loadLibrary("smis-libs-function-math");
		$tbl->addColumn("TOTAL", 3, 1,NULL,"","r_left b_top bold");
		$tbl->addColumn(ArrayAdapter::format("money Rp.", $p->total_biaya), 1, 1,NULL,"","r_left b_top bold");
		$tbl->commit("title","");
		$tbl->addColumn("TERBILANG", 1, 1,NULL,"","r_left  bold");
		$tbl->addColumn(numbertell($p->total_biaya)." Rupiah", 3, 1,NULL,"","r_left  ");
		$tbl->commit("title");
		
		$tbl->addColumn("<small>&nbsp;</small>", 4, 1);
		$tbl->commit("footer");
		
		$tbl->addColumn("", 2, 1);
		$tbl->addColumn("Operator", 2, 1);
		$tbl->commit("footer");
		
		$tbl->addColumn("", 2, 1);
		$tbl->addColumn("</br></br>", 2, 1);
		$tbl->commit("footer");
		
		global $user;
		$tbl->addColumn("", 2, 1);
		$tbl->addColumn($user->getNameOnly(), 2, 1);
		$tbl->commit("footer");
        $tbl->addColumn("", 2, 1);
		$tbl->addColumn(ArrayAdapter::format("date d M Y",date("Y-m-d")), 2, 1);
		$tbl->commit("footer");
		
		$hasil=$tbl->getHtml();
		$final_result="";
		for($tot=0;$tot<$TOTAL_RESULT;$tot++){
			$final_result=$final_result.$hasil." <div class='cutline'></div> ";
		}
		return $final_result;
	}
    
    
    public function getMiniPrintedElement($p, $f){
		$tbl=new TablePrint("");
		$tbl->setMaxWidth(false);
		$tbl->addStyle("width", "100%");
		$tbl->setTableClass("rad_kwitansi");
		global $db;
		
		$SHOW_JENIS_PASIEN=getSettings($db,"radiology-kwitansi-tampil-jenis-pasien","1")=="1";
		$TOTAL_RESULT=getSettings($db,"radiology-kwitansi-jumlah","1")*1;
		$SET_TAMPIL_TINDAKAN=getSettings($db,"radiology-kwitansi-tampil-jenis-pasien","1")=="1";
		$nama=getSettings($db,"smis_autonomous_title", "");
		$alamat=getSettings($db,"smis_autonomous_address", "");
		$tbl->addColumn("<strong> Kwitansi radiology ".$nama."</strong>", 2, 1);
		$tbl->commit("title");
		$tbl->addColumn("<small>".$alamat."</small>", 2, 1);
		$tbl->commit("title");
		
		$tbl->addColumn("<small>&nbsp;</small>", 2, 1);
		$tbl->commit("title");
		
		$tbl->addColumn("Nama", 1, 1,NULL,"","r_left");
		$tbl->addColumn($p->nama_pasien, 1, 1,NULL,"","r_left");
		$tbl->commit("title");
		$tbl->addColumn("Alamat", 1, 1,NULL,"","r_left");
		$tbl->addColumn($p->alamat, 1, 1,NULL,"","r_left");
		$tbl->commit("title");
		
		$tbl->addColumn("No. Lab", 1, 1,NULL,"","r_left");
		$tbl->addColumn($p->no_lab, 1, 1,NULL,"","r_left");
		$tbl->commit("title");
		
		
		$tbl->addColumn("ID-NRM-JP", 1, 1,NULL,"","r_left");
		$nrg=ArrayAdapter::format("only-digit10", $p->id)." - ".ArrayAdapter::format("only-digit6", $p->nrm_pasien)." - ".$p->carabayar;
		$tbl->addColumn($nrg, 1, 1,NULL,"","r_left");
		$tbl->commit("title");	
		
		$tbl->addColumn("RUANG - KLS", 1, 1,NULL,"","r_left");
		$tbl->addColumn(ArrayAdapter::format("unslug", $p->ruangan)." - ".ArrayAdapter::format("unslug", $p->kelas), 1, 1,NULL,"","r_left");
		$tbl->commit("title");
		
		$tbl->addColumn("Tanggal", 1, 1,NULL,"","r_left");
		$tbl->addColumn(ArrayAdapter::format("date d M Y", $p->tanggal), 1, 1,NULL,"","r_left");
		$tbl->commit("title");
		
		$tbl->addColumn("<small>&nbsp;</small>", 2, 1);
		$tbl->commit("title");
		
		$tbl->addColumn("Nama Test", 1, 1,NULL,"","r_left b_bottom b_top");
		$tbl->addColumn("Total Rp.", 1, 1,NULL,"","r_left b_bottom b_top");
		$tbl->commit("title");
        
        
        $radres=new RadiologyResource();
		if($SET_TAMPIL_TINDAKAN){
			$periksa=json_decode($p->periksa,true);
			$harga=json_decode($p->harga,true);
			foreach($periksa as $code=>$value){
				if($value=="1" || $value==1){
					$name=$radres->getNameMap($code);
                    $tbl->addColumn($name, 1, 1,NULL,"","r_left");
					$tbl->addColumn(ArrayAdapter::format("money Rp.", $harga[$p->kelas."_".$code] ), 1, 1,NULL,"","r_left");
					$tbl->commit("title");
				}
			}
		}
		
		loadLibrary("smis-libs-function-math");
		$tbl->addColumn("TOTAL", 1, 1,NULL,"","r_left b_top");
		$tbl->addColumn(ArrayAdapter::format("money Rp.", $p->total_biaya), 1, 1,NULL,"","r_left b_top");
		$tbl->commit("title","");
		$tbl->addColumn(numbertell($p->total_biaya)." Rupiah", 2, 1,NULL,"","r_left b_bottom ");
		$tbl->commit("title");
		
		$tbl->addColumn("<small>&nbsp;</small>", 2, 1);
		$tbl->commit("footer");
		
		$tbl->addColumn("", 1, 1);
		$tbl->addColumn("TTD", 1, 1);
		$tbl->commit("footer");
		
		$tbl->addColumn("", 1, 1);
		$tbl->addColumn("</br></br>", 1, 1);
		$tbl->commit("footer");
		
		global $user;
		$tbl->addColumn("", 1, 1);
		$tbl->addColumn($user->getNameOnly(), 1, 1);
		$tbl->commit("footer");
		
		$hasil=$tbl->getHtml();
		$final_result="";
		for($tot=0;$tot<$TOTAL_RESULT;$tot++){
			$final_result=$final_result.$hasil." <div class='cutline'></div> ";
		}
		return $final_result;
	}
	
    
    public function getPrintedElement($p, $f){
		global $db;
		$model=getSettings($db, "radiology-kwitansi-model", "Reguler");
		$css="<style type='text/css'>".getSettings($db, "radiology-kwitansi-css", "")."</style>";
		if($model=="Reguler"){
			return $this->getRegulerPrintedElement($p, $f).$css;
		}else if($model=="Mini"){
			return $this->getMiniPrintedElement($p, $f).$css;
		}else{
			return $this->getDetailPrintedElement($p, $f).$css;
		}
	}
	
	public function getRegulerPrintedElement($p, $f){
		$tbl=new TablePrint("");
		$tbl->setMaxWidth(false);
		$tbl->addStyle("width", "100%");
		$tbl->setTableClass("rad_kwitansi");
		global $db;
			$nama=getSettings($db,"smis_autonomous_title", "");
		$alamat=getSettings($db,"smis_autonomous_address", "");
		$tbl->addColumn("<h5> Kwitansi Radiology ".$nama."</h5>", 4, 1);
		$tbl->commit("title");
		$tbl->addColumn("No. ".ArrayAdapter::format("only-digit10", $p->id), 1, 1);
		$tbl->addColumn("<small>".$alamat."</small>", 3, 1);
		$tbl->commit("title");
	
		$tbl->addColumn("Nama", 1, 1);
		$tbl->addColumn($p->nama_pasien, 1, 1);
		$tbl->addColumn("Alamat", 1, 1);
		$tbl->addColumn($p->alamat, 1, 1);
		$tbl->commit("header");	
		
		
		$tbl->addColumn("No. Rad", 1, 1);
		$tbl->addColumn($p->no_lab, 1, 1);
		$tbl->addColumn("NRM", 1, 1);
		$tbl->addColumn(ArrayAdapter::format("only-digit8", $p->nrm_pasien), 1, 1);
		$tbl->commit("header");
	
		$tbl->addColumn("Kelas", 1, 1);
		$tbl->addColumn(ArrayAdapter::format("unslug", $p->kelas), 1, 1);
		$tbl->addColumn("Jenis Pasien", 1, 1);
		$tbl->addColumn($p->carabayar, 1, 1);
		$tbl->commit("header");
	
		$tbl->addColumn("Pengirim", 1, 1);
		$tbl->addColumn($p->nama_dokter, 1, 1);
		$tbl->addColumn("Tanggal", 1, 1);
		$tbl->addColumn(ArrayAdapter::format("date d M Y", $p->tanggal), 1, 1);
		$tbl->commit("header");
			
		loadLibrary("smis-libs-function-math");
		$tbl->addColumn("Nominal", 1, 1);
		$tbl->addColumn(ArrayAdapter::format("money Rp.", $p->total_biaya), 3, 1);
		$tbl->commit("body");
		$tbl->addColumn(numbertell($p->total_biaya)." Rupiah", 4, 1);
		$tbl->commit("body");
	
		$tbl->addColumn("TTD Kasir", 2, 1);
		$tbl->addColumn("TTD", 2, 1);
		$tbl->commit("footer");
	
		$tbl->addColumn("", 2, 1);
		$tbl->addColumn("</br></br>", 2, 1);
		$tbl->commit("footer");
	
		global $user;
		$tbl->addColumn("", 2, 1);
		$tbl->addColumn($user->getNameOnly(), 2, 1);
		$tbl->commit("footer");
	
		$hasil=$tbl->getHtml();
		$hasil=$hasil." <div class='cutline'></div> ".$hasil." <div class='cutline'></div> ".$hasil;
		return $hasil;
	}
	
	public function setCode($code,DBTable $dbtable) {
		$this->code = $code;
		
		if ($this->code == RadiologyTemplate::$MODE_DAFTAR) {
			$this->setDelButtonEnable(false);
			$btn = new Button("", "", "Tidak Jadi");
			$btn->setClass("btn-inverse");
			$btn->setIcon("fa fa-stop");
			$btn->setIsButton(Button::$ICONIC);
			$this->addContentButton("tidak_jadi", $btn);
		} else if ($this->code == RadiologyTemplate::$MODE_PERIKSA) {
			// $this->setAddButtonEnable($this->is_stand_alone);
			$this->setAddButtonEnable(true);
			$this->setPrintElementButtonEnable(true);
			$btn = new Button("", "", "Arsipkan");
			$btn->setIcon("fa fa-archive");
			$btn->setIsButton(Button::$ICONIC);
			$this->addContentButton("selesai", $btn);
			$btn = new Button("", "", "Tidak Terbaca");
			$btn->setIcon("fa fa-folder");
			$btn->setIsButton(Button::$ICONIC);
			$this->addContentButton("tidak_terbaca", $btn);
			$dbtable->addCustomKriteria(null, " selesai !='1' ");
			$dbtable->addCustomKriteria(null, " selesai !='-1' ");
		} else if ($this->code == RadiologyTemplate::$MODE_ARCHIVE) {
			$this->setAddButtonEnable(false);
			$this->setDelButtonEnable(false);
			$this->setPrintButtonEnable(false);
			$this->setPrintElementButtonEnable(true);
			$dbtable->addCustomKriteria("selesai", "='1'");
		} else if ($this->code == RadiologyTemplate::$MODE_UNREAD) {
			$btn = new Button("", "", "Kembalikan");
			$btn->setIsButton(Button::$ICONIC);
			$btn->setIcon("icon-black " . Button::$icon_arrow_up);
			$this->addContentButton("kembalikan", $btn);
			$this->setAddButtonEnable(false);
			$this->setDelButtonEnable(false);
			$this->setPrintButtonEnable(false);
			$this->setPrintElementButtonEnable(false);
			$this->setEditButtonEnable(false);
			$dbtable->addCustomKriteria("selesai", "='-1'");
		}
		return $this;		
	}
	
	public function getContentButton($id) {
		$btn_group=null;
		if ($this->code == RadiologyTemplate::$MODE_DAFTAR && 
				( $this->current_data ['selesai'] == "1" || $this->current_data ['selesai'] == "-2" || $this->current_data ['selesai'] == "-1" ) 
			) {
			$btn_group=new ButtonGroup("");
			$btn=new Button($this->name."_edit", "","Edit");
			$btn->setAction($this->action.".edit('".$id."')");
			if($this->current_data ['selesai'] == "1") 	$btn->setClass("btn-success");
			else if($this->current_data ['selesai'] == "-1") 	$btn->setClass("btn-inverse");
			else if($this->current_data ['selesai'] == "-2") 	$btn->setClass("btn-danger");
			$btn->setAtribute("data-content='Edit' data-toggle='popover'");
			$btn->setIcon("fa fa-eye");
			$btn->setIsButton(Button::$ICONIC);
			$btn_group->addElement($btn);
		}else if($this->code == RadiologyTemplate::$MODE_PERIKSA && $this->current_data ['selesai'] == "-2" ) {
			
			$btn=new Button($this->name."_tidak_batal", "","Tidak Batal");
			$btn->setAction($this->action.".kembalikan('".$id."')");
			$btn->setAtribute("data-content='Tidak Batal' data-toggle='popover'");
			$btn->setIcon("fa fa-refresh");
			$btn->setIsButton(Button::$ICONIC);
			
			$btn_group=parent::getContentButton($id);
			$btn_group->addElement($btn);
			$btn_group->setMax(4, "Batal");
			$btn_group->setButtonClass("btn-danger");
		}else {
			
			$btn_group=parent::getContentButton($id);			
			$btn_group->setMax(4, "Aksi");
			$btn_group->setButtonClass("btn-primary");
		
			if($this->e_resep){
				$btn=new Button($this->name."_eresep", "","E-Resep");
				$btn->setAction($this->action.".eresep('".$id."')");
				$btn->setAtribute("data-content='E-Resep' data-toggle='popover'");
				$btn->setIcon("fa fa-file-o");
				$btn->setIsButton(Button::$ICONIC);
				$btn_group->addElement($btn);
			}
			
		}	
		
		
		$btn=new Button($this->name."_printelement", "","Kwitansi");
		$btn->setAction("cetak_kwitansi('".$id."')");
		$btn->setClass("btn-inverse");
		$btn->setAtribute("data-content='Print' data-toggle='popover'");
		$btn->setIcon("fa fa-ticket");
		$btn->setIsButton(Button::$ICONIC);
		$btn_group->addElement($btn);
		return $btn_group;
	}
}

?>