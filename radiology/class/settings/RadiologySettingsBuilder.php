<?php 

class RadiologySettingsBuilder extends SettingsBuilder {
	public function getJS() {
		$result = parent::getJS ();
		ob_start ();
		?>
		<script type="text/javascript">	
			var film_1824;
			var film_2430;
			var film_3040;
			var film_3535;
			var film_3543;
			var film_roll;
			var film=["1824","2430","3040","3535","3543","roll"];
			var lfilm=[];
			var dokter_settings_radiology;
			$(document).ready(function(){
				var film_object=null;
				var film_id="";
				var index=0;
				for	(index = 0; index < film.length; index++) {
				    film_id=film[index];
				    film_object=new TableAction("film_"+film_id,"radiology","settings",new Array());
				    film_object.setSuperCommand("film_"+film_id);
				    film_object.selected=function(json){
					  	var fid=this.prefix;
					  	fid=fid.replace("film_","");
						$("#radiology-film-"+fid+"-nama").val(json.nama);
						$("#radiology-film-"+fid+"-id").val(json.id);
					};
                    film_object.addRegulerData=function(data){
                        data['slug']="film";
                        return data;
                    }
					lfilm[film_id]=film_object;
				}
				for	(index = 0; index < film.length; index++) {
				    var f_id=film[index];
				    $("#radiology-film-"+f_id+"-nama").prop("disabled",true);
					$("#radiology-film-"+f_id+"-id").prop("disabled",true);
				}
				$("#radiology-konsultan-nama").prop("disabled",true);
				$("#radiology-konsultan-id").prop("disabled",true);

				film_1824=lfilm["1824"];
				film_2430=lfilm["2430"];
				film_3040=lfilm["3040"];
				film_3535=lfilm["3535"];
				film_3543=lfilm["3543"];
				film_roll=lfilm["roll"];
				
				dokter_settings_radiology=new TableAction("dokter_settings_radiology","radiology","settings",new Array());
				dokter_settings_radiology.setSuperCommand("dokter_settings_radiology");
				dokter_settings_radiology.selected=function(json){
					$("#radiology-konsultan-nama").val(json.nama);
					$("#radiology-konsultan-id").val(json.id);
				};
                dokter_settings_radiology.addRegulerData=function(data){
                    data['slug']="film";
                    return data;
                }				
			});
			
		</script>
		<?php
		
		$r2 = ob_get_clean ();
		$result .= $r2;
		return $result;
	}
}
?>