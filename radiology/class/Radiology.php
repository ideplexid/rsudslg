<?php
require_once 'smis-framework/smis/template/ModulTemplate.php';
require_once ("smis-base/smis-include-service-consumer.php");
require_once ("smis-libs-hrd/DKResponder.php");

require_once "smis-libs-hrd/EmployeeResponder.php";
require_once 'radiology/class/responder/RadiologyResponder.php';
require_once 'radiology/class/service/RuanganService.php';
require_once 'smis-libs-manajemen/ProvitSharingService.php';
require_once 'radiology/class/service/TarifRadiology.php';
require_once 'radiology/resource/RadiologyResource.php';
require_once 'radiology/class/table/RadiologyTable.php';

/**
 * 
 * this class is used for creating Radiology Logic System
 * including the Radiology Bussiness Process
 * Clearing The Summernote and etc.
 * 
 * @author goblooge
 * @since 28 Oct 2014
 * @version 1.0.1
 * @license Propietary
 * @copyright Nurul Huda <goblooge@gmail.com>
 */
class RadiologyTemplate extends ModulTemplate {
	protected $db;
	protected $mode;
	protected $polislug;
	protected $page;
	protected $protoslug;
	protected $protoname;
	protected $protoimplement;
	protected $action;
	protected $dbtable;
	protected $uitable;
	protected $noreg_pasien;
	protected $nama_pasien;
	protected $nrm_pasien;
	protected $jk;
	protected $list_pesan;
	protected $list_hasil;
	protected $all_component;
    protected $list_grup;
    
	protected $carabayar;
	protected $umur;
	protected $alamat;
	protected $kelas;
	protected $resource;
	protected $uri;
	protected $is_stand_alone;
	public static $MODE_DAFTAR = "pendaftaran";
	public static $MODE_PERIKSA = "pemeriksaan";
	public static $MODE_ARCHIVE = "arsip_terbaca";
	public static $MODE_UNREAD = "arsip_tidak_terbaca";
	public function __construct($db, $mode, $polislug = "all", $noreg = "", $nrm = "", $nama = "", $jk = "", $page = "radiology", $action = "pemeriksaan", $protoslug = "", $protoname = "", $protoimplement = "",$kelas="rawat_jalan") {
		$this->db = $db;
		$this->mode = $mode;
		$this->noreg_pasien = $noreg;
		$this->nama_pasien = $nama;
		$this->nrm_pasien = $nrm;
		$this->jk = $jk;
		$this->carabayar = "Number Register Not Active";
        $this->polislug = $polislug;
        $col = array("id","id_paket","tanggal","nama_pasien","nrm_pasien","noreg_pasien","jk","kelas","alamat","ruangan","no_lab","carabayar","id_marketing","marketing","id_dokter","nama_dokter","id_konsultan","nama_konsultan","id_petugas","nama_petugas","nama_dokter_konsul","id_dokter_konsul","periksa","hasil","harga","umur","pembagian","selesai","waktu_daftar","waktu_datang","waktu_ditangani","waktu_selesai","respontime","uri","froll","frollr","f1824","f2430","f3040","f3535","f3543","f1824r","f2430r","f3040r","f3535r","f3543r","barulama","file","sdq4335","dhf3543","dhf2636","dhf2025","dvb3543","dvb3528","dvb2025","fdental","sdq4335r","dhf3543r","dhf2636r","dhf2025r","dvb3543r","dvb3528r","dvb2025r","fdentalr","akunting","operator","input_di","jenis_kegiatan","biaya","biaya_lain","biaya_konsul","total_biaya","biaya_konsul","status");
        
        require_once "radiology/class/dbtable/RadiologyDBTable.php";
        $this->dbtable = new LaboratoryDBTable ( $db, "smis_rad_pesanan",$col );
        $this->dbtable ->setAutoSynch(getSettings($db,"cashier-real-time-tagihan","0")!="0");

        
        //$this->dbtable = new DBTable($db, "smis_rad_pesanan",$col);
		$this->is_stand_alone=getSettings($db, "radiology-sistem-model", "Stand Alone")=="Stand Alone";
		$this->page = $page;
		$this->protoslug = $protoslug;
		$this->protoimplement = $protoimplement;
		$this->protoname = $protoname;
		$this->action = $action;
		$this->kelas = $kelas;
		$this->alamat = "";
		$this->uri=0;
		$this->initComponent ();
		if ($noreg != "") {
			$data_post = array ("command" => "edit","id" => $noreg );
			$service = new ServiceConsumer($this->db, "get_registered", $data_post);
			$service->execute ();
			$data = $service->getContent ();
			if($data !=NULL){
				$this->carabayar = $data ['carabayar'];
				$this->uri=$data['uri'];
				$this->umur = $data ['umur'];
				$this->alamat = $data['alamat_pasien'];
				if($this->jk=="-1")
					$this->jk=$data['kelamin'];
                    
                if($this->nama_pasien==""){
                    $this->nama_pasien=$data['nama'];
                }
                
                if($this->nrm_pasien==""){
                    $this->nama_pasien=$data['nrm_pasien'];
                }
			}
		}
		if ($polislug != "all") {
			$this->dbtable->addCustomKriteria("ruangan", "='" . $polislug . "'");
		}
		if ($noreg != "") {
			$this->dbtable->addCustomKriteria("noreg_pasien", "='" . $noreg . "'");
		}
		$header=array ("No.",'Tanggal','Pasien',"NRM","No. Reg",'Kelas',"Biaya","Biaya Lain","Total Biaya","Ruangan","No. Rad", "Lampiran", "Status","Layanan");
		$this->uitable = new RadiologyTable($header, ArrayAdapter::format("unslug", $this->mode) . " RADIOLOGY " . ($this->polislug == "all" ? "" : strtoupper($this->protoname)), NULL, true);
		$this->uitable->setName($action);
		$this->uitable->setSystemModel($this->is_stand_alone);
		$this->uitable->setCode($this->mode,$this->dbtable);
	}
	public function initComponent() {
		$this->resource= new RadiologyResource ();
		$this->all_component        = &$this->resource->list_layanan;
		$this->list_pesan           = &$this->resource->list_name;
		$this->list_hasil           = &$this->resource->list_name;
		$this->list_default_pria    = &$this->resource->list_default_pria;
		$this->list_default_wanita  = &$this->resource->list_default_wanita;
        $this->list_grup            = &$this->resource->list_grup;
	}
	public function command($command) {
        require_once "radiology/class/adapter/RadiologyAdapter.php";
        $adapter = new RadiologyAdapter ();
        $adapter->setMap($this->resource->name_map);
        $adapter->setUseNumber(true,"No.","back.");
		$adapter->add("Tanggal", "tanggal", "date d M Y");
		$adapter->add("Pasien", "nama_pasien");
		$adapter->add("NRM", "nrm_pasien", "digit8");
		$adapter->add("No. Reg", "noreg_pasien", "digit8");
        $adapter->add ( "Biaya", "biaya", "money Rp." );
		$adapter->add ( "Biaya Lain", "biaya_lain", "money Rp." );
		$adapter->add ( "Total Biaya", "total_biaya", "money Rp." );
		$adapter->add ("Lampiran", "file", "files-image-show");
		$adapter->add("Kelas", "kelas");
		//$adapter->add("Ruangan", "ruangan", "unslug");
		$adapter->add("No. Rad", "no_lab");
		$adapter->add("selesai", "selesai");
		$adapter->add("Status", "status");
		if ($_POST['command'] == "list") {
			//custom view to accomodate regular checkup fee + additional checkup fee:
			$filter = "1";
			if (isset($_POST['kriteria'])) {
				$filter .= " AND (tanggal LIKE '" . $_POST['kriteria'] . "' OR nama_pasien LIKE '%" . $_POST['kriteria'] . "%' OR nrm_pasien LIKE '%" . $_POST['kriteria'] . "%' OR noreg_pasien LIKE '%" . $_POST['kriteria'] . "%' OR no_lab LIKE '%" . $_POST['kriteria'] . "%')";
			}
			$mode_filter = "";
			if ($this->mode == RadiologyTemplate::$MODE_DAFTAR || $this->mode == RadiologyTemplate::$MODE_PERIKSA) {
				$mode_filter = "AND  (selesai = '0' OR selesai = -2) ";
			} else if ($this->mode == RadiologyTemplate::$MODE_ARCHIVE) {
				$mode_filter = "AND selesai = '1'";
			} else if ($this->mode == RadiologyTemplate::$MODE_UNREAD) {
				$mode_filter = "AND selesai = '-1'";
			}
            
            
            if($this->mode==RadiologyTemplate::$MODE_PERIKSA){
                $btn1 = new Button("", "", "Rehabilitasi Medik");
                $btn1 ->setClass("btn-inverse");
                $btn1 ->setIcon("fa fa-file");
                $btn1 ->setIsButton(Button::$ICONIC);
                $this->uitable->addContentButton("rehab_medik", $btn1);
                
                $btn2 = new Button("", "", "Pelayanan Khusus");
                $btn2 ->setClass("btn-inverse");
                $btn2 ->setIcon("fa fa-file");
                $btn2 ->setIsButton(Button::$ICONIC);
                $this->uitable->addContentButton("pelayanan_khusus", $btn2);
    
                $btn3 = new Button("", "", "Jenis Pelayanan");
                $btn3 ->setClass("btn-inverse");
                $btn3 ->setIcon("fa fa-file");
                $btn3 ->setIsButton(Button::$ICONIC);
                $this->uitable->addContentButton("lap_rl52", $btn3);
            }

			if($this->noreg_pasien!="" && $this->noreg_pasien!="0"){
				$filter .= " AND noreg_pasien='".$this->noreg_pasien."' ";
			}
			
			$query_value = "
				SELECT *, (biaya + biaya_lain) AS 'total_biaya'
				FROM (
					SELECT a.*, SUM(CASE WHEN b.prop = 'del' OR b.jumlah IS NULL THEN 0 ELSE b.jumlah END * CASE WHEN b.prop = 'del' OR b.harga_layanan IS NULL THEN 0 ELSE b.harga_layanan END) AS 'biaya_lain'
					FROM smis_rad_pesanan a LEFT JOIN smis_rad_dpesanan_lain b ON a.id = b.id_pesanan
					WHERE a.prop NOT LIKE 'del' " . $mode_filter . "
					GROUP BY a.id
				) v
				WHERE " . $filter . "
			";
			$query_count = "
				SELECT COUNT(*)
				FROM (
					SELECT a.*, SUM(CASE WHEN b.prop = 'del' OR b.jumlah IS NULL THEN 0 ELSE b.jumlah END * CASE WHEN b.prop = 'del' OR b.harga_layanan IS NULL THEN 0 ELSE b.harga_layanan END) AS 'biaya_lain'
					FROM smis_rad_pesanan a LEFT JOIN smis_rad_dpesanan_lain b ON a.id = b.id_pesanan
					WHERE a.prop NOT LIKE 'del' " . $mode_filter . "
					GROUP BY a.id
				) v
				WHERE " . $filter . "
			";
			//$this->dbtable->setPreferredQuery(true, $query_value, $query_count);
			
		}
        if($command=="list"){
            $this->dbtable->setColumn(array("id","tanggal","nama_pasien","noreg_pasien","nrm_pasien","kelas","total_biaya","ruangan","no_lab","file","selesai"));
        }
        $dbres = new RadiologyResponder($this->dbtable, $this->uitable, $adapter);
		if ($dbres->is("save") && isset($_POST ['kelas']) && $_POST ['kelas']!="") {
			if(getSettings($this->db,"radiology-activate-provit-share","0")=="1"){
                $provit = new ProvitSharingService($this->db, $this->polislug, "smis-pv-radiology",$this->carabayar);
                $provit->execute ();
                $ps = $provit->getContent ();
                $dbres->addColumnFixValue("pembagian", $ps);
            }
            
            $harga = new TarifRadiology($this->db, $_POST ['kelas']);
            $harga->execute ();
            $harga_radiology = $harga->getContent ();
            $dbres->addColumnFixValue("harga", $harga_radiology);
		}
        loadLibrary("smis-libs-function-time");
		$dbres->setJsonColumn(array ("hasil","periksa"));
		if ($command == "save" && isset($_POST ['no_lab']) && ($_POST ['no_lab'] == '' || $_POST ['no_lab'] == '0')) {
			if ($_POST ['id'] == "" || $_POST ['id'] == "0") {
				$nomor = $this->dbtable->getNextID () * 1 ;
				$dbres->addColumnFixValue("no_lab", "RAD-" . $_POST ['noreg_pasien'] . "/" . to_romawi(date("m")) . "/" . date("y") . "/" . $nomor);
			} else {
				$dbres->addColumnFixValue("no_lab", "RAD-" . $_POST ['noreg_pasien'] . "/" . to_romawi(date("m")) . "/" . date("y") . "/" . $_POST ['id']);
			}
		}
        
		$data = $dbres->command($command);
		echo json_encode($data);
		return;
	}
	
	public function setPemeriksaanPreload(){
		if($this->mode==self::$MODE_PERIKSA){
			if(getSettings($this->db,"radiology-ui-pemeriksaan-add-tindakan","1")=="0"){
				$this->uitable->setAddButtonEnable(false);
			}
		}
	}
	
	public function phpPreLoad() {
		$this->setPemeriksaanPreload();
		$service = new RuanganService($this->db);
		$service->execute ();
		$ruangan = $service->getContent ();
		
		$nama_konsultan = getSettings($this->db, "radiology-konsultan-nama", "");
		$id_konsultan = getSettings($this->db, "radiology-konsultan-id", "");		
		require_once 'smis-base/smis-include-service-consumer.php';
		$service = new ServiceConsumer($this->db, "get_kelas");
        //$service->setCached(true,"get_kelas");
		$service->execute ();
		$kelas = $service->getContent ();
		$option_kelas = new OptionBuilder ();
		foreach($kelas as $k) {
			$nama = $k ['nama'];
			$slug = $k ['slug'];
			$option_kelas->add($nama, $slug, $slug == $this->kelas ? "1" : "0");
		}
		
		$jkselect=new OptionBuilder();
		$jkselect->add("L","0",$this->jk=="0")->add("P","1",$this->jk=="1");
		
		$this->uitable->addModal("id", "hidden", "", "");
		$this->uitable->addModal("tanggal", "date", "Tanggal", date("Y-m-d"));
		$this->uitable->addModal("waktu_datang", "datetime", "Datang", "", "y", null, false);
        $this->uitable->addModal("waktu_ditangani", "datetime", "Ditangani", date("Y-m-d H:i"), "y", null, false);
		$this->uitable->addModal("waktu_selesai", "datetime", "Waktu Cetak", "", "y", null, false);
		$this->uitable->addModal("no_lab", "text", "No. Radiologi", "", "y", null, false);
		$this->uitable->addModal("noreg_pasien", "chooser-" . $this->action . "-rad_pasien-Pilih Pasien", "No Reg", $this->noreg_pasien, "n", null, true);
		$this->uitable->addModal("nama_pasien", "text", "Pasien", $this->nama_pasien, "n", null, true);
		$this->uitable->addModal("nrm_pasien", "text", "NRM", $this->nrm_pasien, "n", null, true);
		$this->uitable->addModal("kelas", "select", "Kelas", $option_kelas->getContent (), "n", null, false);
        /*show and hide marketing*/
        if(getSettings($this->db,"radiology-show-marketing","0")=="1"){
            $this->uitable->addModal("marketing", "chooser-".$this->action."-rad_marketing-Marketing", "Marketing", "", "n", null, true);
        }else{
            $this->uitable->addModal ( "marketing", "hidden", "", "", "y", null, true);
        }
        $this->uitable->addModal("id_marketing","hidden","","");
        //$this->uitable->addModal("pengirim", "chooser-".$this->action."-rad_pengirim-Pengirim", "Pengirim", "", "n", null, true);
        //$this->uitable->addModal("id_pengirim","hidden","","");
		$this->uitable->addModal("id_dokter", "hidden", "", "", "y", null, true);
		$this->uitable->addModal("nama_dokter", "chooser-" . $this->action . "-rad_dokter", "Pengirim", "", "n", null, true);
		$this->uitable->addModal("id_konsultan", "hidden", "", $id_konsultan, "y", null, true);
		$this->uitable->addModal("nama_konsultan", "chooser-" . $this->action . "-rad_konsultan", "Konsultan", $nama_konsultan, "y", null, true);
		$this->uitable->addModal("nama_petugas", "chooser-" . $this->action . "-rad_petugas", "Petugas", "", "y", null, true);
		$this->uitable->addModal("id_petugas", "hidden", "", "", "y", null, true);
		$this->uitable->addModal("carabayar", "text", "Jenis Pasien", $this->carabayar, "y", null, true);
		$this->uitable->addModal ("file", "files-image-upload", "Lampiran", "", "y", null, true);
		
		if ($this->polislug == "all" || $this->is_stand_alone){
			$this->uitable->addModal("ruangan", "select", "Asal Ruangan", $ruangan);
		}else{
			$this->uitable->addModal("ruangan", "text", "Asal Ruangan", $this->polislug, "n", null, true);
		}
		$this->uitable->addModal("id_konsultan", "text", "NIP Konsultan", $id_konsultan, "n", null, true);
		$this->uitable->addModal("umur", "text", "Umur", $this->umur, "y", null, false);
		
		$this->uitable->addModal("biaya_konsul", "money", "Biaya Konsul", "", "y", null);

		$jenis_kegiatan_option = new OptionBuilder();
		$jenis_kegiatan_option->addSingle("RADIODIAGNOSTIK - Foto tanpa bahan kontras");
		$jenis_kegiatan_option->addSingle("RADIODIAGNOSTIK - Foto dengan bahan kontras");
		$jenis_kegiatan_option->addSingle("RADIODIAGNOSTIK - Foto dengan rol film");
		$jenis_kegiatan_option->addSingle("RADIODIAGNOSTIK - Flouroskopi");
		$jenis_kegiatan_option->addSingle("RADIODIAGNOSTIK - Foto Gigi");
		$jenis_kegiatan_option->addSingle("RADIODIAGNOSTIK - C.T. Scan");
		$jenis_kegiatan_option->addSingle("RADIODIAGNOSTIK - Lymphografi");
		$jenis_kegiatan_option->addSingle("RADIODIAGNOSTIK - Angiograpi");
		$jenis_kegiatan_option->addSingle("RADIODIAGNOSTIK - Lain-Lain");
		$jenis_kegiatan_option->addSingle("RADIOTHERAPI - Radiotherapi");
		$jenis_kegiatan_option->addSingle("RADIOTHERAPI - Lain-Lain");
		$jenis_kegiatan_option->addSingle("KEDOKTERAN NUKLIR - Diagnostik");
		$jenis_kegiatan_option->addSingle("KEDOKTERAN NUKLIR - Therapi");
		$jenis_kegiatan_option->addSingle("KEDOKTERAN NUKLIR - Lain-Lain");
		$jenis_kegiatan_option->addSingle("IMAGING/PENCITRAAN - USG");
		$jenis_kegiatan_option->addSingle("IMAGING/PENCITRAAN - MRI");
		$jenis_kegiatan_option->addSingle("IMAGING/PENCITRAAN - Lain-Lain");
		
		$this->uitable->addModal("jenis_kegiatan", "select", "Jenis Kegiatan", $jenis_kegiatan_option->getContent(), "y", null);
        
        $status = new OptionBuilder();
        $status->addSingle("", "1");
        $status->addSingle("Sudah Dicek", "0");
        $status->addSingle("Menunggu Hasil", "0");
        $status->addSingle("Selesai", "0");
        $this->uitable->addModal ( "status", "select", "Status", $status->getContent(), "y", null, false, null, false, null);
		
		$modal = $this->uitable->getModal ();
		$modal->setTitle("Pesanan");
		
		$btn = new Button("", "", "Print");
		$btn->setIsButton(Button::$ICONIC_TEXT);
		$btn->setIcon("fa fa-print");
		$btn->setAction("rad_print()");
		$btn->setClass("btn-primary");
		$modal->addFooter($btn);
		
		$btn = new Button("", "", "Tag");
		$btn->setIsButton(Button::$ICONIC_TEXT);
		$btn->setIcon("fa fa-tag");
		$btn->setAction("tag_print()");
		$btn->setClass("btn-primary");
		$modal->addFooter($btn);

		$tabulator = new Tabulator("pesan", "pesanan", Tabulator::$POTRAIT);
		
        require_once 'radiology/resource/modal_pesanan.php';
		$psn = new RadiologyModalPesanan($this->all_component);
		$tabulator->add("rad_perika", "Pemeriksaan", $psn, Tabulator::$TYPE_COMPONENT," fa fa-stethoscope");

        require_once 'radiology/resource/modal_film.php';
        $film = new RadiologyModalFilm("", "", "");
		$tabulator->add("rad_film", "Film", $film, Tabulator::$TYPE_COMPONENT," fa fa-film");
		
        require_once 'radiology/resource/modal_hasil.php';
		$hsl = new RadiologyModalHasil($this->resource);
		$tabulator->add("rad_hasil", "Hasil", $hsl, Tabulator::$TYPE_COMPONENT, " fa fa-address-book",$this->action);		
		
        require_once 'radiology/resource/modal_lain.php';
		$lain = new RadiologyModalLain("","","");
		$tabulator->add("rad_lain", "Lain-Lain", $lain, Tabulator::$TYPE_COMPONENT," fa fa-list");
		
        if(getSettings($this->db,"radiology-show-diagnosa","0")=="1" && $this->mode!=self::$MODE_DAFTAR){
            $tabulator->add("rad_diagnosa", "Diagnosa", "<div id='page_diagnosa_radiology'></div>", Tabulator::$TYPE_HTML,"fa fa-user-md","diagnosa_radiology()");
        }
        
        $modal->addHTML($tabulator->getHtml ());
		$modal->setClass(Modal::$FULL_MODEL);
		
		// modal layanan lain:
		$layanan_lain_modal = new Modal("layanan_lain_add_form", "smis-form-container", "layanan_lain");
		$layanan_lain_modal->setTitle("Layanan Lain");
		$id_hidden = new Hidden("layanan_lain_id", "layanan_lain_id", "");
		$layanan_lain_modal->addElement("", $id_hidden);
		$nama_layanan_text = new Text("layanan_lain_nama", "layanan_lain_nama", "");
		$nama_layanan_text->addAtribute("autofocus");
		$layanan_lain_modal->addElement("Nama", $nama_layanan_text);
		$harga_layanan_text = new Text("layanan_lain_harga", "layanan_lain_harga", "");
		$harga_layanan_text->setTypical("money");
		$harga_layanan_text->setAtribute(" data-thousands=\".\" data-decimal=\",\" data-prefix=\"Rp. \"  data-precision=\"2\" " );
		$layanan_lain_modal->addElement("Harga Satuan", $harga_layanan_text);
		$jumlah_text = new Text("layanan_lain_jumlah", "layanan_lain_jumlah", "1");
		$layanan_lain_modal->addElement("Jumlah", $jumlah_text);
		$subtotal_text = new Text("layanan_lain_subtotal", "layanan_lain_subtotal", "");
		$subtotal_text->setTypical("money");
		$subtotal_text->setAtribute("data-thousands=\".\" data-decimal=\",\" data-prefix=\"Rp. \"  data-precision=\"2\" disabled='disabled' ");
		$layanan_lain_modal->addElement("Subtotal", $subtotal_text);
		$button = new Button("", "", "Simpan");
		$button->setClass("btn-success");
		$button->setIcon("fa fa-floppy-o");
		$button->setIsButton(Button::$ICONIC);
		$button->setAtribute("id='layanan_lain_save_btn'");
		$layanan_lain_modal->addFooter($button);
		
		echo $this->jsSetup("RAD_MODE","",$this->mode);
		echo $this->jsSetup("RAD_NOREG","",$this->noreg_pasien);
		echo $this->jsSetup("RAD_NAMA","",$this->nama_pasien);
		echo $this->jsSetup("RAD_NRM","",$this->nrm_pasien);
		echo $this->jsSetup("RAD_POLISLUG","",$this->polislug);
		echo $this->jsSetup("RAD_PAGE","",$this->page);
		echo $this->jsSetup("RAD_PREFIX","",$this->action);
		echo $this->jsSetup("RAD_PROTOSLUG","",$this->protoslug);
		echo $this->jsSetup("RAD_PROTONAME","",$this->protoname);
		echo $this->jsSetup("RAD_PROTOIMPLEMENT","",$this->protoimplement);
        echo $this->jsSetup("RAD_JK","",$this->jk);
        echo $this->jsSetup($this->action."_jk","",$this->jk);
		echo $this->jsSetup("RAD_LIST_PESAN","",json_encode($this->list_pesan));
		echo $this->jsSetup("RAD_LIST_HASIL","",json_encode($this->list_hasil));
		echo $this->jsSetup("RAD_EDIT_HASIL","",getSettings($this->db,"radiology-ui-pemeriksaan-edit-tindakan","0"));
		echo $this->jsSetup("RAD_EDIT_LAYANAN","",getSettings($this->db,"radiology-ui-pemeriksaan-edit-hasil","0"));
		echo $this->jsSetup("RAD_LIST_GRUP","",json_encode($this->list_grup));
		
		
		require_once 'radiology/resource/print_header.php';
		require_once 'radiology/resource/print_footer.php';
		echo $this->uitable->getHtml ();
		echo $modal->getHtml ();
		echo $layanan_lain_modal->getHtml();
		
		echo addJS("framework/bootstrap/js/bootstrap-datepicker.js");
		echo addJS ( "framework/bootstrap/js/bootstrap-datetimepicker.js" );
		echo addJS("framework/smis/js/table_action.js");		
		echo addJS ( "radiology/resource/js/radiology_function.js",false);
		echo addJS ( "radiology/resource/js/layanan_lain.js",false);
		echo addJS ( "radiology/resource/js/radiology_action.js",false);	
		echo addJS ( "radiology/resource/js/radiology.js",false); 
		echo '<script type="text/javascript">var '.$this->action.'=RAD_ACTION; </script>';
		echo addJS ( "radiology/resource/js/radiology_supercommand.js",false); 
		echo addJS ( "radiology/resource/js/radiology_typeahead.js",false); 
		echo addJS ( "radiology/resource/js/radiology_diagnosa.js",false); 
		loadLibrary("smis-libs-function-javascript");
		
		echo addCSS("framework/bootstrap/css/datepicker.css");
		echo addCSS ( "framework/bootstrap/css/bootstrap-datetimepicker.css" );
		
		echo addCSS("radiology/resource/css/radiology.css",false);
		echo "<style type='text/css'>";
		$margin_right=getSettings($this->db, "radiology-print-margin-right", "0");
		$margin_left=getSettings($this->db, "radiology-print-margin-left", "0");
		echo "#printing_area > div{margin-left:".$margin_left."px !important; margin-right:".$margin_right."px !important; }";
		echo "</style>";
	}
	
	private function jsSetup($id,$name,$value){
		$hidden=new Hidden($id,$name,$value);
		return $hidden->getHtml();
	}
	
	public function superCommand($super_command) {
		        $super = new SuperCommand ();
        
        if($super_command=="rad_dokter")
        {
            $header=array (	'Nama','Jabatan',"NIP");
            $dktable = new Table($header, "", NULL, true);
            $dktable->setName("rad_dokter");
            $dktable->setModel(Table::$SELECT);
            $dkadapter = new SimpleAdapter ();
            $dkadapter->add("Jabatan", "nama_jabatan");
            $dkadapter->add("Nama", "nama");
            $dkadapter->add("NIP", "nip");
            $dkresponder = new EmployeeResponder($this->db, $dktable, $dkadapter, "");
            //$dkresponder->setJenisPegawai(EmployeeResponder::$PEGAWAI_NON_ORGANIK);
            $super->addResponder("rad_dokter", $dkresponder);
        }
        else if($super_command=="rad_konsultan")
        {
            $slug_konsultan = getSettings($db, 'radiology-slug-dokter-konsultan-rad');
            $header=array (	'Nama','Jabatan',"NIP");
            $kktable = new Table ($header, "", NULL, true);
            $kktable->setName("rad_konsultan");
            $kktable->setModel(Table::$SELECT);
            $dkadapter = new SimpleAdapter ();
            $dkadapter->add("Jabatan", "nama_jabatan");
            $dkadapter->add("Nama", "nama");
            $dkadapter->add("NIP", "nip");
            if( $slug_konsultan != null || $slug_konsultan != '') {
                $kkresponder = new EmployeeResponder($this->db, $kktable, $dkadapter, $slug_konsultan);
            } else {
                $kkresponder = new DKResponder($this->db, $kktable, $dkadapter, "employee");
            }
            
            $super->addResponder("rad_konsultan", $kkresponder);
        }
        else if($super_command=="rad_petugas")
        {
            $slug_petugas = getSettings($db, 'radiology-slug-petugas-rad');
            $header=array (	'Nama','Jabatan',"NIP");
            $pettable = new Table ($header, "", NULL, true);
            $pettable->setName("rad_petugas");
            $pettable->setModel(Table::$SELECT);
            $dkadapter = new SimpleAdapter ();
            $dkadapter->add("Jabatan", "nama_jabatan");
            $dkadapter->add("Nama", "nama");
            $dkadapter->add("NIP", "nip");
            if( $slug_petugas != null || $slug_petugas != '') {
                $petresponder = new EmployeeResponder($this->db, $pettable, $dkadapter, $slug_petugas);
            } else {
                $petresponder = new EmployeeResponder($this->db, $pettable, $dkadapter, "");
            }
            
            $super->addResponder("rad_petugas", $petresponder);
        }
        else if($super_command=="rad_pasien")
        {
            $phead=array ('Nama','NRM',"No Reg");
            $ptable = new Table($phead, "", NULL, true);
            $ptable->setName("rad_pasien");
            $ptable->setModel(Table::$SELECT);
            $padapter = new SimpleAdapter ();
            $padapter->add("Nama", "nama_pasien");
            $padapter->add("NRM", "nrm", "digit8");
            $padapter->add("No Reg", "id");
            $presponder = new ServiceResponder($this->db, $ptable, $padapter, "get_registered");
            $super->addResponder("rad_pasien", $presponder);
        }
        else if($super_command=="rad_marketing")
        {
            $marketing_table = new Table (array('Nama', 'Jabatan', 'NIP'), "", NULL, true);
            $marketing_table->setName("rad_marketing");
            $marketing_table->setModel(Table::$SELECT);
            $marketing_adapter = new SimpleAdapter();
            $marketing_adapter->add("Nama", "nama");
            $marketing_adapter->add("Jabatan", "nama_jabatan");
            $marketing_adapter->add("NIP", "nip");
            $marketing_dbresponder = new EmployeeResponder($this->db, $marketing_table, $marketing_adapter, "marketing");
            $super->addResponder ("rad_marketing", $marketing_dbresponder);
        }
        
        
		$init = $super->initialize ();
		if ($init != null) {
			echo $init;
			return;
		}
	}
}


?>