<?php 
require_once 'laboratory/class/MapRuangan.php';
class RadiologyAdapter extends SimpleAdapter{
	public function adapt($d){
		$a=parent::adapt($d);
        $a['Nomor']=substr($d->tanggal, 2,2).self::format("only-digit8", $d->id);
        $a['Ruangan']=MapRuangan::getRealName($d->ruangan);
        $a['Layanan']=$this->getAllLayanan($d->periksa);
		return $a;
    }	
    
    private $map;
    
    public function setMap($map){
        $this->map=$map;
    }
    
    public function getMap($slug){
        return $this->map[$slug];
    }
    
 
    private function getAllLayanan($periksa){
        $hasil="";
        $json=json_decode($periksa,true);
        if($json==null){
            return $hasil;
        }
        foreach($json as $x=>$y){
            if($y==1){
                $hasil.=$this->getMap($x).", ";
            }
        }
        return $hasil;
    }

}