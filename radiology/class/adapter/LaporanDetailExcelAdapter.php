<?php 

class LaporanDetailExcelAdapter extends SimpleAdapter{
    private $map;
    
    public function setMap($map){
        $this->map=$map;
    }
    
    public function getMap($slug){
        return $this->map[$slug];
    }
    
    
    public function adapt($d){
        $one=parent::adapt($d);
        $one['Layanan']=$this->getAllLayanan($d->periksa);
        echo json_encode($one);
        return $one;
    }
    
    private function getAllLayanan($periksa){
        $hasil="";
        $json=json_decode($periksa,true);
        if($json==null){
            return $hasil;
        }
        foreach($json as $x=>$y){
            if($y==1){
                $hasil.=$this->getMap($x).", ";
            }
        }
        return $hasil;
    }
    
}

?>