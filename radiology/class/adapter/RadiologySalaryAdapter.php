<?php

/**
 * this class is used for calculating the Salary and Provit Sharing
 * of Radiology, it will be consume by the Service for 
 * Finance Purpose.
 * 
 * @since 31 October 2014
 * @version 1.0.0
 * @license LGPL v3
 * @copyright Nurul Huda <goblooge@gmail.com>
 * @author goblooge
 *
 */
class RadiologySalaryAdapter extends ArrayAdapter {
	private $content;
	protected $id_karyawan;
	protected $ruangan;
	protected $tipe;
	protected $layanan_list;
	
	public static $TYPE_INDIVIDU="individual";
	public static $TYPE_COMMUNAL="communal";
	public static $TYPE_BOTH="both";
	public static $CODESET;/*=array(
		"tindakan kecil - film besar" 				=> "Tindakan Kecil - Film Besar",
		"tindakan kecil - film kecil"				=> "Tindakan Kecil - Film Kecil",
		"tindakan kecil - posisi khusus"			=> "Tindakan Kecil - Posisi Khusu",
		"tindakan sedang - usg" 					=> "Tindakan Sedang - USG",
		"tindakan sedang - foto kontras"			=> "Tindakan Sedang - Foto Kontras",
		"tindakan besar - foto kontras"				=> "Tindakan Besar - Foto Kontras",
		"tindakan besar - ct scan tanpa kontras"	=> "Tindakan Besar - CT Scan Tanpa Kontras",
		"tindakan besar - ct scan dengan kontras"	=> "Tindakan Besar - CT Scan Dengan Kontras"
	);*/
	
	
	
	public function __construct($id_karyawan,$tipe="individual") {
		parent::__construct ();
		$this->content = array ();
		$this->ruangan = "radiology";
		$this->id_karyawan = $id_karyawan;
		$this->tipe=$tipe;
		$this->layanan_list=$this->getLayananType();
	}
	
	/**
	 * this function used to get all radiology service type so every service 
	 * can be mapped in the system based on it's service grup (like plain foto, contrast study, usg and dental)
	 * @return Array of <string>
	 */
	public function getLayananType(){
		global $db;
        $dbtable=new DBTable($db,"smis_rad_grup");
        $dbtable->setShowAll(true);
        $data=$dbtable->view("",0);
        $adapter_codename=new SelectAdapter("nama","slug",true);
        $adapter_codeset=new SelectAdapter("slug","nama",true);
        
        $CODESET_NAME=$adapter_codename->getContent($data['data']);
        self::$CODESET=$adapter_codeset->getContent($data['data']);
        /*
		$CODESET_NAME=array(
			"x-ray: central nervous system"					=> "X-Ray: Central Nervous System",
			"x-ray: teeth, orbota, nose & ear system"		=> "X-Ray: Teeth, Orbita, Nose & Ear System",
			"x-ray: respiratory system"						=> "X-Ray: Respiratory System",
			"x-ray: abdomen, gastro intestinal & pelvis"	=> "X-Ray: Abdomen, Gastro Intestinal & Pelvis",
			"x-ray: extremity & articulation system"		=> "X-Ray: Extremity & Articulation System",
			"x-ray: urogenital & obstretical system"		=> "X-Ray: Urogenital & Obstretical System",
			"x-ray: others"									=> "X-Ray: Others",
			"ct scan: ct contrast"							=> "CT Scan: CT Contrast",
			"ct scan: ct non-contrast"						=> "CT Scan: CT Non-Contrast",
			"echo cardiography"								=> "ECHO Cardiography",
			"usg: complete abdomial"						=> "USG: Complete Abdomial",
			"usg: kandungan"								=> "USG: Kandungan",
			"usg: small part"								=> "USG: Small Part",
			"usg: doppler / vasculer"						=> "USG: Doppler/Vasculer",
			"usg: endo cavity"								=> "USG: Endo Cavity",
			"usg: invasive"									=> "USG: Invasive",
			"usg: other"									=> "USG: Other"		
		);*/
		$dbtable=new DBTable($db, "smis_rad_layanan");
		$dbtable->setOrder("layanan ASC, nama ASC");
		$dbtable->setShowAll(true);
		$ldata=$dbtable->view("", "0");
		$data=$ldata['data'];
	
		$result=array();
		foreach($data as $layanan){
			$slug="rad_".$layanan->id;
			$grup=$layanan->layanan;
			$result[$slug]=$CODESET_NAME[$grup];
		}
		return $result;
	}
	
	/**
	 * get this layanan code name based on it's id 
	 * so it will be transfered into the current price based name
	 * 
	 * @param unknown $slug
	 */
	public function getSlugName($slug){
		return $this->layanan_list[$slug];
	}
	
	/**
	 * this methode was used to summary of 
	 * the total price of each kind of service
	 * plain, contrast, usg, and ctscan
	 * @param string $periksa array of what patient need
	 * @param string $kelas class of patient
	 * @param string $harga array of pricelist
	 * @param string $code it must be plain, contrast, usg, ctscan
	 * @return number total price of it's kind of code (plain,contrast, usg, ctscan)
	 */	
	protected function getTotalPrice($periksa,$kelas,$harga,$code){
		$tharga=0;
		//global $querylog;
		
		foreach($periksa as $name=>$val){
			//if($val=="1") $querylog->addMessage($name." ---> ".$this->getSlugName($name)." -- ".$code);
			
			if($val=="1" && startsWith($this->getSlugName($name), $code)){
				$idharga=$kelas."_".$name;
				$tharga=$harga[$idharga]*1+$tharga;
				//$witness=" MASUK ----------------------> [ ".$name." ] : [ ".$code." ] ".$harga[$idharga]."  => ".$tharga."  ".$idharga;
				//$querylog->addMessage($witness);
			}
		}
		
		return $tharga;
	}
	
	/**
	 * this function use to calculate salary of each service 
	 * in radiology (plain,contrast,usg, ct scan) 
	 * and each employee (dokter, konsultan, petugas)
	 * 
	 * @param array $d is the array from database
	 * @param array $pembagian is array of pembagian
	 * @param int $nilai_asli is the real price of each tipe (plain,contrast,usg, ct scan)
	 * @param string $code the tipe of service, it should be plain, contrast, usg or ctscan
	 * @param string $codename the code name of tipe service it should be Plain Foto for plain, Foto Kontras for contrast, Ultrasonography fot usg, and CT Scan for ctscan
	 */
	public function calculateSalary($d,$pembagian,$nilai_asli,$code,$codename){
		$ruangan = $this->ruangan;
		$waktu = $d ['tanggal'];
		$nama = $d ['nama_pasien'];
		$nrm = $d ['nrm_pasien'];
		$iddata=$d ['id'];
		$ket = self::format ( "unslug", $d ['kelas'] );
		
		if ($this->id=="-1" || $this->is($d ['id_dokter'],$pembagian['s-dokter-pengirim-'.$code]) ) {
			$honor_persen = $pembagian ['dokter-pengirim-'.$code];
			$honor_sebagai = "Dokter Pengirim ".$codename;
			$nilai = $honor_persen * $nilai_asli / 100;
			$karyawan=$d ['nama_dokter'];
			$this->pushContent ($iddata, $d ['id_dokter'], $waktu, $ruangan, $honor_sebagai, $honor_persen, $nilai, $nilai_asli, $nrm, $nama, $karyawan, $ket );
		}
	
		if ($this->id=="-1" || $this->is($d ['id_konsultan'],$pembagian['s-dokter-konsultan-'.$code]) ) {
			$honor_persen = $pembagian ['dokter-konsultan-'.$code];
			$honor_sebagai = "Dokter Konsultan ".$codename;
			$nilai = $honor_persen * $nilai_asli / 100;
			$karyawan=$d ['nama_konsultan'];
			$this->pushContent ($iddata, $d ['id_konsultan'], $waktu, $ruangan, $honor_sebagai, $honor_persen, $nilai, $nilai_asli, $nrm, $nama, $karyawan, $ket );
		}
	
		if ($this->is($d ['id_petugas'],$pembagian['s-petugas-'.$code]) ) {
			$honor_persen = $pembagian ['petugas-'.$code];
			$honor_sebagai = "Petugas ".$codename;
			$nilai = $honor_persen * $nilai_asli / 100;
			$karyawan = $this->tipe==self::$TYPE_BOTH?"PETUGAS RADIOLOGY":$d ['nama_petugas'];
			$this->pushContent ( $iddata, $d ['id_petugas'], $waktu, $ruangan, $honor_sebagai, $honor_persen, $nilai, $nilai_asli, $nrm, $nama, $karyawan, $ket );
		}
	
	}
	
	public function adapt($d) {
		$pembagian = json_decode ( $d ['pembagian'], true );
		$periksa = json_decode($d ['periksa'],true);
		$harga = json_decode($d ['harga'],true);	
		$kelas=$d['kelas'];
		//var_dump($d);
		
		foreach(self::$CODESET as $code=>$codename){
			$nilai_asli=$this->getTotalPrice($periksa, $kelas, $harga, $code);			
			if($nilai_asli>0 && $nilai_asli!=""){
				$this->calculateSalary($d, $pembagian, $nilai_asli,$code,$codename);
			}	
		}	
	}
	
	/**
	 * determine if this data match the kriteria 
	 * is it individual or communal, and did the current karyawan match the kriteria
	 * @param string $id_karyawan is the karyawan id
	 * @param array $pembagian  is the adjective of current provit sharing
	 * @return boolean 
	 */
	public function is($id_karyawan,$pembagian){
		if($this->tipe==self::$TYPE_BOTH )
			return true;
		else if($this->tipe==self::$TYPE_INDIVIDU && $pembagian==self::$TYPE_INDIVIDU  && ($this->id_karyawan==$id_karyawan || $this->id_karyawan =="-1") )
			return true;
		else if($this->tipe==self::$TYPE_COMMUNAL && $pembagian==self::$TYPE_COMMUNAL)
			return true;
		return false;
	}

	/**
	 * this is a format for every single salary, 
	 * this format represent from server service
	 * 
	 * @param unknown $waktu
	 * @param unknown $ruangan
	 * @param unknown $honor_sebagai
	 * @param unknown $honor_persen
	 * @param unknown $nilai
	 * @param unknown $nilai_asli
	 * @param unknown $nrm
	 * @param unknown $nama
	 * @param string $nama_karyawan
	 * @param string $ket
	 */
	protected function pushContent($iddata,$idkaryawan,$waktu, $ruangan, $honor_sebagai, $honor_persen, $nilai, $nilai_asli, $nrm, $nama, $nama_karyawan="", $ket = "") {
		$array = array ();
		$array ['waktu'] = self::format ( "date d M Y", $waktu );
		$array ['waktu_asli']=$waktu;
		$array ['id'] = $iddata;
		$array ['id_karyawan'] = $idkaryawan;
		$array ['ruangan'] = $ruangan;
		$array ['sebagai'] = $honor_sebagai;
		$array ['percentage'] = $honor_persen . "%";
		$array ['nilai'] = $nilai;
		$array ['asli'] = $nilai_asli;
		$array ['pasien'] = self::format ( "only-digit6", $nrm ) . " : " . $nama;
		$array ['keterangan'] = $ket;
		$array ['karyawan'] = $nama_karyawan;		
		$this->content [] = $array;
	}
	
	public function getContent($data) {
		foreach ( $data as $d ) {
			$this->adapt ( $d );
		}
		return $this->content;
	}
}

?>