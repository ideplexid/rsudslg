<?php 


class RecapitulationProvitShareRadiologyAdapter extends SimpleAdapter{

	
	private $total_diterima;
	private $total_biaya_awal;
	private $total_potongan;
	private $total_pajak;
	private $total_pengembangan;
	private $pajak;
	public function __construct($pajak){
		parent::__construct();
		$this->pajak=$pajak;
	}
	
	public function adapt($d){
		$a=array();
		$pajak=$d['tagihan']*$this->pajak/100;
		$dana_pengembangan=$d['tagihan']-$d['bonus']-$pajak;
		$potongan=$d['tagihan']-$d['bonus'];
		$this->total_diterima+=$d['bonus'];
		$this->total_biaya_awal+=$d['tagihan'];
		$this->total_pajak+=$pajak;
		$this->total_pengembangan+=$dana_pengembangan;
		$this->total_potongan+=$potongan;
		$a['Pajak']=$pajak;
		$a['Bagian RS']=$potongan;
		$a['Dana Pengembangan']=$dana_pengembangan;
		$a['Biaya Awal']=$d['tagihan'];
		$a['Bagian Dokter']=$d['bonus'];
		$a['Dokter']=$d['dokter'];
		return $a;
	}
	
	public function group($rows){
		$result=array();
		foreach($rows as $row){
			if($row['Dokter']=="") 
				$row['Dokter']=" ... N/A ... ";
			$key=$row['Dokter'];
			if(!isset($result[$key])){
				$result[$key]=$row;
			}else{
				$rcontent=$result[$key];
				$rcontent['Biaya Awal']+=$row['Biaya Awal'];
				$rcontent['Dana Pengembangan']+=$row['Dana Pengembangan'];
				$rcontent['Bagian RS']+=$row['Bagian RS'];
				$rcontent['Bagian Dokter']+=$row['Bagian Dokter'];
				$rcontent['Pajak']+=$row['Pajak'];
				$result[$key]=$rcontent;
			}
		}
		
		ksort($result);
		$final_result=array();
		foreach($result as $oneres){
			$oneres['Biaya Awal']=self::format("money Rp.", $oneres['Biaya Awal']);
			$oneres['Dana Pengembangan']=self::format("money Rp.", $oneres['Dana Pengembangan']);
			$oneres['Bagian RS']=self::format("money Rp.", $oneres['Bagian RS']);
			$oneres['Bagian Dokter']=self::format("money Rp.", $oneres['Bagian Dokter']);
			$oneres['Pajak']=self::format("money Rp.", $oneres['Pajak']);
			$final_result[]=$oneres;
		}
		return $final_result;		
	}
	
	
	public function getContent($data){
		$result_data=parent::getContent($data);
		$thedata=$this->group($result_data);
		$array=array();
		$array['Dokter']="<strong>Total</strong>";
		$array['Biaya Awal']="<strong>Bagian Dokter [BG]</strong>";
		$array['Bagian Dokter']="<strong><font id='bagian_dokter'>".self::format("money Rp.", $this->total_diterima)."</font></strong>";
		$array['Bagian RS']="-";
		$array['Pajak']="-";
		$array['Dana Pengembangan']="-";
		$thedata[]=$array;
		
		$array=array();
		$array['Dokter']="<strong>Total</strong>";
		$array['Biaya Awal']="<strong>Pendapatan [P]</strong>";
		$array['Bagian Dokter']="<strong><font id=''>".self::format("money Rp.", $this->total_biaya_awal/3)."</font></strong>";
		$array['Bagian RS']="-";
		$array['Pajak']="-";
		$array['Dana Pengembangan']="-";
		$thedata[]=$array;
		
		$array=array();
		$array['Dokter']="<strong>Total</strong>";
		$array['Biaya Awal']="<strong>Bagian RS ( [BRS]=[P]-[BG] )</strong>";
		$array['Bagian Dokter']="<strong><font id='bagian_accounting'>".self::format("money Rp.", $this->total_biaya_awal/3-$this->total_diterima)."</font></strong>";
		$array['Bagian RS']="-";
		$array['Pajak']="-";
		$array['Dana Pengembangan']="-";
		$thedata[]=$array;
		
		$array=array();
		$array['Dokter']="<strong>Total</strong>";
		$array['Biaya Awal']="<strong>Pajak ( [PJK]=[P]*".$this->pajak."% )</strong>";
		$array['Bagian Dokter']="<strong><font id=''>".self::format("money Rp.", $this->total_biaya_awal*$this->pajak/300)."</font></strong>";
		$array['Bagian RS']="-";
		$array['Pajak']="-";
		$array['Dana Pengembangan']="-";
		$thedata[]=$array;
		
		$array=array();
		$array['Dokter']="<strong>Total</strong>";
		$array['Biaya Awal']="<strong>Dana Pengembangan ([DPG]=[BRS]-[PJK])</strong>";
		$array['Bagian Dokter']="<strong><font id=''>".self::format("money Rp.", $this->total_biaya_awal/3-$this->total_diterima-$this->total_biaya_awal*$this->pajak/200)."</font></strong>";
		$array['Bagian RS']="-";
		$array['Pajak']="-";
		$array['Dana Pengembangan']="-";
		$thedata[]=$array;
		
		return $thedata;
	}
	
	public function getTerimaDokter(){
		return $this->total_diterima;
	}
	
	public function getTerimaRS(){
		return $this->total_potongan;
	}
	
}

?>