<?php
require_once 'radiology/class/adapter/RadiologySalaryAdapter.php';

class RadiologySalaryCollector {
	private $id;
	private $from;
	private $to;
	private $db;
	private $adapter;
	private $mode;
	private $limit_start;
	private $limit_length;
	
	public static $MODE_EMPLOYEE="individual";
	public static $MODE_ROOM="communal";
	public static $MODE_BOTH="both";

	public function __construct($db, $id, $from, $to, $mode="individual",$start=-1,$length=-1) {
		$this->id = $id;
		$this->from = $from;
		$this->to = $to;
		$this->db = $db;
		$this->mode=$mode;
		$this->limit_start=$start;
		$this->limit_length=$length;
	}
	
	public function getSalary() {
		$hasil = array ();
		$dbtable = new DBTable ( $this->db, "smis_rad_pesanan");
		$dbtable->setFetchMethode ( DBTable::$ARRAY_FETCH );
		$ckc = self::createWhere ( array("id_dokter","id_konsultan","id_petugas"), $this->id );
		$dbtable->addCustomKriteria ( null, $ckc );
		$dbtable->addCustomKriteria ( null, " tanggal >= '" . $this->from . "'" );
		$dbtable->addCustomKriteria ( null, " tanggal < '" . $this->to . "'" );
		$data=NULL;
		if($this->limit_start==-1 || $this->limit_length==-1){
			$dbtable->setShowAll(true);
			$dbtable->setDebuggable(true);
			$data = $dbtable->view ( "", "0" );
		}else{
			$dbtable->setShowAll(false);
			$dbtable->setMaximum($this->limit_length);
			$data = $dbtable->view ( "", $this->limit_start );
		}
		$adapter=new RadiologySalaryAdapter ( $this->id, $this->mode);
		$list=$adapter->getContent($data['data']);
		return $list;
	}
	
	public function getTotalItem(){
		$dbtable = new DBTable ( $this->db, "smis_rad_pesanan");
		$dbtable->setFetchMethode ( DBTable::$ARRAY_FETCH );
		$ckc = self::createWhere ( array("id_dokter","id_konsultan","id_petugas"), $this->id );
		$dbtable->addCustomKriteria ( null, $ckc );
		$dbtable->addCustomKriteria ( null, " tanggal >= '" . $this->from . "'" );
		$dbtable->addCustomKriteria ( null, " tanggal < '" . $this->to . "'" );
		$total=$dbtable->count("");
		return $total;
	}
	
	public static function createWhere($check, $id) {
		$d = " ( ";
		$d .= implode ( " ='" . $id . "' OR ", $TABNAME );
		$d .= " ='" . $id . "' ) ";
	}
}

?>