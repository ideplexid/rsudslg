<?php
	global $db;
	global $user;
	require_once 'smis-libs-class/Policy.php';
	require_once 'smis-libs-inventory/policy.php';
	$inventory_policy = new InventoryPolicy("radiology", $user,"modul/");	
	$policy=new Policy("radiology", $user);
	$policy->setDefaultPolicy(Policy::$DEFAULT_POLICY_RESTRICT);
	
    
    // bagian arsip
    $policy->addPolicy("arsip_menu","arsip_menu",Policy::$DEFAULT_COOKIE_CHANGE,"modul/arsip_menu");
	$policy->addPolicy("arsip","arsip_menu",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/arsip_menu/arsip");
    $policy->addPolicy("unread","arsip_menu",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/arsip_menu/unread");
	$policy->addPolicy("arsip_admin","arsip_menu",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/arsip_menu/arsip_admin");
    
    // bagian data induk
    $policy->addPolicy("data_induk","data_induk",Policy::$DEFAULT_COOKIE_CHANGE,"modul/data_induk");
	$policy->addPolicy("evaluasi_penetapan_waktu_lap_hasil_radiologi","data_induk",Policy::$DEFAULT_COOKIE_CHANGE,"resource/php/data_induk/evaluasi_penetapan_waktu_lap_hasil_radiologi");
    $policy->addPolicy("cleaning_data","data_induk",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/data_induk/cleaning_data");
	$policy->addPolicy("koreksi_pv_dokter","data_induk",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/data_induk/koreksi_pv_dokter");
    $policy->addPolicy("pembanding_pv_radiology","data_induk",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/data_induk/pembanding_pv_radiology");
    $policy->addPolicy("layanan","data_induk",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/data_induk/layanan");
    $policy->addPolicy("group_data","data_induk",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/data_induk/group_data");
	//$policy->addPolicy("bacaan_normal","data_induk",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/data_induk/bacaan_normal");
	
    // bagian laporan
	$policy->addPolicy("laporan_menu","laporan_menu",Policy::$DEFAULT_COOKIE_CHANGE,"modul/laporan_menu");
	$policy->addPolicy("laporan","laporan_menu",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/laporan_menu/laporan");
    $policy->addPolicy("laporan_foto","laporan_menu",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/laporan_menu/laporan_foto");
	$policy->addPolicy("laporan_detail","laporan_menu",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/laporan_menu/laporan_detail");
	$policy->addPolicy("laporan_perlayanan_pasien","laporan_menu",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/laporan_menu/laporan_perlayanan_pasien");
	$policy->addPolicy("laporan_per_jenis_pasien","laporan_menu",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/laporan_menu/laporan_per_jenis_pasien");
	$policy->addPolicy("laporan_per_tanggal","laporan_menu",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/laporan_menu/laporan_per_tanggal");
	$policy->addPolicy("laporan_detail_pertanggal","laporan_menu",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/laporan_menu/laporan_detail_pertanggal");
	$policy->addPolicy("laporan_sensus_harian","laporan_menu",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/laporan_menu/laporan_sensus_harian");
	$policy->addPolicy("laporan_pend_perlayanan","laporan_menu",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/laporan_menu/laporan_pend_perlayanan");
    $policy->addPolicy("pv_dokter","laporan_menu",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/laporan_menu/pv_dokter");
    
    $policy->addPolicy("pemeriksaan","pemeriksaan",Policy::$DEFAULT_COOKIE_CHANGE,"modul/pemeriksaan");
    $policy->addPolicy("rehab_medik","pemeriksaan",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/pemeriksaan/rehab_medik");
    $policy->addPolicy("pelayanan_khusus","pemeriksaan",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/pemeriksaan/pelayanan_khusus");
    $policy->addPolicy("lap_rl52","pemeriksaan",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/pemeriksaan/lap_rl52");

	$policy->addPolicy("e_resep","pemeriksaan",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/pemeriksaan/e_resep");
    $policy->addPolicy("show_diagnosa","pemeriksaan",Policy::$DEFAULT_COOKIE_KEEP,"snippet/show_diagnosa");
    
    $policy->addPolicy("settings","settings",Policy::$DEFAULT_COOKIE_CHANGE,"modul/settings");
    $policy->addPolicy("kode_akun", "settings", Policy::$DEFAULT_COOKIE_KEEP,"snippet/kode_akun");
	$policy->combinePolicy($inventory_policy);
	$policy->initialize();
?>
