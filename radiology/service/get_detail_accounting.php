<?php 
require_once "radiology/resource/RadiologyResource.php";
global $db;
$id=$_POST['data'];

$dbtable= new DBTable($db,"smis_rad_pesanan");
$x          = $dbtable->selectEventDel($id);
$resource   = new RadiologyResource();

/*transaksi untuk pasien Radiology*/

$list   = array();
$periksa=json_decode($x->periksa,true);
$harga=json_decode($x->harga,true);
foreach($periksa as $slug=>$v){
    if($v=="0")
        continue;
    
    $biaya=$harga[$x->kelas."_".$slug];
    $nama=$resource->getNameMap($slug);
    $debet=array();
    $dk=$resource->getDebitKredit($slug,$x->carabayar);
    $debet['akun']    = $dk['d'];
    $debet['debet']   = $biaya;
    $debet['kredit']  = 0;
    $debet['ket']     = "Piutang Radiology - ".$nama." - ".$x->no_lab." pasien ".$x->nama_pasien." dengan No.Reg ".$x->id;
    $debet['code']    = "debet-radiology-".$x->id;
    $list[] = $debet;
     
    $kredit=array();
    $kredit['akun']    = $dk['k'];
    $kredit['debet']   = 0;
    $kredit['kredit']  = $biaya;
    $kredit['ket']     = "Pendapatan Radiology - ".$nama." - ".$x->no_lab." pasien ".$x->nama_pasien." dengan No.Reg ".$x->id;
    $kredit['code']    = "kredit-radiology-".$x->id;
    $list[] = $kredit;
}

//Mapping untuk Biaya Konsul Radiology
if($x->biaya_konsul > 0) {
    $debet              = array();
    $debet['akun']      = getSettings($db, "radiology-accounting-debit-biaya-konsul", "");
    $debet['debet']     = $x->biaya_konsul;
    $debet['kredit']    = 0;
    $debet['ket']       = "Piutang Radiology Biaya Konsul - ".$x->no_lab." pasien ".$x->nama_pasien." dengan No.Reg ".$x->id;
    $debet['code']      = "debet-radiology-".$x->id;
    $list[] = $debet;
    
    $kredit = array();
    $kredit['akun']     = getSettings($db, "radiology-accounting-kredit-biaya-konsul", "");
    $kredit['debet']    = 0;
    $kredit['kredit']   = $x->biaya_konsul;
    $kredit['ket']      = "Pendapatan Radiology Biaya Konsul - ".$x->no_lab." pasien ".$x->nama_pasien." dengan No.Reg ".$x->id;
    $kredit['code']     = "kredit-radiology-".$x->id;
    $list[] = $kredit;
}

//Mapping untuk Pesanan Lain-Lain Radiology
$pesananlain_dbtable = new DBTable($db, "smis_rad_dpesanan_lain");
$pesananlain_dbtable->addCustomKriteria ( "id_pesanan", "='" . $id . "'" );
$pesananlain_dbtable->setShowAll ( true );
$data = $pesananlain_dbtable->view ( "", "0" );
$rows = $data ['data'];
$biaya_total = 0;
foreach ( $rows as $d ){
    //biaya layanan lain:
    $layanan_lain_row = $dbtable->get_row("
        SELECT SUM(jumlah * harga_layanan) AS 'biaya_lain'
        FROM smis_rad_dpesanan_lain
        WHERE id_pesanan = '$id' AND prop != 'del'
        GROUP BY id_pesanan");
    $biaya_lain = $layanan_lain_row->biaya_lain != null ? $layanan_lain_row->biaya_lain : 0;
    //$biaya_total = $biaya_total + $biaya_lain;
}
if($biaya_lain > 0) {
    $debet              = array();
    $debet['akun']      = getSettings($db, "radiology-accounting-debit-pesanan-lain", "");
    $debet['debet']     = $biaya_lain;
    $debet['kredit']    = 0;
    $debet['ket']       = "Piutang Radiology Pesanan Lain - ".$x->no_lab." pasien ".$x->nama_pasien." dengan No.Reg ".$x->id;
    $debet['code']      = "debet-radiology-".$x->id;
    $list[] = $debet;
    
    $kredit = array();
    $kredit['akun']     = getSettings($db, "radiology-accounting-kredit-pesanan-lain", "");
    $kredit['debet']    = 0;
    $kredit['kredit']   = $biaya_lain;
    $kredit['ket']      = "Pendapatan Radiology Pesanan Lain - ".$x->no_lab." pasien ".$x->nama_pasien." dengan No.Reg ".$x->id;
    $kredit['code']     = "kredit-radiology-".$x->id;
    $list[] = $kredit;
}

//content untuk header
$header=array();
$header['tanggal']      = $x->waktu_daftar;
$header['keterangan']   = "Transaksi Radiology ".$x->no_lab." Pasien ".$x->nama_pasien." dengan No.Reg ".$x->id;
$header['code']         = "Radiology-".$x->id;
$header['nomor']        = "RAD-".$x->id;
$header['debet']        = $x->biaya + $x->biaya_konsul + $biaya_lain;
$header['kredit']       = $x->biaya + $x->biaya_konsul + $biaya_lain;
$header['io']           = "1";

$transaction_Radiology=array();
$transaction_Radiology['header']=$header;
$transaction_Radiology['content']=$list;


/*transaksi keseluruhan*/
$transaction=array();
$transaction[]=$transaction_Radiology;
echo json_encode($transaction);

/*confirm that already synch*/
$update['akunting']=1;
$id['id']=$x->id;
$dbtable->setName("smis_rad_pesanan");
$dbtable->update($update,$id);

?>