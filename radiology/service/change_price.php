<?php 
require_once 'radiology/service/class/TarifRadiology.php';
global $db;
$id=$_POST['id'];
$kelas=$_POST['kelas'];
$ruang=$_POST['ruang'];
$dbtable=new DBTable($db, "smis_rad_pesanan");
$one=$dbtable->select($id);
$periksa=json_decode($one->periksa,true);


$harga = new TarifRadiology ( $db, $kelas );
$harga->execute ();
$harga_lab= $harga->getContent ();

$biaya=0;
$dharga = json_decode ( $harga_lab, true );
foreach ( $periksa as $k => $v ) {
	if ($v == "1") {
		$the_key = $kelas . "_" . $k;
		$biaya += ($dharga [$the_key] * 1);
	}
}

$hasil=array();
$hasil['harga']=$biaya;
$hasil['nama']="Radiology - ".ArrayAdapter::format("unslug", $ruang)." - ".ArrayAdapter::format("unslug", $kelas);
echo json_encode($hasil);


?>