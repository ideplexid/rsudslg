<?php
global $db;
require_once 'smis-base/smis-include-service-consumer.php';

$nama_konsultan = getSettings ( $db, "radiology-konsultan-nama", "" );
$id_konsultan = getSettings ( $db, "radiology-konsultan-id", "" );
$data ['tanggal'] = $_POST ['waktu'];
$data ['nama_pasien'] = $_POST ['nama_pasien'];
$data ['nrm_pasien'] = $_POST ['nrm_pasien'];
$data ['noreg_pasien'] = $_POST ['no_register'];
$data ['carabayar'] = $_POST ['carabayar'];
$data ['jk'] = $_POST ['jk'];
$detail = json_decode($_POST['detail'], true);
$data ['alamat'] = $detail['alamat'];

$default_kelas=getSettings($db,"radiology-ui-pemeriksaan-default-jenis","rawat_jalan");
$kelas = getSettings($db, "smis-rs-kelas-" . ArrayAdapter::format("slug", $_POST['asal']),null);
if ($kelas == null){
    $kelas = $default_kelas;
}
$data ['kelas'] = $kelas;

//mendapatkan uri pasien saat ini:
$params = array();
$params['noreg_pasien'] = $_POST['no_register'];
$service = new ServiceConsumer($db, "get_uri", $params, "registration");
$service->setMode(ServiceConsumer::$CLEAN_BOTH);
$content = $service->execute()->getContent();
$data ['uri'] = $content[0];
$data ['ruangan'] = $_POST ['asal'];
$data ['umur'] = $_POST ['umur'];
$data ['no_lab'] = "";
$data ['id_konsultan'] = $id_konsultan;
$data ['nama_konsultan'] = $nama_konsultan;
$dbtable = new DBTable ( $db, "smis_rad_pesanan" );
$dbtable->insert ( $data );

$nomor = $dbtable->count ( "" );
$success ['id'] = $dbtable->get_inserted_id ();
$success ['success'] = 1;
$success ['type'] = "insert";
$success ['nomor'] = $nomor;
$content = $success;
$response = new ResponsePackage ();
$response->setContent ( $content );
$response->setStatus ( ResponsePackage::$STATUS_OK );
$response->setAlertVisible ( true );
$response->setAlertContent ( "Data Saved", "Your Data Had Been Saved", ResponsePackage::$TIPE_INFO );
echo json_encode ( $response->getPackage () );
?>