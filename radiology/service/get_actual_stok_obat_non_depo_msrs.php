<?php
	global $db;
	
	if (isset($_POST['id_obat'])) {
		$id_obat = $_POST['id_obat'];
		$dbtable = new DBTable($db, "smis_rad_stok_obat");
		$jumlah = 0;
		$row = $dbtable->get_row("
			SELECT SUM(sisa) AS 'jumlah'
			FROM smis_rad_stok_obat
			WHERE prop NOT LIKE 'del' AND konversi = 1 AND id_obat = '" . $id_obat . "'
		");
		if ($row != null)
			$jumlah += $row->jumlah;
		$row = $dbtable->get_row("
			SELECT CASE WHEN SUM(sisa * konversi) IS NULL THEN 0 ELSE SUM(sisa * konversi) END AS 'jumlah'
			FROM smis_rad_stok_obat
			WHERE prop NOT LIKE 'del' AND konversi > 1 AND id_obat = '" . $id_obat . "'
		");
		if ($row != null)
			$jumlah += $row->jumlah;
		$data = array();
		$data['data'] = array(
			"ruangan"	=> "radiology",
			"jumlah"	=> $jumlah
		);
		echo json_encode($data);
	}
?>