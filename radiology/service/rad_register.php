<?php
global $db;
require_once 'radiology/class/Radiology.php';

$noreg_pasien = $_POST ['noreg_pasien'];
$nama_pasien = $_POST ['nama_pasien'];
$nrm_pasien = $_POST ['nrm_pasien'];
$jk = $_POST ['jk'];
$page = $_POST ['page'];
$action = $_POST ['action'];
$polislug = $_POST ['polislug'];
$pslug = $_POST ['prototype_slug'];
$pname = $_POST ['prototype_name'];
$pimplement = $_POST ['prototype_implement'];
$kelas = $_POST ['kelas'];

ob_start ();
$template = new RadiologyTemplate ( $db, RadiologyTemplate::$MODE_DAFTAR, $polislug, $noreg_pasien, $nrm_pasien, $nama_pasien, $jk, $page, $action, $pslug, $pname, $pimplement, $kelas );
$template->initialize ();
$result = ob_get_clean ();
echo json_encode ( $result );

?>