<?php
	global $db;
	
	if (isset($_POST['noreg_pasien'])) {
		$noreg_pasien = $_POST['noreg_pasien'];
		
		$rows = $dbtable->get_result("
			SELECT kelas, periksa, harga
			FROM smis_rad_pesanan
			WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop NOT LIKE 'del'
		");
		$tindakan_jaspel = array();
		foreach ($rows as $row) {
			$layanan_arr = json_decode($row->periksa, true);
			$harga_arr = json_decode($row->harga, true);
			foreach ($layanan_arr as $key => $value) {
				if ($value == 1) {
					$harga_layanan = $harga_arr[$row->kelas . "_" . $key];
					$layanan_row = $dbtable->get_row("
						SELECT *
						FROM smis_rad_layanan
						WHERE slug = '" . $key . "'
						LIMIT 0, 1
					");
					$nama_layanan = ArrayAdapter::format("unslug", $layanan_row->nama);
					$jaspel_row = $dbtable->get_row("
						SELECT *
						FROM jaspel_radiologi_2016
						WHERE layanan = '" . $nama_layanan . "'
						LIMIT 0, 1
					");
					$jaspel_layanan = 0;
					if ($jaspel_row != null)
						$jaspel_layanan = $jaspel_row->jaspel;
					$d_tindakan_jaspel = array(
						"nama_tindakan"		=> $nama_layanan,
						"harga_tindakan"	=> $harga_layanan,
						"jaspel_tindakan"	=> $jaspel_tindakan
					);
					$tindakan_jaspel[] = $d_tindakan_jaspel;
				}
			}
		}
		
		$data = array();
		$data['data'] = array(
			"ruangan"			=> "radiologi",
			"tindakan_jaspel" 	=> $tindakan_jaspel
		);
		echo json_encode($data);
	}
?>