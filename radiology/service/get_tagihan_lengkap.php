<?php
global $db;
require_once 'radiology/resource/RadiologyResource.php';
$radiology = new RadiologyResource();
$names = $radiology->list_harga;
if (isset($_POST['noreg_pasien'])) {
	$noreg = $_POST['noreg_pasien'];	
	$dbtable = new DBTable ($db, "smis_rad_pesanan");
	$dbtable->addCustomKriteria ( "noreg_pasien", "='" . $noreg . "'" );
	$dbtable->setShowAll ( true );
	$data = $dbtable->view ( "", "0" );
	$rows = $data ['data'];
	$result = array();
	foreach ( $rows as $d ) {        
		/* pemeriksaan lain */
        $biaya_lain=0;
        $keterangan_lain="";
        $query="SELECT *
                FROM smis_rad_dpesanan_lain
                WHERE id_pesanan = '$d->id' AND prop != 'del'";
		$layanan_lain_row = $db->get_result($query);
		foreach($layanan_lain_row as $x){
            $biaya_lain=$x->jumlah*$x->harga_layanan;
            $biaya_lain = (string)$biaya_lain;
            if($biaya_lain==0){
                continue;
            }                
            $keterangan_lain=$x->nama_layanan." ".ArrayAdapter::format("only-money Rp.",$x->harga_layanan)." x ".$x->jumlah;
            $result[] = array(
					'id' => $d->id."_biaya_lain_".$x->id,
					'nama' => (empty($d->no_lab) ? "#$d->id" : "$d->no_lab")." Biaya Lain - ".$x->nama_layanan,
					'waktu' => ArrayAdapter::format("date d M Y", $d->tanggal),
					'ruangan' => $d->ruangan,
					'start' =>$d->tanggal,
					'end' => $d->tanggal,
					'biaya' => $biaya_lain,
					'jumlah' => $x->jumlah,
					'keterangan' => $keterangan_lain,
                    'debet' => getSettings($db, "radiology-accounting-debit-pesanan-lain", ""),
					'kredit' => getSettings($db, "radiology-accounting-kredit-pesanan-lain", ""),
			);
        }
		
		/* nama dokter konsultan */
		if($d->biaya_konsul>0){
			$result[] = array(
					'id' => $d->id."_biaya_konsul",
					'nama' => (empty($d->no_lab) ? "#$d->id" : "$d->no_lab")." Biaya Konsul ".$d->nama_konsultan,
					'waktu' => ArrayAdapter::format("date d M Y", $d->tanggal),
					'ruangan' => $d->ruangan,
					'start' =>$d->tanggal,
					'end' => $d->tanggal,
					'biaya' => $d->biaya_konsul,
					'jumlah' => 1,
					'keterangan' => "Biaya Konsul ".$d->nama_konsultan,
                    'debet' => getSettings($db, "radiology-accounting-debit-biaya-konsul", ""),
					'kredit' => getSettings($db, "radiology-accounting-kredit-biaya-konsul", ""),
			);
		}
		
        /*pemeriksaan reguler*/
        $periksa = json_decode($d->periksa, true);
		$harga = json_decode($d->harga, true);
		$kls = str_replace (" ", "_", $d->kelas);
		foreach ($periksa as $p => $val) {
			if ($val == "1") {
				//$ket['periksa'][$names[$p]] = $harga[$kls.'_'.$p];
                $dk=$radiology->getDebitKredit($p,$d->carabayar);				
				$result[] = array(
						'id' => $d->id."_".$p,
						'nama' => (empty($d->no_lab) ? "#$d->id" : "$d->no_lab")." ".$names[$p],
						'waktu' => ArrayAdapter::format("date d M Y", $d->tanggal),
						'ruangan' => $d->ruangan,
						'start' =>$d->tanggal,
						'end' => $d->tanggal,
						'biaya' => $harga[$kls.'_'.$p],
						'jumlah' => 1,
						'keterangan' => "",
                        'debet' => $dk['d'],
						'kredit' => $dk['k'],
				);
				
			}
		}		
		
	}
	
	$query = "SELECT count(*) as total FROM smis_rad_pesanan WHERE noreg_pasien = '".$noreg."' AND `status` !='Selesai' AND prop!='del' ";
	$ct = $db->get_var($query);
	$selesai = $ct>0?"0":"1";

	echo json_encode (array(
		'selesai' => $selesai,
		'exist' => "1",
		'reverse' => "0",
		'cara_keluar' => "Selesai",
		'jasa_pelayanan' => "1",
		'data' => array( 
			'radiology' => array(
				'result' => $result,
				'jasa_pelayanan' => '1'
			)
		)
	));
}

?>
