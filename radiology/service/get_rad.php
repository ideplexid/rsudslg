<?php 
/**
 * using for get the list of Doctor Treatment
 * used for Resume Medic of each patient on each Number of Registration
 * 
 * @database : smis_rad_pesanan
 * @used	 : - medical_record/modul/resume_medis.php
 * 			   - rawat/class/template/ResumeMedis.php
 * @author 	 : Nurul Huda
 * @copyright: goblooge@gmail.com
 * @license  : LGPLv3
 * @since	 : 14 may 2015
 * @version	 : 1.0.1
 * */
	$dbtable=new DBTable($db, "smis_rad_pesanan");
	$dbtable->setUseWhereforView(true);
	//$save=array("kode_icd","nama_icd","kode_dtd","kode_grup");
	//$dbtable->setOrder("kode_icd , nama_dokter, nama_pasien ");
	if(isset($_POST['noreg_pasien'])){
		$dbtable->addCustomKriteria("noreg_pasien", "='".$_POST['noreg_pasien']."'");
		$dbtable->setShowAll(true);
	}
	$service=new ServiceProvider($dbtable);
	$pack=$service->command($_POST['command']);
	echo json_encode($pack);
?>