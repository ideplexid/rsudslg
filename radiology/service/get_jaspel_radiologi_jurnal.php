<?php
	global $db;
	
	if (isset($_POST['noreg_pasien'])) {
		$noreg_pasien = $_POST['noreg_pasien'];
		
		$dbtable = new DBTable($db, "smis_rad_pesanan");
		$rows = $dbtable->get_result("
			SELECT periksa
			FROM smis_rad_pesanan
			WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop NOT LIKE 'del'
		");
		$jaspel = 0;
		foreach ($rows as $row) {
			$periksa = json_decode($row->periksa, true);
			foreach ($periksa as $key => $value)
				if ($value == "1") {
					$id_layanan = str_replace("rad_", "", $key);
					$layanan_row = $dbtable->get_row("
						SELECT nama
						FROM smis_rad_layanan
						WHERE id = '" . $id_layanan . "'
					");
					if ($layanan_row != null) {
						$jaspel_row = $dbtable->get_row("
							SELECT jaspel
							FROM jaspel_radiologi_2016
							WHERE layanan LIKE '" . $layanan_row->nama . "'
						");
						$jaspel += $jaspel_row->jaspel;
					}
				}
		}
		
		$data = array();
		$data['data'] = array(
			"ruangan"	=> "radiology",
			"jaspel" 	=> $jaspel
		);
		echo json_encode($data);
	}
?>