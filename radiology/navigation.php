<?php
	global $NAVIGATOR;
	$mr = new Menu ("fa fa-bullseye");
	$mr->addProperty ( 'title', 'Radiology' );
	$mr->addProperty ( 'name', 'Radiology' );
	$mr->addSubMenu ( "Daftar Pasien", "radiology", "pemeriksaan", "Daftar Pasien radiology" ,"fa fa-wheelchair");
    
    // bagian arsip modul radiologi
    $mr->addSubMenu ( "Arsip", "radiology", "arsip_menu", "Arsip Radiology" ,"fa fa-archive");
    $mr->addSubMenu ( "Data Induk", "radiology", "data_induk", "Data Induk" ,"fa fa-check");
    $mr->addSubMenu ( "Laporan", "radiology", "laporan_menu", "Laporan" ,"fa fa-bar-chart");
	$mr->addSubMenu ( "Settings", "radiology", "settings", "Settings Dari Radiology" ,"fa fa-gear");
	$mr->addSeparator ();
	require_once 'smis-libs-inventory/navigation.php';
	$inventory_navigator = new InventoryNavigator($NAVIGATOR, "", "Radiology", "radiology");
	$mr = $inventory_navigator->extendMenu($mr);
	$NAVIGATOR->addMenu ( $mr, 'radiology' );
?>
