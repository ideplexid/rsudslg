<?php
global $PLUGINS;

$init ['name'] = 'radiology';
$init ['path'] = SMIS_DIR . "radiology/";
$init ['description'] = "radiology";
$init ['require'] = "administrator";
$init ['service'] = "";
$init ['version'] = "3.3.2";
$init ['number'] = "32";
$init ['type'] = "";

$myplugins = new Plugin ( $init );
$PLUGINS [$init ['name']] = $myplugins;
?>
