<?php 
$tab = new Tabulator ( "arsip_menu", "",Tabulator::$POTRAIT );
$tab->add ( "arsip", "Arsip Terbaca", "", Tabulator::$TYPE_HTML,"fa fa-ambulance" );
$tab->add ( "unread", "Arsip Tidak Terbaca", "", Tabulator::$TYPE_HTML,"fa fa-medkit" );
$tab->add ( "arsip_admin", "Arsip Admin", "", Tabulator::$TYPE_HTML,"fa fa-user-md" );
$tab->setPartialLoad(true,"radiology","arsip_menu","arsip_menu",true);
echo $tab->getHtml ();
?>