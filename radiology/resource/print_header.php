<?php
	global $db;
	$nama_konsultan = getSettings ( $db, "radiology-konsultan-nama", "" );
	$id_konsultan = getSettings ( $db, "radiology-konsultan-id", "" );
	$gaptop= getSettings ( $db, "radiology-print-header-gap-top", "0" );
	$gaptom= getSettings ( $db, "radiology-print-header-gap-bottom", "0" );
	$title_fs= getSettings ( $db, "radiology-print-header-size", "18" );
	$show= getSettings ( $db, "radiology-print-header", "show" );
	$title = getSettings ( $db, "smis_autonomous_title", "" );
	$logo = getLogo ();
    $logo_dinas = "smis-upload/".getSettings ( $db, "smis_autonomous_dinas_logo", "" );
	$adress = getSettings ( $db, "smis_autonomous_address", "" );
	$telp1 = getSettings ( $db, "smis_autonomous_contact", "" );
	$telp2 = getSettings ( $db, "smis_autonomous_contact2", "" );
	
	$utk = ": <t id='untuk'><font  class='label label-important' id='utk_rm'>Rekam Medis</font>";
	$utk .= " <font class='label label-success' id='utk_radiologi'>Radiologi</font>";
	$utk .= " <font class='label label-success' id='utk_dokter'>Pasien</font>";
	$utk .= " <font class='label label-info' id='utk_asuransi'>IKS</font><t>";
	
	$tp = new TablePrint ( "pheader" );
	$tp->setTableClass ( "pheader" );
	$tp->setMaxWidth ( false );
	
	if($show=="show"){
		$tp	->addColumn ( "<img src='" . $logo_dinas . "' />", 1, 2, null, "header_logo_dinas" )
            ->addColumn ( $title, 5, 1, null, "autonomous_title" )
            ->addColumn ( "<img src='" . $logo . "' />", 1, 2, null, "header_logo" )
			->commit ( "title" );
		$tp	->addColumn ( "<u>".$adress."</u>", 5, 1, null, "autonomous_address" )
			->commit ( "title" );
        $tp	->addColumn ( "TELP. ".$telp1." / ".$telp2, 5, 1, null, "autonomous_address" )
			->commit ( "title" );
	}
	
	if($gaptop!="0"){
		$tp	->addColumn ( "<div style='height:".$gaptop."px !important; width:100%'>&nbsp;</div>", 5, 1, null, "" )
			->commit ( "title" );
	}	
	$tp	->addColumn ( "<font style='font-size:".$title_fs."px;'>HASIL PEMERIKSAAN RADIOLOGI</font>", 4, 1 )
		->commit ( "title" );
	if($gaptom!="0"){
		$tp	->addColumn ( "<div style='height:".$gaptom."px !important; width:100%'>&nbsp;</div>", 5, 1, null, "" )
		->commit ( "title" );
	}
    $tp	->addColumn ( "Nama", 1, 1, null, null, "phttl" )
		->addColumn ( "", 1, 1, null, "ph_nama" )
		->addColumn ( "Tanggal", 1, 1, null, null, "phttl" )
		->addColumn ( "", 1, 1, null, "ph_tanggal" )
		->commit ( "header" );
    $tp	->addColumn ( "Jenis Kelamin", 1, 1, null, null, "phttl" )
		->addColumn ( "", 1, 1, null, "ph_jk" )
		->addColumn ( "Jenis Pemeriksaan", 1, 1, null, null, "phttl" )
		->addColumn ( "", 1, 1, null, "ph_pemeriksaan" )
		->commit ( "header" );
    $tp	->addColumn ( "Umur", 1, 1, null, null, "phttl" )
		->addColumn ( "", 1, 1, null, "ph_umur" )
		->addColumn ( "No. Radiologi", 1, 1, null, null, "phttl" )
		->addColumn ( "", 1, 1, null, "ph_nolab" )
		->commit ( "header" );
    $tp	->addColumn ( "No. RM", 1, 1, null, null, "phttl" )
		->addColumn ( "", 1, 1, null, "ph_nrm" )
		->addColumn ( "Datang", 1, 1, null, null, "phttl" )
		->addColumn ( "", 1, 1, null, "ph_datang" )
		->commit ( "header" );
    $tp	->addColumn ( "Alamat", 1, 1, null, null, "phttl" )
		->addColumn ( "", 1, 1, null, "ph_alamat" )
		->addColumn ( "Waktu Cetak", 1, 1, null, null, "phttl" )
		->addColumn ( "", 1, 1, null, "ph_selesai" )
		->commit ( "header" );
    $tp	->addColumn ( "Pengirim", 1, 1, null, null, "phttl" )
		->addColumn ( "", 1, 1, null, "ph_pengirim" )
        ->addColumn ( "Untuk", 1, 1, null, null, "phttl" )
		->addColumn ( $utk, 1, 1, null, "ph_utk" )
		->commit ( "header" );
	/*$tp	->addColumn ( "Nama", 1, 1, null, null, "phttl" )
		->addColumn ( "", 1, 1, null, "ph_nama" )
		->addColumn ( "No. RM", 1, 1, null, null, "phttl" )
		->addColumn ( "", 1, 1, null, "ph_nrm" )
		->commit ( "header" );
	$tp	->addColumn ( "Umur", 1, 1, null, null, "phttl" )
		->addColumn ( "", 1, 1, null, "ph_umur" )
		->addColumn ( "Alamat", 1, 1, null, null, "phttl" )
		->addColumn ( "", 1, 1, null, "ph_alamat" )
		->commit ( "header" );
	$tp	->addColumn ( "Tanggal", 1, 1, null, null, "phttl" )
		->addColumn ( "", 1, 1, null, "ph_tgl" )
		->addColumn ( "Untuk", 1, 1, null, null, "phttl" )
		->addColumn ( $utk, 1, 1, null, "ph_utk" )
		->commit ( "header" );
	$tp	->addColumn ( "No. Radiologi", 1, 1, null, null, "phttl" )
		->addColumn ( "", 1, 1, null, "ph_nolab" )
		->commit ( "header" );
	$tp	->addColumn ( "Datang", 1, 1, null, null, "phttl" )
		->addColumn ( "", 1, 1, null, "ph_datang" )
		->addColumn ( "Alamat", 1, 1, null, null, "phttl" )
		->addColumn ( "", 1, 1, null, "ph_alamat" )
		->commit ( "header" );
    $tp ->addColumn ( "Waktu Cetak", 1, 1, null, null, "phttl" )
		->addColumn ( "", 1, 1, null, "ph_selesai" )
		->addColumn ( "Response Time", 1, 1, null, null, "phttl" )
		->addColumn ( "", 1, 1, null, "ph_response" )
		->commit ( "header" );
    $tp ->addColumn ( "Jenis Pemeriksaan", 1, 1, null, null, "phttl" )
		->addColumn ( "", 1, 1, null, "ph_pemeriksaan" )
		->addColumn ( "Response Time", 1, 1, null, null, "phttl" )
		->addColumn ( "", 1, 1, null, "ph_response_1" )
		->commit ( "header" );*/
    echo "<div id='print_header_radiology' class='hide'>" . $tp->getHtml () . "</div>";
?>














