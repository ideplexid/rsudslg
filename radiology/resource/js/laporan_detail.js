var laporan_detail;
$(document).ready(function(){
	$('.mydate').datepicker();
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
	var column=new Array('id','nama','keterangan','slug');
	laporan_detail=new TableAction("laporan_detail","radiology","laporan_detail",column);
	laporan_detail.getRegulerData=function(){
		var reg_data={	
			page:this.page,
			action:this.action,
			super_command:this.super_command,
			prototype_name:this.prototype_name,
			prototype_slug:this.prototype_slug,
			prototype_implement:this.prototype_implement,
			dari:$("#"+this.prefix+"_dari").val(),
			sampai:$("#"+this.prefix+"_sampai").val(),
			layanan:$("#"+this.prefix+"_layanan").val(),
			carabayar:$("#"+this.prefix+"_carabayar").val()
		};
		return reg_data;
	};
	laporan_detail.view();	
});