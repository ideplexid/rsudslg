var laporan_perlayanan_pasien;

$(document).ready(function() {
    $('.mydate').datepicker();
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
    
    var column=new Array('id','tanggal','nama_pasien','nrm_pasien','noreg_pasien','no_rad','kelas','jenis','ruangan','total_tagihan','layanan','harga_jaspel','nama_konsutan');
    laporan_perlayanan_pasien = new TableAction("laporan_perlayanan_pasien","radiology","laporan_perlayanan_pasien",column);

    $("#laporan_perlayanan_pasien_layanan").on("change",function(){
		laporan_perlayanan_pasien.view();
	});
    
    laporan_perlayanan_pasien.getRegulerData=function(){
		var reg_data={	
				page:this.page,
				action:this.action,
				super_command:this.super_command,
				prototype_name:this.prototype_name,
				prototype_slug:this.prototype_slug,
				prototype_implement:this.prototype_implement,
                dari:$("#"+this.prefix+"_dari").val(),
                sampai:$("#"+this.prefix+"_sampai").val(),
                layanan:$("#"+this.prefix+"_layanan").val(),
				};
		return reg_data;
	};
    
    laporan_perlayanan_pasien.posting=function() {
        var clear_data=this.getRegulerData();
        clear_data['super_command']="posting";
        console.log(clear_data['dari']);
        $.post("",clear_data,function(res){
            var json=getContent(res);
            laporan_perlayanan_pasien.view();
        });
    };
    
    //laporan_perlayanan_pasien.view();
});