function RadiologyAction(name, page, action, column) {
this.initialize(name, page, action, column);
}
RadiologyAction.prototype.constructor = RadiologyAction;
RadiologyAction.prototype = new TableAction();

RadiologyAction.prototype.getRegulerData=function(){
	var reg_data={	
			page:this.page,
			action:this.action,
			super_command:this.super_command,
			prototype_name:this.prototype_name,
			prototype_slug:this.prototype_slug,
			prototype_implement:this.prototype_implement,
			polislug:rad_polislug,
			noreg_pasien:rad_noreg,
			nama_pasien:rad_nama_pasien,
			nrm_pasien:rad_nrm_pasien,
			mode:RAD_MODE,
			jk:RAD_JK
            };
	return reg_data;
};

RadiologyAction.prototype.chooser = function(a,b,c,d,e){
	if(c=="rad_pasien" && this.get("id")!="" && this.get("id")!="0"){
		return;
	}else{
		TableAction.prototype.chooser.call(this,a,b,c,d,e);
	}
};

RadiologyAction.prototype.selesai=function(id){
	var reg_data={	
			page:this.page,
			action:this.action,
			super_command:this.super_command,
			prototype_name:this.prototype_name,
			prototype_slug:this.prototype_slug,
			prototype_implement:this.prototype_implement,
			polislug:rad_polislug,
			mode:RAD_MODE,
			};
	reg_data['command']='save';
	reg_data['id']=id;
	reg_data['selesai']=1;
	var self=this;
	showLoading();
	$.post('',reg_data,function(res){
		var json=getContent(res);
		self.view();
		dismissLoading();					
	});
	
};

RadiologyAction.prototype.kembalikan=function(id){
	var reg_data={	
			page:this.page,
			action:this.action,
			super_command:this.super_command,
			prototype_name:this.prototype_name,
			prototype_slug:this.prototype_slug,
			prototype_implement:this.prototype_implement,
			polislug:rad_polislug,
			mode:RAD_MODE,
			};
	reg_data['command']='save';
	reg_data['id']=id;
	reg_data['selesai']=0;
	var self=this;
	showLoading();
	$.post('',reg_data,function(res){
		var json=getContent(res);
		self.view();
		dismissLoading();					
	});
	
};

RadiologyAction.prototype.show_add_form=function(){
	this.clear();
	this.show_form();
	clear_hasil();
};

RadiologyAction.prototype.tidak_terbaca=function(id){
	var reg_data={	
			page:this.page,
			action:this.action,
			super_command:this.super_command,
			prototype_name:this.prototype_name,
			prototype_slug:this.prototype_slug,
			prototype_implement:this.prototype_implement,
			polislug:rad_polislug,
			mode:RAD_MODE,
			jk:RAD_JK
			};
	reg_data['command']='save';
	reg_data['id']=id;
	reg_data['selesai']=-1;
	var self=this;
	showLoading();
	$.post('',reg_data,function(res){
		var json=getContent(res);
		self.view();
		dismissLoading();					
	});
};


RadiologyAction.prototype.tidak_jadi=function(id){

    showLoading();
    var cek = this.cekTutupTagihan();
    if(cek){
        var reg_data=this.getRegulerData();
        reg_data['command']='save';
        reg_data['id']=id;
        reg_data['selesai']=-2;
        var self=this;
        showLoading();
        $.post('',reg_data,function(res){
            var json=getContent(res);
            self.view();
            dismissLoading();					
        });
    }
    dismissLoading();	
	
};

RadiologyAction.prototype.getSaveData=function(){
	var save_data=this.getRegulerData();
	save_data['command']="save";
	save_data['id']=$("#"+this.prefix+"_id").val();
	if(RAD_MODE=="arsip_terbaca"){
		/* pada mode arsip usr tidak diperkenankan untuk melaklukan penyimpanan data_hasil
		 * sehingga data yang ada akan dibuat sedemikian rupa , 
		 * ketika save tidak ada data yang di post
		 * kan*/
		return save_data;
	}
	for(var i=0;i<this.column.length;i++){
		var name=this.column[i];
		var typical=$("#"+this.prefix+"_"+name).attr('typical');
		var type=$("#"+this.prefix+"_"+name).attr('type');
		if(typical=="money"){
			save_data[name]=$("#"+this.prefix+"_"+name).maskMoney('unmasked')[0];
		}else if(type=="checkbox"){
			save_data[name]=$("#"+this.prefix+"_"+name).is(':checked')?1:0;
		}else{
			save_data[name]=$("#"+this.prefix+"_"+name).val();
		}
	}
	
	if(RAD_MODE=="pendaftaran" || RAD_EDIT_HASIL=="1"){
		/**
		 * ketika berada pada mode daftar
		 * yang mana dipakai oleh petugas ruangan yang melakukan inputan
		 * maka hasil tidak boleh diubah, tetapi data pemesanan boleh berubah
		 * */
		var data_pe
		var data_pesan={};
		for (var i = 0; i < RAD_LIST_PESAN.length; i++) {
			var name_list=RAD_LIST_PESAN[i];
			data_pesan[name_list]=$("#radiology_"+name_list).is(':checked')?1:0;
		}
		save_data['periksa']=data_pesan;	
	}
	
	if(RAD_MODE=="pemeriksaan" || RAD_EDIT_LAYANAN=="1"){
		/**
		 * ketika berada pada mode pemeriksaan
		 * yang mana dilakukan oleh petugas laboratory
		 * maka hasil boleh berubah tetapi pemesiksaan tidak boleh berubah
		 * */
		var data_hasil={};
		for (var i = 0; i < RAD_LIST_HASIL.length; i++) {
			var name_list=RAD_LIST_HASIL[i];
			data_hasil[name_list]=$("#radiology_hasil_"+name_list).code();
		}
		save_data['hasil']=data_hasil;
	}
    
    // Film
    save_data['froll']     = $("#film_froll").val();
    save_data['f1824']     = $("#film_f1824").val();
    save_data['f2430']     = $("#film_f2430").val();
    save_data['f3040']     = $("#film_f3040").val();
    save_data['f3535']     = $("#film_f3535").val();
    save_data['f3543']     = $("#film_f3543").val();
    save_data['sdq4335']   = $("#film_sdq4335").val();
    save_data['dhf3543']   = $("#film_dhf3543").val();
    save_data['dhf2636']   = $("#film_dhf2636").val();
    save_data['dhf2025']   = $("#film_dhf2025").val();
    save_data['dvb3543']   = $("#film_dvb3543").val();
    save_data['dvb3528']   = $("#film_dvb3528").val();
    save_data['dvb2025']   = $("#film_dvb2025").val();
    save_data['fdental']   = $("#film_fdental").val();
    save_data['frollr']    = $("#film_frollr").val();
    save_data['f1824r']    = $("#film_f1824r").val();
    save_data['f2430r']    = $("#film_f2430r").val();
    save_data['f3040r']    = $("#film_f3040r").val();
    save_data['f3535r']    = $("#film_f3535r").val();
    save_data['f3543r']    = $("#film_f3543r").val();
    save_data['fdentalr']  = $("#film_fdentalr").val();
	
	// layanan lain :
	var data_layanan_lain = {};
	var nor = $("tbody#layanan_lain_list").children("tr").length;
	for (var i = 0; i < nor; i++) {
		var layanan_prefix = $("tbody#layanan_lain_list").children("tr").eq(i).prop("id");
		var id = $("#" + layanan_prefix + "_id").text();
		var nama = $("#" + layanan_prefix + "_nama").text();
		var harga = $("#" + layanan_prefix + "_harga").text();
		var jumlah = $("#" + layanan_prefix + "_jumlah").text();
		var d_data = {};
		d_data['id'] = id;
		d_data['nama'] = nama;
		d_data['harga'] = parseFloat(harga.replace(/[^0-9-,]/g, '').replace(",", "."));
		d_data['jumlah'] = jumlah;
		if ($("#" + layanan_prefix).attr("class") == "deleted") {
			d_data['cmd'] = "delete";
		} else if (id == "") {
			d_data['cmd'] = 'insert';
		} else {
			d_data['cmd'] = 'update';
		}
		data_layanan_lain[i] = d_data;
	}
	save_data['layanan_lain'] = JSON.stringify(data_layanan_lain);
	return save_data;
};

/*CLEAR*/
RadiologyAction.prototype.clear=function(){
	for(var i=0;i<this.column.length;i++){
		var name=this.column[i];	
			if($("#"+this.prefix+"_"+name).is(':checkbox')){
				$("#"+this.prefix+"_"+name).attr('checked', false).change();
				$("."+name).hide();
			}else if($("#"+this.prefix+"_"+name).attr('typical')=="money"){
				var val=$("#"+this.prefix+"_"+name).attr("dv");
				$("#"+this.prefix+"_"+name).maskMoney('mask',Number(val));
			}else{
				var val=$("#"+this.prefix+"_"+name).attr("dv");
				$("#"+this.prefix+"_"+name).val(val);
			}
	}
	this.enabledOnNotEdit(this.column_disabled_on_edit);	
	for (var i = 0; i < RAD_LIST_HASIL.length; i++) {
		var name_list=RAD_LIST_HASIL[i];
		$("#radiology_hasil_"+name_list).html("");
	}
	
	for (var i = 0; i < RAD_LIST_PESAN.length; i++) {
		var name_list=RAD_LIST_PESAN[i];
		$("#radiology_"+name_list).prop('checked', false).change();
		
	}
	
	//clear layanan lain:
	$("tbody#layanan_lain_list").html("");
	layanan_lain_num = 0;
};
/*END OF CLEAR*/

var RAD_EDIT_PRINT=false;
RadiologyAction.prototype.printelement=function(id){
	RAD_EDIT_PRINT=true;
	this.edit(id);
};

RadiologyAction.prototype.edit=function (id){
	var self=this;
	showLoading();	
	var edit_data=this.getEditData(id);
	$.post('',edit_data,function(res){		
		var json=getContent(res);
        console.log(json);
		if(json==null) return;
		
		for(var i=0;i<self.column.length;i++){
			var name=self.column[i];
			var typical=$("#"+self.prefix+"_"+name).attr('typical');
			var type=$("#"+self.prefix+"_"+name).attr('type');
			if(typical=="money"){
				$("#"+self.prefix+"_"+name).maskMoney('mask',Number(json[""+name]));
			}else if(type=="checkbox"){
				if(json[""+name]=="1") {
					$("#"+self.prefix+"_"+name).prop('checked', true).change();
					$("."+name).show();
				}else{
					$("#"+self.prefix+"_"+name).prop('checked', false).change();
					$("."+name).hide();
				}
			}else{
				$("#"+self.prefix+"_"+name).val(json[""+name]);
			}
		}
        // film 
        $("#film_froll").val(json['froll']);
        $("#film_f1824").val(json['f1824']);
        $("#film_f2430").val(json['f2430']);
        $("#film_f3040").val(json['f3040']);
        $("#film_f3535").val(json['f3535']);
        $("#film_f3543").val(json['f3543']);
        $("#film_sdq4335").val(json['sdq4335']);
        $("#film_dhf3543").val(json['dhf3543']);
        $("#film_dhf2636").val(json['dhf2636']);
        $("#film_dhf2025").val(json['dhf2025']);
        $("#film_dvb3543").val(json['dvb3543']);
        $("#film_dvb3528").val(json['dvb3528']);
        $("#film_dvb2025").val(json['dvb2025']);
        $("#film_fdental").val(json['fdental']);
        $("#film_frollr").val(json['frollr']);
        $("#film_f1824r").val(json['f1824r']);
        $("#film_f2430r").val(json['f2430r']);
        $("#film_f3040r").val(json['f3040r']);
        $("#film_f3535r").val(json['f3535r']);
        $("#film_f3543r").val(json['f3543r']);
        $("#film_fdentalr").val(json['fdentalr']);
		
		// layanan lain:
		layanan_lain_num = json['layanan_lain_num'];
		$("tbody#layanan_lain_list").html(json['layanan_lain_html']);
		layanan_lain.refresh_number();
		
		fill_header_radiology(self.prefix,json);
		if(json['hasil']!=""){
			try{
				list_hasil_json=$.parseJSON(json['hasil']);
				for (var i = 0; i < RAD_LIST_HASIL.length; i++) {
					var name_list=RAD_LIST_HASIL[i];
					var value_list=list_hasil_json[name_list];
					$("#radiology_hasil_"+name_list).val(value_list);
				}
			}catch(e){
				console.log(e);
			}
		}try{
			if(json['periksa']!=""){
				list_periksa_json=$.parseJSON(json['periksa']);
				for (var i = 0; i < RAD_LIST_PESAN.length; i++) {
					var name_list=RAD_LIST_PESAN[i];
					var value_list=list_periksa_json[name_list];
					if(value_list=="1") $("#radiology_"+name_list).prop('checked', true).change();
					else $("#radiology_"+name_list).prop('checked', false).change();
					rad_hide_show_hasil("radiology_"+name_list);
				}
			}else{
				$(".radiology_checkbox").prop('checked', false);
				$(".div_hasil_radiology").hide();
			}						
		}catch(e){}					
		dismissLoading();
		self.disabledOnEdit(self.column_disabled_on_edit);
		if(RAD_EDIT_PRINT){
			rad_print();
			RAD_EDIT_PRINT=false;
		}else{
            var p=$("#lab_diagnosa_anchor").parent();
            if($(p).hasClass("active")){
                $("#lab_diagnosa_anchor").trigger("click");
            }
			self.show_form();
		}
        
	});
};

RadiologyAction.prototype.eresep=function (id){
	var self=this;
	showLoading();	
	var edit_data=this.getEditData(id);
	$.post('',edit_data,function(res){		
		var json=getContent(res);
		if(json==null) return;
		var noreg_pasien=json.noreg_pasien;
		var x={
			page:"radiology",
			action:"e_resep",
			proto_implement:"",
			proto_slug:"",
			proto_name:"",
			noreg_pasien:noreg_pasien
		};
		LoadSmisPage(x);
		dismissLoading();
	});
};

RadiologyAction.prototype.save = function(){
    showLoading();
    var cek = this.cekTutupTagihan();
    if(cek){
        TableAction.prototype.save.call(this);        
    }
    dismissLoading();
};

RadiologyAction.prototype.cekTutupTagihan = function(){
    var reg_data={	
        page:this.page,
        action:this.action,
        super_command:this.super_command,
        prototype_name:this.prototype_name,
        prototype_slug:this.prototype_slug,
        prototype_implement:this.prototype_implement,
        polislug:rad_polislug,
        };			
    var noreg                 = $("#"+this.prefix+"_noreg_pasien").val();
    if(noreg==""){
        smis_alert("Error","Silakan Pilih Pasien Terlebih Dahulu","alert-danger");
        return;
    }
	reg_data['command']        = 'cek_tutup_tagihan';
	reg_data['noreg_pasien']  = noreg;
    
    var res = $.ajax({
        type: "POST",
        url: "",
        data:reg_data,
        async: false
    }).responseText;

    var json = getContent(res);
    if(json=="1"){
        return false;
    }else{
        return true;
    }
};

RadiologyAction.prototype.rehab_medik=function(id){
	LoadSmisPage({
        page:this.page,
        action:"rehab_medik",
        prototype_name:this.prototype_name,
		prototype_slug:this.prototype_slug,
        prototype_implement:this.prototype_implement,
        id_antrian:id
    });
};

RadiologyAction.prototype.pelayanan_khusus=function(id){
    LoadSmisPage({
        page:this.page,
        action:"pelayanan_khusus",
        prototype_name:this.prototype_name,
		prototype_slug:this.prototype_slug,
        prototype_implement:this.prototype_implement,
        id_antrian:id
    });	
};

RadiologyAction.prototype.lap_rl52=function(id){
    LoadSmisPage({
        page:this.page,
        action:"lap_rl52",
        prototype_name:this.prototype_name,
		prototype_slug:this.prototype_slug,
        prototype_implement:this.prototype_implement,
        id_antrian:id
    });	
};
