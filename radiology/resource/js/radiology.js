var RAD_ACTION;
var RAD_PREFIX=$("#RAD_PREFIX").val();
var RAD_JK=$("#RAD_JK").val();		
var RAD_LIST_HASIL=$.parseJSON($("#RAD_LIST_HASIL").val());
var RAD_LIST_PESAN=$.parseJSON($("#RAD_LIST_PESAN").val());

var RAD_MODE=$("#RAD_MODE").val();
var rad_noreg=$("#RAD_NOREG").val();
var rad_nama_pasien=$("#RAD_NAMA").val();
var rad_nrm_pasien=$("#RAD_NRM").val();
var rad_polislug=$("#RAD_POLISLUG").val();
var rad_the_page=$("#RAD_PAGE").val();
var rad_the_protoslug=$("#RAD_PROTOSLUG").val();
var rad_the_protoname=$("#RAD_PROTONAME").val();
var rad_the_protoimplement=$("#RAD_PROTOIMPLEMENT").val();			
var layanan_lain;
var layanan_lain_num;

var RAD_EDIT_HASIL=$("#RAD_EDIT_HASIL").val();	
var RAD_EDIT_LAYANAN=$("#RAD_EDIT_LAYANAN").val();	

$(document).ready(function() {
	layanan_lain = new LayananLainAction("layanan_lain",rad_the_page,RAD_PREFIX,new Array("harga", "subtotal"));
	$("#layanan_lain_harga, #layanan_lain_jumlah").on("keyup", function(e) {
		if (e.which == 13)
			return;
		$(".btn").removeAttr("disabled");
		$(".btn").attr("disabled", "disabled");
		var harga = $("#layanan_lain_harga").val().replace(/[^0-9-,]/g, '').replace(",", ".");
		var jumlah = $("#layanan_lain_jumlah").val();
		if (!is_numeric(jumlah))
			jumlah = 0;
		var subtotal = parseFloat(jumlah) * parseFloat(harga);
		subtotal = "Rp. " + (parseFloat(subtotal)).formatMoney("2", ".", ",");
		$("#layanan_lain_subtotal").val(subtotal);
		$(".btn").removeAttr("disabled");
	});
	
	$("#layanan_lain_nama").on("keyup", function(e) {
		if (e.which == 13) {
			$("#layanan_lain_harga").focus();
		}
	});
	
	$("#layanan_lain_harga").on("keyup", function(e) {
		if (e.which == 13) {
			$("#layanan_lain_jumlah").focus();
		}
	});
	
	$("#layanan_lain_jumlah").on("keyup", function(e) {
		if (e.which == 13) {
			$('#layanan_lain_save_btn').trigger('click');
		}
	});
	
	
				
	$(".mydate").datepicker();
	$('.mydatetime').datetimepicker({ minuteStep: 1});
	$(document).delegate('textarea', 'keydown', function(e) {
		  var keyCode = e.keyCode || e.which;
		  if (keyCode == 9) {
			e.preventDefault();
			var start = $(this).get(0).selectionStart;
			var end = $(this).get(0).selectionEnd;
			$(this).val($(this).val().substring(0, start) + "\t" + $(this).val().substring(end));
			$(this).get(0).selectionStart =
			$(this).get(0).selectionEnd = start + 1;
		  }
		});
		
	$(".radiology_checkbox input").on("change",function(e){
		var id_attr=$(this).attr("id");
		rad_hide_show_hasil(id_attr);
		
		
	});

	$(".radiology_checkbox input").on("click",function(e){
		let wktdtg = $("#"+RAD_PREFIX+"_waktu_datang").val();
		if(wktdtg=="" && $(this).is(":checked") ){
			let date_ob = new Date();
			let date = ("0" + date_ob.getDate()).slice(-2);
			let month = ("0" + (date_ob.getMonth() + 1)).slice(-2);
			let year = date_ob.getFullYear();
			let hours = date_ob.getHours();
			let minutes = date_ob.getMinutes();
			let seconds = date_ob.getSeconds();
			let fll = year + "-" + month + "-" + date + " " + hours + ":" + minutes + ":" + seconds;
			$("#"+RAD_PREFIX+"_waktu_datang").val(fll);
		}
	});

	
	
	clear_hasil();
	
	
	var column=new Array("id","tanggal","ruangan","kelas",
							"nama_pasien","noreg_pasien","nrm_pasien",
							"no_lab",
                            "id_marketing", "marketing", "id_pengirim", "pengirim",
							"id_dokter","nama_dokter",
							"id_konsultan","nama_konsultan",
							"nama_petugas","id_petugas",
							"jk","carabayar","umur",
							"waktu_daftar","waktu_datang",
							"waktu_selesai","waktu_ditangani",
							"uri","carabayar","froll","frollr","f1824", "f2430", 
							"f3040", "f3535","f3543", "f1824r",
							"f2430r","f3040r","f3535r","f3543r","alamat", "file",
							"sdq4335","dhf3543","dhf2636","dhf2025",
							"dvb3543","dvb3528","dvb2025","fdental",
							"sdq4335r","dhf3543r","dhf2636r","dhf2025r",
							"dvb3543r","dvb3528r","dvb2025r","fdentalr",
							"biaya_konsul", "status", "jenis_kegiatan"
							);
	RAD_ACTION=new RadiologyAction(RAD_PREFIX,rad_the_page,RAD_PREFIX,column);
	RAD_ACTION.setPrototipe(rad_the_protoname,rad_the_protoslug,rad_the_protoimplement);
	RAD_ACTION.view();
});