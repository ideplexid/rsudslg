var rad_dokter;
var rad_konsultan;
var rad_petugas;
var rad_pasien;
var rad_marketing;
var rad_pengirim;

rad_pasien=new TableAction("rad_pasien",rad_the_page,RAD_PREFIX,new Array());
rad_pasien.setSuperCommand("rad_pasien");
rad_pasien.setPrototipe(rad_the_protoname,rad_the_protoslug,rad_the_protoimplement);
rad_pasien.selected=function(json){
	$("#"+RAD_PREFIX+"_nama_pasien").val(json.nama_pasien);
	$("#"+RAD_PREFIX+"_nrm_pasien").val(json.nrm);
	$("#"+RAD_PREFIX+"_noreg_pasien").val(json.id);
	$("#"+RAD_PREFIX+"_jk").val(json.kelamin);
	$("#"+RAD_PREFIX+"_umur").val(json.umur);
	$("#"+RAD_PREFIX+"_ibu").val(json.ibu);
	$("#"+RAD_PREFIX+"_alamat").val(json.alamat_pasien);
    $("#"+RAD_PREFIX+"_carabayar").val(json.carabayar);
    rad_pasien.get_last_ruangan(json.id,json.jenislayanan,json.kamar_inap);
};

rad_pasien.get_last_ruangan = function(noreg,rajal,ranap){
    var cur_ruang = (ranap==null || ranap=="") ? rajal:ranap;
    var data = rad_pasien.getRegulerData();
    data['super_command'] = "";
    data['command'] = "last_position";
    data['noreg_pasien'] = noreg;
    showLoading();
    $.post("",data,function(res){
        var json = getContent(res);
        if(json!=""){
            cur_ruang = json;
        }
        $("#"+RAD_PREFIX+"_ruangan").val(cur_ruang);
        dismissLoading();
    });
};

rad_dokter=new TableAction("rad_dokter",rad_the_page,RAD_PREFIX,new Array());
rad_dokter.setSuperCommand("rad_dokter");
rad_dokter.setPrototipe(rad_the_protoname,rad_the_protoslug,rad_the_protoimplement);
rad_dokter.selected=function(json){
	$("#"+RAD_PREFIX+"_nama_dokter").val(json.nama);
	$("#"+RAD_PREFIX+"_id_dokter").val(json.id);
};

rad_petugas=new TableAction("rad_petugas",rad_the_page,RAD_PREFIX,new Array());
rad_petugas.setSuperCommand("rad_petugas");
rad_petugas.setPrototipe(rad_the_protoname,rad_the_protoslug,rad_the_protoimplement);
rad_petugas.selected=function(json){
	$("#"+RAD_PREFIX+"_nama_petugas").val(json.nama);
	$("#"+RAD_PREFIX+"_id_petugas").val(json.id);
};

rad_konsultan=new TableAction("rad_konsultan",rad_the_page,RAD_PREFIX,new Array());
rad_konsultan.setSuperCommand("rad_konsultan");
rad_konsultan.setPrototipe(rad_the_protoname,rad_the_protoslug,rad_the_protoimplement);
rad_konsultan.selected=function(json){
	$("#"+RAD_PREFIX+"_nama_konsultan").val(json.nama);
	$("#"+RAD_PREFIX+"_id_konsultan").val(json.id);
};

rad_marketing=new TableAction("rad_marketing",rad_the_page,RAD_PREFIX,new Array());
rad_marketing.setSuperCommand("rad_marketing");
rad_marketing.setPrototipe(rad_the_protoname,rad_the_protoslug,rad_the_protoimplement);
rad_marketing.selected=function(json){
	$("#"+EMD_PREFIX+"_marketing").val(json.nama);
	$("#"+EMD_PREFIX+"_id_marketing").val(json.id);
};

rad_pengirim=new TableAction("rad_pengirim",rad_the_page,RAD_PREFIX,new Array());
rad_pengirim.setSuperCommand("rad_pengirim");
rad_pengirim.setPrototipe(rad_the_protoname,rad_the_protoslug,rad_the_protoimplement);
rad_pengirim.selected=function(json){
	$("#"+EMD_PREFIX+"_pengirim").val(json.nama);
	$("#"+EMD_PREFIX+"_id_pengirim").val(json.id);
};