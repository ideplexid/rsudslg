var laporan;
$(document).ready(function(){
$('.mydate').datepicker();
$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
var column=new Array('id','nama','keterangan','slug');
laporan=new TableAction("laporan","radiology","laporan",column);
laporan.getRegulerData=function(){
    var reg_data={	
            page:this.page,
            action:this.action,
            super_command:this.super_command,
            prototype_name:this.prototype_name,
            prototype_slug:this.prototype_slug,
            prototype_implement:this.prototype_implement,
            dari:$("#"+this.prefix+"_dari").val(),
            sampai:$("#"+this.prefix+"_sampai").val()
            };
    return reg_data;
};
laporan.laporan_count=function(){
    $("#rload_modal").modal("show");
    var clear_data=this.getRegulerData();
    clear_data['super_command']="count";
    $.post("",clear_data,function(res){
        var json=getContent(res);
        var num=Number(json);
        $("#rload_modal").modal("show");			
        setTimeout(laporan.laporan_posting(num-1,num),100);	
    });
};

laporan.laporan_posting=function(number,total){
    if(number<0) {
        $("#rload_modal").modal("hide");
        laporan.view();
        return;
    }
    $("#rad_person_bar").sload("true","Loading... [ "+(total-number)+" / "+total+" ]",(total-number)*100/total);
    var clear_data=this.getRegulerData();
    clear_data['super_command']="posting";
    clear_data['limit']=number;
    $.post("",clear_data,function(res){
        var json=getContent(res);
        console.log(json);
        number--;
        setTimeout(laporan.laporan_posting(number,total),100);
    });
};


laporan.view=function(){	
    var self=this;
    var view_data=this.getViewData();
    if(view_data==null){
        showWarning("Error",this.view_data_null_message);
        return;
    }
    showLoading();
    $.post('',view_data,function(res){
        var json=getContent(res);
        if(json==null) {
            
        }else{
            $("#"+self.prefix+"_list").html(json.list);
            $("#"+self.prefix+"_pagination").html(json.pagination);	
        }
        self.afterview(json);
        dismissLoading();

        $(".rad_title_h5").parents().attr("colspan","4");
        $(".rad_title_h5").parentsUntil("tr").siblings().remove();
    });
};

laporan.view();	
});