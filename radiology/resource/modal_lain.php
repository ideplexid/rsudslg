<?php
	class RadiologyModalLain extends HTML {
		public function getHtml() {
			$table = new Table(
				array("No.", "Layanan", "Harga", "Jumlah", "Subtotal"),
				"",
				null,
				true
			);
			$table->setName("layanan_lain");
			$table->setPrintButtonEnable(false);
			$table->setReloadButtonEnable(false);
			$table->setFooterVisible(false);
			
			$content = "<div class='row' style='margin-top: -20px; margin-left: 30px;'>";
				$content .= $table->getHtml();
			$content .= "</div>";
			return $content;
		}
	}
?>