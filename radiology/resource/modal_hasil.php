<?php
class RadiologyModalHasil extends HTML {
	private $jkelamin;
	private $resource;
	
	
	public function __construct(RadiologyResource $resource, $action = "pemeriksaan", $jk = "1") {
		parent::__construct ( "", "", "" );
		$this->resource= $resource;
		$this->jkelamin = $jk;
		$this->action = $action;
	}
	public function setComponent($component) {
		$this->all_component = $component;
	}
	public function summernote($title, $id, $idmodal, $value) {		
		$btn = new Button ( "", "", "Write" );
		$btn->setAction ( "smis_summernote_write('" . $title . "','" . $id . "','" . $idmodal . "')" );
		$btn->setIsButton ( Button::$ICONIC );
		$btn->setIcon ( "icon-black " . Button::$icon_pencil );
		
		$btns = new Button ( "", "", "Show" );
		$btns->setClass ( "btn-inverse" );
		$btns->setAction ( "smis_summernote_show('" . $title . "','" . $id . "','" . $idmodal . "')" );
		$btns->setIsButton ( Button::$ICONIC );
		$btns->setIcon ( "icon-white " . Button::$icon_eye_open );
		
		$inp = new TextArea ( $id, "", $value );
		$inp->setClass ( "hide" );
		$element = new ButtonGroup ( "summernote-buton-group noprint" );
		$element->addButton ( $btn );
		$element->addElement ( $inp );
		$element->addButton ( $btns );
		return $element;
	}
	
	public function getHtml(){
		$all=$this->resource->list_default_pria;
		$set_result = "";
		$ret = "";
		$slug = "radiology_hasil_";
		$form = new Form ( "hasil_radiology", "div_hasil_radiology", "" );
		foreach ( $all as $key => $a_val ) {
			global $db;
			$val=$a_val['name'];
			$nval = $a_val['default'];
			$check = $this->summernote ( ArrayAdapter::format ( "unslug", $key ), $slug . $key, $this->action, $nval );
			$html = $check->getHtml ();
			$form->addElement ( $val, $html, $key );
		}
		$ret .= $form->getHtml ();
		return $ret;
	}
	
}

?>