<?php
global $db;

if(isset($_POST['super_command']) && $_POST['super_command']=="count"){
	$dbtable=new DBTable($db, "smis_rad_lrangkuman_pertanggal");
	$dbtable->truncate();
	$dbtable=new DBTable($db, "smis_rad_pesanan");
	$dari = $_POST ['dari'];
	$sampai = $_POST ['sampai'];
	$dbtable->addCustomKriteria ( "'" . $dari . "'", "<=tanggal" );
	$dbtable->addCustomKriteria ( "'" . $sampai . "'", ">=tanggal" );
	$total=$dbtable->count("");
	$resp=new ResponsePackage();
	$resp->setContent($total);
	$resp->setStatus(ResponsePackage::$STATUS_OK);
	echo json_encode($resp->getPackage());
	return;
}


if(isset($_POST['super_command']) && $_POST['super_command']=="posting"){
	require_once 'radiology/resource/RadiologyResource.php';
	$dbtable=new DBTable($db, "smis_rad_pesanan");
	$dari = $_POST ['dari'];
	$sampai = $_POST ['sampai'];
	$dbtable->addCustomKriteria ( "'" . $dari . "'", "<=tanggal" );
	$dbtable->addCustomKriteria ( "'" . $sampai . "'", ">=tanggal" );
	$dbtable->setMaximum(1);
	$data=$dbtable->view("", $_POST["limit"]);
	$dlist=$data['data'];
	$onelist=$dlist[0];
	$lisperiksa=json_decode($onelist->periksa,true);
	$tanggal=$onelist->tanggal;
	$resr=new RadiologyResource();
	$layanan=$resr->list_reporting;
	$lrangkuman=new DBTable($db, "smis_rad_lrangkuman_pertanggal");
	
	if($lisperiksa!=NULL){
		foreach($lisperiksa as $id=>$yn){
			if($yn=="1"){
				$c=$layanan[$id];
				$nama=$db->escaped_string($c['name']);
				$laporan=$c['laporan'];
				$grup=$c['grup'];
				$query=" INSERT INTO smis_rad_lrangkuman_pertanggal(grup,laporan,tanggal,nama,total) VALUES('".$grup."','".$laporan."','".$tanggal."','".strtoupper($nama)."','1') ON DUPLICATE KEY UPDATE total=total+1 ";
				$db->query($query);
			}
		}
	}
	$resp=new ResponsePackage();
	$resp->setContent("");
	$resp->setStatus(ResponsePackage::$STATUS_OK);
	echo json_encode($resp->getPackage());
	return;
}



$header = array ();
$uitable = new Table ( $header, "", NULL, false );
$uitable->setName ( "laporan_detail_pertanggal" );
$uitable->setFooterVisible ( false );

if (isset ( $_POST ['command'] )) {
	$adapter = new SimpleAdapter();
	$adapter->add("Total", "total");
	$adapter->add("Nama", "nama");
	$adapter->setRemoveZeroEnable(true);
	$dbtable = new DBTable ( $db, "smis_rad_lrangkuman_pertanggal" );
	$dbtable->setOrder(" nama ASC ");
	$dbtable->setShowAll(true);
	
	$dbres = new DBResponder ( $dbtable, $uitable, $adapter );
	if($dbres ->isReload()){
		$dari=$_POST['dari'];
		$sampai=$_POST['sampai'];
        loadLibrary("smis-libs-function-time");
		$date_range=createDateRangeArray($dari,$sampai);
		$month_range=createMonthDateRange($date_range);
		$year_range=createYearMonthRange($month_range);		
		$head_year="<tr><th rowspan='3'>NAMA</th>";
		
		foreach($year_range as $year=>$span){
			$head_year.="<th colspan='".$span."'>".$year."</th>";
		}
		$head_year.="<th rowspan='3'>TOTAL</th></tr>";		
		$head_month="<tr>";
		foreach($month_range as $month=>$span){
			$m=enid_month(substr($month, 5,7)*1);
			$head_month.="<th colspan='".$span."'>".$m."</th>";
		}
		$head_month.="</tr>";		
		$head_date="<tr>";
		foreach($date_range as $date){
			$d=substr($date, 8,10);
			$head_date.="<th>".$d."</th>";
		}
		$head_date.="</tr>";
		$uitable->addHeader("before", $head_year);
		$uitable->addHeader("before", $head_month);
		$uitable->addHeader("before", $head_date);
	}
	
	if($dbres->isView()){
        loadLibrary("smis-libs-function-time");
		$dari=$_POST['dari'];
		$sampai=$_POST['sampai'];
		$qv="SELECT nama ";		
		$date_range=createDateRangeArray($dari,$sampai);
		$header=array();
		$header[]="Nama";
		foreach($date_range as $date){
			$head_name=str_replace("-", "_", $date);
			$header[]=$head_name;
			$qv.=" , SUM(IF(tanggal='".$date."',total,0)) as ".$head_name." ";
			$adapter->add($head_name, $head_name);
		}
		$header[]="Total";
		$qv.=" , SUM(total) as total FROM smis_rad_lrangkuman_pertanggal ";
		$qc=" SELECT COUNT(*) as total FROM smis_rad_lrangkuman_pertanggal ";
		$uitable->setHeader($header);
		$dbtable->setPreferredQuery(true, $qv, $qc);
		$dbtable->setUseWhereforView(true);
		$dbtable->setGroupBy(true, " nama ");
	}
	$data = $dbres->command ( $_POST ['command'] );
	echo json_encode ( $data );
	return;
}

$uitable->addModal ( "dari", "date", "Dari", "" );
$uitable->addModal ( "sampai", "date", "Sampai", "" );

$modal = $uitable->getModal ();
$form = $modal->getForm ();
$form->setTitle("Laporan Detail Per Tanggal Rangkuman");

$btngroup=new ButtonGroup("");
$button = new Button ( "", "", "" );
$button->setClass("btn-primary");
$button->setIsButton ( Button::$ICONIC );
$button->setIcon ( "fa fa-refresh" );
$button->setAction ( "laporan_detail_pertanggal.laporan_detail_pertanggal_count()" );
$btngroup->addElement($button);
$button = new Button ( "", "", "" );
$button->setClass("btn-primary");
$button->setIsButton ( Button::$ICONIC );
$button->setIcon ( "fa fa-print" );
$button->setAction ( "smis_print($('#print_table_laporan_detail_pertanggal').html())" );
$btngroup->addElement($button);
$form->addElement ( "", $btngroup);

$close=new Button("", "", "Batal");
$close	->addClass("btn-primary")
		->setIsButton(Button::$ICONIC_TEXT)
		->setIcon("fa fa-close")
		->setAction("location.reload()");
$person=new LoadingBar("rad_person_bar_pertanggal", "");
$modal=new Modal("rload_modal_pertanggal", "", "Process in Progress");
$modal	->addHTML($person->getHtml(),"after")
		->addFooter($close);

echo $modal->getHtml();
echo $form->getHtml ();
echo $uitable->getHtml ();
echo addCSS ( "framework/bootstrap/css/datepicker.css" );
echo addCSS ( "radiology/resource/css/laporan_detail_pertanggal.css",false );
echo addJS ( "framework/smis/js/table_action.js" );
echo addJS ( "framework/bootstrap/js/bootstrap-datepicker.js" );
echo addJS ( "base-js/smis-base-loading.js");
echo addJS ( "radiology/resource/js/laporan_detail_pertanggal.js",false);
?>
