<?php 
global $db;
global $user;
require_once 'smis-base/smis-include-service-consumer.php';

if(isset($_POST['super_command']) && $_POST['super_command']=="total") {
	$service=new ServiceConsumer($db, "get_total_pv_radiology",NULL,"radiology");
	$service->setMode(ServiceConsumer::$SINGLE_MODE);
	$service->addData("dari", $_POST['dari'])
			->addData("sampai", $_POST['sampai'])
			->addData("id_karyawan", "-1");
	$query="truncate smis_rad_pendapatan";
	$db->query($query);
	
	$content=$service->execute()->getContent();
	$pack=new ResponsePackage();
	$pack->setContent($content);
	$pack->setStatus(ResponsePackage::$STATUS_OK);
	echo json_encode($pack->getPackage());
	return;
}

$header=array("Dokter","Biaya Awal","Bagian Dokter","Bagian RS");
$uitable=new Table($header);
if(isset($_POST['super_command']) && $_POST['super_command']=="limit") {
	$service=new ServiceConsumer($db, "get_pv_radiology");
	$service->setMode(ServiceConsumer::$CLEAN_BOTH)
			->addData("dari", $_POST['dari'])
			->addData("sampai", $_POST['sampai'])
			->addData("limit_start", $_POST['limit_start'])
			->addData("limit_length", 1)
			->addData("id_karyawan", "-1");
	$list=$service->execute()->getContent();
	$dbtable=new DBTable($db, "smis_rad_pendapatan");
	foreach($list as $x){
		$one['id_radiology']=$x['id'];
		$one['id_dokter']=$x['id_karyawan'];
		$one['dokter']=$x['karyawan'];
		$one['waktu']=$x['waktu_asli'];
		$one['tagihan']=$x['asli'];
		$one['bonus']=$x['nilai'];
		$one['persen']=$x['percentage'];
		$one['keterangan']=$x['sebagai']." - ".$x['keterangan']." - ".$x['pasien'];
		$dbtable->insert($one);
	}
	$pack=new ResponsePackage();
	$pack->setContent(count($list));
	$pack->setStatus(ResponsePackage::$STATUS_OK);
	echo json_encode($pack->getPackage());
	return;
}



$uitable->setName("pv_dokter")
		->setActionEnable(false)
		->setFooterVisible(false);

if(isset($_POST['command']) && $_POST['command']== "list") {	
	require_once 'radiology/class/adapter/RecapitulationProvitShareRadiologyAdapter.php';
	loadLibrary("smis-libs-function-math");
	$dbtable=new DBTable($db, "smis_rad_pendapatan");
	$dbtable->setFetchMethode(DBTable::$ARRAY_FETCH);
	$dbtable->addCustomKriteria(NULL, "waktu >= '".$_POST['dari']."'");
	$dbtable->addCustomKriteria(NULL, "waktu < '".$_POST['sampai']."'");
	$dbtable->setShowAll(true);
	$data=$dbtable->view("", "0");
	
	$adapter=new RecapitulationProvitShareRadiologyAdapter($_POST['pajak']);
	$uidata=$adapter->setUseID(false)
					->getContent($data['data']);
	$list=$uitable->setContent($uidata)
				  ->getBodyContent();
	
	$json['list']=$list;
	$json['pagination']="";
	$json['number']="0";
	$json['number_p']="0";
	$json['data']=$uidata;
	$json['waktu']=ArrayAdapter::format("date d M Y H:i:s", date("Y-m-d H:i:s"));
	$json['nomor']="RRAD-".date("dmy-his")."-".substr(md5($user->getNameOnly()), 5,rand(2, 5));
	$ft=ArrayAdapter::format("date d M Y", $_POST['dari']);
	$to=ArrayAdapter::format("date d M Y", $_POST['sampai']);	
	$json['json']=json_encode($uidata);
	$json['status']=1;
	
	$pack=new ResponsePackage();
	$pack->setContent($json);
	$pack->setStatus(ResponsePackage::$STATUS_OK);
	echo json_encode($pack->getPackage());
	return;
}

$pjkserv=new ServiceConsumer($db, "get_pajak");
$pjkserv->execute();
$pajak=$pjkserv->getContent();

$uitable->addModal("dari", "date", "Dari", "")
		->addModal("sampai", "date", "Sampai", "")
		->addModal("pajak", "text", "Pajak (%) ", $pajak);
$action=new Button("","","View");
$action->setClass("btn-primary")
	   ->setIsButton(Button::$ICONIC)
	   ->setIcon("fa fa-circle-o-notch")
	   ->setAction("pv_dokter.rekaptotal()");
$print=new Button("","","View");
$print->setClass("btn-primary")
		->setIsButton(Button::$ICONIC)
		->setIcon("fa fa-print")
		->setAction("pv_dokter.print()");


$btn_froup=new ButtonGroup("");
$btn_froup->addButton($action);
$btn_froup->addButton($print);
$form=$uitable
	  ->getModal()
	  ->setTitle("Radiology")
	  ->getForm()
	  ->addElement("",$btn_froup);

$close=new Button("", "", "Batal");
$close	->addClass("btn-primary")
		->setIsButton(Button::$ICONIC_TEXT)
		->setIcon("fa fa-close")
		->setAction("pv_dokter.batal()");

$load=new LoadingBar("rekap_rad_bar", "");
$modal=new Modal("rekap_rad_modal", "", "Processing...");
$modal	->addHTML($load->getHtml(),"after")
		->addFooter($close);

echo $form->getHtml();
echo "<div class='clear'></div>";
echo "<div id='result_pv_dokter'>".$uitable->getHtml()."</div>";
echo $modal->getHtml();
echo addJS("framework/bootstrap/js/bootstrap-datepicker.js");
echo addJS("framework/smis/js/table_action.js");
echo addCSS("framework/bootstrap/css/datepicker.css");
echo addJS ( "base-js/smis-base-loading.js");
?>

<script type="text/javascript">
	var pv_dokter;
	var pv_dokter_karyawan;
	var pv_dokter_data;
	var IS_pv_dokter_RUNNING;
	$(document).ready(function(){
		$('.mydate').datepicker();
		pv_dokter=new TableAction("pv_dokter","radiology","pv_dokter",new Array());
		pv_dokter.addRegulerData=function(data){
			data['dari']=$("#pv_dokter_dari").val();
			data['sampai']=$("#pv_dokter_sampai").val();
			data['pajak']=$("#pv_dokter_pajak").val();
			$("#dari_table_pv_dokter").html(getFormattedDate(data['dari']));
			$("#sampai_table_pv_dokter").html(getFormattedDate(data['sampai']));
			$("#pajak_table_pv_dokter").html(data['pajak']+"%");			
			return data;
		};

		pv_dokter.batal=function(){
			IS_pv_dokter_RUNNING=false;
			$("#rekap_rad_modal").modal("hide");
		};
		
		pv_dokter.afterview=function(json){
			if(json!=null){
				$("#kode_table_pv_dokter").html(json.nomor);
				$("#waktu_table_pv_dokter").html(json.waktu);
				pv_dokter_data=json;
			}
		};

		pv_dokter.rekaptotal=function(){
			if(IS_pv_dokter_RUNNING) return;
			$("#rekap_rad_bar").sload("true","Fetching total data",0);
			$("#rekap_rad_modal").modal("show");
			IS_pv_dokter_RUNNING=true;
			var d=this.getRegulerData();
			d['super_command']="total";
			$.post("",d,function(res){
				var total=Number(getContent(res));
				if(total>0) {
					pv_dokter.rekaploop(0,total);
				} else {
					$("#rekap_rad_modal").modal("hide");
					IS_pv_dokter_RUNNING=false;
				}
			});
		};

		pv_dokter.rekaploop=function(current,total){
			$("#rekap_rad_bar").sload("true","Calculation... [ "+current+" / "+total+" ] ",(current*100/total));
			if(current>=total || !IS_pv_dokter_RUNNING) {
				$("#rekap_rad_modal").modal("hide");
				pv_dokter.view();
				IS_pv_dokter_RUNNING=false;
				return;
			}
			var d=this.getRegulerData();
			d['super_command']="limit";
			d['limit_start']=current;			
			$.post("",d,function(res){
				setTimeout(function(){pv_dokter.rekaploop(++current,total)},300);
			});
		};
				
	});
</script>