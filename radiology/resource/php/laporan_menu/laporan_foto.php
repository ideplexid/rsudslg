<?php
global $db;
require_once 'smis-libs-class/MasterSlaveTemplate.php';
$presensi=new MasterSlaveTemplate($db, "smis_rad_pesanan", "radiology", "laporan_foto");
$presensi->setDateEnable(true);
$header=array ("No.","Tanggal",'Nama',"F Roll",'F 18x24','F 24x30',"F 30x40","F 35x35","F 35x43",'D-SDQ 43x35','DHF 35x43',"DHF 26x36","DHF 20x25","DVB 35x43","DVB 35x28","DVB 20x25");
$column=array("tanggal","nama_pasien","froll","f1824","f2430","f3040","f3535","f3543","sdq4335","dhf3543","dhf2636","dhf2025","dvb3543","dvb3528","dvb2025");
$dbtable=$presensi->getDBtable()->setView($column);
if(isset($_POST['dari']) && isset($_POST['sampai'])){
	$dbtable->addCustomKriteria(NULL, " tanggal>='".$_POST['dari']."'");
	$dbtable->addCustomKriteria(NULL, " tanggal<'".$_POST['sampai']."'");
	$dbtable->setShowAll(true);
}
$btn=$presensi->addFlag("dari", "Pilih Tanggal Mulainya", "Silakan Pilih Tanggal Mulainya Dahulu")
			  ->addFlag("sampai", "Pilih Tanggal Sampainya", "Silakan Pilih Tanggal Sampainya Dahulu")
			  ->addNoClear("dari")
			  ->addNoClear("sampai")
			  ->getUItable()
			  ->setAddButtonEnable(false)
			  ->getHeaderButton();
$presensi->getUItable()
		 ->setAction(false)
		 ->addModal("dari", "date", "Dari", "")
		 ->addModal("sampai", "date", "Sampai", "");
$presensi->getForm(true,"Laporan Penggunaan Foto")
		 ->addElement("", $btn);
$presensi->getUItable()
		 ->setHeader($header)
		 ->setFooterVisible(false);
$summary=new SummaryAdapter();
$presensi->setAdapter($summary)
		 ->getAdapter()
		 ->setUseNumber(true, "No.")
		 ->add("Tanggal", "tanggal","date d M Y")
		 ->add("Nama", "nama_pasien")
		 ->add("F Roll", "froll")
		 ->add("F 18x24", "f1824")
		 ->add("F 24x30", "f2430")
		 ->add("F 30x40", "f3040")
		 ->add("F 35x35", "f3535")
		 ->add("F 35x43", "f3543")
		 ->add("D-SDQ 43x35", "sdq4335")
		 ->add("DHF 35x43", "dhf3543")
		 ->add("DHF 26x36", "dhf2636")
		 ->add("DHF 20x25", "dhf2025")
		 ->add("DVB 35x43", "dvb3543")
		 ->add("DVB 35x28", "dvb3528")
		 ->add("DVB 20x25", "dvb2025")
		 ->addFixValue("Tanggal","-")
		 ->addFixValue("Nama","Total")
		 ->addSummary("F Roll", "froll")
		 ->addSummary("F 18x24", "f1824")
		 ->addSummary("F 24x30", "f2430")
		 ->addSummary("F 30x40", "f3040")
		 ->addSummary("F 35x35", "f3535")
		 ->addSummary("F 35x43", "f3543")
		 ->addSummary("D-SDQ 43x35", "sdq4335")
		 ->addSummary("DHF 35x43", "dhf3543")
		 ->addSummary("DHF 26x36", "dhf2636")
		 ->addSummary("DHF 20x25", "dhf2025")
		 ->addSummary("DVB 35x43", "dvb3543")
		 ->addSummary("DVB 35x28", "dvb3528")
		 ->addSummary("DVB 20x25", "dvb2025");

$presensi->addViewData("dari", "dari")
		 ->addViewData("sampai", "sampai");
$presensi->initialize();
?>


