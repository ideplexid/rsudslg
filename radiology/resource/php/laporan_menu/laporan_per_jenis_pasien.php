<?php
global $db;
require_once 'smis-libs-class/MasterSlaveTemplate.php';
$presensi=new MasterSlaveTemplate($db, "smis_rad_pesanan", "radiology", "laporan_per_jenis_pasien");
$presensi->setDateEnable(true);

global $db;
require_once 'smis-base/smis-include-service-consumer.php';
$urjip=new ServiceConsumer($db, "get_jenis_patient",array(),"registration");
$urjip->execute();
$content=$urjip->getContent();
$jenis_pasien=array();
foreach($content as $one){
	$option=array();
	$option['value']=$one['value'];
	$option['name']=$one['name'];
	$jenis_pasien[]=$option;
}
$inap=array();
$inap['rawat_inap']="1";
$inap['rawat_jalan']="0";
	
$dbtable=$presensi->getDBtable()->setView($column);
if(isset($_POST['dari']) && isset($_POST['sampai'])){
	$qv=" SELECT tanggal ";
	foreach($inap as $urji=>$inap_value){
		foreach($jenis_pasien as $one){
			$qv.=" , SUM( IF (uri='".$inap_value."' AND carabayar='".$one['value']."',1,0) ) as ".$urji."_".$one['value']."  ";
		}
		$qv.=" , SUM(IF(uri='".$inap_value."',1,0)) as ".$urji."_total";
	}
	$qv.=" , SUM(1) as total";
	$qv.=" FROM smis_rad_pesanan ";
	$qc="SELECT COUNT(*) FROM smis_rad_pesanan ";
	
	$dbtable->addCustomKriteria(NULL, " tanggal>='".$_POST['dari']."'");
	$dbtable->addCustomKriteria(NULL, " tanggal<'".$_POST['sampai']."'");
	$dbtable->setPreferredQuery(true, $qv, $qc);
	$dbtable->setUseWhereforView(true);
	$dbtable->setGroupBy(true, " tanggal ");
	$dbtable->setOrder(" tanggal ASC ");
	$dbtable->setShowAll(true);
}
$btn=$presensi->addFlag("dari", "Pilih Tanggal Mulainya", "Silakan Pilih Tanggal Mulainya Dahulu")
			  ->addFlag("sampai", "Pilih Tanggal Sampainya", "Silakan Pilih Tanggal Sampainya Dahulu")
			  ->addNoClear("dari")
			  ->addNoClear("sampai")
			  ->getUItable()
			  ->setAddButtonEnable(false)
			  ->getHeaderButton();
$presensi->getUItable()
		 ->setAction(false)
		 ->addModal("dari", "date", "Dari", "")
		 ->addModal("sampai", "date", "Sampai", "");
$presensi->getForm(true,"Laporan Per Tanggal")
		 ->addElement("", $btn);
$summary=new SummaryAdapter();
$presensi->setAdapter($summary)
		 ->getAdapter()
		 ->setUseNumber(true, "No.")
		 ->add("Tanggal", "tanggal","date d M Y")
		 ->addFixValue("Tanggal","TOTAL");

$header=array ("No.","Tanggal");

$header_top="";
$header_top_colspan=count($jenis_pasien)+1;
$header_second_top="";
foreach($inap as $urji=>$inap_value){
	foreach($jenis_pasien as $one){
		$header[]=$urji."_".$one['value'];
		$summary->add($urji."_".$one['value'], $urji."_".$one['value']);
		$summary->addSummary($urji."_".$one['value'], $urji."_".$one['value']);
		$header_second_top.=" <th>".$one['name']."</th> ";
	}
	$header[]=$urji."_total";
	$summary->add($urji."_total", $urji."_total");
	$summary->addSummary($urji."_total", $urji."_total");
	$header_second_top.=" <th>Sub Total"."</th> ";
	$header_top.=" <th colspan=".$header_top_colspan." >".ArrayAdapter::format("unslug", $urji)."</th> ";
}

$header[]="Total";
$summary->add("Total", "total");
$summary->addSummary("Total", "total");
$summary->setRemoveZeroEnable(true);
$header_top.=" <th rowspan='2'>Total</th>";-
$header_top="<tr> <th rowspan='2'>No.</th> <th rowspan='2'>Tanggal</th> ".$header_top."</tr>";
$header_top_second="<tr>".$header_top."</tr>";


$presensi->getUItable()
		 ->setHeader($header)
		 ->setHeaderVisible(false)
		 ->addHeader("before", $header_top)
		 ->addHeader("before", $header_second_top)
		 ->setFooterVisible(false);
$summary=new SummaryAdapter();


		 
$presensi->addViewData("dari", "dari")
		 ->addViewData("sampai", "sampai");
$presensi->initialize();
?>


