<?php
$header = array ();
$header [] = "Tanggal";
$header [] = "Nama";
$header [] = "NRM";
$header [] = "No Reg";
$header [] = "Nomor";
$header [] = "Kelas";
$header [] = "Jenis";
$header [] = "Ruangan";
$header [] = "Total Tagihan";
$header [] = "Layanan";
$header [] = "Waktu Datang";
$header [] = "Waktu Ditangani";
$header [] = "Waktu Cetak";
$header [] = "Response Time";

$uitable = new Table ( $header, "", NULL, false );
$uitable->setName ( "laporan_detail" );

if (isset ( $_POST ['command'] )) {
	require_once "radiology/resource/RadiologyResource.php";
    require_once "radiology/class/adapter/LaporanDetailAdapter.php";
    $resource=new RadiologyResource();
    $adapter = new LaporanDetailAdapter ();
    $adapter->setMap($resource->name_map);
	
	$dbtable = new DBTable ( $db, "smis_rad_pesanan" );
	if (isset ( $_POST ['dari'] ) && isset ( $_POST ['sampai'] ) && $_POST ['dari'] != "" && $_POST ['sampai'] != "") {
		$dari = $_POST ['dari'];
		$sampai = $_POST ['sampai'];
		$layanan = $_POST ['layanan'];
		$carabayar = $_POST ['carabayar'];
		
		$dbtable->addCustomKriteria ( "'" . $dari . "'", "<=tanggal" );
		$dbtable->addCustomKriteria ( "'" . $sampai . "'", ">=tanggal" );
		if ($layanan != "")
			$dbtable->addCustomKriteria ( " periksa ", " LIKE '%\"" . $layanan . "\":\"1\"%' " );
		
		if ($carabayar != "")
			$dbtable->addCustomKriteria ( " carabayar ", " ='".$carabayar."'  " );
	}
	
	$dbtable->setOrder ( " tanggal ASC " );
	$dbres = new DBResponder ( $dbtable, $uitable, $adapter );
	if($dbres->isExcel()){
        require_once "radiology/class/adapter/LaporanDetailExcelAdapter.php";
		$newadapter=new LaporanDetailExcelAdapter();
        $newadapter->setMap($resource->name_map);
		$newadapter->add ( "Tanggal", "tanggal", "date d/m/Y" );
		$newadapter->add ( "Nama", "nama_pasien" );
		$newadapter->add ( "NRM", "nrm_pasien", "only-digit6" );
		$newadapter->add ( "No Reg", "noreg_pasien", "only-digit6" );
		$newadapter->add ( "Nomor", "no_lab" );
		$newadapter->add ( "Kelas", "kelas", "unslug" );
		$newadapter->add ( "Jenis", "carabayar", "unslug" );
		$newadapter->add ( "Ruangan", "ruangan", "unslug" );
        $newadapter->add ( "Total Tagihan", "biaya" );
        $newadapter->add ( "Waktu Datang", "waktu_datang","date d/m/Y H:i" );
        $newadapter->add ( "Waktu Ditangani", "waktu_ditangani","date d/m/Y H:i" );
        $newadapter->add ( "Waktu Cetak", "waktu_selesai","date d/m/Y H:i" );
        $newadapter->add ( "Response Time", "respontime","back Menit" );
		$dbres->setAdapter($newadapter);
	}
	
	$data = $dbres->command ( $_POST ['command'] );
	if($data!=null)
		echo json_encode ( $data );
	return;
}

require_once 'radiology/resource/RadiologyResource.php';
$resource = new RadiologyResource ();
$layanan = new OptionBuilder ();
$layanan->add ( "", "" );
foreach ( $resource->list_layanan as $key => $content ) {
	foreach ( $content as $key => $value ) {
		$layanan->add ( $value, $key );
	}
}

require_once "smis-base/smis-include-service-consumer.php";
$ser=new ServiceConsumer($db,"get_jenis_patient",NULL,"registration");
$ser->execute();
$ctx=$ser->getContent();
$ctx[]=array("name"=>"-- Kosong --","value"=>"");


$uitable->addModal ( "dari", "date", "Dari", "" );
$uitable->addModal ( "sampai", "date", "Sampai", "" );
$uitable->addModal ( "carabayar", "select", "Jenis Pasien", $ctx );
$uitable->addModal ( "layanan", "select", "Layanan", $layanan->getContent () );

$modal = $uitable->getModal ();
$form = $modal->getForm ();
$form->setTitle("Laporan Per Layanan");

$btngroup=new ButtonGroup("");
$button = new Button ( "", "", "Reload" );
$button->setClass("btn-primary");
$button->setIsButton ( Button::$ICONIC_TEXT );
$button->setIcon ( "fa fa-refresh" );
$button->setAction ( "laporan_detail.view()" );
$btngroup->addElement($button);

$button = new Button ( "", "", "Print" );
$button->setClass("btn-primary");
$button->setIsButton ( Button::$ICONIC_TEXT );
$button->setIcon ( "fa fa-print" );
$button->setAction ( "smis_print($('#print_table_laporan_detail').html())" );
$btngroup->addElement($button);

$excel = new Button ( "", "", "Download" );
$excel->setClass("btn-primary");
$excel->setIsButton ( Button::$ICONIC_TEXT );
$excel->setIcon ( "fa fa-download" );
$excel->setAction ( "laporan_detail.excel()" );
$btngroup->addElement($excel);

$form->addElement ( "", $btngroup);
echo $form->getHtml ();


echo $uitable->getHtml ();
echo addJS ( "framework/smis/js/table_action.js" );
echo addJS ( "framework/bootstrap/js/bootstrap-datepicker.js" );
echo addCSS ( "framework/bootstrap/css/datepicker.css" );
echo addJS ( "radiology/resource/js/laporan_detail.js",false );
?>
