<?php
global $db;
require_once 'radiology/resource/RadiologyResource.php';

if(isset($_POST['super_command']) && $_POST['super_command']=="posting") {
    $dbtable_insert = new DBTable($db, "smis_rad_perlayanan_pasien");
	$dbtable_insert->truncate();
    
    $dbtable=new DBTable($db, "smis_rad_pesanan");
	$dari = $_POST ['dari'];
	$sampai = $_POST ['sampai'];
    $dbtable->addCustomKriteria ( "'" . $dari . "'", "<=tanggal" );
	$dbtable->addCustomKriteria ( "'" . $sampai . "'", ">=tanggal" );
    $data = $dbtable->view("", 0);
    $dlist = $data['data'];
    
    $rad_resource = new RadiologyResource();
    foreach ( $rad_resource->list_layanan as $key => $content ) {
        foreach ( $content as $key => $value ) {
             $list_layanan[$key] = $value;
        }
    }
    
    foreach($dlist as $d) {
        $tanggal = $d->tanggal;
        $nama_pasien = $d->nama_pasien;
        $nrm_pasien = $d->nrm_pasien;
        $noreg_pasien = $d->noreg_pasien;
        $no_rad = $d->no_lab;
        $kelas = $d->kelas;
        $jenis = $d->carabayar;
        $ruangan = $d->ruangan;
        $total_tagihan = $d->biaya;
        $nama_konsultan = $d->nama_konsultan;
        $waktu_datang = $d->waktu_datang;
        $waktu_ditangani = $d->waktu_ditangani;
        $waktu_selesai = $d->waktu_selesai;
        $respontime = $d->respontime;

        $list_jaspel = $rad_resource->getJaspel($kelas);
        $periksa = json_decode($d->periksa);
        foreach($periksa as $name => $value) {
            if($value != 0) {
                $layanan = $value;
                $jaspel = $list_jaspel[$name];
                $insert['tanggal']          = $tanggal;
                $insert['nama_pasien']      = $nama_pasien;
                $insert['nrm_pasien']       = $nrm_pasien;
                $insert['noreg_pasien']     = $noreg_pasien;
                $insert['no_rad']           = $no_rad;
                $insert['kelas']            = $kelas;
                $insert['jenis']            = $jenis;
                $insert['ruangan']          = $ruangan;
                $insert['total_tagihan']    = $total_tagihan;
                $insert['nama_konsultan']   = $nama_konsultan;
                $insert['layanan']          = $list_layanan[$name];
                $insert['harga_jaspel']     = $jaspel;
                $insert['waktu_datang']     = $waktu_datang;
                $insert['waktu_ditangani']  = $waktu_ditangani;
                $insert['waktu_selesai']    = $waktu_selesai;
                $insert['respontime']       = $respontime;
                $dbtable_insert->insert($insert);
            }
        }
    }
    $resp=new ResponsePackage();
	$resp->setContent("");
	$resp->setStatus(ResponsePackage::$STATUS_OK);
	echo json_encode($resp->getPackage());
	return;
}

$header = array ();
$header [] = "No.";
$header [] = "Tanggal";
$header [] = "Nama";
$header [] = "NRM";
$header [] = "No.Reg";
$header [] = "Nomor";
$header [] = "Kelas";
$header [] = "Jenis";
$header [] = "Ruangan";
$header [] = "Total Tagihan";
$header [] = "Layanan";
$header [] = "Jasa Pelayanan";
$header [] = "Konsultan";

$header [] = "Waktu Datang";
$header [] = "Waktu Ditangani";
$header [] = "Waktu Cetak";
$header [] = "Response Time";

$uitable = new Table ( $header, "", NULL, false );
$uitable->setName ( "laporan_perlayanan_pasien" );

if (isset ( $_POST ['command'] )) {
    $adapter = new SimpleAdapter();
    $adapter->setUseNumber(true,"No.","back.");
    $adapter->add("Tanggal", "tanggal", "date d M Y")
            ->add("Nama", "nama_pasien")
            ->add("NRM", "nrm_pasien", "digit8")
            ->add("No.Reg", "noreg_pasien", "digit8")
            ->add("Nomor", "no_rad")
            ->add("Kelas", "kelas", "unslug")
            ->add("Jenis", "jenis", "unslug")
            ->add("Ruangan", "ruangan", "unslug")
            ->add("Total Tagihan", "total_tagihan", "money Rp.")
            ->add("Layanan", "layanan")
            ->add("Jasa Pelayanan", "harga_jaspel", "money Rp.")
            ->add("Konsultan", "nama_konsultan")
            ->add ( "Waktu Datang", "waktu_datang","date d M Y H:i:s" )
            ->add ( "Waktu Ditangani", "waktu_ditangani","date d M Y H:i:s" )
            ->add ( "Waktu Cetak", "waktu_selesai","date d M Y H:i:s" )
            ->add ( "Response Time", "respontime","back Menit" );
    
    $dbtable = new DBTable ( $db, "smis_rad_perlayanan_pasien" );
    if(isset($_POST['layanan']) && $_POST['layanan'] != "") {
        $dbtable->addCustomKriteria("layanan", "= '".$_POST['layanan']."' ");
    }
	$dbtable->setOrder("tanggal DESC ");
    
    $dbres = new DBResponder ( $dbtable, $uitable, $adapter );
	$data = $dbres->command ( $_POST ['command'] );
	echo json_encode ( $data );
	return;
}

$uitable->addModal ( "dari", "date", "Dari", "" );
$uitable->addModal ( "sampai", "date", "Sampai", "" );

$resource = new RadiologyResource ();
$layanan = new OptionBuilder ();
$layanan->add ( "", "" );
foreach ( $resource->list_layanan as $key => $content ) {
	foreach ( $content as $key => $value ) {
		$layanan->add ( $value, $value );
	}
}
$uitable->addModal ( "layanan", "select", "Layanan", $layanan->getContent () );

$modal = $uitable->getModal ();
$form = $modal->getForm ();
$form->setTitle("Laporan Per Layanan Pasien");

$btngroup=new ButtonGroup("");
$button = new Button ( "", "", "" );
$button->setClass("btn-primary");
$button->setIsButton ( Button::$ICONIC );
$button->setIcon ( "fa fa-refresh" );
$button->setAction ( "laporan_perlayanan_pasien.posting()" );
$btngroup->addElement($button);
$button = new Button ( "", "", "" );
$button->setClass("btn-primary");
$button->setIsButton ( Button::$ICONIC );
$button->setIcon ( "fa fa-print" );
$button->setAction ( "smis_print($('#print_table_laporan_perlayanan_pasien').html())" );
$btngroup->addElement($button);
$form->addElement ( "", $btngroup);

echo $form->getHtml ();
echo $uitable->getHtml ();

echo addJS ( "framework/smis/js/table_action.js" );
echo addJS ( "framework/bootstrap/js/bootstrap-datepicker.js" );
echo addCSS ( "framework/bootstrap/css/datepicker.css" );
//echo addJS ( "base-js/smis-base-loading.js");
echo addJS ( "radiology/resource/js/laporan_perlayanan_pasien.js", false);

?>