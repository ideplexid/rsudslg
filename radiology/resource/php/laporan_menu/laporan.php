<?php
global $db;

if(isset($_POST['super_command']) && $_POST['super_command']=="count"){
	$dbtable=new DBTable($db, "smis_rad_lrangkuman");
	$dbtable->truncate();
	
	$query=" INSERT INTO smis_rad_lrangkuman VALUES(NULL,'<strong class=\"rad_title_h5\"><h4>RESUME</h4></strong>','0','0','0','00','') ";
	$db->query($query);
	$query=" INSERT INTO smis_rad_lrangkuman VALUES(NULL,'<strong><h4 class=\"rad_title_h5\">PER PELAYANAN</h4></strong>','0','0','0','11','') ";
	$db->query($query);
	$query=" INSERT INTO smis_rad_lrangkuman VALUES(NULL,'<strong><h4 class=\"rad_title_h5\">PER BAGIAN</h4></strong>','0','0','0','21','') ";
	$db->query($query);
	$query=" INSERT INTO smis_rad_lrangkuman VALUES(NULL,'<strong><h4 class=\"rad_title_h5\">PER JENIS LAYANAN</h4></strong>','0','0','0','31','') ";
	$db->query($query);
	
	$dbtable=new DBTable($db, "smis_rad_pesanan");
	$dari = $_POST ['dari'];
	$sampai = $_POST ['sampai'];
	$dbtable->addCustomKriteria ( "'" . $dari . "'", "<=tanggal" );
	$dbtable->addCustomKriteria ( "'" . $sampai . "'", ">=tanggal" );
	$total=$dbtable->count("");
	$resp=new ResponsePackage();
	$resp->setContent($total);
	$resp->setStatus(ResponsePackage::$STATUS_OK);
	echo json_encode($resp->getPackage());
	return;
}


if(isset($_POST['super_command']) && $_POST['super_command']=="posting"){
	require_once 'radiology/resource/RadiologyResource.php';
	$dbtable=new DBTable($db, "smis_rad_pesanan");
	$dari = $_POST ['dari'];
	$sampai = $_POST ['sampai'];
	$dbtable->addCustomKriteria ( "'" . $dari . "'", "<=tanggal" );
	$dbtable->addCustomKriteria ( "'" . $sampai . "'", ">=tanggal" );
	$dbtable->setMaximum(1);
	$data=$dbtable->view("", $_POST["limit"]);
	$dlist=$data['data'];
	$onelist=$dlist[0];
	$lisperiksa=json_decode($onelist->periksa,true);
	$resr=new RadiologyResource();
	$layanan=$resr->list_reporting;
	$lrangkuman=new DBTable($db, "smis_rad_lrangkuman");
	
	$lk=$onelist->jk=="0"?1:0;
	$pr=$onelist->jk=="1"?1:0;
	
	$query=" INSERT INTO smis_rad_lrangkuman (id,nama,pria,wanita,total,jenis,prop) VALUES(NULL,'Jumlah Pasien Daftar','".$lk."','".$pr."','1','01','') ON DUPLICATE KEY UPDATE pria=pria+'".$lk."', wanita=wanita+'".$pr."' , total=total+1 ";
	$db->query($query);
	if($onelist->selesai=="0"){
		$query=" INSERT INTO smis_rad_lrangkuman (id,nama,pria,wanita,total,jenis,prop) VALUES(NULL,'Jumlah Pasien Proses','".$lk."','".$pr."','1','02','') ON DUPLICATE KEY UPDATE pria=pria+'".$lk."', wanita=wanita+'".$pr."' , total=total+1 ";
		$db->query($query);
	}else if($onelist->selesai=="1"){
		$query=" INSERT INTO smis_rad_lrangkuman (id,nama,pria,wanita,total,jenis,prop) VALUES(NULL,'Jumlah Pasien Selesai','".$lk."','".$pr."','1','03','') ON DUPLICATE KEY UPDATE pria=pria+'".$lk."', wanita=wanita+'".$pr."' , total=total+1 ";
		$db->query($query);
		$query=" INSERT INTO smis_rad_lrangkuman (id,nama,pria,wanita,total,jenis,prop) VALUES(NULL,'Jumlah Terbaca','".$lk."','".$pr."','1','04','') ON DUPLICATE KEY UPDATE pria=pria+'".$lk."', wanita=wanita+'".$pr."' , total=total+1 ";
		$db->query($query);
	}else if($onelist->selesai=="-1"){
		$query=" INSERT INTO smis_rad_lrangkuman (id,nama,pria,wanita,total,jenis,prop) VALUES(NULL,'Jumlah Pasien Selesai','".$lk."','".$pr."','1','03','') ON DUPLICATE KEY UPDATE pria=pria+'".$lk."', wanita=wanita+'".$pr."' , total=total+1 ";
		$db->query($query);
		$query=" INSERT INTO smis_rad_lrangkuman (id,nama,pria,wanita,total,jenis,prop) VALUES(NULL,'Jumlah Tidak Terbaca','".$lk."','".$pr."','1','05','') ON DUPLICATE KEY UPDATE pria=pria+'".$lk."', wanita=wanita+'".$pr."' , total=total+1 ";
		$db->query($query);
	}else if($onelist->selesai=="-2"){
		$query=" INSERT INTO smis_rad_lrangkuman (id,nama,pria,wanita,total,jenis,prop) VALUES(NULL,'Jumlah Pasien Batal','".$lk."','".$pr."','1','06','') ON DUPLICATE KEY UPDATE pria=pria+'".$lk."', wanita=wanita+'".$pr."' , total=total+1 ";
		$db->query($query);
	}
	
	$ldone=array();
	$gdone=array();
	foreach($lisperiksa as $id=>$yn){
		if($yn=="1"){
			$c=$layanan[$id];
			$nama=$db->escaped_string($c['name']);
			$laporan=$c['laporan'];
			$grup=$c['grup'];
			$query=" INSERT INTO smis_rad_lrangkuman (id,nama,pria,wanita,total,jenis,prop) VALUES(NULL,'".strtoupper($nama)."','".$lk."','".$pr."','1','12','') ON DUPLICATE KEY UPDATE pria=pria+'".$lk."', wanita=wanita+'".$pr."' , total=total+1 ";
			$db->query($query);
			if(!in_array($laporan, $ldone)){
				$query=" INSERT INTO smis_rad_lrangkuman (id,nama,pria,wanita,total,jenis,prop) VALUES(NULL,'".strtoupper($laporan)."','".$lk."','".$pr."','1','22','') ON  DUPLICATE KEY UPDATE pria=pria+'".$lk."', wanita=wanita+'".$pr."' , total=total+1 ";
				$db->query($query);
				$ldone[]=$laporan;
			}
			if(!in_array($grup, $gdone)){
				$query=" INSERT INTO smis_rad_lrangkuman (id,nama,pria,wanita,total,jenis,prop) VALUES(NULL,'".strtoupper($grup)."','".$lk."','".$pr."','1','32','') ON  DUPLICATE KEY UPDATE pria=pria+'".$lk."', wanita=wanita+'".$pr."' , total=total+1 ";
				$db->query($query);
				$gdone[]=$grup;
			}
		}
	}
	$resp=new ResponsePackage();
	$resp->setContent("");
	$resp->setStatus(ResponsePackage::$STATUS_OK);
	echo json_encode($resp->getPackage());
	return;
}



$header = array ("Nama","Pria","Wanita","Total");
$uitable = new Table ( $header, "", NULL, false );
$uitable->setName ( "laporan" );
$uitable->setFooterVisible ( false );

if (isset ( $_POST ['command'] )) {
	$adapter = new SimpleAdapter();
	$adapter->add("Nama", "nama");
	$adapter->add("Pria", "pria");
	$adapter->add("Wanita", "wanita");
	$adapter->add("Total", "total");
	$adapter->add("Jenis", "jenis");
	$dbtable = new DBTable ( $db, "smis_rad_lrangkuman" );
	$dbtable->setOrder("jenis ASC, nama ASC ");
	$dbtable->setShowAll(true);
	$dbres = new DBResponder ( $dbtable, $uitable, $adapter );
	$data = $dbres->command ( $_POST ['command'] );
	echo json_encode ( $data );
	return;
}

$uitable->addModal ( "dari", "date", "Dari", "" );
$uitable->addModal ( "sampai", "date", "Sampai", "" );
$uitable->addModal ( "ruangan", "hidden", "", "igd" );

$modal = $uitable->getModal ();
$form = $modal->getForm ();
$form->setTitle("Laporan Rangkuman");

$btngroup=new ButtonGroup("");
$button = new Button ( "", "", "" );
$button->setClass("btn-primary");
$button->setIsButton ( Button::$ICONIC );
$button->setIcon ( "fa fa-refresh" );
$button->setAction ( "laporan.laporan_count()" );
$btngroup->addElement($button);
$button = new Button ( "", "", "" );
$button->setClass("btn-primary");
$button->setIsButton ( Button::$ICONIC );
$button->setIcon ( "fa fa-print" );
$button->setAction ( "smis_print($('#print_table_laporan').html())" );
$btngroup->addElement($button);
$form->addElement ( "", $btngroup);

$close=new Button("", "", "Batal");
$close	->addClass("btn-primary")
		->setIsButton(Button::$ICONIC_TEXT)
		->setIcon("fa fa-close")
		->setAction("location.reload()");
$person=new LoadingBar("rad_person_bar", "");
$modal=new Modal("rload_modal", "", "Process in Progress");
$modal	->addHTML($person->getHtml(),"after")
		->addFooter($close);

echo $modal->getHtml();
echo $form->getHtml ();
echo $uitable->getHtml ();
echo addJS ( "framework/smis/js/table_action.js" );
echo addJS ( "framework/bootstrap/js/bootstrap-datepicker.js" );
echo addCSS ( "framework/bootstrap/css/datepicker.css" );
echo addJS ( "base-js/smis-base-loading.js");
echo addJS ( "radiology/resource/js/laporan.js",false);
echo addCSS ( "radiology/resource/css/laporan.css",false);

?>