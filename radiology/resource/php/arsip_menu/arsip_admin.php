<?php 
require_once 'smis-libs-class/MasterTemplate.php';
global $db;
$arsip_admin=new MasterTemplate($db, "smis_rad_pesanan", "radiology", "arsip_admin");
$uitable=$arsip_admin->getUItable();
$uitable->setReloadButtonEnable(false);
$uitable->setPrintButtonEnable(false);
$uitable->setEditButtonEnable(false);
$uitable->setAddButtonEnable(false);
$uitable->setDelButtonEnable(false);
$arsip_admin->getDBtable()->addCustomKriteria("selesai", "='1'");

$btn=new Button("", "", "");
$btn->setClass("btn btn-primary");
$btn->setIcon("fa fa-forward");
$btn->setIsButton(Button::$ICONIC);
$uitable->addContentButton("restore_arsip", $btn);
$header=array("No.","Tanggal","No. Rad","Pasien","NRM","No. Reg","Nilai","Layanan");
$uitable->setHeader($header);

require_once "radiology/resource/RadiologyResource.php";
require_once "radiology/class/adapter/RadiologyAdapter.php";
$resource = new RadiologyResource();
$adapter  = new RadiologyAdapter ();
$adapter->setMap($resource->name_map);
$adapter->setUseNumber(true, "No.","back.")
		->add("Tanggal", "tanggal","date d M Y")
		->add("No. Rad", "no_lab")
		->add("Pasien","nama_pasien")
		->add("NRM","nrm_pasien","only-digit8")
		->add("No. Reg","noreg_pasien","only-digit8")
        ->add("Nilai","biaya","money Rp.");
$arsip_admin->setAdapter($adapter);        
$arsip_admin->addResouce("js","radiology/resource/js/arsip_admin.js");
$arsip_admin->initialize();

?>