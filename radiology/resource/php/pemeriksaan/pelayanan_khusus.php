<?php

require_once "smis-base/smis-include-service-consumer.php";

global $db;
$data = $_POST;

$dbtavle = new DBTable ( $db, "smis_rad_pesanan");
$row = $dbtavle->select ( $_POST ['id_antrian'] ); // id adalah id antrian

$data ['id_antrian'] = $_POST ['id_antrian'];
$data ['polislug'] = "radiology";
$data ['noreg_pasien'] = $row->noreg_pasien;
$data ['nama_pasien'] = $row->nama_pasien;
$data ['nrm_pasien'] = $row->nrm_pasien;
$data ['action'] = "pelayanan_khusus";
$data ['page'] = "radiology";

$data ['jk'] = $row->jk;
$data ['umur'] = $row->umur;
$data ['asal_ruang'] = $row->ruangan;
$data ['page'] = "radiology";
$data ['alamat'] = $row->alamat;


if( (!isset($_POST['super_command']) || $_POST['super_command']=="") && (!isset($_POST['command']) || $_POST['command']=="") ){
    $uitable = new Table(array());
    $uitable ->addModal("_nama","text","Nama Pasien",$row->nama_pasien,"",null,true);
    $uitable ->addModal("_noreg","text","Noreg Pasien",$row->noreg_pasien,"",null,true);
    $uitable ->addModal("_nrm","text","NRM Pasien",$row->nrm_pasien,"",null,true);

    $back = new Button("","","Back");
    $back ->setIsButton(BUtton::$ICONIC_TEXT);
    $back ->setClass("btn btn-primary");
    $back ->setIcon("fa fa-backward");
    $back ->setAction("back_to_pemeriksaan()");

    $form = $uitable ->getModal()->getForm();
    $form ->setTitle("Pelayanan Khusus Radiology");
    $form ->addElement("",$back);

    echo $form->getHtml();
    echo addJS("radiology/resource/js/pelayanan_khusus.js",false);
    echo addCSS("radiology/resource/css/pelayanan_khusus.css",false);
}

$service = new ServiceConsumer ( $db, "get_lap_pkhusus", $data, "medical_record" );
$service->execute();
echo $service->getContent();
