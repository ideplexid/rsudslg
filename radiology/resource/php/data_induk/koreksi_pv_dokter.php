<?php 
global $db;
global $user;
require_once 'smis-base/smis-include-service-consumer.php';
require_once 'radiology/class/service/TarifRadiology.php';
if(isset($_POST['super_command']) && $_POST['super_command']=="total") {
	$dbtable=new DBTable($db, "smis_rad_pesanan");
	$dbtable->addCustomKriteria(NULL, "tanggal>='".$_POST['dari']."'");
	$dbtable->addCustomKriteria(NULL, "tanggal<'".$_POST['sampai']."'");
	$dbtable->addCustomKriteria("harga", "='[]'");
	$query=$dbtable->getQueryCount("");
	$content=$db->get_var($query);
	$pack=new ResponsePackage();
	$pack->setContent($content);
	$pack->setStatus(ResponsePackage::$STATUS_OK);
	echo json_encode($pack->getPackage());
	return;
}

$header=array("ID","Pasien","NRM","Biaya","Dokter","Kelas","B/L");
$uitable=new Table($header);
$uitable->setName("koreksi_pv_dokter")
		->setActionEnable(false)
		->setFooterVisible(false);

if(isset($_POST['super_command']) && $_POST['super_command']=="limit") {
	$dbtable=new DBTable($db, "smis_rad_pesanan");
	$dbtable->addCustomKriteria(NULL, "tanggal>='".$_POST['dari']."'");
	$dbtable->addCustomKriteria(NULL, "tanggal<'".$_POST['sampai']."'");
	$dbtable->addCustomKriteria("harga", "='[]'");
	$dbtable->setMaximum(1);
	$limit_start=$_POST['limit_start'];
	$data=$dbtable->view("", $limit_start);
	$rad=$data['data'][0];
	$harga = new TarifRadiology($db, $rad->kelas);
	$harga->execute ();
	$harga_radiology = $harga->getContent ();
	
	$query="SELECT count(*) FROM smis_rad_radiology WHERE id < ".$d->id." AND nrm_pasien='".$d->nrm_pasien."' AND prop!='del' ";
	$barulama=$db->get_var($query)*1>0?0:1;
	
	
	$up['id']=$rad->id;
	$dt['harga']=$harga_radiology;
	$dt['barulama']=$barulama;
	$dbtable->update($dt, $up);
	
	$simple=new SimpleAdapter();
	$simple->add("ID","id","digit6");
	$simple->add("Pasien","nama_pasien");
	$simple->add("NRM","nrm_pasien","digit8");
	$simple->add("Biaya","biaya","money Rp.");
	$simple->add("Dokter","nama_dokter");
	$simple->add("Kelas","kelas");
	$simple->add("B/L","barulama","trivial_0_B_L");
	$content=$simple->getContent($data['data']);
	$uitable->setContent($content);
	$bdcontent=$uitable->getBodyContent();	
	

	
	$pack=new ResponsePackage();
	$pack->setContent($bdcontent);
	$pack->setStatus(ResponsePackage::$STATUS_OK);
	echo json_encode($pack->getPackage());
	return;
	
}

$uitable->addModal("dari", "date", "Dari", "")
		->addModal("sampai", "date", "Sampai", "");
$action=new Button("","","View");
$action->setClass("btn-primary")
	   ->setIsButton(Button::$ICONIC)
	   ->setIcon("icon-white fa fa-circle-o-notch")
	   ->setAction("koreksi_pv_dokter.rekaptotal()");
$btn_froup=new ButtonGroup("");
$btn_froup->addButton($action);
$form=$uitable
	  ->getModal()
	  ->setTitle("Radiology")
	  ->getForm()
	  ->addElement("",$btn_froup);

$close=new Button("", "", "Batal");
$close	->addClass("btn-primary")
		->setIsButton(Button::$ICONIC_TEXT)
		->setIcon("fa fa-close")
		->setAction("koreksi_pv_dokter.batal()");

$load=new LoadingBar("rekap_rad_bar", "");
$modal=new Modal("rekap_rad_modal", "", "Processing...");
$modal	->addHTML($load->getHtml(),"after")
		->addFooter($close);

echo $form->getHtml();
echo "<div class='clear'></div>";
echo "<div id='result_koreksi_pv_dokter'>".$uitable->getHtml()."</div>";
echo $modal->getHtml();
echo addJS("framework/bootstrap/js/bootstrap-datepicker.js");
echo addJS("framework/smis/js/table_action.js");
echo addCSS("framework/bootstrap/css/datepicker.css");
echo addJS ( "base-js/smis-base-loading.js");
?>

<script type="text/javascript">
	var koreksi_pv_dokter;
	var koreksi_pv_dokter_karyawan;
	var koreksi_pv_dokter_data;
	var IS_koreksi_pv_dokter_RUNNING;
	$(document).ready(function(){
		$('.mydate').datepicker();
		koreksi_pv_dokter=new TableAction("koreksi_pv_dokter","radiology","koreksi_pv_dokter",new Array());
		koreksi_pv_dokter.addRegulerData=function(data){
			data['dari']=$("#koreksi_pv_dokter_dari").val();
			data['sampai']=$("#koreksi_pv_dokter_sampai").val();
			$("#dari_table_koreksi_pv_dokter").html(getFormattedDate(data['dari']));
			$("#sampai_table_koreksi_pv_dokter").html(getFormattedDate(data['sampai']));			
			return data;
		};

		koreksi_pv_dokter.batal=function(){
			IS_koreksi_pv_dokter_RUNNING=false;
			$("#rekap_rad_modal").modal("hide");
		};
		
		koreksi_pv_dokter.afterview=function(json){
			if(json!=null){
				$("#kode_table_koreksi_pv_dokter").html(json.nomor);
				$("#waktu_table_koreksi_pv_dokter").html(json.waktu);
				koreksi_pv_dokter_data=json;
			}
		};

		koreksi_pv_dokter.rekaptotal=function(){
			if(IS_koreksi_pv_dokter_RUNNING) return;
			$("#rekap_rad_bar").sload("true","Fetching total data",0);
			$("#rekap_rad_modal").modal("show");
			IS_koreksi_pv_dokter_RUNNING=true;
			var d=this.getRegulerData();
			d['super_command']="total";
			$.post("",d,function(res){
				var total=Number(getContent(res));
				if(total>0) {
					koreksi_pv_dokter.rekaploop(0,total);
				} else {
					$("#rekap_rad_modal").modal("hide");
					IS_koreksi_pv_dokter_RUNNING=false;
				}
			});
		};

		koreksi_pv_dokter.rekaploop=function(current,total){
			$("#rekap_rad_bar").sload("true","Calculation... [ "+current+" / "+total+" ] ",(current*100/total));
			if(current>=total || !IS_koreksi_pv_dokter_RUNNING) {
				$("#rekap_rad_modal").modal("hide");
				IS_koreksi_pv_dokter_RUNNING=false;
				return;
			}
			var d=this.getRegulerData();
			d['super_command']="limit";
			d['limit_start']=current;			
			$.post("",d,function(res){
				var ct=getContent(res);
				$("#koreksi_pv_dokter_list").append(ct);
				setTimeout(function(){koreksi_pv_dokter.rekaploop(++current,total)},300);
			});
		};
				
	});
</script>