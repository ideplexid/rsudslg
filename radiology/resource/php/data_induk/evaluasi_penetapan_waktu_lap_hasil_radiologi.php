<?php
	global $db;
	
	$form = new Form("", "", "Monitoring dan Validasi : Lap. Detail Layanan Radiologi");
	$tanggal_from_text = new Text("evaluasi_penetapan_waktu_tanggal_from", "ks_tanggal_from", date("Y-m-01"));
	$tanggal_from_text->setClass("mydate");
	$tanggal_from_text->setAtribute("data-date-format='yyyy-mm-dd'");
	$form->addElement("Tanggal Awal", $tanggal_from_text);
	$tanggal_to_text = new Text("evaluasi_penetapan_waktu_tanggal_to", "ks_tanggal_to", date("Y-m-d"));
	$tanggal_to_text->setClass("mydate");
	$tanggal_to_text->setAtribute("data-date-format='yyyy-mm-dd'");
	$form->addElement("Tanggal Akhir", $tanggal_to_text);
	$show_button = new Button("", "", "Tampilkan");
	$show_button->setClass("btn-primary");
	$show_button->setIcon("icon-white icon-repeat");
	$show_button->setIsButton(Button::$ICONIC);
	$show_button->setAction("evaluasi_penetapan_waktu.view()");
	$form->addElement("", $show_button);
	
	$table = new Table(
		array("No.", "No. RM", "Pasien", "Tanggal Pemeriksaan", "Jam Pemeriksaan", "Layanan"),
		"",
		null,
		true
	);
	$table->setName("evaluasi_penetapan_waktu");
	$table->setAction(false);
	$table->setFooterVisible(false);
	$table->setHeaderVisible(false);
	$table->addHeader("after", "
		<tr class='inverse'>
			<th rowspan='2' style='vertical-align: middle !important;'>
				<center><small>No.</small></center>
			</th>
			<th rowspan='2' style='vertical-align: middle !important;'>
				<center><small>No. Rekam Medis</small></center>
			</th>
			<th rowspan='2' style='vertical-align: middle !important;'>
				<center><small>Nama Pasien</small></center>
			</th>
			<th colspan='2' style='vertical-align: middle !important;'>
				<center><small>Waktu Pelayanan</small></center>
			</th>
			<th rowspan='2' style='vertical-align: middle !important;'>
				<center><small>Layanan</small></center>
			</th>
		</tr>
		<tr class='inverse'>
			<th style='vertical-align: middle !important;'>
				<center><small>Tanggal</small></center>
			</th>
			<th style='vertical-align: middle !important;'>
				<center><small>Jam</small></center>
			</th>
		</tr>
	");
	
	if (isset($_POST['command'])) {
		$dbtable = new DBTable($db, "smis_rad_pesanan");
		if ($_POST['command'] == "get_jumlah") {
			$tanggal_from = $_POST['tanggal_from'];
			$tanggal_to = $_POST['tanggal_to'];
			$row = $dbtable->get_row("
				SELECT COUNT(*) AS 'jumlah'
				FROM smis_rad_pesanan
				WHERE prop NOT LIKE 'del' AND tanggal >= '" . $tanggal_from . "' AND tanggal <= '" . $tanggal_to . "'
			");
			$data = array();
			$data['jumlah'] = $row->jumlah;
			$data['timestamp'] = date("d-m-Y H:i:s");
			echo json_encode($data);
		} else if ($_POST['command'] == "get_info") {
			$row_num = $_POST['row_num'];
			$tanggal_from = $_POST['tanggal_from'];
			$tanggal_to = $_POST['tanggal_to'];
			$row = $dbtable->get_row("
				SELECT *
				FROM smis_rad_pesanan
				WHERE prop NOT LIKE 'del' AND tanggal >= '" . $tanggal_from . "' AND tanggal <= '" . $tanggal_to . "'
				ORDER BY tanggal, nrm_pasien ASC
				LIMIT " . $row_num . ",1
			");
			$layanan_arr = json_decode($row->periksa, true);
			$element = "";
			foreach ( $layanan_arr as $key => $value ) {
				if ($value == 1) {
					$id_layanan = str_replace("rad_", "", $key);
					$row_layanan = $dbtable->get_row("
						SELECT *
						FROM smis_rad_layanan
						WHERE id = '" . $id_layanan . "'
					");
					$element .= "
						<tr id='data_" . $row_num . "'>
							<td id='data_" . $row_num . "_nomor'></td>
							<td id='data_" . $row_num . "_nrm'>" . ArrayAdapter::format("digit6", $row->nrm_pasien) . "</td>
							<td id='data_" . $row_num . "_nama'>" . $row->nama_pasien . "</td>
							<td id='data_" . $row_num . "_tanggal'>" . ArrayAdapter::format("date d-m-Y", $row->waktu_daftar) . "</td>
							<td id='data_" . $row_num . "_jam'>" . ArrayAdapter::format("date H:i:s", $row->waktu_daftar) . "</td>
							<td id='data_" . $row_num . "_layanan'>" . $row_layanan->nama . "</td>
						</tr>
					";
				}
			}
			$data = array();
			$data['element'] = $element;
			$data['nrm_pasien'] = ArrayAdapter::format("only-digit6", $row->nrm_pasien);
			$data['nama_pasien'] = $row->nama_pasien;
			echo json_encode($data);
		}
		return;
	}
	
	$loading_bar = new LoadingBar("loading_bar", "");
	$button = new Button("", "", "Batal");
	$button->addClass("btn-primary");
	$button->setIsButton(Button::$ICONIC_TEXT);
	$button->setIcon("fa fa-close");
	$button->setAction("evaluasi_penetapan_waktu.cancel()");
	$loading_modal = new Modal("loading_modal", "", "Proses..");
	$loading_modal->addHtml($loading_bar->getHtml(), "after");
	$loading_modal->addFooter($button);
	
	echo $loading_modal->getHtml();
	echo $form->getHtml();
	echo "<div id='table_content'>";
	echo $table->getHtml();
	echo "</div>";
	echo "<div id='info'></div>";
	echo addJS("framework/smis/js/table_action.js");
	echo addJS ("base-js/smis-base-loading.js");
	echo addJS("framework/bootstrap/js/bootstrap-datepicker.js");
	echo addCSS("framework/bootstrap/css/datepicker.css");
?>
<script type="text/javascript">
	function EvaluasiPenetapanWaktuAction(name, page, action, column) {
		this.initialize(name, page, action, column);
	}
	EvaluasiPenetapanWaktuAction.prototype.constructor = EvaluasiPenetapanWaktuAction;
	EvaluasiPenetapanWaktuAction.prototype = new TableAction();
	EvaluasiPenetapanWaktuAction.prototype.view = function() {
		if ($("#evaluasi_penetapan_waktu_tanggal_from").val() == "" || $("#evaluasi_penetapan_waktu_tanggal_to").val() == "") {
			return;
		}
		FINISHED = false;
		$("#evaluasi_penetapan_waktu_list").html("");
		$("#info").html("");
		var self = this;
		$("#loading_bar").sload("true", "Harap ditunggu...", 0);
		$("#loading_modal").smodal("show");
		var data = this.getRegulerData();
		data['command'] = "get_jumlah";
		data['tanggal_from'] = $("#evaluasi_penetapan_waktu_tanggal_from").val();
		data['tanggal_to'] = $("#evaluasi_penetapan_waktu_tanggal_to").val();
		$.post(
			"",
			data,
			function(response) {
				var json = JSON.parse(response);
				self.fill_html(0, json.jumlah);
			}
		);
	};
	EvaluasiPenetapanWaktuAction.prototype.fill_html = function(row_num, limit) {
		if (FINISHED || row_num == limit) {
			if (FINISHED == false && row_num == limit) {
				$("#info").html(
					"<div class='alert alert-block alert-info'>" +
						 "<center><strong>PROSES SELESAI</strong></center>" +
					 "</div>"
				);
			} else {
				$("#info").html(
					"<div class='alert alert-block alert-inverse'>" +
						 "<center><strong>PROSES DIBATALKAN</strong></center>" +
					 "</div>"
				);
			}
			var no = 1;
			var nor = $("tbody#evaluasi_penetapan_waktu_list").children("tr").length;
			for(var i = 0; i < nor; i++) {
				$("tr:eq(" + (i + 2) + ") td:eq(0)").html(no++);
			}
			$("#loading_modal").smodal("hide");
			return;
		}
		var self = this;
		var data = this.getRegulerData();
		data['command'] = "get_info";
		data['row_num'] = row_num;
		data['tanggal_from'] = $("#evaluasi_penetapan_waktu_tanggal_from").val();
		data['tanggal_to'] = $("#evaluasi_penetapan_waktu_tanggal_to").val();
		$.post(
			"",
			data,
			function(response) {
				var json = JSON.parse(response);
				$("#evaluasi_penetapan_waktu_list").append(json.element);
				$("#loading_bar").sload("true", json.nrm_pasien + " - " + json.nama_pasien + " (" + (row_num + 1) + " / " + limit + ")", (row_num+1) * 100 / limit - 1);
				self.fill_html(row_num + 1, limit);
			}
		);
	};
	EvaluasiPenetapanWaktuAction.prototype.cancel = function() {
		FINISHED = true;
	};
	
	var evaluasi_penetapan_waktu;
	var FINISHED;
	$(document).ready(function(){
		$('[data-toggle="popover"]').popover({
			trigger: 'hover',
			'placement': 'top'
		});
		$("#loading_modal").on("show", function() {
			$("a.close").hide();
		});
		$('.mydate').datepicker();
		evaluasi_penetapan_waktu = new EvaluasiPenetapanWaktuAction(
			"evaluasi_penetapan_waktu",
			"monitoring_validasi",
			"evaluasi_penetapan_waktu_lap_hasil_radiologi",
			new Array()
		);
	});
</script>