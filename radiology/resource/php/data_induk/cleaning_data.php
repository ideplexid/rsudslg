<?php 
global $db;
global $user;
require_once 'smis-base/smis-include-service-consumer.php';
require_once 'radiology/class/service/TarifRadiology.php';
if(isset($_POST['super_command']) && $_POST['super_command']=="total") {
	$dbtable=new DBTable($db, "smis_rad_pesanan");
	$dbtable->addCustomKriteria(NULL, "tanggal>='".$_POST['dari']."'");
	$dbtable->addCustomKriteria(NULL, "tanggal<'".$_POST['sampai']."'");
	$dbtable->addCustomKriteria(NULL, "selesai='1'");
	$query=$dbtable->getQueryCount("");
	$content=$db->get_var($query);
	$pack=new ResponsePackage();
	$pack->setContent($content);
	$pack->setStatus(ResponsePackage::$STATUS_OK);
	echo json_encode($pack->getPackage());
	return;
}

$header=array("ID","Pasien","NRM","Biaya","Dokter","Kelas","B/L");
$uitable=new Table($header);
$uitable->setName("cleaning_data")
		->setActionEnable(false)
		->setFooterVisible(false);

if(isset($_POST['super_command']) && $_POST['super_command']=="limit") {
	$dbtable=new DBTable($db, "smis_rad_pesanan");
	$dbtable->addCustomKriteria(NULL, "tanggal>='".$_POST['dari']."'");
	$dbtable->addCustomKriteria(NULL, "tanggal<'".$_POST['sampai']."'");
	$dbtable->addCustomKriteria(NULL, "selesai='1'");
	$dbtable->setMaximum(1);
	$limit_start=$_POST['limit_start'];
	$data=$dbtable->view("", $limit_start);
	$rad=$data['data'][0];
	
	$rad_hasil=json_decode($rad->hasil,true);
	$rad_periksa=json_decode($rad->periksa,true);
	foreach($rad_periksa as $name=>$value){
		if($value=="0" || $value==0){
			$rad_hasil[$name]="";
			unset($rad_hasil[$name]);
		}
	}
	
	$up['id']=$rad->id;
	$dt['hasil']=json_encode($rad_hasil);
	$dbtable->update($dt, $up);
	
	$simple=new SimpleAdapter();
	$simple->add("ID","id","digit6");
	$simple->add("Pasien","nama_pasien");
	$simple->add("NRM","nrm_pasien","digit8");
	$simple->add("Biaya","biaya","money Rp.");
	$simple->add("Dokter","nama_dokter");
	$simple->add("Kelas","kelas");
	$simple->add("B/L","barulama","trivial_0_B_L");
	$content=$simple->getContent($data['data']);
	$uitable->setContent($content);
	$bdcontent=$uitable->getBodyContent();	
	

	
	$pack=new ResponsePackage();
	$pack->setContent($bdcontent);
	$pack->setStatus(ResponsePackage::$STATUS_OK);
	echo json_encode($pack->getPackage());
	return;
	
}

$uitable->addModal("dari", "date", "Dari", "")
		->addModal("sampai", "date", "Sampai", "");
$action=new Button("","","View");
$action->setClass("btn-primary")
	   ->setIsButton(Button::$ICONIC)
	   ->setIcon("icon-white fa fa-circle-o-notch")
	   ->setAction("cleaning_data.rekaptotal()");
$btn_froup=new ButtonGroup("");
$btn_froup->addButton($action);
$form=$uitable
	  ->getModal()
	  ->setTitle("Radiology")
	  ->getForm()
	  ->addElement("",$btn_froup);

$close=new Button("", "", "Batal");
$close	->addClass("btn-primary")
		->setIsButton(Button::$ICONIC_TEXT)
		->setIcon("fa fa-close")
		->setAction("cleaning_data.batal()");


$close=new Button("", "", "Batal");
$close	->addClass("btn-primary")
		->setIsButton(Button::$ICONIC_TEXT)
		->setIcon("fa fa-close")
		->setAction("koreksi_pv_dokter.batal()");

$load=new LoadingBar("rekap_rad_bar", "");
$modal=new Modal("rekap_rad_modal", "", "Processing...");
$modal	->addHTML($load->getHtml(),"after")
		->addFooter($close);

echo $form->getHtml();
echo "<div class='clear'></div>";
echo "<div id='result_cleaning_data'>".$uitable->getHtml()."</div>";
echo $modal->getHtml();
echo addJS("framework/bootstrap/js/bootstrap-datepicker.js");
echo addJS("framework/smis/js/table_action.js");
echo addCSS("framework/bootstrap/css/datepicker.css");
echo addJS ( "base-js/smis-base-loading.js");
?>

<script type="text/javascript">
	var cleaning_data;
	var cleaning_data_karyawan;
	var cleaning_data_data;
	var IS_cleaning_data_RUNNING;
	$(document).ready(function(){
		$('.mydate').datepicker();
		cleaning_data=new TableAction("cleaning_data","radiology","cleaning_data",new Array());
		cleaning_data.addRegulerData=function(data){
			data['dari']=$("#cleaning_data_dari").val();
			data['sampai']=$("#cleaning_data_sampai").val();
			$("#dari_table_cleaning_data").html(getFormattedDate(data['dari']));
			$("#sampai_table_cleaning_data").html(getFormattedDate(data['sampai']));			
			return data;
		};

		cleaning_data.batal=function(){
			IS_cleaning_data_RUNNING=false;
			$("#rekap_rad_modal").modal("hide");
		};
		
		cleaning_data.afterview=function(json){
			if(json!=null){
				$("#kode_table_cleaning_data").html(json.nomor);
				$("#waktu_table_cleaning_data").html(json.waktu);
				cleaning_data_data=json;
			}
		};

		cleaning_data.rekaptotal=function(){
			if(IS_cleaning_data_RUNNING) return;
			$("#rekap_rad_bar").sload("true","Fetching total data",0);
			$("#rekap_rad_modal").modal("show");
			IS_cleaning_data_RUNNING=true;
			var d=this.getRegulerData();
			d['super_command']="total";
			$.post("",d,function(res){
				var total=Number(getContent(res));
				if(total>0) {
					cleaning_data.rekaploop(0,total);
				} else {
					$("#rekap_rad_modal").modal("hide");
					IS_cleaning_data_RUNNING=false;
				}
			});
		};

		cleaning_data.rekaploop=function(current,total){
			$("#rekap_rad_bar").sload("true","Calculation... [ "+current+" / "+total+" ] ",(current*100/total));
			if(current>=total || !IS_cleaning_data_RUNNING) {
				$("#rekap_rad_modal").modal("hide");
				IS_cleaning_data_RUNNING=false;
				return;
			}
			var d=this.getRegulerData();
			d['super_command']="limit";
			d['limit_start']=current;			
			$.post("",d,function(res){
				var ct=getContent(res);
				$("#cleaning_data_list").append(ct);
				setTimeout(function(){cleaning_data.rekaploop(++current,total)},3000);
			});
		};
				
	});
</script>