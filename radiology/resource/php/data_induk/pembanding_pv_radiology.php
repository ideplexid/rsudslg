<?php 
global $db;
global $user;
require_once 'smis-base/smis-include-service-consumer.php';

if(isset($_POST['super_command']) && $_POST['super_command']=="total") {	
	$rtable=new DBTable($db, "smis_rad_pesanan");
	$rtable->addCustomKriteria("tanggal", "='".$_POST['tanggal']."'");
	$qry=$rtable->getQueryCount("");
	$total=$db->get_var($qry);
	
	$query="truncate smis_rad_pembanding";
	$db->query($query);
	
	$pack=new ResponsePackage();
	$pack->setContent($total);
	$pack->setStatus(ResponsePackage::$STATUS_OK);
	echo json_encode($pack->getPackage());
	return;
}
//"Plain Foto","Contrast","USG","CT Scan","Dental"

$header=array("No.",
		"Pasien","NRM","No Reg","No Rad","Pengirim","Konsultan","Petugas","Kelas","Biaya",
		"PF","%p-PF","p-PF","%d-PF","d-PF","%k-PF","k-PF",
		"CS","%p-CS","p-CS","%d-CS","d-CS","%k-CS","k-CS",
		"USG","%p-USG","p-USG","%d-USG","d-USG","%k-USG","k-USG",
		"CTS","%p-CTS","p-CTS","%d-CTS","d-CTS","%k-CTS","k-CTS",
		"DT","%p-DT","p-DT","%d-DT","d-DT","%k-DT","k-DT",
		);


$header1='<tr>
<th rowspan="3">No.</th>
<th rowspan="3">Pasien</th>
<th rowspan="3">NRM</th>
<th rowspan="3">No Reg</th>
<th rowspan="3">No Rad</th>
<th rowspan="3">Pengirim</th>
<th rowspan="3">Konsultan</th>
<th rowspan="3">Petugas</th>
<th rowspan="3">Kelas</th>
<th rowspan="3">Biaya</th>
<th colspan="7">Plain Foto</th>
<th colspan="7">Contrast Study</th>
<th colspan="7">Ultrasonography</th>
<th colspan="7">CT - Scan</th>
<th colspan="7">Dental</th>
</tr>';


$header2='<tr>
<th rowspan="2">Total</th>
<th colspan="2">Petugas</th>
<th colspan="2">Pengirim</th>
<th colspan="2">Konsultan</th>	
<th rowspan="2">Total</th>
<th colspan="2">Petugas</th>
<th colspan="2">Pengirim</th>
<th colspan="2">Konsultan</th>	
<th rowspan="2">Total</th>
<th colspan="2">Petugas</th>
<th colspan="2">Pengirim</th>
<th colspan="2">Konsultan</th>	
<th rowspan="2">Total</th>
<th colspan="2">Petugas</th>
<th colspan="2">Pengirim</th>
<th colspan="2">Konsultan</th>	
<th rowspan="2">Total</th>
<th colspan="2">Petugas</th>
<th colspan="2">Pengirim</th>
<th colspan="2">Konsultan</th>
</tr>';


$header3='<tr>
			<th>%</th>
			<th>Rp.</th>
			<th>%</th>
			<th>Rp.</th>
			<th>%</th>
			<th>Rp.</th>
			<th>%</th>
			<th>Rp.</th>
			<th>%</th>
			<th>Rp.</th>
			<th>%</th>
			<th>Rp.</th>
			<th>%</th>
			<th>Rp.</th>
			<th>%</th>
			<th>Rp.</th>
			<th>%</th>
			<th>Rp.</th>
			<th>%</th>
			<th>Rp.</th>
			<th>%</th>
			<th>Rp.</th>
			<th>%</th>
			<th>Rp.</th>
			<th>%</th>
			<th>Rp.</th>
			<th>%</th>
			<th>Rp.</th>
			<th>%</th>
			<th>Rp.</th>
		</tr>';

$uitable=new Table($header);
$uitable->addHeader("before", $header1);
$uitable->addHeader("before", $header2);
$uitable->addHeader("before", $header3);
$uitable->setHeaderVisible(false);
if(isset($_POST['super_command']) && $_POST['super_command']=="limit") {	
	require_once 'radiology/class/adapter/RadiologySalaryCounter.php';	
	$rtable=new DBTable($db, "smis_rad_pesanan");
	$view=array("id","nama_dokter","id_dokter","nama_konsultan","id_konsultan","nama_petugas","id_petugas","nama_pasien","nrm_pasien","noreg_pasien","kelas","no_lab","biaya","harga","periksa","pembagian","kelas");
	$rtable->setView($view);
	$rtable->addCustomKriteria("tanggal", "='".$_POST['tanggal']."'");
	$rtable->setMaximum(1);
	$one=$rtable->view("", $_POST['limit_start']);	
	$adapter=new RadiologySalaryCounter();
	$svx=$adapter->adapt($one['data'][0]);
	$dbtable=new DBTable($db, "smis_rad_pembanding");
	$result=$dbtable->insert($svx);
	$pack=new ResponsePackage();
	$pack->setContent($result);
	$pack->setStatus(ResponsePackage::$STATUS_OK);
	echo json_encode($pack->getPackage());
	return;
}

$uitable->setName("pembanding_pv_radiology")
		->setActionEnable(false)
		->setFooterVisible(false);

if(isset($_POST['command']) && $_POST['command']== "list") {	
	$dbtable=new DBTable($db, "smis_rad_pembanding");
	$dbtable->setFetchMethode(DBTable::$ARRAY_FETCH);
	$dbtable->setShowAll(true);
	$data=$dbtable->view("", "0");
	
	$adapter=new SummaryAdapter(true,"No.");
	$adapter->add("Pasien", "nama_pasien");
	$adapter->add("NRM", "nrm_pasien","digit6");
	$adapter->add("No Reg", "noreg_pasien","digit6");
	$adapter->add("No Rad", "norad");
	$adapter->add("Pengirim", "nama_pengirim");
	$adapter->add("Konsultan", "nama_konsultan");
	$adapter->add("Petugas", "nama_petugas");
	$adapter->add("Kelas", "kelas","unslug");
	$adapter->add("Biaya", "biaya","money Rp.");
	$adapter->add("PF", "plain","money Rp.");
	$adapter->add("%p-PF", "pp_plain","back%");
	$adapter->add("p-PF", "p_plain","money Rp.");
	$adapter->add("%d-PF", "pd_plain","back%");
	$adapter->add("d-PF", "d_plain","money Rp.");
	$adapter->add("%k-PF", "pk_plain","back%");
	$adapter->add("k-PF", "k_plain","money Rp.");	
	$adapter->add("CS", "contrast","money Rp.");
	$adapter->add("%p-CS", "pp_contrast","back%");
	$adapter->add("p-CS", "p_contrast","money Rp.");
	$adapter->add("%d-CS", "pd_contrast","back%");
	$adapter->add("d-CS", "d_contrast","money Rp.");
	$adapter->add("%k-CS", "pk_contrast","back%");
	$adapter->add("k-CS", "k_contrast","money Rp.");	
	$adapter->add("USG", "usg","money Rp.");
	$adapter->add("%p-USG", "pp_usg","back%");
	$adapter->add("p-USG", "p_usg","money Rp.");
	$adapter->add("%d-USG", "pd_usg","back%");
	$adapter->add("d-USG", "d_usg","money Rp.");
	$adapter->add("%k-USG", "pk_usg","back%");
	$adapter->add("k-USG", "k_usg","money Rp.");	
	$adapter->add("CTS", "ctscan","money Rp.");
	$adapter->add("%p-CTS", "pp_ctscan","back%");
	$adapter->add("p-CTS", "p_ctscan","money Rp.");
	$adapter->add("%d-CTS", "dp_ctscan","back%");
	$adapter->add("d-CTS", "d_ctscan","money Rp.");
	$adapter->add("%k-CTS", "pk_ctscan","back%");
	$adapter->add("k-CTS", "k_ctscan","money Rp.");	
	$adapter->add("DT", "dental","money Rp.");
	$adapter->add("%p-DT", "pp_dental","back%");
	$adapter->add("p-DT", "p_dental","money Rp.");
	$adapter->add("%d-DT", "pd_dental","back%");
	$adapter->add("d-DT", "d_dental","money Rp.");
	$adapter->add("%k-DT", "pk_dental","back%");
	$adapter->add("k-DT", "k_dental","money Rp.");
	$adapter->addFixValue("Kelas", "<strong>Total</strong>");
	$adapter->addSummary("Biaya", "biaya","money Rp.");
	$adapter->addSummary("PF", "plain","money Rp.");
	$adapter->addSummary("p-PF", "p_plain","money Rp.");
	$adapter->addSummary("d-PF", "d_plain","money Rp.");
	$adapter->addSummary("k-PF", "k_plain","money Rp.");
	$adapter->addSummary("CS", "contrast","money Rp.");
	$adapter->addSummary("p-CS", "p_contrast","money Rp.");
	$adapter->addSummary("d-CS", "d_contrast","money Rp.");
	$adapter->addSummary("k-CS", "k_contrast","money Rp.");
	$adapter->addSummary("USG", "usg","money Rp.");
	$adapter->addSummary("p-USG", "p_usg","money Rp.");
	$adapter->addSummary("d-USG", "d_usg","money Rp.");
	$adapter->addSummary("k-USG", "k_usg","money Rp.");
	$adapter->addSummary("CTS", "ctscan","money Rp.");
	$adapter->addSummary("p-CT", "p_ctscan","money Rp.");
	$adapter->addSummary("d-CTS", "d_ctscan","money Rp.");
	$adapter->addSummary("k-CTS", "k_ctscan","money Rp.");
	$adapter->addSummary("DT", "dental","money Rp.");
	$adapter->addSummary("p-DT", "p_dental","money Rp.");
	$adapter->addSummary("d-DT", "d_dental","money Rp.");
	$adapter->addSummary("k-DT", "k_dental","money Rp.");
	
	//$adapter->setRemoveZeroEnable(true);
	$uidata=$adapter->getContent($data['data']);
	$list=$uitable->setContent($uidata)->getBodyContent();
	
	$json['list']=$list;
	$json['pagination']="";
	$json['number']="0";
	$json['number_p']="0";
	$json['data']=$uidata;
	$json['waktu']=ArrayAdapter::format("date d M Y H:i:s", date("Y-m-d H:i:s"));
	$json['nomor']="RRAD-".date("dmy-his")."-".substr(md5($user->getNameOnly()), 5,rand(2, 5));
	$ft=ArrayAdapter::format("date d M Y", $_POST['dari']);
	$to=ArrayAdapter::format("date d M Y", $_POST['sampai']);	
	$json['json']=json_encode($uidata);
	$json['status']=1;
	
	$pack=new ResponsePackage();
	$pack->setContent($json);
	$pack->setStatus(ResponsePackage::$STATUS_OK);
	echo json_encode($pack->getPackage());
	return;
}

$pjkserv=new ServiceConsumer($db, "get_pajak");
$pjkserv->execute();
$pajak=$pjkserv->getContent();

$uitable->addModal("tanggal", "date", "Tanggal", "");
$action=new Button("","","View");
$action->setClass("btn-primary")
	   ->setIsButton(Button::$ICONIC)
	   ->setIcon("fa fa-circle-o-notch")
	   ->setAction("pembanding_pv_radiology.rekaptotal()");
$print=new Button("","","View");
$print->setClass("btn-primary")
		->setIsButton(Button::$ICONIC)
		->setIcon("fa fa-print")
		->setAction("pembanding_pv_radiology.print()");


$btn_froup=new ButtonGroup("");
$btn_froup->addButton($action);
$btn_froup->addButton($print);
$form=$uitable
	  ->getModal()
	  ->setTitle("Radiology")
	  ->getForm()
	  ->addElement("",$btn_froup);

$close=new Button("", "", "Batal");
$close	->addClass("btn-primary")
		->setIsButton(Button::$ICONIC_TEXT)
		->setIcon("fa fa-close")
		->setAction("pembanding_pv_radiology.batal()");

$load=new LoadingBar("rekap_rad_bar", "");
$modal=new Modal("rekap_rad_modal", "", "Processing...");
$modal	->addHTML($load->getHtml(),"after")
		->addFooter($close);

echo $form->getHtml();
echo "<div class='clear'></div>";
echo "<div id='result_pembanding_pv_radiology'>".$uitable->getHtml()."</div>";
echo $modal->getHtml();
echo addJS("framework/bootstrap/js/bootstrap-datepicker.js");
echo addJS("framework/smis/js/table_action.js");
echo addCSS("framework/bootstrap/css/datepicker.css");
echo addJS ( "base-js/smis-base-loading.js");
?>

<script type="text/javascript">
	var pembanding_pv_radiology;
	var pembanding_pv_radiology_karyawan;
	var pembanding_pv_radiology_data;
	var IS_pembanding_pv_radiology_RUNNING;
	$(document).ready(function(){
		$('.mydate').datepicker();
		pembanding_pv_radiology=new TableAction("pembanding_pv_radiology","radiology","pembanding_pv_radiology",new Array());
		pembanding_pv_radiology.addRegulerData=function(data){
			data['tanggal']=$("#pembanding_pv_radiology_tanggal").val();
			$("#tanggal_table_pembanding_pv_radiology").html(getFormattedDate(data['tanggal']));
			return data;
		};

		pembanding_pv_radiology.batal=function(){
			IS_pembanding_pv_radiology_RUNNING=false;
			$("#rekap_rad_modal").modal("hide");
		};
		
		pembanding_pv_radiology.afterview=function(json){
			if(json!=null){
				$("#kode_table_pembanding_pv_radiology").html(json.nomor);
				$("#waktu_table_pembanding_pv_radiology").html(json.waktu);
				pembanding_pv_radiology_data=json;
			}
		};

		pembanding_pv_radiology.rekaptotal=function(){
			if(IS_pembanding_pv_radiology_RUNNING) return;
			$("#rekap_rad_bar").sload("true","Fetching total data",0);
			$("#rekap_rad_modal").modal("show");
			IS_pembanding_pv_radiology_RUNNING=true;
			var d=this.getRegulerData();
			d['super_command']="total";
			$.post("",d,function(res){
				var total=Number(getContent(res));
				if(total>0) {
					pembanding_pv_radiology.rekaploop(0,total);
				} else {
					$("#rekap_rad_modal").modal("hide");
					IS_pembanding_pv_radiology_RUNNING=false;
				}
			});
		};

		pembanding_pv_radiology.rekaploop=function(current,total){
			$("#rekap_rad_bar").sload("true","Calculation... [ "+current+" / "+total+" ] ",(current*100/total));
			if(current>=total || !IS_pembanding_pv_radiology_RUNNING) {
				$("#rekap_rad_modal").modal("hide");
				pembanding_pv_radiology.view();
				IS_pembanding_pv_radiology_RUNNING=false;
				return;
			}
			var d=this.getRegulerData();
			d['super_command']="limit";
			d['limit_start']=current;			
			$.post("",d,function(res){
				setTimeout(function(){pembanding_pv_radiology.rekaploop(++current,total)},300);
			});
		};
				
	});
</script>