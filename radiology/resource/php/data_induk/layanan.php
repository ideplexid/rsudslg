<?php
global  $db;
require_once ("smis-base/smis-include-service-consumer.php");
require_once "smis-libs-class/MasterTemplate.php";
require_once "smis-base/smis-include-service-consumer.php";

$dbtable=new DBTable($db,"smis_rad_grup");
$dbtable->setShowAll(true);
$data=$dbtable->view("","0");
$adapter=new SelectAdapter("nama","slug");
$list_grup=$adapter->getContent($data['data']);

$layanan=new MasterTemplate($db,"smis_rad_layanan","radiology","layanan");
$uitable = $layanan->getUItable();
$uitable->addModal ( "id", "hidden", "", "" );
$uitable->addModal ( "nama", "text", "Nama", "" );
$uitable->addModal ( "laporan", "text", "Kelompok Laporan", "" );
$uitable->addModal ( "layanan", "select", "Layanan", $list_grup );
$uitable->addModal ( "f1824", "text", "Film 18 x 24 cm", "" );
$uitable->addModal ( "f2430", "text", "Film 24 x 30 cm", "" );
$uitable->addModal ( "f3040", "text", "Film 30 x 40 cm", "" );
$uitable->addModal ( "f3535", "text", "Film 35 x 35 cm", "" );
$uitable->addModal ( "f3543", "text", "Film 35 x 43 cm", "" );
$uitable->addModal ("D-SDQ 43x35", "text", "sdq4335","");
$uitable->addModal ("DHF 35x43", "text", "dhf3543","");
$uitable->addModal ("DHF 26x36", "text", "dhf2636","");
$uitable->addModal ("DHF 20x25", "text", "dhf2025","");
$uitable->addModal ("DVB 35x43", "text", "dvb3543","");
$uitable->addModal ("DVB 35x28", "text", "dvb3528","");
$uitable->addModal ("DVB 20x25", "text", "dvb2025","");
$uitable->addModal ("F Dental", "text", "fdental","");
$uitable->addModal ( "pria", "summernote", "Normal Pria", "" );
$uitable->addModal ( "wanita", "summernote", "Normal Wanita", "" );
$uitable->addModal("debet", "chooser-layanan-debet_layanan-Kode Debet Default", "Kode Debet", "","y",NULL,true,NULL,false);
$uitable->addModal("kredit", "chooser-layanan-kredit_layanan-Kode Kredit Default", "Kode Kredit", "","y",NULL,true,NULL,false);


$layanan->setModalTitle("Layanan Radiology");
$array=array ('Nama','Layanan','Laporan', 'Kode Debet', 'Kode Kredit', '18 x 24 cm','24 x 30 cm','30 x 40 cm','35 x 35 cm','35 x 43 cm');
$layanan->getUItable()->setHeader($array);
$adapter = $layanan->getAdapter();
$adapter->add ( "Nama", "nama" );
$adapter->add ( "Layanan", "layanan" );
$adapter->add ( "Laporan", "laporan" );
$adapter->add ( "Kode Debet", "debet" );
$adapter->add ( "Kode Kredit", "kredit" );
$adapter->add ( "18 x 24 cm", "f1824" );
$adapter->add ( "24 x 30 cm", "f2430" );
$adapter->add ( "30 x 40 cm", "f3040" );
$adapter->add ( "35 x 35 cm", "f3535" );
$adapter->add ( "35 x 43 cm", "f3543" );
$adapter->add("D-SDQ 43x35", "sdq4335");
$adapter->add("DHF 35x43", "dhf3543");
$adapter->add("DHF 26x36", "dhf2636");
$adapter->add("DHF 20x25", "dhf2025");
$adapter->add("DVB 35x43", "dvb3543");
$adapter->add("DVB 35x28", "dvb3528");
$adapter->add("DVB 20x25", "dvb2025");
$adapter->add("F Dental", "fdental");
$adapter->setUseNumber(true,"No.","back.");

$layanan->getDBTable()->setOrder(true," grup ASC, nama ASC ");
if($layanan->getDBResponder()->isPreload() || $_POST['super_command']!=""  ){
    if(strpos($_POST['super_command'],"debet")!==false || strpos($_POST['super_command'],"kredit")!==false ){
        $uitable   = new Table(array('Nomor','Nama'),"");
        $uitable   ->setName($_POST['super_command'])
                   ->setModel(Table::$SELECT);
        $adapter   = new SimpleAdapter();
        $adapter   ->add("Nomor","nomor")
                   ->add("Nama","nama");
        $responder = new ServiceResponder($db, $uitable, $adapter, "get_account", "accounting");
        $layanan   ->getSuperCommand()
                   ->addResponder($_POST['super_command'],$responder);
        $layanan   ->addSuperCommand($_POST['super_command'],array())
                   ->addSuperCommandArray($_POST['super_command'],substr($_POST['super_command'],0,5),"nomor");
    }
}

if($layanan->getDBResponder()->isPreload() && $_POST['super_command']==""  ){
    $serv = new ServiceConsumer($db,"get_jenis_patient",NULL,"registration");
    $serv ->execute();
    $cons = $serv->getContent();
    global $wpdb;
    foreach($cons as $x){
        require_once "smis-libs-class/DBCreator.php";
        $dbcreator = new DBCreator($wpdb,"smis_rad_layanan",DBCreator::$ENGINE_MYISAM);
        $dbcreator ->addColumn("debet_".$x['value'], "varchar(64)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
        $dbcreator ->addColumn("kredit_".$x['value'], "varchar(64)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
        $dbcreator ->initialize();
        $uitable   ->addModal("debet_".$x['value'], "chooser-layanan-debet_layanan_".$x['value']."-Kode Debet ".$x['name'], "Kode Debet ".$x['name'], "","y",NULL,true,NULL,false);
        $uitable   ->addModal("kredit_".$x['value'], "chooser-layanan-kredit_layanan_".$x['value']."-Kode Kredit".$x['name'], "Kode Kredit ".$x['name'], "","y",NULL,true,NULL,false);
        $layanan   ->addSuperCommand("debet_layanan_".$x['value'],array())
                   ->addSuperCommandArray("debet_layanan_".$x['value'],"debet_".$x['value'],"nomor");
        $layanan   ->addSuperCommand("kredit_layanan_".$x['value'],array())
                   ->addSuperCommandArray("kredit_layanan_".$x['value'],"kredit_".$x['value'],"nomor");
    }    
    $layanan  ->addSuperCommand("debet_layanan",array())
              ->addSuperCommandArray("debet_layanan","debet","nomor");
    $layanan  ->addSuperCommand("kredit_layanan",array())
              ->addSuperCommandArray("kredit_layanan","kredit","nomor");
}
$layanan->setModalComponentSize(Modal::$MEDIUM);
$layanan->initialize();

?>