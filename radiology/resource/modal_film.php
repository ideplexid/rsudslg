<?php

global $db;

/*$ret = "<div";

$set_result = "";
$label = new Label("", "", "Result");
$set_result .= $label->getHtml ();
$text = new Text("pemeriksaan_result", "pemeriksaan_result", "", "text");
$set_result .= $text->getHtml ();

$ret .= $set_result;
$ret .= "</div>";
echo $ret;*/

class RadiologyModalFilm extends HTML {
    public function getHtml() {
        $form = new Form ( "film_radiology", "div_film_radiology", "" );
        
        $result = "";
        
        $froll = new Text("film_froll", "film_froll", "", "text");
        $f1824 = new Text("film_f1824", "film_f1824", "", "text");
        $f2430 = new Text("film_f2430", "film_f2430", "", "text");
        $f3040 = new Text("film_f3040", "film_f3040", "", "text");
        $f3535 = new Text("film_f3535", "film_f3535", "", "text");
        $f3543 = new Text("film_f3543", "film_f3543", "", "text");
        $sdq4335 = new Text("film_sdq4335", "film_sdq4335", "", "text");
        $dhf3543 = new Text("film_dhf3543", "film_dhf3543", "", "text");
        $dhf2636 = new Text("film_dhf2636", "film_dhf2636", "", "text");
        $dhf2025 = new Text("film_dhf2025", "film_dhf2025", "", "text");
        $dvb3543 = new Text("film_dvb3543", "film_dvb3543", "", "text");
        $dvb3528 = new Text("film_dvb3528", "film_dvb3528", "", "text");
        $dvb2025 = new Text("film_dvb2025", "film_dvb2025", "", "text");
        $fdental = new Text("film_fdental", "film_fdental", "", "text");
        
        $form->addElement("F Roll", $froll);
        $form->addElement("F 18x24", $f1824);
        $form->addElement("F 24x30", $f2430);
        $form->addElement("F 30x40", $f3040);
        $form->addElement("F 35x35", $f3535);
        $form->addElement("F 35x43", $f3543);
        $form->addElement("D-SDQ 43x35", $sdq4335);
        $form->addElement("DHF 35x43", $dhf3543);
        $form->addElement("DHF 26x36", $dhf2636);
        $form->addElement("DHF 20x25", $dhf2025);
        $form->addElement("DVB 35x43", $dvb3543);
        $form->addElement("DVB 35x28", $dvb3528);
        $form->addElement("DVB 20x25", $dvb2025);
        $form->addElement("F Dental", $fdental);
        
        if(getSettings($this->db,"radiology-show-reject","0")=="1") {
            $frollr = new Text("film_frollr", "film_frollr", "", "text");
            $f1824r = new Text("film_f1824r", "film_f1824r", "", "text");
            $f2430r = new Text("film_f2430r", "film_f2430r", "", "text");
            $f3040r = new Text("film_f3040r", "film_f3040r", "", "text");
            $f3535r = new Text("film_f3535r", "film_f3535r", "", "text");
            $f3543r = new Text("film_f3543r", "film_f3543r", "", "text");
            $fdentalr = new Text("film_fdentalr", "film_fdentalr", "", "text");
            
            $form->addElement("RF Roll", $frollr);
            $form->addElement("RF 18x24", $f1824r);
            $form->addElement("RF 24x30", $f2430r);
            $form->addElement("RF 30x40", $f3040r);
            $form->addElement("RF 35x35", $f3535r);
            $form->addElement("RF 35x43", $f3543r);
            $form->addElement("RF Dental", $fdentalr);
        } else {
            $frollr = new Hidden("film_frollr", "film_frollr", "");
            $f1824r = new Hidden("film_f1824r", "film_f1824r", "");
            $f2430r = new Hidden("film_f2430r", "film_f2430r", "");
            $f3040r = new Hidden("film_f3040r", "film_f3040r", "");
            $f3535r = new Hidden("film_f3535r", "film_f3535r", "");
            $f3543r = new Hidden("film_f3543r", "film_f3543r", "");
            $fdentalr = new Hidden("film_fdentalr", "film_fdentalr", "");
            
            $form->addElement("", $frollr);
            $form->addElement("", $f1824r);
            $form->addElement("", $f2430r);
            $form->addElement("", $f3040r);
            $form->addElement("", $f3535r);
            $form->addElement("", $f3543r);
            $form->addElement("", $fdentalr);
        }
        
        
        
        
        $result .= $form->getHtml ();

        return $result;
    }
}

?>