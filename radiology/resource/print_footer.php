<?php
	global $db;
	$nama_konsultan = getSettings ( $db, "radiology-konsultan-nama", "" );
    $ttd_konsultan = getSettings ( $db, "radiology-konsultan-ttd", "" );
    $ttd = "smis-upload/".$ttd_konsultan;
	$title = getSettings ( $db, "smis_autonomous_title", "" );
	$tp = new TablePrint ( "pfooter" );
	$tp->setMaxWidth ( false );
	$tp	->addColumn ( "CATATAN : ", 1, 1,NULL,NULL,"left" )
		->addColumn ( "Salam Sejawat,", 1, 1, "title",NULL,"center" );
	$gapttd= getSettings ( $db, "radiology-print-header-ttd", "20" );
	if($gaptom!="0"){
		$tp	->addColumn ( "<div style='height:".$gapttd."px !important; width:100%'>&nbsp;</div>", 1, 1, "title", "" );
	}
    if($ttd_konsultan != "") {
        $tp	->addColumn ( "", 1, 1 )
            //->addColumn ( "<br/><br/>", 1, 1, "title",NULL,"center" );
            ->addColumn ( "<img src='" . $ttd . "' style='width:100px;height:100px;' />", 1, 1, "title",NULL,"header_logo" );
    } else {
        $tp	->addColumn ( "", 1, 1 )
            ->addColumn ( "<br/><br/><br/>", 1, 1, "title",NULL,"center" );
    }
	
	$tp	->addColumn ( "", 1, 1 )
		->addColumn ( "<u>" . $nama_konsultan . "</u>", 1, 1, "title",NULL,"center" );
	$tp	->addColumn ( "", 1, 1 )
		->addColumn ( "<small>Ahli Radiologi</small>", 1, 1, "title",NULL,"center" );
	echo "<div id='print_footer_radiology' class='hide'>" . $tp->getHtml () . "</div>";
?>














