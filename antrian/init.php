<?php
	global $PLUGINS;
	
	$init['name']        = "antrian";
	$init['path']        = SMIS_DIR . $init['name'] . "/";
	$init['description'] = "Antrian";
	$init['require']     = "administrator";
	$init['service']     = "";
	$init['version']     = "1.0.0";
	$init['number']      = "1";
	$init['type']        = "";
	
	$plugin = new Plugin($init);
	$PLUGINS[$init['name']] = $plugin;
?>