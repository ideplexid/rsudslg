<?php
	global $NAVIGATOR;

	$mr = new Menu ("fa fa-list");
	$mr->addProperty ( 'title', 'Antrian' );
	$mr->addProperty ( 'name', 'Antrian' );

	$mr->addLaravel("Pendaftaran","antrian","pendaftaran","","fa fa-list");
	$mr->addLaravel("Poli","antrian","poli","","fa fa-list");
	$mr->addLaravel("Farmasi","antrian","farmasi","","fa fa-list");
	$mr->addLaravel("Operasi","antrian","operasi","","fa fa-list");
	$mr->addLaravel("Waktu Task ID","antrian","list_task_id","","fa fa-list");
	$mr->addLaravel("Rekap Antrian","antrian","rekap","","fa fa-file");
	$mr->addLaravel("Display Pendaftaran Lt 1","antrian","pendaftaran_display?lantai=1","","fa fa-television");
	$mr->addLaravel("Display Pendaftaran Lt 2","antrian","pendaftaran_display?lantai=2","","fa fa-television");
	$mr->addLaravel("Display Poli Lt 1","antrian","poli_display?lantai=1","","fa fa-television");
	$mr->addLaravel("Display Poli Lt 2","antrian","poli_display?lantai=2","","fa fa-television");
	$mr->addLaravel("Display Farmasi Lt 1","antrian","farmasi_display?lantai=1","","fa fa-television");
	$mr->addLaravel("Display Farmasi Lt 2","antrian","farmasi_display?lantai=2","","fa fa-television");
	$mr->addLaravel("Display Operasi","antrian","operasi_display","","fa fa-television");
	$NAVIGATOR->addMenu ( $mr, 'antrian' );
?>
