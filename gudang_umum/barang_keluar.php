<?php
	require_once("smis-base/smis-include-service-consumer.php");
	require_once("smis-base/smis-include-duplicate.php");
	
	class BarangKeluarTable extends Table {
		public function getBodyContent(){
			$content = "";
			if ($this->content!=NULL) {
				foreach ($this->content as $d) {
					$content .= "<tr>";
					foreach ($this->header as $h) {
						$content .= "<td>" . $d[$h] . "</td>";
					}
					if ($this->is_action) {
						$content .= "<td>" . $this->getFilteredContentButton($d['id'], $d['status'])->getHtml() . "</td>";
					}
					$content .= "</tr>";
				}
			}
			return $content;
		}
		public function getFilteredContentButton($id, $status) {
			$btn_group = new ButtonGroup("noprint");
			if ($status == "sudah") {
				$btn = new Button("", "", "Lihat");
				$btn->setAction($this->action . ".detail('" . $id . "')");
				$btn->setClass("btn-success");
				$btn->setAtribute("data-content='Lihat' data-toggle='popover'");
				$btn->setIcon("icon-eye-open icon-white");
				$btn->setIsButton(Button::$ICONIC);
				$btn_group->addElement($btn);
			} else {
				$btn = new Button("", "", "Ubah");
				if ($status == "dikembalikan") {
					$btn->setClass("btn-inverse");
				} else {
					$btn->setClass("btn-info");
				}
				$btn->setAction($this->action . ".detail('" . $id . "')");
				$btn->setAtribute("data-content='Detail' data-toggle='popover'");
				$btn->setIcon("icon-eye-open icon-white");
				$btn->setIsButton(Button::$ICONIC);
				$btn_group->addElement($btn);
				if ($status != "dikembalikan") {
					$btn = new Button("", "", "Hapus");
					$btn->setAction($this->action . ".del('" . $id . "')");
					$btn->setClass("btn-danger");
					$btn->setAtribute("data-content='Hapus' data-toggle='popover'");
					$btn->setIcon("icon-remove icon-white");
					$btn->setIsButton(Button::$ICONIC);
					$btn_group->addElement($btn);
				}
			}
			$btn = new Button("", "", "Cetak");
			$btn->setAction($this->action . ".print_mutasi('" . $id . "', '" . $status . "')");
			$btn->setClass("btn-inverse");
			$btn->setAtribute("data-content='Cetak' data-toggle='popover'");
			$btn->setIcon("fa fa-print");
			$btn->setIsButton(Button::$ICONIC);
			$btn_group->addElement($btn);
			return $btn_group;
		}
	}
	$barang_keluar_table = new BarangKeluarTable(
		array("Nomor", "Tanggal Keluar", "Unit", "Status"),
		"Gudang Umum : Barang Keluar",
		null,
		true
	);
	$barang_keluar_table->setName("barang_keluar");
	
	if (isset($_POST['super_command']) && $_POST['super_command'] == "barang_keluar") {
		if (isset($_POST['command'])) {
			class BarangKeluarAdapter extends ArrayAdapter {
				public function adapt($row) {
					$array = array();
					$array['id'] = $row->id;
					$array['status'] = $row->status;
					$array['Nomor'] = self::format("digit8", $row->id);
					$array['Tanggal Keluar'] = self::format("date d M Y", $row->tanggal);
					$array['Unit'] = self::format("unslug", $row->unit);
					if ($row->status == "sudah")
						$array['Status'] = "Sudah Diterima";
					else if ($row->status == "dikembalikan")
						$array['Status'] = "Dikembalikan";
					else
						$array['Status'] = "Belum Diterima";
					return $array;
				}
			}
			$barang_keluar_adapter = new BarangKeluarAdapter();
			$columns = array("id", "tanggal", "unit", "status", "keterangan");
			$barang_keluar_dbtable = new DBTable(
				$db,
				"smis_gd_barang_keluar",
				$columns
			);
			class BarangKeluarDBResponder extends DuplicateResponder {
				public function command($command) {
					if ($command != "print_mutasi")
						return parent::command($command);
					$pack = new ResponsePackage();
					if ($command == "print_mutasi") {
						$content = $this->print_mutasi();
						$pack->setContent($content);
						$pack->setStatus(ResponsePackage::$STATUS_OK);
					}
					return $pack->getPackage();
				}
				public function print_mutasi() {
					$id = $_POST['id'];
					$status = $_POST['status'];
					$row_header = $this->dbtable->get_row("
						SELECT *
						FROM smis_gd_barang_keluar
						WHERE id = '" . $id . "'
					");
					$rows_detail = $this->dbtable->get_result("
						SELECT *
						FROM smis_gd_dbarang_keluar
						WHERE id_barang_keluar = '" . $id . "'
					");
					$print_data = "";
					$print_data .= "<center><strong>NOTA MUTASI BARANG NO. " . ArrayAdapter::format("only-digit8", $id) . "</strong></center><br/>";
					$print_data .= "<table border='0'>
										<tr>
											<td>Unit Pengirim</td>
											<td>:</td>
											<td>" . ArrayAdapter::format("unslug", "gudang_umum") . "</td>
										</tr>
										<tr>
											<td>Unit Penerima</td>
											<td>:</td>
											<td>" . ArrayAdapter::format("unslug", $row_header->unit) . "</td>
										</tr>
										<tr>
											<td>Tanggal</td>
											<td>:</td>
											<td>" . ArrayAdapter::format("date d M Y", $row_header->tanggal) . "</td>
										</tr>
										<tr>
											<td>Status</td>
											<td>:</td>
											<td>" . ($status == 'belum'? "SUDAH / DIKEMBALIKAN" : ArrayAdapter::format("unslug", $status)) . "</td>
										</tr>
										<tr>
											<td colspan='3'>&Tab;</td>
										</tr>
									</table>";
					$print_data .= "<table border='1'>
										<tr>
											<th>No.</th>
											<th>Nama Barang</th>
											<th>Jenis Barang</th>
											<th>Jumlah</th>
										</tr>";
					$no = 1;
					if (count($rows_detail) > 0) {
						foreach($rows_detail as $d) {
							$print_data .= "<tr>
												<td>" . $no++ . "</td>
												<td>" . $d->nama_barang . "</td>
												<td>" . $d->nama_jenis_barang . "</td>
												<td>" . $d->jumlah . " " . $d->satuan . "</td>
											</tr>";
							$detail_no = 'a';
							$rows_stok_keluar = $this->dbtable->get_result("
								SELECT smis_gd_stok_barang_keluar.id, smis_gd_stok_barang.tanggal_exp, smis_gd_stok_barang.hna, smis_gd_stok_barang_keluar.jumlah, smis_gd_stok_barang.satuan
								FROM smis_gd_stok_barang_keluar LEFT JOIN smis_gd_stok_barang ON smis_gd_stok_barang_keluar.id_stok_barang = smis_gd_stok_barang.id
								WHERE smis_gd_stok_barang_keluar.id_dbarang_keluar = '" . $d->id . "'
							");
							foreach($rows_stok_keluar as $dsk) {
								$print_data .= "<tr>
													<td></td>
													<td colspan='2'>" . $detail_no++ . ". ED. " . ArrayAdapter::format("date d/m/Y", $dsk->tanggal_exp) . " - " . ArrayAdapter::format("only-money Rp. ", $dsk->hna) . " : " . $dsk->jumlah . " " . $dsk->satuan . "</td>
													<td></td>
												</tr>";
							}
						}
					} else {
						$print_data .= "<tr>
											<td colspan='4' align='center'><i>Tidak terdapat data detail mutasi barang</i></td>
										</tr>";
					}
					$print_data .= "</table><br/>";
					global $user;
					$print_data .= "<table border='0' align='center'>
										<tr>
											<td align='center'>Tuban, " . ArrayAdapter::format("date d M Y", date("Y-m-d")) . "</td>
											<td>&Tab;</td>
											<td>&nbsp;</td>
										</tr>
										<tr>
											<td align='center'>PETUGAS GUDANG UMUM</td>
											<td>&Tab;</td>
											<td align='center'>PETUGAS " . ArrayAdapter::format("unslug", $row_header->unit) . "</td>
										</tr>
										<tr>
											<td>&nbsp;</td>
											<td>&Tab;</td>
											<td>&nbsp;</td>
										</tr>
										<tr>
											<td>&nbsp;</td>
											<td>&Tab;</td>
											<td>&nbsp;</td>
										</tr>
										<tr>
											<td>&nbsp;</td>
											<td>&Tab;</td>
											<td>&nbsp;</td>
										</tr>
										<tr>
											<td>&nbsp;</td>
											<td>&Tab;</td>
											<td>&nbsp;</td>
										</tr>
										<tr>
											<td align='center'><b>" . $user->getNameOnly() . "</b></td>
											<td>&Tab;</td>
											<td align='center'>(_____________________)</td>
										</tr>
									</table>";
					return $print_data;
				}
				public function save() {
					$header_data = $this->postToArray();
					$id['id'] = $_POST['id'];
					if ($id['id'] == 0 || $id['id'] == "") {
						//do insert header here:
						$result = $this->dbtable->insert($header_data);
						$id['id'] = $this->dbtable->get_inserted_id();
						$success['type'] = "insert";
						if (isset($_POST['detail'])) {
							//do insert detail here:
							$dbarang_keluar_dbtable = new DBTable($this->dbtable->get_db(), "smis_gd_dbarang_keluar");
							$detail = $_POST['detail'];
							foreach($detail as $d) {
								$detail_data = array();
								$detail_data['id_barang_keluar'] = $id['id'];
								$detail_data['id_barang'] = $d['id_barang'];
								$detail_data['nama_barang'] = $d['nama_barang'];
								$detail_data['nama_jenis_barang'] = $d['nama_jenis_barang'];
								$detail_data['jumlah'] = $d['jumlah'];
								$detail_data['satuan'] = $d['satuan'];
								$detail_data['konversi'] = $d['konversi'];
								$detail_data['satuan_konversi'] = $d['satuan_konversi'];
								$detail_data['autonomous'] = "[".$this->getAutonomous()."]";
						        $detail_data['duplicate'] = 0;
						        $detail_data['time_updated'] = date("Y-m-d H:i:s");
						        $detail_data['origin_updated'] = $this->getAutonomous();
						        $detail_data['origin'] = $this->getAutonomous();
								$dbarang_keluar_dbtable->insert($detail_data);
								$id_detail = $dbarang_keluar_dbtable->get_inserted_id();
								$stok_dbtable = new DBTable($this->dbtable->get_db(), "smis_gd_stok_barang");
								$stok_query = "
									SELECT smis_gd_stok_barang.id, smis_gd_stok_barang.sisa
									FROM smis_gd_stok_barang LEFT JOIN smis_gd_dbarang_masuk ON smis_gd_stok_barang.id_dbarang_masuk = smis_gd_dbarang_masuk.id
									WHERE smis_gd_dbarang_masuk.id_barang = '" . $d['id_barang'] . "' AND smis_gd_stok_barang.satuan = '" . $d['satuan'] . "' AND smis_gd_stok_barang.konversi = '" . $d['konversi'] . "' AND smis_gd_stok_barang.satuan_konversi = '" . $d['satuan_konversi'] . "'
									ORDER BY smis_gd_stok_barang.tanggal_exp ASC
								";
								$stok_rows = $stok_dbtable->get_result($stok_query);
								$jumlah = $detail_data['jumlah'];
								foreach($stok_rows as $sr) {
									$sisa_stok = 0;
									$jumlah_stok_keluar = 0;
									if ($sr->sisa >= $jumlah) {
										$sisa_stok = $sr->sisa - $jumlah;
										$jumlah_stok_keluar = $jumlah;
										$jumlah = 0;
									} else {
										$sisa_stok = 0;
										$jumlah_stok_keluar = $sr->sisa;
										$jumlah = $jumlah - $sr->sisa;
									}
									//do update stok barang here:
									$stok_data = array();
									$stok_data['sisa'] = $sisa_stok;
									$stok_data['autonomous'] = "[".$this->getAutonomous()."]";
							        $stok_data['duplicate'] = 0;
							        $stok_data['time_updated'] = date("Y-m-d H:i:s");
							        $stok_data['origin_updated'] = $this->getAutonomous();
									$stok_id['id'] = $sr->id;
									$stok_dbtable->update($stok_data, $stok_id);
									//logging riwayat stok barang:
									$riwayat_dbtable = new DBTable($this->dbtable->get_db(), "smis_gd_riwayat_stok_barang");
									$data_riwayat = array();
									$data_riwayat['tanggal'] = date("Y-m-d");
									$data_riwayat['id_stok_barang'] = $sr->id;
									$data_riwayat['jumlah_keluar'] = $jumlah_stok_keluar;
									$data_riwayat['sisa'] = $sisa_stok;
									$data_riwayat['keterangan'] = "Stok Keluar ke " . ArrayAdapter::format("unslug", $_POST['unit']);
									global $user;
									$data_riwayat['nama_user'] = $user->getName();
									$data_riwayat['autonomous'] = "[".$this->getAutonomous()."]";
							        $data_riwayat['duplicate'] = 0;
							        $data_riwayat['time_updated'] = date("Y-m-d H:i:s");
							        $data_riwayat['origin_updated'] = $this->getAutonomous();
							        $data_riwayat['origin'] = $this->getAutonomous();
									$riwayat_dbtable->insert($data_riwayat);
									//do insert stok barang keluar here:
									$stok_keluar_dbtable = new DBTable($this->dbtable->get_db(), "smis_gd_stok_barang_keluar");
									$stok_keluar_data = array();
									$stok_keluar_data['id_dbarang_keluar'] = $id_detail;
									$stok_keluar_data['id_stok_barang'] = $sr->id;
									$stok_keluar_data['jumlah'] = $jumlah_stok_keluar;
									$stok_keluar_data['autonomous'] = "[".$this->getAutonomous()."]";
							        $stok_keluar_data['duplicate'] = 0;
							        $stok_keluar_data['time_updated'] = date("Y-m-d H:i:s");
							        $stok_keluar_data['origin_updated'] = $this->getAutonomous();
							        $stok_keluar_data['origin'] = $this->getAutonomous();
									$stok_keluar_dbtable->insert($stok_keluar_data);
									if ($jumlah == 0) break;
								}
							}
						}
					} else {
						//do update header here:
						$header_data = array();
						$header_data['status'] = $_POST['status'];
						$header_data['keterangan'] = $_POST['keterangan'];
						$header_data['autonomous'] = "[".$this->getAutonomous()."]";
				        $header_data['duplicate'] = 0;
				        $header_data['time_updated'] = date("Y-m-d H:i:s");
				        $header_data['origin_updated'] = "[".$this->getAutonomous()."]";
						$result = $this->dbtable->update($header_data, $id);
						$success['type'] = "update";
						if (isset($_POST['detail'])) {
							//do update detail here:
							$dbarang_keluar_dbtable = new DBTable($this->dbtable->get_db(), "smis_gd_dbarang_keluar");
							$detail = $_POST['detail'];
							foreach($detail as $d) {
								if ($d['cmd'] == "insert") {
									$detail_data = array();
									$detail_data['id_barang_keluar'] = $id['id'];
									$detail_data['id_barang'] = $d['id_barang'];
									$detail_data['nama_barang'] = $d['nama_barang'];
									$detail_data['nama_jenis_barang'] = $d['nama_jenis_barang'];
									$detail_data['jumlah'] = $d['jumlah'];
									$detail_data['satuan'] = $d['satuan'];
									$detail_data['konversi'] = $d['konversi'];
									$detail_data['satuan_konversi'] = $d['satuan_konversi'];
									$detail_data['autonomous'] = "[".$this->getAutonomous()."]";
							        $detail_data['duplicate'] = 0;
							        $detail_data['time_updated'] = date("Y-m-d H:i:s");
							        $detail_data['origin_updated'] = $this->getAutonomous();
							        $detail_data['origin'] = $this->getAutonomous();
									$dbarang_keluar_dbtable->insert($detail_data);
									$id_detail = $dbarang_keluar_dbtable->get_inserted_id();
									$stok_dbtable = new DBTable($this->dbtable->get_db(), "smis_gd_stok_barang");
									$stok_query = "
										SELECT smis_gd_stok_barang.id, smis_gd_stok_barang.sisa
										FROM smis_gd_stok_barang LEFT JOIN smis_gd_dbarang_masuk ON smis_gd_stok_barang.id_dbarang_masuk = smis_gd_dbarang_masuk.id
										WHERE smis_gd_dbarang_masuk.id_barang = '" . $d['id_barang'] . "' AND smis_gd_stok_barang.satuan = '" . $d['satuan'] . "' AND smis_gd_stok_barang.konversi = '" . $d['konversi'] . "' AND smis_gd_stok_barang.satuan_konversi = '" . $d['satuan_konversi'] . "'
										ORDER BY smis_gd_stok_barang.tanggal_exp DESC
									";
									$stok_rows = $stok_dbtable->get_result($stok_query);
									$jumlah = $detail_data['jumlah'];
									foreach($stok_rows as $sr) {
										$sisa_stok = 0;
										$jumlah_stok_keluar = 0;
										if ($sr->sisa >= $jumlah) {
											$sisa_stok = $sr->sisa - $jumlah;
											$jumlah_stok_keluar = $jumlah;
											$jumlah = 0;
										} else {
											$sisa_stok = 0;
											$jumlah_stok_keluar = $sr->sisa;
											$jumlah = $jumlah - $sr->sisa;
										}
										//do update stok barang here:
										$stok_data = array();
										$stok_data['sisa'] = $sisa_stok;
										$stok_data['autonomous'] = "[".$this->getAutonomous()."]";
								        $stok_data['duplicate'] = 0;
								        $stok_data['time_updated'] = date("Y-m-d H:i:s");
								        $stok_data['origin_updated'] = $this->getAutonomous();
										$stok_id['id'] = $sr->id;
										$stok_dbtable->update($stok_data, $stok_id);
										//logging riwayat stok barang:
										$riwayat_dbtable = new DBTable($this->dbtable->get_db(), "smis_gd_riwayat_stok_barang");
										$data_riwayat = array();
										$data_riwayat['tanggal'] = date("Y-m-d");
										$data_riwayat['id_stok_barang'] = $sr->id;
										$data_riwayat['jumlah_keluar'] = $jumlah_stok_keluar;
										$data_riwayat['sisa'] = $sisa_stok;
										$data_riwayat['keterangan'] = "Perubahan Stok Keluar ke " . ArrayAdapter::format("unslug", $_POST['unit']);
										global $user;
										$data_riwayat['nama_user'] = $user->getName();
										$data_riwayat['autonomous'] = "[".$this->getAutonomous()."]";
								        $data_riwayat['duplicate'] = 0;
								        $data_riwayat['time_updated'] = date("Y-m-d H:i:s");
								        $data_riwayat['origin_updated'] = $this->getAutonomous();
								        $data_riwayat['origin'] = $this->getAutonomous();
										$riwayat_dbtable->insert($data_riwayat);
										//do insert stok barang keluar here:
										$stok_keluar_dbtable = new DBTable($this->dbtable->get_db(), "smis_gd_stok_barang_keluar");
										$stok_keluar_data = array();
										$stok_keluar_data['id_dbarang_keluar'] = $id_detail;
										$stok_keluar_data['id_stok_barang'] = $sr->id;
										$stok_keluar_data['jumlah'] = $jumlah_stok_keluar;
										$stok_keluar_data['autonomous'] = "[".$this->getAutonomous()."]";
								        $stok_keluar_data['duplicate'] = 0;
								        $stok_keluar_data['time_updated'] = date("Y-m-d H:i:s");
								        $stok_keluar_data['origin_updated'] = $this->getAutonomous();
								        $stok_keluar_data['origin'] = $this->getAutonomous();
										$stok_keluar_dbtable->insert($stok_keluar_data);
										if ($jumlah == 0) break;
									}
								} else if ($d['cmd'] == "update") {
									$detail_data = array();
									$detail_data['jumlah'] = $d['jumlah'];
									$detail_data['autonomous'] = "[".$this->getAutonomous()."]";
							        $detail_data['duplicate'] = 0;
							        $detail_data['time_updated'] = date("Y-m-d H:i:s");
							        $detail_data['origin_updated'] = $this->getAutonomous();
									$detail_id['id'] = $d['id'];
									$dbarang_keluar_dbtable->update($detail_data, $detail_id);
									if ($d['jumlah'] > $d['jumlah_lama']) {
										//jumlah baru > jumlah lama:
										$stok_dbtable = new DBTable($this->dbtable->get_db(), "smis_gd_stok_barang");
										$stok_query = "
											SELECT smis_gd_stok_barang.id, smis_gd_stok_barang.sisa
											FROM smis_gd_stok_barang LEFT JOIN smis_gd_dbarang_masuk ON smis_gd_stok_barang.id_dbarang_masuk = smis_gd_dbarang_masuk.id
											WHERE smis_gd_dbarang_masuk.id_barang = '" . $d['id_barang'] . "' AND smis_gd_stok_barang.satuan = '" . $d['satuan'] . "' AND smis_gd_stok_barang.konversi = '" . $d['konversi'] . "' AND smis_gd_stok_barang.satuan_konversi = '" . $d['satuan_konversi'] . "'
											ORDER BY smis_gd_stok_barang.tanggal_exp DESC
										";
										$stok_rows = $stok_dbtable->get_result($stok_query);
										$jumlah = $d['jumlah'] - $d['jumlah_lama'];
										foreach($stok_rows as $sr) {
											$sisa_stok = 0;
											$jumlah_stok_keluar = 0;
											if ($sr->sisa >= $jumlah) {
												$sisa_stok = $sr->sisa - $jumlah;
												$jumlah_stok_keluar = $jumlah;
												$jumlah = 0;
											} else {
												$sisa_stok = 0;
												$jumlah_stok_keluar = $sr->sisa;
												$jumlah = $jumlah - $sr->sisa;
											}
											//do update stok barang here:
											$stok_data = array();
											$stok_data['sisa'] = $sisa_stok;
											$stok_data['autonomous'] = "[".$this->getAutonomous()."]";
									        $stok_data['duplicate'] = 0;
									        $stok_data['time_updated'] = date("Y-m-d H:i:s");
									        $stok_data['origin_updated'] = $this->getAutonomous();
											$stok_id['id'] = $sr->id;
											$stok_dbtable->update($stok_data, $stok_id);
											//logging riwayat stok barang:
											$riwayat_dbtable = new DBTable($this->dbtable->get_db(), "smis_gd_riwayat_stok_barang");
											$data_riwayat = array();
											$data_riwayat['tanggal'] = date("Y-m-d");
											$data_riwayat['id_stok_barang'] = $sr->id;
											$data_riwayat['jumlah_keluar'] = $jumlah_stok_keluar;
											$data_riwayat['sisa'] = $sisa_stok;
											$data_riwayat['keterangan'] = "Perubahan Stok Keluar ke " . ArrayAdapter::format("unslug", $_POST['unit']);
											global $user;
											$data_riwayat['nama_user'] = $user->getName();
											$data_riwayat['autonomous'] = "[".$this->getAutonomous()."]";
									        $data_riwayat['duplicate'] = 0;
									        $data_riwayat['time_updated'] = date("Y-m-d H:i:s");
									        $data_riwayat['origin_updated'] = $this->getAutonomous();
									        $data_riwayat['origin'] = $this->getAutonomous();
											$riwayat_dbtable->insert($data_riwayat);
											//do insert stok barang keluar here:
											$stok_keluar_dbtable = new DBTable($this->dbtable->get_db(), "smis_gd_stok_barang_keluar");
											$stok_keluar_data = array();
											$stok_keluar_data['id_dbarang_keluar'] = $d['id'];
											$stok_keluar_data['id_stok_barang'] = $sr->id;
											$stok_keluar_data['jumlah'] = $jumlah_stok_keluar;
											$stok_keluar_data['autonomous'] = "[".$this->getAutonomous()."]";
									        $stok_keluar_data['duplicate'] = 0;
									        $stok_keluar_data['time_updated'] = date("Y-m-d H:i:s");
									        $stok_keluar_data['origin_updated'] = $this->getAutonomous();
									        $stok_keluar_data['origin'] = $this->getAutonomous();
											$stok_keluar_dbtable->insert($stok_keluar_data);
											if ($jumlah == 0) break;
										}
									} else if ($d['jumlah'] < $d['jumlah_lama']) {
										//jumlah baru < jumlah lama:
										$stok_barang_keluar_dbtable = new DBTable($this->dbtable->get_db(), "smis_gd_stok_barang_keluar");
										$stok_keluar_query = "
											SELECT *
											FROM smis_gd_stok_barang_keluar
											WHERE id_dbarang_keluar = '" . $d['id'] . "'
											ORDER BY id DESC
										";
										$stok_keluar = $stok_barang_keluar_dbtable->get_result($stok_keluar_query);
										$jumlah = $d['jumlah_lama'] - $d['jumlah'];
										foreach($stok_keluar as $sk) {
											$simpan_stok_keluar = 0;
											$simpan_stok = 0;
											if ($jumlah >= $sk->jumlah) {
												$jumlah = $jumlah - $sk->jumlah;
												$simpan_stok = $sk->jumlah;
												$simpan_stok_keluar = 0;
											} else {
												$simpan_stok_keluar = $sk->jumlah - $jumlah;
												$simpan_stok = $jumlah;
												$jumlah = 0;
											}
											$stok_keluar_data = array();
											$stok_keluar_data['jumlah'] = $simpan_stok_keluar;
											if ($simpan_stok_keluar == 0)
												$stok_keluar_data['prop'] = "del";
											$stok_keluar_id['id'] = $sk->id;
											$stok_barang_keluar_dbtable->update($stok_keluar_data, $stok_keluar_id);
											$stok_barang_dbtable = new DBTable($this->dbtable->get_db(), "smis_gd_stok_barang");
											$stok_query = "
												SELECT *
												FROM smis_gd_stok_barang
												WHERE id = '" . $sk->id_stok_barang . "'
											";
											$stok_barang_row = $stok_barang_dbtable->get_row($stok_query);
											$stok_data = array();
											$stok_data['sisa'] = $stok_barang_row->sisa + $simpan_stok;
											$stok_data['autonomous'] = "[".$this->getAutonomous()."]";
									        $stok_data['duplicate'] = 0;
									        $stok_data['time_updated'] = date("Y-m-d H:i:s");
									        $stok_data['origin_updated'] = $this->getAutonomous();
											$stok_id['id'] = $sk->id_stok_barang;
											$stok_dbtable = new DBTable($this->dbtable->get_db(), "smis_gd_stok_barang");
											$stok_dbtable->update($stok_data, $stok_id);
											//logging riwayat stok barang:
											$riwayat_dbtable = new DBTable($this->dbtable->get_db(), "smis_gd_riwayat_stok_barang");
											$data_riwayat = array();
											$data_riwayat['tanggal'] = date("Y-m-d");
											$data_riwayat['id_stok_barang'] = $sk->id_stok_barang;
											$data_riwayat['jumlah_masuk'] = $simpan_stok;
											$data_riwayat['sisa'] = $stok_barang_row->sisa + $simpan_stok;
											$data_riwayat['keterangan'] = "Perubahan Stok Keluar ke " . ArrayAdapter::format("unslug", $_POST['unit']);
											global $user;
											$data_riwayat['nama_user'] = $user->getName();
											$data_riwayat['autonomous'] = "[".$this->getAutonomous()."]";
									        $data_riwayat['duplicate'] = 0;
									        $data_riwayat['time_updated'] = date("Y-m-d H:i:s");
									        $data_riwayat['origin_updated'] = $this->getAutonomous();
									        $data_riwayat['origin'] = $this->getAutonomous();
											$riwayat_dbtable->insert($data_riwayat);
											if ($jumlah == 0) break;
										}
									} //end of update
								} else if($d['cmd'] == "delete") {
									$detail_data = array();
									$detail_data['prop'] = "del";
									$detail_id['id'] = $d['id'];
									$dbarang_keluar_dbtable->update($detail_data, $detail_id);
									$stok_barang_keluar_dbtable = new DBTable($this->dbtable->get_db(), "smis_gd_stok_barang_keluar");
									$stok_keluar_query = "
										SELECT *
										FROM smis_gd_stok_barang_keluar
										WHERE id_dbarang_keluar = '" . $d['id'] . "'
										ORDER BY id DESC
									";
									$stok_keluar = $stok_barang_keluar_dbtable->get_result($stok_keluar_query);
									foreach($stok_keluar as $sk) {
										$stok_keluar_data = array();
										$stok_keluar_data['jumlah'] = 0;
										$stok_keluar_data['prop'] = "del";
										$stok_keluar_data['autonomous'] = "[".$this->getAutonomous()."]";
								        $stok_keluar_data['duplicate'] = 0;
								        $stok_keluar_data['time_updated'] = date("Y-m-d H:i:s");
								        $stok_keluar_data['origin_updated'] = $this->getAutonomous();
										$stok_keluar_id['id'] = $sk->id;
										$stok_barang_keluar_dbtable->update($stok_keluar_data, $stok_keluar_id);
										$stok_barang_dbtable = new DBTable($this->dbtable->get_db(), "smis_gd_stok_barang");
										$stok_query = "
											SELECT *
											FROM smis_gd_stok_barang
											WHERE id = '" . $sk->id_stok_barang . "'
										";
										$stok_barang_row = $stok_barang_dbtable->get_row($stok_query);
										$stok_data = array();
										$stok_data['sisa'] = $stok_barang_row->sisa + $sk->jumlah;
										$stok_data['autonomous'] = "[".$this->getAutonomous()."]";
								        $stok_data['duplicate'] = 0;
								        $stok_data['time_updated'] = date("Y-m-d H:i:s");
								        $stok_data['origin_updated'] = $this->getAutonomous();
										$stok_id['id'] = $sk->id_stok_barang;
										$stok_dbtable = new DBTable($this->dbtable->get_db(), "smis_gd_stok_barang");
										$stok_dbtable->update($stok_data, $stok_id);
										//logging riwayat stok barang:
										$riwayat_dbtable = new DBTable($this->dbtable->get_db(), "smis_gd_riwayat_stok_barang");
										$data_riwayat = array();
										$data_riwayat['tanggal'] = date("Y-m-d");
										$data_riwayat['id_stok_barang'] = $sk->id_stok_barang;
										$data_riwayat['jumlah_masuk'] = $sk->jumlah;
										$data_riwayat['sisa'] = $stok_barang_row->sisa + $sk->jumlah;
										$data_riwayat['keterangan'] = "Perubahan Stok Keluar ke " . ArrayAdapter::format("unslug", $_POST['unit']);
										global $user;
										$data_riwayat['nama_user'] = $user->getName();
										$data_riwayat['autonomous'] = "[".$this->getAutonomous()."]";
								        $data_riwayat['duplicate'] = 0;
								        $data_riwayat['time_updated'] = date("Y-m-d H:i:s");
								        $data_riwayat['origin_updated'] = $this->getAutonomous();
								        $data_riwayat['origin'] = $this->getAutonomous();
										$riwayat_dbtable->insert($data_riwayat);
									}
								}
							}
						}
					}
					$success['id'] = $id['id'];
					$success['success'] = 1;
					if ($result === false) $success['success'] = 0;
					return $success;
				}
				public function edit() {
					$id = $_POST['id'];
					$header_row = $this->dbtable->get_row("
						SELECT *
						FROM smis_gd_barang_keluar
						WHERE id = '" . $id . "'
					");
					$data['header'] = $header_row;
					$dbarang_keluar_dbtable = new DBTable($this->dbtable->get_db(), "smis_gd_dbarang_keluar");
					$data['detail'] = $dbarang_keluar_dbtable->get_result("
						SELECT *
						FROM smis_gd_dbarang_keluar
						WHERE id_barang_keluar = '" . $id . "' AND prop NOT LIKE 'del'
					");
					return $data;
				}
				public function delete() {
					$id['id'] = $_POST['id'];
					if ($this->dbtable->isRealDelete()) {
						$result = $this->dbtable->delete(null,$id);
					} else {
						$data['prop'] = "del";
						$data['autonomous'] = "[".$this->getAutonomous()."]";
				        $data['duplicate'] = 0;
				        $data['time_updated'] = date("Y-m-d H:i:s");
				        $data['origin_updated'] = $this->getAutonomous();
						$result = $this->dbtable->update($data, $id);
					}
					$stok_barang_keluar_dbtable = new DBTable($this->dbtable->get_db(), "smis_gd_stok_barang_keluar");
					$stok_barang_dbtable = new DBTable($this->dbtable->get_db(), "smis_gd_stok_barang");
					$riwayat_dbtable = new DBTable($this->dbtable->get_db(), "smis_gd_riwayat_stok_barang");
					$stok_keluar_query = "
						SELECT smis_gd_stok_barang_keluar.*, smis_gd_barang_keluar.unit
						FROM (smis_gd_stok_barang_keluar LEFT JOIN smis_gd_dbarang_keluar ON smis_gd_stok_barang_keluar.id_dbarang_keluar = smis_gd_dbarang_keluar.id) LEFT JOIN smis_gd_barang_keluar ON smis_gd_dbarang_keluar.id_barang_keluar = smis_gd_barang_keluar.id
						WHERE smis_gd_dbarang_keluar.id_barang_keluar = '" . $id['id'] . "' AND smis_gd_stok_barang_keluar.prop NOT LIKE 'del'
						ORDER BY smis_gd_stok_barang_keluar.id DESC
					";
					$stok_keluar = $this->dbtable->get_result($stok_keluar_query);
					foreach($stok_keluar as $sk) {
						$stok_keluar_data = array();
						$stok_keluar_data['jumlah'] = 0;
						$stok_keluar_data['prop'] = "del";
						$stok_keluar_data['autonomous'] = "[".$this->getAutonomous()."]";
				        $stok_keluar_data['duplicate'] = 0;
				        $stok_keluar_data['time_updated'] = date("Y-m-d H:i:s");
				        $stok_keluar_data['origin_updated'] = $this->getAutonomous();
						$stok_keluar_id['id'] = $sk->id;
						$stok_barang_keluar_dbtable->update($stok_keluar_data, $stok_keluar_id);
						$stok_query = "
							SELECT *
							FROM smis_gd_stok_barang
							WHERE id = '" . $sk->id_stok_barang . "'
						";
						$stok_barang_row = $this->dbtable->get_row($stok_query);
						$stok_data = array();
						$stok_data['sisa'] = $stok_barang_row->sisa + $sk->jumlah;
						$stok_data['autonomous'] = "[".$this->getAutonomous()."]";
				        $stok_data['duplicate'] = 0;
				        $stok_data['time_updated'] = date("Y-m-d H:i:s");
				        $stok_data['origin_updated'] = $this->getAutonomous();
						$stok_id['id'] = $sk->id_stok_barang;
						$stok_barang_dbtable->update($stok_data, $stok_id);
						//logging riwayat stok barang:
						$data_riwayat = array();
						$data_riwayat['tanggal'] = date("Y-m-d");
						$data_riwayat['id_stok_barang'] = $sk->id_stok_barang;
						$data_riwayat['jumlah_masuk'] = $sk->jumlah;
						$data_riwayat['sisa'] = $stok_barang_row->sisa + $sk->jumlah;
						$data_riwayat['keterangan'] = "Pembatalan Stok Keluar ke " . ArrayAdapter::format("unslug", $sk->unit);
						global $user;
						$data_riwayat['nama_user'] = $user->getName();
						$data_riwayat['autonomous'] = "[".$this->getAutonomous()."]";
				        $data_riwayat['duplicate'] = 0;
				        $data_riwayat['time_updated'] = date("Y-m-d H:i:s");
				        $data_riwayat['origin_updated'] = $this->getAutonomous();
				        $data_riwayat['origin'] = $this->getAutonomous();
						$riwayat_dbtable->insert($data_riwayat);
					}
					$success['success'] = 1;
					$success['id'] = $_POST['id'];
					if ($result === 'false') $success['success'] = 0;
					return $success;
				}
			}
			$barang_keluar_dbresponder = new BarangKeluarDBResponder(
				$barang_keluar_dbtable,
				$barang_keluar_table,
				$barang_keluar_adapter
			);
			$data = $barang_keluar_dbresponder->command($_POST['command']);
			//push_barang_service:
			if (isset($_POST['push_command'])) {
				class PushBarangServiceConsumer extends ServiceConsumer {
					public function __construct($db, $id_barang_keluar, $tanggal, $unit, $status, $keterangan, $detail, $command) {
						$array['id_barang_keluar'] = $id_barang_keluar;
						$array['tanggal'] = $tanggal;
						$array['unit'] = "gudang_umum";
						$array['status'] = $status;
						$array['keterangan'] = $keterangan;
						$array['detail'] = json_encode($detail);
						$array['command'] = $command;
						parent::__construct($db, "push_barang", $array, $unit);
					}
					public function proceedResult() {
						$content = array();
						$result = json_decode($this->result,true);
						foreach ($result as $autonomous) {
							foreach ($autonomous as $entity) {
								if ($entity != null || $entity != "")
									return $entity;
							}
						}
						return $content;
					}
				}
				$barang_keluar_dbtable = new DBTable($db, "smis_gd_barang_keluar");
				$barang_keluar_row = $barang_keluar_dbtable->get_row("
					SELECT *
					FROM smis_gd_barang_keluar
					WHERE id = '" . $data['content']['id'] . "'
				");
				$stok_barang_keluar_dbtable = new DBTable($db, "smis_gd_stok_barang_keluar");
				$detail = $stok_barang_keluar_dbtable->get_result("
					SELECT smis_gd_stok_barang_keluar.id AS 'id_stok_barang_keluar', smis_gd_dbarang_keluar.id_barang, smis_gd_dbarang_keluar.nama_barang, smis_gd_dbarang_keluar.nama_jenis_barang, smis_gd_stok_barang.id_vendor, smis_gd_stok_barang.nama_vendor, smis_gd_stok_barang_keluar.jumlah, smis_gd_dbarang_keluar.satuan, smis_gd_dbarang_keluar.konversi, smis_gd_dbarang_keluar.satuan_konversi, smis_gd_stok_barang.hna, smis_gd_stok_barang.produsen, smis_gd_stok_barang.tanggal_exp, smis_gd_stok_barang_keluar.prop
					FROM (smis_gd_stok_barang_keluar LEFT JOIN smis_gd_stok_barang ON smis_gd_stok_barang_keluar.id_stok_barang = smis_gd_stok_barang.id) LEFT JOIN smis_gd_dbarang_keluar ON smis_gd_stok_barang_keluar.id_dbarang_keluar = smis_gd_dbarang_keluar.id
					WHERE smis_gd_dbarang_keluar.id_barang_keluar = '" . $data['content']['id'] . "'
				");
				$command = "push_" . $_POST['push_command'];
				$push_barang_service_consumer = new PushBarangServiceConsumer($db, $barang_keluar_row->id, $barang_keluar_row->tanggal, $barang_keluar_row->unit, $barang_keluar_row->status, $barang_keluar_row->keterangan, $detail, $command);
				$push_barang_service_consumer->execute();
			}
			echo json_encode($data);
			return;
		}
		return;
	}

	//get barang chooser:
	$barang_table = new Table(
		array("Barang", "Jenis", "Stok"),
		"",
		null,
		true
	);
	$barang_table->setName("barang");
	$barang_table->setModel(Table::$SELECT);
	$barang_adapter = new SimpleAdapter();
	$barang_adapter->add("Barang", "nama_barang");
	$barang_adapter->add("Jenis", "nama_jenis_barang");
	$barang_adapter->add("Stok", "stok");
	$barang_dbtable = new DBTable($db, "smis_gd_stok_barang");
	$barang_dbtable->setViewForSelect(true);
	$filter = "";
	if (isset($_POST['kriteria'])) {
		$filter = " AND (smis_gd_stok_barang.nama_barang LIKE '%" . $_POST['kriteria'] . "%' OR smis_gd_stok_barang.nama_jenis_barang LIKE '%" . $_POST['kriteria'] . "%')";
	}
	$query_value = "
		SELECT *
		FROM (
			SELECT id_barang AS 'id', nama_barang, nama_jenis_barang, GROUP_CONCAT(CONCAT(sisa, ' ', satuan) ORDER BY satuan, sisa ASC SEPARATOR ', ') AS 'stok'
			FROM (
				SELECT smis_gd_dbarang_masuk.id_barang, smis_gd_stok_barang.nama_barang, smis_gd_stok_barang.nama_jenis_barang, SUM(smis_gd_stok_barang.sisa) AS 'sisa', smis_gd_stok_barang.satuan, smis_gd_stok_barang.konversi, smis_gd_stok_barang.satuan_konversi
				FROM smis_gd_stok_barang LEFT JOIN smis_gd_dbarang_masuk ON smis_gd_stok_barang.id_dbarang_masuk = smis_gd_dbarang_masuk.id
				WHERE smis_gd_stok_barang.prop NOT LIKE 'del' AND smis_gd_stok_barang.konversi = '1' " . $filter . "
				GROUP BY smis_gd_dbarang_masuk.id_barang, smis_gd_stok_barang.satuan, smis_gd_stok_barang.konversi, smis_gd_stok_barang.satuan_konversi
			) v_barang
			GROUP BY id_barang
		) v_stok
	";
	$query_count = "
		SELECT COUNT(*)
		FROM (
			SELECT id_barang AS 'id', nama_barang, nama_jenis_barang, GROUP_CONCAT(CONCAT(sisa, ' ', satuan) ORDER BY satuan, sisa ASC SEPARATOR ', ') AS 'stok'
			FROM (
				SELECT smis_gd_dbarang_masuk.id_barang, smis_gd_stok_barang.nama_barang, smis_gd_stok_barang.nama_jenis_barang, SUM(smis_gd_stok_barang.sisa) AS 'sisa', smis_gd_stok_barang.satuan, smis_gd_stok_barang.konversi, smis_gd_stok_barang.satuan_konversi
				FROM smis_gd_stok_barang LEFT JOIN smis_gd_dbarang_masuk ON smis_gd_stok_barang.id_dbarang_masuk = smis_gd_dbarang_masuk.id
				WHERE smis_gd_stok_barang.prop NOT LIKE 'del' AND smis_gd_stok_barang.konversi = '1' " . $filter . "
				GROUP BY smis_gd_dbarang_masuk.id_barang, smis_gd_stok_barang.satuan, smis_gd_stok_barang.konversi, smis_gd_stok_barang.satuan_konversi
			) v_barang
			GROUP BY id_barang
		) v_stok
	";
	$barang_dbtable->setPreferredQuery(true, $query_value, $query_count);
	class BarangDBResponder extends DBResponder {
		public function edit() {
			$id = $_POST['id'];
			$data['header'] = $this->dbtable->get_row("
				SELECT DISTINCT smis_gd_dbarang_masuk.id_barang, smis_gd_stok_barang.nama_barang, smis_gd_stok_barang.nama_jenis_barang
				FROM smis_gd_stok_barang LEFT JOIN smis_gd_dbarang_masuk ON smis_gd_stok_barang.id_dbarang_masuk = smis_gd_dbarang_masuk.id
				WHERE smis_gd_stok_barang.prop NOT LIKE 'del' AND smis_gd_dbarang_masuk.id_barang = '" . $id . "'
			");
			$satuan_rows = $this->dbtable->get_result("
				SELECT DISTINCT smis_gd_stok_barang.satuan, smis_gd_stok_barang.konversi, smis_gd_stok_barang.satuan_konversi
				FROM smis_gd_stok_barang LEFT JOIN smis_gd_dbarang_masuk ON smis_gd_stok_barang.id_dbarang_masuk = smis_gd_dbarang_masuk.id
				WHERE smis_gd_dbarang_masuk.id_barang = '" . $id . "' AND smis_gd_stok_barang.prop NOT LIKE 'del'
			");
			$satuan_option = "";
			foreach($satuan_rows as $sr) {
				if ($sr->konversi == 1) {
					$satuan_option .= "
						<option value='" . $sr->konversi . "_" . $sr->satuan_konversi . "'>" . $sr->satuan . "</option>
					";
				}
			}
			$data['satuan_option'] = $satuan_option;
			return $data;
		}
	}
	$barang_dbresponder = new BarangDBResponder(
		$barang_dbtable,
		$barang_table,
		$barang_adapter
	);
	
	//get sisa by id_barang, satuan, konversi, dan satuan_konversi:
	$sisa_table = new Table(
		array("id_barang", "sisa", "satuan", "konversi", "satuan_konversi"),
		"",
		null,
		true
	);
	$sisa_table->setName("sisa");
	$sisa_adapter = new SimpleAdapter();
	$sisa_adapter->add("id_barang", "id_barang");
	$sisa_adapter->add("sisa", "sisa");
	$sisa_adapter->add("satuan", "satuan");
	$sisa_adapter->add("konversi", "konversi");
	$sisa_adapter->add("satuan_konversi", "satuan_konversi");
	$columns = array("id_barang", "sisa", "satuan", "konversi", "satuan_konversi");
	$sisa_dbtable = new DBTable(
		$db,
		"smis_gd_stok_barang",
		$columns
	);
	class SisaDBResponder extends DBResponder {
		public function edit() {
			$id_barang = $_POST['id_barang'];
			$satuan = $_POST['satuan'];
			$konversi = $_POST['konversi'];
			$satuan_konversi = $_POST['satuan_konversi'];
			$data = $this->dbtable->get_row("
				SELECT id_barang, SUM(sisa) AS 'sisa', satuan, konversi, satuan_konversi
				FROM (
					SELECT smis_gd_dbarang_masuk.id_barang, smis_gd_stok_barang.sisa, smis_gd_stok_barang.satuan, smis_gd_stok_barang.konversi, smis_gd_stok_barang.satuan_konversi	
					FROM smis_gd_stok_barang LEFT JOIN smis_gd_dbarang_masuk ON smis_gd_stok_barang.id_dbarang_masuk = smis_gd_dbarang_masuk.id
					WHERE smis_gd_stok_barang.prop NOT LIKE 'del' AND smis_gd_dbarang_masuk.id_barang = '" . $id_barang . "' AND smis_gd_stok_barang.satuan = '" . $satuan . "' AND smis_gd_stok_barang.konversi = '" . $konversi . "' AND smis_gd_stok_barang.satuan_konversi = '" . $satuan_konversi . "'
				) v_stok_barang
				GROUP BY id_barang, satuan, konversi, satuan_konversi
			");
			return $data;
		}
	}
	$sisa_dbresponder = new SisaDBResponder(
		$sisa_dbtable,
		$sisa_table,
		$sisa_adapter
	);
	
	$super_command = new SuperCommand();
	$super_command->addResponder("barang", $barang_dbresponder);
	$super_command->addResponder("sisa", $sisa_dbresponder);
	$init = $super_command->initialize();
	if ($init != null) {
		echo $init;
		return;
	}
	
	$barang_keluar_modal = new Modal("barang_keluar_add_form", "smis_form_container", "barang_keluar");
	$barang_keluar_modal->setTitle("Barang Keluar");
	$barang_keluar_modal->setClass(Modal::$FULL_MODEL);
	$id_hidden = new Hidden("barang_keluar_id", "barang_keluar_id", "");
	$barang_keluar_modal->addElement("", $id_hidden);
	$tanggal_text = new Text("barang_keluar_tanggal", "barang_keluar_tanggal", "");
	$tanggal_text->setClass("mydate");
	$tanggal_text->setAtribute("data-date-format='yyyy-m-d'");
	$barang_keluar_modal->addElement("Tanggal", $tanggal_text);
	class UnitServiceConsumer extends ServiceConsumer {
		public function __construct($db) {
			parent::__construct($db, "is_inventory");
		}		
		public function proceedResult() {
			$content = array();
			$result = json_decode($this->result, true);
			foreach($result as $autonomous) {
				foreach($autonomous as $entity=>$res) {
					if($res['value_barang']=="1"){
						$option = array();
						$option['value'] = $entity;
						$option['name'] = ArrayAdapter::format("unslug", $res['name']);
						$content[] = $option;
					}
				}
			}
			return $content;
		}
	}
	$unit_service_consumer = new UnitServiceConsumer($db);
	$unit_service_consumer->execute();
	$unit_option = $unit_service_consumer->getContent();
	$unit_select = new Select("barang_keluar_unit", "barang_keluar_unit", $unit_option);
	$barang_keluar_modal->addElement("Unit", $unit_select);
	class DBarangKeluarTable extends Table {
		public function getHeaderButton() {
			$btn_group = new ButtonGroup("noprint");
			$btn = new Button("", "", "Tambah");
			$btn->setAction($this->action . ".show_add_form()");
			$btn->setClass("btn-primary");
			$btn->setAtribute("id='dbarang_keluar_add'");
			$btn->setIcon("icon-plus icon-white");
			$btn->setIsButton(Button::$ICONIC);
			$btn_group->addElement($btn);
			return $btn_group->getHtml();
		}
	}
	$dbarang_keluar_table = new DBarangKeluarTable(
		array("Nama Barang", "Jenis Barang", "Jumlah", "Keterangan"),
		"",
		null,
		true
	);
	$dbarang_keluar_table->setName("dbarang_keluar");
	$dbarang_keluar_table->setFooterVisible(false);
	$barang_keluar_modal->addBody("dbarang_keluar_table", $dbarang_keluar_table);
	$save_button = new Button("", "", "Simpan");
	$save_button->setClass("btn-success");
	$save_button->setAtribute("id='barang_keluar_save'");
	$save_button->setIcon("fa fa-floppy-o");
	$save_button->setIsButton(Button::$ICONIC);
	$barang_keluar_modal->addFooter($save_button);
	$ok_button = new Button("", "", "OK");
	$ok_button->setClass("btn-success");
	$ok_button->setAtribute("id='barang_keluar_ok'");
	$ok_button->setAction("$($(this).data('target')).smodal('hide')");
	$barang_keluar_modal->addFooter($ok_button);
	
	$dbarang_keluar_modal = new Modal("dbarang_keluar_add_form", "smis_form_container", "dbarang_keluar");
	$dbarang_keluar_modal->setTitle("Detail Barang Keluar");
	$id_hidden = new Hidden("dbarang_keluar_id", "dbarang_keluar_id", "");
	$dbarang_keluar_modal->addElement("", $id_hidden);
	$id_barang_hidden = new Hidden("dbarang_keluar_id_barang", "dbarang_keluar_id_barang", "");
	$dbarang_keluar_modal->addElement("", $id_barang_hidden);
	$nama_barang_button = new Button("", "", "Pilih");
	$nama_barang_button->setClass("btn-info");
	$nama_barang_button->setAction("barang.chooser('barang', 'barang_button', 'barang', barang)");
	$nama_barang_button->setIcon("icon-white icon-list-alt");
	$nama_barang_button->setIsButton(Button::$ICONIC);
	$nama_barang_button->setAtribute("id='barang_browse'");
	$nama_barang_text = new Text("dbarang_keluar_nama_barang", "dbarang_keluar_nama_barang", "");
	$nama_barang_text->setClass("smis-one-option-input");
	$nama_barang_text->setAtribute("disabled='disabled'");
	$nama_barang_input_group = new InputGroup("");
	$nama_barang_input_group->addComponent($nama_barang_text);
	$nama_barang_input_group->addComponent($nama_barang_button);
	$dbarang_keluar_modal->addElement("Nama Barang", $nama_barang_input_group);
	$nama_jenis_barang_text = new Text("dbarang_keluar_nama_jenis_barang", "dbarang_keluar_nama_jenis_barang", "");
	$nama_jenis_barang_text->setAtribute("disabled='disabled'");
	$dbarang_keluar_modal->addElement("Jenis Barang", $nama_jenis_barang_text);
	$satuan_select = new Select("dbarang_keluar_satuan", "dbarang_keluar_satuan", "");
	$dbarang_keluar_modal->addElement("Satuan", $satuan_select);
	$konversi_hidden = new Hidden("dbarang_keluar_konversi", "dbarang_keluar_konversi", "");
	$konversi_hidden->setAtribute("disabled='disabled'");
	$dbarang_keluar_modal->addElement("", $konversi_hidden);
	$satuan_konversi_hidden = new Hidden("dbarang_keluar_satuan_konversi", "dbarang_keluar_satuan_konversi", "");
	$dbarang_keluar_modal->addElement("", $satuan_konversi_hidden);
	$stok_hidden = new Hidden("dbarang_keluar_stok", "dbarang_keluar_stok", "");
	$dbarang_keluar_modal->addElement("", $stok_hidden);
	$f_stok_text = new Text("dbarang_keluar_f_stok", "dbarang_keluar_f_stok", "");
	$f_stok_text->setAtribute("disabled='disabled'");
	$dbarang_keluar_modal->addElement("Stok", $f_stok_text);
	$jumlah_lama_hidden = new Hidden("dbarang_keluar_jumlah_lama", "dbarang_keluar_jumlah_lama", "");
	$dbarang_keluar_modal->addElement("", $jumlah_lama_hidden);
	$jumlah_text = new Text("dbarang_keluar_jumlah", "dbarang_keluar_jumlah", "");
	$dbarang_keluar_modal->addElement("Jumlah", $jumlah_text);
	$dbarang_keluar_button = new Button("", "", "Simpan");
	$dbarang_keluar_button->setClass("btn-success");
	$dbarang_keluar_button->setAtribute("id='dbarang_keluar_save'");
	$dbarang_keluar_button->setIcon("fa fa-floppy-o");
	$dbarang_keluar_button->setIsButton(Button::$ICONIC);
	$dbarang_keluar_modal->addFooter($dbarang_keluar_button);
	
	echo $dbarang_keluar_modal->getHtml();
	echo $barang_keluar_modal->getHtml();
	echo $barang_keluar_table->getHtml();
	echo addJS("framework/smis/js/table_action.js");
	echo addJS("framework/bootstrap/js/bootstrap-datepicker.js");
	echo addCSS("framework/bootstrap/css/datepicker.css");	
?>
<script type="text/javascript">
	function BarangKeluarAction(name, page, action, column) {
		this.initialize(name, page, action, column);
	}
	BarangKeluarAction.prototype.constructor = BarangKeluarAction;
	BarangKeluarAction.prototype = new TableAction();
	BarangKeluarAction.prototype.show_add_form = function() {
		row_id = 0;
		var today = new Date();
		$("#barang_keluar_id").val("");
		$("#barang_keluar_tanggal").val(today.getFullYear() + "-" + (Number(today.getMonth())+1) + "-" + today.getDate());
		$("#barang_keluar_tanggal").removeAttr("disabled");
		$("#barang_keluar_unit").val("");
		$("#barang_keluar_unit").removeAttr("disabled");
		$("#dbarang_keluar_list").children().remove();
		$("#dbarang_keluar_add").show();
		$("#modal_alert_barang_keluar_add_form").html("");
		$(".error_field").removeClass("error_field");
		$("#barang_keluar_save").removeAttr("onclick");
		$("#barang_keluar_save").attr("onclick", "barang_keluar.save()");
		$("#barang_keluar_save").show();
		$("#barang_keluar_ok").hide();
		$("#barang_keluar_add_form").smodal("show");
	};
	BarangKeluarAction.prototype.validate = function() {
		var valid = true;
		var invalid_msg = "";
		var tanggal = $("#barang_keluar_tanggal").val();
		var unit = $("#barang_keluar_unit").find(":selected").text();
		var nord = $("#dbarang_keluar_list").children().length;
		$(".error_field").removeClass("error_field");
		if (tanggal == "") {
			valid = false;
			invalid_msg += "<br/><strong>Tanggal</strong> tidak boleh kosong";
			$("#barang_keluar_tanggal").addClass("error_field");
		}
		if (unit == "") {
			valid = false;
			invalid_msg += "<br/><strong>Unit</strong> tidak boleh kosong";
			$("#barang_keluar_unit").addClass("error_field");
		}
		if (nord == 0) {
			valid = false;
			invalid_msg += "<br/><strong>Detail</strong> tidak boleh kosong";
		}
		if (!valid) {
			$("#modal_alert_barang_keluar_add_form").html(
				"<div class='alert alert-block alert-danger'>" +
					"<h4>Peringatan</h4>" +
					invalid_msg +
				"</div>"
			);
		}
		return valid;
	};
	BarangKeluarAction.prototype.save = function() {
		if (!this.validate()) {
			return;
		}
		$("#barang_keluar_add_form").smodal("hide");
		showLoading();
		var self = this;
		var data = this.getRegulerData();
		data['super_command'] = "barang_keluar";
		data['command'] = "save";
		data['push_command'] = "save";
		data['id'] = "";
		data['tanggal'] = $("#barang_keluar_tanggal").val();
		data['unit'] = $("#barang_keluar_unit").val();
		data['status'] = "belum";
		data['keterangan'] = "";
		var detail = {};
		var nor = $("tbody#dbarang_keluar_list").children("tr").length;
		for(var i = 0; i < nor; i++) {
			var d_data = {};
			var prefix = $("tbody#dbarang_keluar_list").children('tr').eq(i).prop("id");
			d_data['id'] = $("#" + prefix + "_id").text();
			d_data['id_barang'] = $("#" + prefix + "_id_barang").text();
			d_data['nama_barang'] = $("#" + prefix + "_nama_barang").text();
			d_data['nama_jenis_barang'] = $("#" + prefix + "_nama_jenis_barang").text();
			d_data['jumlah'] = $("#" + prefix + "_jumlah").text();
			d_data['satuan'] = $("#" + prefix + "_satuan").text();
			d_data['konversi'] = $("#" + prefix + "_konversi").text();
			d_data['satuan_konversi'] = $("#" + prefix + "_satuan_konversi").text();
			detail[i] = d_data;
		}
		data['detail'] = detail;
		$.post(
			"",
			data,
			function(response) {
				var json = getContent(response);
				if (json == null) { 
					$("#barang_keluar_add_form").smodal("show");
				} else {
					self.view();
				}
				dismissLoading();
			}
		);
	};
	BarangKeluarAction.prototype.edit = function(id) {
		var self = this;
		var data = this.getRegulerData();
		data['super_command'] = "barang_keluar";
		data['command'] = "edit";
		data['id'] = id;
		$.post(	
			"",
			data,
			function (response) {
				var json = getContent(response);
				if (json == null) return;
				$("#barang_keluar_id").val(id);
				$("#barang_keluar_tanggal").val(json.header.tanggal);
				$("#barang_keluar_tanggal").removeAttr("disabled");
				$("#barang_keluar_unit").val(json.header.unit);
				$("#barang_keluar_unit").removeAttr("disabled");
				if (json.header.status == "belum") {
					$("#barang_keluar_tanggal").attr("disabled", "disabled");
					$("#barang_keluar_unit").attr("disabled", "disabled");
				}
				$("#dbarang_keluar_add").show();
				row_id = 0;
				$("#dbarang_keluar_list").children("tr").remove();
				for(var i = 0; i < json.detail.length; i++) {
					var dbarang_keluar_id = json.detail[i].id;
					var dbarang_keluar_id_barang_keluar = json.detail[i].id_barang_keluar;
					var dbarang_keluar_id_barang = json.detail[i].id_barang;
					var dbarang_keluar_nama_barang = json.detail[i].nama_barang;
					var dbarang_keluar_nama_jenis_barang = json.detail[i].nama_jenis_barang;
					var dbarang_keluar_jumlah = json.detail[i].jumlah;
					var dbarang_keluar_satuan = json.detail[i].satuan;
					var dbarang_keluar_konversi = json.detail[i].konversi;
					var dbarang_keluar_satuan_konversi = json.detail[i].satuan_konversi;
					$("tbody#dbarang_keluar_list").append(
						"<tr id='data_" + row_id + "'>" +
							"<td style='display: none;' id='data_" + row_id + "_id'>" + dbarang_keluar_id + "</td>" +
							"<td style='display: none;' id='data_" + row_id + "_id_barang'>" + dbarang_keluar_id_barang + "</td>" +
							"<td style='display: none;' id='data_" + row_id + "_jumlah_lama'>" + dbarang_keluar_jumlah + "</td>" +
							"<td style='display: none;' id='data_" + row_id + "_jumlah'>" + dbarang_keluar_jumlah + "</td>" +
							"<td style='display: none;' id='data_" + row_id + "_satuan'>" + dbarang_keluar_satuan + "</td>" +
							"<td style='display: none;' id='data_" + row_id + "_konversi'>" + dbarang_keluar_konversi + "</td>" +
							"<td style='display: none;' id='data_" + row_id + "_satuan_konversi'>" + dbarang_keluar_satuan_konversi + "</td>" +
							"<td id='data_" + row_id + "_nama_barang'>" + dbarang_keluar_nama_barang + "</td>" +
							"<td id='data_" + row_id + "_nama_jenis_barang'>" + dbarang_keluar_nama_jenis_barang + "</td>" +
							"<td id='data_" + row_id + "_f_jumlah'>" + dbarang_keluar_jumlah + " " + dbarang_keluar_satuan + "</td>" +
							"<td id='data_" + row_id + "_keterangan'>1 " + dbarang_keluar_satuan + " = " + dbarang_keluar_konversi + " " + dbarang_keluar_satuan_konversi + "</td>" +
							"<td>" + 
								"<div class='btn-group noprint'>" +
									"<a href='#' onclick='dbarang_keluar.edit_jumlah(" + row_id + ")' data-content='Ubah' data-toggle='popover' class='input btn btn-warning'>" + 
										"<i class='icon-edit icon-white'></i>" +
									"</a>" +
									"<a href='#' onclick='dbarang_keluar.delete(" + row_id + ")' data-content='Hapus' data-toggle='popover' class='input btn btn-danger'>" + 
										"<i class='icon-remove icon-white'></i>" + 
									"</a>" +
								 "</div>" +
							"</td>" +
						"</tr>"
					);
					row_id++;
				}
				$("#modal_alert_barang_keluar_add_form").html("");
				$(".error_field").removeClass("error_field");
				$("#barang_keluar_save").removeAttr("onclick");
				$("#barang_keluar_save").attr("onclick", "barang_keluar.update(" + id + ")");
				$("#barang_keluar_save").show();
				$("#barang_keluar_ok").hide();
				$("#barang_keluar_add_form").smodal("show");
			}
		);
	};
	BarangKeluarAction.prototype.update = function(id) {
		if (!this.validate()) {
			return;
		}
		$("#barang_keluar_add_form").smodal("hide");
		showLoading();
		var self = this;
		var data = this.getRegulerData();
		data['super_command'] = "barang_keluar";
		data['command'] = "save";
		data['push_command'] = "save";
		data['id'] = id;
		data['tanggal'] = $("#barang_keluar_tanggal").val();
		data['unit'] = $("#barang_keluar_unit").val();
		data['status'] = "belum";
		data['keterangan'] = "";
		var detail = {};
		var nor = $("tbody#dbarang_keluar_list").children("tr").length;
		for(var i = 0; i < nor; i++) {
			var d_data = {};
			var prefix = $("tbody#dbarang_keluar_list").children('tr').eq(i).prop("id");
			d_data['id'] = $("#" + prefix + "_id").text();
			d_data['id_barang'] = $("#" + prefix + "_id_barang").text();
			d_data['nama_barang'] = $("#" + prefix + "_nama_barang").text();
			d_data['nama_jenis_barang'] = $("#" + prefix + "_nama_jenis_barang").text();
			d_data['jumlah'] = $("#" + prefix + "_jumlah").text();
			d_data['jumlah_lama'] = $("#" + prefix + "_jumlah_lama").text();
			d_data['satuan'] = $("#" + prefix + "_satuan").text();
			d_data['konversi'] = $("#" + prefix + "_konversi").text();
			d_data['satuan_konversi'] = $("#" + prefix + "_satuan_konversi").text();
			if ($("#" + prefix).attr("class") == "deleted") {
				d_data['cmd'] = "delete";
			} else if ($("#" + prefix + "_id").text() == "") {
				d_data['cmd'] = "insert";
			} else {
				d_data['cmd'] = "update";
			}
			detail[i] = d_data;
		}
		data['detail'] = detail;
		$.post(
			"",
			data,
			function(response) {
				var json = getContent(response);
				if (json == null) { 
					$("#barang_keluar_add_form").smodal("show");
				} else {
					self.view();
				}
				dismissLoading();
			}
		);
	};
	BarangKeluarAction.prototype.detail = function(id) {
		var self = this;
		var data = this.getRegulerData();
		data['super_command'] = "barang_keluar";
		data['command'] = "edit";
		data['id'] = id;
		$.post(	
			"",
			data,
			function (response) {
				var json = getContent(response);
				if (json == null) return;
				$("#barang_keluar_id").val(id);
				$("#barang_keluar_tanggal").val(json.header.tanggal);
				$("#barang_keluar_tanggal").removeAttr("disabled");
				$("#barang_keluar_tanggal").attr("disabled", "disabled");
				$("#barang_keluar_unit").val(json.header.unit);
				$("#barang_keluar_unit").removeAttr("disabled");
				$("#barang_keluar_unit").attr("disabled", "disabled");
				$("#dbarang_keluar_add").hide();
				row_id = 0;
				$("#dbarang_keluar_list").children("tr").remove();
				for(var i = 0; i < json.detail.length; i++) {
					var dbarang_keluar_id = json.detail[i].id;
					var dbarang_keluar_id_barang_keluar = json.detail[i].id_barang_keluar;
					var dbarang_keluar_id_barang = json.detail[i].id_barang;
					var dbarang_keluar_nama_barang = json.detail[i].nama_barang;
					var dbarang_keluar_nama_jenis_barang = json.detail[i].nama_jenis_barang;
					var dbarang_keluar_jumlah = json.detail[i].jumlah;
					var dbarang_keluar_satuan = json.detail[i].satuan;
					var dbarang_keluar_konversi = json.detail[i].konversi;
					var dbarang_keluar_satuan_konversi = json.detail[i].satuan_konversi;
					$("tbody#dbarang_keluar_list").append(
						"<tr id='data_" + row_id + "'>" +
							"<td style='display: none;' id='data_" + row_id + "_id'>" + dbarang_keluar_id + "</td>" +
							"<td style='display: none;' id='data_" + row_id + "_id_barang'>" + dbarang_keluar_id_barang + "</td>" +
							"<td style='display: none;' id='data_" + row_id + "_jumlah_lama'>" + dbarang_keluar_jumlah + "</td>" +
							"<td style='display: none;' id='data_" + row_id + "_jumlah'>" + dbarang_keluar_jumlah + "</td>" +
							"<td style='display: none;' id='data_" + row_id + "_satuan'>" + dbarang_keluar_satuan + "</td>" +
							"<td style='display: none;' id='data_" + row_id + "_konversi'>" + dbarang_keluar_konversi + "</td>" +
							"<td style='display: none;' id='data_" + row_id + "_satuan_konversi'>" + dbarang_keluar_satuan_konversi + "</td>" +
							"<td id='data_" + row_id + "_nama_barang'>" + dbarang_keluar_nama_barang + "</td>" +
							"<td id='data_" + row_id + "_nama_jenis_barang'>" + dbarang_keluar_nama_jenis_barang + "</td>" +
							"<td id='data_" + row_id + "_f_jumlah'>" + dbarang_keluar_jumlah + " " + dbarang_keluar_satuan + "</td>" +
							"<td id='data_" + row_id + "_keterangan'>1 " + dbarang_keluar_satuan + " = " + dbarang_keluar_konversi + " " + dbarang_keluar_satuan_konversi + "</td>" +
							"<td></td>" +
						"</tr>"
					);
					row_id++;
				}
				$("#modal_alert_barang_keluar_add_form").html("");
				$(".error_field").removeClass("error_field");
				$("#barang_keluar_save").removeAttr("onclick");
				$("#barang_keluar_save").hide();
				$("#barang_keluar_ok").show();
				$("#barang_keluar_add_form").smodal("show");
			}
		);
	};
	BarangKeluarAction.prototype.del = function(id) {
		var self = this;
		var data = this.getRegulerData();
		data['super_command'] = "barang_keluar";
		data['command'] = "del";
		data['id'] = id;
		data['push_command'] = "delete";
		bootbox.confirm(
			"Anda yakin menghapus data ini?", 
			function(result) {
				if(result) {
					showLoading();
					$.post(
						'',
						data,
						function(response) {
							var json = getContent(response);
							if (json == null) return;
							self.view();
							dismissLoading();
						}
					);
				}
			}
		);
	};
	BarangKeluarAction.prototype.print_mutasi = function(id, status) {
		var self = this;
		var data = this.getRegulerData();
		data['super_command'] = "barang_keluar";
		data['command'] = "print_mutasi";
		data['id'] = id;
		data['status'] = status;
		$.post(
			"",
			data,
			function(response) {
				var json = getContent(response);
				if (json == null) return;
				smis_print(json);
			}
		);
	};
	
	function DBarangKeluarAction(name, page, action, column) {
		this.initialize(name, page, action, column);
	}
	DBarangKeluarAction.prototype.constructor = DBarangKeluarAction;
	DBarangKeluarAction.prototype = new TableAction();
	DBarangKeluarAction.prototype.show_add_form = function() {
		$("#dbarang_keluar_id").val("");
		$("#dbarang_keluar_id_barang").val("");
		$("#dbarang_keluar_nama_jenis_barang").val("");
		$("#dbarang_keluar_nama_barang").val("");
		$("#barang_browse").removeAttr("onclick");
		$("#barang_browse").attr("onclick", "barang.chooser('barang', 'barang_button', 'barang', barang)");
		$("#barang_browse").removeClass("btn-info");
		$("#barang_browse").removeClass("btn-inverse");
		$("#barang_browse").addClass("btn-info");
		$("#dbarang_keluar_satuan").html("");
		$("#dbarang_keluar_satuan").removeAttr("onchange");
		$("#dbarang_keluar_satuan").attr("onchange", "barang.setDetailInfo()");
		$("#dbarang_keluar_satuan").removeAttr("disabled");
		$("#dbarang_keluar_satuan").val("");
		$("#dbarang_keluar_konversi").val("");
		$("#dbarang_keluar_satuan_konversi").val("");
		$("#dbarang_keluar_stok").val("");
		$("#dbarang_keluar_f_stok").val("");
		$("#dbarang_keluar_jumlah_lama").val(0);
		$("#dbarang_keluar_jumlah").val("");
		$("#modal_alert_dbarang_keluar_add_form").html("");
		$(".error_field").removeClass("error_field");
		$("#dbarang_keluar_save").removeAttr("onclick");
		$("#dbarang_keluar_save").attr("onclick", "dbarang_keluar.save()");
		$("#dbarang_keluar_add_form").smodal("show");
	};
	DBarangKeluarAction.prototype.validate = function() {
		var valid = true;
		var invalid_msg = "";
		var nama_barang = $("#dbarang_keluar_nama_barang").val();
		var satuan = $("#dbarang_keluar_satuan").val();
		var stok = $("#dbarang_keluar_stok").val();
		var jumlah = $("#dbarang_keluar_jumlah").val();
		$(".error_field").removeClass("error_field");
		if (nama_barang == "") {
			valid = false;
			invalid_msg += "</br><strong>Nama Barang</strong> tidak boleh kosong";
			$("#dbarang_keluar_nama_barang").addClass("error_field");
		}
		if (satuan == "") {
			valid = false;
			invalid_msg += "</br><strong>Satuan</strong> tidak boleh kosong";
			$("#dbarang_keluar_satuan").addClass("error_field");
		}
		if (jumlah == "") {
			valid = false;
			invalid_msg += "</br><strong>Jumlah</strong> tidak boleh kosong";
			$("#dbarang_keluar_jumlah").addClass("error_field");
		} else if (!is_numeric(jumlah)) {
			valid = false;
			invalid_msg += "</br><strong>Jumlah</strong> seharusnya numerik (0-9)";
			$("#dbarang_keluar_jumlah").addClass("error_field");
		} else if (stok != "" && is_numeric(stok) && parseFloat(jumlah) > parseFloat(stok)) {
			valid = false;
			invalid_msg += "</br><strong>Jumlah</strong> tidak boleh melebihi stok";
			$("#dbarang_keluar_jumlah").addClass("error_field");
		}
		if (!valid) {
			$("#modal_alert_dbarang_keluar_add_form").html(
				"<div class='alert alert-block alert-danger'>" +
					"<h4>Peringatan</h4>" +
					invalid_msg +
				"</div>"
			);
		}
		return valid;
	};
	DBarangKeluarAction.prototype.save = function() {
		if (!this.validate()) {
			return;
		}
		var dbarang_keluar_id = $("#dbarang_keluar_id").val();
		var dbarang_keluar_id_barang = $("#dbarang_keluar_id_barang").val();
		var dbarang_keluar_nama_jenis_barang = $("#dbarang_keluar_nama_jenis_barang").val();
		var dbarang_keluar_nama_barang = $("#dbarang_keluar_nama_barang").val();
		var dbarang_keluar_jumlah_lama = $("#dbarang_keluar_jumlah_lama").val();
		var dbarang_keluar_jumlah = $("#dbarang_keluar_jumlah").val();
		var dbarang_keluar_satuan = $("#dbarang_keluar_satuan").find(":selected").text();
		var dbarang_keluar_konversi = $("#dbarang_keluar_konversi").val();
		var dbarang_keluar_satuan_konversi = $("#dbarang_keluar_satuan_konversi").val();
		$("tbody#dbarang_keluar_list").append(
			"<tr id='data_" + row_id + "'>" +
				"<td style='display: none;' id='data_" + row_id + "_id'>" + dbarang_keluar_id + "</td>" +
				"<td style='display: none;' id='data_" + row_id + "_id_barang'>" + dbarang_keluar_id_barang + "</td>" +
				"<td style='display: none;' id='data_" + row_id + "_jumlah_lama'>" + dbarang_keluar_jumlah_lama + "</td>" +
				"<td style='display: none;' id='data_" + row_id + "_jumlah'>" + dbarang_keluar_jumlah + "</td>" +
				"<td style='display: none;' id='data_" + row_id + "_satuan'>" + dbarang_keluar_satuan + "</td>" +
				"<td style='display: none;' id='data_" + row_id + "_konversi'>" + dbarang_keluar_konversi + "</td>" +
				"<td style='display: none;' id='data_" + row_id + "_satuan_konversi'>" + dbarang_keluar_satuan_konversi + "</td>" +
				"<td id='data_" + row_id + "_nama_barang'>" + dbarang_keluar_nama_barang + "</td>" +
				"<td id='data_" + row_id + "_nama_jenis_barang'>" + dbarang_keluar_nama_jenis_barang + "</td>" +
				"<td id='data_" + row_id + "_f_jumlah'>" + dbarang_keluar_jumlah + " " + dbarang_keluar_satuan + "</td>" +
				"<td id='data_" + row_id + "_keterangan'>1 " + dbarang_keluar_satuan + " = " + dbarang_keluar_konversi + " " + dbarang_keluar_satuan_konversi + "</td>" +
				"<td>" + 
					"<div class='btn-group noprint'>" +
						"<a href='#' onclick='dbarang_keluar.edit(" + row_id + ")' data-content='Ubah' data-toggle='popover' class='input btn btn-warning'>" + 
							"<i class='icon-edit icon-white'></i>" +
						"</a>" +
						"<a href='#' onclick='dbarang_keluar.delete(" + row_id + ")' data-content='Hapus' data-toggle='popover' class='input btn btn-danger'>" + 
							"<i class='icon-remove icon-white'></i>" + 
						"</a>" +
					 "</div>" +
				"</td>" +
			"</tr>"
		);
		row_id++;
		$("#dbarang_keluar_add_form").smodal("hide");
	};
	DBarangKeluarAction.prototype.edit = function(r_num) {
		var dbarang_keluar_id = $("#data_" + r_num + "_id").text();
		var dbarang_keluar_id_barang = $("#data_" + r_num + "_id_barang").text();
		var dbarang_keluar_nama_jenis_barang = $("#data_" + r_num + "_nama_jenis_barang").text();
		var dbarang_keluar_nama_barang = $("#data_" + r_num + "_nama_barang").text();
		var dbarang_keluar_jumlah = $("#data_" + r_num + "_jumlah").text();
		var dbarang_keluar_satuan = $("#data_" + r_num + "_konversi").text() + "_" + $("#data_" + r_num + "_satuan_konversi").text();
		$("#dbarang_keluar_id").val(dbarang_keluar_id);
		$("#dbarang_keluar_jumlah").val(dbarang_keluar_jumlah);
		$("#dbarang_keluar_satuan").html("");
		var self = this;
		var data = this.getRegulerData();
		data['super_command'] = "barang";
		data['command'] = "edit";
		data['id'] = dbarang_keluar_id_barang;
		$.post(
			"",
			data,
			function(response) {
				var json = getContent(response);
				if (json == null) return;
				$("#dbarang_keluar_id_barang").val(json.header.id_barang);
				$("#dbarang_keluar_nama_barang").val(json.header.nama_barang);
				$("#dbarang_keluar_nama_jenis_barang").val(json.header.nama_jenis_barang);
				$("#barang_browse").removeAttr("onclick");
				$("#barang_browse").attr("onclick", "barang.chooser('barang', 'barang_button', 'barang', barang)");
				$("#barang_browse").removeClass("btn-info");
				$("#barang_browse").removeClass("btn-inverse");
				$("#barang_browse").addClass("btn-info");
				$("#dbarang_keluar_satuan").html(json.satuan_option);
				$("#dbarang_keluar_satuan").val(dbarang_keluar_satuan);
				var part = $("#dbarang_keluar_satuan").val().split("_");
				$("#dbarang_keluar_konversi").val(part[0]);
				$("#dbarang_keluar_satuan_konversi").val(part[1]);
				data = self.getRegulerData();
				data['super_command'] = "sisa";
				data['command'] = "edit";
				data['id_barang'] = $("#dbarang_keluar_id_barang").val();
				data['satuan'] = $("#dbarang_keluar_satuan").find(":selected").text();
				data['konversi'] = $("#dbarang_keluar_konversi").val();
				data['satuan_konversi'] = $("#dbarang_keluar_satuan_konversi").val();
				$.post(
					"",
					data,
					function(response) {
						var json = getContent(response);
						if (json == null) return;
						$("#dbarang_keluar_stok").val(json.sisa);
						$("#dbarang_keluar_f_stok").val(json.sisa + " " + json.satuan);
						$("#dbarang_keluar_satuan").removeAttr("disabled");
						$("#modal_alert_dbarang_keluar_add_form").html("");
						$(".error_field").removeClass("error_field");
						$("#dbarang_keluar_save").removeAttr("onclick");
						$("#dbarang_keluar_save").attr("onclick", "dbarang_keluar.update(" + r_num + ")");
						$("#dbarang_keluar_add_form").smodal("show");
					}
				);
			}
		);
	};
	DBarangKeluarAction.prototype.edit_jumlah = function(r_num) {
		var dbarang_keluar_id = $("#data_" + r_num + "_id").text();
		var dbarang_keluar_id_barang = $("#data_" + r_num + "_id_barang").text();
		var dbarang_keluar_nama_jenis_barang = $("#data_" + r_num + "_nama_jenis_barang").text();
		var dbarang_keluar_nama_barang = $("#data_" + r_num + "_nama_barang").text();
		var dbarang_keluar_jumlah = $("#data_" + r_num + "_jumlah").text();
		var dbarang_keluar_satuan = $("#data_" + r_num + "_konversi").text() + "_" + $("#data_" + r_num + "_satuan_konversi").text();
		$("#dbarang_keluar_id").val(dbarang_keluar_id);
		$("#dbarang_keluar_jumlah").val(dbarang_keluar_jumlah);
		$("#dbarang_keluar_satuan").html("");
		var self = this;
		var data = this.getRegulerData();
		data['super_command'] = "barang";
		data['command'] = "edit";
		data['id'] = dbarang_keluar_id_barang;
		$.post(
			"",
			data,
			function(response) {
				var json = getContent(response);
				if (json == null) return;
				$("#dbarang_keluar_id_barang").val(json.header.id_barang);
				$("#dbarang_keluar_nama_barang").val(json.header.nama_barang);
				$("#dbarang_keluar_nama_jenis_barang").val(json.header.nama_jenis_barang);
				$("#barang_browse").removeAttr("onclick");
				$("#barang_browse").removeClass("btn-info");
				$("#barang_browse").removeClass("btn-inverse");
				$("#barang_browse").addClass("btn-inverse");
				$("#dbarang_keluar_satuan").html(json.satuan_option);
				$("#dbarang_keluar_satuan").val(dbarang_keluar_satuan);
				var part = $("#dbarang_keluar_satuan").val().split("_");
				$("#dbarang_keluar_konversi").val(part[0]);
				$("#dbarang_keluar_satuan_konversi").val(part[1]);
				data = self.getRegulerData();
				data['super_command'] = "sisa";
				data['command'] = "edit";
				data['id_barang'] = $("#dbarang_keluar_id_barang").val();
				data['satuan'] = $("#dbarang_keluar_satuan").find(":selected").text();
				data['konversi'] = $("#dbarang_keluar_konversi").val();
				data['satuan_konversi'] = $("#dbarang_keluar_satuan_konversi").val();
				$.post(
					"",
					data,
					function(response) {
						var json = getContent(response);
						if (json == null) return;
						$("#dbarang_keluar_stok").val(json.sisa);
						$("#dbarang_keluar_f_stok").val(json.sisa + " " + json.satuan);
						$("#dbarang_keluar_satuan").removeAttr("disabled");
						$("#dbarang_keluar_satuan").attr("disabled", "disabled");
						$("#modal_alert_dbarang_keluar_add_form").html("");	
						$(".error_field").removeClass("error_field");					
						$("#dbarang_keluar_save").removeAttr("onclick");
						$("#dbarang_keluar_save").attr("onclick", "dbarang_keluar.update(" + r_num + ")");
						$("#dbarang_keluar_add_form").smodal("show");
					}
				);
			}
		);
	};
	DBarangKeluarAction.prototype.update = function(r_num) {
		if (!this.validate()) {
			return;
		}
		var dbarang_keluar_id_barang = $("#dbarang_keluar_id_barang").val();
		var dbarang_keluar_nama_jenis_barang = $("#dbarang_keluar_nama_jenis_barang").val();
		var dbarang_keluar_nama_barang = $("#dbarang_keluar_nama_barang").val();
		var dbarang_keluar_jumlah = $("#dbarang_keluar_jumlah").val();
		var dbarang_keluar_satuan = $("#dbarang_keluar_satuan").find(":selected").text();
		var dbarang_keluar_konversi = $("#dbarang_keluar_konversi").val();
		var dbarang_keluar_satuan_konversi = $("#dbarang_keluar_satuan_konversi").val();
		$("#data_" + r_num + "_id_barang").text(dbarang_keluar_id_barang);
		$("#data_" + r_num + "_nama_barang").text(dbarang_keluar_nama_barang);
		$("#data_" + r_num + "_nama_jenis_barang").text(dbarang_keluar_nama_jenis_barang);
		$("#data_" + r_num + "_jumlah").text(dbarang_keluar_jumlah);
		$("#data_" + r_num + "_satuan").text(dbarang_keluar_satuan);
		$("#data_" + r_num + "_konversi").text(dbarang_keluar_konversi);
		$("#data_" + r_num + "_satuan_konversi").text(dbarang_keluar_satuan_konversi);
		$("#data_" + r_num + "_f_jumlah").text(dbarang_keluar_jumlah + " " + dbarang_keluar_satuan);
		$("#data_" + r_num + "_keterangan").text("1 " + dbarang_keluar_satuan + " = " + dbarang_keluar_konversi + " " + dbarang_keluar_satuan_konversi);
		$("#dbarang_keluar_add_form").smodal("hide");
	};
	DBarangKeluarAction.prototype.delete = function(r_num) {
		var id = $("#data_" + r_num + "_id").text();
		if (id.length == 0) {
			$("#data_" + r_num).remove();
		} else {
			$("#data_" + r_num).attr("style", "display: none;");
			$("#data_" + r_num).attr("class", "deleted");
		}
	};
	
	function BarangAction(name, page, action, column) {
		this.initialize(name, page, action, column);
	}
	BarangAction.prototype.constructor = BarangAction;
	BarangAction.prototype = new TableAction();
	BarangAction.prototype.selected = function(json) {
		$("#dbarang_keluar_id_barang").val(json.header.id_barang);
		$("#dbarang_keluar_nama_barang").val(json.header.nama_barang);
		$("#dbarang_keluar_nama_jenis_barang").val(json.header.nama_jenis_barang);
		$("#dbarang_keluar_satuan").html(json.satuan_option);
		this.setDetailInfo();
	};
	BarangAction.prototype.setDetailInfo = function() {
		var part = $("#dbarang_keluar_satuan").val().split("_");
		$("#dbarang_keluar_konversi").val(part[0]);
		$("#dbarang_keluar_satuan_konversi").val(part[1]);
		var data = this.getRegulerData();
		data['super_command'] = "sisa";
		data['command'] = "edit";
		data['id_barang'] = $("#dbarang_keluar_id_barang").val();
		data['satuan'] = $("#dbarang_keluar_satuan").find(":selected").text();
		data['konversi'] = $("#dbarang_keluar_konversi").val();
		data['satuan_konversi'] = $("#dbarang_keluar_satuan_konversi").val();
		$.post(
			"",
			data,
			function(response) {
				var json = getContent(response);
				if (json == null) return;
				$("#dbarang_keluar_stok").val(json.sisa);
				$("#dbarang_keluar_f_stok").val(json.sisa + " " + json.satuan);
			}
		);
	};
	
	var barang_keluar;
	var dbarang_keluar;
	var barang;
	var row_id;
	$(document).ready(function() {
		$('[data-toggle="popover"]').popover({
			trigger: 'hover',
			'placement': 'top'
		});
		$(".mydate").datepicker();
		barang = new BarangAction(
			"barang",
			"gudang_umum",
			"barang_keluar",
			new Array()
		);
		barang.setSuperCommand("barang");
		var dbarang_keluar_columns = new Array("id", "id_barang_keluar", "id_barang", "nama_barang", "nama_jenis_barang", "jumlah", "satuan", "konversi", "satuan_konversi");
		dbarang_keluar = new DBarangKeluarAction(
			"dbarang_keluar",
			"gudang_umum",
			"barang_keluar",
			dbarang_keluar_columns
		);
		var barang_keluar_columns = new Array("id", "tanggal", "unit", "status", "keterangan");
		barang_keluar = new BarangKeluarAction(
			"barang_keluar",
			"gudang_umum",
			"barang_keluar",
			barang_keluar_columns
		);
		barang_keluar.setSuperCommand("barang_keluar");
		barang_keluar.view();
	});
</script>