<?php 	
	$penyimpanan_barang_tabulator = new Tabulator("penyimpanan_barang", "", Tabulator::$POTRAIT);
	$penyimpanan_barang_tabulator->add("stok_barang_bt", "Stok Barang Masuk", "gudang_umum/daftar_stok_barang_belum_tersimpan.php", Tabulator::$TYPE_INCLUDE);
	$penyimpanan_barang_tabulator->add("stok_barang_t", "Stok Barang Tersimpan", "gudang_umum/daftar_stok_barang_tersimpan.php", Tabulator::$TYPE_INCLUDE);
	$penyimpanan_barang_tabulator->add("stok_barang_b", "Stok Barang Dapat Dibongkar", "gudang_umum/daftar_stok_barang_dapat_dibongkar.php", Tabulator::$TYPE_INCLUDE);
	
	echo $penyimpanan_barang_tabulator->getHtml();
?>