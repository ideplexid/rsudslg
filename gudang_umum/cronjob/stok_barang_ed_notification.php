<?php 
	global $notification;
	global $db;
	
	$stok_barang_dbtable = new DBTable($db, "smis_gd_stok_barang");
	$stok_barang_dbtable->addCustomKriteria(" DATEDIFF(tanggal_exp, CURDATE()) ", " <= 30 ");
	$stok_barang_dbtable->setOrder(" tanggal_exp ");
	$stok_barang_dbtable->setMaximum(3);
	$stok_barang_pack = $stok_barang_dbtable->view("", 0);
	$stok_barang_data = $stok_barang_pack['data'];
	$message = "";
	$i = 0;
	foreach($stok_barang_data as $stok) {
		$message .= $stok->nama_barang;
		if ($i > 0)
			$message .= ",";
		$message .= " ";
		$i++;
	}
	$message .= "mendekati ED dalam waktu <= 30 hari.";
	$type = "Stok Barang ED";
	$code = "gudang_umum-barang-ed-" . date("Y-m-d") . $message;
	if ($stok_barang_data != null && count($stok_barang_data) > 0)
		$notification->addNotification($type, md5($code), $message, "gudang_umum", "pengecekan_stok_ed");
?>