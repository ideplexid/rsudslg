<?php 
	require_once("smis-base/smis-include-service-consumer.php");
	require_once("smis-base/smis-include-duplicate.php");
	global $db;
	
	class BarangTTable extends Table {
		public function getBodyContent(){
			$content = "";
			if ($this->content!=NULL) {
				foreach ($this->content as $d) {
					$content .= "<tr>";
					foreach ($this->header as $h) {
						$content .= "<td>" . $d[$h] . "</td>";
					}
					if ($this->is_action) {
						$content .= "<td>" . $this->getFilteredContentButton($d['id'], $d['unit'], $d['kode'])->getHtml() . "</td>";
					}
					$content .= "</tr>";
				}
			}
			return $content;
		}
		public function getFilteredContentButton($id, $unit, $kode) {
			$btn_group = new ButtonGroup("noprint");
			if ($unit == "") {
				$btn = new Button("", "", "Ubah");
				$btn->setAction($this->action . ".edit('" . $id . "')");
				$btn->setClass("btn-warning");
				$btn->setAtribute("data-content='Ubah' data-toggle='popover'");
				$btn->setIcon("icon-edit icon-white");
				$btn->setIsButton(Button::$ICONIC);
				$btn_group->addElement($btn);
				$btn = new Button("", "", "Kirim");
				$btn->setAction($this->action . ".prepare_send('" . $id . "')");
				$btn->setClass("btn-success");
				$btn->setAtribute("data-content='Kirim' data-toggle='popover'");
				$btn->setIcon("fa fa-share");
				$btn->setIsButton(Button::$ICONIC);
				$btn_group->addElement($btn);
			} else {
				$btn = new Button("", "", "Ubah");
				$btn->setAction($this->action . ".edit_kondisi('" . $id . "')");
				$btn->setClass("btn-warning");
				$btn->setAtribute("data-content='Ubah' data-toggle='popover'");
				$btn->setIcon("icon-edit icon-white");
				$btn->setIsButton(Button::$ICONIC);
				$btn_group->addElement($btn);
			}
			$btn = new Button("", "", "Riwayat Inventaris");
			$btn->setAction($this->action . ".view_history('" . $id . "')");
			$btn->setAtribute("data-content='Lihat Riwayat' data-toggle='popover'");
			$btn->setIcon("fa fa-history");
			$btn->setIsButton(Button::$ICONIC);
			$btn_group->addElement($btn);
			$btn = new Button("", "", "Cetak Kode");
			$btn->setAction($this->action . ".print_kode('" . $kode . "')");
			$btn->setClass("btn-inverse");
			$btn->setAtribute("data-content='Ubah' data-toggle='popover'");
			$btn->setIcon("icon-print icon-white");
			$btn->setIsButton(Button::$ICONIC);
			$btn_group->addElement($btn);
			return $btn_group;
		}
	}
	$barang_table = new BarangTTable(
		array("Nomor", "Kode", "Jenis", "Barang", "Merk", "Harga", "Tahun Perolehan", "Penyusutan", "Kondisi", "Lokasi"),
		"",
		null,
		true
	);
	$barang_table->setName("barang_t");
	$barang_table->setReloadButtonEnable(false);
	$barang_table->setPrintButtonEnable(false);
	$barang_table->setAddButtonEnable(false);
		
	//detail history modal: 
	$history_modal = new Modal("history_add_form", "smis_form_container", "history");
	$history_modal->setTitle("Riwayat Inventaris");
	$history_modal->setClass(Modal::$FULL_MODEL);
	$id_hidden = new Hidden("history_id_inventaris", "history_id_inventaris", "");
	$history_modal->addElement("", $id_hidden);
	$nama_barang_text = new Text("history_nama_barang", "history_nama_barang", "");
	$nama_barang_text->setAtribute("disabled='disabled'");
	$history_modal->addElement("Nama Barang", $nama_barang_text);
	$hna_text = new Text("history_hna", "history_hna", "");
	$hna_text->setAtribute("disabled", "disabled");
	$history_modal->addElement("H. Perolehan", $hna_text);
	$history_button = new Button("", "", "Cetak");
	$history_button->setClass("btn-inverse");
	$history_button->setIsButton(Button::$ICONIC);
	$history_button->setIcon("icon-print icon-white");
	$history_button->setAction("detail_history.print()");
	$history_modal->addElement("", $history_button);
	$detail_history_table = new Table(
		array("No.", "Tanggal", "Merk", "Kode", "Mutasi / Retur", "Unit", "Petugas Entri"),
		"",
		null,
		true
	);
	$detail_history_table->setName("detail_history");
	$detail_history_table->setAction(false);
	$history_modal->addBody("detail_history_table", $detail_history_table);
	$history_button = new Button("", "", "OK");
	$history_button->setAtribute("id='history_ok_btn'");
	$history_button->setClass("btn-success");
	$history_button->setAction("$($(this).data('target')).smodal('hide')");
	$history_modal->addFooter($history_button);
	
	if (isset($_POST['super_command']) && $_POST['super_command'] == "detail_history") {
		if (isset($_POST['command'])) {
			$detail_history_adapter = new SimpleAdapter(true, "No.");
			$detail_history_adapter->add("Tanggal", "tanggal", "date d M Y");
			$detail_history_adapter->add("Kode", "kode");
			$detail_history_adapter->add("Merk", "merk");
			$detail_history_adapter->add("Mutasi / Retur", "status", "unslug");
			$detail_history_adapter->add("Unit", "unit", "unslug");
			$detail_history_adapter->add("Petugas Entri", "nama_user");
			$filter = "";
			if (isset($_POST['kriteria'])) {
				$filter = " AND (kode LIKE '%" . $_POST['kriteria'] . "%' OR merk LIKE '%" . $_POST['kriteria'] . "%' OR nama_user LIKE '%" . $_POST['kriteria'] . "%')";
			}
			$detail_history_dbtable = new DBTable($db, "smis_gd_riwayat_inventaris");
			$query_value = "
				SELECT *
				FROM smis_gd_riwayat_inventaris
				WHERE id_inventaris = '" . $_POST['id_inventaris'] . "' AND prop NOT LIKE 'del' " . $filter . "
			";
			$query_count = "
				SELECT COUNT(*)
				FROM smis_gd_riwayat_inventaris
				WHERE id_inventaris = '" . $_POST['id_inventaris'] . "' AND prop NOT LIKE 'del' " . $filter . "
			";
			$detail_history_dbtable->setPreferredQuery(true, $query_value, $query_count);
			class DetailHistoryDBResponder extends DuplicateResponder {
				public function command($command) {
					if ($command != "print_history")
						return parent::command($command);
					$pack = new ResponsePackage();
					if ($command == "print_history") {
						$content = $this->print_history();
						$pack->setContent($content);
						$pack->setStatus(ResponsePackage::$STATUS_OK);
					}
					return $pack->getPackage();
				}
				public function print_history() {
					$id_inventaris = $_POST['id_inventaris'];
					$inventaris_row = $this->dbtable->get_row("
						SELECT *
						FROM smis_gd_inventaris
						WHERE id = '" . $id_inventaris . "'
					");
					$detail_riwayat_inventaris_rows = $this->dbtable->get_result("
						SELECT *
						FROM smis_gd_riwayat_inventaris
						WHERE id_inventaris = '" . $id_inventaris . "'
					");
					$data_print = "<center><strong>DATA RIWAYAT INVENTARIS</strong></center><br/>";
					$data_print .= "<table border='0'>";
						$data_print .= "<tr>";
							$data_print .= "<td>Nama Barang</td>";
							$data_print .= "<td>:</td>";
							$data_print .= "<td>" . $inventaris_row->nama_barang . "</td>";
						$data_print .= "</tr>";
						$data_print .= "<tr>";
							$data_print .= "<td>Harga Perolehan</td>";
							$data_print .= "<td>:</td>";
							$data_print .= "<td>" . ArrayAdapter::format("money Rp. ", $inventaris_row->hna) . "</td>";
						$data_print .= "</tr>";
					$data_print .= "</table><br/>";
					$data_print .= "<table border='1'>";
						$data_print .= "<tr>";
							$data_print .= "<th>No.</th>";
							$data_print .= "<th>Tanggal</th>";
							$data_print .= "<th>Kode</th>";
							$data_print .= "<th>Merk</th>";
							$data_print .= "<th>Mutasi/Retur</th>";
							$data_print .= "<th>Unit</th>";
							$data_print .= "<th>Petugas Entri</th>";
						$data_print .= "</tr>";
						$no = 1;
						if (count($detail_riwayat_inventaris_rows) > 0) {
							foreach($detail_riwayat_inventaris_rows as $drir) {
								$data_print .= "<tr>";
									$data_print .= "<td>" . $no++ . "</td>";
									$data_print .= "<td>" . ArrayAdapter::format("date d M Y", $drir->tanggal) . "</td>";
									$data_print .= "<td>" . $drir->kode . "</td>";
									$data_print .= "<td>" . $drir->merk . "</td>";
									$data_print .= "<td>" . ArrayAdapter::format("unslug", $drir->status) . "</td>";
									$data_print .= "<td>" . ArrayAdapter::format("unslug", $drir->unit) . "</td>";
									$data_print .= "<td>" . $drir->nama_user . "</td>";
								$data_print .= "</tr>";
							}
						} else {
							$data_print .= "<tr>";
								$data_print .= "<td colspan='7'><center><i>Tidak ada riwayat mutasi/retur inventaris<i></center></td>";
							$data_print .= "</tr>";
						}
					$data_print .= "</table>";
					$data_print .= "<br/>";
					global $user;
					$data_print .= "<table border='0' align='center' class='united'>
									<tr>
										<td colspan='3' align='center'>Mengetahui :</td>
									</tr>
									<tr>
										<td>&nbsp;</td>
									</tr>
									<tr>
										<td align='center'>Tuban, " . ArrayAdapter::format("date d M Y", date("Y-m-d")) . "</td>
										<td>&Tab;</td>
										<td>&nbsp;</td>
									</tr>
									<tr>
										<td align='center'>KA. SUB. BAG. LOGISTIK & ASSET</td>
										<td>&Tab;</td>
										<td align='center'>PETUGAS GUDANG UMUM</td>
									</tr>
									<tr>
										<td>&nbsp;</td>
										<td>&Tab;</td>
										<td>&nbsp;</td>
									</tr>
									<tr>
										<td>&nbsp;</td>
										<td>&Tab;</td>
										<td>&nbsp;</td>
									</tr>
									<tr>
										<td>&nbsp;</td>
										<td>&Tab;</td>
										<td>&nbsp;</td>
									</tr>
									<tr>
										<td>&nbsp;</td>
										<td>&Tab;</td>
										<td>&nbsp;</td>
									</tr>
									<tr>
										<td align='center'><b>Suratmin</b></td>
										<td>&Tab;</td>
										<td align='center'><b>" . $user->getNameOnly() . "</b></td>
									</tr>
								</table>";
					return $data_print;
				}
			}
			$detail_history_dbresponder = new DetailHistoryDBResponder(
				$detail_history_dbtable,
				$detail_history_table,
				$detail_history_adapter
			);
			$data = $detail_history_dbresponder->command($_POST['command']);
			echo json_encode($data);
			return;
		}
		return;
	}
	
	if (isset($_POST['command'])) {
		class BarangAdapter extends ArrayAdapter {
			public function adapt($row) {
				$array = array();
				$array['id'] = $row->id;
				$array['unit'] = $row->unit;
				$array['kode'] = $row->kode;
				$array['Nomor'] = self::format("digit8", $row->id);
				$array['Kode'] = $row->kode;
				if ($row->medis)
					$array['Jenis'] = "Medis";
				else
					$array['Jenis'] = "Non-Medis";
				$array['Barang'] = $row->nama_barang;
				$array['Merk'] = $row->merk;
				$array['Harga'] = self::format("money Rp.", $row->hna);
				$array['Tahun Perolehan'] = $row->tahun_perolehan;
				$array['Penyusutan'] = $row->usia_penyusutan . " tahun";
				if ($row->kondisi_baik)
					$array['Kondisi'] = "Baik";
				else 
					$array['Kondisi'] = "Rusak";
				if ($row->unit == "")
					$array['Lokasi'] = "-";
				else
					$array['Lokasi'] = self::format("unslug", $row->unit);
				return $array;
			}
		}
		$barang_adapter = new BarangAdapter();
		$columns = array("id", "id_dbarang_masuk", "nama_barang", "medis", "hna", "kode", "merk", "tahun_perolehan", "usia_penyusutan", "kondisi_baik", "unit");
		$barang_dbtable = new DBTable(
			$db,
			"smis_gd_inventaris",
			$columns
		);
		$barang_dbresponder = new DBResponder(
			$barang_dbtable,
			$barang_table,
			$barang_adapter
		);
		$data = $barang_dbresponder->command($_POST['command']);
		if (isset($_POST['push_command'])) {
			class PushInventarisServiceConsumer extends ServiceConsumer {
				public function __construct($db, $id_inventaris, $nama_barang, $medis, $kode, $merk, $harga_perolehan, $tahun_perolehan, $usia_penyusutan, $kondisi_baik, $command, $unit) {
					$array = array();
					$array['id_inventaris'] = $id_inventaris;
					$array['nama_barang'] = $nama_barang;
					$array['medis'] = $medis;
					$array['kode'] = $kode;
					$array['merk'] = $merk;
					$array['harga_perolehan'] = $harga_perolehan;
					$array['tahun_perolehan'] = $tahun_perolehan;
					$array['usia_penyusutan'] = $usia_penyusutan;
					$array['kondisi_baik'] = $kondisi_baik;
					$array['command'] = $command;
					parent::__construct($db, "push_inventaris", $array, $unit);
				}
				public function proceedResult() {
					$content = array();
					$result = json_decode($this->result, true);
					foreach ($result as $autonomous) {
						foreach ($autonomous as $entity) {
							if ($entity != null || $entity != "")
								return $entity;
						}
					}
					return $content;
				}
			}
			$inventaris_dbtable = new DBTable($db, "smis_gd_inventaris");
			$inventaris_row = $inventaris_dbtable->get_row("
				SELECT smis_gd_inventaris.*
				FROM smis_gd_inventaris
				WHERE smis_gd_inventaris.prop NOT LIKE 'del' AND smis_gd_inventaris.id = '" . $data['content']['id'] . "'
			");
			$command = "push_" . $_POST['push_command'];
			$push_inventaris_service_consumer = new PushInventarisServiceConsumer(
				$db, 
				$inventaris_row->id, 
				$inventaris_row->nama_barang, 
				$inventaris_row->medis, 
				$inventaris_row->kode, 
				$inventaris_row->merk, 
				$inventaris_row->hna, 
				$inventaris_row->tahun_perolehan, 
				$inventaris_row->usia_penyusutan, 
				$inventaris_row->kondisi_baik,
				$command, 
				$inventaris_row->unit
			);
			$result = $push_inventaris_service_consumer->execute();
			if ($result) {
				$riwayat_inventaris_dbtable = new DBTable($db, "smis_gd_riwayat_inventaris");
				$riwayat_inventaris_data = array();
				$riwayat_inventaris_data['id_inventaris'] = $inventaris_row->id;
				$riwayat_inventaris_data['unit'] = $inventaris_row->unit;
				$riwayat_inventaris_data['tanggal'] = date("Y-m-d");
				$riwayat_inventaris_data['kode'] = $inventaris_row->kode;
				$riwayat_inventaris_data['merk'] = $inventaris_row->merk;
				$riwayat_inventaris_data['usia_penyusutan'] = $inventaris_row->usia_penyusutan;
				$riwayat_inventaris_data['kondisi_baik'] = $inventaris_row->kondisi_baik;
				$riwayat_inventaris_data['status'] = "mutasi";
				global $user;
				$riwayat_inventaris_data['nama_user'] = $user->getName();
				$riwayat_inventaris_data['autonomous'] = "[".getSettings($db, "smis_autonomous_id", "")."]";
		        $riwayat_inventaris_data['duplicate'] = 0;
		        $riwayat_inventaris_data['time_updated'] = date("Y-m-d H:i:s");
		        $riwayat_inventaris_data['origin_updated'] = getSettings($db, "smis_autonomous_id", "");
		        $riwayat_inventaris_data['origin'] = getSettings($db, "smis_autonomous_id", "");
				$riwayat_inventaris_dbtable->insert($riwayat_inventaris_data);
			}
		}
		echo json_encode($data);
		return;
	}
	
	$barang_modal = new Modal("barang_t_add_form", "smis_form_container", "barang_t");
	$barang_modal->setTitle("Data Barang Inventaris");
	$id_hidden = new Hidden("barang_t_id", "barang_t_id", "");
	$barang_modal->addElement("", $id_hidden);
	$nama_barang_text = new Text("barang_t_nama_barang", "barang_t_nama_barang", "");
	$nama_barang_text->setAtribute("disabled='disabled'");
	$barang_modal->addElement("Nama Barang", $nama_barang_text);
	$kode_text = new Text("barang_t_kode", "barang_t_kode", "");
	$barang_modal->addElement("Kode", $kode_text);
	$merk_text = new Text("barang_t_merk", "barang_t_merk", "");
	$barang_modal->addElement("Merk", $merk_text);
	$harga_perolehan_text = new Text("barang_t_harga_perolehan", "barang_t_harga_perolehan", "");
	$harga_perolehan_text->setTypical("money");
	$harga_perolehan_text->setAtribute("disabled='disabled' data-thousands=\".\" data-decimal=\",\" data-prefix=\"Rp. \"  data-precision=\"2\"");
	$barang_modal->addElement("Harga", $harga_perolehan_text);
	$tahun_perolehan_text = new Text("barang_t_tahun_perolehan", "barang_t_tahun_perolehan", "");
	$barang_modal->addElement("Tahun", $tahun_perolehan_text);
	$usia_penyusutan_text = new Text("barang_t_usia_penyusutan", "barang_t_usia_penyusutan", "");
	$barang_modal->addElement("Usia Susut (th)", $usia_penyusutan_text);
	$kondisi_option = array(
		array(
			'name'	=> "Baik",
			'value'	=> 1
		),
		array(
			'name'	=> "Rusak",
			'value'	=> 0
		)
	);
	$kondisi_select = new Select("barang_t_kondisi", "barang_t_kondisi", $kondisi_option);
	$barang_modal->addElement("Kondisi", $kondisi_select);
	class UnitServiceConsumer extends ServiceConsumer {
		public function __construct($db) {
			parent::__construct($db, "get_entity", "push_inventaris");
		}		
		public function proceedResult() {
			$content = array();
			$result = json_decode($this->result, true);
			foreach($result as $autonomous) {
				foreach($autonomous as $entity) {
					$option = array();
					$option['value'] = $entity;
					$option['name'] = ArrayAdapter::format("unslug", $entity);
					$number = count($content);
					$content[$number] = $option;
				}
			}
			return $content;
		}
	}
	$unit_service_consumer = new UnitServiceConsumer($db);
	$unit_service_consumer->execute();
	$unit_option = $unit_service_consumer->getContent();
	$unit_select = new Select("barang_t_unit", "barang_t_unit", $unit_option);
	$barang_modal->addElement("Lokasi", $unit_select);
	$barang_button = new Button("", "", "Simpan");
	$barang_button->setAtribute("id='barang_t_save_btn'");
	$barang_button->setClass("btn-success");
	$barang_button->setIcon("fa fa-floppy-o");
	$barang_button->setIsButton(Button::$ICONIC);
	$barang_modal->addFooter($barang_button);
	$barang_modal->addHTML(
	"<div class='alert alert-block alert-inverse'>" .
		 "<h4>Informasi</h4>" .
		 "Berikut ini adalah aturan taksiran umur ekonomis atas perolehan aset untuk pengisian <b>Usia Susut (th)</b>:" .
		 "<ol>
			<li>Gedung Permanen: 20 Tahun</li>
			<li>Peralatan/Mesin dan Peralatan Medis: 5 Tahun</li>
			<li>Komputer dan Alat Elektronik: 2 Tahun</li>
			<li>Kendaraan Bermotor: 5 Tahun</li>
			<li>Peralatan Kantor dan Meubelir: 5 Tahun</li>
		</ol>" .
	 "</div>", "before");
	 
	$print_kode_modal = new Modal("print_kode_add_form", "smis_form_container", "print_kode");
	$print_kode_modal->setTitle("Cetak Kode");
	$kode_hidden = new Hidden("print_kode_kode", "print_kode_kode", "");
	$print_kode_modal->addElement("", $kode_hidden);
	$option = new OptionBuilder();
	$option->add("Kecil", "2");
	$option->add("Sedang", "4", "1");
	$option->add("Besar", "6");
	$option->add("Sangat Besar", "8");
	$ukuran_select = new Select("print_kode_ukuran_setting", "print_kode_ukuran_setting", $option->getContent());
	$print_kode_modal->addElement("Pilihan", $ukuran_select);
	$jumlah_text = new Text("print_kode_jumlah", "print_kode_jumlah", "");
	$print_kode_modal->addElement("Jumlah", $jumlah_text);
	$button = new Button("", "", "Cetak");
	$button->setClass("btn-inverse");
	$button->setIcon("icon-print icon-white");
	$button->setIsButton(Button::$ICONIC);
	$button->setAction("barang_t.cetak_kode()");
	$print_kode_modal->addFooter($button);
	$print_kode_modal->addHTML("<center><div id='preview_kode'></div></center>", "before");
	
	echo $history_modal->getHtml();
	echo $print_kode_modal->getHtml();
	echo $barang_modal->getHtml();
	echo $barang_table->getHtml();
	echo addJS("framework/smis/js/table_action.js");
	echo addJS("framework/bootstrap/js/bootstrap-datepicker.js");
	echo addCSS("framework/bootstrap/css/datepicker.css");
?>
<script type="text/javascript">
	function BarangTAction(name, page, action, column) {
		this.initialize(name, page, action, column);
	}
	BarangTAction.prototype.constructor = BarangTAction;
	BarangTAction.prototype = new TableAction();
	BarangTAction.prototype.edit = function(id) {
		var data = this.getRegulerData();
		data['command'] = "edit";
		data['id'] = id;
		$.post(
			"",
			data,
			function(response) {
				var json = getContent(response);
				if (json == null) return;
				$("#barang_t_id").val(json.id);
				$("#barang_t_nama_barang").val(json.nama_barang);
				$(".barang_t_kode").show();
				$("#barang_t_kode").removeAttr("disabled");
				$("#barang_t_kode").val(json.kode);
				$(".barang_t_merk").show();
				$("#barang_t_merk").val(json.merk);
				$(".barang_t_harga_perolehan").show();
				$("#barang_t_harga_perolehan").maskMoney();				
				$("#barang_t_harga_perolehan").maskMoney('mask', Number(json.hna));
				$(".barang_t_tahun_perolehan").show();
				$("#barang_t_tahun_perolehan").val(json.tahun_perolehan);
				$(".barang_t_usia_penyusutan").show();
				$("#barang_t_usia_penyusutan").val(json.usia_penyusutan);
				$(".barang_t_kondisi").hide();
				$("#barang_t_kondisi").val(json.kondisi_baik);
				$(".barang_t_unit").hide();
				$("#barang_t_unit").val(json.unit);
				$("#modal_alert_barang_t_add_form").html("");
				$(".error_field").removeClass("error_field");
				$("#barang_t_save_btn").removeAttr("onclick");
				$("#barang_t_save_btn").attr("onclick", "barang_t.update()");
				$("#barang_t_add_form").smodal("show");
				$(".alert").show();
			}
		);
	};
	BarangTAction.prototype.validate = function(is_include_unit) {
		var valid = true;
		var invalid_msg = "";
		var kode = $("#barang_t_kode").val();
		var merk = $("#barang_t_merk").val();
		var tahun_perolehan = $("#barang_t_tahun_perolehan").val();
		var usia_penyusutan = $("#barang_t_usia_penyusutan").val();
		var unit = $("#barang_t_unit").find(":selected").text();
		$(".error_field").removeClass("error_field");
		if (kode == "") {
			valid = false;
			invalid_msg += "</br><strong>Kode</strong> tidak boleh kosong";
			$("#barang_t_kode").addClass("error_field");
		} 
		if (merk == "") {
			valid = false;
			invalid_msg += "</br><strong>Merk</strong> tidak boleh kosong";
			$("#barang_t_merk").addClass("error_field");
		} 
		if (tahun_perolehan == "") {
			valid = false;
			invalid_msg += "</br><strong>Tahun</strong> tidak boleh kosong";
			$("#barang_t_tahun_perolehan").addClass("error_field");
		}
		if (unit == "" && is_include_unit) {
			valid = false;
			invalid_msg += "</br><strong>Lokasi</strong> tidak boleh kosong";
			$("#barang_t_unit").addClass("error_field");
		} 
		if (!valid) {
			$("#modal_alert_barang_t_add_form").html(
				"<div class='alert alert-block alert-danger'>" +
					"<h4>Peringatan</h4>" +
					invalid_msg +
				"</div>"
			);
		}
		return valid;
	};
	BarangTAction.prototype.update = function() {
		if (!this.validate(false)) {
			return;
		}
		$("#barang_t_add_form").smodal("hide");
		showLoading();
		var self = this;
		var data = this.getRegulerData();
		data['command'] = "save";
		data['id'] = $("#barang_t_id").val();
		data['kode'] = $("#barang_t_kode").val();
		data['merk'] = $("#barang_t_merk").val();
		data['tahun_perolehan'] = $("#barang_t_tahun_perolehan").val();
		data['usia_penyusutan'] = $("#barang_t_usia_penyusutan").val();
		$.post(
			"",
			data,
			function(response) {
				var json = getContent(response);
				if (json == null) {
					$("#barang_t_add_form").smodal("show");
				} else {
					self.view();
				}
				dismissLoading();
			}
		);
	};
	BarangTAction.prototype.prepare_send = function(id) {
		var data = this.getRegulerData();
		data['command'] = "edit";
		data['id'] = id;
		$.post(
			"",
			data,
			function(response) {
				var today = new Date();
				var json = getContent(response);
				if (json == null) return;
				$("#barang_t_id").val(json.id);
				$("#barang_t_nama_barang").val(json.nama_barang);
				$(".barang_t_kode").show();
				$("#barang_t_kode").val(json.kode);
				$("#barang_t_kode").removeAttr("disabled");
				$("#barang_t_kode").attr("disabled", "disabled");
				$(".barang_t_merk").hide();
				$("#barang_t_merk").val(json.merk);
				$(".barang_t_harga_perolehan").hide();
				$("#barang_t_harga_perolehan").maskMoney();				
				$("#barang_t_harga_perolehan").maskMoney('mask', Number(json.hna));
				$(".barang_t_tahun_perolehan").hide();
				$("#barang_t_tahun_perolehan").val(json.tahun_perolehan);
				$(".barang_t_usia_penyusutan").hide();
				$("#barang_t_usia_penyusutan").val(json.usia_penyusutan);
				$(".barang_t_kondisi").hide();
				$("#barang_t_kondisi").val(json.kondisi_baik);
				$(".barang_t_unit").show();
				$("#barang_t_unit").val(json.unit);
				$("#barang_t_save_btn").removeAttr("onclick");
				$("#barang_t_save_btn").attr("onclick", "barang_t.send()");
				$("#barang_t_add_form").smodal("show");
				$(".alert").hide();
			}
		);
	};
	BarangTAction.prototype.send = function() {
		if (!this.validate(true)) {
			return;
		}
		$("#barang_t_add_form").smodal("hide");
		showLoading();
		var self = this;
		var data = this.getRegulerData();
		data['command'] = "save";
		data['push_command'] = "insert";
		data['id'] = $("#barang_t_id").val();
		data['unit'] = $("#barang_t_unit").val();
		$.post(
			"",
			data,
			function(response) {
				var json = getContent(response);
				if (json == null) {
					$("#barang_t_add_form").smodal("show");
				} else {
					self.view();
				}
				dismissLoading();
			}
		);
	};
	BarangTAction.prototype.edit_kondisi = function(id) {
		var data = this.getRegulerData();
		data['command'] = "edit";
		data['id'] = id;
		$.post(
			"",
			data,
			function(response) {
				var json = getContent(response);
				if (json == null) return;
				$("#barang_t_id").val(json.id);
				$("#barang_t_nama_barang").val(json.nama_barang);
				$(".barang_t_kode").show();				
				$("#barang_t_kode").val(json.kode);
				$("#barang_t_kode").removeAttr("disabled");
				$("#barang_t_kode").attr("disabled", "disabled");
				$(".barang_t_merk").hide();
				$("#barang_t_merk").val(json.merk);
				$(".barang_t_harga_perolehan").hide();
				$("#barang_t_harga_perolehan").maskMoney();				
				$("#barang_t_harga_perolehan").maskMoney('mask', Number(json.hna));
				$(".barang_t_tahun_perolehan").hide();
				$("#barang_t_tahun_perolehan").val(json.tahun_perolehan);
				$(".barang_t_usia_penyusutan").hide();
				$("#barang_t_usia_penyusutan").val(json.usia_penyusutan);
				$(".barang_t_kondisi").show();
				$("#barang_t_kondisi").val(json.kondisi_baik);
				$(".barang_t_unit").hide();
				$("#barang_t_unit").val(json.unit);
				$("#barang_t_save_btn").removeAttr("onclick");
				$("#barang_t_save_btn").attr("onclick", "barang_t.update_kondisi()");
				$("#barang_t_add_form").smodal("show");
				$(".alert").hide();
			}
		);
	};
	BarangTAction.prototype.update_kondisi = function() {
		$("#barang_t_add_form").smodal("hide");
		showLoading();
		var self = this;
		var data = this.getRegulerData();
		data['command'] = "save";
		data['push_command'] = "update";
		data['id'] = $("#barang_t_id").val();
		data['kondisi_baik'] = $("#barang_t_kondisi").val();
		$.post(
			"",
			data,
			function(response) {
				var json = getContent(response);
				if (json == null) {
					$("#barang_t_add_form").smodal("show");
				} else {
					self.view();
				}
				dismissLoading();
			}
		);
	};
	BarangTAction.prototype.print_kode = function(kode) {
		$("#modal_alert_print_kode_add_form").html("");
		$(".error_field").removeClass("error_field");
		$("#print_kode_kode").val(kode);
		$("#print_kode_jumlah").val("1");
		var ukuran_font = $("#print_kode_ukuran_setting").val();
		$("#preview_kode").html(
			"<table border='" + (ukuran_font / 2) + "' cellpadding='" + ((ukuran_font / 2) * 3) + "' class='united'>" +
				"<tr>" +
					"<td id='kode_content'><font size='" + ukuran_font + "'><b>" + kode + "</b></td>" +
				"</tr>" +
			"</table>"
		);
		$("#print_kode_add_form").smodal("show");
	};
	BarangTAction.prototype.validate_print_kode = function() {
		var valid = true;
		var invalid_msg = "";
		var jumlah = $("#print_kode_jumlah").val();
		$(".error_field").removeClass("error_field");
		if (jumlah == "") {
			valid = false;
			invalid_msg += "</br><strong>Jumlah</strong> tidak boleh kosong";
			$("#print_kode_jumlah").addClass("error_field");
		} else if (!is_numeric(jumlah)) {
			valid = false;
			invalid_msg += "</br><strong>Jumlah</strong> seharusnya numerik (0-9)";
			$("#print_kode_jumlah").addClass("error_field");
		} else if (jumlah == 0) {
			valid = false;
			invalid_msg += "</br><strong>Jumlah</strong> tidak boleh 0 (nol)";
			$("#print_kode_jumlah").addClass("error_field");
		}
		if (!valid) {
			$("#modal_alert_print_kode_add_form").html(
				"<div class='alert alert-block alert-danger'>" +
					"<h4>Peringatan</h4>" +
					invalid_msg +
				"</div>"
			);
		}
		return valid;
	};
	BarangTAction.prototype.view_history = function(id) {
		var data = this.getRegulerData();
		data['command'] = "edit";
		data['id'] = id;
		$.post(
			"",
			data,
			function(response) {
				var json = getContent(response);
				if (json == null) return;
				$("#history_id_inventaris").val(id);
				$("#history_nama_barang").val(json.nama_barang);
				$("#history_hna").val("Rp. " + (parseFloat(json.hna)).formatMoney("2", ".", ","));
				detail_history.setSuperCommand("detail_history");
				detail_history.view();
				$("#history_add_form").smodal("show");
			}
		);
	};
	BarangTAction.prototype.cetak_kode = function() {
		if (!this.validate_print_kode())
			return;
		var ukuran = $("#print_kode_ukuran_setting").val();			
		var kode = $("#print_kode_kode").val();
		var jumlah = $("#print_kode_jumlah").val();
		var data_print = "";
		for(var i = 0; i < jumlah; i++) {
			data_print += 	"<table border='" + (ukuran / 2) + "' cellpadding='" + ((ukuran / 2) * 3) + "' class='united'>" +
								"<tr>" +
									"<td id='kode_content'><font size='" + ukuran + "'><b>" + kode + "</b></td>" +
								"</tr>" +
							"</table>";
			if (i + 1 < jumlah)
				data_print += "<br/>";
		}
		smis_print(data_print);
	};
	
	function DetailHistoryAction(name, page, action, column) {
		this.initialize(name, page, action, column);
	}
	DetailHistoryAction.prototype.constructor = DetailHistoryAction;
	DetailHistoryAction.prototype = new TableAction();
	DetailHistoryAction.prototype.getViewData = function() {
		var data = TableAction.prototype.getViewData.call(this);
		data['id_inventaris'] = $("#history_id_inventaris").val();
		console.log(data);
		return data;
	};
	DetailHistoryAction.prototype.print = function() {
		var data = this.getRegulerData();
		data['super_command'] = "detail_history";
		data['command'] = "print_history";
		data['id_inventaris'] = $("#history_id_inventaris").val();
		$.post(
			"",
			data,
			function(response) {
				var json = getContent(response);
				if (json == null) return;
				smis_print(json);
			}
		);
	};

	var barang_t;
	var detail_history;
	$(document).ready(function() {
		$('[data-toggle="popover"]').popover({
			trigger: 'hover',
			'placement': 'top'
		});
		$(".mydate").datepicker();
		$("#print_kode_ukuran_setting").on("change", function() {
			var ukuran = $("#print_kode_ukuran_setting").val();			
			var kode = $("#print_kode_kode").val();
			if (kode != "") {
				$("#preview_kode").html(
					"<table border='" + (ukuran / 2) + "' cellpadding='" + ((ukuran / 2) * 3) + "' class='united'>" +
						"<tr>" +
							"<td id='kode_content'><font size='" + ukuran + "'><b>" + kode + "</b></td>" +
						"</tr>" +
					"</table>"
				);
			}
		});
		detail_history = new DetailHistoryAction(
			"detail_history",
			"gudang_umum",
			"daftar_barang_terinventarisasi",
			new Array()
		);
		var barang_t_columns = new Array("id", "id_dbarang_masuk", "nama_barang", "medis", "hna", "kode", "merk", "tahun_perolehan", "usia_penyusutan", "kondisi_baik", "unit", "harga_perolehan");
		barang_t = new BarangTAction(
			"barang_t",
			"gudang_umum",
			"daftar_barang_terinventarisasi",
			barang_t_columns
		);
		barang_t.view();
	});
</script>
<style type="text/css">
	.united { 
		page-break-inside:avoid !important; 
	}
</style>