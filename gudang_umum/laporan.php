<?php 
	$laporan_tabulator = new Tabulator("laporan", "", Tabulator::$LANDSCAPE);
	$laporan_tabulator->add("laporan_barang_masuk", "Penerimaan Barang Per Vendor", "gudang_umum/laporan_barang_masuk.php", Tabulator::$TYPE_INCLUDE);
	$laporan_tabulator->add("laporan_rekap_faktur", "Rekap Faktur", "gudang_umum/laporan_rekap_faktur.php", Tabulator::$TYPE_INCLUDE);
	$laporan_tabulator->add("laporan_rekap_perbekalan_barang", "Rekap Perbekalan Barang", "gudang_umum/laporan_rekap_perbekalan_barang.php", Tabulator::$TYPE_INCLUDE);
	$laporan_tabulator->add("laporan_perbekalan_barang_per_kategori", "Perbekalan Barang Per Kategori", "gudang_umum/laporan_perbekalan_barang_per_kategori.php", Tabulator::$TYPE_INCLUDE);
	$laporan_tabulator->add("laporan_rekap_mutasi_barang", "Rekap Mutasi Barang", "gudang_umum/laporan_rekap_mutasi_barang.php", Tabulator::$TYPE_INCLUDE);
	$laporan_tabulator->add("laporan_mutasi_barang_per_ruangan", "Mutasi Barang Per Ruangan", "gudang_umum/laporan_mutasi_barang_per_ruangan.php", Tabulator::$TYPE_INCLUDE);
	$laporan_tabulator->add("laporan_inventaris_medis", "Inventaris Medis", "gudang_umum/laporan_inventaris_medis.php", Tabulator::$TYPE_INCLUDE);
	$laporan_tabulator->add("laporan_inventaris_medis_per_ruang", "Inventaris Medis Per Ruangan", "gudang_umum/laporan_inventaris_medis_per_ruang.php", Tabulator::$TYPE_INCLUDE);
	$laporan_tabulator->add("laporan_inventaris_non_medis", "Inventaris Non-Medis", "gudang_umum/laporan_inventaris_non_medis.php", Tabulator::$TYPE_INCLUDE);
	$laporan_tabulator->add("laporan_inventaris_non_medis_per_ruang", "Inventaris Non-Medis Per Ruangan", "gudang_umum/laporan_inventaris_non_medis_per_ruang.php", Tabulator::$TYPE_INCLUDE);
	
	echo $laporan_tabulator->getHtml();
?>
<style type="text/css">
	@page {
		margin-left: 50px;
		margin-right: 50px;
		margin-top: 50px;
		margin-bottom: 50px;
	}
	@media print{
		#signature {
			page-break-inside: avoid !important;
		}
	}
	.united { 
		page-break-inside:avoid !important; 
	}
</style>