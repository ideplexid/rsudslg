<?php 
	$daftar_barang_table = new Table(
		array("Nomor", "Nama Barang", "Jenis Barang", "Stok"),
		"Gudang Umum : Daftar Barang",
		null,
		true
	);
	$daftar_barang_table->setName("daftar_barang");
	$daftar_barang_table->setAddButtonEnable(false);
	$daftar_barang_table->setPrintButtonEnable(false);
	$daftar_barang_table->setReloadButtonEnable(false);
	$daftar_barang_table->setAction(false);
	
	if (isset($_POST['command'])) {
		class DaftarBarangAdapter extends ArrayAdapter {
			public function adapt($row) {
				$array = array();
				$array['id'] = $row->id_barang;
				$array['Nomor'] = self::format("digit8", $row->id_barang);
				$array['Nama Barang'] = $row->nama_barang;
				$array['Jenis Barang'] = $row->nama_jenis_barang;
				$array['Stok'] = $row->stok;
				return $array;
			}
		}
		$daftar_barang_adapter = new DaftarBarangAdapter();
		$columns = array("id_barang", "nama_barang", "nama_jenis_barang", "sisa");
		$daftar_barang_dbtable = new DBTable(
			$db,
			"smis_gd_stok_barang",
			$columns
		);
		$filter = "";
		if (isset($_POST['kriteria'])) {
			$filter = " AND (smis_gd_stok_barang.nama_barang LIKE '%" . $_POST['kriteria'] . "%' OR smis_gd_stok_barang.nama_jenis_barang LIKE '%" . $_POST['kriteria'] . "%')";
		}
		$query_value = "
			SELECT *
			FROM (
				SELECT id_barang, nama_barang, nama_jenis_barang, GROUP_CONCAT(CONCAT(sisa, ' ', satuan) ORDER BY satuan, sisa ASC SEPARATOR ', ') AS 'stok'
				FROM (
					SELECT smis_gd_dbarang_masuk.id_barang, smis_gd_stok_barang.nama_barang, smis_gd_stok_barang.nama_jenis_barang, SUM(smis_gd_stok_barang.sisa) AS 'sisa', smis_gd_stok_barang.satuan, smis_gd_stok_barang.konversi, smis_gd_stok_barang.satuan_konversi
					FROM smis_gd_stok_barang LEFT JOIN smis_gd_dbarang_masuk ON smis_gd_stok_barang.id_dbarang_masuk = smis_gd_dbarang_masuk.id
					WHERE smis_gd_stok_barang.prop NOT LIKE 'del' " . $filter . "
					GROUP BY smis_gd_dbarang_masuk.id_barang, smis_gd_stok_barang.satuan, smis_gd_stok_barang.konversi, smis_gd_stok_barang.satuan_konversi
				) v_barang
				GROUP BY id_barang
			) v_stok
		";
		$query_count = "
			SELECT COUNT(*)
			FROM (
				SELECT id_barang, nama_barang, nama_jenis_barang, GROUP_CONCAT(CONCAT(sisa, ' ', satuan) ORDER BY satuan, sisa ASC SEPARATOR ', ') AS 'stok'
				FROM (
					SELECT smis_gd_dbarang_masuk.id_barang, smis_gd_stok_barang.nama_barang, smis_gd_stok_barang.nama_jenis_barang, SUM(smis_gd_stok_barang.sisa) AS 'sisa', smis_gd_stok_barang.satuan, smis_gd_stok_barang.konversi, smis_gd_stok_barang.satuan_konversi
					FROM smis_gd_stok_barang LEFT JOIN smis_gd_dbarang_masuk ON smis_gd_stok_barang.id_dbarang_masuk = smis_gd_dbarang_masuk.id
					WHERE smis_gd_stok_barang.prop NOT LIKE 'del' " . $filter . "
					GROUP BY smis_gd_dbarang_masuk.id_barang, smis_gd_stok_barang.satuan, smis_gd_stok_barang.konversi, smis_gd_stok_barang.satuan_konversi
				) v_barang
				GROUP BY id_barang
			) v_stok
		";
		$daftar_barang_dbtable->setPreferredQuery(true, $query_value, $query_count);
		$daftar_barang_dbresponder = new DBResponder(
			$daftar_barang_dbtable,
			$daftar_barang_table,
			$daftar_barang_adapter
		);
		$data = $daftar_barang_dbresponder->command($_POST['command']);
		echo json_encode($data);
		return;
	}
	
	echo $daftar_barang_table->getHtml();
	echo addJS("framework/smis/js/table_action.js");
?>
<script type="text/javascript">
	function DaftarBarangAction(name, page, action, column) {
		this.initialize(name, page, action, column);
	}
	DaftarBarangAction.prototype.constructor = DaftarBarangAction;
	DaftarBarangAction.prototype = new TableAction();
	
	var daftar_barang;
	$(document).ready(function() {
		$('[data-toggle="popover"]').popover({
			trigger: 'hover',
			'placement': 'top'
		});
		var daftar_barang_columns = new Array("id_barang", "nama_barang", "nama_jenis_barang", "sisa");
		daftar_barang = new DaftarBarangAction(
			"daftar_barang",
			"gudang_umum",
			"daftar_barang",
			daftar_barang_columns
		);
		daftar_barang.view();
	});
</script>