<?php 
	$pengecekan_stok_barang_ed_form = new Form("psbed_form", "", "Pengecekan Stok ED");
	$tanggal_text = new Text("psbed_tanggal", "psbed_tanggal", date('Y-m-d'));
	$tanggal_text->setClass("mydate");
	$tanggal_text->setAtribute("data-date-format='yyyy-m-d'");
	$pengecekan_stok_barang_ed_form->addElement("Tgl. Sekarang", $tanggal_text);
	$rentang_option = new OptionBuilder();
	$rentang_option->add("7 hari", "7");
	$rentang_option->add("30 hari", "30");
	$rentang_option->add("60 hari", "60");
	$rentang_option->add("90 hari", "90", "1");
	$rentang_select = new Select("psbed_rentang", "psbed_rentang", $rentang_option->getContent());
	$pengecekan_stok_barang_ed_form->addElement("Rentang", $rentang_select);
	$show_button = new Button("", "", "Tampilkan");
	$show_button->setClass("btn-primary");
	$show_button->setIcon("icon-white icon-repeat");
	$show_button->setIsButton(Button::$ICONIC);
	$show_button->setAction("psbed.view()");
	$pengecekan_stok_barang_ed_form->addElement("", $show_button);
	
	$psbed_table = new Table(
		array("Nomor", "Nama Barang", "Jenis Barang", "Produsen", "Vendor", "No. Faktur", "Tgl. Faktur", "Jumlah", "Ket. Jumlah", "Tgl. Exp."),
		"",
		null,
		true
	);
	$psbed_table->setName("psbed");
	$psbed_table->setAddButtonEnable(false);
	$psbed_table->setReloadButtonEnable(false);
	$psbed_table->setPrintButtonEnable(false);
	$psbed_table->setAction(false);
	
	if (isset($_POST['command'])) {
		class PSOEDAdapter extends ArrayAdapter {
			public function adapt($row) {
				$array = array();
				$array['Nomor'] = self::format("digit8", $row->id);
				$array['Nama Barang'] = $row->nama_barang;
				$array['Jenis Barang'] = $row->nama_jenis_barang;
				$array['Produsen'] = $row->produsen;
				$array['Vendor'] = $row->nama_vendor;
				$array['No. Faktur'] = $row->no_faktur;
				$array['Tgl. Faktur'] = self::format("date d M Y", $row->tanggal);
				$array['Jumlah'] = $row->sisa . " " . $row->satuan;
				$array['Ket. Jumlah'] = "1 " . $row->satuan . " = " . $row->konversi . " " . $row->satuan_konversi;
				$array['Tgl. Exp.'] = self::format("date d M Y", $row->tanggal_exp);
				return $array;
			}
		}
		$psbed_adapter = new PSOEDAdapter();
		$psbed_dbtable = new DBTable(
			$db,
			"smis_gd_stok_barang"
		);
		$filter = "";
		if (isset($_POST['kriteria'])) {
			$filter = " AND (smis_gd_stok_barang.nama_barang LIKE '%" . $_POST['kriteria'] . "%' OR smis_gd_stok_barang.nama_jenis_barang LIKE '%" . $_POST['kriteria'] . "%' OR smis_gd_stok_barang.produsen LIKE '%" . $_POST['kriteria'] . "%' OR smis_gd_stok_barang.nama_vendor LIKE '%" . $_POST['keiteria'] . "%')";
		}
		$query_value = "
			SELECT *
			FROM (
				SELECT smis_gd_stok_barang.*, smis_gd_barang_masuk.no_faktur, smis_gd_barang_masuk.tanggal, smis_gd_barang_masuk.tanggal_datang, smis_gd_barang_masuk.tipe
				FROM (smis_gd_stok_barang LEFT JOIN smis_gd_dbarang_masuk ON smis_gd_stok_barang.id_dbarang_masuk = smis_gd_dbarang_masuk.id) LEFT JOIN smis_gd_barang_masuk ON smis_gd_dbarang_masuk.id_barang_masuk = smis_gd_barang_masuk.id
				WHERE smis_gd_stok_barang.prop NOT LIKE 'del' AND smis_gd_stok_barang.tanggal_exp <> '0000-00-00' AND smis_gd_stok_barang.sisa > 0 AND DATEDIFF(smis_gd_stok_barang.tanggal_exp, '" . $_POST['tanggal'] . "') <= " . $_POST['rentang'] . " " . $filter . "
				ORDER BY smis_gd_stok_barang.tanggal_exp, smis_gd_stok_barang.nama_barang ASC
			) v
		";
		$query_count = "
			SELECT COUNT(*)
			FROM (
				SELECT smis_gd_stok_barang.*, smis_gd_barang_masuk.no_faktur, smis_gd_barang_masuk.tanggal, smis_gd_barang_masuk.tanggal_datang, smis_gd_barang_masuk.tipe
				FROM (smis_gd_stok_barang LEFT JOIN smis_gd_dbarang_masuk ON smis_gd_stok_barang.id_dbarang_masuk = smis_gd_dbarang_masuk.id) LEFT JOIN smis_gd_barang_masuk ON smis_gd_dbarang_masuk.id_barang_masuk = smis_gd_barang_masuk.id
				WHERE smis_gd_stok_barang.prop NOT LIKE 'del' AND smis_gd_stok_barang.tanggal_exp <> '0000-00-00' AND smis_gd_stok_barang.sisa > 0 AND DATEDIFF(smis_gd_stok_barang.tanggal_exp, '" . $_POST['tanggal'] . "') <= " . $_POST['rentang'] . " " . $filter . "
				ORDER BY smis_gd_stok_barang.tanggal_exp, smis_gd_stok_barang.nama_barang ASC
			) v
		";
		$psbed_dbtable->setPreferredQuery(true, $query_value, $query_count);
		$psbed_dbresponder = new DBResponder(
			$psbed_dbtable,
			$psbed_table,
			$psbed_adapter
		);
		$data = $psbed_dbresponder->command($_POST['command']);
		echo json_encode($data);
		return;
	}
	
	echo $pengecekan_stok_barang_ed_form->getHtml();
	echo "<div id'table_content'>";
	echo $psbed_table->getHtml();
	echo "</div>";
	echo addJS("framework/smis/js/table_action.js");
	echo addJS("framework/bootstrap/js/bootstrap-datepicker.js");
	echo addCSS("framework/bootstrap/css/datepicker.css");
?>
<script type="text/javascript">
	function PSBEDAction(name, page, action, column) {
		this.initialize(name, page, action, column);
	}
	PSBEDAction.prototype.constructor = PSBEDAction;
	PSBEDAction.prototype = new TableAction();
	PSBEDAction.prototype.getViewData = function() {
		var data = TableAction.prototype.getViewData.call(this);
		data['tanggal'] = $("#psbed_tanggal").val();
		data['rentang'] = $("#psbed_rentang").val();
		return data;
	};
	
	var psbed;
	$(document).ready(function() {
		$('[data-toggle="popover"]').popover({
			trigger: 'hover',
			'placement': 'top'
		});
		$(".mydate").datepicker();
		psbed = new PSBEDAction(
			"psbed",
			"gudang_umum",
			"pengecekan_stok_ed",
			new Array()
		);
		psbed.view();
	});
</script>