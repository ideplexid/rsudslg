<?php 
	require_once("smis-base/smis-include-service-consumer.php");
	require_once("smis-base/smis-include-duplicate.php");
	
	class ReturBarangTable extends Table {
		public function getBodyContent(){
			$content = "";
			if ($this->content!=NULL) {
				foreach ($this->content as $d) {
					$content .= "<tr>";
					foreach ($this->header as $h) {
						$content .= "<td>" . $d[$h] . "</td>";
					}
					if ($this->is_action) {
						$content .= "<td>" . $this->getFilteredContentButton($d['id'], $d['status'], $d['restok'])->getHtml() . "</td>";
					}
					$content .= "</tr>";
				}
			}
			return $content;
		}
		public function getFilteredContentButton($id, $status, $restok) {
			$btn_group = new ButtonGroup("noprint");
			if ($status == "sudah") {
				$btn = new Button("", "", "Lihat");
				$btn->setAction($this->action . ".detail('" . $id . "')");
				if (!$restok) {
					$btn->setClass("btn-success");
				} else {
					$btn->setClass("btn-inverse");
				}
				$btn->setAtribute("data-content='Lihat' data-toggle='popover'");
				$btn->setIcon("icon-eye-open icon-white");
				$btn->setIsButton(Button::$ICONIC);
				$btn_group->addElement($btn);
				if (!$restok) {
					$btn = new Button("", "", "Restok");
					$btn->setAction($this->action . ".restock('" . $id . "')");
					$btn->setAtribute("data-content='Restok' data-toggle='popover'");
					$btn->setIcon("fa fa-refresh");
					$btn->setIsButton(Button::$ICONIC);
					$btn_group->addElement($btn);
				} else {
					$btn = new Button("", "", "Batal Restok");
					$btn->setAction($this->action . ".cancel_restock('" . $id . "')");
					$btn->setClass("btn-danger");
					$btn->setAtribute("data-content='Batal Restok' data-toggle='popover'");
					$btn->setIcon("icon-remove icon-white");
					$btn->setIsButton(Button::$ICONIC);
					$btn_group->addElement($btn);
				}
			} else {
				$btn = new Button("", "", "Ubah");
				$btn->setAction($this->action . ".edit('" . $id . "')");
				$btn->setClass("btn-warning");
				$btn->setAtribute("data-content='Ubah' data-toggle='popover'");
				$btn->setIcon("icon-edit icon-white");
				$btn->setIsButton(Button::$ICONIC);
				$btn_group->addElement($btn);
			}
			return $btn_group;
		}
	}
	$retur_barang_table = new ReturBarangTable(
		array("Unit", "Nomor", "Tanggal Retur", "Nama Barang", "Jenis Barang", "Produsen", "Vendor", "Jumlah", "Status"),
		"Gudang Umum : Retur Barang Unit",
		null,
		true
	);
	$retur_barang_table->setName("retur_barang");
	$retur_barang_table->setAddButtonEnable(false);
	
	if (isset($_POST['command'])) {
		class ReturBarangAdapter extends ArrayAdapter {
			public function adapt($row) {
				$array = array();
				$array['id'] = $row->id;
				$array['status'] = $row->status;
				$array['restok'] = $row->restok;
				$array['Unit'] = self::format("unslug", $row->unit);
				$array['Nomor'] = self::format("digit8", $row->f_id);
				$array['Tanggal Retur'] = self::format("date d M Y", $row->tanggal);
				$array['Nama Barang'] = $row->nama_barang;
				$array['Jenis Barang'] = $row->nama_jenis_barang;
				$array['Produsen'] = $row->produsen;
				$array['Vendor'] = $row->nama_vendor;
				$array['Jumlah'] = $row->jumlah . " " . $row->satuan;
				if ($row->status == "belum") {
					$array['Status'] = "Belum Diterima";
				} else if ($row->status == "sudah") {
					$array['Status'] = "Sudah Diterima";
					if ($row->restok)
						$array['Status'] = "Restok";
				} else if ($row->status == "dikembalikan") {
					$array['Status'] = "Dikembalikan";
				}
				return $array;
			}
		}
		$retur_barang_adapter = new ReturBarangAdapter();
		$columns = array("id", "f_id", "unit", "tanggal", "id_barang", "nama_barang", "nama_jenis_barang", "jumlah", "satuan", "konversi", "satuan_konversi", "hna", "tanggal_exp", "turunan", "keterangan", "status", "id_vendor", "nama_vendor", "produsen", "restok");
		$retur_barang_dbtable = new DBTable(
			$db,
			"smis_gd_retur_barang_unit",
			$columns
		);
		$filter = "";
		if (isset($_POST['kriteria'])) {
			$filter = " AND (nama_barang LIKE '%" . $_POST['kriteria'] . "%' OR nama_jenis_barang LIKE '%" . $_POST['kriteria'] . "%' OR unit LIKE '%" . $_POST['kriteria'] . "%' OR produsen LIKE '%" . $_POST['kriteria'] . "%' OR nama_vendor LIKE '%" . $_POST['kriteria'] . "%')";
		}
		$query_value = "
			SELECT *
			FROM smis_gd_retur_barang_unit
			WHERE prop NOT LIKE 'del' AND status != 'dikembalikan' " . $filter . "
		";
		$query_count = "
			SELECT COUNT(*)
			FROM smis_gd_retur_barang_unit
			WHERE prop NOT LIKE 'del' AND status != 'dikembalikan' " . $filter . "
		";
		$retur_barang_dbtable->setPreferredQuery(true, $query_value, $query_count);
		class ReturBarangDBResponder extends DuplicateResponder {
			public function save() {
				$data = $this->postToArray();
				$id['id'] = $_POST['id'];
				$result = $this->dbtable->update($data, $id);
				$success['type'] = "update";
				if (isset($_POST['restok'])) {
					if ($_POST['restok'] == true) {
						//menambah stok:
						$retur_data = $this->dbtable->get_row("
							SELECT *
							FROM smis_gd_retur_barang_unit
							WHERE id = '" . $id['id'] . "'
						");
						$stok_keluar_data = $this->dbtable->get_row("
							SELECT *
							FROM smis_gd_stok_barang_keluar
							WHERE id = '" . $retur_data->lf_id . "'
						");
						$stok_data = $this->dbtable->get_row("
							SELECT *
							FROM smis_gd_stok_barang
							WHERE id = '" . $stok_keluar_data->id_stok_barang . "'
						");
						$new_stok_data = array();
						$new_stok_data['sisa'] = $stok_data->sisa + $retur_data->jumlah;
						$new_stok_data['autonomous'] = "[".$this->getAutonomous()."]";
				        $new_stok_data['duplicate'] = 0;
				        $new_stok_data['time_updated'] = date("Y-m-d H:i:s");
				        $new_stok_data['origin_updated'] = $this->getAutonomous();
						$new_stok_id = array();
						$new_stok_id['id'] = $stok_keluar_data->id_stok_barang;
						$stok_dbtable = new DBTable($this->dbtable->get_db(), "smis_gd_stok_barang");
						$stok_dbtable->update($new_stok_data, $new_stok_id);
						//logging riwayat stok:
						$riwayat_dbtable = new DBTable($this->dbtable->get_db(), "smis_gd_riwayat_stok_barang");
						$data_riwayat = array();
						$data_riwayat['tanggal'] = date("Y-m-d");
						$data_riwayat['id_stok_barang'] = $stok_keluar_data->id_stok_barang;
						$data_riwayat['jumlah_masuk'] = $retur_data->jumlah;
						$data_riwayat['sisa'] = $stok_data->sisa + $retur_data->jumlah;
						$data_riwayat['keterangan'] = "Restok Retur Barang dari Unit " . ArrayAdapter::format("unslug", $retur_data->unit);
						global $user;
						$data_riwayat['nama_user'] = $user->getName();
						$data_riwayat['autonomous'] = "[".$this->getAutonomous()."]";
				        $data_riwayat['duplicate'] = 0;
				        $data_riwayat['time_updated'] = date("Y-m-d H:i:s");
				        $data_riwayat['origin_updated'] = $this->getAutonomous();
				        $data_riwayat['origin'] = $this->getAutonomous();
						$riwayat_dbtable->insert($data_riwayat);
					} else {
						//mengurangi stok:
						$retur_data = $this->dbtable->get_row("
							SELECT *
							FROM smis_gd_retur_barang_unit
							WHERE id = '" . $id['id'] . "'
						");
						$stok_keluar_data = $this->dbtable->get_row("
							SELECT *
							FROM smis_gd_stok_barang_keluar
							WHERE id = '" . $retur_data->lf_id . "'
						");
						$stok_data = $this->dbtable->get_row("
							SELECT *
							FROM smis_gd_stok_barang
							WHERE id = '" . $stok_keluar_data->id_stok_barang . "'
						");
						$new_stok_data = array();
						$new_stok_data['sisa'] = $stok_data->sisa - $retur_data->jumlah;
						$new_stok_data['autonomous'] = "[".$this->getAutonomous()."]";
				        $new_stok_data['duplicate'] = 0;
				        $new_stok_data['time_updated'] = date("Y-m-d H:i:s");
				        $new_stok_data['origin_updated'] = $this->getAutonomous();
						$new_stok_id = array();
						$new_stok_id['id'] = $stok_keluar_data->id_stok_barang;
						$stok_dbtable = new DBTable($this->dbtable->get_db(), "smis_gd_stok_barang");
						$stok_dbtable->update($new_stok_data, $new_stok_id);
						//logging riwayat stok:
						$riwayat_dbtable = new DBTable($this->dbtable->get_db(), "smis_gd_riwayat_stok_barang");
						$data_riwayat = array();
						$data_riwayat['tanggal'] = date("Y-m-d");
						$data_riwayat['id_stok_barang'] = $stok_keluar_data->id_stok_barang;
						$data_riwayat['jumlah_keluar'] = $retur_data->jumlah;
						$data_riwayat['sisa'] = $stok_data->sisa - $retur_data->jumlah;
						$data_riwayat['keterangan'] = "Pembatalan Restok Retur Barang dari Unit " . ArrayAdapter::format("unslug", $retur_data->unit);
						global $user;
						$data_riwayat['nama_user'] = $user->getName();
						$data_riwayat['autonomous'] = "[".$this->getAutonomous()."]";
				        $data_riwayat['duplicate'] = 0;
				        $data_riwayat['time_updated'] = date("Y-m-d H:i:s");
				        $data_riwayat['origin_updated'] = $this->getAutonomous();
				        $data_riwayat['origin'] = $this->getAutonomous();
						$riwayat_dbtable->insert($data_riwayat);
					}
				}
				$success['id'] = $id['id'];
				$success['success'] = 1;
				if ($result === false) $success['success'] = 0;
				return $success;
			}
		}
		$retur_barang_dbresponder = new ReturBarangDBResponder(
			$retur_barang_dbtable,
			$retur_barang_table,
			$retur_barang_adapter
		);
		$data = $retur_barang_dbresponder->command($_POST['command']);
		//set_retur_barang_status:
		if (isset($_POST['push_command'])) {
			class SetReturBarangStatusServiceConsumer extends ServiceConsumer {
				public function __construct($db, $id, $status, $unit, $command) {
					$array = array();
					$array['id'] = $id;
					$array['status'] = $status;
					$array['command'] = $command;
					parent::__construct($db, "set_retur_barang_status", $array, $unit);
				}
				public function proceedResult() {
					$content = array();
					$result = json_decode($this->result,true);
					foreach ($result as $autonomous) {
						foreach ($autonomous as $entity) {
							if ($entity != null || $entity != "")
								return $entity;
						}
					}
					return $content;
				}
			}
			$retur_barang_dbtable = new DBTable($db, "smis_gd_retur_barang_unit");
			$retur_barang_row = $retur_barang_dbtable->get_row("
				SELECT f_id, unit, status
				FROM smis_gd_retur_barang_unit
				WHERE id = '" . $data['content']['id'] . "'
			");
			$command = "push_" . $_POST['push_command'];
			$set_retur_barang_status_service_consumer = new SetReturBarangStatusServiceConsumer($db, $retur_barang_row->f_id, $retur_barang_row->status, $retur_barang_row->unit, $command);
			$set_retur_barang_status_service_consumer->execute();
		}
		echo json_encode($data);
		return;
	}
	
	$retur_barang_modal = new Modal("retur_barang_add_form", "smis_form_container", "retur_barang");
	$retur_barang_modal->setTitle("Retur Barang");
	$id_hidden = new Hidden("retur_barang_id", "retur_barang_id", "");
	$retur_barang_modal->addElement("", $id_hidden);
	$id_stok_barang_hidden = new Hidden("retur_barang_id_stok_barang", "retur_barang_id_stok_barang", "");
	$retur_barang_modal->addElement("", $id_stok_barang_hidden);
	$tanggal_text = new Text("retur_barang_tanggal", "retur_barang_tanggal", "");
	$tanggal_text->setAtribute("disabled='disabled'");
	$retur_barang_modal->addElement("Tanggal", $tanggal_text);
	$nama_barang_text = new Text("retur_barang_nama_barang", "retur_barang_nama_barang", "");
	$nama_barang_text->setAtribute("disabled='disabled'");
	$retur_barang_modal->addElement("Nama Barang", $nama_barang_text);
	$nama_jenis_text = new Text("retur_barang_jenis", "retur_barang_jenis", "");
	$nama_jenis_text->setAtribute("disabled='disabled'");
	$retur_barang_modal->addElement("Jenis Barang", $nama_jenis_text);
	$produsen_text = new Text("retur_barang_produsen", "retur_barang_produsen", "");
	$produsen_text->setAtribute("disabled='disabled'");
	$retur_barang_modal->addElement("Produsen", $produsen_text);
	$vendor_text = new Text("retur_barang_vendor", "retur_barang_vendor", "");
	$vendor_text->setAtribute("disabled='disabled'");
	$retur_barang_modal->addElement("Vendor", $vendor_text);
	$f_jumlah_text = new Text("retur_barang_f_jumlah", "retur_barang_f_jumlah", "");
	$f_jumlah_text->setAtribute("disabled='disabled'");
	$retur_barang_modal->addElement("Jumlah", $f_jumlah_text);
	$keterangan_textarea = new TextArea("retur_barang_keterangan", "retur_barang_keterangan", "");
	$keterangan_textarea->setLine(2);
	$keterangan_textarea->setAtribute("disabled='disabled'");
	$retur_barang_modal->addElement("Keterangan", $keterangan_textarea);
	$retur_barang_button = new Button("", "", "Terima");
	$retur_barang_button->setClass("btn-success");
	$retur_barang_button->setAtribute("id='retur_barang_accept'");
	$retur_barang_button->setAction("retur_barang.accept()");
	$retur_barang_button->setIcon("fa fa-check");
	$retur_barang_button->setIsButton(Button::$ICONIC);
	$retur_barang_modal->addFooter($retur_barang_button);
	$retur_barang_button = new Button("", "", "Kembalikan");
	$retur_barang_button->setClass("btn-danger");
	$retur_barang_button->setAtribute("id='retur_barang_return'");
	$retur_barang_button->setAction("retur_barang.return()");
	$retur_barang_button->setIcon("fa fa-times");
	$retur_barang_button->setIsButton(Button::$ICONIC);
	$retur_barang_modal->addFooter($retur_barang_button);
	$retur_barang_button = new Button("", "", "OK");
	$retur_barang_button->setClass("btn-success");
	$retur_barang_button->setAtribute("id='retur_barang_ok'");
	$retur_barang_button->setAction("$($(this).data('target')).smodal('hide')");
	$retur_barang_modal->addFooter($retur_barang_button);
	
	echo $retur_barang_modal->getHtml();
	echo $retur_barang_table->getHtml();
	echo addJS("framework/smis/js/table_action.js");
?>
<script type="text/javascript">
	function ReturBarangAction(name, page, action, column) {
		this.initialize(name, page, action, column);
	}
	ReturBarangAction.prototype.constructor = ReturBarangAction;
	ReturBarangAction.prototype = new TableAction();
	ReturBarangAction.prototype.edit = function(id) {
		var data = this.getRegulerData();
		data['command'] = "edit";
		data['id'] = id;
		$.post(
			"",
			data,
			function(response) {
				var json = getContent(response);
				if (json == null) return;
				$("#retur_barang_id").val(json.id);
				$("#retur_barang_tanggal").val(json.tanggal);
				$("#retur_barang_nama_barang").val(json.nama_barang);
				$("#retur_barang_jenis").val(json.nama_jenis_barang);
				$("#retur_barang_produsen").val(json.produsen);
				$("#retur_barang_vendor").val(json.nama_vendor);
				$("#retur_barang_f_jumlah").val(json.jumlah + " " + json.satuan);
				$("#retur_barang_keterangan").val(json.keterangan);
				$("#retur_barang_accept").show();
				$("#retur_barang_return").show();
				$("#retur_barang_ok").hide();
				$("#retur_barang_add_form").smodal("show");
			}
		);
	};
	ReturBarangAction.prototype.accept = function() {
		$("#retur_barang_add_form").smodal("hide");
		showLoading();
		var self = this;
		var data = this.getRegulerData();
		data['command'] = "save";
		data['push_command'] = "save";
		data['id'] = $("#retur_barang_id").val();
		data['status'] = "sudah";
		$.post(
			"",
			data,
			function(response) {
				var json = getContent(response);
				if (json == null) {
					$("#retur_barang_add_form").smodal("show");
				} else {
					self.view();
				}
				dismissLoading();
			}
		);
	};
	ReturBarangAction.prototype.return = function() {
		$("#retur_barang_add_form").smodal("hide");
		showLoading();
		var self = this;
		var data = this.getRegulerData();
		data['command'] = "save";
		data['push_command'] = "save";
		data['id'] = $("#retur_barang_id").val();
		data['status'] = "dikembalikan";
		$.post(
			"",
			data,
			function(response) {
				var json = getContent(response);
				if (json == null) {
					$("#retur_barang_add_form").smodal("show");
				} else {
					self.view();
				}
				dismissLoading();
			}
		);
	};
	ReturBarangAction.prototype.detail = function(id) {
		var data = this.getRegulerData();
		data['command'] = "edit";
		data['id'] = id;
		$.post(
			"",
			data,
			function(response) {
				var json = getContent(response);
				if (json == null) return;
				$("#retur_barang_id").val(json.id);
				$("#retur_barang_tanggal").val(json.tanggal);
				$("#retur_barang_nama_barang").val(json.nama_barang);
				$("#retur_barang_jenis").val(json.nama_jenis_barang);
				$("#retur_barang_produsen").val(json.produsen);
				$("#retur_barang_vendor").val(json.nama_vendor);
				$("#retur_barang_f_jumlah").val(json.jumlah + " " + json.satuan);
				$("#retur_barang_keterangan").val(json.keterangan);
				$("#retur_barang_accept").hide();
				$("#retur_barang_return").hide();
				$("#retur_barang_ok").show();
				$("#retur_barang_add_form").smodal("show");
			}
		);
	};
	ReturBarangAction.prototype.restock = function(id) {
		var self = this;
		bootbox.confirm(
			"Yakin memasukkan Stok Retur ini ke Perbekalan?",
			function(result) {
				if (result) {
					var data = self.getRegulerData();
					data['command'] = "save";
					data['id'] = id;
					data['restok'] = "1";
					$.post(
						"",
						data,
						function(response) {
							var json = getContent(response);
							if (json == null) return;
							self.view();
						}
					);
				}
			}
		);
	};
	ReturBarangAction.prototype.cancel_restock = function(id) {
		var self = this;
		bootbox.confirm(
			"Yakin membatalkan Restok Retur ini ke Perbekalan?",
			function(result) {
				if (result) {
					var data = self.getRegulerData();
					data['command'] = "save";
					data['id'] = id;
					data['restok'] = "0";
					$.post(
						"",
						data,
						function(response) {
							var json = getContent(response);
							if (json == null) return;
							self.view();
						}
					);
				}
			}
		);
	};
	
	var retur_barang;
	$(document).ready(function() {
		$('[data-toggle="popover"]').popover({
			trigger: 'hover',
			'placement': 'top'
		});
		var retur_barang_columns = new Array("id", "f_id", "unit", "tanggal", "id_barang", "nama_barang", "nama_jenis_barang", "jumlah", "satuan", "konversi", "satuan_konversi", "hna", "tanggal_exp", "turunan", "keterangan", "status", "id_vendor", "nama_vendor", "produsen", "restok");
		retur_barang = new ReturBarangAction(
			"retur_barang",
			"gudang_umum",
			"retur_barang_unit",
			retur_barang_columns
		);
		retur_barang.view();
	});
</script>