<?php 

class DFakturAdapter extends ArrayAdapter{
	private $total=0;
	public function adapt($d){
		$a=array();
		$a['nama_barang']=$d->nama_barang;
		$a['id_barang']=$d->id_barang;
		$a['nama_jenis_barang']=$d->nama_jenis_barang;
		$a['jumlah']=$d->jumlah;
		$a['satuan']=$d->satuan;
		$a['konversi']=$d->konversi;
		$a['satuan_konversi']=$d->satuan_konversi;
		$a['hna']=$d->hna;
		$a['harga_sebelum_diskon']=($d->hna+$d->selisih)*$d->jumlah;
		$a['diskon']=$d->diskon;
		$a['t_diskon']=$d->t_diskon;
		if($a['t_diskon']=="nominal"){
			$a['val_diskon']=ArrayAdapter::format("money Rp.", $d->diskon);
			$a['n_diskon']=$d->diskon*1;
			$a['harga_setelah_diskon']=$a['harga_sebelum_diskon']-$a['n_diskon'];
		}else{
			$a['val_diskon']=$d->diskon."%";
			$a['n_diskon']=$a['harga_sebelum_diskon']*($d->diskon*1)/100;
			$a['harga_setelah_diskon']=$a['harga_sebelum_diskon']-$a['n_diskon'];
		}
		$a['hbayar']=$a['harga_setelah_diskon'];
		$this->total+=$a['hbayar'];
		return $a;
	}
	public function getTotalBayar(){
		return $this->total;
	}
}
?>