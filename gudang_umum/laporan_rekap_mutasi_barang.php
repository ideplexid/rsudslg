<?php 
	global $db;
	
	$lrmb_table = new Table(
		array("Nama Ruangan", "Total"),
		"Gudang Umum : Laporan Rekapitulasi Mutasi Barang",
		null,
		true
	);
	$lrmb_table->setName("lrmb");
	$lrmb_table->setAddButtonEnable(false);
	$lrmb_table->setEditButtonEnable(false);
	$lrmb_table->setDelButtonEnable(false);
	
	if (isset($_POST['command'])) {
		class LLSOAdapter extends ArrayAdapter {
			public function adapt($row) {
				$array = array();
				$array['id'] = "";
				$array['Nama Ruangan'] = self::format("unslug", $row->unit);
				$array['Total'] = self::format("money Rp. ", $row->total_hna);
				return $array;
			}
		}
		$lrmb_adapter = new LLSOAdapter();		
		$lrmb_dbtable = new DBTable($db, "smis_gd_barang_keluar");
		$filter = "1";
		if (isset($_POST['kriteria'])) {
			$filter .= " AND (unit LIKE '%" . $_POST['kriteria'] . "%') ";
		}
		$query_value = "
			SELECT *
			FROM (
				SELECT smis_gd_barang_keluar.unit, SUM(smis_gd_stok_barang_keluar.jumlah * smis_gd_stok_barang.hna) AS 'total_hna'
				FROM ((smis_gd_barang_keluar LEFT JOIN smis_gd_dbarang_keluar ON smis_gd_barang_keluar.id = smis_gd_dbarang_keluar.id_barang_keluar) LEFT JOIN smis_gd_stok_barang_keluar ON smis_gd_dbarang_keluar.id = smis_gd_stok_barang_keluar.id_dbarang_keluar) LEFT JOIN smis_gd_stok_barang ON smis_gd_stok_barang_keluar.id_stok_barang = smis_gd_stok_barang.id
				WHERE smis_gd_barang_keluar.prop NOT LIKE 'del' AND smis_gd_dbarang_keluar.prop NOT LIKE 'del' AND smis_gd_stok_barang_keluar.prop NOT LIKE 'del'
				GROUP BY smis_gd_barang_keluar.unit
				ORDER BY smis_gd_barang_keluar.unit ASC
			) v_lrmb
			WHERE " . $filter . "
		";
		$query_count = "
			SELECT COUNT(*)
			FROM (
				SELECT smis_gd_barang_keluar.unit, SUM(smis_gd_stok_barang_keluar.jumlah * smis_gd_stok_barang.hna) AS 'total_hna'
				FROM ((smis_gd_barang_keluar LEFT JOIN smis_gd_dbarang_keluar ON smis_gd_barang_keluar.id = smis_gd_dbarang_keluar.id_barang_keluar) LEFT JOIN smis_gd_stok_barang_keluar ON smis_gd_dbarang_keluar.id = smis_gd_stok_barang_keluar.id_dbarang_keluar) LEFT JOIN smis_gd_stok_barang ON smis_gd_stok_barang_keluar.id_stok_barang = smis_gd_stok_barang.id
				WHERE smis_gd_barang_keluar.prop NOT LIKE 'del' AND smis_gd_dbarang_keluar.prop NOT LIKE 'del' AND smis_gd_stok_barang_keluar.prop NOT LIKE 'del'
				GROUP BY smis_gd_barang_keluar.unit
				ORDER BY smis_gd_barang_keluar.unit ASC
			) v_lrmb
			WHERE " . $filter . "
		";
		$lrmb_dbtable->setPreferredQuery(true, $query_value, $query_count);
		class LLSODBResponder extends DBResponder {
			public function command($command) {
				if ($command != "print_laporan") {
					return parent::command($command);
				}
				$pack = new ResponsePackage();
				if ($command == "print_laporan") {
					$content = $this->print_report();
					$pack->setContent($content);
					$pack->setStatus(ResponsePackage::$STATUS_OK);
				}
				return $pack->getPackage();
			}
			public function print_report() {
				$data = $this->dbtable->get_result("
					SELECT *
					FROM (
						SELECT smis_gd_barang_keluar.unit, SUM(smis_gd_stok_barang_keluar.jumlah * smis_gd_stok_barang.hna) AS 'total_hna'
						FROM ((smis_gd_barang_keluar LEFT JOIN smis_gd_dbarang_keluar ON smis_gd_barang_keluar.id = smis_gd_dbarang_keluar.id_barang_keluar) LEFT JOIN smis_gd_stok_barang_keluar ON smis_gd_dbarang_keluar.id = smis_gd_stok_barang_keluar.id_dbarang_keluar) LEFT JOIN smis_gd_stok_barang ON smis_gd_stok_barang_keluar.id_stok_barang = smis_gd_stok_barang.id
						WHERE smis_gd_barang_keluar.prop NOT LIKE 'del' AND smis_gd_dbarang_keluar.prop NOT LIKE 'del' AND smis_gd_stok_barang_keluar.prop NOT LIKE 'del'
						GROUP BY smis_gd_barang_keluar.unit
						ORDER BY smis_gd_barang_keluar.unit ASC
					) v_lrmb
				");
				$print_data = "";
				$print_data .= "<center><strong>LAPORAN REKAPITULASI MUTASI BARANG</strong></center><br/>";
				$print_data .= "Periode : " . ArrayAdapter::format("date d M Y", date("Y-m-d"));
				$print_data .= "<table border='1'>
									<tr>
										<th>No.</th>
										<th>Nama Ruangan</th>
										<th>Total</th>
									</tr>";
				$total_netto = 0;
				if (count($data) > 0) {
					$no = 1;
					foreach($data as $d) {
						$tanggal_part = explode(" ", $d->tanggal);
						$time_part = explode(":", $tanggal_part[1]);
						$print_data .= "<tr>
											<td>" . $no++ . "</td>
											<td>" . ArrayAdapter::format("unslug", $d->unit) . "</td>
											<td>" . ArrayAdapter::format("money Rp. ", $d->total_hna) . "</td>
										</tr>";
						$total_netto += $d->total_hna;
					}
				} else {
					$print_data .= "<tr>
										<td colspan='3' align='center'><i>Tidak terdapat data mutasi barang</i></td>
									</tr>";
				}
				$print_data .= "<tr>
									<td colspan='2' align='center'><b>T O T A L</b></td>
									<td><b>" . ArrayAdapter::format("money Rp. ", $total_netto) . "</b></td>
								</tr>";
				$print_data .= "</table><br/>";
				global $user;
				$print_data .= "<table border='0' align='center' class='united'>
									<tr>
										<td colspan='3' align='center'>Mengetahui :</td>
									</tr>
									<tr>
										<td>&nbsp;</td>
									</tr>
									<tr>
										<td align='center'>Jember, " . ArrayAdapter::format("date d M Y", date("Y-m-d")) . "</td>
										<td>&Tab;</td>
										<td>&nbsp;</td>
									</tr>
									<tr>
										<td align='center'>KAUR. GUDANG NON OBAT</td>
										<td>&Tab;</td>
										<td align='center'>PETUGAS GUDANG UMUM</td>
									</tr>
									<tr>
										<td>&nbsp;</td>
										<td>&Tab;</td>
										<td>&nbsp;</td>
									</tr>
									<tr>
										<td>&nbsp;</td>
										<td>&Tab;</td>
										<td>&nbsp;</td>
									</tr>
									<tr>
										<td>&nbsp;</td>
										<td>&Tab;</td>
										<td>&nbsp;</td>
									</tr>
									<tr>
										<td>&nbsp;</td>
										<td>&Tab;</td>
										<td>&nbsp;</td>
									</tr>
									<tr>
										<td align='center'><b>Suharyanto</b></td>
										<td>&Tab;</td>
										<td align='center'><b>" . $user->getNameOnly() . "</b></td>
									</tr>
								</table>";
				return $print_data;
			}
		}
		$lrmb_dbresponder = new LLSODBResponder(
			$lrmb_dbtable,
			$lrmb_table,
			$lrmb_adapter
		);
		$data = $lrmb_dbresponder->command($_POST['command']);
		echo json_encode($data);
		return;
	}
	
	echo $lrmb_table->getHtml();
	echo addJS("framework/smis/js/table_action.js");
?>
<script type="text/javascript">
	function LLSOAction(name, page, action, column) {
		this.initialize(name, page, action, column);
	}
	LLSOAction.prototype.constructor = LLSOAction;
	LLSOAction.prototype = new TableAction();
	LLSOAction.prototype.print = function() {
		var data = this.getRegulerData();
		data['command'] = "print_laporan";
		$.post(
			"",
			data,
			function(response) {
				var json = getContent(response);
				if (json == null) return;
				smis_print(json);
			}
		);
	};
	
	var lrmb;
	$(document).ready(function() {
		$('[data-toggle="popover"]').popover({
			trigger: 'hover',
			'placement': 'top'
		});
		lrmb = new LLSOAction(
			"lrmb",
			"gudang_umum",
			"laporan_rekap_mutasi_barang",
			new Array()
		)
	});
</script>