<?php 
	global $db;
	
	$laporan_form = new Form("lbm_form", "", "Gudang Umum : Laporan Penerimaan Barang dari Vendor");
	$tanggal_from_text = new Text("lbm_tanggal_from", "lbm_tanggal_from", "");
	$tanggal_from_text->setClass("mydate");
	$tanggal_from_text->setAtribute("data-date-format='yyyy-mm-dd'");
	$laporan_form->addElement("Waktu Awal", $tanggal_from_text);
	$tanggal_to_text = new Text("lbm_tanggal_to", "lbm_tanggal_to", "");
	$tanggal_to_text->setClass("mydate");
	$tanggal_to_text->setAtribute("data-date-format='yyyy-mm-dd'");
	$laporan_form->addElement("Waktu Akhir", $tanggal_to_text);
	$show_button = new Button("", "", "Tampilkan");
	$show_button->setClass("btn-primary");
	$show_button->setIcon("icon-white icon-repeat");
	$show_button->setIsButton(Button::$ICONIC);
	$show_button->setAction("lbm.view()");
	$print_button = new Button("", "", "Cetak");
	$print_button->setClass("btn-inverse");
	$print_button->setIcon("icon-white icon-print");
	$print_button->setIsButton(Button::$ICONIC);
	$print_button->setAction("lbm.print()");
	$btn_group = new ButtonGroup("noprint");
	$btn_group->addButton($show_button);
	$btn_group->addButton($print_button);
	$laporan_form->addElement("", $btn_group);
	
	$lbm_table = new Table(
		array("No. Faktur", "Vendor", "Nama Barang", "Jumlah", "Satuan", "H. Satuan", "Total Harga"),
		"",
		null,
		true
	);
	$lbm_table->setName("lbm");
	$lbm_table->setAction(false);
	
	if (isset($_POST['command'])) {
		class LBMAdapter extends ArrayAdapter {
			public function adapt($row) {
				$array = array();
				$array['id'] = $row->id;
				$array['No. Faktur'] = $row->no_faktur;
				$array['Vendor'] = $row->nama_vendor;
				$array['Nama Barang'] = $row->nama_barang;
				$array['Jumlah'] = self::format("number", $row->jumlah);
				$array['Satuan'] = $row->satuan;
				$array['H. Satuan'] = self::format("money Rp. ", $row->hna + $row->selisih);
				$array['Total Harga'] = self::format("money Rp. ", ($row->jumlah * ($row->hna + $row->selisih)));
				return $array;
			}
		}
		$lbm_adapter = new LBMAdapter();		
		$lbm_dbtable = new DBTable($db, "smis_gd_barang_masuk");
		if (isset($_POST['command']) == "list") {
			$filter = "tanggal >= '" . $_POST['tanggal_from'] . "' AND tanggal <= '" . $_POST['tanggal_to'] . "'";
			if (isset($_POST['kriteria'])) {
				$filter .= " AND (no_faktur LIKE '%" . $_POST['kriteria'] . "%' OR nama_vendor LIKE '%" . $_POST['kriteria'] . "%' OR nama_barang LIKE '%" . $_POST['kriteria'] . "%' OR satuan LIKE '%" . $_POST['kriteria'] . "%') ";
			}
			$query_value = "
				SELECT *
				FROM (
					SELECT smis_gd_dbarang_masuk.id, smis_gd_barang_masuk.tanggal, smis_gd_barang_masuk.no_faktur, smis_gd_barang_masuk.nama_vendor, smis_gd_dbarang_masuk.nama_barang, smis_gd_dbarang_masuk.jumlah, smis_gd_dbarang_masuk.satuan, smis_gd_dbarang_masuk.hna, smis_gd_dbarang_masuk.selisih, smis_gd_dbarang_masuk.diskon AS 'd_diskon', smis_gd_dbarang_masuk.t_diskon AS 'd_t_diskon', smis_gd_barang_masuk.diskon AS 'h_diskon', smis_gd_barang_masuk.t_diskon AS 'h_t_diskon'
					FROM smis_gd_dbarang_masuk LEFT JOIN smis_gd_barang_masuk ON smis_gd_dbarang_masuk.id_barang_masuk = smis_gd_barang_masuk.id
					WHERE smis_gd_dbarang_masuk.prop NOT LIKE 'del' AND smis_gd_barang_masuk.prop NOT LIKE 'del' AND smis_gd_barang_masuk.id <> '0'
				) v_lbm
				WHERE " . $filter . "
				ORDER BY id ASC
			";
			$query_count = "
				SELECT COUNT(*)
				FROM (
					SELECT smis_gd_dbarang_masuk.id, smis_gd_barang_masuk.tanggal, smis_gd_barang_masuk.no_faktur, smis_gd_barang_masuk.nama_vendor, smis_gd_dbarang_masuk.nama_barang, smis_gd_dbarang_masuk.jumlah, smis_gd_dbarang_masuk.satuan, smis_gd_dbarang_masuk.hna, smis_gd_dbarang_masuk.selisih, smis_gd_dbarang_masuk.diskon AS 'd_diskon', smis_gd_dbarang_masuk.t_diskon AS 'd_t_diskon', smis_gd_barang_masuk.diskon AS 'h_diskon', smis_gd_barang_masuk.t_diskon AS 'h_t_diskon'
					FROM smis_gd_dbarang_masuk LEFT JOIN smis_gd_barang_masuk ON smis_gd_dbarang_masuk.id_barang_masuk = smis_gd_barang_masuk.id
					WHERE smis_gd_dbarang_masuk.prop NOT LIKE 'del' AND smis_gd_barang_masuk.prop NOT LIKE 'del' AND smis_gd_barang_masuk.id <> '0'
				) v_lbm
				WHERE " . $filter . "
				ORDER BY id ASC
			";
			$lbm_dbtable->setPreferredQuery(true, $query_value, $query_count);
		}
		class LBMDBResponder extends DBResponder {
			public function command($command) {
				if ($command != "print_laporan") {
					return parent::command($command);
				}
				$pack = new ResponsePackage();
				if ($command == "print_laporan") {
					$content = $this->print_report();
					$pack->setContent($content);
					$pack->setStatus(ResponsePackage::$STATUS_OK);
				}
				return $pack->getPackage();
			}
			public function print_report() {
				$from = $_POST['from'];
				$to = $_POST['to'];
				$data = $this->dbtable->get_result("
					SELECT *
					FROM (
						SELECT smis_gd_dbarang_masuk.id, smis_gd_barang_masuk.tanggal, smis_gd_barang_masuk.no_faktur, smis_gd_barang_masuk.nama_vendor, smis_gd_dbarang_masuk.nama_barang, smis_gd_dbarang_masuk.jumlah, smis_gd_dbarang_masuk.satuan, smis_gd_dbarang_masuk.hna, smis_gd_dbarang_masuk.selisih, smis_gd_dbarang_masuk.diskon AS 'd_diskon', smis_gd_dbarang_masuk.t_diskon AS 'd_t_diskon', smis_gd_barang_masuk.diskon AS 'h_diskon', smis_gd_barang_masuk.t_diskon AS 'h_t_diskon'
						FROM smis_gd_dbarang_masuk LEFT JOIN smis_gd_barang_masuk ON smis_gd_dbarang_masuk.id_barang_masuk = smis_gd_barang_masuk.id
						WHERE smis_gd_dbarang_masuk.prop NOT LIKE 'del' AND smis_gd_barang_masuk.prop NOT LIKE 'del' AND smis_gd_barang_masuk.id <> '0'
					) v_lbm
					WHERE tanggal >= '" . $from . "' AND tanggal <= '" . $to . "'
					ORDER BY id ASC
				");
				$print_data = "";
				$print_data .= "<center><strong>LAPORAN PENERIMAAN BARANG GUDANG UMUM DARI VENDOR</strong></center><br/>";
				$print_data .= "<table border='0'>
									<tr>
										<td>Dari</td>
										<td>:</td>
										<td>" . ArrayAdapter::format("date d M Y", $from) . "</td>
									</tr>
									<tr>
										<td>Sampai</td>
										<td>:</td>
										<td>" . ArrayAdapter::format("date d M Y", $to) . "</td>
									</tr>
								</table>";
				$print_data .= "<table border='1'>
									<tr>
										<th>No. Faktur</th>
										<th>Vendor</th>
										<th>Nama Obat</th>
										<th>Jumlah</th>
										<th>Satuan</th>
										<th>H. Satuan</th>
										<th>Total Harga</th>
									</tr>";
				$total = 0;
				if (count($data) > 0) {
					foreach($data as $d) {
						$hna = $d->hna + $d->selisih;
						$total_hna = $d->jumlah * ($d->hna + $d->selisih);
						$print_data .= "<tr>
											<td>" . $d->no_faktur . "</td>
											<td>" . $d->nama_vendor . "</td>
											<td>" . $d->nama_barang . "</td>
											<td>" . ArrayAdapter::format("number", $d->jumlah) . "</td>
											<td>" . $d->satuan . "</td>
											<td>" . ArrayAdapter::format("money Rp. ",  $hna) . "</td>
											<td>" . ArrayAdapter::format("money Rp. ", $total_hna) . "</td>
										</tr>";
						$total += $total_hna;
					}
				} else {
					$print_data .= "<tr>
										<td colspan='7' align='center'><i>Tidak terdapat data penerimaan</i></td>
									</tr>";
				}
				$print_data .= "<tr>
									<td colspan='6' align='center'><b>T O T A L</b></td>
									<td><b>" . ArrayAdapter::format("money Rp. ", $total) . "</b></td>
								</tr>";
				$print_data .= "</table><br/>";
				global $user;
				$print_data .= "<table border='0' align='center' class='united'>
									<tr>
										<td colspan='3' align='center'>Mengetahui :</td>
									</tr>
									<tr>
										<td>&nbsp;</td>
									</tr>
									<tr>
										<td align='center'>Jember, " . ArrayAdapter::format("date d M Y", date("Y-m-d")) . "</td>
										<td>&Tab;</td>
										<td>&nbsp;</td>
									</tr>
									<tr>
										<td align='center'>KAUR. GUDANG NON OBAT</td>
										<td>&Tab;</td>
										<td align='center'>PETUGAS GUDANG UMUM</td>
									</tr>
									<tr>
										<td>&nbsp;</td>
										<td>&Tab;</td>
										<td>&nbsp;</td>
									</tr>
									<tr>
										<td>&nbsp;</td>
										<td>&Tab;</td>
										<td>&nbsp;</td>
									</tr>
									<tr>
										<td>&nbsp;</td>
										<td>&Tab;</td>
										<td>&nbsp;</td>
									</tr>
									<tr>
										<td>&nbsp;</td>
										<td>&Tab;</td>
										<td>&nbsp;</td>
									</tr>
									<tr>
										<td align='center'><b>Suharyanto</b></td>
										<td>&Tab;</td>
										<td align='center'><b>" . $user->getNameOnly() . "</b></td>
									</tr>
								</table>";
				return $print_data;
			}
		}
		$lbm_dbresponder = new LBMDBResponder(
			$lbm_dbtable,
			$lbm_table,
			$lbm_adapter
		);
		$data = $lbm_dbresponder->command($_POST['command']);
		echo json_encode($data);
		return;
	}
	
	echo $laporan_form->getHtml();
	echo "<div id='table_content'>";
	echo $lbm_table->getHtml();
	echo "</div>";
	echo addJS("framework/smis/js/table_action.js");
	echo addJS("framework/bootstrap/js/bootstrap-datepicker.js");
	echo addCSS("framework/bootstrap/css/datepicker.css");
?>
<script type="text/javascript">
	function LBMAction(name, page, action, column) {
		this.initialize(name, page, action, column);
	}
	LBMAction.prototype.constructor = LBMAction;
	LBMAction.prototype = new TableAction();
	LBMAction.prototype.view = function() {
		var self = this;
		var data = this.getViewData();
		data['tanggal_from'] = $("#lbm_tanggal_from").val();
		data['tanggal_to'] = $("#lbm_tanggal_to").val();
		showLoading();
		$.post(
			"",
			data,
			function(response) {
				var json = getContent(response);
				if (json == null) return;
				$("#" + self.prefix + "_list").html(json.list);
				$("#" + self.prefix + "_pagination").html(json.pagination);
				dismissLoading();
			}
		);
	};
	LBMAction.prototype.print = function() {
		if ($("#lbm_tanggal_from").val() == "" || $("#lbm_tanggal_to").val() == "")
			return;
		var data = this.getRegulerData();
		data['command'] = "print_laporan";
		data['from'] = $("#lbm_tanggal_from").val();
		data['to'] = $("#lbm_tanggal_to").val();
		$.post(
			"",
			data,
			function(response) {
				var json = getContent(response);
				if (json == null) return;
				smis_print(json);
			}
		);
	};
	
	var lbm;
	$(document).ready(function() {
		$('[data-toggle="popover"]').popover({
			trigger: 'hover',
			'placement': 'top'
		});
		lbm = new LBMAction(
			"lbm",
			"gudang_umum",
			"laporan_barang_masuk",
			new Array()
		)
		$('.mydate').datepicker();
	});
</script>