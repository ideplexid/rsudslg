<?php 
	$riwayat_penyesuaian_stok_form = new Form("form_riwayat_penyesuaian_stok", "", "");
	$tanggal_text = new Text("riwayat_penyesuaian_stok_tanggal", "riwayat_penyesuaian_stok_tanggal", date('Y-m-d'));
	$tanggal_text->setClass("mydate smis-one-option-input");
	$tanggal_text->setAtribute("data-date-format='yyyy-m-d'");
	$show_button = new Button("", "", "Tampilkan");
	$show_button->setClass("btn-primary");
	$show_button->setIcon("icon-white icon-repeat");
	$show_button->setisButton(Button::$ICONIC);
	$show_button->setAction("riwayat_penyesuaian_stok.view()");
	$filter_input_group = new InputGroup("");
	$filter_input_group->addComponent($tanggal_text);
	$filter_input_group->addComponent($show_button);
	$riwayat_penyesuaian_stok_form->addElement("Tanggal", $filter_input_group);
	
	$riwayat_penyesuaian_stok_table = new Table(
		array("Tanggal", "Nama Barang", "Jenis Barang", "Jml. Awal", "Jml. Aktual", "Selisih", "Keterangan", "Petugas Entri"),
		"",
		null,
		true
	);
	$riwayat_penyesuaian_stok_table->setName("riwayat_penyesuaian_stok");
	$riwayat_penyesuaian_stok_table->setAddButtonEnable(false);
	$riwayat_penyesuaian_stok_table->setReloadButtonEnable(false);
	$riwayat_penyesuaian_stok_table->setPrintButtonEnable(false);
	$riwayat_penyesuaian_stok_table->setAction(false);
	
	if (isset($_POST['command'])) {
		class RiwayatPenyesuaianStokAdapter extends ArrayAdapter {
			public function adapt($row) {
				$array = array();
				$array['Tanggal'] = self::format("date d M Y", $row->tanggal);
				$array['Nama Barang'] = $row->nama_barang;
				$array['Jenis Barang'] = $row->nama_jenis_barang;
				$array['Jml. Awal'] = $row->jumlah_lama . " " . $row->satuan;
				$array['Jml. Aktual'] = $row->jumlah_baru . " " . $row->satuan;
				$sign = "";
				if ($row->jumlah_baru - $row->jumlah_lama > 0) {
					$sign = "+";
				}
				$array['Selisih'] = $sign . ($row->jumlah_baru - $row->jumlah_lama) . " " . $row->satuan;
				$array['Keterangan'] = $row->keterangan;
				$array['Petugas Entri'] = $row->nama_user;
				return $array;
			}
		}
		$riwayat_penyesuaian_stok_adapter = new RiwayatPenyesuaianStokAdapter();
		$riwayat_penyesuaian_stok_dbtable = new DBTable(
			$db,
			"smis_gd_penyesuaian_stok"
		);
		$filter = "";
		if (isset($_POST['kriteria'])) {
			$filter = " AND (smis_gd_stok_barang.nama_barang LIKE '%" . $_POST['kriteria'] . "%' OR smis_gd_stok_barang.nama_jenis_barang LIKE '%" . $_POST['kriteria'] . "%' OR smis_gd_penyesuaian_stok.nama_user LIKE '%" . $_POST['kriteria'] . "%' OR smis_gd_stok_barang.produsen LIKE '%" . $_POST['kriteria'] . "%' OR smis_gd_stok_barang.nama_vendor LIKE '%" . $_POST['kriteria'] . "%')";
		}
		$query_value = "
			SELECT smis_gd_penyesuaian_stok.*, smis_gd_stok_barang.nama_barang, smis_gd_stok_barang.nama_jenis_barang, smis_gd_stok_barang.satuan
			FROM smis_gd_penyesuaian_stok LEFT JOIN smis_gd_stok_barang ON smis_gd_penyesuaian_stok.id_stok_barang = smis_gd_stok_barang.id
			WHERE smis_gd_penyesuaian_stok.prop NOT LIKE 'del' AND smis_gd_penyesuaian_stok.tanggal = '" . $_POST['tanggal'] . "' " . $filter . "
		";
		$query_count = "
			SELECT COUNT(*)
			FROM  (
				SELECT smis_gd_penyesuaian_stok.*, smis_gd_stok_barang.nama_barang, smis_gd_stok_barang.nama_jenis_barang, smis_gd_stok_barang.satuan
				FROM smis_gd_penyesuaian_stok LEFT JOIN smis_gd_stok_barang ON smis_gd_penyesuaian_stok.id_stok_barang = smis_gd_stok_barang.id
				WHERE smis_gd_penyesuaian_stok.prop NOT LIKE 'del' AND smis_gd_penyesuaian_stok.tanggal = '" . $_POST['tanggal'] . "' " . $filter . "
			) v_riwayat_penyesuaian_stok
		";
		$riwayat_penyesuaian_stok_dbtable->setPreferredQuery(true, $query_value, $query_count);
		$riwayat_penyesuaian_stok_dbresponder = new DBResponder(
			$riwayat_penyesuaian_stok_dbtable,
			$riwayat_penyesuaian_stok_table,
			$riwayat_penyesuaian_stok_adapter
		);
		$data = $riwayat_penyesuaian_stok_dbresponder->command($_POST['command']);
		echo json_encode($data);
		return;
	}
	
	echo $riwayat_penyesuaian_stok_form->getHtml();
	echo "<div id'table_content'>";
	echo $riwayat_penyesuaian_stok_table->getHtml();
	echo "</div>";
	echo addJS("framework/smis/js/table_action.js");
?>
<script type="text/javascript">
	function RiwayatPenyesuaianStokAction(name, page, action, column) {
		this.initialize(name, page, action, column);
	}
	RiwayatPenyesuaianStokAction.prototype.constructor = RiwayatPenyesuaianStokAction;
	RiwayatPenyesuaianStokAction.prototype = new TableAction();
	RiwayatPenyesuaianStokAction.prototype.view = function() {
		var self = this;
		var data = this.getRegulerData();
		data['command'] = "list";
		data['tanggal'] = $("#riwayat_penyesuaian_stok_tanggal").val();
		data['kriteria'] = $("#riwayat_penyesuaian_stok_kriteria").val();
		showLoading();
		$.post(
			"",
			data,
			function(response) {
				var json = getContent(response);
				if (json == null) return;
				$("#" + self.prefix + "_list").html(json.list);
				$("#" + self.prefix + "_pagination").html(json.pagination);
				dismissLoading();
			}
		);
	};
	
	var riwayat_penyesuaian_stok;
	$(document).ready(function() {
		$('[data-toggle="popover"]').popover({
			trigger: 'hover',
			'placement': 'top'
		});
		riwayat_penyesuaian_stok = new RiwayatPenyesuaianStokAction(
			"riwayat_penyesuaian_stok", 
			"gudang_umum", 
			"riwayat_penyesuaian_stok_barang",
			new Array()
		);
		riwayat_penyesuaian_stok.view();
	});
</script>