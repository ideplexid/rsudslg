<?php 
	$stok_barang_table = new Table(
		array("Nomor", "Nama Barang", "Jenis Barang", "Produsen", "Vendor", "Sisa", "Harga Satuan", "Ket. Jumlah", "Tgl. Exp."),
		"",
		null,
		true
	);
	$stok_barang_table->setName("stok_barang_t");
	$stok_barang_table->setReloadButtonEnable(false);
	$stok_barang_table->setPrintButtonEnable(false);
	$stok_barang_table->setAddButtonEnable(false);
	$stok_barang_table->setAction(false);
	
	if (isset($_POST['command'])) {
		class StokBarangAdapter extends ArrayAdapter {
			public function adapt($row) {
				$array = array();
				$array['id'] = $row->id;
				$array['Nomor'] = self::format("digit8", $row->id);
				$array['Nama Barang'] = $row->nama_barang;
				$array['Jenis Barang'] = $row->nama_jenis_barang;
				$array['Produsen'] = $row->produsen;
				$array['Vendor'] = $row->nama_vendor;
				$array['Sisa'] = $row->sisa . " " . $row->satuan;
				$array['Harga Satuan'] = self::format("money Rp.", $row->hna);
				$array['Ket. Jumlah'] = "1 " . $row->satuan . " = " . $row->konversi . " " . $row->satuan_konversi;
				if ($row->tanggal_exp == "0000-00-00")
					$array['Tgl. Exp.'] = "-";
				else
					$array['Tgl. Exp.'] = self::format("date d M Y", $row->tanggal_exp);
				return $array;
			}
		}
		$stok_barang_adapter = new StokBarangAdapter();
		$columns = array("id", "id_dbarang_masuk", "nama_barang", "nama_jenis_barang", "jumlah", "sisa", "satuan", "konversi", "satuan_konversi", "hna", "tanggal_exp", "id_vendor", "nama_vendor", "produsen");
		$stok_barang_dbtable = new DBTable(
			$db,
			"smis_gd_stok_barang",
			$columns
		);
		$stok_barang_dbtable->addCustomKriteria(" sisa ", " > 0 ");
		$stok_barang_dbresponder = new DBResponder(
			$stok_barang_dbtable,
			$stok_barang_table,
			$stok_barang_adapter
		);
		$data = $stok_barang_dbresponder->command($_POST['command']);
		echo json_encode($data);
		return;
	}
	
	echo $stok_barang_table->getHtml();
	echo addJS("framework/smis/js/table_action.js");
?>
<script type="text/javascript">
	var stok_barang_t;
	$(document).ready(function() {
		$('[data-toggle="popover"]').popover({
			trigger: 'hover',
			'placement': 'top'
		});
		var stok_barang_t_columns = new Array("id", "id_dbarang_masuk", "nama_barang", "nama_jenis_barang", "jumlah", "sisa", "satuan", "konversi", "satuan_konversi", "hna", "tanggal_exp", "id_vendor", "nama_vendor", "produsen");
		stok_barang_t = new TableAction(
			"stok_barang_t",
			"gudang_umum",
			"daftar_stok_barang_tersimpan",
			stok_barang_t_columns
		);
		stok_barang_t.view();
	});
</script>