<?php 
	global $db;
	require_once("smis-base/smis-include-service-consumer.php");
	
	$laporan_form = new Form("lokr_form", "", "Gudang Umum : Laporan Mutasi Barang Per Ruangan");
	class UnitBarangServiceConsumer extends ServiceConsumer {
		public function __construct($db) {
			parent::__construct($db, "is_inventory");
		}		
		public function proceedResult() {
			$content = array();
			$result = json_decode($this->result, true);
			foreach($result as $autonomous) {
				foreach($autonomous as $entity=>$res) {
					if($res['value_barang']=="1"){
						$option = array();
						$option['value'] = $entity;
						$option['name'] = ArrayAdapter::format("unslug", $res['name']);
						$content[] = $option;
					}
				}
			}
			return $content;
		}
	}
	$unit_barang_service_consumer = new UnitBarangServiceConsumer($db);
	$unit_barang_service_consumer->execute();
	$unit_option = $unit_barang_service_consumer->getContent();
	$unit_select = new Select("lokr_unit", "lokr_unit", $unit_option);
	$laporan_form->addElement("Ruangan", $unit_select);
	$tanggal_from_text = new Text("lokr_tanggal_from", "lokr_tanggal_from", "");
	$tanggal_from_text->setClass("mydate");
	$tanggal_from_text->setAtribute("data-date-format='yyyy-mm-dd'");
	$laporan_form->addElement("Waktu Awal", $tanggal_from_text);
	$tanggal_to_text = new Text("lokr_tanggal_to", "lokr_tanggal_to", "");
	$tanggal_to_text->setClass("mydate");
	$tanggal_to_text->setAtribute("data-date-format='yyyy-mm-dd'");
	$laporan_form->addElement("Waktu Akhir", $tanggal_to_text);
	$show_button = new Button("", "", "Tampilkan");
	$show_button->setClass("btn-primary");
	$show_button->setIcon("icon-white icon-repeat");
	$show_button->setIsButton(Button::$ICONIC);
	$show_button->setAction("lokr.view()");
	$print_button = new Button("", "", "Cetak");
	$print_button->setClass("btn-inverse");
	$print_button->setIcon("icon-white icon-print");
	$print_button->setIsButton(Button::$ICONIC);
	$print_button->setAction("lokr.print()");
	$btn_group = new ButtonGroup("noprint");
	$btn_group->addButton($show_button);
	$btn_group->addButton($print_button);
	$laporan_form->addElement("", $btn_group);
	
	$lokr_table = new Table(
		array("No. Mutasi", "Nama Barang", "Jumlah", "Satuan", "H. Netto", "Tot. H. Netto", "Tgl. Exp."),
		"",
		null,
		true
	);
	$lokr_table->setName("lokr");
	$lokr_table->setAction(false);
	
	if (isset($_POST['command'])) {
		class LOKAdapter extends ArrayAdapter {
			public function adapt($row) {
				$array = array();
				$array['id'] = $row->id;
				$array['No. Mutasi'] = self::format("digit8", $row->id);
				$array['Nama Barang'] = $row->nama_barang;
				$array['Jumlah'] = self::format("number", $row->jumlah);
				$array['Satuan'] = $row->satuan;
				$array['H. Netto'] = self::format("money Rp. ", $row->hna);
				$array['Tot. H. Netto'] = self::format("money Rp. ", $row->jumlah * $row->hna);
				$array['Tgl. Exp.'] = self::format("date d-m-Y", $row->tanggal_exp);
				return $array;
			}
		}
		$lokr_adapter = new LOKAdapter();		
		$lokr_dbtable = new DBTable($db, "smis_gd_barang_keluar");
		if (isset($_POST['command']) == "list") {
			$filter = "tanggal >= '" . $_POST['tanggal_from'] . "' AND tanggal <= '" . $_POST['tanggal_to'] . "' AND unit = '" . $_POST['ruangan'] . "'";
			if (isset($_POST['kriteria'])) {
				$filter .= " AND (id LIKE '%" . $_POST['kriteria'] . "%' OR unit LIKE '%" . $_POST['kriteria'] . "%' OR nama_barang LIKE '%" . $_POST['kriteria'] . "%' OR satuan LIKE '%" . $_POST['kriteria'] . "%') ";
			}
			$query_value = "
				SELECT *
				FROM (
					SELECT smis_gd_barang_keluar.id, smis_gd_barang_keluar.tanggal, smis_gd_barang_keluar.unit, smis_gd_stok_barang.nama_barang, smis_gd_stok_barang_keluar.jumlah, smis_gd_stok_barang.satuan, smis_gd_stok_barang.hna, smis_gd_stok_barang.tanggal_exp
					FROM ((smis_gd_stok_barang_keluar LEFT JOIN smis_gd_stok_barang ON smis_gd_stok_barang_keluar.id_stok_barang = smis_gd_stok_barang.id) LEFT JOIN smis_gd_dbarang_keluar ON smis_gd_stok_barang_keluar.id_dbarang_keluar = smis_gd_dbarang_keluar.id) LEFT JOIN smis_gd_barang_keluar ON smis_gd_dbarang_keluar.id_barang_keluar = smis_gd_barang_keluar.id
					WHERE smis_gd_barang_keluar.status <> 'dikembalikan' AND smis_gd_dbarang_keluar.prop NOT LIKE 'del' AND smis_gd_stok_barang_keluar.jumlah <> 0
				) v_lokr
				WHERE " . $filter . "
			";
			$query_count = "
				SELECT COUNT(*)
				FROM (
					SELECT smis_gd_barang_keluar.id, smis_gd_barang_keluar.tanggal, smis_gd_barang_keluar.unit, smis_gd_stok_barang.nama_barang, smis_gd_stok_barang_keluar.jumlah, smis_gd_stok_barang.satuan, smis_gd_stok_barang.hna, smis_gd_stok_barang.tanggal_exp
					FROM ((smis_gd_stok_barang_keluar LEFT JOIN smis_gd_stok_barang ON smis_gd_stok_barang_keluar.id_stok_barang = smis_gd_stok_barang.id) LEFT JOIN smis_gd_dbarang_keluar ON smis_gd_stok_barang_keluar.id_dbarang_keluar = smis_gd_dbarang_keluar.id) LEFT JOIN smis_gd_barang_keluar ON smis_gd_dbarang_keluar.id_barang_keluar = smis_gd_barang_keluar.id
					WHERE smis_gd_barang_keluar.status <> 'dikembalikan' AND smis_gd_dbarang_keluar.prop NOT LIKE 'del' AND smis_gd_stok_barang_keluar.jumlah <> 0
				) v_lokr
				WHERE " . $filter . "
			";
			$lokr_dbtable->setPreferredQuery(true, $query_value, $query_count);
		}
		class LOKDBResponder extends DBResponder {
			public function command($command) {
				if ($command != "print_laporan") {
					return parent::command($command);
				}
				$pack = new ResponsePackage();
				if ($command == "print_laporan") {
					$content = $this->print_report();
					$pack->setContent($content);
					$pack->setStatus(ResponsePackage::$STATUS_OK);
				}
				return $pack->getPackage();
			}
			public function print_report() {
				$from = $_POST['tanggal_from'];
				$to = $_POST['tanggal_to'];
				$ruangan = $_POST['ruangan'];
				$data = $this->dbtable->get_result("
					SELECT *
					FROM (
						SELECT smis_gd_barang_keluar.id, smis_gd_barang_keluar.tanggal, smis_gd_barang_keluar.unit, smis_gd_stok_barang.nama_barang, smis_gd_stok_barang_keluar.jumlah, smis_gd_stok_barang.satuan, smis_gd_stok_barang.hna, smis_gd_stok_barang.tanggal_exp
						FROM ((smis_gd_stok_barang_keluar LEFT JOIN smis_gd_stok_barang ON smis_gd_stok_barang_keluar.id_stok_barang = smis_gd_stok_barang.id) LEFT JOIN smis_gd_dbarang_keluar ON smis_gd_stok_barang_keluar.id_dbarang_keluar = smis_gd_dbarang_keluar.id) LEFT JOIN smis_gd_barang_keluar ON smis_gd_dbarang_keluar.id_barang_keluar = smis_gd_barang_keluar.id
						WHERE smis_gd_barang_keluar.status <> 'dikembalikan' AND smis_gd_dbarang_keluar.prop NOT LIKE 'del' AND smis_gd_stok_barang_keluar.jumlah <> 0
					) v_lokr
					WHERE tanggal >= '" . $from . "' AND tanggal <= '" . $to . "' AND unit = '" . $_POST['ruangan'] . "'
				");
				$print_data = "";
				$print_data .= "<center><strong>LAPORAN MUTASI BARANG PER RUANGAN</strong></center><br/>";
				$print_data .= "<table border='0'>
									<tr>
										<td>Ruangan</td>
										<td>:</td>
										<td>" . ArrayAdapter::format("unslug", $ruangan) . "</td>
									</tr>
									<tr>
										<td>Dari</td>
										<td>:</td>
										<td>" . ArrayAdapter::format("date d M Y", $from) . "</td>
									</tr>
									<tr>
										<td>Sampai</td>
										<td>:</td>
										<td>" . ArrayAdapter::format("date d M Y", $to) . "</td>
									</tr>
								</table>";
				$print_data .= "<table border='1'>
									<tr>
										<th>No. Mutasi</th>
										<th>Nama Barang</th>
										<th>Jumlah</th>
										<th>Satuan</th>
										<th>HNA</th>
										<th>Total HNA</th>
										<th>Tgl. Exp.</th>
									</tr>";
				$total = 0;
				if (count($data) > 0) {
					foreach($data as $d) {
						$hna = $d->hna;
						$total_hna = $d->jumlah * $d->hna;
						$print_data .= "<tr>
											<td>" . ArrayAdapter::format("digit8", $d->id) . "</td>
											<td>" . $d->nama_barang . "</td>
											<td>" . ArrayAdapter::format("number", $d->jumlah) . "</td>
											<td>" . $d->satuan . "</td>
											<td>" . ArrayAdapter::format("money Rp. ",  $hna) . "</td>
											<td>" . ArrayAdapter::format("money Rp. ", $total_hna) . "</td>
											<td>" . ArrayAdapter::format("date d-m-Y", $d->tanggal_exp) . "</td>
										</tr>";
						$total += $total_hna;
					}
				} else {
					$print_data .= "<tr>
										<td colspan='7' align='center'><i>Tidak terdapat data mutasi barang</i></td>
									</tr>";
				}
				$print_data .= "<tr>
									<td colspan='5' align='center'><b>T O T A L</b></td>
									<td><b>Rp. " . ArrayAdapter::format("only-money", $total) . "</b></td>
									<td>&nbsp;</td>
								</tr>";
				$print_data .= "</table><br/>";
				global $user;
				$print_data .= "<table border='0' align='center' class='united'>
									<tr>
										<td align='center'>Jember, " . ArrayAdapter::format("date d M Y", date("Y-m-d")) . "</td>
										<td>&Tab;</td>
										<td>&nbsp;</td>
									</tr>
									<tr>
										<td align='center'>PETUGAS GUDANG UMUM</td>
										<td>&Tab;</td>
										<td align='center'>PETUGAS RUANGAN " .  ArrayAdapter::format("unslug", $ruangan)  . "</td>
									</tr>
									<tr>
										<td>&nbsp;</td>
										<td>&Tab;</td>
										<td>&nbsp;</td>
									</tr>
									<tr>
										<td>&nbsp;</td>
										<td>&Tab;</td>
										<td>&nbsp;</td>
									</tr>
									<tr>
										<td>&nbsp;</td>
										<td>&Tab;</td>
										<td>&nbsp;</td>
									</tr>
									<tr>
										<td>&nbsp;</td>
										<td>&Tab;</td>
										<td>&nbsp;</td>
									</tr>
									<tr>
										<td align='center'><b>" . $user->getNameOnly() . "</b></td>
										<td>&Tab;</td>
										<td align='center'>(_____________________)</td>
									</tr>
									<tr>
										<td>&nbsp;</td>
									</tr>
									<tr>
										<td colspan='3' align='center'>Mengetahui :</td>
									</tr>
									<tr>
										<td>&nbsp;</td>
									</tr>
									<tr>
										<td align='center' colspan='3'>KA. SUB. BAG. LOGISTIK & ASSET</td>
									</tr>
									<tr>
										<td colspan='3'>&nbsp;</td>
									</tr>
									<tr>
										<td colspan='3'>&nbsp;</td>
									</tr>
									<tr>
										<td colspan='3'>&nbsp;</td>
									</tr>
									<tr>
										<td colspan='3'>&nbsp;</td>
									</tr>
									<tr>
										<td align='center' colspan='3'><b>Suratmin</b></td>
									</tr>
								</table>";
				return $print_data;
			}
		}
		$lokr_dbresponder = new LOKDBResponder(
			$lokr_dbtable,
			$lokr_table,
			$lokr_adapter
		);
		$data = $lokr_dbresponder->command($_POST['command']);
		echo json_encode($data);
		return;
	}
	
	echo $laporan_form->getHtml();
	echo "<div id='table_content'>";
	echo $lokr_table->getHtml();
	echo "</div>";
	echo addJS("framework/smis/js/table_action.js");
	echo addJS("framework/bootstrap/js/bootstrap-datepicker.js");
	echo addCSS("framework/bootstrap/css/datepicker.css");
?>
<script type="text/javascript">
	function LOKRAction(name, page, action, column) {
		this.initialize(name, page, action, column);
	}
	LOKRAction.prototype.constructor = LOKRAction;
	LOKRAction.prototype = new TableAction();
	LOKRAction.prototype.view = function() {
		var self = this;
		var data = this.getViewData();
		data['tanggal_from'] = $("#lokr_tanggal_from").val();
		data['tanggal_to'] = $("#lokr_tanggal_to").val();
		data['ruangan'] = $("#lokr_unit").val();
		showLoading();
		$.post(
			"",
			data,
			function(response) {
				var json = getContent(response);
				if (json == null) return;
				$("#" + self.prefix + "_list").html(json.list);
				$("#" + self.prefix + "_pagination").html(json.pagination);
				dismissLoading();
			}
		);
	};
	LOKRAction.prototype.print = function() {
		if ($("#lokr_tanggal_from").val() == "" || $("#lokr_tanggal_to").val() == "" || $("#lokr_unit").val() == "")
			return;
		var data = this.getRegulerData();
		data['command'] = "print_laporan";
		data['tanggal_from'] = $("#lokr_tanggal_from").val();
		data['tanggal_to'] = $("#lokr_tanggal_to").val();
		data['ruangan'] = $("#lokr_unit").val();
		$.post(
			"",
			data,
			function(response) {
				var json = getContent(response);
				if (json == null) return;
				smis_print(json);
			}
		);
	};
	
	var lokr;
	$(document).ready(function() {
		$('[data-toggle="popover"]').popover({
			trigger: 'hover',
			'placement': 'top'
		});
		lokr = new LOKRAction(
			"lokr",
			"gudang_umum",
			"laporan_mutasi_barang_per_ruangan",
			new Array()
		)
		$('.mydate').datepicker();
	});
</script>