<?php 
	$response_package = new ResponsePackage();
	if (isset($_POST['command'])) {
		if ($_POST['command'] == "push_save") {
			$retur_barang_unit_dbtable = new DBTable($db, "smis_gd_retur_barang_unit");
			$retur_barang_unit_row = $retur_barang_unit_dbtable->get_row("
				SELECT id
				FROM smis_gd_retur_barang_unit
				WHERE f_id = '" . $_POST['id_retur_barang'] . "' AND unit = '" . $_POST['unit'] . "'
			");
			if ($retur_barang_unit_row == null) {
				//do insert:
				$retur_data = array();
				$retur_data['f_id'] = $_POST['id_retur_barang'];
				$retur_data['lf_id'] = $_POST['f_id'];
				$retur_data['unit'] = $_POST['unit'];
				$retur_data['tanggal'] = $_POST['tanggal'];
				$retur_data['id_barang'] = $_POST['id_barang'];
				$retur_data['nama_barang'] = $_POST['nama_barang'];
				$retur_data['nama_jenis_barang'] = $_POST['nama_jenis_barang'];
				$retur_data['id_vendor'] = $_POST['id_vendor'];
				$retur_data['nama_vendor'] = $_POST['nama_vendor'];
				$retur_data['jumlah'] = $_POST['jumlah'];
				$retur_data['satuan'] = $_POST['satuan'];
				$retur_data['konversi'] = $_POST['konversi'];
				$retur_data['satuan_konversi'] = $_POST['satuan_konversi'];
				$retur_data['hna'] = $_POST['hna'];
				$retur_data['produsen'] = $_POST['produsen'];
				$retur_data['tanggal_exp'] = $_POST['tanggal_exp'];
				$retur_data['turunan'] = $_POST['turunan'];
				$retur_data['keterangan'] = $_POST['keterangan'];
				$retur_data['status'] = $_POST['status'];
				$retur_barang_unit_dbtable->insert($retur_data);
				$retur_barang_unit_id = $retur_barang_unit_dbtable->get_inserted_id();
				//notify:
				global $notification;
				$key=md5($_POST['unit']." ".$retur_barang_unit_id);
				$msg="Retur Barang dari <strong>".ArrayAdapter::format("unslug", $_POST['unit'])."</strong>";
				$notification->addNotification("Retur Barang Unit", $key, $msg, "gudang_umum","retur_barang_unit");
			} else {
				//do update:
				$retur_data = array();
				$retur_data['f_id'] = $_POST['id_retur_barang'];
				$retur_data['lf_id'] = $_POST['f_id'];
				$retur_data['unit'] = $_POST['unit'];
				$retur_data['tanggal'] = $_POST['tanggal'];
				$retur_data['id_barang'] = $_POST['id_barang'];
				$retur_data['nama_barang'] = $_POST['nama_barang'];
				$retur_data['nama_jenis_barang'] = $_POST['nama_jenis_barang'];
				$retur_data['id_vendor'] = $_POST['id_vendor'];
				$retur_data['nama_vendor'] = $_POST['nama_vendor'];
				$retur_data['jumlah'] = $_POST['jumlah'];
				$retur_data['satuan'] = $_POST['satuan'];
				$retur_data['konversi'] = $_POST['konversi'];
				$retur_data['satuan_konversi'] = $_POST['satuan_konversi'];
				$retur_data['hna'] = $_POST['hna'];
				$retur_data['produsen'] = $_POST['produsen'];
				$retur_data['tanggal_exp'] = $_POST['tanggal_exp'];
				$retur_data['turunan'] = $_POST['turunan'];
				$retur_data['keterangan'] = $_POST['keterangan'];
				$retur_data['status'] = $_POST['status'];
				$retur_id['id'] = $retur_barang_unit_row->id;
				$retur_barang_unit_dbtable->update($retur_data, $retur_id);
			}
			$response_package->setStatus(ResponsePackage::$STATUS_OK);
		} else if ($_POST['command'] == "push_delete") {
			$retur_barang_unit_dbtable = new DBTable($db, "smis_ap_retur_barang_unit");
			$retur_barang_unit_row = $retur_barang_unit_dbtable->get_row("
				SELECT id
				FROM smis_gd_retur_barang_unit
				WHERE f_id = '" . $_POST['id_retur_barang'] . "' AND unit = '" . $_POST['unit'] . "'
			");
			if ($retur_barang_unit_row != null) {
				$retur_barang_unit_id['id'] = $retur_barang_unit_row->id;
				$retur_barang_unit_data['prop'] = "del";
				$retur_barang_unit_dbtable->update($retur_barang_unit_data, $retur_barang_unit_id);
			}
			$response_package->setStatus(ResponsePackage::$STATUS_OK);
		} else {
			$response_package->setStatus(ResponsePackage::$STATUS_FAIL);
		}
	} else {
		$response_package->setStatus(ResponsePackage::$STATUS_FAIL);
	}
	echo json_encode($response_package->getPackage());
?>