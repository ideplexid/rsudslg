<?php
	require_once("smis-base/smis-include-service-consumer.php");
	global $db;

	if (isset($_POST['data'])) {
		$id = $_POST['data'];
		$dbtable = new DBTable($db, "smis_gd_barang_masuk");
		$header_row = $dbtable->get_row("
			SELECT *
			FROM smis_gd_barang_masuk
			WHERE id = '" . $id . "'
		");

		$header = array();
		$list_debit_kredit = array();

		if ($header_row != null) {
			$kode_hutang = "";
			$kode_ppn = "";
			$kode_materai = "";
			$kode_ekspedisi = "";
			$kode_diskon_pembelian = "";
			$kode_pembelian = "";

			$data['id'] = $header_row->id_vendor;
			$service_consumer = new ServiceConsumer(
				$db,
				"get_kode_acc_vendor",
				$data,
				"pembelian"
			);
			$content = $service_consumer->execute()->getContent();
			if ($content != null) {
				$kode_hutang = $content['kode_hutang'];
				$kode_ppn = $content['kode_ppn'];
				$kode_diskon_pembelian = $content['kode_diskon_pembelian'];
				$kode_pembelian = $content['kode_pembelian'];
				$kode_materai = $content['kode_materai'];
				$kode_ekspedisi = $content['kode_ekspedisi'];
			}

			$total_hutang = 0;
			$total_diskon = 0;
			$total_pembelian = 0;
			$total_materai = 0;
			$total_ekspedisi = 0;
			$drows = $dbtable->get_result("
				SELECT *
				FROM smis_gd_dbarang_masuk
				WHERE id_barang_masuk = '" . $id . "' AND prop NOT LIKE 'del'
			");
			if ($drows != null) {
				foreach ($drows as $dr) {
					$subtotal = $dr->jumlah * ($dr->hna / 1.1);
					$diskon = $dr->diskon;
					if ($dr->t_diskon == "persen")
						$diskon = $subtotal * $dr->diskon / 100;
					$total_diskon += $diskon;
					$subtotal = round($subtotal - $diskon);
					$total_hutang += $subtotal;
					$total_pembelian += $subtotal;
				}
			}
			$global_diskon = $header_data['diskon'];
			if ($header_data['t_diskon'] == "persen")
				$global_diskon = $global_diskon * $total_hutang / 100;
			$global_diskon = floor($global_diskon);
			$total_diskon += $global_diskon;
			$total_hutang = $total_hutang - $global_diskon;
			$total_pembelian = $total_pembelian - $global_diskon;
			$ppn = floor($total_hutang * 0.1);
			$total_materai = $header_row->materai;
			$total_ekspedisi = $header_row->ekspedisi;
			$total_pembelian = $total_pembelian - $global_diskon + $ppn + $total_materai + $total_ekspedisi;

			$total_debit = 0;
			$total_kredit = 0;
			$data_debit_kredit = array(
				"akun"		=> $kode_hutang,
				"debet"		=> 0,
				"kredit"	=> $total_hutang,
				"ket"		=> "Hutang BBM No. Faktur " . $header_row->no_faktur . " -  ID Transaksi " . $id,
				"code"		=> "kredit-gudang_umum-hutang_bbm-" . $id
			);
			$list_debit_kredit[] = $data_debit_kredit;
			$total_debit += $data_debit_kredit['debet'];
			$total_kredit += $data_debit_kredit['kredit'];

			$data_debit_kredit = array(
				"akun"		=> $kode_ppn,
				"debet"		=> 0,
				"kredit"	=> $ppn,
				"ket"		=> "PPn BBM No. Faktur " . $header_row->no_faktur . " -  ID Transaksi " . $id,
				"code"		=> "kredit-gudang_umum-ppn_bbm-" . $id
			);
			$list_debit_kredit[] = $data_debit_kredit;
			$total_debit += $data_debit_kredit['debet'];
			$total_kredit += $data_debit_kredit['kredit'];

			$data_debit_kredit = array(
				"akun"		=> $kode_materai,
				"debet"		=> 0,
				"kredit"	=> $total_materai,
				"ket"		=> "Materai BBM No. Faktur " . $header_row->no_faktur . " -  ID Transaksi " . $id,
				"code"		=> "kredit-gudang_umum-materai_bbm-" . $id
			);
			$list_debit_kredit[] = $data_debit_kredit;
			$total_debit += $data_debit_kredit['debet'];
			$total_kredit += $data_debit_kredit['kredit'];

			$data_debit_kredit = array(
				"akun"		=> $kode_ekspedisi,
				"debet"		=> 0,
				"kredit"	=> $total_ekspedisi,
				"ket"		=> "Bea Kirim BBM No. Faktur " . $header_row->no_faktur . " -  ID Transaksi " . $id,
				"code"		=> "kredit-gudang_umum-ekspedisi_bbm-" . $id
			);
			$list_debit_kredit[] = $data_debit_kredit;
			$total_debit += $data_debit_kredit['debet'];
			$total_kredit += $data_debit_kredit['kredit'];

			$data_debit_kredit = array(
				"akun"		=> $kode_diskon_pembelian,
				"debet"		=> $total_diskon,
				"kredit"	=> 0,
				"ket"		=> "Diskon Pembelian BBM No. Faktur " . $header_row->no_faktur . " -  ID Transaksi " . $id,
				"code"		=> "debit-gudang_umum-diskon_bbm-" . $id
			);
			$list_debit_kredit[] = $data_debit_kredit;
			$total_debit += $data_debit_kredit['debet'];
			$total_kredit += $data_debit_kredit['kredit'];

			$data_debit_kredit = array(
				"akun"		=> $kode_pembelian,
				"debet"		=> $total_pembelian,
				"kredit"	=> 0,
				"ket"		=> "Persediaan BBM No. Faktur " . $header_row->no_faktur . " -  ID Transaksi " . $id,
				"code"		=> "debit-gudang_umum-pembelian_bbm-" . $id
			);
			$list_debit_kredit[] = $data_debit_kredit;
			$total_debit += $data_debit_kredit['debet'];
			$total_kredit += $data_debit_kredit['kredit'];

			$header = array(
				"tanggal"		=> $header_row->tanggal_datang,
				"keterangan"	=> "BBM No. Faktur " . $header_row->no_faktur . " -  ID Transaksi " . $id,
				"code"			=> "bbm-gudang_umum-" . $id,
				"nomor"			=> $header_row->no_faktur,
				"debet"			=> $total_debit,
				"kredit"		=> $total_kredit,
				"io"			=> 0
			);
		}

		$transaction = array(
			"header"	=> $header,
			"content"	=> $list_debit_kredit
		);
		$data_final = array();
		$data_final[] = $transaction;
		echo json_encode($data_final);
	}
?>