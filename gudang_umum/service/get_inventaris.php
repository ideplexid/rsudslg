<?php 
	global $db;
	
	if (isset($_POST['dari']) && isset($_POST['sampai'])) {
		$inventaris_dbtable = new DBTable($db, "smis_gd_inventaris");
		$inventaris_rows = $inventaris_dbtable->get_result("
			SELECT *
			FROM smis_gd_inventaris
			WHERE prop NOT LIKE 'del' AND tahun_perolehan >= '" . $_POST['dari'] . "' AND tahun_perolehan <= '" . $_POST['sampai'] . "'
		");
		$data = array();
		foreach($inventaris_rows as $ir) {
			$inventaris = array();
			if ($ir->unit == null)
				$inventaris['ruang'] = ArrayAdapter::format("unslug", "gudang_umum");
			else
				$inventaris['raung'] = ArrayAdapter::format("unslug", $ir->unit);
			$inventaris['barang'] = $ir->nama_barang;
			$inventaris['kode'] = $ir->kode;
			$inventaris['harga'] = $ir->hna;
			$inventaris['masuk'] = $ir->tahun_perolehan;
			$inventaris['durasi'] = $ir->usia_penyusutan;
			$inventaris['kondisi'] = $ir->kondisi_baik == 1 ? "Baik":"Rusak";
			$data[] = $inventaris;
		}
		$response = array();
		$response['data'] = $data;
		echo json_encode($response);
	}
?>