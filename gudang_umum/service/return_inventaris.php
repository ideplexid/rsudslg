<?php 
	$response_package = new ResponsePackage();
	if (isset($_POST['command'])) {
		if ($_POST['command'] == "push_return") {
			//do insert riwayat inventaris:
			$inventaris_dbtable = new DBTable($db, "smis_gd_inventaris");
			$id['id'] = $_POST['f_id'];
			$data['unit'] = "";
			$row = $inventaris_dbtable->get_row("
				SELECT *
				FROM smis_gd_inventaris
				WHERE id = '" . $id['id'] . "'
			");
			$nama_user = $_POST['nama_user'];
			$riwayat_inventaris_dbtable = new DBTable($db, "smis_gd_riwayat_inventaris");
			$riwayat_inventaris_data = array();
			$riwayat_inventaris_data['id_inventaris'] = $row->id;
			$riwayat_inventaris_data['unit'] = $row->unit;
			$riwayat_inventaris_data['tanggal'] = date("Y-m-d");
			$riwayat_inventaris_data['kode'] = $row->kode;
			$riwayat_inventaris_data['merk'] = $row->merk;
			$riwayat_inventaris_data['usia_penyusutan'] = $row->usia_penyusutan;
			$riwayat_inventaris_data['kondisi_baik'] = $row->kondisi_baik;
			$riwayat_inventaris_data['status'] = "retur";
			$riwayat_inventaris_data['nama_user'] = $nama_user;
			$riwayat_inventaris_dbtable->insert($riwayat_inventaris_data);
			//do update inventaris:
			$inventaris_dbtable->update($data, $id);
			
			$response_package->setStatus(ResponsePackage::$STATUS_OK);
			//notify:
			global $notification;
			$key=md5($row->unit." ".$row->id);
			$msg="Retur Inventaris <strong>" . $row->nama_barang . "</strong> dari <strong>".ArrayAdapter::format("unslug", $row->unit)."</strong>";
			$notification->addNotification("Retur Inventaris", $key, $msg, "gudang_umum","daftar_barang_belum_terinventarisasi");
		} else {
			$response_package->setStatus(ResponsePackage::$STATUS_FAIL);
		}
	} else {
		$response_package->setStatus(ResponsePackage::$STATUS_FAIL);
	}
	echo json_encode($response_package->getPackage());
?>