<?php 
	$response_package = new ResponsePackage();
	if (isset($_POST['command'])) {
		if ($_POST['command'] == "push_save") {
			$permintaan_barang_dbtable = new DBTable($db, "smis_gd_permintaan_barang_unit");
			$permintaan_barang_row = $permintaan_barang_dbtable->get_row("
				SELECT id
				FROM smis_gd_permintaan_barang_unit
				WHERE f_id = '" . $_POST['id_permintaan_barang'] . "' AND unit = '" . $_POST['unit'] . "'
			");
			if ($permintaan_barang_row == null) {
				//do insert:
				$permintaan_barang_data = array();
				$permintaan_barang_data['f_id'] = $_POST['id_permintaan_barang'];
				$permintaan_barang_data['unit'] = $_POST['unit'];
				$permintaan_barang_data['tanggal'] = $_POST['tanggal'];
				$permintaan_barang_data['status'] = $_POST['status'];
				$permintaan_barang_dbtable->insert($permintaan_barang_data);
				$permintaan_barang_id = $permintaan_barang_dbtable->get_inserted_id();
				$permintaan_barang_detail = json_decode($_POST['detail'], true);
				$dpermintaan_barang_dbtable = new DBTable($db, "smis_gd_dpermintaan_barang_unit");
				foreach($permintaan_barang_detail as $dpermintaan_barang) {
					$dpermintaan_barang_data = array();
					$dpermintaan_barang_data['f_id'] = $dpermintaan_barang['id'];
					$dpermintaan_barang_data['id_permintaan_barang'] = $permintaan_barang_id;
					$dpermintaan_barang_data['nama_barang'] = $dpermintaan_barang['nama_barang'];
					$dpermintaan_barang_data['jumlah_permintaan'] = $dpermintaan_barang['jumlah_permintaan'];
					$dpermintaan_barang_data['satuan_permintaan'] = $dpermintaan_barang['satuan_permintaan'];
					$dpermintaan_barang_data['jumlah_dipenuhi'] = $dpermintaan_barang['jumlah_dipenuhi'];
					$dpermintaan_barang_data['satuan_dipenuhi'] = $dpermintaan_barang['satuan_dipenuhi'];
					$dpermintaan_barang_data['keterangan'] = $dpermintaan_barang['keterangan'];
					$dpermintaan_barang_dbtable->insert($dpermintaan_barang_data);
				}
				$response_package->setStatus(ResponsePackage::$STATUS_OK);
				//notify:
				global $notification;
				$key=md5($_POST['unit']." ".$permintaan_barang_id);
				$msg="Permintaan Barang dari <strong>".ArrayAdapter::format("unslug", $_POST['unit'])."</strong> masuk";
				$notification->addNotification("Permintaan Barang Unit Masuk", $key, $msg, "gudang_umum","permintaan_barang_unit");
				$notification->commit();
			} else {
				//do update:
				$permintaan_barang_id_data['id'] = $permintaan_barang_row->id;
				$permintaan_barang_data = array();
				$permintaan_barang_data['tanggal'] = $_POST['tanggal'];
				$permintaan_barang_data['status'] = $_POST['status'];
				$permintaan_barang_dbtable->update($permintaan_barang_data, $permintaan_barang_id_data);
				$permintaan_barang_detail = json_decode($_POST['detail'], true);
				$dpermintaan_barang_dbtable = new DBTable($db, "smis_gd_dpermintaan_barang_unit");
				foreach($permintaan_barang_detail as $dpermintaan_barang) {
					$dpermintaan_barang_row = $dpermintaan_barang_dbtable->get_row("
						SELECT id
						FROM smis_gd_dpermintaan_barang_unit
						WHERE f_id = '" . $dpermintaan_barang['id'] . "' AND id_permintaan_barang = '" . $permintaan_barang_row->id . "'
					");
					if ($dpermintaan_barang_row == null) {
						//do insert if f_id not exist:
						$dpermintaan_barang_data = array();
						$dpermintaan_barang_data['f_id'] = $dpermintaan_barang['id'];
						$dpermintaan_barang_data['id_permintaan_barang'] = $permintaan_barang_id;
						$dpermintaan_barang_data['nama_barang'] = $dpermintaan_barang['nama_barang'];
						$dpermintaan_barang_data['jumlah_permintaan'] = $dpermintaan_barang['jumlah_permintaan'];
						$dpermintaan_barang_data['satuan_permintaan'] = $dpermintaan_barang['satuan_permintaan'];
						$dpermintaan_barang_data['jumlah_dipenuhi'] = $dpermintaan_barang['jumlah_dipenuhi'];
						$dpermintaan_barang_data['satuan_dipenuhi'] = $dpermintaan_barang['satuan_dipenuhi'];
						$dpermintaan_barang_data['keterangan'] = $dpermintaan_barang['keterangan'];
						$dpermintaan_barang_dbtable->insert($dpermintaan_barang_data);
					} else {
						//do update or delete if f_id exist:
						$dpermintaan_barang_id_data['id'] = $dpermintaan_barang_row->id;
						$dpermintaan_barang_data = array();
						$dpermintaan_barang_data['nama_barang'] = $dpermintaan_barang['nama_barang'];
						$dpermintaan_barang_data['jumlah_permintaan'] = $dpermintaan_barang['jumlah_permintaan'];
						$dpermintaan_barang_data['satuan_permintaan'] = $dpermintaan_barang['satuan_permintaan'];
						$dpermintaan_barang_data['prop'] = $dpermintaan_barang['prop'];
						$dpermintaan_barang_dbtable->update($dpermintaan_barang_data, $dpermintaan_barang_id_data);
					}
				}
				$response_package->setStatus(ResponsePackage::$STATUS_OK);
				//notify:
				global $notification;
				$key=md5($_POST['unit']." ".$permintaan_barang_row->id);
				$msg="Permintaan Barang dari <strong>".ArrayAdapter::format("unslug", $_POST['unit'])."</strong> diperbarui";
				$notification->addNotification("Permintaan Barang Unit Diperbarui", $key, $msg, "gudang_umum","permintaan_barang_unit");
				$notification->commit();
			}
		} else if ($_POST['command'] == "push_delete") {
			$permintaan_barang_dbtable = new DBTable($db, "smis_gd_permintaan_barang_unit");
			$permintaan_barang_row = $permintaan_barang_dbtable->get_row("
				SELECT id
				FROM smis_gd_permintaan_barang_unit
				WHERE f_id = '" . $_POST['id_permintaan_barang'] . "' AND unit = '" . $_POST['unit'] . "'
			");
			if ($permintaan_barang_row != null) {
				$permintaan_barang_id_data['id'] = $permintaan_barang_row->id;
				$permintaan_barang_data['prop'] = "del";
				$permintaan_barang_dbtable->update($permintaan_barang_data, $permintaan_barang_id_data);
				//notify:
				global $notification;
				$key=md5($_POST['unit']." ".$permintaan_barang_row->id);
				$msg="Permintaan Barang dari <strong>".ArrayAdapter::format("unslug", $_POST['unit'])."</strong> dibatalkan";
				$notification->addNotification("Permintaan Barang Unit Batal", $key, $msg, "gudang_umum","permintaan_barang_unit");
				$notification->commit();
			}
			$response_package->setStatus(ResponsePackage::$STATUS_OK);
		} else {
			$response_package->setStatus(ResponsePackage::$STATUS_FAIL);
		}
	} else {
		$response_package->setStatus(ResponsePackage::$STATUS_FAIL);
	}
	echo json_encode($response_package->getPackage());
?>
