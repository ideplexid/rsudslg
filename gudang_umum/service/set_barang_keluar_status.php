<?php 
	global $db;
	
	$response_package = new ResponsePackage();
	if (isset($_POST['command'])) {
		if ($_POST['command'] == "push_update") {
			$id['id'] = $_POST['id'];
			$data['status'] = $_POST['status'];
			$data['keterangan'] = $_POST['keterangan'];
			$barang_keluar_dbtable = new DBTable($db, "smis_gd_barang_keluar");
			$barang_keluar_dbtable->update($data, $id);
			$response_package->setStatus(ResponsePackage::$STATUS_OK);
			$row = $barang_keluar_dbtable->get_row("
				SELECT *
				FROM smis_gd_barang_keluar
				WHERE id = '" . $id['id'] . "'
			");
			if ($data['status'] == "dikembalikan") {
				// restock:
				$dbarang_keluar_rows = $barang_keluar_dbtable->get_result("
					SELECT *
					FROM smis_gd_dbarang_keluar
					WHERE id_barang_keluar = '" . $id['id'] . "' AND prop NOT LIKE 'del'
				");
				foreach ($dbarang_keluar_rows as $dokr) {
					$stok_barang_keluar_dbtable = new DBTable($db, "smis_gd_stok_barang_keluar");
					$stok_keluar_query = "
						SELECT *
						FROM smis_gd_stok_barang_keluar
						WHERE id_dbarang_keluar = '" . $dokr->id . "'
						ORDER BY id DESC
					";
					$stok_keluar = $stok_barang_keluar_dbtable->get_result($stok_keluar_query);
					foreach($stok_keluar as $sk) {
						$stok_keluar_data = array();
						$stok_keluar_data['jumlah'] = 0;
						$stok_keluar_data['prop'] = "del";
						$stok_keluar_id['id'] = $sk->id;
						$stok_barang_keluar_dbtable->update($stok_keluar_data, $stok_keluar_id);
						$stok_barang_dbtable = new DBTable($db, "smis_gd_stok_barang");
						$stok_query = "
							SELECT *
							FROM smis_gd_stok_barang
							WHERE id = '" . $sk->id_stok_barang . "'
						";
						$stok_barang_row = $stok_barang_dbtable->get_row($stok_query);
						$stok_data = array();
						$stok_data['sisa'] = $stok_barang_row->sisa + $sk->jumlah;
						$stok_id['id'] = $sk->id_stok_barang;
						$stok_dbtable = new DBTable($db, "smis_gd_stok_barang");
						$stok_dbtable->update($stok_data, $stok_id);
						//logging riwayat stok barang:
						$riwayat_dbtable = new DBTable($db, "smis_gd_riwayat_stok_barang");
						$data_riwayat = array();
						$data_riwayat['tanggal'] = date("Y-m-d");
						$data_riwayat['id_stok_barang'] = $sk->id_stok_barang;
						$data_riwayat['jumlah_masuk'] = $sk->jumlah;
						$data_riwayat['sisa'] = $stok_barang_row->sisa + $sk->jumlah;
						$data_riwayat['keterangan'] = "Pembatalan Stok Keluar ke " . ArrayAdapter::format("unslug", $row->unit);
						global $user;
						$data_riwayat['nama_user'] = $_POST['nama_user'];
						$riwayat_dbtable->insert($data_riwayat);
					}
				}
			}
			//notify:
			global $notification;
			$key=md5($row->unit." ".$row->id);
			$status = ArrayAdapter::format("unslug", $_POST['status']);
			if ($status == "SUDAH")
				$status .= " DITERIMA";
			$msg="Barang Keluar ke <strong>".ArrayAdapter::format("unslug", $row->unit)."</strong> sudah dikonfirmasi (<strong>" . $status . "</strong>)";
			$notification->addNotification("Konfirmasi Barang Keluar", $key, $msg, "gudang_umum","barang_keluar");
		} else {
			$response_package->setStatus(ResponsePackage::$STATUS_FAIL);
		}
	} else {
		$response_package->setStatus(ResponsePackage::$STATUS_FAIL);
	}
	echo json_encode($response_package->getPackage());	
?>