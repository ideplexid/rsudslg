<?php 
	require_once("smis-base/smis-include-duplicate.php");

	class StokBarangBTTable extends Table {
		public function getContentButton($id) {
			$btn_group = new ButtonGroup("noprint");
			$btn = new Button("", "", "Simpan Stok");
			$btn->setAction($this->action . ".edit('" . $id . "')");
			$btn->setClass("btn-warning");
			$btn->setAtribute("data-content='Simpan Stok' data-toggle='popover'");
			$btn->setIcon("icon-edit icon-white");
			$btn->setIsButton(Button::$ICONIC);
			$btn_group->addElement($btn);
			return $btn_group;
		}
	}
	$stok_barang_table = new StokBarangBTTable(
		array("Nomor", "Nama Barang", "Jenis Barang", "Produsen", "Vendor", "Jumlah", "Satuan", "Harga Satuan", "Ket. Jumlah"),
		"",
		null,
		true
	);
	$stok_barang_table->setName("stok_barang_bt");
	$stok_barang_table->setReloadButtonEnable(false);
	$stok_barang_table->setPrintButtonEnable(false);
	$stok_barang_table->setAddButtonEnable(false);
	
	if (isset($_POST['command'])) {
		class StokBarangAdapter extends ArrayAdapter {
			public function adapt($row) {
				$array = array();
				$array['id'] = $row->id;
				$array['Nomor'] = self::format("digit8", $row->id);
				$array['Nama Barang'] = $row->nama_barang;
				$array['Jenis Barang'] = $row->nama_jenis_barang;
				$array['Produsen'] = $row->produsen;
				$array['Vendor'] = $row->nama_vendor;
				$array['Jumlah'] = $row->sisa;
				$array['Satuan'] = $row->satuan;
				$array['Harga Satuan'] = self::format("money Rp.", ($row->hna + $row->selisih));
				$array['Ket. Jumlah'] = "1 " . $row->satuan . " = " . $row->konversi . " " . $row->satuan_konversi;
				return $array;
			}
		}
		$stok_barang_adapter = new StokBarangAdapter();
		$columns = array("id", "nama_barang", "nama_jenis_barang", "sisa", "satuan", "konversi", "satuan_konversi", "hna", "id_vendor", "nama_vendor", "produsen");
		$stok_barang_dbtable = new DBTable(
			$db,
			"smis_gd_dbarang_masuk",
			$columns
		);
		$filter = "";
		if (isset($_POST['kriteria'])) {
			$filter = " AND  (smis_gd_dbarang_masuk.nama_barang LIKE '%" . $_POST['kriteria'] . "%')";
		}
		$query_value = "
			SELECT smis_gd_dbarang_masuk.*, smis_gd_barang_masuk.nama_vendor
			FROM smis_gd_dbarang_masuk LEFT JOIN smis_gd_barang_masuk ON smis_gd_dbarang_masuk.id_barang_masuk = smis_gd_barang_masuk.id
			WHERE smis_gd_dbarang_masuk.prop NOT LIKE 'del' AND smis_gd_dbarang_masuk.inventaris = 0 AND smis_gd_dbarang_masuk.sisa > 0 " . $filter . "
			ORDER BY smis_gd_dbarang_masuk.nama_barang ASC
		";
		$query_count = "
			SELECT COUNT(*)
			FROM smis_gd_dbarang_masuk
			WHERE smis_gd_dbarang_masuk.prop NOT LIKE 'del' AND smis_gd_dbarang_masuk.inventaris = 0 AND smis_gd_dbarang_masuk.sisa > 0 " . $filter . "
		";
		$stok_barang_dbtable->setPreferredQuery(true, $query_value, $query_count);
		class StokBarangDBResponder extends DuplicateResponder {
			public function save() {
				//do update sisa dbarang masuk:
				$dbarang_masuk_data = array();
				$dbarang_masuk_data['sisa'] = $_POST['sisa'];
				$dbarang_masuk_data['autonomous'] = "[".$this->getAutonomous()."]";
		        $dbarang_masuk_data['duplicate'] = 0;
		        $dbarang_masuk_data['time_updated'] = date("Y-m-d H:i:s");
		        $dbarang_masuk_data['origin_updated'] = $this->getAutonomous();
				$dbarang_masuk_id['id'] = $_POST['id'];
				$result = $this->dbtable->update($dbarang_masuk_data, $dbarang_masuk_id);
				$success['type'] = "update";
				//do insert stok barang:
				$stok_barang_dbtable = new DBTable($this->dbtable->get_db(), "smis_gd_stok_barang");
				$stok_barang_data = array();
				$stok_barang_data['id_dbarang_masuk'] = $dbarang_masuk_id['id'];
				$stok_barang_data['nama_barang'] = $_POST['nama_barang'];
				$stok_barang_data['nama_jenis_barang'] = $_POST['nama_jenis_barang'];
				$stok_barang_data['jumlah'] = $_POST['jumlah'];
				$stok_barang_data['sisa'] = $_POST['jumlah'];
				$stok_barang_data['satuan'] = $_POST['satuan'];
				$stok_barang_data['konversi'] = $_POST['konversi'];
				$stok_barang_data['satuan_konversi'] = $_POST['satuan_konversi'];
				$stok_barang_data['hna'] = $_POST['hna'];
				$stok_barang_data['produsen'] = $_POST['produsen'];
				$stok_barang_data['tanggal_exp'] = $_POST['tanggal_exp'];
				$stok_barang_data['id_vendor'] = $_POST['id_vendor'];
				$stok_barang_data['nama_vendor'] = $_POST['nama_vendor'];
				$stok_barang_data['autonomous'] = "[".$this->getAutonomous()."]";
		        $stok_barang_data['duplicate'] = 0;
		        $stok_barang_data['time_updated'] = date("Y-m-d H:i:s");
		        $stok_barang_data['origin_updated'] = $this->getAutonomous();
		        $stok_barang_data['origin'] = $this->getAutonomous();
				$stok_barang_dbtable->insert($stok_barang_data);
				$id_stok_barang = $stok_barang_dbtable->get_inserted_id();
				//logging riwayat stok barang:
				$riwayat_dbtable = new DBTable($this->dbtable->get_db(), "smis_gd_riwayat_stok_barang");
				$data_riwayat = array();
				$data_riwayat['tanggal'] = date("Y-m-d");
				$data_riwayat['id_stok_barang'] = $id_stok_barang;
				$data_riwayat['jumlah_masuk'] = $_POST['jumlah'];
				$data_riwayat['sisa'] = $_POST['jumlah'];
				$data_riwayat['keterangan'] = "Stok Simpan";
				global $user;
				$data_riwayat['nama_user'] = $user->getName();
				$data_riwayat['autonomous'] = "[".$this->getAutonomous()."]";
		        $data_riwayat['duplicate'] = 0;
		        $data_riwayat['time_updated'] = date("Y-m-d H:i:s");
		        $data_riwayat['origin_updated'] = $this->getAutonomous();
		        $data_riwayat['origin'] = $this->getAutonomous();
				$riwayat_dbtable->insert($data_riwayat);
				$success['id'] = $dbarang_masuk_id['id'];
				$success['success'] = 1;
				if ($result === false) $success['success'] = 0;
				return $success;
			}
			public function edit() {
				$id = $_POST['id'];
				$data = $this->dbtable->get_row("
					SELECT smis_gd_dbarang_masuk.*, smis_gd_barang_masuk.id_vendor, smis_gd_barang_masuk.nama_vendor
					FROM smis_gd_dbarang_masuk LEFT JOIN smis_gd_barang_masuk ON smis_gd_dbarang_masuk.id_barang_masuk = smis_gd_barang_masuk.id
					WHERE smis_gd_dbarang_masuk.id = '" . $id . "'
				");
				return $data;
			}
		}
		$stok_barang_dbresponder = new StokBarangDBResponder(
			$stok_barang_dbtable,
			$stok_barang_table,
			$stok_barang_adapter
		);
		$data = $stok_barang_dbresponder->command($_POST['command']);
		echo json_encode($data);
		return;
	}
	
	$stok_barang_modal = new Modal("stok_barang_bt_add_form", "smis_form_container", "stok_barang_bt");
	$stok_barang_modal->setTitle("Data Barang Masuk");
	$id_hidden = new Hidden("stok_barang_bt_id", "stok_barang_bt_id", "");
	$stok_barang_modal->addElement("", $id_hidden);
	$nama_barang_text = new Text("stok_barang_bt_nama_barang", "stok_barang_bt_nama_barang", "");
	$nama_barang_text->setAtribute("disabled='disabled'");
	$stok_barang_modal->addElement("Nama Barang", $nama_barang_text);
	$nama_jenis_barang_text = new Text("stok_barang_bt_nama_jenis_barang", "stok_barang_bt_nama_jenis_barang", "");
	$nama_jenis_barang_text->setAtribute("disabled='disabled'");
	$stok_barang_modal->addElement("Jenis Barang", $nama_jenis_barang_text);
	$sisa_hidden = new Hidden("stok_barang_bt_sisa", "stok_barang_bt_sisa", "");
	$stok_barang_modal->addElement("", $sisa_hidden);
	$satuan_hidden = new Hidden("stok_barang_bt_satuan", "stok_barang_bt_satuan", "");
	$stok_barang_modal->addElement("", $satuan_hidden);
	$konversi_hidden = new Hidden("stok_barang_bt_konversi", "stok_barang_bt_konversi", "");
	$stok_barang_modal->addElement("", $konversi_hidden);
	$satuan_konversi_hidden = new Hidden("stok_barang_bt_satuan_konversi", "stok_barang_bt_satuan_konversi", "");
	$stok_barang_modal->addElement("", $satuan_konversi_hidden);
	$hna_hidden = new Hidden("stok_barang_bt_hna", "stok_barang_bt_hna", "");
	$stok_barang_modal->addElement("", $hna_hidden);
	$produsen_text = new Text("stok_barang_bt_produsen", "stok_barang_bt_produsen", "");
	$produsen_text->setAtribute("disabled='disabled'");
	$stok_barang_modal->addElement("Produsen", $produsen_text);
	$id_vendor_hidden = new Hidden("stok_barang_bt_id_vendor", "stok_barang_bt_id_vendor", "");
	$stok_barang_modal->addElement("", $id_vendor_hidden);
	$nama_vendor_text = new Text("stok_barang_bt_nama_vendor", "stok_barang_bt_nama_vendor", "");
	$nama_vendor_text->setAtribute("disabled='disabled'");
	$stok_barang_modal->addElement("Vendor", $nama_vendor_text);
	$f_sisa_text = new Text("stok_barang_bt_f_sisa", "stok_barang_bt_f_sisa", "");
	$f_sisa_text->setAtribute("disabled='disabled'");
	$stok_barang_modal->addElement("Sisa", $f_sisa_text);
	$jumlah_text = new Text("stok_barang_bt_jumlah", "stok_barang_bt_jumlah", "");
	$stok_barang_modal->addElement("Jml. Simpan", $jumlah_text);
	$tanggal_exp_text = new Text("stok_barang_bt_tanggal_exp", "stok_barang_bt_tanggal_exp", "");
	$tanggal_exp_text->setClass("mydate");
	$tanggal_exp_text->setAtribute("data-date-format='yyyy-m-d'");
	$stok_barang_modal->addElement("Tgl. Exp.", $tanggal_exp_text);
	$stok_barang_button = new Button("", "", "Simpan");
	$stok_barang_button->setClass("btn-success");
	$stok_barang_button->setAction("stok_barang_bt.save()");
	$stok_barang_button->setIcon("fa fa-floppy-o");
	$stok_barang_button->setIsButton(Button::$ICONIC);
	$stok_barang_modal->addFooter($stok_barang_button);
	
	echo $stok_barang_modal->getHtml();
	echo $stok_barang_table->getHtml();
	echo addJS("framework/smis/js/table_action.js");
	echo addJS("framework/bootstrap/js/bootstrap-datepicker.js");
	echo addCSS("framework/bootstrap/css/datepicker.css");
?>
<script type="text/javascript">
	function StokBarangBTAction(name, page, action, column) {
		this.initialize(name, page, action, column);
	}
	StokBarangBTAction.prototype.constructor = StokBarangBTAction;
	StokBarangBTAction.prototype = new TableAction();
	StokBarangBTAction.prototype.edit = function(id) {
		var data = this.getRegulerData();
		data['command'] = "edit";
		data['id'] = id;
		$.post(
			"",
			data,
			function(response) {
				var json = getContent(response);
				if (json == null) return;
				$("#stok_barang_bt_id").val(json.id);
				$("#stok_barang_bt_nama_barang").val(json.nama_barang);
				$("#stok_barang_bt_nama_jenis_barang").val(json.nama_jenis_barang);
				$("#stok_barang_bt_sisa").val(json.sisa);
				$("#stok_barang_bt_satuan").val(json.satuan);
				$("#stok_barang_bt_konversi").val(json.konversi);
				$("#stok_barang_bt_satuan_konversi").val(json.satuan_konversi);
				var hna = parseFloat(json.hna) + parseFloat(json.selisih);
				$("#stok_barang_bt_hna").val(hna);
				$("#stok_barang_bt_produsen").val(json.produsen);
				$("#stok_barang_bt_id_vendor").val(json.id_vendor);
				$("#stok_barang_bt_nama_vendor").val(json.nama_vendor);
				$("#stok_barang_bt_f_sisa").val(json.sisa + " " + json.satuan);
				$("#stok_barang_bt_jumlah").val("");
				$("#modal_alert_stok_barang_bt_add_form").html("");
				$(".error_field").removeClass("error_field");
				$("#stok_barang_bt_add_form").smodal("show");
			}
		);
	};
	StokBarangBTAction.prototype.validate = function() {
		var valid = true;
		var invalid_msg = "";
		var jumlah = $("#stok_barang_bt_jumlah").val();
		var sisa = $("#stok_barang_bt_sisa").val();
		$(".error_field").removeClass("error_field");
		if (jumlah == "") {
			valid = false;
			invalid_msg += "</br><strong>Jumlah</strong> tidak boleh kosong";
			$("#stok_barang_bt_jumlah").addClass("error_field");
		} else if (!is_numeric(jumlah)) {
			valid = false;
			invalid_msg += "</br><strong>Jumlah</strong> seharusnya numerik (0-9)";
			$("#stok_barang_bt_jumlah").addClass("error_field");
		} else if (parseInt(jumlah) > parseInt(sisa)) {
			valid = false;
			invalid_msg += "</br><strong>Jumlah</strong> melebihi sisa";
			$("#stok_barang_bt_jumlah").addClass("error_field");
		}
		if (!valid) {
			$("#modal_alert_stok_barang_bt_add_form").html(
				"<div class='alert alert-block alert-danger'>" +
					"<h4>Peringatan</h4>" +
					invalid_msg +
				"</div>"
			);
		}
		return valid;
	};
	StokBarangBTAction.prototype.save = function() {
		if (!this.validate()) {
			return;
		}
		$("#stok_barang_bt_add_form").smodal("hide");
		showLoading();
		var self = this;
		var data = this.getRegulerData();
		data['command'] = "save";
		data['id'] = $("#stok_barang_bt_id").val();
		data['sisa'] = parseInt($("#stok_barang_bt_sisa").val()) - parseInt($("#stok_barang_bt_jumlah").val());
		data['nama_barang'] = $("#stok_barang_bt_nama_barang").val();
		data['nama_jenis_barang'] = $("#stok_barang_bt_nama_jenis_barang").val();
		data['jumlah'] = $("#stok_barang_bt_jumlah").val();
		data['satuan'] = $("#stok_barang_bt_satuan").val();
		data['konversi'] = $("#stok_barang_bt_konversi").val();
		data['satuan_konversi'] = $("#stok_barang_bt_satuan_konversi").val();
		data['hna'] = $("#stok_barang_bt_hna").val();
		data['produsen'] = $("#stok_barang_bt_produsen").val();
		data['tanggal_exp'] = $("#stok_barang_bt_tanggal_exp").val();
		data['id_vendor'] = $("#stok_barang_bt_id_vendor").val();
		data['nama_vendor'] = $("#stok_barang_bt_nama_vendor").val();
		$.post(
			"",
			data,
			function(response) {
				var json = getContent(response);
				if (json == null) {
					$("#stok_barang_bt_add_form").smodal("show");
				} else {
					self.view();
					stok_barang_t.view();
					stok_barang_b.view();
				}
				dismissLoading();
			}
		);
	};

	var stok_barang_bt;
	$(document).ready(function() {
		$('[data-toggle="popover"]').popover({
			trigger: 'hover',
			'placement': 'top'
		});
		$(".mydate").datepicker();
		var stok_barang_bt_columns = new Array("id", "nama_barang", "nama_jenis_barang", "sisa", "satuan", "konversi", "satuan_konversi", "hna", "id_vendor", "nama_vendor", "produsen");
		stok_barang_bt = new StokBarangBTAction(
			"stok_barang_bt",
			"gudang_umum",
			"daftar_stok_barang_belum_tersimpan",
			stok_barang_bt_columns
		);
		stok_barang_bt.view();
	});
</script>