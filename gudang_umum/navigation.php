<?php 
	global $NAVIGATOR;
	
	$gudang_umum_menu = new Menu("fa fa-truck");
	$gudang_umum_menu->addProperty('title', "Gudang Umum");
	$gudang_umum_menu->addProperty('name', "Gudang Umum");
	$gudang_umum_menu->addSubMenu("Barang Masuk", "gudang_umum", "barang_masuk", "Data Barang Masuk", "fa fa-bus");
	$gudang_umum_menu->addSubMenu("Barang Masuk (Non-SP)", "gudang_umum", "barang_masuk_non_sp", "Data Barang Masuk Non-SP", "fa fa-bus");
	$gudang_umum_menu->addSubMenu("Penyimpanan Stok Barang", "gudang_umum", "penyimpanan_barang", "Data Penyimpanan Stok Barang", "fa fa-exchange");
	$gudang_umum_menu->addSubMenu("Inventarisasi Barang", "gudang_umum", "inventarisasi_barang", "Data Inventarisasi Barang", "fa fa-list-ol");
	$gudang_umum_menu->addSeparator();
	$gudang_umum_menu->addSubMenu("Daftar Barang", "gudang_umum", "daftar_barang", "Data Barang", "fa fa-suitcase");
	$gudang_umum_menu->addSubMenu("Riwayat Stok Barang", "gudang_umum", "riwayat_stok_barang", "Data Riwayat Stok Barang","fa fa-history");
	$gudang_umum_menu->addSubMenu("Pengecekan Stok ED", "gudang_umum", "pengecekan_stok_ed", "Pengecekan Stok ED", "fa fa-trash");
	$gudang_umum_menu->addSubMenu("Penyesuaian Stok Barang", "gudang_umum", "penyesuaian_stok_barang", "Penyesuaian Stok Barang", "fa fa-refresh");
	$gudang_umum_menu->addSeparator();
	$gudang_umum_menu->addSubMenu("Permintaan Barang Unit", "gudang_umum", "permintaan_barang_unit", "Data Permintaan Barang Unit", "fa fa-tag");
	$gudang_umum_menu->addSubMenu("Barang Keluar", "gudang_umum", "barang_keluar", "Data Barang Keluar", "fa fa-random");
	$gudang_umum_menu->addSubMenu("Retur Barang Unit", "gudang_umum", "retur_barang_unit", "Data Retur Barang Unit", "fa fa-recycle");
	$gudang_umum_menu->addSubMenu("Retur Pembelian Barang", "gudang_umum", "retur_keluar", "Data Retur Pembelian Barang", "fa fa-recycle");
	$gudang_umum_menu->addSeparator();
	$gudang_umum_menu->addSubMenu("Laporan", "gudang_umum", "laporan", "Laporan" ,"fa fa-book");
	$NAVIGATOR->addMenu($gudang_umum_menu, "gudang_umum");
?>