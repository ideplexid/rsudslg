<?php 
	global $db;
	
	$lim_table = new Table(
		array("Kode", "Nama Barang", "Merk", "Jumlah", "Tahun Perolehan", "Nilai Perolehan", "Keadaan", "Lokasi"),
		"Gudang Umum : Laporan Inventaris Medis",
		null,
		true
	);
	$lim_table->setName("lim");
	$lim_table->setAddButtonEnable(false);
	$lim_table->setEditButtonEnable(false);
	$lim_table->setDelButtonEnable(false);
	
	if (isset($_POST['command'])) {
		class LLSOAdapter extends ArrayAdapter {
			public function adapt($row) {
				$array = array();
				$array['id'] = "";
				$array['Kode'] = $row->kode;
				$array['Nama Barang'] = $row->nama_barang;
				$array['Merk'] = $row->merk;
				$array['Jumlah'] = self::format("number", "1");
				$array['Tahun Perolehan'] = self::format("date Y", $row->tahun_perolehan);
				$array['Nilai Perolehan'] = self::format("money Rp.", $row->hna);
				$array['Keadaan'] = self::format("trivial_1_Baik_Rusak", $row->kondisi_baik);
				$array['Lokasi'] = self::format("unslug", $row->unit);
				return $array;
			}
		}
		$lim_adapter = new LLSOAdapter();		
		$lim_dbtable = new DBTable($db, "smis_gd_inventaris");
		$filter = "1";
		if (isset($_POST['kriteria'])) {
			$filter .= " AND (kode LIKE '%" . $_POST['kriteria'] . "%' OR nama_barang LIKE '%" . $_POST['kriteria'] . "%' OR merk LIKE '%" . $_POST['kriteria'] . "%' OR unit LIKE '%" . $_POST['kriteria'] . "%') ";
		}
		$query_value = "
			SELECT *
			FROM (
				SELECT *
				FROM smis_gd_inventaris
				WHERE prop NOT LIKE 'del' AND medis = '1' AND unit <> ''
				ORDER BY unit, nama_barang ASC
			) v_lim
			WHERE " . $filter . "
		";
		$query_count = "
			SELECT COUNT(*)
			FROM (
				SELECT *
				FROM smis_gd_inventaris
				WHERE prop NOT LIKE 'del' AND medis = '1' AND unit <> ''
				ORDER BY unit, nama_barang ASC
			) v_lim
			WHERE " . $filter . "
		";
		$lim_dbtable->setPreferredQuery(true, $query_value, $query_count);
		class LLSODBResponder extends DBResponder {
			public function command($command) {
				if ($command != "print_laporan") {
					return parent::command($command);
				}
				$pack = new ResponsePackage();
				if ($command == "print_laporan") {
					$content = $this->print_report();
					$pack->setContent($content);
					$pack->setStatus(ResponsePackage::$STATUS_OK);
				}
				return $pack->getPackage();
			}
			public function print_report() {
				$data = $this->dbtable->get_result("
					SELECT *
					FROM (
						SELECT *
						FROM smis_gd_inventaris
						WHERE prop NOT LIKE 'del' AND medis = '1' AND unit <> ''
						ORDER BY unit, nama_barang ASC
					) v_lim
				");
				$print_data = "";
				$print_data .= "<center><strong>LAPORAN INVENTARIS MEDIS RSU. KALIWATES</strong></center><br/>";
				$print_data .= "Periode : " . ArrayAdapter::format("date d M Y", date("Y-m-d"));
				$print_data .= "<table border='1'>
									<tr>
										<th>No.</th>
										<th>Kode</th>
										<th>Nama Barang</th>
										<th>Merk</th>
										<th>Jumlah</th>
										<th>Tahun Perolehan</th>
										<th>Nilai Perolehan</th>
										<th>Keadaan - Baik</th>
										<th>Keadaan - Rusak</th>
										<th>Lokasi</th>
									</tr>";
				$total_jumlah = 0;
				$total_jumlah_baik = 0;
				$total_jumlah_rusak = 0;
				$total_rupiah = 0;
				if (count($data) > 0) {
					$no = 1;
					foreach($data as $d) {
						$tanggal_part = explode(" ", $d->tanggal);
						$time_part = explode(":", $tanggal_part[1]);
						$kondisi_sub = "<td><center>&#10003;</center></td><td>&Tab;</td>";
						if ($d->kondisi_baik == false) {
							$kondisi_sub = "<td>&Tab;</td><td><center>&#10003;</center></td>";
							$total_jumlah_rusak += 1;
						} else {
							$total_jumlah_baik += 1;
						}
						$print_data .= "<tr>
											<td>" . $no++ . "</td>
											<td>" . $d->kode . "</td>
											<td>" . $d->nama_barang . "</td>
											<td>" . $d->merk . "</td>
											<td>" . ArrayAdapter::format("number", "1") . "</td>
											<td><center>" . ArrayAdapter::format("date Y", $d->tahun_perolehan) . "</center></td>
											<td>" . ArrayAdapter::format("money Rp.", $d->hna) . "</td>
											" . $kondisi_sub . "
											<td>" . ArrayAdapter::format("unslug", $d->unit) . "</td>
										</tr>";
						$total_jumlah += 1;
						$total_rupiah += $d->hna;
					}
				} else {
					$print_data .= "<tr>
										<td colspan='10' align='center'><i>Tidak terdapat data inventaris medis</i></td>
									</tr>";
				}
				$print_data .= "<tr>
									<td colspan='4' align='center'><b>T O T A L</b></td>
									<td><b>" . ArrayAdapter::format("number", $total_jumlah) . "</b></td>
									<td>&Tab;</td>
									<td><b>" . ArrayAdapter::format("money Rp. ", $total_rupiah) . "</b></td>
									<td><b>" . ArrayAdapter::format("number", $total_jumlah_baik) . "</b></td>
									<td><b>" . ArrayAdapter::format("number", $total_jumlah_rusak) . "</b></td>
									<td>&Tab;</td>
								</tr>";
				$print_data .= "</table><br/>";
				$print_data .= "<table border='0' align='center' class='united'>
									<tr>
										<td colspan='3' align='center'>Mengetahui :</td>
									</tr>
									<tr>
										<td>&nbsp;</td>
									</tr>
									<tr>
										<td align='center'>Jember, " . ArrayAdapter::format("date d M Y", date("Y-m-d")) . "</td>
										<td>&Tab;</td>
										<td>&nbsp;</td>
									</tr>
									<tr>
										<td align='center'>KAUR. GUDANG NON OBAT</td>
										<td>&Tab;</td>
										<td align='center'>MANBID. A K U</td>
									</tr>
									<tr>
										<td>&nbsp;</td>
										<td>&Tab;</td>
										<td>&nbsp;</td>
									</tr>
									<tr>
										<td>&nbsp;</td>
										<td>&Tab;</td>
										<td>&nbsp;</td>
									</tr>
									<tr>
										<td>&nbsp;</td>
										<td>&Tab;</td>
										<td>&nbsp;</td>
									</tr>
									<tr>
										<td>&nbsp;</td>
										<td>&Tab;</td>
										<td>&nbsp;</td>
									</tr>
									<tr>
										<td align='center'><b>Suharyanto</b></td>
										<td>&Tab;</td>
										<td align='center'><b>M. Masduki, S.Pd.</b></td>
									</tr>
								</table>";
				return $print_data;
			}
		}
		$lim_dbresponder = new LLSODBResponder(
			$lim_dbtable,
			$lim_table,
			$lim_adapter
		);
		$data = $lim_dbresponder->command($_POST['command']);
		echo json_encode($data);
		return;
	}
	
	echo $lim_table->getHtml();
	echo addJS("framework/smis/js/table_action.js");
?>
<script type="text/javascript">
	function LIMAction(name, page, action, column) {
		this.initialize(name, page, action, column);
	}
	LIMAction.prototype.constructor = LIMAction;
	LIMAction.prototype = new TableAction();
	LIMAction.prototype.print = function() {
		var data = this.getRegulerData();
		data['command'] = "print_laporan";
		$.post(
			"",
			data,
			function(response) {
				var json = getContent(response);
				if (json == null) return;
				smis_print(json);
			}
		);
	};
	
	var lim;
	$(document).ready(function() {
		$('[data-toggle="popover"]').popover({
			trigger: 'hover',
			'placement': 'top'
		});
		lim = new LIMAction(
			"lim",
			"gudang_umum",
			"laporan_inventaris_medis",
			new Array()
		)
	});
</script>