<?php 
	global $db;
	
	$laporan_form = new Form("lsok_form", "", "Gudang Umum : Laporan Perbekalan Barang Gudang Umum Per Kategori");
	$jenis_barang_button = new Button("", "", "Pilih");
	$jenis_barang_button->setClass("btn-info");
	$jenis_barang_button->setAction("jenis_barang.chooser('jenis_barang', 'jenis_barang_button', 'jenis_barang', jenis_barang)");
	$jenis_barang_button->setIcon("icon-white icon-list-alt");
	$jenis_barang_button->setIsButton(Button::$ICONIC);
	$jenis_barang_button->setAtribute("id='jenis_barang_browse'");
	$jenis_barang_text = new Text("lsok_nama_jenis_barang", "lsok_nama_jenis_barang", "");
	$jenis_barang_text->setClass("smis-one-option-input");
	$jenis_barang_text->setAtribute("disabled='disabled'");
	$jenis_barang_input_group = new InputGroup("");
	$jenis_barang_input_group->addComponent($jenis_barang_text);
	$jenis_barang_input_group->addComponent($jenis_barang_button);
	$laporan_form->addElement("Jenis Barang", $jenis_barang_input_group);
	$show_button = new Button("", "", "Tampilkan");
	$show_button->setClass("btn-primary");
	$show_button->setIcon("icon-white icon-repeat");
	$show_button->setIsButton(Button::$ICONIC);
	$show_button->setAction("lsok.view()");
	$print_button = new Button("", "", "Cetak");
	$print_button->setClass("btn-inverse");
	$print_button->setIcon("icon-white icon-print");
	$print_button->setIsButton(Button::$ICONIC);
	$print_button->setAction("lsok.print()");
	$btn_group = new ButtonGroup("noprint");
	$btn_group->addButton($show_button);
	$btn_group->addButton($print_button);
	$laporan_form->addElement("", $btn_group);
	
	$lsok_table = new Table(
		array("No. Stok", "Nama Barang", "Jumlah", "Satuan", "H. Netto", "Tot. H. Netto", "Tgl. Exp."),
		"",
		null,
		true
	);
	$lsok_table->setName("lsok");
	$lsok_table->setAction(false);
	
	//chooser jenis barang:
	$jenis_barang_table = new Table(
		array("Nama Jenis Barang"),
		"",
		null,
		true
	);
	$jenis_barang_table->setName("jenis_barang");
	$jenis_barang_table->setModel(Table::$SELECT);
	$jenis_barang_adapter = new SimpleAdapter();
	$jenis_barang_adapter->add("Nama Jenis Barang", "nama_jenis_barang");
	$jenis_barang_dbtable = new DBTable($db, "smis_fr_stok_obat");
	$jenis_barang_dbtable->setViewForSelect(true);
	$filter = "1";
	if (isset($_POST['kriteria'])) {
		$filter .= " AND (nama_jenis_barang LIKE '%" . $_POST['kriteria'] . "%')";
	}
	$query_value = "
		SELECT nama_jenis_barang AS 'id', nama_jenis_barang
		FROM (
			SELECT DISTINCT nama_jenis_barang
			FROM smis_gd_stok_barang
			WHERE prop NOT LIKE 'del'
		) v_jenis_barang
		WHERE " . $filter . "
	";
	$query_count = "
		SELECT COUNT(*)
		FROM (
			SELECT DISTINCT nama_jenis_barang
			FROM smis_gd_stok_barang
			WHERE prop NOT LIKE 'del'
		) v_jenis_barang
		WHERE " . $filter . "
	";
	$jenis_barang_dbtable->setPreferredQuery(true, $query_value, $query_count);
	class JenisBarangDBResponder extends DBResponder {
		public function edit() {
			$id = $_POST['id'];
			$data['id'] = $id;
			$data['nama_jenis_barang'] = $id;
			return $data;
		}
	}
	$jenis_barang_dbresponder = new JenisBarangDBResponder(
		$jenis_barang_dbtable,
		$jenis_barang_table,
		$jenis_barang_adapter
	);
	
	$super_command = new SuperCommand();
	$super_command->addResponder("jenis_barang", $jenis_barang_dbresponder);
	$init = $super_command->initialize();
	if ($init != null) {
		echo $init;
		return;
	}
	
	if (isset($_POST['command'])) {
		class LLSOAdapter extends ArrayAdapter {
			public function adapt($row) {
				$array = array();
				$array['id'] = $row->id;
				$array['No. Stok'] = ArrayAdapter::format("digit8", $row->id);
				$array['Nama Barang'] = $row->nama_barang;
				$array['Jumlah'] = self::format("number", $row->sisa);
				$array['Satuan'] = $row->satuan;
				$array['H. Netto'] = self::format("money Rp. ", $row->hna);
				$array['Tot. H. Netto'] = self::format("money Rp. ", $row->jumlah * $row->hna);
				$array['Tgl. Exp.'] = self::format("date d-m-Y", $row->tanggal_exp);
				return $array;
			}
		}
		$lsok_adapter = new LLSOAdapter();		
		$lsok_dbtable = new DBTable($db, "smis_gd_stok_barang");
		$filter = "1";
		if (isset($_POST['kriteria'])) {
			$filter .= " AND (id LIKE '%" . $_POST['kriteria'] . "%' OR nama_barang LIKE '%" . $_POST['kriteria'] . "%') ";
		}
		$query_value = "
			SELECT *
			FROM (
				SELECT *
				FROM smis_gd_stok_barang
				WHERE prop NOT LIKE 'del' AND nama_jenis_barang = '" . $_POST['nama_jenis_barang'] . "' AND sisa > 0
			) v_lsok
			WHERE " . $filter . "
			ORDER BY nama_barang ASC
		";
		$query_count = "
			SELECT COUNT(*)
			FROM (
				SELECT *
				FROM smis_gd_stok_barang
				WHERE prop NOT LIKE 'del' AND nama_jenis_barang = '" . $_POST['nama_jenis_barang'] . "' AND sisa > 0
			) v_lsok
			WHERE " . $filter . "
			ORDER BY nama_barang ASC
		";
		$lsok_dbtable->setPreferredQuery(true, $query_value, $query_count);
		class LLSODBResponder extends DBResponder {
			public function command($command) {
				if ($command != "print_laporan") {
					return parent::command($command);
				}
				$pack = new ResponsePackage();
				if ($command == "print_laporan") {
					$content = $this->print_report();
					$pack->setContent($content);
					$pack->setStatus(ResponsePackage::$STATUS_OK);
				}
				return $pack->getPackage();
			}
			public function print_report() {
				$data = $this->dbtable->get_result("
					SELECT *
					FROM (
						SELECT *
						FROM smis_gd_stok_barang
						WHERE prop NOT LIKE 'del' AND nama_jenis_barang = '" . $_POST['nama_jenis_barang'] . "' AND sisa > 0
					) v_lsok
					ORDER BY nama_barang ASC
				");
				$print_data = "";
				$print_data .= "<center><strong>LAPORAN PERBEKALAN BARANG GUDANG UMUM PER KATEGORI</strong></center><br/>";
				$print_data .= "<table border='0'>
									<tr>
										<td>Jenis Barang</td>
										<td>:</td>
										<td>" . $_POST['nama_jenis_barang'] . "</td>
									</tr>
									<tr>
										<td>Pada Tanggal</td>
										<td>:</td>
										<td>" . ArrayAdapter::format("date d M Y", date("Y-m-d")) . "</td>
									</tr>
								</table>";
				$print_data .= "<table border='1'>
									<tr>
										<th>No. Stok</th>
										<th>Nama Barang</th>
										<th>Jumlah</th>
										<th>Satuan</th>
										<th>H. Netto</th>
										<th>Tot. H. Netto</th>
										<th>Tgl. Exp.</th>
									</tr>";
				$total = 0;
				if (count($data) > 0) {
					$no = 1;
					foreach($data as $d) {
						$tanggal_part = explode(" ", $d->tanggal);
						$time_part = explode(":", $tanggal_part[1]);
						$print_data .= "<tr>
											<td>" . ArrayAdapter::format("digit8", $d->id) . "</td>
											<td>" . $d->nama_barang . "</td>
											<td>" . ArrayAdapter::format("number", $d->sisa) . "</td>
											<td>" . $d->satuan . "</td>
											<td>" . ArrayAdapter::format("money Rp. ", $d->hna) . "</td>
											<td>" . ArrayAdapter::format("money Rp. ", $d->sisa * $d->hna) . "</td>
											<td>" . ArrayAdapter::format("date d-m-Y", $d->tanggal_exp) . "</td>
										</tr>";
						$total += ($d->sisa * $d->hna);
					}
				} else {
					$print_data .= "<tr>
										<td colspan='7' align='center'><i>Tidak terdapat data stok barang</i></td>
									</tr>";
				}
				$print_data .= "<tr>
									<td colspan='5' align='center'><b>T O T A L</b></td>
									<td><b>" . ArrayAdapter::format("money Rp. ", $total) . "</b></td>
									<td>&nbsp;</td>
								</tr>";
				$print_data .= "</table><br/>";
				global $user;
				$print_data .= "<table border='0' align='center' class='united'>
									<tr>
										<td colspan='3' align='center'>Mengetahui :</td>
									</tr>
									<tr>
										<td>&nbsp;</td>
									</tr>
									<tr>
										<td align='center'>Tuban, " . ArrayAdapter::format("date d M Y", date("Y-m-d")) . "</td>
										<td>&Tab;</td>
										<td>&nbsp;</td>
									</tr>
									<tr>
										<td align='center'>KAUR. GUDANG NON OBAT</td>
										<td>&Tab;</td>
										<td align='center'>PETUGAS GUDANG UMUM</td>
									</tr>
									<tr>
										<td>&nbsp;</td>
										<td>&Tab;</td>
										<td>&nbsp;</td>
									</tr>
									<tr>
										<td>&nbsp;</td>
										<td>&Tab;</td>
										<td>&nbsp;</td>
									</tr>
									<tr>
										<td>&nbsp;</td>
										<td>&Tab;</td>
										<td>&nbsp;</td>
									</tr>
									<tr>
										<td>&nbsp;</td>
										<td>&Tab;</td>
										<td>&nbsp;</td>
									</tr>
									<tr>
										<td align='center'><b>Suharyanto</b></td>
										<td>&Tab;</td>
										<td align='center'><b>" . $user->getNameOnly() . "</b></td>
									</tr>
								</table>";
				return $print_data;
			}
		}
		$lsok_dbresponder = new LLSODBResponder(
			$lsok_dbtable,
			$lsok_table,
			$lsok_adapter
		);
		$data = $lsok_dbresponder->command($_POST['command']);
		echo json_encode($data);
		return;
	}
	
	echo $laporan_form->getHtml();
	echo "<div id='table_content'>";
	echo $lsok_table->getHtml();
	echo "</div>";
	echo addJS("framework/smis/js/table_action.js");
?>
<script type="text/javascript">
	function LSOKAction(name, page, action, column) {
		this.initialize(name, page, action, column);
	}
	LSOKAction.prototype.constructor = LSOKAction;
	LSOKAction.prototype = new TableAction();
	LSOKAction.prototype.view = function() {
		var self = this;
		var data = this.getViewData();
		data['nama_jenis_barang'] = $("#lsok_nama_jenis_barang").val();
		showLoading();
		$.post(
			"",
			data,
			function(response) {
				var json = getContent(response);
				if (json == null) return;
				$("#" + self.prefix + "_list").html(json.list);
				$("#" + self.prefix + "_pagination").html(json.pagination);
				dismissLoading();
			}
		);
	};
	LSOKAction.prototype.print = function() {
		var data = this.getRegulerData();
		data['command'] = "print_laporan";
		data['nama_jenis_barang'] = $("#lsok_nama_jenis_barang").val();
		$.post(
			"",
			data,
			function(response) {
				var json = getContent(response);
				if (json == null) return;
				smis_print(json);
			}
		);
	};
	
	function JenisBarangAction(name, page, action, column) {
		this.initialize(name, page, action, column);
	}
	JenisBarangAction.prototype.constructor = JenisBarangAction;
	JenisBarangAction.prototype = new TableAction();
	JenisBarangAction.prototype.selected = function(json) {
		$("#lsok_nama_jenis_barang").val(json.nama_jenis_barang);
	};
	
	var lsok;
	var jenis_barang;
	$(document).ready(function() {
		$('[data-toggle="popover"]').popover({
			trigger: 'hover',
			'placement': 'top'
		});
		jenis_barang = new JenisBarangAction(
			"jenis_barang",
			"gudang_umum",
			"laporan_perbekalan_barang_per_kategori",
			new Array()
		);
		jenis_barang.setSuperCommand("jenis_barang");
		lsok = new LSOKAction(
			"lsok",
			"gudang_umum",
			"laporan_perbekalan_barang_per_kategori",
			new Array()
		)
	});
</script>