<?php
	global $db;
	require_once("smis-base/smis-include-service-consumer.php");
	
	class BarangMasukTable extends Table {
		public function getContentButton($id) {
			$btn_group = new ButtonGroup("noprint");
			$btn = new Button("", "", "View");
			$btn->setAction($this->action . ".detail('" . $id . "')");
			$btn->setClass("btn-success");
			$btn->setAtribute("data-content='View' data-toggle='popover'");
			$btn->setIcon("icon-eye-open icon-white");
			$btn->setIsButton(Button::$ICONIC);
			$btn_group->addElement($btn);
			$btn = new Button("", "", "Hapus");
			$btn->setAction($this->action . ".del('" . $id . "')");
			$btn->setClass("btn-danger");
			$btn->setAtribute("data-content='Hapus' data-toggle='popover'");
			$btn->setIcon("icon-remove icon-white");
			$btn->setIsButton(Button::$ICONIC);
			$btn_group->addElement($btn);
			return $btn_group;
		}
	}
	$barang_masuk_table = new BarangMasukTable(
		array("Nomor", "Tgl. Faktur", "Tgl. Masuk", "Jatuh Tempo", "Vendor", "Nomor Faktur"),
		"Gudang Umum : Barang Masuk Non-SP",
		null,
		true
	);
	$barang_masuk_table->setName("barang_masuk");
	
	if (isset($_POST['super_command']) && $_POST['super_command'] == "barang_masuk") {
		if (isset($_POST['command'])) {
			$barang_masuk_adapter = new SimpleAdapter();
			$barang_masuk_adapter->add("Nomor", "id", "digit8");
			$barang_masuk_adapter->add("Tgl. Faktur", "tanggal", "date d M Y");
			$barang_masuk_adapter->add("Tgl. Masuk", "tanggal_datang", "date d M Y");
			$barang_masuk_adapter->add("Jatuh Tempo", "tanggal_tempo", "date d M Y");
			$barang_masuk_adapter->add("Vendor", "nama_vendor");
			$barang_masuk_adapter->add("Nomor Faktur", "no_faktur");
			$barang_masuk_dbtable = new DBTable($db, "smis_gd_barang_masuk");
			$barang_masuk_dbtable->addCustomKriteria(" id ", " > 0 ");
			$barang_masuk_dbtable->addCustomKriteria(" id_po ", " = 0 ");
			class BarangMasukDBResponder extends DBResponder {
				public function command($command) {
					if ($command != "get_last_item")
						return parent::command($command);
					$pack = null;
					if ($command=="get_last_item") {
						$pack = new ResponsePackage();
						$content = $this->getLastItem();
						$pack->setContent($content);
						$pack->setStatus(ResponsePackage::$STATUS_OK);
					}
					return $pack->getPackage();
				}
				public function getLastItem() {
					$id_barang = $_POST['id_barang'];
					$data = $this->dbtable->get_row("
						SELECT smis_gd_dbarang_masuk.*
						FROM smis_gd_dbarang_masuk LEFT JOIN smis_gd_barang_masuk ON smis_gd_dbarang_masuk.id_barang_masuk = smis_gd_barang_masuk.id
						WHERE smis_gd_barang_masuk.prop NOT LIKE 'del' AND smis_gd_dbarang_masuk.prop NOT LIKE 'del' AND smis_gd_dbarang_masuk.id_barang = '" . $id_barang . "'
						ORDER BY smis_gd_dbarang_masuk.id DESC
						LIMIT 0, 1
					");
					return $data;
				}
				public function save() {
					$header_data = $this->postToArray();
					$id['id'] = $_POST['id'];
					if ($id['id'] == 0 || $id['id'] == "") {
						//do insert header here:
						$result = $this->dbtable->insert($header_data);
						$id['id'] = $this->dbtable->get_inserted_id();
						$success['type'] = "insert";
						$subtotal = 0;
						if (isset($_POST['detail'])) {
							//do insert detail here:
							$detail_dbtable = new DBTable($this->dbtable->get_db(), "smis_gd_dbarang_masuk");
							$detail = $_POST['detail'];
							foreach($detail as $d) {
								$detail_data = array();
								$detail_data['id_barang_masuk'] = $id['id'];
								$detail_data['id_barang'] = $d['id_barang'];
								$detail_data['nama_barang'] = $d['nama_barang'];
								$detail_data['medis'] = $d['medis'];
								$detail_data['inventaris'] = $d['inventaris'];
								$detail_data['nama_jenis_barang'] = $d['nama_jenis_barang'];
								$detail_data['id_dpo'] = $d['id_dpo'];
								$detail_data['jumlah'] = $d['jumlah'];
								$detail_data['sisa'] = $d['sisa'];
								$detail_data['satuan'] = $d['satuan'];
								$detail_data['konversi'] = $d['konversi'];
								$detail_data['satuan_konversi'] = $d['satuan_konversi'];
								$detail_data['hna'] = $d['hna'];
								$detail_data['selisih'] = $d['selisih'];
								$detail_data['produsen'] = $d['produsen'];
								$detail_data['diskon'] = $d['diskon'];
								$detail_data['t_diskon'] = $d['t_diskon'];
								$detail_dbtable->insert($detail_data);
								$subtotal = $subtotal + ($d['hna'] * $d['jumlah']);
							}
						}
						$success['subtotal'] = $subtotal;
					} else {
						//do update header here:
						$result = $this->dbtable->update($header_data, $id);
						$success['type'] = "update";
						$subtotal = 0;
						if (isset($_POST['detail'])) {
							//do update detail here:
							$detail_dbtable = new DBTable($this->dbtable->get_db(), "smis_gd_dbarang_masuk");
							$detail = $_POST['detail'];
							foreach($detail as $d) {
								$detail_data = array();
								$detail_data['id_barang_masuk'] = $id['id'];
								$detail_data['id_barang'] = $d['id_barang'];
								$detail_data['nama_barang'] = $d['nama_barang'];
								$detail_data['medis'] = $d['medis'];
								$detail_data['inventaris'] = $d['inventaris'];
								$detail_data['nama_jenis_barang'] = $d['nama_jenis_barang'];
								$detail_data['id_dpo'] = $d['id_dpo'];
								$detail_data['jumlah'] = $d['jumlah'];
								$detail_data['sisa'] = $d['sisa'];
								$detail_data['satuan'] = $d['satuan'];
								$detail_data['konversi'] = $d['konversi'];
								$detail_data['satuan_konversi'] = $d['satuan_konversi'];
								$detail_data['hna'] = $d['hna'];
								$detail_data['selisih'] = $d['selisih'];
								$detail_data['produsen'] = $d['produsen'];
								$detail_data['diskon'] = $d['diskon'];
								$detail_data['t_diskon'] = $d['t_diskon'];
								if ($d['cmd'] == "insert") {
									$detail_dbtable->insert($detail_data);
								} else if ($d['cmd'] == "update") {
									//get different (jumlah - sisa):
									$detail_row = $detail_dbtable->get_row("
										SELECT jumlah, sisa
										FROM smis_gd_dbarang_masuk
										WHERE id = '" . $d['id'] . "'
									");
									$selisih = $detail_row->jumlah - $detail_row->sisa;
									$detail_data['sisa'] = $d['jumlah'] - $selisih;
									$detail_data['jumlah'] = $d['jumlah'];
									$detail_id_data['id'] = $d['id'];
									$detail_dbtable->update($detail_data, $detail_id_data);
								} else if ($d['cmd'] == "delete") {
									$detail_data = array();
									$detail_data['prop'] = "del";
									$detail_id_data['id'] = $d['id'];
									$detail_dbtable->update($detail_data, $detail_id_data);
								}
								$subtotal = $subtotal + ($d['hna'] * $d['jumlah']);
							}
						}
						$success['subtotal'] = $subtotal;
					}
					$success['id'] = $id['id'];
					$success['success'] = 1;
					if ($result === false) $success['success'] = 0;
					else {
						if ($success['type'] == "insert") {
							/// notify accounting :
							$h_row = $this->dbtable->get_row("
								SELECT *
								FROM smis_gd_barang_masuk
								WHERE id = '" . $id['id'] . "'
							");
							$total = 0;
							$drows = $this->dbtable->get_result("
								SELECT *
								FROM smis_gd_dbarang_masuk
								WHERE id_barang_masuk = '" . $id['id'] . "' AND prop NOT LIKE 'del'
							");
							if ($drows != null) {
								foreach ($drows as $dr) {
									$subtotal = $dr->jumlah * ($dr->hna / 1.1);
									$diskon = $dr->diskon;
									if ($dr->t_diskon == "persen")
										$diskon = $subtotal * $dr->diskon / 100;
									$subtotal = round($subtotal - $diskon);
									$total += $subtotal;
								}
							}
							$global_diskon = $h_row->diskon;
							if ($h_row->t_diskon == "persen")
								$global_diskon = $global_diskon * $total / 100;
							$global_diskon = floor($global_diskon);
							$total = $total - $global_diskon;
							$ppn = floor($total * 0.1);
							$total = $total + $ppn;

							$data = array(
								"jenis_akun"	=> "transaction",
								"jenis_data"	=> "pembelian",
								"id_data"		=> $id['id'],
								"entity"		=> "gudang_umum",
								"service"		=> "get_detail_accounting_bbm",
								"data"			=> $id['id'],
								"code"			=> "bbm-gudang_umum-" . $id['id'],
								"operation"		=> "",
								"tanggal"		=> $h_row->tanggal_datang,
								"uraian"		=> "BBM No. Faktur " . $h_row->no_faktur . " -  ID Transaksi " . $id['id'] . " dari " . $h_row->nama_vendor . " pada tanggal " . ArrayAdapter::format("date d-m-Y", $h_row->tanggal_datang),
								"nilai"			=> $total
							);
							$service_consumer = new ServiceConsumer(
								$this->dbtable->get_db(),
								"push_notify_accounting",
								$data,
								"accounting"
							);
							$service_consumer->execute();
						}
					}
					return $success;
				}
				public function edit() {
					$id = $_POST['id'];
					$header_row = $this->dbtable->get_row("
						SELECT *
						FROM smis_gd_barang_masuk
						WHERE id = '" . $id . "'
					");
					$data['header'] = $header_row;
					$detail_dbtable = new DBTable($this->dbtable->get_db(), "smis_gd_dbarang_masuk");
					$data['detail'] = $detail_dbtable->get_result("
						SELECT *
						FROM smis_gd_dbarang_masuk
						WHERE id_barang_masuk = '" . $id . "' AND prop NOT LIKE 'del'
					");
					return $data;
				}
				public function delete() {
					$id['id'] = $_POST['id'];
					if ($this->dbtable->isRealDelete()) {
						$result = $this->dbtable->delete(null,$id);
					} else {
						/// notify accounting :
						$h_row = $this->dbtable->get_row("
							SELECT *
							FROM smis_gd_barang_masuk
							WHERE id = '" . $id['id'] . "'
						");
						$total = 0;
						$drows = $this->dbtable->get_result("
							SELECT *
							FROM smis_gd_dbarang_masuk
							WHERE id_barang_masuk = '" . $id['id'] . "' AND prop NOT LIKE 'del'
						");
						if ($drows != null) {
							foreach ($drows as $dr) {
								$subtotal = $dr->jumlah * ($dr->hna / 1.1);
								$diskon = $dr->diskon;
								if ($dr->t_diskon == "persen")
									$diskon = $subtotal * $dr->diskon / 100;
								$subtotal = round($subtotal - $diskon);
								$total += $subtotal;
							}
						}
						$global_diskon = $h_row->diskon;
						if ($h_row->t_diskon == "persen")
							$global_diskon = $global_diskon * $total / 100;
						$global_diskon = floor($global_diskon);
						$total = $total - $global_diskon;
						$ppn = floor($total * 0.1);
						$total = $total + $ppn;

						$data = array(
							"jenis_akun"	=> "transaction",
							"jenis_data"	=> "pembelian",
							"id_data"		=> $id['id'],
							"entity"		=> "gudang_umum",
							"service"		=> "get_detail_accounting_bbm",
							"data"			=> $id['id'],
							"code"			=> "bbm-gudang_umum-" . $id['id'],
							"operation"		=> "del",
							"tanggal"		=> $h_row->tanggal_datang,
							"uraian"		=> "BBM No. Faktur " . $h_row->no_faktur . " -  ID Transaksi " . $id['id'] . " dari " . $h_row->nama_vendor . " pada tanggal " . ArrayAdapter::format("date d-m-Y", $h_row->tanggal_datang),
							"nilai"			=> $total
						);
						$service_consumer = new ServiceConsumer(
							$this->dbtable->get_db(),
							"push_notify_accounting",
							$data,
							"accounting"
						);
						$service_consumer->execute();
						$data['prop'] = "del";
						$result = $this->dbtable->update($data, $id);
					}
					$success['success'] = 1;
					$success['id'] = $_POST['id'];
					if ($result === 'false') $success['success'] = 0;
					return $success;
				}
			}
			$barang_masuk_dbresponder = new BarangMasukDBResponder(
				$barang_masuk_dbtable,
				$barang_masuk_table,
				$barang_masuk_adapter
			);
			$data = $barang_masuk_dbresponder->command($_POST['command']);
			echo json_encode($data);
			return;
		}
		return;
	}
	
	// get chooser vendor:
	$vendor_table = new Table(
		array("Nama", "NPWP", "Alamat", "No. Telp."),
		"",
		null,
		true
	);
	$vendor_table->setName("vendor");
	$vendor_table->setModel(Table::$SELECT);
	$vendor_adapter = new SimpleAdapter();
	$vendor_adapter->add("Nama", "nama");
	$vendor_adapter->add("NPWP", "npwp");
	$vendor_adapter->add("Alamat", "alamat");
	$vendor_adapter->add("No. Telp.", "telpon");
	$vendor_service_responder = new ServiceResponder(
		$db,
		$vendor_table,
		$vendor_adapter,
		"get_daftar_vendor"
	);
	
	// get chooser barang:
	$barang_table = new Table(
		array("Nomor", "Nama", "Jenis", "Inventaris"),
		"",
		null,
		true
	);
	$barang_table->setName("barang");
	$barang_table->setModel(Table::$SELECT);
	$barang_adapter = new SimpleAdapter();
	$barang_adapter->add("Nomor", "id", "digit8");
	$barang_adapter->add("Nama", "nama");
	$barang_adapter->add("Jenis", "nama_jenis_barang");
	$barang_adapter->add("Inventaris", "inventaris", "trivial_1_&#10003;_&#10005;");
	$barang_service_responder = new ServiceResponder(
		$db,
		$barang_table,
		$barang_adapter,
		"get_daftar_barang_nm"
	);
	
	//get satuan typeahead items:
	$satuan_table = new Table(
		array("Satuan"),
		"",
		null,
		true
	);
	$satuan_table->setName("satuan");
	$satuan_adapter = new SimpleAdapter();
	$satuan_adapter->add("Satuan", "satuan");
	$satuan_dbtable = new DBTable($db, "smis_gd_dbarang_masuk");
	$filter = "";
	if (isset($_POST['kriteria'])) {
		$filter = " AND satuan LIKE '%" . $_POST['kriteria'] . "%' ";
	}
	$query_value = "
		SELECT satuan AS 'id', satuan
		FROM (
			SELECT DISTINCT satuan
			FROM smis_gd_dbarang_masuk
			WHERE prop NOT LIKE 'del' " . $filter . "
		) v_satuan
	";
	$query_count = "
		SELECT COUNT(*)
		FROM (
			SELECT DISTINCT satuan
			FROM smis_gd_dbarang_masuk
			WHERE prop NOT LIKE 'del' " . $filter . "
		) v_satuan
	";
	$satuan_dbtable->setPreferredQuery(true, $query_value, $query_count);
	$satuan_dbresponder = new DBResponder(
		$satuan_dbtable,
		$satuan_table,
		$satuan_adapter
	);
	
	//get produsen typeahead items:
	$produsen_table = new Table(
		array("Produsen"),
		"",
		null,
		true
	);
	$produsen_table->setName("produsen");
	$produsen_adapter = new SimpleAdapter();
	$produsen_adapter->add("Produsen", "produsen");
	$produsen_dbtable = new DBTable($db, "smis_gd_stok_barang");
	$filter = "";
	if (isset($_POST['kriteria'])) {
		$filter = " AND produsen LIKE '%" . $_POST['kriteria'] . "%' ";
	}
	$query_value = "
		SELECT produsen AS 'id', produsen
		FROM (
			SELECT DISTINCT produsen
			FROM smis_gd_stok_barang
			WHERE prop NOT LIKE 'del' " . $filter . "
		) v_produsen
	";
	$query_count = "
		SELECT COUNT(*)
		FROM (
			SELECT DISTINCT produsen
			FROM smis_gd_stok_barang
			WHERE prop NOT LIKE 'del' " . $filter . "
		) v_produsen
	";
	$produsen_dbtable->setPreferredQuery(true, $query_value, $query_count);
	$produsen_dbresponder = new DBResponder(
		$produsen_dbtable,
		$produsen_table,
		$produsen_adapter
	);
	
	$super_command = new SuperCommand();
	$super_command->addResponder("vendor", $vendor_service_responder);
	$super_command->addResponder("barang", $barang_service_responder);
	$super_command->addResponder("satuan", $satuan_dbresponder);
	$super_command->addResponder("produsen", $produsen_dbresponder);
	$init = $super_command->initialize();
	if ($init != null) {
		echo $init;
		return;
	}
	
	$barang_masuk_modal = new Modal("barang_masuk_add_form", "smis_form_container", "barang_masuk");
	$barang_masuk_modal->setTitle("Data Barang Masuk Non-SP");
	$barang_masuk_modal->setClass(Modal::$FULL_MODEL);
	$id_hidden = new Hidden("barang_masuk_id", "barang_masuk_id", "");
	$barang_masuk_modal->addElement("", $id_hidden);
	$id_vendor_hidden = new Hidden("barang_masuk_id_vendor", "barang_masuk_id_vendor", "");
	$barang_masuk_modal->addElement("", $id_vendor_hidden);
	$nama_vendor_button = new Button("", "", "Pilih");
	$nama_vendor_button->setAtribute("id='barang_masuk_vendor_btn'");
	$nama_vendor_button->setClass("btn-info");
	$nama_vendor_button->setIsButton(Button::$ICONIC);
	$nama_vendor_button->setIcon("icon-white ".Button::$icon_list_alt);
	$nama_vendor_button->setAction("vendor.chooser('vendor', 'vendor_button', 'vendor', vendor)");
	$nama_vendor_text = new Text("barang_masuk_nama_vendor", "barang_masuk_nama_vendor", "");
	$nama_vendor_text->setClass("smis-one-option-input");
	$nama_vendor_text->setAtribute("disabled='disabled'");
	$nama_vendor_input_group = new InputGroup("");
	$nama_vendor_input_group->addComponent($nama_vendor_text);
	$nama_vendor_input_group->addComponent($nama_vendor_button);
	$barang_masuk_modal->addElement("Vendor", $nama_vendor_input_group);
	$no_faktur_text = new Text("barang_masuk_nofaktur", "barang_masuk_nofaktur", "");
	$barang_masuk_modal->addElement("No. Faktur", $no_faktur_text);
	$tanggal_text = new Text("barang_masuk_tanggal", "barang_masuk_tanggal", "");
	$tanggal_text->setClass("mydate");
	$tanggal_text->setAtribute("data-date-format='yyyy-m-d'");
	$barang_masuk_modal->addElement("Tgl. Faktur", $tanggal_text);
	$tanggal_masuk_text = new Text("barang_masuk_tanggal_masuk", "barang_masuk_tanggal_masuk", "");
	$tanggal_masuk_text->setClass("mydate");
	$tanggal_masuk_text->setAtribute("data-date-format='yyyy-m-d'");
	$barang_masuk_modal->addElement("Tgl. Masuk", $tanggal_masuk_text);
	$tanggal_tempo_text = new Text("barang_masuk_tanggal_tempo", "barang_masuk_tanggal_tempo", "");
	$tanggal_tempo_text->setClass("mydate");
	$tanggal_tempo_text->setAtribute("data-date-format='yyyy-m-d'");
	$barang_masuk_modal->addElement("Jatuh Tempo", $tanggal_tempo_text);
	$diskon_text = new Text("barang_masuk_diskon", "barang_masuk_diskon", "");
	$diskon_text->setTypical("money");
	$diskon_text->setAtribute(" data-thousands=\".\" data-decimal=\",\"  data-precision=\"2\"  " );
	$barang_masuk_modal->addElement("Diskon", $diskon_text);
	$t_diskon_option = new OptionBuilder();
	$t_diskon_option->add("Persen (%)", "persen");
	$t_diskon_option->add("Nominal (Rp)", "nominal");
	$t_diskon_select = new Select("barang_masuk_t_diskon", "barang_masuk_t_diskon", $t_diskon_option->getContent());
	$barang_masuk_modal->addElement("Tipe Diskon", $t_diskon_select);
	$keterangan_textarea = new TextArea("barang_masuk_keterangan", "barang_masuk_keterangan", "");
	$keterangan_textarea->setLine(2);
	$barang_masuk_modal->addElement("Keterangan", $keterangan_textarea);
	$total_text = new Text("barang_masuk_total", "barang_masuk_total", "0");
	$total_text->setAtribute("disabled='disabled' data-thousands='.' data-decimal=',' data-prefix='Rp. '  data-precision='2'");
	$total_text->setTypical("money");
	$barang_masuk_modal->addElement("Tot. Tagihan", $total_text);
	$total_faktur_text = new Text("barang_masuk_tagihan", "barang_masuk_tagihan", "Rp. 0,00");
	$total_faktur_text->setAtribute("data-thousands='.' data-decimal=',' data-prefix='Rp. '  data-precision='2'");
	$total_faktur_text->setTypical("money");
	$barang_masuk_modal->addElement("Tot. Faktur", $total_faktur_text);
	class DBarangMasukTable extends Table {
		public function getHeaderButton() {
			$btn_group = new ButtonGroup("noprint");
			$btn = new Button("", "", "Tambah");
			$btn->setAction($this->action . ".show_add_form()");
			$btn->setClass("btn-primary");
			$btn->setAtribute("id='dbarang_masuk_add'");
			$btn->setIcon("icon-plus icon-white");
			$btn->setIsButton(Button::$ICONIC);
			$btn_group->addElement($btn);
			return $btn_group->getHtml();
		}
	}
	$dbarang_masuk_table = new DBarangMasukTable(
		array("No.", "Nama Barang", "Jenis Barang", "Produsen", "Subtotal", "Jumlah", "Ket. Jumlah", "Diskon"),
		"",
		null,
		true
	);
	$dbarang_masuk_table->setName("dbarang_masuk");
	$dbarang_masuk_table->setFooterVisible(false);
	$barang_masuk_modal->addBody("dbarang_masuk_table", $dbarang_masuk_table);
	$save_button = new Button("", "", "Simpan");
	$save_button->setClass("btn-success");
	$save_button->setAtribute("id='barang_masuk_save'");
	$save_button->setIcon("fa fa-floppy-o");
	$save_button->setIsButton(Button::$ICONIC);
	$barang_masuk_modal->addFooter($save_button);
	$ok_button = new Button("", "", "OK");
	$ok_button->setClass("btn-success");
	$ok_button->setAtribute("id='barang_masuk_ok'");
	$ok_button->setAction("$($(this).data('target')).smodal('hide')");
	$barang_masuk_modal->addFooter($ok_button);
	
	$dbarang_masuk_modal = new Modal("dbarang_masuk_add_form", "smis_form_container", "dbarang_masuk");
	$dbarang_masuk_modal->setTitle("Detail Barang Masuk Non-SP");
	$id_hidden = new Hidden("dbarang_masuk_id", "dbarang_masuk_id", "");
	$dbarang_masuk_modal->addElement("", $id_hidden);
	$id_barang_hidden = new Hidden("dbarang_masuk_id_barang", "dbarang_masuk_id_barang", "");
	$dbarang_masuk_modal->addElement("", $id_barang_hidden);
	$medis_hidden = new Hidden("dbarang_masuk_medis", "dbarang_masuk_medis", "");
	$dbarang_masuk_modal->addElement("", $medis_hidden);
	$inventaris_hidden = new Hidden("dbarang_masuk_inventaris", "dbarang_masuk_inventaris", "");
	$dbarang_masuk_modal->addElement("", $inventaris_hidden);
	$nama_barang_button = new Button("", "", "Pilih");
	$nama_barang_button->setClass("btn-info");
	$nama_barang_button->setAction("barang.chooser('barang', 'barang_button', 'barang', barang)");
	$nama_barang_button->setIsButton(Button::$ICONIC);
	$nama_barang_button->setIcon("icon-white ".Button::$icon_list_alt);
	$nama_barang_text = new Text("dbarang_masuk_nama_barang", "dbarang_masuk_nama_barang", "");
	$nama_barang_text->setClass("smis-one-option-input");
	$nama_barang_text->setAtribute("disabled='disabled'");
	$nama_barang_input_group = new InputGroup("");
	$nama_barang_input_group->addComponent($nama_barang_text);
	$nama_barang_input_group->addComponent($nama_barang_button);
	$dbarang_masuk_modal->addElement("Nama Barang", $nama_barang_input_group);
	$nama_jenis_barang_text = new Text("dbarang_masuk_nama_jenis_barang", "dbarang_masuk_nama_jenis_barang", "");
	$nama_jenis_barang_text->setAtribute("disabled='disabled'");
	$dbarang_masuk_modal->addElement("Jenis Barang", $nama_jenis_barang_text);
	$last_hna_text = new Text("dbarang_masuk_last_hna", "dbarang_masuk_last_hna", "");
	$last_hna_text->setAtribute(" data-thousands=\".\" data-decimal=\",\" data-prefix=\"Rp. \"  data-precision=\"2\" disabled='disabled' ");
	$dbarang_masuk_modal->addElement("Harga Terakhir", $last_hna_text);
	$hna_text = new Text("dbarang_masuk_hna", "dbarang_masuk_hna", "");
	$hna_text->setTypical("money");
	$hna_text->setAtribute(" data-thousands=\".\" data-decimal=\",\" data-prefix=\"Rp. \"  data-precision=\"2\" " );
	$dbarang_masuk_modal->addElement("Harga Satuan", $hna_text);
	$jumlah_text = new Text("dbarang_masuk_jumlah", "dbarang_masuk_jumlah", "");
	$dbarang_masuk_modal->addElement("Jumlah", $jumlah_text);
	$satuan_text = new Text("dbarang_masuk_satuan", "dbarang_masuk_satuan", "");
	$dbarang_masuk_modal->addElement("Satuan", $satuan_text);
	$konversi_text = new Text("dbarang_masuk_konversi", "dbarang_masuk_konversi", "");
	$dbarang_masuk_modal->addElement("Jml. Terkecil", $konversi_text);
	$satuan_konversi_text = new Text("dbarang_masuk_satuan_konversi", "dbarang_masuk_satuan_konversi", "");
	$dbarang_masuk_modal->addElement("Stn. Terkecil", $satuan_konversi_text);
	$produsen_text = new Text("dbarang_masuk_produsen", "dbarang_masuk_produsen", "");
	$dbarang_masuk_modal->addElement("Produsen", $produsen_text);
	$diskon_text = new Text("dbarang_masuk_diskon", "dbarang_masuk_diskon", "");
	$dbarang_masuk_modal->addElement("Diskon", $diskon_text);
	$t_diskon_option = new OptionBuilder();
	$t_diskon_option->add("Persen (%)", "persen");
	$t_diskon_option->add("Nominal (Rp)", "nominal");
	$t_diskon_select = new Select("dbarang_masuk_t_diskon", "dbarang_masuk_t_diskon", $t_diskon_option->getContent());
	$dbarang_masuk_modal->addElement("Tipe Diskon", $t_diskon_select);
	$dbarang_masuk_button = new Button("", "", "Simpan");
	$dbarang_masuk_button->setClass("btn-success");
	$dbarang_masuk_button->setAtribute("id='dbarang_masuk_save'");
	$dbarang_masuk_button->setIcon("fa fa-floppy-o");
	$dbarang_masuk_button->setIsButton(Button::$ICONIC);
	$dbarang_masuk_modal->addFooter($dbarang_masuk_button);
	
	echo $dbarang_masuk_modal->getHtml();
	echo $barang_masuk_modal->getHtml();
	echo $barang_masuk_table->getHtml();
	echo addJS("framework/smis/js/table_action.js");
	echo addJS("framework/bootstrap/js/bootstrap-datepicker.js");
	echo addCSS("framework/bootstrap/css/datepicker.css");
?>
<script type="text/javascript">
	function BarangMasukAction(name, page, action, column) {
		this.initialize(name, page, action, column);
	}
	BarangMasukAction.prototype.constructor = BarangMasukAction;
	BarangMasukAction.prototype = new TableAction();
	BarangMasukAction.prototype.refreshTotal = function() {
		var total = 0;
		var nord = $("tbody#dbarang_masuk_list").children("tr").length;
		for(var i = 0; i < nord; i++) {
			var prefix = $("tbody#dbarang_masuk_list").children("tr").eq(i).prop("id");
			var v_hna = parseFloat($("#" + prefix + "_hna").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var t_diskon = $("#" + prefix + "_t_diskon").text();
			var v_diskon = parseFloat($("#" + prefix + "_diskon").text());
			var v_jumlah = parseFloat($("#" + prefix + "_jumlah").text());
			var v_subtotal = Math.round(v_jumlah * v_hna);
			if (t_diskon == "persen") {
				v_diskon = Math.round((v_diskon * v_subtotal) / 100);
				v_subtotal = Math.round(v_subtotal - v_diskon);
			} else {
				v_subtotal = Math.round(v_subtotal - v_diskon);
			}
			total += v_subtotal;
		}
		var v_global_diskon = $("#barang_masuk_diskon").val();
		v_global_diskon = parseFloat(v_global_diskon.replace(/[^0-9-,]/g, '').replace(",", "."));
		var v_global_t_diskon = $("#barang_masuk_t_diskon").val();
		if (v_global_t_diskon == "persen") {
			v_global_diskon = Math.round((v_global_diskon * total) / 100);
		} else {
			v_global_diskon = Math.round(v_global_diskon + (v_global_diskon / 10));
		}
		total = Math.round(total - v_global_diskon);
		total = "Rp. " + parseFloat(total).formatMoney("2", ".", ",");
		$("#barang_masuk_total").val(total);
	};
	BarangMasukAction.prototype.refreshNoDBarangMasuk = function() {
		var no = 1;
		var nor_dbarang_masuk = $("tbody#dbarang_masuk_list").children("tr").length;
		for(var i = 0; i < nor_dbarang_masuk; i++) {
			var dr_prefix = $("tbody#dbarang_masuk_list").children("tr").eq(i).prop("id");
			$("#" + dr_prefix + "_nomor").html("<div align='right'>" + no + ".</div>");
			no++;
		}
	};
	BarangMasukAction.prototype.check_warning = function() {
		var nor_dbarang_masuk = $("tbody#dbarang_masuk_list").children("tr").length;
		var warning_msg = "";
		var need_show = false;
		for(var i = 0; i < nor_dbarang_masuk; i++) {
			var dr_prefix = $("tbody#dbarang_masuk_list").children("tr").eq(i).prop("id");
			var hna = $("#" + dr_prefix + "_hna").text();
			var v_hna = parseFloat(hna.replace(/[^0-9-,]/g, '').replace(",", "."));
			var last_hna = $("#" + dr_prefix + "_last_hna").text();
			var v_last_hna = parseFloat(last_hna.replace(/[^0-9-,]/g, '').replace(",", "."));
			var selisih = Math.abs(parseFloat(v_hna) - parseFloat(v_last_hna));
			var persentase_selisih = Math.round((selisih * 100) / v_last_hna);
			var nama_barang = $("#" + dr_prefix + "_nama_barang").text();
			if (persentase_selisih > 10) { // ambang batas kewajaran kenaikan harga
				warning_msg += "</br>Harga Netto <strong>" + nama_barang + "</strong> memiliki selisih > 10 % dari Harga Netto terakhirnya";
				need_show = true;
			}
			if (need_show) {
				$("#modal_alert_barang_masuk_add_form").html(
					"<div class='alert alert-block alert-warning'>" +
						"<h4>Peringatan</h4>" +
						warning_msg +
					"</div>"
				);
			} else {
				$("#modal_alert_barang_masuk_add_form").html("");
			}
		}
	};
	BarangMasukAction.prototype.validate = function() {
		var valid = true;
		var invalid_msg = "";
		var nama_vendor = $("#barang_masuk_nama_vendor").val();
		var no_faktur = $("#barang_masuk_nofaktur").val();
		var tanggal = $("#barang_masuk_tanggal").val();
		var tanggal_masuk = $("#barang_masuk_tanggal_masuk").val();
		var jatuh_tempo = $("#barang_masuk_tanggal_tempo").val();
		var diskon = $("#barang_masuk_diskon").val();
		var t_diskon = $("#barang_masuk_t_diskon").val();
		var tagihan_faktur = $("#barang_masuk_tagihan").val();
		var keterangan = $("#barang_masuk_keterangan").val();
		var nord = $("tbody#dbarang_masuk_list").children().length;
		$(".error_field").removeClass("error_field");
		if (nama_vendor == "") {
			valid = false;
			invalid_msg += "</br><strong>Vendor</strong> tidak boleh kosong";
			$("#barang_masuk_nama_vendor").addClass("error_field");
		}
		if (no_faktur == "") {
			valid = false;
			invalid_msg += "</br><strong>No. Faktur</strong> tidak boleh kosong";
			$("#barang_masuk_nofaktur").addClass("error_field");
		}
		if (tanggal == "") {
			valid = false;
			invalid_msg += "</br><strong>Tgl. Faktur</strong> tidak boleh kosong";
			$("#barang_masuk_tanggal").addClass("error_field");
		}
		if (tanggal_masuk == "") {
			valid = false;
			invalid_msg += "</br><strong>Tgl. Masuk</strong> tidak boleh kosong";
			$("#barang_masuk_tanggal_masuk").addClass("error_field");
		}
		if (jatuh_tempo == "") {
			valid = false;
			invalid_msg += "</br><strong>Jatuh Tempo</strong> tidak boleh kosong";
			$("#barang_masuk_tanggal_tempo").addClass("error_field");
		}
		if (diskon == "") {
			valid = false;
			invalid_msg += "</br><strong>Diskon</strong> tidak boleh kosong";
			$("#barang_masuk_diskon").addClass("error_field");
		}
		if (t_diskon == "") {
			valid = false;
			invalid_msg += "</br><strong>Tipe Diskon</strong> tidak boleh kosong";
			$("#barang_masuk_t_diskon").addClass("error_field");
		}
		if (tagihan_faktur == "") {
			valid = false;
			invalid_msg += "</br><strong>Tagihan Fak.</strong> tidak boleh kosong";
			$("#barang_masuk_tagihan").addClass("error_field");
		}
		if (keterangan == "") {
			valid = false;
			invalid_msg += "</br><strong>Keterangan</strong> tidak boleh kosong";
			$("#barang_masuk_keterangan").addClass("error_field");
		}
		if (nord == 0) {
			valid = false;
			invalid_msg += "</br><strong>Detil Obat Masuk</strong> tidak boleh kosong";
		}
		if (!valid) {
			$("#modal_alert_barang_masuk_add_form").html(
				"<div class='alert alert-block alert-danger'>" +
					"<h4>Peringatan</h4>" +
					invalid_msg +
				"</div>"
			);
		}
		return valid;
	};
	BarangMasukAction.prototype.show_add_form = function() {
		row_id = 0;
		$("#barang_masuk_id").val("");
		$("#barang_masuk_id_vendor").val("");
		$("#barang_masuk_nama_vendor").val("");
		$("#barang_masuk_vendor_btn").removeAttr("onclick");
		$("#barang_masuk_vendor_btn").attr("onclick", "vendor.chooser('vendor', 'vendor_button', 'vendor', vendor)");
		$("#barang_masuk_vendor_btn").removeClass("btn-info");
		$("#barang_masuk_vendor_btn").removeClass("btn-inverse");
		$("#barang_masuk_vendor_btn").addClass("btn-info");
		$("#barang_masuk_nofaktur").val("");
		$("#barang_masuk_nofaktur").removeAttr("disabled");
		$("#barang_masuk_tanggal").val("");
		$("#barang_masuk_tanggal").removeAttr("disabled");
		$("#barang_masuk_tanggal_masuk").val("");
		$("#barang_masuk_tanggal_masuk").removeAttr("disabled");
		$("#barang_masuk_tanggal_tempo").val("");
		$("#barang_masuk_tanggal_tempo").removeAttr("disabled");
		$("#barang_masuk_diskon").val("0,00");
		$("#barang_masuk_diskon").removeAttr("disabled");
		$("#barang_masuk_t_diskon").val("persen");
		$("#barang_masuk_t_diskon").removeAttr("disabled");
		$("#barang_masuk_keterangan").val("");
		$("#barang_masuk_keterangan").removeAttr("disabled");
		$("#barang_masuk_total").val("Rp. 0,00");
		$("#barang_masuk_tagihan").val("Rp. 0,00");
		$("#barang_masuk_tagihan").removeAttr("disabled");
		$("#dbarang_masuk_add").show();
		$("#dbarang_masuk_list").children().remove();
		$("#barang_masuk_save").removeAttr("onclick");
		$("#barang_masuk_save").attr("onclick", "barang_masuk.save()");
		$("#barang_masuk_save").show();
		$("#barang_masuk_ok").hide();
		$("#modal_alert_barang_masuk_add_form").html("");
		$(".error_field").removeClass("error_field");
		$("#barang_masuk_add_form").smodal("show");
	};
	BarangMasukAction.prototype.save = function() {
		if (!this.validate()) {
			return;
		}
		$("#barang_masuk_add_form").smodal("hide");
		showLoading();
		var self = this;
		var data = this.getRegulerData();
		data['super_command'] = "barang_masuk";
		data['command'] = "save";
		data['id'] = $("#barang_masuk_id").val();
		data['id_po'] = 0;
		data['id_vendor'] = $("#barang_masuk_id_vendor").val();
		data['nama_vendor'] = $("#barang_masuk_nama_vendor").val();
		data['no_faktur'] = $("#barang_masuk_nofaktur").val();
		data['tanggal'] = $("#barang_masuk_tanggal").val();
		data['tanggal_tempo'] = $("#barang_masuk_tanggal_tempo").val();
		data['tanggal_datang'] = $("#barang_masuk_tanggal_masuk").val();
		data['diskon'] = parseFloat($("#barang_masuk_diskon").val().replace(/[^0-9-,]/g, '').replace(",", "."));
		data['t_diskon'] = $("#barang_masuk_t_diskon").val();
		var tagihan_faktur = $("#barang_masuk_tagihan").val();
		tagihan_faktur = tagihan_faktur.replace(/[^0-9-,]/g, '').replace(",", ".");
		data['tagihan'] = tagihan_faktur;
		data['keterangan'] = $("#barang_masuk_keterangan").val();
		var detail = {};
		var nor = $("tbody#dbarang_masuk_list").children("tr").length;
		for(var i = 0; i < nor; i++) {
			var d_data = {};
			var prefix = $("tbody#dbarang_masuk_list").children("tr").eq(i).prop("id");
			d_data['id_barang'] = $("#" + prefix + "_id_barang").text();
			d_data['nama_barang'] = $("#" + prefix + "_nama_barang").text();
			d_data['medis'] = $("#" + prefix + "_medis").text();
			d_data['inventaris'] = $("#" + prefix + "_inventaris").text();
			d_data['nama_jenis_barang'] = $("#" + prefix + "_nama_jenis_barang").text();
			d_data['jumlah'] = $("#" + prefix + "_jumlah").text();
			d_data['sisa'] = $("#" + prefix + "_sisa").text();
			d_data['satuan'] = $("#" + prefix + "_satuan").text();
			d_data['konversi'] = $("#" + prefix + "_konversi").text();
			d_data['satuan_konversi'] = $("#" + prefix + "_satuan_konversi").text();
			var hna = $("#" + prefix + "_hna").text();
			hna = hna.replace(/[^0-9-,]/g, '').replace(",", ".");
			d_data['hna'] = parseFloat(hna);
			d_data['produsen'] = $("#" + prefix + "_produsen").text();
			d_data['diskon'] = $("#" + prefix + "_diskon").text();
			d_data['t_diskon'] = $("#" + prefix + "_t_diskon").text();
			detail[i] = d_data;
		}
		data['detail'] = detail;
		$.post(
			"",
			data,
			function(response) {
				var json = getContent(response);
				if (json == null) {
					$("#barang_masuk_add_form").smodal("show");
				} else {
					self.view();
				}
				dismissLoading();
			}
		);
	};
	BarangMasukAction.prototype.detail = function(id) {
		var self = this;
		var data = this.getRegulerData();
		data['super_command'] = "barang_masuk";
		data['command'] = "edit";
		data['id'] = id;
		$.post(
			"",
			data,
			function(response) {
				var json = getContent(response);
				if (json == null) return;
				$("#barang_masuk_id").val(json.header.id);
				$("#barang_masuk_id_vendor").val(json.header.id_vendor);
				$("#barang_masuk_nama_vendor").val(json.header.nama_vendor);
				$("#barang_masuk_vendor_btn").removeAttr("onclick");
				$("#barang_masuk_vendor_btn").removeClass("btn-info");
				$("#barang_masuk_vendor_btn").removeClass("btn-inverse");
				$("#barang_masuk_vendor_btn").addClass("btn-inverse");
				$("#barang_masuk_nofaktur").val(json.header.no_faktur);
				$("#barang_masuk_nofaktur").removeAttr("disabled");
				$("#barang_masuk_nofaktur").attr("disabled", "disabled");
				$("#barang_masuk_tanggal").val(json.header.tanggal);
				$("#barang_masuk_tanggal").removeAttr("disabled");
				$("#barang_masuk_tanggal").attr("disabled", "disabled");
				$("#barang_masuk_tanggal_masuk").val(json.header.tanggal_datang);
				$("#barang_masuk_tanggal_masuk").removeAttr("disabled");
				$("#barang_masuk_tanggal_masuk").attr("disabled", "disabled");
				$("#barang_masuk_tanggal_tempo").val(json.header.tanggal_tempo);
				$("#barang_masuk_tanggal_tempo").removeAttr("disabled");
				$("#barang_masuk_tanggal_tempo").attr("disabled", "disabled");
				$("#barang_masuk_diskon").val(parseFloat(json.header.diskon).formatMoney("2", ".", ","));
				$("#barang_masuk_diskon").removeAttr("disabled");
				$("#barang_masuk_diskon").attr("disabled", "disabled");
				$("#barang_masuk_t_diskon").val(json.header.t_diskon);
				$("#barang_masuk_t_diskon").removeAttr("disabled");
				$("#barang_masuk_t_diskon").attr("disabled", "disabled");
				$("#barang_masuk_tagihan").val("Rp. " + (parseFloat(json.header.tagihan)).formatMoney("2", ".", ","));
				$("#barang_masuk_tagihan").removeAttr("disabled");
				$("#barang_masuk_tagihan").attr("disabled", "disabled");
				$("#barang_masuk_keterangan").val(json.header.keterangan);
				$("#barang_masuk_keterangan").removeAttr("disabled");
				$("#barang_masuk_keterangan").attr("disabled", "disabled");
				$("#dbarang_masuk_add").hide();
				row_id = 0;
				$("#dbarang_masuk_list").children().remove();
				for(var i = 0; i < json.detail.length; i++) {
					var dbarang_masuk_id = json.detail[i].id;
					var dbarang_masuk_id_barang_f_masuk = json.detail[i].id_barang_masuk;
					var dbarang_masuk_id_barang = json.detail[i].id_barang;
					var dbarang_masuk_nama_barang = json.detail[i].nama_barang;
					var dbarang_masuk_medis = json.detail[i].medis;
					var dbarang_masuk_inventaris = json.detail[i].inventaris;
					var dbarang_masuk_nama_jenis_barang = json.detail[i].nama_jenis_barang;
					var dbarang_masuk_jumlah = json.detail[i].jumlah;
					var dbarang_masuk_sisa = json.detail[i].sisa;
					var dbarang_masuk_satuan = json.detail[i].satuan;
					var dbarang_masuk_konversi = json.detail[i].konversi;
					var dbarang_masuk_satuan_konversi = json.detail[i].satuan_konversi;
					var dbarang_masuk_hna = "Rp. " + (parseFloat(json.detail[i].hna)).formatMoney("2", ".", ",");
					var dbarang_masuk_produsen = json.detail[i].produsen;
					var dbarang_masuk_subtotal = "Rp. " + (parseFloat(json.detail[i].hna) * parseFloat(dbarang_masuk_jumlah)).formatMoney("2", ".", ",");
					var dbarang_masuk_diskon = json.detail[i].diskon;
					var dbarang_masuk_t_diskon = json.detail[i].t_diskon;
					var f_diskon = dbarang_masuk_diskon + " %";
					if (dbarang_masuk_t_diskon == "nominal")
						f_diskon = "Rp. " + (parseFloat(dbarang_masuk_diskon)).formatMoney("2", ".", ",");
					$("tbody#dbarang_masuk_list").append(
						"<tr id='data_" + row_id + "'>" +
							"<td id='data_" + row_id + "_id' style='display: none;'>" + dbarang_masuk_id + "</td>" +
							"<td id='data_" + row_id + "_id_barang' style='display: none;'>" + dbarang_masuk_id_barang + "</td>" +
							"<td id='data_" + row_id + "_medis' style='display: none;'>" + dbarang_masuk_medis + "</td>" +
							"<td id='data_" + row_id + "_inventaris' style='display: none;'>" + dbarang_masuk_inventaris + "</td>" +
							"<td id='data_" + row_id + "_hna' style='display: none;'>" + dbarang_masuk_hna + "</td>" +
							"<td id='data_" + row_id + "_diskon' style='display: none;'>" + dbarang_masuk_diskon + "</td>" +
							"<td id='data_" + row_id + "_t_diskon' style='display: none;'>" + dbarang_masuk_t_diskon + "</td>" +
							"<td id='data_" + row_id + "_jumlah' style='display: none;'>" + dbarang_masuk_jumlah + "</td>" +
							"<td id='data_" + row_id + "_sisa' style='display: none;'>" + dbarang_masuk_sisa + "</td>" +
							"<td id='data_" + row_id + "_satuan' style='display: none;'>" + dbarang_masuk_satuan + "</td>" +
							"<td id='data_" + row_id + "_konversi' style='display: none;'>" + dbarang_masuk_konversi + "</td>" +
							"<td id='data_" + row_id + "_satuan_konversi' style='display: none;'>" + dbarang_masuk_satuan_konversi + "</td>" +
							"<td id='data_" + row_id + "_nomor'></td>" +
							"<td id='data_" + row_id + "_nama_barang'>" + dbarang_masuk_nama_barang + "</td>" +
							"<td id='data_" + row_id + "_nama_jenis_barang'>" + dbarang_masuk_nama_jenis_barang + "</td>" +
							"<td id='data_" + row_id + "_produsen'>" + dbarang_masuk_produsen + "</td>" +
							"<td id='data_" + row_id + "_f_subtotal'>" + dbarang_masuk_jumlah + " x " + dbarang_masuk_hna + " = " + dbarang_masuk_subtotal + "</td>" +
							"<td id='data_" + row_id + "_f_jumlah'>" + dbarang_masuk_jumlah + " " + dbarang_masuk_satuan + "</td>" +
							"<td id='data_" + row_id + "_f_konversi'>1 " + dbarang_masuk_satuan + " = " + dbarang_masuk_konversi + " " + dbarang_masuk_satuan_konversi + "</td>" +
							"<td id='data_" + row_id + "_f_diskon'>" + f_diskon + "</td>" +
							"<td></td>" +
						"</tr>"
					);
					row_id++;
				}
				barang_masuk.refreshNoDBarangMasuk();
				barang_masuk.refreshTotal();
				$("#barang_masuk_save").removeAttr("onclick");
				$("#barang_masuk_save").hide();
				$("#barang_masuk_ok").show();
				$("#modal_alert_barang_masuk_add_form").html("");
				$(".error_field").removeClass("error_field");
				$("#barang_masuk_add_form").smodal("show");
			}
		);
	};
	
	function DBarangMasukAction(name, page, action, column) {
		this.initialize(name, page, action, column);
	}
	DBarangMasukAction.prototype.constructor = DBarangMasukAction;
	DBarangMasukAction.prototype = new TableAction();
	DBarangMasukAction.prototype.validate = function() {
		var valid = true;
		var invalid_msg = "";
		var nama_barang = $("#dbarang_masuk_nama_barang").val();
		var hna = $("#dbarang_masuk_hna").val();
		var jumlah = $("#dbarang_masuk_jumlah").val();
		var satuan = $("#dbarang_masuk_satuan").val();
		var konversi = $("#dbarang_masuk_konversi").val();
		var satuan_konversi = $("#dbarang_masuk_satuan_konversi").val();
		var produsen = $("#dbarang_masuk_produsen").val();
		var diskon = $("#dbarang_masuk_diskon").val();
		var t_diskon = $("#dbarang_masuk_t_diskon").val();
		$(".error_field").removeClass("error_field");
		if (nama_barang == "") {
			valid = false;
			invalid_msg += "</br><strong>Nama Obat</strong> tidak boleh kosong";
			$("#dbarang_masuk_nama_barang").addClass("error_field");
		}
		if (produsen == "") {
			valid = false;
			invalid_msg += "</br><strong>Produsen</strong> tidak boleh kosong";
			$("#dbarang_masuk_produsen").addClass("error_field");
		}
		if (hna == "") {
			valid = false;
			invalid_msg += "</br><strong>Harga Satuan</strong> tidak boleh kosong";
			$("#dbarang_masuk_subtotal").addClass("error_field");
		} else {
			var v_hna = parseFloat(hna.replace(/[^0-9-,]/g, '').replace(",", "."));
			if (v_hna == 0) {
				valid = false;
				invalid_msg += "</br><strong>Harga Satuan</strong> tidak boleh 0 (nol)";
				$("#dbarang_masuk_hna").addClass("error_field");
			}
		}
		if (jumlah == "") {
			valid = false;
			invalid_msg += "</br><strong>Jumlah</strong> tidak boleh kosong";
			$("#dbarang_masuk_jumlah").addClass("error_field");
		} else if (!is_numeric(jumlah)) {
			valid = false;
			invalid_msg += "</br><strong>Jumlah</strong> seharusnya numerik (0-9)";
			$("#dbarang_masuk_jumlah").addClass("error_field");
		}
		if (satuan == "") {
			valid = false;
			invalid_msg += "</br><strong>Satuan</strong> tidak boleh kosong";
			$("#dbarang_masuk_satuan").addClass("error_field");
		}
		if (konversi == "") {
			valid = false;
			invalid_msg += "</br><strong>Jml. Terkecil</strong> tidak boleh kosong";
			$("#dbarang_masuk_konversi").addClass("error_field");
		} else if (!is_numeric(konversi)) {
			valid = false;
			invalid_msg += "</br><strong>Jml. Terkecil</strong> seharusnya numerik (0-9)";
			$("#dbarang_masuk_konversi").addClass("error_field");
		}
		if (satuan_konversi == "") {
			valid = false;
			invalid_msg += "</br><strong>Stn. Terkecil</strong> tidak boleh kosong";
			$("#dbarang_masuk_satuan_konversi").addClass("error_field");
		}
		if (diskon == "") {
			valid = false;
			invalid_msg += "</br><strong>Diskon</strong> tidak boleh kosong";
			$("#dbarang_masuk_diskon").addClass("error_field");
		} else {
			diskon = parseFloat(diskon);
			if (isNaN(diskon)) {
				valid = false;
				invalid_msg += "</br><strong>Diskon</strong> seharusnya numerik (0-9)";
				$("#dbarang_masuk_diskon").addClass("error_field");
			} else {
				if (t_diskon == "persen" && diskon > 100) {
					valid = false;
					invalid_msg += "</br><strong>Diskon</strong> seharusnya bernilai 0-100 %";
					$("#dbarang_masuk_diskon").addClass("error_field");
				} 
				if (diskon < 0) {
					valid = false;
					invalid_msg += "</br><strong>Diskon</strong> seharusnya bernilai >= 0";
					$("#dbarang_masuk_diskon").addClass("error_field");
				}
			}
		}
		if (t_diskon == "") {
			valid = false;
			invalid_msg += "</br><strong>Tipe Diskon</strong> tidak boleh kosong";
			$("#dbarang_masuk_t_diskon").addClass("error_field");
		}
		if (!valid) {
			$("#modal_alert_dbarang_masuk_add_form").html(
				"<div class='alert alert-block alert-danger'>" +
					"<h4>Peringatan</h4>" +
					invalid_msg +
				"</div>"
			);
		}
		return valid;
	};
	DBarangMasukAction.prototype.show_add_form = function() {
		$("#dbarang_masuk_id").val("");
		$("#dbarang_masuk_id_barang").val("");
		$("#dbarang_masuk_nama_barang").val("");
		$("#dbarang_masuk_medis").val("");
		$("#dbarang_masuk_inventaris").val("");
		$("#dbarang_masuk_nama_jenis_barang").val("");
		$("#dbarang_masuk_jumlah").val("");
		$("#dbarang_masuk_satuan").val("");
		$("#dbarang_masuk_konversi").val("");
		$("#dbarang_masuk_satuan_konversi").val("");
		$("#dbarang_masuk_last_hna").val("Rp. 0,00");
		$("#dbarang_masuk_hna").val("Rp. 0,00");
		$("#dbarang_masuk_produsen").val("");
		$("#dbarang_masuk_diskon").val(0);
		$("#dbarang_masuk_t_diskon").val("persen");
		$("#modal_alert_dbarang_masuk_add_form").html("");
		$(".error_field").removeClass("error_field");
		$("#dbarang_masuk_save").removeAttr("onclick");
		$("#dbarang_masuk_save").attr("onclick", "dbarang_masuk.save()");
		$("#dbarang_masuk_add_form").smodal("show");
	};
	DBarangMasukAction.prototype.save = function() {
		if (!this.validate()) {
			return;
		}
		var id = $("#dbarang_masuk_id").val();
		var id_barang = $("#dbarang_masuk_id_barang").val();
		var nama_barang = $("#dbarang_masuk_nama_barang").val();
		var medis = $("#dbarang_masuk_medis").val();
		var inventaris = $("#dbarang_masuk_inventaris").val();
		var nama_jenis_barang = $("#dbarang_masuk_nama_jenis_barang").val();
		var last_hna = $("#dbarang_masuk_last_hna").val();
		var hna = $("#dbarang_masuk_hna").val();
		var v_last_hna = parseFloat(last_hna.replace(/[^0-9-,]/g, '').replace(",", "."));
		var v_hna = parseFloat(hna.replace(/[^0-9-,]/g, '').replace(",", "."));
		var class_attr = "";
		var selisih = Math.abs(parseFloat(v_hna) - parseFloat(v_last_hna));
		var persentase_selisih = Math.round((selisih * 100) / v_last_hna);
		if (persentase_selisih > 10) { // ambang batas kewajaran kenaikan harga
			class_attr = "class='error'";
		}
		var jumlah = $("#dbarang_masuk_jumlah").val();
		var subtotal = "Rp. " + (v_hna * parseFloat(jumlah)).formatMoney("2", ".", ",");
		var sisa = $("#dbarang_masuk_jumlah").val();
		var satuan = $("#dbarang_masuk_satuan").val();
		var konversi = $("#dbarang_masuk_konversi").val();
		var satuan_konversi = $("#dbarang_masuk_satuan_konversi").val();
		var produsen = $("#dbarang_masuk_produsen").val();
		var diskon = $("#dbarang_masuk_diskon").val();
		var t_diskon = $("#dbarang_masuk_t_diskon").val();
		var f_diskon = diskon + " %";
		if (t_diskon == "nominal")
			f_diskon = "Rp. " + (parseFloat(diskon)).formatMoney("2", ".", ",");
		$("tbody#dbarang_masuk_list").append(
			"<tr id='data_" + row_id + "' " + class_attr + ">" +
				"<td id='data_" + row_id + "_id' style='display: none;'>" + id + "</td>" +
				"<td id='data_" + row_id + "_id_barang' style='display: none;'>" + id_barang + "</td>" +
				"<td id='data_" + row_id + "_medis' style='display: none;'>" + medis + "</td>" +
				"<td id='data_" + row_id + "_inventaris' style='display: none;'>" + inventaris + "</td>" +
				"<td id='data_" + row_id + "_hna' style='display: none;'>" + hna + "</td>" +
				"<td id='data_" + row_id + "_last_hna' style='display: none;'>" + last_hna + "</td>" +
				"<td id='data_" + row_id + "_diskon' style='display: none;'>" + diskon + "</td>" +
				"<td id='data_" + row_id + "_t_diskon' style='display: none;'>" + t_diskon + "</td>" +
				"<td id='data_" + row_id + "_jumlah' style='display: none;'>" + jumlah + "</td>" +
				"<td id='data_" + row_id + "_sisa' style='display: none;'>" + sisa + "</td>" +
				"<td id='data_" + row_id + "_satuan' style='display: none;'>" + satuan + "</td>" +
				"<td id='data_" + row_id + "_konversi' style='display: none;'>" + konversi + "</td>" +
				"<td id='data_" + row_id + "_satuan_konversi' style='display: none;'>" + satuan_konversi + "</td>" +
				"<td id='data_" + row_id + "_nomor'></td>" +
				"<td id='data_" + row_id + "_nama_barang'>" + nama_barang + "</td>" +
				"<td id='data_" + row_id + "_nama_jenis_barang'>" + nama_jenis_barang + "</td>" +
				"<td id='data_" + row_id + "_produsen'>" + produsen + "</td>" +
				"<td id='data_" + row_id + "_f_subtotal'>" + jumlah + " x " + hna + " = " + subtotal + "</td>" +
				"<td id='data_" + row_id + "_f_jumlah'>" + jumlah + " " + satuan + "</td>" +
				"<td id='data_" + row_id + "_f_konversi'>1 " + satuan + " = " + konversi + " " + satuan_konversi + "</td>" +
				"<td id='data_" + row_id + "_f_diskon'>" + f_diskon + "</td>" +
				"<td>" +
					"<div class='btn-group noprint'>" +
						"<a href='#' onclick='dbarang_masuk.edit(" + row_id + ")' data-content='Ubah' data-toggle='popover' class='input btn btn-warning'>" + 
							"<i class='icon-edit icon-white'></i>" +
						"</a>" +
						"<a href='#' onclick='dbarang_masuk.delete(" + row_id + ")' data-content='Hapus' data-toggle='popover' class='input btn btn-danger'>" + 
							"<i class='icon-remove icon-white'></i>" + 
						"</a>" +
					"</div>" +
				"</td>" +
			"</tr>"
		);
		row_id++;
		barang_masuk.refreshNoDBarangMasuk();
		barang_masuk.refreshTotal();
		barang_masuk.check_warning();
		$("#dbarang_masuk_add_form").smodal("hide");
	};
	DBarangMasukAction.prototype.edit = function(r_num) {
		var id = $("#data_" + r_num + "_id").text();
		var id_barang = $("#data_" + r_num + "_id_barang").text();
		var nama_barang = $("#data_" + r_num + "_nama_barang").text();
		var medis = $("#data_" + r_num + "_medis").text();
		var inventaris = $("#data_" + r_num + "_inventaris").text();
		var nama_jenis_barang = $("#data_" + r_num + "_nama_jenis_barang").text();
		var jumlah = $("#data_" + r_num + "_jumlah").text();
		var satuan = $("#data_" + r_num + "_satuan").text();
		var konversi = $("#data_" + r_num + "_konversi").text();
		var satuan_konversi = $("#data_" + r_num + "_satuan_konversi").text();
		var last_hna = $("#data_" + r_num + "_last_hna").text();
		var hna = $("#data_" + r_num + "_hna").text();
		var produsen = $("#data_" + r_num + "_produsen").text();
		var diskon = $("#data_" + r_num + "_diskon").text();
		var t_diskon = $("#data_" + r_num + "_t_diskon").text();
		$("#dbarang_masuk_id").val(id);
		$("#dbarang_masuk_id_barang").val(id_barang);
		$("#dbarang_masuk_nama_barang").val(nama_barang);
		$("#dbarang_masuk_medis").val(medis);
		$("#dbarang_masuk_inventaris").val(inventaris);
		$("#dbarang_masuk_nama_jenis_barang").val(nama_jenis_barang);
		$("#dbarang_masuk_jumlah").val(jumlah);
		$("#dbarang_masuk_satuan").val(satuan);
		$("#dbarang_masuk_konversi").val(konversi);
		$("#dbarang_masuk_satuan_konversi").val(satuan_konversi);
		$("#dbarang_masuk_last_hna").val(last_hna);
		$("#dbarang_masuk_hna").val(hna);
		$("#dbarang_masuk_produsen").val(produsen);
		$("#dbarang_masuk_diskon").val(diskon);
		$("#dbarang_masuk_t_diskon").val(t_diskon);
		$("#modal_alert_dbarang_masuk_add_form").html("");
		$(".error_field").removeClass("error_field");
		$("#dbarang_masuk_save").removeAttr("onclick");
		$("#dbarang_masuk_save").attr("onclick", "dbarang_masuk.update(" + r_num + ")");
		$("#dbarang_masuk_add_form").smodal("show");
	};
	DBarangMasukAction.prototype.update = function(r_num) {
		if (!this.validate()) {
			return;
		}
		var id = $("#dbarang_masuk_id").val();
		var id_barang = $("#dbarang_masuk_id_barang").val();
		var nama_barang = $("#dbarang_masuk_nama_barang").val();
		var medis = $("#dbarang_masuk_medis").val();
		var inventaris = $("#dbarang_masuk_inventaris").val();
		var nama_jenis_barang = $("#dbarang_masuk_nama_jenis_barang").val();
		var last_hna = $("#dbarang_masuk_last_hna").val();
		var hna = $("#dbarang_masuk_hna").val();
		var v_last_hna = parseFloat(last_hna.replace(/[^0-9-,]/g, '').replace(",", "."));
		var v_hna = parseFloat(hna.replace(/[^0-9-,]/g, '').replace(",", "."));
		var selisih = Math.abs(parseFloat(v_hna) - parseFloat(v_last_hna));
		var persentase_selisih = Math.round((selisih * 100) / v_last_hna);
		if (persentase_selisih > 10) { // ambang batas kewajaran kenaikan harga
			$("tr#data_" + r_num).removeAttr("class");
			$("tr#data_" + r_num).attr("class", "error");
		} else {
			$("tr#data_" + r_num).removeAttr("class");
		}
		var produsen = $("#dbarang_masuk_produsen").val();
		var jumlah = $("#dbarang_masuk_jumlah").val();
		var subtotal = "Rp. " + (v_hna * parseFloat(jumlah)).formatMoney("2", ".", ",");
		var selisih = Number($("#data_" + r_num + "_jumlah").text()) - Number($("#data_" + r_num + "_sisa").text());
		var sisa = Number(jumlah) - Number(selisih);
		var satuan = $("#dbarang_masuk_satuan").val();
		var konversi = $("#dbarang_masuk_konversi").val();
		var satuan_konversi = $("#dbarang_masuk_satuan_konversi").val();
		var diskon = $("#dbarang_masuk_diskon").val();
		var t_diskon = $("#dbarang_masuk_t_diskon").val();
		var f_diskon = diskon + " %";
		if (t_diskon == "nominal")
			f_diskon = "Rp. " + (parseFloat(diskon)).formatMoney("2", ".", ",");
		$("#data_" + r_num + "_id").text(id);
		$("#data_" + r_num + "_id_barang").text(id_barang);
		$("#data_" + r_num + "_nama_barang").text(nama_barang);
		$("#data_" + r_num + "_medis").text(medis);
		$("#data_" + r_num + "_inventaris").text(inventaris);
		$("#data_" + r_num + "_nama_jenis_barang").text(nama_jenis_barang);
		$("#data_" + r_num + "_last_hna").text(last_hna);
		$("#data_" + r_num + "_hna").text(hna);
		$("#data_" + r_num + "_diskon").text(diskon);
		$("#data_" + r_num + "_t_diskon").text(t_diskon);
		$("#data_" + r_num + "_jumlah").text(jumlah);
		$("#data_" + r_num + "_sisa").text(sisa);
		$("#data_" + r_num + "_satuan").text(satuan);
		$("#data_" + r_num + "_konversi").text(konversi);
		$("#data_" + r_num + "_satuan_konversi").text(satuan_konversi);
		$("#data_" + r_num + "_produsen").text(produsen);
		$("#data_" + r_num + "_f_subtotal").text(jumlah + " x " + hna + " = " + subtotal);
		$("#data_" + r_num + "_f_jumlah").text(jumlah + " " + satuan);
		$("#data_" + r_num + "_f_konversi").text("1 " + satuan + " = " + konversi + " " + satuan_konversi);
		$("#data_" + r_num + "_f_diskon").text(f_diskon);
		barang_masuk.refreshTotal();
		barang_masuk.check_warning();
		$("#dbarang_masuk_add_form").smodal("hide");
	};
	DBarangMasukAction.prototype.delete = function(r_num) {
		var id = $("#data_" + r_num + "_id").text();
		if (id.length == 0) {
			$("#data_" + r_num).remove();
		} else {
			$("#data_" + r_num).attr("style", "display: none;");
			$("#data_" + r_num).attr("class", "deleted");
		}
		barang_masuk.refreshNoDBarangMasuk();
		barang_masuk.refreshTotal();
	};
	
	function VendorAction(name, page, action, column) {
		this.initialize(name, page, action, column);
	}
	VendorAction.prototype.constructor = VendorAction;
	VendorAction.prototype = new TableAction();
	VendorAction.prototype.selected = function(json) {
		$("#barang_masuk_id_vendor").val(json.id);
		$("#barang_masuk_nama_vendor").val(json.nama);
	};
	
	function BarangAction(name, page, action, column) {
		this.initialize(name, page, action, column);
	}
	BarangAction.prototype.constructor = BarangAction;
	BarangAction.prototype = new TableAction();
	BarangAction.prototype.selected = function(json) {
		$("#dbarang_masuk_id_barang").val(json.id);
		$("#dbarang_masuk_nama_barang").val(json.nama);
		$("#dbarang_masuk_medis").val(json.medis);
		$("#dbarang_masuk_inventaris").val(json.inventaris);
		$("#dbarang_masuk_nama_jenis_barang").val(json.nama_jenis_barang);
		$("#dbarang_masuk_satuan").val(json.satuan);
		$("#dbarang_masuk_konversi").val(json.konversi);
		$("#dbarang_masuk_satuan_konversi").val(json.satuan_konversi);
		var data = this.getRegulerData();
		data['super_command'] = "barang_masuk";
		data['command'] = "get_last_item";
		data['id_barang'] = json.id;
		$.post(
			"",
			data,
			function(response) {
				var jsn = getContent(response);
				if (jsn == null) {
					$("#dbarang_masuk_last_hna").val("Rp. 0,00");
					$("#dbarang_masuk_hna").val("Rp. 0,00");
					$("#dbarang_masuk_produsen").val("");
				} else {
					var hna = jsn.hna;
					var satuan = jsn.satuan;
					var konversi = jsn.konversi;
					var satuan_konversi = jsn.satuan_konversi;
					var produsen = jsn.produsen;
					hna = "Rp. " + parseFloat(hna).formatMoney("2", ".", ",");
					$("#dbarang_masuk_last_hna").val(hna);
					$("#dbarang_masuk_hna").val(hna);
					$("#dbarang_masuk_satuan").val(satuan);
					$("#dbarang_masuk_konversi").val(konversi);
					$("#dbarang_masuk_satuan_konversi").val(satuan_konversi);
					$("#dbarang_masuk_produsen").val(produsen);
				}
			}
		);
	};
		
	var barang_masuk;
	var dbarang_masuk;
	var vendor;
	var barang;
	$(document).ready(function() {
		$('[data-toggle="popover"]').popover({
			trigger: 'hover',
			'placement': 'top'
		});
		$(".mydate").datepicker();
		$("#smis-chooser-modal").on("show", function() {
			if ($("#smis-chooser-modal .modal-header h3").text() == "VENDOR") {
				$("#smis-chooser-modal").removeClass("half_model");
				$("#smis-chooser-modal").addClass("half_model");
			} else {
				$("#smis-chooser-modal").removeClass("half_model");
			}
		});
		$("#smis-chooser-modal").on("hidden", function() {
			$("#smis-chooser-modal").removeClass("half_model");
		});
		vendor = new VendorAction(
			"vendor",
			"gudang_umum",
			"barang_masuk_non_sp",
			new Array()
		);		
		vendor.setSuperCommand("vendor");
		barang = new BarangAction(
			"barang",
			"gudang_umum",
			"barang_masuk_non_sp",
			new Array()
		);		
		barang.setSuperCommand("barang");
		var dbarang_masuk_columns = new Array("id", "id_barang", "nama_barang", "medis", "inventaris", "nama_jenis_barang", "jumlah", "sisa", "satuan", "konversi", "satuan_konversi", "hna", "produsen", "diskon", "t_diskon");
		dbarang_masuk = new DBarangMasukAction(
			"dbarang_masuk",
			"gudang_umum",
			"barang_masuk_non_sp",
			dbarang_masuk_columns
		);
		var barang_masuk_columns = new Array("id", "id_po", "id_vendor", "nama_vendor", "tanggal", "no_faktur", "tanggal", "tanggal_datang", "tanggal_tempo", "diskon", "t_diskon", "keterangan", "total", "tagihan");
		barang_masuk = new BarangMasukAction(
			"barang_masuk",
			"gudang_umum",
			"barang_masuk_non_sp",
			barang_masuk_columns
		);
		barang_masuk.setSuperCommand("barang_masuk");
		barang_masuk.view();
		
		$("#barang_masuk_diskon").on("change", function() {
			var diskon = $("#barang_masuk_diskon").val().replace(/[^0-9-,]/g, '').replace(",", ".");
			if (diskon == "") {
				$("#barang_masuk_diskon").val("0,00");
			}
		});
		
		$("#barang_masuk_t_diskon").on("change", function() {
			var diskon = $("#barang_masuk_diskon").val().replace(/[^0-9-,]/g, '').replace(",", ".");
			var t_diskon = $("#barang_masuk_t_diskon").val();
			if (diskon > 100 && t_diskon == "persen") {
				$("#modal_alert_barang_masuk_add_form").html(
					"<div class='alert alert-block alert-danger'>" +
						"<h4>Peringatan</h4>" +
						"<br /><b>Diskon</b> tidak boleh melebihi 100%" +
					"</div>"
				);
				return;
			}
			$("#modal_alert_barang_masuk_add_form").html("");
			barang_masuk.refreshTotal();
		});
		
		$("#barang_masuk_diskon").on("keyup", function() {
			var diskon = $("#barang_masuk_diskon").val().replace(/[^0-9-,]/g, '').replace(",", ".");
			var t_diskon = $("#barang_masuk_t_diskon").val();
			if (diskon > 100 && t_diskon == "persen") {
				$("#modal_alert_barang_masuk_add_form").html(
					"<div class='alert alert-block alert-danger'>" +
						"<h4>Peringatan</h4>" +
						"<br /><b>Diskon</b> tidak boleh melebihi 100%" +
					"</div>"
				);
				return;
			}
			$("#modal_alert_barang_masuk_add_form").html("");
			barang_masuk.refreshTotal();
		});
		
		$("#dbarang_masuk_hna").on("change", function() {
			var diskon = $("#dbarang_masuk_hna").val().replace(/[^0-9-,]/g, '').replace(",", ".");
			if (diskon == "") {
				$("#dbarang_masuk_hna").val("0,00");
			}
		});
		
		$('#dbarang_masuk_satuan').typeahead({
			minLength:2,
	        source: function (query, process) {
				var satuan_data = {	
					page: "gudang_umum",
					action: "barang_masuk_non_sp",
					super_command: "satuan",
					command: "list",
					prototype_name: "",
					prototype_slug: "",
					prototype_implement: "",
					kriteria: $("#dbarang_masuk_satuan").val()
				};
				var items = new Array;
				items = [""];
				$.ajax({
					url: '',
					type: 'POST',
					data: satuan_data,
					success: function(res) {
						var json = getContent(res);
						var the_data_proses = json.dbtable.data;
						items = [""];	
						$.map(the_data_proses, function(data) {
							var group;
							group = {
								id: data.id,
								name: data.satuan,                            
								toString: function () {
									return JSON.stringify(this);
								},
								toLowerCase: function () {
									return this.name.toLowerCase();
								},
								indexOf: function (string) {
									return String.prototype.indexOf.apply(this.name, arguments);
								},
								replace: function (string) {
									var value = '';
									value +=  this.name;
									if(typeof(this.level) != 'undefined') {
										value += ' <span class="pull-right muted">';
										value += this.level;
										value += '</span>';
									}
									return String.prototype.replace.apply('<div class="typeaheadiv">' + value + '</div>', arguments);
								}
							};
							items.push(group);
						});
						process(items);
					}
				});
	        },
	        updater: function (item) {
	            var item = JSON.parse(item);
				$("#dbarang_masuk_satuan").val(item.name);
	            return item.name;
	        }
	    });
		
		$('#dbarang_masuk_satuan_konversi').typeahead({
			minLength:2,
	        source: function (query, process) {
				var satuan_data = {	
					page: "gudang_umum",
					action: "barang_masuk_non_sp",
					super_command: "satuan",
					command: "list",
					prototype_name: "",
					prototype_slug: "",
					prototype_implement: "",
					kriteria: $("#dbarang_masuk_satuan_konversi").val()
				};
				var items = new Array;
				items = [""];
				$.ajax({
					url: '',
					type: 'POST',
					data: satuan_data,
					success: function(res) {
						var json = getContent(res);
						var the_data_proses = json.dbtable.data;
						items = [""];	
						$.map(the_data_proses, function(data) {
							var group;
							group = {
								id: data.id,
								name: data.satuan,                            
								toString: function () {
									return JSON.stringify(this);
								},
								toLowerCase: function () {
									return this.name.toLowerCase();
								},
								indexOf: function (string) {
									return String.prototype.indexOf.apply(this.name, arguments);
								},
								replace: function (string) {
									var value = '';
									value +=  this.name;
									if(typeof(this.level) != 'undefined') {
										value += ' <span class="pull-right muted">';
										value += this.level;
										value += '</span>';
									}
									return String.prototype.replace.apply('<div class="typeaheadiv">' + value + '</div>', arguments);
								}
							};
							items.push(group);
						});
						process(items);
					}
				});
	        },
	        updater: function (item) {
	            var item = JSON.parse(item);
				$("#dbarang_masuk_satuan_konversi").val(item.name);
	            return item.name;
	        }
	    });
		
		$('#dbarang_masuk_produsen').typeahead({
			minLength:2,
	        source: function (query, process) {
				var produsen_data = {	
					page: "gudang_umum",
					action: "barang_masuk_non_sp",
					super_command: "produsen",
					command: "list",
					prototype_name: "",
					prototype_slug: "",
					prototype_implement: "",
					kriteria: $("#dbarang_masuk_produsen").val()
				};
				var items = new Array;
				items = [""];
				$.ajax({
					url: '',
					type: 'POST',
					data: produsen_data,
					success: function(res) {
						var json = getContent(res);
						var the_data_proses = json.dbtable.data;
						items = [""];	
						$.map(the_data_proses, function(data) {
							var group;
							group = {
								id: data.id,
								name: data.produsen,                            
								toString: function () {
									return JSON.stringify(this);
								},
								toLowerCase: function () {
									return this.name.toLowerCase();
								},
								indexOf: function (string) {
									return String.prototype.indexOf.apply(this.name, arguments);
								},
								replace: function (string) {
									var value = '';
									value +=  this.name;
									if(typeof(this.level) != 'undefined') {
										value += ' <span class="pull-right muted">';
										value += this.level;
										value += '</span>';
									}
									return String.prototype.replace.apply('<div class="typeaheadiv">' + value + '</div>', arguments);
								}
							};
							items.push(group);
						});
						process(items);
					}
				});
	        },
	        updater: function (item) {
	            var item = JSON.parse(item);
				$("#dbarang_masuk_produsen").val(item.name);
	            return item.name;
	        }
	    });
	});
</script>