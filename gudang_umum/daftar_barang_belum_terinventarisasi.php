<?php 
	require_once("smis-base/smis-include-duplicate.php");
	global $db;
	
	class BarangBTTable extends Table {
		public function getContentButton($id) {
			$btn_group = new ButtonGroup("noprint");
			$btn = new Button("", "", "Ubah");
			$btn->setAction($this->action . ".edit('" . $id . "')");
			$btn->setClass("btn-warning");
			$btn->setAtribute("data-content='Ubah' data-toggle='popover'");
			$btn->setIcon("icon-edit icon-white");
			$btn->setIsButton(Button::$ICONIC);
			$btn_group->addElement($btn);
			return $btn_group;
		}
	}
	$barang_table = new BarangBTTable(
		array("Nomor", "Nama Barang", "Jumlah", "Harga Perolehan"),
		"",
		null,
		true
	);
	$barang_table->setName("barang_bt");
	$barang_table->setReloadButtonEnable(false);
	$barang_table->setPrintButtonEnable(false);
	$barang_table->setAddButtonEnable(false);
	
	if (isset($_POST['command'])) {
		class BarangAdapter extends ArrayAdapter {
			public function adapt($row) {
				$array = array();
				$array['id'] = $row->id;
				$array['Nomor'] = self::format("digit8", $row->id);
				$array['Nama Barang'] = $row->nama_barang;
				$array['Jumlah'] = $row->sisa . " " . $row->satuan;
				$array['Harga Perolehan'] = self::format("money Rp.", $row->hna);
				return $array;
			}
		}
		$barang_adapter = new BarangAdapter();
		$columns = array("id", "nama_barang", "sisa", "satuan", "konversi", "satuan_konversi", "hna");
		$barang_dbtable = new DBTable(
			$db,
			"smis_gd_dbarang_masuk",
			$columns
		);
		$filter = "";
		if (isset($_POST['kriteria'])) {
			$filter = " AND (smis_gd_dbarang_masuk.nama_barang LIKE '%" . $_POST['kriteria'] . "%')";
		}
		$query_value = "
			SELECT smis_gd_dbarang_masuk.*
			FROM smis_gd_dbarang_masuk
			WHERE smis_gd_dbarang_masuk.prop NOT LIKE 'del' AND smis_gd_dbarang_masuk.inventaris = 1 AND smis_gd_dbarang_masuk.sisa > 0 " . $filter . "
		";
		$query_count = "
			SELECT COUNT(*)
			FROM smis_gd_dbarang_masuk
			WHERE smis_gd_dbarang_masuk.prop NOT LIKE 'del' AND smis_gd_dbarang_masuk.inventaris = 1 AND smis_gd_dbarang_masuk.sisa > 0 " . $filter . "
		";
		$barang_dbtable->setPreferredQuery(true, $query_value, $query_count);
		class BarangDBResponder extends DuplicateResponder {
			public function save() {
				//do update sisa dbarang masuk:
				$dbarang_masuk_data = array();
				$dbarang_masuk_data['sisa'] = $_POST['sisa'];
				$dbarang_masuk_data['autonomous'] = "[".$this->getAutonomous()."]";
		        $dbarang_masuk_data['duplicate'] = 0;
		        $dbarang_masuk_data['time_updated'] = date("Y-m-d H:i:s");
		        $dbarang_masuk_data['origin_updated'] = $this->getAutonomous();
				$dbarang_masuk_id['id'] = $_POST['id'];
				$result = $this->dbtable->update($dbarang_masuk_data, $dbarang_masuk_id);
				$success['type'] = "update";
				//do insert inventaris:
				$inventaris_dbtable = new DBTable($this->dbtable->get_db(), "smis_gd_inventaris");
				$inventaris_data = array();
				$inventaris_data['id_dbarang_masuk'] = $dbarang_masuk_id['id'];
				$inventaris_data['nama_barang'] = $_POST['nama_barang'];
				$inventaris_data['medis'] = $_POST['medis'];
				$inventaris_data['hna'] = $_POST['hna'];
				$inventaris_data['kode'] = $_POST['kode'];
				$inventaris_data['merk'] = $_POST['merk'];
				$inventaris_data['tahun_perolehan'] = $_POST['tahun_perolehan'];
				$inventaris_data['usia_penyusutan'] = $_POST['usia_penyusutan'];
				$inventaris_data['kondisi_baik'] = 1;
				$inventaris_data['autonomous'] = "[".$this->getAutonomous()."]";
		        $inventaris_data['duplicate'] = 0;
		        $inventaris_data['time_updated'] = date("Y-m-d H:i:s");
		        $inventaris_data['origin_updated'] = $this->getAutonomous();
		        $inventaris_data['origin'] = $this->getAutonomous();
				$inventaris_dbtable->insert($inventaris_data);
				$id_inventaris = $inventaris_dbtable->get_inserted_id();
				$success['id'] = $id_inventaris;
				$success['success'] = 1;
				if ($result === false) $success['success'] = 0;
				return $success;
			}
		}
		$barang_dbresponder = new BarangDBResponder(
			$barang_dbtable,
			$barang_table,
			$barang_adapter
		);
		$data = $barang_dbresponder->command($_POST['command']);
		echo json_encode($data);
		return;
	}
	
	$barang_modal = new Modal("barang_bt_add_form", "smis_form_container", "barang_bt");
	$barang_modal->setTitle("Data Barang Masuk");
	$id_hidden = new Hidden("barang_bt_id", "barang_bt_id", "");
	$barang_modal->addElement("", $id_hidden);
	$sisa_hidden = new Hidden("barang_bt_sisa", "barang_bt_sisa", "");
	$barang_modal->addElement("", $sisa_hidden);
	$nama_barang_text = new Text("barang_bt_nama_barang", "barang_bt_nama_barang", "");
	$nama_barang_text->setAtribute("disabled='disabled'");
	$barang_modal->addElement("Nama Barang", $nama_barang_text);
	$medis_hidden = new Hidden("barang_bt_medis", "barang_bt_medis", "");
	$barang_modal->addElement("", $medis_hidden);
	$hna_hidden = new Hidden("barang_bt_hna", "barang_bt_hna", "");
	$barang_modal->addElement("", $hna_hidden);
	$kode_text = new Text("barang_bt_kode", "barang_bt_kode", "");
	$barang_modal->addElement("Kode", $kode_text);
	$merk_text = new Text("barang_bt_merk", "barang_bt_merk", "");
	$barang_modal->addElement("Merk", $merk_text);
	$tahun_perolehan_text = new Text("barang_bt_tahun_perolehan", "barang_bt_tahun_perolehan", "");
	$tahun_perolehan_text->setClass("mydate");
	$barang_modal->addElement("Tahun", $tahun_perolehan_text);
	$usia_penyusutan_text = new Text("barang_bt_usia_penyusutan", "barang_bt_usia_penyusutan", "");
	$barang_modal->addElement("Usia Susut (th)", $usia_penyusutan_text);
	$barang_button = new Button("", "", "Simpan");
	$barang_button->setClass("btn-success");
	$barang_button->setAction("barang_bt.save()");
	$barang_button->setIcon("fa fa-floppy-o");
	$barang_button->setIsButton(Button::$ICONIC);
	$barang_modal->addFooter($barang_button);
	$barang_modal->addHTML(
	"<div class='alert alert-block alert-inverse' id='usia_susut_info'>" .
		 "<h4>Informasi</h4>" .
		 "Berikut ini adalah aturan taksiran umur ekonomis atas perolehan aset untuk pengisian <b>Usia Susut (th)</b>:" .
		 "<ol>
			<li>Gedung Permanen: 20 Tahun</li>
			<li>Peralatan/Mesin dan Peralatan Medis: 5 Tahun</li>
			<li>Komputer dan Alat Elektronik: 2 Tahun</li>
			<li>Kendaraan Bermotor: 5 Tahun</li>
			<li>Peralatan Kantor dan Meubelir: 5 Tahun</li>
		</ol>" .
	 "</div>", "before");
	
	echo $barang_modal->getHtml();
	echo $barang_table->getHtml();
	echo addJS("framework/smis/js/table_action.js");
	echo addJS("framework/bootstrap/js/bootstrap-datepicker.js");
	echo addCSS("framework/bootstrap/css/datepicker.css");
?>
<script type="text/javascript">
	function BarangBTAction(name, page, action, column) {
		this.initialize(name, page, action, column);
	}
	BarangBTAction.prototype.constructor = BarangBTAction;
	BarangBTAction.prototype = new TableAction();
	BarangBTAction.prototype.edit = function(id) {
		var data = this.getRegulerData();
		data['command'] = "edit";
		data['id'] = id;
		$.post(
			"",
			data,
			function(response) {
				var today = new Date();
				var json = getContent(response);
				if (json == null) return;
				$("#barang_bt_id").val(json.id);
				$("#barang_bt_sisa").val(json.sisa);
				$("#barang_bt_nama_barang").val(json.nama_barang);
				$("#barang_bt_medis").val(json.medis);
				$("#barang_bt_hna").val(json.hna);
				$("#barang_bt_kode").val("");
				$("#barang_bt_merk").val("");
				$("#barang_bt_tahun_perolehan").val(today.getFullYear() + "-" + (today.getMonth()+1) + "-" + today.getDate());
				$("#barang_bt_usia_penyusutan").val(5);
				$("#modal_alert_barang_bt_add_form").html("");
				$(".error_field").removeClass("error_field");
				$("#barang_bt_add_form").smodal("show");
			}
		);
	};
	BarangBTAction.prototype.validate = function() {
		var valid = true;
		var invalid_msg = "";
		var kode = $("#barang_bt_kode").val();
		var merk = $("#barang_bt_merk").val();
		var tahun_perolehan = $("#barang_bt_tahun_perolehan").val();
		var usia_penyusutan = $("#barang_bt_usia_penyusutan").val();
		var unit = $("#barang_bt_unit").val();
		$(".error_field").removeClass("error_field");
		if (kode == "") {
			valid = false;
			invalid_msg += "</br><strong>Kode</strong> tidak boleh kosong";
			$("#barang_bt_kode").addClass("error_field");
		} 
		if (merk == "") {
			valid = false;
			invalid_msg += "</br><strong>Merk</strong> tidak boleh kosong";
			$("#barang_bt_merk").addClass("error_field");
		} 
		if (tahun_perolehan == "") {
			valid = false;
			invalid_msg += "</br><strong>Thn. Perolehan</strong> tidak boleh kosong";
			$("#barang_bt_tahun_perolehan").addClass("error_field");
		} 
		if (!valid) {
			$("#modal_alert_barang_bt_add_form").html(
				"<div class='alert alert-block alert-danger'>" +
					"<h4>Peringatan</h4>" +
					invalid_msg +
				"</div>"
			);
		}
		return valid;
	};
	BarangBTAction.prototype.save = function() {
		if (!this.validate()) {
			return;
		}
		$("#barang_bt_add_form").smodal("hide");
		showLoading();
		var self = this;
		var data = this.getRegulerData();
		data['command'] = "save";
		data['push_command'] = "insert";
		data['id'] = $("#barang_bt_id").val();
		data['sisa'] = parseInt($("#barang_bt_sisa").val()) - 1;
		data['nama_barang'] = $("#barang_bt_nama_barang").val();
		data['medis'] = $("#barang_bt_medis").val();
		data['hna'] = $("#barang_bt_hna").val();
		data['kode'] = $("#barang_bt_kode").val();
		data['merk'] = $("#barang_bt_merk").val();
		data['tahun_perolehan'] = $("#barang_bt_tahun_perolehan").val();
		data['usia_penyusutan'] = $("#barang_bt_usia_penyusutan").val();
		$.post(
			"",
			data,
			function(response) {
				var json = getContent(response);
				if (json == null) {
					$("#barang_bt_add_form").smodal("show");
				} else {
					self.view();
					barang_t.view();
				}
				dismissLoading();
			}
		);
	};

	var barang_bt;
	$(document).ready(function() {
		$('[data-toggle="popover"]').popover({
			trigger: 'hover',
			'placement': 'top'
		});
		$(".mydate").datepicker();
		var barang_bt_columns = new Array("id", "nama_barang", "kode", "merk", "tahun_perolehan", "usia_penyusutan");
		barang_bt = new BarangBTAction(
			"barang_bt",
			"gudang_umum",
			"daftar_barang_belum_terinventarisasi",
			barang_bt_columns
		);
		barang_bt.view();
	});
</script>