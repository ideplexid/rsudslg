<?php 
	require_once("smis-base/smis-include-service-consumer.php");
	global $db;
	
	$laporan_form = new Form("linmpr_form", "", "Gudang Umum : Laporan Inventaris Non-Medis Per Ruangan");
	
	class UnitInventarisNonMedisServiceConsumer extends ServiceConsumer {
		public function __construct($db) {
			parent::__construct ( $db, "get_entity", "push_inventaris" );
		}
		public function proceedResult() {
			$content = array ();
			$result = json_decode ( $this->result, true );
			foreach ( $result as $autonomous ) {
				foreach ( $autonomous as $entity ) {
					$option = array ();
					$option ['value'] = $entity;
					$option ['name'] = ArrayAdapter::format("unslug", $entity ) ;
					$number = count ( $content );
					$content [$number] = $option;
				}
			}
			return $content;
		}
	}
	$unit_inventaris_service_consumer = new UnitInventarisNonMedisServiceConsumer($db);
	$unit_inventaris_service_consumer->execute();
	$unit_option = $unit_inventaris_service_consumer->getContent();
	$unit_select = new Select("linmpr_unit", "linmpr_unit", $unit_option);
	$laporan_form->addElement("Ruangan", $unit_select);
	$show_button = new Button("", "", "Tampilkan");
	$show_button->setClass("btn-primary");
	$show_button->setIcon("icon-white icon-repeat");
	$show_button->setIsButton(Button::$ICONIC);
	$show_button->setAction("linmpr.view()");
	$print_button = new Button("", "", "Cetak");
	$print_button->setClass("btn-inverse");
	$print_button->setIcon("icon-white icon-print");
	$print_button->setIsButton(Button::$ICONIC);
	$print_button->setAction("linmpr.print()");
	$btn_group = new ButtonGroup("noprint");
	$btn_group->addButton($show_button);
	$btn_group->addButton($print_button);
	$laporan_form->addElement("", $btn_group);
	
	$linmpr_table = new Table(
		array("Kode", "Nama Barang", "Merk", "Jumlah", "Tahun Perolehan", "Keadaan", "Lokasi"),
		"",
		null,
		true
	);
	$linmpr_table->setName("linmpr");
	$linmpr_table->setAction(false);
	
	if (isset($_POST['command'])) {
		class LLSOAdapter extends ArrayAdapter {
			public function adapt($row) {
				$array = array();
				$array['id'] = "";
				$array['Kode'] = $row->kode;
				$array['Nama Barang'] = $row->nama_barang;
				$array['Merk'] = $row->merk;
				$array['Jumlah'] = self::format("number", "1");
				$array['Tahun Perolehan'] = self::format("date Y", $row->tahun_perolehan);
				$array['Keadaan'] = self::format("trivial_1_Baik_Rusak", $row->kondisi_baik);
				$array['Lokasi'] = self::format("unslug", $row->unit);
				return $array;
			}
		}
		$linmpr_adapter = new LLSOAdapter();		
		$linmpr_dbtable = new DBTable($db, "smis_gd_inventaris");
		$filter = "1";
		if (isset($_POST['kriteria'])) {
			$filter .= " AND (kode LIKE '%" . $_POST['kriteria'] . "%' OR nama_barang LIKE '%" . $_POST['kriteria'] . "%' OR merk LIKE '%" . $_POST['kriteria'] . "%' OR unit LIKE '%" . $_POST['kriteria'] . "%') ";
		}
		$query_value = "
			SELECT *
			FROM (
				SELECT *
				FROM smis_gd_inventaris
				WHERE prop NOT LIKE 'del' AND medis = '0' AND unit = '" . $_POST['unit'] . "'
				ORDER BY unit, nama_barang ASC
			) v_linmpr
			WHERE " . $filter . "
		";
		$query_count = "
			SELECT COUNT(*)
			FROM (
				SELECT *
				FROM smis_gd_inventaris
				WHERE prop NOT LIKE 'del' AND medis = '0' AND unit = '" . $_POST['unit'] . "'
				ORDER BY unit, nama_barang ASC
			) v_linmpr
			WHERE " . $filter . "
		";
		$linmpr_dbtable->setPreferredQuery(true, $query_value, $query_count);
		class LLSODBResponder extends DBResponder {
			public function command($command) {
				if ($command != "print_laporan") {
					return parent::command($command);
				}
				$pack = new ResponsePackage();
				if ($command == "print_laporan") {
					$content = $this->print_report();
					$pack->setContent($content);
					$pack->setStatus(ResponsePackage::$STATUS_OK);
				}
				return $pack->getPackage();
			}
			public function print_report() {
				$unit = $_POST['unit'];
				$data = $this->dbtable->get_result("
					SELECT *
					FROM (
						SELECT *
						FROM smis_gd_inventaris
						WHERE prop NOT LIKE 'del' AND medis = '0' AND unit = '" . $unit . "'
						ORDER BY unit, nama_barang ASC
					) v_linmpr
				");
				$print_data = "";
				$print_data .= "<center><strong>LAPORAN INVENTARIS NON-MEDIS PER RUANGAN</strong></center><br/>";
				$print_data .= "
								<table border='0'>
									<tr>
										<td>Ruangan</td>
										<td>:</td>
										<td>" . ArrayAdapter::format("unslug", $unit) . "</td>
									</tr>
									<tr>
										<td>Periode</td>
										<td>:</td>
										<td>" . ArrayAdapter::format("date d M Y", date("Y-m-d")) . "</td>
									</tr>
								</table>
				";
				$print_data .= "<table border='1'>
									<tr>
										<th>No.</th>
										<th>Kode</th>
										<th>Nama Barang</th>
										<th>Merk</th>
										<th>Jumlah</th>
										<th>Tahun Perolehan</th>
										<th>Keadaan - Baik</th>
										<th>Keadaan - Rusak</th>
									</tr>";
				$total_jumlah = 0;
				$total_jumlah_baik = 0;
				$total_jumlah_rusak = 0;
				if (count($data) > 0) {
					$no = 1;
					foreach($data as $d) {
						$tanggal_part = explode(" ", $d->tanggal);
						$time_part = explode(":", $tanggal_part[1]);
						$kondisi_sub = "<td><center>&#10003;</center></td><td>&Tab;</td>";
						if ($d->kondisi_baik == false) {
							$kondisi_sub = "<td>&Tab;</td><td><center>&#10003;</center></td>";
							$total_jumlah_rusak += 1;
						} else {
							$total_jumlah_baik += 1;
						}
						$print_data .= "<tr>
											<td>" . $no++ . "</td>
											<td>" . $d->kode . "</td>
											<td>" . $d->nama_barang . "</td>
											<td>" . $d->merk . "</td>
											<td>" . ArrayAdapter::format("number", "1") . "</td>
											<td><center>" . ArrayAdapter::format("date Y", $d->tahun_perolehan) . "</center></td>
											" . $kondisi_sub . "
										</tr>";
						$total_jumlah += 1;
					}
				} else {
					$print_data .= "<tr>
										<td colspan='8' align='center'><i>Tidak terdapat data inventaris non-medis</i></td>
									</tr>";
				}
				$print_data .= "<tr>
									<td colspan='4' align='center'><b>T O T A L</b></td>
									<td><b>" . ArrayAdapter::format("number", $total_jumlah) . "</b></td>
									<td>&Tab;</td>
									<td><b>" . ArrayAdapter::format("number", $total_jumlah_baik) . "</b></td>
									<td><b>" . ArrayAdapter::format("number", $total_jumlah_rusak) . "</b></td>
								</tr>";
				$print_data .= "</table><br/>";
				$print_data .= "<table border='0' align='center' class='united'>
									<tr>
										<td colspan='3' align='center'>Mengetahui :</td>
									</tr>
									<tr>
										<td>&nbsp;</td>
									</tr>
									<tr>
										<td align='center'>Jember, " . ArrayAdapter::format("date d M Y", date("Y-m-d")) . "</td>
										<td>&Tab;</td>
										<td>&nbsp;</td>
									</tr>
									<tr>
										<td align='center'>KAUR. GUDANG NON OBAT</td>
										<td>&Tab;</td>
										<td align='center'>KA. RUANGAN " . ArrayAdapter::format("unslug", $unit) . "</td>
									</tr>
									<tr>
										<td>&nbsp;</td>
										<td>&Tab;</td>
										<td>&nbsp;</td>
									</tr>
									<tr>
										<td>&nbsp;</td>
										<td>&Tab;</td>
										<td>&nbsp;</td>
									</tr>
									<tr>
										<td>&nbsp;</td>
										<td>&Tab;</td>
										<td>&nbsp;</td>
									</tr>
									<tr>
										<td>&nbsp;</td>
										<td>&Tab;</td>
										<td>&nbsp;</td>
									</tr>
									<tr>
										<td align='center'><b>Suharyanto</b></td>
										<td>&Tab;</td>
									<td align='center'>(_____________________)</td>
									</tr>
								</table>";
				return $print_data;
			}
		}
		$linmpr_dbresponder = new LLSODBResponder(
			$linmpr_dbtable,
			$linmpr_table,
			$linmpr_adapter
		);
		$data = $linmpr_dbresponder->command($_POST['command']);
		echo json_encode($data);
		return;
	}
	
	echo $laporan_form->getHtml();
	echo "<div id='table_content'>";
	echo $linmpr_table->getHtml();
	echo "</div>";
	echo addJS("framework/smis/js/table_action.js");
?>
<script type="text/javascript">
	function LINMPRAction(name, page, action, column) {
		this.initialize(name, page, action, column);
	}
	LINMPRAction.prototype.constructor = LINMPRAction;
	LINMPRAction.prototype = new TableAction();
	LINMPRAction.prototype.getViewData = function() {
		var data = TableAction.prototype.getViewData.call(this);
		data['unit'] = $("#linmpr_unit").val();
		return data;
	};
	LINMPRAction.prototype.print = function() {
		var data = this.getRegulerData();
		data['command'] = "print_laporan";
		data['unit'] = $("#linmpr_unit").val();
		$.post(
			"",
			data,
			function(response) {
				var json = getContent(response);
				if (json == null) return;
				smis_print(json);
			}
		);
	};
	
	var linmpr;
	$(document).ready(function() {
		$('[data-toggle="popover"]').popover({
			trigger: 'hover',
			'placement': 'top'
		});
		linmpr = new LINMPRAction(
			"linmpr",
			"gudang_umum",
			"laporan_inventaris_non_medis_per_ruang",
			new Array()
		)
	});
</script>