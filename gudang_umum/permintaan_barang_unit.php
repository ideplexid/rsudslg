<?php 
	require_once("smis-base/smis-include-service-consumer.php");
	require_once("smis-base/smis-include-duplicate.php");
	
	class PermintaanBarangTable extends Table {
		public function getBodyContent(){
			$content = "";
			if ($this->content!=NULL) {
				foreach ($this->content as $d) {
					$content .= "<tr>";
					foreach ($this->header as $h) {
						$content .= "<td>" . $d[$h] . "</td>";
					}
					if ($this->is_action) {
						$content .= "<td>" . $this->getFilteredContentButton($d['id'], $d['status'])->getHtml() . "</td>";
					}
					$content .= "</tr>";
				}
			}
			return $content;
		}
		public function getFilteredContentButton($id, $status) {
			$btn_group = new ButtonGroup("noprint");
			if ($status != "sudah") {
				$btn = new Button("", "", "Ubah");
				$btn->setAction($this->action . ".edit('" . $id . "')");
				$btn->setClass("btn-warning");
				$btn->setAtribute("data-content='Ubah' data-toggle='popover'");
				$btn->setIcon("icon-edit icon-white");
				$btn->setIsButton(Button::$ICONIC);
				$btn_group->addElement($btn);
			} else {
				$btn = new Button("", "", "View");
				$btn->setAction($this->action . ".detail('" . $id . "')");
				$btn->setClass("btn-success");
				$btn->setAtribute("data-content='View' data-toggle='popover'");
				$btn->setIcon("icon-eye-open icon-white");
				$btn->setIsButton(Button::$ICONIC);
				$btn_group->addElement($btn);
			} 
			return $btn_group;
		}
	}
	$permintaan_barang_table = new PermintaanBarangTable(
		array("Unit", "Nomor", "Tanggal", "Status"),
		"Gudang Umum : Permintaan Barang Unit",
		null,
		true
	);
	$permintaan_barang_table->setName("permintaan_barang");
	$permintaan_barang_table->setAddButtonEnable(false);
	
	if (isset($_POST['command'])) {
		class PermintaanBarangAdapter extends ArrayAdapter {
			public function adapt($row) {
				$array = array();
				$array['id'] = $row->id;
				$array['status'] = $row->status;
				$array['Unit'] = self::format("unslug", $row->unit);
				$array['Nomor'] = self::format("digit8", $row->f_id);
				$array['Tanggal'] = self::format("date d M Y", $row->tanggal);
				if ($row->status == "belum") {
					$array['Status'] = "Belum Diterima";
				} else if ($row->status == "sudah") {
					$array['Status'] = "Sudah Diterima";
				}
				return $array;
			}
		}
		$permintaan_barang_adapter = new PermintaanBarangAdapter();
		$columns = array("id", "f_id", "unit", "tanggal", "status");
		$permintaan_barang_dbtable = new DBTable(
			$db,
			"smis_gd_permintaan_barang_unit",
			$columns
		);
		$permintaan_barang_dbtable->addCustomKriteria(" status ", " !='dikembalikan' ");
		class PermintaanBarangDBResponder extends DuplicateResponder {
			public function save() {
				$header_data = $this->postToArray();
				$id['id'] = $_POST['id'];
				//do update here:
				$result = $this->dbtable->update($header_data, $id);
				$success['type'] = "update";
				if (isset($_POST['detail'])) {
					//do update detail here:
					$detail_dbtable = new DBTable($this->dbtable->get_db(), "smis_gd_dpermintaan_barang_unit");
					$detail = $_POST['detail'];
					foreach($detail as $d) {
						$detail_data = array();
						$detail_data['jumlah_dipenuhi'] = $d['jumlah_dipenuhi'];
						$detail_data['satuan_dipenuhi'] = $d['satuan_dipenuhi'];
						$detail_data['keterangan'] = $d['keterangan'];
						$detail_data['autonomous'] = "[".$this->getAutonomous()."]";
				        $detail_data['duplicate'] = 0;
				        $detail_data['time_updated'] = date("Y-m-d H:i:s");
				        $detail_data['origin_updated'] = $this->getAutonomous();
						$detail_id['id'] = $d['id'];
						$detail_dbtable->update($detail_data, $detail_id);
					}
				}
				$success['id'] = $id['id'];
				$success['success'] = 1;
				if ($result === false) $success['success'] = 0;
				return $success;
			}
			public function edit() {
				$id = $_POST['id'];
				$header_row = $this->dbtable->get_row("
					SELECT *
					FROM smis_gd_permintaan_barang_unit
					WHERE id = '" . $id . "'
				");
				$data['header'] = $header_row;
				$detail_dbtable = new DBTable($this->dbtable->get_db(), "smis_gd_dpermintaan_barang_unit");
				$data['detail'] = $detail_dbtable->get_result("
					SELECT *
					FROM smis_gd_dpermintaan_barang_unit
					WHERE id_permintaan_barang = '" . $id . "' AND prop NOT LIKE 'del'
				");
				return $data;
			}
		}
		$permintaan_barang_dbresponder = new PermintaanBarangDBResponder(
			$permintaan_barang_dbtable,
			$permintaan_barang_table,
			$permintaan_barang_adapter
		);
		$data = $permintaan_barang_dbresponder->command($_POST['command']);
		if (isset($_POST['push_command'])) {
			class SetPermintaanBarangStatusServiceConsumer extends ServiceConsumer {
				public function __construct($db, $f_id, $unit, $status, $detail, $command) {
					$array['id'] = $f_id;
					$array['status'] = $status;
					$array['detail'] = json_encode($detail);
					$array['command'] = $command;
					parent::__construct($db, "set_permintaan_barang_status", $array, $unit);
				}
				public function proceedResult() {
					$content = array();
					$result = json_decode($this->result,true);
					foreach ($result as $autonomous) {
						foreach ($autonomous as $entity) {
							if ($entity != null || $entity != "")
								return $entity;
						}
					}
					return $content;
				}
			}
			$permintaan_barang_dbtable = new DBTable($db, "smis_gd_permintaan_barang_unit");
			$permintaan_barang_row = $permintaan_barang_dbtable->get_row("	
				SELECT *
				FROM smis_gd_permintaan_barang_unit
				WHERE id = '" . $data['content']['id'] . "'
			");
			$dpermintaan_barang_dbtable = new DBTable($db, "smis_ap_dpermintaan_barang");
			$detail = $dpermintaan_barang_dbtable->get_result("
				SELECT *
				FROM smis_gd_dpermintaan_barang_unit
				WHERE id_permintaan_barang = '" . $data['content']['id'] . "' AND prop NOT LIKE 'del'
			");
			$command = "push_" . $_POST['push_command'];
			$set_status_permintaan_barang_service_consumer = new SetPermintaanBarangStatusServiceConsumer($db, $permintaan_barang_row->f_id, $permintaan_barang_row->unit, $permintaan_barang_row->status, $detail, $command);
			$set_status_permintaan_barang_service_consumer->execute();
		}
		echo json_encode($data);
		return;
	}
	
	$permintaan_barang_modal = new Modal("permintaan_barang_add_form", "smis_form_container", "permintaan_barang");
	$permintaan_barang_modal->setTitle("Permintaan Barang");
	$permintaan_barang_modal->setClass(Modal::$FULL_MODEL);
	$id_hidden = new Hidden("permintaan_barang_id", "permintaan_barang_id", "");
	$permintaan_barang_modal->addElement("", $id_hidden);
	$unit_text = new Text("permintaan_barang_unit", "permintaan_barang_unit", "");
	$unit_text->setAtribute("disabled='disabled'");
	$permintaan_barang_modal->addElement("Unit", $unit_text);
	$tanggal_text = new Text("permintaan_barang_tanggal", "permintaan_barang_tanggal", "");
	$tanggal_text->setAtribute("disabled='disabled'");
	$permintaan_barang_modal->addElement("Tanggal", $tanggal_text);
	$status_option = array(
		array(
			'name'	=> "Belum Diterima",
			'value'	=> "belum"
		),
		array(
			'name'	=> "Diterima",
			'value'	=> "sudah"
		),
		array(
			'name'	=> "Dikembalikan",
			'value'	=> "dikembalikan"
		)
	);
	$status_select = new Select("permintaan_barang_status", "permintaan_barang_status", $status_option);
	$permintaan_barang_modal->addElement("Status", $status_select);
	$dpermintaan_barang_table = new Table(
		array("Barang", "Jml. Permintaan", "Jml. Dipenuhi", "Keterangan"),
		"",
		null,
		true
	);
	$dpermintaan_barang_table->setName("dpermintaan_barang");
	$dpermintaan_barang_table->setAddButtonEnable(false);
	$dpermintaan_barang_table->setReloadButtonEnable(false);
	$dpermintaan_barang_table->setPrintButtonEnable(false);
	$dpermintaan_barang_table->setFooterVisible(false);
	$permintaan_barang_modal->addBody("dpermintaan_barang_table", $dpermintaan_barang_table);
	$permintaan_barang_button = new Button("", "", "Simpan");
	$permintaan_barang_button->setClass("btn-success");
	$permintaan_barang_button->setAtribute("id='permintaan_barang_save'");
	$permintaan_barang_button->setIcon("fa fa-floppy-o");
	$permintaan_barang_button->setIsButton(Button::$ICONIC);
	$permintaan_barang_modal->addFooter($permintaan_barang_button);
	$permintaan_barang_button = new Button("", "", "OK");
	$permintaan_barang_button->setClass("btn-success");
	$permintaan_barang_button->setAtribute("id='permintaan_barang_ok'");
	$permintaan_barang_button->setAction("$($(this).data('target')).smodal('hide')");
	$permintaan_barang_modal->addFooter($permintaan_barang_button);
	
	$dpermintaan_barang_modal = new Modal("dpermintaan_barang_add_form", "smis_form_container", "dpermintaan_barang");
	$dpermintaan_barang_modal->setTitle("Detail Permintaan Barang");
	$id_hidden =  new Hidden("dpermintaan_barang_id", "dpermintaan_barang_id", "");
	$dpermintaan_barang_modal->addElement("", $id_hidden);
	$nama_barang_text = new Text("dpermintaan_barang_nama_barang", "dpermintaan_barang_nama_barang", "");
	$nama_barang_text->setAtribute("disabled='disabled'");
	$dpermintaan_barang_modal->addElement("Barang", $nama_barang_text);
	$f_jumlah_diminta_text = new Text("dpermintaan_barang_f_jumlah_diminta", "dpermintaan_barang_f_jumlah_diminta", "");
	$f_jumlah_diminta_text->setAtribute("disabled='disabled'");
	$dpermintaan_barang_modal->addElement("Jm. Diminta", $f_jumlah_diminta_text);
	$jumlah_dipenuhi_text = new Text("dpermintaan_barang_jumlah_dipenuhi", "dpermintaan_barang_jumlah_dipenuhi", "");
	$dpermintaan_barang_modal->addElement("Jm. Dipenuhi", $jumlah_dipenuhi_text);
	$satuan_dipenuhi_text = new Text("dpermintaan_barang_satuan_dipenuhi", "dpermintaan_barang_satuan_dipenuhi", "");
	$dpermintaan_barang_modal->addElement("Satuan", $satuan_dipenuhi_text);
	$keterangan_textarea = new TextArea("dpermintaan_barang_keterangan", "dpermintaan_barang_keterangan", "");
	$dpermintaan_barang_modal->addElement("Keterangan", $keterangan_textarea);
	$dpermintaan_barang_button = new Button("", "", "Simpan");
	$dpermintaan_barang_button->setClass("btn-success");
	$dpermintaan_barang_button->setAtribute("id='dpermintaan_barang_save'");
	$dpermintaan_barang_button->setIcon("fa fa-floppy-o");
	$dpermintaan_barang_button->setIsButton(Button::$ICONIC);
	$dpermintaan_barang_modal->addFooter($dpermintaan_barang_button);
	
	echo $dpermintaan_barang_modal->getHtml();
	echo $permintaan_barang_modal->getHtml();
	echo $permintaan_barang_table->getHtml();
	echo addJS("framework/smis/js/table_action.js");
?>
<script type="text/javascript">
	function PermintaanBarangAction(name, page, action, column) {
		this.initialize(name, page, action, column);
	}
	PermintaanBarangAction.prototype.constructor = PermintaanBarangAction;
	PermintaanBarangAction.prototype = new TableAction();
	PermintaanBarangAction.prototype.edit = function(id) {
		var data = this.getRegulerData();
		data['command'] = "edit";
		data['id'] = id;
		$.post(
			"",
			data,
			function(response) {
				var json = getContent(response);
				if (json == null) return;
				$("#permintaan_barang_id").val(json.header.id);
				$("#permintaan_barang_unit").val(json.header.unit.replace("_", " ").toUpperCase());
				$("#permintaan_barang_tanggal").val(json.header.tanggal);
				$("#permintaan_barang_status").val(json.header.status);
				$("#permintaan_barang_status").removeAttr("disabled");
				row_id = 0;
				$("tbody#dpermintaan_barang_list").children("tr").remove();
				$("#modal_alert_permintaan_barang_add_form").html("");
				$(".error_field").removeClass("error_field");
				for(var i = 0; i < json.detail.length; i++) {
					var d_id = json.detail[i].id;
					var d_id_permintaan_barang = json.detail[i].id_permintaan_barang;
					var d_nama_barang = json.detail[i].nama_barang;
					var d_jumlah_diminta = json.detail[i].jumlah_permintaan;
					var d_satuan_diminta = json.detail[i].satuan_permintaan;
					var d_jumlah_dipenuhi = json.detail[i].jumlah_dipenuhi;
					var d_satuan_dipenuhi = json.detail[i].satuan_dipenuhi;
					var d_keterangan = json.detail[i].keterangan;
					var f_jumlah_dipenuhi = "-";
					if (Number(d_jumlah_dipenuhi) != 0) {
						f_jumlah_dipenuhi = d_jumlah_dipenuhi + " " + d_satuan_dipenuhi;
					}
					$("tbody#dpermintaan_barang_list").append(
						"<tr id='data_" + row_id + "'>" +
							"<td id='data_" + row_id + "_id' style='display: none;'>" + d_id + "</td>" +
							"<td id='data_" + row_id + "_jumlah_diminta' style='display: none;'>" + d_jumlah_diminta + "</td>" +
							"<td id='data_" + row_id + "_satuan_diminta' style='display: none;'>" + d_satuan_diminta + "</td>" +
							"<td id='data_" + row_id + "_jumlah_dipenuhi' style='display: none;'>" + d_jumlah_dipenuhi + "</td>" +
							"<td id='data_" + row_id + "_satuan_dipenuhi' style='display: none;'>" + d_satuan_dipenuhi + "</td>" +
							"<td id='data_" + row_id + "_nama_barang'>" + d_nama_barang + "</td>" +
							"<td id='data_" + row_id + "_f_jumlah_diminta'>" + d_jumlah_diminta + " " + d_satuan_diminta + "</td>" +
							"<td id='data_" + row_id + "_f_jumlah_dipenuhi'>" + f_jumlah_dipenuhi + "</td>" +
							"<td id='data_" + row_id + "_keterangan'>" + d_keterangan + "</td>" +
							"<td>" +
								"<div class='btn-group noprint'>" +
									"<a href='#' onclick='dpermintaan_barang.edit(" + row_id + ")' data-content='Ubah' data-toggle='popover' class='input btn btn-warning'>" + 
										"<i class='icon-edit icon-white'></i>" +
									 "</a>" +
								"</div>" +
							"</td>" +
						"</tr>"
					);
					row_id++;
				}
				$("#permintaan_barang_save").removeAttr("onclick");
				$("#permintaan_barang_save").attr("onclick", "permintaan_barang.update(" + id + ")");
				$("#permintaan_barang_save").show();
				$("#permintaan_barang_ok").hide();
				$("#modal_alert_permintaan_barang_add_form").html("");
				$(".error_field").removeClass("error_field");
				$("#permintaan_barang_add_form").smodal("show");
			}
		);
	};
	PermintaanBarangAction.prototype.update = function(id) {
		$("#permintaan_barang_add_form").smodal("hide");
		showLoading();
		var self = this;
		var data = this.getRegulerData();
		data['command'] = "save";
		data['push_command'] = "update";
		data['id'] = $("#permintaan_barang_id").val();
		data['status'] = $("#permintaan_barang_status").val();
		var detail = {};
		var nor = $("tbody#dpermintaan_barang_list").children("tr").length;
		for(var i = 0; i < nor; i++) {
			var d_data = {};
			var prefix = $("tbody#dpermintaan_barang_list").children('tr').eq(i).prop("id");
			d_data['id'] = $("#" + prefix + "_id").text();
			d_data['jumlah_dipenuhi'] = $("#" + prefix + "_jumlah_dipenuhi").text();
			d_data['satuan_dipenuhi'] = $("#" + prefix + "_satuan_dipenuhi").text();
			d_data['keterangan'] = $("#" + prefix + "_keterangan").text();
			detail[i] = d_data;
		}
		data['detail'] = detail;
		$.post(
			"",
			data,
			function(response) {
				var json = getContent(response);
				if (json == null) {
					$("#permintaan_barang_add_form").smodal("show");
				} else {
					self.view();
				}
				dismissLoading();
			}
		);
	};
	PermintaanBarangAction.prototype.detail = function(id) {
		var data = this.getRegulerData();
		data['command'] = "edit";
		data['id'] = id;
		$.post(
			"",
			data,
			function(response) {
				var json = getContent(response);
				if (json == null) return;
				$("#permintaan_barang_id").val(json.header.id);
				$("#permintaan_barang_unit").val(json.header.unit.replace("_", " ").toUpperCase());
				$("#permintaan_barang_tanggal").val(json.header.tanggal);
				$("#permintaan_barang_status").val(json.header.status);
				$("#permintaan_barang_status").removeAttr("disabled");
				$("#permintaan_barang_status").attr("disabled", "disabled");
				row_id = 0;
				$("tbody#dpermintaan_barang_list").children("tr").remove();
				$("#modal_alert_permintaan_barang_add_form").html("");
				$(".error_field").removeClass("error_field");
				for(var i = 0; i < json.detail.length; i++) {
					var d_id = json.detail[i].id;
					var d_id_permintaan_barang = json.detail[i].id_permintaan_barang;
					var d_nama_barang = json.detail[i].nama_barang;
					var d_jumlah_diminta = json.detail[i].jumlah_permintaan;
					var d_satuan_diminta = json.detail[i].satuan_permintaan;
					var d_jumlah_dipenuhi = json.detail[i].jumlah_dipenuhi;
					var d_satuan_dipenuhi = json.detail[i].satuan_dipenuhi;
					var d_keterangan = json.detail[i].keterangan;
					var f_jumlah_dipenuhi = "-";
					if (Number(d_jumlah_dipenuhi) != 0) {
						f_jumlah_dipenuhi = d_jumlah_dipenuhi + " " + d_satuan_dipenuhi;
					}
					$("tbody#dpermintaan_barang_list").append(
						"<tr id='data_" + row_id + "'>" +
							"<td id='data_" + row_id + "_id' style='display: none;'>" + d_id + "</td>" +
							"<td id='data_" + row_id + "_jumlah_diminta' style='display: none;'>" + d_jumlah_diminta + "</td>" +
							"<td id='data_" + row_id + "_satuan_diminta' style='display: none;'>" + d_satuan_diminta + "</td>" +
							"<td id='data_" + row_id + "_jumlah_dipenuhi' style='display: none;'>" + d_jumlah_dipenuhi + "</td>" +
							"<td id='data_" + row_id + "_satuan_dipenuhi' style='display: none;'>" + d_satuan_dipenuhi + "</td>" +
							"<td id='data_" + row_id + "_nama_barang'>" + d_nama_barang + "</td>" +
							"<td id='data_" + row_id + "_f_jumlah_diminta'>" + d_jumlah_diminta + " " + d_satuan_diminta + "</td>" +
							"<td id='data_" + row_id + "_f_jumlah_dipenuhi'>" + f_jumlah_dipenuhi + "</td>" +
							"<td id='data_" + row_id + "_keterangan'>" + d_keterangan + "</td>" +
							"<td></td>" +
						"</tr>"
					);
					row_id++;
				}
				$("#permintaan_barang_save").removeAttr("onclick");
				$("#permintaan_barang_save").hide();
				$("#permintaan_barang_ok").show();
				$("#modal_alert_permintaan_barang_add_form").html("");
				$(".error_field").removeClass("error_field");
				$("#permintaan_barang_add_form").smodal("show");
			}
		);
	};
	
	function DPermintaanBarangAction(name, page, action, column) {
		this.initialize(name, page, action, column);
	}
	DPermintaanBarangAction.prototype.constructor = DPermintaanBarangAction;
	DPermintaanBarangAction.prototype = new TableAction();
	DPermintaanBarangAction.prototype.validate = function() {
		var valid = true;
		var invalid_msg = "";
		var jumlah_dipenuhi = $("#dpermintaan_barang_jumlah_dipenuhi").val();
		var satuan_dipenuhi = $("#dpermintaan_barang_satuan_dipenuhi").val();
		var keterangan = $("#dpermintaan_barang_keterangan").val();
		$(".error_field").removeClass("error_field");
		if (jumlah_dipenuhi == "") {
			valid = false;
			invalid_msg += "<br/><strong>Jm. Dipenuhi</strong> tidak boleh kosong";
			$("#dpermintaan_barang_jumlah_dipenuhi").addClass("error_field");
		} else if (!is_numeric(jumlah_dipenuhi)) {
			valid = false;
			invalid_msg += "<br/><strong>Jm. Dipenuhi</strong> seharusnya numerik (0-9)";
			$("#dpermintaan_barang_jumlah_dipenuhi").addClass("error_field");
		}
		if (satuan_dipenuhi == "") {
			valid = false;
			invalid_msg += "<br/><strong>Satuan</strong> tidak boleh kosong";
			$("#dpermintaan_barang_satuan_dipenuhi").addClass("error_field");
		}
		if (keterangan == "") {
			valid = false;
			invalid_msg += "<br/><strong>Keterangan</strong> tidak boleh kosong";
			$("#dpermintaan_barang_satuan_keterangan").addClass("error_field");
		}
		if (!valid) {
			$("#modal_alert_dpermintaan_barang_add_form").html(
				"<div class='alert alert-block alert-danger'>" +
					"<h4>Peringatan</h4>" +
					invalid_msg +
				"</div>"
			);
		}
		return valid;
	};
	DPermintaanBarangAction.prototype.edit = function(r_num) {
		var id = $("#data_" + r_num + "_id").text();
		var nama_barang = $("#data_" + r_num + "_nama_barang").text();
		var jumlah_diminta = $("#data_" + r_num + "_jumlah_diminta").text();
		var satuan_diminta = $("#data_" + r_num + "_satuan_diminta").text();
		var jumlah_dipenuhi = $("#data_" + r_num + "_jumlah_dipenuhi").text();
		var satuan_dipenuhi = $("#data_" + r_num + "_satuan_dipenuhi").text();
		var keterangan = $("#data_" + r_num + "_keterangan").text();
		$("#dpermintaan_barang_id").val(id);
		$("#dpermintaan_barang_nama_barang").val(nama_barang);
		$("#dpermintaan_barang_f_jumlah_diminta").val(jumlah_diminta + " " + satuan_diminta);
		$("#dpermintaan_barang_jumlah_dipenuhi").val(jumlah_dipenuhi);
		$("#dpermintaan_barang_satuan_dipenuhi").val(satuan_dipenuhi);
		$("#dpermintaan_barang_keterangan").val(keterangan);
		$("#dpermintaan_barang_save").removeAttr("onclick");
		$("#dpermintaan_barang_save").attr("onclick", "dpermintaan_barang.update(" + r_num + ")");
		$("#dpermintaan_barang_add_form").smodal("show");
	};
	DPermintaanBarangAction.prototype.update = function(r_num) {
		if (!this.validate()) {
			return;
		}
		var jumlah_dipenuhi = $("#dpermintaan_barang_jumlah_dipenuhi").val();
		var satuan_dipenuhi = $("#dpermintaan_barang_satuan_dipenuhi").val();
		var keterangan = $("#dpermintaan_barang_keterangan").val();
		$("#data_" + r_num + "_jumlah_dipenuhi").text(jumlah_dipenuhi);
		$("#data_" + r_num + "_satuan_dipenuhi").text(satuan_dipenuhi);
		$("#data_" + r_num + "_f_jumlah_dipenuhi").text(jumlah_dipenuhi + " " + satuan_dipenuhi);
		$("#data_" + r_num + "_keterangan").text(keterangan);
		$("#dpermintaan_barang_add_form").smodal("hide");
	};
	
	var permintaan_barang;
	var dpermintaan_barang;
	var row_id;
	$(document).ready(function() {
		$('[data-toggle="popover"]').popover({
			trigger: 'hover',
			'placement': 'top'
		});
		var dpermintaan_barang_columns = new Array("id", "f_id", "nama_barang", "jumlah_permintaan", "satuan_permintaan", "jumlah_dipenuhi", "satuan_dipenuhi", "keterangan");
		dpermintaan_barang = new DPermintaanBarangAction(
			"dpermintaan_barang",
			"gudang_umum",
			"permintaan_barang_unit",
			dpermintaan_barang_columns
		);
		var permintaan_barang_columns = new Array("id", "f_id", "unit", "tanggal", "status");
		permintaan_barang = new PermintaanBarangAction(
			"permintaan_barang",
			"gudang_umum",
			"permintaan_barang_unit",
			permintaan_barang_columns
		);
		permintaan_barang.view();
	});
</script>