<?php
	global $PLUGINS;
	
	$init['name'] = "gudang_umum";
	$init['path'] = SMIS_DIR . $init['name'] . "/";
	$init['description'] = "Gudang Umum";
	$init['require'] = "administrator";
	$init['service'] = "";
	$init['version'] = "1.0.0";
	$init['number'] = "1";
	$init['type'] = "";
	
	$plugin = new Plugin($init);
	$PLUGINS[$init['name']] = $plugin;
?>