<?php 
	global $db;
	
	$lso_table = new Table(
		array("Jenis Barang", "Jumlah", "Total H. Netto"),
		"Gudang Umum : Laporan Rekap Perbekalan Barang",
		null,
		true
	);
	$lso_table->setName("lso");
	$lso_table->setAddButtonEnable(false);
	$lso_table->setEditButtonEnable(false);
	$lso_table->setDelButtonEnable(false);
	
	if (isset($_POST['command'])) {
		class LLSOAdapter extends ArrayAdapter {
			public function adapt($row) {
				$array = array();
				$array['id'] = "";
				$array['Jenis Barang'] = $row->nama_jenis_barang;
				$array['Jumlah'] = self::format("number", $row->jumlah);
				$array['Total H. Netto'] = self::format("money Rp. ", $row->total_hna);
				return $array;
			}
		}
		$lso_adapter = new LLSOAdapter();		
		$lso_dbtable = new DBTable($db, "smis_gd_stok_barang");
		$filter = "1";
		if (isset($_POST['kriteria'])) {
			$filter .= " AND (nama_jenis_barang LIKE '%" . $_POST['kriteria'] . "%') ";
		}
		$query_value = "
			SELECT *
			FROM (
				SELECT nama_jenis_barang, SUM(sisa) AS 'jumlah', SUM(sisa * hna) AS 'total_hna'
				FROM smis_gd_stok_barang
				WHERE prop NOT LIKE 'del'
				GROUP BY nama_jenis_barang
			) v_lso
			WHERE " . $filter . "
			ORDER BY nama_jenis_barang ASC
		";
		$query_count = "
			SELECT COUNT(*)
			FROM (
				SELECT nama_jenis_barang, SUM(sisa) AS 'jumlah', SUM(sisa * hna) AS 'total_hna'
				FROM smis_gd_stok_barang
				WHERE prop NOT LIKE 'del'
				GROUP BY nama_jenis_barang
			) v_lso
			WHERE " . $filter . "
			ORDER BY nama_jenis_barang ASC
		";
		$lso_dbtable->setPreferredQuery(true, $query_value, $query_count);
		class LLSODBResponder extends DBResponder {
			public function command($command) {
				if ($command != "print_laporan") {
					return parent::command($command);
				}
				$pack = new ResponsePackage();
				if ($command == "print_laporan") {
					$content = $this->print_report();
					$pack->setContent($content);
					$pack->setStatus(ResponsePackage::$STATUS_OK);
				}
				return $pack->getPackage();
			}
			public function print_report() {
				$data = $this->dbtable->get_result("
					SELECT *
					FROM (
						SELECT nama_jenis_barang, SUM(sisa) AS 'jumlah', SUM(sisa * hna) AS 'total_hna'
						FROM smis_gd_stok_barang
						WHERE prop NOT LIKE 'del'
						GROUP BY nama_jenis_barang
					) v_lso
					ORDER BY nama_jenis_barang ASC
				");
				$print_data = "";
				$print_data .= "<center><strong>LAPORAN REKAPITULASI PERBEKALAN BARANG GUDANG UMUM</strong></center><br/>";
				$print_data .= "Periode : " . ArrayAdapter::format("date d M Y", date("Y-m-d"));
				$print_data .= "<table border='1'>
									<tr>
										<th>No.</th>
										<th>Jenis Barang</th>
										<th>Jumlah</th>
										<th>Total Harga Netto</th>
									</tr>";
				$total_jumlah = 0;
				$total_netto = 0;
				if (count($data) > 0) {
					$no = 1;
					foreach($data as $d) {
						$tanggal_part = explode(" ", $d->tanggal);
						$time_part = explode(":", $tanggal_part[1]);
						$print_data .= "<tr>
											<td>" . $no++ . "</td>
											<td>" . $d->nama_jenis_barang . "</td>
											<td>" . ArrayAdapter::format("number", $d->jumlah) . "</td>
											<td>" . ArrayAdapter::format("money Rp. ", $d->total_hna) . "</td>
										</tr>";
						$total_jumlah += $d->jumlah;
						$total_netto += $d->total_hna;
					}
				} else {
					$print_data .= "<tr>
										<td colspan='4' align='center'><i>Tidak terdapat data stok barang</i></td>
									</tr>";
				}
				$print_data .= "<tr>
									<td colspan='2' align='center'><b>T O T A L</b></td>
									<td><b>" . ArrayAdapter::format("number", $total_jumlah) . "</b></td>
									<td><b>" . ArrayAdapter::format("money Rp. ", $total_netto) . "</b></td>
								</tr>";
				$print_data .= "</table><br/>";
				global $user;
				$print_data .= "<table border='0' align='center' class='united'>
									<tr>
										<td colspan='3' align='center'>Mengetahui :</td>
									</tr>
									<tr>
										<td>&nbsp;</td>
									</tr>
									<tr>
										<td align='center'>Jember, " . ArrayAdapter::format("date d M Y", date("Y-m-d")) . "</td>
										<td>&Tab;</td>
										<td>&nbsp;</td>
									</tr>
									<tr>
										<td align='center'>KAUR. GUDANG NON OBAT</td>
										<td>&Tab;</td>
										<td align='center'>PETUGAS GUDANG UMUM</td>
									</tr>
									<tr>
										<td>&nbsp;</td>
										<td>&Tab;</td>
										<td>&nbsp;</td>
									</tr>
									<tr>
										<td>&nbsp;</td>
										<td>&Tab;</td>
										<td>&nbsp;</td>
									</tr>
									<tr>
										<td>&nbsp;</td>
										<td>&Tab;</td>
										<td>&nbsp;</td>
									</tr>
									<tr>
										<td>&nbsp;</td>
										<td>&Tab;</td>
										<td>&nbsp;</td>
									</tr>
									<tr>
										<td align='center'><b>Suharyanto</b></td>
										<td>&Tab;</td>
										<td align='center'><b>" . $user->getNameOnly() . "</b></td>
									</tr>
								</table>";
				return $print_data;
			}
		}
		$lso_dbresponder = new LLSODBResponder(
			$lso_dbtable,
			$lso_table,
			$lso_adapter
		);
		$data = $lso_dbresponder->command($_POST['command']);
		echo json_encode($data);
		return;
	}
	
	echo $lso_table->getHtml();
	echo addJS("framework/smis/js/table_action.js");
?>
<script type="text/javascript">
	function LLSOAction(name, page, action, column) {
		this.initialize(name, page, action, column);
	}
	LLSOAction.prototype.constructor = LLSOAction;
	LLSOAction.prototype = new TableAction();
	LLSOAction.prototype.print = function() {
		var data = this.getRegulerData();
		data['command'] = "print_laporan";
		$.post(
			"",
			data,
			function(response) {
				var json = getContent(response);
				if (json == null) return;
				smis_print(json);
			}
		);
	};
	
	var lso;
	$(document).ready(function() {
		$('[data-toggle="popover"]').popover({
			trigger: 'hover',
			'placement': 'top'
		});
		lso = new LLSOAction(
			"lso",
			"gudang_umum",
			"laporan_rekap_perbekalan_barang",
			new Array()
		)
	});
</script>