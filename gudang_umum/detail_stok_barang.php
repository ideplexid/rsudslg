<?php 
	require_once("smis-base/smis-include-duplicate.php");

	class PenyesuaianStokTable extends Table {
		public function getContentButton($id) {
			$btn_group = new ButtonGroup("noprint");
			$btn = new Button("", "", "Ubah");
			$btn->setAction($this->action . ".edit('" . $id . "')");
			$btn->setClass("btn-warning");
			$btn->setAtribute("data-content='Ubah' data-toggle='popover'");
			$btn->setIcon("icon-white icon-edit");
			$btn->setIsButton(Button::$ICONIC);
			$btn_group->addElement($btn);
			return $btn_group;
		}
	}
	$penyesuaian_stok_table = new PenyesuaianStokTable(
		array("Nomor", "Nama Barang", "Jenis Barang", "Stok", "Produsen", "Keterangan", "Tanggal Exp."),
		"",
		null,
		true
	);
	$penyesuaian_stok_table->setName("penyesuaian_stok");
	$penyesuaian_stok_table->setAddButtonEnable(false);
	
	if (isset($_POST['command'])) {
		class PenyesuaianStokAdapter extends ArrayAdapter {
			public function adapt($row) {
				$array = array();
				$array['id'] = $row->id;
				$array['Nomor'] = self::format("digit8", $row->id);
				$array['Nama Barang'] = $row->nama_barang;
				$array['Jenis Barang'] = $row->nama_jenis_barang;
				$array['Stok'] = $row->sisa . " " . $row->satuan;
				$array['Produsen'] = $row->produsen;
				$array['Keterangan'] = "1 " . $row->satuan . " = " . $row->konversi . " " . $row->satuan_konversi;
				if ($row->tanggal_exp == "0000-00-00")
					$array['Tanggal Exp.'] = "-";
				else
					$array['Tanggal Exp.'] = self::format("date d M Y", $row->tanggal_exp);
				return $array;
			}
		}
		$penyesuaian_stok_adapter = new PenyesuaianStokAdapter();
		$columns = array("id", "id_stok_barang", "tanggal", "jumlah_baru", "jumlah_lama", "keterangan", "nama_user");
		$penyesuaian_stok_dbtable = new DBTable(
			$db,
			"smis_gd_penyesuaian_stok",
			$columns
		);
		$filter = "";
		if (isset($_POST['kriteria'])) {
			$filter = " AND (smis_gd_stok_barang.nama_barang LIKE '%" . $_POST['kriteria'] . "%' OR smis_gd_stok_barang.nama_jenis_barang LIKE '%" . $_POST['kriteria'] . "%')";
		}
		$query_value = "
			SELECT smis_gd_stok_barang.*
			FROM smis_gd_stok_barang
			WHERE smis_gd_stok_barang.prop NOT LIKE 'del' " . $filter . "
			ORDER BY smis_gd_stok_barang.nama_barang, smis_gd_stok_barang.tanggal_exp ASC
		";
		$query_count = "
			SELECT COUNT(*)
			FROM smis_gd_stok_barang
			WHERE smis_gd_stok_barang.prop NOT LIKE 'del' " . $filter . "
		";
		$penyesuaian_stok_dbtable->setPreferredQuery(true, $query_value, $query_count);
		class PenyesuaianStokDBResponder extends DuplicateResponder {
			public function save() {
				$data = $this->postToArray();
				$result = $this->dbtable->insert($data);
				$stok_barang_dbtable = new DBTable($this->dbtable->get_db(), "smis_gd_stok_barang");
				$stok_data = array();
				$stok_data['sisa'] = $_POST['jumlah_baru'];
				$stok_data['autonomous'] = "[".$this->getAutonomous()."]";
		        $stok_data['duplicate'] = 0;
		        $stok_data['time_updated'] = date("Y-m-d H:i:s");
		        $stok_data['origin_updated'] = $this->getAutonomous();
				$stok_id['id'] = $_POST['id_stok_barang'];
				$stok_barang_dbtable->update($stok_data, $stok_id);
				//logging riwayat stok barang:
				$riwayat_dbtable = new DBTable($this->dbtable->get_db(), "smis_gd_riwayat_stok_barang");
				$data_riwayat = array();
				$data_riwayat['tanggal'] = date("Y-m-d");
				$data_riwayat['id_stok_barang'] = $_POST['id_stok_barang'];
				if ($_POST['jumlah_baru'] > $_POST['jumlah_lama'])
					$data_riwayat['jumlah_masuk'] = $_POST['jumlah_baru'] - $_POST['jumlah_lama'];
				else
					$data_riwayat['jumlah_keluar'] = $_POST['jumlah_lama'] - $_POST['jumlah_baru'];
				$data_riwayat['sisa'] = $_POST['jumlah_baru'];
				$data_riwayat['keterangan'] = "Penyesuaian Stok";
				global $user;
				$data_riwayat['nama_user'] = $user->getName();
				$data_riwayat['autonomous'] = "[".$this->getAutonomous()."]";
		        $data_riwayat['duplicate'] = 0;
		        $data_riwayat['time_updated'] = date("Y-m-d H:i:s");
		        $data_riwayat['origin_updated'] = $this->getAutonomous();
		        $data_riwayat['origin'] = $this->getAutonomous();
				$riwayat_dbtable->insert($data_riwayat);
				$success['type'] = "insert";
				$success['id'] = $this->dbtable->get_inserted_id();
				$success['success'] = 1;
				if ($result === false) $success['success'] = 0;
				return $success;
			}
			public function edit() {
				$id = $_POST['id'];
				$data = $this->dbtable->get_row("
					SELECT smis_gd_stok_barang.*
					FROM smis_gd_stok_barang
					WHERE smis_gd_stok_barang.id = '" . $id . "'
				");
				return $data;
			}
		}
		$penyesuaian_stok_dbresponder = new PenyesuaianStokDBResponder(
			$penyesuaian_stok_dbtable,
			$penyesuaian_stok_table,
			$penyesuaian_stok_adapter
		);
		if ($penyesuaian_stok_dbresponder->is("save")) {
			global $user;
			$penyesuaian_stok_dbresponder->addColumnFixValue("nama_user", $user->getName());
		}
		$data = $penyesuaian_stok_dbresponder->command($_POST['command']);
		echo json_encode($data);
		return;
	}
	
	$penyesuaian_stok_modal = new Modal("penyesuaian_stok_add_form", "smis_form_container", "penyesuaian_stok");
	$penyesuaian_stok_modal->setTitle("Penyesuaian Stok Barang");
	$id_stok_barang_hidden = new Hidden("penyesuaian_stok_id_stok_barang", "penyesuaian_stok_id_stok_barang", "");
	$penyesuaian_stok_modal->addElement("", $id_stok_barang_hidden);
	$tanggal_text = new Text("penyesuaian_stok_tanggal", "penyesuaian_stok_tanggal", "");
	$tanggal_text->setClass("mydate");
	$tanggal_text->setAtribute("data-date-format='yyyy-m-d'");
	$penyesuaian_stok_modal->addElement("Tanggal", $tanggal_text);
	$nama_barang_text = new Text("penyesuaian_stok_nama_barang", "penyesuaian_stok_nama_barang", "");
	$nama_barang_text->setAtribute("disabled='disabled'");
	$penyesuaian_stok_modal->addElement("Nama Barang", $nama_barang_text);
	$nama_jenis_text = new Text("penyesuaian_stok_nama_jenis", "penyesuaian_stok_nama_jenis", "");
	$nama_jenis_text->setAtribute("disabled='disabled'");
	$penyesuaian_stok_modal->addElement("Jenis Barang", $nama_jenis_text);
	$produsen_text = new Text("penyesuaian_stok_produsen", "penyesuaian_stok_produsen", "");
	$produsen_text->setAtribute("disabled='disabled'");
	$penyesuaian_stok_modal->addElement("Produsen", $produsen_text);
	$f_jumlah_lama_text = new Text("penyesuaian_stok_f_jumlah_lama", "penyesuaian_stok_f_jumlah_lama", "");
	$f_jumlah_lama_text->setAtribute("disabled='disabled'");
	$penyesuaian_stok_modal->addElement("Jml. Tercatat", $f_jumlah_lama_text);
	$jumlah_lama_hidden = new Hidden("penyesuaian_stok_jumlah_lama", "penyesuaian_stok_jumlah_lama", "");
	$penyesuaian_stok_modal->addElement("", $jumlah_lama_hidden);
	$jumlah_baru_text = new Text("penyesuaian_stok_jumlah_baru", "penyesuaian_stok_jumlah_baru", "");
	$penyesuaian_stok_modal->addElement("Jml. Aktual", $jumlah_baru_text);
	$satuan_text = new text("penyesuaian_stok_satuan", "penyesuaian_stok_satuan", "");
	$satuan_text->setAtribute("disabled='disabled'");
	$penyesuaian_stok_modal->addElement("Satuan", $satuan_text);
	$keterangan_textarea = new TextArea("penyesuaian_stok_keterangan", "penyesuaian_stok_keterangan", "");
	$penyesuaian_stok_modal->addElement("Keterangan", $keterangan_textarea);
	$penyesuaian_stok_button = new Button("", "", "Simpan");
	$penyesuaian_stok_button->setClass("btn-success");
	$penyesuaian_stok_button->setIcon("fa fa-floppy-o");
	$penyesuaian_stok_button->setIsButton(Button::$ICONIC);
	$penyesuaian_stok_button->setAction("penyesuaian_stok.save()");
	$penyesuaian_stok_modal->addFooter($penyesuaian_stok_button);
	
	echo $penyesuaian_stok_modal->getHtml();
	echo $penyesuaian_stok_table->getHtml();
	echo addJS("framework/smis/js/table_action.js");
	echo addJS("framework/bootstrap/js/bootstrap-datepicker.js");
	echo addCSS("framework/bootstrap/css/datepicker.css");
?>
<script type="text/javascript">
	function PenyesuaianStokAction(name, page, action, column) {
		this.initialize(name, page, action, column);
	}
	PenyesuaianStokAction.prototype.constructor = PenyesuaianStokAction;
	PenyesuaianStokAction.prototype = new TableAction();
	PenyesuaianStokAction.prototype.edit = function(id) {
		var data = this.getRegulerData();
		data['command'] = "edit";
		data['id'] = id;
		$.post(
			"",
			data,
			function(response) {
				var json = getContent(response);
				if (json == null) return;
				$("#penyesuaian_stok_id_stok_barang").val(id);
				var today = new Date();
				$("#penyesuaian_stok_tanggal").val(today.getFullYear() + "-" + (today.getMonth()+1) + "-" + today.getDate());
				$("#penyesuaian_stok_nama_barang").val(json.nama_barang);
				$("#penyesuaian_stok_nama_jenis").val(json.nama_jenis_barang);
				$("#penyesuaian_stok_produsen").val(json.produsen);
				$("#penyesuaian_stok_f_jumlah_lama").val(json.sisa + " " + json.satuan);
				$("#penyesuaian_stok_jumlah_lama").val(json.sisa);
				$("#penyesuaian_stok_jumlah_baru").val(json.sisa);
				$("#penyesuaian_stok_satuan").val(json.satuan);
				$("#penyesuaian_stok_keterangan").val("");
				$("#modal_alert_penyesuaian_stok_add_form").html("");
				$(".error_field").removeClass("error_field");
				$("#penyesuaian_stok_add_form").smodal("show");
			}
		);
	};
	PenyesuaianStokAction.prototype.validate = function() {
		var valid = true;
		var invalid_msg = "";
		var tanggal = $("#penyesuaian_stok_tanggal").val();
		var jumlah_aktual = $("#penyesuaian_stok_jumlah_baru").val();
		var jumlah_tercatat = $("#penyesuaian_stok_jumlah_tercatat").val();
		var keterangan = $("#penyesuaian_stok_keterangan").val();
		$(".error_field").removeClass("error_field");
		if (jumlah_aktual == "") {
			valid = false;
			invalid_msg += "</br><strong>Jml. Aktual</strong> tidak boleh kosong";
			$("#penyesuaian_stok_jumlah_baru").addClass("error_field");
		} else if (!is_numeric(jumlah_aktual)) {
			valid = false;
			invalid_msg += "</br><strong>Jml. Aktual</strong> seharusnya numerik (0-9)";
			$("#penyesuaian_stok_jumlah_baru").addClass("error_field");
		} else if (parseFloat(jumlah_aktual) == parseFloat(jumlah_tercatat)) {
			valid = false;
			invalid_msg += "</br><strong>Jml. Aktual</strong> dan <strong>Jml. Tercatat</strong> tidak terdapat selisih";
			$("#penyesuaian_stok_jumlah_baru").addClass("error_field");
		}
		if (tanggal == "") {
			valid = false;
			invalid_msg += "</br><strong>Tanggal</strong> tidak boleh kosong";
			$("#penyesuaian_stok_tanggal").addClass("error_field");
		}
		if (keterangan == "") {
			valid = false;
			invalid_msg += "</br><strong>Keterangan</strong> tidak boleh kosong";
			$("#penyesuaian_stok_keterangan").addClass("error_field");
		}
		if (!valid) {
			$("#modal_alert_penyesuaian_stok_add_form").html(
				"<div class='alert alert-block alert-danger'>" +
					"<h4>Peringatan</h4>" +
					invalid_msg +
				"</div>"
			);
		}
		return valid;
	};
	PenyesuaianStokAction.prototype.save = function() {
		if (!this.validate()) {
			return;
		}
		$("#penyesuaian_stok_add_form").smodal("hide");
		showLoading();
		var self = this;
		var data = this.getRegulerData();
		data['command'] = "save";
		data['id'] = "";
		data['id_stok_barang'] = $("#penyesuaian_stok_id_stok_barang").val();
		data['tanggal'] = $("#penyesuaian_stok_tanggal").val();
		data['jumlah_baru'] = $("#penyesuaian_stok_jumlah_baru").val();
		data['jumlah_lama'] = $("#penyesuaian_stok_jumlah_lama").val();
		data['keterangan'] = $("#penyesuaian_stok_keterangan").val();
		$.post(
			"",
			data,
			function(response) {
				var json = getContent(response);
				if (json == null) {
					$("#penyesuaian_stok_add_form").smodal("show");
				} else {
					self.view();
					riwayat_penyesuaian_stok.view();
				}
				dismissLoading();
			}
		);
	};
	
	var penyesuaian_stok;
	$(document).ready(function() {
		$('[data-toggle="popover"]').popover({
			trigger: 'hover',
			'placement': 'top'
		});
		$(".mydate").datepicker();
		var penyesuaian_stok_columns = new Array("id", "id_stok_barang", "tanggal", "jumlah_baru", "jumlah_lama", "keterangan");
		penyesuaian_stok = new PenyesuaianStokAction(
			"penyesuaian_stok",
			"gudang_umum",
			"detail_stok_barang",
			penyesuaian_stok_columns
		);
		penyesuaian_stok.view();
	});
</script>