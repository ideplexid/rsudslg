<?php 
	require_once("smis-base/smis-include-service-consumer.php");
	
	class BarangMasukTable extends Table {
		public function getContentButton($id) {
			$btn_group = new ButtonGroup("noprint");
			$btn = new Button("", "", "View");
			$btn->setAction($this->action . ".detail('" . $id . "')");
			$btn->setClass("btn-success");
			$btn->setAtribute("data-content='View' data-toggle='popover'");
			$btn->setIcon("icon-eye-open icon-white");
			$btn->setIsButton(Button::$ICONIC);
			$btn_group->addElement($btn);
			$btn = new Button("", "", "Hapus");
			$btn->setAction($this->action . ".del('" . $id . "')");
			$btn->setClass("btn-danger");
			$btn->setAtribute("data-content='Hapus' data-toggle='popover'");
			$btn->setIcon("icon-remove icon-white");
			$btn->setIsButton(Button::$ICONIC);
			$btn_group->addElement($btn);
			return $btn_group;
		}
	}
	$barang_masuk_table = new BarangMasukTable(
		array("Nomor", "Tanggal Masuk", "Nomor SP", "Nomor Faktur"),
		"Gudang Umum : Barang Masuk",
		null,
		true
	);
	$barang_masuk_table->setName("barang_masuk");
	
	if (isset($_POST['super_command']) && $_POST['super_command'] == "barang_masuk") {
		if (isset($_POST['command'])) {
			class BarangMasukAdapter extends ArrayAdapter {
				public function adapt($row) {
					$array = array();
					$array['id'] = $row->id;
					$array['Nomor'] = self::format("digit8", $row->id);
					$array['Tanggal Masuk'] = self::format("date d M Y", $row->tanggal);
					$array['Nomor SP'] = self::format("digit8", $row->id_po);
					$array['Nomor Faktur'] = $row->no_faktur;
					return $array;
				}
			}
			$barang_masuk_adapter = new BarangMasukAdapter();
			$barang_masuk_dbtable = new DBTable($db,"smis_gd_barang_masuk");
			$barang_masuk_dbtable->addCustomKriteria(" id ", " > 0 ");
			$barang_masuk_dbtable->addCustomKriteria(" id_po ", " != 0 ");
			class BarangMasukDBResponder extends DBResponder {
				public function save() {
					$header_data = $this->postToArray();
					$id['id'] = $_POST['id'];
					if ($id['id'] == 0 || $id['id'] == "") {
						//do insert header here:
						$result = $this->dbtable->insert($header_data);
						$id['id'] = $this->dbtable->get_inserted_id();
						$success['type'] = "insert";
						$subtotal = 0;
						if (isset($_POST['detail'])) {
							//do insert detail here:
							$detail_dbtable = new DBTable($this->dbtable->get_db(), "smis_gd_dbarang_masuk");
							$detail = $_POST['detail'];
							foreach($detail as $d) {
								$detail_data = array();
								$detail_data['id_barang_masuk'] = $id['id'];
								$detail_data['id_barang'] = $d['id_barang'];
								$detail_data['nama_barang'] = $d['nama_barang'];
								$detail_data['medis'] = $d['medis'];
								$detail_data['inventaris'] = $d['inventaris'];
								$detail_data['nama_jenis_barang'] = $d['nama_jenis_barang'];
								$detail_data['id_dpo'] = $d['id_dpo'];
								$detail_data['jumlah'] = $d['jumlah'];
								$detail_data['sisa'] = $d['sisa'];
								$detail_data['satuan'] = $d['satuan'];
								$detail_data['konversi'] = $d['konversi'];
								$detail_data['satuan_konversi'] = $d['satuan_konversi'];
								$detail_data['hna'] = $d['hna'];
								$detail_data['selisih'] = $d['selisih'];
								$detail_data['produsen'] = $d['produsen'];
								$detail_data['diskon'] = $d['diskon'];
								$detail_data['t_diskon'] = $d['t_diskon'];
								$detail_dbtable->insert($detail_data);
								$subtotal = $subtotal + ($d['hna'] * $d['jumlah']);
							}
						}
						$success['subtotal'] = $subtotal;
					} else {
						//do update header here:
						$result = $this->dbtable->update($header_data, $id);
						$success['type'] = "update";
						$subtotal = 0;
						if (isset($_POST['detail'])) {
							//do update detail here:
							$detail_dbtable = new DBTable($this->dbtable->get_db(), "smis_gd_dbarang_masuk");
							$detail = $_POST['detail'];
							foreach($detail as $d) {
								$detail_data = array();
								$detail_data['id_barang_masuk'] = $id['id'];
								$detail_data['id_barang'] = $d['id_barang'];
								$detail_data['nama_barang'] = $d['nama_barang'];
								$detail_data['medis'] = $d['medis'];
								$detail_data['inventaris'] = $d['inventaris'];
								$detail_data['nama_jenis_barang'] = $d['nama_jenis_barang'];
								$detail_data['id_dpo'] = $d['id_dpo'];
								$detail_data['jumlah'] = $d['jumlah'];
								$detail_data['sisa'] = $d['sisa'];
								$detail_data['satuan'] = $d['satuan'];
								$detail_data['konversi'] = $d['konversi'];
								$detail_data['satuan_konversi'] = $d['satuan_konversi'];
								$detail_data['hna'] = $d['hna'];
								$detail_data['selisih'] = $d['selisih'];
								$detail_data['produsen'] = $d['produsen'];
								$detail_data['diskon'] = $d['diskon'];
								$detail_data['t_diskon'] = $d['t_diskon'];
								if ($d['cmd'] == "insert") {
									$detail_dbtable->insert($detail_data);
								} else if ($d['cmd'] == "update") {
									//get different (jumlah - sisa):
									$detail_row = $detail_dbtable->get_row("
										SELECT jumlah, sisa
										FROM smis_gd_dbarang_masuk
										WHERE id = '" . $d['id'] . "'
									");
									$selisih = $detail_row->jumlah - $detail_row->sisa;
									$detail_data['sisa'] = $d['jumlah'] - $selisih;
									$detail_data['jumlah'] = $d['jumlah'];
									$detail_id_data['id'] = $d['id'];
									$detail_dbtable->update($detail_data, $detail_id_data);
								} else if ($d['cmd'] == "delete") {
									$detail_data = array();
									$detail_data['prop'] = "del";
									$detail_id_data['id'] = $d['id'];
									$detail_dbtable->update($detail_data, $detail_id_data);
								}
								$subtotal = $subtotal + ($d['hna'] * $d['jumlah']);
							}
						}
						$success['subtotal'] = $subtotal;
					}
					$success['id'] = $id['id'];
					$success['success'] = 1;
					if ($result === false) $success['success'] = 0;
					else {
						if ($success['type'] == "insert") {
							/// notify accounting :
							$h_row = $this->dbtable->get_row("
								SELECT *
								FROM smis_gd_barang_masuk
								WHERE id = '" . $id['id'] . "'
							");
							$total = 0;
							$drows = $this->dbtable->get_result("
								SELECT *
								FROM smis_gd_dbarang_masuk
								WHERE id_barang_masuk = '" . $id['id'] . "' AND prop NOT LIKE 'del'
							");
							if ($drows != null) {
								foreach ($drows as $dr) {
									$subtotal = $dr->jumlah * ($dr->hna / 1.1);
									$diskon = $dr->diskon;
									if ($dr->t_diskon == "persen")
										$diskon = $subtotal * $dr->diskon / 100;
									$subtotal = round($subtotal - $diskon);
									$total += $subtotal;
								}
							}
							$global_diskon = $h_row->diskon;
							if ($h_row->t_diskon == "persen")
								$global_diskon = $global_diskon * $total / 100;
							$global_diskon = floor($global_diskon);
							$total = $total - $global_diskon;
							$ppn = floor($total * 0.1);
							$total = $total + $ppn;

							$data = array(
								"jenis_akun"	=> "transaction",
								"jenis_data"	=> "pembelian",
								"id_data"		=> $id['id'],
								"entity"		=> "gudang_umum",
								"service"		=> "get_detail_accounting_bbm",
								"data"			=> $id['id'],
								"code"			=> "bbm-gudang_umum-" . $id['id'],
								"operation"		=> "",
								"tanggal"		=> $h_row->tanggal_datang,
								"uraian"		=> "BBM No. Faktur " . $h_row->no_faktur . " -  ID Transaksi " . $id['id'] . " dari " . $h_row->nama_vendor . " pada tanggal " . ArrayAdapter::format("date d-m-Y", $h_row->tanggal_datang),
								"nilai"			=> $total
							);
							$service_consumer = new ServiceConsumer(
								$this->dbtable->get_db(),
								"push_notify_accounting",
								$data,
								"accounting"
							);
							$service_consumer->execute();
						}
					}
					return $success;
				}
				public function edit() {
					$id = $_POST['id'];
					$header_row = $this->dbtable->get_row("
						SELECT *
						FROM smis_gd_barang_masuk
						WHERE id = '" . $id . "'
					");
					$data['header'] = $header_row;
					$detail_dbtable = new DBTable($this->dbtable->get_db(), "smis_gd_dbarang_masuk");
					$data['detail'] = $detail_dbtable->get_result("
						SELECT *
						FROM smis_gd_dbarang_masuk
						WHERE id_barang_masuk = '" . $id . "' AND prop NOT LIKE 'del'
					");
					return $data;
				}
				public function delete() {
					$id['id'] = $_POST['id'];
					if ($this->dbtable->isRealDelete()) {
						$result = $this->dbtable->delete(null,$id);
					} else {
						/// notify accounting :
						$h_row = $this->dbtable->get_row("
							SELECT *
							FROM smis_gd_barang_masuk
							WHERE id = '" . $id['id'] . "'
						");
						$total = 0;
						$drows = $this->dbtable->get_result("
							SELECT *
							FROM smis_gd_dbarang_masuk
							WHERE id_barang_masuk = '" . $id['id'] . "' AND prop NOT LIKE 'del'
						");
						if ($drows != null) {
							foreach ($drows as $dr) {
								$subtotal = $dr->jumlah * ($dr->hna / 1.1);
								$diskon = $dr->diskon;
								if ($dr->t_diskon == "persen")
									$diskon = $subtotal * $dr->diskon / 100;
								$subtotal = round($subtotal - $diskon);
								$total += $subtotal;
							}
						}
						$global_diskon = $h_row->diskon;
						if ($h_row->t_diskon == "persen")
							$global_diskon = $global_diskon * $total / 100;
						$global_diskon = floor($global_diskon);
						$total = $total - $global_diskon;
						$ppn = floor($total * 0.1);
						$total = $total + $ppn;

						$data = array(
							"jenis_akun"	=> "transaction",
							"jenis_data"	=> "pembelian",
							"id_data"		=> $id['id'],
							"entity"		=> "gudang_umum",
							"service"		=> "get_detail_accounting_bbm",
							"data"			=> $id['id'],
							"code"			=> "bbm-gudang_umum-" . $id['id'],
							"operation"		=> "del",
							"tanggal"		=> $h_row->tanggal_datang,
							"uraian"		=> "BBM No. Faktur " . $h_row->no_faktur . " -  ID Transaksi " . $id['id'] . " dari " . $h_row->nama_vendor . " pada tanggal " . ArrayAdapter::format("date d-m-Y", $h_row->tanggal_datang),
							"nilai"			=> $total
						);
						$service_consumer = new ServiceConsumer(
							$this->dbtable->get_db(),
							"push_notify_accounting",
							$data,
							"accounting"
						);
						$service_consumer->execute();
						$data['prop'] = "del";
						$result = $this->dbtable->update($data, $id);
					}
					$success['success'] = 1;
					$success['id'] = $_POST['id'];
					if ($result === 'false') $success['success'] = 0;
					return $success;
				}
			}
			$barang_masuk_dbresponder = new BarangMasukDBResponder(
				$barang_masuk_dbtable,
				$barang_masuk_table,
				$barang_masuk_adapter
			);
			$data = $barang_masuk_dbresponder->command($_POST['command']);
			if (isset($_POST['push_command'])) {
				class UpdateSisaDPOServiceConsumer extends ServiceConsumer {
					public function __construct($db, $detail, $command) {
						$array['detail'] = json_encode($detail);
						$array['command'] = $command;
						parent::__construct($db, "update_sisa_dpo", $array, "pembelian");
					}
					public function proceedResult() {
						$content = array();
						$result = json_decode($this->result,true);
						foreach ($result as $autonomous) {
							foreach ($autonomous as $entity) {
								if ($entity != null || $entity != "")
									return $entity;
							}
						}
						return $content;
					}
				}
				$dbarang_masuk_dbtable = new DBTable($db, "smis_gd_dbarang_masuk");
				$dbarang_masuk_rows = $dbarang_masuk_dbtable->get_result("
					SELECT *
					FROM smis_gd_dbarang_masuk
					WHERE id_barang_masuk = '" . $data['content']['id'] . "'
				");
				$command = "push_" . $_POST['push_command'];
				$update_sisa_dpo_service_consumer = new UpdateSisaDPOServiceConsumer($db, $dbarang_masuk_rows, $command);
				$update_sisa_dpo_service_consumer->execute();
			}
			echo json_encode($data);
			return;
		}
		return;
	}
	
	//get daftar po consumer:
	$po_table = new Table(
		array("Nomor", "Tgl. Diterbitkan", "Tgl. Jatuh Tempo", "Vendor"),
		"",
		null,
		true
	);
	$po_table->setName("po");
	$po_table->setModel(Table::$SELECT);
	$po_adapter = new SimpleAdapter();
	$po_adapter->add("Nomor", "id", "digit8");
	$po_adapter->add("Tgl. Diterbitkan", "tanggal", "date d M Y");
	$po_adapter->add("Tgl. Jatuh Tempo", "tanggal_jt", "date d M Y");
	$po_adapter->add("Vendor", "nama_vendor");
	$po_service_responder = new ServiceResponder(
		$db,
		$po_table,
		$po_adapter,
		"get_daftar_opl_non_farmasi"
	);
	
	//get daftar barang per po consumer:
	$barang_table = new Table(
		array("Barang", "Jenis", "Sisa", "Satuan", "Keterangan"),
		"",
		null,
		true
	);
	$barang_table->setName("barang");
	$barang_table->setModel(Table::$SELECT);
	$barang_adapter = new SimpleAdapter();
	$barang_adapter->add("Barang", "nama_barang");
	$barang_adapter->add("Jenis", "nama_jenis_barang");
	$barang_adapter->add("Sisa", "sisa");
	$barang_adapter->add("Satuan", "satuan");
	$barang_adapter->add("Keterangan", "keterangan");
	$barang_service_responder = new ServiceResponder(
		$db,
		$barang_table,
		$barang_adapter,
		"get_daftar_barang_per_opl"
	);
	
	$super_command = new SuperCommand();
	$super_command->addResponder("po", $po_service_responder);
	$super_command->addResponder("barang", $barang_service_responder);
	$init = $super_command->initialize();
	if ($init != null) {
		echo $init;
		return;
	}
	
	$barang_masuk_modal = new Modal("barang_masuk_add_form", "smis_form_container", "barang_masuk");
	$barang_masuk_modal->setTitle("Barang Masuk");
	$barang_masuk_modal->setClass(Modal::$FULL_MODEL);
	$id_hidden = new Hidden("barang_masuk_id", "barang_masuk_id", "");
	$barang_masuk_modal->addElement("", $id_hidden);
	$nopo_button = new Button("", "", "Pilih");
	$nopo_button->setAtribute("id='barang_masuk_nopo_btn'");
	$nopo_button->setClass("btn-info");
	$nopo_button->setIsButton(Button::$ICONIC);
	$nopo_button->setIcon("icon-white ".Button::$icon_list_alt);
	$nopo_button->setAction("po.chooser('po', 'po_button', 'po', po)");
	$nopo_text = new Text("barang_masuk_nopo", "barang_masuk_nopo", "");
	$nopo_text->setClass("smis-one-option-input");
	$nopo_text->setAtribute("disabled='disabled'");
	$nopo_input_group = new InputGroup("");
	$nopo_input_group->addComponent($nopo_text);
	$nopo_input_group->addComponent($nopo_button);
	$barang_masuk_modal->addElement("No. SP", $nopo_input_group);
	$id_vendor_hidden = new Hidden("barang_masuk_id_vendor", "barang_masuk_id_vendor", "");
	$barang_masuk_modal->addElement("", $id_vendor_hidden);
	$nama_vendor_text = new Text("barang_masuk_nama_vendor", "barang_masuk_nama_vendor", "");
	$nama_vendor_text->setAtribute("disabled='disabled'");
	$barang_masuk_modal->addElement("Vendor", $nama_vendor_text);
	$no_faktur_text = new Text("barang_masuk_nofaktur", "barang_masuk_nofaktur", "");
	$barang_masuk_modal->addElement("No. Faktur", $no_faktur_text);
	$tanggal_text = new Text("barang_masuk_tanggal", "barang_masuk_tanggal", "");
	$tanggal_text->setClass("mydate");
	$tanggal_text->setAtribute("data-date-format='yyyy-m-d'");
	$barang_masuk_modal->addElement("Tgl. Faktur", $tanggal_text);
	$tanggal_datang_text = new Text("barang_masuk_tanggal_datang", "barang_masuk_tanggal_datang", "");
	$tanggal_datang_text->setClass("mydate");
	$tanggal_datang_text->setAtribute("data-date-format='yyyy-m-d'");
	$barang_masuk_modal->addElement("Tgl. Datang", $tanggal_datang_text);
	$tanggal_tempo_text = new Text("barang_masuk_tanggal_tempo", "barang_masuk_tanggal_tempo", "");
	$tanggal_tempo_text->setClass("mydate");
	$tanggal_tempo_text->setAtribute("data-date-format='yyyy-m-d'");
	$barang_masuk_modal->addElement("Jatuh Tempo", $tanggal_tempo_text);
	$diskon_text = new Text("barang_masuk_diskon", "barang_masuk_diskon", "");
	$barang_masuk_modal->addElement("Diskon", $diskon_text);
	$t_diskon_option = new OptionBuilder();
	$t_diskon_option->add("Persen (%)", "persen");
	$t_diskon_option->add("Nominal (Rp)", "nominal");
	$t_diskon_select = new Select("barang_masuk_t_diskon", "barang_masuk_t_diskon", $t_diskon_option->getContent());
	$barang_masuk_modal->addElement("Tipe Diskon", $t_diskon_select);
	$keterangan_textarea = new TextArea("barang_masuk_keterangan", "barang_masuk_keterangan", "");
	$keterangan_textarea->setLine(2);
	$barang_masuk_modal->addElement("Keterangan", $keterangan_textarea);
	class DBarangMasukTable extends Table {
		public function getHeaderButton() {
			$btn_group = new ButtonGroup("noprint");
			$btn = new Button("", "", "Tambah");
			$btn->setAction($this->action . ".show_add_form()");
			$btn->setClass("btn-primary");
			$btn->setAtribute("id='dbarang_masuk_add'");
			$btn->setIcon("icon-plus icon-white");
			$btn->setIsButton(Button::$ICONIC);
			$btn_group->addElement($btn);
			return $btn_group->getHtml();
		}
	}
	$dbarang_masuk_table = new DBarangMasukTable(
		array("Nama Barang", "Jenis Barang", "Produsen", "Subtotal", "Jumlah", "Ket. Jumlah", "Diskon"),
		"",
		null,
		true
	);
	$dbarang_masuk_table->setName("dbarang_masuk");
	$dbarang_masuk_table->setFooterVisible(false);
	$barang_masuk_modal->addBody("dbarang_masuk_table", $dbarang_masuk_table);
	$save_button = new Button("", "", "Simpan");
	$save_button->setClass("btn-success");
	$save_button->setAtribute("id='barang_masuk_save'");
	$save_button->setIcon("fa fa-floppy-o");
	$save_button->setIsButton(Button::$ICONIC);
	$barang_masuk_modal->addFooter($save_button);
	$ok_button = new Button("", "", "OK");
	$ok_button->setClass("btn-success");
	$ok_button->setAtribute("id='barang_masuk_ok'");
	$ok_button->setAction("$($(this).data('target')).smodal('hide')");
	$barang_masuk_modal->addFooter($ok_button);
	
	$dbarang_masuk_modal = new Modal("dbarang_masuk_add_form", "smis_form_container", "dbarang_masuk");
	$dbarang_masuk_modal->setTitle("Detail Barang Masuk");
	$id_hidden = new Hidden("dbarang_masuk_id", "dbarang_masuk_id", "");
	$dbarang_masuk_modal->addElement("", $id_hidden);
	$id_barang_hidden = new Hidden("dbarang_masuk_id_barang", "dbarang_masuk_id_barang", "");
	$dbarang_masuk_modal->addElement("", $id_barang_hidden);
	$medis_hidden = new Hidden("dbarang_masuk_medis", "dbarang_masuk_medis", "");
	$dbarang_masuk_modal->addElement("", $medis_hidden);
	$inventaris_hidden = new Hidden("dbarang_masuk_inventaris", "dbarang_masuk_inventaris", "");
	$dbarang_masuk_modal->addElement("", $inventaris_hidden);
	$id_dpo_hidden = new Hidden("dbarang_masuk_id_dpo", "dbarang_masuk_id_dpo", "");
	$dbarang_masuk_modal->addElement("", $id_dpo_hidden);
	$nama_barang_button = new Button("", "", "Pilih");
	$nama_barang_button->setClass("btn-info");
	$nama_barang_button->setAction("barang.chooser('barang', 'barang_button', 'barang', barang)");
	$nama_barang_button->setIsButton(Button::$ICONIC);
	$nama_barang_button->setIcon("icon-white ".Button::$icon_list_alt);
	$nama_barang_text = new Text("dbarang_masuk_nama_barang", "dbarang_masuk_nama_barang", "");
	$nama_barang_text->setClass("smis-one-option-input");
	$nama_barang_text->setAtribute("disabled='disabled'");
	$nama_barang_input_group = new InputGroup("");
	$nama_barang_input_group->addComponent($nama_barang_text);
	$nama_barang_input_group->addComponent($nama_barang_button);
	$dbarang_masuk_modal->addElement("Nama Barang", $nama_barang_input_group);
	$nama_jenis_barang_text = new Text("dbarang_masuk_nama_jenis_barang", "dbarang_masuk_nama_jenis_barang", "");
	$nama_jenis_barang_text->setAtribute("disabled='disabled'");
	$dbarang_masuk_modal->addElement("Jenis Barang", $nama_jenis_barang_text);
	$hna_text = new Text("dbarang_masuk_hna", "dbarang_masuk_hna", "");
	$hna_text->setTypical("money");
	$hna_text->setAtribute(" data-thousands=\".\" data-decimal=\",\" data-prefix=\"Rp. \"  data-precision=\"2\"  disabled='disabled'" );
	$dbarang_masuk_modal->addElement("Herga Satuan", $hna_text);
	$selisih_text = new Text("dbarang_masuk_selisih", "dbarang_masuk_selisih", "");
	$selisih_text->setTypical("money");
	$selisih_text->setAtribute(" data-thousands=\".\" data-decimal=\",\" data-prefix=\"Rp. \"  data-precision=\"2\"" );
	$dbarang_masuk_modal->addElement("Selisih", $selisih_text);
	$jumlah_text = new Text("dbarang_masuk_jumlah", "dbarang_masuk_jumlah", "");
	$dbarang_masuk_modal->addElement("Jumlah", $jumlah_text);
	$satuan_text = new Text("dbarang_masuk_satuan", "dbarang_masuk_satuan", "");
	$satuan_text->setAtribute("disabled='disabled'");
	$dbarang_masuk_modal->addElement("Satuan", $satuan_text);
	$konversi_text = new Text("dbarang_masuk_konversi", "dbarang_masuk_konversi", "");
	$dbarang_masuk_modal->addElement("Jml. Terkecil", $konversi_text);
	$satuan_konversi_text = new Text("dbarang_masuk_satuan_konversi", "dbarang_masuk_satuan_konversi", "");
	$dbarang_masuk_modal->addElement("Stn. Terkecil", $satuan_konversi_text);
	$produsen_text = new Text("dbarang_masuk_produsen", "dbarang_masuk_produsen", "");
	$dbarang_masuk_modal->addElement("Produsen", $produsen_text);
	$diskon_text = new Text("dbarang_masuk_diskon", "dbarang_masuk_diskon", "");
	$dbarang_masuk_modal->addElement("Diskon", $diskon_text);
	$t_diskon_option = new OptionBuilder();
	$t_diskon_option->add("Persen (%)", "persen");
	$t_diskon_option->add("Nominal (Rp)", "nominal");
	$t_diskon_select = new Select("dbarang_masuk_t_diskon", "dbarang_masuk_t_diskon", $t_diskon_option->getContent());
	$dbarang_masuk_modal->addElement("Tipe Diskon", $t_diskon_select);
	$dbarang_masuk_button = new Button("", "", "Simpan");
	$dbarang_masuk_button->setClass("btn-success");
	$dbarang_masuk_button->setAtribute("id='dbarang_masuk_save'");
	$dbarang_masuk_button->setIcon("fa fa-floppy-o");
	$dbarang_masuk_button->setIsButton(Button::$ICONIC);
	$dbarang_masuk_modal->addFooter($dbarang_masuk_button);
	
	echo $dbarang_masuk_modal->getHtml();
	echo $barang_masuk_modal->getHtml();
	echo $barang_masuk_table->getHtml();
	echo addJS("framework/smis/js/table_action.js");
	echo addJS("framework/bootstrap/js/bootstrap-datepicker.js");
	echo addCSS("framework/bootstrap/css/datepicker.css");
?>
<script type="text/javascript">
	function BarangMasukAction(name, page, action, column) {
		this.initialize(name, page, action, column);
	}
	BarangMasukAction.prototype.constructor = BarangMasukAction;
	BarangMasukAction.prototype = new TableAction();
	BarangMasukAction.prototype.validate = function() {
		var valid = true;
		var invalid_msg = "";
		var nopo = $("#barang_masuk_nopo").val();
		var no_faktur = $("#barang_masuk_nofaktur").val();
		var tanggal = $("#barang_masuk_tanggal").val();
		var tanggal_datang = $("#barang_masuk_tanggal_datang").val();
		var tanggal_tempo = $("#barang_masuk_tanggal_tempo").val();
		var diskon = $("#barang_masuk_diskon").val();
		var t_diskon = $("#barang_masuk_t_diskon").val();
		var keterangan = $("#barang_masuk_keterangan").val();
		var nord = $("tbody#dbarang_masuk_list").children().length;
		$(".error_field").removeClass("error_field");
		if (nopo == "") {
			valid = false;
			invalid_msg += "</br><strong>No. SP</strong> tidak boleh kosong";
			$("#barang_masuk_nopo").addClass("error_field");
		}
		if (no_faktur == "") {
			valid = false;
			invalid_msg += "</br><strong>No. Faktur</strong> tidak boleh kosong";
			$("#barang_masuk_nofaktur").addClass("error_field");
		}
		if (tanggal == "") {
			valid = false;
			invalid_msg += "</br><strong>Tgl. Faktur</strong> tidak boleh kosong";
			$("#barang_masuk_tanggal").addClass("error_field");
		}
		if (tanggal_datang == "") {
			valid = false;
			invalid_msg += "</br><strong>Tgl. Datang</strong> tidak boleh kosong";
			$("#barang_masuk_tanggal_datang").addClass("error_field");
		}
		if (tanggal_datang == "") {
			valid = false;
			invalid_msg += "</br><strong>Jatuh Tempo</strong> tidak boleh kosong";
			$("#barang_masuk_tanggal_tempo").addClass("error_field");
		}
		if (diskon == "") {
			valid = false;
			invalid_msg += "</br><strong>Diskon</strong> tidak boleh kosong";
			$("#barang_masuk_diskon").addClass("error_field");
		} else if (!is_numeric(diskon)) {
			valid = false;
			invalid_msg += "</br><strong>Diskon</strong> seharusnya numerik (0-9)";
			$("#barang_masuk_diskon").addClass("error_field");
		}
		if (t_diskon == "") {
			valid = false;
			invalid_msg += "</br><strong>Tipe Diskon</strong> tidak boleh kosong";
			$("#barang_masuk_t_diskon").addClass("error_field");
		}
		if (keterangan == "") {
			valid = false;
			invalid_msg += "</br><strong>Keterangan</strong> tidak boleh kosong";
			$("#barang_masuk_keterangan").addClass("error_field");
		}
		if (nord == 0) {
			valid = false;
			invalid_msg += "</br><strong>Detil Barang Masuk</strong> tidak boleh kosong";
		}
		if (!valid) {
			$("#modal_alert_barang_masuk_add_form").html(
				"<div class='alert alert-block alert-danger'>" +
					"<h4>Peringatan</h4>" +
					invalid_msg +
				"</div>"
			);
		}
		return valid;
	};
	BarangMasukAction.prototype.show_add_form = function() {
		row_id = 0;
		var today = new Date();
		$("#barang_masuk_id").val("");
		$("#barang_masuk_nopo").val("");
		$("#barang_masuk_nopo_btn").removeAttr("onclick");
		$("#barang_masuk_nopo_btn").attr("onclick", "po.chooser('po', 'po_button', 'po', po)");
		$("#barang_masuk_nopo_btn").removeClass("btn-info");
		$("#barang_masuk_nopo_btn").removeClass("btn-inverse");
		$("#barang_masuk_nopo_btn").addClass("btn-info");
		$("#barang_masuk_id_vendor").val("");
		$("#barang_masuk_nama_vendor").val("");
		$("#barang_masuk_nofaktur").val("");
		$("#barang_masuk_nofaktur").removeAttr("disabled");
		$("#barang_masuk_tanggal").val("");
		$("#barang_masuk_tanggal").removeAttr("disabled");
		$("#barang_masuk_tanggal_datang").val("");
		$("#barang_masuk_tanggal_datang").removeAttr("disabled");
		$("#barang_masuk_tanggal_tempo").val("");
		$("#barang_masuk_tanggal_tempo").removeAttr("disabled");
		$("#barang_masuk_diskon").val(0);
		$("#barang_masuk_diskon").removeAttr("disabled");
		$("#barang_masuk_t_diskon").val("persen");
		$("#barang_masuk_t_diskon").removeAttr("disabled");
		$("#barang_masuk_keterangan").val("");
		$("#barang_masuk_keterangan").removeAttr("disabled");
		$("#dbarang_masuk_add").show();
		$("#dbarang_masuk_list").children().remove();
		$("#barang_masuk_save").removeAttr("onclick");
		$("#barang_masuk_save").attr("onclick", "barang_masuk.save()");
		$("#barang_masuk_save").show();
		$("#barang_masuk_ok").hide();
		$("#modal_alert_barang_masuk_add_form").html("");
		$(".error_field").removeClass("error_field");
		$("#barang_masuk_add_form").smodal("show");
	};
	BarangMasukAction.prototype.save = function() {
		if (!this.validate()) {
			return;
		}
		$("#barang_masuk_add_form").smodal("hide");
		showLoading();
		var self = this;
		var data = this.getRegulerData();
		data['super_command'] = "barang_masuk";
		data['command'] = "save";
		data['id'] = $("#barang_masuk_id").val();
		data['id_po'] = $("#barang_masuk_nopo").val();
		data['id_vendor'] = $("#barang_masuk_id_vendor").val();
		data['nama_vendor'] = $("#barang_masuk_nama_vendor").val();
		data['no_faktur'] = $("#barang_masuk_nofaktur").val();
		data['tanggal'] = $("#barang_masuk_tanggal").val();
		data['tanggal_tempo'] = $("#barang_masuk_tanggal_tempo").val();
		data['tanggal_datang'] = $("#barang_masuk_tanggal_datang").val();
		data['diskon'] = $("#barang_masuk_diskon").val();
		data['t_diskon'] = $("#barang_masuk_t_diskon").val();
		data['keterangan'] = $("#barang_masuk_keterangan").val();
		var detail = {};
		var nor = $("tbody#dbarang_masuk_list").children("tr").length;
		for(var i = 0; i < nor; i++) {
			var d_data = {};
			var prefix = $("tbody#dbarang_masuk_list").children("tr").eq(i).prop("id");
			d_data['id_barang'] = $("#" + prefix + "_id_barang").text();
			d_data['nama_barang'] = $("#" + prefix + "_nama_barang").text();
			d_data['medis'] = $("#" + prefix + "_medis").text();
			d_data['inventaris'] = $("#" + prefix + "_inventaris").text();
			d_data['nama_jenis_barang'] = $("#" + prefix + "_nama_jenis_barang").text();
			d_data['id_dpo'] = $("#" + prefix + "_id_dpo").text();
			d_data['jumlah'] = $("#" + prefix + "_jumlah").text();
			d_data['sisa'] = $("#" + prefix + "_sisa").text();
			d_data['satuan'] = $("#" + prefix + "_satuan").text();
			d_data['konversi'] = $("#" + prefix + "_konversi").text();
			d_data['satuan_konversi'] = $("#" + prefix + "_satuan_konversi").text();
			var hna = $("#" + prefix + "_hna").text();
			hna = hna.replace(/[^0-9-,]/g, '').replace(",", ".");
			d_data['hna'] = parseFloat(hna);
			var selisih = $("#" + prefix + "_selisih").text();
			selisih = selisih.replace(/[^0-9-,]/g, '').replace(",", ".");
			d_data['selisih'] = parseFloat(selisih);
			d_data['produsen'] = $("#" + prefix + "_produsen").text();
			d_data['diskon'] = $("#" + prefix + "_diskon").text();
			d_data['t_diskon'] = $("#" + prefix + "_t_diskon").text();
			detail[i] = d_data;
		}
		data['detail'] = detail;
		data['push_command'] = "decrease";
		$.post(
			"",
			data,
			function(response) {
				var json = getContent(response);
				if (json == null) {
					$("#barang_masuk_add_form").smodal("show");
				} else {
					self.view();
				}
				dismissLoading();
			}
		);
	};
	BarangMasukAction.prototype.del = function(id) {
		var self = this;
		var del_data = this.getRegulerData();
		del_data['super_command'] = "barang_masuk";
		del_data['command'] = "del";
		del_data['id'] = id;
		del_data['push_command'] = "increase";
		bootbox.confirm(
			"Anda yakin ingin menghapus Barang Masuk ini?", 
			function(result) {
			   if (result) {
				   showLoading();
					$.post(
						"",
						del_data,
						function(res){
							var json = getContent(res);
							if (json == null) return;
							self.view();
							dismissLoading();
						}
					);
				}
			}
		);
	};
	BarangMasukAction.prototype.detail = function(id) {
		var self = this;
		var data = this.getRegulerData();
		data['super_command'] = "barang_masuk";
		data['command'] = "edit";
		data['id'] = id;
		$.post(
			"",
			data,
			function(response) {
				var json = getContent(response);
				if (json == null) return;
				$("#barang_masuk_id").val(json.header.id);
				$("#barang_masuk_nopo").val(json.header.id_po);
				$("#barang_masuk_nopo_btn").removeAttr("onclick");
				$("#barang_masuk_nopo_btn").removeClass("btn-info");
				$("#barang_masuk_nopo_btn").removeClass("btn-inverse");
				$("#barang_masuk_nopo_btn").addClass("btn-inverse");
				$("#barang_masuk_id_vendor").val(json.header.id_vendor);
				$("#barang_masuk_nama_vendor").val(json.header.nama_vendor);
				$("#barang_masuk_nofaktur").val(json.header.no_faktur);
				$("#barang_masuk_nofaktur").removeAttr("disabled");
				$("#barang_masuk_nofaktur").attr("disabled", "disabled");
				$("#barang_masuk_tanggal").val(json.header.tanggal);
				$("#barang_masuk_tanggal").removeAttr("disabled");
				$("#barang_masuk_tanggal").attr("disabled", "disabled");
				$("#barang_masuk_tanggal_datang").val(json.header.tanggal_datang);
				$("#barang_masuk_tanggal_datang").removeAttr("disabled");
				$("#barang_masuk_tanggal_datang").attr("disabled", "disabled");
				$("#barang_masuk_tanggal_tempo").val(json.header.tanggal_tempo);
				$("#barang_masuk_tanggal_tempo").removeAttr("disabled");
				$("#barang_masuk_tanggal_tempo").attr("disabled", "disabled");
				$("#barang_masuk_diskon").val(json.header.diskon);
				$("#barang_masuk_diskon").removeAttr("disabled");
				$("#barang_masuk_diskon").attr("disabled", "disabled");
				$("#barang_masuk_t_diskon").val(json.header.t_diskon);
				$("#barang_masuk_t_diskon").removeAttr("disabled");
				$("#barang_masuk_t_diskon").attr("disabled", "disabled");
				$("#barang_masuk_keterangan").val(json.header.keterangan);
				$("#barang_masuk_keterangan").removeAttr("disabled");
				$("#barang_masuk_keterangan").attr("disabled", "disabled");
				$("#dbarang_masuk_add").hide();
				row_id = 0;
				$("#dbarang_masuk_list").children().remove();
				for(var i = 0; i < json.detail.length; i++) {
					var dbarang_masuk_id = json.detail[i].id;
					var dbarang_masuk_id_barang_masuk = json.detail[i].id_barang_masuk;
					var dbarang_masuk_id_dpo = json.detail[i].id_dpo;
					var dbarang_masuk_id_barang = json.detail[i].id_barang;
					var dbarang_masuk_nama_barang = json.detail[i].nama_barang;
					var dbarang_masuk_medis = json.detail[i].medis;
					var dbarang_masuk_inventaris = json.detail[i].inventaris;
					var dbarang_masuk_nama_jenis_barang = json.detail[i].nama_jenis_barang;
					var dbarang_masuk_jumlah = json.detail[i].jumlah;
					var dbarang_masuk_sisa = json.detail[i].sisa;
					var dbarang_masuk_satuan = json.detail[i].satuan;
					var dbarang_masuk_konversi = json.detail[i].konversi;
					var dbarang_masuk_satuan_konversi = json.detail[i].satuan_konversi;
					var dbarang_masuk_hna = "Rp. " + (parseFloat(json.detail[i].hna)).formatMoney("2", ".", ",");
					var dbarang_masuk_selisih = "Rp. " + (parseFloat(json.detail[i].selisih)).formatMoney("2", ".", ",");
					var dbarang_masuk_produsen = json.detail[i].produsen;
					var dbarang_masuk_subtotal = "Rp. " + ((parseFloat(json.detail[i].hna) + parseFloat(json.detail[i].selisih)) * parseFloat(dbarang_masuk_jumlah)).formatMoney("2", ".", ",");
					var dbarang_masuk_diskon = json.detail[i].diskon;
					var dbarang_masuk_t_diskon = json.detail[i].t_diskon;
					var f_diskon = dbarang_masuk_diskon + " %";
					if (dbarang_masuk_t_diskon == "nominal")
						f_diskon = "Rp. " + (parseFloat(dbarang_masuk_diskon)).formatMoney("2", ".", ",");
					$("tbody#dbarang_masuk_list").append(
						"<tr id='data_" + row_id + "'>" +
							"<td id='data_" + row_id + "_id' style='display: none;'>" + dbarang_masuk_id + "</td>" +
							"<td id='data_" + row_id + "_id_barang' style='display: none;'>" + dbarang_masuk_id_barang + "</td>" +
							"<td id='data_" + row_id + "_medis' style='display: none;'>" + dbarang_masuk_medis + "</td>" +
							"<td id='data_" + row_id + "_inventaris' style='display: none;'>" + dbarang_masuk_inventaris + "</td>" +
							"<td id='data_" + row_id + "_id_dpo' style='display: none;'>" + dbarang_masuk_id_dpo + "</td>" +
							"<td id='data_" + row_id + "_hna' style='display: none;'>" + dbarang_masuk_hna + "</td>" +
							"<td id='data_" + row_id + "_selisih' style='display: none;'>" + dbarang_masuk_selisih + "</td>" +
							"<td id='data_" + row_id + "_diskon' style='display: none;'>" + dbarang_masuk_diskon + "</td>" +
							"<td id='data_" + row_id + "_t_diskon' style='display: none;'>" + dbarang_masuk_t_diskon + "</td>" +
							"<td id='data_" + row_id + "_jumlah' style='display: none;'>" + dbarang_masuk_jumlah + "</td>" +
							"<td id='data_" + row_id + "_sisa' style='display: none;'>" + dbarang_masuk_sisa + "</td>" +
							"<td id='data_" + row_id + "_satuan' style='display: none;'>" + dbarang_masuk_satuan + "</td>" +
							"<td id='data_" + row_id + "_konversi' style='display: none;'>" + dbarang_masuk_konversi + "</td>" +
							"<td id='data_" + row_id + "_satuan_konversi' style='display: none;'>" + dbarang_masuk_satuan_konversi + "</td>" +
							"<td id='data_" + row_id + "_nama_barang'>" + dbarang_masuk_nama_barang + "</td>" +
							"<td id='data_" + row_id + "_nama_jenis_barang'>" + dbarang_masuk_nama_jenis_barang + "</td>" +
							"<td id='data_" + row_id + "_produsen'>" + dbarang_masuk_produsen + "</td>" +
							"<td id='data_" + row_id + "_f_subtotal'>" + dbarang_masuk_jumlah + " x (" + dbarang_masuk_hna + " + " + dbarang_masuk_selisih + ") = " + dbarang_masuk_subtotal + "</td>" +
							"<td id='data_" + row_id + "_f_jumlah'>" + dbarang_masuk_jumlah + " " + dbarang_masuk_satuan + "</td>" +
							"<td id='data_" + row_id + "_f_konversi'>1 " + dbarang_masuk_satuan + " = " + dbarang_masuk_konversi + " " + dbarang_masuk_satuan_konversi + "</td>" +
							"<td id='data_" + row_id + "_f_diskon'>" + f_diskon + "</td>" +
							"<td></td>" +
						"</tr>"
					);
					row_id++;
				}
				$("#barang_masuk_save").removeAttr("onclick");
				$("#barang_masuk_save").hide();
				$("#barang_masuk_ok").show();
				$("#modal_alert_barang_masuk_add_form").html("");
				$(".error_field").removeClass("error_field");
				$("#barang_masuk_add_form").smodal("show");
			}
		);
	};
	
	function DBarangMasukAction(name, page, action, column) {
		this.initialize(name, page, action, column);
	}
	DBarangMasukAction.prototype.constructor = DBarangMasukAction;
	DBarangMasukAction.prototype = new TableAction();
	DBarangMasukAction.prototype.validate = function() {
		var valid = true;
		var invalid_msg = "";
		var nama_barang = $("#dbarang_masuk_nama_barang").val();
		var jumlah = $("#dbarang_masuk_jumlah").val();
		var satuan = $("#dbarang_masuk_satuan").val();
		var konversi = $("#dbarang_masuk_konversi").val();
		var satuan_konversi = $("#dbarang_masuk_satuan_konversi").val();
		var produsen = $("#dbarang_masuk_produsen").val();
		var diskon = $("#dbarang_masuk_diskon").val();
		var t_diskon = $("#dbarang_masuk_t_diskon").val();
		$(".error_field").removeClass("error_field");
		if (nama_barang == "") {
			valid = false;
			invalid_msg += "</br><strong>Nama Barang</strong> tidak boleh kosong";
			$("#dbarang_masuk_nama_barang").addClass("error_field");
		}
		if (produsen == "") {
			valid = false;
			invalid_msg += "</br><strong>Produsen</strong> tidak boleh kosong";
			$("#dbarang_masuk_produsen").addClass("error_field");
		}
		if (jumlah == "") {
			valid = false;
			invalid_msg += "</br><strong>Jumlah</strong> tidak boleh kosong";
			$("#dbarang_masuk_jumlah").addClass("error_field");
		} else if (!is_numeric(jumlah)) {
			valid = false;
			invalid_msg += "</br><strong>Jumlah</strong> seharusnya numerik (0-9)";
			$("#dbarang_masuk_jumlah").addClass("error_field");
		}
		if (satuan == "") {
			valid = false;
			invalid_msg += "</br><strong>Satuan</strong> tidak boleh kosong";
			$("#dbarang_masuk_satuan").addClass("error_field");
		}
		if (konversi == "") {
			valid = false;
			invalid_msg += "</br><strong>Jml. Terkecil</strong> tidak boleh kosong";
			$("#dbarang_masuk_konversi").addClass("error_field");
		} else if (!is_numeric(konversi)) {
			valid = false;
			invalid_msg += "</br><strong>Jml. Terkecil</strong> seharusnya numerik (0-9)";
			$("#dbarang_masuk_konversi").addClass("error_field");
		}
		if (satuan_konversi == "") {
			valid = false;
			invalid_msg += "</br><strong>Stn. Terkecil</strong> tidak boleh kosong";
			$("#dbarang_masuk_satuan_konversi").addClass("error_field");
		}
		if (diskon == "") {
			valid = false;
			invalid_msg += "</br><strong>Diskon</strong> tidak boleh kosong";
			$("#dbarang_masuk_diskon").addClass("error_field");
		} else if (!is_numeric(diskon)) {
			valid = false;
			invalid_msg += "</br><strong>Diskon</strong> seharusnya numerik (0-9)";
			$("#dbarang_masuk_diskon").addClass("error_field");
		}
		if (t_diskon == "") {
			valid = false;
			invalid_msg += "</br><strong>Tipe Diskon</strong> tidak boleh kosong";
			$("#dbarang_masuk_t_diskon").addClass("error_field");
		}
		if (!valid) {
			$("#modal_alert_dbarang_masuk_add_form").html(
				"<div class='alert alert-block alert-danger'>" +
					"<h4>Peringatan</h4>" +
					invalid_msg +
				"</div>"
			);
		}
		return valid;
	};
	DBarangMasukAction.prototype.show_add_form = function() {
		if ($("#barang_masuk_nopo").val() == "") {
			$(".error_field").removeClass("error_field");
			$("#modal_alert_barang_masuk_add_form").html(
				"<div class='alert alert-block alert-danger'>" +
					"<h4>Peringatan</h4>" +
					"</br><strong>No. SP</strong> tidak boleh kosong" +
				"</div>"
			);
			$("#barang_masuk_nopo").addClass("error_field");
			return;
		}
		$("#dbarang_masuk_id").val("");
		$("#dbarang_masuk_id_barang").val("");
		$("#dbarang_masuk_nama_barang").val("");
		$("#dbarang_masuk_medis").val("");
		$("#dbarang_masuk_inventaris").val("");
		$("#dbarang_masuk_nama_jenis_barang").val("");
		$("#dbarang_id_dpo").val("");
		$("#dbarang_masuk_jumlah").val("");
		$("#dbarang_masuk_satuan").val("");
		$("#dbarang_masuk_konversi").val("");
		$("#dbarang_masuk_satuan_konversi").val("");
		$("#dbarang_masuk_hna").val("");
		$("#dbarang_masuk_selisih").val("Rp. 0,00");
		$("#dbarang_masuk_produsen").val("");
		$("#dbarang_masuk_diskon").val(0);
		$("#dbarang_masuk_t_diskon").val("persen");
		$("#modal_alert_dbarang_masuk_add_form").html("");
		$(".error_field").removeClass("error_field");
		$("#dbarang_masuk_save").removeAttr("onclick");
		$("#dbarang_masuk_save").attr("onclick", "dbarang_masuk.save()");
		$("#dbarang_masuk_add_form").smodal("show");
	};
	DBarangMasukAction.prototype.save = function() {
		if (!this.validate()) {
			return;
		}
		var id = $("#dbarang_masuk_id").val();
		var id_barang = $("#dbarang_masuk_id_barang").val();
		var nama_barang = $("#dbarang_masuk_nama_barang").val();
		var medis = $("#dbarang_masuk_medis").val();
		var inventaris = $("#dbarang_masuk_inventaris").val();
		var nama_jenis_barang = $("#dbarang_masuk_nama_jenis_barang").val();
		var id_dpo = $("#dbarang_masuk_id_dpo").val();
		var hna = $("#dbarang_masuk_hna").val();
		var v_hna = parseFloat(hna.replace(/[^0-9-,]/g, '').replace(",", "."));
		var selisih = $("#dbarang_masuk_selisih").val();
		var v_selisih = parseFloat(selisih.replace(/[^0-9-,]/g, '').replace(",", "."));
		var produsen = $("#dbarang_masuk_produsen").val();
		var jumlah = $("#dbarang_masuk_jumlah").val();
		var subtotal = "Rp. " + ((v_hna + v_selisih) * parseFloat(jumlah)).formatMoney("2", ".", ",");
		var sisa = $("#dbarang_masuk_jumlah").val();
		var satuan = $("#dbarang_masuk_satuan").val();
		var konversi = $("#dbarang_masuk_konversi").val();
		var satuan_konversi = $("#dbarang_masuk_satuan_konversi").val();
		var diskon = $("#dbarang_masuk_diskon").val();
		var t_diskon = $("#dbarang_masuk_t_diskon").val();
		var f_diskon = diskon + " %";
		if (t_diskon == "nominal")
			f_diskon = "Rp. " + (parseFloat(diskon)).formatMoney("2", ".", ",");
		$("tbody#dbarang_masuk_list").append(
			"<tr id='data_" + row_id + "'>" +
				"<td id='data_" + row_id + "_id' style='display: none;'>" + id + "</td>" +
				"<td id='data_" + row_id + "_id_barang' style='display: none;'>" + id_barang + "</td>" +
				"<td id='data_" + row_id + "_medis' style='display: none;'>" + medis + "</td>" +
				"<td id='data_" + row_id + "_inventaris' style='display: none;'>" + inventaris + "</td>" +
				"<td id='data_" + row_id + "_id_dpo' style='display: none;'>" + id_dpo + "</td>" +
				"<td id='data_" + row_id + "_hna' style='display: none;'>" + hna + "</td>" +
				"<td id='data_" + row_id + "_selisih' style='display: none;'>" + selisih + "</td>" +
				"<td id='data_" + row_id + "_diskon' style='display: none;'>" + diskon + "</td>" +
				"<td id='data_" + row_id + "_t_diskon' style='display: none;'>" + t_diskon + "</td>" +
				"<td id='data_" + row_id + "_jumlah' style='display: none;'>" + jumlah + "</td>" +
				"<td id='data_" + row_id + "_sisa' style='display: none;'>" + sisa + "</td>" +
				"<td id='data_" + row_id + "_satuan' style='display: none;'>" + satuan + "</td>" +
				"<td id='data_" + row_id + "_konversi' style='display: none;'>" + konversi + "</td>" +
				"<td id='data_" + row_id + "_satuan_konversi' style='display: none;'>" + satuan_konversi + "</td>" +
				"<td id='data_" + row_id + "_nama_barang'>" + nama_barang + "</td>" +
				"<td id='data_" + row_id + "_nama_jenis_barang'>" + nama_jenis_barang + "</td>" +
				"<td id='data_" + row_id + "_produsen'>" + produsen + "</td>" +
				"<td id='data_" + row_id + "_f_subtotal'>" + jumlah + " x (" + hna + " + " + selisih + ") = " + subtotal + "</td>" +
				"<td id='data_" + row_id + "_f_jumlah'>" + jumlah + " " + satuan + "</td>" +
				"<td id='data_" + row_id + "_f_konversi'>1 " + satuan + " = " + konversi + " " + satuan_konversi + "</td>" +
				"<td id='data_" + row_id + "_f_diskon'>" + f_diskon + "</td>" +
				"<td>" +
					"<div class='btn-group noprint'>" +
						"<a href='#' onclick='dbarang_masuk.edit(" + row_id + ")' data-content='Ubah' data-toggle='popover' class='input btn btn-warning'>" + 
							"<i class='icon-edit icon-white'></i>" +
						"</a>" +
						"<a href='#' onclick='dbarang_masuk.delete(" + row_id + ")' data-content='Hapus' data-toggle='popover' class='input btn btn-danger'>" + 
							"<i class='icon-remove icon-white'></i>" + 
						"</a>" +
					"</div>" +
				"</td>" +
			"</tr>"
		);
		row_id++;
		$("#dbarang_masuk_add_form").smodal("hide");
	};
	DBarangMasukAction.prototype.edit = function(r_num) {
		var id = $("#data_" + r_num + "_id").text();
		var id_barang = $("#data_" + r_num + "_id_barang").text();
		var nama_barang = $("#data_" + r_num + "_nama_barang").text();
		var medis = $("#data_" + r_num + "_medis").text();
		var inventaris = $("#data_" + r_num + "_inventaris").text();
		var nama_jenis_barang = $("#data_" + r_num + "_nama_jenis_barang").text();
		var id_dpo = $("#data_" + r_num + "_id_dpo").text();
		var jumlah = $("#data_" + r_num + "_jumlah").text();
		var satuan = $("#data_" + r_num + "_satuan").text();
		var konversi = $("#data_" + r_num + "_konversi").text();
		var satuan_konversi = $("#data_" + r_num + "_satuan_konversi").text();
		var hna = $("#data_" + r_num + "_hna").text();
		var selisih = $("#data_" + r_num + "_selisih").text();
		var produsen = $("#data_" + r_num + "_produsen").text();
		var diskon = $("#data_" + r_num + "_diskon").text();
		var t_diskon = $("#data_" + r_num + "_t_diskon").text();
		$("#dbarang_masuk_id").val(id);
		$("#dbarang_masuk_id_barang").val(id_barang);
		$("#dbarang_masuk_nama_barang").val(nama_barang);
		$("#dbarang_masuk_medis").val(medis);
		$("#dbarang_masuk_inventaris").val(inventaris);
		$("#dbarang_masuk_nama_jenis_barang").val(nama_jenis_barang);
		$("#dbarang_masuk_id_dpo").val(id_dpo);
		$("#dbarang_masuk_jumlah").val(jumlah);
		$("#dbarang_masuk_satuan").val(satuan);
		$("#dbarang_masuk_konversi").val(konversi);
		$("#dbarang_masuk_satuan_konversi").val(satuan_konversi);
		$("#dbarang_masuk_hna").val(hna);
		$("#dbarang_masuk_selisih").val(selisih);
		$("#dbarang_masuk_produsen").val(produsen);
		$("#dbarang_masuk_diskon").val(diskon);
		$("#dbarang_masuk_t_diskon").val(t_diskon);
		$("#modal_alert_dbarang_masuk_add_form").html("");
		$(".error_field").removeClass("error_field");
		$("#dbarang_masuk_save").removeAttr("onclick");
		$("#dbarang_masuk_save").attr("onclick", "dbarang_masuk.update(" + r_num + ")");
		$("#dbarang_masuk_add_form").smodal("show");
	};
	DBarangMasukAction.prototype.update = function(r_num) {
		if (!this.validate()) {
			return;
		}
		var id = $("#dbarang_masuk_id").val();
		var id_barang = $("#dbarang_masuk_id_barang").val();
		var nama_barang = $("#dbarang_masuk_nama_barang").val();
		var medis = $("#dbarang_masuk_medis").val();
		var inventaris = $("#dbarang_masuk_inventaris").val();
		var nama_jenis_barang = $("#dbarang_masuk_nama_jenis_barang").val();
		var id_dpo = $("#dbarang_masuk_id_dpo").val();
		var hna = $("#dbarang_masuk_hna").val();
		var v_hna = parseFloat(hna.replace(/[^0-9-,]/g, '').replace(",", "."));
		var selisih = $("#dbarang_masuk_selisih").val();
		var v_selisih = parseFloat(selisih.replace(/[^0-9-,]/g, '').replace(",", "."));
		var jumlah = $("#dbarang_masuk_jumlah").val();
		var subtotal = "Rp. " + (v_hna * parseFloat(jumlah)).formatMoney("2", ".", ",");
		var sisa = $("#dbarang_masuk_jumlah").val();
		var satuan = $("#dbarang_masuk_satuan").val();
		var konversi = $("#dbarang_masuk_konversi").val();
		var satuan_konversi = $("#dbarang_masuk_satuan_konversi").val();
		var produsen = $("#dbarang_masuk_produsen").val();
		var diskon = $("#dbarang_masuk_diskon").val();
		var t_diskon = $("#dbarang_masuk_t_diskon").val();
		var f_diskon = diskon + " %";
		if (t_diskon == "nominal")
			f_diskon = "Rp. " + (parseFloat(diskon)).formatMoney("2", ".", ",");
		$("#data_" + r_num + "_id").text(id);
		$("#data_" + r_num + "_id_barang").text(id_barang);
		$("#data_" + r_num + "_nama_barang").text(nama_barang);
		$("#data_" + r_num + "_medis").text(medis);
		$("#data_" + r_num + "_inventaris").text(inventaris);
		$("#data_" + r_num + "_nama_jenis_barang").text(nama_jenis_barang);
		$("#data_" + r_num + "_id_dpo").text(id_dpo);
		$("#data_" + r_num + "_hna").text(hna);
		$("#data_" + r_num + "_selisih").text(selisih);
		$("#data_" + r_num + "_diskon").text(diskon);
		$("#data_" + r_num + "_t_diskon").text(t_diskon);
		$("#data_" + r_num + "_jumlah").text(jumlah);
		$("#data_" + r_num + "_sisa").text(sisa);
		$("#data_" + r_num + "_satuan").text(satuan);
		$("#data_" + r_num + "_konversi").text(konversi);
		$("#data_" + r_num + "_satuan_konversi").text(satuan_konversi);
		$("#data_" + r_num + "_produsen").text(produsen);
		$("#data_" + r_num + "_f_subtotal").text(jumlah + " x " + hna + " = " + subtotal);
		$("#data_" + r_num + "_f_jumlah").text(jumlah + " " + satuan);
		$("#data_" + r_num + "_f_konversi").text("1 " + satuan + " = " + konversi + " " + satuan_konversi);
		$("#data_" + r_num + "_f_diskon").text(f_diskon);
		$("#dbarang_masuk_add_form").smodal("hide");
	};
	DBarangMasukAction.prototype.delete = function(r_num) {
		var id = $("#data_" + r_num + "_id").text();
		if (id.length == 0) {
			$("#data_" + r_num).remove();
		} else {
			$("#data_" + r_num).attr("style", "display: none;");
			$("#data_" + r_num).attr("class", "deleted");
		}
	};
	
	function POAction(name, page, action, column) {
		this.initialize(name, page, action, column);
	}
	POAction.prototype.constructor = POAction;
	POAction.prototype = new TableAction();
	POAction.prototype.selected = function(json) {
		$("#barang_masuk_nopo").val(json.id);
		$("#barang_masuk_id_vendor").val(json.id_vendor);
		$("#barang_masuk_nama_vendor").val(json.nama_vendor);
		$("tbody#dbarang_masuk_list").children().remove();
		$("#barang_masuk_nopo").removeClass("error_field");
		$("#modal_alert_barang_masuk_add_form").html("");
	};
	
	function BarangAction(name, page, action, column) {
		this.initialize(name, page, action, column);
	}
	BarangAction.prototype.constructor = BarangAction;
	BarangAction.prototype = new TableAction();
	BarangAction.prototype.getViewData = function(){
		var view_data = this.getRegulerData();
		view_data['super_command'] = "barang";
		view_data['command'] = "list";
		view_data['id_po'] = $("#barang_masuk_nopo").val();
		view_data['kriteria'] = $("#" + this.prefix + "_kriteria").val();
		view_data['number'] = $("#" + this.prefix + "_number").val();
		view_data['max'] = $("#" + this.prefix + "_max").val();
		return view_data;
	};
	BarangAction.prototype.selected = function(json) {
		$("#dbarang_masuk_id_barang").val(json.id_barang);
		$("#dbarang_masuk_nama_barang").val(json.nama_barang);
		$("#dbarang_masuk_medis").val(json.medis);
		$("#dbarang_masuk_inventaris").val(json.inventaris);
		$("#dbarang_masuk_nama_jenis_barang").val(json.nama_jenis_barang);
		$("#dbarang_masuk_id_dpo").val(json.id);
		var hna = "Rp. " + (parseFloat(json.hpp)).formatMoney("2", ".", ",");
		$("#dbarang_masuk_hna").val(hna);
		$("#dbarang_masuk_jumlah").val(json.sisa);
		$("#dbarang_masuk_satuan").val(json.satuan);
		$("#dbarang_masuk_konversi").val(1);
		$("#dbarang_masuk_satuan_konversi").val(json.satuan);
	};
	
	var barang_masuk;
	var dbarang_masuk;
	var po;
	var barang;
	var row_id;
	$(document).ready(function() {
		$('[data-toggle="popover"]').popover({
			trigger: 'hover',
			'placement': 'top'
		});
		$(".mydate").datepicker();
		$("#smis-chooser-modal").on("show", function() {
			if ($("#smis-chooser-modal .modal-header h3").text() == "PO") {
				$("#smis-chooser-modal").removeClass("half_model");
				$("#smis-chooser-modal").addClass("full_model");
			} else if ($("#smis-chooser-modal .modal-header h3").text() == "BARANG") {
				$("#smis-chooser-modal").removeClass("full_model");
				$("#smis-chooser-modal").addClass("half_model");
			}
		});
		$("#smis-chooser-modal").on("hidden", function() {
			$("#smis-chooser-modal").removeClass("full_model");
			$("#smis-chooser-modal").removeClass("half_model");
		});
		$("#dbarang_masuk_selisih").on("change", function() {
			if ($("#dbarang_masuk_selisih").val() == "") {
				$("#dbarang_masuk_selisih").val("Rp. 0,00");
			}
		});
		po = new POAction(
			"po",
			"gudang_umum",
			"barang_masuk",
			new Array()
		);		
		po.setSuperCommand("po");
		barang = new BarangAction(
			"barang",
			"gudang_umum",
			"barang_masuk",
			new Array()
		);		
		barang.setSuperCommand("barang");
		var dbarang_masuk_columns = new Array("id", "id_barang", "nama_barang", "medis", "inventaris", "nama_jenis_barang", "hna", "produsen", "jumlah", "sisa", "satuan", "konversi", "satuan_konversi", "diskon", "t_diskon", "selisih");
		dbarang_masuk = new DBarangMasukAction(
			"dbarang_masuk",
			"gudang_umum",
			"barang_masuk",
			dbarang_masuk_columns
		);
		var barang_masuk_columns = new Array("id", "id_po", "tanggal", "no_faktur", "tanggal", "tanggal_tempo", "tanggal_datang", "diskon", "t_diskon","keterangan");
		barang_masuk = new BarangMasukAction(
			"barang_masuk",
			"gudang_umum",
			"barang_masuk",
			barang_masuk_columns
		);
		barang_masuk.setSuperCommand("barang_masuk");
		barang_masuk.view();
	});
</script>