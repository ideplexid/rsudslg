<?php 
	require_once("smis-base/smis-include-duplicate.php");

	class StokBarangBTable extends Table {
		public function getContentButton($id) {
			$btn_group = new ButtonGroup("noprint");
			$btn = new Button("", "", "Ubah");
			$btn->setAction($this->action . ".edit('" . $id . "')");
			$btn->setClass("btn-warning");
			$btn->setAtribute("data-content='Ubah' data-toggle='popover'");
			$btn->setIcon("icon-edit icon-white");
			$btn->setIsButton(Button::$ICONIC);
			$btn_group->addElement($btn);
			return $btn_group;
		}
	}
	$stok_barang_table = new StokBarangBTable(
		array("Nomor", "Nama Barang", "Jenis Barang", "Produsen", "Vendor", "Sisa", "Harga Satuan", "Ket. Jumlah", "Tgl. Exp."),
		"",
		null,
		true
	);
	$stok_barang_table->setName("stok_barang_b");
	$stok_barang_table->setReloadButtonEnable(false);
	$stok_barang_table->setPrintButtonEnable(false);
	$stok_barang_table->setAddButtonEnable(false);
	
	if (isset($_POST['command'])) {
		class StokBarangAdapter extends ArrayAdapter {
			public function adapt($row) {
				$array = array();
				$array['id'] = $row->id;
				$array['Nomor'] = self::format("digit8", $row->id);
				$array['Nama Barang'] = $row->nama_barang;
				$array['Jenis Barang'] = $row->nama_jenis_barang;
				$array['Produsen'] = $row->produsen;
				$array['Vendor'] = $row->nama_vendor;
				$array['Jml. Awal'] = $row->jumlah . " " . $row->satuan;
				$array['Sisa'] = $row->sisa . " " . $row->satuan;
				$array['Harga Satuan'] = self::format("money Rp.", $row->hna);
				$array['Ket. Jumlah'] = "1 " . $row->satuan . " = " . $row->konversi . " " . $row->satuan_konversi;
				if ($row->tanggal_exp == "0000-00-00")
					$array['Tgl. Exp.'] = "-";
				else
					$array['Tgl. Exp.'] = self::format("date d M Y", $row->tanggal_exp);
				return $array;
			}
		}
		$stok_barang_adapter = new StokBarangAdapter();
		$columns = array("id", "id_dbarang_masuk", "nama_barang", "nama_jenis_barang", "jumlah", "sisa", "satuan", "konversi", "satuan_konversi", "hna", "tanggal_exp", "id_vendor", "nama_vendor", "produsen");
		$stok_barang_dbtable = new DBTable(
			$db,
			"smis_gd_stok_barang",
			$columns
		);
		$filter = "";
		if (isset($_POST['kriteria'])) {
			$filter = " AND (smis_gd_stok_barang.nama_barang LIKE '%" . $_POST['kriteria'] . "%')";
		}
		$query_value = "
			SELECT *
			FROM smis_gd_stok_barang
			WHERE sisa > 0 AND konversi != 1 AND prop NOT LIKE 'del' " . $filter . "
		";
		$query_count = "
			SELECT COUNT(*)
			FROM smis_gd_stok_barang
			WHERE sisa > 0 AND konversi != 1 AND prop NOT LIKE 'del' " . $filter . "
		";
		$stok_barang_dbtable->setPreferredQuery(true, $query_value, $query_count);
		class StokBarangDBResponder extends DuplicateResponder {
			public function save() {
				//update stok lama:
				$data_lama['sisa'] = $_POST['sisa'];
				$data_lama['autonomous'] = "[".$this->getAutonomous()."]";
		        $data_lama['duplicate'] = 0;
		        $data_lama['time_updated'] = date("Y-m-d H:i:s");
		        $data_lama['origin_updated'] = $this->getAutonomous();
				$data_lama_id['id'] = $_POST['id'];
				$result = $this->dbtable->update($data_lama, $data_lama_id);
				//logging riwayat stok barang:
				$riwayat_dbtable = new DBTable($this->dbtable->get_db(), "smis_gd_riwayat_stok_barang");
				$data_riwayat = array();
				$data_riwayat['tanggal'] = date("Y-m-d");
				$data_riwayat['id_stok_barang'] = $_POST['id'];
				$data_riwayat['jumlah_keluar'] = $_POST['jumlah'];
				$data_riwayat['sisa'] = $_POST['sisa'];
				$data_riwayat['keterangan'] = "Stok Dibongkar";
				global $user;
				$data_riwayat['nama_user'] = $user->getName();
				$data_riwayat['autonomous'] = "[".$this->getAutonomous()."]";
		        $data_riwayat['duplicate'] = 0;
		        $data_riwayat['time_updated'] = date("Y-m-d H:i:s");
		        $data_riwayat['origin_updated'] = $this->getAutonomous();
		        $data_riwayat['origin'] = $this->getAutonomous();
				$riwayat_dbtable->insert($data_riwayat);
				//insert stok baru:
				$data_baru['id_dbarang_masuk'] = $_POST['id_dbarang_masuk'];
				$data_baru['nama_barang'] = $_POST['nama_barang'];
				$data_baru['nama_jenis_barang'] = $_POST['nama_jenis_barang'];
				$data_baru['jumlah'] = $_POST['jumlah'] * $_POST['konversi'];
				$data_baru['sisa'] = $_POST['jumlah'] * $_POST['konversi'];
				$data_baru['satuan'] = $_POST['satuan'];
				$data_baru['konversi'] = "1";
				$data_baru['satuan_konversi'] = $_POST['satuan'];
				$data_baru['hna'] = $_POST['hna'];
				$data_baru['produsen'] = $_POST['produsen'];
				$data_baru['tanggal_exp'] = $_POST['tanggal_exp'];
				$data_baru['id_vendor'] = $_POST['id_vendor'];
				$data_baru['nama_vendor'] = $_POST['nama_vendor'];
				$data_baru['produsen'] = $_POST['produsen'];
				$data_baru['turunan'] = "1";
				$data_baru['autonomous'] = "[".$this->getAutonomous()."]";
		        $data_baru['duplicate'] = 0;
		        $data_baru['time_updated'] = date("Y-m-d H:i:s");
		        $data_baru['origin_updated'] = $this->getAutonomous();
		        $data_baru['origin'] = $this->getAutonomous();
				$this->dbtable->insert($data_baru);
				$id_stok_barang = $this->dbtable->get_inserted_id();
				//logging riwayat stok barang:
				$data_riwayat = array();
				$data_riwayat['tanggal'] = date("Y-m-d");
				$data_riwayat['id_stok_barang'] = $id_stok_barang;
				$data_riwayat['jumlah_masuk'] = $_POST['jumlah'] * $_POST['konversi'];
				$data_riwayat['sisa'] = $_POST['jumlah'] * $_POST['konversi'];
				$data_riwayat['keterangan'] = "Hasil Bongkar Stok";
				$data_riwayat['nama_user'] = $user->getName();
				$data_riwayat['autonomous'] = "[".$this->getAutonomous()."]";
		        $data_riwayat['duplicate'] = 0;
		        $data_riwayat['time_updated'] = date("Y-m-d H:i:s");
		        $data_riwayat['origin_updated'] = $this->getAutonomous();
		        $data_riwayat['origin'] = $this->getAutonomous();
				$riwayat_dbtable->insert($data_riwayat);
				$success['type'] = "update";
				$success['id'] = $data_lama_id['id'];
				$success['success'] = 1;
				if ($result === false) $success['success'] = 0;
				return $success;
			}
		}
		$stok_barang_dbresponder = new StokBarangDBResponder(
			$stok_barang_dbtable,
			$stok_barang_table,
			$stok_barang_adapter
		);
		$data = $stok_barang_dbresponder->command($_POST['command']);
		echo json_encode($data);
		return;
	}
	
	$stok_barang_modal = new Modal("stok_barang_b_add_form", "smis_form_container", "stok_barang_b");
	$stok_barang_modal->setTitle("Data Stok Barang");
	$id_hidden = new Hidden("stok_barang_b_id", "stok_barang_b_id", "");
	$stok_barang_modal->addElement("", $id_hidden);
	$id_dbarang_masuk_hidden = new Hidden("stok_barang_b_id_dbarang_masuk", "stok_barang_b_id_dbarang_masuk", "");
	$stok_barang_modal->addElement("", $id_dbarang_masuk_hidden);
	$nama_barang_text = new Text("stok_barang_b_nama_barang", "stok_barang_b_nama_barang", "");
	$nama_barang_text->setAtribute("disabled='disabled'");
	$stok_barang_modal->addElement("Nama Barang", $nama_barang_text);
	$nama_jenis_barang_text = new Text("stok_barang_b_nama_jenis_barang", "stok_barang_b_nama_jenis_barang", "");
	$nama_jenis_barang_text->setAtribute("disabled='disabled'");
	$stok_barang_modal->addElement("Jenis Barang", $nama_jenis_barang_text);
	$sisa_hidden = new Hidden("stok_barang_b_sisa", "stok_barang_b_sisa", "");
	$stok_barang_modal->addElement("", $sisa_hidden);
	$satuan_hidden = new Hidden("stok_barang_b_satuan", "stok_barang_b_satuan", "");
	$stok_barang_modal->addElement("", $satuan_hidden);
	$konversi_hidden = new Hidden("stok_barang_b_konversi", "stok_barang_b_konversi", "");
	$stok_barang_modal->addElement("", $konversi_hidden);
	$satuan_konversi_hidden = new Hidden("stok_barang_b_satuan_konversi", "stok_barang_b_satuan_konversi", "");
	$stok_barang_modal->addElement("", $satuan_konversi_hidden);
	$hna_hidden = new Hidden("stok_barang_b_hna", "stok_barang_b_hna", "");
	$stok_barang_modal->addElement("", $hna_hidden);
	$tanggal_exp_hidden = new Hidden("stok_barang_b_tanggal_exp", "stok_barang_b_tanggal_exp", "");
	$stok_barang_modal->addElement("", $tanggal_exp_hidden);
	$produsen_text = new Text("stok_barang_b_produsen", "stok_barang_b_produsen", "");
	$produsen_text->setAtribute("disabled='disabled'");
	$stok_barang_modal->addElement("Produsen", $produsen_text);
	$id_vendor_hidden = new Hidden("stok_barang_b_id_vendor", "stok_barang_b_id_vendor", "");
	$stok_barang_modal->addElement("", $id_vendor_hidden);
	$nama_vendor_text = new Text("stok_barang_b_nama_vendor", "stok_barang_b_nama_vendor", "");
	$nama_vendor_text->setAtribute("disabled='disabled'");
	$stok_barang_modal->addElement("Vendor", $nama_vendor_text);
	$f_sisa_text = new Text("stok_barang_b_f_sisa", "stok_barang_b_f_sisa", "");
	$f_sisa_text->setAtribute("disabled='disabled'");
	$stok_barang_modal->addElement("Sisa", $f_sisa_text);
	$keterangan_text = new Text("stok_barang_b_keterangan", "stok_barang_b_keterangan", "");
	$keterangan_text->setAtribute("disabled='disabled'");
	$stok_barang_modal->addElement("Keterangan", $keterangan_text);
	$jumlah_text = new Text("stok_barang_b_jumlah", "stok_barang_b_jumlah", "");
	$stok_barang_modal->addElement("Jumlah", $jumlah_text);
	$stok_barang_button = new Button("", "", "Simpan");
	$stok_barang_button->setClass("btn-success");
	$stok_barang_button->setAction("stok_barang_b.save()");
	$stok_barang_button->setIcon("fa fa-floppy-o");
	$stok_barang_button->setIsButton(Button::$ICONIC);
	$stok_barang_modal->addFooter($stok_barang_button);
	
	echo $stok_barang_modal->getHtml();
	echo $stok_barang_table->getHtml();
	echo addJS("framework/smis/js/table_action.js");
	echo addJS("framework/bootstrap/js/bootstrap-datepicker.js");
	echo addCSS("framework/bootstrap/css/datepicker.css");	
?>
<script type="text/javascript">
	function StokBarangBAction(name, page, action, column) {
		this.initialize(name, page, action, column);
	}
	StokBarangBAction.prototype.constructor = StokBarangBAction;
	StokBarangBAction.prototype = new TableAction();
	StokBarangBAction.prototype.edit = function(id) {
		var data = this.getRegulerData();
		data['command'] = "edit";
		data['id'] = id;
		$.post(
			"",
			data,
			function(response) {
				var json = getContent(response);
				if (json == null) return;
				$("#stok_barang_b_id").val(json.id);
				$("#stok_barang_b_id_dbarang_masuk").val(json.id_dbarang_masuk);
				$("#stok_barang_b_nama_barang").val(json.nama_barang);
				$("#stok_barang_b_nama_jenis_barang").val(json.nama_jenis_barang);
				$("#stok_barang_b_sisa").val(json.sisa);
				$("#stok_barang_b_satuan").val(json.satuan);
				$("#stok_barang_b_konversi").val(json.konversi);
				$("#stok_barang_b_satuan_konversi").val(json.satuan_konversi);
				$("#stok_barang_b_hna").val(json.hna);
				$("#stok_barang_b_tanggal_exp").val(json.tanggal_exp);
				$("#stok_barang_b_produsen").val(json.produsen);
				$("#stok_barang_b_id_vendor").val(json.id_vendor);
				$("#stok_barang_b_nama_vendor").val(json.nama_vendor);
				$("#stok_barang_b_f_sisa").val(json.sisa + " " + json.satuan);
				$("#stok_barang_b_keterangan").val("1 " + json.satuan + " = " + json.konversi + " " + json.satuan_konversi);
				$("#stok_barang_b_jumlah").val("");
				$("#modal_alert_stok_barang_b_add_form").html("");
				$(".error_field").removeClass("error_field");
				$("#stok_barang_b_add_form").smodal("show");
			}
		);
	};
	StokBarangBAction.prototype.validate = function() {
		var valid = true;
		var invalid_msg = "";
		var sisa = $("#stok_barang_b_sisa").val();
		var jumlah = $("#stok_barang_b_jumlah").val();
		$(".error_field").removeClass("error_field");
		if (jumlah == "") {
			valid = false;
			invalid_msg += "</br><strong>Jumlah</strong> tidak boleh kosong";
			$("#stok_barang_b_jumlah").addClass("error_field");
		} else if (!is_numeric(jumlah)) {
			valid = false;
			invalid_msg += "</br><strong>Jumlah</strong> seharusnya numerik (0-9)";
			$("#stok_barang_b_jumlah").addClass("error_field");
		} else if (parseFloat(jumlah) > parseFloat(sisa)) {
			valid = false;
			invalid_msg += "</br><strong>Jumlah</strong> melebihi sisa";
			$("#stok_barang_b_jumlah").addClass("error_field");
		}
		if (!valid) {
			$("#modal_alert_stok_barang_b_add_form").html(
				"<div class='alert alert-block alert-danger'>" +
					"<h4>Peringatan</h4>" +
					invalid_msg +
				"</div>"
			);
		}
		return valid;
	};
	StokBarangBAction.prototype.save = function() {
		if (!this.validate()) {
			return;
		}
		$("#stok_barang_b_add_form").smodal("hide");
		showLoading();
		var self = this;
		var data = this.getRegulerData();
		data['command'] = "save";
		data['id'] = $("#stok_barang_b_id").val();
		data['id_dbarang_masuk'] = $("#stok_barang_b_id_dbarang_masuk").val();
		data['nama_barang'] = $("#stok_barang_b_nama_barang").val();
		data['nama_jenis_barang'] = $("#stok_barang_b_nama_jenis_barang").val();
		data['jumlah'] = parseFloat($("#stok_barang_b_jumlah").val());
		data['konversi'] = parseFloat($("#stok_barang_b_konversi").val());
		data['sisa'] = parseFloat($("#stok_barang_b_sisa").val()) - parseFloat($("#stok_barang_b_jumlah").val());
		data['satuan'] = $("#stok_barang_b_satuan_konversi").val();
		data['hna'] = parseFloat($("#stok_barang_b_hna").val()) / parseFloat($("#stok_barang_b_konversi").val());
		data['produsen'] = $("#stok_barang_b_produsen").val();
		data['tanggal_exp'] = $("#stok_barang_b_tanggal_exp").val();
		data['id_vendor'] = $("#stok_barang_b_id_vendor").val();
		data['nama_vendor'] = $("#stok_barang_b_nama_vendor").val();
		$.post(
			"",
			data,
			function(response) {
				var json = getContent(response);
				if (json == null) {
					$("#stok_barang_b_add_form").smodal("show");
				} else {
					self.view();
					stok_barang_t.view();
				}
				dismissLoading();
			}
		);
	};
	
	var stok_barang_b;
	$(document).ready(function() {
		$('[data-toggle="popover"]').popover({
			trigger: 'hover',
			'placement': 'top'
		});
		$(".mydate").datepicker();
		var stok_barang_b_columns = new Array("id", "id_dbarang_masuk", "nama_barang", "nama_jenis_barang", "jumlah", "sisa", "satuan", "konversi", "satuan_konversi", "hna", "tanggal_exp", "id_vendor", "nama_vendor", "produsen");
		stok_barang_b = new StokBarangBAction(
			"stok_barang_b",
			"gudang_umum",
			"daftar_stok_barang_dapat_dibongkar",
			stok_barang_b_columns
		);
		stok_barang_b.view();
	});
</script>