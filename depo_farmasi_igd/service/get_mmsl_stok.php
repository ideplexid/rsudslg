<?php
	require_once("depo_farmasi_igd/library/InventoryLibrary.php");
	global $db;
	
	if (isset($_POST['nama_obat']) && 
		isset($_POST['nama_jenis_obat'])) {
		$nama_obat = $_POST['nama_obat'];
		$nama_jenis_obat = $_POST['nama_jenis_obat'];
		$dbtable = new DBTable($db, InventoryLibrary::$_TBL_STOK_OBAT);
		$row = $dbtable->get_row("
			SELECT b.nama_obat, b.nama_jenis_obat, SUM(b.sisa) AS 'jumlah'
			FROM " . InventoryLibrary::$_TBL_OBAT_MASUK . " a LEFT JOIN " . InventoryLibrary::$_TBL_STOK_OBAT . " b ON a.id = b.id_obat_masuk
			WHERE a.prop NOT LIKE 'del' AND a.status = 'sudah' AND b.prop NOT LIKE 'del' AND b.nama_obat = '" . $nama_obat . "' AND b.nama_jenis_obat = '" . $nama_jenis_obat . "'
			GROUP BY b.nama_obat, b.nama_jenis_obat
		");
		$sisa_stok = 0;
		if ($row != null)
			$sisa_stok = $row->jumlah;
		
		$data = array();
		$data['jumlah'] = $sisa_stok;
		$data['unit'] = "depo_farmasi_igd";
		echo json_encode($data);
	}
?>