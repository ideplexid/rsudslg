<?php 
	require_once("smis-base/smis-include-service-consumer.php");
	require_once("depo_farmasi_igd/table/PenjualanInstansiLainTable.php");
	require_once("depo_farmasi_igd/table/DPenjualanInstansiLainTable.php");
	require_once("depo_farmasi_igd/adapter/PenjualanInstansiLainAdapter.php");
	require_once("depo_farmasi_igd/adapter/ObatAdapter.php");
	require_once("depo_farmasi_igd/responder/PenjualanInstansiLainDBResponder.php");
	require_once("depo_farmasi_igd/responder/ObatDBResponder.php");
	require_once("depo_farmasi_igd/responder/SisaDBResponder.php");
	require_once("depo_farmasi_igd/library/InventoryLibrary.php");
	global $db;
	
	$penjualan_instansi_lain_table = new PenjualanInstansiLainTable(
		array("No. Penjualan", "No. Resep", "Tanggal/Jam", "Nama Instansi", "Alamat Instansi", "No. Telp. Instansi", "Status"),
		"Depo Farmasi IGD : Penjualan UP",
		null,
		true
	);
	$penjualan_instansi_lain_table->setName("penjualan_instansi_lain");
	
	if (isset($_POST['super_command']) && $_POST['super_command'] == "penjualan_instansi_lain") {
		if (isset($_POST['command'])) {
			if ($_POST['command'] == "get_margin_penjualan") {
				/// mode margin :
				/// 0	=> paten
				/// 1   => per rekanan
				/// 2 	=> per obat
				$mode_margin = $_POST['mode_margin'];
				$id_rekanan = $_POST['id_rekanan'];
				$margin_penjualan = 0;
				if ($mode_margin == 0)
					$margin_penjualan = getSettings($db, "depo_farmasi2-penjualan_instansi_lain-margin_penjualan", 0) / 100;
				else if ($mode_margin == 1) {
					$dbtable = new DBTable($db, InventoryLibrary::$_TBL_INSTANSI_LAIN);
					$row = $dbtable->get_row("
						SELECT *
						FROM " . InventoryLibrary::$_TBL_INSTANSI_LAIN . "
						WHERE prop NOT LIKE 'del' AND id = '" . $id_rekanan . "'
					");
					if ($row != null)
						$margin_penjualan = $row->margin_jual / 100;
				}
				$data = array(
					"margin_penjualan" => $margin_penjualan
				);
				echo json_encode($data);
				return;
			}
			$penjualan_instansi_lain_adapter = new PenjualanInstansiLainAdapter();
			$columns = array("id", "tanggal", "nomor_resep", "markup", "nrm_pasien", "nama_pasien", "alamat_pasien", "no_telpon", "total", "dibatalkan", "diskon", "t_diskon");
			$penjualan_instansi_lain_dbtable = new DBTable(
				$db,
				InventoryLibrary::$_TBL_PENJUALAN_RESEP,
				$columns
			);
			$penjualan_instansi_lain_dbtable->addCustomKriteria("tipe", " ='instansi_lain' ");
			$penjualan_instansi_lain_dbtable->setOrder(" id DESC ");
			$penjualan_instansi_lain_dbresponder = new PenjualanInstansiLainDBResponder(
				$penjualan_instansi_lain_dbtable,
				$penjualan_instansi_lain_table,
				$penjualan_instansi_lain_adapter
			);
			if ($penjualan_instansi_lain_dbresponder->isSave()) {
				global $user;
				$penjualan_instansi_lain_dbresponder->addColumnFixValue("operator", $user->getNameOnly());
			}
			$data = $penjualan_instansi_lain_dbresponder->command($_POST['command']);
			echo json_encode($data);
			return;
		}
		return;
	}
	
	//get apotek lain chooser:
	$instansi_lain_table = new Table(
		array("Nomor", "Nama", "Alamat", "No. Telp."),
		"",
		null,
		true
	);
	$instansi_lain_table->setName("instansi_lain");
	$instansi_lain_table->setModel(Table::$SELECT);
	$instansi_lain_adapter = new SimpleAdapter();
	$instansi_lain_adapter->add("Nomor", "id", "digit8");
	$instansi_lain_adapter->add("Nama", "nama");
	$instansi_lain_adapter->add("Alamat", "alamat");
	$instansi_lain_adapter->add("No. Telp.", "telpon");
	$instansi_lain_dbtable = new DBTable($db, InventoryLibrary::$_TBL_INSTANSI_LAIN);
	$instansi_lain_dbresponder = new DBResponder(
		$instansi_lain_dbtable,
		$instansi_lain_table,
		$instansi_lain_adapter
	);
	
	//get obat chooser:
	$obat_table = new Table(
		array("Kode", "Obat", "Jenis", "Stok"),
		"",
		null,
		true
	);
	$obat_table->setName("obat_instansi_lain");
	$obat_table->setModel(Table::$SELECT);
	$obat_adapter = new ObatAdapter();
	$obat_dbtable = new DBTable($db, InventoryLibrary::$_TBL_STOK_OBAT);
	$obat_dbtable->setViewForSelect(true);
	$filter = "";
	if (isset($_POST['kriteria'])) {
		$filter = " AND (nama_obat LIKE '%" . $_POST['kriteria'] . "%' OR nama_jenis_obat LIKE '%" . $_POST['kriteria'] . "%')";
	}
	$query_value = "
		SELECT v_stok.*, v_harga.hna, v_harga.markup
		FROM (
			SELECT id_obat AS 'id', kode_obat, nama_obat, nama_jenis_obat, GROUP_CONCAT(CONCAT(sisa, ' ', satuan, ' ', label) ORDER BY satuan, sisa, label ASC SEPARATOR ', ') AS 'stok', satuan, konversi, satuan_konversi
			FROM (
				SELECT id_obat, kode_obat, nama_obat, nama_jenis_obat, SUM(sisa) AS 'sisa', satuan, konversi, satuan_konversi, CASE label WHEN 'reguler' THEN '(R)' WHEN 'sito' THEN '(S)' ELSE '(K)' END AS 'label'
				FROM " . InventoryLibrary::$_TBL_STOK_OBAT . " a LEFT JOIN " . InventoryLibrary::$_TBL_OBAT_MASUK . " b ON a.id_obat_masuk = b.id
				WHERE a.prop NOT LIKE 'del' AND b.prop NOT LIKE 'del' AND b.status = 'sudah' AND a.sisa > 0 " . $filter . "
				GROUP BY id_obat, satuan, konversi, satuan_konversi, label
			) v_obat
			GROUP BY id_obat
		) v_stok LEFT JOIN (
			SELECT id_obat, nama_obat, nama_jenis_obat,  MAX(hna) AS 'hna', 0 AS 'markup', satuan, konversi, satuan_konversi
			FROM " . InventoryLibrary::$_TBL_STOK_OBAT . " a LEFT JOIN " . InventoryLibrary::$_TBL_OBAT_MASUK . " b ON a.id_obat_masuk = b.id
			WHERE a.prop NOT LIKE 'del' AND b.prop NOT LIKE 'del' AND b.status = 'sudah' " . $filter . "
			GROUP BY id_obat, nama_obat, nama_jenis_obat, satuan, konversi, satuan_konversi
		) v_harga ON v_stok.id = v_harga.id_obat AND v_stok.satuan = v_harga.satuan AND v_stok.konversi = v_harga.konversi AND v_stok.satuan_konversi = v_harga.satuan_konversi
	";
	$query_count = "
		SELECT COUNT(*)
		FROM (
			" . $query_value . "
		) v_obat
	";
	$obat_dbtable->setPreferredQuery(true, $query_value, $query_count);
	$obat_dbresponder = new ObatDBResponder(
		$obat_dbtable,
		$obat_table,
		$obat_adapter
	);
	
	//get sisa, hna, dan markup by id obat, satuan, konversi, satuan_konversi:
	$sisa_table = new Table(
		array("id_obat", "sisa", "satuan", "konversi", "satuan_konversi"),
		"",
		null,
		true
	);
	$sisa_table->setName("sisa");
	$sisa_adapter = new SimpleAdapter();
	$sisa_adapter->add("id_obat", "id_obat");
	$sisa_adapter->add("sisa", "sisa");
	$sisa_adapter->add("satuan", "satuan");
	$sisa_adapter->add("konversi", "konversi");
	$sisa_adapter->add("satuan_konversi", "satuan_konversi");
	$columns = array("id_obat", "sisa", "satuan", "konversi", "satuan_konversi");
	$sisa_dbtable = new DBTable(
		$db,
		InventoryLibrary::$_TBL_STOK_OBAT,
		$columns
	);
	$sisa_dbresponder = new SisaDBResponder(
		$sisa_dbtable,
		$sisa_table,
		$sisa_adapter
	);
	
	$super_command = new SuperCommand();
	$super_command->addResponder("instansi_lain", $instansi_lain_dbresponder);
	$super_command->addResponder("obat_instansi_lain", $obat_dbresponder);
	$super_command->addResponder("sisa", $sisa_dbresponder);
	$init = $super_command->initialize();
	if ($init != null) {
		echo $init;
		return;
	}
	
	$penjualan_instansi_lain_modal = new Modal("penjualan_instansi_lain_add_form", "smis_form_container", "penjualan_instansi_lain");
	$penjualan_instansi_lain_modal->setTitle("Data Penjualan UP");
	$penjualan_instansi_lain_modal->setClass(Modal::$FULL_MODEL);
	$id_hidden = new Hidden("penjualan_instansi_lain_id", "penjualan_instansi_lain_id", "");
	$penjualan_instansi_lain_modal->addElement("", $id_hidden);
	$ppn_hidden = new Hidden("penjualan_instansi_lain_ppn", "penjualan_instansi_lain_ppn", getSettings($db, "depo_farmasi2-daftar_penjualan_instansi_lain-ppn_jual"));
	$penjualan_instansi_lain_modal->addElement("", $ppn_hidden);
	$nomor_text = new Text("penjualan_instansi_lain_nomor", "penjualan_instansi_lain_nomor", "");
	$nomor_text->addAtribute("autofocus");
	$penjualan_instansi_lain_modal->addElement("Nomor", $nomor_text);
	$markup_hidden = new Hidden("penjualan_instansi_lain_markup", "penjualan_instansi_lain_markup", "0");
	$penjualan_instansi_lain_modal->addElement("", $markup_hidden);
	$id_instansi_lain_hidden = new Hidden("penjualan_instansi_lain_id_instansi_lain", "penjualan_instansi_lain_id_instansi_lain", "");
	$penjualan_instansi_lain_modal->addElement("", $id_instansi_lain_hidden);
	$instansi_lain_button = new Button("", "", "Pilih");
	$instansi_lain_button->setClass("btn-info");
	$instansi_lain_button->setIsButton(Button::$ICONIC);
	$instansi_lain_button->setIcon("icon-white ".Button::$icon_list_alt);
	$instansi_lain_button->setAction("instansi_lain.chooser('instansi_lain', 'instansi_lain_button', 'instansi_lain', instansi_lain)");
	$instansi_lain_button->setAtribute("id='instansi_lain_browse'");
	$instansi_lain_text = new Text("penjualan_instansi_lain_nama_instansi_lain", "penjualan_instansi_lain_nama_instansi_lain", "");
	$instansi_lain_text->setClass("smis-one-option-input");
	$instansi_lain_input_group = new InputGroup("");
	$instansi_lain_input_group->addComponent($instansi_lain_text);
	$instansi_lain_input_group->addComponent($instansi_lain_button);
	$penjualan_instansi_lain_modal->addElement("Instansi Lain", $instansi_lain_input_group);
	$alamat_instansi_lain_text = new Text("penjualan_instansi_lain_alamat_instansi_lain", "penjualan_instansi_lain_alamat_instansi_lain", "");
	$alamat_instansi_lain_text->setAtribute("disabled='disabled'");
	$penjualan_instansi_lain_modal->addElement("Alamat", $alamat_instansi_lain_text);
	$telpon_instansi_lain_text = new Text("penjualan_instansi_lain_telpon_instansi_lain", "penjualan_instansi_lain_telpon_instansi_lain", "");
	$telpon_instansi_lain_text->setAtribute("disabled='disabled'");
	$penjualan_instansi_lain_modal->addElement("No. Telp.", $telpon_instansi_lain_text);
	$diskon_text = new Text("penjualan_instansi_lain_diskon", "penjualan_instansi_lain_diskon", "");
	$diskon_text->setTypical("money");
	$diskon_text->setAtribute(" data-thousands=\".\" data-decimal=\",\"  data-precision=\"2\" " );
	$penjualan_instansi_lain_modal->addElement("Diskon", $diskon_text);
	$t_diskon_option = new OptionBuilder();
	$t_diskon_option->add("Persen (%)", "persen", "1");
	$t_diskon_option->add("Nominal (Rp)", "nominal");
	$t_diskon_select = new Select("penjualan_instansi_lain_t_diskon", "penjualan_instansi_lain_t_diskon", $t_diskon_option->getContent());
	$penjualan_instansi_lain_modal->addElement("Tipe Diskon", $t_diskon_select);
	$total_text = new Text("penjualan_instansi_lain_total", "penjualan_instansi_lain_total", "");
	$total_text->setTypical("money");
	$total_text->setAtribute(" data-thousands=\".\" data-decimal=\",\" data-prefix=\"Rp. \"  data-precision=\"2\"  disabled='disabled'" );
	$penjualan_instansi_lain_modal->addElement("Total", $total_text);
	$dpenjualan_instansi_lain_table = new DPenjualanInstansiLainTable(
		array("No.", "Nama", "Jumlah", "Harga", "Embalase", "Tuslah", "Subtotal"),
		"",
		null,
		true
	);
	$dpenjualan_instansi_lain_table->setName("dpenjualan_instansi_lain");
	$dpenjualan_instansi_lain_table->setFooterVisible(false);
	$penjualan_instansi_lain_modal->addBody("dpenjualan_instansi_lain_table", $dpenjualan_instansi_lain_table);
	$penjualan_instansi_lain_button = new Button("", "", "Simpan");
	$penjualan_instansi_lain_button->setClass("btn-success");
	$penjualan_instansi_lain_button->setIcon("fa fa-floppy-o");
	$penjualan_instansi_lain_button->setIsButton(Button::$ICONIC);
	$penjualan_instansi_lain_button->setAtribute("id='penjualan_instansi_lain_save'");
	$penjualan_instansi_lain_modal->addFooter($penjualan_instansi_lain_button);
	$penjualan_instansi_lain_button = new Button("", "", "OK");
	$penjualan_instansi_lain_button->setClass("btn-success");
	$penjualan_instansi_lain_button->setAtribute("id='penjualan_instansi_lain_ok'");
	$penjualan_instansi_lain_button->setAction("$($(this).data('target')).smodal('hide')");
	$penjualan_instansi_lain_modal->addFooter($penjualan_instansi_lain_button);
	$tombol='<a href="#" class="input btn btn-info" ><i class="icon-white icon-list-alt"></i></a>';
	$penjualan_instansi_lain_modal->addHTML("
		<div class='alert alert-block alert-inverse' id='help_instansi_lain'>
			<h4>Tips</h4>
			Tombol <kbd>Tab</kbd> : Berpindah Cepat ke Isian Berikutnya (dari Kiri ke Kanan)<br/>
			Ketik Min. <strong>3 Karakter</strong> / Tombol " . $tombol . " / <kbd>F7</kbd> pada Isian Instansi Lain : Menentukan Nama Instansi Lain<br/>
			<li>Tombol <kbd>&uarr;</kbd> / <kbd>&darr;</kbd> : Memilih <strong>Persen (%) / Nominal (Rp) / Gratis (100 %)</strong> pada Isian <strong>Tipe Diskon</strong></li>	
			<li>Tombol <kbd>F3</kbd> : Tombol Cepat Menambahkan Obat Jadi Baru</li>
			<li>Tombol <kbd>F6</kbd> : Tombol Cepat Menyimpan Data Transaksi, Pastikan Semua Obat dan Informasi Kepala Transaksi Sudah Lengkap.</li>
		</div>
	", "after");
	
	$dpenjualan_instansi_lain_modal = new Modal("dpenjualan_instansi_lain_add_form", "smis_form_container", "dpenjualan_instansi_lain");
	$dpenjualan_instansi_lain_modal->setTitle("Data Detail Penjualan UP");
	$id_hidden = new Hidden("dpenjualan_instansi_lain_id", "dpenjualan_instansi_lain_id", "");
	$dpenjualan_instansi_lain_modal->addElement("", $id_hidden);
	$id_obat_hidden = new Hidden("dpenjualan_instansi_lain_id_obat", "dpenjualan_instansi_lain_id_obat", "");
	$dpenjualan_instansi_lain_modal->addElement("", $id_obat_hidden);
	$nama_obat_hidden = new Hidden("dpenjualan_instansi_lain_name_obat", "dpenjualan_instansi_lain_name_obat", "");
	$dpenjualan_instansi_lain_modal->addElement("", $nama_obat_hidden);
	$obat_button = new Button("", "", "Pilih");
	$obat_button->setClass("btn-info");
	$obat_button->setAction("obat_instansi_lain.chooser('obat_instansi_lain', 'obat_instansi_lain_button', 'obat_instansi_lain', obat_instansi_lain)");
	$obat_button->setIcon("icon-white icon-list-alt");
	$obat_button->setIsButton(Button::$ICONIC);
	$obat_button->setAtribute("id='dpenjualan_instansi_lain_browse'");
	$nama_obat_text = new Text("dpenjualan_instansi_lain_nama_obat", "dpenjualan_instansi_lain_nama_obat", "");
	$nama_obat_text->addAtribute("autofocus");
	$nama_obat_text->setClass("smis-one-option-input");
	$nama_obat_input_group = new InputGroup("");
	$nama_obat_input_group->addComponent($nama_obat_text);
	$nama_obat_input_group->addComponent($obat_button);
	$dpenjualan_instansi_lain_modal->addElement("Obat", $nama_obat_input_group);
	$kode_obat_hidden = new Hidden("dpenjualan_instansi_lain_kode_obat", "dpenjualan_instansi_lain_kode_obat", "");
	$dpenjualan_instansi_lain_modal->addElement("", $kode_obat_hidden);
	$name_obat_hidden = new Hidden("dpenjualan_instansi_lain_name_obat", "dpenjualan_instansi_lain_name_obat", "");
	$dpenjualan_instansi_lain_modal->addElement("", $name_obat_hidden);
	$nama_jenis_obat_hidden = new Hidden("dpenjualan_instansi_lain_nama_jenis_obat", "dpenjualan_instansi_lain_nama_jenis_obat", "");
	$dpenjualan_instansi_lain_modal->addElement("", $nama_jenis_obat_hidden);
	$satuan_select = new Select("dpenjualan_instansi_lain_satuan", "dpenjualan_instansi_lain_satuan", "");
	$dpenjualan_instansi_lain_modal->addElement("Satuan", $satuan_select);
	$konversi_hidden = new Hidden("dpenjualan_instansi_lain_konversi", "dpenjualan_instansi_lain_konversi", "");
	$konversi_hidden->setAtribute("disabled='disabled'");
	$dpenjualan_instansi_lain_modal->addElement("", $konversi_hidden);
	$satuan_konversi_hidden = new Hidden("dpenjualan_instansi_lain_satuan_konversi", "dpenjualan_instansi_lain_satuan_konversi", "");
	$dpenjualan_instansi_lain_modal->addElement("", $satuan_konversi_hidden);
	$stok_hidden = new Hidden("dpenjualan_instansi_lain_stok", "dpenjualan_instansi_lain_stok", "");
	$dpenjualan_instansi_lain_modal->addElement("", $stok_hidden);
	$f_stok_text = new Text("dpenjualan_instansi_lain_f_stok", "dpenjualan_instansi_lain_f_stok", "");
	$f_stok_text->setAtribute("disabled='disabled'");
	$dpenjualan_instansi_lain_modal->addElement("Stok", $f_stok_text);
	$jumlah_lama_hidden = new Hidden("dpenjualan_instansi_lain_jumlah_lama", "dpenjualan_instansi_lain_jumlah_lama", "");
	$dpenjualan_instansi_lain_modal->addElement("", $jumlah_lama_hidden);
	$hna_text = new Text("dpenjualan_instansi_lain_hna", "dpenjualan_instansi_lain_hna", "");
	$hna_text->setTypical("money");
	$hna_text->setAtribute(" data-thousands=\".\" data-decimal=\",\" data-prefix=\"Rp. \"  data-precision=\"2\"  disabled='disabled'" );
	$dpenjualan_instansi_lain_modal->addElement("HJA", $hna_text);
	if (getSettings($db, "depo_farmasi2-penjualan_instansi_lain-embalase_show-obat_jadi", 0) == 1) {
		$embalase_text = new Text("dpenjualan_instansi_lain_embalase", "dpenjualan_instansi_lain_embalase", "");
		$embalase_text->setTypical("money");
		$embalase_text->setAtribute(" data-thousands=\".\" data-decimal=\",\" data-prefix=\"Rp. \"  data-precision=\"2\"" );
		$dpenjualan_instansi_lain_modal->addElement("Embalase", $embalase_text);
	} else {
		$embalase_hidden = new Hidden("dpenjualan_instansi_lain_embalase", "dpenjualan_instansi_lain_embalase", "");
		$dpenjualan_instansi_lain_modal->addElement("", $embalase_hidden);
	}
	if (getSettings($db, "depo_farmasi2-penjualan_instansi_lain-tuslah_show-obat_jadi", 0) == 1) {
		$tuslah_text = new Text("dpenjualan_instansi_lain_tuslah", "dpenjualan_instansi_lain_tuslah", "");
		$tuslah_text->setTypical("money");
		$tuslah_text->setAtribute(" data-thousands=\".\" data-decimal=\",\" data-prefix=\"Rp. \"  data-precision=\"2\"" );
		$dpenjualan_instansi_lain_modal->addElement("Tusla", $tuslah_text);
	} else {
		$tuslah_hidden = new Hidden("dpenjualan_instansi_lain_tuslah", "dpenjualan_instansi_lain_tuslah", "");
		$dpenjualan_instansi_lain_modal->addElement("", $tuslah_hidden);
	}
	$jumlah_text = new Text("dpenjualan_instansi_lain_jumlah", "dpenjualan_instansi_lain_jumlah", "");
	$dpenjualan_instansi_lain_modal->addElement("Jumlah", $jumlah_text);
	$dpenjualan_instansi_lain_button = new Button("", "", "Simpan");
	$dpenjualan_instansi_lain_button->setClass("btn-success");
	$dpenjualan_instansi_lain_button->setAtribute("id='dpenjualan_instansi_lain_save'");
	$dpenjualan_instansi_lain_button->setIcon("fa fa-floppy-o");
	$dpenjualan_instansi_lain_button->setIsButton(Button::$ICONIC);
	$dpenjualan_instansi_lain_modal->addFooter($dpenjualan_instansi_lain_button);
	$dpenjualan_instansi_lain_modal->addHTML("
		<div class='alert alert-block alert-inverse'>
			<h4>Tips</h4>
			<ul>
				<li><small>Ketik Min. <strong>3 Karakter</strong> / Tombol " . $tombol . " / Tombol <kbd>F2</kbd> : Menentukan Nama Obat</small></li>
				<li><small>Tombol <kbd>Esc</kbd> : Tombol Cepat Kembali ke Formulir Penjualan Instansi Lain</small></li>
				<li>Tombol <kbd>F6</kbd> : Tombol Menyimpan Obat Jadi</li>
			</ul>
		</div>
	", "before");
	
	echo "<div class='alert alert-block alert-inverse'>" .
			 "<h4>Tips</h4>" .
			 "Tombol <kbd>F2</kbd> : Menampilkan Formulir Penjualan Instansi Lain Baru" .
		 "</div>";
	echo $dpenjualan_instansi_lain_modal->getHtml();
	echo $penjualan_instansi_lain_modal->getHtml();
	echo $penjualan_instansi_lain_table->getHtml();
	echo addCSS("depo_farmasi_igd/css/penjualan_instansi_lain.css", false);
	echo addJS("depo_farmasi_igd/js/penjualan_instansi_lain_action.js", false);
	echo addJS("depo_farmasi_igd/js/dpenjualan_instansi_lain_action.js", false);
	echo addJS("depo_farmasi_igd/js/instansi_lain_action.js", false);
	echo addJS("depo_farmasi_igd/js/obat_instansi_lain_action.js", false);
	echo addJS("smis-base-js/smis-base-shortcut.js",false);
	echo addJS("smis-libs-out/webprint/webprint.js", false);
	echo addJS("framework/smis/js/table_action.js");
?>
<script type="text/javascript">
	var embalase_obat_jadi = <?php echo getSettings($db, "depo_farmasi2-penjualan_instansi_lain-embalase-obat_jadi", 0); ?>;
	var tuslah_obat_jadi = <?php echo getSettings($db, "depo_farmasi2-penjualan_instansi_lain-tuslah-obat_jadi", 0); ?>;

	/// mode margin :
	/// 0	=> paten
	/// 1   => per rekanan
	/// 2 	=> per obat
	var mode_margin = <?php echo getSettings($db, "depo_farmasi2-penjualan_instansi_lain-mode_margin_penjualan", 0); ?>;
	var need_margin_penjualan_request = 1;

	var penjualan_instansi_lain;
	var dpenjualan_instansi_lain;
	var instansi_lain;
	var obat_instansi_lain;
	var dpal_num;
	$(document).ready(function() {
		$('.modal').on('shown.bs.modal', function() {
			$(this).find('[autofocus]').focus();
		});
		$('[data-toggle="popover"]').popover({
			trigger: 'hover',
			'placement': 'top'
		});
		$("*").dblclick(function(e) {
			e.preventDefault();
		})
		$("#penjualan_instansi_lain_add_form").on("show", function() {
			$("ul.typeahead").hide();
		});
		$("#smis-chooser-modal").on("show", function() {
			$("#smis-chooser-modal").removeClass("half_model");
			$("#smis-chooser-modal").addClass("half_model");
		});
		$("#smis-chooser-modal").on("hidden", function() {
			$("#smis-chooser-modal").removeClass("half_model");
		});
		$("#penjualan_instansi_lain_t_diskon").on("change", function() {
			var diskon = $("#resep_diskon").val().replace(/[^0-9-,]/g, '').replace(",", ".");
			var t_diskon = $("#resep_t_diskon").val();
			if (diskon > 100 && t_diskon == "persen") {
				$("#modal_alert_penjualan_instansi_lain_add_form").html(
					"<div class='alert alert-block alert-danger'>" +
						"<h4>Peringatan</h4>" +
						"<br /><b>Diskon</b> tidak boleh melebihi 100%" +
					"</div>"
				);
				return;
			}
			$("#modal_alert_penjualan_instansi_lain_add_form").html("");
			if ($("#penjualan_instansi_lain_t_diskon").val() == "gratis") {
				$("#penjualan_instansi_lain_diskon").val("100,00");
				$("#penjualan_instansi_lain_diskon").removeAttr("disabled");
				$("#penjualan_instansi_lain_diskon").attr("disabled", "disabled");
			} else {
				$("#penjualan_instansi_lain_diskon").val("0,00");
				$("#penjualan_instansi_lain_diskon").removeAttr("disabled");
			}
			penjualan_instansi_lain.refreshBiayaTotal();
		});
		$("#penjualan_instansi_lain_add_form").on("show", function() {
			$("ul.typeahead").hide();
		});
		$("#penjualan_instansi_lain_diskon").on("keyup", function() {
			var diskon = $("#penjualan_instansi_lain_diskon").val().replace(/[^0-9-,]/g, '').replace(",", ".");
			var t_diskon = $("#penjualan_instansi_lain_t_diskon").val();
			if (diskon > 100 && t_diskon == "persen") {
				$("#modal_alert_penjualan_instansi_lain_add_form").html(
					"<div class='alert alert-block alert-danger'>" +
						"<h4>Peringatan</h4>" +
						"<br /><b>Diskon</b> tidak boleh melebihi 100%" +
					"</div>"
				);
				return;
			} 
			$("#modal_alert_penjualan_instansi_lain_add_form").html("");
			penjualan_instansi_lain.refreshBiayaTotal();
		});
		$("#penjualan_instansi_lain_diskon").on("change", function() {
			var diskon = $("#penjualan_instansi_lain_diskon").val().replace(/[^0-9-,]/g, '').replace(",", ".");
			if (diskon == "") {
				$("#penjualan_instansi_lain_diskon").val("0,00");
			}
		});
		instansi_lain = new InstansiLainAction(
			"instansi_lain",
			"depo_farmasi_igd",
			"daftar_penjualan_instansi_lain",
			new Array()
		);
		instansi_lain.setSuperCommand("instansi_lain");
		obat_instansi_lain = new ObatInstansiLainAction(
			"obat_instansi_lain",
			"depo_farmasi_igd",
			"daftar_penjualan_instansi_lain",
			new Array()
		);
		obat_instansi_lain.setSuperCommand("obat_instansi_lain");
		dpenjualan_instansi_lain_columns = new Array("id", "id_penjualan_obat_jadi", "id_stok_obat", "jumlah", "satuan", "satuan_konversi", "konversi", "harga", "subtotal", "embalase", "tuslah");
		dpenjualan_instansi_lain = new DPenjualanInstansiLainAction(
			"dpenjualan_instansi_lain",
			"depo_farmasi_igd",
			"daftar_penjualan_instansi_lain",
			dpenjualan_instansi_lain_columns
		);
		penjualan_instansi_lain_columns = new Array("id", "nrm_pasien", "nama_pasien", "alamat_pasien", "no_telpon", "tanggal","total", "markup", "dibatalkan", "diskon", "t_diskon");
		penjualan_instansi_lain = new PenjualanInstansiLainAction(
			"penjualan_instansi_lain",
			"depo_farmasi_igd",
			"daftar_penjualan_instansi_lain",
			penjualan_instansi_lain_columns
		);
		penjualan_instansi_lain.setSuperCommand("penjualan_instansi_lain");
		penjualan_instansi_lain.view();
		
		//typeahead handlers section:
		var instansi_lain_nama = instansi_lain.getViewData();
		$('#penjualan_instansi_lain_nama_instansi_lain').typeahead({
			minLength:3,
			source: function (query, process) {
				var $items = new Array;
				$items = [""];                
				instansi_lain_nama['kriteria']=$('#penjualan_instansi_lain_nama_instansi_lain').val();
				$.ajax({
					url: '',
					type: 'POST',
					data: instansi_lain_nama,
					success: function(res) {
						var json=getContent(res);
						var the_data_proses=json.dbtable.data;
						$items = [""];      				
						$.map(the_data_proses, function(data){
							var group;
							group = {
								id: data.id,
								name: data.nama,                            
								toString: function () {
									return JSON.stringify(this);
								},
								toLowerCase: function () {
									return this.name.toLowerCase();
								},
								indexOf: function (string) {
									return String.prototype.indexOf.apply(this.name, arguments);
								},
								replace: function (string) {
									var value = '';
									value +=  this.name;
									if(typeof(this.level) != 'undefined') {
										value += ' <span class="pull-right muted">';
										value += this.level;
										value += '</span>';
									}
									return String.prototype.replace.apply('<div class="typeaheadiv">' + value + '</div>', arguments);
								}
							};
							$items.push(group);
						});
						process($items);
					}
				});
			},
			updater: function (item) {
				var item = JSON.parse(item);
				instansi_lain.select(item.id);
				$("#penjualan_instansi_lain_diskon").focus();
				return item.name;
			}
		});
		var obat_nama = obat_instansi_lain.getViewData();
		$('#dpenjualan_instansi_lain_nama_obat').typeahead({
			minLength:3,
			source: function (query, process) {
				var $items = new Array;
				$items = [""];                
				obat_nama['kriteria']=$('#dpenjualan_instansi_lain_nama_obat').val();
				$.ajax({
					url: '',
					type: 'POST',
					data: obat_nama,
					success: function(res) {
						var json=getContent(res);
						var the_data_proses=json.dbtable.data;
						$items = [""];      				
						$.map(the_data_proses, function(data){
							var group;
							group = {
								id: data.id,
								name: data.nama_obat,
								kode: data.kode_obat,                            
								toString: function () {
									return JSON.stringify(this);
								},
								toLowerCase: function () {
									return this.name.toLowerCase();
								},
								indexOf: function (string) {
									return String.prototype.indexOf.apply(this.name, arguments);
								},
								replace: function (string) {
									var value = '';
									value +=  this.kode + " - " + this.name;
									if(typeof(this.level) != 'undefined') {
										value += ' <span class="pull-right muted">';
										value += this.level;
										value += '</span>';
									}
									return String.prototype.replace.apply('<div class="typeaheadiv">' + value + '</div>', arguments);
								}
							};
							$items.push(group);
						});
						process($items);
					}
				});
			},
			updater: function (item) {
				var item = JSON.parse(item);  
				obat_instansi_lain.select(item.id);  
				$("#dpenjualan_instansi_lain_jumlah").focus();       
				return item.name;
			}
		});
		$("#penjualan_instansi_lain_nomor").keypress(function(e) {
			if(e.which == 13) {
				$('#penjualan_instansi_lain_nama_instansi_lain').focus();
			}
		});
		$("#dpenjualan_instansi_lain_jumlah").keypress(function(e) {
			$("ul.typeahead").html("");
			$("ul.typeahead").hide();
			if(e.which == 13) {
				$('#dpenjualan_instansi_lain_save').trigger('click');
			}
		});
		shortcut.add("F2", function() {
			var active_tab = $("ul.smis-tabs-header li.active a").text().replace(/[1]/g, '').trim().replace(/[\s]/, '_').toLowerCase();
			if (active_tab == "penjualan") {
				if(!$('#penjualan_instansi_lain_add_form').hasClass('in') && !$("#smis-chooser-modal").hasClass('in') && !$('#dpenjualan_instansi_lain_add_form').hasClass('in')) {
					$("#penjualan_instansi_lain_add").trigger("click");
				} else if ($("#dpenjualan_instansi_lain_add_form").hasClass('in')) {
					$("#dpenjualan_instansi_lain_browse").trigger("click");
				}
			}
		});
		shortcut.add("F7", function() {
			var active_tab = $("ul.smis-tabs-header li.active a").text().replace(/[1]/g, '').trim().replace(/[\s]/, '_').toLowerCase();
			if(active_tab == "penjualan") {
				if ($('#penjualan_instansi_lain_add_form').hasClass('in')) {
					$("#instansi_lain_browse").trigger("click");
				}
			}
		});
		shortcut.add("F3", function() {
			var active_tab = $("ul.smis-tabs-header li.active a").text().replace(/[1]/g, '').trim().replace(/[\s]/, '_').toLowerCase();
			if(active_tab == "penjualan") {
				if ($('#penjualan_instansi_lain_add_form').hasClass('in')) {
					$("#dpenjualan_instansi_lain_add").trigger('click');
				}
			}
		});
		shortcut.add("F6", function() {
			var active_tab = $("ul.smis-tabs-header li.active a").text().replace(/[1]/g, '').trim().replace(/[\s]/, '_').toLowerCase();
			if(active_tab == "penjualan") {
				if ($('#penjualan_instansi_lain_add_form').hasClass('in')) {
					penjualan_instansi_lain.save();
				}
			}
		});
		penjualan_instansi_lain.refreshHargaAndSubtotal();
	});
</script>