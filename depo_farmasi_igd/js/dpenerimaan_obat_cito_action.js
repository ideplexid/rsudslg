function DPenerimaanObatCitoAction(name, page, action, column) {
	this.initialize(name, page, action, column);
}
DPenerimaanObatCitoAction.prototype.constructor = DPenerimaanObatCitoAction;
DPenerimaanObatCitoAction.prototype = new TableAction();
DPenerimaanObatCitoAction.prototype.validate = function() {
	var valid = true;
	var invalid_msg = "";
	var nama_obat = $("#dpenerimaan_obat_cito_name_obat").val();
	var jumlah = $("#dpenerimaan_obat_cito_jumlah").val();
	var hna = $("#dpenerimaan_obat_cito_hna").val().replace(/[^0-9-,]/g, '').replace(",", ".");
	var produsen = $("#dpenerimaan_obat_cito_produsen").val();
	var tanggal_exp = $("#dpenerimaan_obat_cito_tanggal_exp").val();
	var no_batch = $("#dpenerimaan_obat_cito_no_batch").val();
	if (nama_obat == "") {
		valid = false;
		invalid_msg += "</br><strong>Nama Obat</strong> tidak boleh kosong";
		$("#dpenerimaan_obat_cito_nama_obat").addClass("error_field");
	}
	if (jumlah == "") {
		valid = false;
		invalid_msg += "</br><strong>Jumlah Diterima</strong> tidak boleh kosong";
		$("#dpenerimaan_obat_cito_jumlah").addClass("error_field");
	} else if (!is_numeric(jumlah)) {
		valid = false;
		invalid_msg += "</br><strong>Jumlah Diterima</strong> seharusnya numerik (0-9)";
		$("#dpenerimaan_obat_cito_jumlah").addClass("error_field");
	} else if (jumlah <= 0) {
		valid = false;
		invalid_msg += "</br><strong>Jumlah Diterima</strong> seharusnya > 0";
		$("#dpenerimaan_obat_cito_jumlah").addClass("error_field");
	}
	if (hna == "") {
		valid = false;
		invalid_msg += "</br><strong>Harga</strong> tidak boleh kosong";
		$("#dpenerimaan_obat_cito_hna").addClass("error_field");
	} else if (jumlah <= 0) {
		valid = false;
		invalid_msg += "</br><strong>Harga</strong> tidak boleh 0";
		$("#dpenerimaan_obat_cito_hna").addClass("error_field");
	}
	if (produsen == "") {
		valid = false;
		invalid_msg += "</br><strong>Produsen</strong> tidak boleh kosong";
		$("#dpenerimaan_obat_cito_produsen").addClass("error_field");
	}
	if (tanggal_exp == "") {
		valid = false;
		invalid_msg += "</br><strong>Tanggal Exp.</strong> tidak boleh kosong";
		$("#dpenerimaan_obat_cito_tanggal_exp").addClass("error_field");
	}
	if (no_batch == "") {
		valid = false;
		invalid_msg += "</br><strong>No. Batch</strong> tidak boleh kosong";
		$("#dpenerimaan_obat_cito_no_batch").addClass("error_field");
	}
	if (!valid)
		bootbox.alert(invalid_msg);
	return valid;
};
DPenerimaanObatCitoAction.prototype.clear = function() {
	$("#dpenerimaan_obat_cito_row_num").val("");
	$("#dpenerimaan_obat_cito_id_obat").val("");
	$("#dpenerimaan_obat_cito_kode_obat").val("");
	$("#dpenerimaan_obat_cito_name_obat").val("");
	$("#dpenerimaan_obat_cito_nama_obat").val("");
	$("#dpenerimaan_obat_cito_nama_jenis_obat").val("");
	$("#dpenerimaan_obat_cito_jumlah").val("");
	$("#dpenerimaan_obat_cito_satuan").html("");
	$("#dpenerimaan_obat_cito_konversi").val("");
	$("#dpenerimaan_obat_cito_satuan_konversi").val("");
	$("#dpenerimaan_obat_cito_hpp").val("Rp. 0,00");
	$("#dpenerimaan_obat_cito_hna").val("Rp. 0,00");
	$("#dpenerimaan_obat_cito_produsen").val("");
	$("#dpenerimaan_obat_cito_tanggal_exp").val("");
	$("#dpenerimaan_obat_cito_no_batch").val("");
};
DPenerimaanObatCitoAction.prototype.save = function() {
	var limited = $("#penerimaan_obat_limited").val();
	if (limited == "false") {
		if (!this.validate())
			return;
	}
	var r_num = $("#dpenerimaan_obat_cito_row_num").val();
	var id_obat = $("#dpenerimaan_obat_cito_id_obat").val();
	var kode_obat = $("#dpenerimaan_obat_cito_kode_obat").val();
	var nama_obat = $("#dpenerimaan_obat_cito_name_obat").val();
	var nama_jenis_obat = $("#dpenerimaan_obat_cito_nama_jenis_obat").val();
	var jumlah = $("#dpenerimaan_obat_cito_jumlah").val();
	var f_jumlah = parseFloat(jumlah).formatMoney("0", ".", ",");
	var satuan = $("#dpenerimaan_obat_cito_satuan").find(":selected").text();
	var konversi = $("#dpenerimaan_obat_cito_konversi").val();
	var satuan_konversi = $("#dpenerimaan_obat_cito_satuan_konversi").val();
	var hna = parseFloat($("#dpenerimaan_obat_cito_hna").val().replace(/[^0-9-,]/g, '').replace(",", "."));
	var f_hna = (hna / 1.1).formatMoney("2", ".", ",");
	var produsen = $("#dpenerimaan_obat_cito_produsen").val();
	var tanggal_exp = $("#dpenerimaan_obat_cito_tanggal_exp").val();
	var no_batch = $("#dpenerimaan_obat_cito_no_batch").val();
	var subtotal = jumlah * (hna / 1.1);
	subtotal = Math.round(subtotal);
	var f_subtotal = subtotal.formatMoney("2", ".", ",");
	
	if (r_num != "") {
		$("tr#data_" + r_num + " td#jumlah").html(jumlah);
		$("tr#data_" + r_num + " td#f_jumlah").html("<small><div align='right'>" + f_jumlah + "</div></small>");
		$("tr#data_" + r_num + " td#satuan").html(satuan);
		$("tr#data_" + r_num + " td#konversi").html(konversi);
		$("tr#data_" + r_num + " td#satuan_konversi").html(satuan_konversi);
		$("tr#data_" + r_num + " td#hna").html(hna);
		$("tr#data_" + r_num + " td#f_hna").html("<small><div align='right'>" + f_hna + "</div></small>");
		$("tr#data_" + r_num + " td#produsen").html(produsen);
		$("tr#data_" + r_num + " td#tanggal_exp").html(tanggal_exp);
		$("tr#data_" + r_num + " td#no_batch").html(no_batch);
		$("tr#data_" + r_num + " td#subtotal").html(subtotal);
		$("tr#data_" + r_num + " td#f_subtotal").html("<small><div align='right'>" + f_subtotal + "</div></small>");
	} else {
		var html_edit_action = "<a href='#' onclick='dpenerimaan_obat_cito.edit(" + row_num + ")' class='input btn btn-warning'><i class='fa fa-pencil'></i></a>";
		var html_del_action = "<a href='#' onclick='dpenerimaan_obat_cito.del(" + row_num + ")' class='input btn btn-danger'><i class='fa fa-trash-o'></i></a>";
		$("tbody#dpenerimaan_obat_cito_list").append(
			"<tr id='data_" + row_num + "'>" +
				"<td id='nomor'></td>" +
				"<td id='id' style='display: none;'></td>" +
				"<td id='id_obat' style='display: none;'>" + id_obat + "</td>" +
				"<td id='kode_obat'><small>" + kode_obat + "</small></td>" +
				"<td id='nama_obat'><small>" + nama_obat + "</small></td>" +
				"<td id='nama_jenis_obat'><small>" + nama_jenis_obat + "</small></td>" +
				"<td id='jumlah' style='display: none;'>" + jumlah + "</td>" +
				"<td id='konversi' style='display: none;'>" + konversi + "</td>" +
				"<td id='satuan_konversi' style='display: none;'>" + satuan_konversi + "</td>" +
				"<td id='f_jumlah'><small><div align='right'>" + f_jumlah + "</div></small></td>" +
				"<td id='satuan'><small>" + satuan + "</small></td>" +
				"<td id='hna' style='display: none;'>" + hna + "</td>" +
				"<td id='f_hna'><small>" + f_hna + "</small></td>" +
				"<td id='subtotal' style='display: none;'>" + subtotal + "</td>" +
				"<td id='f_subtotal'><small><div align='right'>" + f_subtotal + "</div></small></td>" +
				"<td id='produsen' style='display: none;'>" + produsen + "</td>" +
				"<td id='no_batch' style='display: none;'>" + no_batch + "</td>" +
				"<td id='tanggal_exp' style='display: none;'>" + tanggal_exp + "</td>" +
				"<td>" + html_edit_action + "</td>" +
				"<td>" + html_del_action + "</td>" +
			"</tr>"
		);
		row_num++;
	}
	this.clear();
	penerimaan_obat_cito.update_total();
	this.setEditMode("true");
	$("#dpenerimaan_obat_cito_nama_obat").focus();
};
DPenerimaanObatCitoAction.prototype.setEditMode = function(enable) {
	var limited = $("#penerimaan_obat_cito_limited").val();
	if (enable == "true") {
		if (limited == "false")
			$("#penerimaan_obat_cito_form_save").show();
		$("#penerimaan_obat_cito_form_update").hide();
		$("#penerimaan_obat_cito_form_cancel").hide();
	} else {
		if (limited == "false")
			$("#penerimaan_obat_cito_form_save").hide();
		$("#penerimaan_obat_cito_form_update").show();
		$("#penerimaan_obat_cito_form_cancel").show();
	}
};
DPenerimaanObatCitoAction.prototype.edit = function(r_num) {
	var id_obat = $("tr#data_" + r_num + " td#id_obat").text();
	var kode_obat = $("tr#data_" + r_num + " td#kode_obat").text();
	var nama_obat = $("tr#data_" + r_num + " td#nama_obat").text();
	var nama_jenis_obat = $("tr#data_" + r_num + " td#nama_jenis_obat").text();
	var jumlah_tercatat = parseFloat($("tr#data_" + r_num + " td#jumlah_tercatat").text());
	var jumlah = parseFloat($("tr#data_" + r_num + " td#jumlah").text());
	var satuan = $("tr#data_" + r_num + " td#konversi").text() + "_" + $("tr#data_" + r_num + " td#satuan_konversi").text();
	var konversi = $("tr#data_" + r_num + " td#konversi").text();
	var satuan_konversi = $("tr#data_" + r_num + " td#satuan_konversi").text();
	var hna = "Rp. " + parseFloat($("tr#data_" + r_num + " td#hna").text()).formatMoney("2", ".", ",");
	var hpp = "Rp. " + (parseFloat($("tr#data_" + r_num + " td#hna").text()) / 1.1).formatMoney("2", ".", ",");
	var diskon = parseFloat($("tr#data_" + r_num + " td#diskon").text()).formatMoney("2", ".", ",");
	var t_diskon = $("tr#data_" + r_num + " td#t_diskon").text();
	var produsen = $("tr#data_" + r_num + " td#produsen").text();
	var tanggal_exp = $("tr#data_" + r_num + " td#tanggal_exp").text();
	var no_batch = $("tr#data_" + r_num + " td#no_batch").text();
	var self = this;
	var data = this.getRegulerData();
	data['super_command'] = "obat";
	data['command'] = "edit";
	data['id'] = id_obat;
	$.post(
		"",
		data,
		function(response) {
			var json = getContent(response);
			if (json == null) return;
			self.setEditMode("false");
			$("#dpenerimaan_obat_cito_row_num").val(r_num);
			$("#dpenerimaan_obat_cito_id_obat").val(id_obat);
			$("#dpenerimaan_obat_cito_kode_obat").val(kode_obat);
			$("#dpenerimaan_obat_cito_name_obat").val(nama_obat);
			$("#dpenerimaan_obat_cito_nama_obat").val(nama_obat);
			$("#dpenerimaan_obat_cito_nama_jenis_obat").val(nama_jenis_obat);
			$("#dpenerimaan_obat_cito_jumlah").val(jumlah);
			var html_option = "<option value='" + json.konversi + "_" + json.satuan_konversi + "'>" + json.satuan + "</option>";
			if (json.konversi > 1)
				html_option += "<option value='1_" + json.satuan_konversi + "'>" + json.satuan_konversi + "</option>";
			$("#dpenerimaan_obat_cito_satuan").html(html_option);
			$("#dpenerimaan_obat_cito_satuan").val(konversi + "_" + satuan_konversi);
			$("#dpenerimaan_obat_cito_konversi").val(konversi);
			$("#dpenerimaan_obat_cito_satuan_konversi").val(satuan_konversi);
			$("#dpenerimaan_obat_cito_hpp").val(hpp);
			$("#dpenerimaan_obat_cito_hna").val(hna);
			$("#dpenerimaan_obat_cito_produsen").val(produsen);
			$("#dpenerimaan_obat_cito_tanggal_exp").val(tanggal_exp);
			$("#dpenerimaan_obat_cito_no_batch").val(no_batch);
		}
	);
};
DPenerimaanObatCitoAction.prototype.del = function(r_num) {
	var id = $("tr#data_" + r_num + " td#id").text();
	if (id.length == 0) {
		$("#data_" + r_num).remove();
	} else {
		$("#data_" + r_num).attr("style", "display: none;");
		$("#data_" + r_num).attr("class", "deleted");
	}
	penerimaan_obat.update_total();
};
DPenerimaanObatCitoAction.prototype.cancel = function() {
	this.clear();
	this.setEditMode("true");
};