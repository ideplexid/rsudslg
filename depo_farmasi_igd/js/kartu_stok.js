var obat_ks;
var kartu_stok;
var FINISHED;
$(document).ready(function() {
	$('[data-toggle="popover"]').popover({
		trigger: 'hover',
		'placement': 'top'
	});
	$(".mydate").datepicker();
	obat_ks = new ObatKSAction(
		"obat_ks",
		"depo_farmasi_igd",
		"kartu_stok",
		new Array()
	);
	obat_ks.setSuperCommand("obat_ks");
	kartu_stok = new KartuStokAction(
		"kartu_stok",
		"depo_farmasi_igd",
		"kartu_stok",
		new Array()
	);
	
	$("#loading_modal").on("show", function() {
		$("a.close").hide();
	});
	$("#loading_modal").on("hide", function() {
		$("a.close").show();
	});
	$("tbody#kartu_stok_list").append(
		"<tr>" +
			"<td colspan='7'><strong><center><small>DATA KARTU STOK BELUM DIPROSES</small></center></strong></td>" +
		"</tr>"
	);
	$(document).keyup(function(e) {
		if (e.which == 27) {
			FINISHED = true;
		}
	})
});