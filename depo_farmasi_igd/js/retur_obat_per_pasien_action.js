function ReturObatPerPasienAction(name, page, action, column) {
    this.initialize(name, page, action, column);
}
ReturObatPerPasienAction.prototype.constructor = ReturObatPerPasienAction;
ReturObatPerPasienAction.prototype = new TableAction();
ReturObatPerPasienAction.prototype.search_obat = function() {
    var criteria = $("#search_nama_obat").val();
    var nor = $("#detail_retur_obat_per_pasien_list tr").length;
    if (nor > 0) {
        if (criteria.trim() != "") {
            for (var i = 0; i < nor; i++) {
                var nama_obat = $("#detail_retur_obat_per_pasien_list tr:eq(" + i + ") td#nama_obat").text();
                if (nama_obat.toLowerCase().indexOf(criteria.toLowerCase()) >= 0) {
                    $("#detail_retur_obat_per_pasien_list tr:eq(" + i + ")").show();
                } else {
                    $("#detail_retur_obat_per_pasien_list tr:eq(" + i + ")").hide();
                }
            }
        } else {
            for (var i = 0; i < nor; i++) {
                $("#detail_retur_obat_per_pasien_list tr:eq(" + i + ")").show();
            }
        }
    }
};
ReturObatPerPasienAction.prototype.clear = function() {
    $("#retur_obat_per_pasien_id").val("");
    $("#retur_obat_per_pasien_tanggal").val("");
    $("#retur_obat_per_pasien_noreg_pasien").val("");
    $("#retur_obat_per_pasien_nrm_pasien").val("");
    $("#retur_obat_per_pasien_nama_pasien").val("");
    $("#retur_obat_per_pasien_alamat_pasien").val("");
    $("#retur_obat_per_pasien_dokter").val("");
    $("#detail_retur_obat_per_pasien_list").html("<tr><td colspan='8'><small><center>Pemilihan Pasien Belum Dilakukan</center></small></td></tr>");
    $("#search_panel").hide();
};
ReturObatPerPasienAction.prototype.show_add_form = function() {
    this.clear();
    $("#retur_obat_per_pasien_modal").smodal("show");
};
ReturObatPerPasienAction.prototype.validate = function() {
    var noreg_pasien = $("#retur_obat_per_pasien_noreg_pasien").val();
    var jumlah_detail_retur = 0;
    var nor = $("#detail_retur_obat_per_pasien_list tr").length;
    for (var i = 0; i < nor; i++) {
        var jumlah_retur = parseFloat($("#detail_retur_obat_per_pasien_list tr:eq(" + i + ") td#jumlah_retur").text());
        if (jumlah_retur > 0)
            jumlah_detail_retur++;
    }

    var valid = true;
    $(".error_field").removeClass("error_field");
    var invalid_msg = "";

    if (noreg_pasien == "") {
        valid = false;
        invalid_msg += "</br><strong>Pasien</strong> tidak boleh kosong";
        $("#retur_obat_per_pasien_noreg_pasien").addClass("error_field");
        $("#retur_obat_per_pasien_noreg_pasien").focus();
    }
    if (jumlah_detail_retur == 0) {
        valid = false;
        invalid_msg += "Transaksi tidak dapat disimpan karena tidak ada obat yang diretur.";
    } 

    if (!valid) {
        $("#modal_alert_retur_obat_per_pasien").html(
            "<div class='alert alert-block alert-danger'>" +
            "<h4>Peringatan</h4>" +
            invalid_msg +
            "</div>"
        );
    }
    return valid;
};
ReturObatPerPasienAction.prototype.save = function () {
    if ($(".btn").attr("disabled") == "disabled")
        return;
    $(".btn").removeAttr("disabled");
    $(".btn").attr("disabled", "disabled");
    if (!this.validate()) {
        $(".btn").removeAttr("disabled");
        return;
    }
    showLoading();
    let self = this;
    $("#retur_obat_per_pasien_modal").smodal("hide");

    var cb_data = this.getRegulerData();
    cb_data['command'] = "check_closing_bill";
    cb_data['noreg_pasien'] = $("#retur_obat_per_pasien_noreg_pasien").val();
    $.post(
        "",
        cb_data,
        function (response_cb) {
            var json_cb = getContent(response_cb);
            if (json_cb == null) {
                dismissLoading();
                $("#retur_obat_per_pasien_modal").smodal("show");
                $(".btn").removeAttr("disabled");
                return;
            }
            if (json_cb == "0") {
                var data = self.getRegulerData();
                data['command'] = "save";
                data['id'] = $("#retur_obat_per_pasien_id").val();
                data['noreg_pasien'] = $("#retur_obat_per_pasien_noreg_pasien").val();
                data['nrm_pasien'] = $("#retur_obat_per_pasien_nrm_pasien").val();
                data['nama_pasien'] = $("#retur_obat_per_pasien_nama_pasien").val();
                data['alamat_pasien'] = $("#retur_obat_per_pasien_alamat_pasien").val();
                var detail = {};
                var nor = $("#detail_retur_obat_per_pasien_list tr").length;
                var j = 0;
                for (var i = 0; i < nor; i++) {
                    var jumlah_retur = parseFloat($("#detail_retur_obat_per_pasien_list tr:eq(" + i + ") td#jumlah_retur").text());
                    if (jumlah_retur > 0) {
                        var id_penjualan_resep = $("#detail_retur_obat_per_pasien_list tr:eq(" + i + ") td#id_penjualan_resep").text();
                        var id_stok_obat = $("#detail_retur_obat_per_pasien_list tr:eq(" + i + ") td#id_stok_obat").text();
                        var id_stok_pakai_obat_jadi = $("#detail_retur_obat_per_pasien_list tr:eq(" + i + ") td#id_stok_pakai_obat_jadi").text();
                        var jumlah_retur = $("#detail_retur_obat_per_pasien_list tr:eq(" + i + ") td#jumlah_retur").text();
                        var harga_jual = $("#detail_retur_obat_per_pasien_list tr:eq(" + i + ") td#harga_jual").text();
                        var d_data = {};
                        d_data['id_penjualan_resep'] = id_penjualan_resep;
                        d_data['id_stok_obat'] = id_stok_obat;
                        d_data['id_stok_pakai_obat_jadi'] = id_stok_pakai_obat_jadi;
                        d_data['jumlah_retur'] = jumlah_retur;
                        d_data['harga_jual'] = harga_jual;
                        detail[j] = d_data;
                        j++;
                    }
                }
                data['detail'] = detail;
                $.post(
                    "",
                    data,
                    function (response) {
                        var json = getContent(response);
                        if (json == null) {
                            $("#retur_obat_per_pasien_modal").smodal("show");
                        } else {
                            self.view();
                        }
                        dismissLoading();
                        $(".btn").removeAttr("disabled");
                    }
                );
            } else {
                dismissLoading();
                $(".btn").removeAttr("disabled");
                $("#retur_obat_per_pasien_modal").smodal("show");
            }
        }
    );
};
ReturObatPerPasienAction.prototype.detail = function (id) {
    if ($(".btn").attr("disabled") == "disabled")
        return;
    $(".btn").removeAttr("disabled");
    $(".btn").attr("disabled", "disabled");
    var data = this.getRegulerData();
    data['command'] = "edit";
    data['id'] = id;
    $.post(
        "",
        data,
        function (response) {
            var json = getContent(response);
            if (json == null) return;
            $("#v_retur_obat_per_pasien_id").val(json.header.id);
            $("#v_retur_obat_per_pasien_tanggal").val(json.header.tanggal);
            $("#v_retur_obat_per_pasien_noreg_pasien").val(json.header.noreg_pasien);
            $("#v_retur_obat_per_pasien_nrm_pasien").val(json.header.nrm_pasien);
            $("#v_retur_obat_per_pasien_nama_pasien").val(json.header.nama_pasien);
            $("#v_retur_obat_per_pasien_alamat").val(json.header.alamat_pasien);
            $("#v_retur_obat_per_pasien_dokter").val(json.header.daftar_dokter);
            $("#v_detail_retur_obat_per_pasien_list").html(json.detail_list);
            $("#v_retur_obat_per_pasien_modal").smodal("show");
            $(".btn").removeAttr("disabled");
        }
    );
};
ReturObatPerPasienAction.prototype.print = function (id) {
    if ($(".btn").attr("disabled") == "disabled")
        return;
    $(".btn").removeAttr("disabled");
    $(".btn").attr("disabled", "disabled");
    var self = this;
    var data = this.getRegulerData();
    data['command'] = "print_retur";
    data['id'] = id;
    $.post(
        "",
        data,
        function (response) {
            var json = getContent(response);
            if (json == null) return;
            self.view();
            smis_print(json);
            $(".btn").removeAttr("disabled");
        }
    );
};
ReturObatPerPasienAction.prototype.cancel = function (id) {
    var self = this;
    bootbox.confirm(
        "Yakin membatalkan Retur Obat Per Pasien ini?",
        function (result) {
            if (result) {
                showLoading();
                var data_t = self.getRegulerData();
                data_t['command'] = "edit";
                data_t['id'] = id;
                $.post(
                    "",
                    data_t,
                    function (response_t) {
                        var json_t = getContent(response_t);
                        if (json_t == null) return;
                        var cb_data = self.getRegulerData();
                        cb_data['command'] = "check_closing_bill";
                        cb_data['noreg_pasien'] = json_t.header.noreg_pasien;
                        $.post(
                            "",
                            cb_data,
                            function (response_cb) {
                                var json_cb = getContent(response_cb);
                                if (json_cb == null) {
                                    dismissLoading();
                                    return;
                                }
                                if (json_cb == "0") {
                                    var data = self.getRegulerData();
                                    data['command'] = "save";
                                    data['id'] = id;
                                    data['dibatalkan'] = "1";
                                    $.post(
                                        "",
                                        data,
                                        function (response) {
                                            var json = getContent(response);
                                            if (json == null) return;
                                            self.view();
                                            dismissLoading();
                                        }
                                    );
                                } else {
                                    bootbox.alert("Tagihan pasien tidak dapat diubah karena tagihan pasien sudah ditutup. Silakan menghubungi pihak Kasir.");
                                    dismissLoading();
                                }
                            }
                        );
                    }
                );
            }
        }
    );
};
