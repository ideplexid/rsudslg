function PenerimaanObatCitoAction(name, page, action, column) {
	this.initialize(name, page, action, column);
}
PenerimaanObatCitoAction.prototype.constructor = PenerimaanObatCitoAction;
PenerimaanObatCitoAction.prototype = new TableAction();
PenerimaanObatCitoAction.prototype.show_add_form = function() {
	var data = this.getRegulerData();
	data['action'] = "penerimaan_obat_cito_form";
	data['super_command'] = "";
	data['editable_header'] = "true";
	data['editable_detail'] = "true";
	LoadSmisPage(data);
};
PenerimaanObatCitoAction.prototype.show_detail_form = function(id) {
	if (id == 0 || id == "")
		return;
	var self = this;
	var data = this.getRegulerData();
	data['command'] = "show_detail";
	data['limited'] = $("#penerimaan_obat_cito_limited").val();
	data['id'] = id;
	$.post(
		"",
		data,
		function(response) {
			var json = JSON.parse(response);
			if (json == null) return;
			$("#dpenerimaan_obat_cito_list").html(json.html);
			self.update_total();
			row_num = json.row_num;
		}
	);
};
PenerimaanObatCitoAction.prototype.get_footer = function() {
	if ($("#total").length != 0)
		return;
	var self = this;
	var data = this.getRegulerData();
	data['command'] = "show_footer";
	$.post(
		"",
		data,
		function(response) {
			var json = JSON.parse(response);
			if (json == null) return;
			$("tbody#dpenerimaan_obat_cito_list").after(json.html);
		}
	);
};
PenerimaanObatCitoAction.prototype.update_total = function() {
	var num_rows = $("tbody#dpenerimaan_obat_cito_list").children("tr").length;
	var nomor = 1;
	var total = 0;
	for (var i = 0; i < num_rows; i++) {
		var deleted = $("tbody#dpenerimaan_obat_cito_list tr:eq(" + i + ")").attr("class") == "deleted" ? true : false;
		if (!deleted) {
			var subtotal = parseFloat($("tbody#dpenerimaan_obat_cito_list tr:eq(" + i + ") td#subtotal").text());
			total += subtotal;
			$("tbody#dpenerimaan_obat_cito_list tr:eq(" + i + ") td:eq(0)").html("<small>" + nomor + "</small>");
			nomor++;
		}
	}
	var total_1 = total;
	var ppn = Math.floor(total_1 * 0.1);
	var total_2 = total_1 + ppn;
	$("#total_2").html("<small><strong><div align='right'>" + total_1.formatMoney("2", ".", ",") + "</div></strong></small>");
	$("#ppn").html("<small><strong><div align='right'>" + ppn.formatMoney("2", ".", ",") + "</div></strong></small>");
	$("#tagihan").html("<small><strong><div align='right'>" + total_2.formatMoney("2", ".", ",") + "</div></strong></small>");
};
PenerimaanObatCitoAction.prototype.getSaveData = function() {
	var data = this.getRegulerData();
	data['command'] = "save";
	data['id'] = $("#penerimaan_obat_cito_id").val();
	data['f_id'] = $("#penerimaan_obat_cito_f_id").val();
	data['id_vendor'] = $("#penerimaan_obat_cito_id_vendor").val();
	data['nama_vendor'] = $("#penerimaan_obat_cito_nama_vendor").val();
	data['no_kwitansi'] = $("#penerimaan_obat_cito_no_kwitansi").val();
	data['tanggal'] = $("#penerimaan_obat_cito_tanggal").val();
	data['keterangan'] = $("#penerimaan_obat_cito_keterangan").val();
	data['total'] = parseFloat($("#tagihan").text().replace(/[^0-9-,]/g, '').replace(",", "."));
	var detail = {};
	var nor = $("tbody#dpenerimaan_obat_cito_list").children("tr").length;
	for(var i = 0; i < nor; i++) {
		var d_data = {};
		d_data['id'] = $("tbody#dpenerimaan_obat_cito_list tr:eq(" + i + ") td#id").text();
		d_data['id_obat'] = $("tbody#dpenerimaan_obat_cito_list tr:eq(" + i + ") td#id_obat").text();
		d_data['kode_obat'] = $("tbody#dpenerimaan_obat_cito_list tr:eq(" + i + ") td#kode_obat").text();
		d_data['nama_obat'] = $("tbody#dpenerimaan_obat_cito_list tr:eq(" + i + ") td#nama_obat").text();
		d_data['nama_jenis_obat'] = $("tbody#dpenerimaan_obat_cito_list tr:eq(" + i + ") td#nama_jenis_obat").text();
		d_data['jumlah'] = $("tbody#dpenerimaan_obat_cito_list tr:eq(" + i + ") td#jumlah").text();
		d_data['satuan'] = $("tbody#dpenerimaan_obat_cito_list tr:eq(" + i + ") td#satuan").text();
		d_data['konversi'] = $("tbody#dpenerimaan_obat_cito_list tr:eq(" + i + ") td#konversi").text();
		d_data['satuan_konversi'] = $("tbody#dpenerimaan_obat_cito_list tr:eq(" + i + ") td#satuan_konversi").text();
		d_data['hna'] = $("tbody#dpenerimaan_obat_cito_list tr:eq(" + i + ") td#hna").text();
		d_data['produsen'] = $("tbody#dpenerimaan_obat_cito_list tr:eq(" + i + ") td#produsen").text();
		d_data['tanggal_exp'] = $("tbody#dpenerimaan_obat_cito_list tr:eq(" + i + ") td#tanggal_exp").text();
		d_data['no_batch'] = $("tbody#dpenerimaan_obat_cito_list tr:eq(" + i + ") td#no_batch").text();
		detail[i] = d_data;
	}
	data['detail'] = detail;
	return data;
};
PenerimaanObatCitoAction.prototype.validate = function() {
	var valid = true;
	var invalid_msg = "";
	var id_vendor = $("#penerimaan_obat_cito_id_vendor").val();
	var no_kwitansi = $("#penerimaan_obat_cito_no_kwitansi").val();
	var tanggal = $("#penerimaan_obat_cito_tanggal").val();
	var nord = $("tbody#dpenerimaan_obat_cito_list").children().length;
	var total_cr = 0;
	for (var i = 0; i < nord; i++) {
		var jumlah = parseFloat($("#dpenerimaan_obat_cito_list tr:eq(" + i + ") td#jumlah").text());
		if (jumlah > 0)
			total_cr++;
	}
	$(".error_field").removeClass("error_field");
	if (id_vendor == "") {
		valid = false;
		invalid_msg += "</br><strong>Rekanan</strong> tidak boleh kosong";
		$("#penerimaan_obat_cito_nama_vendor").addClass("error_field");
	}
	if (no_kwitansi == "") {
		valid = false;
		invalid_msg += "</br><strong>No. Kwitansi</strong> tidak boleh kosong";
		$("#penerimaan_obat_cito_no_kwitansi").addClass("error_field");
	}
	if (tanggal == "") {
		valid = false;
		invalid_msg += "</br><strong>Tanggal</strong> tidak boleh kosong";
		$("#penerimaan_obat_cito_tanggal").addClass("error_field");
	}
	
	if (total_cr == 0) {
		valid = false;
		invalid_msg += "</br>Tidak terdapat <strong>detil penerimaan obat</strong> dengan info jumlah diterima > 0";
	}
	if (!valid)
		bootbox.alert(invalid_msg);
	return valid;
};
PenerimaanObatCitoAction.prototype.save = function() {
	if (!this.validate())
		return;
	var self = this;
	var data = this.getSaveData();
	bootbox.dialog({
		message : "Yakin melakukan penyimpanan BBM ini?",
		title	: "Konfirmasi",
		buttons	: {
			success : {
				label 		: "Lanjutkan",
				className 	: "btn-success",
				callback	: function() {
					showLoading();
					$.post(
						"",
						data,
						function() {
							dismissLoading();
							self.back();
						}
					);
				}
			},
			danger 	: {
				label 		: "Batal",
				className	: "btn-inverse"
			}
		}
	});
};
PenerimaanObatCitoAction.prototype.detail = function(id) {
	var self = this;
	var data = this.getRegulerData();
	data['command'] = "edit";
	data['id'] = id;
	$.post(
		"",
		data,
		function(response) {
			var json = getContent(response);
			if (json == null) 
				return;
			data = self.getRegulerData();
			data['action'] = "penerimaan_obat_cito_form";
			data['super_command'] = "";
			data['id'] = json.id;
			data['f_id'] = json.f_id;
			data['no_kwitansi'] = json.no_kwitansi;
			data['id_vendor'] = json.id_vendor;
			data['nama_vendor'] = json.nama_vendor;
			data['tanggal'] = json.tanggal;
			data['keterangan'] = json.keterangan;
			data['total'] = json.total;
			data['editable_header'] = "false";
			data['editable_detail'] = "false";
			data['limited'] = "false";

			LoadSmisPage(data);
		}
	);
};
PenerimaanObatCitoAction.prototype.back = function() {
	var data = this.getRegulerData();
	data['action'] = "penerimaan_obat_cito";
	data['super_command'] = "";
	data['kriteria'] = "";
	data['max'] = 5;
	data['number'] = 0;
	LoadSmisPage(data);
};