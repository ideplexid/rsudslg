var pengaturan_margin;
var obat;
$(document).ready(function() {
	obat = new ObatAction(
		"obat",
		"depo_farmasi_igd",
		"pengaturan_margin",
		new Array()
	);
	obat.setSuperCommand("obat");
	pengaturan_margin = new PengaturanMarginAction(
		"pengaturan_margin",
		"depo_farmasi_igd",
		"pengaturan_margin",
		new Array("id", "id_obat", "kode_obat", "nama_obat", "nama_jenis_obat", "margin_jual", "hja", "hpp_max")
	);
	pengaturan_margin.view();

	$("#pengaturan_margin_hja").on("keyup", function(e) {
		var v_hpp = $("#pengaturan_margin_hpp_max").val().replace(/[^0-9-,]/g, '').replace(",", ".");
		var v_hja = $("#pengaturan_margin_hja").val().replace(/[^0-9-,]/g, '').replace(",", ".");
		var v_margin = 0;
		if (v_hpp > 0)
			v_margin = (v_hja - v_hpp) / v_hpp * 100;
		$("#pengaturan_margin_margin_jual").val(v_margin);
	});

	$("#pengaturan_margin_margin_jual").on("keyup", function(e) {
		var v_hpp = $("#pengaturan_margin_hpp_max").val().replace(/[^0-9-,]/g, '').replace(",", ".");
		var v_margin = $("#pengaturan_margin_margin_jual").val();
		var v_hja = parseFloat(v_hpp) + parseFloat(v_margin * v_hpp / 100);
		$("#pengaturan_margin_hja").val(parseFloat(v_hja).formatMoney("2", ".", ","));
	});
});