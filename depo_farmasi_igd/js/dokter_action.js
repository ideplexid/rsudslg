function DokterAction(name, page, action, column) {
	this.initialize(name, page, action, column);
}
DokterAction.prototype.constructor = DokterAction;
DokterAction.prototype = new TableAction();
DokterAction.prototype.chooser = function(modal, elm_id, param,action, modal_title) {
	if ($(".btn").attr("disabled") == "disabled")
		return;
	$(".btn").removeAttr("disabled");
	$(".btn").attr("disabled", "disabled");
	var data = this.getRegulerData();
	var dt = this.addChooserData(data);
	var self = this;
	data['super_command'] = param;
	$.post(
		'',
		dt,
		function(res) {
			show_chooser(self, param, res, action.getShowParentModalInChooser(), modal_title);
			action.view();
			action.focusSearch();
			this.current_chooser=action;
			CURRENT_SMIS_CHOOSER=action;
			$(".btn").removeAttr("disabled");
		}
	);
};
DokterAction.prototype.selected = function(json) {
	$("#resep_id_dokter").val(json.id);
	$("#resep_nama_dokter").val(json.nama);
	$("#resep_name_dokter").val(json.nama);
	$("#resep_diskon").focus();
};