<?php
	require_once("smis-base/smis-include-service-consumer.php");
	global $db;

	$table = new Table(
		array("No.", "ID Obat", "Kode Obat", "Nama Obat", "Jenis Obat"),
		"Depo Depo Farmasi IGD : Daftar Narkotika",
		true,
		null
	);
	$table->setName("daftar_narkotika");
	$table->setAction(false);
	$adapter = new SimpleAdapter(true, "No.");
	$adapter->add("ID Obat", "id");
	$adapter->add("Kode Obat", "kode");
	$adapter->add("Nama Obat", "nama");
	$adapter->add("Jenis Obat", "nama_jenis_barang");
	$service_responder = new ServiceResponder(
		$db,
		$table,
		$adapter,
		"daftar_narkotika"
	);
	$super_command = new SuperCommand();
	$super_command->addResponder("daftar_narkotika", $service_responder);
	$init = $super_command->initialize();
	if ($init != null) {
		echo $init;
		return;
	}

	echo $table->getHtml();
	echo addJS("framework/smis/js/table_action.js");	
?>
<script type="text/javascript">
	var daftar_narkotika;
	$(document).ready(function() {
		daftar_narkotika = new TableAction(
			"daftar_narkotika",
			"depo_farmasi_igd",
			"daftar_narkotika",
			new Array()
		);
		daftar_narkotika.setSuperCommand("daftar_narkotika");
		daftar_narkotika.view();
	});
</script>