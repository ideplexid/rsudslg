<?php
	require_once("depo_farmasi_igd/library/InventoryLibrary.php");
	global $db;

	$table = new Table(
		array("ID", "Batas Bawah (Rp)", "Batas Atas (Rp)", "Margin Jual (%)"),
		"Depo Depo Farmasi IGD : Pengaturan Margin Jual Per Rentang Harga"
	);
	$table->setName("pengaturan_margin_obat_rentang_harga");

	if (isset($_POST['command'])) {
		$adapter = new SimpleAdapter();
		$adapter->add("ID", "id");
		$adapter->add("Batas Bawah (Rp)", "batas_bawah", "money");
		$adapter->add("Batas Atas (Rp)", "batas_atas", "money");
		$adapter->add("Margin Jual (%)", "margin_jual", "money");

		$dbtable = new DBTable(
			$db,
			InventoryLibrary::$_TBL_MARGIN_JUAL_RENTANG_HARGA
		);

		$dbresponder = new DBResponder(
			$dbtable,
			$table,
			$adapter
		);
		$data = $dbresponder->command($_POST['command']);
		echo json_encode($data);
		return;
	}

	$modal = new Modal("pengaturan_margin_obat_rentang_harga_add_form", "smis_form_container", "pengaturan_margin_obat_rentang_harga");
	$modal->setTitle("Data Pengaturan Margin");
	$id_hidden = new Hidden("pengaturan_margin_obat_rentang_harga_id", "pengaturan_margin_obat_rentang_harga_id", "");
	$modal->addElement("", $id_hidden);
	$batas_bawah_text = new Text("pengaturan_margin_obat_rentang_harga_batas_bawah", "pengaturan_margin_obat_rentang_harga_batas_bawah", "");
	$batas_bawah_text->setAtribute(" data-thousands=\".\" data-decimal=\",\"  data-precision=\"2\"  " );
	$batas_bawah_text->setTypical("money");
	$modal->addElement("Batas Bawah", $batas_bawah_text);
	$batas_atas_text = new Text("pengaturan_margin_obat_rentang_harga_batas_atas", "pengaturan_margin_obat_rentang_harga_batas_atas", "");
	$batas_atas_text->setAtribute(" data-thousands=\".\" data-decimal=\",\"  data-precision=\"2\"  " );
	$batas_atas_text->setTypical("money");
	$modal->addElement("Batas Atas", $batas_atas_text);
	$margin_jual_text = new Text("pengaturan_margin_obat_rentang_harga_margin_jual", "pengaturan_margin_obat_rentang_harga_batas_margin_jual", "");
	$margin_jual_text->setAtribute(" data-thousands=\".\" data-decimal=\",\"  data-precision=\"2\"  " );
	$margin_jual_text->setTypical("money");
	$modal->addElement("Margin Jual", $margin_jual_text);
	$save_button = new Button("", "", "Simpan");
	$save_button->setClass("btn-success");
	$save_button->setAction("pengaturan_margin_obat_rentang_harga.save()");
	$save_button->setIcon("fa fa-floppy-o");
	$save_button->setIsButton(Button::$ICONIC);
	$modal->addFooter($save_button);

	echo $modal->getHtml();
	echo $table->getHtml();
	echo addJS("framework/smis/js/table_action.js");
?>
<script type="text/javascript">
	var pengaturan_margin_obat_rentang_harga;

	$(document).ready(function() {
		var columns = new Array("id", "batas_bawah", "batas_atas", "margin_jual");
		pengaturan_margin_obat_rentang_harga = new TableAction(
			"pengaturan_margin_obat_rentang_harga",
			"depo_farmasi_igd",
			"pengaturan_margin_obat_rentang_harga",
			columns
		);
		pengaturan_margin_obat_rentang_harga.view();
	});
</script>