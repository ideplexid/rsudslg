<?php 
require_once("smis-framework/smis/template/NavigatorTemplate.php");
class SmisPrototypeNavigator extends NavigatorTemplate{
	public function __construct($navigation, $tooltip, $name, $page){
		parent::__construct($navigation, $tooltip, $name, $page);
	}
	
	public function adminMenu(){
		
	}
	
	public function topMenu(){
		if(strpos($this->menus,'maps')!==false || strpos($this->menus,'all')!==false){
			$this->addMenu("Maps of ".$this->name, "Maps", "maps",null,"smis_action","fa fa-map");
		}
	}
}

?>