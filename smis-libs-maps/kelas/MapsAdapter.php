<?php 


class MapsAdapter extends SimpleAdapter{
	public function adapt($d){
		$l=parent::adapt($d);
		$l['id']=$d->id;

		$l['Nama']=$d->nama;
		$l['Alamat']=$d->alamat;
		$l['Tipe']=$d->tipe;
		$l['Keterangan']=$d->keterangan;
		$l['Kode']=$d->code;


		if($d->tipe=="point") $l['Property']=$d->icon;
		else $l['Property']=$d->color;

		$gps=$d->gps;
		$list_gps=array();
		$latlng=explode(" | ",$gps);
		foreach($latlng as $ll){
			if($ll=="")
				continue;
			$pos=explode(" # ",$ll);
			$lat=$pos[0];
			$lng=$pos[1];
			$list_gps[]=array("lat"=>$lat,"lng"=>$lng);
		}
		$l['maps']=array(
				"gps"=>$list_gps,
				"id"=>$d->id,
				"nama"=>$d->nama,
				"alamat"=>$d->alamat,
				"tipe"=>$d->tipe,
				"icon"=>$d->icon,
				"color"=>$d->color,
				"ket"=>$d->keterangan
		);

		return $l;
	}
}

?>