<?php 

class MapsTable extends Table{
	private $_point_enable=true;
	private $_area_enable=true;
	private $_line_enable=true;
	
	public function setPointEnabled($enabled){
		$this->_point_enable=$enabled;
	}
	
	public function setAreaEnabled($enabled){
		$this->_area_enable=$enabled;
	}
	
	public function setLineEnabled($enabled){
		$this->_line_enable=$enabled;
	}
	
	public function getHeaderButton(){
		if($this->model==self::$EDIT){
			$btn_group_1=new ButtonGroup("noprint ".$this->name."_point_area");
			if($this->_point_enable) $btn_group_1->add($this->name."_point",$this->name."_point","Point",$this->action.".add('point')", "btn-primary", "data-content='New Point' data-toggle='popover'");
			if($this->_area_enable) $btn_group_1->add($this->name."_area",$this->name."_area","Area",$this->action.".add('area')", "btn-primary", "data-content='New Area' data-toggle='popover'");
			if($this->_line_enable) $btn_group_1->add($this->name."_line",$this->name."_line","Line",$this->action.".add('line')", "btn-primary", "data-content='New Line' data-toggle='popover'");

			$btn_group_2=new ButtonGroup("noprint ".$this->name."_ok_cancel hide");
			$btn_group_2->add($this->name."_ok",$this->name."_ok","OK",$this->action.".ok()", "btn-success", "data-content='Add New Area' data-toggle='popover'");
			$btn_group_2->add($this->name."_cancel",$this->name."_cancel","Cancel",$this->action.".cancel()", "btn-danger", "data-content='Add New Area' data-toggle='popover'");
			return $btn_group_1->getHtml()." ".$btn_group_2->getHtml();
		}else{
			return "";
		}
	}
}


?>