<?php 

class MapsResponder extends DBResponder{
	public function view(){
		$kriteria=isset($_POST['kriteria'])?$_POST['kriteria']:"";
		$number=(isset($_POST['number']) && $_POST['number']!="")?$_POST['number']:0;
		$max=isset($_POST['max'])?$_POST['max']:"10";
		$this->dbtable->setMaximum($max);

		$d=$this->dbtable->view($kriteria,$number);
		$page=$d['page'];
		$data=$d['data'];
		$max_page=$d['max_page'];

		$uidata=$this->adapter->getContent($data);
		$this->uitable->setContent($uidata);
		$list=$this->uitable->getBodyContent();
		$pagination=$this->uitable->getPagination($page,5,$max_page);

		$maps=array();
		foreach($uidata as $data){
			$maps[]=$data['maps'];
		}

		$json['list']=$list;
		if($this->debug) $json['dbtable']=$d;
		$json['pagination']=$pagination->getHtml();
		$json['number']=$page;
		$json['number_p']=$number;
		$json['maps']=$maps;
		return $json;
	}
}


?>