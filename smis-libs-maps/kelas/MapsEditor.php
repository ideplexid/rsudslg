<?php 

/**
 *
 * this plugins depends on Google Maps function
 * using MapsTableAction as based of the javascript action
 *
 *
 * @version 1.0.1
 * @copyright Nurul Huda <goblooge@gmail.com>
 * @since 14 Mei 2014
 * @author goblooge
 *
 */


require_once("smis-framework/smis/template/ModulTemplate.php");
require_once("smis-libs-class/MasterTemplate.php");


class MapsEditor extends MasterTemplate{

	protected $poliname;
	protected $polislug;
	protected $uitable;
	protected $dbres;
	protected $dbtable;
	protected $modal;
	protected $lat;
	protected $lon;
	protected $zoom;
	protected $code_name;
	protected $code_type;
	protected $code_value;
	protected $editable;
	protected $action;
	protected $dbtable_name;
	protected $db;
	protected $adapter;
	protected $uitable_modal;
	protected $custom_column;
	protected $icon_path;
	protected $icon_enabled;
	public function __construct($db,$table_name,$polislug,$poliname,$action="maps"){
		parent::__construct($db,"smis_gm_maps_".$table_name,$polislug,$action);
		$this->setTableName("smis_gm_maps_".$table_name);
		$this->polislug=$polislug;
		$this->poliname=$poliname;
		$this->db=$db;
		$this->action=$action;
		$this->uitable=new MapsTable(array("Nama",'Alamat',"Keterangan",'Tipe','Property',"Kode"), "Maps ".$poliname, NULL, true);
		$this->uitable->setName("maps");
		$this->lat="-7.314114";
		$this->lon="112.687057";
		$this->zoom="15";
		$this->code_name="";
		$this->code_type="hidden";
		$this->code_value="";
		$this->editable=true;
		$this->adapter;
		$this->uitable_modal=array();
		$this->custom_column=array();
		$this->icon_path="smis-icons/";
		$this->icon_enabled=true;
		
	}
	
	public function setIconEnable($enabled){
		$this->icon_enabled=$enabled;
	}
	
	public function setIconPath($path){
		$this->icon_path=$path;
	}

	public function setHeader($header){
		$this->uitable->setHeader($header);
	}

	public function setTableName($tblname){
		$this->dbtable_name=$tblname;
	}
	
	
	
	public function setAdapter($adapter){
		$this->adapter=$adapter;
	}
	
	public function getAdapter(){
		if($this->adapter==null)
			$this->adapter=new MapsAdapter();
		return $this->adapter;
	}

	public function setMapProperty($lat,$lon,$zoom){
		$this->lat=$lat;
		$this->lon=$lon;
		$this->zoom=$zoom;
	}

	public function setCodeProperty($name,$tipe,$value){
		$this->code_name=$name;
		$this->code_type=$tipe;
		$this->code_value=$value;
	}

	public function setEditable($editable){
		$this->editable=$editable;
	}

	public function initialize(){
		
		if(isset($_POST['super_command']) && $_POST['super_command']!=""){
			$this->superCommand($_POST['super_command']);
		}else if(isset($_POST['command'])){
			if($_POST['command']=='maps_click'){
				echo json_encode($this->MapsClick());
			}else{
				$this->command($_POST['command']);
			}
		}else{
			parent::initialize();
		}
	}

	public function MapsClick(){
		$pack=new ResponsePackage();
		$pack->setStatus(ResponsePackage::$STATUS_OK);
		$pack->setWarning(true,"Detail Proyek","Override Maps Click in MapsEditor");
		return $pack->getPackage();
	}

	public function command($command){
		$this->dbres=new MapsResponder($this->getDBtable(), $this->getUItable(), $this->getAdapter());
		$data=$this->dbres->command($_POST['command']);
		echo json_encode($data);
	}

	public function select($id){
		$this->dbtable=new DBTable($this->db, $this->dbtable_name);
		$data=$this->dbtable->select($id,false);
		return $data;
	}

	
	public function addModal($column,$type,$name,$default_value,$empty='y',$typical=null,$disable=false,$option=null){
		$array=array();
		$array['column']=$column;
		$array['type']=$type;
		$array['name']=$name;
		$array['default']=$default_value;
		$array['empty']=$empty;
		$array['typical']=$typical;
		$array['disable']=$disabled;
		$array['option']=$option;
		$this->custom_column[]=$column;
		$this->uitable_modal[]=$array;
	}
	
	private final function modalPreload(){
		foreach($this->uitable_modal as $array){
			$column=$array['column'];
			$type=$array['type'];
			$name=$array['name'];
			$default_value=$array['default'];
			$empty=$array['empty'];
			$typical=$array['typical'];
			$disabled=$array['disable'];
			$option=$array['option'];
			$this->getUItable()->addModal($column,$type,$name,$default_value,$empty,$typical,$disable,$option);
		}
	}
	
		

	/*when it's star build*/
	public function phpPreload(){
		$select_icon=array();
		foreach (new DirectoryIterator($this->icon_path) as $file) {
			if($file->isDot()) continue;
			$data['name']=$file->getFilename();
			$data['value']=$this->icon_path.$file->getFilename();
			$select_icon[]=$data;
		}

		/*This is Modal Form and used for add and edit the table*/
		$header="<div class=\"maps well\" id='google_maps_".$this->polislug."'></div>";
		$header.='<input id="pac-input" class="controls" type="text" placeholder="Search Box">';
		
		$this->getUItable()->addModal("id", "hidden", "", "");
		$this->getUItable()->addModal("tipe", "hidden", "", "");
		$this->getUItable()->addModal("gps", "hidden", "", "");
		$this->getUItable()->addModal("nama", "text", "Nama", "");
		$this->getUItable()->addModal("alamat", "text", "Alamat", "");
		if($this->icon_enabled)
			$this->getUItable()->addModal("icon", "select", "Icon", $select_icon);
		$this->getUItable()->addModal("color", "color", "Warna", "");
		$this->getUItable()->addModal("code", $this->code_type, $this->code_name, $this->code_value);
		$this->modalPreload();
		$this->getUItable()->addModal("keterangan", "text", "Keterangan", "");
		
		if($this->is_truncate){
			$truncate= new Button ( "", "", "" );
			$truncate->setAction ( $this->action.".tuncate_db()" );
			$truncate->setIsButton ( Button::$ICONIC );
			$truncate->setIcon("fa fa-trash");
			$truncate->setClass("btn-primary");
			$this->getUItable()->addFooterButton($truncate);
		}
		$this->getModal()->setTitle($this->modal_title);
		if($this->editable){
			$this->uitable->addHeader("before","<tr><td colspan='10'>".$header."</div></td></tr>");
			if($this->always_visible_form){
				$form=$this->getModal()->joinFooterAndForm();
				echo $form->getHtml();
				echo $this->getUItable()->getHtml ();
			}else{
				echo $this->getUItable()->getHtml ();
				echo $this->getModal()->getHtml();
			}
		}else{
			echo $header;
			echo "<div style='display:none;'>".$this->uitable->getHtml()."</div>";
		}
		
		
		
		echo addJS("framework/smis/js/table_action.js");
		echo addJS("framework/smis/js/maps_editor.js");
		echo addJS("framework/bootstrap/js/bootstrap-colorpicker.js");
		echo addJS("framework/bootstrap/js/bootstrap-select.js");
		
		if($this->is_use_date){
			echo addJS ( "framework/bootstrap/js/bootstrap-datepicker.js" );
			echo addCSS ( "framework/bootstrap/css/datepicker.css" );
			?>
				<script type="text/javascript">
				$(".mydate").datepicker();
				</script>			
			<?php
		}
		
		if($this->is_use_datetime){
			echo addJS ( "framework/bootstrap/js/bootstrap-datetimepicker.js" );
			echo addCSS ( "framework/bootstrap/css/bootstrap-datetimepicker.css" );
			?>
			<script type="text/javascript">
			$('.mydatetime').datetimepicker({ minuteStep: 1});
			</script>			
			<?php
		}
		
		if($this->is_use_time){
			if(!$this->is_use_datetime){
				echo addJS ( "framework/bootstrap/js/bootstrap-datetimepicker.js" );
				echo addCSS ( "framework/bootstrap/css/bootstrap-datetimepicker.css" );
			}
			?>
			<script type="text/javascript">
			$('.mytime').datetimepicker({ minuteStep: 1,startView:'day',maxView:'day'});
			</script>			
			<?php
		}
		
		if($this->is_use_select2){
			echo addJS ( "framework/bootstrap/js/bootstrap-select.js" );
			echo addCSS ( "framework/bootstrap/css/bootstrap-select.css" );
			?>
			<script type="text/javascript">
			$(".select2").select2();
			</script>			
			<?php
		}
	}
	public function jsPreLoad(){
		

		?>
		<script type="text/javascript">
		var <?php echo $this->action; ?>;		
		$(document).ready(function(){
			$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'bottom'});
			$(".mycolor").colorpicker();
			var column=new Array('id','nama','alamat','tipe','gps','icon','color','code','keterangan');
			<?php
				foreach($this->custom_column as $x){
					echo "column.push('".$x."');";
				}
			?>
			<?php echo $this->action; ?>=new MapsAction("<?php echo $this->action; ?>","<?php echo $this->polislug; ?>","<?php echo $this->action; ?>",column,"<?php echo $this->lat; ?>","<?php echo $this->lon; ?>",<?php echo $this->zoom; ?>);
			
			<?php $this->onReady();?>
				<?php if($this->is_truncate){
					echo $this->action;?>.tuncate_db=function(){
						var data=this.getRegulerData();
						data['command']="tuncate_db";
						$.post("",data,function(res){
							<?php echo $this->action; ?>.view();
						});
					}
					<?php 
				}
				?>

				<?php if($this->is_multiple){
					echo $this->action.".setMultipleInput(true);";
					}
				?>

				<?php if($this->is_autofocus_on_multiple!=null){
					echo $this->action.".setFocusOnMultiple('".$this->is_autofocus_on_multiple."');";
					}
				?>
				
				<?php if($this->is_autofocus){
					echo $this->action.".setEnableAutofocus();";
					}
				?>

				<?php if($this->is_next_enter){
					echo $this->action.".setNextEnter();";
					}
				?>
				
				<?php if(count($this->command_viewdata)>0){
					echo $this->action;?>.addViewData=function(view_data){
						<?php foreach($this->command_viewdata as $view){
							echo $view;
						}?>
						return view_data;
					}
					<?php 
				}
				?>

				<?php if(count($this->add_regulerdata)>0){
					echo $this->action;?>.addRegulerData=function(reg_data){
						<?php foreach($this->add_regulerdata as $reg){
							echo $reg;
						}?>
						return reg_data;
					}
					<?php 
				}
				?>

				<?php if(count($this->add_chooserdata)>0){
					echo $this->action;?>.addChooserData=function(c_data){
						<?php foreach($this->add_chooserdata as $c){
							echo $c;
						}?>
						return c_data;
					}
					<?php 
				}
				?>
		});

		

		function format(state) {
		    var originalOption = state.element;
		    return "<img class='smis-option-img' src='"+$(originalOption).attr('value')+"' />" + state.text;
		}
		$("#<?php echo $this->action; ?>_icon").select2({
		    formatResult: format,
		    formatSelection: format,
		    escapeMarkup: function(m) { return m; }
		});
		</script>
		<?php 
		//$this->getSuperCommandJavascript();			
		
	}
	
	public function cssPreLoad(){
		echo addCSS("framework/bootstrap/css/bootstrap-colorpicker.css");
		echo addCSS("framework/bootstrap/css/bootstrap-select.css");
		?>
		<style type="text/css">
			.maps{
				min-height:300px !important;
				margin-bottom:20px;
				padding:10px;
			}
		</style>
		<?php 
	}
	
	public function htmlPreLoad(){}
	
}


?>