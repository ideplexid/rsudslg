<?php 
require_once("smis-framework/smis/template/InstallatorTemplate.php");
class SmisPrototypeInstallator extends InstallatorTemplate{
	
	public function __construct($db,$slug,$code){
		parent::__construct($db,$slug, $code);
	}
	
	public function init($params){
		if(strpos($params,'maps')!==false || strpos($params,'all')!==false){
			$this->addModul($this->getMapsTable());
		}
	}
	
	public function getMapsTable(){
		$query="CREATE TABLE IF NOT EXISTS `smis_gm_maps_".$this->slug."` (
				  `id` int(11) NOT NULL AUTO_INCREMENT,
				  `nama` varchar(32) NOT NULL,
				  `alamat` varchar(128) NOT NULL,
				  `keterangan` text NOT NULL,
				  `kecamatan` varchar(32) NOT NULL,
				  `no_telp` varchar(32) NOT NULL,
				  `email` varchar(64) NOT NULL,
				  `nama_perusahaan` varchar(128) NOT NULL,
				  `alamat_perusahaan` varchar(128) NOT NULL,
				  `tinggi_menara` int(11) NOT NULL,
				  `akhir` date NOT NULL,
				  `status` varchar(32) NOT NULL,
				  `tipe` varchar(8) NOT NULL,
				  `gps` text NOT NULL,
				  `icon` text NOT NULL,
				  `color` varchar(7) NOT NULL,
				  `code` varchar(32) NOT NULL,
				  `prop` varchar(10) NOT NULL,
				  PRIMARY KEY (`id`)
				) ENGINE=InnoDB   ;
			";		
		return $query;
	}
	
	
}

?>