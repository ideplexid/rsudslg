<?php 
function recursive_sum($db,$dbtable,$id_parent,$recursive){
    if($recursive==0){
        return;
    }
    $query      = "SELECT sum(nilai_saldo_awal) as total FROM smis_ac_account WHERE id_parent = '".$id_parent."' AND prop!='del'";
    $total      = $db->get_var($query);
    $dbtable->update(array("nilai_saldo_awal"=>$total,"have_child"=>1),array("id"=>$id_parent));
    if($recursive!=0){
        $parent = $dbtable    ->select(array("id"=>$id_parent));
        if($parent->id_parent!=0){
            $recursive--;
            recursive_sum($db,$dbtable,$parent->id_parent,$recursive);
        }
    }
}