<?php 

function recursive_del($db,DBTable $dbtable,$id_child){
    $child          = $dbtable ->selectEventDel($id_child);
    $id_parent      = $child->id_parent;
    $child_number   = $child->child_number;
    if($id_parent!="0"){
        $query = "SELECT count(*) as total FROM smis_ac_account WHERE id_parent = '$id_parent' AND prop !='del' ";
        $total = $db->get_var($query);
        require_once "accounting/function/recursive_sum.php";
        recursive_sum($db,$dbtable,$id_parent,$child_number);
        if($total==0){
            $update               = array();
            $update['have_child'] = 0;
            $dbtable ->update($update,array("id"=>$id_parent)); 
        }
    }
}