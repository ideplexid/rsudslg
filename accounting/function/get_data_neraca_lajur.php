<?php 

function get_laba_tahun_berjalan($db,$dari,$sampai){
    $query = "SELECT 
                SUM( IF(jenis='transaction',smis_ac_transaksi_detail.debet,0)) AS debit_transaksi,
                SUM( IF(jenis='memorial' OR jenis='',smis_ac_transaksi_detail.debet,0)) AS debit_memorial,
                SUM( IF(jenis='transaction',smis_ac_transaksi_detail.kredit,0)) AS kredit_transaksi,
                SUM( IF(jenis='memorial' OR jenis='',smis_ac_transaksi_detail.kredit,0)) AS kredit_memorial
            FROM smis_ac_transaksi LEFT JOIN smis_ac_transaksi_detail 
                   ON smis_ac_transaksi_detail.id_transaksi = smis_ac_transaksi.id
            WHERE  tanggal >= '".$dari."' 
                   AND tanggal <= '".$sampai."' 
                   AND smis_ac_transaksi_detail.nomor_account>='4' ";
    $x = $db->get_row($query);
	return $x;
}

function get_data_neraca_lajur($db,$dari,$sampai,$filter){
    $akun_laba_tahun_berjalan=getSettings($db,"accounting-akun-khusus-laba-berjalan","NOT SET");
    $qv = "SELECT smis_ac_transaksi.id AS id, 
			smis_ac_transaksi.tanggal AS tanggal, 
			smis_ac_transaksi.jenis AS jenis, 
			smis_ac_transaksi.nomor AS nomor, 
			smis_ac_transaksi.keterangan AS keterangan, 
			smis_ac_transaksi_detail.id AS id_detail,
            if(STRCMP(smis_ac_transaksi_detail.nomor_account,'4')>0 AND tanggal < '".$dari."','".$akun_laba_tahun_berjalan."',smis_ac_transaksi_detail.nomor_account) AS nomor_account,
            
            smis_ac_transaksi_detail.nama_account AS nama_account, 
			smis_ac_transaksi_detail.keterangan AS uraian,
					
			sum(if ( jenis='saldo_awal' OR tanggal < '".$dari." ',smis_ac_transaksi_detail.debet,0))  AS debet_saldo_awal, 
			sum(if ( jenis='saldo_awal' OR tanggal < '".$dari." ',smis_ac_transaksi_detail.kredit ,0)) AS kredit_saldo_awal,
			
            sum(if( (jenis='memorial' OR jenis='') AND tanggal >= '".$dari."',smis_ac_transaksi_detail.debet,0))  AS debet_memorial, 
			sum(if( (jenis='memorial' OR jenis='') AND tanggal >= '".$dari."',smis_ac_transaksi_detail.kredit ,0)) AS kredit_memorial,
			
            sum(if(jenis='transaction' AND tanggal >= '".$dari."',smis_ac_transaksi_detail.debet,0))  AS debet_transaksi, 
			sum(if(jenis='transaction' AND tanggal >= '".$dari."',smis_ac_transaksi_detail.kredit ,0)) AS kredit_transaksi,
			
			smis_ac_account.sub as sub_name,
            smis_ac_account.klas as class_name,
            smis_ac_transaksi.prop AS prop
            FROM smis_ac_transaksi
            LEFT JOIN smis_ac_transaksi_detail 
            ON smis_ac_transaksi.id = smis_ac_transaksi_detail.id_transaksi 
            LEFT JOIN smis_ac_account ON smis_ac_transaksi_detail.nomor_account=smis_ac_account.nomor
			";
	
	$qc = "SELECT count(*) as total
			FROM smis_ac_transaksi
			LEFT JOIN smis_ac_transaksi_detail 
			ON smis_ac_transaksi.id = smis_ac_transaksi_detail.id_transaksi ";
    
    $query = "SELECT 
                SUM( IF(jenis='transaction',smis_ac_transaksi_detail.debet,0)) AS debit_transaksi,
                SUM( IF(jenis='memorial',smis_ac_transaksi_detail.debet,0)) AS debit_memorial,
                SUM( IF(jenis='transaction',smis_ac_transaksi_detail.kredit,0)) AS kredit_transaksi,
                SUM( IF(jenis='memorial',smis_ac_transaksi_detail.kredit,0)) AS kredit_memorial
            FROM smis_ac_transaksi LEFT JOIN smis_ac_transaksi_detail 
                   ON smis_ac_transaksi_detail.id_transaksi = smis_ac_transaksi.id
            WHERE  tanggal >= '".$dari."' 
                   AND tanggal < '".$sampai."' 
                   AND smis_ac_transaksi_detail.nomor_account>='4' ";

    $dbtable = new DBTable ( $db, "smis_ac_transaksi" );
    $dbtable->setPreferredQuery ( true, $qv, $qc );
    $dbtable->setUseWhereforView ( true );
    $dbtable->setShowAll ( true );
    $dbtable->setFetchMethode(DBTable::$OBJECT_FETCH);
    $dbtable->setForceOrderForQuery ( true, " smis_ac_transaksi_detail.nomor_account ASC " );
    $dbtable->setGroupBy ( true, " smis_ac_transaksi_detail.nomor_account" );
    $dbtable->addCustomKriteria ( "smis_ac_transaksi.fix ", " ='0' " );
    $dbtable->addCustomKriteria ( "smis_ac_transaksi_detail.prop ", " NOT LIKE '%del%'" );
    $dbtable->addCustomKriteria ( "smis_ac_transaksi.tanggal ", " <= '".$sampai."' " );
    if($filter!=""){
        $dbtable->addCustomKriteria ( " smis_ac_transaksi_detail.grup ", "='".$filter."'" );
    }

    $data = $dbtable->view("", 0);
    $content = $data['data'];
    return $content;
}

?>