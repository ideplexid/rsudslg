<?php
	global $db;
	global $user;
	require_once 'smis-libs-class/Policy.php';
	require_once 'smis-libs-inventory/policy.php';
	$inventory_policy = new InventoryPolicy("accounting", $user,"modul/");
	
	$policy=new Policy("accounting", $user);
	$policy->setDefaultPolicy(Policy::$DEFAULT_POLICY_RESTRICT);
	$policy->addPolicy("account_read", "account_read", Policy::$DEFAULT_COOKIE_CHANGE,"modul/account_read");
	$policy->addPolicy("tipe_akun_read", "account_read", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/account_read/tipe_akun_read");
	$policy->addPolicy("nomor_perkiraan_read", "account_read", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/account_read/nomor_perkiraan_read");
	$policy->addPolicy("account", "account", Policy::$DEFAULT_COOKIE_CHANGE,"modul/account");
	$policy->addPolicy("tipe_akun", "account", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/account/tipe_akun");
	$policy->addPolicy("nomor_perkiraan", "account", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/account/nomor_perkiraan");
	$policy->addPolicy("nomor_perkiraan_next", "account", Policy::$DEFAULT_COOKIE_KEEP,"snippet/nomor_perkiraan_next");
	$policy->addPolicy("grup_transaksi", "grup_transaksi", Policy::$DEFAULT_COOKIE_CHANGE,"modul/grup_transaksi");
	
    $policy->addPolicy("jurnal", "jurnal", Policy::$DEFAULT_COOKIE_CHANGE,"modul/jurnal");
	$policy->addPolicy("accounting_pasien_bayar", "jurnal", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/jurnal/accounting_pasien_bayar");
	$policy->addPolicy("saldo_awal", "jurnal", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/jurnal/saldo_awal");
	$policy->addPolicy("transaction", "jurnal", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/jurnal/transaction");
	$policy->addPolicy("transaction_masuk", "jurnal", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/jurnal/transaction_masuk");
	$policy->addPolicy("transaction_keluar", "jurnal", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/jurnal/transaction_keluar");
	$policy->addPolicy("error", "jurnal", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/jurnal/error");
	$policy->addPolicy("journal", "jurnal", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/jurnal/journal");
	$policy->addPolicy("transaksi_memorial", "jurnal", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/jurnal/memorial");
	$policy->addPolicy("memorial", "jurnal", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/jurnal/memorial");
	$policy->addPolicy("journalmemorial", "jurnal", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/jurnal/journalmemorial");
	
    $policy->addPolicy("laporan", "laporan", Policy::$DEFAULT_COOKIE_CHANGE,"modul/laporan");
	$policy->addPolicy("ledger", "laporan", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/laporan/ledger");
	$policy->addPolicy("trialbalance", "laporan", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/laporan/trialbalance");	
	$policy->addPolicy("adjustedledger", "laporan", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/laporan/adjustedledger");
	$policy->addPolicy("adjustedtrialbalance", "laporan", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/laporan/adjustedtrialbalance");
	$policy->addPolicy("income", "laporan", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/laporan/income");
	$policy->addPolicy("worksheet", "laporan", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/laporan/worksheet");	
	$policy->addPolicy("worksheet_laba_rugi", "laporan", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/laporan/worksheet_laba_rugi");
	$policy->addPolicy("worksheet_neraca", "laporan", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/laporan/worksheet_neraca");
	$policy->addPolicy("cash_flow", "laporan", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/laporan/cash_flow");
	
    $policy->addPolicy("excel_laba_rugi", "laporan", Policy::$DEFAULT_COOKIE_KEEP,"snippet/excel_laba_rugi");
    $policy->addPolicy("excel_neraca", "laporan", Policy::$DEFAULT_COOKIE_KEEP,"snippet/excel_neraca");
	
    $policy->addPolicy("worksheet_format", "laporan", Policy::$DEFAULT_COOKIE_KEEP,"snippet/worksheet_format");
	$policy->addPolicy("worksheet_print", "laporan", Policy::$DEFAULT_COOKIE_KEEP,"snippet/worksheet_print");
	$policy->addPolicy("excel_neraca_lajur", "laporan", Policy::$DEFAULT_COOKIE_KEEP,"snippet/excel_neraca_lajur");
	$policy->addPolicy("worksheet_print_neraca_lajur", "laporan", Policy::$DEFAULT_COOKIE_KEEP,"snippet/worksheet_print_neraca_lajur");
	$policy->addPolicy("worksheet_print_ledger", "laporan", Policy::$DEFAULT_COOKIE_KEEP,"snippet/worksheet_print_ledger");
	$policy->addPolicy("preview_neraca_lajur", "laporan", Policy::$DEFAULT_COOKIE_KEEP,"snippet/preview_neraca_lajur");
	$policy->addPolicy("preview_laba_rugi", "laporan", Policy::$DEFAULT_COOKIE_KEEP,"snippet/preview_laba_rugi");
	$policy->addPolicy("preview_neraca", "laporan", Policy::$DEFAULT_COOKIE_KEEP,"snippet/preview_neraca");
	$policy->addPolicy("preview_ledger", "laporan", Policy::$DEFAULT_COOKIE_KEEP,"snippet/preview_ledger");
	
    $policy->addPolicy("print_neraca_lajur", "laporan", Policy::$DEFAULT_COOKIE_KEEP,"snippet/print_neraca_lajur");
	$policy->addPolicy("print_laba_rugi", "laporan", Policy::$DEFAULT_COOKIE_KEEP,"snippet/print_laba_rugi");
	$policy->addPolicy("print_neraca", "laporan", Policy::$DEFAULT_COOKIE_KEEP,"snippet/print_neraca");
	$policy->addPolicy("print_ledger", "laporan", Policy::$DEFAULT_COOKIE_KEEP,"snippet/print_ledger");
	
    
	$policy->addPolicy("inventaris", "inventaris", Policy::$DEFAULT_COOKIE_CHANGE,"modul/inventaris");
	$policy->addPolicy("hutang", "hutang", Policy::$DEFAULT_COOKIE_CHANGE,"modul/hutang");
	$policy->addPolicy("beban", "beban", Policy::$DEFAULT_COOKIE_CHANGE,"modul/beban");
	$policy->addPolicy("receivable", "receivable", Policy::$DEFAULT_COOKIE_CHANGE,"modul/receivable");
	
	$policy->addPolicy("cashbank", "cashbank", Policy::$DEFAULT_COOKIE_CHANGE,"modul/cashbank");
	$policy->addPolicy("cash_in", "cashbank", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/cash_bank/cash_in");
	$policy->addPolicy("bank_in", "cashbank", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/cash_bank/bank_in");
	$policy->addPolicy("cash_out", "cashbank", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/cash_bank/cash_out");
	$policy->addPolicy("bank_out", "cashbank", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/cash_bank/bank_out");	
	
    $policy->addPolicy("notify", "notify", Policy::$DEFAULT_COOKIE_CHANGE,"modul/notify");	
	$policy->addPolicy("archive_notify", "notify", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/notify/archive_notify");	
	$policy->addPolicy("all_notify", "notify", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/notify/all_notify");	
	$policy->addPolicy("penjualan_notify", "notify", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/notify/penjualan_notify");	
    $policy->addPolicy("pelunasan_asuransi_notify", "notify", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/notify/pelunasan_asuransi_notify");	
    $policy->addPolicy("return_penjualan_notify", "notify", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/notify/return_penjualan_notify");	
	$policy->addPolicy("pembelian_notify", "notify", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/notify/pembelian_notify");	
	$policy->addPolicy("return_pembelian_notify", "notify", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/notify/return_pembelian_notify");	
	$policy->addPolicy("penerimaan_piutang_notify", "notify", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/notify/penerimaan_piutang_notify");	
	$policy->addPolicy("hutang_gaji_notify", "notify", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/notify/hutang_gaji_notify");	
	$policy->addPolicy("bayar_gaji_notify", "notify", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/notify/bayar_gaji_notify");	
	$policy->addPolicy("bayar_umum_notify", "notify", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/notify/bayar_umum_notify");	
	$policy->addPolicy("pembayaran_hutang_notify", "notify", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/notify/pembayaran_hutang_notify");	
	$policy->addPolicy("adjustment_persediaan_notify", "notify", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/notify/adjustment_persediaan_notify");	
	$policy->addPolicy("pemakaian_notify", "notify", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/notify/pemakaian_notify");	
	$policy->addPolicy("piutang_notify", "notify", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/notify/piutang_notify");	
	$policy->addPolicy("persediaan_notify", "notify", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/notify/persediaan_notify");	
	
    
    $policy->addPolicy("post_accounting", "notify", Policy::$DEFAULT_COOKIE_KEEP,"snippet/post_accounting");
	$policy->addPolicy("un_post_accounting", "notify", Policy::$DEFAULT_COOKIE_KEEP,"snippet/un_post_accounting");	
	$policy->addPolicy("preview_post_accounting", "notify", Policy::$DEFAULT_COOKIE_KEEP,"snippet/preview_post_accounting");	
	
    $policy->addPolicy("settings", "settings", Policy::$DEFAULT_COOKIE_CHANGE,"modul/settings");	
	$policy->addPolicy("kode_akun", "settings", Policy::$DEFAULT_COOKIE_KEEP,"snippet/kode_akun");
	
    $policy->combinePolicy($inventory_policy);
	$policy->initialize();
?>
