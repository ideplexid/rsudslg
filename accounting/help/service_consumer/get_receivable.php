<?php 

/**
 * 
 * this provided manual for get_receivable service
 * 
 * @version 1.0
 * @since 8 Jan 2015
 * @author goblooge
 * @license LGPL v3
 * @copyright Nurul Huda <goblooge@gmail.com>
 * 
 */

require_once 'smis-libs-class/ManualService.php';

$response=array();
$response['page']="0"; 													// always 0
$response['max_page']="0"; 												// always 0.
$response['data']=array(); 												// array of string
$response['data']["0"]=array();											// array of string number 0
$response['data']["0"]["layanan"]="Resep Obat (paten)";					// based on layanan sistem, resep obat, tindakan perawat, tindakan dokter dll
$response['data']["0"]["total"]="1200000";								// jumlahan total per tanggal
$response['data']["0"]["urjip"]="up";							// berisi tiga tempat URI, URJ atau Unit Penunjang
$response['data']["1"]=array();											// array of string number 0
$response['data']["1"]["layanan"]="Tindakan Dokter (paten)";			// based on layanan sistem, resep obat, tindakan perawat, tindakan dokter dll
$response['data']["1"]["total"]="1200000";								// jumlahan total per tanggal
$response['data']["1"]["urjip"]="uri";									// berisi tiga tempat URI, URJ atau Unit Penunjang
$response['data']["2"]=array();											// array of string number 0
$response['data']["2"]["layanan"]="Tindakan Perawat (paten)";			// based on layanan sistem, resep obat, tindakan perawat, tindakan dokter dll
$response['data']["2"]["total"]="5000000";								// jumlahan total per tanggal
$response['data']["2"]["urjip"]="urj";									// berisi tiga tempat URI, URJ atau Unit Penunjang

$provided=array();
$provided['awal']="2014-12-1";
$provided['akhir']="2014-12-31";

$descrption="
This Service Consumer have several goal :
1. provide data of each service in room
2. data that provided is in total
3. the summary of ammount based on seervice type (tindakan dokter, tindakan perawat, resep)
";

$author="
* @version 1.0
* @since 6 Jan 2015
* @author goblooge
* @license LGPL v3
* @copyright Nurul Huda &#60; goblooge@gmail.com &#62;
";

$man=new ManualService("get_receivable");
$man->setAuthor($author);
$man->setDataNeeded($response);
$man->setDataProvided($provided);
$man->setDescription($descrption);

echo $man->getHtml();



?>