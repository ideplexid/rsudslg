<?php 

/**
 * 
 * this provided manual for get_inventaris service
 * 
 * @version 1.0
 * @since 16 Nov 2014
 * @author goblooge
 * @license LGPL v3
 * @copyright Nurul Huda <goblooge@gmail.com>
 * 
 */

require_once 'smis-libs-class/ManualService.php';

$response=array();
$response["0"]=array();											// array of string number 0
$response['data']["0"]["ruang"]="Apotek";								// put racikan name
$response['data']["0"]["barang"]="Lemari";								// put racikan name
$response['data']["0"]["kode"]="RSMM-XXX1";								// Kode Barang Inventaris
$response['data']["0"]["harga"]="1200000";								// Harga Beli Barang
$response['data']["0"]["masuk"]="2013-12-12";							// Tanggal Pertama Masuk
$response['data']["0"]["durasi"]="5";									// Durasi Barang Susut (dalam tahun)
$response['data']["0"]["kondisi"]="baik";								// kondisi barang


$provided=array();
$provided['dari']="2014-12-1";
$provided['sampai']="2014-12-31";

$descrption="
This Service Consumer have several goal :
1. Provide data inventaris for each room
2. Provide Amount of depreciations cost
3. Provide Last Date of depreciations
";

$author="
* @version 1.0
* @since 6 Jan 2015
* @author goblooge
* @license LGPL v3
* @copyright Nurul Huda &#60; goblooge@gmail.com &#62;
";

$man=new ManualService("get_inventaris");
$man->setAuthor($author);
$man->setDataNeeded($response);
$man->setDataProvided($provided);
$man->setDescription($descrption);

echo $man->getHtml();



?>