<?php

global $db;
require_once 'accounting/function/get_data_neraca_lajur.php';
require_once 'accounting/class/adapter/WorksheetAdapter.php';
$akunltb = getSettings($db,"accounting-akun-khusus-laba-berjalan","NOT SET");
$dari    = $_POST['from_date'];
$sampai  = $_POST['to_date'];
$result  = get_data_neraca_lajur($db,$dari,$sampai,$_POST['filter_grup']);
$x       = get_laba_tahun_berjalan($db,$dari,$sampai);   
$adapter = new WorksheetAdapter();
$adapter ->setTotalLabaBerjalan($akunltb,$x->debit_transaksi,$x->kredit_transaksi,$x->debit_memorial,$x->kredit_memorial);
$adapter ->setFooterEnabled(true)
         ->setMoneyEnable(false);
$content = $adapter->getContent($result);

require_once ("smis-libs-out/php-excel/PHPExcel.php");
$file = new PHPExcel ();
$file->getProperties ()->setCreator ( "PT. Inovasi Ide Utama" );
$file->getProperties ()->setLastModifiedBy ( "Operator" );
$file->getProperties ()->setTitle ( "Neraca Lajur" );
$file->getProperties ()->setSubject ( "Neraca Lajur" );
$file->getProperties ()->setDescription ( "Neraca Lajur" );
$file->getProperties ()->setKeywords ( "Neraca Lajur" );
$file->getProperties ()->setCategory ( "Neraca Lajur" );
$sheet = $file->getActiveSheet ();

$row = 1;
$sheet->setCellValue("A".$row, "NERACA LAJUR");
$sheet->mergeCells('A'.$row.':'.'J'.$row);
$sheet->getStyle('A'.$row.':'.'J'.$row)->getFont()->setBold(true);
$sheet->getStyle('A'.$row.':'.'J'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$sheet->getStyle('A'.$row.':'.'J'.$row)->getFont()->setSize(18);
$sheet->getStyle('A'.$row.':'.'J'.$row)->getFont()->getColor()->setRGB('#0000FF');
$row++;
$sheet->setCellValue("A".$row, strtoupper(date("F Y",strtotime($sampai))));
$sheet->mergeCells('A'.$row.':'.'J'.$row);
$sheet->getStyle('A'.$row.':'.'J'.$row)->getFont()->setBold(true);
$sheet->getStyle('A'.$row.':'.'J'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$sheet->getStyle('A'.$row.':'.'J'.$row)->getFont()->setSize(12);
$sheet->getStyle('A'.$row.':'.'J'.$row)->getFont()->getColor()->setRGB('#0000FF');
$row = $row + 2;

$r = $row+1;
$sheet->setCellValue("A".$row, "Nomor Account");
$sheet->mergeCells('A'.$row.':'.'A'.$r);
$sheet->setCellValue("B".$row, "Nama Account");
$sheet->mergeCells('B'.$row.':'.'B'.$r);
$sheet->setCellValue("C".$row, "Saldo Awal");
$sheet->mergeCells('C'.$row.':'.'D'.$row);
$sheet->setCellValue("E".$row, "Transaksi");
$sheet->mergeCells('E'.$row.':'.'F'.$row);
$sheet->setCellValue("G".$row, "Laba Rugi");
$sheet->mergeCells('G'.$row.':'.'H'.$row);
$sheet->setCellValue("I".$row, "Neraca");
$sheet->mergeCells('I'.$row.':'.'J'.$row);
$sheet->getStyle('A'.$row.':'.'J'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$sheet->getStyle('A'.$row.':'.'J'.$row)->getFont()->setBold(true);
foreach(range('A','J') as $columnID) {
    $sheet->getColumnDimension($columnID)->setAutoSize(true);
}
$row++;
$sheet->setCellValue("C".$row, "Debet");
$sheet->setCellValue("D".$row, "Kredit");
$sheet->setCellValue("E".$row, "Debet");
$sheet->setCellValue("F".$row, "Kredit");
$sheet->setCellValue("G".$row, "Debet");
$sheet->setCellValue("H".$row, "Kredit");
$sheet->setCellValue("I".$row, "Debet");
$sheet->setCellValue("J".$row, "Kredit");
$sheet->getStyle('A'.$row.':'.'J'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$sheet->getStyle('A'.$row.':'.'J'.$row)->getFont()->setBold(true);
$row++;
foreach(range('A','j') as $columnID) {
    $sheet->getColumnDimension($columnID)->setAutoSize(true);
}
$firstrow=$row;
for($i = 0; $i < sizeof($content)-2; $i++) {
    $sheet->setCellValueExplicit("A".$row, $content[$i]['Nomor Account'],PHPExcel_Cell_DataType::TYPE_STRING);
    $sheet->setCellValue("B".$row, $content[$i]['Nama Account']);
    $sheet->setCellValue("C".$row, $content[$i]['Debet Saldo Awal']);
    $sheet->setCellValue("D".$row, $content[$i]['Kredit Saldo Awal']);
    $sheet->setCellValue("E".$row, $content[$i]['Debet JP']);
    $sheet->setCellValue("F".$row, $content[$i]['Kredit JP']);
    $sheet->setCellValue("G".$row, $content[$i]['FDebet RugiLaba']);
    $sheet->setCellValue("H".$row, $content[$i]['FKredit RugiLaba']);
    $sheet->setCellValue("I".$row, $content[$i]['FDebet Neraca']);
    $sheet->setCellValue("J".$row, $content[$i]['FKredit Neraca']);
    $row++;
}

$lastrow = $row - 1;
$sheet->setCellValue("A".$row, "TOTAL");
$sheet->mergeCells('A'.$row.':'.'B'.$row);
$sheet->setCellValue("C".$row, "=SUM(C6:C".$lastrow.")");
$sheet->setCellValue("D".$row, "=SUM(D6:D".$lastrow.")");
$sheet->setCellValue("E".$row, "=SUM(E6:E".$lastrow.")");
$sheet->setCellValue("F".$row, "=SUM(F6:F".$lastrow.")");
$sheet->setCellValue("G".$row, "=SUM(G6:G".$lastrow.")");
$sheet->setCellValue("H".$row, "=SUM(H6:H".$lastrow.")");
$sheet->setCellValue("I".$row, "=SUM(I6:I".$lastrow.")");
$sheet->setCellValue("J".$row, "=SUM(J6:J".$lastrow.")");

$row++;
$lastrow = $row - 1;
$sheet->setCellValue("A".$row, $content[$i+2]['Nama Account']);
$sheet->mergeCells('A'.$row.':'.'B'.$row);
$sheet->setCellValue("C".$row, "=IF( (C".$lastrow."-D".$lastrow.") > 0,(C".$lastrow."-D".$lastrow."),0)");
$sheet->setCellValue("D".$row, "=IF( (D".$lastrow."-C".$lastrow.") > 0,(D".$lastrow."-C".$lastrow."),0)");
$sheet->setCellValue("E".$row, "=IF( (E".$lastrow."-F".$lastrow.") > 0,(E".$lastrow."-F".$lastrow."),0)");
$sheet->setCellValue("F".$row, "=IF( (F".$lastrow."-E".$lastrow.") > 0,(F".$lastrow."-E".$lastrow."),0)");
$sheet->setCellValue("G".$row, "=IF( (G".$lastrow."-H".$lastrow.") > 0,(G".$lastrow."-H".$lastrow."),0)");
$sheet->setCellValue("H".$row, "=IF( (H".$lastrow."-G".$lastrow.") > 0,(H".$lastrow."-G".$lastrow."),0)");
$sheet->setCellValue("I".$row, "=IF( (I".$lastrow."-J".$lastrow.") > 0,(I".$lastrow."-J".$lastrow."),0)");
$sheet->setCellValue("J".$row, "=IF( (J".$lastrow."-I".$lastrow.") > 0,(J".$lastrow."-I".$lastrow."),0)");

$sheet->getStyle('A'.$firstrow.":A".$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$sheet->getStyle('A'.($lastrow).':'.'J'.$row)->getFont()->setBold(true);
$sheet->getStyle('C'.$firstrow.':'.'J'.$row)->getNumberFormat()->setFormatCode('#,##0.00');

foreach(range('A','J') as $columnID) {
    $sheet->getColumnDimension($columnID)->setAutoSize(true);
}

header ( 'Content-Type: application/vnd.ms-excel' );
header ( 'Content-Disposition: attachment;filename="NERACA LAJUR.xls"' );
header ( 'Cache-Control: max-age=0' );
$writer = PHPExcel_IOFactory::createWriter ( $file, 'Excel5' );
$writer->save ( 'php://output' );
return;

?>