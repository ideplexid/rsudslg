<?php

global $db;
$dari = $_POST['from_date'];
$sampai = $_POST['to_date'];
$akun = $_POST['account'];

$qv = "SELECT smis_ac_transaksi.id AS id, 
        smis_ac_transaksi.tanggal AS tanggal, 
        smis_ac_transaksi.jenis AS jenis, 
        smis_ac_transaksi.nomor AS nomor, 
        smis_ac_transaksi.code AS code, 
        smis_ac_transaksi.keterangan AS keterangan, 
        smis_ac_transaksi_detail.id AS id_detail, 
        smis_ac_transaksi_detail.nomor_account AS nomor_account, 
        smis_ac_transaksi_detail.nama_account AS nama_account, 
        smis_ac_transaksi_detail.keterangan AS uraian, 
        smis_ac_transaksi_detail.debet AS debet, 
        smis_ac_transaksi_detail.kredit AS kredit,
        smis_ac_transaksi_detail.grup AS grup,
        smis_ac_transaksi.prop AS prop
        FROM smis_ac_transaksi
        LEFT JOIN smis_ac_transaksi_detail 
        ON smis_ac_transaksi.id = smis_ac_transaksi_detail.id_transaksi 
        ";

$qc = "SELECT count(*) as total
        FROM smis_ac_transaksi
        LEFT JOIN smis_ac_transaksi_detail 
        ON smis_ac_transaksi.id = smis_ac_transaksi_detail.id_transaksi ";
        
$column = array ();
$dbtable = new DBTable ( $db, "smis_ac_transaksi", $column );
$dbtable->setPreferredQuery ( true, $qv, $qc );
$dbtable->setUseWhereforView ( true );
$dbtable->setShowAll(true);
//$dbtable->addCustomKriteria ( NULL, " ( smis_ac_transaksi.jenis = 'transaction' OR smis_ac_transaksi.jenis = 'saldo_awal' ) " );
$dbtable->addCustomKriteria(" smis_ac_transaksi.tanggal >= " ,"'".$dari."'");
$dbtable->addCustomKriteria(" smis_ac_transaksi.tanggal <= " ,"'".$sampai."'");
$dbtable->addCustomKriteria ( "smis_ac_transaksi.fix ", " ='0' " );
$dbtable->addCustomKriteria ( "smis_ac_transaksi_detail.prop ", " NOT LIKE '%del%'" );
if (isset ( $_POST ['account'] )) {
    if(isset($_POST['all_child']) && $_POST['all_child'] =="0"){
        $dbtable->addCustomKriteria ( "smis_ac_transaksi_detail.nomor_account ", " ='" . $_POST ['account'] . "'" );
    }else{
        $dbtable->addCustomKriteria ( "smis_ac_transaksi_detail.nomor_account ", " LIKE '" . $_POST ['account'] . "%'" );
    }
}

$data = $dbtable->view("", 0);
$content = $data['data'];

$summary = 0;
if(substr($akun,0,1)*1<4){
    $query="SELECT SUM(smis_ac_transaksi_detail.debet) as debet, SUM(smis_ac_transaksi_detail.kredit) as kredit 
            FROM smis_ac_transaksi
            RIGHT JOIN smis_ac_transaksi_detail 
            ON smis_ac_transaksi.id = smis_ac_transaksi_detail.id_transaksi
            WHERE smis_ac_transaksi.tanggal<'".$dari."' 
            AND smis_ac_transaksi.prop!='del'
            AND smis_ac_transaksi_detail.prop!='del' 
            AND nomor_account LIKE '".$akun."%'
            ";
    $hasil = $db->get_row($query);
    $summary=0;
    if($hasil!=null){
        $summary=$hasil->debet-$hasil->kredit;
    }    
}

$dbt = new DBTable($db,"smis_ac_account");
$a = $dbt->select(array("nomor"=>$akun));

ob_start();
echo "<div>";
    echo "<h2 align='center'><strong>NERACA LAJUR</h2>";
    $periode = ArrayAdapter::dateFormat("date d M Y H:i",$dari)." - ".ArrayAdapter::dateFormat("date d M Y H:i",$sampai);
    echo "<h4 align='center'><strong>".strtoupper($periode)."</h4>";
    echo "<table border='1' width='100%'>";
        echo "<tbody>";
            echo "<tr>";
                echo "<td align='center' colspan='3'><strong>".$a->nomor."</strong></td>";
                echo "<td align='center' colspan='4'><strong>".strtoupper($a->nama)."</strong></td>";
                echo "<td align='center' colspan='2'><strong>SALDO</strong></td>";
            echo "</tr>";
            echo "<tr>";
                echo "<td align='center' ><strong>No.</strong></td>";
                echo "<td align='center' ><strong>Tanggal</strong></td>";
                echo "<td align='center' ><strong>Code</strong></td>";
                echo "<td align='center' ><strong>Keterangan</strong></td>";
                echo "<td align='center' ><strong>Dept.</strong></td>";
                echo "<td align='center' ><strong>Debet</strong></td>";
                echo "<td align='center' ><strong>Kredit</strong></td>";
                echo "<td align='center' ><strong>Debet</strong></td>";
                echo "<td align='center' ><strong>Kredit</strong></td>";
            echo "</tr>";
            $no = 1;
            if($summary>0) {
                echo "<tr>";
                    echo "<td>".$no."</td>";
                    echo "<td> - </td>";
                    echo "<td> - </td>";
                    echo "<td>Saldo Awal Bulan Berjalan</td>";
                    echo "<td> - </td>";
                    echo "<td align='right' >0</td>";
                    echo "<td align='right' >0</td>";
                    if($summary > 0) {
                        echo "<td align='right' >".$summary."</td>";
                    } else {
                        echo "<td align='right' >0</td>";
                    }
                    if($summary < 0) {
                        $v = abs($summary);
                        echo "<td align='right' >".$v."</td>";
                    } else {
                        echo "<td align='right' >0</td>";
                    }
                echo "</tr>";
                $no++;
            }
            $total_transaksi_debet = 0;
            $total_transaksi_kredit = 0;
            $total_saldo_debet = 0;
            $total_saldo_kredit = 0;
            foreach($content as $c) {
                echo "<tr>";
                    $summary = $summary + $c->debet - $c->kredit;  
                    echo "<td>".$no."</td>";
                    echo "<td>".ArrayAdapter::dateFormat("date d M Y",$c->tanggal)."</td>";
                    echo "<td>".$c->nomor."</td>";
                    echo "<td>".$c->keterangan."</td>";
                    echo "<td>".$c->grup."</td>";
                    if($c->jenis!="saldo_awal") {
                        echo "<td align='right' >".$c->debet."</td>";
                        $total_transaksi_debet = $total_transaksi_debet + $c->debet;
                        echo "<td align='right' >".$c->kredit."</td>";
                        $total_transaksi_kredit = $total_transaksi_kredit + $c->kredit;
                    } else {
                        echo "<td align='right' >0</td>";
                        echo "<td align='right' >0</td>";
                    }
                    if($summary > 0) {
                        echo "<td align='right' >".$summary."</td>";
                        $total_saldo_debet = $total_saldo_debet + $summary;
                    } else {
                        echo "<td align='right' >0</td>";
                    }
                    if($summary < 0) {
                        $v = abs($summary);
                        echo "<td align='right' >".$v."</td>";
                        $total_saldo_kredit = $total_saldo_kredit + $v;
                    } else {
                        echo "<td align='right' >0</td>";
                    }
                echo "</tr>";
                $no++;
            }
            echo "<tr>";
                echo "<td> </td>";
                echo "<td> </td>";
                echo "<td> </td>";
                echo "<td> </td>";
                echo "<td>Total</td>";
                echo "<td align='right' >".$total_transaksi_debet."</td>";
                echo "<td align='right' >".$total_transaksi_kredit."</td>";
                echo "<td> </td>";
                echo "<td> </td>";
            echo "<tr>";
            echo "<tr>";
                echo "<td> </td>";
                echo "<td> </td>";
                echo "<td> </td>";
                echo "<td> </td>";
                echo "<td>Mutasi</td>";
                if($total_transaksi_debet > $total_transaksi_kredit) {
                    $mutasi_debet = $total_transaksi_debet - $total_transaksi_kredit;
                    echo "<td align='right' >".$mutasi_debet."</td>";
                } else {
                    $mutasi_debet = 0;
                    echo "<td align='right' >".$mutasi_debet."</td>";
                }
                if($total_transaksi_kredit > $total_transaksi_debet) {
                    $mutasi_kredit = $total_transaksi_kredit - $total_transaksi_debet;
                    echo "<td align='right' >".$mutasi_kredit."</td>";
                } else {
                    $mutasi_kredit = 0;
                    echo "<td align='right' >".$mutasi_kredit."</td>";
                }
                echo "<td> </td>";
                echo "<td> </td>";
            echo "<tr>";
            echo "<tr>";
                echo "<td> </td>";
                echo "<td> </td>";
                echo "<td> </td>";
                echo "<td> </td>";
                echo "<td>Saldo Awal</td>";
                echo "<td align='right' >".$total_saldo_debet."</td>";
                echo "<td align='right' >".$total_saldo_kredit."</td>";
                echo "<td> </td>";
                echo "<td> </td>";
            echo "<tr>";
            echo "<tr>";
                echo "<td> </td>";
                echo "<td> </td>";
                echo "<td> </td>";
                echo "<td> </td>";
                echo "<td>Saldo Akhir</td>";
                if(($mutasi_debet + $total_saldo_debet) > ($mutasi_kredit + $total_saldo_kredit)) {
                    $total_saldo_akhir_debet = ($mutasi_debet + $total_saldo_debet) - ($mutasi_kredit + $total_saldo_kredit);
                    echo "<td align='right' >".$total_saldo_akhir_debet."</td>";
                } else {
                    $total_saldo_akhir_debet = 0;
                    echo "<td align='right' >".$total_saldo_akhir_debet."</td>";
                }
                if(($mutasi_debet + $total_saldo_debet) < ($mutasi_kredit + $total_saldo_kredit)) {
                    $total_saldo_akhir_kredit = ($mutasi_kredit + $total_saldo_kredit) - ($mutasi_debet + $total_saldo_debet);
                    echo "<td align='right' >".$total_saldo_akhir_kredit."</td>";
                } else {
                    $total_saldo_akhir_kredit = 0;
                    echo "<td align='right' >".$total_saldo_akhir_kredit."</td>";
                }
                echo "<td> </td>";
                echo "<td> </td>";
            echo "<tr>";
        echo "</tbody>";
    echo "</table>";
echo "</div>";
$x = ob_get_clean();

$response=new ResponsePackage();
$response->setStatus(ResponsePackage::$STATUS_OK);
//$response->setContent($x);
$response->setWarning(true,"Preview",$x);
echo json_encode($response->getPackage());

?>