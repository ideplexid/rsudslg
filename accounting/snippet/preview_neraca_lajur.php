<?php
global $db;
require_once 'accounting/function/get_data_neraca_lajur.php';
require_once 'accounting/class/adapter/WorksheetAdapter.php';
$akunltb = getSettings($db,"accounting-akun-khusus-laba-berjalan","NOT SET");
$dari    = $_POST['from_date'];
$sampai  = $_POST['to_date'];
$result  = get_data_neraca_lajur($db,$dari,$sampai,$_POST['filter_grup']);
$x       = get_laba_tahun_berjalan($db,$dari,$sampai);   
$adapter = new WorksheetAdapter();
$adapter ->setTotalLabaBerjalan($akunltb,$x->debit_transaksi,$x->kredit_transaksi,$x->debit_memorial,$x->kredit_memorial);
$adapter ->setFooterEnabled(true)
         ->setMoneyEnable(false);
$content = $adapter->getContent($result);

echo "<div>";
    echo "<button type='button' class='btn btn-primary' onclick='preview_neraca_lajur.back()'>Back</button>";
    echo "&nbsp";
    echo "<button type='button' class='btn btn-primary' onclick='preview_neraca_lajur.cetak()'>Print</button>";
echo "</div>";

echo "<div id='print_table_preview_neraca_lajur'>";
    echo "<h2 align='center'><strong>NERACA LAJUR</h2>";
    echo "<h4 align='center'><strong>".strtoupper(date("F Y",strtotime($sampai)))."</h4>";
    echo "<div class='table_neraca_lajur' border='1' width='500px'>";
        echo "<div class='thead' >";
            echo "<div class='tr'>";
                echo "<div class='td setengah' align='center' ><strong>Nomor</strong></div>";
                echo "<div class='td tigakali' align='center' ><strong>Nama Account</strong></div>";
                echo "<div class='td duakali' width='100px' align='center' colspan='2' ><strong>Saldo Awal</strong></div>";
                echo "<div class='td duakali' width='100px' align='center' colspan='2' ><strong>Transaksi</strong></div>";
                echo "<div class='td duakali' width='100px' align='center' colspan='2' ><strong>Laba Rugi</strong></div>";
                echo "<div class='td duakali' width='100px' align='center' colspan='2' ><strong>Neraca</strong></div>";
            echo "</div>";
            echo "<div class='tr'>";
                echo "<div class='td setengah last'  align='center'>&nbsp;</div>";
                echo "<div class='td tigakali last'  align='center'>&nbsp;</div>";
                echo "<div class='td last'  align='center'><strong>Debet</strong></div>";
                echo "<div class='td last'  align='center'><strong>Kredit</strong></div>";
                echo "<div class='td last'  align='center'><strong>Debet</strong></div>";
                echo "<div class='td last'  align='center'><strong>Kredit</strong></div>";
                echo "<div class='td last'  align='center'><strong>Debet</strong></div>";
                echo "<div class='td last'  align='center'><strong>Kredit</strong></div>";
                echo "<div class='td last'  align='center'><strong>Debet</strong></div>";
                echo "<div class='td last'  align='center'><strong>Kredit</strong></div>";
            echo "</div>";            
        echo "</div>";
        echo "<div class='tbody'>";
            $total=sizeof($content);
            $number=$total;
            $i = 0;
            for(; $i < $total-2; $i++) {
                echo "<div class='tr'>";
                    echo "<div class='td setengah'  >".$content[$i]['Nomor Account']."</div>";
                    echo "<div class='td tigakali' >".$content[$i]['Nama Account']."</div>";
                    echo "<div class='td' align='right'>".number_format($content[$i]['Debet Saldo Awal'],2,",",".")."</div>";
                    echo "<div class='td' align='right'>".number_format($content[$i]['Kredit Saldo Awal'],2,",",".")."</div>";
                    echo "<div class='td' align='right'>".number_format($content[$i]['Debet JP'],2,",",".")."</div>";
                    echo "<div class='td' align='right'>".number_format($content[$i]['Kredit JP'],2,",",".")."</div>";                    
                    echo "<div class='td' align='right'>".number_format($content[$i]['FDebet RugiLaba'],2,",",".")."</div>";
                    echo "<div class='td' align='right'>".number_format($content[$i]['FKredit RugiLaba'],2,",",".")."</div>";
                    echo "<div class='td' align='right'>".number_format($content[$i]['FDebet Neraca'],2,",",".")."</div>";
                    echo "<div class='td' align='right'>".number_format($content[$i]['FKredit Neraca'],2,",",".")."</div>";
                echo "</div>";             
            }
            
        echo "</div>";
        
        echo "<div class='tfoot'>";
            for(; $i < $total; $i++) {
                echo "<div class='tr'>";
                    echo "<div class='td setengah last'  ></div>";
                    echo "<div class='td tigakali last' >".$content[$i]['Account']."</div>";
                    echo "<div class='td last' align='right'> ".number_format($content[$i]['Debet Saldo Awal'],2,",",".")."</div>";
                    echo "<div class='td last' align='right'> ".number_format($content[$i]['Kredit Saldo Awal'],2,",",".")."</div>";
                    echo "<div class='td last' align='right'> ".number_format($content[$i]['Debet JP'],2,",",".")."</div>";
                    echo "<div class='td last' align='right'> ".number_format($content[$i]['Kredit JP'],2,",",".")."</div>";                    
                    echo "<div class='td last' align='right'> ".number_format($content[$i]['FDebet RugiLaba'],2,",",".")."</div>";
                    echo "<div class='td last' align='right'> ".number_format($content[$i]['FKredit RugiLaba'],2,",",".")."</div>";
                    echo "<div class='td last' align='right'> ".number_format($content[$i]['FDebet Neraca'],2,",",".")."</div>";
                    echo "<div class='td last' align='right'> ".number_format($content[$i]['FKredit Neraca'],2,",",".")."</div>";
                echo "</div>";
            }
        echo "</div>";
    echo "</div>";
echo "</div>";

$dari   = new Hidden("preview_neraca_lajur_dari","",$_POST['from_date']);
$sampai = new Hidden("preview_neraca_lajur_sampai","",$_POST['to_date']);
$grup = new Hidden("preview_neraca_lajur_grup","",$_POST['filter_grup']);
echo $dari->getHtml();
echo $sampai->getHtml();
echo $grup->getHtml();
echo addJS  ("accounting/resource/js/preview_neraca_lajur.js",false);
echo addCSS ("accounting/resource/css/preview_neraca_lajur.css",false);

?>
