<?php 

/*ob_start();
echo "AAAAA";
$x=ob_get_clean();*/

global $db;
$dari=$_POST['from_date'];
$sampai=$_POST['to_date'];
$akun=$_POST['akun']=="laba_rugi"?" >='4' ":" <'4' ";

$qv = "SELECT smis_ac_transaksi.id AS id, 
			smis_ac_transaksi.tanggal AS tanggal, 
			smis_ac_transaksi.jenis AS jenis, 
			smis_ac_transaksi.nomor AS nomor, 
			smis_ac_transaksi.keterangan AS keterangan, 
			smis_ac_transaksi_detail.id AS id_detail, 
			smis_ac_transaksi_detail.nomor_account AS nomor_account, 
			smis_ac_transaksi_detail.nama_account AS nama_account, 
			smis_ac_transaksi_detail.keterangan AS uraian,
			sum(smis_ac_transaksi_detail.debet)  AS debet, 
			sum(smis_ac_transaksi_detail.kredit) AS kredit,
            smis_ac_account.sub as sub_name,
            smis_ac_account.klas as class_name,
			smis_ac_transaksi.prop AS prop
			FROM smis_ac_transaksi
			LEFT JOIN smis_ac_transaksi_detail 
			ON smis_ac_transaksi.id = smis_ac_transaksi_detail.id_transaksi 
            LEFT JOIN smis_ac_account ON smis_ac_transaksi_detail.nomor_account=smis_ac_account.nomor
			";
	
	$qc = "SELECT count(*) as total
			FROM smis_ac_transaksi
			LEFT JOIN smis_ac_transaksi_detail 
			ON smis_ac_transaksi.id = smis_ac_transaksi_detail.id_transaksi ";
            
$dbtable = new DBTable ( $db, "smis_ac_transaksi" );
$dbtable->setPreferredQuery ( true, $qv, $qc );
$dbtable->setUseWhereforView ( true );
$dbtable->setShowAll ( true );
$dbtable->setForceOrderForQuery ( true, " smis_ac_transaksi_detail.nomor_account ASC " ); // not work, in dbtable
$dbtable->setGroupBy ( true, " smis_ac_transaksi_detail.nomor_account" );
$dbtable->addCustomKriteria ( "smis_ac_transaksi.fix ", " ='0' " );
$dbtable->addCustomKriteria ( "smis_ac_transaksi_detail.nomor_account ", " ".$akun." " );
$dbtable->addCustomKriteria ( "smis_ac_transaksi.tanggal ", " < '".$sampai."' " );

if(isset($_POST['filter_grup']) && $_POST['filter_grup']!=""){
    $dbtable->addCustomKriteria ( " smis_ac_transaksi_detail.grup ", "='".$_POST['filter_grup']."'" );
}

$data = $dbtable->view("", 0);
$content = $data['data'];

ob_start();
echo "<div>";
    echo "<h2 align='center'><strong>".ArrayAdapter::format("unslug",$_POST['akun'])."</strong></h2>";
    echo "<h4 align='center'><strong>".strtoupper(date("d F Y",strtotime($dari)))." - ".strtoupper(date("d F Y",strtotime($sampai)))."</strong></h4>";
    
    echo "<table width='100%'>";
        /*echo "<thead>";
            echo "<tr>";
                echo "<th></th>";
                echo "<th></th>";
                echo "<th></th>";
                echo "<th><strong>Debet (IDR)</strong></th>";
                echo "<th><strong>Kredit (IDR)</strong></th>";
            echo "</tr>";
        echo "</thead>";*/
        echo "<tbody>";
            $nomor_account = "";
            $class_name = "0";
            $sub_name = "0";
            $total_class_name_debet = 0;
            $total_class_name_kredit = 0;
            $total_sub_name_debet = 0;
            $total_sub_name_kredit = 0;
            $total_class_name = 0;
            $total_sub_name = 0;
            $j = 0;
            /*for($i = 0; $i < sizeof($content); $i++) {
                if($content[$i]->nomor_account == ""){
                    continue;
                }
                else if(strcmp(substr($content[$i]->nomor_account, 0, 1), $class_name) != 0 && strcmp(substr($content[$i]->nomor_account, 0, 2), $sub_name) != 0 && $class_name == "0" && $sub_name == "0") {
                     echo "<tr>";
                        echo "<td><strong>".$content[$i]->class_name."</strong></td>";
                        echo "<td></td>";
                        echo "<td></td>";
                        echo "<td></td>";
                        echo "<td></td>";
                    echo "</tr>";
                    echo "<tr>";
                        echo "<td></td>";
                        echo "<td><strong>".$content[$i]->sub_name."</strong></td>";
                        echo "<td></td>";
                        echo "<td></td>";
                        echo "<td></td>";
                    echo "</tr>";
                    echo "<tr>";
                        echo "<td></td>";
                        echo "<td class='text-right'>".$content[$i]->nomor_account."</td>";
                        echo "<td>".$content[$i]->nama_account."</td>";
                        if($content[$i]->debet - $content[$i]->kredit > 0) {
                            $debet = $content[$i]->debet - $content[$i]->kredit;
                            $kredit = 0;
                            echo "<td class='text-right'>".$debet."</td>";
                            echo "<td class='text-right'>".$kredit."</td>";
                        } else {
                            $debet = 0;
                            $kredit = ($content[$i]->debet - $content[$i]->kredit)*-1;
                            echo "<td class='text-right'>".$debet."</td>";
                            echo "<td class='text-right'>".$kredit."</td>";
                        }
                    echo "</tr>";
                    $class_name = substr($content[$i]->nomor_account, 0, 1);
                    $sub_name = substr($content[$i]->nomor_account, 0, 2);
                    $total_sub_name_debet = $total_sub_name_debet + $debet;
                    $total_sub_name_kredit = $total_sub_name_kredit + $kredit;
                    $total_class_name_debet = $total_class_name_debet + $debet;
                    $total_class_name_kredit = $total_class_name_kredit + $kredit;
                }
                else if(strcmp(substr($content[$i]->nomor_account, 0, 1), $class_name) != 0 && strcmp(substr($content[$i]->nomor_account, 0, 2), $sub_name) != 0) {
                    echo "<tr>";
                        echo "<td></td>";
                        echo "<td><strong>Total ".$content[$i - 1]->sub_name."</strong></td>";
                        echo "<td></td>";
                        echo "<td class='text-right'><strong>$total_sub_name_debet</strong></td>";
                        echo "<td class='text-right'><strong>$total_sub_name_kredit</strong></td>";
                    echo "</tr>";
                    $debet = 0;
                    $kredit = 0;
                    echo "<tr>";
                        echo "<td><strong>Total ".$content[$i - 1]->class_name."</strong></td>";
                        echo "<td></td>";
                        echo "<td></td>";
                        echo "<td class='text-right'><strong>".$total_class_name_debet."</strong></td>";
                        echo "<td class='text-right'><strong>".$total_class_name_kredit."</strong></td>";
                    echo "</tr>";
                    $total_class_name_debet = 0;
                    $total_class_name_kredit = 0;
                    echo "<tr>";
                        echo "<td> </td>";
                        echo "<td> </td>";
                        echo "<td> </td>";
                        echo "<td> </td>";
                        echo "<td> </td>";
                    echo "</tr>";
                    echo "<tr>";
                        echo "<td> </td>";
                        echo "<td> </td>";
                        echo "<td> </td>";
                        echo "<td> </td>";
                        echo "<td> </td>";
                    echo "</tr>";
                    echo "<tr>";
                        echo "<td><strong>".$content[$i]->class_name."</strong></td>";
                        echo "<td></td>";
                        echo "<td></td>";
                        echo "<td></td>";
                        echo "<td></td>";
                    echo "</tr>";
                    echo "<tr>";
                        echo "<td></td>";
                        echo "<td><strong>".$content[$i]->sub_name."</strong></td>";
                        echo "<td></td>";
                        echo "<td></td>";
                        echo "<td></td>";
                    echo "</tr>";
                    echo "<tr>";
                        echo "<td></td>";
                        echo "<td class='text-right'>".$content[$i]->nomor_account."</td>";
                        echo "<td>".$content[$i]->nama_account."</td>";
                        if($content[$i]->debet - $content[$i]->kredit > 0) {
                            $debet = $content[$i]->debet - $content[$i]->kredit;
                            $kredit = 0;
                            echo "<td class='text-right'>".$debet."</td>";
                            echo "<td class='text-right'>".$kredit."</td>";
                        } else {
                            $debet = 0;
                            $kredit = ($content[$i]->debet - $content[$i]->kredit)*-1;
                            echo "<td class='text-right'>".$debet."</td>";
                            echo "<td class='text-right'>".$kredit."</td>";
                        }
                    echo "</tr>";
                    $class_name = substr($content[$i]->nomor_account, 0, 1);
                    $sub_name = substr($content[$i]->nomor_account, 0, 2);
                    $total_sub_name_debet = 0;
                    $total_sub_name_kredit = 0;
                    $total_class_name_debet = $total_class_name_debet + $debet;
                    $total_class_name_kredit = $total_class_name_kredit + $kredit;
                }
                else if(strcmp(substr($content[$i]->nomor_account, 0, 1), $class_name) == 0 && strcmp(substr($content[$i]->nomor_account, 0, 2), $sub_name) != 0) {
                    if($total_sub_name_debet == 0 && $total_sub_name_kredit == 0) {
                        $total_sub_name_debet = $total_sub_name_debet + $debet;
                        $total_sub_name_kredit = $total_sub_name_kredit + $kredit;
                    }
                    echo "<tr>";
                        echo "<td></td>";
                        echo "<td><strong>Total ".$content[$i - 1]->sub_name."</strong></td>";
                        echo "<td></td>";
                        echo "<td class='text-right'><strong>".$total_sub_name_debet."</strong></td>";
                        echo "<td class='text-right'><strong>".$total_sub_name_kredit."</strong></td>";
                    echo "</tr>";
                    $total_sub_name_debet = 0;
                    $total_sub_name_kredit = 0;
                    echo "<tr>";
                        echo "<td> </td>";
                        echo "<td> </td>";
                        echo "<td> </td>";
                        echo "<td> </td>";
                        echo "<td> </td>";
                    echo "</tr>";
                    echo "<tr>";
                        echo "<td></td>";
                        echo "<td><strong>".$content[$i]->sub_name."</strong></td>";
                        echo "<td></td>";
                        echo "<td></td>";
                        echo "<td></td>";
                    echo "</tr>";
                    echo "<tr>";
                        echo "<td></td>";
                        echo "<td class='text-right'>".$content[$i]->nomor_account."</td>";
                        echo "<td>".$content[$i]->nama_account."</td>";
                        if($content[$i]->debet - $content[$i]->kredit > 0) {
                            $debet = $content[$i]->debet - $content[$i]->kredit;
                            $kredit = 0;
                            echo "<td class='text-right'>".$debet."</td>";
                            echo "<td class='text-right'>".$kredit."</td>";
                        } else {
                            $debet = 0;
                            $kredit = ($content[$i]->debet - $content[$i]->kredit)*-1;
                            echo "<td class='text-right'>".$debet."</td>";
                            echo "<td class='text-right'>".$kredit."</td>";
                        }
                    echo "</tr>";
                    $class_name = substr($content[$i]->nomor_account, 0, 1);
                    $sub_name = substr($content[$i]->nomor_account, 0, 2);
                    $total_sub_name_debet = $total_sub_name_debet + $debet;
                    $total_sub_name_kredit = $total_sub_name_kredit + $kredit;
                    $total_class_name_debet = $total_class_name_debet + $debet;
                    $total_class_name_kredit = $total_class_name_kredit + $kredit;
                }
                else if(strcmp(substr($content[$i]->nomor_account, 0, 1), $class_name) == 0 && strcmp(substr($content[$i]->nomor_account, 0, 2), $sub_name) == 0) {
                    echo "<tr>";
                        echo "<td></td>";
                        echo "<td class='text-right'>".$content[$i]->nomor_account."</td>";
                        echo "<td>".$content[$i]->nama_account."</td>";
                        if($content[$i]->debet - $content[$i]->kredit > 0) {
                            $debet = $content[$i]->debet - $content[$i]->kredit;
                            $kredit = 0;
                            echo "<td class='text-right'>".$debet."</td>";
                            echo "<td class='text-right'>".$kredit."</td>";
                        } else {
                            $debet = 0;
                            $kredit = ($content[$i]->debet - $content[$i]->kredit)*-1;
                            echo "<td class='text-right'>".$debet."</td>";
                            echo "<td class='text-right'>".$kredit."</td>";
                        }
                    echo "</tr>";
                    $class_name = substr($content[$i]->nomor_account, 0, 1);
                    $sub_name = substr($content[$i]->nomor_account, 0, 2);
                    $total_sub_name_debet = $total_sub_name_debet + $debet;
                    $total_sub_name_kredit = $total_sub_name_kredit + $kredit;
                    $total_class_name_debet = $total_class_name_debet + $debet;
                    $total_class_name_kredit = $total_class_name_kredit + $kredit;
                }
                $j++;
            }
            echo "<tr>";
                echo "<td></td>";
                echo "<td><strong>Total ".$content[$j - 1]->sub_name."</strong></td>";
                echo "<td></td>";
                echo "<td class='text-right'><strong>".$total_sub_name_debet."</strong></td>";
                echo "<td class='text-right'><strong>".$total_sub_name_kredit."</strong></td>";
            echo "</tr>";
            echo "<tr>";
                echo "<td><strong>Total ".$content[$j - 1]->class_name."</strong></td>";
                echo "<td></td>";
                echo "<td></td>";
                echo "<td class='text-right'><strong>".$total_class_name_debet."</strong></td>";
                echo "<td class='text-right'><strong>".$total_class_name_kredit."</strong></td>";
            echo "</tr>";*/
            for($i = 0; $i < sizeof($content); $i++) {
                if($content[$i]->nomor_account == ""){
                    continue;
                }
                else if(strcmp(substr($content[$i]->nomor_account, 0, 1), $class_name) != 0 && strcmp(substr($content[$i]->nomor_account, 0, 2), $sub_name) != 0 && $class_name == "0" && $sub_name == "0") {
                     echo "<tr>";
                        echo "<td><strong>".$content[$i]->class_name."</strong></td>";
                        echo "<td></td>";
                        echo "<td></td>";
                        echo "<td></td>";
                    echo "</tr>";
                    echo "<tr>";
                        echo "<td></td>";
                        echo "<td><strong>".$content[$i]->sub_name."</strong></td>";
                        echo "<td></td>";
                        echo "<td></td>";
                    echo "</tr>";
                    echo "<tr>";
                        echo "<td></td>";
                        echo "<td class='text-right'>".$content[$i]->nomor_account."</td>";
                        echo "<td>".$content[$i]->nama_account."</td>";
                        $value = $content[$i]->debet - $content[$i]->kredit;
                        echo "<td class='text-right'>".$value."</td>";
                    echo "</tr>";
                    $class_name = substr($content[$i]->nomor_account, 0, 1);
                    $sub_name = substr($content[$i]->nomor_account, 0, 2);
                    $total_sub_name = $total_sub_name + $value;
                    $total_class_name = $total_class_name + $value;
                }
                else if(strcmp(substr($content[$i]->nomor_account, 0, 1), $class_name) != 0 && strcmp(substr($content[$i]->nomor_account, 0, 2), $sub_name) != 0) {
                    echo "<tr>";
                        echo "<td></td>";
                        echo "<td><strong>Total ".$content[$i - 1]->sub_name."</strong></td>";
                        echo "<td></td>";
                        echo "<td class='text-right'><strong>$total_sub_name</strong></td>";
                    echo "</tr>";
                    $debet = 0;
                    $kredit = 0;
                    echo "<tr>";
                        echo "<td><strong>Total ".$content[$i - 1]->class_name."</strong></td>";
                        echo "<td></td>";
                        echo "<td></td>";
                        echo "<td class='text-right'><strong>".$total_class_name."</strong></td>";
                    echo "</tr>";
                    $total_class_name = 0;
                    echo "<tr>";
                        echo "<td> </td>";
                        echo "<td> </td>";
                        echo "<td> </td>";
                        echo "<td> </td>";
                    echo "</tr>";
                    echo "<tr>";
                        echo "<td> </td>";
                        echo "<td> </td>";
                        echo "<td> </td>";
                        echo "<td> </td>";
                    echo "</tr>";
                    echo "<tr>";
                        echo "<td><strong>".$content[$i]->class_name."</strong></td>";
                        echo "<td></td>";
                        echo "<td></td>";
                        echo "<td></td>";
                    echo "</tr>";
                    echo "<tr>";
                        echo "<td></td>";
                        echo "<td><strong>".$content[$i]->sub_name."</strong></td>";
                        echo "<td></td>";
                        echo "<td></td>";
                    echo "</tr>";
                    echo "<tr>";
                        echo "<td></td>";
                        echo "<td class='text-right'>".$content[$i]->nomor_account."</td>";
                        echo "<td>".$content[$i]->nama_account."</td>";
                        $value = $content[$i]->debet - $content[$i]->kredit;
                        echo "<td class='text-right'>".$value."</td>";
                    echo "</tr>";
                    $class_name = substr($content[$i]->nomor_account, 0, 1);
                    $sub_name = substr($content[$i]->nomor_account, 0, 2);
                    $total_sub_name = 0;
                    $total_class_name = $total_class_name + $value;
                }
                else if(strcmp(substr($content[$i]->nomor_account, 0, 1), $class_name) == 0 && strcmp(substr($content[$i]->nomor_account, 0, 2), $sub_name) != 0) {
                    if($total_sub_name == 0) {
                        $total_sub_name = $total_sub_name + $value;
                    }
                    echo "<tr>";
                        echo "<td></td>";
                        echo "<td><strong>Total ".$content[$i - 1]->sub_name."</strong></td>";
                        echo "<td></td>";
                        echo "<td class='text-right'><strong>".$total_sub_name."</strong></td>";
                    echo "</tr>";
                    $total_sub_name = 0;
                    echo "<tr>";
                        echo "<td> </td>";
                        echo "<td> </td>";
                        echo "<td> </td>";
                        echo "<td> </td>";
                    echo "</tr>";
                    echo "<tr>";
                        echo "<td></td>";
                        echo "<td><strong>".$content[$i]->sub_name."</strong></td>";
                        echo "<td></td>";
                        echo "<td></td>";
                    echo "</tr>";
                    echo "<tr>";
                        echo "<td></td>";
                        echo "<td class='text-right'>".$content[$i]->nomor_account."</td>";
                        echo "<td>".$content[$i]->nama_account."</td>";
                        $value = $content[$i]->debet - $content[$i]->kredit;
                        echo "<td class='text-right'>".$value."</td>";
                    echo "</tr>";
                    $class_name = substr($content[$i]->nomor_account, 0, 1);
                    $sub_name = substr($content[$i]->nomor_account, 0, 2);
                    $total_sub_name = $total_sub_name + $value;
                    $total_class_name = $total_class_name + $value;
                }
                else if(strcmp(substr($content[$i]->nomor_account, 0, 1), $class_name) == 0 && strcmp(substr($content[$i]->nomor_account, 0, 2), $sub_name) == 0) {
                    echo "<tr>";
                        echo "<td></td>";
                        echo "<td class='text-right'>".$content[$i]->nomor_account."</td>";
                        echo "<td>".$content[$i]->nama_account."</td>";
                        $value = $content[$i]->debet - $content[$i]->kredit;
                        echo "<td class='text-right'>".$value."</td>";
                    echo "</tr>";
                    $class_name = substr($content[$i]->nomor_account, 0, 1);
                    $sub_name = substr($content[$i]->nomor_account, 0, 2);
                    $total_sub_name = $total_sub_name + $value;
                    $total_class_name = $total_class_name + $value;
                }
                $j++;
            }
            echo "<tr>";
                echo "<td></td>";
                echo "<td><strong>Total ".$content[$j - 1]->sub_name."</strong></td>";
                echo "<td></td>";
                echo "<td class='text-right'><strong>".$total_sub_name."</strong></td>";
            echo "</tr>";
            echo "<tr>";
                echo "<td><strong>Total ".$content[$j - 1]->class_name."</strong></td>";
                echo "<td></td>";
                echo "<td></td>";
                echo "<td class='text-right'><strong>".$total_class_name."</strong></td>";
            echo "</tr>";
        echo "</tbody>";
    echo "</table>";
echo "</div>";

$x = ob_get_clean();


$response=new ResponsePackage();
$response->setStatus(ResponsePackage::$STATUS_OK);
//$response->setContent($x);
$response->setWarning(true,"Preview",$x);
echo json_encode($response->getPackage());
?>