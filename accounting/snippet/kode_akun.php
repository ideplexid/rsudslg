<?php 
global $db;
require_once "smis-base/smis-include-service-consumer.php";
$kode_akun   = $_POST['super_command'];
$uitable     = new Table(array("No.","Kode","Nama"));
$uitable->setName($kode_akun);
$uitable->setModel(Table::$SELECT);
if(isset($_POST['command'])){
    $adapter = new SimpleAdapter();
    $adapter ->add("Kode","nomor")
             ->add("Nama","nama")
             ->setUseNumber(true,"No.","back.");
    $dbtable = new DBTable($db,"smis_ac_account");
    $dbres   = new DBResponder($dbtable,$uitable,$adapter);
    $data    = $dbres->command($_POST['command']);
    echo json_encode($data);   
    return;
}
echo $uitable->getHtml();
?>