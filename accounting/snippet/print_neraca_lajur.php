<?php

global $db;
$dari=$_POST['from_date'];
$sampai=$_POST['to_date'];

$qv = "SELECT smis_ac_transaksi.id AS id, 
    smis_ac_transaksi.tanggal AS tanggal, 
    smis_ac_transaksi.jenis AS jenis, 
    smis_ac_transaksi.nomor AS nomor, 
    smis_ac_transaksi.keterangan AS keterangan, 
    smis_ac_transaksi_detail.id AS id_detail, 
    smis_ac_transaksi_detail.nomor_account AS nomor_account, 
    smis_ac_transaksi_detail.nama_account AS nama_account, 
    smis_ac_transaksi_detail.keterangan AS uraian,
            
    sum(if(jenis='saldo_awal' OR tanggal < '".$dari."',smis_ac_transaksi_detail.debet,0))  AS debet_saldo_awal, 
    sum(if(jenis='saldo_awal' OR tanggal < '".$dari."',smis_ac_transaksi_detail.kredit ,0)) AS kredit_saldo_awal,
    sum(if(jenis='transaction' AND tanggal >= '".$dari."',smis_ac_transaksi_detail.debet,0))  AS debet_transaksi, 
    sum(if(jenis='transaction' AND tanggal >= '".$dari."',smis_ac_transaksi_detail.kredit ,0)) AS kredit_transaksi,
    sum(if(jenis='memorial' AND tanggal >= '".$dari."',smis_ac_transaksi_detail.debet,0))  AS debet_memorial, 
    sum(if(jenis='memorial' AND tanggal >= '".$dari."',smis_ac_transaksi_detail.kredit ,0)) AS kredit_memorial,
    
    smis_ac_transaksi.prop AS prop
    FROM smis_ac_transaksi
    LEFT JOIN smis_ac_transaksi_detail 
    ON smis_ac_transaksi.id = smis_ac_transaksi_detail.id_transaksi 
    ";

$qc = "SELECT count(*) as total
    FROM smis_ac_transaksi
    LEFT JOIN smis_ac_transaksi_detail 
    ON smis_ac_transaksi.id = smis_ac_transaksi_detail.id_transaksi ";
        
$dbtable = new DBTable ( $db, "smis_ac_transaksi" );
$dbtable->setPreferredQuery ( true, $qv, $qc );
$dbtable->setUseWhereforView ( true );
$dbtable->setShowAll ( true );
$dbtable->setForceOrderForQuery ( true, " smis_ac_transaksi_detail.nomor_account ASC " ); // not work, in dbtable
$dbtable->setGroupBy ( true, " smis_ac_transaksi_detail.nomor_account" );
$dbtable->addCustomKriteria ( "smis_ac_transaksi.fix ", " ='0' " );
$dbtable->addCustomKriteria ( "smis_ac_transaksi_detail.prop ", " NOT LIKE '%del%'" );
if(isset($_POST['filter_grup']) && $_POST['filter_grup']!=""){
    $dbtable->addCustomKriteria ( " smis_ac_transaksi_detail.grup ", "='".$_POST['filter_grup']."'" );
}

$data = $dbtable->view("", 0);
$content = $data['data'];

echo "<div id='print_table_preview_neraca_lajur'>";
    echo "<h2 align='center'><strong>NERACA LAJUR</h2>";
    echo "<h4 align='center'><strong>".strtoupper(date("d F Y",strtotime($dari)))." - ".strtoupper(date("d F Y",strtotime($sampai)))."</h4>";
    echo "<table border='1' width='100%'>";
        echo "<tbody>";
            echo "<tr>";
                echo "<td align='center' rowspan='2' style='white-space: nowrap'><strong>Nomor Account</strong></td>";
                echo "<td align='center' rowspan='2' style='white-space: nowrap'><strong>Nama Account</strong></td>";
                echo "<td align='center' colspan='2' style='white-space: nowrap'><strong>Saldo Awal</strong></td>";
                echo "<td align='center' colspan='2' style='white-space: nowrap'><strong>Transaksi</strong></td>";
                echo "<td align='center' colspan='2' style='white-space: nowrap'><strong>Laba Rugi</strong></td>";
                echo "<td align='center' colspan='2' style='white-space: nowrap'><strong>Neraca</strong></td>";            echo "</tr>";
            echo "<tr>";
                echo "<td align='center'><strong>Debet</strong></td>";
                echo "<td align='center'><strong>Kredit</strong></td>";
                echo "<td align='center'><strong>Debet</strong></td>";
                echo "<td align='center'><strong>Kredit</strong></td>";
                echo "<td align='center'><strong>Debet</strong></td>";
                echo "<td align='center'><strong>Kredit</strong></td>";
                echo "<td align='center'><strong>Debet</strong></td>";
                echo "<td align='center'><strong>Kredit</strong></td>";
            echo "</tr>";
            $total_debet_saldo_awal = 0;
            $total_kredit_saldo_awal = 0;
            $total_debet_transaksi = 0;
            $total_kredit_transaksi = 0;
            $total_debet_neraca_saldo = 0;
            $total_kredit_neraca_saldo = 0;
            $total_debet_jurnal_umum = 0;
            $total_kredit_jurnal_umum = 0;
            $total_debet_neraca_saldo_disesuaikan = 0;
            $total_kredit_neraca_saldo_disesuaikan = 0;
            $total_debet_laba_rugi = 0;
            $total_kredit_laba_rugi = 0;
            $total_debet_neraca = 0;
            $total_kredit_neraca = 0;
            $total_debet_akumulasi_laba_rugi = 0;
            $total_kredit_akumulasi_laba_rugi = 0;
            $total_debet_akumulasi_neraca = 0;
            $total_kredit_akumulasi_neraca = 0;
            for($i = 0; $i < sizeof($content); $i++) {
                if($content[$i]->nomor_account == ""){
                    continue;
                } else {
                    echo "<tr>";
                        echo "<td>".$content[$i]->nomor_account."</td>";
                        echo "<td style='white-space: nowrap'>".$content[$i]->nama_account."</td>";
                        
                        echo "<td align='right'>".number_format($content[$i]->debet_saldo_awal,2,",",".")."</td>";
                        $total_debet_saldo_awal = $total_debet_saldo_awal + $content[$i]->debet_saldo_awal;
                        echo "<td align='right'>".number_format($content[$i]->kredit_saldo_awal,2,",",".")."</td>";
                        $total_kredit_saldo_awal = $total_kredit_saldo_awal + $content[$i]->kredit_saldo_awal;
                        
                        $debet_transaksi = $content[$i]->debet_transaksi + $content[$i]->debet_memorial;
                        echo "<td align='right'>".number_format($debet_transaksi,2,",",".")."</td>";
                        $total_debet_transaksi = $total_debet_transaksi + $debet_transaksi;
                        $kredit_transaksi = $content[$i]->kredit_transaksi + $content[$i]->kredit_memorial;
                        echo "<td align='right'>".number_format($kredit_transaksi,2,",",".")."</td>";
                        $total_kredit_transaksi = $total_kredit_transaksi + $kredit_transaksi;
                        
                        $debet_neraca_saldo = $content[$i]->debet_saldo_awal + $content[$i]->debet_transaksi;
                        $kredit_neraca_saldo = $content[$i]->kredit_saldo_awal + $content[$i]->kredit_transaksi;
                        $total_debet_neraca_saldo = $total_debet_neraca_saldo + $debet_neraca_saldo;
                        $total_kredit_neraca_saldo = $total_kredit_neraca_saldo + $kredit_neraca_saldo;
                        
                        $total_debet_jurnal_umum = $total_debet_jurnal_umum + $content[$i]->debet_memorial;
                        $total_kredit_jurnal_umum = $total_kredit_jurnal_umum + $content[$i]->kredit_memorial;
                        
                        $debet_neraca_saldo_disesuaikan = $debet_neraca_saldo + $content[$i]->debet_memorial;
                        $kredit_neraca_saldo_disesuaikan = $kredit_neraca_saldo + $content[$i]->kredit_memorial;
                        $total_debet_neraca_saldo_disesuaikan = $total_debet_neraca_saldo_disesuaikan + $debet_neraca_saldo_disesuaikan;
                        $total_kredit_neraca_saldo_disesuaikan = $total_kredit_neraca_saldo_disesuaikan + $kredit_neraca_saldo_disesuaikan;
                        if((substr($content[$i]->nomor_account, 0, 1)*1) >= 4){
                            echo "<td align='right'>".number_format($debet_neraca_saldo_disesuaikan,2,",",".")."</td>";
                            $total_debet_laba_rugi = $total_debet_laba_rugi + $debet_neraca_saldo_disesuaikan;
                            echo "<td align='right'>".number_format($kredit_neraca_saldo_disesuaikan,2,",",".")."</td>";
                            $total_kredit_laba_rugi = $total_kredit_laba_rugi + $kredit_neraca_saldo_disesuaikan;
                            echo "<td align='right'>".number_format("0",2,",",".")."</td>";
                            echo "<td align='right'>".number_format("0",2,",",".")."</td>";
                        } else {
                            echo "<td align='right'>".number_format("0",2,",",".")."</td>";
                            echo "<td align='right'>".number_format("0",2,",",".")."</td>";
                            echo "<td align='right'>".number_format($debet_neraca_saldo_disesuaikan,2,",",".")."</td>";
                            $total_debet_neraca = $total_debet_neraca + $debet_neraca_saldo_disesuaikan;
                            echo "<td align='right'>".number_format($kredit_neraca_saldo_disesuaikan,2,",",".")."</td>";
                            $total_kredit_neraca = $total_kredit_neraca + $kredit_neraca_saldo_disesuaikan;
                        }
                    echo "</tr>";
                }
            }
            echo "<tr>";
                echo "<td align='center' colspan='2'><strong>TOTAL</strong></td>";
                echo "<td align='right'><strong>".number_format($total_debet_saldo_awal,2,",",".")."</strong></td>";
                echo "<td align='right'><strong>".number_format($total_kredit_saldo_awal,2,",",".")."</strong></td>";
                echo "<td align='right'><strong>".number_format($total_debet_transaksi,2,",",".")."</strong></td>";
                echo "<td align='right'><strong>".number_format($total_kredit_transaksi,2,",",".")."</strong></td>";
                echo "<td align='right'><strong>".number_format($total_debet_laba_rugi,2,",",".")."</strong></td>";
                echo "<td align='right'><strong>".number_format($total_kredit_laba_rugi,2,",",".")."</strong></td>";
                echo "<td align='right'><strong>".number_format($total_debet_neraca,2,",",".")."</strong></td>";
                echo "<td align='right'><strong>".number_format($total_debet_neraca,2,",",".")."</strong></td>";                
            echo "</tr>";
        echo "</tbody>";
    echo "</table>";
echo "</div>";

?>