<?php

global $db;
require_once 'accounting/class/adapter/WorksheetAdapter.php';
require_once 'accounting/function/get_data_laba_rugi.php';
$dari    = $_POST['from_date'];
$sampai  = $_POST['to_date'];
$result  = get_data_laba_rugi($db,$dari,$sampai,$_POST['filter_grup']);
$adapter = new WorksheetAdapter();
$content = $adapter->getContent($result);

require_once ("smis-libs-out/php-excel/PHPExcel.php");
$file = new PHPExcel ();
$file->getProperties ()->setCreator ( "PT. Inovasi Ide Utama" );
$file->getProperties ()->setLastModifiedBy ( "Operator" );
$file->getProperties ()->setTitle ( "Laporan Laba Rugi" );
$file->getProperties ()->setSubject ( "Laporan Laba Rugi" );
$file->getProperties ()->setDescription ( "Laporan Laba Rugi" );
$file->getProperties ()->setKeywords ( "Laporan Laba Rugi" );
$file->getProperties ()->setCategory ( "Laporan Laba Rugi" );
$sheet = $file->getActiveSheet ();

$row = 1;
$sheet->setCellValue("A".$row, "LAPORAN LABA RUGI");
$sheet->mergeCells('A'.$row.':'.'D'.$row);
$sheet->getStyle('A'.$row.':'.'D'.$row)->getFont()->setBold(true);
$sheet->getStyle('A'.$row.':'.'D'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$sheet->getStyle('A'.$row.':'.'D'.$row)->getFont()->setSize(18);
$sheet->getStyle('A'.$row.':'.'D'.$row)->getFont()->getColor()->setRGB('#0000FF');
$row++;
$sheet->setCellValue("A".$row, strtoupper(date("F Y",strtotime($sampai))));
$sheet->mergeCells('A'.$row.':'.'D'.$row);
$sheet->getStyle('A'.$row.':'.'D'.$row)->getFont()->setBold(true);
$sheet->getStyle('A'.$row.':'.'D'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$sheet->getStyle('A'.$row.':'.'D'.$row)->getFont()->setSize(12);
$sheet->getStyle('A'.$row.':'.'D'.$row)->getFont()->getColor()->setRGB('#0000FF');

$row             = $row + 2;
$first           = $row;
$total_lbr    = 0;
$total_cur_class = 0;
$total_cur_sub   = 0;    
$cur_class       = null;
$cur_sub         = null;
$total_item      = count($content);

foreach($content as $x){
    $row++;
    $total_item--;
    $class  = $x['Class Name'];
    $sub    = $x['Sub Name'];
    $nomor  = $x['Nomor Account'];
    $nama   = $x['Nama Account'];
    $nilai  = $x['labarugi'];
    $total_lbr += $nilai;
    
    if($cur_class==null || $cur_class!=$class){
        if($cur_class!=null){
            $sheet ->setCellValue("B".$row, "Total ".$sub);
            $sheet ->setCellValue("D".$row, $total_cur_sub);   
            $sheet ->getStyle('A'.$row.':D'.$row)
                   ->getFont()
                   ->setBold(true);
            
            $row++;
            $sheet ->setCellValue("A".$row, "Total ".$class);
            $sheet ->setCellValue("D".$row, $total_cur_class);
            $sheet ->getStyle('A'.$row.':D'.$row)
                   ->getFont()
                   ->setBold(true);
            $total_cur_class = 0;
            $total_cur_sub = 0;
            $row++;
        }        

        $cur_class = $class;
        $cur_sub   = $sub;
        $sheet ->setCellValue("A".$row, $cur_class);
        $sheet ->getStyle('A'.$row.':D'.$row)
               ->getFont()
               ->setBold(true);
        $row++;
        $sheet ->setCellValue("B".$row, $cur_sub);
        $sheet ->getStyle('A'.$row.':D'.$row)
               ->getFont()
               ->setBold(true);
        $row++;        
    }
    
    if($cur_sub==null || $cur_sub!=$sub){
        if($cur_sub!=null){
            $sheet ->setCellValue("B".$row, "Total ".$sub);
            $sheet ->setCellValue("D".$row, $total_cur_sub);
            $sheet ->getStyle('A'.$row.':D'.$row)
                   ->getFont()
                   ->setBold(true);  
            $total_cur_sub = 0;
        }
        $row++;
        $cur_sub   = $sub;
        $sheet  ->setCellValue("B".$row, $sub);
        $sheet  ->getStyle('A'.$row.':D'.$row)
                ->getFont()
                ->setBold(true); 
        $row++;
    }    
    $total_cur_class += $nilai;
    $total_cur_sub   += $nilai;
    $sheet->setCellValue("B".$row, " ".$nomor);
    $sheet->setCellValue("C".$row, $nama);
    $sheet->setCellValue("D".$row, $nilai);
}
$row++;
$sheet ->setCellValue("B".$row, "Total ".$sub);
$sheet ->setCellValue("D".$row, $total_cur_sub);   
$sheet ->getStyle('A'.$row.':D'.$row)
       ->getFont()
       ->setBold(true);

$row++;
$sheet ->setCellValue("A".$row, "Total ".$class);
$sheet ->setCellValue("D".$row, $total_cur_class);
$sheet ->getStyle('A'.$row.':D'.$row)
       ->getFont()
       ->setBold(true);
$total_cur_class = 0;
$total_cur_sub = 0;
$row++;

$sheet ->setCellValue("A".$row, "Total Laba Rugi");
$sheet ->setCellValue("D".$row, $total_lbr);
$sheet ->getStyle('A'.$row.':D'.$row)
       ->getFont()
       ->setBold(true);
$row++;

$sheet  ->getStyle("D".$first.":D".$row)
        ->getNumberFormat()
        ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
$sheet  ->getStyle("A".$first.":C".$row)
        ->getNumberFormat()
        ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
        
foreach(range('A','D') as $columnID) {
    $sheet ->getColumnDimension($columnID)
           ->setAutoSize(true);
}


header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="LAPORAN NERACA.xls"');
header('Cache-Control: max-age=0');
$writer = PHPExcel_IOFactory::createWriter($file,'Excel5');
$writer->save('php://output');
return;

header ( 'Content-Type: application/vnd.ms-excel' );
header ( 'Content-Disposition: attachment;filename="LAPORAN LABA RUGI.xls"' );
header ( 'Cache-Control: max-age=0' );
$writer = PHPExcel_IOFactory::createWriter ( $file, 'Excel5' );
$writer->save ( 'php://output' );
return;

?>