<?php

global $db;
$dari   = $_POST['from_date'];
$sampai = $_POST['to_date'];
$akun   = $_POST['account'];

$qv = " SELECT smis_ac_transaksi.id AS id, 
        smis_ac_transaksi.tanggal AS tanggal, 
        smis_ac_transaksi.jenis AS jenis, 
        smis_ac_transaksi.nomor AS nomor, 
        smis_ac_transaksi.code AS code, 
        smis_ac_transaksi.keterangan AS keterangan, 
        smis_ac_transaksi_detail.id AS id_detail, 
        smis_ac_transaksi_detail.nomor_account AS nomor_account, 
        smis_ac_transaksi_detail.nama_account AS nama_account, 
        smis_ac_transaksi_detail.keterangan AS uraian, 
        smis_ac_transaksi_detail.debet AS debet, 
        smis_ac_transaksi_detail.kredit AS kredit,
        smis_ac_transaksi_detail.grup AS grup,
        smis_ac_transaksi.prop AS prop
        FROM smis_ac_transaksi
        LEFT JOIN smis_ac_transaksi_detail 
        ON smis_ac_transaksi.id = smis_ac_transaksi_detail.id_transaksi 
        ";

$qc = " SELECT count(*) as total
        FROM smis_ac_transaksi
        LEFT JOIN smis_ac_transaksi_detail 
        ON smis_ac_transaksi.id = smis_ac_transaksi_detail.id_transaksi ";
        
$column = array ();
$dbtable = new DBTable ( $db, "smis_ac_transaksi", $column );
$dbtable->setPreferredQuery ( true, $qv, $qc );
$dbtable->setUseWhereforView ( true );
$dbtable->setShowAll(true);
$dbtable->addCustomKriteria(" smis_ac_transaksi.tanggal >= " ,"'".$dari."'");
$dbtable->addCustomKriteria(" smis_ac_transaksi.tanggal <= " ,"'".$sampai."'");
$dbtable->addCustomKriteria ( "smis_ac_transaksi.fix ", " ='0' " );
$dbtable->addCustomKriteria ( "smis_ac_transaksi_detail.prop ", " NOT LIKE '%del%'" );
if (isset ( $_POST ['account'] )) {
    if(isset($_POST['all_child']) && $_POST['all_child'] =="0"){
        $dbtable->addCustomKriteria ( "smis_ac_transaksi_detail.nomor_account ", " ='" . $_POST ['account'] . "'" );
    }else{
        $dbtable->addCustomKriteria ( "smis_ac_transaksi_detail.nomor_account ", " LIKE '" . $_POST ['account'] . "%'" );
    }
}

$data = $dbtable->view("", 0);
$content = $data['data'];
$summary = 0;
if(substr($akun,0,1)*1<4){
    $query="SELECT SUM(smis_ac_transaksi_detail.debet) as debet, SUM(smis_ac_transaksi_detail.kredit) as kredit 
            FROM smis_ac_transaksi
            RIGHT JOIN smis_ac_transaksi_detail 
            ON smis_ac_transaksi.id = smis_ac_transaksi_detail.id_transaksi
            WHERE smis_ac_transaksi.tanggal<'".$dari."' 
            AND smis_ac_transaksi.prop!='del'
            AND smis_ac_transaksi_detail.prop!='del' 
            AND nomor_account LIKE '".$akun."%'
            ";
    $hasil = $db->get_row($query);
    $summary=0;
    if($hasil!=null){
        $summary=$hasil->debet-$hasil->kredit;
    }    
}

$dbt = new DBTable($db,"smis_ac_account");
$a = $dbt->select(array("nomor"=>$akun));

echo "<div>";
    echo "<button type='button' class='btn btn-primary' onclick='preview_ledger.back()'>Back</button>";
    echo "&nbsp";
    echo "<button type='button' class='btn btn-primary' onclick='preview_ledger.cetak()'>Print</button>";
echo "</div>";

echo "<div id='print_table_preview_ledger'>";
    echo "<h2 align='center'><strong>BUKU BESAR</h2>";
    $periode = ArrayAdapter::dateFormat("date d M Y H:i",$dari)." - ".ArrayAdapter::dateFormat("date d M Y H:i",$sampai);
    echo "<h4 align='center'><strong>".strtoupper($periode)."</h4>";
    echo "<div class='table_ledger' border='1'>";
        echo "<div class='thead' >";
            echo "<div class='tr'>";
                echo "<div class='td noaccount' align='center' ><strong>".$a->nomor."</strong></div>";
                echo "<div class='td namaaccount' align='center' ><strong>".strtoupper($a->nama)."</strong></div>";
                echo "<div class='td saldo' align='center' ><strong>SALDO</strong></div>";
            echo "</div>";
            echo "<div class='tr'>";
                echo "<div class='td no last' align='center' ><strong>No.</strong></div>";
                echo "<div class='td tanggal last' align='center' ><strong>Tanggal</strong></div>";
                echo "<div class='td code last' align='center' ><strong>Code</strong></div>";
                echo "<div class='td keterangan last' align='center' ><strong>Keterangan</strong></div>";
                echo "<div class='td dept last' align='center' ><strong>Dept.</strong></div>";
                echo "<div class='td nilai last' align='center' ><strong>Debet</strong></div>";
                echo "<div class='td nilai last' align='center' ><strong>Kredit</strong></div>";
                echo "<div class='td nilai last' align='center' ><strong>Debet</strong></div>";
                echo "<div class='td nilai last' align='center' ><strong>Kredit</strong></div>";
            echo "</div>";
        echo "</div>";
        echo "<div class='tbody'>";
            $no = 1;
            if($summary>0) {
                echo "<div class='tr'>";
                    echo "<div class='td no' align='center'>".$no."</div>";
                    echo "<div class='td tanggal'> - </div>";
                    echo "<div class='td code'> - </div>";
                    echo "<div class='td keterangan'>Saldo Awal Bulan Berjalan</div>";
                    echo "<div class='td dept'> - </div>";
                    echo "<div class='td nilai' align='right' >".number_format(0,2,",",".")."</div>";
                    echo "<div class='td nilai' align='right' >".number_format(0,2,",",".")."</div>";
                    if($summary > 0) {
                        echo "<div class='td nilai' align='right' >".number_format($summary,2,",",".")."</div>";
                    } else {
                        echo "<div class='td nilai' align='right' >".number_format(0,2,",",".")."</div>";
                    }
                    if($summary < 0) {
                        $v = abs($summary);
                        echo "<div class='td nilai' align='right' >".number_format($v,2,",",".")."</div>";
                    } else {
                        echo "<div class='td nilai' align='right' >".number_format(0,2,",",".")."</div>";
                    }
                echo "</div>";
                $no++;
            }
            $total_transaksi_debet = 0;
            $total_transaksi_kredit = 0;
            $total_saldo_debet = 0;
            $total_saldo_kredit = 0;
            foreach($content as $c) {
                echo "<div class='tr'>";
                    $summary = $summary + $c->debet - $c->kredit;  
                    echo "<div class='td no' align='center'>".$no."</div>";
                    if($c->tanggal == NULL) {
                        echo "<div class='td tanggal'>&nbsp;</div>";
                    } else {
                        echo "<div class='td tanggal'>".ArrayAdapter::dateFormat("date d M Y",$c->tanggal)."</div>";
                    }
                    if($c->nomor == NULL) {
                        echo "<div class='td code'>&nbsp;</div>";
                    } else {
                        echo "<div class='td code'>".$c->nomor."</div>";
                    }
                    if($c->keterangan == NULL) {
                        echo "<div class='td keterangan'>&nbsp;</div>";
                    } else {
                        echo "<div class='td keterangan'>".$c->keterangan."</div>";
                    }
                    if($c->grup == NULL) {
                        echo "<div class='td dept'>&nbsp;</div>";
                    } else {
                        echo "<div class='td dept'>".$c->grup."</div>";
                    }
                    if($c->jenis!="saldo_awal") {
                        $c_debet = $c->debet;
                        echo "<div class='td nilai' align='right' >".number_format($c_debet,2,",",".")."</div>";
                        $total_transaksi_debet = $total_transaksi_debet + $c->debet;
                        $c_kredit = $c->kredit;
                        echo "<div class='td nilai' align='right' >".number_format($c_kredit,2,",",".")."</div>";
                        $total_transaksi_kredit = $total_transaksi_kredit + $c->kredit;
                    } else {
                        echo "<div class='td nilai' align='right' >".number_format(0,2,",",".")."</div>";
                        echo "<div class='td nilai' align='right' >".number_format(0,2,",",".")."</div>";
                    }
                    if($summary > 0) {
                        echo "<div class='td nilai' align='right' >".number_format($summary,2,",",".")."</div>";
                        $total_saldo_debet = $total_saldo_debet + $summary;
                    } else {
                        echo "<div class='td nilai' align='right' >".number_format(0,2,",",".")."</div>";
                    }
                    if($summary < 0) {
                        $v = abs($summary);
                        echo "<div class='td nilai' align='right' >".number_format($v,2,",",".")."</div>";
                        $total_saldo_kredit = $total_saldo_kredit + $v;
                    } else {
                        echo "<div class='td nilai' align='right' >".number_format(0,2,",",".")."</div>";
                    }
                echo "</div>";
                $no++;
            }
            echo "</div>";
            echo "<div class='tfoot'>";
                echo "<div class='tr'>";
                    echo "<div class='td footkiri'>&nbsp;</div>";
                    echo "<div class='td dept'>Total</div>";
                    echo "<div class='td nilai' align='right' >".number_format($total_transaksi_debet,2,",",".")."</div>";
                    echo "<div class='td nilai' align='right' >".number_format($total_transaksi_kredit,2,",",".")."</div>";
                    echo "<div class='td footkanan'>&nbsp;</div>";
                echo "</div>";
                echo "<div class='tr'>";
                    echo "<div class='td footkiri'>&nbsp;</div>";
                    echo "<div class='td dept'>Mutasi</div>";
                    if($total_transaksi_debet > $total_transaksi_kredit) {
                        $mutasi_debet = $total_transaksi_debet - $total_transaksi_kredit;
                        echo "<div class='td nilai' align='right' >".number_format($mutasi_debet,2,",",".")."</div>";
                    } else {
                        $mutasi_debet = 0;
                        echo "<div class='td nilai' align='right' >".number_format($mutasi_debet,2,",",".")."</div>";
                    }
                    if($total_transaksi_kredit > $total_transaksi_debet) {
                        $mutasi_kredit = $total_transaksi_kredit - $total_transaksi_debet;
                        echo "<div class='td nilai' align='right' >".number_format($mutasi_kredit,2,",",".")."</div>";
                    } else {
                        $mutasi_kredit = 0;
                        echo "<div class='td nilai' align='right' >".number_format($mutasi_kredit,2,",",".")."</div>";
                    }
                    echo "<div class='td footkanan'>&nbsp;</div>";
                echo "</div>";
                echo "<div class='tr'>";
                    echo "<div class='td footkiri'>&nbsp;</div>";
                    echo "<div class='td dept'>Saldo Awal</div>";
                    echo "<div class='td nilai' align='right' >".number_format($total_saldo_debet,2,",",".")."</div>";
                    echo "<div class='td nilai' align='right' >".number_format($total_saldo_kredit,2,",",".")."</div>";
                    echo "<div class='td footkanan'>&nbsp;</div>";
                echo "</div>";
                echo "<div class='tr'>";
                    echo "<div class='td footkiri'>&nbsp;</div>";
                    echo "<div class='td dept'>Saldo Akhir</div>";
                    if(($mutasi_debet + $total_saldo_debet) > ($mutasi_kredit + $total_saldo_kredit)) {
                        $total_saldo_akhir_debet = ($mutasi_debet + $total_saldo_debet) - ($mutasi_kredit + $total_saldo_kredit);
                        echo "<div class='td nilai' align='right' >".number_format($total_saldo_akhir_debet,2,",",".")."</div>";
                    } else {
                        $total_saldo_akhir_debet = 0;
                        echo "<div class='td nilai' align='right' >".number_format($total_saldo_akhir_debet,2,",",".")."</div>";
                    }
                    if(($mutasi_debet + $total_saldo_debet) < ($mutasi_kredit + $total_saldo_kredit)) {
                        $total_saldo_akhir_kredit = ($mutasi_kredit + $total_saldo_kredit) - ($mutasi_debet + $total_saldo_debet);
                        echo "<div class='td nilai' align='right' >".number_format($total_saldo_akhir_kredit,2,",",".")."</div>";
                    } else {
                        $total_saldo_akhir_kredit = 0;
                        echo "<div class='td nilai' align='right' >".number_format($total_saldo_akhir_kredit,2,",",".")."</div>";
                    }
                    echo "<div class='td footkanan'>&nbsp;</div>";
                echo "</div>";
            echo "</div>";
        echo "</div>";
    echo "</div>";
echo "</div>";

$dari        = new Hidden("preview_ledger_dari","",$_POST['from_date']);
$sampai      = new Hidden("preview_ledger_sampai","",$_POST['to_date']);
$akun        = new Hidden("preview_ledger_akun","",$_POST['account']);
$grup        = new Hidden("preview_ledger_grup","",$_POST['filter_grup']);
$child       = new Hidden("preview_ledger_child","",$_POST['all_child']);
$form        = new Hidden("preview_ledger_form","",$_POST['format']);
echo $dari   ->getHtml();
echo $sampai ->getHtml();
echo $akun   ->getHtml();
echo $grup   ->getHtml();
echo $child  ->getHtml();
echo $form   ->getHtml();
echo addJS  ("accounting/resource/js/preview_ledger.js",false);
echo addCSS ("accounting/resource/css/preview_ledger.css",false);

?>