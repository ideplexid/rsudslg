<?php 
global $db;
require_once "smis-base/smis-include-service-consumer.php";
$dbtable            = new DBTable($db,"smis_ac_notify");
$notify             = $dbtable->select($_POST['id']);
$datalist['data']   = $notify->data;
$serv               = new ServiceConsumer($db,$notify->service,$datalist,$notify->entity);
$serv->execute();
$content            = $serv->getContent();
$dbacc              = new DBTable($db,"smis_ac_account");
$_cache_acc         = array();
$table              = new TablePrint("");
$table->setDefaultBootrapClass(true);
$table->setMaxWidth(false);



foreach($content as $transaction){
    $dbtable->setName("smis_ac_transaksi");
    $header             = $transaction['header'];
    $detail_transaksi   = $transaction['content'];
    
    $table  ->addColumn("TRANSAKSI NOMOR ",3,1)
            ->addColumn(ArrayAdapter::digitFormat("digit8",$_POST['id']),3,1)
            ->commit("title");
    $table  ->addColumn("Tanggal ",2,1)
            ->addColumn(ArrayAdapter::dateFormat(" date d M Y H:i",$header['tanggal']),4,1)
            ->commit("body");
    $table  ->addColumn("Keterangan ",2,1)
            ->addColumn($header['keterangan'],4,1)
            ->commit("body");
    $table  ->addColumn("Nomor",2,1)
            ->addColumn($header['nomor'],4,1)
            ->commit("body");
    $table  ->addColumn("Grup",2,1)
            ->addColumn(isset($header['grup'])?ArrayAdapter::format("unslug",$header['grup']):"",4,1)
            ->commit("body");
    
    $table  ->addColumn("<strong>No.</strong>",1,1)
            ->addColumn("<strong>Nomor</strong>",1,1)
            ->addColumn("<strong>Kode</strong>",1,1)
            ->addColumn("<strong>Keterangan</strong>",1,1)
            ->addColumn("<strong>Debet</strong>",1,1)
            ->addColumn("<strong>Kredit</strong>",1,1)
            ->commit("body");
    $no=0;
    foreach($detail_transaksi as $detail){
        $kode_akun=$detail['akun'];
        if(!isset($_cache_acc[$kode_akun])){
            $x=$dbacc->select(array("nomor"=>$kode_akun));
            if($x!=null){
                $_cache_acc[$kode_akun]=$x->nama;
            }
        }
        
        $no++;
        $table  ->addColumn($no.".",1,1)
                ->addColumn($kode_akun,1,1)
                ->addColumn($_cache_acc[$kode_akun],1,1)
                ->addColumn($detail['ket'],1,1)
                ->addColumn(ArrayAdapter::moneyFormat("only-money",$detail['debet']),1,1)
                ->addColumn(ArrayAdapter::moneyFormat("only-money",$detail['kredit']),1,1)
                ->commit("body");
    }    
    $table  ->addColumn("Total",4,1)
            ->addColumn(ArrayAdapter::moneyFormat("only-money",$header['debet']),1,1)
            ->addColumn(ArrayAdapter::moneyFormat("only-money",$header['kredit']),1,1)
            ->commit("footer");
}

$response=new ResponsePackage();
$response->setStatus(ResponsePackage::$STATUS_OK);
$response->setContent($table->getHtml());
echo json_encode($response->getPackage());
?>