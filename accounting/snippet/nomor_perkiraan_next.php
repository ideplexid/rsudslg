<?php 
$id_induk = $_POST['id_induk'];
$query = "SELECT ifnull(max(nomor),-1) as total FROM smis_ac_account WHERE id_parent = '".$id_induk."' AND prop!='del'";
$max = $db->get_var($query);
$response = new ResponsePackage();
$response ->setStatus(ResponsePackage::$STATUS_OK);
$response ->setContent($max);       

if($max*1=="-1"){
    $response ->setContent($_POST['nomor_induk']);  
    $response ->setAlertVisible(true);  
    $response ->setAlertContent("Data Tidak Ditemukan","System tidak dapat menemukan data increment parent ini",Alert::$DANGER);
}else{
    if(strpos($max,".")!==false){
        $max = substr($max,strlen($_POST['nomor_induk'])+1);
        $length = strlen($max);
        $max = $max*1+1;
        $max = ArrayAdapter::format("only-digit".$length,$max);
        $max = $_POST['nomor_induk'].".".$max; 
    }else{
        $max = $max*1+1;
    }
    $response ->setContent($max);       
}
echo json_encode($response->getPackage());