<?php 

global $db;
require_once 'accounting/class/adapter/WorksheetAdapter.php';
require_once 'accounting/function/get_data_neraca.php';
$dari    = $_POST['from_date'];
$sampai  = $_POST['to_date'];
$result  = get_data_neraca($db,$dari,$sampai,$_POST['filter_grup']);

$adapter = new WorksheetAdapter();
$adapter ->setFooterEnabled(false);
$content = $adapter->getContent($result);

echo "<div>";
    echo "<button type='button' class='btn btn-primary' onclick='preview_neraca.back()'>Back</button>";
    echo "&nbsp";
    echo "<button type='button' class='btn btn-primary' onclick='preview_neraca.cetak()'>Print</button>";
echo "</div>";

echo "<div id='print_table_preview_neraca'>";
    echo "<h2 align='center'><strong>NERACA</strong></h2>";
    echo "<h4 align='center'><strong>".strtoupper(date("F Y",strtotime($sampai)))."</strong></h4>";
    
    $total_pasiva       = 0;
    $total_cur_class    = 0;
    $total_cur_sub      = 0;    
    $cur_class          = null;
    $cur_sub            = null;
    
    $table      = new TablePrint("");
    $table->setDefaultBootrapClass(false);
    $table->setMaxWidth(true);
    $total_item = count($content);
    foreach($content as $x){
        $total_item--;
        $class  = $x['Class Name'];
        $sub    = $x['Sub Name'];
        $nomor  = $x['Nomor Account'];
        $nama   = $x['Nama Account'];
        $nilai  = $x['neraca'];
        $dk     = $x['dk'];
        if(!startsWith($nomor,"1")){
            $total_pasiva += $nilai;
        }
        if($cur_class==null || $cur_class!=$class){
            if($cur_class!=null){        
                $table  ->addColumn("",1,1)
                        ->addColumn("Total ".$cur_sub,2,1,null,null,"subclass")
                        ->addColumn(ArrayAdapter::format("money Rp.",$total_cur_sub),1,1,null,null,"subclass")
                        ->commit("body");
                $table  ->addColumn("Total ".$cur_class,3,1,null,null,"supclass")
                        ->addColumn(ArrayAdapter::format("money Rp.",$total_cur_class),1,1,null,null,"supclass")
                        ->commit("body");
                $total_cur_class  = 0;
                $total_cur_sub    = 0;
                $table  ->addSpace(4,1,"body");
            }
            $cur_class  = $class;
            $cur_sub    = $sub;
            $table      ->addColumn($cur_class,4,1,null,null,"supclass")
                        ->commit("body");
            $table      ->addColumn("",1,1)
                        ->addColumn($cur_sub,3,1,null,null,"subclass")
                        ->commit("body");
        }
        
        if($cur_sub==null || $cur_sub!=$sub){
            if($cur_sub!=null){
                $table  ->addColumn("",1,1)
                        ->addColumn("Total ".$cur_sub,2,1,null,null,"subclass")
                        ->addColumn(ArrayAdapter::format("money Rp.",$total_cur_sub),1,1,null,null,"subclass")
                        ->commit("body");
                $total_cur_sub = 0;
            }
            $table  ->addSpace(4,1,"body");
            $cur_sub    = $sub;
            $table      ->addColumn("",1,1)
                        ->addColumn($cur_sub,3,1,null,null,"subclass")
                        ->commit("body");
        }
        
        $total_cur_class += $nilai;
        $total_cur_sub   += $nilai;
 
        $table  ->addColumn("",1,1)
                ->addColumn($nomor,1,1)
                ->addColumn($nama,1,1)
                ->addColumn(ArrayAdapter::format("money Rp.",$nilai),1,1)
                ->commit("body");
    }
    
    $table  ->addColumn("",1,1)
            ->addColumn("Total ".$cur_sub,2,1,null,null,"subclass")
            ->addColumn(ArrayAdapter::format("money Rp.",$total_cur_sub),1,1,null,null,"subclass")
            ->commit("body");
    $table  ->addColumn("Total ".$cur_class,3,1,null,null,"supclass")
            ->addColumn(ArrayAdapter::format("money Rp.",$total_cur_class),1,1,null,null,"supclass")
            ->commit("body");    
    $table  ->addColumn("Total Pasiva",3,1,null,null,"motherclass")
            ->addColumn(ArrayAdapter::format("money Rp.",$total_pasiva),1,1,null,null,"motherclass")
            ->commit("body");
echo $table ->getHtml();
echo "</div>";
    
$dari   = new Hidden("preview_neraca_dari","",$_POST['from_date']);
$sampai = new Hidden("preview_neraca_sampai","",$_POST['to_date']);
$grup   = new Hidden("preview_neraca_grup","",$_POST['filter_grup']);
$format = new Hidden("preview_neraca_format","",$_POST['filter_format']);

echo $dari      ->getHtml();
echo $sampai    ->getHtml();
echo $grup      ->getHtml();
echo $format    ->getHtml();
echo addJS   ("accounting/resource/js/preview_neraca.js",false);
echo addCSS  ("accounting/resource/css/preview_neraca.css",false);
?>