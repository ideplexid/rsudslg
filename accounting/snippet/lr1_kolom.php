<?php 

global $db;
$dari=$_POST['from_date'];
$sampai=$_POST['to_date'];
$akun=$_POST['akun']=="laba_rugi"?" >='4' ":" <'4' ";

$qv = " SELECT smis_ac_transaksi.id AS id, 
		smis_ac_transaksi.tanggal AS tanggal, 
		smis_ac_transaksi.jenis AS jenis, 
		smis_ac_transaksi.nomor AS nomor, 
		smis_ac_transaksi.keterangan AS keterangan, 
		smis_ac_transaksi_detail.id AS id_detail, 
		smis_ac_transaksi_detail.nomor_account AS nomor_account, 
		smis_ac_transaksi_detail.nama_account AS nama_account, 
		smis_ac_transaksi_detail.keterangan AS uraian,
		sum(smis_ac_transaksi_detail.debet)  AS debet, 
		sum(smis_ac_transaksi_detail.kredit) AS kredit,
        smis_ac_account.sub as sub_name,
        smis_ac_account.klas as class_name,
		smis_ac_transaksi.prop AS prop
		FROM smis_ac_transaksi
		LEFT JOIN smis_ac_transaksi_detail 
		ON smis_ac_transaksi.id = smis_ac_transaksi_detail.id_transaksi 
        LEFT JOIN smis_ac_account ON smis_ac_transaksi_detail.nomor_account=smis_ac_account.nomor ";
	
	$qc = " SELECT count(*) as total
			FROM smis_ac_transaksi
			LEFT JOIN smis_ac_transaksi_detail 
			ON smis_ac_transaksi.id = smis_ac_transaksi_detail.id_transaksi ";
            
$dbtable = new DBTable ( $db, "smis_ac_transaksi" );
$dbtable->setPreferredQuery ( true, $qv, $qc );
$dbtable->setUseWhereforView ( true );
$dbtable->setShowAll ( true );
$dbtable->setForceOrderForQuery ( true, " smis_ac_transaksi_detail.nomor_account ASC " ); // not work, in dbtable
$dbtable->setGroupBy ( true, " smis_ac_transaksi_detail.nomor_account" );
$dbtable->addCustomKriteria ( "smis_ac_transaksi.fix ", " ='0' " );
$dbtable->addCustomKriteria ( "smis_ac_transaksi_detail.nomor_account ", " ".$akun." " );
$dbtable->addCustomKriteria ( "smis_ac_transaksi.tanggal ", " < '".$sampai."' " );

if(isset($_POST['filter_grup']) && $_POST['filter_grup']!=""){
    $dbtable->addCustomKriteria ( " smis_ac_transaksi_detail.grup ", "='".$_POST['filter_grup']."'" );
}

$data = $dbtable->view("", 0);
$content = $data['data'];

if($_POST['akun'] == "laba_rugi") {
    require_once ("smis-libs-out/php-excel/PHPExcel.php");
    $file = new PHPExcel ();
    $file->getProperties ()->setCreator ( "PT. Inovasi Ide Utama" );
    $file->getProperties ()->setLastModifiedBy ( "Operator" );
    $file->getProperties ()->setTitle ( "Laporan Laba Rugi" );
    $file->getProperties ()->setSubject ( "Laporan Laba Rugi" );
    $file->getProperties ()->setDescription ( "Laporan Laba Rugi" );
    $file->getProperties ()->setKeywords ( "Laporan Laba Rugi" );
    $file->getProperties ()->setCategory ( "Laporan Laba Rugi" );
    $sheet = $file->getActiveSheet ();
}else{
    require_once ("smis-libs-out/php-excel/PHPExcel.php");
    $file = new PHPExcel ();
    $file->getProperties ()->setCreator ( "PT. Inovasi Ide Utama" );
    $file->getProperties ()->setLastModifiedBy ( "Operator" );
    $file->getProperties ()->setTitle ( "Neraca" );
    $file->getProperties ()->setSubject ( "Neraca" );
    $file->getProperties ()->setDescription ( "Neraca" );
    $file->getProperties ()->setKeywords ( "Neraca" );
    $file->getProperties ()->setCategory ( "Neraca" );
    $sheet = $file->getActiveSheet ();
}


$row = 1;
$sheet->setCellValue("A".$row, ArrayAdapter::format("unslug",$_POST['akun']));
$sheet->mergeCells('A'.$row.':'.'D'.$row);
$sheet->getStyle('A'.$row.':'.'D'.$row)->getFont()->setBold(true);
$sheet->getStyle('A'.$row.':'.'D'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$sheet->getStyle('A'.$row.':'.'D'.$row)->getFont()->setSize(18);
$sheet->getStyle('A'.$row.':'.'D'.$row)->getFont()->getColor()->setRGB('#0000FF');
$row++;
$sheet->setCellValue("A".$row, strtoupper(date("d F Y",strtotime($dari)))." - ".strtoupper(date("d F Y",strtotime($sampai))));
$sheet->mergeCells('A'.$row.':'.'D'.$row);
$sheet->getStyle('A'.$row.':'.'D'.$row)->getFont()->setBold(true);
$sheet->getStyle('A'.$row.':'.'D'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$sheet->getStyle('A'.$row.':'.'D'.$row)->getFont()->setSize(12);
$sheet->getStyle('A'.$row.':'.'D'.$row)->getFont()->getColor()->setRGB('#0000FF');
$row = $row + 2;

$nomor_account = "";
$class_name = "0";
$sub_name = "0";
$total_class_name_debet = 0;
$total_class_name_kredit = 0;
$total_sub_name_debet = 0;
$total_sub_name_kredit = 0;
$total_class_name = 0;
$total_sub_name = 0;
$j = 0;

for($i = 0; $i < sizeof($content); $i++) {
    if($content[$i]->nomor_account == ""){
        continue;
    }
    else if(strcmp(substr($content[$i]->nomor_account, 0, 1), $class_name) != 0 && strcmp(substr($content[$i]->nomor_account, 0, 2), $sub_name) != 0 && $class_name == "0" && $sub_name == "0") {
        $sheet->setCellValue("A".$row, $content[$i]->class_name);
        $sheet->getStyle('A'.$row)->getFont()->setBold(true);
        $row++;
        $sheet->setCellValue("B".$row, $content[$i]->sub_name);
        $sheet->getStyle('B'.$row)->getFont()->setBold(true);
        $sheet->getStyle('B'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $row++;
        $sheet->getStyle('B'.$row.':'.'E'.$row)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
        $sheet->setCellValueExplicit("B".$row, $content[$i]->nomor_account,PHPExcel_Cell_DataType::TYPE_STRING);
        $sheet->setCellValue("C".$row, $content[$i]->nama_account);
        $value = $content[$i]->debet - $content[$i]->kredit;
        $sheet->setCellValue("D".$row, $value);
        $sheet->getStyle('B'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
        $sheet->getStyle('D'.$row.':'.'E'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
        $row++;
        $class_name = substr($content[$i]->nomor_account, 0, 1);
        $sub_name = substr($content[$i]->nomor_account, 0, 2);
        $total_sub_name = $total_sub_name + $value;
        $total_class_name = $total_class_name + $value;
    }
    else if(strcmp(substr($content[$i]->nomor_account, 0, 1), $class_name) != 0 && strcmp(substr($content[$i]->nomor_account, 0, 2), $sub_name) != 0) {
        $sheet->setCellValue("B".$row, "Total ".$content[$i - 1]->sub_name);
        $sheet->getStyle('B'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $sheet->setCellValue("D".$row, $total_sub_name);
        $sheet->getStyle('B'.$row.':'.'E'.$row)->getFont()->setBold(true);
        $debet = 0;
        $kredit = 0;
        $row++;
        $sheet->setCellValue("A".$row, "Total ".$content[$i - 1]->class_name);
        $sheet->setCellValue("D".$row, $total_class_name);
        $total_class_name = 0;
        $sheet->mergeCells('A'.$row.':'.'B'.$row);
        $sheet->getStyle('A'.$row.':'.'E'.$row)->getFont()->setBold(true);
        $sheet->getStyle('A'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $row = $row + 2;
        $sheet->setCellValue("A".$row, $content[$i]->class_name);
        $sheet->getStyle('A'.$row)->getFont()->setBold(true);
        $row++;
        $sheet->setCellValue("B".$row, $content[$i]->sub_name);
        $sheet->getStyle('B'.$row)->getFont()->setBold(true);
        $row++;
        $sheet->getStyle('B'.$row.':'.'E'.$row)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
        $sheet->setCellValueExplicit("B".$row, $content[$i]->nomor_account,PHPExcel_Cell_DataType::TYPE_STRING);
        $sheet->setCellValue("C".$row, $content[$i]->nama_account);
        $value = $content[$i]->debet - $content[$i]->kredit;
        $sheet->setCellValue("D".$row, $value);
        $sheet->getStyle('B'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
        $sheet->getStyle('D'.$row.':'.'E'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
        $row++;
        $class_name = substr($content[$i]->nomor_account, 0, 1);
        $sub_name = substr($content[$i]->nomor_account, 0, 2);
        $total_sub_name = 0;
        $total_class_name = $total_class_name + $value;
    }
    else if(strcmp(substr($content[$i]->nomor_account, 0, 1), $class_name) == 0 && strcmp(substr($content[$i]->nomor_account, 0, 2), $sub_name) != 0) {
        if($total_sub_name == 0) {
            $total_sub_name = $total_sub_name + $value;
        }
        $sheet->setCellValue("B".$row, "Total ".$content[$i - 1]->sub_name);
        $sheet->getStyle('B'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $sheet->setCellValue("D".$row, $total_sub_name);
        $sheet->getStyle('B'.$row.':'.'E'.$row)->getFont()->setBold(true);
        $total_sub_name = 0;
        $row = $row + 2;
        $sheet->setCellValue("B".$row, $content[$i]->sub_name);
        $sheet->getStyle('B'.$row)->getFont()->setBold(true);
        $sheet->getStyle('B'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $row++;
        $sheet->getStyle('B'.$row.':'.'E'.$row)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
        $sheet->setCellValueExplicit("B".$row, $content[$i]->nomor_account,PHPExcel_Cell_DataType::TYPE_STRING);
        $sheet->setCellValue("C".$row, $content[$i]->nama_account);
        $value = $content[$i]->debet - $content[$i]->kredit;
        $sheet->setCellValue("D".$row, $value);
        $sheet->getStyle('B'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
        $sheet->getStyle('D'.$row.':'.'E'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
        $row++;
        $class_name = substr($content[$i]->nomor_account, 0, 1);
        $sub_name = substr($content[$i]->nomor_account, 0, 2);
        $total_sub_name = $total_sub_name + $value;
        $total_class_name = $total_class_name + $value;
    }
    else if(strcmp(substr($content[$i]->nomor_account, 0, 1), $class_name) == 0 && strcmp(substr($content[$i]->nomor_account, 0, 2), $sub_name) == 0) {
        $sheet->getStyle('B'.$row.':'.'E'.$row)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
        $sheet->setCellValueExplicit("B".$row, $content[$i]->nomor_account,PHPExcel_Cell_DataType::TYPE_STRING);
        $sheet->setCellValue("C".$row, $content[$i]->nama_account);
        $value = $content[$i]->debet - $content[$i]->kredit;
        $sheet->setCellValue("D".$row, $value);
        $sheet->getStyle('B'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
        $sheet->getStyle('D'.$row.':'.'E'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
        $row++;
        $class_name = substr($content[$i]->nomor_account, 0, 1);
        $sub_name = substr($content[$i]->nomor_account, 0, 2);
        $total_sub_name = $total_sub_name + $value;
        $total_class_name = $total_class_name + $value;
    }
    $j++;
}
$sheet->setCellValue("B".$row, "Total ".$content[$j - 1]->sub_name);
$sheet->setCellValue("D".$row, $total_sub_name);
$sheet->getStyle('B'.$row.':'.'E'.$row)->getFont()->setBold(true);
$row++;
$sheet->setCellValue("A".$row, "Total ".$content[$j - 1]->class_name);
$sheet->setCellValue("D".$row, $total_class_name);
$sheet->mergeCells('A'.$row.':'.'B'.$row);
$sheet->getStyle('A'.$row.':'.'E'.$row)->getFont()->setBold(true);
$sheet->getStyle('A'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

foreach(range('A','D') as $columnID) {
    $sheet->getColumnDimension($columnID)->setAutoSize(true);
}

header ( 'Content-Type: application/vnd.ms-excel' );
header ( 'Content-Disposition: attachment;filename="'.ArrayAdapter::format("unslug",$_POST['akun']).'.xls"' );
header ( 'Cache-Control: max-age=0' );
$writer = PHPExcel_IOFactory::createWriter ( $file, 'Excel5' );
$writer->save ( 'php://output' );
return;

?>