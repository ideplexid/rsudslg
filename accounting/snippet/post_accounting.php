<?php 
global $db;
require_once "accounting/snippet/del_accounting.php";
require_once "smis-base/smis-include-service-consumer.php";
$dbtable                    = new DBTable($db,"smis_ac_notify");
$notify                     = $dbtable->select($_POST['id']);
$datalist                   = array();
$datalist['data']           = $notify->data;
$serv                       = new ServiceConsumer($db,$notify->service,$datalist,$notify->entity);
$serv->execute();
$content                    = $serv->getContent();
$response                   = new ResponsePackage();
$response                   ->setStatus(ResponsePackage::$STATUS_OK);

foreach($content as $transaction){
    $dbtable->setName("smis_ac_transaksi");
    $header                 = $transaction['header'];
    $detail_transaksi       = $transaction['content'];
   
    /*header*/
    $t_header               = array();
    $t_header['jenis']      = "transaction";
    $t_header['tanggal']    = $header['tanggal'];
    $t_header['nomor']      = $header['nomor'];
    $t_header['keterangan'] = $header['keterangan'];
    $t_header['debet']      = $header['debet'];
    $t_header['kredit']     = $header['kredit'];
    $t_header['io']         = $header['io'];
    $t_header['id_notify']  = $_POST['id'];
    $t_header['code']       = $header['code'];
    $t_header['prop']       = "";
    $t_header['grup']       = isset($header['grup'])?$header['grup']:getSettings($db,"smis-ac-default-grup","");
    
    if($t_header['debet']!=$t_header['kredit'] || $t_header['kredit']=="0"){
        $response ->setAlertVisible(true)
                  ->setAlertContent("Posting Failed"," Posting Failed Not Balance / Transaction Zero !!, See Detail to Trace",ResponsePackage::$TIPE_DANGER);    
        echo json_encode($response->getPackage());
        return;
    }

    $query="SELECT * 
            FROM smis_ac_transaksi 
            WHERE id_notify='".$_POST['id']."' 
            AND code='".$header['code']."' ";
    $d_header               = $db->get_row($query);
    $id_header              = "";
    if($d_header==null){
        $dbtable->insert($t_header);
        $id_header                   = $dbtable->get_inserted_id();
    }else{
        $t_header_exist['id_notify'] = $_POST['id'];
        $t_header_exist['code']      = $header['code'];
        $dbtable->update($t_header,$t_header_exist);
        $id_header                   = $d_header->id;
    }
    
    /*detail*/
    $dbtable->setName("smis_ac_transaksi_detail");
    foreach($detail_transaksi as $detail){
        if($detail['debet']=="0" && $detail['kredit']=="0"){
            continue;
        }
        $t_detail                   = array();
        $t_detail['id_notify']      = $_POST['id'];
        $t_detail['id_transaksi']   = $id_header;
        $t_detail['code']           = $detail['code'];
        $t_detail['nomor_account']  = $detail['akun'];
        $t_detail['keterangan']     = $detail['ket'];
        $t_detail['debet']          = $detail['debet'];
        $t_detail['kredit']         = $detail['kredit'];
        $t_detail['grup']           = $t_header['grup'];
        $t_detail['prop']           = "";
        
        $query="SELECT * FROM smis_ac_transaksi_detail 
                WHERE id_notify='".$_POST['id']."'
                AND id_transaksi='".$id_header."'
                AND code='".$header['code']."' ";
        $d_detail=$db->get_row($query);
        if($d_detail==null){
            $dbtable->insert($t_detail);
        }else{
            $detail_exist['id_notify']      = $_POST['id'];
            $detail_exist['id_transaksi']   = $id_header;
            $detail_exist['code']           = $detail['code'];
            $dbtable->update($t_detail,$detail_exist);
        }
    }
}

/**Fill Data With Account Number*/
$query="UPDATE smis_ac_transaksi_detail LEFT JOIN smis_ac_account
        ON smis_ac_transaksi_detail.nomor_account=smis_ac_account.nomor
        SET smis_ac_transaksi_detail.nama_account=smis_ac_account.nama
        WHERE smis_ac_transaksi_detail.id_notify='".$_POST['id']."'";
$db->query($query);

/**Cek Weather All data Valid or Not */
$query="SELECT COUNT(*) as total FROM smis_ac_transaksi_detail LEFT JOIN smis_ac_account
        ON smis_ac_transaksi_detail.nomor_account=smis_ac_account.nomor
        WHERE (smis_ac_transaksi_detail.nama_account='' OR smis_ac_transaksi_detail.nomor_account='')  
              AND smis_ac_transaksi_detail.id_notify='".$_POST['id']."'
              AND smis_ac_transaksi_detail.prop!='del' ";
$total      = $db->get_var($query);

if($total>0){
    /*remove the inserted data*/
    require "accounting/snippet/del_accounting.php";
    $response ->setAlertVisible(true)
              ->setAlertContent("Posting Failed"," Posting Failed because one or more of data not Complete !!, See Detail to Trace",ResponsePackage::$TIPE_WARNING);    
}else{
    /*update that notify is already synch*/
    $krtx['id']      = $_POST['id'];
    $updtx['status'] = 1;
    $updtx['ket']    = "";
    $dbtable->setName("smis_ac_notify");
    $dbtable->update($updtx,$krtx);
}
echo json_encode($response->getPackage());
?>