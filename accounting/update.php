<?php

global $wpdb;
global $NUMBER;

require_once 'smis-libs-inventory/install.php';
$install = new InventoryInstallator($wpdb, "", "");
$install->setUsing(false, true, true);
$install->extendInstall("ac");
$install->install();

require_once 'smis-libs-class/DBCreator.php';
require_once "accounting/resource/install/table/smis_ac_notify.php";
require_once "accounting/resource/install/table/smis_ac_account.php";
require_once "accounting/resource/install/table/smis_ac_tipe_account.php";
require_once "accounting/resource/install/table/smis_ac_grup.php";
require_once "accounting/resource/install/table/smis_ac_transaksi.php";
require_once "accounting/resource/install/table/smis_ac_transaksi_detail.php";
require_once "accounting/resource/install/table/smis_acc_cashbank.php";
require_once "accounting/resource/install/view/smis_ac_error.php";


?>