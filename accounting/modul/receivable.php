<?php 
global $db;
require_once 'smis-base/smis-include-service-consumer.php';
$header=array("Ruangan","Layanan","Penunjang","Rawat Jalan","Rawat Inap","Total");
$uitable=new Table($header,"",NULL,false);
$uitable->setName("receivable")
		->setFooterVisible(false);

if(isset($_POST['command'])){
	require_once 'accounting/adapter/ReceivableAdapter.php';
	$adapter=new ReceivableAdapter();
	$adapter->add("Ruangan", "smis_entity")
			->add("Layanan", "layanan")
			->add("Total", "nilai")
			->setUseID(false);
	$dbres=new ServiceResponder($db, $uitable, $adapter, "get_receivable");
	$dbres->addData("awal", $_POST['dari'])
		  ->addData("akhir", $_POST['sampai'])
		  ->setMode(ServiceConsumer::$JOIN_ENTITY);
	$data=$dbres->command($_POST['command']);
	echo json_encode($data);
	return;
}

$btn_refresh=new Button("", "", "");
$btn_refresh->setClass("btn-primary")
			->setIsButton(Button::$ICONIC)
			->setIcon("fa fa-refresh")
			->setAction("receivable.view()");
$btn_print=new Button("", "", "");
$btn_print	->setClass("btn-primary")
			->setIsButton(Button::$ICONIC)
			->setIcon("fa fa-print")
			->setAction("receivable.print()");

$uitable->addModal("dari", "date", "Dari", "")
		->addModal("sampai", "date", "Sampai", "");
$form=$uitable->getModal()->getForm();
$form->addElement("", $btn_refresh)
	 ->addElement("", $btn_print);

echo $form->getHtml();
echo $uitable->getHtml();
echo addJS ( "framework/bootstrap/js/bootstrap-datepicker.js" );
echo addJS ( "framework/smis/js/table_action.js" );
echo addCSS ( "framework/bootstrap/css/datepicker.css" );

?>
<script type="text/javascript">
	var receivable;
	$(document).ready(function(){
		$(".mydate").datepicker();
		receivable=new TableAction("receivable","accounting","receivable");
		receivable.addRegulerData=function(reg_data){
			reg_data['dari']=$("#receivable_dari").val();
			reg_data['sampai']=$("#receivable_sampai").val();
			return reg_data;
		};
		
	});
</script>