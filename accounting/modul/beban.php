<?php
require_once 'accounting/class/adapter/HutangAdapter.php';
require_once 'smis-base/smis-include-service-consumer.php';
global $db;

$header=array("No.","Tanggal","Account","Ruang","Jenis","Keterangan","No Ref","Nilai","Code","Vendor");
$table=new Table($header,"",NULL,false);
$table->setFooterVisible(false);
$table->setName("beban");

if(isset($_POST['command'])){
    loadLibrary("smis-libs-function-time");    
	$adapter=new HutangAdapter();
    require_once "accounting/class/responder/BebanResponder.php";
	$dbres=new bebanResponder($db, $table, $adapter, "get_beban_accounting","finance");
	$dbres	->addData("dari", $_POST['dari'])
			->addData("sampai", $_POST['sampai'])
			->setMode(ServiceConsumer::$JOIN_ENTITY);
	$data=$dbres->command($_POST['command']);
	echo json_encode($data);
	return;
}

$dbtable=new DBTable($db,"smis_ac_grup");
$dbtable->setShowAll(true);
$list=$dbtable->view("",0);
$selectadapter=new SelectAdapter("nama","slug");
$grup=$selectadapter->getContent($list['data']);
$grup[]=array("name"=>"","value"=>"","default"=>1);

$table->addModal("dari", "date", "Dari", "");
$table->addModal("sampai", "date", "Sampai", "");
$table->addModal("nomor", "text", "Nomor", "-");
$table->addModal("grup","select","Grup",$grup);

$modal=$table->getModal();
$modal->setTitle("beban");
$form=$modal->getForm();
$btn=new Button("", "", "List");
$btn->setClass("btn-primary");
$btn->setIsButton(Button::$ICONIC_TEXT);
$btn->setIcon("fa fa-refresh");
$btn->setAction("beban.view()");
$form->addElement("", $btn);

$btn=new Button("", "", "Print");
$btn->setClass("btn-primary");
$btn->setIsButton(Button::$ICONIC_TEXT);
$btn->setIcon("fa fa-print");
$btn->setAction("beban.print()");
$form->addElement("", $btn);

$btn=new Button("", "", "Save");
$btn->setClass("btn-primary");
$btn->setIsButton(Button::$ICONIC_TEXT);
$btn->setIcon("fa fa-save");
$btn->setAction("beban.saving()");
$form->addElement("", $btn);


require_once "accounting/class/adapter/AccountAdapter.php";
$dbtable_account = new DBTable($db,"smis_ac_account");
$dbtable_account->setShowAll ( true );
$dbtable_account->setOrder ( "nomor ASC" );
$data = $dbtable_account->view ( "", "0" );
$data_list = $data ['data'];
$account = new AccountAdapter ();
$account_list = $account->getContent ( $data_list );
$account_list[]=array("name"=>"","value"=>"");

$select=new Select("","",$account_list);
$hidden=new Hidden("account_list_container_beban","",$select->getValue());
$SETTINGS=getSettings($db, "smis-rs-accounting-settings-beban", "{}");
$hidden_settings=new Hidden("settings_list_container_beban","",$SETTINGS);        

echo $hidden->getHtml();
echo $hidden_settings->getHtml();
echo $form->getHtml();
echo $table->getHtml();
echo addJS ( "framework/bootstrap/js/bootstrap-datepicker.js" );
echo addJS ( "framework/smis/js/table_action.js" );
echo addCSS ( "framework/bootstrap/css/datepicker.css" );
echo addJS ( "accounting/resource/js/beban.js",false );
echo addJS("framework/bootstrap/js/bootstrap-select.js");
echo addCSS ( "framework/bootstrap/css/bootstrap-select.css" );


?>