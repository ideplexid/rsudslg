<?php 
	$tab=new Tabulator("cashbank", "cashbank");
	$tab->add("cash_in","Kas Masuk", "",Tabulator::$TYPE_HTML,"fa fa-money")
			  ->add("cash_out","Kas Keluar", "",Tabulator::$TYPE_HTML,"fa fa-money")
			  ->add("bank_in","Bank Masuk", "",Tabulator::$TYPE_HTML,"fa fa-university")
			  ->add("bank_out","Bank Keluar", "",Tabulator::$TYPE_HTML,"fa fa-university");
    $tab->setPartialLoad(true,"accounting","cashbank","cashbank",true);
    echo $tab->getHtml();
?>