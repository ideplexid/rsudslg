<?php
require_once 'smis-framework/smis/api/SettingsBuilder.php';
global $db;
$settings = new SettingsBuilder ( $db, "accounting", "accounting", "settings", "Settings Accounting" );
$settings->setShowDescription ( true );
if($settings->isGroupAdded("general", "General"," fa fa-database")){
    $dbtable = new DBTable($db,"smis_ac_grup");
    $dbtable ->setShowAll(true);
    $data    = $dbtable ->view("","0");
    $adapter = new SelectAdapter("nama","slug");
    $list    = $adapter ->getContent($data['data']);
    $settings->addCurrent ( "smis-ac-default-grup", "Default Grup Transaksi", $list, "select", "" );   
}

if($settings->isGroupAdded("notify", "Tab Notify"," fa fa-lightbulb-o")){
    $settings ->addCurrent ( "accounting-notify-all_notify", "Semua", "1", "checkbox", "" )
              ->addCurrent ( "accounting-notify-archive_notify", "Arsip", "1", "checkbox", "" )
              ->addCurrent ( "accounting-notify-adjustment_persediaan_notify", "Adjustment Persediaan", "1", "checkbox", "" )
              ->addCurrent ( "accounting-notify-bayar_gaji_notify", "Pembayaran Gaji", "1", "checkbox", "" )
              ->addCurrent ( "accounting-notify-bayar_umum_notify", "Pembayaran Umum", "1", "checkbox", "" )
              ->addCurrent ( "accounting-notify-hutang_gaji_notify", "Hutang Gaji", "1", "checkbox", "" )
              ->addCurrent ( "accounting-notify-pemakaian_notify", "Pemakaian BHP", "1", "checkbox", "" )
              ->addCurrent ( "accounting-notify-pembayaran_hutang_notify", "Pembayaran Hutang", "1", "checkbox", "" )
              ->addCurrent ( "accounting-notify-pembelian", "Pembelian", "1", "checkbox", "" )
              ->addCurrent ( "accounting-notify-piutang_notify", "Piutang", "1", "checkbox", "" )
              ->addCurrent ( "accounting-notify-penerimaan_piutang_notify", "Penerimaan Piutang", "1", "checkbox", "" )
              ->addCurrent ( "accounting-notify-penjualan_notify", "Penjualan Notify", "1", "checkbox", "" )
              ->addCurrent ( "accounting-notify-pelunasan_asuransi_notify", "Pelunasan Asuransi Notify", "1", "checkbox", "" )
              ->addCurrent ( "accounting-notify-persediaan_notify", "Persediaan Notify", "1", "checkbox", "" )
              ->addCurrent ( "accounting-notify-return_pembelian", "Pembelian", "1", "checkbox", "" )
              ->addCurrent ( "accounting-notify-return_penjualan_notify", "Penjualan", "1", "checkbox", "" );
}

if($settings->isGroupAdded("transaksi", "Tab Transaksi"," fa fa-list")){
    $settings ->addCurrent ("accounting-transaksi-transaction", "Transaksi Harian", "1", "checkbox", "" )
              ->addCurrent ("accounting-transaksi-transaction_masuk", "T. Harian Masuk", "1", "checkbox", "" )
              ->addCurrent ("accounting-transaksi-transaction_keluar", "T. Harian Keluar", "1", "checkbox", "" )
              ->addCurrent ("accounting-transaksi-saldo_awal", "Saldo Awal", "1", "checkbox", "" )
              ->addCurrent ("accounting-transaksi-journal", "Jurnal Umum", "1", "checkbox", "" )
              ->addCurrent ("accounting-transaksi-journalmemorial", "Input Transaksi Penyesuaian", "1", "checkbox", "" )
              ->addCurrent ("accounting-transaksi-memorial", "Transaksi Penyesuaian", "1", "checkbox", "" )
              ->addCurrent ("accounting-transaksi-error", "Data Error", "1", "checkbox", "" )
              ->addCurrent ("accounting-transaksi-min-date", "Tanggal Minimal", "", "date", "Tanggal Minimal yang boleh dimasukan pada transaksi" );
}

if($settings->isGroupAdded("laporan", "Tab Laporan"," fa fa-book")){
    $settings ->addCurrent ("accounting-transaksi-ledger", "Buku Besar", "1", "checkbox", "" )
              ->addCurrent ("accounting-transaksi-trialbalance", "Neraca Saldo", "1", "checkbox", "" )
              ->addCurrent ("accounting-transaksi-worksheet", "Neraca Lajur", "1", "checkbox", "" )
              ->addCurrent ("accounting-transaksi-worksheet_laba_rugi", "Laba Rugi", "1", "checkbox", "" )
              ->addCurrent ("accounting-transaksi-worksheet_neraca", "Neraca", "1", "checkbox", "" )
              ->addCurrent ("accounting-transaksi-income", "Laporan Laba Rugi", "1", "checkbox", "" )
              ->addCurrent ("accounting-transaksi-cash_flow", "Laporan Arus Kas", "1", "checkbox", "" );
}

if($settings->isGroupAdded("akun_khusus", "Akun Khusus"," fa fa-tag")){
    $settings->addCurrent ( "accounting-akun-khusus-laba-berjalan", "Laba Tahun Berjalan", "", "chooser-settings-akun_khusus_laba_tahun_berjalan-Akun", "" );
    $settings->addSuperCommandAction("akun_khusus_laba_tahun_berjalan","kode_akun");
    $settings->addSuperCommandArray("akun_khusus_laba_tahun_berjalan","accounting-akun-khusus-laba-berjalan","nomor",true);    
}
$settings->setDateEnabled(true);
$settings->setPartialLoad(true);
$settings->init ();
?>