<?php 

$tabs=new Tabulator("jurnal", "",Tabulator::$LANDSCAPE);
if(getSettings($db,"accounting-transaksi-accounting_pasien_bayar","1")=="1") 
    $tabs->add("accounting_pasien_bayar", "Accounting Pasien", "",Tabulator::$TYPE_HTML,"fa fa-file-excel-o");
if(getSettings($db,"accounting-transaksi-transaction","1")=="1") 
    $tabs->add("transaction", "Transaksi Harian", "",Tabulator::$TYPE_HTML,"fa fa-file");
if(getSettings($db,"accounting-transaksi-transaction_masuk","1")=="1") 
    $tabs->add("transaction_masuk", "T. Harian Masuk", "",Tabulator::$TYPE_HTML,"fa fa-file");
if(getSettings($db,"accounting-transaksi-transaction_keluar","1")=="1") 
    $tabs->add("transaction_keluar", "T. Harian Keluar", "",Tabulator::$TYPE_HTML,"fa fa-file");
if(getSettings($db,"accounting-transaksi-saldo_awal","1")=="1") 
    $tabs->add("saldo_awal", "Saldo Awal", "",Tabulator::$TYPE_HTML,"fa fa-file");    
if(getSettings($db,"accounting-transaksi-journal","1")=="1") 
    $tabs->add("journal", "Jurnal Transaksi", "",Tabulator::$TYPE_HTML,"fa fa-file-text");
if(getSettings($db,"accounting-transaksi-memorial","1")=="1") 
    $tabs->add("memorial", "Input Jurnal Umum", "",Tabulator::$TYPE_HTML,"fa fa-recycle");
if(getSettings($db,"accounting-transaksi-journalmemorial","1")=="1") 
    $tabs->add("journalmemorial", "Jurnal Umum", "",Tabulator::$TYPE_HTML,"fa fa-refresh");
if(getSettings($db,"accounting-transaksi-error","1")=="1") 
    $tabs->add("error", "Data Error", "",Tabulator::$TYPE_HTML,"fa fa-warning");
$tabs->setPartialLoad(true,"accounting","jurnal","jurnal",true);        
echo $tabs->getHtml();

?>
