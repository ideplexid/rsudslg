<?php
require_once 'accounting/class/adapter/InventarisAdapter.php';
require_once 'smis-base/smis-include-service-consumer.php';
global $db;

$header=array("No.","Account","Ruang","Barang","Kode","Masuk","Durasi","Akhir","Masa","Kondisi","Nilai","Penyusutan","Sisa","Penyusutan Baik","Sisa Baik","Penyusutan Rusak","Sisa Rusak");
$table=new Table($header,"",NULL,false);
$table->setFooterVisible(false);
$table->setName("inventaris");

if(isset($_POST['command'])){
    loadLibrary("smis-libs-function-time");    
	$adapter=new InventarisAdapter($_POST['dari']);
    require_once "accounting/class/responder/InventarisResponder.php";
	$dbres=new InventarisResponder($db, $table, $adapter, "get_inventaris");
	$dbres	->addData("dari", $_POST['dari'])
			->addData("sampai", $_POST['sampai'])
			->setMode(ServiceConsumer::$JOIN_ENTITY);
	$data=$dbres->command($_POST['command']);
	echo json_encode($data);
	return;
}

$dbtable=new DBTable($db,"smis_ac_grup");
$dbtable->setShowAll(true);
$list=$dbtable->view("",0);
$selectadapter=new SelectAdapter("nama","slug");
$grup=$selectadapter->getContent($list['data']);
$grup[]=array("name"=>"","value"=>"","default"=>1);

$table->addModal("dari", "date", "Dari", "");
$table->addModal("sampai", "date", "Sampai", "");
$table->addModal("nomor", "text", "Nomor", "-");
$table->addModal("grup","select","Grup",$grup);

$modal=$table->getModal();
$modal->setTitle("Inventaris");
$form=$modal->getForm();
$btn=new Button("", "", "List");
$btn->setClass("btn-primary");
$btn->setIsButton(Button::$ICONIC_TEXT);
$btn->setIcon("fa fa-refresh");
$btn->setAction("inventaris.view()");
$form->addElement("", $btn);

$btn=new Button("", "", "Print");
$btn->setClass("btn-primary");
$btn->setIsButton(Button::$ICONIC_TEXT);
$btn->setIcon("fa fa-print");
$btn->setAction("inventaris.print()");
$form->addElement("", $btn);

$btn=new Button("", "", "Save");
$btn->setClass("btn-primary");
$btn->setIsButton(Button::$ICONIC_TEXT);
$btn->setIcon("fa fa-save");
$btn->setAction("inventaris.saving()");
$form->addElement("", $btn);


require_once "accounting/class/adapter/AccountAdapter.php";
$dbtable_account = new DBTable($db,"smis_ac_account");
$dbtable_account->setShowAll ( true );
$dbtable_account->setOrder ( "nomor ASC" );
$data = $dbtable_account->view ( "", "0" );
$data_list = $data ['data'];
$account = new AccountAdapter ();
$account_list = $account->getContent ( $data_list );
$account_list[]=array("name"=>"","value"=>"");

$select=new Select("","",$account_list);
$hidden=new Hidden("account_list_container_inventaris","",$select->getValue());
$SETTINGS=getSettings($db, "smis-rs-accounting-settings-inventaris-penyusutan", "{}");
$hidden_settings=new Hidden("settings_list_container_inventaris","",$SETTINGS);        

echo $hidden->getHtml();
echo $hidden_settings->getHtml();
echo $form->getHtml();
echo $table->getHtml();
echo addJS ( "framework/bootstrap/js/bootstrap-datepicker.js" );
echo addJS ( "framework/smis/js/table_action.js" );
echo addCSS ( "framework/bootstrap/css/datepicker.css" );
echo addJS ( "accounting/resource/js/inventaris.js",false );
echo addJS("framework/bootstrap/js/bootstrap-select.js");
echo addCSS ( "framework/bootstrap/css/bootstrap-select.css" );


?>