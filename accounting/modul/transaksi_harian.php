<?php 

$tabs=new Tabulator("transaksi_harian", "",Tabulator::$LANDSCAPE);
if(getSettings($db,"accounting-transaksi-accounting_pasien_bayar","1")=="1") 
    $tabs->add("accounting_pasien_bayar", "Accounting Pasien", "",Tabulator::$TYPE_HTML,"fa fa-file-excel-o");
if(getSettings($db,"accounting-transaksi-transaction","1")=="1") 
    $tabs->add("transaction", "Transaksi Harian", "",Tabulator::$TYPE_HTML,"fa fa-file");
if(getSettings($db,"accounting-transaksi-transaction_masuk","1")=="1") 
    $tabs->add("transaction_masuk", "T. Harian Masuk", "",Tabulator::$TYPE_HTML,"fa fa-file");
if(getSettings($db,"accounting-transaksi-transaction_keluar","1")=="1") 
    $tabs->add("transaction_keluar", "T. Harian Keluar", "",Tabulator::$TYPE_HTML,"fa fa-file");
if(getSettings($db,"accounting-transaksi-saldo_awal","1")=="1") 
    $tabs->add("saldo_awal", "Saldo Awal", "",Tabulator::$TYPE_HTML,"fa fa-file");
if(getSettings($db,"accounting-transaksi-journal","1")=="1") 
    $tabs->add("journal", "Jurnal Transaksi", "",Tabulator::$TYPE_HTML,"fa fa-file-text");
if(getSettings($db,"accounting-transaksi-ledger","1")=="1") 
    $tabs->add("ledger", "Buku Besar", "",Tabulator::$TYPE_HTML,"fa fa-book");
if(getSettings($db,"accounting-transaksi-trialbalance","1")=="1") 
    $tabs->add("trialbalance", "Neraca Saldo", "",Tabulator::$TYPE_HTML,"fa fa-money");
if(getSettings($db,"accounting-transaksi-memorial","1")=="1") 
    $tabs->add("memorial", "Input Jurnal Umum", "",Tabulator::$TYPE_HTML,"fa fa-recycle");
if(getSettings($db,"accounting-transaksi-journalmemorial","1")=="1") 
    $tabs->add("journalmemorial", "Jurnal Umum", "",Tabulator::$TYPE_HTML,"fa fa-refresh");
if(getSettings($db,"accounting-transaksi-adjustedledger","1")=="1") 
    $tabs->add("adjustedledger", "Buku Besar Penyesuaian", "",Tabulator::$TYPE_HTML,"fa fa-book");
if(getSettings($db,"accounting-transaksi-adjustedtrialbalance","1")=="1") 
    $tabs->add("adjustedtrialbalance", "Neraca Saldo Penyesuaian", "",Tabulator::$TYPE_HTML,"fa fa-money");
if(getSettings($db,"accounting-transaksi-worksheet","1")=="1") 
    $tabs->add("worksheet", "Neraca Lajur", "",Tabulator::$TYPE_HTML,"fa fa-file-excel-o");
if(getSettings($db,"accounting-transaksi-worksheet_laba_rugi","1")=="1") 
    $tabs->add("worksheet_laba_rugi", "Laba Rugi", "",Tabulator::$TYPE_HTML,"fa fa-money");
if(getSettings($db,"accounting-transaksi-worksheet_neraca","1")=="1") 
    $tabs->add("worksheet_neraca", "Neraca", "",Tabulator::$TYPE_HTML,"fa fa-book");
if(getSettings($db,"accounting-transaksi-income","1")=="1") 
    $tabs->add("income", "Laporan Laba Rugi", "",Tabulator::$TYPE_HTML,"fa fa-money");
if(getSettings($db,"accounting-transaksi-cash_flow","1")=="1") 
    $tabs->add("cash_flow", "Laporan Arus Kas", "",Tabulator::$TYPE_HTML,"fa fa-money");
if(getSettings($db,"accounting-transaksi-error","1")=="1") 
    $tabs->add("error", "Data Error", "",Tabulator::$TYPE_HTML,"fa fa-warning");


$tabs->setPartialLoad(true,"accounting","transaksi_harian","transaksi_harian",true);        
echo $tabs->getHtml();

?>
