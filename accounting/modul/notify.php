<?php 

    $status = new OptionBuilder();
    $status ->add("","",1)
            ->add("Posted","1")
            ->add("Un Posted","0");
    $info  = new OptionBuilder();
    $info  ->add("","",1)
            ->add("Removed","Removed","0")
            ->add("Updated","Updated","0")
            ->add("Ignored","Ignored","0");
    
    $uitable = new Table(array());
    $uitable ->setName("notify")
             ->addModal("dari","datetime","Dari","")
             ->addModal("sampai","datetime","Sampai","")
             ->addModal("status","select","Status",$status->getContent())
             ->addModal("info","select","Info",$info->getContent())
             ->addModal("ruangan","text","Ruangan","")
             ->addModal("id","text","ID","");
    
    $form   = $uitable->getModal()->getForm();
    $button = new Button("","","");
    $button ->setClass("btn-primary")
            ->setIsButton(Button::$ICONIC_TEXT)
            ->setIcon(" fa fa-search")
            ->setAction("notify.search_find()");
    $form   ->addElement("",$button);
    
	$tab = new Tabulator("notify", "notify",Tabulator::$LANDSCAPE_RIGHT);
    if(getSettings($db,"accounting-notify-all_notify","1")=="1")                    $tab ->add("all_notify","Semua", "",Tabulator::$TYPE_HTML,"fa fa-lightbulb-o");
    if(getSettings($db,"accounting-notify-penjualan_notify","1")=="1")              $tab ->add("penjualan_notify","Penjualan", "",Tabulator::$TYPE_HTML,"fa fa-money");
    if(getSettings($db,"accounting-notify-pelunasan_asuransi_notify","1")=="1")     $tab ->add("pelunasan_asuransi_notify","Pelunasan Asuransi", "",Tabulator::$TYPE_HTML,"fa fa-money");
    if(getSettings($db,"accounting-notify-return_penjualan_notify","1")=="1")       $tab ->add("return_penjualan_notify","Return Penjualan", "",Tabulator::$TYPE_HTML,"fa fa-money");
    if(getSettings($db,"accounting-notify-pembelian_notify","1")=="1")              $tab ->add("pembelian_notify","Pembelian", "",Tabulator::$TYPE_HTML,"fa fa-money");
    if(getSettings($db,"accounting-notify-return_pembelian_notify","1")=="1")       $tab ->add("return_pembelian_notify","Return Pembelian", "",Tabulator::$TYPE_HTML,"fa fa-money");
    if(getSettings($db,"accounting-notify-piutang_notify","1")=="1")                $tab ->add("piutang_notify","Piutang", "",Tabulator::$TYPE_HTML,"fa fa-money");
    if(getSettings($db,"accounting-notify-penerimaan_piutang_notify","1")=="1")     $tab ->add("penerimaan_piutang_notify","Penerimaan Piutang", "",Tabulator::$TYPE_HTML,"fa fa-money");
    if(getSettings($db,"accounting-notify-hutang_gaji_notify","1")=="1")            $tab ->add("hutang_gaji_notify","Hutang Gaji", "",Tabulator::$TYPE_HTML,"fa fa-money");
    if(getSettings($db,"accounting-notify-bayar_gaji_notify","1")=="1")             $tab ->add("bayar_gaji_notify","Bayar Gaji", "",Tabulator::$TYPE_HTML,"fa fa-money");
    if(getSettings($db,"accounting-notify-pembayaran_hutang_notify","1")=="1")      $tab ->add("pembayaran_hutang_notify","Pembayaran Hutang", "",Tabulator::$TYPE_HTML,"fa fa-money");
    if(getSettings($db,"accounting-notify-bayar_umum_notify","1")=="1")             $tab ->add("bayar_umum_notify","Pembayaran Umum", "",Tabulator::$TYPE_HTML,"fa fa-money");
    if(getSettings($db,"accounting-notify-adjustment_persediaan_notify","1")=="1")  $tab ->add("adjustment_persediaan_notify","Adjustment", "",Tabulator::$TYPE_HTML,"fa fa-money");
    if(getSettings($db,"accounting-notify-pemakaian_notify","1")=="1")              $tab ->add("pemakaian_notify","Pemakaian BHP", "",Tabulator::$TYPE_HTML,"fa fa-money");
    if(getSettings($db,"accounting-notify-persediaan_notify","1")=="1")             $tab ->add("persediaan_notify","Persediaan", "",Tabulator::$TYPE_HTML,"fa fa-money");
    if(getSettings($db,"accounting-notify-archive_notify","1")=="1")                $tab ->add("archive_notify","Arsip", "",Tabulator::$TYPE_HTML,"fa fa-briefcase");
    $tab ->setPartialLoad(true,"accounting","notify","notify",false);
    
    echo addJS("framework/bootstrap/js/bootstrap-datetimepicker.js",true);
    echo addCSS("framework/bootstrap/css/bootstrap-datetimepicker.css",true);
    echo addJS("accounting/resource/js/notify.js",false);
    
    echo $form->getHtml();
    echo "<div class='clear'></div>";
    echo $tab->getHtml();
?>