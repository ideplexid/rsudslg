<?php 

require_once "smis-libs-class/MasterTemplate.php";
$grup=new MasterTemplate($db,"smis_ac_grup","accounting","grup_transaksi");
$grup->getUItable()->setHeader(array("No.","Name","Slug","Keterangan"));
$grup->getAdapter()->setUseNumber(true,"No.","back.");
$grup->getAdapter()->add("Name","nama");
$grup->getAdapter()->add("Slug","slug");
$grup->getAdapter()->add("Keterangan","keterangan");
$grup->getUItable()->addModal("id","hidden","","");
$grup->getUItable()->addModal("nama","text","Nama","");
$grup->getUItable()->addModal("slug","text","Slug","");
$grup->getUItable()->addModal("keterangan","textarea","Keterangan","");
$grup->setModalTitle("Grup Name");
$grup->initialize();
?>