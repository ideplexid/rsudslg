<?php 

$tabs=new Tabulator("laporan", "",Tabulator::$LANDSCAPE);
if(getSettings($db,"accounting-transaksi-ledger","1")=="1") 
    $tabs->add("ledger", "Buku Besar", "",Tabulator::$TYPE_HTML,"fa fa-book");
if(getSettings($db,"accounting-transaksi-trialbalance","1")=="1") 
    $tabs->add("trialbalance", "Neraca Saldo", "",Tabulator::$TYPE_HTML,"fa fa-money");
/*
if(getSettings($db,"accounting-transaksi-adjustedledger","1")=="1") 
    $tabs->add("adjustedledger", "Buku Besar Penyesuaian", "",Tabulator::$TYPE_HTML,"fa fa-book");
if(getSettings($db,"accounting-transaksi-adjustedtrialbalance","1")=="1") 
    $tabs->add("adjustedtrialbalance", "Neraca Saldo Penyesuaian", "",Tabulator::$TYPE_HTML,"fa fa-money");
*/
if(getSettings($db,"accounting-transaksi-worksheet","1")=="1") 
    $tabs->add("worksheet", "Neraca Lajur", "",Tabulator::$TYPE_HTML,"fa fa-file-excel-o");
if(getSettings($db,"accounting-transaksi-worksheet_laba_rugi","1")=="1") 
    $tabs->add("worksheet_laba_rugi", "Laba Rugi", "",Tabulator::$TYPE_HTML,"fa fa-money");
if(getSettings($db,"accounting-transaksi-worksheet_neraca","1")=="1") 
    $tabs->add("worksheet_neraca", "Neraca", "",Tabulator::$TYPE_HTML,"fa fa-book");
if(getSettings($db,"accounting-transaksi-income","1")=="1") 
    $tabs->add("income", "Laporan Laba Rugi", "",Tabulator::$TYPE_HTML,"fa fa-money");
if(getSettings($db,"accounting-transaksi-cash_flow","1")=="1") 
    $tabs->add("cash_flow", "Laporan Arus Kas", "",Tabulator::$TYPE_HTML,"fa fa-money");


$tabs->setPartialLoad(true,"accounting","laporan","laporan",true);        
echo $tabs->getHtml();

?>
