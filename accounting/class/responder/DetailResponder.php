<?php 

class DetailResponder extends DBChildResponder {
	private $nama_account;
	public function __construct($dbtable, $table, $adapter, $id_parent, $name) {
		parent::__construct ( $dbtable, $table, $adapter, $id_parent );
		$this->id_parent = $id_parent;
		$this->nama_account = $name;
	}
	public function save() {
		$data = $this->postToArray ();
		$id ['id'] = $_POST ['id'];
		$data ['nama_account'] = $this->nama_account;
		if ($_POST ['id'] == 0 || $_POST ['id'] == "") {
			$result = $this->dbtable->insert ( $data );
			$id ['id'] = $this->dbtable->get_inserted_id ();
			$success ['type'] = 'insert';
		} else {
			$result = $this->dbtable->update ( $data, $id );
			$success ['type'] = 'update';
		}
		$success ['id'] = $id ['id'];
		$success ['success'] = 1;
		if ($result === false)
			$success ['success'] = 0;
		return $success;
	}
}

?>