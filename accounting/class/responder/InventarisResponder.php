<?php 

class InventarisResponder extends ServiceResponder{
    
    public function command($command){
        if($command=="saving"){
            $response=new ResponsePackage();
            $response->setStatus(ResponsePackage::$STATUS_OK);
            $response->setAlertVisible(true);
            $success=$this->saving();
            if($success===false){
                $response->setAlertContent("Penyimpanan Gagal", "Terdapat Kesalahan Penyimpanan");
                $response->setContent(0);
            }else{
                $response->setAlertContent("Penyimpanan Berhasil", "Pernyimpanan Berhasil");
                $response->setContent(1);
            }
            return $response->getPackage();
        }else{
            return parent::command($command);
        }
    }
    
    public function saving(){
		$data=$_POST['list'];
        $settings=$_POST['settings'];
        $dari=$_POST['dari'];
        $sampai=$_POST['sampai'];
        
        global $db;
        setSettings($db, "smis-rs-accounting-settings-inventaris-penyusutan", json_encode($settings));
                
        /*begin saing the data*/
        $insert=array();
        $insert['jenis']="memorial";
        $insert['tanggal']=$dari;
        
        $nomor=$_POST['nomor'];
        if($nomor=="" || $nomor=="-"){
            require_once "accounting/function/new_transaksi_harian.php";
            $format=new_transaksi_harian($db,$sampai,"DPC");
            $insert['nomor']=$format;
        }
        
        $insert['grup']=$_POST['grup'];
        $insert['keterangan']="Penyusutan ".ArrayAdapter::dateFormat("date d M Y",$dari)." - ".ArrayAdapter::dateFormat("date d M Y",$sampai);
        $insert['fix']=0;
        $insert['io']=0;
        
        $success=true;
        $db->begin_transaction();
        $db->set_autocommit(false);
        $smis_ac_transaksi=new DBTable($db, "smis_ac_transaksi");
        $success=$smis_ac_transaksi->insert($insert);
        $inserted_id=$smis_ac_transaksi->get_inserted_id();	
        $total_kredit=0;
        $total_debit=0;
        if($success!==false){
            $smis_ac_transaksi_detail=new DBTable($db, "smis_ac_transaksi_detail");
            foreach($data as $x){
                $sub=array();
                $sub['id_transaksi']=$inserted_id;
                $sub['nomor_account']=$x['akun'];
                $sub['nama_account']=$x['akun_name'];
                $sub['keterangan']=$x['name']." - ".$insert['keterangan'];
                $sub['debet']=$x['dk']=="debit"?$x['nilai']:0;
                $sub['kredit']=$x['dk']=="kredit"?$x['nilai']:0;
                
                $total_kredit+=$sub['kredit'];
                $total_debit+=$sub['debet'];			
                
                $success=$smis_ac_transaksi_detail->insert($sub);
                if($success===false){
                    break;				
                }
            }	
        }	
        $id_up=array("id"=>$inserted_id);
        $data_up=array("debet"=>$total_debit,"kredit"=>$total_kredit);
        $smis_ac_transaksi->update($data_up,$id_up);
        
        if($success===false){
            $db->rollback();
        }else{
            $db->commit();
        }
        $db->set_autocommit(true);
        
        /*end saving the data*/
        return $success;
	}
    
}

?>