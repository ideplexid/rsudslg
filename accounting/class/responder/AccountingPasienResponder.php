<?php 

class AccountingPasienResponder extends DBResponder{
    
    public function command($command){
        if($command=="saveall"){
            $response=new ResponsePackage();
            $response->setStatus(ResponsePackage::$STATUS_OK);
            $response->setAlertVisible(true);
            $success=$this->saveAll();
            if($success===false){
                $response->setAlertContent("Penyimpanan Gagal", "Terdapat Kesalahan Penyimpanan</br> Silakan Coba Lagi",ResponsePackage::$TIPE_WARNING);
                $response->setContent(0);
            }else{
                $response->setAlertContent("Penyimpanan Berhasil", "Pernyimpanan Berhasil");
                $response->setContent(1);
            }
            return $response->getPackage();
        }else if($command=="list"){
            $cont=$this->listing();
            $res = new ResponsePackage();
            $res->setStatus(ResponsePackage::$STATUS_OK);
            $res->setContent($cont);
            return $res->getPackage();
        }else{
            return parent::command($command);
        }
    }
    
    public function listing(){
        $db=$this->getDBTable()->get_db();
        $table=$this->getUITable();
        require_once 'accounting/class/adapter/AccountingPasienBayarAdapter.php';
        require_once 'accounting/class/adapter/AccountAdapter.php';
        
        $SETTINGS=getSettings($db, "smis-rs-accounting-settings-pasien-bayar", NULL);
        if($SETTINGS==NULL || $SETTINGS==""){
            $SETTINGS=array();
        }else{
            $SETTINGS=json_decode($SETTINGS,true);
        }
        
        $dbtable_account = $this->getDBTable();
        $dbtable_account->setShowAll ( true );
        $dbtable_account->setOrder ( "nomor ASC" );
        $data = $dbtable_account->view ( "", "0" );
        $data_list = $data ['data'];
        $account = new AccountAdapter ();
        $account_list = $account->getContent ( $data_list );
        
        $serv=new ServiceConsumer($db, "get_accounting_pasien_bayar",NULL,"kasir");
        $serv->addData("noreg_pasien", $_POST['noreg_pasien']);
        $serv->execute();
        $cont = $serv->getContent();
        $akun=$cont['data'];
        $adapter=new AccountingPasienBayarAdapter($account_list,$SETTINGS);
        $data_cont=$adapter->getContent($akun);
        $table->setContent($data_cont);
        $cont['body_content']=$table->getBodyContent();
        return $cont;
    }
    
    public function saveAll(){
        $db=$this->getDBTable()->get_db();
        $SETTINGS=getSettings($db, "smis-rs-accounting-settings-pasien-bayar", NULL);
        if($SETTINGS==NULL || $SETTINGS==""){
            $SETTINGS=array();
        }else{
            $SETTINGS=json_decode($SETTINGS,true);
        }
        
        $data=json_decode($_POST['data'],true);
        $nama_pasien=$_POST['nama_pasien'];
        $nrm_pasien=$_POST['nrm_pasien'];
        $noreg_pasien=$_POST['noreg_pasien'];
        
        $insert=array();
        $insert['jenis']="transaction";
        $insert['tanggal']=$_POST['tanggal'];
        
        $nomor=$_POST['nomor'];
        if($nomor=="" || $nomor=="-"){
            require_once "accounting/function/new_transaksi_harian.php";
            $format=new_transaksi_harian($db,$_POST['tanggal'],"TM");
            $insert['nomor']=$format;
        }
        
        $insert['grup']=$_POST['grup'];
        $insert['keterangan']=$_POST['keterangan'];
        $insert['fix']=0;
        $insert['io']=1;
        
        $success=true;
        $db->begin_transaction();
        $db->set_autocommit(false);
        $smis_ac_transaksi=new DBTable($db, "smis_ac_transaksi");
        $success=$smis_ac_transaksi->insert($insert);
        $inserted_id=$smis_ac_transaksi->get_inserted_id();	
        $total_kredit=0;
        $total_debit=0;
        if($success!==false){
            $smis_ac_transaksi_detail=new DBTable($db, "smis_ac_transaksi_detail");
            foreach($data as $x){
                $sub=array();
                $sub['id_transaksi']=$inserted_id;
                $sub['nomor_account']=$x['akun'];
                $sub['nama_account']=$x['akun_name'];
                $sub['keterangan']=$x['name']." - ".$insert['keterangan'];
                $sub['debet']=$x['dk']=="debit"?$x['money']:0;
                $sub['kredit']=$x['dk']=="kredit"?$x['money']:0;
                
                $total_kredit+=$sub['kredit'];
                $total_debit+=$sub['debet'];			
                
                $ARRAY_NAME=$x['code'];
                $ARRAY_OBJECT=array();
                $ARRAY_OBJECT['akun']=$x['akun'];
                $ARRAY_OBJECT['dk']=$x['dk'];
                $SETTINGS[$ARRAY_NAME]=$ARRAY_OBJECT;
                
                $success=$smis_ac_transaksi_detail->insert($sub);
                if($success===false){
                    break;				
                }
            }	
        }	
        $id_up=array("id"=>$inserted_id);
        $data_up=array("debet"=>$total_debit,"kredit"=>$total_kredit);
        $smis_ac_transaksi->update($data_up,$id_up);
        
        if($success===false){
            $db->rollback();
        }else{
            setSettings($db, "smis-rs-accounting-settings-pasien-bayar", json_encode($SETTINGS));
            $db->commit();
        }
        $db->set_autocommit(true);
        return $success;
    }
    
}

?>