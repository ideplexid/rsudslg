<?php 

class ParentResponder extends DBParentResponder {
		public function __construct($parent_dbtable, $parent_table, $parent_adapter) {
			parent::__construct ( $parent_dbtable, $parent_table, $parent_adapter );
		}
		public function save() {
			$data = $this->postToArray ();
			$id ['id'] = $_POST ['id'];
			if ($_POST ['id'] == 0 || $_POST ['id'] == "") {
				$result = $this->dbtable->insert ( $data );
				$id ['id'] = $this->dbtable->get_inserted_id ();
				$success ['type'] = 'insert';
			} else {
				global $db;
				
                $query="SELECT SUM(smis_ac_transaksi_detail.debet) as debet,
				SUM(smis_ac_transaksi_detail.kredit) as kredit
				FROM smis_ac_transaksi_detail
				WHERE id_transaksi='".$_POST['id']."' 
				AND smis_ac_transaksi_detail.prop!='del' ";
                $x=$db->get_row($query);
				$data['debet']=$x->debet;
				$data['kredit']=$x->kredit;				 
				
                $query="UPDATE smis_ac_transaksi_detail 
                        set grup='".$_POST['grup']."', 
                        code = '".$data['nomor']."'
                        WHERE id_transaksi='".$_POST['id']."'";
                $db->query($query);
                
                $result = $this->dbtable->update ( $data, $id );
				$success ['type'] = 'update';
			}
			$success ['id'] = $id ['id'];
			$success ['success'] = 1;
			if ($result === false)
				$success ['success'] = 0;
			return $success;
		}
	}

 ?>