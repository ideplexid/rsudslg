<?php 
class NomorPerkiraanResponder extends DBResponder{
    public function excel(){
        date_default_timezone_set ( "Asia/Jakarta" );
        $query = "SELECT max(child_number) FROM smis_ac_account WHERE prop!='del'";
        $max   = $this->dbtable->get_db()->get_var($query);

        require_once ("smis-libs-out/php-excel/PHPExcel.php");
        $file  = new PHPExcel ();
        $sheet = $file->getActiveSheet();
        $sheet ->setCellValue("A1","No.");
        $sheet ->setCellValue("B1","Nomor");
        

        $i              = 1;        
        $adjustment     = $max+4;
        $currentColumn  = 'C';
        $columnIndex         = PHPExcel_Cell::columnIndexFromString($currentColumn);
        $adjustedColumnIndex = $columnIndex + $adjustment;
        $adjustedColumn      = PHPExcel_Cell::stringFromColumnIndex($adjustedColumnIndex);
        $letters             = array();
        $letter              = 'C';
        while ($letter !== $adjustedColumn) {
            $letters[] = $letter++;
        }
        $sheet ->mergeCells("C1:".$letters[$max]."1");
        $sheet ->setCellValue("C1","Nama");
        $sheet ->setCellValue($letters[$max+1]."1","Tipe Akun");
        $sheet ->setCellValue($letters[$max+2]."1","Saldo Normal");
        $sheet ->setCellValue($letters[$max+3]."1","Saldo Awal");
        $sheet ->setCellValue($letters[$max+4]."1","Update Terakhir");
        $sheet ->getStyle("A1:".$letters[$max+4]."1")->getFont()->setBold(true);

        $this->dbtable->setShowAll(true);
        $data       = $this->dbtable->view("",0);
        $list       = $data['data'];
        $no         = 1;
        $iter       = 0;
        foreach($list as $x){
            $no++;
            $iter++;

            $sheet->setCellValue("A".$no,$iter.". ");
            $sheet->setCellValueExplicit("B".$no,$x->nomor,PHPExcel_Cell_DataType::TYPE_STRING);
            if($x->child_number==0){
                $sheet->setCellValue("C".$no,$x->nama);
            }else{
                $sheet->setCellValue($letters[$x->child_number].$no,$x->nama);
            }
            if($x->have_child==1){
                $sheet ->getStyle("C".$no.":".$letters[$max].$no)->getFont()->setBold(true);
            }
            
            $sheet->setCellValue($letters[$max+1].$no,$x->nama_tipe);
            $sheet->setCellValue($letters[$max+2].$no,$x->dk==1?"Kredit":"Debet");
            $sheet->setCellValue($letters[$max+3].$no,$x->nilai_saldo_awal);
            $sheet->setCellValue($letters[$max+4].$no,ArrayAdapter::format("date d M Y",$x->tgl_saldo_awal) );              
        }
        $sheet->getStyle($letters[$max+3]."2:".$letters[$max+3].$no)->getNumberFormat()->setFormatCode('#,##0.00');

        $this->setAutoSize($sheet,"A","B");
        $sheet->getColumnDimension($letters[$max])->setAutoSize(true);
        $this->setAutoSize($sheet,$letters[$max+1],$letters[$max+4]);

        header  ('Content-Type: application/vnd.ms-excel');
        header  ('Content-Disposition: attachment;filename="Nomor Akun.xls"' );
        header  ('Cache-Control: max-age=0');
        $writer = PHPExcel_IOFactory::createWriter( $file, 'Excel5' );
        $writer ->save ( 'php://output' );
        return;
        return;
    }

    private function setAutoSize($sheet, $start,$end){
        foreach(range($start,$end) as $columnID) {
            $sheet  ->getColumnDimension($columnID)
                    ->setAutoSize(true);
        }
    }

    
    public function pdf(){
        $this->dbtable->setShowAll(true);
        $data       = $this->dbtable->view("",0);
        $list       = $data['data'];
        /*$tp         = new TablePrint ( "printing-no-perkiraan" );
        $tp         ->setMaxWidth ( false );
        $tp         ->setDefaultBootrapClass ( true );
        $tp         ->addColumn("No.", 1, 1,"center" )
                    ->addColumn("Nomor", 1, 1,"center" )
                    ->addColumn("Nama", 1, 1,"center" )
                    ->addColumn("Tipe Akun", 1, 1,"center" )
                    ->addColumn("Saldo Normal", 1, 1,"center" )
                    ->addColumn("Saldo Awal", 1, 1,"center" )
                    ->addColumn("Update Terakhir", 1, 1,"center" )
                    ->commit("header"); */  
        
        $pdt = "<div style='width:100%; text-align:center;clear:both;'><h2>Nomor Perkiraan</h2></div></br></br></br></br>";
        $pdt .= "<table style='width:100%'>";
        $pdt .= "<tr >";
            $pdt .= "<td style='border-bottom:solid 1px #000'><strong>No.</strong></td>";
            $pdt .= "<td style='border-bottom:solid 1px #000'><strong>Nomor</strong></td>";
            $pdt .= "<td style='border-bottom:solid 1px #000'><strong>Nama</strong></td>";
            $pdt .= "<td style='border-bottom:solid 1px #000'><strong>Tipe Akun</strong></td>";
            $pdt .= "<td style='border-bottom:solid 1px #000'><strong>Saldo Normal</strong></td>";
            $pdt .= "<td colspan='2' style='border-bottom:solid 1px #000'><strong>Saldo Awal</strong></td>";
            $pdt .= "<td style='border-bottom:solid 1px #000'><strong>Update Terakhir</strong></td>";
        $pdt .= "</tr>";

        

        
        $no         = 1;
        $iter       = 0;
        foreach($list as $x){
            $no++;
            $iter++;
            /*
            $tp	->addColumn($iter.".",1, 1,"left")
                ->addColumn($x->nomor,1, 1,"left")
                ->addColumn($x->nama,1, 1,"left")
                ->addColumn($x->nama_tipe,1, 1,"left")
                ->addColumn($x->dk==1?"Kredit":"Debet",1, 1,"left")
                ->addColumn(number_format($x->nilai_saldo_awal,0,",","."),1, 1,"right")
                ->addColumn(ArrayAdapter::format("date d/m/Y",$x->tgl_saldo_awal),1, 1,"left")
                ->commit("body");*/
            $class = "ttab".$x->child_number;
            if($x->have_child){
                $class.=" ttab_parent ";
            }else{
                $class.=" ttab_child ";
            }

            $pdt .= "<tr>";
                $pdt .= "<td style='border-bottom:solid 1px #000'>".$iter."."."</td>";
                $pdt .= "<td style='border-bottom:solid 1px #000'>".$x->nomor."</td>";
                if($x->have_child){
                    $pdt .= "<td style='padding-left:".($x->child_number*20)."px; border-bottom:solid 1px #000' ><strong>".$x->nama."</strong></td>";
                }else{
                    $pdt .= "<td style='padding-left:".($x->child_number*20)."px; border-bottom:solid 1px #000'>".$x->nama." </td>";
                }
               
                
                $pdt .= "<td style='border-bottom:solid 1px #000;'>".$x->nama_tipe."</td>";
                $pdt .= "<td style='text-align:center;border-bottom:solid 1px #000;'>".($x->dk==1?"Kredit":"Debet")."</td>";
                $pdt .= "<td style='text-align:left;border-bottom:solid 1px #000;'>Rp.</td>";
                $pdt .= "<td style='text-align:right;border-bottom:solid 1px #000;'>".number_format($x->nilai_saldo_awal,0,",",".")."</td>";
                $pdt .= "<td style='text-align:right;border-bottom:solid 1px #000;'>".ArrayAdapter::format("date d/m/Y",$x->tgl_saldo_awal)."</td>";
            $pdt .= "</tr>";
        }
        //$print_data =  $tp->getHtml();
        $pdt .= "</table>";

        require_once 'smis-libs-out/mpdf-7.1.0/vendor/autoload.php';
        include 'smis-libs-out/mpdf-7.1.0/src/Mpdf.php';
      
        $mpdf = new \Mpdf\Mpdf(['mode' => 'utf-8', 'format' => 'A4-P']);
        $mpdf->SetTitle('Proposal '.$p_name);
        $print_data = "<html><body>".$pdt."</body></html>";
        $pathfile = "smis-temp/nomor_perkiraan.pdf";
        //$mpdf->WriteHTML(file_get_contents('accounting/resource/css/nomor_perkiraan.css'),1);
        $mpdf->WriteHTML($print_data,0);
        $mpdf->SetDisplayMode('fullpage');
        $mpdf->Output($pathfile, "F");
        return $pathfile;
	}
}

