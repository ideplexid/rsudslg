<?php 
require_once "smis-libs-class/MasterTemplate.php";
require_once "smis-libs-class/MasterSlaveTemplate.php";
require_once "accounting/class/table/AllNotifyTable.php";

class NotifyTemplate extends MasterSlaveTemplate{
    private $is_archive   = true;
    public function setArchive($enable){
        $this->is_archive = $enable;
        return $this;
    }

    public function initialize(){
        $this->setDateTimeEnable(true);
        
        $post = new Button("","","Post");
        $post->setClass("btn btn-success");
        $post->setIcon(" fa fa-download");
        $post->setIsButton(Button::$ICONIC_TEXT);
        $post->setAction("notify.post_all()");

        $un_post = new Button("","","Un Post");
        $un_post->setClass("btn btn-danger");
        $un_post->setIcon(" fa fa-upload");
        $un_post->setIsButton(Button::$ICONIC_TEXT);
        $un_post->setAction("notify.un_post_all()");

        $archive = null;
        $action_archive= "";
        $dbtable = $this->getDBTable();
        if($this->is_archive){
            $archive = new Button("","","Archive");
            $archive->setClass("btn btn-primary");
            $archive->setIcon(" fa fa-upload");
            $archive->setIsButton(Button::$ICONIC_TEXT);
            $archive->setAction("notify.archive_all()");  
            $dbtable->addCustomKriteria(" archive ","='0' ");
            $action_archive = "archive";
        }else{
            $archive = new Button("","","Un Archive");
            $archive->setClass("btn btn-primary");
            $archive->setIcon(" fa fa-upload");
            $archive->setIsButton(Button::$ICONIC_TEXT);
            $archive->setAction("notify.un_archive_all()");
            $dbtable->addCustomKriteria(" archive ","='1' ");
            $action_archive = "un_archive";
        }

        $alltable = new AllNotifyTable(array("No.","Code","Tanggal","Ruangan","Uraian","Nilai","Info","Status"));
        $alltable->setName($this->action);
        
        if($this->getDBResponder()->isView()){
            $dbtable = $this ->getDBTable();
            if(isset($_POST['dari']) && $_POST['dari']!="")
                $dbtable->addCustomKriteria(" tanggal>= ","'".$_POST['dari']."'");
                
            if(isset($_POST['sampai']) && $_POST['sampai']!="")
                $dbtable->addCustomKriteria(" tanggal< ","'".$_POST['sampai']."'");    
            
            if(isset($_POST['ruangan']) && $_POST['ruangan']!="")
                $dbtable->addCustomKriteria(" entity ","='".$_POST['ruangan']."'");
            
            if(isset($_POST['status']) && $_POST['status']!="")
                $dbtable->addCustomKriteria(" status ","='".$_POST['status']."'");
            
            if(isset($_POST['id']) && $_POST['id']!="")
                $dbtable->addCustomKriteria(" id ","='".$_POST['id']."'");

            if(isset($_POST['info']) && $_POST['info']!="")
                $dbtable->addCustomKriteria(" ket "," LIKE '%".$_POST['info']."%'");
                
            if(isset($_POST['action']) && $_POST['action']!="all_notify" && $_POST['action']!="archive_notify"){
                $jenis = str_replace("_notify","",$_POST['action']);
                $dbtable->addCustomKriteria(" jenis_data ","='".$jenis."'");
            }    
        }
        
        $this->setUITable($alltable);
        $responder = $this   ->getDBResponder();
        $responder->setUITable($alltable);
        
        $form   = $this ->getForm();
        $this   ->getUItable()
                ->clearContent();        
        $this   ->setDateEnable(true);
        $this   ->getUItable()
                ->setAddButtonEnable(false)
                ->setDelButtonEnable(true)
                ->setEditButtonEnable(false)
                ->setPrintButtonEnable(false)
                ->setReloadButtonEnable(false)
                ->addHeaderButton($post)
                ->addHeaderButton($un_post)
                ->addHeaderButton($archive)
                ->addContentButton("post_akunting",$post)
                ->addContentButton("un_post_akunting",$un_post)
                ->addContentButton($action_archive,$archive);

        $this   ->getUItable()
                ->addModal("id","hidden","","")
                ->addModal("entity","text","Ruangan","")
                ->addModal("tanggal","datetime","Tanggal","")
                ->addModal("jenis_akun","text","Jenis Akun","")
                ->addModal("jenis_data","text","Jenis Data","")
                ->addModal("status","checkbox","Status","");
                
        $adapter = $this ->getAdapter();
        $adapter->setUseNumber(true,"No.","back.")
                ->add("Tanggal","tanggal","date d M Y H:i")
                ->add("Ruangan","entity","unslug")
                ->add("Code","code")
                ->add("Info","ket")
                ->add("Uraian","uraian")
                ->add("Nilai","nilai","money Rp.")
                ->add("Data","jenis_data","unslug")
                ->add("Transaksi","jenis_akun","unslug")
                ->add("Status","status","trivial_1_<i class='fa fa-check'></i>_");
        
        $this->setModalTitle("Accounting Notify");    
        $this->addResouce("js","accounting/resource/js/notify.js");
        $this->addResouce("js","smis-base-js/smis-base-loading.js");
        $this->addRegulerData("id","notify_id","free-id-value");
        $this->addRegulerData("ruangan","notify_ruangan","free-id-value");
        $this->addRegulerData("status","notify_status","free-id-value");
        $this->addRegulerData("info","notify_info","free-id-value");
        $this->addRegulerData("dari","notify_dari","free-id-value");
        $this->addRegulerData("sampai","notify_sampai","free-id-value");
        $this->setAutoReload(true);
        parent::initialize();
    }
}

?>