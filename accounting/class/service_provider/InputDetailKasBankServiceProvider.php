<?php 

class InputDetailKasBankServiceProvider extends ServiceProvider{
    
    public function save(){
        $result = parent::save();
        $this->reCount();
        return $result;
    }
    
    public function delete(){
        $result = parent::delete();
        $this->reCount();
        return $result;
    }
    
    public function reCount(){
        $query  ="SELECT SUM(debet-kredit) as total
                  FROM smis_ac_transaksi_detail
                  WHERE smis_ac_transaksi_detail.id_transaksi='".$_POST['id_transaksi']."'
                  AND smis_ac_transaksi_detail.prop!='del'
                  AND smis_ac_transaksi_detail.nomor_account!='".$_POST['proceed_akun']."'";
        $nilai  = $this->dbtable->get_db()->get_var($query);
        
        $update                  = array();
        $update['debet']         = $nilai<0?abs($nilai):0;
        $update['kredit']        = $nilai>0?abs($nilai):0;
        $update['nomor_account'] = $_POST['proceed_akun'];
        $update['nama_account']  = $_POST['nama_akun'];
        $update['id_transaksi']  = $_POST['id_transaksi'];
        $update['keterangan']    = $_POST['ket'];
        $update['grup']          = $_POST['grup'];
        $idx                     = array();
        $idx['nomor_account']    = $_POST['proceed_akun'];
        $idx['id_transaksi']     = $_POST['id_transaksi'];
        $this->dbtable->insertOrUpdate($update,$idx);
        
        
        
        $query = "UPDATE smis_ac_transaksi, (
                    SELECT SUM(debet) as debet, SUM(kredit) as kredit
                    FROM smis_ac_transaksi_detail
                    WHERE smis_ac_transaksi_detail.id_transaksi='".$_POST['id_transaksi']."'
                    AND smis_ac_transaksi_detail.prop!='del'
                 ) X
                 SET smis_ac_transaksi.debet=X.debet, smis_ac_transaksi.kredit=X.kredit 
                 WHERE smis_ac_transaksi.id='".$_POST['id_transaksi']."'
                 AND smis_ac_transaksi.prop!='del'
                 ";
        $this->dbtable->get_db()->query($query);
        
        
    }
    
}

?>