<?php 
    class JournalAdapter extends ArrayAdapter{
        private $saldo = 0;
        private $db;
        private $account;
        private $dari;
        private $included=false;
        private $saldo_awal=false;
        private $no_saldo_awal=0;
        private $no_saldo_awal_debet=0;
        private $no_saldo_awal_kredit=0;
        public function setDB($db){
            $this->db=$db;
            return $this;
        }
        
        public function setAccount($account){
            $this->account=$account;
            return $this;
        }
        
        public function setDari($dari){
            $this->dari=$dari;
            return $this;
        }
        
        public function setIncluded($included){
            $this->included=$included;
            return $this;
        }
        
        public function adapt($onerow) {
            $this->number++;
            $array = array ();
            $array ['id_notify']    = $onerow->id_notify;
            $array ['id']           = $onerow->id;
            $array ['No. Ref']      = self::format ( 'only-digit8', $onerow->id );;
            $array ['No.']          = $this->number.".";
            $array ['Tanggal']      = self::format ( 'date d M Y H:i:s', $onerow->tanggal );
            $array ['Nomor']        = $onerow->nomor;
            $array ['Grup']         = self::format("unslug",$onerow->grup);
            $array ['Jenis']        = self::format("unslug",$onerow->jenis);
            $array ['Keterangan']   = $onerow->keterangan;
            $array ['Account']      = $onerow->nomor_account . " " . $onerow->nama_account;
            $array ['Uraian']       = $onerow->uraian;            
            if($onerow->jenis!="saldo_awal"){
                $array ['Debet']             = self::format ( 'money Rp.', $onerow->debet );
                $array ['Kredit']            = self::format ( 'money Rp.', $onerow->kredit );
                $this->no_saldo_awal        += $onerow->debet - $onerow->kredit;
                $this->no_saldo_awal_debet  += $onerow->debet;
                $this->no_saldo_awal_kredit += $onerow->kredit;
            }else{
                $array ['Debet']        = self::format ( 'money Rp.', "0" );
                $array ['Kredit']       = self::format ( 'money Rp.', "0");    
                $this->saldo_awal       = true;            
            }
            $array ['Code']         = $onerow->code;
            $this->saldo            = $this->saldo + ($onerow->debet * 1) - ($onerow->kredit * 1);
            $array ['Saldo Debet']  = $this->saldo > 0 ? self::format ( 'money Rp.', $this->saldo ) : self::format ( 'money Rp.', "0" );
            $array ['Saldo Kredit'] = $this->saldo < 0 ? self::format ( 'money Rp.', $this->saldo * (- 1) ) : self::format ( 'money Rp.', "0" );
            return $array;
        }
        
        public function getSummary($db,$noakun,$dari,$included=true){
            if($db==null || substr($noakun, 0, 1)*1  >=4){
                return 0;
            }
            $query="SELECT SUM(smis_ac_transaksi_detail.debet) as debet, 
                    SUM(smis_ac_transaksi_detail.kredit) as kredit 
                    FROM smis_ac_transaksi
                    LEFT JOIN smis_ac_transaksi_detail 
                    ON smis_ac_transaksi.id = smis_ac_transaksi_detail.id_transaksi
                    WHERE smis_ac_transaksi.tanggal<'".$dari."' 
                    AND smis_ac_transaksi.prop!='del'
                    AND smis_ac_transaksi_detail.prop!='del' 
                    AND smis_ac_transaksi.prop!='del' 
                    AND nomor_account ".($included ?" LIKE '".$noakun."%' ":" = '".$noakun."' ").";";
            $hasil=$db->get_row($query);
            $summary=0;
            if($hasil!=null){
                $summary=$hasil->debet-$hasil->kredit;
            }
            return $summary;        
        }
        
        public function getContent($data){
            $saldo=$this->getSummary($this->db,$this->account,$this->dari,$this->included);
            $this->saldo=$saldo;
            $result=array();
            $result=parent::getContent($data);            
            if($this->saldo_awal===false || $saldo!=0){
                $array                  = array ();
                $array ['Account']      = $this->account;
                $array ['Uraian']       = "Saldo Awal Bulan Lalu";  
                $array ['Debet']        = self::format ( 'money Rp.', "0" );
                $array ['Kredit']       = self::format ( 'money Rp.', "0");          
                $array ['Saldo Debet']  = $saldo > 0 ? self::format ( 'money Rp.', $saldo ) : self::format ( 'money Rp.', "0" );
                $array ['Saldo Kredit'] = $saldo < 0 ? self::format ( 'money Rp.', $saldo * (- 1) ) : self::format ( 'money Rp.', "0" );
                array_unshift($result,$array);                    
            }            
            $array                  = array ();
            $array ['Uraian']       = "<strong>Total</strong>";  
            $array ['Debet']        = $this->no_saldo_awal_debet > 0 ? self::format ( 'money Rp.', $this->no_saldo_awal_debet ) : self::format ( 'money Rp.', "0" );
            $array ['Kredit']       = $this->no_saldo_awal_kredit > 0 ? self::format ( 'money Rp.', $this->no_saldo_awal_kredit  ) : self::format ( 'money Rp.', "0" );    
            $result[]               = $array;
            return $result;
        }
    }
?>