<?php 

class InventarisAdapter extends ArrayAdapter{    
    private $nilai;
	private $penyusutan;
	private $sisa;
	private $sisa_baik;
	private $sisa_rusak;
	private $penyusutan_baik;
	private $penyusutan_rusak;
	private $first_date;
	
	public function __construct($first){
		$this->first_date	= $first;
        $this->number		= 1;
	}
	
	public function adapt($d){
		$a				= array();
        $a['No.']		= $this->number.".";
		$a['Ruang']		= $d['ruang'];
		$a['Barang']	= $d['barang'];
		$a['Kode']		= $d['kode'];
		$a['Masuk']		= self::format("date d M Y", $d['masuk']);
		$a['Durasi']	= $d['durasi']." Tahun";
		$akhir			= add_year($d['masuk'], $d['durasi']);
		$masa			= month_different($d['masuk'], $this->first_date);
		$penyusutan		= $d['harga'] / ($d['durasi']*12);
		$sisa			= $d['harga'] - ($penyusutan*$masa);
        if($sisa<0){
            $sisa=0;
        }
		
		$this->nilai		+= $d['harga'];
		$this->penyusutan	+= $penyusutan;
		$this->sisa			+= $sisa;
		
		$a['Akhir']			= self::format("date d M Y", $akhir);
		$a['Masa']			= $masa." Bulan";
		$a['Kondisi']		= $d['kondisi'];
		$a['Nilai']			= self::format("only-money", $d['harga']);
		$a['Penyusutan']	= self::format("only-money", $penyusutan);
        $a['Sisa']			= self::format("only-money", $sisa);
		
		if($a['Kondisi']=="Baik"){
			$a['Penyusutan Baik']	= $a['Penyusutan'];
			$a['Sisa Baik']			= $a['Sisa'];
			$a['Penyusutan Rusak']	= "";
			$a['Sisa Rusak']		= "";
			$this->penyusutan_baik += $penyusutan;
			$this->sisa_baik	   += $sisa;	
		}else{
			$a['Penyusutan Baik']	 = "";
			$a['Sisa Baik']			 = "";
			$a['Penyusutan Rusak']	 = $a['Penyusutan'];
			$a['Sisa Rusak']	 	 = $a['Sisa'];
			$this->penyusutan_rusak += $penyusutan;
			$this->sisa_rusak		+= $sisa;
		}        
        $a['Account']				 = "";
		$this->number++;
		return $a;
	}
	
	public function getContent($data){
        $data			 		= array_slice($data,0,7);
        $d				 		= parent::getContent($data);		
		$a				 		= array();
		$a['Ruang']		 		= "<strong>Total</strong>";
		$a['Barang']	 		= "";
		$a['Kode']		 		= "";
		$a['Masuk']		 		= "";
		$a['Durasi']	 		= "";
		$a['Akhir']		 		= "";
		$a['Masa']		 		= "";
		$a['Kondisi']	 		= "";
		$a['Nilai']		 		= self::format("only-money", $this->nilai);
		$a['Penyusutan'] 		= self::format("only-money", $this->penyusutan);
		$a['Sisa']		 		= self::format("only-money", $this->sisa);
		$a['Penyusutan Baik']	= self::format("only-money.", $this->penyusutan_baik);
		$a['Sisa Baik']			= self::format("only-money", $this->sisa_baik);
		$a['Penyusutan Rusak']	= self::format("only-money", $this->penyusutan_rusak);
		$a['Sisa Rusak']		= self::format("only-money", $this->sisa_rusak);
        $a['Account']			= "";
		$d[]					= $a;
		return $d;
	}	
}

?>