<?php 
class HutangAdapter extends ArrayAdapter{    
    private $nilai;	
	public function __construct(){
        $this->number   = 1;
        $this->nilai    = 0;
	}
	
	public function adapt($d){
		$a                  = array();
        $a['No.']           = $this->number.".";
		$a['Tanggal']       = ArrayAdapter::format("date d M Y",$d['waktu']);
        $a['Ruang']         = ArrayAdapter::format("unslug",$d['asal_faktur']);;
        $a['Jenis']         = ArrayAdapter::format("unslug",$d['jenis']);
        $a['Keterangan']    = $d['keterangan'];
        $a['No Ref']        = $d['noref'];
        $a['Nilai']         = ArrayAdapter::format("only-money Rp.",$d['nilai']);
        $a['Code']          = $d['code'];
        $this->nilai       += $d['nilai'];
        $dtl                = json_decode($d['json'],true);
        if($dtl!=null){
            $a['Vendor']    = $dtl['nama_vendor'];
        }
		$a['Account']       = "";
		$this->number++;
		return $a;
	}
	
	public function getContent($data){
        $data               = array_slice($data,0,7);
        $d                  = parent::getContent($data);		
		$a                  = array();
		$a['No.']           = "";
		$a['Jenis']         = "<strong>Total</strong>";
        $a['Nilai']         = ArrayAdapter::format("only-money Rp.",$this->nilai);
        $a['Account']       = "";
		$d[]                = $a;
		return $d;
	}	
}
?>