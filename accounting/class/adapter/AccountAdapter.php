<?php 
class AccountAdapter extends ArrayAdapter
{
	public function adapt($d)
	{
		$a = array();
		$a["name"] = $d->nomor . " - " . $d->nama;
		$a["value"] = $d->nomor;
		return $a;
	}
}
?>