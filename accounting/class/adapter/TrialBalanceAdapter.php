<?php 

class TrialBalanceAdapter extends ArrayAdapter {
	private $debet 			= 0;
	private $kredit 		= 0;
	private $sa_debet		= 0;
	private $sa_kredit		= 0;
	private $ta_debet		= 0;
	private $ta_kredit		= 0;
	
	public function adapt($onerow) {
		$array             		 = array ();
		$array ['id']      		 = $onerow->id;
		$array ['Nomor']   		 = $onerow->nomor_account;
		$array ['Nama']    		 = $onerow->nama_account;
		$array ['SA_Debet']      = self::format ( 'money Rp.',$onerow->debet_saldo_awal*1);
		$array ['SA_Kredit']     = self::format ( 'money Rp.',$onerow->kredit_saldo_awal*1);
		$array ['TA_Debet']      = self::format ( 'money Rp.',$onerow->debet_transaksi*1);
		$array ['TA_Kredit']     = self::format ( 'money Rp.',$onerow->kredit_transaksi*1);
		
		$this->sa_debet   += $onerow->debet_saldo_awal;
		$this->sa_kredit  += $onerow->kredit_saldo_awal;
		$this->ta_debet   += $onerow->debet_transaksi;
		$this->ta_kredit  += $onerow->kredit_transaksi;
		
		$grand = $onerow->grand * 1;
		if ($grand > 0) {
			$array ['Debet']    = self::format ( 'money Rp.', $grand );
			$array ['Kredit']   = self::format ( 'money Rp.', 0 );
			$this->debet        += $grand;
		} else {
			$array ['Debet']    = self::format ( 'money Rp.', 0 );
			$array ['Kredit']   = self::format ( 'money Rp.', $grand * (- 1) );
			$this->kredit       += ($grand * (- 1));
		}
		return $array;
	}
	public function getContent($d) {
		$data                   = parent::getContent ( $d );
		$array ['Nama']         = "<strong>Grand Total</strong>";
		$array ['SA_Debet']     = self::format ( 'money Rp.',$this->sa_debet);
		$array ['SA_Kredit']    = self::format ( 'money Rp.',$this->sa_kredit);
		$array ['TA_Debet']     = self::format ( 'money Rp.',$this->ta_debet);
		$array ['TA_Kredit']    = self::format ( 'money Rp.',$this->ta_kredit);
		$array ['Debet']        = "<strong>" . self::format ( 'money Rp.', $this->debet ) . "</strong>";
		$array ['Kredit']       = "<strong>" . self::format ( 'money Rp.', $this->kredit ) . "</strong>";
		$data  []               = $array;
		return $data;
	}
}

?>