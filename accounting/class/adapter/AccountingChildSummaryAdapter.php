<?php 
class AccountingChildSummaryAdapter extends SimpleAdapter{	
	protected $sum_array;
	protected $sum_value;
	protected $sum_format;
    protected $name_transaction;	
	
	public function __construct($use_number=false,$number_name="",$transaction_name="transaction"){
		parent::__construct($use_number,$number_name);
		$this->sum_array		= array();
		$this->sum_value		= array();
		$this->sum_format		= array();
        $this->name_transaction	= $transaction_name;
	}
	
	public function addSummary($name,$dbname,$format="text"){
		$this->sum_array[$name]		= $dbname;
		$this->sum_format[$name]	= $format;
		return $this;
	}
	
	public function addFixValue($name,$value){
		$this->sum_value[$name]		= $value;
		return $this;
	}
	
	public function adapt($d){
		$ar		= array();
		if(is_array($d)){
			$ar = $d;
		}else{
			$ar	= get_object_vars($d);
		}
		$array=parent::adapt($ar);
		foreach($this->sum_array as $name=>$dbname){
			if(!isset($this->sum_value[$name]))
				$this->sum_value[$name]	 = 0;
			$this->sum_value[$name]		+= $ar[$dbname];
		}
		return $array;
	}
	
	public function getContent($data){
		$content					= parent::getContent($data);
		$sum_debit					= $this->sum_value["Debet"];
		$sum_kredit					= $this->sum_value["Kredit"];
		foreach($this->sum_format as $name=>$format){
			if($format=="text") continue;
			$value					= $this->sum_value[$name];
			$this->sum_value[$name]	= self::format($format, $value);
		}
		if($sum_debit!=$sum_kredit){
			$this->sum_value["Debet"]	= "<font id='".$this->name_transaction."_red_block' class='red_text'>".self::format("money Rp.", $sum_debit)."</font>";
			$this->sum_value["Kredit"]	= "<font class='red_text'>".self::format("money Rp.", $sum_kredit)."</font>";
		}
		$content[]	= $this->sum_value;
		return $content;
	}
}
?>