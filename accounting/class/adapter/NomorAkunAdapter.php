<?php
class NomorAkunAdapter extends ArrayAdapter{

    public function adapt($d){
        $this->number++;
        $array                  = array();
        $array['No.']           = $this->number.".";
        $array['id']            = $d->id;
        $array['Nomor']         = $d->nomor;
        $array['Name']          = "<div class='ttab".$d->child_number." ttab".($d->have_child=="1"?"_parent":"_child")."' >".$d->nama."</div>";
        $array['Saldo Normal']  = $d->dk=="1"?"Kredit":"Debit";

        $array["Tipe Akun"]       = $d->nama_tipe;
        $array["Saldo Awal"]      = self::format("money Rp.",$d->nilai_saldo_awal);
        $array["Update Terakhir"] = self::format("date d M Y",$d->tgl_saldo_awal);
        $array["have_child"]      = $d->have_child;
        return $array;        
    }
}