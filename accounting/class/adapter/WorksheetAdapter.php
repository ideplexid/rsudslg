<?php 

class WorksheetAdapter extends ArrayAdapter {
	protected $debet_rl;
	protected $kredit_rl;
    protected $enabled_footer;
    protected $debet_saldo;
	protected $kredit_saldo;
    protected $debet_jp;
	protected $kredit_jp;
    
	protected $debet_nr;
	protected $kredit_nr;
	protected $debet_p;
	protected $kredit_p;
	protected $debet_sa;
	protected $kredit_sa;
	protected $debet_j;
	protected $kredit_j;
	protected $debet_m;
	protected $kredit_m;
    
    protected $debet_lbrg_memorial;
    protected $debet_lbrg_transaction;
    protected $kredit_lbrg_memorial;
    protected $kredit_lbrg_transaction;
    protected $kode_akun_lbrg;
    protected $enable_money;
    
    public function setTotalLabaBerjalan($noakun, $dt,$kt,$dm,$km){
        $this->debet_lbrg_transaction  = $dt;
        $this->kredit_lbrg_transaction = $kt;
        $this->debet_lbrg_memorial  = $dm;
        $this->kredit_lbrg_memorial = $km;
        $this->kode_akun_lbrg       = $noakun;
        $this->enabled_footer       = true;
        $this->enable_money         = true;
        return $this;
    }
    
    public function setMoneyEnable($enabled){
        $this->enable_money=$enabled;
        return $this;
    }
    
    public function setFooterEnabled($enabled){
        $this->enabled_footer=$enabled;
        return $this;
    }
    
    private function getAdder($noakun,$transaction=true,$debet=true){
        if($noakun==$this->kode_akun_lbrg){
            if($transaction){
                if($debet)  return $this->debet_lbrg_transaction;
                else        return $this->kredit_lbrg_transaction;
            } else {
                if($debet)  return $this->debet_lbrg_memorial;
                else        return $this->kredit_lbrg_memorial;
            }
        }
        return 0;
    }
    
	public function money($number) {
		if ($number == 0 || $number == "0" || $number*1<0) {
			return $this->enable_money?"-":0;
		} else {
			return $this->enable_money?self::format ( "money ", $number ):$number;
		}
	}
    
    public function moneyNegate($number){
        if($number*1>=0){
            return $this->money($number);
        }else{
            return $this->enable_money?self::format ( "money ", $number ):abs($number);
        }
    }
    
	public function adapt($d) {
        
        $dt = $this->getAdder($d->nomor_account,true,true);
        $kt = $this->getAdder($d->nomor_account,true,false);
        $dm = $this->getAdder($d->nomor_account,false,true);
        $km = $this->getAdder($d->nomor_account,false,false);
        
        
		$a                                  = array ();
		$a ['id']                           = $d->id;
		$a ['Account']                      = $d->nomor_account . " " . $d->nama_account;
        $a ['Nomor Account']                = $d->nomor_account;
        $a ['dk']                           = $d->dk;
        $a ['Nama Account']                 = $d->nama_account;
        $a ['Sub Name']                     = $d->sub_name;
        $a ['Class Name']                   = $d->class_name;
			
		$a ['Debet Saldo Awal']             = $this->money ( $d->debet_saldo_awal);
		$a ['Kredit Saldo Awal']            = $this->money ( $d->kredit_saldo_awal);
		$a ['Debet Jurnal']                 = $this->money ( $d->debet_transaksi  + $dt);
		$a ['Kredit Jurnal']                = $this->money ( $d->kredit_transaksi + $kt);
		$a ['Debet Penyesuaian']            = $this->money ( $d->debet_memorial   + $dm);
		$a ['Kredit Penyesuaian']           = $this->money ( $d->kredit_memorial  + $km);
        
        $debet_jp                           = ($d->debet_transaksi+$d->debet_memorial   +$dt +$dm );
		$kredit_jp                          = ($d->kredit_transaksi+$d->kredit_memorial +$kt +$km );		
        $debet_saldo                        = ($d->debet_transaksi +$d->debet_saldo_awal+$dt);
		$kredit_saldo                       = ($d->kredit_transaksi + $d->kredit_saldo_awal+$kt);
        $debet                              = $debet_saldo + $d->debet_memorial+$dm;
		$kredit                             = $kredit_saldo + $d->kredit_memorial+$km ;
        
		$a ['Debet JP']                     = $this->money ( $debet_jp );
		$a ['Kredit JP']                    = $this->money ( $kredit_jp );		
        $a ['Debet Saldo']                  = $this->money ( $debet_saldo);
		$a ['Kredit Saldo']                 = $this->money ( $kredit_saldo);        
        
        $a ['Debet Setelah Penyesuaian']    = $this->money ( $debet);
		$a ['Kredit Setelah Penyesuaian']   = $this->money ( $kredit);			
		$this->debet_sa                    += $d->debet_saldo_awal;
		$this->kredit_sa                   += $d->kredit_saldo_awal;
		$this->debet_j                     += $d->debet_transaksi+$dt;
		$this->kredit_j                    += $d->kredit_transaksi+$kt;
		$this->debet_m                     += $d->debet_memorial+$dm;
		$this->kredit_m                    += $d->kredit_memorial+$km;		
        $this->debet_saldo                 += $debet_saldo;
		$this->kredit_saldo                += $kredit_saldo;
        $this->debet_jp                    += $debet_jp;
		$this->kredit_jp                   += $kredit_jp;
        
		if (substr($d->nomor_account, 0, 1)*1 > 3) { //semua pasiva
			$a ['Debet RugiLaba']           = $this->money ( $debet );
			$a ['Kredit RugiLaba']          = $this->money ( $kredit );
			$a ['Debet Neraca']             = $this->money ( 0 );
			$a ['Kredit Neraca']            = $this->money ( 0 );
            
            $a ['FDebet RugiLaba']          = $this->money ( $debet-$kredit );
			$a ['FKredit RugiLaba']         = $this->money ( $kredit-$debet );
			$a ['FDebet Neraca']            = $this->money ( 0 );
			$a ['FKredit Neraca']           = $this->money ( 0 );
			$a ['SNeraca']                  = $this->moneyNegate ( 0 );
			$a ['neraca']                   = 0;
            $a ['SRugiLaba']                = $this->moneyNegate ( $kredit-$debet );
            $a ['labarugi']                 = $kredit-$debet;
            $this->debet_rl                += $debet;
			$this->kredit_rl               += $kredit;
		} else {                                    // aktiva
			$a ['Debet Neraca']             = $this->money ( $debet );
			$a ['Kredit Neraca']            = $this->money ( $kredit );
			$a ['Debet RugiLaba']           = $this->money ( 0 );
			$a ['Kredit RugiLaba']          = $this->money ( 0 );
            $a ['FDebet RugiLaba']          = $this->money ( 0 );
			$a ['FKredit RugiLaba']         = $this->money ( 0 );
			$a ['FDebet Neraca']            = $this->money ( $debet-$kredit );
			$a ['FKredit Neraca']           = $this->money ( $kredit-$debet );
            
            if(substr($d->nomor_account, 0, 1)*1 > 1) {
                $a ['SNeraca']                  = $this->moneyNegate ( $kredit-$debet );
                $a ['neraca']                   = $kredit-$debet;
            } else {
                $a ['SNeraca']                  = $this->moneyNegate ( $debet-$kredit );
                $a ['neraca']                   = $debet-$kredit;
            }
            
            
            $a ['SRugiLaba']                = $this->moneyNegate ( 0 );
            $a ['labarugi']                 = 0;
            $this->debet_n                 += $debet;
			$this->kredit_n                += $kredit;
		}
        return $a;
	}

	public function getContent($d) {
		$data                               = parent::getContent($d);
        if($this->enabled_footer){
            $a ['Nama Account']                 = "Sub Total";
            $a ['Account']                      = "<strong>Sub Total</strong>";
            $a ["Debet Saldo Awal"]             = $this->money ( $this->debet_sa );
            $a ["Kredit Saldo Awal"]            = $this->money ( $this->kredit_sa );
            $a ["Debet Jurnal"]                 = $this->money ( $this->debet_j );
            $a ["Kredit Jurnal"]                = $this->money ( $this->kredit_j );
            $a ["Debet Penyesuaian"]            = $this->money ( $this->debet_m );
            $a ["Kredit Penyesuaian"]           = $this->money ( $this->kredit_m );
            $a ["Debet JP"]                     = $this->money ( $this->debet_jp );
            $a ["Kredit JP"]                    = $this->money ( $this->kredit_jp );
            $a ['Debet Setelah Penyesuaian']    = $this->money ( $this->debet_p );
            $a ['Kredit Setelah Penyesuaian']   = $this->money ( $this->kredit_p );
            $a ['Debet RugiLaba']               = $this->money ( $this->debet_rl );
            $a ['Kredit RugiLaba']              = $this->money ( $this->kredit_rl );
            $a ['Debet Neraca']                 = $this->money ( $this->debet_n );
            $a ['Kredit Neraca']                = $this->money ( $this->kredit_n );        
            $a ['Debet Saldo']                  = $this->money ( $this->debet_saldo);
            $a ['Kredit Saldo']                 = $this->money ( $this->kredit_saldo);        
            $a ['FDebet RugiLaba']              = $this->money ( $this->debet_rl );
            $a ['FKredit RugiLaba']             = $this->money ( $this->kredit_rl );
            $a ['FDebet Neraca']                = $this->money ( $this->debet_n );
            $a ['FKredit Neraca']               = $this->money ( $this->kredit_n );
            $a ['SRugiLaba']                    = $this->moneyNegate ( $this->kredit_rl-$this->debet_rl );
            $a ['labarugi']                     = $this->kredit_rl-$this->debet_rl;
            $a ['SNeraca']                      = $this->moneyNegate ( $this->debet_n-$this->kredit_n );
            $a ['neraca']                       = $this->debet_n-$this->kredit_n;
            $data []                            = $a;
                
            $r                                  = $this->kredit_rl - $this->debet_rl;
            $n                                  = $this->debet_n - $this->kredit_n;
                
            $b ['Nama Account']                 = ($r==0?"Impas":($r > 0 ? "Laba Bersih" : "Rugi Bersih"));
            $b ['Account']                      = "<strong>" . ($r==0?"Impas":($r > 0 ? "Laba Bersih" : "Rugi Bersih")) . "</strong>";
            if($this->debet_sa==$this->kredit_sa){
                $b ["Debet Saldo Awal"]         = "-";
                $b ["Kredit Saldo Awal"]        = "-";
            }else if($this->debet_sa > $this->kredit_sa){
                $b ["Debet Saldo Awal"]         = $this->money ( $this->debet_sa-$this->kredit_sa );
                $b ["Kredit Saldo Awal"]        = "-";
            }else{
                $b ["Debet Saldo Awal"]         = "-";
                $b ["Kredit Saldo Awal"]        = $this->money ($this->kredit_sa-$this->debet_sa) ;
            }
            $b ["Debet Jurnal"]                 = "-";
            $b ["Kredit Jurnal"]                = "-";
            $b ["Debet Penyesuaian"]            = "-";
            $b ["Kredit Penyesuaian"]           = "-";
            $b ["Debet JP"]                     = "-";
            $b ["Kredit JP"]                    = "-";
            $b ['Debet Setelah Penyesuaian']    = "-";
            $b ['Kredit Setelah Penyesuaian']   = "-";
            $b ['Debet RugiLaba']               = $this->money ( $r < 0 ? ($r * - 1) : 0 );
            $b ['Kredit RugiLaba']              = $this->money ( $r > 0 ? $r : 0 );
            $b ['Debet Neraca']                 = $this->money ( $n > 0 ? $n : 0 );
            $b ['Kredit Neraca']                = $this->money ( $n < 0 ? $n * - 1 : 0 );        
            $b ['FDebet RugiLaba']              = $this->money ( $r < 0 ? ($r * - 1) : 0 );
            $b ['FKredit RugiLaba']             = $this->money ( $r > 0 ? $r : 0 );
            $b ['FDebet Neraca']                = $this->money ( $n > 0 ? $n : 0 );
            $b ['FKredit Neraca']               = $this->money ( $n < 0 ? $n * - 1 : 0 );        
            $data []                            = $b;
        }
		return $data;
	}
}

?>