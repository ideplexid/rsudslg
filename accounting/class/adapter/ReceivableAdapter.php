<?php 
class ReceivableAdapter extends SimpleAdapter{
	private $nilai;
	private $rawat_inap;
	private $rawat_jalan;
	private $penunjang;
	
	public function adapt($d){
		$a						= parent::adapt($d);
		$a['Rawat Inap']		= ($d['urjip']=="uri")?$d['nilai']:0;
		$a['Rawat Jalan']		= ($d['urjip']=="urj")?$d['nilai']:0;
		$a['Penunjang']			= ($d['urjip']=="up")?$d['nilai']:0;		
		$this->nilai			+= $d['nilai'];
		$this->rawat_inap		+= ($d['urjip']=="uri")?$d['nilai']:0;
		$this->rawat_jalan		+= ($d['urjip']=="urj")?$d['nilai']:0;
		$this->penunjang		+= ($d['urjip']=="up")?$d['nilai']:0;		
		return $a;
	}
	
	/**
	 * combining data based on Ruangan and Layanan
	 * @param unknown $dt
	 */
	public function resume($dt){
		$result		= array();
		$current	= NULL;
		foreach($dt as $a){
			if($current==NULL ){
				$current = $a;
			}else if($current['Ruangan']==$a['Ruangan'] && $current['Layanan']==$a['Layanan'] ){
				$current['Rawat Inap']	+= $a['Rawat Inap'];
				$current['Rawat Jalan']	+= $a['Rawat Jalan'];
				$current['Penunjang']	+= $a['Penunjang'];
				$current['Total']		+= $a['Total'];
			}else{
				$result[]				 = $current;
				$current				 = $a;
			}
		}
		$result[]						 = $current;
		return $result;
	}
	
	public function refactor($dt){
		$result		= array();
		$current	= NULL;
		foreach($dt as $a){
			$a['Total']			= self::format("money Rp.", $a['Total']);
			$a['Rawat Jalan']	= self::format("money Rp.", $a['Rawat Jalan']);
			$a['Rawat Inap']	= self::format("money Rp.", $a['Rawat Inap']);
			$a['Penunjang']		= self::format("money Rp.", $a['Penunjang']);
			$result[]			= $a;
		}
		return $result;
	}
		
	public function getContent($data){
		$d	= parent::getContent($data);		
		$d	= $this->resume($d);
		$d	= $this->refactor($d);		
		$a					= array();
		$a['Layanan']		= "<strong>Total</strong>";
		$a['Total']			= self::format("money Rp.", $this->nilai);
		$a['Rawat Inap']	= self::format("money Rp.", $this->rawat_inap);
		$a['Rawat Jalan']	= self::format("money Rp.", $this->rawat_jalan);
		$a['Penunjang']		= self::format("money Rp.", $this->penunjang);
		$d[]				= $a;
		return $d;
	}
}
?>