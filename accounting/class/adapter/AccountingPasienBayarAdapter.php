<?php 

class AccountingPasienBayarAdapter extends ArrayAdapter
{
	protected $account;
	protected $option;
	protected $settings;
	public function __construct($account, $setting)
	{
		parent::__construct();
		$this->account = $account;
		$this->number = 1;
		$this->settings = $setting;
	}

	public function adapt($d)
	{
		$a = array();
		$a['No.'] = $this->number . ".";
		$code = ArrayAdapter::format("unslug", $d['code']);
		$code = str_replace("+", " - ", $code);
		$name = new Text("acc_name_" . $this->number, "", $code);
		$name->addClass(" name_lebar ");
		$a['Nama'] = $name->getHtml();

		$dk = new Checkbox("acc_d_" . $this->number, "Debit", "0");
		$dk->addAtribute("data-trig", "acc_k_" . $this->number);
		$dk->addAtribute("data-nilai", "acc_nilai_" . $this->number);
		$dk->addAtribute("data-summary", "debit");
		$dk->addClass(" acc_dk ");
		$a['Debit'] = $dk->getHtml();

		$dk = new Checkbox("acc_k_" . $this->number, "Kredit", "0");
		$dk->addAtribute("data-trig", "acc_d_" . $this->number);
		$dk->addAtribute("data-nilai", "acc_nilai_" . $this->number);
		$dk->addAtribute("data-summary", "kredit");
		$dk->addClass(" acc_dk ");
		$a['Kredit'] = $dk->getHtml();

		$AKUN = "";
		$DK = "";
		if (isset($this->settings[$d['code']])) {
			$AKUN = $this->settings[$d['code']]['akun'];
			$DK = $this->settings[$d['code']]['dk'];
		}

		$money = new Text("acc_nilai_" . $this->number, "", $d['nilai']);
		$money->setClass("acc_money");
		$money->addAtribute("data-dk", $DK);
		$money->addAtribute("data-akun", $AKUN);
		$money->addAtribute("data-code", $d['code']);
		$money->addAtribute("data-number", $this->number);
		$money->addAtribute("data-name-selector", "acc_name_" . $this->number);
		$money->addAtribute("data-debit-selector", "acc_d_" . $this->number);
		$money->addAtribute("data-kredit-selector", "acc_k_" . $this->number);
		$money->addAtribute("data-akun-selector", "acc_account_" . $this->number);
		$money->setDisabled(true);
		$money->setModel(Text::$MONEY);
		$a['Nilai'] = $money->getHtml();

		$ac = new Select("acc_account_" . $this->number, "", $this->account);
		$ac->addAtribute("data-nilai", "acc_nilai_" . $this->number);
		$ac->setClass("acc_akun");
		$a['Account'] = $ac->getHtml();
		$this->number++;
		return $a;
	}
}

?>