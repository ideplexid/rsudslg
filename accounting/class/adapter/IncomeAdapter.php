<?php 

class IncomeAdapter extends ArrayAdapter {
		private $debet;
		private $kredit;
		public function adapt($onerow) {
			$array 				= array ();
			$array ['id'] 		= $onerow->id;
			$array ['Nomor'] 	= $onerow->nomor;
			$array ['Account'] 	= $onerow->nomor_account . " " . $onerow->nama_account;
			$array ['Uraian'] 	= $onerow->uraian;
			$array ['Debet'] 	= self::format ( 'money Rp.', $onerow->debet );
			$array ['Kredit'] 	= self::format ( 'money Rp.', $onerow->kredit );
			
			$this->debet 	+= $onerow->debet;
			$this->kredit 	+= $onerow->kredit;
			return $array;
		}
		public function getContent($d) {
			$data 					= parent::getContent ( $d );
			$array ['Nomor'] 		= "-";
			$array ['Keterangan'] 	= "-";
			$array ['Account'] 		= "-";
			$array ['Uraian'] 		= "<strong>Sub Total</strong>";
			$array ['Debet'] 		= "<strong>" . self::format ( 'money Rp.', $this->debet ) . "</strong>";
			$array ['Kredit'] 		= "<strong>" . self::format ( 'money Rp.', $this->kredit ) . "</strong>";
			$data [] 				= $array;
			
			$lr 					= $this->debet - $this->kredit;
			$l ['Nomor'] 			= "-";
			$l ['Keterangan'] 		= "-";
			$l ['Account'] 			= "-";
			$l ['Uraian'] 			= "Sub Total";
			if ($lr < 0) { 
				$l ['Uraian'] 		= "<strong>Laba Bersih</strong>";
				$lr 				= $lr * (- 1);
				$l ['Debet'] 		= "<strong>" . self::format ( 'money Rp.', 0 ) . "</strong>";
				$l ['Kredit'] 		= "<strong>" . self::format ( 'money Rp.', $lr ) . "</strong>";
			} else {
				$l ['Uraian'] 		= "<strong>Rugi Bersih</strong>";
				$lr 				= $lr;
				$l ['Debet'] 		= "<strong>" . self::format ( 'money Rp.', $lr ) . "</strong>";
				$l ['Kredit']		= "<strong>" . self::format ( 'money Rp.', 0 ) . "</strong>";
			}
			$data [] 				= $l;
			return $data;
		}
	}

?>