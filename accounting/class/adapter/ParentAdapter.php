<?php 

class ParentAdapter extends ArrayAdapter {
		public function adapt($onerow) {
			$array 					= array ();
			$array ['id'] 			= $onerow->id;
			$array ['Tanggal'] 		= self::format ( "date d-M-Y", $onerow->tanggal );
			$array ['Nomor'] 		= $onerow->nomor;
            $array ['Grup'] 		= self::format("unslug",$onerow->grup);
			$array ['Keterangan'] 	= $onerow->keterangan;
			$array ['Debet'] 		= self::format ( "money Rp.", $onerow->debet);
			$array ['Kredit'] 		= self::format ( "money Rp.", $onerow->kredit);
			return $array;
		}
}
?>