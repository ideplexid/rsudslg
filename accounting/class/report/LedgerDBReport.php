<?php 

class LedgerDBReport extends DBReport{
    
    public function excel(){
        if($_POST['format']=="standard"){
            return $this->excel_standard();
        }else if($_POST['format']=="daily"){
            return $this->excel_pertanggal();
        }
    }
    
    public function detail(){
        $dbtable = new DBTable($this->dbtable->get_db(),"smis_ac_transaksi");
        $x       = $dbtable->select($_POST['id']);
        
        $this->dbtable->addCustomKriteria(" id_transaksi ","='".$_POST['id']."'");
        $this->dbtable->setShowAll(true);
        $data    = $this->dbtable->view("",0);
        $list    = $data['data'];        
        $table   = new TablePrint("");
        $table  ->setDefaultBootrapClass(true)
                ->setMaxWidth(false);
        
        $table  ->addColumn("TRANSAKSI NOMOR ",3,1)
                ->addColumn(ArrayAdapter::digitFormat("digit8",$_POST['id']),3,1)
                ->commit("title");
        $table  ->addColumn("Tanggal ",2,1)
                ->addColumn(ArrayAdapter::dateFormat(" date d M Y H:i",$x->tanggal),4,1)
                ->commit("body");      
        $table  ->addColumn("Keterangan ",2,1)
                ->addColumn($x->keterangan,4,1)
                ->commit("body");     
        $table  ->addColumn("Nomor",2,1)
                ->addColumn($x->nomor,4,1)
                ->commit("body");
        $table  ->addColumn("ID Notify",2,1)
                ->addColumn($x->id_notify,4,1)
                ->commit("body");      
        $table  ->addColumn("Grup",2,1)
                ->addColumn($x->grup,4,1)
                ->commit("body");
        $table  ->addColumn("<strong>No.</strong>",1,1)
                ->addColumn("<strong>Nomor</strong>",1,1)
                ->addColumn("<strong>Nama</strong>",1,1)
                ->addColumn("<strong>Keterangan</strong>",1,1)
                ->addColumn("<strong>Debet</strong>",1,1)
                ->addColumn("<strong>Kredit</strong>",1,1)
                ->commit("body");
        $no     = 0;
        foreach($list as $xd){
            $no++;
            $table  ->addColumn($no.".",1,1)
                    ->addColumn($xd->nomor_account,1,1)
                    ->addColumn($xd->nama_account,1,1)
                    ->addColumn($xd->keterangan,1,1)
                    ->addColumn(ArrayAdapter::moneyFormat("only-money",$xd->debet),1,1)
                    ->addColumn(ArrayAdapter::moneyFormat("only-money",$xd->kredit),1,1)
                    ->commit("body");
        }
        $table  ->addColumn("Total",4,1)
                ->addColumn(ArrayAdapter::moneyFormat("only-money",$x->debet),1,1)
                ->addColumn(ArrayAdapter::moneyFormat("only-money",$x->kredit),1,1)
                ->commit("footer");
        return $table->getHtml();
    }
    
    public function excel_pertanggal(){
        $account    = $this->getDetailAccount($_POST['account']);
        date_default_timezone_set ( "Asia/Jakarta" );
        require_once ("smis-libs-out/php-excel/PHPExcel.php");
        
        $file       = new PHPExcel ();
        $sheet      = $file->getActiveSheet();
        $periode    = ArrayAdapter::dateFormat("date d M Y H:i",$_POST['from_date'])." - ".ArrayAdapter::dateFormat("date d M Y H:i",$_POST['to_date']);
        
        $row        = 1;
        $sheet->mergeCells("A".$row.":I".$row)->setCellValue("A".$row,"BUKU BESAR");
        $row++;
        $sheet->mergeCells("A".$row.":I".$row)->setCellValue("A".$row,$periode);
        $row        = $row + 2;
        $sheet->mergeCells("A".$row.":C".$row)->setCellValue("A".$row," ".strtoupper($account->nomor));
        $sheet->mergeCells("D".$row.":G".$row)->setCellValue("D".$row,strtoupper($account->nama));
        $sheet->mergeCells("H".$row.":I".$row)->setCellValue("H".$row,"SALDO");
        $row++;
        
        $sheet->setCellValue("A".$row,"No.");
        $sheet->setCellValue("B".$row,"Tanggal");
        $sheet->setCellValue("C".$row,"Code");
        $sheet->setCellValue("D".$row,"Keterangan");
        $sheet->setCellValue("E".$row,"Dept.");
        $sheet->setCellValue("F".$row,"Debet");
        $sheet->setCellValue("G".$row,"Kredit");
        $sheet->setCellValue("H".$row,"Debet");
        $sheet->setCellValue("I".$row,"Kredit");
        
        $this->dbtable->setShowAll(true);
        $this->dbtable->addCustomKriteria(" tanggal >= " ,"'".$_POST['from_date']."'");
        $this->dbtable->addCustomKriteria(" tanggal < " ,"'".$_POST['to_date']."'");
        $data       = $this->dbtable->view("",0);
        $list       = $data['data'];
        $no         = $row;
        $iter       = 0;
        $summary    = $this->getSummary($_POST['account'],$_POST['from_date'],$_POST['all_child']=="1");
        $saldo_awal = $summary;
        
        if($summary!=0){
            $no++;
            $iter++;
            $sheet->setCellValue("A".$no,"1.");
            $sheet->setCellValue("B".$no,"-");
            $sheet->setCellValue("C".$no,"-");
            $sheet->setCellValue("D".$no,"Saldo Awal Bulan Berjalan");
            $sheet->setCellValue("E".$no,"-");
            $sheet->setCellValue("F".$no,"0");
            $sheet->setCellValue("G".$no,"0");
            $sheet->setCellValue("H".$no,$summary>0?$summary:"0");
            $sheet->setCellValue("I".$no,$summary<0?abs($summary):"0");                
        }
        
        $current_date    = "";
        $sum_debet       = 0;
        $sum_kredit      = 0;
        $counter         = count($list);
        foreach($list as $x){
            $counter--;
            $sumdate     = substr($x->tanggal,0,10);
            if($x->jenis=="saldo_awal"){
                $iter++;
                $no++;
                $summary = $summary+$x->debet-$x->kredit;            
                $sheet->setCellValue("A".$no,$iter.".");
                $sheet->setCellValue("B".$no,ArrayAdapter::dateFormat("date d M Y",$sumdate));
                $sheet->setCellValue("D".$no,"Saldo Awal Bulan Berjalan");
                $sheet->setCellValue("H".$no,$summary>0?$summary:"0");
                $sheet->setCellValue("I".$no,$summary<0?abs($summary):"0");
            }else if( ($sumdate==$current_date || $current_date=="") && $counter>0){
                $sum_debet      += $x->debet;
                $sum_kredit     += $x->kredit;
                $current_date    = $sumdate;
            }else{                
                if($counter==0 && $sumdate!=$current_date){
                    $iter++;
                    $no++;
                    $summary = $summary+$sum_debet-$sum_kredit;            
                    $sheet->setCellValue("A".$no,$iter.".");
                    $sheet->setCellValue("B".$no,ArrayAdapter::dateFormat("date d M Y",$current_date));
                    $sheet->setCellValue("F".$no,$sum_debet);
                    $sheet->setCellValue("G".$no,$sum_kredit);
                    $sheet->setCellValue("H".$no,$summary>0?$summary:"0");
                    $sheet->setCellValue("I".$no,$summary<0?abs($summary):"0");
                    $sum_debet      = $x->debet;
                    $sum_kredit     = $x->kredit; 
                    $current_date   = $sumdate;
                }
                $iter++;
                $no++;
                $summary = $summary+$sum_debet-$sum_kredit;            
                $sheet->setCellValue("A".$no,$iter.".");
                $sheet->setCellValue("B".$no,ArrayAdapter::dateFormat("date d M Y",$current_date));
                $sheet->setCellValue("F".$no,$sum_debet);
                $sheet->setCellValue("G".$no,$sum_kredit);
                $sheet->setCellValue("H".$no,$summary>0?$summary:"0");
                $sheet->setCellValue("I".$no,$summary<0?abs($summary):"0");   
                $sum_debet      = $x->debet;
                $sum_kredit     = $x->kredit;
                $current_date   = $sumdate;
            }            
        }
        $this->footer($sheet,$no);                        
        $this->writingFile($file);
        return;
    }    
    
    public function excel_standard(){
        $account=$this->getDetailAccount($_POST['account']);
        date_default_timezone_set ( "Asia/Jakarta" );
        require_once ("smis-libs-out/php-excel/PHPExcel.php");
        
        $file    = new PHPExcel ();
        $sheet   = $file->getActiveSheet();
        $periode = ArrayAdapter::dateFormat("date d M Y H:i",$_POST['from_date'])." - ".ArrayAdapter::dateFormat("date d M Y H:i",$_POST['to_date']);
        
        $row = 1;
        $sheet->mergeCells("A".$row.":I".$row)->setCellValue("A".$row,"BUKU BESAR");
        $row++;
        $sheet->mergeCells("A".$row.":I".$row)->setCellValue("A".$row,$periode);
        $row = $row + 2;
        $sheet->mergeCells("A".$row.":C".$row)->setCellValue("A".$row," ".strtoupper($account->nomor));
        $sheet->mergeCells("D".$row.":G".$row)->setCellValue("D".$row,strtoupper($account->nama));
        $sheet->mergeCells("H".$row.":I".$row)->setCellValue("H".$row,"SALDO");
        $row++;
        
        $sheet->setCellValue("A".$row,"No.");
        $sheet->setCellValue("B".$row,"Tanggal");
        $sheet->setCellValue("C".$row,"Code");
        $sheet->setCellValue("D".$row,"Keterangan");
        $sheet->setCellValue("E".$row,"Dept.");
        $sheet->setCellValue("F".$row,"Debet");
        $sheet->setCellValue("G".$row,"Kredit");
        $sheet->setCellValue("H".$row,"Debet");
        $sheet->setCellValue("I".$row,"Kredit");
        
        $this->dbtable->setShowAll(true);
        $this->dbtable->addCustomKriteria(" tanggal >= " ,"'".$_POST['from_date']."'");
        $this->dbtable->addCustomKriteria(" tanggal < " ,"'".$_POST['to_date']."'");
        $data       = $this->dbtable->view("",0);
        $list       = $data['data'];
        $no         = $row;
        $iter       = 0;
        $summary    = $this->getSummary($_POST['account'],$_POST['from_date'],$_POST['all_child']=="1");
        $saldo_awal = $summary;
        
        if($summary!=0){
            $no++;
            $iter++;
            $sheet->setCellValue("A".$no,"1.");
            $sheet->setCellValue("B".$no,"-");
            $sheet->setCellValue("C".$no,"-");
            $sheet->setCellValue("D".$no,"Saldo Awal Bulan Berjalan");
            $sheet->setCellValue("E".$no,"-");
            $sheet->setCellValue("F".$no,"0");
            $sheet->setCellValue("G".$no,"0");
            $sheet->setCellValue("H".$no,$summary>0?$summary:"0");
            $sheet->setCellValue("I".$no,$summary<0?abs($summary):"0");                
        }
        
        foreach($list as $x){
            $iter++;
            $no++;
            $summary = $summary+$x->debet-$x->kredit;            
            $sheet->setCellValue("A".$no,$iter.".");
            $sheet->setCellValue("B".$no,ArrayAdapter::dateFormat("date d M Y",$x->tanggal));
            $sheet->setCellValueExplicit("C".$no,$x->nomor, PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->setCellValue("D".$no,$x->keterangan);
            $sheet->setCellValue("E".$no,$x->grup);
            if($x->jenis!="saldo_awal"){
                $sheet->setCellValue("F".$no,$x->debet);
                $sheet->setCellValue("G".$no,$x->kredit);
            }
            $sheet->setCellValue("H".$no,$summary>0?$summary:"0");
            $sheet->setCellValue("I".$no,$summary<0?abs($summary):"0");
        }

        $this->footer($sheet,$no);                        
        $this->writingFile($file);
        return;
    }
    
    
    public function getSummary($noakun,$dari,$included=true){
        if(substr($noakun, 0, 1)*1  >=4){
            return 0;
        }
        $query   = " SELECT SUM(smis_ac_transaksi_detail.debet) as debet, SUM(smis_ac_transaksi_detail.kredit) as kredit 
                    FROM smis_ac_transaksi
                    RIGHT JOIN smis_ac_transaksi_detail 
                    ON smis_ac_transaksi.id = smis_ac_transaksi_detail.id_transaksi
                    WHERE smis_ac_transaksi.tanggal<'".$dari."' 
                    AND smis_ac_transaksi.prop!='del'
                    AND smis_ac_transaksi_detail.prop!='del' 
                    AND nomor_account ".($included?" LIKE '".$noakun."%' ":" = '".$noakun."' ").";";
        $db      = $this->dbtable->get_db();
        $hasil   = $db->get_row($query);
        $summary = 0;
        if($hasil!=null){
            $summary = $hasil->debet-$hasil->kredit;
        }
        return $summary;
    }
    
    
    private function footer($sheet,$no){
        $last_number=$no;
        $no++;
        $sheet->setCellValue("E".$no,"Total");
        $sheet->setCellValue("F".$no,"=SUM(F6:F".$last_number.")");
        $sheet->setCellValue("G".$no,"=SUM(G6:G".$last_number.")");
        $no++;
        $sheet->setCellValue("E".$no,"Mutasi");
        $sheet->setCellValue("F".$no,"=IF(F".($no-1).">G".($no-1).",F".($no-1)."-G".($no-1).",0)");
        $sheet->setCellValue("G".$no,"=IF(G".($no-1).">F".($no-1).",G".($no-1)."-F".($no-1).",0)");
        $no++;
        $sheet->setCellValue("E".$no,"Saldo Awal");
        $sheet->setCellValue("F".$no,"=H6");
        $sheet->setCellValue("G".$no,"=I6");
        $no++;
        $sheet->setCellValue("E".$no,"Saldo Akhir");
        $sheet->setCellValue("F".$no,"=IF(F".($no-2)."+F".($no-1).">G".($no-2)."+G".($no-1).",F".($no-2)."+F".($no-1)."-G".($no-2)."-G".($no-1).",0)");
        $sheet->setCellValue("G".$no,"=IF(F".($no-2)."+F".($no-1)."<G".($no-2)."+G".($no-1).",G".($no-2)."+G".($no-1)."-F".($no-2)."-F".($no-1).",0)");
         
        $thin['borders']['top']['style']        = PHPExcel_Style_Border::BORDER_THIN;
        $all['borders']['allborders']['style']  = PHPExcel_Style_Border::BORDER_THIN;        
        $fillheader['fill']['type']             = PHPExcel_Style_Fill::FILL_SOLID;
        $fillheader['fill']['color']['rgb']     = 'EEEEEE';
        
        $sheet->getStyle("A4:I4")->applyFromArray($thin);
        $sheet->getStyle("A6:I6")->applyFromArray($thin);
        $sheet->getStyle("A".($last_number+1).":I".($last_number+1) )->applyFromArray ( $thin );        
        $sheet->getStyle("A4:I".$last_number )->applyFromArray ( $all );
        $sheet->getStyle('F6:I'.$no)->getNumberFormat()->setFormatCode('#,##0.00');
        $sheet->getStyle('D6:D'.$last_number)->getAlignment()->setWrapText(true);
        $this->setAutoSize($sheet,"A","I");
         
        /*start - BLOCK UNTUK TEXT ALIGN*/
        $center                             = array();
        $center['alignment']                = array();
        $center['alignment']['horizontal']  = PHPExcel_Style_Alignment::HORIZONTAL_CENTER;
        $sheet->getStyle('A1:I4')->applyFromArray($center);
        
        $sheet->getStyle('A1:I4')->getFont()->setBold(true);
        $sheet->getStyle("A1:I2")->getFont()->getColor()->setRGB('2222BB');
        $sheet->getStyle("A1:F1")->getFont()->setSize(15);
    }
    
    private function writingFile($file){
        header  ('Content-Type: application/vnd.ms-excel');
        header  ('Content-Disposition: attachment;filename="Buku Besar.xls"' );
        header  ('Cache-Control: max-age=0');
        $writer = PHPExcel_IOFactory::createWriter( $file, 'Excel5' );
        $writer ->save ( 'php://output' );
        return;
    } 
    
    private function setAutoSize($sheet, $start,$end){
        foreach(range($start,$end) as $columnID) {
            $sheet  ->getColumnDimension($columnID)
                    ->setAutoSize(true);
        }
    }
    
    private function getDetailAccount($account){
        $dbtable = new DBTable($this->getDBTable()->get_db(),"smis_ac_account");
        return $dbtable->select(array("nomor"=>$account));
    }
}

?>