<?php 

class TrialBalanceDBReport extends DBReport{
    
    protected $filename;
    
    public function setFileName($name){
        $this->filename=$name;
        return $this;
    }
   
    
    public function excel(){
        date_default_timezone_set ( "Asia/Jakarta" );
        require_once ("smis-libs-out/php-excel/PHPExcel.php");
        
        $file    = new PHPExcel ();
        $sheet   = $file->getActiveSheet();
        $periode = ArrayAdapter::dateFormat("date d M Y H:i",$_POST['from_date'])." - ".ArrayAdapter::dateFormat("date d M Y H:i",$_POST['to_date']);
        
        $sheet->mergeCells("A1:J1")->setCellValue("A1","NERACA SALDO");
        $sheet->mergeCells("A2:J2")->setCellValue("A2",$periode); 
        
        $sheet->mergeCells("A3:A4")->setCellValue("A3","No.");
        $sheet->mergeCells("B3:B4")->setCellValue("B3","Grup");
        $sheet->mergeCells("C3:C4")->setCellValue("C3","Nomor Rekening");
        $sheet->mergeCells("D3:D4")->setCellValue("D3","Nama Rekening");
        
        $sheet->mergeCells("E3:F3")->setCellValue("E3","Saldo Awal");
        $sheet->mergeCells("G3:H3")->setCellValue("G3","Transaksi");
        $sheet->mergeCells("I3:J3")->setCellValue("I3","Saldo Akhir");
        
        $sheet->setCellValue("E4","Debet");
        $sheet->setCellValue("F4","Kredit");
        $sheet->setCellValue("G4","Debet");
        $sheet->setCellValue("H4","Kredit");
        $sheet->setCellValue("I4","Debet");
        $sheet->setCellValue("J4","Kredit");
        
        $this->dbtable->setShowAll(true);
        $this->dbtable->addCustomKriteria(" tanggal <="," '".$_POST['to_date']."' ");
        $data=$this->dbtable->view("",0);
        $list=$data['data'];
        $no=4;
        $iter=0;
        
        foreach($list as $x){
            $iter++;
            $no++;
             
            $sheet->setCellValue("A".$no,$iter.". ");
            $sheet->setCellValue("B".$no,$x->grup);
            $sheet->setCellValue("C".$no,$x->nomor_account." ");
            $sheet->setCellValue("D".$no,$x->nama_account);            
            $sheet->setCellValue("E".$no,$x->debet_saldo_awal);
            $sheet->setCellValue("F".$no,$x->kredit_saldo_awal);            
            $sheet->setCellValue("G".$no,$x->debet_transaksi);
            $sheet->setCellValue("H".$no,$x->kredit_transaksi);
            $sheet->setCellValue("I".$no,$x->grand>0?$x->grand:0);
            $sheet->setCellValue("J".$no,$x->grand<0?$x->grand*-1:0);
        }
        $last_number=$no;
        $no++;
        $sheet->setCellValue("D".$no,"Total");
        $sheet->setCellValue("E".$no,"=SUM(E5:E".$last_number.")");
        $sheet->setCellValue("F".$no,"=SUM(F5:F".$last_number.")");
        $sheet->setCellValue("G".$no,"=SUM(G5:G".$last_number.")");
        $sheet->setCellValue("H".$no,"=SUM(H5:H".$last_number.")");
        $sheet->setCellValue("I".$no,"=SUM(I5:I".$last_number.")");
        $sheet->setCellValue("J".$no,"=SUM(J5:J".$last_number.")");
        $no++;
        $sheet->setCellValue("D".$no,"Mutasi");
        $sheet->setCellValue("I".$no,"=IF(I".($no-1).">J".($no-1).",I".($no-1)."-J".($no-1).",0)");
        $sheet->setCellValue("J".$no,"=IF(J".($no-1).">I".($no-1).",J".($no-1)."-I".($no-1).",0)");
        
        
        $thin['borders']['top']['style']=PHPExcel_Style_Border::BORDER_THIN;
        $all['borders']['allborders']['style']=PHPExcel_Style_Border::BORDER_THIN;        
        $fillheader['fill']['type']=PHPExcel_Style_Fill::FILL_SOLID;
        $fillheader['fill']['color']['rgb']='EEEEEE';
        
        $sheet->getStyle ( "A3:J3" )->applyFromArray ( $thin );
        $sheet->getStyle ( "A5:J5" )->applyFromArray ( $thin );
        $sheet->getStyle ( "A".($last_number+1).":J".($last_number+1) )->applyFromArray ( $thin );        
        $sheet->getStyle('E4:J'.$no)->getNumberFormat()->setFormatCode('#,##0.00');
        
        $sheet->getColumnDimension("A")->setAutoSize(true);
        $sheet->getColumnDimension("B")->setAutoSize(true);
        $sheet->getColumnDimension("C")->setAutoSize(true);
        $sheet->getColumnDimension("D")->setAutoSize(true);
        $sheet->getColumnDimension("E")->setAutoSize(true);
        $sheet->getColumnDimension("F")->setAutoSize(true);
        $sheet->getColumnDimension("G")->setAutoSize(true);
        $sheet->getColumnDimension("H")->setAutoSize(true);
        $sheet->getColumnDimension("I")->setAutoSize(true);
        $sheet->getColumnDimension("J")->setAutoSize(true);
         
        /*start - BLOCK UNTUK TEXT ALIGN*/
        $center = array();
        $center ['alignment']=array();
        $center ['alignment']['horizontal']=PHPExcel_Style_Alignment::HORIZONTAL_CENTER;
        $sheet->getStyle ( 'A1:J3' )->applyFromArray ($center);
        $sheet->getStyle('A'.($last_number+1).':J'.($last_number+2) )->getFont()->setBold(true);
        
        $sheet->getStyle('A1:J4')->getFont()->setBold(true);
        $sheet->getStyle("A1:J2")->getFont()
                                    ->getColor()->setRGB('2222BB');
        $sheet->getStyle("A1:J1")->getFont()
                                    ->setSize(15);
        
                                
        header ( 'Content-Type: application/vnd.ms-excel' );
        header ( 'Content-Disposition: attachment;filename="'.$this->filename.'.xls"' );
        header ( 'Cache-Control: max-age=0' );
        $writer = PHPExcel_IOFactory::createWriter ( $file, 'Excel5' );
        $writer->save ( 'php://output' );
        return;
    }
    
}

?>