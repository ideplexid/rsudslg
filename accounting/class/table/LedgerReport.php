<?php

class LedgerReport extends Report{
    public function getContentButton($id){
		$bgroup	= new ButtonGroup('noprint');
		if($id=="" || $id=="0") {
			return $bgroup;
		}
		$bgroup->setMax($this->content_button_max, $this->content_button_default);
		if($this->detail_button_enable){
            $detail	  = new Button($this->name."_detail", "","Detail");
            $detail	  ->setAction($this->action.".detail('".$id."')")
                      ->setClass("btn-info")
                      ->setAtribute("data-content='Detail' data-toggle='popover'")
                      ->setIcon("fa fa-eye")
                      ->setIsButton(Button::$ICONIC);
            $bgroup	  ->addElement($detail);
        }

		foreach($this->content_button as  $slug=>$btn){
            if($slug=="repost" && $this->current_data['id_notify']!="0" ){
                $btn ->setAction($this->action.".".$slug."('".$id."')")
                     ->setId($this->name."_".$slug."_".$id)
                     ->setAtribute(" data-notify-id = '".$this->current_data['id_notify']."' ")
                     ->setClass("btn btn-warning re_post_button");
                $bgroup->addElement($btn);
            }
        }
		return $bgroup;
	}
}

?>