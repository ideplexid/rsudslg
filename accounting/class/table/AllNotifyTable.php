<?php 

class AllNotifyTable extends Table{
    
    public function isRemoved(){
        return strpos($this->current_data['Info'],"Removed")!==false;
    }

    public function isIgnored(){
        return strpos($this->current_data['Info'],"Ignored")!==false;
    }

    public function isUpdated(){
        return strpos($this->current_data['Info'],"Updated")!==false;
    }

    public function isEmpty(){
        return $this->current_data['Info']=="";
    }

    public function isPosted(){
        return $this->current_data['Status']!="";
    }

    public function getContentButton($id){
        $bgroup=new ButtonGroup('noprint');
		if($id=="" || $id=="0") 
            return $bgroup;
        $bgroup->setMax(10, $this->content_button_default);
        $unpost = false;
        $post   = false;
        $repost = false;
        
        if($this->isUpdated() || $this->isEmpty() ){
            if(!$this->isPosted()){
                $post   = true;
            }else if($this->isUpdated()){
                $repost = true;
            }else if($this->isEmpty()){
                $unpost = true;
            }
        }else if( ($this->isIgnored() || $this->isRemoved()) && $this->isPosted() ){
            $unpost = true;
        }

        foreach($this->content_button as  $slug=>$btn){
            if($slug=="post_akunting" && ($post || $repost) ){
                $btn->setAction("notify.".$slug."('".$id."')");
                $btn->setId($this->name."_".$slug);
                $btn->setAtribute(" data-notify_id = '".$id."' ");
                if($post){
                    $btn->setValue("Post");
                    $btn->setClass("btn btn-success post_button");
                }else{
                    $btn->setValue("Re Post");
                    $btn->setClass("btn btn-warning post_button");
                }
                $bgroup->addElement($btn);
            }else if($slug=="un_post_akunting" && $unpost){
                $btn->setAction("notify.".$slug."('".$id."')");
                $btn->setId($this->name."_".$slug);
                $btn->setAtribute(" data-notify_id = '".$id."' ");
                $btn->setClass("btn btn-danger un_post_button");
                $bgroup->addElement($btn);
            }else{
                $btn->setAction("notify.".$slug."('".$id."')");
                $bgroup->addElement($btn);
            }
        }
        
        $btn = new Button("","","Detail");
        $btn ->setAction("notify.detail('".$id."')")
             ->setId($this->name."_detail")
             ->setAtribute(" data-notify_id = '".$id."' ")
             ->setClass("btn btn-primary")
             ->setIcon("fa fa-list-alt")
             ->setIsButton(Button::$ICONIC_TEXT);
        $bgroup->addElement($btn);

        if($this->delete_button_enable){
            $delete	 = new Button($this->name."_del", "","Delete");
            $delete	 ->setAction($this->action.".del('".$id."')")
                     ->setClass("btn-danger")
                     ->setAtribute("data-content='Delete' data-toggle='popover'")
                     ->setIcon("fa fa-trash")
                     ->setIsButton(Button::$ICONIC_TEXT);
            $bgroup ->addElement($delete);
        }
        return $bgroup;
    }
}

?>