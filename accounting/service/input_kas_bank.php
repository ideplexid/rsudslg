<?php 
global $db;
if(isset($_POST['command'])){
    $qv =  "SELECT DISTINCT smis_ac_transaksi.id AS id, 
			smis_ac_transaksi.tanggal AS tanggal, 
			smis_ac_transaksi.jenis AS jenis, 
            smis_ac_transaksi.io AS io, 
			smis_ac_transaksi.nomor AS nomor, 
            smis_ac_transaksi.grup AS grup, 
			smis_ac_transaksi.keterangan AS keterangan,
            smis_ac_transaksi_detail.debet as debet,
            smis_ac_transaksi_detail.kredit as kredit
            FROM smis_ac_transaksi
            LEFT JOIN smis_ac_transaksi_detail 
            ON smis_ac_transaksi.id = smis_ac_transaksi_detail.id_transaksi 
            LEFT JOIN smis_ac_account ON smis_ac_transaksi_detail.nomor_account=smis_ac_account.nomor 
			";
	
	$qc =  "SELECT count(DISTINCT smis_ac_transaksi.id) as total
			FROM smis_ac_transaksi
			LEFT JOIN smis_ac_transaksi_detail 
			ON smis_ac_transaksi.id = smis_ac_transaksi_detail.id_transaksi ";

    $dbtable = new DBTable($db,"smis_ac_transaksi");
    $dbtable ->setPreferredQuery(true,$qv,$qc)
             ->setUseWhereForView(true)
             ->addCustomKriteria ( "smis_ac_transaksi.fix ", " ='0' " )
             ->addCustomKriteria ( "smis_ac_transaksi.jenis ", " != 'saldo_awal' " )
             ->addCustomKriteria ( "smis_ac_transaksi_detail.prop ", " NOT LIKE '%del%'" )
             ->addCustomKriteria ( "smis_ac_transaksi_detail.nomor_account ", " = '".$_POST['kode_akun']."'" );
    $service = new ServiceProvider($dbtable);
    if($service->isView() && $_POST['dari']!="" && $_POST['sampai']!=""){
            $dbtable ->addCustomKriteria("smis_ac_transaksi.tanggal >= "," '".$_POST['dari']."' ")
                     ->addCustomKriteria("smis_ac_transaksi.tanggal  < "," '".$_POST['sampai']."' ");;
    }
    $result  = $service->command($_POST['command']);
    if($service->isDel()){
        $del                 = array();
        $del['id_transaksi'] = $_POST['id'];
        $prop['prop']        = "del";
        $dbtable             = new DBTable($db,"smis_ac_transaksi_detail");
        $dbtable->update($prop,$del);
    }
    echo json_encode($result);
}

    
	
?>