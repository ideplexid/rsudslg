<?php 
    $dbtable        = new DBTable ( $db, "smis_ac_grup" );
    $dbtable->setShowAll ( true );
    $dbtable->setOrder ( "nama ASC" );
    $data           = $dbtable->view ( "", 0 );
    $jobd            = new SelectAdapter("nama", "slug");
    $grup          = $jobd->getContent ( $data ['data'] );
    $grup[]        = array("name"=>"","value"=>"");
    echo json_encode($grup);
?>