<?php 
global $db;
$data['jenis_akun'] = $_POST['jenis_akun'];
$data['jenis_data'] = $_POST['jenis_data'];
$data['id_data']    = $_POST['id_data'];
$data['entity']     = $_POST['entity'];
$data['service']    = $_POST['service'];
$data['data']       = $_POST['data'];
$data['code']       = $_POST['code'];
$data['operation']  = $_POST['operation'];
$data['tanggal']    = $_POST['tanggal'];
$data['uraian']     = $_POST['uraian'];
$data['nilai']      = $_POST['nilai'];

$exist['jenis_akun'] = $_POST['jenis_akun'];
$exist['jenis_data'] = $_POST['jenis_data'];
$exist['id_data']    = $_POST['id_data'];
$exist['entity']     = $_POST['entity'];
$exist['code']       = $_POST['code'];

$result = 1;
$dbtable = new DBTable($db,"smis_ac_notify");
if(!$dbtable->is_exist($exist,true) && $_POST['operation']!="del"){
    $dbtable->insert($data);
}else{
    $x = $dbtable->select($exist);
    if($x->status=="0"){
        if($_POST['operation']=='del'){
            $data['ket'] = "<div class='badge badge-inverse'>Ignored</div>";
        }else{
            $data['ket'] = "";
        }
       $dbtable->update($data,$exist);
    }else{
        if($_POST['operation']=='del'){
            $data['ket'] = "<div class='badge badge-important'>Removed</div>";
            $result=0;
        }else{
            $data['ket'] = "<div class='badge badge-warning'>Updated</div>";
        }
       $dbtable->update($data,$exist);
    }
}
echo json_encode($result);

?>
