<?php 
    global $wpdb;
    $view = "CREATE OR REPLACE VIEW smis_ac_error
             AS SELECT smis_ac_transaksi.id AS id, 
             smis_ac_transaksi.tanggal AS tanggal, 
             smis_ac_transaksi.jenis AS jenis, 
             smis_ac_transaksi.nomor AS nomor, 
             smis_ac_transaksi.grup AS grup, 
             smis_ac_transaksi.code AS code, 
             smis_ac_transaksi.keterangan AS keterangan, 
             smis_ac_transaksi.debet as debet,
             smis_ac_transaksi.kredit as kredit,
             smis_ac_transaksi.prop AS prop
             FROM smis_ac_transaksi
             LEFT JOIN smis_ac_transaksi_detail 
             ON smis_ac_transaksi.id = smis_ac_transaksi_detail.id_transaksi 
             WHERE (smis_ac_transaksi.debet!=smis_ac_transaksi.kredit OR smis_ac_transaksi_detail.nomor_account='')
             AND smis_ac_transaksi.jenis!='saldo_awal'
             GROUP BY smis_ac_transaksi.id
             ORDER BY DATE(smis_ac_transaksi.tanggal) DESC, 
             smis_ac_transaksi.nomor DESC;";
    $wpdb->query($view);
?>