<?php

$query = "INSERT INTO `smis_ac_tipe_account` (`id`, `prop`, `tipe`, `saldo_normal`) VALUES
(1, '', 'Kas & Setara Kas', '-1'),
(2, '', 'Piutang usaha', '-1'),
(3, '', 'Piutang lain-lain', '-1'),
(4, '', 'Persediaan', '-1'),
(5, '', 'Aset lancar lainnya', '-1'),
(6, '', 'Aset tetap', '-1'),
(7, '', 'Akumulasi Penyusutan', '1'),
(8, '', 'Investasi', '-1'),
(9, '', 'Investasi entitas asosiasi', '-1'),
(10, '', 'Investasi entitas anak', '-1'),
(11, '', 'Aset tidak lancar lainnya', '-1'),
(12, '', 'Utang usaha', '1'),
(13, '', 'Utang lain-lain jangka pendek', '1'),
(14, '', 'Utang lain-lain jangka pendek la', '1'),
(15, '', 'Utang lain-lain jangka panjang', '1'),
(16, '', 'Ekuitas', '1'),
(17, '', 'Pendapatan', '1'),
(18, '', 'Harga pokok pendapatan', '-1'),
(19, '', 'Pedapatan lain-lain', '1'),
(20, '', 'Beban lain-lain', '-1'),
(21, '', 'Pajak', '-1'),
(22, '', 'Pendapatan Komprehensif Lainnya', '1');";
global $wpdb;
$wpdb->query($query);