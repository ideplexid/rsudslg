$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
var preview_laba_rugi;
$(document).ready(function(){
    var column=new Array('');
    preview_laba_rugi=new ReportAction("preview_laba_rugi","accounting","preview_laba_rugi",column);
    preview_laba_rugi.back=function(){
        var data=this.getRegulerData();
        data['action']  = "worksheet_laba_rugi";
        data['dari']    = $("#preview_laba_rugi_dari").val();
        data['sampai']  = $("#preview_laba_rugi_sampai").val();
        data['grup']    = $("#preview_laba_rugi_grup").val();
        data['format']  = $("#preview_laba_rugi_format").val();
        showLoading();
        //LoadSmisPage(data);
        $.post("",data,function(res){
        $("div#worksheet_laba_rugi").html(res);
            dismissLoading();
        })
    };
    
    preview_laba_rugi.cetak=function(){
        var area = $("#print_table_preview_laba_rugi").html();
        smis_print(area);
    };
});