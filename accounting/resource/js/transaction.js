var transaction;
$(document).ready(function(){
	$(".mydatetime").datetimepicker({ minuteStep: 1});
	$("#transaction_child_nomor_account").select2().on("select2-close", function () {
	    setTimeout(function() {
	        $('.select2-container-active').removeClass('select2-container-active');
	        $(':focus').blur();
	        $("#transaction_child_keterangan").focus();
	    }, 100);
	});

	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
	var child_column=new Array('id','id_transaksi','nomor_account',"keterangan","debet","kredit");
	var parent_column=new Array('id','tanggal','nomor',"keterangan");
	
	transaction=new ParentChildAction("transaction","accounting","transaction",parent_column,child_column,'id_transaksi');
	transaction.parent.view();
	transaction.child.setEnableAutofocus(true);
	transaction.child.setMultipleInput(true);
	transaction.child.setNextEnter();
	$('#transaction_child_add_form').on('shown.bs.modal', function() {
		$('#transaction_child_nomor_account').select2('open');
	});	
    
    transaction.parent.addViewData=function(add){
        add['filter_grup']=$("#transaction_filter_grup").val();
        return add;
    };

	transaction.child.save=function (){
		if(!this.cekSave()){
			return;
		}
		var self=this;
		var a=this.getSaveData();
		showLoading();
		$.ajax({url:"",data:a,type:'post',success:function(res){
			var json=getContent(res);
			if(json==null) {
				$("#"+self.prefix+"_add_form").smodal('hide');
				return;		
			}else if(json.success=="1"){
				$('#transaction_child_nomor_account').select2('open');
				self.view();
				self.clear();
			}
			dismissLoading();
			self.postAction(a['id']);
		}});
		this.aftersave();
	};
	
});