var worksheet_neraca;
$(document).ready(function(){
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
	$('.mydate').datepicker();
	var column=new Array('');
	worksheet_neraca=new ReportAction("worksheet_neraca","accounting","worksheet_neraca",column);
    
    var from_date = $("#worksheet_neraca_dari").val();
    var to_date = $("#worksheet_neraca_sampai").val();
    var grup = $("#worksheet_neraca_grup").val();
    var format = $("#worksheet_neraca_format").val();
    
    if(from_date != ""){
        $("#worksheet_neraca_from_date").val(from_date);
    }
    if(to_date != ""){
        $("#worksheet_neraca_to_date").val(to_date);
    }
    if(grup != ""){
        $("#worksheet_neraca_filter_grup").val(grup);
    }
    if(format != ""){
        $("#worksheet_neraca_filter_format").val(format);
    }
    
	worksheet_neraca.expand=function(){
		if($("#print_table_worksheet_neraca").hasClass("only_income_worksheet_neraca")){
			$("#print_table_worksheet_neraca").removeClass("only_income_worksheet_neraca");
		}else{
			$("#print_table_worksheet_neraca").addClass("only_income_worksheet_neraca");
		}
	};
    worksheet_neraca.addRegulerData=function(a){
        a['filter_grup']=$("#"+this.prefix+"_filter_grup").val();
        a['filter_format']=$("#"+this.prefix+"_filter_format").val();
        return a;
    };
    
    worksheet_neraca.download_format_excel=function(){
        var data=this.getViewData();
        //data['action']="worksheet_format";
        data['action']="excel_neraca";
        data['akun']="neraca";
        download(data);
    };
    
    worksheet_neraca.print_preview=function(){
        var data=this.getViewData();
        data['action']="worksheet_print";
        data['akun']="neraca";
        showLoading();
        $.post("",data,function(res){
            var json=getContent(res);
            smis_print(json);
            dismissLoading();
        });
    };
    
    worksheet_neraca.preview=function(){
        var data=this.getViewData();
        data['page']= "accounting";
        data['action']= "preview_neraca";
        data['prototype_implement']= "";
        data['prototype_slug']= "";
        data['prototype_name']= "";
        showLoading();
        $.post("",data,function(res){
            $("div#worksheet_neraca").html(res);
            dismissLoading();
        })
    };

    worksheet_neraca.addViewData = function(a){
        a['from_date']   = a['from_date']+" 00:00:00";
        a['to_date']     = a['to_date']+" 23:59:59";
        return a;
    };
    
    worksheet_neraca.view();
});