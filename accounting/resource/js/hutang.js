var hutang;
var hutang_settings;
var hutang_select;
$(document).ready(function(){
    hutang_settings=$.parseJSON($("#settings_list_container_hutang").val());
    hutang_select=$("#account_list_container_hutang").val();
    
    $(".mydate").datepicker();
    hutang=new TableAction("hutang","accounting","hutang");
    hutang.addRegulerData=function(reg_data){
        reg_data['dari']=$("#hutang_dari").val();
        reg_data['sampai']=$("#hutang_sampai").val();
        return reg_data;
    };
    hutang.afterview=function (json){
        $("#hutang_list tr").each(function(){
            var no=$(this).eq(0).children().eq(0).html();
            var ruang=$(this).eq(0).children().eq(3).html();
            var jenis=$(this).eq(0).children().eq(4).html();
            var keterangan=$(this).eq(0).children().eq(5).html();
            var noref=$(this).eq(0).children().eq(6).html();
            var nilai=$(this).eq(0).children().eq(7).html().replace(" Rp. ","").unformatMoney();
            var code=$(this).eq(0).children().eq(8).html();
            var vendor=$(this).eq(0).children().eq(9).html();
            var element=$(this).eq(0).children().eq(2);
            if(no==""){
                hutang.createSelect(element,"kredit",no,ruang,jenis,keterangan,noref,Number(nilai),code,vendor);
            }else{
                hutang.createSelect(element,"debit",no,ruang,jenis,keterangan,noref,Number(nilai),code,vendor);
            }
            
        });
        $(".aci_hutang").each(function(){
            var akun=$(this).data("name");
            if(hutang_settings[akun]!=null){
                $(this).val(hutang_settings[akun]);
            }else{
                $(this).val("");
            }
        });
        $(".aci_hutang").select2();
    };
    
    hutang.createSelect=function (element,dk,nomor,ruang,jenis,keterangan,noref,uang,code,vendor){
        var name=ruang+" - "+jenis+" - "+vendor+" - "+code;
        var keterangan=keterangan+" - "+noref;
        if(nomor==""){
            name="Total";
            keterangan=" Persediaan ";
        }
        var hasil="<select data-nomor='"+nomor+"' data-keterangan='"+keterangan+"' data-tag='select' data-name='"+name+"' data-uang='"+uang+"' data-dk='"+dk+"' class='aci_hutang'>"+hutang_select+"</select>";
        $(element).html(hasil);
    };
    
    hutang.saving=function (){
        var a=this.getRegulerData();
        a['command']="saving";
        var all_data={};
        var number=0;
        var message="<ul>";
        var success=true;
        $(".aci_hutang").each(function(){
            if($(this).data("tag")=="select"){
                var one_data={};
                one_data['akun']=$(this).val();
                var akuname_length=one_data['akun'].length+4;
                one_data['dk']=$(this).data("dk");
                one_data['nilai']=$(this).data("uang");
                one_data['name']=$(this).data("name");
                one_data['keterangan']=$(this).data("keterangan");
                one_data['akun_name']=$(this).children("option:selected").text().substring(akuname_length);
                hutang_settings[one_data['name']]=one_data['akun'];
                all_data[number]=one_data;
                if(one_data['akun']==""){
                    success=false;
                    message+="<li>Nomor Akun pada <strong> "+$(this).data("nomor")+" "+one_data['name']+"</strong> Tidak Boleh Kosong</li>";
                }
                number++;
            }
        });
        message+="</ul>";
        if(!success){
            showWarning("Peringatan !!!",message);
            return;
        }
        a['nomor']=$("#"+this.prefix+"_nomor").val();
        a['grup']=$("#"+this.prefix+"_grup").val();
        a['list']=all_data;
        a['settings']=hutang_settings;
        showLoading();
        $.post("",a,function(res){
            var content=getContent(res);
            dismissLoading();
        });
    };
    
    
    
    
});