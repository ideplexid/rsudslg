$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
var preview_neraca_lajur;
$(document).ready(function(){
    var column=new Array('');
    preview_neraca_lajur=new ReportAction("preview_neraca_lajur","accounting","preview_neraca_lajur",column);
    preview_neraca_lajur.back=function(){
        var data=this.getRegulerData();
        data['action']= "worksheet";
        data['dari']= $("#preview_neraca_lajur_dari").val();
        data['sampai']= $("#preview_neraca_lajur_sampai").val();
        data['grup']= $("#preview_neraca_lajur_grup").val();
        showLoading();
        $.post("",data,function(res){
            $("div#worksheet").html(res);
            dismissLoading();
        });
    };
    
    preview_neraca_lajur.cetak=function(){
        var data=this.getRegulerData();
        data['action']= "print_neraca_lajur";
        data['from_date']= $("#preview_neraca_lajur_dari").val();
        data['to_date']= $("#preview_neraca_lajur_sampai").val();
        data['filter_grup']= $("#preview_neraca_lajur_grup").val();
        showLoading();
        $.post("",data,function(res){
            smis_print(res);
            dismissLoading();
        });
    };
    
});



