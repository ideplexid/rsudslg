var inventaris;
var inventaris_settings;
var inventaris_select;
$(document).ready(function(){
    inventaris_settings=$.parseJSON($("#settings_list_container_inventaris").val());
    inventaris_select=$("#account_list_container_inventaris").val();
    
    $(".mydate").datepicker();
    inventaris=new TableAction("inventaris","accounting","inventaris");
    inventaris.addRegulerData=function(reg_data){
        reg_data['dari']=$("#inventaris_dari").val();
        reg_data['sampai']=$("#inventaris_sampai").val();
        return reg_data;
    };
    inventaris.afterview=function (json){
        $("#inventaris_list tr").each(function(){
            var no=$(this).eq(0).children().eq(0).html();
            var ruang=$(this).eq(0).children().eq(2).html();
            var barang=$(this).eq(0).children().eq(3).html();
            var kondisi=$(this).eq(0).children().eq(9).html();
            var nilai=$(this).eq(0).children().eq(11).html().replace(" Rp. ","").unformatMoney();
            var element=$(this).eq(0).children().eq(1);
            if(no==""){
                inventaris.createSelect(element,"Akumulasi Penyusutan",Number(nilai),"debit",no);
            }else{
                inventaris.createSelect(element,ruang+" - "+barang+" - "+kondisi,Number(nilai),"kredit",no);
            }
            
        });
        $(".aci_inventaris").each(function(){
            var akun=$(this).data("name");
            if(inventaris_settings[akun]!=null){
                $(this).val(inventaris_settings[akun]);
            }else{
                $(this).val("");
            }
        });
        $(".aci_inventaris").select2();
    };
    
    inventaris.saving=function (){
        var a=this.getRegulerData();
        a['command']="saving";
        var all_data={};
        var number=0;
        var message="<ul>";
        var success=true;
        $(".aci_inventaris").each(function(){
            if($(this).data("tag")=="select"){
                var one_data={};
                one_data['akun']=$(this).val();
                var akuname_length=one_data['akun'].length+4;
                one_data['dk']=$(this).data("dk");
                one_data['nilai']=$(this).data("uang");
                one_data['name']=$(this).data("name");
                one_data['akun_name']=$(this).children("option:selected").text().substring(akuname_length);
                inventaris_settings[one_data['name']]=one_data['akun'];
                all_data[number]=one_data;
                if(one_data['akun']==""){
                    success=false;
                    message+="<li>Nomor Akun pada <strong> "+$(this).data("nomor")+" "+one_data['name']+"</strong> Tidak Boleh Kosong</li>";
                }
                number++;
            }
        });
        message+="</ul>";
        if(!success){
            showWarning("Peringatan !!!",message);
            return;
        }
        a['nomor']=$("#"+this.prefix+"_nomor").val();
        a['grup']=$("#"+this.prefix+"_grup").val();
        a['list']=all_data;
        a['settings']=inventaris_settings;
        showLoading();
        $.post("",a,function(res){
            var content=getContent(res);
            dismissLoading();
        });
    };
    
    
    
    inventaris.createSelect=function (element,name,uang,dk,nomor){
        var hasil="<select data-nomor='"+nomor+"' data-tag='select' data-name='"+name+"' data-uang='"+uang+"' data-dk='"+dk+"' class='aci_inventaris'>"+inventaris_select+"</select>";
        $(element).html(hasil);
    };
});