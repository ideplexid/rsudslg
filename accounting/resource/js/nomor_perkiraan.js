var nomor_perkiraan;
var parent_nomor_perkiraan;
$(document).ready(function(){
	$(".mydate").datepicker();
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
	var column		= new Array("id","tipe","nama_tipe","dk","id_parent","nama_parent","nomor_parent","nama","nomor","child_number","tgl_saldo_awal","nilai_saldo_awal","have_child");
	nomor_perkiraan	= new TableAction("nomor_perkiraan","accounting","nomor_perkiraan",column);
	nomor_perkiraan.view();
	nomor_perkiraan.more_option = function(x){
		var data 			= this.getRegulerData();
		data['id_induk'] 	= nomor_perkiraan.get("id_parent");
		data['nomor_induk'] = nomor_perkiraan.get("nomor_parent");
		data['action'] 		= "nomor_perkiraan_next";
		showLoading();
		$.post("",data,function(res){
			var json = getContent(res);
			nomor_perkiraan.set("nomor",json);
			dismissLoading();
		});
	};
	nomor_perkiraan.edit=function (id){
		var self		= this;
		var json_obj	= new Array();
		showLoading();	
		var edit_data	= this.getEditData(id);
		$.post('',edit_data,function(res){		
			var json	= getContent(res);
			if(json==null) return;
			for(var i=0;i<self.column.length;i++){
				if($.inArray(self.column[i],self.noclear)!=-1 && !self.edit_clear_for_no_clear){
					continue;
				}
				var name	= self.column[i];
				var the_id	= "#"+self.prefix+"_"+name;
				if( name in self.json_column && self.json_column.length>0){
					/** this function handling if there is a column that part of another column */
					var json_grup_name=self.json_column[name];
					if(json[json_grup_name]==""){
						continue;
					}
					if(!(json_grup_name in json_obj)){
						json_obj[json_grup_name]=$.parseJSON(json[json_grup_name]);
					}
					smis_edit(the_id,json_obj[json_grup_name][""+name]);    
				}else{
					/** this function handling reguler column **/
					smis_edit(the_id,json[""+name]);    
				}
			}
			if(json['have_child']=="1"){
				$("#nomor_perkiraan_nilai_saldo_awal").prop('disabled', true);
			}else{
				$("#nomor_perkiraan_nilai_saldo_awal").prop('disabled', false);
			}
			dismissLoading();
			self.disabledOnEdit(self.column_disabled_on_edit);
			self.show_form();
		});
		return this;
	};

	parent_nomor_perkiraan = new TableAction("parent_nomor_perkiraan","accounting","nomor_perkiraan",column);
	parent_nomor_perkiraan.setSuperCommand("parent_nomor_perkiraan");
	parent_nomor_perkiraan.selected = function(x){
		nomor_perkiraan.set("id_parent",x.id);
		nomor_perkiraan.set("nama_parent",x.nama);
		nomor_perkiraan.set("nomor_parent",x.nomor);
		nomor_perkiraan.set("child_number",Number(x.child_number)+1);
	};
	$("#nomor_perkiraan_tipe").on("change",function(){
		var data = nomor_perkiraan.getRegulerData();
		data['command'] = "get_tipe_account";
		data['id']		= $(this).val();
		showLoading();
		$.post("",data,function(res){
			var json = getContent(res);
			nomor_perkiraan.set("nama_tipe",json.tipe);
			nomor_perkiraan.set("dk",json.saldo_normal);
			dismissLoading();
		});
	});
});