var transaction_masuk;
$(document).ready(function(){
	var startDate = $("#transaction_masuk_start_date").val();
	if(startDate==""){
		startDate="1900-01-01";
	}
	$("#transaction_masuk_parent_tanggal").datetimepicker({ minuteStep: 1,startDate:startDate});
	$("#transaction_masuk_child_nomor_account").select2().on("select2-close", function () {
	    setTimeout(function() {
	        $('.select2-container-active').removeClass('select2-container-active');
	        $(':focus').blur();
	        $("#transaction_masuk_child_keterangan").focus();
	    }, 100);
	});

	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
	var child_column=new Array('id','id_transaksi','nomor_account',"keterangan","debet","kredit","klas","sub");
	var parent_column=new Array('id','tanggal','nomor',"keterangan","grup");
	
	transaction_masuk=new ParentChildAction("transaction_masuk","accounting","transaction_masuk",parent_column,child_column,'id_transaksi');
	transaction_masuk.parent.view();
	transaction_masuk.child.setEnableAutofocus(true);
	transaction_masuk.child.setMultipleInput(true);
	transaction_masuk.child.setNextEnter();
	$('#transaction_masuk_child_add_form').on('shown.bs.modal', function() {
		$('#transaction_masuk_child_nomor_account').select2('open');
	});	
	
	transaction_masuk.parent.pindah=function(id){
		var data=this.getRegulerData();
		bootbox.confirm("Anda Yakin Memindah Transaksi ini ke Transaksi Keluar",function(result){
			if(result){
				data['command']="save";
				data['id']=id;
	
				data['pindah']="pindah";
				showLoading();
				$.post("",data,function(res){
					var json=getContent(res);
					transaction_masuk.parent.view();
					dismissLoading();
				});
			}
		});
	}
    
    transaction_masuk.parent.addViewData=function(add){
        add['filter_grup']=$("#transaction_masuk_filter_grup").val();
        return add;
    };

	transaction_masuk.child.save=function (){
		if(!this.cekSave()){
			return;
		}
		var self=this;
		var a=this.getSaveData();
		showLoading();
		$.ajax({url:"",data:a,type:'post',success:function(res){
			var json=getContent(res);
			if(json==null) {
				$("#"+self.prefix+"_add_form").smodal('hide');
				return;		
			}else if(json.success=="1"){
				$('#transaction_masuk_child_nomor_account').select2('open');
				self.view();
				self.clear();
			}
			dismissLoading();
			self.postAction(a['id']);
		}});
		this.aftersave();
	};
    
    transaction_masuk.parent.addCekSave=function(result){
        if($("#transaction_masuk_red_block").hasClass("red_text")){
            result.good=false;
            result.msg=result.msg+" </br> Transaksi Tidak Balance !!!";
        }
        return result;
    };
	
});