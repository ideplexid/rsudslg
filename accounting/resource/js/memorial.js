var memorial;
$(document).ready(function(){
	var startDate = $("#transaction_memorial_start_date").val();
	if(startDate==""){
		startDate="1900-01-01";
	}
	$(".mydate").datepicker();
	$("#memorial_parent_tanggal").datetimepicker({minuteStep:1,startDate:startDate});
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
	$("#memorial_child_nomor_account").select2().on("select2-close", function () {
	    setTimeout(function() {
	        $('.select2-container-active').removeClass('select2-container-active');
	        $(':focus').blur();
	        $("#memorial_child_keterangan").focus();
	    }, 100);
	});
	var child_column  = new Array('id','id_transaksi','nomor_account',"keterangan","debet","kredit");
	var parent_column = new Array('id','tanggal','nomor',"keterangan","grup");
	
	memorial = new ParentChildAction("memorial","accounting","memorial",parent_column,child_column,'id_transaksi');
	memorial.parent.view();
	memorial.child.setEnableAutofocus(true);
	memorial.child.setMultipleInput(true);
	memorial.child.setNextEnter();
	$('#memorial_child_add_form').on('shown.bs.modal', function() {
		$('#memorial_child_nomor_account').select2('open');
	});	
    
    memorial.parent.addViewData=function(add){
        add['filter_grup']=$("#memorial_filter_grup").val();
        return add;
    };

	memorial.child.save=function (){
		if(!this.cekSave()){
			return;
		}
		var self = this;
		var a	 = this.getSaveData();
		showLoading();
		$.ajax({url:"",data:a,type:'post',success:function(res){
			var json = getContent(res);
			if(json==null) {
				$("#"+self.prefix+"_add_form").smodal('hide');
				return;		
			}else if(json.success=="1"){
				$('#memorial_child_nomor_account').select2('open');
				self.view();
				self.clear();
			}
			dismissLoading();
			self.postAction(a['id']);
		}});
		this.aftersave();
	};
    
    memorial.parent.addCekSave=function(result){
        if($("#memorial_red_block").hasClass("red_text")){
            result.good = false;
            result.msg	= result.msg+" </br> Transaksi Tidak Balance !!!";
        }
        return result;
    };
});