$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
var preview_neraca;
$(document).ready(function(){
    var column=new Array('');
    preview_neraca=new ReportAction("preview_neraca","accounting","preview_neraca",column);
    preview_neraca.back=function(){
        var data=this.getRegulerData();
        //data['page']= "accounting";
        data['action']= "worksheet_neraca";
        data['dari']    = $("#preview_neraca_dari").val();
        data['sampai']  = $("#preview_neraca_sampai").val();
        data['grup']    = $("#preview_neraca_grup").val();
        data['format']  = $("#preview_neraca_format").val();
        showLoading();
        //LoadSmisPage(data);
        $.post("",data,function(res){
        $("div#worksheet_neraca").html(res);
            dismissLoading();
        })
    };
    
    preview_neraca.cetak=function(){
        var area = $("#print_table_preview_neraca").html();
        smis_print(area);
    };
});