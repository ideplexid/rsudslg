var worksheet;
$(document).ready(function(){
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
	$('.mydate').datepicker();
	var column=new Array('');
	worksheet=new ReportAction("worksheet","accounting","worksheet",column);
    
    var from_date = $("#worksheet_dari").val();
    var to_date = $("#worksheet_sampai").val();
    var grup = $("#worksheet_grup").val();
    
    if(from_date != ""){
        $("#worksheet_from_date").val(from_date);
    }
    if(to_date != ""){
        $("#worksheet_to_date").val(to_date);
    }
    if(grup != ""){
        $("#worksheet_filter_grup").val(grup);
    }
    
	worksheet.expand=function(){
		if($("#print_table_worksheet").hasClass("only_income_worksheet")){
			$("#print_table_worksheet").removeClass("only_income_worksheet");
		}else{
			$("#print_table_worksheet").addClass("only_income_worksheet");
		}
	};
    worksheet.addRegulerData=function(a){
        a['filter_grup']=$("#"+this.prefix+"_filter_grup").val();
        return a;
    };
    worksheet.addViewData = function(a){
        a['from_date']   = a['from_date']+" 00:00:00";
        a['to_date']     = a['to_date']+" 23:59:59";
        return a;
    };
    
    worksheet.download_format_excel=function(){
        var data=this.getViewData();
        data['action']="excel_neraca_lajur";
        download(data);
    };
    
    worksheet.print_preview=function(){
        var data=this.getViewData();
        data['action']="worksheet_print_neraca_lajur";
        showLoading();
        $.post("",data,function(res){
            var json=getContent(res);
            smis_print(json);
            dismissLoading();
        });
    };
    
    worksheet.preview=function(){
        var data=this.getViewData();
        data['page']= "accounting";
        data['action']= "preview_neraca_lajur";
        data['prototype_implement']= "";
        data['prototype_slug']= "";
        data['prototype_name']= "";
        showLoading();
        $.post("",data,function(res){
            $("div#worksheet").html(res);
            dismissLoading();
        })
    };
    
    worksheet.view();
});