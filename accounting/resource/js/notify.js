var notify;
$(document).ready(function(){
    $(".mydatetime").datetimepicker({minuteStep:1});
    notify                  = new TableAction("notify","accounting","notify",new Array("id","entity","tanggal","jenis_akun","jenis_data","status","info","ruangan"));
    notify.addRegulerData   = function(a){
        a['action']         = notify.find_id();
        return a;
    };
    notify.find_id  = function(){
        var id      = $("div#notify_header > ul > li.active a").attr("id");
        var idx     = id.replace("_anchor","");
        return idx;
    };
    
    notify.search_find = function(){
        $("#"+notify.find_id()+"_search_button").trigger('click');
    };
    
    notify.post_akunting = function(id){
        var data         = this.getRegulerData();
        data['action']   = "post_accounting";
        data['id']       = id;
        showLoading();
        $.post("",data,function(res){
            var content  = getContent(res);
            $("#"+notify.find_id()+"_search_button").trigger('click');
            dismissLoading();
        });
    };
    
    notify.un_post_akunting = function(id){
        var data            = this.getRegulerData();
        data['action']      = "un_post_accounting";
        data['id']          = id;
        showLoading();
        $.post("",data,function(res){
            var content     = getContent(res);
            $("#"+notify.find_id()+"_search_button").trigger('click');
            dismissLoading();
        });
    };

    notify.archive = function(id){
        var data         = this.getRegulerData();
        data['command']  = "save";
        data['archive']  = "1";
        data['id']       = id;
        showLoading();
        $.post("",data,function(res){
            var content  = getContent(res);
            $("#"+notify.find_id()+"_search_button").trigger('click');
            dismissLoading();
        });
    };
    
    notify.un_archive = function(id){
        var data            = this.getRegulerData();
        data['command']     = "save";
        data['archive']     = "0";
        data['id']          = id;
        showLoading();
        $.post("",data,function(res){
            var content     = getContent(res);
            $("#"+notify.find_id()+"_search_button").trigger('click');
            dismissLoading();
        });
    };
    
    notify.proceed_loop = function(list_id,data,current){
        if(!smis_loader.isShown() || current>=list_id.length){
            smis_loader.hideLoader();
            $("#"+notify.find_id()+"_search_button").trigger('click');
            return;
        }
        data['id']      = list_id[current];
        current++;
        $.post("",data,function(res){
            smis_loader.updateLoader('true',"Processing... "+current+" / "+list_id.length,current*100/list_id.length);
            notify.proceed_loop(list_id,data,current);
        });
    };

    notify.archive_all     = function(){        
        var list_id     = new Array();
        $( "table#table_"+notify.find_id()+" .post_button" ).each(function(e) {
            var x       = $(this).data("notify_id");
            list_id.push(x);
        });
        smis_loader.updateLoader('true',"Processing... ",0);
        smis_loader.showLoader();
        var data        = this.getRegulerData();
        data['command']  = "save";
        data['archive']  = "1";
        this.proceed_loop(list_id,data,0);
    };

    notify.un_archive_all     = function(){        
        var list_id     = new Array();
        $( "table#table_"+notify.find_id()+" .post_button" ).each(function(e) {
            var x       = $(this).data("notify_id");
            list_id.push(x);
        });
        smis_loader.updateLoader('true',"Processing... ",0);
        smis_loader.showLoader();
        var data        = this.getRegulerData();
        data['command']  = "save";
        data['archive']  = "0";
        this.proceed_loop(list_id,data,0);
    };
    
    notify.post_all     = function(){        
        var list_id     = new Array();
        $( "table#table_"+notify.find_id()+" .post_button" ).each(function(e) {
            var x       = $(this).data("notify_id");
            list_id.push(x);
        });
        smis_loader.updateLoader('true',"Processing... ",0);
        smis_loader.showLoader();
        var data        = this.getRegulerData();
        data['action']  = "post_accounting";
        this.proceed_loop(list_id,data,0);
    };
        
    notify.un_post_all  = function(){
        var list_id     = new Array();
        $( "table#table_"+notify.find_id()+" .un_post_button" ).each(function(e) {
            var x       = $(this).data("notify_id");
            list_id.push(x);
        });
        smis_loader.updateLoader('true',"Processing... ",0);
        smis_loader.showLoader();
        var data        = this.getRegulerData();
        data['action']  = "un_post_accounting";
        this.proceed_loop(list_id,data,0);
    };
    
    notify.detail       = function(id){
        var data        = this.getRegulerData();
        data['command'] = 'detail';
        data['action']  = 'preview_post_accounting';
        data['id']      = id;
        showLoading();
        $.post("",data,function(res){
            var json    = getContent(res);
            dismissLoading();
            showWarning("Detail Of "+id,json);
        });
    };    
});