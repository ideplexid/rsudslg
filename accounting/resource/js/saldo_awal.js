var saldo_awal;
$(document).ready(function(){
	$(".mydatetime").datetimepicker({ minuteStep: 1});
	$("#saldo_awal_child_nomor_account").select2().on("select2-close", function () {
	    setTimeout(function() {
	        $('.select2-container-active').removeClass('select2-container-active');
	        $(':focus').blur();
	        $("#saldo_awal_child_keterangan").focus();
	    }, 100);
	});

	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
	var child_column=new Array('id','id_transaksi','nomor_account',"keterangan","debet","kredit");
	var parent_column=new Array('id','tanggal','nomor',"keterangan","grup");
	
	saldo_awal=new ParentChildAction("saldo_awal","accounting","saldo_awal",parent_column,child_column,'id_transaksi');
	saldo_awal.parent.view();
	saldo_awal.child.setEnableAutofocus(true);
	saldo_awal.child.setMultipleInput(true);
	saldo_awal.child.setNextEnter();
	$('#saldo_awal_child_add_form').on('shown.bs.modal', function() {
		$('#saldo_awal_child_nomor_account').select2('open');
	});	
    
    saldo_awal.parent.addViewData=function(add){
        add['filter_grup']=$("#saldo_awal_filter_grup").val();
        return add;
    };

	saldo_awal.child.save=function (){
		if(!this.cekSave()){
			return;
		}
		var self=this;
		var a=this.getSaveData();
		showLoading();
		$.ajax({url:"",data:a,type:'post',success:function(res){
			var json=getContent(res);
			if(json==null) {
				$("#"+self.prefix+"_add_form").smodal('hide');
				return;		
			}else if(json.success=="1"){
				$('#saldo_awal_child_nomor_account').select2('open');
				self.view();
				self.clear();
			}
			dismissLoading();
			self.postAction(a['id']);
		}});
		this.aftersave();
	};
	
});