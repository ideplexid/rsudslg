/**
 * digunakan khusus cash flow untuk
 * pembuatan laporan aliran cash
 * 
 * @author 		: Nurul Huda
 * @copyright 	: goblooge@gmail.com
 * @license 	: LGPLv2
 * @used		: /accounting/resource/php/transaksi_harian/cash_flow.php
 * @since		: 5 Sep 2017
 * @version		: 1.0.0
 * 
 * */

var cash_flow;
$(document).ready(function(){
	$('.mydate').datepicker();
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
	var column=new Array('id','nama','nilai');	
	cash_flow=new TableAction("cash_flow","accounting","cash_flow",column);
	cash_flow.addRegulerData=function(a){
        a['dari']=$("#"+this.prefix+"_dari").val();
        a['sampai']=$("#"+this.prefix+"_sampai").val();
        return a;
    };
});