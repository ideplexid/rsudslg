var beban;
var beban_settings;
var beban_select;
$(document).ready(function(){
    beban_settings=$.parseJSON($("#settings_list_container_beban").val());
    beban_select=$("#account_list_container_beban").val();
    
    $(".mydate").datepicker();
    beban=new TableAction("beban","accounting","beban");
    beban.addRegulerData=function(reg_data){
        reg_data['dari']=$("#beban_dari").val();
        reg_data['sampai']=$("#beban_sampai").val();
        return reg_data;
    };
    beban.afterview=function (json){
        $("#beban_list tr").each(function(){
            var no=$(this).eq(0).children().eq(0).html();
            var ruang=$(this).eq(0).children().eq(3).html();
            var jenis=$(this).eq(0).children().eq(4).html();
            var keterangan=$(this).eq(0).children().eq(5).html();
            var noref=$(this).eq(0).children().eq(6).html();
            var nilai=$(this).eq(0).children().eq(7).html().replace(" Rp. ","").unformatMoney();
            var code=$(this).eq(0).children().eq(8).html();
            var vendor=$(this).eq(0).children().eq(9).html();
            var element=$(this).eq(0).children().eq(2);
            if(no==""){
                beban.createSelect(element,"debit",no,ruang,jenis,keterangan,noref,Number(nilai),code,vendor);
            }else{
                beban.createSelect(element,"kredit",no,ruang,jenis,keterangan,noref,Number(nilai),code,vendor);
            }
            
        });
        $(".aci_beban").each(function(){
            var akun=$(this).data("name");
            if(beban_settings[akun]!=null){
                $(this).val(beban_settings[akun]);
            }else{
                $(this).val("");
            }
        });
        $(".aci_beban").select2();
    };
    
    beban.createSelect=function (element,dk,nomor,ruang,jenis,keterangan,noref,uang,code,vendor){
        var name=ruang+" - "+jenis+" - "+vendor+" - "+code;
        var keterangan=keterangan+" - "+noref;
        if(nomor==""){
            name="Total";
            keterangan=" Persediaan ";
        }
        var hasil="<select data-nomor='"+nomor+"' data-keterangan='"+keterangan+"' data-tag='select' data-name='"+name+"' data-uang='"+uang+"' data-dk='"+dk+"' class='aci_beban'>"+beban_select+"</select>";
        $(element).html(hasil);
    };
    
    beban.saving=function (){
        var a=this.getRegulerData();
        a['command']="saving";
        var all_data={};
        var number=0;
        var message="<ul>";
        var success=true;
        $(".aci_beban").each(function(){
            if($(this).data("tag")=="select"){
                var one_data={};
                one_data['akun']=$(this).val();
                var akuname_length=one_data['akun'].length+4;
                one_data['dk']=$(this).data("dk");
                one_data['nilai']=$(this).data("uang");
                one_data['name']=$(this).data("name");
                one_data['keterangan']=$(this).data("keterangan");
                one_data['akun_name']=$(this).children("option:selected").text().substring(akuname_length);
                beban_settings[one_data['name']]=one_data['akun'];
                all_data[number]=one_data;
                if(one_data['akun']==""){
                    success=false;
                    message+="<li>Nomor Akun pada <strong> "+$(this).data("nomor")+" "+one_data['name']+"</strong> Tidak Boleh Kosong</li>";
                }
                number++;
            }
        });
        message+="</ul>";
        if(!success){
            showWarning("Peringatan !!!",message);
            return;
        }
        a['nomor']=$("#"+this.prefix+"_nomor").val();
        a['grup']=$("#"+this.prefix+"_grup").val();
        a['list']=all_data;
        a['settings']=beban_settings;
        showLoading();
        $.post("",a,function(res){
            var content=getContent(res);
            dismissLoading();
        });
    };
    
    
    
    
});