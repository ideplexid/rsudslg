var accounting_pasien_bayar;
var accounting_already_added=false;
$(document).ready(function(){
    $(".mydatetime").datetimepicker({ minuteStep: 1});
    $('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
    formatMoney("#accounting_pasien_bayar_debit");
    formatMoney("#accounting_pasien_bayar_kredit");
    formatMoney("#accounting_pasien_bayar_sisa");
    accounting_pasien_bayar=new TableAction("accounting_pasien_bayar","accounting","accounting_pasien_bayar",new Array());
    accounting_pasien_bayar.addViewData=function(d){
        d['noreg_pasien']=$("#accounting_pasien_bayar_noreg_pasien").val();
        return d;
    };
    
    accounting_pasien_bayar.acc_dk=function(){
        $( "#accounting_pasien_bayar_list .acc_dk" ).on("change",function() {
            var idx = $(this).data("trig");
            var nilai=$(this).data("nilai");
            var summary=$(this).data("summary");
            if($(this).is(":checked")){
                $( "#"+idx ).prop( "checked", false);
                $("#"+nilai).data("dk",summary);
            }else{
                $( "#"+idx ).prop( "checked", true);
                summary=$("#"+idx ).data("summary");
                $("#"+nilai).data("dk",summary);
                
            }
            accounting_pasien_bayar.acc_counter();
        });
    };
    
    accounting_pasien_bayar.acc_format=function(){
        $( "#accounting_pasien_bayar_list .acc_money" ).each(function() {
            var idx = $(this).attr("id");
            formatMoney("#"+idx);
        });
    };
    
    accounting_pasien_bayar.acc_akun=function(){
        $("#accounting_pasien_bayar_list .acc_akun").each(function(e){
            var nilai=$(this).data("nilai");
            var id=$(this).attr("id");
            var number=Number($("#"+nilai).data("number"));
            $("#"+id).select2().on("change", function () {
                setTimeout(function() {
                    $('.select2-container-active').removeClass('select2-container-active');
                    $(':focus').blur();
                    $("#acc_account_"+(number+1)).select2('open');
                    $("#"+nilai).data("akun",$("#"+id).val());
                }, 100);
            });
        });
    };
    
    accounting_pasien_bayar.acc_counter=function(){
        var debit=0;
        var kredit=0;
        $( "#accounting_pasien_bayar_list .acc_money" ).each(function() {
            var dk=$(this).data("dk");
            var akun=$(this).data("akun");
            var selector=$(this).data("akun-selector");
            var debit_selector=$(this).data("debit-selector");
            var kredit_selector=$(this).data("kredit-selector");
            
            var uang=getMoney(this);
            if(dk=="debit"){
                debit+=uang;
                $("#"+debit_selector).prop( "checked", true);
            }else if(dk=="kredit"){
                kredit+=uang;
                $("#"+kredit_selector).prop( "checked", true);
            }
            $("#"+selector).select2('val',null);
            $("#"+selector).select2('val',akun);
            
        });
        setMoney("#accounting_pasien_bayar_debit",debit);
        setMoney("#accounting_pasien_bayar_kredit",kredit);
    };



    accounting_pasien_bayar.checkall=function(){
        var tgl=$("#accounting_pasien_bayar_tanggal").val();
        var nomor=$("#accounting_pasien_bayar_nomor").val();
        var sisa=getMoney("#accounting_pasien_bayar_sisa");

        var result="<ul>";
        var success=true;
        $("#accounting_pasien_bayar_debit").removeClass("error_field");
        $("#accounting_pasien_bayar_kredit").removeClass("error_field");
        $("#accounting_pasien_bayar_sisa").removeClass("error_field");
        $("#accounting_pasien_bayar_tanggal").removeClass("error_field");
        $("#accounting_pasien_bayar_nomor").removeClass("error_field");

        if(sisa!=0 && accounting_pasien_bayar==false){
            success=false;
            result+="<li>Terdapat Sisa Pembayaran yang Belum Lunas. "+
                "Tidak Bisa Dimasukan dengan Transaksi Auto, "+
                "Karena dipastikan tidak akan balance.</li>";
            $("#accounting_pasien_bayar_sisa").addClass("error_field");
        }
        
        var debit=getMoney("#accounting_pasien_bayar_debit");
        var kredit=	getMoney("#accounting_pasien_bayar_kredit");
        if(debit!=kredit){
            success=false;
            result+="<li>Debit dan Kredit Tidak Balance, "+
            "Pastikan Debit Kredit Balance dengan Memindahkan Antara Debit dan Kredit</li>";
            $("#accounting_pasien_bayar_debit").addClass("error_field");
            $("#accounting_pasien_bayar_kredit").addClass("error_field");
        }
        
        if(tgl=="" || tgl=="0000-00-00 00:00:00" || tgl=="0000-00-00"){
            success=false;
            result+="<li>Silakan Isi Tanggal</li>";
            $("#accounting_pasien_bayar_tanggal").addClass("error_field");
        }

        if(nomor==""){
            success=false;
            result+="<li>Silakan Isi Nomor Transaksi atau Isi dengan tanda strip (-) untuk Memberikan Nomor Otomatis </li>";
            $("#accounting_pasien_bayar_nomor").addClass("error_field");
        }
        
        

        $( "#accounting_pasien_bayar_list .acc_money" ).each(function() {
            var dk=$(this).data("dk");
            var akun=$(this).data("akun");
            var number=$(this).data("number");
            var akun_selector=$(this).data("akun-selector");
            var name_selector=$(this).data("name-selector");
            var debit_selector=$(this).data("debit-selector");
            var kredit_selector=$(this).data("kredit-selector");
            $("#"+debit_selector).removeClass("error_field");
            $("#"+kredit_selector).removeClass("error_field");
            $("#"+akun_selector).removeClass("error_field");
            $("#"+name_selector).removeClass("error_field");
            if(dk==""){
                success=false;
                result+="<li>Pada Nomor "+number+". Debit atau Kredit Belum di Set</li>";
                $("#"+debit_selector).addClass("error_field");
                $("#"+kredit_selector).addClass("error_field");
            }
            if(akun==""){
                success=false;
                result+="<li>Pada Nomor "+number+". Akun Belum di Set</li>";
                $("#"+akun_selector).addClass("error_field");
            }
            if($("#"+name_selector).val()==""){
                success=false;
                result+="<li>Pada Nomor "+number+". Nama Belum di Set</li>";
                $("#"+name_selector).addClass("error_field");
            }
        });
        result+="</ul>";
        if(!success){
            showWarning("Terdapat Kesalahan",result);
        }
        return success;
    };

    accounting_pasien_bayar.saveall=function(){
        if(this.checkall()){
            var data=this.getRegulerData();
            data['command']='saveall';
            data['noreg_pasien']=$("#accounting_pasien_bayar_noreg_pasien").val();
            data['nrm_pasien']=$("#accounting_pasien_bayar_nrm_pasien").val();
            data['nama_pasien']=$("#accounting_pasien_bayar_nama_pasien").val();
            data['keterangan']=$("#accounting_pasien_bayar_keterangan").val();
            data['tanggal']=$("#accounting_pasien_bayar_tanggal").val();
            data['nomor']=$("#accounting_pasien_bayar_nomor").val();
            data['grup']=$("#accounting_pasien_bayar_grup").val();

            var ldt=new Array();
            $( "#accounting_pasien_bayar_list .acc_money" ).each(function() {			
                var dt={};
                dt['dk']=$(this).data("dk");
                dt['akun']=$(this).data("akun");
                dt['code']=$(this).data("code");
                dt['name']=$("#"+($(this).data("name-selector"))).val();
                var akuname_length=dt['akun'].length+4;
                var akun_selector=$(this).data("akun-selector");
                dt['akun_name']=$("#"+akun_selector+" option:selected").text().substring(akuname_length);
                dt['money']=getMoney(this);
                ldt.push(dt);
            });
            data['data']=JSON.stringify(ldt);
            
            showLoading();
            $.post("",data,function(res){
                var json=getContent(res);
                if(json=="1"){
                    $("#accounting_pasien_bayar_list").html("");
                    setMoney("#accounting_pasien_bayar_sisa",0);
                    setMoney("#accounting_pasien_bayar_debit",0);
                    setMoney("#accounting_pasien_bayar_kredit",0);
                    $("#accounting_pasien_bayar_noreg_pasien").val("");
                    $("#accounting_pasien_bayar_nama_pasien").val("");
                    $("#accounting_pasien_bayar_nomor").val("");
                    $("#accounting_pasien_bayar_nrm_pasien").val("");
                    $("#accounting_pasien_bayar_keterangan").val("");
                    $("#accounting_pasien_bayar_tanggal").val("");
                    $("#accounting_pasien_bayar_noreg_pasien").focus();
                    accounting_already_added=false;
                }else{

                }
                dismissLoading();
            });
        }
    };

    accounting_pasien_bayar.loading=function(){
        var a=this.getViewData();
        showLoading();
        $.post("",a,function(res){
            var content=getContent(res);
            $("#accounting_pasien_bayar_list").html(content.body_content);
            setMoney("#accounting_pasien_bayar_sisa",content.total);
            $("#accounting_pasien_bayar_nama_pasien").val(content.nama_pasien);
            $("#accounting_pasien_bayar_nrm_pasien").val(content.nrm_pasien);
            $("#accounting_pasien_bayar_keterangan").val(content.keterangan);
            $("#accounting_pasien_bayar_tanggal").val(content.tanggal);
            accounting_pasien_bayar.acc_format();
            accounting_pasien_bayar.acc_akun();
            accounting_pasien_bayar.acc_dk();
            accounting_pasien_bayar.acc_counter();
            dismissLoading();
            $("#accounting_pasien_bayar_nomor").val("-");
            $("#accounting_pasien_bayar_nomor").focus();
            accounting_already_added=false;
        });
    };
    
    accounting_pasien_bayar.recount=function(replacer_s){
        formatMoney("#acc_nilai"+replacer_s);
        var debit=getMoney("#accounting_pasien_bayar_debit");
        var kredit=getMoney("#accounting_pasien_bayar_kredit");
        var nilai=debit-kredit;
        var uang=0;
        if(nilai>0){
            $("#acc_k"+replacer_s).prop( "checked", true);
            setMoney("#acc_nilai"+replacer_s,nilai);
            $("#acc_k"+replacer_s).trigger("change");
        }else{
            nilai=nilai*(-1);
            $("#acc_d"+replacer_s).prop( "checked", true);
            setMoney("#acc_nilai"+replacer_s,nilai);
            $("#acc_d"+replacer_s).trigger("change");
        }
        
    };
    
    accounting_pasien_bayar.newitem=function(){        
        if(accounting_already_added){
            smis_alert("Pemberitahuan","Sudah Ditambahkan Satu Baris </br>Pembantu untuk transaksi yang belum lunas !","alert-success");
            var l=$("#accounting_pasien_bayar_list").children().length;
            var replacer_s="_"+(l);
            accounting_pasien_bayar.recount(replacer_s);
            return;
        }
        
        accounting_already_added=true;
        var l=$("#accounting_pasien_bayar_list").children().length;
        var last_s="_"+l;
        var replacer_s="_"+(l+1);
        var num_s="<td>"+(l)+".</td>";
        var num_r="<td>* "+(l+1)+".</td>";
        var children=$("#accounting_pasien_bayar_list").children();
        var last=$("#accounting_pasien_bayar_list tr:nth-last-child(1)").html();
        last=replaceAll(last_s,replacer_s,last);
        last=replaceAll(num_s,num_r,last);
        var str="<tr class='success'>"+last+"</tr>";
        
        
        $("#accounting_pasien_bayar_list").append(str);        
        $("#accounting_pasien_bayar_list tr:nth-last-child(1) td:nth-last-child(2) div").remove();
        
        $("#acc_account"+replacer_s).select2().on("change", function () {
            setTimeout(function() {
                $('.select2-container-active').removeClass('select2-container-active');
                $(':focus').blur();
                $("#acc_account"+replacer_s).select2('open');
                $("#acc_nilai"+replacer_s).data("akun",$("#acc_account"+replacer_s).val());
            }, 100);
        });
        
        $("#acc_nilai"+replacer_s).prop("disabled",false);
        
        
        $( "#acc_d"+replacer_s+", "+"#acc_k"+replacer_s ).on("change",function() {
            var idx = $(this).data("trig");
            var nilai=$(this).data("nilai");
            var summary=$(this).data("summary");
            if($(this).is(":checked")){
                $( "#"+idx ).prop( "checked", false);
                $("#"+nilai).data("dk",summary);
            }else{
                $( "#"+idx ).prop( "checked", true);
                summary=$("#"+idx ).data("summary");
                $("#"+nilai).data("dk",summary);
            }
            accounting_pasien_bayar.acc_counter();
        });
        accounting_pasien_bayar.recount(replacer_s);
        $("#acc_name"+replacer_s).val("");
    };
    
    

    $("#accounting_pasien_bayar_noreg_pasien").keypress(function(e) {
        if(e.which == 13) {
            accounting_pasien_bayar.loading();
        }
    });
});