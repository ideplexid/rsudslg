var ledger;
$(document).ready(function(){
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
	//$('.mydatetime').datetimepicker({minuteStep:1});
	$('.mydate').datepicker();
	var column  = new Array('account');
	ledger      = new ReportAction("ledger","accounting","ledger",column);
    
    var dari            = $("#ledger_dari").val();
    var sampai          = $("#ledger_sampai").val();
    var akun            = $("#ledger_akun").val();
    var grup            = $("#ledger_grup").val();
    var child           = $("#ledger_child").val();
    var form            = $("#ledger_form").val();
    
    if(dari != ""){
        $("#ledger_from_date").val(dari);
    }
    if(sampai != ""){
        $("#ledger_to_date").val(sampai);
    }
    if(akun != ""){
        $("#ledger_account").val(akun);
    }
    if(grup != ""){
        $("#ledger_filter_grup").val(grup);
    }
    if(child != ""){
        $("#ledger_all_child").val(child);
    }
    if(form != ""){
        $("#ledger_format").val(form);
    }
    
    ledger.addRegulerData=function(a){
        a['filter_grup'] = $("#"+this.prefix+"_filter_grup").val();
        a['all_child']   = $("#"+this.prefix+"_all_child").val();
        a['format']      = $("#"+this.prefix+"_format").val();
        a['filter_jenis']       = $("#"+this.prefix+"_filter_jenis").val();
        return a;
    };
    ledger.addViewData = function(a){
        a['from_date']  = a['from_date']+" 00:00:00";
        a['to_date']    = a['to_date']+" 23:59:59";
        return a;
    };

    $("#ledger_account").select2();
    
    ledger.print_preview=function(){
        var data        = this.getViewData();
        data['action']  = "worksheet_print_ledger";
        showLoading();
        $.post("",data,function(res){
            var json    = getContent(res);
            smis_print(json);
            dismissLoading();
        });
    };

    ledger.repost=function(id){
        var data         = this.getRegulerData();
        data['action']   = "post_accounting";
        data['id']       = $("#ledger_repost_"+id).data("notify-id");
        showLoading();
        $.post("",data,function(res){
            var json    = getContent(res);
            smis_alert("Pemberitahuan","Re Post Berhasil","alert-info")
            dismissLoading();
        });
    };
    
    ledger.preview=function(){
        var data                    = this.getViewData();
        data['page']                = "accounting";
        data['action']              = "preview_ledger";
        data['prototype_implement'] = "";
        data['prototype_slug']      = "";
        data['prototype_name']      = "";
        showLoading();
        $.post("",data,function(res){
            $("div#ledger").html(res);
            dismissLoading();
        })
    };
    
    ledger.view();
});