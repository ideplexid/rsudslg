var transaction_keluar;
$(document).ready(function(){
	var startDate = $("#transaction_keluar_start_date").val();
	if(startDate==""){
		startDate="1900-01-01";
	}
	$("#transaction_keluar_parent_tanggal").datetimepicker({ minuteStep: 1,startDate:startDate});
	$("#transaction_keluar_child_nomor_account").select2().on("select2-close", function () {
	    setTimeout(function() {
	        $('.select2-container-active').removeClass('select2-container-active');
	        $(':focus').blur();
	        $("#transaction_keluar_child_keterangan").focus();
	    }, 100);
	});

	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
	var child_column=new Array('id','id_transaksi','nomor_account',"keterangan","debet","kredit","klas","sub");
	var parent_column=new Array('id','tanggal','nomor',"keterangan","grup");
	
	transaction_keluar=new ParentChildAction("transaction_keluar","accounting","transaction_keluar",parent_column,child_column,'id_transaksi');
	transaction_keluar.parent.view();
	transaction_keluar.child.setEnableAutofocus(true);
	transaction_keluar.child.setMultipleInput(true);
	transaction_keluar.child.setNextEnter();
	$('#transaction_keluar_child_add_form').on('shown.bs.modal', function() {
		$('#transaction_keluar_child_nomor_account').select2('open');
	});
	
	transaction_keluar.parent.pindah=function(id){
		var data=this.getRegulerData();
		bootbox.confirm("Anda Yakin Memindah Transaksi ini ke Transaksi Masuk",function(result){
			if(result){
				data['command']="save";
				data['id']=id;
				data['pindah']="pindah";
				showLoading();
				$.post("",data,function(res){
					var json=getContent(res);
					transaction_keluar.parent.view();
					dismissLoading();
				});
			}
		});
	};
    
    transaction_keluar.parent.addViewData=function(add){
        add['filter_grup']=$("#transaction_keluar_filter_grup").val();
        return add;
    };

	transaction_keluar.child.save=function (){
		if(!this.cekSave()){
			return;
		}
		var self=this;
		var a=this.getSaveData();
		showLoading();
		$.ajax({url:"",data:a,type:'post',success:function(res){
			var json=getContent(res);
			if(json==null) {
				$("#"+self.prefix+"_add_form").smodal('hide');
				return;		
			}else if(json.success=="1"){
				$('#transaction_keluar_child_nomor_account').select2('open');
				self.view();
				self.clear();
			}
			dismissLoading();
			self.postAction(a['id']);
		}});
		this.aftersave();
	};
    
    transaction_keluar.parent.addCekSave=function(result){
        if($("#transaction_keluar_red_block").hasClass("red_text")){
            result.good=false;
            result.msg=result.msg+" </br> Transaksi Tidak Balance !!!";
        }
        return result;
    };
	
});