var error;
$(document).ready(function(){
	$(".mydatetime").datetimepicker({ minuteStep: 1});
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
	$("#error_child_nomor_account").select2().on("select2-close", function () {
	    setTimeout(function() {
	        $('.select2-container-active').removeClass('select2-container-active');
	        $(':focus').blur();
	        $("#error_child_keterangan").focus();
	    }, 100);
	});
	var child_column=new Array('id','id_transaksi','nomor_account',"keterangan","debet","kredit");
	var parent_column=new Array('id','tanggal','nomor',"keterangan");
	
	error=new ParentChildAction("error","accounting","error",parent_column,child_column,'id_transaksi');
	error.parent.view();
	error.child.setEnableAutofocus(true);
	error.child.setMultipleInput(true);
	error.child.setNextEnter();
	$('#error_child_add_form').on('shown.bs.modal', function() {
		$('#error_child_nomor_account').select2('open');
	});	
    
    error.parent.addViewData=function(add){
        add['filter_grup']=$("#error_filter_grup").val();
        return add;
    };

	error.child.save=function (){
		if(!this.cekSave()){
			return;
		}
		var self=this;
		var a=this.getSaveData();
		showLoading();
		$.ajax({url:"",data:a,type:'post',success:function(res){
			var json=getContent(res);
			if(json==null) {
				$("#"+self.prefix+"_add_form").smodal('hide');
				return;		
			}else if(json.success=="1"){
				$('#error_child_nomor_account').select2('open');
				self.view();
				self.clear();
			}
			dismissLoading();
			self.postAction(a['id']);
		}});
		this.aftersave();
	};
});