$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
var preview_ledger;
$(document).ready(function(){
    var column=new Array('');
    preview_ledger=new ReportAction("preview_ledger","accounting","preview_ledger",column);
    preview_ledger.back=function(){
        var data=this.getRegulerData();
        //data['page']= "accounting";
        data['action']      = "ledger";
        data['dari']        = $("#preview_ledger_dari").val();
        data['sampai']      = $("#preview_ledger_sampai").val();
        data['akun']        = $("#preview_ledger_akun").val();
        data['grup']        = $("#preview_ledger_grup").val();
        data['child']       = $("#preview_ledger_child").val();
        data['form']        = $("#preview_ledger_form").val();
        showLoading();
        //LoadSmisPage(data);
        $.post("",data,function(res){
        $("div#ledger").html(res);
            dismissLoading();
        })
    };
    
    preview_ledger.cetak=function(){
        var area = $('#print_table_preview_ledger').html();
        smis_print(area);
    };
});