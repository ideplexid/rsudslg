var worksheet_laba_rugi;
$(document).ready(function(){
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
	$('.mydate').datepicker();
	var column=new Array('');
	worksheet_laba_rugi=new ReportAction("worksheet_laba_rugi","accounting","worksheet_laba_rugi",column);
    
    var from_date   = $("#worksheet_laba_rugi_dari").val();
    var to_date     = $("#worksheet_laba_rugi_sampai").val();
    var grup        = $("#worksheet_laba_rugi_grup").val();
    var format      = $("#worksheet_laba_rugi_format").val();
    
    if(from_date != ""){
        $("#worksheet_laba_rugi_from_date").val(from_date);
    }
    if(to_date != ""){
        $("#worksheet_laba_rugi_to_date").val(to_date);
    }
    if(grup != ""){
        $("#worksheet_laba_rugi_filter_grup").val(grup);
    }
    if(format != ""){
        $("#worksheet_laba_rugi_filter_format").val(format);
    }
    
	worksheet_laba_rugi.expand=function(){
		if($("#print_table_worksheet_laba_rugi").hasClass("only_income_worksheet_laba_rugi")){
			$("#print_table_worksheet_laba_rugi").removeClass("only_income_worksheet_laba_rugi");
		}else{
			$("#print_table_worksheet_laba_rugi").addClass("only_income_worksheet_laba_rugi");
		}
	};
    worksheet_laba_rugi.addRegulerData=function(a){
        a['filter_grup']=$("#"+this.prefix+"_filter_grup").val();
        a['filter_format']=$("#"+this.prefix+"_filter_format").val();
        return a;
    };
    
    worksheet_laba_rugi.download_format_excel=function(){
        var data=this.getViewData();
        //data['action']="worksheet_format";
        data['action']="excel_laba_rugi";
        data['akun']="laba_rugi";
        download(data);
    };
    
    worksheet_laba_rugi.print_preview=function(){
        var data=this.getViewData();
        data['action']="worksheet_print";
        data['akun']="laba_rugi";
        showLoading();
        $.post("",data,function(res){
            var json=getContent(res);
            smis_print(json);
            dismissLoading();
        });
    };
    
    worksheet_laba_rugi.preview=function(){
        var data=this.getViewData();
        data['page']= "accounting";
        data['action']= "preview_laba_rugi";
        data['prototype_implement']= "";
        data['prototype_slug']= "";
        data['prototype_name']= "";
        showLoading();
        $.post("",data,function(res){
            $("div#worksheet_laba_rugi").html(res);
            dismissLoading();
        })
    };

    worksheet_laba_rugi.addViewData = function(a){
        a['from_date']   = a['from_date']+" 00:00:00";
        a['to_date']     = a['to_date']+" 23:59:59";
        return a;
    };
    
    worksheet_laba_rugi.view();
});