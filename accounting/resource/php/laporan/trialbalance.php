<?php
global $db;
$uitable = new Report(array("Nomor","Nama","SA_Debet","SA_Kredit","TA_Debet","TA_Kredit","Debet","Kredit"), "&nbsp;", NULL);
$uitable ->setName("trialbalance");
$uitable ->setDiagram(false);
$header0 = "<tr> <th rowspan='2'>Nomor</th> <th rowspan='2'>Nama</th> <th colspan='2'>Saldo Awal</th> <th colspan='2'>Transaksi</th> <th colspan='2'>Saldo Akhir</th>  </tr>";
$header1 = "<tr> <th>Debet</th><th>Kredit</th> <th>Debet</th><th>Kredit</th> <th>Debet</th><th>Kredit</th>  </tr>";
$uitable ->addHeader("before",$header0)
		 ->addHeader("before",$header1)
		 ->setHeaderVisible(false)
		 ->setMode(Report::$DATE);
if(isset($_POST['command'])) {
	require_once "accounting/class/adapter/TrialBalanceAdapter.php";	
	$qv = "	SELECT smis_ac_transaksi.id AS id, 
			smis_ac_transaksi.tanggal AS tanggal, 
			smis_ac_transaksi.nomor AS nomor, 
			smis_ac_transaksi.keterangan AS keterangan, 
			smis_ac_transaksi_detail.id AS id_detail, 
			smis_ac_transaksi_detail.nomor_account AS nomor_account, 
			smis_ac_transaksi_detail.nama_account AS nama_account, 
			smis_ac_transaksi_detail.keterangan AS uraian,
        	sum(smis_ac_transaksi_detail.debet-smis_ac_transaksi_detail.kredit) as grand,            
            sum(if(jenis='saldo_awal' OR tanggal < '".$_POST['from_date']."',smis_ac_transaksi_detail.debet,0))  AS debet_saldo_awal, 
			sum(if(jenis='saldo_awal' OR tanggal < '".$_POST['from_date']."',smis_ac_transaksi_detail.kredit ,0)) AS kredit_saldo_awal,
			sum(if(jenis!='saldo_awal' AND tanggal >= '".$_POST['from_date']."',smis_ac_transaksi_detail.debet,0))  AS debet_transaksi, 
			sum(if(jenis!='saldo_awal' AND tanggal >= '".$_POST['from_date']."',smis_ac_transaksi_detail.kredit ,0)) AS kredit_transaksi,
			smis_ac_transaksi.prop AS prop
			FROM smis_ac_transaksi
			LEFT JOIN smis_ac_transaksi_detail 
			ON smis_ac_transaksi.id = smis_ac_transaksi_detail.id_transaksi 
		  ";	
	$qc = "	SELECT count(*) as total
			FROM smis_ac_transaksi
			LEFT JOIN smis_ac_transaksi_detail 
			ON smis_ac_transaksi.id = smis_ac_transaksi_detail.id_transaksi ";
	
	$column  = array();
	$dbtable = new DBTable($db, "smis_ac_transaksi", $column);
	$dbtable ->setPreferredQuery(true, $qv, $qc)
			 ->setUseWhereforView(true)
			 ->addCustomKriteria("smis_ac_transaksi.fix ", " ='0' ")
			 ->addCustomKriteria("smis_ac_transaksi_detail.prop ", " NOT LIKE '%del%'");
	if(isset($_POST['filter_grup']) && $_POST['filter_grup']!="" && $_POST['filter_grup']!="null"){
        		$dbtable->addCustomKriteria(" smis_ac_transaksi_detail.grup ", "='".$_POST['filter_grup']."'");
    	}
	$dbtable ->setGroupBy(true, " smis_ac_transaksi_detail.nomor_account ")
			 ->setForceOrderForQuery(true, " smis_ac_transaksi_detail.nomor_account ASC ")
			 ->setShowAll(true);
	
    require_once "accounting/class/report/TrialBalanceDBReport.php";
	$dbres = new TrialBalanceDBReport($dbtable, $uitable, new TrialBalanceAdapter(), 'tanggal', DBReport::$TIME);
	$dbres ->setUseFromControl(false)
		   ->setFileName("Neraca Saldo");
    $data  = $dbres->command($_POST ['command']);
	echo json_encode($data, JSON_NUMERIC_CHECK);
	return;
}

$dbtable = new DBTable($db,"smis_ac_grup");
$dbtable ->setShowAll(true);
$list	 = $dbtable->view("",0);
$adapter = new SelectAdapter("nama","slug");
$grup	 = $adapter->getContent($list['data']);
$grup[]  = array("name"=>"","value"=>"","default"=>1);

/* This is Modal Form and used for add and edit the table */
$uitable ->addModal("filter_grup","select","Grup",$grup);
$modal   = $uitable->getAdvanceModal();
$modal	 ->setTitle("Neraca Saldo");

$excel   = new Button("","","");
$excel   ->setAction("trialbalance.excel()")
		 ->setIcon("fa fa-download")
		 ->setClass("btn-primary")
		 ->setIsButton(Button::$ICONIC);
$modal   ->getForm()
		 ->addElement("",$excel);

echo addJS  ("framework/smis/js/table_action.js");
echo addJS  ("framework/smis/js/report_action.js");
echo addJS  ("framework/bootstrap/js/bootstrap-datepicker.js");
echo addJS  ("accounting/resource/js/trialbalance.js",false);
echo addCSS ("framework/bootstrap/css/datepicker.css");
echo addCSS ("accounting/resource/css/trialbalance.css",false);

echo "<h1>Neraca Saldo</h1>";
echo $modal		->getModalSkeleton();
echo $uitable	->getHtml();
?>