<?php

/**
 * digunakan khusus cash flow untuk
 * pembuatan laporan aliran cash
 * 
 * @author 		: Nurul Huda
 * @copyright 	: goblooge@gmail.com
 * @license 	: LGPLv2
 * @database 	: - smis_ac_account
 *                - smis_ac_transaksi
 *                - smis_ac_transaksi_detail
 * @since		: 5 Sep 2017
 * @version		: 1.1.0
 * 
 * */


global $db;
if (isset ( $_POST ['command'] )) {
    $setup=array();
    $setup['Piutang Dagang']=array();
    $setup['Hutang Lancar']=array();
    $setup['Pendapatan Usaha']=array();
    $setup['Pendapatan Lain']=array();
    $setup['Harga Pokok Penjualan']=array();
    $setup['Piutang Dagang']=array();
    $setup['Biaya Operasional']=array();
    $setup['Biaya Dibayar Dimuka']=array();
    $setup['Harta Tetap Berwujud']=array();
    $setup['Hutang Jangka panjang']=array();
    $setup['Hutang Lain']=array();
    
    $dbtable=new DBTable($db,"smis_ac_account");
    foreach($setup as $key=>$value){
        $dbtable->addCustomKriteria("sub","='".$key."'");
        $dbtable->setShowAll(true);
        $dbtable->setOrder(" nomor ASC ");
        $list=$dbtable->view("",0);
        $datalist=$list['data'];
        
        foreach($datalist as $x){
            $query="SELECT smis_ac_transaksi_detail.nama_account, 
                    smis_ac_transaksi_detail.nomor_account, 
                    SUM(smis_ac_transaksi_detail.debet) as debet, 
                    SUM(smis_ac_transaksi_detail.kredit) as kredit 
                    FROM smis_ac_transaksi LEFT JOIN smis_ac_transaksi_detail ON smis_ac_transaksi.id=smis_ac_transaksi_detail.id_transaksi
                    WHERE smis_ac_transaksi.tanggal>='".$_POST['dari']."' AND smis_ac_transaksi.tanggal<'".$_POST['sampai']."'
                    AND smis_ac_transaksi_detail.prop!='del' AND smis_ac_transaksi.prop!='del'
                    AND smis_ac_transaksi_detail.nomor_account='".$x->nomor."'";
            $baris=$db->get_row($query);
            if($baris!=null && ( $baris->kredit>0 || $baris->debet>0)  ){
                $setup[$key][]=array("nama_account"=>$baris->nama_account,"nomor_account"=>$baris->nomor_account,"debet"=>$baris->debet,"kredit"=>$baris->kredit);    
            }
        }
    }
    
    
    /*start excel*/
    require_once ("smis-libs-out/pclzip.lib.php");
    require_once("smis-libs-out/php-excel/PHPExcel.php");
    loadLibrary("smis-libs-function-time");
    
    $file = new PHPExcel ();
    $sheet = $file->getSheet(0);    
    $sheet->mergeCells ( 'A1:E1' )->setCellValue ( 'A1',"LAPORAN ARUS KAS");
		
    
    /*umum*/
    $sheet->setCellValue("A2","Dari");
    $sheet->setCellValue("B2",ArrayAdapter::format("date d M Y",$_POST['dari']));
    $sheet->setCellValue("C2","Sampai");
    $sheet->setCellValue("D2",ArrayAdapter::format("date d M Y",$_POST['sampai']));
    
    $sheet->setCellValue("A3","Sub");
    $sheet->setCellValue("B3","Nomor");
    $sheet->setCellValue("C3","Akun");
    $sheet->setCellValue("D3","Debit");
    $sheet->setCellValue("E3","Kredit");
    
    $sheet->getColumnDimension ( "A" )->setAutoSize ( true );
    $sheet->getColumnDimension ( "B" )->setAutoSize ( true );
    $sheet->getColumnDimension ( "C" )->setAutoSize ( true );
    $sheet->getColumnDimension ( "D" )->setAutoSize ( true );
    $sheet->getColumnDimension ( "E" )->setAutoSize ( true );
    
    
    /*start put the data*/
    $start=4;
    foreach($setup as $sub=>$content){
        $first=true;
        foreach($content as $c){
            if($first){
                $sheet->setCellValue("A".$start,$sub);
                $first=false;
            }
            $sheet->setCellValue("B".$start,"'".$c['nomor_account']);
            $sheet->setCellValue("C".$start,$c['nama_account']);
            $sheet->setCellValue("D".$start,$c['debit']);
            $sheet->setCellValue("E".$start,$c['kredit']);
            $start++;
        }
    }
    
    $sheet->setCellValue("A".$start,"TOTAL");
    $sheet->setCellValue("D".$start,"=SUM(D3:D".($start-1).")");
    $sheet->setCellValue("E".$start,"=SUM(E3:E".($start-1).")");
    $sheet  ->getStyle('D3:E'.$start)
            ->getNumberFormat()
            ->setFormatCode("#,##0");
    
    $sheet->getStyle ( "A1" . ":E3" )->getFont ()->setBold ( true );
    
    $fillcabang['borders']=array();
    $fillcabang['borders']['allborders']=array();
    $fillcabang['borders']['allborders']['style']=PHPExcel_Style_Border::BORDER_THIN ;
    $sheet->getStyle ( 'A1:E' . $start )->applyFromArray ( $fillcabang );
    $sheet->getStyle ( "A".$start . ":E".$start )->getFont ()->setBold ( true );
    
    header('Content-type: application/vnd.ms-excel');	
	header("Content-Disposition: attachment; filename='Laporan Cash Flow.xls'");
	$writer = PHPExcel_IOFactory::createWriter($file, 'Excel5');
	$writer->save('php://output');
    return;
    
}

/* This is Modal Form and used for add and edit the current */


$uitable=new Table(array());
$uitable->setName("cash_flow");
$uitable->addModal("dari","datetime","Dari","");
$uitable->addModal("sampai","datetime","Sampai","");
$form=$uitable->getModal()->getForm();

$proses=new Button("","","Proses");
$proses->setClass(" btn-primary");
$proses->setIcon("fa fa-refresh");
$proses->setIsButton(Button::$ICONIC_TEXT);
$proses->setAction ( "cash_flow.excel()" );
$form->addElement ( "", $proses );
echo $form->getHtml ();
echo addJS ( "framework/bootstrap/js/bootstrap-datepicker.js" );
echo addCSS ( "framework/bootstrap/css/datepicker.css" );
echo addJS ( "framework/smis/js/table_action.js" );
echo addJS ( "accounting/resource/js/cash_flow.js",false );

?>