<?php
global $db;
$uitable = new Report ( array (
		"Account",
		"Debet Saldo Awal",
		"Kredit Saldo Awal",
		"Debet JP",
		"Kredit JP",
		"Debet Jurnal",
		"Kredit Jurnal",
        "Debet Saldo",
		"Kredit Saldo",
		"Debet Penyesuaian",
		"Kredit Penyesuaian",
		"Debet Setelah Penyesuaian",
		"Kredit Setelah Penyesuaian",
		"Debet RugiLaba",
		"Kredit RugiLaba",
		"Debet Neraca",
		"Kredit Neraca",
        "FDebet RugiLaba",
		"FKredit RugiLaba",
		"FDebet Neraca",
		"FKredit Neraca" 
), "", NULL );
$uitable ->setClass("only_income_worksheet worksheet_table");
$uitable ->setName ( "worksheet" )
		 ->setDiagram ( false )
		 ->setHeaderVisible(true)
		 ->addHeader ( "before", "
		 <tr>
          <td rowspan='2'>Account</td>
          <td colspan='2'>Saldo Awal</td>
          <td colspan='2'>Transaksi</td>
          <td colspan='2'>Transaksi</td>
          <td colspan='2'>Neraca Saldo</td> 
          <td colspan='2'>Jurnal Umum</td>
          <td colspan='2'>Neraca Saldo</td>
          <td colspan='2'>Laba Rugi</td>
          <td colspan='2'>Neraca</td>
          <td colspan='2'>Akumulasi Laba Rugi</td>
          <td colspan='2'>Akumulasi Neraca</td>
		</tr>")
		->addHeader ( "before", "
		<tr>	
         <td>Debit</td> <td>Kredit</td>
         <td>Debit</td> <td>Kredit</td>
         <td>Debit</td> <td>Kredit</td>
         <td>Debit</td> <td>Kredit</td>
         <td>Debit</td> <td>Kredit</td>
         <td>Debit</td> <td>Kredit</td>
         <td>Debit</td> <td>Kredit</td>
         <td>Debit</td> <td>Kredit</td>
         <td>Debit</td> <td>Kredit</td>
         <td>Debit</td> <td>Kredit</td>
        </tr>" );

$uitable->setMode(Report::$DATE);
if (isset ( $_POST ['command'] )) {
	$akun_laba_tahun_berjalan=getSettings($db,"accounting-akun-khusus-laba-berjalan","NOT SET");
	require_once 'accounting/class/adapter/WorksheetAdapter.php';
	$qv = "SELECT smis_ac_transaksi.id AS id, 
			smis_ac_transaksi.tanggal AS tanggal, 
			smis_ac_transaksi.jenis AS jenis, 
			smis_ac_transaksi.nomor AS nomor, 
			smis_ac_transaksi.keterangan AS keterangan, 
			smis_ac_transaksi_detail.id AS id_detail,
            if(STRCMP(smis_ac_transaksi_detail.nomor_account,'4')>0 AND tanggal < '".$_POST['from_date']."','".$akun_laba_tahun_berjalan."',smis_ac_transaksi_detail.nomor_account) AS nomor_account,
            
            smis_ac_transaksi_detail.nama_account AS nama_account, 
			smis_ac_transaksi_detail.keterangan AS uraian,
					
			sum(if ( jenis='saldo_awal' OR tanggal < '".$_POST['from_date']." ',smis_ac_transaksi_detail.debet,0))  AS debet_saldo_awal, 
			sum(if ( jenis='saldo_awal' OR tanggal < '".$_POST['from_date']." ',smis_ac_transaksi_detail.kredit ,0)) AS kredit_saldo_awal,
			
            sum(if( (jenis='memorial' OR jenis='') AND tanggal >= '".$_POST['from_date']."',smis_ac_transaksi_detail.debet,0))  AS debet_memorial, 
			sum(if( (jenis='memorial' OR jenis='') AND tanggal >= '".$_POST['from_date']."',smis_ac_transaksi_detail.kredit ,0)) AS kredit_memorial,
			
            sum(if(jenis='transaction' AND tanggal >= '".$_POST['from_date']."',smis_ac_transaksi_detail.debet,0))  AS debet_transaksi, 
			sum(if(jenis='transaction' AND tanggal >= '".$_POST['from_date']."',smis_ac_transaksi_detail.kredit ,0)) AS kredit_transaksi,
			
			smis_ac_account.sub as sub_name,
            smis_ac_account.klas as class_name,
            smis_ac_transaksi.prop AS prop
            FROM smis_ac_transaksi
            LEFT JOIN smis_ac_transaksi_detail 
            ON smis_ac_transaksi.id = smis_ac_transaksi_detail.id_transaksi 
            LEFT JOIN smis_ac_account ON smis_ac_transaksi_detail.nomor_account=smis_ac_account.nomor
			";
	require_once 'accounting/function/get_data_neraca_lajur.php';
    $laba_berjalan		= get_laba_tahun_berjalan($db,$_POST['from_date'],$_POST['to_date']);    
	$worksheet_adapter 	= new WorksheetAdapter();
    $worksheet_adapter 	->setTotalLabaBerjalan($akun_laba_tahun_berjalan,$laba_berjalan->debit_transaksi,$laba_berjalan->kredit_transaksi,$laba_berjalan->debit_memorial,$laba_berjalan->kredit_memorial);
	$dbtable = new DBTable($db,"smis_ac_transaksi" );
	$dbtable ->setPreferredQuery(true,$qv,$qc)
			 ->setUseWhereforView(true)
			 ->setShowAll(true)
			 ->setForceOrderForQuery(true," smis_ac_transaksi_detail.nomor_account ASC " )
			 ->setGroupBy(true," smis_ac_transaksi_detail.nomor_account" )
			 ->addCustomKriteria("smis_ac_transaksi.fix ", " ='0' " )
			 ->addCustomKriteria("smis_ac_transaksi_detail.prop ", " NOT LIKE '%del%'" )
			 ->addCustomKriteria("smis_ac_transaksi.tanggal ", " < '".$_POST['to_date']."' " );
	if(isset($_POST['filter_grup']) && $_POST['filter_grup']!="" && $_POST['filter_grup']!="null"){
        $dbtable->addCustomKriteria ( " smis_ac_transaksi_detail.grup ", "='".$_POST['filter_grup']."'" );
    }
    
	$dbres 	= new DBReport ( $dbtable, $uitable, $worksheet_adapter , 'tanggal', DBReport::$TIME );
    $data 	= $dbres->command ( $_POST ['command'] );
	echo json_encode ( $data, JSON_NUMERIC_CHECK );
	return;
}

if(isset($_POST['dari'])){
    $dari 	= substr($_POST['dari'],0,10);
}
if(isset($_POST['sampai'])){
    $sampai = substr($_POST['sampai'],0,10);
}
if(isset($_POST['grup'])){
    $grup 	= $_POST['grup'];
}

$uitable ->addModal("dari", "hidden", "",$dari)
		 ->addModal("sampai", "hidden", "",$sampai)
		 ->addModal("grup", "hidden", "",$grup);

$dbtable	= new DBTable($db,"smis_ac_grup");
$dbtable	->setShowAll(true);
$list		= $dbtable->view("",0);
$adapter	= new SelectAdapter("nama","slug");
$grup		= $adapter->getContent($list['data']);
$grup[]		= array("name"=>"","value"=>"","default"=>1);

/* This is Modal Form and used for add and edit the table */
$uitable ->addModal("filter_grup","select","Grup",$grup);
$modal   = $uitable->getAdvanceModal ();
$modal   ->setTitle ( "Neraca Lajur" );

$print	= new Button("", "", "Print");
$print	->setIsButton(Button::$ICONIC_TEXT)
		->setAction("worksheet.print()")
		->setClass("btn-primary")
		->setIcon("fa fa-print");

$hide   = new Button("", "", "Expand");
$hide   ->setIsButton(Button::$ICONIC_TEXT)
	    ->setAction("worksheet.expand()")
	    ->setClass("btn-primary")
	    ->setIcon("fa fa-expand");

$excel  = new Button("", "", "Download");
$excel  ->setIsButton(Button::$ICONIC_TEXT)
		->setAction("worksheet.download_format_excel()")
		->setClass("btn-primary")
		->setIcon("fa fa-file-excel-o");

$preview = new Button("", "", "Print Preview");
$preview ->setIsButton(Button::$ICONIC_TEXT)
		 ->setAction("worksheet.print_preview()")
		 ->setClass("btn-primary")
		 ->setIcon("fa fa-file-excel-o");

$preview = new Button("", "", "Preview");
$preview ->setIsButton(Button::$ICONIC_TEXT)
		 ->setAction("worksheet.preview()")
		 ->setClass("btn-primary")
		 ->setIcon("fa fa-file-o");

$buttongrup = new ButtonGroup("","","");
$buttongrup ->setMax(3,"Action")
			->addButton($print)
			->addButton($hide)
			->addButton($excel)
			->addButton($preview);
$modal		->addFooter($buttongrup);

echo addJS  ("framework/bootstrap/js/bootstrap-datepicker.js");
echo addCSS ("framework/bootstrap/css/datepicker.css");
echo addJS  ("framework/smis/js/table_action.js");
echo addJS  ("framework/smis/js/report_action.js");
echo addCSS ("accounting/resource/css/worksheet.css",false);
echo addJS  ("accounting/resource/js/worksheet.js",false);

echo "<h1>Neraca Lajur</h1>";
echo $modal->getModalSkeleton();
echo $uitable->getHtml ();
?>
