<?php
global $db;
$uitable = new Report ( array (
		"Account",
		"Debet Saldo Awal",
		"Kredit Saldo Awal",
		"Debet Jurnal",
		"Kredit Jurnal",
        "Debet Saldo",
		"Kredit Saldo",
		"Debet Penyesuaian",
		"Kredit Penyesuaian",
		"Debet Setelah Penyesuaian",
		"Kredit Setelah Penyesuaian",
		"Debet RugiLaba",
		"Kredit RugiLaba",
        "FDebet RugiLaba",
		"FKredit RugiLaba",
        "SRugiLaba"
), "", NULL );
$uitable->setClass("only_income_worksheet_laba_rugi");
$uitable->setName ( "worksheet_laba_rugi" );
$uitable->setDiagram ( false );
$uitable->addHeader ( "before", "<tr><td rowspan='2'>Account</td> <td colspan='2'>Saldo Awal</td> <td colspan='2'>Transaksi</td> <td colspan='2'>Neraca Saldo</td>  <td colspan='2'>Jurnal Umum</td> <td colspan='2'>Neraca Saldo Di Sesuaikan</td> <td colspan='2'>Laba Rugi</td>	 <td colspan='2'>Akumulasi Laba Rugi</td> <td rowspan='2'>Laba Rugi</td> 	</tr>" );
$uitable->addHeader ( "before", "<tr>	<td>Debit</td>	<td>Kredit</td> <td>Debit</td>	<td>Kredit</td>	<td>Debit</td>	<td>Kredit</td>	<td>Debit</td> <td>Kredit</td>	<td>Debit</td>	<td>Kredit</td> <td>Debit</td>	<td>Kredit</td> <td>Debit</td>	<td>Kredit</td>	</tr>" );
$uitable->setMode(Report::$DATE);
/* this is respond when system have to response */
if (isset ( $_POST ['command'] )) {
	
	require_once 'accounting/class/adapter/WorksheetAdapter.php';
	$qv = "SELECT smis_ac_transaksi.id AS id, 
			smis_ac_transaksi.tanggal AS tanggal, 
			smis_ac_transaksi.jenis AS jenis, 
			smis_ac_transaksi.nomor AS nomor, 
			smis_ac_transaksi.keterangan AS keterangan, 
			smis_ac_transaksi_detail.id AS id_detail, 
			smis_ac_transaksi_detail.nomor_account AS nomor_account, 
			smis_ac_transaksi_detail.nama_account AS nama_account, 
			smis_ac_transaksi_detail.keterangan AS uraian, 	
					
			sum(if(jenis='saldo_awal' OR tanggal < '".$_POST['from_date']."',smis_ac_transaksi_detail.debet,0))  AS debet_saldo_awal, 
			sum(if(jenis='saldo_awal' OR tanggal < '".$_POST['from_date']."',smis_ac_transaksi_detail.kredit ,0)) AS kredit_saldo_awal,
			sum(if(jenis='transaction' AND tanggal >= '".$_POST['from_date']."',smis_ac_transaksi_detail.debet,0))  AS debet_transaksi, 
			sum(if(jenis='transaction' AND tanggal >= '".$_POST['from_date']."',smis_ac_transaksi_detail.kredit ,0)) AS kredit_transaksi,
			sum(if( (jenis='memorial' OR jenis='') AND tanggal >= '".$_POST['from_date']."',smis_ac_transaksi_detail.debet,0))  AS debet_memorial, 
			sum(if( (jenis='memorial' OR jenis='') AND tanggal >= '".$_POST['from_date']."',smis_ac_transaksi_detail.kredit ,0)) AS kredit_memorial,
            
			smis_ac_account.sub as sub_name,
            smis_ac_account.klas as class_name,
			smis_ac_transaksi.prop AS prop
			FROM smis_ac_transaksi
			LEFT JOIN smis_ac_transaksi_detail 
			ON smis_ac_transaksi.id = smis_ac_transaksi_detail.id_transaksi 
            LEFT JOIN smis_ac_account ON smis_ac_transaksi_detail.nomor_account=smis_ac_account.nomor 
			";
	
	$qc = "SELECT count(*) as total
			FROM smis_ac_transaksi
			LEFT JOIN smis_ac_transaksi_detail 
			ON smis_ac_transaksi.id = smis_ac_transaksi_detail.id_transaksi 
            LEFT JOIN smis_ac_account ON smis_ac_transaksi_detail.nomor_account=smis_ac_account.nomor
            ";
	
	$dbtable = new DBTable ( $db, "smis_ac_transaksi" );
	$dbtable->setPreferredQuery ( true, $qv, $qc );
	$dbtable->setUseWhereforView ( true );
	$dbtable->setShowAll ( true );
	$dbtable->setForceOrderForQuery ( true, " smis_ac_transaksi_detail.nomor_account ASC " ); // not work, in dbtable
	$dbtable->setGroupBy ( true, " smis_ac_transaksi_detail.nomor_account" );
	$dbtable->addCustomKriteria ( NULL, " smis_ac_transaksi_detail.nomor_account NOT LIKE '1%' " );
	$dbtable->addCustomKriteria ( NULL, " smis_ac_transaksi_detail.nomor_account NOT LIKE '2%' " );
	$dbtable->addCustomKriteria ( NULL, " smis_ac_transaksi_detail.nomor_account NOT LIKE '3%' " );
	$dbtable->addCustomKriteria ( "smis_ac_transaksi.fix ", " ='0' " );
    $dbtable->addCustomKriteria ( "smis_ac_transaksi.tanggal ", " >= '".$_POST['from_date']."' " );
	$dbtable->addCustomKriteria ( "smis_ac_transaksi_detail.prop ", " NOT LIKE '%del%'" );
	$dbtable->addCustomKriteria ( "smis_ac_transaksi.tanggal  ", " < '".$_POST['to_date']."' " );
	
    if(isset($_POST['filter_grup']) && $_POST['filter_grup']!="" && $_POST['filter_grup']!="null"){
        $dbtable->addCustomKriteria ( " smis_ac_transaksi_detail.grup ", "='".$_POST['filter_grup']."'" );
    }
    $adapter =  new WorksheetAdapter();
    $adapter ->setMoneyEnable(true);
    $adapter ->setFooterEnabled(true);
    require_once "accounting/class/report/TrialBalanceDBReport.php";
	$dbres = new TrialBalanceDBReport ( $dbtable, $uitable,$adapter, 'tanggal', DBReport::$TIME );
	$dbres->setUseFromControl(false);
    $dbres->setMode(DBReport::$TIME);
    $dbres->setFileName("Laba Rugi");
    $data = $dbres->command ( $_POST ['command'] );
	echo json_encode ( $data, JSON_NUMERIC_CHECK );
	return;
}

if(isset($_POST['dari'])){
    $dari = substr($_POST['dari'],0,10);
}
if(isset($_POST['sampai'])){
    $sampai = substr($_POST['sampai'],0,10);
}
if(isset($_POST['grup'])){
    $grup = $_POST['grup'];
}
if(isset($_POST['format'])){
    $format = $_POST['format'];
}
$uitable->addModal("dari", "hidden", "",$dari);
$uitable->addModal("sampai", "hidden", "",$sampai);
$uitable->addModal("grup", "hidden", "",$grup);
$uitable->addModal("format", "hidden", "",$format);

$dbtable=new DBTable($db,"smis_ac_grup");
$dbtable->setShowAll(true);
$list=$dbtable->view("",0);
$selectadapter=new SelectAdapter("nama","slug");
$grup=$selectadapter->getContent($list['data']);
$grup[]=array("name"=>"","value"=>"","default"=>1);

$format=new OptionBuilder();
$format->add("1 Kolom","lr_1kolom","1");

/* This is Modal Form and used for add and edit the table */
$uitable->addModal("filter_grup","select","Grup",$grup);
$uitable->addModal("filter_format","select","Format",$format->getContent());
$modal = $uitable->getAdvanceModal ();
$modal->setTitle ( "Neraca Lajur" );

$print=new Button("", "", "Print");
$print->setIsButton(Button::$ICONIC_TEXT);
$print->setAction("worksheet_laba_rugi.print()");
$print->setClass("btn-primary");
$print->setIcon("fa fa-print");

$hide=new Button("", "", "Expand");
$hide->setIsButton(Button::$ICONIC_TEXT);
$hide->setAction("worksheet_laba_rugi.expand()");
$hide->setClass("btn-primary");
$hide->setIcon("fa fa-expand");

$excel=new Button("", "", "Download");
$excel->setIsButton(Button::$ICONIC_TEXT);
$excel->setAction("worksheet_laba_rugi.download_format_excel()");
$excel->setClass("btn-primary");
$excel->setIcon("fa fa-file-excel-o");

$print_preview=new Button("", "", "Print Preview");
$print_preview->setIsButton(Button::$ICONIC_TEXT);
$print_preview->setAction("worksheet_laba_rugi.print_preview()");
$print_preview->setClass("btn-primary");
$print_preview->setIcon("fa fa-file-excel-o");

$preview = new Button("", "", "Preview");
$preview->setIsButton(Button::$ICONIC_TEXT);
$preview->setAction("worksheet_laba_rugi.preview()");
$preview->setClass("btn-primary");
$preview->setIcon("fa fa-file-o");

$buttongrup=new ButtonGroup("","","");
$buttongrup->setMax(3,"Action");
$buttongrup->addButton($print);
$buttongrup->addButton($hide);
$buttongrup->addButton($excel);
//$buttongrup->addButton($print_preview);
$buttongrup->addButton($preview);
$modal->addFooter($buttongrup);

echo addJS ( "framework/bootstrap/js/bootstrap-datepicker.js" );
echo addCSS ( "framework/bootstrap/css/datepicker.css" );
echo addJS ( "framework/smis/js/table_action.js" );
echo addJS ( "framework/smis/js/report_action.js" );
echo addCSS ( "accounting/resource/css/worksheet_laba_rugi.css",false );
echo addJS ( "accounting/resource/js/worksheet_laba_rugi.js",false );

echo "<h1>Laporan Laba Rugi</h1>";
echo $modal->getModalSkeleton();
echo $uitable->getHtml ();
?>
