<?php
global $db;
$header	 = array ("No.","Jenis","Grup","No. Ref",'Tanggal','Keterangan',"Account","Uraian","Debet","Kredit","Saldo Debet","Saldo Kredit" );
$fast = new Button("","","Re Post");
$fast ->setClass("btn btn-warning")
	  ->setIcon("fa fa-refresh")
	  ->setIsButton(Button::$ICONIC_TEXT);
require_once "accounting/class/table/LedgerReport.php";
$uitable = new LedgerReport ($header,"",null);
$uitable ->setAction(true)
		 ->setAddButtonEnable(false)
		 ->setEditButtonEnable(false)
		 ->setDelButtonEnable(false)
		 ->setReloadButtonEnable(false)
		 ->setPrintButtonEnable(false)
		 ->setDetailButtonEnable(true)
		 ->setName("ledger")
		 ->setDiagram(false)
		 ->setMode(Report::$DATE)
		 ->addContentButton("repost",$fast);
/* this is respond when system have to response */
if (isset ( $_POST ['command'] )) {
	require_once "accounting/class/adapter/JournalAdapter.php";
	$qv = "SELECT smis_ac_transaksi.id AS id, 
			smis_ac_transaksi.tanggal AS tanggal, 
            smis_ac_transaksi.jenis AS jenis, 
			if(smis_ac_transaksi.jenis='saldo_awal',0,1) as urutan,
			smis_ac_transaksi.nomor AS nomor, 
            smis_ac_transaksi.code AS code, 
			smis_ac_transaksi.id_notify AS id_notify, 
			smis_ac_transaksi.keterangan AS keterangan, 
			smis_ac_transaksi_detail.id AS id_detail, 
			smis_ac_transaksi_detail.nomor_account AS nomor_account, 
			smis_ac_transaksi_detail.nama_account AS nama_account, 
			smis_ac_transaksi_detail.keterangan AS uraian, 
			smis_ac_transaksi_detail.debet AS debet, 
			smis_ac_transaksi_detail.kredit AS kredit,
            smis_ac_transaksi_detail.grup AS grup,
			smis_ac_transaksi.prop AS prop
			FROM smis_ac_transaksi
			LEFT JOIN smis_ac_transaksi_detail 
			ON smis_ac_transaksi.id = smis_ac_transaksi_detail.id_transaksi 
			";
	
	$qc = "SELECT count(*) as total
			FROM smis_ac_transaksi
			LEFT JOIN smis_ac_transaksi_detail 
			ON smis_ac_transaksi.id = smis_ac_transaksi_detail.id_transaksi ";
	
	$column  = array();
	$dbtable = new DBTable($db,"smis_ac_transaksi",$column);
	$dbtable ->setPreferredQuery(true, $qv, $qc)
			 ->setUseWhereforView(true)
			 ->setOrder(" urutan ASC, tanggal ASC ",true)
			 ->addCustomKriteria( "smis_ac_transaksi.fix ", " ='0' ")
			 ->addCustomKriteria( "smis_ac_transaksi_detail.prop ", " NOT LIKE '%del%'");
	
	if (isset ( $_POST ['account'] )) {
        if(isset($_POST['all_child']) && $_POST['all_child'] =="0"){
            $dbtable->addCustomKriteria ( "smis_ac_transaksi_detail.nomor_account ", " ='" . $_POST ['account'] . "'" );
        }else{
            $dbtable->addCustomKriteria ( "smis_ac_transaksi_detail.nomor_account ", " LIKE '" . $_POST ['account'] . "%'" );
        }
	}
    
    if(isset($_POST['filter_grup']) && $_POST['filter_grup']!="" && $_POST['filter_grup']!="null"){
        $dbtable->addCustomKriteria ( " smis_ac_transaksi_detail.grup ", "='".$_POST['filter_grup']."'" );
	}
	if(isset($_POST['filter_jenis']) && $_POST['filter_jenis']!="%"){
        $dbtable->addCustomKriteria ( " smis_ac_transaksi.jenis ", "='".$_POST['filter_jenis']."'" );
    }

    require_once "accounting/class/report/LedgerDBReport.php";
    $adapter = new JournalAdapter();
    $adapter ->setDB($db)
             ->setDari($_POST['from_date'])
             ->setAccount($_POST ['account'])
			 ->setIncluded($_POST['all_child'] =="1");
	$dbres   = new LedgerDBReport( $dbtable, $uitable, $adapter, 'tanggal', DBReport::$TIME );
	$dbres   ->setMode(DBReport::$TIME);
    $data    = $dbres->command ( $_POST ['command'] );
    if($data!=null){
        echo json_encode ( $data, JSON_NUMERIC_CHECK );
	}
	return;
}

require_once "accounting/class/adapter/AccountAdapter.php";
$accountdb  = new DBTable ( $db, "smis_ac_account" );
$accountdb  ->setOrder( "nomor ASC;" );

$accountdb  ->setShowAll( true );
$data       = $accountdb->view ( "", "0" );
$adapter    = new AccountAdapter ();
$option     = $adapter->getContent ( $data ['data'] );

$all_child	= new OptionBuilder();
$all_child  ->add("Include Child","1",'1')
			->add("Exclude Child","0",'0');

$format		= new OptionBuilder();
$format		->add("Standard","standard",'1')
			->add("Flowing","flowing",'0')
			->add("Daily","daily",'0');

$jenis		= new OptionBuilder();
$jenis		->add(" - - Semua - - ","%","1")
			->add(" - - Kosong - - ","",'0')
			->add("SALDO AWAL","saldo_awal",'0')
			->add("MEMORIAL","memorial",'0')
			->add("TRANSACTION","transaction",'0');

$dbtable 	= new DBTable($db,"smis_ac_grup");
$dbtable 	->setShowAll(true);
$list	 	= $dbtable->view("",0);
$adapter	= new SelectAdapter("nama","slug");
$grup		= $adapter->getContent($list['data']);
$grup[]		= array("name"=>"","value"=>"","default"=>1);

if(isset($_POST['dari'])){
    $dari 	= substr($_POST['dari'],0,10);
}
if(isset($_POST['sampai'])){
    $sampai = substr($_POST['sampai'],0,10);
}
if(isset($_POST['akun'])){
    $akun 	= $_POST['akun'];
}
if(isset($_POST['grup'])){
    $grup 	= $_POST['grup'];
}
if(isset($_POST['child'])){
    $child 	= $_POST['child'];
}
if(isset($_POST['form'])){
    $form 	= $_POST['form'];
}

$uitable ->addModal("dari", "hidden", "",$dari)
		 ->addModal("sampai", "hidden", "",$sampai)
		 ->addModal("akun", "hidden", "",$akun)
		 ->addModal("grup", "hidden", "",$grup)
		 ->addModal("child", "hidden", "",$child)
		 ->addModal("form", "hidden", "",$form)
		 ->addModal("filter_grup","select","Grup",$grup)
		 ->addModal("filter_jenis", "select", "Jenis",$jenis->getContent())
		 ->addModal("account", "select", "Account", $option)
		 ->addModal("all_child", "select", "Data Included", $all_child->getContent())
		 ->addModal("format", "select", "Format", $format->getContent());
$modal 	 = $uitable->getAdvanceModal ();
$modal	 ->setTitle ( "Buku Besar" );

$excel   = new Button("","","");
$excel   ->setAction("ledger.excel()")
		 ->setIcon("fa fa-download")
		 ->setClass("btn-primary")
		 ->setIsButton(Button::$ICONIC);
$modal	 ->getForm()
		 ->addElement("",$excel);
$uitable ->clearContent();

$preview = new Button("", "", "");
$preview ->setIsButton(Button::$ICONIC)
		 ->setAction("ledger.print_preview()")
		 ->setClass("btn-primary")
		 ->setIcon("fa fa-file-o");

$print	 = new Button("", "", "");
$print	 ->setIsButton(Button::$ICONIC)
		 ->setAction("ledger.preview()")
		 ->setClass("btn-primary")
		 ->setIcon("fa fa-file-text-o");
$modal	 ->getForm()
		 ->addElement("",$print);

echo addJS  ("framework/bootstrap/js/bootstrap-datepicker.js");
echo addCSS ("framework/bootstrap/css/datepicker.css");
echo addJS  ("framework/bootstrap/js/bootstrap-select.js");
echo addCSS ("framework/bootstrap/css/bootstrap-select.css");
echo addJS  ("framework/smis/js/table_action.js");
echo addJS  ("framework/smis/js/report_action.js");
echo addJS  ("accounting/resource/js/ledger.js",false);

echo "<h1>Buku Besar</h1>";
echo $modal		->getModalSkeleton();
echo $uitable	->getHtml ();
?>
