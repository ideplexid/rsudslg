<?php
global $db;
$uitable = new Report ( array ('Nomor',"Account","Uraian","Debet","Kredit" ), "&nbsp;", NULL );
$uitable->setName ( "income" );
$uitable->setDiagram ( false );
$uitable->setMode(Report::$DATE);
/* this is respond when system have to response */
if (isset ( $_POST ['command'] )) {
	require_once ("accounting/class/adapter/IncomeAdapter.php");
	
	$qv = "SELECT smis_ac_transaksi.id AS id, 
			smis_ac_transaksi.tanggal AS tanggal, 
			smis_ac_transaksi.nomor AS nomor, 
			smis_ac_transaksi.keterangan AS keterangan, 
			smis_ac_transaksi_detail.id AS id_detail, 
			smis_ac_transaksi_detail.nomor_account AS nomor_account, 
			smis_ac_transaksi_detail.nama_account AS nama_account, 
			smis_ac_transaksi_detail.keterangan AS uraian, 
			smis_ac_transaksi_detail.debet AS debet, 
			smis_ac_transaksi_detail.kredit AS kredit,
            smis_ac_transaksi_detail.grup AS grup,
			smis_ac_transaksi.prop AS prop
			FROM smis_ac_transaksi
			LEFT JOIN smis_ac_transaksi_detail 
			ON smis_ac_transaksi.id = smis_ac_transaksi_detail.id_transaksi 
			";
	
	$qc = "SELECT count(*) as total
			FROM smis_ac_transaksi
			LEFT JOIN smis_ac_transaksi_detail 
			ON smis_ac_transaksi.id = smis_ac_transaksi_detail.id_transaksi ";
	
	$column = array ();
	$dbtable = new DBTable ( $db, "smis_ac_transaksi", $column );
	$dbtable->setPreferredQuery ( true, $qv, $qc );
	$dbtable->setUseWhereforView ( true );
	$dbtable->setShowAll ( true );
	$dbtable->setForceOrderForQuery ( true, " smis_ac_transaksi_detail.nomor_account ASC " ); // not work, in dbtable
	$dbtable->addCustomKriteria ( "smis_ac_transaksi.fix ", " ='0' " );
	$dbtable->addCustomKriteria ( "smis_ac_transaksi.jenis ", " !='closed' " );
	$dbtable->addCustomKriteria ( "smis_ac_transaksi_detail.prop ", " NOT LIKE '%del%'" );
	$dbtable->addCustomKriteria ( null, "(smis_ac_transaksi_detail.nomor_account LIKE '5%' OR smis_ac_transaksi_detail.nomor_account LIKE '4%' )" );
	if(isset($_POST['filter_grup']) && $_POST['filter_grup']!="" && $_POST['filter_grup']!="null"){
        $dbtable->addCustomKriteria ( " smis_ac_transaksi_detail.grup ", "='".$_POST['filter_grup']."'" );
    }
    
	$dbres = new DBReport ( $dbtable, $uitable, new IncomeAdapter (), 'tanggal', DBReport::$DATE );
	$dbres->setMode(DBReport::$TIME);
    $data = $dbres->command ( $_POST ['command'] );
	echo json_encode ( $data, JSON_NUMERIC_CHECK );
	return;
}

$dbtable=new DBTable($db,"smis_ac_grup");
$dbtable->setShowAll(true);
$list=$dbtable->view("",0);
$selectadapter=new SelectAdapter("nama","slug");
$grup=$selectadapter->getContent($list['data']);
$grup[]=array("name"=>"","value"=>"","default"=>1);

/* This is Modal Form and used for add and edit the table */
$uitable->addModal("filter_grup","select","Grup",$grup);
$modal = $uitable->getAdvanceModal ();
$modal->setTitle ( "Laporan Laba Rugi" );



echo addJS ( "framework/bootstrap/js/bootstrap-datepicker.js" );
echo addCSS ( "framework/bootstrap/css/datepicker.css" );
echo addJS ( "framework/smis/js/table_action.js" );
echo addJS ( "framework/smis/js/report_action.js" );
echo addJS ( "framework/smis/js/report_action.js" );

echo $modal->getModalSkeleton();
echo $uitable->getHtml ();
?>

<script type="text/javascript">

var income;
$(document).ready(function(){
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
	$('.mydate').datepicker();
	var column=new Array('');
	income=new ReportAction("income","accounting","income",column);
    income.addRegulerData=function(add){
        add['filter_grup']=$("#error_filter_grup").val();
        return add;
    };
});
</script>

<style type='text/css'>
#table_income tfoot {
	display: none;
}
</style>