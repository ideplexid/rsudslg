<?php
	global $db;
    
    $option = new OptionBuilder();
    $option ->add("Debit","-1",1);
    $option ->add("Kredit","1",0);

    require_once 'smis-libs-class/MasterTemplate.php';
	$tipe = new MasterTemplate($db, "smis_ac_tipe_account", "accounting", "tipe_akun");
	$tipe->getUItable()->addHeaderElement("No.");
	$tipe->getUItable()->addHeaderElement("Tipe Akun");
	$tipe->getUItable()->addHeaderElement("Saldo Normal");
	$tipe->getUItable()->setAction(false);
	$tipe->getAdapter()->setUseNumber(true, "No.","back.");
	$tipe->getAdapter()->add("Tipe Akun", "tipe");
	$tipe->getAdapter()->add("Saldo Normal", "saldo_normal","trivial_1_Kredit_Debit");
	$tipe->getUItable()->addModal("id", "hidden", "", "");
	$tipe->getUItable()->addModal("tipe", "text", "Tipe Akun", "");
	$tipe->getUItable()->addModal("saldo_normal", "select", "Saldo Normal", $option ->getContent());
	$tipe->setModalTitle("Jenis Potongan");
	$tipe->initialize();