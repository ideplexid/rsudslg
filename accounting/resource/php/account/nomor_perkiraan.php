<?php
global $db;
global $user;
$header	 = array ('No.','Nomor','Name',"Tipe Akun","Saldo Normal","Saldo Awal","Update Terakhir" );
require_once "accounting/class/table/NomorAkunTable.php";
$uitable = new NomorAkunTable($header,"Accounting - Nomor Perkiraan");
$uitable ->setExcelButtonEnable(true);
$uitable ->setPDFButtonEnable(true);

if(isset($_POST['super_command']) && $_POST['super_command']!=""){
	$header	 = array ('No.','Nomor','Name' );
	$uitable ->setHeader($header);
	$uitable ->setName ("parent_nomor_perkiraan");
	$uitable ->setModel(Table::$SELECT);
	$dbtable = new DBTable($db,"smis_ac_account");
	require_once "accounting/class/adapter/NomorAkunAdapter.php";
	$adapter = new NomorAkunAdapter();
	$dbresponder = new DBResponder($dbtable,$uitable,$adapter);
	$super = new SuperCommand();
	$super ->addResponder ( "parent_nomor_perkiraan", $dbresponder );
	$init = $super->initialize ();
	if ($init != null) {
		echo $init;
		return;
	}
}

$uitable ->setName ("nomor_perkiraan");
if (isset ( $_POST ['command'] )) {
	if($_POST['command'] == "get_tipe_account"){
		$dtbale 	= new DBTable($db,"smis_ac_tipe_account");
		$data 		= $dtbale ->select($_POST['id']);
		$response 	= new ResponsePackage();
		$response 	->setContent($data);
		$response 	->setStatus(ResponsePackage::$STATUS_OK);
		echo json_encode($response ->getPackage());
		return ;
	}
	require_once "accounting/class/adapter/NomorAkunAdapter.php";
	require_once "accounting/class/responder/NomorPerkiraanResponder.php";
	
	$adapter = new NomorAkunAdapter();
	$dbtable = new DBTable($db,"smis_ac_account");
	$dbtable ->setOrder ("nama_tipe ASC, nomor ASC");
	$dbres 	 = new NomorPerkiraanResponder ($dbtable, $uitable, $adapter);
	$old_parent_number = "";
	if($dbres->isSave()){
		if($_POST['id']!="0"){
			$old = $dbtable->select($_POST['id']);
			$old_parent_number = $old->nomor;
		}
		$dbres->addColumnFixValue("update_oleh",$user->getNameOnly());
	}
	$data 	 = $dbres->command ($_POST ['command']);
	if($dbres->isSave()){
		if($_POST['id_parent']!="0"){
			require_once "accounting/function/recursive_sum.php";
			recursive_sum($db,$dbtable,$_POST['id_parent'],$_POST['child_number']);	
		}
		
		if($_POST['have_child']=="1" && $_POST['id']!=""){
			require_once "accounting/function/recursive_child.php";
			recursive_child($db,$old_parent_number,$_POST['nomor'],$_POST['id']);
		}
	}
	if($dbres->isDel()){
		require_once "accounting/function/recursive_del.php";
		recursive_del($db,$dbtable,$_POST['id']);
	}
	if($data!=NULL){
		echo json_encode($data);
	}
	return;
}

$dk = new OptionBuilder();
$dk ->add("","0","1")
	->add("Debet","-1","0")
	->add("Kredit","1","0");

$dbtable = new DBTable($db,"smis_ac_tipe_account");
$dbtable ->setShowAll(true);
$data 	 = $dbtable ->view("","0");
$select  = new SelectAdapter("tipe","id");
$ctx 	 = $select ->getContent($data['data']);
$ctx[] 	 = array("name"=>"","value"=>"","default"=>1);

$uitable ->addModal("id","hidden","","");
$uitable ->addModal("tipe","select","Tipe Akun",$ctx);
$uitable ->addModal("nama_tipe","hidden","","");
$uitable ->addModal("dk","select","Saldo Normal",$dk->getContent(),"n",null,true);
$uitable ->addModal("id_parent","hidden","","");
$uitable ->addModal("nama_parent","chooser-nomor_perkiraan-parent_nomor_perkiraan-Induk Akun","Induk","","y",null,true);
$uitable ->addModal("nomor_parent","text","Nomor Induk","","y",null,true);
$uitable ->addModal("nama","text","Nama Akun","");
$uitable ->addModal("nomor","text","Nomor Akun","","n",null,false,"nomor_perkiraan");
$uitable ->addModal("child_number","hidden","","");
$uitable ->addModal("have_child","hidden","","");
$uitable ->addModal("tgl_saldo_awal","date","Tanggal Saldo Awal","");
$uitable ->addModal("nilai_saldo_awal","money","Nilai Saldo Awal","");

$modal 		= $uitable->getModal ();
$modal		->setComponentSize(Modal::$MEDIUM);
$modal		->setTitle("Nomor Perkiraan");

echo $uitable	->getHtml ();
echo $modal		->getHtml ();
echo addJS 	("framework/bootstrap/js/bootstrap-datepicker.js");
echo addJS 	("framework/smis/js/table_action.js");
echo addJS 	("accounting/resource/js/nomor_perkiraan.js",false);
echo addCSS ("accounting/resource/css/nomor_perkiraan.css",false);
echo addCSS ("framework/bootstrap/css/datepicker.css");