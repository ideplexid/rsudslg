<?php
global $db;
global $user;
$header	 = array ('No.','Nomor','Name',"Tipe Akun","Saldo Normal","Saldo Awal","Update Terakhir" );
require_once "accounting/class/table/NomorAkunTable.php";
$uitable = new NomorAkunTable($header,"Accounting - Nomor Perkiraan");
$uitable ->setExcelButtonEnable(true);
$uitable ->setPDFButtonEnable(true);
$uitable ->setAction(false);
$uitable ->setName ("nomor_perkiraan_read");
if (isset ( $_POST ['command'] )) {
	require_once "accounting/class/adapter/NomorAkunAdapter.php";
	require_once "accounting/class/responder/NomorPerkiraanResponder.php";
	
	$adapter = new NomorAkunAdapter();
	$dbtable = new DBTable($db,"smis_ac_account");
	$dbtable ->setOrder ("nama_tipe ASC, nomor ASC");
	$dbres 	 = new NomorPerkiraanResponder ($dbtable, $uitable, $adapter);	
	$data 	 = $dbres->command ($_POST ['command']);
	if($data!==null){
		echo json_encode($data);
	}
	return;
}
echo $uitable	->getHtml ();
echo addJS 	("framework/bootstrap/js/bootstrap-datepicker.js");
echo addJS 	("framework/smis/js/table_action.js");
echo addJS 	("accounting/resource/js/nomor_perkiraan_read.js",false);
echo addCSS ("accounting/resource/css/nomor_perkiraan.css",false);
echo addCSS ("framework/bootstrap/css/datepicker.css");