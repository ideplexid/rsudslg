<?php 

global $db;
require_once ("smis-base/smis-include-service-consumer.php");

$header=array("No.","Nama","Debit","Kredit","Account","Nilai");
$table=new Table($header,"");

if(isset($_POST['command']) && $_POST['command']!=""){
	require_once "accounting/class/responder/AccountingPengeluaranResponder.php";
    $dbtable=new DBTable ( $db, "smis_ac_account" );
    $adapter=new SimpleAdapter();
    $dbres=new AccountingPengeluaranResponder($dbtable,$table,$adapter);
    $pack=$dbres->command($_POST['command']);
	echo json_encode($pack);
	return;
}

$dbtable=new DBTable($db,"smis_ac_grup");
$dbtable->setShowAll(true);
$list=$dbtable->view("",0);
$selectadapter=new SelectAdapter("nama","slug");
$grup=$selectadapter->getContent($list['data']);
$grup[]=array("name"=>"","value"=>"","default"=>1);

$table->addModal("noreg_pasien", "text", "No Reg Pasien", "","y");
$table->addModal("nama_pasien", "text", "Nama Pasien", "","y",NULL,true);
$table->addModal("nrm_pasien", "text", "NRM Pasien", "","y",NULL,true);
$table->addModal("keterangan", "text", "Keterangan", "","y",NULL,true);
$table->addModal("grup", "select", "Grup", $grup,"y",NULL,false);
$table->addModal("tanggal", "datetime", "Tanggal", "","y");
$table->addModal("nomor", "text", "Nomor", "","y");
$table->addModal("sisa", "money", "Sisa", "0","y",NULL,true);
$table->addModal("debit", "money", "Debit", "0","y",NULL,true);
$table->addModal("kredit", "money", "Kredit", "0","y",NULL,true);

$table->setFooterVisible(false);
$table->setActionEnable(false);
$table->setName("accounting_pasien_bayar");

$form=$table->getModal()->getForm();
$button=new Button("", "", "Load");
$button->setIcon("fa fa-refresh");
$button->setAction("accounting_pasien_bayar.loading()");
$button->setClass("btn-primary");
$button->setIsButton(Button::$ICONIC_TEXT);
$form->addElement("", $button);

$button=new Button("", "", "New");
$button->setIsButton(Button::$ICONIC_TEXT);
$button->setIcon("fa fa-plus-circle");
$button->setAction("accounting_pasien_bayar.newitem()");
$button->setClass("btn-danger");
$form->addElement("", $button);


$button=new Button("", "", "Save");
$button->setIsButton(Button::$ICONIC_TEXT);
$button->setIcon("fa fa-save");
$button->setAction("accounting_pasien_bayar.saveall()");
$button->setClass("btn-success");
$form->addElement("", $button);


echo $form->getHtml();
echo $table->getHtml();
echo addJS ( "framework/bootstrap/js/bootstrap-datetimepicker.js" );
echo addJS ( "framework/smis/js/table_action.js" );;
echo addCSS ( "framework/bootstrap/css/bootstrap-datetimepicker.css" );
echo addJS("framework/bootstrap/js/bootstrap-select.js");
echo addCSS ( "framework/bootstrap/css/bootstrap-select.css" );

echo addJS ( "accounting/resource/js/accounting_pasien_bayar.js",false );
echo addCSS ( "accounting/resource/css/accounting_pasien_bayar.css",false );


?>
