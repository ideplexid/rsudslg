<?php 

require_once 'smis-libs-class/MasterTemplate.php';

global $db;
global $user;

$bank_in = new MasterTemplate($db, "smis_acc_cashbank", "accounting", "bank_in");
$bank_in ->setDateTimeEnable(true)
		 ->setModalTitle("Bank Masuk");
$dbtable = $bank_in->getDBtable();
$dbtable ->addCustomKriteria("cb", " =0")
		 ->addCustomKriteria("io", " =1");

$uitable = $bank_in->getUItable();
$header  = array("Waktu","Penyerah","Penerima","Penginput","Nilai","No Bukti");
$uitable ->setHeader($header);
$uitable ->addModal("id", "hidden", "", "")
		 ->addModal("nobukti","text", "Nomor Bukti", "")
		 ->addModal("waktu","datetime", "Waktu", "")
		 ->addModal("nama_penyerah", "text", "Penyetor", "")
		 ->addModal("nama_penerima", "text", "No Rekening", "")
		 ->addModal("nilai","money", "Nilai", "")
		 ->addModal("keterangan", "textarea", "Keterangan", "");
$adapter = $bank_in->getAdapter();
$dbres	 = $bank_in->getDBResponder();
$adapter ->add("Waktu", "waktu","date d M Y H:i:s")
		 ->add("Penyerah", "nama_penyerah")
		 ->add("Penerima","nama_penerima")
		 ->add("Penginput","nama_penginput")
		 ->add("Nilai", "nilai","money Rp.")
		 ->add("No Bukti", "nobukti");

if($dbres->is("save") && ( $_POST['id']=="" || $_POST['id']=="0") ){
	$dbres->addColumnFixValue("nama_penginput", $user->getNameOnly());
	$dbres->addColumnFixValue("cb","0");
	$dbres->addColumnFixValue("io","1");
}
$bank_in->initialize();
?>