<?php 

require_once 'smis-libs-class/MasterTemplate.php';

global $db;
global $user;

$cash_out	= new MasterTemplate($db, "smis_acc_cashbank", "accounting", "cash_out");
$cash_out	->setModalTitle("Kas Keluar");
$dbtable	= $cash_out->getDBtable();
$dbtable	->addCustomKriteria("cb", " =1")
			->addCustomKriteria("io", " =0");
$uitable	= $cash_out->getUItable();
$header		= array("Waktu","Penyerah","Penerima","Penginput","Nilai","No Bukti");
$uitable	->setHeader($header);
$uitable	->addModal("id", "hidden", "", "")
			->addModal("nama_penyerah", "text", "Penyerah", "")
			->addModal("nama_penerima", "text", "Penerima", "")
			->addModal("nilai","money", "Nilai", "")
			->addModal("keterangan", "textarea", "Keterangan", "");
$adapter	= $cash_out->getAdapter();
$dbres		= $cash_out->getDBResponder();
$adapter	->add("Waktu", "waktu","date d M Y H:i:s")
			->add("Penyerah", "nama_penyerah")
			->add("Penerima","nama_penerima")
			->add("Penginput","nama_penginput")
			->add("Nilai", "nilai","money Rp.")
			->add("No Bukti", "nobukti");

if($dbres->is("save") && ( $_POST['id']=="" || $_POST['id']=="0") ){
	$dbres	 ->addColumnFixValue("nama_penginput", $user->getNameOnly());
	$random	 = rand(5, 20);
	$nobukti = "CI-".date("m/d/Y/H/i/s")."-".substr((md5($user->getNameOnly())), $random,3);
	$dbres	 ->addColumnFixValue("nobukti", $nobukti)
			 ->addColumnFixValue("cb","1")
			 ->addColumnFixValue("io","0");
}

$cash_out->initialize();
?>