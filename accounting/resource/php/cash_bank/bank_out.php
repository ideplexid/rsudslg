<?php 

require_once 'smis-libs-class/MasterTemplate.php';

global $db;
global $user;

$bank_out = new MasterTemplate($db, "smis_acc_cashbank", "accounting", "bank_out");
$bank_out ->setDateEnable(true)
		  ->setModalTitle("Bank Keluar");
$dbtable = $bank_out->getDBtable();
$dbtable ->addCustomKriteria("cb", " =0")
		 ->addCustomKriteria("io", " =0");

$uitable = $bank_out->getUItable();
$header	 = array("Waktu","Penyerah","Penerima","Penginput","Nilai","No Bukti");
$uitable ->setHeader($header);
$uitable ->addModal("id", "hidden", "", "")
		 ->addModal("nobukti","text", "Nomor Bukti", "")
		 ->addModal("nama_penyerah", "text", "No Rekening", "")
		 ->addModal("nama_penerima", "text", "Pengambil", "")
		 ->addModal("nilai","money", "Nilai", "")
		 ->addModal("keterangan", "textarea", "Keterangan", "");
$adapter = $bank_out->getAdapter();
$dbres	 = $bank_out->getDBResponder();
$adapter ->add("Waktu", "waktu","date d M Y H:i:s")
		 ->add("Penyerah", "nama_penyerah")
		 ->add("Penerima","nama_penerima")
		 ->add("Penginput","nama_penginput")
		 ->add("Nilai", "nilai","money Rp.")
		 ->add("No Bukti", "nobukti");

if($dbres->is("save") && ( $_POST['id']=="" || $_POST['id']=="0") ){
	$dbres->addColumnFixValue("nama_penginput", $user->getNameOnly());
	$dbres->addColumnFixValue("cb","0");
	$dbres->addColumnFixValue("io","0");
}
$bank_out->initialize();
?>