<?php
global $db;
$header=array ('Nomor',"Grup",'Tanggal','Keterangan',"Account","Uraian","Debet","Kredit" );
$uitable = new Report ( $header );
$uitable->setName ( "journal" );
$uitable->setDiagram ( false );
$uitable->setMode(Report::$DATETIME);
/* this is respond when system have to response */
if (isset ( $_POST ['command'] )) {
	require_once "accounting/class/adapter/JournalAdapter.php";
	
	$qv = "SELECT smis_ac_transaksi.id AS id, 
			smis_ac_transaksi.tanggal AS tanggal, 
			smis_ac_transaksi.nomor AS nomor, 
			smis_ac_transaksi.keterangan AS keterangan, 
			smis_ac_transaksi_detail.id AS id_detail, 
			smis_ac_transaksi_detail.nomor_account AS nomor_account, 
			smis_ac_transaksi_detail.nama_account AS nama_account, 
			smis_ac_transaksi_detail.keterangan AS uraian, 
			smis_ac_transaksi_detail.debet AS debet, 
			smis_ac_transaksi_detail.kredit AS kredit,
            smis_ac_transaksi_detail.grup as grup,
			smis_ac_transaksi.prop AS prop
			FROM smis_ac_transaksi
			LEFT JOIN smis_ac_transaksi_detail 
			ON smis_ac_transaksi.id = smis_ac_transaksi_detail.id_transaksi 
			";
	
	$qc = "SELECT count(*) as total
			FROM smis_ac_transaksi
			LEFT JOIN smis_ac_transaksi_detail 
			ON smis_ac_transaksi.id = smis_ac_transaksi_detail.id_transaksi ";
	
	$column = array ();
	$dbtable = new DBTable ( $db, "smis_ac_transaksi", $column );
	$dbtable->setPreferredQuery ( true, $qv, $qc );
	$dbtable->setUseWhereforView ( true );
	$dbtable->addCustomKriteria ( "smis_ac_transaksi.fix ", " ='0' " );
	$dbtable->addCustomKriteria ( "smis_ac_transaksi.jenis ", " ='transaction' " );
	$dbtable->addCustomKriteria ( "smis_ac_transaksi_detail.prop ", " NOT LIKE '%del%'" );
	if(isset($_POST['filter_grup']) && $_POST['filter_grup']!=""){
        $dbtable->addCustomKriteria ( " smis_ac_transaksi_detail.grup ", "='".$_POST['filter_grup']."'" );
    }
        
	$dbres = new DBReport ( $dbtable, $uitable, new JournalAdapter(), 'tanggal', DBReport::$DATE );
	$dbres->setMode(DBReport::$TIME);
    $data = $dbres->command ( $_POST ['command'] );
	echo json_encode ( $data, JSON_NUMERIC_CHECK );
	return;
}

$dbtable=new DBTable($db,"smis_ac_grup");
$dbtable->setShowAll(true);
$list=$dbtable->view("",0);
$selectadapter=new SelectAdapter("nama","slug");
$grup=$selectadapter->getContent($list['data']);
$grup[]=array("name"=>"","value"=>"","default"=>1);

/* This is Modal Form and used for add and edit the table */
$uitable->addModal("filter_grup","select","Grup",$grup);
$modal = $uitable->getAdvanceModal ();
$modal->setTitle ( "Jurnal Transaksi" );

echo addJS ( "framework/bootstrap/js/bootstrap-datetimepicker.js" );
echo addCSS ( "framework/bootstrap/css/bootstrap-datetimepicker.css" );
echo addJS ( "framework/smis/js/table_action.js" );
echo addJS ( "framework/smis/js/report_action.js" );
echo "<h1>Jurnal Transaksi</h1>";
echo $modal->getModalSkeleton();
echo $uitable->getHtml ();
?>

<script type="text/javascript">

var journal;
$(document).ready(function(){
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
	$('.mydatetime').datetimepicker({minuteStep:1});
	var column=new Array('');
	journal=new ReportAction("journal","accounting","journal",column);
    journal.addRegulerData=function(a){
        a['filter_grup']=$("#"+this.prefix+"_filter_grup").val();
        return a;
    };
});
</script>