<?php
global $db;
require_once ("smis-framework/smis/database/DBParentChildResponder.php");

$head=array('Tanggal',"Grup",'Nomor','Keterangan',"Debet","Kredit" );
$parent_table = new Table ( $head, "", NULL, true );
$parent_table->setName ( "saldo_awal_parent" );
$parent_table->setActionName ( "saldo_awal.parent" );

$child_table = new Table ( array ("No.",'Nomor Account',"Account",'Keterangan',"Debet","Kredit" ), "", NULL, true );
$child_table->setName ( "saldo_awal_child" );
$child_table->setActionName ( "saldo_awal.child" );
$child_table->setClass ( "" );
$child_table->setFooterVisible(false);


/* this is respond when system have to response */
if (isset ( $_POST ['command'] )) {
    require_once ("accounting/class/adapter/AccountingChildSummaryAdapter.php");
	require_once ("accounting/class/adapter/ParentAdapter.php");
	require_once ("accounting/class/responder/DetailResponder.php");
	require_once ("accounting/class/responder/ParentResponder.php");
    
	
	
	// parent
	
	$parent_adapter = new ParentAdapter ();
	$parent_column = array ('id','tanggal','nomor',"keterangan","grup");
	$parent_dbtable = new DBTable ( $db, "smis_ac_transaksi", $parent_column );
	$parent_dbtable->addCustomKriteria ( " jenis ", "='saldo_awal'" );
	$parent_dbtable->addCustomKriteria ( " fix ", "='0'" );
	$parent_dbtable->setOrder(" DATE(tanggal) DESC, nomor DESC ");
	if(isset($_POST['filter_grup']) && $_POST['filter_grup']!=""){
        $parent_dbtable->addCustomKriteria ( " grup ", "='".$_POST['filter_grup']."'" );
    }
    $parent = new ParentResponder ( $parent_dbtable, $parent_table, $parent_adapter );
	$parent->addColumnFixValue("jenis","saldo_awal");
    
	// child
	$nama_account = "";
	if (isset ( $_POST ['nomor_account'] )) {
		$query = "SELECT nama FROM smis_ac_account WHERE nomor='" . $_POST ['nomor_account'] . "' ";
		$nama_account = $db->get_var ( $query );
	}
		
	$child_adapter = new AccountingChildSummaryAdapter();
	$child_adapter->setUseNumber(true,"No.","back.");
	$child_adapter->addFixValue("Keterangan", "<strong>TOTAL</strong>");
	$child_adapter->addSummary("Debet", "debet","money Rp.");
	$child_adapter->addSummary("Kredit", "kredit","money Rp.");
	$child_adapter->add("Nomor Account", "nomor_account");
	$child_adapter->add("Account", "nama_account");
	$child_adapter->add("Keterangan", "keterangan");
	$child_adapter->add("Debet", "debet","money Rp.");
	$child_adapter->add("Kredit", "kredit","money Rp.");
	$child_column = array (	'id','id_transaksi','nomor_account',"keterangan","debet","kredit");
	$child_dbtable = new DBTable ( $db, "smis_ac_transaksi_detail", $child_column );
	$child_dbtable->setShowAll(true);
	$child = new DetailResponder ( $child_dbtable, $child_table, $child_adapter, "id_transaksi", $nama_account );
	
	$dbres = new DBParentChildResponder ( $parent, $child );
	$data = $dbres->command ( $_POST ['super_command'], $_POST ['command'] );
	echo json_encode ( $data );
	return;
}

$dbtable=new DBTable($db,"smis_ac_grup");
$dbtable->setShowAll(true);
$list=$dbtable->view("",0);
$selectadapter=new SelectAdapter("nama","slug");
$grup=$selectadapter->getContent($list['data']);
$grup[]=array("name"=>"","value"=>"","default"=>1);

/* Parent Modal */
$parent_table->addModal("id","hidden","","");
$parent_table->addModal("tanggal","datetime","Tanggal",date("Y-m-d H:i:s"));
$parent_table->addModal("nomor","text","Nomor","","y",null,true);
$parent_table->addModal("grup","select","Grup",$grup);
$parent_table->addModal("keterangan","text","Keterangan","");
$modal_parent = $parent_table->getModal ();
$modal_parent->setTitle ( "Saldo Awal" );
$modal_parent->addBody ( "detail_saldo_awal", $child_table );
$modal_parent->setClass ( Modal::$FULL_MODEL );

/* Child Modal */
$dbtable_account = new DBTable ( $db, "smis_ac_account" );
$dbtable_account->setShowAll ( true );
$dbtable_account->setOrder ( "nomor ASC" );
$data = $dbtable_account->view ( "", "0" );
$data_list = $data ['data'];
class account_adapter extends ArrayAdapter {
	public function adapt($d) {
		$a = array ();
		$a ["name"] = $d->nomor . " - " . $d->nama;
		$a ["value"] = $d->nomor;
		return $a;
	}
}
$account = new account_adapter ();
$account_list = $account->getContent ( $data_list );

$child_table->addModal("id", "hidden", "", "");
$child_table->addModal("id_transaksi", "hidden", "", "");
$child_table->addModal("nomor_account", "select", "Nomor Akun", $account_list,"n",null,false,null,true,"nomor_account");
$child_table->addModal("keterangan", "text", "Keterangan", "","y",null,false,null,false,"debet");
$child_table->addModal("debet", "money", "Debet", "","y",null,false,null,false,"kredit");
$child_table->addModal("kredit", "money", "Kredit", "","y",null,false,null,false,"save");

$modal_child = $child_table->getModal ();
$modal_child->setTitle ( "Detail Saldo Awal" );

$select=new Select("saldo_awal_filter_grup","",$grup);
$button=new Button("","","");
$button->setIcon("fa fa-search");
$button->setAction("saldo_awal.parent.view()");
$button->setClass("btn-primary");
$button->setIsButton(Button::$ICONIC);
$form=new Form("","","");
$form->addElement("Grup",$select,"");
$form->addElement("",$button,"");

echo "<h1>Saldo Awal</h1>";
echo $form->getHtml();
echo $parent_table->getHtml ();
echo $modal_parent->getHtml ();
echo $modal_child->getHtml ();
echo addJS ( "framework/bootstrap/js/bootstrap-datetimepicker.js" );
echo addCSS ( "framework/bootstrap/css/bootstrap-datetimepicker.css" );
echo addJS ( "framework/smis/js/table_action.js" );
echo addJS ( "framework/smis/js/child_parent.js" );
echo addCSS ( "framework/bootstrap/css/datepicker.css" );
echo addJS("framework/bootstrap/js/bootstrap-select.js");
echo addCSS ( "framework/bootstrap/css/bootstrap-select.css" );

echo addJS("accounting/resource/js/saldo_awal.js",false);
echo addCSS ( "accounting/resource/css/saldo_awal.css" ,false);


?>
