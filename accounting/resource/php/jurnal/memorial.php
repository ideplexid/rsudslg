<?php
global $db;
require_once   ("smis-framework/smis/database/DBParentChildResponder.php");
$parent_table  = new Table(array('Tanggal',"Grup",'Nomor','Keterangan',"Debet","Kredit"), "", NULL, true );
$parent_table  ->setName("memorial_parent")
			   ->setActionName("memorial.parent");

$child_table   = new Table(array('No.','Nomor Account',"Account",'Keterangan',"Debet","Kredit"), "", NULL, true );
$child_table   ->setName("memorial_child")
			   ->setActionName("memorial.child")
			   ->setClass("well");

/* this is respond when system have to response */
if (isset ( $_POST ['command'] )) {
	require_once ("accounting/class/adapter/AccountingChildSummaryAdapter.php");
	require_once ("accounting/class/adapter/ParentAdapter.php");
	require_once ("accounting/class/responder/DetailResponder.php");
	require_once ("accounting/class/responder/ParentResponder.php");
	
	$parent_adapter = new ParentAdapter ();
	$parent_column 	= array ('id','tanggal','nomor',"keterangan","debet","kredit","grup");
	$parent_dbtable = new DBTable ( $db, "smis_ac_transaksi", $parent_column );
	$parent_dbtable ->addCustomKriteria ( " jenis ", "='memorial'" )
					->addCustomKriteria ( " fix ", "='0'" )
					->setOrder(" DATE(tanggal) DESC, nomor DESC ");
	if(isset($_POST['filter_grup']) && $_POST['filter_grup']!=""){
        $parent_dbtable->addCustomKriteria ( " grup ", "='".$_POST['filter_grup']."'" );
    }
    $parent = new ParentResponder ( $parent_dbtable, $parent_table, $parent_adapter );	
	$parent ->addColumnFixValue("jenis","memorial");
	
	$nama_account 		= "";
	if (isset ( $_POST ['nomor_account'] )) {
		$query 			= "SELECT nama FROM smis_ac_account WHERE nomor='" . $_POST ['nomor_account'] . "' ";
		$nama_account 	= $db->get_var ( $query );
	}	
	$child_adapter 	= new AccountingChildSummaryAdapter(false,"","memorial");
	$child_adapter	->setUseNumber(true,"No.","back.")
					->addFixValue("Keterangan", "<strong>TOTAL</strong>")
					->addSummary("Debet", "debet","money Rp.")
					->addSummary("Kredit", "kredit","money Rp.")
					->add("Nomor Account", "nomor_account")
					->add("Account", "nama_account")
					->add("Keterangan", "keterangan")
					->add("Debet", "debet","money Rp.")
					->add("Kredit", "kredit","money Rp.");
	$child_dbtable 	= new DBTable($db,"smis_ac_transaksi_detail");
	$child 			= new DetailResponder($child_dbtable,$child_table,$child_adapter,"id_transaksi",$nama_account);
	$dbres 			= new DBParentChildResponder ( $parent, $child );
    if($dbres->isParentProcess($_POST ['super_command']) && $_POST['command']=="save" && $_POST['id']!="0" &&  $_POST['id']!=0){
        if(!isset($_POST['nomor']) || $_POST['nomor']==""){
            require_once "accounting/function/new_transaksi_harian.php";
            $format	= new_transaksi_harian($db,$_POST['tanggal'],"JUM");
            $parent ->addColumnFixValue("nomor",$format);
        }
    }
	$data = $dbres->command ( $_POST ['super_command'], $_POST ['command'] );
	echo json_encode ( $data );
	return;
}

$dbtable		= new DBTable($db,"smis_ac_grup");
$dbtable->setShowAll(true);
$list			= $dbtable->view("",0);
$selectadapter	= new SelectAdapter("nama","slug");
$grup			= $selectadapter->getContent($list['data']);
$grup[]			= array("name"=>"","value"=>"","default"=>1);
$parent_table	->addModal("id","hidden","","")
				->addModal("tanggal","datetime","Tanggal",date("Y-m-d H:i:s"))
				->addModal("nomor","text","Nomor","","y",null,true)
				->addModal("grup","select","Grup",$grup)
				->addModal("keterangan","text","Keterangan","");
$modal_parent 	= $parent_table->getModal ();
$modal_parent 	->setTitle ( "Input Jurnal Umum" );
$modal_parent 	->addBody ( "detail_memorial", $child_table );
$modal_parent 	->setClass ( Modal::$FULL_MODEL );

/* Child Modal */
$dbtable_account  = new DBTable ( $db, "smis_ac_account" );
$dbtable_account  ->setShowAll ( true );
$dbtable_account  ->setOrder ( "nomor ASC" );
$data 			  = $dbtable_account->view ( "", "0" );
$data_list 		  = $data ['data'];

require_once 'accounting/class/adapter/AccountAdapter.php';
$account 		= new AccountAdapter();
$account_list 	= $account->getContent ( $data_list );

$child_table ->addModal("id", "hidden", "", "")
			 ->addModal("id_transaksi", "hidden", "", "")
			 ->addModal("nomor_account", "select", "Nomor Akun", $account_list,"n",null,false,null,true,"nomor_account")
			 ->addModal("keterangan", "text", "Keterangan", "","y",null,false,null,false,"debet")
			 ->addModal("debet", "money", "Debet", "","y",null,false,null,false,"kredit")
			 ->addModal("kredit", "money", "Kredit", "","y",null,false,null,false,"save");
$modal_child = $child_table->getModal ();
$modal_child ->setTitle ( "Jurnal Umum" );

$select = new Select("memorial_filter_grup","",$grup);
$button = new Button("","","");
$button	->setIcon("fa fa-search")
		->setAction("memorial.parent.view()")
		->setClass("btn-primary")
		->setIsButton(Button::$ICONIC);
$form	= new Form("","","");
$form	->addElement("Grup",$select,"")
		->addElement("",$button,"");

$m_date	  	 = getSettings($db,"accounting-transaksi-min-date","1900-01-01");
$min_date 	 = new Hidden("transaction_memorial_start_date","transaction_masuk_start_date",$m_date);
		
echo "<h1>Input Jurnal Umum</h1>";
echo $form			->getHtml();
echo $parent_table	->getHtml ();
echo $modal_parent	->getHtml ();
echo $modal_child	->getHtml ();
echo $min_date		->getHtml ();

echo addJS  ("framework/bootstrap/js/bootstrap-datepicker.js");
echo addCSS ("framework/bootstrap/css/datepicker.css");
echo addJS  ("framework/bootstrap/js/bootstrap-datetimepicker.js");
echo addCSS ("framework/bootstrap/css/bootstrap-datetimepicker.css");
echo addJS  ("framework/smis/js/table_action.js");
echo addJS  ("framework/smis/js/child_parent.js");
echo addJS  ("accounting/resource/js/memorial.js",false);
echo addCSS ("accounting/resource/css/transaction.css",false);

?>