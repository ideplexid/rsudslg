<?php
	global $NAVIGATOR;

	// Administrator Top Menu
	$mr = new Menu ("fa fa-dollar");
	$mr->addProperty ( 'title', 'Akuntansi' );
	$mr->addProperty ( 'name', 'Akuntansi' );

	$mr->addSubMenu ( "Nomor Perkiraan", "accounting", "account", "Account of Accounting" ,"fa fa-sort-numeric-asc");
	//$mr->addSubMenu ( "No Perkiraan", "accounting", "account_read", "Account of Accounting" ,"fa fa-sort-numeric-asc");
	$mr->addSubMenu ( "List Transaksi", "accounting", "notify", "List Notify of transacation" ,"fa fa-lightbulb-o");
	$mr->addSubMenu ( "Jurnal", "accounting", "jurnal", "Journal Transaction","fa fa-ticket" );
	$mr->addSubMenu ( "Laporan", "accounting", "laporan", "Report Transaction","fa fa-book" );
	$mr->addSubMenu ( "Grup Transaksi", "accounting", "grup_transaksi", "Grup Transaksi Akunting" ,"fa fa-tag");
	$mr->addSubMenu ( "Inventaris", "accounting", "inventaris", "Reporting" ,"fa fa-book");
	$mr->addSubMenu ( "Hutang", "accounting", "hutang", "Hutang" ,"fa fa-credit-card");
	$mr->addSubMenu ( "Beban", "accounting", "beban", "Beban" ,"fa fa-money");
	$mr->addSubMenu ( "Piutang", "accounting", "receivable", "Piutang" ,"fa fa-money");
	$mr->addSubMenu ( "Cash Bank", "accounting", "cashbank", "Kas dan Bank" ,"fa fa-bank");
	$mr->addSubMenu ( "Settings", "accounting", "settings", "Settings" ,"fa fa-cogs");
	
	$mr->addSeparator ();
	require_once 'smis-libs-inventory/navigation.php';
	$inventory_navigator = new InventoryNavigator($NAVIGATOR, "", "Accounting", "accounting");
	$mr = $inventory_navigator->extendMenu($mr);
	$NAVIGATOR->addMenu ( $mr, 'accounting' );
?>
