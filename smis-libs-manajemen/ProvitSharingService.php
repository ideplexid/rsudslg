<?php
require_once ("smis-base/smis-include-service-consumer.php");
class ProvitSharingService extends ServiceConsumer {
	public function __construct($db, $ruang, $provit,$carabayar="") {
		$data = array (
				"ruang" => $ruang,
				"provit" => $provit,
				"carabayar"=>$carabayar
		);
		parent::__construct ( $db, "get_pv", $data, "manajemen" );
		$this->setMode ( ServiceConsumer::$SINGLE_MODE );
	}
	public function getContent() {
		$data = parent::getContent ();
		return json_encode ( $data );
	}
}

?>