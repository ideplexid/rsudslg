<?php
global $db;
global $user;
require_once 'smis-libs-class/Policy.php';
require_once 'smis-libs-inventory/policy.php';
$ipolicy=new InventoryPolicy("fisiotherapy", $user,"modul/");

$policy=new Policy("medical_checkup", $user);
$policy->addPolicy("pemeriksaan", "pemeriksaan", Policy::$DEFAULT_COOKIE_CHANGE,"modul/pemeriksaan");
$policy->addPolicy("mcu_rs", "mcu_rs", Policy::$DEFAULT_COOKIE_CHANGE,"modul/mcu_rs");
$policy->addPolicy("settings", "settings", Policy::$DEFAULT_COOKIE_CHANGE,"modul/settings");
$policy->addPolicy("laporan", "laporan", Policy::$DEFAULT_COOKIE_CHANGE,"modul/laporan");
$policy->setDefaultPolicy(Policy::$DEFAULT_POLICY_RESTRICT);
$policy->combinePolicy($ipolicy);
$policy->initialize();

?>
