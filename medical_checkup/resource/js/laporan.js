var laporan;
var dokter_konsul;
var dokter_periksa;
var pengirim;

$(document).ready(function(){
	$('.mydate').datepicker();
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
	var column  = new Array('id','nama','keterangan','slug');
	laporan     = new TableAction("laporan","medical_checkup","laporan",column);
	laporan.getRegulerData=function(){
		var reg_data    = {	
			page                : this.page,
			action              : this.action,
			super_command       : this.super_command,
			prototype_name      : this.prototype_name,
			prototype_slug      : this.prototype_slug,
			prototype_implement : this.prototype_implement,
			dari                : $("#"+this.prefix+"_dari").val(),
			sampai              : $("#"+this.prefix+"_sampai").val(),
            carabayar           : $("#"+this.prefix+"_carabayar").val(),
            dokter_konsul       : $("#"+this.prefix+"_dokter_konsul").val(),
            dokter_periksa      : $("#"+this.prefix+"_dokter_periksa").val(),
            pengirim            : $("#"+this.prefix+"_pengirim").val(),
            id_dokter_konsul     : $("#"+this.prefix+"_id_dokter_konsul").val(),
            id_dokter_periksa    : $("#"+this.prefix+"_id_dokter_periksa").val(),
            id_pengirim          : $("#"+this.prefix+"_id_pengirim").val()

		};
		return reg_data;
    };
    dokter_konsul   = new TableAction("dokter_konsul","medical_checkup","laporan",new Array());
    dokter_periksa  = new TableAction("dokter_periksa","medical_checkup","laporan",new Array());
    pengirim        = new TableAction("pengirim","medical_checkup","laporan",new Array());
    
    dokter_konsul.setSuperCommand("dokter_konsul");
    dokter_periksa.setSuperCommand("dokter_periksa");
    pengirim.setSuperCommand("pengirim");

    dokter_konsul.selected = function(a){
        $("#laporan_dokter_konsul").val(a.nama);
        $("#laporan_id_dokter_konsul").val(a.id);
    };
    dokter_periksa.selected = function(a){
        $("#laporan_dokter_periksa").val(a.nama);
        $("#laporan_id_dokter_periksa").val(a.id);
    };
    pengirim.selected = function(a){
        $("#laporan_pengirim").val(a.nama);
        $("#laporan_id_pengirim").val(a.id);
    };



    laporan.view();	
});