var mcu_action;
var mcu_pasien;
var mcu_list_pesan;
var mcu_noreg=$("#mcu_noreg_pasien").val();
var mcu_nama_pasien=$("#mcu_nama_pasien").val();
var mcu_nrm_pasien=$("#mcu_nrm_pasien").val();
var mcu_polislug=$("#mcu_polislug").val();
var mcu_the_page=$("#mcu_page").val();
var mcu_the_protoslug=$("#mcu_protoslug").val();
var mcu_the_protoname=$("#mcu_protoname").val();
var mcu_the_protoimplement=$("#mcu_protoimplement").val();
var MCU_PREFIX=$("#mcu_action").val();
var MCU_MODE=$("#mcu_mode").val();
var marketing;
var pengirim;
var dokter_konsul;
var dokter_periksa;

$(document).ready(function() {
    $(".mydatetime").datetimepicker({minuteStep:1});
    mcu_pasien=new TableAction("mcu_pasien",mcu_the_page,MCU_PREFIX,new Array());
    mcu_pasien.setSuperCommand("mcu_pasien");
    mcu_pasien.setPrototipe(mcu_the_protoname,mcu_the_protoslug,mcu_the_protoimplement);
    mcu_pasien.selected=function(json){
        $("#"+MCU_PREFIX+"_nama_pasien").val(json.nama_pasien);
        $("#"+MCU_PREFIX+"_nrm_pasien").val(json.nrm);
        $("#"+MCU_PREFIX+"_noreg_pasien").val(json.id);
        $("#"+MCU_PREFIX+"_carabayar").val(json.carabayar);
        $("#"+MCU_PREFIX+"_jk").val(json.kelamin);
        $("#"+MCU_PREFIX+"_umur").val(json.umur);
        $("#"+MCU_PREFIX+"_alamat").val(json.alamat_pasien);
        $("#"+MCU_PREFIX+"_ibu").val(json.ibu);
    };
    
    mcu_paket=new TableAction("mcu_paket",mcu_the_page,MCU_PREFIX,new Array());
    mcu_paket.setSuperCommand("mcu_paket");
    mcu_paket.setPrototipe(mcu_the_protoname,mcu_the_protoslug,mcu_the_protoimplement);
    mcu_paket.selected=function(json){
        $("#"+MCU_PREFIX+"_nama").val(json.nama);
        $("#"+MCU_PREFIX+"_laboratory").val(json.laboratory);
        $("#"+MCU_PREFIX+"_radiology").val(json.radiology);
        $("#"+MCU_PREFIX+"_fisiotherapy").val(json.fisiotherapy);
        $("#"+MCU_PREFIX+"_elektromedis").val(json.elektromedis);
        $("#"+MCU_PREFIX+"_tarif_laboratory").val(json.tarif_laboratory);
        $("#"+MCU_PREFIX+"_tarif_radiology").val(json.tarif_radiology);
        $("#"+MCU_PREFIX+"_tarif_fisiotherapy").val(json.tarif_fisiotherapy);
        $("#"+MCU_PREFIX+"_tarif_elektromedis").val(json.tarif_elektromedis);
        $("#"+MCU_PREFIX+"_total_laboratory").val(json.total_laboratory);
        $("#"+MCU_PREFIX+"_total_radiology").val(json.total_radiology);
        $("#"+MCU_PREFIX+"_total_fisiotherapy").val(json.total_fisiotherapy);
        $("#"+MCU_PREFIX+"_total_elektromedis").val(json.total_elektromedis);
        setMoney("#"+MCU_PREFIX+"_total",Number(json.tarif_manual));
        $("#"+MCU_PREFIX+"_kelas").val(json.kelas);
        $("#"+MCU_PREFIX+"_marketing").val(json.marketing);
        $("#"+MCU_PREFIX+"_id_marketing").val(json.id_marketing);
        $("#"+MCU_PREFIX+"_pengirim").val(json.pengirim);
        $("#"+MCU_PREFIX+"_id_pengirim").val(json.id_pengirim);
        $("#"+MCU_PREFIX+"_bagi_hasil_dokter_konsul").val(json.bagi_konsul);
        $("#"+MCU_PREFIX+"_bagi_hasil_dokter_periksa").val(json.bagi_periksa);
    };
    mcu_paket.addRegulerData = function(data){
        data['nama_pasien'] = mcu_nama_pasien;
        data['noreg_pasien'] = mcu_noreg;
        data['nrm_pasien'] = mcu_nrm_pasien;
        return data;
    }
    
    var column=new Array("id","tanggal","nama","alamat","ibu",
                    "kelas","nama_pasien","noreg_pasien",
                    "nrm_pasien","uri","jk","umur","carabayar","total",
                    "marketing", "id_marketing", "pengirim", "id_pengirim",
                    "laboratory","radiology","fisiotherapy","elektromedis",
                    "tarif_laboratory","tarif_radiology","tarif_fisiotherapy","tarif_elektromedis",
                    "total_laboratory","total_radiology","total_fisiotherapy","total_elektromedis",
                    "id_dokter_konsul", "dokter_konsul", "bagi_hasil_dokter_konsul",
                    "id_dokter_periksa", "dokter_periksa", "bagi_hasil_dokter_periksa","ruangan"
                    );
    mcu_action=new TableAction(MCU_PREFIX,mcu_the_page,MCU_PREFIX,column);
    mcu_action.setPrototipe(mcu_the_protoname,mcu_the_protoslug,mcu_the_protoimplement);
    mcu_action.setEnableAutofocus(true);
    mcu_action.setNextEnter();
    mcu_action.getRegulerData=function(){
        var reg_data={	
            page:this.page,
            action:this.action,
            super_command:this.super_command,
            prototype_name:this.prototype_name,
            prototype_slug:this.prototype_slug,
            prototype_implement:this.prototype_implement,
            polislug:mcu_polislug,
            noreg_pasien:mcu_noreg,
            nama_pasien:mcu_nama_pasien,
            nrm_pasien:mcu_nrm_pasien,
            mode:MCU_MODE
        };
        return reg_data;
    };

    mcu_action.selesai=function(id){
        var selesai_data=this.getRegulerData();
        var self=this;
        selesai_data['command']="selesai";
        selesai_data['id']=id;
        showLoading();
        $.post("",selesai_data,function(res){
            var json=getContent(res);
            self.view();
            dismissLoading();
        });
    };
    mcu_action.view();



    mcu_action.save = function(){
        showLoading();
        var cek = this.cekTutupTagihan();
        if(cek){
            TableAction.prototype.save.call(this);        
        }
        dismissLoading();
    };

    mcu_action.cekTutupTagihan = function(){
        var reg_data={	
            page:this.page,
            action:this.action,
            super_command:this.super_command,
            prototype_name:this.prototype_name,
            prototype_slug:this.prototype_slug,
            prototype_implement:this.prototype_implement,
        };			
        var noreg                 = $("#"+this.prefix+"_noreg_pasien").val();
        if(noreg==""){
            smis_alert("Error","Silakan Pilih Pasien Terlebih Dahulu","alert-danger");
            return;
        }
        reg_data['command']        = 'cek_tutup_tagihan';
        reg_data['noreg_pasien']  = noreg;
        
        var res = $.ajax({
            type: "POST",
            url: "",
            data:reg_data,
            async: false
        }).responseText;

        var json = getContent(res);
        if(json=="1"){
            return false;
        }else{
            return true;
        }
    };

    window[MCU_PREFIX]=mcu_action;
    
    marketing = new TableAction("marketing", mcu_the_page, MCU_PREFIX, new Array());
    marketing.setSuperCommand("marketing");
    marketing.setPrototipe(mcu_the_protoname,mcu_the_protoslug,mcu_the_protoimplement);
    marketing.selected = function(json){
		$("#"+MCU_PREFIX+"_marketing").val(json.nama);
		$("#"+MCU_PREFIX+"_id_marketing").val(json.id);
	};
    stypeahead("#"+MCU_PREFIX+"_marketing",3,marketing,"nama",function(json){
		$("#"+MCU_PREFIX+"_marketing").val(json.nama);
		$("#"+MCU_PREFIX+"_id_marketing").val(json.id);
	});
    
    pengirim = new TableAction("pengirim", mcu_the_page, MCU_PREFIX, new Array());
    pengirim.setSuperCommand("pengirim");
    pengirim.setPrototipe(mcu_the_protoname,mcu_the_protoslug,mcu_the_protoimplement);
    pengirim.selected = function(json){
		$("#"+MCU_PREFIX+"_pengirim").val(json.nama);
		$("#"+MCU_PREFIX+"_id_pengirim").val(json.id);
	};
    stypeahead("#"+MCU_PREFIX+"_pengirim",3,pengirim,"nama",function(json){
		$("#"+MCU_PREFIX+"_pengirim").val(json.nama);
		$("#"+MCU_PREFIX+"_id_pengirim").val(json.id);
	});
    
    dokter_konsul = new TableAction("dokter_konsul", mcu_the_page, MCU_PREFIX, new Array());
    dokter_konsul.setSuperCommand("dokter_konsul");
    dokter_konsul.setPrototipe(mcu_the_protoname,mcu_the_protoslug,mcu_the_protoimplement);
    dokter_konsul.selected = function(json){
		$("#"+MCU_PREFIX+"_dokter_konsul").val(json.nama);
		$("#"+MCU_PREFIX+"_id_dokter_konsul").val(json.id);
	};
    stypeahead("#"+MCU_PREFIX+"_dokter_konsul",3,pengirim,"nama",function(json){
		$("#"+MCU_PREFIX+"_dokter_konsul").val(json.nama);
		$("#"+MCU_PREFIX+"_id_dokter_konsul").val(json.id);
	});
    
    dokter_periksa = new TableAction("dokter_periksa", mcu_the_page, MCU_PREFIX, new Array());
    dokter_periksa.setSuperCommand("dokter_periksa");
    dokter_periksa.setPrototipe(mcu_the_protoname,mcu_the_protoslug,mcu_the_protoimplement);
    dokter_periksa.selected = function(json){
		$("#"+MCU_PREFIX+"_dokter_periksa").val(json.nama);
		$("#"+MCU_PREFIX+"_id_dokter_periksa").val(json.id);
	};
    stypeahead("#"+MCU_PREFIX+"_dokter_periksa",3,pengirim,"nama",function(json){
		$("#"+MCU_PREFIX+"_dokter_periksa").val(json.nama);
		$("#"+MCU_PREFIX+"_id_dokter_periksa").val(json.id);
	});
});