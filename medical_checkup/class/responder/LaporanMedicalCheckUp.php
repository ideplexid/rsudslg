<?php 

class LaporanMedicalCheckUp extends DBResponder{
    public function excel() {
        $this->dbtable->setShowAll(true);
        $data = $this->dbtable->view("",0);
        $fix  = $data['data'];
        
        require_once ("smis-libs-out/php-excel/PHPExcel.php");
        $file = new PHPExcel ();
        
        $sheet = $file->getActiveSheet ();
        $i = 1;
        
        $objDrawing = new PHPExcel_Worksheet_Drawing();
        $objDrawing ->setName('logo_img');
        $objDrawing ->setDescription('logo_img');
        $objDrawing ->setPath("medical_checkup/resource/img/kediri.jpeg");
        $objDrawing ->setCoordinates('A'.$i);
        $objDrawing ->setOffsetX(20); 
        $objDrawing ->setOffsetY(5);
        $objDrawing ->setWidth(100); 
        $objDrawing ->setHeight(100);
        $objDrawing ->setWorksheet($sheet);
        $i++;
        
        $sheet->setCellValue("C".$i, getSettings($db,"smis_autonomous_title"));
        $sheet->getStyle("C".$i)->getFont()->setBold(true)->setSize(14);
        $i++;
        $sheet->setCellValue("C".$i, getSettings($db,"smis_autonomous_address"));
        $sheet->getStyle("C".$i)->getFont()->setBold(true)->setSize(14);
        $i++;
        $sheet->setCellValue("C".$i, getSettings($db,"smis_autonomous_contact"));
        $sheet->getStyle("C".$i)->getFont()->setBold(true)->setSize(14);
        $i += 3;
        $sheet->mergeCells("A".$i.":L".$i)->setCellValue("A".$i,"Laporan Layanan Medical Check up");
        $sheet->getStyle("A".$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle("A".$i)->getFont()->setBold(true)->setSize(14);
        $i++;
        $sheet->mergeCells("A".$i.":L".$i)->setCellValue("A".$i, strtoupper(ArrayAdapter::dateFormat("date d M Y", $_POST['dari']))." - ".strtoupper(ArrayAdapter::dateFormat("date d M Y", $_POST['sampai'])));
        $sheet->getStyle("A".$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle("A".$i)->getFont()->setBold(true);
        $i += 3;
        
        $border_start = $i;
        $sheet->setCellValue("A".$i, "No.");
        $sheet->setCellValue("B".$i, "Tanggal");
        $sheet->setCellValue("C".$i, "Nama");
        $sheet->setCellValue("D".$i, "NRM");
        $sheet->setCellValue("E".$i, "No. Reg");
        $sheet->setCellValue("F".$i, "Kelas");
        $sheet->setCellValue("G".$i, "Cara Bayar");
        $sheet->setCellValue("H".$i, "Pengirim");
        $sheet->setCellValue("I".$i, "Dr. Konsul");
        $sheet->setCellValue("J".$i, "Dr. Periksa");
        $sheet->setCellValue("K".$i, "Nama");
        $sheet->setCellValue("L".$i, "Biaya");
        $sheet->getStyle("A".$i.":L".$i)->getFont()->setBold(true);
        $i++;
        
        $no=1;
        foreach($fix as $f) {
            $sheet->setCellValue("A".$i, $no.".");
            $sheet->setCellValue("B".$i, ArrayAdapter::format("date d M Y",$f->tanggal));
            $sheet->setCellValue("C".$i, $f->nama_pasien);
            $sheet->setCellValue("D".$i, $f->nrm_pasien);
            $sheet->setCellValue("E".$i, $f->noreg_pasien);
            $sheet->setCellValue("F".$i, ArrayAdapter::format("date d M Y",$f->kelas));
            $sheet->setCellValue("G".$i, ArrayAdapter::format("unslug",$f->carabayar));
            $sheet->setCellValue("H".$i, $f->pengirim);
            $sheet->setCellValue("I".$i, $f->dokter_konsul);
            $sheet->setCellValue("J".$i, $f->dokter_periksa);
            $sheet->setCellValue("K".$i, $f->nama);
            $sheet->setCellValue("L".$i, $f->total);
            $i++;
            $no++;
        }
        $sheet->setCellValue("B".$i, "TOTAL");
        $sheet->setCellValue("L".$i, "=SUM(L12:L".($i-1).")");
        $sheet->getStyle('L12:L'.$i)->getNumberFormat()->setFormatCode("#.##0,00");

        $i++;
        $border_end = $i - 1;
        
        $sheet->getStyle("A".$border_end.":L".$border_end)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle("A".$border_end.":L".$border_end)->getFont()->setBold(true);


        $thin = array ();
        $thin['borders']=array();
        $thin['borders']['allborders']=array();
        $thin['borders']['allborders']['style']=PHPExcel_Style_Border::BORDER_THIN ;
        $sheet  ->getStyle ( "A".$border_start.":L".$border_end )->applyFromArray ($thin);
        
        $filename = "Laporan Medical Check Up ".strtoupper(ArrayAdapter::dateFormat("date d M Y", $_POST['dari']))." - ".strtoupper(ArrayAdapter::dateFormat("date d M Y", $_POST['sampai'])).".xls";
        
        header ( 'Content-Type: application/vnd.ms-excel' );
        header ( 'Content-Disposition: attachment;filename="'.$filename.'"' );
        header ( 'Cache-Control: max-age=0' );
        $writer = PHPExcel_IOFactory::createWriter ( $file, 'Excel5' );
        $writer->save ( 'php://output' );
		return;
    }
}