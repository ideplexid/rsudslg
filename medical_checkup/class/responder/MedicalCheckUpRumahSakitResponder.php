<?php 

class MedicalCheckUpRumahSakitResponder extends DBResponder{    
    private $post_to_array;    
    public function save(){
        $result=parent::save();
        $this->pushPaket($result);
        return $result;
    }
    
    public function postToArray(){
        if($this->post_to_array==null){
            $this->post_to_array=parent::postToArray();
        }
        return $this->post_to_array;
    }
    
    private function pushPaket($result){
        $data           = $this->postToArray();
        $data['id']     = $result["id"];
        $data['no_lab'] = "PKT-".$result["id"];
        $data['ruangan']="medical_checkup";
        $entity=array();
        if($data['laboratory']!==""){
            $entity[]="laboratory";
        }
        
        if($data['radiology']!==""){
            $entity[]="radiology";
        }
        
        if($data['fisiotherapy']!==""){
            $entity[]="fisiotherapy";
        }
        
        if($data['elektromedis']!==""){
            $entity[]="elektromedis";
        }
        
        require_once "smis-base/smis-include-service-consumer.php";
        $serv=new ServiceConsumer($this->getDBTable()->get_db(),"push_paket",$data);
        $serv->setEntity($entity);
        $serv->execute();
    }
}