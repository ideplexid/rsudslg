<?php 

class MedicalCheckUpResponder extends DBResponder{
    
    private $post_to_array;
    public function command($command){
		if($command=="cek_tutup_tagihan"){
			$pack=new ResponsePackage();
            $result = $this->cek_tutup_tagihan();
            if($result=="1"){
                $pack->setAlertVisible(true);
			    $pack->setAlertContent("Peringatan", "<strong>Pasien Telah Di Tutup Tagihanya Oleh Kasir untuk proses pembayaran</strong>, </br>silakan menghubungi kasir dahulu bila akan menambah, menubah, mengurangi dan menghapus tagihan pasien",Alert::$DANGER);
            }

			$pack->setContent($result);
			$pack->setStatus(ResponsePackage::$STATUS_OK);
			return $pack->getPackage();
		}else{
			return parent::command($command);
		}
    }
    
    public function delete(){
        $id['id']				= $_POST['id'];
        $one = $this->dbtable->select($id);
        $_POST['noreg_pasien'] = $one->noreg_pasien;
        $cek = $this->cek_tutup_tagihan();

        
        if($cek=="1"){
            $success['success']	= -1;
            return $success;
        }

        return parent::delete();
	}

    public function save(){
        $result=parent::save();
        $this->pushPaket($result);
        return $result;
    }
    
    public function postToArray(){
        if($this->post_to_array==null){
            $this->post_to_array=parent::postToArray();
        }
        return $this->post_to_array;
    }
    
    private function pushPaket($result){
        $data           = $this->postToArray();
        $data['id']     = $result["id"];
        $data['no_lab'] = "PKT-".$result["id"];
        $data['ruangan']="medical_checkup";
        $pemeriksaan_lab = json_decode($data['laboratory']);
        $pemeriksaan_rad = json_decode($data['radiology']);
        $pemeriksaan_fst = json_decode($data['fisiotherapy']);
        $pemeriksaan_emd = json_decode($data['elektromedis']);
        $entity=array();
        /*if($data['laboratory']!==""){
            $entity[]="laboratory";
        }
        
        if($data['radiology']!==""){
            $entity[]="radiology";
        }
        
        if($data['fisiotherapy']!==""){
            $entity[]="fisiotherapy";
        }
        
        if($data['elektromedis']!==""){
            $entity[]="elektromedis";
        }*/
        
        if(sizeof($pemeriksaan_lab) != 0){
            $entity[]="laboratory";
        }
        
        if(sizeof($pemeriksaan_rad) != 0){
            $entity[]="radiology";
        }
        
        if(sizeof($pemeriksaan_fst) != 0){
            $entity[]="fisiotherapy";
        }
        
        if(sizeof($pemeriksaan_emd) != 0){
            $entity[]="elektromedis";
        }
        
        require_once "smis-base/smis-include-service-consumer.php";
        $serv=new ServiceConsumer($this->getDBTable()->get_db(),"push_paket",$data);
        $serv->setEntity($entity);
        $serv->execute();
    }

    public function cek_tutup_tagihan(){
        global $db;
        if(getSettings($db,"mcu-aktifkan-tutup-tagihan","0")=="0"){
            return 0;
        }
        require_once "smis-base/smis-include-service-consumer.php";
        $serv = new ServiceConsumer($this->getDBTable()->get_db(),"cek_tutup_tagihan",NULL,"registration");
        $serv ->addData("noreg_pasien",$_POST['noreg_pasien']);
        $serv ->execute();
        $result = $serv ->getContent();
        return $result;
    }
    
}