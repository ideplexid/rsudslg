<?php

require_once 'smis-framework/smis/template/ModulTemplate.php';
require_once ("smis-base/smis-include-service-consumer.php");
require_once "smis-libs-hrd/EmployeeResponder.php";

class MedicalCheckUpRumahSakitTemplate extends ModulTemplate {
    protected $db;
	protected $mode;
	protected $polislug;
	protected $page;
	protected $protoslug;
	protected $protoname;
	protected $protoimplement;
	protected $action;
	protected $dbtable;
	protected $uitable;
    protected $noreg_pasien;
	protected $nama_pasien;
	protected $nrm_pasien;
	protected $list_pesan;
	protected $list_hasil;
	protected $kelas;
	protected $uri;
	protected $jk;
	protected $umur;
	protected $carabayar;
	protected $nyeri_list;
	protected $is_stand_alone;
	protected $is_keep;
	public static $MODE_DAFTAR = "pendaftaran";
	public static $MODE_PERIKSA = "pemeriksaan";
	public static $MODE_ARCHIVE = "archive";
    
    public function __construct($db, $mode, $polislug = "all", $noreg = "", $nrm = "", $nama = "", $page = "medical_checkup", $action = "mcu_rs", $protoslug = "", $protoname = "", $protoimplement = "",$kelas="",$jk="-1") {
        $this->db = $db;
		$this->mode = $mode;
		$this->noreg_pasien = $noreg;
		$this->nama_pasien = $nama;
		$this->nrm_pasien = $nrm;
		$this->polislug = $polislug;
		$this->dbtable = new DBTable ( $db, "smis_mcu_pesanan" );
		$this->is_stand_alone=getSettings($db, "mcu-sistem-model", "Stand Alone")=="Stand Alone";
		$this->is_keep=getSettings($db, "mcu-add-button", "0")=="1";
		$this->page = $page;
		$this->protoslug = $protoslug;
		$this->protoimplement = $protoimplement;
		$this->protoname = $protoname;
		$this->action = $action;
		$this->kelas=$kelas;
		$this->jk=$jk;
		$this->carabayar="No Reg Not Active";
        
        if ($noreg != "") {
			$data_post = array ("command" => "edit","id" => $noreg );
			$service = new ServiceConsumer($this->db, "get_registered", $data_post);
			$service->execute ();
			$data = $service->getContent ();
			$this->carabayar = $data ['carabayar'];
			$this->uri=$data['uri'];
			$this->umur = $data ['umur'];
			if($this->jk=="-1")
				$this->jk=$data['kelamin'];
		}
		
		if ($polislug != "all") {
			$this->dbtable->addCustomKriteria ( "ruangan", "='" . $polislug . "'" );
		}
		
		if ($noreg != "") {
            $this->dbtable->addCustomKriteria ( "noreg_pasien", "='" . $noreg . "'" );
		}
        
        $header=array ('Tanggal','Pasien',"NRM","No. Reg",'Kelas',"Carabayar","Nama","Biaya");
        $name=ucfirst ( $this->mode ) . " Medical Check Up " . ($this->polislug == "all" ? "" : ucfirst ( $this->protoname ));
		$this->uitable = new Table( $header, $name, NULL, true );
		$this->uitable->setPrintButtonEnable(false);
		$this->uitable->setReloadButtonEnable(false);
		$this->uitable->setPrintElementButtonEnable(false);
		$this->uitable->setName ( $action );
        
        if ($this->mode == self::$MODE_DAFTAR) {
			$this->uitable->setDelButtonEnable ( false );
		} 
        /*else if($this->mode == self::$MODE_PERIKSA){
			$this->uitable->setAddButtonEnable ( $this->is_stand_alone || $this->is_keep);
			$btn=new Button("", "", "");
			$btn->setIsButton(Button::$ICONIC);
			$btn->setClass("btn-primary");
			$btn->setIcon(" fa fa-check");
			$this->uitable->addContentButton("selesai", $btn);
			$this->dbtable->addCustomKriteria ( "selesai", "='0'" );
		} 
        else {
			$btn=new Button("", "", "");
			$btn->setIsButton(Button::$ICONIC);
			$btn->setClass("btn-primary");
			$btn->setIcon(" fa fa-eye");
			$this->uitable->addContentButton("edit", $btn);
			$this->uitable->setEditButtonEnable ( false);
			$this->uitable->setAddButtonEnable ( false);
			$this->uitable->setDelButtonEnable ( false);
			$this->dbtable->addCustomKriteria ( "selesai", "='1'" );
		}*/
    }
    
    public function command($command) {
        require_once "medical_checkup/class/responder/MedicalCheckUpRumahSakitResponder.php";
		$adapter = new SimpleAdapter ();
		$adapter->add ( "Tanggal", "tanggal", "date d M Y" );
		$adapter->add ( "Pasien", "nama_pasien" );
		$adapter->add ( "NRM", "nrm_pasien", "digit8" );
		$adapter->add ( "No. Reg", "noreg_pasien", "digit8" );
		$adapter->add ( "Biaya", "total", "money Rp." );
		$adapter->add ( "Kelas", "kelas","unslug" );
        $adapter->add ( "Nama", "nama" );
		$adapter->add ( "Carabayar", "carabayar","unslug" );
		$dbres = new MedicalCheckUpRumahSakitResponder( $this->dbtable, $this->uitable, $adapter );
		$data = $dbres->command ( $_POST ['command'] );
		echo json_encode ( $data );
		return;
	}
    
    public function phpPreLoad() {
        $this->uitable->addModal ( "id", "hidden", "", "" );
		$this->uitable->addModal ( "tanggal", "datetime", "Tanggal",date("Y-m-d H:i:s"),"n",NULL,false,null,false,"umur");
        $this->uitable->addModal ( "uri", "hidden", "", $this->uri );
		if ($this->mode == self::$MODE_DAFTAR) {
			$this->uitable->addModal ( "noreg_pasien", "text", "No Reg", $this->noreg_pasien, "n", null, true,null,false,"nama_pasien" );
			if ($this->noreg_pasien != "")
				$this->uitable->addModal ( "nama_pasien", "text", "Pasien", $this->nama_pasien, "n", null, true,null,false );
			else
				$this->uitable->addModal ( "nama_pasien", "chooser-" . $this->action . "-mcu_pasien-Pasien", "Pasien", $this->nama_pasien, "n", null, true ,null,false);
			$this->uitable->addModal ( "nrm_pasien", "text", "NRM", $this->nrm_pasien, "n", null, true ,null,false,"kelas");
			
		} else if($this->mode == self::$MODE_PERIKSA){
			//if($this->is_stand_alone || $this->is_keep){
				$this->uitable->addModal ( "noreg_pasien", "text", "No Reg", $this->noreg_pasien, "n", null, false,null,false,"nama_pasien");
				$this->uitable->addModal ( "nama_pasien", "chooser-" . $this->action . "-mcu_pasien-Pasien", "Pasien", $this->nama_pasien, "n", null, false,null,false);
				$this->uitable->addModal ( "nrm_pasien", "text", "NRM", $this->nrm_pasien, "n", null, false,null,false,"kelas");
			//}else{
			//	$this->uitable->addModal ( "noreg_pasien", "text", "No Reg", $this->noreg_pasien, "n", null, !$this->is_stand_alone, null,false,"nama_pasien");
			//	$this->uitable->addModal ( "nama_pasien", "text", "Pasien", $this->nama_pasien, "n", null, !$this->is_stand_alone,null,false);
			//	$this->uitable->addModal ( "nrm_pasien", "text", "NRM", $this->nrm_pasien, "n", null, !$this->is_stand_alone,null,false,"kelas");
			//}
		}else{
			$this->uitable->addModal ( "noreg_pasien", "text", "No Reg", $this->noreg_pasien, "n", null, !$this->is_stand_alone );
			$this->uitable->addModal ( "nama_pasien", "text", "Pasien", $this->nama_pasien, "n", null, !$this->is_stand_alone);
			$this->uitable->addModal ( "nrm_pasien", "text", "NRM", $this->nrm_pasien, "n", null, !$this->is_stand_alone);
		}	
        $this->uitable->addModal ( "ibu", "text", "Ibu",$this->ibu);
        $this->uitable->addModal ( "alamat", "text", "Alamat",$this->alamat);
        
		$jkselect=new OptionBuilder();
		$jkselect->add("L","0",$this->jk=="0")->add("P","1",$this->jk=="1");
		if(getSettings($this->db, "mcu-show-gender", "0")=="1"){
			$this->uitable->addModal ( "jk", "select", "L/P", $jkselect->getContent(),"n",NULL,false,null,false,"umur");
		}else{
			$this->uitable->addModal ( "jk",  "hidden", "", $this->jk,"y",null,false,null,false,"umur");
		}
        
		if(getSettings($this->db, "mcu-show-aged", "0")=="1") {
			$this->uitable->addModal ( "umur", "text", "Umur", $this->umur,"y",null,false,null,false,"carabayar");
		}else{
			$this->uitable->addModal ( "umur",  "hidden", "", $this->umur,"y",null,false,null,false,"carabayar");
		}
        
		if($this->is_stand_alone){
			$service = new ServiceConsumer ( $this->db, "get_jenis_patient",NULL,"registration" );
			$service->execute ();
			$jenis_pasien = $service->getContent ();
			$cbayar=new OptionBuilder();
			foreach($jenis_pasien as $jp){
				$cbayar->add($jp['name'],$jp['value'],$jp['value']==$this->carabayar?"1":"0");
			}
			$this->uitable->addModal ( "carabayar", "select", "Jenis Pasien", $cbayar->getContent(), "y", null, false,null,false,"shift" );
		}else{
			$this->uitable->addModal ( "carabayar", "text", "Jenis Pasien", $this->carabayar, "y", null, false ,null,false,"shift" );
		}
        $this->uitable->addModal ( "nama", "chooser-".$this->action."-mcu_paket-Paket MCU", "Paket", "", "n", null, true ,null,false,"id_dokter");
        $this->uitable->addModal ( "kelas", "text", "Kelas", "", "n", null, true ,null,false,"id_dokter");
        $this->uitable->addModal ( "marketing", "chooser-".$this->action."-marketing-Marketing", "Marketing", "", "n", null, true ,null,false);
        $this->uitable->addModal ( "id_marketing","hidden","","");
        $this->uitable->addModal ( "pengirim", "chooser-".$this->action."-pengirim-Pengirim", "Pengirim", "", "n", null, true ,null,false);
        $this->uitable->addModal ( "id_pengirim","hidden","","");
        $this->uitable->addModal ( "dokter_konsul", "chooser-".$this->action."-dokter_konsul-Dokter Konsul", "Dokter Konsul", "", "n", null, true ,null,false);
        $this->uitable->addModal ( "id_dokter_konsul","hidden","","");
        $this->uitable->addModal ( "bagi_hasil_dokter_konsul","hidden","","");
        $this->uitable->addModal ( "dokter_periksa", "chooser-".$this->action."-dokter_periksa-Dokter Periksa", "Dokter Periksa", "", "n", null, true ,null,false);
        $this->uitable->addModal ( "id_dokter_periksa","hidden","","");
        $this->uitable->addModal ( "bagi_hasil_dokter_periksa","hidden","","");
        $this->uitable->addModal ( "total", "money", "Biaya", "0", "n", null, true ,null,false,"");
        
        $this->uitable->addModal ( "laboratory", "hidden", "", "", "n", null, true ,null,false,"");
        $this->uitable->addModal ( "radiology", "hidden", "", "", "n", null, true ,null,false,"");
        $this->uitable->addModal ( "fisiotherapy", "hidden", "", "", "n", null, true ,null,false,"");
        $this->uitable->addModal ( "elektromedis", "hidden", "", "", "n", null, true ,null,false,"");
        $this->uitable->addModal ( "tarif_laboratory", "hidden", "", "", "n", null, true ,null,false,"");
        $this->uitable->addModal ( "tarif_radiology", "hidden", "", "", "n", null, true ,null,false,"");
        $this->uitable->addModal ( "tarif_fisiotherapy", "hidden", "", "", "n", null, true ,null,false,"");
        $this->uitable->addModal ( "tarif_elektromedis", "hidden", "", "", "n", null, true ,null,false,"");
        $this->uitable->addModal ( "total_laboratory", "hidden", "", "", "n", null, true ,null,false,"");
        $this->uitable->addModal ( "total_radiology", "hidden", "", "", "n", null, true ,null,false,"");
        $this->uitable->addModal ( "total_fisiotherapy", "hidden", "", "", "n", null, true ,null,false,"");
        $this->uitable->addModal ( "total_elektromedis", "hidden", "", "", "n", null, true ,null,false,"");
		
		$modal = $this->uitable->getModal ();
		$modal->setTitle ( "Pesanan" );
        echo $this->uitable->getHtml ();
		echo $modal->getHtml ();
        
        $mcu_mode           = new Hidden("mcu_mode","",$this->mode);
        $mcu_noreg_pasien   = new Hidden("mcu_noreg_pasien","",$this->noreg_pasien);
        $mcu_nama_pasien    = new Hidden("mcu_nama_pasien","",$this->nama_pasien);
        $mcu_nrm_pasien     = new Hidden("mcu_nrm_pasien","",$this->nrm_pasien);        
        $mcu_polislug       = new Hidden("mcu_polislug","",$this->polislug);
        $mcu_protoslug      = new Hidden("mcu_protoslug","",$this->protoslug);
        $mcu_protoname      = new Hidden("mcu_protoname","",$this->protoname);
        $mcu_protoimplement = new Hidden("mcu_protoimplement","",$this->protoimplement);
        $mcu_action         = new Hidden("mcu_action","",$this->action);
        $mcu_page           = new Hidden("mcu_page","",$this->page);
        $mcu_mode           = new Hidden("mcu_mode","",$this->mode);
        
		echo $mcu_mode->getHtml();
		if ($this->mode != self::$MODE_DAFTAR){
			echo $mcu_noreg_pasien->getHtml();
			echo $mcu_nama_pasien->getHtml();
			echo $mcu_nrm_pasien->getHtml();	
		}
        echo $mcu_polislug->getHtml();
        echo $mcu_protoslug->getHtml();
        echo $mcu_protoname->getHtml();
        echo $mcu_protoimplement->getHtml();
        echo $mcu_action->getHtml();
        echo $mcu_page->getHtml();
        echo $mcu_mode->getHtml();
        
        echo addJS ( "framework/bootstrap/js/bootstrap-datetimepicker.js" );
		echo addJS ( "framework/smis/js/table_action.js" );
        echo addJS ( "medical_checkup/resource/js/medical_checkup.js",false );
        echo addCSS ( "framework/bootstrap/css/bootstrap-datetimepicker.css" );
    }
    
    public function superCommand($super_command) {
        global $db;
		/* PASIEN */
        $super = new SuperCommand ();
        if($super_command=="mcu_pasien"){
            $ptable = new Table ( array ('Nama','NRM',"No Reg"), "", NULL, true );
            $ptable->setName ( "mcu_pasien" );
            $ptable->setModel ( Table::$SELECT );
            $padapter = new SimpleAdapter ();
            $padapter->add ( "Nama", "nama_pasien" );
            $padapter->add ( "NRM", "nrm", "digit8" );
            $padapter->add ( "No Reg", "id" );
            $presponder = new ServiceResponder ( $this->db, $ptable, $padapter, "get_registered","registration" );
            $super->addResponder ( "mcu_pasien", $presponder );                
        }else if($super_command=="mcu_paket"){
            $ptable = new Table ( array ('Nama','Tarif',"Kelas","Keterangan"), "", NULL, true );
            $ptable->setName ( "mcu_paket" );
            $ptable->setModel ( Table::$SELECT );
            $padapter = new SimpleAdapter ();
            $padapter->add ( "Nama", "nama" );
            $padapter->add ( "Tarif", "total", "money Rp." );
            $padapter->add ( "Kelas", "kelas","unslug" );
            $padapter->add ( "Keterangan", "keteragan","unslug" );
            $presponder = new ServiceResponder ( $this->db, $ptable, $padapter, "get_paket","manajemen" );     
            $super->addResponder ( "mcu_paket", $presponder ); 
        }else if($super_command=="marketing"){
            $marketing_table = new Table (array('nama', 'Jabatan', 'NIP'), "", NULL, true);
            $marketing_table->setName("marketing");
            $marketing_table->setModel(Table::$SELECT);
            $marketing_adapter = new SimpleAdapter();
            $marketing_adapter->add("Nama", "nama");
            $marketing_adapter->add("Jabatan", "nama_jabatan");
            $marketing_adapter->add("NIP", "nip");
            $marketing_dbresponder = new EmployeeResponder($this->db, $marketing_table, $marketing_adapter, "marketing");
            $super->addResponder ( "marketing", $marketing_dbresponder );
        }else if($super_command=="pengirim"){
            $pengirim_table = new Table(array("Nama", "Jabatan", "NIP"));
            $pengirim_table->setName("pengirim");
            $pengirim_table->setModel(Table::$SELECT);
            $pengirim_adapter = new SimpleAdapter();
            $pengirim_adapter->add("Nama", "nama");
            $pengirim_adapter->add("Jabatan", "nama_jabatan");
            $pengirim_adapter->add("NIP", "nip");
            $pengirim_dbresponder = new EmployeeResponder($this->db, $pengirim_table, $pengirim_adapter, "");
            $pengirim_dbresponder->setJenisPegawai(EmployeeResponder::$PEGAWAI_NON_ORGANIK);
            $super->addResponder ( "pengirim", $pengirim_dbresponder );
        }else if($super_command=="dokter_konsul"){
            $pengirim_table = new Table(array("Nama", "Jabatan", "NIP"));
            $pengirim_table->setName("dokter_konsul");
            $pengirim_table->setModel(Table::$SELECT);
            $pengirim_adapter = new SimpleAdapter();
            $pengirim_adapter->add("Nama", "nama");
            $pengirim_adapter->add("Jabatan", "nama_jabatan");
            $pengirim_adapter->add("NIP", "nip");
            $pengirim_dbresponder = new EmployeeResponder($this->db, $pengirim_table, $pengirim_adapter, "");
            $super->addResponder ( "dokter_konsul", $pengirim_dbresponder );
        }else if($super_command=="dokter_periksa"){
            $pengirim_table = new Table(array("Nama", "Jabatan", "NIP"));
            $pengirim_table->setName("dokter_periksa");
            $pengirim_table->setModel(Table::$SELECT);
            $pengirim_adapter = new SimpleAdapter();
            $pengirim_adapter->add("Nama", "nama");
            $pengirim_adapter->add("Jabatan", "nama_jabatan");
            $pengirim_adapter->add("NIP", "nip");
            $pengirim_dbresponder = new EmployeeResponder($this->db, $pengirim_table, $pengirim_adapter, "");
            $super->addResponder ( "dokter_periksa", $pengirim_dbresponder );
        }
        
		$init = $super->initialize ();
		if ($init != null) {
			echo $init;
			return;
		}
	}
}

?>