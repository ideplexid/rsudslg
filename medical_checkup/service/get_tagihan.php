<?php

global $db;

if (isset($_POST['noreg_pasien'])) {
    $noreg = $_POST['noreg_pasien'];	
	$dbtable = new DBTable ($db, "smis_mcu_pesanan");
	$dbtable->addCustomKriteria ( "noreg_pasien", "='" . $noreg . "'" );
	$dbtable->setShowAll ( true );
	$data = $dbtable->view ( "", "0" );
	$rows = $data ['data'];
	$result = array();
    
    foreach ( $rows as $d ) {
        $result[] = array(
			'id' => $d->id,
			'nama' => $d->nama." - ".$d->dokter_periksa,	
			'waktu' => ArrayAdapter::format("date d M Y", $d->tanggal),
			'ruangan' => $d->ruangan,		
			'start' =>$d->tanggal,
			'end' => $d->tanggal,
			'biaya' => $d->total,
			'jumlah' => 1,
			'keterangan' => $d->nama." a.n. ".$d->nama_pasien." Noreg: ".$d->noreg_pasien,
		);
    }
    
    echo json_encode (array(
		'selesai' => "1",
		'exist' => "1",
		'reverse' => "0",
		'cara_keluar' => "Selesai",
		'jasa_pelayanan' => "1",
		'data' => array( 
			'medical_checkup' => array(
				'result' => $result,
				'jasa_pelayanan' => '1'
			)
		)
	));
}

?>
