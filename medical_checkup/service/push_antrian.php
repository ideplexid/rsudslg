<?php
global $db;
require_once 'smis-base/smis-include-service-consumer.php';

$data ['tanggal'] = $_POST ['waktu'];
$data ['nama_pasien'] = $_POST ['nama_pasien'];
$data ['nrm_pasien'] = $_POST ['nrm_pasien'];
$data ['noreg_pasien'] = $_POST ['no_register'];

$detail = json_decode($_POST['detail'], true);
$data ['alamat'] = $detail['alamat'];
$data ['ibu'] = $detail['ibu'];

//mendapatkan uri pasien saat ini:
$params = array();
$params['noreg_pasien'] = $_POST['no_register'];
$service = new ServiceConsumer($db, "get_uri", $params, "registration");
$service->setMode(ServiceConsumer::$CLEAN_BOTH);
$content = $service->execute()->getContent();
$data ['uri'] = $content[0];
$data ['ruangan'] = "pendaftaran";
$data ['jk'] = $_POST ['jk'];
$data ['umur'] = $_POST ['umur'];
$dbtable = new DBTable ( $db, "smis_mcu_pesanan" );
$dbtable->insert ( $data );

$nomor = $dbtable->count ( "" );
$success ['id'] = $dbtable->get_inserted_id ();
$success ['success'] = 1;
$success ['type'] = "insert";
$success ['nomor'] = $nomor;
$content = $success;
$response = new ResponsePackage ();
$response->setContent ( $content );
$response->setStatus ( ResponsePackage::$STATUS_OK );
$response->setAlertVisible ( true );
$response->setAlertContent ( "Data Saved", "Your Data Had Been Saved", ResponsePackage::$TIPE_INFO );
echo json_encode ( $response->getPackage () );
?>