<?php
require_once "smis-libs-hrd/EmployeeResponder.php";
$header = array ();
$header [] = "Tanggal";
$header [] = "Pasien";
$header [] = "NRM";
$header [] = "No Reg";
$header [] = "Kelas";
$header [] = "Cara Bayar";
$header [] = "Nama";
$header [] = "Pengirim";
$header [] = "Dr. Konsul";
$header [] = "Dr. Periksa";
$header [] = "Biaya";
$uitable = new Table ( $header, "", NULL, false );
$uitable->setName ( "laporan" );

if(isset($_POST['super_command']) && $_POST['super_command']!=""){
    $super = new SuperCommand ();
    if($_POST['super_command']=="pengirim"){
        $pengirim_table = new Table(array("Nama", "Jabatan", "NIP"));
        $pengirim_table->setName("pengirim");
        $pengirim_table->setModel(Table::$SELECT);
        $pengirim_adapter = new SimpleAdapter();
        $pengirim_adapter->add("Nama", "nama");
        $pengirim_adapter->add("Jabatan", "nama_jabatan");
        $pengirim_adapter->add("NIP", "nip");
        $pengirim_dbresponder = new EmployeeResponder($db, $pengirim_table, $pengirim_adapter, "");
        $pengirim_dbresponder->setJenisPegawai(EmployeeResponder::$PEGAWAI_NON_ORGANIK);
        $super->addResponder ( "pengirim", $pengirim_dbresponder );
    }else if($_POST['super_command']=="dokter_konsul"){
        $pengirim_table = new Table(array("Nama", "Jabatan", "NIP"));
        $pengirim_table->setName("dokter_konsul");
        $pengirim_table->setModel(Table::$SELECT);
        $pengirim_adapter = new SimpleAdapter();
        $pengirim_adapter->add("Nama", "nama");
        $pengirim_adapter->add("Jabatan", "nama_jabatan");
        $pengirim_adapter->add("NIP", "nip");
        $pengirim_dbresponder = new EmployeeResponder($db, $pengirim_table, $pengirim_adapter, "");
        $super->addResponder ( "dokter_konsul", $pengirim_dbresponder );
    }else if($_POST['super_command']=="dokter_periksa"){
        $pengirim_table = new Table(array("Nama", "Jabatan", "NIP"));
        $pengirim_table->setName("dokter_periksa");
        $pengirim_table->setModel(Table::$SELECT);
        $pengirim_adapter = new SimpleAdapter();
        $pengirim_adapter->add("Nama", "nama");
        $pengirim_adapter->add("Jabatan", "nama_jabatan");
        $pengirim_adapter->add("NIP", "nip");
        $pengirim_dbresponder = new EmployeeResponder($db, $pengirim_table, $pengirim_adapter, "");
        $super->addResponder ( "dokter_periksa", $pengirim_dbresponder );
    }
    
    $init = $super->initialize ();
    if ($init != null) {
        echo $init;
        return;
    }
}


if (isset ( $_POST ['command'] )) {
	require_once "medical_checkup/class/responder/LaporanMedicalCheckUp.php";
  
    $adapter = new SimpleAdapter();
    $adapter->add ( "Tanggal", "tanggal", "date d M Y" );
    $adapter->add ( "Pasien", "nama_pasien" );
    $adapter->add ( "NRM", "nrm_pasien", "only-digit6" );
    $adapter->add ( "No Reg", "noreg_pasien", "only-digit6" );
    $adapter->add ( "Kelas", "kelas", "unslug" );
    $adapter->add ( "Cara Bayar", "carabayar", "unslug" );
    $adapter->add ( "Biaya", "total","money Rp." );
    $adapter->add ( "Pengirim", "pengirim");
    $adapter->add ( "Dr. Konsul", "dokter_konsul");
    $adapter->add ( "Dr. Periksa", "dokter_periksa");
    $adapter->add ( "Nama", "nama" );
	
	$dbtable = new DBTable ( $db, "smis_mcu_pesanan" );
	if (isset ( $_POST ['dari'] ) && isset ( $_POST ['sampai'] ) && $_POST ['dari'] != "" && $_POST ['sampai'] != "") {
		$dari   = $_POST ['dari'];
		$sampai = $_POST ['sampai'];
		$dbtable->addCustomKriteria ( "'" . $dari . "'", "<=tanggal" );
		$dbtable->addCustomKriteria ( "'" . $sampai . "'", ">=tanggal" );
    }

    if(isset($_POST['pengirim']) && $_POST['pengirim']!=""){
        $dbtable->addCustomKriteria(" id_pengirim "," = '".$_POST['id_pengirim']."' ");
    }
    if(isset($_POST['dokter_konsul']) && $_POST['dokter_konsul']!=""){
        $dbtable->addCustomKriteria(" id_dokter_konsul "," = '".$_POST['id_dokter_konsul']."' ");
    }
    if(isset($_POST['dokter_periksa']) && $_POST['dokter_periksa']!=""){
        $dbtable->addCustomKriteria(" id_dokter_periksa "," = '".$_POST['id_dokter_periksa']."' ");
    }


    $carabayar = $_POST ['carabayar'];
    if ($carabayar != "")
			$dbtable->addCustomKriteria ( " carabayar ", " ='".$carabayar."'  " );
	
	$dbtable->setOrder ( " tanggal ASC " );
	$dbres = new LaporanMedicalCheckUp ( $dbtable, $uitable, $adapter );
	if($dbres->isExcel()){
        $dbtable->setShowAll(true);
	}
	
	$data = $dbres->command ( $_POST ['command'] );
	if($data!=null)
		echo json_encode ( $data );
	return;
}


require_once "smis-base/smis-include-service-consumer.php";
$ser=new ServiceConsumer($db,"get_jenis_patient",NULL,"registration");
$ser->execute();
$ctx=$ser->getContent();
$ctx[]=array("name"=>"-- Kosong --","value"=>"");


$uitable->addModal ( "dari", "date", "Dari", "" );
$uitable->addModal ( "sampai", "date", "Sampai", "" );
$uitable->addModal ( "carabayar", "select", "Jenis Pasien", $ctx );
$uitable->addModal ( "pengirim", "chooser-laporan-pengirim-Pengirim", "Pengirim", "" );
$uitable->addModal ( "dokter_konsul", "chooser-laporan-dokter_konsul-Dr. Konsul", "Dr. Konsul", "" );
$uitable->addModal ( "dokter_periksa", "chooser-laporan-dokter_periksa-Dr. Periksa", "Dr. Periksa", "" );
$uitable->addModal ( "id_pengirim", "hidden", "", "" );
$uitable->addModal ( "id_dokter_konsul", "hidden", "", "" );
$uitable->addModal ( "id_dokter_periksa", "hidden", "", "" );

$modal = $uitable->getModal ();
$form = $modal->getForm ();
$form->setTitle("Laporan Medical Check Up");

$btngroup=new ButtonGroup("");
$button = new Button ( "", "", "Reload" );
$button->setClass("btn-primary");
$button->setIsButton ( Button::$ICONIC_TEXT );
$button->setIcon ( "fa fa-refresh" );
$button->setAction ( "laporan.view()" );
$btngroup->addElement($button);

$excel = new Button ( "", "", "Download" );
$excel->setClass("btn-primary");
$excel->setIsButton ( Button::$ICONIC_TEXT );
$excel->setIcon ( "fa fa-download" );
$excel->setAction ( "laporan.excel()" );
$btngroup->addElement($excel);

$form->addElement ( "", $btngroup);
echo $form->getHtml ();


echo $uitable->getHtml ();
echo addJS ( "framework/smis/js/table_action.js" );
echo addJS ( "framework/bootstrap/js/bootstrap-datepicker.js" );
echo addCSS ( "framework/bootstrap/css/datepicker.css" );
echo addJS ( "medical_checkup/resource/js/laporan.js",false );
?>
