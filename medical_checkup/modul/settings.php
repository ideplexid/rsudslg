<?php
	require_once 'smis-framework/smis/api/SettingsBuilder.php';
	global $db;	
	$smis = new SettingsBuilder ( $db, "mcu_settings", "medical_checkup", "settings" );
	$smis->setShowDescription ( true );
	$smis->setTabulatorMode ( Tabulator::$POTRAIT );
	$smis->addTabs ( "mcu", "Medical Checkup Settings" );
	$option=new OptionBuilder();
	$option->addSingle("Stand Alone");
	$option->addSingle("Integrated");
	
	$smis->addItem ( "mcu", new SettingsItem ( $db, "mcu-sistem-model", "Model System", $option->getContent(), "select", "System Model mcu untuk persiapan keseluruhan, ketika Stand Alone berarti sendiri - sendiri, ketika Integrated berarti terintegrasi" ) );
	$smis->addItem ( "mcu", new SettingsItem ( $db, "mcu-ui-model", "Show UI Simple Model", "0", "checkbox", "Tampilkan Model UI Simple" ) );
	$smis->addItem ( "mcu", new SettingsItem ( $db, "mcu-add-button", "Add Button Enabled", "0", "checkbox", "Tetap Tampilkan Tombol Tambah Meskipun Telah Terintegrasi" ) );
	$smis->addItem ( "mcu", new SettingsItem ( $db, "mcu-show-gender", "Tampilkan Gender", "1", "checkbox", "Tampilkan Gender" ) );
    $smis->addItem ( "mcu", new SettingsItem ( $db, "mcu-show-aged", "Tampilkan Umur", "1", "checkbox", "Tampilkan Umur" ) );
    $smis->addItem ( "mcu", new SettingsItem ( $db, "mcu-aktifkan-tutup-tagihan", "Aktifkan Tutup Tagihan", "0", "checkbox", "mengaktifkan tutup tagihan" ) );
	
	$response = $smis->init ();
?>