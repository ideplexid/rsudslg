<?php
global $NAVIGATOR;
// Administrator Top Menu
$mr = new Menu ("fa fa-medkit");
$mr->addProperty ( 'title', 'Medical Chekcup' );
$mr->addProperty ( 'name', 'Medical Chekcup' );
$mr->addSubMenu ( "Medical Chekcup", "medical_checkup", "pemeriksaan", "Medical Chekcup" ,"fa fa-stethoscope");
$mr->addSubMenu ( "Medical Chekcup RS", "medical_checkup", "mcu_rs", "Medical Chekcup" ,"fa fa-stethoscope");
$mr->addSubMenu ( "Settings", "medical_checkup", "settings", "Settings" ,"fa fa-cog");
$mr->addSubMenu ( "Laporan", "medical_checkup", "laporan", "Laporan" ,"fa fa-list-alt");

require_once 'smis-libs-inventory/navigation.php';
$inventory_navigator = new InventoryNavigator($NAVIGATOR, "", "Medical Checkup", "medical_checkup");
$mr = $inventory_navigator->extendMenu($mr);
$NAVIGATOR->addMenu ( $mr, 'medical_checkup' );
?>
