<?php
require_once ("smis-base/smis-include-service-consumer.php");
global $db;
class OksigenService extends ServiceConsumer {
	public function __construct($db, $polislug) {
		parent::__construct ( $db, "get_oksigen", array (
				"ruang" => $polislug 
		) );
	}
	public function getMenit() {
		$oksigen = $this->getContent ();
		return $oksigen ['menit'] * 1;
	}
	public function getJam() {
		$oksigen = $this->getContent ();
		return $oksigen ['jam'] * 1;
	}
	public function getLiter() {
		$oksigen = $this->getContent ();
		return $oksigen ['liter'] * 1;
	}
}

?>