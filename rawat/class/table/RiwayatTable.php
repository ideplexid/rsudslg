<?php 

class RiwayatTable extends Table{
    private $polislug;
    public function setPolislug($p){
        $this->polislug = $p;
        return $this;
    }

    public function getPrintedElement($p,$f){
        global $db;
        $dbtable = new DBTable($db,"smis_rwt_in_log_".$this->polislug);
        $dbtable ->addCustomKriteria("noreg_pasien","='".$p->no_register."'");
        $dbtable ->setShowAll(true);
        $dbtable ->setOrder(" tanggal DESC ",true);
        $list = $dbtable->view("","0");
        $data = $list['data'];

        $tbl = new TablePrint("");
        $tbl->setDefaultBootrapClass(true);
        $tbl->addColumn("Waktu",1,1);
        $tbl->addColumn("Petugas",1,1);
        $tbl->addColumn("Keterangan",1,1);
        $tbl->addColumn("Asal",1,1);
        $tbl->commit("title");
        foreach($data as $x){
            $tbl->addColumn(ArrayAdapter::format("date d M Y H:i:s",$x->tanggal),1,1);
            $tbl->addColumn($x->petugas,1,1);
            $tbl->addColumn($x->keterangan,1,1);
            $tbl->addColumn($x->asal,1,1);
            $tbl->commit("body");
        }

        return $tbl->getHtml();
    }
}