<?php 

class BiayaTable extends Table{
	public function getBodyContent(){
		$content="";
		
		foreach($this->body_before as $h){
			$content.=$h;
		}
		
		if($this->content!=NULL){
			foreach($this->content as $d){
				$content.="<tr>";
					foreach($this->header as $h){
						$value=isset($d[$h])?$d[$h]:"";
						$value=str_replace("IGD"," PERAWAT",$value);
						$content.="<td>".($value)."</td>";
					}
					if($this->is_action){
						$this->current_data=$d;
						$content.="<td class='noprint'>".$this->getContentButton($d['id'])->getHtml()."</td>";
					}
				$content.="</tr>";
			}
		}
		
		foreach($this->body_after as $h){
			$content.=$h;
		}
		
		
		return $content;
	}
}

?>