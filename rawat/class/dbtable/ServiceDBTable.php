<?php

/**
 *  
 * @author Nurul Huda
 * @version 1.0.0
 * @since 6 Pebruari 2018
 * @license LGPL v2
 * @copyright Nurul Huda <goblooge@gmail.com>
 */
class ServiceDBTable extends MySQLTable{
	/*@var Database*/
	protected $fetch;
    protected $service;
    protected $entity;
    
	/**
	 * @param Database $db
	 * @param Table name $name
	 * @param array of column $column
	 */
	public function __construct(string $name,Array $column,string $service,string $entity){
        parent::__construct($name,$column);        
		$this->inserted_id  = -1;
        $this->fetch        = DBTable::$OBJECT_FETCH;
        $this->service      = $service;
        $this->entity       = $entity;
	}
    
    public function setFetchMethode($fetch){
		$this->fetch=$fetch;
		return $this;
	}
    
	public function get_db(){
        global $db;
        return $db;
    }
    
	public function get_default_column($name=null){
		return $this->column;
	}
    
	public function escaped_string($string){
        global $db;
		return $db->escaped_string($string);
	}
	
	public function delete($id){	
        return true;
	}
    
	public function get_inserted_id(){
		return $this->inserted_id;
	}
    
    public function updateByCustomKriteria($update,$kriteria,$warp=null){
       return true;
    }
    
    public function is_exist($id,$include_del=false){
		return true;
	}
    
	public function restore($data,$id){
		return true;
	}
    
    public function executeService($data){
        global $db;
        $serv = new ServiceConsumer($db,$this->service,$data,$this->entity);
        $serv->addData("table",$this->name);
        $serv->execute();
        return $serv->getContent();
    }
    
    public function getNextID(){
		$query  = "SHOW TABLE STATUS LIKE '".$this->name."'";
		$result = $this->get_row($query,false);
        $nextId = $result['Auto_increment'];
		return $nextId;
	}
	
	public function insert($data,$use_prop=true,$warp=null){
        /*TODO user service*/
        $x['command']   = "insert";
		$x['data']      = $data;
        $x['use_prop']  = $use_prop;
        $x['warp']      = $warp;
        $this->inserted_id = $this->executeService($x);
        return true;
	}
	
	public function update($data,$id,$warp=null){
        $x['command']   = "update";
		$x['data']      = $data;
        $x['id']        = $id;
        $x['warp']      = $warp;
        $this->inserted_id = $this->executeService($x);
		return true;
	}
	
    public function view($term,$page){
        $x['command']           = "view";
		$x['term']              = $term;
        $x['page']              = $page;
        $x['custom_kriteria']   = $this->getDataCustomKriteria();
        $x['max']               = $this->getMaximum();
        $result                 = $this->executeService($x);
        if($this->fetch==DBTable::$OBJECT_FETCH){
            $result['data'] = (object) $result['data'];
        }
        return $result;
	}
    
    public function get_row($query){
        $x['command']   = "get_row";
        $x['query']     = $query;
        $result         = $this->executeService($x);
        if($this->fetch==DBTable::$OBJECT_FETCH){
            $result = (object) $result;
        }
		return $result;
	}
	
	public function get_var($query){
        $x['command']   = "get_var";
        $x['query']     = $query;
        $result         = $this->executeService($x);
		return $result;
	}
	
	public function query($query){
        $x['command']       = "query";
		$x['query']         = $query;
        $result             = $this->executeService($x);
		return $result;
	}
	
	public function get_result($query){	
        $x['command']       = "get_result";
        $x['query']         = $query;
        $result             = $this->executeService($x);
		return $result;
	}
    
    public function getLastQueryError(){
		return $this;
    }
}
?>