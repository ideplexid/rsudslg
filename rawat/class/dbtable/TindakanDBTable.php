<?php 

/**
 * 
 * this class used for determining
 * and auto synchronize data automatically  
 * to cashier based on it's current needed
 * 
 * @author      : Nurul Huda
 * @license     : LGPLv3
 * @database    : smis_amb_ambulan
 * @version     : 1.0.1
 * @since       : 09-Mar-2017
 * @copyright   : goblooge@gmail.com
 * 
 * */

class TindakanDBTable extends DBTable{
    private $autosynch;
    private $entity;
    private $jenis_tagihan;
    private $adapter;
    private $is_multi;
    public function __construct($db,$name,$column=NULL){
        parent::__construct($db,$name,$column);
        $this->autosynch=getSettings($db,"cashier-real-time-tagihan","0")!="0";
        $this->entity = "";
        $this->jenis_tagihan="";
        $this->adapter = null;
        $this->is_multi = true;
    }
    /**
     * @brief set whether should auto synchronize or not
     * @param boolean $auto 
     * @return  
     */
    public function setAutoSynch($auto){
        $this->autosynch=$auto;
        return $this;
    }

    public function setMultiple($multi){
        $this->is_multi=$multi;
        return $this;
    }

    public function setEntity($entity){
        $this->entity=$entity;
        return $this;
    }

    public function setAdapter($adapter){
        $this->adapter=$adapter;
        return $this;
    }

    public function setJenis($jenis){
        $this->jenis_tagihan=$jenis;
        return $this;
    }
    
    /**
     * @brief override DBTable->insert();
     */
    public function insert($data,$use_prop=true,$warp=null){
        $result=parent::insert($data,$use_prop,$warp);
        if($this->autosynch){
            $row=$this->select($this->get_inserted_id());
            $this->synchToCashier($row);
        }
		return $result;
	}
    
    /**
     * @brief override DBTable->update();
     */
    public function update($data,$id,$warp=null){
		$result=parent::update($data,$id,$warp);
        if($this->autosynch){
            if(isset($data['prop']) && $data['prop']=="del"){
                $row=$this->select($id,false,false);
                $this->synchToCashier($row,"del");
            }else{
                $row=$this->select($id);
                $this->synchToCashier($row);
            }
            
        }
		return $result;
	}
    
    /**
     * @brief synchronize data through service to cashier
     * @param Array/Object $raw 
     * @param String $prop, if delete then prop should be filled with string 'del' 
     * @return  null
     */
    private function synchToCashier($raw,$prop=""){
        require_once "smis-base/smis-include-service-consumer.php";
        $data=array();
        $data['grup_name']      = $this->jenis_tagihan;
        $data['nama_pasien']    = $raw->nama_pasien;
        $data['noreg_pasien']   = $raw->noreg_pasien;
        $data['nrm_pasien']     = $raw->nrm_pasien;
        $data['entity']         = $this->entity;
        $data['list']           = $this->adapt($raw,$prop);
        $serv = new ServiceConsumer($this->get_db(),"proceed_receivable",$data,"kasir");
        $serv->execute();
        $serv->getContent();
    }
	
    /**
     * @brief this function used to adapt one record in this entity
     *          should become how much record in cashier
     * @param Array/Object $d 
     * @param String $prop , if del, then prop should be filled with string 'del' 
     * @return  Array $result that contains data of one record becoming.
     */
    public function adapt($d,$prop=""){
        /*
        global $db;
        $result=array();
        // biaya layanan lain:
		$layanan_lain_row = $db->get_row("
            SELECT SUM(jumlah * harga_layanan) AS 'biaya_lain'
            FROM smis_lab_dpesanan_lain
            WHERE id_pesanan = '" . $d->id . "' AND prop NOT LIKE 'del'
            GROUP BY id_pesanan
        ");
        $biaya_lain = $layanan_lain_row->biaya_lain != null ? $layanan_lain_row->biaya_lain : 0;        
        $onedata ['waktu'] = ArrayAdapter::format ( "date d M Y", $d->tanggal );
		$onedata ['nama'] = empty($d->no_lab) ? "Laboratorium #$d->id" : "Laboratorium $d->no_lab";
		$onedata ['biaya'] = $d->biaya + $biaya_lain;
		$onedata ['jumlah'] = 1;
		$onedata ['start'] = $d->tanggal;
		$onedata ['end'] = $d->tanggal;
        $onedata ['id'] = $d->id;
        $onedata ['ruangan'] = "laboratory";
        
		$onedata ['keterangan'] = "Laboratory Senilai " . ArrayAdapter::format ( "only-money Rp.", $d->biaya );
		$onedata ['prop']       = $prop;
        $result [] = $onedata;*/
        $result = array();
        if($this->is_multi){
            $content   = array();
            $content[] = $d;
            $list = $this->adapter->getContent($content);
            foreach($list as $x){
                if($prop=="del"){
                    $x['prop'] = $prop;
                }
                $result[] = $x;
            }
        }else{
            $onedata = (array) $this->adapter->adapt($d);
            $onedata['prop'] = $prop;
            $result [] = $onedata;    
        }
        

        return $result;
    }
    
}