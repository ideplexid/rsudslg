<?php 

class AokConnectedResponder extends RawatResponder{
	protected $polislug;
	protected $ruang;
	public function __construct($dbtable, $uitable, $adapter,$polislug){
		parent::__construct($dbtable, $uitable, $adapter,$polislug);
		$this->polislug=$polislug;
		$this->ruang=getSettings($dbtable->get_db(),"smis-rs-stok-obat-boi-" . $polislug,"");
	}
	
	public function command($command){
		if($command=="scan"){
			$pack=new ResponsePackage();
			$content=$this->scan();
			$pack->setContent($content);
			$pack->setStatus(ResponsePackage::$STATUS_OK);
			if($content==null){
				$pack->setAlertVisible(true);
				$pack->setAlertContent("Scan Complete", "Kode Not Found Try Again", ResponsePackage::$TIPE_INFO);			
			}else{
				$pack->setAlertVisible(false);
			}		
			return $pack->getPackage();
		}else{
			return parent::command($command);
		}
	}
	
	public function scan(){
		$this->dbtable->setFetchMethode(DBTable::$ARRAY_FETCH);
		$kriteria['id_obat']=$_POST['id_obat'];
		$obat=$this->dbtable->select($kriteria);
		if($obat!=null){
			$obat['sisa']=$this->getSisaStokObat($_POST['id_obat']);
		}
		return $obat;
	}
	
	public function edit(){
		$this->dbtable->setFetchMethode(DBTable::$ARRAY_FETCH);
		$hasil=parent::edit();
		if($hasil['id_obat']=="0"){
			$hasil['sisa']=0;
		}else{
			$hasil['sisa']=$this->getSisaStokObat($hasil['id_obat']);
		}
		return $hasil;
	}
	
	private function getSisaStokObat($id_obat){
		$serv=new ServiceConsumer($this->getDBTable()->get_db(),"get_item_stok",NULL,$this->ruang);
		$serv->addData("id_obat",$id_obat);
		$serv->addData("ruangan",$this->ruang);
		$serv->execute();
		$content=$serv->getContent();
		$jumlah=$content['jumlah'];
		return $jumlah;
	}
	
	
}

?>