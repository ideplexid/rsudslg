<?php
require_once "rawat/class/responder/RawatResponder.php";
require_once "rawat/class/responder/BedResponder.php";
class BedServiceResponder extends BedResponder{
    public function __construct(DBTable $dbtable){
		parent::__construct($dbtable, null, null,$entity);
	}
    
    public function command($command){	
		if($command=="list"){
			$content=$this->view();			
		}else if($command=='save'){
			$content=$this->save();
		}else if($command=="del"){
			$content=$this->delete();
		}else if($command=="edit"){
			$content=$this->edit();
		}else if($command=="select"){
			$content=$this->select();
		}else if($command=="print-element"){
			$content=$this->printing("print-element");
		}else if($command=="printing"){
			$content=$this->printing();
		}		
		return $content;
	}
    
    public function view(){
		$kriteria=isset($_POST['kriteria'])?$_POST['kriteria']:"";
		$number=(isset($_POST['number']) && $_POST['number']!="")?$_POST['number']:0;
		$max=isset($_POST['max'])?$_POST['max']:"10";
		$this->dbtable->setMaximum($max);
		$d=$this->dbtable->view($kriteria,$number);
		return $d;
	}
    
	public function edit(){
		$id=$_POST['id'];
		$row=$this->dbtable->select($id,false);
		return $row;
	}
	
	public function select(){
		$row=$this->edit();
		return $row;
	}
}

?>