<?php 

class AlokStandAloneResponder extends RawatResponder{
    public function notifyAccounting($id,$operation){
        $slug=str_replace("_","-",$this->acc_jenis_data);
        if(getSettings($this->dbtable->get_db(),"smis-rs-acc-e-".$slug."-".$this->polislug,"1")=="1"){
            $data['jenis_akun'] = "transaction";
            $data['jenis_data'] = "penjualan"; //$this->acc_jenis_data;
            $data['id_data']    = $id;
            $data['entity']     = $this->polislug;
            $data['service']    = $this->acc_service;
            $data['data']       = $id;
            $data['code']       = $this->acc_code."-".strtoupper($this->polislug)."-".$id;
            $data['operation']  = $operation;
            
            $x=$this->dbtable->selectEventDel($id,false);
            $hasil=(array)$x;
            $data['tanggal']    = $hasil[$this->acc_tanggal];
            $data['uraian']     = ucwords($this->acc_jenis_data)." ".$hasil['nama_pasien']." dengan Noreg ".$hasil['noreg_pasien']." : ".$hasil[$this->uraian];
            $data['nilai']      = ($hasil["harga"]+$hasil["hpp"])*$hasil['jumlah'];
            
            require_once "smis-base/smis-include-service-consumer.php";
            $serv=new ServiceConsumer($this->dbtable->get_db(),"push_notify_accounting",$data,"accounting");
            $serv->execute();
        }
        return $this;
    }
}

?>