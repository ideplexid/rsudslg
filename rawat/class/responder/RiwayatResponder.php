<?php 

class RiwayatResponder extends DBResponder{
	private $polislug;
	function __construct(DBTable $dbtable,Table $uitable=NULL,ArrayAdapter $adapter=NULL,$polislug){
		parent::__construct($dbtable,$uitable,$adapter);
		$this->polislug=$polislug;
	}
	
	public function command($command){			
		if($command!="reintegrated"){
			return parent::command($command);
		}else{
			
			$pack=new ResponsePackage();
			$content=NULL;
			$status='not-authorized';	//not-authorized, not-command, fail, success
			$alert=array();
			
			$content=$this->reintegrated();
			$pack->setContent($content);
			$pack->setStatus(ResponsePackage::$STATUS_OK);
			
			return $pack->getPackage();
		}
			
	}
	
	private function reintegrated(){
		$id['id']=$_POST['id'];
		$row=$this->getDBTable()->select($id);
		$nrm_pasien=$row->nrm_pasien;
		
		$ser=new ServiceConsumer($this->getDBTable()->get_db(),"get_last_registered",NULL,"registration");
		$ser->addData("nrm_pasien",$nrm_pasien);
		$ser->execute();
		$pasien=$ser->getContent();
		if($pasien==NULL || !isset($pasien['id'])){
			return "";
		}else{
			$noreg_lama=$row->no_register;
			$noreg_baru=$pasien['id'];
			$carabayar=$pasien['carabayar'];
			$this->updateData($noreg_lama,$noreg_baru,$carabayar);
		}
		return json_encode($row);
	}
	
	private function updateData($noreg_lama,$noreg_baru,$carabayar){
		$db=$this->getDBTable()->get_db();
		$entity=$this->polislug;
		$query=array();
		$query["antrian"]="UPDATE smis_rwt_antrian_".$entity." set carabayar='".$carabayar."',no_register='".$noreg_baru."' WHERE no_register='".$noreg_lama."' AND no_register<0 ;";
		$query["tindakan_perawat"]="UPDATE smis_rwt_tindakan_perawat_".$entity."   set carabayar='".$carabayar."' ,noreg_pasien='".$noreg_baru."' WHERE  noreg_pasien='".$noreg_lama."' AND noreg_pasien<0 ";
		$query["tindakan_perawat_igd"]="UPDATE smis_rwt_tindakan_igd_".$entity."   set carabayar='".$carabayar."' ,noreg_pasien='".$noreg_baru."' WHERE  noreg_pasien='".$noreg_lama."' AND noreg_pasien<0 ";
		$query["tindakan_dokter"]="UPDATE smis_rwt_tindakan_dokter_".$entity."   set carabayar='".$carabayar."' ,noreg_pasien='".$noreg_baru."' WHERE  noreg_pasien='".$noreg_lama."' AND noreg_pasien<0 ";
		$query["recovery_room"]="UPDATE smis_rwt_rr_".$entity."   set carabayar='".$carabayar."' ,noreg_pasien='".$noreg_baru."' WHERE  noreg_pasien='".$noreg_lama."' AND noreg_pasien<0 ";
		$query["periksa"]="UPDATE smis_rwt_konsultasi_dokter_".$entity."   set carabayar='".$carabayar."' ,noreg_pasien='".$noreg_baru."'  WHERE  noreg_pasien='".$noreg_lama."' AND noreg_pasien<0 ";
		$query["alat_obat"]="UPDATE smis_rwt_alok_".$entity."   set carabayar='".$carabayar."' ,noreg_pasien='".$noreg_baru."' WHERE  noreg_pasien='".$noreg_lama."' AND noreg_pasien<0 ";
		$query["operasi"]="UPDATE  smis_rwt_ok_".$entity."   set carabayar='".$carabayar."',noreg_pasien='".$noreg_baru."'  WHERE  noreg_pasien='".$noreg_lama."' AND noreg_pasien<0 ";
		$query["recovery_room_operasi"]="UPDATE  smis_rwt_ok_".$entity."   set carabayar='".$carabayar."',noreg_pasien='".$noreg_baru."'  WHERE  noreg_pasien='".$noreg_lama."' AND noreg_pasien<0 ";
		$query["vk"]="UPDATE smis_rwt_vk_".$entity."   set carabayar='".$carabayar."' ,noreg_pasien='".$noreg_baru."' WHERE  waktu>='$awal' AND waktu<'$akhir' WHERE noreg_pasien='".$noreg_lama."' AND noreg_pasien<0 ";
		$query["bronchoscopy"]="UPDATE  smis_rwt_bronchoscopy_".$entity."   set carabayar='".$carabayar."' ,noreg_pasien='".$noreg_baru."' WHERE  noreg_pasien='".$noreg_lama."' AND noreg_pasien<0 ";
		$query["endoscopy"]="UPDATE  smis_rwt_endoscopy_".$entity."   set carabayar='".$carabayar."',noreg_pasien='".$noreg_baru."'  WHERE  noreg_pasien='".$noreg_lama."' AND noreg_pasien<0 ";
		$query["ekg"]="UPDATE  smis_rwt_ekg_".$entity."   set carabayar='".$carabayar."',noreg_pasien='".$noreg_baru."'  WHERE  noreg_pasien='".$noreg_lama."' AND noreg_pasien<0 ";
		$query["spirometry"]="UPDATE smis_rwt_spirometry_".$entity."   set carabayar='".$carabayar."',noreg_pasien='".$noreg_baru."'  WHERE  noreg_pasien='".$noreg_lama."' AND noreg_pasien<0 ";
		$query["bed"]="UPDATE smis_rwt_bed_".$entity."   set carabayar='".$carabayar."',noreg_pasien='".$noreg_baru."'  WHERE  noreg_pasien='".$noreg_lama."' AND noreg_pasien<0 ";
		$query["audiometry"]="UPDATE  smis_rwt_audiometry_".$entity."   set carabayar='".$carabayar."',noreg_pasien='".$noreg_baru."'  WHERE  noreg_pasien='".$noreg_lama."' AND noreg_pasien<0 ";
		$query["konsul"]="UPDATE smis_rwt_konsul_dokter_".$entity."   set carabayar='".$carabayar."',noreg_pasien='".$noreg_baru."'  WHERE  noreg_pasien='".$noreg_lama."' AND noreg_pasien<0 ";
		$query["visite"]="UPDATE smis_rwt_visite_dokter_".$entity."   set carabayar='".$carabayar."',noreg_pasien='".$noreg_baru."'  WHERE  noreg_pasien='".$noreg_lama."' AND noreg_pasien<0 ";
		$query["oksigen_manual"]="UPDATE  smis_rwt_oksigen_manual_".$entity."   set carabayar='".$carabayar."',noreg_pasien='".$noreg_baru."'  WHERE  noreg_pasien='".$noreg_lama."' AND noreg_pasien<0 ";
		$query["oksigen_central"]="UPDATE smis_rwt_oksigen_central_".$entity."   set carabayar='".$carabayar."',noreg_pasien='".$noreg_baru."'  WHERE  noreg_pasien='".$noreg_lama."'  AND noreg_pasien<0 ";
		$query["faal_paru"]="UPDATE smis_rwt_faal_paru_".$entity."   set carabayar='".$carabayar."',noreg_pasien='".$noreg_baru."'  WHERE  noreg_pasien='".$noreg_lama."' AND noreg_pasien<0 ";

		foreach($query as $q){
			$db->query($q);	
		}

		
	}
	
}

?>