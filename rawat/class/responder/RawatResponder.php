<?php
require_once "smis-base/smis-include-duplicate.php";
class RawatResponder extends DuplicateResponder{
	protected $polislug;
    
    protected $acc_service;
    protected $acc_code;
    protected $acc_jenis_data;
    protected $acc_tanggal;
    protected $nilai;
    protected $uraian;
    
    
	public function __construct($dbtable, $uitable, $adapter,$polislug){
		parent::__construct($dbtable, $uitable, $adapter);
		$this->polislug=$polislug;
        $this->setDuplicate(false,"");
        $this->setAutonomous(getSettings($dbtable->get_db(),"smis_autonomous_id",""));
	}

    public function command($command){
        if($command=="cek_tutup_tagihan"){
            $pack=new ResponsePackage();
            require_once "rawat/function/tutup_tagihan.php";
            $result = tutup_tagihan($_POST['noreg_pasien'],$this->polislug);            
            if($result=="1"){
                $pack->setAlertVisible(true);
			    $pack->setAlertContent("Peringatan", "<strong>Pasien Telah Di Tutup Tagihanya Oleh Kasir untuk proses pembayaran</strong>, </br>silakan menghubungi kasir dahulu bila akan menambah, menubah, mengurangi dan menghapus tagihan pasien",Alert::$DANGER);
            }
			$pack->setContent($result);
			$pack->setStatus(ResponsePackage::$STATUS_OK);
			return $pack->getPackage();
		}else if($command=="del" && $this->getDeleteCapability()){
            $pack=new ResponsePackage();
			$content		 = $this->delete();
            if($content['success']==1){
                $pack->setAlertContent("Data Removed", "Your Data Had Been Removed", ResponsePackage::$TIPE_INFO);
            }else if($content['success']==-1){
                $pack->setAlertContent("Peringatan", "<strong>Pasien Telah Di Tutup Tagihanya Oleh Kasir untuk proses pembayaran</strong>, </br>silakan menghubungi kasir dahulu bila akan menambah, menubah, mengurangi dan menghapus tagihan pasien", Alert::$DANGER);
            }else{
                $pack->setAlertContent("Data Not Removed", "Your Data Not Removed", ResponsePackage::$TIPE_INFO);    
            }
			$pack->setContent($content);
			$pack->setStatus(ResponsePackage::$STATUS_OK);
            $pack->setAlertVisible(true);		
            return $pack->getPackage();	
		}else{
			return parent::command($command);
		}
    }

    public function setAccounting($service,$code,$jenis_data,$tanggal,$uraian,$nilai){
        $this->acc_service=$service;
        $this->acc_code=$code;
        $this->acc_jenis_data=$jenis_data;
        $this->acc_tanggal=$tanggal;
        $this->nilai=$nilai;
        $this->uraian=$uraian;
        return $this;
    }
	
	public function save(){
		$data=parent::save();
		$carabayar=$_POST['carabayar'];
		$noreg=$_POST['noreg_pasien'];
		$bpjs_check=getSettings($this->dbtable->get_db(), "smis-rs-bpjs-check-".$this->polislug, "0")=="1";
		$autoload=getSettings($this->dbtable->get_db(), "smis-rs-bpjs-autoload-".$this->polislug, "1")=="1";
		if(strpos($carabayar, "umum")=== false && $bpjs_check && $autoload){
			$serv=new TotalTagihanServiceConsumer($this->dbtable->get_db(), $noreg);
			$serv->execute();
			$hasil=$serv->getContent();
			$data['bpjs']="1";
			$data['total_tagihan']=$hasil;
		}else{
			$data['bpjs']="0";
		}
        
        $this->notifyAccounting($data['id'],"");
		return $data;
	}
	
	public function delete(){

        $id['id']				= $_POST['id'];
        $one = $this->dbtable->select($id);
        require_once "rawat/function/tutup_tagihan.php";
        $cek = tutup_tagihan($one->noreg_pasien,$this->polislug);            

        if($cek=="1"){
            $success['success']	= -1;
            return $success;
        }

		$data=parent::delete();
		$carabayar=$_POST['carabayar'];
		$bpjs_check=getSettings($this->dbtable->get_db(), "smis-rs-bpjs-check-".$this->polislug, "0")=="1";
		$autoload=getSettings($this->dbtable->get_db(), "smis-rs-bpjs-autoload-".$this->polislug, "1")=="1";
        if(strpos($carabayar, "umum")=== false && $bpjs_check && $autoload){
			$serv=new TotalTagihanServiceConsumer($this->dbtable->get_db(), $_POST['noreg_pasien']);
			$serv->execute();
			$hasil=$serv->getContent();
			$data['bpjs']="1";
			$data['total_tagihan']=$hasil;
		}else{
			$data['bpjs']="0";
		}
        $this->notifyAccounting($_POST['id'],"del");
		return $data;
	}
    
    public function notifyAccounting($id,$operation){
        $slug=str_replace("_","-",$this->acc_jenis_data);
        if(getSettings($this->dbtable->get_db(),"smis-rs-acc-e-".$slug."-".$this->polislug,"1")=="1"){
            $data['jenis_akun'] = "transaction";
            $data['jenis_data'] = "penjualan";//$this->acc_jenis_data;
            $data['id_data']    = $id;
            $data['entity']     = $this->polislug;
            $data['service']    = $this->acc_service;
            $data['data']       = $id;
            $data['code']       = $this->acc_code."-".strtoupper($this->polislug)."-".$id;
            $data['operation']  = $operation;
            
            $x=$this->dbtable->selectEventDel($id,false);
            $hasil=(array)$x;
            $data['tanggal']    = $hasil[$this->acc_tanggal];
            $data['uraian']     = ucwords($this->acc_jenis_data)." ".$hasil['nama_pasien']." dengan Noreg ".$hasil['noreg_pasien']." : ".$hasil[$this->uraian];
            $data['nilai']      = $hasil[$this->nilai];
            
            require_once "smis-base/smis-include-service-consumer.php";
            $serv=new ServiceConsumer($this->dbtable->get_db(),"push_notify_accounting",$data,"accounting");
            $serv->execute();            
        }
        return $this;
    }
    

}