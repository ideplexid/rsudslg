<?php 

class OksigenManualResponder extends RawatResponder {
	public function __construct($dbtable, $uitable, $adapter, $polislug) {
		parent::__construct ( $dbtable, $uitable, $adapter ,$polislug);
	}
	public function postToArray() {
		$data = parent::postToArray ();
		if(!isset($_POST['harga'])){
			require_once 'rawat/class/service_consumer/OksigenService.php';
			$oksigen = new OksigenService ( $db, $this->polislug );
			$oksigen->execute ();
			$liter = $oksigen->getLiter ();
			$data ['harga_liter'] = $liter;
			$data ['harga'] = $_POST ['jumlah_liter'] * $data ['harga_liter'] + $_POST ['biaya_lain'];
		}else{
			$waktu_pasang = $_POST['waktu_pasang'];
			$waktu_lepas  = $_POST['waktu_lepas'];
			if($waktu_pasang=="" || $waktu_pasang=="0000-00-00 00:00:00" || $waktu_lepas=="" || $waktu_lepas=="0000-00-00 00:00:00" ){
				$data["jumlah_jam"]	= 0;
				$data["harga"]		= 0;
			}else{
				loadLIbrary("smis-libs-function-time");
				$minute 			= minute_different($waktu_lepas,$waktu_pasang);
				$hour 				= ceil($minute/60);
				$data["jumlah_jam"]	= $hour;
				$data["harga"]		= $hour*$_POST['harga_jam'];
			}
		}
		return $data;
	}
}
