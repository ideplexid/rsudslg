<?php

require_once "smis-base/smis-include-duplicate.php";
class AlokServiceResponder extends DuplicateResponder{
	protected $polislug;
	public function __construct(DBTable $dbtable,$entity){
		parent::__construct($dbtable, null, null);
		$this->polislug=$entity;
        $this->setDuplicate(false,"");
        $this->setAutonomous(getSettings($dbtable->get_db(),"smis_autonomous_id",""));
	}
    
    
    public function command($command){	
		if($command=="list"){
			$content=$this->view();			
		}else if($command=='save'){
			$content=$this->save();
		}else if($command=="del"){
			$content=$this->delete();
		}else if($command=="edit"){
			$content=$this->edit();
		}else if($command=="select"){
			$content=$this->select();
		}else if($command=="print-element"){
			$content=$this->printing("print-element");
		}else if($command=="printing"){
			$content=$this->printing();
		}		
		return $content;
	}
	
	public function save(){
		$data=parent::save();
		$carabayar=$_POST['carabayar'];
		$noreg=$_POST['noreg_pasien'];
		$bpjs_check=getSettings($this->dbtable->get_db(), "smis-rs-bpjs-check-".$this->polislug, "0")=="1";
		if(strpos($carabayar, "bpjs")!==false && $bpjs_check){
			$serv=new TotalTagihanServiceConsumer($this->dbtable->get_db(), $noreg);
			$serv->execute();
			$hasil=$serv->getContent();
			$data['bpjs']="1";
			$data['total_tagihan']=$hasil;
		}else{
			$data['bpjs']="0";
		}
		return $data;
	}
	
	public function delete(){
		$data=parent::delete();
		$carabayar=$_POST['carabayar'];
		$bpjs_check=getSettings($this->dbtable->get_db(), "smis-rs-bpjs-check-".$this->polislug, "0")=="1";
		if(strpos($carabayar, "bpjs")!==false && $bpjs_check){
			$serv=new TotalTagihanServiceConsumer($this->dbtable->get_db(), $noreg);
			$serv->execute();
			$hasil=$serv->getContent();
			$data['bpjs']="1";
			$data['total_tagihan']=$hasil;
		}else{
			$data['bpjs']="0";
		}
		return $data;
	}
    
     public function view(){
		$kriteria=isset($_POST['kriteria'])?$_POST['kriteria']:"";
		$number=(isset($_POST['number']) && $_POST['number']!="")?$_POST['number']:0;
		$max=isset($_POST['max'])?$_POST['max']:"10";
		$this->dbtable->setMaximum($max);
		$d=$this->dbtable->view($kriteria,$number);
		return $d;
	}
}

?>