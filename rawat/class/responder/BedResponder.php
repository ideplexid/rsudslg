<?php 
/**
 * this class used to handling
 * room service saving price.
 * each room have different 
 * methode of price calculation
 * 
 * @author    : Nurul Huda
 * @since     : 12 Des 2015
 * @version   : 1.3.0
 * @copyright : goblooge@gmail.com
 * @database  : - smis_rwt_bed_kamar_
 *              - smis_rwt_bed_
 * */


class BedResponder extends RawatResponder {
	public function __construct($dbtable, $uitable, $adapter, $polislug) {
		parent::__construct ( $dbtable, $uitable, $adapter,$polislug );
	}
	public function delete() {
		$id                      = $_POST ['id'];
		$bed                     = $this->select ( $id );
		$id_bed                  = $bed->id_bed;
		$setdata["terpakai"]     = "0";
        $setdata["nama_pasien"]  = "";
        $setdata["nrm_pasien"]   = "0";
        $setdata["noreg_pasien"] = "0";
        $uptd['id']              = $id_bed;
        $dbtable_bed             = new DBTable ( $this->getDBTable()->get_db(), "smis_rwt_bed_kamar_" . $this->polislug );
		$dbtable_bed->update ($setdata, $uptd);
		return parent::delete ();
	}
	
	/**
	 * fungsi ini untuk menghitung 
	 * selisih perhitungan hari sesuai dengan dua aturan yang berlaku
	 * 
	 * SYSTEM PERHITUNGAN "hari"
	 * - dihitung hanya tangal saja dari tanggal berapa sampai tanggal berapa.
	 * - misal tanggal 25 juni 2016 22:00 - 27 juni 2016 08:00 dihitung 3 hari karena
	 * - hanya memperhatikan tanggal dan jam tidak dianggap
	 * 
	 * SYSTEM PERHITUNGAN "jam"
	 * - minimal dihitung sesuai dengan nilai minimal Hari misalnya 6 jam
	 * - maka tanggal 25 juni 2016 22:00 - 27 juni 2016 08:00 dihitung 2 hari karena
	 * - jika dihitung jamnya adalah 
	 * - 2 jam pada tanggal 25 juni 2016
	 * - 24 jam pada tanggal 26 juni 2016
	 * - 8 jam pada tanggal 27 juni 2016
	 * - sehingga jika di total 2 jam + 24 jam + 8 jam = 34 jam
	 * - jika dihitung harian adalah 34 jam = 1 x 24 jam + 10 jam
	 * - berarti dihitung 1 hari + 10 jam 
	 * - (karena 10 jam sudah lebih dari nilai minimal yaitu 6 berarti dihitung 1 hari lagi)
	 * - sehingga 34 jam 	= (1 x 24 jam) + 10 jam 
	 * 					 	= 1 hari + 10 jam 
	 * 					 	= 1 hari + 1 hari (10 jam > 6 jam berarti dihitung 1 hari)
	 * 						= 2 hari
	 * SYSTEM PERHITUNGAN PER JAM tertentu
     * - sistem akan menghitung untuk hari berikutnya yang kurang dari jam 10 maka akan dihitung 
     * - satu hari, misal ditentukan jam 10.00 maka ketika pasien masuk jam 23.00 malam dan pulang jam 11.00 siang, dihitung 2 hari
     * - tapi jika masuk jam 02.00 malam dan keluar jam 12.00 maka dihitung 1 hari.
     * 
	 * @param date $masuk
	 * @param date $keluar
	 */
     
	public static function getDayLength($masuk,$keluar,$polislug){
        global $db;
        loadLibrary("smis-libs-function-time");
		$model              = getSettings($db,"smis-rs-bed-counter-" . $polislug,"hari");
		$day                = 9;
		if($model=="hari"){
			$day            = day_diff_only( $masuk, $keluar )+1;
		}else if($model=="jam"){
			$hour           = hour_different($masuk, $keluar );
			$min_hour       = getSettings($db,"smis-rs-bed-counter-jam-" . $polislug, "6")*1;
			$day            = floor($hour/24);
			$sisa_hari      = $hour%24;
			if($sisa_hari >= $min_hour){				
				$day++;
			}
		}else if($model=="per jam"){
			$day            = day_diff_only( $masuk, $keluar )+1;
			if($day<=1){
                $day        = 1;
            }else{
                $jam        = substr($keluar,10);
                $cut_hour   = getSettings($db,"smis-rs-bed-counter-cut-off-" . $polislug, "10:00");
                if($jam < $cut_hour){
                    $day--;
                }
            }
		}else if($model=="per jam+"){
			$min_hour       = getSettings($db,"smis-rs-bed-counter-jam-" . $polislug, "6")*1;
			$cut_hour   	= getSettings($db,"smis-rs-bed-counter-cut-off-" . $polislug, "10:00");
			$jam_keluar     = substr($keluar,10);
			$jam_masuk		= substr($masuk,10);
			$tgl_keluar     = substr($keluar,0,10);
			$tgl_masuk		= substr($masuk,0,10);
			$keluar_passed  = $tgl_keluar." ".$cut_hour;
			$masuk_passed   = $tgl_masuk." ".$cut_hour;

			/**menghitung excess hour awal */
			$excess_day_on 	  = 0 ;
			if($masuk<$masuk_passed){
				$excess_day_on = 1;
			}

			/** menghitung excess hour akhir */
			$excess_day_off 		= 0 ;
			if($keluar_passed<$keluar){
				$hour_excess  		= hour_different($keluar_passed, $keluar );
				if($hour_excess>$min_hour){
					$excess_day_off = 1;
				}
			}

			/**menghitung antara masuk sampai keluar */
			$passed_day = day_diff_only( $masuk, $keluar );

			$day = $excess_day_on+$excess_day_off+$passed_day;
		}	
		return $day;
	}
	
	
	public function save() {
		$data               = $this->postToArray ();
		$day                = self::getDayLength($_POST ['waktu_masuk'], $_POST ['waktu_keluar'],$this->polislug);
		$id ['id']          = $_POST ['id'];
		$data ['hari']      = $day;
		$data ['biaya']     = $day * $data ['harga'];
		$dbtable_bed        = new DBTable ( $this->getDBTable()->get_db (), "smis_rwt_bed_kamar_" . $this->polislug );

		if ($_POST ['id'] != 0 || $_POST ['id'] != "") {
			$dbtable        = new DBTable ( $this->getDBTable()->get_db (), "smis_rwt_bed_" . $this->polislug );
			$row = $dbtable->select ( $_POST ['id'], false );
			$id_bed         = $row->id_bed;
			if ($id_bed != $data ['id_bed']) {
                $setdata["terpakai"]       = "0";
				$setdata["nama_pasien"]    = "";
				$setdata["nrm_pasien"]     = "0";
				$setdata["noreg_pasien"]   = "0";
				$uptd['id']                = $id_bed;
                $dbtable_bed->update ( $setdata, $uptd);
			}
		}

		if ($_POST ['id'] == 0 || $_POST ['id'] == "") {
			$result                 = $this->dbtable->insert ( $data );
			$id ['id']              = $this->dbtable->get_inserted_id ();
			$success ['type']       = 'insert';
		} else {
			$result                 = $this->dbtable->update ( $data, $id );
			$success ['type']       = 'update';
		}
        
        $setdata["terpakai"]       = "1";
        $setdata["nama_pasien"]    = $data ['nama_pasien'];
        $setdata["nrm_pasien"]     = $data ['nrm_pasien'];
        $setdata["noreg_pasien"]   = $data ['noreg_pasien'];
		if (isset ( $_POST ["empty"] )) {
			$setdata["terpakai"]       = "0";
            $setdata["nama_pasien"]    = "";
            $setdata["nrm_pasien"]     = "0";
			$setdata["noreg_pasien"]   = "0";
		}else{
			$this->setBedSynch($data ['noreg_pasien'],$this->polislug,$_POST['nama_bed']);
		}    
        $uptd['id']                = $data ['id_bed'];
		$dbtable_bed->update ( $setdata,$uptd);

		$success ['id']             = $id ['id'];
		$success ['success']        = 1;
		if ($result === false)
			$success ['success']    = 0;
		
        /* duplicate function */
        $this->updateSynchOrigin($success);        
		$data                       = $success;
		$carabayar                  = $_POST['carabayar'];
		$noreg                      = $_POST['noreg_pasien'];
		$bpjs_check                 = getSettings($this->dbtable->get_db(), "smis-rs-bpjs-check-".$this->polislug, "0")=="1";
		if(strpos($carabayar, "umum")===false && $bpjs_check){
			$serv                   = new TotalTagihanServiceConsumer($this->dbtable->get_db(), $noreg);
			$serv->execute();
			$hasil                  = $serv->getContent();
			$data['bpjs']           = "1";
			$data['total_tagihan']  = $hasil;
		}else{
			$data['bpjs']           = "0";
		}
        
        $this->notifyAccounting($data['id'],"");
        
        //synch table avaiblabe bed
        require_once "rawat/function/update_available_beds.php";
        update_available_beds($this->polislug);

		return $data;
		
	}

	public function setBedSynch($noreg,$ruangan,$bed){
		require_once ("smis-base/smis-include-service-consumer.php");
		$datax = array();
		$datax['noreg_pasien'] 		= $noreg;
		$datax['last_ruangan'] 		= $ruangan;
		$datax['last_bed'] 	   		= $bed;
		$datax['last_kelas']   		= getSettings($db, "smis-rs-kelas-" . $ruangan, "");
		$serv  				   = new ServiceConsumer($this->getDBtable()->get_db(),"update_last_ruangan",$datax,"registration");
        $serv->execute();

        
	}


}

?>