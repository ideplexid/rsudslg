<?php

require_once 'smis-base/smis-include-service-consumer.php';
require_once 'smis-libs-bpjs/TotalTagihanServiceConsumer.php';

class LayananResponder extends DBResponder{
	private $polislug;
	public function setPolislug($p){
		$this->polislug=$p;
	}
	
	public function select(){
		$data=parent::select();
		$noreg=$data['No Register'];
		$carabayar=$data['carabayar'];
		$bpjs_check=getSettings($this->dbtable->get_db(), "smis-rs-bpjs-check-".$this->polislug, "0")=="1";
		
        /*service pengambilan data pasien ke pendaftaran*/
        if(getSettings($this->dbtable->get_db(),"smis-rs-check-pendaftaran-".$this->polislug,"0")=="1"){
            $responder = new ServiceResponder ( $db, $uitable, $adapter, "get_registered" );
            $responder->addData("id", $noreg);
            $responder->addData("command", "edit");
            $result = 	$responder->command ( "edit" );
            $px		=	$result['content'];
			$data['px']=$px;
			require_once "smis-libs-bpjs/function-naik-kelas.php";  
			$data['nama_naik_kelas']=select_topup_class($px['naik_kelas_bpjs']);
        }
        /*service pengambilan data pasien ke pendaftaran*/
        
        if(strpos($carabayar, "bpjs")!==false && $bpjs_check){
			$serv=new TotalTagihanServiceConsumer($this->dbtable->get_db(), $noreg);
			$serv->execute();
			$hasil=$serv->getContent();
			$data['bpjs']="1";
			$data['total_tagihan']=$hasil;
			$data['plafon_bpjs']=$this->getPlafon($this->dbtable->get_db(), $noreg);	
		}else{
			$data['bpjs']="0";
		}
		return $data;
	}
	
	public function getPlafon($db,$noreg){
		$data_post = array ();
		$data_post['command']="edit";
		$data_post['id']=$noreg;
		$service = new ServiceConsumer ( $db, "get_registered", $data_post );
		$service->execute ();
		$data = $service->getContent ();
		if($data!=NULL){
			return $data['plafon_bpjs'];
		}
		return "0";
	}
	
}

?>