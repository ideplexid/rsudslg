<?php 
class OperasiDetailResponder extends DBResponder{
    public function excel() {
        ob_clean();
        show_error();
        
        $this->dbtable->setShowAll(true);
        $data = $this->dbtable->view("",0);
        $fix  = $this->adapter->getContent($data['data']);
        
        require_once ("smis-libs-out/php-excel/PHPExcel.php");
        $file = new PHPExcel ();
        $sheet = $file->getActiveSheet ();

        $i = 1;
        
        $objDrawing = new PHPExcel_Worksheet_Drawing();
        $objDrawing->setName('logo_img');
        $objDrawing->setDescription('logo_img');
        $objDrawing->setPath(getLogoNonInterlaced());
        $objDrawing->setCoordinates('A'.$i);
        $objDrawing->setOffsetX(20); 
        $objDrawing->setOffsetY(5);
        $objDrawing->setWidth(100); 
        $objDrawing->setHeight(100);
        $objDrawing->setWorksheet($sheet);
        $i++;
        
        $sheet->setCellValue("C".$i, getSettings($db,"smis_autonomous_title"));
        $sheet->getStyle("C".$i)->getFont()->setBold(true)->setSize(14);
        $i++;
        $sheet->setCellValue("C".$i, getSettings($db,"smis_autonomous_address"));
        $sheet->getStyle("C".$i)->getFont()->setBold(true)->setSize(14);
        $i++;
        $sheet->setCellValue("C".$i, getSettings($db,"smis_autonomous_contact"));
        $sheet->getStyle("C".$i)->getFont()->setBold(true)->setSize(14);
        $i += 3;
        $sheet->mergeCells("A".$i.":L".$i)->setCellValue("A".$i,"LAPORAN OPERASI");
        $sheet->getStyle("A".$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle("A".$i)->getFont()->setBold(true)->setSize(14);
        $i++;
        $sheet->mergeCells("A".$i.":L".$i)->setCellValue("A".$i, strtoupper(ArrayAdapter::dateFormat("date d M Y", $_POST['dari']))." - ".strtoupper(ArrayAdapter::dateFormat("date d M Y", $_POST['sampai'])));
        $sheet->getStyle("A".$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle("A".$i)->getFont()->setBold(true);
        $i += 3;
        
        $border_start = $i;
        $sheet->setCellValue("A".$i, "No.");
        $sheet->setCellValue("B".$i, "Waktu");
        $sheet->setCellValue("C".$i, "Nama");
        $sheet->setCellValue("D".$i, "Tindakan");
        $sheet->setCellValue("E".$i, "No.Reg");
        $sheet->setCellValue("F".$i, "NRM");
        $sheet->setCellValue("G".$i, "Operator");
        $sheet->setCellValue("H".$i, "Anastesi");
        $sheet->setCellValue("I".$i, "Harga Dokter");
        $sheet->setCellValue("J".$i, "Jumlah");
        $sheet->setCellValue("K".$i, "Total");
        $sheet->setCellValue("L".$i, "Cara Bayar");
        
        $sheet->getStyle("A".$i.":C".$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle("A".$i.":C".$i)->getFont()->setBold(true);
        $i++;
        $start = $i;

        foreach($fix as $a) {
          
            $sheet->setCellValue("A".$i, $a['No.']);
            $sheet->setCellValue("B".$i, $a["Waktu"]);
            $sheet->setCellValue("C".$i, $a["Pasien"]);
            $sheet->setCellValue("D".$i, $a["Tindakan"]);
            $sheet->setCellValue("E".$i, $a["No. Reg"]);
            $sheet->setCellValue("F".$i, $a["NRM"]);
            $sheet->setCellValue("G".$i, $a["Dr. Operator"]);
            $sheet->setCellValue("H".$i, $a["Dr. Anastesi"]);
            $sheet->setCellValue("I".$i, $a["Harga"]);
            $sheet->setCellValue("J".$i, $a["Jumlah"]);
            $sheet->setCellValue("K".$i, $a["Total"]);
            $sheet->setCellValue("L".$i, $a["Cara Bayar"]);
            $sheet->getStyle("A".$i.":C".$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $sheet->getStyle("I".$i)->getNumberFormat()->setFormatCode('#,##0.00');
            $sheet->getStyle("K".$i)->getNumberFormat()->setFormatCode('#,##0.00');
            $i++;
        }
        
        $border_end = $i - 1;
        $end = $i-2;
        $jumlah_data = ($end - $start) + 1;
        $sheet->getStyle("A".$border_end.":L".$border_end)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle("A".$border_end.":L".$border_end)->getFont()->setBold(true);
        
        $thin = array ();
        $thin['borders']=array();
        $thin['borders']['allborders']=array();
        $thin['borders']['allborders']['style']=PHPExcel_Style_Border::BORDER_THIN ;
        $sheet  ->getStyle ( "A".$border_start.":L".$border_end )->applyFromArray ($thin);
        
        $filename = "Laporan Operasi ".strtoupper(ArrayAdapter::dateFormat("date d M Y", $_POST['dari']))." - ".strtoupper(ArrayAdapter::dateFormat("date d M Y", $_POST['sampai'])).".xls";
        
        header ( 'Content-Type: application/vnd.ms-excel' );
        header ( 'Content-Disposition: attachment;filename="'.$filename.'"' );
        header ( 'Cache-Control: max-age=0' );
        $writer = PHPExcel_IOFactory::createWriter ( $file, 'Excel5' );
        ob_clean();
        $writer ->save ( 'php://output' );
		return;
    }
}