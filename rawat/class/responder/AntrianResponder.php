<?php 
require_once "smis-base/smis-include-duplicate.php";
require_once "rawat/class/resource/OutPatientValidator.php";
class AntrianResponder extends DuplicateResponder {
	private $poliname;
	private $polislug;
	private $mode;
    private $px;
    
	public function __construct($dbtable, $uitable, $adapter,$poliname,$polislug,$mode) {
		parent::__construct ( $dbtable, $uitable, $adapter );
		$this->poliname	= $poliname;
		$this->polislug	= $polislug;
		$this->mode		= $mode;
		$this->dbtable	= $dbtable;
        $this->px		= null;
        $this->setDuplicate(false,"");
        $this->setAutonomous(getSettings($dbtable->get_db(),"smis_autonomous_id",""));
	}
	
	public function view(){
		$adapter = $this->adapter;
		$adapter ->setJumlahBed($this->getJumlahBed());
		return parent::view();
	}

	private function getJumlahBed(){
		$query = "SELECT count(*) as total 
					FROM smis_rwt_bed_kamar_" . $this->polislug . " 
					WHERE prop!='del' AND keterangan NOT LIKE '%[Extra Bed]%' ";
		$total = $this->dbtable->get_db()->get_var($query);
		return $total;
	}
	
	public function command($command){	
		if($command=="skb"){
			global $db;
			require_once 'smis-base/smis-include-service-consumer.php';
			$query  	= "SELECT nrm_pasien FROM smis_rwt_antrian_".$this->polislug." WHERE id='".$_POST['id']."' ";
			$nrm		= $db->get_var($query);
			$data['id'] = $nrm;
			$service	= new ServiceConsumer($db, "get_skb",$data,"registration");
			$service	->setMode(ServiceConsumer::$SINGLE_MODE)
						->execute();
			$pack		= $service->getContent();
			return $pack;
		}else if($command=="save_dokumen"){
			$pack		= new ResponsePackage();
			$content	= $this->save_dokumen();
			$pack		->setContent($content)
						->setStatus(ResponsePackage::$STATUS_OK)
						->setAlertVisible(true)
						->setAlertContent("Data Saved", "Your Data Had Been Saved", ResponsePackage::$TIPE_INFO);
			return $pack->getPackage();
		}else if($command=="titipan"){
			$pack	 = new ResponsePackage();
			$content = $this->titipan();
			$pack	 ->setContent($content)
					 ->setStatus(ResponsePackage::$STATUS_OK)
					 ->setAlertVisible(true)
					 ->setAlertContent("Data Saved", "Your Data Had Been Saved", ResponsePackage::$TIPE_INFO);
			return $pack->getPackage();
		}else if($command=="save"){
			$pack		= new ResponsePackage();
			$content	= NULL;
			$status		= 'not-command';	
			$alert		= array();			
			$content	= $this->save();
			if($content['success']==-1){
				$content['success'] = 0;
				$pack	->setContent($content)
						->setStatus(ResponsePackage::$STATUS_OK)
						->setAlertVisible(false)
						->setWarning(true, "Pasien Telah Terdaftar", "Pasien Dengan Nomor RM <strong>".ArrayAdapter::format("only-digit8", $_POST['nrm_pasien'])."</strong> Telah Terdaftar");
			}else if($content['success']<0){
				$alasan 			= $this->getErrorReason($content['success'],$_POST['nrm_pasien'],$_POST['no_register']);
				$content['success'] = 0;
				$pack	->setWarning(true, "Pengeluaran Gagal",$alasan )
						->setContent($content)
						->setStatus(ResponsePackage::$STATUS_OK)
						->setAlertVisible(false);
			}else{
				$pack	->setContent($content)
						->setStatus(ResponsePackage::$STATUS_OK)
						->setAlertVisible(true)
						->setAlertContent("Data Saved", "Your Data Had Been Saved", ResponsePackage::$TIPE_INFO);
			}
			return $pack->getPackage();
		}else{
			return parent::command($command);
		}
	}

	public function isPatientCouldOut($noreg,$nrm,$cara_keluar){
		return OutPatientValidator::is_patient_can_out($this->getDBtable()->get_db(),$this->polislug,$noreg,$nrm,$cara_keluar);
    }

	public static function getErrorReason($errno,$nrm,$noreg_pasien){
		return OutPatientValidator::pulang_error_reason($errno,$nrm,$noreg_pasien);
	}
	
	public function save_dokumen(){
		$up['id']		 = $_POST['id'];
		$data['dokumen'] = $_POST['dokumen'];
		return $this->getDBTable()->update($data,$up);
	}
    
    public function titipan(){
        $up['id']		 = $_POST['id'];
        $data['titipan'] = " NOT titipan ";
        $warp['titipan'] = DBController::$NO_STRIP_WRAP;
		return $this->getDBTable()->update($data,$up,$warp);
	}
		
	public function saveIntegrated(){
		global $db;
		$data = $this->postToArray ();
		show_error();
		
		if(isset($data ['waktu'])){
			if(strlen($data ['waktu'])<11){
				$data ['waktu'] = $data ['waktu'] . " " . date ( "H:i:s" );
			}
		}else{
			$data ['waktu'] = date("Y-m-d H:i:s");
		}
		$id ['id'] = $_POST ['id'];
		$noreg 	   = $_POST ['no_register'];
		$nrm 	   = $_POST ['nrm_pasien'];
		
		if (! isset ( $_POST ['selesai'] ) && $this->isNoregExist ( $nrm, $noreg )) { // sudah ada
			/**
			 * jaga - jaga ternyata kalau sudah di pulangkan, cukup di update selesainya saja
			 * yang asalnya selesai = 1 menjadi selesai = 0, 
			 * atau kalau yang selesainya = 0 maka tetap selesai = 0.
			 * 
			 * fungsi ini biasanya dipanggil melalui service push_antrian,
			 * sehingga ketika push_antrian tidak ada idnya,
			 * namun kadang $nrm dan $noreg sudah ada
			 */
			$data_search ['nrm_pasien']  = $nrm;
			$data_search ['no_register'] = $noreg;
			$up['selesai'] 				 = "0";
			$row 						 = $this->dbtable->select ( $data_search );
			$tid['id']					 = $row->id;
			$no_urut 					 = $row->nomor;
			$success ['id'] 			 = $row->id;			
			$this->dbtable->update($up,$tid);
			$success ['nomor'] 			 = $no_urut;
			$success ['success'] 		 = 1;	
			show_error();
			/**memasukan kamar */
			if(isset($_POST['id_bed_kamar']) && $_POST['id_bed_kamar']!="0"){
				$this->insertBed($_POST ['nrm_pasien'],$_POST ['nama_pasien'],$_POST ['no_register'],$_POST['id_bed_kamar'],$_POST['bed_kamar'],$data['waktu'],$_POST['carabayar']);
			}
			
			return $success;
		}
		
		if (isset ( $_POST ['selesai'] ) && $_POST['id']!="" && $_POST['id']!="0") {
			/**
			 * update khusus ketika ada kasus selesai, 
			 * dipanggil melalui menu Rawat -> Antrian
			 * bukan dari service.
			 */
			unset($data ['waktu']);
			$success ['id'] 		= $id ['id'];
			$success ['success'] 	= $this->isPatientCouldOut($noreg,$nrm,$_POST['cara_keluar']);
			if($success ['success']!=1){
				return $success;
            }
            
            /**menghitung los */

            if($_POST['waktu_keluar']=="" || $_POST['waktu_keluar']=="0000-00-00 00:00"){
                $query = "SELECT MAX(waktu_keluar) as max_keluar FROM smis_rwt_bed_".$this->polislug." WHERE noreg_pasien = '".$_POST['no_register']."' AND prop!='del' ";
                $max_keluar = $this->dbtable->get_db()->get_var($query);
                if($max_keluar!=null && $max_keluar!="" && $max_keluar!="0000-00-00 00:00:00"){
                    $_POST['waktu_keluar'] = $max_keluar;
                }
            }

            if($_POST['waktu_keluar']=="" || $_POST['waktu_keluar']=="0000-00-00 00:00"){
                $_POST['waktu_keluar'] = date("Y-m-d H:i:s");
            }

            $antrian = $this->dbtable->select(array("id"=>$id ['id']));
            $los = $this->hitung_los($antrian->waktu,$_POST['waktu_keluar']);
            $data['lama_rawat']		    = $los;
            $data['waktu_keluar']		= $_POST['waktu_keluar'];

            $this->dbtable->update($data,$id);
            
			$result 		 = $this->dbtable->update ( $data, $id );
			$success['type'] = 'update';				
			$nama			 = isset($_POST['nama_pasien'])?$_POST['nama_pasien']:"";
			$cara_keluar 	 = isset($_POST['cara_keluar'])?$_POST['cara_keluar']:"";
			$noreg			 = isset($_POST['no_register'])?$_POST['no_register']:"";
			require_once "rawat/function/notif_pasien_out.php";
			notif_pasien_out($db, $this->polislug, $this->poliname,$_POST['id'],$nama,$cara_keluar);
			
			/**pengeluaran pasien */
			global $user;
			require_once "rawat/function/in_log.php";
			in_log($this->getDBTable()->get_db(),$this->polislug,$_POST['nama_pasien'],$_POST['no_register'],$_POST['nrm_pasien'],$user->getNameOnly(),$this->polislug,"Pasien Keluar Dari Ruangan ".$_POST['cara_keluar']);

			$this->updateIGDStatus($_POST['id'],$noreg,$_POST['waktu_keluar'],$_POST['cara_keluar']);
			$this->pushOutKasir($_POST['id'],$noreg,$_POST['waktu_keluar'],$_POST['cara_keluar']);
			$this->updateRujukanStatus($_POST['id'],$noreg);
		} else {			
			$waktu 		   = date("Y-m-d");
			if(isset($_POST['waktu'])  ){
				if(strlen($_POST['waktu'])<11){
					$waktu = trim($_POST['waktu']);
				}else{
					$pw = explode(" ",trim($_POST['waktu']));
					$waktu = $pw[0];//substr($_POST['waktu'], 0,10);
				}
			}
			$data ['nomor'] 	= $this->getLastNumber( $waktu);
			$data ['kunjungan'] = $this->getStatusPasien( $_POST ['nrm_pasien'] );
			$success ['nomor'] 	= $data ['nomor'];
			
			if ($_POST ['id'] == 0 || $_POST ['id'] == "") {
				/* Dipakai Khusus Insert Baru, terutama yang dari service */
				if(isset($_POST['id_bed_kamar']) && $_POST['id_bed_kamar']!="0"){
					$this->insertBed($_POST ['nrm_pasien'],$_POST ['nama_pasien'],$_POST ['no_register'],$_POST['id_bed_kamar'],$_POST['bed_kamar'],$data['waktu'],$_POST['carabayar']);
				}

				/**
				 * jika setting RL - 5.2 diisi maka ketika pasien masuk pertama kali ke ruang ini akan di set 
				 * Laporan RL - 5.2 miliknya adalah sesuai defaultnya
				 * jika tidak di centang maka di lewati
				 */
				if( !isset( $data['rl52'] ) && getSettings($db, "smis-rs-rl52-".$this->polislug, "0")=="1"){
					$jenis_kegiatan = getSettings($db, "smis-rs-rl52-default-".$this->polislug, "");
					$data['rl52'] = $jenis_kegiatan;
					$params = array();
					$params['noreg_pasien'] = $_POST['noreg_pasien'];
					$params['ruangan_entri'] = $this->polislug;
					$params['jenis_kegiatan'] = $jenis_kegiatan;
					$consumer_service = new ServiceConsumer(
						$this->dbtable->get_db(),
						"update_jenis_kegiatan",
						$params,
						"registration"
					);
					$consumer_service->execute();
				}
				
				/**
				 * jika pertama kali pasien masuk kesini dan kelasnya adalah " - "
				 * maka kelas akan di set sesuai dengan kelas asal ruangan ini.
				 */
				if(isset($data['kelas']) && ($data['kelas']==" - " || $data['kelas']=="") ) {
					$data['kelas'] = getSettings($db, "smis-rs-kelas-".$this->polislug,"");
				}
				
				$result 		 = $this->dbtable->insert ( $data );
				$id['id'] 		 = $this->dbtable->get_inserted_id ();
				$success['type'] = 'insert';
				$nama			 = isset($_POST['nama_pasien'])?$_POST['nama_pasien']:"";
				require_once "rawat/function/notif_pasien_in.php";
				notif_pasien_in($db,$id['id'],$this->poliname,$this->polislug,$nama,$_POST['asal']);
			} else {
				$result = $this->dbtable->update ( $data, $id );
				$success ['type'] = 'update';
				/**memasukan kamar */
				if(isset($_POST['id_bed_kamar']) && $_POST['id_bed_kamar']!="0"){
					$this->insertBed($_POST ['nrm_pasien'],$_POST ['nama_pasien'],$_POST ['no_register'],$_POST['id_bed_kamar'],$_POST['bed_kamar'],$data['waktu'],$_POST['carabayar']);
				}
			}
		}
		
		$success ['id'] 		  = $id ['id'];
		$success ['success'] 	  = 1;
		if ($result === false)
			$success ['success']  = 0;
            
        /* duplicate function */
        $this->updateSynchOrigin($success);        
		return $success;
	}
	
	public function insertBed($nrm_pasien,$nama_pasien,$noreg_pasien, $id_bed,$nama_bed,$waktu,$carabayar){
		require_once "rawat/function/insert_bed.php";
		insert_bed($this->polislug,$nrm_pasien,$nama_pasien,$noreg_pasien, $id_bed,$nama_bed,$waktu,$carabayar);
	} 
    
    public function updateIGDStatus($id, $noreg_pasien,$carakeluar,$waktu){
		require_once "rawat/function/update_igd_status.php";
		update_igd_status($this->getDBTable()->get_db(),$this->polislug,$id, $noreg_pasien,$carakeluar,$waktu);
	}

	public function pushOutKasir($id,$noreg_pasien,$carakeluar,$waktu){
		require_once "rawat/function/push_out_to_kasir.php";
		push_out_to_kasir($this->getDBTable()->get_db(),$this->polislug,$id,$noreg_pasien,$carakeluar,$waktu);

	}

	public function updateRujukanStatus($id,$noreg_pasien){
		require_once "rawat/function/update_rujukan_status.php";
		update_rujukan_status($this->dbtable,$this->polislug,$id,$noreg_pasien);
	}
	
	private function getLastNumber($date) {
		$query 	= "SELECT MAX(nomor) as nomor,count(*) as total  FROM smis_rwt_antrian_" . $this->polislug. " WHERE DATE(waktu) = '".$date."' ";
		$row 	= $this->dbtable->get_row ( $query );
		$total 	= $row->total*1;
		$nomor 	= $row->nomor*1;
		if ($total == 0)
			return 1;
		else
			return ($nomor + 1);
	}

    public function hitung_los($waktu_masuk,$waktu_keluar){
        $masuk = strtotime(ArrayAdapter::format("date Y-m-d",$waktu_masuk));
        $keluar = strtotime(ArrayAdapter::format("date Y-m-d",$waktu_keluar));
        $datediff = $keluar-$masuk;
        $lama_hari = ceil($datediff/(60*60*24));
        return  $lama_hari;
    }

	public function save() {
		if($this->mode=="Integrated"){
			$noreg	= $this->getDBTable()->getNextID()*(-1);
			if( (!isset($_POST['id']) || $_POST['id']=="" || $_POST['id']=="0") &&
					(!isset($_POST['no_register']) || $_POST['no_register']=="" || ($_POST['no_register']*1) < 0) ){
				$this->addColumnFixValue("no_register", $noreg);
			}			
			return $this->saveIntegrated();
		}else{
			/* khusus stand alone, cuma memastikan noregnya ngawur saja bukan generated dari system
			 * mengambil noreg sebagai autoincrement dan dikali -1 supaya membedakan dengan yang integrated
			 * */
			$noreg	= $this->getDBTable()->getNextID()*(-1); 
			$nrm 	= $_POST ['nrm_pasien'];
			if(	isset($_POST ['id']) && ( $_POST ['id']=="" || $_POST ['id']=="0" ) && isset($_POST ['nrm_pasien']) && $this->isNRMExist($nrm) ){
				$success['id'] 		= 0;
				$success['nomor'] 	= "";
				$success['success'] = -1;
				return $success;
			}
			if( (!isset($_POST['id']) || $_POST['id']=="" || $_POST['id']=="0") && 
					(!isset($_POST['no_register']) || $_POST['no_register']=="" || ($_POST['no_register']*1) < 0) ){
					$this->addColumnFixValue("no_register", $noreg);
			}
			if( !isset($_POST['waktu']) || $_POST['waktu']=="")
				$this->addColumnFixValue("waktu", date("Y-m-d"));
			return $this->saveIntegrated();
		}
	}
	
	public function isNoregExist($nrm, $noreg) {
		$data ['nrm_pasien'] 	= $nrm*1;
		$data ['no_register'] 	= $noreg;
		if ($this->dbtable->is_exist ( $data )){
			return true;
		}	
		return false;
	}
	
	public function isNRMExist($nrm) {
		$data ['nrm_pasien'] 	= $nrm*1;
		$data ['selesai'] 		= "0";
		if ($this->dbtable->is_exist ( $data )){
			return true;
		}
		return false;
	}
	
	private function getStatusPasien($nrm) {
		$data ['nrm_pasien'] 	= $nrm;
		if ($this->dbtable->is_exist ( $data )){
			return "Lama";
		}
		return "Baru";
	}
	
	public function edit(){
		$this->getDBTable()->setFetchMethode(DBTable::$ARRAY_FETCH);
		$id		= $_POST['id'];
		$row	= $this->dbtable->select($id,false);
		if($row['waktu_keluar']=="0000-00-00 00:00:00"){
			$row['waktu_keluar'] = date("Y-m-d H:i:s");
		}
		return $row;
	}
    
    public function setPatientOut($noreg_pasien,$waktu_keluar,$cara_keluar,$ket_keluar,$ruang_tujuan){
        
        $antrian = $this->dbtable->select(array("no_register"=>$noreg_pasien));
        $los = $this->hitung_los($antrian->waktu,$waktu_keluar);

        $id['no_register']			= $noreg_pasien;
        $data['waktu_keluar']		= $waktu_keluar;
        $data['cara_keluar']		= $cara_keluar;
        $data['lama_rawat']		    = $los;
        $data['keterangan_keluar']	= $ket_keluar;
        $data['selesai']			= 1;
        $data['ruang_tujuan']	    = $ruang_tujuan;
        
        $this->dbtable->update($data,$id);

		$this->updateIGDStatus("",$noreg_pasien,$cara_keluar,$waktu_keluar);
		$this->pushOutKasir("",$noreg_pasien,$cara_keluar,$waktu_keluar);
		$this->updateRujukanStatus("",$noreg_pasien);
		
		/**pengeluaran pasien */
		global $user;
		require_once "rawat/function/in_log.php";
		in_log($this->getDBTable()->get_db(),$this->polislug,$_POST['nama_pasien'],$noreg_pasien,$_POST['nrm_pasien'],$user->getNameOnly(),$this->polislug,"Pasien Dipindahkan Ke Ruangan ".$_POST['ruangan']." : ".$cara_keluar." : ".$ket_keluar);
    }
    
    public function getIDFromNoreg($noreg_pasien){
        $id['no_register']	= $noreg_pasien;
        $this->dbtable->setFetchMode(DBTable::$ARRAY_FETCH);
        $row				= $this->dbtable->select($id);
        return isset($row['id'])?$row['id']:0;
    }
}

?>