<?php 
class AlokConnectedResponder extends AlokStandAloneResponder{
	protected $polislug;
	protected $ruang;
	public function __construct($dbtable, $uitable, $adapter,$polislug){
		parent::__construct($dbtable, $uitable, $adapter,$polislug);
		$this->polislug=$polislug;
		$this->ruang=getSettings($dbtable->get_db(),"smis-rs-stok-obat-boi-" . $polislug,"");
	}
	
	public function command($command){	
		if($command=='save'){
			$pack=new ResponsePackage();
			$content=NULL;
			$status='not-authorized';	//not-authorized, not-command, fail, success
			$alert=array();
			if($_POST['id_obat']!="" && $_POST['id_obat']!="0"){
				$result=$this->pushStokItem($_POST);
				if($result['success']==1){
					$kode=$result['id_transaksi'];
					$this->addColumnFixValue("kode",$kode);
					$content=$this->save();
					$pack->setContent($content);
					$pack->setStatus(ResponsePackage::$STATUS_OK);
					$pack->setAlertVisible(true);
					$pack->setAlertContent("Data Saved", "Your Data Had Been Saved with ID Transaction [ ".$kode." ]", ResponsePackage::$TIPE_INFO);
				}else{
					$pack->setStatus(ResponsePackage::$STATUS_OK);
					$pack->setAlertVisible(false);
					$pack->setWarning(true,"Terjadi Kesalahan !!!",$result['reason']);
				}
			}else{
				$content=$this->save();
				$pack->setContent($content);
				$pack->setStatus(ResponsePackage::$STATUS_OK);
				$pack->setAlertVisible(true);
				$pack->setAlertContent("Data Saved", "Your Data Had Been Saved, No ID Transaction", ResponsePackage::$TIPE_INFO);
			}
			return $pack->getPackage();

		}else{
			return parent::command($command);
		}
	}
		
	private function getSisaStokObat($id_obat){
		$serv=new ServiceConsumer($this->getDBTable()->get_db(),"get_item_stok",NULL,$this->ruang);
		$serv->addData("id_obat",$id_obat);
		$serv->addData("ruangan",$this->ruang);
		$serv->execute();
		$content=$serv->getContent();
		$jumlah=$content['jumlah'];
		return $jumlah;
	}
	
	private function pushStokItem($dataset){
		global $user; 
		$id_obat=$dataset['id_obat'];
		$jumlah=$dataset['jumlah'];
		$satuan=$dataset['satuan'];
		$next=$this->getDBTable()->getNextID();
		$keterangan="Dipakai Oleh Pasien ".
					$dataset['nama_pasien']." dengan NRM ".
					$dataset['nrm_pasien']." dan Nomor Registrasi ".
					$dataset['noreg_pasien']." Pada ID ".
					$next."di Ruang ".ArrayAdapter::format("unslug",$this->polislug);
		$operator=$user->getUsername();
		
		$serv=new ServiceConsumer($this->getDBTable()->get_db(),"push_item_stok",NULL,$this->ruang);
		$serv->addData("id_obat",$id_obat);
		$serv->addData("jumlah",$jumlah);
		$serv->addData("satuan",$satuan);
		$serv->addData("keterangan",$keterangan);
		$serv->addData("user",$operator);
		$serv->execute();
		return $serv->getContent();
	}
	
	private function cancelItemStok($id){
		global $user;
		$row=$this->getDBTable()->select($id);
		if($row==NULL || $row->id_obat*1==0){
			return;
		}
		$id_obat=$row->id_obat;
		$satuan=$row->satuan;
		$id_transaksi=$row->kode;
		$jumlah=$row->jumlah;
		$operator=$user->getUsername();
		$keterangan="Dibatalkan Transaksi Alat Obat Kesehatan (ALOK) di ".ArrayAdapter::format("unslug",$this->polislug)." 
					pada Pasien ".$_POST['nama_pasien']." 
					dengan NRM ".$_POST['nrm_pasien']." 
					dan No Register ".$_POST['noreg_pasien'];
		
		$serv=new ServiceConsumer($this->getDBTable()->get_db(),"cancel_item_stok",NULL,$this->ruang);
		$serv->addData("id_transaksi",$id_transaksi);
		$serv->addData("jumlah",$jumlah);
		$serv->addData("keterangan",$keterangan);
		$serv->addData("user",$operator);
		$serv->addData("satuan",$satuan);
		$serv->addData("id_obat",$id_obat);
		
		$serv->execute();
		return $serv->getContent();
	}
	
	
	public function delete(){
		$this->cancelItemStok($_POST['id']);
		return parent::delete();
	}
    
    
}

?>