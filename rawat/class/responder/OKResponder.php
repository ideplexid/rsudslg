<?php 

class OKResponder extends RawatResponder{
	public function command($command){		
		if($command=="find_perujuk"){
			$pack=new ResponsePackage();
			$content=$this->find_perujuk();
			$pack->setContent($content);
			$pack->setStatus(ResponsePackage::$STATUS_OK);
			return $pack->getPackage();
		}else{
			return parent::command($command);
		}
    }
    
    public function save(){
        /*update laporan operasi di medical record*/
        $update_mr  = new ServiceConsumer($this->getDBTable()->get_db(), "update_operasi",NULL,"medical_record");
        $update_mr  ->addData("noreg_pasien", $_POST['noreg_pasien'])
                    ->addData("operator", $_POST['nama_operator_satu']==""?$_POST['nama_operator_dua']:$_POST['nama_operator_satu'])
                    ->addData("asisten_operator", $_POST['nama_asisten_operator_satu']==""?$_POST['nama_asisten_operator_dua']:$_POST['nama_asisten_operator_satu'])
                    ->addData("instrument", isset($_POST['nama_instrument']) && $_POST['nama_instrument']!=""?$_POST['nama_instrument_dua']:$_POST['nama_instrument'])
                    ->addData("anastesi", $_POST['nama_anastesi'])
                    ->addData("asisten_anastesi", $_POST['nama_asisten_anastesi'])
                    ->addData("ruangan", $this->polislug)
                    ->addData("nama_icd", isset($_POST['nama_icd'])?$_POST['nama_icd']:"" )
                    ->addData("kode_icd", isset($_POST['kode_icd'])?$_POST['kode_icd']:"")
                    ->addData("ket_icd", isset($_POST['ket_icd'])?$_POST['ket_icd']:"")
                    ->execute();
       return parent::save();
    }
    
    public function detail(){
       $this->getDBTable()
            ->setFetchMethode(DBTable::$ARRAY_FETCH);
       $x       = $this->select();
       $table   = new TablePrint("");
       $table   ->setDefaultBootrapClass(true)
                ->addColumn("Operasi ".$x['nama_tindakan']."",3,1,"title")
                ->setMaxWidth(false);
       $list = array(
            "dokter_pendamping","operator_satu","operator_dua","asisten_operator_satu","asisten_operator_dua",
            "anastesi","asisten_anastesi","asisten_anastesi_dua",
            "instrument","instrument_dua","team_ok","oomloop_satu","oomloop_dua",
            "bidan","bidan_dua","perawat"
       );

       $table ->addColumn("<strong>Break Down</strong>",1,1)
              ->addColumn("",1,1)
              ->addColumn(ArrayAdapter::format("money Rp.",$total),1,1)
              ->commit("body");
       
       $total = 0;
       foreach($list as $name){
           if( ($x["harga_".$name]=="" || $x["harga_".$name]=="0") && $x["nama_".$name]==""){
               continue;
           }
           $table ->addColumn(ArrayAdapter::slugFormat("unslug",$name),1,1)
                  ->addColumn($x["nama_".$name],1,1)
                  ->addColumn(ArrayAdapter::format("money Rp.",$x["harga_".$name]),1,1)
                  ->commit("body");
           $total += $x["harga_".$name];
       }
       $name_only = array("perawat_dua","perawat_tiga","perawat_empat","perawat_lima","perawat_enam","perawat_tujuh","perawat_delapan","perawat_sembilan","perawat_sepuluh");
       foreach($name_only as $name){
            if($x["nama_".$name]==""){
                continue;
            }
            $table  ->addColumn(ArrayAdapter::slugFormat("unslug",$name),1,1)
                    ->addColumn($x["nama_".$name],1,1)
                    ->addColumn("",1,1)
                    ->commit("body");        
        }

       $list_only = array("harga_sewa_kamar","harga_sewa_alat","recovery_room");
       foreach($list_only as $name){
           if($x[$name]=="" || $x[$name]=="0"){
               continue;
           }
           $table ->addColumn(ArrayAdapter::slugFormat("unslug",$name),1,1)
                  ->addColumn("",1,1)
                  ->addColumn(ArrayAdapter::format("money Rp.",$x[$name]),1,1)
                  ->commit("body");
            $total += $x[$name];
       }
       
       $table ->addColumn("<strong>Total Break Down</strong>",1,1)
              ->addColumn("",1,1)
              ->addColumn(ArrayAdapter::format("money Rp.",$total),1,1)
              ->commit("body");


        
        $table ->addColumn("<strong>Tindakan 1</strong>",1,1)
              ->addColumn("",1,1)
              ->addColumn(ArrayAdapter::format("money Rp.",$x['harga_tindakan_satu']),1,1)
              ->commit("body");
        $table ->addColumn("<strong>Tindakan 2 </strong>",1,1)
              ->addColumn(" Rp. ".ArrayAdapter::format("only-money",$x['harga_tindakan_dua'])." * ".$x['percent_bius']."%",1,1)
              ->addColumn(ArrayAdapter::format("money Rp.",$x['harga_tindakan_dua']*$x['percent_bius']/100),1,1)
              ->commit("body");

        if($x['percent_cito']=="0"){
            $table  ->addColumn("<strong>Total</strong>",1,1)
                    ->addColumn("",1,1)
                    ->addColumn(ArrayAdapter::format("money Rp.",$x['harga_tindakan']),1,1)
                    ->commit("body");    
        }else{
            $table  ->addColumn("<strong>Sub Total</strong>",1,1)
                    ->addColumn("",1,1)
                    ->addColumn(ArrayAdapter::format("money Rp.",$x['harga_tindakan_dua']*$x['percent_bius']/100 + $x['harga_tindakan_satu']),1,1)
                    ->commit("body");
            $table  ->addColumn("<strong>Cyto</strong>",1,1)
                    ->addColumn($x['percent_cito']."%",1,1)
                    ->addColumn(ArrayAdapter::format("money Rp.",$x['harga_tindakan']),1,1)
                    ->commit("body");
        }
              
       $table ->addColumn("Kelas",1,1)
              ->addColumn(ArrayAdapter::slugFormat("unslug",$x['kelas']),1,1)
              ->addColumn("",1,1)
              ->commit("body");
       return $table ->getHtml();
    }
	
	public function find_perujuk(){	
		$service=new ServiceConsumer($this->dbtable->get_db(),"get_dokter_perujuk",NULL,"registration");
		$service->setMode(ServiceConsumer::$SINGLE_MODE);
		$service->addData("id_dokter",$_POST['id_operator']);
		$service->addData("nama_dokter",$_POST['nama_operator']);
		$service->addData("nrm_pasien",$_POST['nrm_pasien']);
		$service->addData("tanggal",$_POST['tanggal_operasi']);
		$service->execute();
		$content=$service->getContent();
		return $content;
	}
    
    public function notifyAccounting($id,$operation){
        $slug=str_replace("_","-",$this->acc_jenis_data);
        if(getSettings($this->dbtable->get_db(),"smis-rs-acc-e-".$slug."-".$this->polislug,"1")=="1"){
            $data['jenis_akun'] = "transaction";
            $data['jenis_data'] = "penjualan";//$this->acc_jenis_data;
            $data['id_data']    = $id;
            $data['entity']     = $this->polislug;
            $data['service']    = $this->acc_service;
            $data['data']       = $id;
            $data['code']       = $this->acc_code."-".strtoupper($this->polislug)."-".$id;
            $data['operation']  = $operation;
            
            $x=$this->dbtable->selectEventDel($id,false);
            require_once "rawat/function/summary_biaya_ok.php";
            $hasil=(array)$x;
            $data['tanggal']    = $hasil[$this->acc_tanggal];
            $data['uraian']     = ucwords($this->acc_jenis_data)." ".$hasil['nama_pasien']." dengan Noreg ".$hasil['noreg_pasien']." : ".$hasil[$this->uraian];
            $data['nilai']      = summary_biaya_ok($hasil);
            
            require_once "smis-base/smis-include-service-consumer.php";
            $serv=new ServiceConsumer($this->dbtable->get_db(),"push_notify_accounting",$data,"accounting");
            $serv->execute();
        }
        return $this;
    }
}

?>