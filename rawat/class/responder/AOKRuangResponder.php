<?php 

class AOKRuangResponder extends DBResponder{
	
	public function command($command){
		if($command=="reclear"){
			$pack=new ResponsePackage();
			$this->reclear();
			$pack->setStatus(ResponsePackage::$STATUS_OK);
			$pack->setAlertContent("Success","Data Berhasil Di bersihkan");
			$pack->setContent("SUKSES");
			return $pack->getPackage();
		}else{
			return parent::command($command);
		}
	}
	
	private function reclear(){
		$polislug=$_POST['prototype_slug'];
		$query="UPDATE smis_rwt_aok_".$polislug." SET prop='del' WHERE connect=0 AND id_obat=0 AND nama IN (
			SELECT nama FROM ( SELECT nama FROM smis_rwt_aok_".$polislug." WHERE id_obat!=0 AND connect=1)X
		); ";
		$db=$this->getDBTable()->get_db();
		$db->query($query);
	}
	
}

?>