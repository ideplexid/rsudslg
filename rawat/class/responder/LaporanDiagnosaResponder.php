<?php 

class LaporanDiagnosaResponder extends ServiceResponder{
    
	public function excel(){
		$clean = ob_get_clean();
		global $user;
		loadLibrary("smis-libs-function-export");
        $this->addData("command","list");
        $this->addData("showall","1");
        $this->getService()->setData($this->getData());
		$this->getService()->execute();
		$d = $this->getService()->getContent();
        $data = $d['data'];
        require_once "smis-libs-out/php-excel/PHPExcel.php";
		$fillcabang = array();
        $fillcabang['borders'] = array();
        $fillcabang['borders']['allborders'] = array();
        $fillcabang['borders']['allborders']['style'] = PHPExcel_Style_Border::BORDER_THIN ;

		$file   = new PHPExcel ();
        $file   ->setActiveSheetIndex ( 0 );
		$sheet  = $file->getActiveSheet ( 0 );
		$sheet  ->setTitle ("Laporan Diagnosa Gizi");
		
		$dari  = ArrayAdapter::format("date d M Y", $_POST['dari']);
		$sampai  = ArrayAdapter::format("date d M Y", $_POST['sampai']);

		$sheet->mergeCells("A1:M1")->setCellValue ( "A1", "LAPORAN DIAGNOSA GIZI" );
		$sheet->mergeCells("A2:M2")->setCellValue ( "A2", "Tanggal ". $dari ." - ".$sampai);

		$sheet->setCellValue("A4","No.");
		$sheet->setCellValue("B4","Tanggal Kunjungan");
		$sheet->setCellValue("C4","Tanggal Lahir");
		$sheet->setCellValue("D4","No. RM");
		$sheet->setCellValue("E4","No. Registrasi");
		$sheet->setCellValue("F4","Nama");
		$sheet->setCellValue("G4","Poli Rujukan");
		$sheet->setCellValue("H4","Dokter");
		$sheet->setCellValue("I4","Jenis Kelamin");
		$sheet->setCellValue("J4","Diagnosa");
		$sheet->setCellValue("K4","BB/TB");
		$sheet->setCellValue("L4","Status Gizi");
		$sheet->setCellValue("M4","Cara Bayar ");
		$sheet->getStyle ( "A1:M4")->getFont ()->setBold ( true );
		
		$start = 4;
		$nomor = 0;
		foreach($data as $x){
			$start++;
			$nomor++;
			$sheet->setCellValue("A".$start,$nomor.". ");
			$sheet->setCellValue("B".$start,ArrayAdapter::format("date d M Y",$x['tanggal']) );
			$sheet->setCellValue("C".$start,ArrayAdapter::format("date d M Y",$x['tanggal_lahir']) );
			$sheet->setCellValue("D".$start,$x['nrm_pasien']);
			$sheet->setCellValue("E".$start,$x['noreg_pasien']);
			$sheet->setCellValue("F".$start,$x['nama_pasien']);
			$sheet->setCellValue("G".$start,ArrayAdapter::format("unslug",$x['ruangan_rujukan']) );
			$sheet->setCellValue("H".$start,$x['nama_dokter']);
			$sheet->setCellValue("I".$start,$x['jk']=="1"?"P":"L");
			$sheet->setCellValue("J".$start,$x['nama_icd']);
			$sheet->setCellValue("K".$start,$x['bbtb']);
			$sheet->setCellValue("L".$start,$x['status_gizi']);
			$sheet->setCellValue("M".$start,ArrayAdapter::format("unslug",$x['carabayar']));
		}
		$sheet->getStyle ( 'A4:M'.$start )->applyFromArray ( $fillcabang );
		$sheet->getColumnDimension("A")->setAutoSize(true);
		$sheet->getColumnDimension("B")->setAutoSize(true);
		$sheet->getColumnDimension("C")->setAutoSize(true);
		$sheet->getColumnDimension("D")->setAutoSize(true);
		$sheet->getColumnDimension("E")->setAutoSize(true);
		$sheet->getColumnDimension("F")->setAutoSize(true);
		$sheet->getColumnDimension("G")->setAutoSize(true);
		$sheet->getColumnDimension("H")->setAutoSize(true);
		$sheet->getColumnDimension("I")->setAutoSize(true);
		$sheet->getColumnDimension("J")->setAutoSize(true);
		$sheet->getColumnDimension("K")->setAutoSize(true);
		$sheet->getColumnDimension("L")->setAutoSize(true);
		$sheet->getColumnDimension("M")->setAutoSize(true);

		header ( 'Content-Type: application/vnd.ms-excel' );
        header ( 'Content-Disposition: attachment;filename="Laporan Diagnosa Gizi.xls"' );
        header ( 'Cache-Control: max-age=0' );
        $writer = PHPExcel_IOFactory::createWriter ( $file, 'Excel5' );
        $writer->save ( 'php://output' );        
		return;
	}
}