<?php 

class GiziResponder extends DBResponder {
	private $polislug;
	private $simple;
	public function __construct($dbtable, $uitable, $adapter, $polislug) {
		parent::__construct ( $dbtable, $uitable, $adapter );
		$this->polislug = $polislug;
		$this->simple=false;
	}
	
	public function setSimple($simple){
		$this->simple=$simple;
	}
	
	public function save(){
		if($_POST['id']!="" && $_POST['id']!="0"){
			
			$id=$_POST['id'];
			$nama=$_POST['nama_pasien'];
			$noreg=$_POST['noreg_pasien'];				
			$row=$this->dbtable->select($id);
			$key=md5($this->poliname."-gizi-rubah-".$id ['id']."-".rand(0, 10000));
			$msg="Pasien <strong>".$nama."</strong> pada Ruangan <strong>".ArrayAdapter::format("unslug", $this->polislug)."</strong> Perubahan ";
			$change=false;
			if($this->simple){
				if($row->menu_pagi!=$_POST['menu_pagi'] && $row->menu_pagi!=""){
					$msg.=". Menu dari <u>".$row->menu_pagi."</u> Menjadi <u>".$_POST['menu_pagi']."</u>";
					$change=true;
				}
				if($row->diet_pagi!=$_POST['diet_pagi'] && $row->diet_pagi!=""){
					$msg.=". Diet dari <u>".$row->diet_pagi."</u> Menjadi <u>".$_POST['diet_pagi']."</u>";
					$change=true;
				}
				if($row->pk_pagi!=$_POST['pk_pagi'] && $row->pk_pagi!=""){
					$msg.=". Pesanan Khusus dari <u>".$row->pk_pagi."</u> Menjadi <u>".$_POST['pk_pagi']."</u>";
					$change=true;
				}
				
			}else{
				if($row->menu_pagi!=$_POST['menu_pagi'] && $row->menu_pagi!=""){
					$msg.=". Menu Pagi dari <u>".$row->menu_pagi."</u> Menjadi <u>".$_POST['menu_pagi']."</u>";
					$change=true;
				}
				if($row->diet_pagi!=$_POST['diet_pagi'] && $row->diet_pagi!=""){
					$msg.=". Diet Pagi dari <u>".$row->diet_pagi."</u> Menjadi <u>".$_POST['diet_pagi']."</u>";
					$change=true;
				}
				if($row->pk_pagi!=$_POST['pk_pagi'] && $row->pk_pagi!=""){
					$msg.=". Pesanan Khusus Pagi dari <u>".$row->pk_pagi."</u> Menjadi <u>".$_POST['pk_pagi']."</u>";
					$change=true;
				}
				
				if($row->menu_siang!=$_POST['menu_siang'] && $row->menu_siang!=""){
					$msg.=". Menu Siang dari <u>".$row->menu_siang."</u> Menjadi <u>".$_POST['menu_siang']."</u>";
					$change=true;
				}
				if($row->diet_siang!=$_POST['diet_siang'] && $row->diet_siang!=""){
					$msg.=". Diet Siang dari <u>".$row->diet_siang."</u> Menjadi <u>".$_POST['diet_siang']."</u>";
					$change=true;
				}
				if($row->pk_siang!=$_POST['pk_siang'] && $row->pk_siang!=""){
					$msg.=". Pesanan Khusus Siang dari <u>".$row->pk_siang."</u> Menjadi <u>".$_POST['pk_siang']."</u>";
					$change=true;
				}
				
				if($row->menu_siang!=$_POST['menu_malam'] && $row->menu_malam!=""){
					$msg.=". Menu Malam dari <u>".$row->menu_malam."</u> Menjadi <u>".$_POST['menu_malam']."</u>";
					$change=true;
				}
				if($row->diet_malam!=$_POST['diet_malam'] && $row->diet_malam!=""){
					$msg.=". Diet Malam dari <u>".$row->diet_malam."</u> Menjadi <u>".$_POST['diet_malam']."</u>";
					$change=true;
				}
				if($row->pk_malam!=$_POST['pk_malam'] && $row->pk_malam!=""){
					$msg.=". Pesanan Malam dari <u>".$row->pk_malam."</u> Menjadi <u>".$_POST['pk_malam']."</u>";
					$change=true;
				}
			}
			if($change){
				global $notification;
				$notification->addNotification("Perubahan Resep Pasien", $key, $msg,"gizi","pesanan");
				
			}
		}
		return parent::save();
	}
	
	public function edit() {
		$id = $_POST ['id'];
		$dbtable = new DBTable ( $this->dbtable->get_db (), "smis_rwt_gizi_" . $this->polislug );
		if (! $dbtable->is_exist ( $id )) {
			$data ['id'] = $id;
			$dbtable->insert ( $data );
			$id = $dbtable->get_inserted_id ();
		}
		$row = $this->dbtable->select ( array (
				"id" => "='" . $id . "'"
		), false );
		return $row;
	}
}
?>