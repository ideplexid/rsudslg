<?php 

class RujukanResponder extends DBResponder {
	private $polislug;
	private $kelas;
    private $error;
	public function __construct($dbtable, $uitable, $adapter, $polislug) {
		parent::__construct ( $dbtable, $uitable, $adapter );
		$this->polislug = $polislug;
		$this->kelas="rawat_jalan";
        $this->error="";
	}
	
    public function command($command){
        if($command=="save"){
            $pack=new ResponsePackage();
			$content=NULL;
			$status='not-command';	//not-authorized, not-command, fail, success
			$alert=array();			
			$content=$this->save();
			if($content['success']==-100){
				$pack->setContent($content);
				$pack->setStatus(ResponsePackage::$STATUS_OK);
				$pack->setAlertVisible(false);
				$pack->setWarning(true, "Pengeluaran Gagal",$this->error);
			}
            $pack->setContent($content);
            $pack->setStatus(ResponsePackage::$STATUS_OK);
			$pack->setAlertVisible(true);
			$pack->setAlertContent("Data Saved", "Your Data Had Been Saved", ResponsePackage::$TIPE_INFO);                
			return $pack->getPackage();
        }else{
            return parent::command($command);
        }
    }
    
	public function setKelas($kelas){
		$this->kelas=$kelas;
	}
	
	public function save() {
        $db = $this->getDBTable()->get_db();         
        $dbtable = new DBTable($db,"smis_rwt_antrian_".$this->polislug);
        $dbtable->update(array("ruang_tujuan"=>$_POST['ruangan']),array("no_register"=>$_POST['noreg_pasien']));

        if(isset($_POST['save_and_out'])){
            require_once "rawat/class/responder/AntrianResponder.php";
            $uitable=new Table(array());
            $adapter=new SimpleAdapter();
            $poliname="";
            $polislug=$this->polislug;
            $antrian=new AntrianResponder($dbtable, $uitable, $adapter,$poliname,$polislug,"");
            
            if(isset($_POST['cara_keluar'])){
                $cara_keluar = $_POST['cara_keluar'];
            }else{
				$cara_keluar = "Pindah Kamar";//getSettings($db,"smis-rs-register-df-cara-keluar-".$this->polislug,"Pindah Kamar");
            }
            
            $code=$antrian->isPatientCouldOut($_POST['noreg_pasien'],$_POST['nrm_pasien'],$cara_keluar);
            if($code<0){
                $this->error = $antrian->getErrorReason($code,$_POST['nrm_pasien'],$_POST['noreg_pasien']);
                $success ['success'] = -100;
                return $success;
            }else{
                $hasil = $this->saveOnly();
                if($hasil['success']==1){
                    $noreg_pasien = $_POST['noreg_pasien'];
                    $waktu_keluar = $_POST['waktu'];                    
                    $ket_keluar   = $_POST['keterangan'];
                    $antrian->setPatientOut($noreg_pasien,$waktu_keluar,$cara_keluar,$ket_keluar,$_POST['ruangan']);
                }
                $page	= isset($_POST['page'])?$_POST['page']:"";
                $action = "antrian";
				forceChangeCookie($page,$action);

				$this->notifSalahKamar();
				return $hasil;
            }
        }else{
           return $this->saveOnly();
        }
	}
    
    public function notifSalahKamar(){
		global $db;
		require_once "rawat/function/notif_salah_kamar.php";
		notif_salah_kamar($db, $this->polislug, $_POST['nama_pasien'], $_POST['noreg_pasien'], $_POST['nrm_pasien']);
	}
    
    public function saveOnly(){
        $data 		= $this->postToArray ();
		$no_urut 	= $this->pushService ();

		if ($no_urut == - 1) { // gagal
			$success ['id'] 		= - 1;
			$success ['success'] 	= 0;
			return $success;
		}

		$data ['no_urut'] 			= $no_urut;
		$id ['id'] 					= $_POST ['id'];
		if ($_POST ['id'] == 0 || $_POST ['id'] == "") {
			$result 				= $this->dbtable->insert ( $data );
			$id ['id'] 				= $this->dbtable->get_inserted_id ();
			$success ['type'] 		= 'insert';
		} else {
			$result = $this->dbtable->update ( $data, $id );
			$success ['type'] 		= 'update';
		}
		$success ['id'] 			= $id ['id'];
		$success ['success'] 		= 1;
		if ($result === false)
			$success ['success'] 	= 0;
		return $success;
    }
    
	public function pushService() {
        global $db;
		$id 		= $_POST ['id'];
		$id_antrian = $_POST ['id_antrian'];
		$tanggal 	= $_POST ['waktu'];
		$db 		= $this->dbtable->get_db ();
		$dbtable 	= new DBTable ( $db, "smis_rwt_antrian_" . $this->polislug );
		$row 		= $dbtable->select ( $id_antrian );
		$ruangan 	= $_POST ['ruangan'];
		
		$array					 = array ();
		$array['waktu'] 		 = $_POST ['waktu'];
		$array['nrm_pasien'] 	 = $row->nrm_pasien;
		$array['nama_pasien'] 	 = $row->nama_pasien;
		$array['nama_dokter'] 	 = $_POST['nama_dokter'];
		$array['id_dokter'] 	 = $_POST['id_dokter'];
		$array['id'] 			 = "";
		$array['command'] 		 = 'save';
		$array['carabayar'] 	 = $row->carabayar;
		$array['detail'] 		 = $row->detail;
		$array['ibu_kandung']	 = $row->ibu_kandung;
		$array['no_register'] 	 = $row->no_register;
		$array['umur'] 		 	 = $row->umur;
		$array['asal'] 		 	 = strtoupper ( $this->polislug );
		$array['kelas'] 		 = $this->kelas;
		$array['jk'] 			 = $row->jk;
        $array['alamat'] 		 = $row->alamat;
        $array['waktu_register'] = $row->waktu_register;
		$array['golongan_umur'] 	= $row->golongan_umur;
		$array['keterangan_pindah'] = $_POST['keterangan'];
		

		if(isset($_POST['id_bed_kamar']) && isset($_POST['bed_kamar'])  && $_POST['id_bed_kamar']!="0" && $_POST['bed_kamar']!=""){
			$array['id_bed_kamar'] 		 = $_POST['id_bed_kamar'];
			$array['bed_kamar'] 		 = $_POST['bed_kamar'];		
		}

		$antri  = new ServiceConsumer ( $this->dbtable->get_db (), "push_antrian", $array, $ruangan );
		$antri  ->execute ();
		$result = $antri->getContent ();

		if ($result != null && $result != "" && isset ( $result ['content'] ['nomor'] )) {
			return $result ['content'] ['nomor'];
		} else {
			return - 1;
		}
	}
}

?>