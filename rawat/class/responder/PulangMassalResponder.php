<?php 

class PulangMassalResponder extends DBResponder {
	public function command($command){
        if($command=="pulangkan"){
            $pack=new ResponsePackage();
            $pack->setStatus(ResponsePackage::$STATUS_OK);
            $content=$this->pulangkan();
            $pack->setContent($content);
            return $pack->getPackage();
        }else if($command=="aktifkan"){
            $pack=new ResponsePackage();
            $pack->setStatus(ResponsePackage::$STATUS_OK);
            $content=$this->aktifkan();
            $pack->setContent($content);
            return $pack->getPackage();
        }else{
            return parent::command($command);
        }
    }
    
    private function pulangkan(){
        $kriteria=$this->getDBTable()->getCustomKriteria();
        return $this->getDBTable()->updateByCustomKriteria(array("selesai"=>"1")," 1 ".$kriteria);
    }
    
    private function aktifkan(){
       $kriteria=$this->getDBTable()->getCustomKriteria();
        return $this->getDBTable()->updateByCustomKriteria(array("selesai"=>"0")," 1 ".$kriteria);
    }
}

?>