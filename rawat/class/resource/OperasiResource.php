<?php 

class OperasiResource{

    public static function getAdapterTagihan(){
        $okadapter = new OptionBuilder();
        $okadapter ->add("Operasi Detail di Akunting dan Kasir","TagihanOKAdapter","1");
        $okadapter ->add("Operasi Detail di Akunting","TagihanOKAdapterAccounting","0");
        $okadapter ->add("Operasi Tidak Detail","-","0");
        return $okadapter;
    }

    public static function getUIOperasiDiagnosa(){
        $opsi = new OptionBuilder();
        $opsi ->add("Tampilkan","textarea;Diagnosa;;y;null;true;null;true;;","0")
              ->add("Sembunyikan","hidden;;;;;;;;;;","1");
        return $opsi->getContent();
    }


    public static function getUIHargaTindakan1(){
        $opsi = new OptionBuilder();
        $opsi ->add("Tampilkan - Disable","money;Harga Tindakan 1;;y;null;true;null;false;harga_sewa_alat","0")
              ->add("Tampilkan - Editable","money;Harga Tindakan 1;;y;null;false;null;false;harga_sewa_alat","0")
              ->add("Sembunyikan","hidden;;;;;;;;;;","1");
        return $opsi->getContent();
    }

    public static function getUIHargaTindakan2(){
        $opsi = new OptionBuilder();
        $opsi ->add("Tampilkan - Disable","money;Harga Tindakan 2;;y;null;true;null;false;harga_sewa_alat","0")
              ->add("Tampilkan - Editable","money;Harga Tindakan 2;;y;null;false;null;false;harga_sewa_alat","0")
              ->add("Sembunyikan","hidden;;;;;;;;;;","1");
        return $opsi->getContent();
    }


    public static function getUIOperasiTindakan(){
        $opsi = new OptionBuilder();
        $opsi ->add("Tampilkan - Tindakan Dokter 1","chooser-ok-ok_nama_tindakan-Tindakan 1;Tindakan 1;;y;null;false;null;true;","0")
              ->add("Tampilkan - Tindakan Operasi 1","chooser-ok-ok_nama_operasi-Tindakan 1;Tindakan 1;;y;null;false;null;true;","0")
              ->add("Tampilkan - Tindakan VK 1","chooser-ok-ok_nama_vk-Tindakan 1;Tindakan 1;;y;null;false;null;true;","0")
              ->add("Sembunyikan","hidden;;;;;;;;;;","1");
        return $opsi->getContent();
    }

    public static function getUIOperasiTindakanDua(){
        $opsi = new OptionBuilder();
        $opsi ->add("Tampilkan - Tindakan Dokter 2","chooser-ok-ok_nama_tindakan-Tindakan 2;Tindakan 2;;y;null;false;null;true;","0")
              ->add("Tampilkan - Tindakan Operasi 2","chooser-ok-ok_nama_operasi_dua-Tindakan 2;Tindakan 2;;y;null;false;null;true;","0")
              ->add("Tampilkan - Tindakan VK 2","chooser-ok-ok_nama_vk-Tindakan 2;Tindakan 2;;y;null;false;null;true;","0")
              ->add("Sembunyikan","hidden;;;;;;;;;;","1");
        return $opsi->getContent();
    }

    public static function getUIOperasiSewaKamar(){
        $opsi = new OptionBuilder();
        $opsi ->add("Tampilkan","chooser-ok-ok_tarif_ok;Sewa Kamar;;y;null;false;null;false;harga_sewa_kamar","0")
              ->add("Sembunyikan","hidden;;;;;;;;;;","1");
        return $opsi->getContent();
    }

    public static function getUIOperasiHargaSewaKamar(){
        $opsi = new OptionBuilder();
        $opsi ->add("Tampilkan - Disable","money;Harga Sewa OK;;y;null;true;null;false;harga_sewa_alat","0")
              ->add("Tampilkan - Editable","money;Harga Sewa OK;;y;null;false;null;false;harga_sewa_alat","0")
              ->add("Sembunyikan","hidden;;;;;;;;;;","1");
        return $opsi->getContent();
    }

    public static function getUIOperasiHargaSewaAlat(){
        $opsi = new OptionBuilder();
        $opsi ->add("Tampilkan - Disable","money;Sewa Alat;;y;null;true;null;false;kelas","0")
              ->add("Tampilkan - Editable","money;Sewa Alat;;y;null;false;null;false;kelas","0")
              ->add("Sembunyikan","hidden;;;;;;;;;;","1");
        return $opsi->getContent();
    }

    public static function getUIOperasiKelas(){
        $opsi = new OptionBuilder();
        $opsi ->add("Tampilkan - Disable","text;Kelas;;y;null;true;null;false;nama_operator_satu","0")
              ->add("Sembunyikan","hidden;;;;;;;;;;","1");
        return $opsi->getContent();
    }

    public static function getUIOperasiPerujuk(){
        $opsi = new OptionBuilder();
        $opsi ->add("Tampilkan - Disable","chooser-ok-ok_perujuk-Perujuk;Perujuk;;n;null;true;null;false;","0")
              ->add("Tampilkan - Editable","chooser-ok-ok_perujuk-Perujuk;Perujuk;;n;null;true;null;false;","0")
              ->add("Sembunyikan","hidden;;;;;;;;;;","1");
        return $opsi->getContent();
    }

    public static function getUIOperasiKodePerujuk(){
        $opsi = new OptionBuilder();
        $opsi ->add("Tampilkan - Disable","text;Kode Perujuk;;y;null;true;null;false;;","0")
              ->add("Tampilkan - Editable","text;Kode Perujuk;;y;null;false;null;false;;","0")
              ->add("Sembunyikan","hidden;;;;;;;;;;","1");
        return $opsi->getContent();
    }


    public static function getUIOperasiDokterPendamping(){
        $opsi = new OptionBuilder();
        $opsi ->add("Tampilkan","chooser-ok-ok_dokter_pendamping-Dokter Pendamping;Dokter Pendamping;;n;null;false;null;false;","0")
              ->add("Sembunyikan","hidden;;;;;;;;;;","1");
        return $opsi->getContent();
    }

    public static function getUIOperasiBiayaDokterPendamping(){
        $opsi = new OptionBuilder();
        $opsi ->add("Tampilkan - Disable","money;Biaya Dokter Pendamping;;y;null;true;null;false;nama_operator_dua","0")
              ->add("Tampilkan - Editable - Boleh Nol","money;Biaya Dokter Pendamping;;y;null;false;null;false;nama_operator_dua","0")
              ->add("Tampilkan - Editable - Tidak Boleh Nol","money;Biaya Dokter Pendamping;;n;null;false;null;false;nama_operator_dua","0")
              ->add("Sembunyikan","hidden;;;;;;;;;;","1");
        return $opsi->getContent();
    }

    

    public static function getUIOperasiOperator1(){
        $opsi = new OptionBuilder();
        $opsi ->add("Tampilkan","chooser-ok-ok_operator_satu-Operator I;Operator I;;n;null;false;null;false;","0")
              ->add("Sembunyikan","hidden;;;;;;;;;;","1");
        return $opsi->getContent();
    }

    public static function getUIOperasiJenisOperator1(){
        $opsi = new OptionBuilder();
        $opsi ->add("Tampilkan","text;Jenis Operator I;;y;null;true;null;false;","0")
              ->add("Sembunyikan","hidden;;;;;;;;;;","1");
        return $opsi->getContent();
    }

    public static function getUIOperasiBiayaOperator1(){
        $opsi = new OptionBuilder();
        $opsi ->add("Tampilkan - Disable","money;Biaya Operator I;;y;null;true;null;false;nama_operator_dua","0")
              ->add("Tampilkan - Editable - Boleh Nol","money;Biaya Operator I;;y;null;false;null;false;nama_operator_dua","0")
              ->add("Tampilkan - Editable - Tidak Boleh Nol","money;Biaya Operator I;;n;null;false;null;false;nama_operator_dua","0")
              ->add("Sembunyikan","hidden;;;;;;;;;;","1");
        return $opsi->getContent();
    }

    public static function getUIOperasiOperator2(){
        $opsi = new OptionBuilder();
        $opsi ->add("Tampilkan","chooser-ok-ok_operator_dua-Operator II;Operator II;;n;null;false;null;false;","0")
              ->add("Sembunyikan","hidden;;;;;;;;;;","1");
        return $opsi->getContent();
    }

    public static function getUIOperasiJenisOperator2(){
        $opsi = new OptionBuilder();
        $opsi ->add("Tampilkan","text;Jenis Operator II;;y;null;true;null;false;","0")
              ->add("Sembunyikan","hidden;;;;;;;;;;","1");
        return $opsi->getContent();
    }

    public static function getUIOperasiBiayaOperator2(){
        $opsi = new OptionBuilder();
        $opsi ->add("Tampilkan - Disable","money;Biaya Operator II;;y;null;true;null;false;nama_asisten_operator_satu","0")
              ->add("Tampilkan - Editable","money;Biaya Operator II;;y;null;false;null;false;nama_asisten_operator_satu","0")
              ->add("Sembunyikan","hidden;;;;;;;;;;","1");
        return $opsi->getContent();
    }

    public static function getUIOperasiAsistenOperator1(){
        $opsi = new OptionBuilder();
        $opsi ->add("Tampilkan - Asisten Operator I","chooser-ok-ok_asisten_operator_satu-Asisten Operator I;Asisten Operator I;;n;null;false;null;false","0")
              ->add("Tampilkan - Asisten","chooser-ok-ok_asisten_operator_satu-Asisten;Asisten;;n;null;false;null;false","0")
              ->add("Sembunyikan","hidden;;;;;;;;;;","1");
        return $opsi->getContent();
    }

    public static function getUIOperasiBiayaAsistenOperator1(){
        $opsi = new OptionBuilder();
        $opsi ->add("Tampilkan - Disable - Biaya Ass. OP I","money;Biaya Ass. OP I;;y;null;true;null;false;nama_asisten_operator_dua","0")
              ->add("Tampilkan - Editable - Biaya Ass. OP I","money;Biaya Ass. OP I;;y;null;false;null;false;nama_asisten_operator_dua","0")
              ->add("Tampilkan - Disable - Asisten","money;Biaya Assisten;;y;null;true;null;false;nama_asisten_operator_dua","0")
              ->add("Tampilkan - Editable - Asisten","money;Biaya Assisten;;y;null;false;null;false;nama_asisten_operator_dua","0")
              ->add("Sembunyikan","hidden;;;;;;;;;;","1");
        return $opsi->getContent();
    }

    public static function getUIOperasiAsistenOperator2(){
        $opsi = new OptionBuilder();
        $opsi ->add("Tampilkan","chooser-ok-ok_asisten_operator_dua-Asisten Operator II;Asisten Operator II;;n;null;false;null;false","0")
              ->add("Sembunyikan","hidden;;;;;;;;;;","1");
        return $opsi->getContent();
    }

    public static function getUIOperasiBiayaAsistenOperator2(){
        $opsi = new OptionBuilder();
        $opsi ->add("Tampilkan - Disable","money;Biaya Ass. OP II;;y;null;true;null;false;anastesi_hadir","0")
              ->add("Tampilkan - Editable","money;Biaya Ass. OP II;;y;null;false;null;false;anastesi_hadir","0")
              ->add("Sembunyikan","hidden;;;;;;;;;;","1");
        return $opsi->getContent();
    }

    public static function getUIOperasiKehadiranAnastesi(){
        $opsi = new OptionBuilder();
        $opsi ->add("Tampilkan","checkbox;Anastesi Hadir ?;0;y;null;false;null;false;nama_anastesi","0")
              ->add("Sembunyikan","hidden;;;;;;;;;;","1");
        return $opsi->getContent();
    }

    public static function getUIOperasiAnastesi(){
        $opsi = new OptionBuilder();
        $opsi ->add("Tampilkan","chooser-ok-ok_anastesi-Dokter Anastesi;Dokter Anastesi;;y;null;false;null;false","0")
              ->add("Sembunyikan","hidden;;;;;;;;;;","1");
        return $opsi->getContent();
    }

   
    public static function getUIOperasiBiayaAnastesi(){
        $opsi = new OptionBuilder();
        $opsi ->add("Tampilkan - Disable","money;Biaya Anastesi;;y;null;true;null;false;nama_asisten_anastesi","0")
              ->add("Tampilkan - Editable","money;Biaya Anastesi;;y;null;false;null;false;nama_asisten_anastesi","0")
              ->add("Sembunyikan","hidden;;;;;;;;;;","1");
        return $opsi->getContent();
    }

    public static function getUIOperasiAsistenAnastesi1(){
        $opsi = new OptionBuilder();
        $opsi ->add("Tampilkan - Assisten Anastesi I","chooser-ok-ok_asisten_anastesi-Ass. Anastesi;Ass. Anastesi;;y;null;false;null;false;","0")
              ->add("Tampilkan - Perawat Anastesi","chooser-ok-ok_asisten_anastesi-Perawat Anastesi;Perawat Anastesi;;y;null;false;null;false;","0")
              ->add("Sembunyikan","hidden;;;;;;;;;;","1");
        return $opsi->getContent();
    }

    public static function getUIOperasiBiayaAsistenAnastesi1(){
        $opsi = new OptionBuilder();
        $opsi ->add("Tampilkan - Disable - Biaya Ass. Anastesi","money;Biaya Ass. Anastesi;;y;null;true;null;false;nama_asisten_anastesi_dua","0")
              ->add("Tampilkan - Editable - Biaya Ass. Anastesi","money;Biaya Ass. Anastesi;;y;null;false;null;false;nama_asisten_anastesi_dua","0")
              ->add("Tampilkan - Disable - Biaya Perawat Anastesi","money;Biaya Perawat Anastesi;;y;null;true;null;false;nama_asisten_anastesi_dua","0")
              ->add("Tampilkan - Editable - Biaya Perawat Anastesi","money;Biaya Perawat Anastesi;;y;null;false;null;false;nama_asisten_anastesi_dua","0")
              ->add("Sembunyikan","hidden;;;;;;;;;;","1");
        return $opsi->getContent();
    }


    public static function getUIOperasiAsistenAnastesi2(){
        $opsi = new OptionBuilder();
        $opsi ->add("Tampilkan","chooser-ok-ok_asisten_anastesi_dua-Ass. Anastesi II;Ass Anastesi II;;y;null;false;null;false;","0")
              ->add("Sembunyikan","hidden;;;;;;;;;;","1");
        return $opsi->getContent();
    }

    public static function getUIOperasiBiayaAsistenAnastesi2(){
        $opsi = new OptionBuilder();
        $opsi ->add("Tampilkan - Disable","money;Biaya Ass. Anastesi II;;y;null;true;null;false;nama_instrument","0")
              ->add("Tampilkan - Editable","money;Biaya Ass. Anastesi II;;y;null;false;null;false;nama_instrument","0")
              ->add("Sembunyikan","hidden;;;;;;;;;;","1");
        return $opsi->getContent();
    }


    public static function getUIOperasiInstrument1(){
        $opsi = new OptionBuilder();
        $opsi ->add("Tampilkan - Instrument I","chooser-ok-ok_instrument-Instrument I;Instrument I;;y;null;false;null;false;","0")
              ->add("Tampilkan - Instrument","chooser-ok-ok_instrument-Instrument;Instrument;;y;null;false;null;false;","0")
              ->add("Sembunyikan","hidden;;;;;;;;;;","1");
        return $opsi->getContent();
    }

    public static function getUIOperasiBiayaInstrument1(){
        $opsi = new OptionBuilder();
        $opsi ->add("Tampilkan - Disable - Instrument I","money;Biaya Instrument I;;y;null;true;null;false;nama_instrument_dua","0")
              ->add("Tampilkan - Editable  - Instrument I","money;Biaya Instrument I;;y;null;false;null;false;nama_instrument_dua","0")
              ->add("Tampilkan - Disable  - Instrument","money;Biaya Instrument;;y;null;true;null;false;nama_instrument_dua","0")
              ->add("Tampilkan - Editable  - Instrument","money;Biaya Instrument;;y;null;false;null;false;nama_instrument_dua","0")
              ->add("Sembunyikan","hidden;;;;;;;;;;","1");
        return $opsi->getContent();
    }

    public static function getUIOperasiInstrument2(){
        $opsi = new OptionBuilder();
        $opsi ->add("Tampilkan","chooser-ok-ok_instrument_dua;Instrument II;;y;null;false;null;false;","0")
              ->add("Sembunyikan","hidden;;;;;;;;;;","1");
        return $opsi->getContent();
    }

    public static function getUIOperasiBiayaInstrument2(){
        $opsi = new OptionBuilder();
        $opsi ->add("Tampilkan - Disable","money;Biaya Instrument II;;y;null;true;null;false;nama_oomloop_satu","0")
              ->add("Tampilkan - Editable","money;Biaya Instrument II;;y;null;false;null;false;nama_oomloop_satu","0")
              ->add("Sembunyikan","hidden;;;;;;;;;;","1");
        return $opsi->getContent();
    }

    public static function getUIOperasiOomloop1(){
        $opsi = new OptionBuilder();
        $opsi ->add("Tampilkan - Oomloop I","chooser-ok-ok_oomloop_satu-Oomloop I;Oomloop I;;y;null;false;null;false;harga_oomloop_satu","0")
              ->add("Tampilkan - Oomloop","chooser-ok-ok_oomloop_satu-Oomloop;Oomloop;;y;null;false;null;false;harga_oomloop_satu","0")
              ->add("Tampilkan - Perawat Sirkuler I","chooser-ok-ok_oomloop_satu-Perawat Sirkuler I;Perawat Sirkuler I;;y;null;false;null;false;harga_oomloop_satu","0")
              ->add("Tampilkan - Perawat Sirkuler","chooser-ok-ok_oomloop_satu-Perawat Sirkuler;Perawat Sirkuler;;y;null;false;null;false;harga_oomloop_satu","0")
              ->add("Sembunyikan","hidden;;;;;;;;;;","1");
        return $opsi->getContent();
    }

    public static function getUIOperasiBiayaOomloop1(){
        $opsi = new OptionBuilder();
        $opsi ->add("Tampilkan - Disable  - Oomloop I","money;Biaya Oomloop;;y;null;true;null;false;nama_oomloop_dua","0")
              ->add("Tampilkan - Editable - Oomloop I","money;Biaya Oomloop;;y;null;false;null;false;nama_oomloop_dua","0")
              ->add("Tampilkan - Disabled - Biaya Perawat Sirkuler I","money;Biaya Perawat Sirkuler I;;y;null;true;null;false;nama_oomloop_dua","0")
              ->add("Tampilkan - Editable - Biaya Perawat Sirkuler I","money;Biaya Perawat Sirkuler I;;y;null;false;null;false;nama_oomloop_dua","0")
              ->add("Sembunyikan","hidden;;;;;;;;;;","1");
        return $opsi->getContent();
    }

    public static function getUIOperasiOomloop2(){
        $opsi = new OptionBuilder();
        $opsi ->add("Tampilkan - Oomloop II","chooser-ok-ok_oomloop_dua;Oomloop II;;y;null;false;null;false;harga_oomloop_dua","0")
              ->add("Tampilkan - Perawat Sirkuler II","chooser-ok-ok_oomloop_dua;Perawat Sirkuler II;;y;null;false;null;false;harga_oomloop_dua","0")
              ->add("Sembunyikan","hidden;;;;;;;;;;","1");
        return $opsi->getContent();
    }

    public static function getUIOperasiBiayaOomloop2(){
        $opsi = new OptionBuilder();
        $opsi ->add("Tampilkan - Disable - Oomloop II","money;Biaya Oomloop II;;y;null;true;null;false;nama_bidan","0")
              ->add("Tampilkan - Editable - Oomloop II","money;Biaya Oomloop II;;y;null;false;null;false;nama_bidan","0")
              ->add("Tampilkan - Disable - Perawat Sirkuler II","money;Biaya Perawat Sirkuler II;;y;null;true;null;false;nama_bidan","0")
              ->add("Tampilkan - Editable - Perawat Sirkuler II","money;Biaya Perawat Sirkuler II;;y;null;false;null;false;nama_bidan","0")
              ->add("Sembunyikan","hidden;;;;;;;;;;","1");
        return $opsi->getContent();
    }

    public static function getUIOperasiBidan1(){
        $opsi = new OptionBuilder();
        $opsi ->add("Tampilkan","chooser-ok-ok_team_bidan;Bidan I;;y;null;false;null;false;","0")
              ->add("Sembunyikan","hidden;;;;;;;;;;","1");
        return $opsi->getContent();
    }

    public static function getUIOperasiBiayaBidan1(){
        $opsi = new OptionBuilder();
        $opsi ->add("Tampilkan - Disable","money;Biaya Bidan I;;y;null;true;null;false;nama_bidan_dua","0")
              ->add("Tampilkan - Editable","money;Biaya Bidan I;;y;null;false;null;false;nama_bidan_dua","0")
              ->add("Sembunyikan","hidden;;;;;;;;;;","1");
        return $opsi->getContent();
    }

    public static function getUIOperasiBidan2(){
        $opsi = new OptionBuilder();
        $opsi ->add("Tampilkan","chooser-ok-ok_team_bidan;Bidan II;;y;null;false;null;false;","0")
              ->add("Sembunyikan","hidden;;;;;;;;;;","1");
        return $opsi->getContent();
    }

    public static function getUIOperasiBiayaBidan2(){
        $opsi = new OptionBuilder();
        $opsi ->add("Tampilkan - Disable","money;Biaya Bidan II;;y;null;true;null;false;nama_perawat","0")
              ->add("Tampilkan - Editable","money;Biaya Bidan II;;y;null;false;null;false;nama_perawat","0")
              ->add("Sembunyikan","hidden;;;;;;;;;;","1");
        return $opsi->getContent();
    }

    public static function getUIOperasiPerawat($name="",$rum="I"){

        if($rum=="I"){
            $rum ="Recovery Room";
        }

        $opsi = new OptionBuilder();
        $opsi ->add("Tampilkan","chooser-ok-ok_perawat".$name."-Perawat ".$rum.";Perawat ".$rum.";;y;null;false;null;false","0")
              ->add("Sembunyikan","hidden;;;;;;;;;;","1");
        return $opsi->getContent();
    }

    public static function getUIOperasiBiayaPerawat(){
        $opsi = new OptionBuilder();
        $opsi ->add("Tampilkan - Disable","money;Biaya Perawat;;y;null;true;null;false;nama_team_ok","0")
              ->add("Tampilkan - Editable","money;Biaya Perawat;;y;null;false;null;false;nama_team_ok","0")
              ->add("Sembunyikan","hidden;;;;;;;;;;","1");
        return $opsi->getContent();
    }


    public static function getUIOperasiTeamOK(){
        $opsi = new OptionBuilder();
        $opsi ->add("Tampilkan","chooser-ok-ok_team_ok;Team OK;;y;null;false;null;false;","0")
              ->add("Sembunyikan","hidden;;;;;;;;;;","1");
        return $opsi->getContent();
    }

    public static function getUIOperasiBiayaTeamOK(){
        $opsi = new OptionBuilder();
        $opsi ->add("Tampilkan - Disable","money;Biaya Team OK;;y;null;true;null;false;recovery_room","0")
              ->add("Tampilkan - Editable","money;Biaya Team OK;;y;null;false;null;false;recovery_room","0")
              ->add("Sembunyikan","hidden;;;;;;;;;;","1");
        return $opsi->getContent();
    }

    public static function getUIOperasiBiayaRR(){
        $opsi = new OptionBuilder();
        $opsi ->add("Tampilkan - Disable","money;Recovery Room;;y;null;true;null;false;keterangan","0")
              ->add("Tampilkan - Editable","money;Recovery Room;;y;null;false;null;false;keterangan","0")
              ->add("Sembunyikan","hidden;;;;;;;;;;","1");
        return $opsi->getContent();
    }

    public static function getUIOperasiKeterangan(){
        $opsi = new OptionBuilder();
        $opsi ->add("Tampilkan","textarea;Keterangan;;y;null;false;null;false;save","0")
              ->add("Sembunyikan","hidden;;;;;;;;;;","1");
        return $opsi->getContent();
    }

}

?>