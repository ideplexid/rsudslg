<?php

class OutPatientValidator{
    private static $ERROR_BED        = -2;
    private static $ERROR_OPERASI    = -3;
    private static $ERROR_OKSIGEN    = -4;
    private static $ERROR_PLEBITIS   = -5;
    private static $ERROR_CAUTI      = -6;
    private static $ERROR_DIAGNOSA   = -7;
    private static $CURRENT_PATIENT  = null;
    private static $RM_ERRNO         = "";
    private static $RM_LOCKER        = "";
    
    public static function is_patient_can_out(Database $db,$polislug,$noreg,$nrm,$cara_keluar){
        if(!self::cek_kamar($db,$polislug,$noreg,$nrm)){
            return self::$ERROR_BED;
        }else if(!self::cek_oksigen($db,$polislug,$noreg,$nrm)){
            return self::$ERROR_OKSIGEN;
        }else if(!self::cek_ok($db,$polislug,$noreg,$nrm)){
            return self::$ERROR_OPERASI;
        }else if(!self::cek_plebitis($db,$polislug,$noreg,$cara_keluar)){
            return self::$ERROR_PLEBITIS;
        }else if(!self::cek_cauti($db,$polislug,$noreg,$cara_keluar)){
            return self::$ERROR_CAUTI;
        }else if(!self::cek_diagnosa($db,$polislug,$noreg,$cara_keluar)){
            return self::$ERROR_DIAGNOSA;
        }
        return 1;
    }

    private static function cek_kamar($db,$polislug,$noreg,$nrm){
		$query = "SELECT count(*) as total FROM smis_rwt_bed_".$polislug." WHERE nrm_pasien='".$nrm."' AND noreg_pasien='".$noreg."' AND ( waktu_masuk='0000-00-00 00:00:00' OR waktu_keluar='0000-00-00 00:00:00') AND prop!='del'";
		$total = $db->get_var($query);
		if(($total*1)>0){
            return false;
        }
		$query = "SELECT count(*) as total FROM smis_rwt_bed_kamar_".$polislug." WHERE nrm_pasien='".$nrm."' AND noreg_pasien='".$noreg."' AND prop!='del' ";
		$total = $db->get_var($query);
		if(($total*1)>0){
            return false;
        }
		return true;
	}
	
	private static function cek_ok($db,$polislug,$noreg,$nrm){
		if(getSettings($db, "smis-rs-non-zero-operasi-" .$polislug, "1")=="1"){
            return true;
        }
		$query = "SELECT count(*) as total FROM smis_rwt_ok_".$polislug." WHERE nrm_pasien='".$nrm."' AND noreg_pasien='".$noreg."' AND ( harga_operator_satu='0' AND harga_operator_satu='0' OR nama_operator_satu='' AND nama_operator_dua='')  AND prop!='del'";
		$total = $db->get_var($query);
		if(($total*1)>0){
            return false;
        }
		return true;
	}
	
	private static function cek_oksigen($db,$polislug,$noreg,$nrm){
		$query="SELECT count(*) as total FROM smis_rwt_oksigen_central_".$polislug." WHERE nrm_pasien='".$nrm."' AND noreg_pasien='".$noreg."' AND ( mulai='0000-00-00 00:00:00' OR selesai='0000-00-00 00:00:00') AND prop!='del'";
		$total=$db->get_var($query);
		if(($total*1)>0) return false;	
		return true;
	}
    
    private static function cek_plebitis($db,$polislug,$noreg,$cara_keluar){
        if( $cara_keluar=="Tidak Datang" || getSettings($db,"smis-rs-check-plebitis-".$polislug,"0")=="0"){
            return true;
        }        
        $px=self::get_patient_on_registration($db,$noreg);
        if($px['rekam_medis']!=""){
            $rm=json_decode($px['rekam_medis'],true);
            if(isset($rm['plebitis']) && $rm['plebitis']=="1"){
               $rm_lock=self::get_rekam_medis_locker($db,$polislug,$noreg);
               if($rm_lock['plebitis']==""){
                  self::$RM_ERRNO="";
                  return true;
               }
               self::$RM_ERRNO=$rm_lock['plebitis'];
               return false;
            }
        }
        return true;
    }
    
    private static function cek_cauti($db,$polislug,$noreg,$cara_keluar){
        if($cara_keluar=="Tidak Datang" || getSettings($db,"smis-rs-check-cauti-".$polislug,"0")=="0"){
            return true;
        }
        $px=self::get_patient_on_registration($db,$noreg);
        if($px['rekam_medis']!=""){
            $rm=json_decode($px['rekam_medis'],true);
            if(isset($rm['cauti']) && $rm['cauti']=="1"){
                $rm_lock=$self::get_rekam_medis_locker($db,$polislug,$noreg);
               if($rm_lock['cauti']==""){
                    return true;
               }
               self::$RM_ERRNO=$rm_lock['cauti'];
               return false;
            }
        }
        return true;
    }
    
    private static function cek_diagnosa($db,$polislug,$noreg,$cara_keluar){
        if($cara_keluar=="Tidak Datang" || getSettings($db,"smis-rs-check-diagnosa-".$polislug,"0")=="0"){
            return true;
        }
        $rm_lock=self::get_rekam_medis_locker($db,$polislug,$noreg);
        if($rm_lock['diagnosa']==""){
           return true;
        }
        self::$RM_ERRNO=$rm_lock['diagnosa'];
        return false;
    }
    
    private static function get_rekam_medis_locker($db,$polislug,$noreg){
        if(self::$RM_LOCKER==null){
            require_once "smis-base/smis-include-service-consumer.php";
            $service=new ServiceConsumer($db,"get_rekam_medis_locker",array("noreg_pasien"=>$noreg,"ruangan"=>$polislug),"medical_record");
            $service->execute();
            self::$RM_LOCKER=$service->getContent();
        }
        return self::$RM_LOCKER;
    }
    
    private static function get_patient_on_registration($db,$noreg){
        if(self::$CURRENT_PATIENT==null){
            require_once "smis-base/smis-include-service-consumer.php";
            /*service pengambilan data pasien ke pendaftaran*/
            $responder               = new ServiceResponder ( $db, new Table(""), new SimpleAdapter(), "get_registered" );
            $responder               ->addData("id", $noreg);
            $responder               ->addData("command", "edit");
            $result                  = 	$responder->command ( "edit" );
            self::$CURRENT_PATIENT   =	$result['content'];
            /*service pengambilan data pasien ke pendaftaran*/
        }
        return self::$CURRENT_PATIENT;
    }
	
    public static function pulang_error_reason($errno,$nrm,$noreg_pasien){
        if($errno==self::$ERROR_BED){
              return "Pasien Dengan Nomor RM <strong>".ArrayAdapter::format("only-digit8", $nrm)."</strong> Tidak Dapat dikeluarkan karena Masih Terdapat Penggunaan Bed yang mungkin Belum di set Tanggal Masuk atau Tanggal Keluarnya. 
                      <ul>
                          <li>silakan klik Tombol Hijau <font class='btn btn-success'><i class='fa fa-sign-in'></i></font></li>
                          <li>masuk ke Tab <a href='javascript:;' ><i class='fa fa-wrench'></i> Alat dan Obat</a> kemudian pilih tab <a href='javascript:;' ><i class='fa fa-bed'></i> Bed Kamar</a> </li>
                          <li>Pastikan semua penggunaan Bed Kamar telah di isi Baik <strong>Tanggal Masuk</strong> maupun <strong>Tanggal Keluarnya</strong>.</li>
                          <li>Jika Terdapat kamar yang masih belum di set baik tanggal masuk atau pun tanggal keluarnya, klik tombol edit <font class='btn btn-warning'><i class='fa fa-pencil'></i></font> </li>
                          <li>Setelah muncul dialog tekan Masukan waktu <strong>Masuk</strong> atau Waktu <strong>Keluar</strong> yang masih kosong.</li>
                          <li>Waktu Masuk atau Waktu Keluar yang masih kosong biasanya berisikan 0000-00-00 00:00:00, pastikan waktu masuk dan keluar tidak berisikan 0000-00-00 00:00:00  </li>
                          <li>Jika Sudah benar, klik tombol <font class='btn btn-warning'>Save dan Kosongkan</font> </li>
                      </ul>
                      Terima Kasih";
                      
          }else if($errno==self::$ERROR_OPERASI){
              return "Pasien Dengan Nomor RM <strong>".ArrayAdapter::format("only-digit8", $nrm)."</strong> Tidak Dapat dikeluarkan karena Masih Operasi yang Belum Di isi Baik Biaya Dokter Operator, Maupun Nama DOkter Operator-nya. 
                      <ul>
                          <li>silakan klik Tombol Hijau <font class='btn btn-success'><i class='fa fa-sign-in'></i></font></li>
                          <li>masuk ke Tab <a href='javascript:;' ><i class='fa fa-wrench'></i> Alat dan Obat</a> kemudian pilih tab <a href='javascript:;' ><i class='fa fa-renren'></i> Oksigen Central</a> </li>
                          <li>Pastikan semua penggunaan OKsigen Central telah di isi Baik <strong>Waktu Mulai</strong> maupun <strong>Waktu Selesainya</strong>.</li>
                          <li>Jika Terdapat Oksigen Manual yang masih belum di set baik waktu mulai atau pun tanggal waktu selesainya, klik tombol edit <font class='btn btn-warning'><i class='fa fa-pencil'></i></font> </li>
                          <li>Setelah muncul dialog tekan Masukan waktu <strong>Mulai</strong> atau Waktu <strong>Selesai</strong> yang masih kosong.</li>
                          <li>Waktu Mulai atau Waktu Selesai yang masih kosong biasanya berisikan 0000-00-00 00:00:00, pastikan waktu mulai dan selesai tidak berisikan 0000-00-00 00:00:00  </li>
                          <li>Jika Sudah benar, klik tombol <font class='btn btn-primary'><i class='fa fa-save'></i></font> </li>							
                      </ul>
                      Terima Kasih";
          }else if($errno==self::$ERROR_OKSIGEN){
              return "Pasien Dengan Nomor RM <strong>".ArrayAdapter::format("only-digit8", $nrm)."</strong> Tidak Dapat dikeluarkan karena Masih Terdapat Penggunaan Oksigen Central yang mungkin Belum di set Mulai atau Selesainya. 
                      <ul>
                          <li>silakan klik Tombol Hijau <font class='btn btn-success'><i class='fa fa-sign-in'></i></font></li>
                          <li>masuk ke Tab <a href='javascript:;' ><i class='fa fa-heartbeat'></i> Pelayanan</a> kemudian pilih tab <a href='javascript:;' ><i class='fa fa-cut'></i> Kamar Operasi</a> </li>
                          <li>Pastikan semua <strong>Biaya Dokter Operator</strong> Tidak Boleh 0 dan <strong>Nama Dokter Operatornya</strong> Telah Terisi.</li>
                          <li>Jika Terdapat Operasi yang masih belum di set baik Biaya Dokter mulai atau Nama Dokternya, klik tombol edit <font class='btn btn-warning'><i class='fa fa-pencil'></i></font> </li>
                          <li>Setelah muncul dialog tekan Masukan <strong>Biaya Dokter</strong> dan <strong>Nama Dokter</strong> yang masih kosong. (Minimal Nama dan Biaya Dokter Operator Satu) </li>
                          <li>Jika Sudah benar, klik tombol <font class='btn btn-primary'><i class='fa fa-save'></i></font> </li>							
                      </ul>
                      Terima Kasih";
          }else if($errno==self::$ERROR_PLEBITIS){
              return "Pasien Dengan Nomor RM <strong>".ArrayAdapter::format("only-digit8", $nrm)."</strong> Tidak Dapat dikeluarkan karena Data Plebitis Belum Lengkap 
                      <div class='alert alert-error'>".self::$RM_ERRNO."</div>
                      <ul>
                          <li>silakan klik Tombol Hijau <font class='btn btn-success'><i class='fa fa-sign-in'></i></font></li>
                          <li>masuk ke Tab <a href='javascript:;' ><i class='fa fa-area-chart'></i> Laporan</a> kemudian pilih tab <a href='javascript:;' ><i class='fa fa-list'></i> Plebitis RM-35</a> </li>
                          <li>Pastikan data <strong>Plebitis</strong> sudah terisi setiap hari selama perawatan (tidak boleh kurang 1 hari pun).</li>							
                      </ul>
                      Terima Kasih";
          }else if($errno==self::$ERROR_CAUTI){
              return "Pasien Dengan Nomor RM <strong>".ArrayAdapter::format("only-digit8", $nrm)."</strong> Tidak Dapat dikeluarkan karena Data Ca-UTI Belum Lengkap 
                      <div class='alert alert-error'>".self::$RM_ERRNO."</div>
                      <ul>
                          <li>silakan klik Tombol Hijau <font class='btn btn-success'><i class='fa fa-sign-in'></i></font></li>
                          <li>masuk ke Tab <a href='javascript:;' ><i class='fa fa-area-chart'></i> Laporan</a> kemudian pilih tab <a href='javascript:;' ><i class='fa fa-snowflake'></i> Cauti RM-36</a> </li>
                          <li>Pastikan data <strong>Plebitis</strong> sudah terisi setiap hari selama perawatan (tidak boleh kurang 1 hari pun).</li>							
                      </ul>
                      Terima Kasih";
          }else if($errno==self::$ERROR_DIAGNOSA){
              return "Pasien Dengan Nomor RM <strong>".ArrayAdapter::format("only-digit8", $nrm)."</strong> Tidak Dapat dikeluarkan karena Data Diagnosa Tidak Lengkap 
                      <div class='alert alert-error'>".self::$RM_ERRNO."</div>
                      <ul>
                          <li>silakan klik Tombol Hijau <font class='btn btn-success'><i class='fa fa-sign-in'></i></font></li>
                          <li>masuk ke Tab <a href='javascript:;' ><i class='fa fa-heartbeat'></i> Pelayanan</a> kemudian pilih tab <a href='javascript:;' ><i class='fa fa-stethoscope'></i> Diagnosa</a> </li>
                          <li>Pastikan data <strong>Diagnosa</strong> sudah terisi untuk Nomor Registrasi .".$noreg_pasien.".</li>							
                      </ul>
                      Terima Kasih";
          }
      } 
}

?>