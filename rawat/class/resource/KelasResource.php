<?php

class KelasResource{
    public static $kelas_opti;
    public static $kelas_serv;
    public static $kelas_defl;
    public static $kelas_grup;
    
    public static function getKelasOption(){
        if(self::$kelas_opti==null){
            $opsi_kelas = new OptionBuilder();
            $opsi_kelas ->add("Sesuai Kelas Ruangan","0","1")
                        ->add("Sesuai Kelas Asal Pasien","1","0")
                        ->add("Pilih Sendiri Kelas","-1","0");
            $kelas              = self::getKelas();
            foreach($kelas as $k){
                $nama = $k ['nama'];
                $slug = $k ['slug'];
                $opsi_kelas->add ($nama,$slug);
            }
            self::$kelas_opti =  $opsi_kelas;
        }
        return self::$kelas_opti;
    }

    public static function getKelas(){
        if(self::$kelas_serv == null ){
            global $db;
            $service = new ServiceConsumer($db, "get_kelas");
            $service->setCached(true,"get_kelas");
            $service->execute ();
            self::$kelas_serv = $service->getContent ();
        }
        return self::$kelas_serv;
    }

    public static function getKelasOptionDefault($kelas_df=""){
        if(self::$kelas_defl==null){
            $option_kelas       = new OptionBuilder ();
            $kelas              = self::getKelas();
            foreach($kelas as $k){
                $nama           = $k ['nama'];
                $slug           = $k ['slug'];
                $option_kelas   ->add($nama,$slug,$slug == $kelas_df?"1":"0");
            }
            self::$kelas_defl = $option_kelas;
        }
        return self::$kelas_defl;
    }

    public static function getKelasOptionGroup($kelas_df){
        if(self::$kelas_grup==null){
            $option_kelas_grup  = new OptionBuilder ();
            $kelas              = self::getKelas();
            foreach($kelas as $k){
                $nama           = $k ['nama'];
                $slug           = $k ['slug'];
                $option_kelas_grup->add($nama,$slug,$slug == $kelas_df?"1":"0");
            }
            $option_kelas_grup->add("Non Kelas","non_kelas");
            self::$kelas_grup = $option_kelas_grup;
        }
        return self::$kelas_grup;
    }


}

?>