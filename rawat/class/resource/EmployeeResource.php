<?php

class EmployeeResource{
    private static $perawat;
    private static $dokter;
    
    public static function getJumlahPerawat(){
        if(self::$perawat==null){
            $perawat = new OptionBuilder();
            $perawat ->add("0 Perawat","0","0")
                     ->add("1 Perawat","1","1")
                     ->add("2 Perawat","2","0")
                     ->add("3 Perawat","3","0")
                     ->add("4 Perawat","4","0")
                     ->add("5 Perawat","5","0")
                     ->add("6 Perawat","6","0")
                     ->add("7 Perawat","7","0")
                     ->add("8 Perawat","8","0")
                     ->add("9 Perawat","9","0")
                     ->add("10 Perawat","10","0");
            self::$perawat = $perawat;
        }
        return self::$perawat;
    }

    public static function getJumlahDokter(){
        if(self::$dokter==null){
            $dokter = new OptionBuilder();
            $dokter ->add("1 Dokter","1","1")
                    ->add("2 Dokter","2","0")
                    ->add("3 Dokter","3","0");
            self::$dokter = $dokter;
        }
        return self::$dokter;
    }

}

?>


