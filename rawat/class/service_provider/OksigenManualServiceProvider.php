<?php 

class OksigenManualServiceProvider extends ServiceProvider {
    
    private $polislug;
    public function __construct(DBTable $dbtable,$polislug){
        parent::__construct($dbtable);
        $this->polislug=$polislug;
    }
    
	public function postToArray() {
		$data = parent::postToArray ();
		require_once 'rawat/class/service_consumer/OksigenService.php';
		$oksigen = new OksigenService ( $db, $this->polislug );
		$oksigen->execute ();
		$liter = $oksigen->getLiter ();

		$data ['harga_liter'] = $liter;
		$data ['harga'] = $_POST ['jumlah_liter'] * $data ['harga_liter'] + $_POST ['biaya_lain'];
		return $data;
	}
}

?>