<?php 

class ServiceTableOperator{
    /*@var DBTable*/
    protected $dbtable;
    
    
    public function __construct(Database $db,$name,$polislug){
        $adapter = null;
        $dbtable = new TindakanDBTable($db, $name);
        $jenis = "";
        if(startsWith($name,"smis_rwt_ok_")){
            require_once "rawat/class/adapter/TagihanOKAdapter.php";
            $adapter = new TagihanOKAdapter ($db,$polislug);
            $adapter ->setAlwaysSHow(true);
        }else{
            require_once "rawat/class/adapter/TagihanAdapter.php";
            $adapter = new TagihanAdapter ($db,$polislug);
            if(startsWith($name,"smis_rwt_visite_")){
                $jenis = "visite_dokter";
            }else if(startsWith($name,"smis_rwt_konsul_")){
                $jenis = "konsultasi_dokter";
            }else if(startsWith($name,"smis_rwt_konsultasi_")){
                $jenis = "konsultasi_dokter";
            }else if(startsWith($name,"smis_rwt_tindakan_perawat_")){
                $jenis = "tindakan_perawat";
            }else if(startsWith($name,"smis_rwt_tindakan_igd_")){
                $jenis = "tindakan_perawat";
            }else if(startsWith($name,"smis_rwt_tindakan_dokter_")){
                $jenis = "tindakan_perawat";
            }else if(startsWith($name,"smis_rwt_bed_")){
                $jenis = "bed";
            }else if(startsWith($name,"smis_rwt_oksigen_central_")){
                $jenis = "oksigen_central";
            }else if(startsWith($name,"smis_rwt_oksigen_manual_")){
                $jenis = "oksigen_manual";
            }else if(startsWith($name,"smis_rwt_alok_")){
                $jenis = "alok";
            }else if(startsWith($name,"smis_rwt_rr_")){
                $jenis = "rr";
            }else if(startsWith($name,"smis_rwt_vk_")){
                $jenis = "vk";
            }
            $adapter ->setAdapterName($jenis);
        }
        
        $dbtable ->setAutoSynch(true);
        $dbtable ->setAdapter($adapter);
        $dbtable ->setEntity($polislug);
        $dbtable ->setJenis($jenis);

        $this->dbtable = $dbtable;
    }
    
    
    public function command($command){
        $hasil = null;
        if($command=="insert"){
            $hasil = $this->insert();
        }else if($command=="update"){
            $hasil = $this->update();
        }else if($command=="view"){
            $hasil = $this->view();
        }else if($command=="query"){
            $hasil = $this->query();
        }else if($command=="get_result"){
            $hasil = $this->get_result();
        }else if($command=="get_row"){
            $hasil = $this->get_row();
        }else if($command=="get_var"){
            $hasil = $this->get_var();
        }
        return $hasil;
    }
    
    public function insert(){
        $data       = $_POST['data'];
        $use_prop   = $_POST['use_prop'];
        $warp       = $_POST['warp'];
        $result     = $this->dbtable->insert($data,$use_prop,$warp);
        return $this->dbtable->get_inserted_id();
    }
    public function update(){
        $data   = $_POST['data'];
        $id     = $_POST['id'];
        $warp   = $_POST['warp'];
        $result = $this->dbtable->update($data,$id,$warp);
        return $id;
    }
    public function view(){
        $term = $_POST['term'];
        $page = $_POST['page'];
        $custome = $_POST['custom_kriteria'];
        $max = $_POST['max'];
        $this->dbtable->setMaximum($max);
        $this->dbtable->setCustomKriteria($custome);
        return $this->dbtable->view($term,$page);            
    }
    public function query(){
        $query = $_POST['query'];
        return $this->dbtable->query($query);
    }
    public function get_result(){
        $query = $_POST['query'];
        return $this->dbtable->get_result($query);
    }
    public function get_row(){
        $query = $_POST['query'];
        return $this->dbtable->get_row($query);    
    }
    public function get_var(){
        $query = $_POST['query'];
        return $this->dbtable->get_var($query);    
    }
}

?>