<?php 

class OksigenCentralServiceProvider extends ServiceProvider{
    
    private $polislug;
    public function __construct(DBTable $dbtable,$polislug){
        parent::__construct($dbtable);
        $this->polislug=$polislug;
    }
    
    public function postToArray() {
		$data = parent::postToArray ();
		$mulai = strlen ( $_POST ['mulai'] ) == 16 ? $_POST ['mulai'] . ":00" : $_POST ['mulai'];
		$selesai = strlen ( $_POST ['selesai'] ) == 16 ? $_POST ['selesai'] . ":00" : $_POST ['selesai'];
		if($selesai=="0000-00-00 00:00:00" || $_POST ['selesai']=="" || $mulai=="0000-00-00 00:00:00" || $_POST ['mulai']==""){
			$data ['jam'] = "Belum Selesai";
			$data ['menit'] = "Belum Selesai";
		}else{
            loadLibrary("smis-libs-function-time");
			$selisih = date_difference ( $mulai, $selesai );
			$data ['jam'] = $selisih ['hours'];
			$data ['menit'] = $selisih ['mins'];
		}
		
		require_once 'rawat/class/service_consumer/OksigenService.php';
		$oksigen = new OksigenService ( $db, $this->polislug );
		$oksigen->execute ();
		$jam = $oksigen->getJam ();
		$menit = $oksigen->getMenit ();

		$data ['harga_jam'] = $jam;
		$data ['harga_menit'] = $menit;
		
		if($selesai=="0000-00-00 00:00:00" || $_POST ['selesai']=="" || $mulai=="0000-00-00 00:00:00" || $_POST ['mulai']=="" ){
			$data ['harga']="0";
		}else{
			$data ['harga'] = $data ['jam'] * $data ['harga_jam'] * $data ['skala'] + $data ['menit'] * $data ['harga_menit'] * $data ['skala'];
		}
		return $data;
	}
}

?>