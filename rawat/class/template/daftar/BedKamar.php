<?php 

require_once ("smis-base/smis-include-service-consumer.php");
class BedKamar extends ModulTemplate {
	private $poliname;
	private $polislug;
	private $uitable;
	private $dbres;
	private $dbtable;
	private $db;
	public function __construct($db, $polislug, $poliname) {
		parent::__construct ();
		$this->polislug = $polislug;
		$this->poliname = $poliname;
		$this->db = $db;
		$header=array ('Nama','Kelas Aplicare','Status Bed','Keterangan',"Pasien","NRM","No Reg");
		$this->uitable = new Table ( $header, "Bed Kamar " . $poliname, NULL, true );
		$btn = new Button ( "", "", "Kosongkan" );
		$btn->setClass ( "btn-success" );
		$btn->setIsButton ( Button::$ICONIC );
		$btn->setIcon ( "icon-white icon-trash" );
		$this->uitable->addContentButton ( "kosongkan", $btn );
		
		$btn = new Button ( "", "", "Booking" );
		$btn->setClass ( "btn-success" );
		$btn->setIsButton ( Button::$ICONIC );
		$btn->setIcon ( "fa fa-book" );
		$this->uitable->addContentButton ( "booking", $btn );
		
		$btn = new Button ( "", "", "Un Booking" );
		$btn->setClass ( "btn-danger" );
		$btn->setIsButton ( Button::$ICONIC );
		$btn->setIcon ( "fa fa-book" );
		$this->uitable->addContentButton ( "unbooking", $btn );
		
		$this->uitable->setMaxContentButton(10,"");
		
		$this->uitable->setName ( "bed_kamar" );
	}
	public function command($command) {

        
        require_once "rawat/class/adapter/DaftarBedKamarAdapter.php";
		$adapter = new DaftarBedKamarAdapter ();
		$adapter->add ( "Nama", "nama" );
        $adapter->add ( "Pasien", "nama_pasien" );
        $adapter->add ( "NRM", "nrm_pasien", "digit8" );
		$adapter->add ( "NRM", "nrm_pasien", "digit8" );
		$adapter->add ( "No Reg", "noreg_pasien","digit8" );
		$adapter->add ( "Status Bed", "terpakai", "trivial_1_Terpakai_Kosong" );
        $adapter->add ( "Keterangan", "keterangan" );
        
        
		$this->dbtable = new DBTable ( $this->db, "smis_rwt_bed_kamar_" . $this->polislug );
		if(isset($_POST['urutan']) && $_POST['urutan']!=""){
			$this->dbtable->setOrder ($_POST['urutan']);
        }
        
        
		
		$this->dbres = new DBResponder ( $this->dbtable, $this->uitable, $adapter );
		$data = $this->dbres->command ( $_POST ['command'] );
		echo json_encode ( $data );
        
        if($this->dbres->isSave() || $this->dbres->isDel()){
            require_once "rawat/function/update_available_beds.php";
            update_available_beds($this->polislug );
        }

        return;
	}
	public function jsLoader() {
		echo addJS ( "framework/smis/js/table_action.js" );
	}
	public function phpPreload() {
		$pakai = array (
				array (
						"name" => "Kosong",
						"value" => "0",
						"default" => "1"
				),
				array (
						"name" => "Terpakai",
						"value" => "1"
				)
        );
        
        

		global $db;
		$urutan = new OptionBuilder();
		$urutan ->add("Urut By Noreg","noreg_pasien ASC","1");
		$urutan ->add("Nama","nama ASC","0");
        $urutan ->add("Status Bed","terpakai ASC","0");
        
        $dbtable = new DBTable($db,"smis_adm_prototype");
        $protot = $dbtable ->select(array("slug"=>$this->polislug));

		$this->uitable->addModal("urutan","select","Urutan",$urutan->getContent());
		$form = $this->uitable->getModal()->getForm();

		$btn = new Button("","","");
		$btn ->setIcon(" fa fa-search");
		$btn ->setClass("btn-primary");
		$btn ->setIsButton(Button::$ICONIC_TEXT);
		$btn ->setAction("bed_kamar.view()");
		$form ->addElement("",$btn);
		$this->uitable->clearContent();
		$this->uitable->addModal("id","hidden","","");
        $this->uitable->addModal("nama","text","Nama","");


        $aplicare = new OptionBuilder();
        $aplicare ->add("","",$protot->kelas_aplicare==""?"1":"0");
        $aplicare ->add("-","NON",$protot->kelas_aplicare=="NON"?"1":"0");
        $aplicare ->add("VVIP","VVP",$protot->kelas_aplicare=="VVP"?"1":"0");
        $aplicare ->add("VIP","VIP",$protot->kelas_aplicare=="VIP"?"1":"0");
        $aplicare ->add("UTAMA","UTM",$protot->kelas_aplicare=="UTM"?"1":"0");
        $aplicare ->add("KELAS I","KL1",$protot->kelas_aplicare=="KL1"?"1":"0");
        $aplicare ->add("KELAS II","KL2",$protot->kelas_aplicare=="KL2"?"1":"0");
        $aplicare ->add("KELAS III","KL3",$protot->kelas_aplicare=="KL3"?"1":"0");
        $aplicare ->add("ICU","ICU",$protot->kelas_aplicare=="ICU"?"1":"0");
        $aplicare ->add("ICCU","ICC",$protot->kelas_aplicare=="ICC"?"1":"0");
        $aplicare ->add("NICU","NIC",$protot->kelas_aplicare=="NIC"?"1":"0");
        $aplicare ->add("PICU","PIC",$protot->kelas_aplicare=="PIC"?"1":"0");
        $aplicare ->add("IGD","IGD",$protot->kelas_aplicare=="IGD"?"1":"0");
        $aplicare ->add("UGD","UGD",$protot->kelas_aplicare=="UGD"?"1":"0");
        $aplicare ->add("RUANG BERSALIN","SAL",$protot->kelas_aplicare=="SAL"?"1":"0");
        $aplicare ->add("HCU","HCU",$protot->kelas_aplicare=="HCU"?"1":"0");
        $aplicare ->add("RUANG ISOLASI","ISO",$protot->kelas_aplicare=="ISO"?"1":"0");


        if($protot->jenis_kelas*1==1){
            $this->uitable->addModal("kelas_aplicare","select","Kelas Aplicare",$aplicare->getContent(),"n",null,true);
        }else{
            $this->uitable->addModal("kelas_aplicare","select","Kelas Aplicare",$aplicare->getContent());
        }
        
        
		$modal = $this->uitable->getModal ();
		$modal->setTitle ( "Bed Kamar " . $this->poliname );

		echo $form->getHtml ();
		echo $this->uitable->getHtml ();
		echo $modal->getHtml ();
	}
	public function jsPreLoad() {
		?>
				<script type="text/javascript">
				var bed_kamar;
				$(document).ready(function(){
					$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
					var column=new Array('id','nama',"kelas_aplicare");
					bed_kamar=new TableAction("bed_kamar","<?php echo $this->polislug;?>","bed_kamar",column);
					bed_kamar.setPrototipe("<?php echo $this->poliname ?>","<?php echo $this->polislug ?>","rawat");
					bed_kamar.addViewData=function(u){
						u['urutan']=bed_kamar.get("urutan");
						return u;
					};
					bed_kamar.kosongkan=function(id){
						var self=this;						
						bootbox.confirm("Anda Yakin Mengosongkan Bed ini. Bed tidak seharusnya dikosongkan dengan cara ini. cukup memulangkan pasien saja. ?", function(result) {
							if(result){
								var a=self.getRegulerData();
								a['command']="save";
								a['id']=id;
								a['nama_pasien']="";
								a['terpakai']="0";
								a['nrm_pasien']="";
								a['noreg_pasien']="";								
								showLoading();
								$.ajax({url:"",data:a,type:'post',success:function(res){
									var json=getContent(res);
									if(json==null) return;
									self.view();
									self.clear();
									dismissLoading();
								}});
							}
						});
					};
					
					bed_kamar.unbooking=function(id){
						var self=this;
						bootbox.confirm("Anda yakin mau menghilangkan Booking ?", function(result) {
							if(result){
								var a=self.getRegulerData();
								a['command']="save";
								a['id']=id;
								a['keterangan']="";							
								showLoading();
								$.ajax({url:"",data:a,type:'post',success:function(res){
									var json=getContent(res);
									if(json==null) return;
									self.view();
									self.clear();
									dismissLoading();
								}});
							}							
						});						
					};
					
					bed_kamar.booking=function(id){
						var self=this;
						bootbox.prompt("Nama Pasien yang Booking ?", function(result) {                
						  if (result !== null) {                                  								
							var a=self.getRegulerData();
							a['command']="save";
							a['id']=id;
							a['keterangan']="di Booking Oleh "+result;
							showLoading();
							$.ajax({url:"",data:a,type:'post',success:function(res){
								var json=getContent(res);
								if(json==null) return;
								self.view();
								self.clear();
								dismissLoading();
							}});							  
						  } 
						});
					};
					bed_kamar.view();
				});
			</script>
		<?php
	}
}


?>