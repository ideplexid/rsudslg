<?php 
require_once ("smis-base/smis-include-service-consumer.php");
class pulangmassal extends ModulTemplate {
	private $poliname;
	private $polislug;
	private $uitable;
	private $dbres;
	private $dbtable;
	private $db;
	private $mode;
	public function __construct($db, $polislug, $poliname) {
		parent::__construct ();
		$this->polislug = $polislug;
		$this->poliname = $poliname;
		$this->db = $db;
		$header=array ("No.",'Waktu','No.Reg','Nama','NRM','Carabayar',"Status");
		$this->uitable = new Table ( $header , "", NULL, true );
		$this->uitable->setName ( "pulangmassal" );
		
        $this->uitable->setReloadButtonEnable(false);
		$this->uitable->setPrintButtonEnable(false);
        $this->uitable->setDelButtonEnable(false);
        $this->uitable->setAddButtonEnable(false);  
        
        
	}
	public function command($command) {
        require_once "rawat/class/responder/PulangMassalResponder.php";
		$adapter = new SimpleAdapter ();
        $adapter->setUseNumber(true,"No.","back.");
		$adapter->add ( "Nama", "nama_pasien" );
		$adapter->add ( "Waktu", "waktu", "date d M Y H:i" );
		$adapter->add ( "No.Reg", "no_register", "only-digit8" );
		$adapter->add ( "NRM", "nrm_pasien","only-digit8" );
		$adapter->add ( "Carabayar", "carabayar","unslug" );
        $adapter->add ( "Status", "selesai","trivial_0_Aktif_Pulang" );
		$this->dbtable = new DBTable ( $this->db, "smis_rwt_antrian_" . $this->polislug );
		$this->dbtable->setOrder ( "id ASC" );
		$this->dbres = new PulangMassalResponder ( $this->dbtable, $this->uitable, $adapter );
        if($this->dbres->isView() || $_POST['command']=="pulangkan" ||  $_POST['command']=="aktifkan"){
            if(isset($_POST['dari']) && $_POST['dari']!="")                     $this->dbtable->addCustomKriteria(NULL," waktu>='".$_POST['dari']."' ");
            if(isset($_POST['sampai']) && $_POST['sampai']!="")                 $this->dbtable->addCustomKriteria(NULL," waktu<'".$_POST['sampai']."' ");
            if(isset($_POST['status_keluar']) && $_POST['status_keluar']!="")   $this->dbtable->addCustomKriteria(NULL," selesai='".$_POST['status_keluar']."' ");
            if( isset($_POST['aksi_filter']) && $_POST['aksi_filter']!="" 
                && isset($_POST['aksi_grup']) && $_POST['aksi_grup']!="" ){
                $grup_nrm=explode(" ",$_POST['aksi_grup']);
                /* kode 1 untuk aksi filter adalah kode kecuali, sehinga operandya adalah !=, selain itu yakni 0 operandya adalah = */
                $operand=$_POST['aksi_filter']=="1"?"!=":"=";
                $operand_logic=$_POST['aksi_filter']=="1"?"AND":"OR";
                $logic=array();
                foreach($grup_nrm as $nrm){
                    $logic[]="nrm_pasien".$operand."'".$nrm."'";
                }
                $kriteria="(".implode(" ".$operand_logic." ",$logic).")";
                $this->dbtable->addCustomKriteria(NULL,$kriteria);
            }
        }
		$data = $this->dbres->command ( $_POST ['command'] );
		echo json_encode ( $data );
		return;
	}
	

	// load css and javascript before loading
	public function jsLoader() {
		echo addJS ( "framework/smis/js/table_action.js" );
	}

	/* when it's star build */
	public function phpPreload() {
        $selesai=new OptionBuilder();
        $selesai->add("","","1");
        $selesai->add("Pulang","1","0");
        $selesai->add("Aktif","0","0");
        
        $grup=new OptionBuilder();
        $grup->add("","","1");
        $grup->add("Kecuali","1","0");
        $grup->add("Termasuk","0","0");
        
        
        $this->uitable->addModal ( "status_keluar", "select", "Status", $selesai->getContent() );
		$this->uitable->addModal ( "dari", "date", "Dari", "" );
        $this->uitable->addModal ( "sampai", "date", "Sampai", "");
        $this->uitable->addModal ( "aksi_filter", "select", "Filter NRM", $grup->getContent() );
		$this->uitable->addModal ( "aksi_grup", "text", "Grup NRM", "" );
		$form=$this->uitable->getModal()->getForm();
        $form->setTitle("Pasien Ruangan ".$this->poliname);
        
        $tampil=new Button("","","Tampilkan");
        $tampil->addClass("btn-primary");
        $tampil->setIsButton(Button::$ICONIC_TEXT);
        $tampil->setIcon(" fa fa-refresh");
        $tampil->setAction("pulangmassal.view()");
        $form->addElement("",$tampil);
        
        $aktif=new Button("","","Aktifkan");
        $aktif->addClass("btn-primary");
        $aktif->setIsButton(Button::$ICONIC_TEXT);
        $aktif->setIcon(" fa fa-sign-in");
        $aktif->setAction("pulangmassal.aktifkan()");
        $form->addElement("",$aktif);
        
        
        $pulang=new Button("","","Pulangkan");
        $pulang->addClass("btn-primary");
        $pulang->setIsButton(Button::$ICONIC_TEXT);
        $pulang->setIcon(" fa fa-sign-out");
        $pulang->setAction("pulangmassal.pulangkan()");
        $form->addElement("",$pulang);
        
        
        $this->uitable->clearContent();
        loadLibrary("smis-libs-function-medical");	
		$this->uitable->addModal ( "id", "hidden", "", "" );
		$this->uitable->addModal ( "nama_pasien", "text", "Nama Pasien", "","y",null,true );
        $this->uitable->addModal("selesai", "checkbox", "Selesai", "");
		$this->uitable->addModal("waktu_keluar", "datetime", "Tanggal Keluar", date("Y-m-d H:i"),"n");
		$this->uitable->addModal("cara_keluar", "select", "Keluar", medical_carapulang());
		$this->uitable->addModal("keterangan_keluar", "textarea", "Keterangan", "");
		$modal = $this->uitable->getModal ();
		$modal->setTitle ( " Pasien " . $this->poliname );
        $modal->setComponentSize(Modal::$MEDIUM);
		
        $proto_nama         = new Hidden("pulangmassal_proto_name","",$_POST['prototype_name']);
		$proto_slug         = new Hidden("pulangmassal_proto_slug","",$_POST['prototype_slug']);
		$proto_implement    = new Hidden("pulangmassal_proto_implement","",$_POST['prototype_implement']);
		$poliname           = new Hidden("pulangmassal_poliname","",$this->poliname);
		$polislug           = new Hidden("pulangmassal_polislug","",$this->polislug);
		
		echo $proto_nama->getHtml();
		echo $proto_slug->getHtml();
		echo $proto_implement->getHtml();
		echo $poliname->getHtml();
		echo $polislug->getHtml();
        echo $form->getHtml();
        echo $this->uitable->getHtml ();
		echo $modal->getHtml ();
        echo addJS ( "framework/bootstrap/js/bootstrap-datetimepicker.js" );
		echo addCSS ( "framework/bootstrap/css/bootstrap-datetimepicker.css" );
        echo addJS ( "framework/bootstrap/js/bootstrap-datepicker.js" );
		echo addCSS ( "framework/bootstrap/css/datepicker.css" );
        echo addJS ( "rawat/resource/js/pulangmassal.js",false );
	}
	
}

?>