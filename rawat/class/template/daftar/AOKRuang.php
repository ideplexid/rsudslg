<?php 
require_once ("smis-base/smis-include-service-consumer.php");
class AOKRuang extends ModulTemplate {
	private $poliname;
	private $polislug;
	private $uitable;
	private $dbres;
	private $dbtable;
	private $db;
	private $mode;
	public function __construct($db, $polislug, $poliname) {
		parent::__construct ();
		$this->polislug = $polislug;
		$this->poliname = $poliname;
		$this->db = $db;
		$header=array ('Nama','Harga');
		$this->uitable = new Table ( $header , "Alat Obat Kesehatan " . $poliname, NULL, true );
		$this->uitable->setName ( "aokruang" );
		
		$btn=new Button("","","");
		$btn->setIsButton(Button::$ICONIC);
		$btn->setClass("btn-inverse");
		$btn->setAction("aokruang.reclear()");
		$btn->setIcon(" fa fa-trash");
		$this->uitable->addHeaderButton($btn);		
		$this->uitable->setReloadButtonEnable(false);
		$this->uitable->setPrintButtonEnable(false);
	}
	public function command($command) {
		require_once "rawat/class/responder/AOKRuangResponder.php";
		$adapter = new SimpleAdapter ();
		$adapter->add ( "Nama", "nama" );
		$adapter->add ( "Harga", "harga", "money Rp." );
		$adapter->add ( "Stok", "stok", "number" );
		$adapter->add ( "Satuan", "satuan" );
		$adapter->add ( "Kode", "kode" );
		$adapter->add ( "BOI", "connect","trivial_1_<i class='fa fa-check'></i>_" );
		$this->dbtable = new DBTable ( $this->db, "smis_rwt_aok_" . $this->polislug );
		$this->dbtable->setOrder ( "nama ASC" );
		$this->dbtable->addCustomKriteria("connect","=0");
		$this->dbres = new AOKRuangResponder ( $this->dbtable, $this->uitable, $adapter );
		$data = $this->dbres->command ( $_POST ['command'] );
		echo json_encode ( $data );
		return;
	}
	public function superCommand($super_command) {
		$head=array ('Nama','Satuan',"ID Obat","HNA" );
		$dktable = new Table ( $head);
		$dktable->setName ( "list_aokruang_obat" );
		$dktable->setModel ( Table::$SELECT );
		$dkadapter = new SimpleAdapter ();
		$dkadapter->add ( "Nama", "nama_dokter" );
		$dkadapter->add ( "Jabatan", "jabatan", "unslug" );
		$dkadapter->add ( "Kelas", "kelas", "unslug" );
		$dkadapter->add ( "Tarif", "tarif", "money Rp." );
		$list_aokruang_obat = new ServiceResponder ( $this->db, $dktable, $dkadapter, "get_list_obat" );
		$super = new SuperCommand ();
		$super->addResponder ( "list_aokruang_obat", $list_aokruang_obat );
		$init = $super->initialize ();
		if ($init != null) {
			echo $init;
			return;
		}
	}

	// load css and javascript before loading
	public function jsLoader() {
		echo addJS ( "framework/smis/js/table_action.js" );
	}

	/* when it's star build */
	public function phpPreload() {
		$this->uitable->addModal ( "id", "hidden", "", "" );
		$this->uitable->addModal ( "kode", "hidden", "", "" );
		$this->uitable->addModal ( "connect", "hidden", "", "0" );
		$this->uitable->addModal ( "nama", "text", "Nama", "" );
		$this->uitable->addModal ( "id_obat", "hidden", "", "","y",NULL,true );
		$this->uitable->addModal ( "satuan", "text", "Satuan", "Layanan","y",NULL,true );
		$this->uitable->addModal ( "all_satuan", "hidden", "", "" );
		$this->uitable->addModal ( "kategori", "text", "Kategori", "Non Obat Gudang","n",NULL,true );
		$this->uitable->addModal ( "harga_asli", "money", "Harga Asli", "" );
		$this->uitable->addModal ( "kelipatan", "hidden", "", "1","n" );
		$this->uitable->addModal ( "harga", "hidden", "", "" );
		$this->uitable->addModal ( "stok", "hidden", "","" );
		$modal = $this->uitable->getModal ();
		$modal->setTitle ( "Alat Obat " . $this->poliname );
		echo $this->uitable->getHtml ();
		echo $modal->getHtml ();
	}
	
	public function jsPreLoad() {
		?>
		<script type="text/javascript">
				var aokruang;
				var list_aokruang_obat;

				function appenlist(list){
					$("#aokruang_satuan").html("");
					try{
						var asatuan=$.parseJSON("["+list+"]");
						var lsatuan=asatuan[0];
						$.each(lsatuan,function(index,val){
							$("#aokruang_satuan").append('<option value='+val+'>'+val+'</option>');
						});
					}catch(e){
						console.log(e);
					}
				}

				
				
				$(document).ready(function(){

					$("#aokruang_connect").on("change",function(){
						if($("#aokruang_connect").is(":checked")){
							$(".aokruang_harga_asli, .aokruang_harga ").hide("fast");
							$("#aokruang_chooser_nama").attr('onclick', "aokruang.chooser('aokruang','aokruang_nama','list_aokruang_obat',list_aokruang_obat,'list_aokruang_obat')");
						}else{
							$("#aokruang_chooser_nama").attr('onclick',"");
							$(".aokruang_harga_asli, .aokruang_harga ").show("fast");
						}
					});

					$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
					$("#aokruang_harga_asli, #aokruang_kelipatan").on("change",function(){
						var uang=getMoney("#aokruang_harga_asli");
						$("#aokruang_harga").val(uang);
					});
					
					var column=new Array('id','nama',"id_obat","connect","harga_asli","kelipatan",'satuan','all_satuan','kategori',"stok","harga","kode");
					aokruang=new TableAction("aokruang","<?php echo $this->polislug;?>","aokruang",column);
					aokruang.setPrototipe("<?php echo $this->poliname ?>","<?php echo $this->polislug ?>","rawat");
					aokruang.edit=function (id){
						var self=this;
						showLoading();	
						var edit_data=this.getEditData(id);
						$.post('',edit_data,function(res){		
							var json=getContent(res);
							if(json==null) return;
							appenlist(json.all_satuan);							
							for(var i=0;i<self.column.length;i++){
								var name=self.column[i];
								var the_id="#"+self.prefix+"_"+name;
								smis_edit(the_id,json[""+name]);
							}
							dismissLoading();
							self.disabledOnEdit(self.column_disabled_on_edit);
							self.show_form();
						});
					};
					
					aokruang.reclear=function (id){
						var self=this;
						showLoading();	
						var edit_data=this.getRegulerData(id);
						edit_data['command']="reclear";
						$.post('',edit_data,function(res){		
							var json=getContent(res);
							self.view();
							dismissLoading();
						});
					};

					aokruang.clear=function(){	
						$("#aokruang_satuan").html("");
						for(var i=0;i<this.column.length;i++){
							var the_id="#"+this.prefix+"_"+this.column[i];
							smis_clear(the_id);
						}
						this.enabledOnNotEdit(this.column_disabled_on_edit);
					};
					
					aokruang.view();
					

					list_aokruang_obat=new TableAction("list_aokruang_obat","<?php echo $this->polislug ?>","aokruang",new Array());
					list_aokruang_obat.setPrototipe("<?php echo $this->poliname ?>","<?php echo $this->polislug ?>","rawat");
					list_aokruang_obat.setSuperCommand("list_aokruang_obat");
					list_aokruang_obat.selected=function(json){
						$("#aokruang_nama").val(json.nama);
						$("#aokruang_id_obat").val(json.id);						
						$("#aokruang_satuan").val(json.satuan);
						appenlist(json.satuan);
					};					
				});
			</script>
<?php
	}
}

?>