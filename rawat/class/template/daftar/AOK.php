<?php 
require_once ("smis-base/smis-include-service-consumer.php");
class AOK extends ModulTemplate {
	private $poliname;
	private $polislug;
	private $uitable;
	private $dbres;
	private $dbtable;
	private $db;
	private $mode;
	public function __construct($db, $polislug, $poliname) {
		parent::__construct ();
		$this->polislug = $polislug;
		$this->poliname = $poliname;
		$this->db = $db;
		$header=array ('Nama','Harga');
		$this->uitable = new Table ( $header , "Alat Obat Kesehatan " . $poliname, NULL, true );
		$this->uitable->setName ( "aok" );
	}
	public function command($command) {
		$adapter = new SimpleAdapter ();
		$adapter->add ( "Nama", "nama" );
		$adapter->add ( "Harga", "harga", "money Rp." );
		$adapter->add ( "Stok", "stok", "number" );
		$adapter->add ( "Satuan", "satuan" );
		$adapter->add ( "Kode", "kode" );
		$adapter->add ( "BOI", "connect","trivial_1_<i class='fa fa-check'></i>_" );
		$this->dbtable = new DBTable ( $this->db, "smis_rwt_aok_" . $this->polislug );
		$this->dbtable->setOrder ( "nama ASC" );
		$this->dbres = new DBResponder ( $this->dbtable, $this->uitable, $adapter );
		$data = $this->dbres->command ( $_POST ['command'] );
		echo json_encode ( $data );
		return;
	}
	public function superCommand($super_command) {
		$head=array ('Nama','Satuan',"ID Obat","HNA" );
		$dktable = new Table ( $head);
		$dktable->setName ( "list_aok_obat" );
		$dktable->setModel ( Table::$SELECT );
		$dkadapter = new SimpleAdapter ();
		$dkadapter->add ( "Nama", "nama_dokter" );
		$dkadapter->add ( "Jabatan", "jabatan", "unslug" );
		$dkadapter->add ( "Kelas", "kelas", "unslug" );
		$dkadapter->add ( "Tarif", "tarif", "money Rp." );
		$list_aok_obat = new ServiceResponder ( $this->db, $dktable, $dkadapter, "get_list_obat" );
		$super = new SuperCommand ();
		$super->addResponder ( "list_aok_obat", $list_aok_obat );
		$init = $super->initialize ();
		if ($init != null) {
			echo $init;
			return;
		}
	}

	// load css and javascript before loading
	public function jsLoader() {
		echo addJS ( "framework/smis/js/table_action.js" );
	}

	/* when it's star build */
	public function phpPreload() {
		$this->uitable->addModal ( "id", "hidden", "", "" );
		$this->uitable->addModal ( "kode", "hidden", "", "" );
		$this->uitable->addModal ( "connect", "checkbox", "BOI", "" );
		$this->uitable->addModal ( "nama", "chooser-aok-list_aok_obat", "Nama", "" );
		$this->uitable->addModal ( "id_obat", "text", "ID Obat", "","y",NULL,true );
		$this->uitable->addModal ( "satuan", "text", "Satuan", "" );
		$this->uitable->addModal ( "all_satuan", "hidden", "", "" );
		$this->uitable->addModal ( "kategori", "text", "Kategori", "" );
		$this->uitable->addModal ( "harga_asli", "money", "Harga Asli", "" );
		$this->uitable->addModal ( "kelipatan", "text", "Pengali", "","n","" );
		$this->uitable->addModal ( "harga", "money", "Harga", "" );
		$this->uitable->addModal ( "stok", "hidden", "","" );
		$modal = $this->uitable->getModal ();
		$modal->setTitle ( "Alat Obat " . $this->poliname );
		echo $this->uitable->getHtml ();
		echo $modal->getHtml ();
	}
	
	public function jsPreLoad() {
		?>
		<script type="text/javascript">
				var aok;
				var list_aok_obat;

				function appenlist(list){
					$("#aok_satuan").html("");
					try{
						var asatuan=$.parseJSON("["+list+"]");
						var lsatuan=asatuan[0];
						$.each(lsatuan,function(index,val){
							$("#aok_satuan").append('<option value='+val+'>'+val+'</option>');
						});
					}catch(e){
						console.log(e);
					}
				}

				
				
				$(document).ready(function(){

					$("#aok_connect").on("change",function(){
						if($("#aok_connect").is(":checked")){
							$(".aok_harga_asli, .aok_harga ").hide("fast");
							$("#aok_chooser_nama").attr('onclick', "aok.chooser('aok','aok_nama','list_aok_obat',list_aok_obat,'list_aok_obat')");
						}else{
							$("#aok_chooser_nama").attr('onclick',"");
							$(".aok_harga_asli, .aok_harga ").show("fast");
						}
					});

					$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
					$("#aok_harga_asli, #aok_kelipatan").on("change",function(){
						var uang=getMoney("#aok_harga_asli");
						var pengali=parseFloat($("#aok_kelipatan").val());
						setMoney("#aok_harga",uang*pengali);
					});
					
					var column=new Array('id','nama',"id_obat","connect","harga_asli","kelipatan",'satuan','all_satuan','kategori',"stok","harga","kode");
					aok=new TableAction("aok","<?php echo $this->polislug;?>","aok",column);
					aok.setPrototipe("<?php echo $this->poliname ?>","<?php echo $this->polislug ?>","rawat");
					aok.edit=function (id){
						var self=this;
						showLoading();	
						var edit_data=this.getEditData(id);
						$.post('',edit_data,function(res){		
							var json=getContent(res);
							if(json==null) return;
							appenlist(json.all_satuan);							
							for(var i=0;i<self.column.length;i++){
								var name=self.column[i];
								var the_id="#"+self.prefix+"_"+name;
								smis_edit(the_id,json[""+name]);
							}
							dismissLoading();
							self.disabledOnEdit(self.column_disabled_on_edit);
							self.show_form();
						});
					};

					aok.clear=function(){	
						$("#aok_satuan").html("");
						for(var i=0;i<this.column.length;i++){
							var the_id="#"+this.prefix+"_"+this.column[i];
							smis_clear(the_id);
						}
						this.enabledOnNotEdit(this.column_disabled_on_edit);
					};
					
					aok.view();
					

					list_aok_obat=new TableAction("list_aok_obat","<?php echo $this->polislug ?>","aok",new Array());
					list_aok_obat.setPrototipe("<?php echo $this->poliname ?>","<?php echo $this->polislug ?>","rawat");
					list_aok_obat.setSuperCommand("list_aok_obat");
					list_aok_obat.selected=function(json){
						$("#aok_nama").val(json.nama);
						$("#aok_id_obat").val(json.id);						
						$("#aok_satuan").val(json.satuan);
						appenlist(json.satuan);
					};					
				});
			</script>
<?php
	}
}

?>