<?php 
require_once ("smis-base/smis-include-service-consumer.php");
class AOKGudangHerOff extends ModulTemplate {
	private $poliname;
	private $polislug;
	private $uitable;
	private $dbres;
	private $dbtable;
	private $db;
	private $mode;
	public function __construct($db, $polislug, $poliname) {
		parent::__construct ();
		$this->polislug = $polislug;
		$this->poliname = $poliname;
		$this->db = $db;
		$header=array ('Nama','Harga',"Satuan");
		$this->uitable = new Table ( $header , "Alat Obat Kesehatan " . $poliname, NULL, true );
		$this->uitable->setDelButtonEnable(false);
		$this->uitable->setAddButtonEnable(false);
		$this->uitable->setReloadButtonEnable(false);
		$this->uitable->setEditButtonEnable(false);
		
		$btn=new Button("","","");
		$btn->setClass("btn-primary");
		$btn->setIcon("fa fa-recycle");
		$btn->setIsButton(Button::$ICONIC);		
		$this->uitable->setName ( "aokgudangheroff" );
		$this->uitable->addContentButton("restored",$btn);
	}
	public function command($command) {
		$adapter = new SimpleAdapter ();
		$adapter->add ( "Nama", "nama" );
		$adapter->add ( "Harga", "harga", "money Rp." );
		$adapter->add ( "Stok", "stok", "number" );
		$adapter->add ( "Satuan", "satuan" );
		$adapter->add ( "Kode", "kode" );
		$adapter->add ( "BOI", "connect","trivial_1_<i class='fa fa-check'></i>_" );
		$this->dbtable = new DBTable ( $this->db, "smis_rwt_aok_" . $this->polislug );
		$this->dbtable->setDelete(true);
		$this->dbtable->addCustomKriteria("connect","=1");
		$this->dbtable->addCustomKriteria("prop"," LIKE '%del%'");
		$this->dbtable->setOrder ( "nama ASC" );
		$this->dbres = new DBResponder ( $this->dbtable, $this->uitable, $adapter );
		$this->dbres->setExistIncludeDel(true);
		$data = $this->dbres->command ( $_POST ['command'] );
		echo json_encode ( $data );
		return;
	}
	public function superCommand($super_command) {
		$head=array ('Nama','Satuan',"ID Obat","HNA" );
		$dktable = new Table ( $head);
		$dktable->setName ( "list_aokgudangheroff_obat" );
		$dktable->setModel ( Table::$SELECT );
		$dkadapter = new SimpleAdapter ();
		$dkadapter->add ( "Nama", "nama_dokter" );
		$dkadapter->add ( "Jabatan", "jabatan", "unslug" );
		$dkadapter->add ( "Kelas", "kelas", "unslug" );
		$dkadapter->add ( "Tarif", "tarif", "money Rp." );
		$list_aokgudangheroff_obat = new ServiceResponder ( $this->db, $dktable, $dkadapter, "get_list_obat" );
		$super = new SuperCommand ();
		$super->addResponder ( "list_aokgudangheroff_obat", $list_aokgudangheroff_obat );
		$init = $super->initialize ();
		if ($init != null) {
			echo $init;
			return;
		}
	}

	// load css and javascript before loading
	public function jsLoader() {
		echo addJS ( "framework/smis/js/table_action.js" );
	}

	/* when it's star build */
	public function phpPreload() {
		$this->uitable->addModal ( "id", "hidden", "", "" );
		$this->uitable->addModal ( "kode", "hidden", "", "" );
		$this->uitable->addModal ( "connect", "hidden", "", "1" );
		$this->uitable->addModal ( "nama", "text", "Nama", "","y",NULL,true );
		$this->uitable->addModal ( "id_obat", "text", "ID Obat", "","y",NULL,true );
		$this->uitable->addModal ( "satuan", "text", "Satuan", "","y",NULL,true );
		$this->uitable->addModal ( "all_satuan", "hidden", "", "" );
		$this->uitable->addModal ( "kategori", "text", "Kategori", "","y",NULL,true );
		$this->uitable->addModal ( "harga_asli", "money", "Harga Asli", "" );
		$this->uitable->addModal ( "kelipatan", "text", "Pengali", "","n","","y",NULL,true );
		$this->uitable->addModal ( "harga", "money", "Harga", "" );
		$this->uitable->addModal ( "stok", "hidden", "","" );
		$modal = $this->uitable->getModal ();
		$modal->setTitle ( "Alat Obat " . $this->poliname );
		echo $this->uitable->getHtml ();
		echo $modal->getHtml ();
	}
	
	public function jsPreLoad() {
		?>
		<script type="text/javascript">
				var aokgudangheroff;
				var list_aokgudangheroff_obat;

				function appenlist(list){
					$("#aokgudangheroff_satuan").html("");
					try{
						var asatuan=$.parseJSON("["+list+"]");
						var lsatuan=asatuan[0];
						$.each(lsatuan,function(index,val){
							$("#aokgudangheroff_satuan").append('<option value='+val+'>'+val+'</option>');
						});
					}catch(e){
						console.log(e);
					}
				}

				
				
				$(document).ready(function(){

					$("#aokgudangheroff_connect").on("change",function(){
						if($("#aokgudangheroff_connect").is(":checked")){
							$(".aokgudangheroff_harga_asli, .aokgudangheroff_harga ").hide("fast");
							$("#aokgudangheroff_chooser_nama").attr('onclick', "aokgudangheroff.chooser('aokgudangheroff','aokgudangheroff_nama','list_aokgudangheroff_obat',list_aokgudangheroff_obat,'list_aokgudangheroff_obat')");
						}else{
							$("#aokgudangheroff_chooser_nama").attr('onclick',"");
							$(".aokgudangheroff_harga_asli, .aokgudangheroff_harga ").show("fast");
						}
					});

					$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
					$("#aokgudangheroff_harga_asli, #aokgudangheroff_kelipatan").on("change",function(){
						var uang=getMoney("#aokgudangheroff_harga_asli");
						var pengali=parseFloat($("#aokgudangheroff_kelipatan").val());
						setMoney("#aokgudangheroff_harga",uang*pengali);
					});
					
					var column=new Array('id','nama',"id_obat","connect","harga_asli","kelipatan",'satuan','all_satuan','kategori',"stok","harga","kode");
					aokgudangheroff=new TableAction("aokgudangheroff","<?php echo $this->polislug;?>","aokgudangheroff",column);
					aokgudangheroff.setPrototipe("<?php echo $this->poliname ?>","<?php echo $this->polislug ?>","rawat");
					aokgudangheroff.edit=function (id){
						var self=this;
						showLoading();	
						var edit_data=this.getEditData(id);
						$.post('',edit_data,function(res){		
							var json=getContent(res);
							if(json==null) return;
							appenlist(json.all_satuan);							
							for(var i=0;i<self.column.length;i++){
								var name=self.column[i];
								var the_id="#"+self.prefix+"_"+name;
								smis_edit(the_id,json[""+name]);
							}
							dismissLoading();
							self.disabledOnEdit(self.column_disabled_on_edit);
							self.show_form();
						});
					};
					
					aokgudangheroff.restored=function (id){
						var self=this;
						showLoading();	
						var edit_data=this.getRegulerData(id);
						edit_data['command']="save";
						edit_data['id']=id;
						edit_data['prop']="";
						$.post('',edit_data,function(res){		
							var json=getContent(res);
							self.view();
							dismissLoading();
						});
					};

					aokgudangheroff.clear=function(){	
						$("#aokgudangheroff_satuan").html("");
						for(var i=0;i<this.column.length;i++){
							var the_id="#"+this.prefix+"_"+this.column[i];
							smis_clear(the_id);
						}
						this.enabledOnNotEdit(this.column_disabled_on_edit);
					};
					
					aokgudangheroff.view();
					

					list_aokgudangheroff_obat=new TableAction("list_aokgudangheroff_obat","<?php echo $this->polislug ?>","aokgudangheroff",new Array());
					list_aokgudangheroff_obat.setPrototipe("<?php echo $this->poliname ?>","<?php echo $this->polislug ?>","rawat");
					list_aokgudangheroff_obat.setSuperCommand("list_aokgudangheroff_obat");
					list_aokgudangheroff_obat.selected=function(json){
						$("#aokgudangheroff_nama").val(json.nama);
						$("#aokgudangheroff_id_obat").val(json.id);						
						$("#aokgudangheroff_satuan").val(json.satuan);
						appenlist(json.satuan);
					};					
				});
			</script>
<?php
	}
}

?>