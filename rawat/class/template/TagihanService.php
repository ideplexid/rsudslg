<?php 

/**
 * this class used to detail all price 
 * that the patient spend for medical treatment
 * is used for sending detail in cashier 
 * and also used for viewing the data price in
 * the current room
 * 
 * @author      : Nurul Huda
 * @since       : 14 Jul 2014
 * @version     : 1.0.4
 * @copyright   : goblooge@gmail.com
 * */

class TagihanService{
	private $polislug;
	private $noreg;
	private $db;
	private $adaptermode;
	private $layanan;
	private $total;
	private $position;
	private $grupname;
	private $keterangan_keluar;
	private $total_alat_obat;
	
	
	public static $MODE_RIGID="rigid";
	public static $MODE_RESUME="resume";
	
	public function __construct($db,$noreg,$polislug){
		$this->noreg=$noreg;
		$this->polislug=$polislug;
		$this->db=$db;		
		$this->adaptermode=self::$MODE_RESUME;
		$this->layanan= array();		 
		$this->layanan["alok"] = "1";
		$this->layanan["rr"] = "0";
		$this->layanan["oksigen_central"] = "0";
		$this->layanan["oksigen_manual"] = "0";
		$this->layanan["bed"] = "1";
		
		$this->layanan["audiometry"] = "1";
		$this->layanan["bronchoscopy"] = "1";
		$this->layanan["ekg"] = "1";
		$this->layanan["endoscopy"] = "1";
		$this->layanan["faal_paru"] = "1";
		$this->layanan["spirometry"] = "1";
		
		$this->layanan["konsul_dokter"] = "0";
		$this->layanan["konsultasi_dokter"] = "0";
		$this->layanan["tindakan_dokter"] = "0";
		
		$this->layanan["tindakan_perawat"] = "1";
		$this->layanan["tindakan_igd"] = "1";
		
		$this->layanan["visite_dokter"] = "0";
		$this->layanan["ok"] = "0";
		$this->layanan["vk"] = "1";
		$this->total_alat_obat=0;
		$this->grupname=array();
		foreach($this->layanan as $key=>$val){
			$name=getSettings($this->db, "smis-rs-name-".$key."-".$this->polislug, ArrayAdapter::format("unslug", $key));
			$this->grupname[$key]=$name;
		}
	}
	
	public function setMode($mode){
		$this->adaptermode=$mode;
	}
	
	public function getTagihan(){
		$antrian = new DBTable ( $this->db, "smis_rwt_antrian_" . $this->polislug );
		$exist = $antrian->is_exist ( array (
				"no_register" => $this->noreg
		) );
		
		$response = array ();
		$response ['exist'] = $exist == "0" ? "0" : "1";
		if ($exist == "0") {
			return $response;
		}
		
        $query = "SELECT MAX(id) as id FROM smis_rwt_antrian_" . $this->polislug  . " 
                  WHERE no_register='" . $this->noreg . "' AND prop!='del' ";
		$id_antrian = $this->db->get_var ( $query );
		
		$antrian->setViewForSelect ( false );
		$row = $antrian->select ( array (
				"id" => $id_antrian
		) );
		$response ['selesai'] = $row->selesai;
		$response ['cara_keluar'] = $row->cara_keluar;
		$response ['real_name'] = get_entity_name($this->db,$this->polislug);
		$this->keterangan_keluar=$row->keterangan_keluar;
		$response ['reverse'] = "1";
		$adapter=NULL;
		if($this->adaptermode==self::$MODE_RESUME){
			$response ['data'] = $this->getResume();
		}else{
			$response ['data'] = $this->getRigid();
		}		
		
		return $response;
	}
	
	/* ini dipakai untuk yang kasir
	 * sehingga data yang ditampilkan adalah Resume
	 * */
	public function getResume(){
		require_once "rawat/class/adapter/TagihanAdapter.php";
        require_once "rawat/class/adapter/TagihanDokterAdapter.php";
        
        $adapter_td  = new TagihanDokterAdapter($this->db,$this->polislug);
		$adapter     = new TagihanAdapter ($this->db,$this->polislug);
 		$adapterok   = $adapter;
        $setting     = getSettings($this->db,"smis-rs-acc-ok-detail-".$this->polislug,"TagihanOKAdapter");
		if($setting != "-" && $setting!=""){
            require_once "rawat/class/adapter/".$setting.".php";
            $adapterok = new TagihanOKAdapter($this->db,$this->polislug);
        }
        $ldata = array ();
		foreach ( $this->layanan as $l => $jaspel ) {
			$adapter->setAdapterName ( $l );
			$audiometry = new DBTable ( $this->db, "smis_rwt_" . $l . "_" . $this->polislug );
			$audiometry->setShowAll ( true );
			$audiometry->addCustomKriteria ( "noreg_pasien", "='" . $this->noreg . "'" );
			$data = $audiometry->view ( "", 0 );
			$result=null;			
 			if($l=="ok"){
 				$result = $adapterok->getContent ( $data ['data'] );
 			}else if($l=="tindakan_dokter"){
				$result = $adapter_td->getContent ( $data ['data'] );
			}else{
				$result = $adapter->getContent ( $data ['data'] );
			}
			$unit_data = array (
					"result" => $result,
					"jasa_pelayanan" => $jaspel
			);
			$ldata [$l] = $unit_data;
		}
		$this->total_alat_obat=$adapter->getTotalAlatObat();
		return $ldata;
	}
	
	/**
	 * @brief ini tampilan untuk
	 * 		  tagihan ruangan sehingga yang 
     *        ditampilkan adalah tagihan milik ruangan
	 * @return  Array 
	 */
	public function getRigid(){
		require_once "rawat/class/adapter/TagihanAdapter.php";
		require_once "rawat/class/adapter/TagihanRigidAdapter.php";
		$visitename=getSettings($this->db,  "smis-rs-printout-visite-" . $this->polislug, "Visite");
		$susun=getSettings($this->db, "smis-rs-printout-susun-" . $this->polislug, "0")=="1";
		$jalok=getSettings($this->db, "smis-rs-printout-jumlah-alok-".$this->polislug, "0")=="1";
		$jsemua=getSettings($this->db, "smis-rs-printout-susun-jumlah-".$this->polislug, "0")=="1";
		$space=getSettings($this->db, "smis-rs-printout-space-".$this->polislug, "0")=="1";
		$non_bed_oksigen=getSettings($this->db, "smis-rs-printout-alok-bed-oksigen-".$this->polislug, "0")=="1";
		
		
		$this->position="";
		if(getSettings($this->db, "smis-rs-printout-waktu-" . $this->polislug, "0")=="1") $this->position="waktu";
		else if(getSettings($this->db, "smis-rs-printout-jenis-" . $this->polislug, "0")=="1") $this->position="jenis";
		else if(getSettings($this->db, "smis-rs-printout-layanan-" . $this->polislug, "0")=="1") $this->position="nama";
		else if(getSettings($this->db, "smis-rs-printout-satuan-" . $this->polislug, "0")=="1") $this->position="satuan";
		else if(getSettings($this->db, "smis-rs-printout-keterangan-" . $this->polislug, "0")=="1") $this->position="keterangan";
		else if(getSettings($this->db, "smis-rs-printout-jumlah-" . $this->polislug, "0")=="1") $this->position="jumlah";
		
		
		$adapter = new TagihanRigidAdapter($this->polislug);
		$adapter->setVisiteName($visitename);
		$ldata = array ();
		foreach ( $this->layanan as $l => $jaspel ) {
			$adapter->setAdapterName ( $l );
			$layanan = new DBTable ( $this->db, "smis_rwt_" . $l . "_" . $this->polislug );
			$layanan->setShowAll ( true );
			$layanan->addCustomKriteria ( "noreg_pasien", "='" . $this->noreg . "'" );
			$data = $layanan->view ( "", 0 );
			$result = $adapter->getContent ( $data ['data'] );
			$ada=0;
			foreach ($result as $one_r){
				if($ada==0 && $susun){
					$ada=1;
					$a [$this->position] = "<strong>".$this->grupname[$l]."</strong>";
					if($l=="visite_dokter" && $visitename!="Visite"){
						$a [$this->position] = "<strong>".$visitename."</strong>";
					}
					$ldata[]=$a;
				}
				$one_r['layanan']=$this->grupname[$l];
				$ldata[]=$one_r;
				$ada=true;
			}
			
			if($l=="rr" && $adapter->getTotalAlatObatTanpaOksigenBed()>0 && $jalok && $non_bed_oksigen ){
				$a=array();
				$a[$this->position]="<strong class='separator_print_out'>TOTAL ALAT DAN OBAT</strong>";
				$a ['biaya'] = $adapter->getTotalAlatObatTanpaOksigenBed();
				$ldata[]=$a;
				if($space){
					$a=array();
					$a[$this->position]="&nbsp;";
					$a ['biaya'] = "";
					$ldata[]=$a;
				}
			}else if($l=="oksigen_manual" && $adapter->getTotalOksigen()>0 && $jalok && $non_bed_oksigen){				
				$a=array();
				$a[$this->position]="<strong class='separator_print_out'>TOTAL OKSIGEN MANUAL/CENTRAL</strong>";
				$a ['biaya'] = $adapter->getTotalOksigen();
				$ldata[]=$a;
				if($space){
					$a=array();
					$a[$this->position]="&nbsp;";
					$a ['biaya'] = "";
					$ldata[]=$a;
				}
			}else if($l=="bed" && $adapter->getTotalBed()>0 && $jalok && $non_bed_oksigen){				
				$a=array();
				$a[$this->position]="<strong class='separator_print_out'>TOTAL BED</strong>";
				$a ['biaya'] = $adapter->getTotalBed();
				$ldata[]=$a;
				if($space){
					$a=array();
					$a[$this->position]="&nbsp;";
					$a ['biaya'] = "";
					$ldata[]=$a;
				}
			}else if($l=="bed" && $adapter->getTotalAlatObat()>0 && $jalok && !$non_bed_oksigen){				
				$a=array();
				$a[$this->position]="<strong class='separator_print_out'>TOTAL ALAT DAN OBAT</strong>";
				$a ['biaya'] = $adapter->getTotalAlatObat();
				$ldata[]=$a;
				if($space){
					$a=array();
					$a[$this->position]="&nbsp;";
					$a ['biaya'] = "";
					$ldata[]=$a;
				}
			}else if($l=="tindakan_dokter" && $adapter->getTotalTindakanDokter()>0 && $jsemua){
				$a=array();
				$a[$this->position]="<strong class='separator_print_out'>TOTAL TINDAKAN DOKTER + ASISTEN PERAWAT</strong>";
				$a ['biaya'] = $adapter->getTotalTindakanDokter();
				$ldata[]=$a;
				if($space){
					$a=array();
					$a[$this->position]="&nbsp;";
					$a ['biaya'] = "";
					$ldata[]=$a;
				}
			}else if($l=="tindakan_igd" && $adapter->getTotalTindakanPerawat()>0 && $jsemua){
				$a=array();
				$a[$this->position]="<strong class='separator_print_out'>TOTAL TINDAKAN PERAWAT</strong>";
				$a ['biaya'] = $adapter->getTotalTindakanPerawat();
				$ldata[]=$a;
				if($space){
					$a=array();
					$a[$this->position]="&nbsp;";
					$a ['biaya'] = "";
					$ldata[]=$a;
				}
			}else if($l=="konsul_dokter" && $adapter->getTotalKonsul()>0 && $jsemua){
				$a=array();
				$a[$this->position]="<strong class='separator_print_out'>TOTAL KONSUL</strong>";
				$a ['biaya'] = $adapter->getTotalKonsul();
				$ldata[]=$a;
				if($space){
					$a=array();
					$a[$this->position]="&nbsp;";
					$a ['biaya'] = "";
					$ldata[]=$a;
				}
			}else if($l=="konsultasi_dokter" && $adapter->getTotalKonsul()>0 && $jsemua){
				$a=array();
				$a[$this->position]="<strong class='separator_print_out'>TOTAL PERIKSA DOKTER</strong>";
				$a ['biaya'] = $adapter->getTotalPeriksa();
				$ldata[]=$a;
				if($space){
					$a=array();
					$a[$this->position]="&nbsp;";
					$a ['biaya'] = "";
					$ldata[]=$a;
				}
			}else if($l=="visite_dokter" && $adapter->getTotalVisite()>0 && $jsemua){
				$a=array();
				$a[$this->position]="<strong class='separator_print_out'>TOTAL ".strtoupper($visitename)." DOKTER (V)</strong>";
				$a ['biaya'] = $adapter->getTotalVisite();
				$ldata[]=$a;
				if($space){
					$a=array();
					$a[$this->position]="&nbsp;";
					$a ['biaya'] = "";
					$ldata[]=$a;
				}
			}else if($l=="spirometry" && $adapter->getTotalPenunjangPerawat()>0 && $jsemua){
				$a=array();
				$a[$this->position]="<strong class='separator_print_out'>TOTAL PENUNJANG </strong>";
				$a ['biaya'] = $adapter->getTotalPenunjangPerawat();
				$ldata[]=$a;
				if($space){
					$a=array();
					$a[$this->position]="&nbsp;";
					$a ['biaya'] = "";
					$ldata[]=$a;
				}
			}
		}
		
		$this->total=$adapter->getTotal();
		$this->total_alat_obat=$adapter->getTotalAlatObat();
		return $ldata;
	}
	
	public function getRigidTotal(){
		return $this->total;
	}
	
	public function getRigidPosition(){
		return $this->position;
	}
	
	public function getKeterangan(){
		return $this->keterangan_keluar;
	}
	
	public function getTotalAlatObat(){
		return $this->total_alat_obat;
	}
	
	
}


?>