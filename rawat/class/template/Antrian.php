<?php
/**
 * this class used to handle 
 * the queue that registered in this patient
 * @author 		: Nurul Huda
 * @copyright 	: goblooge@gmail.com
 * @license 	: LGPLv3
 * @since 		: 17 Aug 2014
 * @database 	: smis_rwt_antrian_
 * @version 	: 4.0.5 
 * 
 */
require_once "rawat/class/adapter/AntrianAdapter.php";
require_once "rawat/class/responder/AntrianResponder.php";
require_once "smis-framework/smis/template/ModulTemplate.php";

 class AntrianTemplate extends ModulTemplate {
	private $poliname;
	private $polislug;
	private $uitable;
	private $dbres;
	private $dbtable;
	private $db;
	private $mode;
    private $is_titipan_enabled;
	private $is_no_profile_enabled;
	
	public function __construct($db,$polislug,$poliname) {
		parent::__construct();
		$this->polislug = $polislug;
		$this->poliname = $poliname;
		$this->db       = $db;
		
		$layani = new Button("","","");
		$layani ->setClass("btn-success")
                ->setIcon("fa fa-sign-in")
				->setIsButton(Button::$ICONIC);	
					
		$pulang = new Button("","","");
		$pulang ->setClass("btn-danger")
                ->setIcon("fa fa-sign-out")
				->setIsButton(Button::$ICONIC);
		$this->is_pulang_button_enable = getSettings($this->db,"smis-rs-show-exit-button-".$this->polislug,"1")=="1";		                
		$titip  = new Button("","","");
		$titip  ->setClass("btn-warning")
                ->setIcon("fa fa-signing")
                ->setIsButton(Button::$ICONIC);
		$this->is_titipan_enabled 	 = getSettings($this->db,"smis-rs-titipan-".$this->polislug,"0")=="1";
        $this->is_no_profile_enabled = getSettings($this->db,"smis-rs-profile-".$this->polislug,"0")=="1";
        
		$header         = array('Waktu',"Waktu Register","Nomor","Asal","Kunjungan","No. Register",'NRM','Jenis Kelamin','Pasien',"Umur","Cara Bayar","Alamat","Dokumen","Status Pasien","Keterangan");
		if($this->is_titipan_enabled){
            $header[]   = "Titipan";
		}
		if($this->is_no_profile_enabled){
            $header[]   = "No. Profile";
        }
        $this->mode     = getSettings($this->db,"smis-rs-antrian-mode-".$this->polislug,"Integrated");
		$this->uitable  = new Table($header,"Antrian " . $poliname,NULL,true);
		$this->uitable  ->setName("antrian")
                        ->addContentButton("layani",$layani)
                        ->setDelButtonEnable(false)
                        ->setEditButtonEnable(false)
                        ->setReloadButtonEnable( false)
                        ->setPrintButtonEnable(false)
                        ->setHelpButtonEnabled(true,$this->polislug,"antrian",$this->polislug,$this->poliname,"rawat");
		if($this->is_pulang_button_enable){
			$this->uitable->addContentButton("edit",$pulang);
		}
		if($this->is_titipan_enabled){
            $this ->uitable  
                  ->addContentButton("titipan",$titip);
        }
        if($this->mode=="Integrated"){
            $this   ->uitable
                    ->setAddButtonEnable(false);
        } 
            
		if(getSettings($db,"smis-rs-skb-".$this->polislug,"0")=="1"){
			$btn    = new Button("","","SKB");
			$btn    ->setIsButton(Button::$ICONIC)
                    ->setIcon("fa fa-heartbeat")
                    ->setClass(" btn-primary");
			$this   ->uitable
                    ->addContentButton("skb",$btn);
		}
	}

	public function superCommand($super_command) {		
		if ($_POST ['super_command'] == "validasi_spesialisasi") {
			$id = $_POST['id'];
			$carapulang = $_POST['carapulang'];
			$enable_field_spesialisasi = getSettings($this->db, "smis-rs-laporan-rl31-enable_field_spesialisasi-" . $this->polislug, "0");
			$valid = "1";
			if ($enable_field_spesialisasi == "1" && $carapulang != "Kabur" && $carapulang != "Tidak Datang") {
				$spesialisasi_row = $this->db->get_row("
					SELECT spesialisasi
					FROM smis_rwt_antrian_" . $this->polislug . "
					WHERE id = '" . $id . "'
				");
				$spesialisasi = "";
				if ($spesialisasi_row != null)
					$spesialisasi = $spesialisasi_row->spesialisasi;
				if ($spesialisasi == "")
					$valid = "0";
			}

			$data['valid'] = $valid;
			echo json_encode($data);
			return;
		}

		$dktable   = new Table(array('RS / Klinik','Unit',"Spesialis"),"",NULL,true);
		$dktable   ->setName("unit_rujukan");
		$dktable   ->setModel(Table::$SELECT);
		$dkadapter = new SimpleAdapter();
		$dkadapter ->add("RS / Klinik","nama")
				   ->add("Unit","poli")
				   ->add("Spesialis","is_spesialis","trivial_0_Tidak_Ya");
		$rujukan   = new ServiceResponder($this->db,$dktable,$dkadapter,"get_tujuan_rujukan","medical_record");
		
		$super 		= new SuperCommand();
		$super		->addResponder("unit_rujukan",$rujukan);
		$init 		= $super->initialize();
		if($init != null) {
			echo $init;
			return;
		}
	}
	
	public function command($command) {		
		$this->dbtable 	 = new DBTable($this->db,'smis_rwt_antrian_' . $this->polislug);
		$filter 		 = "";
		if(isset($_POST['kriteria'])) {
			$filter 	 = "WHERE asal LIKE '%" . $_POST['kriteria'] . "%' 
							OR nama_pasien LIKE '%" . $_POST['kriteria'] . "%' 
							OR bed_kamar LIKE '%" . $_POST['kriteria'] . "%'";
			if(is_numeric($_POST['kriteria'])) {
				$filter .= "OR no_register LIKE '%" .($_POST['kriteria'] * 1) . "%' 
							OR nrm_pasien LIKE '%" .($_POST['kriteria'] * 1) . "%'
							OR detail LIKE '%\"no_profile\":\"".($_POST['kriteria'] * 1)."\"%'";
			}
		}
		$query_value = "
			SELECT *
			FROM(
				SELECT a.*,b.nama_bed AS 'bed_kamar'
				FROM smis_rwt_antrian_" . $this->polislug . " a LEFT JOIN(
					SELECT *
					FROM smis_rwt_bed_" . $this->polislug . "
					WHERE id IN(
						SELECT MAX(id) 
						FROM smis_rwt_bed_" . $this->polislug . "
						WHERE prop NOT LIKE 'del'
						GROUP BY noreg_pasien
					)
				) b ON a.no_register = b.noreg_pasien
				WHERE a.selesai = '0'
				ORDER BY date(a.waktu) ASC,a.nomor ASC
			) v
			" . $filter . "
		";
		$query_count = "
			SELECT COUNT(*)
			FROM(
				SELECT a.*,b.nama_bed AS 'bed_kamar'
				FROM smis_rwt_antrian_" . $this->polislug . " a LEFT JOIN(
					SELECT *
					FROM smis_rwt_bed_" . $this->polislug . "
					WHERE id IN(
						SELECT MAX(id) 
						FROM smis_rwt_bed_" . $this->polislug . "
						WHERE prop NOT LIKE 'del'
						GROUP BY noreg_pasien
					)
				) b ON a.no_register = b.noreg_pasien
				WHERE a.selesai = '0'
				ORDER BY date(a.waktu),a.nomor ASC
			) v
			" . $filter . "
		";
		$this->dbtable->setPreferredQuery(true,$query_value,$query_count);
		$this->dbres = new AntrianResponder($this->dbtable,$this->uitable,new AntrianAdapter($this->getTotalPasien()) ,$this->poliname,$this->polislug,$this->mode);
		$data 		 = $this->dbres->command($_POST ['command']);
		echo json_encode($data);
	}
	
	/** get the total patient that curretnly inside this room 
	 *  @return $total is the int of patient inside this room
	 */
	private function getTotalPasien(){
		$query	= "SELECT count(*) as total FROM smis_rwt_antrian_".$this->polislug." WHERE selesai=0 AND prop!='del'; ";
		$total	= $this->dbtable->get_db()->get_var($query);
		return $total;
	}
	

	public function phpPreload(){
		$this->uitable->addModal("id","hidden","","");
		if($this->mode=="Stand Alone"){
			/*untuk memanipulasi data ruang*/	
			require_once 'smis-base/smis-include-service-consumer.php';
			$urjip   =   new ServiceConsumer($this->db,"get_urjip",array());
			$urjip   ->setMode(ServiceConsumer::$MULTIPLE_MODE)
                     ->setCached(true,"get_urjip")
                     ->execute();
			$content = $urjip->getContent();
			$ruangan = array();
			foreach($content as $autonomous=>$ruang){
				foreach($ruang as $nama_ruang=>$jip){
					$option          		= array();
					$option['value'] 		= $nama_ruang;
					$option['name']  		= $ruang['name'];//ArrayAdapter::format("unslug",$nama_ruang);
					if($nama_ruang==$this->polislug) {
						$option['default']	= "1";
					}
					$ruangan[] 		 		= $option;
				}
			}
			$option          = array();
			$option['value'] = "Pendaftaran";
			$option['name']  = "PENDAFTARAN";
			$ruangan[]       = $option;
			
			/*untuk memanipulasi data kelas*/
			require_once 'smis-base/smis-include-service-consumer.php';
			$service 	  	  = new ServiceConsumer($this->db,"get_kelas");
            $service 	  	  ->setCached(true,"get_kelas")
                     	  	  ->execute();
			$kelas   	  	  = $service->getContent();
			$option_kelas 	  = new OptionBuilder();
			$cur_kelas 	  	  = getSettings($this->db,"smis-rs-kelas-" . $this->polislug,"");
			foreach($kelas as $k) {
				$nama 		  = $k ['nama'];
				$slug 		  = $k ['slug'];
				$option_kelas ->add($nama,$slug,$slug == $cur_kelas ? "1" : "0");
			}
			
			/*untuk memanipulasi data bahwa selesai*/
			$selesai = new OptionBuilder();
			$selesai ->add("Ya","1","0")
                     ->add("Belum","0","1");
			/**pilihan jenis kelamin */
			$jk 	 = new OptionBuilder();
			$jk 	 ->add("Laki-Laki","0","1")
                	 ->add("Perempuan","1","0");
			
			/*untuk memanipulasi data cara bayar*/
			$service          = new ServiceConsumer($this->db,"get_carabayar");
			$service          ->execute();
			$dataasuransi     = $service->getContent();			
			$adapter          = new SelectAdapter("nama","slug");
			$jenis_pembayaran = $adapter->getContent($dataasuransi);
			
			$this ->uitable
                  ->addModal("no_register","hidden","","","")
                  ->addModal("nrm_pasien","text","NRM Pasien","","n")
                  ->addModal("nama_pasien","text","Nama Pasien","","n")
                  ->addModal("ibu_kandung","text","Ibu Kandung","")
                  ->addModal("jk","select","Jenis Kelamin",$jk->getContent(),"n")
                  ->addModal("asal","select","Asal",$ruangan,$this->mode=="Integrated"?"y":"n")
                  ->addModal("carabayar","select","Carabayar",$jenis_pembayaran,"n")
                  ->addModal("kelas","select","Kelas",$option_kelas->getContent())
                  ->addModal("selesai","select","Selesai",$selesai->getContent());
		}else{
			$this ->uitable
                  ->addModal("no_register","hidden","","","")
                  ->addModal("nrm_pasien","hidden","","","")
                  ->addModal("nama_pasien","hidden","","","");
		}
		
		$dokumen = new OptionBuilder();
		$dokumen ->addSingle("Belum Menerima Dokumen")
                 ->addSingle("Telah Menerima Dokumen")
                 ->addSingle("Tidak Menerima Dokumen")
                 ->addSingle("Dokumen Pindah Ruangan Lain")
                 ->addSingle("Dikembalikan Ke Rekam Medis");
		
        loadLibrary("smis-libs-function-medical");	
        
        $keluar = new OptionBuilder();
		$keluar	->add("Tidak Datang","Tidak Datang")
				->add("Dipulangkan Hidup","Dipulangkan Hidup")
				->add("Dipulangkan Mati <=48 Jam","Dipulangkan Mati <=48 Jam")
				->add("Dipulangkan Mati <=24 Jam","Dipulangkan Mati <=24 Jam")
				->add("Dipulangkan Mati >48 Jam","Dipulangkan Mati >48 Jam")
				->add("Pulang Paksa","Pulang Paksa")
				->add("Kabur","Kabur")
                ->add("Pindah ke RS Lain","Pindah ke RS Lain")
                ->add("Dirujuk","Dirujuk")
                ->add("Dikembalikan ke Perujuk");

		$this ->uitable
              ->addModal("waktu_keluar","datetime","Tanggal","","n")
              ->addModal("cara_keluar","select","Status Keluar",$keluar	->getContent(),"n")
              ->addModal("keterangan_keluar","textarea","Keterangan","");
		if(getSettings($this->db,"smis-rs-dokumen-".$this->polislug,"0")=="1"){
			$this->uitable->addModal("dokumen","select","Dokumen",$dokumen->getContent());
		}
		if(getSettings($this->db,"smis-rs-tampil-rujukan-antrian-".$this->polislug,"0")=="1"){
			$this ->uitable
				  ->addModal("id_rs_rujuk","hidden","","")
				  ->addModal("nama_rs","chooser-antrian-unit_rujukan-Rujuk Ke","RS / Klinik Rujukan","")
				  ->addModal("nama_unit","text","Unit Rujukan","")
				  ->addModal("is_spesialistik","checkbox","Unit Spesialis","",null,true);
		}
			
		$modal = $this->uitable->getModal();
		$modal ->setTitle($this->poliname);
		$modal ->setComponentSize(Modal::$MEDIUM);
		
		$proto_nama = new Hidden("antrian_proto_name","",$_POST['prototype_name']);
		$proto_slug = new Hidden("antrian_proto_slug","",$_POST['prototype_slug']);
		$proto_impl = new Hidden("antrian_proto_implement","",$_POST['prototype_implement']);
		$poliname   = new Hidden("antrian_poliname","",$this->poliname);
		$polislug   = new Hidden("antrian_polislug","",$this->polislug);
		$mode       = new Hidden("antrian_mode","",$this->mode);
		$exitmode   = new Hidden("antrian_exit_mode","",getSettings($this->db,"smis-rs-exit-mode-" . $this->polislug,"0"));
		$noreg_p    = new Hidden("noreg_pasien","","");

		echo $noreg_p	 ->getHtml();
		echo $proto_nama ->getHtml();
		echo $proto_slug ->getHtml();
		echo $proto_impl ->getHtml();
		echo $poliname   ->getHtml();
		echo $polislug   ->getHtml();
		echo $mode       ->getHtml();
        echo $this->uitable->getHtml();
		echo $modal      ->getHtml();
		echo addJS("framework/bootstrap/js/bootstrap-datetimepicker.js");
		echo addJS("framework/smis/js/table_action.js");
		echo addJS("rawat/resource/js/antrian.js",false);
		echo addCSS("framework/bootstrap/css/bootstrap-datetimepicker.css");
	}
}
?>
