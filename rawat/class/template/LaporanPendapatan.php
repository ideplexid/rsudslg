<?php
require_once ("smis-framework/smis/template/ModulTemplate.php");
class LaporanPendapatan extends ModulTemplate {
	private $polislug;
	private $db;
	private $tabs;
	private $poliname;
	public function __construct($db, $polislug, $poliname) {
		parent::__construct ();
		$this->polislug = $polislug;
		$this->poliname = $poliname;
		$this->db = $db;
	}
	
	public function initialize() {
		if($_POST['action']!='laporan_pendapatan'){
			echo $this->loadTabs($_POST['action']);
		}else{
			parent::initialize();
		}
	}
	
	public function loadTabs($tb) {		
		$slug = $_POST ['prototype_slug'];
		$name = $_POST ['prototype_name'];
		$db=$this->db;		
		ob_start();
		$obj=null;
		
		switch ($tb){
			case "tindakan_perawat_detail" 	: require_once 'rawat/class/template/laporan_pendapatan/TindakanPerawatDetail.php'; 	$obj=new TindakanPerawatDetail ( $db, $slug, $name ); 	break;
			case "tindakan_igd_detail" 		: require_once 'rawat/class/template/laporan_pendapatan/TindakanIGDDetail.php'; 		$obj=new TindakanIGDDetail ( $db, $slug, $name ); 		break;
			case "tindakan_dokter_detail" 	: require_once 'rawat/class/template/laporan_pendapatan/TindakanDokterDetail.php'; 	    $obj=new TindakanDokterDetail ( $db, $slug, $name ); 	break;
			case "konsul_detail" 			: require_once 'rawat/class/template/laporan_pendapatan/KonsulDetail.php'; 			    $obj=new KonsulDetail ( $db, $slug, $name ); 			break;
			case "visite_detail" 			: require_once 'rawat/class/template/laporan_pendapatan/VisiteDetail.php'; 			    $obj=new VisiteDetail ( $db, $slug, $name ); 			break;
			case "periksa_detail" 			: require_once 'rawat/class/template/laporan_pendapatan/PeriksaDetail.php'; 			$obj=new PeriksaDetail ( $db, $slug, $name ); 			break;
			case "alat_obat_detail" 		: require_once 'rawat/class/template/laporan_pendapatan/AlatObatDetail.php'; 			$obj=new AlatObatDetail ( $db, $slug, $name ); 			break;
			case "oksigen_central_detail" 	: require_once 'rawat/class/template/laporan_pendapatan/OksigenCentralDetail.php'; 	    $obj=new OksigenCentralDetail ( $db, $slug, $name ); 	break;
			case "oksigen_manual_detail" 	: require_once 'rawat/class/template/laporan_pendapatan/OksigenManualDetail.php'; 		$obj=new OksigenManualDetail ( $db, $slug, $name ); 	break;
			case "bed_detail" 				: require_once 'rawat/class/template/laporan_pendapatan/BedDetail.php'; 				$obj=new BedDetail ( $db, $slug, $name ); 				break;
			case "operasi_detail" 			: require_once 'rawat/class/template/laporan_pendapatan/OperasiDetail.php'; 			$obj=new OperasiDetail ( $db, $slug, $name ); 	        break;
		}
		if($obj!=null) $obj->initialize ();
		$res=ob_get_clean();
		return $res;
	}
	
	/* when it's star build */
	public function phpPreLoad() {
        global $db;
        $this->tabs = new Tabulator ( 'laporan_pendapatan', "laporan_pendapatan", Tabulator::$LANDSCAPE );
        if(getSettings($db,"smis-rs-lapend-tperawat-".$this->polislug,"1")=="1")
            $this->tabs->add ( "tindakan_perawat_detail", "Tindakan Perawat", "", Tabulator::$TYPE_HTML," fa fa-list","loadPendapatan('tindakan_perawat_detail','".$this->polislug."','".$this->poliname."')");
        if(getSettings($db,"smis-rs-lapend-tigd-".$this->polislug,"1")=="1")
            $this->tabs->add ( "tindakan_igd_detail", "Tindakan Ruangan", "", Tabulator::$TYPE_HTML," fa fa-warning","loadPendapatan('tindakan_igd_detail','".$this->polislug."','".$this->poliname."')");
        if(getSettings($db,"smis-rs-lapend-tdokter-".$this->polislug,"1")=="1")
            $this->tabs->add ( "tindakan_dokter_detail", "Tindakan Dokter", "", Tabulator::$TYPE_HTML," fa fa-user-md","loadPendapatan('tindakan_dokter_detail','".$this->polislug."','".$this->poliname."')");
        if(getSettings($db,"smis-rs-lapend-konsul-".$this->polislug,"1")=="1")
            $this->tabs->add ( "konsul_detail", "Konsul Dokter", "", Tabulator::$TYPE_HTML," fa fa-user-md","loadPendapatan('konsul_detail','".$this->polislug."','".$this->poliname."')");
        if(getSettings($db,"smis-rs-lapend-visite-".$this->polislug,"1")=="1")
            $this->tabs->add ( "visite_detail", "Visite Dokter", "", Tabulator::$TYPE_HTML," fa fa-user-md","loadPendapatan('visite_detail','".$this->polislug."','".$this->poliname."')");
        if(getSettings($db,"smis-rs-lapend-periksa-".$this->polislug,"1")=="1")
            $this->tabs->add ( "periksa_detail", "Periksa Dokter", "", Tabulator::$TYPE_HTML," fa fa-user-md","loadPendapatan('periksa_detail','".$this->polislug."','".$this->poliname."')");
        if(getSettings($db,"smis-rs-lapend-alok-".$this->polislug,"1")=="1")
            $this->tabs->add ( "alat_obat_detail", "Alat Obat", "", Tabulator::$TYPE_HTML," fa fa-glass","loadPendapatan('alat_obat_detail','".$this->polislug."','".$this->poliname."')");
        if(getSettings($db,"smis-rs-lapend-oksigen-central-".$this->polislug,"1")=="1")
            $this->tabs->add ( "oksigen_central_detail", "O2 Central", "", Tabulator::$TYPE_HTML," fa fa-fire-extinguisher","loadPendapatan('oksigen_central_detail','".$this->polislug."','".$this->poliname."')");
        if(getSettings($db,"smis-rs-lapend-oksigen-manual-".$this->polislug,"1")=="1")
            $this->tabs->add ( "oksigen_manual_detail", "O2 Manual", "", Tabulator::$TYPE_HTML," fa fa-fire-extinguisher","loadPendapatan('oksigen_manual_detail','".$this->polislug."','".$this->poliname."')");
        if(getSettings($db,"smis-rs-lapend-bed-".$this->polislug,"1")=="1")
            $this->tabs->add ( "bed_detail", "Bed", "", Tabulator::$TYPE_HTML," fa fa-bed","loadPendapatan('bed_detail','".$this->polislug."','".$this->poliname."')");
        if(getSettings($db,"smis-rs-lapend-operasi-".$this->polislug,"1")=="1")
            $this->tabs->add ( "operasi_detail", "Operasi","", Tabulator::$TYPE_HTML," fa fa-scissors","loadPendapatan('operasi_detail','".$this->polislug."','".$this->poliname."')");
        
        echo $this->tabs->getHtml ();
        echo addJS("rawat/resource/js/laporan_pendapatan.js",false);
	}
}

?>





